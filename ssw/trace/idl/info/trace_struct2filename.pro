function trace_struct2filename, struct, outdir=outdir, $
    soho=soho, prefix=prefix, extension=extension, $
    incwave=incwave, incsize=incsize, hourly=hourly
;+
;   Name: trace_struct2filename
;
;   Purpose: derive filenames from trace structures
;
;   Input Parameters:
;      struct - vector of TRACE structures (ex, output from read_trace)  
;
;   Output:
;      function returns implied filenames, format determined by keywords
;
;   Keyword Parameters:
;      outdir -    if supplied, prepend this path to output filenames
;      soho   -    if set, output in soho-standard format
;      prefix -    if supplied, prepend this prefix to filenames
;      extension - if supplied, append this to filenames
;      incwave -   if set, append wave_nam to filenames 
;      incsize -   if set, append NX to filename 
;      hourly -    if set, name is implied hourly file name ('triyyyymmdd.hh00')
;
;   Calls:
;      time2file, data_chk, concat_dir, gt_tagval, strmids, the usual suspects
;     
;   History:
;      26-Feb-1998 - S.L.Freeland
;      12-Mar-1998 - S.L.Freeland - change order of default naming logic
;-  
if not data_chk(struct,/struct) then begin
   box_message,['Need structure input (ex: output from read_trace)...', $
		'IDL> names = trace_struct2filename']
   return,-1
endif

sarea=gt_tagval(struct,/SOU_AREA,found=found)
saabb=str2arr('a0,a1,a2')

if not found then begin
   box_message,['TRACE struct?? Need at least source area and times']
   return,struct		
endif

soho   = keyword_set(soho)
hourly = keyword_set(hourly)
incwave=keyword_set(incwave)
incsize=keyword_set(incsize)

; define the time part ('time2file' does the dirty work)
time=time2file(struct,delimit=(['_','.'])(hourly),sec=(1-(soho or hourly)))
if hourly then time=strmids(time,0,strlen(time)-2) + '00'
     
; define wave
swave=strmid(gt_tagval(struct,/wave_len)+'____',0,4)

case 1 of
   incwave: fwave='_'+ swave
   else: fwave=''
endcase

sa='_'+saabb(sarea)                        ; pre-define source area piece

; define prefix
case 1 of
   soho: prefix='trac_'+swave +'__'+sa+'_'      ; soho summary 'standard' 
   data_chk(prefix,/string):
   hourly: prefix='tri'
   else: prefix=''
endcase   

; define extension
case 1 of
   data_chk(extension,/string):           ; user defined
   soho:extension='.fts'                  ; soho standard
   else: extension=''                     ; default=null
endcase

if hourly or soho then sa(*)=''           ; reset to null

nsize='_'+string(gt_tagval(struct,/naxis1),format='(i4.4)')  ; naxis piece
if not incsize then nsize(*)=''

; now build the filename
retval=prefix+time+sa+fwave+nsize+extension

; attach (prepend) path if OUTDIR specified
if data_chk(outdir,/string) then retval=concat_dir(outdir,retval)

return,retval
end
