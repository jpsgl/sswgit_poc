
pro suvi_write_badpix, x_arr, y_arr, x_adj, y_adj, badpix_str, _extra=_extra

  setup_suvi_env

  n_badpix = n_elements(x_arr)
  n_adj = n_elements(x_adj[0,*])

  out_buff_hdr =                'Bad Pixel       Replacement Pixels'
  out_buff_hdr = [out_buff_hdr, '[X   ,Y]        [X   ,Y]']
  out_buff_hdr = [out_buff_hdr, '________        ________']
  
  for i=0,n_badpix-1 do begin
;    for j=0,n_adj-1 do begin
     x_adj0 = reform(x_adj[i,*])
     y_adj0 = reform(y_adj[i,*])
     out_buff_gpa0 =       string(x_arr[i],'(i4.4)') + ' ' + string(y_arr[i],'(i4.4)')
     out_buff0     = '[' + string(x_arr[i],'(i4.4)') + ' ' + string(y_arr[i],'(i4.4)') + ']'
     ss_adj0_good = where((x_adj0 ne -1) and (y_adj0 ne -1), n_adj0_good)
     if n_adj0_good gt 0 then begin
        for j=0,n_adj0_good-1 do begin
           if j eq 0 then begin
              out_buff_gpa0 = out_buff_gpa0 + ' '     + string(x_adj0[ss_adj0_good[j]],'(i4.4)') + $
                              ',' + string(y_adj0[ss_adj0_good[j]],'(i4.4)')
              out_buff0     = out_buff0    + ':    [' + string(x_adj0[ss_adj0_good[j]],'(i4.4)') + $
                              ',' + string(y_adj0[ss_adj0_good[j]],'(i4.4)') + ']'
           endif else begin
              out_buff_gpa0 = out_buff_gpa0 + ','   + string(x_adj0[ss_adj0_good[j]],'(i4.4)') + $
                              ',' + string(y_adj0[ss_adj0_good[j]],'(i4.4)')
              out_buff0     = out_buff0     + ', [' + string(x_adj0[ss_adj0_good[j]],'(i4.4)') + $
                              ',' + string(y_adj0[ss_adj0_good[j]],'(i4.4)') + ']'
           endelse
        endfor
        if not exist(out_buff) then begin
           out_buff     = out_buff0
           out_buff_gpa = out_buff_gpa0
        endif else begin
           out_buff     = [out_buff, out_buff0]
           out_buff_gpa = [out_buff_gpa, out_buff_gpa0]
        endelse
     endif
  endfor
;STOP
; file_badpix_genx = concat_dir(get_logenv('SUVI_BADPIX'), $
;                    time2file(badpix_str.date_obs, prefix='suvi_badpix.genx'))
; SAVEGEN, file=file_badpix, str=result
  file_badpix_ascii = concat_dir(get_logenv('SUVI_BADPIX'), 'suvi_badpix_' + $
                      time2file(badpix_str.date_obs)) + '.dat'
  file_append, file_badpix_ascii, out_buff_gpa, /new

; Print out bad pixels and replacements:
  print, ''
  print, ''
  prstr, [out_buff_hdr, out_buff]
  print, ''
  print, ''
  
end
