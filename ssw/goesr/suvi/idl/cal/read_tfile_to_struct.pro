FUNCTION read_tfile_to_struct, file

;SUVI function that reads in space delimited items from a text file
;  and produces a structure

;Written by: ABK (29-Feb-2012)

;create structure with read in filename information as first entry
suvi_strct={filename: file}

IF NOT file_exist(file) THEN BEGIN
  print, 'Input file does not exist'
  RETURN, suvi_strct
ENDIF

const_list=rd_tfile(file)
num_const=n_elements(const_list)

FOR i=0, num_const-1 DO BEGIN
  component=strsplit(const_list[i], /EXTRACT)
  num_comp=n_elements(component)
  suvi_strct=add_tag(suvi_strct,double(component[1:num_comp-1]), component[0]) 
ENDFOR

RETURN, suvi_strct

END
