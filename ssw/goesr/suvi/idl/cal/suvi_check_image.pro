
pro suvi_check_image, index, data, wave=wave, $
                      index_arr=index_arr, data_arr=data_arr, $
                      interactive=interactive, use_syn=use_syn, $
                      ref_refresh=ref_refresh, last_refresh=last_refresh, $
                      ratio_threshold=ratio_threshold, $
                      do_display=do_display, _extra=_extra, qstop=qstop

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     SUVI_CHECK_IMAGE
; CATEGORY:
;     Instrument Monitoring
; PURPOSE:
;     Perform a set of standard sanity checks on a SUVI FITS or sequense
;     of images and associated header tags to look for abnormalities.
;     If an abnormal image or header is found, then flag it, log it and notify
;     the humans.
; CALLS:
;     suvi_check_image, index, data
; METHOD:
;     - Check that all header tags are present and of the proper type.
;     - Compare the moments of the image intensity distribution of the image or
;     - images being tested, to those of of a stored reference image,
;       which may be an average of a set of prior images of the same type
;       (wavelength, filter set, exposure duration, etc).
;     - Compare the image or images being tested to the latest prior image or
;       images of the same type.
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     - index - header
;     - data - Image
; OUTPUTS:
;     - Set of parameters quantifying the outcome of the set pf checks
;     - Weirdness_vector - Vector parameter which contains the parametrized measures
;                          of outcomes of all checks
;     - Weirdness scalar - Single combined, weighted scalar weirdness value for each file.
; KEYWORD INPUTS:
;     - do_write - If set, write the generated FITS file(s) to disk.
; KEYWORD OUTPUTS:
;
; CALLED BY ROUTINES:
;     - Standalone.
; ROUTINES CALLED:
;     - writefits
; TRAPPED ERRORS:
;
; UNTRAPPED ERRORS:
;
; NOTA BENA:
; TODO:
; DEVELOPMENT STATUS:
;     2016-04-06 - Doc header added.
; TEST AND VERIFICATION DESCRIPTION:
;     Generated FITS files and headers examined for consistency.
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION History:
;     2016-10-05 GLS - Updates to doc header.
; VERSION CONTROL STATUS:
;     2015-10-05 - Last commit
;-
; ==============================================================================

set_logenv, 'SUVI_REFDB',    concat_dir(get_logenv('SUVI_FM'), 'suvi_refdb')

if not exist(ratio_thrshhold) then ratio_threshold = 10.0
if not exist(do_display) then do_display = 1

; Iteratively select parameters and run suvi_limbfit:

wave_arr_suvi = ['94', '131', '171', '195', '284', '304']
wave_arr_sdo  = ['94', '131', '171', '193',  '94', '304']


ans = ''
while strlowcase(strmid(ans,0,1)) ne 'n' do begin

   
if keyword_set (interactive) then begin
   delvarx, index, data, index_arr, data_arr, moment_arr, hist_arr
   ss_wave = xmenu_sel(wave_arr_suvi)
   wave = wave_arr_suvi[ss_wave]
endif

print, ''
print, '******************************'
print, 'Now running SUVI image checks.'
print, '******************************'
print, ''

if keyword_set(use_syn) then begin
   if not exist(wave) then $
      if exist(index) then $
         wave = fix(strmid(index.asname,0,(strlen(index.asname)-1))) else wave=171
   filnam_syn = 'suvi_syn_' + strtrim(wave,2) + '.sav'
   set_logenv, 'SUVI_SYN_DATA', concat_dir(get_logenv('SUVI_FM'), 'syn_data')
   file_syn = concat_dir(get_logenv('SUVI_SYN_DATA'), filnam_syn)
   restore, file_syn
;  n_img = n_elements(index_arr)
;  if not exist(n_syn_lim) then n_syn_lim = 7
;  if n_syn_lim lt n_img then $
;     ss_keep = indgen(n_syn_lim) + (n_img-n_syn_lim) else $
;     ss_keep = indgen(n_img)
;  index_arr = index_arr[ss_keep]
;  data_arr = data_arr[*,*,ss_keep]
;  index = concat_struct(index_arr, index)
;  data = [[[data_arr]], [[data]]]
endif

n_img = n_elements(index_arr)
if not exist(index) then index = index_arr[n_img-1]
if not exist(data) then data = data_arr[*,*,(n_img-1)]

; Generate statistics of image series:
n_img = n_elements(index_arr)
for i=0, n_img-1 do begin
   if not exist(moment_arr) then moment_arr = moment(data_arr[*,*,i]) else $
      moment_arr = [[moment_arr], [moment(data_arr[*,*,i])]]
   if not exist(hist_arr) then hist_arr = histogram(data_arr[*,*,i], nbins=256) else $
      hist_arr = [[hist_arr], [histogram(data_arr[*,*,i], nbins=256)]]
endfor
   
; Read reference SUVI FITS file of matching filter:
ref_image_file = concat_dir(concat_dir(get_logenv('SUVI_REFDB'), 'ref_images'), $
                            'suvi_ref_image_' + strtrim(wave,2)) + 'A'
print, ''
print, 'Retrieving reference image for wave ' + strtrim(wave,2) + ' from $SUVI_REFDB :'
print, get_logenv('SUVI_REFDB')

ss_status = file_exist(ref_image_file)
if ( (ss_status eq 1) and (not keyword_set(ref_refresh)) ) then begin
   restore, ref_image_file
endif else begin
   index_ref   = index_arr[0]
   data_ref    = data_arr[*,*,0]
   moments_ref = moment(data_ref)
   hist_ref    = histogram(data_ref)
   save, index_ref, data_ref, moments_ref, hist_ref, file=ref_image_file
endelse

; Read last good SUVI FITS file of matching filter:
last_image_file = concat_dir(concat_dir(get_logenv('SUVI_REFDB'), 'last_images'), $
                             'suvi_last_image_' + strtrim(wave,2)) + 'A'
print, ''
print, 'Retrieving last image for wave ' + strtrim(wave,2) + ' from $SUVI_REFDB :'
print, get_logenv('SUVI_REFDB')

ss_status = file_exist(last_image_file)
if ( (ss_status eq 1) and (not keyword_set(last_refresh)) ) then begin
   restore, last_image_file
endif else begin
   index_last   = index_arr[n_img-2]
   data_last    = data_arr[*,*,n_img-2]
   moments_last = moment(data_last)
   hist_last    = histogram(data_last)
   save, index_last, data_last, moments_last, hist_last, file=last_image_file
endelse

; Check header tags:
print, 'Checking header tags...'
print, ''

tag_list_ref = tag_names(index_ref)
n_tags_ref = n_elements(tag_list_ref)
tag_list = tag_names(index)
n_tags = n_elements(tag_list)

print, ''
;for i=0, n_img-1 do begin
;  tag_list = tag_names(index[i])
   tag_list = tag_names(index)
   n_tags = n_elements(tag_list)

   if n_tags_ref ne n_tags then $
      print, 'Wrong number of tags in image.  Returning.' else $
      print, 'Correct Number of tags found in header.'

   for j=0, n_tags-1 do begin
      if tag_list[j] ne tag_list_ref[j] then begin
         print, ''
         if not exist(bad_tag_names) then bad_tag_names = tag_list[j] else $
            bad_tag_names = [bad_tag_names, tag_list[j]]
         print, 'Tag name does not match ref tag name for ' + $
                strtrim(tag_list_ref[j],2) + ' ' + strtrim(tag_list[j],2)
      endif

      if size(index.(j), /type) ne size(index_ref.(j), /type) then begin
         print, ''
         if not exist(bad_tag_type) then bad_tag_type = tag_list[j] else $
            bad_tag_type = [bad_tag_type, tag_list[j]]
         print, 'Tag type does not match ref tag type for ' + $ 
                strtrim(tag_list_ref[j],2) + ' ' + strtrim(tag_list[j],2)
      endif
   endfor

;endfor

if n_elements(bad_tag_names) eq 0 and n_elements(bad_tag_type) eq 0 then begin
   print, ''
   print, 'Tag set matches reference.  Header seems ok.'
endif

; Compare moments of image arrays with moments of reference image array and
; moments of nearest previous image of same wavelength, etc:
print, ''
print, 'Moments of image arrays:'
print, 'Mean:     ', moment_arr[0,*], format = '(a10, 100f8.1)' ; i5.5)'
print, 'Variance: ', moment_arr[1,*], format = '(a10, 100f8.1)'
print, 'Skewness: ', moment_arr[2,*], format = '(a10, 100f8.1)'
print, 'Kurtosis: ', moment_arr[3,*], format = '(a10, 100f8.1)'

print, ''
print, 'Ratio of moments of image arrays to reference image array:'
print, 'Ratio of mean:     ', moment_arr[0,*] / moments_ref[0], format = '(a19, 100f6.2)'
print, 'Ratio of Variance: ', moment_arr[1,*] / moments_ref[1], format = '(a19, 100f6.2)'
print, 'Ratio of Skewness: ', moment_arr[2,*] / moments_ref[2], format = '(a19, 100f6.2)'
print, 'Ratio of Kurtosis: ', moment_arr[3,*] / moments_ref[3], format = '(a19, 100f6.2)'

print, ''
print, 'Ratio of moments of image arrays to last good image array:'
print, 'Ratio of mean:     ', moment_arr[0,*] / moments_last[0], format = '(a19, 100f6.2)'
print, 'Ratio of Variance: ', moment_arr[1,*] / moments_last[1], format = '(a19, 100f6.2)'
print, 'Ratio of Skewness: ', moment_arr[2,*] / moments_last[2], format = '(a19, 100f6.2)'
print, 'Ratio of Kurtosis: ', moment_arr[3,*] / moments_last[3], format = '(a19, 100f6.2)'

ratio_mean_to_ref =  moment_arr[0,(n_img-1)] / moments_ref[0]
print, ''
print, 'Filter pinhole check: If ratio of image mean to reference image mean'
print, 'exceeds threshold, then flag as potential pinhole in aperture filter.'
print, 'Threshold is set to ' + string(ratio_threshold, '$(f6.2)')
print, 'For this image this ratio is: ' + string(ratio_mean_to_ref, '$(f6.2)')
print, ''
if ratio_mean_to_ref gt ratio_threshold then begin
   print, '************************************************'
   print, 'POTENTIAL FILTER PINHOLE DETECTED FOR THIS IMAGE'
   print, 'USING RATIO OF IMAGE MEAN TO REF IMAGE MEAN'
   print, '************************************************'
endif else begin
   print, '***************************************************'
   print, 'NO POTENTIAL FILTER PINHOLE DETECTED FOR THIS IMAGE'
   print, 'USING RATIO OF IMAGE MEAN TO REF IMAGE MEAN'
   print, '***************************************************'
endelse
print, ''

if keyword_set(do_display) then begin
   wdef,1,1024,768-128,/ll
   !p.multi = [0,2,2]
   img_pkt_time = anytim(index_arr.img_pkt_time, /yoh)
   utplot, index_arr.img_pkt_time, moment_arr[0,*] / moments_ref[0], $
           title='Ratio of Means to Ref Mean For ' + strtrim(wave,2) + 'A', $
           charsize=1.5, psym=-2, symsize=2, /ynoz
   utplot, index_arr.img_pkt_time, moment_arr[1,*] / moments_ref[1], $
           title='Ratio of Variances to Ref Variance ' + strtrim(wave,2) + 'A', $
           charsize=1.5, psym=-4, symsize=2, /ynoz
   utplot, index_arr.img_pkt_time, moment_arr[2,*] / moments_ref[2], $
           title='Ratio of Skewness to Ref Skewness ' + strtrim(wave,2) + 'A',  $
           charsize=1.5, psym=-5, symsize=2, /ynoz
   utplot, index_arr.img_pkt_time, moment_arr[3,*] / moments_ref[3], $
           title='Ratio of Kurtosis to Ref Kurtosis ' + strtrim(wave,2) + 'A',  $
           charsize=1.5, psym=-6, symsize=2, /ynoz
   buff = 255b - tvrd() & tv, buff

   wdef,2,1024,768-128,/ul
   !p.multi = [0,1,2]
   ratio_of_means = moment_arr[0,*] / moments_ref[0]
   utplot, index_arr.img_pkt_time, ratio_of_means, $
           yrange=[0.0, 1.1*ratio_threshold], ystyle=1, $
           title='FILTER CHECK: Ratio of Means to Ref Mean For ' + strtrim(wave,2) + 'A', $
           charsize=1.5, psym=-2, symsize=2, /ynoz
;          yrange=[0.9*min(ratio_of_means), 1.1*ratio_threshold], ystyle=1, $
   outplot, index_arr.img_pkt_time, replicate(ratio_threshold, n_img), $
            psym=0, linestyle=2, thick=3
   plot_goes, img_pkt_time[0], img_pkt_time[n_img-1], xstyle=1, charsize=1.5
   buff = 255b - tvrd() & tv, buff
   
;  if not exist(n_sig_crop) then n_sig_crop=3
   if ( not exist(n_sig_crop) and (not keyword_set(do_log)) ) then do_sqrt = 1
   tv_matrix, index=index_arr, data=data_arr, n_sig_crop=n_sig_crop, $
              do_log=do_log, do_sqrt=do_sqrt, /do_lr
endif

; Save current image and statistics in last good image save file,
; overwriting previous image file:
index_last   = index
data_last    = data
moments_last = moment(data_last)
hist_last    = histogram(data_last)
save, index_last, data_last, moments_last, hist_last, file=last_image_file

print, ''
print, '****************************'
print, 'SUVI image checks completed.'
print, '****************************'
print, ''

ans = 'n'
if keyword_set(interactive) then $
   read, 'Select another wave? (def is yes) : ', ans


endwhile


if keyword_set(qstop) then STOP

end


