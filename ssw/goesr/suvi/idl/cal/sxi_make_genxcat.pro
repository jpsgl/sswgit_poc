pro sxi_make_genxcat,time0,time1, _extra=_extra, catdir=catdir, goesn=goesn,$
level=level, parent=parent, only_tags=only_tags, exclude_tags=exclude_tags, test=test, temp=temp,$ 
ascii_list=ascii_list, format=format

only_tags=only_tags+',fname,amp_gain'
;
;+
;   Name: sxi_make_genxcat
;
;   Purpose: obvious
;
;   History:
;      circa 2006 - S.L.Freeland - for GOES-13/sxi
;      2009 - add a few fields to ascii summary
;      1-sep-2009 - swap in 'exptime' for 'exposure'
;
;   Input Parameters:
;       time0,time1 - time to condsider 
;  
;   Keyword Paramters:
;      goesn - satellite#, def=13
;      parent - parent path for catalog file output
;      ascii_list - if set, generate ascci summary list in catalog file
;-
;      
if n_elements(time0) eq 0 then time0=reltime(/yest,/date_only)
if n_elements(time1) eq 0 then time1=reltime(time0,days=2,/date_only)
if n_elements(goesn) eq 0 then goesn=13
if n_elements(level) eq 0 then level=0
sswdbtop='/net/ssw/sswdb/goes/sxig'+strtrim(goesn,2)

case 1 of
    data_chk(parent,/string): ; user supplied
    goesn eq 13: parent='/sxi02/sxifm1a/fits_sec'
    else: parent=concat_dir(sswdbtop,'level0')
endcase

if n_elements(parent) eq 0 then parent='/sxi02/sxifm1a/fits_sec'
ascii_list=keyword_set(ascii_list)
pre='sxi'+strtrim(goesn,2)+'l'+strtrim(level,2)
if n_elements(catdir) eq 0 then $
   catdir='/archive/sswdb/goes/sxig'+ strtrim(goesn,2)+'/genxcat' + $
      (['','_temp'])(keyword_set(temp))

tgrid=timegrid(time0,time1,/days,out='ecs',/date_only) ; daily catalog grid
testing=keyword_set(test)

ncat=n_elements(tgrid)

template={naxis1:0,naxis2:0,xcen:0,ycen:0,crpix1:0,crpix2:0,exptime:0.,$
   shutmdur:0.,exposure:0.,binning:0b,img_mean:0.,img_sdev:0.,sat_pix:0,miss_pix:0,ccd_tmp:0.}

for i=0,ncat-2 do begin 
   sxif=ssw_time2filelist(tgrid(i),tgrid(i+1),parent=parent, patt='*_A*.FTS')
if get_logenv('check_sxif') ne '' then stop,'sxif,parent,tgrid(i)'
   box_message,['Working on range:',arr2str([tgrid(i),tgrid(i+1)],' -to- ')]
   if sxif(0) ne '' then begin 
      if testing then sxif=sxif(0:9<n_elements(sxif)-1) 
      mreadfits_header,sxif,index,only_tags=only_tags
      ;comp=ssw_struct_compress(index,template)
      comp=index
      fname=gt_tagval(comp,/filename,missing=gt_tagval(comp,/fname))
      if ~required_tags(index,'filename') then begin 
         box_message,'adding FILENAME...'
         index=add_tag(index,fname,'filename')
      endif
      if strpos(sxif(0),'20100506') ne -1 then begin 
         box_message,'format change transition...'
         fname=strextract(sxif,'/06/','.FTS')
         comp.filename=fname
      endif
      comp=add_tag(comp,fname,'filename')
      comp=add_tag(comp,gt_tagval(comp,/amp_gain,missing='??'),'ampagain')
      comp=add_tag(comp,gt_tagval(comp,/amp_gain,missing='??'),'ampbgain')
      
      write_genxcat,comp,topdir=catdir,prefix=pre,/neleme,/geny,/day_round,/over
      if ascii_list then begin
         ascii_info=get_infox(index,$
            'filename,ccd_tmp,shutmode,exptime,naxis1,naxis2,binning,wavelnth,f1filter,f2filter,img_code', $
            form='a,f6.1,a,f8.3,i3,i3,i1,a,a,a,a')
         ascii_name=concat_dir(strmids(sxif(0),0,str_lastpos(sxif,'/')),$
            'image_list.txt')
         file_append,ascii_name,ascii_info,/new ; generate ascii listing
         box_message,'ascii_list>> ' + ascii_name
      endif
        
   endif else box_message,'No files for timerange...'
endfor

return
end

