
pro suvi_prep, index, data, indexp, datap, this_img_name=this_img_name, $
               delt_sec_write=delt_sec_write, $
               no_readout_correction=no_readout_correction, $
               no_bad_pix=no_bad_pix, $
               no_bad_col=no_bad_col, $
               no_dark=no_dark, $
               no_flat=no_flat, $
               no_reg=no_reg, $
               no_convert_to_rad=no_convert_to_rad, $
               do_keep_stages=do_keep_stages, $
               do_display=do_display, do_pause=do_pause, do_write=do_write, $
               verbose=verbose, _extra=_extra

; TODO:
;   - Put 'Done with...' print outs within individual processing routines.

;comments = [ $
;  '-------------------------------------------------------------------------------', $
;  ' Begin GPA routines for image file ' + 
;  '-------------------------------------------------------------------------------' ]
;prstr, comments

if not exist(dir_gpa_log) then dir_gpa_log = get_logenv('GPA_LOG')
file_gpa_log = concat_dir(dir_gpa_log, 'gpa_run_log')
file_append, file_gpa_log, '', /new

indexp = index
datap  = float(data)

if keyword_set(do_keep_stages) then begin
  index_stages = index
  data_stages = data
  stage_vec = 'raw'
endif
  
if keyword_set(do_display) then begin
  xsiz_win = 256*5
  ysiz_win = 256*2
  xsiz_cell = 256
  ysiz_cell = 256
  wdef,0,256*5,256*2
  siz_data = size(datap)
  xsiz_data = siz_data[1]
  ysiz_data = siz_data[2]
; xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
; tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 0*256, 1*256
  tvscl, safe_log10(congrid(datap, xsiz_data/5, ysiz_data/5)), 0*256, 1*256
  !p.multi = [10,5,2]
  plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
        title=index.asname + ' Raw Image     ', charsize=2
  if keyword_set(do_pause) then stop
endif
  
if not keyword_set(no_crop) then begin
  CropDownlinkedImage, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'crop']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 1*256, 1*256
    xout_norm = (1*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (1*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [9,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Cropping     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

comments = [ $
  '-------------------------------------------------------------------------------', $
  'Done with CropDownlinkedImage.', $
  '-------------------------------------------------------------------------------' ]
prstr, comments

endif

if not keyword_set(no_readout_correction) then begin
  SerialRegisterReadoutCorrection, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'rdout_corr']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 2*256, 1*256
    xout_norm = (2*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (1*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [8,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Readout Corr     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done withSerialRegisterReadoutCorrection.'
  print, '-------------------------------------------------------------------------------'

endif

if not keyword_set(no_bad_pix) then begin
  ReplacePermanentBadPixel, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'badpixreplace']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 3*256, 1*256
    xout_norm = (3*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (1*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [7,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Bad Pix Replace     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with ReplacePermanentBadPixel'
  print, '-------------------------------------------------------------------------------'
endif

if not keyword_set(no_bad_col) then begin
  ReplacePermanentBadColumn, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'badcolreplace']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 4*256, 1*256
    xout_norm = (4*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (1*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [6,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Bad Col Replace     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with ReplacePermanentBadColumn'
  print, '-------------------------------------------------------------------------------'
endif

if not keyword_set(no_dark) then begin
  SubtractProcessedDark, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'darksub']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 0*256, 0*256
    xout_norm = (0*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (0*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [5,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Dark Sub     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with SubtractProcessedDark'
  print, '-------------------------------------------------------------------------------'
endif

if not keyword_set(no_flat) then begin
  CorrectVignettingAndFlatField, indexp, datap, indexp, datap, flat_select=flat_select, $
                                 _extra=_extra

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'flat']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 1*256, 0*256
    xout_norm = (1*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (0*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [4,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Flat Field     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with CorrectVignettingAndFlatField'
  print, '-------------------------------------------------------------------------------'
endif

if not keyword_set(no_reg) then begin
  RotationOrientationCorrection, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'derot']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
    tvscl, safe_log10(rebin(datap, xsiz_data/5, ysiz_data/5, /samp)), 2*256, 0*256
    xout_norm = (2*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (0*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [3,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Rotation     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with RotationOrientationCorrection'
  print, '-------------------------------------------------------------------------------'
endif

if not keyword_set(no_convert_to_rad) then begin
  ConvertToRadiance, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'convertorad']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
; TODO - Replace with better scaling algorithm here:
n_sig_crop = 3
mom_datap = moment(datap)
datap_disp = datap < (mom_datap[0] + n_sig_crop*sqrt(mom_datap[1]))
    tvscl, safe_log10(rebin(datap_disp, xsiz_data/5, ysiz_data/5, /samp)), 3*256, 0*256
    xout_norm = (3*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (0*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [2,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Conv To Radiance     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with ConvertToRadiance'
  print, '-------------------------------------------------------------------------------'
endif

if not keyword_set(no_decontam) then begin
  CorrectContaminationSignalLoss, indexp, datap, indexp, datap

  if keyword_set(do_keep_stages) then begin
    index_stages = concat_struct(index_stages, indexp)
    data_stages = [[[data_stages]],[[datap]]]
    stage_vec = [stage_vec, stage_vec + '+' + 'decontam']
  endif

  if keyword_set(do_display) then begin
    siz_data = size(datap)
    xsiz_data = siz_data[1]
    ysiz_data = siz_data[2]
;   xstepper,safe_log10(rebin([[[datap]],[[datap]]], xsiz_data/2, ysiz_data/2, 2, /samp))
; TODO - Replace with better scaling algorithm here:
n_sig_crop = 3
mom_datap = moment(datap)
datap_disp = datap < (mom_datap[0] + n_sig_crop*sqrt(mom_datap[1]))
    tvscl, safe_log10(rebin(datap_disp, xsiz_data/5, ysiz_data/5, /samp)), 4*256, 0*256
    xout_norm = (4*256 + .1*xsiz_cell)/xsiz_win
    yout_norm = (0*256 + .1*ysiz_cell)/ysiz_win
    !p.multi = [1,5,2]
    plot, indgen(10), /nodata, /noerase, xstyle=4, ystyle=4, $
          title=index.asname + ' After Contam Correct     ', charsize=2
    if keyword_set(do_pause) then stop
  endif

  print, '-------------------------------------------------------------------------------'
  print, 'Done with CorrectContaminationSignalLoss'
  print, '-------------------------------------------------------------------------------'
endif

; Begin adding additional header tags, and FITS file creation:

t_sec_write_start = anytim(!stime)

;outfil_prepped = time2file(indexp.date_obs) + '_' + $
;                 string(indexp.wavelnth, '(i4.4)') + '.fits'
;outfil_l1b = this_img_name + '_l1b.fits'
;outfil_l1b = concat_dir(get_logenv('SUVI_L1B'), outfil_l1b)

; Read in 'external data':
SUVI_BAD_PIXELS       = read_bad_pixels()
SUVI_BAD_CCD_COLUMNS  = read_bad_columns()
SUVI_FLAT             = set_suvi_flat_files()
STATIC_PARAMS         = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_static.txt'))
measured_params       = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_measured_params.txt'))
EPHEMERIS             = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_sim_ephemeris.txt'))
ENTRANCE_TRANS        = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_ent_filter_trans.txt'))
FOCAL_PLANE_FILTER_TRANSMISSION $
                      = read_tfile_to_array( concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_fw_filter_trans.txt'))
FOCAL_PLANE_FILTER1_TRANSMISSION $
                      = read_tfile_to_array( concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_fw1_filter_trans.txt'))
FOCAL_PLANE_FILTER2_TRANSMISSION $
                      = read_tfile_to_array( concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_fw2_filter_trans.txt'))
SUVI_LINEARITY        = read_tfile_to_array( concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_linearity.txt'))
SUVI_GAIN_LEFT        = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_gain_left_amp.txt'))
SUVI_GAIN_RIGHT       = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                        'suvi_gain_right_amp.txt'))

; Define wavelength and spectral channel description tags:
wavelnth = strtrim(string(ulong(STATIC_PARAMS.WAVELENGTH(indexp.asstatus))),1)
fw1_str  = string(indexp.fw1status, '(i1.1)')
fw2_str  = string(indexp.fw2status, '(i1.1)')
chnl_description = $
  wavelnth + 'A' + '__FW1-' + fw1_str + '__FW2-' + fw2_str + '__' + indexp.exptype

; Define sun center pixel location tags:
SUN_CENT_PIX_U2 = indexp.gtyerr * $
                  MEASURED_PARAMS.GT_READING_TO_PIXEL_CONV_U2[0]      + $
                  (STATIC_PARAMS.SUVI_GT_ET_OFFSET_U2_ARCSEC[0]       / $
                  MEASURED_PARAMS.SUVI_PLT_SCL[0])                    + $
                  STATIC_PARAMS.WAVELENGTH_OFFSET_U2(indexp.asstatus)

SUN_CENT_PIX_U3 = indexp.gtzerr * $
                  MEASURED_PARAMS.GT_READING_TO_PIXEL_CONV_U3[0]      + $
                  (STATIC_PARAMS.SUVI_GT_ET_OFFSET_U3_ARCSEC[0]       / $
                  MEASURED_PARAMS.SUVI_PLT_SCL[0])                    + $
                  STATIC_PARAMS.WAVELENGTH_OFFSET_U2(indexp.asstatus)

SUN_CENT_PIX_U2 = round(SUN_CENT_PIX_U2)
SUN_CENT_PIX_U3 = round(SUN_CENT_PIX_U3)

; Calculate image statistics:
IMG_TOT = total(datap)
IMG_MIN = min(datap)
IMG_MAX = max(datap)
IMG_MED = median(datap)
IMG_AVG = average(datap)
IMG_STDV = stdev(datap)

; Define info and statistics on the permanent bad pixels:
numbad_pix = suvi_bad_pixels.nbad
bad_pix_str = ''
FOR i=0, numbad_pix-1 DO $
   bad_pix_str = bad_pix_str + ',' + $
   strtrim(suvi_bad_pixels.(i+2).bad_x,2) + ',' + $
   strtrim(suvi_bad_pixels.(i+2).bad_y,2)
bad_pix_str = strmid(bad_pix_str, 1, strlen(bad_pix_str)-1)

; Define info and statistics on the permanent bad columns:
numbad_col = suvi_bad_ccd_columns.nbad
bad_col_str = ''
FOR i=0, numbad_col-1 DO $
  bad_col_str = bad_col_str + ',' + strtrim(suvi_bad_ccd_columns.(i+2).bad_c,2)
bad_col_str = strmid(bad_col_str, 1, strlen(bad_col_str)-1)

; Define number of corrected pixels:
fixed_pixels = numbad_pix + (numbad_col*indexp.NAXIS1)

; Define number of missing pixels (pixels with value set to -9999):
ss_missing = where(datap eq -9999, num_misspix)

; Add additional structure tags to indexp:
 indexp = add_tag(indexp, anytim(indexp.img_pkt_time, /ccsds), 'date_obs')
 indexp = add_tag(indexp, strmid(indexp.date_obs, 0,10),'date')
 indexp = add_tag(indexp, strmid(indexp.date_obs,11,12),'time')
 indexp = add_tag(indexp, 'GOES-16', 'satid')
 indexp = add_tag(indexp, 'SUVI_FM1', 'instrid') 
;indexp = add_tag(indexp, outfil_l1b, 'L1BIMG')
 indexp = add_tag(indexp, (EPHEMERIS.SOLAR_DISTANCE[0]*149598000), 'SUNDIST')
 indexp = add_tag(indexp, ' ', 'HELIOLON')
 indexp = add_tag(indexp, ' ', 'HELIOLAT')
 indexp = add_tag(indexp, 'HPLN-TAN', 'CTYPE1')
 indexp = add_tag(indexp, 'HPLAT-TAN', 'CTYPE2')
 indexp = add_tag(indexp, SUN_CENT_PIX_U3, 'CRPIX1') 
 indexp = add_tag(indexp, SUN_CENT_PIX_U2, 'CRPIX2')
 indexp = add_tag(indexp, ((640-SUN_CENT_PIX_U3)*MEASURED_PARAMS.SUVI_PLT_SCL[0]), 'CRVAL1')
 indexp = add_tag(indexp, ((640-SUN_CENT_PIX_U2)*MEASURED_PARAMS.SUVI_PLT_SCL[0]), 'CRVAL2')
 indexp = add_tag(indexp, '9.5deg', 'CROTA2')          ; TOFIX - Should be table lookup
 indexp = add_tag(indexp, '2.5arcsec/pixel', 'CDELT1') ; TOFIX - Should be table lookup
 indexp = add_tag(indexp, '2.5arcsec/pixel', 'CDELT2') ; TOFIX - Should be table lookup
 indexp = add_tag(indexp, '180deg', 'LONPOLE')         ; TOFIX - Should be table lookup
 indexp = add_tag(indexp, indexp.frameType, 'VIEWOBJ')
 indexp = add_tag(indexp, chnl_description, 'SPECCHNL')
 indexp = add_tag(indexp, indexp.sht_exp_time, 'EXPTIME')
 indexp = add_tag(indexp, indexp.cmd_exp_time, 'CMDEXPTM')
 indexp = add_tag(indexp, indexp.ccdtemp1, 'CCDTEMP')
 indexp = add_tag(indexp, ulong(wavelnth), 'WAVELNTH')
 indexp = add_tag(indexp, 'Ver.3', 'GPAVER')
 indexp = add_tag(indexp, SUVI_FLAT[indexp.asstatus], 'FLATFN')
;indexp = add_tag(indexp, RAW_IMG_LOCATION, 'L0IMG')
;indexp = add_tag(indexp, SUVI_GAIN_CONSTANT, 'GAIN')
;indexp = add_tag(indexp, SUVI_LINEARITY_SOURCE, 'LINLOC')
;indexp = add_tag(indexp, PHOT_ELE_CONV, 'PHOTELEC')
;indexp = add_tag(indexp, QE, 'QE')
;indexp = add_tag(indexp, PHOT_ENG_CONV, 'PHOTENG')
;indexp = add_tag(indexp, SUVI_EFFA, 'EFFAREA')
 indexp = add_tag(indexp, indexp.rdoutport, 'AMP')
 indexp = add_tag(indexp, anytim(indexp.time_last_bakeout, /ccsds), 'LSTBKOUT')
;indexp = add_tag(indexp, SUVI_CONTAM_THICKNESS, 'CTM_THCK')
;indexp = add_tag(indexp, SUVI_TOT_IRRADIANCE, 'IRRADNCE')
 indexp = add_tag(indexp, IMG_TOT, 'IMG_TOT')
 indexp = add_tag(indexp, IMG_MIN, 'IMG_MIN')
 indexp = add_tag(indexp, IMG_MAX, 'IMG_MAX')
 indexp = add_tag(indexp, IMG_MED, 'IMG_MED')
 indexp = add_tag(indexp, IMG_STDV, 'IMG_STDV')
 indexp = add_tag(indexp, IMG_AVG, 'IMG_AVG')
 indexp = add_tag(indexp, -9999, 'MISSPXVL')
 indexp = add_tag(indexp, num_misspix, 'MISSPXCT')
;indexp = add_tag(indexp, BAD_PIX_FN, 'BADPIXFN')
 indexp = add_tag(indexp, numbad_pix, 'BADPIXCT')
 indexp = add_tag(indexp, bad_pix_str, 'BADPX001')
;indexp = add_tag(indexp, BAD_COL_FN, 'BADCOLFN')
 indexp = add_tag(indexp, numbad_col, 'BADCOLCT')
 indexp = add_tag(indexp, bad_col_str, 'BADCL001')
 indexp = add_tag(indexp, fixed_pixels, 'FIXPIX')
 indexp = add_tag(indexp, EPHEMERIS.SOLAR_DIAMETER_ARCMIN[0], 'SOLDIA')
 indexp = add_tag(indexp, ((EPHEMERIS.SOLAR_DIAMETER_ARCMIN[0]*60) / $
                           MEASURED_PARAMS.SUVI_PLT_SCL[0]), 'SOLDIAPX')
 indexp = add_tag(indexp, EPHEMERIS.SOLAR_B_ANGLE[0], 'SOLBANGL')
;indexp = add_tag(indexp, SN_LEV3, 'SNLEV3')
;indexp = add_tag(indexp, FULL_WELL, 'FULLWELL')
 indexp = add_tag(indexp, indexp.gtyerr, 'GTERRU2')
 indexp = add_tag(indexp, indexp.gtzerr, 'GTERRU3')

; Add tags requested by Dan Seaton:
 indexp = add_tag(indexp, indexp.crota2, 'CROTA')
 indexp = add_tag(indexp, 1.0, 'PC1_1')
 indexp = add_tag(indexp, 0.0, 'PC1_2')
 indexp = add_tag(indexp, 0.0, 'PC2_1')
 indexp = add_tag(indexp, 1.0, 'PC2_2')
 indexp = add_tag(indexp, 'GPA ver4.0', 'CREATOR') ; TODO: Use GPAVER for part of this
 indexp = add_tag(indexp, indexp.heliolon, 'HGLN_OBS')
 indexp = add_tag(indexp, indexp.heliolat, 'HGLT_OBS')
 indexp = add_tag(indexp, 0.0, 'HEEX_OBS')
 indexp = add_tag(indexp, 0.0, 'HEEY_OBS')
 indexp = add_tag(indexp, 0.0, 'HEEZ_OBS')
 indexp = add_tag(indexp, 0.0, 'DATASUM')
 indexp = add_tag(indexp, 0.0, 'CHECKSUM')
 indexp = add_tag(indexp, 0.0, 'DIAM_SUN')
 indexp = add_tag(indexp, 0, 'UUID')
 indexp = add_tag(indexp, '', 'PROD_SITE')

; TOFIX - KLUGE - Removing tags with > 8 character names:
 indexp = rem_tag(indexp, 'img_pkt_time')
 indexp = rem_tag(indexp, 'sht_exp_time')
 indexp = rem_tag(indexp, 'cmd_exp_time')
 indexp = rem_tag(indexp, 'time_last_bakeout')

; Set NAXIS1, NAXIS2 tags for prepped header structure:
 siz_datap = size(datap)
 indexp.naxis1 = siz_datap[1]
 indexp.naxis2 = siz_datap[2]
 indexp.encol  = siz_datap[1]
 indexp.enrow  = siz_datap[2]
 
 print, '-------------------------------------------------------------------------------'
 print, 'Image Statistics:'
 print, '-------------------------------------------------------------------------------'
 print, '  Total   of pixel values: ' + strtrim(IMG_TOT,2)
 print, '  Minimum of pixel values: ' + strtrim(IMG_MIN,2)
 print, '  Maximum of pixel values: ' + strtrim(IMG_MAX,2)
 print, '  Average of pixel values: ' + strtrim(IMG_AVG,2)
 print, '  Median  of pixel values: ' + strtrim(IMG_MED,2)
 print, '  Standard deviation of pixel values: ' + strtrim(IMG_STDV,2)

; TODO Fix this kluge:
; Convert datap from double to float before FITS write and return:
 datap = float(datap)

if keyword_set(do_keep_stages) then begin
  index_stages = concat_struct(index_stages, indexp)
  data_stages = [[[data_stages]],[[datap]]]
  stage_vec = [stage_vec, stage_vec + '+' + 'final']
endif

if keyword_set(do_write) then begin
; indexp_out = indexp.SUVI_FITS_HDR_VALUES
  indexp_out = indexp
  outfil_l1b = this_img_name + '_l1b.fits'
  set_logenv, 'SUVI_L1B', '/archive1/suvi/suvi_data/fm1/l1b'
  outfil_l1b = concat_dir(get_logenv('SUVI_L1B'), outfil_l1b)
  mwritefits, indexp_out, datap, outfile=outfil_l1b, loud=verbose
endif

; Calculate delt for FITS creation:
t_sec_write_end = anytim(!stime)
delt_sec_write = t_sec_write_end - t_sec_write_start

end
