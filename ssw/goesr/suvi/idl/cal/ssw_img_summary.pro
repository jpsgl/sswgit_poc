
pro ssw_img_summary, index, data, verbose=verbose, do_write=do_write

if get_logenv('SUVI_LOGS') eq '' then setenv, 'SUVI_LOGS', './'

IMG_TOT  = total(data)
IMG_MIN  = min(data)
IMG_MAX  = max(data)
IMG_MED  = median(data)
IMG_AVG  = average(data)
IMG_STDV = stdev(data)

buff = ['Total:  ' + string(IMG_TOT),  $
        'Min:    ' + string(IMG_MIN),  $
	'Max:    ' + string(IMG_MAX),  $
	'Avg:    ' + string(IMG_AVG),  $
	'StDev:  ' + string(IMG_STDV), $
	'Median: ' + string(IMG_MED),  $
	'Data Type: ' + strcompress(data_type(suvi_img), /remove)]

if keyword_set(verbose) then $
  prstr, buff

if keyword_set(do_write) then $
  file_append, concat_dir(get_logenv('SUVI_LOGS', 'gpa_run_log.txt')

end
