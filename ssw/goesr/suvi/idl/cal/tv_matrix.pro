
pro tv_matrix, index=index, data=data, nx=nx, $
               n_sig_crop=n_sig_crop, do_log=do_log, do_sqrt=do_sqrt, $
               do_lr=do_lr

if ( (not exist(index)) or (not exist(data)) ) then begin
   restore, '/archive1/suvi/suvi_data/fm1/syn_data/suvi_syn_171.sav'
   index = index_arr
   data = data_arr
endif

siz_data = size(data)
xsiz_data = siz_data[1]
ysiz_data = siz_data[2]
n_img = siz_data[3]

aspect = float(ysiz_data)/float(xsiz_data)

if not exist(xsiz_win) then xsiz_win = 1024
if not exist(nx) then nx = ceil(sqrt(n_img)) ; nx=10
xsiz_cell = float(xsiz_win)/float(nx)
;if not exist(ny) then ny = ceil(nx/aspect)
ny = ceil(float(n_img)/float(nx))
ysiz_cell = fix(xsiz_cell*aspect)
ysiz_win = (ny)*ysiz_cell

wdef,0,xsiz_win,ysiz_win,/ur
;!p.multi = [0,3,2]

lil_data = congrid(data,xsiz_cell,ysiz_cell,n_img)
for i=0,n_img-1 do begin
   lil_data0 = lil_data[*,*,i]
   if exist(n_sig_crop) then begin
      mom_data0 = moment(lil_data0)
      lil_data0 = lil_data0 < (mom_data0[0] + n_sig_crop*sqrt(mom_data0[1]))
   endif
   if keyword_set(do_log) then lil_data0 = safe_log10(lil_data0)
   if keyword_set(do_sqrt) then lil_data0 = sqrt(lil_data0)

   if ( (i eq n_img-1) and keyword_set(do_lr) ) then begin
      xpos0 = (nx-1)*xsiz_cell
      ypos0 = 0.0
   endif else begin
      xpos0 = (i mod nx)*xsiz_cell
      ypos0 = ysiz_win - ((fix(i / nx)+1)*ysiz_cell)
   endelse
   xpos1 = xpos0 + xsiz_cell
   ypos1 = ypos0 + ysiz_cell
;  tvscl, lil_data0, (i mod nx)*xsiz_cell, ysiz_win - ((fix(i / nx)+1)*ysiz_cell)
   tvscl, lil_data0, xpos0, ypos0
;  !p.position = [xpos0/xsiz_win, ypos0/ysiz_win, xpos1/xsiz_win, ypos1/ysiz_win
   xout_norm = (xpos0 + .10*xsiz_cell)/xsiz_win
   yout_norm = (ypos0 + .11*ysiz_cell)/ysiz_win
   xyouts, xout_norm, yout_norm, /norm, charsize=2.0, $
           'Wave: ' + index[i].asname
   t_string = strmid(anytim(index[i].img_pkt_time, /ecs), 0, 16)
   xout_norm = (xpos0 + .10*xsiz_cell)/xsiz_win
   yout_norm = (ypos0 + .04*ysiz_cell)/ysiz_win
   xyouts, xout_norm, yout_norm, /norm, charsize=2.0, $
           'Date: ' + t_string
   if (i eq n_img-1) then begin
      xout_norm = (xpos0 + .10*xsiz_cell)/xsiz_win
      yout_norm = (ypos0 + .90*ysiz_cell)/ysiz_win
      xyouts, xout_norm, yout_norm, /norm, charsize=2.0, 'Current Image'
      xleft  = xpos0 / xsiz_win
      xright = 0.99*(xpos0 + xsiz_cell)/xsiz_win
      ybot   = ypos0 / ysiz_win
      ytop   = (ypos0 + ysiz_cell)/ysiz_win
      plots, [xleft, xright, xright, xleft, xleft], $
             [ybot,  ybot,   ytop,   ytop,  ybot ], thick=3, /norm
   endif
endfor

end
