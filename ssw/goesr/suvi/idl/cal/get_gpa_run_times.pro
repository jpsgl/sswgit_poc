
pro get_gpa_run_times, gpa_run_log, omit_first=omit_first

  if not exist(gpa_run_log) then $
     gpa_run_log = '/Users/slater/soft/idl_temp/suvi/plt/temp/gpa_run_log_1.txt'

  buff = rd_tfile(gpa_run_log)

;CropDownlinkedImage took:             0.079 seconds
;SerialRegisterReadoutCorrection took: 0.0   seconds
;ReplacePermanentBadPixel took:        0.016 seconds
;ReplacePermanentBadColumn took:       0.11  seconds
;SubtractProcessedDark took:           2.1   seconds
;CorrectVignettingAndFlatField took:   0.016 seconds
;RotationOrientationCorrection took:   0.012 seconds
;ConvertToRadiance took:               4.0   seconds
;CorrectContaminationSignalLoss took:  0.081 seconds
;EXECUTION TIME (SECONDS) FOR THIS CASE =  5.87
  
  ss_crop    = where(strpos(buff, 'CropDownlinkedImage took:')               ne -1, n_crop)
  ss_readout = where(strpos(buff, 'SerialRegisterReadoutCorrection took:')   ne -1, n_readout)
  ss_badpix  = where(strpos(buff, 'ReplacePermanentBadPixel took:')          ne -1, n_badpix)
  ss_badcol  = where(strpos(buff, 'ReplacePermanentBadColumn took:')         ne -1, n_badcol)
  ss_dark    = where(strpos(buff, 'SubtractProcessedDark took:')             ne -1, n_dark)
  ss_flat    = where(strpos(buff, 'CorrectVignettingAndFlatField took:')     ne -1, n_flat)
  ss_rot     = where(strpos(buff, 'RotationOrientationCorrection took:')     ne -1, n_rot)
  ss_rad     = where(strpos(buff, 'ConvertToRadiance took:')                 ne -1, n_rad)
  ss_loss    = where(strpos(buff, 'CorrectContaminationSignalLoss took:')    ne -1, n_loss)
  ss_process = where(strpos(buff, 'EXECUTION TIME (SECONDS) FOR THIS CASE = ')  ne -1, n_process)
  
  buff_stage = buff[ss_crop]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_crop = t_sec_stage

  mom_crop = moment(t_sec_stage)
  max_crop = max(t_sec_stage)

  buff_stage = buff[ss_readout]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_readout = t_sec_stage

  mom_readout = moment(t_sec_stage)
  max_readout = max(t_sec_stage)

  buff_stage = buff[ss_badpix]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_badpix = t_sec_stage

  mom_badpix = moment(t_sec_stage)
  max_badpix = max(t_sec_stage)

  buff_stage = buff[ss_badcol]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_badcol = t_sec_stage

  mom_badcol = moment(t_sec_stage)
  max_badcol = max(t_sec_stage)

  buff_stage = buff[ss_dark]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_dark = t_sec_stage

  mom_dark = moment(t_sec_stage)
  max_dark = max(t_sec_stage)

  buff_stage = buff[ss_flat]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_flat = t_sec_stage

  mom_flat = moment(t_sec_stage)
  max_flat = max(t_sec_stage)

  buff_stage = buff[ss_rot]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_rot = t_sec_stage

  mom_rot = moment(t_sec_stage)
  max_rot = max(t_sec_stage)

  buff_stage = buff[ss_rad]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_rad = t_sec_stage

  mom_rad = moment(t_sec_stage)
  max_rad = max(t_sec_stage)

  buff_stage = buff[ss_loss]
  pos_took = (strpos(buff_stage, 'took:'))[0]
  pos_sec = (strpos(buff_stage, 'seconds'))[0]
  buff_time = strtrim(strmid(buff_stage, pos_took+5, (pos_sec-1)-(pos_took+4)),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_loss = t_sec_stage

  mom_loss = moment(t_sec_stage)
  max_loss = max(t_sec_stage)

  buff_stage = buff[ss_process]
  pos_equal = (strpos(buff_stage, '= '))[0]
  buff_time = strtrim(strmid(buff_stage, pos_equal+1),2)
  t_sec_stage = float(buff_time)
  if keyword_set(omit_first) then t_sec_stage = t_sec_stage[1:*]
  t_sec_process = t_sec_stage
  
  mom_process = moment(t_sec_stage)
  max_process = max(t_sec_stage)

  mean_crop    = mom_crop[0]
  mean_readout = mom_readout[0]
  mean_badpix  = mom_badpix[0]
  mean_badcol  = mom_badcol[0]
  mean_dark    = mom_dark[0]
  mean_flat    = mom_flat[0]
  mean_rot     = mom_rot[0]
  mean_rad     = mom_rad[0]
  mean_loss    = mom_loss[0]

  mean_process = mom_process[0]
  
  mean_tot = mean_crop + mean_readout + mean_badpix + mean_badcol + $
             mean_dark + mean_flat + mean_rot + mean_rad + mean_loss

  max_tot = max_crop + max_readout + max_badpix + max_badcol + $
            max_dark + max_flat + max_rot + max_rad + max_loss

  t_sec_tot = t_sec_crop + t_sec_readout + t_sec_badpix + t_sec_badcol + $
              t_sec_dark + t_sec_flat + t_sec_rot + t_sec_rad + t_sec_loss

  t_sec_misc = t_sec_process - t_sec_tot
  mom_misc = moment(t_sec_misc)
  max_misc = max(t_sec_misc)
  
  mean_misc = mom_misc[0]

  print, 'mean_run_time_crop    = ' + string(mean_crop,    format='$(f6.3)')
  print, 'mean_run_time_readout = ' + string(mean_readout, format='$(f6.3)')
  print, 'mean_run_time_badpix  = ' + string(mean_badpix,  format='$(f6.3)')
  print, 'mean_run_time_badcol  = ' + string(mean_badcol,  format='$(f6.3)')
  print, 'mean_run_time_dark    = ' + string(mean_dark,    format='$(f6.3)')
  print, 'mean_run_time_flat    = ' + string(mean_flat,    format='$(f6.3)')
  print, 'mean_run_time_rot     = ' + string(mean_rot,     format='$(f6.3)')
  print, 'mean_run_time_rad     = ' + string(mean_rad,     format='$(f6.3)')
  print, 'mean_run_time_loss    = ' + string(mean_loss,    format='$(f6.3)')

  print, 'mean_run_time_misc    = ' + string(mean_misc,    format='$(f6.3)')

  print, 'mean total means = ' + string(mean_tot, format='$(f6.3)')
  
  print, 'max_run_time_crop    = ' + string(max_crop,    format='$(f6.3)')
  print, 'max_run_time_readout = ' + string(max_readout, format='$(f6.3)')
  print, 'max_run_time_badpix  = ' + string(max_badpix,  format='$(f6.3)')
  print, 'max_run_time_badcol  = ' + string(max_badcol,  format='$(f6.3)')
  print, 'max_run_time_dark    = ' + string(max_dark,    format='$(f6.3)')
  print, 'max_run_time_flat    = ' + string(max_flat,    format='$(f6.3)')
  print, 'max_run_time_rot     = ' + string(max_rot,     format='$(f6.3)')
  print, 'max_run_time_rad     = ' + string(max_rad,     format='$(f6.3)')
  print, 'max_run_time_loss    = ' + string(max_loss,    format='$(f6.3)')

  print, 'max_run_time_misc    = ' + string(max_misc,    format='$(f6.3)')

  print, 'mean total maxs = ' + string(max_tot, format='$(f6.3)')
  
;Receipt of last bit of image data - Marks start of image processing
;Image reconstruction - Packet reader builds next image and header
;Accessing Calibration Frames - Included in each routine separately
;Apply system gain and linearity corrections - Included in ConvertToRadiance
;Subtract dark frame - SubtractProcessedDark
;Flat-field/Vignette Corrections - CorrectVignettingAndFlatField
;Consequential Corrections - Includes: CropDownlinkedImage,
;                                      SerialRegisterReadoutCorrection
;                                      ReplacePermanentBadPixel
;                                      ReplacePermanentBadColumn
;                                      CorrectContaminationSignalLoss
;Calculate Incident Photon Counting Rate - ConvertToRadiance
;Append Metadata 
;L1b Data Product in specified data format
;Margin/Reserve

  STOP

end
