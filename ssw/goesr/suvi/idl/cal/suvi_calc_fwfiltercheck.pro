pro SUVI_CALC_FWFILTERCHECK

;+
;
; Reads in log files from a FW filter check and flips through the images
; to ensure that there is no light leakage
;
;-

logpath = '/net/em1a/disk1/egsesw/log/'

if N_ELEMENTS(filename) eq 0 then filename = DIALOG_PICKFILE(path=logpath + 'user_files/', filter = '*filter*.usr')
filtertags = ['$GI_EXPOSURE', '$LI_FW_POS', 'PICTURE']
filtdat = READ_STIMLOG(filename, filtertags, extradat, alldat)

numimg = N_ELEMENTS(filtdat)
for i = 0, numimg-1 do begin
	help, filtdat[i], /str
	splitfile = STRSPLIT(/extract, filtdat[i].picture, '_')
    filenum = STRING(ULONG(splitfile[2]), format = '(i07)')
    filestr = FILE_SEARCH('/net/em1a/disk1/egsesw/log/fits/*' + filenum + '.fits')
	if N_ELEMENTS(filestr) gt 1 then begin
		filetai = FILE2TIME(out='tai', STRJOIN(splitfile[0:1], '_'))
		foundtai = FILE2TIME(out='tai', STRMID(filestr, 32, 15))
		taidiff = ABS(filetai - foundtai)
		matchfile = WHERE(taidiff eq MIN(taidiff))
		filestr = filestr[matchfile]
	endif
	filestr = filestr[0]

	img = READFITS(filestr, hdr)
	answer = ''
	READ, 'Hit enter to continue', answer
	IMG_SUMMARY, img
endfor

end

