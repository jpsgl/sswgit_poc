
pro mk_suvi_full_header2

; In mk_suvi_full_header :

tnames_isp_mnem_new = 'ispm_' + string(indgen(n_tags_isp_mnem)+1,'(i3.3)')
for i=1,n_tags_isp_mnem do suvi_fits_isp_mnem_hdr = $
   rep_tag_name(suvi_fits_isp_mnem_hdr, tnames_isp_mnem[i-1], tnames_isp_mnem_new[i-1])
suvi_fits_isp_mnem_hdr_comments = tnames_isp_mnem

suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_mnem_hdr)

; Changed to:

tnames_isp_mnem_new = 'ispm_' + string(indgen(n_tags_isp_mnem)+1,'(i3.3)')
for i=0,n_tags_isp_mnem-1 do suvi_fits_isp_mnem_hdr2 = $
   add_tag(suvi_fits_isp_mnem_hdr2, suvi_fits_isp_mnem_hdr.(i), tnames_isp_mnem_new(i))
suvi_fits_isp_mnem_hdr_comments = tnames_isp_mnem

suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_mnem_hdr2)

end





