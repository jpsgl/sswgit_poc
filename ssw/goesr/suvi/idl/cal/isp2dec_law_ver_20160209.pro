
function isp2dec, byte_arr, n_shift_bit, do_debug=do_debug, quiet=quiet

if not keyword_set(quiet) then quiet = 1

dec2bin, byte_arr, bin_arr, quiet=quiet
bin_vec = reform(bin_arr, n_elements(bin_arr))
bin2hex, reform(bin_arr[*,0]), hex_val0, quiet=quiet
bin2hex, reform(bin_arr[*,1]), hex_val1, quiet=quiet
;shex_val0 = string(hex_val0, format='(i4.4)')
;shex_val1 = string(hex_val1, format='(i4.4)')
shex_val0 = string(hex_val0)
shex_val1 = string(hex_val1)
if strlen(shex_val0) eq 1 then shex_val0 = '0' + shex_val0
if strlen(shex_val1) eq 1 then shex_val1 = '0' + shex_val1
shex_val  = shex_val0 + shex_val1
smask = 'fff0'
cmd = 'num0 = ('  + "'" + shex_val + "'x" + ' and ' + "'" + smask + "'x" + ')/' + '16'

if not keyword_set(quiet) then print, cmd
result = execute(cmd)

if keyword_set(do_debug) then $
   STOP, 'ISP2DEC: Stopping before return.'
return, num0

end
