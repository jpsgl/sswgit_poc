
pro mk_json2, instr_arr, waves_arr, fovx_ref=fovx_ref, fovy_ref=fovy_ref, $
             files_arr, n_img_arr, dir_top=dir_top, dir_full=dir_full, $
             url_top=url_top, url_full=url_full, _extra=_extra

; Determine time range of all images to construct directory name:
file_times = file2time(reform(files_arr))
n_times = n_elements(file_times)
file_times_sec = anytim(file_times)
ss_sort = sort(file_times_sec)
file_times = file_times[ss_sort]

file_time0 = time2file(file_times[0], /sec)
file_time1 = time2file(file_times[n_times-1], /sec)

if not exist(dir_top) then $
   dir_top = '/sanhome/slater/public_html/data/cruiser'
if not exist(dir_full) then $
   dir_full = concat_dir(dir_top, 'cruiser_' + file_time0 + '__' + file_time1)
if file_exist(dir_full) eq 0 then mk_dir, dir_full

if not exist(file_json) then file_json = 'cruiser.json'
file_json = concat_dir(dir_full, file_json)

;        {
;            "baseUrl" : "http://www.lmsal.com/solarsoft/irisa/data/level2/.../cruiser...",
;
;            "metadata" : {
;                "fov" : {"x" : "123.1", "y" : "82.003"}
;             },
;
;            "data" : [
;
;               {"instrument" : "IRIS", "wavelength" : "1330",
;                "images": [
;                    "sdo_iris_1330_20140201_005701.jpg",
;                    "sdo_iris_1330_20140201_005805.jpg",

if not exist(fovx_ref) then fovx_ref = '100.000'
if not exist(fovy_ref) then fovy_ref = '100.000'

;if not exist(url_top) then url_top = 'http://www.lmsal.com/~slater/cruiser' 
if not exist(url_top) then url_top = 'cruiserdata' 
;url_base = url_cruiser + '/multi_dim_scrolling/' + file_time0 + '_' + file_time1
if not exist(url_full) then $
   url_full = concat_dir(url_top, file_time0 + '_' + file_time1)

; Create header section of json file:
file_append, file_json, '', /new
file_append, file_json, '         {'
file_append, file_json, '             "baseUrl" : "' + url_full + '",'
file_append, file_json, ''

; Create metadata section:
file_append, file_json, '             "metadata" : {'
file_append, file_json, '                 "fov" : {"x" : "' + string(fovx_ref,'$(f7.3)') + '", "y" : "' + $
                                                              string(fovy_ref,'$(f7.3)') + '", "unit" : "arcsec"}'
file_append, file_json, '              },'
file_append, file_json, ''

; Create data section:
file_append, file_json, '             "data" : ['
file_append, file_json, ''

n_waves = n_elements(waves_arr)
for j=0,n_waves-1 do begin
;  { "instrument" : "AIA", "wavelength" : "1400",
   file_append, file_json, '                {"instrument" : "' + strupcase(instr_arr[j]) + '", ' + $
                                            '"wavelength" : "' + waves_arr[j] + '",'
   file_append, file_json, '                 "images": ['

   if j eq 0 then files0 = files_arr[0:(n_img_arr[0]-1)] else $
                  files0 = files_arr[total(n_img_arr[0:(j-1)]):(total(n_img_arr[0:(j)])-1)]

   n_files = n_elements(files0)
   file_times = file2time(files0)

   for i=0,n_files-1 do begin
      if i lt (n_files-1) then $
         file_append, file_json, '                     "' + files0[i] + '",' else $
         file_append, file_json, '                     "' + files0[i] + '"'
   endfor

   file_append, file_json, '                     ]'
   if j lt (n_waves-1) then $
      file_append, file_json, '                },' else $
      file_append, file_json, '                }'
endfor

; Add trailer to json file
file_append, file_json, '             ]'
file_append, file_json, '         }'

end
