
pro ssw_fov_match, index0, data0, index1, data1, t0=t0, $
  file0_sdo=file0_sdo, file1=file1, $
  wave0=wave0, wave1=wave1, x0=x0, y0=y0, nx=nx, ny=ny, $
  data0_fov=data0_fov, data1_fov=data1_fov, $
  do_pause=do_pause, do_display=do_display, do_map=do_map, _extra=_extra

if not exist(wave0) then wave0 = 171
if not exist(wave1) then wave1 = 304

;if not exist(x0) then x0 = 0
;if not exist(y0) then y0 = 0
;if not exist(nx) then nx = index0.naxis1
;if not exist(ny) then ny = index0.naxis2

if ( (not exist(index0)) or (not exist(data0)) or $
     (not exist(index1)) or (not exist(data1)) ) then begin
  if not exist(wave0) then wave0 = 171
  if not exist(wave1) then wave1 = 304
  if not exist(file0_sdo) then file0_sdo = last_file(t0, wave=wave0)
  if not exist(file1) then file1 = last_file(t0, wave=wave1)

  read_sdo, file0_sdo, index0_sdo, data0_sdo
; Read file0_sdo then re-map to synthetic suvi image using aia2suvi.pro:
  aia2suvi, file_sdo=file0_sdo, $
            xcen_suvi=xcen_suvi, ycen_suvi=ycen_suvi, crota2_suvi=crota2_suvi, $
            index_suvi=index0, data_suvi=data0, do_map=do_map, _extra=_extra
  if keyword_set(do_pause) then dum = get_kbrd()
  read_sdo, file1, index1, data1
endif else begin
  wave0 = index0.wavelnth
  wave1 = index1.wavelnth
endelse

; Generate regulation WCS structures from header structures:
wcs0 = fitshead2wcs(index0)
wcs1 = fitshead2wcs(index1)

naxis0_1 = index0.naxis1
naxis0_2 = index0.naxis2
crpix0_1 = index0.crpix1
crpix0_2 = index0.crpix2
cdelt0_1 = index0.cdelt1
cdelt0_2 = index0.cdelt2
crval0_1 = index0.crval1
crval0_2 = index0.crval2

; If [x0,y0,nx,ny] not passed then allow interactive selection of FOV from data0:
if ( (not exist(x0)) or (not exist(y0)) or (not exist(nx)) or (not exist(ny)) ) $
  then begin

  ans = 'Y'
  while strupcase(strmid(ans,0,1)) eq 'Y' do begin

    wdef,0,1024
    tvscl, safe_log10(congrid(data0,1024,1024,interp=interp,cubic=cubic))
    box_cursor, x0_scaled, y0_scaled, nx_scaled, ny_scaled
    unscale_fac = naxis0_1/1024d0
    x0 = fix(x0_scaled*unscale_fac)
    y0 = fix(y0_scaled*unscale_fac)
    nx = fix(nx_scaled*unscale_fac)
    ny = fix(ny_scaled*unscale_fac)

    data0_fov = data0[x0:(x0+nx-1), y0:(y0+ny-1)]

; Define arrays to contain x and y Helioprojective (arcsec) coordinates of all image pixels:
    sol_pix0x = dblarr(nx, ny)
    sol_pix0y = dblarr(nx, ny)

    for i=0,nx-1 do begin
      for j=0,ny-1 do begin
        sol_pix0x[i,j] = (x0 + i - crpix0_1)*wcs0.pc[0,0] + (y0 + j - crpix0_2)*wcs0.pc[0,1]
        sol_pix0y[i,j] = (x0 + i - crpix0_1)*wcs0.pc[1,0] + (y0 + j - crpix0_2)*wcs0.pc[1,1]
      endfor
    endfor

    sol0x = sol_pix0x * cdelt0_1 ; + crval0_1
    sol0y = sol_pix0y * cdelt0_2 ; + crval0_2

; Rotate, scale and translates image0 cutout solar coordinates
;   to image1 pixel coordinates:
    sol1x =  sol0x*cos(index1.crota2/!radeg) + sol0y*sin(index1.crota2/!radeg)
    sol1y = -sol0x*sin(index1.crota2/!radeg) + sol0y*cos(index1.crota2/!radeg)

    pix1x = sol1x/index1.cdelt1 + index1.crpix1 - 1 
    pix1y = sol1y/index1.cdelt2 + index1.crpix2 - 1

; Interpolate data1 to find image2 cutout:
    data1_fov = interpolate(data1, pix1x, pix1y, cubic=-0.5)

    if keyword_set(do_display) then begin
      if ny gt 1024 then begin
        lil_data0_fov = safe_log10(congrid(data0_fov, nx/2, ny/2))
        lil_data1_fov = safe_log10(congrid(data1_fov, nx/2, ny/2))
      endif else begin
        lil_data0_fov = safe_log10(data0_fov)
        lil_data1_fov = safe_log10(data1_fov)
      endelse
      xstepper, [[[lil_data0_fov]],[[lil_data1_fov]]]
    endif

    read, 'Select another fov? (default is no): ', ans
  endwhile

endif else begin

  data0_fov = data0[x0:(x0+nx-1), y0:(y0+ny-1)]

; Define arrays to contain x and y Helioprojective (arcsec) coordinates of all image pixels:
  sol_pix0x = dblarr(nx, ny)
  sol_pix0y = dblarr(nx, ny)

  for i=0,nx-1 do begin
    for j=0,ny-1 do begin
      sol_pix0x[i,j] = (x0 + i - crpix0_1)*wcs0.pc[0,0] + (y0 + j - crpix0_2)*wcs0.pc[0,1]
      sol_pix0y[i,j] = (x0 + i - crpix0_1)*wcs0.pc[1,0] + (y0 + j - crpix0_2)*wcs0.pc[1,1]
    endfor
  endfor

  sol0x = sol_pix0x * cdelt0_1 ; + crval0_1
  sol0y = sol_pix0y * cdelt0_2 ; + crval0_2

; Rotate, scale and translates image0 cutout solar coordinates
;   to image1 pixel coordinates:
  sol1x =  sol0x*cos(index1.crota2/!radeg) + sol0y*sin(index1.crota2/!radeg)
  sol1y = -sol0x*sin(index1.crota2/!radeg) + sol0y*cos(index1.crota2/!radeg)

  pix1x = sol1x/index1.cdelt1 + index1.crpix1 - 1 
  pix1y = sol1y/index1.cdelt2 + index1.crpix2 - 1

; Interpolate data1 to find image2 cutout:
  data1_fov = interpolate(data1, pix1x, pix1y, cubic=-0.5)

  if keyword_set(do_display) then begin
    if ny gt 1024 then begin
      lil_data0_fov = safe_log10(congrid(data0_fov, nx/2, ny/2))
      lil_data1_fov = safe_log10(congrid(data1_fov, nx/2, ny/2))
    endif else begin
      lil_data0_fov = safe_log10(data0_fov)
      lil_data1_fov = safe_log10(data1_fov)
    endelse
    xstepper, [[[lil_data0_fov]],[[lil_data1_fov]]]
  endif

endelse

end
