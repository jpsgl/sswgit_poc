;+
function read_tfile_to_array, file
;
;SUVI function that reads in space delimited items from a text file
;  and produces an array
;
;	Written by: ABK (29-Feb-2012)
;	29-Apr-2013	DSS	Removed extra processing and went with rd_tfile; original processing was giving me problems.
;
;
;-

if not file_exist(file) then begin
	print, 'rd_tfile_to_array: Input file '+strcompress(file,/remove)+' does not exist'
endif

suvi_arry=rd_tfile(file,/autocol,nocomment=';')
return, suvi_arry

end
