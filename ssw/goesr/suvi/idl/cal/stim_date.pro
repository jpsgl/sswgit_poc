function STIM_DATE, time = time

;+
; $Id: stim_date.pro 1 2009-12-02 01:08:32Z linford $
;
; $Log: stim_date.pro,v $
; Revision 1.1  2008/06/05 20:17:19  boerner
; moved repository from CROM to SOLSERV
;
; Revision 1.2  2007/08/24 20:44:46  boerner
; Pre-emi version committed
;
; Revision 1.1  2007/08/17 00:51:12  boerner
; Initial revision spun off from PB_YEARDATE
;
;
;
; Returns an 8-char string giving the current date in YYYYMMDD format
;
; If keyword /time is set, also returns the 6 character time in YYYYMMDD_HHMMSS
;
; WRITTEN: PB 1/15/06 (as PB_YEARDATE)
;
;-

datevect = BIN_DATE(SYSTIME())

if datevect[1] ge 10 then month = STRTRIM(datevect[1], 2) $
	else month = '0' + STRTRIM(datevect[1], 2)

if datevect[2] ge 10 then day = STRTRIM(datevect[2], 2) $
	else day = '0' + STRTRIM(datevect[2], 2)

yeardate = STRTRIM(datevect[0],2) + month + day

if KEYWORD_SET(time) then begin
	if datevect[3] ge 10 then hour = STRTRIM(datevect[3], 2) $
		else hour = '0' + STRTRIM(datevect[3], 2)
	if datevect[4] ge 10 then minute = STRTRIM(datevect[4], 2) $
		else minute = '0' + STRTRIM(datevect[4], 2)
	if datevect[5] ge 10 then second = STRTRIM(datevect[5], 2) $
		else second = '0' + STRTRIM(datevect[5], 2)
	yeardate = yeardate + '_' + hour + minute + second
endif

RETURN, yeardate

end
