
pro sod, waves, rc=rc, gc=gc, bc=bc

;file_171 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194254_00633855.fits'
;file_195 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194854_00633891.fits'
;file_131 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194134_00633847.fits'
;file_094 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194624_00633876.fits'
;file_284 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194324_00633858.fits'
;file_304 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194744_00633884.fits'
;file_171 = '~/data/suvi/20170418_194254_00633855.fits'
;file_195 = '~/data/suvi/20170418_194854_00633891.fits'
;file_131 = '~/data/suvi/20170418_194134_00633847.fits'
file_094 = '~/data/suvi/20170418_194624_00633876.fits'
file_284 = '~/data/suvi/20170418_194324_00633858.fits'
file_304 = '~/data/suvi/20170418_194744_00633884.fits'

;file_131 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194134_00633847.fits'
;file_131 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_195334_00633919.fits'
file_131 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H2100/20170418_212934_00634495.fits'
;file_171 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194254_00633855.fits'
;file_171 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_195454_00633927.fits'
file_171 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H2100/20170418_213054_00634503.fits'
;file_195 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194204_00633850.fits'
;file_195 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_195404_00633922.fits'
file_195 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H2100/20170418_213004_00634498.fits'

files_arr = [file_171, file_195, file_131, file_094, file_284, file_304]

;if not exist(files_arr) then begin
;   dirs_arr = file_search('/archive1/suvi/suvi_data/fm1/l0/2017/04/18','H????')
;   files_arr = file_search(dirs_arr[18:23], '20170418_*.fits')
;endif

;mreadfits_header, files_arr, index_arr

;waves_arr = fix(str_replace(index_arr.info_014, 'A', ''))
waves_suvi = [171, 195, 131, 094, 284, 304]
waves_aia  = [171, 193, 131, 094, 335, 304]

;ss_match_195 = where( (waves_arr eq 195) and $
;                      (index_arr.info_023 eq 'LONG') and $
;                      (index_arr.info_005 eq 'LIGHT'), n_195)
;ss_match_171 = where( (waves_arr eq 171) and $
;                      (index_arr.info_023 eq 'LONG') and $
;                      (index_arr.info_005 eq 'LIGHT'), n_171)
;ss_match_131 = where( (waves_arr eq 131) and $
;                      (index_arr.info_023 eq 'LONG') and $
;                      (index_arr.info_005 eq 'LIGHT'), n_131)

;files_195 = files_arr[ss_match_195]
;files_171 = files_arr[ss_match_171]
;files_131 = files_arr[ss_match_131]
;files_195 = files_195[ss_samp]
;files_171 = files_171[ss_samp]
;files_131 = files_131[ss_samp]
;n_195 = n_elements(files_195)
;n_171 = n_elements(files_171)
;n_131 = n_elements(files_131)

;mreadfits, files_171, index_171, data_171
;mreadfits, files_131, index_131, data_131

;lil_data_131 = safe_log10(rebin(data_131, 1330/2, 1292/2, n_131, /samp))
;xstepper, lil_data_131
;STOP
; Define defaults for sun center position and radius, in pixels:
xpix_cen0 = 641.0
ypix_cen0 = 640.5
rpix0     = 384.75

if not exist(waves) then waves = waves_suvi
n_waves = n_elements(waves)

for i=0,n_waves-1 do begin

   ss_match_wave = (where(waves[i] eq waves_suvi, n_match_wave))[0]
   wave0 = waves_suvi[ss_match_wave]
   file0 = files_arr[ss_match_wave]
   mreadfits, file0, index0, data0, /quiet

   data1 = bytscl(safe_log10(nospike(reform(data0), min=1)))
   data2 = rotate_3d(data1, 6)
;  data2 = data1
   siz_data2 = size(data2)
   xsiz = siz_data2[1]
   ysiz = siz_data2[2]

; First, find the limb for the split scaling:
; (for now, just use hard-coded values for file_195 computed by suvi_limbfit / fit_limb:
;  suvi_limbfit, index0, data0, index1, data1
   xpix_cen0 = 651.0 -2 ; 640.5 ; 689.5 ; 641.0
   ypix_cen0 = 689.5 -2 ; 641.0 ; 651.0 ; 640.5
   rpix0     = 384.75 -3 ; + 4

; Create masks for on disk and off disk:
   ss_on_disk  = cir_mask(data2, xpix_cen0, ypix_cen0, rpix0)
   ss_off_disk = cir_mask(data2, xpix_cen0, ypix_cen0, rpix0, /outside)

; Optionally add timestamp:
   if keyword_set(add_tstamp) then begin
      set_plot, 'z'
      time_string = '18-April-2017' ; anytim(index0.date_obs, /ccsds)
      device, set_resolution = [xsiz, ysiz]
      tvscl, data2
      label = ' GOES-R SUVI Solar Corona ' + string(wave0, '$(i4.4)') + 'A ' + time_string
      xyouts, 10, 75, label, charsize=1.5, color=255, /device, charthick=2
      data2 = tvrd()
      set_plot, 'x'
   endif

; Read canonical color table for AIA wave corresponding to this wave:
   aia_lct, raia, gaia, baia, wavelnth=waves_aia[ss_match_wave]

   again_inside = 'y'
   loadct, 0

   while again_inside eq 'y' do begin

      lil_data2 = rebin(data2, xsiz/2, ysiz/2, /samp)
      xstepper, [[[lil_data2]], [[lil_data2]]]
      tvlct, /get, r0, g0, b0

      rc = raia[r0]
      gc = gaia[g0]
      bc = baia[b0]
      
      rbuff3 = (raia[r0])[data2]
      gbuff3 = (gaia[g0])[data2]
      bbuff3 = (baia[b0])[data2]
      tbuff3 = [[[rbuff3]],[[gbuff3]],[[bbuff3]]]

      file_jpeg = 'wave_' + strtrim(wave0,2) + '_method_ver4' + $
                  time2file(anytim(index0.info_001, /ccsds), /sec) + '.jpg'
      write_jpeg, file_jpeg, tbuff3, quality=85, true=3
      spawn, 'open ' + file_jpeg

      read, "Enter 'y' to repeat, or return to save on disk color table ", again_inside

   endwhile

   r_inside = r0 & g_inside = g0 & b_inside = b0
   rbuff_inside = (raia[r0])[data2]
   gbuff_inside = (gaia[g0])[data2]
   bbuff_inside = (baia[b0])[data2]
   tbuff_inside = [[[rbuff_inside]],[[gbuff_inside]],[[bbuff_inside]]]

   again_outside = 'y'
   loadct, 0

   while again_outside eq 'y' do begin

      lil_data2 = rebin(data2, xsiz/2, ysiz/2, /samp)
      xstepper, [[[lil_data2]], [[lil_data2]]]
      tvlct, /get, r0, g0, b0

      rc = raia[r0]
      gc = gaia[g0]
      bc = baia[b0]
      
      rbuff3 = (raia[r0])[data2]
      gbuff3 = (gaia[g0])[data2]
      bbuff3 = (baia[b0])[data2]
      tbuff3 = [[[rbuff3]],[[gbuff3]],[[bbuff3]]]

      file_jpeg = 'wave_' + strtrim(wave0,2) + '_method_ver4' + $
                  time2file(anytim(index0.info_001, /ccsds), /sec) + '.jpg'
      write_jpeg, file_jpeg, tbuff3, quality=85, true=3
      spawn, 'open ' + file_jpeg

      read, "Enter 'y' to repeat, or return to save off disk color table ", again_outside

   endwhile

   r_outside = r0 & g_outside = g0 & b_outside = b0
   rbuff_outside = (raia[r0])[data2]
   gbuff_outside = (gaia[g0])[data2]
   bbuff_outside = (baia[b0])[data2]
   tbuff_outside = [[[rbuff_outside]],[[gbuff_outside]],[[bbuff_outside]]]

; Use on disk and off disk masks to construct a bi-scaled image:
   rbuff_composite = rbuff_inside
   gbuff_composite = gbuff_inside
   bbuff_composite = bbuff_inside
   rbuff_composite[ss_off_disk] = rbuff_outside[ss_off_disk]
   gbuff_composite[ss_off_disk] = gbuff_outside[ss_off_disk]
   bbuff_composite[ss_off_disk] = bbuff_outside[ss_off_disk]

   tbuff_composite = [[[rbuff_composite]],[[gbuff_composite]],[[bbuff_composite]]]
   file_jpeg = 'wave_' + strtrim(wave0,2) + '_composite_ver0' + $
               time2file(anytim(index0.info_001, /ccsds), /sec) + '.jpg'
   write_jpeg, file_jpeg, tbuff_composite, quality=85, true=3
   spawn, 'open ' + file_jpeg
STOP
   if not exist(rc_arr) then rc_arr = rc else rc_arr = [[rc_arr], [rc]]
   if not exist(gc_arr) then gc_arr = gc else gc_arr = [[gc_arr], [gc]]
   if not exist(bc_arr) then bc_arr = bc else bc_arr = [[bc_arr], [bc]]
   if not exist(t_cube) then t_cube = tbuff3 else t_cube = [[[t_cube]], [[tbuff3]]]

endfor
STOP
end

