
pro suvi_muvi, t0, t1, files_arr=files_arr, files_do_restore=files_do_restore, $
               use_save_file=use_save_file, save_file=save_file, $
               wave_string=wave_string, fw1=fw1, exptime=exptime, verbose=verbose, $
               max_files=max_files, data=data, index=index, $
               do_despike=do_despike, thresh_spike=thresh_spike, do_floor=do_floor, $
               no_rot_flip=no_rot_flip, do_roll_correct=do_roll_correct, $
               do_log=do_log, do_qtr_power=do_qtr_power, $
               do_sigscale=do_sigscale, use_aia_ct=use_aia_ct, do_stretch=do_stretch, $
               use_curr_ct=use_curr_ct, use_grayscale=use_grayscale, $
               do_xstep=do_xstep, do_save_cube=do_save_cube

; Set defaults for image calibration and processing:
  
if not exist(thresh_spike) then thresh_spike = 5

; File search and selection section:

; If images and headers are not passed in, then do file search section:

if ( (not exist(data)) or (not exist(index_match)) ) then begin

   if not exist(wave_string) then wave_string = '171A'
;  if not exist(fw1) then fw1 = 'THIN_AL'
   if not exist(exptime) then exptime = '0.95'
   if not exist(exp_string) then exp_string = 'LONG'
   if not exist(img_type) then img_type = 'LIGHT'

   if not exist(parent) then parent = '/archive1/suvi/suvi_data/fm1/l0/'

; Select files:
   if not exist(files_arr) then begin
      if not keyword_set(files_do_restore) then $
         files_arr = ssw_time2filelist(parent=parent, t0, t1, pflat=pflat) else $
         restore, './files_arr.sav'
   endif
   n_files = n_elements(files_arr)

   mreadfits_header, files_arr, index_arr, only_tags='info_005,info_008,info_014,info_023'
;  mreadfits_header, files_arr, index_arr, only_tags=suvi_isp2index(/defined)

   um = ssw_uniq_modes(index_arr, 'info_005,info_008,info_014,info_023', mc=mc)
;  um = ssw_uniq_modes(index_arr, 'naxis1,naxis2,summode,exptype,imgtype,fw1,fw2,wavelnth', mc=mc)

   if keyword_set(verbose) then prstr, um+string(mc), /nomore

;  ss = struct_where(index_arr, search=['info_014='+wave_string,   'info_008='+fw1, $
;                                   'info_023='+exp_string, 'info_005='+img_type])
;  ss = struct_where(index_arr, search=['exptime>'+exptime, 'wavelnth='+wave_string, 'fw1='+fw1])
;  ss_match = where( (index_arr.info_014 eq wave_string)   and $
;                    (index_arr.info_008 eq fw1)        and $
;                    (index_arr.info_023 eq exp_string) and $
;                    (index_arr.info_005 eq img_type),  n_match)
   ss_match = where( (index_arr.info_014 eq wave_string)   and $
                     (index_arr.info_023 eq exp_string) and $
                     (index_arr.info_005 eq img_type),  n_match)
                                  
   if keyword_set(verbose) then help, files_arr, index_arr, ss_match

   files_arr_match = files_arr[ss_match]
   n_files = n_elements(files_arr_match)

   if exist(max_files) then begin
      if n_files gt max_files then begin
         dt_sec = (anytim(t1) - anytim(t0))/ max_files
         t_grid_sec = anytim(timegrid(t0, t1, seconds=dt_sec))
         t_files_sec = anytim(file2time(files_arr_match))
         ss_sub_samp = tim2dset(anytim(t_files_sec, /ints), anytim(t_grid_sec, /ints))
         files_arr_samp = files_arr_match[ss_sub_samp]
         n_files = n_elements(files_arr_samp)
      endif else begin
         files_arr_samp = files_arr_match
      endelse
   endif else begin
      files_arr_samp = files_arr_match
   endelse

   mreadfits, files_arr_samp, index, data, /quiet

endif

; Now process the images:

ii = index
dd = data

wave = fix(strmid(index[0].info_014, 0, strlen(index[0].info_014)-1))

wave_suvi_arr = [304, 284, 171, 195, 131, 094]
wave_aia_arr  = [304, 131, 171, 193, 131, 094]

if not exist(wave) then wave = '195'
ss_match = where(fix(wave) eq wave_suvi_arr, ss_match)
wave_aia = wave_aia_arr[ss_match[0]]

tt = anytim(ii.info_001, /ccsds)
n_frames = n_elements(index_final)
t0 = tt[0]
t1 = tt[n_frames-1]

movie_name = 'suvi_movie' + '_' + strtrim(wave,2) + $
             '_' + (['nsp', 'des'+string(thresh_spike,'$(i3.3)')])[keyword_set(do_despike)] + $
             '_' + (['ndr', 'der'])[keyword_set(do_roll_correct)] + $
             '_' + (['qtr', 'log'])[keyword_set(do_log)] + $
             '_' + (['col', 'str'])[keyword_set(do_stretch)] + $
             '_' + time2file(t0, /sec) + '_' + time2file(t1, /sec)
movie_dir = concat_dir('/sanhome/slater/public_html/suvi/suvi_movies', $
                       movie_name)
mk_dir, movie_dir
spawn, 'chmod 775 ' + movie_dir

; Optionally save raw index, data:
if keyword_set(do_save_raw) then begin
   save_file = movie_name + '_raw.sav'
   save_dir = '/sanhome/slater/public_html/suvi/save_files'
   save, files_arr_samp, index, data, file=concat_dir(save_dir, save_file)
endif

; Optionally de-spike:
if keyword_set(do_despike) then $
   for i=0,n_frames-1 do $
      dd[0,0,i] = nospike(dd[*,*,i], min=thresh_spike)
;dd = ssw_unspike_cube(ii, dd, threshold=thresh_spike)

; Optionally use median of corner box of 1330 x 1292 x n_frames data cube for image floor:
if keyword_set(do_floor) then begin
   dd_floor = median(dd[52:(52+5),52:(52+5),*])
; Now subtract that from cube:
   dd = dd - dd_floor
endif
STOP
dd_min = min(dd)

; Optionally rotate, transpose:
if not keyword_set(no_rot_flip) then rdd = rotate_3d(dd,6) else rdd = dd

; Optionally remove P angle roll:
if keyword_set(do_roll_correct) then $
   for i=0,n_frames-1 do $
      rdd[0,0,i] = rot(rdd[*,*,i], get_rb0p(tt[i], /pang, /deg, missing=dd_min, /quiet))

; Scale intensity:
if keyword_set(do_log) then sdata = bytscl(safe_log10(rdd), top=250) else $
   if keyword_set(do_qtr_power) then sdata = bytscl((rdd>0)^.25, top=250) else $
      sdata = bytscl(rdd)
;  if keyword_set(do_qtr_power) then sdata = bytscl((rdd+min(rdd))^.25, top=250)

if keyword_set(do_sigscale) then sdata = ssw_sigscale(ii, rdd)

; Define color table:

; Optionally use current display graphics settings:
if keyword_set(use_curr_ct) then tvlct, /get, r, g, b

; Optionally use corresponding AIA color table):
if keyword_set(use_aia_ct) then aia_lct, r, g, b, wave = wave_aia, /load

if fix(wave) eq 284 then begin
   loadct, 1
   tvlct, /get, r, g, b
endif

if keyword_set(use_grayscale) then begin
   loadct, 0
   tvlct, /get, r, g, b
endif

if keyword_set(do_stretch) then begin
   restore, '/Users/slater/soft/idl_temp/suvi/plt/temp/ctab0.sav'
   r = r0 & g = g0 & b = b0
endif

; Optionally make js/mp4:
if keyword_set(do_web) then begin
;  special_movie2, tt, sdata, movie_name=movie_name, $
;     movie_dir=movie_dir, thumbsizde=300, /inctimes, r, g, b
   special_movie2, tt, sdata, movie_name=movie_name, $
      movie_dir=movie_dir, thumbsizde=300, r, g, b, label=''
;  special_movie2, tt, sdata, movie_name=movie_name, movie_dir=movie_dir, $
;     thumbsizde=300, r, g, b, label=strtrim(wave,2) + '  ' + tt  , $
;     labstyle='/ll, size=4, color=50'

; Eliminate filmstrip:
   film_thumbnail_remove, thumbdir=movie_dir
endif

; Optionally display raw and processed movie:
if keyword_set(do_xstep) then begin
   siz_data = size(data)
   siz_sdata = size(sdata)
   ysiz_data = siz_data[2]
   ysiz_sdata = siz_sdata[2]
   buff1 = (bytscl(safe_log10(rotate_3d(data, 6))))[0:511,ysiz_data-512:*,*]
   buff2 = (bytscl(sdata))[0:511,ysiz_sdata-512:*,*]
   buff12 = [[buff1],[buff2]]
   xstepper, buff12
endif

STOP

end
