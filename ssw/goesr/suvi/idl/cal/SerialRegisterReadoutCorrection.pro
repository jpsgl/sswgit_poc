
pro SerialRegisterReadoutCorrection, iindex, idata, oindex, odata

;+
;NAME:   SerialRegisterReadoutCorrection
;PURPOSE:
;        Flip image in case left amp was used
;CALLING SEQUENCE:
;        SerialRegisterReadoutCorrection, iindex, idata, oindex, odata
;INPUTS:
;        idata: Input image array 
;OUTPUTS:
;        odata: Output image array
;               Only change: Flipped in case left amp was readout amp
;KEYWORDS:
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;                   RWN  MAR. 29, 2011
;                   NVN  Feb 2012
;        FEB. 2012  ABK, Update to correlate to SUVI CDRL80
;        MAR. 2013  ABK  Updated to correct CCD readout port definition:
;                        (0=left, 1=right)
;        JUL. 2016  GLS  Updated to correct CCD readout port definition:
;                        (1=left, 2=right), and crrected the flipped/unflipped
;                        cases.
;-

t0=systime(1)
print, '--> 2. Starting SerialRegisterReadoutCorrection.pro (SUVI GPA Routine #2)'

; NOTA BENA: Here we assume:
;   CCD_READOUT_STATUS = 1  ==> CCD register is read out from left amplifier
;                           ==> image reflected above vertical centerline
;   CCD_READOUT_STATUS = 2  ==> CCD register is read out from right amplifier
;                           ==> image not reflected

suv_ceb_rdout_port = iindex.rdoutport
oindex = iindex
odata  = idata

print, 'CCD readout amplifier: ' + (['LEFT', 'RIGHT'])[suv_ceb_rdout_port-1]

IF (suv_ceb_rdout_port EQ 1) THEN BEGIN
  odata = ROTATE(odata,5) 
  print, 'SUVI image FLIPPED for amplifier correction.'
ENDIF else begin
  print, 'SUVI image NOT FLIPPED.'
endelse

t1=systime(1)
print, 'SerialRegisterReadoutCorrection took: ' + $
       string(t1-t0, '(f5.1)'), + ' seconds'

END
