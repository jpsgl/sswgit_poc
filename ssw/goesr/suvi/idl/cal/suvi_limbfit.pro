
pro suvi_limbfit, index0, data0, index1, data1, t0=t0, $
  file0=file0, file1=file1, wave0=wave0, wave1=wave1, $
  do_smooth=do_smooth, do_median=do_median, do_log=do_log, $
  use_find_limb2=use_find_limb2, use_p2sw_fit=use_p2sw_fit, $
  use_fit_limb=use_fit_limb, $
  x0_fov_arr=x0_fov_arr, y0_fov_arr=y0_fov_arr, $
  nx_fov_arr=nx_fov_arr, ny_fov_arr=ny_fov_arr, $
  err_roll=err_roll, err_scale_fac=err_scale_fac, $
  err_cen_x_pix=err_cen_x_pix, err_cen_y_pix=err_cen_y_pix, $
  do_pause=do_pause, do_display=do_display, do_map=do_map, $
  do_shift=do_shift, do_xstep_cube=do_xstep_cube, $
  verbose=verbose, qstop=qstop, _extra=_extra

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     suvi_limbfit
; CATEGORY:
;     Image registration
; PURPOSE:
;     Determine platescale, pointing, and orientation of an image by cross correlation
;     with another image taken at (nearly) the same time for which these parameters are known.
; CALLS:
;     suvi_limbfit, index0, data0, index1, data1, wave=wave
; METHOD:
;     
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     - index0 - Input FITS header structure
;     - data0  - Input image
; OUTPUTS:
;     - index1 - Output FITS header structure (with updated pointing, etc. keywords)
;     - data1  - Output image (optionally registered)
; KEYWORD INPUTS:
;     verbose
;     _extra
; KEYWORD OUTPUTS:
;     wavecorr_struct - structure containing various parameters related to the
; CALLED BY ROUTINES:
; ROUTINES CALLED:
; TRAPPED ERRORS:
;
; UNTRAPPED ERRORS:
;
; NOTA BENA:
;
; TODO:
;     - Handle vectors of images
;     - Caching of reusables (fit coefficients, etc)
; DEVELOPMENT STATUS:
; TEST AND VERIFICATION DESCRIPTION:
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION HISTORY:
; VERSION CONTROL STATUS:
;-
; ==============================================================================

if not keyword_set(do_shift) then do_shift=1

if not exist(wave0) then wave0 = 171
if not exist(wave1) then wave1 = 304

wave_arr      = [094, 131, 171, 193, 211, 304, 335, 4500, 1600, 1700]
do_log_arr    = [  0,   0,   0,   0,   0,   1,   0,    0,    0,    0]
do_smooth_arr = [  0,   0,   0,   0,   0,   1,   0,    0,    0,    0]
do_median_arr = [  0,   0,   0,   0,   0,   1,   0,    0,    0,    0]
ss_wave0 = where(wave_arr eq wave0, n_match)

if not exist(do_log) then do_log = do_log_arr[ss_wave0]
if not exist(do_smooth) then do_smooth = do_smooth_arr[ss_wave0]
if not exist(do_median) then do_median = do_median_arr[ss_wave0]

if not exist(use_fit_limb) then use_fit_limb = 1
if not exist(n_points) then n_points = 40
if not exist(missing) then missing = 0

if ( (not exist(index0)) or (not exist(data0)) or $
     (not exist(index1)) or (not exist(data1)) ) then begin
  if not exist(file0_sdo) then file0_sdo = last_file(t0, wave=wave0)
  if not exist(file1) then file1 = last_file(t0, wave=wave1)

; Read file0_sdo:
  read_sdo, file0_sdo, index0_sdo, data0_sdo

; Re-map to synthetic suvi image using aia2suvi.pro:
  aia2suvi, file_sdo=file0_sdo, t0=t0, $
            xcen_suvi=xcen_suvi, ycen_suvi=ycen_suvi, crota2_suvi=crota2_suvi, $
            index_suvi=index0, data_suvi=data0, do_map=do_map, _extra=_extra
  if keyword_set(do_pause) then dum = get_kbrd()
  read_sdo, file1, index1, data1
endif

; Optionally introduce known roll, plate scale, and alignment errors:
if ( exist(err_roll) or exist(err_scale_fac) or $
     exist(err_cen_x_pix) or exist(err_cen_y_pix) ) then begin
  if not exist(err_roll) then err_roll = 0
  if not exist(err_scale_fac) then err_scale_fac = 1
  if not exist(err_cen_x_pix) then err_cen_x_pix = 0
  if not exist(err_cen_y_pix) then err_cen_y_pix = 0
  cen_x_pix = index0.crpix1 + err_cen_x_pix
  cen_y_pix = index0.crpix2 + err_cen_y_pix
data_orig = data0
  data0 = rot(data0, err_roll, err_scale_fac, cen_x_pix, cen_y_pix, $
              interp=interp, cubic=cubic, missing=missing, pivot=pivot)
endif

wcs0 = fitshead2wcs(index0)
wcs1 = fitshead2wcs(index1)

naxis0_1 = index0.naxis1
naxis0_2 = index0.naxis2
crpix0_1 = index0.crpix1
crpix0_2 = index0.crpix2
cdelt0_1 = index0.cdelt1
cdelt0_2 = index0.cdelt2
crval0_1 = index0.crval1
crval0_2 = index0.crval2

cdelt_suvi = 2.5d0              ; Approximate SUVI plate scale
fov_suvi_ff = 1280d0*cdelt_suvi ; Mean SUVI full frame FOV in arcsec
img_cen_x_suvi_pix = 1280d0/2+0.5
img_cen_y_suvi_pix = 1280d0/2+0.5
r_solar_arcsec_avg = 1992d0/2   ; Mean radius of sun in arcsec
r_solar_arcsec_pix = r_solar_arcsec_avg/cdelt_suvi

wdef, 0, 1280

; Define first guesses for limb fitting routine:
xpix_cen = index0.crpix1
ypix_cen = index0.crpix2
rpix_sec = get_rb0p(index0.date_obs, /radius)
rpix = rpix_sec / index0.cdelt1

; Choose among available generic limb fitting routines:

case 1 of

   keyword_set(use_find_limb2): begin
      find_limb2, data0, xpix_cen, ypix_cen, rpix, r_err, $
                  oblateness, ob_angle, bias, brightness, sig_bright, $
                  sxt=sxt, qtest=qtest
   end

   keyword_set(use_p2sw_fit): begin
      param = [xpix_cen, ypix_cen, rpix]
      type = 'max'
      theta_size = 10000l
      rho_size = 40l
      band = [0.9,1.1]
      roll = 0.0
      buff = p2sw_fit_limb(data0, param, type, theta_size, rho_size, band, $
                           roll=roll, gamma=gamma, maxiter=maxiter, tol=tol, nbcol=nbcol, $
                           sigmaa=sigmaa, show=show, holes=holes)
   end

   keyword_set(use_fit_limb): begin
;     STOP
      data_to_fit = data0
      if not exist(swid) then swid = 5
      if not exist(mwid) then mwid = 5
      if keyword_set(do_smooth) then data_to_fit = smooth(data_to_fit, swid)
      if keyword_set(do_median) then data_to_fit = median(data_to_fit, mwid)
      if keyword_set(do_log) then data_to_fit = safe_log10(data_to_fit)

      if wave0 eq 304 then rloop = 1 else rloop = 2

; Display image:
;     tvscl, safe_log10(data0)
; Define a circle of 361 points using the initial guess center coordinates and raduius, and plot it:
;     r_pix_arr = dblarr(361) + rpix
;     theta_arr = lindgen(361)*!pi/180d0
;     x_pix_arr = r_pix_arr*sin(theta_arr)
;     y_pix_arr = r_pix_arr*cos(theta_arr)
;     plots, (x_pix_arr+xpix_cen), (y_pix_arr+ypix_cen), linestyle=0, /dev      

;     fit_limb, data0, xpix_cen_out, ypix_cen_out, rpix_out, $
;               initial=[xpix_cen, ypix_cen, rpix_sec], pixel_size=index0.cdelt1 ;, $
;     fit_limb, data_to_fit, xpix_cen_out, ypix_cen_out, rpix_out, /norfit, date=index0.date_obs, $
      fit_limb, data_to_fit, xpix_cen_out, ypix_cen_out, rpix_out, $
                initial=[xpix_cen, ypix_cen, rpix_sec], pixel_size=index0.cdelt1, $
                xlimb=xlimb, ylimb=ylimb, n_points=n_points, _extra=_extra ;, $
;     fit_limb, bytscl(data0^.25), xpix_cen_out, ypix_cen_out, rpix_out, $
;               initial=[xpix_cen, ypix_cen, rpix_sec], pixel_size=index0.cdelt1 ;, $
;               r_err, index=index, summed_pixels=summed_pixels, verbose=verbose, $
;               oblateness=oblateness, ob_angle=ob_angle, bias=bias, $
;               interactive=interactive, quiet=quiet, initial_values=initial, $
;               fast=fast, xinitial=hxa_x, yinitial=hxa_y, decompress=decompress, $
;               noiterate=noiterate, iterate=iterate, kill=RLOOP, norfit=norfit, date=date, $
;               noobcorrect=noobcorrect, ellipse=ellipse, resolution=resolution

; Display image:
      tvscl, safe_log10(data0)
; Define a circle of 361 points using the initial guess center coordinates and raduius, and plot it:
      r_pix_arr = dblarr(361) + rpix
      theta_arr = lindgen(361)*!pi/180d0
      x_pix_arr = r_pix_arr*sin(theta_arr)
      y_pix_arr = r_pix_arr*cos(theta_arr)
      plots, (x_pix_arr+xpix_cen), (y_pix_arr+ypix_cen), linestyle=2, /dev      

      plots, xlimb, ylimb, /dev, psym=1, symsiz=1.5

; Define a circle of 361 points using the fitted center coordinates and raduius, and plot it:
      r_pix_arr = dblarr(361) + rpix_out
      theta_arr = lindgen(361)*!pi/180d0
      x_pix_arr = r_pix_arr*sin(theta_arr)
      y_pix_arr = r_pix_arr*cos(theta_arr)
      plots, (x_pix_arr+xpix_cen_out), (y_pix_arr+ypix_cen_out), linestyle=0, /dev      

      x_err_calc = crpix0_1 - xpix_cen_out
      y_err_calc = crpix0_2 - ypix_cen_out
      data2 = rot(data0, 0, 1, xpix_cen_out, ypix_cen_out, $
                  interp=interp, cubic=cubic, missing=missing, pivot=pivot)

      cube0 = [[[transpose(safe_log10(rebin(data_orig, naxis0_1/2, naxis0_2/2, /samp)))]], $
               [[transpose(safe_log10(rebin(data0    , naxis0_1/2, naxis0_2/2, /samp)))]]]
      cube1 = [[[transpose(safe_log10(rebin(data_orig, naxis0_1/2, naxis0_2/2, /samp)))]], $
               [[transpose(safe_log10(rebin(data2    , naxis0_1/2, naxis0_2/2, /samp)))]]]

      xstepper, transpose([[cube0], [cube1]], [1,0,2])
;     xstepper, [[[safe_log10(data_orig)]], [[safe_log10(data2)]]]

   end
   
   keyword_set(use_aia_corr): begin
   
if not exist(nx_fov_arr) then nx_fov_arr = [512, 256, 256, 512, 512]
if not exist(ny_fov_arr) then ny_fov_arr = [512, 512, 512, 256, 256]
n_fov = n_elements(nx_fov_arr)

if not exist(x0_fov_arr) then $
  x0_fov_arr = img_cen_x_suvi_pix + r_solar_arcsec_pix*[0,-1,+1, 0, 0] - nx_fov_arr/2
if not exist(y0_fov_arr) then $
  y0_fov_arr = img_cen_y_suvi_pix + r_solar_arcsec_pix*[0, 0, 0,-1,+1] - ny_fov_arr/2

; Calculate x,y offsets for all FOVs by cross correlation:
for i=0, n_fov-1 do begin

  ssw_fov_match, index0, data0, index1, data1, t0=t0, $
    file0_sdo=file0_sdo, file1=file1, wave0=wave0, wave1=wave1, $
    x0=x0_fov_arr[i], y0=y0_fov_arr[i], nx=nx_fov_arr[i], ny=ny_fov_arr[i], $
    data0_fov=data0_fov, data1_fov=data1_fov, $
    do_pause=do_pause, do_display=do_display, do_map=do_map, _extra=_extra

  fov_cube = [[[data1_fov]], [[data0_fov]]]

  disp = tr_get_disp(fov_cube, mad=mad, nowin=nowin, shift=do_shift, $
                     do_surf=do_surf, peak_str=peak_str, _extra=_extra)

  if keyword_set(verbose) then begin
    print, 'disp[0,1] = ' + strtrim(disp[0,1],2)
    print, 'disp[1,1] = ' + strtrim(disp[1,1],2)
  endif

  if not exist(xdisp_arr) then xdisp_arr = disp[0,1] else $
    xdisp_arr = [xdisp_arr, disp[0,1]]
  if not exist(ydisp_arr) then ydisp_arr = disp[1,1] else $
    ydisp_arr = [ydisp_arr, disp[1,1]]

  if keyword_set(do_xstep_cube) then xstepper, safe_log10(fov_cube)

endfor
  
; From the set of offsets, calculate mean x,y errors, scale error, and roll error:
xdisp_mean = mean(xdisp_arr)
ydisp_mean = mean(ydisp_arr)

scale_fac_x = 1d0 - ((xdisp_arr[2] - xdisp_arr[1]) / $
              ((x0_fov_arr[2] + nx_fov_arr[2]/2) - (x0_fov_arr[1] + nx_fov_arr[1]/2)))
scale_fac_y = 1d0 - ((ydisp_arr[4] - ydisp_arr[3]) / $
              ((y0_fov_arr[4] + ny_fov_arr[4]/2) - (y0_fov_arr[3] + ny_fov_arr[3]/2)))

cdelt0_1 = cdelt0_1*scale_fac_x
cdelt0_2 = cdelt0_2*scale_fac_y

if keyword_set(verbose) then begin
  print, 'scale_fac_x = ' + strtrim(scale_fac_x,2)
  print, 'scale_fac_y = ' + strtrim(scale_fac_y,2)
  print, 'plate scale x = ' + strtrim(cdelt0_1,2)
  print, 'plate scale y = ' + strtrim(cdelt0_2,2)
endif

end
endcase

; Update WCS and other pointing keywords:
oindex = index0 ; TOFIX - define iindex

if keyword_set(qstop) then $
  stop, 'SUVI_LIMBNFIT: Stopping on request before return.'

end

