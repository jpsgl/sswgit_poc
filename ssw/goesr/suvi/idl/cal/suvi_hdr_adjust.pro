
function suvi_hdr_adjust, suvi_index_arr, suvi_case_struct, _extra=_extra

  if not exist(suvi_case_struct) then $
     suvi_case_struct = suvi_gen_case_set()

  tags_case_str = tag_names(suvi_case_struct)
  n_tags_case_str = n_elements(tags_case_str)
  tags_suvi_hdr = tag_names(suvi_index_arr)
  n_tags_suvi_hdr = n_elements(tags_suvi_hdr)

  n_hdr = n_elements(suvi_index_arr)
  n_case = n_elements(suvi_case_struct.(0))
  if n_hdr ne n_case then STOP, 'SUVI_HDR_ADJUST: N_CASE differs from N_HDR.  Stopping.'
  
  for i=0,n_tags_case_str-1 do begin
     ss_hdr = where(tags_case_str[i] eq tags_suvi_hdr, n_match)
     if n_match ne 1 then $
        stop, 'SUVI_HDR_ADJUST: No match to case tag name among suvi hdr tags.  Stopping.'
     for j=0,n_hdr-1 do begin
;       suvi_index_arr[j].(ss_hdr) = suvi_case_struct[j].(i)
        suvi_index_arr[j].(ss_hdr) = (suvi_case_struct.(i))[j]
     endfor
  endfor
STOP
  return, suvi_index_arr
  
  end
