
function brenner, imdex, data

; A macro to determine image focal quality image-wide (not ROI-wide)
; Based on algorithm F-3 "Brenner Gradient"
; In: Sun et al., 2004. MICROSCOPY RESEARCH AND TECHNIQUE 65, 139–149.

; Version: 0.1
; Date: 14/07/2006
; Author: Andy Weller

W = getWidth()
H = getHeight()

; Initialize to 0 ('out of focus')
for j=0, h-1 do begin
   for i=0, w-1 do begin
      p  = getPixel(i,j)
      p1 = getPixel(i+2,j)
; Maximum value is best-focused, decreasing as defocus increases:
      brenner += (p1-p)*(p1-p)
   endfor
endfor

print(brenner)

return, brenner
    
end
