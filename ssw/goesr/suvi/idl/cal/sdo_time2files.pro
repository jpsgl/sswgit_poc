function sdo_time2files, t0, t1, index, _extra=_extra, $
   level=level, waves=waves, aia=aia, hmi=hmi, mag=mag, pre_ops=pre_ops, $
   fast=fast, refresh=refresh, parent=parent, synoptic=synoptic
;
;   Name: sdo_time2files
;
;   Purpose: time range, optionally wavelist -> filelist
;
;   History:
;      22-mar-2010 - S.L.Freeland 
;      30-mar-2010 - hook for 'non standard' aia Level1 path
;       2-apr-2010 - test l1 -> real l1
;      27-apr-2010 - fractional data levels ( 1.5 -> 1p5 )
;      26-apr-2010 - add /FAST switch (future default?)
;      10-dec-2015 - remove ancient/backup check
;      25-feb-2016 - expand time window if nothing found between user time range - return Something...
;-
;
common sdo_time2files_blk, called
common sdo_time2files_blk1, t0c, t1c, parentc, filesc
mag=keyword_set(mag)
hmi=keyword_set(hmi) or mag
aia=1-hmi ; default
inst=(['AIA','HMI'])(hmi)

;parent=get_logenv(inst+'_DATA')
case 1 of 
   data_chk(parent,/string): ; User supplied
   keyword_set(synoptic): parent='/archive/sdo/AIA/synoptic'
   else:
endcase

caches=['/cache/sdo/'+inst,'/net/bigdata/Volumes/cache/sdo/'+inst]
caches=caches[0]
if data_chk(parent,/string) then caches=[parent,caches]  

css=where(file_exist(caches),ccnt)
if ccnt eq 0 then begin
   box_message,'Dont see cache on this machine
   return,''
endif
cache=caches(css(0)) ; pick first
if n_elements(level) eq 0 then level=0
if float(level) eq 1. then level=1 

;if aia and level eq 1 then begin 
;   cache=str_replace(cache,'AIA','test-jps')
;   if n_elements(called) eq 0 then begin 
;      box_message,nbox=2,'Using L1 TEST tree>> ' + cache
;      called=1
;  endif
;endif

retval='' 
if level eq 1 then begin 
   box_message,'Avoiding L1 symlinks, using jsoc...
   cache=str_replace(cache,'cache','archive')
   ssw_jsoc_time2data,t0,t1,index,retval,waves=waves,jsoc2=jsoc2,/files_only
   if ~file_exist(retval[0]) then begin
      box_message,'No SDO data for this time range(!) - expanding time range +/- 1 day, reduced cadence
      time_window,[t0,t1],t0x,t1x,hours=24 ; +/- 1 day
      ssw_jsoc_time2data,t0x,t1x,index,retval,waves=waves,jsoc2=jsoc2,/files_only,cadence='24m' ;
   endif 
   return,retval ; !!! EARLY EXIT due to new paradigm.
endif


slev=(['lev'+strtrim(level,2),'mg'])(mag)
if strlen(slev) gt 4 then begin 
   slev=str_replace(strmid(slev,0,6),'.','p') ; fractional lev= levNpM
endif

eparent=concat_dir(cache,slev)
prefix=''
pre_ops=keyword_set(pre_ops)
comet0='23:00 15-dec-2011'
comet1='01:00 16-dec-2011'

comet=abs(ssw_deltat(t0,comet0,/min)) le 120
comet=0 ; ignore lovejoy window...
case 1 of 
   pre_ops: begin 
      parent='/archive/sdo/AIA/pre_ops/' + slev
   endcase
   comet: begin 
      box_message,'Comet lovejoy window'
      parent='/sanhome/seguin/aia_lev1nrt2/lev1'
   endcase
   file_exist(eparent): parent=eparent
   file_exist(parent): 
   else: begin
      box_message,'Define '+inst+'_DATA'
      return,''
   endcase
endcase
      
if keyword_set(fast) then begin 
   if n_elements(t0c) eq 0 or keyword_set(refresh) then begin ; init common
      t0c=''
      t1c=''
      parentc=''
   endif
   if t0c eq t0 and t1c eq t1 and parentc eq parent then begin 
      retval=filesc ; use common
   endif else begin 
      paths=ssw_time2paths(t0,t1,parent=parent,/hourly)
      retval=''
      for p=0,n_elements(paths)-1 do retval=[temporary(retval),concat_dir(paths(p),findfile(paths(p)))]
      ss=where(strpos(retval,'.fits') ne -1,sscnt)
      if sscnt eq 0 then retval='' else retval=retval(ss)
      retval=strarrcompress(retval)
      if retval(0) ne '' then begin
         retval=ssw_time2filelist(t0,t1,in_files=retval)
      endif
      filesc=retval ; update common
      t0c=t0
      t1c=t1
      parentc=parent
   endelse
endif else begin 
   retval=ssw_time2filelist(t0,t1,parent=parent,/hourly,_extra=_extra)
endelse
if retval(0) ne '' then begin
   nodarks=where(strpos(retval,'d.fits') eq -1,ndcnt)
   if ndcnt gt 0 then retval=retval(nodarks) else retval=''
endif
if retval(0) ne '' and keyword_set(waves) then begin 
   ss=-1
   case 1 of 
      data_chk(waves,/string): if n_elements(waves) eq 1 then swaves=str2arr(waves) else swaves=waves
      else: swaves=strtrim(waves,2)
   endcase
   for w=0,n_elements(swaves)-1 do ss=[temporary(ss),where(strpos(retval,swaves(w)+'.fits') ne -1)]
   sss=where(ss ne -1, count)
   if count eq 0 then begin 
      box_message,'Files in your time range, but none matching your WAVES...'
      retval=''
   endif else retval=retval(ss(sss))
endif

if get_logenv('check') ne '' then stop,'retval'

if retval(0) eq '' then begin
   box_message,'trying jsoc time2data...
   ssw_jsoc_time2data,t0,t1,index,retval,waves=waves,jsoc2=jsoc2,/files_only
endif
   
return,retval
end
