
pro go_suvi_limbfit

; Iteratively select parameters and run suvi_limbfit:

  wave_arr_suvi = ['094', '131', '171', '193', '284', '304']
  wave_arr_sdo  = ['094', '131', '171', '193', '094', '304']
; x_offset_arr = ['00', '10', '20', '30', '40', '50']
; y_offset_arr = ['00', '10', '20', '30', '40', '50']
  x_offset_arr = ['00', '10']
  y_offset_arr = ['00', '10']

  ans = ''
  while ans ne 'n' do begin
     
     ss_wave = xmenu_sel(wave_arr_suvi)

;    ss_x_offset = xmenu_sel(x_offset_arr)
;    ss_y_offset = xmenu_sel(y_offset_arr)

err_cen_x_pix = ''
read, 'Enter x error (pixels): ', err_cen_x_pix
err_cen_y_pix = ''
read, 'Enter y error (pixels): ', err_cen_y_pix

;    suvi_limbfit, t0='14-apr-2016 05:00', wave0=fix(wave_arr_sdo(ss_wave)), $
;                  err_cen_x_pix=fix(x_offset_arr(ss_x_offset)), $
;     		   err_cen_y_pix=fix(y_offset_arr(ss_y_offset)), $
;		   n_points=20

     suvi_limbfit, t0='14-apr-2016 05:00', wave0=fix(wave_arr_sdo(ss_wave)), $
                   err_cen_x_pix=fix(err_cen_x_pix), $
		   err_cen_y_pix=fix(err_cen_y_pix), $
                   n_points=20

;     case ss_routine of
;        0: go_mk_suvi
;        1: go_suvi_check_image
;        2: go_suvi_limb_fit
;        3: go_suvi_ltc
;        4: go_suvi_flatfield
;        5: go_suvi_make_badpix
;        6: go_suvi_make_dark
;     endcase

     read, 'Select another wave? (def is yes) : ', ans

  endwhile

end
