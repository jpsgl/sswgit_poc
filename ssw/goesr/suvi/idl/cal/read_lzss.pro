;+ 
;
; Prototype program to read SUVI data from LZSS archive. The data are stored as netCDF-4 files.
; 
; PURPOSE:
;    To extract individual packets from LZSS netCDF files and write
;    them to apid packet files. Apid index files are also updated.
; 
; CATEGORY:
;	
;
; CALLING SEQUENCE:
;	read_lzss
;
; INPUTS:
;	fn: List of files
;
; OPTIONAL INPUTS:
;	None
;	
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;	APID packet and index files 
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;	None
;
; SIDE EFFECTS:
;       APID packet files created
;       A large number of files may be open at any one time	
;
; RESTRICTIONS:
;       The only netCDF-4 format is supported.	
;
; PROCEDURE:
;	Create a list of all netCDF files in $LZSS_DIR/netCDF_files.
;       For each file, get dimesions, # variable and their attributes.
;       Get data for each packet variable.
;       Determine, packet time, apid and write out the packet to an
;       apid packet file (along with a index record).
; 
;
; EXAMPLE:
;	
;
; MODIFICATION HISTORY:
; 	Written by: Dnyanesh Mathur
;                   24-Jun-2015
;-
 
pro read_lzss, fn

; Initialize variables
packet_info = {PACKET_INFO_STR, seq_number:0l, julday:0l, lun:0l, pos_p:0ll, pos_i:0ll} ; holds relevant output information for apid packets
packet_info_buf = replicate (packet_info, 128)                    ; There are 128 possible SUVI apids
packet_info_buf(*).seq_number = -1
packet_info_buf(*).julday = -1
packet_info_buf(*).lun = -1
packet_info_buf(*).pos_p = -1
packet_info_buf(*).pos_i = -1


next_lun = 99               ; First lun to use; luns are used from 99 downwards (100-127 are used by IDL)
FIRST_APID = 768            ; SUVI apid range is 768 - 895 (although fewer than 40 are used)
packet_data = bytarr(8192)  ; Latgest SUVI packet is 8192 bytes in size
index_rec = bytarr(26)      ; All index records are 26 bytes in size (See page 131 of 
                            ; Maintenance, Test & Operations Manual for ESTE - CDRL 84 (SUVP-RP-09-1210)

pos = 0ll                   ; file position 

error_count = 0             ; Initialize error counter


lzss_file_dir = getenv ('LZSS_DIR')       ; Parent directory of LZSS netCDF files
packet_file_dir = getenv ('PACKET_DIR')   ; Parent directory of packet files (sorted by apids)

print, lzss_file_dir
print, packet_file_dir

; Get a list of input files. 
fn = file_search (strjoin ([lzss_file_dir, "/netCDF_files/*.nc"]))
print, fn
; print, packet_file_dir


; Determine number of files to process 
num_files = 1
fn_info = size (fn )
if (fn_info(0) ne 0) then num_files = fn_info(1)
print, "# files = ", num_files
read, "Press 1 to continue, 0 to abort: ", yy
if (yy eq 0) then stop

; Loop over the number of files; Extract each packet and output it to
; an appropriate packet file and write out the corresponding index
; record.

for file_count = 0, num_files - 1 do begin
  nc_id = ncdf_open(fn(file_count), /NOWRITE)

  nc_info = ncdf_inquire( nc_id )
  print, file_count, format='("file num = ", i5, $)' ; "  ", nc_info

  ; Read and print all global attribute values
  ; for i = 0, nc_info.ngatts-1 do begin 
  for i = 14, 15 do begin
    name=ncdf_attname(nc_id,i,/global) 
    ncdf_attget, nc_id, name, val,/global
    print, name, string(val), format='(a20," : ",a, $)'
  endfor

  ncdf_diminq,nc_id,0,dim0_name,dim0_size   ; dim0_size contains the number of packets in the file
  print, dim0_name, dim0_size, format='("   ", a20, ":", i8, $)'

  ncdf_diminq,nc_id,1,dim1_name,dim1_size   ; dim1_size contains the size of the file in bytes
  ; print,dim1_name, dim1_size
  print, dim1_name, dim1_size, format='("   ", a20, ":", i12, $)'
  print, " "

  ncdf_varget, nc_id, 0, packet_sizes
  ncdf_varget, nc_id, 1, packet_offsets
  ncdf_varget, nc_id, 2, packet_buffer

  ; packet_sizes and packet_offsets are arrays of length dim0_size
  ; packet_buffer is a 1-d array of size, number_of_data_bytes

  ; Determine the directory in which to place the current packet. The directory is determined by Apid. If this is the first packet for
  ; a given apid, a new apid packet file has to be opened. If a file is already open, only the record number needs to be advanced.
  ; Record number 0 indicates that no apid packet file is currently open. Set up an array to manage writing of packet data to apid packet file
  ; in appropriate directory. The array is indexed by apid_index and holds the Logical Unit Number (LUN) associated with the output files
  ; for the apid. 

  ; Print out apid and size for each packet in file
  for i=0, (dim0_size - 1) do begin
    apid = (packet_buffer(packet_offsets(i))*256l + packet_buffer(packet_offsets(i)+1)) and '7FF'x               ; 11 bit APID mask
    packet_data(0:packet_sizes(i)-1) = packet_buffer(packet_offsets(i):packet_offsets(i)+packet_sizes(i)-1)      ; Copy packet data from nctCDF file
    seq_num = (packet_data(2)*256l + packet_data(3)) and '3fff'x
    pkt_len = packet_data(4)*256l + packet_data(5)
    ;print, i, apid, packet_sizes(i), seq_num, pkt_len, packet_data(6:15), format = '("Packet #: ", i5, ": Apid: ", z04, "  Size = ", i6, " Bytes", " Seq_Num = ", i7, " Len = ", i5, 10i4)'
    index_rec(16:25) = packet_data(6:15)

    ; Check if sequence number has the expected value
    apid_index = apid - FIRST_APID  ; FIRST_APID is element 0 in lun_list
    expected_seq_num = (packet_info_buf(apid_index).seq_number + 1) and '3fff'x
    if ((expected_seq_num ne 0) and (seq_num ne expected_seq_num)) then begin  ; throw error if unexpected sequence number (except for 1st packet)
      print, "*** Error *** Unexpected sequence number; Expected ", seq_numbers(apid_index)+1, ", Received ", packet_seq_num
      error_count = error_count + 1
    endif 
    packet_info_buf(apid_index).seq_number = seq_num

    packet_time_ds2k = 0ll
    packet_time_msod = 0ll
    packet_time_usec = 0ll
    packet_unix_time_sec = 0l
    packet_unix_time_usec = 0l

    packet_time_ds2k = packet_data(6) * 65536ll + packet_data(7) * 256ll + packet_data(8)
    packet_time_msod = packet_data(9) * 16777216ll + packet_data(10) * 65536ll + packet_data(11) * 256ll + packet_data(12)
    packet_data_usec = packet_data(13) * 256ll + packet_data(14)
    packet_unix_time_sec = ulong(946684800l + 43200 + packet_time_ds2k * 86400l + (packet_time_msod / 1000l)) ; unix time at 1/1/2000 == 946684800 + 12 hrs
    packet_unix_time_usec = ulong((packet_time_msod mod 1000ll) * 1000ll + packet_data_usec)
    
    index_rec(0:3) = byte(swap_endian(2l), 0, 4)  ; Set file type
    index_rec(4:7) = byte (swap_endian(packet_unix_time_sec), 0, 4) ; Set unix seconds
    index_rec(8:11) = byte (swap_endian(packet_unix_time_usec), 0, 4) ; Set unix micro-seconds
    index_rec(12:15) = 0
    index_rec(16:25) = packet_data(6:15)

    ; Write out packet, including the index record
    ;   Determine if an packet file for the apid and date is open. 
    ;   If no packet file for the apid is open, construct file
    ;   file name from apid and day number. Otherwise check if 
    ;   the file is for correct date. If not, close the currently 
    ;   open file and construct new file name and open it.

    jul_days = packet_data(6) * 65536l + packet_data(7) * 256l + packet_data(8) + julday(1,1,2000) + 1  ; Add 1 because day # in secondary header start at 0
    
    if (packet_info_buf(apid_index).julday ne jul_days) then begin
      packet_info_buf(apid_index).julday = jul_days
      if (packet_info_buf(apid_index).lun eq -1) then begin   ; File does not exist, 
        packet_info_buf(apid_index).lun = next_lun            ;_so assign it the next available lun
      endif else begin                                        ; if files are open but the day number does not match, close them
        close, packet_info_buf(apid_index).lun
        close, packet_info_buf(apid_index).lun - 1
      endelse
        
      ; Construct file name from the time of packet
      caldat, jul_days, mon, dy, yr
      apid_pkt_fn = string (apid, yr, mon, dy, apid, format = '("/Users/mathur/SUVI/log/packets/0x", z04, "/", i4,2i02, ".0x", z04)')  ; apid is used both for directory and extension
      apid_index_fn = strjoin ([apid_pkt_fn, "x"])
      openw, packet_info_buf(apid_index).lun, apid_pkt_fn  ;, /APPEND
      packet_info_buf(apid_index).pos_p = 0
      openw, packet_info_buf(apid_index).lun - 1, apid_index_fn  ;, /APPEND  ; Use next lun for index file
      packet_info_buf(apid_index).pos_i = 0
      next_lun = next_lun - 2  ; Used up 2 luns for packet and index files
   endif  

    writeu, packet_info_buf(apid_index).lun, packet_data(0:packet_sizes(i)-1)
    point_lun, - packet_info_buf(apid_index).lun, pos
    if (pos ne (packet_info_buf(apid_index).pos_p + (packet_sizes(i)))) then begin
      print,"Error in writing packet", " Apid=", apid, "old pos = ", packet_info_buf(apid_index).pos_p, "size=", packet_sizes(i), "pos=", pos
      error_count = error_count + 1
    endif
    packet_info_buf(apid_index).pos_p = pos
    ; wait, 0.1
    writeu, packet_info_buf(apid_index).lun - 1, index_rec  ; index file lun is, packet_file_lun - 1
    point_lun, - (packet_info_buf(apid_index).lun - 1), pos
    if (pos ne packet_info_buf(apid_index).pos_i + 26ll) then begin
      print,"Error in writing packet", " Apid=", apid, "old pos = ", packet_info_buf(apid_index).pos_i, "size=", 26, "pos=", pos
      error_count = error_count + 1
    endif
    packet_info_buf(apid_index).pos_i = pos
  endfor
  
  ncdf_close, nc_id

endfor

for i = 0, 127 do begin
  if (packet_info_buf(i).lun ne -1) then begin
    print, i + FIRST_APID, i + FIRST_APID, packet_info_buf(i).pos_p, packet_info_buf(i).pos_i, format='("apid= ", i5, " (0x", z04, ")", "  # pos_p = ", i12, "  pos_i = ", i12)' 
    flush, packet_info_buf(i).lun
    wait, 0.2
    close, packet_info_buf(i).lun
    flush, packet_info_buf(i).lun - 1
    wait, 0.2
    close, packet_info_buf(i).lun - 1
    packet_info_buf(i).lun = -1
  endif
endfor

print, "Number of errors = ", error_count
end
