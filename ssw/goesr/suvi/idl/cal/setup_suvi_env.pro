
pro setup_suvi_env, parent_soft=parent_soft, parent_data=parent_data, fm=fm, $
                    remove_old=remove_old, reset_parent=reset_parent, _extra=_extra

;suvi
;	suvi_fm1
;		suvi_fm1_flatdb
;			packets
;			l0_fits
;			l0a_fits
;			l0b_fits
;		suvi_fm1_flats

;TODO:
;SSW_SUVI
;SUVI_DATA
;SUVI_DOC

if not exist(parent_soft) then parent_soft = concat_dir(get_logenv('SSW'), 'suvi')
if not exist(parent_data) then parent_data = '/archive1/suvi'
if not exist(parent_docs) then parent_docs = '/archive1/suvi'
if not exist(fm) then fm = 1

if ( (get_logenv('SUVI_SOFT') eq '') or keyword_set(reset_parent) ) then $
   set_logenv, 'SUVI_SOFT', concat_dir(parent_soft, 'suvi_soft')

if ( (get_logenv('SUVI_DATA') eq '') or keyword_set(reset_parent) ) then $
   set_logenv, 'SUVI_DATA', concat_dir(parent_data, 'suvi_data')

if ( (get_logenv('SUVI_DOCS') eq '') or keyword_set(reset_parent) ) then $
   set_logenv, 'SUVI_DOCS', concat_dir(parent_docs, 'suvi_docs')

set_logenv, 'SUVI_FM',            concat_dir(get_logenv('SUVI_DATA'), 'fm' + string(fm, '(i1.1)'))
set_logenv, 'SUVI_CAL_DATA',      concat_dir(get_logenv('SUVI_FM'), 'cal_data')
set_logenv, 'SUVI_EXTERNAL_DATA', get_logenv('SUVI_CAL_DATA')
set_logenv, 'SUVI_TLM',           concat_dir(get_logenv('SUVI_FM'), 'tlm')
set_logenv, 'SUVI_PACKETS',	  concat_dir(get_logenv('SUVI_TLM'), 'packets')
set_logenv, 'SUVI_L0',            concat_dir(get_logenv('SUVI_FM'), 'l0')
set_logenv, 'SUVI_L1',            concat_dir(get_logenv('SUVI_FM'), 'l1')
set_logenv, 'SUVI_L1B',           concat_dir(get_logenv('SUVI_FM'), 'l1b')

if not exist(dir_tlm)	  then dir_tlm	   = get_logenv('SUVI_TLM')
if not exist(dir_packets) then dir_packets = get_logenv('SUVI_PACKETS')
if not exist(dir_l0)	  then dir_l0  	   = get_logenv('SUVI_L0')
if not exist(dir_l1)	  then dir_l1      = get_logenv('SUVI_L1')
if not exist(dir_l1b)	  then dir_l1b     = get_logenv('SUVI_L1B')

set_logenv, 'SUVI_FM',            concat_dir(get_logenv('SUVI_DATA'),   'fm' + string(fm, '(i1.1)'))
set_logenv, 'SUVI_FLATS',         concat_dir(get_logenv('SUVI_FM'),     'suvi_flats')
set_logenv, 'SUVI_FLATDB',        concat_dir(get_logenv('SUVI_FM'),     'suvi_flatdb')
set_logenv, 'SUVI_FLAT_PACKETS',  concat_dir(get_logenv('SUVI_FLATDB'), 'packets')
set_logenv, 'SUVI_FLAT_L0_FITS',  concat_dir(get_logenv('SUVI_FLATDB'), 'l0_fits')
set_logenv, 'SUVI_FLAT_L0a_FITS', concat_dir(get_logenv('SUVI_FLATDB'), 'l0a_fits')
set_logenv, 'SUVI_FLAT_L0b_FITS', concat_dir(get_logenv('SUVI_FLATDB'), 'l0b_fits')
set_logenv, 'SUVI_DARKS',         concat_dir(get_logenv('SUVI_FM'),     'suvi_darks')
set_logenv, 'SUVI_DARKDB',        concat_dir(get_logenv('SUVI_FM'),     'suvi_darkdb')
set_logenv, 'SUVI_BADPIX',        concat_dir(get_logenv('SUVI_FM'),     'suvi_badpixels')
set_logenv, 'SUVI_BADCOL',        concat_dir(get_logenv('SUVI_FM'),     'suvi_badcolumns')
set_logenv, 'SUVI_GAIN',          concat_dir(get_logenv('SUVI_FM'),     'suvi_gain')
set_logenv, 'SUVI_GAINDB',        concat_dir(get_logenv('SUVI_FM'),     'suvi_gaindb')
set_logenv, 'SUVI_GAIN_CAL_FITS', concat_dir(get_logenv('SUVI_GAINDB'), 'gain_cal_fits')

if not file_exist(get_logenv('SUVI_DATA'))          then mk_dir, get_logenv('SUVI_DATA')
if not file_exist(get_logenv('SUVI_FM'))            then mk_dir, get_logenv('SUVI_FM')
if not file_exist(get_logenv('SUVI_FLATS'))         then mk_dir, get_logenv('SUVI_FLATS')
if not file_exist(get_logenv('SUVI_FLATDB'))        then mk_dir, get_logenv('SUVI_FLATDB')
if not file_exist(get_logenv('SUVI_FLAT_PACKETS'))  then mk_dir, get_logenv('SUVI_FLAT_PACKETS')
if not file_exist(get_logenv('SUVI_FLAT_L0_FITS'))  then mk_dir, get_logenv('SUVI_FLAT_L0_FITS')
if not file_exist(get_logenv('SUVI_FLAT_L0a_FITS')) then mk_dir, get_logenv('SUVI_FLAT_L0a_FITS')
if not file_exist(get_logenv('SUVI_FLAT_L0b_FITS')) then mk_dir, get_logenv('SUVI_FLAT_L0b_FITS')
if not file_exist(get_logenv('SUVI_DARKS'))         then mk_dir, get_logenv('SUVI_DARKS')
if not file_exist(get_logenv('SUVI_DARKDB'))        then mk_dir, get_logenv('SUVI_DARKDB')
if not file_exist(get_logenv('SUVI_BADPIX'))        then mk_dir, get_logenv('SUVI_BADPIX')
if not file_exist(get_logenv('SUVI_BADCOL'))        then mk_dir, get_logenv('SUVI_BADCOL')
if not file_exist(get_logenv('SUVI_GAIN'))          then mk_dir, get_logenv('SUVI_GAIN')
if not file_exist(get_logenv('SUVI_GAINDB'))        then mk_dir, get_logenv('GAINDB')
if not file_exist(get_logenv('SUVI_GAIN_CAL_FITS')) then mk_dir, get_logenv('SUVI_GAIN_CAL_FITS')

if not file_exist(get_logenv('SUVI_REFDB'))         then mk_dir, get_logenv('SUVI_REFDB')

end
