
pro go_mk_suvi_fits, t0, use_oberon=use_oberon, use_titan=use_titan

; set_logenv, 'GPA_PATH', '$HOME/soft/idl_temp/suvi/plt/temp'
; ssw_path, get_logenv('GPA_PATH'), /prepend
  ssw_path, get_logenv('SSW_SUVI'), /prepend

use_oberon = 1

if keyword_set(use_oberon) then $
   dir_pid = '/archive1/suvi/suvi_data/fm1/tlm/packet_ids/0x032a' else $
   dir_pid = '/titan/goes/suvi_backup/fm1/tlm/packet_ids/0x032a'

; Example packet file name: '20170716.0x032a'
file_packet = concat_dir(dir_pid, time2file(t0, /date) + '.0x032a')

if file_exist(file_packet) eq 1 then $
   mk_suvi_fits, file_packet, do_prep=do_prep, /do_write ; do_write=do_write

end

