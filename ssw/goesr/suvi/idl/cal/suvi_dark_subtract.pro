;+
pro SubtractProcessedDark, iindex, idata, oindex, odata, $
  exposure_threshold=exposure_threshold, $
  temperature_threshold=temperature_threshold, day_threshold=day_threshold

;NAME:  suvi_dark_subtract
;PURPOSE: 
;       Select recent dark images on the basis of the
;       exposure time and CCD temperature (and readout_status),
;       and subtract the pixel by pixel median of these dark images
;       from the SUVI science image.
;CALLIG SEQUENCE:
;INPUTS:
;OUTPUTS: 
;KEYWORDS:
;PROCEDURE CALLS:
;MODIFICATION HISTORY:
;       2015-12-08 - GLS - Total re-write of SubtractProcessedDark.pro
;-

t0=systime(1)

if not exist(t_off_max)    then t_off_max    = 3600   ; Maximum acceptable time offset in seconds
if not exist(temp_off_max) then temp_off_max =    2   ; Maximum acceptable temperature offset in degrees
if not exist(exp_factor)   then exp_factor   =    0.1 ; Max fractional offset in expoisure duration

; Get necessary tags from image header:
t_img          = anytim(iindex.img_pkt_time, /ccsds)
t_img_sec      = anytim(t_img)
ccd_temp_img   = iindex.ccdtemp1
rdout_port_img = iindex.rdoutport
expdur_img     = iindex.cmd_exp_time

; Determine latest bakeout prior to image time:
if iindex.time_last_bakeout ne '' then $
  t_bake_last = anytim(iindex.time_last_bakeout, /ccsds) else $
  t_bake_last = anytim('01-jan-2000', /ccsds) ; If no bakeouts

; Read headers of all darks:
ddir = concat_dir(get_logenv('SUVI_CAL_DATA'), '/dark_v2')
dinfil = file_list(ddir,'*.fits')
mreadfits_header, dinfil, dindex

; Sort darks by time:
t_dark          = anytim(dindex.date_obs, /ccsds)
t_dark_sec      = anytim(t_dark)
ss_sort_dark = sort(t_dark_sec)
dinfil = dinfil[ss_sort_dark]
dindex = dindex[ss_sort_dark]
t_dark          = anytim(dindex.date_obs, /ccsds)
t_dark_sec      = anytim(t_dark)

; Get necessary tags from dark headers:
ccd_temp_dark   = dindex.ccdtemp
;rdout_port_dark = (['left', 'right'])[dindex.amp]
rdout_port_dark = dindex.amp + 1
expdur_dark     = dindex.exptime

; Filter darks for acceptable set:
ss_good = where( (rdout_port_dark eq rdout_port_img) and $
                 (expdur_dark ge (1d0-exp_factor)*expdur_img) and $
                 (expdur_dark le (1d0+exp_factor)*expdur_img) and $
                 (ccd_temp_dark ge (ccd_temp_img - temp_off_max)) and $
                 (ccd_temp_dark le (ccd_temp_img + temp_off_max)), n_good)
dinfil = dinfil[ss_good]
dindex = dindex[ss_good]
t_dark          = anytim(dindex.date_obs, /ccsds)
t_dark_sec      = anytim(t_dark)

; Select all darks within normal time window, or, if none within time window,
; simply select nearest one:
t_off_abs = abs(t_dark_sec - t_img_sec)
ss_close = where(t_off_abs le t_off_max, n_close)
if n_close eq 0 then begin
  t_off_min = min(t_off_abs, ss_close)
  n_close = 1
endif

dinfil_select = dinfil[ss_close]
dindex_select = dindex[ss_close]

dark_names = file_break(dinfil_select) ; Remove path

mreadfits, dinfil_select, dindex_select, ddata_select, /quiet
ndark = n_elements(dindex_select)
if ndark gt 1 then darkimage = reform(median(ddata_select, dimension=3)) else $
 		   darkimage = reform(ddata_select)

odata = idata - darkimage

; Add addtional header tags:
oindex = add_tag(oindex, n_elements(DARK_NAMES), 'NUMDARKS')
for j=0, n_elements(DARK_NAMES)-1 DO $
  indexp = add_tag(indexp, DARK_NAMES[j], 'DARKFN_' + string(j, '(i2.2)'))

out_arr = ''
out_arr = [out_arr, 'SUVI image time: ' + t_img]
out_arr = [out_arr, 'SUVI dark files: ']
out_arr = [out_arr, dinfil_select]
out_arr = [out_arr, '']
prstr, out_arr
;if keyword_set(do_log) then $
;  file_append, file_log, out_arr

t1=systime(1)
print, 'SubtractProcessedDark took: ', t1-t0, ' seconds'

end


