;________________________________________________________________________________________________________________________
;
; p2sw_polar_circle_limb : retourne la distance entre l'origine de la transformation polaire et un point du limbe repere 
;                     par son angle, connaissant le rayon du disque solaire et les coordonnees de son centre.
;________________________________________________________________________________________________________________________
;
; Appel : p2sw_polar_circle_limb, x, a, f
;
; Parametres : x : valeur pour de theta en degres pour laquelle on desire connaitre la distance. 
;              a : tableau de trois elements contenant les parametres dont depend le profil : 
;                  delta_x : abscisse du centre du disque solaire dans le repere de la transformation.
;                  delta_y : ordonnee du centre du dsique solqire dqns le repere de la transformation.     
;                  radius : rayon du disque solaire. 
;              f : resultat : distance entre l'origine de la transformation et le point du limbe considere. 
;________________________________________________________________________________________________________________________
;
; Remarque : pour tenir compte de la discretisation : 
;              dummy = a(0)*sin(x) + a(1)*cos(x) + sqrt(a(2)^2 - (a(0)*cos(x) - a(1)*sin(x))^2) 
;              x0 = a(0)+round(dummy*cos(x)-a(0))
;              y0 = a(1)+round(dummy*sin(x)-a(1))
;              f = sqrt(x0^2 + y0^2)
;            mais ne fonctionne pas...
;________________________________________________________________________________________________________________________
;
; Historique : 02/07/1997
;              23/09/2010 : Added analytic partial derivatives (dbs)
;________________________________________________________________________________________________________________________
  
pro p2sw_polar_circle_limb, x, a, f, pder

  f = a(0)*sin(x) + a(1)*cos(x) + sqrt(a(2)^2 - (a(0)*cos(x) - a(1)*sin(x))^2) 

  IF N_PARAMS() GE 4 THEN $
    pder = [ [sin(x) - (cos(x)*(a(0)*cos(x) - a(1)*sin(x)))/(a(2)^2 - (a(0)*cos(x) - a(1)*sin(x))^2)^0.5], $
             [cos(x) + (sin(x)*(a(0)*cos(x) - a(1)*sin(x)))/(a(2)^2 - (a(0)*cos(x) - a(1)*sin(x))^2)^0.5], $
             [a(2)/(a(2)^2 - (a(0)*cos(x) - a(1)*sin(x))^2)^0.5] ]



end
