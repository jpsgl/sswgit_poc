
pro go_mk_suvi, fm=fm, ss_input_type=ss_input_type, ss_img=ss_img, $
                interactive=interactive, pktfil=pktfil, $
                iindex_arr=iindex_arr, idata_arr=idata_arr, $
                do_img_switch=do_img_switch, do_check_image=do_check_image, $
                do_prep=do_prep, do_l1b=do_l1b, stages=stages, _extra=_extra

  if not exist(ss_input_type) then interactive = 1

  
  if (not exist(iindex_arr) or not exist(idata_arr) ) then begin


; Define directory paths:
  if not exist(fm) then fm = '1' 
  setup_suvi_env, fm=fm, _extra=_extra

  img_size_bytes = 3465216l

  img_source_arr = ['Packet File', 'L0 FITS File']
  if not exist(ss_input_type) then ss_input_type = $
     xmenu_sel(img_source_arr, /fixed, size_font=20, tit='SUVI PLT Image Source Menu')

  case ss_input_type of
     0: begin

        do_rd_packet = 1
;do_l1b = 0
;       files = file_list(get_logenv('SUVI_PACKETS'), '*.0x032a')
;       filnams = file_break(files)
;       ss_fil = xmenu_sel(filnams)
;       pktfil = files(ss_fil)
;       f_info = file_info(pktfil)
;       siz_file = f_info.size
;       n_img_packet = siz_file/img_size_bytes

        ans0 = 'y'
;       if keyword_set(interactive) then begin
;          print, ''
;          print, 'There appear to be ' + string(n_img_packet, '(i3.3)') + ' images in this packet.'
;          read, 'Select image number or hit <cr> to return to main menu: ', ans0
;          ss_img = fix(ans0)
;       endif
        
while strmid(strlowcase(ans0),0,1) ne 'n' do begin
        
        mk_suvi_fits, pktfil, ss_img=ss_img, $
                      /do_index_arr, iindex_arr, /do_data_arr, idata_arr, $
                      this_img_name=this_img_name, do_prep=do_prep, interactive=interactive, _extra=_extra

;       if keyword_set(interactive) then begin
;          print, ''
;          read, 'Select another image number or hit <cr> to return to main menu (1 to ' + string(n_img_packet, '(i3.3)') + ') : ', ans0
;          ss_img = fix(ans0)
;       endif

        print, ''
        read, 'Select another image? (default is yes): ', ans0

endwhile

     endcase

     1: begin
        do_rd_packet = 0
        files = file_list(get_logenv('SUVI_L0'), '*.fits')
        filnams = file_break(files)
        ss_fil = xmenu_sel(filnams)

        suvi_mreadfits, files[ss_fil], iindex_arr, idata_arr

     endcase

  end


endif

; Optionally call suvi_check_image:
if keyword_set(do_check_image) then begin

           index_ref = iindex_arr[0]
           data_ref = reform(idata_arr[*,*,0])
           wave_ref =  strtrim(index_ref.asname,2)
           file_ref = 'suvi_ref_image_' + wave_ref
           print, 'wave = ' + wave_ref
           ans1 = ''
           read, 'Write ref image? (default no) : ', ans1
           if strlowcase(strmid(ans1,0,1)) eq 'y' then begin
;             cmd = "save, index_ref_" + wave_ref + ", data_ref_" + wave_ref + $
              cmd = "save, index_ref, data_ref, " + $
                    "file = '" + concat_dir(get_logenv('SUVI_REFFDB'), file_ref) + "'"
              result = execute(cmd)
           endif

           suvi_check_image, iindex_arr[0], reform(idata_arr[*,*,0]), $
              /use_syn, /ref_refresh, last_refresh=last_refresh, $
              do_display=do_display, _extra=_extra, qstop=qstop
           
endif   

; Optionally call suvi_prep and create L1b (index, data) from L1 (index,data):
  if keyword_set(do_l1b) then begin

     stages = str2arr('readout_corr bad_pix bad_col dark flat reg radiance',' ')
     ss_stages = xmenu_sel(stages)
     if ( (where(stages[ss_stages] eq 'readout_corr'))[0] eq -1) then no_readout_correction = 1 else $
        no_readout_correction = 0
     if ( (where(stages[ss_stages] eq 'bad_pix'))[0] eq -1) then no_bad_pix = 1 else $
        no_bad_pix = 0
     if ( (where(stages[ss_stages] eq 'bad_col'))[0] eq -1) then no_bad_col  = 1 else $
        no_bad_col  = 0
     if ( (where(stages[ss_stages] eq 'dark'))[0] eq -1) then no_dark = 1 else $
        no_dark = 0
     if ( (where(stages[ss_stages] eq 'flat'))[0] eq -1) then no_flat = 1 else $
        no_flat = 0
     if ( (where(stages[ss_stages] eq 'reg'))[0] eq -1) then no_reg = 1 else $
        no_reg = 0
     if ( (where(stages[ss_stages] eq 'radiance'))[0] eq -1) then no_convert_to_rad = 1 else $
        no_convert_to_rad = 0

     comments = [ $
  '-------------------------------------------------------------------------------', $
  ' Begin GPA routines for image file ', $ ; + 
  '-------------------------------------------------------------------------------' ]
     prstr, comments

     if keyword_set(do_img_switch) then begin
        files_switch = file_list(concat_dir(get_logenv('SUVI_DATA'), 'l0_switch'), '*.fits')
        filnams_switch = file_break(files_switch)
        ss_switch = xmenu_sel(filnams_switch)

        mreadfits, files_switch[ss_switch], index_dum, data_dum
;STOP
        idata_arr = data_dum
     endif

; SUVI_L1b_Processing, data, datap
     suvi_prep, iindex_arr[0], reform(idata_arr[*,*,0]), indexp, datap, this_img_name=this_img_name, $
                no_readout_correction=no_readout_correction, $
                no_bad_pix=no_bad_pix, $
                no_bad_col=no_bad_col, $
                no_dark=no_dark, $
                no_flat=no_flat, $
                no_reg=no_reg, $
                no_convert_to_rad=no_convert_to_rad, $
                do_display=do_display, do_write=do_write, verbose=verbose, $
                _extra=_extra

  endif
     
end




