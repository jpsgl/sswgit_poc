;+
;	18-Apr-2013	DSS	Moved paths from hardcoded to env based
;	28-Apr-2013	DSS	updated common blocks
;
;
;
;
pro setup_suvi_gpa

;set_logenv,'SUVI_EXTERNAL_DATA', getenv('HOME')+'/suvi/GPDS/external_data'
;set_logenv, 'SUVI_DATA_L1B', getenv('HOME')+'/suvi/GPDS/test_images/L1b'

if get_logenv('SUVI_EXTERNAL_DATA') eq '' then set_logenv, 'SUVI_EXTERNAL_DATA', '~/external_data'
if get_logenv('SUVI_DATA_L1B') eq '' then set_logenv, 'SUVI_DATA_L1B', '~/test_images/L1b'
print, get_logenv('SUVI_EXTERNAL_DATA')
print, get_logenv('SUVI_DATA_L1B')

COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT, STATIC_PARAMS, MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, FOCAL_PLANE_FILTER_TRANSMISSION, SUVI_LINEARITY, SUVI_GAIN_RIGHT, SUVI_GAIN_LEFT
COMMON OUT_METADATA, DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, PHOT_ENG_CONV, ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, SN_LEV3, FULL_WELL, SHT_EXP_TIME, IMG_PKT_TIME 

SUVI_BAD_PIXELS=read_bad_pixels()
SUVI_BAD_CCD_COLUMNS=read_bad_columns()
SUVI_FLAT=set_suvi_flat_files()
STATIC_PARAMS=read_tfile_to_struct('suvi_static.txt')
MEASURED_PARAMS=read_tfile_to_struct('suvi_measured_params.txt')
EPHEMERIS=read_tfile_to_struct('suvi_sim_ephemeris.txt')
ENTRANCE_TRANS=read_tfile_to_struct('suvi_ent_filter_trans.txt')
FOCAL_PLANE_FILTER_TRANSMISSION=read_tfile_to_array('suvi_fw_filter_trans.txt')
SUVI_LINEARITY_SOURCE='suvi_linearity.txt'
SUVI_LINEARITY=read_tfile_to_array(SUVI_LINEARITY_SOURCE)
SUVI_GAIN_RIGHT=read_tfile_to_struct('suvi_gain_right_amp.txt')
SUVI_GAIN_LEFT=read_tfile_to_struct('suvi_gain_left_amp.txt')
;-
end
