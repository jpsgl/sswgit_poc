
pro mk_suvi_movie, t0, t1, wave=wave, n_samp=n_samp, $
                   use_save_file=use_save_file, do_roll_correct=do_roll_correct, $
                   do_despike=do_despike, thresh_spike=thresh_spike, do_log=do_log, $
                   do_sigscale=do_sigscale, do_stretch=do_stretch, do_grayscale=do_grayscale, $
                   do_save_cube=do_save_cube

wave_suvi_arr = [304, 284, 171, 195, 131, 094]
wave_aia_arr  = [304, 131, 171, 193, 131, 094]

if not exist(wave) then wave = '195'
ss_match = where(fix(wave) eq wave_suvi_arr, ss_match)
wave_aia = wave_aia_arr[ss_match[0]]

if not exist(thresh_spike) then thresh_spike = 5

t0_sec = anytim(t0)
t1_sec = anytim(t1)
t_dur_sec = t1_sec - t0_sec

if not exist(n_samp) then n_samp = 100
if not exist(minutes_samp) then minutes_samp = fix(t_dur_sec/n_samp/60)

if not keyword_set(use_save_file) then begin

   if not exist(n_hours_back) then n_hours_back = 6

   if not exist(t0) then t0 = anytim('12-feb-2017', /ccsds)
   if not exist(t1) then t1 = anytim((anytim(ut_time())-n_hours_back*3600l), /ccsds)

   if not exist(wave) then wave = '195'
   if not exist(expdur) then expdur = 1

   if not exist(n_chunk) then n_chunk = 1000

   if not exist(files) then begin
;     files = file_search('/sanhome/shing/FM1/l0/2017/??', $
;                         '*_' + string(wave,'$(i3.3)') + '_light_*.fits')
;     files = file_search('/archive1/suvi/suvi_data/fm1/l0/2017/??', $
;                         '*_' + string(wave,'$(i3.3)') + '_light_*.fits')
      t0_ext = anytim(t0, /ext)
      files = file_search('/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H????', $
                          '*_' + string(wave,'$(i3.3)') + '_light_*.fits')
   endif
   
   n_files = n_elements(files)
   print, ' Total files of wave ' + string(wave,'$(i3.3)') + ' found: ' + $
          strtrim(n_files, 2)

   filnams = file_break(files)                                                         
   t_filnam = strmid(filnams, 22, 15)
   t_files = anytim(file2time(t_filnam), /ccsds)
STOP
t_sec = anytim(t_files)
ss_good = where( ( (t_sec ge anytim(t0)) and (t_sec le anytim(t1)) ), n_good)
t_sec = t_sec[ss_good]
files = files[ss_good]
filnams = filnams[ss_good]
t_filnam = t_filnam[ss_good]
t_files = t_files[ss_good]

ss_sort = sort(t_sec)
t_sec = t_sec[ss_sort]
files = files[ss_sort]
filnams = filnams[ss_sort]
t_filnam = t_filnam[ss_sort]
t_files = t_files[ss_sort]

   t_files_int = anytim(t_files, /ints)
   ss_match_t0 = tim2dset(t_files_int, anytim(t0, /ints))
   ss_match_t1 = tim2dset(t_files_int, anytim(t1, /ints))

   files = files[ss_match_t0:ss_match_t1]
   n_files = n_elements(files)
   n_chunks = ceil(double(n_files)/n_chunk)

   for i=0, n_chunks-1 do begin
      mreadfits_header, files[(i*n_chunk):(((i+1)*n_chunk-1)<(n_files-1))], index0
      if i eq 0 then index_arr = index0 else $
         index_arr = concat_struct(index_arr, index0)
      print, 'Header read of ' + strtrim(n_chunk,2) + ' files done.'
   endfor
   
   index = index_arr

   ss_good_exp = where(index.info_003 eq 1, n_good_exp)
   files = files[ss_good_exp]
   index = index[ss_good_exp]

; 24G    /titan/goes/suvi/GOES-16/packets/0x032a/20170212.0x032a
; 28G    /titan/goes/suvi/GOES-16/packets/0x032a/20170213.0x032a
; 28G    /titan/goes/suvi/GOES-16/packets/0x032a/20170214.0x032a
; 28G    /titan/goes/suvi/GOES-16/packets/0x032a/20170215.0x032a
; 28G    /titan/goes/suvi/GOES-16/packets/0x032a/20170216.0x032a

; 23G    /archive1/suvi/suvi_data/fm1/l0/2017/02/12
; 28G    /archive1/suvi/suvi_data/fm1/l0/2017/02/13
; 28G    /archive1/suvi/suvi_data/fm1/l0/2017/02/14
; 28G    /archive1/suvi/suvi_data/fm1/l0/2017/02/15
; 27G    /archive1/suvi/suvi_data/fm1/l0/2017/02/16

   t_grid = timegrid(t0, t1, minutes=minutes_samp)
   t_suvi = anytim(index.info_001, /ccsds)

   fmt_timer, t_suvi

   t_int = anytim(t_suvi, /ints)
   ss_match = tim2dset(t_int, anytim(t_grid, /ints))
   t_samp = t_suvi[ss_match]

   files_samp = files[ss_match]
   index_samp = index[ss_match]

   mreadfits, files_samp, index_final, data_final

endif else begin

   restore, '/Users/slater/soft/idl_temp/suvi/plt/temp/final.sav'

endelse

ii = index_final
dd = data_final

tt = anytim(ii.info_001, /ccsds)
n_frames = n_elements(index_final)

movie_name = 'suvi_movie' + '_' + strtrim(wave,2) + $
             '_' + (['nsp', 'des'+string(thresh_spike,'$(i3.3)')])[keyword_set(do_despike)] + $
             '_' + (['ndr', 'der'])[keyword_set(do_roll_correct)] + $
             '_' + (['qtr', 'log'])[keyword_set(do_log)] + $
             '_' + (['col', 'str'])[keyword_set(do_stretch)] + $
             '_' + time2file(t0, /sec) + '_' + time2file(t1, /sec)
movie_dir = concat_dir('/sanhome/slater/public_html/suvi/suvi_movies', $
                       movie_name)
mk_dir, movie_dir
spawn, 'chmod 775 ' + movie_dir

if keyword_set(do_save_cube) then begin
   save_file = movie_name + '.sav'
   save_dir = '/sanhome/slater/public_html/suvi/save_files'
   save, files_samp, index_final, data_final, file=concat_dir(save_dir, save_file)
endif
   
; Rotate / flip images:
;for i=0, n_frames-1 do begin
;   if i eq 0 then data_cube = rotate(reform(data_final[*,*,i]), 6) else $
;      data_cube = [[[data_cube]], [[rotate(reform(data_final[*,*,i]), 6)]]]
;endfor

; De-spike:
if keyword_set(do_despike) then $
   for i=0,n_frames-1 do $
      dd[0,0,i] = nospike(dd[*,*,i], min=thresh_spike)
;  dd = ssw_unspike_cube(ii, dd, threshold=thresh_spike)

; Calculate median of corner box of 1330 x 1292 x n_frames data cube for image floor:
dd_floor = median(dd[52:(52+5),52:(52+5),*])
; Now subtract that from cube:
dd = dd - dd_floor
dd_min = min(dd)

; Rotate, transpose:
rdd = rotate_3d(dd,6)

; Remove P angle roll:
if keyword_set(do_roll_correct) then $
   for i=0,n_frames-1 do $
      rdd[0,0,i] = rot(rdd[*,*,i], get_rb0p(tt[i], /pang, /deg, missing=dd_min, /quiet))

; Scale intensity:
if keyword_set(do_log) then $
   sdata = bytscl(safe_log10(rdd), top=250) else $
      sdata = bytscl((rdd>0)^.25, top=250)
;     sdata = bytscl((rdd+min(rdd))^.25, top=250)
if keyword_set(do_sigscale) then $
   sdata = ssw_sigscale(ii, rdd) else $

; Get color table:
aia_lct, r, g, b, wave = wave_aia

if fix(wave) eq 284 then begin
   loadct, 1
   tvlct, /get, r, g, b
endif

if keyword_set(do_grayscale) then begin
   loadct, 0
   tvlct, /get, r, g, b
endif

if keyword_set(do_stretch) then begin
   restore, '/Users/slater/soft/idl_temp/suvi/plt/temp/ctab0.sav'
   r = r0 & g = g0 & b = b0
endif

; Make js/mp4:
;special_movie2, tt, sdata, movie_name=movie_name, $
;   movie_dir=movie_dir, thumbsizde=300, /inctimes, r, g, b
 special_movie2, tt, sdata, movie_name=movie_name, $
    movie_dir=movie_dir, thumbsizde=300, r, g, b, label=''
;special_movie2, tt, sdata, movie_name=movie_name, movie_dir=movie_dir, $
;   thumbsizde=300, r, g, b, label=strtrim(wave,2) + '  ' + tt  , $
;   labstyle='/ll, size=4, color=50'

; Eliminate filmstrip:
film_thumbnail_remove, thumbdir=movie_dir

end

