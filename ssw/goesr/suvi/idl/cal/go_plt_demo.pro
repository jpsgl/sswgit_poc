
pro go_plt_demo, ss_input_type=ss_input_type, ss_img=ss_img, interactive=interactive, $
                 iindex_arr=iindex_arr, idata_arr=idata_arr, $
                 do_img_switch=do_img_switch, $
                 do_prep=do_prep, do_l1b=do_l1b, stages=stages, _extra=_extra

;  wdelete
;  wdelete

  if not exist(ss_input_type) then interactive = 1
  if not exist(fm) then fm = 1

; Define directory paths:
  setup_suvi_env, fm=fm, _extra=_extra
  
  img_size_bytes = 3465216l

; Iteratively select PLT routine to run:
  routine_arr = ['suvi_mfits', 'suvi_check_image/filter_check', 'suvi_limbfit', 'suvi_platescale', $
                 'suvi_get_disp', 'suvi_align_image', 'suvi_flatfield', $
                 'suvi_make_dark', 'suvi_make_badpix', 'suvi_despike', 'suvi_ltc', $
                 'suvi_prep', 'suvi_sharpness']
;                'suvi_prep', 'suvi_sharpness', 'suvi_filter_check']
  ans = ''
  while strlowcase(strmid(ans,0,1)) ne 'n' do begin

;     wdelete
;     wdelete

     !p.multi = 0
     ss_routine = xmenu_sel(routine_arr, /fixed, size_font=20, tit='SUVI PLT Demo Main Menu')

     case ss_routine of
         0: go_mk_suvi, /do_index_arr, /do_data_arr, iindex_arr=iindex_arr, $
                        idata_arr=idata_arr, /do_write, _extra=_extra
         1: suvi_check_image, wave=wave, /interactive, /use_syn, $
                              /ref_refresh, /last_refresh, /do_display, $
                              ratio_threshold=ratio_threshold, _extra=_extra
         2: go_suvi_limbfit
         3: go_suvi_limbfit
         4: go_suvi_limbfit
         5: go_suvi_limbfit
         6: suvi_flatfield
         7: suvi_make_dark
         8: buff = suvi_make_badpix(/do_write)
         9: go_markus
        10: buff = suvi_ltc()
        11: go_mk_suvi, iindex_arr=iindex_arr, idata_arr=idata_arr, /do_prep, /do_l1b, $
                       /do_display, _extra=_extra ; , /no_dark
        12: buff = suvi_sharpness(/use_syn)
        else: return
     endcase
;       1: go_suvi_check_image, do_check_image=do_check_image, _extra=_extra
;       1: go_mk_suvi, iindex_arr=iindex_arr, idata_arr=idata_arr, /do_check_image, $
;                      /do_display, _extra=_extra ; , /no_dark
;       13: suvi_filter_check

     read, 'Select another routine? (def is yes) : ', ans

  endwhile



  return



  if not exist(ss_input_type) then ss_input_type = xmenu_sel(['Packet File','L1 File'])

  case ss_input_type of
     0: begin
        do_rd_packet = 1
        files = file_list(concat_dir(get_logenv('SUVI_DATA'), 'tlm'), '*.0x032a')
        filnams = file_break(files)
        ss_fil = xmenu_sel(filnams)
        pktfil = files(ss_fil)
        f_info = file_info(pktfil)
        siz_file = f_info.size
        n_img_packet = siz_file/img_size_bytes

        if keyword_set(interactive) then begin
           print, 'There appear to be ' + string(n_img_packet, '(i2.2)') + $
                  ' images in this packet.'
           ans = ''
           read, 'Select image number to process: ', ans
           ss_img = fix(ans)
        endif

        mk_suvi_fits, pktfil, ss_img=ss_img, $
                      /do_index_arr, iindex_arr, /do_data_arr, idata_arr, $
                      this_img_name=this_img_name, _extra=_extra

     endcase

     1: begin
        do_rd_packet = 0
        files = file_list(concat_dir(get_logenv('SUVI_DATA'), 'l0'), '*.fits')
        filnams = file_break(files)
        ss_fil = xmenu_sel(filnams)

        mreadfits, files[ss_fil], iindex_arr, idata_arr

     endcase

  end

; Optionally call suvi_prep and create L1b (index, data) from L1 (index,data):
  if keyword_set(do_l1b) then begin

     stages = str2arr('readout_corr bad_pix bad_col dark flat reg radiance',' ')
     ss_stages = xmenu_sel(stages)
     if ( (where(stages[ss_stages] eq 'readout_corr'))[0] eq -1) then $
        no_readout_correction = 1 else no_readout_correction = 0
     if ( (where(stages[ss_stages] eq 'bad_pix'))[0] eq -1) then no_bad_pix = 1 else $
        no_bad_pix = 0
     if ( (where(stages[ss_stages] eq 'bad_col'))[0] eq -1) then no_bad_col  = 1 else $
        no_bad_col  = 0
     if ( (where(stages[ss_stages] eq 'dark'))[0] eq -1) then no_dark = 1 else $
        no_dark = 0
     if ( (where(stages[ss_stages] eq 'flat'))[0] eq -1) then no_flat = 1 else $
        no_flat = 0
     if ( (where(stages[ss_stages] eq 'reg'))[0] eq -1) then no_reg = 1 else $
        no_reg = 0
     if ( (where(stages[ss_stages] eq 'radiance'))[0] eq -1) then $
        no_convert_to_rad = 1 else no_convert_to_rad = 0

     comments = [ $
  '-------------------------------------------------------------------------------', $
  ' Begin GPA routines for image file ', $ ; + 
  '-------------------------------------------------------------------------------' ]
     prstr, comments

     if keyword_set(do_img_switch) then begin
        files_switch = file_list(concat_dir(get_logenv('SUVI_DATA'), 'l0_switch'), '*.fits')
        filnams_switch = file_break(files_switch)
        ss_switch = xmenu_sel(filnams_switch)

        mreadfits, files_switch[ss_switch], index_dum, data_dum
;STOP
        idata_arr = data_dum
     endif

; SUVI_L1b_Processing, data, datap
     suvi_prep, iindex_arr[0], reform(idata_arr[*,*,0]), indexp, datap, $
                this_img_name=this_img_name, $
                no_readout_correction=no_readout_correction, $
                no_bad_pix=no_bad_pix, $
                no_bad_col=no_bad_col, $
                no_dark=no_dark, $
                no_flat=no_flat, $
                no_reg=no_reg, $
                no_convert_to_rad=no_convert_to_rad, $
                do_display=do_display, do_write=do_write, verbose=verbose, $
                _extra=_extra

  endif
     
end




