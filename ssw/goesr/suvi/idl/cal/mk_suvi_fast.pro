
pro mk_suvi_fast, pktfile, $
                  verbose=verbose, qdebug=qdebug, _extra=_extra

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     MK_SUVI_FAST
; CALLS:
;     mk_suvi_fast, pktfile
; METHOD:
; INPUTS:
;     pktfile - SUVI packet file
; OUTPUTS:
;     SUVI L0 FITS file(s)
; NOTA BENA:
;     - Note that the 16-bit words are swapped. Examples:
;       - Packet Flag   is at i16[3] instead of i16[2].
;       - Packet Number is at i16[5] instead of i16[4].
; TODO:
; HISTORY:
; 2017-02-14 - GLS
; VERSION CONTROL STATUS:
;-
; ==============================================================================

; Define misc parameters:
qswap = is_member(/swap_os)

; Define SUVI environmentals and directory paths:
;setup_suvi_env, fm=fm, _extra=_extra

; Define default packet file if not passed:
if not exist(pktfile) then $
   pktfile = '/titan/goes/suvi/GOES-16/packets/0x032a/20170213.0x032a'

; Define packet structure constants:
img_size_bytes = 1330l*1292l*2
hdr_words = 6+14+24
hdr_bytes = hdr_words*2
data_words = 4074
data_bytes = data_words*2
n_packets_per_img = long(img_size_bytes / data_bytes)

f_info = file_info(pktfile)
siz_file = f_info.size
n_img_in_file = siz_file / double(img_size_bytes)

; Calculate the starting packet number (record) if n_img_jump is passed:
if exist(n_img_jump) then recnum = long(n_img_jump[i_ss_img])*n_packets_per_img

; Define image packet structure:
pkt_struct = {science_packet, ccsds_prime:bytarr(6), ccsds_sec:bytarr(14), $
              data_head:bytarr(24), data:bytarr(data_bytes)}

; Open packet file for reading:
openr, lun, pktfile, /get_lun

; Define ASSOC variable to contain one complete image plus headers:
;img_buff = assoc(lun, bytarr(hdr_bytes+data_bytes, n_packets_per_img))
img_buff = assoc(lun, intarr(hdr_words+data_words, n_packets_per_img))

; Loop through images in the packet file, skipping the first image, which is
; actually the ISP header:
for i=1, n_img_in_file-1 do begin

   img_buff0 = img_buff[i]
; Extract sub-array which contains the image:
   img = img_buff0[hdr_words:*,*]
STOP
   delvarx, img_buff0, img

endfor

free_lun, lun

end
