;+
function suvi_get_dark, iindex, idata, $
  exposure_threshold=exposure_threshold, $
  temperature_threshold=temperature_threshold, day_threshold=day_threshold

;
;NAME:  suvi_get_dark
;PURPOSE: 
;       Select 10 most recent dark images on the basis of the
;       exposure time and CCD temperature (and readout_status),
;       and subtract the composite dark image (median of the 
;       collected dark images) from the SUVI science image.
;CALLIG SEQUENCE:
;       SUVI_IMG = SubtractProcessedDark(SUVI_IMG)
;INPUTS:
;       SUVI_IMG - SUVI raw image
;OUTPUTS: 
;       dinfil_final
;KEYWORDS:
;PROCEDURE CALLS:
;MODIFICATION HISTORY:
;	31-Jan-2013 AK  Modified code to match what is stated in CDL80 for
;	determining temperature threshold (+/- 2 degrees)
;	30-Apr-2013 DSS	traded pixel-by-pixel median calculation for the
;                       array operation with dimension option in median function
;       27-jan-2016 GLS Broke out 'suvi_get_dark' from 'SubtractProcessedDark'
;-

t0=systime(1)
print, '--> 5a. Starting get_suvi_dark.pro (SUVI GPA Routine #5a)'

if n_elements(temperature_threshold) ne 1 then teth = 2.0 else $
                                               teth = temperature_threshold
if n_elements(day_threshold)         ne 1 then dth  = 7   else $
                                               dth  = day_threshold

; Get necessary image info from header:
time               = anytim(iindex.img_pkt_time, /ccsds)
SUVI_CCD_TEMP      =        iindex.ccdtemp1
CCD_READOUT_STATUS =        iindex.rdoutport
EXP_TIME           =        iindex.cmd_exp_time

if iindex.time_last_bakeout ne '' then $
  TBAKE = anytim(iindex.time_last_bakeout, /ccsds) else $
  TBAKE = anytim('01-jan-2000', /ccsds) ; If no bakeouts

if keyword_set(do_read_time) then begin
  print, 'Original image time = ', time
  ans = ''
  read, 'enter image time: ', ans
  time  = anytim(ans, /ccsds)
  print, 'Kluged image time = ', anytim(time, /ccsds)
endif

tstart = anytim(reltime(time, days=-dth), /ccsds) ; normally 7 days before the image

IF n_elements(exposure_threshold) NE 1 THEN exth=(0.1*EXP_TIME) ELSE exth=exposure_threshold

buff = ['Image stats:', $
        '  time           = ' + anytim(time, /ccsd), $
        '  exptime        = ' + strtrim(EXP_TIME), $
        '  ccd_temp       = ' + strtrim(SUVI_CCD_TEMP), $
        '  readout_status = ' + strtrim(CCD_READOUT_STATUS), $
        '  last bakeout   = ' + anytim(tbake, /ccsd)]
if keyword_set(verbose) then prstr, buff
if keyword_set(do_runlog) then $
  file_append, concat_dir(get_logenv('SUVI_LOGS'), 'gpa.log')

dir = '$SUVI_CAL_DATA'+'/dark'
dinfil = file_list(dir,'*.fits')
mreadfits, dinfil, dindex, /nodata 
if tag_exist(dindex, 'date_obs') then $
  dtime = anytim(dindex.date_obs, /ccsds) else $
  dtime = anytim(dindex.t_obs, /ccsds)
ss_sort = sort(anytim(dtime))
dinfil = dinfil[ss_sort]
dindex = dindex[ss_sort]
dtime = dtime[ss_sort]
dinfil0 = dinfil
dindex0 = dindex
dtime0 = dtime

; Select all darks taken between tstart and image time:
sss = sel_timrange(anytim(dtime, /yoh), anytim(tstart, /yoh), anytim(time, /yoh), /bet)
if sss[0] ne -1 then begin
	dinfil=dinfil[sss]
	dindex=dindex[sss]
	dtime=dtime[sss]
        smatch = where((dindex.exptime ge (EXP_TIME-exth)) and $
                       (dindex.exptime le (exth+EXP_TIME)) and $
                       (abs(dindex.ccdtemp) ge (abs(SUVI_CCD_TEMP)-teth)) and $
                       (abs(dindex.ccdtemp) le (abs(SUVI_CCD_TEMP)+teth)) and $
                       (dindex.amp eq CCD_READOUT_STATUS), nmatch)

	if smatch[0] ne -1 then begin
		dinfil=dinfil[smatch]
		dindex=dindex[smatch]
		dtime=dtime[smatch]
; make sure everything is after last bakeout
		delt = int2secarr(dtime,tbake)  
                safter = where(delt gt 0, nafter)  
		if safter[0] ne -1 then begin
			ndark=min([10, nafter])
			sfinal=sss[smatch[safter[nafter-1-ndark+1:nafter-1]]]
			dinfil_final=dinfil0[sfinal]
; Must relax the condition of "after last bakeout". Instead use nearest earlier
                endif else begin
			sfinal=sss[smatch[nmatch-1]]
			dinfil_final=dinfil0[sfinal]
		endelse
		goto, found
	endif else begin
		print, 'Darks exist between ',anytim(tstart,/ccsd),' and ',anytim(time,/ccsd)
                print, 'But conditions do not match. ' + $
                       'So we need to expand the time range to pick up one dark frame.'
	endelse
endif else print, 'SubtractProcessedDarks: image time is out of range for dark library'

; Search unconstrained period
print,'Search unconstrained period.'
sss=where(int2secarr(dtime0, time) lt 0)  ; assume darks exist before image
dinfil=dinfil0[sss]
dindex=dindex0[sss]
dtime=dtime0[sss]
smatch=where(EXP_TIME ge (dindex.exptime-exth) and $
             EXP_TIME le (exth+dindex.exptime) and $
             abs(SUVI_CCD_TEMP) ge (abs(dindex.ccdtemp)-teth) and $
             abs(SUVI_CCD_TEMP) le (abs(dindex.ccdtemp)+teth) and $
             CCD_READOUT_STATUS eq (dindex.amp+1), nmatch)

if smatch[0] ne -1 then begin
; Take the last one
	dinfil_final=dinfil[smatch[nmatch-1]]
	goto, found
endif else begin
        print, '     WARNING:  No dark matching all constraints.  Using latest dark.'
        dinfil_final=dinfil[n_elements(dinfil)-1]
        goto, found
endelse

found:

print, '     +++Selected Darks:'
print, '          ', dinfil_final

return, dinfil_final

end

