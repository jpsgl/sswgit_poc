
pro go_mk_suvi_movie

;pro mk_suvi_movie, t0, t1, wave=wave, $
;                   use_save_file=use_save_file, do_roll_correct=do_roll_correct, $
;                   do_despike=do_despike, thresh_spike=thresh_spike, do_log=do_log, $
;                   do_stretch=do_stretch, do_grayscale=do_grayscale, $
;                   do_save_cube=do_save_cube



; February time interval:
t0 = '12-feb-2017'
t1 = '16-feb-2017 20:00'

; Six waves, color, de_rolled, log-scaled, no_stretch:

;mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log, n_samp=400
;mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct,          n_samp=400
 mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log, n_samp=400
;mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log, n_samp=400
;mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log, n_samp=400
;mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log, n_samp=400

STOP

; January time interval:
t0 = '29-jan-2017'
t1 = '30-jan-2017'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log, n_samp=400
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct,          n_samp=400
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log, n_samp=400
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log, n_samp=400
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log, n_samp=400
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log, n_samp=400

STOP

; January time interval:
t0 = '29-jan-2017'
t1 = '30-jan-2017'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log

STOP

; January time interval:
t0 = '29-jan-2017'
t1 = '30-jan-2017'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log, n_samp=125
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct,          n_samp=125
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log, n_samp=125
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log, n_samp=125
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log, n_samp=125
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log, n_samp=125

STOP

; January time interval:
t0 = '29-jan-2017'
t1 = '30-jan-2017'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log

STOP

; February time interval:
t0 = '18-feb-2017'
t1 = '23-feb-2017'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_save_cube

STOP

; February time interval:
t0 = '12-feb-2017'
t1 = '16-feb-2017 20:00'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_save_cube

STOP

; January time interval:
t0 = '29-jan-2017'
t1 = '30-jan-2017'

; Six waves, color, de_rolled, log-scaled, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=195, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_despike, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_despike, /do_roll_correct, /do_save_cube

STOP

mk_suvi_movie, t0, t1, wave=195, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=195, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=171, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=304, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=284, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=131, /do_roll_correct, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_roll_correct, /do_log, /do_save_cube
mk_suvi_movie, t0, t1, wave=094, /do_roll_correct, /do_save_cube

STOP

; Four waves, color, de_rolled, 1/4 power scaling, no_stretch:

mk_suvi_movie, t0, t1, wave=195
mk_suvi_movie, t0, t1, wave=171
mk_suvi_movie, t0, t1, wave=304
mk_suvi_movie, t0, t1, wave=284

; Four waves, color, de_rolled, log scaling, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_log
mk_suvi_movie, t0, t1, wave=171, /do_log
mk_suvi_movie, t0, t1, wave=304, /do_log
mk_suvi_movie, t0, t1, wave=284, /do_log

; Four waves, color, de_rolled, 1/4 power scaling, stretch:

mk_suvi_movie, t0, t1, wave=195, /do_stretch
mk_suvi_movie, t0, t1, wave=171, /do_stretch
mk_suvi_movie, t0, t1, wave=304, /do_stretch
mk_suvi_movie, t0, t1, wave=284, /do_stretch

; Four waves, color, de_rolled, log scaling, stretch:

mk_suvi_movie, t0, t1, wave=195, /do_log, /do_stretch
mk_suvi_movie, t0, t1, wave=171, /do_log, /do_stretch
mk_suvi_movie, t0, t1, wave=304, /do_log, /do_stretch
mk_suvi_movie, t0, t1, wave=284, /do_log, /do_stretch


; February time interval:
t0 = '12-feb-2017'
t1 = '16-feb-2017 20:00'

; Four waves, color, de_rolled, 1/4 power scaling, no_stretch:

mk_suvi_movie, t0, t1, wave=195
mk_suvi_movie, t0, t1, wave=171
mk_suvi_movie, t0, t1, wave=304
mk_suvi_movie, t0, t1, wave=284

; Four waves, color, de_rolled, log scaling, no_stretch:

mk_suvi_movie, t0, t1, wave=195, /do_log
mk_suvi_movie, t0, t1, wave=171, /do_log
mk_suvi_movie, t0, t1, wave=304, /do_log
mk_suvi_movie, t0, t1, wave=284, /do_log

; Four waves, de_rolled, 1/4 power scaling, stretch:

mk_suvi_movie, t0, t1, wave=195, /do_stretch
mk_suvi_movie, t0, t1, wave=171, /do_stretch
mk_suvi_movie, t0, t1, wave=304, /do_stretch
mk_suvi_movie, t0, t1, wave=284, /do_stretch

; Four waves, de_rolled, log scaling, stretch:

mk_suvi_movie, t0, t1, wave=195, /do_log, /do_stretch
mk_suvi_movie, t0, t1, wave=171, /do_log, /do_stretch
mk_suvi_movie, t0, t1, wave=304, /do_log, /do_stretch
mk_suvi_movie, t0, t1, wave=284, /do_log, /do_stretch

; Four waves, no roll, log scale, stretch:

mk_suvi_movie, t0, t1, wave=195, /do_log, /do_stretch, /no_roll_correct
mk_suvi_movie, t0, t1, wave=171, /do_log, /do_stretch, /no_roll_correct
mk_suvi_movie, t0, t1, wave=304, /do_log, /do_stretch, /no_roll_correct
mk_suvi_movie, t0, t1, wave=284, /do_log, /do_stretch, /no_roll_correct

end
