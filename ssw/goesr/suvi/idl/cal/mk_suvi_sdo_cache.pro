
pro mk_suvi_sdo_cache

wave_arr    = [  '94', '131', '171', '193', '211', '304', '335','1600','1700','4500','blos', 'mag','cont']
instr_arr   = [ 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'hmi', 'hmi', 'aia']
wave_suffix = ['0094','0131','0171','0193','0211','0304','0335','1600','1700','4500','blos','blos','cont']

if not exist(dir_cache) then dir_cache = '/archive1/suvi/suvi_data/fm1/suvi_local_sdo_cache'

n_wave = n_elements(wave_arr)

for i=0, n_wave-1 do begin
   file_wave0 = last_file(wave=wave_arr[i])
   filnam0 = file_break(file_wave0)
   spawn, 'cp -p ' + file_wave0 + ' ' + dir_cache
endfor

end
