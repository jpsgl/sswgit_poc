
pro CropDownlinkedImage, iindex, idata, oindex, odata

;+
;NAME:   CropDownlinkedImage
;PURPOSE:
;        Remove overscan and extended pixels from 1330 columns by 1292 rows of pixels in raw image 
;        down to CCD sized 1280 x 1280 pixels. 
;CALLING SEQUENCE:
;        SUVI_IMG = CropDownlinkedImage(SUVI_RAW_IMAGE)
;INPUTS:
;        SUVI_RAW_IMAGE - SUVI raw image 
;OUTPUTS:
;        SUVI_IMG_OVERSCAN_PIXELS_REMOVED - SUVI raw image corrected by removal of overscan and 
;                                           extended pixels surrounding the image
;KEYWORDS:
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;                   RWN  APR. 14, 2011
;		           Aug. 11, 2011	Remove CALC_BIAS determination
;        MAR. 2012  ABK, added CAL_BIAS back in for trending purposes
;-

t0=systime(1)

COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT, STATIC_PARAMS, $
                      MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, $
                      FOCAL_PLANE_FILTER_TRANSMISSION, SUVI_LINEARITY, SUVI_GAIN_RIGHT, $
                      SUVI_GAIN_LEFT
COMMON OUT_METADATA,  DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, $
                      SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, $
                      PHOT_ENG_CONV, ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, $
                      SN_LEV3, FULL_WELL, SHT_EXP_TIME, IMG_PKT_TIME
COMMON SUVI_GPDS,     ISP, ISP_MAP, INFO

if get_logenv('SUVI_TOP') eq '' then set_logenv, 'SUVI_TOP', '/archive1/suvi'
if get_logenv('SUVI_INSTRUMENT') eq '' then set_logenv, 'SUVI_INSTRUMENT', 'EM1'
if get_logenv('SUVI_DATA') eq '' then $
   set_logenv,'SUVI_DATA', $
     concat_dir(concat_dir(get_logenv('SUVI_TOP'), get_logenv('SUVI_INSTRUMENT')), 'data')
if get_logenv('SUVI_META') eq '' then $
   set_logenv,'SUVI_META', $
     concat_dir(concat_dir(get_logenv('SUVI_TOP'), get_logenv('SUVI_INSTRUMENT')), 'meta')
if get_logenv('SUVI_CATS') eq '' then $
   set_logenv,'SUVI_CATS', concat_dir('SUVI_META', 'suvi_cats')
if get_logenv('SUVI_LOGS') eq '' then $
   set_logenv,'SUVI_LOGS', concat_dir('SUVI_LOGS', 'suvi_logs')

; Define directory paths:
if get_logenv('SUVI_DATA') eq '' then set_logenv, 'SUVI_DATA', '~/suvi_data'
if get_logenv('SUVI_TLM')  eq '' then $
  set_logenv, 'SUVI_TLM', concat_dir(get_logenv('SUVI_DATA'), 'tlm')
if get_logenv('SUVI_L0')   eq '' then $
  set_logenv, 'SUVI_L0',  concat_dir(get_logenv('SUVI_DATA'), 'l0')
if get_logenv('SUVI_L1')   eq '' then $
  set_logenv, 'SUVI_L1',  concat_dir(get_logenv('SUVI_DATA'), 'l1')

if not exist(dir_tlm) then dir_tlm = get_logenv('SUVI_TLM')
if not exist(dir_l0)  then dir_l0  = get_logenv('SUVI_L0')
if not exist(dir_l1)  then dir_l1  = get_logenv('SUVI_L1')
if not exist(dir_gpa_log) then dir_gpa_log = get_logenv('GPA_LOG')
file_gpa_log = concat_dir(dir_gpa_log, 'gpa_run_log')

odata = idata

print, '--> 1. Starting CropDownlinkedImage.pro (SUVI GPA Routine #1)'
file_append, file_gpa_log, '--> 1. Starting CropDownlinkedImage.pro (SUVI GPA Routine #1)'

;Note: lower left corner pixel (0,0) of 1280 x 1280 array starts at (30,4) of 1330 x 1292 raw input 
;      array with readout amplifier positioned in lower left below CCD    
nsize = size(idata)
nx = nsize(1)
ny = nsize(2)

;calculate the detector bias from the overscan rows and columns. Use the mean of a section from the 
;middle of the overscan and extended segments to eliminate edge effects
ROW_BIAS_CALC  = mean(idata[4:nx-4,ny-5:ny-2])
COL_BIAS1_CALC = mean(idata[4:25,4:ny-4])  
COL_BIAS2_CALC = mean(idata[nx-15:nx-4,4:ny-4])

; Add these parameters to the index structure:
oindex = add_tag(oindex, ROW_BIAS_CALC,  'ROWBIAS')
oindex = add_tag(oindex, COL_BIAS1_CALC, 'COLBIAS1')
oindex = add_tag(oindex, COL_BIAS2_CALC, 'COLBIAS2')

print, 'SUVI Image Size: ', nx, ' x ', ny
;file_append, file_gpa_log, 'SUVI Image Size: ' + strtrim(nx,2) + ' x ' + strtrim(ny,2)

print, 'Calculated bias from overscan regions:'
print, '      Row: ', ROW_BIAS_CALC
print, '      Left Columns: ', COL_BIAS1_CALC
print, '      Right Columns: ', COL_BIAS2_CALC
;file_append, file_gpa_log, 'Calculated bias from overscan regions:'
;file_append, file_gpa_log, '      Row: ' + ' ' + strtrim(ROW_BIAS_CALC, 2)
;file_append, file_gpa_log, '      Left Columns: ' + ' ' + strtrim(COL_BIAS1_CALC, 2)
;file_append, file_gpa_log, '      Right Columns: ' + ' ' + strtrim(COL_BIAS2_CALC, 2)

; Extract 1280 x 1280 pixels, cropping the SUVI image to remove the overscan rows and columns
odata = idata[30:nx-21,4:ny-9]

nsize=size(odata)
nx= nsize(1)
ny= nsize(2)

print, '    SUVI Image Size (after processing): ', nx, ' x ', ny
;file_append, file_gpa_log, '    SUVI Image Size (after processing): ' + ' ' + nx, ' x ', ny

t1=systime(1)
print, '    CropDownlinkedImage took: ', t1-t0, ' seconds'
;file_append, file_gpa_log, '    CropDownlinkedImage took: ', t1-t0, ' seconds'

END
