
pro suvi_flatfield, files_list, n_iter=n_iter, use_prev=use_prev, do_display=do_display

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     SUVI_FLATFIELD
; CATEGORY:
;     Calibration
; PURPOSE:
;     This routine computes the best fit flatfield image for a given wavelength
;     from a sequence of images taken during a Kuhn raster.
; CALLS:
;     suvi_flatfield
; METHOD:
;     - Best fit to Kuhn raster image set.
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     Set of FITS files from an LTC sequence.
; OUTPUTS:
;     Writes the calculated flatfield and header to a FITS file in the SUVI
;     flatfield database. 
; KEYWORD INPUTS:
; KEYWORD OUTPUTS:
; CALLED BY ROUTINES:
; ROUTINES CALLED:
; TRAPPED ERRORS:
; UNTRAPPED ERRORS:
; NOTA BENA:
; TODO:
; DEVELOPMENT STATUS:
; TEST AND VERIFICATION DESCRIPTION:
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gopal Vasudevan
; MODIFICATION History:
;     2016-04-07 GLS - Updates to doc header.
; VERSION CONTROL STATUS:
;     2016_04-07 - Last commit.
;-
; ==============================================================================

if not exist(files_list) then $
   files_list = '/archive1/suvi/suvi_data/fm1/suvi_flatdb/l0c_fits/SUVI_FMx_20160121_104936_195_list.txt'
if not exist(outfil_flat) then $
   outfil_flat = '/archive1/suvi/suvi_data/fm1/suvi_flats/SUVI_FMx_20160121_104936_195_L0c_flat.fits'
if not exist(n_iter) then n_iter = 1
if not exist(do_display) then do_display = 1

cmd = './suvi_ff' + ' -i ' + files_list + ' -niter ' + string(n_iter, '(i2.2)') + $
      ' -w 1.2' + ' - r 1.414' + ' -o ' + outfil_flat

if not keyword_set(use_prev) then $
   spawn, cmd

if keyword_set(do_display) then begin
   wdef, 0, 1280/2, 1280/2
   mreadfits, outfil_flat, index_flat, data_flat
   tvscl, rebin(data_flat, 1280/2, 1280/2, /samp)
endif

end
