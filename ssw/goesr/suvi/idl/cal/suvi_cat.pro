
suvi_cat, t0, t1

if not exist(fm_ver)   then fm_ver   = '*'
if not exist(img_type) then img_type = '*'
if not exist(as_pos)   then as_pos   = '*'
if not exist(f1_pos)   then f1_pos   = '*'
if not exist(f2_pos)   then f2_pso   = '*'
filt_set = 'as' + as_pos + 'f1' + f1_pos + 'f2' + f2_pos
if not exist(exp_dur)  then exp_dur  = 's'
if not exist(amp_type) then amp_type = 'l'
if not exist(temp_val) then temp_val = '*'

;pattern = 'suvi_20120115_000000_fmx_dark_asxf1xf2x_exps_ampl_m650.fits'
pattern = 'fm' + fm_ver + '_' + img_type + '_' + filt_set + $
	  '_exp' + exp_dur + '_amp' + amp_type + '_m' + temp_val

sxi genxcat wrapper is:
$SSW/site/idl/sxi/sxi_make_genxcat.pro

includes calls to some "fundamentals"

ssw_time2filelist.pro ; does NFS or http/cloud search of a cron-tree)
mreadfits_header.pro ; optional ONLY_TAGS=<list>)  ;
write_genxcat.pro

Others of interest:

ssw_time2paths (called by ssw_time2filelist for example)
accepts time range or vector of "random" times )

For cron-tree generation, just pass output verbatim->mk_dir

For example:

IDL> times=['12:30 15-mar-2013','23:50 2-mar-2018'] ; time-vect

IDL> mypaths=ssw_time2paths(times,parent='/sanhome/freeland/my/parent/dir',/hour)
IDL> mypaths=ssw_time2paths(times,parent='/sanhome/freeland/my/parent/dir',/hour) & more,mypaths
/sanhome/freeland/my/parent/dir/2013/03/15/H1200
/sanhome/freeland/my/parent/dir/2018/03/02/H2300

IDL> mk_dir,mypaths & print,file_exist(mypaths)
   1   1



files = ssw_time2filelist(t0, t1, pattern=pattern
