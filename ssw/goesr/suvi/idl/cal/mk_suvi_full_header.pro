
function mk_suvi_full_header, isp_packet, suvi_img=suvi_img, index=index, $
         do_packet_read=do_packet_read, $
         do_write=do_write, _extra=_extra, qdebug=qdebug

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     MK_SUVI_FULL_HEADER
; CATEGORY:
;     FITS header generation
; PURPOSE:
; CALLS:
;     mk_suvi_full_header, packet
; METHOD:
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     isp_packet - SUVI ISP packet
; OUTPUTS:
;     suvi_fits_hdr
; KEYWORD INPUTS:
;     - do_write - If set, write the generated FITS file(s) to disk.
; KEYWORD OUTPUTS:
;     - index - IDL structure version of header, with parallel values
;               and full ISP names
; CALLED BY ROUTINES:
; ROUTINES CALLED:
;     - mk_suvi_isp_map
;     - mk_suvi_isp_struct
;     - mk_suvi_info_struct
;     - struct2fitshead
; TRAPPED ERRORS:
; UNTRAPPED ERRORS:
; NOTA BENA:
;     - Note that the 16-bit words are swapped. Examples:
;       - Packet Flag   is at i16[3] instead of i16[2].
;       - Packet Number is at i16[5] instead of i16[4].
; TODO:
;     - Caching of reusables (fit coefficients, etc)
; DEVELOPMENT STATUS:
; TEST AND VERIFICATION DESCRIPTION:
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION History:
;     2016-01-20 - GLS - Written
; VERSION CONTROL STATUS:
;     No commits
;-
; ==============================================================================

version = isp_packet.ccsds_sec[9]*256l + isp_packet.ccsds_sec[10]

; Extract image time from packet and define various time parameters:
julian_day_epoch = 2451545D ; This is 1200 UT on 2000-01-01 (which seems to give the right answer)
apid   = ( ( packet.ccsds_prime[0] and 3 ) * 256 ) + packet.ccsds_prime[1]
daynum = packet.ccsds_sec[0]*65536l + packet.ccsds_sec[1]*256l + packet.ccsds_sec[2]
msec   = packet.ccsds_sec[3]*16777216l + packet.ccsds_sec[4]*65536l + $
                 packet.ccsds_sec[5]*256l + packet.ccsds_sec[6] 
usec   = packet.ccsds_sec[7]*256l + packet.ccsds_sec[8] 
	
jul_day = julian_day_epoch + double( daynum ) + double( msec ) / 86400000.0D
CALDAT, jul_day, mn, dy, yr, hr, mi, sec
anystring = string(  yr, mn, dy, hr, mi, sec, $
                     format = '(i4.4, "-", i2.2, "-", i2.2, " ", i2.2, ":", i2.2, ":", f6.3)' )

img_tstr = strtrim(hr,1) + ':' + strtrim(mi,1) + ':' + strtrim(sec,1)+' '+ $
           strtrim(yr,1) + '/' + strtrim(mn,1) + '/' + strtrim(dy,1)
img_pkt_time = anytim(img_tstr, /yohkoh)

isp  = mk_suvi_isp_struct(isp_packet)
info = mk_suvi_info_struct(isp, img_pkt_time=img_pkt_time)

dim1 = isp.head.suv_isp_hdr_cols
dim2 = isp.head.suv_isp_hdr_rows

; If image passed, then calculate image statistics:
if exist(image) then begin
   immean   = MEAN(image)
   immedian = MEDIAN(image)
   immax    = MAX(image)
   immin    = MIN(image)
   imstdev  = STDEV(image)
   subimg   = image[50:dim1-51,4:dim2-3]
   simmean  = MEAN(subimg)
   simmax   = MAX(subimg)
   simstdev = STDEV(subimg)
endif
                
; Generate various 'pieces' of the eventual full SUVI FITS L0 header:

; First create minimal L0 header structure:
l0_min_hdr = { simple:'t', bitpix:16, naxis:2, naxis1:dim1, naxis2:dim2}
tnames_l0_min_hdr = tag_names(l0_min_hdr)
n_tags_l0_min_hdr = n_elements(tnames_l0_min_hdr)
;l0_min_hdr_comments = strarr(n_tags_l0_min_hdr)
l0_min_hdr_comments = tnames_l0_min_hdr

; Next create headers from the isp 'head' sub-structure tags and the isp 'mnem' sub-structure tags:
tnames_isp_head = tag_names(isp.head)
tnames_isp_mnem = tag_names(isp.mnem)
n_tags_isp_head = n_elements(tnames_isp_head)
n_tags_isp_mnem = n_elements(tnames_isp_mnem)

suvi_fits_isp_head_hdr = isp.head
suvi_fits_isp_mnem_hdr = isp.mnem

; Change the tag names to unique, numbered 8 character names:
tnames_isp_head_new = 'isph_' + string(indgen(n_tags_isp_head)+1,'(i3.3)')
for i=1,n_tags_isp_head do suvi_fits_isp_head_hdr = $
  rep_tag_name(suvi_fits_isp_head_hdr, tnames_isp_head[i-1], tnames_isp_head_new[i-1])
suvi_fits_isp_head_hdr_comments = tnames_isp_head

tnames_isp_mnem_new = 'ispm_' + string(indgen(n_tags_isp_mnem)+1,'(i3.3)')
for i=1,n_tags_isp_mnem do suvi_fits_isp_mnem_hdr = $
  rep_tag_name(suvi_fits_isp_mnem_hdr, tnames_isp_mnem[i-1], tnames_isp_mnem_new[i-1])
suvi_fits_isp_mnem_hdr_comments = tnames_isp_mnem

; Likewise, create another temporary structure to hold the 'info' structure isp-derived tags:
tnames_info = tag_names(info)
n_tags_info = n_elements(tnames_info)
suvi_fits_info_hdr = info
tnames_info_new = 'info_' + string(indgen(n_tags_info)+1,'(i3.3)')
for i=1,n_tags_info do suvi_fits_info_hdr = $
  rep_tag_name(suvi_fits_info_hdr, tnames_info[i-1], tnames_info_new[i-1])
suvi_fits_info_hdr_comments = tnames_info

; NB: If necessary, create another component structure with WCS tags that are not in
;     the other component structures

; Now join then together into on big structure:
suvi_fits_hdr_struct = join_struct(l0_min_hdr,           suvi_fits_info_hdr)
suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_head_hdr)
suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_mnem_hdr)

; Now concatenate the comment arrays for comment fields of the full SUVI FITS header:
suvi_fits_hdr_comments = [l0_min_hdr_comments, suvi_fits_info_hdr_comments, $
                           suvi_fits_isp_head_hdr_comments, suvi_fits_isp_mnem_hdr_comments]

; Add a header structure tag for the header comment vector:
;suvi_fits_hdr_comments = [suvi_fits_hdr_comments, 'suv_fits_hdr_comments'] 
;suvi_fits_hdr_struct = add_tag(suvi_fits_hdr_struct, suvi_fits_hdr_comments, 'hdr_comm')

; Create full index structure (with tag names and tag values) for this image:
suvi_fits_hdr_values = suvi_fits_hdr_struct
suvi_fits_hdr_names  = suvi_fits_hdr_comments
;index = {suvi_fits_hdr_values:suvi_fits_hdr_struct, suvi_fits_hdr_names:suvi_fits_hdr_comments}
index = {hdr_vals:suvi_fits_hdr_struct, hdr_nams:suvi_fits_hdr_comments}

; Finally, create the actual SUVI FITS header from the full SUVI FITS header structure:
suvi_fits_hdr = struct2fitshead(suvi_fits_hdr_struct, outimage, use_sxaddpar=use_sxaddpar, $
	        use_fxaddpar=use_fxaddpar, comments=suvi_fits_hdr_comments, $
                allow_crota=allow_crota, dateunderscore2dash=dateunderscore2dash, $
                _extra=_extra)

; Now, add top level tags for limited subset of the full ISP header, with tag names
; translated from the long ISP mnemonics to 8 character FITS header tag names.
; We start with the info struct tags:
;index = join_struct(index, l0_min_hdr)
;index = join_struct(index, info)

if keyword_set(qs) then STOP, ' Stopping on request before return.'

return, suvi_fits_hdr

end
