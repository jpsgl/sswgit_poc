FUNCTION GenerateSUVIHeader, SUVI_IMG, RAW_IMG_LOCATION
;+
;NAME: GenerateSUVIHeader 
;PURPOSE: 
;         Populate FITS header with various information needed for
;         using the image
;CALLIG SEQUENCE:
;         SUVI_FITS_HEADER =  SUVIHeaderCreation(SUVI_IMG)
;MODIFICATION HISTORY:
;        FEV. 2012, NVN (Created)
;-

; All 'printf' statements are written to output log file for GPDS testing purposes.  Can be ignored for main program functionality
  t0=systime(1)
; printf, 26, '--> 10. Starting GenerateSUVIHeader.pro (Extra SUVI Routine)'
  print ,     '--> 10. Starting GenerateSUVIHeader.pro (Extra SUVI Routine)'

; COMMON SIM_ISP, ISP
  COMMON SUVI_GPDS, ISP, ISP_MAP, INFO
  COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT, STATIC_PARAMS, MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, FOCAL_PLANE_FILTER_TRANSMISSION, SUVI_LINEARITY, SUVI_GAIN_RIGHT, SUVI_GAIN_LEFT
  COMMON OUT_METADATA, DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, PHOT_ENG_CONV, SHUTTER_CLOSE_TIME, SHUTTER_OPEN_TIME, ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, SN_LEV3, FULL_WELL

;general image statistics
IMG_TOT=total(SUVI_IMG)
IMG_MIN=min(SUVI_IMG)
IMG_MAX=max(SUVI_IMG)
IMG_MED=median(SUVI_IMG)
IMG_AVG=average(SUVI_IMG)
IMG_STDV=stdev(SUVI_IMG)

;statistics and information on the permanent bad pixels
bp_dir='$SUVI_EXTERNAL_DATA'+'/bad_pixels'
spawn,'ls -tr '+bp_dir, bp_fn
BAD_PIX_FN=bp_dir+'/'+bp_fn[n_elements(bp_fn)-1]
bad_pix_list=rd_tfile(BAD_PIX_FN, 2)
numbad_pix=n_elements(bad_pix_list[0,*])
bad_pix_str=''
FOR x=0, numbad_pix-1 DO bad_pix_str=bad_pix_str+','+bad_pix_list[0,x]+','+bad_pix_list[1,x]
bad_pix_str=strmid(bad_pix_str,1,strlen(bad_pix_str)-1)

;statistics and information on the permanent bad columns
bc_dir='$SUVI_EXTERNAL_DATA'+'/bad_columns'
spawn,'ls -tr '+bc_dir, bc_fn
BAD_COL_FN=bc_dir+'/'+bc_fn[n_elements(bc_fn)-1]
bad_col_list=rd_tfile(BAD_COL_FN, 2)
numbad_col=n_elements(bad_col_list[0,*])
bad_col_str=''
FOR y=0, numbad_col-1 DO bad_col_str=bad_col_str+','+bad_col_list[0,y]
bad_col_str=strmid(bad_col_str,1,strlen(bad_col_str)-1)

;make the initial header
;time=anytim(info.earth_time,/int)
 time=anytim(info.img_pkt_time,/int)
mkhdr, header, SUVI_IMG  ; produce a minimum header (string) from image
hdrstruct=fitshead2struct(header) ; convert to structure

;number of corrected pixels
fixed_pixels=numbad_pix+(numbad_col*hdrstruct.NAXIS1)

;assume missing pixels replaced with -9999, find number of missing pixels
num_misspix=0
FOR aa=0, hdrstruct.NAXIS1-1 DO BEGIN
   FOR bb=0, hdrstruct.NAXIS2-1 DO BEGIN
      IF SUVI_IMG(aa,bb) EQ -9999 THEN num_misspix=num_misspix+1
   ENDFOR
ENDFOR

;Spectral channel description
wavelnth = strtrim(string(ulong(STATIC_PARAMS.WAVELENGTH(indexp.asstatus))),1)
fw1_str=strtrim(string(info.fw1status),1)
fw2_str=strtrim(string(info.fw2status),1)
chnl_description=wv_str+'A_FW1POS-'+fw1_str+'_FW2POS-'+fw2_str+'_'+exptype

;Sun center pixel location
SUN_CENT_PIX_U2=info.gtyerr*MEASURED_PARAMS.GT_READING_TO_PIXEL_CONV_U2[0]+(STATIC_PARAMS.SUVI_GT_ET_OFFSET_U2_ARCSEC[0]/MEASURED_PARAMS.SUVI_PLT_SCL[0])+STATIC_PARAMS.WAVELENGTH_OFFSET_U2(info.asstatus)
SUN_CENT_PIX_U3=info.gtzerr*MEASURED_PARAMS.GT_READING_TO_PIXEL_CONV_U3[0]+(STATIC_PARAMS.SUVI_GT_ET_OFFSET_U3_ARCSEC[0]/MEASURED_PARAMS.SUVI_PLT_SCL[0])+STATIC_PARAMS.WAVELENGTH_OFFSET_U2(info.asstatus)

SUN_CENT_PIX_U2=round(SUN_CENT_PIX_U2)
SUN_CENT_PIX_U3=round(SUN_CENT_PIX_U3)

;remove unnecessary information from header
hdrstruct=rem_tag(hdrstruct, 'SIMPLE')
hdrstruct=rem_tag(hdrstruct, 'BITPIX')
hdrstruct=rem_tag(hdrstruct, 'COMMENT')
hdrstruct=rem_tag(hdrstruct, 'HISTORY')


;add metadata
hdrstruct=add_tag(hdrstruct, 'GOES-16', 'SATID')
hdrstruct=add_tag(hdrstruct, 'SUVI_FM1', 'INSTRID')
hdrstruct=add_tag(hdrstruct, strmid(anytim(time,/ccsds),11,12),'TIME')
hdrstruct=add_tag(hdrstruct, anytim(time,/ccsds),'DATE_OBS')
hdrstruct.date=strmid(anytim(time,/ccsds),0,10)
hdrstruct=add_tag(hdrstruct, 'image_name.fits', 'L1BIMG')
hdrstruct=add_tag(hdrstruct, (EPHEMERIS.SOLAR_DISTANCE[0]*149598000), 'SUNDIST')
hdrstruct=add_tag(hdrstruct, ' ', 'HELIOLON')
hdrstruct=add_tag(hdrstruct, ' ', 'HELIOLAT')
hdrstruct=add_tag(hdrstruct, 'HPLN-TAN', 'CTYPE1')
hdrstruct=add_tag(hdrstruct, 'HPLAT-TAN', 'CTYPE2')
hdrstruct=add_tag(hdrstruct, SUN_CENT_PIX_U3, 'CRPIX1')
hdrstruct=add_tag(hdrstruct, SUN_CENT_PIX_U2, 'CRPIX2')
hdrstruct=add_tag(hdrstruct, ((640-SUN_CENT_PIX_U3)*MEASURED_PARAMS.SUVI_PLT_SCL[0]), 'CRVAL1')
hdrstruct=add_tag(hdrstruct, ((640-SUN_CENT_PIX_U2)*MEASURED_PARAMS.SUVI_PLT_SCL[0]), 'CRVAL2')
hdrstruct=add_tag(hdrstruct, '9.5deg', 'CROTA') ;SC Mnemonic
hdrstruct=add_tag(hdrstruct, '2.5arcsec/pixel', 'CDELT1')
hdrstruct=add_tag(hdrstruct, '2.5arcsec/pixel', 'CDELT2')
hdrstruct=add_tag(hdrstruct, '180deg', 'LONPOLE')
hdrstruct=add_tag(hdrstruct, info.frameType, 'VIEWOBJ')
hdrstruct=add_tag(hdrstruct, chnl_description, 'SPECCHNL')
hdrstruct=add_tag(hdrstruct, (SHUTTER_CLOSE_TIME-SHUTTER_OPEN_TIME), 'EXPTIME')
hdrstruct=add_tag(hdrstruct, info.sht_exp_time,'CMDEXPTM')
hdrstruct=add_tag(hdrstruct, info.ccdtemp1,'CCDTEMP')
hdrstruct=add_tag(hdrstruct, info.asstatus,'APSELPOS')
hdrstruct=add_tag(hdrstruct, info.fw1status,'FW1POS')
hdrstruct=add_tag(hdrstruct, info.fw2status,'FW2POS')
hdrstruct=add_tag(hdrstruct, ulong(STATIC_PARAMS.WAVELENGTH(info.asstatus)),'WAVELNTH')
hdrstruct=add_tag(hdrstruct, 'Ver.1', 'GPAVER')


hdrstruct=add_tag(hdrstruct, n_elements(DARK_NAMES), 'NUMDARKS')
FOR j=0, n_elements(DARK_NAMES)-1 DO hdrstruct=add_tag(hdrstruct, DARK_NAMES[j], 'DARKFN'+strtrim(string(j+1),1))

hdrstruct=add_tag(hdrstruct,SUVI_FLAT[info.asstatus], 'FLATFN')
hdrstruct=add_tag(hdrstruct,RAW_IMG_LOCATION,'L0IMG')

hdrstruct=add_tag(hdrstruct,SUVI_GAIN_CONSTANT, 'GAIN')
hdrstruct=add_tag(hdrstruct,SUVI_LINEARITY_SOURCE, 'LINLOC')
hdrstruct=add_tag(hdrstruct,PHOT_ELE_CONV, 'PHOTELEC')
hdrstruct=add_tag(hdrstruct,QE,'QE')
hdrstruct=add_tag(hdrstruct,PHOT_ENG_CONV, 'PHOTENG')
hdrstruct=add_tag(hdrstruct,SUVI_EFFA,'EFFAREA')

hdrstruct=add_tag(hdrstruct,info.rdoutport,'AMP')

hdrstruct=add_tag(hdrstruct,info.yaw_flip,'YAW_FLIP')

hdrstruct=add_tag(hdrstruct,strmid(anytim(anytim(info.time_last_bakeout, /int),/ccsds),0,10),'LSTBKOUT')
hdrstruct=add_tag(hdrstruct,SUVI_CONTAM_THICKNESS,'CTM_THCK')
hdrstruct=add_tag(hdrstruct,SUVI_TOT_IRRADIANCE,'IRRADNCE')
hdrstruct=add_tag(hdrstruct,IMG_TOT,'IMG_TOT')
hdrstruct=add_tag(hdrstruct,IMG_MIN,'IMG_MIN')
hdrstruct=add_tag(hdrstruct,IMG_MAX,'IMG_MAX')
hdrstruct=add_tag(hdrstruct,IMG_MED,'IMG_MED')
hdrstruct=add_tag(hdrstruct,IMG_STDV,'IMG_STDV')
hdrstruct=add_tag(hdrstruct,IMG_AVG,'IMG_AVG')
hdrstruct=add_tag(hdrstruct,-9999,'MISSPXVL')
hdrstruct=add_tag(hdrstruct,num_misspix, 'MISSPXCT')
hdrstruct=add_tag(hdrstruct,BAD_PIX_FN,'BADPIXFN')
hdrstruct=add_tag(hdrstruct,numbad_pix,'BADPIXCT')
hdrstruct=add_tag(hdrstruct,bad_pix_str,'BADPX001')
hdrstruct=add_tag(hdrstruct,BAD_COL_FN,'BADCOLFN')
hdrstruct=add_tag(hdrstruct,numbad_col,'BADCOLCT')
hdrstruct=add_tag(hdrstruct,bad_col_str,'BADCL001')
hdrstruct=add_tag(hdrstruct,fixed_pixels,'FIXPIX')
hdrstruct=add_tag(hdrstruct,EPHEMERIS.SOLAR_DIAMETER_ARCMIN[0],'SOLDIA')
hdrstruct=add_tag(hdrstruct,((EPHEMERIS.SOLAR_DIAMETER_ARCMIN[0]*60)/MEASURED_PARAMS.SUVI_PLT_SCL[0]),'SOLDIAPX')
hdrstruct=add_tag(hdrstruct,EPHEMERIS.SOLAR_B_ANGLE[0],'SOLBANGL')
hdrstruct=add_tag(hdrstruct,SN_LEV3,'SNLEV3')
hdrstruct=add_tag(hdrstruct,FULL_WELL, 'FULLWELL')
hdrstruct=add_tag(hdrstruct,info.gtyerr,'GTERRU2')
hdrstruct=add_tag(hdrstruct,info.gtzerr,'GTERRU3')
hdrstruct=add_tag(hdrstruct,ROW_BIAS_CALC,'ROWBIAS')
hdrstruct=add_tag(hdrstruct,COL_BIAS1_CALC,'COLBIAS1')
hdrstruct=add_tag(hdrstruct,COL_BIAS2_CALC,'COLBIAS2')

t1=systime(1)
;printf, 26, '   CorrectContaminationSignalLoss took: ', t1-t0, ' seconds'
 print ,     '   CorrectContaminationSignalLoss took: ', t1-t0, ' seconds'

return, hdrstruct
end
