
function suvi_ltc, files_suvi, wave=wave, use_sdo=use_sdo, _extra=_extra

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     SUVI_LTC
; CATEGORY:
;     Calibration
; PURPOSE:
;     This routine measures the photon transfer curve from a sequence of files
;     taken during a standard LTC observing sequence. It sorts the images into
;     pairs of light and dark images, despikes them, computes an average dark,
;     and plots variance against mean intensity from a selected sub-field from
;     the dark-subtracted light images. The points are fitted to a straight
;     line, and the slope of the line is the gain.
; CALLS:
;     Gain = suvi_ltc(files_suvi)
; METHOD:
;     - Variance vs mean intensity.
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     Set of FITS files from an LTC sequence.
; OUTPUTS:
;     Calculated gain.
; KEYWORD INPUTS:
; KEYWORD OUTPUTS:
; CALLED BY ROUTINES:
; ROUTINES CALLED:
; TRAPPED ERRORS:
; UNTRAPPED ERRORS:
; NOTA BENA:
; TODO:
; DEVELOPMENT STATUS:
; TEST AND VERIFICATION DESCRIPTION:
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION History:
;     2016-04-07 GLS - Updates to doc header.
; VERSION CONTROL STATUS:
;     2016_04-07 - Last commit.
;-
; ==============================================================================

; If necessary, define SUVI environmentals:
if get_logenv('SUVI_DATA') eq '' then setup_suvi_env, fm=fm, _extra=_extra

if not exist(wave) then wave = 171
if keyword_set(use_sdo) then files_suvi = last_file(last_n=5, t_delt=300, wave=wave)

if not exist(files_suvi) then $
   files_suvi = file_list(concat_dir(get_logenv('SUVI_GAIN_CAL_FITS'), '131126/T_N60/Amp0'), '*.fit')

if not exist(ss_good) then ss_good = indgen(30)+20
ss_set_1 = indgen(13)*3+22
ss_set_2 = indgen(13)*3+21
mreadfits, files_suvi[ss_set_1], index_suvi_set1, data_suvi_set1
mreadfits, files_suvi[ss_set_2], index_suvi_set1, data_suvi_set2
data_suvi_set1 = float(data_suvi_set1)
data_suvi_set2 = float(data_suvi_set2)
siz_data = size(data_suvi_set1)
xsiz_data = siz_data[1]
ysiz_data = siz_data[2]
n_img = n_elements(index_suvi_set1)

if not exist(nx) then nx = 15 ;100
if not exist(ny) then ny = 15 ;100
if not exist(x0) then x0 = xsiz_data/2 -nx/2
if not exist(y0) then y0 = ysiz_data/2 -ny/2

box_cube_set1 = data_suvi_set1[x0:(x0+nx-1),y0:(y0+ny-1),*]
box_cube_set2 = data_suvi_set2[x0:(x0+nx-1),y0:(y0+ny-1),*]

; Calculate a difference cube for the box_cube:
diff_cube = box_cube_set1
for i=0, n_img-2 do diff_cube = box_cube_set2[*,*,i+1] - box_cube_set1[*,*,i]

; Calculate moments of all arrays in the box_cube:
for i=0, n_img-1 do begin
   mom0 = moment(box_cube_set1[*,*,i])
   if not exist(mom_arr) then mom_arr = mom0 else $
      mom_arr = [[mom_arr], [mom0]]
endfor

mean_arr = mom_arr[0,*]
var_arr = mom_arr[1,*]

; Calculate mean 'dn' vector by subtracting 'bls' :
mean_bls = mean(data_suvi_set1[12:18,500:*,2])
dn_mean_arr = mean_arr - mean_bls

; Calculate gains derived from this image set:
gain_arr = dn_mean_arr / var_arr

;coef = poly_fit(dn_mean_arr, var_arr, 1, yfit=var_fit)
;read_noise = coef[0]
;gain = coef[1]

coef = poly_fit(dn_mean_arr, gain_arr, 1, yfit=gain_fit)
;read_noise = coef[0]
;gain = coef[1]

gain_str = {files_gain_cal:files_suvi, gain_arr:gain_arr}
gain_str = add_tag(gain_str, anytim(file2time(files_suvi[0]), /ccsds), 'date_obs')

;quiet = 1
if not keyword_set(quiet) then begin
   wdef,0,1024,512
   plot,  dn_mean_arr, gain_arr, xtit='mean DN over central portion of image', ytit='gain', charsize=1.8, psym=1, symsiz=1.5
   oplot, dn_mean_arr, gain_fit, psym=-2
   print, ''
   print, '=============================================='
   print, ' LTC calculation from SUVI images circa: ' + gain_str.date_obs
   print, ' Computed gains = ' + string(gain_arr, '(f6.2)')
;  print, ' Computed read noise = ' + string(read_noise, '(f6.2)')
   print, '=============================================='
   print, ''
endif

;========================================================
; LAW's Example 1 (using multiple image pairs at different exposure times):

dir_ltc_fit = '/net/artemis/Volumes/disk1/suvi/131126/T_N60/Amp0'
dir_ltc_fit = '/net/topaz/Users/slater/soft/idl_temp/suvi/plt/temp/artemis'
files_ltc_fit = file_list(dir_ltc_fit, '*.fit')

mreadfits, files_ltc_fit(20:59),hdr,img                  ; images 20-59 are the image pairs and darks
img *= 1.                                                ; float the images

dn = 0.                                                  ; initialize the array
for i=1,37,3 do dn = [dn,avg(img(500:600,500:600,i+1))]  ; get the average signal in the middle of the image
dn = dn[1:*]                                             ; kill the first element, since it is not data
dn -= avg(img(12:18,500:*,2))                            ; subtract the average BLS level of the image to determine actual signal in DN

img2 = img                                               ; make a copy of the datacube

for i=1,37,3 do img2(*,*,i) -= img(*,*,i+1)              ; go through the dataset to create difference images of image pairs
sd = 0.                                                  ; initialize the array
for i=1,37,3 do sd = [sd,stdev(img2(500:600,500:600,i))] ; build array of stdev of difference image in center of image.
                                                         ; (don't have to background subtract since difference of image pair takes care of that.)
sd = sd(1:*)                                             ; kill the first element, since it is not data.
gain = 2*dn/sd^2.                                        ; gain is calculated from relation of signal level (dn), and variance=sd^2.
                                                         ; there is a factor of 2 due to the the difference of 2 images
print, gain                                              ; units for gain is electrons/DN, high number means lower gain
wdef,0,1024,512
plot, dn, gain, psym=-4                                  ; gain vs. signal level. accuracy for low signal levels is poor
STOP
;if not keyword_set(quiet) then begin
;   wdef,2,1024,512
   plot,  dn[0:11], gain[0:11], /ynoz, xtit='mean DN over central portion of image', ytit='gain', charsize=1.8, psym=1, symsiz=1.5
   coef = poly_fit(dn[0:11], gain[0:11], 1, yfit=gain_fit)

mom = moment(patch)(*2, root2 or whatever)
coef = poly_fit(dn[0:11], (mom[whichever is var], 1, yfit=gain_fit)
gain = 1/coef[1]) (inverse slope)

   oplot, dn[0:11], gain_fit, psym=-2
   print, ''
   print, '=============================================='
   print, ' LTC calculation from SUVI images circa: ' + gain_str.date_obs
   print, ' Computed gains = ' + string(gain_arr, '(f6.2)')
   print, '=============================================='
   print, ''
;endif
;========================================================

return, gain_str

end

