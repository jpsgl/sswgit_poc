
pro tail_dir, path, string, n_tail=n_tail, wait=wait

if not exist(n_tail) then n_tail = 10
if not exist(wait) then wait = 0

; ls -altrF /archive1/suvi/suvi_data/fm1/l0 | grep fits | tail -10

cmd = 'ls -altrF ' + path + ' | grep ' + string + ' | tail -' + strtrim(n_tail,2) + ' > ./buff_current.txt'
;print, cmd
spawn, cmd
buff_last = rd_tfile('./buff_current.txt')

;while get_kbrd() eq '' do begin
while 1 eq 1 do begin
;  buff_last = rd_tfile('./buff_last.txt')
   n_last = n_elements(buff_last)
   cmd = 'ls -altrF ' + path + ' | grep ' + string + ' | tail -' + strtrim(n_tail,2) + ' > ./buff_current.txt'
;print, cmd
   spawn, cmd
   buff_current = rd_tfile('./buff_current.txt')
   n_current = n_elements(buff_current)
   ss_match_last = where(buff_current eq buff_last[n_last-1], n_match_last)
   if ss_match_last ne n_current-1 then $
      prstr, buff_current[(ss_match_last+1):(n_current-1)]
   buff_last = buff_current
   wait, wait
endwhile

end
