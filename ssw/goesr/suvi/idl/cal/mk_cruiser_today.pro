
pro mk_cruiser_today, t0, t1, obs=obs, t_grid_in=t_grid_in, max_frames=max_frames, $
                      days=days, $
                      uniq_samp=uniq_samp, use_oberon=use_oberon, $
                      do_devig=do_devig, do_decon=do_decon, $
                      remove_old=remove_old, dir_root=dir_root, no_mk_dir=no_mk_dir, $
                      no_skip=no_skip, no_drotate=no_drotate, fast=fast, do_log=do_log, $
                      do_write=do_write, do_test=do_test, file_type=file_type, $
                      do_color=do_color, quality=quality, do_xstep=do_xstep, $
                      _extra=_extra

if not exist(days) then days = 30

if not exist(t_grid_in) then begin
   if ( exist(t0) and (not exist(t1)) ) then t1 = reltime(t0, days=days, out='ccsds')
   t0_sec = anytim(t0)
   t1_sec = anytim(t1)
   delt_sec = t1_sec - t0_sec

   if not exist(max_frames) then begin
      if days eq 1 then max_frames = 360
      if days eq 30 then max_frames = 360
      if not exist(max_frames) then max_frames = 360
   endif
   if not exist(cadence) then cadence = delt_sec/max_frames

   t_grid = anytim(timegrid(t0, t1, seconds=cadence), /ccsds)
   t_grid_sec = anytim(t_grid)
endif else begin
   t_grid_sec = anytim(t_grid_in)
   ss_sort = sort(t_grid_sec)
   t_grid_sec = t_grid_sec[ss_sort]
   t_grid = anytim(t_grid_sec, /ccsds)
   n_grid = n_elements(t_grid_sec)
   t0_sec = t_grid_sec[0]
   t1_sec = t_grid_sec[n_grid-1]
endelse

if not keyword_set(uniq_samp) then uniq_samp = 1

; Set image processing defaults:
if not exist(do_devig) then do_devig = 0
if not exist(do_despike) then do_despike = 1
if not exist(do_floor) then do_floor = 1
if not exist(do_log) then do_log = 1
if not exist(use_aia_ct) then use_aia_ct = 1
if not exist(do_stretch) then do_stretch = 0

if not exist(mk_log) then mk_log = 1
if not exist(log_pipeline) then log_pipeline = '/sanhome/slater/logs/log_cruiser_today.txt'
if not exist(log_fits_read) then log_fits_read = '/sanhome/slater/logs/log_fits_read_times_cruiser_today.txt'

if not exist(file_type) then file_type = 'jpeg'
case file_type of
   'jpeg': file_suffix = 'jpg'
   'gif':  file_suffix = 'gif'
   else:   file_suffix = 'jpg'
endcase

if not exist(t1) then t1 = t0

; Create output directory and url:
if not exist(dir_top) then dir_top = '/sanhome/slater/public_html/data/cruiser'
dirnam_cruiser = 'cruiser_' + time2file(t0) + '__' + time2file(t1) + (['_grayscale','_color'])[keyword_set(do_color)]
dir_cruiser = concat_dir(dir_top, dirnam_cruiser)
mk_dir, dir_cruiser
if not exist(url_top) then url_top = 'https://www.lmsal.com/~slater/'
url_cruiser = url_top + 'data/cruiser/' + dirnam_cruiser

; Find all instruments with images for this time range:

; Include desired instruments with images in the requested volume of phase space:

inst_wave_arr_full = ['suvi_304A', 'suvi_171A', 'suvi_195A','suvi_284A','suvi_131A','suvi_94A', $
                      'swap_172', $
                      'aia_171', 'hmi_blos', $
                      'iris_1400', $
                      'sot_ca', $
                      'xrt_ti']
;n_inst_wave_arr_full = n_elements(inst_wave_arr_full)

;wave_arr_full = inst_wave_arr_full
;for i=0, n_inst_wave_arr_full-1 do $
;   wave_full_arr[i] = strmid(inst_req_arr[i], 0, strpos(inst_req_arr[i],'_'))

if not exist(inst_wave_req_arr) then $
   inst_wave_req_arr = ['suvi_304A', 'suvi_171A', 'suvi_195A','suvi_284A','suvi_131A','suvi_94A']

if keyword_set(do_decon) then begin
   inst_wave_req_arr = ['suvi_304A', 'suvi_284A','suvi_304A', 'suvi_284A']
   decon_arr = [0, 0, 1, 1]
endif

n_inst_wave_req_arr = n_elements(inst_wave_req_arr)

inst_req_arr = inst_wave_req_arr
for i=0, n_inst_wave_req_arr-1 do $
   inst_req_arr[i] = strmid(inst_req_arr[i], 0, strpos(inst_req_arr[i],'_'))
inst_req_arr = all_vals(inst_req_arr)
n_inst_req = n_elements(inst_req_arr)

for j=0, n_inst_req-1 do begin

   case inst_req_arr[j] of

      'suvi': begin
         ss_suvi = where(strpos(inst_wave_req_arr, 'suvi') ne -1, n_suvi)
         wave_arr = inst_wave_req_arr[ss_suvi]
         wave_arr = strmid(wave_arr, strpos(wave_arr[0],'_')+1)
         n_waves = n_suvi

; Set SUVI defaults:
         if not exist(fw1) then fw1 = 'THIN_AL'
         if not exist(exptime) then exptime = '0.95'
         if not exist(exp_string) then exp_string = 'LONG'
         if not exist(img_type) then img_type = 'LIGHT'
         
;        suvi_cat, t0, t1, cat_suvi, files_suvi, search=search
         if not exist(parent) then begin
            if keyword_set(use_oberon) then $
               parent = '/archive1/suvi/suvi_data/fm1/l0/' else $
               parent = '/titan/goes/suvi_backup/fm1/l0/'
         endif

;        files_arr = ssw_time2filelist(parent=parent, t0, t1, pflat=pflat)
;        t0_ex = anytim(t0, /ex)
;        mo0 = string(t0_ex[5],'$(i2.2)') & day0 =string( t0_ex[4],'$(i2.2)')

;                       suvi_fm1_l0_20170701_235010_195_THINAL_OPEN_LIGHT_LONG.fits
         file_string = 'suvi_fm1_l0_2017????_??????_*_*_*_LIGHT_LONG.fits'

;        dirs_arr = file_list(parent + '2017/' + mo0 + '/' + day0, 'H*')
;        files_arr = file_list(dirs_arr, file_string) ; '*.fits')





if exist(t0) then $
   tim_arr = anytim(timegrid(t0, t1, days=1), /ccsds, /date)

if not exist(pid) then pid = '0x032a'

;packet_dir_archive1 = concat_dir('/archive1/suvi/suvi_data/fm1/tlm/packet_ids', pid)
;packet_dir_titan = concat_dir('/titan/goes/suvi/GOES-16/packets', pid)
;packet_dir_alt_titan = concat_dir('/titan/goes/suvi_backup/fm1/tlm/packet_ids', pid)

;fits_dir_archive1 = '/archive1/suvi/suvi_data/fm1/l0'
fits_dir_archive1 = '/titan/goes/suvi_backup/fm1/l0'
dirs_mon_fits = concat_dir(fits_dir_archive1, anytim(tim_arr, /ecs, /date))
n_dirs_mon_fits = n_elements(dirs_mon_fits)
;files_arr_fits = strarr(n_dirs_mon_fits)
for i=0, n_dirs_mon_fits-1 do begin
;  files_fits_mon0 = file_search(concat_dir(dirs_mon_fits[i], '/H*'), 'suvi_fm1_l0_2017*.fits')
   files_fits_mon0 = file_search(concat_dir(dirs_mon_fits[i], '/H*'), file_string)
;  if files_fits_mon0[0] ne '' then files_arr_fits[i] = n_elements(files_fits_mon0) else $
;     files_arr_fits[i] = 0
   if files_fits_mon0[0] ne '' then begin
      if not exist(files_arr) then files_arr = files_fits_mon0 else $
         files_arr = [files_arr, files_fits_mon0]
   endif
endfor





         t_files = anytim(file2time(strmid(file_break(files_arr),12,15)), /ccsds)
         t_files_sec = anytim(t_files)
         wave_files = fix(strmid(file_break(files_arr),28,3))
         
;        mreadfits_header, files_arr, index_arr, only_tags='info_005,info_008,info_014,info_023'

;        um = ssw_uniq_modes(index_arr, 'info_005,info_008,info_014,info_023', mc=mc)
;        um = ssw_uniq_modes(index_arr, 'naxis1,naxis2,summode,exptype,imgtype,fw1,fw2,wavelnth', mc=mc)

         if keyword_set(verbose) then prstr, um+string(mc), /nomore

; -------------------------------------------------------
; Begin block in case this is a 284/304 deconvolution run
; -------------------------------------------------------

; In case 284/304 deconvolution is desired, find all 281/304 pairs for subsequent use:

if keyword_set(do_decon) then begin

   ss_match_284 = where(wave_files eq 284, n_match_284)
   ss_match_304 = where(wave_files eq 304, n_match_304)

   if ( (n_match_284 gt 0) and (n_match_304 ge 0) ) then begin

      files_284 = files_arr[ss_match_284]
      t_files_284 = t_files[ss_match_284]
      t_files_284_sec = t_files_sec[ss_match_284]
      ss_grid_284 = tim2dset(anytim(t_files_284_sec, /ints), anytim(t_grid_sec, /ints))
      ss_grid_284_uniq = ss_grid_284[uniq(ss_grid_284)]
      if keyword_set(uniq_samp) then begin
         t_grid_284 = t_files_284[ss_grid_284_uniq]
         files_grid_284 = files_284[ss_grid_284_uniq]
      endif else begin
         t_grid_284 = t_files_wave[ss_grid_284]
         files_grid_284 = files_284[ss_grid_284]
      endelse
      n_files_284 = n_elements(files_grid_284)

      files_304 = files_arr[ss_match_304]
      t_files_304 = t_files[ss_match_304]
      t_files_304_sec = t_files_sec[ss_match_304]
      ss_grid_304 = tim2dset(anytim(t_files_304_sec, /ints), anytim(t_grid_sec, /ints))
      ss_grid_304_uniq = ss_grid_304[uniq(ss_grid_304)]
      if keyword_set(uniq_samp) then begin
         t_grid_304 = t_files_304[ss_grid_304_uniq]
         files_grid_304 = files_304[ss_grid_304_uniq]
      endif else begin
         t_grid_304 = t_files_wave[ss_grid_304]
         files_grid_304 = files_304[ss_grid_304]
      endelse
      n_files_304 = n_elements(files_grid_304)

      if ( (n_files_284 eq 0) or (n_files_284 ne n_files_304) ) then $
         STOP, 'Mismatch in sampled numbers of 284A and 304A files.  Stopping.'

      n_frames = n_files_284

      mreadfits, files_grid_284, index_grid_284, data_grid_284
      mreadfits, files_grid_304, index_grid_304, data_grid_304

; Crop overscan, etc rows, columns to produce 1280 x 1280 images:
      nx = 1330 & ny = 1292
      data_crop_284 = data_grid_284[30:nx-21, 4:ny-9, *]
      data_crop_304 = data_grid_304[30:nx-21, 4:ny-9, *]

; Correct for readout and CCD rotation:
      data_rot_284 = rotate_3d(data_crop_284,6)
      data_rot_304 = rotate_3d(data_crop_304,6)

; De-vignette:
      data_dvig_284 = data_rot_284
      data_dvig_304 = data_rot_304
      if keyword_set(do_devig) then begin
         file_vig = $
            '/net/topaz/Users/slater/soft/idl_temp/suvi/plt/temp/composite_vig_profile_284_304.fits'
         mreadfits, file_vig_decon, index_vig_mask, data_vig_mask
         for k=0,n_frames-1 do data_dvig_284[0,0,k] = data_rot_284[*,*,k]*data_vig_mask
         for k=0,n_frames-1 do data_dvig_304[0,0,k] = data_rot_304[*,*,k]*data_vig_mask
      endif

; De_convolve 286 and 304 images:

;B284 =  1.865205180868294 * DN284 - 0.154593657689261 * DB304
;B304 = -0.197652640870476 * DN284 + 2.191111504521824 * DN304

;C284 =  1.007532904128063 * DN284 - 0.071086385412153 * DN304
;C304 = -0.106766559147186 * DN284 + 1.007532904128063 * DN304

      a0_284 =  1.865205180868294
      b0_284 = -0.154593657689261
      a0_304 = -0.197652640870476
      b0_304 =  2.191111504521824
 
      a1_284 =  1.007532904128063
      b1_284 = -0.071086385412153
      a1_304 = -0.106766559147186
      b1_304 =  1.007532904128063

      data_decon1_284 = a0_284*data_dvig_284 + b0_284*data_dvig_304
      data_decon1_304 = a0_304*data_dvig_284 + b0_304*data_dvig_304

      data_decon2_284 = a1_284*data_dvig_284 + b1_284*data_dvig_304
      data_decon2_304 = a1_304*data_dvig_284 + b1_304*data_dvig_304

; Optionally de-spike:
      data_284 = data_dvig_284
      data_304 = data_dvig_304
      if keyword_set(do_despike) then begin
         for k=0,n_frames-1 do begin
            data_284[0,0,k] = nospike(data_dvig_284[*,*,k], min=thresh_spike)
            data_304[0,0,k] = nospike(data_dvig_304[*,*,k], min=thresh_spike)

            data_decon1_284[0,0,k] = nospike(data_decon1_284[*,*,k], min=thresh_spike)
            data_decon1_304[0,0,k] = nospike(data_decon1_304[*,*,k], min=thresh_spike)

            data_decon2_284[0,0,k] = nospike(data_decon2_284[*,*,k], min=thresh_spike)
            data_decon2_304[0,0,k] = nospike(data_decon2_304[*,*,k], min=thresh_spike)
         endfor
      endif

; Optionally use median of corner box of 1280 x 1280 x n_frames data cube for image floor:
      if keyword_set(do_floor) then begin
         data_floor_284 = median(data_284[52:(52+5),52:(52+5),*])
         data_floor_304 = median(data_304[52:(52+5),52:(52+5),*])

         data_floor_decon1_284 = median(data_decon1_284[52:(52+5),52:(52+5),*])
         data_floor_decon1_304 = median(data_decon1_304[52:(52+5),52:(52+5),*])

         data_floor_decon2_284 = median(data_decon2_284[52:(52+5),52:(52+5),*])
         data_floor_decon2_304 = median(data_decon2_304[52:(52+5),52:(52+5),*])
; Now subtract that from cube:
         data_284 = data_284 - data_floor_284
         data_304 = data_304 - data_floor_304

         data_decon1_284 = data_decon1_284 - data_floor_decon1_284
         data_decon1_304 = data_decon1_304 - data_floor_decon1_304

         data_decon2_284 = data_decon2_284 - data_floor_decon2_284
         data_decon2_304 = data_decon2_304 - data_floor_decon2_304
      endif

; Scale intensity:
      if keyword_set(do_log) then begin
         data_284 = bytscl(safe_log10(data_284), top=250)
         data_304 = bytscl(safe_log10(data_304), top=250)

         data_decon1_284 = bytscl(safe_log10(data_decon1_284), top=250)
         data_decon1_304 = bytscl(safe_log10(data_decon1_304), top=250)

         data_decon2_284 = bytscl(safe_log10(data_decon2_284), top=250)
         data_decon2_304 = bytscl(safe_log10(data_decon2_304), top=250)
      endif

; Define color table:

; Optionally use current display graphics settings:
      if keyword_set(use_curr_ct) then tvlct, /get, r0, g0, b0

; Optionally use corresponding AIA color table):
      loadct, 1, rgb=rgb_1
      r_284 = reform(rgb_1[*,0]) & g_284 = reform(rgb_1[*,1]) & b_284 = reform(rgb_1[*,2])
;     aia_lct, r_304, g_304, b_304, wave = 304

      if not keyword_set(do_color) then begin
         loadct, 0, rgb=rgb_0
         r_284 = reform(rgb_0[*,0]) & g_284 = reform(rgb_0[*,1]) & b_284 = reform(rgb_0[*,2])
         r_304 = reform(rgb_0[*,0]) & g_304 = reform(rgb_0[*,1]) & b_304 = reform(rgb_0[*,2])
      endif

      if keyword_set(do_stretch) then begin
         restore, '/Users/slater/soft/idl_temp/suvi/plt/temp/ctab0.sav'
         r_284 = r0 & g_284 = g0 & b_284 = b0
         r_304 = r0 & g_304 = g0 & b_304 = b0
      endif

      files_out0_284 = 'suvi_284_' + time2file(t_grid_284, /sec) + '.' + file_suffix
      files_out0_284 = concat_dir(dir_cruiser[0], files_out0_284)

      files_out0_304 = 'suvi_304_' + time2file(t_grid_304, /sec) + '.' + file_suffix
      files_out0_304 = concat_dir(dir_cruiser[0], files_out0_304)

      files_out0_decon1_284 = 'suvi_284_decon1_' + time2file(t_grid_284, /sec) + '.' + file_suffix
      files_out0_decon1_284 = concat_dir(dir_cruiser[0], files_out0_decon1_284)

      files_out0_decon1_304 = 'suvi_304_decon1_' + time2file(t_grid_304, /sec) + '.' + file_suffix
      files_out0_decon1_304 = concat_dir(dir_cruiser[0], files_out0_decon1_304)

      files_out0_decon2_284 = 'suvi_284_decon2_' + time2file(t_grid_284, /sec) + '.' + file_suffix
      files_out0_decon2_284 = concat_dir(dir_cruiser[0], files_out0_decon2_284)

      files_out0_decon2_304 = 'suvi_304_decon2_' + time2file(t_grid_304, /sec) + '.' + file_suffix
      files_out0_decon2_304 = concat_dir(dir_cruiser[0], files_out0_decon2_304)

      if keyword_set(no_write) eq 0 then begin
         for k=0,n_frames-1 do begin

            data0_284_single = reform(data_284[*,*,k])
            data0_304_single = reform(data_304[*,*,k])

            data0_decon1_284_single = reform(data_decon1_284[*,*,k])
            data0_decon1_304_single = reform(data_decon1_304[*,*,k])

            data0_decon2_284_single = reform(data_decon2_284[*,*,k])
            data0_decon2_304_single = reform(data_decon2_304[*,*,k])

            if keyword_set(do_color) then begin
               write_jpeg, files_out0_284[k], truecolor(data0_284_single, r_284, g_284, b_284), true=1, quality=quality
               write_jpeg, files_out0_304[k], truecolor(data0_304_single, r_304, g_304, b_304), true=1, quality=quality

               write_jpeg, files_out0_decon1_284[k], truecolor(data0_decon1_284_single, r_284, g_284, b_284), true=1, quality=quality
               write_jpeg, files_out0_decon1_304[k], truecolor(data0_decon1_304_single, r_304, g_304, b_304), true=1, quality=quality

               write_jpeg, files_out0_decon2_284[k], truecolor(data0_decon2_284_single, r_284, g_284, b_284), true=1, quality=quality
               write_jpeg, files_out0_decon2_304[k], truecolor(data0_decon2_304_single, r_304, g_304, b_304), true=1, quality=quality
            endif else begin
               write_jpeg, files_out0_284[k], data0_284_single, quality=quality
               write_jpeg, files_out0_304[k], data0_304_single, quality=quality

               write_jpeg, files_out0_decon1_284[k], data0_decon1_284_single, quality=quality
               write_jpeg, files_out0_decon1_304[k], data0_decon1_304_single, quality=quality

               write_jpeg, files_out0_decon2_284[k], data0_decon2_284_single, quality=quality
               write_jpeg, files_out0_decon2_304[k], data0_decon2_304_single, quality=quality
            endelse
         endfor
      endif

; Create file names arraty for all files
;     files_out_arr = [files_out0_284, files_out0_304, files_out0_decon1_284, files_out0_decon1_304, files_out0_decon2_284, files_out0_decon2_304]
      files_out_arr = [files_out0_284, files_out0_304, files_out0_decon2_284, files_out0_decon2_304]

;     n_files_arr = [n_frames, n_frames, n_frames, n_frames, n_frames, n_frames]
      n_files_arr = [n_frames, n_frames, n_frames, n_frames]

; Make JSON file:
      if total(n_files_arr) gt 0 then begin

         filnams_arr = file_break(files_out_arr)
         n_img_arr = n_files_arr

;        inst_arr = ['suvi', 'suvi', 'suvi', 'suvi', 'suvi', 'suvi']
         inst_arr = ['suvi', 'suvi', 'suvi', 'suvi']
;        inst_wave_arr = ['suvi_284', 'suvi_304', 'suvi_284_decon1', 'suvi_304_decon1', 'suvi_284_decon2', 'suvi_304_decon2']
         inst_wave_arr = ['suvi_284', 'suvi_304', 'suvi_284_decon2', 'suvi_304_decon2']

         if not keyword_set(no_write) then $
            mk_json2, inst_arr, inst_wave_arr, fovx_ref=fovx_ref, fovy_ref=fovy_ref, $
                      filnams_arr, n_img_arr, dir_top=dir_obs_out, dir_full=dir_cruiser, $
                      url_top=url_obs, url_full=url_cruiser, _extra=_extra

      endif

   endif else begin
      STOP, ' Either no 284 files or no 304 files found.  Stopping.'
   endelse

   n_files_arr = 0

endif else begin

; ---------------------------------------------------
; Begin block in case not a 284/304 deconvolution run
; ---------------------------------------------------

; For each wave, find all files for that wave:
         for i=0, n_waves-1 do begin
            wave0 = wave_arr[i] & wave_num0 = fix(strmid(wave0,0,strlen(wave0)-1))
;           ss_match = where( (strupcase(index_arr.info_014) eq wave0)   and $
;                             (index_arr.info_023 eq exp_string) and $
;                             (index_arr.info_005 eq img_type),  n_match)
            ss_match = where(wave_files eq wave_num0, n_match)
            
            if keyword_set(verbose) then help, files_arr, index_arr, n_match, ss_match

            if n_match gt 0 then begin
               
               if keyword_set(verbose) then help, files_arr[ss_match], ss_match

               files_inst_wave0 = files_arr[ss_match]
;              index_inst_wave0 = index_arr[ss_match]

; For the file set for this wave, match the image times to the time grid:
               t_files_wave = t_files[ss_match]
               t_files_wave_sec = t_files_sec[ss_match]
               ss_grid_samp = tim2dset(anytim(t_files_wave_sec, /ints), anytim(t_grid_sec, /ints))
               ss_grid_samp_uniq = ss_grid_samp[uniq(ss_grid_samp)]
               if keyword_set(uniq_samp) then begin
                  t_grid_samp = t_files_wave[ss_grid_samp_uniq]
                  files_grid_samp = files_inst_wave0[ss_grid_samp_uniq]
               endif else begin
                  t_grid_samp = t_files_wave[ss_grid_samp]
                  files_grid_samp = files_inst_wave0[ss_grid_samp]
               endelse
               
               n_files_samp = n_elements(files_grid_samp)
;              mreadfits_header, files_grid_samp, index_grid_samp, $
;                                only_tags='info_005,info_008,info_014,info_023'

               if not exist(inst_wave_arr) then begin
                  inst_arr = 'suvi'
                  inst_wave_arr = 'suvi_' + wave_arr[i]
                  files_inst_wave = files_grid_samp
                  n_files_arr = n_files_samp
               endif else begin
                  inst_arr = [inst_arr, 'suvi']
                  inst_wave_arr = [inst_wave_arr, 'suvi_' + wave_arr[i]]
                  files_inst_wave = [files_inst_wave, files_grid_samp]
                  n_files_arr = [n_files_arr, n_files_samp]
               endelse



; If reference image does not exist, make the first image the first image of the first instrument
;   the reference:

; Read in all images in sample set:
               mreadfits, files_grid_samp, index_grid_samp, data_grid_samp

; Add wcs header tags to SUVI if necessary:
; KLUGE: For now, assume all images are centered, with plate scale of 2.4 arcsec
               index_grid_samp = add_tag(index_grid_samp, 'cdelt1', 2.4)



; Process all images:

dd = data_grid_samp
n_frames = n_files_samp

wave_suvi_arr_string = ['304A', '284A', '171A', '195A', '131A', '094A']
wave_aia_arr_string  = ['304A', '131A', '171A', '193A', '131A', '094A']
wave_aia_arr         = [ 304,    131,    171,    193,    131,    094]

ss_match = where(wave0 eq wave_suvi_arr_string, ss_match)
wave_aia = wave_aia_arr[ss_match[0]]

; Optionally de-vignette:
if ( keyword_set(do_devig) or keyword_set(do_decon) ) then begin
   file_devig_decon = $
      '/net/topaz/Users/slater/soft/idl_temp/suvi/plt/temp/composite_vig_profile_284_304.fits'
   mreadfits, file_devig_decon, index_decon, data_decon
   for k=0,n_frames-1 do dd[0,0,k] = dd[*,*,k]*data_decon
endif

; Optionally de-spike:
if keyword_set(do_despike) then $
   for k=0,n_frames-1 do $
      dd[0,0,k] = nospike(dd[*,*,k], min=thresh_spike)
;dd = ssw_unspike_cube(ii, dd, threshold=thresh_spike)

; Optionally use median of corner box of 1330 x 1292 x n_frames data cube for image floor:
if keyword_set(do_floor) then begin
   dd_floor = median(dd[52:(52+5),52:(52+5),*])
; Now subtract that from cube:
   dd = dd - dd_floor
endif

dd_min = min(dd)

; Optionally rotate, transpose:
if not keyword_set(no_rot_flip) then rdd = rotate_3d(dd,6) else rdd = dd

; Optionally remove P angle roll:
if keyword_set(do_roll_correct) then $
   for i=0,n_frames-1 do $
      rdd[0,0,k] = rot(rdd[*,*,k], get_rb0p(tt[k], /pang, /deg, missing=dd_min, /quiet))

; Scale intensity:
if keyword_set(do_log) then sdata = bytscl(safe_log10(rdd), top=250) else $
   if keyword_set(do_qtr_power) then sdata = bytscl((rdd>0)^.25, top=250) else $
      sdata = bytscl(rdd)
;  if keyword_set(do_qtr_power) then sdata = bytscl((rdd+min(rdd))^.25, top=250)

if keyword_set(do_sigscale) then sdata = ssw_sigscale(ii, rdd)

;  data_grid_processed = sdata

; Define color table:

; Optionally use current display graphics settings:
if keyword_set(use_curr_ct) then tvlct, /get, r0, g0, b0

; Optionally use corresponding AIA color table):
;if keyword_set(use_aia_ct) then aia_lct, r0, g0, b0, wave = wave_aia ; , /load

if wave0 eq '284A' then begin
   loadct, 1, rgb=rgb_1
   r0 = reform(rgb_1[*,0]) & g0 = reform(rgb_1[*,1]) & b0 = reform(rgb_1[*,2])
;  tvlct, /get, r0, g0, b0
endif

if not keyword_set(do_color) then begin
   loadct, 0, rgb=rgb_0
;  r0 = reform(rgb_1[*,0]) & g0 = reform(rgb_1[*,1]) & b0 = reform(rgb_1[*,2])
   r0 = reform(rgb_0[*,0]) & g0 = reform(rgb_0[*,1]) & b0 = reform(rgb_0[*,2])
;  tvlct, /get, r0, g0, b0
endif

if keyword_set(do_stretch) then begin
   restore, '/Users/slater/soft/idl_temp/suvi/plt/temp/ctab0.sav'
;  r = r0 & g = g0 & b = b0
endif



               files_out0 = 'suvi_' + string(wave0, '(i4.4)') + 'A_' + time2file(t_grid_samp, /sec) + $
                            '.' + file_suffix
               files_out0 = concat_dir(dir_cruiser[0], files_out0)

; Write out images for this wave in desired format (jpeg, gif, etc):
               if keyword_set(no_write) eq 0 then begin
;                 suvi_lct, index0[0], r0, g0, b0, /noload
                  for k=0,n_files_samp-1 do begin
                     case file_type of
                        'jpeg': begin
                           data0_single = reform(sdata[*,*,k])
                           if keyword_set(do_color) then begin
                              datar = r0[data0_single] & datag = g0[data0_single] & datab = b0[data0_single]
;                             datat = [[[datar]],[[datag]],[[datab]]]
                              write_jpeg, files_out0[k], truecolor(data0_single, r0, g0, b0), true=1, quality=quality
                           endif else begin
                              write_jpeg, files_out0[k], data0_single, quality=quality
                           endelse
                        end
                        'gif':  write_gif,  files_out0[k], data0_single, r0, g0, b0
                        else:   write_jpeg, files_out0[k], data0_single, quality=quality
                     endcase
                  endfor
               endif

; Add output file names for this wave to array of file names for all waves:
               if not exist(files_out_arr) then files_out_arr = files_out0 else $
                  files_out_arr = [files_out_arr, files_out0]

; Delete the last index and data arrays, and working variables, before processing another wave:
               delvarx, files_out0, index_grid_samp, data_grid_samp

               print, 'Completed SUVI wave ' + wave0 + '.  ' + strtrim(n_files_samp,2) + ' files written.'

            endif else begin
               print, 'No files found for: ' + 'SUVI ' + wave_sarr[i]
            endelse

         endfor

endelse ; End of SUVI non-284/304 deconvolution case
         
      end

   endcase

endfor

; Make JSON file:

if total(n_files_arr) gt 0 then begin

; This bit of code is for SDO jpegs taken from the SunInTime dirs:
;  dir_in = '/viz2/media/SunInTime' + timefile(anytim(ut_time(), /ccsds))
;  dir_cruiser = '/viz2/media/hv_jp2kwrite/cruiser'
;  filnams_in = ['l0094','l0131','l0171','l0193','l0211','l0304','l0331','l1600','l1700'] + '.jpg'
;; files_arr = file_search(dir_source, 'l????.jpg')
;  filnams_fits = file_search(dir_source, 'AIA*.fits')
;  files_in = concat_dir(dir_in, filnams_in)
;  files_out = concat_dir(dir_cruiser, filnams_in)

   filnams_arr = file_break(files_out_arr)
   n_img_arr = n_files_arr

   if not keyword_set(no_write) then $
      mk_json2, inst_arr, wave_arr, fovx_ref=fovx_ref, fovy_ref=fovy_ref, $
                filnams_arr, n_img_arr, dir_top=dir_obs_out, dir_full=dir_cruiser, $
                url_top=url_obs, url_full=url_cruiser, _extra=_extra

endif

end
