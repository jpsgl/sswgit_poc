;+
pro ConvertToRadiance, iindex, idata, oindex, odata, qdebug=qdebug

;NAME:   ConvertToRadiance
;PURPOSE:
;        Convert image value from DN to radiance (W/m^2-sr)
;CALLING SEQUENCE:
;        SUVI_IMG = ConvertToRadiance(SUVI_IMG)
;INPUTS:
;        SUVI_IMG - SUVI image 
;OUTPUTS:
;        SUVI_IMG_Radiance - SUVI image corrected for:
;                              - gain and linearity
;                              - incident photon conversion, 
;                              - flux conversion
;                              - irradiance conversion
;                              - radiance conversion
;KEYWORDS:
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;                   RWN  May 17, 2011
;        FEB. 2012, ABK, Update to correlate with CDRL80
;        MAR. 2013  ABK  Updated access to exposure time
;	 APR. 2013  DSS	 Moved from double loop over each pixel to array operators
;                        for improved speed
;-

t0=systime(1)
print, '--> 8. Starting ConvertToRadiance.pro (SUVI GPA Routine #8)'

oindex = iindex
odata  = idata

AP_SELECTOR_POS    = iindex.asstatus
FILTER_WHEEL_1_POS = iindex.fw1status
FILTER_WHEEL_2_POS = iindex.fw2status
SUVI_CCD_TEMP      = iindex.ccdtemp1
CCD_READOUT_STATUS = iindex.rdoutport
EXP_TIME           = iindex.cmd_exp_time ; Should this be SHT_EXP_TIME ?

STATIC_PARAMS      = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_static.txt'))
measured_params    = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_measured_params.txt'))
EPHEMERIS          = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_sim_ephemeris.txt'))
ENTRANCE_TRANS     = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_ent_filter_trans.txt'))
FOCAL_PLANE_FILTER_TRANSMISSION $
                   = read_tfile_to_array(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                    'suvi_fw_filter_trans.txt'))
FOCAL_PLANE_FILTER1_TRANSMISSION $
                   = read_tfile_to_array(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                    'suvi_fw1_filter_trans.txt'))
FOCAL_PLANE_FILTER2_TRANSMISSION $
                   = read_tfile_to_array(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                    'suvi_fw2_filter_trans.txt'))
SUVI_LINEARITY     = read_tfile_to_array(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_linearity.txt'))
SUVI_GAIN_LEFT     = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_gain_left_amp.txt'))
SUVI_GAIN_RIGHT    = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_gain_right_amp.txt'))

SN_LEV3            = MEASURED_PARAMS.SUVI_SN_LEV3
FULL_WELL          = MEASURED_PARAMS.SUVI_95_FULL_WELL[0]

;Gain and Linearity Correction (convert DN to electrons)
img_dims = size(odata)
odata_electron  = odata*0.0
odata2          = odata
odata_electron2 = odata*0.0

if (SUVI_CCD_TEMP lt 0) then begin
	approx_temp=abs(SUVI_CCD_TEMP)
	approx_temp=round(approx_temp)
	approx_temp_str='m'+strtrim(approx_temp, 1)
endif else begin
	approx_temp_str=strtrim(round(SUVI_CCD_TEMP), 1)
endelse

case CCD_READOUT_STATUS of
	1: command = 'SUVI_GAIN_CONSTANT = SUVI_GAIN_LEFT.' + approx_temp_str
	2: command = 'SUVI_GAIN_CONSTANT = SUVI_GAIN_RIGHT.'  + approx_temp_str
        else: stop, 'ConvertToRadiance: illegal readout amp value ' + $
                    strcompress(CCD_READOUT_STATUS,/remove)
endcase

success=execute(command)

print, ' SUVI approximate CCD Temperature: ', approx_temp_str
print, ' Location of SUVI_GAIN_CONSTANT: ', command
print, ' SUVI gain constant (based on temperature and amplifier): ', SUVI_GAIN_CONSTANT

;multiply appropriate flat field image with SUVI image

;Copied the following double loop from Ann's "ConvertToRadiance_kap_debug.pro" lines 62-68:
FOR i=0,img_dims(1)-1 DO BEGIN
    FOR j=0,img_dims(2)-1 DO BEGIN
; ONLY FOR TEST PURPOSES, there should be no negative numbers real SUVI data
        IF odata(i,j) LT 0 THEN odata(i,j)=abs(odata(i,j))
        odata_ELECTRON(i,j) = $
          odata(i,j) * SUVI_LINEARITY(round(odata(i,j))) * SUVI_GAIN_CONSTANT ; (DN * e-/DN)
    ENDFOR
ENDFOR

neg=where(odata2 lt 0, nneg)
mid=where(((odata2(*) ge 0) and (odata2(*) le 16384)), nmid)
big=where(odata2 gt 16384, nbig)

if (nneg gt 0) then odata_ELECTRON2(neg) = $
     odata2(neg)*SUVI_LINEARITY(0)*SUVI_GAIN_CONSTANT(0)
if (nmid gt 0) then odata_ELECTRON2(mid) = $
     odata2(mid)*SUVI_LINEARITY(round(odata2(mid)))*SUVI_GAIN_CONSTANT(0)
if (nbig gt 0) then odata_ELECTRON2(big) = $
     odata2(big)*SUVI_LINEARITY(16384)*SUVI_GAIN_CONSTANT(0)

;print, 'Processed '+strcompress(nneg+nmid+nbig,/remove)+' total pixels of 1280x1280 (1638400)'

print, ' SUVI image converted from DN to electrons using' + $
       ' temperature and amplifier dependent gain constant' + $
       ' and pixel location dependent linearity coefficient' + $
       ' (Whole disk sum (Kapusta): ', total (odata_ELECTRON), ').'

print, ' SUVI image converted from DN to electrons using' + $
       ' temperature and amplifier dependent gain constant' + $
       ' and pixel location dependent linearity coefficient' + $
       ' (Whole disk sum (DSS): ', total (odata_ELECTRON2), ').'

print, ' Continuing using Kapusta value for gain*linearity conversion.'

SN_LEV3 = SN_LEV3 * SUVI_LINEARITY(SN_LEV3) * SUVI_GAIN_CONSTANT
FULL_WELL = FULL_WELL * SUVI_LINEARITY(FULL_WELL) * SUVI_GAIN_CONSTANT

;Incident Photon Conversion (convert electrons to photons)
PHOT_ELE_CONV = STATIC_PARAMS.PHOT_ELEC_CONVERSION(AP_SELECTOR_POS)
QE = STATIC_PARAMS.SUVI_QE(AP_SELECTOR_POS)

odata_PHOTON = odata_ELECTRON / (PHOT_ELE_CONV/QE)   ; (e- / e-/photon)
SN_LEV3 = SN_LEV3/(PHOT_ELE_CONV/QE)
FULL_WELL = FULL_WELL/(PHOT_ELE_CONV/QE)

print, ' Photon-electron conversion: ', PHOT_ELE_CONV
print, ' Quantum efficiency: ', QE
print, ' SUVI image converted to incident photons using' + $
       ' photon-electron conversion and CCD specific Quantum Efficiency' + $
       ' (Whole disk sum: ', total(odata_PHOTON), ').'

;Flux Conversion (convert photon to J/s)
PHOT_ENG_CONV= STATIC_PARAMS.PHOT_ENG_CONVERSION(AP_SELECTOR_POS)

odata_FLUX = odata_PHOTON *PHOT_ENG_CONV / EXP_TIME   ;(photon * J/photon /s)
SN_LEV3=SN_LEV3 *PHOT_ENG_CONV /EXP_TIME
FULL_WELL=FULL_WELL *PHOT_ENG_CONV /EXP_TIME

print, ' Photon-energy conversion coefficient (wavelength dependent): ', PHOT_ENG_CONV
print, ' SUVI image converted to flux (Whole disk sum: ', total(odata_FLUX), ').'

;Irradiance Conversion (convert J/s to W/m^2)
SUVI_EFFA = STATIC_PARAMS.SUVI_GEOM_AREA * $
            STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS) * $
            STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS) * $
            ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS) * $
            FOCAL_PLANE_FILTER_TRANSMISSION(FILTER_WHEEL_1_POS, FILTER_WHEEL_2_POS)

if keyword_set(qdebug) then begin
	print, ''
        print, 'SUVI_EFFA = STATIC_PARAMS.SUVI_GEOM_AREA * '
        print, '            STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS) * '
        print, '            STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS) * '
        print, '            ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS) * '
        print, '            FOCAL_PLANE_FILTER_TRANSMISSION(FILTER_WHEEL_1_POS, FILTER_WHEEL_2_POS)'
        print, ''
        print, ' AP_SELECTOR_POS: ' + strtrim(AP_SELECTOR_POS,2)
	print, ' FILTER_WHEEL_1_POS: ' + strtrim(FILTER_WHEEL_1_POS,2)
        print, ' FILTER_WHEEL_2_POS: ' + strtrim(FILTER_WHEEL_2_POS,2)
	print, ' STATIC_PARAMS.SUVI_GEOM_AREA: '+ strcompress(STATIC_PARAMS.SUVI_GEOM_AREA,/remove)
        print, ' STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS): ' + $
                   strcompress(STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS),/remove)
        print, ' STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS): ' + $
                   strcompress(STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS),/remove)
        print, ' ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS): ' + $
                   strcompress(ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS),/remove)
        print, ' FOCAL_PLANE_FILTER_TRANSMISSION(FILTER_WHEEL_1_POS, FILTER_WHEEL_2_POS): ' + $
                   strcompress(FOCAL_PLANE_FILTER_TRANSMISSION(FILTER_WHEEL_1_POS, FILTER_WHEEL_2_POS),/remove)
	print, ''
endif

odata_IRRADIANCE = odata_FLUX / (SUVI_EFFA[0]) ;(J/s / m^2) or (W/m^2)
SN_LEV3=SN_LEV3/(SUVI_EFFA[0])
FULL_WELL=FULL_WELL/(SUVI_EFFA[0])

print, ' SUVI effective area: ', SUVI_EFFA
print, ' SUVI image converted to irradiance(Whole disk sum: ', total(odata_IRRADIANCE), ').'

;Radiance Conversion

odata = odata_IRRADIANCE / STATIC_PARAMS.SOLID_ANG[0]   ; (J/m^2 -sr-s) or (W/m^2 -sr)
SN_LEV3=SN_LEV3/ STATIC_PARAMS.SOLID_ANG[0]
FULL_WELL=FULL_WELL/ STATIC_PARAMS.SOLID_ANG[0]

print, ' SUVI solid angle: ', STATIC_PARAMS.SOLID_ANG[0]
print, ' SUVI image converted to radiance (Whole disk sum: ', total(odata_IRRADIANCE), ').'

print, ' SUVI Signal to Noise Level 3 converted to radiance: ', SN_LEV3
print, ' SUVI Full Well converted to radiance: ', FULL_WELL

t1=systime(1)
print, ' ConvertToRadiance took: ', t1-t0, ' seconds'

END
