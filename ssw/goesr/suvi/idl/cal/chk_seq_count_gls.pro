
;+
; pro chk_seq_count, fn
; program to check for discontinuitits in CCSDS sequence counter ;
;-

pro chk_seq_count, fn=fn, apid=apid, tsec_arr=tsec_arr

if get_logenv('DIR_APIDS') eq '' then $
   set_logenv, 'DIR_APIDS', '/archive1/suvi/suvi_data/fm1/apids'

if ( keyword_set(interactive) or (not exist(fn)) ) then begin
   paths_apid = file_list(get_logenv('DIR_APIDS'), '0x*a')
   pathnams_apid = file_break(paths_apid)
   ss_sel = xmenu_sel(pathnams_apid)
   pathnam_apid = pathnams_apid[ss_sel]
   path_apid = paths_apid(ss_sel)

   files_apid = file_list(path_apid, '*.' + pathnam_apid)
   filnams_apid = file_break(files_apid)
   ss_sel = xmenu_sel(filnams_apid)
   fn = files_apid(ss_sel)
endif

get_lun, flun
openr, flun, fn
x = assoc( flun, bytarr(20) )
a = x(0)
; print, a, format='(20z)'
pkt_len = a(4) * 256 + a(5)
r = assoc( flun, bytarr( pkt_len + 7 ))

seq_count = (a(2) and '3f'x) * 256 + a(3)

; Extract image time and define various time parameters:
daynum = a[06]*65536l    + a[07]*256l   + a[08]
msec   = a[09]*16777216l + a[10]*65536l + a[11]*256l + a[12] 
usec   = a[13]*256l      + a[14] 
mjd    = {mjd:daynum, time:msec}
t_pkt  = anytim(mjd, /ccsds)
t_sec  = anytim(mjd)
t_sec_arr = t_sec

num_discontinuities = 0
i = 1l 

while not(eof(flun)) do begin
   a = r(i) 
;  print, i
   seq_count_new = (a(2) and '3f'x) * 256 + a(3)
   if (seq_count_new ne (seq_count + 1)) then begin
      print, i, seq_count, seq_count_new
      num_discontinuities = num_discontinuities + 1
   endif
   seq_count = seq_count_new

; Handle rollovers:
   if (seq_count eq 16383) then seq_count = -1
   i = i + 1

; Extract image time and define various time parameters:
   daynum = a[06]*65536l    + a[07]*256l   + a[08]
   msec   = a[09]*16777216l + a[10]*65536l + a[11]*256l + a[12] 
   usec   = a[13]*256l      + a[14] 
   mjd    = {mjd:daynum, time:msec}
   t_pkt  = anytim(mjd, /ccsds)
   t_sec  = anytim(mjd)
   t_sec_arr = [t_sec_arr, t_sec]
STOP
endwhile

print, num_discontinuities, format='("Number of discontinuities = ", i6)'

free_lun, flun

end

