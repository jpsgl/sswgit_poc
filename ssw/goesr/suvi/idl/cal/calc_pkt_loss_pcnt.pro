
function calc_pkt_loss_pcnt, n_pkt_lost_tot_arr=n_pkt_lost_tot_arr

buff1a = rd_tfile('./pkt_stats_ver3a.txt',4)
pktfil_arr_0 = reform(buff1a[0,*])
n_pkt_arr = reform(buff1a[1,*])
n_gap = reform(buff1a[2,*])
ss_sort = sort(pktfil_arr_0)
pktfil_arr_0 = pktfil_arr_0[ss_sort]
n_pkt_arr = n_pkt_arr[ss_sort]
n_gap = n_gap[ss_sort]

buff2a = rd_tfile('./pkt_stats_ver3b.txt',3)
pktfil_arr = reform(buff2a[0,*])
n_pkt_lost_arr = reform(buff2a[2,*])

uniq_pkt_files = all_vals(pktfil_arr)
n_uniq = n_elements(uniq_pkt_files)
n_pkt_lost_tot_arr = dblarr(n_uniq)
pcnt_loss_arr = dblarr(n_uniq)

for i=0,n_uniq-1 do begin
   ss_match = where(pktfil_arr eq uniq_pkt_files[i], n_match)
   n_pkt_lost_tot_arr[i] = total(double(n_pkt_lost_arr[ss_match]))
   pcnt_loss_arr[i] = 100d*total(double(n_pkt_lost_arr[ss_match]))/double(n_pkt_arr[i])
endfor

;prstr, pktfil_arr_0 + '   ' + uniq_pkt_files + '   ' + $
;       n_pkt_arr + '   ' + n_gap + '   ' + string(n_pkt_lost_tot_arr,'$(i6.5)') + '   ' + $
;       string(pcnt_loss_arr,'$(f9.3)')
 prstr, pktfil_arr_0 + '   ' + $
        n_pkt_arr + '   ' + n_gap + '   ' + string(n_pkt_lost_tot_arr,'$(i6.5)') + '   ' + $
        string(pcnt_loss_arr,'$(f9.3)'), /nomore
STOP
return, pcnt_loss_arr

end

