pro suvi_mfits, fname, $
	stable = stable, range=range, size=size

;+
;
; Run continuously to process images as they call in (sits in a loop calling suvi_img)
;
; $Id: suvi_mfits.pro 487 2015-10-01 17:01:50Z suvitc $
; $Date: 2015-10-01 10:01:50 -0700 (Thu, 01 Oct 2015) $
; $Revision: 487 $
; $Author: suvitc $
; $HeadURL: svn+ssh://suvitc@suvifsw1.atc.lmco.com/home/suvisvn/idltc/suvi_mfits.pro $
;
; INPUT:
;	fname	-	the name of the packet file to run on
;
; KEYWORD PARAMETERS:
;	/size   - if set, select file and get size of packet file in MBytes and approximate number of images
;	    example:
;	      suvi_mfits,/size             ;print size of packet file and fits image names at default locations 20%, 40%, 60%, 80%
;             suvi_mfits,range=0.5,/size   ;print the name of the fits file halfway through the packetfile
;             suvi_mfits,range=[0.5,0.7321],/size   ;print the fits file at 50% and 73.21% locations of the selected packetfile
;	      suvi_mfits,range=[0,1],/size ;print the first and last image names of the packetfile
;
;       /range	- if set to a fraction, will start ripping starting at that section of the packet file
;               - if set to a 2 element array, the first fraction is the start, the second is the end of the rip
;                        if the second is 1, will continue to rip waiting for new packets
;           example:
;             suvi_mfits,range=0.5        ;start ripping halfway through the packetfile, and continue processing more packets
;             suvi_mfits,range=[0.5,0.7]   ;rip from 0.5 to 0.7 and stop
;             suvi_mfits,range=[0.6,1] ; same as suvi_mfits,range=0.6 ; start ripping at 60%, and continue processing more packets
;
;	/stable -	if set to zero, then don't wait for the packet size to stabilize before
;				ripping a new batch of images (if you're using takepic commands)
;
; History:
;   06-Aug-15, Shing, made better use of /range and /size keywords and also print fits image names.
;   01-Aug-15, Shing, added /range keyword to pass on to suvi_img to select a range to extract
;                     added /size keyword to simply get the file size in MBytes and ~images
;		      went back to stable=0 as default due to fact that when seqencer is continuously running, there are no pauses in the packet stream
;			for a quiet/stable pause between images.
;   28-Jul-15, Shing. Replace SPAWN commands that gets file size with IDL's file_info() function
;			reduce wait state from 5s to 0.2s since file_info() is much faster than spawning a shell and performing an ls -altr command
;			change default mode to stable=1 so that the section checking filesize is executed. (previously was not checking!)
;			remove sections that reopened LUN, which is used in suvi_img
;           added a wait state to be nicer in the infinite loop
;   19-Sep-13, Mateos. Replaced hard coded path in imgpath and statfile with
;     path derived from $GSE_PKT_FILE_DIR.
;	07-Feb-13, Anonymous, Attempted to fix the FOOBAR code for open/close on files 
;-

common suvi_img_common, filename, splitna, recnum, statfile, statlun, lun, idate

	
if N_ELEMENTS(stable) eq 0 then stable = 0 ;;default to 0 so filesize is NOT checked when looping
print,'New, Faster looping!!!!!',stable
;imgpath = '/net/em1a/disk1/egsesw/log/packets/0x032a/'
imgpath = getenv('GSE_PKT_FILE_DIR') + '/0x032a/'
CD, imgpath, current = old_dir
if N_ELEMENTS(fname) eq 0 then fname = DIALOG_PICKFILE (/READ, Filter = '*.0x032a', path = imgpath)
filename = fname			; restarting 
recnum = 0
if fname eq '' then goto,last_stop ; abort procedure gracefully

; Run suvi_img
; Check the size of the file
; Wait 0.01 seconds, then check it again.
; if different, then go to a new loop
; if the same, then wait
PRINT, 'Press any key to exit...'
PRINT, 'Reading from Pkt File: ',fname
filename = fname
na = strsplit(/extract, fname,'_')
if (n_elements(na) eq 2) then begin
	splnas = strsplit(/extract, na[1],'.')
	splitna = splnas[0] 			; hhmmss
endif else begin
	splitna = ""
endelse

; Setup Stats logging:
append = 0
if N_ELEMENTS(statfile) gt 0 then begin
	if FILE_EXIST(statfile) then begin
      append = 1
	endif
endif
if not append then begin
	;statfile = '~/stats/' + STIM_DATE(/time) + '_suviimg_stats.txt'
	statfile = getenv('HOME') + '/stats/' + STIM_DATE(/time) + $
       '_suviimg_stats.txt'
	OPENW, statlun, /get_lun, statfile
	PRINTF, statlun, 'ApID', 'Date', 'Time', 'TAI', 'SerialNo', 'Naxis1', 'Naxis2', $
      'Mean', 'Median', 'Min', 'Max', 'StdDev', 'InMax', 'InMean', 'InStdDev', $
      format = '(a10, a11, a9, a14, a10, 10a12)'
;    CLOSE, statlun
    FREE_LUN, statlun ; close above closes file but does not release the lun	
endif

;++++++++++++++++++++++++++
; Select a packet file
;--------------------------

;;;if N_ELEMENTS(size) eq 0 then begin

 OPENR, lun, fname, /get_lun
; date rollover check
 utnow = ut_time(/ex)
 idat = anytim(utnow,/ext)
 idate = string(format='(i4,i2.2,i2.2)',idat[6],idat[5],idat[4]);yyyymmdd
free_lun,lun


if N_ELEMENTS(size) eq 0 then begin

case N_ELEMENTS(range) of
	0: range=[0,1]
	1: range=[range,1]
	else: ;
	endcase

;pktdir = '/net/em1a/disk1/egsesw/log/'
;imgpath = pktdir + 'packets/0x032a/'
;fitspath = pktdir + 'fits/'
;CD, imgpath, current = old_dir
;if N_ELEMENTS(pktfname) eq 0 then $
;   pktfname = DIALOG_PICKFILE (/READ, Filter = '*.0x032a', path = imgpath)
;if N_ELEMENTS(filename) eq 0 then filename = pktfname
;if pktfname ne filename then begin      ; reset the common block,
;                              ; because you're looking at a new file
;    recnum = 0
;    filename = pktfname
;endif
;OPENR, lun, pktfname, /get_lun

 SUVI_IMG, fname, range=range
 print,'first SUVI_IMG done!'
 if (fname ne filename) then fname = filename
;;a = fstat(lun)
;;if (a.open eq 0) then begin 	; unit closed reopen
;;	OPENR, lun, fname, /get_lun
;;print,'HAD TO REOPEN'
;;endif

 qdone = 0
end else begin ;just check the size of the selected file
	qdone=1
	sz=(file_info(fname)).size/1000000.
	print,''
	print,''
	print,''
	print,'Selected packet file in MByte is ',sz
	print,'Approximate # of images in selected packetfile is ',long(sz/3.428) ;we don't know the exact number since images could be different sizes
	print,'    range    approx. image#'
	
	if N_ELEMENTS(range) eq 0 then begin
		for i=0.2,0.8,0.2 do begin
		recnum=2
		print,''
		print,''
		print,''
		print,format='(f8.1,i10)',i,long(sz/3.428*i)
		suvi_img,fname,range= [0-i,1] ;call suvi_img a default range of negative indices to get the filenames there
		end
	  endif else begin
	  	for i=0,N_ELEMENTS(range)-1 do begin
		recnum=2
		print,''
		print,''
		print,''
		print,format='(f8.3,i10)',range(i),long(sz/3.428*range(i))
		rng = 0-range(i)
		if rng eq 0 then rng= -.00001 ;close to zero, but still negative
		if rng lt -1 then rng = -.99999 ; close to -1 but not quite so we do not hit the end of file
		suvi_img,fname,range= [rng,1] ;call suvi_img with a negative argument to get the file name at the position	
		end
		endelse
	goto,last_stop
endelse


;print, 'Waiting', format='($,a)'
while ((not qdone) and (recnum gt 0))do begin
	kb_inp = STRLOWCASE(GET_KBRD(0))
	case kb_inp of
		'': begin
			    ;SPAWN, 'ls -altr ' + fname, result, errcode ;;;;;; replace spawn command with IDL's file_info function
			    ;lsres = STRSPLIT(/extract, result, ' ')
			    finfores=file_info(fname)
			    nfsize = finfores.size  ;;; LONG(lsres[4])
				if N_ELEMENTS(fsize) eq 0 then fsize = nfsize
			    if nfsize ne fsize then begin
			        ; It's changing; wait for it to stabilize, then read
					if stable then begin
						print_cntr=0 ; no need to print every 0.2s loop
				        while nfsize ne fsize do begin
				        	fsize = nfsize
							print_cntr += 1
				        	;SPAWN, 'ls -altr ' + fname, result, errcode ;;;;;; replace spawn command with IDL's file_info function
				        	;lsres = STRSPLIT(/extract, result, ' ')
				        	PRINT, 'Waiting for file to stabilize...', nfsize
							WAIT, 0.2 ;; IDL's file_info is fast, so reduce wait state to 0.2s (was 5s!)
										;; file_info seems to need 0.2s to work properly to catch packet file size change
							finfores=file_info(fname)
				        	nfsize = finfores.size  ;;; LONG(lsres[4])
						print,'nfsize=',nfsize
				        endwhile
					endif
			        SUVI_IMG, fname
					print,'SUVI_IMG done! lun=',lun,recnum
					if (recnum lt 0) then qdone=1
					if (fname ne filename) then fname = filename
;;					a = fstat(lun)
;;					if (a.open eq 0) then begin 	; unit closed reopen
;;						OPENR, lun, fname, /get_lun
;;                       print,'HAD TO REOPEN HERE TOO'
;;					endif
			        fsize = nfsize
			    endif
				wait,0.5 ;; be a little nicer in this infinite loop
			end
		else: begin
				qdone = 1
			end
	endcase
endwhile


Free_lun, lun

last_stop:
PRINT, 'Exiting SUVI_MFITS'
CD, old_dir


end
