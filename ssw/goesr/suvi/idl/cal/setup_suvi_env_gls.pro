
pro setup_suvi_env

if get_logenv('SUVI_TOP') eq '' then set_logenv, 'SUVI_TOP', '/archive1/suvi'
if get_logenv('SUVI_INSTRUMENT') eq '' then set_logenv, 'SUVI_INSTRUMENT', 'EM1'
if get_logenv('SUVI_DATA') eq '' then $
   set_logenv,'SUVI_DATA', $
     concat_dir(concat_dir(get_logenv('SUVI_TOP'), get_logenv('SUVI_INSTRUMENT')), 'data')
if get_logenv('SUVI_META') eq '' then $
   set_logenv,'SUVI_META', $
     concat_dir(concat_dir(get_logenv('SUVI_TOP'), get_logenv('SUVI_INSTRUMENT')), 'meta')
if get_logenv('SUVI_CATS') eq '' then $
   set_logenv,'SUVI_CATS', concat_dir('SUVI_META', 'suvi_cats')
if get_logenv('SUVI_LOGS') eq '' then $
   set_logenv,'SUVI_LOGS', concat_dir('SUVI_LOGS', 'suvi_logs')

; Define directory paths:
if get_logenv('SUVI_DATA') eq '' then set_logenv, 'SUVI_DATA', '~/suvi_data'
if get_logenv('SUVI_TLM')  eq '' then $
  set_logenv, 'SUVI_TLM', concat_dir(get_logenv('SUVI_DATA'), 'tlm')
if get_logenv('SUVI_L0')   eq '' then $
  set_logenv, 'SUVI_L0',  concat_dir(get_logenv('SUVI_DATA'), 'l0')
if get_logenv('SUVI_L1')   eq '' then $
  set_logenv, 'SUVI_L1',  concat_dir(get_logenv('SUVI_DATA'), 'l1')

if not exist(dir_tlm) then dir_tlm = get_logenv('SUVI_TLM')
if not exist(dir_l0)  then dir_l0  = get_logenv('SUVI_L0')
if not exist(dir_l1)  then dir_l1  = get_logenv('SUVI_L1')



