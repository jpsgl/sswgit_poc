
pro suvi_make_dark, pktfil, n_iter=n_iter, use_prev=use_prev, do_display=do_display

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     SUVI_MAKE_DARK
; CATEGORY:
;     Calibration
; PURPOSE:
;     Combines a series of dark frames taken at different times and temperatures
;     and determines a master dark frame as well as estimates of the dark current
;     and pedestal as s function of temperature.
; CALLS:
;     suvi_make_dark
; METHOD:
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     Set of FITS files from an LTC sequence.
; OUTPUTS:
;     Calculated gain.
; KEYWORD INPUTS:
; KEYWORD OUTPUTS:
; CALLED BY ROUTINES:
; ROUTINES CALLED:
; TRAPPED ERRORS:
; UNTRAPPED ERRORS:
; NOTA BENA:
; TODO:
; DEVELOPMENT STATUS:
; TEST AND VERIFICATION DESCRIPTION:
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION History:
;     2016-04-07 GLS - Updates to doc header.
; VERSION CONTROL STATUS:
;     2016_04-07 - Last commit.
;-
; ==============================================================================

if not exist(pktfil) then $
   pktfil = '/archive1/suvi/suvi_data/fm1/tlm/20130617.0x032a'
if not exist(dir_top) then $
   dir_top = '/archive1/suvi/suvi_data'
dir_dark = concat_dir(dir_top, 'fm1/suvi_darks')
if not exist(do_display) then do_display = 1

;cmd = './suvi_make_dark' + ' -i ' + files_list + ' -niter ' + string(n_iter, '(i2.2)') + $
;      ' -w 1.2' + ' - r 1.414' + ' -o ' + outfil_flat

cmd = './make_dark -i ' + pktfil + ' -o ' + dir_top + ' -ndarks 2 -e 1000'

if not keyword_set(use_prev) then $
   spawn, cmd

outfil_dark = file_list(dir_dark, 'suvi_' + strmid(file_break(pktfil),0,8) + '*.fits')

if keyword_set(do_display) then begin
   wdef, 0, 1280/2, 1280/2
   mreadfits, outfil_dark, index_dark, data_dark
   tvscl, rebin(data_dark, 1280/2, 1280/2, /samp)
endif

end
