
pro go_suvi_make_dark, _extra=_extra

  ans = ''
  while ans ne 'n' do begin
     wdelete, 0

     files   = file_list(get_logenv('SUVI_TLM'), '*.0x032a')
     filnams = file_break(files)
     ss_fil  = xmenu_sel(filnams)
     pktfil  = files(ss_fil)

     suvi_make_dark, pktfil
     read, 'Make another composite dark? (def is yes) : ', ans
  endwhile
  
end

