
pro CorrectContaminationSignalLoss, iindex, idata, oindex, odata

;+
;NAME: CorrectContaminationSignalLoss
;PURPOSE:
;      Correct image values for contamination signal loss 
;CALLING SEQUENCE:
;      CorrectContaminationSignalLoss(iindex, idata, oindex, odata)
;INPUTS:
;      iindex - Input header structure for SUVI image
;      idata - Input SUVI image
;OUTPUTS:
;      oindex - Updated output header structure for SUVI image
;      odata - Output SUVI image corrected for contamination signal loss
;KEYWORDS:
;PROCEDURE CALLS:
;MODIFICATION HISTORY:
;     Feb. 2011, ABK (Created)
;     Aug. 2011, RWN (Basic part written)
;     Feb. 2012, NVN (Modified to follow new philosophy)
;     Mar. 2013  ABK Updated for exposure time reading
;     Aug. 2016  GLS Corrected SUVI_CONTAM_THICKNESS_284 and SUVI_CONTAM_THICKNESS_304
;-

COMMON LOGGING, LOGID, QDEBUG
COMMON SUVI_GPDS, ISP, ISP_MAP, INFO
COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT,STATIC_PARAMS, $
       MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, FOCAL_PLANE_FILTER_TRANSMISSION, $
       SUVI_LINEARITY, SUVI_GAIN_RIGHT, SUVI_GAIN_LEFT
COMMON OUT_METADATA, DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, $
       SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, PHOT_ENG_CONV, $
       ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, SN_LEV3, FULL_WELL, SHT_EXP_TIME, $
       IMG_PKT_TIME 

t0=systime(1)
print, '--> 9. Starting CorrectContaminationSignalLoss.pro (SUVI GPA Routine #9)'

SUVI_IMG = idata

AP_SELECTOR_POS    = iindex.asstatus
FILTER_WHEEL_1_POS = iindex.fw1status
FILTER_WHEEL_2_POS = iindex.fw2status
SUVI_CCD_TEMP      = iindex.ccdtemp1
CCD_READOUT_STATUS = iindex.rdoutport
EXP_TIME           = iindex.cmd_exp_time ; Should this be SHT_EXP_TIME ?

STATIC_PARAMS      = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_static.txt'))
measured_params    = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_measured_params.txt'))
EPHEMERIS          = read_tfile_to_struct(concat_dir(get_logenv('SUVI_CAL_DATA'), $
                                                     'suvi_sim_ephemeris.txt'))

SUVI_TOT_IRRADIANCE = total(SUVI_IMG* STATIC_PARAMS.SOLID_ANG[0])

time=anytim(info.img_pkt_time,/int)
tbake=anytim(info.time_last_bakeout,/int)
AP_SELECTOR_POS=info.asstatus
EXP_TIME=info.sht_exp_time

e284file='$SUVI_CAL_DATA'+'/irradiance/EXIS_284.txt'
out=rd_tfile(e284file,3)
e284d=reform(out[0,*])
e284t=reform(out[1,*])
e284time=e284d+' '+e284t
e284flux=reform(float(out[2,*]))
e304file='$SUVI_CAL_DATA'+'/irradiance/EXIS_304.txt'
out=rd_tfile(e304file,3)
e304d=reform(out[0,*])
e304t=reform(out[1,*])
e304time=e304d+' '+e304t
e304flux=reform(float(out[2,*]))

; for SUVI, determine if the last bakeout took place on the same day
if (anytim(time,/int)).day eq (anytim(tbake,/int)).day then goto, sameday 

s284file='$SUVI_CAL_DATA'+'/irradiance/SUVI_284.txt'
out=rd_tfile(s284file,3)
s284d=reform(out[0,*])
s284t=reform(out[1,*])
s284time=s284d+' '+s284t
s284flux=reform(float(out[2,*]))
s304file='$SUVI_CAL_DATA'+'/irradiance/SUVI_304.txt'
out=rd_tfile(s304file,3)
s304d=reform(out[0,*])
s304t=reform(out[1,*])
s304time=s304d+' '+s304t
s304flux=reform(float(out[2,*]))

ndays=min([81,  (anytim(time,/int)).day - (anytim(tbake,/int)).day])
bs284=where(int2secarr(s284time, time)/86400. ge -2 and int2secarr(s284time, time)/86400. lt -1)
if ndays eq 1 then sss284=bs284[0] else sss284=(bs284[0]-indgen(ndays))
realdata=where(sss284 GE 0) ; checks to see if there are fewer days of real data than the time since last bakeout, takes care of beginning of mission
sss284=sss284(realdata) 
SUVI_284=average(s284flux[sss284])
bs304=where(int2secarr(s304time, time)/86400. ge -2 and int2secarr(s304time, time)/86400. lt -1)
if ndays eq 1 then sss304=bs304[0] else sss304=(bs304[0]-indgen(ndays))
realdata=where(sss304 GE 0) ; checks to see if there are fewer days of real data than the time since last bakeout, takes care of beginning of mission
sss304=sss304(realdata)  
SUVI_304=average(s304flux[sss304])
print, '   Number of days since last bakeout: ', ndays
print, n_elements(sss284),' days of SUVI daily irradiance averaged'
print, '         SUVI averaged irradiance for the 284 channel over ', n_elements(sss284), ' days is: ', SUVI_284
print, '         SUVI averaged irradiance for the 304 channel over ', n_elements(sss304), ' days is: ', SUVI_304
goto, EXIS_calc

sameday:
printf, logid, 'Image taken on the same day a bake out ends.  Read SUVI irradiance'
s284file='$SUVI_CAL_DATA'+'/temp_irradiance/SUVI_284.txt'
out=rd_tfile(s284file,3)
s284d=out[0]
s284t=out[1]
s284time=s284d+' '+s284t
SUVI_284=float(out[2])
s304file='$SUVI_CAL_DATA'+'/temp_irradiance/SUVI_304.txt'
out=rd_tfile(s304file,3)
s304d=out[0]
s304t=out[1]
s304time=s304d+' '+s304t
SUVI_304=float(out[2])
print, '         SUVI irradiance for the 284 channel over is: ', SUVI_284
print, '         SUVI irradiance for the 304 channel over is: ', SUVI_304

;Collect EXIS Daily Average Data
EXIS_calc:
be284=where(int2secarr(e284time, time)/86400. ge -2 and int2secarr(e284time, time)/86400. lt -1)
if ndays eq 1 then sse284=be284[0] else sse284=(be284[0]-indgen(ndays))
realdata=where(sse284 GE 0) ; checks to see if there are fewer days of real data than the time since last bakeout, takes care of beginning of mission
sse284=sse284(realdata) 
EXIS_284=average(e284flux[sse284])
be304=where(int2secarr(e304time, time)/86400. ge -2 and int2secarr(e304time, time)/86400. lt -1)
if ndays eq 1 then sse304=be304[0] else sse304=(be304[0]-indgen(ndays))
realdata=where(sse304 GE 0) ; checks to see if there are fewer days of real data than the time since last bakeout, takes care of beginning of mission
sse304=sse304(realdata)
EXIS_304=average(e304flux[sse304])
print, n_elements(sse284),' days of EXIS daily irradiance averaged'
print, '         EXIS averaged irradiance for the 28.4nm channel over ', n_elements(sse284), ' days is: ', EXIS_284
print, '         EXIS averaged irradiance for the 30.4nmn channel over ', n_elements(sse304), ' days is: ', EXIS_304

data_assigned:
SUVI_EXIS_LOSS_RATIO_284 = SUVI_284 / EXIS_284
SUVI_CONTAM_THICKNESS_284 = ALOG(SUVI_EXIS_LOSS_RATIO_284) / (-0.00178)
SUVI_EXIS_LOSS_RATIO_304 = SUVI_304 / EXIS_304
SUVI_CONTAM_THICKNESS_304 = ALOG(SUVI_EXIS_LOSS_RATIO_304) / (-0.00204)
SUVI_CONTAM_THICKNESS = (0.7*SUVI_CONTAM_THICKNESS_284 + 0.3*SUVI_CONTAM_THICKNESS_304)    
case AP_SELECTOR_POS of
0: SUVI_CONTAM_LOSS = exp(-0.0002  * SUVI_CONTAM_THICKNESS)  ;94A
1: SUVI_CONTAM_LOSS = exp(-0.00041 * SUVI_CONTAM_THICKNESS)  ;131A
2: SUVI_CONTAM_LOSS = exp(-0.00069 * SUVI_CONTAM_THICKNESS)  ;171A
3: SUVI_CONTAM_LOSS = exp(-0.00089 * SUVI_CONTAM_THICKNESS)  ;195A
4: SUVI_CONTAM_LOSS = exp(-0.00178 * SUVI_CONTAM_THICKNESS)  ;284A
5: SUVI_CONTAM_LOSS = exp(-0.00204 * SUVI_CONTAM_THICKNESS)  ;304A
endcase
print, '    SUVI to EXIS Loss Ratio for the 284A channel: ', SUVI_EXIS_LOSS_RATIO_284
print, '    Estimated contamination thickness on the 284A channel: ', SUVI_CONTAM_THICKNESS_284
print, '    SUVI to EXIS Loss Ratio for the 3044A channel: ', SUVI_EXIS_LOSS_RATIO_304
print, '    Estimated contamination thickness on the 304A channel: ', SUVI_CONTAM_THICKNESS_304  
print, '    Estimated contamination thickness for all channels: ', SUVI_CONTAM_THICKNESS
print, '    Estimated loss due to contamination for the signal channel: ', SUVI_CONTAM_LOSS    

SUVI_SIGNAL_LOSS_CORRECTED_IMG = SUVI_IMG / SUVI_CONTAM_LOSS

print, '   SUVI image radiance before correction: ', total(SUVI_IMG)
print, '   SUVI image radiance after correction: ', total(SUVI_SIGNAL_LOSS_CORRECTED_IMG)

if AP_SELECTOR_POS eq 4 and abs(EXP_TIME) le 0.011 then begin
openw, 1, '$SUVI_CAL_DATA'+'/temp_irradiance/SUVI_284.txt'
printf, 1, anytim(time, /ccsds), SUVI_TOT_IRRADIANCE, format='(a,5x, f10.4)'
close, 1
endif
if AP_SELECTOR_POS eq 5 and abs( EXP_TIME) le 0.011 then begin
openw, 1, '$SUVI_CAL_DATA'+'/temp_irradiance/SUVI_304.txt'
printf, 1, anytim(time, /ccsds), SUVI_TOT_IRRADIANCE, format='(a,5x, f10.4)'
close, 1
endif

t1=systime(1)
print, ' CorrectContaminationSignalLoss took: ', t1-t0, ' seconds'

odata = SUVI_SIGNAL_LOSS_CORRECTED_IMG

end
