
pro suvi_fits_keyword_list

   IMG_PKT_TIME		STRING    ''
   SHT_EXP_TIME		FLOAT           0.00000
   CMD_EXP_TIME		FLOAT           0.00000
   IMAGETYPE		STRING    ''
   FRAMETYPE		STRING    ''
   FW1POS		INT              0
   FW1STATUS		INT              0
   FW1NAME		STRING    ''
   FW2POS          INT              0
   FW2STATUS       INT              0
   FW2NAME         STRING    ''
   ASPOS           INT              0
   ASSTATUS        INT              0
   ASNAME          STRING    '195A'
   RDOUTPORT       STRING    ''
   GTYERR          FLOAT           0.00000
   GTZERR          FLOAT           0.00000
   YAW_FLIP        INT              0
   CCDTEMP1        FLOAT           0.00000
   CCDTEMP2        FLOAT           0.00000
   TIME_LAST_BAKEOUT
                   STRING    ''
   AMPNAME         STRING    ''
   EXPTYPE         STRING    ''
   STCOL           INT              0
   ENCOL           INT              0
   STROW           INT              0
   ENROW           INT              0
   SUMM            STRING    ''
   DATE_OBS        STRING    '2016-01-19T18:42:17.84'                                    
   T_OBS           STRING    '2016-01-19T18:42:18.84Z'
   NAXIS           LONG                 2
   NAXIS1          INT           1280
   NAXIS2          INT           1280
   CDELT1          FLOAT           2.50000
   CDELT2          FLOAT           2.50000
   CRPIX1          DOUBLE           640.50000
   CRPIX2          DOUBLE           640.50000
   CRVAL1          FLOAT           0.00000
   CRVAL2          FLOAT           0.00000
   CROTA2          INT              0
   WAVELNTH        INT            195


