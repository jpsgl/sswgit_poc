
pro druckit, waves, rc=rc, gc=gc, bc=bc

;file_171 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194254_00633855.fits'
;file_195 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194854_00633891.fits'
;file_131 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194134_00633847.fits'
;file_094 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194624_00633876.fits'
;file_284 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194324_00633858.fits'
;file_304 = '/archive1/suvi/suvi_data/fm1/l0/2017/04/18/H1900/20170418_194744_00633884.fits'
file_171 = '~/data/suvi/20170418_194254_00633855.fits'
file_195 = '~/data/suvi/20170418_194854_00633891.fits'
file_131 = '~/data/suvi/20170418_194134_00633847.fits'
file_094 = '~/data/suvi/20170418_194624_00633876.fits'
file_284 = '~/data/suvi/20170418_194324_00633858.fits'
file_304 = '~/data/suvi/20170418_194744_00633884.fits'

files_arr = [file_171, file_195, file_131, file_094, file_284, file_304]

waves_suvi = [171, 195, 131, 094, 284, 304]
waves_aia  = [171, 193, 131, 094, 335, 304]

if not exist(waves) then waves = waves_suvi
n_waves = n_elements(waves)

for i=0,n_waves-1 do begin

   ss_match_wave = (where(waves[i] eq waves_suvi, n_match_wave))[0]
   wave0 = waves_suvi[ss_match_wave]
   file0 = files_arr[ss_match_wave]
   mreadfits, file0, index0, data0, /quiet

   data1 = bytscl(safe_log10(nospike(reform(data0), min=1)))
   data2 = rotate_3d(data1, 6)
   siz_data2 = size(data2)
   xsiz = siz_data2[1]
   ysiz = siz_data2[2]
   
; Add timestamp:
   set_plot, 'z'
   time_string = '18-April-2017' ; anytim(indexp.date_obs, /ccsds)
   device, set_resolution = [xsiz, ysiz]
   tvscl, data2
   label = ' GOES-R SUVI Solar Corona ' + string(wave0, '$(i4.4)') + 'A ' + time_string
   xyouts, 10, 75, label, charsize=1.5, color=255, /device, charthick=2
   data2 = tvrd()
   set_plot, 'x'

; Read canonical color table for AIA wave corresponding to this wave:
   aia_lct, raia, gaia, baia, wavelnth=waves_aia[ss_match_wave]
   
   again = 'y'
   while again eq 'y' do begin

      loadct, 0
      lil_data2 = rebin(data2, xsiz/2, ysiz/2, /samp)
      xstepper, [[[lil_data2]], [[lil_data2]]]
;     if not exist(r1) then tvlct, /get, r0, g0, b0
      tvlct, /get, r0, g0, b0

;     rbuff0 = r0[data2]
;     gbuff0 = g0[data2]
;     bbuff0 = b0[data2]
     
;     rbuff1 = raia[rbuff0]
;     gbuff1 = gaia[gbuff0]
;     bbuff1 = baia[bbuff0]

;     tbuff1 = [[[rbuff1]],[[gbuff1]],[[bbuff1]]]

;     rbuff2 = raia[r0[data2]]
;     gbuff2 = gaia[g0[data2]]
;     bbuff2 = baia[b0[data2]]

      rc = raia[r0]
      gc = gaia[g0]
      bc = baia[b0]
      
      rbuff3 = (raia[r0])[data2]
      gbuff3 = (gaia[g0])[data2]
      bbuff3 = (baia[b0])[data2]

      tbuff3 = [[[rbuff3]],[[gbuff3]],[[bbuff3]]]

;     file_jpeg = 'wave_' + strtrim(wave0,2) + '_method1.jpg'
;     write_jpeg, file_jpeg, tbuff1, quality=85, true=3
;     spawn, 'open ' + file_jpeg

      file_jpeg = 'wave_' + strtrim(wave0,2) + '_method3.jpg'
      write_jpeg, file_jpeg, tbuff3, quality=85, true=3
      spawn, 'open ' + file_jpeg

      read, "Enter 'y' to repeat, or return to quit ", again

   endwhile

;  if not exist(r_arr) then r_arr = rbuff0 else r_arr = [[r_arr], [rbuff0]]
;  if not exist(g_arr) then g_arr = gbuff0 else g_arr = [[g_arr], [gbuff0]]
;  if not exist(b_arr) then b_arr = bbuff0 else b_arr = [[b_arr], [bbuff0]]

   if not exist(rc_arr) then rc_arr = rc else rc_arr = [[rc_arr], [rc]]
   if not exist(gc_arr) then gc_arr = gc else gc_arr = [[gc_arr], [gc]]
   if not exist(bc_arr) then bc_arr = bc else bc_arr = [[bc_arr], [bc]]

   if not exist(t_cube) then t_cube = tbuff3 else t_cube = [[[t_cube]], [[tbuff3]]]

endfor
STOP
end

