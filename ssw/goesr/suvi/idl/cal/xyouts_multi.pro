
pro xyouts_multi, x, y, string, _extra=_extra

;+
; translate subwindow nomarlized coords to full window normalized
; coords for xyouts, so you don't have to.
;-

  xmin = !x.region[0]
  xmax = !x.region[1]
  ymin = !y.region[0]
  ymax = !y.region[1]
  xsiz = xmax - xmin
  ysiz = ymax - ymin
  
  x_out = xmin + xsiz*x
  y_out = ymin + ysiz*y

  xyouts, x_out, y_out, string, /norm, _extra=_extra

end
