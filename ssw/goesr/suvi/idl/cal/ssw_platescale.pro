
pro ssw_platescale, index0, data0, index1, data1, t0=t0, $
  file0=file0, file1=file1, wave0=wave0, wave1=wave1, $
  x0_fov_arr=x0_fov_arr, y0_fov_arr=y0_fov_arr, $
  nx_fov_arr=nx_fov_arr, ny_fov_arr=ny_fov_arr, $
  err_roll=err_roll, err_scale_fac=err_scale_fac, $
  err_cen_x_pix=err_cen_x_pix, err_cen_y_pix=err_cen_y_pix, $
  do_pause=do_pause, do_display=do_display, do_map=do_map, $
  do_shift=do_shift, do_xstep_cube=do_xstep_cube, $
  verbose=verbose, qstop=qstop, _extra=_extra

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     ssw_platescale
; CATEGORY:
;     Image registration
; PURPOSE:
;     Determine platescale, pointing, and orientation of an image by cross correlation
;     with another image taken at (nearly) the same time for which these parameters are known.
; CALLS:
;     ssw_platescale, cube
; METHOD:
;     Cross correlation
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     - index0 - Input FITS header structure
;     - data0  - Input image
; OUTPUTS:
;     - index1 - Output FITS header structure (with updated pointing, etc. keywords)
;     - data1  - Output image (optionally registered)
; KEYWORD INPUTS:
;     verbose
;     _extra
; KEYWORD OUTPUTS:
;     corr_struct - structure containing various parameters related to the
;                   input arrays and the correlation results:
;     xdisp_arr - Array of X correlation offsets
;     ydisp_arr - Array of Y correlation offsets
;     peak_str - Strength of correlation peak (sigma)
;     do_shift - If set, shift output image by correlation offsets.
; CALLED BY ROUTINES:
;     aia2suvi
; ROUTINES CALLED:
;     tr_get_disp
; TRAPPED ERRORS:
;
; UNTRAPPED ERRORS:
;
; NOTA BENA:
;
; TODO:
;     - Handle vectors of images
;     - Caching of reusables (fit coefficients, etc)
; DEVELOPMENT STATUS:
;     2015-06-11 - Beta running
; TEST AND VERIFICATION DESCRIPTION:
;     Compare simulated or actual images with various offsets, roll, scale, and 
;       errors in offsets and roll against AIA images.
; TESTING STATUS:
;     2015-06-11 - Tested against simulated images with various offsets, roll,
;                  scale, and errors in offsets and roll.
; DELIVERY ESTIMATE:
;     2015-06-11 - 90
; MAN DAYS REMAINING ESTIMATE:
;     2015-06-11 - 02 man days
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION HISTORY:
;     progver = 'ver_20150611a' ; GLS - Added documentation format.
; VERSION CONTROL STATUS:
;     2015-06-11 - Last commit
;-
; ==============================================================================

if not keyword_set(do_shift) then do_shift=1

if not exist(wave0) then wave0 = 171
if not exist(wave1) then wave1 = 304

if not exist(missing) then missing = 0

if ( (not exist(index0)) or (not exist(data0)) or $
     (not exist(index1)) or (not exist(data1)) ) then begin
  if not exist(file0_sdo) then file0_sdo = last_file(t0, wave=wave0)
  if not exist(file1) then file1 = last_file(t0, wave=wave1)

  read_sdo, file0_sdo, index0_sdo, data0_sdo
; Read file0_sdo then re-map to synthetic suvi image using aia2suvi.pro:
  aia2suvi, file_sdo=file0_sdo, $
            xcen_suvi=xcen_suvi, ycen_suvi=ycen_suvi, crota2_suvi=crota2_suvi, $
            index_suvi=index0, data_suvi=data0, do_map=do_map, _extra=_extra
  if keyword_set(do_pause) then dum = get_kbrd()
  read_sdo, file1, index1, data1
endif

; Optionally introduce known roll, plate scale, and alignment errors:
if ( exist(err_roll) or exist(err_scale_fac) or $
     exist(err_cen_x_pix) or exist(err_cen_y_pix) ) then begin
  if not exist(err_roll) then err_roll = 0
  if not exist(err_scale_fac) then err_scale_fac = 1
  if not exist(err_cen_x_pix) then err_cen_x_pix = 0
  if not exist(err_cen_y_pix) then err_cen_y_pix = 0
  cen_x_pix = index0.crpix1 + err_cen_x_pix
  cen_y_pix = index0.crpix2 + err_cen_y_pix
  data0 = rot(data0, err_roll, err_scale_fac, cen_x_pix, cen_y_pix, $
              interp=interp, cubic=cubic, missing=missing, pivot=pivot)
endif

wcs0 = fitshead2wcs(index0)
wcs1 = fitshead2wcs(index1)

naxis0_1 = index0.naxis1
naxis0_2 = index0.naxis2
crpix0_1 = index0.crpix1
crpix0_2 = index0.crpix2
cdelt0_1 = index0.cdelt1
cdelt0_2 = index0.cdelt2
crval0_1 = index0.crval1
crval0_2 = index0.crval2

cdelt_suvi = 2.5d0              ; Approximate SUVI plate scale
fov_suvi_ff = 1280d0*cdelt_suvi ; Mean SUVI full frame FOV in arcsec
img_cen_x_suvi_pix = 1280d0/2+0.5
img_cen_y_suvi_pix = 1280d0/2+0.5
r_solar_arcsec_avg = 1992d0/2   ; Mean radius of sun in arcsec
r_solar_arcsec_pix = r_solar_arcsec_avg/cdelt_suvi

if not exist(nx_fov_arr) then nx_fov_arr = [512, 256, 256, 512, 512]
if not exist(ny_fov_arr) then ny_fov_arr = [512, 512, 512, 256, 256]
n_fov = n_elements(nx_fov_arr)

if not exist(x0_fov_arr) then $
  x0_fov_arr = img_cen_x_suvi_pix + r_solar_arcsec_pix*[0,-1,+1, 0, 0] - nx_fov_arr/2
if not exist(y0_fov_arr) then $
  y0_fov_arr = img_cen_y_suvi_pix + r_solar_arcsec_pix*[0, 0, 0,-1,+1] - ny_fov_arr/2

; Calculate x,y offsets for all FOVs by cross correlation:
for i=0, n_fov-1 do begin

  ssw_fov_match, index0, data0, index1, data1, t0=t0, $
    file0_sdo=file0_sdo, file1=file1, wave0=wave0, wave1=wave1, $
    x0=x0_fov_arr[i], y0=y0_fov_arr[i], nx=nx_fov_arr[i], ny=ny_fov_arr[i], $
    data0_fov=data0_fov, data1_fov=data1_fov, $
    do_pause=do_pause, do_display=do_display, do_map=do_map, _extra=_extra

  fov_cube = [[[data1_fov]], [[data0_fov]]]

  disp = tr_get_disp(fov_cube, mad=mad, nowin=nowin, shift=do_shift, $
                     do_surf=do_surf, peak_str=peak_str, _extra=_extra)

  if keyword_set(verbose) then begin
    print, 'disp[0,1] = ' + strtrim(disp[0,1],2)
    print, 'disp[1,1] = ' + strtrim(disp[1,1],2)
  endif

  if not exist(xdisp_arr) then xdisp_arr = disp[0,1] else $
    xdisp_arr = [xdisp_arr, disp[0,1]]
  if not exist(ydisp_arr) then ydisp_arr = disp[1,1] else $
    ydisp_arr = [ydisp_arr, disp[1,1]]

  if keyword_set(do_xstep_cube) then xstepper, safe_log10(fov_cube)

endfor
  
; From the set of offsets, calculate mean x,y errors, scale error, and roll error:
xdisp_mean = mean(xdisp_arr)
ydisp_mean = mean(ydisp_arr)

scale_fac_x = 1d0 - ((xdisp_arr[2] - xdisp_arr[1]) / $
              ((x0_fov_arr[2] + nx_fov_arr[2]/2) - (x0_fov_arr[1] + nx_fov_arr[1]/2)))
scale_fac_y = 1d0 - ((ydisp_arr[4] - ydisp_arr[3]) / $
              ((y0_fov_arr[4] + ny_fov_arr[4]/2) - (y0_fov_arr[3] + ny_fov_arr[3]/2)))

cdelt0_1 = cdelt0_1*scale_fac_x
cdelt0_2 = cdelt0_2*scale_fac_y

if keyword_set(verbose) then begin
  print, 'scale_fac_x = ' + strtrim(scale_fac_x,2)
  print, 'scale_fac_y = ' + strtrim(scale_fac_y,2)
  print, 'plate scale x = ' + strtrim(cdelt0_1,2)
  print, 'plate scale y = ' + strtrim(cdelt0_2,2)
endif

if keyword_set(qstop) then $
  stop, 'SUVI_PLATESCALE: Stopping on request before return.'

end

