
function suvi_sharpness, index, data, start_foc_pos, num_foc_steps, x1, x2, y1, y2, $
                         use_syn=use_syn, normalize=normalize, quiet=quiet

;+
; Sharpness array: sharp[axis,img]
; Sharpness measure: (disk center slope variance)/(mean intensity)
; Sharpness measure normalized by dividing by the median
; Sample call:
;   result = suvi_sharpness(index, data, /normalize)
;-

if keyword_set(use_syn) then begin
   if not exist(wave) then $
      if exist(index) then wave = fix(strmid(index.asname,0,(strlen(index.asname)-1))) else wave=171
   filnam_syn = 'suvi_syn_' + strtrim(wave,2) + '.sav'
   set_logenv, 'SUVI_SYN_DATA', concat_dir(get_logenv('SUVI_FM'), 'syn_data')
   file_syn = concat_dir(get_logenv('SUVI_SYN_DATA'), filnam_syn)
   restore, file_syn
   index = index_arr
   data = data_arr
endif

if N_ELEMENTS(x1) eq 0 then x1 = (1330/2) - 128
if N_ELEMENTS(y1) eq 0 then y1 = (1292/2) - 128
if N_ELEMENTS(x2) eq 0 then x2 = x1 + 127
if N_ELEMENTS(y2) eq 0 then y2 = y1 + 127

n_img = n_elements(index)
sharp = fltarr(2,n_img)

for j=0,n_img-1 do begin

    im = float(data[*,*,j]) > 0
    slope = fltarr(256)

    for i=0,(y2-y1-1) do slope(i) = mean(im(x1+1:x2+1,y2-i)-im(x1:x2,y2-i))
    sharp(0,j) = median(slope)

    for i=0,(x2-x1-1) do slope(i) = mean(im(x2-i,y1+1:y2+1)-im(x2-i,y1:y2))
    sharp(1,j) = median(slope)

    dx = im(x1+1:x2+1,y1:y2) - im(x1-1:x2-1,y1:y2)
    dy = im(x1:x2,y1+1:y2+1) - im(x1:x2,y1-1:y2-1)
    
    m0 = mean(float(im(x1:x2,y1:y2)))^2

    sharp(0,j) = variance(dx)/m0
    sharp(1,j) = variance(dy)/m0

    if keyword_set(tvpix) then begin
      window,0,XSIZE=(x2-x1+1)*5,YSIZE=(y2-y1+1)*5, TITLE='FOCUS='+string((j)*num_foc_steps+start_foc_pos)
      tvscl, rebin(im[x1:x2,y1:y2],(x2-x1+1)*5,(y2-y1+1)*5)
    end

endfor

if keyword_set(normalize) then begin
  res = sharp
  normalize = fltarr(2)
  for k=0,1 do normalize(k) = median(sharp(k,*), /even) 
  tmp_sharp = sharp
  for l=0,n_img-1 do sharp(*,l) = tmp_sharp(*,l)/normalize(*)
end

;fcp = indgen(n_img)*num_foc_steps + start_foc_pos

yts = 'Sharpness Residual'
wdef,2,1200,800
!p.multi = [0,0,2]
;plot, fcp, sharp(0,*), ytit='Y '+yts, charsize=2, psym=-4, title='Focus Position by mean'
;plot, fcp, sharp(1,*), ytit='X '+yts, charsize=2, psym=-4
plot, sharp(0,*), ytit='Y '+yts, charsize=2, psym=-4
plot, sharp(1,*), ytit='X '+yts, charsize=2, psym=-4
!p.multi = 0

return, sharp

end
