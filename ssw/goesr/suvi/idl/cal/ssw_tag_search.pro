
function ssw_tag_search, index, extension=extension, search_arr, $
  list=list, table=table, parent=parent, $
  quiet=quiet, verbose=verbose

if tag_exist(index, 'suvi_fits_hdr_names') then $
  tnames = strlowcase(index.suvi_fits_hdr_names) else $
  tnames = strlowcase(tag_names(index))

n_terms = n_elements(search_arr)
for i=0, n_terms-1 do begin
  pos_arr = strpos(tnames, strlowcase(search_arr[i]))
  ss_match = where(pos_arr ne -1, n_match)

  if not exist(tag_name_match) then begin
    tag_name_match = tnames[ss_match]
    tag_pos_match = ss_match
  endif else begin
    tag_name_match = [tag_name_match, tnames[ss_match]]
    tag_pos_match  = [tag_pos_match, ss_match]
  endelse
endfor

n_match = n_elements(tag_name_match)

buff = tag_name_match + '     ' + strtrim(tag_pos_match)
if not keyword_set(quiet) then prstr, buff

return, buff
end
