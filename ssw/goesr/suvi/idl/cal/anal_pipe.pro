
pro anal_pipe, t0=t0, t1=t1, titan=titan

if not exist(pid) then pid = '0x032a'

; if keyword_set(titan) then $
packet_dir_archive1 = concat_dir('/archive1/suvi/suvi_data/fm1/tlm/packet_ids', pid)
packet_dir_titan = concat_dir('/titan/goes/suvi/GOES-16/packets', pid)

if not exist(dir_pool) then dir_pool = '/sanhome/slater/pools/go_mk_suvi_fits'
dir_err = concat_dir(dir_pool, 'error')

if exist(t0) then $
   tim_arr = anytim(timegrid(t0, t1, days=1), /ccsds, /date)

if not exist(tim_arr) then tim_arr = file_break(file_list(dir_err, '*'))
packet_files_archive1 = concat_dir(packet_dir_archive1, time2file(tim_arr, /date) + '.0x032a')
packet_files_titan = concat_dir(packet_dir_titan, time2file(tim_arr, /date) + '.0x032a')

prstr, tim_arr + '   ' + $
       string((file_info(packet_files_archive1)).size/1024d/1024d/1024d, '$(f7.3)') + ' GB   ' + $
       string((file_info(packet_files_titan)).size/1024d/1024d/1024d, '$(f7.3)') + ' GB', /nomore

STOP
end



