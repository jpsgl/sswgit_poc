;+
pro generate_pseudo_daily_irradiance, time, flux, $
s284_half_life=s284_half_life, s304_half_life=s304_half_life, $
e284_half_life=e284_half_life, e304_half_life=e304_half_life
nt=n_elements(time)
;-
; completely arbitary time constants
if n_elements(s284_half_life) ne 1 then s284_half_life=5. ; 5 years
if n_elements(s304_half_life) ne 1 then s304_half_life=2. ; 2 years
if n_elements(e284_half_life) ne 1 then e284_half_life=25. ; 25 years
if n_elements(e304_half_life) ne 1 then e304_half_life=15. ; 15 years
if n_elements(s284_bakeout_increase) ne 1 then s284_bakeout_increase=0.15
if n_elements(s304_bakeout_increase) ne 1 then s304_bakeout_increase=0.10
alpha=fltarr(4)
alpha[0]=alog(2)/(s284_half_life*365.25*86400.)
alpha[1]=alog(2)/(s304_half_life*365.25*86400.)
alpha[2]=alog(2)/(e284_half_life*365.25*86400.)
alpha[3]=alog(2)/(e304_half_life*365.25*86400.)
flux=fltarr(4,nt)
for i=0, 3 do flux[i,*]=exp(-alpha[i]*int2secarr(time,time[0]))

print,'time range: ',anytim(time[0],/ecs),'  -  ',anytim(time[nt-1],/ecs)

outfil=['$SUVI_GPDS1'+'/irradiance/SUVI_284.txt', $
'$SUVI_GPDS1'+'/irradiance/SUVI_304.txt', $
'$SUVI_GPDS1'+'/irradiance/EXIS_284.txt', $
'$SUVI_GPDS1'+'/irradiance/EXIS_304.txt']
for i=0,3 do begin
openw, 1, outfil[i]
for j=0, nt-1 do printf, 1, anytim(time[j],/ecs), flux[i, j], $
format='(a, 3x, f10.5)'
close,1
endfor

end
