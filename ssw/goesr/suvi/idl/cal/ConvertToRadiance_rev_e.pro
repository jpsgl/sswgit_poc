;+
FUNCTION ConvertToRadiance, SUVI_IMG
;
;
;NAME:   ConvertToRadiance
;PURPOSE:
;        Convert image value from DN to radiance (W/m^2-sr)
;CALLING SEQUENCE:
;        SUVI_IMG = ConvertToRadiance(SUVI_IMG)
;INPUTS:
;        SUVI_IMG - SUVI image 
;OUTPUTS:
;        SUVI_IMG_Radiance - SUVI image corrected for gain and linearity, incident photon conversion, 
;                            flux conversion, irradiance conversion, and radiance conversion
;KEYWORDS:
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;                   RWN  May 17, 2011
;        FEB. 2012, ABK, Update to correlate with CDRL80
;        MAR. 2013  ABK  Updated access to exposure time
;	 APR. 2013  DSS	 Moved from double loop over each pixel to array operators for improved speed
;	 MAY. 2014  PB   Split FW1 and FW2 in external data 
;
;-

; All 'printf' statements are written to output log file for GPDS testing purposes.  Can be ignored for main program functionality
COMMON LOGGING, LOGID, QDEBUG
COMMON SUVI_GPDS, ISP, ISP_MAP, INFO
COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT, STATIC_PARAMS, MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, FW1_FILTER_TRANSMISSION, FW2_FILTER_TRANSMISSION, SUVI_LINEARITY, SUVI_GAIN_RIGHT, SUVI_GAIN_LEFT
COMMON OUT_METADATA, DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, PHOT_ENG_CONV, ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, SN_LEV3, FULL_WELL, SHT_EXP_TIME, IMG_PKT_TIME 

t0=systime(1)
printf, logid, '--> 8. Starting ConvertToRadiance.pro (SUVI GPA Routine #8)'

AP_SELECTOR_POS=info.asstatus
FILTER_WHEEL_1_POS=info.fw1status
FILTER_WHEEL_2_POS=info.fw2status
SUVI_CCD_TEMP=info.ccdtemp1
;CCD_READOUT_STATUS=isp.mnem.suv_ceb_rdout_port
 CCD_READOUT_STATUS=isp.suv_ceb_rdout_port_isp
SN_LEV3=MEASURED_PARAMS.SUVI_SN_LEV3
FULL_WELL=MEASURED_PARAMS.SUVI_95_FULL_WELL[0]
EXP_TIME=info.sht_exp_time

;Gain and Linearity Correction (convert DN to electrons)
img_dims = size(SUVI_IMG)
;SUVI_IMG_ELECTRON = fltarr(img_dims(1),img_dims(2))
suvi_img_electron  = (suvi_img*0.0)

; Switching from Kapusta gain tables to FM1 gain tables:
; ******************************************************
; if (SUVI_CCD_TEMP lt 0) then begin
;	approx_temp=abs(SUVI_CCD_TEMP)
;	approx_temp=round(approx_temp)
;	approx_temp_str='m'+strtrim(approx_temp, 1)
;endif else begin
;	approx_temp_str=strtrim(round(SUVI_CCD_TEMP), 1)
;endelse

;case CCD_READOUT_STATUS of
;	0: command='SUVI_GAIN_CONSTANT=SUVI_GAIN_RIGHT.'+approx_temp_str
;	1: command='SUVI_GAIN_CONSTANT=SUVI_GAIN_LEFT.'+approx_temp_str
;	else: stop, 'ConvertToRadiance: illegal readout amp value '+strcompress(CCD_READOUT_STATUS,/remove)
;endcase

;success=execute(command)
; ******************************************************

; Result = INTERPOL( V, X, XOUT [, /LSQUADRATIC] [, /NAN] [, /QUADRATIC] [, /SPLINE] )

case CCD_READOUT_STATUS of
  1: begin
       suvi_gain_table_left  = read_tfile_to_array('suvi_fm1_gain_left_amp.txt')
       suvi_gain_constant  = interpol(float(reform(suvi_gain_table_left[2,*])),  float(reform(suvi_gain_table_left[1,*])), suvi_ccd_temp)
     end
  0: begin
       suvi_gain_table_right = read_tfile_to_array('suvi_fm1_gain_right_amp.txt')
       suvi_gain_constant = interpol(float(reform(suvi_gain_table_right[2,*])), float(reform(suvi_gain_table_right[1,*])), suvi_ccd_temp)
     end
  else: stop, 'ConvertToRadiance: illegal readout amp value '+strcompress(CCD_READOUT_STATUS,/remove)
endcase

;printf, logid, '     SUVI approximate CCD Temperature: ', approx_temp_str
;printf, logid, '     Location of SUVI_GAIN_CONSTANT: ', command
printf, logid, '     SUVI gain constant (based on temperature and amplifier): ', SUVI_GAIN_CONSTANT

;multiply appropriate flat field image with SUVI image

; NB: Start Correction To Kapusta code: Following two versions of Kapusta logic are replaced by DSS 'where' logic :
; *************************************
;FOR i=0,img_dims(1)-1 DO BEGIN
;    FOR j=0,img_dims(2)-1 DO BEGIN
;        ;ONLY FOR TEST PURPOSES, there should be no negative numbers real SUVI data
;        IF SUVI_IMG(i,j) LT 0 THEN SUVI_IMG(i,j)=abs(SUVI_IMG(i,j))
;        SUVI_IMG_ELECTRON(i,j) = SUVI_IMG(i,j) * SUVI_LINEARITY(round(SUVI_IMG(i,j))) * SUVI_GAIN_CONSTANT ;(DN * e-/DN)
;    ENDFOR
;ENDFOR

;for i=0,img_dims(1)-1 do begin
;	for j=0,img_dims(2)-1 do begin
;		if (SUVI_IMG(i,j) lt 0) then SUVI_IMG_ELECTRON(i,j) = SUVI_IMG(i,j) * SUVI_LINEARITY(0) * SUVI_GAIN_CONSTANT ;(DN * e-/DN)
;		if ((SUVI_IMG(i,j) ge 0) and (SUVI_IMG(i,j) le 16384)) then SUVI_IMG_ELECTRON(i,j) = SUVI_IMG(i,j) * SUVI_LINEARITY(round(SUVI_IMG(i,j))) * SUVI_GAIN_CONSTANT ;(DN * e-/DN)
;		if (SUVI_IMG(i,j) gt 16384) then SUVI_IMG_ELECTRON(i,j) = SUVI_IMG(i,j) * SUVI_LINEARITY(16384) * SUVI_GAIN_CONSTANT ;(DN * e-/DN)
;	endfor
;endfor
; **********************************
; NB: End Correction To Kapusta code

; NB: Start Correction To DSS code: 16384 replaced by 16383 :
; *********************************
neg=where(suvi_img lt 0, nneg)
mid=where(((suvi_img(*) ge 0) and (suvi_img(*) le 16383)), nmid)
big=where(suvi_img gt 16383, nbig)

if (nneg gt 0) then SUVI_IMG_ELECTRON(neg)=SUVI_IMG(neg)*SUVI_LINEARITY(0)*SUVI_GAIN_CONSTANT(0)
if (nmid gt 0) then SUVI_IMG_ELECTRON(mid)=SUVI_IMG(mid)*SUVI_LINEARITY(round(suvi_img(mid)))*SUVI_GAIN_CONSTANT(0)
if (nbig gt 0) then SUVI_IMG_ELECTRON(big)=SUVI_IMG(big)*SUVI_LINEARITY(16383)*SUVI_GAIN_CONSTANT(0)

;print, 'Processed '+strcompress(nneg+nmid+nbig,/remove)+' total pixels of 1280x1280 (1638400)'
printf, logid, '     SUVI image converted from DN to electrons using temperature and amplifier dependent gain constant and pixel location dependent linearity coefficient (Whole disk sum: ', total (SUVI_IMG_ELECTRON), ').'

SN_LEV3=SN_LEV3 * SUVI_LINEARITY(SN_LEV3) * SUVI_GAIN_CONSTANT
FULL_WELL=FULL_WELL * SUVI_LINEARITY(FULL_WELL) * SUVI_GAIN_CONSTANT

;Incident Photon Conversion (convert electrons to photons)
PHOT_ELE_CONV=STATIC_PARAMS.PHOT_ELEC_CONVERSION(AP_SELECTOR_POS)
QE=STATIC_PARAMS.SUVI_QE(AP_SELECTOR_POS)

SUVI_IMG_PHOTON = SUVI_IMG_ELECTRON / (PHOT_ELE_CONV/QE)		;(e- / e-/photon)
SN_LEV3=SN_LEV3/(PHOT_ELE_CONV/QE)
FULL_WELL=FULL_WELL/(PHOT_ELE_CONV/QE)

;Flux Conversion (convert photon to J/s)
PHOT_ENG_CONV= STATIC_PARAMS.PHOT_ENG_CONVERSION(AP_SELECTOR_POS)

SUVI_IMG_FLUX = SUVI_IMG_PHOTON *PHOT_ENG_CONV / EXP_TIME			;(photon * J/photon /s)
SN_LEV3=SN_LEV3 *PHOT_ENG_CONV /EXP_TIME
FULL_WELL=FULL_WELL *PHOT_ENG_CONV /EXP_TIME

printf, logid, '     Photon-energy conversion coefficient (wavelength dependent): ', PHOT_ENG_CONV
printf, logid, '     SUVI image converted to flux (Whole disk sum: ', total(SUVI_IMG_FLUX), ').'

;Irradiance Conversion (convert J/s to W/m^2)

SUVI_EFFA=STATIC_PARAMS.SUVI_GEOM_AREA*STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS)*STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS) * $
ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS)*FW1_FILTER_TRANSMISSION(FILTER_WHEEL_1_POS, AP_SELECTOR_POS) *FW2_FILTER_TRANSMISSION (FILTER_WHEEL_2_POS, AP_SELECTOR_POS)

if (qdebug) then begin
	printf, logid, ''
	printf, logid, 'AP_SELECTOR_POS: ' + strtrim(AP_SELECTOR_POS,2)
	printf, logid, 'FILTER_WHEEL_1_POS: ' + strtrim(FILTER_WHEEL_1_POS,2)
        printf, logid, 'FILTER_WHEEL_2_POS: ' + strtrim(FILTER_WHEEL_2_POS,2)
	printf, logid, 'STATIC_PARAMS.SUVI_GEOM_AREA: '+strcompress(STATIC_PARAMS.SUVI_GEOM_AREA,/remove)
	printf, logid, 'STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS): '+strcompress(STATIC_PARAMS.MIRROR_REFLECT_PRIMARY(AP_SELECTOR_POS),/remove)
	printf, logid, 'STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS): '+strcompress(STATIC_PARAMS.MIRROR_REFLECT_SECONDARY(AP_SELECTOR_POS),/remove)
	printf, logid, 'ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS): '+strcompress(ENTRANCE_TRANS.ENTRANCE_FILTER_TRANSMISSION(AP_SELECTOR_POS),/remove)
	printf, logid, ''
endif

SUVI_IMG_IRRADIANCE = SUVI_IMG_FLUX / (SUVI_EFFA[0]) ;(J/s / m^2) or (W/m^2)
SN_LEV3=SN_LEV3/(SUVI_EFFA[0])
FULL_WELL=FULL_WELL/(SUVI_EFFA[0])

printf, logid, '     SUVI effective area: ', SUVI_EFFA
printf, logid, '     SUVI image converted to irradiance(Whole disk sum: ', total(SUVI_IMG_IRRADIANCE), ').'

;Radiance Conversion

SUVI_IMG_RADIANCE = SUVI_IMG_IRRADIANCE / STATIC_PARAMS.SOLID_ANG[0]					;(J/m^2 -sr-s) or (W/m^2 -sr)
SN_LEV3=SN_LEV3/ STATIC_PARAMS.SOLID_ANG[0]
FULL_WELL=FULL_WELL/ STATIC_PARAMS.SOLID_ANG[0]

printf, logid, '     SUVI solid angle: ', STATIC_PARAMS.SOLID_ANG[0]
printf, logid, '     SUVI image converted to radiance (Whole disk sum: ', total(SUVI_IMG_RADIANCE), ').'

printf, logid, '       SUVI Signal to Noise Level 3 converted to radiance: ', SN_LEV3
printf, logid, '       SUVI Full Well converted to radiance: ', FULL_WELL

t1=systime(1)
printf, logid, '   ConvertToRadiance took: ', t1-t0, ' seconds'

return, SUVI_IMG_RADIANCE

END
