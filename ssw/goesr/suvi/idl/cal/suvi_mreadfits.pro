
pro suvi_mreadfits, files, index, data, _extra=_extra

; IDL> tn=tag_names(drms)       ; "old" names vecgtor
; IDL> tnnew=tn+'_new'     ; "new" names vector
; IDL> template=drms[0]    ;
; IDL> nt=n_elements(tn)
; IDL> for i=0,nt-1 do template=rep_tag_name(template,tn[i],tnnew[i])

  mreadfits, files, iindex, data, _extra=_extra
  n_img = n_elements(iindex)

  restore, concat_dir(get_logenv('SUVI_DOCS'), 'suvi_index_template.sav')
; index_template = add_tag(index_template, iindex[0].comment, 'comment')
; index_template = add_tag(index_template, iindex[0].history, 'history')

  tnames_as_read = tag_names(iindex)
  n_tags = n_elements(tnames_as_read)
  tnames_template = tag_names(index_template)

; index = iindex[0]
; for i=0,n_tags-1 do index = rep_tag_name(index,tnames_as_read[i],tnames_template[i])
  
  index = replicate(index_template, n_img)
  index[*] = iindex

end
