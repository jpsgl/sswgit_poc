
function get_adjacents, x_arr, y_arr, img, _extra=_extra

siz_img = size(img)
xsiz = siz_img[1]
ysiz = siz_img[2]

x_adj = [[x_arr-1],[x_arr+0],[x_arr+1], $
         [x_arr-1],[x_arr+0],[x_arr+1], $
         [x_arr-1],[x_arr+0],[x_arr+1]]
y_adj = [[y_arr-1],[y_arr-1],[y_arr-1], $
         [y_arr+0],[y_arr+0],[y_arr+0], $
         [y_arr+1],[y_arr+1],[y_arr+1]]

; Flag pixels which are off the array:
ss_x_bad = where( ((x_adj lt 0) or (x_adj gt (xsiz-1))), n_x_bad )
if n_x_bad gt 0 then begin
   x_adj[ss_x_bad] = -1
   y_adj[ss_x_bad] = -1
endif
ss_y_bad = where( ((y_adj lt 0) or (y_adj gt (ysiz-1))), n_y_bad )
if n_y_bad gt 0 then begin
   x_adj[ss_y_bad] = -1
   y_adj[ss_y_bad] = -1
endif

; Flag center pixel:
x_adj[*,4] = -1
y_adj[*,4] = -1

return, [[[x_adj]],[[y_adj]]]

end
