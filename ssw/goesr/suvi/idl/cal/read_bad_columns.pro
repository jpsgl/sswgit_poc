
;+
function read_bad_columns
;-


file_bad_col = file_list(concat_dir(get_logenv('SUVI_CAL_DATA'), 'bad_columns'), $
                         'suvi_bad_columns_ver??.txt')
if file_bad_col[0] eq '' then $
  STOP, 'Bad column file(s) not found.  Stopping.'
file_bad_col = file_bad_col[sort(file_bad_col)]
file_bad_col = file_bad_col[n_elements(file_bad_col)-1]

bc1 = rd_tfile(file_bad_col)    ; read as a string per line
bc2 = rd_tfile(file_bad_col, 2) ; read only the two entries
nbad=n_elements(bc1)
bnames='BAD'+string(lindgen(nbad)+1,format='(i3.3)')
for i=0,nbad-1 do begin
pcomma=strarr(100)
jstart=0
for j=0, 100 do begin
out=strpos(bc1[i],',',jstart)
if out eq -1 then goto, cont
pcomma[j]=out
jstart=out+1
endfor
cont:
ntcomma=j
print,'i = ',i,'   ntcomma = ',ntcomma
pcomma=pcomma[0:ntcomma-1]
npcomma=fix(pcomma)
; ntcomma <2
if ntcomma eq 1 then begin
cvalues=intarr(2)
cvalues[0]=strmid(bc2[1,i],0,strlen(bc2[1,i])-1)
cvalues[1]=strmid(bc1[i],pcomma[0]+1,strlen(bc1[i])-npcomma[0]-1)
goto, found_good
endif

; ntcomma >=2
cvalues=strarr(ntcomma-1)
for j=0, ntcomma-2 do begin
cvalues[j]=strmid(bc1[i], npcomma[j]+1, npcomma[j+1]-npcomma[j]-1)
endfor

; treat the first and last entries
bc22=bc2[1,i]
cvalue0=strmid(bc22,0,strlen(bc22)-1)
cvaluen=strmid(bc1[i],npcomma[ntcomma-1]+1,strlen(bc1[i])-npcomma[ntcomma-1])
cvalues=[cvalue0, cvalues, cvaluen] 

found_good:
ngood=n_elements(cvalues)
bc20=bc2[0,i]  ; bad column
;   print,'ng2 = ',ng2,'   ngood = ',ngood
gc=cvalues

badstr =  {ngood:  ngood, $ ; # of good pixels used to replace this bad pixel
bad_c:   fix(bc20), $ ; bad column
good_c:  fix(gc)}     ; good columns

; help,badstr,/str
if i eq 0 then bp={file: file_break(tinfil), $ ; name of the bad pixels file
nbad:  nbad, $ ; # of good pixels used to replace this bad pixel
bad001: badstr} else $
bp=add_tag(bp,badstr,bnames[i])
endfor

return, bp

end
