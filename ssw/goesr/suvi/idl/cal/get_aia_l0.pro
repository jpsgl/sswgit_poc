
function get_aia_l0, wave, index=index, n_img=n_img

  if not exist(dir_l0) then dir_l0 = '/SUM13/D801884506/S00000'
  if not exist(wave) then wave = 171
  if not exist(n_img) then n_img = 1
  files_all = file_list(dir_l0, 'aia.lev0.*image.fits')

  mreadfits_header, files_all, index_all, exten=1
  
  ss_wave = where(index_all.wavelnth eq wave, n_wave)

  ss_wave = ss_wave[0:((n_img<n_wave)-1)]
  files = files_all[ss_wave]
  index = index_all[ss_wave]
  read_sdo, files[ss_wave], index_drms, data

  return, data

end
