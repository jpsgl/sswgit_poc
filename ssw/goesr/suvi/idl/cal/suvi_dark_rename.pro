
function suvi_dark_rename

if not exist(dir_darks) then dir_darks = '~/suvi_data/cal_data/dark/'

;files = file_list('~/suvi_data/cal_data/dark/','SUVI*.fits')
;mreadfits_header, files, index

;time_string = time2file(index.date_obs,/sec) + '_'
;exp_string  = (['s_exp','l_exp'])[(index.exptime) ge 0.1] + '_'
;amp_string  = (['l_amp','r_amp'])[(index.ampname) eq 'right'] + '_'
;;temp_string = str_replace(string(index.CCDTEMP,'(f5.1)'),'-','m')
;temp_string = str_replace(str_replace(string(index.CCDTEMP*10,'(i4.3)'),'-','m'),'.','_')

;outfiles = 'suvi_fmx_dark_' + time_string + exp_string + amp_string + temp_string + '.fits'
;outfiles = concat_dir('~/suvi_data/cal_data/dark/', outfiles)

files = file_list('~/suvi_data/cal_data/dark/','suvi*.fits')
mreadfits_header, files, index

time_string = time2file(index.date_obs,/sec) + '_'
fm_string   = 'fm' + 'x' + '_'
as_string   = 'asx'
fw1_string  = 'f1x'
fw2_string  = 'f2x'
filt_string = as_string + fw1_string + fw2_string + '_'
exp_string  = (['exps','expl'])[(index.exptime) ge 0.1] + '_'
amp_string  = (['ampl','ampr'])[(index.ampname) eq 'right'] + '_'
;temp_string = str_replace(string(index.CCDTEMP,'(f5.1)'),'-','m')
temp_string = str_replace(str_replace(string(index.CCDTEMP*10,'(i4.3)'),'-','m'),'.','_')

outfiles = 'suvi_' + time_string + fm_string + 'dark_' + filt_string + exp_string + $
                     amp_string + temp_string + '.fits'
outfiles = concat_dir('~/suvi_data/cal_data/dark_new/', outfiles)
outdirs = ssw_time2paths(index.date_obs, /hour)      

nfil = n_elements(files)
for i=0,nfil-1 do begin
  cmd = 'cp -p ' + files[i] + ' ' + outfiles[i]
;print, cmd
;stop
  spawn, cmd
endfor

return, outfiles

end
