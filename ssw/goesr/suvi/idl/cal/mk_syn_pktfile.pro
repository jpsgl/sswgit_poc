
pro mk_syn_pktfile, pktfil=pktfil, wave_arr=wave_arr, $
                    use_save=use_save, do_save=do_save, $
                    do_mk_pktfil=do_mk_pktfil, do_sub=do_sub, $
                    do_prep=do_prep, do_disp=do_disp, do_stop=do_stop, $
                    delt_sec_arr=delt_sec_arr, _extra=_extra

  if not exist(use_save) then use_save=1
  if not exist(do_save) then do_save=1
  if not exist(do_disp) then do_disp=1

; Define directory paths:
  if not exist(fm) then fm = '1' 
  setup_suvi_env, fm=fm, _extra=_extra

; mk_suvi_fits, pktfil, ss_img=ss_img, $
;               /do_index_arr, iindex_arr, /do_data_arr, idata_arr, $
;               this_img_name=this_img_name, do_prep=do_prep, $
;               interactive=interactive, _extra=_extra

  if not exist(file_suvi_gpa_case_set) then $
     file_suvi_gpa_case_set = 'suvi_gpa_case_set.sav'
  if not exist(file_suvi_syn_cube) then $
     file_suvi_syn_cube = 'suvi_syn_cube.sav'
  if keyword_set(do_stop) then STOP
  
  if (keyword_set(use_save) and (file_exist(file_suvi_gpa_case_set) eq 1)) then $
     restore, file_suvi_gpa_case_set else $
        go_mk_suvi, /do_index_arr, /do_data_arr, $
                    iindex_arr=iindex_arr, idata_arr=idata_arr, pktfil=pktfil, _extra=_extra
  if keyword_set(do_save) then $
     save, iindex_arr, idata_arr, pktfil, file=file_suvi_gpa_case_set
  if keyword_set(do_stop) then STOP

  iindex_arr = suvi_hdr_adjust(iindex_arr, suvi_case_struct, _extra=_extra)
  if keyword_set(do_stop) then STOP

  if (keyword_set(use_save) and (file_exist(file_suvi_syn_cube) eq 1)) then $
     restore, file_suvi_syn_cube else $
        gen_syn_cube, index_suvi_template=iindex_arr, data_arr=idata_arr, $
                      do_disp=do_disp, do_xstep=do_xstep, /raw, _extra=_extra
  if keyword_set(do_save) then $
     save, iindex_arr, idata_arr, file=file_suvi_syn_cube
  if keyword_set(do_stop) then STOP

  if keyword_set(do_mk_pktfil) then begin
     rd_suvi_pktfile, pktfil, fix(idata_arr), do_disp=do_disp, do_sub=do_sub, _extra=_extra
     if keyword_set(do_stop) then STOP
  endif
     
  if keyword_set(do_prep) then begin

     print, ''
     print, ' =========================================================================='
     print, ' Begin SUVI Ground Processing Development System Testing at ' + $
            strmid(anytim(!stime,/yoh),0,15)
     print, ' =========================================================================='
     print, ' STARTING CASE ' + string(1, format='(i2.2)')
     print, ' =========================================================================='
     print, ''

     n_img = n_elements(iindex_arr)
     for i=0,n_img-1 do begin
        suvi_prep, iindex_arr[i], reform(idata_arr[*,*,i]), $
                   indexp0, datap0, delt_sec_write=delt_sec_write, $
                   /do_disp, _extra=_extra
        if not exist(indexp_arr) then begin
           indexp_arr = indexp0
           datap_arr = datap0
           delt_sec_arr = delt_sec_write
        endif else begin
           indexp_arr = concat_struct(indexp_arr, indexp0)
           datap_arr = [[[datap_arr]], [[datap0]]]
           delt_sec_arr = [delt_sec_arr, delt_sec_write]
        endelse
        print, ''
        print, ' =========================================================================='
        print, ' FINISHED CASE ' + string(i+1, format='(i2.2)')
        print, ''
        print, ' STARTING CASE ' + string(i+2, format='(i2.2)')
        print, ' =========================================================================='
        print, ''
     endfor
  endif
  if keyword_set(do_stop) then STOP

end
