;+
pro SubtractProcessedDark, iindex, idata, oindex, odata, $
  exposure_threshold=exposure_threshold, $
  temperature_threshold=temperature_threshold, day_threshold=day_threshold

;
;NAME:  SubtractProcessedDark
;PURPOSE: 
;       Select 10 most recent dark images on the basis of the
;       exposure time and CCD temperature (and readout_status),
;       and subtract the composite dark image (median of the 
;       collected dark images) from the SUVI science image.
;CALLIG SEQUENCE:
;       SUVI_IMG = SubtractProcessedDark(SUVI_IMG)
;INPUTS:
;       SUVI_IMG - SUVI raw image
;OUTPUTS: 
;       SUVI_IMG_DARK_SUBTRACTED - SUVI image corrected by subtracting the 
;                                  composite dark image.
;KEYWORDS:
;PROCEDURE CALLS:
;MODIFICATION HISTORY:
;	31-Jan-2013 AK  Modified code to match what is stated in CDL80 for
;	determining temperature threshold (+/- 2 degrees)
;	30-Apr-2013 DSS	traded pixel-by-pixel median calculation for the
;                       array operation with dimension option in median function
;-

t0=systime(1)
print, '--> 5. Starting SubtractProcessedDark.pro (SUVI GPA Routine #5)'

oindex = iindex
odata  = idata

dinfil_final = suvi_get_dark(iindex, idata, $
  exposure_threshold=exposure_threshold, $
  temperature_threshold=temperature_threshold, day_threshold=day_threshold)

print, '     +++Selected Darks:'
print, '          ', dinfil_final
dark_names = file_break(dinfil_final) ; Remove path

mreadfits, dinfil_final, dindex_final, ddata_final, /quiet
ndark = n_elements(index)
if ndark gt 1 then darkimage = median(ddata_final,dimension=3) else $
 		   darkimage = ddata_final

odata = idata - darkimage

; Add addtional header tags:
oindex = add_tag(oindex, n_elements(DARK_NAMES), 'NUMDARKS')
for j=0, n_elements(DARK_NAMES)-1 DO $
  indexp = add_tag(indexp, DARK_NAMES[j], 'DARKFN_' + string(j, '(i2.2)'))

t1=systime(1)
print, 'SubtractProcessedDark took: ', t1-t0, ' seconds'

end
