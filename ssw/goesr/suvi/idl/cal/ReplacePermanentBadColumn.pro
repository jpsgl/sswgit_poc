;+
pro ReplacePermanentBadColumn, iindex, idata, oindex, odata

;+
;NAME:   ReplacePermanentBadColumn
;PURPOSE:
;        Replace permanent bad columns by mean of neighboring, non_bad columns
;CALLING SEQUENCE:
;        SUVI_IMG = ReplacePermanentBadColumn(SUVI_IMG)
;INPUTS:
;        SUVI_IMG - SUVI image to have bad columns replaced by mean of
;                   neighboring non_bad columns
;OUTPUTS:
;        SUVI_IMG_BAD_COLUMN_CORRECTED - floating point SUVI image
;                                        corrected by replacing bad columns 
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2012, NVN, written as a new item following the philosophy
;                        for replacing bad columns, i.e., relying on the lookup table. 
;        FEB. 2012  ABK, Update to correlate with SUVI CDRL80
;-

t0=systime(1)
print, '--> 4. Starting ReplacePermanentBadColumn.pro (SUVI GPA Routine #4)'

; According to CDRL 80 (4.4), a lookup table should be used that contains a list
; of bad columns and good columns whose means at each row are used to replace
; the value of that (column, row). The whole information is stored in a structure
; called SUVI_BAD_COLUMNS

oindex = iindex
odata  = idata

ny = (size(odata))[2]

SUVI_BAD_CCD_COLUMNS = read_bad_columns()
NBAD = SUVI_BAD_CCD_COLUMNS.NBAD
print, '   Number of permanent bad columns: ', NBAD

if nbad gt 0 then begin
  bc_tags=tag_names(SUVI_BAD_CCD_COLUMNS)

  for i=0,nbad-1 do begin
; First item in bc is the file name, second the number of bad columns
    command='badstr=SUVI_BAD_CCD_COLUMNS.'+bc_tags[i+2]
    result=execute(command)

    if result ne 1 then begin
      print, 'Problem interpretting bad pixel structure variable.  Returning.'
      return
    endif

    bad_c=badstr.bad_c
    good_c=badstr.good_c
    ngood=badstr.ngood
    for j=0,ny-1 do $
      odata[bad_c,j]=average(idata[good_c,j])

print, '     Bad column ', bad_c, ' replaced with ', ngood, ' good columns:'
print, '        Column value of ',idata[bad_c,   0],' at (', bad_c,',',   0, ') is replaced with ', $
                  average(idata[good_c,   0]),', which is the average of ',ngood,' columns.'
print, '        Column value of ',idata[bad_c, 640],' at (', bad_c,',', 640, ') is replaced with ', $
                  average(idata[good_c, 640]),', which is the average of ',ngood,' columns.'
print, '        Column value of ',idata[bad_c,1100],' at (', bad_c,',',1100, ') is replaced with ', $
                  average(idata[good_c,1100]),', which is the average of ',ngood,' columns.'

  endfor
endif

t1=systime(1)
print, '   ReplacePermanentBadColumn took: ', t1-t0, ' seconds'

END
