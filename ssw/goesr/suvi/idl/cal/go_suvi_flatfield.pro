
pro go_suvi_flatfield, use_prev=use_prev, _extra=_extra

  if not exist(use_prev) then use_prev = 0
  
; Iteratively select parameters and run make_suvi_flat:

  iter_arr = ['1', '2', '3', '4', '5', '6', '7']
;  x_offset_arr = ['00', '10']
;  y_offset_arr = ['00', '10']

  ans = ''
  while ans ne 'n' do begin
     
     ss_wave = xmenu_sel(iter_arr, title='Select)
;     ss_x_offset = xmenu_sel(x_offset_arr)
;     ss_y_offset = xmenu_sel(y_offset_arr)

     suvi_flatfield, use_prev=use_prev
     
     read, 'Make another flat field? (def is yes) : ', ans

  endwhile

end

