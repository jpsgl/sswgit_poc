
pro info, ss, add=add

  if get_logenv('INFO_FILE') eq '' then $
     filnam_info = '~/soft/idl_temp/info.txt'
  if not keyword_set(add) then begin
     buff = strlowcase(rd_tfile(filnam_info))
     ss_match = where(strpos(buff, strlowcase(ss)) ne -1, n_match)
     if n_match gt 0 then prstr, buff[ss_match]
  endif else begin
     file_append, filnam_info, ss
  endelse

end

