
pro go_suvi_prep, iindex_arr, idata_arr, $
    no_readout_correction=no_readout_correction, $
    no_bad_pix=no_bad_pix, $
    no_bad_col=no_bad_col, $
    no_dark=no_dark, $
    no_flat=no_flat, $
    no_reg=no_reg, $
    no_convert_to_rad=no_convert_to_rad, $
    do_l0_write=do_l0_write, do_l1b_write=do_l1b_write, $
    do_stop=do_stop, _extra=_extra

  if not exist(do_l0_write)  then do_l0_write = 1
  if not exist(do_l1b_write) then do_l1b_write = 1
  
  do_prep = 1

  no_readout_correction = 0
  no_bad_pix = 0
  no_bad_col = 0
  no_dark = 0
  no_flat = 0
  no_reg = 0
  no_convert_to_rad = 0

;  no_readout_correction = 0
;  no_bad_pix = 1
;  no_bad_col = 1
;  no_dark = 1
;  no_flat = 1
;  no_reg = 0
;  no_convert_to_rad = 1
  
  if keyword_set(do_prep) then begin

     print, ''
     print, ' =========================================================================='
     print, ' Begin SUVI Ground Processing Development System Testing at ' + $
            strmid(anytim(!stime,/yoh),0,15)
     print, ' =========================================================================='
     print, ''

     n_img = n_elements(iindex_arr)
     for i=0,n_img-1 do begin

        print, ''
        print, ' =========================================================================='
        print, ' STARTING CASE ' + string(i+1, format='(i2.2)')
        print, ' =========================================================================='
        print, ''

        sttim_case = anytim(systime(/sec), /ccsds)
        
        if ( keyword_set(do_l0_write) or keyword_set(info_tags_only) ) then begin
           index_l0 = iindex_arr[i]
           tnames_l0 = tag_names(index_l0)

; For now (2016-08-07) include only the 33 'info' keywords:
           tnames_l0 = tnames_l0[0:32]
           index_l0 = str_subset(index_l0, tnames_l0)

; Add replacement tags with names le 8:
           index_l0 = add_tag(index_l0, anytim(index_l0.img_pkt_time, /ccsds), 'date_obs')
           index_l0 = add_tag(index_l0, anytim(index_l0.img_pkt_time, /ccsds), 'pkttime')
           index_l0 = add_tag(index_l0, index_l0.sht_exp_time, 'exptime')
           index_l0 = add_tag(index_l0, index_l0.cmd_exp_time, 'cmdexptm')
           index_l0 = add_tag(index_l0, anytim(index_l0.time_last_bakeout, /ccsds), 'lstbkout')

; Set misc tags (kluge):
           index_l0.simple = 't'
           index_l0.bitpix = 16

           data_l0 = reform(idata_arr[*,*,i])
        endif

;       suvi_prep, iindex_arr[i], reform(idata_arr[*,*,i]), $
        suvi_prep, index_l0, data_l0, $
                   indexp0, datap0, delt_sec_write=delt_sec_write, $
                   no_readout_correction=no_readout_correction, $
                   no_bad_pix=no_bad_pix, $
                   no_bad_col=no_bad_col, $
                   no_dark=no_dark, $
                   no_flat=no_flat, $
                   no_reg=no_reg, $
                   no_convert_to_rad=no_convert_to_rad, $
                   /do_disp, _extra=_extra

        if not exist(indexp_arr) then begin
           indexp_arr = indexp0
           datap_arr = datap0
           delt_sec_arr = delt_sec_write
        endif else begin
           indexp_arr = concat_struct(indexp_arr, indexp0)
           datap_arr = [[[datap_arr]], [[datap0]]]
           delt_sec_arr = [delt_sec_arr, delt_sec_write]
        endelse

        if keyword_set(do_l0_write) then begin

; Remove tags with names gt 8:
           index_l0 = rem_tag(index_l0, 'img_pkt_time')
           index_l0 = rem_tag(index_l0, 'sht_exp_time')
           index_l0 = rem_tag(index_l0, 'cmd_exp_time')
           index_l0 = rem_tag(index_l0, 'time_last_bakeout')

           set_logenv, 'SUVI_L0', '/archive1/suvi/suvi_data/fm1/l0'
           outfil_l0 = 'suvi_gpa_l0_case_' + string(i+1,'(i2.2)') + '_' + $
                        time2file(index_l0.date_obs, /sec) + '.fits'
           outfil_l0 = concat_dir(get_logenv('SUVI_L0'), outfil_l0)
           mwritefits, index_l0, data_l0, outfile=outfil_l0, loud=verbose
           mwritefits, index_l0, fix(data_l0), outfile=outfil_l0, loud=verbose
        endif
           
        if keyword_set(do_l1b_write) then begin
           set_logenv, 'SUVI_L1B', '/archive1/suvi/suvi_data/fm1/l1b'
           outfil_l1b = 'suvi_gpa_l1b_case_' + string(i+1,'(i2.2)') + '_' + $
                        time2file(indexp0.date_obs, /sec) + '.fits'
           outfil_l1b = concat_dir(get_logenv('SUVI_L1B'), outfil_l1b)
           mwritefits, indexp0, fix(datap0), outfile=outfil_l1b, loud=verbose
        endif

        entim_case = anytim(systime(/sec), /ccsds)
        t_delt_sec_case = anytim(entim_case) - anytim(sttim_case)
        
        print, ''
        print, ' =========================================================================='
        print, ' FINISHED CASE ' + string(i+1, format='(i2.2)')
        print, ' EXECUTION TIME (SECONDS) FOR THIS CASE = ' + string(t_delt_sec_case, format='(f5.2)')
        print, ' =========================================================================='
        print, ''

        if keyword_set(do_stop) then STOP

     endfor

  endif

  if keyword_set(do_stop) then STOP

end
