
function set_suvi_flat_files

dir='$SUVI_CAL_DATA'+'/flat'

flat_94  = dir+'/SUVI_FM1_94A_FLAT.fits'
flat_131 = dir+'/SUVI_FM1_131A_FLAT.fits'
flat_171 = dir+'/SUVI_FM1_171A_FLAT.fits'
flat_195 = dir+'/SUVI_FM1_195A_FLAT.fits'
flat_284 = dir+'/SUVI_FM1_284A_FLAT.fits'
flat_304 = dir+'/SUVI_FM1_304A_FLAT.fits'

flats=[flat_94, flat_131, flat_171, flat_195, flat_284, flat_304]

return, flats

END
