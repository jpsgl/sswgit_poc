
pro pkt_select, pktfname0, ss_img

;+
; Extract selected image/header pairs from a SUVI packet file, and write out
;    the selected ones to a new packet file.
; History:
;    2016-07-30 - GLS - Written, based upon rd_suvi_pktfile.pro.
;-

; Define misc defaults:
if not exist(pktfname0) then pktfname0 = './pktfile_test0'
if not exist(pktfname1) then pktfname1 = './pktfile_test1'

; Remove existing pktfname1:
spawn, 'rm ' + pktfname1
spawn, 'touch ' + pktfname1

; Open packet file:
OPENU, lun0, pktfname0, /get_lun
OPENU, lun1, pktfname1, /get_lun

; Define packet structure:
pkt_template0  = {pkt0, img_hdr:BYTARR(3465216l)}
pkt_template1  = {pkt1, img_hdr:BYTARR(3465216l)}

; Setup error handling using CATCH (especially for EOF errors):
CATCH, error_status
if error_status ne 0 then begin
   PRINT, 'SUVI_IMG detected an error: ' + !error_state.msg
   RETURN
endif
	
; Define associated variables for reading one img_hdr pair:
rec0 = ASSOC( lun0, pkt_template0 )
rec1 = ASSOC( lun1, pkt_template1 )

n_img = n_elements(ss_img)
for i=0, n_img-1 do begin
  img_hdr0 = rec0[ss_img[i]]
  rec1[i] = img_hdr0
endfor

; Close packet file and free the lun:
FREE_LUN, lun0
FREE_LUN, lun1

end

