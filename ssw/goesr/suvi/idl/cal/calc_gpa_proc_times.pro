
pro calc_gpa_proc_times, logfil

if not exist(logfil) then logfil ='~/suvi_data/logs/suvi_prep.log151214.160115'
spawn, 'grep "took:" ' + logfil + ' > ./exec_times.txt'
buff = rd_tfile('./exec_times.txt',4)
func_arr = reform(buff[0,*])
sec_arr = reform(buff[2,*])
n_rec = n_elements(buff[0,*])

func_vals = all_vals(func_arr)

for i=0,n_elements(func_vals)-1 do begin
  ss_func = where(func_arr eq func_vals[i], n_match)
  sec_func = sec_arr(ss_func)
  t_avg = total(float(sec_func))/n_match
  t_max = max(float(sec_func))
  print, 'Mean execution time for ' + func_vals[i] + ' = ' + strtrim(t_avg,2) + ' sec'
  print, 'Max  execution time for ' + func_vals[i] + ' = ' + strtrim(t_max,2) + ' sec'
endfor

end
