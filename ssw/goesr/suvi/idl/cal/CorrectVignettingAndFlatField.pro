;+
pro CorrectVignettingAndFlatField, iindex, idata, oindex, odata, $
                                   flat_select=flat_select, $
                                   use_unitary=use_unitary, _extra=_extra

;
;NAME:   CorrectVignettingAndFlatField
;PURPOSE:
;        Correct SUVI image by multiplying by appropriate flat field image 
;CALLING SEQUENCE:
;        CorrectVignettingAndFlatField, iindex, idata, oindex, odata
;INPUTS:
;        SUVI_IMG - SUVI image 
;OUTPUTS:
;        SUVI_IMG_FLAT_CORRECT - SUVI image corrected by multiplying
;                                by appropriate flat field image 
;KEYWORDS:
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;                   RWN  May. 9, 2011
;        FEB. 2012  ABK  Added wavelength dependence and external access
;	 OCT. 2016  GV, GLS
;                        Corrected the flat field selection based on
;	 		 aperture selector and filter wheel 2
;	 		 position. This change is needed since the
;	 		 flat field depends on the two filters adding
;	 		 different shadow on the CCD.
;-

t0=systime(1)
print, '--> 6. Starting CorrectVignettingAndFlatField.pro (SUVI GPA Routine #6)'

oindex = iindex
odata  = idata

use_unitary = 1
if keyword_set(use_unitary) then return

AP_SELECTOR_POS = iindex.asstatus
FW1_POS = iindex.fw1status
FW2_POS = iindex.fw2status

; GLS - 2016-10-17:
; Define string arrays for constructing appropriate flat filed FITS file
; based upon AS, FW!, and FW2 values:
as_str_arr  = ['094', '131', '171', '195', '284', '304']
fw1_str_arr = ['Open',  'ThinAl', 'ThinZr', 'ThickZr', 'ThickAl']
fw2_str_arr = ['Glass', 'Open',   'ThinAl', 'ThinZr',  'ThickAl']

; Define flat directory:
dir_flat = concat_dir(get_logenv('SUVI_CAL_DATA'), 'flat')

; Generate fully qualified flat file name:
if not keyword_set(flat_select) then begin
;  SUVI_FLAT = set_suvi_flat_files()
   file_flat = '/SUVI_FM1_Flat_' + as_str_arr[AP_SELECTOR_POS] + '_' + $
                              fw1_str_arr[FW1_POS] + '_' + $
                              fw2_str_arr[FW2_POS] + '_20161015' + '.fits'
   FLAT_IMG_LOC = concat_dir(dir_flat, file_flat)
endif else begin
  files_flat = file_list(concat_dir(get_logenv('SUVI_DATA'), 'l0_switch'), '*flat.fits')
  filnams_flat = file_break(files_flat)
  ss_select = xmenu_sel(filnams_flat)
  FLAT_IMG_LOC = files_flat[ss_select]
endelse

print, 'Using SUVI Flat: ', FLAT_IMG_LOC

mreadfits, FLAT_IMG_LOC, FLAT_HEAD, FLAT_DATA

; Multiply appropriate flat field image with SUVI image
odata = idata * FLAT_DATA

t1=systime(1)
print, '   CorrectVignettingAndFlatField took: ', t1-t0, ' seconds'

END
