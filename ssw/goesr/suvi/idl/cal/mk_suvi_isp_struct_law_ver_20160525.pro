function ext_mnem, data, map

;destroy, outstruct

outstruct={name:'',val:0L}
len=n_elements(map)
outstruct=replicate(outstruct,len)

for i=0,len-1 do begin
	typ=map(i).type
	stbyte=map(i).stbyte
	stbit=map(i).stbit
	nbits=map(i).nbits
	mnem=map(i).name
;	help, mnem
	case typ of
		'UL1':	out=long(data(stbyte:stbyte+3),0,1)		;unsigned, noswap
		'UL':	out=long(data(stbyte:stbyte+3),0,1)		;typo, should be UL1
		'IL1':	out=long(data(stbyte:stbyte+3),0,1)		;signed, noswap
		'IS1':	out=fix(data(stbyte:stbyte+1),0,1)		;signed, noswap
		'IU1':	out=fix(data(stbyte:stbyte+1),0,1)		;unsigned, noswap
		'UB':	out=reform(data(stbyte))			;unsigned byte
		else: 	out=long(data(stbyte:stbyte+3),0,1)
	endcase
	qswap=is_member(/swap_os)
	if (qswap) then dec2sun, out

;;;-additional processing
	case typ of
		'IU1':	out=unsign(out,16)	;make unsigned (changes the output type too though)
		'UL1':	out=ulong(out)	;
		'UL':	out=ulong(out)	;
		else:
	endcase

	case typ of
		'UL1':	nbit4type=32
		'UL':	nbit4type=32
		'IL1':	nbit4type=32
		'IS1':	nbit4type=16
		'IU1':	nbit4type=16
		'UB':	nbit4type=8
		else:	nbit4type=32
	endcase

	if (nbit4type ne nbits) then begin
		stbit=nbit4type-stbit-nbits	;database terminology has bit0=MSB
		out=mask(out,stbit,nbits)
	endif
	out=reform(out)
	outstruct(i).name=mnem
	outstruct(i).val=out
endfor 
return,outstruct
end

function mk_suvi_isp_struct, pktdata, head_only=head_only

;common log file
COMMON LOGGING, LOGID, QDEBUG
;common shared data structures for easy access and manipulation
COMMON SUVI_GPDS, ISP2, ISP_MAP, INFO

if (n_elements(isp2) ne 0) then dejavu=1 else dejavu=0

if dejavu then mnem=isp2.mnem $ ;if this is not the first time, then recall the isp struct from last time 
	else isp_map=mk_suvi_isp_map() ;otherwise generate the map

nstruct=n_elements(pktdata)
if (nstruct gt 1) then nstruct=1 ;force this routine to only process one isp packet

;str='mk_suvi_isp_struct: Start processing '+strcompress(nstruct,/remove)+' packets'
;if (keyword_set(head_only)) then str=str+' -- headers only'
;print, str

for k=0,nstruct-1 do begin
	tmp=ext_mnem(pktdata(k).data_head,isp_map.head)
	head=create_struct(tmp(0).name,tmp(0).val)
	len=n_elements(tmp)
	for i=1,len-1 do head=add_tag(head,tmp(i).val,tmp(i).name)
	if (keyword_set(head_only)) then begin
		if (k eq 0) then begin
		   isp=create_struct('head',head)
                   isp=replicate(isp,nstruct)
		endif else isp(k).head=head
	endif else begin
		tmp=ext_mnem(pktdata(k).data,isp_map.tlm)
		len=n_elements(tmp)
		if dejavu then begin 
			mnem.(0)=tmp(0).val
			len=n_elements(tag_names(mnem))  ;only copy the correct number of records, no repeats
		     endif else mnem=create_struct(tmp(0).name,tmp(0).val)
                for i=1,len-1 do begin
;                  if ( (strupcase(tmp(i).name) eq 'SUV_AS_POS' )   or $
;                       (strupcase(tmp(i).name) eq 'SUV_FW1_POS')   or $
;                       (strupcase(tmp(i).name) eq 'SUV_FW2_POS') ) then $
		   tmpname=tmp(i).name
                   if ( (strupcase(tmpname) eq 'SUV_AS_POS' ) ) then $
                      tmpval=isp2dec(pktdata(k).data[12:13]) else $
                      tmpval=tmp(i).val
		      if dejavu then mnem.(i)=tmpval  $
			else mnem=add_tag(mnem,tmpval,tmpname)
                endfor
                if (k eq 0) then begin
		    isp=create_struct('mnem',mnem,'head',head)
                    isp=replicate(isp,nstruct)
		endif else begin
		   isp(k).head=head
                   isp(k).mnem=mnem
		endelse
	endelse
endfor
isp2=isp
return, isp

end
