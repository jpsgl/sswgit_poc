;+
pro run_sequence_packet_test, logfn, pktfil, n_img=n_img
;
;	29-Apr-2013	DSS	comments; moved isp generation to a separate routine; added info struct for converted isp data; added SUVI_GPDS and LOGGING common blocks
;
;
;
;
;
;
;-
if (n_elements(logfn) eq 0) then stop, 'Must specify log file name.  Cannot continue."

COMMON LOGGING, LOGID, QDEBUG
COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT, STATIC_PARAMS, MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, FOCAL_PLANE_FILTER_TRANSMISSION, SUVI_LINEARITY, SUVI_GAIN_RIGHT, SUVI_GAIN_LEFT
COMMON OUT_METADATA, DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, PHOT_ENG_CONV, ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, SN_LEV3, FULL_WELL, SHT_EXP_TIME, IMG_PKT_TIME
COMMON SUVI_GPDS, ISP, ISP_MAP, INFO

qdebug=0
qswap=is_member(/swap_os)

;;;-open logfile for writing
openw, logid, logfn, /APPEND, WIDTH=250, /get_lun
printf, logid, '---------------------------------------------------------------------------'
printf, logid, '||  Begin SUVI Ground Processing Development System Testing for '+ systime()+'  ||'
printf, logid, '---------------------------------------------------------------------------'
printf, logid, ' '

;;;-run setup and time it
t0=systime(1)
setup_suvi_gpa
t1=systime(1)
printf, logid, 'Setup took ',t1-t0,' seconds (',(t1-t0)/60,') min' 
printf, logid, ' '

;;;-path to L0 fits output
if not exist(topdir) then topdir = '/disk1/egsesw/idl/gpa/gpa'
imgpath = concat_dir(topdir, 'test_images/L0')
fitspath=imgpath

;;;-path to packets
;pktdir = './test_images/packet' ; concat_dir(get_logenv('SUVI_DATA_L1B'), 'packet')
;pktdir = '/sanhome/suvi/log/packets/0x032a' ; concat_dir(get_logenv('SUVI_DATA_L1B'), 'packet')
 pktdir = './'
help,imgpath,pktdir

;;;-default packet file if not provided in args
;pktfname='20130429_185843_CDRL83_RevA.0x032a'
 pktfname='20150414.0x032a'
if (n_elements(pktfil) eq 1) then pktfname=pktfil

;;;-check for non-incremental image serial number
ispisni = -1

;;;-output image stats to file
statfile=pktdir+'GPDS_suviimg_stats.txt'
openw, statlun, /get_lun, statfile
printf, statlun, 'ApID', 'Date', 'Time', 'TAI', 'SerialNo', 'Naxis1', 'Naxis2', $
	'Mean', 'Median', 'Min', 'Max', 'StdDev', 'InMax', 'InMean', 'InStdDev', $
	format = '(a10, a11, a9, a14, a10, 10a12)'
close, statlun
free_lun, statlun

;;;-print to console
print, 'Reading from packet '+pktfname
printf, logid, 'Reading from packet '+pktfname

;;;-open the packet file for reading
;openr, lun, concat_dir(pktdir, pktfname), /get_lun
print, 'pktfname = ' + pktfname
;STOP
openr, lun, pktfname, /get_lun

;;;-define image packet structure
hdr_dwords=6
data_words=4074
data_bytes=data_words*2
;pkt_template is original kapusta structure
;pkt_template = {pkt, prihdr:BYTARR(6), sechdr:BYTARR(14), imghdr:LONARR(hdr_dwords), imgdata:INTARR(data_words)}
;pkt_stuct is sabolish updated structure
pkt_struct={science_packet, ccsds_prime:bytarr(6), ccsds_sec:bytarr(14), data_head:bytarr(24), data:bytarr(data_bytes)}

;;;-define pixel vector and required static FITS keywords for L0
img_template = intarr(1722604)
keyword1 = string( format = '( "SIMPLE  =                    T", 49x, a1 )', " " )
keyword2 = string( format = '( "BITPIX  =                   16", 49x, a1 )', " " )
keyword3 = string( format = '( "NAXIS   =                    2", 49x, a1 )', " " )
fitsend = 'END' + string( replicate(32b,77) ) 	;	Append after the last FITS keyword

;;;-Error handler (especially for EOF errors)
catch, error_status
if error_status ne 0 then begin
	print, 'run_sequence_packet_test: error encountered...'
	close,lun
	free_lun, lun
	close, logid
	free_lun, logid
	stop, !error_state.msg
endif

;;;-associate packet structure with image packet file
record=assoc( lun, pkt_struct )

;;;-associate sabolish struct with pkt file, handle record
;openr, ll, pktdir+pktfname, /get_lun
;record=assoc(ll,pkt_struct)

;;;-check and double-check that you aren't at the end of the packet file
if (n_elements(recnum) eq 0) then recnum=0l
if (recnum gt 0) then testrecnum=recnum-1 else testrecnum=0
testrec=record(testrecnum)
if EOF(lun) then begin					;	Were you already off the end?
	print, 'run_sequence_packet_test: File already finished...'
	close, lun
	free_lun, lun
	close, logid
	free_lun, logid
	return
endif
testrec = record(recnum)
if EOF(lun) then begin					;	Were you sitting at the last record?
	print, 'run_sequence_packet_test: File already finished...'
	close, lun
	free_lun, lun
	close, logid
	free_lun, logid
	return
endif

;;;-initialize display window so it doesn't pop up for every image
;window, 14, xsize = 1330, ysize = 1330

;;;-read each packet, calculate start pixel number and copy packet data to image array
thisrecnum=recnum
;pktdata=rec[thisrecnum]
packet=record(thisrecnum)
newfile=0
writeimg=0
if not exist(n_img) then n_img = 1000
num_imgs=0
newisp=0
while ( (not EOF(lun)) and (num_imgs le n_img) ) do begin 
	packet=record(thisrecnum)
	title = '********** TEST CASE ' + strtrim(num_imgs,2) + '***************'
	printf, logid, title
	tstart_pkt=systime(1)
	printf, logid, 'Starting L0 Packet Reading...'

;;;-construct file name from date
	apid = ( ( packet.ccsds_prime[0] and 3 ) * 256 ) + packet.ccsds_prime[1]
	julian_day_epoch = 2451545D	 ;   this is 1200 UT on 1-1-2000? Seems to give the right answer, though
	daynum = packet.ccsds_sec[0]*65536l + packet.ccsds_sec[1]*256l + packet.ccsds_sec[2]
	msec = packet.ccsds_sec[3]*16777216l + packet.ccsds_sec[4]*65536l + packet.ccsds_sec[5]*256l + packet.ccsds_sec[6] 
	usec = packet.ccsds_sec[7]*256l + packet.ccsds_sec[8] 
	
	jul_day = julian_day_epoch + double( daynum ) + double( msec ) / 86400000.0D
	CALDAT, jul_day, mn, dy, yr, hr, mi, sec
		anystring = string( format = '(i4.4, "-", i2.2, "-", i2.2, " ", i2.2, ":", i2.2, ":", f6.3)',$
		yr, mn, dy, hr, mi, sec )

;;;-below is FOR TESTING ONLY: Need to offset date to be able to use dark frame data, irradiance data, etc
;	yr_test=2012
;	mn_test=2
;	dy_test=20
;;;-above is FOR TESTING ONLY

;;;-continue making packet name
;	img_tstr = strtrim(hr,1)+':'+strtrim(mi,1)+':'+strtrim(sec,1)+' '+strtrim(yr_test,1)+'/'+strtrim(mn_test,1)+'/'+strtrim(dy_test,1)
	img_tstr = strtrim(hr,1)+':'+strtrim(mi,1)+':'+strtrim(sec,1)+' '+strtrim(yr,     1)+'/'+strtrim(mn,     1)+'/'+strtrim(dy,     1)
	img_time=anytim(img_tstr, /yohkoh)
	img_pkt_time=img_time
	thistai = anytim2tai(anystring)

;;;-read the packet data header, dataset=isn
	hdr=mk_suvi_isp_struct(packet,/head_only)
;STOP, 'STOP 1'
	dataset=hdr.head.suv_isp_hdr_isn
	date=string(format='(i4.4, i2.2, i2.2)', yr, mn, dy)
	time=string(format='(i2.2, i2.2, i2.2)', hr, mi, sec)
	serialno=string(format='(i7.7)', dataset)
	this_img_name=date+'_'+time+'_'+serialno+'.fits'
	if (n_elements(img_names) eq 0) then img_names=this_img_name else img_names=[img_names, this_img_name]
	print, "Img Names: ", this_img_name 

	img=img_template

;;;-loop through and assign image data to pixels
	while not newfile do begin
		; Note that the 16-bit words are swapped. Examples: Packet Flag is at i16[3]
		; instead of i16[2]. Packet Number is at i16[5] instead of i16[4].	 
;		i16=unsign(fix(swap_endian( pktdata.imghdr),0,12))	; view header as 16bit words
;		i32=swap_endian( pktdata.imghdr)	; view header as 32bit dwords
		;print,"IMG HDR info i16, i32:", i16,i32
;;;-read in packet data header	 
		packet=record(thisrecnum)
	 	hdr=mk_suvi_isp_struct(packet,/head_only)
;STOP, 'STOP 2'
		pkt_num=hdr.head.suv_isp_hdr_pkt_num
		dataset=hdr.head.suv_isp_hdr_isn
;;;-pkt_num eq 0 indicates packet contains an ISP
		if (pkt_num eq 0) then begin
			newisp=1
			print, ""
			print, "Found leading ISP packet. Moving on to 1st image data packet"
			; need to remap
;			i16isp=unsign(fix(swap_endian(pktdata.imgdata)))	; view pkt as 16bit words
;			i32isp=swap_endian(ulong(pktdata.imgdata,0,2037))	; view pkt as 32bit works
;			tmpi = i32[0]				; new value

;;;-flag non-sequential ISN jumps
			tmpi=hdr.head.suv_isp_hdr_isn
			if (ispisni ne -1) then begin
				isndiff=tmpi-ispisni		; should always be 1, except isn is set
				if (isndiff eq 1) then ispisn=string(tmpi) else ispisn=string(tmpi)+'*'
			endif else begin
		 		ispisn=string(tmpi)
			endelse 
		 
;;;-generate ISP structre and the metadata
;			isp=mk_suvi_isp_struct(packet)
;			info=mk_suvi_info_struct(isp)
;STOP, 'STOP 3'
			isp_record_number=thisrecnum
			thisrecnum = thisrecnum+1l
;			pktdata=rec[thisrecnum]
			continue  ; next iteration of while loop
		endif

;;;-image Data packet number starts at 1 (0 is reserved for copy of ISP)
		num_pixls=pkt_num*data_words
		indx=(pkt_num-1)*data_words
;		img[indx:indx+(data_words-1)]=pktdata.imgdata
		imgwords=fix(packet.data,0,data_words)
		img(indx:indx+data_words-1)=imgwords
;		flag = i16[3]
		flag=hdr.head.suv_isp_hdr_flg
;		print, pkt_num, indx, indx + (data_words - 1), i16, i32, $
;		format = '("Packet No.: ", i5, "  Image Array Start/End indices: ",i0,"/",i0,". IMG HDR info:", /, "i16 = ", 12(Z4.4,x), /, "i32 = ", 6(Z8.8,x))'

;;;-Read next record
		pkt_num=hdr.head.suv_isp_hdr_pkt_num
		thisrecnum = thisrecnum+1l
;		pktdata=rec[thisrecnum]
;		dataset_new = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x
		packet_new=record(thisrecnum)
		hdr_new=mk_suvi_isp_struct(packet_new,/head_only)
;STOP, 'STOP 4'
		dataset_new=hdr_new.head.suv_isp_hdr_isn
		if (dataset_new ne dataset) then begin
			newfile=1
			if (newisp) then begin
				writeimg=1
				newisp=0
			endif else printf, logid, 'Partial image did not contain an ISP."
			print, pkt_num, $
			format='("New image header detected after Packet No. ", i0)'
		endif
		if EOF(lun) then begin
			newfile=1
			print, "Found EOF"
			if ((flag eq 3) and (newisp eq 1)) then writeimg=1
			; Capture data if boundary case: last packet added to img[] was the
			; next-to-last packet of the entire image, and the last packet of the
			; entire image ends up EOF.
;			i16_nxt = unsign(fix(swap_endian( pktdata.imghdr),0,12))
;			flag_nxt = i16_nxt[3]
			flag_nxt=hdr_new.head.suv_isp_hdr_flg
			if (flag_nxt eq 3) then begin
				if (newisp) then writeimg=1
				pkt_num_nxt=hdr_new.head.suv_isp_hdr_pkt_num
;				pkt_num_nxt = i16_nxt[5]  ; note words are swapped
				; Last packet may not be full of data
				data_words_nxt=2*hdr_new.head.suv_isp_hdr_data_sz32
				exp_data_words=(hdr_new.head.suv_isp_hdr_rows*hdr_new.head.suv_isp_hdr_cols)-pkt_num*data_words
;				data_words_nxt = 2*i16_nxt[4]
;				exp_data_words = (i16_nxt[10] * i16_nxt[11]) - pkt_num * data_words
				if (data_words_nxt ne exp_data_words) then begin
					print, data_words_nxt, exp_data_words, $
					format='("ERROR: Header-specified size (",i5,") of data in last packet does not match expected size (",i4,"). Using expected size")'
					data_words_nxt=exp_data_words
				endif
				indx=pkt_num*data_words ; next-to-last packet was full
;				img[indx: indx + (data_words_nxt-1)] = pktdata.imgdata[0:data_words_nxt-1]
;				i32_nxt=swap_endian(pktdata.imghdr)
				imgwords=fix(packet.data,0,data_words)
				img(indx:indx+data_words-1)=imgwords
;				print, pkt_num_nxt, indx, indx + (data_words_nxt - 1), $
;				i16_nxt, i32_nxt, $
;				format = '("Packet No.: ", i5, "  Image Array Start/End indices: ",i0,"/",i0,". IMG HDR info:", /, "i16 = ", 12(Z4.4,x), /, "i32 = ", 6(Z8.8,x))'
			endif
		endif
;if ((thisrecnum mod 50) eq 0) then stop
	endwhile
	newfile=0

	; If you have a full image,write stats and FITS file
	if (writeimg) then begin
		writeimg=0
		recnum=thisrecnum
		; Turn the pixel vector into a 2D image array
		version = packet.ccsds_sec[9]*256l + packet.ccsds_sec[10]
		; Get number of rows and columns from last image packet
		;hdr=mk_suvi_isp_struct(record(thisrecnum),/head_only)
		hdr=mk_suvi_isp_struct(packet,/head)
;STOP, 'STOP 5'
		dim1=hdr.head.suv_isp_hdr_cols
		dim2=hdr.head.suv_isp_hdr_rows
;		dim1 = i16[10]  ; columns (words are swapped in i16[])
;		dim2 = i16[11]  ; rows
		print, "Rows and columns read from image packet header"
		outimage=uintarr(dim1, dim2)
		outimage_lastpix=dim1*dim2-1
		outimage[0]=img[0:outimage_lastpix]

	; Calculate image statistics and write them to a text file

		immean = MEAN(outimage)
		immedian = MEDIAN(outimage)
		immax = MAX(outimage)
		immin = MIN(outimage)
		imstdev = STDEV(outimage)
		subimg = outimage[50:dim1-51,4:dim2-3]
		simmean = MEAN(subimg)
		simmax = MAX(subimg)
		simstdev = STDEV(subimg)
		OPENW, statlun, /append, /get_lun, statfile
		PRINTF, statlun, apid, date, time, thistai, serialno, dim1, dim2, $
		immean, immedian, immin, immax, imstdev, simmax, simmean, simstdev, $
		format = '(a10, a11, a9, i14, a10, 2i12, 2f12.1, 2i12, f12.3, i12, 2f12.3)'
		close, statlun
		FREE_LUN, statlun

		; Generate a header and write a FITS file
		keyword4 = STRING( format = '( "NAXIS1  =                 ", i4.4, 49x, a1 )', dim1, " " )
		keyword5 = STRING( format = '( "NAXIS2  =                 ", i4.4, 49x, a1 )', dim2, " " )
		fitshead = [keyword1, keyword2, keyword3, keyword4, keyword5, fitsend]

		WRITEFITS, fitspath + this_img_name, outimage, fitshead
		printf, logid, '  Wrote ',this_img_name

;**************************************************
;Start GPA Testing to Process L0 image to L1b image
;**************************************************

		tend_pkt=systime(1)
		printf, logid, '  Producing L0 Image from Packet Files took: ', tend_pkt-tstart_pkt, ' seconds'
		printf, logid, ' '
		tstart_gpa=systime(1)
help, isp, info
		isp=mk_suvi_isp_struct(record(isp_record_number))
		info=mk_suvi_info_struct(isp)
help, isp, info
;STOP, 'STOP 6'
		printf, logid, '   Test Parameters: '
		printf, logid, '     - ', info.asname, 'image with ', strtrim(info.sht_exp_time,1), ' second exposure.'
		printf, logid, '     - Observation date/time is at '+ strtrim(info.img_pkt_time,1)
		printf, logid, '     - CCD Temperature is '+strtrim(info.ccdtemp1,1)
		printf, logid, '     - Last Bakeout occurred on '+strtrim(info.time_last_bakeout,1)

                print, '   Test Parameters: '
                print, '     - ', info.asname, 'image with ', strtrim(info.sht_exp_time,1), ' second exposure.'
                print, '     - Observation date/time is at '+ strtrim(info.img_pkt_time,1)
                print, '     - CCD Temperature is '+strtrim(info.ccdtemp1,1)
                print, '     - Last Bakeout occurred on '+strtrim(info.time_last_bakeout,1)
;STOP, 'STOP 7: Stopping before suv_ceb_rdout_port reference.'
		IF isp.mnem.suv_ceb_rdout_port EQ 0 THEN printf, logid,  '     - Image read out of the Left CCD Register (Amplifier).' ELSE  printf, logid,  '     - Image read out of the Right CCD Register (Amplifier).'
		printf, logid, ' '
		printf, logid, '   Test Output: '
		input_img = fitspath+this_img_name
		output_img = 'SUVI_FM1_'+date+time+'_'+info.asname+'_1b.fits'
;STOP, 'STOP 8: Stopping before SUVI_L1b_Processing call.'
		SUVI_L1b_Processing, input_img, output_img
;STOP, 'STOP 9: Stopping after  SUVI_L1b_Processing call.'
		tend_gpa=systime(1)
		printf, logid, ' '
		printf, logid, '  Completed and wrote', output_img,' in ', tend_gpa-tstart_gpa, 'seconds'
		printf, logid, ' '
	ENDIF
	num_imgs=num_imgs+1

endwhile 

te=systime(1)
printf, logid, '----------------------------------------------------'
printf, logid, ' '
printf, logid, 'Completed run_sequence_packet_test in', te-t0, ' seconds (',(te-t0)/60,') min' 
close, logid
free_lun, logid

close, lun
free_lun, lun

print, 'Done'

end
