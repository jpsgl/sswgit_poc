
pro mk_poster, waves, do_little=do_little, do_write=do_write, $
               do_annotate=do_annotate, do_stop=do_stop

dir_top_in = '/Users/slater/soft/idl_temp/suvi/plt/temp/SUVI_20170418_2'
;dir_top_out = '/Users/slater/soft/idl_temp/suvi/plt/temp/poster_images'
dir_top_out = '/sanhome/slater/public_html/suvi/poster_images_beta'
files = file_list(dir_top_in, 'suvi_double_res_???.png')

if not exist(waves) then waves = ['171','195','284','131','304','094']
n_waves = n_elements(waves)

file_waves = strmid(files, 6, 3, /rev)

for i=0, n_waves-1 do begin
   ss_match = where(file_waves eq waves[i], n_match)
   if n_match eq 1 then begin
      READ_PNG, files[ss_match], img, r0, g0, b0, /verb
      
      if keyword_set(do_little) then begin
         tvlct, r0, g0, b0
         lil_img = rebin(img, 2560/4, 2560/4, /samp)
         xstepper, [[[lil_img]],[[lil_img]]]
         imgr = bytarr(640, 640)
         imgr[3,3] = lil_img[3:636,3:636]
         imgr = rot(imgr, -25.724490, missing=0)
         xstepper, [[[imgr]],[[imgr]]]

;        wdef,0,640
;        tv, lil_img
;        STOP

; Iteratively edit annotation:
;        if keyword_set(do_annotate) then begin
         if 1 eq 2 then begin
            ans = 'y'
            while ans eq 'y' do begin
               set_plot, 'z'
               device, set_resolution = [640,640]
               tv, lil_img
;              time_string = '18-April-2017' ; anytim(indexp.date_obs, /ccsds)
;              label = ' GOES-R SUVI Solar Corona ' + string(wave0, '$(i4.4)') + 'A ' + time_string
               label = 'SUVI  THE HIGH CORONA'
               xyouts, 10, 75, label, charsize=1.5, color=255, /device, charthick=2
               img_an = tvrd()
               set_plot, 'x'
               xstepper, [[[img_an]],[[img_an]]]
               read, 'Tweek again? ', ans
            endwhile
            STOP
         endif

         xsiz_eimg = ceil((sin(25.724490/!radeg) + cos(25.724490/!radeg))*640)
         eimg = bytarr(xsiz_eimg, xsiz_eimg)
         eimg[(xsiz_eimg-640)/2+4, (xsiz_eimg-640)/2+4] = lil_img[3:636,5:636]
         eimgr = rot(eimg, -25.724490, missing=0)
         xstepper, [[[eimgr]],[[eimgr]]]
      endif else begin
         imgr = bytarr(2560, 2560)
         imgr[3,3] = img[3:2556,3:2556]
         imgr = rot(imgr, -25.724490, missing=0)

         xsiz_eimg = ceil((sin(25.724490/!radeg) + cos(25.724490/!radeg))*2560)
         eimg = bytarr(xsiz_eimg, xsiz_eimg)
         eimg[(xsiz_eimg-2560)/2+4, (xsiz_eimg-2560)/2+4] = img[3:2556,3:2556]
         eimgr = rot(eimg, -25.724490, missing=0)

; Add Annotation:
;        time_string = '18-April-2017' ; anytim(indexp.date_obs, /ccsds)
;        label = ' GOES-R SUVI Solar Corona ' + string(wave0, '$(i4.4)') + 'A ' + time_string
         label = 'SUVI  THE HIGH CORONA'

         set_plot, 'z'
         label = 'SUVI: THE HIGH EUV CORONA'
         device, set_resolution = [2560, 2560]
         tv, img
         xyouts, 240, 200, label, charsize=11, color=255, /device, charthick=5
         img_an = tvrd()
         set_plot, 'x'

         set_plot, 'z'
         label = 'SUVI: THE HIGH EUV CORONA'
         device, set_resolution = [2560, 2560]
         tv, imgr
         xyouts, 240, 200, label, charsize=11, color=255, /device, charthick=5
         imgr_an = tvrd()
         set_plot, 'x'

         set_plot, 'z'
         label = 'SUVI: THE HIGH EUV CORONA'
         device, set_resolution = [2560, 2560]
         tv, img
         xyouts, 240, 200, label, charsize=11, color=255, /device, charthick=5
         img_an = tvrd()
         set_plot, 'x'

         set_plot, 'z'
         label = 'SUVI: THE HIGH EUV CORONA'
         device, set_resolution = [2560, 2560]
         tv, img
         xyouts, 240, 200, label, charsize=11, color=255, /device, charthick=5
         img_an = tvrd()
         set_plot, 'x'

         if keyword_set(do_write) then begin

            filnam_out = 'suvi_poster_image_' + string(waves[i], '$(i4.4)') + '.png'
            file_out = concat_dir(dir_top_out, filnam_out)
            WRITE_PNG, file_out, img, r0, g0, b0, order=order, $
                       transparent=t0, /verbose, xres=xres, yres=yres

            filnam_out = 'suvi_poster_image_' + string(waves[i], '$(i4.4)') + '_derolled.png'
            file_out = concat_dir(dir_top_out, filnam_out)
            WRITE_PNG, file_out, imgr, r0, g0, b0, order=order, $
                       transparent=t0, /verbose, xres=xres, yres=yres

            filnam_out = 'suvi_poster_image_' + string(waves[i], '$(i4.4)') + '_embedded.png'
            file_out = concat_dir(dir_top_out, filnam_out)
            WRITE_PNG, file_out, eimg, r0, g0, b0, order=order, $
                       transparent=t0, /verbose, xres=xres, yres=yres

            filnam_out = 'suvi_poster_image_' + string(waves[i], '$(i4.4)') + '_derolled_embedded.png'
            file_out = concat_dir(dir_top_out, filnam_out)
            WRITE_PNG, file_out, eimgr, r0, g0, b0, order=order, $
                       transparent=t0, /verbose, xres=xres, yres=yres
         endif

      endelse
   endif else begin
      print, 'No match found for specified wave: ' + strtrim(waves[i],2)
      print, 'Stopping.'
      STOP
   endelse
   if keyword_set(do_stop) then STOP

   delvarx, img, lil_img, imgr, eimg, eimgr

endfor

end

