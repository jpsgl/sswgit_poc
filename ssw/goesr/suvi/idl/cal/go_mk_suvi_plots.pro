
pro go_mk_suvi_plots, pkt_fil, ss_img=ss_img

if get_logenv('SUVI_TLM') eq '' then $
   set_logenv, 'SUVI_TLM', '/archive1/suvi/suvi_data/fm1'

;if not exist(pkt_fil) then begin
;   files = file_list(get_logenv('SUVI_TLM'), '*.0x032a')
;   filnams = file_break(files)
;   ss_fil = xmenu_sel(filnams)
;   pkt_fil = files(ss_fil)
;endif

mk_suvi_fits, pkt_fil, ss_img=ss_img, /do_index_arr, iindex_arr, $
              this_img_name=this_img_name, interactive=interactive, $
              _extra=_extra

img_pkt_time = iindex_arr.img_pkt_time
SUV_OTS01P_TEL_FWD    = iindex_arr.SUV_OTS01P_TEL_FWD
SUV_OTS02P_TEL_AFT    = iindex_arr.SUV_OTS02P_TEL_AFT
SUV_OTS06P_FPA_CCD    = iindex_arr.SUV_OTS06P_FPA_CCD
;SUV_PPCI_MEM_SBIT_ERR = iindex_arr.SUV_PPCI_MEM_SBIT_ERR
;SUV_PPCI_MEM_DBIT_ERR = iindex_arr.SUV_PPCI_MEM_DBIT_ERR
;SUV_SWA_MEM_ERR_CNT   = iindex_arr.SUV_SWA_MEM_ERR_CNT
;SUV_SWA_MISS_TMCD_CNT = iindex_arr.SUV_SWA_MISS_TMCD_CNT
;SUV_SWA_REXMIT_ENG    = iindex_arr.SUV_SWA_REXMIT_ENG

; Conversion:   83.5055 + -1.23147E-01x + 9.11876E-05x^2  + -4.17058E-08x^3  + 9.70631E-12x^4 + -9.09571E-16x^5
; Conversion:   83.5055 + -1.23147E-01x + 9.11876E-05x^2  + -4.17058E-08x^3  + 9.70631E-12x^4 + -9.09571E-16x^5
; Conversion: -166.1905 +  0.05438212x  + 1.370277E-06x^2 + -6.595168E-11x^3 + 4.0E-15x^4     +  0.0x^5

!p.multi = [0,1,3]
wdef, 0, 1024, 1024
utplot, img_pkt_time, SUV_OTS01P_TEL_FWD, /ynoz
utplot, img_pkt_time, SUV_OTS02P_TEL_AFT, /ynoz
utplot, img_pkt_time, SUV_OTS06P_FPA_CCD, /ynoz
STOP
end
