function swap_time2files_local,t0,t1,minute_minute_cadence,minute_cadence,hour_cadence=hour_cadence, hours_cadence=hours_cadence, $
   debug=debug, crontree=crontree, old_paradigm=old_paradigm


crontree=keyword_set(crontree) or 1-keyword_set(old_paradigm) ; crontree is new default, 3-jan-2016

debug=keyword_set(debug)
if keyword_set(crontree) then begin 
   allfiles=ssw_time2filelist(t0,t1,parent='/archive1/proba2/swap',count=count)
   alltimes=file2time(allfiles)
endif else begin 
   swap_top=concat_dir('$ssw_client','swap_els')
   files=findfile(swap_top)
   sst=where(strmatch(files,'proba*_20*',/fold),count)
   files=files[sst] 
   times=file2time(files)
   order=sort(anytim(times))
   files=files[order]
   times=times[order]
   count=0
   d0=where(anytim(times) ge anytim(t0[0]),dcnt)
   if dcnt eq 0 then begin 
      box_message,'No directories >= your starttime'
      return,-1
   endif
   dirs=concat_dir(swap_top,files[d0])
   times=times[d0]
   lists=concat_dir(dirs,'swap_els.list')
   lists=lists[where(file_exist(lists))]

   allfiles=rd_tfiles(lists,filemap=filemap)
   alltimes=file2time(allfiles)
   order=sort(anytim(alltimes))
   allfiles=allfiles[order]
   alltimes=alltimes[order]
endelse


case 1 of 
   keyword_set(minute_cadence):
   keyword_set(hour_cadence):
   keyword_set(hours_cadence): hour_cadence=hours_cadence
   else: begin 
      retval=allfiles ; no cadence, return all
   endcase
endcase

if n_elements(retval) eq 0 then begin ; apply requested cadence
   ss=grid_data(alltimes,minutes=minute_cadence,hours=hour_cadence,/ss)
   retval=allfiles[ss]
endif

return,retval
end





