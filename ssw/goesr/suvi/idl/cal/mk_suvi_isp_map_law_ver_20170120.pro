
function mk_suvi_isp_map, infil, dat=dat

if get_logenv('SUVI_EXTERNAL_DATA') eq '' then $
  set_logenv, 'SUVI_EXTERNAL_DATA', './'

if not exist(infil) then $
;  infil = concat_dir(get_logenv('SUVI_EXTERNAL_DATA'), 'isp_TandCrevD.txt')
infil = '~/soft/idl_temp/suvi/plt/temp/mk_suvi_isp_map_law_ver_20170120.pro'  ;;read thyself
;print,'reading ',infil
buff0 = rd_tfile(infil)
x=strpos(buff0,';;;beginning of isp_TandCrevD.txt')  ;find the beginning of the table; note this line is the first occurance of the search parameter!!!
skip=where(x gt -1)
skip = skip(1)+2		;; skip the first occurance skip(0) and skip another 2 lines to get to the actual table

;;buff1 = rd_tfile(infil,6,1)
buff1 = rd_tfile(infil,7,skip)  ;; read 7 column data, 1st column is the ';' for the idl comment
;;dat   = buff1(1:*,*)
dat   = buff1(2:*,*)	     ;; get rid of the 'Num' column too I guess looking at the original code.

dum = {pkt_map, name:'', stbyte:0, stbit:0, nbits:0, type:''}

hdr  = dat(*,0:8)
dat  = dat(*,9:*)
len  = n_elements(dat(0,*))
head = replicate({pkt_map},9)
tlm  = replicate({pkt_map},len)

head.name   = reform(hdr(0,*))
head.stbyte = reform(fix(hdr(1,*))-20)
head.stbit  = reform(fix(hdr(2,*)))
head.nbits  = reform(fix(hdr(3,*)))
head.type   = reform(hdr(4,*))

tlm.name    = reform(dat(0,*))
tlm.stbyte  = reform(fix(dat(1,*))-44)
tlm.stbit   = reform(fix(dat(2,*)))
tlm.nbits   = reform(fix(dat(3,*)))
tlm.type    = reform(dat(4,*))

;tlm = add_tag(tlm,head,'head')
;tlm.head = head

isp_map = create_struct('tlm', tlm, 'head', head)

;STOP
return, isp_map

end

;;;beginning of isp_TandCrevD.txt
; Num	  Mnemonic	   Start Byte	  Start Bit	  Num bits	  Type    Description	  Units
; 1	   SUV_ISP_HDR_ISN	  20	  0	  32	  UL1	  Image Summary Packet Header image serial number.	  
; 2	   SUV_ISP_HDR_FLG	  24	  0	  16	  IU1	  Image Summary Packet Header image data sequence flag = 0=SINGLE Packet  
; 3	   SUV_ISP_HDR_IMG_TYPE   26	  0	  16	  IU1	  "Image Packet Header image type, 10 = Mission data; 11 = Non-Mission Sequence Data (taken in OPS mode), 12 = Single image (Acquired by Take Picture command, not by sequence). "	  
; 4	   SUV_ISP_HDR_PKT_NUM    28	  0	  16	  IU1	  Image Summary Packet Header packet number (0=ISP).	  
; 5	   SUV_ISP_HDR_DATA_SZ32  30	  0	  16	  IU1	  Image Summary Packet Header size of packet data (does not include header data)  
; 6	   SUV_ISP_HDR_SPIX	  32	  0	  32	  UL1	  Image Summary Packet Header start pixel number of current packet	  
; 7	   SUV_ISP_HDR_EPIX	  36	  0	  32	  UL1	  Image Summary Packet Header end pixel number of current packet. 
; 8	   SUV_ISP_HDR_ROWS	  40	  0	  16	  IU1	  Image Summary Packet Header image number of rows.	  
; 9	   SUV_ISP_HDR_COLS	  42	  0	  16	  IU1	  Image Summary Packet Header image number of columns.    
; 10	   SUV_AS_MTR_STS 44	  0	  32	  UL1	  Aperture selector status word.  
; 11	   SUV_AS_ENC_BITS	  46	  0	  6	  UB	  Aperture selector encoder bits 0-8 of the status word.  
; 12	   SUV_AS_DONE    47	  1	  1	  UB	  Aperture selector DONE bit of the status word.  
; 13	   SUV_AS_ENC_ALWAYS_ON   47	  3	  1	  UB	  Aperture selector encoder always on bit of the status word.	  
; 14	   SUV_AS_ENC_CHAN_A	  47	  7	  1	  UB	  Aperture selector encoder channel A bit of the status word.	  
; 15	   SUV_AS_ENC_CHAN_B	  47	  6	  1	  UB	  Aperture selector encoder channel B bit of the status word.	  
; 16	   SUV_AS_ENC_CHAN_C	  47	  5	  1	  UB	  Aperture selector encoder channel C bit of the status word.	  
; 17	   SUV_AS_SPIN_TMR_DONE   47	  2	  1	  UB	  Aperture selector spin timer done bit of the status word.	  
; 18	   SUV_AS_STEP_MODE	  47	  4	  1	  UB	  Aperture selector step mode bit of the status word.	  
; 19	   SUV_AS_STUCK   47	  0	  1	  UB	  Aperture Stuck bit of the status word.  
; 20	   SUV_AS_ODOMETER	  48	  0	  32	  UL1	  Aperture selector odometer.	  
; 21	   SUV_AS_CMDED_DELAY	  52	  0	  16	  IU1	  Commanded delay setting for Aperture Selector motor.    
; 22	   SUV_AS_CMDED_ENC_CFG   54	  0	  16	  IU1	  Commanded encoder setting for Aperture Selector Motor.  
; 23	   SUV_AS_CMDED_TGT	  56	  0	  16	  IU1	  Commanded target position for Aperture Selector motor.  
; 24	   SUV_AS_POS	  58	  0	  16	  IU1	  Aperture Selector current software position.    
; 25	   SUV_AS_SM_STAT 60	  0	  16	  IU1	  Aperture Selector state machine state.  
; 26	   SUV_AS_SPIN_TIME	  62	  0	  16	  IU1	  Aperture Selector spin time.    
; 27	   SUV_AS_MACHINE_STATE   64	  0	  8	  UB	  "Aperture machine state. 0-normal, 1-move in progress, 2-step in progress, 3-step to target in progress, 4-spin in progress."   
; 28	   SUV_AS_STEP_WAIT_TIME  66	  0	  16	  IU1	  Aperture wait time to wait after each step move mSec
; 29	   SUV_CA_DMA_STS 68	  0	  32	  UL1	  CCD Readout DMA Status. 
; 30	   SUV_CA_FRM_EXP 72	  0	  32	  UL1	  Exposure time in milliseconds.  mSec
; 31	   SUV_CA_FRM_SROW	  76	  0	  16	  IU1	  Start row for CCD readout (1..1292).    
; 32	   SUV_CA_FRM_EROW	  78	  0	  16	  IU1	  End row for CCD readout (1..1292).	  
; 33	   SUV_CA_FRM_SCOL	  80	  0	  16	  IU1	  Start column for CCD readout (1..1330). 
; 34	   SUV_CA_FRM_ECOL	  82	  0	  16	  IU1	  End column for CCD readout (1..1330).   
; 35	   SUV_CA_FRM_TYPE	  84	  0	  8	  UB	  Frame type_ 0 = use current value_ default LIGHT_ 1 = LIGHT/DARK	  
; 36	   SUV_CA_FRM_EXT 85	  0	  8	  UB	  Extend mode_ TRUE/FALSE_ readout extended pixels and rows.	  
; 37	   SUV_CA_FRM_SUMMD	  86	  0	  8	  UB	  Pixel sum mode 0 = default = 1x1; 1= 2x2; 2= 4x4; 3= 8x8.	  
; 38	   SUV_CA_FRM_FLUSH	  87	  0	  8	  UB	  CCD flush; 0=NO; 1=YES  
; 39	   SUV_CA_MECH_AS 88	  0	  32	  UL1	  Aperture configuration position.	  
; 40	   SUV_CA_MECH_FOC	  92	  0	  32	  UL1	  Focus mechanism configuration position. 
; 41	   SUV_CA_MECH_FW1	  96	  0	  32	  UL1	  Filter Wheel 1 configuration position.  
; 42	   SUV_CA_MECH_FW2	  100	  0	  32	  UL1	  Filter Wheel 2 configuration position.  
; 43	   SUV_CA_PIC_COUNT	  104	  0	  32	  UL1	  Number of readouts performed.   
; 44	   SUV_CA_PIC_ST  108	  0	  32	  UL1	  Camera (picture taking) state machine state.    
; 45	   SUV_CA_TKP_APID	  112	  0	  32	  UL1	  APID for the take picture command.	  
; 46	   SUV_CA_TKP_EDONE_STS   116	  0	  32	  UL1	  Exposure done status of SUV_TAKE_PIC command.   
; 47	   SUV_CA_TKP_EOFRM_DAYS  120	  0	  32	  UL1	  End of frame mission time days of SUV_TAKE_PIC command. 
; 48	   SUV_CA_TKP_EOFRM_MSEC  124	  0	  32	  UL1	  End of frame mission time milliseconds of SUV_TAKE_PIC command. 
; 49	   SUV_CA_TKP_EOFRM_USEC  128	  0	  32	  UL1	  End of frame mission time microseconds of SUV_TAKE_PIC command. 
; 50	   SUV_CA_TKP_EXP_STS	  132	  0	  32	  UL1	  Exposure status of SUV_TAKE_PIC command.	  
; 51	   SUV_CA_TKP_FLUSH_STS   136	  0	  32	  UL1	  CCD flush status of SUV_TAKE_PIC command.	  
; 52	   SUV_CA_TKP_HDR_STS	  140	  0	  32	  UL1	  Send CCD header status of SUV_TAKE_PIC command. 
; 53	   SUV_CA_TKP_IMG_TYP	  144	  0	  32	  UL1	  "Image type, 10 = Mission data; 11 = Non-Mission Sequence Data (taken in OPS mode), 12 = Single image (Acquired by Take Picture command, not by sequence)."	  
; 54	   SUV_CA_TKP_ISP_STS	  148	  0	  32	  UL1	  Set before readout ISP data status of SUV_TAKE_PIC command.	  
; 55	   SUV_CA_TKP_LED_STS	  152	  0	  32	  UL1	  LED status of SUV_TAKE_PIC command.	  
; 56	   SUV_CA_TKP_MECH_STS    156	  0	  32	  UL1	  Mechanism status of SUV_TAKE_PIC command.	  
; 57	   SUV_CA_TKP_RDCFG_STS   160	  0	  32	  UL1	  CCD readout setup configuration status of SUV_TAKE_PIC command. 
; 58	   SUV_CA_TKP_RDONE_STS   164	  0	  32	  UL1	  CCD readout done status of SUV_TAKE_PIC command.	  
; 59	   SUV_CA_TKP_RDOUT_STS   168	  0	  32	  UL1	  CCD readout status of SUV_TAKE_PIC command.	  
; 60	   SUV_CA_TKP_ST  172	  0	  32	  UL1	  FSA CA take picture function state.	  
; 61	   SUV_CA_TKP_STS 176	  0	  32	  UL1	  Status of SUV_TAKE_PIC command. 
; 62	   SUV_DOOR_1_CMDED_DUR   180	  0	  16	  IU1	  Door motor 1 commanded duration.	  
; 63	   SUV_DOOR_1_CMDED_STEPS 182	  0	  16	  IU1	  Door 1 commanded number of steps.	  
; 64	   SUV_DOOR_1_SM_STAT	  184	  0	  16	  IU1	  Door motor 1 state machine state.	  
; 65	   SUV_DOOR_1_SW_STATE    186	  1	  8	  IU1	  Door motor 1 software state.    
; 66	   SUV_DOOR_1_GBX_SWTC_OVR	  187	  7	  1	  UB	  Door motor 1 gearbox switch override state (enable = 1; disable = 0)    
; 67	   SUV_DOOR_1_LTC_SWTC_OVR	  187	  6	  1	  UB	  Door motor 1 latch switch override state (enable = 1; disable = 0)	  
; 68	   SUV_DOOR_1_STS 188	  0	  32	  UL1	  Door 1 status word.	  
; 69	   SUV_DOOR_1_STEPS_COMPL 190	  0	  12	  IU1	  Door 1 status word - steps complete.    
; 70	   SUV_DOOR_1_CLOSED_SW   191	  6	  1	  UB	  Door 1 status word - Closed switch.	  
; 71	   SUV_DOOR_1_DONE	  191	  4	  1	  UB	  Door 1 status word - DONE bit.  
; 72	   SUV_DOOR_1_OPEN_SW	  191	  7	  1	  UB	  Door 1 status word - Open switch bit.   
; 73	   SUV_DOOR_1_UNLATCHED_SW	  191	  5	  1	  UB	  Door 1 status word - Unlatched switch.  
; 74	   SUV_DOOR_2_CMDED_DUR   192	  0	  16	  IU1	  Door motor 2 commanded duration.	  
; 75	   SUV_DOOR_2_CMDED_STEPS 194	  0	  16	  IU1	  Door 2 commanded number of steps.	  
; 76	   SUV_DOOR_2_SM_STAT	  196	  0	  16	  IU1	  Door motor 2 state machine state.	  
; 77	   SUV_DOOR_2_SW_STATE    198	  1	  8	  IU1	  Door motor 2 software state.    
; 78	   SUV_DOOR_2_GBX_SWTC_OVR	  199	  7	  1	  UB	  Door 2 gearbox switch override state (enable = 1; disable = 0)  
; 79	   SUV_DOOR_2_LTC_SWTC_OVR	  199	  6	  1	  UB	  Door 2 latch switch override state (enable = 1; disable = 0)    
; 80	   SUV_DOOR_2_STS 200	  0	  32	  UL1	  Door 2 status word.	  
; 81	   SUV_DOOR_2_STEPS_COMPL 202	  0	  12	  IU1	  Door 2 status word - Steps complete bits.	  
; 82	   SUV_DOOR_2_CLOSED_SW   203	  6	  1	  UB	  Door 2 status word - Closed switch bit. 
; 83	   SUV_DOOR_2_DONE	  203	  4	  1	  UB	  Door 2 status word - DONE bit.  
; 84	   SUV_DOOR_2_OPEN_SW	  203	  7	  1	  UB	  Door 2 status word - Open switch bit.   
; 85	   SUV_DOOR_2_UNLATCHED_SW	  203	  5	  1	  UB	  Door 2 status word - Unlatched switch bit.	  
; 86	   SUV_DL1_CMD_MAX_DUR    204	  0	  16	  IU1	  Door latch 1 commanded HOPA maximum duration.   
; 87	   SUV_DOOR_LATCH_1_PW_1  204	  0	  32	  UL1	  Door latch 1 parent mnemonic 1. 
; 88	   SUV_DL1_CMD_POST_DUR   206	  0	  16	  IU1	  Door latch commanded HOPA duration after unlatch.	  
; 89	   SUV_DOOR_LATCH_1_STS   208	  0	  32	  UL1	  Door latch 1 status	  
; 90	   SUV_DL1_UNLATCH_TIME   210	  0	  14	  IU1	  Door latch 1 status word - Unlatch time.	  
; 91	   SUV_DL1_HOPA_DONE	  211	  6	  1	  UB	  Door latch 1 status word - HOPA done (1 if HOPA is unpowered).  
; 92	   SUV_DL1_UNLATCHED_SW   211	  7	  1	  UB	  Door latch 1 status word - Unlatched switch.    
; 93	   SUV_DL2_CMD_MAX_DUR    212	  0	  16	  IU1	  Door latch 2 commanded HOPA maximum duration.   
; 94	   SUV_DOOR_LATCH_2_PW_1  212	  0	  32	  UL1	  Door latch 2 parent mnemonic 1. 
; 95	   SUV_DL2_CMD_POST_DUR   214	  0	  16	  IU1	  Door latch 2 commanded HOPA duration after unlatch.	  
; 96	   SUV_DOOR_LATCH_2_STS   216	  0	  32	  UL1	  Door latch 2 status.    
; 97	   SUV_DL2_UNLATCH_TIME   218	  0	  14	  IU1	  Door latch 2 status word - Unlatch time.	  
; 98	   SUV_DL2_HOPA_DONE	  219	  6	  1	  UB	  Door latch 2 status word - HOPA done (1 if HOPA is unpowered).  
; 99	   SUV_DL2_UNLATCHED_SW   219	  7	  1	  UB	  Door latch 2 status word - Unlatched switch.    
; 100	   SUV_FOCUS_MTR_STS	  220	  0	  32	  UL1	  Focus motor status	  
; 101	   SUV_FOCUS_STEPS_COMPL  222	  0	  10	  IU1	  Focus status word - Steps complete.	  
; 102	   SUV_FOCUS_CENTER_IND   223	  6	  1	  UB	  Focus status word - Center indicator (0 = decremented). 
; 103	   SUV_FOCUS_DONE 223	  3	  1	  UB	  Focus status word - DONE bit.   
; 104	   SUV_FOCUS_EDGE_LIMIT   223	  7	  1	  UB	  Focus status word - Edge limit (1 = out of range).	  
; 105	   SUV_FOCUS_ENC_AUTO_MODE	  223	  4	  1	  UB	  Focus status word - Encode mode (1 = always on).	  
; 106	   SUV_FOCUS_SW_OVERRIDE  223	  5	  1	  UB	  Focus status word - Switch override (1 = enabled).	  
; 107	   SUV_FOCUS_ODOMETER	  224	  0	  32	  UL1	  Focus motor odometer.   
; 108	   SUV_FOCUS_CMDED_ENC_CFG	  228	  0	  16	  IU1	  Focus motor commanded encoder setting.  
; 109	   SUV_FOCUS_MTR_CMDED_DUR	  230	  0	  16	  IU1	  Focus motor commanded duration. 
; 110	   SUV_FOCUS_SM_STAT	  232	  0	  16	  IU1	  Focus mechanism state machine state.    
; 111	   SUV_FOCUS_CMDED_STEPS  234	  0	  16	  IU1	  Focus mechanism commanded number of steps.	  
; 112	   SUV_FOCUS_CMDED_POS    236	  0	  16	  IS1	  Focus mechanism commanded position.	  
; 113	   SUV_FOCUS_POS  238	  0	  16	  IS1	  Focus mechanism current software position.	  
; 114	   SUV_FOCUS_MCH_STATE    240	  0	  8	  UB	  Focus machine state. 0-normal_ 1-increment move in progress_ 2- 
; 115	   SUV_FW1_CMDED_DELAY    244	  0	  16	  IU1	  Filterwheel 1 commanded delay.  
; 116	   SUV_FW1_CMDED_ENC_CFG  246	  0	  16	  IU1	  Filterwheel 1 commanded encoder setting.	  
; 117	   SUV_FW1_CMDED_TGT	  248	  0	  16	  IU1	  Filterwheel 1 commanded target position.	  
; 118	   SUV_FW1_POS    250	  0	  16	  IU1	  Filterwheel 1current software position. 
; 119	   SUV_FW1_SM_STAT	  252	  0	  16	  IU1	  Filterwheel 1 state machine state.	  
; 120	   SUV_FW1_SPIN_TIME	  254	  0	  16	  IU1	  Filterwheel 1 spin time.	  
; 121	   SUV_FW1_MACHINE_STATE  256	  0	  8	  UB	  "Filterwheel 1 machine state. 0-normal, 1-move in progress, 2-step in progress, 3-step to target in progress, 4-spin in progress."	  
; 122	   SUV_FW1_STEP_WAIT_TIME 258	  0	  16	  IU1	  Filterwheel 1 wait time to wait after each step mSec
; 123	   SUV_FW1_SPIN_ODOMETER  260	  0	  32	  UL1	  Filterwheel 1 spin odometer.    
; 124	   SUV_FW1_STS    264	  0	  32	  UL1	  Status of filterwheel 1 
; 125	   SUV_FW1_ENC_BITS	  266	  0	  9	  IU1	  Filterwheel 1 encoder bits.	  
; 126	   SUV_FW1_ENC_ALWYS_ON   267	  3	  1	  UB	  Filterwheel 1 encoder is always on bit. 
; 127	   SUV_FW1_ENC_CHAN_A	  267	  7	  1	  UB	  Filterwheel 1 encoder channel A.	  
; 128	   SUV_FW1_ENC_CHAN_B	  267	  6	  1	  UB	  Filterwheel 1 encoder channel B.	  
; 129	   SUV_FW1_ENC_CHAN_C	  267	  5	  1	  UB	  Filterwheel encoder channel C.  
; 130	   SUV_FW1_MOVE_DONE	  267	  1	  1	  UB	  Filterwheel 1 DONE bit. Move completed. Ready for new command.  
; 131	   SUV_FW1_SPIN_TIMER_DONE	  267	  2	  1	  UB	  Filterwheel 1 spin timer done bit.	  
; 132	   SUV_FW1_STEP_MODE	  267	  4	  1	  UB	  Filterwheel 1 step mode bit.    
; 133	   SUV_FW2_CMDED_DELAY    268	  0	  16	  IU1	  Filterwheel 2 commanded delay.  
; 134	   SUV_FW2_CMDED_ENC_CFG  270	  0	  16	  IU1	  Filterwheel 2 commanded encoder setting.	  
; 135	   SUV_FW2_CMDED_TGT	  272	  0	  16	  IU1	  Filterwheel 2 commanded target position.	  
; 136	   SUV_FW2_POS    274	  0	  16	  IU1	  Filterwheel 2 current software position.	  
; 137	   SUV_FW2_SM_STAT	  276	  0	  16	  IU1	  Filterwheel 2 state machine state.	  
; 138	   SUV_FW2_SPIN_TIME	  278	  0	  16	  IU1	  Filterwheel 2 spin time.	  
; 139	   SUV_FW2_MACHINE_STATE  280	  0	  8	  UB	  "Filterwheel 2 machine state. 0-normal, 1-move in progress, 2-step in progress, 2-step to target in progress, 4-spin in progress."	  
; 140	   SUV_FW2_STEP_WAIT_TIME 282	  0	  16	  IU1	  Filterwheel 2 wait time to wait after each step mSec
; 141	   SUV_FW2_SPIN_ODOMETER  284	  0	  32	  UL1	  Filterwheel 2 spin odometer.    
; 142	   SUV_FW2_STS    288	  0	  32	  UL1	  Status of filterwheel 1 
; 143	   SUV_FW2_ENC_BITS	  290	  0	  9	  IU1	  Filterwheel 2 encoder bits.	  
; 144	   SUV_FW2_ENC_ALWYS_ON   291	  3	  1	  UB	  Filterwheel 2 encoder is always on.	  
; 145	   SUV_FW2_ENC_CHAN_A	  291	  7	  1	  UB	  Filterwheel channel encoder A   
; 146	   SUV_FW2_ENC_CHAN_B	  291	  6	  1	  UB	  Filterwheel 2 channel encoder B.	  
; 147	   SUV_FW2_ENC_CHAN_C	  291	  5	  1	  UB	  Filterwheel 2 channel encoder C.	  
; 148	   SUV_FW2_MOVE_DONE	  291	  1	  1	  UB	  Filterwheel 2 DONE bit. Move is completed.	  
; 149	   SUV_FW2_SPIN_TIMER_DONE	  291	  2	  1	  UB	  Filterwheel 2 spin timer DONE bit. Value is valid.	  
; 150	   SUV_FW2_STEP_MODE	  291	  4	  1	  UB	  Filterwheel 2 step mode enabled.	  
; 151	   SUV_GT_AVG_SAMPLES	  292	  0	  32	  UL1	  Number of GT samples averaged for the Solar Reference Frame off 
; 152	   SUV_GT_CHN_CNT 296	  0	  32	  UL1	  Counter for the number of packets sent on the Guide Telescope D 
; 153	   SUV_GT_CHN_REJ_CNT	  300	  0	  32	  UL1	  Guide Telescope channel reject count.   
; 154	   SUV_GT_DATA_STS	  304	  0	  32	  UL1	  Parent telemetry monitor for Guide Telescope Data status.	  
; 155	   SUV_GT_INTERRUPT_CNT   304	  0	  8	  UB	  Number of interrupts that occurred during the Guide Telescope Data acquisition  
; 156	   SUV_GT_BAD_SAMPLES	  305	  0	  8	  UB	  Number of bad Guide Telescope samples found when calculating the error  
; 157	   SUV_GT_PPS_OCCURRED    306	  0	  1	  UB	  Pulse Per Second PID occurred during the Guide Telescope Data processing	  
; 158	   SUV_GT_INTERVAL	  308	  0	  32	  UL1	  Sampling interval	  mSec
; 159	   SUV_GT_OS_ERR_Y_NR	  312	  0	  32	  IL1	  Calculated Solar Reference Frame Y Offset Error	  nanoR
; 160	   SUV_GT_OS_ERR_Z_NR	  316	  0	  32	  IL1	  Calculated Solar Reference Frame Z Offset Error	  nanoR
; 161	   SUV_GT_PD1_P   320	  0	  16	  IU1	  Primary Y+ photodiode 1 voltage_ HK/DA channel 17. Includes overflow flag	  Volts
; 162	   SUV_GT_PD2_P   322	  0	  16	  IU1	  Primary Y- photodiode 2 voltage_ HK/DA channel 18. Includes overflow flag	  Volts
; 163	   SUV_GT_PD3_P   324	  0	  16	  IU1	  Primary Z+ photodiode 3 voltage_ HK/DA channel 19. Includes overflow flag	  Volts
; 164	   SUV_GT_PD4_P   326	  0	  16	  IU1	  Primary Z- photodiode 4 voltage_ HK/DA channel 20. Includes overflow flag	  Volts
; 165	   SUV_GT_YERR_P  328	  0	  16	  IU1	  Primary Y error_ no scale or offset_ HK/DA channel 21. Includes overflow flag   Volts
; 166	   SUV_GT_ZERR_P  330	  0	  16	  IU1	  Primary Z error_ no scale or offset_ HK/DA channel 22. Includes overflow flag   Volts
; 167	   SUV_GT_YOFF_P  332	  0	  16	  IU1	  Primary Y offset DAC voltage_ HK/DA channel 31. Includes overflow flag  Volts
; 168	   SUV_GT_ZOFF_P  334	  0	  16	  IU1	  Primary Z offset DAC voltage_ HK/DA channel 29. Includes overflow flag  Volts
; 169	   SUV_GT_YTOAD_P 336	  0	  16	  IU1	  Primary Y error_ scaled and offset_ primary source of pointing  Volts
; 170	   SUV_GT_ZTOAD_P 338	  0	  16	  IU1	  Primary Z error_ scaled and offset_ primary source of pointing  Volts
; 171	   SUV_GT_PD1_R   340	  0	  16	  IU1	  Redundant Y+ photodiode 1 voltage_ HK/DA channel 17. Includes overflow flag	  Volts
; 172	   SUV_GT_PD2_R   342	  0	  16	  IU1	  Redundant Y- photodiode 2 voltage_ HK/DA channel 18. Includes overflow flag	  Volts
; 173	   SUV_GT_PD3_R   344	  0	  16	  IU1	  Redundant Z+ photodiode 3 voltage_ HK/DA channel 19. Includes overflow flag	  Volts
; 174	   SUV_GT_PD4_R   346	  0	  16	  IU1	  Redundant Z- photodiode 4 voltage_ HK/DA channel 20. Includes overflow flag	  Volts
; 175	   SUV_GT_YERR_R  348	  0	  16	  IU1	  Redundant Y error_ no scale or offset_ HK/DA channel 21. Includes overflow flag Volts
; 176	   SUV_GT_ZERR_R  350	  0	  16	  IU1	  Redundant Z error_ no scale or offset_ HK/DA channel 22. Includes overflow flag Volts
; 177	   SUV_GT_YOFF_R  352	  0	  16	  IU1	  Redundant Y offset DAC voltage_ HK/DA channel 31. Includes overflow flag	  Volts
; 178	   SUV_GT_ZOFF_R  354	  0	  16	  IU1	  Redundant Z offset DAC voltage_ HK/DA channel 29. Includes overflow flag	  Volts
; 179	   SUV_GT_YTOAD_R 356	  0	  16	  IU1	  Redundant Y error_ scaled and offset_ primary source of pointing	  Volts
; 180	   SUV_GT_ZTOAD_R 358	  0	  16	  IU1	  Redundant Z error_ scaled and offset_ primary source of pointing	  Volts
; 181	   SUV_GT_STS	  360	  0	  32	  UL1	  Parent monitor for GT linear range_ acquisition range_ and photo-diode status   
; 182	   SUV_GT_LIN_RANGE	  361	  7	  1	  UB	  0 = Out of Linear Range_ 1 = In Linear Range.   
; 183	   SUV_GT_ACQ_RANGE	  362	  7	  1	  UB	  0 = Out of Acquisition Range_ 1 = In Acquisition Range. 
; 184	   SUV_GT_PD1_ON  363	  2	  1	  UB	  0/1 = Diode 1 on No/Yes.	  
; 185	   SUV_GT_PD2_ON  363	  3	  1	  UB	  0/1 = Diode 2 on No/Yes.	  
; 186	   SUV_GT_PD3_ON  363	  4	  1	  UB	  0/1 = Diode 3 on No/Yes.	  
; 187	   SUV_GT_PD4_ON  363	  5	  1	  UB	  0/1 = Diode 4 on No/Yes.	  
; 188	   SUV_HTR1_PARENT_STATES 364	  0	  32	  UL1	  Heater 1 States - DC | LOW_DC | HIGH_DC | ON | OFF	  
; 189	   SUV_HTR1_PWR_STATE	  364	  0	  16	  IU1	  Heater 1 Power State    
; 190	   SUV_HTR1_DC_STATE	  366	  0	  16	  IU1	  Heater 1 Current Duty Cycle State	  
; 191	   SUV_HTR1_HWD_ENA_ST    368	  0	  16	  IU1	  Heater 1 Relay Enable Status    
; 192	   SUV_HTR1_PARENT_STATUS 368	  0	  32	  UL1	  Heater 1 Enable Status  
; 193	   SUV_HTR1_INST_ENA_ST   370	  0	  16	  IU1	  Heater 1 Software Instance Enable State 
; 194	   SUV_HTR2_PARENT_STATES 372	  0	  32	  UL1	  Heater 2 States - DC | LOW_DC | HIGH_DC | ON | OFF	  
; 195	   SUV_HTR2_PWR_STATE	  372	  0	  16	  IU1	  Heater 2 Power State    
; 196	   SUV_HTR2_DC_STATE	  374	  0	  16	  IU1	  Heater 2 Current Duty Cycle State	  
; 197	   SUV_HTR2_HWD_ENA_ST    376	  0	  16	  IU1	  Heater 2 Relay Enable Status    
; 198	   SUV_HTR2_PARENT_STATUS 376	  0	  32	  UL1	  Heater 2 Enable Status  
; 199	   SUV_HTR2_INST_ENA_ST   378	  0	  16	  IU1	  Heater 2 Software Instance Enable State 
; 200	   SUV_HTR3_PARENT_STATES 380	  0	  32	  UL1	  Heater 3 States - DC | LOW_DC | HIGH_DC | ON | OFF	  
; 201	   SUV_HTR3_PWR_STATE	  380	  0	  16	  IU1	  Heater 3 Power State    
; 202	   SUV_HTR3_DC_STATE	  382	  0	  16	  IU1	  Heater 3 Current Duty Cycle State	  
; 203	   SUV_HTR3_HWD_ENA_ST    384	  0	  16	  IU1	  Heater 3 Relay Enable Status    
; 204	   SUV_HTR3_PARENT_STATUS 384	  0	  32	  UL1	  Heater 3 Enable Status  
; 205	   SUV_HTR3_INST_ENA_ST   386	  0	  16	  IU1	  Heater 3 Software Instance Enable State 
; 206	   SUV_HTR4_PARENT_STATES 388	  0	  32	  UL1	  Heater 4 States - DC | LOW_DC | HIGH_DC | ON | OFF	  
; 207	   SUV_HTR4_PWR_STATE	  388	  0	  16	  IU1	  Heater 4 Power State    
; 208	   SUV_HTR4_DC_STATE	  390	  0	  16	  IU1	  Heater 4 Current Duty Cycle State	  
; 209	   SUV_HTR4_HWD_ENA_ST    392	  0	  16	  IU1	  Heater 4 Relay Enable Status    
; 210	   SUV_HTR4_PARENT_STATUS 392	  0	  32	  UL1	  Heater 4 Enable Status  
; 211	   SUV_HTR4_INST_ENA_ST   394	  0	  16	  IU1	  Heater 4 Software Instance Enable State 
; 212	   SUV_HTR5_PARENT_STATES 396	  0	  32	  UL1	  Heater 5 States - DC | LOW_DC | HIGH_DC | ON | OFF	  
; 213	   SUV_HTR5_PWR_STATE	  396	  0	  16	  IU1	  Heater 5 Power State    
; 214	   SUV_HTR5_DC_STATE	  398	  0	  16	  IU1	  Heater 5 Current Duty Cycle State	  
; 215	   SUV_HTR5_HWD_ENA_ST    400	  0	  16	  IU1	  Heater 5 Relay Enable Status    
; 216	   SUV_HTR5_PARENT_STATUS 400	  0	  32	  UL1	  Heater 5 Enable Status  
; 217	   SUV_HTR5_INST_ENA_ST   402	  0	  16	  IU1	  Heater 5 Software Instance Enable State 
; 218	   SUV_HTR6_PARENT_STATES 404	  0	  32	  UL1	  Heater 6 States - DC | LOW_DC | HIGH_DC | ON | OFF	  
; 219	   SUV_HTR6_PWR_STATE	  404	  0	  16	  IU1	  Heater 6 Power State    
; 220	   SUV_HTR6_DC_STATE	  406	  0	  16	  IU1	  Heater 6 Current Duty Cycle State	  
; 221	   SUV_HTR6_HWD_ENA_ST    408	  0	  16	  IU1	  Heater 6 Relay Enable Status    
; 222	   SUV_HTR6_PARENT_STATUS 408	  0	  32	  UL1	  Heater 6 Enable Status  
; 223	   SUV_HTR6_INST_ENA_ST   410	  0	  16	  IU1	  Heater 6 Software Instance Enable State 
; 224	   SUV_IP_IMAGE_FLAGS	  412	  0	  32	  UL1	  Image flags for bakeout_ eclipse_ etc.  
; 225	   SUV_LOCKOUT_OVERR_REG  416	  0	  32	  UL1	  Lockout Override register on the Bridge/SpaceWire board (register)	  
; 226	   SUV_LOCKOUT_REG	  420	  0	  32	  UL1	  Lockout Register Bit Assignments on the Bridge/SpaceWire board (register offset 0x5c).  
; 227	   SUV_MEC_BD1_VER	  424	  0	  16	  IU1	  Mechanism controller board 1 version.   
; 228	   SUV_MEC_BD1_ERR_CNT    426	  0	  16	  IU1	  Mechanism board 1 error count.  
; 229	   SUV_MEC_BD1_STATUS	  428	  0	  32	  UL1	  Mechanism board 1 status	  
; 230	   SUV_BD1_SHT_ENC_ERROR  430	  5	  1	  UB	  Mechanism board 1 status - Shutter encoder error.	  
; 231	   SUV_BD1_SHT_RDY_NARROW 430	  7	  1	  UB	  Mechanism board 1 status - Shutter ready for narrow (10 ms after the final brake command).	  
; 232	   SUV_BD1_SHT_SPIN_TMR_DN	  430	  6	  1	  UB	  Mechanism board 1 status - Shutter spin timer done.	  
; 233	   SUV_BD1_DOOR1_LATCH_DN 431	  5	  1	  UB	  Mechanism board 1 status - Door 1 latch done.   
; 234	   SUV_BD1_DOOR1_MTR_DONE 431	  6	  1	  UB	  Mechanism board 1 status - Door 1motor done.    
; 235	   SUV_BD1_DOOR1_SW_CHG   431	  4	  1	  UB	  Mechanism board 1 status - Door 1 microswitch change.   
; 236	   SUV_BD1_FW1_ENC_ERROR  431	  1	  1	  UB	  Mechanism board 1 status - Filterwheel1 encoder error.  
; 237	   SUV_BD1_FW1_MOVE_COMPL 431	  3	  1	  UB	  Mechanism board 1 status - Filterwheel 1 move complete. 
; 238	   SUV_BD1_FW1_SPIN_TMR_DN	  431	  2	  1	  UB	  Mechanism board 1 status - Filterwheel 1 spin timer done.	  
; 239	   SUV_BD1_SERIAL_IF_ERROR	  431	  7	  1	  UB	  Mechanism board 1 status -Serial interface error.	  
; 240	   SUV_BD1_SHT_DONE	  431	  0	  1	  UB	  Mechanism board 1 status - Shutter done.	  
; 241	   SUV_MEC_BD2_VER	  432	  0	  16	  IU1	  Mechanism controller board 2 version.   
; 242	   SUV_MEC_BD2_ERR_CNT    434	  0	  16	  IU1	  Mechanism board 2 error count.  
; 243	   SUV_MEC_BD2_STATUS	  436	  0	  32	  UL1	  Mechanism board 2 status	  
; 244	   SUV_BD2_AS_ENC_ERROR   438	  6	  1	  UB	  Mechanism board 2 status - Aperture selector encoder error.	  
; 245	   SUV_BD2_AS_SPIN_TMR_DN 438	  7	  1	  UB	  Mechanism board 2 status - Aperture selector spin timer done.   
; 246	   SUV_BD2_FOCUS_DONE	  438	  5	  1	  UB	  Mechanism board 2 status - Focus mechanism done.	  
; 247	   SUV_BD2_AS_MOVE_COMPL  439	  0	  1	  UB	  Mechanism board 2 status - Aperture selector move complete.	  
; 248	   SUV_BD2_DOOR2_LATCH_DN 439	  5	  1	  UB	  Mechanism board 2 status - Door 2 latch done.   
; 249	   SUV_BD2_DOOR2_MTR_DONE 439	  6	  1	  UB	  Mechanism board 2 status - Door 2 motor done.   
; 250	   SUV_BD2_DOOR2_SW_CHG   439	  4	  1	  UB	  Mechanism board 2 status - Door 2 microswitch change.   
; 251	   SUV_BD2_FW2_ENC_ERROR  439	  1	  1	  UB	  Mechanism board 2 status - Filterwheel 2 encoder error. 
; 252	   SUV_BD2_FW2_MOVE_COMPL 439	  3	  1	  UB	  Mechanism board 2 status - Filterwheel 2 move complete. 
; 253	   SUV_BD2_FW2_SPIN_TMR_DN	  439	  2	  1	  UB	  Mechanism board 2 status - Filterwheel 2 spin timer done.	  
; 254	   SUV_BD2_SERIAL_IF_ERROR	  439	  7	  1	  UB	  Mechanism board 2 serial interface error.	  
; 255	   SUV_OTS10P_FAA_HSG	  440	  0	  16	  IU1	  Front Aperture housing temperature (measured by primary thermister)	  Deg C
; 256	   SUV_OTS11P_SPI_HSG	  442	  0	  16	  IU1	  Spider Assembly temperature (measured by primary thermister)    Deg C
; 257	   SUV_OTS12P_FWS_SH_HSG  444	  0	  16	  IU1	  Shutter housing temperature (measured by primary thermister).   Deg C
; 258	   SUV_OTS13P_FWS_FW_HSG  446	  0	  16	  IU1	  Filter Wheel housing temperature (measured by primary thermister)	  Deg C
; 259	   SUV_OTS14P_SEB_TOP	  448	  0	  16	  IU1	  SUVI Electronics Box top temperature (measured by primary thermister)   Deg C
; 260	   SUV_OTS15P_SEB_PWR_CNV 450	  0	  16	  IU1	  SUVI Electronics Box power converter temperature (measured by primary thermister)	  Deg C
; 261	   SUV_OTS16P_SEB_CPU	  452	  0	  16	  IU1	  SUVI Electronics Box CPU temperature (measured by primary thermister)   Deg C
; 262	   SUV_OTS03P_FDA_GBOX    454	  0	  16	  IU1	  Door mechanism temperature (measured by primary thermister).    Deg C
; 263	   SUV_OTS01P_TEL_FWD	  456	  0	  16	  IU1	  Primary mirror temperature (measured by primary thermister).    Deg C
; 264	   SUV_OTS02P_TEL_AFT	  458	  0	  16	  IU1	  Secondary mirror temperature (measured by primary thermister).  Deg C
; 265	   SUV_OTS04P_GTA_FWD	  460	  0	  16	  IU1	  Guide Telescope forward temperature (measured by primary thermister)    Deg C
; 266	   SUV_OTS05P_GTA_AFT	  462	  0	  16	  IU1	  Guide Telescope aft temperature (measured by primary thermister Deg C
; 267	   SUV_OTS06P_FPA_CCD	  464	  0	  16	  IU1	  CCD heater block temperature (measured by primary thermister).  Deg C
; 268	   SUV_OTS07P_CEB_EXT	  466	  0	  16	  IU1	  Camera Electronics Box external temperature (measured by primary thermister)    Deg C
; 269	   SUV_OTS08P_SEB_BASEPLT 468	  0	  16	  IU1	  SUVI Electronics Box baseplate temperature (measured by primary thermister)	  Deg C
; 270	   SUV_OTS09P_FAA_HOPA    470	  0	  16	  IU1	  Parafin actuator temperature (measured by primary thermister).  Deg C
; 271	   SUV_OTS10R_FAA_HSG	  472	  0	  16	  IU1	  Front Aperture housing temperature (measured by redundant thermister)   Deg C
; 272	   SUV_OTS11R_SPI_HSG	  474	  0	  16	  IU1	  /Spider Assembly temperature (measured by redundant thermister) Deg C
; 273	   SUV_OTS12R_FWS_SH_HSG  476	  0	  16	  IU1	  Shutter housing temperature (measured by redundant thermister). Deg C
; 274	   SUV_OTS13R_FWS_FW_HSG  478	  0	  16	  IU1	  Filter Wheel housing temperature (measured by redundant thermis Deg C
; 275	   SUV_OTS14R_SEB_TOP	  480	  0	  16	  IU1	  SUVI Electronics Box top temperature (measured by redundant thermister) Deg C
; 276	   SUV_OTS15R_SEB_PWR_CNV 482	  0	  16	  IU1	  SUVI Electronics Box power converter temperature (measured by redundant thermister)	  Deg C
; 277	   SUV_OTS16R_SEB_CPU	  484	  0	  16	  IU1	  SUVI Electronics Box CPU temperature (measured by redundant thermister) Deg C
; 278	   SUV_OTS03R_FDA_GBOX    486	  0	  16	  IU1	  Door mechanism temperature (measured by redundant thermister).  Deg C
; 279	   SUV_OTS01R_TEL_FWD	  488	  0	  16	  IU1	  Primary mirror temperature (measured by redundant thermister).  Deg C
; 280	   SUV_OTS02R_TEL_AFT	  490	  0	  16	  IU1	  Secondary mirror temperature (measured by redundant thermister) Deg C
; 281	   SUV_OTS04R_GTA_FWD	  492	  0	  16	  IU1	  Guide Telescope forward temperature (measured by redundant thermister)  Deg C
; 282	   SUV_OTS05R_GTA_AFT	  494	  0	  16	  IU1	  Guide Telescope aft temperature (measured by redundant thermister)	  Deg C
; 283	   SUV_OTS06R_FPA_CCD	  496	  0	  16	  IU1	  CCD heater block temperature (measured by redundant thermister) Deg C
; 284	   SUV_OTS07R_CEB_EXT	  498	  0	  16	  IU1	  Camera Electronics Box external temperature (measured by redundant thermister)  Deg C
; 285	   SUV_OTS08R_SEB_BASEPLT 500	  0	  16	  IU1	  SUVI Electronics Box baseplate temperature (measured by redundant thermister)   Deg C
; 286	   SUV_OTS09R_FAA_HOPA    502	  0	  16	  IU1	  Parafin actuator temperature (measured by redundant thermister) Deg C
; 287	   SUV_PW_REG_1   504	  0	  32	  UL1	  Power Register 1 Value updated every second or when Power CSC command is executed.	  
; 288	   SUV_HOPA_ENA   505	  7	  1	  UB	  Power Register 1 HOPA Enable    
; 289	   SUV_BLUE_LED   506	  7	  1	  UB	  Power Register 1 Blue LED	  
; 290	   SUV_HTR_CTL_1  506	  6	  1	  UB	  Power Register 1 Heater Control 1	  
; 291	   SUV_HTR_CTL_2  506	  5	  1	  UB	  Power Register 1 Heater Control 2	  
; 292	   SUV_HTR_CTL_3  506	  1	  1	  UB	  Power Register 1 Heater Control 3	  
; 293	   SUV_HTR_CTL_4  506	  2	  1	  UB	  Power Register 1 Heater Control 4	  
; 294	   SUV_HTR_CTL_5  506	  4	  1	  UB	  Power Register 1 Heater Control 5	  
; 295	   SUV_HTR_CTL_6  506	  3	  1	  UB	  Power Register 1 Heater Control 6	  
; 296	   SUV_CEB_PWR_ENA	  507	  7	  1	  UB	  Power Register 1 CEB Power Enable	  
; 297	   SUV_HTR_ENA_1  507	  6	  1	  UB	  Power Register 1 Heater Enable 1	  
; 298	   SUV_HTR_ENA_2  507	  5	  1	  UB	  Power Register 1 Heater Enable 2	  
; 299	   SUV_HTR_ENA_3  507	  1	  1	  UB	  Power Register 1 Heater Enable 3	  
; 300	   SUV_HTR_ENA_4  507	  2	  1	  UB	  Power Register 1 Heater Enable 4	  
; 301	   SUV_HTR_ENA_5  507	  4	  1	  UB	  Power Register 1 Heater Enable 5	  
; 302	   SUV_HTR_ENA_6  507	  3	  1	  UB	  Power Register 1 Heater Enable 6	  
; 303	   SUV_PWR_P5V_P  508	  0	  16	  IU1	  "Primary Power Converter +5V supply to Housekeeping ADC Board. MUX address 0x10, channel 1."    Volts
; 304	   SUV_PWR_P15V_P 510	  0	  16	  IU1	  Primary Power Converter +15V supply to Housekeeping ADC Board.  Volts
; 305	   SUV_PWR_N15V_P 512	  0	  16	  IU1	  "Primary Power Convert -15V supply to Housekeeping ADC Board. MUX address 0x12, channel 3."	  Volts
; 306	   SUV_PWR_P3_3V_P	  514	  0	  16	  IU1	  Primary Power Converter +3.3V supply to Housekeeping ADC Board. Volts
; 307	   SUV_CNV_P15V_MTR_I_P   516	  0	  16	  IU1	  Primary Power Converter measurement of +15V Mechanism Motor current	  Amps
; 308	   SUV_CNV_P15V_MECH_V_P  518	  0	  16	  IU1	  Primary Power Converter measurement of +15V Mechanism Board voltage	  Volts
; 309	   SUV_OP_HTR_I_P 520	  0	  16	  IU1	  Primary Mechanism Board measurement of Operational Heater current	  Amps
; 310	   SUV_FOCUS_CNTR_V_P	  522	  0	  16	  IU1	  Primary Mechanism Board measurement of 0-to-5 volts focus center voltage	  Volts
; 311	   SUV_CCD_HTR_V_P	  524	  0	  16	  IU1	  Primary Power Converter measurement of +15V CCD heater voltage. Volts
; 312	   SUV_CCD_HTR_I_P	  526	  0	  16	  IU1	  Primary Power Converter measurement of +15V CCD heater current. Amps
; 313	   SUV_HK_10VREF1_P	  528	  0	  16	  IU1	  Primary 10-Volt reference voltage for MUX addresses 0x30 - 0x3F Volts
; 314	   SUV_HK_10VREF2_P	  530	  0	  16	  IU1	  Primary 10-Volt reference voltage for MUX addresses 0x40 - 0x4F Volts
; 315	   SUV_HK_ANLG_GND_V_P    532	  0	  16	  IU1	  Primary Analog ground reference voltage (0 Volts). MUX address  Volts
; 316	   SUV_PWR_P5V_R  536	  0	  16	  IU1	  Redundant Power Converter +5V supply to Housekeeping ADC Board. Volts
; 317	   SUV_PWR_P15V_R 538	  0	  16	  IU1	  Redundant Power Converter +15V supply to Housekeeping ADC Board Volts
; 318	   SUV_PWR_N15V_R 540	  0	  16	  IU1	  Redundant Power Convert -15V supply to Housekeeping ADC Board.  Volts
; 319	   SUV_PWR_P3_3V_R	  542	  0	  16	  IU1	  Redundant Power Converter +3.3V supply to Housekeeping ADC Board	  Volts
; 320	   SUV_CNV_P15V_MTR_I_R   544	  0	  16	  IU1	  Redundant Power Converter measurement of +15V Mechanism Motor current   Amps
; 321	   SUV_CNV_P15V_MECH_V_R  546	  0	  16	  IU1	  Redundant Power Converter measurement of +15V Mechanism Board voltage   Volts
; 322	   SUV_OP_HTR_I_R 548	  0	  16	  IU1	  Redundant Mechanism Board measurement of Operational Heater current	  Amps
; 323	   SUV_FOCUS_CNTR_V_R	  550	  0	  16	  IU1	  Redundant Mechanism Board measurement of 0-to-5 volts focus center voltage	  Volts
; 324	   SUV_CCD_HTR_V_R	  552	  0	  16	  IU1	  Redundant Power Converter measurement of +15V CCD heater voltage	  Volts
; 325	   SUV_CCD_HTR_I_R	  554	  0	  16	  IU1	  Redundant Power Converter measurement of +15V CCD heater current	  Amps
; 326	   SUV_HK_10VREF1_R	  556	  0	  16	  IU1	  Redundant 10-Volt reference voltage for MUX addresses 0x30 - 0x Volts
; 327	   SUV_HK_10VREF2_R	  558	  0	  16	  IU1	  Redundant 10-Volt reference voltage for MUX addresses 0x40 - 0x Volts
; 328	   SUV_HK_ANLG_GND_V_R    560	  0	  16	  IU1	  "Redundant Analog ground reference voltage (0 Volts). MUX address 0x0F, channel 0."	  Volts
; 329	   SUV_SHT_BC_CLOSE_TIME  564	  0	  32	  UL1	  Shutter bottom center close time W1 & W0	  mSec
; 330	   SUV_SHT_BC_OPEN_TIME   568	  0	  32	  UL1	  Shutter bottom center open time W1 & W0 mSec
; 331	   SUV_SHT_BOT_CLOSE_TIM  572	  0	  32	  UL1	  Shutter bottom close time W1 & W0	  mSec
; 332	   SUV_SHT_BOT_OPEN_TIME  576	  0	  32	  UL1	  Shutter bottom open time W1 & W0	  mSec
; 333	   SUV_SHT_OC_EXP_NTICKS  580	  0	  32	  UL1	  Shutter open/close exposure duration in ticks.  
; 334	   SUV_SHT_OC_EXP_STICKS  584	  0	  32	  UL1	  Shutter open/close exposure start tick number.  
; 335	   SUV_SHT_CMDED_DELAY    588	  0	  16	  IU1	  Shutter commanded delay.	  mSec
; 336	   SUV_SHT_CMDED_ENC	  590	  0	  16	  IU1	  Shutter commanded encoder settings.	  
; 337	   SUV_SHT_CMDED_EXP	  592	  0	  16	  IU1	  Shutter commanded exposure time	  4 mSec
; 338	   SUV_SHT_SM_STATE	  594	  0	  16	  IU1	  Shutter state machine state.    
; 339	   SUV_SHT_SPIN_TIME	  598	  0	  16	  IU1	  Shutter spin time.	  16mSec
; 340	   SUV_SHT_STATE  600	  0	  8	  UB	  "Shutter state. 0-normal, 1-open/close in progress, 2-narrow sweep loop in progress."   
; 341	   SUV_SHT_OC_EXP_ENDED   601	  0	  8	  UB	  "Shutter open/close exposure end flag. 0-in progress when SUV_SHT_STATE is 1, 1-ended." 
; 342	   SUV_SHT_N_NR_EXP	  604	  0	  16	  IU1	  Number of shutter narrow sweeps.	  
; 343	   SUV_SHT_N_NR_EXP_CMPL  606	  0	  16	  IU1	  Number of shutter narrow sweeps completed.	  
; 344	   SUV_SHT_SPIN_ODOMETER  608	  0	  32	  UL1	  Shutter spin odometer.  
; 345	   SUV_SHT_STATUS 612	  0	  32	  UL1	  Shutter Status  
; 346	   SUV_SHT_ENCODER_BITS   614	  0	  5	  UB	  Shutter status word - Ready for narrow. 
; 347	   SUV_SHT_READY_4_NARROW 614	  6	  1	  UB	  Shutter status word - Ready for narrow. 
; 348	   SUV_SHT_SPIN_TIMER_DONE	  614	  7	  1	  UB	  Shutter status word - Spin timer done.  
; 349	   SUV_SHT_DONE   615	  0	  1	  UB	  Shutter status word - DONE bit. 
; 350	   SUV_SHT_ENC_ALWAYS_ON  615	  3	  1	  UB	  Shutter status word - Encoder always on.	  
; 351	   SUV_SHT_ENC_CHAN_A	  615	  7	  1	  UB	  Shutter status word - Encoder channel A.	  
; 352	   SUV_SHT_ENC_CHAN_B	  615	  6	  1	  UB	  Shutter status word - Encoder channel b.	  
; 353	   SUV_SHT_ENC_CHAN_C	  615	  5	  1	  UB	  Shutter status word - Encoder channel C.	  
; 354	   SUV_SHT_STEP_MODE	  615	  4	  1	  UB	  Shutter status word - Step mode.	  
; 355	   SUV_SHT_WAIT_STATE_A   615	  2	  1	  UB	  Shutter status word - Waiting state A   
; 356	   SUV_SHT_WAIT_STATE_B   615	  1	  1	  UB	  Shutter status word - Waiting state B   
; 357	   SUV_SHT_TC_CLOSE_TIME  616	  0	  32	  UL1	  Shutter top center close time W1 & W0   mSec
; 358	   SUV_SHT_TC_OPEN_TIME   620	  0	  32	  UL1	  Shutter top center open time W1 & W0    mSec
; 359	   SUV_SHT_TOP_CLOSE_TIM  624	  0	  32	  UL1	  Shutter top close time W1 & W0  mSec
; 360	   SUV_SHT_TOP_OPEN_TIME  628	  0	  32	  UL1	  Shutter top open time W1 & W0   mSec
; 361	   SUV_SQ_CFG_STATE	  632	  0	  32	  UL1	  Sequencer Configuration state for Event and Mech Config.	  
; 362	   SUV_SQ_ST_EVT_0	  632	  0	  1	  UB	  Sequencer Event 0 State 
; 363	   SUV_SQ_ST_EVT_1	  632	  1	  1	  UB	  Sequencer Event 1 State 
; 364	   SUV_SQ_ST_EVT_2	  632	  2	  1	  UB	  Sequencer Event 2 State 
; 365	   SUV_SQ_ST_EVT_3	  632	  3	  1	  UB	  Sequencer Event 3 State 
; 366	   SUV_SQ_ST_EVT_4	  632	  4	  1	  UB	  Sequencer Event 4 State 
; 367	   SUV_SQ_ST_EVT_5	  632	  5	  1	  UB	  Sequencer Event 5 State 
; 368	   SUV_SQ_ST_EVT_6	  632	  6	  1	  UB	  Sequencer Event 6 State 
; 369	   SUV_SQ_ST_EVT_7	  632	  7	  1	  UB	  Sequencer Event 7 State 
; 370	   SUV_SQ_ST_EVT_10	  633	  2	  1	  UB	  Sequencer Event 10 State	  
; 371	   SUV_SQ_ST_EVT_11	  633	  3	  1	  UB	  Sequencer Event 11 State	  
; 372	   SUV_SQ_ST_EVT_12	  633	  4	  1	  UB	  Sequencer Event 12 State	  
; 373	   SUV_SQ_ST_EVT_13	  633	  5	  1	  UB	  Sequencer Event 13 State	  
; 374	   SUV_SQ_ST_EVT_14	  633	  6	  1	  UB	  Sequencer Event 14 State	  
; 375	   SUV_SQ_ST_EVT_15	  633	  7	  1	  UB	  Sequencer Event 15 State	  
; 376	   SUV_SQ_ST_EVT_8	  633	  0	  1	  UB	  Sequencer Event 8 State 
; 377	   SUV_SQ_ST_EVT_9	  633	  1	  1	  UB	  Sequencer Event 9 State 
; 378	   SUV_SQ_MECH_CFG_ST	  634	  0	  8	  UB	  "Sequencer Mechanism Config State. Default 0=Simultaneous move, 1=serial move, 2=staggered move. "	  
; 379	   SUV_SQ_FLUSH_MODE	  635	  4	  4	  UB	  Tells the Sequencer to flush the CCD before taking a picture_ 0 
; 380	   SUV_SQ_FRM_MODE	  635	  0	  4	  UB	  "Sequencer Frame Preprocessing Mode. Default 0=mechanisms move in line with start of frame, 1=mechanisms of next frame can move before start of frame." 
; 381	   SUV_SQ_FDB_DEF_ID	  636	  0	  32	  UL1	  Identifier of the default Frame Definition Block Table file.    
; 382	   SUV_SQ_FDB_DEF_VER	  640	  0	  32	  UL1	  Version of the default Frame Definition Block Table file.	  
; 383	   SUV_SQ_FDB_FILE_ID	  644	  0	  32	  UL1	  Identifier of the currently loaded Frame Definition Block Table 
; 384	   SUV_SQ_FDB_FILE_VER    648	  0	  32	  UL1	  Version of the currently loaded Frame Definition Block Table file.	  
; 385	   SUV_SQ_FDB_RC  652	  0	  16	  IU1	  Sequencer Frame Definition Block (FDB) Return Code.	  
; 386	   SUV_SQ_FRAME_RC	  652	  0	  32	  UL1	  Sequencer Frame Return Code (either for FDB or SDB)	  
; 387	   SUV_SQ_SDB_RC  654	  0	  16	  IU1	  Sequencer Script Definition Block Return Code   
; 388	   SUV_SQ_OVR_CNT 656	  0	  16	  IU1	  Number of times a frame overrun its allocated time slot.	  
; 389	   SUV_SQ_OVR_FLD 656	  0	  32	  UL1	  Sequencer Overrun time fields   
; 390	   SUV_SQ_INSUF_TIME_CNT  658	  0	  16	  IU1	  Number of times a frame could not execute because of insufficient time slice.   
; 391	   SUV_SQ_SCT_CAD 660	  0	  32	  UL1	  Sequence Control Table frame cadence in deciseconds.    
; 392	   SUV_SQ_SCT_FILE_ID	  664	  0	  32	  UL1	  File identifier of the Sequence Control Table being used by the sequencer.	  
; 393	   SUV_SQ_SCT_FILE_VER    668	  0	  32	  UL1	  File version number of the Sequence Control Table being used by sequencer.	  
; 394	   SUV_SQ_SCT_FRM_NUM	  672	  0	  32	  UL1	  Current frame number for the Sequence Control Table. This is a one-up number since the sequencer was turned on. 
; 395	   SUV_SQ_SCT_NUM_SEQ	  676	  0	  32	  UL1	  Number of sequences in the current Sequence Control Table.	  
; 396	   SUV_SQ_SCT_RUN_CNT	  680	  0	  32	  UL1	  Number of times the current sequence in the Sequence Control Table has been run.	  
; 397	   SUV_SQ_SCT_TIME	  684	  0	  32	  UL1	  Time currently running iteration of the Sequence Control Table. 
; 398	   SUV_SQ_SEQ_FDB_ID	  688	  0	  32	  UL1	  FDB Identifier from the framelist of the sequence in the current	  
; 399	   SUV_SQ_SEQ_FRMLIST	  692	  0	  32	  UL1	  Number of the entry in the framelist of the sequence in the current	  
; 400	   SUV_SQ_SEQ_ID  696	  0	  32	  UL1	  Sequence Identifier in the current Sequence Control Table selected	  
; 401	   SUV_SQ_STATUS  700	  0	  32	  UL1	  Sequencer Status; 0 = OFF_ 1 = ON_ 2=ACTIVE_WAIT	  
; 402	   SUV_SQ_BASE    704	  0	  16	  IU1	  Current Sequencer Start Base; 0=NO_BASE_ 1=HOUR_ 2=DAY. 
; 403	   SUV_SQ_STRT_PAR	  704	  0	  32	  UL1	  Sequencer Configuration parameters for startup (Base and Offset)	  
; 404	   SUV_SQ_STRT_OFFSET	  706	  0	  16	  IU1	  Current Sequencer Start Offset in minutes.	  
; 405	   SUV_SQ_TIME_SLICE	  708	  0	  32	  UL1	  Sequencer available time slice for next frame to be executed (in ticks).	  
; 406	   SUV_CEB_CCD_BIAS0	  712	  0	  8	  UB	  Commanded DAC Channel 0 bias voltage.   
; 407	   SUV_CEB_CCD_BIAS1	  713	  0	  8	  UB	  Commanded DAC Channel 1 bias voltage.   
; 408	   SUV_CEB_CCD_BIAS2	  714	  0	  8	  UB	  Commanded DAC Channel 2 bias voltage.   
; 409	   SUV_CEB_CCD_BIAS3	  715	  0	  8	  UB	  Commanded DAC Channel 3 bias voltage.   
; 410	   SUV_CEB_CCD_BIAS4	  716	  0	  8	  UB	  Commanded DAC Channel 4 bias voltage.   
; 411	   SUV_CEB_CCD_BIAS5	  717	  0	  8	  UB	  Commanded DAC Channel 5 bias voltage.   
; 412	   SUV_CEB_CCD_BIAS6	  718	  0	  8	  UB	  Commanded DAC Channel 6 bias voltage.   
; 413	   SUV_CEB_CCD_BIAS7	  719	  0	  8	  UB	  Commanded DAC Channel 7 bias voltage.   
; 414	   SUV_CEB_SPW_REG	  720	  0	  16	  IU1	  Last read CEB SpaceWire IF FPGA register.	  
; 415	   SUV_CEB_SPW_REG_VAL    722	  0	  16	  IU1	  Last read CEB SpaceWire If FPGA register value. 
; 416	   SUV_CEB_VID_GAIN_L	  724	  0	  16	  IU1	  Left CDS/ADC Video Processing ASIC gain.	  
; 417	   SUV_CEB_VID_GAIN_R	  726	  0	  16	  IU1	  Right CDS/ADC Video Processing ASIC gain.	  
; 418	   SUV_CEB_VID_OS_L	  728	  0	  16	  IU1	  Left CDS/ADC Video Processing ASIC offset voltage.	  
; 419	   SUV_CEB_VID_OS_R	  730	  0	  16	  IU1	  Right CDS/ADC Video Processing ASIC offset voltage.	  
; 420	   SUV_CEB_VCR_VAL	  732	  0	  16	  IU1	  Last read CEB Video Control register value.	  
; 421	   SUV_CEB_BCR_VAL	  734	  0	  16	  IU1	  Last read CEB Bias Control register value.	  
; 422	   SUV_CEB_MUX_BIAS_V_JD  736	  0	  16	  IU1	  CCD Bias Voltage_ JFET Drain.   mV
; 423	   SUV_CEB_MUX_BIAS_V_OD  738	  0	  16	  IU1	  CCD Bias Voltage_ Output Drain. mV
; 424	   SUV_CEB_MUX_BIAS_VOG1  740	  0	  16	  IU1	  CCD Bias Voltage_ Output Gate 1.	  mV
; 425	   SUV_CEB_MUX_BIAS_VOG2  742	  0	  16	  IU1	  CCD Bias Voltage_ Output Gate 2.	  mV
; 426	   SUV_CEB_MUX_BIAS_V_RD  744	  0	  16	  IU1	  CCD Bias Voltage_ Reset Drain.  mV
; 427	   SUV_CEB_MUX_BIAS_VREF  746	  0	  16	  IU1	  CCD Bias Voltage Reference.	  mV
; 428	   SUV_CEB_MUX_BIAS_V_SS  748	  0	  16	  IU1	  CCD Bias Voltage_ Substrate Supply.	  mV
; 429	   SUV_CEB_MUX_BIAS_V_DD  750	  0	  16	  IU1	  CCD Bias Voltage_ Dump Drain.   mV
; 430	   SUV_CEB_MUX_GRND_REFV  752	  0	  16	  IU1	  CEB Secondary Power Supply Rail _ Ground Reference.	  mV
; 431	   SUV_CEB_MUX_P15V	  754	  0	  16	  IU1	  CEB Secondary Power Supply Rail_ 15 Volts.	  mV
; 432	   SUV_CEB_MUX_P30V	  756	  0	  16	  IU1	  CEB Secondary Power Supply Rail_ 30 Volts.	  mV
; 433	   SUV_CEB_MUX_P5V	  758	  0	  16	  IU1	  CEB Secondary Power Supply Rail_ 5 Volts.	  mV
; 434	   SUV_CEB_MUX_T_CCD_1    760	  0	  16	  IU1	  CCD Operating Temperature_ 1.   C
; 435	   SUV_CEB_MUX_T_CCD_2    762	  0	  16	  IU1	  CCD Operating Temperature_ 2.   C
; 436	   SUV_CEB_MUX_T_CEB_PWB  764	  0	  16	  IU1	  CED Internal PWB Temperature.   C
; 437	   SUV_CEB_SPW_LINK_STS   766	  0	  16	  IU1	  CEB SpaceWire interface link status.    
; 438	   SUV_CEB_WGS_DBE_CNT    768	  0	  16	  IU1	  CEB WGS Double Bit Error count. 
; 439	   SUV_CEB_WGS_ERR_ADR    770	  0	  16	  IU1	  CEB WGS Error Address Register. 
; 440	   SUV_CEB_WGS_SBE_CNT    772	  0	  16	  IU1	  CEB WGS Single Bit Error count. 
; 441	   SUV_CEB_WGS_STS	  774	  0	  16	  IU1	  CEB WGS status. 
; 442	   SUV_CEB_PWR_ST 776	  0	  1	  UB	  CEB power state (on/off).	  
; 443	   SUV_CEB_RDOUT_PORT	  776	  1	  2	  UB	  "Readout port  (left = 0, right=1)."    
; 444	   SUV_CEB_WD_TOUT	  776	  3	  7	  IU1	  Camera IF watchdog timer timeout value. Sec
; 445	   SUV_CEB_LAST_RUN_TBL   777	  2	  4	  UB	  Last CEB control table that was started.	  
; 446	   SUV_CEB_ST	  779	  0	  8	  UB	  Camera Electronics Box (CEB) state machine state.	  
; 447	   SUV_CEB_I2C_REG	  780	  0	  16	  IU1	  Last read CEB I2C Device register.	  
; 448	   SUV_CEB_I2C_REG_VAL    782	  0	  16	  IU1	  Last read CEB I2C Device register value.	  
; 449	   SUV_GT_OS_ERR_Y_NR	  784	  0	  32	  UL	  Average GT Y Error during exposure	  nanoR
; 450	   SUV_GT_OS_ERR_Z_NR	  788	  0	  32	  UL	  Average GT Z Error during exposure	  nanoR
