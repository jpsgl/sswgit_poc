
function SUVI_MAKE_BADPIX, infil_flat, dir_flat=dir_flat, $
                           thresh=thresh, do_write=do_write

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     SUVI_MAKE_BADPIX
; CATEGORY:
;     Calibration
; PURPOSE:
;     Read in the flat file and make a bad pixel mask.
; CALLS:
;     out = suvi_make_badpixel()
; METHOD:
;     - Replace bad pixels with the mean value of surrrounding pixels.
; DEPENDENCIES AND RESTRICTIONS:
;     - Must be run under the IDL SolarSoft environment with access to several
;       SolarSoft low level rotuines.
; INPUTS:
;     Set of FITS files from an LTC sequence.
; OUTPUTS:
;     Calculated gain.
; KEYWORD INPUTS:
;       thresh  - the threshold value in the flatfield; defaults to 0.4
;       /write  - if set, then a genx file of bad pixel masks is written out
;       /static - if set, then the "static" bad pixel map is updated (i.e.
;                 the img_path dependent list is copied to the new fields
;                 in the flat_index)
; KEYWORD OUTPUTS:
; CALLED BY ROUTINES:
; ROUTINES CALLED:
; TRAPPED ERRORS:
; UNTRAPPED ERRORS:
; NOTA BENA:
; TODO:
; DEVELOPMENT STATUS:
; TEST AND VERIFICATION DESCRIPTION:
; TESTING STATUS:
; DELIVERY ESTIMATE:
; MAN DAYS REMAINING ESTIMATE:
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION History:
;     2016-04-07 GLS - Updates to doc header.
; VERSION CONTROL STATUS:
;     2016_04-07 - Last commit.
;-
; ==============================================================================

; Set up some defaults
   if not exist(ss_input_type) then interactive = 1
   if not exist(fm) then fm = 1
   if not KEYWORD_SET(thresh) then thresh = 0.006 ; 0.5

; If necessary, define directory paths:
   if get_logenv('SUVI_DATA') eq '' then setup_suvi_env, fm=fm, _extra=_extra

; Read nearest badpix file:
   
; Read the flat file from which the bad pixel list will be generated:

   if not exist(infil_flat) then infil_flat = 'SUVI_FMx_20160121_104936_195_L0c_flat.fits'

   mreadfits, concat_dir(get_logenv('SUVI_FLATS'), infil_flat), index_flat, data_flat
   siz_data = size(data_flat)
   xsiz_data = siz_data[1]
   ysiz_data = siz_data[2]

; Identify bad pixels by simple thresholding:
   ss_badpix = long64(where(data_flat lt thresh, n_badpix))
   badpix_vals = data_flat[ss_badpix]
   x_badpix = ss_badpix mod xsiz_data
   y_badpix = ss_badpix  /  xsiz_data

; Display flat field image:
   isurface, data_flat
   
; Compute adjacent good pixels for each bad pixel (for use in replacing the bad pixel values):
   adj_arr = get_adjacents(x_badpix, y_badpix, data_flat, _extra=_extra)
   x_adj = reform(adj_arr[*,*,0])
   y_adj = reform(adj_arr[*,*,1])

; Print out bad pixel and surrounding pixel x and y coordinates:
   for i=0, n_badpix-1 do begin
      print, ''
      print, '[' + string(x_badpix[i], '$(i4.4)') + ', ' + string(y_badpix[i], '$(i4.4)') + ']'
      print, string(reform(x_adj[i,*]),'$(f5.0)')
      print, string(reform(y_adj[i,*]),'$(f5.0)')
   endfor
   print, ''

; Define structure to contain bad pixel locations and other information:
   badpix_str = index_flat
   badpix_str = add_tag(badpix_str, anytim(file2time(infil_flat), /ccsds), 'date_obs')
   badpix_str = add_tag(badpix_str, x_badpix, 'x_badpix')
   badpix_str = add_tag(badpix_str, y_badpix, 'y_badpix')
   badpix_str = add_tag(badpix_str, ss_badpix, 'ss_badpix')
   badpix_str = add_tag(badpix_str, badpix_vals, 'badpix_vals')

; Optionally write bad pixel info to both a genx file and an ASCII file:
if KEYWORD_SET(do_write) then begin
   file_badpix = concat_dir(get_logenv('SUVI_BADPIX'), 'suvi_badpix_' + time2file(badpix_str.date_obs))
   SAVEGEN, file=file_badpix, struct=badpix_str

   suvi_write_badpix, x_badpix, y_badpix, x_adj, y_adj, badpix_str, _extra=_extra
endif
   
;===============================

; Template badpix code from IRIS:

;sswdata = CONCAT_DIR('$SSW_IRIS','data')
;if N_ELEMENTS(ttag) eq 0 then ttag = TIME2FILE(/sec, RELTIME(/now))

;if N_TAGS(flat_index) eq 0 then begin

; Read the latest flat_index file
;	flatinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*flat.genx'))
;	flatind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(flatinds), 0, 15)))
;	latest = WHERE(flatind_tai eq MAX(flatind_tai))
;	RESTGEN, file = flatinds[latest], flat_index
;endif

; Load the old bad pixel map for reference
;if KEYWORD_SET(static) then begin
;	sswdata = CONCAT_DIR('$SSW_IRIS','data')
;	badinds = FILE_SEARCH(CONCAT_DIR(sswdata, '*badpix.genx'))
;	badind_tai = ANYTIM2TAI(FILE2TIME(STRMID(FILE_BASENAME(badinds), 0, 15)))
;	latest = WHERE(badind_tai eq MAX(badind_tai))
;	RESTGEN, file = badinds[latest], str = badpix_str
;endif

; Loop through the index fields and make a structure tag for each
;nflat = N_ELEMENTS(flat_index)
;for i = 0, nflat - 1 do begin
;	imgpath = STRMID(flat_index[i].img_path, 0, 3)
;	case imgpath of
;		'SJI'	:	begin
;			if KEYWORD_SET(static) then begin
;				pathind = (WHERE(flat_index.img_path eq flat_index[i].img_path))[0]
;				badpix = badpix_str.(pathind)
;			endif else begin
;				MREADFITS, CONCAT_DIR(sswdata, flat_index[i].filename), hdr, dat
;				badpix = LONG64(WHERE(dat lt thresh))
;				if KEYWORD_SET(fix) then case 1 of
;					flat_index[i].img_path eq 'SJI_2796' : badpix = IRIS_PREP_FIX_BADPIX_2796(badpix)
;					flat_index[i].img_path eq 'SJI_2832' : badpix = IRIS_PREP_FIX_BADPIX_2796(badpix)
;					flat_index[i].img_path eq 'SJI_1330' : badpix = IRIS_PREP_FIX_BADPIX_1330(badpix)
;       endcase
;			endelse
;		end
;		'FUV'	:	begin
;			badpix = [3125941ll, 2699108ll, 2678390ll]
;		end
;		else	:	begin
;			badpix = [0ll]
;		end
;	endcase
	; Make a new structure tag for this recnum
;	tagname = 'F' + STRTRIM(flat_index[i].recnum, 2)
;	if i eq 0 then begin
;		result = CREATE_STRUCT(tagname, badpix)
;	endif else begin
;		result = CREATE_STRUCT(result, tagname, badpix)
;	endelse
;endfor

;if KEYWORD_SET(write) then begin
;	if N_ELEMENTS(path) eq 0 then path = CONCAT_DIR('$SSW_IRIS','data')
;	fname = CONCAT_DIR(path, ttag + '_badpix.genx')
;	SAVEGEN, file = fname, str = result
;endif

;===============================


RETURN, badpix_str

end
