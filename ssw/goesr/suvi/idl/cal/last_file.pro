
function last_file, t0, lag=lag, t_delt=t_delt, struct=struct, $
  time=time, t_lag_sec=t_lag_sec, file=file, head=head, data=data, lil_data=lil_data, $
  all=all, method=method, instr=instr, wave=wave, $
  do_print=do_print, use_suvi_cache=use_suvi_cache, $
  $
  ds=ds, search=search

wave_arr =        [  '94', '131', '171', '193', '211', '304', '335','1600','1700','4500','blos', 'mag','cont']
instr_arr =       [ 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'aia', 'hmi', 'hmi', 'aia']
wave_string_arr = ['0094','0131','0171','0193','0211','0304','0335','1600','1700','4500','blos','blos','cont']

if not exist(dir_cache) then dir_cache = '/archive1/suvi/suvi_data/fm1/suvi_local_sdo_cache'

if not exist(wave) then wave = '171'
n_wave = n_elements(wave)
ss_wave_match = where(strtrim(wave,2) eq wave_arr, n_match)
ss_wave_match = ss_wave_match[0]
wave_string = wave_string_arr[ss_wave_match]

if get_logenv('AIA_DATA') eq '' then set_logenv, 'AIA_DATA', '/cache/sdo/AIA/lev1p5'
if not exist(file) then file = 1

if keyword_set(use_suvi_cache) then begin
   files_local = file_list(dir_cache, 'AIA????????_??????_' + wave_string + '.fits')
   files = files_local
endif else begin

   if exist(ds) then ds_in = strtrim(ds) else ds = '1.5'
   if not exist(method) then method = 'time2files' ; 'file_search'
   if not exist(t_arr) then t_delt = [-01,-02,-04,-08,-16,-32,-64]
   n_delt = n_elements(t_delt)

   if not exist(t0) then t0 = anytim(ut_time(!stime), /ccsds)
   if keyword_set(verbose) then print, ' t0 = ' + anytim(t0, /ccsds)
   t0_sec = anytim(t0)

   if not exist(xsiz_lil) then xsiz_lil = 1024
   if not exist(ysiz_lil) then ysiz_lil = xsiz_lil

; Find latest file of desired type:

   case method of

      'time2files': begin
         files = ''
         i = 0
         while ((files[0] eq '') and (i le n_delt-1)) do begin
            print, 'Searching last ' + strtrim(abs(t_delt[i]),2) + ' hours'

            paths = ssw_time2paths(anytim(timegrid(anytim(anytim(!stime)-10*3600d,/ccsds),!stime,/hours),/ccsds),/hour)

            files = sdo_time2files(anytim(t0_sec + t_delt[i]*3600d0, /ccsds), anytim(t0_sec, /ccsds), $
                                   level=level, parent=get_logenv('AIA_DATA'), wave=strtrim(wave,2), aia=aia, hmi=hmi)
            i = i+1
         endwhile
      end

;  'file_search':
;    Result = FILE_SEARCH(Dir_Specification, Recur_Pattern)

   endcase

endelse
   
n_files = n_elements(files)
last_file = files[n_files-1]
retval = last_file

if last_file ne '' then begin

  if (keyword_set(time) or keyword_set(lag) or keyword_set(struct)) then last_time = file2time(last_file)
  if (keyword_set(head) or keyword_set(struct)) then begin
    if keyword_set(data) or keyword_set(lil_data) or keyword_set(all) then $
      read_sdo, last_file, last_header, last_data, /uncomp_delete else $
      read_sdo, last_file, last_header, /uncomp_delete
    endif

  if (keyword_set(lag) and exist(last_time)) then begin
    t_lag_sec = t0_sec - anytim(last_time)
    day_lag =  long(t_lag_sec)/86400l
    hrs_lag = (long(t_lag_sec) - day_lag*86400l)/3600l
    min_lag = (long(t_lag_sec) - day_lag*86400l - hrs_lag*3600ll)/60l
    sec_lag =  long(t_lag_sec) mod 60l

    last_lag = string(min_lag, format='(i2.2)') + ' min'
    if ( (hrs_lag ne 0) or (day_lag ne 0) ) then $
      last_lag = string(hrs_lag, format='(i2.2)') + 'h ' + last_lag
    if day_lag ne 0 then last_lag = strtrim(day_lag,2) + ' day ' + string(hrs_lag, format='(i2.2)') + ' hr'
  endif

  if keyword_set(struct) then begin
    last_struct = $
      { last_file:last_file, last_time:last_time, last_header:last_header }
    if keyword_set(data) or keyword_set(all) then $
      last_struct = add_tag(last_struct, last_data, 'last_data')
    if keyword_set(lil_data) or keyword_set(all) then begin
      last_lil = rebin(last_data, xsiz_lil, ysiz_lil, /samp)
      last_struct = add_tag(last_struct, last_lil, 'last_lil')
    endif
  endif

  if keyword_set(struct) then retval = last_struct else $
    if keyword_set(lag) then retval = last_lag else $
      if keyword_set(time) then retval = last_time else $
        if keyword_set(head) then retval = last_header else $
          if keyword_set(data) then retval = last_data

endif

if (keyword_set(do_print) and exist(last_header)) then begin
  t_names = tag_names(last_header)
  t_names = t_names[sort(t_names)]
  print, t_names, format = '$(8a-12)'
endif

return, retval

end
