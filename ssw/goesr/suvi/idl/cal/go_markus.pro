
pro go_markus

;GOTO,SKIP_READ
;__________________________DATA FILE NAMES______________________________
files = swap_time2files_local('2016-04-06 07:30', '2016-04-06 08:30')

;__________________________READ DATA______________________________
mreadfits,files,index,data
nf	=n_elements(files)
help,files,nf,index,data
print,'Number of files found = ',nf

;__________________________DISPLAY IMAGES____________________________
;xstepper, data

;__________________________UNSPIKE IMAGES____________________________
SKIP_READ:
dim	=size(data)
nx	=dim(1)
ny	=dim(2)
nt	=dim(3)
window,0,xsize=(nx/2)*3+1,ysize=ny/2

ans0 = 'yes'
it = 1

while ( (it le (nt-2)) and (strlowcase(strmid(ans0,0,1)) ne 'n') ) do begin
;for it=1,nt-2 do begin
 statistic,data(*,*,it),zavg,zsig
 c1	=zavg-3*zsig
 c2	=zavg+3*zsig
 statistic,data(*,*,it)

 thr	=1.15
 data_unspiked=trace_unspike_time(index,data(*,*,it-1:it+1),outindex,thresh=thr)
;tv,bytscl(data(*,*,it),min=c1,max=c2),0,0
 tv,rebin(bytscl(data(*,*,it),min=c1,max=c2), nx/2, ny/2, /samp), 0,0
 tv,rebin(bytscl(reform(data_unspiked[*,*,1]),min=c1,max=c2), nx/2, ny/2, /samp), nx/2+1,0
 tv,rebin(bytscl(data(*,*,it)-data_unspiked,min=c1,max=c2), nx/2, ny/2, /samp), (2*(nx/2))+1,0
 statistic,data_unspiked

 it = it + 1
;ans0 = ''
 read,'continue?',ans0
;if strlowcase(strmid(ans0,1) eq 'n') then stop
;endfor
endwhile
 
end
