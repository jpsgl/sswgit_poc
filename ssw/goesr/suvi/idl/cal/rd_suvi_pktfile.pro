
pro rd_suvi_pktfile, pktfname, subimg_cube, do_sub=do_sub, fullheader=fullheader, $
                     do_display=do_display

;+
; Extract images from SUVI packet file
;
; Version control information:
; $Id:       suvi_img.pro 335 2013-09-19 22:17:53Z mateos $
; $Date:     2016-06-28                                   $
; $Revision: 1                                            $
; $Author:   mateos                                       $
; $HeadURL:  svn+ssh://suvitc@suvifsw1.atc.lmco.com/home/suvisvn/idltc/suvi_img.pro $
;
; History:
;    2016-06-28 - GLS - Written, based upon SUVI_IMG.PRO (M. Mateos, MDM, GAL, LAW)
;                     - Relevant original history records from suvi_img.pro appended below.
;    2016-05-18 - LAW - Added isp_packet call so we can store info in the fits header.
;    2012-07-17 - MM  - Add free_lun wherever it was missing.
;    2012-07-12 - MM  - Add checks for data size of last packet.
;    2012-07-10 - MM  - Read number of rows and columns from image packet header.
;    2012-07-09 - MM  - Skip leading packet #0 (copy of ISP). Start filling at
;                       img[0], not img[data_words].
;    2012-05-12 - GAL - Copied suvi_img to suvi_imgsmemi for emi and made the
;                       following changes:
;                       - Changed "hard-coded" dim2 from 1280 to 128 and clipped first
;                         and last 50 column pixels from the image for "inside" stat
;                         calculation, as suggest by Mons.
;    2012-05-29 - MDM - Clip first 4 lines to handle dropped packet because of EOF
;                       vs new image code error
;-

; Define misc defaults:
if not exist(wait_disp) then wait_disp = 2

; Open packet file:
OPENU, lun0, pktfname, /get_lun

; Define two separate templates for packet structure:
hdr_dwords = 6
data_words = 4074l
data_bytes = data_words*2
pkt_template  = {pkt,            prihdr:BYTARR(6), sechdr:BYTARR(14), $
                                 imghdr:LONARR(hdr_dwords), imgdata:INTARR(data_words)}
pkt_template2 = {science_packet, ccsds_prime:bytarr(6), ccsds_sec:bytarr(14), $
                                 data_head:bytarr(24), data:bytarr(data_bytes)}

; Define pixel vectors (to contain original packet data and substitute image data:
img_vector = INTARR( 1722604 )
subimg_vector = INTARR( 1722604 )

; Fill subimg_vector with substitute image pixels:
if size(subimg_cube, /type) ne 2 then begin
   STOP, 'RD_SUVI_PKTFILE: SUBIMG is not Integer.  Returning.'
   return
endif
siz_subimg_cube = size(subimg_cube)
subdim1 = siz_subimg_cube[1] & subdim2 = siz_subimg_cube[2]

; Setup error handling using CATCH (especially for EOF errors):
CATCH, error_status
if error_status ne 0 then begin
   PRINT, 'SUVI_IMG detected an error: ' + !error_state.msg
   RETURN
endif

eof_cntr = 0
recnum = 0
	
; Define two associated variables for the two different packet file structures:
rec  = ASSOC( lun0, pkt_template )   ; pkt_template  is how suvi_img was originally written
rec2 = ASSOC( lun0, pkt_template2 )  ; pkt_template2 is used to pass the isp_packet

; Begin reading the packet file.
thisrecnum = recnum + 1l
pktdata = rec[thisrecnum]
; Initialize flags and counters:
new_img = 0
finished_img = 0
n_img = 0

; Begin new image loop:

while ((eof_cntr eq 0) and (recnum gt -1)) do begin 

   apid = ( ( pktdata.prihdr[0] and 3 ) * 256 ) + pktdata.prihdr[1]
   ; Construct image file time:
   julian_day_epoch = 2451545D  ; This corresponds to 2000-01-01 12:00 UT (instead of 00:00UT)
                                ; But it seems to give the right image time nonetheless.
   daynum = pktdata.sechdr[0]*65536l    + pktdata.sechdr[1]*256l   + pktdata.sechdr[2]
   msec   = pktdata.sechdr[3]*16777216l + pktdata.sechdr[4]*65536l + $
            pktdata.sechdr[5]*256l      + pktdata.sechdr[6] 
   usec   = pktdata.sechdr[7]*256l      + pktdata.sechdr[8] 
   
   ; Extract SUVI image serial number:
   serial_no = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x

   img = img_vector
   subimg_vector[0] = reform(subimg_cube[*,*,n_img], subdim1*subdim2)
   subimg = subimg_vector

   if recnum eq 0 then begin
      whole_img = 1
      firstpacket = rec2[0] ; process the isp_packet
   endif else whole_img = 0 ; special case for first record

   ; Begin image extraction loop.
   ; Read each packet, calculate the start postion number of image pixels in this packet,
   ; and copy packet data to image array
   
   while (not new_img) do begin

      ; Note that the 16-bit words are swapped. For example,
      ; Pkt Flag is at i16[3] not i16[2]. Pkt Number is at i16[5] not i16[4].
      i16 = unsign(fix(swap_endian( pktdata.imghdr),0,12))
      i32 = swap_endian( pktdata.imghdr)
    
      pkt_num = i16[5]
      serial_no = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x

      if pkt_num eq 0 then begin
         whole_img = 1
         print, "Found leading ISP packet. Moving on to 1st image data packet"
	 
         firstpacket = rec2[thisrecnum] ; remember the isp_packet
         print, ' First packet is ', + strtrim(thisrecnum,2)

         thisrecnum = thisrecnum + 1l
         pktdata = rec[thisrecnum]
      endif
      
      ; Image data starts at packet number 1 (0 is reserved for copy of ISP)
      num_pixls = pkt_num * data_words
      indx = (pkt_num-1) * data_words

      ; First data packet's contribution to the final image is inserted into
      ; image vector array here:
      img[indx: indx + (data_words-1)] = pktdata.imgdata

      ; Optionally insert piece of substitute image into this packet's data block:
      if keyword_set(do_sub) then begin
         pktdata.imgdata = subimg[indx: indx + (data_words-1)]
         rec[thisrecnum] = pktdata
      endif

      flag = i16[3]

      ; Read next record:
      thisrecnum = thisrecnum + 1l
      pktdata = rec[thisrecnum]
      serial_no_new = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x

      if (serial_no_new ne serial_no) then begin
         new_img = 1
         finished_img = 1
         print, i16[5], format='("New image header detected after Packet No. ", i0)'
      endif

      if EOF(lun0) then begin
         eof_cntr += 1
         new_img = 1
         print, "Found EOF", pkt_num
         if flag eq 3 then finished_img = 1
         ; Capture data if boundary case: last packet added to img[] was the
         ; next-to-last packet of the entire image, and the last packet of the
         ; entire image ends up EOF.
         i16_nxt = unsign(fix(swap_endian( pktdata.imghdr),0,12))
         flag_nxt = i16_nxt[3]
         if flag_nxt eq 3 then begin
            pkt_num_nxt = i16_nxt[5]  ; note that words are swapped
            ; Last packet may not be full of data
            data_words_nxt = 2*i16_nxt[4]
            exp_data_words = (i16_nxt[10] * i16_nxt[11]) - pkt_num * data_words

            if data_words_nxt ne exp_data_words then begin
               print, data_words_nxt, exp_data_words, $
                      format = '("ERROR: Header-specified size (",i5,") of data in last packet' + $
                               'does not match expected size (",i4,"). Using expected size")'
               data_words_nxt = exp_data_words
            endif

            ; Current packet's contribution to the final image is inserted into
            ; image vector array here:
            indx = pkt_num * data_words ; next-to-last packet was full
            img[indx: indx + (data_words_nxt-1)] = pktdata.imgdata[0:data_words_nxt-1]

            ; Optionally insert piece of substitute image into this packet's data block:
            if keyword_set(do_sub) then begin
               pktdata.imgdata[0:data_words_nxt-1] = subimg[indx: indx + (data_words_nxt-1)]
               rec[thisrecnum] = pktdata
            endif

            i32_nxt=swap_endian(pktdata.imghdr)
         endif
      endif
   endwhile

   new_img = 0
 
   if ((pkt_num lt 421) and (eof_cntr gt 0)) then begin ; we hit EOF before image completed
STOP
      eof_cntr +=1
      recnum -= 10
   endif
   
   print,' Beginning at ' + strtrim(recnum,2) + ' difference in recnums is ', thisrecnum - recnum
   recnum = thisrecnum

   ; If we have a full image then convert pixel vector into a 2D image array:
   ; Get number of rows and columns from last image packet:

   if whole_img and finished_img then begin
      dim1 = i16[10]               ; columns (words are swapped in i16[])
      dim2 = i16[11]               ; rows

      outimage = UINTARR(dim1, dim2)
      outimage_lastpix = dim1 * dim2 - 1
      outimage[0] = img[0:outimage_lastpix]

      finished_img = 0
      n_img = n_img + 1
      
      if keyword_set(do_display) then begin
         erase & tvscl, safe_log10(outimage<1000) & wait, wait_disp
      endif
   endif
    
endwhile 

; Close packet file and free the lun:
FREE_LUN, lun0

end
