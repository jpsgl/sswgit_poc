
function tag2pkt, struct, pkt, pkt_map


; Define packet structure:
  pkt_template0  = {pkt0, img_hdr:BYTARR(3465216l)}
  pkt_template1  = {pkt1, img_hdr:BYTARR(3465216l)}
	
; Define associated variables for reading one img_hdr pair:
  rec0 = ASSOC( lun0, pkt_template0 )
  rec1 = ASSOC( lun1, pkt_template1 )


  tnames = strupcase(tag_names(struct))
  n_tags = n_elements(tnames)

  typ    = pkt_map.type
  stbyte = pkt_map.stbyte
  stbit  = pkt_map.stbit
  nbits  = pkt_map.nbits
  mnem   = pkt_map.name

  for i=0. n_tags-1 do begin
     ss_match = where(tnames[i] eq mnem, n_match)
     if n_match eq 1 then begin
        



destroy, outstruct

outstruct={name:'',val:0L}
len=n_elements(map)
outstruct=replicate(outstruct,len)

for i=0,len-1 do begin
        typ=map[i].type
	stbyte=map[i].stbyte
	stbit=map[i].stbit
	nbits=map[i].nbits
	mnem=map[i].name
if ( (keyword_set(do_stop)) and (typ eq 'UL') ) then STOP
;	help, mnem
	case typ of
		'UL1':	out=long(data(stbyte:stbyte+3),0,1)		;unsigned, noswap
		'UL':	out=long(data(stbyte:stbyte+3),0,1)		;typo, should be UL1
		'IL1':	out=long(data(stbyte:stbyte+3),0,1)		;signed, noswap
		'IS1':	out=fix(data(stbyte:stbyte+1),0,1)		;signed, noswap
		'IU1':	out=fix(data(stbyte:stbyte+1),0,1)		;unsigned, noswap
		'UB':	out=reform(data(stbyte))			;unsigned byte
		else: 	out=long(data(stbyte:stbyte+3),0,1)
	endcase
	qswap=is_member(/swap_os)
	if (qswap) then dec2sun, out

;;;-additional processing
	case typ of
		'IU1':	out=unsign(out,16)	;make unsigned (changes the output type too though)
		'UL1':	out=ulong(out)	;
		'UL':	out=ulong(out)	;
		else:
	endcase

	case typ of
		'UL1':	nbit4type=32
		'UL':	nbit4type=32
		'IL1':	nbit4type=32
		'IS1':	nbit4type=16
		'IU1':	nbit4type=16
		'UB':	nbit4type=8
		else:	nbit4type=32
	endcase

	if (nbit4type ne nbits) then begin
		stbit=nbit4type-stbit-nbits	;database terminology has bit0=MSB
		out=mask(out,stbit,nbits)
	endif
	out=reform(out)
	outstruct(i).name=mnem
	outstruct(i).val=out
endfor 
return,outstruct
end

function mk_suvi_isp_struct, pktdata, head_only=head_only

if (n_elements(isp_map) eq 0) then isp_map=mk_suvi_isp_map()
nstruct=n_elements(pktdata)

for k=0,nstruct-1 do begin
        tmp=ext_mnem(pktdata(k).data_head,isp_map.head)
	head=create_struct(tmp(0).name,tmp(0).val)
	len=n_elements(tmp)

        for i=1,len-1 do head=add_tag(head,tmp(i).val,tmp(i).name)

        if (keyword_set(head_only)) then begin
		if (k eq 0) then begin
		   isp=create_struct('head',head)
                   isp=replicate(isp,nstruct)
		endif else isp(k).head=head
	endif else begin
                tmp=ext_mnem(pktdata(k).data,isp_map.tlm,/do_stop)
                mnem=create_struct(tmp(0).name,tmp(0).val)
		len=n_elements(tmp)

                for i=1,len-1 do mnem=add_tag(mnem,tmp(i).val,tmp(i).name)

                if (k eq 0) then begin
		   isp=create_struct('mnem',mnem,'head',head)
                   isp=replicate(isp,nstruct)
		endif else begin
		   isp(k).head=head
                   isp(k).mnem=mnem
		endelse
	endelse
endfor
isp2=isp
return, isp

end
