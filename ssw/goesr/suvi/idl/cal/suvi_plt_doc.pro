
; ==============================================================================
;+
; ROUTINE NAME:
;     suvi_check_image
; CATEGORY:
;     SUVI data quality control.
; PURPOSE:
;     Check quality of image and comopleteness of image header, and assign values
;     to appropriate quality tags.  Such tags should include, for example:
;       missing_tag_list (comma delimited)
;       percentd - percent of good pixels
;       ratio of first moment of pixel values to mission mean of same
;       ratio of second moment of pixel values to mission mean of same
;       ratio of third moment of pixel values to mission mean of same
; CALLS:
;     suvi_check_image, iindex, idata, oindex, odata
;     suvi_check_image, iindex, idata, outfil=outfil
;     suvi_check_image, infil=infil, oindex, odata
;     suvi_check_image, infil=infil, outfil=outfil
; ARRAYS ALLOWED: Yes. N.B. - Arrays allowed in call (either array of file names or 
; INPUTS:
;     EITHER:
;     iindex, idata - Input SUVI FITS header structure and image data array
;     OR:
;     infil
; OUTPUTS:
;     EITHER:
;     oindex, odata - Output SUVI FITS header structure and image data array
;     OR:
;     outfil - Updated FITS file with quality tags populated.
; RUNNING MODES:
;     Interactive: User file selection options. Display options.
;     Batch: Pipeline to run through specified dates and log results.
; DEPENDENCIES AND RESTRICTIONS:
;     Must be run under the IDL SolarSoft environment with access to several
;     SolarSoft low level rotuines.
; RUNNING ENVIRONMENT SETUP:
;     The routine calls SETUP_SUVI_ENV to set up the environment.
; REFERENCE DOCUMENTS:
;     SUVI_FITS_KEYWORD_LIST - /Volumes/mars/suvi/suvi_docs/suvi_fits_keyword_list.pro
;-

; ==============================================================================
;+
; ROUTINE NAME:
;     suvi_mfits
; CATEGORY:
;     Image processing.
; PURPOSE:
;     Packet reader and FITS file generator.
; CALLS:
;     suvi_mfits, pktfil, oindex, odata
;     suvi_mfits, pktfil, outfil=outfil
; ARRAYS ALLOWED: Yes.
; INPUTS:
;     SUVI packet file
; OUTPUTS:
;     EITHER:
;     oindex, odata - vector of SUVI FITS header structures and cube of SUVI image data arrays
;     OR:
;     outfile - Output FITS file(s).
; DEPENDENCIES AND RESTRICTIONS:
;     Must be run under the IDL SolarSoft environment with access to several
;     SolarSoft low level rotuines.
;-

; ==============================================================================
;+
; ROUTINE NAME:
;     suvi_limbfit
; CATEGORY:
;     Image calibration.
; PURPOSE:
;     Fit solar limb to obtain image platescale and sun center coordinates.
; CALLS:
;     suvi_limbfit, iindex, idata, oindex, odata
;     suvi_limbfit, iindex, idata, outfil=outfil
;     suvi_limbfit, infil=infil, oindex, odata
;     suvi_limbfit, infil=infil, outfil=outfil
; ARRAYS ALLOWED: Yes.
; INPUTS:
;     EITHER:
;     iindex, idata - Input SUVI FITS header structure and image data array
;     OR:
;     infil - Input FITS file
; OUTPUTS:
;     EITHER:
;     oindex, odata - Output SUVI FITS header structure and image data array
;     OR:
;     outfil - Output FITS file with updated WCS header tags.
; DEPENDENCIES AND RESTRICTIONS:
;     Must be run under the IDL SolarSoft environment with access to several
;     SolarSoft low level rotuines.
;-

; ==============================================================================
;+
; ROUTINE NAME:
;     suvi_flatfield
; CATEGORY:
;     Image calibration.
; PURPOSE:
;     Generate flat field image for a given wavelength.
; CALLS:
;     suvi_flatfield, time, infil=infil, t_off_max=t_off_max, n_img_min=n_img_min, $
;                     n_img_max=n_img_max 
; INPUTS:
;     EITHER:
;     infil - list of SUVI infiles to be used in the generation of the flat field image.
;     OR:
;     time - reference time for search image search
;     t_off_max - maximum time offset for input images
;     n_img_min - minimum number of images to be used for the generation of the flat
;     n_img_max - maximum number of images to be used for the generation of the flat
; OUTPUTS:
;     EITHER:
;     oindex, odata - Output SUVI FITS header structure and data array for
;                     generated flat field.
;     OR:
;     outfil - Output FITS file for generated flat field image.
; DEPENDENCIES AND RESTRICTIONS:
;     Must be run under the IDL SolarSoft environment with access to several
;     SolarSoft low level rotuines.
;-

Calibration Tools:
SUVI_MAKE_BADPIX
SUVI_MAKE_DARK
SUVI_LTC

Image Processing:
SUVI_PREP
SUVI_FILTER_CHECK
SUVI_SHARPNESS

Non-Image PLT Tools:
SUVI_GTCAL
SUVI_ANALYZE_MECH
SUVI_THROUGHPUT
SUVI_GET_RESPONSE

Non-PLT Image Processing:
SUVI_DEM_FIT
SUVI_IMAGE_BLEND
SUVI_DESPIKE
SUVI_ALIGN_IMAGE
SUVI_GET_DISP

