
pro gen_syn_cube, t0, t1, hours=hours, days=days, wave_suvi=wave_suvi, $
                  naxis1_suvi=naxis1_suvi, naxis2_suvi=naxis2_suvi, $
                  index_suvi_template=index_suvi_template, $
                  index_sdo=index_sdo, data_sdo=data_sdo, $
                  index_arr=index_arr, data_arr=data_arr, $
                  do_xstep=do_xstep, do_save=do_save, $
                  verbose=verbose, _extra=_extra

  if not exist(days) then $
     if not exist(hours) then $
        hours = 4
  if not exist(t0) then t0 = '01-mar-2016'
  if not exist(t1) then t1 = '16-mar-2016'
  if not exist(wave_suvi) then wave_suvi = 171
  if not exist(naxis1_suvi) then naxis1_suvi = 1330 ; 1280
  if not exist(naxis2_suvi) then naxis2_suvi = 1292 ; 1280

  if not exist(wait_disp) then wait_disp = 0.2 ;0.5
  if not exist(n_disp_rep) then n_disp_rep = 1 ;2
  
  wave_arr_sdo  = [4500, 1600, 1700, 304, 335, 171, 131, 211, 193,  94,  94]
  wave_arr_suvi = [   0,    0,    0, 304,   0, 171, 131,   0, 195,  94, 284]

; Define directory paths:
  if not exist(fm) then fm = 1
  setup_suvi_env, fm=fm, _extra=_extra
  set_logenv, 'SUVI_SYN_DATA', concat_dir(get_logenv('SUVI_FM'), 'syn_data')

  case 1 of

     (not exist(index_sdo)) and (exist(index_suvi_template)): begin
          t_arr = anytim(index_suvi_template.img_pkt_time, /ccsds)
          n_img = n_elements(t_arr)
for i=0,n_img-1 do t_arr[i] = reltime(t_arr[i], days=-8, out='ccsds')
STOP          
          if keyword_set(verbose) then prstr, t_arr

          delvarx, index_arr, data_arr
          for i=0, n_img-1 do begin
             delvarx, index_sdo, data_sdo
             wave_suvi = fix(strmid(index_suvi_template[i].asname,0, $
                                    strlen(index_suvi_template[i].asname)-1))
             ss_wave_match = where(wave_suvi eq wave_arr_suvi, n_match)
             if n_match gt 0 then $
                wave_sdo = (wave_arr_sdo[ss_wave_match])[0] else $
                   stop, 'gen_syn_cube: Cannot find match to requested SUVI wave'             
             file_sdo = last_file(t_arr[i], wave=wave_sdo)
             aia2suvi, file_sdo=file_sdo, t0=t0, wave_suvi=wave_suvi, raw=raw, $
                       naxis1_suvi=naxis1_suvi, naxis2_suvi=naxis2_suvi, $
                       suvi_index_template=index_suvi_template[i], $
                       index_sdo=index_sdo, data_sdo=data_sdo, $
                       index_suvi=index_suvi, data_suvi=data_suvi, $
                       do_disp=0, _extra=_extra
             if not exist(index_arr) then begin
                index_arr = index_suvi
                data_arr = data_suvi
             endif else begin
                index_arr = concat_struct(index_arr, index_suvi)
                data_arr = [[[data_arr]],[[data_suvi]]]
             endelse
          endfor
     end

     (exist(index_sdo)) and (not exist(index_suvi_template)): begin
          n_img = n_elements(index_sdo)

          delvarx, index_arr, data_arr
          for i=0, n_img-1 do begin
             aia2suvi, file_sdo=file_sdo, t0=t0, wave_suvi=wave_suvi, raw=raw, $
                       naxis1_suvi=naxis1_suvi, naxis2_suvi=naxis2_suvi, $
                       index_sdo=index_sdo[i], data_sdo=data_sdo[*,*i], $
                       index_suvi=index_suvi, data_suvi=data_suvi, $
                       do_disp=0, _extra=_extra
             if not exist(index_arr) then begin
                index_arr = index_suvi
                data_arr = data_suvi
             endif else begin
                index_arr = concat_struct(index_arr, index_suvi)
                data_arr = [[[data_arr]],[[data_suvi]]]
             endelse
          endfor
     end

     (not exist(index_sdo)) and (not exist(index_suvi_template)): begin
          ss_wave_match = where(wave_suvi eq wave_arr_suvi, n_match)
          if n_match gt 0 then $
             wave_sdo = (wave_arr_sdo[ss_wave_match])[0] else $
             stop, 'gen_syn_cube: Cannot find match to requested SUVI wave'

          t_grid = anytim(timegrid(t0, t1, hours=hours, days=days), /ccsds)
          n_grid = n_elements(t_grid)
          n_img = n_grid
          if keyword_set(verbose) then prstr, t_grid

          delvarx, index_arr, data_arr
          for i=0, n_grid-1 do begin
             file_sdo = last_file(t_grid[i], wave=wave_sdo)
             aia2suvi, file_sdo=file_sdo, t0=t0, wave_suvi=wave_suvi, raw=raw, $
                       naxis1_suvi=naxis1_suvi, naxis2_suvi=naxis2_suvi, $
                       index_sdo=index_sdo, data_sdo=data_sdo, $
                       index_suvi=index_suvi, data_suvi=data_suvi, $
                       do_disp=0, _extra=_extra
             if not exist(index_arr) then begin
                index_arr = index_suvi
                data_arr = data_suvi
             endif else begin
                index_arr = concat_struct(index_arr, index_suvi)
                data_arr = [[[data_arr]],[[data_suvi]]]
             endelse
          endfor
     end

  endcase     

  if keyword_set(do_disp) then begin
     lil_data = safe_log10(rebin(data_arr, naxis1_suvi/2, naxis2_suvi/2, n_img, /samp))
     wdef,0,naxis1_suvi/2, naxis2_suvi/2
     for ii=0, n_disp_rep-1 do begin
        for jj=0, n_img-1 do begin
           tvscl, lil_data[*,*,jj] & wait, wait_blink
        endfor
     endfor
  endif
     
  if keyword_set(do_xstep) then $
     xstepper, safe_log10(rebin(data_arr, naxis1_suvi/2, naxis2_suvi/2, n_img, /samp))
  
  if keyword_set(do_save) then begin
     filnam_syn = 'suvi_syn_' + strtrim(wave_suvi,2) + '.sav'
     file_syn = concat_dir(get_logenv('SUVI_SYN_DATA'), filnam_syn)
     if keyword_set(verbose) then help, file_syn
     save, file=file_syn, index_arr, data_arr
  endif

  end
