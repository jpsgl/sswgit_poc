
function suvi_gen_case_set, index, tab=tab, do_print=do_print

  case_num  = indgen(66)
  
  asname    = [            094, 094, 094, 131, 131, 131, 171, 171, 195, 195, 284, 284, 304, 304 ]
  asname    = [ asname,    094, 094, 094, 131, 131, 131, 171, 171, 195, 195, 284, 284, 304, 304 ]
  asname    = [ asname,    094, 094, 094, 131, 131, 131, 171, 171, 195, 195, 284, 284, 304, 304 ]
  asname    = [ asname,    094, 094, 094, 131, 131, 131, 171, 171, 195, 195, 284, 284, 304, 304 ]
  asname    = [ asname,    195, 195, 195, 195, 195, 195, 195, 195, 195, 195 ]

  asname = strtrim(asname,2) + 'A'
  
  aspos     = [             34,  34,  34,   4,   4,   4,  10,  10,  16,  16,  22,  22,  28,  28 ]
  aspos     = [ aspos,      34,  34,  34,   4,   4,   4,  10,  10,  16,  16,  22,  22,  28,  28 ]
  aspos     = [ aspos,      34,  34,  34,   4,   4,   4,  10,  10,  16,  16,  22,  22,  28,  28 ]
  aspos     = [ aspos,      34,  34,  34,   4,   4,   4,  10,  10,  16,  16,  22,  22,  28,  28 ]
  aspos     = [ aspos,      16,  16,  16,  16,  16,  16,  16,  16,  16,  16 ]

  asstatus  = [              0,   0,   0,   1,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5 ]
  asstatus  = [ asstatus,    0,   0,   0,   1,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5 ]
  asstatus  = [ asstatus,    0,   0,   0,   1,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5 ]
  asstatus  = [ asstatus,    0,   0,   0,   1,   1,   1,   2,   2,   3,   3,   4,   4,   5,   5 ]
  asstatus  = [ asstatus,    3,   3,   3,   3,   3,   3,   3,   3,   3,   3 ]

  fw1name   = [            'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL' ]
  fw1name   = [ fw1name,   'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL' ]
  fw1name   = [ fw1name,   'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL' ]
  fw1name   = [ fw1name,   'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_ZR', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL' ]
  fw1name   = [ fw1name,   'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL', 'THIN_AL' ]

  fw1status = [            2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1 ]
  fw1status = [ fw1status, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1 ]
  fw1status = [ fw1status, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1 ]
  fw1status = [ fw1status, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1 ]
  fw1status = [ fw1status, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]

  fw2name   = [            'THIN_ZR', 'OPEN',    'OPEN',    'THIN_ZR', 'OPEN',    'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN'    ]
  fw2name   = [ fw2name,   'THIN_ZR', 'OPEN',    'OPEN',    'THIN_ZR', 'OPEN',    'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN'    ]
  fw2name   = [ fw2name,   'THIN_ZR', 'OPEN',    'OPEN',    'THIN_ZR', 'OPEN',    'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN'    ]
  fw2name   = [ fw2name,   'THIN_ZR', 'OPEN',    'OPEN',    'THIN_ZR', 'OPEN',    'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN',    'THIN_AL', 'OPEN'    ]
  fw2name   = [ fw2name,   'OPEN',    'OPEN',    'OPEN',    'OPEN',    'OPEN',    'OPEN',    'OPEN',    'OPEN',    'OPEN',    'OPEN'    ] 

  fw2status = [            2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1 ]
  fw2status = [ fw2status, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1 ]
  fw2status = [ fw2status, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1 ]
  fw2status = [ fw2status, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1 ]
  fw2status = [ fw2status, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]

  SUV_CEB_MUX_T_CCD_1 = [                      -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65]
  SUV_CEB_MUX_T_CCD_1 = [ SUV_CEB_MUX_T_CCD_1, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65]
  SUV_CEB_MUX_T_CCD_1 = [ SUV_CEB_MUX_T_CCD_1, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65]
  SUV_CEB_MUX_T_CCD_1 = [ SUV_CEB_MUX_T_CCD_1, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65, -65]
  SUV_CEB_MUX_T_CCD_1 = [ SUV_CEB_MUX_T_CCD_1, -65, -65, -65, -65, -65, -68, -63, -70, -50, -65 ]

  CCDTEMP1 = SUV_CEB_MUX_T_CCD_1
  
  SUV_CEB_MUX_T_CCD_2 = [                      -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64]
  SUV_CEB_MUX_T_CCD_2 = [ SUV_CEB_MUX_T_CCD_2, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64]
  SUV_CEB_MUX_T_CCD_2 = [ SUV_CEB_MUX_T_CCD_2, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64]
  SUV_CEB_MUX_T_CCD_2 = [ SUV_CEB_MUX_T_CCD_2, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64, -64]
  SUV_CEB_MUX_T_CCD_2 = [ SUV_CEB_MUX_T_CCD_2, -64, -64, -64, -64, -64, -67, -62, -69, -49, -64 ]

  CCDTEMP2 = SUV_CEB_MUX_T_CCD_2

  SUV_CEB_RDOUT_PORT_ISP = [                         2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ]
  SUV_CEB_RDOUT_PORT_ISP = [ SUV_CEB_RDOUT_PORT_ISP, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
  SUV_CEB_RDOUT_PORT_ISP = [ SUV_CEB_RDOUT_PORT_ISP, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ]
  SUV_CEB_RDOUT_PORT_ISP = [ SUV_CEB_RDOUT_PORT_ISP, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
  SUV_CEB_RDOUT_PORT_ISP = [ SUV_CEB_RDOUT_PORT_ISP, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ]

  SUV_CEB_RDOUT_PORT = SUV_CEB_RDOUT_PORT_ISP

  YAW_FLIP_ENABLED = [                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
  YAW_FLIP_ENABLED = [ YAW_FLIP_ENABLED, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
  YAW_FLIP_ENABLED = [ YAW_FLIP_ENABLED, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
  YAW_FLIP_ENABLED = [ YAW_FLIP_ENABLED, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
  YAW_FLIP_ENABLED = [ YAW_FLIP_ENABLED, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]

  YAW_FLIP = YAW_FLIP_ENABLED

; create_struct, new, 'name',['tag1','tag2','tag3'], 'D(2),F,A(1)'
  suvi_case_struct = { asname:asname, aspos:aspos, asstatus:asstatus, fw1name:fw1name, fw1status:fw1status, fw2name:fw2name, fw2status:fw2status, $
                       SUV_CEB_MUX_T_CCD_1:SUV_CEB_MUX_T_CCD_1, CCDTEMP1:CCDTEMP1, SUV_CEB_MUX_T_CCD_2:SUV_CEB_MUX_T_CCD_2, CCDTEMP2:CCDTEMP2, $
                       SUV_CEB_RDOUT_PORT:SUV_CEB_RDOUT_PORT, YAW_FLIP:YAW_FLIP }

; tab = string(asname,                 '(i3.3)')  + '  ' + $
;       string(aspos,                  '(i2.2)')  + '  ' + $
;       string(asstatus,               '(i1.1)')  + '  ' + $
;       string(fw1name,                '(a7)')    + '  ' + $
;       string(fw1status,              '(i1.1)')  + '  ' + $
;       string(fw2name,                '(a7)')    + '  ' + $
;       string(fw2status,              '(i1.1)')  + '  ' + $
;       string(SUV_CEB_MUX_T_CCD_1,    '(f4.0)')  + '  ' + $
;       string(SUV_CEB_RDOUT_PORT,     '(i1.1)')  + '  ' + $
;       string(YAW_FLIP,               '(i1.1)')

  tab = string(case_num+1,             '(f8.0)')  + '  ' + $
        string(asname,                 '(f6.0)')  + '  ' + $
        string(aspos,                  '(f5.0)')  + '  ' + $
        string(asstatus,               '(f8.0)')  + '  ' + $
        string(fw1name,                '(a7)')    + '  ' + $
        string(fw1status,              '(f9.0)')  + '  ' + $
        string(fw2name,                '(a7)')    + '  ' + $
        string(fw2status,              '(f9.0)')  + '  ' + $
        string(SUV_CEB_MUX_T_CCD_1,    '(f19.0)') + '  ' + $
        string(SUV_CEB_RDOUT_PORT,     '(f22.0)') + '  ' + $
        string(YAW_FLIP,               '(f16.0)')

  tab = [strupcase('case_num  asname  aspos  asstatus  fw1name  fw1status  fw2name  fw2status' + $
                   'SUV_CEB_MUX_T_CCD_1  SUV_CEB_RDOUT_PORT  YAW_FLIP'), tab]
  
  if keyword_set(do_print) then begin
     print, ''
     for i=0, n_elements(tab)-1 do print, tab[i]
     print, ''
  endif

  if exist(index) then begin

; Select only science images (e.g., remove darks):
     ss_dark  = where(strlowcase(index.frametype) eq 'dark',  n_dark)
     ss_light = where(strlowcase(index.frametype) eq 'light', n_light)
     if n_light gt 0 then index_light = index[ss_light]
     n_case_as_run = n_elements(index_light)
     case_num_as_run = lindgen(n_case_as_run)
     asname_as_run = index_light.asname
     aspos_as_run = index_light.aspos
     asstatus_as_run = index_light.asstatus
     fw1name_as_run = index_light.fw1name
     fw1status_as_run = index_light.fw1status
     fw2name_as_run = index_light.fw2name
     fw2status_as_run = index_light.fw2status
     suv_ceb_mux_t_ccd_1_as_run = index_light.suv_ceb_mux_t_ccd_1
     suv_ceb_rdout_port_as_run = index_light.suv_ceb_rdout_port
     yaw_flip_as_run = index_light.yaw_flip

     tab_as_run = string(case_num_as_run+1,             '(f8.0)')  + '  ' + $
                  string(asname_as_run,                 '(a6)')    + '  ' + $
                  string(aspos_as_run,                  '(f5.0)')  + '  ' + $
                  string(asstatus_as_run,               '(f8.0)')  + '  ' + $
                  string(fw1name_as_run,                '(a7)')    + '  ' + $
                  string(fw1status_as_run,              '(f9.0)')  + '  ' + $
                  string(fw2name_as_run,                '(a7)')    + '  ' + $
                  string(fw2status_as_run,              '(f9.0)')  + '  ' + $
                  string(suv_ceb_mux_t_ccd_1_as_run,    '(f19.0)') + '  ' + $
                  string(suv_ceb_rdout_port_as_run,     '(f22.0)') + '  ' + $
                  string(yaw_flip_as_run,               '(f16.0)')
     
     tab_as_run = [strupcase('case_num  asname  aspos  asstatus  fw1name  fw1status  fw2name  fw2status  SUV_CEB_MUX_T_CCD_1  SUV_CEB_RDOUT_PORT  YAW_FLIP'), tab_as_run]

     if keyword_set(do_print) then begin
        print, ''
STOP
        for i=0, n_elements(tab_as_run)-1 do print, tab_as_run[i]
        print, ''
     endif
  endif
     
  return, suvi_case_struct
  
end
