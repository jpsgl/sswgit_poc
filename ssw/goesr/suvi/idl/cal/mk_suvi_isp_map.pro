
function mk_suvi_isp_map

;destroy, isp_map

if get_logenv('SUVI_EXTERNAL_DATA') eq '' then $
  set_logenv, 'SUVI_EXTERNAL_DATA', './'
infil=concat_dir('SUVI_EXTERNAL_DATA','isp_TandCrevD.txt')

dat=rd_tfile(infil,6,1)
dat=dat(1:*,*)

dum={pkt_map, name:'',stbyte:0,stbit:0,nbits:0,type:''}

hdr=dat(*,0:8)
dat=dat(*,9:*)
len=n_elements(dat(0,*))
head=replicate({pkt_map},9)
tlm=replicate({pkt_map},len)

head.name=reform(hdr(0,*))
head.stbyte=reform(fix(hdr(1,*))-20)
head.stbit=reform(fix(hdr(2,*)))
head.nbits=reform(fix(hdr(3,*)))
head.type=reform(hdr(4,*))

tlm.name=reform(dat(0,*))
tlm.stbyte=reform(fix(dat(1,*))-44)
tlm.stbit=reform(fix(dat(2,*)))
tlm.nbits=reform(fix(dat(3,*)))
tlm.type=reform(dat(4,*))
;tlm=add_tag(tlm,head,'head')
;tlm.head=head

map=create_struct('tlm',tlm,'head',head)

isp_map=map
return, map

end

