;+
pro RotationOrientationCorrection, iindex, idata, oindex, odata

;NAME:   RotationOrientationCorrection
;PURPOSE:
;        Rotate image 90 deg counter-clockwise (CCW) to account for mechanical mounting of SUVI CCD 
;        when yaw flip is not enabled; otherwise rotate image 90 deg clockwise (270 deg CCW) when 
;        yaw flip enabled 
;CALLING SEQUENCE:
;        SUVI_IMG = RotationOrientationCorrection(SUVI_IMG)
;INPUTS:
;        SUVI_IMG - SUVI image 
;OUTPUTS:
;        SUVI_IMG_ORIENTATION_CORRECTED - SUVI image corrected for mechanical mounting of SUVI CCD
;KEYWORDS:
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;                   RWN  May 11, 2011
;        FEB. 2012  ABK, Update to correlate to SUVI CDRL80
;-

t0=systime(1)
print, '--> 7. Starting RotationOrientationCorrection.pro (SUVI GPA Routine #7)'

oindex = iindex
odata  = idata

; NOTA BENA: Logic assumes the following:
;   YAW_FLIP_ENABLED = 0 ==> S/C NOT in yaw flip mode
;   YAW_FLIP_ENABLED = 1 ==> S/C IS  in yaw flip mode

YAW_FLIP_ENABLED = iindex.yaw_flip
print, '     Yaw flip status: ', YAW_FLIP_ENABLED

; If yaw flipped, then rotate image 90 deg CCW, else rotate 90 deg CW:
if (YAW_FLIP_ENABLED eq 0) then begin
  odata = rotate(idata, 1)
  print, 'SUVI image rotated 90 degrees counterclockwise (270 degrees clockwise).'
endif else begin
  odata = rotate(idata, 3)
  print, 'SUVI image rotated 90 degrees clockwise.'	
end    

t1=systime(1)
print, 'RotationOrientationCorrection took: ', t1-t0, ' seconds'

END
