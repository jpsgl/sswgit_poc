
pro aia2suvi, file_sdo=file_sdo, t0=t0, wave_suvi=wave_suvi, raw=raw, $
              do_disp=do_disp, do_map=do_map, $
              naxis1_suvi=naxis1_suvi, naxis2_suvi=naxis2_suvi, $
              xcen_suvi=xcen_suvi, ycen_suvi=ycen_suvi, crota2_suvi=crota2_suvi, $
              missing=missing, pivot=pivot, $
              suvi_index_template=suvi_index_template, $
              index_sdo=index_sdo, data_sdo=data_sdo, $
              index_suvi=index_suvi, data_suvi=data_suvi, map_suvi=map_suvi, $
              do_jpeg=do_jpeg, do_write=do_write, _extra=_extra

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     AIA2SUVI
; CATEGORY:
;     Synthetic data generation
; PURPOSE:
;     Generate synthetic SUVI FITS image and header from specified AIA FITS file.
;     Optionally write out FITS file.
; CALLS:
;     aia2suvi, file_sdo=file_sdo, index_suvi=index_suvi, data_suvi=data_suvi
; METHOD:
;     
; INPUTS:
;     file_sdo - AIA FITS file reference
;     wave_suvi - Alternatively, enter a legitimate suvi wavelength
;                 (304, 171, 131, 195, 94, 284) and the appropriate AIA matching
;                 wavelength will be searched for. 
;     xcen_suvi - Desired xcen for generated SUVI synthetic image and header
;                 (default is xcen for input AIA image).
;     ycen_suvi - Desired ycen for generated SUVI synthetic image and header
;                 (default is ycen for input AIA image).
;     crota_suvi - Desired CROTA2 (roll) for generated SUVI synthetic image and header
;                  (default is CROTA2 for input AIA image).
;     do_write - If set then write synthetic SUVI image and header to FITS file.
;     do_map - if set then create a map object for the suvi image and header and
;              call plot_map to display the image.
; OUTPUTS:
;     
; KEYWORD INPUTS:
;     
; KEYWORD OUTPUTS:
;     index_suvi
;     data_suvi
;     map_suvi
; DEVELOPMENT STATUS:
;     2015-05-21 - Beta written and tested.
; TODO:
;     
; TEST AND VERIFICATION DESCRIPTION:
;     Test matrix of waves, spatial offsets, rolls, position errors, roll errors,
;     and scale errors.
; TESTING STATUS:
;     2015-06-11 - Tested to produce images with a varierty of AIA filters, spatial shifts,
;                  rolls, and with header errors in position and scale.
; PERCENT COMPLETE ESTIMATE:
;     95%
; MAN DAYS REMAINING ESTIMATE:
;     1.0 man days
; CONTACT:
;     Gregory L Slater (GLS) slater@lmsal.com
; MODIFICATION HISTORY:
;     progver = 'ver_20150506a' ; GLS - Written.
;     progver = 'ver_20150615a' ; GLS - Added documentation format.
; VERSION CONTROL STATUS:
;     2015-05-21 - Last commit
;-
; ==============================================================================

wave_arr_sdo  = [4500, 1600, 1700, 304, 335, 171, 131, 211, 193,  94,  94]
wave_arr_suvi = [   0,    0,    0, 304,   0, 171, 131,   0, 195,  94, 284]

if not exist(naxis1_suvi) then begin
  if keyword_set(raw) then naxis1_suvi = 1330 else naxis1_suvi = 1280
endif
if not exist(naxis2_suvi) then begin
  if keyword_set(raw) then naxis2_suvi = 1292 else naxis2_suvi = 1280
endif 
if not exist(cdelt1_suvi) then cdelt1_suvi = 2.5
if not exist(cdelt2_suvi) then cdelt2_suvi = 2.5
if not exist(crota2_suvi) then crota2_suvi = 0 ; 5
if not exist(xcen_suvi) then xcen_suvi = 0 ; -50
if not exist(ycen_suvi) then ycen_suvi = 0 ;  25

if not exist(missing) then missing = 0
if not exist(pivot) then pivot = 0

if not exist(wait_disp) then wait_disp = 0.2

if not exist(dir_suvi_syn) then $
   dir_suvi_syn = '/archive1/suvi/suvi_data/fm1/syn_data'

if not exist(index_sdo) or not exist(data_sdo) then begin
  if not exist(file_sdo) then begin
    if not exist(wave_sdo) then begin
      if exist(wave_suvi) then begin
        ss_wave_match = where(wave_suvi eq wave_arr_suvi, n_match)
        wave_sdo = (wave_arr_sdo[ss_wave_match])[0]
      endif else begin
        wave_sdo = 171
      endelse
    endif
    file_sdo = last_file(t0, wave=wave_sdo)
  endif
  read_sdo, file_sdo, index_sdo, data_sdo, uncomp_delete=uncomp_delete, $
            use_index=use_index, use_shared=use_shared, noshell=noshell
endif

; Not finished. Should use sdo wave arr to match index with wave_suvi,
;   fw1name_suvi, fw2name_suvi, fw1pos_suvi, fw2pos_suvi, etc. :
if not exist(wave_suvi) then begin
  ss_wave_match = where(index_sdo.wavelnth eq wave_arr_sdo, n_match)
  wave_suvi = (wave_arr_suvi[ss_wave_match])[0]
endif
img_pkt_time_suvi = index_sdo.date_obs
asname_suvi = strtrim(wave_suvi,2) + 'A'
;fw1name_suvi = []

;index_suvi = { naxis: 2, naxis1: naxis1_suvi, naxis2: naxis2_suvi, $
index_suvi = { img_pkt_time: img_pkt_time_suvi, sht_exp_time: 0.0, cmd_exp_time: 0.0, $
               imagetype: '', frametype: '', $
               fw1pos: 0, fw1status: 0, fw1name: '', fw2pos: 0, fw2status: 0, fw2name: '', $
               aspos: 0, asstatus: 0, asname: asname_suvi, rdoutport: '', $
               gtyerr: 0.0, gtzerr: 0.0, $
               yaw_flip: 0, ccdtemp1: 0.0, ccdtemp2: 0.0, time_last_bakeout: '', $
               ampname: '', exptype: '', stcol: 0, encol: 0, strow: 0, enrow: 0, summ: '' }

tags_suvi_from_sdo = ['naxis','naxis1','naxis2','date_obs','t_obs','wave_sdo', $
		      'xcen','ycen','crpix1','crpix2','crval1','crval2','cdelt1','cdelt2', $
		      'exptime']

;index_suvi_from_sdo = $
;   str_subset(index_sdo, tags_suvi_from_sdo, include=include, exclude=exclude, $
;              regex=regex, version=version, quiet=quiet, status=status)

; This fails - ask sam why :
;index_suvi = merge_struct(index_suvi, index_suvi_from_sdo)

; ...so add the tags one by one:
index_suvi = add_tag(index_suvi, index_sdo.date_obs, 'date_obs') ; Add this by merge_struct
index_suvi = add_tag(index_suvi, index_sdo.t_obs, 't_obs')       ; Add this by merge_struct
index_suvi = add_tag(index_suvi, 2l, 'naxis')
index_suvi = add_tag(index_suvi, naxis1_suvi, 'naxis1')
index_suvi = add_tag(index_suvi, naxis2_suvi, 'naxis2')
index_suvi = add_tag(index_suvi, cdelt1_suvi, 'cdelt1')
index_suvi = add_tag(index_suvi, cdelt2_suvi, 'cdelt2')

crpix1_suvi = (naxis1_suvi+1.d0)/2.d0 - xcen_suvi/cdelt1_suvi
crpix2_suvi = (naxis2_suvi+1.d0)/2.d0 - ycen_suvi/cdelt2_suvi
crval1_suvi = $
  comp_fits_crval(xcen_suvi, cdelt1_suvi, naxis1_suvi, crpix1_suvi)
crval2_suvi = $
  comp_fits_crval(ycen_suvi, cdelt2_suvi, naxis2_suvi, crpix2_suvi)

index_suvi = add_tag(index_suvi, crpix1_suvi, 'crpix1')
index_suvi = add_tag(index_suvi, crpix2_suvi, 'crpix2')
index_suvi = add_tag(index_suvi, crval1_suvi, 'crval1')
index_suvi = add_tag(index_suvi, crval2_suvi, 'crval2')
index_suvi = add_tag(index_suvi, crota2_suvi, 'crota2')

index_suvi = add_tag(index_suvi, wave_suvi, 'wavelnth')

rot_ang = crota2_suvi
fov_sdo  = index_sdo.cdelt1*index_sdo.naxis1
fov_suvi = index_suvi.cdelt1*index_suvi.naxis1
rot_mag = fov_sdo/fov_suvi

sin_rot_ang = sin(crota2_suvi/!radeg)
cos_rot_ang = cos(crota2_suvi/!radeg)

rot_x0 = index_sdo.crpix1 + ( xcen_suvi*cos_rot_ang - ycen_suvi*sin_rot_ang)/index_sdo.cdelt1
rot_y0 = index_sdo.crpix2 + ( xcen_suvi*sin_rot_ang + ycen_suvi*cos_rot_ang)/index_sdo.cdelt2

;Result = ROT( A, Angle, [Mag, X0, Y0] [, /INTERP] [, CUBIC=value{-1 to 0}] $
;              [, MISSING=value] [, /PIVOT] )
data_suvi = congrid(rot(data_sdo, rot_ang, rot_mag, rot_x0, rot_y0, $
                    interp=interp, cubic=cubic, missing=missing, pivot=pivot), $
                    1280, 1280, interp=interp)

; QUESTION - Should the RDOUTPOT reflextion be done before or after adding strips?

; Adjust the synthetic image for rdoutport and yaw_flip settings:
if exist(suvi_index_template) then begin
   if ((suvi_index_template.rdoutport eq 1) and (suvi_index_template.yaw_flip eq 0)) then $
      data_suvi = rotate(data_suvi,6) ; [1,0], 180 cc,    transpose
   if ((suvi_index_template.rdoutport eq 1) and (suvi_index_template.yaw_flip eq 1)) then $
      data_suvi = rotate(data_suvi,4) ; [1,1],   0 cc,    transpose
   if ((suvi_index_template.rdoutport eq 2) and (suvi_index_template.yaw_flip eq 0)) then $
      data_suvi = rotate(data_suvi,3) ; [2,0], 270 cc, no transpose
   if ((suvi_index_template.rdoutport eq 2) and (suvi_index_template.yaw_flip eq 1)) then $
      data_suvi = rotate(data_suvi,1) ; [2,1],  90 cc, no transpose
endif
   
; If sunthetic SUVI image is to be raw, then add strips approriately:
if keyword_set(raw) then begin
   help, data_suvi
   buff = data_suvi
   data_suvi = fltarr(naxis1_suvi, naxis2_suvi)
   data_suvi[30:(30+1279), 4:(4+1279)] = buff
endif

;ssw_register, index_sdo, data_sdo, index_suvi_reg, data_suvi, ref_index=index_suvi, $
;              ref_map=ref_map, derotate=derotate, drotate=drotate, $
;              clobber=clobber, correl=correl, roll=roll, $
;              offsets=offsets
;index_suvi = index_suvi_reg

if keyword_set(do_map) then begin
   wdef,0,naxis1_suvi/2,naxis2_suvi/2
   index2map, index_suvi, data_suvi, map_suvi
   plot_map, map_suvi, grid=15, /log
endif

if keyword_set(do_disp) then begin
   wdef,0,naxis1_suvi/2,naxis2_suvi/2
   tvscl, safe_log10(rebin(data_suvi, naxis1_suvi/2, naxis2_suvi/2, /samp))
   xyouts, 0.1, 0.1, /norm, strtrim(suvi_index_template.rdoutport,2) + '  ' + $
                            strtrim(suvi_index_template.yaw_flip, 2) 
   wait, wait_disp
endif

if keyword_set(do_jpeg) then begin
   datap_unscl = datap
   datap_scaled = $
      aia_intscale(datap, exptime=indexp.exptime, wavelnth=fix(wave), /bytescale)
   tvlct, r, g, b, /get

   filnam_suvi_jpeg = 'suvi_synthetic_' + string(wave_suvi, '$(i4.4)') + '_' + $
                 time2file(index_suvi.date_obs) + '.jpeg'
   write_jpeg, concat_dir(concat_dir(dir_suvi_syn, 'jpeg'), filnam_suvi_jpeg), $
               truecolor(datap_scaled, r, g, b), true=1, qual=90
endif

if keyword_set(do_fits) then begin
   filnam_suvi = 'suvi_synthetic_' + string(wave_suvi, '$(i4.4)') + '_' + $
                 time2file(index_suvi.date_obs) + '.fits'
   mwritefits, index_suvi, data_suvi, $
               outfile=concat_dir(concat_dir(dir_suvi_syn, 'fits'), filnam_suvi)
endif

end
