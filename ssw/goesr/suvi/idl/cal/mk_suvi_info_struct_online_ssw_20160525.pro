
function mk_suvi_info_struct, isp, img_pkt_time=img_pkt_time

;COMMON LOGGING, LOGID, QDEBUG
;COMMON EXTERNAL_DATA, SUVI_BAD_PIXELS, SUVI_BAD_CCD_COLUMNS, SUVI_FLAT, STATIC_PARAMS, MEASURED_PARAMS, EPHEMERIS, ENTRANCE_TRANS, FOCAL_PLANE_FILTER_TRANSMISSION, SUVI_LINEARITY, SUVI_GAIN_RIGHT, SUVI_GAIN_LEFT
;COMMON OUT_METADATA, DARK_NAMES, SUVI_CONTAM_THICKNESS, SUVI_TOT_IRRADIANCE, SUVI_EFFA, SUVI_LINEARITY_SOURCE, SUVI_GAIN_CONSTANT, PHOT_ELE_CONV, QE, PHOT_ENG_CONV, ROW_BIAS_CALC, COL_BIAS1_CALC, COL_BIAS2_CALC, SN_LEV3, FULL_WELL, SHT_EXP_TIME, IMG_PKT_TIME
;COMMON SUVI_GPDS, ISP2, ISP_MAP, INFO2

info={ img_pkt_time: '', sht_exp_time: 0.0, cmd_exp_time: 0.0, imagetype: '', frametype: '', $
		fw1pos: 0, fw1status: 0, fw1name: '', fw2pos: 0, fw2status: 0, fw2name: '', $
		aspos: 0, asstatus: 0, asname: '', rdoutport: 0, gtyerr: 0.0, gtzerr: 0.0, $
		yaw_flip: 0, ccdtemp1: 0.0, ccdtemp2: 0.0, time_last_bakeout: '', $
		ampname: '', exptype: '', stcol: 0, encol: 0, strow: 0, enrow: 0, summ: '' }

case isp.head.suv_isp_hdr_img_type of
	10: info.imagetype='SEQUENCE'
	11: info.imagetype='CALIBRATION'
	12: info.imagetype='TAKEPIC'
	else: info.imagetype='???'
endcase
	
case isp.mnem.suv_ca_frm_type of
	0: info.frametype='LAST'
	1: info.frametype='LIGHT'
	2: info.frametype='DARK'
	3: info.frametype='LED'
	4: info.frametype='LTC'
	5: info.frametype='SPAT'
	6: info.frametype='VPAT'
	7: info.frametype='RCLK'
	else: info.frametype='???'
endcase

info.fw1pos=isp.mnem.suv_fw1_enc_bits
case 1 of
	(isp.mnem.suv_fw1_enc_bits gt 71) and (isp.mnem.suv_fw1_enc_bits lt 80): begin
			info.fw1status=0
			info.fw1name='OPEN'
	end
	(isp.mnem.suv_fw1_enc_bits gt 134) and (isp.mnem.suv_fw1_enc_bits lt 143): begin
			info.fw1status=1
			info.fw1name='THIN_AL'
	end
	(isp.mnem.suv_fw1_enc_bits gt 200) and (isp.mnem.suv_fw1_enc_bits le 205): begin
			info.fw1status=2
			info.fw1name='THIN_ZR'
	end
	(isp.mnem.suv_fw1_enc_bits gt 264) and (isp.mnem.suv_fw1_enc_bits lt 273): begin
			info.fw1status=3
			info.fw1name='THICK_ZR'
	end
	(isp.mnem.suv_fw1_enc_bits gt 8) and (isp.mnem.suv_fw1_enc_bits lt 17): begin
			info.fw1status=4
			info.fw1name='THICK_AL'
	end
	else: begin
		info.fw1status=-1
		info.fw1name='???'
	end
endcase

info.fw2pos=isp.mnem.suv_fw2_enc_bits
case 1 of
	(isp.mnem.suv_fw2_enc_bits gt 71) and (isp.mnem.suv_fw2_enc_bits lt 80): begin
			info.fw2status=0
			info.fw2name='GLAS'
	end
	(isp.mnem.suv_fw2_enc_bits gt 134) and (isp.mnem.suv_fw2_enc_bits lt 143): begin
			info.fw2status=1
			info.fw2name='OPEN'
	end
	(isp.mnem.suv_fw2_enc_bits gt 200) and (isp.mnem.suv_fw2_enc_bits le 205): begin
			info.fw2status=2
			info.fw2name='THIN_AL'
	end
	(isp.mnem.suv_fw2_enc_bits gt 264) and (isp.mnem.suv_fw2_enc_bits lt 273): begin
			info.fw2status=3
			info.fw2name='THIN_ZR'
	end
	(isp.mnem.suv_fw2_enc_bits gt 8) and (isp.mnem.suv_fw2_enc_bits lt 17): begin
			info.fw2status=4
			info.fw2name='THICK_AL'
	end
else: begin
		info.fw2status=-1
		info.fw2name='???'
	end
endcase

info.aspos=isp.mnem.suv_as_pos
case 1 of 
	 (isp.mnem.suv_as_pos ge  4) and (isp.mnem.suv_as_pos le  9)  : begin
		info.asstatus=0
		info.asname='131A'
	end
	 (isp.mnem.suv_as_pos ge 10) and (isp.mnem.suv_as_pos le 15)  : begin
		info.asstatus=1
		info.asname='171A'
	end
	 (isp.mnem.suv_as_pos ge 16) and (isp.mnem.suv_as_pos le 21)  : begin
		info.asstatus=2
		info.asname='195A'
	end
	 (isp.mnem.suv_as_pos ge 22) and (isp.mnem.suv_as_pos le 27)  : begin
		info.asstatus=3
		info.asname='284A'
	end
	 (isp.mnem.suv_as_pos ge 28) and (isp.mnem.suv_as_pos le 33)  : begin
		info.asstatus=4
		info.asname='304A'
	end
;	((isp.mnem.suv_as_pos ge 34) and (isp.mnem.suv_as_pos le 36) or $
;        (isp.mnem.suv_as_pos ge  1) and (isp.mnem.suv_as_pos le  3)) : begin
         (isp.mnem.suv_as_pos ge 34) and (isp.mnem.suv_as_pos le 36)  : begin
		info.asstatus=5
		info.asname='94A'
	end
         (isp.mnem.suv_as_pos ge  1) and (isp.mnem.suv_as_pos le  3)  : begin
		info.asstatus=5
		info.asname='94A'
	end
	else: begin
		info.asstatus=-1
		info.asname='???'
	end
endcase

;PRINT, 'isp.mnem.suv_as_enc_bits = ' + strtrim(isp.mnem.suv_as_enc_bits,2)
;HELP,   isp.mnem.suv_as_pos

case isp.mnem.suv_ceb_rdout_port of
  1: begin
       info.rdoutport = 1
       ampname = 'LEFT'
     end
  2: begin
       info.rdoutport = 2
       ampname = 'RIGHT'
     end
  else: begin
       info.rdoutport = isp.mnem.suv_ceb_rdout_port
       ampname = '????'
     end
endcase

case isp.mnem.suv_ca_frm_summd of
	0: info.summ='DEFAULT'
	1: info.summ='1x1'
	2: info.summ='2x2'
	3: info.summ='4x4'
	4: info.summ='8x8'
	else: info.summ='???'
endcase

info.strow=isp.mnem.suv_ca_frm_srow
info.enrow=isp.mnem.suv_ca_frm_erow
info.stcol=isp.mnem.suv_ca_frm_scol
info.encol=isp.mnem.suv_ca_frm_ecol

sht_bc_time=(isp.mnem.suv_sht_bc_close_time-isp.mnem.suv_sht_bc_open_time)/1000000.
sht_bot_time=(isp.mnem.suv_sht_bot_close_tim-isp.mnem.suv_sht_bot_open_time)/1000000.
sht_tc_time=(isp.mnem.suv_sht_tc_close_time-isp.mnem.suv_sht_tc_open_time)/1000000.
sht_top_time=(isp.mnem.suv_sht_top_close_tim-isp.mnem.suv_sht_top_open_time)/1000000.
sht_exp_time=median([sht_tc_time, sht_top_time, sht_bc_time, sht_bot_time])

if (sht_exp_time le 0.072) then sht_exp_time=(sht_exp_time*7.0/20.0)*isp.mnem.suv_sht_n_nr_exp
;isp.expDuration=sht_exp_time
info.sht_exp_time=sht_exp_time
info.cmd_exp_time=isp.mnem.suv_ca_frm_exp*0.001

if (info.cmd_exp_time lt 1) then info.exptype='SHORT' else info.exptype='LONG'


info.gtyerr=-15.0124+0.0073853*isp.mnem.suv_gt_yerr_p+2050
info.gtzerr=-15.0124+0.0073853*isp.mnem.suv_gt_zerr_p+2050

;isp.head.suv_isp_hdr_isn
;isp.head.suv_isp_hdr_flg
;isp.head.suv_isp_hdr_img_type
;isp.head.suv_isp_hdr_pkt_num
;isp.head.suv_isp_hdr_data_sz32

info.img_pkt_time=img_pkt_time

info.yaw_flip=0

;isp.stpix=i32hdr[4]
;isp.enpix=i32hdr[5]
;isp.nrows=i16hdr[11]
;isp.ncols=i16hdr[10]
;

info.ccdtemp1=(isp.mnem.suv_ceb_mux_t_ccd_1-1024.0)*0.946
info.ccdtemp2=(isp.mnem.suv_ceb_mux_t_ccd_2-1024.0)*0.946
;---below for test only
;info.ccdtemp1=info.ccdtemp1-79
;---above for test only

bakeout=isp.mnem.suv_ip_image_flags and '00000020'x
bakeout=bakeout/32
if (bakeout eq 1) then info.time_last_bakeout=img_pkt_time
info2=info
return, info
end
