
pro mk_suvi_tgrid, t0, t1, n_cycles=n_cycles

if not exist(waves_suvi) then waves_suvi = [094, 195, 304, 131, 171, 284]
n_waves = n_elements(waves_suvi)
if not exist(t_diff_sec_max) then t_diff_sec_max = 2
if not exist(dir_suvi_syn) then $
   dir_suvi_syn = '/archive1/suvi/suvi_data/fm1/syn_data'

wave_series_sdo_arr  = [094, 193, 304, 131, 193, 094, 171, 193, 331, 304, 193]
wave_series_suvi_arr = [094, 195, 304, 131, 195, 094, 171, 195, 284, 304, 195]
t_off_sec_cum_arr    = [020, 030, 050, 070, 100, 120, 150, 170, 180, 200, 220]

n_frame_cycle = n_elements(t_off_sec_cum_arr)
t_off_sec__arr = t_off_sec_cum_arr[1:*] - t_off_sec_cum_arr
t_cycle_sec = t_off_sec_cum_arr[n_frame_cycle-1]

t0_sec = anytim(t0)
if not exist(t1) then begin
   if not exist(n_cycles) then n_cycles = 5
   t_interval_sec = t_cycle_sec*(n_cycles+1)
   t1_sec = t0_sec + t_interval_sec ; Add one cycle for padding in jsoc query call
   t1 = anytim(t1_sec, /ccsds)
endif else begin
   t_interval_sec = anytim(t1) - anytim(t0)
   n_cycles = interval_sec / t_cycle_sec
endelse

for i=0, n_waves-1 do begin
   wave_suvi0 = waves_suvi[i]
   wave_sdo0 = waves_sdo[i]
   ss_wave0 = where(wave_series_suvi_arr eq wave0, n_wave0)
   t_off_sec_wave0 = t_off_sec_cum_arr[ss_wave0]
   if n_cycles gt 1 then do begin
      for j=1, n_cycles-1 do $
         t_off_sec_wave0 = [t_off_sec_wave0, t_off_sec_cum_arr[ss_wave0]]
   endif
   t_sec_wave0 = t0_sec + t_off_sec_wave0
   t_wave0 = anytim(t_sec_wave0, /ccsds)
   t_wave_ex0 = anytim(t_wave0, /ext)
   n_sample0 = n_elements(t_wave0)

; Call jsoc query for all records for this wave during interval:
   ssw_jsoc_time2data, t0, t1, wave=wave_sdo0, index_sdo, file_sdo, /files_only, $
                       xquery=xquery
   t_sdo = index_sdo.date_obs
   t_sdo_ex = anytim(t_sdo, /ext)
; Loop through smaple times for this suvi wave to find closest SDO matches:
   for j=0, n_sample0-1 do begin
      ss_match = tim2dset(t_sdo_ex, t_wave_ex0[j])
      if j eq 0 then ss_match_wave0 = ss_match else $
                     ss_match_wave0 = [ss_match_wave0, ss_match]
   endfor

   t_sdo_wave0 = t_sdo[ss_match_wave0]
   file_sdo_wave0 = file_sdo[ss_match_wave0]
   t_sdo_sec_wave0 = anytim(t_sdo_wave0)
   t_diff_sec_wave0 = t_sdo_sec_wave0 - t_sec_wave0
   ss_bad = where(t_diff_sec_wave0 gt t_diff_sec_max, n_bad)
   if n_bad gt 0 then begin
      print, 'One or more SUVI times has no close SDO time.  Returning.'
      if keyword_set(qstop) then stop
      return
   endif

   for j=0, n_sample0-1 do begin   

;     read_sdo, file_sdo_wave0[j], index_sdo0, data_sdo0, $
;               uncomp_delete=uncomp_delete, use_index=use_index, $
;               use_shared=use_shared, noshell=noshell

;     aia_prep, index, data, indexp, datap, /use_hdr_pnt
;     datap_unscl = datap
;     datap_scaled = $
;        aia_intscale(datap, exptime=indexp.exptime, wavelnth=fix(wave), /bytescale)

;     aia_lct, r, g, b, wavelnth=fix(wave), /load

      aia2suvi, file_sdo=file_sdo, t0=t0, wave_suvi=wave_suvi, raw=raw, $
                naxis1_suvi=naxis1_suvi, naxis2_suvi=naxis2_suvi, $
                index_sdo=index_sdo, data_sdo=data_sdo, $
                index_suvi=index_suvi, data_suvi=data_suvi, $
                do_disp=0, _extra=_extra



