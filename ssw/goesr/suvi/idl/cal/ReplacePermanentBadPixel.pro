
pro ReplacePermanentBadPixel, iindex, idata, oindex, odata

;+
;NAME:   ReplacePermanentBadPixel
;PURPOSE:
;        Replace permanent bad pixels by mean of neighboring, non_bad pixels
;CALLING SEQUENCE:
;        SUVI_IMG = ReplacePermanentBadPixel(SUVI_IMG)
;INPUTS:
;        SUVI_IMG - SUVI image to have bad pixels replaced by mean of neighboring non_bad pixels
;OUTPUTS:
;        SUVI_IMG_BAD_PIX_CORRECTED - floating point SUVI image corrected by replacing bad pixels 
;PROCEDURE CALLS:    
;MODIFICATION HISTORY:
;        FEB. 2011, ABK  (Created)
;        APR. 2011  RWN  
;        FEB. 2012, NVN, modified so that it reflects the new philosophy of getting the new information
;                        on the locations of bad and good pixels from a lookup table.
;        FEB. 2012  ABK, Update to correlate to SUVI CDRL80
;-

t0 = systime(1)
print, '--> 3. Starting ReplacePermanentBadPixel.pro (SUVI GPA Routine #3)'

; SUVI_BAD_PIXELS is read from the most recent SUVI bad pixel file using read_bad_pixels.pro
; According to CDRL 80 (4.3), a lookup table should be used that contains a list of the locations of 
; bad pixels and those of good pixels whose means are used to replace the values of bad pixels.
; The whole information is stored in a structure called SUVI_BAD_PIXELS in common block EXTERNAL_DATA

oindex = iindex
odata  = idata

SUVI_BAD_PIXELS = read_bad_pixels()
NBAD = SUVI_BAD_PIXELS.NBAD
print, '   Number of permanent bad pixels: ', NBAD

if nbad gt 0 then begin
  bp_tags = tag_names(SUVI_BAD_PIXELS)

  for i=0,NBAD-1 do begin
; First item in bp is the file name, second the number of bad pixels
    command = 'badstr = SUVI_BAD_PIXELS.' + bp_tags[i+2]
    result = execute(command)

    if result ne 1 then begin
      print, ' Problem interpretting bad pixel structure variable.  Returning.'
      return
    endif

    bad_x=badstr.bad_x
    bad_y=badstr.bad_y
    ngood=badstr.ngood
    replacement_val = average(idata[badstr.good_x, badstr.good_y])
    odata[bad_x,bad_y] = replacement_val

    print, 'Pixel value of ' + strtrim(idata[bad_x,bad_y],2) + $
           ' at (' + strtrim(bad_x,2)  + ',' + strtrim(bad_y,2) + $
           ') is replaced with ' + strtrim(replacement_val,2) + $
           ', which is the   average of ' + strtrim(ngood,2) + ' pixels.'

  endfor
endif
  
t1 = systime(1)
print, 'ReplacePermanentBadPixel took: ', t1-t0, ' seconds'

END
