
;+
function read_bad_pixels

;	18-Apr-2013	DSS	Added description header
;
;	Purpose:	Reads a bad pixels file and returns a bad pixel structure
;
;
;	bp={file: infile, nbad:  0, bad0001: badstr}
;
;	badstr =  {ngood:  ngood, $ ; # of good pixels used to replace this bad pixel
;		bad_x:   0, $ ; X coordinate of the bad pixel
;		bad_y:   0, $ ; y coordinate of the bad pixel
;		good_x:  0, $ ; x coordinates of the good pixels
;		good_y:  0}   ; y coordinates of the good pixels
;-

file_bad_pix = file_list(concat_dir(get_logenv('SUVI_CAL_DATA'), 'bad_pixels'), $
                         'suvi_bad_pixels_ver??.txt')
if file_bad_pix[0] eq '' then $
  STOP, 'Bad pixel file(s) not found.  Stopping.'
file_bad_pix = file_bad_pix[sort(file_bad_pix)]
file_bad_pix = file_bad_pix[n_elements(file_bad_pix)-1]

bp1 = rd_tfile(file_bad_pix)    ; read as a string per line
bp2 = rd_tfile(file_bad_pix, 3) ; read only the two entries, representing bad pixels
nbad=n_elements(bp1)
bnames='BAD'+string(lindgen(nbad)+1,format='(i4.4)')
for i=0,nbad-1 do begin
pcomma=strarr(100)
jstart=0
for j=0, 100 do begin
out=strpos(bp1[i],',',jstart)
if out eq -1 then goto, cont
pcomma[j]=out
jstart=out+1
endfor
cont:
ntcomma=j
;   print,'i = ',i,'   ntcomma = ',ntcomma
pcomma=pcomma[0:ntcomma-1]
npcomma=fix(pcomma)
cvalues=strarr(ntcomma-1)
for j=0, ntcomma-2 do begin
cvalues[j]=strmid(bp1[i], npcomma[j]+1, npcomma[j+1]-npcomma[j]-1)
endfor


; treat the first and last entries
bp22=bp2[2,i]
cvalue0=strmid(bp22,0,strlen(bp22)-1)
cvaluen=strmid(bp1[i],npcomma[ntcomma-1]+1,strlen(bp1[i])-npcomma[ntcomma-1])
cvalues=[cvalue0, cvalues, cvaluen] 
ng2=n_elements(cvalues)  ; should be 2*number of good pixels, confirm this is an even number
ngood=ng2/2
if ngood*2 ne ng2 then begin
print,'Odd number of comma separated values  --> something is wrong with the bad pixel file.'
return,-1
endif 
bp20=bp2[0,i]  ; bad pix X coord
bp21=bp2[1,i]  ; bad pix y coord
;   print,'ng2 = ',ng2,'   ngood = ',ngood
gx=cvalues[2*indgen(ngood)]
gy=cvalues[2*indgen(ngood)+1]
;   help,gx,gy

badstr =  {ngood:  ngood, $ ; # of good pixels used to replace this bad pixel
bad_x:   fix(bp20), $ ; X coordinate of the bad pixel
bad_y:   fix(bp21), $ ; y coordinate of the bad pixel
good_x:  fix(gx),   $ ; x coordinates of the good pixels
good_y:  fix(gy)} ; y coordinates of the good pixels
;   help,badstr,/str

if i eq 0 then bp={file: file_break(tinfil), $ ; name of the bad pixels file
nbad:  nbad, $ ; # of bad pixels in this structure
bad0001: badstr} else $
bp=add_tag(bp,badstr,bnames[i])
endfor

return, bp

end
