
pro rename_suvi_fits, do_write=do_write, verbose=verbose, qdebug=qdebug, _extra=_extra

; ==============================================================================
;+
; PROJECT:
;     SUVI
; ROUTINE NAME:
;     RENAME_SUVI_FITS
; MODIFICATION History:
; 2015-10-05 GLS - Updates to doc header.
;-
; ==============================================================================



dir_top = '/archive1/suvi/suvi_data/fm1/l0'
path_file = ssw_time2paths(anytim(!stime, /ccsds), parent=get_logenv('SUVI_L0'), /hourly)
mk_dir, path_file
outfil_l0 = 'suvi_fm1_l0_' + index.asname + '_' + $
            time2file(anytim(!stime, /ccsds)) + '.fits'
spawn, ['mv, 'old_file', file_new']


concat_dir(path_file, outfil_l0), data, suvi_fits_hdr
print, ' Wrote ' + concat_dir(path_file, outfil_l0)













  
if not exist(fm) then fm = 1

; Define SUVI environmentals and directory paths:
setup_suvi_env, fm=fm, _extra=_extra

if not exist(do_display) then do_display = 0

set_logenv, 'SUVI_TLM', '/titan/goes/suvi/GOES-16/packets/0x032a'
if ( keyword_set(interactive) or (not exist(pktfil)) ) then begin
  files = file_list(get_logenv('SUVI_TLM'), '*.0x032a')
  filnams = file_break(files)
  ss_fil = xmenu_sel(filnams)
  pktfil = files(ss_fil)

; Check for existence of index.sav file for this packet, and, if it exists,
; use it for image selection:
  pkt_index_file = pktfil + '.index.sav'
  if file_exist(pkt_index_file) then begin
    restore, pktfil + '.index.sav'
    tag_list = 'DATE_OBS,ASNAME,FRAMETYPE,FW1NAME,FW2NAME,SHT_EXP_TIME,RDOUTPORT,YAW_FLIP,CCDTEMP1'
    info = get_infox(index_packet, tag_list, /number)
    ss_img = xmenu_sel(info, /fixed)
  endif
endif

img_size_bytes = 3465216l
n_packets_per_img = long(img_size_bytes / (6+14+24+4074*2))
f_info = file_info(concat_dir(get_logenv('SUVI_TLM'), file_break(pktfil)))
siz_file = f_info.size
n_img_in_file = siz_file/img_size_bytes

if not exist(ss_img) then ss_img = lindgen(n_img_in_file) else $
  if ss_img[0] eq -1 then ss_img = lindgen(n_img_in_file)

n_img_jump = ss_img
n_imgs_to_process = n_elements(ss_img)

i_ss_img = 0
first_header = 1

; Calculate the starting packet number (record) if n_img_jump is passed:
if exist(n_img_jump) then recnum = long(n_img_jump[i_ss_img])*n_packets_per_img
   
; If t_ref does not exist, then set it to an arbitrary time earlier than any
;   time likely to be encountered in a SUVI packet file:
if not exist(t_ref) then t_ref = '01-jan-2000'
t_ref_sec = anytim(t_ref)

comments = [ $
  '-------------------------------------------------------------------------------', $
  ' Begin SUVI Ground Processing Development System Testing at ' + anytim(!stime, /ccsds), $
  '-------------------------------------------------------------------------------' ]
prstr, comments

; Call the setup routine:
;setup_suvi_gpa

qswap = is_member(/swap_os)

; Check for non-incremental image serial number:
ispisni = -1

; Open packet file for reading:
openr, lun, concat_dir(get_logenv('SUVI_TLM'), file_break(pktfil)), /get_lun

; Define image packet structure:
hdr_dwords =    6
data_words = 4074
data_bytes = data_words*2

 pkt_struct = {pkt,            prihdr:     BYTARR(6), sechdr:   BYTARR(14), $
               imghdr:  LONARR(hdr_dwords), imgdata:INTARR(data_words)}

;pkt_struct = {science_packet, ccsds_prime:BYTARR(6), ccsds_sec:BYTARR(14), $
;              data_head:LONARR(06), data:INTARR(data_words)}
 pkt_struct = {science_packet, ccsds_prime:bytarr(6), ccsds_sec:bytarr(14), $
               data_head:bytarr(24),           data:bytarr(data_bytes)}

; Error handler (especially for EOF errors):
;catch, error_status
;if error_status ne 0 then begin
;  print, 'MK_SUVI_FITS: error encountered. Returning'
;  free_lun, lun
;  return
;endif

; Associate packet structure with image packet file
record = assoc( lun, pkt_struct )

; Check and double-check that you aren't at the end of the packet file:
if (n_elements(recnum) eq 0) then recnum = 0l
if (recnum gt 0) then testrecnum = recnum-1 else testrecnum = 0

testrec = record(testrecnum)
if EOF(lun) then begin
  print, 'MK_SUVI_FITS: Already off the end. Returning.'
  free_lun, lun
  return
endif

testrec = record(recnum)
if EOF(lun) then begin
  print, 'MK_SUVI_FITS: Sitting at the last record. Returning.'
  free_lun, lun
  return
endif

; Initialize variables prior to reading packet file:
thisrecnum = recnum
packet     = record(thisrecnum)
pkt_num    = -1
newfile    = 0
writeimg   = 0
newisp     = 0

; Define integer pixel vector:
img_template = intarr(1722604)

; Read successive packets, construct images and headers, and write out FITS files:
while ( (not EOF(lun)) and (i_ss_img lt n_imgs_to_process) ) do begin 

;       thisrecnum = long(n_img_jump[i_ss_img])*n_packets_per_img
        packet = record(thisrecnum)
	tstart_pkt=systime(1)

; Extract image time and define various time parameters:
	julian_day_epoch = 2451545D ; This is 1200 UT on 2000-01-01 (which seems to give the right answer)
	apid   = ( ( packet.ccsds_prime[0] and 3 ) * 256 ) + packet.ccsds_prime[1]
	daynum = packet.ccsds_sec[0]*65536l + packet.ccsds_sec[1]*256l + packet.ccsds_sec[2]
	msec   = packet.ccsds_sec[3]*16777216l + packet.ccsds_sec[4]*65536l + $
                 packet.ccsds_sec[5]*256l + packet.ccsds_sec[6] 
	usec   = packet.ccsds_sec[7]*256l + packet.ccsds_sec[8] 
	
	jul_day = julian_day_epoch + double( daynum ) + double( msec ) / 86400000.0D
	CALDAT, jul_day, mn, dy, yr, hr, mi, sec
        anystring = string(  yr, mn, dy, hr, mi, sec, $
                             format = '(i4.4, "-", i2.2, "-", i2.2, " ", i2.2, ":", i2.2, ":", f6.3)' )

	img_tstr = strtrim(hr,1) + ':' + strtrim(mi,1) + ':' + strtrim(sec,1)+' '+ $
                   strtrim(yr,1) + '/' + strtrim(mn,1) + '/' + strtrim(dy,1)
	img_time = anytim(img_tstr, /yohkoh)
	img_pkt_time = img_time
        img_time_sec = anytim(img_time)
        thistai = anytim2tai(anystring)

; Read the packet data header, dataset=isn
        if keyword_set(mk_pkt_data_arr) then begin
          if not exist(pkt_data_arr) then pkt_data_arr = packet.data else $
            pkt_data_arr = [[pkt_data_arr], [packet.data]]
          help, pkt_data_arr
        endif

        if keyword_set(qdebug) then begin
;         help, packet
;         isp_map = mk_suvi_isp_map()
;         help, isp_map, isp_map
;         STOP, "Stopping after packet read. Packet is in variable 'PACKET'."
          print, packet.data[0:49], format='(50z4)'
        endif

        hdr = mk_suvi_isp_struct(packet, /head_only)
	dataset = hdr.head.suv_isp_hdr_isn
	date = string(format='(i4.4, i2.2, i2.2)', yr, mn, dy)
	time = string(format='(i2.2, i2.2, i2.2)', hr, mi, sec)
	serialno = string(format='(i7.7)', dataset)
	this_img_name = date + '_' + time + '_' + serialno
	if (n_elements(img_names) eq 0) then $
          img_names = this_img_name else $
          img_names = [img_names, this_img_name]

; Define new image variable from image template vector:
	img = img_template

; Loop through and insert image data into img_template:

	while not newfile do begin
;               if keyword_set(verbose) then $
;                       print, 'Start of inner while loop: new file not yet encountered.'
;               i16 = unsign(fix( swap_endian( pktdata.datahead),0,12 ))
;               i32 = swap_endian( packet.datahead )

; Read in packet data header:	 
		packet = record(thisrecnum)

;               pkt_num = i16[5]
;               dataset = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x

                hdr = mk_suvi_isp_struct(packet, /head_only)
                pkt_num_old = pkt_num
		pkt_num = hdr.head.suv_isp_hdr_pkt_num
                dataset = hdr.head.suv_isp_hdr_isn

; If this packet number = 0, then it contains the ISP header:
		if (pkt_num eq 0) then begin
                        newisp = 1
; Found leading ISP packet, moving on to first image data packet:

; Flag non-sequential ISN jumps:
			tmpi = hdr.head.suv_isp_hdr_isn
			if (ispisni ne -1) then begin
				isndiff = tmpi-ispisni
				if (isndiff eq 1) then ispisn = string(tmpi) else ispisn = string(tmpi)+'*'
; Difference should always be 1 unless isn is set:
			endif else begin
		 		ispisn = string(tmpi)
			endelse 
		 
			isp_record_number = thisrecnum
			thisrecnum = thisrecnum + 1l
			continue
		endif

; Nota Bena: first image data packet is packet 1 (packet 0 is reserved for copy of ISP):
		num_pixls = pkt_num*data_words
		indx = (pkt_num-1)*data_words
		imgwords = fix(packet.data,0,data_words)
		img(indx:indx+data_words-1) = imgwords
		flag = hdr.head.suv_isp_hdr_flg

; Read next record:
		pkt_num = hdr.head.suv_isp_hdr_pkt_num
		thisrecnum = thisrecnum + 1l
		packet_new = record(thisrecnum)
		hdr_new = mk_suvi_isp_struct(packet_new, /head_only)

		dataset_new = hdr_new.head.suv_isp_hdr_isn
		if (dataset_new ne dataset) then begin
                        newfile = 1
                        if keyword_set(verbose) then print, 'new file encountered.'
			if (newisp) then begin
                                if keyword_set(verbose) then print, 'new isp encountered.'
				writeimg = 1
 				newisp = 0
                        endif else begin
; Partial image - did not contain an ISP.
                                if keyword_set(verbose) then print, ' Partial image - did not contain an ISP.'
                        endelse
 			print, pkt_num, format='("New image header detected after packet no. ", i0)'
		endif

		if EOF(lun) then begin
; Encountered EOF - new image:
                        if keyword_set(verbose) then print, 'EOF encountered.'
                        newfile = 1
                        if keyword_set(qdebug) then stop, 'MK_SUVI_FITS: Stopping at EOF.'
			if ((flag eq 3) and (newisp eq 1)) then writeimg = 1
; Capture data if boundary case: the last packet added to img[] was the next-to-last packet
;   of the entire image, and the last packet of the entire image ends with an EOF:
			flag_nxt = hdr_new.head.suv_isp_hdr_flg
			if (flag_nxt eq 3) then begin
				if (newisp) then writeimg = 1
				pkt_num_nxt = hdr_new.head.suv_isp_hdr_pkt_num
; Last packet may not be full of data:
				data_words_nxt = 2*hdr_new.head.suv_isp_hdr_data_sz32
				exp_data_words = (hdr_new.head.suv_isp_hdr_rows*hdr_new.head.suv_isp_hdr_cols) - $
                                                 pkt_num*data_words
				if (data_words_nxt ne exp_data_words) then begin
					print, data_words_nxt, exp_data_words, $
; Error? Header-specified size of data in last packet does not match expected size. Using expected size:
					data_words_nxt=exp_data_words
				endif
				indx = pkt_num*data_words ; next-to-last packet was full
				imgwords = fix(packet.data,0,data_words)
				img(indx:indx+data_words-1)=imgwords
			endif
		endif

	endwhile
	newfile = 0

; If you have a full image then create index and data arrays for this image optionally write FITS file:
	if writeimg then begin
		writeimg = 0 ; reset image completion flag
		recnum = thisrecnum

if keyword_set(do_call_hdr_maker) then begin
   suvi_fits_hdr = mk_suvi_full_header(record(isp_record_number), index=index)
   STOP
endif

   
                version = packet.ccsds_sec[9]*256l + packet.ccsds_sec[10]
; Reform image pixel vector into apprpriate 2D image array.
; Get appropriate number of rows and columns from the last image packet header:
;        	hdr  = mk_suvi_isp_struct(packet), /head)
                isp  = mk_suvi_isp_struct(record(isp_record_number))
                info = mk_suvi_info_struct(isp, img_pkt_time=img_pkt_time)

		dim1 = isp.head.suv_isp_hdr_cols
		dim2 = isp.head.suv_isp_hdr_rows
		outimage = uintarr(dim1, dim2)
		outimage_lastpix = dim1*dim2-1
		outimage[0] = img[0:outimage_lastpix]
; Calculate image statistics and optionally write them to a text file:
		immean   = MEAN(outimage)
		immedian = MEDIAN(outimage)
		immax    = MAX(outimage)
		immin    = MIN(outimage)
		imstdev  = STDEV(outimage)
		subimg   = outimage[50:dim1-51,4:dim2-3]
		simmean  = MEAN(subimg)
		simmax   = MAX(subimg)
		simstdev = STDEV(subimg)

; ...and optionally write statistics parameters to a text file:
                if keyword_set(do_stats) then begin
                  stat_string = arr2str([string([apid, date, time, thistai], format='(10a12)'), $
                                         string([serialno, dim1, dim2, immean, immedian, immin, $
                                                 immax, imstdev, simmax, simmean, simstdev], $
                                                format='(10f12.2)')])
                  file_append, statfile, stat_string
                endif

; Generate various 'pieces' of the eventual full SUVI FITS L0 header:

; First create minimal L0 header structure:
l0_min_hdr = { simple:'t', bitpix:16, naxis:2, naxis1:dim1, naxis2:dim2}
tnames_l0_min_hdr = tag_names(l0_min_hdr)
n_tags_l0_min_hdr = n_elements(tnames_l0_min_hdr)
;l0_min_hdr_comments = strarr(n_tags_l0_min_hdr)
l0_min_hdr_comments = tnames_l0_min_hdr

; Next create headers from the isp 'head' sub-structure tags and the isp 'mnem' sub-structure tags:
tnames_isp_head = tag_names(isp.head)
tnames_isp_mnem = tag_names(isp.mnem)
suvi_fits_isp_head_hdr_comments = tnames_isp_head
suvi_fits_isp_mnem_hdr_comments = tnames_isp_mnem
n_tags_isp_head = n_elements(tnames_isp_head)
n_tags_isp_mnem = n_elements(tnames_isp_mnem)

file_suvi_index_template = $
  concat_dir(get_logenv('SUVI_DOCS'), 'suvi_index_template.sav')

suvi_fits_isp_head_hdr_ref = isp.head
suvi_fits_isp_mnem_hdr_ref = isp.mnem

;; If the suvi_index_template.sav file has not yet been created, then
;; change the tag names to unique, numbered 8 character names:
;if file_exist(file_suvi_index_template) eq '' then begin
;  suvi_fits_isp_head_hdr = suvi_fits_isp_head_hdr_ref
;  suvi_fits_isp_mnem_hdr = suvi_fits_isp_mnem_hdr_ref
;
;  tnames_isp_head_new = 'isph_' + string(indgen(n_tags_isp_head)+1,'(i3.3)')
;  for i=1,n_tags_isp_head do suvi_fits_isp_head_hdr = $
;    rep_tag_name(suvi_fits_isp_head_hdr, tnames_isp_head[i-1], tnames_isp_head_new[i-1])
;
;  tnames_isp_mnem_new = 'ispm_' + string(indgen(n_tags_isp_mnem)+1,'(i3.3)')
;  for i=1,n_tags_isp_mnem do suvi_fits_isp_mnem_hdr = $
;    rep_tag_name(suvi_fits_isp_mnem_hdr, tnames_isp_mnem[i-1], tnames_isp_mnem_new[i-1])
;endif else begin
;  suvi_fits_isp_head_hdr = isp.head
;  suvi_fits_isp_mnem_hdr = isp.mnem
;endelse

suvi_fits_isp_head_hdr = isp.head
suvi_fits_isp_mnem_hdr = isp.mnem

suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_mnem_hdr)

; Likewise, create another temporary structure to hold the 'info' structure isp-derived tags:
tnames_info = tag_names(info)
suvi_fits_info_hdr_comments = tnames_info
n_tags_info = n_elements(tnames_info)

suvi_fits_info_hdr_ref = info

;; As with the isp tag names, if the suvi_index_template.sav file has not yet
;; been created, then change the tag names to unique, numbered 8 character names:
;if file_exist(file_suvi_index_template) eq '' then begin
;  suvi_fits_info_hdr = suvi_fits_info_hdr_ref
;
;  tnames_info_new = 'info_' + string(indgen(n_tags_info)+1,'(i3.3)')
;  for i=1,n_tags_info do suvi_fits_info_hdr = $
;    rep_tag_name(suvi_fits_info_hdr, tnames_info[i-1], tnames_info_new[i-1])
;endif else begin
;  suvi_fits_info_hdr = info
;endelse

suvi_fits_info_hdr = info

; NB: If necessary, create another component structure with WCS tags that are not in
;     the other component structures

; Now join then together into on big structure:
suvi_fits_hdr_struct = join_struct(l0_min_hdr,           suvi_fits_info_hdr_ref)
suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_head_hdr_ref)
suvi_fits_hdr_struct = join_struct(suvi_fits_hdr_struct, suvi_fits_isp_mnem_hdr_ref)

; And also for the 8 character tag name version (for FITS write):
suvi_fits_hdr_struct_out = join_struct(l0_min_hdr,               suvi_fits_info_hdr)
suvi_fits_hdr_struct_out = join_struct(suvi_fits_hdr_struct_out, suvi_fits_isp_head_hdr)
suvi_fits_hdr_struct_out = join_struct(suvi_fits_hdr_struct_out, suvi_fits_isp_mnem_hdr)

if file_exist(file_suvi_index_template) ne '' then begin
  restore, file_suvi_index_template
  tmp_struct = suvi_fits_hdr_struct
  index_as_written[*] = tmp_struct
  suvi_fits_hdr_struct_out = index_as_written
endif

; Now concatenate the comment arrays for comment fields of the full SUVI FITS header:
suvi_fits_hdr_comments = [l0_min_hdr_comments, suvi_fits_info_hdr_comments, $
                          suvi_fits_isp_head_hdr_comments, suvi_fits_isp_mnem_hdr_comments]

; Create full index structure (with tag names and tag values) for this image:
suvi_fits_hdr_values = suvi_fits_hdr_struct
suvi_fits_hdr_names  = suvi_fits_hdr_comments

; Check on line suvi header tag name list against the constructed tag list:
; file_append, 'suvi_fits_tag_names.txt', tnames_suvi_fits_hdr,/new

index = suvi_fits_hdr_struct

; Finally, create the actual SUVI FITS header from the full SUVI FITS header structure:
suvi_fits_hdr = struct2fitshead(suvi_fits_hdr_struct_out, outimage, use_sxaddpar=use_sxaddpar, $
	        use_fxaddpar=use_fxaddpar, comments=suvi_fits_hdr_comments, $
                allow_crota=allow_crota, dateunderscore2dash=dateunderscore2dash, $
                _extra=_extra)

; Define output data array:
; (Here, we add the option of replacing the data array extracted from the packet with
; a data array extracted from an external FITS file)
if not exist(files_fits_alt) then begin
   data = float(outimage)
endif else begin

; buff = rd_tfile('cdrl_81_test_case_list_kapusta_original.txt',3)
; files_kap = buff[1,*]                            
; files_kap = strmid(files_kap,28)                 
; files_kap=reform(files_kap)                      
; for i=0,65 do files_kap[i] = str_replace(files_kap[i],"',","")
; files_kap = concat_dir('/sanhome/slater/public_html/outgoing/vasudevan/test_images/L0',files_kap)
; file_append,'cdrl_81_test_case_file_list_kapusta_original.txt',files_kap,/new
; buff = rd_tfile('cdrl_81_test_case_file_list_kapusta_original.txt')

;  mreadfits, files_fits_alt[num_imgs], index_alt, data_alt

   print, '******************************************'
   print, files_fits_alt[num_imgs]
   print, '******************************************'
;  TODO - Fix following one line kluge:
;  data = data_alt
endelse

; Optionally display image:
if keyword_set(do_display) then begin
   wdef,0,1330,1292
   erase
   tvscl, safe_log10(data)
endif

; Optionally print out selected header structure tags:
if keyword_set(verbose) then begin
   print, 'IMAGE TIME   = ' + strtrim(index.img_pkt_time,2)
   print, 'ASNAME       = ' + strtrim(index.asname,2)
   print, 'SHT_EXP_TIME = ' + strtrim(index.sht_exp_time,2)
   print, ''
   print, 'ASPOS        = ' + strtrim(index.aspos,2)
   print, 'ASSTATUS     = ' + strtrim(index.asstatus,2)
   print, 'FW1NAME      = ' + strtrim(index.fw1name,2)
   print, 'FW1STATUS    = ' + strtrim(index.fw1status,2)
   print, 'FW2NAME      = ' + strtrim(index.fw2name,2)
   print, 'FW2STATUS    = ' + strtrim(index.fw2status,2)
   print, 'FRAMETYPE    = ' + strtrim(index.frametype,2)
endif
STOP
; Optionally write FITS file:
if keyword_set(do_write) then begin
   set_logenv, 'SUVI_L0', '/archive1/suvi/suvi_data/fm1/l0'
   path_file = ssw_time2paths(anytim(!stime, /ccsds), parent=get_logenv('SUVI_L0'), /hourly)
   mk_dir, path_file
;  outfil_l0 = this_img_name + '_l0.fits'
   outfil_l0 = 'suvi_fm1_l0_' + index.asname + '_' + $
               time2file(anytim(!stime, /ccsds)) + '.fits'
   WRITEFITS, concat_dir(path_file, outfil_l0), data, suvi_fits_hdr
   print, ' Wrote ' + concat_dir(path_file, outfil_l0)
endif

; Optionally save the full SUVI IDL header structure which uses the original ISP
; tag names, as a template for converting from the FITS 8 character proxy names
; to ISP tag names after reading the FITS file:
if keyword_set(do_save_templates) then begin
   mreadfits, concat_dir(get_logenv('SUVI_L0'), outfil_l0), index_as_read
   tnames_as_read = tag_names(index_as_read)
   tnames_orig = tag_names(index)
   n_tags_orig = n_elements(tnames_orig)

   index_template = index_as_read
   for i=0,n_tags_orig-1 do $
      index_template = rep_tag_name(index_template,tnames_as_read[i],tnames_orig[i])

   index_as_written = index
   for i=0,n_tags_orig-1 do $
      index_as_written = rep_tag_name(index_as_written,tnames_orig[i],tnames_as_read[i])

   save, index_as_written, index_as_read, index_template, $
         file = concat_dir(get_logenv('SUVI_DOCS'), 'suvi_index_template.sav')
endif

; Optionally call suvi_prep and create L1 (index, data) from L0 (index,data):
if keyword_set(do_prep) then begin

;comments = [ $
;  '-------------------------------------------------------------------------------', $
;  ' Begin GPA routines for image file ' + 
;  '-------------------------------------------------------------------------------' ]
;prstr, comments

; SUVI_L1b_Processing, data, datap
;  suvi_prep, index, data, indexp, datap, this_img_name=this_img_name, $
;             do_write=do_write, _extra=_extra
   suvi_prep, suvi_fits_hdr_struct, data, indexp, datap, this_img_name=this_img_name, $
              do_write=do_write, _extra=_extra
endif

; Optionally create input and output index arrays:
if keyword_set(do_index_arr) then begin
  if not exist(iindex_arr) then begin
    iindex_arr = index
    if exist(indexp) then oindex_arr = indexp
  endif else begin
    iindex_arr = concat_struct(iindex_arr, index)
    if exist(indexp) then oindex_arr = concat_struct(oindex_arr, indexp)
  endelse 
endif
  
; Optionally create input and output data arrays:
if keyword_set(do_data_arr) then begin
  if not exist(idata_arr) then begin
    idata_arr = data
    if exist(datap) then odata_arr = datap
  endif else begin     
    idata_arr = [[[idata_arr]], [[data]]]
    if exist(datap) then odata_arr = [[[odata_arr]], [[datap]]]
  endelse
endif

endif ; End of writeimg block

if keyword_set(verbose) then begin
  print, "=========================="
  print, "FINISHED PROCESSING IMAGE " + strtrim(ss_img[i_ss_img])
  if exist(index) then begin
    n_tags_index = n_elements(tag_names(index))
    help, index, data, n_tags_index
  endif
  if exist(iindex_arr) then help, iindex_arr, idata_arr
  if exist(oindex_arr) then help, oindex_arr, odata_arr
  print, "=========================="
endif

i_ss_img = i_ss_img + 1
delvarx, index, data

if keyword_set(verbose) then help, newfile
if keyword_set(qdebug) then $
  stop, 'MK_SUVI_FITS: Stopping on request at end of while.'

endwhile

free_lun, lun

end
