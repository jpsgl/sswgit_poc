
pro mk_telem_mnem_plot

while 1 eq 1 do begin

   restore, '/sanhome/shing/suvi/fm4tv/caseRT/GIF_FULL/buff_mnem.gif'

   !p.multi = [0,3,3]
   set_plot, 'x'
   wdef, 0, 1024+256, 1024

   t_fed = anytim(buff_mnem.fed.daytime, /ccsds)
   t_ked = anytim(buff_mnem.ked.daytime, /ccsds)
   t_swa = anytim(buff_mnem.swa.daytime, /ccsds)

; Raw values:
;  utplot, t_fed, buff_mnem.fed.value[*,0], charsiz=2, psym=-6, symsiz=2, title=buff_mnem.fed.mnem[0]
;  utplot, t_fed, buff_mnem.fed.value[*,1], charsiz=2, psym=-6, symsiz=2, title=buff_mnem.fed.mnem[1]
;  utplot, t_fed, buff_mnem.fed.value[*,2], charsiz=2, psym=-6, symsiz=2, title=buff_mnem.fed.mnem[2]

; Engioneering units:

   t_var = t_fed
   var = smooth(buff_mnem.fed.value[*,0], 3)
   coef = [83.5055, -1.23147d-1, 9.11876d-5, -4.17058d-8, 9.70631d-12, -9.09571d-16]
   var_c = coef[0] + coef[1]*var + coef[2]*var^2 + coef[3]*var^3 + coef[4]*var^4 + coef[5]*var^5
   var_c_min = min(var_c) & var_c_max = max(var_c) & var_c_avg = mean(var_c) & var_c_offset = fix(abs(0.1*var_c_avg)>5)
   yrange_min = var_c_min - var_c_offset ; & if var_c_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_c_max + var_c_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Deg'
   utplot, t_var, var_c, psym=1, symsiz=0.5, title=strupcase(buff_mnem.fed.mnem[0]), $
           yrange=yrange, charsize=2.5, ycharsize=1.2, ytitle=ytitle

   t_var = t_fed
   var = smooth(buff_mnem.fed.value[*,1], 3)
   coef = [83.5055, -1.23147d-1, 9.11876d-5, -4.17058d-8, 9.70631d-12, -9.09571d-16]
   var_c = coef[0] + coef[1]*var + coef[2]*var^2 + coef[3]*var^3 + coef[4]*var^4 + coef[5]*var^5
   var_c_min = min(var_c) & var_c_max = max(var_c) & var_c_avg = mean(var_c) & var_c_offset = fix(abs(0.1*var_c_avg)>5)
   yrange_min = var_c_min - var_c_offset ; & if var_c_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_c_max + var_c_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Deg'
   utplot, t_var, var_c, psym=1, symsiz=0.5, title=strupcase(buff_mnem.fed.mnem[1]), $
           yrange=yrange, charsize=2.5, ycharsize=1.2, ytitle=ytitle

   t_var = t_fed
   var = smooth(buff_mnem.fed.value[*,2], 3)
   coef = [-166.1905, 5.438212d-2, 1.370277d-6, -6.595168d-11, 4.0d-15, 0.0]
   var_c = coef[0] + coef[1]*var + coef[2]*var^2 + coef[3]*var^3 + coef[4]*var^4 + coef[5]*var^5
   var_c_min = min(var_c) & var_c_max = max(var_c) & var_c_avg = mean(var_c) & var_c_offset = fix(abs(0.1*var_c_avg)>5)
   yrange_min = var_c_min - var_c_offset ; & if var_c_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_c_max + var_c_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Deg'
   utplot, t_var, var_c, psym=1, symsiz=0.5, title=strupcase(buff_mnem.fed.mnem[2]), $
           yrange=yrange, charsize=2.5, ycharsize=1.2, ytitle=ytitle

; Raw:

   psym = 1 ; psym = -6
   
   t_var = t_ked
   var = buff_mnem.ked.value[*,0]
   var_min = min(var) & var_max = max(var) & var_avg = mean(var) & var_offset = fix(abs(0.1*var_avg)>1)
   yrange_min = var_min - var_offset ; & if var_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_max + var_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Cumulative Error Count'
   utplot, t_var, var, psym=psym, symsiz=0.5, title=strupcase(buff_mnem.ked.mnem[0]), $
           yrange=yrange, yticks=yrange_max-yrange_min, charsize=2.5, xcharsiz=0.9, ycharsize=1.2, $
           ytitle=ytitle
  
   t_var = t_ked
   var = buff_mnem.ked.value[*,1]
   var_min = min(var) & var_max = max(var) & var_avg = mean(var) & var_offset = fix(abs(0.1*var_avg)>1)
   yrange_min = var_min - var_offset ; & if var_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_max + var_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Cumulative Error Count'
   utplot, t_var, var, psym=psym, symsiz=0.5, title=strupcase(buff_mnem.ked.mnem[1]), $
           yrange=yrange, yticks=yrange_max-yrange_min, charsize=2.5, xcharsiz=0.9, ycharsize=1.2, $
           ytitle=ytitle

   t_var = t_swa
   var = buff_mnem.swa.value[*,0]
   var_min = min(var) & var_max = max(var) & var_avg = mean(var) & var_offset = fix(abs(0.1*var_avg)>1)
   yrange_min = var_min - var_offset ; & if var_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_max + var_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Cumulative Error Count'
   utplot, t_var, var, psym=psym, symsiz=0.5, title=strupcase(buff_mnem.swa.mnem[0]), $
           yrange=yrange, yticks=yrange_max-yrange_min, charsize=2.5, xcharsiz=0.9, ycharsize=1.2, $
           ytitle=ytitle

   t_var = t_swa
   var = buff_mnem.swa.value[*,1]
   var_min = min(var) & var_max = max(var) & var_avg = mean(var) & var_offset = fix(abs(0.1*var_avg)>1)
   yrange_min = var_min - var_offset ; & if var_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_max + var_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Cumulative Count'
   utplot, t_var, var, psym=psym, symsiz=0.5, title=strupcase(buff_mnem.swa.mnem[1]), $
           yrange=yrange, yticks=yrange_max-yrange_min, charsize=2.5, xcharsiz=0.9, ycharsize=1.2, $
           ytitle=ytitle

   t_var = t_swa
   var = buff_mnem.swa.value[*,2]
   var_min = min(var) & var_max = max(var) & var_avg = mean(var) & var_offset = fix(abs(0.1*var_avg)>1)
   yrange_min = var_min - var_offset ; & if var_min ge 0 then yrange_min = yrange_min>0
   yrange_max = var_max + var_offset & yrange = [yrange_min, yrange_max]
   ytitle = 'Cumulative Count'
   utplot, t_var, var, psym=psym, symsiz=0.5, title=strupcase(buff_mnem.swa.mnem[2]), $
           yrange=yrange, yticks=yrange_max-yrange_min, charsize=2.5, xcharsiz=0.9, ycharsize=1.2, $
           ytitle=ytitle

   buff = 255b - bytscl(tvrd())

;  write_gif, '/sanhome/slater/public_html/suvi/telem_mnem_plots/telem_mnem_plots_set1.gif', buff
;  write_gif, '/sanhome/shing/suvi/fm4tv/suvi_fm1_hk/telem_mnem_plots_set1.gif', buff
   write_gif, '/sanhome/shing/suvi/suvi_inst/fm1/ops/hk/telem_mnem_plots_set1.gif', buff

   wait, 600

endwhile
  
end

  
