pro SUVI_IMG, pktfname, img_names, old = old, range=range

;+
;
; Rips and writes FITS files out of a packet file
;
; $Id: suvi_img.pro 487 2015-10-01 17:01:50Z suvitc $
; $Date: 2015-10-01 10:01:50 -0700 (Thu, 01 Oct 2015) $
; $Revision: 487 $
; $Author: suvitc $
; $HeadURL: svn+ssh://suvitc@suvifsw1.atc.lmco.com/home/suvisvn/idltc/suvi_img.pro $
;
; History:
; 06-Aug-15, Shing, made better use of /range to print fits image names if <0.
; 01-Aug-15, Shing, utilize /range keyword to passed from suvi_mfits to select a range to extract
; 30-Jul-15, L.Shing, removed annoying print for every packet 
;			improved suvi_mfits.pro so this procedure is not called until packet file is stable
; 19-Sep-2013, Mateos. Replace hard coded path in pktdir with path derived
;  from $GSE_PKT_FILE_DIR.
; 07-Feb-13, Anonymous, Attempted to fix the FOOBAR code for open/close on files 
; 17-Jul-12, M.Mateos. Add free_lun wherever it was missing.
; 12-Jul-12, M.Mateos. Add checks for data size of last packet.
; 10-Jul-12, M.Mateos. Read number of rows and columns from image packet header.
; 09-Jul-12, M.Mateos. Skip leading packet #0 (copy of ISP). Start filling at
;  img[0], not img[data_words].
; 25-May-12, G.Linford. Copied suvi_imgsmemi to suvi_img for emi testing.
; 25-May-12, G.Linford. Copied suvi_img to suvi_imgsmemi for emi and made the
;  following changes:
;  Changed "hard-coded" dim2 from 1280 to 128 and clipped first and last 50
;  column pixels from the image for "inside" stat calculation, as suggest by
;  Mons.
; 29-May-12, M.Morrison.  Clip first 4 lines to handle dropped packet because
;  of EOF vs new image code error
;        *** NEED TO FIX THE EOF/new Image error - packet is incremented
;            unnecessarily **
;
; Extract image data from science packets and format them into FITS files
;
;  IDL> suvi_img, science_packet_file_name
;
; INPUT:
;  pktfname = name of file containing SUVI science 
;       data packets. Packets must conform to SUVI CCSDS packet definiton.
;
; OUTPUT:
;  img_names   -  FITS file names of all images written by this routine
;                 (string array)
;
; KEYWORD PARAMETERS:
;  /old     -- if set, assume that the image packets are formatted as they were
;              prior to EDU EMI testing (2011-11). 
;-

;++++++++++++++++++++++++++
; Set up statistics file
;--------------------------
;append = 0
;if N_ELEMENTS(statfile) gt 0 then begin
;   if FILE_EXIST(statfile) then begin
;      append = 1
;   endif
;endif
;if not append then begin
;   statfile = '~/stats/' + STIM_DATE(/time) + '_suviimg_stats.txt'
;   OPENW, statlun, /get_lun, statfile
;   PRINTF, statlun, 'ApID', 'Date', 'Time', 'TAI', 'SerialNo', 'Naxis1', 'Naxis2', $
;      'Mean', 'Median', 'Min', 'Max', 'StdDev', 'InMax', 'InMean', 'InStdDev', $
;      format = '(a10, a11, a9, a14, a10, 10a12)'
;   CLOSE, statlun
;   FREE_LUN, statlun ; close above closes file but does not release the lun
;endif

;++++++++++++++++++++++++++
; Select a packet file
;--------------------------
pktdir = '/archive1/suvi/suvi_data/fm1/tlm'
imgpath = '/archive1/suvi/suvi_data/fm1/tlm'
fitspath = '/archive1/suvi/suvi_data/fm1/tlm/fits_tmp'

; Initialize params:
utnow = ut_time(/ex)
eof_cntr=0 ; initialize eof_cntr
recnum = 0

OPENR, lun, pktfname, /get_lun
filename = pktfname

statfile = '~/statfile'
OPENW, statlun, /get_lun, statfile
PRINTF, statlun, 'ApID', 'Date', 'Time', 'TAI', 'SerialNo', 'Naxis1', 'Naxis2', $
        'Mean', 'Median', 'Min', 'Max', 'StdDev', 'InMax', 'InMean', 'InStdDev', $
        format = '(a10, a11, a9, a14, a10, 10a12)'
FREE_LUN, statlun

;++++++++++++++++++++++++++
; Set up image packet
; format based on change of
; 2011/11/11
;--------------------------
;if not KEYWORD_SET(old) then old = 0
pktdate = STRSPLIT(/extract, pktfname, '/')
dateint = LONG(pktdate[N_ELEMENTS(pktdate)-1])

old=0 ;*******************************delete this later*********************************
if old then begin
   hdr_dwords = 2
   data_words = 4082l
   pkt_template = {pktold, prihdr:BYTARR(6), sechdr:BYTARR(14), imghdr:LONARR(hdr_dwords), imgdata:INTARR(data_words)}
endif else begin
   hdr_dwords = 6
   data_words = 4074l
   pkt_template = {pkt, prihdr:BYTARR(6), sechdr:BYTARR(14), imghdr:LONARR(hdr_dwords), imgdata:INTARR(data_words)}
endelse

;++++++++++++++++++++++++++
; Define pixel vector
; and required static FITS
; keywords 
;--------------------------
img_template = INTARR( 1722604 )
keyword1 = STRING( format = '( "SIMPLE  =                    T", 49x, a1 )', " " )
keyword2 = STRING( format = '( "BITPIX  =                   16", 49x, a1 )', " " )
keyword3 = STRING( format = '( "NAXIS   =                    2", 49x, a1 )', " " )
fitsend = 'END' + STRING( REPLICATE(32b,77) )   ;  Append after the last FITS keyword

;++++++++++++++++++++++++++
; Error handler (especially
; for EOF errors)
;--------------------------
CATCH, error_status
if error_status ne 0 then begin
   PRINT, 'SUVI_IMG detected an error'
   PRINT, !error_state.msg
   RETURN
endif

get_fname=0

sz=(file_info(pktfname)).size/8192.    ;;calculate approximate number of recnums given each packet is 8122 bytes
 if N_ELEMENTS(range) eq 0 then range=[0,1]
if (recnum eq 0) or (range(0) lt 0) then begin
 if (range(0) lt 0) then begin ;range is called with a negative number, so just print the filename
 	get_fname=1
 	range *= -1
	endif
 if (range(0) gt 1) then range(0)=1
 if (range(1) gt 1) then range(1)=1
 pkt_num=2
 recnum=long(range(0)*sz)  ;;jump to the desired place, not always the beginning!
 if (sz-recnum) lt 1 then recnum -= 2 ;;don't get too close to the end of file!
 if (get_fname eq 0) then recnum -= 421
 if recnum lt 0 then recnum=0
endif

a = fstat(lun)
if (a.open eq 0) then begin OPENR, lun, pktfname,/get_lun	; unit closed reopen
	print,'reopening in suvi_img',recnum
	endif
	
;++++++++++++++++++++++++++
; associate packet structure 
; with image packet file
;--------------------------
rec = ASSOC( lun, pkt_template )

;++++++++++++++++++++++++++++
; Check and double-check that
; you aren't at the end of
; the packet file
;----------------------------
if N_ELEMENTS(recnum) eq 0 then recnum = 0l

if recnum gt 0 then testrecnum = recnum - 1 else testrecnum = 0
testrec = rec[testrecnum]
if EOF(lun) then begin              ;  Were you already off the end?
    PRINT, 'File already finished...'
;    CLOSE, lun
    FREE_LUN, lun ; close above closes file but does not release the lun
    RETURN
endif
testrec = rec[recnum]
if EOF(lun) then begin              ;  Were you sitting at the last record?
    PRINT, 'File already finished...'
;    CLOSE, lun
    FREE_LUN, lun ; close above closes file but does not release the lun
    RETURN
endif

;++++++++++++++++++++++++++
; Read each packet, 
; calculate start pixel 
; number and copy packet 
; data to image array
;--------------------------
thisrecnum = recnum + 1l
pktdata = rec[thisrecnum]
newfile = 0
writeimg = 0
while ((eof_cntr eq 0) and (recnum gt -1))do begin 

   ;++++++++++++++++++++++++++
   ; Construct file name
   ;--------------------------
   apid = ( ( pktdata.prihdr[0] and 3 ) * 256 ) + pktdata.prihdr[1]
   julian_day_epoch = 2451545D    ;   this is 1200 UT on 1-1-2000? Seems to give the right answer, though
   daynum = pktdata.sechdr[0]*65536l + pktdata.sechdr[1]*256l + pktdata.sechdr[2]
   msec = pktdata.sechdr[3]*16777216l + pktdata.sechdr[4]*65536l + pktdata.sechdr[5]*256l + pktdata.sechdr[6] 
   usec = pktdata.sechdr[7]*256l + pktdata.sechdr[8] 
   
    jul_day = julian_day_epoch + DOUBLE( daynum ) + DOUBLE( msec ) / 86400000.0D
    CALDAT, jul_day, mn, dy, yr, hr, mi, sec
   anystring = STRING( format = '(i4.4, "-", i2.2, "-", i2.2, " ", i2.2, ":", i2.2, ":", f6.3)',$
      yr, mn, dy, hr, mi, sec )
   thistai = ANYTIM2TAI(anystring)
   
   if old then begin
      dataset = SWAP_ENDIAN( pktdata.imghdr[1] ) 
   endif else begin
      dataset = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x
   endelse

   ; Old image naming scheme using APID_YYYY_MM_DD_HH_MM_SS.SSS_SN
   ;  this_img_name = STRING( format = '( z3.3, "_", i4.4, i2.2, "_", i2.2, "_", i2.2, "_", i2.2, "_", f06.3, "_", i6.6, ".fits")', apid, yr, mn, dy, hr, mi, sec, dataset )
   ; New image naming scheme using YYYYMMDD_HHMMSS_SN
   date = STRING(format = '(i4.4, i2.2, i2.2)', yr, mn, dy)
   time = STRING(format = '(i2.2, i2.2, i2.2)', hr, mi, sec)
   serialno = STRING(format = '(i7.7)', dataset)
   this_img_name = date + "_" + time + "_" + serialno + '.fits'
   if N_ELEMENTS(img_names) eq 0 then begin
      img_names = this_img_name
   endif else begin
      img_names = [img_names, this_img_name]
   endelse
   PRINT,"New image file name: ", this_img_name,recnum

   img = img_template
   if recnum eq 0 then wholeimage=1 else wholeimage=0 ;;; special case for first record
;;;whole_image=0

   ;++++++++++++++++++++++++++
   ; Loop through and assign
   ; image data to pixels
   ;--------------------------
   while (not newfile) and (get_fname eq 0) do begin
   
    
    ; Note that the 16-bit words are swapped. Examples: Packet Flag is at i16[3]
    ; instead of i16[2]. Packet Number is at i16[5] instead of i16[4].
    i16=unsign(fix(swap_endian( pktdata.imghdr),0,12))
    i32=swap_endian( pktdata.imghdr)
    ;print,"IMG HDR info i16, i32:", i16,i32
    
      if old then begin
         pkt_num = SWAP_ENDIAN( pktdata.imghdr[0] )
         dataset = SWAP_ENDIAN( pktdata.imghdr[1] )
      endif else begin
         ;pkt_num = SWAP_ENDIAN( pktdata.imghdr[3] ) / 65536ul
         pkt_num = i16[5]
         ;dataset = SWAP_ENDIAN( pktdata.imghdr[5] ) and '0000ffff'x
         dataset = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x
      endelse
      if pkt_num eq 0 then begin
	  	 wholeimage=1
         print, ""
         print, "Found leading ISP packet. Moving on to 1st image data packet"
         thisrecnum = thisrecnum + 1l
         pktdata=rec[thisrecnum]
         continue  ; next iteration of while loop
      endif
      
      ; Image Data packet number starts at 1 (0 is reserved for copy of ISP)
      num_pixls = pkt_num * data_words
      indx = (pkt_num-1) * data_words
      img[indx: indx + (data_words-1)] = pktdata.imgdata
      flag = i16[3]
      ;flag =pktdata.imghdr[0] / 65536ul
      ;print,flag
      ;print, pkt_num, indx, indx + (data_words - 1), i16, i32, $
      ;   format = '("Packet No.: ", i5, "  Image Array Start/End indices: ",i0,"/",i0,". IMG HDR info:", /, "i16 = ", 12(Z4.4,x), /, "i32 = ", 6(Z8.8,x))'

      ; Read next record
      thisrecnum = thisrecnum + 1l
      pktdata=rec[thisrecnum]
      if old then begin
         dataset_new = SWAP_ENDIAN( pktdata.imghdr[1] ) 
      endif else begin
         ;dataset_new = SWAP_ENDIAN( pktdata.imghdr[5] ) and '0000ffff'x
         dataset_new = SWAP_ENDIAN( pktdata.imghdr[0] ) and '0000ffff'x
      endelse

      if (dataset_new ne dataset) then begin
         newfile = 1
         writeimg = 1
         print, i16[5], $
           format='("New image header detected after Packet No. ", i0)'
      endif
      if EOF(lun) then begin
         eof_cntr += 1
         newfile = 1
         print, "Found EOF",pkt_num
         if old or flag eq 3 then writeimg = 1
         ; Capture data if boundary case: last packet added to img[] was the
         ; next-to-last packet of the entire image, and the last packet of the
         ; entire image ends up EOF.
         i16_nxt = unsign(fix(swap_endian( pktdata.imghdr),0,12))
         flag_nxt = i16_nxt[3]
         if flag_nxt eq 3 then begin
            writeimg = 1
            pkt_num_nxt = i16_nxt[5]  ; note words are swapped
            ; Last packet may not be full of data
            data_words_nxt = 2*i16_nxt[4]
            exp_data_words = (i16_nxt[10] * i16_nxt[11]) - pkt_num * data_words
            if data_words_nxt ne exp_data_words then begin
               print, data_words_nxt, exp_data_words, $
                  format='("ERROR: Header-specified size (",i5,") of data in last packet does not match expected size (",i4,"). Using expected size")'
               data_words_nxt = exp_data_words
            endif
            indx = pkt_num * data_words ; next-to-last packet was full
            img[indx: indx + (data_words_nxt-1)] = pktdata.imgdata[0:data_words_nxt-1]
            i32_nxt=swap_endian(pktdata.imghdr)
            print, pkt_num_nxt, indx, indx + (data_words_nxt - 1), $
               i16_nxt, i32_nxt, $
               format = '("Packet No.: ", i5, "  Image Array Start/End indices: ",i0,"/",i0,". IMG HDR info:", /, "i16 = ", 12(Z4.4,x), /, "i32 = ", 6(Z8.8,x))'
         endif
      endif
      ;if ((thisrecnum mod 50) eq 0) then stop
   endwhile
   newfile = 0
   
   
if ((pkt_num lt 421) and (eof_cntr gt 0)) then begin ;we hit EOF before image completed
	eof_cntr +=1
print,"WAIT EOF",pkt_num
recnum -= 10
	wait, (422-pkt_num)/(422./10) ;takes about 10s to read out full image (422 packets)
	end 						; do a wait so we do not keep re-entering suvi_img
   
   
   ;++++++++++++++++++++++++++
   ; If you have a full image,
   ; write stats and FITS file
   ;--------------------------
   if (wholeimage and writeimg and (get_fname eq 0)) then begin
      writeimg = 0
print,'*************begin at ',recnum,'*****DIFF in RECs=',thisrecnum-recnum
if eof_cntr gt 0 then recnum += 2 else recnum = thisrecnum
      ;++++++++++++++++++++++++++
      ; Turn the pixel vector 
      ; into a 2D image array
      ;--------------------------
      ; Attempt to make this block back-compatible
      version = pktdata.sechdr[9]*256l + pktdata.sechdr[10]
      if version lt 15248l then begin  ; FSW versions before KER/FSA 0x3C3F
         if num_pixls lt (1280l * 130l) then emi = 1 else emi = 0
         if emi then begin
            dim1 = 1280l
            dim2 = 128l
         endif else begin
            dim1 = 1330l
            dim2 = 1294l
         endelse
         dim1 = 1280l
         dim2 = 1280l      ; change for EMI frames to 128l but, normal is 1280l
      endif else begin
         ; Get number of rows and columns from last image packet
         dim1 = i16[10]  ; columns (words are swapped in i16[])
         dim2 = i16[11]  ; rows
         print, "Rows and columns read from image packet header"
      endelse
      outimage = UINTARR(dim1, dim2)
      outimage_lastpix = dim1 * dim2 - 1
      outimage[0] = img[0:outimage_lastpix]
      ;++++++++++++++++++++++++++
      ; Calculate image statistics
      ; and write them to a text
      ; file
      ;--------------------------
      immean = MEAN(outimage)
      immedian = MEDIAN(outimage)
      immax = MAX(outimage)
      immin = MIN(outimage)
      imstdev = STDEV(outimage)
      subimg = outimage[50:dim1-51,4:dim2-3]
      simmean = MEAN(subimg)
      simmax = MAX(subimg)
      simstdev = STDEV(subimg)
      OPENW, statlun, /append, /get, statfile
      PRINTF, statlun, apid, date, time, thistai, serialno, dim1, dim2, $
         immean, immedian, immin, immax, imstdev, simmax, simmean, simstdev, $
         format = '(a10, a11, a9, i14, a10, 2i12, 2f12.1, 2i12, f12.3, i12, 2f12.3)'
;	  CLOSE, statlun						; close the stats file
	  FREE_LUN, statlun
      
      ;++++++++++++++++++++++++++
      ; Generate a header and 
      ; write a FITS file
      ;--------------------------
      keyword4 = STRING( format = '( "NAXIS1  =                 ", i4.4, 49x, a1 )', dim1, " " )
      keyword5 = STRING( format = '( "NAXIS2  =                 ", i4.4, 49x, a1 )', dim2, " " )
      fitshead = [keyword1, keyword2, keyword3, keyword4, keyword5, fitsend]

      WRITEFITS, fitspath + this_img_name, outimage, fitshead
             PRINT, "Wrote ",this_img_name,recnum
             PRINT, ""
;if stdev(outimage(400:500,*) gt 5) then stop ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    endif
    
if (N_ELEMENTS(range) gt 0) then if (range(1) lt 1) and (recnum gt sz*range(1)) then begin ;;we've reached the limit the user wants; so, time to stop
        recnum= -1
	wholeimage=0
	endif
   ;stop,'debug'
if (get_fname ne 0) then recnum= -1  ;;abort this loop if all we wanted was the filename  
endwhile 


;CLOSE, lun							; close the pkt file
FREE_LUN, lun

CD, old_dir

end
