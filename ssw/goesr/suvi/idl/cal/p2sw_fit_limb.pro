;+
;________________________________________________________________________________________________________________________
;
; p2sw_fit_limb : determine les coordonnees du centre et le rayon du disque solaire pour une image donnee en fitant le limbe
;            en coordonnees polaires.
;________________________________________________________________________________________________________________________
;
; Appel : prof = fit_limb(img, param, type, theta_size, rho_size, band, [gamma])
;
; Resultat : tableau de dimensions [theta_size, 2]. prof(*, 0) contient les valeurs de theta valides et prof(*, 1)
;            contient les valeurs correspondantes de la distance au centre du max du gradient. Plus modification de
;            param (contient x, y et r), maxiter, tol, nbcol, sigmaa
;
; Parametres : img : tableau 2x2. Image dans laquelle ont veut mesurer les coordonnees du centre et le rayon. 
;              param : three element vector containing the coordinates and radius first guesses.
;              type : character string describing the type of limb (maximum ou minimum du gradient). Doit etre egal
;                     a 'min' ou 'max'.
;              theta_size : nombre d'echantillons le long d'une circonference. Une bonne valeur est 10000. 
;              rho_size : nombre d'echantillons le long d'un rayon. Une bonne valeur est 40.
;              band : tableau de deux elements contenant les bornes min et max (en rayons solaires) de la bande dans
;                     laquelle le limbe est suppose se situer.
;            
; Keywords : roll : angle antre le nord solaire et le haut de l'image. Permet le traitement des images tournees.
;            gamma : Tableau de deux elements contenant les valeurs min et max du contraste du limbe. si
;                    present, ne prend pas en compte dans le fit tous les points du limbe ne presentant pas un
;                    contraste compris entre les deux bornes.
;            maxiter : nombre maximum d'iterations. Si absent ou egal a 0, la valeur par defaut est 15. En retour,
;                      contient l'iteration maximale.
;            tol : tolerance de deviation du fit. Si absent ou egal a 0, la valeur par defaut est 2e-3. En retour,
;                  contient la difference max - min du fit.
;            nbcol : valeur du nombre minimum acceptable de colonnes valides. Si absent, la valeur par defaut est 0.
;                    En retour, contient le nombre de colonnes valides lors de la derniere iteration. Plus ce nombre
;                    est faible, moins la mesure est fiable. Si 0, les resultats n'ont pas de sens.
;            sigmaa : tableau de trois elements dans lequel sont retournes les ecarts types des coordonnes.
;            show : si present, affiche les profils a chaque iteration.
;            holes : si present et different de 0, prend en compte les latitudes des trous coronaux pour le fit.
;                    0.0->0.1 CH | 0.1->0.4 | 0.4->0.6 CH | 0.6->0.9 | 0.9->1.0 CH.
;________________________________________________________________________________________________________________________
;
; Historique : 03/07/1997
;              07/07/1997 : possibilite d'utiliser des images avec blocs manquants.
;              15/07/1997 : acceleration de la derivee et du reperage des blocs manquants.
;              16/07/1997 : ajout d'un critere de convergence.
;              18/07/1997
;              21/07/1997 : bug si limbe invisible fixe.
;              06/09/1997 : ajout du nombre de colonnes utilisees comme critere de fiabilite de la mesure. 
;              08/09/1997 : differenciation entre les longueurs d'ondes.
;                           retourne l'erreur sigmaa sur les coordonnees.
;                           ajout du keyword show.
;                           modification en fonction retournant le prof final.
;              09/09/1997 
;              07/11/1997 : centrage different suivant les longueurs d'onde.
;                           ajout du keyword hole. Si present et different de zero, les trous coronaux sont pris en 
;                           compte.
;              03/12/1997 : bug cacul de dummy fixe.
;              17/08/1998 : bug du calcul de prof fixe (prof = prof + 1).
;                           ajout du keyword gamma.
;              18/08/1998
;              19/10/1998 : modification des bornes des trous coronaux. Determinees comme etant les limites exterieures 
;                           des marches a 304.
;                           sauvegarde de la valeur de !p.multi.
;                           ajout du parametre band pour permettre de specifier les bornes min et max du rayon a prendre
;                           en compte.
;              27/10/1998 : plus besoin de passer l'image en flotant pour obtenir le meilleur resultat.
;              10/11/1998 : modification du critere de convergence.
;              28/01/2000 : ajout du keyword roll pour la prise en compte des images tournees.
;________________________________________________________________________________________________________________________
;-
  
function p2sw_fit_limb, img, param, type, theta_size, rho_size, band, $
                   roll=roll, gamma=gamma, maxiter=maxiter, tol=tol, nbcol=nbcol, sigmaa=sigmaa, show=show, holes=holes

  if keyword_set(tol) eq 0 then tol = double(1.5e-3)
  if keyword_set(maxiter) eq 0 then maxiter = 30
  if keyword_set(nbcol) eq 0 then nbcol = 100
 
  show_present = keyword_set(show)
  gamma_present = keyword_set(gamma)

  if show_present then begin
    savemulti = !p.multi
    if gamma_present then begin
      !p.multi = [0, 1, 2]
      window, !d.window+1, xsize=1200, ysize=400, title='Solar limb : polar coordinates'
    endif else begin
      !p.multi = 0
      window, !d.window+1, xsize=1200, ysize=256, title='Solar limb : polar coordinates' 
    endelse
  endif

  ;if type eq 'min' then oband = [0.98d, 1.04d] else oband = [0.97d, 1.03d]
  if type eq 'min' then oband = [0.99d, 1.02d] else oband = [0.97d, 1.03d]

  fit = [0.0d, 1.0d]
  correction = [0.0d, 0.0d, param[2]]
  w = replicate(1.0d, theta_size)

  mmax = double(1e6)
  
  angle = !pi*roll/180.0d

  i = 0
  repeat begin

    if i eq 0 then profile = p2sw_limb_profile(img, param, type, theta_size, rho_size, oband, roll=roll, holes=holes) $
    else profile = p2sw_limb_profile(img, param, type, theta_size, rho_size, band, roll=roll, gamma=gamma, holes=holes)

    nonzero = where(profile[*, 2] eq 1.0, count)
    
    if count gt nbcol then  begin

      x    = profile[nonzero, 0] - angle
      prof = profile[nonzero, 1]
      
      fit = curvefit(x, prof, w, correction, sigmaa, function_name='p2sw_polar_circle_limb', /double)

      if show_present then begin
        plot, x, prof, psym=3, yrange = [param[2]*band[0]-1, param[2]*band[1]+1], xtitle = 'angle (radians)',$
                               ytitle = 'distance au centre (pixels)', xstyle=1, ystyle=1, xrange=[-angle, -angle+2*!pi]
        oplot, x, fit
        if gamma_present then begin
          if i eq 0 then !p.multi = [0, 1, 2] $
          else begin
            plot, profile[*, 0] - angle, profile[*, 3], psym=3, yrange=[gamma[0]-0.5, gamma[1]+0.5], $
                  xtitle = 'angle(radians)', ytitle = 'gamma', xstyle=1, ystyle=1, xrange=[-angle, -angle+2*!pi]
            oplot, replicate(gamma[0], theta_size)
            oplot, replicate(gamma[1], theta_size)
          endelse
        endif
      endif
  
      mmax = double(sqrt(correction[0]^2.0d + correction[1]^2.0d + (param[2]-correction[2])^2.0d))

      param[0:1] = param[0:1]+correction[0:1]
      param[2] = correction[2]
    
    endif 
    
    i = i + 1
   
  endrep until (mmax lt tol) or (i eq maxiter) or (count le nbcol)

  if show_present then begin
    wdelete, !d.window
    !p.multi = savemulti
  endif

  if count le nbcol then return, -1
  
  maxiter = i
  nbcol = count
  if mmax ge tol then maxiter = maxiter + 1 
  tol = mmax

  return, [[x], [prof]]

end
