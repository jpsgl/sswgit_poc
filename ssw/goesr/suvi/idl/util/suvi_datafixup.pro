pro suvi_datafixup,index,data,oindex,odata, debug=debug
;
;+
;   Name: suvi_datafixup
;
;   Purpose: perform L0->L1B+ data transforms, update .HISTORY
;
;   Input Parameters:
;      index,data - from L0 (L1 + /UNDO)
;
;   Output Parameters:
;      oindex,odata - corrected + .HISTORY 
;
;   History:
;      20-feb-2018 - S.L.Freeland, G.L.Slater
;
;-
debug=keyword_set(debug)
version=.1

if n_params() lt 4 then begin 
   box_message,'Need 4 params: iin,din,iou,dout 
   return ; EARLY EXIT on bad input
endif

oindex=index ; assume >L0 inpput
odata=data

if required_tags(index,'info_001,info_002') then begin 
   box_message,'Level0 in put, calling isp->index translator'
   oindex=suvi_isp2index(index) ; Does the isp.x  -> index.x (for some subset of params)
endif

; from Greg l0->l1b

hist=get_history(oindex,'suvi_datafixup',found=found)
if debug then stop,'hist,found'
if ~found  then begin 
   
   update_history,oindex,/caller,version=version
   if debug then stop,'oindex,/caller,version=version'
   nx=data_chk(data,/nx)
   ny=data_chk(data,/ny)
   binning = fix(strmid(oindex.summode,0,1))

; Do SUVI FM-dependent cropping:
   if debug then stop, 'Stopping before image cropping.'
fm = 2 ; For the moment, fm fixed at 2.
; Also, note that for the moment, all the images must have same dimension ( same summode)
   case fm of
      1:    odata=odata[0:(1278/binning+1), 0:(1278/binning+1), *]
      2:    odata=odata[(30/binning[0]):(nx-(20/binning[0]+1)), (4/binning[0]):(ny-(8/binning[0]+1)), *]
      else: odata=odata[(30/binning[0]):(nx-(20/binning[0]+1)), (4/binning[0]):(ny-(8/binning[0]+1)), *]
   endcase
HELP, nx[0], ny[0], odata
   update_history, oindex, 'FM-dependent cropping done.' ; 'odata = idata[30:nx-21,4:ny-9]'

   oindex.naxis1 = data_chk(odata,/nx)
   oindex.naxis2 = data_chk(odata,/ny)
   oindex.crpix1 = (oindex.naxis1/2)+.5
   oindex.crpix2 = (oindex.naxis2/2)+.5
;  oindex.crval1 = 0.0 ; -30*2.5
;  oindex.crval2 = 0.0 ; -04*2.5

;  readout port flip
   if n_elements(readout_port) eq 0 then readout_port=1
   mess='SUVI image NOT FLIPPED'
   if readout_port eq 1 then begin 
      odata=rotate_3d(odata,5)
      mess='SUVI image FLIPPED for amplifier correction.'
   endif
   update_history, oindex,/caller,mess

; yaw_flip
  if ~keyword_set(yaw_flip) then yaw_flip=gt_tagval(oindex[0],/yaw_flip,missing=0)
  mess=(['SUVI image rotated 90 degrees clockwise.',$
         'SUVI image rotated 90 degrees counterclockwise (270 degrees clockwise).'])(yaw_flip)
  odata=rotate_3d(odata,([1,3])(yaw_flip))
  update_history,oindex,/caller,mess
  if debug then stop,'oindex,/caller,mess'

endif else box_message,'DATA already corrected'

return
end

   
