function suvi_time2files,t0,t1,wave=wave,long=long,short=short,dark=dark, parent=parent, backup=backup, $
   minutes_cadence=minutes_cadence, hours_cadence=hours_cadence, level=level, goes=goes, debug=debug, $
   match_times=match_times, level0=level0
;
;+
;   Name: suvi_time2files
;
;   Purpose: return file list for users {time range, wave, data-level, long/short, cadence...}
;
;   Input Parameters:
;      t0,t1 - time range
;
;   Output:
;      function returns impllied file list
;
;   Method:
;      setup params/pattern/parent & call ssw_time2filelist.pro
;
;   Keyword Parameters:
;      wave - {171,304,etc..}
;      long/short (mutually exclusive switches)
;      backup - switch - if set, use backup data area
;      match_times - vector of times or filelist to match (for example, call /LONG 1st, then /SHORT w/LONG filename vector for composites...)
;
;   History:
;      20-feb-2018 - S.L.Freeland
;
;-


case 1 of 
   n_params() eq 2:  ; user supplied time range
   n_params() eq 1: begin ; vector of times or file2time compatible file names
      if valid_time(t0[0]) then times=t0 else times=file2time(t0)
      t0temp=t0
      time_window,times,t0,t1,minutes=[-5,5]; implied time range
   endcase
   keyword_set(match_times): begin
      if valid_time(match_times[0]) then times=match_times else times=file2time(match_times,out='ecs')
      t0temp=t0
      time_window,times,t0,t1,minutes=[-5,5] ; implied time range
   endcase
   else: begin 
      box_message,'Need timerange, time-vector or match_times vector
     return,'' ; EARLY EXIT on bad input
   endcase
endcase
 
debug=keyword_set(debug)

if n_elements(goes) eq 0 then goes='r'

case 1 of 
   is_member(goes,'r,16'): fm=1
   is_member(goes,'s,17'): fm=2
   else: fm=1 ; default='R'
endcase

level0=keyword_set(level0)
level=([1,0])(level0)

if ~keyword_set(wave) then wave=171

swave=strtrim(wave,2)
swave=strpad(swave,3,fill='0')

dark=keyword_set(dark)
short=keyword_set(short)
long=1-short ; default

case 1 of 
   keyword_set(filter): lfilt=strupcase(filter)
   is_member(strtrim(wave,2),'171,195,284,304'):lfilt='THINAL'
   is_member(strtrim(wave,2),'94,131'): lfilt='THINZR'
   else: begin 
      box_message,'No filter/default - setting filter=>*'
      lfilt='*'
   endcase
endcase

penv=get_logenv('SUVI_DATA')
case 1 of 
   keyword_set(parent): ; user supplied
   keyword_set(backup): parent='/mars/goes/suvi_backup/'
   file_exist(penv): parent=penv
   else: parent='/archive1/suvi/suvi_data/'
endcase

parentf=parent
sfm='fm'+strtrim(fm,2)
sl='l'+strtrim(level,2)
if strpos(parentf,sfm) eq -1 then parentf=concat_dir(parentf,sfm)
if strpos(parentf,sfm+'/'+sl) eq -1 then parentf=concat_dir(parentf,sl)
if level0 then parentf=concat_dir(parent,sfm+'/fits/packet_ids/0x032a/')


box_message,[parent,parentf]


case 1 of 
   level0: patt=''
   keyword_set(times): patt='*'  ; matching exact times
   dark:patt='*DARK*'
   else: patt='*'+swave+'_*'+lfilt+'*OPEN_LIGHT_*'+(['SHORT*','LONG*'])(long)
endcase

retval=''
count=0

if file_exist(parentf) then begin 
   retval=ssw_time2filelist(t0,t1,parent=parentf,pattern=patt,count=count,/HOUR)
end

if count gt 0 and n_elements(times) gt 0 then begin 
   ftimes=file2time(retval)
   ssm=tim2dset(anytim(ftimes,/int), anytim(times,/int),delta=dts)
   retval=retval[ssm]
   count=n_elements(retval)
endif
   
if debug then stop,'retval,patt,parentf'
return,retval
end
