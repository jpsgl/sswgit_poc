pro read_suvi,files,index,data,register=register,ref_map=ref_map, $
   xcen=xcen, ycen=ycen, _extra=_extra
;
;   Name: read_suvi
;
;   Purpose: vector of suvi files (L0/L1n/) -> index,data, optionally w/register
;
;   Input Parameters:
;      files - list of suvi FITS (from suvi_time2files for example)
;
;   Output Parameters:
;      index,data - canonical SSW "index,data" 2D/3D (1:1 files)
;
;   Keyword Parameters:
;      register (switch) - if set, call ssw_register (roll=0)
;      ref_map - (not yet verified) - optional reference map(s) for registration (waven:wavem) -> ssw_register
;      xcen/ycen - optional sun center - kind of a kludge until headers fully copacetic
;      _extra - inherit -> ssw_register ( like /derotate...)
;
;   History:
;      22-feb-2018 - S.L.Freeland
;      22-may-2018 - 'index only' speedup; skip DATA stuff

if n_params() eq 2  then begin 
   mreadfits_header,files,index,only_tags=only_tags
   if required_tags(index,'info_001,info_002') then begin
      box_message,'Level0 in put, calling isp->index translator'
      index=suvi_isp2index(index) ; Does the isp.x  -> index.x (for some subset of params)
   endif 
endif else begin 
mreadfits,files,ii0,dd0

suvi_datafixup, ii0,dd0,index,data

if n_elements(xcen) eq 0 then xcen=0
if n_elements(ycen) eq 0 then ycen=0

index.crval1=xcen & index.crval2=ycen

if keyword_set(register) then begin
   ssw_register,index,data,rindex,rdata,roll=0,ref_map=ref_map, _extra=_extra
   index=rindex
   data=rdata
endif

endelse ; DATA piece 

return
end


