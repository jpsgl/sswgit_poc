;+
; Project     : EOVSA
;                  
; Name        : EOVSA_SEARCH
;               
; Purpose     : Search EOVSA catalog for files
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> files=eovsa_search(tstart,tend)
; 
; Inputs      : TSTART = UT to start search
;               TEND = UT to end search (optional)
; Outputs     : FILES = matching files
;
; Keywords    : See EOVSA objects
;                   
; History     : 22 November 2013 (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function eovsa_search,tstart,tend,_ref_extra=extra

files=''
eovsa=obj_new('eovsa',_extra=extra)
if ~obj_valid(eovsa) then return,files

files=eovsa->search(tstart,tend,_extra=extra)
obj_destroy,eovsa

return,files
end
