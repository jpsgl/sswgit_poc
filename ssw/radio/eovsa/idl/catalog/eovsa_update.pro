 ;+
; Project     : EOVSA
;                  
; Name        : EOVSA_UPDATE
;               
; Purpose     : Update EOVSA catalog
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> eovsa_update,tstart,tend
; 
; Inputs      : TSTART,TEND = UT start and end times to populate.
;
; Outputs     : None
;
; Keywords    : FILES = filenames used to populate catalog
;                   
; History     : Written, 17 April 2014 (Zarro, ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro eovsa_update,tstart,tend,files=files,count=count,_ref_extra=extra

files='' & count=0

eovsa=obj_new('eovsa_db',_extra=extra)
if ~obj_valid(eovsa) then return
if ~eovsa->write_access(_extra=extra) then return

files=eovsa_list(tstart,tend,count=count,_extra=extra)
if count gt 0 then eovsa->update,files,_extra=extra

obj_destroy,eovsa

return
end
