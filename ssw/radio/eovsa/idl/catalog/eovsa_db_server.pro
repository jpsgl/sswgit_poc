;+
; Project     : EOVSA

; Name        : EOVSA_DB_SERVER
;
; Purpose     : Return HTTP location of EOVSA Database
;
; Category    : Utility database
;
; Syntax      : IDL> server=eovsa_db_server()
;
; Inputs      : None
;
; Outputs     : URL of EOVSA Database 
;
; Keywords    : None
;
; History     : Written, 8 January 2015 (Zarro, ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

function eovsa_db_server

return,'http://sohowww.nascom.nasa.gov/solarsoft/radio/eovsa/database'

end
