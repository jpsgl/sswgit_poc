;+
; Project     : EOVSA
;
; Name        : EOVSA_SERVER
;
; Purpose     : Return HTTP location of main EOVSA file server
;
; Category    : Utility database
;
; Syntax      : IDL> server=eovsa_server()
;
; Inputs      : None
;
; Outputs     : URL of EOVSA server
;
; Keywords    : None
;
; History     : Written, 13 October 2014 (Zarro, ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-

function eovsa_server

return,'http://ovsa.njit.edu'

end
