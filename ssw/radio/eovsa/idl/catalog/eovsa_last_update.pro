 ;+
; Project     : EOVSA
;                  
; Name        : EOVSA_LAST_UPDATE
;               
; Purpose     : Return date/time of last update to EOVSA catalog
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> eovsa_last_update
; 
; Inputs      : None
;
; Outputs     : DATE = UTC date/time of last update
;
; Keywords    : SSW = check local database in SSW (def = remote)
;                   
; History     : Written, 5 October 2014 (Zarro, ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro eovsa_last_update,date,_ref_extra=extra

eovsa=obj_new('eovsa_db',_extra=extra)
eovsa->last_update,date,_extra=extra

obj_destroy,eovsa

return
end
