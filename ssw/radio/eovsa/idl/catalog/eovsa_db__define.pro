;+
; Project     :	VSO
;
; Name        :	EOVSA_DB__DEFINE
;
; Purpose     :	Object wrapper around EOVSA database software
;
; Category    :	Databases
;
; History     :	3-May-2013, Zarro (ADNET), written.
;
;-

function eovsa_db::init,_ref_extra=extra,err=err,remote=remote,age=age

;-- if local database is older than AGE days then force remote search

if ~keyword_set(remote) && is_number(age) then begin
 local_db=concat_dir(self->ssw_dir(),'eovsa.dbx')
 ltime=file_time(local_db)
 ctime=systim()
 diff=abs(anytim(ltime)-anytim(ctime))
 age_secs=age*24.*3600.
 dprint,'% age (days):',diff/24./3600. 
 if diff gt age_secs then begin
  mprint,'Local EOVSA catalog older than remote master. Searching remote catalog.'
  remote=1b
 endif
endif

zdbase=keyword_set(remote) ?  eovsa_db_server() : self->ssw_dir()
chk=self->dbase::init(err=err)
if ~chk then return,0 
self->set,dbname='eovsa',zdbase=zdbase,_extra=extra

return,1

end

;--------------------------------------------------
function eovsa_db::search,tstart,tend,_ref_extra=extra,count=count,$
         times=times,type=type,metadata=metadata

forward_function eovsa_server

files='' & times=0.d & type=''

self->list,tstart,tend,metadata,_extra=extra,count=count

data_types=['image','spectrum','lightcurve']
if count gt 0 then begin
 times=parse_time(metadata.filename,/tai,ymd=ymd,sep='')
 files=eovsa_server()+'/fits/'+ymd+'/'+metadata.filename
 stype=metadata.type
 type=strarr(count)
 for i=0,2 do begin
  chk=where(i eq stype,scount)
  if scount gt 0 then type[chk]=data_types[i]
 endfor
 files=strtrim(files,2)
 type='radio/'+type
endif

return,files

end

;--------------------------------------------------------------------
;-- update catalog with FITS header metadata

pro eovsa_db::update,files,verbose=verbose,older=older,$
                     force=force,err=err,_ref_extra=extra

err=''
if ~self->write_access(err=err) then return

verbose=keyword_set(verbose)
force=keyword_set(force)

if is_blank(files) then begin
 err='No filenames entered.'
 message,err,/info
 return
endif

zdbase=self->getprop(/zdbase)
if verbose then message,'Writing to '+zdbase,/info

nfiles=n_elements(files)
updated=0

for i=0,nfiles-1 do begin

 self->parse,files[i],metadata

;-- First check for metadata nearest file time. If none, then add it.
;-- If same, check that it differs

 window=1800.d
 time=metadata.date_obs
 tstart=time-window & tend=time+window
 self->list,tstart,tend,entries,count=count,err=err,verbose=verbose and (i eq 0)

 if count eq 0 then begin
  if verbose then message,'Adding ' +metadata.filename,/info
  self->add,metadata,err=err
  updated=updated+1
  continue
 endif

;-- How does this one differ?

 check=where(trim(metadata.filename) eq trim(entries.filename),scount)

 if scount eq 0 then begin
  if verbose then message,'Adding ' +metadata.filename,/info
  self->add,metadata,err=err
  updated=updated+1
  continue
 endif

 if scount gt 1 then begin
  message,'Duplicate file names!',/info
  continue
 endif

 if scount eq 1 then begin
  entry=entries[check]
  same=match_struct(metadata,entry,exclude=['id','cat_date','deleted'],dtag=dtag)
  if same and ~force then continue
  if verbose and ~same then begin
   message,'Metadata differences - '+arr2str(dtag),/info
  endif

;-- If current file date is older than cataloged file date, then skip
;   unless /older is set

  if ~force and ((metadata.file_date lt entry.file_date) and ~keyword_set(older)) then continue 
  metadata.id=entry.id 
  if verbose then message,'Replacing ' +metadata.filename,/info
  self->add,metadata,err=err,/replace
  if is_blank(err) then updated=updated+1
 endif

endfor

if verbose then message,trim(updated) +' database entries updated.',/info

return
end

;--------------------------------------------------------------------
;-- parse FITS file header into catalog metadata

pro eovsa_db::parse,file,metadata,err=err,header=header

hfits=obj_new('hfits')
hfits->read,file,header=header,err=err,/nodata
obj_destroy,hfits
if is_string(err) then begin
 message,err,/info
 return
endif

stc=fitshead2struct(header)
metadata=self->getprop(/meta,err=err)
if is_string(err) then return
struct_assign,stc,metadata
metadata.id=-1
metadata.date_obs=anytim2tai(stc.date_obs)
metadata.date_end=anytim2tai(stc.date_end)
metadata.filename=file_basename(file)

;-- if type is not in FITS header, get it from filename

if ~have_tag(stc,'type') then begin
 types=['_im','_sp','_(lt|li)']
 for i=0,n_elements(types)-1 do begin
  if stregex(metadate.filename,type[i],/bool,/fold) then begin
   metadata.type=i
   break
  endif
 endfor 
endif

;-- add new tags

if have_tag(metadata,'cal_flag') and ~have_tag(stc,'cal_flag') then metadata.cal_flag=-1

if have_tag(metadata,'proj_id') then begin
 metadata.proj_id=-1
 if have_tag(stc,'project_',k1) or have_tag(stc,'proj_id',k2) then begin
  if k1 gt -1 then k=k1 else k=k2
  if is_number(stc.(k)) then metadata.proj_id=fix(stc.(k)) else begin
   ids=['normal','phasecal','solpntcal']
   for i=0,n_elements(ids)-1 do begin
    if stregex(stc.(k),ids[i],/bool,/fold) then begin
     metadata.proj_id=i
     break
    endif
   endfor 
  endelse
 endif
endif

if is_url(file) then begin
 resp=sock_head(file,date=date)
endif else begin
 info=file_info(file)
 date=systim(0,info.mtime,/utc)
endelse
metadata.file_date=anytim2tai(date)

get_utc,utc
metadata.cat_date=anytim2tai(utc)

return
end 

;---------------------------------------------------------------------

function eovsa_db::ssw_dir
return,local_name('$SSW/radio/eovsa/catalog') 
end

;----------------------------------------------------------------------
;-- check if item is valid metadata

function eovsa_db::valid_meta,item

if ~is_struct(item) then return,0b
def=self->getprop(/meta,err=err)
if is_string(err) then return,0b
if ~match_struct(item,def,/tags_only) then return,0b
if ~valid_time(item.date_obs) then return,0b
check=where(item.type eq [0,1,2],count)
if count eq 0 then return,0b

return,1b

end

;---------------------------------------------------------------------

pro eovsa_db::list,tstart,tend,items,count=count,err=err,_ref_extra=extra,$
                xmin=xmin,xmax=xmax,ymin=ymin,ymax=ymax,$
                cal_flag=cal_flag

count=0l
items=-1

;-- parse search parameters

day_secs=24.d*3600.d
test=''

valid_tstart=valid_time(tstart)
valid_tend=valid_time(tend)
nearest=valid_tstart and ~valid_tend 

;-- set time search range

if valid_tstart and valid_tend then $
 test = [test,trim(anytim2tai(tstart),"(f15.3)")+' < date_obs < '+trim(anytim2tai(tend),"(f15.3)")]

;-- set coordinate search ranges

if is_number(xmin) and ~is_number(xmax) then test=[test,'xcen > '+trim(xmin)]
if is_number(xmax) and ~is_number(xmin) then test=[test,'xcen < '+trim(xmax)]
if is_number(xmin) and is_number(xmax) then test=[test,trim(xmin)+' < xcen < '+trim(xmax)]
if is_number(ymin) and ~is_number(ymax) then test=[test,'ycen > '+trim(ymin)]
if is_number(ymax) and ~is_number(ymin) then test=[test,'ycen < '+trim(ymax)]
if is_number(ymin) and is_number(ymax) then test=[test,trim(ymin)+' < ycen < '+trim(ymax)]

;-- set calibration type

if is_number(cal_flag) then test=[test,'cal_flag='+trim(cal_flag)]

;-- lookup data type

dtype=self->datatype(_extra=extra)
if is_string(dtype) then test=[test,dtype]

;-- lookup project ID

proj_id=self->proj_id(_extra=extra)
if is_string(proj_id) then test=[test,proj_id]

;-- handle three distinct cases:
;   (1) just start time entered (nearest=1)
;   (2) just start time entered (nearest=1) + additional search keywords
;   (3) start and end time entered (nearest=0) + additional search keywords


if is_string(extra) then begin
 for i=0,n_elements(extra)-1 do begin
  add_to_search=scope_varfetch(extra[i],/ref_extra)
  if is_string(add_to_search) then begin
   if is_string(test) then test=test+','+add_to_search else test=add_to_search
  endif
 endfor
endif

if is_blank(test) and ~nearest then begin
 err='No search criteria entered.'
 message,err,/info
 return
endif

;-- open the database

self->open,err=err,_extra=extra
if is_string(err) then return

if is_string(test) then begin
 test=arr2str(test,delim=',')
 if strpos(test,',') eq 0 then test=strmid(test,1,strlen(test))
endif else if nearest then test='date_obs = '+trim(anytim2tai(tstart),"(f15.3)")+'('+trim(day_secs)+')'

if self.have_deleted then test=test+',deleted=n'

dprint,'test: ',test

entries = dbfind(test,/silent,count=count)
if count eq 0 then begin
 err2='No matching entries found.'
 message,err2,/info
 self->close,_extra=extra
 return
endif

entries = entries[uniq(entries)]
count = n_elements(entries)

;-- extract the requested entries, sorted by times

entries = dbsort(entries,'date_obs')
items=self->extract(entries,err=err)

if nearest then begin
 tstart_tai=anytim2tai(tstart)
 diff=abs(items.date_obs-tstart_tai)
 ok=where(diff eq min(diff))
 ok=ok[0]
 items=items[ok]
 tend=items
 count=1
endif

self->close,_extra=extra
return
end

;------------------------------------------------------------------------------
;-- set data type

function eovsa_db::datatype,images=images,spectra=spectra,lightcurves=lightcurves

dtype='' & type=''
if keyword_set(images) then dtype=dtype+' 0'
if keyword_set(spectra) then dtype=dtype+' 1'
if keyword_set(lightcurves) then dtype=dtype+' 2'
if is_string(dtype) then begin
 dtype=trim(dtype) & dtype=str_replace(dtype,' ',',') 
 if strpos(dtype,',') gt -1 then type='type=['+dtype+']' else type='type='+dtype 
endif
return,type
end

;---------------------------------------------------------------------
;-- lookup PROJ_ID

function eovsa_db::proj_id,proj_id=proj_id,$
                normal=normal,phasecal=phasecal,solpntcal=solpntcal

pid=''
np=n_elements(proj_id)
if np gt 0 then begin
 for i=0,np-1 do begin
  ptype=proj_id[i]
  if is_number(ptype) then begin
   if fix(ptype) eq 0 then normal=1
   if fix(ptype) eq 1 then phasecal=1
   if fix(ptype) eq 2 then solpntcal=1
  endif
 endfor
endif

ptype=''
if keyword_set(normal) then ptype=ptype+' 0'
if keyword_set(phasecal) then ptype=ptype+' 1'
if keyword_set(solpntcal) then ptype=ptype+' 2'
if is_string(ptype) then begin
 ptype=trim(ptype) & ptype=str_replace(ptype,' ',',') 
 if strpos(ptype,',') gt -1 then pid='proj_id=['+ptype+']' else pid='proj_id='+ptype 
endif

return,pid
end

;-----------------------------------------------------------------------

pro eovsa_db__define

struct={eovsa_db,inherits dbase}

return & end

