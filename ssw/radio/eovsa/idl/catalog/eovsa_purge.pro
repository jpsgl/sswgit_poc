 ;+
; Project     : EOVSA
;                  
; Name        : EOVSA_PURGE
;               
; Purpose     : Purge EOVSA catalog of deleted items
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> eovsa_purge
; 
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : None
;                   
; History     : Writte, 28 July 2014 (Zarro, ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

pro eovsa_purge,_ref_extra=extra

eovsa=obj_new('eovsa_db',_extra=extra)
if ~obj_valid(eovsa) then return
eovsa->purge,_extra=extra
obj_destroy,eovsa

return
end
