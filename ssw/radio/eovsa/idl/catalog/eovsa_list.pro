;+
; Project     : EOVSA
;                  
; Name        : EOVSA_LIST
;               
; Purpose     : List EOVSA file system
;               [NB: this does a brute force searching of the EOVSA
;                directories. It doesn't search the EOVSA catalog.]
;                             
; Category    : Utility database 
;               
; Syntax      : IDL> files=eovsa_list(tstart,tend)
; 
; Inputs      : TSTART = UT to start search
;               TEND = UT to end search (optional)
; Outputs     : FILES = matching files
;
; Keywords    : Coming soon
;                   
; History     : Written, 17 April 2014 (Zarro, ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-    

function eovsa_list,tstart,tend,_ref_extra=extra

rhost=eovsa_server()
if ~have_network(rhost,_extra=extra) then return,''

s=obj_new('site')
s->setprop,rhost=rhost,topdir='fits',ext='(fits|fts)',/full
files=s->search(tstart,tend,_extra=extra)
obj_destroy,s

return,files
end
