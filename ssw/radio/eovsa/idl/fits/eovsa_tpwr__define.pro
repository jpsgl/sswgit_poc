;+
;NAME: 
; eovsa_tpwr::plot
;PURPOSE:
; Plot method for eovsa_tpwr object.
;CALLING SEQUENCE:
; obj -> plot, xtpwr=xtpwr, ytpwr=ytpwr, tpwr=tpwr, 
;		antnum=antnum, dim1_use=dim1_use,
;		specplot=specplot, plotman_obj=plotman_obj
; Any plot keywords passed in will be passed through to the plot
; routines through _extra.
;KEYWORDS:
; xtpwr = plot power in X polarization
; ytpwr = plot power in Y polarization
; tpwr = plot sum of X and Y
; antnum = the antenna number
; specplot = if set, will plot a specplot, the default is to use a
;            utplot object
; status = 1 if successful
; plotman_obj = plots the output to an interactive plotman window
; err_msg - Error message if any.  Default is ''.
;EXTRA KEYWORDS:
; dim1_use = select frequency channel for utplot
;OUTPUT:
;HISTORY:
; Fixed problem with early fits files having 2-d frequency variables,
;       jmm, 2014-10-03
; Added plotman compatability and expanded header, sjs, 2014-09-27
;	schonfsj@gmail.com
; Hacked from hsi_obs_summ_flag__plot, jmm, 2014-05-01
; 	jimm@ssl.berkeley.edu
;-
Pro eovsa_tpwr::plot, xtpwr=xtpwr, ytpwr=ytpwr, tpwr=tpwr, $
                      antnum=antnum, specplot=specplot, $
                      status=status, err_msg=err_msg, $
                      plotman_obj=plotman_obj, $
                      _extra=_extra

  status = 1
  err_msg = ''

  data = self -> get(/data)
  If(~is_struct(data)) Then Begin
     message, /info, 'No data to plot'
     Return
  Endif

  If(~keyword_set(antnum)) Then ant = 0 Else ant = long(antnum[0])-1

  If(size(data.sfreq.sfreq, /n_dim) Eq 2) Then dim1_vals = data.sfreq.sfreq[*, 0] $
  ELSE dim1_vals = data.sfreq.sfreq

  ok = where(dim1_vals Gt 0)
  If(ok[0] Ne -1) Then dim1_vals = dim1_vals[ok] $
  Else Begin
     message, /info, 'No nonzero frequencies'
     Return
  Endelse
  dim1_ids = 'Channel '+strcompress(/remove_all, string(indgen(n_elements(dim1_vals))))
  If(keyword_set(xtpwr)) Then Begin
     dname = 'X Total Power'
     tim_arr = data.xtpwr.ut
     data = reform(data.xtpwr.tsys[ant, ok, *])
  Endif Else If(keyword_set(ytpwr)) Then Begin
     dname = 'Y Total Power'
     tim_arr = data.ytpwr.ut
     data = reform(data.ytpwr.tsys[ant, ok, *])
  Endif Else Begin
     dname = 'X+Y Total Power'
     tim_arr = data.xtpwr.ut
     data = reform(data.xtpwr.tsys[ant, ok, *]+data.ytpwr.tsys[ant, ok, *])
  Endelse
  data = transpose(data) ;ntimesXnchannels
  title = 'EOVSA '+dname+' Ant: '+strcompress(/remove_all, string(ant+1))
  ytitle = dname
  times = anytim(tim_arr)
  utbase = min(times)
	
  If(keyword_set(specplot)) Then Begin
     specplot_obj = obj_new('specplot', times-utbase, data, utbase=anytim(utbase,/vms), $
	frequency=dim1_vals, ytitle='Frequency (GHz)', id=title, _extr=_extra)
     If(keyword_set(plotman_obj)) Then Begin
     	plotman_obj = plotman(input=specplot_obj, plot_type='specplot', $
		status = status, err_msg=err_msg, _extra=_extra)
     Endif Else specplot_obj -> plot, status = status, err_msg=err_msg, _extra=_extra
  Endif Else Begin
     utplot_obj = obj_new('utplot', times-utbase, data, utbase=anytim(utbase,/vms), $
	id=title, dim1_id = dim1_ids, yrange=[0.0, 1.2*max(data)])
     If(keyword_set(plotman_obj)) Then Begin
     	plotman_obj = plotman(input=utplot_obj, plot_type='utplot', $
		status = status, err_msg=err_msg, _extra=_extra)
     Endif Else utplot_obj -> plot, status = status, err_msg=err_msg, _extra=_extra
  Endelse

  If(err_msg Ne '') Then print, err_msg

  If(Not status) Then Return

  Return
End


;+
;NAME: 
; eovsa_tpwr::allplot
;PURPOSE:
; Plot method for eovsa_tpwr object. Plots the x and y spectrum for each antenna
;CALLING SEQUENCE:
; obj -> allplot, _extra=_extra
; Any plot keywords passed in will be passed through to the plot
; routines through _extra.
;KEYWORDS:
; status = 1 if successful
; err_msg - Error message if any.  Default is ''.
;OUTPUT:
;HISTORY:
; Created based on eovsa_tpwr::plot, sjs, 2014-09-27
;	schonfsj@gmail.com
;-
Pro eovsa_tpwr::allplot, status=status, err_msg=err_msg, _extra=_extra

  status = 1
  err_msg = ''

  data = self -> get(/data)
  If(~is_struct(data)) Then Begin
     message, /info, 'No data to plot'
     Return
  Endif

  If(size(data.sfreq.sfreq, /n_dim) Eq 2) Then dim1_vals = data.sfreq.sfreq[*, 0] $
  ELSE dim1_vals = data.sfreq.sfreq

  ok = where(dim1_vals Gt 0)
  If(ok[0] Ne -1) Then dim1_vals = dim1_vals[ok] $
  Else Begin
     message, /info, 'No nonzero frequencies'
     Return
  Endelse

  tim_arr = data.xtpwr.ut
  times = anytim(tim_arr)
  utbase = min(times)

  For ant=0,n_elements(data.xtpwr.tsys[*,0,0])-1 Do Begin
     xdata = transpose(reform(data.xtpwr.tsys[ant, ok, *]))
     ydata = transpose(reform(data.ytpwr.tsys[ant, ok, *]))
     xtitle = 'EOVSA X Power Ant: '+strcompress(/remove_all, string(ant+1))
     ytitle = 'EOVSA Y Power Ant: '+strcompress(/remove_all, string(ant+1))
     xspecplot_obj = obj_new('specplot', times-utbase, xdata, utbase=anytim(utbase,/vms), $
	frequency=dim1_vals, id=xtitle, ytitle='Frequency (GHz)', _extr=_extra)
     yspecplot_obj = obj_new('specplot', times-utbase, ydata, utbase=anytim(utbase,/vms), $
	frequency=dim1_vals, id=ytitle, ytitle='Frequency (GHz)', _extr=_extra)
     If ant eq 0 Then Begin
	plotman_obj = plotman(input=xspecplot_obj, plot_type='specplot', desc='Ant 1 X', $
		status = status, err_msg=err_msg, _extra=_extra)
     Endif Else Begin
	plotman_obj->new_panel,input=xspecplot_obj,plot_type='specplot',strcompress('Ant '+string(ant+1)+' X')
     Endelse
     plotman_obj->new_panel,input=yspecplot_obj,plot_type='specplot',strcompress('Ant '+string(ant+1)+' Y')
  Endfor

  If(err_msg Ne '') Then print, err_msg

  If(Not status) Then Return

  Return
End


;+
;NAME:
; eovsa_tpwr__define
;PROJECT:
; HESSI
;CATEGORY:
; Obeserving Summary
;PURPOSE:
; Defines the eovsa_tpwr object
;CALLING SEQUENCE:
; eovsa_tpwr__define
;METHODS DEFINED IN THIS CLASS:
; READ, Reads in data from file
; keywords: filename = the input file, full path please
; WRITE, Writes data to a file
; keywords: filename = the output file, full path please
; ONLY_TIME_RANGE, Keeps only the data inside the given time range for the
;                  object, resets the other parameters as necessary
; keywords: time_range = the time range, this is required
; SET, Sets values for structures and keywords
; keywords: info = Sets the info structure to the value passed in
;           data = Sets the data structure to the value passed in
;           control = Sets the control structure to the value passed in
;           _extra = passed through to FRAMEWORK::SET, this is the way
;                    to set individual parameters in the info and
;                    control structures
; GET, gets parameters and data
;           Control= the control structure (type eovsa_tpwr_control)
;                    contains, class_name, version (object), id_string,
;                    vers_info (version # for the info), vers_data (version #
;                    for the data), obs_time_interval
;           info= the info structure
;           data = the data structure,
;           time_array, xaxis pass out an array of interval Start Times.
;           _extra = passed through to FRAMEWORK::GET, this is the way
;                    to get individual parameters in the info and
;                    control structures
; GETDATA, Calls Framework Getdata, to get the data
; keywords: filename= the data file, can have the full path, or can be found
;                     in the data directory or archive
;           obs_time_interval= the time interval in UT, any ANytim format
;           Data_dir= the data directory, the default is '$HSI_DATA_ARCHIVE'
;                     if the full path is not in the filename
;           Filedb_dir= the directory containing the filedb structure,
;                       the default is '$HSI_FILEDB_ARCHIVE'
;           Class_name= the object class name, must be passed in
;           _extra=_extra, passed through to FRAMEWORK::GETDATA
;HISTORY:
; 2014-04-08, jmm, jimm@ssl.berkeley.edu, Hacked from hsi_qlook__define
;
;-
FUNCTION eovsa_tpwr::init, _extra=_extra

  ret = self -> framework::init()
  self -> set, info = {dummy: -1}
  self -> set, data = -1
  self -> set, control = {nodata: 1b}
  self -> set, need_update = 1  ;If nothing is set, nothing happens
  IF Keyword_Set( _EXTRA ) THEN self -> Set, _EXTRA = _extra
  RETURN, 1
END

PRO eovsa_tpwr::cleanup
  
  self -> framework::cleanup
  
  RETURN
END


PRO eovsa_tpwr::Read, filename=filename, quiet=quiet, _extra=_extra

;First deal with the file
  If(KEYWORD_SET(filename)) Then filex = filename[0] $
  ELSE filex = 'eovsa_tpwr.fits'

  filex0 = file_search(filex, count = bb)
  IF(bb EQ 0) THEN BEGIN
     message, /info, filex+' NOT Found, Bye...'
     self -> set, nodata = 1
     RETURN
  ENDIF
  filex = filex0[0]

;Read the file
  eovsa_read_pwrfits, filex, fits_header, mirhdr, sfreq, xtpwr, ytpwr

  If(is_struct(sfreq) && is_struct(xtpwr) && is_struct(ytpwr)) Then Begin ;there's data here
;Define the structures, and read in the info & data.
     info = {fits_header:fits_header, mirhdr:mirhdr}
     data = {sfreq:sfreq, xtpwr:xtpwr, ytpwr:ytpwr}
     self -> set, nodata = 0b
  Endif Else Begin
     If(~keyword_set(quiet)) Then $
        message, /info, 'File: '+filex+' Read Error'
     self -> set, nodata = 1b
     Return
  Endelse

;??? do not know about thie  self -> setdata, data ;?
  self -> set, data = data
  self -> set, info = info
  self -> set, need_update = 0
;And that should be it
  Return
End

PRO eovsa_tpwr::Set, info = info, data = data, _extra = _extra

;First set the structures                                                       
  IF(KEYWORD_SET(info)) THEN BEGIN
     self -> framework::set, info = info
  ENDIF
  IF(KEYWORD_SET(data)) THEN BEGIN
     IF(ptr_valid(self.data)) THEN ptr_free, self.data
     self.data = ptr_new(data)
  ENDIF
  self -> framework::set, _extra = _extra
  Return
End

Function eovsa_tpwr::Get, info = info, data = data, _extra = _extra

   IF(KEYWORD_SET(info)) THEN BEGIN
      otp = self -> framework::get(/info)
   ENDIF ELSE IF(KEYWORD_SET(data)) THEN BEGIN
      If(ptr_valid(self.data)) Then otp = *self.data $
      Else otp = -1
   ENDIF Else otp = self -> framework::get(_extra=_extra)

   Return, otp
End

PRO eovsa_tpwr::Write, filename=filename, overwrite=overwrite, _extra=_extra

;Check for data
  info = self -> get(/info)
  data = self -> get(/data)
  IF(~is_struct(data) || ~is_struct(info)) THEN BEGIN
     message, 'No data to output: ', /info
     RETURN
  ENDIF

;error checking
  If(~is_struct(info.mirhdr) || ~is_struct(data.sfreq) || ~is_struct(data.xtsys) || ~is_struct(data.ytsys)) Then Begin
     message, /info,  'No data to output: '
  Endif

;deal with the file
  IF(KEYWORD_SET(filename)) THEN filex = filename ELSE BEGIN
     date_obs0 = anytim(/ccsds, data.xtsys.ut[0])
     filex = 'eovsa_1-18GHz_sp_'+STRMID(date_obs0,0,4)+$
             STRMID(date_obs0,5,2)+STRMID(date_obs0,8,2)+$
             '_'+STRMID(date_obs0,11,2)+STRMID(date_obs0,14,2)+$
             '.fits'
  ENDELSE
  filename = filex              ;passed out

;Write the file
  FXWRITE, filename, info.fits_header
  mwrfits, info.mirhdr, filename, info.mirhdr.header
  mwrfits, data.sfreq, filename, data.sfreq.header
  mwrfits, data.xtpwr, filename, data.xtpwr.header
  mwrfits, data.ytpwr, filename, data.ytpwr.header
  
;And that should be it
  RETURN
END

;A getdata program, that will work
;currently a filename is needed to load data into the object, if no
;file is input, then whatver data is there is returned, but an
;incorrect filename will reset the data pointer to -1 and return a -1

Function eovsa_tpwr::getdata, filename = filename, _extra = _extra

  If(is_string(filename)) Then Begin
     If(is_string(file_search(filename))) Then Begin
        self -> read, filename = filename
     Endif Else Begin
        message, /info, 'No file: '+filename
        self -> set, data = -1
        self -> set, info = {bad:-1}
     Endelse
  Endif

  Return, self -> get(/data)

End

FUNCTION eovsa_tpwr::data_time_range, _extra = _extra
  data = self -> get(/data)
  control = self -> get(/control)

  tr0 = anytim([0.0d0, 0.0d0], _extra=_extra)
;Ok
  IF(control.nodata EQ 1) THEN return, tr0
  IF(is_struct(data) && is_struct(data.sfreq)) THEN return, minmax(data.sfreq.ut)
  return, tr0
END

PRO eovsa_tpwr__define

;EOVSA Total power object
  self = {eovsa_tpwr, inherits framework}

  RETURN
END
