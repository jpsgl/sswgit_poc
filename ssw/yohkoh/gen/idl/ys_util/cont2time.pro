pro cont2time, input, startt, stopt, passn, ut=ut, jst=jst, $
    numss=numss, visss=visss, duration=duration
;+
;   Name: cont2time
;
;   Purpose: return times from from contacts and/or mk_visible output files
;
;   Input Parameters:
;      input - file name or array containing information
;
;   Calling Sequence:
;      cont2time, filename, startt, stopt    
;
;   History:
;      8-apr-1995 (SLF) - written
;
;-
; initialize output parameter
startt=''
stopt=''
passn=0
visss=-1
numss=-1
duration=-1

if n_elements(input) eq 0 then begin
   message,/info, "Need input file or contacts info array..."
   return
endif

if n_elements(input) eq 1 then begin
   if file_exist(input) then contarray=rd_tfile(input,nocomment=';') else begin 
      tbeep
      message,/info,"No input file found"
      return
   endelse
endif else contarray=input

; eliminate contacts header
good=where(strlen(strcompress(contarray,/remove)) gt 50,gcnt)
contarray=contarray(good)

jst0=strsplit(contarray,'(',/head,tail=tail0)	; JST start
ut0 =strsplit(tail0,')',/head,tail=tail1)	; UT start
tail1=strtrim(tail1,2)
jst1=gt_day(jst0,/string) + ' ' + $		; JST stop
   strsplit(tail1,' ',/head,tail=tail2)	
 
pass=lonarr(n_elements(jst0))

; break remaining into coloums			 ;  -----------------------
rest=strtrim(str2cols(tail2),2) ; |Nt|Day|Tot|Pass|Visible|
;						 ;  -----------------------

duration=float(reform(rest(2,*)))		; return duration via keyword

ut1=fmt_tim(anytim2ints(ut0,offset=duration*60.)) 	 	; UT stop via timegrid/offset

visss= where(reform(rest(4,*)) ne '',vcnt)      ; flag visible orbits
passn=lonarr(n_elements(jst0))			; init passN array
numss=where(reform(rest(3,*)) ne '',ncnt)
if ncnt gt 0 then passn(numss) = $		; update if PassN available
   long(strmid(reform(rest(3,numss)),1,1))

if keyword_set(jst) then begin
   startt=jst0
   stopt =jst1
endif else begin
   startt=ut0
   stopt =ut1
endelse      

return
end
