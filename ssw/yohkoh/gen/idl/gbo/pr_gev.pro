pro pr_gev, st_date, en_date, gev_data, above=above, qstop=qstop, $
  outfile=outfile, hc=hc, quiet=quiet
;
;+
;NAME:
;	pr_gev
;PURPOSE:
;	To list the GOES events
;CALLING SEQUENCE:
;	pr_gev, '1-jan
;	pr_gev, '30-oct-91', '2-nov-91', /hc
;INPUT:
;	st_date	- The JST date to search for.  If no date is entered,
;		  the current date is used.  The year is not
;		  required, but if entered should be of the
;		  form 1-Oct-91 style.  The date should be
;		  entered in string style with date first.
;OPTIONAL INPUT:
;	en_date	- The ending date to list data for.  If no date is passed,
;		  it only searches for "st_date"
;OPTIONAL KEYWORD INPUT:
;	outfile	- If a filename is passed, the results will also
;		  be written to that file
;	hc	- If set, write the results to a temporary file, print
;		  it and delete it.
;OUTPUT:
;	The times and classes of the GOES events
;HISTORY:
;	Written 6-Jun-92 by M.Morrison
;	12-Apr-93 (MDM) - Used STATUS keyword with RD_GEV to see if any
;			  events are available for the time period
;	16-May-93 (MDM) - Modified format in order to fit on 80 column terminal
;       14-May-93 (HSH) - Added the GEV_DATA output and put a /quiet keyword
;-
;
qhc = 0
qout = 0
if (keyword_set(outfile)) then begin
    openw, lunout, outfile, /get_lun
    qout = 1
end
if (keyword_set(hc)) then begin
    openw, lunhc, 'pr_gev.temporary', /get_lun
    qhc = 1
end
;
if (n_elements(st_date) eq 0) then begin
    st_date = strmid(!stime, 0, 6)
end else begin
    ; If the date was provided, make sure to add a yr if none provided:
    day_arr = timstr2ex(st_date)
    if day_arr(6) eq 0 then day_arr(6) = fix(strmid(!stime,9,2))
    dummy = fmt_tim(day_arr, day_str, time_str)
    st_date = day_str
end
;
day_arr1 = timstr2ex(st_date)
;
if (n_elements(en_date) eq 0) then begin
    ex2int, day_arr1, time, day
    int2ex, time, day+1, day_arr2		;just end 24 hours later
end else begin
    day_arr2 = timstr2ex(en_date)
end
;
if (n_elements(above) eq 0) then above = '' else above=strupcase(above)
;
rd_gev, fmt_tim(day_arr1), fmt_tim(day_arr2), gev_data, status=status
if (status gt 0) then begin
    print, 'PR_GEV: No events available between ', fmt_tim(day_arr1), ' and ', fmt_tim(day_arr2)
    return
end
;
tit = strarr(5)
tit(0) = 'PR_GEV.PRO Run on: '+ !stime
tit(1) = '  '
tit(2) = '   Date       Time (UT)      X-Ray Opt Loca- NOAA      Peak Radio    Reports
tit(3) = '           Begin  Max   End  Class Imp tion  Region
tit(4) = '  '
        ;'15-NOV-91  22:33 22:39 22:54  X1.5 3B S13W19 6919   1100   740  1900 II,III
;
if n_elements(quiet) eq 0 then begin
  for i=0,n_elements(tit)-1 do begin
      print, tit(i)
      if (qhc) then printf, lunhc, tit(i)
      if (qout) then printf, lunout, tit(i)
  end
endif
;
ew_arr = ['E', 'W']
ns_arr = ['S', 'N']
;
qfirst = 1
for i=0,n_elements(gev_data)-1 do begin
    gev = gev_data(i)
    class = string(gev.st$class)

    qwrite = (class gt above)
    if (qwrite) then begin
	tim1 = strmid(fmt_tim(gev), 0, 16)
	tim2 = strmid(gt_time( anytim2ints(gev, off=gev.peak) , /str), 0, 5)
	tim3 = strmid(gt_time( anytim2ints(gev, off=gev.duration), /str), 0, 5)		;remove seconds portion
	if (gev.noaa eq 0) then noaa = '    ' else noaa = string(gev.noaa, format='(i4)')
	str1 = ''
	for j=0,2 do if (gev.radio(j) eq 0) then str1=str1 + '      ' else str1 = str1 + string(gev.radio(j), format='(i6)')
	location = '      '
	if (gev.location(0) ne -999) then location = string( ns_arr( gev.location(1) ge 0 ), abs(gev.location(1)), $
						ew_arr( gev.location(0) ge 0 ), abs(gev.location(0)), format='(2(a,i2.2))')
	str = string(tim1, tim2, tim3, class, string(gev.st$halpha), location, $
			noaa, str1, strtrim(gev.st$comment), format='(3(a,1x), 6(1x,a))')
        if n_elements(quiet) eq 0 then begin
	  print, str 
	  if (qhc) then printf, lunhc, str
	  if (qout) then printf, lunout, str
        endif
    end
end
;
if (qhc) then begin
    free_lun, lunhc
    dprint, 'pr_gev.temporary', /delete
end
if (qout) then free_lun, lunout
;
if (keyword_set(qstop)) then stop
end

