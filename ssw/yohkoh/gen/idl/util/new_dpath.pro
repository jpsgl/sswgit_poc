pro new_dpath, inpath, remove=remove, home=home, current=current, $
	prepend=prepend, init=init, file=file, empty=empty, loud=loud, $
	save=save
;
;+
;   Name: new_dpath
;
;   Purpose: update data paths common area 
;
;   Input Parameters:
;      inpath - string or string array of paths to add (or remove)
;
;   Keyword Parameters:
;      remove  -  switch, if set, remove instead of add
;      home    -  switch, if set, include home directory (add/remove)
;      current - switch, if set, include current directory (add/remove)
;      prepend - switch, if set, users new stuff comes first
;      init    - switch, if set, re-initiailize before adding new path(s)
;      empty   - switch, if set, empty before adding new path(s)
;		 -- NOTE:initialize=system defaults, empty=empty --
;      loud    - switch, if set, extra messages
;      file    - two options: if switch set, read file: HOME/setup.ysdpaths
;			      if string, read paths from specified file 
;      save    - switch, if set, save current dpaths to HOME/setup.ysdpaths
;      
;   Calling Examples:
;      new_dpath			   ; add current directory
;      new_dpath,/current,/home		   ; same plus home directory
;      new_dpath,'/usr/name/data'	   ; add named path/path arrray
;      new_dpath,'/usr/name/data',/prepend ; same, but first, not last
;      new_dpath,'/usr/name/data',/remove  ; remove named path/path array
;      new_dpath,/init			   ; reinitialize (system defaults)
;      new_dpath,/empty,/current	   ; empty and add current
;      new_dpath,data_paths(),/remove	   ; same as empty
;      new_dpath,/file			   ; adds whatever paths are listed
;					   ; in file: HOME/setup.ysdpaths
;      new_dpath,/save			   ; save current paths in file:
;					   ; HOME/setup.ysdpaths
;
;   Restrictions: proto-routine uses common block, not system variables 
;		  todo - add recursive (+path) feature??
;
;   Common Blocks:
;      data_paths - contains string array of data paths
;
;   History:
;      slf -  7-may-93
;      slf -  8-may-93	add file keyword and function
;      slf -  9-may-93  changed default file name to setyp.ysdpaths
;			changed default-to-add-current-path restrictions
;      slf - 20-may-93  improved save option (call uniqo,file_delete)
;      slf - 22-may-93  add doc header terminator
;      slf - 26-may-93  read of DIR_SITE_SETUP/setup.ysdpaths on init
;-
common data_paths, dpaths
common data_paths1, last_update
; 
; -------- --- check keyword definitions----------------
; fuzzy logic used here (I am in a hurry)...
kpath=''
if keyword_set(home) then kpath=[kpath,getenv('HOME')]
; data paths file option
deffile=concat_dir(getenv('HOME'),'setup.ysdpaths')
fsize=size(file)
case fsize(fsize(0)+1) of
   7: pfile=file
   0: pfile=''
   else: pfile=deffile
endcase
;
fpath=rd_tfile(pfile,/compress,/quiet,/nocomment) ; read into path array
;--------------------------------------------------------------

loud=keyword_set(loud)

;---------------------------------------------------------------
; default is to add current path under some convoluted set of circumstances...

nodef=keyword_set(save) or keyword_set(file) or keyword_set(init) or $
   keyword_set(empty) or n_elements(kpath) gt 1 or n_elements(inpath) gt 0

if keyword_set(current) or (1-nodef)  then kpath=[kpath,curdir()]
;--------------------------------------------------------------
;
; ---------------- check incoming path(s) for viability -------------
spath=size(inpath)
if spath(spath(0)+1) ne 7 and n_elements(inpath) ne 0 then begin
   message,/info,'Input paths must be string or string array, returning...'
   return	; unstructured bailout!
endif
; -----------------------------------------------------------------
;
;--- combine keyword/positional parameter paths -----
path=''
if n_elements(inpath) ne 0 then path=[path,inpath]	;postional
if n_elements(kpath)  gt 1 then path=[path,kpath(1:*)]	;keyword
if fpath(0) ne ''	   then path=[path,fpath]	;file
;--------------------------------------------
;
if n_elements(path) eq 1 then begin
   if 1-keyword_set(init) and 1-keyword_set(empty) and 1-keyword_set(save) then $
	message,/info,'No valid path specifications' 
endif else path=path(1:*)
;
;-------------- initialize data paths on request or 1st --------------
init_file=concat_dir('$DIR_SITE_SETUP','setup.ysdpaths')
if keyword_set(empty) then begin
   dpaths=''
endif else begin
   if n_elements(dpaths) eq 0 or keyword_set(init) or $
      n_elements(last_update) eq 0 then begin
      finf=file_info2(init_file,finfo)
      if n_elements(dpaths) eq 0 then dpaths=''
      if finf(0) then begin
         if n_elements(last_update) eq 0 then last_update='0'
         if last_update ne finfo.date then begin
            dpaths=rd_tfile(init_file,/nocomment) 
	    last_update=finfo.date
         endif
      endif
   endif
endelse
;----------------------------------------------------------------------

case keyword_set(remove) of
   1: begin							; remove paths
         ss=rem_elem(dpaths,path, count)
	 if count gt 0 then begin
	     if loud then message,/info,'Removing elements from data path...
             dpaths=dpaths(ss) 
	 endif else begin
	     if 1-keyword_set(empty) and loud then $
		message,/info,'All data paths removed!'
	     dpaths=''
         endelse
      endcase
   0: begin							; add paths
         if keyword_set(prepend) then dpaths=[path,dpaths] else $
		dpaths=[dpaths,path]
	 if n_elements(dpaths) gt 1 then begin		; make it uniq
            upaths=uniqo(dpaths)    			; preserve order
            dpaths=dpaths(upaths)
         endif
         valid=where(dpaths ne '',vcount)
         if vcount gt 0 then begin
            dpaths=dpaths(valid)			; purge nulls
	    if loud then message,/info,'Adding elements to data path' 
	 endif
      endcase         
endcase
pcount=0
if dpaths(0) ne '' then pcount=n_elements(dpaths)
message,/info,'Data path now contains ' + strtrim(pcount,2) + ' elements'

if keyword_set(save) then begin		
   if pfile eq '' then pfile=deffile
   file_delete,pfile				; remove existing
   file_append,pfile,data_paths()		; write new
   message,/info,'Saving data paths in file: ' + pfile
endif   	

return
end
