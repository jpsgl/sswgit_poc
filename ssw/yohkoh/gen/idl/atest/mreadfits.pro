pro mreadfits, files, info, data, strtemplate=strtemplate, nodata=nodata
;
;   Name: mreadfits
;
;   Purpose: read multiple FITs into data cube, header-> IDL structure array
;
;   Input Parameters:
;      files - fits files to read
;
;   Keyword Paramters:
;      strtemplate - template structure for read (reccommended, not required)
;      nodata - switch, if set, dont read the data (return only structures)
;
;   History:
;      21-March-1996 (S.L.Freeland) PROTOTYPE For EIT/SPARTAN originally
;      23-March-1996 (S.L.Freeland) no 'data' parameter implies /nodata
;
;   Restrictions:
;      Flat FITS (2D) - 8 and 16 bit only only for now
;-
nf=n_elements(files)

; ------------- define the template structure -----------
if not data_chk(strtemplate,/struct) then begin
   head=headfits(files(0))		; pretty fast header-only read
   fits_interp, head, strtemplate   	; no template, generate one
endif
; --------------------------------------------------------

; ---------- read all headers first --------------
info=replicate(strtemplate,nf)
for i=0,nf-1 do begin
   head=headfits(files(i))			; read header-only
   fits_interp,head,outstr,instruc=strtemplate ; convert to structure
   info(i)=outstr   
endfor
; --------------------------------------------------------

; ------ now read/populate the cube unless /nodata set -----------
nodata=keyword_set(nodata) or n_params() lt 3
if not nodata then begin
;  size output array
   dat=readfits(files(0))		; get representative image
   data=make_array(max(info.naxis1),max(info.naxis2),nf, $
        type=data_chk(dat,/type))
   i=0
   data(0,0,i)=dat			; insert 2d->3d
   while i lt (nf-1) do begin
      i=i+1
      dat=readfits(files(i))		; get representative image
      data(0,0,i)=dat			; insert 2d->3d
   endwhile
endif
; ----------------------------------------------------------------

return
end
