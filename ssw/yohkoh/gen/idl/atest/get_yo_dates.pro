function get_yo_dates, index, launch=launch, entrance=entrance, 	$
		pointing=pointing, moon=moon, mercury=mercury,		$
		safehold=safehold, value=value, verbose=verbose, 	$
		header=header, bakeout=bakeout
;+
; NAME:
;   get_yo_dates
; PURPOSE:
;   Return significant Yohkoh dates.
; CALLING SEQUENCE:
;   Launch = get_yo_dates(/launch)	; Return launch date
;   SXT_ent= get_yo_dates(/entrance)	; Date SXT entrance filter failed
;   SXT_ent= get_yo_dates(/entr,/verbos); Provide additional information
;   SXT_ent= get_yo_dates(/entr,/ver,/head); Provide additional information with header
;   SXT_ent= get_yo_dates(/entr,/value)	; Provide values of entrance filter 
;   SC_pnt = get_yo_dates(/pointing)	; Dates of S/C pointing changes
;   SC_pnt = get_yo_dates(/poi,verbose)	; Provide additional information
;   SC_pnt = get_yo_dates(/poi,/val)	; Return the values
;   moon   = get_yo_dates(moon=1)	; Get first eclipse date
;   mercury= get_yo_dates(/mercury)	; Get the mercury date
;   bakeouts=get_yo_dates(/bakeout)     ; bakeout (ranges)
;   bakeouts=get_yo_dates(/bakeout,/header,/verbos) ; additional info
;   SC_pnt = get_yo_dates(index,/point)	; Return nominal pointing time of index
;
; OPTIONAL INPUTS:
;   index	= Standard Yohkoh index or time in any Yohkoh format
; OPTIONAL INPUT KEYWORDS:
;   launch	= Set to return launch date
;   entrance	= Set to return dates when SXT entrance filter transmission changed
;		  Value returned the fractional open area
;   safehold	= Set ot return the dates when SXT was in safehold
;   pointing	= Set to return dates of Yohkoh spacecraft pointing
;
;   verbose	= Set to return additional information as a string vector
;   value	= Set to return the associated value (rather than the date)
;   header	= If verbose is set, return a header as first element of output 
;			string vector
;   bakeout     = return times of SXT CCD bakeouts (from file)
;    
;
; RESTRICTIONS:
;   The keywords launch, entrance and pointing are mutually exclusive.
; MODIFICATION HISTORY:
;   25-jan-93, J. R. Lemen LPARL, Written
;   22-aug-95, S. L. Freeland - second entrance filter failure
;   13-nov-95, JRL, Added safehold dates; Updated Moon (eclipse) dates
;   29-aug-96, S.L. Freeland  - third entrance filter failterwq!
;    1-oct-97, S.L. Freeland - add BAKEOUT keyword and function
;    3-oct-97, S.L. Freeland - fine tune 3rd ent failure per LWA
;    2-feb-98, S.L.Freeeland - fourth ent filt failure (ESTIMATE!)
;    2-feb-99, S.L.Freeland  - fifth ent filter failure (ESTIMATE!)
;   19-feb-99, P.G.Shirts    - changed 1st ent. failure date from:
;                              13-NOV-92  16:50:00 to 13-NOV-92  18:00:00
;                              5th failure time to 30-Jan-99  22:03:37
;   24-feb-99, P.G.Shirts    - changed fifth ent. failure date from:
;                              30-Jan-99  22:03:37 to 30-JAN-99 23:17
;   27-feb-99, P.G.Shirts    - changed 16 August 1995 filter failure 
;                              time from 08:21:02 to 08:04:20, per Acton
;    5-May-99, S.L.Freeland  - declare leak era ~ VERY APPROX 18-apr-99
;    4-March-00, Zarro       - fixed IDL 5.3 syntax error
;   17-May-00, B.N. Handy    - Implement pointing changes.  Needs more
;                              history, but this will enable my QUICK
;                              page OP_TERM_SCORE updates.
;   13-Aug-2001, S.L.Freeland - add/modify entrance filter info(1999)
;    5-nov-2002, S.L.Freeland - add end of mission (last sxt image+1sec)
;                               to entrance filter epochs
;-
;
on_error,2		; Return to caller

; ----------- Check the input parameters -----------------

ok = keyword_set(pointing) + keyword_set(launch) + keyword_set(entrance) +	$
     keyword_set(mercury)  + keyword_set(moon)   + keyword_set(safehold)

ok=ok +  keyword_set(bakeout)
if (ok eq 0) then begin
    doc_library,'get_yo_dates'
    message,'You must supply at least one keyword parameter'
endif

if ok gt 1 then begin
    message,'/pointing, /launch, /entrance, /mercury, /moon',/cont
    message,' are mutually exclusive'
endif

ok = keyword_set(value) + keyword_set(verbose)
if ok gt 1 then message,'/value and /verbose are mutually exclusive'

; ---------------  Set up the data base  ---------------------------
launch_date = '30-Aug-91 10:30:00'		; Yohkoh Launch date
merc_date   = ' 6-nov-93 03:07:23'		; First Contact time 
merc_date   = [merc_date, ' 6-nov-93 03:13:48',' 6-nov-93 03:59:41']	; 2nd and central point
merc_date   = [merc_date, ' 6-nov-93 04:40:03',' 6-nov-93 04:45:24']	; fourth, fifth
merc_date   = fmt_tim(merc_date)

moon_date   = ['13-Nov-93 20:54','24-Nov-95 03:25']	; This is an incomplete list


; ---- Dates of entrance filter changes ----
E_times = [launch_date, fmt_tim('13-Nov-92 18:00'),     $
;                       fmt_tim('13-Nov-92 16:50'),     $ ; per L. Acton
                        fmt_tim('16-Aug-95  08:04:20'), $ ; per L. Acton
                        fmt_tim('24-Aug-96  07:00:00'), $
                        fmt_tim('24-Jan-98  00:00:00'), $ ; ESTIMATE
 			fmt_tim('30-Jan-99  23:17'),    $ ; Loren Acton 
                        fmt_tim('12-MAR-99  02:00'), $ ; per L. Acton
                        fmt_tim('20-Apr-99  19:02'),    $ ; per L. Acton
                        fmt_tim('14-DEC-01  21:12:16')]  ; last SXT + 1sec
ent_val = [0., 1./12, 2./12,3./12, 4./12, 5./12., 6/12.,7/12.,7/12.]		; Open fraction of entrance filter

; ---- Dates of Spacecraft safeholds ------

;		 ----- start -----   ------ stop ------
safe_date   = ['24-Oct-95 07:29:20','28-Oct-95 05:00:47']

; ---- Dates of Spacecraft pointing changes ------
point_dates = ['31-Jul-99 12:00',  $         ; Not Complete!
               '11-Feb-00 12:00'] 


; ---------------  Return the requested information ----------------

case 1 of

keyword_set(launch):   	begin
			   outval = launch_date
			   if keyword_set(verbose) then begin
			      outval = ['Yohkoh launched on '+outval]
			      if keyword_set(header) then outval = 	$
					['Yohkoh launched from Kagoshima Space Center',outval]
			   endif
			end
keyword_set(pointing): 	begin
                          outval = point_dates
			  message,/info,'/pointing history not complete'
			endcase
keyword_set(entrance):  begin
			  if n_elements(index) eq 0 then begin
				times = E_times(0:n_elements(E_times)-1)
				vvv = ent_val
			  endif else begin
				times = anytim2ints(index)
				vvv = fltarr(n_elements(times))
				for i=0,n_elements(E_times)-1 do begin
				   ss = sel_timrange(times, E_times(i), E_times(i+1), /between)
				   if ss(0) gt -1 then vvv(ss) = ent_val(i)
				endfor
			  endelse
			  if keyword_set(verbose) then begin
				outval = strarr(n_elements(times))
				for i=0,n_elements(times)-1 do 	$
				  outval(i) = string(fmt_tim(times(i)),vvv(i),format='(a,f10.4)')
				if keyword_set(header) then outval = ['  Date               Open Fraction of SXT Entrance Filter',outval]
			  endif else if keyword_set(value) then begin
				  outval = vvv	
				  if n_elements(vvv) eq 1 then outval = outval(0)
			  endif else outval = fmt_tim(times)
			end
keyword_set(mercury):  	begin
			   outval = merc_date
			   if keyword_set(verbose) then begin
				outval = 'First  contact ' + merc_date(0)
				outval = [outval, 'Second contact '+merc_date(1)]
				outval = [outval, 'Central point  '+merc_date(2)]
				outval = [outval, 'Third  contact '+merc_date(3)]
				outval = [outval, 'Fourth contact '+merc_date(4)]
			   endif
			end
keyword_set(moon):	outval = moon_date
keyword_set(safehold):  begin
			   outval = safe_date
			   if keyword_set(verbose) then begin
				txt = ['Start time: ','Stop time:  ']
				for i=0,n_elements(safe_date)-1 do $
					outval(i) = txt(i mod 2)+outval(i)
			   endif
			   if keyword_set(header) then outval = $
					['Spacecraft safehold dates:', outval]
			end
keyword_set(bakeout) or keyword_set(warm): begin
   outval=''
   bakefile=concat_dir('$DIR_SXT_DOC','sxt_ccdbakeouts.dat')
   if not file_exist(bakefile) then message,/info, $
      "No bakeout file: " + bakefile + " found..." else begin
      data=rd_tfile(bakefile,nocom=([';',''])(keyword_set(header)))
      cols=str2cols(data,ncols=5,colpos=colpos)
      if keyword_set(verbose)  or keyword_set(header) then outval=data else $
         outval=strmid(data,0,colpos(3))
      endelse
   end
endcase 

return,outval
end
