pro mk_sdc, infil, outfil, week=week, run_time=run_time
;
;+
;NAME:
;	mk_sdc
;PURPOSE:
;	Generate the SXT Dark Current SDA image (SDC) file from the
;	SFR files of all unique (exposure and resolution) dark current 
;	images excluding SAA and non-100% images
;OPTONAL INPUT:
;	infil	- A list of the SFR file names that were used to make
;		  the SDL file.  If this parameter is not passed, the
;		  program will use FINDDIR to get the directory for that
;		  week of data and then use FINDFILE.
;KEYWORD INPUT:
;	week	- a string with the year/week if the format
;		  91_49 where 91 is the year, 49 is the week
;HISTORY:
;	Written 20-Apr-92 by M.Morrison
;Ver 1.11 26-May-92 (MDM) - Changed to write a temporary file and then
;                           move it to on-line after finished generating it
;          8-Jul-92 (MDM) - added "outfil"
;	  13-May-93 (MDM) - Changed GT_EXPDUR call to have /NOCORR to not
;			    correct for the shutter error
;			  - Added check for upper/lower half image (for FR)
;Ver 1.12 16-May-93 (MDM) - Patch to exclude BLS dark images taken "11-APR-93"
;			    which were "Qrtr Dark DPE=11 (38.0 )"
;	  18-Aug-93 (MDM) - Changed to use DIR_SXT_SDC and DIR_SXT_SDL
;Ver 1.13 19-Aug-93 (MDM) - Added check to avoid the SLS images which are
;			    QR, DPE=2, NaBan images which have scatter light
;			    in them.
;Ver 1.20  4-Aug-94 (MDM) - Modified to exclude dark images which have staturated
;			    pixels
;Ver 1.21  7-apr-95 (SLF) - avoid frames with fill data (show 100% "good")
;Ver 1.22  6-Jul-95 (MDM) - Added check that the image is not a patrol image
;			  - Updated ProgVerNo (it was not for Ver 1.21)
;-
;
start_time = systime(1)
run_time = 0.
;
progVerNo = 1.220*1000
progName = 'MK_SDC.PRO'
;
if (n_elements(week) eq 0) then begin
    if (infil(0) eq '') then return		;no SFR files found
    rd_roadmap, infil, roadmap
    tarr = anytim2ex(roadmap(0))
    week = ex2week(tarr)
    week = string(tarr(6), week, format="(i2.2, '_', i2.2)")
end
;
weekid = string(week, fix(progverno/1000), format="(a, 'a.', i2.2)")
sdl_infil    = concat_dir('$DIR_SXT_SDL', 'sdl'+weekid)
outfil       = concat_dir('$DIR_REFORMAT_SC', 'sdc'+weekid)
outfil_final = concat_dir('$DIR_SXT_SDC', 'sdc'+weekid)
rd_sdl, xx, yy, sdl_index, sdl_data, infil=sdl_infil
if (n_elements(sdl_data) eq 0) then begin
    message, 'No SDL file (so assume there are no dark current images', /info
    return
end
;
if (n_elements(infil) eq 0) then begin
    indir_arr = [finddir(week+'a'), finddir(week+'b')]
    infil = file_list(indir_arr, 'sfr*')
end
;
rd_roadmap, infil, roadmap
rd_fheader, infil(0), fheader_in
gen_struct, file_header=fhead
fhead.refverno = fheader_in.refverno
fhead.ref2verno = fheader_in.ref2verno
;
;
;----- Diagnostic variables
;
qprint = 1
qdebug = 1
;
;---------------------------------------- General Stuff
;
ref_struct, ref
lunout = 2	;output log
sxt_struct, sxt_version=sxt_version, sxt_darklog=sxt_darklog
;
rsiz = ref.SXT_Fil_Rec_Siz
open_da_file, lunout, '', '', outfil, rsiz, bytout, fpoint, /nolower

maxsamps = 0
;
;v = gt_res(sdl_index) + sdl_index.sxt.explevmode*256
v = sdl_index.sxt.corner_cmd(1) + gt_res(sdl_index)*1024L + sdl_index.sxt.explevmode*2048L	;changed 13-May-93
vuniq = v( uniq(v, sort(v)) )
rbm = sdl_index.gen.rbm_status
act_expdur = gt_expdur(sdl_index, /nocorr)	;msec
exp_expdur = gt_dpe(sdl_index, /conv)		;msec
exp_expdur = fix(exp_expdur/8.0+1)*8.0		;there is a 8 millisec granularity in exposure duration because of Line Sync
err_expdur = (act_expdur-exp_expdur)/exp_expdur
get_sdl_info, sdl_index, sdl_data, act_avg, act_dev, exp_avg=exp_avg
err_avg    = (act_avg-exp_avg)/exp_avg
temp = gt_temp_ccd(sdl_index)
err_temp = abs(temp-(-21))
;
for iuniq=0,n_elements(vuniq)-1 do begin
    ss0 = where(v eq vuniq(iuniq))
    tit = gt_res(sdl_index(ss0(0)), /str) + gt_dpe(sdl_index(ss0(0)), /conv, space=4) + ' msec ' + $
			'StLin:' + string(sdl_index(ss0(0)).sxt.corner_cmd(1), format='(i4)') + $
			' (' + strtrim(n_elements(ss0),2) + ' images) '

    good = bytarr(n_elements(sdl_index))
    ;sel = where((sdl_index.sxt.percentd eq 255) and (v eq vuniq(iuniq)) and (abs(err_expdur) lt 0.05) and (err_temp lt 1.5))

    sel = where((sdl_index.sxt.percentd eq 255) and (v eq vuniq(iuniq)) and $
		(abs(err_expdur) lt 0.05) and (err_temp lt 1.5) and (sdl_index.sxt.percentover le 5) and $
		(sdl_data.hist(170) lt gt_shape_cmd(sdl_index,/x)) and $
		(gt_pfi_ffi(sdl_index) ne 3) )
;   slf, 7-apr-95 - avoid frames with fill data ( 'AA'hex = 170 dec)

    if (sel(0) ne -1) then good(sel) = 1
    ;
    dummy = where( sel_timrange(sdl_index, '11-Apr-93', '18-Apr-93', /between,/bool) and (gt_dpe(sdl_index) eq 11)) ;MDM 16-May-93
    if (dummy(0) ne -1) then good(dummy) = 0	;do not use those image
    dummy = where( sel_timrange(sdl_index, '17-Nov-92', '30-dec-99', /between,/bool) and (gt_filta(sdl_index) eq 2)) ;MDM 19-Aug-93
    if (dummy(0) ne -1) then good(dummy) = 0	;do not use those image
    ;
    ss = where(good and (rbm eq 0), count)
    if (count eq 0) then begin
	if (qdebug) then print, 'No good images when looking at RBM flag - now expanding criteria to look at average'
	ss = where(good and (abs(err_avg) lt 0.05), count)
    end

    if (count eq 0) then begin
	tit2 = 'No good exposures for this selection'
	;stop
    end else begin
	tit2 = strtrim(n_elements(ss),2) + ' good exposures for this selection'

	ss2 = tim2dset(roadmap, sdl_index(ss(0)))
	rd_xda, infil, ss2(0), index, data, roadmap	;read the first image only

	rdwrt, 'W', lunout, bytout, rsiz, index, 1
	rdwrt, 'W', lunout, bytout, rsiz, data, 1
	maxsamps = max([maxsamps, get_nbytes(data)])
    end
    if (qdebug) then print, tit, tit2
end

;-------------------- Write out pointer and file header

if (maxsamps gt 0) then begin
    wrtSXTMap, lunout, fpoint, bytout, fhead, rsiz
    file_id = weekid
    wrt1p_fh, lunout, fpoint, fhead, rsiz, progName, progVerNo, file_id, 'SDC', 'SXT'
    close, lunout
end else begin
    message, 'No good dark images found', /info
    close, lunout
    spawn, 'rm -f ' + outfil
end
;
if (n_elements(outfil_final) ne 0) then begin
    cmd = 'rm -f ' + outfil_final               ;remove any copy of the file that might already exist
    spawn, cmd
    cmd = 'mv ' + outfil + ' ' + outfil_final
    spawn, cmd
    outfil = outfil_final
end
;
end_time = systime(1)
run_time = (end_time-start_time)/60.
print, 'MK_SDC took', run_time, ' minutes to run'
;
end
