;+

function hxt_autoimg,files, iindex_out, idata_out, $
                     counts_per_interval = cpi, $
                     accum_chan = accum_chan, $
                     channels = channels, $
                     n_pix = n_pix, $
                     expfac = expfac, $
                     xy=xy, $
                     hxi_directory = hxi_directory, $
                     psout=psout, $
                     error=ierror_out, $
                     pixons=pixons, $
                     sensitivity=sensitivity, $
                     resolution=resolution, $
                     pixon_sizes=pixon_sizes, $
                     pixon_map=ipmap_out, $
                     flare_list=flare_list, $
                     trange=trange, $
                     btrange=btrange, $
                     ss=ss, $
                     lambda_max=lambda_max, $
                     nolook=nolook, notty=notty,_extra=_extra

;NAME:
;     HXT_AUTOIMG
;PURPOSE:
;     Generate HXT images automatically
;CATEGORY:
;CALLING SEQUENCE:
;     hxi_files = hxt_autoimg(files, index_out, data_out)
;INPUTS:
;     files = string array of hda file names to process.  Should include
;             the full path to the file.
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS:
;    hxi_directory = path where the HXI files should be saved
;    counts_per_interval = attempt to get this many counts per time interval
;                          in channle accum_chan.  Default = 1000.
;    accum_chan = channel to do the accumulation in (default = LO channel)
;                 (LO=0, M1=1, M2=2, HI=3)
;    channels = vector of channel numbers to image.  Default = [0,1] 
;    n_pix = size of image.  Default = 64
;    expfac = image scale expansion factor.  Default = 1.0 (SXT size pixels)
;    error = returns with error estimate on the images.
;    /psout =  write a .ps file summarizing the image selections.  The
;            summary is output to the current device by default.
;    /pixons = Use pixon reconstruction.  Very slow!
;    flare_list = list of flares to analyze.  This is only relevant if there
;                 are multiple flares in a file.  To analyze just the first
;                 flare in the file use flare_list=[0], the second [1], the
;                 first and the second, [0,1], etc.
;    trange = time range to analyze.  This keyword overrides the automatic
;             selection.  There should be an even number of elements.  Each
;             pair gives one time range.
;    btrange = the time of the background suptraction.  overides the default.
;              must be a 2 element array giving the start and end time.
;    ss = index array which selects a subset of the data elements in the 
;         specified file.  Only works for a single file.
;    nolook = do not display intermediate images.  Also see /notty for pixons.
;    *** PIXON ONLY KEYWORDS:
;    pixon_map = returns with the pixon_maps for the images
;    pixon_sizes = list of pixon sizes to use
;    resolution = size of smallest pixon
;    sensitivity = sensitivity of pixon map to weak sources (0.0 = most
;                  sensitve, 3.0 is rather insensitive to weak sources)
;    /notty = do not display images
;    *** MEM ONLY KEYWORDS
;    lambda_max = max value of lambda allowed
;OUTPUTS:
;     hxi_files = string array of hxi file names written
;     index_out = HXI index for the output images
;     data_out = reconstructed images
;COMMON BLOCKS:
;SIDE EFFECTS:
;     Writes hxi files.
;RESTRICTIONS:
;     The background interval is always calculated from the LO lightcurve.
;PROCEDURE:
;     Uses MEM image reconstruction and outputs HXI files with the
;     reconstructed images.
;
;     The result is written to an hxi file and a summary of the image times
;     is written to a postscript file.
;MODIFICATION HISTORY:
;     T. Metcalf 1997-06-18
;     T. Metcalf 1997-06-19  Added ps output.
;     T. Metcalf 1997-06-20  Compute background from LO rather than M1.
;                            Added night, fms, and saa checks
;     T. Metcalf 1997-12-19  Added a work around for a background which is 
;                            at the start or end of flare mode.  Added a few
;                            more pixon keywords.
;     T. Metcalf 1999-Feb-22 Added trange keyword.
;     T. Metcalf 1999-Jun-07 Added ss keyword
;     T. Metcalf 1999-Jun-28 Protected the output index and data to avoid
;                            structure conflicts.  Also catch errors when
;                            using file_list to look for a matching spr file.
;     T. Metcalf 1999-Jul-01 The previous fix did not work with files w/o
;                            flare mode.  Fixed this bug.
;     T. Metcalf 1999-Oct-27 Added the btrange keyword to override the default
;                            background times.
;     T. Metcalf 2001-Aug-28 Added nolook and notty keywords.
;     T. Metcalf 2003-Jan-23 Added lambda_max and _extra keywords.
;                            Fixed a couple of small bugs which
;                            cropped up for (1) a very short flare
;                            interval and (2) multiple files indexing
;                            bug.  Both caused the code to crash but
;                            did not produce erroneous results.
;-

   nfiles = n_elements(files)

   expfac = 1.0
   if NOT keyword_set(n_pix) then n_pix = 64
   if n_elements(channels) LE 0 then channels = [0,1]
   if NOT keyword_set(cpi) then cpi = 1000.
   if n_elements(accum_chan) LE 0 then accum_chan = 0
   if n_elements(hxi_directory) LE 0 then hxi_directory = '' $
   else begin
      if strmid(hxi_directory,strlen(hxi_directory)-1,1) NE '/' then $
         hxi_directory = hxi_directory + '/'
   endelse

   out = ['junk']

   for ifile = 0L,nfiles-1L do begin

      if NOT keyword_set(ss) OR nfiles NE 1 then ss = -1
      rd_xda,files(ifile),ss,index,data

      nindex = n_elements(index)

      dpmode = gt_dp_mode(index)
      tim2orbit, index, tim2night=tim2night, tim2fms=tim2fms, saa=saa
      flare = where((dpmode EQ 9) AND $            ; Flare mode?
                    (tim2night GT 0.25) AND $      ; Not too close to night
                    (tim2fms GT 0.25) AND $        ; Not too close to fms
                    (NOT saa),nflare)              ; Not in SAA

      if nflare LE 0 then message,/info,'No flare mode in '+files(ifile) $
      else begin

         iflare = squeeze(dpmode EQ 9,/nosort,/index)
         niflare = n_elements(iflare)
         flare_range  = [-1L]
         nflare_range = 0L
         for i=0L,niflare-1L do begin    ; Look for multiple flares
            if dpmode(iflare(i)) EQ 9 then begin
               flare_range = [flare_range,iflare(i)]
               if i LT niflare-1L then $
                  flare_range = [flare_range,iflare(i+1)-1] $
               else $
                  flare_range = [flare_range,nindex-1]
               nflare_range = nflare_range + 1L
            endif
         endfor
         flare_range = flare_range(1:*)

         if n_elements(flare_list) LE 0 then flare_list = indgen(nflare_range)

         for ijflare = 0L,n_elements(flare_list)-1L do begin

            jflare = flare_list(ijflare)

            flare1 = flare_range(2L*jflare)
            flare2 = flare_range(2L*jflare+1)

            bckoffset = 3

            if flare2-flare1 LT bckoffset*2+1 then begin
               message,/info,strcompress('WARNING: Skipping '+ $
                  string(ijflare)+' due to very short flare interval')
               continue
            endif

            ; Must only pass flare mode data to auto_bck_find when using the
            ; LO channel since M1 and higher are zero outside of flare mode.
            ; If the LO channel is used outside of flare mode, the background
            ; may artificially be zero for M1, M2, and HI.


            nbckflare = flare2-bckoffset-flare1-bckoffset+1
            ib1 = (flare1+bckoffset)<(flare2-bckoffset)
            ib2 = (flare2-bckoffset)>(flare1+bckoffset)
            if keyword_set(btrange) then begin
               bt1 = int2secarr(index(ib1:ib2),btrange(0))
               bt2 = int2secarr(index(ib1:ib2),btrange(1))
               ssbck = where(bt1 GE 0 AND bt2 LE 0)
               if ssbck(0) LT 0 then $
                  message,'ERROR: background times must be in flare mode'
               ssbck = [ssbck(0),ssbck(n_elements(ssbck)-1)]
            endif else begin
               ratio_f = auto_bck_find(index(ib1:ib2), $
                           index(ib1:ib2).hxt.sum_l, $
                           ssbck,/quiet,dtb_min=4.0, $
                           /subscripts)
            endelse
            if ssbck(1)-ssbck(0) LE 1 then begin  ; Must be at least 3 points
               if ssbck(1) EQ nbckflare-1 then ssbck(0)=ssbck(0)-1 $
               else ssbck(1) = ssbck(1)+1
            endif
            bck_trange = fmt_tim((index(ib1:ib2))(ssbck),/msec)
            print,'Background: ',bck_trange

            ;ratio_f = auto_bck_find(index(flare1+3:flare2-3),  $
            ;                        index(flare1+3:flare2-3).hxt.sum_l, $
            ;                        bck_trange,/quiet)
            ;bck_trange = fmt_tim(bck_trange)

            case accum_chan of
               0: lcflare = index(flare1:flare2).hxt.sum_l
               1: lcflare = index(flare1:flare2).hxt.sum_m1
               2: lcflare = index(flare1:flare2).hxt.sum_m2
               3: lcflare = index(flare1:flare2).hxt.sum_h
               else: message,'accum_chan is out of range'
            endcase

            peak = max(lcflare,ipeak)
            peak_time = fmt_tim(index(flare1+ipeak))

            nlcflare = n_elements(lcflare)

            lo1 = 0L
            hi1 = 0L

            REPEAT begin    ; Find central time range around peak
               lo1 = (lo1 + 1L) < ipeak
               hi1 = (hi1 + 1L) < (nlcflare-1-ipeak)
               no_more_flare_mode = ((ipeak-lo1) LE 0) AND $
                                    ((ipeak+hi1) GE (nlcflare-1))
               counts = total(lcflare(ipeak-lo1:ipeak+hi1))
            endrep UNTIL (counts GE cpi) OR no_more_flare_mode

            lo1 = ipeak-lo1
            hi1 = ipeak+hi1
            img_trange=[fmt_tim(index(flare1+lo1),/msec), $
                         fmt_tim(index(flare1+hi1),/msec)]
            cnts = [counts]
            nimg_trange = 1L

            lo2 = lo1
            REPEAT begin    ; Find time ranges before peak
               lo2 = (lo2 - 1L)>0L
               counts = total(lcflare(lo2:lo1))
               if (counts GE cpi) OR (lo2 LE 0 AND lo2 NE lo1) then begin
                  img_trange=[[fmt_tim(index(flare1+lo2),/msec), $
                               fmt_tim(index(flare1+lo1),/msec)],[img_trange]]
                  lo1=lo2
                  nimg_trange = nimg_trange + 1L
                  cnts = [counts,cnts]
               endif
            endrep UNTIL (lo2 LE 0)

            hi2 = hi1
            REPEAT begin    ; Find time ranges after peak
               hi2 = (hi2 + 1L)<(nlcflare-1)
               counts = total(lcflare(hi1:hi2))
               if (counts GE cpi) OR (hi2 GE (nlcflare-1) AND hi2 NE hi1) then begin
                  img_trange=[[img_trange],[fmt_tim(index(flare1+hi1),/msec), $
                                          fmt_tim(index(flare1+hi2),/msec)]]
                  hi1=hi2
                  nimg_trange = nimg_trange + 1L
                  cnts = [cnts,counts]
               endif
            endrep UNTIL (hi2 GE (nlcflare-1))

            if keyword_set(trange) then begin
               img_trange = trange
               nimg_trange = n_elements(img_trange)/2L
               message,/info,'Using user specified time ranges'
            endif

            ftime = time2file(img_trange(0),delimit='.',/year2digit)
            outfile = hxi_directory + 'hxi' + ftime
            psfile = hxi_directory + 'hxi' + ftime + '.ps'
            out = [out,outfile]
            print,outfile & print


            print
            if keyword_set(psout) then begin
               old_device = !d.name
               set_plot,'ps'
               device,file=psfile,bits_per=4
            endif
            utplot,index(flare1:flare2),index(flare1:flare2).hxt.sum_l, $
               title='Automatic Interval Selection'
            outplot,index(flare1:flare2),index(flare1:flare2).hxt.sum_m1
            ;outplot,index(flare1:flare2),index(flare1:flare2).hxt.sum_m2
            ;outplot,index(flare1:flare2),index(flare1:flare2).hxt.sum_h
            outplot,[bck_trange(0),bck_trange(0)], $
               2*[0,max(index.hxt.sum_m1)],thick=2,line=1
            outplot,[bck_trange(1),bck_trange(1)], $
               2*[0,max(index.hxt.sum_m1)],thick=2,line=1

            for i=0L,nimg_trange-1 do begin
               print,img_trange(2*i),' ',img_trange(2*i+1),' ',cnts(i)
               outplot,[img_trange(2*i),img_trange(2*i)], $
                       2*[0,max(index.hxt.sum_m1)],thick=2,line=0
               outplot,[img_trange(2*i+1),img_trange(2*i+1)], $
                       2*[0,max(index.hxt.sum_m1)],thick=2,line=0
            endfor
            if keyword_set(psout) then begin
               device,/close
               set_plot,old_device
            endif
            print

            status = -1
            if n_elements(xy) EQ 2 then begin
               message,/info,'Using supplied HXT coordinates'+string(xy[0])+' '+string(xy[1])
               status = 1
            endif
            if status(0) NE 1 then $
               xy = get_hxt_pos(index(flare1+ipeak),/nouser,status=status)
            if status(0) NE 1 then begin
               status = -1
               ; If get_hxt_pos didn't work, try to find an spr file
               hxtfile = files(ifile)
               hdapos = rstrpos(hxtfile,'hda')
               ; Assume spr and hda files are in the same directory
               sxtfile1 = hxtfile
               strput,sxtfile1,'spr',hdapos
               print,sxtfile1 & print
               sxtfile = (findfile(sxtfile1))(0)
               if sxtfile EQ '' then begin  ; Didn't work ... Search data paths
                  sarr = splitstr(sxtfile1,'/')
                  sxtfile = sarr(n_elements(sarr)-1)
                  catch,Error_Status
                  dp1 = ''
                  if Error_Status EQ 0 then dp1 = data_paths()
                  catch,/cancel
                  catch,Error_Status
                  dp2 = ''
                  if Error_Status EQ 0 then dp2 = data_paths2()
                  sxtfile = file_list([dp1,dp2],sxtfile)
               endif
               if sxtfile NE '' then begin
                  rd_xda,sxtfile,-1,sxtindex,/nodata
                  sxt_flare_mode = where(gt_dp_mode(sxtindex) EQ 9,nsxtfmode)
                  if nsxtfmode GT 0 then begin
                     junk=min(abs(int2secarr(sxtindex(sxt_flare_mode),peak_time)),isxt)
                     if sxt_flare_mode(isxt) EQ 0 then isxt=isxt+1
                     if gt_dp_mode(sxtindex(sxt_flare_mode(isxt)-1)) NE 9 then $
                        isxt=isxt+1  ; Don't use the first flare mode image
                     xy = gt_center(sxtindex(sxt_flare_mode(isxt)),/hxt)
                     status = 1
                  endif
               endif
               ; If spr didn't work, try GOES list and sxt database
               if status(0) LT 0 then $
                  xy = get_hxt_pos(index(flare1+ipeak),/nouser,/get_sxt, $
                                   status=status)
               if status(0) LT 0 then $
                  xy = get_hxt_pos(index(flare1+ipeak),/nouser,/get_goes, $
                                   status=status)
               if status(0) LT 0 then $
                  message,/info,"Can't get the HXT flare position: "+files(ifile)
            endif

            if status(0) GE 0 then begin   ; Got HXT coordinates?
               print,'HXT coordinates for ',peak_time,' flare: ',xy & print
               junk = where(gt_dp_mode(index(flare1:flare2)) EQ 9,nflr)
               nframes = nflr*4.0 + (flare2-flare1+1-nflr)/2.0

               hxt_multimg, index, data, i_out, d_out, patterns, $
                            bck_trange=bck_trange, img_trange=img_trange, $
                            xy=xy, expfac=expfac, channels=channels, $ 
                            n_pix=n_pix, outfile=outfile, $
                            /sato, /calc_btot, /nonorm, /new_mod_patterns, $
                            dt_res=nframes,pixons=pixons,error=e_out, $
                            sensitivity=sensitivity,resolution=resolution, $
                            pixon_sizes=pixon_sizes,pixonmap=p_out, $
                            lambda_max=lambda_max, $
                            nolook=nolook,notty=notty,_extra=_extra

               if n_elements(index_out) GT 0 then begin
                  index_out = [index_out,temporary(i_out)]
                  data_out = [[[data_out]],[[temporary(d_out)]]]
                  error_out = [[[error_out]],[[temporary(e_out)]]]
                  if n_elements(p_out) GT 0 then $
                     pmap_out = [[[pmap_out]],[[temporary(p_out)]]]
               endif else begin
                  index_out = temporary(i_out)
                  data_out = temporary(d_out)
                  error_out = temporary(e_out)
                  if n_elements(p_out) GT 0 then $
                     pmap_out = temporary(p_out)
               endelse
            endif
   
         endfor

      endelse

   endfor

   if n_elements(index_out) GT 0 then iindex_out = temporary(index_out)
   if n_elements(data_out)  GT 0 then idata_out  = temporary(data_out)
   if n_elements(error_out) GT 0 then ierror_out = temporary(error_out)
   if n_elements(pmap_out)  GT 0 then ipmap_out  = temporary(pmap_out)

   if n_elements(out) LE 1 then return,'' else return,out[1:*]

end
