function sxl2radiance,edges,dtime,index,flux,indata=indata, $
	sttime=sttime,endtime=endtime, $
	te_sxt=te_sxt,em_sxt=em_sxt,te_corr=te_corr, $
	photon=photon,erg=erg,debug=debug,loud=loud,new=new

;+
; NAME
;	sxl2radiance
; PURPOSE
;	To calculate the spectral radiance at the sun in a band from SXT signals 
;	averaged over specified time intervals.  Assumed spectrum is from
;	the temperature derived from Al.1/AlMg filter ratio for the same
;	time interval.
; CALLING EXAMPLES
;	output_structure=sxl2radiance([10,18],24.*60.*60.)
;	out=sxl2radiance(edges,dtime,index,flux,indata=indata, $
;		sttime=sttime,endtime=endtime,te_sxt,em_sxt,te_corr, $
;		photon=photon,erg=erg,debug=debug,loud=loud,new=new)
; INPUT
;	edges, 1-d array of wavelengths defining bands for flux ratio analysis
;	  Note:  Band limits must lie within the range 1.26867-298.029 A.
;	dtime, length of averaging time intervals (seconds)
; OPTIONAL INPUT
;	index, index structure from sxl_analysis.pro
;	flux, signal structure from sxl_analysis.pro
; OPTIONAL KEYWORD INPUT
;       indata, name of .genx file with the sxl fluxes and index
;		required if index and flux are not passed in.
;	sttime, starting time of desired analysis interval
;	endtime, ending time of desired analysis interval
;     Note:  If sttime or endtime are not provided analysis refers
;		to index for the start and/or end time.
;	/photon, do computations in photons 
;	/erg,    do computations in ergs (default)
;	/debug, stops program just before end
;	/loud,  prints out a lot of results to screen
;	/new,  force reading of sxl data
; OUTPUT 
;	IDL structure containing:
;		time, day:	structure times of midpoints of intervals
;		fmttim:		ascii time of midpoints of intervals
;		units:		either ergs or photons per second
;		band:	edges in angstrom of spectral band(s)
;		rad:	spectral radiance(s) for the specified band(s)
;	  			in photons or ergs per wavelength bin
; OPTIONAL KEYWORD OUTPUT 
;	te_sxt, alog10(SXT temperatures) from two-filter ratio 
;	em_sxt, alog10(SXT emission measure) from two-filter ratio
;	te_corr, array of algorithm-corrected AlMg/Al.1 temperatures
; PRINTED OUTPUT
; RESTRICTIONS
;	SXT filter-failure response era is determined by sttime,
;		data sets that cross failure boundaries will become 
;		increasingly inaccurate.
; PROGRAMS CALLED
;	sxl_analysis, sxl_select, sxt_flux, sxt_teem2,
;	sxt_sig_per_dn_lwa, stdev
; HISTORY
;	28-Jun-99  LWA  Version 1.0 completed.
;	 4-Jul-99  LWA  Header work.
;	 5-Jul-99  LWA  Changed name and added fmttim and units to
;			  output structure.
;-

; *********************************************************
;  Step 0:  What are the units to use?
; *********************************************************
if keyword_set(photon) and keyword_set(erg) then begin
  print,' **** Error in sxl2radiance ****',string(7b),string(7b)
  print,'      You cannot specify both /photon and /erg'
  stop 
endif else if keyword_set(photon) then Units=0 else Units=1

; *********************************************************
;  Step 1:  Either input SXL flux and index or get the
;       data with sxl_analysis.pro if index, and flux
;	have not been passed in.
; *********************************************************
if keyword_set(new) or n_elements(index) eq 0 or $
	n_elements(flux) eq 0 then begin
   if keyword_set(indata) then begin
      restgen,file=indata,index,flux
   endif else begin
      sxl_analysis,sttime,endtime,index,flux,filter=[2,3],res=[1,2]
   endelse
endif
ni=n_elements(index)
if n_elements(sttime) eq 0 then sttime=anytim2ints(index(0))
if n_elements(endtime) eq 0 then endtime=anytim2ints(index(ni-1))

; *********************************************************
;  Step 2:  Process SXL fluxes and compute the averages.
; *********************************************************
al=where(gt_filtb(index) eq 2)		; First, the thin-Al.
alout=sxl_select(index(al),flux(0,al),dtime,start=sttime,$
	endt=endtime,tout=altout)
dg=where(gt_filtb(index) eq 3)
dgout=sxl_select(index(dg),flux(0,dg),dtime,start=sttime,$
	endt=endtime,tout=dgtout)
ok=lindgen(n_elements(alout))
ii=where(alout eq -1)
ok=kill_index(ok,ii)
ii=where(dgout eq -1)
ok=kill_index(ok,ii)

; *********************************************************
;  Step 3:  ; Derive the two-filter ratio temperatures
;	and emission measures.
; *********************************************************
waste =  sxt_flux(6.2,2)	;Run to set up common block
date=sttime
sxt_teem2,2,alout(ok),3,dgout(ok),te_sxt,em_sxt,/interp,date=date
te_sxt=reform(te_sxt) & em_sxt=reform(em_sxt)

; *********************************************************
;  Step 4:  Loop through the spectral bands and compute 
;	and print out the TRUE/SXT flux ratios in each band.
; *********************************************************
nn=n_elements(edges)
bandflux=fltarr(n_elements(ok),nn-1)
rz=fltarr(n_elements(ok),nn-1,3)   ; Flux ratio output array
                                   ; rz(DEM,band,(3mk,Tsxt,Tcorr))

for j=0,nn-2 do begin   ; Loop through the spectral bands

   ; Compute the radiance in the band for each of the intervals,
   ; based upon the Thin Aluminum signal and using the derived temperatures.
   t_band = sxt_sig_per_dn_lwa(te_sxt,2,edges(j),edges(j+1),$
	erg=units,date=date)
   t_band = alout(ok)*10^t_band

   ; Compute the radiance in the band for each of the intervals,
   ; based upon the Thin Aluminum signal and using the corrected temperatures.
   fit2=[36.0540,     -10.4261,     0.901680]
   te_corr = fit2(0)+(te_sxt*fit2(1))+(te_sxt^2*fit2(2))
   tcorr_band = sxt_sig_per_dn_lwa(te_corr,2,edges(j),edges(j+1),$
	erg=units,date=date)
   tcorr_band = alout(ok)*10^tcorr_band

   ; Compute the radiance in the band for each of the intervals,
   ; based upon the Thin Aluminum signal and an assumed 3 MK signal.
   mk3_band = sxt_sig_per_dn_lwa(alog10(3.0e6),2,edges(j),edges(j+1),$
	erg=units,date=date)
   mk3_band = alout(ok)*10^mk3_band(0)

   ; Compute and output the ratios of dgout to derived radiance
   ; for each case, averages and std deviations.
   ratio_tru_mk3 = dgout(ok)/mk3_band  
   ratio_tru_t =   dgout(ok)/t_band
   ratio_tru_tcorr = dgout(ok)/tcorr_band

   ; Load the RZ array.
   rz(*,j,0)=ratio_tru_mk3
   rz(*,j,1)=ratio_tru_t
   rz(*,j,2)=ratio_tru_tcorr

   ; Load array used below for bandflux ratio.
   bandflux(*,j)=t_band

   if keyword_set(loud) then begin
      print
      print,'              SXT Temp            Flux ratios'
      print,'    Model #    T (MK)       AlMg/3MK        AlMg/Tsxt      Bandflux 
      for i=0,n_elements(ok)-1 do begin
          print,ok(i)+1,10^(te_sxt(i)-6),ratio_tru_mk3(i),ratio_tru_t(i),$
	     t_band(i) 
      endfor

         if n_elements(ok) gt 1 then begin  ; Do averages of 2 or more DEM cases.
            std_tru_mk3=stdev(ratio_tru_mk3,avg_tru_mk3)
            std_tru_t=stdev(ratio_tru_t,avg_tru_t)
            std_bandflux=stdev(t_band,avg_t_band)
            print
            print,'     Average values: ',avg_tru_mk3,avg_tru_t,avg_t_band
            print,'     Std deviations: ',std_tru_mk3,std_tru_t,std_bandflux
	    print,''
         endif
   endif

endfor

; *********************************************************
;  Step 6:  Print out the TRUE longwave to shortwave band flux ratio.
; *********************************************************
if keyword_set(loud) then begin

   if nn eq 3 then begin  ; Only 2 wavelength bands.
      ratio_long_short=bandflux(*,1)/bandflux(*,0)
      print
      print,'              SXT Temp    Flux band ratios'
      print,'    Model #    T (MK)  (long_wave_band)/(short_wave_band)
      for i=0,n_elements(ok)-1 do begin
          print,ok(i)+1,10^(te_sxt(i)-6),ratio_long_short(i) 
      endfor
   endif

endif

; *********************************************************
;  Step 7:  Prepare the output data structure.
; *********************************************************
out = {time:1L,day:1,fmttim:'',units:'', $
	edges:fltarr(n_elements(edges)), $
	rad:fltarr(n_elements(edges)-1)}
out = replicate(out,n_elements(alout))
out.time = altout.time
out.day = altout.day
out.fmttim = fmt_tim(altout)
case 1 of
   units eq 0 : kind = 'photons/s'
   units eq 1 : kind = 'ergs/s'
endcase
out.units = kind
out.edges = edges
out.rad = -1.
for i=0,n_elements(ok)-1 do out(ok(i)).rad = bandflux(i,*)
if keyword_set(debug) then stop

return, out

end

