pro xy_lwa, index, data, nx=nx, ny=ny, xy=xy, bin=bin, noscale=noscale, $
	color=color, ddhhmm=ddhhmm, charsize=charsize,charthick=charthick, $
	xoffset=xoffset,yoffset=yoffset,notimes=notimes, $
	noimg=noimg, date_only=date_only

;+
; NAME:
;       xy_lwa
; PURPOSE:
;	To make a raster pattern of a time series of SXT images
;	to display time evolution of something or other.
;	The images start at upper left and go to lower right,
;	and the formatted time is written at the bottom of each
;	image.
; CALLING SEQUENCE:
;	xy_lwa, index,data, nx=nx, ny=ny, xy=sy, bin=bin, noscale=noscale,$
;		color=color, ddhhmm=ddhhmm, charsize=charsize,$
;		charthick=charthick, xoffset=xoffset, yoffset=yoffset,$
;		notimes=notimes, noimg=noimg
; INPUTS:
;	data = image array
;	index = index structure or string array
;	   If a string array is used then the string is printed on the
;	   image in place of the time derived from index.
; KEYWORD INPUTS
;       nx = number of images in x direction
;       ny = number of images in y direction
; WINDOW REQUIREMENT
;	This routine requires that the right sized IDL window be prepared.
;	The screen is NOT erased by this program.
; OPTIONAL KEYWORD INPUTS
;       bin = rebin factor
;	Keyword NOSCALE acts like NOSCALE in STEPPER.
;	/ddhhmm makes time output in 26-APR-94  19:26:57 style,
;		else only 19:26:57 is written on the images.
;       /date_only limits time output to date, e.g., 26-APR-94
;	color=color sets color of characters
;	charsize=charsize sets character size, default = 1.6
;	charthick=charthick sets thickness of vector drawn characters, default=1
;	xoffset, yoffset = offsets in x & y to apply to each image.
;	/notimes eliminates output of time labels.
;	/noimg eliminates output of images, as program does not erase the
;	        screen this permits adding labels or times to an array of images
;               from some other source.
;	xy=xy, size of single picture in screen pixels,
;		e.g., xy=256 or xy=[128,64] where x dimension comes first.
; HISTORY:
;	HSH, written some time in early 1992
;       LWA modified to use existing window, 11/2/92
;	Fixed noscale option, put index, data in standard order, 4/07/94  LWA
;	Added ddhhmm option,  4/29/94.
;	Added charsize keyword,  4/30/94.
;	Added xoffset, yoffset and notimes keywords, 8/04/94 LWA.
;	Fixed error and ammended header, 10/26/94 LWA.
;	Added xy keyword, 12/18/94 LWA.
;	Added date_only keyword, 5/15/95, LWA
;       Fixed date_only and ddhhmm logic, 5/21/95, LWA
;	Changed variable typ to tyyp,  3/19/97, LWA
;-

if keyword_set(date_only) and keyword_set(ddhhmm) then begin
   print,'!!!!!YOU CANNOT USE BOTH /ddhhmm AND /date-only!!!!!!
   return
endif

if keyword_set(bin) eq 0 then bin=1
if keyword_set(color) eq 0 then color=255
factor = bin
if NOT keyword_set(xy) then begin
   siz = size(data) & n_img = siz(3)
   x_siz = siz(1) & y_siz = siz(2)
   x_factor = factor * x_siz
   y_factor = factor * y_siz 
endif else begin
   if n_elements(xy) eq 2 then begin
      x_siz=xy(0) & y_siz=xy(1)
   endif else begin
      x_siz=xy & y_siz=xy 
   endelse
   x_factor = factor * x_siz
   y_factor = factor * y_siz
endelse

tyyp = size(index)

wide = nx 
deep = ny
xy = indgen(wide*deep)
x = xy mod wide
y = xy/wide & y = deep - y - 1

ttt=0
if keyword_set(noscale) then noscale=0 else noscale=1
if keyword_set(ddhhmm) then ttt=1 else ttt=0
if keyword_set(date_only) then ttt=2 else ttt=ttt
if NOT keyword_set(xoffset) then xoffset=0 else xoffset=xoffset
if NOT keyword_set(yoffset) then yoffset=0 else yoffset=yoffset

if NOT keyword_set(noimg) then begin
   minim = min(data) & maxim = max(data)
   dat = bytscl(data, min = minim, max = maxim, top = !d.n_colors)
endif

; if (siz(0) eq 2) then ni=0 else ni=siz(3)-1

ni = n_elements(index)-1
for i = 0,ni do begin

   if NOT keyword_set(noimg) then begin
      case NOSCALE of

         0:    tv,rebin(dat(*,*,i),x_factor,y_factor,/sample), $
                  x_factor*x(i)+xoffset, y_factor*y(i)+yoffset

         1:    tvscl,rebin(dat(*,*,i),x_factor,y_factor,/sample), $
                  x_factor*x(i)+xoffset, y_factor*y(i)+yoffset

     endcase
   endif

   if NOT keyword_set(notimes) then begin
      if tyyp(2) eq 8 then begin
        case ttt of      
   
   	   ;  Print times only
   	   0 :  hhmm = strmid(fmt_tim(index(i)),10,9)
  
	   ;  Print dates & times
	   1 :  hhmm = fmt_tim(index(i))

	   ;  Print dates only
	   2 :  hhmm = strmid(fmt_tim(index(i)),0,9)

        endcase
      endif

      if (tyyp(2) ne 8)  then hhmm = index(i)

      if NOT keyword_set(charsize) then charsize=1.6 else charsize=charsize
      if NOT keyword_set(charthick) then charthick=1 else charthick=charthick

      xyouts, x_factor*x(i)+xoffset+2, 3+y_factor*y(i)+yoffset+2, hhmm,$
         /device, charsize=charsize, color=color, charthick=charthick

   endif

endfor
print,ttt 
end

