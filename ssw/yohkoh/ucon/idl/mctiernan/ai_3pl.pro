;+
; NAME:
;	Ai_3pl
; PURPOSE:
;	the inital values of a for the triple P.L..
; CALLING SEQUENCE:
;	a=Ai_3pl(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	Spring,' 92 JMcT
;-
FUNCTION Ai_3pl, fy, sfy, e, nch
   
   break2, fy, sfy, e, nch, ax, fx	;fit fy to a triple power law
   ;ax=[log(k1),-gm1,log(k2),-gm2,log(k3),-gm3]
   eb1 = (ax(2)-ax(0))/(-ax(3)+ax(1))
   eb1 = 10.0^eb1                 ;1st break energy
   eb2 = (ax(4)-ax(2))/(-ax(5)+ax(3))
   eb2 = 10.0^eb2                 ;2nd break energy
   
   a = [10.0^ax(0), -ax(1), -ax(3), -ax(5), eb1, eb2] 
   ;a=[k,gm1,gm2,gm3,eb1,eb2]
   
   RETURN, a
END

