;+
; NAME:
;	Ai_thp2pl
; PURPOSE:
;	the inital values of a for the thermal + double P.L..
; CALLING SEQUENCE:
;	a=Ai_thp2pl(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	Spring,' 92 JMcT
;-
FUNCTION Ai_thp2pl, fy, sfy, e, nch
   
   break2, fy, sfy, e, nch, ax, fx	;fit fy to a triple power law
;ax=[log(k1),-gm1,log(k2),-gm2,log(k3),-gm3]
   eb2 = (ax(4)-ax(2))/(-ax(5)+ax(3))
   eb2 = 10.0^eb2                 ;2nd break energy
   axhi = [10.0^ax(2), -ax(3), -ax(5), eb2] ;the upper double p.l.
   e10 = 10.0^e
;now proceed as in ainit4
   IF (ax(3) LT ax(1)) THEN BEGIN ;ca'nt do this kind of fit 
      print, 'no low energy excess in ai_thp2pl'
      print, 'try a different fit'
      a = [1.0, 10.0, axhi]
   ENDIF ELSE BEGIN
      fhi = fx_2pl(axhi, e10, nch, neb2) ;high energy part
      f = (10.0^fy)-fhi		;the low energy excess
      n = 0
      WHILE (f(n) GT 0.0) DO n = n+1
      nh = n-1
      subs = indgen(n)
      IF (n GT 1) THEN BEGIN	;now fit the low energy excess
         elo = e(subs)            ;to the thermal fn
         flo = alog10(f(subs))
         slo = sfy(subs)
         ax1 = ai_1th(flo, slo, elo, n) ;guess the thermal fit
         a = [ax1, axhi]           ;a=[e47,t6,k1,gamma1,gamma2,ebr]
         
      ENDIF ELSE a = [1.0, 10.0, axhi]
   ENDELSE
   ret1:	RETURN, a
END

