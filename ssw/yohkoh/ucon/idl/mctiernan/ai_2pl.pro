;+
; NAME:
;	Ai_2pl
; PURPOSE:
;	the inital values of a for the double power law.. 
; CALLING SEQUENCE:
;	a=Ai_2pl(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	Spring,' 92 JMcT
;-
FUNCTION Ai_2pl, fy, sfy, e, nch
   
   break, fy, sfy, e, nch, ax, fx ;fit fy to a double power law
;ax=[log(k1),-gm1,log(k2),-gm2]
   ebr = (ax(2)-ax(0))/(-ax(3)+ax(1))
   ebr = 10.0^ebr               ;calculate the break energy
   a = [10.0^ax(0), -ax(1), -ax(3), ebr] ;a=[k,gm1,gm2,ebr]
   
   RETURN, a
END

