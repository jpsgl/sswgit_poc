;+
; NAME:
;	Ai_plppl
; PURPOSE:
;	the inital values of a for the sum of two power laws..
; CALLING SEQUENCE:
;	a=Ai_plppl(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	Spring,' 92 JMcT
;-
FUNCTION Ai_plppl, fy, sfy, e, nch
   
   break, fy, sfy, e, nch, ax, fx	;fit fy to a double power law
   ;ax=[log(k1),-gm1,log(k2),-gm2]
   
   IF (ax(3) LT ax(1)) THEN BEGIN ;ca'nt do this kind of fit 
      print, 'gm2>gm1 in ai_plppl, cannot do it'
      print, 'try the other double p.l.'
      a = [10.0^ax(0), -ax(1), 10.0^ax(2), -ax(3)]
   ENDIF ELSE BEGIN
      fhi = ax(2)+ax(3)*e		;high energy part
      f = (10.0^fy)-(10.0^fhi)	;the low energy excess
      subs = where(f GT 0.0)      ;only useful where flo>0
      n = N_ELEMENTS(subs)
      IF (n GT 1) THEN BEGIN	;now fit the low energy excess
         elo = e(subs)            ;to a different power law
         flo = alog10(f(subs))
         slo = sfy(subs)
         lfit, flo, slo, elo, axlo
      ENDIF ELSE axlo = [0.0, 7.0]	;only 1 channel, forget it
      ;make it look like no component
      
      a = [10.0^axlo(0), -axlo(1), 10.0^ax(2), -ax(3)]
   ENDELSE                      ;a=[k1,gm1,k2,gm2]
;   a = [10.0^ax(0), -ax(1), 10.0^ax(2), -ax(3)]
   
   RETURN, a
END


