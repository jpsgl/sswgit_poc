;+
; NAME:
;	Ai_thppl_l
; PURPOSE:
;	the inital values of a for the thermal + power law..
;       Same as ainit4 for now
; CALLING SEQUENCE:
;	a=Ai_thppl_l(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	Spring,' 92 JMcT
;-
FUNCTION Ai_thppl_l, fy, sfy, e, nch
   
   
   break, fy, sfy, e, nch, ax, fx	;fit fy to a double power law
;ax=[log(k1),-gm1,log(k2),-gm2]
   
   IF (ax(3) LT ax(1)) THEN BEGIN ;ca'nt do this kind of fit 
      print, 'NO LOW ENERGY EXCESS IN AINIT4'
      print, 'EXPECT A BAD FIT'
      a = [1.0, 30.0, 10.0^ax(2), -ax(3)]
   ENDIF ELSE BEGIN
      fhi = ax(2)+ax(3)*e		;high energy part
      f = (10.0^fy)-(10.0^fhi)	;the low energy excess
      ssf = where(f GT 0.0)
      n = min(ssf)
      IF(n NE 0) THEN BEGIN
         print, 'NO LOW ENERGY EXCESS IN AINIT4'
         print, 'EXPECT A BAD FIT'
         a = [1.0, 30.0, 10.0^ax(2), -ax(3)]
         GOTO, ret1
      ENDIF
      WHILE ((f(n) GT 0.0) AND (n LT nch-2)) DO n = n+1
      nh = n-1
      subs = indgen(n)
      IF (n GT 1) THEN BEGIN	;now fit the low energy excess
         elo = e(subs)            ;to the thermal fn
         flo = alog10(f(subs))
         slo = sfy(subs)
         ax1 = ai_1th_l(flo, slo, elo, n) ;guess the thermal fit
         a = [ax1, 10.0^ax(2), -ax(3)] ;a=[e47,t6,k,gamma]
         
      ENDIF ELSE a = [1.0, 30.0, 10.0^ax(2), -ax(3)]
   ENDELSE
   ret1: RETURN, a

END

