;+
; NAME:
;	Ai_1pl_ecut
; PURPOSE:
;	the inital values of a for the power law that cuts off below e0
; CALLING SEQUENCE:
;	a=Ai_1pl_ecut(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	29-mar-94, JMM
;-
FUNCTION Ai_1pl_ecut, fy, sfy, e, nch
   
   IF(nch GT 3) THEN BEGIN
      break, fy, sfy, e, nch, ax, fx ;fit fy to a double power law
;ax=[log(k1),-gm1,log(k2),-gm2]
      ebr = (ax(2)-ax(0))/(-ax(3)+ax(1))
      ebr = 10.0^ebr            ;calculate the break energy
   
      IF (ax(1) LT ax(3)) THEN BEGIN ;ca'nt do this kind of fit 
         print, 'A LOW ENERGY EXCESS IN AI_1PL_ECUT'
         print, 'EXPECT A BAD FIT'
      ENDIF
      a = [10^ax(2)*ebr^ax(3), -ax(3), ebr]
   ENDIF ELSE BEGIN
      lfit, fy, sfy, e, ax      ;just fit fy to a power law
      a = [10.0^ax(0)*20.0^ax(1), -ax(1), 20.0] ;ax(0)=log(k),ax(1)=-gamma
   ENDELSE
   
   RETURN, a
END

