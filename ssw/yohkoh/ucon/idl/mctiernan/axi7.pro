;+
; NAME:
;	Axi7
; PURPOSE:
;	the inital values of a for the thermal + P.L..
; CALLING SEQUENCE:
;	a=Axi7(fy,sfy,e,nch)
; INPUT:
;	fy=Log(Photon Flux), 
;	sfy=Unc. in fy squared,
;	e=log(channel energies), 
;	nch=no. of channels
; OUTPUT:
;	a=initial values for a, the fit parameters
; HISTORY:
;	Spring,' 92 JMcT
;-
FUNCTION Axi7, fy, sfy, e, nch
   
   break, fy, sfy, e, nch, ax, fx	;fit fy to a double power law
   ;ax=[log(k1),-gm1,log(k2),-gm2]
   
   IF (ax(3) LT ax(1)) THEN BEGIN ;ca'nt do this kind of fit 
      print, 'no low energy excess in axi7'
      print, 'try a different fit'
      a = [0.0, 30.0, 10.0^ax(2), -ax(3)]
   ENDIF ELSE BEGIN
      fhi = ax(2)+ax(3)*e		;high energy part
      f = (10.0^fy)-(10.0^fhi)	;the low energy excess
      n = 0
      WHILE (f(n) GT 0.0) DO n = n+1
      nh = n-1
      subs = indgen(n)
      IF (n GT 1) THEN BEGIN	;now fit the low energy excess
         elo = e(subs)            ;to the thermal fn
         flo = fy(subs)
         slo = sfy(subs)
         ax1 = ai_1th(flo, slo, elo, n) ;guess the thermal fit
         a = [ax1, 10.0^ax(2), -ax(3)] ;a=[e47,t6,k,gamma]
         
      ENDIF ELSE a = [1.0, 30.0, 10.0^ax(2), -ax(3)]
   ENDELSE
   ret1:	RETURN, a
END

