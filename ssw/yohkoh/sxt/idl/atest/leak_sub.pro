function leak_sub, index, data, index_out, $
        udata_in=udata_in, udata_out=udata_out, $ ;jmm, 12-feb-95
	update_index=update_index, $
	leak_only=leak_only, leak_index=leak_index0, save=save, qdebug=qdebug, $
	dc_interpolate=dc_interpolate, dc_scalar=dc_scalar, float=float, $
	force_darksub=force_darksub, orbit_correct=orbit_correct, $
	noleak=noleak, second_order=second_order, synthetic_sfc=synthetic_sfc, $
        grill=grill, nogrill=nogrill
;
;+
;NAME:
;	leak_sub
;PURPOSE:
;	Subtract the Scattered light image from input FFI images.  This is
;	needed for Thin-Al and AlMg images taken after 13-Nov-92. 
;CALLING SEQUENCE:
;	data_out = leak_sub(index, data, index_out)
;	data_out = leak_sub(index, data, /update_index)
;INPUT:
;	index	- The index structure for each image
;	data	- The data array (NX x NY x N).  It should have had the
;		  dark current already subtracted.  If the input is
;		  byte type, then DARK_SUB is called.
;OUTPUT:
;	data_out- The leak (scattered light) subtracted image
;	index_out- The modified index header
;OPTIONAL KEYWORD INPUT:
;       udata_in - the uncertainty in the data array, passed through to DARK_SUB
;       udata_out(INPUT/OUTPUT) - The uncertainty in the output array, passed out of DARK_SUB,
;                   This must be set to a variable before the call, i.e.
;                   YOU (the user) have a statement like "udata_out=1"
;                   before calling LEAK_SUB with the udata_out keyword, the calculation
;                   will not be done if a zero or undefined value is passed in.
;                   Note that leak_only data "officially" has no uncertainty,
;                   So don't use this with the leak_only keyword, you will get an array of zeros
;	leak_only- If set, the output is only the scattered light
;		  images that go with each index.
;	save	- If set, store the data in a common block to avoid 
;		  having to read it from memory many times.
;	dc_interpolate - If set, do integration time interpolation on
;		  the dark current subtraction images.
;	dc_scalar - If set, use a scalar value for the dark current
;		  subtraction
;	force_darksub - If set, then call DARK_SUB even if the data type 
;		  is non-byte.  This is needed with restore low-8 data.
;       noleak - if set, subtrace leak(!) (seems silly in a routine called
;                leak_sub, but needed for leak bybass from callers
;                like sxt_prep...)
;       synthetic_sfc - if set, generate and apply synthetic SFC
;                       [ hook for P.G.Shirts routines ][B
;
;OPTIONAL KEYWORD OUTPUT:
;	leak_index- Return the index of the reference images
;METHOD:
;	1. Call DARK_SUB if "data" is byte type
;	2. For PFI, extract the relevant portion of the images
;	3. Scale the scattered light image for exposure duration.
;	4. Subtract off the scattered light
;HISTORY:
;	Written 7-Jan-93 by M.Morrison
;	14-Jan-93 (MDM) - Modification to perform leak subtraction 
;			  properly for quarter resolution images.
;	 3-Feb-93 (MDM) - Modified to correct AlMg leak images also.
;			- Added /SAVE option to store the leak image
;			  in a common block - avoids reading it multiple
;			  times, but uses memory up.
;	 9-mar-93 (JRL) - Fixed a bug in case no leak images are available.
;	 7-Apr-93 (MDM) - Modified to work with PFI images
;			  Adjusted code to do REBIN only once per uniq resolution
;			  (faster/more memory efficient)
;			- Added check to see if leak calibration images exist.
;			- Corrected an error that leak subtraction would not
;			  have been performed if the neutral density filter was
;			  in place!!
;V2.0	 6-Jun-93 (MDM) - Added version number, and started calls to HIS_INDEX
;V2.1	 9-Jul-93 (MDM) - Corrected an error caused when a smaller image is 
;			  imbedded in a larger array (256x256 inside a 512x512xN)
;V2.11	 3-Aug-93 (MDM) - Modification for handling unassembled ORs
;V3.00	 4-Aug-93 (MDM) - Modified to use EXT_SUBIMG2 which is much faster than
;			  rebinning the whole image.
;			- Added INDEX_OUT parameter
;			- Do not do a "fix" of data when it is non-byte type
;			  when creating the DATA_OUT variable
;V3.01	26-Aug-93 (MDM) - Modified to use ALIGN1IMG instead of EXT_SUBIMG2
;			- Added /UPDATE_INDEX option
;	26-Aug-93 (MDM) - Replaced Al.1 image sfc921127.0947 with sfc930819.1607
;				** V3.xx was never put on-line **
;V4.00	27-Aug-93 (MDM) - Replaced code with the essence of DARK_SUB
;V4.01  11-Oct-93 (MDM) - Made sure that the output index was passed to 
;			  HIS_INDEX so that the .HIS structure was appended
;V4.02	13-Oct-93 (MDM) - Added /DC_INTERPOLATE and /DC_SCALAR options
;			- Adjusted code since the DARK_SUB history was not being
;			  preserved
;V4.03	 8-Feb-95 (MDM) - Added /FLOAT keyword to be passed through to DARK_SUB
;	12-Feb-95 (jmm) - Added the UDATA keywords
;V4.04	27-Feb-95 (MDM) - Put McTiernan modifications on-line
;V4.05	23-Mar-95 (MDM) - Added /force_darksub
;V4.06	 9-May-95 (MDM) - Modified how the /FLOAT option worked (it was always
;			  doing a FIX before on the leak image).
;			- Use ROUND instead of FIX when not going to floating
;			  output
;V4.07   11-Feb-98 (SLF) - add ORBIT_CORRECT (pass to dark_sub)
;        26-Oct-98 (SLF) - add /NOLEAK keyword and function
;V4.08   10-Jan-2000 - S.L.Freeland add /SECOND_ORDER keyword and function
;                      Add history bit
;V4.09   23-Aug-2000 - Add /SYNTHETIC_SFC keyword and function
;                      Add history bit
;V4.10    8-Jan-2001 - S.L.Freeland - add Al.1 Grill correction option
;                      (no longer applied to SFCs)
;V4.20   13-Nov-2001 - S.L.Freeland - remove a call to 'fix' - why it
;                      was there at all is TBD (ask mons...)
;-
;
common get_leak_image, lindex, ldata
common get_leak_image2, called
second_order=keyword_set(second_order)
;
progverno = 4.1*1000
;
n = n_elements(index)
index_out = index
his_index, index_out
;
quncert = keyword_set(udata_out) ;jmm, 12-feb-95
noleak=keyword_set(noleak)
yesleak=1-noleak
synthetic_sfc=keyword_set(synthetic_sfc) or get_logenv('sxt_syn_sfc') ne ''
nogrill=keyword_set(nogrill)
grill = keyword_set(grill) or (synthetic_sfc - nogrill)

;
if (keyword_set(leak_only)) then begin
    nx = max(gt_shape(index, /x))
    ny = max(gt_shape(index, /y))
    if (keyword_set(float)) then data_out = fltarr(nx,ny,n) $
			else data_out = intarr(nx, ny, n)
    if(quncert) then udata_out = bytarr(nx, ny, n) ;jmm, 12-feb-95
end else begin
    siz = size(data)
    nx = siz(1)
    ny = siz(2)
    typ = siz( siz(0)+1 )

    if (typ eq 1) or (keyword_set(force_darksub)) then begin
       udata_out = quncert      ;jmm, 12-feb-95, udata_out has to be set to be passed into DARK_SUB
       data_out = dark_sub(index, data, index_out, interpolate = dc_interpolate, $
                           force_scalar = dc_scalar, float = float, $
                           orbit_correct=orbit_correct, $ ; slf 11-feb-98
                           udata_in = udata_in, udata_out = udata_out) ;jmm, 12-feb-95
    end else begin
       data_out = data
       if(quncert) then begin   ;jmm, 12-feb-95
          if(keyword_set(udata_in)) then udata_out = udata_in $
            else udata_out = byte(make_array(size = size(data_out)) + 0) ;pass out an array of zeros if nothing is passed in and no dark subtraction.
       end
    end
end
;
if (keyword_set(qdebug)) then print, 'Finished making output array'
;
;---------------------------------------- Deterimine the optimal order of the images to do the subtraction
;
sel_leak_image, index, leak_files, dset
;
xcorn = fix(gt_corner(index, /lower, /x)/4)     ;0-255
ycorn = fix(gt_corner(index, /lower, /y)/4)
res = fix(gt_res(index))

val =      xcorn         +  ycorn*256L               + res*256L*256L  + dset*256L*256L*64       ;dset specifies different leak image

;      low 8-bits 0:7      second 8-bits  8:15          third 8-bits     fourth 8-bits
;---- do all FR in order of DPE, then all HR, then all QR
;
ss = sort([val])
;
last_val = -999
;
qdebug=keyword_set(qdebug)
if yesleak then begin 
for iimg=0,n-1 do begin
    i = ss(iimg)		;do the subtraction in order of DPE so that
    ;
    if (dset(i) ne -1) then begin
	case 1 of 
           synthetic_sfc: begin
              mk_syn_sfc, index(i), leak_index0, leak_data
              get_leak_image, index(i), leak_index=leak_index0, leak_data=leak_data
              if grill then begin 
                 algrill=sxt_get_grill(index(i),status)
                 if status eq 1 then begin
                    leak_data=leak_data+algrill
                 endif
              endif
            endcase
            else: if (val(i) ne last_val) then get_leak_image, index(i), leak_index0, leak_data, qdebug=qdebug, /save
        endcase	    
        if (keyword_set(qdebug)) then print, 'Using leak image ', fmt_tim(leak_index0), ' for val=', val(i)
	

	factor = (gt_expdur(index(i)) / ([gt_expdur(leak_index0),2691.09])(synthetic_sfc) )
if get_logenv('synsfc_debug') ne '' then stop,'syndebug'
	if (keyword_set(qdebug)) then print, 'LEAK_SUB: Exposure normalization factor: ', factor
	if (keyword_set(leak_only)) then begin
	    data_out(0,0,i) = leak_data * factor
	end else begin
	    siz = size(leak_data)
	    nx2 = siz(1)-1
	    ny2 = siz(2)-1
	    ;;data_out(0,0,i) = data_out(0:nx2,0:ny2,i) - fix(leak_data * factor)
	    if (data_type(data_out) ge 4) then data_out(0,0,i) = data_out(0:nx2,0:ny2,i) - (leak_data * factor) $
				else data_out(0,0,i) = data_out(0:nx2,0:ny2,i) - round(leak_data * factor)

	    his_index, index_out, i, 'time_leak', leak_index0.his.time_leak
	    his_index, index_out, i, 'day_leak',  leak_index0.his.day_leak
            pver=progverno
            if second_order then begin
	       if gt_res(index_out(i)) ge 1 and $
		  gt_pfi_ffi(index_out(i),/ffi) then begin
	            if n_elements(called) eq 0 or keyword_set(qdebug) then $
		       box_message,'Applying SECOND ORDER leak correction
	               so_data=sxt_deleak(index_out(i), data_out(*,*,i), leak_index0, leak_data)
	               data_out(0,0,i)=so_data
                       pver = progverno + 2^15.          ; set history flag
	       endif 
	    endif  
            pver=pver+([0.,2^14.])(synthetic_sfc)        ; set history flag
	    his_index, index_out, i, 'q_leak_sub', pver
       end

   end

    last_val = val(i)
endfor
endif      ; end of Leak inhibit

;
if (not keyword_set(save)) then begin
    lindex = 0	;clear the GET_LEAK_SUB common block variables
    ldata = 0
end
;
if (keyword_set(qstop)) then stop
if (keyword_set(update_index)) then index = index_out
return, data_out
end

