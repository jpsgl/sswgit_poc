function sxt_scatwings,res,rr,no_ctr_smooth=no_ctr_smooth

;+
; NAME
;	sxt_scatwings
; PURPOSE
;	To prepare an array mimicing the sxt scattering wings, including
;	the entrance filter shadow rays.
; CALLING EXAMPLE
;	out = sxt_scatwings(res,rr)
;	out = sxt_scatwings(index(0)[,rr])
;	out = sxt_scatwings(1)
; INPUT
;	res, sxt image resolution in the form of a number (i.e., 0 for FR, i
;	     1 for HR, 2 for QR) or an index structure.
; OPTIONAL KEYWORD INPUT
;	no_ctr_smooth, set this keyword to avoid 7-point smoothing of
;	    the wing array out to R=5 pixels.
; OUTPUT
;	Array (not normalized) giving the scattering function.  Array size is
;		FR	2049x2049
;		HR	1025x1025
;		QR	513x513
;	The arrays are made one pixel larger than the SXT images in order to
;	have a center pixel.
; OPTIONAL OUTPUT
;	rr, array giving the radial distance of every pixel from the center
;	    pixel in pixel units.
; DETAILS
;	Based upon extensive analysis of 27-Feb-92 09:51 X-flare starburst
;	I have concluded that -2 is the best choice of slope for the scattering
;	wings, both in and between the rays.  The intensity in the rays is about
;	one-third the intensity between the rays.  To match the appearance of
;	the starburst I've set the rays equal to one degree and 4 degrees and 
;	applied a 7-pixel boxcar smoothing of the scatter image.  The expression
;	used to produce the model starburst is
;
;		signal = 3e6 * rr^(-2)
; PROGRAMS CALLED
;	datatype, findgen, rebin, reform, sqrt, temporary, sector_bound, big_smooth
; HISTORY
;	 9-Apr-2001  LWA, written
;	10-Apr-2001  LWA, changed variable mask to maskk to avoid IDL conflict.
;			  Added keyword no_ctr_smooth.
;	30-May-2001  LWA, Rotated rays 1 degree clockwise.
;-

tp = datatype(res,2)
if tp eq 8 then res=gt_res(res) else res=res

case 1 of
   res eq 0 : nx=2049
   res eq 1 : nx=1025
   res eq 2 : nx=513
endcase

x=nx/2
xx=(findgen(nx)-x)^2
xx=rebin(xx,nx,nx)
yy=(findgen(nx)-x)^2
yy=reform(yy,1,nx)
yy=rebin(yy,nx,nx)
rr=sqrt(temporary(xx)+temporary(yy))

rr(x,x)=1.		;To prevent problems at the origin.
wings=3e6*rr^(-2.)	;Constant chosen for easy alog10 display.

maskk=bytarr(nx,nx)

n_ray=0.5   		;Half-angle of narrow ray in degrees.
w_ray=2.0   		;Half-angle of wide ray in degrees.

for i=0,5 do begin
   shadow=sector_bound(-n_ray+(i*60+1),n_ray+(i*60+1),x,x,nx,nx)
   maskk(shadow)=1
   shadow=sector_bound(-w_ray+30+(i*60+1),w_ray+30+(i*60+1),x,x,nx,nx)
   maskk(shadow)=1
endfor

if keyword_set(no_ctr_smooth) then begin
   ctr=blank_circle(nx,nx,x,x,6,/image)
   jj=where(ctr eq 1)
   hold_img=wings
endif

ii=where(maskk eq 1)
wings(ii)=wings(ii)/3.
wings=big_smooth(wings,7,/noccd)

if keyword_set(no_ctr_smooth) then wings(jj)=hold_img(jj)

return,wings

end



