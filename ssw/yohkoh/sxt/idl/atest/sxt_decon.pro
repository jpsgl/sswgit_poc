;+
;
; Deconvolve PSF from an SXT image to get a cleaner image.
;

PRO sxt_decon, in_img, out_img, index=index, xc=xc, yc=yc, $
	slevel=slevel, spower=spower, log10T=log10T, qstop=qstop, $
        delta=delta, moffat=moffat, wiener=wiener, $ 
	scat_fraction=scat_fraction, no_shadow=no_shadow

;
; INPUT PARAMETERS:
;	in_img = input image.
;
; KEYWORD INPUT PARAMETER
;	INDEX, SXT index structure
;       XC,YC, the center of the FOV in CCD full-res coords (optional)
;	LOG10T, estimate of temperature in the region
;
; CALLING SEQUENCE
;	sxt_decon,in_image,out_image,index=index
;       sxt_decon,in_image,out_image,index=index,xc=144,yc=213,log10t=6.12
;       sxt_decon,in_image,out_image,index=index,/delta,scat_fraction=.044
;
; OUTPUT PARAMETERS:
;	out_img = deconvolved image.
;
; RESTRICTIONS:
;	There are some questions about the validity of the scattering
;	function is for the thick filters (i.e., other than
;	Al.1, AlMg, Mg3).
;	Calls SXT_PSF for point spread function, and then uses Fourier
;	transforms for convolution.
;	USES BIG ARRAYS, TAKES TIME, USES MEMORY.
;
; HISTORY:
;	Written June 17, 1994   Barry LaBonte (as SXT_SCATTER)
;	Cleaned up the code   June 21, 1994  BJL
;	Default values are Acton's result for 6-Sep-92 image  June 23,1994 BJL
;	30-Oct-96 [LWA]  Generalized method of determining pixel size.
;			 (called LWA_SCATTER)
;	08-Mar-99 DMcK   Revised deconvolution from LWA_SCATTER, 
;			 incorporated SXT_PSF.  Renamed DMCK_DECON
;	31-Mar-99 DMcK   Removed some unused code, renamed SXT_DECON
;	01-Apr-99 DMcK   Added DOUBLE=1 to FFT calls
;	07-Nov-99 DMcK   Added log10T optional input, for use in SXT_PSF.
;	08-Feb-01 DMcK   Included /DELTA, SLEVEL, SPOWER, & SCAT_FRACTION
;			 as optional inputs for SXT_PSF.  Included /WIENER
;			 for optional Wiener (optimal) filtering.  The latter
;			 calls Tom Metcalf's WIENER.PRO.
;	09-APR-01 DMcK	 Added switch NO_SHADOW to make SXT_SCATWINGS the
;			 default scattering-wing model.
;-


if NOT keyword_set(index) then begin
   beep & print,'**** SXT_DECON REQUIRES INPUT OF INDEX'
          print,'**** PLEASE CHANGE YOUR CODE!'
   return
endif

sttime=systime(1)

pix=gt_pix_size(index)/gt_pix_size(1)

; How big is the input?
sz = SIZE(in_img)
s1 = sz(1)
s2 = sz(2)
IF( s1 NE s2 ) THEN  $
	PRINT, 'WARNING: Image may not include whole Sun.'

filt=gt_filtb(index)
res=gt_res(index)
if not keyword_set(xc) or not keyword_set(yc) then begin
print,' XC, YC not specified; using the input index structure '
psf=sxt_psf(index,/four,dim=2*s1,slevel=slevel,spower=spower,$ 
    log10t=log10t,/noverbose,delta=delta,moffat=moffat,$ 
    scat_fraction=scat_fraction,no_shadow=no_shadow)
endif else begin
;print,' I''m using the input XC,YC '
psf=sxt_psf(filt,xc,yc,/four,dim=2*s1,slevel=slevel,spower=spower,$ 
    res=res,log10t=log10t,/noverbose,delta=delta,moffat=moffat,$ 
    scat_fraction=scat_fraction,no_shadow=no_shadow)
endelse

; Take the Fourier Transform
psf = FFT( TEMPORARY(psf), -1 ,double=1)

; NOW THE IMAGE  ------------------------------------------------
; Insert into double sized array to avoid Fourier wraparound
big = FLTARR( 2*s1, 2*s2 )
big(s1/2:s1-1+s1/2, s2/2:s2-1+s2/2) = in_img

; Transform,  convolve, and back transform
if keyword_set(wiener) then begin
  box_message,'Implementing Wiener filter'
  w = wiener(big)
  big = FFT( ( FFT(TEMPORARY(big),-1,double=1) * w / psf ),1,double=1)
endif else begin
  big = FFT( ( FFT(TEMPORARY(big),-1,double=1) / psf ),1,double=1)
endelse

; Extract the valid region
out_img = FLOAT( TEMPORARY( big(s1/2:s1-1+s1/2, s2/2:s2-1+s2/2) ) )
if keyword_set(qstop) then stop

print,' SXT_DECON took ',strtrim((systime(1)-sttime)/60.,1), $
' minutes to complete your request.'

END
