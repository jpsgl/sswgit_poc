function sxt_deleak, index, data, lkind, lkdat,imap,yn=yn,debug=debug
;+
;
; PROGRAM NAME
;	sxt_deleak
; PURPOSE
;	Second order straylight correction.  Add/subtract enough
;	of the SFC leak image to force the average signal in the
;	corners of the sxt_prep'd FFI to become zero.
; SAMPLE CALLING SEQUENCE
;	output = sxt_deleak(index,data,yn=yn)
; INPUT
;       index, index structure of SSC datafile
;       data, SSC floating point image array
; OPTIONAL KEYWORD INPUT
;	debug, stops program on each image processing cycle
; OUTPUT
;	Array of second-order leak corrected images.
; OPTIONAL INPUT/OUTPUT	
;       lkind, the SFC index(s)
;       lkdat, the SFC leak image(s)
;       imap, vector relating which SFC goes with which SSC		 
; OPTIONAL KEYWORD OUTPUT
;       yn, array flagging images to which 2nd order leak
;	    correction has been applied or not needed.
;	    	 0 if no correction
;		 + or - value of average corner correction
;		    in DN/pix if correction made.
;	    Correction applied if residual corner signal 
;	    greater than .01 DN/pix.
; PROCEDURE
;	Add/subtract enough of the SFC leak image, after correction
;	for x-ray scattering, to force the average signal in the 
;	periphery of the composite image to become zero.
;     Details:
;	1.  Compute DN/pix inside 1.1 Rsun of SSC, ds.
;	2.  Approximate scattered light (DN/pix) outside of
;		1.7 Rsun from the power-law expression
;		sl = 0.00657*ds^0.962.
;	3.  Determine SSC signal (DN/pix) outside of 1.7 Rsun
;		in SSC image, dc, in DN/pix.
;	4.  Subtract sl from dc to determine residual corner
;		signal, rc, in DN/pix.
;	5.  Subtact or add enough SFC to SSC to force rc ==> 0.
; PROGRAMS CALLED
;	get_leak_image, sxt_cen, shift_res, get_rb0p, gt_pix_size, gt_res,
;	blank_circle, gt_expdur, total, abs
; HISTORY
;	16-Aug-99 LWA, Mod. of cfix.pro to correct for x-ray scatter.
;       10-Jan-2000 - S.L.Freeland - case where user supplied (avoid get_leak_image call)
;-

start_time = systime(1)
run_time = 0.

; IF NECESSARY, READ IN THE LEAK DATA
case 1 of
   data_chk(lkind,/struct) and data_chk(lkdat,/nimage) eq 1: imap=0
   n_elements(imap) eq 0: get_leak_image, index,lkind,lkdat,imap
   else:
endcase

; SET UP THE SUNCENTER AND SOLAR RADIUS ARRAYS

sunr = 1.1	;Hardwired radius for disk intensity.
refr = 1.7	;Hardwired radius outside of which signal should be 0.

xy = sxt_cen(index)
for i=0,1 do xy(i,*) = xy(i,*)/(2.*gt_res(index))
rr = get_rb0p(index,/radius)
rr = rr/gt_pix_size(index)
out = data
yn = fltarr(n_elements(index))

for i = 0,n_elements(index)-1 do begin
   img = data(*,*,i)
   res = gt_res(index(i))
   if res eq 0 then begin
      out = 0
      print,'  *** HALT, PROGRAM ONLY GOOD FOR QR AND HR FFIs, YOU '
      print,'  *** HAVE INCLUDED AN FR IMAGE IN THE INPUT.	
      return, out
   endif 
   iisun = blank_circle(512/res,512/res,xy(0,i),xy(1,i),sunr*rr(i),720.,/image)
   sun = total(img*(iisun ne 1))/n_elements(where(iisun ne 1))  ;Disk signal.
   refimg = blank_circle(512/res,512/res,xy(0,i),xy(1,i),refr*rr(i),/image)
   refimg(*,0:10) = 1		;Eliminate edge effects.
   refimg(0,*) = 1
   refimg((512/res)-1,*) = 1
   iiref = where(refimg ne 1)
   ref = total(img*(refimg ne 1))/n_elements(iiref)  ;Corner signals.
   scat = 0.00657*sun^0.962189		;Correct corner sigs for scatter. 
   ref = ref-scat	;Should be zero if leak_sub was perfect.
   if abs(ref) gt .01 then begin
      img = gt_expdur(index(i))*lkdat(*,*,imap(i))/gt_expdur(lkind(imap(i)))
      lkref = total(img*(refimg ne 1))/n_elements(iiref)  ;Corner signals in SFC.
      out(0,0,i) = data(*,*,i) - (ref/lkref)*img  ;2nd order leak corr.
      yn(i) = -ref	;Average correction in reference areas, DN/pix.
   endif
   if keyword_set(debug) then stop
endfor

end_time = systime(1)
run_time = (end_time-start_time)/60.
print, 'SXT_DELEAK took: ', run_time, ' minutes to process your request'

return, out

end
