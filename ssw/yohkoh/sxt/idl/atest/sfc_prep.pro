function sfc_prep, index, data, image, index_out=index_out, $
		   testing=testing
;+
; NAME
;	sfc_prep
; PURPOSE
;	To prepare a smoothed SFC image for straylight correction.
; CALLING SEQUENCE
;	sfc_image = sfc_prep(sft_index,sft_data,image)
; INPUT
;	sft_index   -index structure for selected SFT image
;	sft_data    -selected SFT image
; OUTPUT
;	Smoothed SFC image (floating point).
; OPTIONAL OUTPUT
;	image	    - dark corrected input image
;       index_out   - index of the dark corrected image
; PROCEDURE
;	1.  Dark_sub with /dc_intepolate
;	2.  Smooth with SFC_DE_SPIKER (median filter)
;	3.  Choose width for boxcar-average smoothing
;	4.  Boxcar-average smooth with BIG_SMOOTH
; RESTRICTIONS
;	Can only process one image at a time
;	Slow -- takes about 1 minutes to process one image on MIPS machine.
;	Calls LWA/ucon programs SFC_DE_SPIKER and BIG_SMOOTH.
; HISTORY
;	LWA, 11-Dec-94  Written
;	LWA, 18-Dec-94  Took out one despike and added variable
;		smoothing width -- based on tests with Al.1
;		and added image keyword
;	LWA, 20-Dec-94, Added test for ND filter for smoothing.
;	SLF, 18-nov-95, SLF, Add Al.1 grill correction per LWA
;       NN,   7-Aug-97, Added index_out from dark_sub
;       SLF, 18-Feb-98, Merged ORBIT_CORRECT SDC with NN changes
;       SLF,  3-Dec-98, Use time dependent (entrance filter era)
;                       dependent GRILL correction, add /TESTING
;-

testing=keyword_set(testing)
; Subtract dark signal from straylight image.

if not testing then begin 
   out=dark_sub(index,data, index_out, /dc_interpolate,/float, /orbit_correct)
      image=fix(out)

;  First stage smoothing and despike of straylight image.

   out=sfc_de_spiker(out,3)

;  Set boxcar smoothing width and smooth straylight image

    width=fix(10^(1.391946-0.0010278969*(max(out)))+0.5)
    if width lt 3 then width=3
    if (gt_filta(index) eq 6 and gt_res(index) eq 1) then width=8
    if (gt_filta(index) eq 6 and gt_res(index) eq 2) then width=4

   out=big_smooth(out,width)

endif

;;;;;;;;;;;;;;;;; NEW AL.1 GRILL CORRECTION STUFF;;;;;;;;;;;;;;;;;;;

; updated 3-December-1998 - S.L.Freeland - time/entrance era dependent file

if (ssw_time_compare(index,(get_yo_dates(/ent))(2),/later)  AND	$	 ; after 2nd ent.
   (gt_filta(index) eq 1) and (gt_filtb(index) eq 2) ) OR testing then begin  ; Al.1/open
   grill_files=file_list('$DIR_SXT_CALIBRATE','al_grill_*.genx') ; grill files
   if grill_files(0) ne '' then begin
      earlier=ssw_time_compare(file2time(grill_files),index,/earlier)
      ss=(reverse(where(earlier)))(0)>0
      restgen,file=grill_files(ss),grill,inq=testing   ; read grill
      box_message,['sfc_prep; Applying GRILL correction for', get_info(index,/non), $
		   'Using grill file: ' + strsplit(grill_files(ss),'/',/last,/tail)]
      if not testing then out=out+(grill*(gt_expdur(index)/2.668e3))      
   endif else message,/info,"Cannot find Al.1 grill files"
endif
;;;;;;;;;;;;;;;;; END OF GRILL CORRECTION STUFF

if testing then stop,'testing...'
return,out

end

