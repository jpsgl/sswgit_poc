function sxl2radiance,edges,dtime,index,flux,indata=indata, $
	sttime=sttime,endtime=endtime, $
	te_corr=te_corr,irradiance=irradiance, $
	photon=photon,erg=erg,debug=debug,loud=loud,new=new

;+
; NAME
;	sxl2radiance
; PURPOSE
;	To calculate the spectral radiance at the sun in a band from SXT signals 
;	averaged over specified time intervals.  Assumed spectrum is from
;	the temperature derived from Al.1/AlMg filter ratio for the same
;	time interval.  Convert to irradiance if keyword set.
; CALLING EXAMPLES
;	output_structure=sxl2radiance([10,18],24.*60.*60.)
;	out=sxl2radiance(edges,dtime,index,flux,indata=indata, $
;		sttime=sttime,endtime=endtime,te_corr=te_corr, $
;		photon=photon,erg=erg,debug=debug,loud=loud, $
;		irradiance=irradiance,new=new)
; INPUT
;	edges, 1-d array of wavelengths (A) defining bands for flux ratio analysis
;	  Note:  Band limits must lie within the range 1.26867-298.029 A.
;	dtime, length of averaging time intervals (seconds)
; OPTIONAL INPUT
;	index, index structure from sxl_analysis.pro
;	flux, signal structure from sxl_analysis.pro
; OPTIONAL KEYWORD INPUT
;       indata, name of .genx file with the sxl fluxes and index
;		required if index and flux are not passed in.
;	sttime, starting time of desired analysis interval
;	endtime, ending time of desired analysis interval
;     Note:  If sttime or endtime are not provided analysis refers
;		to index for the start and/or end time.
;	/photon, do computations in photons 
;	/erg,    do computations in ergs (default)
;	/debug, stops program just before end
;	/loud,  prints out a lot of results to screen
;	/new,  force reading of sxl data from SXT database
;	/irradiance, output irradiance, i.e., (flux/cm^2/s) at earth.
; OUTPUT 
;	IDL structure containing:
;		time, day:	structure times of midpoints of intervals
;		fmttim:		ascii time of midpoints of intervals
;		units:	ergs/cm^2/s, ergs/s, photons/cm^2/s, or photons/s
;		temp:	alog10(temperature), [K]
;		em:	alog10(emission measure), [cm^-3]
;		interp:	=0 for temp & em values computed from data
;			=1 for interpolated temp & em values
;		al:	SXT Al.1 average full-disk signal for interval (DN/s)
;		almg:	SXT AlMg average full-disk signal for interval (DN/s)
;		band:	edges in angstrom of spectral band(s)
;		rad:	spectral radiance(s) for the specified band(s)
;	  			in photons or ergs per wavelength bin
;	Example:	** Structure <40373688>, 10 tags, length=944, refs=1:
;			   TIME            LONG          43200000
;			   DAY             INT           5672
;			   FMTTIM          STRING    '12-JUL-94  12:00:00'
;			   UNITS           STRING    'photons/s'
;			   TEMP            FLOAT           6.35518
;			   EM              FLOAT           48.8143
;			   INTERP	   INT		 0
;  			   AL              FLOAT       4.75468e+06
;			   ALMG            FLOAT       2.36557e+06
;			   EDGES           FLOAT     Array[111]
;  			   RAD             FLOAT     Array[110]
; OPTIONAL KEYWORD OUTPUT 
;	te_corr, array of algorithm-corrected AlMg/Al.1 temperatures
; PRINTED OUTPUT
; RESTRICTIONS
;	Oribital variation of AU ignored in computing irradiance.
; PROGRAMS CALLED
;	sxl_analysis, sxl_select, sxt_flux, sxt_teem2,
;	sxt_sig_per_dn_lwa, stdev
; HISTORY
;	14-Jul-99  LWA  Created from sxl2radiance.pro.
;	19-Jul-99  LWA  Temporary fix to for-loop to deal with JUMP.
;	 3-Aug-99  LWA  Derive temperatures from sxt images out to
;			Rsun=1.1 only, to improve signal to noise.
;	 2-Nov-99  LWA  Deleted all unused junk still in sxl2rad3.pro.
;	 3-Nov-99  LWA  Changed step 2 to force use of right date.
;			Added irradiance keyword.
;      	 4-Nov-99  LWA  Added code to properly handle leak-era boundaries.
;                       Pad arrays to make alout and dgout same length.
;	18-May-00  LWA  Changed all "for loops" to data type long.
;	19-May-00  LWA  Installed check to assure data series are same length.
;	24-Jan-01  LWA  Renamed from sxl2rad5.pro
;-

; *********************************************************
;  Step 0:  What are the units to use?
; *********************************************************
if keyword_set(photon) and keyword_set(erg) then begin
  print,' **** Error in sxl2radiance ****',string(7b),string(7b)
  print,'      You cannot specify both /photon and /erg'
  stop 
endif else if keyword_set(photon) then Units=0 else Units=1

; *********************************************************
;  Step 1:  Either input SXL flux and index or get the
;       data with sxl_analysis.pro if index, and flux
;	have not been passed in.
; *********************************************************
if keyword_set(new) or n_elements(index) eq 0 or $
	n_elements(flux) eq 0 then begin
   if keyword_set(indata) then begin
      restgen,file=indata,index,flux
   endif else begin
      sxl_analysis,sttime,endtime,index,flux,filter=[2,3],res=[1,2]
   endelse
endif
ni=n_elements(index)
if n_elements(sttime) eq 0 then sttime=anytim2ints(index(0))
if n_elements(endtime) eq 0 then endtime=anytim2ints(index(ni-1))

; *********************************************************
;  Step 2:  Process SXL fluxes and compute the averages.
; *********************************************************
al=where(gt_filtb(index) eq 2)		; First, the thin-Al.
alout=sxl_select(index(al),flux(0,al),dtime,start=sttime,$
	endt=endtime,tout=altout,ok=alok)  
; Note:  flux(0,al(alok)) are the valid Al.1 data.

dg=where(gt_filtb(index) eq 3)
dgout=sxl_select(index(dg),flux(0,dg),dtime,start=sttime,$
	endt=endtime,tout=dgtout,ok=dgok)

; Make certain the 2 data series are the same length
if n_elements(alout) ne n_elements(dgout) then begin
   if n_elements(alout) lt n_elements(dgout) then begin
      dgout = dgout(lindgen(n_elements(alout)))
      dgtout = dgtout(lindgen(n_elements(alout)))
   endif else begin
      alout = alout(lindgen(n_elements(dgout)))
      altout = altout(lindgen(n_elements(dgout)))
   endelse
endif

okal=where(alout ne -1)		;Intervals with Al.1 data.
okdg=where(dgout ne -1)		;Intervals with AlMg data.
ok=[okal,okdg]
ok=ok(uniq(ok,sort(ok)))	;All intervals with data.
okal2=kill_index(okal,okdg)  	;Intervals with only Al.1 data
if keyword_set(debug) then begin
   print,'  STOP #1'
   stop
endif

; *********************************************************
;  Step 3:  Derive the two-filter ratio temperatures
;	and emission measures.
; *********************************************************
waste =  sxt_flux(6.2,2)	;Run to set up common block
; date=sttime			;11/3/99

; First, pick pairs of Al.1, AlMg images within 5 minutes of one another.
choose_pairs,index(al(alok)),index(dg(dgok)),300.,alii,dgii
 
; Limit data to that inside R=1.05*Rsun.
aldat=reform(flux(0,al(alok(alii))))-reform(flux(1,al(alok(alii))))
dgdat=reform(flux(0,dg(dgok(dgii))))-reform(flux(1,dg(dgok(dgii))))

; Derive Te and EM from these pairs of images.
; Check for entrance filter era as you go along.
nd=n_elements(aldat)
te=fltarr(nd)
em=fltarr(nd)
valid=intarr(nd)
alindex=index(al(alok(alii)))
gg = sxt_chk_era(alindex)
if min(gg) eq -1 then gg=[0,nd] else gg=[0,gg,nd]
ng = n_elements(gg)
for i = 0L,ng-2 do begin
   ggg=gg(i)+indgen(gg(i+1)-gg(i))
   sxt_teem2,2,aldat(ggg),3,dgdat(ggg),$
	te0,em0,dte,dem,valid0,/interp,date=anytim2ints(alindex(ggg(0)))
   te(ggg)=te0
   em(ggg)=em0
   valid(ggg)=valid0
endfor

ii = where(valid eq 0,nv)
if nv gt 0 and nv lt n_elements(te) then begin
   te=te(ii)
   em=em(ii)
endif
if nv eq 0 then begin 
   print,'  *** THERE ARE NO DATA VALID FOR COMPUTING T AND EM IN THIS SET ***'
   dun=0
   return,dun
endif

;Average the log10(Te) and log10(EM) over the desired intervals.
divyup,index(al(alok(alii(ii)))),te,start=sttime,dtime,te_sxt,tout
divyup,index(al(alok(alii(ii)))),em,start=sttime,dtime,em_sxt
nt=tim2dset(tout,endtime)
if keyword_set(loud) then print, 'nt = ',nt

if nt+1 lt n_elements(tout) then begin
   te_sxt=te_sxt(lindgen(nt))	;Clip off extra time intervals
   em_sxt=em_sxt(lindgen(nt))
endif

;Interpolate the missing temperatures.
xx=where(te_sxt ne -1,nnxx)
if keyword_set(debug) then begin
   print,'  STOP #2'
   stop
endif

if n_elements(nnxx) lt n_elements(te_sxt) and nnxx gt 0 then begin
   texx=interpol(te_sxt(xx),xx,indgen(n_elements(altout)))
   ll=where(indgen(n_elements(altout)) lt min(xx),nnll)
   if nnll gt 0 then texx(ll)=te_sxt(min(xx))
   mm=where(indgen(n_elements(altout)) gt max(xx),nnmm)
   if nnmm gt 0 then texx(mm)=te_sxt(max(xx))
   emxx=interpol(em_sxt(xx),xx,indgen(n_elements(altout)))
   if nnll gt 0 then emxx(ll)=em_sxt(min(xx))
   if nnmm gt 0 then emxx(mm)=em_sxt(max(xx))
endif else begin
   texx=te_sxt
   emxx=em_sxt
endelse

if keyword_set(debug) then begin
   print,'  STOP #3
   stop
endif

; *********************************************************
;  Step 4:  Loop through the spectral bands and compute 
;	the radiance in each band.
; *********************************************************
nn=n_elements(edges)
bandflux=-1+fltarr(n_elements(altout),nn-1)

for j=0L,nn-2 do begin   ; Loop through the spectral bands

   ; Compute the radiance in the band for each of the intervals,
   ; using the derived or interpolated temperatures.
   ; Check for entrance filter era as you go along.
   ni = n_elements(okdg)
   gg = sxt_chk_era(dgtout(okdg))
   if min(gg) eq -1 then gg=[0,ni] else gg=[0,gg,ni]
   ng = n_elements(gg)
   for i = 0L,ng-2 do begin
      ggg=gg(i)+indgen(gg(i+1)-gg(i))
      sig = sxt_sig_per_dn_lwa(texx(okdg(ggg)),3,edges(j),edges(j+1),$
             erg=units,date=anytim2ints(dgtout(okdg(ggg(0)))))
      bandflux(okdg(ggg),j) = dgout(okdg(ggg))*10^sig
   endfor
   if min(okal2) ne -1 then begin
      ni2 = n_elements(okal2)
      gg = sxt_chk_era(altout(okal2))
      if min(gg) eq -1 then gg=[0,ni2] else gg=[0,gg,ni2]
      ng = n_elements(gg)
      for i=0L,ng-2 do begin
         ggg=gg(i)+indgen(gg(i+1)-gg(i))
         sig = sxt_sig_per_dn_lwa(texx(okal2(ggg)),2,edges(j),edges(j+1),$
              erg=units,date=anytim2ints(altout(okal2(ggg(0)))))
         t_band = alout(okal2(i))*10^sig
         bandflux(okal2(ggg),j) = alout(okal2(ggg))*10^sig
      endfor
   endif
endfor		

; *********************************************************
;  Step 7:  Prepare the output data structure.
; *********************************************************
out = {time:1L,day:1,fmttim:'',units:'', $
	temp:0.0,em:0.0,interp:1,al:0.0,almg:0.0, $
	edges:fltarr(n_elements(edges)), $
	rad:fltarr(n_elements(edges)-1)}
   out = replicate(out,n_elements(alout))
   out.time = altout.time
   out.day = altout.day
   out.fmttim = fmt_tim(altout)
case 1 of
   units eq 0 and keyword_set(irradiance) : kind = 'photons/cm^2/s'
   units eq 0 and NOT keyword_set(irradiance) : kind = 'photons/s'
   units eq 1 and keyword_set(irradiance): kind = 'ergs/cm^2/s'
   units eq 1 and NOT keyword_set(irradiance): kind = 'ergs/s'
endcase
out.units = kind
out.temp = texx
out.em = emxx
out.interp = 1
out(xx).interp = 0 
out.al = -1
for i=0L,n_elements(okal)-1 do out(okal(i)).al = alout(okal(i))
out.almg = -1
for i=0L,n_elements(okdg)-1 do out(okdg(i)).almg = dgout(okdg(i))
out.edges = edges
siz = size(bandflux)
if siz(0) eq 1 then out.rad = bandflux $
   else out.rad = transpose(bandflux)

; Convert to irradiance (units per sqare cm per sec) if desired.
if keyword_set(irradiance) then begin
   ii = where(out.rad  eq -1,nii)
   AU = 1.496e13  
   out.rad = out.rad/(4.*!pi*(AU)^2)
   if nii gt 0 then begin
      bg=out.rad
      bg(ii) = -1
      out.rad = bg
   endif
endif

if keyword_set(loud) then begin
   help,al,alout,altout,alok,okal,okal2,dg,dgout,dgtout,dgok,okdg,ok
   pmm,out.rad
endif

if keyword_set(debug) then begin
   print,'  STOP #4, LAST STOP'
   stop
endif

return, out

end

