pro gtab_summary, sttim0, entim0, outfil=outfil, short=short
;+
;NAME:
;	gtab_summary
;PURPOSE:
;	To list the headers of the SXT sequence tables
;SAMPLE CALLING SEQUENCE:
;	gtab_summary
;	gtab_summary, outfil = outfil [,/short]
;OPTIONAL INPUT:
;	sttim	- The starting time to search/list tables.  If not
;		  passed in then '30-Aug-91' is used
;	entim	- The ending time to search/list tables.  If not passed,
;		  then the current date/time is used.
;OPTIONAL KEYWORD INPUT:
;	outfil 	- The name of the output file.  If not passed, then
;		  the file name will be gtab_summary.txt in the default
;		  directory
;	short   - if set, truncate upload and author (fits on small screen)
;
;HISTORY:
;	Ver 1.0  8-Jul-92 (MDM) 
;	Ver 1.1 29-Apr-93 (MDM) - Modified into a procedure
;				- Added STTIM and ENTIM options
;	         7-Feb-94 (SLF) - prepended fileid to line per lwa request
;		14-Feb-94 (SLF) - truncate leading date from headline(1)
;				  add /short (truncate planned upload)
;	Ver 1.2	 7-Mar-95 (MDM) - Changed the way SLF did trimming of the header
;				  information.  
;-
;
if (n_elements(outfil) eq 0) then outfil = 'gtab_summary.txt'
if (n_elements(sttim0) eq 0) then sttim = anytim2ints('30-Aug-91') else sttim = anytim2ints(sttim0)
if (n_elements(entim0) eq 0) then entim = anytim2ints(!stime)      else entim = anytim2ints(entim0)
;
dir = '$DIR_SXT_TABLES'
ff = file_list( dir, 'btb*', file=file, /cd)
fileid = strmid(file, 3, 20)
daytim = anytim2ints( fid2ex( fileid ) )
ss = sel_timrange(daytim, sttim, entim)
if (ss(0) eq -1) then begin
    print, 'GTAB_SUMMARY: No tables found in the time range of'
    print, fmt_tim(sttim), ' to ', fmt_tim(entim)
    return
end else begin
    fileid = fileid(ss)
    file=file(ss)
end
;
openw, lun, outfil, /get_lun
printf, lun, 'GTAB_SUMMARY   Ver 1.2  7-Mar-95
printf, lun, 'Program Run: ', !stime
printf, lun, ' '
;
last_table = bytarr(4096)
for i=0,n_elements(fileid)-1 do begin
    infil = gtab_file(itab0, ientry0, table=table, itab=itab, ientry=ientry, fid=fileid(i), headline=headline)
    headline = strmid(headline, 7, 63)
    ;headline(1)=strmid(headline(1),5,63)		;MDM removed 7-Mar-95
    str = strmid(file(i),3,11) + ' ' + $
	string(itab, format='(i4)') + ' ' + headline(1) 
    if not keyword_set(short) then str=str + ' ' + headline(2)
    if (total(last_table-table) ne 0) then str = str + '#'
    print, str
    printf, lun, str
    last_table = table
end
;
free_lun, lun
end
