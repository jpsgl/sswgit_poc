;+
; NAME:
;       BCS_24HR_PLOT
;
; PURPOSE:
;       Plots the light curve of the reformatted data for the requested BCS
;       channel as four panels of 6 hours, with SAA and Night indicated.
;
; CALLING SEQUENCE:  
;       BCS_24HR_PLOT,date
;
; INPUTS:
;       date    required date in either format '30-mar-92' or in the
;               format yymmdd, e.g. '920621'
;
; KEYWORDS:
;       chan    requested channel in range 1-4 [Def = 3, Ca XIX]
;       files   if present, the files used are listed
;       nobid   if present, supress blockid list on extreme right
;       pause   pauses at end of each 6hr segment for debug...
;       bda     if present, use available BDA files (Def is OBS log)
;       ref     if present, uses files of ref directories (FLARE machines)
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;
; RESTRICTIONS:
;       uses the common block UTCOMMON from UTPLOT
;       produces common block FILES_24HR,inff,nffq,ffff
;
; PROCEDURE:
;       Reads the ROADMAP from all the files and plots the curve.
;
; MODIFICATION HISTORY:
;       CDP     May 92  Written
;       RDB     Jun 92  Reworked to include SAA and Night, and improve
;                       plot scaling; Fixed some titles; Get first file.
;       RDB  19-Jun-92  Fixed file search for UNIX by using FILE_LIST.
;       RDB  21-jun-92  Various improvments for data gaps
;       RDB  22-Jun-92  Modified plot of YOHKOH_EVENTS arrays
;                       Input date can also be yymmdd format
;                       Extra checks when few data files
;                       FILES switch option
;       RDB  23-Jun-92  Few extra catches for improper data
;                       PAUSE switch option
;       RDB  30-Jun-92  Modified peak search routine
;       RDB   1-Jul-92  Set !p.psym=0 at start in case UTPLOT had screwed up
;       RDB   6-Jul-92  Map the use of BLOCKID in roadmap
;                       NOBID keyword
;       RDB   7-Jul-92  Corrected error in qx logic - file at end of 
;                       day could make code think one at end of this day
;                       Get file headers when FILES switch used
;       RDB   9-Jul-92  Corrected selection of orbit at end of prev. day
;       RDB  22-Jul-92  Added CLEARPLOT at end
;       RDB  30-Oct-92  Increased size of chars on plot
;       RDB  11-Nov-92  Added CLEAR_UTPLOT, and changed way letter size incr.
;       RDB  18-Nov-92  Modified to use OBServing log if /OBS switch used.
;                       Also, supress compressed files (_Z or .Z) when using
;                       the BDA roadmaps.
;       CDP     Apr-93  Tidy header documentation.
;       RDB  26-May-93  Modified to use new OBD files - BCS part of obs. log
;                       Some error message printed if not nobid
;       RDB  13-Jul-93  Made window larger on x-windows display
;                       Corrected fault where files not listed under /obs
;       RDB   6-Aug-93  Switched so uses OBS logs by default. 
;                       Removed /obs, installed /bda
;       RDB  11-Aug-93  Put clearplot, etc at start, those at end conditional
;       RDB  24-Sep-93  Added Channel to main title
;       RDB   4-Jan-94  Still plot page even if no data on given date
;                       Some reorganization in prep. for plotting other instruments.
;       RDB   5-Jan-94  Fixed problem in OBS logs if previous day missing
;       RDB  11-Jan-94  Declare which week data in for OBS plot
;       RDB   7-Mar-94  Modified to fix errors that resulted after updates to UTPLOT...
;       RDB  14-Mar-94  Corrected one of fixes of 7-Mar-94...
;       RDB  17-Nov-94  Corrected logic error in plotting file markers using OBS
;       RDB  10-Feb-95  Improved file seach on flare machines (ref keyword)
;                       Improved plot of saa and night bars
;       RDB  20-Nov-97  Fixed charsize problem of extension names that came in with V5
;       DMZ   1-Mar-02  Corrected serious time conversion problems
;
;-

pro bcs_24hr_plot,dday,chan=chan,files=files,pause=pause,nobid=nobid, $
	bda=bda,ref=ref

;common utcommon,a,b,c,d
;
;common files_24hr,infil,nffq,ffq

ansp = ''               ;string used for pause keyword

clearplot               ;can be left in funny state if utplot goes down
clear_utplot
;;;!p.psym = 0             

bcs = 1
if keyword_set(ref) then bda =1
if keyword_set(bda) then bcs =1

if bcs then begin
  rq_chan = 3
  if keyword_set(chan) then rq_chan = chan
  if rq_chan lt 1 or rq_chan gt 4 then message,'Error in requested channel no.'
endif

ps_long                 ;stretch if postscript plotter
iwin=0
if !d.name eq 'X' then begin
  window,iwin,xsiz=640,ysiz=860
  plot,[0,0],[0,0],/nodata,color=0      ;!!! needed for cursor setup
endif

!p.multi = [0,0,4]      ;put 4 6hr plots on a page
!p.charsize = 1.5
;;!x.charsize = 1.5     ;bigger char sizes
;;!y.charsize = 1.5

;
;       get all file names for this day, and last of previous day in case
;       orbit spans a day
;

day = strupcase(dday)

;       day may also be in form yymmdd, e.g. 920621, if so convert it.
if strlen(day) eq 6 and strmid(day,0,1) eq '9' then begin
   jarr = fid2ex(day)
   day  = strmid(fmt_tim(jarr),0,9)
endif

print,' '
print,'Requested Date: ',day

tarr = timstr2ex(day)
ex2int,tarr,msdoy,nday

;-- check for 4 digit year (DMZ, 1-Mar-02)

year=trim(string(tarr(6)))
if strlen(year) eq 4 then year=strmid(year,2,2)
tarr(6)=year
infil = string(tarr([6,5,4]),format='(3i2.2)') 

;       get date of day before
int2ex,0l,nday-1,tarrm1
;       and the day after
int2ex,0l,nday+1,tarrp1

;
;       setup titles
;
!ytitle = ''
!mtitle = ''
main_tit = ''

if bcs then begin
;;  channels = ['Fe XXVI','Fe XXV','Ca XIX','S XV']
  !y.title = bcs_channels(rq_chan-1) + ' counts/sec'
  main_tit = 'BCS '+ bcs_channels(rq_chan-1) + ' Total Count Data for ' + infil
endif


;
;       Now get the data
;
;;if not keyword_set(obs) then begin              ;/OBS switch not present
if keyword_set(bda) then begin              ;/BDA switch present

print,'Using BDA Roadmaps'
print,' '

if keyword_set(ref) then dpaths = '/yd*/ref*' else dpaths = data_paths()

;       find files for end of previous day
ffm1 = 'bda' + string(tarrm1([6,5,4]),format='(3i2.2)') + '.2*'
qm1  = file_list(dpaths,ffm1)
nqm1 = n_elements(qm1)
if nqm1 eq 1 and qm1(0) eq '' then nqm1 = 0     ;null list
if nqm1 ge 1 then begin
   jz = where(strpos(ffm1,'Z') gt 0)
   if jz(0) ge 0 then print,'Compressed files include: ',ffm1(jz)
   jz = where(strpos(ffm1,'Z') lt 0)            ;look for uncompressed files
   if jz(0) gt 0 then begin
      qm1 = qm1(jz)                             ;must be uncompressed
      nqm1 = n_elements(qm1)
   endif else nqm1 = 0
endif

;       only see day period from previous day orbit if start <70 mins 
;       before 0000 UT, therefore only take files that start after this
if nqm1 ge 1 then begin
   break_file,qm1,disk,dir,file,ext
   jjqm1 = where(ext gt 0.2250)
   if jjqm1(0) gt 0 then begin
      qm1  = qm1(jjqm1)
      nqm1 = n_elements(qm1)
   endif else begin
      qm1  = ''
      nqm1 = 0
   endelse
endif


;       find files for required day
ff0 = 'bda' + infil + '.*'
q0  = file_list(dpaths,ff0)
nq0  = n_elements(q0)
jz = where(strpos(q0,'Z') gt 0)
if jz(0) ge 0 then print,'Compressed files include: ',q0(jz)
jz  = where(strpos(q0,'Z') lt 0)                ;look for uncompressed files
if jz(0) ge 0 then begin
   q0 = q0(jz)                                  ;must be uncompressed
   nq0  = n_elements(q0)
endif else nq0 = 0
if nq0 eq 1 and q0(0) eq '' then nq0 = 0                ;null list

;       form list of all files
ff  = q0
if nqm1 gt 0 then ff = [qm1(nqm1-1),q0]

;       save file name in common
ffq  = strarr(17)
nffq = n_elements(ff)
if nffq eq 1 and ff(0) eq '' then nffq = 0
if nffq ge 1 then ffq(0:nffq-1) = ff


;       print list of files, and reformatter machine on keyword
if keyword_set(files) and nffq gt 0 then begin
   print,ff
   rd_fheader,ff,fheader
   print,string(fheader.st$machine)
endif

if nffq eq 0 then begin
   print,'** No Data for requested date **  ',day
   return
endif

;
;  read the roadmap
rd_roadmap,ff,roadmap

;
;       split and load all file names
break_file,ff,disk,dir,fil,ext
fil = strmid(fil,3,6)

endif else begin                                ;default to OBServing log

   print,'Using Observing Log'
   print,' '


;   anytim2weeks,tarr,'',owe,oyr
;   fweek = string(oyr,owe,format='(i2.2,1h_,i2.2)')

;-- use Y2K compliant "week_loop" (DMZ, 1-Mar-02)

   temp=week_loop(tarr,tarr,week=fweek)
   print,'Data Contained in files for week:  ',fweek
   print,' '

   if bcs then rd_obs,tarr,tarrp1,roadmap,sxtp,sxtf,w_h,fid,/bcs
;??
   if n_elements(roadmap) eq 1 then begin
     print,'** No Data for requested date **  ',day
     return
   end

;   ffw = where(strmid(string(fid.st$fileid),0,6) eq infil)
 
;-- check for yymmdd substring (DMZ, 1-Mar-02)

   check=strpos(string(fid.st$fileid),infil)
   ffw=where(check gt -1, count)
   if count eq 0 then begin
    message,'No matching logs files for requested day',/cont
    return
   endif

   ffwrq = [ffw(0)-1,ffw]
   ff = string(fid(ffwrq).st$fileid)
   break_file,ff,disk,dir,fil,ext
;   print,fil+ext

   nqm1 = 0                                             ;previous day
   if fil(0) lt fil(1) and ext(0) gt 0.2220 then nqm1 = 1

   nq0 = n_elements(where(infil eq fil))                ;current day
;;   filmax = max(nq0)
;;   if nqm1 eq 0 then filmax = filmax-1
;;   fil = fil(0:filmax) & ext = ext(0:filmax)          ;ignore next day
   if nqm1 eq 0 then begin
     fil = fil(1:*) & ext = ext(1:*)
   endif
   filmax = n_elements(fil)-1

;               list filenames if required
   if keyword_set(files) then print,fil+ext,format='(5a15)'
   if keyword_set(pause) then read,'* Continue: ',ansp

   ffq  = strarr(17)
   ffq(0:filmax) = fil+ext
   nffq = n_elements(ff)
endelse

tcnt  = 0 & tt = -1             ;& blockid = 10
if n_elements(roadmap) gt 1 then begin

   tcnts = gt_total_cnts(roadmap)

   tt = roadmap.time/3600./1000. + (roadmap.day - nday)*24.0
   if nqm1 gt 0 and tt(0) gt 20. then tt = tt-24.       ;data start in daym1

endif else print,'*** Some major problem with the data ***'

;;yohkoh_events,[nday-1,nday],tonit,today,tesaa,txsaa,/hours
yohkoh_events,nday,tonit,today,tesaa,txsaa,/hours
print,' '

st = fid2ex(infil+'.0000')
sss = int2secarr(st)

nam_flag = 0            ;flag to put name in different place
ext_nam = ''


;>>             go through the 6 hour segments

for ii=0,3 do begin

;       derive vectors indicating data within this period
tx = where(tt ge 6.0*ii  and tt lt 6.0*(ii+1))
txs = tx                ;save this vector
if n_elements(tx) gt 1 then $
   tx = where(tt ge 6.0*ii  and tt lt 6.0*(ii+1) $
         and roadmap.blockid le 1 and roadmap.dgi gt 0)
qx = where(ext ge 0.06*ii and ext lt 0.06*(ii+1))
if ii eq 3 and nqm1 gt 0 and nq0 gt 0 then begin
   qx = where(ext(1:*) ge 0.18 and ext(1:*) lt 0.2399)
   if qx(0) ge 0 then qx = qx + 1
endif
;;help,tx,qx

;       if first file of this day starts more than 30 mins into the
;       day, data from end of previous day is plotted; use this name
if ii eq 0 and nqm1 gt 0 and nq0 gt 0 then begin
   if ext(1) gt 0.0030 then begin
      ext_nam = ext(0)
      nam_flag = 1
   endif
endif

;       calculate start and end time of this segment
s = sss + 6.0*3600.*ii
e = sss + 6.0*3600.*(ii+1)

trange = fmt_tim(anytim2ints(st,off=[s,e]))

;       look for reformatter problems by printing blockID useage
qblockid = 0
if n_elements(txs) gt 1 and bcs then begin
   qblockid = histogram(roadmap(txs).blockid,min=0,max=5)
   if n_elements(tx) ne n_elements(txs) then $
      print,'Blockid usage: ',qblockid,format='(a,6i7)'
endif


;                       plot the data
if n_elements(tx) gt 1 then begin

   if bcs then cnts = (roadmap(tx).total_cnts(rq_chan-1)*10.)/(roadmap(tx).dgi*0.125)

   pmax = good_max(cnts)
   if pmax le 125. then pmax = 125.
   pmax = pmax*1.2      ;leave room for filenam and night/SAA


;;*   utplot,roadmap(tx),cnts,st,psym=3,xrange=[s,e],xstyle=1,yrange=[0.,pmax]
   utplot,roadmap(tx),cnts,timer=trange,xstyle=1,psym=3,yrange=[0.,pmax]

endif else begin

;;*      utplot,[s,e],[0,0],st,psym=3,xrange=[s,e],xstyle=1
      utplot,trange,[0,0],psym=3,timer=trange,xstyle=1
      xyouts,0.4*(!x.crange(1)-!x.crange(0))+!x.crange(0),0.4*!y.crange(1), $
         'NO DATA',charsize=3
;               if there is data of some type, list blockid usage
      if n_elements(qblockid) eq 6 then begin
         xyouts,0.8*(!x.crange(1)-!x.crange(0))+!x.crange(0), $
           0.7*!y.crange(1),'BID  Frms.'
         for jqb = 0,5 do xyouts,0.8*(!x.crange(1)-!x.crange(0))+!x.crange(0), $
           (0.6-jqb*0.1)*!y.crange(1), $
           string(jqb,qblockid(jqb),format='(i2,i8)'),charsiz=0.8
      endif else begin
         if n_elements(qx) ge 1 and qx(0) ge 0 then begin
            print,'*** Major problem with the data ***'
            if not keyword_set(nobid) then $
               xyouts,0.36*(!x.crange(1)-!x.crange(0))+!x.crange(0), $
               0.2*!y.crange(1),'*** Major problem with the data ***',charsiz=0.9
         endif
      endelse

endelse

;       Plot markers for new files, and write filename
;       If the time of the new file is close to end of 6 hr period, 
;       then output name on the next line

if nam_flag then xyouts,!x.crange(0)+500.,!y.crange(1)*0.85,ext_nam,charsiz=1.0
nam_flag = 0

!p.thick=1
if n_elements(qx) ge 1 and qx(0) ge 0 then begin
    for i=0,n_elements(qx)-1 do begin

;-- correct misuse of int2secarr (DMZ, 1-Mar-02)

       temp=fid2ex(fil(qx(i))+ext(qx(i)))
       d=anytim(temp)
       xval = int2secarr(temp)

;;*       outplot,[xval,xval],[0,!y.crange(1)],st          ;vertical line
       outplot,[xval,xval],[0,!y.crange(1)],d           ;vertical line

       if xval+1800. le !x.crange(1) then begin
          xyouts,xval+500.,!y.crange(1)*0.85,ext(qx(i)),charsiz=1.0
       endif else begin
          ext_nam = ext(qx(i))
          nam_flag = 1
          ;;print,'Output name on next line'
       endelse

    endfor
endif


;       plot the times of SAA and night
!p.thick=3
;;nsaa = n_elements(tesaa)
;;if nsaa gt 0 and nsaa(0) ne -1 then for j=0,nsaa-1 do $
;;;;*   outplot,([tesaa(j),txsaa(j)])*3600.,[.93*!y.crange(1),.93*!y.crange(1)],/data,psym=0
;;   outplot,([tesaa(j),txsaa(j)])*3600.,[.93*!y.crange(1),.93*!y.crange(1)],psym=0,tarr

rr = where((tesaa ge ii*6.-0.6 and txsaa le (ii+1)*6.+0.6),nsaa)
if keyword_set(pause) and nsaa gt 0 then print,tesaa(rr),txsaa(rr)
if nsaa gt 0 and nsaa(0) ne -1 then for j=0,nsaa-1 do $
   outplot,([tesaa(rr(j)),txsaa(rr(j))])*3600.,[.93*!y.crange(1),.93*!y.crange(1)],psym=0,tarr

!p.thick=2
;;nnit = n_elements(tonit)
;;if nnit gt 0 and tonit(0) ne -1 then for j=0,nnit-1 do $
;;;;*   outplot,([tonit(j),today(j)])*3600.,[.96*!y.crange(1),.96*!y.crange(1)],/data,psym=0
;;   outplot,([tonit(j),today(j)])*3600.,[.96*!y.crange(1),.96*!y.crange(1)],psym=0,tarr

rr = where((tonit ge ii*6.-0.6 and today le (ii+1)*6.+0.6),nnit)
if keyword_set(pause) then print,tonit(rr),today(rr)
if nnit gt 0 and tonit(rr(0)) ne -1 then for j=0,nnit-1 do $
   outplot,([tonit(rr(j)),today(rr(j))])*3600.,[.96*!y.crange(1),.96*!y.crange(1)],psym=0,tarr

!p.thick=1

;       label SAA and Night bars and do main title
set_xy,0,1,0,1
xyouts,1.01,0.90,'SAA',charsize=0.6
xyouts,1.01,0.96,'Night',charsize=0.6
if ii eq 0 then xyouts,0.27,1.05,main_tit,charsiz=1.2

;       output filename if near end of the segment
if nam_flag then xyouts,1.01,0.80,ext_nam,charsize=0.6


;       list blockid usage on right, unless supressed
if not keyword_set(nobid) then begin
   if n_elements(qblockid) eq 6 then for jqb = 0,5 do $
      xyouts,1.01,(0.35-jqb*0.05)*!y.crange(1), $
      string(qblockid(jqb),format='(i6)'),charsiz=0.5
endif

if keyword_set(pause) then read,'* Continue: ',ansp

endfor

print,' '


;
;               re-emphasize the name
;   
;;xyouts,0.80,0.0,strmid(fmt_tim(roadmap(0)),0,10),/normal,charsize=1.5
ypos = 0.
if !d.name eq 'PS' then ypos = -0.01
xyouts,0.80,ypos,day,/normal,charsize=1.5
;;if keyword_set(obs) then xyouts,0.15,ypos,'OBS',/normal,charsize=1.2
if not keyword_set(bda) then xyouts,0.15,ypos,'OBS',/normal,charsize=1.2

if keyword_set(pause) then read,'* Continue: ',ansp

;
;  reset display parameters
;
!y.title = ' '
!p.multi = 0
print,' '

if !d.name ne 'X' then begin    ;clearing gives problem if cursors used!!
  clearplot
  clear_utplot
endif

ps_reset

end
