;  IDL common for hardwired parameters used in determination and 
;  interpolation of X-matrix


ndeg = 3	; degree of surface fit polynomial for output inverse X
xdim = 56	; horiz. dimension of reduced X-matrix array from XTTX
ydim = 64	; vertical dimension of reduced X-matrix array from XTTX
ntype = 14	; number of independent calibration polarization measurements
nxraw = 224	; horiz. dimension of original data
nyraw = 1024	; vertical dimension of original data
nxstr = 4	; compression factor for rebin in wavelength direction
nystr = 16	; compression factor for rebin in slit length direction

;  vertical (spectral) ranges within rebinned array for processing of fitting
;  for each CCDSIDE

;  the following ranges of spectral pixels are for "continuum" for determining
;  the crosstalk from intensity into polarization.  In DLSP, they approximately 
;  cover one fringe wavelength (perhaps a little more) and range over the weak 
;  lines to the red of 6302.5 in order to start and end in the continuum.  
;  These are given in terms of spectral pixel index of the merged images after
;  removal of spectrum curvature.
crw1 = 6 & crw2 = 12
