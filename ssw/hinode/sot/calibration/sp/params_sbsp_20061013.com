;  IDL common for hardwired instrument parameters used in calibration
;  of maps
;!!!these values should be made as seldom-changed parameters

;  nxtnd is number of points to which to extend spectral range.  MUST BE POWER OF 2!
	nxtnd = 128

;  search range of pixels for cross-correlation, used in corshft_sbsp.pro
;  and slitshft_sbsp.pro
	nsearch = 15

;  spectral pixel range for averaging reference 
;  variation of continuum along slit, used in slitshft_sbsp.pro
	cc1 = 6 & cc2 = 12

;  number of pixels at ends of array to be apodized with cosine bell, used
;  in slitshft_sbsp.pro
;  !! Not used for Solar-B
;	napod = 50

;  range of spectral pixels to cross-correlate, isolate strong lines, used in 
;  specshft_sbsp.pro
	wc1 = 10 & wc2 = 108

;  hardwired CCD pixel range along the slit for fitting curvature, used in 
;  specross_sbsp.pro
	sr1 = 20 & sr2 = 1000

;  hardwired spectral range around spectrum lines for cross-correlation, 
;  used in specross_sbsp.pro
	wl1 = 10 & wl2 = 108

;  Define the range of CCD pixels along the slit to include in the final 
;  display of calibrated Stokes spectra to avoid vignetted areas, etc.
	dsx1 = 1
	dsx2 = 1023

;  Active spectral pixel range of area after merging for removing spectral
;  curvature.  This range avoids vignetted areas on either extreme 
;  of the spectrum.
	msh1 = 1
	msh2 = 111

;  fixed image ranges for two polarization image areas to process spectrally.
;  ranges outside this are are flat-fielded without regard to division by
;  spectral lines. Both CCDSIDEs should have the same number of pixels 
;  in each dimension.
;  sp1 refers to bottom of spectrum for each side (bottom, top spectral images)
;  spectral ranges sp1,sp2 are selected so as the lines occur at roughly
;  the same pixels in each image
	sp1 = [1,1]  ;  bottom (start) of spectrum for each CCDSIDE
	sp2 = [111,111] ;  top (end) of spectrum for each CCDSIDE
	sl1 = [1,1]     ;  left (start) along slit for each CCDSIDE
;  lo res has some vignetting at top, so cut it at 640
	sl2 = [1023,1023] ;  right (end) along slit for each CCDSIDE

;  Range of spectral pixels of CCDSIDE1 to shift spectrally in order to
;  merge the two CCDSIDES.  Range is restricted as such to avoid vignetted 
;  areas, which are left unshifted.
	ssh1 = 1
	ssh2 = 111

;  For fringe correction after merging and removing spectral curvature,
;  set weights for fitting fringes +/-12 pixels on either side of 
;  each solar line to zero so that the line polarization does not bias
;  the fringe fitting.
	lc1 = 78	;  center of 6301 line
	lc2 = 125	;  center of 6302 line
	wss = 12	;  range of pixels about line to omit from fringe fit
	polsmr = 7	;  boxcar averaging width of spectra before fringe fit
	polimt = .006	;  wavelength integration of polarization limit below
			;  which full spectrum is fitted for fringes
	frfrq = 0.115	;  fringe spectral frequency, radians/pixel
	boxsmth = 51	;  boxcar smoothing range for fringe coefficients, pixels
	vlimit = 0.002	;  limit on amplitude of Stokes V polarization fringes
	tpi = 2.*!pi
	mfltrng = 17    ;  range of pixels for median filtering of spikes
	guesq = [0.0006,1.5, 0.0016]	; initial guess for fringes in Q,U,V
        guesu = [0.001,-1.5,-0.0008]	; parameters = [amplitude, phase
        guesv = [0.0007,-0.7, 0.0008]   ; (radians), offset]
