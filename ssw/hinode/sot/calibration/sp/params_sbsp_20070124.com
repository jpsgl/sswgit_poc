;  IDL common for hardwired instrument parameters used in calibration
;  of maps
;!!!these values should be made as seldom-changed parameters
;  Note that the pixel indices along the slit direction are reversed from
;  those during pre-launch testing.  The images are now reversed in 
;  the vertical direction during reformatting.  Also, pixel indices in 
;  the header like SPCCDIX0, SPCCDIX1 refer to the pre-launch CCD coordinates,
;  not the coordinates of LEVEL0 data.

;  constants for HINODE SP
	cccc = 2.99792e8	;  speed of light, m/sec
	disper = 21.549		;  pixel spectral spacing, mA
	wavl = 6302.0		;  wavelength center of bandpass, A

;  nxtnd is number of points to which to extend spectral range.  MUST BE POWER OF 2!
	nxtnd = 128

;  search range of pixels for cross-correlation, used in corshft_sbsp.pro
;  and slitshft_sbsp.pro
	nsearch = 15

;  spectral pixel range for averaging reference 
;  variation of continuum along slit, used in slitshft_sbsp.pro
	cc1 = 6 & cc2 = 12

;  number of pixels at ends of array to be apodized with cosine bell, used
;  in slitshft_sbsp.pro
;  !! Not used for Solar-B
;	napod = 50

;  range of spectral pixels to cross-correlate, isolate strong lines, used in 
;  specshft_sbsp.pro
	wc1 = 10 & wc2 = 108

;  hardwired CCD pixel range along the slit for fitting curvature, used in 
;  specross_sbsp.pro.  Also used in calib_sbsp.pro to avoid ends of array
;  when finding residual spectral shifts between ccdsides.  Given in terms
;  of LEVEL0 data image coordnates
	sr1 = 20 & sr2 = 1000

;  hardwired spectral range around spectrum lines for cross-correlation, 
;  used in specross_sbsp.pro
	wl1 = 10 & wl2 = 108


;  Active spectral pixel range of area after merging for removing spectral
;  curvature.  This range avoids vignetted areas on either extreme 
;  of the spectrum.
	msh1 = 1
	msh2 = 111

;  fixed image ranges for two polarization image areas to process spectrally.
;  ranges outside this are are flat-fielded without regard to division by
;  spectral lines. Both CCDSIDEs should have the same number of pixels 
;  in each dimension.
;  sp1 refers to bottom of spectrum for each side (bottom, top spectral images)
;  spectral ranges sp1,sp2 are selected so as the lines occur at roughly
;  the same pixels in each image
	sp1 = [1,1]  ;  bottom (start) of spectrum for each CCDSIDE
	sp2 = [111,111] ;  top (end) of spectrum for each CCDSIDE
;  the following given in terms of LEVEL0 image coordinates
	sl1 = [0,0]     ;  left (start) along slit for each CCDSIDE
;  lo res has some vignetting at top, so cut it at 640
	sl2 = [1021,1021] ;  right (end) along slit for each CCDSIDE

;  Range of spectral pixels of CCDSIDE1 to shift spectrally in order to
;  merge the two CCDSIDES.  Range is restricted as such to avoid vignetted 
;  areas, which are left unshifted.
	ssh1 = 1
	ssh2 = 111

;  Parameters for finding and rectifying the thermal spectral drift, and
;  finding the thermal drift along the slit.  Used in thermd_sbsp.pro, 
;  sp_prep.pro, calib_sbsp.pro
	stdlnctr = 29.0	; standard line center after corrrection, 6301.5
	ondisk = 1000.	; minimum slit-averaged continuum intensity for 
			; cross-correlation for drift along slit.  This
			; value is for standard map 4.8 sec integration
	dsknorm= 5000.	; minimum continuum intensity for normalization of
			; the 2nd derivative of the continuum intensity.
			; For intensities < dsknorm, normalize by dsknorm
			; in order to not enhance noise of 2nd derivative in
			; umbra.  This value is for standard map 4.8 sec integration
	nlag	= 41	; search range for cross-correlation of 2nd derivatives
			; is +/- nlag/2
	napod2dr= 15	; apodization range for 2nd derivative to condition the
			; end along the slit.  This avoids vignetting, etc.
	avoidumb= 0.6	; avoid umbrae when correcting for residual crosstalk
			; in calib_sbsp.pro, thermd_sbsp.pro when intensities 
			; are less than this factor times the median along the slit
	avoidpen = 0.90	; same as avoidumb, but for avoiding most of thepenumbra, 
			; but accepting granular lanes (Kubo-san recommendation)
	mfltrang= 15	; range for median filtering of thermal drifts, pixels,
			; used in sp_prep.pro
	nsmoothr= 50	; smoothing range for thermal drifts, used in sp_prep.pro
