
pro go_minime_sot_errors, sttim, entim, days1=days1, days2=days2, $
       use_exist_err_struct=use_exist_err_struct, $
       use_exact=use_exact, only_changes=only_changes, $
       n_err_arr=n_err_arr, err_struct=err_struct, jmod=jmod, outpath=outpath, do_write=do_write, $
       verbose=verbose

if not exist(sttim) then sttim = anytim('22-oct-2006', /ccsds)
if not exist(entim) then entim = anytim(ut_time(), /ccsds)
if not exist(days1) then days1 = 1
if not exist(days2) then days2 = 1
if not exist(imod) then imod = 5
if not exist(jmod) then jmod = 5 ; 1
if not exist(mess_days) then mess_days = days1
if not exist(mnem_arr) then mnem_arr = ['fpp_err_code', 'fpp_err_count']
if not exist(prefix) then prefix = 'sot_errors_'
;if not exist(outpath) then outpath = '~/data/sot_genxcat_minime_sot_errors'
if not exist(outpath) then outpath = '$SSW/hinode/sot/idl/monitoring/sot_error_genxcat'
if not file_exist(outpath) then mk_dir, outpath

if ~keyword_set(use_exact) then $
   tg1 = anytim(timegrid(sttim, entim, days=days1), /ecs, /date) else $
   tg1 = anytim(sttim, /ecs)

ng1 = n_elements(tg1)
;PRSTR, TG1
;STOP, 'CHECKING TG1 BEFORE CONTINUING.'

if ~keyword_set(use_existing_err_struct) then delvarx, err_struct

for i = 0,(ng1-2)>0 do begin

   if ~keyword_set(use_exact) then $
      tg2 = anytim(timegrid(tg1[i], tg1[i+1], days=days2), /ecs, /date) else $
      tg2 = anytim(entim, /ecs)

   ng2 = n_elements(tg2)
;PRSTR, TG2
;STOP, 'CHECKING TG2 BEFORE CONTINUING.'

   delvarx, err_struct0, err_struct
   for j = 0,(ng2-2)>0 do begin

      if ~keyword_set(use_exact) then begin
         dates_hk = time2file(tg2[j], /date_only)
      endif else begin
         dates_hk = all_vals(time2file([anytim(sttim, /ccsds), anytim(entim, /ccsds)], /date_only))
      endelse
      
      filnams_hk = dates_hk + '.0x0544'
;     if file_exist(concat_dir('/net/kokuten/archive/hinode/sot/hk/0x0544', filnams_hk)) then begin
      exist_arr_hk = file_exist(concat_dir('/archive1/hinode/sot/hk/0x0544', filnams_hk))
      ss_exist_hk = where(exist_arr_hk eq 1, n_exist)
      if n_exist gt 0 then begin
         filnams_hk = filnams_hk[ss_exist_hk]
;PRSTR, FILNAMS_HK
;STOP, 'CHECKING FILNAMS_HK BEFORE CONTINUING.'

         delvarx, mnem_struct0, err_struct0, err_time_full, err_num_dec_full, err_num_diff_full
         delvarx, err_time, err_num_dec, ss_code_change_arr, n_code_change, ss_cnt_change_arr, n_cnt_change

         if ~keyword_set(use_exact) then $
            mnem_struct0 = sag_get_mnem(tg2[j], tg2[j+1], mnem_arr) else $
            mnem_struct0 = sag_get_mnem(sttim, entim, mnem_arr)
         err_time_full = anytim(mnem_struct0.(0).daytime, /ints)
         err_num_dec_full = reform(mnem_struct0.(0).value[*,0])
         err_num_diff_full = err_num_dec_full[1:*] - err_num_dec_full
         ss_code_change_arr = where(err_num_diff_full ne 0, n_code_change) + 1

         err_num_hex_full = strlowcase(to_hex(err_num_dec_full, 4))

         err_cnt_arr = reform(mnem_struct0.(0).value[*,1])
         err_cnt_diff_arr = err_cnt_arr[1:*] - err_cnt_arr
         ss_cnt_change_arr = where(err_cnt_diff_arr ne 0, n_cnt_change) + 1

         if ~keyword_set(only_changes) then begin
            err_time = err_time_full
            err_num_dec = err_num_dec_full
            err_num_hex = err_num_hex_full
            err_struct0 = get_err_info(err_num_hex_in=err_num_hex, verbose=verbose, _extra=_extra)

            err_struct0 = join_struct(err_time, err_struct0)
            if not exist(err_struct) then err_struct = err_struct0 else $
               err_struct = concat_struct(err_struct, err_struct0)         
         endif else begin
            if (n_cnt_change gt 0) then begin
               err_time = err_time_full[ss_cnt_change_arr]
               err_cnt = err_cnt_arr[ss_cnt_change_arr]
               err_num_dec = err_num_dec_full[ss_cnt_change_arr]

               err_num_hex = err_num_hex_full[ss_cnt_change_arr]

               err_struct0 = get_err_info(err_num_hex_in=err_num_hex, $ ; err_cnt=err_cnt, $
                                          verbose=verbose, _extra=_extra)

               err_struct0 = join_struct(err_time, err_struct0)
;HELP, err_num_dec, err_num_hex, err_struct0
;STOP, 'CHECKING ERR_STRUCT0 BEFORE CONTINUING.'
               if not exist(err_struct) then err_struct = err_struct0 else $
                  err_struct = concat_struct(err_struct, err_struct0)
            endif
         endelse

         if not exist(n_err_arr) then n_err_arr = n_cnt_change else n_err_arr = [n_err_arr, n_cnt_change]

;        if ( (keyword_set(verbose)) and ((j mod jmod) eq 0) ) then $
;           print, ' Finished up through ' + anytim(tg2[j], /ccsds, /date) + '.'
      
         if ( (keyword_set(verbose)) and (n_cnt_change gt 0) ) then $
            print, ' Found ' + strtrim(n_cnt_change,2) + ' errors. Total errors = ' + strtrim(fix(total(n_err_arr)),2)

      endif else begin
     
         if keyword_set(verbose) then begin
            if ~keyword_set(use_exact) then begin
               print, ' No HK .0x544 file found for ' + tg2[j] + '.  Skipping.'
            endif else begin
               print, ' No HK .0x544 file found for any of:'
               prstr, filnams_hk, /no_more
               print, ' Returning.'
            endelse
         endif

      endelse

   endfor

   if ( exist(err_struct) and keyword_set(do_write) ) then begin
      write_genxcat, err_struct, topdir=outpath, prefix=prefix, /day_round, /nelement, /geny

      if keyword_set(verbose) then begin
         if ~keyword_set(use_exact) then $
            print, ' GENXCAT file written for ' + tg1[i] else $
            print, ' GENXCAT file written for interval ' + $
                   anytim(sttim, /ccsds) + ' - ' + anytim(entim, /ccsds)
      endif
   endif else begin
      if keyword_set(verbose) then $
         print, ' No errors found for interval ' + tg2[j-1] + ' to ' + tg2[j]
   endelse
      
   if ~keyword_set(use_exact) then begin
      if ( (keyword_set(verbose)) and ((i mod imod) eq 0) ) then $
         print, ' Finished up through ' + anytim(tg1[i], /ccsds, /date) + '.'
   endif

endfor

end
