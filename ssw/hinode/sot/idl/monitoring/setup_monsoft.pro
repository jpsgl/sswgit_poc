
pro setup_monsoft, fppsw=fppsw

set_logenv, 'FPPSW', '$HOME/soft/idl/fppsw'
set_logenv, 'FPPSW', '/archive/fppsw'

ssw_path, get_logenv('FPPSW') + '/idl', /full_prepend
;add_path, get_logenv('FPPSW') + '/idl/sag'
add_path, get_logenv('FPPSW') + '/idl/fpp_test'

; ---- Define Environmental variables and aliases
set_logenv, 'EGSE_VER',             '5.1'

set_logenv, 'GEN_DBASE_DIR',        '$FPPSW/dbase/cal/info'

set_logenv, 'SAG_HKPLOT_TO_LOCAL',  'yes'

set_logenv, 'SAG_TELEM_INFO',       '$FPPSW/dbase/telemetry'
set_logenv, 'SAG_HK_MAP_FILE',      '$SAG_TELEM_INFO/fpp_hkmap_v001.tab'
set_logenv, 'SAG_DATA_STRUCT',      'sag_fpp_pkt_struct(extens, filnam)'
set_logenv, 'SAG_EGSE_DB_RD_PRO',   'sag_sxi_rd_hk_db"

set_logenv, 'SAG_HK_TIME_USE',      'EGSE'          ; Use EGSE or INST receipt time for HK sample labeling
set_logenv, 'SAG_EGSE_EPOCH',       '1-Jan-70'
set_logenv, 'SAG_INSTRUMENT_EPOCH', '1-Jan-2000'
set_logenv, 'SAG_PKT_HEAD_LEN',     '0'
set_logenv, 'SAG_HK_NMAX_KEY',      '130'

set_logenv, 'GSE_DATABASE_DIR',     '$FPPSW/egse_v6.x/dbs'

; Top of FPP HK data archive:
 set_logenv, 'GSE_FPP_FEP_TM_PKT_FILE_DIR', '/archive1/hinode/sot/hk'
;set_logenv, 'GSE_FPP_FEP_TM_PKT_FILE_DIR', '/net/md5/Volumes/mars/hinode/sot/hk'

set_logenv, 'HINODE_OPS',           '/archive1/hinode'
set_logenv, 'SOT_OPS',              '/archive1/hinode/sot'

set_logenv, 'HOSTNAME',             ''

end

