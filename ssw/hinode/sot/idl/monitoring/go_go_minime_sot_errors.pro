
t_now = anytim(ut_time(), /ccsds)
day_now = anytim(t_now, /date)
sttim = reltime(day_now, days=-100, out='ccsds')
entim = anytim(anytim(t_now) + 86400d0, /ccsds)

prstr, [sttim, entim]

setup_monsoft

go_minime_sot_errors, sttim, entim, n_err_arr=n_err_arr, $
   err_struct=err_struct, /verbose, /only_changes, /do_write

get_sot_errors, n_days_lookback=100, /do_write
get_sot_errors, n_days_lookback= 50, /do_write
get_sot_errors, n_days_lookback= 10, /do_write
get_sot_errors, n_days_lookback=  5, /do_write
get_sot_errors, n_days_lookback=  3, /do_write
get_sot_errors, n_days_lookback=  2, /do_write
get_sot_errors, n_days_lookback=  1, /do_write

end

