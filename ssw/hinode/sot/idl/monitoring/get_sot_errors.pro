
pro get_sot_errors, sttim, entim, n_days_lookback=n_days_lookback, info_new=info_new, $
  err_code=err_code, err_num_hex=err_num_hex, err_num_dec=err_num_dec, err_comment=err_comment, $
  uniq_vals=uniq_vals, help=help, do_print=do_print, do_write=do_write, debug=debug

; TODO: Handle fpp_err_count rollovers

if exist(n_days_lookback) then begin
   entim = anytim(ut_time(!stime), /ccsds)
   sttim = anytim(anytim(entim) - n_days_lookback*86400l, /ccsds)
endif else begin
   n_days_lookback = ceil((anytim(entim) - anytim(sttim))/86400l)
endelse

if not exist(dir_sot_err_db) then $
   dir_sot_err_db = '$SSW/hinode/sot/idl/monitoring/sot_error_genxcat'
;  dir_sot_err_db = '/net/topaz/Users/slater/data/sot_genxcat_minime_sot_errors'

if not exist(outdir_errlist) then $
   outdir_errlist = get_logenv('FPP_ERROR_ARCHIVE')
if not exist(outfil_errlist) then $
   outfil_errlist = concat_dir('/archive1/hinode/sot/sot_errors', $
                                          'error_list_last_' + string(n_days_lookback, format='$(i4.4)') + '_days.txt')
;  outfil_errlist = concat_dir(get_logenv('FPP_ERROR_ARCHIVE'), $
;                                         'error_list_last_' + string(n_days_lookback, format='$(i4.4)') + '_days.txt')

no_error_arr = [' No errors found in this time interval', $
                ' No non-roll-over changes in error count found.', $
                ' No changes in error count found.', $
                ' No new data found.']

;data = sag_get_mnem(sttim, entim, ['fpp_err_code','fpp_err_count'])

; Read sot error structure from geny error database files covering specified time interval:
if ~keyword_set(no_initialize) then initialize = 1
read_genxcat, sttim, entim, err_struct, topdir=dir_sot_err_db, $
              count=count, status=status, initialize=initialize

if (count gt 0) then begin

;  Date      Time      Cnt  Err  Num OpCod Sys  DMA  Addr  SeqID
;  17-MAR-08 09:11:59  229  8074 162 34c3       107  6ac   135d

;  IDL> help, err_struct, /str                                                                                      
;  TIME            LONG           3940000
;  DAY             LONG             13552
;  ERR_NAME        STRING    'SP_OUTPUT_TOUT_ERR'
;  ERR_NUM_HEX     STRING    '00008222'
;  ERR_NUM_DEC     LONG             33314
;  ERR_MESS        STRING    'waiting for previous SP output'

;  Missing: error count, extracted corresponding timeline lines, ted comments

   len_err_name_max = max(strlen(strtrim(err_struct.err_name,2)))
;  t_err = anytim({msec:err_struct.time, d79:err_struct.day}, /ccsds)
   t_err = anytim(err_struct, /ccsds)
   
   err_info_arr =  strmid(anytim(t_err, /ecs), 0, 19)  + '  ' + string(err_struct.err_num_dec, '$(i5.5)') + '  ' + $
                   string(err_struct.err_name, format='$(a-' + strtrim(len_err_name_max,2) + ')') + '  ' + $
                   err_struct.err_mess

endif else begin
;  no_error_hdr = ['================================', $
;                  no_error_arr[0] + string(n_days_lookback, format='$(i4.4)') + ' DAYS', $
;                  '================================', '']
   err_info_arr =  no_error_arr[0]
endelse

out_arr = ['', $
           'FPP Error Listing For Last ' + string(n_days_lookback, format='$(i4.4)') + ' Days', $
           '', $
           'Date       Time      Num    Error Code' + string('', format='$(a8)') + '  Diagnoses', $
           '---------- --------  -----  ----------' + string('', format='$(a8)') + '  ---------']

out_arr = [out_arr, reverse(err_info_arr)]

if keyword_set(do_print) then prstr, out_arr, /nomore
if keyword_set(do_write) then file_append, outfil_errlist, out_arr, /new

if keyword_set(debug) then stop, ' Stopping on request.'

end
