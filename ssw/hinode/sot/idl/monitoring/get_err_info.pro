
function get_err_info, input, err_num_hex_in=err_num_hex_in, err_num_dec=err_num_dec, $
  err_num_hex=err_num_hex, err_name=err_name, err_mess=err_mess, err_struct=err_struct, $
  err_file=err_file, verbose=verbose

if not exist(filnam_err) then begin
; filnam_err = get_logenv('SSW') + '/hinode/sot/idl/util/errors.lis'
  filnam_err = get_logenv('SSW') + '/hinode/sot/idl/monitoring/SOT00024_FPP_Error_List.txt'

  if file_exist(filnam_err) eq 0 then $
    filnam_err = '/hinode/sot/idl/monitoring/SOT00024_FPP_Error_List.txt'
;   filnam_err = '/Users/slater/soft/idl/idl_startup/errors.lis'
;   filnam_err = '/Users/slater/soft/idl_temp/idl_startup/sot/SOT00024_FPP_Error_List.txt'
;   filnam_err = '/net/castor/Users/slater/soft/idl/idl_startup/errors.lis'
endif

buff_raw = rd_tfile(filnam_err)
n_rec_buff = n_elements(buff_raw)
buff_len = strlen(buff_raw)
pos_slash = strpos(buff_raw, '//')
err_mess_arr = strarr(n_rec_buff)
for i=0,n_rec_buff-1 do err_mess_arr[i] = strmid(buff_raw[i], pos_slash[i]+3, buff_len[i]-(pos_slash[i]+3))

buff_col = rd_tfile(filnam_err, /auto, nocomment='%')
err_name_arr = reform(buff_col[1,*])
err_num_hex_arr = reform(buff_col[2,*])
err_num_hex_arr = strlowcase(strmid(err_num_hex_arr,2))

if ( (not exist(input)) and (not exist(err_num_hex_in)) ) then begin

; No data input, then return error table structure:
  err_struct = create_struct('err_name',err_name_arr, 'err_num_hex',err_num_hex_arr, $
                             'err_num_dec',err_num_dec_arr, 'err_mess', err_mess_arr)
  err_struct = ssw_flatten_vecttags(err_struct)
  retval = err_struct
  if keyword_set(verbose) then print, ' No input value.  Returning error table structure.'

endif else begin

  if exist(input) then begin

    if exist(err_num_hex_in) then $
      print, ' Both INPUT argument and ERR_NUM_HEX_IN passed.  Ignoring ERR_NUM_HEX_IN.'
    sz_param = size(input, /type)
    if sz_param eq 8 then begin
; Input is structure.  Assuming hk struct.
      mnem_names = input.(0).mnem
      ss_match = where(mnem_names eq 'fpp_err_code', n_match)
      if n_match eq 0 then begin
        retval = -1
        if keyword_set(verbose) then print, ' input structure does not contain FPP_ERR_CODE mnemonic.  Returning.'
      endif else begin
        err_num_dec = reform(input.(0).value[*,0])
      endelse
    endif else begin
      err_num_dec = input
    endelse
    n_rec_input = n_elements(err_num_dec)

    err_num_hex = str2arr(longhex(err_num_dec), delim=' ')
    if strmid(err_num_hex[0],0,2) eq '0x' then err_num_hex = strmid(err_num_hex,2,20)

  endif else begin

; Only ERR_NUM_HEX_IN passed in

    err_num_hex = strlowcase(err_num_hex_in)
    if strmid(err_num_hex[0],0,2) eq '0x' then err_num_hex = strmid(err_num_hex_in,2)
    n_rec_input = n_elements(err_num_hex)

    if not exist(err_num_dec_in) then begin
      err_num_dec = lonarr(n_rec_input)
      for i=0,n_rec_input-1 do begin
        hex2dec, err_num_hex[i], err_num_dec0, /quiet
        err_num_dec[i] = err_num_dec0
      endfor
    endif else begin
      err_num_dec = err_num_dec_in
    endelse
  endelse

  err_name = replicate('Unknown', n_rec_input)
  err_mess = replicate('Unknown', n_rec_input)
  for i=0,n_rec_input-1 do begin
    ss_match = where(err_num_hex[i] eq err_num_hex_arr, n_match)
    if n_match eq 1 then begin
      err_name[i] = err_name_arr[ss_match]
      err_mess[i] = err_mess_arr[ss_match]
    endif
  endfor

; START HERE (inclusion of 'knowledge base' record matches):
; buff_raw = rd_tfile(filnam_knowledge, /auto, nocomment='%')
; err_knowledge_

; err_explanation = replicate('', n_rec_input)
; for i=0,n_rec_input-1 do begin
;   ss_match = where(err_num_hex[i] eq err_num_hex_arr, n_match)
;   if n_match eq 1 then begin
;     err_name[i] = err_name_arr[ss_match]
;     err_mess[i] = err_mess_arr[ss_match]
;   endif
; endfor

  err_struct = create_struct('err_name',err_name, 'err_num_hex',err_num_hex, $
                             'err_num_dec',err_num_dec, 'err_mess',err_mess)
  err_struct = ssw_flatten_vecttags(err_struct)
  retval = err_struct

endelse

return, retval

end
