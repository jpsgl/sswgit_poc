
function sotct_tim2reset, time, cmd=cmd, t_delta_lo=t_delta_lo, t_first_valid=t_first_valid

;+
;   Name: SOTCT_TIM2RESET
;
;   Purpose: Return closest reset time(s) PRIOR TO input time(s)
;
;   Input Parameters:
;      TIME - Vector of times in any SSW compliant format (see ANYTIM.PRO) or 
;             vector of SSW compliant index structures with DATE_OBS tag
;   Output Paramters:
;      T_RESET - closest reset time(s) PRIOR TO input TIME
;
;   Keyword Parameters:
;      cmd - Command string associated with the CT reset operation
;            (from ORL file)
;      
;   Calling Sequence:
;      IDL> t_reset = sotct_tim2reset(time [, cmd=cmd, t_first_valid=t_first_valid]
;
;   History:
;      29-Aug-2008
;-

if not exist(infil_genx) then infil_genx = concat_dir(get_logenv('SOT_CT_CALIBRATION'),'sotct_reset')
if not exist(t_first_valid) then t_first_valid = '22-feb-2007'

restgenx, t_ct_reset, cmd_ct_reset, file=infil_genx
ss_valid = where(anytim(t_ct_reset) ge anytim(t_first_valid), n_valid)
if n_valid gt 0 then begin
  t_ct_reset = t_ct_reset[ss_valid]
  cmd_ct_reset = cmd_ct_reset[ss_valid]
end else begin
  return, ' No valid reset times in specified database.  Returning.'
endelse

if size(time, /type) eq 8 then begin
  if tag_exist(time, 'date_obs') ne -1 then $
    t_sec = anytim(time.date_obs) else $
    return, ' Unrecognized time format.  Returning.'
endif else begin
  t_sec = anytim(time)
endelse

n_tim = n_elements(t_sec)
t_reset_sec = anytim(t_ct_reset)
t_reset_string = anytim(t_ct_reset, /yoh)

for i=0,n_tim-1 do begin
  tdiff0 = t_reset_sec - t_sec[i]
  ss_lo = where(tdiff0 le 0, n_lo)
  if n_lo gt 0 then begin
    tdiff0_min_lo = min(abs(tdiff0[ss_lo]), ss_close_lo)
    if i eq 0 then begin
      t_reset_close_lo = t_reset_string[ss_lo[ss_close_lo]]
      t_delta_lo = tdiff0_min_lo
      cmd_close = cmd_ct_reset[ss_lo[ss_close_lo]]
    endif else begin
      t_reset_close_lo = [t_reset_close_lo, t_reset_string[ss_lo[ss_close_lo]]]
      t_delta_lo = [t_delta_lo, tdiff0_min_lo]
      cmd_close = [cmd_close, cmd_ct_reset[ss_lo[ss_close_lo]]]
    endelse
  endif else begin
    if i eq 0 then begin
      t_reset_close_lo = 'Invalid'
      t_delta_lo = !values.f_nan
      cmd_close = 'Invalid'
    endif else begin
      t_reset_close_lo = [t_reset_close_lo, 'Invalid']    
      t_delta_lo = [t_delta_lo, !values.f_nan]
      cmd_close = [cmd_close, 'Invalid']
    endelse
  endelse
endfor

return, t_reset_close_lo

end
