
function rd_sot_corr_db, t0, t1, dir_top=dir_top

if not exist(dir_top) then begin
  if not exist(filnam_db) then filnam_db = 'hinode_sdo_alignment' ; _20160411'
  if get_logenv('SOT_CORR_DB') eq '' then $
     set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                           filnam_db)
  dir_top = get_logenv('SOT_CORR_DB')
endif

if not exist(t0) or not exist(t1) then begin
   if not exist(files_genx) then begin
      print, ' Must pass either t0 and t1 OR files_genx.  Returning.'
      return, -1
   endif
endif else begin
   read_genxcat, t0, t1, topdir=dir_top, index_sot_corr, catfiles=files_corr_cat
endelse

t_corr = index_sot_corr.date_obs
t_corr_sec = anytim(index_sot_corr.date_obs)
t0_sec = anytim(t0)
t1_sec = anytim(t1)

ss_good = where( ((t_corr_sec ge t0_sec) and (t_corr_sec le t1_sec)), n_good)
if n_good gt 0 then begin
   index_sot_corr = index_sot_corr[ss_good]
endif else begin
   index_sot_corr = -1
endelse

return, index_sot_corr

end
