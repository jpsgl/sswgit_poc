FUNCTION fg_waveid, index, wave_nomove=wave_nomove
;+
; NAME:
;       FG_WAVEID
;
; PURPOSE:
;       Returns a string with the wavelength in Angstroms
;       corresponding to the header keyword WAVEID. 
;
; CATEGORY:
;	FITS processing
;
; SAMPLE CALLING SEQUENCE:
;	wave = fg_waveid(id [, /wave_nomove])
;
; INPUTS:
;	ID	 - the number of WAVEID or the SSW index structure
;
; OUTPUTS:
;       WAVE     - string with the wavelength in Angstroms
;
; OPTIONAL INPUT KEYWORD PARAMETERS: 
;       WAVE_NOMOVE - if set, get wavelength from the position of FW
;                     in the case of NO_MOVE. The input should be 
;                     the index structure
;
; OUTPUT KEYWORD PARAMETERS: 
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;V0.0  Created by T.E. Berger, LMSAL.
;V0.1  Add WAVE_NOMOVE keyword. 26-Jul-2007. Y. Katsukawa
;-

type = size(index, /type)

if type eq 8 then idnumber = index.waveid else idnumber = index

case idnumber of 
    0: begin
        if not KEYWORD_SET(wave_nomove) or type ne 8 then RETURN, 'BFI_NO_MOVE' $
        else begin
            wbfw = index.wbfw
            if (wbfw ge 156 and wbfw le 159) then RETURN, '3883'
            if (wbfw ge 116 and wbfw le 119) then RETURN, '3968'
            if (wbfw ge 76  and wbfw le 79)  then RETURN, '4305'
            if (wbfw ge 196 and wbfw le 199) then RETURN, '4504'
            if (wbfw ge 236 and wbfw le 239) then RETURN, '5550'
            if (wbfw ge 36  and wbfw le 39)  then RETURN, '6684'
            RETURN, 'UNDEFINED'
        endelse
    end
    1: RETURN, '3883'
    2: RETURN, '3968'
    3: RETURN, '4305'
    4: RETURN, '4504'
    5: RETURN, '5550'
    6: RETURN, '6684'
    7: begin
        if not KEYWORD_SET(wave_nomove) or type ne 8 then RETURN, 'NFI_NO_MOVE' $
        else begin
            nbfw = index.nbfw
            if (nbfw ge 196 and nbfw le 199) then RETURN, '5172'
            if (nbfw ge 156 and nbfw le 159) then RETURN, '5250'
            if (nbfw ge 76  and nbfw le 79)  then RETURN, '5576'
            if (nbfw ge 36  and nbfw le 39)  then RETURN, '6302'
            if (nbfw ge 236 and nbfw le 239) then RETURN, '6563'
            if (nbfw ge 116 and nbfw le 119) then RETURN, '5896'
            RETURN, 'UNDEFINED'
        endelse
    end
;The following are "last offset from Line Center" wavelengths:
    8:  RETURN, '5172'
    9:  RETURN, '5250'
    10: RETURN, '5576'
    11: RETURN, '6302'
    12: RETURN, '6563'
    13: RETURN, '5896'
;The following are "BASE LC wavelengths", not taking into account any offsets:
    18: RETURN, '5172'
    19: RETURN, '5250'
    20: RETURN, '5576'
    21: RETURN, '6302'
    22: RETURN, '6563'
    23: RETURN, '5896'
    
    else: begin
        MESSAGE,'Undefined waveid code. Returning UNDEFINED',/INFORM
        RETURN, 'UNDEFINED'
    end

endcase

END
