
function aia_wave_match, index, mode=mode, ss_axis3=ss_axis3, ss_axis4=ss_axis4, $
  loval_sot=loval_sot, hival_sot=hival_sot, do_invert_sot=do_invert_sot,  nsig_sot=nsig_sot, $
  loval_sdo=loval_sdo, hival_sdo=hival_sdo, do_invert_sdo=do_invert_sdo,  nsig_sdo=nsig_sdo, $
  _extra=_extra

if not exist(mode) then begin
  mode = index.wave + ' ' + index.obs_type + ' ' + string(index.waveoff, format='$(i5.4)')
  if tag_exist(index, 'naxis3') then begin
    if not exist(ss_axis3) then ss_axis3 = index.naxis3-1
    mode = mode + ' ' + string(ss_axis3, format='$(i1.1)')
  endif
  if tag_exist(index, 'naxis4') then begin
    if not exist(ss_axis4) then ss_axis4 = index.naxis4-1
    mode = mode + ' ' + string(ss_axis4, format='$(i1.1)')
  endif
endif

;              mode                                    sdo      lo      hi       inv   nsig  lo    hi    inv   nsig
;                                                      wave     sot     sot      sot   sot   sdo   sdo   sdo   sdo
mode_table = [['6302A SP IQUV 4D array'              , 'unk' ,     '0',    '0',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['CN bandhead 3883 FG (simple)  0000'  , 'cont' ,  '700', '1600',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['Ca II H line FG (simple)  0000'      , '1700',   '500', '1200',  '0',  '4',  '0',  '5000',  '0',  '6'], $
              ['Ca II H line FG focus scan'          , '1700',   '500', '1200',  '0',  '4',  '0',  '0',  '0',  '6'], $
	      ['G band 4305 FG (simple)'             , 'cont',   '900', '2500',  '0',  '3',  '0',  '0',  '0',  '5'], $ ; spots plus granules
	      ['G band 4305 FG (simple)  0000'       , 'cont',   '900', '2500',  '0',  '3',  '0',  '0',  '0',  '5'], $ ; spots plus granules
	      ['G band 4305 FG focus scan'           , 'cont',   '400',  '700',  '0',  '3',  '0',  '0',  '0',  '5'], $ ; only granules
	      ['NFI no move FG focus scan'           , 'cont',   '480',  '800',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['NFI no move FG (simple)'             , 'cont',   '480',  '800',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['NFI no move FG (simple)  0000'       , 'cont',   '480',  '800',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['BFI no move FG (simple)'             , 'cont',   '480',  '800',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['BFI no move FG (simple)  0000'       , 'cont',   '480',  '800',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['TF H I 6563 base FG (simple)'        , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple) -2004'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple) -1172'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple) -0756'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple) -0340'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple)  0076'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple)  0492'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple)  0908'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF H I 6563 base FG (simple)  1740'  , '304' ,   '600', '1100',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF Fe I 5250 base FG (simple) -1300' , 'cont',   '700', '1600',  '0',  '3',  '0',  '0',  '0',  '3'], $ 
	      ['TF Na I 5896 base FG (simple)'       , 'cont',  '1700', '3200',  '0',  '3',  '0',  '0',  '0',  '3'], $ ; FG (simple)
	      ['TF Na I 5896 FG (simple)'            , 'cont',   '700', '1600',  '0',  '3',  '0',  '0',  '0',  '3'], $ ; FG (simple)
              ['TF Na I 5896 FG (simple)  0000'      , 'cont',   '700', '1600',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['TF Na I 5896 FG MG4 V/I'             , 'blos',  '-426',  '574',  '1', '75',  '0',  '0',  '0', '60'], $ ; FG MG4 V/I
              ['TF Na I 5896 FG MG4 V/I  0140'       , 'blos',  '-426',  '574',  '1', '75',  '0',  '0',  '0', '60'], $ ; FG MG4 V/I
              ['TF Na I 5896 FG shuttered I and V 0' , 'cont',  '1700', '3200',  '0',  '3',  '0',  '0',  '0', '60'], $
              ['TF Na I 5896 FG shuttered I and V 1' , 'blos',  '-200',  '200',  '1', '12',  '0',  '0',  '0', '60'], $
              ['TF Na I 5896 FG shuttered IV+DG -0016 2', 'blos',  '-200',  '200',  '1', '12',  '0',  '0',  '0', '60'], $
              ['TF Na I 5896 FG shuttered I and V  0140 1', 'blos',  '-200',  '200',  '1', '12',  '0',  '0',  '0', '60'], $
              ['TF Na I 5896 FG shuttered IV+DG 2'   , 'blos',  '-200',  '200',  '0', '12',  '0',  '0',  '0', '10'], $
              ['TF Fe I 5576 FG (simple)'            , 'blos',  '-200',  '200',  '0', '12',  '0',  '0',  '0', '10'], $
              ['TF Na I 5896 FG shutterless I and V with 0.2s intervals -0172 1', 'blos',  '-200',  '200',  '0', '12',  '0',  '0',  '0', '10' ], $
              ['TF Na I 5896 FG shutterless I and V with 0.2s intervals  0148 1', 'blos',  '-200',  '200',  '1', '12',  '0',  '0',  '0', '10' ], $
              ['TF Fe I 6302 FG shuttered I and V -0120 1', 'blos',  '-200',  '200',  '1', '12',  '0',  '0',  '0', '60'], $
              ['blue cont 4504 FG (simple)'          , 'cont',  '1300', '2650',  '0',  '3',  '0',  '0',  '0',  '3'], $
              ['blue cont 4504 FG (simple)  0000'    , 'cont',  '1300', '2650',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['green cont 5550 FG (simple)'         , 'cont',  '1500', '2600',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['green cont 5550 FG (simple)  0000'   , 'cont',  '1500', '2600',  '0',  '3',  '0',  '0',  '0',  '3'], $
	      ['red cont 6684 FG (simple)'           , 'cont',  '1600', '2500',  '0',  '4',  '0',  '0',  '0',  '3'], $
	      ['red cont 6684 FG (simple)  0000'     , 'cont',  '1600', '2500',  '0',  '4',  '0',  '0',  '0',  '3']]

;             ['CN bandhead 3883 FG (simple)'        , 'unk' ,   '700', '1600',  '0',  '3'], $
;             ['Ca II H line FG (simple)'            , '1700',   '200',  '800',  '0',  '3'], $
;             ['G band 4305'                         , 'cont',   '300', '2500',  '0'      ], $ ; spots plus granules
;             ['TF Na I 5896 FG shutterless I and V with 0.2s intervals 1' , 'cont',  '20000',  '33000',  '0', '2',  '0',  '0',  '0', '5'], $

ss_mode_match = where(mode[0] eq mode_table[0,*], n_mode_match)

;if n_mode_match eq 0 then begin
;  pos_
;  ss_mode_match = where(mode[0] eq mode_table[0,*], n_mode_match)

if n_mode_match eq 1 then begin
  aia_match     =        mode_table[1,ss_mode_match]
  loval_sot     = (float(mode_table[2,ss_mode_match]))[0]
  hival_sot     = (float(mode_table[3,ss_mode_match]))[0]
  do_invert_sot = (fix(  mode_table[4,ss_mode_match]))[0]
  nsig_sot      = (fix(  mode_table[5,ss_mode_match]))[0]
  loval_sdo     = (float(mode_table[6,ss_mode_match]))[0]
  hival_sdo     = (float(mode_table[7,ss_mode_match]))[0]
  do_invert_sdo = (fix(  mode_table[8,ss_mode_match]))[0]
  nsig_sdo      = (fix(  mode_table[9,ss_mode_match]))[0]
endif else begin
;  stop, 'WARNING: AIA_WAVE_MATCH: No match found to ' + strtrim(mode,2) + '.  Stopping.'
  aia_match = ''
endelse

;print, 'mode = ' + mode
;print, 'loval_sot = ' + strtrim(loval_sot)
;print, 'hival_sot = ' + strtrim(hival_sot)
;print, 'do_invert_sot = ' + strtrim(do_invert_sot)
;print, 'loval_sdo = ' + strtrim(loval_sdo)
;print, 'hival_sdo = ' + strtrim(hival_sdo)
;print, 'do_invert_sdo = ' + strtrim(do_invert_sdo)

return, aia_match

end

  





