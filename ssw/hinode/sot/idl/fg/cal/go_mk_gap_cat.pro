
function go_mk_gap_cat, t0, t1, _extra=_extra

if not exist(dir_top) then begin
  if not exist(filnam_db) then filnam_db = 'hinode_sdo_alignment' ; _20160411'
  if get_logenv('SOT_CORR_DB') eq '' then $
     set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                           filnam_db)
  dir_top = get_logenv('SOT_CORR_DB')
endif
  
if not exist(t0) then t0 = '01-jun-2013'
if not exist(t1) then t1 = '01-jun-2016'

t_grid = timegrid(t0, t1, days=1)

n_int = n_elements(t_grid)-1

for i=0,n_int-1 do begin
   gap_cat0 = mk_sot_corr_cat(t_grid[i], t_grid[i+1], n_gap=n_gap, $
                 sttim_gap=sttim_gap0, entim_cont=entim_gap0, dur_gap_sec=dur_gap_sec0, $
                  _extra=_extra)
   if n_gap gt 0 then begin
      if not exist(sttim_gap_arr) then begin
         sttim_gap_arr = sttim_gap0
         entim_gap_arr = entim_gap0
         dur_gap_sec_arr = dur_gap_sec0
      endif else begin
         sttim_gap_arr = [sttim_gap_arr, sttim_gap0]
         entim_gap_arr = [entim_gap_arr, entim_gap0]
         dur_gap_sec_arr = [dur_gap_sec_arr, dur_gap_sec0]
      endelse
   endif
endfor

gap_cat = {sttim_gap:sttim_gap_arr, entim_gap:entim_gap_arr, dur_gap_sec:dur_gap_sec_arr}
STOP
save, gap_cat, file=concat_dir(get_logenv('SOT_CORR_DB'), 'gap_cat.sav')

return, gap_cat

end

