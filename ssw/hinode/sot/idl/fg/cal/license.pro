
function license, server=server, user=user, machine=machine, search_string=search_string, $
  do_sort=do_sort, used=used, free=free, loud=loud, summary=summary

;+
; Example Calls:
;-   

if not exist(idl_dir) then idl_dir = get_logenv('IDL_DIR')

server_arr  = ['sunbeam', 'mailstar']
max_lic_arr = [35, 10]
if not exist(server) then begin
  server_arr  = ['sunbeam', 'mailstar']
  max_lic_arr = [35, 10]
endif else begin
  ss_match_server = where(server eq server_arr, n_match_server)
  server_arr = server
  max_lic_arr = max_lic_arr[ss_match_server]
endelse
n_server = n_elements(server_arr)

if not exist(outfil) then outfil = './license.lis'
if not exist(col) then col = 8

ss_bad_rec_arr = ['HI', 'Users of idl'] ; ad hocs for various machines
n_bad_rec = n_elements(ss_bad_rec_arr)

ss_rem_arr = [', start Mon', ', start Tue', ', start Wed', ', start Thu', $
              ', start Fri', ', start Sat', ', start Sun', $
              '(', ')', ', 6 licenses']
n_rem = n_elements(ss_rem_arr)

if idl_dir ne '' then begin

for j=0, n_server-1 do begin

  cmd = concat_dir(idl_dir, 'bin/lmutil') + ' lmstat -a -c 1700@' + server_arr[j] + $
        ' | grep "6 licenses"'
; if exist(user) then cmd = cmd + ' | grep -i ' + user
  if exist(machine) then cmd = cmd + ' | grep -i ' + machine
  t_cur_ex = anytim(!stime, /ex)
  mon_cur = t_cur_ex[5]
  yr_cur = t_cur_ex[6]
  spawn, cmd, listing

  for i=0,n_bad_rec-1 do begin
    ss_good_rec0 = where(strpos(listing, ss_bad_rec_arr[i]) eq -1, n_ss_good_rec0)
    listing = listing[ss_good_rec0]
  endfor

  for i=0,n_rem-1 do listing = str_replace(listing, ss_rem_arr[i], '')

  file_append, outfil, listing, /new
;buff0 = rd_tfile(outfil)
;tot_license = n_elements(buff0)
  buff = rd_tfile(outfil, col, delim=' ')
  n_rec = n_elements(buff[0,*])
  user_arr = string(buff[0,*], '$(a10)')
  mach_arr = string(buff[1,*], '$(a10)')
  idl_ver_arr = string(buff[3,*], '$(a05)')
  idl_serv_arr = string(buff[4,*], '$(a10)')
; idl_serv_arr = string(buff[4,*], '$(a8)')
  date_arr = buff[6,*]

  mon_arr = strarr(n_rec)
  day_arr = strarr(n_rec)
  yr_arr = strarr(n_rec)
  pos_slash_serv = strpos(idl_serv_arr,'/')
  pos_slash_date = strpos(date_arr,'/')
  for i=0,n_rec-1 do begin
    idl_serv_arr[i] = strmid(idl_serv_arr[i],0,pos_slash_serv[i])
    mon_arr[i] = strmid(date_arr[i],0,pos_slash_date[i])
    day_arr[i] = strmid(date_arr[i],pos_slash_date[i]+1)
    if fix(mon_arr[i]) gt mon_cur then yr_arr[i] = yr_cur-1 else yr_arr[i] = yr_cur
  endfor

  tim_arr = buff[7,*]

  t_arr = yr_arr + '-' + string(fix(mon_arr),'$(i2.2)') + '-' + $
                         string(fix(day_arr),'$(i2.2)') + ' ' + $
                         tim_arr
  t_arr = anytim(t_arr, /yoh)

  table = user_arr + '   ' + mach_arr + '   ' + idl_serv_arr + '   ' + t_arr

  if keyword_set(do_sort) then begin
    uniq_user = all_vals(user_arr)
    n_uniq_user = n_elements(uniq_user)
    match_arr = intarr(n_uniq_user)
    for i=0,n_uniq_user-1 do begin
      ss_match = where(user_arr eq uniq_user[i], n_match)
      match_arr[i] = n_match
    endfor
    sort_match = sort(match_arr)
    sort_uniq_user = uniq_user[sort_match]
    for i=0,n_uniq_user-1 do begin
      ss_match = where(user_arr eq sort_uniq_user[i], n_match)
      if not exist(sort_table) then sort_table = table[ss_match] else $
        sort_table = [table[ss_match], sort_table]
    endfor

    table = sort_table
  endif

  total_lic0 = n_elements(table)
  if not exist(total_lic) then total_lic = total_lic0 else $
    total_lic = total_lic + total_lic0

if exist(user) then begin
  ss_match_user = where(strpos(table, user) ne -1, n_match_user)
  table = table[ss_match_user]
  if not exist(total_user) then total_user = n_elements(table) else $
    total_user = total_user + n_elements(table)
endif

  summary_text = strtrim((max_lic_arr[j] - total_lic0),2) + ' of ' + strtrim(max_lic_arr[j], 2) + ' licenses remaining for server ' + strtrim(server_arr[j],2) 
  if not exist(table_arr) then table_arr = ['', server_arr[j]+':', summary_text, '', table, ''] else $
    table_arr = [table_arr, server_arr[j]+':', summary_text, '', table, '']
  delvarx, sort_table

endfor

  if keyword_set(loud) then prstr, table_arr, /nomore
  summary = table_arr
; if keyword_set(get_summary) then out = summary

  if keyword_set(used) then begin
    if exist(user) then out = fix(total_user) else out = fix(total_lic)
  endif else begin
    out = fix(total(max_lic_arr) - total_lic)
  endelse

endif

return, out

end

