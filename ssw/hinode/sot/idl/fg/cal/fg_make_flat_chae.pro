
function fg_make_flat_chae, files, flat_file, outdir_flat=outdir_flat, bfi=bfi, $
  use_fg_prep=use_fg_prep, $
  wave=wave, bin_siz=bin_siz, xr=xr, yr=yr, shift_flag=shift_flag, $
  min_frac=min_frac, object=object, c=c, xoffset, yoffset, $
  maxiter=maxiter, niter, $
  max_error=max_error, last_error, error_arr=error_arr, $
  mask0=mask0, do_print=do_print, notvshow=notvshow, $
  do_make_top_html_files=do_make_top_html_files, $
  do_make_flat_dir_html_files=do_make_flat_dir_html_files, $
  do_make_only_current_flat_dir_html_file=do_make_only_current_flat_dir_html_file, $
  _extra=_extra

;+
; NAME: FG_MAKE_FLAT_CHAE
; PURPOSE: Produce the flat field images for arbitray  observations
; CALLING SEQUENCES:
;         flat = fg_make_flat_chae(files, flat_file, xoff, yoff)
;         flat = fg_make_flat_chae(files, outfile, xoff, yoff, /use_fg_prep, $
;           mask0=mask, maxiter=100, min_error=0.002, min_frac=0.4, object=object)
; INPUT:
;      files        an array of data filenames
;      flat_file   a flat image filename to be written
;
; OPTIONAL INPUT:
; INPUT KEYWORDS:
;   use_fg_prep     if set, read & dark-subtract with fg_prep, otherwise with readfits
;                   if not set, it assumes the files are already dark-subtracted
;   shift_flag       if set, the given shift is used.
;   min_frac     if set, the data is enforced to have values greater than or equal to
;                 min_frac*median(data)
;   xr, yr  ranges of x & y to use (2 element arrays, min & max)
;   mask0   single bytearray mask (1=good data, 0=bad) used on all images
;   See gain_calib for the other keywords
; OUTPUTS
;    flat field image, ie divide a raw image by this to correct it
;    xoffset, yoffset  arrays of x & y offsets of the input images
;    niter = # iterations used
;    last_error = final error achieved
;  HISTORY
;        2004 July, J. Chae. Added new keywords: setmask and minfrac
;  9-Dec-2009 TDT Hinode SOT version
;  9-Aug-2010 TDT 2nd Hinode SOT version
; 11-Sep-2012 TDT added niter & last_error
; 25-Sep-2012 TDT added notvshow keyword
;-

nf = n_elements(files)
if not keyword_set(min_frac) then min_frac = 0.001

if not exist(dir_top) then $
  dir_top = '/sanhome/slater/public_html/missions/hinode/sot/' + $
            (['nfi', 'bfi'])[keyword_set(bfi)] + '/flats'

t_begin_for = systime(/secon)
if keyword_set(do_print) then $
  print, 'FG_MAKE_FLAT_CHAE: Processing ' + string(nf, format='$(i3.3)') + ' files.'
read_sot, files, index_sot, data_sot, /silent

if strmid(outdir_flat,0,57) eq '/sanhome/slater/public_html/missions/hinode/sot/' + $
   (['nfi', 'bfi'])[keyword_set(bfi)] + '/flats' then begin
   print, 'Removing directory ' + outdir_flat + '.'
   spawn, 'rm -rf ' + outdir_flat
endif else begin
   stop, 'Stopping at deletion of existing OUTDIR_FLAT due to unrecognized parent directory.'
endelse

spawn, ['mkdir', outdir_flat], /noshell

; Write out fits file list for this set:
file_append, concat_dir(outdir_flat, 'files_fits.txt'), files, /new

; Write out file times list for this set:
times_fits = anytim(index_sot.date_obs, /ccsds)
file_append, concat_dir(outdir_flat, 'times_fits.txt'), times_fits, /new

if keyword_set(use_fg_prep) then begin
   fg_prep, index_sot, data_sot, indexp_sot, datap_sot, /no_flatfield, /quiet
endif else begin
   indexp_sot = index_sot
   datap_sot = data_sot
endelse

for k=0, nf-1 do begin
   ind = indexp_sot[k]
   tmp = datap_sot[*,*,k]

   if exist(mask0) then begin
      if n_elements(mask0) ne n_elements(tmp) then $
         stop, ' Image and mask are have different sizes.  Stopping.'
   endif

; Optionally, bin down images to speed flat fitting:
   if exist(bin_siz) then begin
      s_orig = size(tmp)
      tmp = rebin(tmp, s_orig[1]/float(bin_siz), s_orig[2]/float(bin_siz), /samp)
   endif

   s = size(tmp)

   if k eq 0 then begin
      if n_elements(xr) ne 2 then xr=[0, s(1)-1]
      if n_elements(yr) ne 2 then yr=[0, s(2)-1]
      nx=xr(1)-xr(0)+1
      ny=yr(1)-yr(0)+1
      logimages=fltarr(nx, ny, nf)
   endif
   m1=median(tmp(xr(0):xr(1), yr(0):yr(1)))*min_frac > 0.001
   logimages(*,*,k) = alog(tmp(xr(0):xr(1), yr(0):yr(1))>m1 )
endfor

t_finish_for = systime(/secon)
print, ' Time to read and process all FITS file = ' + $
   string(t_finish_for-t_begin_for, format='$(i3.3)') + ' seconds.'

if keyword_set(mask0) then begin
   mask0_binned = mask0
   if exist(bin_siz) then mask0_binned = $
      rebin(mask0_binned, s_orig[1]/float(bin_siz), s_orig[2]/float(bin_siz), /samp)
   mask = bytarr(nx, ny, nf)
   for k=0, nf-1 do mask[*,*,k] = mask0_binned
endif

t1 = systime(/second)

flat = gaincalib(logimages, xoffset, yoffset, object=object, mask=mask, $
   maxiter=maxiter, niter, $
   max_error=max_error, last_error, error_arr=error_arr, $
   c=c, shift_flag=shift_flag, silent=silent, /notvshow)

t2 = systime(/second)
print, t2-t1, ' seconds elapsed'

flat1=replicate(1., s(1), s(2))
flat1(xr(0):xr(1), yr(0):yr(1))=exp(flat)
object1=replicate(exp(median(object)), s(1), s(2))
object1(xr(0):xr(1), yr(0):yr(1))=exp(object)
object=object1
c=exp(c)
h = headfits(files(nf/2))
writefits, concat_dir(outdir_flat, flat_file), flat1, h
save, error_arr, file=concat_dir(outdir_flat, str_replace(flat_file, 'fits', 'sav'))

; ****************************
; Make the thumbnail and the reduced resolution gif images for the current flat set:
; ****************************

if exist(bin_siz) then $
   flat1_unbinned = $
      rebin(flat1, indexp_sot[0].naxis1, indexp_sot[0].naxis2, /samp) else $
   flat1_unbinned = flat1

; Replace NaN's:
ss_fin = where(finite(datap_sot) eq 1, n_fin, comp=ss_inf, ncomp=n_inf)
if n_inf gt 0 then datap_sot[ss_inf] = min(datap_sot[ss_fin])

stdev_arr = dblarr(indexp_sot[0].naxis1)
for i=0,indexp_sot[0].naxis1-1 do stdev_arr[i] = stddev(datap_sot[i,*,*])
stdev_median = median(stdev_arr)
ss_match_median = where(stdev_arr eq stdev_median, n_match_median)
mean_samp = mean(datap_sot[ss_match_median,*,*])
lo_lim = mean_samp - (7*stdev_median)
hi_lim = mean_samp + (7*stdev_median)
cube_full  = bytscl((datap_sot>lo_lim)<hi_lim)
cube_large = $
   rebin(cube_full, 1024, 1024*(double(indexp_sot[0].naxis2)/indexp_sot[0].naxis1), nf, /samp)
cube_small = $
   rebin(cube_full,  256,  256*(double(indexp_sot[0].naxis2)/indexp_sot[0].naxis1), nf, /samp)
cube_tiny = $
   rebin(cube_full,  128,  128*(double(indexp_sot[0].naxis2)/indexp_sot[0].naxis1), nf, /samp)

filnam_image = concat_dir(outdir_flat, 'data_' + time2file(indexp_sot.date_obs, /sec) + '.gif')
filnam_thumb = concat_dir(outdir_flat, 'data_' + time2file(indexp_sot.date_obs, /sec) + '_thumb.gif')

for i=0, nf-1 do begin
   write_gif, filnam_image[i], cube_large[*,*,i]
   write_gif, filnam_thumb[i], cube_tiny[*,*,i]
endfor

stdev_arr = dblarr(indexp_sot[0].naxis1)
for i=0,indexp_sot[0].naxis1-1 do stdev_arr[i] = stddev(flat1_unbinned[i,*,*])
stdev_median = median(stdev_arr)
ss_match_median = where(stdev_arr eq stdev_median, n_match_median)
mean_samp = mean(flat1_unbinned[ss_match_median,*,*])
lo_lim = mean_samp - (7*stdev_median)
hi_lim = mean_samp + (7*stdev_median)
flat_full  = bytscl((flat1_unbinned>lo_lim)<hi_lim)
flat_large = $
   rebin(flat_full, 1024, 1024*(double(indexp_sot[0].naxis2)/indexp_sot[0].naxis1), /samp)
flat_small = $
   rebin(flat_full,  256,  256*(double(indexp_sot[0].naxis2)/indexp_sot[0].naxis1), /samp)
flat_tiny  = $
   rebin(flat_full,  128,  128*(double(indexp_sot[0].naxis2)/indexp_sot[0].naxis1), /samp)

filnam_flat_image = concat_dir(outdir_flat, 'flat_' + time2file(indexp_sot[0].date_obs, /sec) + '.gif')
filnam_flat_thumb = concat_dir(outdir_flat, 'flat_' + time2file(indexp_sot[0].date_obs, /sec) + '_thumb.gif')

write_gif, filnam_flat_image, flat_large
write_gif, filnam_flat_thumb, flat_tiny

; ****************************
; Now make various html files:
; ****************************

if keyword_set(do_make_top_html_files) then begin

; Make the top level index.html file:
   html_arr = [ $
      "<html>", $
      "", $
      "<frameset cols='320,930,*' frameborder='0'>", $
      "<frame name='flat_dirs' src='flat_dir_links.html'>", $
      "<frame name='thumbnails' src='thumbnail_image_links.html'>", $
      "<frame name='images' src='images.html'>", $
      "</frameset>", $
      "", $
      "</html>", $
      "" ]
   file_append, concat_dir(dir_top, 'index.html'), html_arr, /new

; Make the 'thumbnail_image_links.html' file:
   html_arr = ["<html>", "<body>", "", "thumbnails", "", "</body>", "</html>"]
   file_append, concat_dir(dir_top, 'thumbnails.html'), html_arr, /new

; Make the 'images.html' file:
   html_arr = ["<html>", "<body>", "", "images", "", "</body>", "</html>"]
   file_append, concat_dir(dir_top, 'images.html'), html_arr, /new

; Make the flat directory link list file:
   flat_paths = $
      file_list(dir_top, (['sot_nfi_flat_', 'sot_bfi_flat_'])[keyword_set(bfi)] + $
                '????A_?_?x?_????????_????')
   n_flat_dirs = n_elements(flat_paths)
   break_file, flat_paths, flat_dir_logicals, flat_dir_parents, flat_dir_arr

   html_arr = ["<html>", "<body>", ""]
   file_append, concat_dir(dir_top, 'flat_dir_links.html'), html_arr, /new

   html_arr = "<a href='" + concat_dir(flat_dir_arr, 'index.html') + $
              "' target='thumbnails'>" + flat_dir_arr + "</a><br/>"
   file_append, concat_dir(dir_top, 'flat_dir_links.html'), html_arr

   html_arr = ["", "</body>", "</html>", ""]
   file_append, concat_dir(dir_top, 'flat_dir_links.html'), html_arr

endif ; end of make top level html files block

; Make the index.html files for all of the flat directories:

if keyword_set(do_make_flat_dir_html_files) then begin

   if keyword_set(do_make_only_current_flat_dir_html_file) then $
      flat_paths = outdir_flat else $
      flat_paths = $
         file_list(dir_top, (['sot_nfi_flat_', 'sot_bfi_flat_'])[keyword_set(bfi)] + $
                   '????A_?_?x?_????????_????')
   n_flat_dirs = n_elements(flat_paths)
   break_file, flat_paths, flat_dir_logicals, flat_dir_parents, flat_dir_arr

   for i=0, n_flat_dirs-1 do begin

      wave0 = strmid(flat_dir_arr[i],13,5)

      thumbnail_paths = file_list(flat_paths[i], 'data_20??????_??????_thumb.gif')
      break_file, thumbnail_paths, thumbnail_logicals, thumbnail_parents, $
                  thumbnail_file_arr
      thumbnail_file_arr = thumbnail_file_arr + '.gif'

      image_paths = file_list(flat_paths[i], 'data_20??????_??????.gif')
      break_file, image_paths, image_logicals, image_parents, image_file_arr
      image_file_arr = image_file_arr + '.gif'

      flat_thumbnail_paths = file_list(flat_paths[i], 'flat_20??????_??????_thumb.gif')
      break_file, flat_thumbnail_paths, flat_thumbnail_logicals, flat_thumbnail_parents, $
                  flat_thumbnail_file_arr
      flat_thumbnail_file_arr = flat_thumbnail_file_arr + '.gif'

      flat_image_paths = file_list(flat_paths[i], 'flat_20??????_??????.gif')
      break_file, flat_image_paths, flat_image_logicals, flat_image_parents, $
                  flat_image_file_arr
      flat_image_file_arr = flat_image_file_arr + '.gif'

      n_files = n_elements(image_file_arr)
;     times = anytim(indexp_sot.date_obs, /ccsds)
      times = rd_tfile(concat_dir(flat_paths[i], 'times_fits.txt'))

      html_arr = [ $
         "<html>", $
         "<body>", $
         "", $
         "<table border=2 cellpadding=2 cellspacing=3>" ]

      for j=0, n_files-1 do begin
         if j mod 5 eq 0 then html_arr = [html_arr, "<tr align=center>"]
         href = "<td><A HREF='" + image_file_arr[j] + "' target='images'><IMG SRC='" + $
                thumbnail_file_arr[j] + "'><br><em>" + wave0 + "<br>" + times[j] + $
                "</em></A></td>"
         if (j+1) mod 5 eq 0 then href = [href, "</tr>"]
         html_arr = [html_arr, href]
      endfor

      html_arr = [html_arr, ["</table>", "", "<table border=2 cellpadding=2 cellspacing=3>", $
                             "<tr align=center>"]]
      href = "<td><A HREF='" + flat_image_file_arr + "' target='images'><IMG SRC='" + $
             flat_thumbnail_file_arr + "'><br><em>" + wave0 + " Flat<br>" + times[0] + $
             "</em></A></td></tr></table>"  
      html_arr = [html_arr, [href, "", "</body>", "</html>", ""]]
      file_append, concat_dir(flat_paths[i], 'index.html'), html_arr, /new

   endfor

endif ; end of make flat directories html files block

return, flat1
end
