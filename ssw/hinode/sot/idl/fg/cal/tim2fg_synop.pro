
pro tim2fg_synop, t0, delta_hr=delta_hr, wave_arr=wave_arr, owave_arr=owave_arr, $
  read_files=read_files, files=files, cat=cat, index=index, data=data, $
  lildata=lildata, lilfac=lilfac, quiet=quiet

; Set default section:
; ----------------------
if not exist(level) then level = 0
if level eq 0 then begin
  set_logenv,'SOT_DATA','/net/kokuten/archive/hinode/sot/level0',quiet=quiet
  sirius=1
endif else sirius = 0

if not exist(delta_hr) then delta_hr =  6		; For now, 12 hour search window
if not exist(toff_max) then toff_max = 41		; Default max time difference
							;   between first and last images
if not exist(lilfac) then lilfac = 8

if not exist(infil_template) then infil_template = $
  '/net/hikari/home/slater/soft/idl/fpp/throughput/index_fg_template_new.sav'

; Set defaults for tag search:
if not exist(pnt_limit) then pnt_limit = 200
if not exist(naxis1) then naxis1 = 4096
if not exist(naxis2) then naxis2 = 2048
if not exist(xsiz_box) then xsiz_box = 256
if not exist(ysiz_box) then ysiz_box = xsiz_box
; ----------------------

wave_mnem_arr = ['gband', 'blue', 'red', 'green', 'ca', 'cn']
wave_name_arr = ['G*band*4305', 'blue*cont*4504', 'red*cont*6684', $
		 'green*cont*5550', 'Ca*II*H*line', 'CN*bandhead*3883']
wave_uint_flag_arr = [0,0,0,0,0,0]

nwave = n_elements(wave_arr)
t_start = anytim(addtime(anytim(t0,/yoh),delta_min=-delta_hr*60),/yoh)
t_end   = anytim(addtime(anytim(t0,/yoh),delta_min= delta_hr*60),/yoh)

wave0 = wave_arr(0)
ss_match = where(wave_mnem_arr eq wave0, nmatch)
if nmatch eq 0 then stop,' Specified wavelength not recognized.  Stopping'
search_array = ['wave=' + wave_name_arr(ss_match), 'obs_type=FG*(simple)', $
		'naxis1='+ strtrim(naxis1,2), 'naxis2=' + strtrim(naxis2,2), $
		'xcen=-' + strtrim(pnt_limit,2) + '~' + strtrim(pnt_limit,2), $
		'ycen=-' + strtrim(pnt_limit,2) + '~' + strtrim(pnt_limit,2)]

sot_cat, t_start, t_end, cat0, files0, $
  sirius=sirius, search_array=search_array, count=count0, quiet=quiet

if count0 gt 0 then begin
  ss_close = tim2dset(anytim(cat0.date_obs,/int), anytim(t0,/int), delta_sec=toff)
  cat = cat0(ss_close)		; Begin the cat vector with this first file.
  files = files0(ss_close)	; Begin the file vector with this first file.
  owave_arr = wave0
endif else begin
  if not keyword_set(quiet) then $
    stop,' No images within delta_hr of input time.  Stopping.'
endelse
 
if nwave gt 1 then begin
  t_ref = cat.date_obs
  t_start = anytim(addtime(anytim(t_ref,/yoh),delta_min=-delta_hr*60),/yoh)
  t_end   = anytim(addtime(anytim(t_ref,/yoh),delta_min= delta_hr*60),/yoh)
  for i=1,nwave-1 do begin
    wave0 = wave_arr(i)
    ss_match = where(wave_mnem_arr eq wave0, nmatch)
    if nmatch eq 0 then stop,' Specified wavelength not recognized.  Stopping'
    search_array = ['wave=' + wave_name_arr(ss_match), 'obs_type=FG*(simple)', $
		    'naxis1='+ strtrim(naxis1,2), 'naxis2=' + strtrim(naxis2,2), $
		    'xcen=-' + strtrim(pnt_limit,2) + '~' + strtrim(pnt_limit,2), $
		    'ycen=-' + strtrim(pnt_limit,2) + '~' + strtrim(pnt_limit,2)]

    sot_cat, t_start, t_end, cat0, files0, $
      sirius=sirius, search_array=search_array, count=count0, quiet=quiet

    if count0 gt 0 then begin
      ss_close = tim2dset(anytim(cat0.date_obs,/int), anytim(t_ref,/int), delta_sec=toff)
      if toff le toff_max then begin
	cat = [cat, cat0(ss_close)]
	files = [files, files0(ss_close)]
	owave_arr = [owave_arr, wave0]
      endif else begin
	stop, 'No files for wave ' + strtrim(wave0,2) + ' found within offset limit.  Stopping.'
      endelse
    endif else begin
      if not keyword_set(quiet) then $
	stop,' No images within delta_hr of first image time.  Stopping.'
    endelse
  endfor
endif

if keyword_set(read_files) then begin
  restore, infil_template
  read_sot, files, index, data, strtemplate=index_fg_template
  sizdata = size(data)
  lildata = rebin(data, sizdata(1)/lilfac, sizdata(2)/lilfac, sizdata(3), /sample)
endif

end
