;--------------------------------------------------------------------
FUNCTION fg_image_type, index, array_pos, $
                        QUIET=quiet, VERSION=progver, NAME=prognam

;+
; NAME:
;	FG_IMAGE_TYPE
;
; PURPOSE:
;	Return a string code indicating what type of image is
;	contained in a data array whose associated header index is
;	passed in INDEX. 
;
; CATEGORY:
;	FITS processing
;
; SAMPLE CALLING SEQUENCE:
;       img_type=FG_IMAGE_TYPE(index, i)
;
; INPUTS:
;       INDEX: an SSW header index structure.
;       ARRAY_POS: scalar variable indicating position in associated
;       data array. ARRAY_POS is ge 0 and le 3.
; 
; OUTPUTS:
;       IMAGE_TYPE: two character string constant from the
;       following list:
;       'FG': simple filtergram
;       'SI': Stokes I image
;       'SQ': Stokes Q image
;       'SU': Stokes U image
;       'SV': Stokes V image
;
; OUTPUTS (Optional):
;      None.
;       
; OPTIONAL INPUT KEYWORD PARAMETERS:
;       None.
;
; OPTIONAL OUTPUT KEYWORD PARAMETERS:
;       VERSION: the version number of this routine.
;       NAME: the name of this routine.
;
; RESTRICTIONS: INDEX must be an SSW image index structure with appropriate 
;        keywords for treating the image.
;    
;
; PROCEDURE:
;
;
; MODIFICATION HISTORY:
;v1.0  Created 22-Oct-2007, T. Berger LMSAL
;v1.1  Added gen_id 7, MG2 I & V but really I & Dopplergram, TDT 3-Jan-2008
;v1.2  Mods for gen_ids 4,6,8,9--11. TEB 18-Apr-2008.
;v1.3  added gen_id 18.  TEB 5-Apr-2009.
;v1.4  2011-12-16 - Greg Slater, LMSAL. Added gen_id 12 for focus scans.
;-
;-----------------------------------------------------------------------------
;
;Name, version, and timing information
progver = 'V1.4'
prognam = 'FG_IMAGE_TYPE.PRO'

genid = gt_tagval(index,/gen_id)
i = array_pos
iv = ['SI','SV']
iq = ['SI','SQ']
iu = ['SI','SU']
iuv =['SI','SU','SV']
iquv = ['SI','SQ','SU','SV']
doppler = ['DN','DD','DG']  ;Numerator, Denominator, Velocity image
ivdg = ['SI','DG','SV']
switch genid of

   1: RETURN,'FG'
   2: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iv[i]
   end
   3: begin
      if (i lt 0) or (i gt 3) then begin
         MESSAGE,'Invalid array index.' 
         RETURN,-1
      end
      RETURN,iquv[i]
   end
   4: RETURN,'VI'  ;Stokes V over I image.
   5:
   6: RETURN,'VI'
   7: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iv[i]
   end
   8: RETURN,'VI'
   9: RETURN,doppler[2]
   10: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,doppler[i]
   end
   11: RETURN,doppler[2]
   12: RETURN,'FG'
   13:
   14:
   15: begin
      MESSAGE,'Gen_id not yet implemented.'
      RETURN,-1
   end
   17: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iv[i]
   end
   18: begin
      if (i lt 0) or (i gt 2) then begin
         MESSAGE,'Invalid array index.'
         RETURN, -1
      end
      RETURN,ivdg[i]
   end
   32: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iv[i]
   end
   33: begin
      if (i lt 0) or (i gt 3) then begin
         MESSAGE,'Invalid array index.' 
         RETURN,-1
      end
      RETURN,iquv[i]
   end
   34: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iq[i]
   end
   35: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iu[i]
   end
   36: begin
      if (i lt 0) or (i gt 2) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iuv[i]
   end
   38:
   39: begin
      if (i lt 0) or (i gt 1) then begin
         MESSAGE,'Invalid array index.'
         RETURN,-1
      end
      RETURN,iv[i]
   end

endswitch

END












