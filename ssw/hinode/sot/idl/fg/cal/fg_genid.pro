FUNCTION fg_genid, gen_id

;Returns the OBS_TYPE string associated with the GEN_ID keyword. There should
; be a one-to-one correspondence between these variables. Note that there is NOT
; a one-to-one correspondence between OBS_TYPE and OBS_ID.

;Last modification: 9-Mar-2009 TDT at LMSAL.

case gen_id of

  1: RETURN,'FG (simple)'
  2: RETURN,'FG Shuttered I and V'
  3: RETURN,'FG Shuttered IQUV'
  4: RETURN,'FG MG4 V/I'
  5: RETURN,'FG MG4 V and I'
  6: RETURN,'FG MG2 V/I'
  7: RETURN,'FG MG2 V and I'
  8: RETURN,'FG MG1 V/I'
  9: RETURN,'FG DG4 single V'
  10: RETURN,'FG DG4 two V'
  11: RETURN,'FG DG2'
  12: RETURN,'FG focus scan'
  13: RETURN,'FG Iscan'
  14: RETURN,'FG Laser tune'
  15: RETURN,'FG EMI test'
  17: RETURN,'FG IV (0.1s)'
  18: RETURN,'FG IV and DG'
  32: RETURN,'FG Shutterless I and V'
  33: RETURN,'FG Shutterless IQUV'
  34: RETURN,'FG Shutterless I and Q'
  35: RETURN,'FG Shutterless I and U'
  36: RETURN,'FG Shutterless IUV'
  38: RETURN,'FG Shutterless IV 0.2sec'
  39: RETURN,'FG Shutterless IV 0.1sec'

  else: begin
     MESSAGE,'Undefined GEN_ID code. Returning UNDEFINED',/INFORM
     RETURN,'UNDEFINED'
  end

end

END
