
pro go_batch_sdo_corr, start_time, end_time

do_write=1

;search_array = ['naxis1=1024','wave=Ca*II*H*line || wave=G*band*4305']
 search_array = ['naxis1>511 && naxis2>511','wave=Ca*II*H*line || wave=G*band*4305']

dir_top = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_aia_align_ver1
if not file_exist(dir_top) then begin
  print, 'Making directory ' + strtrim(dir_top,2)
  mk_dir, dir_top
endif

sttim_run = anytim(!stime, /ccsds)
go_sdo_corr, start_time, end_time, /level0, search_array=search_array, $
  dir_top=dir_top, do_write=do_write
entim_run = anytim(!stime, /ccsds)

run_time_hours = (anytim(entim_run) - anytim(sttim_run))/3600d
days_processed = (anytim(end_time) - anytim(start_time))/86400d0

hours_per_day = days_processed/double(run_time_hours)

text = strmid(anytim(entim_run, /yoh),0,15) + ': PROCESSED ' + strmid(anytim(start_time, /yoh),0,15) + $
       ' TO ' + strmid(anytim(end_time, /yoh),0,15) + $
       ' AT ' + string(hours_per_day, '$(f4.2)') + ' HOURS PER DAY.'

file_append, '/sanhome/slater/public_html/logs/sot_aia_align/days_done.log', text

end
