pro fg_get_dark, index, dark_index, dark_image                   $
                 , darkdir =darkdir,  deltaday=deltaday          $
                 , darkfile=darkfile                             $
                 , verbose=verbose,           loud=loud          $
                 , version=progver,        name=prognam          $
                 , retrn=retrn
;+
; NAME:
;	FG_GET_DARK
;
; PURPOSE:
;       Return an FG dark image appropriate for the index structure. 
;       This routine is called from FG_DARK_SUB by default when a 
;       user-provided dark image is not available.
;       
; CATEGORY:
;	FITS processing
;
; SAMPLE CALLING SEQUENCE:
;	fg_get_dark, index, dark_index, dark_image   $
;                [, darkdir=darkdir, deltaday=deltaday, darkfile=darkfile $
;                 , /verbose, /loud, version=version, name=name ]
;
; INPUTS:
;	INDEX	   - the SSW index structure
;
; OUTPUTS:
;       DARK_INDEX - the SSW header index structure for the dark image
;       DARK_IMAGE - 2D full FOV dark mage without ROI extraction
;	
; OPTIONAL INPUT KEYWORD PARAMETERS: 
;	DARKDIR    - Alternate directory for processed dark current 
;                    fits images,
;		     default =  concat_dir('SOT_SSWDB_FG_CAL'','darks')
;	VERBOSE    - Set for a few more messages; overrides /loud
;       LOUD       - Not set to minimize the messages
;
; OUTPUT KEYWORD PARAMETERS: 
;       DELTADAY   - Time difference (day) between the image and the dark
;       DARKFILE   - Filename of the dark FITS file
;       VERSION    - The current version number of the program
;       NAME       - The name of this routine
;	RETRN      - Flag for branching on unusual return
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;V1.0  Created from fg_get_flat.YK 21-Nov-2007.
;
;-
;-----------------------------------------------------------------------------
;
prognam = 'FG_GET_DARK.PRO'
progver = 'V1.0'

retrn   = 0
loud    = KEYWORD_SET(loud)
verbose = KEYWORD_SET(verbose)
if verbose then loud = 1b

; default directory
if N_ELEMENTS(darkdir) eq 0 then darkdir = concat_dir(getenv('SOT_SSWDB_FG_CAL'),'darks')

; get size, wavelength & summing
img_nx0 = gt_tagval(index,/naxis1)
img_ny0 = gt_tagval(index,/naxis2)
img_waveid = gt_tagval(index, /waveid)
img_exptime= gt_tagval(index,/exptime)
img_psum   = gt_tagval(index, /campsum)
img_ssum   = gt_tagval(index, /camssum)
img_binx   = gt_tagval(index, /fgbinx)
img_biny   = gt_tagval(index, /fgbiny)
img_tccd  = gt_tagval(index, /t_fgccd)
img_tceb  = gt_tagval(index, /t_fgceb)

; summing 
sum = '1x1'
if (img_ssum gt 1) or (img_psum gt 1) then $
  sum = STRING(img_psum,img_ssum, form='(I1,"x",I1)')

; return file list
file = 'dark_fg_'+sum+'_'+'*.fits'
filelist = FILE_SEARCH(darkdir, file, count=nfile)

if nfile le 0 then begin 
    ; If not found, return array of 1s.
    MESSAGE, /CONT, 'No dark found. Returning uniform bias...'
    dark_index = index
    dark_image = REPLICATE(190,img_nx0, img_ny0)
    darkfile = 'No dark found in database. Using uniform bias.'
    deltaday=-1
    retrn = 1
    RETURN
endif

; Check time difference
; read header to get DATE_OBS
mreadfits, filelist, dark_index, /nodata
utc_obs  = str2utc(index.date_obs)
utc_dark = str2utc(dark_index.date_obs)
deltaday = (utc_dark.mjd-utc_obs.mjd) + (utc_dark.time - utc_obs.time)/8.64e7

; use minimum DT
deltaday = MIN(ABS(deltaday),imin)
darkfile = filelist[imin]

if (verbose) then begin
    MESSAGE,/info,'Model dark data information.'
    MESSAGE,/info,'      File name: '+file
endif

; read header again
mreadfits, darkfile, dark_index, /nodata

; CCD and CEB temperatures
t_ceb0 = gt_tagval(dark_index, 'T_FGCEB')
t_ccd0 = gt_tagval(dark_index, 'T_FGCCD')

; read parameters from image extension
a0 = mrdfits(darkfile, 1, /fscale, /silent)
b0 = mrdfits(darkfile, 2, /fscale, /silent)
b1 = mrdfits(darkfile, 3, /fscale, /silent)
c0 = mrdfits(darkfile, 4, /fscale, /silent)
c1 = mrdfits(darkfile, 5, /fscale, /silent)

; bias correction
t0 = FLOAT(img_tceb-t_ceb0)
t1 = FLOAT(img_tccd-t_ccd0)
dark_image = a0 + b0*t0 + b1*t1

; dark current correction
dark_image += img_exptime*EXP(c0 + c1*t1)
dark_image = FLOAT(dark_image)
   
; update dark_index
sz = SIZE(dark_image,/dim)
dark_index.naxis = 2
dark_index = add_tag(dark_index,sz[0],'NAXIS1',index='NAXIS')
dark_index = add_tag(dark_index,sz[1],'NAXIS2',index='NAXIS1')

RETURN
END
