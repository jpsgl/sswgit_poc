
function mk_sot_corr_cat, t0, t1, dir_top=dir_top, gap_cat=gap_cat, $
         sttim_gap=sttim_gap, entim_cont=entim_gap, dur_gap_sec=dur_gap_sec, $
         n_gap=n_gap, $
         t_diff_max=t_diff_max, peak_str_min=peak_str_min, $
         do_plot=do_plot, _extra=_extra

if not exist(dir_top) then begin
  if not exist(filnam_db) then filnam_db = 'hinode_sdo_alignment' ; _20160411'
  if get_logenv('SOT_CORR_DB') eq '' then $
     set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                           filnam_db)
  dir_top = get_logenv('SOT_CORR_DB')
endif

if not exist(t_diff_max) then t_diff_max = 1800 ; seconds
if not exist(peak_str_min) then peak_str_min = 8
if not keyword_set(do_plot) then do_plot = 1

if not exist(t0) or not exist(t1) then begin
   if not exist(files_genx) then begin
      print, ' Must pass either t0 and t1 OR files_genx.  Returning.'
      return, -1
   endif
endif else begin
   read_genxcat, t0, t1, topdir=dir_top, index_sot_corr, catfiles=files_corr_cat, $
      error=err_genx, status=status_genx
   if err_genx ne 0 then STOP, 'Error in read_genxcat.  Stopping.'
endelse

t_corr = anytim(index_sot_corr.date_obs, /ccsds)
t_corr_sec = anytim(t_corr)
t0_sec = anytim(t0)
t1_sec = anytim(t1)

;ss_good = where( (t_corr_sec ge t0_sec) and $
;                 (t_corr_sec le t1_sec) and $
;                 (index_sot_corr.peak_str ge peak_str_min), n_good)
ss_good = where( ( (t_corr_sec ge t0_sec) and $
                   (t_corr_sec le t1_sec) ), n_good)

if n_good gt 0 then begin

   t_corr = t_corr[ss_good]
   t_corr = [anytim(t0, /ccsds), t_corr, anytim(t1, /ccsds)]
   ss_uniq = uniq(t_corr)
   t_corr = t_corr[ss_uniq]
   t_corr_sec = anytim(t_corr)
   
   t_diff_sec = t_corr_sec[1:*] - t_corr_sec

   ss_gap = where(t_diff_sec gt t_diff_max, n_gap)

   if n_gap gt 0 then begin
      sttim_gap = t_corr[ss_gap]
      entim_gap = t_corr[ss_gap+1]
      dur_gap_sec = t_diff_sec[ss_gap]
      gap_cat = {sttim_cont:sttim_gap, entim_cont:entim_gap, dur_gap_sec:dur_gap_sec}
   endif else begin
      gap_cat = -1
   endelse

endif else begin
   sttim_gap = anytim(t0, /ccsds)
   entim_gap = anytim(t1, /ccsds)
   dur_gap_sec = anytim(entim_gap) - anytim(sttim_gap)
   gap_cat = {sttim_cont:sttim_gap, entim_cont:entim_gap, dur_gap_sec:dur_gap_sec}
endelse

if keyword_set(do_plot) then begin
   wdef,0,1024,512
   utplot, t_corr, lindgen(n_elements(t_corr)), psym=3
endif
;help, n_gap

return, gap_cat

end
