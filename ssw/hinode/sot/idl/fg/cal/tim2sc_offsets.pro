
function tim2sc_offsets, item, pnt_struct=pnt_struct, $
  sc2sot_ew_ns=sc2sot_ew_ns, sc2xrt_ew_ns=sc2xrt_ew_ns, sot2xrt_ew_ns, $
  sc2sot_ew_off=sc2sot_ew_off, t_ew_sot=t_ew_sot, $
  sc2sot_ns_off=sc2sot_ns_off, t_ns_sot=t_ns_sot, $
  sc2xrt_ew_off=sc2xrt_ew_off, t_ew_xrt=t_ew_xrt, $
  sc2xrt_ns_off=sc2xrt_ns_off, t_ns_xrt=t_ns_xrt, $
  qstop=qstop

if not exist(filnam_sc_xrt_off) then $
  filnam_sc_sot_off = '$HOME/soft/idl/idl_startup/align_sot.genx
if not exist(filnam_sc_xrt_off) then $
  filnam_sc_xrt_off = '$HOME/soft/idl/idl_startup/align_xrt.genx

; Calculate SOT offset tables

restgenx,file=filnam_sc_sot_off, sc_sot_struct

ss_ew_sot = where(abs(sc_sot_struct.xcen) gt 50, n_ew)                                 
ss_ns_sot = where(abs(sc_sot_struct.ycen) gt 50, n_ew)

t_ew_sot = sc_sot_struct[ss_ew_sot].date_obs
t_sec_ew_sot = anytim(t_ew_sot)

t_ns_sot = sc_sot_struct[ss_ns_sot].date_obs
t_sec_ns_sot = anytim(t_ns_sot)

xcen_sot_orig = sc_sot_struct[ss_ew_sot].xcen
ycen_sot_orig = sc_sot_struct[ss_ns_sot].ycen

xcen_sot_fit = sc_sot_struct[ss_ew_sot].xcen_fit
ycen_sot_fit = sc_sot_struct[ss_ns_sot].ycen_fit

sc2sot_ew_off = xcen_sot_fit - xcen_sot_orig
sc2sot_ns_off = ycen_sot_fit - ycen_sot_orig

ss_good = where(sc2sot_ns_off le 100, n_good)
if n_good gt 0 then begin
  sc2sot_ns_off = sc2sot_ns_off[ss_good]
  t_ns_sot = t_ns_sot[ss_good]
  t_sec_ns_sot = t_sec_ns_sot[ss_good]
endif

; Calculate XRT offset tables

restgenx,file=filnam_sc_xrt_off, sc_xrt_struct

ss_ew_xrt = where(abs(sc_xrt_struct.xcen) gt 50, n_ew)                                 
ss_ns_xrt = where(abs(sc_xrt_struct.ycen) gt 50, n_ew)

t_ew_xrt = sc_xrt_struct[ss_ew_xrt].date_obs
t_sec_ew_xrt = anytim(t_ew_xrt)

t_ns_xrt = sc_xrt_struct[ss_ns_xrt].date_obs
t_sec_ns_xrt = anytim(t_ns_xrt)

xcen_xrt_orig = sc_xrt_struct[ss_ew_xrt].xcen
ycen_xrt_orig = sc_xrt_struct[ss_ns_xrt].ycen

xcen_xrt_fit = sc_xrt_struct[ss_ew_xrt].xcen_fit
ycen_xrt_fit = sc_xrt_struct[ss_ns_xrt].ycen_fit

sc2xrt_ew_off = xcen_xrt_fit - xcen_xrt_orig
sc2xrt_ns_off = ycen_xrt_fit - ycen_xrt_orig

ss_good = where(sc2xrt_ns_off le 100, n_good)
if n_good gt 0 then begin
  sc2xrt_ns_off = sc2xrt_ns_off[ss_good]
  t_ns_xrt = t_ns_xrt[ss_good]
  t_sec_ns_xrt = t_sec_ns_xrt[ss_good]
endif

; if time array is passed, then spline interpolate to find
; EW and NS offsets from SC to SOT, SC to XRT, and SOT to XRT

if exist(item) then begin

  if size(item, /type) eq 8 then begin
    times = anytim(item.date_obs)
  endif else begin
    times = anytim(item, /yoh)
  endelse
  times_sec = anytim(times)
  n_tim = n_elements(times)

;  sc2sot_ew_spline = spline(t_sec_ew_sot, sc2sot_ew_off, times_sec)
;  sc2sot_ns_spline = spline(t_sec_ns_sot, sc2sot_ns_off, times_sec)

;  sc2xrt_ew_spline = spline(t_sec_ew_xrt, sc2xrt_ew_off, times_sec)
;  sc2xrt_ns_spline = spline(t_sec_ns_xrt, sc2xrt_ns_off, times_sec)

  sc2sot_ew_spline = interpol(sc2sot_ew_off, t_sec_ew_sot, times_sec)
  sc2sot_ns_spline = interpol(sc2sot_ns_off, t_sec_ns_sot, times_sec)

  sc2xrt_ew_spline = interpol(sc2xrt_ew_off, t_sec_ew_xrt, times_sec)
  sc2xrt_ns_spline = interpol(sc2xrt_ns_off, t_sec_ns_xrt, times_sec)

  sot2xrt_ew_spline = sc2xrt_ew_spline - sc2sot_ew_spline
  sot2xrt_ns_spline = sc2xrt_ns_spline - sc2sot_ns_spline

  sc2sot_ew_ns = [[sc2sot_ew_spline], [sc2sot_ns_spline]]
  sc2xrt_ew_ns = [[sc2xrt_ew_spline], [sc2xrt_ns_spline]]
  sot2xrt_ew_ns = [[sot2xrt_ew_spline], [sot2xrt_ns_spline]]

; Rename offset array to preserve naming convention from tim2xrt_sot_offsets.pro
  offsets_sot_xrt = sot2xrt_ew_ns

endif else begin
  offsets_sot_xrt = -1
endelse

pnt_struct = {sc2sot_ew_off:sc2sot_ew_off, t_ew_sot:t_ew_sot, $
              sc2sot_ns_off:sc2sot_ns_off, t_ns_sot:t_ns_sot, $
              sc2xrt_ew_off:sc2xrt_ew_off, t_ew_xrt:t_ew_xrt, $
              sc2xrt_ns_off:sc2xrt_ns_off, t_ns_xrt:t_ns_xrt}

return, offsets_sot_xrt

end

