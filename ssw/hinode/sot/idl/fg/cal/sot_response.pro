
pro sot_response, t, response, resp_struct=resp_struct, $
  wave=wave, left=left, right=right, all=all, $
  do_plot=do_plot, quiet=quiet, _extra=_extra

;+
; NAME:
; PURPOSE:
; CALLING SEQUENCE:
; EXAMPLE CALLS:
;   IDL> 
; REQUIRED INPUTS:
; OPTIONAL INPUTS:
; KEYWORD INPUTS (for call to HINODE_POINTING_SCAN):
; OUTPUTS:
; USAGE:
; REVISION HISTORY:
;   v 1.0    2014-03-20 - GLS LMSAL
; TODO:
;   Shorter name
;-

restore, '$HOME/soft/idl/idl_startup/sot/bfi_response_mission.sav'

if not exist(wave) then wave = 'gband'
wave_arr = ['gband','blue','red','green','ca','cn']
ss_match_wave = (where(wave_arr eq strlowcase(strtrim(wave,2)), count_match))[0]

if not exist(wave_col) then wave_col = [1,10,2,8,12,11]

if not exist(t) then begin
;  prstr, [ $
;          ';+', $
;          '; NAME:', $
;          '; PURPOSE:', $
;          '; CALLING SEQUENCE:', $
;          '; EXAMPLE CALLS:', $
;          ';   IDL> ', $
;          '; REQUIRED INPUTS:', $
;          '; OPTIONAL INPUTS:', $
;          '; KEYWORD INPUTS (for call to HINODE_POINTING_SCAN):', $
;          '; OUTPUTS:', $
;          '; USAGE:', $
;          '; REVISION HISTORY:', $
;          ';   v 1.0    2014-03-20 - GLS LMSAL', $
;          '; TODO:', $
;          ';   Shorter name', $
;          ';-' $
;         ]

  prstr, [ $
          '; CALL:', $
          ';   IDL> sot_response, t, response, resp_struct=resp_struct, $', $
	  ';        wave=wave, left=left, right=right, all=all, $', $
          ';        do_plot=do_plot)', $
          ';     where:', $
          ';       - wave is one of ["gband","blue","red","green","ca","cn"] (default is "gband")', $
          ';       - /do_plot to plot the mission response for selected wave' $
	  ]
;          ';', $
;          '; EXAMPLE CALLS:', $
;          ';   Basic call returns BFI mean full frame response interpolated at passed T_SAMP values:', $
;          ';     IDL> resp = sot_response(t_samp)', $
;          ';   BFI mean left CCD response interpolated at passed T_SAMP values:', $
;          ';     IDL> resp = sot_response(t_samp, /left)', $
;          ';   BFI mean right CCD response interpolated at passed T_SAMP values:', $
;          ';     IDL> resp = sot_response(t_samp, /right)', $
;          ';   BFI mean full, left, and right CCD responses interpolated at passed T_SAMP values', $
;          ';   (Output is 3 x N, where is n_elements(t_samp) ):', $
;          ';     IDL> resp = sot_response(t_samp, /all)', $
;          ';   Basic call returns BFI mean full frame response interpolated at passed T_SAMP values:', $
;          ';     IDL> resp = sot_response(t_samp)' $
;         ]

  wdef,0,1024,1280
  device, decomp=0
  linecolors
;  !p.multi = [0,1,6]
  for i=0,5 do begin
    tag_ids = indgen(4) + 4*i
    t_arr = bfi_response_struct.(tag_ids[0])
    bfi_full_ccd_mean = bfi_response_struct.(tag_ids[1])
    bfi_left_half_ccd_mean = bfi_response_struct.(tag_ids[2])
    bfi_right_half_ccd_mean = bfi_response_struct.(tag_ids[3])

    norm = max(bfi_full_ccd_mean)
;    tit = 'BFI ' + wave + ' normalized response (smoothed)'
    tit = 'BFI normalized response (smoothed)'
    ytit = 'Normalized Response'

    if i eq 0 then begin
      utplot,  t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN/norm,7), /ystyle, charsize=2, psym=3, $
        yrange=[0.30, 1.05], tit=tit, ytit=ytit, thick=3, charthick=2, $
        color=0, background=255, /nodata
;      yrange=[6e3,1.2e4], tit=tit
    endif

    outplot, t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN/norm,7), psym=-3, color=wave_col[i], linestyle=0, thick=3
    outplot, t_arr, smooth(BFI_RIGHT_HALF_CCD_MEAN/norm,7), psym=-3, color=wave_col[i], linestyle=1, thick=3
leg_arr = ['gband _____','blue  _____','red   _____','green _____','ca    _____','cn    _____']
    ssw_legend, leg_arr, $
                textcolors=wave_col, charsize=2, charthick=2, /top, /right
    ssw_legend, leg_arr, $
                textcolors=wave_col, charsize=2, charthick=2, /bot, /right
;   ssw_legend, ['left CCD ...............', 'right CCD -----'], charsize=1.5, /top, /right
  endfor
  !p.multi = 0

  resp_struct = bfi_response_struct
  return
endif

;t_names = tag_names(bfi_respnse_struct)
tag_ids = indgen(4) + 4*ss_match_wave
t_arr = bfi_response_struct.(tag_ids[0])
bfi_full_ccd_mean = bfi_response_struct.(tag_ids[1])
bfi_left_half_ccd_mean = bfi_response_struct.(tag_ids[2])
bfi_right_half_ccd_mean = bfi_response_struct.(tag_ids[3])

tsec = anytim(t)
t_arr_sec = anytim(t_arr)

resp_full = interpol(bfi_full_ccd_mean, t_arr_sec, tsec)
resp_left = interpol(bfi_left_half_ccd_mean, t_arr_sec, tsec)
resp_right = interpol(bfi_right_half_ccd_mean, t_arr_sec, tsec)

if keyword_set(do_plot) then begin
  wdef,0,1024,512
  norm = max(bfi_full_ccd_mean)
  tit='BFI ' + strupcase(wave) + ' normalized response (smoothed)'
  ytit = 'Normalized Response'
  utplot,  t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN/norm,7), /ynoz, charsize=1.5, linestyle=1, $
    tit=tit, ytit=ytit, /year
;  utplot,  t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN,7), /ynoz, charsize=1.5, linestyle=1, $
;    tit='bfi response (smoothed)'
;    yrange=[6e3,1.2e4], tit='bfi response (smoothed)'
;  outplot, t_arr, smooth(BFI_RIGHT_HALF_CCD_MEAN,7), linestyle=2
  outplot, t_arr, smooth(BFI_RIGHT_HALF_CCD_MEAN/norm,7), linestyle=2
  ssw_legend, ['left CCD ...............', 'right CCD -----'], charsize=2, /top, /right
  if n_elements(t) eq 1 then begin
    t_plot = [t,t]
    resp_left_plot = [resp_left, resp_left]
    resp_right_plot = [resp_right, resp_right]
  endif else begin
    t_plot = [t,t]
    resp_left_plot = resp_left
    resp_right_plot = resp_right
  endelse
  outplot, t_plot, resp_left_plot/norm,  psym=2
  outplot, t_plot, resp_right_plot/norm, psym=4
endif

case 1 of
  keyword_set(left):  out = resp_left
  keyword_set(right): out = resp_right
  keyword_set(all):   out = transpose([[resp_full], [resp_left], [resp_right]])
  else: out = resp_full
endcase

response = out
resp_struct = bfi_response_struct

end

