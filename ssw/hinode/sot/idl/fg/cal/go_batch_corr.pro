
pro go_batch_corr, t0, t1, hours_grid=hours_grid, xrt=xrt, level0=level0, $
  dir_top=dir_top, search_array=search_array, percentd_min=percentd_min, $
  cat_sot=cat_sot, files_sot=files_sot, refresh_cat=refresh_cat, $
  every_nth=every_nth, n_max_files=n_max_files, $
  shimizu_init=shimizu_init, $
  force_invert=force_invert, nsig_in=nsig_in, interactive=interactive, $
  do_raw_cube=do_raw_cube, do_cubes=do_cubes, do_reg=do_reg, wcs_ref=wcs_ref, $
  do_xstep=do_xstep, do_movie_map=do_movie_map, $
  do_write=do_write, do_test=do_test, $
  verbose=verbose, no_log_time=no_log_time, $
  xdisp_ref=xdisp_ref, ydisp_ref=ydisp_ref, $
  lil_reg_cube=lil_reg_cube, do_debug=do_debug, do_composite=do_composite, $
  add_waves=add_waves, add_map_waves=add_map_waves, case_num=case_num, $
  _extra=_extra

;+
; name: go_batch_corr
; call: go_batch_corr, t0, t1
; -

if get_logenv('SOT_CORR_DB') eq '' then $
   set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                         'hinode_sdo_alignment')
print, get_logenv('SOT_CORR_DB')
dir_top = get_logenv('SOT_CORR_DB')
mk_dir, dir_top
mk_dir, concat_dir(get_logenv('SOT_CORR_DB'), 'database')

if get_logenv('SEARCH_ARRAY') eq '' then $
   set_logenv, 'SEARCH_ARRAY', $
               'naxis1>256, naxis2>256, ' + $
               'obs_type=FG*(simple), ' + $
               'wave=Ca*II*H*line'
print, get_logenv('SEARCH_ARRAY')
search_array = str2arr(get_logenv('SEARCH_ARRAY'))
prstr, search_array

if not exist(do_write) then do_write = 1

file_log_exec_times = concat_dir(dir_top, 'sot_l1_corr_exec_times.log')
if not exist(hrs_forward) then hrs_forward = 6
if not exist(minutes_backoff) then minutes_backoff = 0
if not exist(level0) and not keyword_set(quicklook) then level0 = 1
if not exist(mode_select) then mode_select = 'fg'
if not exist(hours_grid) then hours_grid = 1.0 ; 0.5

if ( (not exist(search_array)) and (not keyword_set(interactive)) ) then $
  search_array = ['naxis1=1024 && naxis2=512 || naxis1=512 && naxis2=512 || naxis1=864 && naxis2=512', $
                  'wave=Ca*II*H*line']

if not exist(percentd_min) then percentd_min = 98
if not exist(peak_str_min) then peak_str_min = -1 ;8

if not exist(case_num) then case_num = 0

use_shimizu = 0

; TODO - Fix the cat_sot refresh logic:
delvarx, cat_sot
delvarx, xdisp_rot, ydisp_rot, xdisp_ref_arr, ydisp_ref_arr, $
  indexp_arr, lil_compare_cube
delvarx, lil_reg_cube, cutoutp_add_wave_cube, $
  n_max_files, index_str_arr, wcs_ref

  if (exist(cat_sot) eq 0) or keyword_set(refresh_cat) then $
    sot_cat, t0, t1, cat_sot, files_sot, xrt=keyword_set(xrt), search_array=search_array, $
             level0=level0, quicklook=quicklook, count=count, tcount=tcount, refresh=refresh_cat

;ss_good = where(cat_sot.naxis1 ge 256 and $
;	         cat_sot.naxis1 ge 256 and $
;		 strlowcase(cat_sot.obs_type) eq 'fg (simple)' and $
;		  ( (strlowcase(cat_sot.wave) eq 'ca ii h line') ), n_good)
;		  ( (strlowcase(cat_sot.wave) eq 'ca ii h line') or $
;		    (strlowcase(cat_sot.wave) eq '6302a') or $
;		    (strlowcase(cat_sot.wave) eq 'cn bandhead 3883') or $
;		    (strlowcase(cat_sot.wave) eq 'g band 4305') or $
;		    (strlowcase(cat_sot.wave) eq 'nfi no move') or $
;		    (strlowcase(cat_sot.wave) eq 'tf h i 6563') or $
;		    (strlowcase(cat_sot.wave) eq 'blue cont 4504') or $
;		    (strlowcase(cat_sot.wave) eq 'green*cont*5550') or $
;		    (strlowcase(cat_sot.wave) eq 'red*cont*6684') ), n_good)
;cat_sot = cat_sot[ss_good]
;files_sot = files_sot[ss_good]
;count = n_good
  
  if count gt 0 then begin

    if keyword_set(interactive) then begin
      ss_mode = sot_umodes(incat=cat_sot, /int)
      files_sot_select = files_sot[ss_mode] ; [last_nelem(ss_mode,200)]
    endif else begin
      files_sot_select = files_sot
    endelse
    n_files_select = n_elements(files_sot_select)
;STOP 
    ; Optionally only process every 'nth' file:
    if keyword_set(do_select_sub_sampling) then begin
      every_nth = ''
      print, "Enter sub_sample frequency ('every_nth' - default is 1)", every_nth
      read, every_nth
      if every_nth eq '' then every_nth = 1 else every_nth = fix(every_nth)
    endif

    if keyword_set(every_nth) then begin
      ss_nth = indgen(n_files_select/every_nth) * every_nth
      files_sot_select = files_sot_select[ss_nth]
      n_files_select = n_elements(files_sot_select)
    endif

    t_files = file2time(files_sot_select)
    t_sec_files = anytim(t_files)
    ss_sort = sort(t_sec_files)
    files_sot_select = files_sot_select[ss_sort]

    if not exist(n_max_files_in) then $
      n_max_files = n_files_select else n_max_files = n_max_files_in
    if keyword_set(debug) then STOP
    mreadfits_header, files_sot_select, index_raw

    error_corr = 0
    for i=0, n_files_select-1 do begin
      t0_exec_full_loop_sec = anytim(!stime)
      delvarx, index_sot, data_sot

      file_sot = reform(files_sot_select[i])
      read_sot, file_sot, index_sot, data_sot

; If read_sot failed, try once more:
      if not exist(index_sot) then begin
        print, 'read_sot failed.  Waiting 100 seconds and trying again.'
        wait, 100
        read_sot, file_sot, index_sot, data_sot
      endif

; Define percentd for images:
      if tag_exist(index_sot, 'percentd') then percentd = index_sot.percentd else $
        percentd = (n_elements(where(data_sot ne 0))/n_elements(data_sot))*100
    
      if ( (percentd ge percentd_min) and $
           ((index_sot.sc_attx ne 0) or (index_sot.sc_atty ne 0)) ) then begin

; TOCHECK - NOTA BENA: ASSUMING SIGN IS NEGATIVE HERE!
if ( (not exist(xdisp_ref_arr)) or (keyword_set(shimizu_init)) ) then begin
  print, 'Using Shimizu offsets as first guesses for cross correlation.'

  sot_offsets = get_shimizu(index_sot.date_obs)
  xdisp_ref_arr = sot_offsets[0]
  ydisp_ref_arr = sot_offsets[1]
  xdisp_rot = sot_offsets[0]
  ydisp_rot = sot_offsets[1]
  if keyword_set(do_debug) then STOP, 'GO_BATCH_CORR: Stopping on request prior to updating XCEN/YCEN.'
endif else begin
  if ~ finite(peak_str) then begin
    print, 'PEAK_STR is not finite. Excluding.'
  endif else begin
    if (peak_str ge peak_str_min) then begin
      xdisp_ref_arr = [xdisp_ref_arr, xdisp_rot]
      ydisp_ref_arr = [ydisp_ref_arr, ydisp_rot]
    endif else begin
      print, 'PEAK_STR = ' + strtrim(peak_str,2)
      print, 'PEAK_STR less than minimum value.  Excluding.'
    endelse
  endelse
endelse
   
print, 'Updating level0 CRVAL1/2 and X/YCEN (initial guesses for sdo cutout location).'

xcen_orig = index_sot.xcen
ycen_orig = index_sot.ycen

index_sot.xcen   = index_sot.xcen + total(xdisp_ref_arr)
index_sot.ycen   = index_sot.ycen + total(ydisp_ref_arr)
index_sot.crval1 = $
  comp_fits_crval(index_sot.xcen, index_sot.cdelt1, index_sot.naxis1, index_sot.crpix1)
index_sot.crval2 = $
  comp_fits_crval(index_sot.ycen, index_sot.cdelt2, index_sot.naxis2, index_sot.crpix2)
index_sot.crpix1 = $
  comp_fits_crpix(index_sot.xcen, index_sot.cdelt1, index_sot.naxis1, index_sot.crval1)
index_sot.crpix2 = $
  comp_fits_crpix(index_sot.ycen, index_sot.cdelt2, index_sot.naxis2, index_sot.crval2)

        new_corr, index_sot, data_sot, indexp_sot, datap_sot, $
                  xcen_orig=xcen_orig, ycen_orig=ycen_orig, $
                  file_sot=file_sot, file_sdo=file_sdo, $
                  dt_max=dt_max, use_maps=use_map, $
                  xquery=xquery, $
                  xdisp_ref_arr=xdisp_ref_arr, ydisp_ref_arr=ydisp_ref_arr, $
                  use_shimizu=use_shimizu, $
                  do_prep_sot=do_prep_sot, $
                  norollcorrect=norollcorrect, nocrop2disk=nocrop2disk, $
                  xdisp_rot=xdisp_rot, ydisp_rot=ydisp_rot, $
                  peak_str=peak_str, $
                  last_good_xcen=last_good_xcen, last_good_ycen=last_good_ycen, $
                  do_img_plot=do_img_plot, do_hist_plot=do_hist_plot, $
                  do_plot_map=do_plot_map, do_blink=do_blink, $
                  error_corr=error_corr, verbose=verbose, _extra=_extra

        if ( (keyword_set(do_write)) and (error_corr ne 1) ) then begin
;         file_genx0 = concat_dir(dir_top, 'sot_corr_cat_ca_' + time2file(t0) + '_' + $
;                                 string(n_files_select, '$(i4.4)'))
          files_corr_cat0 = file_list(dir_top, 'sot_corr_cat_ca_' + time2file(t0) + '_' + $
                                      string(n_files_select, '$(i4.4)') + '*')
          if files_corr_cat0[0] ne '' then begin
            files_corr_cat0 = files_corr_cat0[0]
            rd_genx, files_corr_cat0, buff
            indexp_sot_arr = concat_struct(buff.savegen0, indexp_sot)
            ssw_file_delete, files_corr_cat0
          endif else begin
            indexp_sot_arr = indexp_sot
          endelse

          write_genxcat, indexp_sot_arr, topdir=dir_top, /nelements, $
                         catname=concat_dir(dir_top, 'sot_corr_cat_ca_' + $
                                            time2file(t0) + '_' + $
                                            string(n_files_select, '$(i4.4)'))
      
          delvarx, indexp_sot, indexp_sot_arr
        endif else begin
          if error_corr eq 1 then $
            print, 'Error occurred somewhere in NEW_CORR. No correlation result for this image.'
        endelse

      endif

      print, ' Finished ' + strtrim(i,2) + ' of ' + strtrim(n_files_select,2)

      t1_exec_full_loop_sec = anytim(!stime)
      full_loop_time_sec = t1_exec_full_loop_sec - t0_exec_full_loop_sec
      sbuff = 'Full loop time for this image = ' + strtrim(full_loop_time_sec,2) + ' seconds.'
      print, sbuff
      file_append, file_log_exec_times, sbuff

      error_corr = 0 ; Reset ERROR_CORR

    endfor

  endif

end
