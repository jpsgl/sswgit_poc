
pro go_go_batch_corr, t0, t1, search_array=search_array, $
  interactive=interactive, do_reg=do_reg, do_xstep=do_xstep, $
  only_list=only_list, dir_top=dir_top, _extra=_extra

if get_logenv('SOT_CORR_DB') eq '' then $
   set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                         'hinode_sdo_alignment')
print, get_logenv('SOT_CORR_DB')
dir_top = get_logenv('SOT_CORR_DB')
mk_dir, dir_top
mk_dir, concat_dir(get_logenv('SOT_CORR_DB'), 'database')

if not exist(dir_sot_event_lists) then $
  dir_sot_event_lists = concat_dir(dir_top, 'database')
; dir_sot_event_lists = '/net/topaz/Users/slater/soft/idl/idl_startup/sot'
filnam_fg_start = concat_dir(dir_sot_event_lists, 'start_fg.lis')
filnam_fg_stop  = concat_dir(dir_sot_event_lists, 'stop_fg.lis')

; Create list of all fg_start times:
buff_fg_start_no_parse = rd_tfile(filnam_fg_start)
buff_fg_start = rd_tfile(filnam_fg_start, 13, delim=' ')

t_fg_start = str_replace(reform(buff_fg_start[1,*]), '.', ' ')
ss_valid_time = where( (strmid(t_fg_start, 0,2) eq '20') and $
                       (strmid(t_fg_start, 4,1) eq  '/') and $
                       (strmid(t_fg_start, 7,1) eq  '/') and $
                       (strmid(t_fg_start,10,1) eq  ' ') and $
                       (strmid(t_fg_start,13,1) eq  ':') and $
                       (strmid(t_fg_start,16,1) eq  ':')       )
buff_fg_start_no_parse = buff_fg_start_no_parse[ss_valid_time]
buff_fg_start = buff_fg_start[*, ss_valid_time]
t_fg_start = t_fg_start[ss_valid_time]
t_fg_start_sec = anytim(t_fg_start)

; Sort unique times:
ss_uniq_sort_fg_start = uniq(t_fg_start_sec, sort(t_fg_start_sec))
buff_fg_start_no_parse = buff_fg_start_no_parse[ss_uniq_sort_fg_start]
buff_fg_start = buff_fg_start[*, ss_uniq_sort_fg_start]
t_fg_start = t_fg_start[ss_uniq_sort_fg_start]
t_fg_start_sec = t_fg_start_sec[ss_uniq_sort_fg_start]

t_fg_start_int = anytim(t_fg_start, /int)
n_t_fg_start = n_elements(t_fg_start)
t_fg_start_sec_delt = t_fg_start_sec[1:*] - t_fg_start_sec

t_start = t_fg_start
t_start_sec = t_fg_start_sec
t_start_int = anytim(t_start, /int)
n_t_start = n_elements(t_start)
t_start_sec_delt = t_start_sec[1:*] - t_start_sec
t_start_25_sec = t_start_sec + 25 ; TODO: Why is 25 sec added here?

hcr = ssw_hcr_query(ssw_hcr_make_query(t0, t1, instrument='sot', /all_select), /remove)
t0_hcr = hcr.starttime
t1_hcr = hcr.stoptime
if ( (n_elements(hcr) gt 1) and (anytim(t0_hcr[0]) lt anytim(t0)) ) then begin
   t0_hcr = t0_hcr[1:*]
   t1_hcr = t1_hcr[1:*]
endif
n_hcr = n_elements(t0_hcr)

for i=0,n_hcr-1 do begin

   go_batch_corr, t0_hcr[i], t1_hcr[i], level0=1, search_array=search_array, interactive=interactive, $
    refresh_cat=refresh_cat, $
    do_reg=do_reg, do_xstep=do_xstep, verbose=verbose, every_nth=every_nth, $
    lil_reg_cube=lil_reg_cube, add_waves=add_waves, add_map_waves=add_map_waves, $
    case_num=case_num, _extra=_extra
  print, 'FINISHED WITH HCR MODE ' + strtrim(i,2)

endfor

end
