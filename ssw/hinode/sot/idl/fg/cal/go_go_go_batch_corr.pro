
pro go_go_go_batch_corr, t0, t1, dir_top=dir_top, _extra=_extra

if not exist(t1) then t1 = reltime(t0, days=1, out='ccsds')

; Hardwire $SOT_CORR_DB for now:
;  set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
;                                        'hinode_sdo_alignment_hop79')
;  set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
;                                        'hinode_sdo_alignment_synop')
;  set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
;                                        'hinode_sdo_alignment_test')
   set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                         'hinode_sdo_alignment_ver18')

if get_logenv('SOT_CORR_DB') eq '' then $
   set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                         'hinode_sdo_alignment')
print, get_logenv('SOT_CORR_DB')
dir_top = get_logenv('SOT_CORR_DB')
mk_dir, dir_top

; Hardwire $MINI_ARCHIVES for now:
set_logenv, 'MINI_ARCHIVES', '/archive1/hinode/sot/mini_archives'
parent_mini_archives = get_logenv('MINI_ARCHIVES')
mk_dir, parent_mini_archives

if get_logenv('MINI_ARCHIVES') eq '' then $
   set_logenv, 'MINI_ARCHIVES', '/archive1/hinode/sot/mini_archives_ver17'

; Hardwire $SEARCH_ARRAY for now:
;set_logenv, 'SEARCH_ARRAY', $
;            'naxis1>256,naxis2>256,' + $
;            'obs_type=FG MG4 V/I,' + $
;            'wave=TF*Na*I*5896'
;set_logenv, 'SEARCH_ARRAY', $
;            'naxis1>256,naxis2>256,' + $
;            'obs_type=FG*shuttered*I*and*V,' + $
;            'wave=TF*Fe*I*6302'
;set_logenv, 'SEARCH_ARRAY', $
;            'naxis1>256, naxis2>256, ' + $
;            'obs_type=FG*(simple), ' + $
;            'wave=Ca*II*H*line || wave=TF*Fe*I*6302'
 set_logenv, 'SEARCH_ARRAY', $
             'naxis1>256, naxis2>256, ' + $
             'obs_type=FG*(simple), ' + $
             'wave=Ca*II*H*line'

;ss_syn = where(prog_no eq 19 and abs(xcen) lt  50. and abs(ycen) lt  50.)
;ss_w79 = where(prog_no eq 17 and abs(xcen) lt 800. and abs(ycen) lt 800.)

; This filters for only synoptic files:
;set_logenv, 'SEARCH_ARRAY', $
;            'naxis1>256, naxis2>256, ' + $
;            'obs_type=FG*(simple), ' + $
;            'wave=Ca*II*H*line || wave=G*band*4305, ' + $
;            'prog_no=19, ' + $
;            'xcen>-050 || xcen<050, ' + $
;            'ycen>-050 || ycen<050'
; This filters for only HOP 79 files:
;set_logenv, 'SEARCH_ARRAY', $
;            'naxis1>256, naxis2>256, ' + $
;            'obs_type=FG*(simple), ' + $
;            'wave=Ca*II*H*line || wave=G*band*4305, ' + $
;            'prog_no=17, ' + $
;            'xcen>-800 || xcen<800, ' + $
;            'ycen>-800 || ycen<800'

if get_logenv('SEARCH_ARRAY') eq '' then $
   set_logenv, 'SEARCH_ARRAY', $
     'naxis1>256, naxis2>256, ' + $
     'obs_type=FG*(simple), ' + $
     'wave=Ca*II*H*line'

;    'naxis1>256, naxis2>256, ' + $
;    'obs_type=FG*(simple), ' + $
;    'wave=Ca*II*H*line || ' + $
;    'wave=CN*bandhead*3883 || ' + $
;    'wave=G*band*4305 || ' + $
;    'wave=NFI*no*move || ' + $
;    'wave=TF*H*I*6563*base || ' + $
;    'wave=blue*cont*4504 || ' + $
;    'wave=green*cont*5550 || ' + $
;    'wave=red*cont*6684'

;    'naxis1>256, naxis2>256, ' + $
;    'obs_type=FG*(simple), ' + $
;    'wave=Ca*II*H*line || ' + $
;    'wave=6302A || ' + $
;    'wave=CN*bandhead*3883 || ' + $
;    'wave=G*band*4305 || ' + $
;    'wave=NFI*no*move || ' + $
;    'wave=TF*H*I*6563*base || ' + $
;    'wave=blue*cont*4504 || ' + $
;    'wave=green*cont*5550 || ' + $
;    'wave=red*cont*6684'

;    'naxis1=1024 && naxis2= 512 || ' + $
;    'naxis1= 512 && naxis2= 512 || ' + $
;    'naxis1= 864 && naxis2= 512, ' + $
;    'wave=Ca*II*H*line'

search_array = str2arr(get_logenv('SEARCH_ARRAY'), ',')

go_go_batch_corr, t0, t1, search_array=search_array, /sot_noscale, /sdo_noscale, $
                  dir_top=dir_top, _extra=_extra

;go_go_batch_corr, t0, t1, file_cases=file_cases, ss_start=ss_start, search_array=search_array, $
;  interactive=interactive, do_reg=do_reg, do_xstep=do_xstep, $
;  only_list=only_list, _extra=_extra

end
