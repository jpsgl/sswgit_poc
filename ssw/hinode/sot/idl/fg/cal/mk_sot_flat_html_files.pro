
pro mk_sot_flat_html_files, dir_top=dir_top, bfi=bfi,$
  by_date=by_date, _extra=_extra

;+
; NAME: 
; PURPOSE: 
; CALLING SEQUENCES:
; INPUT:
; OPTIONAL INPUT:
; INPUT KEYWORDS:
; OUTPUTS
;  HISTORY
; 2014-08-05 GLS
;-

if not exist(dir_top) then $
  dir_top = '/sanhome/slater/public_html/missions/hinode/sot/' + $
            (['nfi', 'bfi'])[keyword_set(bfi)] + '/flats'

; ****************************
;  Make various html files:
; ****************************

; Make the top level index.html file:
   html_arr = [ $
      "<html>", $
      "", $
      "<frameset cols='320,930,*' frameborder='0'>", $
      "<frame name='flat_dirs' src='flat_dir_links.html'>", $
      "<frame name='thumbnails' src='thumbnail_image_links.html'>", $
      "<frame name='images' src='images.html'>", $
      "</frameset>", $
      "", $
      "</html>", $
      "" ]
   file_append, concat_dir(dir_top, 'index.html'), html_arr, /new

; Make the 'thumbnail_image_links.html' file:
   html_arr = ["<html>", "<body>", "", "thumbnails", "", "</body>", "</html>"]
   file_append, concat_dir(dir_top, 'thumbnails.html'), html_arr, /new

; Make the 'images.html' file:
   html_arr = ["<html>", "<body>", "", "images", "", "</body>", "</html>"]
   file_append, concat_dir(dir_top, 'images.html'), html_arr, /new

; Make the flat directory link list file:
   ccd_side = (['?', '?'])[keyword_set(bfi)]
   flat_paths = $
      file_list(dir_top, (['sot_nfi_flat_', 'sot_bfi_flat_'])[keyword_set(bfi)] + $
                '????A_' + ccd_side + '_?x?_????????_????')
   n_flat_dirs = n_elements(flat_paths)

   if keyword_set(by_date) then begin
      t_dir= anytim(file2time(flat_paths), /ccsds)
      t_dir_sec = anytim(t_dir)
      ss_sort = sort(t_dir_sec)
      flat_paths = flat_paths[ss_sort]
      t_dir_sec = t_dir_sec[ss_sort]
   endif

   break_file, flat_paths, flat_dir_logicals, flat_dir_parents, flat_dir_arr

   html_arr = ["<html>", "<body>", ""]
   file_append, concat_dir(dir_top, 'flat_dir_links.html'), html_arr, /new

   html_arr = "<a href='" + concat_dir(flat_dir_arr, 'index.html') + $
              "' target='thumbnails'>" + flat_dir_arr + "</a><br/>"
   file_append, concat_dir(dir_top, 'flat_dir_links.html'), html_arr

   html_arr = ["", "</body>", "</html>", ""]
   file_append, concat_dir(dir_top, 'flat_dir_links.html'), html_arr

; Make the index.html files for all of the flat directories:

   for i=0, n_flat_dirs-1 do begin

      wave0 = strmid(flat_dir_arr[i],13,5)

      thumbnail_paths = file_list(flat_paths[i], 'data_20??????_??????_thumb.gif')
      break_file, thumbnail_paths, thumbnail_logicals, thumbnail_parents, $
                  thumbnail_file_arr
      thumbnail_file_arr = thumbnail_file_arr + '.gif'

      image_paths = file_list(flat_paths[i], 'data_20??????_??????.gif')
      break_file, image_paths, image_logicals, image_parents, image_file_arr
      image_file_arr = image_file_arr + '.gif'

      flat_thumbnail_paths = file_list(flat_paths[i], 'flat_20??????_??????_thumb.gif')
      break_file, flat_thumbnail_paths, flat_thumbnail_logicals, flat_thumbnail_parents, $
                  flat_thumbnail_file_arr
      flat_thumbnail_file_arr = flat_thumbnail_file_arr + '.gif'

      flat_image_paths = file_list(flat_paths[i], 'flat_20??????_??????.gif')
      break_file, flat_image_paths, flat_image_logicals, flat_image_parents, $
                  flat_image_file_arr
      flat_image_file_arr = flat_image_file_arr + '.gif'

      n_files = n_elements(image_file_arr)
;     times = anytim(indexp_sot.date_obs, /ccsds)
      times = rd_tfile(concat_dir(flat_paths[i], 'times_fits.txt'))

      html_arr = [ $
         "<html>", $
         "<body>", $
         "", $
         "<table border=2 cellpadding=2 cellspacing=3>" ]

      for j=0, n_files-1 do begin
         if j mod 5 eq 0 then html_arr = [html_arr, "<tr align=center>"]
         href = "<td><A HREF='" + image_file_arr[j] + "' target='images'><IMG SRC='" + $
                thumbnail_file_arr[j] + "'><br><em>" + wave0 + "<br>" + times[j] + $
                "</em></A></td>"
         if (j+1) mod 5 eq 0 then href = [href, "</tr>"]
         html_arr = [html_arr, href]
      endfor

      html_arr = [html_arr, ["</table>", "", "<table border=2 cellpadding=2 cellspacing=3>", $
                             "<tr align=center>"]]
      href = "<td><A HREF='" + flat_image_file_arr + "' target='images'><IMG SRC='" + $
             flat_thumbnail_file_arr + "'><br><em>" + wave0 + " Flat<br>" + times[0] + $
             "</em></A></td></tr></table>"  
      html_arr = [html_arr, [href, "", "</body>", "</html>", ""]]
      file_append, concat_dir(flat_paths[i], 'index.html'), html_arr, /new

   endfor

end
