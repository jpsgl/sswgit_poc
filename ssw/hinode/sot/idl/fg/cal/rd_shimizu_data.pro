
pro rd_shimizu_data, pnt_str=pnt_str, align_str=align_str, refresh=refresh, $
  date_obs=date_obs, r_solar=r_solar, t_r_solar=t_r_solar, $
  sot2dc_ns_a=sot2dc_ns_a, t_sot2dc_ns_a=t_sot2dc_ns_a, $
  sot2dc_ns_b=sot2dc_ns_b, t_sot2dc_ns_b=t_sot2dc_ns_b, $
  sot2dc_ew_a=sot2dc_ew_a, t_sot2dc_ew_a=t_sot2dc_ew_a, $
  sot2dc_ew_b=sot2dc_ew_b, t_sot2dc_ew_b=t_sot2dc_ew_b, $
  sot2sc_ns=sot2sc_ns, t_sot2sc_ns=t_sot2sc_ns, $
  sot2sc_ew=sot2sc_ew, t_sot2sc_ew=t_sot2sc_ew, $
  sot2xrt_ns_a=sot2xrt_ns_a, t_sot2xrt_ns_a=t_sot2xrt_ns_a, $
  sot2xrt_ns_b=sot2xrt_ns_b, t_sot2xrt_ns_b=t_sot2xrt_ns_b, $
  sot2xrt_ew_a=sot2xrt_ew_a, t_sot2xrt_ew_a=t_sot2xrt_ew_a, $
  sot2xrt_ew_b=sot2xrt_ew_b, t_sot2xrt_ew_b=t_sot2xrt_ew_b, $
  sot2xrt_ns=sot2xrt_ns, t_sot2xrt_ns=t_sot2xrt_ns, $
  sot2xrt_ew=sot2xrt_ew, t_sot2xrt_ew=t_sot2xrt_ew, $
  do_corrections=do_corrections, do_plot=do_plot, do_multi=do_multi, t0=t0, t1=t1, win0=win0

;+
;   Name: RD_SHIMIZU_DATA
;
;   Purpose: Read the Shimizu-generated Hinode alignment results table and return all parameters
;
;   Input Parameters:
;
;   Output Paramters:
;      See call.
;
;   Keyword Parameters:
;      
;   Calling Sequence:
;      IDL> rd_align_data, [output params here]
;
;   History:
;      03-feb-2009 - G.L.Slater
;   TODO:
;      - Change check on legitimate data values to use IS_NUMBER.PRO or the like
;-

if not exist(win0) then win0 = 0

align_str = hinode_time2alignment(refresh=refresh)

date_obs     = align_str.(00)
r_solar	     = align_str.(21)
sot2dc_ns_a  = align_str.(23)
sot2dc_ns_b  = align_str.(24)
sot2dc_ew_a  = align_str.(25)
sot2dc_ew_b  = align_str.(26)
sot2sc_ns    = align_str.(28)
sot2sc_ew    = align_str.(29)
sot2xrt_ns_a = align_str.(33)
sot2xrt_ns_b = align_str.(34)
sot2xrt_ew_a = align_str.(35)
sot2xrt_ew_b = align_str.(36)

ss_good = where( (date_obs ne '') and (align_str.(21) ne '') and (strtrim(align_str.(21),2) ne 'N/A'), n_good)
r_solar = double(align_str[ss_good].(21))
t_r_solar = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(23) ne '') and (strtrim(align_str.(23),2) ne 'N/A'), n_good)
sot2dc_ns_a = double(align_str[ss_good].(23))
t_sot2dc_ns_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(24) ne '') and (strtrim(align_str.(24),2) ne 'N/A'), n_good)
sot2dc_ns_b = double(align_str[ss_good].(24))
t_sot2dc_ns_b = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(25) ne '') and (strtrim(align_str.(25),2) ne 'N/A'), n_good)
sot2dc_ew_a = double(align_str[ss_good].(25))
t_sot2dc_ew_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(26) ne '') and (strtrim(align_str.(26),2) ne 'N/A'), n_good)
sot2dc_ew_b = double(align_str[ss_good].(26))
t_sot2dc_ew_b = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(28) ne '') and (strtrim(align_str.(28),2) ne 'N/A'), n_good)
sot2sc_ns = double(align_str[ss_good].(28))
t_sot2sc_ns = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(29) ne '') and (strtrim(align_str.(29),2) ne 'N/A'), n_good)
sot2sc_ew = double(align_str[ss_good].(29))
t_sot2sc_ew = date_obs[ss_good]

; Kluge for removing specific known bad table entries not yet removed by hinode_time2alignment:
ss_good = where( $
  (date_obs ne '')                     and $
  (align_str.(33) ne '')               and $
  (strtrim(align_str.(33),2) ne 'N/A') and $
  (strtrim(align_str.(33),2) ne '"Note: See ""Non-eclipse sheet"" for +2.5 reason"'), n_good)

sot2xrt_ns_a = double(align_str[ss_good].(33))
t_sot2xrt_ns_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(34) ne '') and (strtrim(align_str.(34),2) ne 'N/A'), n_good)
sot2xrt_ns_b = double(align_str[ss_good].(34))
t_sot2xrt_ns_b = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(35) ne '') and (strtrim(align_str.(35),2) ne 'N/A'), n_good)
sot2xrt_ew_a = double(align_str[ss_good].(35))
t_sot2xrt_ew_a = date_obs[ss_good]
 
ss_good = where( (date_obs ne '') and (align_str.(36) ne '') and (strtrim(align_str.(36),2) ne 'N/A'), n_good)
sot2xrt_ew_b = double(align_str[ss_good].(36))
t_sot2xrt_ew_b = date_obs[ss_good]

if keyword_set(do_corrections) then begin
  sot2xrt_ew_a[0] = -sot2xrt_ew_a[0]
endif

; Duplicate initial and final points in all arrays to cover points outside table times:

t0_table = anytim('01-oct-2006', /ccsds)
t1_table = anytim(ut_time(!stime), /ccsds)

sot2sc_ns      = [ sot2sc_ns[0],    sot2sc_ns,      sot2sc_ns[n_elements(sot2sc_ns)-1] ]
sot2sc_ew      = [ sot2sc_ew[0],    sot2sc_ew,      sot2sc_ew[n_elements(sot2sc_ew)-1] ]
sot2xrt_ns_a   = [ sot2xrt_ns_a[0], sot2xrt_ns_a,   sot2xrt_ns_a[n_elements(sot2xrt_ns_a)-1] ]
sot2xrt_ns_b   = [ sot2xrt_ns_b[0], sot2xrt_ns_b,   sot2xrt_ns_b[n_elements(sot2xrt_ns_b)-1] ]
sot2xrt_ew_a   = [ sot2xrt_ew_a[0], sot2xrt_ew_a,   sot2xrt_ew_a[n_elements(sot2xrt_ew_a)-1] ]
sot2xrt_ew_b   = [ sot2xrt_ew_b[0], sot2xrt_ew_b,   sot2xrt_ew_b[n_elements(sot2xrt_ew_b)-1] ]
t_sot2sc_ns    = [ t0_table,        t_sot2sc_ns,    t1_table ]
t_sot2sc_ew    = [ t0_table,        t_sot2sc_ew,    t1_table ]
t_sot2xrt_ns_a = [ t0_table,        t_sot2xrt_ns_a, t1_table ]
t_sot2xrt_ns_b = [ t0_table,        t_sot2xrt_ns_b, t1_table ]
t_sot2xrt_ew_a = [ t0_table,        t_sot2xrt_ew_a, t1_table ]
t_sot2xrt_ew_b = [ t0_table,        t_sot2xrt_ew_b, t1_table ]

; Create time arrays converted to seconds:
t_sec_sot2sc_ns    = anytim(t_sot2sc_ns)
t_sec_sot2sc_ew    = anytim(t_sot2sc_ew)
t_sec_sot2xrt_ns_a = anytim(t_sot2xrt_ns_a)
t_sec_sot2xrt_ns_b = anytim(t_sot2xrt_ns_b)
t_sec_sot2xrt_ew_a = anytim(t_sot2xrt_ew_a)
t_sec_sot2xrt_ew_b = anytim(t_sot2xrt_ew_b)

; Calculate averaged (a,b) sot2xrt parameters (ns, ew):


n_ns_a = n_elements(t_sot2sc_ns_a)
n_ns_b = n_elements(t_sot2sc_ns_b)

t_sot2xrt_ns_ab = t_sot2xrt_ns_a
sot2xrt_ns_ab = sot2xrt_ns_a

for i=0, n_ns_a-1 do begin
  ss_match_b = where(t_sot2xrt_ns_a[i] eq t_sot2xrt_ns_b, n_match_b)
  if n_match_b gt 0 then sot2xrt_ns_ab[i] = (sot2xrt_ns_a[i] + sot2xrt_ns_b[ss_match_b])/2
endfor

t_sot2xrt_ns_ba = t_sot2xrt_ns_b
sot2xrt_ns_ba = sot2xrt_ns_b

for i=0, n_ns_b-1 do begin
  ss_match_a = where(t_sot2xrt_ns_b[i] eq t_sot2xrt_ns_a, n_match_a)
  if n_match_a gt 0 then sot2xrt_ns_ba[i] = (sot2xrt_ns_b[i] + sot2xrt_ns_a[ss_match_a])/2
endfor

t_sot2xrt_ns = [t_sot2xrt_ns_ab, t_sot2xrt_ns_ba]
sot2xrt_ns   = [sot2xrt_ns_ab, sot2xrt_ns_ba]
ss_uniq_t_sot2xrt_ns = uniq(t_sot2xrt_ns)
t_sot2xrt_ns = t_sot2xrt_ns[ss_uniq_t_sot2xrt_ns]
sot2xrt_ns   =   sot2xrt_ns[ss_uniq_t_sot2xrt_ns]

t_sec_sot2xrt_ns = anytim(t_sot2xrt_ns)
ss_sort_t_sot2xrt_ns = sort(t_sec_sot2xrt_ns)
t_sec_sot2xrt_ns = t_sec_sot2xrt_ns[ss_sort_t_sot2xrt_ns]
t_sot2xrt_ns = t_sot2xrt_ns[ss_sort_t_sot2xrt_ns]
sot2xrt_ns   =   sot2xrt_ns[ss_sort_t_sot2xrt_ns]


n_ew_a = n_elements(t_sot2sc_ew_a)
n_ew_b = n_elements(t_sot2sc_ew_b)

t_sot2xrt_ew_ab = t_sot2xrt_ew_a
sot2xrt_ew_ab = sot2xrt_ew_a

for i=0, n_ew_a-1 do begin
  ss_match_b = where(t_sot2xrt_ew_a[i] eq t_sot2xrt_ew_b, n_match_b)
  if n_match_b gt 0 then sot2xrt_ew_ab[i] = (sot2xrt_ew_a[i] + sot2xrt_ew_b[ss_match_b])/2
endfor

t_sot2xrt_ew_ba = t_sot2xrt_ew_b
sot2xrt_ew_ba = sot2xrt_ew_b

for i=0, n_ew_b-1 do begin
  ss_match_a = where(t_sot2xrt_ew_b[i] eq t_sot2xrt_ew_a, n_match_a)
  if n_match_a gt 0 then sot2xrt_ew_ba[i] = (sot2xrt_ew_b[i] + sot2xrt_ew_a[ss_match_a])/2
endfor

t_sot2xrt_ew = [t_sot2xrt_ew_ab, t_sot2xrt_ew_ba]
sot2xrt_ew   = [sot2xrt_ew_ab, sot2xrt_ew_ba]
ss_uniq_t_sot2xrt_ew = uniq(t_sot2xrt_ew)
t_sot2xrt_ew = t_sot2xrt_ew[ss_uniq_t_sot2xrt_ew]
sot2xrt_ew   =   sot2xrt_ew[ss_uniq_t_sot2xrt_ew]

t_sec_sot2xrt_ew = anytim(t_sot2xrt_ew)
ss_sort_t_sot2xrt_ew = sort(t_sec_sot2xrt_ew)
t_sec_sot2xrt_ew = t_sec_sot2xrt_ew[ss_sort_t_sot2xrt_ew]
t_sot2xrt_ew = t_sot2xrt_ew[ss_sort_t_sot2xrt_ew]
sot2xrt_ew   =   sot2xrt_ew[ss_sort_t_sot2xrt_ew]

; Define xrt to spacecraft offsets:
xrt2sc_ns = sot2sc_ns - sot2xrt_ns
xrt2sc_ew = sot2sc_ew - sot2xrt_ew

; Define pointing structure:
pnt_str = {rsun:r_solar, t_r_solar:t_r_solar, $
           sot2dc_ns_a:sot2dc_ns_a, t_sot2dc_ns_a:t_sot2dc_ns_a, $
           sot2dc_ns_b:sot2dc_ns_b, t_sot2dc_ns_b:t_sot2dc_ns_b, $
           sot2dc_ew_a:sot2dc_ew_a, t_sot2dc_ew_a:t_sot2dc_ew_a, $
           sot2dc_ew_b:sot2dc_ew_b, t_sot2dc_ew_b:t_sot2dc_ew_b, $
           sot2sc_ns:sot2sc_ns, t_sot2sc_ns:t_sot2sc_ns, t_sec_sot2sc_ns:t_sec_sot2sc_ns, $
           sot2sc_ew:sot2sc_ew, t_sot2sc_ew:t_sot2sc_ew, t_sec_sot2sc_ew:t_sec_sot2sc_ew, $
           sot2xrt_ns_a:sot2xrt_ns_a, t_sot2xrt_ns_a:t_sot2xrt_ns_a, t_sec_sot2xrt_ns_a:t_sec_sot2xrt_ns_a, $
           sot2xrt_ns_b:sot2xrt_ns_b, t_sot2xrt_ns_b:t_sot2xrt_ns_b, t_sec_sot2xrt_ns_b:t_sec_sot2xrt_ns_b, $
           sot2xrt_ew_a:sot2xrt_ew_a, t_sot2xrt_ew_a:t_sot2xrt_ew_a, t_sec_sot2xrt_ew_a:t_sec_sot2xrt_ew_a, $
           sot2xrt_ew_b:sot2xrt_ew_b, t_sot2xrt_ew_b:t_sot2xrt_ew_b, t_sec_sot2xrt_ew_b:t_sec_sot2xrt_ew_b, $
	   sot2xrt_ns:sot2xrt_ns, t_sot2xrt_ns:t_sot2xrt_ns, t_sec_sot2xrt_ns:t_sec_sot2xrt_ns, $
           sot2xrt_ew:sot2xrt_ew, t_sot2xrt_ew:t_sot2xrt_ew, t_sec_sot2xrt_ew:t_sec_sot2xrt_ew}

if keyword_set(do_plot) then begin
  if not exist(t0) then t0 = date_obs[0]
  if not exist(t1) then t1 = date_obs[n_elements(date_obs)-1]
  tnames = tag_names(pnt_str)
  cleanplot

  if keyword_set(do_multi) then begin
    wdef,win0,2048,1024
    !p.multi=[0,1,11]
    for i=0,10 do begin
      utplot, pnt_str.(i*2+1)[1:*], pnt_str.(i*2)[1:*], timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
        psym=4, ystyle=1, xmargin=[5,5], ymargin=[0,0], xcharsize=2, xtitle=''
    endfor
  endif else begin
    wdef,win0,1024,512
    utplot,  pnt_str.( 1), pnt_str.( 0), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[ 0], psym=1
STOP
    utplot,  pnt_str.( 3), pnt_str.( 2), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[ 2] + '  ' + tnames[ 4], psym=1
    outplot, pnt_str.( 5), pnt_str.( 4), psym=4
;    utplot,  pnt_str.( 3), pnt_str.( 2), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
;      ystyle=1, xcharsize=2, title=tnames[ 2] + '  ' + tnames[ 4], psym=10, linestyle=1
;    outplot, pnt_str.( 5), pnt_str.( 4), psym=10, linestyle=2
;    outplot, pnt_str.( 3), pnt_str.( 2), psym=5
;    outplot, pnt_str.( 5), pnt_str.( 4), psym=6
STOP
    utplot,  pnt_str.( 7), pnt_str.( 6), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[ 6] + '  ' + tnames[ 8], psym=1
    outplot, pnt_str.( 9), pnt_str.( 8), psym=4
;    utplot,  pnt_str.( 7), pnt_str.( 6), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
;      ystyle=1, xcharsize=2, title=tnames[ 6] + '  ' + tnames[ 8], psym=10, linestyle=1
;    outplot, pnt_str.( 9), pnt_str.( 8), psym=10, linestyle=2
;    outplot, pnt_str.( 7), pnt_str.( 6), psym=5
;    outplot, pnt_str.( 9), pnt_str.( 8), psym=6
STOP
    utplot,  pnt_str.( 9), pnt_str.(10), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[10], psym=1
STOP
    utplot,  pnt_str.(11), pnt_str.(12), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[12], psym=1
STOP
    utplot,  pnt_str.(13), pnt_str.(14), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[14] + '  ' + tnames[16], psym=-1, linestyle=2
STOP
    outplot, pnt_str.(15), pnt_str.(16), psym=-4, linestyle=2
;    utplot,  pnt_str.(15), pnt_str.(16), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
;      ystyle=1, xcharsize=2, title=tnames[14] + '  ' + tnames[16], psym=-1, linestyle=2
;    utplot,  pnt_str.(13), pnt_str.(14), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
;      ystyle=1, xcharsize=2, title=tnames[14] + '  ' + tnames[16], psym=10, linestyle=1
;    outplot, pnt_str.(15), pnt_str.(16), psym=10, linestyle=2
;    outplot, pnt_str.(13), pnt_str.(14), psym=5
;    outplot, pnt_str.(15), pnt_str.(16), psym=6
STOP
    utplot,  pnt_str.(17), pnt_str.(18), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
      ystyle=1, xcharsize=2, title=tnames[18] + '  ' + tnames[20], psym=-1, linestyle=1
STOP
    outplot, pnt_str.(19), pnt_str.(20), psym=-4, linestyle=2
;    utplot,  pnt_str.(19), pnt_str.(20), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
;      ystyle=1, xcharsize=2, title=tnames[18] + '  ' + tnames[20], psym=-1, linestyle=2
;    utplot,  pnt_str.(17), pnt_str.(18), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
;      ystyle=1, xcharsize=2, title=tnames[18] + '  ' + tnames[20], psym=10, linestyle=1
;    outplot, pnt_str.(19), pnt_str.(20), psym=10, linestyle=2
;    outplot, pnt_str.(17), pnt_str.(18), psym=5
;    outplot, pnt_str.(19), pnt_str.(20), psym=6
STOP
    for i=0,10 do begin
      utplot, pnt_str.(i*2+1), pnt_str.(i*2), timerange=[anytim(t0,/ccsds), anytim(t1,/ccsds)], $
        ystyle=1, xcharsize=2, title=tnames[i*2], psym=1
;      wait,1
STOP
    endfor
  endelse

endif

end
