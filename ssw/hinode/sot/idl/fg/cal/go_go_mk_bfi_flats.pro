
;go_go_mk_bfi_flats

sttim = anytim(last_flat(/bfi), /ccsds, /sec)
entim = anytim(ut_time(), /ccsds)

go_mk_bfi_flat, sttim, /mk_flat, /do_print, max_err=.5, /use_fg_prep, $
  maxiter=5, level0=1 ; , /do_make_top, /do_make_flat, /do_make_only

sttim = anytim(last_flat(/bfi), /ccsds, /sec)
entim = anytim(ut_time(), /ccsds)

go_mk_bfi_flat, sttim, /mk_flat, /do_print, max_err=.5, /use_fg_prep, $
  maxiter=5, level0=0 ; , /do_make_top, /do_make_flat, /do_make_only

mk_sot_flat_html_files, /bfi, /by_date

end
