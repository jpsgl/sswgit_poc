
pro sot_make_mini_movies_gls, t0, t1, outcube, grid=grid, _extra=_extra, register=register, $
                          ref_index=ref_index, derotate=derotate, label_cubes=label_cubes, $
                          sdo_clip=sdo_clip, debug=debug, do_www=do_www, $
                          no_register=no_register, no_derotate=no_derotate, no_www=no_www

if n_elements(grid) eq 0 then grid=50
register = 1-keyword_set(no_register)
derotate = 1-keyword_set(no_derotate)
do_www = 1-keyword_set(no_www)
debug = keyword_set(debug)

parent_in = '/archive1/hinode/sot/mini_archives_ver16/'
;parent_www = concat_dir(parent_in,'mini_www')
parent_www = '/sanhome/slater/public_html/mini_archives/mini_www'
parents = concat_dir(parent_in,['mini_sot_'+str2arr('unshift,shifted'),'mini_sdo'])

hcr=ssw_hcr_query(ssw_hcr_make_query(t0,t1,instrument='sot',/all_select),/remove)
t0=hcr.starttime
t1=hcr.stoptime
labcubes=keyword_set(label_cubes)

nhcr=n_elements(hcr)
for i=0,nhcr-1 do begin
      box_message,arr2str([t0[i],t1[i]],' -- ') 
      f0=ssw_time2filelist(t0[i],t1[i],parent=parents[0],count=cnt0)
      f1=ssw_time2filelist(t0[i],t1[i],parent=parents[1],count=cnt1)
      f2=ssw_time2filelist(t0[i],t1[i],parent=parents[2],count=cnt2)
      if f0[0] ne '' and (cnt0 eq cnt2) then begin 
         if keyword_set(grid) then begin
           ss=grid_data(file2time(f0),nsamp=grid)
           f0=f0[ss]
           f1=f1[ss]
           f2=f2[ss]
         endif
         delvarx,ii0,dd0,ii1,dd1,ii2,dd2,ii3,dd3      
         mreadfits,f0,ii0,dd0,_extra=_extra
         mreadfits,f1,ii1,dd1,_extra=_extra
         mreadfits,f2,ii2,dd2,_extra=_extra
;        s0=ssw_sigscale(ii0,dd0)
;        s1=ssw_sigscale(ii1,dd1)
;        s2=ssw_sigscale(ii2,dd2,/log)
         s0=dd0<1200
         s1=dd1<1200
         s2=dd2<4000
         if keyword_set(register) then  begin 
;           ssw_register,ii0,dd0<1200,ii3,dd0_reg,ref_index=ref_index,derotate=derotate,_extra=_extra
            ssw_register,ii0,dd0<1200,ii3,dd0_reg,ref_index=0,derotate=derotate,_extra=_extra
;           s0_reg = ssw_sigscale(ii3,dd0_reg,/log)
            s0_reg = bytscl(dd0_reg)
;           ssw_register,ii0,dd2<4000,ii3,dd2_reg,ref_index=ref_index,derotate=derotate,_extra=_extra
            ssw_register,ii0,dd2<4000,ii3,dd2_reg,ref_index=0,derotate=derotate,_extra=_extra
;           s2_reg = ssw_sigscale(ii3,dd2_reg,/log)
            s2_reg = bytscl(dd2_reg)

            s0 = bytscl(s0)
            s1 = bytscl(s1)
            s2 = bytscl(s2)
            s0_reg = bytscl(s0_reg)
            s2_reg = bytscl(s2_reg)

            siz_s0 = size(s0)

            s02_reg = bytarr(siz_s0[1],siz_s0[2],siz_s0[3]*2)
            for j=0, siz_s0[3]-1 do s02_reg[0,0,(j*2)]   = s0_reg[*,*,j]
            for j=0, siz_s0[3]-1 do s02_reg[0,0,(j*2)+1] = s2_reg[*,*,j]
            for j=0, 2*siz_s0[3]-1 do begin
               s02_reg[0:7,*,j] = 0
               s02_reg[*,0:7,j] = 0
               s02_reg[(siz_s0[1]-1-7):*,*,j] = 0
               s02_reg[*,(siz_s0[2]-1-7):*,j] = 0
            endfor

            s0_double = bytarr(siz_s0[1],siz_s0[2],siz_s0[3]*2)
            for j=0, siz_s0[3]-1 do s0_double[0,0,(j*2)]   = s0[*,*,j]
            for j=0, siz_s0[3]-1 do s0_double[0,0,(j*2)+1] = s0[*,*,j]
            for j=0, 2*siz_s0[3]-1 do begin
               s0_double[0:7,*,j] = 0
               s0_double[*,0:7,j] = 0
               s0_double[(siz_s0[1]-1-7):*,*,j] = 0
               s0_double[*,(siz_s0[2]-1-7):*,j] = 0
            endfor

            s1_double = bytarr(siz_s0[1],siz_s0[2],siz_s0[3]*2)
            for j=0, siz_s0[3]-1 do s1_double[0,0,(j*2)]   = s1[*,*,j]
            for j=0, siz_s0[3]-1 do s1_double[0,0,(j*2)+1] = s1[*,*,j]
            for j=0, 2*siz_s0[3]-1 do begin
               s1_double[0:7,*,j] = 0
               s1_double[*,0:7,j] = 0
               s1_double[(siz_s0[1]-1-7):*,*,j] = 0
               s1_double[*,(siz_s0[2]-1-7):*,j] = 0
            endfor

            s2_double = bytarr(siz_s0[1],siz_s0[2],siz_s0[3]*2)
            for j=0, siz_s0[3]-1 do s2_double[0,0,(j*2)]   = s2[*,*,j]
            for j=0, siz_s0[3]-1 do s2_double[0,0,(j*2)+1] = s2[*,*,j]
            for j=0, 2*siz_s0[3]-1 do begin
               s2_double[0:7,*,j] = 0
               s2_double[*,0:7,j] = 0
               s2_double[(siz_s0[1]-1-7):*,*,j] = 0
               s2_double[*,(siz_s0[2]-1-7):*,j] = 0
            endfor

;           outcube=[[s2_double,s02_reg], [s0_double,s1_double]]
            outcube=[s0_double, s1_double, s2_double, s02_reg]
         endif else outcube=[bytscl(s0), bytscl(s1), bytscl(s2)]
         if keyword_set(debug) then begin
            xstepper,outcube
            stop,'s0,s1,s2,s02_reg,outcube'
         endif
         
         if do_www then begin
            mpath=concat_dir(ssw_time2paths(parent=parent_www,t0[i]), $
                             time2file(hcr[i].starttime)) & mk_dir,mpath
            if get_logenv('check_path') ne '' then stop,mpath
            mname='sot_aia_'+time2file(hcr[i].starttime)
            times=[ii0.date_obs,ii0.date_obs]
            times=times[sort(times)] ; since outcube is now doubled
            special_movie2,times,outcube,movie_name=mname,movie_dir=mpath, $
                           /nogif,/ffmpeg,_extra=_extra, html=html,table=3, $
                           /label,/lable,/inctime,thumbsize=250
            film_thumbnail_remove,thumbdir=mpath
         endif
      endif ; eise box_message,'Mis-match SOT:AIA >> ' + hcr[i].starttime
      
endfor

return
end

