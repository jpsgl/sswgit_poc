
FUNCTION fg_get_reg_coeff, t0, wave0=wave0, wave_ref=wave_ref, gband_ref=gband_ref, $
  filnam_geny=filnam_geny, p_arr=p_arr, q_arr=q_arr, sot_reg_coeff=sot_reg_coeff, quiet=quiet

;+
; NAME:
;       get_fg_reg_coeff
; PURPOSE:
;       Return FG registration coefficients for a specified time and two specified wave bands 
; CATEGORIES:
;       Image registration, SOT
; SAMPLE CALLING SEQUENCE:
;	pq_cube = fg_get_reg_coeff(t0 [,wave0=wave0] [,wave_ref=wave_ref]
;		    [,gband_ref=gband_ref] [,filnam_geny=filnam_geny] 
;		    [,p_arr=p_arr] [,q_arr=q_arr] [,sot_reg_coeff=sot_reg_coeff]
;		    [,quiet=quiet]
; INPUTS:
;	t0 - Reference time
;	wave0 - Acronym for wave band for which reg coefficients are desired (e.g., 'gband')
;		Acronyms are as follows
;			'gband'	= 'G*band*4305'
;			'blue'	= 'blue*cont*4504'"
;			'red'	= 'red*cont*6684'"
;			'green'	= 'green*cont*5550'"
;			'ca'	= 'Ca*II*H*line'"
;			'cn'	= 'CN*bandhead*3883'"	                   
; OUTPUTS
;	pq_cube - Three dimensional array of registration coefficients of the form
;		  [2,2,2], where:
;			pq_cube(*,*,0) = p, the polynomial coefficient array for x coord
;					 transformation in poly_2d
;			pq_cube(*,*,1) = q, the polynomial coefficient array for y coord
;					 transformation in poly_2d
; OPTIONAL OUTPUTS
;	p_arr - Polynomial coefficients for x coordinate transformation via poly_2d
;	q_arr - Polynomial coefficients for y coordinate transformation via poly_2d
;	sot_reg_coeff - The structure array of registration coefficients for the entire
;			mission
;
;MODIFICATION HISTORY
;v0.9  Written by G. Slater, LMSAL, Oct. 2007
;v1.0  Minor mods to syntax, name change, T. Berger, LMSAL 18-Oct-2007
;v1.1  Added coefficient file bomb-out. TEB, LMSAL, 19-Oct-2007.
;-
;-----------------------------------------------------------------------------
;;Name, version, and timing information
prognam = 'FG_GET_REG_COEFF.PRO'
progver = 'V1.0'

; Set default section:
; ----------------------
if N_ELEMENTS(wave0) eq 0 then wave0 = 'gband'
if N_ELEMENTS(wave_ref) eq 0 then wave_ref = 'blue'
; ----------------------

if exist(filnam_geny) then begin
  filnam_geny0 = filnam_geny
endif else begin
  if get_logenv('SOT_REG_COEFF') eq '' then begin
     MESSAGE,'Coefficient file not found. Returning...'
     RETURN,-1
  end
  filnam_geny0 = get_logenv('SOT_REG_COEFF')
endelse

restgenx, file=filnam_geny0, sot_reg_coeff

if wave0 eq 'blue' then begin
  p_arr = fltarr(2,2)
  q_arr = fltarr(2,2)
endif else begin
  ss_match = WHERE( ((sot_reg_coeff.wave eq wave0) and (sot_reg_coeff.wave_ref eq wave_ref)), n_match)
  if n_match gt 0 then begin
    t_coef_start = anytim(sot_reg_coeff(ss_match).date_start)
    t_coef_end = anytim(sot_reg_coeff(ss_match).date_end)
    t_coef_avg = (t_coef_start + t_coef_end)/2
    ss_close = tim2dset(anytim(t_coef_avg,/int), anytim(t0,/int))
    p_arr = sot_reg_coeff(ss_match(ss_close)).p
    q_arr = sot_reg_coeff(ss_match(ss_close)).q
  endif else begin
    MESSAGE, 'No match to wave0 and wave_ref in sot_reg_coeff database.  Stopping.'
  endelse
endelse

if KEYWORD_SET(gband_ref) then begin
  ss_match = WHERE( ((sot_reg_coeff.wave eq 'gband') and (sot_reg_coeff.wave_ref eq wave_ref)), n_match)
  if n_match gt 0 then begin
    t_coef_start = anytim(sot_reg_coeff(ss_match).date_start)
    t_coef_end = anytim(sot_reg_coeff(ss_match).date_end)
    t_coef_avg = (t_coef_start + t_coef_end)/2
    ss_close = tim2dset(anytim(t_coef_avg,/int), anytim(t0,/int))
    p_gb = sot_reg_coeff(ss_match(ss_close)).p
    q_gb = sot_reg_coeff(ss_match(ss_close)).q
  endif else begin
    MESSAGE, 'No match to wave0 and wave_ref in sot_reg_coeff database.  Stopping.'
  endelse
  ;STOP
  p_arr = p_arr - p_gb
  q_arr = q_arr - q_gb
endif

RETURN, [ [[p_arr]], [[q_arr]] ]
END
