
pro go_go_new_corr, file_cases, ss_start=ss_start, search_arr=search_arr, $
  interactive=interactive, do_reg=do_reg, do_xstep=do_xstep, $
  only_list=only_list, _extra=_extra

if not exist(file_cases) then $
  file_cases = '/sanhome/nitta/public_html/outgoing/hinode/output_find_flares_sot_sp_fg_20150818.txt'
buff = rd_tfile(file_cases)
t0_arr = anytim(reverse(reform(strmid(buff ,9,17))), /ccsds)
t1_arr = anytim(reverse(reform(strmid(buff,29,17))), /ccsds)

; TODO - Add Ted's case here:
t0_added = ['22-jun-2015 17:34']
t1_added = ['22-jun-2015 20:00']
if exist(t0_added) then begin
  t0_added = anytim(t0_added, /ccsds)
  t1_added = anytim(t1_added, /ccsds)
  t0_arr = [t0_added, t0_arr]
  t1_arr = [t1_added, t1_arr]
endif

if keyword_set(only_list) then begin
  prstr, t0_arr + '   ' + t1_arr

  return
endif

t0_sec_arr = anytim(t0_arr)
t1_sec_arr = anytim(t1_arr)
n_case = n_elements(t0_arr)

if not exist(dir_sot_event_lists) then $
  dir_sot_event_lists = '/net/topaz/Users/slater/soft/idl/idl_startup/sot'
filnam_fg_start = concat_dir(dir_sot_event_lists, 'start_fg.lis')
filnam_fg_stop  = concat_dir(dir_sot_event_lists, 'stop_fg.lis')

; Create list of all fg_start times:
buff_fg_start_no_parse = rd_tfile(filnam_fg_start)
buff_fg_start = rd_tfile(filnam_fg_start, 13, delim=' ')

t_fg_start = str_replace(reform(buff_fg_start[1,*]), '.', ' ')
ss_valid_time = where( (strmid(t_fg_start, 0,2) eq '20') and $
                       (strmid(t_fg_start, 4,1) eq  '/') and $
                       (strmid(t_fg_start, 7,1) eq  '/') and $
                       (strmid(t_fg_start,10,1) eq  ' ') and $
                       (strmid(t_fg_start,13,1) eq  ':') and $
                       (strmid(t_fg_start,16,1) eq  ':')       )
buff_fg_start_no_parse = buff_fg_start_no_parse[ss_valid_time]
buff_fg_start = buff_fg_start[*, ss_valid_time]
t_fg_start = t_fg_start[ss_valid_time]
t_fg_start_sec = anytim(t_fg_start)
ss_uniq_sort_fg_start = uniq(t_fg_start_sec, sort(t_fg_start_sec))

buff_fg_start_no_parse = buff_fg_start_no_parse[ss_uniq_sort_fg_start]
buff_fg_start = buff_fg_start[*, ss_uniq_sort_fg_start]
t_fg_start = t_fg_start[ss_uniq_sort_fg_start]
t_fg_start_sec = t_fg_start_sec[ss_uniq_sort_fg_start]
t_fg_start_int = anytim(t_fg_start, /int)
n_t_fg_start = n_elements(t_fg_start)
t_fg_start_sec_delt = t_fg_start_sec[1:*] - t_fg_start_sec

t_start = t_fg_start
t_start_sec = t_fg_start_sec
t_start_int = anytim(t_start, /int)
n_t_start = n_elements(t_start)
t_start_sec_delt = t_start_sec[1:*] - t_start_sec
t_start_25_sec = t_start_sec + 25

if not exist(ss_start) then ss_start = 0
for i=ss_start,n_case-1 do begin
  t_diff_start_sec = t0_sec_arr[i] - t_start_25_sec
  max_pos_diff = max(where(t_diff_start_sec gt 0), ss_max)
  t0_start = anytim(t_start[ss_max], /ccsds)
  print, 'LATEST PRIOR FG_START: ' + anytim(t0_start,  /ccsds)
  print, 'Requested start time:  ' + anytim(t0_arr[i], /ccsds)
; prstr, [anytim(t0_start, /ccsds), anytim(t0_arr[i], /ccsds)]

; go_sdo_corr, t0_arr[i], t1_arr[i], level0=1, search_arr=search_arr, interactive=interactive, $
  go_new_corr, t0_start,  t1_arr[i], level0=1, search_arr=search_arr, interactive=interactive, $
    do_reg=do_reg, do_xstep=do_xstep, verbose=verbose, every_nth=every_nth, $
    lil_reg_cube=lil_reg_cube, _extra=_extra
  print, 'FINISHED WITH CASE ' + strtrim(i,2)

  if keyword_set(do_xstep) then xstepper, lil_reg_cube

endfor


end

