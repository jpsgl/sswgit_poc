
pro go_nitta_corr, t0, t1, _extra=_extra

if not exist(dir_top) then $
   dir_top = concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                       'hinode_sdo_alignment_nitta')

go_go_go_batch_corr, t0, t1, dir_top=dir_top, _extra=_extra

end
