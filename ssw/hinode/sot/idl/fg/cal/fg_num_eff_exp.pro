FUNCTION fg_num_eff_exp, iindex, QUIET=quiet, QSTOP=qstop
;+
;   Name: fg_num_exp
;
;   Purpose: number of effective exposures for IV or IQUV (shuttered or shutterless), 
;              for dark correction and intensity normalization purposes
;
;   Input Parameters:
;      iindex -- SSW index header for any type of FG IV or IQUV (if array, all must be same observable)
;
;   Output Paramters:
;      neff = 2 or 4 element array of number of exposures for I,V or I,Q,U,V
;
;   Keyword Parameters:
;      none
;      
;   Calling Sequence:
;      IDL> neff = fg_num_eff_exp(index[0])
;
;   History:
;v0.9   10-Oct-2007 - T. Tarbell, LMSAL
;v1.0   Minor syntax changes, logic fix on index[0] check. T. Berger,
;       LMSAL  18-Oct-2007.  
;v1.1   Added FG simple gen_id and changed "Unknown..." statement. TEB,
;       LMSAL, 19-Oct-2007.
;v1.11  Changed to gt_tagval and added missing values for handling
;       flats and darks. TEB 25-Oct-2007. 
;v1.12  Bug fix in assignment of index from iindex. TEB 6-Nov-2007.
;v1.13  New gen_id 7 (MG2 I & V but really Dopplergram) and new obs_id's 69-77. TDT 3-Jan-2008.
;v1.14  New gen_id's 4, 6, 9 and new obs_id's 78-85. TDT 8-Apr-2008.
;v1.15  Added old gen_id's 34-36 and old obs_id's 34-36, 45-47 for future use. TDT 16-Apr-2008.
;v1.16  New gen_id's 8, 18, and new obs_id's 86-9.  TDT 9-Mar-2009
;v1.17  2011-12-16 - Greg Slater, LMSAL. Handled gen_id 12 for focus scans.
;-
;-----------------------------------------------------------------------------
;;Name, version
prognam = 'FG_NUM_EFF_EXP.PRO'
progver = 'V1.1.7'

loud = 1 - KEYWORD_SET(quiet)
niv = REPLICATE(1,2)
niuv = REPLICATE(1,3)
niquv = REPLICATE(1,4)
nv_or_dg_only = [1]             ; Note these are already dark & flatfield corrected

if (N_ELEMENTS(iindex) gt 1) then index = REFORM(iindex[0]) else index=iindex
fgnint = gt_tagval(index,/fgnint,missing=1)
gen_id = gt_tagval(index,/gen_id,missing=1)
obs_id = gt_tagval(index,/obs_id,missing=1)

if KEYWORD_SET(qstop) then STOP

if (loud) then MESSAGE,/INFO,'Gen_id = '+STRTRIM(gen_id,2)

case gen_id of

  1: begin                                    ;FG (simple)
       neff = 1
       if (loud) then MESSAGE,/info,fg_genid(1)	; obs id 1, 71-2
    end

  2: begin
       neff = niv*fgnint*2                     ; FGIV
	if (loud) then MESSAGE,/info,fg_genid(2)	; obs id 2, 41, 42, 61-67
    end

  3: begin
       neff = niquv*fgnint*4                   ; FGIQUV  (but obs_id's 69-70 and 74-77 are really I&V DG
	if (loud) then MESSAGE,/info,fg_genid(3) 	; obs_id 3, 57, 58, 59, 60, 68-70, 74-77
    end

  4: begin
       neff = nv_or_dg_only*fgnint*8                     ; FG MG4 V/I (8 images)
	if (loud) then MESSAGE,/info,fg_genid(4)	; obs id 83, 85
    end

  6: begin
       neff = nv_or_dg_only*fgnint*4                   ; FG MG2 V/I (4 images)
	if (loud) then MESSAGE,/info,fg_genid(6)	; obs id 81-82, 84
    end

  7: begin
       neff = niv*fgnint*4                   ; MG2 I & V, but really Fe I 5576 I & Dopplergram
	if (loud) then MESSAGE,/info,fg_genid(7) 	; obs_id 73
    end

  8: begin
     neff = nv_or_dg_only*fgnint*2                     ; MG1 V/I (already dark & flatfield corrected)
	if (loud) then MESSAGE,/info,fg_genid(8) 	; obs_id 8, 88-89
    end

  9: begin
       neff = nv_or_dg_only*fgnint*4                     ; FG DG4 (only vel downloaded)
	if (loud) then MESSAGE,/info,fg_genid(9)	; obs id 78-80
    end

  12: begin
        neff = 1                                        ; FG focus scan                  
        if (loud) then MESSAGE,/info,fg_genid(12)	; obs id 12
     end

  17: begin
        neff = niv*fgnint*2                             ; FGIV (.1s) obsolete, probably never run in flight
        if (loud) then MESSAGE,/info,fg_genid(17)       ; obs_id 17
     end

  18: begin
        neff = niuv*4*(fgnint/7)                   ; FGIVDG shuttered IV & Dopplergram
        if (loud) then MESSAGE,/info,fg_genid(18)    ; obs_id 86-87
     end

  32: begin			; FGSIV
         neff = niv*fgnint*16	; obs_id 32
         if (obs_id eq 44) then neff(0) = neff(0)/2
         if (loud) then MESSAGE,/info,fg_genid(32)
      end	

  33: begin			; FGSIQUV
        neff = niquv*fgnint*8	; obs_id 33
	 if (obs_id gt 33) then neff(0) = neff(0)/2	; obs_id 43 or 54
	 if (obs_id eq 54) then neff(3) = neff(3)/2
        if (loud) then MESSAGE,/info,fg_genid(33)
     end

  34: begin			; FGSIQ obsolete, probably never run in flight
         neff = niv*fgnint*16	; obs_id 34, needs to be checked against real data
         if (obs_id eq 45) then neff(0) = neff(0)/2
         if (loud) then MESSAGE,/info,fg_genid(34)
      end	

  35: begin			; FGSIU obsolete, probably never run in flight
         neff = niv*fgnint*16	; obs_id 35, needs to be checked against real data
         if (obs_id eq 46) then neff(0) = neff(0)/2
         if (loud) then MESSAGE,/info,fg_genid(35)
      end	

  36: begin			; FGSIUV obsolete, probably never run in flight
         neff = niuv*fgnint*16	; obs_id 36, needs to be checked against real data
         if (obs_id eq 47) then neff(0) = neff(0)/2
         if (loud) then MESSAGE,/info,fg_genid(36)
      end	

  38: begin			; FGSIV (.2s)
        neff = niv*fgnint*8	; obs_id 38
	 if (obs_id eq 55) then neff(0) = neff(0)/2
        if (loud) then MESSAGE,/info,fg_genid(38)
     end	

  39: begin			; FGSIV (.1s)
        neff = niv*fgnint*8	; obs_id 39
	 if (obs_id eq 56) then neff(0) = neff(0)/2
        if (loud) then MESSAGE,/info,fg_genid(39)
     end	

  else: begin			; other GEN_ID
          MESSAGE,/INFO,'GEN_ID, OBS_ID not yet implemented: ',gen_id, obs_id
	   if (index.naxis eq 3) then neff = REPLICATE(1,index.naxis3) else neff = 1
       end	
endcase

if (loud) then begin
  nim = N_ELEMENTS(neff)
  MESSAGE,/info,'Number of images in set = '+STRTRIM(nim,2)
  for i=0,nim-1 do MESSAGE,/info,'Number of effective exposures in image '+STRTRIM(i+1,2)+' = '+STRTRIM(neff[i],2)
end

RETURN, neff
END
