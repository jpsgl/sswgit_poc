
function xrt_fix_pnt, iindex

;+
;   Name: XRT_FIX_PNT
;
;   Purpose: Update XRT header values for CRVAL1, CRVAL2, XCEN, YCEN 
;            from Shimizu-generated Hinode alignment results table
;
;   Calling Sequence:
;      IDL> oindex = xrt_fix_pnt(iindex)
;
;   History:
;      08-jun-2014 - G.L.Slater
;-

  oindex = iindex

  naxis1   = iindex.naxis1
  naxis2   = iindex.naxis2
  chip_sum = iindex.chip_sum
  p1col    = iindex.p1col
  p2col    = iindex.p2col
  crpix1   = iindex.crpix1
  crpix2   = iindex.crpix2
  crval1   = iindex.crval1
  crval2   = iindex.crval2
  xcen     = iindex.xcen
  ycen     = iindex.ycen
  sc_attx  = iindex.sc_attx
  sc_atty  = iindex.sc_atty

; Define correct value arrays for pointing tags (CRPIX1, CRPIX2, CRVAL1, CRVAL2, XCEN, YCEN):
; crval1_fix     = ( (naxis1 * chip_sum + 1)/2. - ((2048 + 1)/2. - p1col) ) * platescl_fix/chip_sum + sc_attx
; crval2_fix     = ( (naxis2 * chip_sum + 1)/2. - ((2048 + 1)/2. - p1row) ) * platescl_fix/chip_sum + sc_atty
; crval1_patrick = ( (p1col + p2col)/2. - 1023.5 ) * platescl_fix/chip_sum + sc_attx
; crval2_patrick = ( (p1row + p2row)/2. - 1023.5 ) * platescl_fix/chip_sum + sc_atty

  offsets = get_shimizu(iindex.date_obs, /xrt)  
  crval1_fix = crval1 + reform(offsets[0,*])
  crval2_fix = crval2 + reform(offsets[1,*])

  xcen_fix   = crval1_fix
  ycen_fix   = crval2_fix

  oindex.crval1 = crval1_fix
  oindex.crval1 = crval1_fix
  oindex.xcen   = xcen_fix
  oindex.ycen   = ycen_fix

  return, oindex

end

