
function get_shimizu, t, xrt=xrt, do_plot=do_plot

;+
;   Name: get_shimizu
;
;   Purpose: ssw time -> Either SOT or XRT to spacecraft boresight offsets
;			 by interpolation on Shimizu san alignment table data
;
;   Input Parameters:
;      time - desired time(s) - any ssw (string, 'index', catalogs...)
;
;   Output Parameters:
;      - [2, n] array containing ew and ns SOT or XRT offsets from
;	 SC boresight for all times passed
;
;   Keyword Parameters:
;      xrt - switch to return xrt to spacecraft boresight offsets
;	     (by default returns sot to spacecraft boresight offsets
;
;   History:
;      08-jun-2014 - GLS
;-

tsec = anytim(t)

rd_shimizu_data, pnt_str=pnt_str

; SOT BFI CCD center to spacecraft boresight:
sot2sc_ew_arr   = pnt_str.sot2sc_ew
t_sot2sc_ew_arr = pnt_str.t_sot2sc_ew
tsec_sot2sc_ew_arr = anytim(t_sot2sc_ew_arr)

sot2sc_ns_arr   = pnt_str.sot2sc_ns
t_sot2sc_ns_arr = pnt_str.t_sot2sc_ns
tsec_sot2sc_ns_arr = anytim(t_sot2sc_ns_arr)

sot2sc_ew = interpol(sot2sc_ew_arr, tsec_sot2sc_ew_arr, tsec)
sot2sc_ns = interpol(sot2sc_ns_arr, tsec_sot2sc_ns_arr, tsec)

; SOT BFI CCD center to XRT boresight:
sot2xrt_ew_arr   = pnt_str.sot2xrt_ew
t_sot2xrt_ew_arr = pnt_str.t_sot2xrt_ew
tsec_sot2xrt_ew_arr = anytim(t_sot2xrt_ew_arr)

sot2xrt_ns_arr   = pnt_str.sot2xrt_ns
t_sot2xrt_ns_arr = pnt_str.t_sot2xrt_ns
tsec_sot2xrt_ns_arr = anytim(t_sot2xrt_ns_arr)

sot2xrt_ew = interpol(sot2xrt_ew_arr, tsec_sot2xrt_ew_arr, tsec)
sot2xrt_ns = interpol(sot2xrt_ns_arr, tsec_sot2xrt_ns_arr, tsec)

; XRT boresight to spacecraft boresight:
xrt2sc_ew = sot2sc_ew - sot2xrt_ew 
xrt2sc_ns = sot2sc_ns - sot2xrt_ns 

if keyword_set(do_plot) then begin
  wdef,0,1024,512+256
  !p.multi = [0,1,2]
  if n_elements(t) eq 1 then begin
    t_plot = [t,t]
    ew_plot = [sot2sc_ew]
    ns_plot = [sot2sc_ns]
  endif else begin
    t_plot = t
    ew_plot = sot2sc_ew
    ns_plot = sot2sc_ns
  endelse

  utplot, t_sot2sc_ew_arr, sot2sc_ew_arr, /ynoz ;, psym=-2
  outplot, t_plot, ew_plot, psym=-2
  utplot, t_sot2sc_ns_arr, sot2sc_ns_arr, /ynoz ;, psym=-2
  outplot, t_plot, ns_plot, psym=-2
endif

if keyword_set(xrt) then $
  return, reform(transpose([[xrt2sc_ew],[xrt2sc_ns]])) else $
  return, reform(transpose([[sot2sc_ew],[sot2sc_ns]]))

end

