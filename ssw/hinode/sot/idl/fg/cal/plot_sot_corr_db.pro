
function plot_sot_corr_db, t0, t1, search_array=search_array, $
  plot_interp=plot_interp, plot_seg=plot_seg, $
  xdisp_interp=xdisp_interp, xdisp_close=xdisp_close, xdisp_return=xdisp_return, $
  ydisp_interp=ydisp_interp, ydisp_close=ydisp_close, ydisp_return=ydisp_return, $
  do_plot=do_plot, psym=psym, $
  loud=loud, verbose=verbose, qstop=qstop, _extra=_extra

;+
; Call: struc = plot_sot_corr_db(t0, t1)
; Returns full (vector) structure of the SOT SDO correlation database matched records
;-

  if get_logenv('SEARCH_ARRAY') eq '' then $
    set_logenv, 'SEARCH_ARRAY', $
      'naxis1>256, naxis2>256, ' + $
      'obs_type=FG*(simple), ' + $
      'wave=Ca*II*H*line || ' + $
      'wave=6302A || ' + $
      'wave=CN*bandhead*3883 || ' + $
      'wave=G*band*4305 || ' + $
      'wave=NFI*no*move || ' + $
      'wave=TF*H*I*6563*base || ' + $
      'wave=blue*cont*4504 || ' + $
      'wave=green*cont*5550 || ' + $
      'wave=red*cont*6684'

;     'naxis1=1024 && naxis2= 512 || ' + $
;     'naxis1= 512 && naxis2= 512 || ' + $
;     'naxis1= 864 && naxis2= 512, ' + $
;     'wave=Ca*II*H*line'

; search_array = str2arr(get_logenv('SEARCH_ARRAY'), ',')

  if not exist(search_array) then $
    search_array = ['naxis1=1024 && naxis2=512','wave=Ca*II*H*line']
  if not exist(do_plot) then do_plot = 1
  if not exist(psym) then psym = 3
  if not keyword_set(verbose) then verbose = 0

  quiet = 1 - verbose
  loud = verbose
  if not keyword_set(quiet_where) then quiet_where = 1

  if get_logenv('HINODE_SDO_ALIGNMENT') eq '' then begin

    if not exist(topdir_genx) then begin
      if keyword_set(do_test) then $
        topdir_genx = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_aia_align_test' else $
        topdir_genx = '$SSWDB/hinode/gen/hinode_sdo_alignment'
    endif
    if not exist(topdir_seg) then begin
      if keyword_set(do_test) then $
        topdir_seg = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_corr_segments_test' else $
        topdir_seg = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_corr_segments'
    endif

  endif else begin

    print, ' Environmental "HINODE_SDO_ALIGNMENT" is set.  Using that.'
    topdir_genx = concat_dir(get_legenv('HINODE_SDO_ALIGNMENT'), 'sot_aia_align')
    topdir_seg  = concat_dir(get_legenv('HINODE_SDO_ALIGNMENT'), 'sot_corr_segments')

  endelse

  t0_sdo_mission = '01-jul-2010'

; Read the full shimizu pointing db:
; sot_offsets = get_shimizu()

; Read the portion of the SOT SDO correlation db within t0 and t1:
  read_genxcat, t0, t1, buff, topdir=topdir_genx, count=count

  if exist(search_array) then begin
    ss_good = struct_where(buff, search_array=search_array, count_good, $
                           quiet=quiet_where)
    if count_good gt 0 then begin
       buff = buff[ss_good]
       count_corr = count_good
    endif else begin
       print, ' No matches to image filters. Returning.'
       return, -1
    endelse
  endif else begin
    ss_good = lindgen(n_elements(buff))
  endelse

  if keyword_set(do_plot) then begin
    wdef, 0, 1024, 1024
    !p.multi = [0,0,2]
    utplot, buff.date_obs, buff.xdisp_rot, ytitle='xdisp', charsize=1.5, psym=psym
    utplot, buff.date_obs, buff.ydisp_rot, ytitle='ydisp', charsize=1.5, psym=psym
  endif

  if keyword_set(qstop) then stop

  return, buff
  
end

