
pro comp_arc, t0, t1, obsdir=obsdir, $
              t_grid=t_grid, files0_arr=files0_arr, files1_arr=files1_arr, $
              num_files0_arr=num_files0_arr, num_files1_arr=num_files1_arr, $
              _extra=_extra

if not exist(days) then days = 5
  
t_grid = anytim(timegrid(t0,t1,days=days), /yoh)
n_grid = n_elements(t_grid) -1 

if not exist(obsdir) then obsdir = 'FG'

obs_dir_arr = ['FG', 'FGMG', 'FGIV', 'FGSIV200', 'FGFOCUS']
n_obs_dirs = n_elements(obs_dir_arr)

;for j=0, n_obs_dirs-1 do begin
for i=0, n_grid-1 do begin
      print, 'Now working on interval ' + t_grid[i] + ' to ' + t_grid[i+1] + '.'
      sot_cat, t_grid[i], t_grid[i+1], cat0, files0, /level0, search='OBSDIR='+obsdir
      sot_cat, t_grid[i], t_grid[i+1], cat0, files1, /level1, search='OBSDIR='+obsdir, /check_files

      n_files1 = n_elements(files1)
      t_files0 = file2time(files0)
      t_files1 = file2time(files1)
      for k=0, n_files1-1 do begin
         file1_cat = files1[k]
         t_file1 = t_files1[k]
         ss_match_time = where(t_files0 eq t_file1, n_match)
         if n_match gt 0 then begin
            file0 = files0[ss_match_time]
            filnam0 = file_break(file0)
            ss_und = strpos(filnam0,'_')
            filnam1 = strmid(filnam0, ss_und+1)
            path1 = file_break(file1_cat, /path)
            file1 = concat_dir(path1, filnam1)
            filnam1_fixed = filnam0
            file1_fixed = concat_dir(path1, filnam1_fixed)
;           STOP
            spawn, ['mv', file1, file1_fixed], /noshell
         endif
      endfor
      
      if not exist(files0_arr) then begin
         files0_arr = files0
         num_files0_arr = n_elements(files0)
         files1_arr = files1
         num_files1_arr = n_elements(files1)
      endif else begin
         files0_arr = [files0_arr, files0]
         num_files0_arr = [num_files0_arr, n_elements(files0)]
         files1_arr = [files1_arr, files1]
         num_files1_arr = [num_files1_arr, n_elements(files1)]
      endelse

      print, 'Finished with interval.'
      help, files0
      help, files1
   endfor
;endfor

end
