
function sot_fix_roll_scale, index, interp=interp, loud=loud, $
            _extra=_extra

if not exist(loud) then loud = 1

oindex = index

ss_already = get_history(index, caller='sot_fix_roll_scale', found=found)
if (not found) then begin

; For now use mission-averged corrections (to be replaced later by table interpolation)

  delta_roll = 0.05 ; degrees
  scale_fac = 1.0135

  if size(index, /type) eq 8 then $
    t_arr = index.date_obs else $
    t_arr = index

  oindex.crota1 = oindex.crota1 + delta_roll
  oindex.crota2 = oindex.crota2 + delta_roll
  oindex.cdelt1 = oindex.cdelt1 * scale_fac
  oindex.cdelt2 = oindex.cdelt2 * scale_fac

  history_rec = 'CROTA1/2 and CDELT1/2 updated.'
  update_history, oindex, history_rec, caller='sot_fix_roll_scale', version=version

endif else begin
  if (loud) then box_message, 'Roll/scale correction already applied. Skipping...'
endelse

return, oindex

end

