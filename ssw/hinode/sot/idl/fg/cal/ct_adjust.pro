
pro ct_adjust, align_struct, x0, y0, nsig_bad=nsig_bad, do_plot=do_plot, $
  _extra=_extra

if not exist(nsig_bad) then nsig_bad = 3

x0_fixed = x0
y0_fixed = y0

t_align = align_struct.date_obs
t_align_sec = anytim(t_align)
n_t_align = n_elements(t_align)

t_reset_arr = sotct_tim2reset(t_align, cmd=cmd, t_delta_min=t_delta_min, /before)
t_uniq = all_vals(t_reset_arr)
t_grid = anytim([anytim(t_uniq, /ccsds), anytim(t_align[n_t_align-1], /ccsds)], /ccsds)
t_grid_sec = anytim(t_grid)
n_t_grid = n_elements(t_grid)

if keyword_set(do_plot) then begin
  wdef,0,1024,1024
endif

for i=0, n_t_grid-2 do begin
  ss_subset = $
    where((t_align_sec gt t_grid_sec[i]) and (t_align_sec le t_grid_sec[i+1]), n_subset)
  t_subset = t_align[ss_subset]
  t_subset_sec = t_align_sec[ss_subset]
  x0_subset = x0[ss_subset]
  y0_subset = y0[ss_subset]

if n_subset gt 1 then begin

; Fix bad x0 values for this grid interval:


; First cull of x0_subset:
  coeff_x0_fit0 = poly_fit(t_subset_sec, x0_subset, 1, yfit=x0_fit)
  
  diff_fit = x0_fit - x0_subset
  diff_mom = moment(diff_fit)
  diff_mean = diff_mom[0]
  diff_var = diff_mom[1]
  diff_stddev = sqrt(diff_var)

  ss_good = where(abs(diff_fit) le diff_stddev*nsig_bad, n_good)

if n_good gt 1 then begin

  t_subset_good = t_subset[ss_good]
  t_subset_good_sec = t_align_sec[ss_good]
  x0_subset_good = x0_subset[ss_good]

; Linear fit of x0_subset:
  coeff_x0_fit_good = poly_fit(t_subset_good_sec, x0_subset_good, 1, yfit=x0_fit_good)

  diff_fit_good = x0_fit_good - x0_subset_good
  diff_mom_good = moment(diff_fit_good)
  diff_mean_good = diff_mom_good[0]
  diff_var_good = diff_mom_good[1]
  diff_stddev_good = sqrt(diff_var_good)

  ss_good_final = ss_subset[where(abs(diff_fit) le diff_stddev_good*nsig_bad, n_good_final)]
  ss_bad_final  = ss_subset[where(abs(diff_fit) gt diff_stddev_good*nsig_bad, n_bad_final) ]
  t_good_final = t_align[ss_good_final]

  if n_bad_final gt 0 then begin
    x0_interp = interpol(x0[ss_good_final], t_align_sec[ss_good_final], t_align_sec[ss_bad_final])
    for j=0, n_bad_final-1 do x0_fixed[ss_bad_final[j]] = x0_interp[j]
  endif

  if keyword_set(do_plot) then begin
;   !p.multi = [0,1,2]
;   if i eq 0 then then utplot,  t_align,  x0, psym=-4, /ystyle, /ynoz
;   utplot,  t_align,  x0, psym=-4, /ystyle, /ynoz
;   outplot, t_subset, x0_subset, psym=-1            
;   outplot, t_align, x0_fixed, psym=-2                  
    utplot, t_align, x0_fixed, psym=-2
;   !p.multi = 0
  endif

endif


; Fix bad y0 values for this grid interval:


; First fit of y0_subset:
  coeff_y0_fit0 = poly_fit(t_subset_sec, y0_subset, 1, yfit=y0_fit)
  
  diff_fit = y0_fit - y0_subset
  diff_mom = moment(diff_fit)
  diff_mean = diff_mom[0]
  diff_var = diff_mom[1]
  diff_stddev = sqrt(diff_var)

  ss_good = where(abs(diff_fit) le diff_stddev*nsig_bad, n_good)

if n_good gt 1 then begin

  t_subset_good = t_subset[ss_good]
  t_subset_good_sec = t_align_sec[ss_good]
  y0_subset_good = y0_subset[ss_good]

; Second fit of y0_subset:
  coeff_y0_fit_good = poly_fit(t_subset_good_sec, y0_subset_good, 1, yfit=y0_fit_good)

  diff_fit_good = y0_fit_good - y0_subset_good
  diff_mom_good = moment(diff_fit_good)
  diff_mean_good = diff_mom_good[0]
  diff_var_good = diff_mom_good[1]
  diff_stddev_good = sqrt(diff_var_good)

  ss_good_final = ss_subset[where(abs(diff_fit) le diff_stddev_good*nsig_bad, n_good_final)]
  ss_bad_final  = ss_subset[where(abs(diff_fit) gt diff_stddev_good*nsig_bad, n_bad_final) ]
  t_good_final = t_align[ss_good_final]

  if n_bad_final gt 0 then begin
    y0_interp = interpol(y0[ss_good_final], t_align_sec[ss_good_final], t_align_sec[ss_bad_final])
    for j=0, n_bad_final-1 do y0_fixed[ss_bad_final[j]] = y0_interp[j]
  endif

  if keyword_set(do_plot) then begin
;   !p.multi = [0,1,2]
;   if i eq 0 then then utplot,  t_align,  y0, psym=-4, /ystyle, /ynoz
;   utplot,  t_align,  y0, psym=-4, /ystyle, /ynoz
;   outplot, t_subset, y0_subset, psym=-1            
;   outplot, t_align, y0_fixed, psym=-2                  
    utplot, t_align, y0_fixed, psym=-2                  
    if keyword_set(pause) then STOP
;   !p.multi = 0
  endif

endif

endif

endfor

if keyword_set(do_plot) then begin
  wdef,0,1024,1024
  !p.multi = [0,1,2]
  utplot, align_struct.date_obs, x0_fixed, psym=4, /ystyle             
  utplot, align_struct.date_obs, y0_fixed, psym=4, /ystyle             
endif

x0 = x0_fixed
y0 = y0_fixed

end  

