
function build_str_arr, files, do_plot=do_plot

; NOTE - Keyword PERCENTD not always defined.

tag_list = 'naxis1, naxis2, date_obs, ctime, data_lev, orig_rf0, ver_rf0, ' + $
           'saa, hlz, waveid, obs_type, xscale, yscale, fgxoff, fgyoff, ' + $
           'fgccdix0, fgccdix1, fgccdiy0, fgccdiy1, crpix1, crpix2, ' + $
           'sc_attx, sc_atty, crval1, crval2, cdelt1, cdelt2, sat_rot, ' + $
           'inst_rot, crota1, crota2, xcen, ycen, fovx, fovy, fgbinx, fgbiny, ' + $
           'exptime, wave, darkflag, fgmode, fgnint, ctserv, ctmex, ctmey, ' + $
           'ctmode, t_spccd, t_fgccd, t_ctccd, focus, ' + $
           'xdisp_rot, ydisp_rot, peak_str, file_sot, file_sdo'

if not exist(files) then $
  files = file_list('/net/castor/Users/slater/data/sot_aia_align/','*.genx')
if not exist(outdir_plots) then $
  outdir_plots = '/sanhome/slater/public_html/missions/hinode/alignment'
nfiles = n_elements(files)

for i=0,nfiles-1 do begin

  rd_genx, files[i], buff
  align_str = str_subset(buff.savegen0, tag_list)

  if not exist(align_str_arr) then align_str_arr = align_str else $
    align_str_arr = concat_struct(align_str_arr, align_str)
  delvarx, buff, align_str

endfor

if keyword_set(do_plot) then begin
  wdef,0,1024,1024
  !p.multi = [0,1,3]
  utplot, align_str_arr.date_obs, align_str_arr.xdisp_rot, psym=3, $
    ytit='x offset (arcsec)', chars=3, yrange=[-20,20], /ystyle
  utplot, align_str_arr.date_obs, align_str_arr.ydisp_rot, psym=3, $
    ytit='y offset (arcsec)', chars=3, yrange=[-20,20], /ystyle
  utplot, align_str_arr.date_obs, align_str_arr.waveid,    psym=4, $
    ytit='waveid', chars=3

  buff = 255b - tvrd()
  tv, buff
  write_gif, concat_dir(outdir_plots, 'correlation_test_run_20140215_20140218.gif'), buff
endif

return, align_str_arr

end

