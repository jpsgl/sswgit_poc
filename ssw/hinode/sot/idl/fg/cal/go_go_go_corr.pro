
; go_go_go_corr, check_submit=check_submit, do_debug=do_debug

if not exist(max_tries) then max_tries = 2

pool_dir  = '/sanhome/slater/logs/time_pools/sot/sot_corr'
pool_file = concat_dir(pool_dir, 'time_pool_sot_corr.txt')
if not exist(dir_jobs) then dir_jobs = '/sanhome/slater/jobs/go_go_go_corr'
if not exist(dir_logs) then dir_logs = '/sanhome/slater/logs/go_go_go_corr'

; Read time pool file:
pool_buff = rd_tfile(pool_file)
n_rec = n_elements(pool_buff)
pool_index = lindgen(n_rec)
pool_rec_arr = rd_tfile(pool_file, 2)
t_arr = pool_rec_arr[0,*]
status_arr = pool_rec_arr[1,*]
ss_good = where(fix(status_arr) le max_tries, n_good)
t_good = t_arr[ss_good]
status_good = status_arr[ss_good]
t_sec_good = anytim(t_good)
min_t_sec_good = min(t_sec_good, ss_min)
t0 = t_good[ss_min]
status0 = status_good[ss_min]

t1 = reltime(t0, days=1)

pool_index_this_run = ss_good[ss_min]
pool_rec0 = pool_buff[pool_index_this_run]
ss_undone = where(pool_index ne pool_index_this_run, n_undone)
pool_buff = pool_buff[ss_undone]

if keyword_set(do_debug) then $
   STOP, 'Stopping in go_go_go_corr before re-write of time pool file.'

; Write out new time pool:
file_append, pool_file, pool_buff, /new

if get_logenv('SEARCH_ARRAY') eq '' then $
   set_logenv, 'SEARCH_ARRAY', $
      'naxis1>256,naxis2>256,' + $
      'obs_type=FG*(simple),' + $
      'wave=Ca*II*H*line || ' + $
      'wave=CN*bandhead*3883 || ' + $
      'wave=G*band*4305 || ' + $
      'wave=NFI*no*move || ' + $
      'wave=TF*H*I*6563*base || ' + $
      'wave=blue*cont*4504 || ' + $
      'wave=green*cont*5550 || ' + $
      'wave=red*cont*6684'

;   set_logenv, 'SEARCH_ARRAY', $
;     'naxis1>256,naxis2>256,' + $
;     'obs_type=FG*(simple),' + $
;     'wave=green*cont*5550'

;   set_logenv, 'SEARCH_ARRAY', $
;     'naxis1>256, naxis2>256, ' + $
;     'obs_type=FG*(simple), ' + $
;     'wave=Ca*II*H*line || ' + $
;     'wave=6302A || ' + $
;     'wave=CN*bandhead*3883 || ' + $
;     'wave=G*band*4305 || ' + $
;     'wave=NFI*no*move || ' + $
;     'wave=TF*H*I*6563*base || ' + $
;     'wave=blue*cont*4504 || ' + $
;     'wave=green*cont*5550 || ' + $
;     'wave=red*cont*6684'

search_array = str2arr(get_logenv('SEARCH_ARRAY'), ',')

if keyword_set(do_debug) then $
   STOP, 'Stopping in go_go_go_corr before call to go_go_corr.'

go_go_corr, t0, t1, search_array=search_array, /sot_noscale, /sdo_noscale, /_extra

catch, Error_status
if error_status eq 0 then $
   status0 = strtrim(10,2) else $
   status0 = strtrim(fix(status0) + 1,2)
pool_rec0 = t0 + '  ' + status0
catch, /cancel

help, pool_rec0

; Append updated record for this time interval to time pool file:
file_append, pool_file, pool_rec0

; Now start another instance of this driver, and exit:

procedure_name = 'go_go_go_corr'
job_time = anytim(!stime, /ccsds)
;job_name = 'go_'+ procedure_name + '_' + time2file(job_time) + '.pro'
;if exist(dir_jobs) then job_name = concat_dir(dir_jobs, job_name)
job_name = procedure_name
;log_name = 'go_'+ procedure_name + '_' + time2file(job_time) + '.log'
log_name = procedure_name + '_' + time2file(job_time) + '.log'
if exist(dir_logs) then log_name = concat_dir(dir_logs, log_name)

sswbatch = get_logenv('$ssw_batch') ; optional local environmental?
case 1 of
   file_exist(sswbatch): batch=sswbatch ; $ssw_batch defined - use that
   else: batch = '/ssw/site/bin/ssw_batch'
endcase
box_message, 'Using batch script> ' + batch

cmd = batch + ' ' + job_name + ' ' + log_name + ' &'
box_message, cmd

if keyword_set(do_debug) then $
   STOP, 'Stopping in go_go_go_corr before spawning batch job.'

if keyword_set(check_submit) then $
   box_message,'check_submit, not really submitting' else $
   spawn, cmd

end

