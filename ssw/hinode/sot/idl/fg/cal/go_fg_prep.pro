
pro go_fg_prep, t0, t1, hours_grid=hours_grid, xrt=xrt, level0=level0, $
  search_array=search_array, percentd_min=percentd_min, $
  cat_sot=cat_sot, files_sot=files_sot, refresh_cat=refresh_cat, $
  every_nth=every_nth, n_max_files=n_max_files, $
  do_delete_old_logs=do_delete_old_logs, do_write=do_write, $
  continue=continue, verbose=verbose, _extra=_extra

;+
; name: go_fg_prep
; call: go_fg_prep, t0, t1
; -

if not exist(level0) and not keyword_set(quicklook) then level0 = 1
if not exist(hours_grid) then hours_grid = 1.0
if not exist(dir_top) then dir_top = '/net/topaz/Users/slater/data/fg_level1'
if not exist(percentd_min) then percentd_min = 100

;if not exist(search_array) then $
;  search_array = ['naxis1=2048','naxis2=1024','waveid=23']

if keyword_set(do_delete_old_logs) then $
  spawn, 'rm ' + concat_dir(dir_top, 'logs') + '/*'
; spawn, ['rm', '/Users/slater/data/fg_level1/logs/*'], /noshell

t_grid = anytim(timegrid(t0, t1, hours=hours_grid), /ccsds)
n_grid = n_elements(t_grid)-1

  for i=0, n_grid-1 do begin

    sot_cat, t_grid[i], t_grid[i+1], cat_sot, files_sot, xrt=keyword_set(xrt), $
             search_array=search_array, level0=level0, quicklook=quicklook, $
             count=count, tcount=tcount

if count gt 0 then begin
  ss_fg = where(strlowcase(strmid(cat_sot.obs_type,0,2)) eq 'fg', count_fg)

    if count_fg gt 0 then begin

      ; Filter out non-existent files:
      ss_exist = where(file_exist(files_sot) eq 1, n_exist)

      if n_exist gt 0 then begin

        files_sot = files_sot[ss_exist]
        cat_sot = cat_sot[ss_exist]
        n_files = n_exist

        ; Optionally only process every 'nth' file:
        if keyword_set(every_nth) then begin
          ss_nth = indgen(ceil(float(n_files)/every_nth)) * every_nth
          files_sot = files_sot[ss_nth]
          cat_sot = cat_sot[ss_nth]
          n_files = n_elements(ss_nth)
        endif

        for j=0, n_files-1 do begin
          file_sot = files_sot[j]
          delvarx, index_sot, data_sot
          read_sot, file_sot, index_sot, data_sot

          ; If read_sot failed, try once more:
          if not exist(index_sot) then begin
            print, 'read_sot failed.  Waiting 100 seconds and trying again.'
            wait, 100
            read_sot, files_sot, index_sot, data_sot
          endif

          if exist(index_sot) then begin

            if tag_exist(index_sot, 'percentd') then begin
              if index_sot.percentd ge percentd_min then begin

;n_index = n_elements(index_sot)
;size_data = size(data_sot)
;if size_data[0] eq 4 then begin
;  if size_data[4] eq n_index then begin
;    print, ' Num data arrays (fourth dim) equal to n_index.  Calling FG_PREP.'

                fg_prep, index_sot, data_sot, $
                  outflatfits=do_write, outdir=concat_dir(dir_top, 'prepped'), $
                  _extra=_extra

;  endif else begin
;    stop, ' Number of data arrays (fourth dim) not equal to n_index.  Stopping.'
;  endelse
;endif else begin
;  if keyword_set(verbose) then $
;    print, ' Not a 4-D data array.  Skipping.'
;endelse

              endif
            endif

          endif else begin
            print, ' read_sot failed a second time.  Skipping this file.'
          endelse

        endfor

      endif

    endif

endif

    print, ' Finished for '+ anytim(t_grid[i],   /yoh) + $
                             anytim(t_grid[i+1], /yoh)

  endfor

end

