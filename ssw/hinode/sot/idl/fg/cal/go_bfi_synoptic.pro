
pro go_bfi_synoptic, sttim, entim, dir_cat=dir_cat, t_truncate=t_truncate, $
  days_chunk=days_chunk, do_save=do_save, do_plot=do_plot, do_write=do_write, $
  _extra=_extra

if not exist(t_win_sot_minutes) then t_win_sot_minutes = 2
if not exist(n_synop_redo) then n_synop_redo = 1

wave_name_arr = ['gband','blue','red','green','ca','cn']
wave_tname_arr = $
  ['g*band*4305','blue*cont*4504','red*cont*6684','green*cont*5550','ca*ii*h*line','cn*bandhead*3883']
waveid_search_arr = 'waveid=' + ['3','4','6','5','2','1']

synop_recs = rd_tfile('$HOME/soft/idl/idl_startup/sot/sot_synop.lis')

; Remove bad records:
t_synop = strmid(synop_recs,64,19)
ss_good_t = where( ((strmid(t_synop, 0,2) eq '20') and $
                    (strmid(t_synop, 4,1) eq '/' ) and $
                    (strmid(t_synop, 7,1) eq '/' ) and $
                    (strmid(t_synop,10,1) eq '.' ) and $
                    (strmid(t_synop,13,1) eq ':' ) and $
                    (strmid(t_synop,16,1) eq ':' )), n_good_t)
synop_recs = synop_recs[ss_good_t]
t_synop = t_synop[ss_good_t]

; Sort by tim file name:
;filnam = strmid(synop_recs,0,26)
;ss_sort_filnam = sort(filnam)
;synop_recs = synop_recs[ss_sort_filnam]
;t_synop = t_synop[ss_sort_filnam]

; Extract only final version of each time file:
;filnam_no_vernum = strmid(synop_recs,0,12)
;ss_uniq_fn = uniq(filnam_no_vernum)
;synop_recs = synop_recs[ss_uniq_fn]
;t_synop = t_synop[ss_uniq_fn]

; Sort times:
ss_sort_time = sort(t_synop)
synop_recs = synop_recs[ss_sort_time]
t_synop = t_synop[ss_sort_time]

; Remove duplicate records:
ss_uniq_time = uniq(t_synop)
t_synop = t_synop[ss_uniq_time]
synop_recs = synop_recs[ss_uniq_time]

; Once again define the times using only the sorted, good synop records: 
filnam_tim = strmid(synop_recs,0,26)
t_synop = strmid(synop_recs,64,19)
t_synop = str_replace(t_synop,'.',' ')
t_synop_sec = anytim(t_synop)

; Restore last saved 'bfi_response_mission.sav' file, if it exists:
filnam_bfi_response = '$HOME/soft/idl/idl_startup/sot/bfi_response_mission.sav'
if file_exist(filnam_bfi_response) ne 0 then $
  restore, filnam_bfi_response

;if exist(t_truncate) then begin
;  t_truncate_sec = anytim(t_truncate)
;  t_arr_sec = anytim(t_arr)
;  ss_good_t_arr = where(t_arr_sec le t_truncate_sec, n_good_t_arr)
;  t_arr = t_arr[ss_good_t_arr]
;endif

if ( exist(bfi_response_struct) and exist(t_synop_sec_last) ) then begin
  ss_synop_new = where(t_synop_sec gt t_synop_sec_last, n_synop_new)
  if n_synop_new gt 0 then begin
    t_synop_new = t_synop[[ss_synop_new[0]-reverse(lindgen(n_synop_redo)+1), ss_synop_new]]
  endif else begin
    if keyword_set(do_plot) then $
      bfi_response_struct_read = sot_time2bfi_response(do_write=do_write)
    print, ' No new synoptics.  Returning.'
    return
  endelse  
endif else begin
  ss_synop_new = 0
  t_synop_new = t_synop
endelse

bfi_synoptic, t_synop_new, t_win_sot_minutes=t_win_sot_minutes, level0=level0, $
              dir_cat=dir_cat, $
              bfi_response_struct=bfi_response_struct, t_synop_sec_last=t_synop_sec_last, $
              days_chunk=days_chunk, do_prep=do_prep, $
              do_save=do_save, quiet=quiet, _extra=_extra

if keyword_set(do_save) then $
  save, bfi_response_struct, t_synop_sec_last, file=filnam_bfi_response

if keyword_set(do_plot) then $
  bfi_response_struct_read = sot_time2bfi_response(do_write=do_write)

end

