
pro mk_bfi_response_struct, db_dir=db_dir

if not exist(db_dir) then $
  db_dir = '$HOME/Users/slater/soft/idl/idl_startup/sot'
file_gband = concat_dir(db_dir, 'bfi_synoptic_results_waveid_3_20140321_1336.sav')
file_blue  = concat_dir(db_dir, 'bfi_synoptic_results_waveid_4_20140321_1249.sav')
file_red   = concat_dir(db_dir, 'bfi_synoptic_results_waveid_6_20140321_1409.sav')
file_green = concat_dir(db_dir, 'bfi_synoptic_results_waveid_5_20140321_1431.sav')
file_ca    = concat_dir(db_dir, 'bfi_synoptic_results_waveid_2_20140321_1456.sav')
file_cn    = concat_dir(db_dir, 'bfi_synoptic_results_waveid_1_20140321_1519.sav')

restore, file_gband
t_arr_gband = t_arr
bfi_full_ccd_mean_gband = bfi_full_ccd_mean
bfi_left_half_ccd_mean_gband = bfi_left_half_ccd_mean
bfi_right_half_ccd_mean_gband = bfi_right_half_ccd_mean

restore, file_blue
t_arr_blue = t_arr
bfi_full_ccd_mean_blue = bfi_full_ccd_mean
bfi_left_half_ccd_mean_blue = bfi_left_half_ccd_mean
bfi_right_half_ccd_mean_blue = bfi_right_half_ccd_mean

restore, file_red
t_arr_red = t_arr
bfi_full_ccd_mean_red = bfi_full_ccd_mean
bfi_left_half_ccd_mean_red = bfi_left_half_ccd_mean
bfi_right_half_ccd_mean_red = bfi_right_half_ccd_mean

restore, file_green
t_arr_green = t_arr
bfi_full_ccd_mean_green = bfi_full_ccd_mean
bfi_left_half_ccd_mean_green = bfi_left_half_ccd_mean
bfi_right_half_ccd_mean_green = bfi_right_half_ccd_mean

restore, file_ca
t_arr_ca = t_arr
bfi_full_ccd_mean_ca = bfi_full_ccd_mean
bfi_left_half_ccd_mean_ca = bfi_left_half_ccd_mean
bfi_right_half_ccd_mean_ca = bfi_right_half_ccd_mean

restore, file_cn
t_arr_cn = t_arr
bfi_full_ccd_mean_cn = bfi_full_ccd_mean
bfi_left_half_ccd_mean_cn = bfi_left_half_ccd_mean
bfi_right_half_ccd_mean_cn = bfi_right_half_ccd_mean

bfi_response_struct = {t_arr_gband:t_arr_gband}
bfi_response_struct = add_tag(bfi_response_struct, bfi_full_ccd_mean_gband, 'bfi_full_ccd_mean_gband')
bfi_response_struct = add_tag(bfi_response_struct, bfi_left_half_ccd_mean_gband, 'bfi_left_half_ccd_mean_gband')
bfi_response_struct = add_tag(bfi_response_struct, bfi_right_half_ccd_mean_gband, 'bfi_right_half_ccd_mean_gband')

bfi_response_struct = add_tag(bfi_response_struct, t_arr_blue, 't_arr_blue')
bfi_response_struct = add_tag(bfi_response_struct, bfi_full_ccd_mean_blue, 'bfi_full_ccd_mean_blue')
bfi_response_struct = add_tag(bfi_response_struct, bfi_left_half_ccd_mean_blue, 'bfi_left_half_ccd_mean_blue')
bfi_response_struct = add_tag(bfi_response_struct, bfi_right_half_ccd_mean_blue, 'bfi_right_half_ccd_mean_blue')

bfi_response_struct = add_tag(bfi_response_struct, t_arr_red, 't_arr_red')
bfi_response_struct = add_tag(bfi_response_struct, bfi_full_ccd_mean_red, 'bfi_full_ccd_mean_red')
bfi_response_struct = add_tag(bfi_response_struct, bfi_left_half_ccd_mean_red, 'bfi_left_half_ccd_mean_red')
bfi_response_struct = add_tag(bfi_response_struct, bfi_right_half_ccd_mean_red, 'bfi_right_half_ccd_mean_red')

bfi_response_struct = add_tag(bfi_response_struct, t_arr_green, 't_arr_green')
bfi_response_struct = add_tag(bfi_response_struct, bfi_full_ccd_mean_green, 'bfi_full_ccd_mean_green')
bfi_response_struct = add_tag(bfi_response_struct, bfi_left_half_ccd_mean_green, 'bfi_left_half_ccd_mean_green')
bfi_response_struct = add_tag(bfi_response_struct, bfi_right_half_ccd_mean_green, 'bfi_right_half_ccd_mean_green')

bfi_response_struct = add_tag(bfi_response_struct, t_arr_ca, 't_arr_ca')
bfi_response_struct = add_tag(bfi_response_struct, bfi_full_ccd_mean_ca, 'bfi_full_ccd_mean_ca')
bfi_response_struct = add_tag(bfi_response_struct, bfi_left_half_ccd_mean_ca, 'bfi_left_half_ccd_mean_ca')
bfi_response_struct = add_tag(bfi_response_struct, bfi_right_half_ccd_mean_ca, 'bfi_right_half_ccd_mean_ca')

bfi_response_struct = add_tag(bfi_response_struct, t_arr_cn, 't_arr_cn')
bfi_response_struct = add_tag(bfi_response_struct, bfi_full_ccd_mean_cn, 'bfi_full_ccd_mean_cn')
bfi_response_struct = add_tag(bfi_response_struct, bfi_left_half_ccd_mean_cn, 'bfi_left_half_ccd_mean_cn')
bfi_response_struct = add_tag(bfi_response_struct, bfi_right_half_ccd_mean_cn, 'bfi_right_half_ccd_mean_cn')

;help, bfi_response_struct

save, bfi_response_struct, file=concat_dir(db_dir, 'bfi_response_mission.sav')

end

