
function last_flat, bfi=bfi

if not exist(get_time) then get_time = 1

t_flat = flat_stats(wave, bfi=bfi, side=side, bin=bin, $
  flat_files=flat_files, get_time=get_time, t_flat=t_flat, help=help)

n_flat = n_elements(t_flat)
t_flat = t_flat[n_flat-1]

return, t_flat

end
