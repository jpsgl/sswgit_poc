
function bigass, t0, t1, above=above, no_print=no_print, nmax_flares=nmax_flares

;+
; Name:
;    BIGASS
; History:
;    GLS - 04-Mar-2016 - Transcribed from lines of code provided by SLF
;-

if not exist(above) then above = 'M5.1'
  
gev = get_gev(t0, t1)
decode_gev, gev, above=above, fstart, fend, fpeak, class=class
order = reverse(sort(class))
flist = fpeak[order] + '   ' + class[order] 
n_flares = n_elements(flist)

if exist(nmax_flares) then n_flares = min([n_flares, nmax_flares])

if not keyword_set(no_print) then begin
   print, ''
   print, 'Flare Peak Time (UT)  Peak'
   print, '                      GOES'
   print, '====================  ===='
   for i=0, n_flares-1 do print, flist[i]
   print, ''
endif

return, flist

end
