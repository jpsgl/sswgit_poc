
function wcs_pix2pix_2, pix_arr1, index1, index2, $
  wcs1=wcs1, wcs2=wcs2

wcs1 = fitshead2wcs(index1)
wcs2 = fitshead2wcs(index2)

coord1 = wcs_get_coord(wcs1, pix_arr1)
wcs_convert_from_coord, wcs1, coord1, 'hg', hgln, hglt
wcs_convert_diff_rot, wcs1, wcs2, hgln, hglt
wcs_convert_to_coord, wcs2, coord2, 'hg', hgln, hglt
pix_arr2 = wcs_get_pixel(wcs2, coord2)

return, pix_arr2

end
