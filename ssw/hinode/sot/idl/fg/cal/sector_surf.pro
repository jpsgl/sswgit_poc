
pro sector_surf, wave_nam, t_nam_prefix=t_nam_prefix, nsig_max=nsig_max, $
  infil_prefix=infil_prefix, outfil_gif=outfil_gif, index_arr=index_arr, do_write=do_write, qstop=qstop

if not exist(db_dir) then db_dir = '/net/castor/Users/slater/soft/idl/idl_startup'
if not exist(infil_prefix) then infil_prefix = 'fg_throughput_mom_'
if not exist(t_nam_prefix) then t_nam_prefix = 'fg_med_sector_'
if not exist(interval_grid) then interval_grid = 24	; Hours
if not exist(nsig_max) then nsig_max = 3

wave_name_arr = ['gband','blue','red','green','ca','cn']
wave_label_arr = ['gband','blue ','red  ','green','ca   ','cn   ']
wavelength_arr = ['4305 A','4504 A','6684 A','5550 A','3968 A','3883 A']
n_waves = n_elements(wave_name_arr)
lo_lim_arr = [0.900e4, 0.650e4, 0.950e4, 0.645e4, 0.08e4, 0.50e4]
hi_lim_arr = [1.200e4, 0.800e4, 1.030e4, 0.710e4, 0.14e4, 1.00e4]
ymin_arr = [0.80, 0.89, 0.95, 0.90, 0.90, 0.90]
ymax_arr = [1.01, 1.01, 1.01, 1.01, 1.01, 1.01]
psym_arr = [2, 1, 5, 4, 6, 7]
color_arr = [13, 10, 2, 8, 5, 12]

    if not exist(infil_save) then infil_save = infil_prefix + strtrim(wave_nam,2) + '.sav'
    infil_save_full = concat_dir(db_dir, infil_save)

    ss_match = where(wave_name_arr eq strlowcase(strtrim(wave_nam,2)), count_match)
    if count_match eq 1 then begin
      if ss_match le (n_elements(lo_lim_arr)-1) then begin
	lo_lim_val = lo_lim_arr[ss_match[0]]
	hi_lim_val = hi_lim_arr[ss_match[0]]
	yrange = [ymin_arr[ss_match[0]], ymax_arr[ss_match[0]]]
      endif
    endif else begin
      stop, ' Invalid wave name passed.  Stopping.'
    endelse

    if (file_exist(infil_save_full)) eq 1 then restore, infil_save_full

    ss_good_t_fgccd = where( (index_arr.t_fgccd ge -45) and (index_arr.t_fgccd le -41), n_good )
    index_arr = index_arr[ss_good_t_fgccd]

    t_nam_arr = strlowcase(tag_names(index_arr))
    pos_arr = strpos(t_nam_arr, t_nam_prefix)
    ss_tag_match = where(pos_arr ne -1, n_tag_match)

    t_arr = index_arr.date_obs
    n_tim = n_elements(t_arr)
    t_sec = anytim(t_arr)
    t_sec_min = min(t_sec, max=t_sec_max)
    t_min = anytim(t_sec_min, /yoh)
    t_max = anytim(t_sec_max, /yoh)
    t_grid = anytim(timegrid(t_min, t_max, hours=interval_grid), /yoh)
    np_grid = n_elements(t_grid)
    value_cube = fltarr(np_grid, n_tag_match)

      for j=0,n_tag_match-1 do begin

        value_arr = index_arr.(ss_tag_match[j])
        ss_good_value = where( ((value_arr ge lo_lim_val) and (value_arr le hi_lim_val)), n_good)
        t_arr0 = t_arr[ss_good_value]
        value_arr = value_arr[ss_good_value]

        mean_arr = fltarr(n_good)
        std_arr = fltarr(n_good)
        sig_arr = fltarr(n_good)

        for k=0,n_good-1 do begin
          sub_arr = value_arr[(k-50)>0:(k+49)<(n_good-1)]
          mean_arr[k] = mean(sub_arr)
          std_arr[k] = stddev(sub_arr)
          sig_arr[k] = abs(value_arr[k] - mean_arr[k]) / std_arr[k]
        endfor

        ss_valid = where(sig_arr le nsig_max)
        t_arr0 = t_arr0[ss_valid]
        value_arr = value_arr[ss_valid]

        ss_close = tim2dset(anytim(t_arr0,/ints), anytim(t_grid,/ints))
        value_close = value_arr[ss_close]
        value_cube[0,j] = value_close

      endfor

    psym = 2
    view_title = 'Median Count Rate History Versus CCD Position For BFI Filter ' + strtrim(wavelength_arr[ss_match[0]],2)
    xtitle = 'Gridded Mission Time (' + strtrim(t_min,2) + ' to ' + strtrim(t_max,2) + ')'
    ytitle = 'CCD Sector Number'
    ztitle = 'Median Count Rate'

    isurface, value_cube, zrange=[lo_lim_val, hi_lim_val], psym=psym, $
      view_title=view_title,  xtitle=xtitle,  ytitle=ytitle,  ztitle=ztitle

end




