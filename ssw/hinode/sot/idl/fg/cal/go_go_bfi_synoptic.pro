
;pro go_go_bfi_synoptic

dir_cat = '/archive1/hinode/sot/metadata/sswdb/hinode/sot/sot_genxcat'
level0 = 1

;go_bfi_synoptic, '01-oct-2006', '20-mar-2014', wave_nam='gband', /do_prep, /do_save
;go_bfi_synoptic, '01-oct-2006', '20-mar-2014', wave_nam='blue',  /do_prep, /do_save
;go_bfi_synoptic, '01-oct-2006', '20-mar-2014', wave_nam='red',   /do_prep, /do_save
;go_bfi_synoptic, '01-oct-2006', '20-mar-2014', wave_nam='green', /do_prep, /do_save
;go_bfi_synoptic, '01-oct-2006', '20-mar-2014', wave_nam='ca',    /do_prep, /do_save
;go_bfi_synoptic, '01-oct-2006', '20-mar-2014', wave_nam='cn',    /do_prep, /do_save

;go_bfi_synoptic, t_trun=anytim(anytim(!stime)-25*86400l, /ccsds), /do_prep, /do_save, /do_plot
go_bfi_synoptic, /do_prep, /do_save, /do_plot, /do_write, dir_cat=dir_cat, level0=level0

end
