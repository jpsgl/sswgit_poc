
pro go_go_corr, t0, t1, file_cases=file_cases, ss_start=ss_start, search_array=search_array, $
  interactive=interactive, do_reg=do_reg, do_xstep=do_xstep, $
  only_list=only_list, _extra=_extra

if not exist(dir_top) then begin
  if get_logenv('SOT_CORR_DB') eq '' then $
    set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), 'hinode_sdo_alignment')
  mk_dir, concat_dir(get_logenv('SOT_CORR_DB'), 'database')
  dir_top = get_logenv('SOT_CORR_DB')
endif

; Create list of all fg_start times:
if not exist(dir_sot_event_lists) then $
  dir_sot_event_lists = concat_dir(dir_top, 'database')
filnam_fg_start = concat_dir(dir_sot_event_lists, 'start_fg.lis')
filnam_fg_stop  = concat_dir(dir_sot_event_lists, 'stop_fg.lis')

buff_fg_start_no_parse = rd_tfile(filnam_fg_start)
buff_fg_start = rd_tfile(filnam_fg_start, 13, delim=' ')

t_fg_start = str_replace(reform(buff_fg_start[1,*]), '.', ' ')
ss_valid_time = where( (strmid(t_fg_start, 0,2) eq '20') and $
                       (strmid(t_fg_start, 4,1) eq  '/') and $
                       (strmid(t_fg_start, 7,1) eq  '/') and $
                       (strmid(t_fg_start,10,1) eq  ' ') and $
                       (strmid(t_fg_start,13,1) eq  ':') and $
                       (strmid(t_fg_start,16,1) eq  ':')       )
buff_fg_start_no_parse = buff_fg_start_no_parse[ss_valid_time]
buff_fg_start = buff_fg_start[*, ss_valid_time]
t_fg_start = t_fg_start[ss_valid_time]
t_fg_start_sec = anytim(t_fg_start)
ss_uniq_sort_fg_start = uniq(t_fg_start_sec, sort(t_fg_start_sec))

buff_fg_start_no_parse = buff_fg_start_no_parse[ss_uniq_sort_fg_start]
buff_fg_start = buff_fg_start[*, ss_uniq_sort_fg_start]
t_fg_start = t_fg_start[ss_uniq_sort_fg_start]
t_fg_start_sec = t_fg_start_sec[ss_uniq_sort_fg_start]
t_fg_start_int = anytim(t_fg_start, /int)
n_t_fg_start = n_elements(t_fg_start)
t_fg_start_sec_delt = t_fg_start_sec[1:*] - t_fg_start_sec

t_start = t_fg_start
t_start_sec = t_fg_start_sec
t_start_int = anytim(t_start, /int)
n_t_start = n_elements(t_start)
t_start_sec_delt = t_start_sec[1:*] - t_start_sec
t_start_25_sec = t_start_sec + 25

t_diff_start_sec = t0 - t_start_25_sec
max_pos_diff = max(where(t_diff_start_sec gt 0), ss_max)
t0_start = anytim(t_start[ss_max], /ccsds)
print, 'LATEST PRIOR FG_START: ' + anytim(t0_start,  /ccsds)
print, 'Requested start time:  ' + anytim(t0, /ccsds)

go_corr, t0, t1, level0=1, search_array=search_array, interactive=interactive, $
  refresh_cat=refresh_cat, $
  do_reg=do_reg, do_xstep=do_xstep, verbose=verbose, every_nth=every_nth, $
  lil_reg_cube=lil_reg_cube, add_waves=add_waves, add_map_waves=add_map_waves, $
  _extra=_extra

print, 'Finished for time interval from ' + anytim(t0, /yoh) + ' to ' + anytim(t1, /yoh)

; Error handling:
catch, Error_status
catch, /cancel

end

