
pro mk_sot_event_lists

if not exist(dir_timeline) then $
  dir_timeline = '/archive1/hinode/sot/ops/timeline'
if not exist(dir_sot_event_lists) then $
  dir_sot_event_lists = '$HOME/soft/idl/idl_startup/sot'

; grep "START FG" /archive1/hinode/sot/ops/timeline/monthly_file_20??_??.tim > start_fg.lis
; grep "STOP FG"  /archive1/hinode/sot/ops/timeline/monthly_file_20??_??.tim > stop_fg.lis

cmd = 'grep "START FG" ' + dir_timeline + '/monthly_file_20??_??.tim > ' + $
      dir_sot_event_lists + '/start_fg.lis'
spawn, cmd

cmd = 'grep "STOP FG" '  + dir_timeline + '/monthly_file_20??_??.tim > ' + $
      dir_sot_event_lists + '/stop_fg.lis'
spawn, cmd

cmd = 'grep "Synoptic" ' + concat_dir(dir_sot_event_lists, 'start_fg.lis') + ' > ' + $
      concat_dir(dir_sot_event_lists, 'sot_synop.lis')
spawn, cmd

end
