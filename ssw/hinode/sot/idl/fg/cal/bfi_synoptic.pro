
pro bfi_synoptic, t_synop, t_win_sot_minutes=t_win_sot_minutes, level0=level0, $
  dir_cat=dir_cat, $
  bfi_response_struct=bfi_response_struct, t_synop_sec_last=t_synop_sec_last, $
  days_chunk=days_chunk, do_prep=do_prep, $
  do_save=do_save, quiet=quiet, _extra=_extra

if not exist(t_win_sot_minutes) then t_win_sot_minutes = 3d0
if not exist(level0) then level0 = 1

;xll_samp = [1024-128, 1024+64]
;xur_samp = xll_samp + 64 - 1
 xll_samp = [1024- 64, 1024+ 8]
 xur_samp = xll_samp + 56 - 1

 yll_samp = [64, 64]
 yur_samp = yll_samp + 1024-128-1
n_sample_regions = n_elements(xll_samp)

if keyword_set(plot_sample_regions) then begin
  wdef,0,1024
  plot,[0,4095,4095,0,0],[0,0,4095,4095,0],xrange=[0,4096],yrange=[0,4096]
  for i=0,n_sample_regions-1 do $
    oplot,[xll_samp[i],xur_[i],xur_[i],xll_[i],xll_[i]],[yll_[i],yll_[i],yur_[i],yur_[i],yll_[i]]
endif

wave_name_arr = ['gband','blue','red','green','ca','cn']
wave_tname_arr = $
  ['g*band*4305','blue*cont*4504','red*cont*6684','green*cont*5550','ca*ii*h*line','cn*bandhead*3883']
waveid_arr = [3, 4, 6, 5, 2, 1]

if not exist(search_array) then begin
  if not exist(naxis_search_str) then naxis_search_str = 'naxis=2'
  if not exist(naxis1_search_str) then naxis1_search_str = 'naxis1=4096 || naxis1=2048'
  if not exist(naxis2_search_str) then naxis2_search_str = 'naxis2=2048 || naxis2=1024'
; if not exist(naxis12_search_str) then $
;   naxis12_search_str = '(naxis1=2048 && naxis2=1024) || (naxis1=4096 && naxis2=2048)'
  if not exist(obs_type_search_str) then obs_type_search_str = 'obs_type=FG*(simple)'
  if not exist(xcen_search_str) then xcen_search_str = 'xcen=-30~30'
  if not exist(ycen_search_str) then ycen_search_str = 'ycen=-30~30'
; TODO: Translate following into a proper search string:
; if not exist(t_fgccd_search_str) then t_fgccd_search_str = 't_fgccd ge -45 and t_fgccd le -41'
   search_array = [naxis_search_str, naxis1_search_str, naxis2_search_str, $
                   obs_type_search_str, xcen_search_str, ycen_search_str]
endif

if exist(bfi_response_struct) then begin
  t_arr                         = bfi_response_struct.t_arr
  img_wave_arr                  = bfi_response_struct.img_wave_arr
  img_full_ccd_mean_arr         = bfi_response_struct.img_full_ccd_mean_arr
  img_left_half_ccd_mean_arr    = bfi_response_struct.img_left_half_ccd_mean_arr
  img_right_half_ccd_mean_arr   = bfi_response_struct.img_left_half_ccd_mean_arr
  img_full_ccd_median_arr       = bfi_response_struct.img_full_ccd_median_arr
  img_left_half_ccd_median_arr  = bfi_response_struct.img_left_half_ccd_median_arr
  img_right_half_ccd_median_arr = bfi_response_struct.img_left_half_ccd_median_arr
  img_full_ccd_moment_arr       = bfi_response_struct.img_full_ccd_moment_arr
  img_left_half_ccd_moment_arr  = bfi_response_struct.img_left_half_ccd_moment_arr
  img_right_half_ccd_moment_arr = bfi_response_struct.img_left_half_ccd_moment_arr
endif

n_synop = n_elements(t_synop)
;STOP
for i=0, n_synop-1 do begin
  proc_start_time = !stime
  t_now_sec = anytim(!stime, /sec)
  t_start_sec = anytim(t_synop[i], /sec)
  delvarx, cat_fg, files_fg

  sot_cat, t_synop[i], anytim(anytim(t_synop[i]) + (t_win_sot_minutes)*60d0, /ccsds), $
           cat_fg, files_fg, search_array=search_array, level0=level0, topdir=dir_cat, /quiet
;STOP
  if (size(cat_fg,/type) eq 8) then begin
    exist_arr = file_exist(files_fg)
    ss_exist = where(exist_arr eq 1, n_exist)

    if n_exist gt 0 then begin
      files_fg = files_fg(ss_exist)
      nfiles = n_elements(files_fg)
 
      for j=0,nfiles-1 do begin

        delvarx, index_fg0, data_fg0
	read_sot, files_fg(j), index_fg0, data_fg0 ;, strtemplate=index_fg_template

        if tag_exist(index_fg0, 'percentd') then percentd = index_fg0.percentd else $
          percentd = (n_elements(where(data_fg0 ne 0))/n_elements(data_fg0))*100
        if keyword_set(verbose) then print, 'percentd = ' + strtrim(percentd,2)

        ss_match_waveid = where(index_fg0.waveid eq waveid_arr, n_match_waveid)

        if ( (percentd ge 98) and (n_match_waveid ne 0) and $
             ( ( (index_fg0.naxis1 eq 4096) and (index_fg0.naxis2 eq 2048) )  or $
               ( (index_fg0.naxis1 eq 2048) and (index_fg0.naxis2 eq 1024) ) ) ) then begin

          data_fg0 = uint(data_fg0)

          if keyword_set(do_display) then tvscl, rebin(data_fg0, 1024, 512, /samp)

	  if keyword_set(do_prep) then begin
	    fg_prep, index_fg0, data_fg0, oindex_fg0, odata_fg0, $
                     /no_badpix, /no_flatfield, /no_pointing
	  endif else begin
	    oindex_fg0 = index_fg0
	    odata_fg0  = data_fg0
	  endelse

; Kluge: Rebin image to 2048 x 1024 (using nearest neighbor sampling),
;   regardless of original dimensions:
          if index_fg0.naxis1 eq 4096 then odata_fg0 = rebin(odata_fg0, 2048, 1024, /samp)

          ss_100 = where(odata_fg0 gt 100, count_100)
;         odata_fg0_100 = odata_fg0[ss_100]
          odata_fg0_100 = odata_fg0

	  norm_fac = oindex_fg0.exp0*float(oindex_fg0.campsum)*float(oindex_fg0.camssum)
	  fg_mean   = mean(odata_fg0_100)   / norm_fac
	  fg_median = median(odata_fg0_100) / norm_fac
	  fg_moment = moment(odata_fg0_100) / norm_fac

          sample0 = odata_fg0[xll_samp[0]:xur_samp[0], yll_samp[0]:yur_samp[0]]
          ss_100 = where(sample0 gt 100, count_100)
;         sample0_100 = sample0[ss_100]
          sample0_100 = sample0

	  fg_sample0_mean   = mean(sample0_100)   / norm_fac
	  fg_sample0_median = median(sample0_100) / norm_fac
	  fg_sample0_moment = moment(sample0_100) / norm_fac

          sample1 = odata_fg0[xll_samp[1]:xur_samp[1], yll_samp[1]:yur_samp[1]]
          ss_100 = where(sample1 gt 100, count_100)
;         sample1_100 = sample1[ss_100]
          sample1_100 = sample1

          fg_sample1_mean   = mean(sample1_100)   / norm_fac
	  fg_sample1_median = median(sample1_100) / norm_fac
	  fg_sample1_moment = moment(sample1_100) / norm_fac

          if not exist(t_arr) then begin
            t_arr                         = oindex_fg0.date_obs
            img_wave_arr                  = wave_name_arr[ss_match_waveid]
            img_full_ccd_mean_arr         = fg_mean
            img_left_half_ccd_mean_arr    = fg_sample0_mean
            img_right_half_ccd_mean_arr   = fg_sample1_mean
            img_full_ccd_median_arr       = fg_median
            img_left_half_ccd_median_arr  = fg_sample0_median
            img_right_half_ccd_median_arr = fg_sample1_median
            img_full_ccd_moment_arr       = fg_moment
            img_left_half_ccd_moment_arr  = fg_sample0_moment
            img_right_half_ccd_moment_arr = fg_sample1_moment
          endif else begin
            t_arr                         = [t_arr, oindex_fg0.date_obs]
            img_wave_arr                  = [img_wave_arr, wave_name_arr[ss_match_waveid]]
            img_full_ccd_mean_arr         = [img_full_ccd_mean_arr, fg_mean]
            img_left_half_ccd_mean_arr    = [img_left_half_ccd_mean_arr, fg_sample0_mean]
            img_right_half_ccd_mean_arr   = [img_right_half_ccd_mean_arr, fg_sample1_mean]
            img_full_ccd_median_arr       = [img_full_ccd_median_arr, fg_median]
            img_left_half_ccd_median_arr  = [img_left_half_ccd_median_arr, fg_sample0_median]
            img_right_half_ccd_median_arr = [img_right_half_ccd_median_arr, fg_sample1_median]
            img_full_ccd_moment_arr       = [[img_full_ccd_moment_arr], [fg_moment]]
            img_left_half_ccd_moment_arr  = [[img_left_half_ccd_moment_arr], [fg_sample0_moment]]
            img_right_half_ccd_moment_arr = [[img_right_half_ccd_moment_arr], [fg_sample1_moment]]
          endelse

        endif else begin
          print, '****************************************************'
          print, 'PERCENTD to small or image is wrong size.  Skipping.'
          print, '****************************************************'
        endelse

      endfor
      t_synop_sec_last = anytim(t_synop[i])

    endif else begin
      nfiles = 0
    endelse

  endif else begin
    nfiles = 0
  endelse

  if nfiles gt 0 then begin
    proc_end_time = !stime
    proc_time = addtime(proc_end_time, diff=proc_start_time, /sec)
    print, 'Finished for ' + strmid(t_synop[i],0,10) + '. ' + $
	   string(nfiles,format='$(i5.5)') + ' files. ' + $
	   strtrim(float(proc_time)/nfiles,2) + ' sec per file. ' + $
	   strtrim(proc_time,2) + ' sec total.'
  endif else begin
    proc_end_time = !stime
    proc_time = addtime(proc_end_time, diff=proc_start_time, /sec)
    print, 'Finished for ' + strmid(t_synop[i],0,10) + '.  No matches found.'
  endelse

endfor

; b = array[UNIQ(array, SORT(array))]
ss_uniq_sort = uniq(t_arr, sort(t_arr))
t_arr                         = t_arr[ss_uniq_sort]
img_wave_arr                  = img_wave_arr[ss_uniq_sort]
img_full_ccd_mean_arr         = img_full_ccd_mean_arr[ss_uniq_sort]
img_left_half_ccd_mean_arr    = img_left_half_ccd_mean_arr[ss_uniq_sort]
img_right_half_ccd_mean_arr   = img_right_half_ccd_mean_arr[ss_uniq_sort]
img_full_ccd_median_arr       = img_full_ccd_median_arr[ss_uniq_sort]
img_left_half_ccd_median_arr  = img_left_half_ccd_median_arr[ss_uniq_sort]
img_right_half_ccd_median_arr = img_right_half_ccd_median_arr[ss_uniq_sort]
img_full_ccd_moment_arr       = img_full_ccd_moment_arr[*, ss_uniq_sort]
img_left_half_ccd_moment_arr  = img_left_half_ccd_moment_arr[*, ss_uniq_sort]
img_right_half_ccd_moment_arr = img_right_half_ccd_moment_arr[*, ss_uniq_sort]

bfi_response_struct = { t_arr:t_arr, $
                        img_wave_arr:img_wave_arr, $
                        img_full_ccd_mean_arr:img_full_ccd_mean_arr, $
                        img_left_half_ccd_mean_arr:img_left_half_ccd_mean_arr, $
                        img_right_half_ccd_mean_arr:img_right_half_ccd_mean_arr, $
                        img_full_ccd_median_arr:img_full_ccd_median_arr, $
                        img_left_half_ccd_median_arr:img_left_half_ccd_median_arr, $
                        img_right_half_ccd_median_arr:img_right_half_ccd_median_arr, $
                        img_full_ccd_moment_arr:img_full_ccd_moment_arr, $
                        img_left_half_ccd_moment_arr:img_left_half_ccd_moment_arr, $
                        img_right_half_ccd_moment_arr:img_right_half_ccd_moment_arr }

if keyword_set(do_save) then $
  save, bfi_response_struct, t_synop_sec_last, file=filnam_bfi_response

end
