
pro rd_align_data, align_struct=align_struct, $
  r_solar=r_solar, t_r_solar=t_r_solar, $
  disk_cen_sot_ns_a=disk_cen_sot_ns_a, t_disk_cen_sot_ns_a=t_disk_cen_sot_ns_a, $
  disk_cen_sot_ns_b=disk_cen_sot_ns_b, t_disk_cen_sot_ns_b=t_disk_cen_sot_ns_b, $
  disk_cen_sot_ew_a=disk_cen_sot_ew_a, t_disk_cen_sot_ew_a=t_disk_cen_sot_ew_a, $
  disk_cen_sot_ew_b=disk_cen_sot_ew_b, t_disk_cen_sot_ew_b=t_disk_cen_sot_ew_b, $
  sot_fovcen2scpnt_ns=sot_fovcen2scpnt_ns, t_sot_fovcen2scpnt_ns=t_sot_fovcen2scpnt_ns, $
  sot_fovcen2scpnt_ew=sot_fovcen2scpnt_ew, t_sot_fovcen2scpnt_ew=t_sot_fovcen2scpnt_ew, $
  sot_fovcen2xrt_fovcen_ns_a=sot_fovcen2xrt_fovcen_ns_a, t_sot_fovcen2xrt_fovcen_ns_a=t_sot_fovcen2xrt_fovcen_ns_a, $
  sot_fovcen2xrt_fovcen_ns_b=sot_fovcen2xrt_fovcen_ns_b, t_sot_fovcen2xrt_fovcen_ns_b=t_sot_fovcen2xrt_fovcen_ns_b, $
  sot_fovcen2xrt_fovcen_ew_a=sot_fovcen2xrt_fovcen_ew_a, t_sot_fovcen2xrt_fovcen_ew_a=t_sot_fovcen2xrt_fovcen_ew_a, $
  sot_fovcen2xrt_fovcen_ew_b=sot_fovcen2xrt_fovcen_ew_b, t_sot_fovcen2xrt_fovcen_ew_b=t_sot_fovcen2xrt_fovcen_ew_b

;+
;   Name: RD_ALIGN_DATA
;
;   Purpose: Read the Shimizu-generated Hinode alignment results table and return all parameters
;
;   Input Parameters:
;
;   Output Paramters:
;      See call.
;
;   Keyword Parameters:
;      
;   Calling Sequence:
;      IDL> rd_align_data, [output params here]
;
;   History:
;      03-feb-2009 - G.L.Slater
;      13-Mar-2013 - Zarro, use is_number() to check for non-numeric
;                    structure fields
;   TODO:
;      - Change check on legitimate data values to use IS_NUMBER.PRO
;        or the like
;-

align_str = hinode_time2alignment()

date_obs		   = align_str.(00)
r_solar			   = align_str.(21)
disk_cen_sot_ns_a 	   = align_str.(23)
disk_cen_sot_ns_b 	   = align_str.(24)
disk_cen_sot_ew_a 	   = align_str.(25)
disk_cen_sot_ew_b	   = align_str.(26)
sot_fovcen2scpnt_ns 	   = align_str.(28)
sot_fovcen2scpnt_ew 	   = align_str.(29)
sot_fovcen2xrt_fovcen_ns_a = align_str.(33)
sot_fovcen2xrt_fovcen_ns_b = align_str.(34)
sot_fovcen2xrt_fovcen_ew_a = align_str.(35)
sot_fovcen2xrt_fovcen_ew_b = align_str.(36)

ss_good = where( (date_obs ne '') and (align_str.(21) ne '') and (strtrim(align_str.(21),2) ne 'N/A'), n_good)
r_solar = double(align_str[ss_good].(21))
t_r_solar = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(23) ne '') and (strtrim(align_str.(23),2) ne 'N/A'), n_good)
disk_cen_sot_ns_a = double(align_str[ss_good].(23))
t_disk_cen_sot_ns_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(24) ne '') and (strtrim(align_str.(24),2) ne 'N/A'), n_good)
disk_cen_sot_ns_b = double(align_str[ss_good].(24))
t_disk_cen_sot_ns_b = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(25) ne '') and (strtrim(align_str.(25),2) ne 'N/A'), n_good)
disk_cen_sot_ew_a = double(align_str[ss_good].(25))
t_disk_cen_sot_ew_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(26) ne '') and (strtrim(align_str.(26),2) ne 'N/A'), n_good)
disk_cen_sot_ew_b = double(align_str[ss_good].(26))
t_disk_cen_sot_ew_b = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(28) ne '') and (strtrim(align_str.(28),2) ne 'N/A'), n_good)
sot_fovcen2scpnt_ns = double(align_str[ss_good].(28))
t_sot_fovcen2scpnt_ns = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(29) ne '') and (strtrim(align_str.(29),2) ne 'N/A'), n_good)
sot_fovcen2scpnt_ew = double(align_str[ss_good].(29))
t_sot_fovcen2scpnt_ew = date_obs[ss_good]

;-- better to use is_number here and other similar places

;ss_good = where( (date_obs ne '') and (align_str.(33) ne '') and (strtrim(align_str.(33),2) ne 'N/A'), n_good)
ss_good = where( (date_obs ne '') and is_number(align_str.(33)),n_good)
if n_good gt 0 then sot_fovcen2xrt_fovcen_ns_a = double(align_str[ss_good].(33))

t_sot_fovcen2xrt_fovcen_ns_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(34) ne '') and (strtrim(align_str.(34),2) ne 'N/A'), n_good)
sot_fovcen2xrt_fovcen_ns_b = double(align_str[ss_good].(34))
t_sot_fovcen2xrt_fovcen_ns_b = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(35) ne '') and (strtrim(align_str.(35),2) ne 'N/A'), n_good)
sot_fovcen2xrt_fovcen_ew_a = double(align_str[ss_good].(35))
t_sot_fovcen2xrt_fovcen_ew_a = date_obs[ss_good]

ss_good = where( (date_obs ne '') and (align_str.(36) ne '') and (strtrim(align_str.(36),2) ne 'N/A'), n_good)
sot_fovcen2xrt_fovcen_ew_b = double(align_str[ss_good].(36))
t_sot_fovcen2xrt_fovcen_ew_b = date_obs[ss_good]

align_struct = {r_solar:r_solar, t_r_solar:t_r_solar, $
                disk_cen_sot_ns_a:disk_cen_sot_ns_a, t_disk_cen_sot_ns_a:t_disk_cen_sot_ns_a, $
                disk_cen_sot_ns_b:disk_cen_sot_ns_b, t_disk_cen_sot_ns_b:t_disk_cen_sot_ns_b, $
                disk_cen_sot_ew_a:disk_cen_sot_ew_a, t_disk_cen_sot_ew_a:t_disk_cen_sot_ew_a, $
                disk_cen_sot_ew_b:disk_cen_sot_ew_b, t_disk_cen_sot_ew_b:t_disk_cen_sot_ew_b, $
                sot_fovcen2scpnt_ns:sot_fovcen2scpnt_ns, t_sot_fovcen2scpnt_ns:t_sot_fovcen2scpnt_ns, $
                sot_fovcen2scpnt_ew:sot_fovcen2scpnt_ew, t_sot_fovcen2scpnt_ew:t_sot_fovcen2scpnt_ew, $
                sot_fovcen2xrt_fovcen_ns_a:sot_fovcen2xrt_fovcen_ns_a, t_sot_fovcen2xrt_fovcen_ns_a:t_sot_fovcen2xrt_fovcen_ns_a, $
                sot_fovcen2xrt_fovcen_ns_b:sot_fovcen2xrt_fovcen_ns_b, t_sot_fovcen2xrt_fovcen_ns_b:t_sot_fovcen2xrt_fovcen_ns_b, $
                sot_fovcen2xrt_fovcen_ew_a:sot_fovcen2xrt_fovcen_ew_a, t_sot_fovcen2xrt_fovcen_ew_a:t_sot_fovcen2xrt_fovcen_ew_a, $
                sot_fovcen2xrt_fovcen_ew_b:sot_fovcen2xrt_fovcen_ew_b, t_sot_fovcen2xrt_fovcen_ew_b:t_sot_fovcen2xrt_fovcen_ew_b}

end
