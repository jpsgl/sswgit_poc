
pro go_sdo_corr, t0, t1, hours_grid=hours_grid, xrt=xrt, level0=level0, $
  dir_top=dir_top, search_array=search_array, percentd_min=percentd_min, $
  cat_sot=cat_sot, files_sot=files_sot, refresh_cat=refresh_cat, $
  every_nth=every_nth, n_max_files=n_max_files, $
  force_invert=force_invert, nsig_in=nsig_in, interactive=interactive, $
  do_raw_cube=do_raw_cube, do_cubes=do_cubes, do_reg=do_reg, wcs_ref=wcs_ref, $
  do_xstep=do_xstep, do_write=do_write, do_test=do_test, $
  verbose=verbose, no_log_time=no_log_time, $
  _extra=_extra

;+
; name: go_sdo_corr
; call: go_sdo_corr, t0, t1
; -

if not exist(hrs_forward) then hrs_forward = 6
if not exist(minutes_backoff) then minutes_backoff = 0
if not exist(level0) and not keyword_set(quicklook) then level0 = 1
if not exist(mode_select) then mode_select = 'fg'
if not exist(hours_grid) then hours_grid = 1.0 ; 0.5
;if not exist(dir_top) then dir_top = '/net/castor/Users/slater/data/sot_aia_align'
;if not exist(dir_top) then dir_top = '/sanhome/slater/public_html/data/sot_aia_align'
;if not exist(dir_top) then dir_top = '/ssw/hinode/sot/idl/fg/cal/data/sot_aia_align'

if not exist(dir_top) then begin
  if keyword_set(do_test) then $
    dir_top = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_aia_align_test' else $
    dir_top = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_aia_align'
endif
if not exist(dir_top_seg) then begin
  if keyword_set(do_test) then $
    dir_top_seg = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_corr_segments_test' else $
    dir_top_seg = '$SSWDB/hinode/gen/hinode_sdo_alignment/sot_corr_segments'
endif

if not exist(percentd_min) then percentd_min = 98

if keyword_set(interactive) then begin

  if not exist(cat_sot) or keyword_set(refresh_cat) then $
    sot_cat, t0, t1, cat_sot, files_sot, xrt=keyword_set(xrt), search_array=search_array, $
             level0=level0, quicklook=quicklook, count=count, tcount=tcount

  if keyword_set(xrt) then $
    mode_tags = 'ec_fw1_, ec_fw2_ naxis1, naxis2' else $
    mode_tags = 'wave, waveid, obs_type, naxis1, naxis2'

  ans = ''
  while strupcase(strmid(ans,0,1)) ne 'N' do begin
    ss_mode = sot_umodes(incat=cat_sot, /int)

    files_sot_select = files_sot[ss_mode] ; [last_nelem(ss_mode,200)]
    n_files_select = n_elements(ss_mode)

    ; Optionally only process every 'nth' file:
    if keyword_set(every_nth) then begin
      ss_nth = indgen(n_files_select/every_nth) * every_nth
      files_sot_select = files_sot_select[ss_nth]
      n_files_select = n_elements(files_sot_select)
    endif

    if not exist(n_max_files) then n_max_files = n_files_select

    if keyword_set(do_raw_cube) then begin
      mreadfits, files_sot_select, index_raw, data_raw
;     xstepper, data_raw
    endif

    for i=0, (n_files_select<n_max_files)-1 do begin
      file_sot = reform(files_sot_select[i])
      delvarx, index_sot, data_sot
      read_sot, file_sot, index_sot, data_sot

; If read_sot failed, try once more:
      if not exist(index_sot) then begin
        print, 'read_sot failed.  Waiting 100 seconds and trying again.'
        wait, 100
        read_sot, file_sot, index_sot, data_sot
      endif

;     if exist(index_sot) then begin

; Define percentd for images:
      if tag_exist(index_sot, 'percentd') then percentd = index_sot.percentd else $
        percentd = (n_elements(where(data_sot ne 0))/n_elements(data_sot))*100
    
      if percentd ge percentd_min then begin

        sdo_corr, index_sot, data_sot, indexp_sot, datap_sot, $
                  file_sot=file_sot[0], file_sdo=file_sdo, $
                  norollcorrect=norollcorrect, nocrop2disk=nocrop2disk, $
                  peak_str=peak_str, align_str=align_str, $
                  do_img_plot=do_img_plot, do_hist_plot=do_hist_plot, $
                  do_plot_map=do_plot_map, do_blink=do_blink, $
                  verbose=verbose, _extra=_extra

        if ( keyword_set(do_reg) or exist(wcs_ref) ) then begin
          if not exist(wcs_ref) then wcs_ref = indexp_sot
          if not exist(cutout) then cutout = 1
          if not exist(match_fov) then match_fov = 1

;         ssw_reg, indexp_sot, datap_sot, indexr_sot, datar_sot, wcs_ref=wcs_ref, $
;                  cutout=cutout, match_fov=match_fov, $
;                  scale_ref=scale_ref, roll_ref=roll_ref, crpix1_ref, crpix2_ref, $
;                  x0_ref=x0_ref, y0_ref=y0_ref, $
;                  interp=interp, cubic=cubic, x_off=x_off, y_off=y_off, $
;                  _extra=_extra, qstop=qstop

do_derot = 1
derotate = 1
;STOP
;if ( (indexr_sot.[] eq []) and keyword_set(do_derot) ) then derotate = 1
          ssw_register, indexp_sot, datap_sot, indexr_sot, datar_sot, ref_index=wcs_ref, $
                        ref_map=ref_map, derotate=derotate, drotate=drotate, $
                        clobber=clobber, correl=correl, roll=roll, $
                        offsets=offsets

        endif

        if keyword_set(do_write) then begin
          if not exist(align_str_arr) then align_str_arr = align_str else $
            align_str_arr = concat_struct(align_str_arr, align_str)
        endif

        if keyword_set(do_cubes) then begin
          if not exist(indexp_arr) then begin
            indexp_arr = indexp_sot
            datap_cube = datap_sot
            if exist(indexr_sot) then begin
              indexr_arr = indexr_sot
              datar_cube = datar_sot
            endif
          endif else begin
            indexp_arr = concat_struct(indexp_arr, indexp_sot)
            datap_cube = [[[datap_cube]], [[datap_sot]]]
            if exist(indexr_sot) then begin
              indexr_arr = concat_struct(indexr_arr, indexr_sot)
              datar_cube = [[[datar_cube]], [[datar_sot]]]
            endif
          endelse
        endif

      endif else begin
        print, ' read_sot failed a second time.  Skipping this file.'
        skip = 1
      endelse

    endfor
    help, index_raw, indexp_arr, indexr_arr, data_raw, datap_cube, datar_cube

    if ( exist(datar_cube) and keyword_set(do_xstep) )then begin
      device, decomp=0
      siz_datap_cube = size(datap_cube)
      xsiz_datap_cube = siz_datap_cube[1]
      ysiz_datap_cube = siz_datap_cube[2]
      n_img = n_elements(datap_cube[0,0,*])
      lil_datap_cube = rebin(datap_cube, xsiz_datap_cube/2, ysiz_datap_cube/2, n_img, /samp)
      lil_datar_cube = rebin(datar_cube, xsiz_datap_cube/2, ysiz_datap_cube/2, n_img, /samp)
      xstepper, [[lil_datar_cube],[lil_datap_cube]]
    endif

    if ( exist(align_str_arr) and keyword_set(do_write) ) then begin
;     write_genxcat, align_str_arr, topdir=dir_top, /hour_round, $
;                    /nelements, /geny
      write_genxcat, align_str_arr, topdir=dir_top, /hour_round
      delvarx, align_str_arr
    endif

    read, ' Select another (def is yes)? ', ans
  endwhile

endif else begin

  if not exist(dir_logs) then dir_logs = concat_dir('$HOME', 'logs')
; file_t_last = concat_dir(dir_logs, 'last_file_sdo_corr' + '.txt')
; file_t_last = concat_dir(dir_logs, 'last_file_sdo_corr.txt')
  filnam_pipeline_log = concat_dir(dir_logs, 'sdo_corr_pipeline_data.txt')
  if file_exist(filnam_pipeline_log) then begin
;   buff = str2arr(rd_tfile(filnam_pipeline_log), delim='   ')
    buff = rd_tfile(filnam_pipeline_log, 3)

    n_rec = n_elements(buff[0,*])
    rec_last = buff[*,n_rec-1]
    t_create_ut     = rec_last[0]
    t_create_ut_sec = anytim(t_create_ut)
    t_obs_last_ut   = rec_last[1]
    t_obs_last_ut_sec = anytim(t_obs_last_ut)
    t_exec_sec      = rec_last[2]

    t_now_ut        = anytim(ut_time(!stime), /ccsds)
    t_now_ut_sec    = anytim(t_now_ut)
    t_lag_sec       = t_now_ut_sec - t_obs_last_ut_sec
    sec_backward    = minutes_backoff*60d0
    sec_forward     = t_lag_sec < hrs_forward*3600d0

    if not exist(t0) then begin
      t0 = anytim(t_obs_last_ut_sec - sec_backward, /ccsds)
      t1 = anytim(t_obs_last_ut_sec + sec_forward, /ccsds)
    endif
  endif else begin
    print, ' Pipeline log not found.'
  endelse

  if keyword_set(verbose) then begin
    if exist(t_obs_ut) then print, 't_last = ' + strtrim(t_obs_ut,2)
    print, 't0 = '  + strtrim(t0,2)
    print, 't1 = '  + strtrim(t1,2)
  endif

  t_grid = anytim(timegrid(t0, t1, hours=hours_grid), /ccsds)
  n_grid = n_elements(t_grid)-1

  for i=0, n_grid-1 do begin
    sot_cat, t_grid[i], t_grid[i+1], cat_sot, files_sot, xrt=keyword_set(xrt), $
             search_array=search_array, level0=level0, quicklook=quicklook, $
             count=count, tcount=tcount


if count gt 0 then begin


    ; Filter for desired mode and against bad mode list:
    mode = strlowcase(cat_sot.wave + ' ' + cat_sot.obs_type)
;   ss_match = where(strpos(mode, mode_select) ne -1, n_match)

     ; Totally Ad Hoc Kluge 2:
    ;if mode eq 'TF Na I 5896 FG shutterless I and V with 0.2s intervals' then n_match = 0
    ;if mode eq 'TF Na I 5896 FG shutterless I and V with 0.2s intervals 1' then n_match = 0
    ss_match = where(  ( (strpos(mode, mode_select) ne -1) and $
                         (mode ne 'tf na i 5896 fg shutterless i and v with 0.2s intervals'  ) and $
                         (mode ne 'tf na i 5896 fg shutterless i and v with 0.2s intervals 1') ),  n_match)

    if n_match gt 0 then begin

      files_sot = files_sot[ss_match]
      cat_sot = cat_sot[ss_match]

      ; Filter out non-existent files:
      ss_exist = where(file_exist(files_sot) eq 1, n_exist)

      if n_exist gt 0 then begin

        files_sot = files_sot[ss_exist]
        cat_sot = cat_sot[ss_exist]
        n_files = n_exist

        ; Optionally only process every 'nth' file:
        if keyword_set(every_nth) then begin
          ss_nth = indgen(ceil(float(n_files)/every_nth)) * every_nth
          files_sot = files_sot[ss_nth]
          cat_sot = cat_sot[ss_nth]
          n_files = n_elements(ss_nth)
        endif

        for j=0, n_files-1 do begin

skip = 0
;t_exec_start = anytim(ut_time(!stime), /ccsds)
 t_exec_start = anytim(!stime, /ccsds)

          file_sot = reform(files_sot[j])
          delvarx, index_sot, data_sot
          read_sot, file_sot, index_sot, data_sot

          ; If read_sot failed, try once more:
          if not exist(index_sot) then begin
            print, 'read_sot failed.  Waiting 100 seconds and trying again.'
            wait, 100
            read_sot, file_sot, index_sot, data_sot
          endif

          if exist(index_sot) then begin

            ; Define percentd for images:
            if tag_exist(index_sot, 'percentd') then percentd = index_sot.percentd else $
              percentd = (n_elements(where(data_sot ne 0))/n_elements(data_sot))*100
    
            if percentd ge percentd_min then begin

              sdo_corr, index_sot, data_sot, indexp_sot, datap_sot, $
                file_sot=file_sot[0], file_sdo=file_sdo, $
                norollcorrect=norollcorrect, nocrop2disk=nocrop2disk, $
                peak_str=peak_str, align_str=align_str, $
                do_img_plot=do_img_plot, do_hist_plot=do_hist_plot, $
                do_xstep=do_xstep, do_blink=do_blink, verbose=verbose, $
                _extra=_extra

              if exist(align_str) then begin
                if not exist(align_str_arr) then align_str_arr = align_str else $
                  align_str_arr = concat_struct(align_str_arr, align_str)
                delvarx, align_str
              endif

            endif

          endif else begin
            print, ' read_sot failed a second time.  Skipping this file.'
skip = 1
          endelse

;t_exec_end = anytim(ut_time(!stime), /ccsds)
 t_exec_end = anytim(!stime, /ccsds)

if ( (skip eq 0) and (not keyword_set(no_log_time)) ) then begin
  t_exec_sec = long(anytim(t_exec_end) - anytim(t_exec_start))
  t_obs = anytim(index_sot.date_obs, /ccsds)

  if not exist(dir_logs) then dir_logs = concat_dir('$HOME', 'logs')
; filnam_t_last = concat_dir(dir_logs, 'last_file_' + strtrim(hd.wavelnth,2))
  filnam_pipeline_log = concat_dir(dir_logs, 'sdo_corr_pipeline_data.txt')
  buff = strtrim(t_exec_end,2) + '   ' + strtrim(t_obs,2) + '   ' + string(t_exec_sec,'$(i4.4)')
  file_append, filnam_pipeline_log, buff, new=new
endif

        endfor

        if ( exist(align_str_arr) and keyword_set(do_write) ) then begin
;         write_genxcat, align_str_arr, topdir=dir_top, /hour_round, $
;                        /nelements, /geny
          write_genxcat, align_str_arr, topdir=dir_top, /hour_round
          delvarx, align_str_arr
        endif

      endif

    endif


endif


    print, ' Finished for '+ anytim(t_grid[i],   /yoh) + $
                             anytim(t_grid[i+1], /yoh)

  endfor

endelse

end

