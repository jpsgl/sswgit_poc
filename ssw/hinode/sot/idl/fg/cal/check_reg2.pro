
pro check_reg2, t0, wave_arr=wave_arr, wave_ref=wave_ref, delta_hr=delta_hr,  $
  data=data, data_reg=data_reg, sample_cube=sample_cube, p_arr=p_arr, q_arr=q_arr, $
  display=display, quiet=quiet

;+
; NAME:
;       check_reg2
; PURPOSE:
;       Check the registration coefficients for a specified time and two specified
;	wave bands;
; SAMPLE CALLING SEQUENCE:
;	check_reg2, '12-sep-2007', wave_arr=['blue','gband'], sample_cube=sample_cube
; INPUTS:
;	t0 - Reference time
;	wave_arr - String array of acronyms for wave bands (e.g., ['gband','green'])
;		   Acronyms are as follows
;			'gband'	= 'G*band*4305'
;			'blue'	= 'blue*cont*4504'"
;			'red'	= 'red*cont*6684'"
;			'green'	= 'green*cont*5550'"
;			'ca'	= 'Ca*II*H*line'"
;			'cn'	= 'CN*bandhead*3883'"
; OPIONAL KEYWORDS:
;	display - If set, launch xstepper.pro with the generated sample data cube
;		  composed of unregistered/registered image samples
; OUTPUTS
;	sample_cube - Sampled array consisting of the four corners of each unregistered
;		      image side-by-side with the corresponding samples from the
;		      registered images.
; OPTIONAL OUTPUTS
;	P_ARR - Polynomial coefficients for x coordinate transformation via poly_2d
;	Q_ARR - Polynomial coefficients for y coordinate transformation via poly_2d
;-

; Set default section:
; ----------------------
if not exist(wave_arr) then wave_arr = ['blue','ca']
if not exist(wave_ref) then wave_ref = 'blue'
if not exist(interp) then interp = 2
if not exist(cubic) then cubic = -0.5
; ----------------------

if get_logenv('SOT_REG_COEFF') eq '' then $
  set_logenv, 'SOT_REG_COEFF', $
    '/net/hikari/home/slater/soft/idl/fpp/fg_prep/sot_reg_coeff.geny', quiet=quiet
;    '/net/hikari/home/slater/soft/idl/fpp/fg_prep/sot_reg_coeff_v0.geny', quiet=quiet

tim2fg_synop, t0, delta_hr=delta_hr, wave_arr=wave_arr, owave_arr=owave_arr, $
  /read_files, files=files, cat=cat, index=index, data=data, lildata=lildata, $
  quiet=quiet

sizdata = size(data)
nimg = sizdata(3)
data_reg = intarr(sizdata(1),sizdata(2),nimg)

delvarx, p_arr, q_arr
for i=0,nimg-1 do begin
  wave0 = owave_arr(i)
  if wave0 eq wave_ref then begin
    data_reg(0,0,i) = data(*,*,i)
  endif else begin
    t0 = anytim(index(i).date_obs,/int)
    pq_cube = get_fg_reg_coeff(t0, wave0=wave0, wave_ref=wave_ref, filnam_geny=filnam_geny, $
      p_arr=p0, q_arr=q0)

    if not exist(p_arr) then p_arr = p0 else p_arr = [[[p_arr]],[[p0]]]
    if not exist(q_arr) then q_arr = q0 else q_arr = [[[q_arr]],[[q0]]]

    data_reg(0,0,i) = $
      poly_2d(reform(data(*,*,i)),p0,q0,interp,cubic=cubic,missing=missing)
  endelse
endfor

; Construct sample cube consisting of the four 256x256 'corners' of interleaved
; images from the data and data_reg cubes:

;sample_cube = intarr(512,512,nimg*2)
sample_cube = intarr(512*2,512,nimg)

sample_cube(0,0,0) = data(0:255,0:255,*)
sample_cube(256,0,0) = data((4095-255):4095,0:255,*)
sample_cube(0,256,0) = data(0:255,(2047-255):2047,*)
sample_cube(256,256,0) = data((4095-255):4095,(2047-255):2047,*)

;sample_cube(0,0,nimg) = data_reg(0:255,0:255,*)
;sample_cube(256,0,nimg) = data_reg((4095-255):4095,0:255,*)
;sample_cube(0,256,nimg) = data_reg(0:255,(2047-255):2047,*)
;sample_cube(256,256,nimg) = data_reg((4095-255):4095,(2047-255):2047,*)
sample_cube(0+512,0,0) = data_reg(0:255,0:255,*)
sample_cube(256+512,0,0) = data_reg((4095-255):4095,0:255,*)
sample_cube(0+512,256,0) = data_reg(0:255,(2047-255):2047,*)
sample_cube(256+512,256,0) = data_reg((4095-255):4095,(2047-255):2047,*)

;if keyword_set(interleave) then $
;  sample_cube = sample_cube(*,*,reform(transpose(indgen(nimg,2)),nimg*2))

if keyword_set(display) then xstepper, sample_cube	;, xsize=512, ysize=256

end
