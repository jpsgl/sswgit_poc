
function mk_flat_cube, wave, bfi=bfi, nfi=nfi, side=side, bin=bin, $
  get_hfits=get_hfits, get_fits=get_fits, $
  t_flats=t_flats, flat_files=flat_files, flat_fits=flat_fits, $
  index_flat=index_flat, data_flat=data_flat

waves_bfi = [ '6684', '4305', '3968', '3883', '4504', '5550' ]
waves_nfi = [ '6302', '5576', '5896', '5250', '5172', '6563' ]

if n_params() eq 0 then begin
  print, ""
  print, "Call: flat_cube = mk_flat_cube(wave, bfi=bfi, nfi=nfi, side=side, bin=bin)
  print, "Example call: flat_cube = mk_flat_cube(6684, /bfi, side='c')"
  print, "Must supply 'WAVE' from among:"
  print, ['BFI: ', waves_bfi]
  print, ['NFI: ', waves_nfi]
  print, "Must supply CCD region parameter 'SIDE' from among:"
  print, ['BFI: ','l','r','c']
  print, ['NFI: ','f']
  return, ''
endif

if not exist(dir_top) then $
  dir_top = '/sanhome/slater/public_html/missions/hinode/sot/' + $
            (['nfi', 'bfi'])[keyword_set(bfi)] + '/flats'

pw_dir = curdir()
cd, dir_top
flat_dirs = $
  file_search('sot_' + (['nfi', 'bfi'])[keyword_set(bfi)] + '_flat_????A_?_?x?_????????_????')

ss_match_wave = where(strpos(flat_dirs, strtrim(wave,2) + 'A_') ne -1, n_match_wave)
if n_match_wave gt 0 then begin

  flat_dirs = flat_dirs[ss_match_wave]

  ss_match_side = where(strpos(flat_dirs, 'A_' + side + '_') ne -1, n_match_side)
  if n_match_side gt 0 then begin
    flat_dirs = flat_dirs[ss_match_side]
  endif else begin
    print, 'No flats matching request found. Returning.'
    return, ''
  endelse

  if exist(bin) then begin
    pattern = strtrim(wave,2) + 'A_' + side + '_' + strtrim(bin,2) + 'x'
    ss_match_bin = where(strpos(flat_dirs, pattern) ne -1, n_match_bin)
    if n_match_bin gt 0 then begin
      flat_dirs = flat_dirs[ss_match_bin]
    endif else begin
      print, 'No flats matching request found. Returning.'
      return, ''
    endelse
  endif

endif else begin
  print, 'No flats matching request found. Returning.'
  return, ''
endelse

flat_files = file_search(flat_dirs, 'flat_????????_??????.gif')
t_flats = file2time(flat_files)

if flat_files[0] ne '' then begin
  n_files = n_elements(flat_files)
  flat_cube = files2data(flat_files)
;  for i=0, n_files-1 do begin
;    read_gif, flat_files[i], flat_gif0
;    if i eq 0 then flat_cube = flat_gif0 else $
;      flat_cube = [[[flat_cube]],[[flat_gif0]]]
;  endfor
endif else begin
  print, 'No flats matching request found. Returning.'
  return, ''
endelse

flat_fits = flat_dirs + '/' + flat_dirs + '.fits'
ss_exist_arr = file_exist(flat_fits)
ss_exist = where(ss_exist_arr ne -1, n_exist)
ss_not_exist = where(ss_exist_arr eq -1, n_not_exist)

if n_not_exist gt 0 then begin
  print, ' The following flat dirs do not contain a flat file with correct naming convention:'
  prstr, flat_dirs[ss_not_exist]
endif

if n_exist gt 0 then begin
  flat_fits = flat_fits[ss_exist]
  if keyword_set(get_hfits) then mreadfits_header, flat_fits, index_flat
  if keyword_set(get_fits) then mreadfits, flat_fits, index_flat, data_flat
endif

if not keyword_set(no_display) then xstepper, flat_cube

cd, pw_dir
return, flat_cube

end
