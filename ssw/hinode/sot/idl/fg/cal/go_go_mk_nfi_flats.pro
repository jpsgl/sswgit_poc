
;go_go_mk_nfi_flats

sttim = anytim(last_flat(), /ccsds, /sec)
entim = anytim(ut_time(), /ccsds)

go_mk_nfi_flat, sttim, /mk_flat, /do_print, max_err=.003, min_frac=.001, /use_fg_prep, $
  maxiter=100, level0=1 ; , /do_make_top, /do_make_flat, /do_make_only

sttim = anytim(last_flat(), /ccsds, /sec)
entim = anytim(ut_time(), /ccsds)

go_mk_nfi_flat, sttim, /mk_flat, /do_print, max_err=.003, min_frac=.001, /use_fg_prep, $
  maxiter=100, level0=0 ; , /do_make_top, /do_make_flat, /do_make_only

mk_sot_flat_html_files, /by_date

end
