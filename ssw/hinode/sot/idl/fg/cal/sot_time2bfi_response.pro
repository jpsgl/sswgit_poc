
function sot_time2bfi_response, t, wave=wave, left=left, right=right, all=all, $
  do_plot=do_plot, do_ratio=do_ratio, s_wid=s_wid, do_write=do_write, $
  verbose=verbose, quiet=quiet, _extra=_extra

;+
; NAME:
; PURPOSE:
; CALLING SEQUENCE:
; EXAMPLE CALLS:
;   IDL> 
; REQUIRED INPUTS:
; OPTIONAL INPUTS:
; KEYWORD INPUTS (for call to HINODE_POINTING_SCAN):
; OUTPUTS:
; USAGE:
; REVISION HISTORY:
;   v 1.0    2014-03-20 - GLS LMSAL
; TODO:
;   Shorter name
;-

if not exist(filnam_bfi_response) then $
 filnam_bfi_response  = '$HOME/soft/idl/idl_startup/sot/bfi_response_mission.sav'
restore, filnam_bfi_response

wave_arr = ['gband','blue','red','green','ca','cn']

if not exist(wave_col) then wave_col = [1,10,2,8,12,11]
if not exist(s_wid) then s_wid = 7

if not exist(t) then begin
;  prstr, [ $
;          ';+', $
;          '; NAME:', $
;          '; PURPOSE:', $
;          '; CALLING SEQUENCE:', $
;          '; EXAMPLE CALLS:', $
;          ';   IDL> ', $
;          '; REQUIRED INPUTS:', $
;          '; OPTIONAL INPUTS:', $
;          '; KEYWORD INPUTS (for call to HINODE_POINTING_SCAN):', $
;          '; OUTPUTS:', $
;          '; USAGE:', $
;          '; REVISION HISTORY:', $
;          ';   v 1.0    2014-03-20 - GLS LMSAL', $
;          '; TODO:', $
;          ';   Shorter name', $
;          ';-' $
;         ]

  prstr, [ $
          '; CALL:', $
          ';   IDL> response = sot_time2bfi_response(t, wave=wave, left=left, right=right, all=all,', $
          ';                                         do_plot=do_plot)', $
          ';     where:', $
          ';       - wave is one of ["gband","blue","red","green","ca","cn"] (default is "gband")', $
          ';       - /do_plot to plot the mission response for selected wave', $
          ';', $
          '; EXAMPLE CALLS:', $
          ';   Basic call returns BFI mean full frame response interpolated at passed T_SAMP values:', $
          ';     IDL> resp = sot_time2bfi_response(t_samp)', $
          ';   BFI mean left CCD response interpolated at passed T_SAMP values:', $
          ';     IDL> resp = sot_time2bfi_response(t_samp, /left)', $
          ';   BFI mean right CCD response interpolated at passed T_SAMP values:', $
          ';     IDL> resp = sot_time2bfi_response(t_samp, /right)', $
          ';   BFI mean full, left, and right CCD responses interpolated at passed T_SAMP values', $
          ';   (Output is 3 x N, where is n_elements(t_samp) ):', $
          ';     IDL> resp = sot_time2bfi_response(t_samp, /all)', $
          ';   Basic call returns BFI mean full frame response interpolated at passed T_SAMP values:', $
          ';     IDL> resp = sot_time2bfi_response(t_samp)' $
         ]

  if keyword_set(do_write) then begin
    cur_val_plot = !d.name
    set_plot, 'z'
  endif

  wdef,0,1024,1280
  device, decomp=0
  linecolors
  if keyword_set(do_ratio) then !p.multi = [0,1,6] else !p.multi = 0
  for i=0,5 do begin
    wave = wave_arr[i]
    ss_match_wave = where(bfi_response_struct.img_wave_arr eq wave, n_match_wave)
    t_arr = bfi_response_struct.t_arr[ss_match_wave]
    bfi_full_ccd_mean = bfi_response_struct.img_full_ccd_mean_arr[ss_match_wave]
    bfi_left_half_ccd_mean = bfi_response_struct.img_left_half_ccd_mean_arr[ss_match_wave]
    bfi_right_half_ccd_mean = bfi_response_struct.img_right_half_ccd_mean_arr[ss_match_wave]

    if not keyword_set(do_ratio) then begin

      norm = max(bfi_full_ccd_mean)
;     tit = 'BFI ' + wave + ' normalized response (smoothed)'
      tit = 'BFI normalized response (smoothed)'
      ytit = 'Normalized Response'

      if i eq 0 then begin
        utplot,  t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN/norm,s_wid), /ystyle, charsize=2, psym=3, $
          yrange=[0.30, 1.05], tit=tit, ytit=ytit, thick=3, charthick=2, $
          color=0, background=255, /nodata
;         yrange=[6e3,1.2e4], tit=tit
      endif

      outplot, t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN/norm,s_wid), psym=-3, color=wave_col[i], linestyle=0, thick=3
      outplot, t_arr, smooth(BFI_RIGHT_HALF_CCD_MEAN/norm,s_wid), psym=-3, color=wave_col[i], linestyle=1, thick=3

      leg_arr = ['gband _____','blue  _____','red   _____','green _____','ca    _____','cn    _____']
      ssw_legend, leg_arr, $
                  textcolors=wave_col, charsize=2, charthick=2, /top, /right
      ssw_legend, leg_arr, $
                  textcolors=wave_col, charsize=2, charthick=2, /bot, /right
;     ssw_legend, ['left CCD ...............', 'right CCD -----'], charsize=1.5, /top, /right

    endif else begin

      norm = max(bfi_full_ccd_mean)
      tit='BFI ' + strupcase(wave_arr[i]) + ' right/left CCD response ratio (smoothed)'
      ytit = 'Ratio'
      utplot,  t_arr, $
        smooth(smooth(bfi_right_half_ccd_mean/norm,s_wid) / smooth(bfi_left_half_ccd_mean/norm,s_wid),s_wid), $
        yrange=[0.96,1.00], /ystyle, charsize=3, psym=3, tit=tit, ytit=ytit, /year

    endelse

  endfor
  !p.multi = 0

  if keyword_set(do_write) then begin
    outfil_gif = '/archive1/hinode/sot/bfi_throughput/bfi_throughput_latest.gif'
    zbuff2file2, outfil_gif
    set_plot, cur_val_plot
  endif

  return, bfi_response_struct
endif

if not exist(wave) then wave = 'gband'
wave_arr = ['gband','blue','red','green','ca','cn']
ss_match_wave = (where(wave_arr eq strlowcase(strtrim(wave,2)), count_match))[0]

;t_names = tag_names(bfi_respnse_struct)
wave = wave_arr[ss_match_wave]
ss_match_wave = where(bfi_response_struct.img_wave_arr eq wave, n_match_wave)
t_arr = bfi_response_struct.t_arr[ss_match_wave]
bfi_full_ccd_mean = bfi_response_struct.img_full_ccd_mean_arr[ss_match_wave]
bfi_left_half_ccd_mean = bfi_response_struct.img_left_half_ccd_mean_arr[ss_match_wave]
bfi_right_half_ccd_mean = bfi_response_struct.img_right_half_ccd_mean_arr[ss_match_wave]

tsec = anytim(t)
t_arr_sec = anytim(t_arr)

resp_full = interpol(bfi_full_ccd_mean, t_arr_sec, tsec)
resp_left = interpol(bfi_left_half_ccd_mean, t_arr_sec, tsec)
resp_right = interpol(bfi_right_half_ccd_mean, t_arr_sec, tsec)

if keyword_set(do_plot) then begin
  wdef,0,1024,1024
  !p.multi = [0,1,2]

  norm = max(bfi_full_ccd_mean)
  tit='BFI ' + strupcase(wave) + ' normalized response (smoothed)'
  ytit = 'Normalized Response'
  utplot,  t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN/norm,s_wid), /ynoz, charsize=1.5, linestyle=1, $
    tit=tit, ytit=ytit, /year
;  utplot,  t_arr, smooth(BFI_LEFT_HALF_CCD_MEAN,s_wid), /ynoz, charsize=1.5, linestyle=1, $
;    tit='bfi response (smoothed)'
;    yrange=[6e3,1.2e4], tit='bfi response (smoothed)'
;  outplot, t_arr, smooth(BFI_RIGHT_HALF_CCD_MEAN,s_wid), linestyle=2
  outplot, t_arr, smooth(BFI_RIGHT_HALF_CCD_MEAN/norm,s_wid), linestyle=2
  ssw_legend, ['left CCD ...............', 'right CCD -----'], charsize=2, /top, /right
  if n_elements(t) eq 1 then begin
    t_plot = [t,t]
    resp_left_plot = [resp_left, resp_left]
    resp_right_plot = [resp_right, resp_right]
  endif else begin
    t_plot = [t,t]
    resp_left_plot = resp_left
    resp_right_plot = resp_right
  endelse
  outplot, t_plot, resp_left_plot/norm,  psym=2
  outplot, t_plot, resp_right_plot/norm, psym=4
STOP
  tit='BFI ' + strupcase(wave) + ' right/left CCD response ratio (smoothed)'
  ytit = 'Ratio'
  utplot,  t_arr, $
    smooth(smooth(bfi_right_half_ccd_mean/norm,s_wid) / smooth(bfi_left_half_ccd_mean/norm,s_wid),s_wid), $
    yrange=[0.96,1.02], /ystyle, charsize=1.5, psym=3, tit=tit, ytit=ytit, /year

  !p.multi = 0
endif

case 1 of
  keyword_set(left):  out = resp_left
  keyword_set(right): out = resp_right
  keyword_set(all):   out = transpose([[resp_full], [resp_left], [resp_right]])
  else: out = resp_full
endcase

return, out

end

