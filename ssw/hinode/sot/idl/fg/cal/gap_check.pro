
function gap_check, t_arr, gap_cat=gap_cat, _extra=_extra

if not exist(dir_top) then begin
  if not exist(filnam_db) then filnam_db = 'hinode_sdo_alignment' ; _20160411'
  if get_logenv('SOT_CORR_DB') eq '' then $
     set_logenv, 'SOT_CORR_DB', concat_dir(concat_dir(get_logenv('SSWDB'), 'hinode/gen'), $
                                           filnam_db)
  dir_top = get_logenv('SOT_CORR_DB')
endif

;gap_cat = rd_gap_cat()
restore, concat_dir(get_logenv('SOT_CORR_DB'), 'gap_cat.sav')
if not exist(gap_cat) then STOP, 'Cannot find gap file.'

t_sec = anytim(t_arr)
t_sec_min = min(t_sec)
t_sec_max = max(t_sec)

sttim_gap_sec = anytim(gap_cat.sttim_gap)
entim_gap_sec = anytim(gap_cat.entim_gap)

ss_sttim_last = max(where(sttim_gap_sec lt t_sec_min, n_sttim_earlier), ss_where_sttim_lt)
sttim_sec_last = sttim_gap_sec[ss_sttim_last]

ss_entim_last = max(where(entim_gap_sec lt t_sec_min, n_entim_earlier), ss_where_entim_lt)
entim_sec_last = entim_gap_sec[ss_entim_last]

ss_sttim_next = min(where(sttim_gap_sec gt t_sec_min, n_sttim_later  ), ss_where_sttim_gt)
sttim_sec_next = sttim_gap_sec[ss_sttim_next]

ss_entim_next = min(where(sttim_gap_sec gt t_sec_min, n_sttim_later  ), ss_where_entim_gt)
entim_sec_next = entim_gap_sec[ss_sttim_next]

if ( (ss_sttim_last ne ss_entim_last) or $
     (ss_sttim_next ne ss_entim_next) or $
     ( (sttim_sec_next gt t_sec_min) and (entim_sec_next lt t_sec_max) ) ) then $
   gap_flag = 1 else gap_flag = 0
;STOP
return, gap_flag

end
