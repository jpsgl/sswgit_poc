
function poly_func, x, a

;+
; function poly_func, x, a
;
; y = a[0] + a[1]*x + a[2]*x^2 + a[3]*x^3
;-

if not exist(a) then begin
  restore, '/Users/slater/soft/idl/idl_startup/sot/coeff_fit_sot_delta_scalefac.sav'
  a = reform(coeff_fit_ds)
endif

y = a[0] + a[1]*x + a[2]*x^2 + a[3]*x^3

return, y

end
