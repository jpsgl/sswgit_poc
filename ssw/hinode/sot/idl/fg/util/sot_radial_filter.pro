FUNCTION sot_radial_filter, image, x0, y0, r0, r1, step0, step1, $
                            DELTA1=delta1, DELTA2=delta2,$
                            FPLOT=fplot, QUIET=quiet
;+
; NAME:    SOT_RADIAL_FILTER.PRO
;
; PURPOSE: 
;       Return a two-step "radial density filter" that can be applied
;       to limb images in order to balance the intensities of the
;       disk, spicules, and prominences.
;
;
; SAMPLE CALLING SEQUENCE: 
;      IDL> rfilt = sot_radial_filter(image, -5262, -5823, 8624, 8700, 0.03, 0.5)
;      IDL> filtered_image = image*rfilt
;      IDL> TVSCL, filtered_image
;
; INPUTS:
;      IMAGE = 2-D image of the solar limb. Program was developed for SOT
;         images of the solar limb which are sub-fields of the disk. The
;         program has not been tested on full-disk images.
;      X0 = position of disk center x-coord in IMAGE pixels, ref to (0,0)
;         of image.
;      Y0 = position of disk center y-coord in IMAGE pixels, ref to (0,0)
;         of image. 
;
;   The reason we require X0, Y0 rather than using the FITS header
;   keywords XCEN and YCEN is that these values can be in error due to
;   correlation tracker drift and/or S/C rotation (by as much as 1
;   degree in some cases). Thus the user must determine an accurate solar
;   disk center position by trial and error prior to calling this
;   routine.  Note that X0 and Y0 will be NEGATIVE numbers in this
;   coordinate definition. 
;
;       R0 = radius of solar disk in IMAGE pixels.
;       R1 = radius of top of spicules in pixels.
;       STEP0 = factor to multiply disk intensity in order to match
;         prominence intensity. Usually about 0.03 for Ca II and 0.1 for
;         H-alpha SOT images of the solar limb. 
;       STEP1 = factor to multiply spicule intensity in order to match
;         prominence intensity. Usual range is 0.1--0.5.
;
; OUTPUTS:
;       FILT = 2-D floating point array with values in range from 0.0
;          to 1.0. 
; 
; OPTIONAL KEYWORD INPUTS:
;       DELTA1 = width factor for first ERF step function. Default value = 5
;         pixels.
;       DELTA2 = width factor for second ERF step function. Default value =
;         20 pixels. 
;    These factors determine the "smoothness" of the step transition from
;    one density factor to the next. Smaller values result in sharper

;    transitions between levels; larger values "feather" the transition
;    more and result in less obvious filtering of the limb images. 
;       QUIET: if set, suppress all status messages.
;
; OPTIONAL KEYWORD OUTPUTS:
;       RADIUS_IMAGE = a 2-D image of same dimensions as IMAGE in which each
;         pixel's value is the radius (in pixels) from solar disk center. 
;       FPLOT: if set, a sample radial cut is plotted to show the filter
;         profile.
;
; RESTRICTIONS:
;       None.
;
; MODIFICATION HISTORY:
;
;v1.0 Written 15-April-2008, T.E.Berger (TEB) LMSAL.
;-

progver = 'V1.0'
prognam = 'SOT_RADIAL_FILTER'

time0 = SYSTIME(1)

quiet = 1
loud = 1 - KEYWORD_SET(quiet)
if (loud) then MESSAGE,/INFO, 'Start  '+prognam+' '+progver

if KEYWORD_SET(delta1) then d1 = delta1 else d1 = 5
if KEYWORD_SET(delta2) then d2 = delta2 else d2 = 20

sz = SIZE(image)
xs = sz[1]
ys = sz[2]
xarr =  FINDGEN(xs)#REPLICATE(1,ys) - x0
yarr = REPLICATE(1,xs)#FINDGEN(ys) - y0
radius_image = SQRT( xarr^2.0 + yarr^2.0 )

filt = step0 + step1/2*(1+ERRORF((radius_image - r0)/d1)) + (1-step0-step1)/2*(1+ERRORF((radius_image - r1)/d2))

if KEYWORD_SET(fplot) then begin
   m = ABS(FLOAT(y0))/(ABS(x0) + 1.0e-05)
   x1 = xs
   y1 = ROUND(x1*m)
   if y1 gt ys then begin
      y1 = ys
      x1 = ROUND(y1/m)
   end
   rvals = INDGEN( ROUND(SQRT(x1^2.+y1^2.)) )
   PLOT,filt[rvals],xtitle='Radius, pixels', ytitle='Normalized value'
   icut = image[rvals]
   OPLOT,icut/MAX(icut),lines=1
   XYOUTS,0.2,0.2,'Solid = radial filter',/norm
   XYOUTS,0.2,0.175,'Dashed = image',/norm
end

time1 = SYSTIME(1)
if (loud) then MESSAGE,/INFO, 'Total time for '+prognam+' '+progver+' = '+ (time1-time0)+' seconds.'

RETURN,filt
END
