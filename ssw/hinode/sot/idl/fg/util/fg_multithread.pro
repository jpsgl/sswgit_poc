pro fg_multithread, recover=recover
;
;+
;   Name: fg_multithread
;
;   Purpose: setup for beta test of Thomas Strause fg_prep_batch
;
;   Input Parameters:
;      NONE
;
;   Keyword Paramters
;      recover (switch)
;
mtpath=concat_dir('$SSW_SOT','multithread')

files=['fg_prep','make_str'] ; list of beta routines

if keyword_set(recover) then remove_path,mtpath else $
   ssw_path,mtpath,/prepend

recompile,files

return
end


