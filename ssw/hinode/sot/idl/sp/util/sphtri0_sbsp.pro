;-----------------------------------------------------------------------------
;
;	subroutine:  sphtri0_sbsp
;
;	purpose:  Transform a unit vector between two spherical reference
;		  points.  (Same effect as sphtri but does not calculate
;		  parallactic angle).
;
;	reference:  Fundamental spherical trigonometry equations from Smart:
;		    Text-Book on Spherical Astronomy, eq. A, B, C, and D.
;
;	authors:  kcjones@sunspot.noao.edu (Phil Wiborg),   paul@ncar
;
;-----------------------------------------------------------------------------
pro sphtri0_sbsp, olat, b, c, x, y 
;
;	usage:	call sphtri0_sbsp( olat,  az0, el0,  az1, el1 )
;
;		Transform a unit vector between two spherical
;		reference points.
;
;		Definition:
;
;			sct0	~ The great circle sector that
;				  connects the two reference points
;				  on the sphere
;
;		Input Arguments (radians):
;
;			olat	- !pi/2.-abs(sct0)
;			az0	- Azimuth about first reference
;				- point, from sector sct0
;			el0	- Elevation at first reference
;				  point, positive away from surface
;
;		Output Arguments (radians):
;
;			az1	- Azimuth about second reference
;				  point, from sector sct0
;			el1	- Elevation at second reference
;				  point, positive away from surface
;
;		Note: If az0 is positive CCW, the az1 is positive CCW
;		      If az0 is positive  CW, the az1 is positive  CW
;
;	The routine can be used to convert between coordinate
;	systems as follows:
;
;	(1) Let: az0 = hour angle      (2) Let: az0 = azimuth
;	         el0 = declination              el0 = elevation
;	        olat = earth latitude          olat = earth latitude
;
;	   Then: az1 = azimuth            Then: az1 = hour angle
;	         el1 = elevation                el1 = declination
;
;ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

	sina = sin(olat)
	cosa = cos(olat)

	sinb = sin(b)
	cosb = cos(b)
	sinc = sin(c)
	cosc = cos(c)

	tmp1 = cosc*cosb
	tmp2 = sinc*cosa
	sinx = -cosc*sinb
	cosx = tmp2-tmp1*sina
	;siny = amax1(-1.,amin1(1.,sinc*sina+tmp1*cosa))
	siny = max([-1.,min([1.,sinc*sina+tmp1*cosa])])

	x    = atan(sinx,cosx)
	y    = asin(siny)

	return
	end
