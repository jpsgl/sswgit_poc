pro solareph_sbsp,iy,im,id,utimed,ras,dcs,gsdt,b0ang,peeang,ctrlong,r0

;  routine to compute the solar ephemeris values give the time input
;  coordinate in arcseconds from sun center to solar latitude and longitude

;       INPUTS:
;
;                    iy = year (e.g., 2002)
;                    im = month (1-12)
;                    id = day of month
;                 utimed = universal time of day (hours).
;
;
;       OUTPUTS:
;
;                   ras = right ascension disk center(radians).
;                   dcs = declination disk center(radians).
;                  gsdt = right ascension from central meridian(radians).
;                 b0ang = b0-angle(radians).
;                peeang = p-angle(radians).
;               ctrlong = carrington longitude disk center(radians).
;                    r0 = solar radius (arcsecs).

        djd = 0.d

	iy = long(iy)
	im = long(im)
	id = long(id)


;       --<Julian day that starts at 12:00 ut on the given date.
        julian1200_sbsp, iy, im, id, jd 

;       --<Set Julian day in continuous form.
	utimed = double(utimed)
        djd = 1.d*jd-.5+utimed/24.d

;       --<Solar coordinates:
;       --<  b0ang:  b0-angle
;       --< peeang:  p-angle
;       --<     r0:  radius in arcseconds
        solcor_sbsp, djd, ras, dcs, gsdt, b0ang, peeang, ctrlong, r0 

return
end
