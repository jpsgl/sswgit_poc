pro cormax_sbsp, fftera, ffterb, nsearch, sh, vig=vig
;+
;
;	procedure:  cormax
;
;	purpose:  find the maximum of a cross correlation function
;
;	author:  lites@ncar, 4/93	(minor mod's by rob@ncar)
;
;  History:
;      23-Nov-2006 - Bruce Lites - adapted from ASP routine cormax.pro
;
;==============================================================================
;
;	Check number of arguments.
;
if n_params() ne 4 then begin
	print
	print, "usage:  cormax, fftera, ffterb, nsearch, sh"
	print
	print, "	Arguments"
	print, "		fftera,	- input Fourier transforms of 1st"
	print, "		ffterb	  and 2nd vectors to be shifted"
	print, "		nsearch	- number of pixels either side to"
	print, "			  search for maximum"
	print, "		sh	- pixel shift of 2nd vector with"
	print, "			  respect to first"
	print
	print, "	Keywords"
	print, "		vig	- approximate vignetting shift "
	print
	return
endif
;-

;  get dimensions of transform arrays (assumed to be the same for both)
ntot = sizeof_sbsp(fftera, 1)

;  multiply: (a-transform) X (conjugate of b-transform)
ccor = fftera*conj(ffterb)

;  transform back to real space
corr = float(fft(ccor,1))

;  find local maximum near zero shift.  Search +-nsearch pixels either direction
;  if keyword vig is set use restricted range for search +/-2 pixels
xx = indgen(nsearch*2)
temp = fltarr(2*nsearch)
temp(0:nsearch-1) = corr(ntot-nsearch:ntot-1)
temp(nsearch:2*nsearch-1)=corr(0:nsearch-1)
;plot,xx,temp
if keyword_set(vig) then begin
	tempmin = min(temp)
	temp(0:nsearch+vig-4) = tempmin
	temp(nsearch+vig+4:*) = tempmin
endif
amax = max(temp,imx)

;  fit parabola to 3 interpolated pixels around maximum position
;  ensure that we are not at endpoints
if imx eq 0 then imx = 1
if imx eq nsearch*2-1 then imx = nsearch*2-2

;  use direct parabola fit
parabofit,xx(imx-1:imx+1),temp(imx-1:imx+1),sh

;oplot,xx,temp,linestyle=2

sh = (sh-nsearch)

return
end
