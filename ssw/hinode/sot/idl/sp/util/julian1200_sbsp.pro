        pro julian1200_sbsp, iy, month, iday, jul
;       julian day number at 0 h ut=0.5+julian(iday,month,iyr).
        md = [0,31,59,90,120,151,181,212,243,273,304,334]
	md = long(md)
;
        iyr  = iy
;        if(iyr lt  50) then iyr = iyr+2000
;        if(iyr le 100) then iyr = iyr+1900

        leap = 0
        jyr  = iyr-1600
        i1   = jyr/400
        i2   = (jyr-400*i1)/100
        i3   = (jyr-400*i1-100*i2)/4
        jul  = 2305448+365*jyr+97*i1+24*i2+i3
        if (jyr mod 4) eq 0 then   leap = 1
        if (jyr mod 100) eq 0 then leap = 0
        if (jyr mod 400) eq 0 then leap = 1
        jul  = jul+md(month-1)+iday
        if(month le 2) then jul = jul-leap
;       leap=1 in leap years.

        return
        end
