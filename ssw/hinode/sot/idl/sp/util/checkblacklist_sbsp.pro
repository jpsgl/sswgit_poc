function checkblacklist_sbsp, index, dat
	bad = checkblacklist_time_sbsp(index.date_obs)
	if bad lt 0 then begin
		box_message, 'Falling back on heuristics for dropout detection'
		bad = checkdropout_sbsp(index,dat)
	endif

	sidebad = fltarr(index.num_side,4)
	for iside=0,index.num_side-1 do for istk=0,3 do $
		sidebad[iside,istk] = (bad AND 2b^(istk+4*iside)) ne 0

	return, sidebad
end
