function corshft_sbsp, line1, line2, nsrch=nsrch, noslope=noslope
;+
;
;  function:  corshft_sbsp
;
;  purpose:  find the shift between line 1 and line 2 by correlation
;	     sub-pixel accuracy with polynomial interpolation
;	     adapted to do cross-correlation, not just difference
;	     tries only +/- 4 pixel shifts.  Result is the amount
;	     line2 is shifted with respect to line1, not the correction
;	     shift which should be applied, which is the negative of the
;	     result.
;	     Modified 3/06 from DLSP routine for Solar-B
;	     Modified 4/05 by lites from corshft.pro to do direct extraction
;	     of parabola coefficients instead of using poly_fit.pro
;	     Modified 11/14 by Lites from corshft_sbsp.pro to have optional
;	     input search range nsearch
;
;==============================================================================


if n_params() ne 2 then begin
	print
	print, "usage:  ret = corshft_sbsp(line1,line2)"
	print
	print, "	Find the shift between line 1 and line 2 by"
	print, "	correlation sub-pixel accuracy with polynomial"
	print, "	interpolation adapted to do cross-correlation, not"
	print, "	just difference, tries only +/- nsearch/2 pixel shifts."
	print, "	Result is the amount line2 is shifted with respect"
	print, "	to line1, not the correction shift which should be"
	print, "	applied, which is the negative of the result."


	print, "KEYWORDS: "
	print, "	nsrch 	= search limit. Default is nsearch = 15 "
	print, "	noslope	= if set, then do not remove linear slope "
	print, "		  before correlating "


	print
	return, 0
endif
;-
;

;  no search range keyword
;  THIS IS A HARDWIRED DEFAULT PARAMETER!!!
nsearch = 15
nsr = nsearch
if nsr mod 2 ne 1 then nsr = nsr+1
nsr2 = nsr/2

;  with specified search range keyword
if keyword_set(nsrch) then begin
	nsr = nsrch
	if nsr mod 2 ne 1 then nsr = nsr+1
	nsr2 = nsr/2
endif
corrvec = fltarr(nsr)
dimen = size(line1)
dimen2 = size(line2)

if keyword_set(noslope) then begin
; for SBSP try just removing the mean instead
	var1 = line1 - mean_sbsp(line1)
	var2 = line2 - mean_sbsp(line2)
endif else begin
;	Remove linear trend of vectors.
	x1 = indgen(dimen(1))
	coef1 = linfit(x1, line1)
	alin1 = coef1(0) + x1*coef1(1)
	var1 = line1 - alin1
	x2 = indgen(dimen2(1))
	coef2 = linfit(x2, line2)
	alin2 = coef2(0) + x2*coef2(1)
	var2 = line2 - alin2
endelse

;  find cross-correlation by shift, multiplication
for i = -nsr2,nsr2 do corrvec(nsr2+i) = total(var1(nsr2:dimen(1)-nsr2-1)*  $
				var2(nsr2+i:dimen2(1)-nsr2-1+i))
;
;	Find maximum location of cross-correlation within 3 pixels of center;
;	avoid first and last points to allow for correct interpolation.
;
amax = max(corrvec(1:nsr-2), imax)
;
;	Correct index for dimension of corrvec.
;
imax = imax + 1
;
;	Fit parabola about this point.
;
xx = indgen(nsr)

;  fitting parabola of three points is equivalent to inverting a 3x3 matrix
;  to get the 2nd order polynomial coefficients yy = a + b x + c x^2
parabofit,xx(imax-1:imax+1),corrvec(imax-1:imax+1),sh
;coeff = fltarr(3)
;yy = corrvec(imax-1:imax+1)
;xx = xx(imax-1:imax+1)
;cc = fltarr(3,3)
;cc(0,*) = 1.
;for kk = 0,2 do cc(1,kk) = xx(kk)
;for kk = 0,2 do	cc(2,kk) = xx(kk)*xx(kk)
;cc = invert(cc,status,/double)
;for kk = 0,2 do coeff(kk) = total(cc(*,kk)*yy(*))



;
;return, -coeff(1) / 2.0 / coeff(2) - float(nsr2)
return,sh - float(nsr2)
end
