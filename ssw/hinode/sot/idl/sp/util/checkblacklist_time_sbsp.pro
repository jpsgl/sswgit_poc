function checkblacklist_time_sbsp, time
	; check blacklist, return -1 in case of problems

	blfile = get_logenv('$SOT_SP_CALIBRATION')+'/blacklist.geny'
	if not file_exist(blfile) then begin
		box_message, 'Cannot find blacklist file under ' + $
			get_logenv('$SOT_SP_CALIBRATION')
		return, -1
	endif else begin
		restgenx,file=blfile[0],blacklist
		time_tai = anytim2tai(time)
		black_tai = anytim2tai(blacklist.time)
		; check that blacklist is more recent than data
		if max(black_tai) lt max(time_tai) then begin
			box_message, 'Blacklist is older than data'
			return, -1
		endif else begin
			bad = bytarr(n_elements(time))
			; reduce blacklist size to ROI for speed
			roi = where(black_tai ge min(time_tai) and black_tai le max(time_tai), nroi)
			if nroi ne 0 then begin
				blacklist = blacklist[roi]
				for t=0,n_elements(time)-1 do begin
					pos = where(strcmp(blacklist.time, time[t]), inblacklist)
					if inblacklist ne 0 then bad[t] = blacklist[pos].bad
				endfor
			endif
		endelse
	endelse

	if n_elements(time) eq 1 then bad = bad[0]
	return, bad
end
