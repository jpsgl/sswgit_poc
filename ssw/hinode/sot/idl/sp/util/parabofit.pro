pro parabofit,xx,yy,sh

;  routine to fit a parabola to three data points, then
;  output the extremum position

;  INPUTS:	xx	= independent variable of 3 points
;		yy	= dependent variable of 3 points

;  OUTPUT:      sh	= xx position of extremum


;  fitting parabola of three points is equivalent to inverting a 3x3 matrix
;  to get the 2nd order polynomial coefficients yy = a + b x + c x^2
coeff = fltarr(3)
cc = fltarr(3,3)
cc(0,*) = 1.
for kk = 0,2 do cc(1,kk) = xx(kk)
for kk = 0,2 do cc(2,kk) = xx(kk)*xx(kk)
cc = invert(cc,status,/double)
for kk = 0,2 do coeff(kk) = total(cc(*,kk)*yy(*))
sh = -coeff(1)/(2.*coeff(2))

return
end
