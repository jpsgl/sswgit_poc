pro vigshift_sbsp,stksi,ssum,vigsh


;  Routine to estimate crudely the empirical thermal drift along
;  the slit from the vignetting seen most clearly in CCDSIDE1.
;
;  This routine finds the edge (near the top of the raw CCD image)
;  to the nearest pixel based on the gradient that is at least
;  0.4 times the median value in the vicinity of the top of the
;  raw level0 image for CCDSIDE1.  If no such edge is found, then
;  this routine returns the value -2000

;  INPUTS:	stksi	= Stokes I raw level0 image for CCDSIDE1
;			  This has had wraparound corrected and is
;			  restored from bit shift, if present
;			  Slit must include the very top of the
;			  raw CCD image!
;		ssum	= flag for summing along slit
;
;  OUTPUT:	vigsh	= integer approximation to shift, pixels

nsl = sizeof_sbsp(stksi,2)
sltv = fltarr(nsl)
vigsh = -2000

; find sharp decrease in intensity at top of CCDSIDE1
;  get continuum variation along the slit
	for isl = 0,nsl-1 do sltv(isl) = mean_sbsp(stksi(1:10,isl))
;  search for sharp gradient at top 20 pixels, ignore top row which is
;  bad
	temm = sltv(nsl-21:nsl-2)
	for jj = 1,19 do begin
		delt = temm(jj)-temm(jj-1)
;print,'vigshift: -delt,limit: ',-delt,0.25*abs(max(temm))
		if -delt gt 0.25*abs(max(temm)) then begin
			iedge = jj
			goto, kilroy
		endif
	endfor
	return

	kilroy: vigsh = 20 - iedge
	if ssum eq 2 then vigsh = 21 - iedge


return
end
