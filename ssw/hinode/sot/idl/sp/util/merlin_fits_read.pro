function merlin_fits_read, file
	tmp = mrdfits(file, 0, header)
	index = fitshead2struct(header)
	output = add_tag(output, index, 'HEADER')
	for i = 1, 42 do begin
		tmp_data = mrdfits(file, i, tmp_header)
		name = fxpar(tmp_header, 'EXTNAME')
		name = strcompress(name, /rem)
		output = add_tag(output, tmp_data, name)
	endfor

	return, output
end
