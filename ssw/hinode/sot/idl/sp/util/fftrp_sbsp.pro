pro fftrp_sbsp, dat, nst, nend, fftr, avg
;+
;
;	procedure:  fftrp
;
;	purpose:  Obtain Fourier transform of data.  Make smooth transition
;		  from beginning to end, remove mean, and kill frequencies with
;		  rgb variation.
;
;	author:  lites@ncar, 4/93	(minor mod's by rob@ncar)
;
;  History:
;      23-Nov-2006 - Bruce Lites, modified from ASP routine

;
;==============================================================================
;
;	Check number of arguments.
;
if n_params() ne 5 then begin
	print
	print, "usage:  fftrp, dat, nst, nend, fftr, avg"
	print
	print, "	Obtain Fourier transform of data"
	print, "	(make smooth transition from beginning to end, remove"
	print, "	mean, and kill frequencies with rgb variation)."
	print
	print, "	Arguments"
	print, "		dat	- input space data"
	print, "		nst,	- start and end pixels for"
	print, "		 nend	  interpolation"
	print, "		fftr	- output interpolated array,"
	print, "			  Fourier space:  complex(1024)"
	print, "		avg	- output mean of array from nst to"
	print, "			  nend which has been subtracted"
	print, "			  from dat"
	print
	return
endif
;-

;  get dimensions of array
nx = sizeof_sbsp(dat, 1)
nx1 = nx-1

;  size output array
;!!HARDWIRED for HINODE SBSP
npt = 1024

;  always interpolate from an array 1024 points long
nmask = 1024-(nend-nst+1) 
if nmask lt 20 then message, $
	'no. pts '+ stringit(nmask) + ' is too close to 1024 limit'

;  define the interpolated array
temp = fltarr(1024)

;  extend the array so that there is smooth transition to symmetry
temp(nst:nend) = dat(nst:nend)
temp = extend_sbsp(temp,nst,nend)

;  remove mean from array
avg = mean_sbsp(temp)
temp = temp-avg

;  fft the extended data
fftr = fft(temp,-1)

;  get power spectrum
; pspec = (abs(fftr))^2
; pspec = alog10(pspec)
; stop

return
end
