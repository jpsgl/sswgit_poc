pro process_dkgn,darkdir,flatdir,roistart,xctr,yctr,skipex=skipex

;  Modified for 2016 data to only process non-redundant dark files.  Darks
;  must be paired with corresponding flats, and in 2016 there are often
;  several flats per dark

;  Routine to process a pair of dark and flat observations taken during eclipse 
;  season.  The philosophy is that the data taken closely adjacent in time will have
;  more appropriate dark levels for this processing.  Data are presumed to 
;  be taken with the same spectral roi

;  INPUTS:  
;	darkdir	= array of directories containing raw dark fits files
;	flatdir = array of directories containing raw flat fits files
;	roistart = array of pixel start values for spectral ROI
;	xctr,yctr = arrays of x,y centers of flat field data
;	skipex = keyword, if set, then skip examination of images and
;			use stored values

;  determine the number of pairs to process
npair = n_elements(darkdir)

skewname = 'null'

;  begin loop over pairs of dark/flat observations
for kk = 0,npair-1 do begin

;  average the dark data
	print,'BEGIN PROCESSING DARKS'
;  mod to check for redundant dark
	if kk gt 0 and darkdir(kk) eq darkdir(kk-1) then goto,jump555
		avg_darkdata,darkdir(kk),roistart(kk),skipexam=skipex
;  determine file name of save file of good darks
;  get the string representation of the start pixel of the ROI
		rstrt = strcompress(string(roistart(kk)),/remove_all)
;  get the time ID for the filename
		ppos = strpos(darkdir(kk),'/20')
		plgth = strlen(darkdir(kk))-ppos - 2
		timid = strmid(darkdir(kk),ppos+1,plgth)
;  generate the output filename for the selected files for averaging
		dkfilnam = 'goodarks_' + rstrt + '_' + timid + '.save'
	jump555:
	print,'END PROCESSING DARKS'


; now determine the good flats
	print,'BEGIN FINDING GOOD FLATS'
	select_flats,flatdir(kk),roistart(kk),skipexam=skipex
;  determine file name of save file of good darks
;  get the string representation of the start pixel of the ROI
;  get the time ID for the filename
	ppos = strpos(flatdir(kk),'/20')
	plgth = strlen(flatdir(kk))-ppos - 2
	timid = strmid(flatdir(kk),ppos+1,plgth)
;  generate the output filename for the selected files for averaging
	flatfilnam = 'goodflats_' + rstrt + '_' + timid + '.save'
	print,'END FINDING GOOD FLATS'

;  Spectral skew is computed and averaged for all ROI56 files.  Usually
;  this routine is run on those files first.  If not then we ask for an
;  input of the appropriate file name.  If this is a fast-map processing,
;  must also enter the skewname manually for a valid normal map ROI56
;  run
	if kk eq 0 then begin
		restore,flatfilnam
		datt = readfits(derf(0),hdr)
		ssum = sxpar(hdr,'CAMSSUM')
		if roistart(kk) ne 56 then begin
			print,'First pair processed is non-standard spectral ROI (.ne.56).'
			print,'Must enter a previously computed, valid save-file for spectral skew.'
			print,'Enter path name of save file like: shftavg_20100516_1721_roi56.save'
			read,'Enter Here: ',skewname
			print,'file path for skew data is: ',skewname
		endif
		if ssum eq 2 then begin
			print,'data is FAST MAP. Must enter a previously computed, valid '
			print,'save-file for spectral skew from a normal map.'
			print,'Enter path name of save file like: shftavg_20100516_1721_roi56.save'
			read,'Enter Here: ',skewname
			print,'file path for skew data is: ',skewname
		endif
	endif

;  now process flats
	print,'BEGIN PROCESSING FLATS'
	flatgen3_sbsp,flatdir(kk),xctr(kk),yctr(kk),dkfilnam,flatfilnam, $
		roistart(kk),skewname

	dumy = ''
	print,'Finished processing flats at ',timid
	read,'Close display windows and type any character to continue: ',dumy
	

;  end loop over obs. pairs
endfor

end
