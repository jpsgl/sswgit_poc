pro getdark_sbsp,hdr,darkavg_std,darkavg_fast,dark,l1index=l1index

;  Routine to select and output the appropriate dark image for SP
;  observations based on header information.
;
;  Routine to compute a dark image based on header information
;  For operational version, this routine will use temperatures, header
;  information on gain, summing etc to select the proper dark
;  image.  The output image, dark, will cover the entire images
;  for the spectral ROI selected, for each CCDSIDE.  It will be
;  binned accordingly if summing is specified, i.e. if summing
;  two pixels, the array will be 512 pixels in the serial direction
;  instead of 1024.


;  INPUTS:
;       hdr             = FITS Level0 header for the SP data being
;                         processed
;       darkavg_std     = dark image for standard map
;       darkavg_fast    = dark image for fast map
;	l1index		= header structure, updates history

;  OUTPUTS:
;       dark            = properly scaled dark image
;                         [112,(obs. number of slit positions),2(each 
;                         CCDSIDE)]

;
;  History:
;      6-Oct-2006 - Bruce Lites
;     16-Oct-2006 - S.L.Freeland - sswify, use $SOT_SP_CALIBRATION environemental
;                   restgenx in place of 'restore'
;     23-Oct-2006 - Bruce Lites - modified from getgain_sbsp.pro to reflec
;                   dark image production
;     24-May-2007 - S.L.Freeland - changed environmental -> SOT_SP_CALIBRATION
;      3-Oct-2007 - T. Tarbell - added code to handle CAMDACA, CAMDACB = 5
;     17-Oct-2007 - T. Tarbell - accurate deltas for DAC 5 fast map mode
;     22-Oct-2007 - T. Tarbell - interpolation in time for gradual increase of dark levels
version=1.00	; This version updated recently for long-term temporal drifts
;		  of the dark, and for allowing CAMDACA,B = 5 or 7 (see above)

if data_chk(l1index,/struct) then $
        update_history,l1index,/caller,version=version; report VERSION->.HISTORY


;  THE BASIC DATA ARE READ IN IN SP_PREP.PRO, AND PASSED TO CALIB_SBSP.PRO,
;  THEN TO THIS ROUTINE FOR CUSTOMIZATION TO SPECIFIC OBSERVATIONS.  Could be
;  modified in the future to compensate for small thermal variations of the
;  dark offset



;  get the number of pixels binned along the slit (along serial direction)
        ssum = sxpar(hdr,'CAMSSUM')

;  Range of pixels along the slit
        spccdix0 = sxpar(hdr,'SPCCDIX0')
        spccdix1 = sxpar(hdr,'SPCCDIX1')
;  Range of spectral pixels
        spccdiy0 = sxpar(hdr,'SPCCDIY0')
;        spccdiy1 = sxpar(hdr,'SPCCDIY1')
;  get other variables
        exptime = sxpar(hdr,'EXPTIME')
        camamp = sxpar(hdr,'CAMAMP')
        camgain = sxpar(hdr,'CAMGAIN')
        scnsum = sxpar(hdr,'SCN_SUM')
        camdaca = sxpar(hdr,'CAMDACA')
        camdacb = sxpar(hdr,'CAMDACB')
        dateobs = sxpar(hdr,'DATE_OBS')

;  test to see if the current observation fits with the expected parameters
;  more options will be needed if these parameters change
        ;if spccdiy0 ne 56 then begin
        ;  print,'in getdark_sbsp, data file has non-standard spectral ROI: ',spccdiy0 
	;  print,'this is now a non-fatal error, program continues'
        ;endif
        if camamp ne 1 then begin
          print,'in getdark_sbsp, data file has non-standard camera amplifier: ',camamp
          stop
        endif
        if camgain ne 2 then begin
          print,'in getdark_sbsp, data file has non-standard camera gain: ',camgain
          stop
        endif
        if (camdaca ne 5) and (camdaca ne 7) then begin
          print,'in getdark_sbsp, data file has non-standard camera DAC A offset ',camdaca
          stop
        endif
       if (camdacb ne 5) and (camdacb ne 7) then begin
          print,'in getdark_sbsp, data file has non-standard camera DAC B offset ',camdacb
          stop
        endif

; find the time difference in days and change in dark level since the Oct 2006 darks were taken
; change in time from least square fits of std mode (DAC 7) darks only between Oct 2006 & 2007 eclipses
	tjd = anytim2jd([dateobs,'23-Oct-2006 12:00'])
	delta_t = float(tjd(0).int-tjd(1).int)+tjd(0).frac
	delta_d = delta_t * [0.77, 0.53]

;  disable linear increase of time dependence after 20071117:  we note that the
;  dark levels in late November 2007 have reverted to their values near launch
;  as determined from off-limb measurements.  This <<might>> be due to the
;  resetting of heaters around November 17, 2007 at 12:00 (this time
;  is not during an SP map, so there will not be a change in the
;  middle of a map), so just use this as a cutoff date for the time being
	;trevert = anytim2jd('17-Nov-2007 12:00')
	;if trevert.int - tjd(0).int lt 0 then delta_d(*) = 0.
;  for now, do not apply time-dependent offsets
	delta_d(*) = 0.


;  for Oct 2006 data, get the current average dark table
        if ssum ne 2 then begin
                dark = darkavg_std
		if camdaca eq 5 then delta_d(0) = delta_d(0) - 8226.
		if camdacb eq 5 then delta_d(1) = delta_d(1) - 8241.
		dark(0,0,0) = dark(*,*,0) + delta_d(0)		
		dark(0,0,1) = dark(*,*,1) + delta_d(1)		
                dix0 = spccdix0
                dix1 = spccdix1
;  adjust the data for the exposure time
                dark = dark*exptime/4.8
        endif else begin
;  for serial summed data, get appropriate dark
;  in case of summing, get actual range of measured values along slit
                dark = darkavg_fast
;  dark correction derived from standard maps of 4.8 sec integration
;  This fast map dark average had both scan and serial summing, and
;  3.2 sec integration,so the correction should be (3.2*2.*2.)/4.8 = 3.2/1.2
;  need to adjust this correction for scan summing, serial summing
		delta_d = delta_d*3.2/1.2
		;delta_d = delta_d*32./24.	; correct for different exposure times, fast/std
		if camdaca eq 5 then delta_d(0) = delta_d(0) - 10934.
		if camdacb eq 5 then delta_d(1) = delta_d(1) - 10925.
		dark(0,0,0) = dark(*,*,0) + delta_d(0)		
		dark(0,0,1) = dark(*,*,1) + delta_d(1)		
		dix0 = spccdix0/2
                dix1 = spccdix1/2
;;  adjust the data for the exposure time, serial summing, scan summing
;  adjust the data for the exposure time, scan summing only.  The presumption
;  is that the offsets are applied in the read process, not the serial summing.
;  There will be some dark level that adds in the serial summing, but since the
;  offsets of the CAMDACA,B are determined empirically and independently for fast
;  and normal maps, can ignore the serial summing in this case.
;  this particular dark average had both scan summing and serial summing
;  unlike fast maps, the darks selected for this average had an exposure 
;  time of 3.2 sec instead of 1.6 seconds!
;                dark = dark*scnsum*ssum*exptime/(2.*2.*3.2) ; old version
                dark = dark*scnsum*exptime/(2.*3.2)
        endelse

;  Adjust dark for the selected image properties
        dark = dark(*,dix0:dix1,*)

return
end
