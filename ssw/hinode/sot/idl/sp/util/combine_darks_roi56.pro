pro combine_darks_roi56,roi56list

;  routine to read in the average dark images from eclipse season data 
;  as processed by avg_darkdata.pro, then combine to a grand average
;  dark image, adjusted so as to have the dark level corresponding to 
;  the CAMDACA/B=7 levels expected by SP_PREP (note that the eclipse
;  season data have CAMDACA/B=5, the present norm

;  !!FOR NOW HARDWIRED FOR STANDARD MAPS, NOT FAST MAPS!!  PRODUCES
;  FAST MAP DARKS FROM THE STANDARD MAP VALUES!

;  INPUTS:
;	roi56list = array of path names to the IDL-save files of lists of good dark
;			images for ROI=56 dark data, one path for each operation considered

;  first find the number of good files in each directory
nelmt56 = n_elements(roi56list) & nf56 = lonarr(nelmt56)

for kk = 0,nelmt56-1 do begin
	restore,roi56list(kk)
	nf56(kk) = n_elements(derf)
endfor


;  the output name for the results is based on the date and time of the 
;  first ROI56 data
ppos = strpos(roi56list(0),'goodarks_56_')
stryear = strmid(roi56list(0),ppos+12,4)
;savnam = 'fulldark_' + strmid(roi56list(0),ppos+12,15)
savnam = 'darks_' + stryear + '_56'


;  get averages of these darks weighted by the respective number of files in each
;  mean for ROI=56
for kk = 0,nelmt56-1 do begin
;  establish array sizes
	restore,roi56list(kk)
	if kk eq 0 then begin
		sz = size(darkavg)
		nxx = sz(1) & nyy = sz(2)
		dk56 = fltarr(nxx,nyy,2)
	endif
	dk56 = dk56 + darkavg*float(nf56(kk))
endfor
dk56 = dk56/float(total(nf56))


;  conversion terms for DAC7 to DAC5
dark56_std = dk56
dark56_fast = dk56
sconv = [8226.,8241.]
fconv = [10934.,10925.]

;  first convert the Std maps to DAC7
for iside = 0,1 do dark56_std(*,*,iside) = dark56_std(*,*,iside) + sconv(iside)

;  NOTE! Calculation to determine the darks for fast maps by rebinning
;  normal map CAMSSUM=1 data.  This is adequate, but the rebinned darks need 
;  to be shifted downward by one pixel to correspond to fast map data.  This
;  is done here, not in 
;  for a fast map without fast map measurements.  When fast maps become available
;  this procedure must change!

;  now convert the fast maps to DAC7
for iside = 0,1 do begin
        dark56_fast(*,*,iside) = (4./3.)*(dark56_std(*,*,iside) - $
                sconv(iside)) + 64.*2.937 + fconv(iside)
endfor
;  rebin the data to the fast map size
;  take into account the empirical -1 pixel shift discovered when comparing
;  data from 2010
dark56_fast = shift(rebin(dark56_fast,nxx,512,2),0,-1,0)


save,filename=savnam+'.save',dark56_std,dark56_fast

;  save the needed data in ssw geny format
savegenx,file=savnam+'.geny',dark56_std,dark56_fast

end
