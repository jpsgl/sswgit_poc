pro sqrtfn_sbsp,xx,amp,value

;  square-root function for non-linear least-squares fitting
;  of low amplitude linear polarization signals

value = amp*sqrt(xx)

return
end
