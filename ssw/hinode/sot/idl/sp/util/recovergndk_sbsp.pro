pro recovergndk_sbsp,fxyear
;  routine to recover the gain and dark data based on the year of
;  the observation

;
; Name: recovergndk_sbsp
;
; Purpose: retrieve all the needed gain and dark image data
;
;
; Output Paramters:
; common sp_prep_com
;
;
; Side Effects:
; Hardwired for specific data
;
; Restrictions:
;
version = 1.0  ; 12-Jan-2010 - B. Lites (above changes)
;  -- modify for extracting the data here based on the year of
;  	operation.  It is a little complex, so contain it all here
;  	instead of here and in sp_prep.pro
version = 2.0  ; 27-Jul-2011 - B. Lites (above changes)
;
;-
;
;	INPUT
;	fxyear	= the year of observation, integer

common sp_prep_com, $
scoef,degn,sltpos,nftot, vdeg,vigcoef, calpos,skx, $ 
;  fast, standard gain tables for default ROI
f_mflat, f_shftsl,f_specrfit,f_specshft,f_svrr, $; gainfile
s_mflat,s_shftsl,s_specrfit,s_specshft,s_svrr, $
;  darks for early mission
darkavg_std,darkavg_fast, $ 
;  eclipse data fast, standard gain tables for full ROI
sr_mflat,sr_shftsl,sr_specrfit,sr_specshft,sr_svrr, $
fr_mflat,fr_shftsl,fr_specrfit,fr_specshft,fr_svrr, $
;  eclipse darks, from full ROI
full_darkavg_std,full_darkavg_fast



;;;;;;;;;;;;;;GAIN TABLES;;;;;;;;;;;;;;;;;;;;;;

;  fast map gaintable, early mission, 2006-2007
gaintbl_fast = ['gaintable_fastmap_20070827.geny']

;  standard normal map gaintables
;  note: the first entry below combines data from 2006-2007
;		so it is assigned year 2006
gaintbl_std = [	'gaintable_stdmap_20070826.geny', $
				'gaintable_20090731_140100_roi56.geny', $
				'gaintable_may2010_roi56.geny', $
				'gaintable_july2011_roi56.geny', $
				'gaintable_july2012_roi56.geny', $
				'gaintable_july2013_roi56.geny', $
				'gaintable_july2014_roi56.geny', $
				'gaintable_may2016_roi56.geny', $
				'gaintable_july2017_roi56.geny' $
;***** add in here gaintables from eclipse season data for following years
				]
;  indices for years where we have standard spectral ROI gaintables
;***** add in here years as the standard gaintables are processed
gn_yearstd = [2006,2009,2010,2011,2012,2013,2014,2016,2017]

;  full-wavelength normal map gaintables
full_gaintbl_std = [	'gaintablefullflat_20090731_140100.geny' ]
;  indices for years where we have full-wavelength gaintables
;***** add in here years as the full-wavelength  gaintables are processed
gn_yearfull = [2009]


;  FIRST, SELECT GAINTABLE DATA FOR FULL SPECTRAL ROI, BOTH
;  NORMAL AND FAST MAPS
;  for full spectral ROI, select the data based on year of observation
ngn = n_elements(gn_yearfull)
for kk = 0,ngn-1 do begin
	deltyear = abs(fxyear - gn_yearfull)
	mmnn = min(deltyear,iyr)
endfor
; restore the NEW FULL SPECTRAL GAIN TABLES for the closest year
gainfile=file_search('$SOT_SP_CALIBRATION',full_gaintbl_std(iyr))
if not file_exist(gainfile) then begin
   box_message,'Cannot find full spectral gaintable for ', $
	   	gn_yearfull(iyr),' under $SOT_SP_CALIBRATION'
   stop
endif
restgenx,file=gainfile(0),mflat,shftsl,specrfit,specshft,svrr
sr_mflat = mflat & sr_shftsl = shftsl
sr_specrfit = specrfit & sr_specshft = specshft & sr_svrr = svrr
;  rebin data for fast map and apply empirical -1 pixel shift
fr_mflat = shift(rebin(sr_mflat,224,512,2),0,-1,0)
fr_specrfit = shift(rebin(sr_specrfit,512),-1)
fr_specshft = shift(rebin(sr_specshft,512),-1)
fr_svrr = shift(rebin(sr_svrr,512,2),-1,0)
fr_shftsl = sr_shftsl/2.


;  NEXT, SELECT GAINTABLE DATA FOR NORMAL (56-167) SPECTRAL ROI,
;  BOTH NORMAL AND FAST MAPS
;  for normal spectral ROI, select the data based on year of observation
ngn = n_elements(gn_yearstd)
for kk = 0,ngn-1 do begin
	deltyear = abs(fxyear - gn_yearstd)
	mmnn = min(deltyear,iyr)
endfor
; restore the NEW FULL SPECTRAL GAIN TABLES for the closest year
gainfile=file_search('$SOT_SP_CALIBRATION',gaintbl_std(iyr))
if not file_exist(gainfile) then begin
   box_message,'Cannot find full spectral gaintable for ', $
	   	gn_yearstd(iyr),' under $SOT_SP_CALIBRATION'
   stop
endif
restgenx,file=gainfile(0),mflat,shftsl,specrfit,specshft,svrr
s_mflat = mflat & s_shftsl = shftsl
s_specrfit = specrfit & s_specshft = specshft & s_svrr = svrr
;  rebin data for fast map
;  take into account the empirical 1-pixel shift discovered when comparing
;  data from 2010
f_mflat = shift(rebin(s_mflat,112,512,2),0,-1,0)
f_specrfit = shift(rebin(s_specrfit,512),-1)
f_specshft = shift(rebin(s_specshft,512),-1)
f_svrr = shift(rebin(s_svrr,512,2),-1,0)
f_shftsl = s_shftsl/2.


;  FOR THE EARLY MISSION, 2006-2007, USE THE EARLY MISSION 
;  FAST MAP SPECIFIC DATA
if (fxyear eq 2006 or fxyear eq 2007) then begin
; restore the FAST MAP early mission flat data only once, then calculate 
; the gain table based on this for each slit position
	gainfile=file_search('$SOT_SP_CALIBRATION',gaintbl_fast)
	if not file_exist(gainfile) then begin
		box_message,'Cannot find fast map gain table file for',$
	   'early mission under $SOT_SP_CALIBRATION'
		stop
	endif
	restgenx,file=gainfile(0),mflat,shftsl,specrfit,specshft,svrr
; store temporary variables to be passed to the processing routine
; and altered there
	f_mflat = mflat & f_shftsl = shftsl
	f_specrfit = specrfit & f_specshft = specshft & f_svrr = svrr
endif



;;;;;;;;;;;;;;DARK IMAGES;;;;;;;;;;;;;;;;;;;;;;

;  normal and fast map darks, early mission, 2006-2007
darktbl_early = ['darktable_gn2_amp1_q0_roi56_20061123.geny']

;  standard ROI normal map dark tables
darktbl_std = [	$
	'darks_2010_56.geny', $
	'darks_2011_56.geny', $
	'darks_2012_56.geny', $
	'darks_2013_56.geny', $
	'darks_2014_56.geny', $
	'darks_2016_56.geny', $
	'darks_2017_56.geny' $
;***** add in here darks from eclipse season data for following years
]
;  indices for years where we have standard spectral ROI darktables
;***** add in here years as the standard darks are processed
dk_yearstd = [2010,2011,2012,2013,2014,2016,2017]

;  full-wavelength normal map darktables
full_dktbl_std = [	'fulldark_20090731_133200.geny']
;  indices for years where we have full-wavelength gaintables
;***** add in here years as the full-wavelength  darks are processed
dk_yearfull = [2009]


;  SELECT DARK TABLE DATA FOR FULL SPECTRAL ROI, BOTH NORMAL AND FAST MAPS
;  for full spectral ROI, select the data based on year of observation
ngn = n_elements(dk_yearfull)
for kk = 0,ngn-1 do begin
	deltyear = abs(fxyear - dk_yearfull)
	mmnn = min(deltyear,iyr)
endfor
; get the stored dark images
; restore the dark data for regular and fast maps with the standard parameter
; settings
darkfile=file_search('$SOT_SP_CALIBRATION',full_dktbl_std(iyr))
if not file_exist(darkfile) then begin
   box_message,'Cannot find full spectral dark table for ', $
	   	dk_yearfull(iyr),' under $SOT_SP_CALIBRATION'
   stop
endif
restgenx,file=darkfile(0),full_darkavg_std,full_darkavg_fast
;  since darks are just rebinned from camssum=1 observations, must
;  account for the empirical -1 pixel shift.
full_darkavg_fast = shift(full_darkavg_fast,0,-1,0)


;  SELECT DARK TABLE DATA FOR STANDARD SPECTRAL ROI (56-167), BOTH 
;  NORMAL AND FAST MAPS
;  for full spectral ROI, select the data based on year of observation
ngn = n_elements(dk_yearstd)
for kk = 0,ngn-1 do begin
	deltyear = abs(fxyear - dk_yearstd)
	mmnn = min(deltyear,iyr)
endfor
; get the stored dark images
; restore the dark data for regular and fast maps with the standard parameter
; settings
darkfile=file_search('$SOT_SP_CALIBRATION',darktbl_std(iyr))
if not file_exist(darkfile) then begin
   box_message,'Cannot find standard spectral ROI dark table for ', $
	   	dk_yearstd(iyr),' under $SOT_SP_CALIBRATION'
   stop
endif
restgenx,file=darkfile(0),darkavg_std,darkavg_fast
;  fast map darks generated from normal map data are rebinned AND SHIFTED 
;  in the routine combine_darks_roi56.pro for May 2010 data onward, so no 
;  need to shift the images vertically here for the imperical -1 pixel shift.


;  STANDARD ROI EARLY MISSION DARKS
;  at present, use these values for 2009 and earlier
if fxyear le 2009 then begin
; get the stored dark images
; restore the dark data for regular and fast maps with the standard parameter
; settings
	darkfile=file_search('$SOT_SP_CALIBRATION',darktbl_early)
	if not file_exist(darkfile) then begin
   		box_message,'Cannot find dark table files for early', $
			'mission under $SOT_SP_CALIBRATION'
   	stop
	endif
	restgenx,file=darkfile(0),darkavg_std,darkavg_fast
endif
;  The fast map darks do not need shifting since they are derived from
;  actual data binned along the slit



return
end
