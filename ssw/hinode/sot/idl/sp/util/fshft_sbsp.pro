function fshft_sbsp, line, sh, nst, nend, nxtnd
;+
;
;	function:  fshft_sbsp
;
;	purpose:  shift array line by non-integer pixel shift sh by fourier
;		  or linear interpolation; uses wraparound for ends
;
;	notes:  1/95, lites@ncar (& rob@ncar) -
;			"extend"ed array to 256 points;
;			added frequency filtering option;
;			removed extraneous code and variables.
;		10/96, lites, put in keyword for possibility of linear
;			interpolation.
;		07/05, mofidifed from fshft.pro by lites to larger arrays 
;			for sbsp
;
;==============================================================================
;
;	Check number of parameters.
;
if n_params() ne 5 then begin
	print
	print, "usage:  ret = fshft_sbsp(line, sh, nst, nend, nxtnd)"
	print
	print, "	Shift array line by non-integer pixel shift sh by"
	print, "	linear interpolation.  Uses wraparound for ends."
	print, "	The parameter sh would be the negative of the result"
	print, "	of corshft to shift line2 back onto line1."
	print
	print, "	Arguments"
	print, "		line	- one-dimensional array (vector)"
	print, "		sh	- fractional pixel shift"
	print, "		nst	- starting index for shift"
	print, "		nend	- ending index for shift"
	print, "		nxtnd	- power of 2 > (nend-nst+1)"
	print, "			  note: allow enough points for "
	print, "			  smooth extension"
	print
	print
	return, 0
endif
;-
;
;	Set general parameters.
;
nx = n_elements(line)
nx1 = nx - 1
;
;	interpolate to nxtnd (= power of 2) points.
;
nmask = nxtnd - (nend - nst + 1)
if nmask le 14 then $
	message, 'no. pts ' + stringit(nmask) + ' is too close to nxtnd limit.'
;
;	Extend the array so that there is smooth transition in wraparound.
;
linesh = fltarr(nxtnd)
linesh(nst:nend) = line(nst:nend)
linesh = extend_sbsp(linesh, nst, nend)


;  since already extended, etc, do fourier interpolation directly
fftr = fft(linesh,-1)
fftr = xshift_sbsp(fftr,sh)
linesh = float(fft(fftr,1))


return, linesh(0:nx1)
end
