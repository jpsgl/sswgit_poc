pro genycoef_xtalk,input_xtalk,outfilname
;  routine to savegenx the fit coefficients for residual crosstalk
;  as a function of spectrum pixel and slit length

; INPUTS:
;	input_xtalk	= the path to the output file name from fitsurf.pro, for
;		example: /Users/lites/bruce/hinode/newresidcross/coef_xtalk.save
;	outfilname	= the name for the curent output file, usually named
;		after the date of the flat field operation used to generate
;		the crosstalk data, for example residxtalk_20100519

; revised version to use the 2010 eclipse season flat results

;  MUST BE RUN UNDER SSW!

;  save crosstalk parameters
restore,input_xtalk

;  create the .geny file
savegenx,file=outfilname,scoef,ndeg,sltpos,nftot


end
