function checkdropout_sbsp, index, dat
	if not strcmp(index.instrume, 'SOT/SP') then begin
		box_message, 'data is not from SOT/SP'
		stop
	endif
	date_obs = index.date_obs
	nw = index.naxis1
	nsl = index.naxis2
	numside = index.num_side

	ld = make_array(nw+2,nsl+2,type=size(dat,/type))

	maxzero = (long(nw)*long(nsl) mod 16384l)
	if maxzero eq 0 then maxzero = 16384l
	sidebad = 0b
	for istk = 0,3 do begin
		for iside = 0,numside-1 do begin
			ld[1:nw,1:nsl] = dat(*,*,iside,istk)
			countz = histogram(label_region(ld eq 0., /ulong)) ge maxzero
			if n_elements(countz) gt 1 then $
				if total(countz[1:*], /integer) ne 0 then begin
					sidebad = sidebad OR 2b^(istk+4*iside)
					print, string(date_obs, iside+1, (['I','Q','U','V'])[istk],$
						format='("packet loss test limit exceeded at file ",A,", CCD side ",I1,", Stokes ",A)')
				endif
		endfor
	endfor

	return, sidebad
end
