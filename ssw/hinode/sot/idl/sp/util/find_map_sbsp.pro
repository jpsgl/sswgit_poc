pro find_map_sbsp, map_date, cat, files, noblacklist=noblacklist, end_date=end_date
	; find a map around map_date

	; search interval
	miniv = 2. ; hours
	maxiv = 2. ; hours

	map_tai = anytim2tai(map_date)

	if keyword_set(end_date) then begin
		end_tai = anytim2tai(end_date)
		time0 = utc2str(tai2utc(map_tai))
		time1 = utc2str(tai2utc(end_tai))
	endif else begin
		time0 = utc2str(tai2utc(map_tai-miniv*60.*60.)) ; minus 2 hours
		time1 = utc2str(tai2utc(map_tai+maxiv*60.*60.)) ; plus 2 hours
	endelse

	; search catalog
	SEARCH_CAT:
	sot_cat, time0, time1, cat, files, /level0, count=ncat

	; sanity check
	if ncat eq 0 then begin
		print, "That's weird, no files were found. Is the catalog up to date?"
		stop
	endif

	; only SP4D please
	wsp4d = where(cat.obs_type eq 'SP IQUV 4D array', nsp4d)
	cat = cat[wsp4d]
	files = files[wsp4d]

	; sanity check
	if nsp4d eq 0 then begin
		print, "That's weird, no SP4D files were found."
		stop
	endif

	if not keyword_set(end_date) then begin
		; find closest file
		times = anytim2tai(cat.date_obs)
		difference = min(abs(times-map_tai), nearest)

		; identify map based on macroid
		wmap = where(cat.macroid eq cat[nearest].macroid, nmap)

		; sanity checks
		if wmap[0] eq 0 then begin
			miniv = 2.*miniv
			print, miniv, format='("Search hit minimum, extending range by ",I0," hours.")'
			time0 = utc2str(tai2utc(anytim2tai(time0)-miniv*60.*60.))
			goto, SEARCH_CAT
		endif
		if wmap[nmap-1] eq nsp4d-1 then begin
			maxiv = 2.*maxiv
			print, maxiv, format='("Search hit maximum, extending range by ",I0," hours.")'
			time1 = utc2str(tai2utc(anytim2tai(time1)+maxiv*60.*60.))
			goto, SEARCH_CAT
		endif

		if not keyword_set(noblacklist) then begin
			bad = checkblacklist_time_sbsp(cat[wmap].date_obs)
			if bad[0] eq -1 then begin
				print, "Problems encountered during blacklist check."
				stop
			endif
			wmap = wmap[where(bad eq 0, nmap)]
		endif

		cat = cat[wmap]
		files = files[wmap]
	endif
end
