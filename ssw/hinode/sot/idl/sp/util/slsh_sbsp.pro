pro slsh_sbsp,stks,shftsl,ny1,ny2

;  routine to apply the spatial shifts of the image as a function
;  of spectral pixel number to full Stokes data.  When applied, these 
;  shifts rectify the images so that spectral variation is strictly 
;  along the spectral pixel direction.  This process is known as removal 
;  of the spectral skew.

;  INPUTS:	stks	- Stokes spectral images (spectral,along slit,4)
;		shftsl	- previously computed shifts to be applied
;		ny1,ny2 - range of pixels along slit to shift (avoiding
;				defects near the ends)

;  OUTPUT: 	stks
 



;  working arrays next power of two larger in length
nyy = sizeof_sbsp(stks,2)
nxx = sizeof_sbsp(stks,1)	; full spectral dimension of data
for i = 0,20 do begin
	np2 = 2^i
	if np2 gt nyy then begin
		ntrp = np2
		goto, kilroy
	endif
endfor
kilroy:
sltvar = fltarr(ntrp)	; slit variation extended to next power of 2


nyb =  ny2-ny1+1
for kk = 0,3 do begin	;  loop over Stokes parameters
	for i = 0,nxx-1 do begin
		sltvar(*) = 0.
		sltvar(0:nyb-1) = stks(i,ny1:ny2,kk)
; extend the array rather than apodize
		sltvar = extend_sbsp(sltvar,0,nyb-1)
		fftera = fft(sltvar,-1)
		fftera = xshift_sbsp(fftera,-shftsl(i))
		sltvar = float(fft(fftera,1))
		stks(i,ny1:ny2,kk) = sltvar(0:nyb-1)

	endfor
endfor

end
