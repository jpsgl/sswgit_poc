pro icrossv_sbsp, stks, sr1, sr2
;+
;
;  procedure:  icrossv_sbsp
;
;  purpose:  Determine residual I -> V crosstalk from the continuum
;	     spectral region, then apply the inverse of that crosstalk to
;	     the data.  Crosstalk determined and applied on a row-by-row basis
;	     by finding a linear trend of the crosstalk along the slit.
;  THIS ROUTINE CORRECTS ONLY V SPECTRA.  THIS ROUTINE IS APPLIED IN 
;  CALIB_SBSP.PRO ONLY TO SINGLE-SIDED DATA.  IT AVOIDS APPLYING THE CORRECTION
;  TO REGIONS OF HIGH POLARIZATION BECAUSE V-SIGNAL OVERLAPS INTO CONTINUUM
;  WAVELENGTHS FOR STRONG FIELDS LIKE IN SUNSPOTS
;
;==============================================================================
;
;	Check number of parameters.
;
;;if n_params() ne 10 then begin
if n_params() ne 3 then begin
	print
	print, "usage:  icrossv_sbsp, stks, sr1, sr2"
	print
	print, "	Determine residual i -> V crosstalk from the"
	print, "	continuum spectral region, then apply the inverse of"
	print, "	that crosstalk to the data.  Crosstalk determined and"
	print, "	applied on a row-by-row basis."
	print
	print, "	Arguments"
	print, "		stks	- spectral images (all 4 are input"
	print, "			  but only qa,ua,va are modified)"
	print, "			  first index is along slit, second"
	print, "			  is wavelength"
	print, "	sr1,sr2		- range of pixels along the slit for "
	print, "			  fitting the crosstalk (avoid ends)"
	print
	print, "	Keywords"
	print, "		(none)"
	print
	return
endif
;-


;
;	Get dimensions of arrays.
;

nw = sizeof_sbsp(stks, 1)
ny = sizeof_sbsp(stks, 2)
nw1 = nw-1
ny1 = ny-1

;	Get median of Stokes vector in continuum window.
;
pmed = fltarr(4,ny)		; zero array

;  get average of spectrum along the slit
spect = fltarr(nw)
for jj = sr1,sr2 do spect = spect + stks(*,jj,0)

;  get median of spectrum, find continuum wavelengths
spect = spect/float(sr2-sr1+1)
mdn = median(spect)
whr = where(spect gt mdn)
;  calculate mean continuum level for Stokes V only
for kk = 0,3,3 do begin
	for jj = 0, ny1 do pmed(kk,jj) =  $
	    mean_sbsp(stks(whr,jj,kk))
endfor

;  find the wavelength-integrated Stokes V/I signal level along slit
;  this will be used as a criterion whether or not to apply the
;  crosstalk.  Strong Stokes V signals contaminate the so-called
;  continuum in this narrow bandpass, so where there are strong
;  signals anyway (sunspots etc), don't apply the correction
vbar = fltarr(ny)
for jj = 0,ny1 do $
    vbar(jj) = total(abs(stks(*,jj,3)))/total(stks(*,jj,0))

;	Calculate crosstalk levels.
for kk = 3,3 do  pmed(kk,*) = pmed(kk,*)/pmed(0,*)

;	Subtract crosstalk correction from the polarization spectra
;  check for very low intensities (off limb), where no correction will
;  be applied because of the possibility of intensity values very close to
;  zero
;  also bypass points where the fractional Stokes V polarization over the 
;  entire bandpass is larger than 0.02.  This value was determined
;  empirically by examining Stokes V of a large sunspot.  
for jj = 0, ny1 do begin
	if pmed(0,jj) gt 100. and vbar(jj) lt 0.02 then begin
		stks(*,jj,3) = stks(*,jj,3) $
			- pmed(3,jj)*stks(*,jj,0)
	endif
endfor


return
end
