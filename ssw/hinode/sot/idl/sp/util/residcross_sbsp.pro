pro residcross_sbsp,slpos,scoef,degn,calpos,nftot,nx,ny,resid

;  do linear interpolation on residual crosstalk data to
;  get values at the slit scan position desired

;  INPUTS:	slpos	= slit scan step position at which
;			  the residual crosstalk values are desired
;		scoef	= array of measured coefficient values [degn,3,nftot]
;		degn	= degree of polynomial fit
;		calpos	= set of slit scan positions where 
;			  calibration data have been taken
;		nftot	= number of slit scan calibration positions
;		nx		= number of spectral pixels
;		ny		= number of pixels along the slit
;  OUTPUT:	resid	= array of residual crosstalk corrections [112,1024,3]

;   History:
;    23-Nov-2006 - Bruce Lites, HAO.
;	 16-Aug-2011 - Bruce Lites, HAO, altered for variation along slit only


if slpos le calpos(0) then begin
        k1 = 0
endif else if slpos gt calpos(nftot-1) then begin
        k1 = nftot-2
endif else begin
        for kk = 0,nftot-2 do begin
        if slpos gt calpos(kk) and slpos le calpos(kk+1) then k1 = kk
        endfor
endelse
k2 = k1 + 1

;  setup output array
resid = fltarr(ny,nx,3)

;  interpolate the solutions, not the coefficients
residk1 = dblarr(nx,ny,3) & residk2 = dblarr(nx,ny,3)
residk1(*,*,*) = 0.D & residk2(*,*,*) = 0.D 
resid = dblarr(nx,ny,3) & resid(*,*,*) = 0.D

for istks = 0,2 do begin
	for jj = 0,ny-1 do begin
	for mj = 0,degn do begin
		residk1(*,jj,istks) = residk1(*,jj,istks) + $
                scoef(mj,istks,k1) * double(jj)^mj
		residk2(*,jj,istks) = residk2(*,jj,istks) + $
                scoef(mj,istks,k1) * double(jj)^mj
		resid(*,jj,istks) = ( (calpos(k2)-slpos)*residk1(*,jj,istks) +  $
        	(slpos-calpos(k1))*residk2(*,jj,istks))/(calpos(k2)-calpos(k1))
	endfor
	endfor
endfor

return
end
