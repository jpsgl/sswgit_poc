
pro select_flats,input_dir,roistart,goodflats,skipexam=skipexam
;
;+
;   Name: select_flats
;!!MUST BE RUN UNDER SOLARSOFT
;
;
;   Purpose: Pass through the raw data directory selecting good flats, and 
;	eliminating files with radiation hits, or dropouts.  Output a save
;	file containing the list of pathnames to good files.
;		
;   Inputs:
;	input_dir = directory path to flat field files
;       roistart        = spectral ROI start pixel
;       skipexam        = if set, then skip manual examination of images

;
;   Output: 
;	goodflats	= string array with list of good files
;	IDL SAVE file with list of good flat field file names
;
;   History:
;    3-Dec-2009 - Bruce Lites, HAO.
;-
;     
version=1.0 

;  get the string representation of the start pixel of the ROI
rstrt = strcompress(string(roistart),/remove_all)
;  get the time ID for the filename
;ppos = strpos(input_dir,'/200')
ppos = strpos(input_dir,'/20')
plgth = strlen(input_dir)-ppos - 2
timid = strmid(input_dir,ppos+1,plgth)
;  generate the output filename for the selected files for averaging
outfilnam = 'goodflats_' + rstrt + '_' + timid + '.save'

;  bypass manual examination
if keyword_set(skipexam) then begin
        restore,outfilnam
        nfile = n_elements(derf)
        print,'bypassing the exam of each file, read filenames from save file'
        goto,bypass39
endif


;  Cycle through the files, displaying each one and rejecting bad
;  files

;  find the file names in the current directory
derf = file_search(input_dir + '*.fits')
nfile = n_elements(derf)
print,'directory: ',input_dir,'; number of files = ',nfile
fileindx = intarr(nfile)        ;  accept index for each file (1=accept)
goodflats = derf ; sequential list of good file path names
goodflats(*) = ' '

;  loop through files examining them
countd = 0
for kk = 0,nfile-1 do begin
        ;iseq = whr(kk)
        ;flnm = derf(kk)
	sp4fits = derf(kk)
;  First read in the data and the FITS header
	print,' Begin processing file ',kk,' of ',nfile-1,'  ',sp4fits
        hdr=headfits(sp4fits)
        read_sot,sp4fits,index,dat
	dat = float(dat)

;        print,'file ',kk,' of ',nfile
;        print,flnm
;        dat = readfits(flnm,hdr)
	bitshft = sxpar(hdr,'SPBSHFT')
        nxs2 = sxpar(hdr,'NAXIS2')
        nxs1 = sxpar(hdr,'NAXIS1')

; DIGITAL WRAP, BIT SHIFTING CORRECTIONS
; correct for digital wrap-around of Stokes I, if wrap-around is present
        temp = dat(*,*,*,0)
        whr = where(temp lt 0., countwrap)
        if countwrap ne 0 then temp(whr) = temp(whr) + 65536.
        dat(0,0,0,0) = temp
;  account for bit shifting of the data by multiplying the appropriate
;  Stokes images by 2
        switch bitshft of
                3:      dat(*,*,*,1:2) = 2.*dat(*,*,*,1:2)
                2:      dat(*,*,*,3) = 2.*dat(*,*,*,3)
                1:      dat(*,*,*,0) = 2.*dat(*,*,*,0)
        endswitch

;  initialize display area
        if kk eq 0 then begin
                disp = bytarr(8*nxs1,nxs2)
		seqnum = lonarr(nfile)
		filnam = derf
		flatavg = fltarr(nxs1,nxs2,2)
        endif

;  fill display
        for iside = 0,1 do begin

;  check to see if there are any zero or negative Stokes I values
		whrzz = where(dat(*,*,iside,0) le 0.,countzz)
		skipthis = 0
		if countzz ne 0 then begin
			skipthis = 1
			print,'CAUTION! ZERO VALUES FOR STOKES I! iside ',iside
			stop
		endif

;  get average Stokes I for each side
        for istks = 0,3 do begin
                istrt = iside*nxs1*4 + istks*nxs1
                temp = dat(*,*,iside,istks)
                ;if istks eq 0 then temp = temp-min(temp)-100.
                if istks eq 0 then begin
			temp = bytscl(temp)
		endif else begin
			rms = stddev(temp)
;  plot QUV range is twice rms deviation
			temp = bytscl(temp,min=-2.*rms,max=2.*rms)
		endelse
                disp(istrt:istrt+nxs1-1,*) = temp
        endfor
        endfor
		strnfile = strcompress(string(nfile-1))
		strkk = strcompress(string(kk))
		titl = 'File ' + strkk + ' of ' + strnfile
	;tvwin,disp,title=titl
	    if kk eq 0 then begin
	        tvwin,disp,title=titl
	    endif else begin
	        tvwin,disp,title=titl,/currw
	    endelse


        accept = ' '
        ;read,' accept this file? (y/n), or go back (b) ',accept
        read,' type n to reject, b to go back, any other to continue: ',accept
;  case to back up sequence
        if accept eq 'b' then begin
                read,' enter number of files to go back: ',bkup
                kk = kk - fix(bkup) - 1
;  be sure not to back up beyond beginning of file
                if kk lt 0 then kk = 0
		endif
;  case of rejecting current one
        if accept eq 'n' then fileindx(kk) = 0
;  case to accept the file
        if accept ne 'n' and skipthis eq 0 then fileindx(kk) = 1

;  end loop over selected files
endfor


;  readjust the list of filenames for good files to be averaged
countd = 0
for kk = 0,nfile-1 do begin
        flnm = derf(kk)
        if fileindx(kk) eq 1 then begin
                goodflats(countd) = derf(kk)
                countd = countd + 1
        endif
endfor
goodflats = goodflats(0:countd-1)
derf = goodflats
save,filename=outfilnam,derf

;  skip manual examination
bypass39:countd = n_elements(derf)

stop
read,' type any character to continue beyond flat selection: ',accept

return
end
