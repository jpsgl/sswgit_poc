pro readl1_sbsp,in_file,dat,hdr

;  routine to read in and properly normalize the level1 SP data

;	INPUT:		in_file	= the fits input file

;	OUTPUTS:	dat	= the floating point Stokes data
;			hdr	= the level1 fits header

dat = float(readfits(in_file,hdr,/silent))

;  adjust for intensity wraparound
temp = dat(*,*,0)
whr = where(temp lt 0., countwrap)
if countwrap ne 0 then temp(whr) = temp(whr) + 65536.
dat(0,0,0) = temp

;  bit shifting for SP data
        bitshft = sxpar(hdr,'SPBSHFT')
;  account for bit shifting of the data by multiplying the appropriate
;  Stokes images by 2
        switch bitshft of
                3:      dat(*,*,1:2) = 2.*dat(*,*,1:2)
                2:      dat(*,*,3) = 2.*dat(*,*,3)
                1:      dat(*,*,0) = 2.*dat(*,*,0)
        endswitch


return
end
