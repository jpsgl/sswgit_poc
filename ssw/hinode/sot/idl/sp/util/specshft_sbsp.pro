pro specshft_sbsp,dat,ssl1,ssl2,wc1,wc2,specshft

;  routine to cross-correlate the flat field spectral profiles from
;  of one CCDSIDE relative to the other to find the relative spectral
;  shift as a function of distance along the slit.

;  INPUTS:	dat		= Flat-fielded Stokes I images
;		ssl1,ssl2	= active range along slit for correlation, data
;					units
;		wc1,wc2		= spectral pixel range for cross-correlation to
;					get spectral shifts
;

;  OUTPUTS:	specshft	= spectral shifts along the slit for the two
;				  CCDSIDEs: shift CCDSIDE0 relative to 
;				  CCDSIDE1


;  size of original image
nyy = sizeof_sbsp(dat,2)

specshft = fltarr(nyy) ; output spectral shifts
specshft(*) = 0.

;  separate out the two image areas
im1 = dat(*,*,0) & im2 = dat(*,*,1)
y1 = ssl1 & y2 = ssl2

; loop through slit length finding cross-correlation of the lines
;  shift CCDSIDE0 relative to CCDSIDE1
;  skip first row with zero intensity
;  the spectral range [wc1:wc2], perform this shift for the 
;  range [ssl1,ssl2], as set in params_sbsp.dat
for jj = ssl1, ssl2 do begin
        tempr = im1(wc1:wc2,jj)
	temps = im2(wc1:wc2,jj)
        specshft(jj) = corshft_sbsp(temps,tempr)
endfor
;  extrapolate to ends of arrays
if y1 gt 0 then specshft(0:y1-1) = specshft(y1)
if y2 lt nyy-1 then specshft(y2+1:nyy-1) = specshft(y2)


xxx = findgen(nyy)
window,xs=700,ys=500,/free
plot,xxx,specshft,title='spectral shifts between CCDSIDES, pixels'

return
end
