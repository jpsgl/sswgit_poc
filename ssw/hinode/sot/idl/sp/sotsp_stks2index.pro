pro sotsp_stks2index, stksin, index, data, http_parent=http_parent, $
   cont_only=cont_only, long_only=long_only, trans_only=trans_only, $
   outdir=outdir, status=status, caveat_use=caveat_use, $
   level2=level2, _extra=_extra , debug=debug
   
;+
;    Name: sotsp_stks2index
;
;    Purpose: time/url/scanid -> SOT-SP ;
; 
;    Input Parameters:
;       stskin - time, local file url of SOT-SP level2, or SOT/SP "scanid"
;
;    Output:
;       index,data - ssw compliant stokes map "index,data"
;
;    Keyword Parameters:
;       http_parent - assumed parent url; <http://...>hinode/level1d/....
;                     default='http://www.lmsal.com/solarsoft/'
;       status - 1(ok) if "index,data" return properly
;       caveat_use - if set, allow pre-vet access with warning
;       level2 - if set, apply to SP Level2 (inversions)
;
;    Calling Sequence:
;       IDL> sotsp_stks2index, scanid, index [, data]
;
;   History:
;      5-nov-2007 - S.L.Freeland - ssw client access -> remote SOT/SP L1D data
;      7-feb-2008 - S.L.Freeland - temporary warning during paradigm shift..
;     17-mar-2008 - S.L.Freeland - allow /CAVEAT_USE  
;     30-may-2008 - S.L.Freeland - add /LEVEL2 keyword&function (inversions)
;     12-feb-2009 - S.L.Freeland - specific l2 dir (probs with symbolic link?)
;                                  size check instead of exist check
;      9-jun-2009 - S.L.Freeland - set /CAVEAT (renable verbatim www pages)
;     17-sep-2009 - S.L.Freeland - sot.lmsal.com -> www.lmsal.com (sot prob?)
;     21-mar-2013 - S.L.Freeland - reinstated use of sot.lmsal.com server
;     15-apr-2015 - S.L.Freeland - server reconfig... use solarsoft url vector
;
;   TODO: allow the header-only read w/out transfer.. maybe later this afternoon
;-
debug=keyword_set(debug)
status=0
level2=keyword_set(level2)
caveat_use=1  ; slf, 9-jun-2009 - 
if not data_chk(stksin,/string) then begin 
   box_message,'Need time, url, or local SOT/SP L1D file'
   return
endif
;------------------- TEMPORARY Out of Service --------------------
dayaftertom=utc2dow(reltime(days=2,out_style='utc_int'),/string)
if keyword_set(caveat_use) or level2 then begin
   box_message,'Note that xcen/ycen are not yet validated, but help yourself'
endif else begin 
   box_message,nbox=2,['Sorry: Temporarily out of Service...', $
   'Doing a little vetting on derived XCEN/YCEN...', $
   'check back on ' + dayaftertom, $
   '(sotsp_stks2struct.pro should work though...) please try:','', $
   "IDL> stks=sotsp_stks2struct('" + stksin + "')","" ],/center 
   ;"or, feel free to accept w/caveat emptor coords via /CAVEAT switch:","", $
   ;"IDL> sotsp_stks2index,'" + stksin + "',index,data,/CAVEAT"] $
   return ; !!! early exit
endelse
;

envparent=get_logenv('HINODE_DATA_URL')
case 1 of
   data_chk(http_parent,/string):   ; user supplied
   file_exist(envparent): http_parent=envparent
   else: http_parent='http://www.lmsal.com/solarsoft/hinode/' ; 'http://sot.lmsal.com/'
endcase

case 1 of
   file_exist(stksin) and 1-is_dir(stksin) : locfile=stksin  ; user supplied 
   strpos(stksin,'http:') ne -1 : url=stksin ; user supplied full URL
   strpos(stksin,'_') ne -1 and strlen(stksin) eq 15: begin ; user supp direct
      time=file2time(stksin)
   endcase
   else: time=stksin  ; user supplied time
endcase

if n_elements(time) gt 0 then begin ; try mapping time->url
   subdir=time2file(time,/sec)
   dpath=anytim(time,/ecs,/date_only)
   hour='/H'+strmid(ssw_strsplit(time2file(time),'_',/tail),0,2)+'00'
   spl2path=(['level1d/','level2hao/'])(level2) +$
        dpath+'/SP3D/'+subdir
   url=http_parent + (['','/'])(strlastchar(http_parent) ne '/') + spl2path
   ;url=str_replace(url,'sot.lmsal.com/data/sot','www.lmsal.com/solarsoft/data/hinode/sot')
endif
 
if debug then stop,url


if n_elements(outdir) eq 0 then outdir=curdir()
 
wnames='_'+str2arr('conti,longi,trans,veloc')+'_'
fnames='sotsp'+wnames+subdir+'.fits'
if level2 then fnames=subdir+'.fits'

if n_elements(url) gt 0 then begin 
   if n_elements(url) eq 1 then url=url(0)
   if strpos(url(0),fnames(0)) eq -1 then urls=concat_dir(url,fnames) else urls=url ; user supplied?
   locfile=concat_dir(outdir,fnames)
   ssw_file_delete,locfile
   box_message,'Tranferring stokes maps -> local..'
   sock_copy,urls,out_dir=outdir,progress=get_logenv('ssw_nox') eq ''
endif
if debug then stop,'urls
fex=file_exist(locfile)
ssok=where(file_size(locfile) gt 2880,okcnt)
ssb=1-ssok

status=(okcnt eq n_elements(locfile)) and is_fits(locfile(0))
if status then begin 
   case 1 of
      level2: begin
         read_sotsp,locfile,index,data,scan_info=scan_info,_extra=_extra
      endcase
      else: mreadfits,locfile,index,data,_extra=_extra
   endcase
endif else box_message,'Problem with at least one file/transfer...'

return
end
      
