function merlin_read_fits2, file, silent=silent
;
;+
;   Name: merlin_read_fits2
;
;   Purpose: read SOT/SP Merlin inversion Binary Extended FITS files
;
;   Input Parameters:
;      file - SOT/SP Level2 (inversions) 
;
;   Keyword:
;      silent - run quieter (->ssw_binfits2struct -> mrdfits)
;
;   History:
;      20-apr-2008 - Scott Mcintosh -          
;      22-apr-2008 - S.L.Freeeland - use ssw_binfits2struct.pro
;
;   Restrictions:
;      gen routine uses execute so not VM compatible
;      not sure yet...
;-
field_names = ['FieldStrength','FieldInclination','FieldAzimuth','DoppShift1',$
	'DoppShift2', 'DoppWidth', 'LineStength', 'Damping', 'SrcFunc',$
	'SrcFuncGrad', 'MacroTurb', 'Alpha', 'Delta', 'FieldStrengthErr',$
	'FieldInclErr', 'FieldAzimuth', 'DoppShift1Err', 'DoppShift2Err', $
	'DoppWidthErr', 'LineStengthErr', 'DampingErr', 'SrcFuncErr', $
	'SrcFuncGradErr', 'MacroTurbErr', 'AlphaError', 'DeltaError', $
	'ChiSq_I', 'ChiSq_Q', 'ChiSq_U', 'ChiSq_V', 'ChiSq_Tot',$
	'LineStength', 'Polarization', 'MagStokesV', 'FitAttrib', 'NumIt', $
	'XCoord', 'YCoord', 'Times', 'MechSlitPos', 'ScattLight']

labels = ['Magnetic Field', 'Magnetic Field Inclination', 'Magnetic Field Azimuth',$
	'Doppler Shift of 630.15nm','Doppler Shift of 630.25nm','Doppler Width',$
	'Line Strength', 'Damping Parameter', 'Source Function', 'Source Function Gradient',$
	'Macroturbulence','Stray Light Fraction','Stray Light Shift','Magnetic Field Error',$
	'Magnetic Field Inclination Error','Magnetic Field Azimuth Error',$
	'Doppler Shift of 630.15nm Error','Doppler Shift of 630.25nm Error',$
	'Doppler Width Error','Line Strength Error','Damping Parameter Error',$
	'Source Function Error','Source Function Gradient Error','Macroturbulence Error',$
	'Stray Light Fraction Error','Stray Light Shift Error','Chi-Squared for Stokes I',$
	'Chi-Squared for Stokes Q','Chi-Squared for Stokes U','Chi-Squared for Stokes V',$
	'Total Chi-Squared [I, Q, U & V]','Continuum Intensity','Polarization Degree',$
	'Magnitude of Stokes V','Final Status of Inversion','Number of Iterations',$
	'Map X Coordinates', 'Map Y Coordinates', 'Frame Times', 'Mechanical Slit Positions',$
	'Scattered Light Profile']

retval=ssw_binfits2struct(file,silent=silent) ; ssw-gen routine
retval=join_struct({header:headfits(file),data_labels:labels}, $
          temporary(retval))

return, retval 
end
