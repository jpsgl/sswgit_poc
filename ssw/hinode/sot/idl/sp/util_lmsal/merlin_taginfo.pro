function merlin_taginfo, unit=unit, range=range, log=log, $
   fits_label=fits_label, data_label=data_label, field_name=field_name, $
   maps_only=maps_only, xycoord=xycoord, debug=debug
;
;+
;   Name: merlin_taginfo
;
;   Purpose: sot/sp binary extension tag/descriptors/scaling info
;
;   I/O - (generally called by non humanoid wrapper routine)
;
;   Scaling info derived from Scott McIntosh work
;
;   History:
;      Circa 1-aug-2008 - S.L.Freeland - SOT/SP Level2 helper routine
;      22-jan-2008 - S.L.Freeland - per Bruce mag field unit kG->Gauss

common merlin_taginfo_blk, units, ranges, fits_labels, data_labels, field_names,logs 


if 1 then begin ; bypass "caching" at least for now
units = ['Gauss','Degrees','Degrees','km/s','km/s','mA','',$
	'','','','km/s','','km/s',$
	'','','','','','','','','','','','','',$
	'','','','','',$
	'Data Number','','Percent','Value','Iterations']
ranges = [[0,5.e3],[0,180],[0,180],[-5,5],[-5,5],[10,90],[0.01,100.],$
	[0.,1.5],[1,1.e5],[1,1.e5],[0.,3.],[0.,1.],[-10,10],$
	[0.,0.],[0.,0.],[0.,0.],[0.,0.],[0.,0.],[0.,0.],$
	[0.,0.],[0.,0.],[0.,0.],[0.,0.],[0.,0.],[0.,0.],[0.,0.],$
	[0.,0.],[0.,0.],[0.,0.],[0.,0.],[0.,0.],$
;	[1.e-8,1.e-2],[1.e-8.,1.e-2],[1.e-8.,1.e-2],[1.e-8.,1.e-2],[1.e-8.,1.e-2],$
	[0.,0.],[0.,0.],[-5.,5.],[0.,6.],[0.,0.]]
fits_labels = ['SFLD','SPSI','SAZM','SCEN1','SCEN2','SDOP','SETA0',$
	'SDMP','SBZERO','SB1MU','VMAC','SALPHA','SDELTA',$
	'SFLD_ERR','SPSI_ERR','SAZM_ERR','SCEN1_ERR','SCEN2_ERR',$
	'SDOP_ERR','SETA0_ERR','SDMP_ERR','SBZERO_ERR','SB1MU_ERR',$
	'VMAC_ERR','SALPHA_ERR','SDELTA_ERR',$
	'CHISQ_I','CHISQ_Q','CHISQ_U','CHISQ_V','CHISQ_TOT',$
	'ICONT','NET_POL','STOKESV','RCONV','NITER']
logs = [0,0,0,0,0,0,1,$
	0,1,1,0,0,0,$
	0,0,0,0,0,0,0,0,0,0,0,0,0,$
	1,1,1,1,1,$
	0,1,0,0,0]

data_labels= ['Magnetic Field', 'Magnetic Field Inclination', 'Magnetic Field Azimuth',$
        'Doppler Shift of 630.15nm','Doppler Shift of 630.25nm','Doppler Width',$
        'Line Strength', 'Damping Parameter', 'Source Function', 'Source Function Gradient',$
        'Macroturbulence','Stray Light Fraction','Stray Light Shift','Magnetic Field Error',$
        'Magnetic Field Inclination Error','Magnetic Field Azimuth Error',$
        'Doppler Shift of 630.15nm Error','Doppler Shift of 630.25nm Error',$
        'Doppler Width Error','Line Strength Error','Damping Parameter Error',$
        'Source Function Error','Source Function Gradient Error','Macroturbulence Error',$
        'Stray Light Fraction Error','Stray Light Shift Error','Chi-Squared for Stokes I',$
        'Chi-Squared for Stokes Q','Chi-Squared for Stokes U','Chi-Squared for Stokes V',$
        'Total Chi-Squared [I, Q, U & V]','Continuum Intensity','Polarization Degree',$
        'Magnitude of Stokes V','Final Status of Inversion','Number of Iterations',$
        'Map X Coordinates', 'Map Y Coordinates', 'Frame Times', 'Mechanical Slit Positions',$
        'Scattered Light Profile']

field_names= ['FieldStrength','FieldInclination','FieldAzimuth','DoppShift1',$
        'DoppShift2', 'DoppWidth', 'LineStength', 'Damping', 'SrcFunc',$
        'SrcFuncGrad', 'MacroTurb', 'Alpha', 'Delta', 'FieldStrengthErr',$
        'FieldInclErr', 'FieldAzimuth', 'DoppShift1Err', 'DoppShift2Err', $
        'DoppWidthErr', 'LineStengthErr', 'DampingErr', 'SrcFuncErr', $
        'SrcFuncGradErr', 'MacroTurbErr', 'AlphaError', 'DeltaError', $
        'ChiSq_I', 'ChiSq_Q', 'ChiSq_U', 'ChiSq_V', 'ChiSq_Tot',$
        'LineStength', 'Polarization', 'MagStokesV', 'FitAttrib', 'NumIt', $
        'XCoord', 'YCoord', 'Times', 'MechSlitPos', 'ScattLight']
endif

xyc=keyword_set(xycoord)
if xyc then begin
   units=[units,'arcsec','arcsec']
   logs=[logs,0,0]
   ranges=[[ranges],[-2000,2000],[-2000,2000]]
   fits_labels=[fits_labels,'XCOORD','YCOORD']
endif


case 1 of 
   keyword_set(unit): retval=units
   keyword_set(field_name): retval=field_names
   keyword_set(range): retval=ranges
   keyword_set(data_label): retval=data_labels
   keyword_set(log): retval=logs
   keyword_set(fits_label): retval=fits_labels
   else: reval=data_labels
endcase
debug=keyword_set(debug)

if debug then stop,'retval

if keyword_set(maps_only) then begin 
   retval=retval(0:where(strpos(data_labels,'Iterations') ne -1) + (2*xyc))
endif

return, retval
end
