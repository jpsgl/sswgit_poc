function sotsp_time2level2, time0, time1, $
   interactive=interactive, check_exist=check_exist, count=count
;
;+
;   Name: sotsp_time2level2
;
;   Purpose: user time/time range -> SOT/SP Level 2 (inversion) FITS filelist
;
;   Input Parameters:
;      time0 - time or start time of range
;      time1 - stop time of range
;  
;   Keyword Parameters:
;      interactive - if set and multiple matches, present menu for selection
;      count (output) - number of files returned (0->none in range)
;
;   Calling Sequence:
;      IDL> l2files=sotsp_time2level2(time) ; closest L2 to time
;      IDL> l2files=sotsp_time2level2(time0,time1) ; all in range
;      IDL> l2files=sotsp_time2level2(time0,time1,/interact) ; menu select one
;
;
;   Restrictions:
;      local only for now; assume hao-like tree under $sotsp_level2
;
;   History:
;      20-may-2008 - S.L.Freeland - helper function
;
;-
;      


;-
; 
parent=get_logenv('$sotsp_level2')
if not file_exist(parent) then $
   parent=concat_dir('$ssw_path','hinode/level2d')  ; lmsal default
case 1 of 
   n_params() eq 0: begin 
      time0='1-dec-2006'
      time1='31-dec-2006'
   endcase
   n_params() eq 1: begin
      time_window,time0,time0,time1,days=[0,1]

   endcase
   else:
endcase
   
dgrid=ssw_time2paths(time0,time1,parent=parent)

tops=concat_dir(dgrid,'SP3D')

sptops=concat_dir(tops,'????????_??????')

sps=file_search(sptops)

spid=ssw_strsplit(sps,'SP3D/',/tail)

fits=concat_dir(sps,spid+'.fits')
nfits=n_elements(fits) ; idealized number fits in time window

if keyword_set(check_exist) then begin 
   fex=where(file_exist(fits),sse)
   case 1 of  
     sse eq 0: begin
        box_message,'No FITS found in your time range'
        return,''
     endcase
     sse eq nfits: ; all exist
     else: box_message,'Not all FITS in time range exist'
   endcase
   fits=fits(fex)
   spid=spid(fex)
endif 

retval=fits

case 1 of
   n_params() eq 1: begin  ; get closest
      ftimes=anytim(file2time(fits))
      dtime=abs(anytim(time0) - ftimes) 
      ss=(where(dtime eq min(dtime)))(0) ; closest
      retval=fits(ss)
   endcase
   keyword_set(interactive): begin 
      ss=xmenu_sel(spid,/one)
      if ss(0) then box_message,'No selection, returning 1st'
      retval=fits(ss>0)
   endcase
   else:
endcase
count=n_elements(retval)*(retval(0) ne '')

return, retval
end
 

   



