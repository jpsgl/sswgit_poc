pro sotsp_reform,index,data,rindex,rdata, $
   scan_info=scan_info, status=status, only_maps=only_maps, concat=concat, $
   debug=debug, error=error
;+
;   Name: sotsp_reform
;
;   Purpose: reform SOT/SP repeted maps -> n2D "index,data"
;
;   Input Parameters:
;      index - L1/L2 index (read_sotsp output)
;      data - L1/L2 data (readd_sotsp output)
;
;   Keyword Parameters:
;      scan_info - TIMES/SLITPOS etc structure (keyword output from read_sotsp)
;      status (output) - return "true" if data was reformed
;      only_maps - vector of desired maps (default=all ~36/38)
;      concat - if set, concatenate maps in x direction / removes channel dim
;      error - if set, error - Constant SLITPOS for now
;
;   Calling Sequence:
;      sotsp_reform, index, data , oindex, odata [,scan_info=scan_info]
;      sotsp_reform, ii,dd, oii, odd, only_maps=<map subscripts> [,/concat]
;
;   Calling Example/Context:
;      read_sotsp,<l2file>, ii,dd,scan_info=si
;      sotsp_reform, ii, dd, oii, odd ,only_maps=[33,0,1,2,3] ,/concat, scan=si
;      help, dd, odd & print,oii.ftype
;  DD              FLOAT     = Array[4156, 256, 36] ; << in
;  ODD             FLOAT     = Array[65, 256, 319]  ; << out 
;  Magnitude of Stokes V,Magnetic Field,Magnetic Field Inclination,Magnetic Field Azimuth,Doppler Shift of 630.15nm
;
;   History:
;      3-Jun-2008 - S.L.Freeland - SOT/SP ssw helper utility
;
;   Restrictions:
;      
;      if SCAN_INFO is not available, then 'ideal' is returned - that
;      assumes no missing data as frames/times aligned on derived boundries
;      Qualitative, not quantitative as of today (but ok for qkl "movies")
;-

status=0
debug=keyword_set(debug)

if not data_chk(scan_info,/struct) then begin 
   box_message,'Single map/scan - no action currently taken'
   return
endif

slitpos=scan_info.slitpos

hpos=histogram(slitpos)
error=n_elements(hpos) eq 1

if error then begin 
   box_message,'Error - Constant SLITPOS...'
   return
endif 
if max(hpos) le 1 then begin
   box_message,'No multiple repeats.. returning
   return
endif 

nmaps=max(hpos)-1
ny=data_chk(data,/ny)
nx=data_chk(data,/nx)
nim=data_chk(data,/nimages)
swidth=round(max(abs(deriv_arr(slitpos)))) 
step=round(median(deriv_arr(slitpos)))
swidth=(swidth/step)+1

sss=where(slitpos eq min(slitpos)) ; scan starts

;ideal=fltarr(nmaps*swidth,data_chk(data,/ny), data_chk(data,/nimages))
rdata=fltarr(swidth,data_chk(data,/ny),data_chk(data,/nimages),nmaps)
rdata=make_array(swidth,data_chk(data,/ny),data_chk(data,/nimages),nmaps, $
   type=data_chk(data,/type))

for i=0,(nmaps*swidth)-swidth,swidth do begin
   rdata(0,0,0,i/swidth)=data(i:i+(swidth-1)<(nx-1),*,*)
endfor

rindex=index

if keyword_set(only_maps) then begin 
   rindex=rindex(only_maps)
   rdata=temporary(rdata(*,*,only_maps,*))
   if keyword_set(concat) then begin 
      alltypes=arr2str(rindex.ftype)
      nf=(size(rdata))(3)
      dx=data_chk(rdata,/nx)
      dy=data_chk(rdata,/ny)
      ni=(size(rdata))(4)
      retval=make_array(nf*dx,dy,ni,type=data_chk(rdata,/type))
      for i=0,nf-1 do begin 
         retval(i*swidth,0,0)=reform(rdata(*,*,i,*))
      endfor
      delvarx,rdata
      rdata=retval
      rindex=rindex(0)
      rindex.ftype=alltypes
   endif
endif
   

;ideal(0,0,0)=data
;ideal=reform(ideal,nmaps,swidth,ny,nim) ; eventually, more elegant
;ideal=transpose(ideal,[1,2,3,0])

status=1


if debug then stop
return
end 
;
;
