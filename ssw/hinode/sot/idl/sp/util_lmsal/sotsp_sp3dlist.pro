function sotsp_sp3dlist,t0,t1,scanid=scanid, $
   hao=hao, lmsal=lmsal, darts=darts, debug=debug, ftimes=ftimes, $
   spobsid=spobsid, local_paths=local_paths, $
   parent_only=parent_only, level=level, cdf=cdf, binfits=binfits, savefile=savefile
;
;+
;   Name: sotsp_sp3dlist
;
;   Purpose: return urls from designated SOT/SP server + Level + Opt
; 
;   Input Parameters:
;      t0,t1 - users time range (optionally, use SCANID keyword)
;
;   Keyword Parameters:
;      level=level - SP Level (def=1)
;      ftimes (output) - times of returned files
;      cdf/binfits(default) - level2 format desired
;      local_paths (switch) - if set, return paths, not urls
;      spobsid (output) - the SOT/SP "obsid" of form YYYYMMDD_HHMMSS
;
;   History:
;      Circa 1-aug-2008 - S.L.Freeland
;      30-jan-2009 - S.L.Freeland - Level 2 Hooks, /CDF, /BINFITS
;       4-feb-2011 - S.L.Freeland - server change (backwardly compat, I think)
;      15-apr-2014 - S.L.Freeland - path change, variable list -> listx to avoid V+8.0 IDL data type collision
;      22-oct-2014 - S.L.Freeland - add /LOCAL_PATH and output SPOBSID keyword/function
;-      

debug=keyword_set(debug)
hao=keyword_set(hao)
lmsal=keyword_set(lmsal)
darts=keyword_set(darts)
prefix='SP3D/'

cdf=keyword_set(cdf)
savefile=keyword_set(savefile)
binfits=1-(cdf or savefile) ; default L2 = Binary Extended FITS

if n_elements(level) eq 0 then level=1
lev='level'+strtrim(level,2)

haotop='http://download.hao.ucar.edu/pub/dwijn/hinode/'+lev
case 1 of 
   hao: l1parent=haotop
   level eq 2: l1parent='http://www.lmsal.com/solarsoft/hinode/level2hao/'   
   lmsal: l1parent='http://www.lmsal.com/solarsoft/hinode/level1hao/'
   else: l1parent=haotop
endcase


case 1 of 
   n_params() eq 2: begin 
      dsub=ssw_time2paths(t0,t1,parent='/')
   endcase
   data_chk(scanid,/string): begin
      dsub=anytim(file2time(scanid),/ecs,/date_only)
   endcase
   else: begin 
      box_message,'Supply time range or scanid keyword'
      return,''
   endcase
endcase

l1urls=concat_dir(l1parent,concat_dir(dsub,prefix)) 
if data_chk(scanid,/string) then begin 
   l1scandirs=concat_dir(l1urls,scanid + (['','/outputFiles/'])(cdf)) 
endif else begin 
   l1scandirs=''
   for i=0,n_elements(l1urls)-1 do begin 
      sock_list,l1urls(i),l1ll
      prepatt='HREF="20'
      ss=where(strpos(l1ll,prepatt) ne -1,idcnt)
      if idcnt eq 0 then prepatt=strlowcase(prepatt)
      ss=where(strpos(l1ll,prepatt) ne -1,idcnt)
      if idcnt gt 0 then begin 
         ids=strextract(l1ll(ss),strmid(prepatt,0,6),'/">')
         l1scandirs=[l1scandirs,concat_dir(l1urls(i),ids)]
      endif
      if n_elements(l1scandirs) gt 0 then l1scandirs=l1scandirs(1:*) else $
         box_message,'No scandirs found..'
   endfor
endelse

ns=n_elements(l1scandirs)

l1scandirs=l1scandirs+(['','/'])(str_lastpos(l1scandirs,'/') ne $
                                strlen(l1scandirs)-1)
retval=l1scandirs ; default
if keyword_set(local_paths) then begin 
   sotsp_parent=get_logenv('sotsp_parent')
   case 1 of 
      sotsp_parent ne '':
      lmsal: sotsp_parent='/archive/hinode/sot'
      else: 
   endcase
   l1paths=str_replace(l1scandirs,'http://www.lmsal.com/solarsoft/hinode',sotsp_parent)
   retval=l1paths
endif
spobsid=strextract(l1scandirs + '/','/SP3D/','/')

if debug then stop,'spobsid,l1scandirs,parent_only
if keyword_set(parent_only) then return,retval ; EARLY EXIT if /PARENT_ONLY

retval=''
 
prepatt='<a href="'
case level of 
   2: begin 
      case 1 of
         cdf:      patt='.fits.nc'
         savefile: patt='>'+scanid+'.sav</a>'
         else:     patt='>'+scanid+'.fits</a>

       endcase
   endcase
   else: begin 
      patt='.fits'
   endcase
endcase
for i=0,ns-1 do begin 
   sock_list,l1scandirs(i),listx
   listx=web_dechunk(listx) ; clean chunking
   fitsss=where(strpos(listx,patt) ne -1, fcnt)
   if fcnt gt 0 then begin
      prepatt=([prepatt,strlowcase(prepatt)])(strpos(listx(fitsss(0)),prepatt) eq -1)
      if (savefile or binfits)*(level eq 2) then begin
         fits=scanid+(['.fits','.sav'])(savefile)
      endif else fits=strextract(listx(fitsss),prepatt,patt) + patt
      furl=l1scandirs(i)+fits
      retval=[temporary(retval),furl]
   endif
endfor

if n_elements(retval) eq 1 then box_message,'No files found' else begin 
   retval=retval(1:*) ; eliminate initial null
   ftimes=file2time(retval,out='ecs') 
endelse

if keyword_set(local_paths) then $
   retval=str_replace(retval,'http://www.lmsal.com/solarsoft/hinode',sotsp_parent)
   
if debug then stop ,'before return'
return,retval
end
