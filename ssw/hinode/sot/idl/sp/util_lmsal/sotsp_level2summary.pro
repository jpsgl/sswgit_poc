pro sotsp_level2summary, l2dirs, thermdrift=thermdrift, outdir=outdir
;
;+
;   Name: sotsp_level2summary
;
;   Purpose: generate www & vovent for SOT-SP Level 2 data
;
;   Input Parameters"
;      l2dirs - list of one or more SOT Level 2 directories
;
;   Keyword Parameters:
;      thermdrift - if set, include thermal drift plot from L1 generation
;
;   History:
;      9-Oct-2007 - S.L.Freeland - to summarize stksimages_sbsp output & ...
;-

tpath=get_logenv('path_http')
ttop=get_logenv('top_http')
set_logenv,'path_http',get_logenv('ssw_path')
set_logenv,'top_http','http://www.lmsal.com/solarsoft'

if not data_chk(l2dirs,/string) then begin 
   box_message,'Need list of Level2 directories...'
   return
endif

nl2=n_elements(l2dirs)
fex=file_exist(l2dirs)
okss=where(fex,okcnt)
badss=where(1-fex,bcnt)

case 1 of 
   okcnt eq nl2:  ; all exist
   bcnt eq nl2: begin 
      box_message,'Cannot find any of your input directories..'
      return
   endcase
   else: begin 
      box_message,['Only found ' + strtrim(okcnt,2) + ' of your L2 paths ', $
                   'Missing: ',l2dirs(badss) ]
   endcase
endcase

l2d=l2dirs(okss)
nl2=okcnt

l1d=str_replace(l2d,'level2','level1')
if n_elements(outdir) eq 0 then outdir=l2d
hdocs=concat_dir(l2d,'index.html')

l2http=str_replace(l2d,'/net/kokuten/archive','http://www.lmsal.com/solarsoft')
l1http=str_replace(l2d,'level2','level1')

for i=0,nl2-1 do begin ; for each L2 directory
   box_message,'Processing>> ' + l2d(i)
   stks=file_search(l2d(i),'stksimoutput.save')
   if stks(0) eq '' then begin 
      box_message,'Warning: No stksimages_sbsp output file found..'
   endif else begin 
      stksdata=ssw_save2struct(stks(0))
      l1files=file_search(l1d(i),'*fits')
      l1times=file2time(l1files,out='ecs')
      l1urls=str_replace(l1files,'/net/kokuten/archive','http://www.lmsal.com/solarsoft')
      l1urls=str_replace(l1urls,'/sot','')
       
;     create a map index reflecting entire scan/l1input
      avtime=anytim((total(anytim(l1times))/n_elements(l1times)),/ecs)
      read_sot,[l1files(0),last_nelem(l1files)],index
      index2fov,index,/extremes,center_fov=cfov, size_fov=sfov
      ssw_fake_index,avtime,spindex, fov_as=sfov,xcen=sfov(0),ycen=sfov(1),$  
        cdelt1=index(0).cdelt2
; 
      thermsave=(file_search(l1d(i),'therm*save'))(0)
      if file_exist(thermsave) then thermdata=ssw_save2struct(thermsave)
      html_doc,hdocs(i),/header
      file_append,hdocs(i),hinode_credits(/sot,/url)
      cont=bytscl(gt_tagval(stksdata,/conti))
      ;long=bytscl(gt_tagval(stksdata,/blapp),min=-25,max=25)
      long=bytscl(gt_tagval(stksdata,/blapp),min=-2000,max=2000)
      long=ssw_sigscale(spindex,gt_tagval(stksdata,/blapp)>(-1500)<1500)
      ;trans=ssw_sigscale(spindex,gt_tagval(stksdata,/btapp))
      trans=gt_tagval(stksdata,/btapp)
      trans=256-bytscl(trans,min=0,max=2000 < max(trans))
      labels=['Continuum','Logitudinal','Transverse','Velocity (6301.5 &#x212b)'] 
      gfiles=str2arr('cont,long,trans,veloc,therm,context')
      veloc=stksdata.ctrline(*,*,0)
      veloc=bytscl(veloc,min=median(veloc)-2,max=median(veloc)+2)
      tfiles=gfiles+'_thumb'
      mfiles=gfiles+'_micon'
      cts=[0,0,0,0]
      delvarx,all_micon
      for g=0,3 do begin 
         estat=execute('idat='+gfiles(g))
         wdef,xx,/zbuffer,im=idat
         loadct,cts(g)
         tvscl,idat
         zbuff2file2,concat_dir(l2d(i),gfiles(g)+'.png')
         ithumb=confac(idat,.4)
         wdef,xx,im=ithumb,/zbuffer
         tvscl,ithumb
         zbuff2file2,concat_dir(l2d(i),tfiles(g)+'.png')
         ithumb=confac(idat,.1)
         wdef,xx,im=ithumb,/zbuffer
         tvscl,ithumb
         zbuff2file2,concat_dir(l2d(i),mfiles(g)+'.png')
         if n_elements(all_micon) eq 0 then all_micon=bytscl(ithumb) else $
            all_micon=[all_micon,bytscl(ithumb)]
      endfor
      wdef,xx,im=all_micon,/zbuffer
      tv,all_micon
      zbuff2file2,concat_dir(l2d(i),'clt_micon.png')
      tab=str2html(gfiles(0:3)+'.png',link=tfiles(0:3)+'.png')      
      tab=[[labels],[tab]]
      file_append,hdocs(i),strtab2html(tab,/row0header)
;     Thermal drift
      wdef,xx,650,400, /zbuffer
      linecolors
      utplot,l1times,thermdata.wdelw,background=11,color=7,$
          title='Thermal Drift History Along Slit w/smoothed fit & Cadence',psym=3
      outplot,l1times,thermdata.fitww,color=5
      evt_grid,l1times,ticklen=.01,tickpos=.2,color=4
      zbuff2file2,concat_dir(l2d(i),'thermaldrift.png')
      file_append,hdocs(i), $
      ['<p><font color=orange size=+1> Thermal Drift (Level0 -> Level1)', $
       '</font>',strtab2html('<img src="thermaldrift.png">')]
      utplot,l1times,29.0-thermdata.avctr,background=11,color=7,$
          title='Average Centerline w/smoothed fit & Cadence',psym=3
      outplot,l1times,thermdata.fitav,color=5
      evt_grid,l1times,ticklen=.01,tickpos=.2,color=4
      zbuff2file2,concat_dir(l2d(i),'avectr.png')
      file_append,hdocs(i), $
      ['<p><font color=orange size=+1> Average Centerline (Level0 -> Level1)', $
       '</font>',strtab2html('<img src="avectr.png">')]

      stksmovie=[[[cont]],[[long]],[[trans]]]
      utimes=anytim(anytim(replicate(avtime,3))+[-1,0,1])
      ;image2movie,stksmovie,label=['Continum','Logitudnal','Transverse'],$
      ;/no_film, /context, /xrt,movie_name='stksimages_sbsp',movie_dir=l2d(i),$
      ;/still, html=html, thumbsize=512,tperline=1, uttimes=utimes, charsize=.5
      ; file_append,hdocs(i),html
      file_append,hdocs(i),rd_tfile('sotsp.txt')
      l2subdir=ssw_strsplit(l2d(i),'/',/last,/tail) 
      file_append,hdocs(i),["<font size=+1>IDL> </font>",$
      "<font color=blue> stks=sotsp_stks2struct('"+l2subdir+"')</font>"]
      file_append,hdocs(i),['<br> The above will copy data from the ',$
      "SOT/SP Level2 server; Then, for example<br>",$
      "<font size=+1>IDL> </font>",$
      "<font color=blue> help,stks,/str</font><br>", $
      "Output structure tags map 1:1 to save file 'Output Parameters' description in doc header of ",$
      '<a href="http://sohowww.nascom.nasa.gov/solarsoft/hinode/sot/idl/sp/util/stksimages_sbsp.pro">stksimages_sbsp.pro</a>'] 

      file_append,hdocs(i),['<p>',hinode_credits(/url)]
      html_doc,hdocs(i),/trailer
      
   endelse
endfor

return
end

   
