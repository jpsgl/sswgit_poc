pro sotsp_level2dsummary, l2files, link_list=link_list, table=table, $
   debug=debug, qonly=qonly, sot2ssw=sot2ssw

debug=keyword_set(debug)

set_logenv,'path_http',concat_dir(get_logenv('ssw_path'),'hinode')
set_logenv,'top_http','http://www.lmsal.com/solarsoft/hinode'

spids=strextract(l2files,'SP3D/','/')

bad=where(strlen(spids) ne 15,bss)
if bss gt 0 then begin 
   box_message,['Poorly formed l2 file names?',spids(bad)]
   retur
endif

filenames=ssw_strsplit(l2files,'/',/tail,head=paths)

tdir=''
if keyword_set(table) then tdir='_'+strtrim(table,2) else table=0
tab=table
help,tab,table

outpaths=str_replace(paths,'level2d','level2dd') + tdir

ups=all_vals(outpaths)

mdirs=outpaths
mnams='sotsp_level2_'+spids
for i=0,n_elements(ups)-1 do if 1-file_exist(ups(i)) then mk_dir, ups(i) ; create 

hdocs=concat_dir(outpaths,mnams)+'.html'
hindex=concat_dir(outpaths,'index.html')
set_plot,'z'
if n_elements(table) eq 0 then table=0
tab=table
help,tab,table

idlcmds="<p><font color=orange size=+1> sotsp_stks2index,'" + spids + "',index,data,/LEVEL2</font><br>"
idlcmdss="<p><font color=orange size=+1> sotsp_stks2index,'" + spids + "',index,data,/LEVEL2,/SCALE </font> ; SP Team scaling applied</font><br>"

l1urls='http://sot.lmsal.com/data/sot/level1d/' +  $
   ssw_strsplit(paths,'level2d',/tail) + '/index.html'
l2urls=str_replace(l1urls,'level1d','level2dd')
qonly=keyword_set(qonly)
sot2ssw=keyword_set(sot2ssw)
generate=(1-qonly) and (1-sot2ssw)
if  generate then  begin 
for i=0,n_elements(l2files) -1 do begin 
   table=tab
   help,table,tab,i
   delvarx,dd
   read_sotsp,l2files(i), ii,dd, /scale,/los_mag, scan_info=scan_info
   nim=n_elements(ii)
   ii.date_obs=anytim(anytim(ii.date_obs)+(findgen(nim)),/ccsds)
   labels=spids(i) + '   ' + ii.ftype 
   delvarx,oii,odd,status,error
   sotsp_reform,ii,dd,oii,odd, status=status, scan_info=scan_info, $
      only_maps=[31,33,0,1,2],/concat, error=error
   if not error then begin 
   mxy=([0,250])(status) 
   thumbsize=([0,250])(1-status)
   image2movie, dd, label=ii.ftype, movie_dir=mdirs(i), $
      thumbsize=thumbsize, maxxy=mxy, corexy=status, $ ; thumbsize=350, $
      movie_name=mnams(i), table=table, html=html,/still, tperline=6
   if status then begin  ; movies for repeat maps....
    dd=odd
    dd(*,0:20,*)=0
if debug then stop,'labels,scan_info,ftype
   sst=where(scan_info.slitpos eq min(scan_info.slitpos))
   sst=sst(0:data_chk(dd,/nimag)-1)
   ftimes=scan_info.times(sst)
   labels=anytim(ftimes,/ecs,/truncate) + ' ' + oii.ftype
   labels=str_replace(labels,'Magnetic Field',' B')
   special_movie, ftimes, dd, /no_gifanimate, table=table, $
      movie_name=mnams(i)+'s', movie_dir=mdirs(i), thumbsize=200, $
      html=html2,label=labels, labstyle='/LC', maxxy=250,outsize=512
   endif
   endif else begin
      box_message,'Error in Level2; constant slit position'
      html='<p><font color=red size=+2>Error in Level2 data - Constant SLITPOS</font>'
   endelse 
   html_doc,hdocs(i),/header
   file_append,hdocs(i),hinode_credits(/url,/sot)
   file_append,hdocs(i),str2html(l1urls(i),$
      link='SP Level1 Summary corresponding to <em>this</em> Level2/Inversion')
   file_append,hdocs(i),html  ; thumbnail table
   if status then file_append,hdocs(i),html2 ; "movie" 
   file_append,hdocs(i),rd_tfile('$SSW/site/idl/util/fpp/sotsp_l2.txt')
   file_append,hdocs(i),idlcmds(i)
   file_append,hdocs(i),['-OR-',idlcmdss(i)]
   file_append,hdocs(i),['<p>',hinode_credits(/url)]
   html_doc,hdocs(i),/trailer
endfor
endif ; (qonly/sot2ssw branch)

set_logenv,'path_http',concat_dir(get_logenv('ssw_path'),'hinode')
set_logenv,'top_http','http://www.lmsal.com/solarsoft/hinode'
sshd=where(file_exist(hdocs),hdcnt)
hdocs=hdocs(sshd)
hindex=hindex(sshd)
if sot2ssw then begin 
    box_message,'sot2ssw,hdocs'
    for i=0,hdcnt-1 do begin 
       dat=rd_tfile(hdocs(i))
       ss=where(strpos(dat,'level1d') ne -1,sscnt)
       if sscnt gt 0 then begin
          dat(ss)=str_replace(dat(ss),'sot.lmsal.com/data/sot', $
                       'www.lmsal.com/solarsoft/hinode')
          file_append,hdocs(i),dat,/new
          if i le 1 then stop,hdocs(i)

       endif
    endfor
endif else $
   html_linklist,hdocs,insert='sundiv.gif',/absolute,/init
for i=0,hdcnt-1 do begin
   file_append,hindex(i),rd_tfile(hdocs(i)),/new
endfor

if debug then stop,'before return'
return
end

   

