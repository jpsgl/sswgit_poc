function sotsp_l2struct2losmag, fitsstruct, scale=scale
;
;+
;   Name: sotsp_l2struct2losmag
;
;   Purpose: Line of Sight B from SOTSP Level2 FITS structure
;
;   Input Parameters:
;      fitsstruct - SOTSP L2 FITS structure (merlin_read_fits output)
;
;   History:
;      25-apr-2008 - S.L.Freeland - standalone func from S.McIntosh prog.
;-

if not required_tags(fitsstruct,'sfld,spsi,salpha') then begin
   box_message,'Expect SOTSP Level2 output structure'
   return,-1
endif

image = fitsstruct.sfld*cos(fitsstruct.spsi*!dtor)*fitsstruct.salpha

return,image
end
