function ssw_binfits2struct, binfits, extensions=extensions, $
   only_tags=only_tags, silent=silent
;+
;   Name: ssw_binfits2struct
;
;   Purpose: read binary extended FITS -> structure; tags 1:1 extensions
;
;   Input Parameters:
;      binfits - file name of binary fits file
;
;   Output:
;      function returns structure; tags 1:1 extensions
;
;   Keyword Parameters:
;      extensions - optional list if EXTENSION numbers to include (def=all)
;      only_tags  - optional list of "tagnames" (EXTENSION TTYPE1)
;
;  Restrictions:
;     Uses execute, so not VM compatible
;     EXTENSTIONS and ONLY_TAGS not implemented yet; check tommorrow
;
;  History:
;     Circa 1-Aug-2008 - S.L.Freeland, originally for SOT/SP Level2 (Merlin)
;     9-feb-2009 - S.L.Freeland - add 'uniq' to param/extension names
;     8-apr-2013 - S.L.Freeland - add /READONLY to fxposit call 
;-
          
if not is_fits(binfits) then begin 
   box_message,'Need Binary Extended FITS file input...
   return,-1
endif

lun=fxposit(binfits,1,/readonly)
next=get_fits_nextend(binfits) ; number of extensions
pnames='p'+strtrim(sindgen(next),2)
parr=strarr(next)
for i=0, next-1 do begin 
   edata=(mrdfits(lun,0,head,silent=silent))
   if data_chk(edata,/struct) then edata=edata.(0)
   estat=execute(pnames(i)+'=edata') ; x(i) 
   pname=fxpar(head,"ttype1")
   if not data_chk(pname,/string) then $
      pname=sotsp_l2_new2old(fxpar(head,"extname"),/ttype)
   parr(i)=pname
   estat=execute(pname+'=temporary(' + pnames(i) + ')') 
endfor
parr=parr(uniq(parr,sort(parr))) ; added 9-feb-2009 to avoild Duplicate exte
free_lun,lun
pstring=strtrim(parr,2)

vals=strcompress(arr2str(strtrim(parr,2)),/remove)
estring='retval=create_struct(pstring,'+vals+')'
estat=execute(estring)
return,retval
end
