pro sotsp_level2index,t0,t1,link_list=link_list

set_logenv,'path_http',concat_dir(get_logenv('ssw_path'),'hinode')

case n_params() of 
   0: begin 
         box_message,'Need intput time or time range...'
         return
   endcase
   1: tg=timegrid(t0,reltime(t0,days=32),/month,/quiet)
   2: tg=timegrid(t0,t1,/month,/quiet)
   else: box_message,'???'
endcase

ng=n_elements(tg)
for m=0,ng-2 do begin 
   l2dirs=sot_time2files(tg(m),tg(m+1),/level2,obs='SP4D')
   if l2dirs(0) ne '' then begin 
      l2dirs=str_replace(l2dirs,'//','/')
      ndirs=n_elements(l2dirs)
      index=concat_dir(l2dirs,'index.html')
      indchk=file_exist(index)
      indok=where(indchk,okcnt) 
      case 1 of 
         okcnt eq ndirs: ; all index.html exist
         okcnt eq 0: box_message,'Level 2 directories but no index.htmls..'
         else: begin
            box_message,'Only a subset of index.htmls found - doing those..'
            l2dirs=l2dirs(indok)
            ndirs=okcnt
            index=index(indok)
         endcase
      endcase
      mdirs=strmids(l2dirs,0,strpos(l2dirs,'200')+8) ; monthly dirs
      relind=str_replace(index,mdirs(0),'')
      mindex=concat_dir(mdirs(0),'month_index.html')
      html_doc,mindex,/header
      file_append,mindex,hinode_credits(/url,/sot)
      spcreds=(rd_tfile('sotsp.txt'))(0:2)
      spcreds=str_replace(spcreds,'<br>SolarSoft','')
      spcreds= $
      [spcreds,'<br><em>SolarSoft</em> users may cut&paste the commands ',$
      ' <font color=orange> in orange </font> from the table below verbim into ', $
      ' an sswidl session to transfer&restore the correponding maps/fluxes ' ,$ 
      ' into a local structure to access the physical parameters  summarized here. ', $
      ' The structure tags (<font color=orange>stks</font>,in these examples) ',$
      ' correspond 1:1 to variables&units described in doc-header of ', $
      ' <a href="http://sohowww.nascom.nasa.gov/solarsoft/hinode/sot/idl/sp/util/stksimages_sbsp.pro"> stksimages_sbsp.pro</a> .<p>']
      file_append,mindex,spcreds
      l2subdir=strmid(relind,14,15)
      hlinks=str2html(relind,link_text=l2subdir)
      micons=file_search(l2dirs,'*clt_micon.png')
      relmics=str_replace(micons,mdirs(0),'')
      ilinks=str2html(relind,link_text=relmics)
      idlcmd= $
        "<font color=orange> stks=sotsp_stks2struct('"+l2subdir+"')</font>" 
      idlcmd=['Verbatim SSWIDL command',idlcmd]
      ilinks=['Continum/Longitudinal/Transverse<br>(Thumbnails -> Details)',ilinks]
      hlinks=['SP Level2 ID',hlinks]
      htable=[[hlinks],[idlcmd],[ilinks]] 
      file_append,mindex, $
          strtab2html(transpose(htable),padd=1,spac=1,border=2,/left,/row0)
      file_append,mindex,hinode_credits(/url,/mission)
      html_doc,mindex,/trailer
   endif else box_message,'No Level2 dirs for ' + arr2str([tg(m),tg(m+1)],'->')
endfor

return
end
