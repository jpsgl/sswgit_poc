function extra2struct,_extra=_extra
return,_extra
end

function ssw_image_stats, index, data, normal=normal, $
   limits=limits, percents=percents, missing=missing
;
case 1 of
   n_elements(limits) gt 0: lim=limits
   n_elements(percents) gt 0: lim=.01*percents
   else: lim=[0.01,0.10,0.25,0.75,0.90,0.95,0.98,0.99] ; secchi defaults
endcase

dsort=data[sort(data)]
st=n_elements(dsort)
dlim=lim*st
percentile=dsort(dlim)
stags='DATAP'+string(lim*100,format='(I2.2)')
vals=strtrim(percentile,2)

estring='stats=extra2struct('+arr2str(stags+'='+vals)+')'
estat=execute(estring)
return,stats
end


 


