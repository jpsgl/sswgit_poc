pro sotsp_getdata, spid, level=level, outdir=outdir, $
   no_subdir=nosubdir, urls=urls, no_copy=no_copy, $
   lmsal=lmsal, binfits=binfits, cdf=cdf, savefile=savefile
;+ 
;   Name: sotsp_getdata
;
;   Purpose: get higher order Hinode SOT-SP data->local
;
;   Input Parameters:
;      spid - YYYYMMDD_HHMMSS group/map ID (generally, 1st image in map)
;
;   Keyword Parameters:
;      lmsal - (switch) - if set, from lmsal  (default, for now)
;      outdir  - optional parent directory for output files
;      no_subdir - don't create <parent>/<yyyymmdd_hhmmss>/ ; data-><parent>
;      urls=urls - (output) L1 FITS url list 
;      no_get=no_get - (switch) - don't actually transfer
;                                 (but URLS output is valid)
;
;   Calling Example:
;      IDL> sotsp_getdata,'20070517_161737', outdir=outdir
;      Above transfers Level1 files associated with:
;      http://sot.lmsal.com/data/sot/level1d/2007/05/17/SP3D/20070517_161737/
;
;   Restrictions:
;      I would assume so - let me know
;
;   History:
;      Circa 1-aug-2008 - S.L.Freeland
;      30-jan-2009 - S.L.Freeland - add Level2 hooks (binfits/cdf -> sotsp_3dlist)
;-

copy=1-keyword_set(no_copy)  ; default server->local
no_subdir=keyword_set(no_subdir)
hao=keyword_set(hao)
if n_elements(spid) eq 0 then spid=''
if strlen(spid) ne 15 then begin
   box_message,'Input needs to be YYYYMMDD_HHMMSS SOT/SP Level1 ID'
   return
endif

lmsal=1-hao  ; default - TODO; allow environmental for SP host server
if n_elements(outdir) eq 0 then outdir=curdir()
odir=outdir

; sotsp_sp3dlist does the web spid 2 URLS piece
listx=sotsp_sp3dlist(scanid=spid,lmsal=lmsal,hao=hao,level=level,cdf=cdf,binfits=binfits, savefile=savefile)
if listx(0) ne '' then begin
   if not keyword_set(no_subdir) then odir=concat_dir(odir,spid)
   if not file_exist(odir) then mk_dir,odir ; create if required
   sock_copy,listx,out_dir=odir
endif
   
return
end

