function sotsp_l2_new2old,newextname, short=short, ttype1=ttype1
;
;+
;    Name: sotsp_new2old
;
;    Purpose: helper for L2 format change (loss of FITS extension keywords)
;
;    Input Parameters:
;       newextname - 'EXTNAME' from "New Format" file
;    
     Output:
;       function returns old paradigm name -or- TTYPE1 (aka SHORT)
;
;    Keyword Parameters:
;       TTYPE1,SHORT (synonyms) - if set, return the implied TTYPE1
;
;    Context:
;       Original L2 Extension Headers contained TTYPE1, now gone
;       -------- Old -------- 
;       TTYPE1  = 'SFLD    '      /Label for column 1     
;       EXTNAME = 'FieldStrength'      /Merlin : Field Strength     
;       -------- New ---------
;       EXTNAME = 'Field_Strength'     /Merlin : Field Strength (Underscore)
;       (no TTYPE1 included)
;    This function does the mapping wrapping so downline SW works.
;
;    History:
;      22-Jan-2010 - S.L.Freeland
;-

common sotsp_l2_new2old_blk,old,ttype,new, bold

if n_elements(old) eq 0 then begin 

old=str2arr('FieldStrength,FieldInclination,FieldAzimuth,DoppShift1,DoppShift2,DoppWidth,LineStength,Damping,SrcFunc,SrcFuncGrad,MacroTurb,Alpha,Delta,FieldStrengthErr,FieldInclErr,FieldAzimuth,DoppShift1Err,DoppShift2Err,DoppWidthErr,LineStengthErr,DampingErr,SrcFuncErr,SrcFuncGradErr,MacroTurbErr,AlphaError,DeltaError,ChiSq_I,ChiSq_Q,ChiSq_U,ChiSq_V,ChiSq_Tot,LineStength,Polarization,MagStokesV,FitAttrib,NumIt,XCoord,YCoord,Times,MechSlitPos,ScattLight')

ttype=str2arr('SFLD,SPSI,SAZM,SCEN1,SCEN2,SDOP,SETA0,SDMP,SBZERO,SB1MU,VMAC,SALPHA,SDELTA,SFLD_ERR,SPSI_ERR,SAZM_ERR,SCEN1_ERR,SCEN2_ERR,SDOP_ERR,SETA0_ERR,SDMP_ERR,SBZERO_ERR,SB1MU_ERR,VMAC_ERR,SALPHA_ERR,SDELTA_ERR,CHISQ_I,CHISQ_Q,CHISQ_U,CHISQ_V,CHISQ_TOT,SCCT,NET_POL,STOKESV,RCONV,NITER,XCOORD,YCOORD,TIMES,SLITPOS,SCATTPRO')
new=str2arr('Field_Strength,Field_Inclination,Field_Azimuth,Doppler_Shift1,Doppler_Shift2,Doppler_Width,Line_Strength,Damping ,Source_Function,Source_Function_Gradient,Macro_Turbulence,Stray_Light_Fill_Factor,Stray_Light_Shift,Field_Strength_Error,Field_Inclination_Error,Field_Azimuth_Error,Doppler_Shift1_Error,Doppler_Shift2_Error,Doppler_Width_Error,Line_Strength_Error,Damping_Error,Source_Function_Error,Source_Function_Gradient_Error,Macro_Turbulence_Error,Stray_Light_Fill_Factor_Error,Stray_Light_Shift_Error,ChiSq_I ,ChiSq_Q ,ChiSq_U ,ChiSq_V ,ChiSq_Total,Continuum_Intensity,Polarization,StokesV_Magnitude,Fitting_Attribute,Number_of_Iterations,X_Coordinate,Y_Coordinate,Times   ,Mechincal_Slit_Position,Scattered_Light_Profile')

bold=[[old],[ttype]]

endif

ss=where(newextname eq new,ncnt)

tt=keyword_set(ttype1) or keyword_set(short)

if ncnt eq 1 then begin 
   retval=reform(bold(ss,tt)) 
endif else begin 
   box_message,'no match, returning input
   retval=newextname
endelse

if n_elements(retval) eq 1 then retval=retval(0)

return,retval
end
