pro read_sotsp, files, index,data, _extra=_extra, scan_info=scan_info, phead=phead
;
;+
;   Name: read_sotsp
;
;   Purpose: ssw style api for sot/sp level2 (for now)
;
;   Input Parameters:
;      files - SOT/SP Level 2 (merlin/binary extended fits)
;
;   Output Parameters:
;      index,data - ssw compatible "index,data", 1:1 sp maps
;
;   Keyword Parameters:
;      _extra - keywords -> sotsp_l2struct2data via inheritance & -> mrdfits for "non-merlin"
;      scan_info (outpu) -> structure containg 1D vectors
;                           (TIMES, SLITPOS, and SCATT)
;      phead (output) -> primary FITS header(structure)
;      silent (switch) -> inherit via mrdfits
; 
;   Calling Sequence:
;      IDL> read_sotsp, l2file, index, data , scan_info=scan_info
;
;   Calling Example:
;      IDL> read_sotsp,l2,index,data,scan_info=si
;      IDL> help,data,si,index,/str
; DATA            FLOAT     = Array[4156, 256, 36] <<<DATA 
; ** Structure <2663bd4>, 3 tags, length=66944, data length=66944, refs=1:
;   TIMES           STRING    Array[4156]  <<< times per slit position 
;   SLITPOS         FLOAT     Array[4156]  <<< slit pos 
;   SCATTPRO        FLOAT     Array[112]   <<< scattered light profile
; ** Structure <2930804>, 54 tags, length=624, data length=620, refs=1:
;   SIMPLE          INT              1      <<< index.simple
;   BITPIX          LONG                 8  <<< index... etc 
;   (...etc; more tags here...)
;   FITSTAGS        STRING    'SFLD'        <<< ssw/info added info
;   CUNIT1          STRING    'kG'
;   CUNIT2          STRING    'kG'
;   FTYPE           STRING    'Magnetic Field'
;   INSTRUME        STRING    'SOT/SP Magnetic Field' <<< ssw superset added
;   TELESCOP        STRING    'HINODE'
;   CDELT1          FLOAT          0.148565
;   CDELT2          FLOAT          0.159991
;   DATE_OBS        STRING    ' 1-Dec-2006 11:00:47.000'
;   DATE_END        STRING    ' 1-Dec-2006 16:59:52.000'
;   
;
;   History:
;      1-Jun-2008 - S.L.Freeland - ssw interface to merlin/bin extended FITS
;     25-aug-2016 - S.L.Freeland - train to read Ted/Phil style SP "movie" cubes, add PHEAFD output
;-

if not file_exist(files(0)) then begin
   box_message,'Require existing & local SOTSP Level2 filename'
   return
endif

mreadfits,files[0],phead
next=get_fits_nextend(files[0]) ; get #nextesions

if required_tags(phead,'invcode') then begin 
   l2s=merlin_read_fits2(files(0),_extra=_extra)
   sotsp_l2struct2data, l2s, index, data , _extra=_extra, scan_info=scan_info
endif else begin  ; TODO? - additional sanity check for non-Merlin?
   dat=mrdfits(files[0],1,head,_extra=_extra)
   index=replicate(fitshead2struct(head),next)
   data=make_array(data_chk(dat,/nx),data_chk(dat,/ny),type=data_chk(dat,/type),next)
   for e=0,next-1 do begin ; extension loop
      delvarx,dat,head
      dat=mrdfits(files[0],e+1,head,_extra=_extra)
      data[0,0,e]=dat
      index[e]=fitshead2struct(head)
   endfor
endelse

return
end


