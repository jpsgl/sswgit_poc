function sotsp_stks2struct, stksin, http_parent=http_parent, complete_path=complete_path, debug=debug, $
   no_delete=no_delete, outdir=outdir, locfile=locfile
;
;+
;    Name: sotsp_stks2struct
;
;    Purpose: time/url -> SOT-SP stksimages_sbsp -> struct (optional http trans);
;    Input Parameters:
;       stskin - time, local file, SCANID,  or url of SOT-SP level2 
;
;    Output:
;       function returns structure containing SP maps & info
;       ( returns -1 if transfer problem or stks image not yet available)
;
;    Keyword Parameters:
;       http_parent - assumed parent url; <http://...>hinode/level2/....
;                     default='http://www.lmsal.com/solarsoft/'
;       no_delete - if set, do not delete transferred file
;       outdir - optional directory for file transfer (implies /NO_DELETE)
;       locfile (output) - full path of local file
;
;    Calling Sequence:
;       IDL> spstr=sotsp_stks2struct(url)    ; url=remote level1d
;       IDL> spstr=sotsp_stks2struct('SCANID') ; YYYYMMDD_HHMMSS (scan start)
;       IDL> spstr=sotsp_stks2struct(time)   ; maps time->url->transfer->str
;
;   History:
;      9-oct-2007 - S.L.Freeland - ssw client access -> remote L1D data
;     16-oct-2007 - S.L.Freeland - IDL version dependent fix
;                                  (in call to ssw_save2struct)        
;      1-nov-2007 - S.L.Freeland - return -1 if bad transfer...
;      6-feb-2007 - S.L.Freeland - tweak for new org/urlI
;      4-mar-2008 - S.L.Freeland - add /NO_DELETE, OUTDIR and LOCFILE keywords & function
;     17-mar-2008 - S.L.Freeland - Level2 references -> Level1D
;     16-sep-2009 - S.L.Freeland - (temp?) - sot.lmsal.com -> www.lmsal.com link 
;     27-mar-2015 - T. Fredvik - Added keyword complete_path. If set, regard
;                                http_parent as the complete path to the
;                                YYYY/MM/DD sub directories where the L1 files
;                                are stored. 
;  
;-
if not data_chk(stksin,/string) then begin 
   box_message,'Need time, url, or local SOT/SP L1D file'
   return,-1
endif

deleteit=1-keyword_set(no_delete) or file_exist(oudir) ; default is delete

userurl=keyword_set(http_parent)
if not userurl then  $
   http_parent='http://sot.lmsal.com/data/'

case 1 of
   file_exist(stksin) and 1-is_dir(stksin) : locfile=stksin  ; user supplied 
   strpos(stksin,'http:') ne -1 : url=stksin ; user supplied full URL
   strpos(stksin,'_') ne -1 and strlen(stksin) eq 15: begin ; user supp direct
      time=file2time(stksin)
   endcase
   else: time=stksin  ; user supplied time
endcase

if n_elements(time) gt 0 then begin ; try mapping time->url
   subdir=time2file(time,/sec)
   dpath=anytim(time,/ecs,/date_only)
   hour='/H'+strmid(ssw_strsplit(time2file(time),'_',/tail),0,2)+'00'
   hour=''
   spl2path='sot/level1hao/'+dpath+'/SP3D/'+subdir
   IF keyword_set(complete_path) THEN spl2path=dpath+'/SP3D/'+subdir
   url=concat_dir(http_parent,spl2path)
endif
sname=stksin+'_' + 'stksimg.save'
if n_elements(url) gt 0 then begin 
   if strpos(url,sname) eq -1 then $
      url=concat_dir(url,sname)
   if file_exist(outdir) then out_dir=outdir else out_dir=get_temp_dir()
   locfile=concat_dir(out_dir,sname)
   ssw_file_delete,locfile
   box_message,'Tranferring stokes L1D maps -> local..'
   if not userurl then url=str_replace(url,'sot.lmsal.com/data/sot','www.lmsal.com/solarsoft/data/hinode/sot') ; sot.lmsal.com issue??
   if keyword_set(debug) then stop,'url'
   sock_copy,url,out_dir=out_dir,progress=get_logenv('ssw_nox') eq ''
endif
retval=-1
; define default tag list for older IDL versions...
taglist='conti,ctrline,blapp,btapp,pii,pq,pu,pv,pll,ptot,plc,rufaz,polnet,vcs'
if file_exist(locfile) then begin 
   if since_version('6.2') then retval=ssw_save2struct(locfile)  else $ 
      retval=ssw_save2struct(locfile,only_params=taglist) ;
   if data_chk(url,/string) and deleteit then ssw_file_delete,locfile ; clean up...
endif else box_message,'Problem with transfer or not available on server'

return,retval
end
      
