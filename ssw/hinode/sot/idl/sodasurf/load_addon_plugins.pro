;External procedure for loading add-on plugins to the Display Window.

PRO load_addon_plugins, Display_Window=DW

DW->GetProperty, data_obj=data_obj

;Error handling
Catch, Error
IF Error NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error loading add-on plugins.'+!Error_State.Msg)
   Print, ''
   Print, 'load_addon_plugins: ' + !Error_State.Msg
ENDIF

IF OBJ_ISA(DW, "Sodasurf_Display_Window") THEN BEGIN
;======================Add line here to load plugin==========================

   ; Go through the array of 4 ptrs to data_obj.
   ; If data_obj[i] is not a null object,
   ; then load an SOT_TAB for that data obj
   FOR I=0,3 DO BEGIN
	IF (data_obj[I]) NE OBJ_NEW() THEN DW->Register_Plugin, plugin=Obj_New("sot_tab", DW=DW, rank=I)
    ENDFOR
   ; Load tab for histogram
   DW->Register_Plugin, plugin=Obj_New("pdf_tab", DW=DW)

;============================End load plugin=================================
ENDIF

END
