; FUNCTIONNAME:
;       get_custom_cube_ptr.pro
;
; PURPOSE:
;   Function that returns the pointer to an arbitrary 3D
;   data cube selected by the user using the Browse Panel
;   of the Main Window. If the data cube already resides
;   in the system memory, then the corresponding pointer to this
;   cube is returned. Otherwise a new cube is loaded into memory.
;
; CALLING SEQUENCE:
;   This function is called by the load_event method of the Main Module
;   load_event handles user requests to load 3D data cubes into system memory.
;   and is invoked whenever the user clicks on any of the "Load" buttons
;   in the Main Window.
;
; INPUTS:
;   dir: the directory in which the cube is located
;   filename: the file name of the cube
;   variable_name: a string describing the name of the variable stored
;     in filename
;   min: a 3-element integer array giving the coordinates of the vertex
;     of the sub-region to be loaded.
;   max: the other 3-element integer array giving coordinates of the vertex
;     opposite to the one specified by min. max >= min
;     With min=[x0,y0,z0], max=[x1,y1,z1], the subcube c[i,j,k], s.t.
;     [i,j,k] in [min, max] is loaded.
;   cube_size: a 3-element integer array giving the size of the cube
;     stored in filename.
;
; KEYWORD PARAMETERS:
;   KEY1: Document keyword parameters like this. Note that the keyword
;     is shown in ALL CAPS!
;
;   KEY2: Yet another keyword. Try to use the active, present tense
;     when describing your keywords.  For example, if this keyword
;     is just a set or unset flag, say something like:
;     "Set this keyword to use foobar subfloatation. The default
;      is foobar superfloatation."
;
; OUTPUTS:
;   cube_ptr: The pointer to the 3D cube in system memory.
;
; SIDE EFFECTS:
;   Describe "side effects" here.  There aren't any?  Well, just delete
;   this entry.
;
; RESTRICTIONS:
;   Describe any "restrictions" here.  Delete this section if there are
;   no important restrictions.
;
; PROCEDURE:
;   You can describe the foobar superfloatation method being used here.
;   You might not need this section for your routine.
;
; EXAMPLE:
;   cube_ptr = get_custom_cube_ptr(
;
; MODIFICATION HISTORY:
;   Written by:  Mark Cheung, April 2003
;   April, 2003   Any additional mods get described here.  Remember to
;      change the stuff above if you add a new keyword or
;      something!

function get_custom_cube_ptr, dir=dir, filename=filename, variable_name=variable_name,$
     min=min, max=max, cube_size=cube_size

;First get the bounds of the 3D cube to load
proper_max = cube_size - 1 ;The upper bound should be limited to [287, 99, 287]
min = long(min)
a = where(min lt 0) OR (min gt proper_max)
if (n_elements(a) eq 1) AND (a(0) ne -1) then begin
    min[where(min lt 0)] = 0
endif
a = where((max gt proper_max) OR (max lt 0))
if (n_elements(a) eq 1) AND (a(0) ne -1) then begin
   max[a] = proper_max[a]
endif
max = long(max)
xmin = min[0]
ymin = min[1]
zmin = min[2]
xmax = max[0]
ymax = max[1]
zmax = max[2]

;x_size = max[0]-min[0]+1
;y_size = max[1]-min[1]+1
;z_size = max[2]-min[2]+1

print, max, min
;Check if dir has '/' as the last character. If not, append it to dir
last_char = strmid(dir, strlen(dir)-1, 1)
if (last_char ne '/') then dir = dir+'/'

print, "loading custom cube"+dir+filename
close, 1
openr, 1, dir+filename, error=file_error

IF file_error NE 0 THEN BEGIN
    Catch, Cancel=1
    ok = Dialog_Message(!Error_State.Msg $
       + '. Check file name and access permissions.')
    Print, ''
    Print, !Error_State.Msg $
       + '. Check file name and access permissions.'
    RETURN, Ptr_New(findgen(xmax-xmin, ymax-ymin, zmax-zmin))
ENDIF ELSE BEGIN
    ;a = assoc(1,fltarr(cube_size[0],cube_size[1],cube_size[2]))

    ;Error handling
    Catch, Error
   IF Error NE 0 THEN BEGIN
    Catch, Cancel=1
    ok = Dialog_Message('Error with loading custom cube'+ !Error_State.Msg)
    Print, ''
    Print, 'Error with loading custom cube ' + !Error_State.Msg
    RETURN, Ptr_New()
   ENDIF ELSE BEGIN
    temp = assoc(1,fltarr(cube_size[0],cube_size[1],cube_size[2]))
    ;temp = assoc(1,fltarr(cube_size[0],cube_size[1],cube_size[2]))
    cube =temp(0)
    IF MAX(CUBE) GT 1e20 THEN swap_endian_inplace, cube
    close,1
    cube=cube[xmin:xmax, ymin:ymax, zmin:zmax];*sqrt(!PI*4.0)
    cube_ptr = ptr_new(float(cube))
    help, cube
    return, cube_ptr
   ENDELSE
ENDELSE
end
