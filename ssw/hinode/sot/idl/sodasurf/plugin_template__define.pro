; Copyright (c) 2003, Max Planck Institute for Aeronomy
;	Unauthorized reproduction prohibited.
;+
; CLASS_NAME:
;	plugin_template__define
;
; PURPOSE:
;	This is a template for plugin modules of the MURAM GUI tool.
;	Plugin modules are visible as tab widgets in a Display Window.
;
; SUPERCLASSES:
;       Inherits from the plugin class
;
; SUBCLASSES:
;       List classes that inherit from this class, if any.
;
; CREATION:
;       A plugin module is loaded with a Display Window with the following code:
;
; METHODS:
;       The following methods must be declared for a plugin module.
;
;	Update: A procedure for updating the tab in the Display Window belonging
;		to this plugin. This procedure is only called when this tab
;		is the currently selected one in the Display Window.
;
;	Refresh: A procedure for refreshing the tab in the Display Window.
;		When the Display Window is resized, this method is called.
;
;	GetProperty: to retrieve attributes of the plugin
;
;	SetProperty: to set attributes of the plugin
;
;	Init: The creation of the tab within the Display Window is handled in
;		this method. Widget in the tab for this plugin are usually created
;		in this method.
;
;
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, May 2003.
;			Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

PRO plugin_template::Refresh, resize=resize

IF N_ELEMENTS(resize) EQ 0 THEN resize=0

; Error handling for all trapped errors.
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, /Cancel
   ok = Error_Message(Traceback=1, /Error)
   RETURN
ENDIF

IF resize THEN BEGIN
   widget_control, self.drawID, XSize=self.tab_size[0], YSize=self.tab_size[1]
END

self.DW->GetProperty, colorbar = colorbar

self->Update
END

;==================================================================
; METHODNAME:
;       plugin_template::DrawPostscript
;
; PURPOSE:
;	Since IDL handles postscript and X devices differently,
;	this procedure is called when a user does a postscript save/
;	Generally, the code in this method is similar to that of Update.
;
; CALLING SEQUENCE:
;	plugin_template->DrawPostscript
;
;	
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, May 2003
;	May, 2003	Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

PRO plugin_template::DrawPostscript

END

;==================================================================
; METHODNAME:
;       plugin_template::Update
;
; PURPOSE:
;	For updating the contours tab in the Display Window if it is currently selected
;	and some event (user-driven or not) occurs.
;
; CALLING SEQUENCE:
;	plugin_template->Update
;
;	
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, May 2003
;	May, 2003	Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

PRO plugin_template::Update

   self.DW->GetProperty, DW_ID=DW_ID, $		; The widget ID of the Display Window
	DW_tabs = DW_tabs, $			; The widget ID of the Tab widget in the DW
	movie_player = movie_player, $		; The widget ID of the movie_player compound widget
	orientation = orientation, $		; Viewing orientation: 1=x-z, 2=x-y, 3=z-y
	data_obj = data_obj, $			; Object of class data_set. Holds info about data cube
	colorbar = colorbar, $			; Colorbar object of this Display Window
	r=r, g=g, b=b				; Color vectors for plotting and drawing

;=====Retrive information about the data set attached to this Display Window===
data_obj->GetProperty, $
	cube_ptr=cube_ptr, $			; Pointer to data cube
	minValue = minValue, $			; Global min over data cube
	maxValue = maxValue, $			; Global max over data cube
	unit = unit, $				; Units of the data cube
	variable_name = variable_name, $	; Variable name
	variable_object = variable_object	; An object of class "variable_type",
						; or of its sub-class such as "temperature"
						; These objects have methods for loading and handling
						; data of the associated variable.

;===Get the plotting device index of the draw widget in this tab=========
widget_control, self.drawID, get_value=wid

;================Set the current plotting device as wid==================
IF !D.NAME EQ 'X' THEN wset, wid

;======================Load color vectors for plotting===================
TVLCT, r, g, b

;=====Get the frame number from the slider in the movie player module====
widget_control, movie_player, get_uvalue = movie_player_state
widget_control, movie_player_state.frame_slider, get_value=frame_number


;========Do different things depending on the viewing orientation========

case orientation of
   1: begin
	;Do something
   end
   2: begin
	;Do something
   end
   3: begin
	;Do something
   end
   else:begin
	;This option should never be needed.
   end
endcase
	
if (!D.NAME eq 'X') then begin

endif else begin
; Device is postscript

endelse

END

; METHODNAME:
;       plugin_template::SetProperty
;
; PURPOSE:
;	For modifying the attributes of a contours_tab class object
;
; EXAMPLE:
;	The following example creates a Display_Window object named DW and retrives the
;	widget ID of the Display Window using the GetProperty function of this object.
;
;	plugin_temp = Obj_New("plugin_template", DW=DW)
;	plugin_temp->SetProperty, plugin_name="New Plugin Template"
;	
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, May 2003
;	May, 2003	Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

PRO plugin_template::SetProperty, $
	plugin_name = plugin_name, $
	draw_canvas_size = draw_canvas_size

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in SetProperty Method. Returning...')
   Print, ''
   Print, 'plugin_template::SetProperty Method: ' + !Error_State.Msg
ENDIF

IF N_ELEMENTS(plugin_name) NE 0 THEN self.plugin_name=plugin_name
IF N_ELEMENTS(draw_canvas_size) NE 0 THEN self.draw_canvas_size=draw_canvas_size

END

; METHODNAME:
;       plugin_template::GetProperty
;
; PURPOSE:
;	For the retrieval of information about the contours_tab object
;
; CALLING SEQUENCE:
;	plugin_temp = Obj_New("plugin_template", DW=DW)
;	plugin_temp->GetProperty, plugin_name=plugin_name, ...
;
; OPTIONAL INPUTS:
;	plugin_name : Returns the name of this plugin
;	tab_ID: Returns the widget ID of the tab associated with this plugin
;	drawID: Returns the widget ID of the draw widget contained in the contours tab.
;	DW : Returns an object pointer to the Display Window object to which
;		this plugin is attached.
;	draw_canvas_size: The size of the draw widget with widget ID drawID
;
; EXAMPLE:
;	plugin_temp = Obj_New("plugin_template", DW=DW)
;	plugin_temp->GetProperty, plugin_name=plugin_name, ...
;	
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, May 2003
;	May, 2003	Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

PRO plugin_template::GetProperty, $
	plugin_name = plugin_name, $
	tab_ID = tab_ID, $
	DW = DW, $
	drawID = drawID, $
	draw_canvas_size = draw_canvas_size

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in GetProperty Method. Returning...')
   Print, ''
   Print, 'contours_tab::GetProperty Method: ' + !Error_State.Msg
ENDIF

plugin_name = self.plugin_name
tab_ID = self.tab_ID
DW = self.DW
drawID = self.drawID
draw_canvas_size = self.draw_canvas_size

END

; METHODNAME:
;       plugin_template::Init
;
; PURPOSE:
;	For the creation of a the contours_tab plugin and its tab in a Display Window
;
; INPUTS:
;	DW: The Display Window Object to which this plugin is attached.
;
;
;
; EXAMPLE:
;	The following example creates a Display_Window object named DW and retrives the
;	widget ID of the Display Window using the GetProperty function of this object.
;
;	DW = Obj_New("Display_Window")
;	plugin_temp= Obj_New("plugin_template", DW=DW)
;	
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, May 2003
;	April, 2003	Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

FUNCTION plugin_template::INIT, $
	DW=DW

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in INIT Method. Returning...')
   Print, ''
   Print, 'plugin_template::INIT Method: ' + !Error_State.Msg
   RETURN, 0
ENDIF

IF N_ELEMENTS(tab_ID) EQ 0 THEN tab_ID=0L

;=====Pixel tab widget=================================
DW->GetProperty, $
	xsize=xsize, $			; x size of Display Window
	ysize=ysize, $			; y size of Display Window
	DW_ID=DW_ID, $			; Widget ID of Display Window
	DW_tabs=DW_tabs			; Widget ID of Tab widget in Display Window

plugin_tab = widget_base(DW_tabs, Title="Plugin Template", xsize = xsize, ysize=ysize-70)
plugin_draw = widget_draw(plugin_tab, xsize=xsize, ysize=ysize-70) ; Minus 70 in height to accomodate movie_player widget

self.draw_canvas_size = [xsize,ysize-70]
self.tab_size = [xsize,ysize-70]
self.plugin_name='Plugin Template'
self.drawID = plugin_draw
self.tab_ID = plugin_tab
self.DW = DW

;==============Need to store a pointer to this plugin object=======
;==============in the widget uvalue of tab_ID======================
widget_control, self.tab_ID, set_uvalue=self

RETURN, 1
END

PRO plugin_template__define

struct = {plugin_template, $
	drawID:0L, $			; The widget ID of the draw canvas
	draw_canvas_size:intarr(2), $	; The [width,height] of the draw widget with ID=drawID
	INHERITS plugin $		; Inherit methods and attributes from class "plugin"
	}
END
