PRO movie_player_events, event
; Select relevant display window
widget_control, event.top, get_uvalue=DW

   DW->GetProperty, $
	movie_player = movie_player, $
	data_obj = data_obj, $
	DW_ID = DW_ID, $
	drawID = drawID, $
	wbackground = wbackground, $
	colorbar = colorbar	

   (data_obj[0])->GetProperty, cube_ptr=cube_ptr

   ;Get widget ID of the movie_player
   movie_player = movie_player;
   widget_control, movie_player, get_uvalue = movie_player_state
   widget_control, movie_player_state.view_buttons, get_uvalue = view_buttons_state

   max_frames = widget_info(movie_player_state.frame_slider,/slider_min_max)
   max_frames = max_frames[1]
   if (max_frames eq 10000) then begin
	widget_control, movie_player_state.frame_slider, sensitive=0
	widget_control, movie_player_state.forward_button, sensitive=0
	widget_control, movie_player_state.rewind_button, sensitive=0
	max_frames = 0
   endif else begin
	widget_control, movie_player_state.frame_slider, sensitive=1
   endelse

   widget_control, movie_player_state.frame_slider, get_value=frame_number
   if (frame_number ge max_frames) then frame_number=0

   case event.id of
	movie_player_state.forward_button: begin
		frame_number = (frame_number+1) mod max_frames
		widget_control, movie_player_state.frame_slider, set_value=frame_number
		DW->Redraw
	        stop
		end

 	movie_player_state.rewind_button: begin
		frame_number = (frame_number-1) mod max_frames
		widget_control, movie_player_state.frame_slider, set_value=frame_number
		DW->Redraw
		end

	movie_player_state.frame_slider: begin
		DW->Redraw
		end

	movie_player_state.play_button: begin
		play=1
		widget_control, movie_player_state.play_button, set_uvalue=1
	        widget_control, event.top, get_uvalue = DW ;Reference to Display_Window object
		for i=1,max_frames do begin
	           if play then begin
		   frame_number = (frame_number+1) mod max_frames
		   widget_control, movie_player_state.frame_slider, set_value=frame_number
		   DW->Redraw
		   ;DW_update, {id:movie_player, top:event.top}
		   wait, 0.05
	           endif
		   widget_control, movie_player_state.play_button, get_uvalue=play
		endfor
		DW->Redraw
		end

	movie_player_state.pause_button: begin
		widget_control, movie_player_state.play_button, set_uvalue=0		
		end

	view_buttons_state.xz_view: begin
		DW->SetProperty, orientation = 1
		slider_max = n_elements((*cube_ptr)(0,*,0)) - 1
		widget_control, movie_player_state.frame_slider, get_value=frame_number
   		if (frame_number ge slider_max) then $
			widget_control, movie_player_state.frame_slider, set_value=0
		if (slider_max eq 0) then begin
		   slider_max=10000
		   widget_control, movie_player_state.frame_slider, sensitive=0
		   widget_control, movie_player_state.forward_button, sensitive=0
		   widget_control, movie_player_state.rewind_button, sensitive=0
		endif else begin
		   widget_control, movie_player_state.frame_slider, sensitive=1
		   widget_control, movie_player_state.forward_button, sensitive=1
		   widget_control, movie_player_state.rewind_button, sensitive=1
		endelse
   		widget_control, movie_player_state.frame_slider, set_slider_max=slider_max

		;Erase, Color=wbackground
		DW->Refresh, resize=1
		end

	view_buttons_state.xy_view: begin
		DW->SetProperty, orientation = 2
		slider_max = n_elements((*cube_ptr)(0,0,*)) - 1
		widget_control, movie_player_state.frame_slider, get_value=frame_number
   		if (frame_number ge slider_max) then $
			widget_control, movie_player_state.frame_slider, set_value=0
		if (slider_max eq 0) then begin
		   slider_max=10000
		   widget_control, movie_player_state.frame_slider, sensitive=0
		   widget_control, movie_player_state.forward_button, sensitive=0
		   widget_control, movie_player_state.rewind_button, sensitive=0
		endif else begin
		   widget_control, movie_player_state.frame_slider, sensitive=1
		   widget_control, movie_player_state.forward_button, sensitive=1
		   widget_control, movie_player_state.rewind_button, sensitive=1
		endelse
   		widget_control, movie_player_state.frame_slider, set_slider_max=slider_max

		;Erase, Color=wbackground
		DW->Refresh, resize=1
		end

	view_buttons_state.yz_view: begin
		DW->SetProperty, orientation = 3
		slider_max = n_elements((*cube_ptr)(*,0,0)) - 1
		widget_control, movie_player_state.frame_slider, get_value=frame_number
   		if (frame_number ge slider_max) then $
			widget_control, movie_player_state.frame_slider, set_value=0
		if (slider_max eq 0) then begin
		   slider_max=10000
		   widget_control, movie_player_state.frame_slider, sensitive=0
		   widget_control, movie_player_state.forward_button, sensitive=0
		   widget_control, movie_player_state.rewind_button, sensitive=0
		endif else begin
		   widget_control, movie_player_state.frame_slider, sensitive=1
		   widget_control, movie_player_state.forward_button, sensitive=1
		   widget_control, movie_player_state.rewind_button, sensitive=1
		endelse

   		widget_control, movie_player_state.frame_slider, set_slider_max=slider_max
		;Erase, Color=wbackground
		DW->Refresh, resize=1	
		end   

	else: begin
	end

	endcase

END
