;Handles events from the menus of the top level window
pro main_menu_events, event

   widget_control, event.id, get_value=button_label

   case button_label of

	;Kill the window if exit button is clicked.
	"Exit": widget_control, event.top, /destroy
   endcase

end
