PRO sot_profiles_menu_events, event
   ; Reference to Display_Window object
   widget_control, event.top, get_uvalue=DW
   DW->GetProperty, $
    DW_ID = DW_ID, $
    orientation = orientation, $
    main_ID=main_ID, colorbar=colorbar

   menu_id = widget_info(event.id, /parent)
   widget_control, menu_id,  get_uvalue = info
   sot_tab_obj = info.sot_tab_obj
   sot_tab_obj->GetProperty, profiles_window=profiles_window, $
    profiles_menu=profiles_menu, drawID=drawID

   widget_control, event.id, get_value=button_label

   case button_label of

    "Show Profiles": begin
    ; Create a new profiles window
          xsizes=[600, 600, 600] ;The profiles window sizes for the respective orientations
          ysizes=[500, 500, 500]
          xsize=xsizes[orientation-1]
          ysize=ysizes[orientation-1]
          profiles_window=widget_base(xsize=xsize, ysize=ysize, $
       group_leader = DW_ID, title="Orthogonal Profiles Window")
          profiles_draw=widget_draw(profiles_window, frame=1, $
           xsize=xsize-2, ysize=ysize-2, xoffset=1, yoffset=1)
          sot_tab_obj->SetProperty, profiles_orientation=orientation
          widget_control, profiles_window, /realize

          profiles_window_state = {profiles_window:profiles_window,$
        	profiles_draw:profiles_draw, sot_tab_obj:sot_tab_obj, profileID:event.id}
          widget_control, profiles_window, set_uvalue = profiles_window_state

          sot_tab_obj->SetProperty, profiles_window = profiles_window
          widget_control, event.id, set_value = "Hide Profiles"
          xmanager, 'profiles_events', profiles_window, /just_reg, cleanup='sot_profiles_window_cleanup'

    end

    "Hide Profiles": begin
       ;Kill the profiles window
       widget_control, profiles_window, /destroy
       widget_control, event.id, set_value = "Show Profiles"
    end

    "Blur off": begin
        ;Switch blur on/off
       sot_tab_obj->SetProperty, blur_or_not=0l
       sot_tab_obj->Update
       widget_control, event.id, set_value = "Blur on"
    end

    "Blur on": begin
       ;Switch blur on/off
       sot_tab_obj->SetProperty, blur_or_not=1l
       sot_tab_obj->Update
       widget_control, event.id, set_value = "Blur off"
    end

    "Zoom in": begin
       ;Zoom in 
       sot_tab_obj->GetProperty, draw_xsize=draw_xsize, draw_ysize=draw_ysize, tab_size=tab_size, zoom=zoom;, data_obj=data_obj, rank=rank
       ;data_obj[rank]->GetProperty, cube_size=cube_size
       sot_tab_obj->SetProperty, draw_xsize=max([2*draw_xsize,tab_size[0]]), draw_ysize=max([2*draw_ysize, tab_size[1]]), zoom=2*zoom
       sot_tab_obj->Refresh, resize=1
    end

    "Zoom out": begin
       ;Zoom out
       sot_tab_obj->GetProperty, draw_xsize=draw_xsize, draw_ysize=draw_ysize, tab_size=tab_size, zoom=zoom
       sot_tab_obj->SetProperty, draw_xsize=max([draw_xsize/2,tab_size[0]]), draw_ysize=max([draw_ysize/2, tab_size[1]]), zoom=zoom/2.
       sot_tab_obj->Refresh, resize=1
    end

    "Zoom to fit window": begin
       ;Zoom out
       sot_tab_obj->GetProperty, draw_xsize=draw_xsize, draw_ysize=draw_ysize, tab_size=tab_size
       sot_tab_obj->SetProperty, draw_xsize=tab_size[0], draw_ysize=tab_size[1]
       sot_tab_obj->Refresh, resize=1
    end

    "Zoom 1x": begin
       ;Original size
       sot_tab_obj->GetProperty, draw_xsize=draw_xsize, draw_ysize=draw_ysize, tab_size=tab_size
       sot_tab_obj->SetProperty, draw_xsize=tab_size[0], draw_ysize=tab_size[1], zoom=1
       sot_tab_obj->Refresh, resize=1
    end

    'Custom 2D Profile': begin
       IF orientation EQ 1 THEN BEGIN
       widget_control, drawID, get_uvalue = cursor_pos
       print, cursor_pos
       i0 = min([cursor_pos.i0, cursor_pos.i1])
       i1 = max([cursor_pos.i0, cursor_pos.i1])
       j0 = min([cursor_pos.j0, cursor_pos.j1])
       j1 = max([cursor_pos.j0, cursor_pos.j1])
       print, i0, j0, i1, j1
       ; Line is undefined->exit case statement
       if (i0 eq i1) and (j0 eq j1) then break

       p_length = sqrt(float(i1-i0)^2.+float(j1-j0)^2.)
       cosine = (float(i1)-float(i0))/p_length
       sine = (float(j1)-float(j0))/p_length
       N_p = ceil(p_length)
       i_axis = fltarr(N_p)
       k_axis = fltarr(N_p)
       i_axis = i0+findgen(N_p)*cosine
       k_axis = j0+findgen(N_p)*sine

       DW->GetProperty, data_obj = data_obj
       data_obj->GetProperty,$
          cube_ptr = cube_ptr, $
          variable_name=variable_name, $
          variable_obj = variable_obj, $
          dir = dir, nmod=nmod
       N_y = n_elements((*cube_ptr)[0,*,0])
       j_axis = findgen(N_y)
       temp_cube = interpolate(*cube_ptr, i_axis, j_axis, k_axis, /grid)
       help, temp_cube
       export_slice = fltarr(N_p,1,N_y)

       temp_line = fltarr(N_p)
       for h=0,N_y-1,1 do begin
       for g=0,N_p-1,1 do begin
         temp_line[g] = temp_cube[g, h, g]
       endfor
          export_slice[*,0,h] = temp_line
       endfor

       data_obj = Obj_New("data_set", data_type=2, dir=dir, $
          nmod = nmod, cube_ptr=Ptr_New(export_slice), $
          variable_name = variable_name, $
          variable_object = variable_obj, $
          grid_ptr=Ptr_New([i_axis, j_axis, k_axis]))

       DW_new = OBJ_NEW("Display_Window", group_leader=main_ID, data_obj = data_obj)
       ENDIF
    END

    "Line Profile": BEGIN
       sot_tab_obj->GetProperty, line_profile_window=line_profile_window, tab_ID=tab_ID
       IF widget_info(line_profile_window, /valid_ID) THEN BEGIN
       widget_control, line_profile_window, /destroy
       sot_tab_obj->SetProperty, line_profile_window=0L
       ENDIF ELSE BEGIN
       line_profile_window = widget_base(xsize=400,ysize=600, group_leader=tab_ID)
       line_profile_draw = widget_draw(line_profile_window, xsize=400,ysize=600)
       sot_tab_obj->SetProperty, line_profile_window=line_profile_window
       widget_control, line_profile_window, set_uvalue = $
          {line_profile_draw:line_profile_draw, $
          line_profile_window:line_profile_window}
       widget_control, line_profile_window, /realize
       xmanager, 'profiles_events', line_profile_window
       Print, "line_profile_window=", line_profile_window
       ;sot_tab_obj->Update
       ENDELSE
    END

    "Show Colorbar": BEGIN
       widget_control, event.id, set_value = "Hide Colorbar"
       sot_tab_obj->SetProperty, draw_colorbar=1
       sot_tab_obj->Refresh, resize=1
    END

    "Hide Colorbar": BEGIN
       widget_control, event.id, set_value = "Show Colorbar"
       sot_tab_obj->SetProperty, draw_colorbar=0
       sot_tab_obj->Refresh, resize=1
    END

    "Draw Axes Labels": BEGIN
       widget_control, event.id, set_value = "Hide Axes Labels"
       sot_tab_obj->SetProperty, draw_axes_labels=1
       sot_tab_obj->Refresh, resize=1
    END

    "Hide Axes Labels": BEGIN
       widget_control, event.id, set_value = "Draw Axes Labels"
       sot_tab_obj->SetProperty, draw_axes_labels=0
       sot_tab_obj->Refresh, resize=1
    END

    "Scale colors to slice extrema": BEGIN
       ;widget_control, event.id, set_value = "Scale colors to cube extrema"
       sot_tab_obj->SetProperty, tvscl_by_slice=1
       sot_tab_obj->Refresh, resize=1
    END

    "Scale colors to cube extrema": BEGIN
       ;widget_control, event.id, set_value = "Scale colors to slice extrema"
       sot_tab_obj->SetProperty, tvscl_by_slice=0
       sot_tab_obj->Refresh, resize=1
    END

    "99.9%": BEGIN
       ;=======Apply settings changes to surface Tab Plugin=====
       sot_tab_obj->GetProperty, data_obj=data_obj, rank=rank
       data_obj->GetProperty, cube_ptr=cube_ptr
       min=min(*cube_ptr)
       max=max(*cube_ptr)
       hist=histogram(*cube_ptr, min=min, max=max, nbins=100, locations=locations)
       hist=total(hist, /cumulative)/total(hist)
       min = locations[min(where(hist GE 0.0005))]
       max = locations[max(where(hist LE 0.9995))]
       data_obj->SetProperty, colormin=min, colormax=max
       sot_tab_obj->SetProperty, tvscl_by_slice=2
       sot_tab_obj->Refresh, resize=1
    END


    "99%": BEGIN
       ;=======Apply settings changes to surface Tab Plugin=====
       sot_tab_obj->GetProperty, data_obj=data_obj, rank=rank
       data_obj->GetProperty, cube_ptr=cube_ptr
       min=min(*cube_ptr)
       max=max(*cube_ptr)
       hist=histogram(*cube_ptr, min=min, max=max, nbins=100, locations=locations)
       hist=total(hist, /cumulative)/total(hist)
       min = locations[min(where(hist GE 0.005))]
       max = locations[max(where(hist LE 0.995))]
       data_obj->SetProperty, colormin=min, colormax=max
       sot_tab_obj->SetProperty, tvscl_by_slice=2
       sot_tab_obj->Refresh, resize=1
    END

    "95%": BEGIN
       ;=======Apply settings changes to surface Tab Plugin=====
       sot_tab_obj->GetProperty, data_obj=data_obj, rank=rank
       data_obj->GetProperty, cube_ptr=cube_ptr
       min=min(*cube_ptr)
       max=max(*cube_ptr)
       hist=histogram(*cube_ptr, min=min, max=max, nbins=100, locations=locations)
       hist=total(hist, /cumulative)/total(hist)
       min = locations[min(where(hist GE 0.025))]
       max = locations[max(where(hist LE 0.975))]
       data_obj->SetProperty, colormin=min, colormax=max
       sot_tab_obj->SetProperty, tvscl_by_slice=2
       sot_tab_obj->Refresh, resize=1
    END

   "Scale colors to range:": BEGIN
       sot_tab_obj->GetProperty, data_obj = data_obj
       
       IF N_ELEMENTS(range_settings_window) EQ 0 THEN BEGIN
       ;===============Build new Settings Window================
       range_settings_window = widget_base(xsize=250,ysize=90,row=3, $
          title="Range settings", group_leader=DW_ID, $
          event_pro="sot_profiles_menu_events")
       data_obj->GetProperty, colormin=colormin, colormax=colormax

       min_label  = widget_label(range_settings_window, value="Min")
       min_text = widget_text(range_settings_window, Value=string(colormin, format="(F12.3)"), /editable, event_pro='sot_profiles_menu_events')
       max_label  = widget_label(range_settings_window, value='Max')
       max_text = widget_text(range_settings_window, Value=string(colormax, format="(F12.3)"), /editable, event_pro='sot_profiles_menu_events')

       apply_button = widget_button(range_settings_window, value="Apply", event_pro=$
          "sot_range_settings_window_events")
       done_button = widget_button(range_settings_window, value="Done", event_pro=$
          "sot_range_settings_window_events")

       ;=============Save widget IDs======================
       sot_tab_obj->SetProperty, range_settings_window=range_settings_window
       widget_control, range_settings_window, set_uvalue = {$
          sot_tab_obj:sot_tab_obj, $
          min_text:min_text, max_text:max_text,$
          range_settings_window:range_settings_window, data_obj:data_obj}

       ;==========Realize the Settings Window================
       widget_control, range_settings_window, /realize
       xmanager, 'sot_profiles_menu_events', range_settings_window, $
          /just_reg, cleanup='sot_range_settings_window_cleanup'

       ;==========Change context menu button label to "Hide Settings"===
       ;widget_control, event.id, set_value="Hide Settings"
       ENDIF
    ENDCASE

    ELSE: BEGIN
    END
   ENDCASE
END

PRO sot_profiles_window_cleanup, profiles_window
    widget_control, profiles_window, get_uvalue = profiles_window_state
    sot_tab_obj = profiles_window_state.sot_tab_obj
    sot_tab_obj->GetProperty, tab_ID=tab_ID
    if (widget_info(tab_ID, /valid_id)) then begin
       sot_tab_obj->SetProperty, profiles_window=0L
    endif

    if (widget_info(profiles_window_state.profileID, /valid_id)) then begin
       widget_control, profiles_window_state.profileID, set_value="Show Profiles"
    endif
    print, "Profile window " + string(profiles_window) + " killed"
END

PRO sot_cursor_events, event
; Coordinates of box containing active cursor region

; Fetch information about Display Window Object
sot_tab_ID=widget_info(event.id, /parent)
widget_control, sot_tab_ID, get_uvalue=sot_tab_obj
sot_tab_obj->GetProperty, DW=DW, $
    profiles_window=profiles_window, $
    line_profile_window = line_profile_window, $
    active_draw_region=active_draw_region, $
    context_menu=context_menu

i0 = active_draw_region[0]
j0 = active_draw_region[1]
i1 = active_draw_region[2]
j1 = active_draw_region[3]

DW->GetProperty, $
    DW_ID=DW_ID, $
    r=r, g=g, b=b, $
    orientation=orientation, $
    movie_player=movie_player

sot_tab_obj->GetProperty, data_obj=data_obj

widget_control, movie_player, get_uvalue=movie_player_state
widget_control, movie_player_state.frame_slider, get_value=frame_number

widget_control, event.id, get_value=wid
wset, wid

if  event.type EQ 1 AND (event.x ge i0) AND (event.x lt i1) AND $
    (event.y ge j0) AND (event.y lt j1) then begin

    data_obj->GetProperty, vertex1=vertex1, vertex2=vertex2, cube_ptr=cube_ptr

    case orientation of
       1: begin
       imin=vertex1[0]
       imax=vertex2[0]
       jmin=vertex1[2]
       jmax=vertex2[2]
       end
       2: begin
       imin=vertex1[0]
       imax=vertex2[0]
       jmin=vertex1[1]
       jmax=vertex2[1]
       end
       3: begin
       imin=vertex1[2]
       imax=vertex2[2]
       jmin=vertex1[1]
       jmax=vertex2[1]
       end
    endcase
    iscale=1
    jscale=1
    if (imax gt imin) then iscale=imax-imin
    if (jmax gt jmin) then jscale=jmax-jmin
    inew = floor((float(event.x-i0)*iscale)/float(i1-i0))
    jnew = floor((float(event.y-j0)*jscale)/float(j1-j0))

    widget_control, event.id, get_uvalue = cursor_state
    cursor_state.i = inew
    cursor_state.j = jnew
    widget_control, event.id, set_uvalue = cursor_state

    ;stop
    print, cursor_state
    case event.release of

       1: begin
       cursor_state.i0 = inew
       cursor_state.j0 = jnew
       cursor_state.x0 = event.x
       cursor_state.y0 = event.y
       if (cursor_state.x0 ne cursor_state.x1) and $
         (cursor_state.y0 ne cursor_state.y1) and $
         (cursor_state.x1 ge i0) and $
         (cursor_state.y1 ge j0) then begin
         sot_tab_obj->Update
         plots, [cursor_state.x0, cursor_state.x1], $
            [cursor_state.y0,cursor_state.y1], /device, psym=6,$
            thick=1.5, color=0
         plots, [cursor_state.x0, cursor_state.x1], $
            [cursor_state.y0,cursor_state.y1], /device, psym=6,$
            thick=1.5, color=0
       endif
       widget_control, event.id, set_uvalue = cursor_state
       print, "C1=",cursor_state.x0, cursor_state.y0
       end

       2:begin
       cursor_state.i1 = inew
       cursor_state.j1 = jnew
       cursor_state.x1 = event.x
       cursor_state.y1 = event.y
       if (cursor_state.x0 ne cursor_state.x1) and $
         (cursor_state.y0 ne cursor_state.y1) and $
         (cursor_state.x0 ge i0) and $
         (cursor_state.y0 ge j0) then begin
         sot_tab_obj->Update
         plots, [cursor_state.x0, cursor_state.x1], $
            [cursor_state.y0,cursor_state.y1], /device, psym=6,$
            thick=1.5, color=0
       endif
       widget_control, event.id, set_uvalue = cursor_state
       print, "C2=",cursor_state.x1, cursor_state.x1
       end

       4:begin
       base_ID = widget_info(event.id, /parent)
       widget_displaycontextmenu, event.id, event.x, event.y, $
         context_menu
       end

       else:begin
       end
    endcase
endif

if event.type EQ 5 AND (event.x ge i0) AND (event.x lt i1) AND $
    (event.y ge j0) AND (event.y lt j1) then begin

    data_obj->GetProperty, vertex1=vertex1, vertex2=vertex2, cube_ptr=cube_ptr

    case orientation of
       1: begin
       imin=vertex1[0]
       imax=vertex2[0]
       jmin=vertex1[2]
       jmax=vertex2[2]
       end
       2: begin
       imin=vertex1[0]
       imax=vertex2[0]
       jmin=vertex1[1]
       jmax=vertex2[1]
       end
       3: begin
       imin=vertex1[2]
       imax=vertex2[2]
       jmin=vertex1[1]
       jmax=vertex2[1]
       end
    endcase

    iscale=1
    jscale=1
    if (imax gt imin) then iscale=imax-imin
    if (jmax gt jmin) then jscale=jmax-jmin
    inew = floor((float(event.x-i0)*iscale)/float(i1-i0))
    jnew = floor((float(event.y-j0)*jscale)/float(j1-j0))

    widget_control, event.id, get_uvalue = cursor_state
    cursor_state.i = inew
    cursor_state.j = jnew

       IF cursor_state.i0 NE cursor_state.i1 AND cursor_state.j0 NE cursor_state.j1 THEN BEGIN
           case orientation of
             1: print, "C1=",cursor_state.i0, cursor_state.j0, (*cube_ptr)[cursor_state.i0,frame_number,cursor_state.j0]
             2: print, "C1=",(data_obj->axis('x'))[cursor_state.i0], (data_obj->axis('y'))[cursor_state.j0],$
		 (data_obj->axis('z'))[frame_number], $
		7.25e7^2*total((*cube_ptr)[min([cursor_state.i0,cursor_state.i1]):max([cursor_state.i0,cursor_state.i1])$
		,min([cursor_state.j0,cursor_state.j1]):max([cursor_state.j0,cursor_state.j1]),frame_number])
             3: print, "C1=",cursor_state.i0, cursor_state.j0, (*cube_ptr)[frame_number,cursor_state.i0,cursor_state.j0]
            endcase
       ENDIF


    case event.release of

       1: begin
       cursor_state.i0 = inew
       cursor_state.j0 = jnew
       cursor_state.x0 = event.x
       cursor_state.y0 = event.y
       if (cursor_state.x0 ne cursor_state.x1) and $
         (cursor_state.y0 ne cursor_state.y1) and $
         (cursor_state.x1 ge i0) and $
         (cursor_state.y1 ge j0) then begin
         sot_tab_obj->Update
         plots, [cursor_state.x0, cursor_state.x1], $
            [cursor_state.y0,cursor_state.y1], /device, psym=6,$
            thick=1.5, color=0
       endif
       end

       2:begin
       cursor_state.i1 = inew
       cursor_state.j1 = jnew
       cursor_state.x1 = event.x
       cursor_state.y1 = event.y
       if (cursor_state.x0 ne cursor_state.x1) and $
         (cursor_state.y0 ne cursor_state.y1) and $
         (cursor_state.x0 ge i0) and $
         (cursor_state.y0 ge j0) then begin
         sot_tab_obj->Update
         plots, [cursor_state.x0, cursor_state.x1], $
            [cursor_state.y0,cursor_state.y1], /device, psym=6,$
            thick=1.5, color=0
       endif
       print, "C2=",cursor_state.x1, cursor_state.x1
       end

       4:begin
       base_ID = widget_info(event.id, /parent)
       widget_displaycontextmenu, event.id, event.x, event.y, $
         context_menu
       end

       else:begin
       end
    endcase

    ; Store the cursor positions in the uvalue of this draw widget.
    widget_control, event.id, set_uvalue = cursor_state

endif


IF widget_info(profiles_window, /valid_id) AND $
    widget_info(DW_ID, /valid_id) AND $
    (event.x ge i0) AND (event.x lt i1) AND $
    (event.y ge j0) AND (event.y lt j1) THEN BEGIN

    ; Draw the cross-sections
    widget_control, profiles_window, get_uvalue = profiles_window_state
    widget_control, profiles_window_state.profiles_draw, get_value = pwid
    wset, pwid
    sot_tab_obj->draw_profiles

ENDIF

IF widget_info(line_profile_window, /valid_id) AND $
    widget_info(DW_ID, /valid_id) AND $
    (event.x ge i0) AND (event.x lt i1) AND $
    (event.y ge j0) AND (event.y lt j1) THEN BEGIN

    ; Draw the line profile
    sot_tab_obj->draw_line_profile

ENDIF
END


PRO sot_range_settings_window_events, event

;=========Retrive Information about Settings Window==========
range_settings_window = event.top
widget_control, range_settings_window, get_uvalue = info

;===Retrieve label of the button that generated this event===

IF (TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_BUTTON') THEN BEGIN
   widget_control, event.id, get_value = button_label

   ;==================Handle different button events==================
   CASE button_label OF

    "Apply": BEGIN
       ;=======Apply settings changes to surface Tab Plugin=====
       widget_control, info.min_text, get_value=min
       widget_control, info.max_text, get_value=max
       info.data_obj->SetProperty, colormin=min, colormax=max
       info.sot_tab_obj->SetProperty, tvscl_by_slice=2
       info.sot_tab_obj->Refresh, resize=1
    ENDCASE

    "Done": BEGIN
       widget_control, range_settings_window, /destroy
       info.sot_tab_obj->SetProperty, range_settings_window=0L
    ENDCASE

    ELSE: BEGIN
       ; This shouldn't happen
    ENDCASE
   END
ENDIF


END

PRO sot_range_settings_window_cleanup, range_settings_window

   widget_control, range_settings_window, get_uvalue = info
   info.sot_tab_obj->GetProperty, context_menu = context_menu
   widget_control, context_menu, get_uvalue = context_menu_state
   ;widget_control, context_menu_state.settings, set_value = "Settings"

   info.sot_tab_obj->SetProperty, range_settings_window = 0L
   widget_control, range_settings_window, /destroy

END
