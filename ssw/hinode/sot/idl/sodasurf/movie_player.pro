PRO movie_player_events, event
; Select relevant display window
widget_control, event.top, get_uvalue=DW

   DW->GetProperty, $
	movie_player = movie_player, $
	data_obj = data_obj, $
	DW_ID = DW_ID, $
	drawID = drawID, $
	wbackground = wbackground, $
	colorbar = colorbar, $
        orientation=orientation

   (data_obj[0])->GetProperty, cube_ptr=cube_ptr

   ;Get widget ID of the movie_player
   movie_player = movie_player;
   widget_control, movie_player, get_uvalue = movie_player_state
   widget_control, movie_player_state.view_buttons, get_uvalue = view_buttons_state

   max_frames = widget_info(movie_player_state.frame_slider,/slider_min_max)
   max_frames = max_frames[1]
   if (max_frames eq 10000) then begin
	widget_control, movie_player_state.frame_slider, sensitive=0
	widget_control, movie_player_state.forward_button, sensitive=0
	widget_control, movie_player_state.rewind_button, sensitive=0
	max_frames = 0
   endif else begin
	widget_control, movie_player_state.frame_slider, sensitive=1
   endelse

   widget_control, movie_player_state.frame_slider, get_value=frame_number
   if (frame_number ge max_frames) then frame_number=0

   case event.id of
	movie_player_state.forward_button: begin
		frame_number = (frame_number+1) mod max_frames
		widget_control, movie_player_state.frame_slider, set_value=frame_number
		DW->Redraw
	        stop
		end

 	movie_player_state.rewind_button: begin
		frame_number = (frame_number-1) mod max_frames
		widget_control, movie_player_state.frame_slider, set_value=frame_number
		DW->Redraw
		end

	movie_player_state.frame_slider: begin
		DW->Redraw
		end

	movie_player_state.play_button: begin
		play=1
		widget_control, movie_player_state.play_button, set_uvalue=1
	        widget_control, event.top, get_uvalue = DW ;Reference to Display_Window object
		for i=1,max_frames do begin
	           if play then begin
		   frame_number = (frame_number+1) mod max_frames
		   widget_control, movie_player_state.frame_slider, set_value=frame_number
		   DW->Redraw
		   ;DW_update, {id:movie_player, top:event.top}
		   wait, 0.05
	           endif
		   widget_control, movie_player_state.play_button, get_uvalue=play
		endfor
		DW->Redraw
		end

	movie_player_state.pause_button: begin
		widget_control, movie_player_state.play_button, set_uvalue=0		
		end

	view_buttons_state.xz_view: begin
		DW->SetProperty, orientation = 1
		slider_max = n_elements((*cube_ptr)(0,*,0)) - 1
		widget_control, movie_player_state.frame_slider, get_value=frame_number
   		if (frame_number ge slider_max) then $
			widget_control, movie_player_state.frame_slider, set_value=0
		if (slider_max eq 0) then begin
		   slider_max=10000
		   widget_control, movie_player_state.frame_slider, sensitive=0
		   widget_control, movie_player_state.forward_button, sensitive=0
		   widget_control, movie_player_state.rewind_button, sensitive=0
		endif else begin
		   widget_control, movie_player_state.frame_slider, sensitive=1
		   widget_control, movie_player_state.forward_button, sensitive=1
		   widget_control, movie_player_state.rewind_button, sensitive=1
                endelse
                widget_control, movie_player_state.bookmark1, sensitive = 0
                widget_control, movie_player_state.bookmark2, sensitive = 0
                widget_control, movie_player_state.bookmark3, sensitive = 0
   		widget_control, movie_player_state.frame_slider, set_slider_max=slider_max

		;Erase, Color=wbackground
		DW->Refresh, resize=1
		end

	view_buttons_state.xy_view: begin
		DW->SetProperty, orientation = 2
		slider_max = n_elements((*cube_ptr)(0,0,*)) - 1
		widget_control, movie_player_state.frame_slider, get_value=frame_number
   		if (frame_number ge slider_max) then $
			widget_control, movie_player_state.frame_slider, set_value=0
		if (slider_max eq 0) then begin
		   slider_max=10000
		   widget_control, movie_player_state.frame_slider, sensitive=0
		   widget_control, movie_player_state.forward_button, sensitive=0
		   widget_control, movie_player_state.rewind_button, sensitive=0
		endif else begin
		   widget_control, movie_player_state.frame_slider, sensitive=1
		   widget_control, movie_player_state.forward_button, sensitive=1
		   widget_control, movie_player_state.rewind_button, sensitive=1
               endelse
                widget_control, movie_player_state.bookmark1, sensitive = 1
                widget_control, movie_player_state.bookmark2, sensitive = 1
                widget_control, movie_player_state.bookmark3, sensitive = 1
   		widget_control, movie_player_state.frame_slider, set_slider_max=slider_max

		;Erase, Color=wbackground
		DW->Refresh, resize=1
		end

	view_buttons_state.yz_view: begin
		DW->SetProperty, orientation = 3
		slider_max = n_elements((*cube_ptr)(*,0,0)) - 1
		widget_control, movie_player_state.frame_slider, get_value=frame_number
   		if (frame_number ge slider_max) then $
			widget_control, movie_player_state.frame_slider, set_value=0
		if (slider_max eq 0) then begin
		   slider_max=10000
		   widget_control, movie_player_state.frame_slider, sensitive=0
		   widget_control, movie_player_state.forward_button, sensitive=0
		   widget_control, movie_player_state.rewind_button, sensitive=0
		endif else begin
		   widget_control, movie_player_state.frame_slider, sensitive=1
		   widget_control, movie_player_state.forward_button, sensitive=1
		   widget_control, movie_player_state.rewind_button, sensitive=1
		endelse
                widget_control, movie_player_state.bookmark1, sensitive = 0
                widget_control, movie_player_state.bookmark2, sensitive = 0
                widget_control, movie_player_state.bookmark3, sensitive = 0
   		widget_control, movie_player_state.frame_slider, set_slider_max=slider_max
		;Erase, Color=wbackground
		DW->Refresh, resize=1	
            end
        movie_player_state.bookmark1: begin
            widget_control, movie_player_state.frame_slider, get_value = frame_number
            widget_control, movie_player_state.bookmark1, set_uvalue   = frame_number
            DW->GetProperty, orientation = orientation, bookmarks=bookmarks
            IF orientation EQ 2 THEN BEGIN
                bookmarks[0] = frame_number
            ENDIF
            DW->SetProperty, bookmarks=bookmarks
        end
        movie_player_state.bookmark2: begin
            widget_control, movie_player_state.frame_slider, get_value = frame_number
            widget_control, movie_player_state.bookmark2, set_uvalue   = frame_number
            DW->GetProperty, orientation = orientation, bookmarks=bookmarks
            IF orientation EQ 2 THEN BEGIN
                bookmarks[1] = frame_number
            ENDIF
            DW->SetProperty, bookmarks=bookmarks
        end
        movie_player_state.bookmark3: begin
            widget_control, movie_player_state.frame_slider, get_value = frame_number
            widget_control, movie_player_state.bookmark3, set_uvalue   = frame_number            
            DW->GetProperty, orientation = orientation, bookmarks=bookmarks
            IF orientation EQ 2 THEN BEGIN
                bookmarks[2] = frame_number
            ENDIF
            DW->SetProperty, bookmarks=bookmarks
        end

	else: begin
	end

	endcase

END


;====================================
;Movie player compound widget
;returns the widget ID of the movie player

function movie_player, parent_base, wxsize, wysize
movie_player = widget_base(parent_base, xsize=wxsize, ysize=80, xoffset=0, yoffset=wysize-85, frame=1)
play_button = widget_button(movie_player, xoffset=2,yoffset=2,xsize=40,$
		ysize=40, value='Play', event_pro='movie_player_events')
pause_button = widget_button(movie_player, xoffset=44,yoffset=2,xsize=40,$
		ysize=40, value='Pause', event_pro='movie_player_events')
rewind_button = widget_button(movie_player, xoffset=86,yoffset=2, xsize=40,$
		ysize=40, value='Rew', event_pro='movie_player_events')
frame_slider = widget_slider(movie_player, xoffset=128, yoffset=0, $
		xsize=wxsize-40-30-2-64-40, ysize=40, /drag,$
		minimum=0, maximum=99, event_pro='movie_player_events')
forward_button = widget_button(movie_player, xoffset=wxsize-42, yoffset=2, xsize=40,$
		ysize=40, value='For', event_pro='movie_player_events')
; Make a set of radio buttons to choose the viewing orientation.
view_buttons = widget_base(movie_player, /exclusive, xsize = 150, ysize=25, xoffset=2, yoffset = 40, column=3)
xz_view = widget_button(view_buttons, value = 'x-z', uvalue = '1', event_pro='movie_player_events')
xy_view = widget_button(view_buttons, value = 'x-y', uvalue = '2', event_pro='movie_player_events')
yz_view = widget_button(view_buttons, value = 'z-y', uvalue = '3', event_pro='movie_player_events')
widget_control, xy_view, /set_button
view_buttons_state = {xz_view:xz_view, xy_view:xy_view, yz_view:yz_view}
widget_control, view_buttons, set_uvalue=view_buttons_state
status_text = widget_text(movie_player, value='Status', xoffset = 160, yoffset = 40, xsize = 25)
bookmark1 = widget_button(movie_player, value = '= Start time', uvalue = '1', event_pro='movie_player_events', xoffset = 360, yoffset = 40, xsize = 80)
bookmark2 = widget_button(movie_player, value = '= Peak time', uvalue = '2', event_pro='movie_player_events', xoffset = 440, yoffset = 40, xsize = 80)
bookmark3 = widget_button(movie_player, value = '= End time', uvalue = '3', event_pro='movie_player_events', xoffset = 520, yoffset = 40, xsize = 80)
widget_control, bookmark1, set_uvalue = 0
widget_control, bookmark2, set_uvalue = 0
widget_control, bookmark3, set_uvalue = 0
movie_player_state = {movie_player:movie_player, play_button:play_button, $
		pause_button:pause_button, rewind_button:rewind_button,$
		frame_slider:frame_slider, forward_button:forward_button, $
		view_buttons:view_buttons, status_text:status_text, $
                bookmark1:bookmark1, bookmark2:bookmark2, bookmark3:bookmark3}
widget_control, movie_player, set_uvalue=movie_player_state
return, movie_player
end
