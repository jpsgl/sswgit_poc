function process_MDI_header, header

Nvar = N_ELEMENTS(header)
varname = strarr(Nvar)
value   = strarr(Nvar)

FOR I=0,Nvar-1 DO BEGIN
   line = strsplit(header[I], '=', /extract)
   IF N_ELEMENTS(line) EQ 2 THEN BEGIN
      varname[I] = line[0]
      value[I]   = line[1]
   ENDIF ELSE BEGIN
      varname[I] = 'COMMENT'
      value[I]   = line[0]
   ENDELSE
ENDFOR

result = { $
	bunit:value[where(strmatch(varname, 'BUNIT*', /fold_case) EQ 1)], $
	bscale:value[where(strmatch(varname, 'BSCALE*', /fold_case) EQ 1)], $
	t_obs:value[where(strmatch(varname, 'T_OBS*', /fold_case) EQ 1)], $
	obs_mode:value[where(strmatch(varname, 'OBS_MODE*', /fold_case) EQ 1)], $
	xcen:float((value[where(strmatch(varname, 'XCEN*', /fold_case) EQ 1)])[0]), $
	ycen:float((value[where(strmatch(varname, 'YCEN*', /fold_case) EQ 1)])[0]), $
	datamax:value[where(strmatch(varname, 'DATAMAX*', /fold_case) EQ 1)], $
	datamin:value[where(strmatch(varname, 'DATAMIN*', /fold_case) EQ 1)], $
	datavals:value[where(strmatch(varname, 'DATAVALS*', /fold_case) EQ 1)], $
	xscale:float((value[where(strmatch(varname, 'XSCALE*', /fold_case) EQ 1)])[0]), $
	yscale:float((value[where(strmatch(varname, 'YSCALE*', /fold_case) EQ 1)])[0]) $

}
return,result
END


;Procedure within the Main Module
;Called by
;(1) the Load Button in the File Panel OR
;(2) the Load button in the Browse Panel

pro load_event, event

;COMMON GLOBAL

data_obj = replicate(Obj_New(),4)
variable_name=strarr(4)

widget_control, event.top, get_uvalue=main_state ;
;widget_control, main_state.FP, get_uvalue=FP_state
widget_control, main_state.CP, get_uvalue=CP_state
;widget_control, main_state.BP, get_uvalue=BP_state
;widget_control, main_state.MDIP, get_uvalue = MDIP_state
widget_control, main_state.SOTP, get_uvalue = SOTP_state
widget_control, main_state.TRACEP, get_uvalue = TRACEP_state

widget_control,  CP_state.x_size_text, get_value = x_size
widget_control,  CP_state.y_size_text, get_value = y_size
widget_control,  CP_state.z_size_text, get_value = z_size
widget_control,  CP_state.x_lbound_text, get_value = x0
widget_control,  CP_state.x_ubound_text, get_value = x1
widget_control,  CP_state.y_lbound_text, get_value = y0
widget_control,  CP_state.y_ubound_text, get_value = y1
widget_control,  CP_state.z_lbound_text, get_value = z0
widget_control,  CP_state.z_ubound_text, get_value = z1
widget_control,  CP_state.x_incr_text, get_value = x_incr
widget_control,  CP_state.y_incr_text, get_value = y_incr
widget_control,  CP_state.z_incr_text, get_value = z_incr
rot = widget_info(CP_state.CP_rotate_droplist, /droplist_select)

x_size = fix(x_size)
y_size = fix(y_size)
z_size = fix(z_size)
;print, x_size, y_size, z_size
x0 = fix(x0)
y0 = fix(y0)
z0 = fix(z0)
x1 = fix(x1)
y1 = fix(y1)
z1 = fix(z1)
z_incr = (fix(z_incr))[0]

okay_to_start_DW = 0

case event.id of

    ;Load SOT data
    SOTP_state.SOTP_load_button: begin
       print, "Load SOT data"
       ;SOTP_state.file_select_Object->GetProperty, dir=dir, filename=prefix
       ;last_char = strmid(dir, strlen(dir)-1, 1)
       ;if (last_char ne '/') then dir = dir+'/'
       widget_control, SOTP_state.SOT_seq_start_text, get_value = tstart
       widget_control, SOTP_state.SOT_seq_stop_text, get_value  = tstop
       widget_control, SOTP_state.SOT_wavelength_droplist, get_value=wavelength
       wave_index = widget_info(SOTP_state.SOT_wavelength_droplist, /droplist_select)
       widget_control, SOTP_state.SOT_obsmode_droplist, get_value=obsmode
       obs_index  = widget_info(SOTP_state.SOT_obsmode_droplist, /droplist_select)
       widget_control, SOTP_state.SOT_naxis1_droplist, get_value=naxis1
       widget_control, SOTP_state.SOT_naxis2_droplist, get_value=naxis2
       naxis1 = naxis1[widget_info(SOTP_state.SOT_naxis1_droplist,/droplist_select)]
       naxis2 = naxis2[widget_info(SOTP_state.SOT_naxis2_droplist,/droplist_select)]
     

       sot_cat, tstart, tstop, cat
       IF size(cat,/type) EQ 2 THEN GOTO, DONOTHING
       FILTER = ''
       IF wave_index GT 0 THEN FILTER = ['wave='+strjoin(strsplit(wavelength[wave_index],' ',/extract),'*')]
       IF obs_index GT 0 THEN FILTER = [FILTER, 'OBS_TYPE='+strjoin(strsplit(obsmode[obs_index],' ',/extract),'*')+'*']
       IF LONG(Naxis1) GT 0 THEN FILTER = [FILTER, 'Naxis1='+Naxis1]
       IF LONG(Naxis2) GT 0 THEN FILTER = [FILTER, 'Naxis2='+Naxis2]
       subcat=cat
       ff = where(FILTER NE '')
       IF ff[0] NE -1 THEN BEGIN
	 FILTER = FILTER[ff]
	 print, filter
         ss = struct_where(cat,search_array=FILTER)
         IF ss[0] NE -1 THEN BEGIN 
		files = sot_cat2files(cat[ss])
	        print, N_ELEMENTS(ss), " files found"
	 ENDIF ELSE BEGIN
	 	error=1
                print, "No files found"
	 ENDELSE
       ENDIF ELSE BEGIN
	 files=sot_cat2files(cat)
       ENDELSE

       IF N_ELEMENTS(files) EQ 0 THEN GOTO, DONOTHING
       
       Nslices   = (z1[0]-z0[0])/z_incr+1
       filenames = strarr(Nslices)
       case rot of
	  0: begin
		data = intarr(x1-x0+1,y1-y0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)
	  endcase
	  1: begin
		data = intarr(y1-y0+1,x1-x0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)		
	  endcase
	  2: begin
		data = intarr(x1-x0+1,y1-y0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)	
	  endcase
	  3: begin
		data = intarr(y1-y0+1,x1-x0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)		
	  endcase
       end
       xcen     = fltarr(Nslices)
       ycen     = fltarr(Nslices)
       time_obs = strarr(Nslices)
       IF files[0] NE -1 THEN BEGIN
         FOR m=z0[0],z1[0],z_incr DO BEGIN
		read_sot, files[m], index, slice
		nxslice = N_ELEMENTS(slice[*,0])
		nyslice = N_ELEMENTS(slice[0,*])
       		case obsmode[obs_index] of
			'SP IQUV 4D array' : BEGIN
				IF m EQ z0[0] THEN BEGIN
					data2=data
					data3=data
					data4=data
				ENDIF
				data[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,0,0]), rot)
				data2[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,0,1]), rot)
				data3[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,0,2]), rot)
				data4[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,0,3]), rot)
				variable_name[0] = "SOT: "+"Stokes I"
				variable_name[1] = "SOT: "+"Stokes Q"
				variable_name[2] = "SOT: "+"Stokes U"
				variable_name[3] = "SOT: "+"Stokes V"
			ENDCASE
	  		'FG shuttered I and V': BEGIN
				IF m EQ z0[0] THEN data2=data
				data[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,0]), rot)
				data2[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,1]), rot)
				variable_name[0] = "SOT: "+"Stokes I"
				variable_name[1] = "SOT: "+"Stokes V"
			ENDCASE
	  		'FG shutterless I and V': BEGIN
				IF m EQ z0[0] THEN data2=data
				data[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,0]), rot)
				data2[*,*,(m-z0[0])/z_incr]=rotate(reform(slice[x0:x1,y0:y1,1]), rot)
				variable_name[0] = "SOT: "+"Stokes I"
				variable_name[1] = "SOT: "+"Stokes V"
			ENDCASE
		  	else: BEGIN
				data[*,*,(m-z0[0])/z_incr]=rotate(slice[x0:x1,y0:y1], rot)
				variable_name[0] = "SOT: "+wavelength[wave_index]
			ENDCASE
        	end
		filenames[(M-z0[0])/z_incr]= filenames[m]
		time_obs[(M-z0[0])/z_incr] = ((cat[ss]).date_obs)[M]
	  	xcen[(M-z0[0])/z_incr]     = ((cat[ss]).xcen)[M]
	  	ycen[(M-z0[0])/z_incr]     = ((cat[ss]).ycen)[M]
         ENDFOR
       ENDIF
 
       deltax = float((cat[ss]).fovx)/(cat[ss]).naxis1
       deltay = float((cat[ss]).fovy)/(cat[ss]).naxis2
       FOR M=0,Nslices-1 DO BEGIN
	   case rot of
		0: begin
		   xaxis[*,M] = (findgen((size(data))[1])-(size(data))[1]/2)*deltax[M]+xcen[M]
	           yaxis[*,M] = (findgen((size(data))[2])-(size(data))[2]/2)*deltay[M]+ycen[M]
		endcase
		1: begin
		   xaxis[*,M] = rotate((findgen((size(data))[1])-(size(data))[1]/2)*deltay[M]+ycen[M],2)
	           yaxis[*,M] = (findgen((size(data))[2])-(size(data))[2]/2)*deltax[M]+xcen[M]
		endcase
		2: begin
		   xaxis[*,M] = rotate(findgen((size(data))[1])*deltax[M]+xcen[M],2)
	           yaxis[*,M] = rotate(findgen((size(data))[2])*deltay[M]+ycen[M],2)
		endcase
		3: begin
		   xaxis[*,M] = findgen((size(data))[1])*deltay[M]+ycen[M]
	           yaxis[*,M] = rotate(findgen((size(data))[2])*deltax[M]+xcen[M],2)
		endcase
	   end
       ENDFOR
  
       
       Nx = (size((data)))[1]
       Ny = (size((data)))[2]
       Nz = (size((data)))[3]
       filenames = Ptr_New(filenames,/no_copy)
       xaxis_ptr = Ptr_new(xaxis, /no_copy)
       yaxis_ptr = Ptr_new(yaxis, /no_copy)
       zaxis_ptr = Ptr_new(time_obs, /no_copy)
       data_obj[0] = Obj_New("data_set", data_type=2, dir=dir, $
          filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
          cube_size=[Nx,Ny,Nz], cube_ptr=PTR_NEW(data, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
	  xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[0])

       IF N_ELEMENTS(data2) NE 0 THEN BEGIN
		data_obj[1] = Obj_New("data_set", data_type=2, dir=dir, $
          filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
          cube_size=[Nx,Ny,Nz], cube_ptr=Ptr_New(data2, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
	  xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[1])
       ENDIF
       IF N_ELEMENTS(data3) NE 0 THEN BEGIN
		data_obj[2] = Obj_New("data_set", data_type=2, dir=dir, $
          filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
          cube_size=[Nx,Ny,Nz], cube_ptr=Ptr_New(data3, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
	  xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[2])
       ENDIF
       IF N_ELEMENTS(data4) NE 0 THEN BEGIN
		data_obj[3] = Obj_New("data_set", data_type=2, dir=dir, $
          filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
          cube_size=[Nx,Ny,Nz], cube_ptr=Ptr_New(data4, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
	  xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[3])
       ENDIF

       okay_to_start_DW = 1
    ENDCASE

    SOTP_state.SOTP_check_button: begin
       print, "Check SOT data"
       ;SOTP_state.file_select_Object->GetProperty, dir=dir, filename=prefix
       ;last_char = strmid(dir, strlen(dir)-1, 1)
       ;if (last_char ne '/') then dir = dir+'/'
       widget_control, SOTP_state.SOT_seq_start_text, get_value = tstart
       widget_control, SOTP_state.SOT_seq_stop_text, get_value  = tstop
       widget_control, SOTP_state.SOT_wavelength_droplist, get_value=wavelength
       wave_index = widget_info(SOTP_state.SOT_wavelength_droplist, /droplist_select)
       widget_control, SOTP_state.SOT_obsmode_droplist, get_value=obsmode
       obs_index  = widget_info(SOTP_state.SOT_obsmode_droplist, /droplist_select)
       widget_control, SOTP_state.SOT_naxis1_droplist, get_value=naxis1
       widget_control, SOTP_state.SOT_naxis2_droplist, get_value=naxis2
       naxis1 = naxis1[widget_info(SOTP_state.SOT_naxis1_droplist,/droplist_select)]
       naxis2 = naxis2[widget_info(SOTP_state.SOT_naxis2_droplist,/droplist_select)]

       refresh=1
       
       sot_cat, tstart, tstop, cat, refresh=1
       IF size(cat,/type) EQ 2 THEN GOTO, DONOTHING
       FILTER = ''
       IF wave_index GT 0 THEN FILTER = ['wave='+strjoin(strsplit(wavelength[wave_index],' ',/extract),'*')]
       ;IF obs_index GT 0 THEN FILTER = [FILTER, 'OBS_TYPE='+obsmode[obs_index]]
       IF obs_index GT 0 THEN FILTER = [FILTER, 'OBS_TYPE='+strjoin(strsplit(obsmode[obs_index],' ',/extract),'*')+'*']
       IF LONG(Naxis1) GT 0 THEN FILTER = [FILTER, 'Naxis1='+Naxis1]
       IF LONG(Naxis2) GT 0 THEN FILTER = [FILTER, 'Naxis2='+Naxis2]
       subcat=cat
       ff = where(FILTER NE '')
       IF ff[0] NE -1 THEN BEGIN
	 FILTER = FILTER[ff]
	 print, filter
         ss = struct_where(cat,search_array=FILTER)
         IF ss[0] NE -1 THEN BEGIN 
		files = sot_cat2files(cat[ss])
	 ENDIF ELSE BEGIN
	 	error=1
		print, "No files found"
	 ENDELSE
       ENDIF ELSE BEGIN
	 files=sot_cat2files(cat)
         ss   =indgen(N_ELEMENTS(cat))
       ENDELSE
       ;stop

       ;table = strarr(4,N_ELEMENTS(files))
       ;table[0,*]   = index.wave
       ;table[1,*]   = string(index.naxis1,format="(I4)")
       ;table[2,*]   = string(index.naxis2,format="(I4)")
       ;table[3,*]   = index.date
       ;index_chart  = widget_base(xsize=300, ysize=400)
       ;table_widget = widget_table(index_chart, xsize=300, ysize=400)
       ;stop
       
       IF N_ELEMENTS(files) EQ 0 THEN BEGIN
	  widget_control,  CP_state.z_size_text, set_value = string(0,format="(I5)")
	  widget_control,  CP_state.z_lbound_text, set_value = string(0,format="(I5)")
          widget_control,  CP_state.z_ubound_text, set_value = string(0,format="(I5)")
	  GOTO, DONOTHING
       ENDIF
       ;stop
       widget_control,  CP_state.x_size_text, set_value = string(max((cat[ss]).naxis1),format="(I5)")
       widget_control,  CP_state.y_size_text, set_value = string(max((cat[ss]).naxis2),format="(I5)")
       widget_control,  CP_state.z_size_text, set_value = string(N_ELEMENTS(files),format="(I5)")
       widget_control,  CP_state.x_lbound_text, set_value = string(0,format="(I5)")
       widget_control,  CP_state.x_ubound_text, set_value = string(max((cat[ss]).naxis1)-1,format="(I5)")
       widget_control,  CP_state.y_lbound_text, set_value = string(0,format="(I5)")
       widget_control,  CP_state.y_ubound_text, set_value = string(max((cat[ss]).naxis2)-1,format="(I5)")
       widget_control,  CP_state.z_lbound_text, set_value = string(0,format="(I5)")
       widget_control,  CP_state.z_ubound_text, set_value = string(N_ELEMENTS(files)-1,format="(I5)")
       ;stop
    ENDCASE

    TraceP_state.TraceP_check_button: begin
       print, "Check Trace data"
       widget_control, TraceP_state.Trace_seq_start_text, get_value = tstart
       widget_control, TraceP_state.Trace_seq_stop_text, get_value  = tstop
       widget_control, TraceP_state.Trace_wavelength_droplist, get_value=wavelength
       wave_index = widget_info(TraceP_state.Trace_wavelength_droplist, /droplist_select)
       widget_control, TraceP_state.Trace_naxis1_text, get_value=naxis1
       widget_control, TraceP_state.Trace_naxis2_text, get_value=naxis2
       widget_control, TraceP_state.TraceP_xcen_text, get_value=xcen_user
       widget_control, TraceP_state.TraceP_ycen_text, get_value=ycen_user
       widget_control, TraceP_state.TraceP_rad_text, get_value=search_rad
       xcen_user = float(xcen_user)
       ycen_user = float(ycen_user)

       refresh=0
       IF N_ELEMENTS(last_refresh) EQ 0 THEN last_refresh=systime(/seconds)
       IF (systime(/seconds) - last_refresh ) GT 3600 THEN refresh=1
       Trace_cat, tstart, tstop, cat, refresh=refresh
       IF size(cat,/type) EQ 2 THEN GOTO, DONOTHING
       FILTER = ''
       IF wave_index GT 0 THEN FILTER = ['wave_len='+strjoin(strsplit(wavelength[wave_index],' ',/extract),'*')]
       IF LONG(Naxis1) GT 0 THEN FILTER = [FILTER, 'Naxis1='+Naxis1]
       IF LONG(Naxis2) GT 0 THEN FILTER = [FILTER, 'Naxis2='+Naxis2]
       subcat=cat
       ff = where(FILTER NE '')
       Nfiles=0
       IF ff[0] NE -1 THEN BEGIN
	 FILTER = FILTER[ff]
	 print, filter
         ss = struct_where(cat,search_array=FILTER)
	 if ss[0] NE -1 THEN BEGIN
		Nfiles=N_Elements(ss)
	 ENDIF ELSE BEGIN
	 	error=1
		print, "No files found"
	        Nfiles = 0
	 ENDELSE
       ENDIF ELSE BEGIN
         Nfiles = N_ELEMENTS(cat)
         ss=INDGEN(N_ELEMENTS(cat))
       ENDELSE

	
       IF Nfiles GT 0 AND (strsplit(search_rad,' ',/extract))[0] NE '*' THEN BEGIN
	   good = INTARR(Nfiles)
	   FOR I=0,Nfiles-1 DO BEGIN
		dist=sqrt((((cat[ss])[I]).xcen-xcen_user)^2.0+(((cat[ss])[I]).ycen-ycen_user)^2.0)
		IF dist LE float(search_rad) THEN good[I] = 1
	   ENDFOR
	   IF total(good) GT 0 THEN ss=ss[where(good EQ 1)]
	   Nfiles=total(good)
       ENDIF
 
       IF Nfiles EQ 0 THEN BEGIN
          widget_control,  CP_state.z_size_text, set_value = string(0,format="(I5)")
          widget_control,  CP_state.z_lbound_text, set_value = string(0,format="(I5)")
          widget_control,  CP_state.z_ubound_text, set_value = string(0,format="(I5)")
  	  GOTO, DONOTHING
       ENDIF ELSE BEGIN
          widget_control,  CP_state.x_size_text, set_value = string(max((cat[ss]).naxis1),format="(I5)")
          widget_control,  CP_state.y_size_text, set_value = string(max((cat[ss]).naxis2),format="(I5)")
          widget_control,  CP_state.z_size_text, set_value = string(Nfiles,format="(I5)")
          widget_control,  CP_state.x_lbound_text, set_value = string(0,format="(I5)")
          widget_control,  CP_state.x_ubound_text, set_value = string(max((cat[ss]).naxis1)-1,format="(I5)")
          widget_control,  CP_state.y_lbound_text, set_value = string(0,format="(I5)")
          widget_control,  CP_state.y_ubound_text, set_value = string(max((cat[ss]).naxis2)-1,format="(I5)")
          widget_control,  CP_state.z_lbound_text, set_value = string(0,format="(I5)")
          widget_control,  CP_state.z_ubound_text, set_value = string(Max([Nfiles-1,0]),format="(I5)")
       ENDELSE
    ENDCASE

    TraceP_state.TraceP_load_button: begin
       print, "Check Trace data"
       widget_control, TraceP_state.TraceP_Calibrate_Group, get_value=calibrate
       ;calibrate is an array storing the state of bistate buttons Normalize, Unspike and Destreak
       widget_control, TraceP_state.Trace_seq_start_text, get_value = tstart
       widget_control, TraceP_state.Trace_seq_stop_text, get_value  = tstop
       widget_control, TraceP_state.Trace_wavelength_droplist, get_value=wavelength
       wave_index = widget_info(TraceP_state.Trace_wavelength_droplist, /droplist_select)
       widget_control, TraceP_state.Trace_naxis1_text, get_value=naxis1
       widget_control, TraceP_state.Trace_naxis2_text, get_value=naxis2
       widget_control, TraceP_state.TraceP_xcen_text, get_value=xcen_user
       widget_control, TraceP_state.TraceP_ycen_text, get_value=ycen_user
       widget_control, TraceP_state.TraceP_rad_text, get_value=search_rad
       xcen_user = float(xcen_user)
       ycen_user = float(ycen_user)

       refresh=0
       IF N_ELEMENTS(last_refresh) EQ 0 THEN last_refresh=systime(/seconds)
       IF (systime(/seconds) - last_refresh ) GT 3600 THEN refresh=1
       Trace_cat, tstart, tstop, cat, refresh=refresh
       IF size(cat,/type) EQ 2 THEN GOTO, DONOTHING
       FILTER = ''
       IF wave_index GT 0 THEN FILTER = ['wave_len='+strjoin(strsplit(wavelength[wave_index],' ',/extract),'*')]
       IF LONG(Naxis1) GT 0 THEN FILTER = [FILTER, 'Naxis1='+Naxis1]
       IF LONG(Naxis2) GT 0 THEN FILTER = [FILTER, 'Naxis2='+Naxis2]
       subcat=cat
       ff = where(FILTER NE '')
       IF ff[0] NE -1 THEN BEGIN
	 FILTER = FILTER[ff]
	 print, filter
         ss = struct_where(cat,search_array=FILTER)
	 IF ss[0] NE -1 THEN BEGIN
         	Nfiles=N_ELEMENTS(ss)
	 ENDIF ELSE BEGIN
	 	error=1
		print, "No files found"
	        Nfiles=0
	 ENDELSE
       ENDIF ELSE BEGIN
         Nfiles = N_ELEMENTS(cat)
	 ss = INDGEN(Nfiles)
       ENDELSE
       IF (strsplit(search_rad,' ',/extract))[0] NE '*' THEN BEGIN
	   good = INTARR(Nfiles)
	   FOR I=0,Nfiles-1 DO BEGIN
		dist=sqrt((((cat[ss])[I]).xcen-xcen_user)^2.0+(((cat[ss])[I]).ycen-ycen_user)^2.0)
		IF dist LE search_rad THEN good[I] = 1
	   ENDFOR
	   IF total(good) GT 0 THEN ss=ss[where(good EQ 1)]
	   Nfiles=total(good)
       ENDIF
       
       Nslices = (z1[0]-z0[0])/z_incr+1
       case rot of
	  0: begin
		data = intarr(x1-x0+1,y1-y0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)
	  endcase
	  1: begin
		data = intarr(y1-y0+1,x1-x0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)		
	  endcase
	  2: begin
		data = intarr(x1-x0+1,y1-y0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)	
	  endcase
	  3: begin
		data = intarr(y1-y0+1,x1-x0+1,Nslices)
	        xaxis  = fltarr((size(data))[1],Nslices)
		yaxis  = fltarr((size(data))[2],Nslices)		
	  endcase
       end
       IF calibrate[3] THEN data=fltarr((size(data))[1:3])
       xcen     = fltarr(Nslices)
       ycen     = fltarr(Nslices)
       time_obs = strarr(Nslices)

       FOR m=z0[0],z1[0],z_incr DO BEGIN
		trace_cat2data, (cat[ss])[m], index, slice
                IF TOTAL(calibrate) GT 0 THEN BEGIN
		        trace_prep, index, slice, index_out, slice_out, $
				normalize=calibrate[0], unspike=calibrate[1], destreak=calibrate[2], float=calibrate[3]
		        slice=slice_out
		ENDIF
		nxslice = N_ELEMENTS(slice[*,0])
		nyslice = N_ELEMENTS(slice[0,*])
		data[0:(x1-x0),0:(y1-y0),(m-z0[0])/z_incr]=rotate(slice[x0:x1,y0:y1], rot)
		variable_name[0] = 'TRACE: '+wavelength[wave_index]
		time_obs[(M-z0[0])/z_incr] = ((cat[ss]).time)[M]
	  	xcen[(M-z0[0])/z_incr]     = ((cat[ss]).xcen)[M]
	  	ycen[(M-z0[0])/z_incr]     = ((cat[ss]).ycen)[M]
       ENDFOR
       deltax = float((cat[ss]).cdelt1)
       deltay = float((cat[ss]).cdelt1)
       FOR M=0,Nslices-1 DO BEGIN
	   case rot of
		0: begin
		   xaxis[*,M] = (findgen((size(data))[1])-0.5*(size(data))[1])*deltax[M]+xcen[M]
	           yaxis[*,M] = (findgen((size(data))[2])-0.5*(size(data))[1])*deltay[M]+ycen[M]
		endcase
		1: begin
		   xaxis[*,M] = rotate((findgen((size(data))[1])-0.5*(size(data))[1])*deltay[M]+ycen[M],2)
	           yaxis[*,M] = (findgen((size(data))[2])-0.5*(size(data))[2])*deltax[M]+xcen[M]
		endcase
		2: begin
		   xaxis[*,M] = rotate((findgen((size(data))[1])-0.5*(size(data))[1])*deltax[M]+xcen[M],2)
	           yaxis[*,M] = rotate((findgen((size(data))[2])-0.5*(size(data))[2])*deltay[M]+ycen[M],2)
		endcase
		3: begin
		   xaxis[*,M] = (findgen((size(data))[1])-(size(data))[1])*deltay[M]+ycen[M]
	           yaxis[*,M] = rotate((findgen((size(data))[2])-(size(data))[2])*deltax[M]+xcen[M],2)
		endcase
	   end
       ENDFOR
  
       
       Nx = N_ELEMENTS(xaxis[*,0])
       Ny = N_ELEMENTS(yaxis[*,0])
       Nz = N_ELEMENTS(time_obs[*,0])
       xaxis_ptr = Ptr_new(xaxis, /no_copy)
       yaxis_ptr = Ptr_new(yaxis, /no_copy)
       zaxis_ptr = Ptr_new(time_obs, /no_copy)

       data_obj[0] = Obj_New("data_set", data_type=2, dir=dir, $
          filename=filename, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
          cube_size=[Nx,Ny,Nz], cube_ptr=PTR_NEW(data, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
	  xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[0])      
       
       okay_to_start_DW = 1
    ENDCASE
	
    ELSE: BEGIN
    ENDCASE
END

IF okay_to_start_DW THEN $
   ;data_obj->GetProperty, variable_name = variable_name
   DW = OBJ_NEW("Display_Window", group_leader=event.top, data_obj = data_obj)

DONOTHING: print, " "
end
