PRO display_Window_TLB_Events, event

; The event handler for top-level base events.

Widget_Control, event.top, Get_UValue=DW
DW->GetProperty, drawID=drawID, $
    DW_tabs = DW_tabs, $
    xsize=xsize, $
    ysize=ysize, $
    r=r, g=g, b=b, $
    plugins_ptr=plugins_ptr

   ; What kind of event is this:

thisEvent = Tag_Names(event, /Structure_Name)

CASE thisEvent OF

   'WIDGET_TAB': BEGIN
   ENDCASE

   'WIDGET_BASE': BEGIN

         ; Resize the draw widget.

;      IF StrUpCase(!Version.OS_Family) NE 'UNIX' THEN BEGIN
;
;         Widget_Control, drawid, XSize=event.x, YSize=event.y
;         xsize = event.x
;         ysize = event.y
;
;      ENDIF ELSE BEGIN

            ; This code added to work-around UNIX resize bug when
            ; TLB has a menu bar in IDL 5.2.

         Widget_Control, event.top, TLB_GET_Size=newsize
         xsize = event.x
         ysize = event.y
     widget_control, DW_tabs, XSize=xsize, YSize=ysize-70
     widget_control, DW_tabs, get_uvalue = DW_tabs_state

     FOR i=0,(N_ELEMENTS(*(plugins_ptr))-1) DO BEGIN
       this_plugin = (*(plugins_ptr))[i]
       IF this_plugin NE Obj_New() THEN $
          this_plugin->ResizeTab, tab_size=[xsize,ysize-100]
     ENDFOR

   ;ENDELSE

         ; Need to refresh the Display Window

      DW->Refresh, resize=1

   ENDCASE

   'WIDGET_KBRD_FOCUS': BEGIN

         ; Keyboard focus events if color protection is turned on.

      IF event.enter EQ 0 THEN BEGIN
         ;Widget_Control, event.top, Set_UValue=info, /No_Copy
         RETURN
      ENDIF

         ; Load colors and execute.

      TVLCT, r, g, b
      print, "Calling from display_Window_TBL_Events"
;      wset, wid
;      erase
;      DW_update, {top:event.top, id:event.id}
      DW->Redraw

      ENDCASE

ENDCASE

DW->SetProperty, xsize=xsize, ysize=ysize

END
;----------------------------------------------------------------------------------------
