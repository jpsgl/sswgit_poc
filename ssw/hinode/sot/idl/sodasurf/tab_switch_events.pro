;Event handling procedure for when user changes tabs
;in the display window

pro tab_switch_events, event

   widget_control, event.top, get_uvalue = DW ;Reference to Display_Window object
   DW->Refresh, resize=1

end
