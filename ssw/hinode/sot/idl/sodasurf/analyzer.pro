; This is the entry point to the MURAM GUI

PRO ANALYZER

COMMON GLOBAL, last_refresh

IF Float(!VERSION.Release) LT 6.0 THEN BEGIN
   error = dialog_message("Error: Must use IDL Version 6.x and above", /error, /center)
   GOTO, TERMINATE
ENDIF

;======= Retain the image ========
set_plot, "X"
device, decompose=1, retain=2

;======Add user home directory to IDL system path=====
IF NOT(strcmp(!PATH, "~/.muramgui:", 2)) THEN !PATH = "~/.muramgui:~/:"+!PATH

;=========Attempt to load user configurations=========

default_path = '~'
nmod = '000000'

close, 7
openr, 7, '~/.muramgui/my_config', error=config_file_error
IF config_file_error EQ 0 THEN BEGIN
   lines = strarr(5)
   fields = strarr(5)
   values = strarr(5)
   i = 0
   WHILE NOT(EOF(7)) AND (i LE 5) DO BEGIN
    line = '' & READF, 7, line
    split_line = strsplit(line, '=', /extract)
    IF (N_ELEMENTS(split_line) EQ 2) THEN BEGIN
       fields[i] = split_line[0]
       values[i] = split_line[1]
       i=i+1
    ENDIF
   ENDWHILE

   default_path = values[where(fields EQ 'default_path')]
   nmod = values[where(fields EQ 'default_nmod')]

ENDIF
close, 7
;#############################################
;Windows Configuration
;#############################################
;Make nine plot windows

winID = [20,21,22,23,24,25,26,27,28]

; Window visible?
win_vis = intarr(9)
win_vis[*] = 0

winscale= 1.0
winxpos = 0
winypos = 0

;Size of Panels
FP_xsize = 350
FP_ysize = 280

;Size of SOT Panel
SOTP_xsize = FP_xsize
SOTP_ysize = 220

;Size of TRACE Panel
TraceP_xsize=FP_xsize
TraceP_ysize=220

;Size of Control Panel
CP_xsize = FP_xsize
CP_ysize = 320

;Size of Display Panel
DP_xsize = 0
DP_ysize = CP_ysize + SOTP_ysize + TraceP_ysize



;Size of Browse Panel
;BP_xsize = FP_xsize
;BP_ysize = 120
;Size of MDI Panel
;MDIP_xsize = FP_xsize
;MDIP_ysize = 180

header_font = 'TIMES*BOLD*14'
;#############################################
;Initialize and realize widgets
;#############################################

;Top level base main
main = widget_base(title = "SoDaSurf Launchpad", mbar = menubar,$
    scr_xsize = CP_xsize + DP_xsize, scr_ysize = DP_ysize)

;File menu
file_menu = widget_button(menubar, value='File', /MENU)
exit_button = widget_button(file_menu, value = 'Exit', event_pro='main_menu_events')


;;=============================================
;;Initialize File Panel (FP)
;FP = widget_base(main, Title='File Panel', frame=1,$
;    xsize = FP_xsize, ysize = FP_ysize, xoffset = 0, yoffset = 0)
;
;y=10
;
;FP_label = widget_label(FP, value='File Panel', xoffset = 5, yoffset=y+2,$
;    font = header_font)
;
;;Fill File Panel
;y=y+30
;nmod_label = widget_label(FP, value = 'NMOD:', xoffset = 5, yoffset=y+2)
;nmod_text = widget_text(FP, /editable, value=nmod,$
;    xoffset = 5+70, yoffset=y, xsize=30)
;
;y=y+30
;dir_label = widget_label(FP, value = 'Directory:', xoffset = 5, yoffset=y+2)
;dir_text = widget_text(FP, /editable, value=default_path,$
;    xoffset = 5+70, yoffset=y,xsize=30)
;
;y=y+35
;
;LHS = widget_base(FP, xsize = (FP_xsize-10)/2, ysize = 95, yoffset=y, $
;    xoffset=5, row=3)
;RHS = widget_base(FP, xsize = (FP_xsize-10)/2, ysize = 95, yoffset=y, $
;     xoffset=ceil(FP_xsize/2)+5, row=3)
;
;v=obj_new("variable_type")
;v->VariablesListWidget, list_widget_ID = a_list, $
;    parent = LHS, $
;    labelstr = 'a:', $
;    label_ID = a_list_label
;v->VariablesListWidget, list_widget_ID = b_list, $
;    parent = LHS, $
;    labelstr = 'b:', $
;    label_ID = b_list_label
;v->VariablesListWidget, list_widget_ID = c_list, $
;    parent = LHS, $
;    labelstr = 'c:', $
;    label_ID = c_list_label
;v->VariablesListWidget, list_widget_ID = d_list, $
;    parent = RHS, $
;    labelstr = 'd:', $
;    label_ID = d_list_label
;v->VariablesListWidget, list_widget_ID = e_list, $
;    parent = RHS, $
;    labelstr = 'e:', $
;    label_ID = e_list_label
;v->VariablesListWidget, list_widget_ID = f_list, $
;    parent = RHS, $
;    labelstr = 'f:', $
;    label_ID = f_list_label
;
;widget_control, a_list, set_droplist_select=1
;
;y=y+110
;exp_label = widget_label(FP, value = "expression:", yoffset = y+4, xoffset=10)
;exp_text = widget_text(FP, value = "a", yoffset=y, xoffset = 90, xsize=16, /editable)
;load_button = widget_button(FP, value='Load', xoffset=220, $
;       yoffset=y, ysize=25, event_pro='load_event')
;
;FP_state = {FP:FP, nmod_label:nmod_label, nmod_text:nmod_text, dir_label:dir_label,$
;    dir_text:dir_text, input_lists_IDs:[a_list, b_list, c_list, d_list, e_list, f_list], $
;    load_button:load_button, exp_text:exp_text}
;
;widget_control, FP, set_uvalue=FP_state


;;=============================================
;;Initialize Browse Panel
;;This Panel allows the user to browse the filesystem for any arbitrary 3D cube to load.
;;The implementation is based on Fannings file selector (fsc_fileselec.pro)
;
;BP = widget_base(main, Title='Browse Panel', frame=1,$
;    xsize = BP_xsize, ysize = BP_ysize, xoffset = 0, yoffset = FP_ysize)
;
;file_select = fsc_fileselect(BP, xsize=18, ObjectRef=file_select_Object)
;file_select_Object->SetProperty, Directory=default_path
;file_select_Object->SetProperty, Filename='result.000000'
;BP_load_button = widget_button(BP, xoffset=5, yoffset=90, value='Load', $
;    event_pro='load_event')
;BP_state = {file_select:file_select, file_select_Object:file_select_Object, $
;    BP_load_button: BP_load_button}
;widget_control, BP, set_uvalue = BP_state
;
;y = FP_ysize+BP_ysize

;;=============================================
;;Initialize MDI Panel
;;This Panel allows the user to load MDI Fits files/sequences
;;The implementation is based on Fannings file selector (fsc_fileselec.pro)
;
;MDIP = widget_base(main, Title='MDI Panel', frame=1,$
;    xsize = MDIP_xsize, ysize = MDIP_ysize, xoffset = 0, yoffset = y)
;MDI_panel_label = widget_label(MDIP, value="Hires MDI loader", yoffset = 2, xoffset=2)
;
;file_selectMDI = fsc_fileselect(MDIP, xsize=18, ObjectRef=file_select_ObjectMDI)
;;file_select_ObjectMDI->SetProperty, Directory="/.automount/shadow/root/shadowd3/mdidata/960815/"
;file_select_ObjectMDI->SetProperty, Directory="~/mdi/"
;file_select_ObjectMDI->SetProperty, Filename='hr_M_1024x640_01h.'
;
;MDI_seq_start_label = widget_label(MDIP, value = "Seq Start:", yoffset = 90, xoffset=10)
;MDI_seq_start_text  = widget_text(MDIP, value = '31734', yoffset=90, xoffset = 90, xsize=16, /editable)
;MDI_seq_stop_label  = widget_label(MDIP, value = "Seq Stop:", yoffset = 118, xoffset=10)
;MDI_seq_stop_text   = widget_text(MDIP, value = '31735', yoffset=118, xoffset = 90, xsize=16, /editable)
;MDI_seq_increment_label  = widget_label(MDIP, value = "Min inc.:", yoffset = 146, xoffset=10)
;MDI_seq_increment_text   = widget_text(MDIP, value = '1', yoffset=146, xoffset = 90, xsize=16, /editable)
;
;MDIP_load_button = widget_button(MDIP, xoffset=250, yoffset=90, value='Load', $
;    event_pro='load_event')
;
;MDIP_state = {file_select:file_selectMDI, file_select_Object:file_select_ObjectMDI,$
;	 MDIP_load_button:MDIP_load_button, MDI_seq_start_text:MDI_seq_start_text, $
;	MDI_seq_stop_text:MDI_seq_stop_text, MDI_seq_increment_text:MDI_seq_increment_text}
;
;widget_control, MDIP, set_uvalue = MDIP_state

;=============================================
;Initialize SOT Panel
;This Panel allows the user to load SOT Fits files/sequences
;The implementation is based on Fannings file selector (fsc_fileselec.pro)
y=0

SOTP = widget_base(main, Title='SOT Panel', frame=1,$
    xsize = SOTP_xsize, ysize = SOTP_ysize, xoffset = 0, yoffset = y)
SOT_panel_label = widget_label(SOTP, value="SOT loader", yoffset = 2, xoffset=2)

current_time = strsplit(systime(), ' ', /extract)
year         = current_time[4]
month        = current_time[1]
day          = current_time[2]
IF strlen(day) EQ 1 THEN day = '0'+day
tstart       = day+"-"+month+"-"+year+" 00:00"
tstop        = day+"-"+month+"-"+year+" 23:59"

SOT_seq_start_label = widget_label(SOTP, value = "Start Time:", yoffset = 24, xoffset=10)
SOT_seq_start_text  = widget_text(SOTP, value = tstart, yoffset=20, xoffset = 90, xsize=25, /editable)
SOT_seq_stop_label  = widget_label(SOTP, value = "Stop Time:", yoffset = 54, xoffset=10)
SOT_seq_stop_text   = widget_text(SOTP, value = tstop, yoffset=50, xoffset = 90, xsize=25, /editable)
SOT_wavelength_label = widget_label(SOTP, value = 'Wavelength:', xoffset=10, yoffset=84)
wavelength_arr = ['','BFI no move', 'CN bandhead 3883','Ca II H line','G band 4305','blue cont 4504',$
'green cont 5550','red cont 6684','NFI no move','TF Mg I 5172','TF Fe I 5250','TF Fe I 5576','TF Fe I 6302',$
'TF H I 6563','TF Na I 5896','laser line 6328','laser line 5430','laser line 5940','laser line 6117',$
'TF Mg I 5172 base','TF Fe I 5250 base','TF Fe I 5576 base','TF Fe I 6302 base','TF H I 6563 base',$
'TF Na I 5896 base','laser line 6328 base','solaser line 5430 base','laser line 5940 base',$
'laser line 6117 base']
SOT_wavelength_droplist = widget_droplist(SOTP, value=wavelength_arr, uvalue = 'wavelength', yoffset=80, xoffset=80)

SOT_obsmode_label    = widget_label(SOTP, value='Obs. mode:', yoffset=115, xoffset=10)
obstype_arr = ['','FG (simple)','FG shuttered I and V','FG shuttered IQUV','FG MG4 V/I','FG MG4 V and I',$
	'FG MG2 V/I','FG MG2 V and I','FG MG1 V/I','FG DG4 single V','FG DG4 two V','FG DG2','FG focus scan',$
	'FG Iscan','FG Laser tune','FG EMI test','FG shutterless I and V','FG Shutterless IQUV',$
	'FG Shutterless I and Q','FG Shutterless I and U','FG Shutterless IUV','FG shutterless IV 0.2sec',$
	'FG shutterless IV 0.1sec','SP IQUV 4D array','CT reference frame']
SOT_obsmode_droplist = widget_droplist(SOTP, value=obstype_arr, uvalue='obstype', yoffset=111, xoffset=80)

;SOT_seq_increment_label  = widget_label(SOTP, value = "Cadence (min):", yoffset = 148, xoffset=10)
;SOT_seq_increment_text   = widget_text(SOTP, value = '1', yoffset=144, xoffset = 100, xsize=16, /editable)
SOT_naxis1_label = widget_label(SOTP, value='Naxis1:', xoffset=10, yoffset=154)
;SOT_naxis1_text  = widget_text(SOTP, value='2048', xoffset=70, yoffset=150, xsize=6, /editable)
SOT_naxis1_droplist=widget_droplist(SOTP,value=['*','49','112','192','400','512','1024','2048','4096']$
	,xoffset=45, yoffset=150, xsize=6)
SOT_naxis2_label = widget_label(SOTP, value='Naxis2:', xoffset=130, yoffset=154)
;SOT_naxis2_text  = widget_text(SOTP, value='2048', xoffset=180, yoffset=150, xsize=6, /editable)
SOT_naxis2_droplist=widget_droplist(SOTP,value=['*','50','512','1024','2048','4096']$
	,xoffset=165, yoffset=150, xsize=6)
SOTP_check_button = widget_button(SOTP, xoffset=250, yoffset=144, value='Check', event_pro='load_event')
SOTP_load_button = widget_button(SOTP, xoffset=250, yoffset=174, value='Load', event_pro='load_event')

SOTP_state = {SOTP_load_button:SOTP_load_button, SOT_seq_start_text:SOT_seq_start_text, $
	 SOT_seq_stop_text:SOT_seq_stop_text, $
	 SOT_wavelength_droplist:SOT_wavelength_droplist, SOT_obsmode_droplist:SOT_obsmode_droplist,$
	 SOT_naxis1_droplist:SOT_naxis1_droplist, SOT_naxis2_droplist:SOT_naxis2_droplist,$
	 SOTP_check_button:SOTP_check_button}

widget_control, SOTP, set_uvalue = SOTP_state

;=============================================


;=============================================
;Initialize Trace Panel
;This Panel allows the user to load Trace Fits files/sequences
;The implementation is based on Fannings file selector (fsc_fileselec.pro)
y=SOTP_ysize

TraceP = widget_base(main, Title='Trace Panel', frame=1,$
    xsize = TraceP_xsize, ysize = TraceP_ysize, xoffset = 0, yoffset = y)
Trace_panel_label = widget_label(TraceP, value="Trace loader", yoffset = 2, xoffset=2)

Trace_seq_start_label = widget_label(TraceP, value = "Start Time:", yoffset = 24, xoffset=10)
Trace_seq_start_text  = widget_text(TraceP, value = '26-nov-2006 00:00', yoffset=20, xoffset = 90, xsize=25, /editable)
Trace_seq_stop_label  = widget_label(TraceP, value = "Stop Time:", yoffset = 54, xoffset=10)
Trace_seq_stop_text   = widget_text(TraceP, value = '26-nov-2006 12:00', yoffset=50, xoffset = 90, xsize=25, /editable)
Trace_wavelength_label = widget_label(TraceP, value = 'Wavelength:', xoffset=10, yoffset=84)
wavelength_arr = ['','171','195','284','1216','1550','1600','1700','WL']
Trace_wavelength_droplist = widget_droplist(TraceP, value=wavelength_arr, uvalue = 'wavelength', yoffset=80, xoffset=80)

Trace_naxis1_label = widget_label(TraceP, value='Naxis1:', xoffset=10, yoffset=119)
Trace_naxis1_text  = widget_text(TraceP, value='1024', xoffset=70, yoffset=115, xsize=6, /editable)
Trace_naxis2_label = widget_label(TraceP, value='Naxis2:', xoffset=130, yoffset=119)
Trace_naxis2_text  = widget_text(TraceP, value='1024', xoffset=180, yoffset=115, xsize=6, /editable)

TraceP_check_button = widget_button(TraceP, xoffset=290, yoffset=164, value='Check', event_pro='load_event')
TraceP_load_button = widget_button(TraceP, xoffset=290, yoffset=194, value='Load ', event_pro='load_event')

TraceP_xcen_label = widget_label(TraceP, xoffset=10,yoffset=145+4, value="Xcen: ")
TraceP_xcen_text  = widget_text(TraceP, xoffset=70,yoffset=145, value='0', /editable, xsize=6)
TraceP_ycen_label = widget_label(TraceP, xoffset=130,yoffset=145+4, value="Ycen: ")
TraceP_ycen_text  = widget_text(TraceP, xoffset=180,yoffset=145, value='0', /editable, xsize=6)
TraceP_rad_label  = widget_label(TraceP, xoffset=10,yoffset=178, value="Search radius: ")
TraceP_rad_text   = widget_text(TraceP, xoffset=124,yoffset=168, value='250', /editable, xsize=6)

TraceP_Calibrate_Group = cw_bgroup(TraceP, ["Normalize","Unspike","Destreak","Float"], /nonexclusive, yoffset=192,column=4)

TraceP_state = {TraceP_load_button:TraceP_load_button, Trace_seq_start_text:Trace_seq_start_text, $
	 Trace_seq_stop_text:Trace_seq_stop_text, $
	 Trace_wavelength_droplist:Trace_wavelength_droplist, TraceP_Calibrate_Group:TraceP_Calibrate_Group, $
	 TraceP_xcen_text:TraceP_xcen_text, TraceP_ycen_text:TraceP_ycen_text, TraceP_rad_text:TraceP_rad_text,$
	 Trace_naxis1_text:Trace_naxis1_text, Trace_naxis2_text:Trace_naxis2_text, TraceP_check_button:TraceP_check_button}

widget_control, TraceP, set_uvalue = TraceP_state


;=============================================



;Initialize Control Panel (CP)

CP = widget_base(main, Title='Control Panel', frame=1,$
    xsize = CP_xsize, ysize = CP_ysize, xoffset = 0, yoffset = SOTP_ysize+TraceP_ysize)
y=10
CP_label = widget_label(CP, value = 'Control Panel', xoffset = 5, yoffset = y,$
    font = header_font)
y=y+30
xoff = 70
x_size_label = widget_label(CP, value = "Nx", xoffset = xoff-85, yoffset=y+6)
x_size_text = widget_text(CP, /editable, value='2048', xoffset = xoff-45, yoffset = y+3, xsize = 5)
x_lbound_label = widget_label(CP, value = "x_min", xoffset = xoff+10, yoffset=y+6)
x_lbound_text = widget_text(CP, /editable, value='0', xoffset = 50+xoff, yoffset = y+3, xsize = 5)
x_ubound_label = widget_label(CP, value = "x_max", xoffset = 100+xoff, yoffset=y+6)
x_ubound_text = widget_text(CP, /editable, value='2047', xoffset = 140+xoff, yoffset = y+3, xsize = 5)
x_incr_label = widget_label(CP, value = "x_inc", xoffset = 190+xoff, yoffset=y+6)
x_incr_text = widget_text(CP, value='1', xoffset = 230+xoff, yoffset = y+3, xsize = 2)

y=y+30
y_size_label = widget_label(CP, value = "Ny", xoffset = xoff-85, yoffset=y+6)
y_size_text = widget_text(CP, /editable, value='1024', xoffset = xoff-45, yoffset = y+3, xsize = 5)
y_lbound_label = widget_label(CP, value = "y_min", xoffset = xoff+10, yoffset=y+6)
y_lbound_text = widget_text(CP, /editable, value='0', xoffset = 50+xoff, yoffset = y+3, xsize = 5)
y_ubound_label = widget_label(CP, value = "y_max", xoffset = 100+xoff, yoffset=y+6)
y_ubound_text = widget_text(CP, /editable, value='1023', xoffset = 140+xoff, yoffset = y+3, xsize = 5)
y_incr_label = widget_label(CP, value = "y_inc", xoffset = 190+xoff, yoffset=y+6)
y_incr_text = widget_text(CP, value='1', xoffset = 230+xoff, yoffset = y+3, xsize = 2)

y=y+30
z_size_label = widget_label(CP, value = "Nz", xoffset = xoff-85, yoffset=y+6)
z_size_text = widget_text(CP, /editable, value='0', xoffset = xoff-45, yoffset = y+3, xsize = 5)
z_lbound_label = widget_label(CP, value = "z_min", xoffset = xoff+10, yoffset=y+6)
z_lbound_text = widget_text(CP, /editable, value='0', xoffset = 50+xoff, yoffset = y+3, xsize = 5)
z_ubound_label = widget_label(CP, value = "z_max", xoffset = 100+xoff, yoffset=y+6)
z_ubound_text = widget_text(CP, /editable, value='0', xoffset = 140+xoff, yoffset = y+3, xsize = 5)
z_incr_label = widget_label(CP, value = "z_inc", xoffset = 190+xoff, yoffset=y+6)
z_incr_text = widget_text(CP, /editable, value='1', xoffset = 230+xoff, yoffset = y+3, xsize = 2)

y=y+30
;CP_swap_endian_base = widget_base(CP, xoffset=10, yoffset=y, /nonexclusive, $
;    xsize=120, ysize=20 )
;CP_swap_endian_checkbox = widget_button(CP_swap_endian_base,value="Swap endian?",$
;     xoffset=0, yoffset=0)
CP_rotate_label = widget_label(CP, xoffset=xoff-85, yoffset=143, value="Rotate: ")
CP_rotate_droplist = widget_droplist(CP, xoffset=xoff-20, yoffset=135, value=['0','90 deg ccw', '90 deg cw', '180 deg'], uvalue='rotate')

y=y+50
CP_label = widget_label(CP, value = 'System Log', xoffset = 5, yoffset = y, $
    font=header_font)

y=y+20
log = [systime()+": SOT o'Ayam started"]
log_text = widget_text(CP, frame=1, /scroll, /wrap, value = log, uvalue=log,$
    xoffset=5, yoffset=y, xsize = 48, ysize = 6)

;Array to hold list of Display Window IDs and associated files
DW_IDs = replicate(0l,10)
DW_dirs = replicate("a",10)
DW_nmods = replicate("a",10)
DW_quantities = replicate("a",10)

CP_state = {CP:CP, CP_label:CP_label, log_text:log_text, DW_IDs:DW_IDs,$
    DW_dirs:DW_dirs,DW_nmods:DW_nmods,DW_quantities:DW_quantities, $
    x_lbound_text:x_lbound_text, y_lbound_text:y_lbound_text,$
    z_lbound_text:z_lbound_text, x_ubound_text:x_ubound_text, $
    y_ubound_text:y_ubound_text, z_ubound_text:z_ubound_text, $
    x_size_text:x_size_text, y_size_text:y_size_text, z_size_text:z_size_text, CP_rotate_droplist:CP_rotate_droplist,$
    x_incr_text:x_incr_text, y_incr_text:y_incr_text, z_incr_text:z_incr_text}

widget_control, CP, set_uvalue=CP_state

;=============================================
;Array to hold object pointers to the data_obj's loaded into system memory.
;This array can be accessed by other modules.

data_obj_list = objarr(10)

;Define main_state
main_state = {main:main, CP:CP, data_obj_list:data_obj_list,  SOTP:SOTP, TraceP:TraceP}
widget_control, main, set_uvalue=main_state

;=================================
;Create and realize and the Main window (and its children widgets)

IF !VERSION.OS_FAMILY EQ 'unix' THEN BEGIN
	set_plot, 'X'
ENDIF ELSE BEGIN
	set_plot, 'WIN'
ENDELSE

widget_control, /realize, main

;Register with Xmanager
xmanager, 'simple', main, /no_block

TERMINATE: print, ""

end

pro drop_list_event, event
end

pro simple_event, event
end
