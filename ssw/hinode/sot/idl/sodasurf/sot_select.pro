;+
; NAME:
;       sot_select_ev
; PURPOSE:
;       The event handler for sot_select
;
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL 
;-

pro sot_select_event, ev

widget_control, ev.id, get_uvalue=uval

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, test, filnam, ss, result, filnam_sel, index, data,m,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat,index_sel, data_sel, dum1, big_base

current_day = strmid(reltime(days=-1),0,2)
current_month = strmid(reltime(days=-1),3,3)
current_year = '20'+strmid(reltime(days=-1),7,2); put reltime in current date/time instead of international

hours_arr = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17','18', '19', '20','21', '22','23']
minutes_arr = ['00', '10', '20','30', '40', '50']
minutes_arr1 = ['00', '10', '20','30', '40', '50']
days_arr= [current_day,strcompress(indgen(31)+1)]
months_arr= [current_month,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
years_arr= [current_year,'2006', '2007', '2008']

hours=hours_arr[widget_info(w_hours, /droplist_select)]
minutes=minutes_arr[widget_info(w_minutes,/droplist_select)]
day=days_arr[widget_info(w_day,/droplist_select)]
month=months_arr[widget_info(w_month,/droplist_select)]
year=years_arr[widget_info(w_year,/droplist_select)]
hours1=hours_arr[widget_info(w_hours1, /droplist_select)]
minutes1=minutes_arr1[widget_info(w_minutes,/droplist_select)]
day1=days_arr[widget_info(w_day1,/droplist_select)]
month1=months_arr[widget_info(w_month1,/droplist_select)]
year1=years_arr[widget_info(w_year1,/droplist_select)]

w_data_arr=['w_hours', 'w_minutes', 'w_day', 'w_month', 'w_year', 'w_hours1', 'w_minutes1', 'w_day1', 'w_month1', 'w_year1','w_obstype1', 'w_wavelength1', 'w_subdirectory1', 'w_nx1', 'w_ny1', 'w_pixel_size1', 'w_ew1', 'w_tolerance1', 'w_ns1', 'w_nstolerance1', 'w_exposure_text', 'w_data_quality_text', 'w_focus_text', 'w_slit_text']

if uval eq 'searchall' then begin

    if dum1 eq 1 then begin
        widget_control, big_base, /destroy  ;flag
    endif

    t0=day+'-'+month+'-'+year+' '+hours+':'+minutes
    t1=day1+'-'+month1+'-'+year1+' '+hours1+':'+minutes
    sot_cat, t0, t1, full_cat
    cat=full_cat

if size(full_cat[0], /type) eq 8 then begin

    big_base = widget_base(ev.top, /column)
    widget_control, big_base, update=0
    greeting = widget_label(big_base, value = 'Please refine search, Wild Cards OK->')

    wildcards = widget_base(big_base, /row)

    obstype_arr = ssw_uniq_modes(cat,'obs_type')
    obstype_arr = ['*',obstype_arr]
    wavelength_arr = ssw_uniq_modes(cat,'wave')
    wavelength_arr = ['*',wavelength_arr]

    obstype = widget_base(wildcards, /column)
    w_obstype1 = cw_bgroup(obstype, obstype_arr, uvalue ='obstype', /exclusive,$
                           /frame, label_top='Obs. TYPE', set_value=0)
 
    wavelength = widget_base(wildcards, /column)
     w_wavelength1 = cw_bgroup(wavelength, wavelength_arr, uvalue = 'wavelength',$
                              /exclusive, /frame, label_top = 'Wavelength', set_value=0)
 
    spatial = widget_base(big_base, /row)

    nx_arr = ssw_uniq_modes(cat, 'naxis1')
    nx_arr = ['*',nx_arr]
    nx = widget_base(spatial, /row)
     w_nx1 = cw_bgroup(nx, nx_arr, uvalue = 'nx', /exclusive, /frame,$
                      label_top = 'NX', set_value=0)

    ny_arr = ssw_uniq_modes(cat, 'naxis2')
    ny_arr = ['*',ny_arr]
    ny = widget_base(spatial, /row)
    w_ny1 = cw_bgroup(ny, ny_arr, uvalue = 'ny', /exclusive, /frame,$
                      label_top = 'NY', set_value=0)

    refine_basis = widget_base(big_base, /row)
    refine_base = widget_base(refine_basis, /row)
    refine_search = widget_button(refine_base, value = 'Refine', uvalue = 'refine')
    widget_control, big_base, /update

    dum1 = 1 ; flag
endif else print, 'try again'

endif

if uval eq 'refine' then begin

    cat=full_cat
    widget_control, w_obstype1, get_value=obs_index
    obstype1=obstype_arr[obs_index]
    widget_control, w_wavelength1, get_value=wave_index
    wavelength1=wavelength_arr[wave_index]
    widget_control, w_nx1, get_value=nx_index
    nx1=nx_arr[nx_index]
    widget_control, w_ny1, get_value=ny_index
    ny1=ny_arr[ny_index]

    sot_list

endif

if uval eq 'dismiss' then begin

    widget_control, ev.top, /destroy

endif

end

PRO sot_select, index_out, data_out

;+
; NAME:
;       sot_select
; PURPOSE:
;       Browse and filter SOT database via GUI, iteratively select files,
;       read and display selections.  Functionality of the GUI is meant to
;       match that of the web-based SOT data browser.
; CALLING SEQUENCE:
;       sot_select [,index_out [,data_out]]
; INPUTS: None so far.
; OUTPUTS:
;       INDEX_OUT - Index record array for selected files
;       DATA_OUT  - Data array for selected files
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL 
;                   - Fixed display button to read data and display it in one click
;                   - Increased y size so more files are available
;                   - Widget now focuses on each selection, rather than the top
;                   - Display is only the atv call
;        1-Feb-2007 - Version 1.3 - Marty Hu LMSAL
;                   - ListBox widget to replace droplist widget with wavelength, obstype, etc. (1/24/07)
;                   - Make use of the multiple keyword (2.1.07)
;-

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, test, filnam, ss, result, filnam_sel, index, data,m,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat,index_sel, data_sel, dum1, big_base

base=widget_base(/column, title='SOT Select')

dum1 = 0 ;;; flag
test= 0
m=0
if not exist(index) then index= -1
if not exist(data) then data= -1

start_search = widget_base(base, /row)
current_day = strmid(reltime(days=-1),0,2)
current_month = strmid(reltime(days=-1),3,3)
current_year = '20'+strmid(reltime(days=-1),7,2)

hours_arr = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17','18', '19', '20','21', '22','23']
minutes_arr = ['00', '10', '20','30', '40', '50']
minutes_arr1 = ['00', '10', '20','30', '40', '50']
days_arr= [current_day,strcompress(indgen(31)+1)]
months_arr= [current_month,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
years_arr= [current_year,'2006', '2007', '2008']

start_time=widget_base(base, /row)
w_day = widget_droplist(start_time, title='Date:', value = days_arr, uvalue='w_day')
w_month = widget_droplist(start_time, value=months_arr, uvalue='w_month')
w_year = widget_droplist(start_time, value = years_arr, uvalue='w_year')
w_hours = widget_droplist(start_time, title='Start Time:', value = hours_arr, uvalue='w_hours')
w_minutes = widget_droplist(start_time, title= ':',value = minutes_arr, uvalue='w_minutes')
stop_time=widget_base(base, /row)
w_day1 = widget_droplist(stop_time, title='Date:', value = days_arr, uvalue='w_day1')
w_month1 = widget_droplist(stop_time, value=months_arr, uvalue='w_month1')
w_year1 = widget_droplist(stop_time, value = years_arr, uvalue='w_year1')
w_hours1 = widget_droplist(stop_time, title='Stop Time: ', value = hours_arr, uvalue='w_hours1')
w_minutes1 = widget_droplist(stop_time, title= ':',value = minutes_arr1, uvalue='w_minutes1')
display_space=widget_label(stop_time, value = '    ')

searchall=widget_base(base, /row)
w_search_all = widget_button(searchall, value = 'Search All', uvalue='searchall')
dismiss = widget_button(searchall, value = 'Dismiss', uvalue = 'dismiss')

widget_control, base, /realize
xmanager, 'sot_select', base, /no_block

index_out = index
data_out = data

end

