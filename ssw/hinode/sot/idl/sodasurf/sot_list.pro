;+
; NAME:
;       sot_list_ev
; PURPOSE:
;       The event handler for sot_list1
;
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL 
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL
;-

pro sot_list_event, ev

widget_control, ev.id, get_uvalue=uval

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, test, filnam, ss, result, filnam_sel, index, data,m,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat, index_sel, data_sel, dum1, big_base

if uval eq 'dismiss' then begin

    if exist(index_sel) then index = index_sel
    if exist(data_sel) then data = data_sel
    widget_control, ev.top, /destroy

endif

if uval eq 'wlist' then begin

    selected=widget_info(ev.id, /list_select)

for ii=0, n_elements(selected)-1 do begin

    if strmid(filnam_sel[selected[ii]],0,1) ne '*' then $
      filnam_sel[selected[ii]] = '*   '+filnam_sel[selected[ii]] else $
      filnam_sel[selected[ii]] = strmid(filnam_sel[selected[ii]], 4, strlen(filnam_sel[selected[ii]]))

endfor

    widget_control, result, set_value=filnam_sel
    widget_control, result, set_list_select=selected[0]

endif

if uval eq 'reset' then begin

    list=get_infox(index, 'TIME_D$OBS, naxis1, naxis2, wave')
    blank=strarr(n_elements(list))
    blank[*]=' '
    filnam_sel=list+ blank + filnam
    filnam_sel=list+ blank + strmid(filnam,strpos(filnam, '/', /reverse_search))
    widget_control, result, set_value=filnam_sel

endif

if uval eq 'read' then begin

    ss=where(strmid(filnam_sel,0,1) eq '*')
    mreadfits, filnam(ss),index_sel,data_sel
    
endif

if uval eq 'display' then begin 

                                ;widget_control, ev.id, get_value=displayarr
;    file1=displayarr[widget_info(ev.id, /droplist_select)]

;    case file1 of
    
;        'Display':
;        'tvscl': begin
;            wdef, 0, index_sel(0).naxis1, index_sel(0).naxis2
;            tvscl, ssw_sigscale(index_sel(0), data_sel(*,*,0))
;        end
;        'iimage': iimage,data_sel(*,*,0)
;        ;'img_summary': img_summary,data_sel(*,*,0)
;        ;'xdisp_fits' : xdisp_fits, dir = filnam[ss[0]]
;        'atv': atv, ssw_sigscale(index_sel(0), data_sel(*,*,0))

    
    ss=where(strmid(filnam_sel,0,1) eq '*')
    mreadfits, filnam(ss),index_sel,data_sel
    
    atv, ssw_sigscale(index_sel(0), data_sel(*,*,0))

;    endcase
endif


;
; sun_visor is  Ralph Seguin's (seguin@lmsal.com) movie tool to
; display multiple FITS and other images as movies.
; It takes a list of FITS files on the command line, or in a command file
; with one line per image filename.
;
if uval eq 'sun_visor' then begin
    ; Get the list of selected images into ss
    ss=where(strmid(filnam_sel,0,1) eq '*')

    ; Now build up the command line to run.
	cmd='sun_visor ' + strjoin(filnam(ss), ' ') + ' &'

	; Finally, launch the program.
	spawn, cmd
endif

;
; So(lar)Da(ta) Surf(er)  Mark Cheung's (cheung@lmsal.com) IDL movie browsing tool to
; display 3D cubes (e.g. a stack of images)
; It takes a list of FITS files on the command line, or in a command file
; with one line per image filename.

if uval eq 'soda_surf' then begin
    ; Get the list of selected images into ss
    ss=where(strmid(filnam_sel,0,1) eq '*')
    Launch_SoDASurf, Group_leader=0, filenames=filnam[ss]
endif

end

PRO sot_list

;+
; NAME:
;       sot_list
; PURPOSE:
;       Displays the list of selections filtered from the sot_select
;       routine. User can select multiple items (via click and drag) and display or read
;       them for further use.
; CALLING SEQUENCE:
;       This is a subroutine for sot_select.
; INPUTS: None so far.
; OUTPUTS:
;       INDEX_OUT - Index record array for selected files
;       DATA_OUT  - Data array for selected files
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL 
;                   - Fixed display button to read data and display it in one click
;                   - Increased y size so more files are available
;                   - Widget now focuses on each selection, rather than the top
;                   - Display is only the atv call
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL
;                   - ListBox widget to replace droplist widget with wavelength, obstype, etc.
;                   - Use of the multiple keyword
;-

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, test, filnam, ss, result, filnam_sel, index, data,m,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat, index_sel, data_sel, dum1, big_base

test= 0
m=0
dum=0

base=widget_base(/column, title='SOT List')
refine_base = widget_base(base, /row)

start_search = widget_base(base, /row)
reset    = widget_button(start_search, value = 'Reset', uvalue='reset')
read     = widget_button(start_search, value='Read', uvalue='read', xsize=100)
;display = widget_droplist(start_search, value=['Display', 'tvscl', 'iimage', 'atv'], uvalue='display', xsize=100)
display  = widget_button(start_search, value='Display', uvalue = 'display')
sun_visor= widget_button(start_search, value='sun_visor', uvalue = 'sun_visor')
; Next line added by Mark Cheung
soda_surf= widget_button(start_search, value='SoDaSurf', uvalue = 'soda_surf')

if size(cat[0], /type) eq 8 then begin

    cat_arr=['cat.obs_type', 'obstype1', 'cat.wave', 'wavelength1', 'cat.naxis1', 'nx1', 'cat.naxis2', 'ny1']

    blank_arr = lindgen(n_elements(cat_arr))
    blank_arr[*] = 1

    for ii=0,n_elements(cat_arr)-1 do begin

            statement = "if strtrim(("+cat_arr[ii]+")(0),2) eq '*' then blank_arr[ii] = 0"
            retval = execute(statement)
            if blank_arr[ii] eq 0 then blank_arr[ii-1] = 0

    endfor

    filled_cat = where(blank_arr eq 1)

    if filled_cat[0] ne -1 then begin
        
        cat_arr1=cat_arr[filled_cat]

        for ii=0, n_elements(cat_arr)-2,2 do begin

if ii ne 2 then begin

            statement='m=where(strtrim('+cat_arr[ii+1]+',2) eq strtrim('+cat_arr[ii]+',2))'
            retval=execute(statement)
            if m[0] ne -1 then cat=cat[m]
            statement = "if m[0] eq -1 and "+cat_arr[ii+1]+" ne '*' then dum=1"                
            retval = execute(statement)

        endif else begin

            statement='m=where(cat.wave eq strtrim("'+wavelength1+'",2))'
            retval=execute(statement)
            if wavelength1 ne '*' and m[0] ne -1 then cat = cat[m]

        endelse

    endfor

endif

    if size(cat, /type) eq 8 and dum eq 0 then begin
        filnam = sot_cat2files(cat)
        mreadfits, filnam,index
        list=get_infox(index, 'TIME_D$OBS, naxis1, naxis2, wave')
        blank=strarr(n_elements(list))
        blank[*]=' '
        filnam_sel=list+ blank + strmid(filnam,strpos(filnam, '/', /reverse_search))

        if test ne 1 then result = widget_list(base, value=filnam_sel, uvalue='wlist', font='helvetica',ysize=30,/frame, /multiple)

        if test eq 1 then begin
            widget_control, result, /destroy
            result = widget_list(base, value=filnam_sel, uvalue='wlist', font='helvetica',ysize=30,/frame, /multiple)

        endif
	;stop
        test=1

    endif else begin
        print, 'try again 2'
    endelse
    
endif else print, 'try again 3'

num_images = widget_label(refine_base, value = 'Found '+ strtrim(n_elements(cat),2) + ' images')

dismiss = widget_button(base, value = 'Dismiss', uvalue = 'dismiss', xsize=200)

widget_control, base, /realize
xmanager, 'sot_list', base, /no_block

index_out = index
data_out = data

end
