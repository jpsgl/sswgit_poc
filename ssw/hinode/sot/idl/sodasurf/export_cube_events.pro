pro export_cube_events, event

widget_control, event.id, get_value=button_label

case button_label of

'Export': begin
endcase

'Binary cube': begin
   export_window = widget_base(xsize=280, ysize=110, event_pro='export_events', Title="Export data cube")
   file_select = fsc_fileselect(export_window, xsize=18, ObjectRef=file_select_Object)
   file_select_Object->SetProperty, Directory='./'
   file_select_Object->SetProperty, Filename='cube.dat'
   widget_control, export_window, set_uvalue = file_select_Object
   ok_button = widget_button(export_window, value="OK", xsize=120, xoffset=5, yoffset=80, $
	event_pro='export_events', uvalue={export_window:export_window, DW_ID:event.top})
   cancel_button = widget_button(export_window, value="Cancel", xsize=120, xoffset=140, yoffset=80,$
	event_pro='export_events', uvalue={export_window:export_window, DW_ID:event.top})
   widget_control, export_window, /realize
endcase

'OK': begin

   ;Write the 3D cube to file.
   widget_control, event.id, get_uvalue = OK_button_uvalue
   widget_control, OK_button_uvalue.export_window, get_uvalue = file_select_Object
   file_select_Object->GetProperty, Directory=dir
   file_select_Object->GetProperty, Filename=file
   widget_control, OK_button_uvalue.DW_ID, get_uvalue = DW
   DW->GetProperty, data_obj = data_obj, main_ID=main_ID
   data_obj[0]->GetProperty, cube_ptr = cube_ptr

   ;Check if dir has '/' as the last character. If not, append it to dir
   last_char = strmid(dir, strlen(dir)-1, 1)
   if (last_char ne '/') then dir = dir+'/'
   
   ;save, file=dir+file, *cube_ptr
   close, 3
   openw, 3, dir+file, error=file_error
   IF (file_error NE 0) THEN BEGIN
   	Catch, Cancel=1
 	ok = Dialog_Message(!Error_State.Msg+' '+!Error_State.Sys_Msg)
	Print, ''
	Print, !Error_State.Msg+' '+!Error_State.Sys_Msg
	close, 3
   ENDIF ELSE BEGIN
	writeu, 3, *cube_ptr
	close, 3
	entry =    'Data exported to ' +dir+file+'. Size=('+$
		string(n_elements((*cube_ptr)[*,0,0]), format='(I3)') + ',' +$
		string(n_elements((*cube_ptr)[0,*,0]), format='(I3)') + ',' +$
		string(n_elements((*cube_ptr)[0,0,*]), format='(I3)') + ')'
	widget_control, OK_button_uvalue.export_window, /destroy
   ENDELSE
endcase

"Cancel": begin
   ;Destroy the Export Window
   print, "Export cancelled"
   widget_control, event.id, get_uvalue = Cancel_button_uvalue
   widget_control, Cancel_button_uvalue.export_window, /destroy
endcase

else: begin
endcase

end

end
