; Copyright (c) 2003, Max Planck Institute for Aeronomy
;   Unauthorized reproduction prohibited.
;+
; CLASS_NAME:
;   variable_type__define
;
; PURPOSE:
;   This object class defines what a variable type is.
;   It is the superclass of object classes such as temperature_type etc.
;
; CATEGORY:
;   Main Module
;
; SUPERCLASSES:
;       List classes this class inherits from.
;
; SUBCLASSES:
;       List classes that inherit from this class, if any.
;
; CREATION:
;       Note how an object of this class is created.
;       Generally, this is just a reference to the class'
;       Init method.
;
; METHODS:
;       List the methods intrinsic to this class. There is no
;       need to list the inherited methods.
;
; MODIFICATION HISTORY:
;   Written by:  Mark Cheung, April 2003.
;      Any additional mods get described here.  Remember to
;      change the stuff above if you add a new keyword or
;      something!

FUNCTION variable_type::Filename, nmod=nmod

RETURN, 'N/A'

END

; Given a menu parent, this method creates Menu Items for each
; variable type in the variables_list.

pro variable_type::VariablesMenuItems, $
    menu_Item_IDs = menu_Item_IDs, $
    menu_parent = menu_parent, $
    event_pro = event_pro

menu_item_IDs = intarr(n_elements(self.variables_list)-1)
; The running index starts at 1 because the button for the "None" variable
; is not used.
for i=1,(n_elements(self.variables_list)-1),1 do begin
   menu_item_IDs[i-1] = widget_button(menu_parent, value=self.variables_list[i], $
    event_pro=event_pro)
endfor
end

;Creates a droplist widget containing the list of defined variables.
pro variable_type::VariablesListWidget, $
    list_widget_ID = list_widget_ID, $
    parent = parent, $
    label_ID = label_ID, $
    labelstr = labelstr

IF N_ELEMENTS(labelstr) NE 0 THEN label_ID = $
    widget_label(parent, value=labelstr)

list_widget_ID = widget_droplist(parent, value=self.variables_list)

end

pro variable_type::GetProperty, $
    variable_name=variable_name, $
    unit=unit, $
    variables_list = variables_list
;   units_list = units_list

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in GetProperty Method. Returning...')
   Print, ''
   Print, 'data_set::GetProperty Method: ' + !Error_State.Msg
ENDIF

variable_name = self.variable_name
variables_list = self.variables_list
;units_list = self.units_list
unit=self.unit
end

FUNCTION variable_type::INIT, $
    variable_name=variable_name, $
    unit=unit, $
    variables_list=variables_list

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in INIT Method. Returning...')
   Print, ''
   Print, 'variable_type::INIT Method: ' + !Error_State.Msg
   RETURN, 0
ENDIF

IF N_ELEMENTS(variable_name) EQ 0 THEN variable_name = "None"
IF N_ELEMENTS(unit) EQ 0 THEN unit = ""

self.variable_name=variable_name
self.unit=unit

;================================================
; If an item is added to this list, then the length
; of array in variable_type__define must also be
; updated.
;================================================
self.variables_list = [$    ;Entries in this list
    "None", $   ;are all subclasses of variable_type class.
    "temperature", $
    "pressure", $
    "rho", $
    "v_x", $
    "v_y", $
    "v_z", $
    "energy", $
    "eint", $
    "entropy", $
    "mu", $
    "B_x", $
    "B_y", $
    "B_z", $
    "B_abs", $
    "B_horiz", $
    "B_y_ani", $
    "B_abs_ani", $
    "rho_ani", $
    "i_c_ani", $
    "t_ani", $
    "v_y_ani", $
    "inc_angle_ani", $
    "inc_angle", $
    "inv_beta", $
    "divB", $
    "pRHC", $
    "tRHC", $
    "H_p", $
    "eqp", $
    "mach", $
    "Qrad", $
    "vort_x", $
    "vort_y", $
    "vort_z" $
    ]

RETURN, 1
END

PRO variable_type__define

struct = {variable_type, $
    variable_name:'', $       ;name of variable
    unit:'', $        ;Units of this variable (e.g. for temp, unit=K)
    variables_list: strarr(35) $   ;List containing all defined variables.
    }
END 
