; Copyright (c) 2003, Max Planck Institute for Aeronomy
;	Unauthorized reproduction prohibited.
;+
; CLASS_NAME:
;	plugin__define
;
; PURPOSE:
;	This object class defines what a plugin is.
;	It is the superclass of object classes such as pixel_tab etc.
;
; CATEGORY:
;	Main Module	
;
; SUPERCLASSES:
;       List classes this class inherits from.
;
; SUBCLASSES:
;       List classes that inherit from this class, if any.
;
; CREATION:
;       Note how an object of this class is created.
;       Generally, this is just a reference to the class'
;       Init method.
;
; METHODS:
;       List the methods intrinsic to this class. There is no
;       need to list the inherited methods.
;
; MODIFICATION HISTORY:
; 	Written by:	Mark Cheung, April 2003.
;			Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!

PRO plugin::ResizeTab, $
	tab_size = tab_size

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in ResizeTab Method. Returning...')
   Print, ''
   Print, 'plugin::ResizeTab Method: ' + !Error_State.Msg
ENDIF

IF N_ELEMENTS(tab_size) EQ 2 THEN BEGIN
   widget_control, self.tab_ID, XSize=tab_size[0], YSize=tab_size[1]
   self.tab_size = tab_size
ENDIF
END

;==============Method for Drawing on Postscript=============
PRO plugin::DrawPostscript

END

;==============Method for destroying plugin=================
PRO plugin::Destroy
; Default is just to destroy the tab widget.
; To override, define a Destroy method for the plugin object class
; that is a sub-class of the plugin class.

widget_control, self.tab_ID, /destroy
Heap_free, self

END

PRO plugin::GetProperty, $
	plugin_name = plugin_name, $
	tab_ID = tab_ID, $
	DW = DW, $
	tab_size = tab_size, $
	context_menu = context_menu

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in GetProperty Method. Returning...')
   Print, ''
   Print, 'data_set::GetProperty Method: ' + !Error_State.Msg
ENDIF

plugin_name = self.plugin_name
tab_ID = self.tab_ID
DW = self.DW
tab_size = self.tab_size
context_menu = self.context_menu

END

FUNCTION plugin::INIT, $
	plugin_name=plugin_name, $
	tab_ID=tab_ID, $
	DW=DW

;Error Handling
Catch, theError
IF theError NE 0 THEN BEGIN
   Catch, Cancel=1
   ok = Dialog_Message('Error in INIT Method. Returning...')
   Print, ''
   Print, 'data_set::INIT Method: ' + !Error_State.Msg
   RETURN, 0
ENDIF

IF N_ELEMENTS(plugin_name) EQ 0 THEN variable_name = "Generic Plugin"
IF N_ELEMENTS(tab_ID) EQ 0 THEN tab_ID=0L

widget_control, tab_ID, set_uvalue = self ; Store 
self.plugin_name=plugin_name
self.tab_ID=tab_ID
self.DW=DW

RETURN, 1
END

PRO plugin__define

struct = {plugin, $
	plugin_name:'', $		; Name of the plugin
	tab_ID:0L, $			; Widget ID of the plugin
	DW:Obj_New(), $			; Display Window Object to which the plugin belongs
	tab_size:intarr(2), $		; The [width,height] of the tab with widget ID=tab_ID
	context_menu:0L $		; Widget ID of the context Menu
	}
END
