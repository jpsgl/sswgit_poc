; Launch_SoDaSurf
; starts a Solar Data Surfer window for data viewing

; Usage Example 1
; IDL> sot_cat, time0, time1, cat, files
; <<some filtering commands to pick the right files>>
; IDL> launch_sodasurf, files = files

; Usage Example 2
; IDL> launch_sodasurf, data=data
; where data is a 3D data cube.



; IF data parameter specified, then any filenames given will be
; ignored.
PRO Launch_SoDaSurf, $
                     Group_leader=Group_leader, $
                     filenames=filenames, wxsize=wxsize, wysize=wysize, $
                     data=data



screen_size = get_screen_size()
IF N_ELEMENTS(wxsize) EQ 0 THEN wxsize=floor(screen_size[0]*0.85)
IF N_ELEMENTS(wysize) EQ 0 THEN wysize=floor(screen_size[1]*0.85)
IF N_ELEMENTS(data) NE 0 THEN BEGIN
    data = reform(data, /overwrite)
    data_size = size(data)
    IF (data_size[0] LT 3) THEN BEGIN
        Nfiles=1
    ENDIF ELSE BEGIN
        Nfiles=data_size[3]
    ENDELSE
    Nz = Nfiles
    Nx = data_size[1]
    Ny = data_size[2]
    ind_file_ratio = 1
    xcen = fltarr(Nz)
    ycen = fltarr(Nz)
    xaxis= fltarr(Nx,Nz)
    yaxis= fltarr(Ny,Nz)
    FOR k=0,Nz-1 DO BEGIN
        xaxis[*,K] = findgen(Nx)+xcen[K]
        yaxis[*,K] = findgen(Ny)+ycen[K]
    ENDFOR
    variable_name = strarr(4)
    data_obj = Obj_New()
    data_obj = replicate(data_obj,4)
    variable_name[0] = 'Generic data'
    filenames = Ptr_New(strarr(Nz),/no_copy)
    xaxis_ptr = Ptr_new(xaxis, /no_copy)
    yaxis_ptr = Ptr_new(yaxis, /no_copy)
                                ;index = index[indgen(Nz)*ind_file_ratio]
    time_obs  = fltarr(Nz)      ;index.date_obs
    zaxis_ptr = Ptr_new(time_obs, /no_copy)
    cat_ptr   = Ptr_new()       ; Ptr_new(index, /no_copy)

    data_obj[0] = Obj_New("data_set", data_type=2, dir=dir, $
                          filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
                          cube_size=[Nx,Ny,Nz], cube_ptr=PTR_NEW(data), axes_units = ["pixel","pixel","pixel"], $
                          xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[0], cat_ptr=cat_ptr)
    
ENDIF ELSE BEGIN
    Nfiles=N_ELEMENTS(filenames)
    read_sot, filenames, index
    Nx   = MAX(index.Naxis1)
    Ny   = MAX(index.Naxis2)
    Nz   = Nfiles
    ind_file_ratio = N_ELEMENTS(index)/Nfiles
    xcen = index.xcen
    ycen = index.ycen
    time_obs=index.date_obs
    xaxis= fltarr(Nx,Nz)
    yaxis= fltarr(Ny,Nz)
    variable_name = strarr(4)
    data = intarr(Nx,Ny,Nz)
    data_obj = Obj_New()
    data_obj = replicate(data_obj,4)

    FOR M=0,Nz-1 DO BEGIN
        read_sot, filenames[m], this_index, slice
        slice_size = size(slice)
        nxslice = slice_size[1]
        nyslice = slice_size[2]
        CASE (index.obs_type)[0] OF
            'SP IQUV 4D array' : BEGIN
                IF M EQ 0 THEN BEGIN
                    data2=data
                    data3=data
                    data4=data
                    variable_name[0] = "SOT: "+"Stokes I"
                    variable_name[1] = "SOT: "+"Stokes Q"
                    variable_name[2] = "SOT: "+"Stokes U"
                    variable_name[3] = "SOT: "+"Stokes V"
                ENDIF
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1,0,0]
                data2[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,0,1]
                data3[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,0,2]
                data4[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,0,3]
            ENDCASE
            'FG shuttered I and V': BEGIN
                IF M EQ 0 THEN BEGIN
                    data2=data
                    variable_name[0] = "SOT: "+"Stokes I"
                    variable_name[1] = "SOT: "+"Stokes V"
                ENDIF
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1,0]
                data2[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,1]
            ENDCASE
            'FG shutterless I and V': BEGIN
                IF M EQ 0 THEN BEGIN
                    data2 = data
                    variable_name[0] = "SOT: "+"Stokes I"
                    variable_name[1] = "SOT: "+"Stokes V"
                ENDIF
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1,0]
                data2[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,1]
            ENDCASE
            'FG shutterless Stokes': BEGIN
                IF M EQ 0 THEN BEGIN
                    data2 = data
                    data3 = data
                    data4 = data
                    variable_name[0] = "SOT: "+"Stokes I"
                    variable_name[1] = "SOT: "+"Stokes Q"
                    variable_name[2] = "SOT: "+"Stokes U"
                    variable_name[3] = "SOT: "+"Stokes V"           
                ENDIF
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1,0]
                data2[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,1]
                data3[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,2]
                data4[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,3]
            ENDCASE
            'FG shuttered Stokes': BEGIN
                IF M EQ 0 THEN BEGIN
                    data2 = data
                    data3 = data
                    data4 = data
                    variable_name[0] = "SOT: "+"Stokes I"
                    variable_name[1] = "SOT: "+"Stokes Q"
                    variable_name[2] = "SOT: "+"Stokes U"
                    variable_name[3] = "SOT: "+"Stokes V"           
                ENDIF
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1,0]
                data2[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,1]
                data3[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,2]
                data4[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,3]
            ENDCASE
            'FG shutterless I and V with 0.2s intervals': BEGIN
                IF M EQ 0 THEN BEGIN
                    data2 = data
                    variable_name[0] = "SOT: "+"Stokes I"
                    variable_name[1] = "SOT: "+"Stokes V"
                ENDIF
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1,0]
                data2[0:nxslice-1,0:nyslice-1,M] = slice[0:nxslice-1,0:nyslice-1,1]
            ENDCASE
            ELSE: BEGIN
                IF M EQ 0 THEN variable_name[0] = "SOT: "+this_index.wave
                data[0:nxslice-1,0:nyslice-1,M]  = slice[0:nxslice-1,0:nyslice-1]
            ENDCASE
        END
        deltax = float((this_index.fovx)[0])/(this_index.naxis1)[0]
        deltay = float((this_index.fovy)[0])/(this_index.naxis2)[0]
        xaxis[0:nxslice-1,M] = congrid((findgen((size(data))[1])-(size(data))[1]/2)*deltax+(this_index.xcen)[0],nxslice)
        yaxis[0:nyslice-1,M] = congrid((findgen((size(data))[2])-(size(data))[2]/2)*deltay+(this_index.ycen)[0],nyslice)
    ENDFOR

    filenames = Ptr_New(filenames,/no_copy)
    xaxis_ptr = Ptr_new(xaxis, /no_copy)
    yaxis_ptr = Ptr_new(yaxis, /no_copy)
    index = index[indgen(Nz)*ind_file_ratio]
    time_obs=index.date_obs
    zaxis_ptr = Ptr_new(time_obs, /no_copy)
    cat_ptr   = Ptr_new(index, /no_copy)

    data_obj[0] = Obj_New("data_set", data_type=2, dir=dir, $
                          filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
                          cube_size=[Nx,Ny,Nz], cube_ptr=PTR_NEW(data, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
                          xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[0], cat_ptr=cat_ptr)

    IF N_ELEMENTS(data2) NE 0 THEN BEGIN
        data_obj[1] = Obj_New("data_set", data_type=2, dir=dir, $
                              filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
                              cube_size=[Nx,Ny,Nz], cube_ptr=Ptr_New(data2, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
                              xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[1], cat_ptr=cat_ptr)
    ENDIF
    IF N_ELEMENTS(data3) NE 0 THEN BEGIN
        data_obj[2] = Obj_New("data_set", data_type=2, dir=dir, $
                              filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
                              cube_size=[Nx,Ny,Nz], cube_ptr=Ptr_New(data3, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
                              xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[2], cat_ptr=cat_ptr)
    ENDIF
    IF N_ELEMENTS(data4) NE 0 THEN BEGIN
        data_obj[3] = Obj_New("data_set", data_type=2, dir=dir, $
                              filenames=filenames, vertex1=[0,0,0], vertex2=[Nx-1,Ny-1,Nz-1], $
                              cube_size=[Nx,Ny,Nz], cube_ptr=Ptr_New(data4, /no_copy), axes_units = ["arcsec","arcsec","Time"], $
                              xaxis_ptr=xaxis_ptr, yaxis_ptr=yaxis_ptr, zaxis_ptr=zaxis_ptr, variable_name=variable_name[3], cat_ptr=cat_ptr)
    ENDIF

ENDELSE

DW = OBJ_NEW("sodasurf_display_window", Group_leader=Group_leader, data_obj = data_obj, wxsize=wxsize, wysize=wysize)

END
