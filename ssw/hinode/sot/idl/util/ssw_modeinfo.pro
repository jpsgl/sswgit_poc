function ssw_modeinfo, allmatches, submatches, umode=umode, $
   movie_name=movie_name
;
;+ 
;   Name: ssw_modeinfo
;
;   Purpose: return some mode/movie statistics
;
;   Input Paramters:
;      allmatches - index/catalog records match
;      submatches - subset of above in implied movie/mode
;
;   Output:
;      function returns structure with mode/movie statistics
;
;   Keyword Parameters:
;      umode - uniq mode description used to define a "match"
;              (output of ssw_uniq_modes.pro for example)
;
;   History:
;      27-feb-2007 - S.L.Freeland - help externalize movies->html/vo
;
;   
;-

atimes=anytim(allmatches.date_obs,/ccsds)
stimes=anytim(submatches.date_obs,/ccsds)
time_window,atimes,at0,at1, out='ccsds'
time_window,stimes,st0,st1, out='ccsds'


retval={start_time:'',stop_time:'',mstart_time:'',mstop_time:'', $ 
   nmatches:0l,nss:0l, dqreject:0l,$
   movie_frames:0, umode:'None Specified', $
   url_mthumb:'',url_micon:'',url_thumb:'', $
   url_mparent:'',url_javascript:'',url_mpeg:'',url_gifanim:'', $
   url_context:'', url_context_thumb:'', $
   mean_exptime:0., $
   cadence_mode:0., cadence_movie:'', wavelnth:''}


retval.start_time=at0
retval.stop_time=at1
retval.mstart_time=st0
retval.mstop_time=st1
retval.nmatches=n_elements(allmatches)
retval.movie_frames=n_elements(submatches)
retval.nss=n_elements(submatches) ; for some existing indexing sw
retval.mean_exptime=mean(gt_tagval(allmatches,/exptime,missing=-1))
retval.cadence_mode=average(ssw_deltat(atimes,/min))
retval.cadence_movie=average(ssw_deltat(stimes,/min))
if data_chk(umode,/string) then retval.umode=umode(0)

return,retval
end
