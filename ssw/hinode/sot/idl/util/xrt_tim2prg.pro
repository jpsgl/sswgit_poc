
function xrt_tim2prg, iindex, use_ssw_dist=use_ssw_dist, refresh=refresh, $
  flflg_fix=flflg_fix, obs_mode_fix=obs_mode_fix, flr_trig_cfa=flr_trig_cfa, $
  flr_trig_resp_cfa=flr_trig_resp_cfa, flr_prog_enabled=flr_prog_enabled, $
  prog_no_orl=prog_no_orl, do_index_fix=do_index_fix, oindex=oindex, $
  _extra=_extra, debug=debug

; Set default and derived keywords:
use_ssw_dist = keyword_set(use_ssw_dist)
refresh = keyword_set(refresh) or n_elements(sttim_ff) eq 0


if not exist(filnam_xrt_flr_prgs) then begin
  hinode_db = concat_dir(get_logenv('SSWDB'), 'hinode/gen/')
  filnam_xrt_flr_prgs = concat_dir(hinode_db, 'xrt_flr_prgs.dat')
endif

n_rec = n_elements(iindex)
if tag_exist(iindex,'obs_mode') then obs_mode_orig = iindex.obs_mode else $
  obs_mode_orig = replicate('', n_rec)

flflg_fix = replicate('NON', n_rec)
obs_mode_fix = replicate('QT', n_rec)
flr_trig_cfa = replicate('', n_rec)
flr_trig_resp_cfa = replicate('', n_rec)
flr_prog_enabled = replicate('NO', n_rec)
prog_no_orl = replicate('', n_rec)

; Extract times and program numbers from all XRT index (header) records:
t_arr = iindex.date_obs
t_sec = anytim(t_arr)
n_tim = n_rec
prog_no_hdr = iindex.prog_no

; If required, read CFA-generated 'flare trigger' table:
if refresh then begin
  filnam_flr_trig = 'xrt_flare_flags.txt'
  filnam_flr_trig = concat_dir('$SSW_XRT','idl/util/' + filnam_flr_trig)
  if n_elements(url_flareflag) eq 0 then url_flareflag = $
    'http://xrt.cfa.harvard.edu/missionops/flare_trigger_list/xrt_flare_flags.txt'
  case 1 of
    use_ssw_dist and file_exist(filnam_flr_trig): begin
      box_message, 'Using SSW distributed version of CFA flare trigger table...'
      buff_flr_trig = rd_tfile(filnam_flr_trig)
      endcase
      else: sock_list, url_flareflag, buff_flr_trig
  endcase
  ssff = where(strpos(buff_flr_trig,'#') eq -1, ffcnt)
; Convert 1D table -> 2D table:
  cols = str2cols(buff_flr_trig(ssff), ' ', /trim) 
; Convert 2D table -> 3x1D vectors:
  strtab2vect, cols, sttim_flr_trig, entim_flr_trig, flr_resp_cfa

  sttim_flr_trig_sec = anytim(sttim_flr_trig)
  entim_flr_trig_sec = anytim(entim_flr_trig)
endif

; If required, read file containing table of flare response intervals and corresponding
; program numbers extracted from XRT ORL files (xrt_flr_prg_st_end.txt):
buff = rd_tfile(filnam_xrt_flr_prgs, 3)

; Extract start times, end times, and ORL program numbers:
sttim_buff = reform(buff[0,*])
sttim_date = strmid(sttim_buff, 0,10)
sttim_time = strmid(sttim_buff,11, 8)
sttim_flr_prog  = sttim_date + ' ' + sttim_time

entim_buff = reform(buff[1,*])
entim_date = strmid(entim_buff, 0,10)
entim_time = strmid(entim_buff,11, 8)
entim_flr_prog = entim_date + ' ' + entim_time

flr_prog_orl = reform(buff[2,*])

sttim_flr_prog_sec = anytim(sttim_flr_prog)
entim_flr_prog_sec = anytim(entim_flr_prog)

for i=0,n_tim-1 do begin

  ss_close_start_trig = max(where(t_sec[i] gt sttim_flr_trig_sec, n_close_start_trig))
  ss_close_end_trig   = min(where(t_sec[i] lt entim_flr_trig_sec, n_close_end_trig))

  ss_close_start_prog = max(where(t_sec[i] gt sttim_flr_prog_sec, n_close_start_prog))
  ss_close_end_prog   = min(where(t_sec[i] lt entim_flr_prog_sec, n_close_end_prog))

  if ( (ss_close_start_trig ne -1) and (ss_close_end_trig ne -1) ) then begin
    if (ss_close_start_trig eq ss_close_end_trig) then begin
      flr_trig_cfa[i] = 'YES'
      flr_trig_resp_cfa[i] = flr_resp_cfa[ss_close_start_trig]
      flflg_fix[i] = 'FLR'
    endif
  endif

  if ( (ss_close_start_prog ne -1) and (ss_close_end_prog ne -1) ) then begin
    if (ss_close_start_prog eq ss_close_end_prog) then begin
      flr_prog_enabled[i] = 'YES'
      if (prog_no_hdr[i] eq flr_prog_orl[ss_close_start_prog]) then begin
        obs_mode_fix[i] = 'FL'
        prog_no_orl[i] = flr_prog_orl[ss_close_start_prog]
      endif
    endif
  endif

endfor

if keyword_set(do_index_fix) then begin
  oindex = iindex
  oindex.flrflg = flflg_fix
  oindex.obs_mode = obs_mode_fix
endif

if keyword_set(debug) then stop, ' Stopping in XRT_TIM2PRG on request. '

; TODO: Change returned variable (structure?):
return, prog_no_orl

end

