function ssw_csv2struct, file, refresh=refresh, ncols=ncols, $
   debug=debug, patch=patch , data=data, in_data=in_data, tag_names=tag_names, loud=loud
;
;+
;   Name: ssw_csv2struct 
;
;   Purpose: 
;
;   Input Parameters:
;      file - csvfile name
;
;   Output Parameters:
;      function returns structure vector, one element per cvs record
;
;   Keyword Parameters:
;      refresh (keyword) - if set refresh cache
;      nocols - optionally, only apply to records with This many columns
;               (default = records with MODE(#columns-per-record)
;      data (output) - raw rd_tfiles(output) - strarr(#records)
;
;   History:
;      14-Jan-2008 - S.L.Freeland - originally for hinode alignment file
;      22-Jan-2008 - S.L.Freeland - auto-bad record reject, NCOL keyword
;
;   Method:
;      yes
;-

debug=keyword_set(debug)
loud=keyword_set(loud)

refresh=keyword_set(refresh) or n_elements(alignment) eq 0

if refresh then begin 
   case 1 of 
      n_elements(in_data) gt 0: data=in_data ; user supplied straa
      file_exist(file): data=rd_Tfile(file)
      else: begin 
         box_messsage,'Need input FILE, or IN_DATA (csv strarr)
         return,-1  ; EARLY EXIT
      endcase
   endcase
   valid=data 
   nvalid=n_elements(data)
   chkcol=lonarr(nvalid)
   for i=0,nvalid-1 do begin
      nq=where_pattern(valid(i),'"',qcnt) ; escaped comma check
      if qcnt eq 2 then begin 
        qsub=strextract(valid(i),'"')
        ssub=str_replace(qsub,',','/')
        valid(i)=str_replace(valid(i),qsub,ssub)
      endif
      nc=where_pattern(valid(i),',',ccnt)
      chkcol(i)=ccnt
   endfor
   modev=mode_val(chkcol)
   if keyword_set(ncols) then begin 
      if loud then box_message,'Only including lines where #cols='+strtrim(ncols,2)
      modev=ncols-1 ; #commas=ncols-1
   endif
   vss=where(chkcol eq modev,okcnt)
   if okcnt eq 0 then begin
      if loud then box_message,'No records with your supplied NCOLS'
      return,-1 ; Early Exit
   endif
   bss=where(chkcol lt modev,bcnt)
   if keyword_set(patch) and bcnt gt 0 then begin ; add #missing commas
      if loud then box_message,'patching ' + strtrim(bcnt,2) + ' records w/null columns)
      nmiss=modev-chkcol(bss)
      for b=0,bcnt-1 do begin
         if nmiss(b) gt 0 then $
            valid(bss(b))=valid(bss(b))+ arr2str(replicate(',',nmiss(b)))
      endfor
      sspatched=bss
      vss=indgen(n_elements(valid)) ; all good
      okcnt=nvalid  ; now patched
   endif

   if okcnt lt nvalid then begin   
      if loud then box_message,'Rejecting: ' + strtrim(bcnt,2) + ' records (comma count)
      valid=valid(vss)
   endif
   cols=strarr(modev+1,okcnt)
   for i=0,okcnt-1 do cols(0,i)=str2arr(valid(i),',')
   estring='alignstruct={' + arr2str('p'+strtrim(indgen(modev+1),2)+':""') + '}'
   estat=execute(estring) ; 
   nrecs=n_elements(valid)
   alignstruct=replicate(alignstruct,nrecs)
   reads,cols ,alignstruct  ; populate
   if debug then stop,'cols,align'
endif 

retval=alignstruct    ; all data 

return,retval
end


