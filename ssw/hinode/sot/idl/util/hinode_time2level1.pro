pro hinode_time2level1, time0, time1,inparent,outparent, $
   xrt=xrt, prefix=prefix, quicklook=quicklook, sirius=sirius, level0=level0, $
   obs_type=obs_type, refresh=refresh
;
;+
;   Name: hinode_time2level1
;
;   Purpose: generate Level1 from QuickLook or final Leve0; parallel tree
;
;   Input Parameters:
;      time0, time1 - desired time range
;      inparent - the top level to Hinode; aka $HINODE_DATA
;                 (assumed to include 'level0' and/or 'QuickLook' subdirectories
;      outparent - optional parent for output (default is under level0 parent)
;                 Level1 files writtent to <parent>/{xrt,sot}/level1 -or-
;                                          <parent>/{xrt,sot},/level1_qkl 
;   Output:
;       level1 counterparts for all level input files are generated
;       (e.g., these are the {xrt,sot}_prep'ed output as FITS)
;       Names inherited from Level0 with "L1_" prefix prepended
;
;   Keyword Parameters:
;      obs_type - optional obs type to process - default='FG'
;      xrt - if set, apply to XRT (default=sot)
;      level0/sirius (synonyms) - apply to final "sirius" level0
;      quicklook - apply to QuickLook data 
;      refresh - if set, re-prep/regenerate even if L1 file exists      
;
;   Method:
;      use sot_cat and SSW-gen string/data management utilities
;   
;   History:
;      26-July-2007 - S.L.Freeland
;-
;      

l0=keyword_set(sirius) or keyword_set(level0)
quicklook=keyword_set(quicklook)

l0 = l0 or (1-quicklook)
xrt=keyword_set(xrt)

hinparent=get_logenv('HINODE_DATA')
if hinparent(0) eq '' then hinparent='/net/kokuten/archive/hinode/'
if n_elements(outparent) eq 0 then begin 
   box_message,'Warning: no OUTPARENT ($HINODE_DATA) , using input parent' 
   outparent=hinparent
endif

tg=timegrid(time0,time1,/day,out='ecs')
tg=anytim(tg,/ecs,/date_only)

if n_elements(obs_type) eq 0 then obs_type='FG'
obs_type=([obs_type,'XRT'])(xrt)

for i=0,n_elements(tg)-1 do begin  ; by day
   sstring='obsdir='+obs_type
   sot_cat,tg(i),tg(i+1),cat,xrt=xrt,sirius=l0, files, search=sstring
   ssprob=where(strpos(files,hinparent) ne 0,pcnt)
   if pcnt gt 0 then begin 
      stop,'Problem w/file names??'
   endif
   rootnames=strmid(files,strlen(hinparent),1000)  
   l1names=str_replace(rootnames,obs_type+'2','L1_'+obs_type+'2')
   l1names=str_replace(l1names,'QuickLook','level1_qkl')
   l1names=str_replace(l1names,'level0','level1')
   l1names=concat_dir(outparent,l1names)
;  implied uniq output directory list; create if required
   dirs=ssw_strsplit(l1names,'L1_',/head)
   udirs=all_vals(dirs)
   ndirs=where(1-file_exist(udirs),ncnt)
   if ncnt gt 0 then mk_dir, udirs(ndirs) ; << create "missing" directories

   for f=0,n_elements(l1names)-1 do begin  ; not efficient, but easy to understand...
      read_sot,files(f),index,data
      call_procedure,(['fg','xrt'])(xrt)+'_prep',index,data,oindex,odata
      oindex=struct2ssw(oindex,/nopoint)
      mwritefits, oindex, odata, outfile=l1names(f)
   endfor
endfor

return
end




