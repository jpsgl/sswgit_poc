function sot_iquv_scale, ii, dd, noscale=noscale, mag=mag
  scaleit=1-keyword_set(noscale)
  if scaleit then begin 
     case 1 of 
        keyword_set(mag): retval=bytscl(dd,min=-60,max=60)
        else: retval=bytscl(dd)
     endcase
  endif else  retval=dd
return,retval
end  

pro sot_iquv_3d, index, data, oindex, odata,  noscale=noscale, $
   i_only=i_only, u_only=u_only, q_only=q_only, v_only=v_only, debug=debug

;
;+
;   Name: sot_iquv_3d 
;
;   Purpose: convert non simple SOT modes (SP, FG-stokes etc) -> "movies"
;
;   Input Parameters:
;      index,data - NAXIS>2 cubes from read_sot
;
;   Output Parameters:
;      oindex, odata - 3D data (collapsed dimension)
;
;   Keyword Parameters:
;      noscale - if set, don't scale - default is individual scaling of
;                corresponding (I,Q,U,V) slices
;      i_only, u_only, q_only, v_only - if set, single slice
;      (default concatenates all available slices)
;
;   History:
;      10-Apr-2007 - S.L.Freeland - simplify auto movie pipeline
;
;   Restrictions:
;
;-

debug=debug
if not required_tags(index,/naxis) then begin 
   box_message,'Expect "index,data" from read_sot..."
   return
endif

naxis=gt_tagval(index,/naxis)
 
aaxis=all_vals(naxis)

if min(naxis) le 2 or n_elements(aaxis) gt 1 then begin
   box_message,"Expect NAXIS>2 and consistent for all input..."
   return
endif
oindex=index
case data_chk(data,/ndim) of 
   3:
   4: begin 
      if strmatch(index(0).obs_type,'*I and V*') then begin 
         ivss=0
         i=sot_iquv_scale(index,reform(data(*,*,0,*)), noscale=noscale)
         v=sot_iquv_scale(index,reform(data(*,*,1,*)), noscale=noscale,/mag)
         odata=[i,v]
      endif else begin 
         i=sot_iquv_scale(index,reform(data(*,*,0,*)),noscale=noscale)
         q=sot_iquv_scale(index,reform(data(*,*,1,*)),noscale=noscale)
         u=sot_iquv_scale(index,reform(data(*,*,2,*)),noscale=noscale)
         v=sot_iquv_scale(index,reform(data(*,*,3,*)),noscale=noscale,/mag)
         odata=[i,q,u,v]
      endelse
   endcase
   5: begin 
      spss=0
         i=sot_iquv_scale(index,reform(data(*,*,spss,0,*)),noscale=noscale)
         q=sot_iquv_scale(index,reform(data(*,*,spss,1,*)),noscale=noscale)
         u=sot_iquv_scale(index,reform(data(*,*,spss,2,*)),noscale=noscale)
         v=sot_iquv_scale(index,reform(data(*,*,spss,3,*)),noscale=noscale,/mag)
         odata=[i,q,u,v]
   endcase
   else: begin 
      box_message,'Unexpected NAXIS'
      return
   endcase
endcase

return
end
 


