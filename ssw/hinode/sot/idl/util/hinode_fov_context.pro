function hinode_fov_context,index,data,sindex,sdata, _extra=_extra, $
   xrt=xrt, eit=eit, fdmap=fdmap, rr=rr, gg=gg, bb=bb, debug=debug
;+
;   Name: hinode_fov_context
;
;   Purpose: sot/xrt wrapper for ssw_fov_context (see that routine...)
;
;   Input Parameters
;      index,data - sot or xrt 'index,data', generally level1 (prepped)
;
;   Output:
;      function returns context image graphic
;
;   Output Parameters:
;      sindex,sdata - scaled version of 'index,data' used in graphic
;
;   Keyword Parameters:
;      xrt (switch) - if set, use XRT synoptic full disk
;      eit (switch) - if set, use EIT full disk
;      fdmap (map object) - user supplied (pass->ssw_fov_context)
;
;
debug=keyword_set(debug)

if 1-data_chk(index,/struct) or data_chk(data,/nimage) le 1 then begin 
   box_message,'Expect "index,data" (2D or 3D)"
   return,-1
endif

;
  
case 1 of 
   data_chk(data,/type) eq 1:   begin  ; user supplied data already scaled
      sindex=struct2ssw(index,/nopoint)
      sdata=data
   endcase
   gt_tagval(index(0),/instrume, missing='') eq 'XRT': begin xrt, logscale
      sdata=bytscl(ssw_sigscale(index,data,/log))
      sindex=struct2ssw(index,/nopoint)
   endcase
   else: begin  ; assume sot or other 
      sdata=bytscl(ssw_sigscale(index,data))
      sindex=struct2ssw(index,/nopoint)
   endcase
endcase

fd_type='eit195'   ;init default
case 1 of
   valid_map(fdmap):  ; user supplied map (assume full disk, but...)
   keyword_set(xrt): begin ; use xrt
      xrtsynop=xrt_time2synop(sindex,/only_one)
      if file_exist(xrtsynop) then begin 
         read_sot,xrtsynop,findex,fdata
         xrt_prep,findex,fdata,fpindex,fpdata
         ;fsdata=bytscl(ssw_sigscale(fpindex,fpdata,/log))
         fsdata=bytscl((fpdata>1)^.2)
         index2map,findex,fsdata,fdmap
      endif else box_message,'Cannot find XRT synoptic; will use EIT
    endcase
    else: 
endcase


retval=ssw_fov_context(sindex,sdata,rr,gg,bb, _extra=_extra,fdmap=fdmap,fd_type=fd_type,/l1q)

if debug then stop,'retval...'
return,retval
end

         
   

   



