function sot_cat2files, catrecs, urls=urls, doy=doy, debug=debug, $
   sirius=sirius, darts=darts, lmsal=lmsal, level0=level0, l1=l1 , $
   old_names=old_names
;+
;   Name: sot_cat2files
;
;   Purpose: map from sot catalog entry to files -or- urls
;
;   Input Parameters:
;      catrecs - desired FPP/SOT catalog entries
;
;   Output Parameters:
;      function returns implied nfs path -or- FPP data center urls
;
;   Keyword Paramters:
;      urls - if set, return URLS instead of NFS filenames
;      doy - if set, ...yyyyDdoy_hhmmss.msec instead of
;                    ...yyyymmdd_hhmmss.msec (pre launch test data?)
;   
;   History:
;      10-Apr-2006 - S.L.Freeland - called from sot_time2files( w/CATALOG) 
;      11-oct-2006 - S.L.Freeland - $FPP_DATA -> $SOT_DATA
;      27-nov-2006 - S.L.Freeland - handle name change (millisec->10ths resolution)
;      16-May-2007 - S.L.Freeland - XRT dark hook
;      25-may-2007 - S.L.Freeland - xrt level0 proper
;      12-Jun-2007 - S.L.Freeland - add $HINODE_DATA option
;             Where $HINODE_DATA/<sot/xrt>/<QuickLook/levelo>/yyyy/mm/dd/...
;      18-Jul-2007 - S.L.Freeland - fix xrt if $HINODE_DATA is used
;      16-Aug-2007 - S.L.Freeland - make Level0 vs QuickLook based on      
;                                   catalog.LEVEL0 field
;      20-oct-2007 - S.L.Freeland - if /URL and server=OSLO, assume .gz
;                                   (maybe only for a day or two.. check back)
;-

debug=keyword_set(debug)
if not required_tags(catrecs,'date_obs') then begin 
   box_message,'Requires sot or xrt catalog records'
   return,replicate('',n_elements(catrecs)>1)
endif

anytim_dobs=gt_tagval(catrecs,/anytim_dobs,found=found)
if not found then begin 
   catrecs=add_tag(catrecs,anytim(catrecs.date_obs),'anytim_dobs')
   catrecs=add_tag(catrecs,1,'level0')
   catrecs=add_tag(catrecs,'CR','obsdir')
endif
sirius=keyword_set(sirius) or keyword_set(level0) or $
   gt_tagval(catrecs(0),/level0,missing=1)
   
urls=keyword_set(urls)

sotdata=get_logenv('$SOT_DATA')
sotdata=([sotdata,str_replace(sotdata,'QuickLook','level0')])(sirius)

xrtdata=get_logenv('$XRT_DATA')
xrtdata=([xrtdata,str_replace(xrtdata,'QuickLook','level0')])(sirius)
sbdata=get_logenv('$SOLARB_DATA')  ; common parent defined?

hinourl=get_logenv('HINODE_DATA_URL') 
case 1 of 
   hinourl ne '': urlp=hinourl
   keyword_set(darts): urlp='http://darts.isas.jaxa.jp/pub/solar/hinode/'
   else: urlp = 'http://sot.lmsal.com/data/'
endcase

hinoparent=get_logenv('HINODE_DATA')  ; optional parent of 
if file_exist(hinoparent) or urls then begin 
   if not file_exist(sotdata) then sotdata= $ 
      concat_dir(concat_dir(hinoparent,'sot'),(['QuickLook','level0'])(sirius))
   if not file_exist(xrtdata) then xrtdata= $ 
      concat_dir(concat_dir(hinoparent,'xrt'),(['QuickLook','level0'])(sirius))
endif
obsdir=strtrim(gt_tagval(catrecs,/obsdir,missing=''),2)

ssxrt=where(strupcase(obsdir) eq 'XRT',xcnt)

xrt=xcnt gt 0
case 1 of 
   keyword_set(urls): begin 
      top=urlp+'/'+(['sot','xrt'])(xrt) + '/' + (['QuickLook','level0'])(sirius)
   endcase
   else: begin 
      case 1 of 
         file_exist(xrtdata) and xrt: top=xrtdata
         file_exist(sotdata): top=sotdata
         file_exist(sbdata): top=concat_dir(sbdata,'sot')
         else: top='/net/kokuten/archive/sci/' ; lmsal PP center default?
      endcase
      if not file_exist(top) then begin
         box_message,"No local SOT data??; use /URLS to return remote names(urls)"
         return,''
      endif
   endcase
endcase

ftd=anytim(catrecs.anytim_dobs,/ecs)
ft=strcompress(str_replace(str_replace(str_replace(ftd,' ','_'),'/',' '),':',' '),/remove)
if keyword_set(doy) then begin 
    doys=anytim2doy(anytim(catrecs.anytim_dobs,/int))
    sdoys=fstring(doys,format='("D",I3.3)')   
    ft=strmid(ft,0,4)+sdoys+strmid(ft,8,18)
endif

hours=gt_tagval(catrecs,/hour,missing='',found=found) 
if not found then begin
   hours='H'+strmid(ftd,11,2)+'00'
   catrecs=add_tag(catrecs,hours,'hour')
endif

yymmdd=strmid(ftd,0,10)
prefix=obsdir
ss=where(prefix eq 'FGFOCUS',sscnt)
if sscnt gt 0 then prefix(ss)='FG' 
fprefix=gt_tagval(catrecs,/FPREFIX,missing='')
ssdiff=where(fprefix ne '',sscnt)
if sscnt gt 0 then prefix(ssdiff)=fprefix(ssdiff)
if xcnt gt 0 then obsdir(ssxrt)=''
darkchk=gt_tagval(catrecs,/ec_imty_,missing='')
darkstr=strarr(n_elements(darkchk))
ssd=where(strlowcase(darkchk) eq 'dark',dcnt)
if dcnt gt 0 then darkstr(ssd)='d'
if catrecs[0].obsdir ne 'XRT' then begin ; XRT protect logic{
   sss=where(catrecs.fprefix eq '' and strpos(catrecs.obs_type,'FG MG4') ne -1,fcnt)
   if fcnt gt 0 then prefix[sss]='FGMG4_'
endif

fname=yymmdd+'/'+obsdir+'/'+strtrim(catrecs.hour,2)+'/'+prefix+ft+ darkstr + '.fits'


retval=concat_dir(top,fname)
;sstrunc=where(catrecs.anytim_dobs ge 8.8015749e+08,ncnt) ; file name res change
; ideally this is handled by reformatter version, but not yet in catalog...
;if ncnt gt 0 or sirius then begin 

old_names=keyword_set(old_names) or gt_tagval(catrecs(0),/obs_type,missing='') eq "CT reference frame"
form=([2,1])(keyword_set(old_names))
retval=strmids(retval,0,strpos(retval,'.fits') - (form+(darkstr eq 'd'))) + $
   darkstr + '.fits'
;endif

if keyword_set(l1) then begin 
   l0dir=(['QuickLook','level0'])(sirius)
   l1dir=(['level1_qkl','level1'])(sirius)
   break_file,retval,ll,pp,ff,ee,vv
   ff='L1_'+ff
   pp=str_replace(pp,l0dir,l1dir)
   retval=pp+ff+ee+vv
endif

if strpos(retval(0),'.uio') ne -1 then retval=retval+'.gz'

if debug then stop,'retval'
return,retval
end
