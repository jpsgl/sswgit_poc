function hinode_time2alignment, time, _extra=_extra ,  $
   column_names=column_names, $
   debug=debug, patch=patch, sspatched=sspatched, data=data, invalid=invalid
;+
;   Name: hinode_time2alignment
;
;   Purpose: ssw time -> Shimizu san alignment table entry(ies)
;
;   Input Parameters:
;      time - desired time(s) - any ssw (string, 'index', catalogs...)
;
;   Output Parameters:
;      function returns one or more alignment table structures
;
;   Keyword Parameters:
;      data (output) - raw rd_tfiles(output) - strarr(#records)
;      column_names - vector of column names (-> tag_names)
;
;   History:
;      14-Jan-2008 - S.L.Freeland 
;      22-Jan-2008 - S.L.Freeland - generic piece -> ssw_csv2struct.pro
;                       auto-bad record reject, NCOL keyword
;       7-jan-2009 - S.L.Freeland - handle csv format change
;       8-jun-2010 - S.L.Freeeland - yet another csv change...
;      16-jun-2011 - S.L.Freeland - ditto, two fewer columns circa august 2010
;      18-dec-2012 - S.L.Freeland - fix last record access
;
;   Method:
;      yes
;-


debug=keyword_set(debug)

file=concat_dir('$SSWDB','hinode/gen/alignment/model/Hinode_alignment_trend_by_Shimizu.csv')

if not file_exist(file) then begin 
   box_message,['You do not have $SSWDB/hinode/gen/alignment/... online',$
     '(ask SSW administrator...)']
   return,-1 ;  EARLY EXIT
endif

refresh=keyword_set(refresh) or n_elements(alignment) eq 0
if refresh then begin 
   data=rd_tfile(file)
   nvalid=n_elements(data)
   if nvalid eq 1 then begin
      data=data(0)
      bdata=byte(data)
      ssbr=[where(bdata eq 13,lcnt),strlen(data)]
      newdata=strarr(lcnt+1)
      new=[0,ssbr+1]
      len=ssbr-new
      for i=0,lcnt do newdata(i)=strmid(data,new(i),len(i))
      data=newdata
   endif
   if keyword_set(invalid) then valid=data else $
   valid=data(where(is_number(strmid(data,0,1)),nvalid))
   p1=ssw_csv2struct(in_data=valid)
   p2=ssw_csv2struct(in_data=valid,ncols=51)
   p3=ssw_csv2struct(in_data=valid,ncols=49) ; slf 16-jun-2011
   if data_chk(p2,/struct) then begin 
      tn1=tag_names(p1)
      tn2=tag_names(p2)
      needss=rem_elem(tn1,tn2,ncnt)
      if ncnt gt 0 then begin 
         for i=0,ncnt-1 do p2=add_tag(p2,'',tn1(needss(i)))
      endif
      alignstruct=concat_struct(p1,p2)
      if data_chk(p3,/struct) then begin 
         tn3=tag_names(p3)
         needss=rem_elem(tn1,tn3,ncnt)
         if ncnt gt 0 then begin 
            for i=0,ncnt-1 do p3=add_tag(p3,'',tn1(needss(i)))
         endif
         alignstruct=concat_struct(alignstruct,p3)
      endif
   endif else begin
      box_message,'P2 not a structure, using P1 verbatim'
      alignstruct=p1
   endelse
   alignstruct.(0)=str_replace(alignstruct.(0),'.','-')
   alignstruct=alignstruct(sort(anytim(alignstruct.(0))))
endif 

retval=alignstruct    ; all data 

if n_elements(time) gt 0 then begin 
  ss=tim2dset(anytim(alignstruct.(0),/int), anytim(time,/int))
  retval=retval(ss)
endif

if debug then stop,'before return,alignstruct
return,retval
end


