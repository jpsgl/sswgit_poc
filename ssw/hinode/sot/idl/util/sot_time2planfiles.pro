
function sot_time2planfiles, input0, input1, debug=debug, voevents=voevents, $
   xrt=xrt, xml=xml
;+
;   Name: sot_time2planfiles
;
;   Purpose: return plan info (stucture vector) for input time/structure
;
;   Input Paramters:
;      input0 - start time (ssw compliant time, sot catalog or index...)
;      input1 - end time (ssw compliant time, sot catalog or index...)
;
;   Output:
;      function returns corresponding *pln files 
;
;   Keyword 
;      voevents/xml (synonyms): if set, return planning VOE xmls, not .pln
;      xrt - if set, XRT (SOT is historical default, guessing by Name:)
;
;   Calling Sequence:
;      IDL> plns = sot_time2planfiles(t0)
;      IDL> plns = sot_time2plnafiles(index0, index1)
;      IDL> plns = sot_time2planfiles(t0, t1, /voevents) ; .xml instead of .pln
;
;   History:
;      23-Feb-2007 - S.L.Freeland
;      27-feb-2007 - S.L.Freeland - broke file logic from sot_time2planinfo
;      15-may-2008 - S.L.Freeland - /VOEVENTS keyword
;      03-jan-2013 - G.L.S - added +/- buffer to time range in plan file
;                            to insure finding match 
;      21-apr-2014 - S.L.Freeland - filter out bad VOE file names
;-

if not exist(input0) then begin
   print, 'Must supply at least one time-related parameter (input0 or input1).
   print, 'Returning empty string.'
   return, -1
endif else begin
   n_elem = n_elements(input0)
   case size(input0, /type) of
      7: begin
            time0 = anytim(input0[0], /ccsds)
            if n_elem eq 1 then $
               time1 = time0 else $
               time1 = anytim(input0[n_elem-1], /ccsds)
         end
      8: begin
            time0 = anytim(input0[0].date_obs, /ccsds)
            if n_elem eq 1 then $
               time1 = time0 else $
               time1 = anytim(input0[n_elem-1].date_obs, /ccsds)
         end
      else: begin
            print, 'Unrecognized INPUT1.  Returning.'
            return, ''
         end
   endcase
endelse

if exist(input1) then begin
   n_elem = n_elements(input1)
   case size(input1, /type) of
      7: begin
            if n_elem eq 1 then $
                  time1 = anytim(input1, /ccsds) else $
                  time1 = anytim(input1[n_elem-1], /ccsds)
         end
      8: begin
            if n_elem eq 1 then $
                  time1 = anytim(input1.date_obs, /ccsds) else $
                  time1 = anytim(input1[n_elem-1].date_obs, /ccsds)
         end
      else: begin
            print, 'Unrecognized INPUT1.  Returning.'
            return, ''
         end
   endcase
endif

debug=keyword_set(debug)
xrt = keyword_set(xrt)
xml = keyword_set(voevents) or keyword_set(xml)
ext = (['.pln','.xml'])(xml)

sswdb = get_logenv('SSWDB')
case keyword_set(xrt) of
   1: begin
      sotmeta = get_logenv('XRT_METADATA') ; location of planfiles
      if not file_exist(sotmeta) then begin
         sotmeta = sswdb + '/hinode/gen/metadata/xrt'
      endif
   end

   else: begin
      sotmeta = get_logenv('SOT_METADATA') ; location of planfiles
      if not file_exist(sotmeta) then begin
         sotmeta = sswdb + '/hinode/gen/metadata/sot'
      endif
   end
endcase

if not file_exist(sotmeta) then begin
   print, 'SOT_TIME2PLANFILES: ' + $
          'Unable to determine METADATA directory. Returning.'
   return, -1
endif

time0_buffered = anytim(anytim(time0) - 3*86400l, /ccsds)
time1_buffered = anytim(anytim(time1), /ccsds)
plantime = timegrid(time0_buffered, time1_buffered, /days)

plandirs = anytim(plantime, /ecs, /date_only)
dailydirs = concat_dir(sotmeta, plandirs)
planfiles = ''
inst = (['SOT-', 'XRT-'])[xrt]
prefix = ([inst, 'VOEvent-'])[xml]

for i=0, n_elements(plantime)-1 do begin 
;  date = ([time2file(plantime(i), /date_only), $
;          anytim(plantime(i), /ccsds, /date_only)])[xml]
   date = ([time2file(plantime(i), /date_only), $
            ''])[xml]
   pfiles = file_search(dailydirs(i), prefix+date+'*'+ext) 
   if 1-xml then pfiles = last_nelem(pfiles) else begin ; added bad-file filter
      times=strextract(pfiles,'VOEvent-','Z-')
      vt=valid_time(times)
      okss=where(vt,okcnt)
      badss=where(1-vt,badcnt)
      case 1 of 
         okcnt eq n_elements(pfiles): ; all ok, keep mouth shut
         okcnt gt 0: begin
		       box_message,['Omitting one or more bad filenames',pfiles[badss]]
		       pfiles = pfiles[okss]
		     end
         else:
      endcase
   endelse   
   planfiles = [planfiles, pfiles] 
endfor


ss = where(file_exist(planfiles), pfcnt)

case 1 of 
   pfcnt eq 0: begin
      box_message, 'No plan files found'
      planfiles = ''
   endcase

; Eliminate initial element:
   pfcnt ge (n_elements(dailydirs)): planfiles=planfiles(1:*)

   else: begin 
;      box_message, 'At least one plan file in your time range not found'
      planfiles = planfiles(ss)
      planfiles = planfiles(where(planfiles ne ''))
   endcase
endcase

ss = where(planfiles ne '', pcnt)
if pcnt gt 0 then planfiles = planfiles(ss)

if debug then stop,'planfiles'

return, planfiles
end

