
pro get_obev, date, ts_saa=ts_saa, te_saa=te_saa, ts_ngt=ts_ngt, te_ngt=te_ngt, $
  ts_xtw=ts_xtw, te_xtw=te_xtw, t_op_start=t_op_start, t_op_end=t_op_end, $
  t0_evt=t0_evt, t1_evt=t1_evt, ts_evt_list=evt_list, topdir_in=topdir_in, $
  topdir_out=topdir_out

if not exist(topdir_in) then begin
  ops_dir = get_logenv('SBOPS_DUEVT_PATH')
  if ops_dir ne '' then begin
    topdir_out = strmid(ops_dir, 0, strlen(ops_dir)-7)
    if strpos(topdir_out,'sotco/') ne -1 then topdir_out = '/net/sotcows/' + topdir_out
  endif else begin
;    dir_try = 
;    print, ''
    print, 'The environmental variable SBOPS_DUEVT_PATH is not defined.  Trying:'
    print, '/home/flare/slb/cp/cmdpln/latest/'
    sbops_duevt_path = '/home/flare/slb/cp/cmdpln/latest/'
    if not file_exist(sbops_duevt_path) then begin
      print, 'Not found.  Trying:'
      print, '/home/sotco/ops/cp/cmdpln/latest/'
      sbops_duevt_path = '/home/sotco/ops/cp/cmdpln/latest/'
      if not file_exist(sbops_duevt_path) then begin
        print, 'Not found.  Trying:'
        print, '/net/sotcows/home/sotco/ops/cp/cmdpln/latest/'
        sbops_duevt_path = '/net/sotcows/home/sotco/ops/cp/cmdpln/latest/'
        if not file_exist(sbops_duevt_path) then begin
          print, 'Not found.  Cannot find OBEV file directory.  Giving up and returning.'
          return
        endif else begin
          topdir_out = strmid(sbops_duevt_path, 0, strlen(ops_dir)-7)
        endelse
      endif else begin
        topdir_out = strmid(sbops_duevt_path, 0, strlen(ops_dir)-7)
      endelse
    endif else begin
      topdir_out = strmid(sbops_duevt_path, 0, strlen(ops_dir)-7)
    endelse
  endelse
endif else begin
  topdir_out = topdir_in
endelse

parent = concat_dir(topdir_out, time2file(date, /date_only))
parent_aux = parent + '/auxiliary'
parent_draft = concat_dir(topdir_out+'/draft_plan', time2file(date, /date_only))
generic_suffix = time2file(date, /date_only) + '????.evt'
prefix_obev = 'obev_'
prefix_op_per = 'op_period_'
file_pat_obev = prefix_obev + generic_suffix
file_pat_op_per = prefix_op_per + generic_suffix

infil_obev = file_list(parent, file_pat_obev)
if infil_obev[0] eq '' then infil_obev = file_list(parent_aux, file_pat_obev)
if infil_obev[0] eq '' then infil_obev = file_list(parent_draft, file_pat_obev)

if infil_obev[0] ne '' then begin
  evt_list = rd_tfiles(infil_obev)
  n_evt = n_elements(evt_list)
  t_evt = strmid(evt_list, 13, 19)
  t_evt = str_replace(t_evt, '.', ' ')
  t0_evt = t_evt[0]
  t1_evt = t_evt[n_evt-1]

  ss_saa_entr = where(strmid(evt_list,0,8) eq 'SAA_ENTR', n_entr)
  ss_saa_exit = where(strmid(evt_list,0,8) eq 'SAA_EXIT', n_exit)

  if n_entr gt 0 then begin
    ts_saa = t_evt[ss_saa_entr]
  endif else begin
    ts_saa = -1
  endelse

  if n_exit gt 0 then begin
    te_saa = t_evt[ss_saa_exit]
  endif else begin
    te_saa = -1
  endelse

  ss_ngt_entr = where(strmid(evt_list,0,8) eq 'NGT_ENTR', n_ngt_entr)
  ss_ngt_exit = where(strmid(evt_list,0,8) eq 'NGT_EXIT', n_ngt_exit)

  if n_ngt_entr gt 0 then begin
    ts_ngt = t_evt[ss_ngt_entr]
  endif else begin
    ts_ngt = -1
  endelse

  if n_ngt_exit gt 0 then begin
    te_ngt = t_evt[ss_ngt_exit]
  endif else begin
    te_ngt = -1
  endelse

  ss_xtw_entr = where(strmid(evt_list,0,8) eq 'XTW_ENTR', n_xtw_entr)
  ss_xtw_exit = where(strmid(evt_list,0,8) eq 'XTW_EXIT', n_xtw_exit)

  if n_xtw_entr gt 0 then begin
    ts_xtw = t_evt[ss_xtw_entr]
  endif else begin
    ts_xtw = -1
  endelse

  if n_xtw_exit gt 0 then begin
    te_xtw = t_evt[ss_xtw_exit]
  endif else begin
    te_xtw = -1
  endelse

endif else begin
  ts_saa = 0
  te_saa = 0
  ts_ngt = 0
  te_ngt = 0
  ts_xtw = 0
  te_xtw = 0
endelse  

infil_op_per = file_list(parent, file_pat_op_per)
if infil_op_per[0] eq '' then infil_op_per = file_list(parent_aux, file_pat_op_per)
if infil_op_per[0] eq '' then infil_op_per = file_list(parent_draft, file_pat_op_per)

if infil_op_per[0] ne '' then begin
  evt_arr_op = rd_tfiles(infil_op_per, 3)
  n_evt_op = n_elements(evt_arr_op[0,*])
  t_evt_op = evt_arr_op[2,*]
  t_evt_op = str_replace(t_evt_op, '.', ' ')
  t0_evt_op = t_evt_op[0]
  t1_evt_op = t_evt_op[n_evt_op-1]

  ss_op_start = where(evt_arr_op[0,*] eq 'OP_START', n_op_start)
  ss_op_end = where(evt_arr_op[0,*] eq 'OP_END', n_op_end)

  if n_op_start eq 1 then begin
    t_op_start = t_evt_op[ss_op_start]
  endif else begin
    t_op_start = -1
  endelse

  if n_op_end eq 1 then begin
    t_op_end = t_evt_op[ss_op_end]
  endif else begin
    t_op_end = -1
  endelse
endif else begin
  t_op_start = 0
  t_op_end = 0
endelse  

end
