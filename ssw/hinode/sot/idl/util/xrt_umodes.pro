function xrt_umodes,index,taglist,_extra=_extra,mcount=mcount, no_roundexp=no_round_exp, add_pointing=add_pointing
;+
;   Name: xrt_umodes
;
;   Purpose: return uniq XRT instrument modes within input index/catalog'
;
;   Input Parameters:
;      index - xrt "index" or catalog records (structure vector)
;      taglist - optional list of tags which define a "mode" (default supplied)
;
;   Output:
;      function returns string array of uniq modes (see ssw_uniq_modes.pro)
;
;   Keyword Parameters:
;      mcount (output) - #matches per mode (1:1 w/function output)
;      no_roundexp - don't round EXPTIME (default rounds to "close enough")
;      add_pointing - if set, include pointing in consideration of mode
;                     (~kludgy, but useful for auto-movie making, for examp)
;
;      _extra - other keywords -> ssw_uniq_modes.pro (see that doc-header)
;               inc, /interactive, where_mode=,... etc..
;
;   Calling Sequence:
;      IDL> umodes=xrt_umodes(catalog [....]) ; xrt_cat output, for example
;      IDL> umodes=xrt_umodes(index [...]) ; read_xrt output, for example
;
;   Calling Example:
;      IDL> t0='15-mar-2007' & t1='16-mar-2007'
;      IDL> xrt_cat,t0,t1,xcat
;      IDL> um=xrt_umodes(xcat, mcount=mc) & more,um+string(mc)
;     256       256  SCI  Open   Al_mesh  normal      4.0000         238
;     256       256  SCI  Open   Al_mesh  normal     33.0000         237
;     256       256  SCI  Open   Ti_poly  normal      0.0000         215
;     512       256  SCI  Open  Al_thick  normal     66.0000          54
;( ... etc..)
;    2048      2048  SCI  Open   Ti_poly  normal      1.0000           5
;    2048      2048  SCI  Open   Ti_poly  normal      8.0000           5
;    2048      2048  SCI  Open   Ti_poly  normal     23.0000          26
;
;
;   Method:
;      set a default taglist and call ssw_uniq_modes.pro
;
;   History:
;      Circa 15-aug-2007 - S.L.Freeland
;      2-nov-2007 - add EC_IMTY_ 
;      
if not keyword_set(no_round_exp) then index.exptime=round(index.exptime) ; avoid extraneous modes

if n_elements(taglist) eq 0 then $
   taglist='naxis1,naxis2,datatype,ec_fw1_,ec_fw2_,ec_imty_,exptime'

if keyword_set(add_pointing) then begin
   car=ssw_index2carr(index,/add,/fast,/round)  
   index=rep_tag_value(index,round(index.car_lon/20),'car_lon')
   taglist=taglist+',car_lon,hel_lat'
endif

umodes=ssw_uniq_modes(index,taglist,_extra=_extra,mcount=mcount)
return,umodes
end
