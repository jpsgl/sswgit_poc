pro sot_ctmovie, time0, time1, index, data, lastn=lastn, incxy=incxy, $
   cadence_min=cadence_min, maxframes=maxframes, binfact=binfact, $
   www=www,outdir=outdir, zbuffer=zbuffer

common sot_ctmovie_blk,lcat

incxy=keyword_set(incxy)

www=keyword_set(www) or keyword_set(outdir) or incxy
zbuffer=keyword_set(zbuffer) or !d.name eq 'Z'

ctstring='obs_type=CT*reference*'
init=n_elements(lcat) eq 0 or keyword_set(refresh)

if init then begin 
   if n_params() eq 2 then begin 
      sot_cat,time0,time1,cat,search=ctstring
   endif else begin 
      sot_cat,reltime(days=-2),reltime(/now),cat, search=ctstring
   endelse
endif else cat=lcat

mindex=struct2ssw(cat,/nopoint)

nct=n_elements(cat)

ss=lindgen(nct)

if keyword_set(cadence_min) or keyword_set(maxframes) then begin  
   ss=grid_data(mindex,minutes=cadence_min,nsamp=maxframes)
endif
if keyword_set(lastn) then ss=last_nelem(ss,lastn)


files=sot_cat2files(cat(ss))
read_sot,files,index,data
mindex=struct2ssw(index,/nopoint)
if www and n_elements(binfact) eq 0 then binfact=7
if keyword_set(binfact) then begin 
   data=confac(temporary(data),binfact)
   mreadfits_fixup,index,data
endif
if www then begin 
   sdata=ssw_sigscale(index,data>800<1400)
   if keyword_set(incxy) then begin 
      sdata=bytscl(temporary(sdata),top=255-15) + 15
      ptemp=!d.name
      wdef,xx,data_chk(sdata,/nx),128,zbuffer=zbuffer
      linecolors
      utplot,mindex,mindex.xcen,/ynoz,back=11,color=5,ytit='XCEN', $
         charsize=.7
      event_movie3,mindex,outmovie,data=sdata, $
         outsize=  [!d.x_size,128]
      utplot,mindex,mindex.ycen,/ynoz,back=11,color=9,ytit='YCEN', $
         charsize=.7
      delvarx,sdata
      event_movie3,mindex,sdata,data=outmovie , $
         outsize=  [!d.x_size,128]
      set_plot,ptemp
   endif
   if not data_chk(outdir,/string) then outdir=$
      str_replace(strcompress(sot_umodes(incat=cat(0))),' ','_')
   movie_dir=concat_dir(curdir(),outdir)
   if not file_exist(movie_dir) then begin 
      box_message,'Creating movie_dir> ' + movie_dir
      spawn,['mkdir','-p',movie_dir],/noshell
   endif
   movie_name='CT_'+time2file(mindex(0))
   tvlct,r,g,b,/get
   special_movie,mindex,sdata,r,g,b,/inctimes, thumbsize=150, $
      movie_name=movie_name, movie_dir=movie_dir
endif
 
stop

end


