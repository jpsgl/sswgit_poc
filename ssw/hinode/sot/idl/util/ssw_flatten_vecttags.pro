function ssw_flatten_vecttags, js
;+
;   Name: ssw_flattern_vecttags
;
;   Purpose:   {one:fltarr(10),two:fltarr(10)} -> 10x{one:0.,two:0.}
;
;   Input Paramters:
;      js - single structure which has vector tags
;
;   Output:
;      function returns structure vector with scalar tag
;
;   History:
;      18-jul-2008 - generalization of jsoc utility
;
;   Restrictions:
;     all tags are 1D vectors with same #elements
;     uses execute so not VM compatible
;-
;      
ntags=n_tags(js)
varr='js.('+strtrim(indgen(ntags),2)+')(0)'
count=n_elements(js.(0))

tnames=tag_names(js)
estring='tnames,' + arr2str(varr)
es='retval=create_struct('+estring + ')'
estat=execute(es)

if n_elements(js.(0)) gt 1 then retval=replicate(retval(0),count)

for i=0,ntags-1 do retval.(i)=js.(i) 

return,retval
end
