pro sot_make_genxcat, time0,  time1, days=days, month=month, debug=debug, $
   check_naxis3=check_naxis3, temp=temp, uniq_naxis3=uniq_naxis3, xrt=xrt, $
   hinode=hinode, sirius=sirius, quicklook=quicklook
;
;+
;   Name: sot_make_genxcat
;
;   Purpose: generate genx catalog for SolarB FPP(SOT) & XRT
;
;   Input Parameters:
;      time0,time1 - time range to process
;
;   Keyword Parameters:
;      days - catalog granularity in DAYS (/DAYS = 1 = daily)
;      month - catalog granularity (/MONTH = monthly)
;      hinode - write to $SSW/hinode/<inst> rather than $SSW/packages/solarb
;      sirius - if set, write to $SSWDB/hinode/<instr>/<inst_>genxcat_sirius/
;
;   History:
;      25-March-2006 - S.L.Freeland
;       3-April-2006 - tweaks... protect against mixed axis# observables
;                      include S* and I* observables
;      19-sep-2006 - S.L.Freeland - fpp->sot, update xrt taglist
;      11-Oct-2006 - S.L.Freeland - add /hinode 
;      12-Oct-2006 - S.L.Freeland - case where OBSDIR ne PREFIX via .FPREFIX
;       1-dec-2006 - S.L.Freeland - add some keywords for SOT
;      20-feb-2006 - S.L.Freeland - PROG_NO
;       28-feb-2007 - S.L.Freeland - PROG_VER
;        9-Mar-2007 - S.L.Freeland - include PROG_NO & PROG_VER for XRT as well
;        4-apr-2007 - S.L.Freeland - add /SIRIUS keyword & function
;       15-may-2007 - S.L.Freeland - add some XRT keyrd per 
;                                       Jonathan Cirtain and Mark Weber
;       22-may-2007 - S.L.Freeland - add post processing mode removal
;                                    (initially for certain SOT sequences)
;       15-jun-2007 - S.L.Freeland - add a couple of tags
;       26-Jul-2007 - S.L.Freeland - couple more for SP scans and whatnot
;        7-aug-2007 - S.L.Freeland - fixed logic error!
;-
;   
debug=keyword_set(debug)
if data_chk(observables,/string) then doobs=observables else $   
   doobs=str2arr('FGRAW,SRAW,CR,CL,FGFOCUS,FG')

catgrid=timegrid(time0,time1,days=days,month=month,/ecs)
xrt=keyword_set(xrt)
if not xrt then strtemp=$
   {naxis:0,naxis1:0,naxis2:0,naxis3:0,xcen:0,ycen:0,obs_type:'',$
    cdelt1:0.,crota1:0.,exptime:0.,wave:'',slit:0,focus:0,date_obs:'', $
    fovx:0,fovy:0,target:'',jop_id:0,observer:'',planner:'',obstitle:'',$
    anytim_dobs:0.d, telescop:'',fileorig:'',waveid:0b,darkflag:0b,awveoff:0b,$
   campsum:0b,camssum:0b,prog_no:0b,prog_ver:0b,ver_rf0:'',spnint:0b, $
   slitpos:0,scn_sum:0b,slitindx:0,macroid:0}
  

case 1 of
   xrt: ctags='date_obs,ec_fw1_,ec_fw2_,ec_fw1,ec_fw2,e_fw1_p,e_fw2_p,ec_vl,ec_vl_,exptime,xcen,ycen,fovx,fovy,tr_mode,obstitle,target,sci_obs,obs_dec,join_sb,obs_id,jop_id,observer,planner,tohbans,datatype,flflg,naxis1,naxis2,telescop,fileorig,prog_no,prog_ver,foc_pos,ver_rf0,ec_inde,ec_imtyp,ec_imty_,chip_sum,cal_info,calimage'
   else: ctags='date_obs,naxis,naxis1,naxis2,naxis3,xcen,ycen,cdelt1,crota1,exptime,fovx,fovy,obstitle,target,jop_id,observer,planner,slit,focus,instrme,obs_type,wave,telescop,fileorig,waveid,darkflag,waveoff,campsum,camssum,prog_no,prog_ver,ver_rf0,spnint,slitpos,scn_sum,slitindx,macroid'
endcase

ncat=n_elements(catgrid)
nobs=n_elements(doobs)
allobs=1-xrt
catpre=(['sot','xrt'])(xrt)

subdir=catpre+'_genxcat' + (['','_sirius'])(keyword_set(sirius))
subdir=subdir+(['','_temp'])(keyword_set(temp))

topdir=([concat_dir('$SSWDB','packages/solarb/'),$
         concat_dir(concat_dir('$SSWDB','hinode'),catpre)])(keyword_set(hinode))

cdir=concat_dir(topdir,subdir) 
    
box_message,'Output to>> ' + cdir
if not file_exist(cdir) then file_mkdir,cdir

for i=0,ncat-2 do begin ; for each catalog 
      box_message,arr2str([catgrid(i),catgrid(i+1)],'-TO-')
      ff=sot_time2files(catgrid(i),catgrid(i+1),xrt=xrt,allobs=allobs,/noct)
if debug then stop,'ff

      if ff(0) ne '' then begin 
         cols=str2cols(ff,/un,'/') 
         ncols=data_chk(cols,/nx)
         strtab2vect,cols,columns=indgen(3)+(ncols-3),obs,hour,fname
         if xrt then obs=replicate('XRT',n_elements(ff))
         uobs=all_vals(obs)
         file_append,'allobs.dat',uobs,/uniq ; keep track of all OBS  
         first=strmid(uobs,0,1)
         ssok=where(first eq 'C' or first eq 'F' or $  ; only science files
                    first eq 'S' or first eq 'I' or first eq 'X',ocnt)
         if ocnt gt 0 then begin 
         ; form template superset (mix&match observables)
         if not xrt then begin 
         xfils=strarr(ocnt)     ; include one of each obs type
         for uf=0,ocnt-1 do xfils(uf)=ff((where(obs eq uobs(ssok(uf))))(0))
         if keyword_set(check_naxis3) then begin 
            for uf=0,ocnt-1 do begin 
               read_sot,xfils(uf),ii,/uniq_naxis3
               box_message,xfils(uf)
               help,gt_tagval(ii,/NAXIS3,missing=-1) 
            endfor
         endif  
         if not data_chk(strtemp,/struct) then $
            read_sot,xfils,strtemp,/all_keywords
         t3ss=tag_exist(strtemp,'NAXIS3')
         if not t3ss then strtemp=add_tag(strtemp,0,'NAXIS3',index='NAXIS2')
         strtemp.naxis3=0  ; assure init=0
         endif else begin 
            box_message,'xrt'
            read_sot,ff(0),strtemp 
         endelse
         for ob=0,ocnt-1 do begin
            box_message,'Observable>> ' + uobs(ssok(ob))
            sso=where(obs eq uobs(ssok(ob)),fcnt)
            delvarx,cdata
            read_sot,ff(sso),cdata,strtemp=strtemp(0)
            ndata=n_elements(cdata)
            case 1 of 
               ndata eq fcnt: ; 1:1 - ok as is
               ndata mod fcnt eq 0: begin
                  box_message,'Even multiple, assuming 3D'
                  css=lindgen(fcnt)*(ndata/fcnt)
                  cdata=temporary(cdata(css))
               endcase
               else: begin 
                  utims=uniq(cdata.date_obs)
                  if n_elements(sso) ne n_elements(utims) then $
                    box_message,['WARNING: mismatch nfiles:nindex!!',ff(sso(0))]
                    cdata=cdata(utims)
               endcase
            endcase
            subdata=str_subset(cdata,ctags)
            subdata=add_tag(subdata,'','FPREFIX')

            ssdiff=where(strpos(ff(sso),uobs(ssok(ob))+'200') eq -1, dcnt)
            if dcnt gt 0 then begin
               break_file,ff(sso),ll,pp,fname
               subdata.fprefix=ssw_strsplit(fname,'200',/head)
            endif

            subdata=rep_tag_name(subdata,'TELESCOP','HOUR')
            subdata.hour=hour(sso)
            subdata=rep_tag_name(subdata,'FILEORIG','OBSDIR')
            subdata.obsdir=uobs(ssok(ob))
            nax3=gt_tagval(cdata,/naxis3,missing=0)
if keyword_set(check_naxis3) then stop,'nax3'
            subdata=add_tag(subdata,0,'NAXIS3',index='NAXIS2')
            subdata.naxis3=nax3

            retx=add_tag(subdata,anytim(subdata.date_obs),'anytim_dobs')
            if data_chk(allobsx,/struct) then begin 
               delvarx,err
               allobsx=concat_struct(allobsx,retx,err=err )
               if err(0) ne '' then begin 
                  help,allobsx, retx,/str
                  print,ff(sso)
                endif
            endif else begin 
               allobsx=temporary(retx)
            endelse
         endfor
            order=sort(allobsx.anytim_dobs) ; ->cronolog
            allobsx=allobsx(order)
 ;           post processing removals
            okcnt=n_elements(allobsx)
            if xrt then okss=lindgen(okcnt) else begin ; so far, nothing for XRT
               remove=$
               ((allobsx.wave eq "BFI no move" and allobsx.naxis1 eq 256) or $
                (allobsx.wave eq "TF Na I 5896" and allobsx.naxis1 le 256))
               okss=where(1-remove,okcnt)
               if total(remove) gt 0 then $
                  box_message,'Removing post-processing sequences'
            endelse
            if okcnt gt 1 then begin 
               allobsx=allobsx(okss)
               write_genxcat,allobsx,topdir=cdir, prefix=catpre, $
              /nelements,/geny,/delete,/day_round
            endif else box_message,'No frames after removal(??!!)' 
            delvarx,allobsx ; start over for next catalog
         endif
      endif
endfor

return
end
