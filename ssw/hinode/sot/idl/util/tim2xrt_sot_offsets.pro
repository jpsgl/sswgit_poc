
function tim2xrt_sot_offsets, item, status=status, $
  sot_fovcen2xrt_fovcen_ns_a=sot_fovcen2xrt_fovcen_ns_a, t_sot_fovcen2xrt_fovcen_ns_a=t_sot_fovcen2xrt_fovcen_ns_a, $
  sot_fovcen2xrt_fovcen_ns_b=sot_fovcen2xrt_fovcen_ns_b, t_sot_fovcen2xrt_fovcen_ns_b=t_sot_fovcen2xrt_fovcen_ns_b, $
  sot_fovcen2xrt_fovcen_ew_a=sot_fovcen2xrt_fovcen_ew_a, t_sot_fovcen2xrt_fovcen_ew_a=t_sot_fovcen2xrt_fovcen_ew_a, $
  sot_fovcen2xrt_fovcen_ew_b=sot_fovcen2xrt_fovcen_ew_b, t_sot_fovcen2xrt_fovcen_ew_b=t_sot_fovcen2xrt_fovcen_ew_b, $
  qstop=qstop
;  pnt_struct=pnt_struct, t_ns, t_ew, off_nsa, off_nsb, off_ns_avg, off_ewa, off_ewb, off_ew_avg

;+
;   Name: TIM2XRT_SOT_OFFSETS
;
;   Purpose: Return interpolated xrt-sot offsets for input time(s)
;
;   Input Parameters:
;      ITEM - Vector of times in any SSW compliant format (see ANYTIM.PRO) or 
;             vector of SSW compliant index structures with DATE_OBS tag
;   Output Paramters:
;      OFFSETS - Interpolated xrt-sot offsets for input time(s)
;   Optional Outputs:
;      STATUS - Vector of status values for each input time.  Interpretation of values:
;               +1 - time value falls within range of alignment data values
;               -1 - time value earlier then first NS table point
;               -2 - time value later then last NS table point
;               -3 - time value earlier then first EW table point
;               -4 - time value later then last EW table point
;
;   Keyword Parameters:
;      
;   Calling Sequence:
;      IDL> offsets = tim2xrt_sot_offsets(item)
;
;   History:
;      07-oct-2008 - G.L.Slater
;      09-oct-2008 - S.L.Freeland -> $SSW & add environmental lookup
;      03-feb-2009 - G.L.Slater - Broke out Shimizu table read to rd_align_data.pro
;                               - Changed spline extrapolation of outlier points to nearest table values
;                               - Added WARNING message for out of table range input times
;                               - Added STATUS optional output parameter to show out of range input times
;-

if size(item, /type) eq 8 then begin
  times = anytim(item.date_obs)
endif else begin
  times = anytim(item, /yoh)
endelse
times_sec = anytim(times)
n_tim = n_elements(times)

;dbf=concat_dir(concat_dir('$SOT_CALIBRATION','hinode'),'sot_xrt_offsets')
;restgenx, pnt_struct, t_ns, t_ew, off_nsa, off_nsb, off_ns_avg, off_ewa, off_ewb, off_ew_avg, file = dbf 
;off_ns_spline = spline(t_ns, off_ns_avg, times)
;off_ew_spline = spline(t_ew, off_ew_avg, times)
;offsets = [[off_ew_spline], [off_ns_spline]]

rd_align_data, $
  r_solar=r_solar, t_r_solar=t_r_solar, $
  disk_cen_sot_ns_a=disk_cen_sot_ns_a, t_disk_cen_sot_ns_a=t_disk_cen_sot_ns_a, $
  disk_cen_sot_ns_b=disk_cen_sot_ns_b, t_disk_cen_sot_ns_b=t_disk_cen_sot_ns_b, $
  disk_cen_sot_ew_a=disk_cen_sot_ew_a, t_disk_cen_sot_ew_a=t_disk_cen_sot_ew_a, $
  disk_cen_sot_ew_b=disk_cen_sot_ew_b, t_disk_cen_sot_ew_b=t_disk_cen_sot_ew_b, $
  sot_fovcen2scpnt_ns=sot_fovcen2scpnt_ns, t_sot_fovcen2scpnt_ns=t_sot_fovcen2scpnt_ns, $
  sot_fovcen2scpnt_ew=sot_fovcen2scpnt_ew, t_sot_fovcen2scpnt_ew=t_sot_fovcen2scpnt_ew, $
  sot_fovcen2xrt_fovcen_ns_a=sot_fovcen2xrt_fovcen_ns_a, t_sot_fovcen2xrt_fovcen_ns_a=t_sot_fovcen2xrt_fovcen_ns_a, $
  sot_fovcen2xrt_fovcen_ns_b=sot_fovcen2xrt_fovcen_ns_b, t_sot_fovcen2xrt_fovcen_ns_b=t_sot_fovcen2xrt_fovcen_ns_b, $
  sot_fovcen2xrt_fovcen_ew_a=sot_fovcen2xrt_fovcen_ew_a, t_sot_fovcen2xrt_fovcen_ew_a=t_sot_fovcen2xrt_fovcen_ew_a, $
  sot_fovcen2xrt_fovcen_ew_b=sot_fovcen2xrt_fovcen_ew_b, t_sot_fovcen2xrt_fovcen_ew_b=t_sot_fovcen2xrt_fovcen_ew_b

; KLUGE - Fix sign error in first (Mercury transit) value for sot_fovcen2xrt_fovcen_ew_a
; KLUGE - Replicate first sot_fovcen2xrt_fovcen points for both a and b:
sot_fovcen2xrt_fovcen_ew_a[0] = -sot_fovcen2xrt_fovcen_ew_a[0]
sot_fovcen2xrt_fovcen_ns_b = [sot_fovcen2xrt_fovcen_ns_a[0], sot_fovcen2xrt_fovcen_ns_b]
sot_fovcen2xrt_fovcen_ew_b = [sot_fovcen2xrt_fovcen_ew_a[0], sot_fovcen2xrt_fovcen_ew_b]

t_sot_fovcen2xrt_fovcen_ns_b = [t_sot_fovcen2xrt_fovcen_ns_a[0], t_sot_fovcen2xrt_fovcen_ns_b]
t_sot_fovcen2xrt_fovcen_ew_b = [t_sot_fovcen2xrt_fovcen_ew_a[0], t_sot_fovcen2xrt_fovcen_ew_b]

n_tim_ns_a = n_elements(t_sot_fovcen2xrt_fovcen_ns_a)
n_tim_ns_b = n_elements(t_sot_fovcen2xrt_fovcen_ns_b)
n_tim_ew_a = n_elements(t_sot_fovcen2xrt_fovcen_ew_a)
n_tim_ew_b = n_elements(t_sot_fovcen2xrt_fovcen_ew_b)
if ( (n_tim_ns_a ne n_tim_ns_b) or (n_tim_ew_a ne n_tim_ew_b) ) then $
  stop, 'Number of points for different offsets are different.  Stopping.'

sot_fovcen2xrt_fovcen_ns_avg = (sot_fovcen2xrt_fovcen_ns_a + sot_fovcen2xrt_fovcen_ns_b)/2
sot_fovcen2xrt_fovcen_ew_avg = (sot_fovcen2xrt_fovcen_ew_a + sot_fovcen2xrt_fovcen_ew_b)/2

t_ns_sec = anytim(t_sot_fovcen2xrt_fovcen_ns_a)
t_ew_sec = anytim(t_sot_fovcen2xrt_fovcen_ew_a)

off_ns_spline = spline(t_ns_sec, sot_fovcen2xrt_fovcen_ns_avg, times_sec)
off_ew_spline = spline(t_ew_sec, sot_fovcen2xrt_fovcen_ew_avg, times_sec)
status = intarr(n_tim) + 1

ss_before_ns = where(times_sec le t_ns_sec[0], n_before_ns)
ss_after_ns  = where(times_sec ge t_ns_sec[n_tim_ns_a-1], n_after_ns)
ss_before_ew = where(times_sec le t_ew_sec[0], n_before_ew)
ss_after_ew  = where(times_sec ge t_ew_sec[n_tim_ew_a-1], n_after_ew)

if n_before_ns gt 0 then begin
  off_ns_spline[ss_before_ns] = sot_fovcen2xrt_fovcen_ns_avg[0]
  status[ss_before_ns] = -1
endif
if n_after_ns gt 0 then begin
  off_ns_spline[ss_after_ns] = sot_fovcen2xrt_fovcen_ns_avg[n_tim_ns_a-1]
  status[ss_after_ns] = -2
endif
if n_before_ew gt 0 then begin
  off_ew_spline[ss_before_ew] = sot_fovcen2xrt_fovcen_ew_avg[0]
  status[ss_before_ew] = -3
endif
if n_after_ew gt 0 then begin
  off_ew_spline[ss_after_ew] = sot_fovcen2xrt_fovcen_ew_avg[n_tim_ew_a-1]
  status[ss_after_ew] = -4
endif

if ( (n_before_ns gt 0) or (n_before_ew gt 0) ) then $
  print, ' WARNING: Some or all times before first time in table.'
  print, '          Setting the corresponding values to first table vlaues.'
if ( (n_after_ns gt 0) or (n_after_ew gt 0) ) then $
  print, ' WARNING: Some or all times after last time in table.'
  print, '          Setting the corresponding values to last table values.'

offsets = [[off_ew_spline], [off_ns_spline]]

if keyword_set(qstop) then stop,' Stopping on request.'

return, offsets

end

