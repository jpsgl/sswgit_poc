pro do_sot_demo, demofile, index, data,  pause=pause, wait=wait
;+
;   Name: do_demo
;
;   Purpose: run an idl demo program (format can be IDL main routine)
;
;   Input Parameters:
;      demofile - name of file to demo (execute) - if none, menu select from
;                 files in $DIR_GEN_DOC with names: XXXdemo.pro
;
;   Keyword Parameters:
;      pause - if set, pause after IDL statement execution until user <CR>s
;      wait  - if set, number of seconds to wait between each line
;
;   Calling Sequence:
;      do_demo [,demofile]
;
;   Non comment lines in demofile are displayed with highlights to terminal
;   and then executed - comment lines are echoed
;
;   History:
;      10-Jan-1995 (SLF)
;      23-oct-2007 (! time to retire) - use xmenu_sel
;       2-nov-2007 - allow "IDL>" in commands      
;       8-nov-2007 - do_demo->do_sot_demo 
;  
;   Restrictions:
;      single line IDL commands for now
;-
if keyword_set(wait) then iwait=wait else iwait=0.
pause=keyword_set(pause)

if n_elements(demofile) eq 0 then begin
   demofiles=file_search('$SOT_DEMO','*.pro')
   case 1 of 
      demofiles(0) eq '': begin
         tbeep
         message,/info,"Can't find any demo files and none supplied, returning..."
         return
      endcase
      n_elements(demofiles) gt 1: begin
         tbeep
         message,/info,"Select a demo file..."
         ss=xmenu_sel(demofiles,/one)
         if ss(0) eq -1 then message,"Nothing selected, Aborting..."
         demofile=demofiles(ss)
      endcase
      else: demofile=demofiles(0)
   endcase
endif

if not file_exist(demofile) then begin
   tbeep
   message,/info,"Demo file: " + demofile + " not found, returning..."
   return
endif

input=rd_tfile(demofile(0),/compress)

line="--------------------------------------------------------"
prstr,["","Executing Demo File: " + demofile,line,""]
qtemp=!quiet
!quiet=1		; shut off compilation messages
resp=''
for i=0,n_elements(input) -1 do begin
   case 1 of 
      input(i) eq 'end':
      strlen(input(i)) le 1: print
      strmid(input(i),0,1) eq ';': prstr,[strmid(input(i),1,1000)]
      else: begin
         cmd=strtrim(input(i),2)
         if strpos(cmd,'IDL>') eq 0 then cmd=strtrim(strmid(cmd,4,1000),2)
         if strmid(cmd,0,1) ne strupcase(strmid(cmd,0,1)) then begin 
         prstr,strjustify(["IDL> " + cmd],/box)
         wait,iwait
         exestat=execute(cmd)      
         lastcom=0
         if pause then begin
            print
            read,"Enter <CR> to continue, anything else to quit: ",resp
            if resp ne "" then message,"Aborting on request..."
         endif    
         endif ; else box_message,cmd,nbox=2
      endcase
   endcase
endfor

!quiet=qtemp
tbeep
prstr,["","End of Demo..."]
return 
end
