;+
; NAME:
;       sot_select_ev
; PURPOSE:
;       The event handler for sot_select
;
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL 
;       10-May-2007 - Version 1.3 - Marty Hu LMSAL
;       29-Jun-2007 - Version 1.4 - Marty Hu LMSAL
;-

pro sot_select_event, ev

widget_control, ev.id, get_uvalue=uval

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, filnam, ss, result, filnam_sel, index, data,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat,index_sel, data_sel, big_base, w_tag_names1, tag_names_arr, w_1, blog_arr, w1, searchstring, blog_base

current_day = strmid(reltime(days=-1),0,2)
current_month = strmid(reltime(days=-1),3,3)
current_year = '20'+strmid(reltime(days=-1),7,2); put reltime in current date/time instead of international

hours_arr = ['00','01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17','18', '19', '20','21', '22','23']
minutes_arr = ['00', '10', '20','30', '40', '50']
minutes_arr1 = ['00', '10', '20','30', '40', '50']
days_arr= [current_day,strcompress(indgen(31)+1)]
months_arr= [current_month,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
years_arr= [current_year,'2006', '2007', '2008']

hours=hours_arr[widget_info(w_hours, /droplist_select)]
minutes=minutes_arr[widget_info(w_minutes,/droplist_select)]
day=days_arr[widget_info(w_day,/droplist_select)]
month=months_arr[widget_info(w_month,/droplist_select)]
year=years_arr[widget_info(w_year,/droplist_select)]
hours1=hours_arr[widget_info(w_hours1, /droplist_select)]
minutes1=minutes_arr1[widget_info(w_minutes,/droplist_select)]
day1=days_arr[widget_info(w_day1,/droplist_select)]
month1=months_arr[widget_info(w_month1,/droplist_select)]
year1=years_arr[widget_info(w_year1,/droplist_select)]

if uval eq 'searchall' then begin

    if exist(w_obstype1) then begin
        if widget_info(w_obstype1, /valid_id) then begin
            widget_control, big_base, /destroy
        endif
    endif

    if exist(w_1) then begin
        if size(w_1, /type) ne 7 then begin
            if widget_info(w_1, /valid_id) then begin
                widget_control, blog_base, /destroy
            endif
        endif
    endif

    t0=day+'-'+month+'-'+year+' '+hours+':'+minutes
    t1=day1+'-'+month1+'-'+year1+' '+hours1+':'+minutes
    widget_control, /hourglass
    sot_cat, t0, t1, full_cat, /sirius
    cat=full_cat

    if size(full_cat[0], /type) eq 8 then begin

        big_base = widget_base(ev.top, /column)
        widget_control, big_base, update=0
        num_images = widget_label(big_base, value = 'Found '+ strtrim(n_elements(cat),2) + ' images: Please refine search, Wild Cards OK->')
        wildcards = widget_base(big_base, /row)

        obstype_arr = ssw_uniq_modes(cat,'obs_type')
        obstype_arr = ['*',obstype_arr]
        wavelength_arr = ssw_uniq_modes(cat,'wave')
        wavelength_arr = ['*',wavelength_arr]

        obstype = widget_base(wildcards, /column)
        w_obstype1 = cw_bgroup(obstype, obstype_arr, uvalue ='obstype', /exclusive,$
                               /frame, label_top='Obs. TYPE', set_value=0)
        
        wavelength = widget_base(wildcards, /column)
        w_wavelength1 = cw_bgroup(wavelength, wavelength_arr, uvalue = 'wavelength',$
                                  /exclusive, /frame, label_top = 'Wavelength', set_value=0)
        
        nx_arr = ssw_uniq_modes(cat, 'naxis1')
        nx_arr = ['*',nx_arr]
        nx = widget_base(wildcards, /row)
        w_nx1 = cw_bgroup(nx, nx_arr, uvalue = 'nx', /exclusive, /frame,$
                          label_top = 'NX', set_value=0)

        ny_arr = ssw_uniq_modes(cat, 'naxis2')
        ny_arr = ['*',ny_arr]
        ny = widget_base(wildcards, /row)
        w_ny1 = cw_bgroup(ny, ny_arr, uvalue = 'ny', /exclusive, /frame,$
                          label_top = 'NY', set_value=0)

        spatial = widget_base(big_base, /row)

        tag_names_arr = ['*',tag_names(cat)]
        tag_names = widget_base(spatial, /row)
        w_tag_names1 = widget_droplist(tag_names, value = tag_names_arr, title = 'Tag Names', uvalue = 'tag')

        refine_basis = widget_base(big_base, /row)
        refine_base = widget_base(refine_basis, /row)
        refine_search = widget_button(refine_base, value = 'Refine', uvalue = 'refine')
        widget_control, big_base, /update

    endif else NoData = dialog_message('Please Reselect Parameters (No Data Found)')

endif

if uval eq 'tag' then begin

    num = widget_info(w_tag_names1, /droplist_select)
    searchstring = tag_names_arr[num]
    statement = "blog_arr = ssw_uniq_modes(full_cat, '"+ searchstring +"')"
    retval=execute(statement)

; makes an array of all the values for a given string, (ie. for cat.wave)

    if exist(w_1) then begin
        if size(w_1, /type) ne 7 then begin
            if widget_info(w_1, /valid_id) then begin
                widget_control, blog_base, /destroy
            endif
        endif
    endif

    blog_arr = ['*',blog_arr]
    blog_base = widget_base(ev.top, /row)
    w_1 = widget_droplist(blog_base, value=[blog_arr], uvalue = 'w1', title = searchstring)    

endif

if uval eq 'refine' then begin

    cat=full_cat
    widget_control, w_obstype1, get_value=obs_index
    obstype1=obstype_arr[obs_index]
    widget_control, w_wavelength1, get_value=wave_index
    wavelength1=wavelength_arr[wave_index]
    widget_control, w_nx1, get_value=nx_index
    nx1=nx_arr[nx_index]
    widget_control, w_ny1, get_value=ny_index
    ny1=ny_arr[ny_index]

    if exist(w_1) then begin
        if size(w_1, /type) ne 7 then begin
            if widget_info(w_1, /valid_id) then w1=strtrim(blog_arr[widget_info(w_1, /droplist_select)],2)
        endif
    endif

;if the searchstring (structure tag) widget exists then find the value
;of the the item selected and store it in w1

m=0
dum=0 ;clear value for dum

    if size(cat[0], /TYPE) eq 8 then begin

        cat_arr=['cat.obs_type', 'obstype1', 'cat.wave', 'wavelength1', 'cat.naxis1', 'nx1', 'cat.naxis2', 'ny1']
        statement = "if exist(searchstring) then cat_arr = [cat_arr, 'cat.'+searchstring]"

        retval = execute(statement)

        if exist(w1) then begin
            if w1 ne '0' then cat_arr = [cat_arr, 'w1']
        endif

        blank_arr = lindgen(n_elements(cat_arr))
        blank_arr[*] = 1

        for ii=0,n_elements(cat_arr)-1 do begin

            statement = "if strtrim(("+cat_arr[ii]+")(0),2) eq '*' then blank_arr[ii] = 0"
            retval = execute(statement)
            if blank_arr[ii] eq 0 then blank_arr[ii-1] = 0

        endfor

        filled_cat = where(blank_arr eq 1)

        if filled_cat[0] ne -1 then begin
            
            cat_arr1=cat_arr[filled_cat]

            for ii=0, n_elements(cat_arr)-2,2 do begin

                if ii ne 2 then begin

                    statement='m=where(strtrim('+cat_arr[ii+1]+',2) eq strtrim('+cat_arr[ii]+',2))'
                    retval=execute(statement)
                    if m[0] ne -1 then cat=cat[m]
                    statement = "if m[0] eq -1 and "+cat_arr[ii+1]+" ne '*' then dum=1"                
                    retval = execute(statement)

                endif else begin

                    statement='m=where(cat.wave eq strtrim("'+wavelength1+'",2))'
                    retval=execute(statement)
                    if wavelength1 ne '*' and m[0] ne -1 then cat = cat[m]

                endelse

            endfor

        endif

        if size(cat, /type) eq 8 and dum eq 0 then begin
            
            widget_control, /hourglass

            if filnam eq 1 then sot_list, /level0 else sot_list
            
        endif else NoData = dialog_message('Please Reselect Parameters (No Data Found)')
        
    endif else print, 'try again 3'

endif

if uval eq 'dismiss' then begin

    widget_control, ev.top, /destroy

endif

end

PRO sot_select, index_out, data_out, files_out, level0=level0

;+
; NAME:
;       sot_select
; PURPOSE:
;       Browse and filter SOT database via GUI, iteratively select files,
;       read and display selections.  Functionality of the GUI is meant to
;       match that of the web-based SOT data browser.
; CALLING SEQUENCE:
;       sot_select [,index_out [,data_out], [files_out],level0=level0]
; INPUTS: None so far.
; KEYWORD PARAMETERS:
;       level0: Use level0 catalog (default is Quicklook)
; OUTPUTS:
;       INDEX_OUT - Index record array for selected files
;       DATA_OUT  - Data array for selected files
;       FILES_OUT - Filename array for selected files
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL 
;                   - Fixed display button to read data and display it in one click
;                   - Increased y size so more files are available
;                   - Widget now focuses on each selection, rather than the top
;                   - Display is only the atv call
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL
;                   - ListBox widget to replace droplist widget with wavelength, obstype, etc. (1/24/07)
;                   - Make use of the multiple keyword (2.1.07)
;       10-May-2007 - Version 1.3 Marty Hu LMSAL
;                   - Added structure tag keyword search
;                   - Resizable widget functionality
;                   - Cosmetic changes and removal of common blocks
;                   - Recurrent searches are more user friendly
;       29-Jun-2007 - Version 1.4 Marty Hu LMSAL
;                   - Fixed filnames so they are not all the same
;                   - Added level0 keyword, panorama, and sodasurf
;                   - Added files keyword
;-

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, filnam, ss, result, filnam_sel, index, data,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat,index_sel, data_sel, big_base, w_tag_names1, tag_names_arr, w_1, blog_arr, w1, searchstring, blog_base

base=widget_base(/column, title='SOT Select')

if not exist(index) then index= -1
if not exist(data) then data= -1

if keyword_set(level0) then filnam=1 else filnam=0

start_search = widget_base(base, /row)
current_day = strmid(reltime(days=-1),0,2)
current_month = strmid(reltime(days=-1),3,3)
current_year = '20'+strmid(reltime(days=-1),7,2)

hours_arr = ['00','01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17','18', '19', '20','21', '22','23']
minutes_arr = ['00', '10', '20','30', '40', '50']
minutes_arr1 = ['00', '10', '20','30', '40', '50']
days_arr= [current_day,strcompress(indgen(31)+1)]
months_arr= [current_month,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
years_arr= [current_year,'2006', '2007', '2008']

start_time=widget_base(base, /row)
w_day = widget_droplist(start_time, title='Date:', value = days_arr, uvalue='w_day')
w_month = widget_droplist(start_time, value=months_arr, uvalue='w_month')
w_year = widget_droplist(start_time, value = years_arr, uvalue='w_year')
w_hours = widget_droplist(start_time, title='Start Time:', value = hours_arr, uvalue='w_hours')
w_minutes = widget_droplist(start_time, title= ':',value = minutes_arr, uvalue='w_minutes')
stop_time=widget_base(base, /row)
w_day1 = widget_droplist(stop_time, title='Date:', value = days_arr, uvalue='w_day1')
w_month1 = widget_droplist(stop_time, value=months_arr, uvalue='w_month1')
w_year1 = widget_droplist(stop_time, value = years_arr, uvalue='w_year1')
w_hours1 = widget_droplist(stop_time, title='Stop Time: ', value = hours_arr, uvalue='w_hours1')
w_minutes1 = widget_droplist(stop_time, title= ':',value = minutes_arr1, uvalue='w_minutes1')
display_space=widget_label(stop_time, value = '    ')

searchall=widget_base(base, /row)
w_search_all = widget_button(searchall, value = 'Search All', uvalue='searchall')
dismiss = widget_button(searchall, value = 'Dismiss', uvalue = 'dismiss')

widget_control, base, /realize
xmanager, 'sot_select',base

index_out = index
data_out = data
files_out=filnam(ss)

end
