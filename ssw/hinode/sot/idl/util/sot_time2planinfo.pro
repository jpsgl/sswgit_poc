function sot_time2planinfo, times,time1, debug=debug, match_slot=match_slot, $ 
   window=window
;
;+
;   Name: sot_time2planinfo
;
;   Purpose: return plan info (stucture vector) for input time/structure
;
;   Input Paramters:
;      times - input time (ssw compliant time, sot catalog or index...)
;
;   Output:
;      function returns structure(s) translation of corresponding *pln file 
; 
;   Keyword Parameter:
;      match_slot - slot number to match (default is all)
;      window - optional window check (+/1 days)
;
;   Calling Sequence:
;      IDL> plnstr=sot_time2planinfo(time)
;
;   Calling Example:
;      IDL> plnstr=sot_time2planinfo('22-feb-2007')
;      IDL> help,plnstr & help, plnstr,/str
;PLNSTR          STRUCT    = -> <Anonymous> Array[12] ; << one per ADS/slot number
;
;Structure <104cbd78>, 14 tags, length=304, data length=304, refs=1:
;   TIME_RANGE      STRING    '2007/02/22.10:49:00 to 2007/02/22.11:15:00'
;   VOEVENTFILE     STRING    'VOEvent-2007-02-22T10:49:00Z-SOT-Pln.xml'
;   OBSTITLE        STRING    'FAR0942'
;   OBS_NUM         STRING    '0'
;   JOP_ID          STRING    '0'
;   NOAA_NUM        STRING    '10942'
;   OBJECTS         STRING    'OBJECTS:'
;   SCI_OBJ         STRING    'Active region tracking'
;   TARGET          STRING    'Active Region 10942'
;   JOIN_SB         STRING    'ES'
;   SLOTNUMBER      STRING    '6'
;   DATE_OBS        STRING    '2007-02-22T10:49:00.000'
;   ENDTIME         STRING    '2007-02-22T11:15:00.000'
;   HEADER          STRING    Array[6]
;
;
;   History:
;      23-Feb-2007 - S.L.Freeland
;
;-
;
debug=keyword_set(debug)

case n_params() of 
   2: planfiles=sot_time2planfiles(times,time1,debug=debug)
   1: planfiles=sot_time2planfiles(times,debug=debug)
   else: begin 
      box_message,'Need time or time range..
      return,-1
   endcase
endcase

if debug then stop,'planfiles'
if planfiles(0) eq '' then begin
   box_message,'No planfiles availble in your time range...
   return,-1
endif
planstruct=sot_plan2struct(planfiles(0))

for i=1,n_elements(planfiles)-1 do begin 
   nextplan=sot_plan2struct(planfiles(i),debug=debug)
   planstruct=[temporary(planstruct),nextplan]
endfor


if n_elements(match_slot) gt 0 then begin 
   ss=where(float(planstruct.slotnumber) eq float(match_slot),scnt)
   if scnt gt 0 then begin 
      planstruct=planstruct(ss)
   endif else begin 
      box_message,'No match for slotnumber> ' + strtrim(match_slot,2)
      planstruct=-1
   endelse
endif

return,planstruct
end
