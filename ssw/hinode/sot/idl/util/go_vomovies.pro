set_plot,'z
cd,'$SSW/site/idl/util/fpp'
ssw_path,'$SSW_SOT/idl/fg',/prepend

tg=timegrid(reltime(days=-1),reltime(days=2),/day,out='ecs')
tg=anytim(tg,/date_only,/ecs)

modes=['FG*','SP*']
search='obs_type='+modes
for i=0,n_elements(tg)-2 do begin 
   for m=0,n_elements(modes)-1 do begin 
   delvarx,cat
   sot_cat,tg(i),tg(i+1),cat, search=search(m)
   if n_elements(cat) gt 10 then begin 
   uprog=fix(all_vals(cat.prog_no))
   np=n_elements(uprog)
   for pn=0,np-1 do begin      
      hinode_make_wwwmovies,tg(i),tg(i+1),/refresh,/voevent_movies,$
         prog_no=uprog(pn), min_frames=2, /make_voevent,/IQUV, /context, $
         fg_only=modes(m) eq 'FG*', sp_only=modes(m) eq 'SP*', /add_point, $
         v_only=modes(m) eq 'FG*'
   endfor
   endif else box_message,'Insufficient catalog entries for ' + tg(i)  
   endfor
endfor
end
