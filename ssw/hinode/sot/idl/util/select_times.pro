
pro select_times, ts_win, te_win, t_rep_dur=t_rep_dur, ts_interv_arr, te_interv_arr, $
  n_reps_req=n_reps_req, ts_offset=ts_offset, te_offset=te_offset, t_rep_offset=t_rep_offset, $
  n_reps_good=n_reps_good, ts_rep_arr=ts_rep_arr, te_rep_arr=te_rep_arr, $
  ts_interv_out=ts_interv_out, te_interv_out=te_interv_out, status=status

ts_win_sec = anytim(ts_win)
te_win_sec = anytim(te_win)

ts_interv_sec = anytim(ts_interv_arr)
te_interv_sec = anytim(te_interv_arr)
ts_interv_out = ts_interv_sec
te_interv_out = te_interv_sec

n_ts = n_elements(ts_interv_sec)
n_te = n_elements(te_interv_sec)

if min(ts_interv_sec) gt min(te_interv_sec) then te_interv_sec = te_interv_sec[1:*]
if min(ts_interv_out) gt min(te_interv_out) then te_interv_out = te_interv_out[1:*]

if max(te_interv_sec) lt max(ts_interv_sec) then ts_interv_sec = ts_interv_sec[0:n_ts-2]
if max(te_interv_out) lt max(ts_interv_out) then ts_interv_out = ts_interv_out[0:n_ts-2]

if exist(ts_offset) then ts_interv_sec = ts_interv_sec + ts_offset*60d0
if exist(te_offset) then te_interv_sec = te_interv_sec - te_offset*60d0
t_rep_dur_sec = t_rep_dur*60d0
if exist(t_rep_offset) then t_rep_offset_sec = t_rep_offset*60d0 else t_rep_offset_sec = 0d0

t_len_interv_arr = te_interv_sec - ts_interv_sec

n_interv = n_elements(t_len_interv_arr)
nreps_arr = fix(t_len_interv_arr / (t_rep_offset_sec + t_rep_dur_sec) )
ss_good = where(nreps_arr gt 0, n_good)

if n_good gt 0 then begin
  ts_interv_sec = ts_interv_sec[ss_good]
  te_interv_sec = te_interv_sec[ss_good]
  n_interv = n_good
  nreps_arr = nreps_arr[ss_good]
  for i=0, n_interv-1 do begin
    ts_offset0 = t_rep_offset_sec + findgen(nreps_arr[i])*(t_rep_offset_sec + t_rep_dur_sec)
    ts_rep0 = ts_interv_sec[i] + ts_offset0
    te_rep0 = ts_rep0 + t_rep_dur_sec
    if not exist(ts_rep_arr) then begin
      ts_rep_arr = ts_rep0
      te_rep_arr = te_rep0
    endif else begin
      ts_rep_arr = [ts_rep_arr, ts_rep0]
      te_rep_arr = [te_rep_arr, te_rep0]
    endelse
  endfor

  n_reps_tot = n_elements(ts_rep_arr)

  ss_good = where( (ts_rep_arr ge ts_win_sec) and $
                   (te_rep_arr le te_win_sec), n_reps_tot_good )
  ts_rep_arr = ts_rep_arr[ss_good]
  te_rep_arr = te_rep_arr[ss_good]

  if n_reps_tot_good ge n_reps_req then begin
    ts_rep_arr = ts_rep_arr[0:n_reps_req-1]
    te_rep_arr = te_rep_arr[0:n_reps_req-1]
    n_reps_good = n_reps_req
    status = 1
  endif else begin
    n_reps_good = n_reps_tot
    status = -1
  endelse
endif else begin
  ts_rep_arr = -1
  te_rep_arr = -1
  n_reps_good = 0
  status = -1
endelse

end





