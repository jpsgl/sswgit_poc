function hinode_planvoe2struct, planvoe, debug=debug

sse=where(1-file_exist(planvoe),mcnt)

if mcnt gt 0 then begin 
   box_message,'At least one input planning VOE is missing
   planvoe=planvoe(sse)
endif

pdata=rd_Tfiles(planvoe,filemap=filemap) ; VOE -> strarray

stemp={date_obs:'',date_end:'',telescope:'',instrument:'',Tohbans:'',$
   ChiefPlanner:'',ChiefObserver:'',obsId:'',obs_num:'',jop_id:'',$
   jop:'',join_sb:'',obstitle:'',sci_obj:'',sci_obs:'',NOAA_NUM:'',$
   TARGET:'',slotnumber:'',xcen:'',ycen:'',saa_start:'',saa_stop:'',$
   goal:'',purpose:'', description:''}

nret=max(filemap)+1
retval=replicate(stemp,nret)

tags=tag_names(stemp)
updata=strtrim(strupcase(pdata),2)
sstring=':'+tags+'>'

ssint=where(strpos(updata,'<CRD:TIMEINTERVAL>') ne -1,nint)
ssend=where(strpos(updata,'</CRD:TIMEINTERVAL>') ne -1,enint)

if get_logenv('check_int') ne '' then stop,'nint,enint'


debug=keyword_set(debug)
if debug then stop,'ssint

ssp=where(ssint ne ssend,scnt)

if scnt gt 0 then begin  
   box_message,'gluing...'

   for s=0,scnt-1 do begin 
      updata(ssint(ssp(s)))=arr2str(updata[ssint(ssp(s)):ssend(ssp(s))],' ')
      updata[ssint(ssp(s))+1:ssend(ssp(s))]=''
   endfor
   ssint=where(strpos(updata,'<CRD:TIMEINTERVAL>') ne -1,nint)
endif

if debug then stop,updata(ssint)
if nint ne nret then begin 
   if get_logenv('check_plan') ne '' then stop,'nint,nret'
   box_message,'Time Interval not 1:1 (!!)'
   return,''
endif

tint=strextract(strcompress(updata(ssint)),':TIMEINTERVAL>','<')
strtab2vect,str2cols(tint,' ',/una),dobs,dend
retval.date_obs=dobs
retval.date_end=dend
for i=0,n_tags(stemp)-1 do begin  ; tags which are 1:1 per xml tag
   ss=where(strpos(updata,sstring(i)) ne -1,sscnt)
   if sscnt eq nret then begin 
      retval.(i)=strextract(pdata(ss),'>','<')
   endif else begin

   endelse

endfor

; tags not 1:1
for i=0,nret-1 do begin 
  ssssa=where(i eq filemap and strpos(updata,'"SAAINTERVAL"') ne -1, saacnt)
  if saacnt gt 0 then begin 
     saat=strextract(updata(ssssa),'VALUE="','"')
     strtab2vect,str2cols(saat,' ',/un),saastart,saastop
     retval(i).saa_start=arr2str(saastart)
     retval(i).saa_stop=arr2str(saastop)
  endif
  ssd=where(i eq filemap and strpos(pdata,'<Description>') ne -1, dcnt)
  ssde=where(i eq filemap and strpos(pdata,'</Description>') ne -1, decnt)
  if dcnt gt 0 and decnt gt 0 then begin 
     desc=arr2str(strtrim(pdata(ssd:ssde),2))
     retval(i).description=strextract(desc,'>,',',<')
  endif 

endfor


return,retval
end
