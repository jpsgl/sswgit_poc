set_plot,'z
ssw_path,'$SSW_SOT/idl/fg',/prepend

t0='18:04 2-may-2007'
t1='20:20 2-may-2007
t0='23:25 2-may-2007'
t1='00:05 3-may-2007'
t0='01:30 9-may-2007'
t1='02:15 9-may-2007'
t0='05:00 12-may-2007'
t1='05:40 12-may-2007'
t0='13:25 12-may-2007'
t1='13:50 12-may-2007
t0='13:30 9-may-2007'
t1='14:30 9-may-2007'
t0='19:24 13-may-2007'
t1='20:25 13-may-2007'
t0='07:25 14-may-2007'
t1='07:55 14-may-2007'
t0='22:00 13-may-2007'
t1='23_00 13-may-2007
t0='04:20 15-may-2007'
t1='04:50 15-may-2007'
sot=1
xrt=1
if sot then begin 
 hinode_make_wwwmovies,t0,t1,/refresh,/latest_events,$
          min_frames=3, /make_voevent,/IQUV, /context, $
          /add_point, $
        /v_only, max_frames=60,max_xy=768, labstyle='/UC', $
        table=3, gamma=.7
endif

if xrt then begin 
hinode_make_wwwmovies,t0,t1,/refresh,/latest_event,$
         min_frames=2, /make_voevent,/xrt, /context,/add_point, $ 
         max_frames=60, max_xy=512
endif

end
