function hinode_credits, sot=sot, xrt=xrt, eis=eis, mission=mission, $
   url_link=url_link , html=html, no_mission=no_mission, $
   short=short, long=long
;
;+
;   Name: hinode_credits
;
;   Purpose: return web/pub credits for Hinode instruments, opt. as html
;
;   Input Parameters:
;      NONE:
;
;   Output:
;      function returns string arry containing implied credits
;
;   Calling Sequence:
;      IDL> credarr=hinode_credits([,/sot] [,/xrt] [,eis] [,/mission] [,/html])
;
;   Keyword Parameters:
;      sot, xrt, eis - instrument credits
;      mission - Hinode-wide credit
;      html - if set, include html wrapper (of my choosing...)
;      url_link - if set, include web refernces as links (implies /html)
;
;   History:
;      10-May-2007 - S.L.Freeland - for auto-crediting dynamic www pages   
;-

xrt=keyword_set(xrt)
sot=keyword_set(sot)
eis=keyword_set(eis)
mission=keyword_set(mission)
long=keyword_set(long)
urls=keyword_set(url_link)
html=keyword_set(html) or urls

short=1-long ; default is short credit(s)
if long then box_message,'Sorry, only short credits for now...'
short=1 ; temporary, I expect 

miss_cred='Hinode is a Japanese mission developed and launched by ' + $ 
     'ISAS/JAXA, with NAOJ as domestic partner and NASA and STFC (UK)' + $
     ' as international partners. It is operated by these agencies ' + $
     'in co-operation with ESA and NSC (Norway).'

xrt_scred='Hinode XRT, Courtesy SAO, NASA, JAXA, and NAOJ'
sot_scred='Hinode SOT, Courtesy NAOJ, LMATC, JAXA, NASA, MELCO, and HAO'
eis_scred='Hinode EIS, Courtesy MSSL, RAL, NASA, NRL, Univ. of Oslo, JAXA, and NAOJ'

sot_lcred=''
xrt_lcred=''
eis_lcred='The EIS instrument was designed and developed by an international collaboration of Mullard Space Science Laboratory (MSSL), The University of Birmingham, Ratherford Appleton Laboratory (RAL), NASA MSFC/GSFC, NRL Hulbert Center for Space Research, University of Oslo, JAXA, and NAOJ.'

xrt_url=(['','http://xrt.cfa.harvard.edu/'])(urls and xrt)
sot_url=(['','http://sot.lmsal.com/'])(urls and sot)
eis_url=(['','http://solar-b.nao.ac.jp/eis_e/'])(urls and eis)

xrtc=(['',xrt_scred + str2html(xrt_url,link_text='  (XRT@SAO) ')])(xrt)
sotc=(['',sot_scred + str2html(sot_url,link_text='  (SOT@LMATC)')])(sot)
eisc=(['',eis_scred + str2html(eis_url,link_text='  (EIS@NRL)')])(eis)
missc=(['',miss_cred])(mission)

allcreds=[xrtc,sotc,eisc,missc]
allcreds=strarrcompress(allcreds)
if total(strlen(allcreds)) eq 0 then allcreds=miss_cred 

if html then $
   allcreds=['<p>','<em>',allcreds,'</em><p>']

return, allcreds
end
