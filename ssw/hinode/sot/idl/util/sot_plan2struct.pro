function sot_plan2struct,plan, debug=debug
;+
;   Name: sot_plan2struct
;
;   Purpose: SOT *pln files -> idl structure vector
;
;   Input Paramters:
;      plan - plan file (local or url) or contents of plan file
;
;   Output
;     function returns structure version
;
;   History:
;      23-feb-2007 - S.L.Freeland
;      27-feb-2007 - S.L.Freeland - avoid tag order problem w/creat_struct??
;-
debug=keyword_set(debug)
if n_elements(plan) eq 0 then begin
   box_message,'Requires plan file, url, or contents
   return,''
endif

if n_elements(plan) eq 1 then begin 
   if strpos(plan,'http://') ne -1 then sock_list,plan,plancont else begin
      if file_exist(plan) then plancont=rd_Tfile(plan) else begin 
         box_message,'Cannot find plan file...'
         return,''
      endelse
   endelse
endif else plancont= plan ; user supplied contents

absss=where(strpos(plancont,'ABS:') ne -1,abscnt)  ; slots
dupit=abscnt eq 1
if dupit then begin ; special/fake
   plancont=[plancont,plancont(absss(0):*)]
   absss=where(strpos(plancont,'ABS:') ne -1,abscnt)  ; slots
endif

dtabss=deriv_arr(absss)
avdt=all_vals(dtabss)
if n_elements(avdt) gt 1  then begin 
   box_message,'Not all slots same length??'
   stop
endif

; create structure based on *pln contents (auto extending if new fields added) 

refplan=reform(plancont(absss(0):*),abscnt,avdt) ; 1D -> 2D 
template=strtrim(reform(refplan(0,1:*)),2)
template=strtrim(reform(plancont(absss(0)+1:absss(0)+(avdt-1))),2)
ntags=n_elements(template) ; #fields per slot
cols=str2cols(template,': ',/unaligned,/trim) ; seperate name/value pairs
strtab2vect,cols,tagnames,tagvals  ; 2D -> 1D
;create_struct,slotstr,'slotstr',['time_range',tagnames],replicate('A',ntags+1)
; create_struct did not seem to preserve tag order?? 
slotstr={time_range:''}  ;init with ADS/Timerange
for i=0,ntags-1 do slotstr=add_tag(slotstr,'',tagnames(i))

allslots=replicate(slotstr,abscnt)
slotdata=strtrim(ssw_strsplit(plancont(absss(0):*),': ',/tail),2)
reads,slotdata,allslots ; populate structure vector
if debug then stop,'slotdata,allslots' 

; add DATE_OBS & ENDTIME tags
tranges=allslots.time_range
endtime=ssw_strsplit(strlowcase(tranges),'to', head=date_obs,/tail)
slotdata=add_tag(allslots,anytim(str_replace(date_obs,'.',' '),/ccsds), $
      'date_obs') 
slotdata=add_tag(slotdata,anytim(str_replace(endtime,'.',' '),/ccsds), $
      'endtime')

; add HEADER
header=strarrcompress(plancont(0:absss(0)-1))
slotdata=add_tag(slotdata,header,'header')
if dupit then slotdata=slotdata(0) ; only one slot which was duplicated

return,slotdata
end
