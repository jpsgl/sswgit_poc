
function get_voevent_blorch, item, who=who

if keyword_set(who) then begin
	buff = '    <Who>'
	buff = [buff, '        <PublisherID>http://sot.lmsal.com/mk_voe_obs.pro</PublisherID>']
	buff = [buff, '        <Date>'+ item.creation_time +'</Date>']
	buff = [buff, '']
	buff = [buff, '        <Contact>']
	buff = [buff, '            <Name>Dr. Neal Hurlburt</Name>']
	buff = [buff, '            <Institution>Lockheed Martin Solar and Astrophysics Laboratory ' + $
				   '(LMSAL)</Institution>']
	buff = [buff, '            <Communication>']
	buff = [buff, '                <Uri>http://www.lmsal.com</Uri>']
	buff = [buff, '                <AddressLine>Lockheed Martin Advanced Tech Center, ' + $
				       '3251 Hanover Rd, O/ADBS, B/252, Palo Alto, CA 94304</AddressLine>']
	buff = [buff, '                <Telephone>+1-650-424-2382</Telephone>']
	buff = [buff, '                <Email>hurlburt@lmsal.com</Email>']
	buff = [buff, '            </Communication>']
	buff = [buff, '        </Contact>']
	buff = [buff, '        <lmsal:Telescope>Hinode</lmsal:Telescope>']
	buff = [buff, '        <lmsal:Instrument>' + strupcase(item.instrume) + '</lmsal:Instrument>']
	buff = [buff, '    </Who>']
endif

return, buff

end

