function ssw_index2carr, index, add=add, fast=fast, round=round, $
   offset=offset, dc=dc
;
;+
;   Name: ssw_index2carr
;
;   Purpose: ssw standards (xcen,ycen..) -> carrington, optionally -> index
;
;   Input Paramters:
;      index - ssw standard time/pointing assumed
;
;   Keyword Parameters:
;      
nout=n_elements(index)
case 1 of 
   required_tags(index,'date_obs'): utc=anytim(index.date_obs,/utc_int)
   required_tags(index,'time,mjd'): utc=anytim(index,/utc_int)
   required_tags(index,'day,time'): utc=anytim(index,/utc_int)
   required_tags(index,'DATE_D$OBS'): utc=anytim(index.date_d$dobs,/utc_int)
   else: begin 
      box_message,'Expect ssw times...'
      return,-1
   endcase
endcase

wcs=fitshead2wcs(index(0))
wcs=replicate(wcs,nout)
carr=fltarr(nout)
latt=fltarr(nout)
lonn=fltarr(nout)

if n_elements(offset) gt 0 then off=offset else off=-1
if keyword_set(fast) then begin 
   times=anytim(utc)
   medt=median([times])
   hel=arcmin2hel(index.xcen/60., index.ycen/60, date=medt) ; -> common time
   strtab2vect,hel,latt,lonn
   carr=tim2carr(utc,offset=lonn, dc=dc)
endif else begin 
   for i=0,nout-1 do begin 
   ;wcs(i)=fitshead2wcs(index(i))
      hel=arcmin2hel(index(i).xcen/60.,index(i).ycen/60.,date=utc(i))
      lonn(i)=hel(1)
      latt(i)=hel(0) 
      carr(i)=tim2carr(utc(i),offset=([lonn(i),off])(n_elements(offset) gt 0))
   endfor
endelse
retval=carr
if keyword_set(round) then begin 
   lonn=round(lonn)
   latt=round(latt)
   carr=round(carr)
endif
if keyword_set(add) then begin 
   if required_tags(index,'hel_lon,hel_lat,car_lon') then begin 
      index.hel_lon=lonn
      index.hel_lat=latt
      index.car_lon=carr
   endif else begin 
      index=add_tag(index,lonn,'hel_lon')
      index=add_tag(index,latt, 'hel_lat')
      index=add_tag(index,carr,'car_lon')
   endelse
endif
return,retval
end

