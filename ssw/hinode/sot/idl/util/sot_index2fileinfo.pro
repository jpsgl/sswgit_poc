pro sot_index2fileinfo,index,fprefix,obsdir,fnames,fpaths, $
   millisec=millisec, file_type=file_type, notype=notype, parent=parent
;
;+
;   Name: sot_index2fileinfo
;
;   Purpose: return implied file prefix, subdirectory and tbd for input index
;
;   Input Paramters:
;      index - sot or xrt catalog or index records
;
;   Output Parameters: 
;      fprefix - implied file prefix
;      obsdir - implied sudbdirectory name  
;      fnames - filename sans prefix: yyymmdd_hhmmss.m[mm][.type]
;      fpaths - full path relative to <top>yyyy/mmm/dd/<obsdir>/Hhhhh/ 
;            
;
;   Keyword Parameters:
;      millisec - if set, file granulaity -> millisecs (def=decisecs per?)
;      file_type - explicit file type - def =.fits
;      notype - if set, don't append a file type  
;      parent - if supplied, prepend this to 'fpath' output
;
;   History:
;      4-Jan-2006 - S.L.Freeland - realize sot/xrt reformatter logic->ssw apps
;                                  (initially for write_sot.pro)
;                                  Some logic from sot_cat2files.pro
;     07-Feb-2014 - GLS - Added obsdir definition for case of
;                         OBS_TYPE 'FG shutterless I and V'
;
;-
millisec=keyword_set(millisec)

if not required_tags(index,'naxis,obs_type') then begin 
   box_message,'Require SOT/XRT catalog or index structures
   return
endif

; index->obsdir mapping 
otypes=['FG (simple)','CT live frame','FG focus scan','SP IQUV 4D array',$
        'CT reference frame','FG shuttered Stokes','FG shuttered I and V',$
        'FG shutterless I and V','FG shutterless Stokes']

odirs=str2arr('FG,CL,FGFOCUS,SP4D,CR,FGIQUV,FGIV,FGSIV,FGSIQUV')

; initialize output variables...
nout=n_elements(index)
fprefix=strarr(nout)
obsdir=fprefix
if keyword_set(parent) then fpath=replicate(parent(0),nout) else $
   fpath=fprefix

if required_tags(index,'fprefix,obsdir') then begin ; catalog structures
   obsdir=index.obsdir
   fprefix=index.fprefix
endif else begin ; index structures
   for i=0,n_elements(otypes)-1 do begin 
      ss=where(strmid(index.obs_type,0,22) eq otypes(i),otcnt)
      if otcnt gt 0 then begin
         obsdir(ss)=odirs(i) 
      endif      
   endfor
   fprefix=obsdir
endelse
ss=where(obsdir eq 'FGFOCUS',foccnt) ; special casess
if foccnt gt 0 then fprefix(ss)='FG'

millis=ssw_strsplit(anytim(index.date_obs,/ecs),'.',/tail)
fnames=time2file(index.date_obs,/sec) + '.' + strmid(millis,0,([1,3])(millisec))
 
case 1 of 
   keyword_set(notype): ftype=''
   data_chk(file_type,/string): ftype='.'+str_replace(file_type,'.','')
   else: ftype='.fits'
endcase
fnames=fnames+ftype

fecs=anytim(index.date_obs,/ecs)
pdelim=get_delim()
fpaths=strmids(fecs,0,10)+pdelim+obsdir+pdelim+'H'+strmid(fecs,11,2) + '00' 
if keyword_set(parent) then fpaths=parent+pdelim+fpaths


return 
end 
;   
