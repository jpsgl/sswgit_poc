function xrt_time2flareflag, itimes, flare_only=flare_only, response_only=response_only, $
   url_flareflag=url_flareflag, ssw_dist=ssw_dist, refresh=refresh, ff0=ff0, ff1=ff1
;
;+
;   Name: xrt_time2flareflag
;
;   Purpose: check input times against XRT flareflag list;
;
;   Input Parameters:
;      itimes - time(s) to check (any ssw time, index, or catalog vector)
;
;   Output:
;      function returns structure vector -or- boolean if /FLARE_ONLY or /RESPONSE_ONLY were set
;
;   Keyword Parameters:
;      url_flareflag - optional URL containing XRT time ranges
;                      (default=http://xrt.cfa.harvard.edu/missionops/flare_trigger_list/xrt_flare_flags.txt)
;      flare_only - if set, return boolean "flare flag set"
;      response_only - if set, return boolean "flare flag response" (if true, implies flare flag set -and- response)
;      add_tag - if input are structures and this is set, append tags to input
;      ff0 (optional output) - returns flare flag start times (mission)
;      ff1 (optional output) - returns flare flag stop times (mission)
;
;   History:
;      13-sep-2010 - S.L.Freeland
;      30-jan-2013 - S.L.Freeland - sock_list2 -> sock_list
;
;   Comments:
;      rev 1. uses URL via socket  
;      if/when this list ("dbase") is included in SSW_XRT/... distribution, would provide a local option
;-

common xrt_time2flareflag, ffstart, ffend, ffresp ; cache FF time ranges and FF-response

refresh=keyword_set(refresh) or n_elements(ffstart) eq 0 
ssw_dist=keyword_set(ssw_dist)

if refresh then begin 
   fname='xrt_flare_flags.txt'
   ssw=concat_dir('$SSW_XRT','idl/util/'+fname)
   if n_elements(url_flareflag) eq 0 then url_flareflag=$
      'http://xrt.cfa.harvard.edu/missionops/flare_trigger_list/xrt_flare_flags.txt'
   case 1 of 
      ssw_dist and file_exist(ssw): begin
         box_message,'using ssw... 
         dbase=rd_tfile(ssw)
      endcase
      else: sock_list,url_flareflag,dbase
   endcase
   ssff=where(strpos(dbase,'#') eq -1,ffcnt)
   cols=str2cols(dbase(ssff),' ',/trim)  ; 1D -> 2D table
   strtab2vect,cols,ffstart,ffend,ffresp ; 2D -> 3x1D vectors
endif 

times=itimes
if data_chk(times,/struct) then times=gt_tagval(times,/date_obs) ; catalog or index input

nt=n_elements(times)
ffepoch=ssw_time2epoch(times,ffstart,ffend)  ; ssw-gen times -> epoch# 


ffset=fix(ffepoch ne -1) 
ssff=where(ffset,ffcnt)
resp=intarr(nt)           ; default is false (N/A or 'NO')
if ffcnt gt 0 then $
   resp(ssff)=strupcase(ffresp(ffepoch(ssff))) eq 'YES'

if n_elements(ffset) eq 1 then ffset=ffset(0)
if n_elements(resp) eq 1 then resp=resp(0)

; now set user defined output preference
case 1 of
   keyword_set(flare_only): retval=ffset    ; Boolean FF=TRUE
   keyword_set(response_only): retval=resp  ; Boolean FF=TRUE -and- Response=YES
   else: begin
      retval={flflg:ffset, flflgrsp:resp}
   endcase
endcase
ff0=ffstart & ff1=ffend ; output ->  keywords

return,retval
end
