
pro plot_sot_errors, sttim, entim, dec_num=dec_num, $
  sort_style=sort_style, $
  no_rotate=no_rotate, debug=debug, no_mess=no_mess, only_mess=only_mess, cat_err=cat_err, $
  no_summary=no_summary, do_list=do_list, do_hist=do_hist, use_dec=use_dec, use_hex=use_hex, $
  do_log=do_log, do_tplot=do_tplot, initialize=initialize, _extra=_extra

;+
; NAME: PLOT_SOT_ERRORS
;
; EXAMPLE CALLS:
;   Simple summary of errors over specified period:
;     IDL> plot_sot_errors, '22-oct-2006','06-apr-2011'
;   Full listing of errors over specified interval:
;     IDL> plot_sot_errors, '01-feb-2011','06-apr-2011', /do_list
;   Histogram of uniq errros:
;     IDL> plot_sot_errors, '01-feb-2011','06-apr-2011', /do_hist
;   Exclude 'message' errors (only 'true' errors):
;     IDL> plot_sot_errors, '01-feb-2011','06-apr-2011', /no_mess
;   Plot cumulative errors against time for specified interval:
;     IDL> plot_sot_errors, '01-feb-2011','06-apr-2011', /do_tplot
;   Specify a particular decimal error number:
;     IDL> plot_sot_errors, '01-feb-2011','06-apr-2011', /do_tplot, dec_num=312l
;-

if not exist(sttim) then sttim = anytim('22-oct-2006', /ccsds)
if not exist(entim) then entim = anytim(ut_time(!stime), /ccsds)
if not exist(topdir) then topdir = '/net/castor/Users/slater/data/sot_genxcat_minime_sot_errors'

if not keyword_set(sort_style) then sort_style = 'descend'

read_genxcat, sttim, entim, cat_err, topdir=topdir, count=count, status=status, $
  loud=keyword_set(verbose), _extra=_extra

t_err = anytim(cat_err, /ccsds)
err_name = cat_err.err_name
err_num_hex = strmid(cat_err.err_num_hex,4,4)
err_num_dec = strtrim(cat_err.err_num_dec,2)
err_mess = cat_err.err_mess
n_err = n_elements(t_err)

if status eq 0 then begin
  print, ''
  print, ''
  print, strupcase(' No errors or messages found in specified time interval.')
  print, strupcase(' Last error info:')
  tab_hdr = ['DATE/TIME', 'ERROR NAME', 'HEX CODE', 'DEC CODE', 'MESSAGE']
  print, ''
  print, tab_hdr, format='$(a-20, 2x, a-25, 2x, a-8, 2x, a-8, 2x, a-40)
  print, ''
  print, t_err[0], err_name[0], err_num_hex[0], err_num_dec[0], err_mess[0], $
         format = '$(a-20, 2x, a-25, 2x, a8, 2x, a8, 2x, a-40)'
  print, ''
  return
endif

if exist(dec_num) then begin
  ss_match = where(err_num_dec eq dec_num, n_match)
  if n_match eq 0 then begin
    print, ''
    print, ''
    print, strupcase(' No errors of specified numbers found in specified time interval.')
    print, strupcase(' Last error or message info:')
    tab_hdr = ['DATE/TIME', 'ERROR NAME', 'HEX CODE', 'DEC CODE', 'MESSAGE']
    print, ''
    print, tab_hdr, format='$(a-20, 2x, a-25, 2x, a-8, 2x, a-8, 2x, a-40)
    print, ''
    print, t_err[0], err_name[0], err_num_hex[0], err_num_dec[0], err_mess[0], $
         format = '$(a-20, 2x, a-25, 2x, a8, 2x, a8, 2x, a-40)'
    print, ''
    return
  endif else begin
    cat_err = cat_err[ss_match]
    t_err = t_err[ss_match]
    err_name = err_name[ss_match]
    err_num_hex = err_num_hex[ss_match]
    err_num_dec = err_num_dec[ss_match]
    err_mess = err_mess[ss_match]
    n_err = n_match
  endelse
endif

if keyword_set(no_mess) then begin
  ss_err_only = where(err_num_dec lt 32768l, n_err_only)
  if n_err_only eq 0 then begin
    print, ''
    print, ''
    print, strupcase(' No true errors found in specified time interval.')
    print, strupcase(' Last error or message info:')
    tab_hdr = ['DATE/TIME', 'ERROR NAME', 'HEX CODE', 'DEC CODE', 'MESSAGE']
    print, ''
    print, tab_hdr, format='$(a-20, 2x, a-25, 2x, a-8, 2x, a-8, 2x, a-40)
    print, ''
    print, t_err[0], err_name[0], err_num_hex[0], err_num_dec[0], err_mess[0], $
         format = '$(a-20, 2x, a-25, 2x, a8, 2x, a8, 2x, a-40)'
    print, ''
    return
  endif else begin
    cat_err = cat_err[ss_err_only]
    t_err = t_err[ss_err_only]
    err_name = err_name[ss_err_only]
    err_num_hex = err_num_hex[ss_err_only]
    err_num_dec = err_num_dec[ss_err_only]
    err_mess = err_mess[ss_err_only]
    n_err = n_err_only
  endelse
endif

err_names_uniq = all_vals(cat_err.err_name)
n_uniq = n_elements(err_names_uniq)

err_table_struct = get_err_info()
dec_vals_uniq = strarr(n_uniq)
hex_vals_uniq = strarr(n_uniq)
mess_vals_uniq = strarr(n_uniq)
for i=0,n_uniq-1 do begin
  ss_match_err_tab =  where(err_table_struct.err_name eq err_names_uniq[i], n_match_err_tab)
  dec_vals_uniq[i] = err_table_struct[ss_match_err_tab].err_num_dec
  hex_vals_uniq[i] = err_table_struct[ss_match_err_tab].err_num_hex
  mess_vals_uniq[i] = err_table_struct[ss_match_err_tab].err_mess
endfor

if keyword_set(no_mess) then begin
  ss_err_only = where(long(dec_vals_uniq) lt 32768l, n_err_only)
  if n_err_only gt 0 then begin
    err_names_uniq = err_names_uniq[ss_err_only]
    dec_vals_uniq = dec_vals_uniq[ss_err_only]
    hex_vals_uniq = hex_vals_uniq[ss_err_only]
    mess_vals_uniq = mess_vals_uniq[ss_err_only]
    n_uniq = n_err_only
  endif else begin
    n_uniq = 0
  endelse
endif
 
err_hist_arr = lonarr(n_uniq)

for i=0,n_uniq-1 do begin
  ss_match_err0 = where(cat_err.err_name eq err_names_uniq[i], n_match_err0)
  err_hist_arr[i] = n_match_err0
endfor

if sort_style eq 'descend' then begin
  ss_sort = reverse(sort(err_hist_arr))
  err_hist_arr = err_hist_arr[ss_sort]
  err_names_uniq = err_names_uniq[ss_sort]
  dec_vals_uniq = dec_vals_uniq[ss_sort]
  hex_vals_uniq = hex_vals_uniq[ss_sort]
  mess_vals_uniq = mess_vals_uniq[ss_sort]
endif

if not keyword_set(no_summary) then begin
  tab_hdr = ['ERROR NAME', 'HEX CODE', 'DEC CODE', 'MESSAGE', 'OCCURRENCES']
  print, ''
  print, tab_hdr, format='$(a-25, 2x, a-8, 2x, a-8, 2x, a-40, 2x, a-12)'
  print, ''
  for i=0, n_uniq-1 do $
    print, err_names_uniq[i], hex_vals_uniq[i], strtrim(dec_vals_uniq[i],2), mess_vals_uniq[i], $
           strtrim(err_hist_arr[i],2), $
           format = '$(a-25, 2x, a8, 2x, a8, 2x, a-40, 2x, a6)'
  print, ''
endif    

if keyword_set(do_list) then begin
  tab_hdr = ['DATE/TIME', 'ERROR NAME', 'HEX CODE', 'DEC CODE', 'MESSAGE']
  print, ''
  print, tab_hdr, format='$(a-20, 2x, a-25, 2x, a-8, 2x, a-8, 2x, a-40)
  print, ''
  for i=0,n_err-1 do $
    print, t_err[i], err_name[i], err_num_hex[i], err_num_dec[i], err_mess[i], $
           format = '$(a-20, 2x, a-25, 2x, a8, 2x, a8, 2x, a-40)'
  print, ''
endif

if keyword_set(do_hist) then begin
  max_err_tot = max(err_hist_arr)
  yrange = [0, 2.0*max_err_tot]
  yrange_io = [0.1, 1e2*max_err_tot]

  if keyword_set(use_hex) then annotation = hex_vals_uniq else annotation = err_names_uniq

  wdef, 0, 1024,512

  if keyword_set(do_log) then begin
    plot_io, ERR_HIST_ARR, psym=10, yrange=yrange_io, ystyle=1, xrange=[-1,n_uniq], xstyle=1, $
      ycharsize=1.5, xtickname=strarr(60)+' ', thick=2 ; , yticklen=1
    for i=0,n_uniq-1 do xyouts, i, err_hist_arr[i]*2, annotation[i], orientation=90, $
      _extra=_extra
  endif else begin
    plot, ERR_HIST_ARR, psym=10, yrange=yrange, ystyle=1, xrange=[-1,n_uniq], xstyle=1, $
      ycharsize=1.5, xtickname=strarr(60)+' ', thick=2 ; , yticklen=1
    for i=0,n_uniq-1 do xyouts, i, err_hist_arr[i]+.1*max_err_tot, annotation[i], orientation=90, $
      _extra=_extra
  endelse

  if not keyword_set(no_rotate) then begin
    buff = tvrd()
    wdef, 0, 512, 1024
    tv,rotate(buff,3)
  endif

endif

if keyword_set(do_tplot) then begin
  ss_err = lindgen(n_err)
  cum_errors = ss_err
  wdef, 0, 1024,512
  utplot, t_err, ss_err
endif

if keyword_set(debug) then stop, ' Stopping on request.'

end
