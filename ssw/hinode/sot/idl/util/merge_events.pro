
pro merge_events, date, ts_evt=ts_evt, te_evt=te_evt, ts_saa=ts_saa, te_saa=te_saa, $
  t_op_start=t_op_start, t_op_end=t_op_end, t0_evt=t0_evt, t1_evt=t1_evt, $
  do_plot=do_plot, ts_plot=ts_plot, te_plot=te_plot, verbose=verbose, qstop=qstop

if not exist(t_buff_sec) then t_margin_sec = 7200d0

get_obev, date, ts_saa=ts_saa, te_saa=te_saa, ts_ngt=ts_xtw, te_ngt=te_xtw, ts_xtw=ts_ngt, te_xtw=te_ngt, $
  t_op_start=t_op_start, t_op_end=t_op_end, t0_evt=t0_evt, t1_evt=t1_evt

if ( (ts_saa[0] eq 0) and (te_saa[0] eq 0) and $
     (ts_xtw[0] eq 0) and (te_xtw[0] eq 0) and $
     (ts_ngt[0] eq 0) and (te_ngt[0] eq 0) ) then begin
  ts_evt = 0
  te_evt = 0
  if keyword_set(verbose) then $
    print, ' No evnts of any find found for date.  Returning empty-handed.'
  return
endif

if ( (ts_saa[0] ne 0) and (ts_saa[0] ne -1) ) then begin
  ts_saa_sec = anytim(ts_saa)
  t_buff_sec = anytim(ts_saa)
endif
if ( (te_saa[0] ne 0) and (te_saa[0] ne -1) ) then begin
  te_saa_sec = anytim(te_saa)
  if not exist(t_buff_sec) then t_buff_sec = anytim(te_saa) else $
    t_buff_sec = [t_buff_sec, anytim(te_saa)]
endif

if ( (ts_xtw[0] ne 0) and (ts_xtw[0] ne -1) ) then begin
  ts_xtw_sec = anytim(ts_xtw)
  if not exist(t_buff_sec) then t_buff_sec = anytim(ts_xtw) else $
    t_buff_sec = [t_buff_sec, anytim(ts_xtw)]
endif
if ( (te_xtw[0] ne 0) and (te_xtw[0] ne -1) ) then begin
  te_xtw_sec = anytim(te_xtw)
  if not exist(t_buff_sec) then t_buff_sec = anytim(te_xtw) else $
    t_buff_sec = [t_buff_sec, anytim(te_xtw)]
endif

if ( (ts_ngt[0] ne 0) and (ts_ngt[0] ne -1) ) then begin
  ts_ngt_sec = anytim(ts_ngt)
  if not exist(t_buff_sec) then t_buff_sec = anytim(ts_ngt) else $
    t_buff_sec = [t_buff_sec, anytim(ts_ngt)]
endif
if ( (te_ngt[0] ne 0) and (te_ngt[0] ne -1) ) then begin
  te_ngt_sec = anytim(te_ngt)
  if not exist(t_buff_sec) then t_buff_sec = anytim(te_ngt) else $
    t_buff_sec = [t_buff_sec, anytim(te_ngt)]
endif

t_min_sec = min(t_buff_sec) - t_margin_sec
t_max_sec = max(t_buff_sec) + t_margin_sec
t_min = anytim(t_min_sec, /ccsds)
t_max = anytim(t_max_sec, /ccsds)

sec_arr = t_min_sec + lindgen(t_max_sec-t_min_sec+1)
n_sec = n_elements(sec_arr)
mask_arr = lonarr(n_sec) + 0l

; ---------------

if ( exist(ts_saa_sec) and exist(te_saa_sec) ) then begin
  evt_nam = 'SAA'
  ts_arr = ts_saa_sec
  te_arr = te_saa_sec

  n_ts = n_elements(ts_arr)
  n_te = n_elements(te_arr)
  if n_ts gt n_te then $
    print, evt_nam + ' starts greater than ends on ' + date
  if n_ts lt n_te then $
    print, evt_nam + ' starts less than ends on ' + date

  if ts_arr[0] gt te_arr[0] then begin
    ts_arr = [t_min_sec, ts_arr]
    n_ts = n_ts + 1
    print, evt_nam + ' first start after first end on ' + date
  endif

  if ts_arr[n_ts-1] gt te_arr[n_te-1] then begin
    te_arr = [te_arr, t_max_sec]
    n_te = n_te + 1
    print, evt_nam + ' last start after last end on ' + date
  endif

  if n_ts ne n_te then $
    print, ' Total adjusted starts not equal to total adjusted ends.  Something is frigged.  Stopping.'

  for i=0, n_ts-1 do begin
    ss_inside = where( ( (sec_arr ge ts_arr[i]) and (sec_arr le te_arr[i]) ), n_inside)
    if n_inside gt 0 then mask_arr[ss_inside] = 1l
  endfor

  ts_saa_new_sec = ts_arr
  ts_saa_new = anytim(ts_arr, /ccsds)
  te_saa_new_sec = te_arr
  te_saa_new = anytim(te_arr, /ccsds)

endif

if ( exist(ts_xtw_sec) and exist(te_xtw_sec) ) then begin
  evt_nam = 'XTW'
  ts_arr = ts_xtw_sec
  te_arr = te_xtw_sec

  n_ts = n_elements(ts_arr)
  n_te = n_elements(te_arr)
  if n_ts gt n_te then $
    print, evt_nam + ' starts greater than ends on ' + date
  if n_ts lt n_te then $
    print, evt_nam + ' starts less than ends on ' + date

  if ts_arr[0] gt te_arr[0] then begin
    ts_arr = [t_min_sec, ts_arr]
    n_ts = n_ts + 1
    print, evt_nam + ' first start after first end on ' + date
  endif

  if ts_arr[n_ts-1] gt te_arr[n_te-1] then begin
    te_arr = [te_arr, t_max_sec]
    n_te = n_te + 1
    print, evt_nam + ' last start after last end on ' + date
  endif

  if n_ts ne n_te then $
    print, ' Total adjusted starts not equal to total adjusted ends.  Something is frigged.  Stopping.'

  for i=0, n_ts-1 do begin
    ss_inside = where( ( (sec_arr ge ts_arr[i]) and (sec_arr le te_arr[i]) ), n_inside)
    if n_inside gt 0 then mask_arr[ss_inside] = 1l
  endfor

  ts_xtw_new_sec = ts_arr
  ts_xtw_new = anytim(ts_arr, /ccsds)
  te_xtw_new_sec = te_arr
  te_xtw_new = anytim(te_arr, /ccsds)

endif

if ( exist(ts_ngt_sec) and exist(te_ngt_sec) ) then begin
  evt_nam = 'NGT'
  ts_arr = ts_ngt_sec
  te_arr = te_ngt_sec

  n_ts = n_elements(ts_arr)
  n_te = n_elements(te_arr)
  if n_ts gt n_te then $
    print, evt_nam + ' starts greater than ends on ' + date
  if n_ts lt n_te then $
    print, evt_nam + ' starts less than ends on ' + date

  if ts_arr[0] gt te_arr[0] then begin
    ts_arr = [t_min_sec, ts_arr]
    n_ts = n_ts + 1
    print, evt_nam + ' first start after first end on ' + date
  endif

  if ts_arr[n_ts-1] gt te_arr[n_te-1] then begin
    te_arr = [te_arr, t_max_sec]
    n_te = n_te + 1
    print, evt_nam + ' last start after last end on ' + date
  endif

  if n_ts ne n_te then $
    print, ' Total adjusted starts not equal to total adjusted ends.  Something is frigged.  Stopping.'

  for i=0, n_ts-1 do begin
    ss_inside = where( ( (sec_arr ge ts_arr[i]) and (sec_arr le te_arr[i]) ), n_inside)
    if n_inside gt 0 then mask_arr[ss_inside] = 1l
  endfor

  ts_ngt_new_sec = ts_arr
  ts_ngt_new = anytim(ts_arr, /ccsds)
  te_ngt_new_sec = te_arr
  te_ngt_new = anytim(te_arr, /ccsds)

endif

diff_arr = mask_arr[1:*] - mask_arr
ss_st_evt = where(diff_arr eq  1, n_st_evt)
if mask_arr[0] eq 1 then ss_st_evt = [0, ss_st_evt]

ss_en_evt = where(diff_arr eq -1, n_en_evt)
if mask_arr[n_sec-1] eq 1 then ss_en_evt = [ss_en_evt, n_sec-1]

ts_evt_sec = sec_arr[ss_st_evt]
te_evt_sec = sec_arr[ss_en_evt]

ts_evt = anytim(ts_evt_sec, /ccsds)
te_evt = anytim(te_evt_sec, /ccsds)

if keyword_set(do_plot) then begin
  if ( (exist(ts_plot)) and (exist(te_plot)) ) then $
    timerange = [ts_plot, te_plot] else $
    timerange = [t_min, t_max]
  yrange=[0,1]

; Fill in SAAs:

  wdef,win0,1024,256
  utplot, sec_arr, lonarr(n_sec), timerange=timerange, yrange=yrange, xstyle=1, ystyle=1

  if ( exist(ts_saa_sec) and exist(te_saa_sec) ) then begin
    pattern = bytarr(10,10)
    for i=0,9 do pattern[i,i] = 255b
    for i=0, n_elements(ts_saa_new_sec)-1 do begin
      x_poly = [ts_saa_new_sec[i], te_saa_new_sec[i], te_saa_new_sec[i], ts_saa_new_sec[i]] - anytim(timerange[0])
      y_poly = [!y.crange[0],!y.crange[0],!y.crange[1],!y.crange[1]]
      polyfill, x_poly, y_poly, pattern=pattern
    endfor
  endif

; Fill in XTWs:

  wdef,win1,1024,256
  utplot, sec_arr, lonarr(n_sec), timerange=timerange, yrange=yrange, xstyle=1, ystyle=1

  if ( exist(ts_xtw_sec) and exist(te_xtw_sec) ) then begin
    pattern = bytarr(10,10)
    for i=0,9 do pattern[i,i] = 255b
    for i=0, n_elements(ts_xtw_new_sec)-1 do begin
      x_poly = [ts_xtw_new_sec[i], te_xtw_new_sec[i], te_xtw_new_sec[i], ts_xtw_new_sec[i]] - anytim(timerange[0])
      y_poly = [!y.crange[0],!y.crange[0],!y.crange[1],!y.crange[1]]
      polyfill, x_poly, y_poly, pattern=pattern
    endfor
  endif

; Fill in NGTs:

  wdef,win2,1024,256
  utplot, sec_arr, lonarr(n_sec), timerange=timerange, yrange=yrange, xstyle=1, ystyle=1

  if ( exist(ts_ngt_sec) and exist(te_ngt_sec) ) then begin
    pattern = bytarr(10,10)
    for i=0,9 do pattern[i,i] = 255b
    for i=0, n_elements(ts_ngt_new_sec)-1 do begin
      x_poly = [ts_ngt_new_sec[i], te_ngt_new_sec[i], te_ngt_new_sec[i], ts_ngt_new_sec[i]] - anytim(timerange[0])
      y_poly = [!y.crange[0],!y.crange[0],!y.crange[1],!y.crange[1]]
      polyfill, x_poly, y_poly, pattern=pattern
    endfor
  endif

; Fill in composite events:

  wdef,win3,1024,256
  utplot, sec_arr, lonarr(n_sec), timerange=timerange, yrange=yrange, xstyle=1, ystyle=1

  pattern = bytarr(10,10)
  for i=0,9 do pattern[i,i] = 255b
  for i=0, n_elements(ts_evt_sec)-1 do begin
    x_poly = [ts_evt_sec[i], te_evt_sec[i], te_evt_sec[i], ts_evt_sec[i]] - anytim(timerange[0])
    y_poly = [!y.crange[0],!y.crange[0],!y.crange[1],!y.crange[1]]
    polyfill, x_poly, y_poly, pattern=pattern
  endfor

endif

if keyword_set(qstop) then stop, ' Stopping on request.'

end
