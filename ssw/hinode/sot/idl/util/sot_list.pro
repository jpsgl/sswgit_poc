;+
; NAME:
;       sot_list_ev
; PURPOSE:
;       The event handler for sot_list
;
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL 
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL
;       10-May-2007 - Version 1.3 - Marty Hu LMSAL
;       29-Jun-2007 - Version 1.4 - Marty Hu LMSAL
;-

pro sot_list_event, ev

widget_control, ev.id, get_uvalue=uval

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, filnam, ss, result, filnam_sel, index, data,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat, index_sel, data_sel, big_base, w_tag_names1, tag_names_arr, w_1, blog_arr, w1, searchstring, blog_base

if size(uval,/TYPE) eq 7 then begin

    if uval eq 'dismiss' then begin

        if exist(index_sel) then index = index_sel
        if exist(data_sel) then data = data_sel
        widget_control, ev.top, /destroy

    endif

    if uval eq 'wlist' then begin

        selected=widget_info(ev.id, /list_select)

        for ii=0, n_elements(selected)-1 do begin

            if strmid(filnam_sel[selected[ii]],0,1) ne '*' then $
              filnam_sel[selected[ii]] = '*   '+filnam_sel[selected[ii]] else $
              filnam_sel[selected[ii]] = strmid(filnam_sel[selected[ii]], 4, strlen(filnam_sel[selected[ii]]))

        endfor

        widget_control, result, set_value=filnam_sel
        widget_control, result, set_list_select=selected[0]

    endif

    if uval eq 'reset' then begin

        list=get_infox(index, 'TIME_D$OBS, naxis1, naxis2, wave')
        blank=strarr(n_elements(list))
        blank[*]=' '
        filnam_sel=list+ blank + filnam
        filnam_sel=list+ blank + strmid(filnam,strpos(filnam, '/', /reverse_search))
        widget_control, result, set_value=filnam_sel

    endif
    

    if uval eq 'read' then begin

        ss=where(strmid(filnam_sel,0,1) eq '*')
        mreadfits, filnam(ss),index_sel,data_sel
        
    endif

    if uval eq 'display' then begin 

                                ;widget_control, ev.id, get_value=displayarr
;    file1=displayarr[widget_info(ev.id, /droplist_select)]

;    case file1 of
        
;        'Display':
;        'tvscl': begin
;            wdef, 0, index_sel(0).naxis1, index_sel(0).naxis2
;            tvscl, ssw_sigscale(index_sel(0), data_sel(*,*,0))
;        end
;        'iimage': iimage,data_sel(*,*,0)
;        ;'img_summary': img_summary,data_sel(*,*,0)
;        ;'xdisp_fits' : xdisp_fits, dir = filnam[ss[0]]
;        'atv': atv, ssw_sigscale(index_sel(0), data_sel(*,*,0))

        
        ss=where(strmid(filnam_sel,0,1) eq '*')
        mreadfits, filnam(ss),index_sel,data_sel
        
        atv, ssw_sigscale(index_sel(0), data_sel(*,*,0))

;    endcase
    endif

;
; panorama is  Ralph Seguin's (seguin@lmsal.com) movie tool to
; display multiple FITS and other images as movies.
; It takes a list of FITS files on the command line, or in a command  file
; with one line per image filename.
;
    if uval eq 'panorama' then begin
                                ; Get the index of selected images into ss
        ss=where(strmid(filnam_sel,0,1) eq '*')

                                ; Now build up the command line to run.
                                ;
                                ; TODO: FIXME: Should put in multi-channel support, based upon         WAVEID or
                                ; some other partitioning mechanism.
                                ;
        wls = strmid(filnam_sel(ss),36,18)
        sorted = sort(wls)
        uwls = uniq(wls,sorted)
        channels = ' '
        print,'Channels:'

        for ii=0, n_elements(uwls)-1 do begin
            print,sorted[ii],uwls[ii],wls[uwls[ii]]
            chanidx = where(strmid(filnam_sel(ss),36,18) eq wls[uwls[ii]])
            channels = channels + ' -channel ' + strjoin(filnam(chanidx),' ')
        endfor

        cmd='panorama -datamin 220 ' + channels + ' &'

                                ; Finally, launch the program.  
        print, cmd
        spawn, cmd
    endif

;
; So(lar)Da(ta) Surf(er)  Mark Cheung's (cheung@lmsal.com) IDL movie browsing tool to
; display 3D cubes (e.g. a stack of images)
; It takes a list of FITS files on the command line, or in a command file
; with one line per image filename.

    if uval eq 'soda_surf' then begin
                                ; Get the list of selected images into ss

        widget_control, /hourglass
        ss=where(strmid(filnam_sel,0,1) eq '*')
        Launch_SoDASurf, Group_leader=0, filenames=filnam[ss]
    endif

endif else begin
    if tag_names(ev,/STRUCTURE_NAME) eq 'WIDGET_BASE' then begin
        widget_control, uval.list,SCR_XSIZE=ev.X+uval.diff[0], $
                        SCR_YSIZE=ev.Y+uval.diff[1]
    endif
endelse

end

PRO sot_list, level0=level0

;+
; NAME:
;       sot_list
; PURPOSE:
;       Displays the list of selections filtered from the sot_select
;       routine. User can select multiple items (via click and drag) and display or read
;       them for further use.
; CALLING SEQUENCE:
;       This is a subroutine for sot_select.
; INPUTS: None so far.
; KEYWORD PARAMETERS:
;       level0: Use level0 catalog (default is Quicklook)
; OUTPUTS:
;       INDEX_OUT - Index record array for selected files
;       DATA_OUT  - Data array for selected files
; MODIFICATION HISTORY:                                                       
;       27-Oct-2006 - Marty Hu LMSAL
;       13-Dec-2006 - Version 1.0 - Marty Hu LMSAL
;       17-Jan-2007 - Version 1.1 - Marty Hu LMSAL 
;                   - Fixed display button to read data and display it in one click
;                   - Increased y size so more files are available
;                   - Widget now focuses on each selection, rather than the top
;                   - Display is only the atv call
;        1-Feb-2007 - Version 1.2 - Marty Hu LMSAL
;                   - ListBox widget to replace droplist widget with wavelength, obstype, etc.
;                   - Use of the multiple keyword
;       10-May-2007 - Version 1.3 Marty Hu LMSAL
;                   - Added structure tag keyword search
;                   - Resizable widget functionality
;                   - Cosmetic changes and removal of common blocks
;                   - Recurrent searches are more user friendly
;       29-Jun-2007 - Version 1.4 Marty Hu LMSAL
;                   - Fixed filnames so they are not all the same
;                   - Added level0 keyword, panorama, and sodasurf
;                   - Added files keyword
;-

common share3, w_hours, w_minutes, w_day, w_month, w_year, w_hours1, w_minutes1, w_day1, w_month1, w_year1,w_obstype1, w_wavelength1, w_nx1, w_ny1, dum, cat, filnam, ss, result, filnam_sel, index, data,obstype1,wavelength1, nx1, ny1, obstype_arr, wavelength_arr, nx_arr, ny_arr, full_cat, index_sel, data_sel, big_base, w_tag_names1, tag_names_arr, w_1, blog_arr, w1, searchstring, blog_base

base=widget_base(/COLUMN, title='SOT List')
refine_base = widget_base(base, /ROW)

start_search = widget_base(base, /ROW)
reset = widget_button(start_search, value = 'Reset', uvalue='reset')
read=widget_button(start_search, value='Read', uvalue='read', xsize=100)
;display=widget_droplist(start_search, value=['Display', 'tvscl', 'iimage', 'atv'], uvalue='display', xsize=100)
display=widget_button(start_search, value='Display', uvalue = 'display')
; Next line added by Mark Cheung
panorama = widget_button(start_search, value='Panorama', uvalue = 'panorama')
soda_surf= widget_button(start_search, value='SoDaSurf', uvalue = 'soda_surf')
dismiss = widget_button(start_search, value = 'Dismiss', uvalue = 'dismiss', xsize=200)


if size(cat, /type) eq 8 and dum eq 0 then begin

    if keyword_set(level0) then filnam = sot_cat2files(cat, /sirius) else filnam = sot_cat2files(cat)

    mreadfits, filnam,index
    list=get_infox(index, 'TIME_D$OBS, naxis1, naxis2, wave')
    blank=strarr(n_elements(list))
    blank[*]=' '
    filnam_sel=list+ blank + strmid(transpose(filnam),strpos(transpose(filnam), '/', /REVERSE_SEARCH))
    result = widget_list(base, value=filnam_sel, uvalue='wlist', font='helvetica',ysize=30,/FRAME, /MULTIPLE)

endif else begin
    print, 'try again 2'
endelse

num_images = widget_label(refine_base, value = 'Found '+ strtrim(n_elements(cat),2) + ' images')

widget_control, base, /realize, /tlb_size_events

;; result is a common block and only exists if widget_list is defined
;; or else it is a long

  geom=widget_info(result,/GEOMETRY)
  bgeom=widget_info(base,/GEOMETRY)
  list_size_diff=[geom.SCR_XSIZE-bgeom.SCR_XSIZE,geom.SCR_YSIZE-bgeom.SCR_YSIZE]
  state={diff:list_size_diff,list:result}
  widget_control, base ,SET_UVALUE=state

xmanager, 'sot_list',base

index_out = index
data_out = data
files_out=filnam(ss)

end
