
pro hop79ew_15n, date, _extra=extra

if not exist(date) then begin

  print, ''
  print, ''
  print, " Call: hop79ew_15n, [date]"
  print, ''
  print, ''
  print, ' This is a wrapper for calling GET_PNT_TIMES for LAT=15 scan.
  print, ' Date of timeline is only required parameter, eg:
  print, "   IDL> hop79ew_15n, '13-apr-2013'"
  print, ' This routine will then call GET_PNT_TIMES with the following args:
  print, '   get_pnt_times, [your chosen date], lat=15, /bfi 
  print, ' All get_pnt_times keywords are inherited, so you may use any of
  print, '   the GET_PNT_TIMES keywords.  For example:
  print, "     IDL> hop79ew_15n, '04-feb-2010', lat=15, /bfi, dt_dwell_min=10, ts_win='05-feb-2010 05:30'"
  print, ' Optional keywords:
  print, '   TS_WIN         Optional earliest time to begin first scan (automatically constrained to be'
  print, '                    after OP_START time and before OP_END time.'
  print, '   DT_DWELL_MIN:  Dwell period between re-points in minutes (default is 15 min for 30 min for NS scan).'
  print, '   DT_POST_SAA:   Offset in minutes between end of an SAA and start of a re-point (default is 1 min).'
  print, '   DT_PRE_SAA:    Minimum offset in minutes between end of dwell interval (DT_DWELL_MIN) and start of an SAA'
  print, '                    (default is 1 min).'
  print, '   BFI:           Use the BFI FOV when calculating positions and overlaps on the Sun.'
  print, '   NFI:           Use the NFI FOV when calculating positions and overlaps.'
  print, '   SP:            Use the SP slit dimensions when calculating positions and overlaps.'

endif else begin

  get_pnt_times, date, lat=15, /bfi, _extra=extra

endelse

end
