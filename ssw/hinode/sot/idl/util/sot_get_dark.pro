function sot_get_dark,index,dark_index, update_history=update_history
;+
;   Name: sot_get_dark
;
;   Purpose: return dark frames implied by input 'index'
;
;   Input Paraemters:
;      index record(s) from read_sot.pro 
;
;   Output:
;      function returns appropriate dark frame data array
;
;   Output Parameters:
;      dark_index - associated index for dark data
;
;   Keyword Parameters:
;      update_history - if set, add history info to input 'index'
;
;   History:
;      16-Oct-2006 - S.L.Freeland - beta version - SP piece from Bruce Lites
;                   getdark_sbsp.pro
;      17-Oct-2006 - S.L.Freeland - tweak prelaunch 
;      19-Oct-2006 - S.L.Freeland - fixed above... prelaunch 
;
;   Restrictions:
;      proto version - prelaunch/pre-turn on  pending $SSWDB/hindoe/sot/dark/
;                      dbase population - for SP, return Bruce Lites
;                      preliminary test data
;     
; 

sp4d=strpos(gt_tagval(index(0),/obs_type),'SP IQUV 4D') ne -1

nx=gt_tagval(index,/naxis1)
ny=gt_tagval(index,/naxis2)
summing=gt_tagval(index,/CAMSSUM)

itime=gt_tagval(index,/date_obs)
prelaunch=ssw_deltat(itime(0),ref='1-oct-2006',/days) lt 0

dark_index=index

if sp4d then begin 
   dark_index=dark_index(0)
   dark_index.obs_type=dark_index.obs_type+' DARK'
   if prelaunch then begin 
      fname='darkav_24jan06_save'+(['','_sum'])(summing(0) ne 1)
      darkcals=file_search('$SOT_CALIBRATION',fname+'*')
      if file_exist(darkcals(0)) then begin 
          restgenx,file=darkcals(0),dark 
          box_message,'Prelaunch, using test data
      endif else begin 
         box_message,'Cannot find test dark, returning dummy data'
         dark=dblarr(nx(0),ny(0),2)
       endelse
   endif else begin 
      box_message,'No dark frame yet, returning dummy array'
      dark=dblarr(nx(0),ny(0)/summing(0),2)
   endelse
      ; subrange logic per Bruce Lites
      dix0=gt_tagval(dark_index,/spccdix0,missing=0)/summing(0)
      dix1=gt_tagval(dark_index,/spccdix1,missing=ny)/summing(0)
      dark=temporary(dark(*,dix0:dix1,*))
endif else begin 
   box_message,'No FG darks available, returning dummy data'
   dark=dblarr(nx/summing, ny/summing)
endelse

return,dark
end
