pro hinode_server_select,hinode_parent, $
   lmsal=lmsal,oslo=oslo,gsfc=gsfc,darts=darts,mssl=mssl, $
   _extra=_extra_, status=status, current=current , reset=reset
;+
;   Name: hinode_server_select
;
;   Purpose: select desired HINODE http server for future requests (sot_cat...)
;
;   Input Parameters:
;      hinode_parent - explicit HINODE IP parent (in lieu of switches)
;
;   Keyword Parameters:
;      /LMSAL,/OSLO,/GSFC,/DARTS,/MSSL - desired HINODE http server
;      reset - restore original value of $HINODE_DATA_URL
;      status - if set, show setting of $HINODE_DATA_URL upon exit 
;      current (output) - return current setting of $HINODE_DATA_URL
;
;   Calling Sequence:
;      IDL> hinode_server_select [,parentip] [,{/lmsal,/oslo,/darts,/mssl}] 
;                                [,/reset] [,/status] [,current=current]
;
;   History:
;      16-oct-2007 - S.L.Freeland
;      20-oct-2007 - S.L.Freeland - per Stein, add Oslo parent (.gz for today)
;       2-nov-2007 - S.L.F - enable /GSFC
;      21-nov-2007 - S.L.F. - enable /MSSL per Jian Sun
;-
common hinode_server_select_blk,original
if n_elements(original) eq '' then original=get_logenv('HINODE_DATA_URL')
reset=keyword_set(reset)
lmsal=keyword_set(lmsal)
oslo=keyword_set(oslo)
gsfc=keyword_set(gsfc)
mssl=keyword_set(mssl)
darts=keyword_set(darts)
status=keyword_set(status)

case 1 of 
   n_params() eq 1: ; user supplied via positional parameter
   oslo:  hinode_parent="http://sdc.uio.no/vol/fits/" 
   darts: hinode_parent="http://darts.isas.jaxa.jp/pub/solar/hinode/"
   lmsal: hinode_parent="http://sot.lmsal.com/data/"
   gsfc:  hinode_parent="http://umbra.nascom.nasa.gov/hinode/" 
   mssl:  hinode_parent="http://solar.ads.rl.ac.uk/MSSL-data/"
   reset: hinode_parent=original ;
   else:
endcase

if data_chk(hinode_parent,/string) then $
   set_logenv,'HINODE_DATA_URL',hinode_parent,/quiet

if status then box_message,'Current $HINODE_DATA_URL = ' + $
         get_logenv('HINODE_DATA_URL')

return
end
