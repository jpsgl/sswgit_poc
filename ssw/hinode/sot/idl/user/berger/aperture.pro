FUNCTION aperture, nx, ny, xz, yz, XC=xc, YC=yc

;Returns a binary mask of an ellipse with diameters NX and NY in the
;x- and y-axes, centered at XC,YC in the image of size XZ,YZ.


if KEYWORD_SET(xc) then xc = xc else xc = 0
if KEYWORD_SET(yc) then yc = yc else yc = 0

im = BYTARR(xz,yz)
theta = (2*!PI/(nx-1))*FINDGEN(nx)
x = nx/2*COS(theta)+xc
y = ny/2*SIN(theta)+yc
ci = POLYFILLV(x,y,xz,yz)

im[ci] = 1

RETURN,im
END

