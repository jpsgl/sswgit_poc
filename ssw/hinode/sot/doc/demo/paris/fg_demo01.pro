; SOT SSW Tutorial file fg_demo01.pro 
;
;The following tutorial is found in the SSW distribution in the following file: 
;$SSW/hinode/sot/doc/demo/paris/fg_demo01.pro. In it you will learn how to access 
;FG data files on your local disk as well as from a remote server. You will also 
;learn how to use the fg_prep.pro routine to apply basic photometric corrections 
;to FG images. 

;We want to produce a co-aligned dataset of SOT/FG G-band, Ca II H-line, 
; and Stokes V/I magnetogram images of active region AR 10926 for a period
; of time on 9-December, 2006.  

; First, if your data is on your local hard drive, you need to point the catalog to that 
; location using the $SOT_DATA environment variable:
set_logenv,'SOT_DATA','/Users/berger/research/projects/hinode/sot/data'

;Now find all relevant observations for the time interval of interest.  
;We make use of the SSW routines sot_cat.pro and sot_umodes.pro to access the data 
;catalog and identify all unique observational modes that were executed during the 
;time interval:

time0 = '09-Dec-2006T11:30:00'
time1 = '09-Dec-2006T15:00:00'

sot_cat, time0, time1, /level0, cat, files

;Note that there are two versions of the SOT catalog: one is the QuickLook catalog 
;and the other is the Level-0 catalog. By default sot_cat searches the QuickLook 
;catalog.  However data is purged form the QuickLook database after a nominal period
;of a few months. If the data of interest is older than a few months, you need to 
;include the “/level0” keyword in your call to sot_cat. In general it is advised to 
;use QuickLook data only for cursory data exploration. Always use the /level0 flag 
;for data that you intend to analyze for scientific content.  

;Sot_cat returns two variables: “cat” which is the SOT catalog structure for the time 
;range indicated, and “files” which are the filenames of all SOT files found in the 
;local database for the time range given. The filenames are referenced to the local 
;source of SOT FITS files on your system. This location is set by the environment 
;variable “SOT_DATA”. If you do not have SOT data locally on your machine, you can 
;request that sot_cat return URLs to the nearest database center, as illustrated below.  

help, cat, files
;CAT             STRUCT    = -> <Anonymous> Array[2994]
;FILES           STRING    = Array[2994]

;For the date/time period specified, a total of 2994 SOT observations are found . 
;This includes FG, SP, correlation tracker reference frames, and other images 
;not necessarily of interest to our example.  In order to winnow this down to 
;the particular type of observations we are interested in, we use sot_umodes.pro 
;to identify all “unique modes” that were observed during the time interval:

modes = sot_umodes(cat, mcount=mc, info=info)

;You can print out the unique modes found to see what is available using the SSW 
;string printing routine “prstr”:

prstr, modes
;FG (simple)   		  G band 4305    	2048     1024
;FG (simple)  		  Ca II H line     	2048     1024
;SP IQUV 4D array         6302A      		 112      512
;FG shuttered I and V     TF Fe I 6302          2048     1024

;We wish to select only the Ca II H-line FG mode for further 
;analysis in this example. There are two options to accomplish this selection. 
;One, you can call sot_cat.pro again, this time using the ‘search_array’ input 
;parameter to return only the 2048 x 1024 magnetogram images:

sot_cat, time0, time1, ccat, cfiles, /level0, search_array=['wave=Ca*II*H*line','naxis1=2048','naxis2=1024']
help, ccat, cfiles
;CCAT             STRUCT    = -> <Anonymous> Array[103]
;CFILES           STRING    = Array[103]

;The second option is to use the interactive feature of the sot_umodes function as follows:

ss = sot_umodes(cat, /interactive)

;In the widget that opens, choose the observation type that you would like to 
;analyze. In this case, we choose the line with 'Ca II H line' in it in order  
;to select only the Ca H-line data:

cfiles = files[ss]
help,cfiles
;CFILES          STRING    = Array[103]


;Take a look at the filenames contained in CFILES to see that they are pointing to 
; the correct data location:

print, cfiles
nc = N_elements(cfiles)

;Keep in mind that nothing has been read yet - there are no images in your 
;SSW session yet. In order to read a data file and take a look at the 
;contents, we use the read_sot routine:

read_sot,cfiles[0],index,data

;The "INDEX" variable contains the FITS header data in a structure format.
;You can look at the data in the header as follows:

help,index,/str

;Note that the keywords "CAMSSUM" and "CAMPSUM" are both 2. This indicates that
;the images are summed in both the Serial and Parallel directions on the camera.
;Thus the images are 2x2 summed pixel images that were originally 4096x2048 
;full resolution pixels.  

;Similarly, you can examine the images in the data file as follows.

WINDOW,xs=1024,ys=512
TVSCL,REBIN(data,1024,512)

;At this point we have identified the set of 103 files we wish to analyze.
;We next calibrate the full set of images, using FG_PREP.PRO. There are several
;ways to do this: 

;1. Handing the entire block of pre-read index and data variables to fg_prep:
read_sot,cfiles, index, data
fg_prep,index, data, index_out, data_out, /despike

;2. Reading the files from within fg_prep:
fg_prep, cfiles, -1, index_out, data_out, /despike

;3. Handing individual images to fg_prep from within a loop:
outpath='/Volumes/data1/SOT/2006/12/09/FG/'
mkdir,outpath
for i=0,nc-1 do begin
   read_sot,cfiles[i],index,data
   fg_prep,index,data,index_out,data_out, /despike
   mwritefits,sgtruct2ssw(index_out),data_out,$
              outfile=STRING(outpath+'20061209_caH_level1.',i,'.fits',format='(a,i05,a)'), $
              /flat_fits
end

;The methods are listed in order of decreasing memory useage in IDL. Note that the 
;first methods results in full 3D data cubes for both the level-0 and level-1 data.
;This can rapidly overwhelm the available RAM for laptops or small workstations. The
;second method reads in one level-0 file at a time but returns a full 3-D level-1 
;data cube. The third method reads in one file at a time and writes out one file
;at a time and thus minimizes memory load.

;Next, examine the calibrated data as a movie using a new movie viewer called sodasurf.
; if you used method 3 above, you won't have a calibrated data_out cube. Make an abbreviated one:

fg_prep,cfiles[0:20],-1,sindex_out,sdata_out,/despike

;Launch sodasurf:
launch_sodasurf, data=sdata_out


;Occasionally an image will be corrupted in the download process from the spacecraft.
;These images will typically have large sections of the FOV with 0 pixel values. 
;We can remove instances of incomplete images using a simple filter:

percent_zero = dblarr(nc)
for i=0,nc-1 do percent_zero(i) = (n_elements(where(data_out[*,*,i] eq 0)) / (2048.0*1024)) * 100
ss_good = where(percent_zero lt 5, n_good)
index_cah = index_out(ss_good)
data_cah = reform(data_out[*,*,ss_good])


; We next search for and process the Magnetogram images using the same steps, but simply specifying the
; appropriate FGIV mode:
time0 = '09-Dec-2006T11:30:00'
time1 = '09-Dec-2006T15:00:00'
sot_cat, time0, time1, mcat, mfiles, /level0, search_array=['wave=*Fe*I*6302*','naxis1=2048','naxis2=1024']
help, mcat
; MCAT             STRUCT    = -> <Anonymous> Array[103]

; We again 'prep' the images, using FG_PREP.PRO:
fg_prep, mfiles[0:19], -1, index_out, data_out, /despike
nm = n_elements(index_out)
help,data_out


;For making presentation movies you usually want to write each of the data
;files to disk as PNG, JPEG, or GIF files that are read into an MPEG movie
;producers. Here we demonstrate how to use the QuickTime Pro program to make 
;a movie.
for i=0,nm-1 do write_gif,string('./sot_20061209_mag.',i,'.gif',format='(a,i04,a)'),$
                          bytscl(data_out[512:1535,128:768,1,i]>(-500)<500)

;Finally, create Stokes V/I "polarization density" magnetograms:
data_out = reform(data_out[*,*,1,*]/(data_out[*,*,0,*]+1))


;For very large datasets that you want to briefly explore, you can use the 
;timegrid.pro routine to sub-sample the catalog:
sot_cat,time0,time1,/level0,cat

;Sample only 3 images from the whole time series. The files are evenly 
;spaced throughout the dataset:
n_samp = 3
n_rec = n_elements(cat)
t_grid = timegrid(cat(0).date_obs, cat(n_rec-1).date_obs, nsamp=n_samp)
ss_samp = tim2dset(anytim(cat.date_obs,/ints), anytim(t_grid,/ints))
cat_samp = cat[ss_samp]
files_samp = files[ss_samp]

; You can also select a subset of the magnetograms that are closest in
; in time to the caH set. 

sot_cat, time0, time1, cat, files, /level0, search_array=['wave=Fe*6302','naxis1=2048','naxis2=1024']
ss_samp_mag = tim2dset(anytim(cat.date_obs,/ints), anytim(index_cah.date_obs,/ints))
cat_samp_mag = cat[ss_samp_mag]
files_samp_mag = files[ss_samp_mag]


;The previous demonstration assumed that you had the data files on a disk 
;local to your system. Although the SOT catalog is online locally (if the 
;hinode branch of the SolarSoft Database tree has been installed), the actual 
;FITS data files may not be available locally.  In this case one can 
;remotely access the FITS files from one of the online database centers.  
;To specify the desired institution, the routine hinode_server_select just 
;sets the environmental which points to desired sot/xrt http server.  
;In the following demonstration we use the Oslo as our remote server site.


;First, set the hinode_server_select variable to the nearest data center. 
;Here we use the Oslo data center: 

hinode_server_select,/oslo

;Now use sot_cat with the /URL keyword set:
time0 = '09-Dec-2006T11:30:00'
time1 = '09-Dec-2006T15:00:00'
sot_cat, time0, time1, /level0, cat, urls, /URLS
help, urls
; URLS            STRING    = Array[2994]

;Use sot_umodes to narrow down the selection:
ss = sot_umodes(cat,/int)

;Selecting the G-band data this time, we narrow our dataset down and then use sock_copy to get the data:

gurls = urls[ss]
dir_local = './demo'
if file_exist(dir_local) eq 0 then spawn,'mkdir '+dir_local

;Now use the sock_copy routine to fetch the files and write them to the specified directory.
;This takes about 13 seconds per image during the current demonstration in Paris using the Oslo
;data center. 
sock_copy, gurls, out_dir='./demo'
gfiles = file_list(dir_local, '*.fits*')


