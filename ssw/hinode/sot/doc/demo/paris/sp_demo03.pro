
; SOT/SP Demo 3. - SOT/SP Level0 -> Level1 -> Stokes maps
;
; Assumes local Hinode/sot level0 data tree 

; Sub-Demo 3.1 - Find and group SP level0 images by map via sotsp_time2scan.pro
;       /span_day keyword links slit images for a map across "next UTDay"
;       The function sotsp_time2scan returns one structure per map-group
;       and includes tags required to subscript 'catalog' and 'files' 
;       (or 'urls') output parameters for calibration and map generation. 

IDL> time0 = '12-jan-2007 16:00'
IDL> time1 = '12-jan-2007 17:00'
IDL> sot_cat, time0, time1, /level0, search=['obsdir=SP4D', 'ycen<-900'], cat, files
IDL> scninfo = sotsp_time2scan(time0, time1, /span_day, cat, files)
IDL> help, scninfo, cat, files
SCNINFO         STRUCT    = -> <Anonymous> Array[4]
CAT             STRUCT    = -> <Anonymous> Array[239]
FILES           STRING    = Array[239]

IDL> more,get_infox(scninfo(0:3),'date_obs,nrecs,ssstart,ssstop,macroid')
2007-01-12T16:14:05.883        64         0        63      8627
2007-01-12T16:24:46.656        64        64       127      8628
2007-01-12T16:35:27.441        64       128       191      8629
2007-01-12T16:46:08.016        47       192       238      8630

; Show 1st & last Level0 file names (1 slit scan per file) for each map

IDL> fnames = ssw_strsplit(files,'/',/tail)  ; 'files' output, sans full path
IDL> more, fnames(scninfo(0:3).ssstart) + ' - ' + fnames(scninfo(0:3).ssstop)            
SP4D20070112_161405.8.fits - SP4D20070112_162429.8.fits
SP4D20070112_162446.6.fits - SP4D20070112_163510.4.fits
SP4D20070112_163527.4.fits - SP4D20070112_164551.0.fits
SP4D20070112_164608.0.fits - SP4D20070112_165343.5.fits

; Sub-Demo 3.2 - Example: using above <scninfo>, <cat>, & <files> output, calibrate the 
;       set of slit scan images which for the 2nd map  above, e.g.

IDL> scan=scninfo(1) & spname=time2file(scan.date_obs,/sec)
IDL> more, spname & mk_dir, spname ; name by 1st scan image & make empty directory
20070112_162446

; sp_prep.pro is now called to calibrate the Level0 images (4D->3D)
; Although ssw clients are welcome to execute sp_prep, note that the
; SOT/SP group intends to distribute Level1 and reconstructed stokes maps
; If you want the maps but prefer to access pre-generated Maps, you
; may use the function sotsp_stks2struct to ingest the data on the 
; SOT/SP server -> your sswidl client session - for example, to see
; if the above map is already resident on the remote server, and 
; transfer/read into into your sswidl session:
; 
; IDL> stks=sotsp_stks2struct(spname) ; <spname> as derived above 

; If available, the output of the above is a structure containing the 
; Maps (as described in 1.3 below)
; Alternately, the ssw-client call to sp_prep call is:

IDL> sp_prep, files(scan. ssstart:scan. ssstop), outdir=spname, /quiet	; L0->L1

; sp_prep calls will take a a few to many tens of minutes (One reason why SOT/SP data
; management baseline is to offer Level1 data).  This particular example should
; take about five minutes on a current desktop machine.

; Sub-Demo 3.3 - Construct stokes maps from Level1 files created in 1.2 above

IDL> stksimages_sbsp, spname, outstokes, outdir=spname ; Level1 -> "Level1.5" 
IDL> help, outstokes, /str
** Structure <2a45810>, 14 tags, length=4194304, data length=4194304, refs=1:
   BLAPP           FLOAT     Array[64, 1024]
   BTAPP           FLOAT     Array[64, 1024]
   CONTI           FLOAT     Array[64, 1024]
   CTRLINE         FLOAT     Array[64, 1024, 2]
   PII             FLOAT     Array[64, 1024]
   PLC             FLOAT     Array[64, 1024]
   PLL             FLOAT     Array[64, 1024]
   POLNET          FLOAT     Array[64, 1024]
   PQ              FLOAT     Array[64, 1024]
   PTOT            FLOAT     Array[64, 1024]
   PU              FLOAT     Array[64, 1024]
   PV              FLOAT     Array[64, 1024]
   RUFAZ           FLOAT     Array[64, 1024]
   VCS             FLOAT     Array[64, 1024, 2]

IDL> stks_struct_local = ssw_save2struct(concat_dir(spname, 'stksimoutput.save'))

; SOT/SP Demo 4.  L0 -> L1 -> Stokes maps for remote clients
;
; Assumes local SOT catalogs but no local Level0 data
;
; The process is very simular to Demo 1. with additional 
; level0 -> local sswidl client transfer step.  We take advantage
; of the optional /URLS switch so that the 'files' output of 
; sotsp_time2scan are urls rather than nfs file paths.
;
; Sub-Demo 4.1 Select desired SOT/SP remote server and then run SP scan/map
;     grouping function sotsp_time2scan with /URL switch

IDL> hinode_server_select, /oslo  ; or /lmsal or /darts 
IDL> scninfo = sotsp_time2scan(time0, time1, /span_day, cat, urls, /urls) 

IDL> help,scninfo, cat, urls

; Sub-Demo 4.2 Use 2.1 output to identify & transfer Level0 SOT/SP server-> local client
;     and then calibrate L0->L1 via sp_prep 

IDL> scan = scninfo(1) & spname = time2file(scan.date_obs,/sec) ; same Map as 1.2
IDL> more, spname & mk_dir,spname ; name by 1st scan image & make empty directory

IDL> l0dir = spname + '_l0' & mk_dir, l0dir  ; create target directory for L0 files
IDL> sock_copy,urls(scan. ssstart:scan.ssstop), out_dir=l0dir ; L0 -> local 
IDL> sp_prep, file_search(l0dir(0),'*fits*'), outdir=spname, /quiet	; local L0 -> L1

; Sub-Demo 4.3 - Stokes map creation identical to 1.3 

IDL> stksimages_sbsp, spname, outstokes, outdir=spname ; Level1 -> "Level1.5"
IDL> stks_struct_remote = ssw_save2struct(concat_dir(spname,'stksimoutput.save'))

end
