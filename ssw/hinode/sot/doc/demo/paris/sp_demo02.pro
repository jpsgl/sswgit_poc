; SOT SSW Tutorial file sp_demo02.pro 
;
;The following tutorial is found in the SSW distribution in the following file: 
;$SSW/hinode/sot/doc/demo/paris/sp_demo02.pro. In it you will learn how to produce 
;SP Level-1 maps from slit images. The slit images must be accessible from a 
;local Level-0 SOT data directory. 

;First, if necessary, set your SOT_DATA environment variable to point to the 
;local database tree:
set_logenv,'SOT_DATA','/Users/berger/research/projects/hinode/sot/data'

; Sub-Demo 1.1 - Find and group SP level0 images by map via sotsp_time2scan.pro
;/span_day keyword links slit images for a map across "next UTDay"
;The function sotsp_time2scan returns one structure per map-group
;and includes tags required to subscript 'catalog' and 'files' 
;(or 'urls') output parameters for calibration and map generation. 

scninfo=sotsp_time2scan('27-aug-2007 19:25','27-aug-2007 20:45',/span_day,cat,files)
help, scninfo, cat, file
;CAT             STRUCT    = -> <Anonymous> Array[548]
;FILES           STRING    = Array[548]
;SCNINFO         STRUCT    = -> <Anonymous> Array[92]

more,get_infox(scninfo[0:9],'date_obs,nrecs,ssstart,ssstop,macroid')

;2007-08-27T19:32:07.505         6         0         5      6174
;2007-08-27T19:32:53.839         6         6        11      6175
;2007-08-27T19:33:40.155         6        12        17      6176
;2007-08-27T19:34:26.092         6        18        23      6177
;2007-08-27T19:35:11.010         6        24        29      6178
;2007-08-27T19:35:57.148         6        30        35      6179
;2007-08-27T19:36:42.067         6        36        41      6180
;2007-08-27T19:37:28.692         6        42        47      6181
;2007-08-27T19:38:14.825         6        48        53      6182
;2007-08-27T19:39:01.139         6        54        59      6183

; Show 1st & last Level0 file names (1 slit scan per file) for each map

fnames = ssw_strsplit(files,'/',/tail)  ; 'files' output, sans full path
more, fnames(scninfo[0:9].ssstart) + ' - ' + fnames(scninfo[0:9].ssstop)            
; SP4D20070827_193207.5.fits - SP4D20070827_193239.7.fits
; SP4D20070827_193253.8.fits - SP4D20070827_193325.8.fits
; SP4D20070827_193340.1.fits - SP4D20070827_193412.1.fits
; SP4D20070827_193426.0.fits - SP4D20070827_193457.1.fits
; SP4D20070827_193511.0.fits - SP4D20070827_193543.2.fits
; SP4D20070827_193557.1.fits - SP4D20070827_193628.2.fits
; SP4D20070827_193642.0.fits - SP4D20070827_193714.6.fits
; SP4D20070827_193728.6.fits - SP4D20070827_193800.8.fits
; SP4D20070827_193814.8.fits - SP4D20070827_193846.9.fits
; SP4D20070827_193901.1.fits - SP4D20070827_193933.2.fits

; Sub-Demo 1.2 - Example: using above <scninfo>, <cat>, & <files> output, calibrate the 
;       set of slit scan images which for the 2nd map  above, e.g.

scan=scninfo[1] & spname=time2file(scan.date_obs,/sec)
more, spname & mk_dir, spname ; name by 1st scan image & make empty directory
;     20070827_193253 << Uniq SP Map identifier = <spname> for this demo.  

;Sp_prep.pro is now called to calibrate the Level-0 images. 
;This combines the orthogonal polarization slit images to create a 
;3-D [x,y,Stokes vector] slit array. 

sp_prep,files(scan.ssstart:scan.ssstop),outdir=spname, /quiet	; L0->L1

;Depending on the number of slit images in your scan, the sp_prep calls 
;will take a few to many tens of minutes to complete. This is one reason 
;why it is probably worth it to check for pre-calibrated Level-1 maps in the 
;database, as shown in the previous tutorial. The SOT team intends to produce 
;Level-1 SP maps for the entire mission. Currently the processing to accomplish 
;this is ongoing. Check the website:
;    http://www.lmsal.com/solarsoft/hinode/level2/2006/12/month_index.html
;to see if there are level 1.5 maps for the date of interest.

;Next we generate the 2-D maps from the calibrated spectra. The Stokes component 
;maps are sometimes referred to as “Level-1.5” data. 

stksimages_sbsp,spname,outstokes,outdir=spname ; Level1 -> "Level1.5" 
help, stks, /str
stks_struct_local = ssw_save2struct(concat_dir(spname,'stksimoutput.save'))

;As in the FG tutorial above, we now show how to retrieve Level-0 slit data 
;files from a remote database. Aside from fetching the data, the process is 
;very similar to the process shown above. We will use the optional /URLS 
;switch so that the 'files' output of sotsp_time2scan are urls rather than 
;nfs file paths.

hinode_server_select, /oslo  ; or /lmsal or /darts 
scninfo=sotsp_time2scan('27-aug-2007 19:25','27-aug-2007 20:45',/span_day,cat,files,/urls) 

help,scninfo,cat,files
;SCNINFO         STRUCT    = -> <Anonymous> Array[92]  << same as 1.1 above
;CAT             STRUCT    = -> <Anonymous> Array[548] << same as 1.1 above
;FILES           STRING    = Array[548 ]               << L0 URLS


;Choose the second scan in the scan structure:

scan=scninfo[1]
spname=time2file(scan.date_obs,/sec) ; same Map as 1.2
more,spname
;     20061212_035005  << Uniq SP Map identifier = <spname> for this demo.
mk_dir,spname 

l0dir = spname + '_l0'
mk_dir,l0dir  ; create target directory for L0 files

;Now fetch the data from the remote database:

sock_copy,files(scan.ssstart:scan.ssstop),out_dir=l0dir ; L0 -> local 

;And call sp_prep to calibrate the spectra: 
sp_prep, file_search(l0dir[0],'*fits*'), outdir=spname, /quiet	; local L0 -> L1

; Sub-Demo 2.3 - Stokes map creation identical to 1.3 

stksimages_sbsp, spname, outstokes, outdir=spname ; Level1 -> "Level1.5"
stks_struct_remote = ssw_save2struct(concat_dir(spname,'stksimoutput.save'))

end
