; SOT SSW Tutorial file sp_demo01.pro 
;
;The following tutorial is found in the SSW distribution in the following file: 
;$SSW/hinode/sot/doc/demo/paris/sp_demo01.pro. In it you will learn how to access 
;SP Level-1 files from remote databases and display the maps on a solar 
;heliographic grid. 

;The maps we will retrieve are co-temporal to the FG images from 9-December, 2006
;that were produced in fg_demo01.pro.
;
; S.L.Freeland 6-nov-2007

;First we specify the time span for which we want SP maps.
time0 = '09-dec-2006 11:30'
time1 = '09-dec-2006 15:00'

;Recall that the SP data files in the database are single slit images.
;We need to know which slit images belong to a single scan set in 
;order to create a unified 2-d "map" of the magnetic field. 
;We use the sotsp_time2scan function to return a "scan" structure
;with information on which SP files in the database correspond to 
;a given single scan set.

scn=sotsp_time2scan(time0,time1,/span)
help,scn & more,get_infox(scn,'date_obs,nrecs,ssstart,ssstop')
;SCN             STRUCT    = -> <Anonymous> Array[3]
;2006-12-09T11:30:01.543       843         0       842
;2006-12-09T12:40:05.354      1000       843      1842
;2006-12-09T14:00:05.393       842      1843      2684

;NRECS is the number of slit images in a given scan set. 
;The SSSTART and SSSTOP values indicate which files belong to the scan.

;The time2file function returns the unique identifier for each of the 
;three scans found in the time period.
scanids=time2file(scn.date_obs,/sec)

;Next we use the SOTSP_STKS2INDEX function to fetch a particular scan 
;from the database. Each Level-1 scan set contains maps of continuum'
;intensity, longitudinal flux density, and transverse flux density. The 
;flux density maps are in units of Mx/cm^2.

sotsp_stks2index,scanids[1], index, data  ; 2nd scan -> sswidl "index,data"
help,index,data & info=get_infox(index,'date_obs,wave,cunit1,xcen,ycen') & more,info
;INDEX           STRUCT    = -> MS_248917823009 Array[3]
;DATA            FLOAT     = Array[1000, 512, 3]
;2006-12-09T12:40:05.354  6302A                  Continuum Intensity   -420.4725   -100.8778
;2006-12-09T12:40:05.354  6302A  Longitudinal Flux Density, Mx cm^-2   -420.4725   -100.8778
;2006-12-09T12:40:05.354  6302A    Transverse Flux Density, Mx cm^-2   -420.4725   -100.8778

;Finally we use the SSW mapping software to place the maps on a solar grid:

index2map,index,data,maps
wdef,xx,1024,/ur
plot_map,maps[1],fov=6,grid=5,/limb,drange=[-1000,1000],title=info(1),margin=.05
