; Demo to illustrate remote access of SSW compliant Stokes "index,data" -> local sswidl session
;
; S.L.Freeland 6-nov-2007
IDL> scn=sotsp_time2scan('13-dec-2006','14-dec-2006',/span)
IDL> scanids=time2file(scn.date_obs,/sec)
IDL> help,scn & more,get_infox(scn,'date_obs,nrecs,ssstart,ssstop')
SCN             STRUCT    = -> <Anonymous> Array[4]
2006-12-13T04:30:05.312      1000         0       999
2006-12-13T07:50:05.283      1000      1000      1999
2006-12-13T12:51:04.078      1024      2000      3023
2006-12-13T16:21:04.276      1000      3024      4023

; >>> SOTSP_STKS2INDEX - SSW compatible STOKES 2D FITS from remote server->sswidl session
IDL> sotsp_stks2index,scanids(1), index, data  ; 2nd scan -> sswidl "index,data"
IDL> help,index,data & info=get_infox(index,'date_obs,wave,cunit1,xcen,ycen') & more,info
INDEX           STRUCT    = -> MS_248469518001 Array[3]
DATA            FLOAT     = Array[1000, 512, 3]
2006-12-13T07:50:05.283  6302A                  Continuum Intensity    411.4149    -93.4475
2006-12-13T07:50:05.283  6302A  Longitudinal Flux Density, Mx cm^-2    411.4149    -93.4475
2006-12-13T07:50:05.283  6302A    Transverse Flux Density, Mx cm^-2    411.4149    -93.4475

;    -> Mapping engine per D.M.Zarro et. al.
IDL> index2map,index,data,maps
IDL> wdef,xx,1024,/ur
IDL> plot_map,maps(1),fov=6,grid=5,/limb,drange=[-1000,1000],title=info(1),margin=.05

