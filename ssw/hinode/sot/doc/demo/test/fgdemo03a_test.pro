
; SOT SolarSoftDemo fgdemo03a 
; Suppose the user wants to produce a co-aligned movie of SOT Filtergraph magnetogram and
; Ca II H line images of the large sunspot on Nov 9, 2006 from 11:30 UT to 15:00 UT.
; The first step is to find all relevant FG observations for the time interval of interest.  
; In using SolarSoft, there are usually several ways to accomplish a given task.  In this case, 
; we make use of the SolarSoft routine sot_umodes.pro and sot_cat.pro to identify all unique 
; observational modes that were executed during the time interval:

; Example FGDEMO03A: Searching the SOT Catalog For Observations of Interest

; The key routine for catalog searches is sot_cat.pro.  It is quite versatile.  This program 
; returns SOT catalog records in a structure array:

time0 = '09-dec-2006 11:30'
time1 = '09-dec-2006 15:00'
n_samp = 20

sot_cat, time0, time1, cat, /level0

; At this point we note that there are two versions of the SOT catalog.  One is the quicklook 
; catalog and the other is the level0 catalog.  The quicklook catalog is created from the quicklook 
; data in near real time and  incomplete.  By default sot_cat searches the quicklook catalog.  
; If the data of interest is over two weeks old, then the level0 flag should be used in the call 
; to sot_cat, as shown.

help, cat

; Notice that for this 3.5 hr period, a rather 103 catalog records were found, each corresponding
; to a single FITS file in the archive.  In order to winnow this down to the particular type of
; observations we are interested in, we use the routine sot_umodes.pro to identify all unique
; observational modes that were executed during the time interval:

modes = sot_umodes(time0, time1, mcount=mc, info=info, incat=cat)

; We print out the unique modes found to see what is available:

help, modes
prstr, modes

; The modes we are after for the magnetograms are ones with the SOT catalog 'wave' tag set to
; "TF Fe I 6302". so our next step is to call sot_cat.pro again, this time using the 'search_arry'
; input parameter to restrict the search to particular values of any of the SOT catalog tag list.
; We also specify the NAXIS1 and NAXIS2 keywords to be 2048 and 1024, respectively:

sot_cat, time0, time1, cat, files, /level0, search_array=['wave=TF*Fe*I*6302','naxis1=2048','naxis2=1024']
help, cat

; We use the SSW routine timegrid.pro to sub-sample these images:

n_rec = n_elements(cat)
t_grid = timegrid(cat(0).date_obs, cat(n_rec-1).date_obs, nsamp=n_samp)
ss_samp = tim2dset(anytim(cat.date_obs,/ints), anytim(t_grid,/ints))
cat_samp = cat(ss_samp)
files_samp = files(ss_samp)

; At this point we have identified the set of files we wish to analyze, and we have located them
; at an accessible server.

; We next 'prep' the full set of images, using FG_PREP.PRO:
STOP
fg_prep, files_samp, -1, index_out, data_out, /despike
n_img = n_elements(index_out)

; Finally, we remove instances of incomplete images:

percent_zero = dblarr(n_img)
for i=0,n_img-1 do percent_zero(i) = (n_elements(where(data_out(*,*,1,i) eq 0)) / (2048d0*1024d0)) * 100
ss_good = where(percent_zero lt 5, n_good)

index = index_out(ss_good)
data = reform(data_out(*,*,1,ss_good))

end
