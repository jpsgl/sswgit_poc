
; SOT/SP Demo 1. - SOT/SP Level0 -> Level1 -> Stokes maps
;
; Assumes local Hinode/sot level0 data tree 

; Sub-Demo 1.1 - Find and group SP level0 images by map via sotsp_time2scan.pro
;       /span_day keyword links slit images for a map across "next UTDay"
;       The function sotsp_time2scan returns one structure per map-group
;       and includes tags required to subscript 'catalog' and 'files' 
;       (or 'urls') output parameters for calibration and map generation. 

;IDL> scninfo=sotsp_time2scan('27-aug-2007 19:25','27-aug-2007 20:45',/span_day,cat,files)
;IDL> help, scninfo, cat, file
;CAT             STRUCT    = -> <Anonymous> Array[548]
;FILES           STRING    = Array[548]
;SCNINFO         STRUCT    = -> <Anonymous> Array[92]

scninfo=sotsp_time2scan('27-aug-2007 19:25','27-aug-2007 20:45',/span_day,cat,files)
help,scninfo,cat,file

;IDL> more,get_infox(scninfo(0:9),'date_obs,nrecs,ssstart,ssstop,macroid')
;2007-08-27T19:32:07.505         6         0         5      6174
;2007-08-27T19:32:53.839         6         6        11      6175
;2007-08-27T19:33:40.155         6        12        17      6176
;2007-08-27T19:34:26.092         6        18        23      6177
;2007-08-27T19:35:11.010         6        24        29      6178
;2007-08-27T19:35:57.148         6        30        35      6179
;2007-08-27T19:36:42.067         6        36        41      6180
;2007-08-27T19:37:28.692         6        42        47      6181
;2007-08-27T19:38:14.825         6        48        53      6182
;2007-08-27T19:39:01.139         6        54        59      6183

more,get_infox(scninfo(0:9),'date_obs,nrecs,ssstart,ssstop,macroid')

; Show 1st & last Level0 file names (1 slit scan per file) for each map

;IDL> fnames=ssw_strsplit(files,'/',/tail)  ; 'files' output, sans full path
;IDL> more,fnames(scninfo(0:9).ssstart) + ' - ' + fnames(scninfo(0:9).ssstop)            
;SP4D20070827_193207.5.fits - SP4D20070827_193239.7.fits
;SP4D20070827_193253.8.fits - SP4D20070827_193325.8.fits
;SP4D20070827_193340.1.fits - SP4D20070827_193412.1.fits
;SP4D20070827_193426.0.fits - SP4D20070827_193457.1.fits
;SP4D20070827_193511.0.fits - SP4D20070827_193543.2.fits
;SP4D20070827_193557.1.fits - SP4D20070827_193628.2.fits
;SP4D20070827_193642.0.fits - SP4D20070827_193714.6.fits
;SP4D20070827_193728.6.fits - SP4D20070827_193800.8.fits
;SP4D20070827_193814.8.fits - SP4D20070827_193846.9.fits
;SP4D20070827_193901.1.fits - SP4D20070827_193933.2.fits

fnames=ssw_strsplit(files,'/',/tail)  ; 'files' output, sans full path
more,fnames(scninfo(0:9).ssstart) + ' - ' + fnames(scninfo(0:9).ssstop)            

; Sub-Demo 1.2 - Example: using above <scninfo>, <cat>, & <files> output, calibrate the 
;       set of slit scan images which for the 2nd map  above, e.g.

;IDL> scan=scninfo(1) & spname=time2file(scan.date_obs,/sec)
;IDL> more, spname & mk_dir, spname ; name by 1st scan image & make empty directory
;     20070827_193253 << Uniq SP Map identifier = <spname> for this demo.  

scan=scninfo(1) & spname=time2file(scan.date_obs,/sec)
more, spname & mk_dir, spname

;    <sp_prep> is now called to calibrate the Level0 images (4D->3D)
;    Although ssw clients are welcome to execute sp_prep, note that the
;    SOT/SP group intends to distribute Level1 and reconstructed stokes maps
;    If you want the maps but prefer to access pre-generated Maps, you
;    may  use the function sotsp_stks2struct to ingest the data on the 
;    SOT/SP server -> your sswidl client session - for example, to see
;    if the above map is already resident on the remote server, and 
;    transfer/read into into your sswidl session:
; 
;    IDL> stks=sotsp_stks2struct(spname) ; <spname> as derived above 
;   
;    If available, the output of the above is a structure containing the 
;    Maps (as described in 1.3 below)
;
;    Alternately, the ssw-client call to sp_prep call is:

;IDL> sp_prep,files(scan.ssstart:scan.ssstop),outdir=spname	; L0->L1

sp_prep,files(scan.ssstart:scan.ssstop),outdir=spname, /quiet

;    sp_prep calls will take a a few to many tens of minutes  
;    (One reason why SOT/SP data management baseline is to offer Level1 data)

; Sub-Demo 1.3 - Construct stokes maps from Level1 files created in 1.2 above

;IDL> stksimages_sbsp,spname,outstokes,outdir=spname ; Level1 -> "Level1.5" 
;IDL> help, stks, /str

stksimages_sbsp,spname,outstokes,outdir=spname ; Level1 -> "Level1.5"
stks_struct_local = ssw_save2struct(concat_dir(spname,'stksimoutput.save'))

; SOT/SP Demo 2.  L0 -> L1 -> Stokes maps for remote clients
;
; Assumes local SOT catalogs but no local Level0 data
;
; The process is very simular to Demo 1. with additional 
; level0 -> local sswidl client transfer step.  We take advantage
; of the optional /URLS switch so that the 'files' output of 
; sotsp_time2scan are urls rather than nfs file paths.
;
; Sub-Demo 2.1 Select desired SOT/SP remote server and then run SP scan/map
;     grouping function sotsp_time2scan with /URL switch

;IDL> hinode_server_select, /oslo  ; or /lmsal or /darts 
;IDL> scninfo=sotsp_time2scan('27-aug-2007 19:25','27-aug-2007 20:45',/span_day,cat,files,/urls) 

hinode_server_select, /oslo  ; or /lmsal or /darts
scninfo=sotsp_time2scan('27-aug-2007 19:25','27-aug-2007 20:45',/span_day,cat,files,/urls)

;IDL>  help,scninfo,cat,files
;SCNINFO         STRUCT    = -> <Anonymous> Array[92]  << same as 1.1 above
;CAT             STRUCT    = -> <Anonymous> Array[548] << same as 1.1 above
;FILES           STRING    = Array[548 ]               << L0 URLS

help,scninfo,cat,files

; Sub-Demo 2.2 Use 2.1 output to identify & transfer Level0 SOT/SP server-> local client
;     and then calibrate L0->L1 via sp_prep 

;IDL> scan=scninfo(1) & spname=time2file(scan.date_obs,/sec) ; same Map as 1.2
;IDL> more,spname & mk_dir,spname ; name by 1st scan image & make empty directory
;     20061212_035005  << Uniq SP Map identifier = <spname> for this demo.

scan=scninfo(1) & spname=time2file(scan.date_obs,/sec) + '_remote'
more, spname & mk_dir, spname

;IDL> l0dir = spname + '_l0' & mk_dir,l0dir  ; create target directory for L0 files
;IDL> sock_copy,files(scan.ssstart:scan.ssstop),out_dir=l0dir ; L0 -> local 
;IDL> sp_prep, file_search(l0dir,'*fits'),outdir=spname ; local L0 -> L1

l0dir = spname + '_l0' & mk_dir, l0dir		; create target directory for L0 files
sock_copy,files(scan.ssstart:scan.ssstop), out_dir=l0dir	; L0 -> local 
STOP
l0dir = l0dir(0)

sp_prep, file_search(l0dir,'*fits*'), outdir=spname, /quiet	; local L0 -> L1

; Sub-Demo 2.3 - Stokes map creation identical to 1.3 

stksimages_sbsp, spname, outstokes, outdir=spname ; Level1 -> "Level1.5"
stks_struct_remote = ssw_save2struct(concat_dir(spname,'stksimoutput.save'))

end
