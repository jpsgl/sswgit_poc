; SOT/SP Level0 -> Level1 -> Stokes maps
;
; SOT/SP Demo 1. - Assumes local Hinode/sot level0 data tree 
;
; 1.1 - find and group SP level0 images by map via sotsp_time2scan.pro
;       /span_day keyword links slit images for a map across "next UTDay"
;       The function sotsp_time2scan returns one structure per map-group
;       and includes tags required to subscript 'catalog' and 'files' 
;       (or 'urls') output parameters for calibration and map generation. 
;
IDL> scninfo=sotsp_time2scan('12-dec-2006','13-dec-2006',/span_day,cat,files)
IDL> help,scninfo,cat,files 
SCNINFO         STRUCT    = -> <Anonymous> Array[6]     ; Number of maps
CAT             STRUCT    = -> <Anonymous> Array[6251]  ; Catalog records
FILES           STRING    = Array[6251]                 ; L0 file paths/names

IDL> more,get_infox(scninfo,'date_obs,nrecs,ssstart,ssstop,macroid')
2006-12-12T00:00:03.573       210         0       209      7348
2006-12-12T03:50:05.857      1000       210      1209      7349
2006-12-12T10:10:08.680      2047      1210      3256      7350
2006-12-12T15:30:08.752       994      3257      4250      7351
2006-12-12T17:40:05.435      1000      4251      5250      7352
2006-12-12T20:30:05.255      1000      5251      6250      7353

;    Show 1st & last Level0 file names (1 slit scan per file) for each map
IDL> fnames=ssw_strsplit(files,'/',/tail)  ; 'files' output, sans full path
IDL> more,fnames(scninfo.ssstart) + ' - ' + fnames(scninfo.ssstop)
SP4D20061212_000003.5.fits - SP4D20061212_001316.5.fits
SP4D20061212_035005.8.fits - SP4D20061212_045709.0.fits 
SP4D20061212_101008.6.fits - SP4D20061212_131118.8.fits
SP4D20061212_153008.7.fits - SP4D20061212_163320.3.fits
SP4D20061212_174005.4.fits - SP4D20061212_184315.8.fits
SP4D20061212_203005.2.fits - SP4D20061212_213315.6.fits

; 1.2 - Example: using above <scninfo>, <cat>, & <files> output, calibrate the 
;       set of slit scan images which for the 2nd map  above, e.g.

IDL> scan=scninfo(1) & spname=time2file(scan.date_obs,/sec)
IDL> more,spname & mk_dir,spname ; name by 1st scan image & make empty directory
     20061212_035005  << Uniq SP Map identifier = <spname> for this demo.  

;    <sp_prep> is now called to calibrate the Level0 images (4D->3D)
;    Although ssw clients are welcome to execute sp_prep, note that the
;    SOT/SP group intends to distribute Level1 and reconstructed stokes maps
;    If you want the maps but prefer to access pre-generated Maps, you
;    may  use the function sotsp_stks2struct to ingest the data on the 
;    SOT/SP server -> your sswidl client session - for example, to see
;    if the above map is already resident on the remote server, and 
;    transfer/read into into your sswidl session:
; 
;    IDL> stks=sotsp_stks2struct(spname) ; <spname> as derived above 
;   
;    If available, the output of the above is a structure containing the 
;    Maps (as described in 1.3 below)
;
;    Alternately, the ssw-client call to sp_prep call is:
IDL> sp_prep,files(scan.ssstart:scan.ssstop),outdir=spname ; L0->L1

;    sp_prep calls will take a a few to many tens of minutes  
;    (One reason why SOT/SP data management baseline is to offer Level1 data)

; 1.3 - Construct stokes maps from Level1 files created in 1.2 above
IDL> stksimages_sbsp,spname,outstokes,outdir=spname ; Level1 -> "Level1.5" 

IDL> help,stks,/str
 
; SOT/SP Demo 2.  L0 -> L1 -> Stokes maps for remote clients
;                 Assumes local SOT catalogs but  No local Level0 data
;
; The process is very simular to Demo 1. with additional 
; level0 -> local sswidl client transfer step.  We take advantage
; of the optional /URLS switch so that the 'files' output of 
; sotsp_time2scan are urls rather than nfs file paths.
;
; 1.1 Select desired SOT/SP remote server and then run SP scan/map
;     grouping function sotsp_time2scan with /URL switch
;
IDL> hinode_server_select,/oslo  ; or /lmsal or /darts 
IDL> scninfo=sotsp_time2scan('12-dec-2006','13-dec-2006',/span_day,cat,files,/urls)
 
IDL>  help,scninfo,cat,files
SCNINFO         STRUCT    = -> <Anonymous> Array[6]     << same as 1.1 above
CAT             STRUCT    = -> <Anonymous> Array[6251]  << same as 1.1 above
FILES           STRING    = Array[6251]                 << L0 URLS

; 2.2 Use 2.1 output to identify & transfer Level0 SOT/SP server-> local client
;     and then calibrate L0->L1 via sp_prep 

IDL> scan=scninfo(1) & spname=time2file(scan.date_obs,/sec) ; same Map as 1.2
IDL> more,spname & mk_dir,spname ; name by 1st scan image & make empty directory
     20061212_035005  << Uniq SP Map identifier = <spname> for this demo.

IDL> l0dir=spname+'l0' & mk_dir,l0dir  ; create target directory for L0 files
IDL> sock_copy,files(scan.ssstart:scan.ssstop),out_dir=l0dir ; L0 -> local 
IDL> l0dir = l0dir(0)
IDL> sp_prep,file_search(l0dir,'*fits*'),outdir=spname ; local L0 -> L1

; 2.3 - Stokes map creation identical to 1.3 



