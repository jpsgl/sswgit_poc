FUNCTION get_xrt_expdur, index, unnormalized=unnormalized,           $
                         microseconds=microseconds, seconds=seconds, $
                         verbose=verbose, quiet=quiet,               $
                         qabort=qabort,   qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       GET_XRT_EXPDUR
;
; CATEGORY:
;       
;       Data information.
;
; PURPOSE:
;       
;       Retrieve the actual exposure duration of the image,
;       either the normalized value or the original measurement.
;       See Note #1.
;
; CALLING SEQUENCE:
;
;       Result = GET_XRT_EXPDUR( index [,/microseconds] [,/seconds] $
;                 [/unnormalized] [,/verbose] [,/quiet] [,/qstop]   $
;                 [,qabort=qabort] )
;       
; INPUTS:
;       
;       INDEX        - [Mandatory] (structure array, [Nimg])
;                      FITS header keywords. One structure per image.
;
; KEYWORDS:
;       
;	/MICROSECONDS - [Optional] (Boolean) Result has units of 
;                       microseconds. This keyword overrules "/seconds".
;       /SECONDS      - [Optional] (Boolean) Result has units of seconds.
;                       This keyword is overruled by "/microseconds".
;       /UNNORMALIZED - [Optional] (Boolean) The original, measured 
;                       ACTUAL exposure duration is returned, before any
;                       normalizations were applied to the data. 
;                       See Note #1.
;	/VERBOSE      - [Optional] (Boolean) If set, print out extra
;                       information. Overrides "/quiet" (see Note #2).
;       /QUIET        - [Optional] (Boolean) If set, suppress messages 
;                       (see Note #2).
;       /QSTOP        - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;       
;       Result        - [Mandatory] (double-float array, [Nimg])
;                       This is the measured time that the CCD was 
;                       actually exposed (E_ETIM for normal images,
;                       EXCCDEX for darks). This value compensates 
;                       for whether the image was performed with multiple 
;                       passes of the camera shutter. See Note #1.
;                       Default units are milliseconds.
;       QABORT        - [Optional] (Boolean) Indicates that the program
;                       exited gracefully without completing. (Might be
;                       useful for calling programs.)
;                       0: Program ran to completion.
;                       1: Program aborted before completion.
;
; EXAMPLES:
;
;       Print the exposure duration relative to the data, in millsec.
;       IDL> print, get_xrt_expdur(index)
;      
;       Print the original exposure duration of the data, in sec.
;       IDL> print, get_xrt_expdur(index, /unnormalized, /seconds)
;      
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) The measured exposure duration of the image data is stored
;          in the keyword E_ETIM for normal images and EXCCDEX for
;          darks. This is the default result returned by this program.
;          If <xrt_prep.pro> is used to normalize the data, then the
;          corresponding keyword value will be updated (to 1000000 microsec).
;          It is recommended that all user programs update this keyword
;          when they renormalize the data by any factor. The *original* 
;          exposure duration value can be recovered through other keywords,
;          using the "/unnormalized" switch.
;
;       2) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2006-May-11' ;--- (M.Weber (SAO)) Written. 
progver = 'v2008-Aug-20' ;--- (K.Reeves (SAO)) Added corrections for darks
;                      
;-
; =========================================================================


;=== Initial setup =======================================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (not q_vb)

  ;=== Initialize program constants.
  prognam='GET_XRT_EXPDUR'
  prognul='              '

  ;=== Announce version of <get_xrt_expdur.pro>.
  if q_vb then box_message, prognam+': Running ' + prognam + $
                            ' ' + progver + '.'

  ;=== Set some keyword Booleans.
  qstop   = keyword_set(qstop)
  qabort  = 0B
  q_micro = keyword_set(microseconds)
  q_sec   = keyword_set(seconds) and (not q_micro)
  q_unorm = keyword_set(unnormalized)


;=== Check inputs ========================================

  ;=== Check for number of inputs.
  if (n_params() ne 1) then begin
    if (not q_qt) then box_message, prognam+': Incorrect number of ' $
                                    + 'parameters. Aborting.'
    qabort = 1B
    return, 0B
  endif

  ;=== Check for input type.
  in_type = datatype(index)
  if (in_type ne 'STC') then begin
    if (not q_qt) then box_message, prognam+': Input type is not ' $ 
                                    + 'a structure. Aborting.'
    qabort = 1B
    return, 0B
  endif

  ;=== Check for necessary keywords.
  if not required_tags(index,'E_ETIM,E_ETIM_M,E_ETIM_E,EXCCDEX,E_SH_OPE,E_SH_CLO',$
                       missing=missing) then begin
    if (not q_qt) then box_message, prognam+': Input structure ' $ 
                   + 'does not have the necessary tags. Aborting.'
    qabort = 1B
    return, 0B
  endif


;=== Retrieve the appropriate value ======================


  ;;=== Dark exposure times are recorded in the exccdex keyword

  ndarks = where(index.ec_imtyp eq 1, complement = nnormal)

  expdur = dblarr(n_elements(index))
  clock_max = 1677721500

  if ndarks[0] ne -1 then begin 
     if q_unorm then begin
        expdur[ndarks] = index[ndarks].e_sh_clo - index[ndarks].e_sh_ope
     endif else begin
        expdur[ndarks] = double(index[ndarks].exccdex)
     endelse
     ;;=== Fix negative values due to counter rollover
     nneg = where(expdur lt 0)
     if nneg[0] ne -1 then $
        expdur[nneg] = clock_max - index[nneg].e_sh_ope + index[nneg].e_sh_clo
  endif

  if nnormal[0] ne -1 then begin
     if q_unorm then begin
        expdur[nnormal] = index[nnormal].e_etim_m * 4d * $
                          2d^(3d*index[nnormal].e_etim_e)
     endif else begin
        expdur[nnormal] = double(index[nnormal].e_etim)
     endelse
  endif

;=== Adjust units ========================================

  if (not q_micro) then begin
    if q_sec then expdur = expdur / 1d6 $
             else expdur = expdur / 1d3
  endif


;=== Finish ==============================================

if qstop then stop

  if n_elements(expdur) eq 1 then expdur = expdur[0]
  return, expdur


END ;======================================================================
