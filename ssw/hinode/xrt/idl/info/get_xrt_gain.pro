FUNCTION get_xrt_gain, input1, verbose=verbose, quiet=quiet, $
                       qabort=qabort, qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       GET_XRT_GAIN
;
; CATEGORY:
;       
;       Data information.
;
; PURPOSE:
;       
;       Retrieve the camera gain for the image (el per DN).
;
; CALLING SEQUENCE:
;
;       Result = GET_XRT_GAIN( input1 [,/verbose] [,/quiet] $
;                              [,/qstop] [,qabort=qabort]    )
;       
; INPUTS:
;       
;       INPUT1   - [Mandatory] (structure array, [Nimg])
;                           OR (string array, [Nimg])
;                  There are two possible formats for INPUT1:
;                  a) An XRT index-type structure which corresponds
;                     to the image header keywords in an XRT FITS file.
;                  b) A string whose elements are members of the set
;                     {'L', 'LEFT', 'R', 'RIGHT', 'DEFAULT'}.
;                  "LEFT" and "RIGHT" refer to two possible read-out
;                  ports on the XRT CCD. "DEFAULT" will return the
;                  value to use if the port ID is not clear.
;
; KEYWORDS:
;       
;	/VERBOSE - [Optional] (Boolean) If set, print out extra
;                  information. Overrides "/quiet" (see Note #1).
;       /QUIET   - [Optional] (Boolean) If set, suppress messages 
;                  (see Note #1).
;       /QSTOP   - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;       
;       Result   - [Mandatory] (float array, [Nimg])
;                  Units = 'electrons per 12-bit ADU (DN)'.
;                  This is the XRT flight camera gain, as measured in
;                  the laboratory. This ratio is used to convert units
;                  of XRT data between DN (DataNumbers) and 
;                  'el' (CCD electrons). If the program must abort 
;                  gracefully (see QABORT), then a scalar -1.0 is 
;                  returned. For a set of inputs in which some elements 
;                  are recognized and some are not, a "gain" value of 
;                  -1.0 will be given for the unrecognized inputs.
;       QABORT   - [Optional] (Boolean) Indicates that the program
;                  exited gracefully without completing. (Might be
;                  useful for calling programs.)
;                  0: Program ran to completion.
;                  1: Program aborted before completion.
;
; EXAMPLES:
;
;       Print the gain for a set of images in "index" and "data":
;       IDL> print, get_xrt_gain(index)
;      
;       Print the gain for a specific CCD port:
;       IDL> print, get_xrt_gain('R')
;      
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2006-May-21' ;--- (M.Weber (SAO)) Written. 
;                      
;-
; =========================================================================


;=== Initial setup =======================================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (not q_vb)

  ;=== Initialize program constants.
  prognam='GET_XRT_GAIN'
  prognul='            '
  gain_l = 58.8d  ;; electrons per 12-bit ADU (DN)
  gain_r = 59.1d  ;; electrons per 12-bit ADU (DN)
  gain_d = gain_r ;; default is set to Right CCD port.

  ;=== Announce version of the program.
  if q_vb then box_message, prognam+': Running ' + progver + '.'

  ;=== Set some keyword Booleans.
  qstop   = keyword_set(qstop)
  qabort  = 0B


;=== Check inputs and make output ========================

  ;=== Check for number of inputs.
  if (n_params() ne 1) then begin
    if (not q_qt) then box_message, prognam+': Incorrect number of ' $
                                    + 'parameters. Aborting.'
    qabort = 1B
    return, 0B
  endif

  output = fltarr(n_elements(input1)) - 1.

  ;=== Check for input type.
  in_type = datatype(input1)
  case 1 of
    (in_type eq 'STC') : begin
                           if not required_tags(input1, 'CCD_READ', $
                                         missing=missing) then begin
                             if (not q_qt) then box_message, prognam + $
                               ': Input structure does not have the '  $
                               + 'necessary tags. Aborting.'
                             qabort = 1B
                             return, -1L
                           endif

                           ssl = where(input1.ccd_read eq 0, cntl)
                           ssr = where(input1.ccd_read eq 1, cntr)

                         end

    (in_type eq 'STR') : begin
                           ssl = where((strupcase(input1) eq 'L') or $
                                      (strupcase(input1) eq 'LEFT'), cntl)
                           ssr = where((strupcase(input1) eq 'R') or $
                                      (strupcase(input1) eq 'RIGHT'), cntr)
                           ssd = where((strupcase(input1) eq 'DEFAULT'), $
                                                                      cntd)
                         end

     else              : begin
                           if (not q_qt) then box_message, prognam + $
                             ': Input type is not a structure or a ' $
                             + 'string. Aborting.'
                           qabort = 1B
                           return, -1L
                         end
     endcase


;=== Retrieve the appropriate value ======================

  if (cntl ge 1) then output[ssl] = gain_l
  if (cntr ge 1) then output[ssr] = gain_r
  if (cntd ge 1) then output[ssd] = gain_d
  output = output[0]


;=== Finish ==============================================

if qstop then stop

  return, output


END ;======================================================================
