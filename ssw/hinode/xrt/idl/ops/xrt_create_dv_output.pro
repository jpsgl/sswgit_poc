pro xrt_create_dv_output, ecid_list, missing_idx, prsnt_idx, index, ss_outliers

; =========================================================================
;		
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;
;       XRT_CREATE_DV_OUTPUT
;
; CATEGORY:
;
;       XRT Data Verification utilities
;
;
; PURPOSE:
;
;       Print to the screen a list of missing XRT images in a given time interval
;
;
; CALLING SEQUENCE:
;
;       XRT_CREATE_DV_OUTPUT, ecid_list, missing_idx, prsnt_idx, index
;
; INPUTS:
;
;       ecid_list: 1D list of EC_IDs output by xrt_id_ecids
;       missing_idx: list of index numbers for ecid_list where images
;                    are missing
;       prsnt_idx: list of index numbers for ecid_list where images
;                    are present
;       index: index structures for present images
;
;
; COMMON BLOCKS:
;
; none
;
; PROCEDURE:
;
;     This routine is a subroutine of xrt_missing_images that formats
;     the output info on missing images in the usual way for the DV report.
;
; KNOWN PROBLEMS:
;
;     None.
;
; NOTES:
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       directed to this email address:
;                xrt_manager ~at~ head.cfa.harvard.edu
;
;
;
; MODIFICATION HISTORY:
; 
progver='v2011.Feb.10'     ;--- K. Reeves written
progver='v2011.Mar.09'     ;--- K. Reeves, SAO, inserted a check for more 
;                               than one image in test for image split
;                               into two files.
progver='v2011.Oct.10'     ;--- K. Reeves, SAO, inserted some rounding in case ecid_list is not an integer.
;
;-
; =========================================================================

  ;; report images with incorrect time stamps

  n_outliers = n_elements(ss_outliers)
  if ss_outliers[0] ne -1 then print, string(n_outliers,format='(i3)')+$
                                 ' image(s) with incorrect time stamps at '+$
                                 anytim(index[ss_outliers].date_obs,/vms)

  ;; This part is from the old xrt_dv_find_missing_imgs, and it finds
  ;; images split into two files

  ;compute the difference between successive IDs
  ;only necessary if there is more than one image
  
  if n_elements(index) gt 1 then begin
     ecid_diff = shift(index.ec_id,-1) - index.ec_id

     ;;remove meaningless difference between last and first element 
     ecid_diff = ecid_diff[0:n_elements(ecid_diff)-2]

     ind0=where(ecid_diff EQ 0,count0)
  
     if count0 gt 0 then begin 
        string='  1 image,  EC_ID: '+string(index[ind0].ec_id,format='(i6)')+ $
               ' is divided in two files '+anytim(index[ind0].date_obs,/vms)+$
               ' '
        print, string
     endif
  endif

  match_diff = shift(missing_idx, -1) - missing_idx

  ;; if there are missing images, then find times, list number
  ;; of images

  if missing_idx[0] ne -1 then begin
     count=1
     missing_idx_last = missing_idx[0]
     for i=0,n_elements(missing_idx)-1 do begin
        
        ;; find low and high end of ec_id gap so that times
        ;; bounding the gap can be reported

        n_low = max(where(prsnt_idx lt missing_idx[i]))
        if n_low eq -1 then n_low = 0

        ind_match_low  = where(strmatch(index.ec_id, $
                                        round(ecid_list[prsnt_idx[n_low]])) eq 1)
        starttimes = index[ind_match_low].date_obs
        
        n_high = min(where(prsnt_idx gt missing_idx[i]))
        ind_match_high=where(strmatch(index.ec_id, $
                                      round(ecid_list[prsnt_idx[n_high]])) eq 1)
        endtimes = index[ind_match_high].date_obs
        
        ;; increment count of missing images in a row if missing_idx
        ;; and missing_idx_last are consecutive

        if missing_idx_last - missing_idx[i] eq -1 then count = count+1 

        ;; lots of string-massaging to make the output look pretty

        if count eq 1 then begin 
           img_str = 'image,  ' 
           ecid_str = string(ecid_list[missing_idx[i]],format='(i6)')
        endif else begin 
           img_str = 'images, '
           if match_diff[i] ne 1 then $
              ecid_str = ecid_str + ' - '+ $
                         string(ecid_list[missing_idx[i]],format='(i6)')
        endelse

        string=string(count,format='(i3)')+' '+img_str+'EC_ID: '+$
               ecid_str + ' missing between ' $
               +anytim(starttimes,/vms)+' and '+anytim(endtimes,/vms)

        ;; set missing_idx_last to current value of missing_idx for next
        ;; time around
        missing_idx_last = missing_idx[i]
        
        ;; if match_diff ne 1 then the we've reached the
        ;; end of a sequence of consecutive images, so string is output

        if match_diff[i] ne 1 then begin
           print, string
           count=1
        endif
     endfor

  endif
end
