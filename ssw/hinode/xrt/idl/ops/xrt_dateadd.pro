FUNCTION xrt_dateadd,             $
                     time_in,     $
                     days_addend, $
                     qstop=qstop

  qstop = keyword_set(qstop)

  time_ex_in = anytim(time_in, /ex)

  mjd_in = date2mjd(time_ex_in[6], time_ex_in[5], time_ex_in[4])

  mjd_out = mjd_in + long(days_addend)

  time_str_out = mjd2str(mjd_out)

  return, time_str_out

END
