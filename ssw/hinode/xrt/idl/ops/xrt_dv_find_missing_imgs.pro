pro xrt_dv_find_missing_imgs, start_time, $
                              end_time, $
                              xrtfiledir=xrtfiledir, timeline_dir = timeline_dir

; =========================================================================
;		
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;
;       XRT_DV_FIND_MISSING_IMGS
;
; CATEGORY:
;
;       XRT Data Verification utilities
;
;
; PURPOSE:
;
;       Print to the screen a list of missing XRT images in a given time interval
;
;
; CALLING SEQUENCE:
;
;       XRT_DV_FIND_MISSING_IMGS,start_time,end_time 
;                               [,xrtfiledir=xrtfiledir]
;
; INPUTS:
;
;       start_time: (string or double) start of the time
;                   interval that is checked for missing images. Can be in 
;                   any format accepeted by ANYTIM.
;       end_time:   (string or double) end of the time
;                   interval that is checked for missing images. Can be in 
;                   any format accepeted by ANYTIM.
;
; OPTIONAL INPUTS:
;
;       xrtfiledir: (string) Root of the directory trees where XRT
;       images reside. The default is '/archive/hinode/xrt/'. 
;       Directory structure is assumed to be in the form 
;       xrtfiledir+'QLraw/yyyy/mm/dd/Hxx00/'
;
;       timeline_dir: (string) root of directory where XRT timelines 
;                      reside.  The default is '/archive/hinode/xrt/timelines/'. 
;                      Directory structure is assumed to be in the form 
;                      timeline_dir + /yyyy/mm/dd/'
;
; EXAMPLE:
;
;       find all missing images between 2008/10/03 10:00 UT and
;             2008/10/04 10:00 UT
;       xrt_dv_find_missing_imgs,'03-OCT-2008 10:00','04-OCT-2008 10:00'
;
; COMMON BLOCKS:
;
; none
;
; PROCEDURE:
;
;     This routine is a wrapper for xrt_missing_imgs, which calls
;     xrt_id_ecids to do the heavy lifting.  See the comments in those
;     programs for algorithmic details.
;
; KNOWN PROBLEMS:
;  
;  1) When the program is started in the middle of a time period where
;  no data was intentially taken, an error message is returned: 
;
;  ------------------------------------------------------------
; | No times between t0 and t1, returning record closest to t0 |
; | DeltaT (catrec,t0) = 25381 seconds                         |
;  ------------------------------------------------------------
;  ------------------------------------------
; | No XRT records meet time/search criteria |
;  ------------------------------------------
; 
;  The solution is to expand the time range of the search.
;
;
; NOTES:
;    Warning: since this program reads in the FITS file header, 
;    it will be slow for time intervals spanning more than a few days,
;    where a sizable number of FITS header needs to be read in.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       directed to this email address:
;                xrt_manager ~at~ head.cfa.harvard.edu
;
;
;
; MODIFICATION HISTORY:
; 
progver='v2011.Feb.10'     ;--- K. Reeves written
;
progver='v2011.Jul.22'     ;--- K. Reeves, added timeline_dir keyword
;
;
;-
; =========================================================================

  
  xrt_missing_imgs, start_time, end_time, /dv, archive_prefix = xrtfiledir


end
