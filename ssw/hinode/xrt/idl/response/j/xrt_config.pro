FUNCTION xrt_config, cal = cal

; =========================================================================
;+
; PROJECT:
;
;       Solar-B / XRT 
;
; NAME:
;       
;       XRT_CONFIG
;
; CATEGORY:
;       
;       XRT RESPONSE
;
; PURPOSE:
;
;       Get the information about the XRT configuration
;
; CALLING SEQUENCE:
;
;       result = XRT_CONFIG()
;       
; INPUTS:
;       
;       none
;
; KEYWORDS:
;       
;       CAL - [Optional input] If set as "cal = 1", the factor is calculated based on the calibration in Narukage et al. (2011).
;                              If set as "cal = 2", the factor is calculated based on the calibration in Narukage et al. (2013).
;                              Default is "cal = 2" to use the latest calibration result of Narukage et al. (2013).
;
; OUTPUTS:
;       
;       return - (structure array) information about the XRT configuration
;
; EXAMPLES:
;
;       IDL> config = xrt_config()
;
; COMMON BLOCKS:
;
;       none 
;
; NOTES:
;
;       The detail of calibrated filter thickness of the Hinode/XRT is described in
;         Narukage et al. 2011, Solar Phys., 269, 169.
;         http://adsabs.harvard.edu/doi/10.1007/s11207-010-9685-2
;       and
;         Narukage et al. 2013, Solar Phys., in press
;         http://adsabs.harvard.edu/doi/10.1007/s11207-013-0368-7
;       These two papers are the reference papers of this program.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                noriyuki.narukage ~at~ nao.ac.jp
;       
; MODIFICATION HISTORY:
;
progver = 'v2013-Oct-31' ;--- (N.Narukage (ISAS/JAXA)) Written.
;
;-
; =========================================================================

; --- read filter config data ---------------------------------------------

  if not keyword_set(cal) then cal = 2
  if (cal ne 1) and (cal ne 2) then cal = 2

  ps = path_sep()
  file_prefix = get_logenv('$SSW_XRT') + ps + 'idl' + ps + 'response' + ps + 'j' + ps
  if file_test('/home/flare/all/naru/xrt/ssw/response/j/')      then file_prefix = '/home/flare/all/naru/xrt/ssw/response/j/'
  if file_test('/Users/noriyuki/work/xrt/test_ssw/response/j/') then file_prefix = '/Users/noriyuki/work/xrt/test_ssw/response/j/'

  restgenx, config, file = file_prefix + 'database' + ps + 'XRT' + ps + 'XRT_CONFIG' + '_cal' + string(cal, format='(i1)')


  return, config

END
