FUNCTION xrt_calc_att, material, thickness, lambda, kev=kev, oxide_thickness=oxide_thickness, info = info

; =========================================================================
;+
; PROJECT:
;
;       Solar-B / XRT 
;
; NAME:
;       
;       XRT_CALC_ATT
;
; CATEGORY:
;       
;       XRT RESPONSE
;
; PURPOSE:
;
;       Get the attenuation of material
;       using the HENKE http://henke.lbl.gov/optical_constants/ .
;
; CALLING SEQUENCE:
;
;       result = XRT_CALC_ATT(material, thickness, lambda)
;       
; INPUTS:
;       
;       material  - (string) name of material
;       thickness - (float array) thickness of polyimide [A]
;       LAMBDA    - (float array) Wave length [A]
;
; KEYWORDS:
;       
;       none
;
; OUTPUTS:
;       
;       return - (float array) estimated attenuation of the material
;
; EXAMPLES:
;
;       IDL> lambda = findgen(400)+1.
;       IDL> att = xrt_calc_att('polyimide', 1000., lambda)
;
; COMMON BLOCKS:
;
;       none 
;
; NOTES:
;
;       This program requires the data file "polyimide.att" calculated
;       by HENKE (http://henke.lbl.gov/optical_constants/).
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                noriyuki.narukage ~at~ nao.ac.jp
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-Sep-10' ;--- (N.Narukage (ISAS/JAXA)) Written.
;
;-
; =========================================================================

; --- warning -------------------------------------------------------------

  if n_elements(thickness) ge 2 then begin
    print
    print, '*****  WARNING from XRT_CALC_ATT.PRO  *************************************'
    print, 'The inputed "THICKNESS" should not be array.'
    print, 'Then the case of first elements of inputted array is only processed.'
    print, '*************************************  WARNING from XRT_CALC_ATT.PRO  *****'
    print
  endif

  thickness = thickness[0]

; --- read filter config data ---------------------------------------------

  ps = path_sep()
  file_prefix = get_logenv('$SSW_XRT') + ps + 'idl' + ps + 'response' + ps + 'j' + ps
  if file_test('/home/flare/all/naru/xrt/ssw/response/j/') then file_prefix = '/home/flare/all/naru/xrt/ssw/response/j/'

  dbdir = file_prefix + 'database' + ps + 'att_len' + ps + 'nm' + ps
  if keyword_set(kev) then dbdir = file_prefix + 'database' + ps + 'att_len' + ps + 'ev' + ps


  if not keyword_set(oxide_thickness) then oxide_thickness=0.

  for j=0, (oxide_thickness ne 0) do begin

    if j eq (oxide_thickness ne 0) then begin
      mat = material
    endif else begin
      if material eq 'Al' then begin & mat = 'Al2O3' & r = (26.98)*2./(26.98*2.+16.00*3.) & endif
      if material eq 'Ti' then begin & mat = 'TiO2'  & r = (47.87)*1./(47.87*1.+16.00*2.) & endif
      if material eq 'Be' then begin & mat = 'BeO'   & r = ( 9.01)*1./( 9.01*1.+16.00*1.) & endif
      if material eq 'Cu' then begin & mat = 'CuO'   & r = (63.55)*1./(63.55*1.+16.00*1.) & endif
      if material eq 'Cr' then begin & mat = 'Cr2O3' & r = (52.00)*2./(52.00*2.+16.00*3.) & endif
      if r eq 0 then print, ' ********** Input oxide '+material+' data!! **********'
    endelse

    file = dbdir + mat + '.att'

    ascii = rd_ascii(file)
    head = ascii[0:1]

    temp = strsplit(head[0], '=', /ext)
    temp = strsplit(temp[1], ',', /ext)

    if j eq (oxide_thickness ne 0) then begin
      tkn = 1d * thickness

      if (oxide_thickness ne 0) then begin
        tkn = 1d * thickness - 1d*oxide_thickness*r*oxide_density/(temp[0]*1.)

        if keyword_set(info) then begin
          print, mat+' thickness = ', thickness, ' - ', 1d*oxide_thickness*r*oxide_density/(temp[0]*1.), ' = ', tkn 
          print
          info = tkn
        endif
      
      endif

    endif else begin
      tkn = 1d*oxide_thickness

      oxide_density = temp[0]*1.

      if keyword_set(info) then begin
        print, mat+' thickness = ', oxide_thickness
        print
      endif
    
    endelse

    n = n_elements(ascii)-2
    data = strarr(n, 2)
    for i=0,n-1 do begin
      temp = STRSPLIT( ascii[i+2], '[ ' + STRING(9B) + ']+', /EXTRACT )
      data[i,*] = temp[0:1]
    endfor

    if not keyword_set(kev) then begin
      wl = data[*,0]*10d ; Wavelength [A]
    endif else begin
      wl = data[*,0]/1000d ; Wavelength [keV]
    endelse

    al = data[*,1]*1d4 ; Atten Length [A]

    att = exp(-tkn/al) ; attenuation

; --- intrpolate ----------------------------------------------------------

    temp = INTERPOL(att, wl, lambda) < 1 > 0

; --- return result -------------------------------------------------------

    if j eq 0 then res = temp else res = res * temp

  endfor


  return, res

END
