FUNCTION xrt_time2contam, fw1, fw2, time, index=index, ccd=ccd, filter=filter, message = message

; =========================================================================
;+
; PROJECT:
;
;       Solar-B / XRT 
;
; NAME:
;       
;       XRT_TIME2CONTAM
;
; CATEGORY:
;       
;       XRT CONTAMINATION
;
; PURPOSE:
;
;       Get the estimated thickness of contamination.
;
; CALLING SEQUENCE:
;
;       result = XRT_TIME2CONTAM(fw1, fw2, time)
;       
; INPUTS:
;       
;       FW1    - (long) Filter No. on filter wheel 1.
;       FW2    - (long) Filter No. on filter wheel 2.
;       TIME   - (string array) Time when you want to calcurate the XRT effective area,
;                because XRT effective area influenced by contamination is a function of time.
;                This time is used to derive the thickness of contaminant.
;                If you directly give the contamination thickness with CONTAMINATION_THICKNESS
;                keyword, you should omit this TIME input.
;
; KEYWORDS:
;       
;       INDEX  - [Optional input] (structure) If set, you can omit the inputs of FW1, FW2 and TIME.
;       CCD    - [Optional output] (float) Conatmination thickness on CCD will be returned.
;       FILTER - [Optional output] (float) Total conatmination thickness on filters
;                mounted on FW1 and FW2 will be returned.
;
; OUTPUTS:
;       
;       return - (float array) estimated thickness of contamination [Angstrom]
;
; EXAMPLES:
;      
;       Get total contamination thickness on thin-Al-poly filter and CCD
;       at 11-Mar-2007 00:55:00:
;       IDL> thickness = xrt_time2contam(1, 0, '2007/03/11 00:55:00')
;       or
;       IDL> thickness = xrt_time2contam(1, 0, '11-Mar-2007 00:55:00')
;
;       Get total contamination thickness on thin-Al-poly filter and CCD
;       with index:
;       IDL> thickness = xrt_time2contam(index=index)
;
; COMMON BLOCKS:
;
;       none 
;
; NOTES:
;
;       This version covers data from 2006/09/22 to within the past
;       month or so. The database is updated after each XRT CCD
;       bakeout operation, which occur approximately every three
;       to four weeks.  This subroutine will not work for data more
;       recent than that.
;
;       The detail of how to estimate the thickness of contaminant is described in
;       Narukage et al. 2011, Solar Phys., 269, 169.
;       http://adsabs.harvard.edu/doi/10.1007/s11207-010-9685-2
;       This is the reference paper of this program.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                noriyuki.narukage ~at~ nao.ac.jp 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-Sep-07' ;--- (N.Narukage (ISAS/JAXA)) Written.
progver = 'v2008-Dec-08' ;--- (N.Narukage (ISAS/JAXA)) Updated to read
;                             the database of contamination thickness on CCD.
progver = 'v2008-Dec-09' ;--- (M.Weber (SAO/NASA)) Updated the NOTES to reflect
;                             the span of data that can now be corrected.
progver = 'v2009-Jul-27' ;--- (N.Narukage (NAOJ)) Updated to read the database of
;                             contamination thickness on focal-plane analysis filters.
progver = 'v2011-Feb-17' ;--- (N.Narukage (NAOJ)) Added the information on the reference paper.
progver = 'v2013-Nov-04' ;--- (N.Narukage (NAOJ)) Modified for the message from the program.
;
;-
; =========================================================================

  ps = path_sep()
  file_prefix = get_logenv('$SSW_XRT') + ps + 'idl' + ps + 'response' + ps
  if file_test('/home/flare/all/naru/xrt/ssw/response/') then file_prefix = '/home/flare/all/naru/xrt/ssw/response/'

; --- setting for message -------------------------------------------------

  if not keyword_set(message) then message = ['XRT_TIME2CONTAM']

; --- process of option ---------------------------------------------------

  if keyword_set(index) then begin
    time = anytim(index.date_obs, /ecs)
    fw1 = index.ec_fw1
    fw2 = index.ec_fw2
  endif

  if n_elements(uniq(fw1)) ge 2 or n_elements(uniq(fw2)) ge 2 then begin
    fw1  = fw1[0]
    fw2  = fw2[0]
    time = time[0]

    message = [message, '']
    message = [message, '*****  WARNING from XRT_TIME2CONTAM.PRO  **********************************']
    message = [message, 'The inputted filter pairs (FW1 and FW2) should be unique.']
    message = [message, 'However, your inputted filters contain some kinds of filter pairs.']
    message = [message, 'Then the case of first filter pair is only processed.']
    message = [message, '**********************************  WARNING from XRT_TIME2CONTAM.PRO  *****']
  endif

  if n_elements(time) eq 0 then begin
    time = fw1
    fw1 = 0
    fw2 = 0

    message = [message, '']
    message = [message, '*****  WARNING from XRT_TIME2CONTAM.PRO  **********************************']
    message = [message, 'XRT_TIME2CONTAM.PRO has been updated']
    message = [message, '    to return the total contaminant thickness on CCD and analysis filters.']
    message = [message, 'However, your inputed parameter is obeyed the format of old version.']
    message = [message, 'Check the manual of XRT_TIME2CONTAM.PRO.']
    message = [message, 'In this process,']
    message = [message, '    the contaminant thickness only on CCD is returned']
    message = [message, '    in the same way as old version.']
    message = [message, '**********************************  WARNING from XRT_TIME2CONTAM.PRO  *****']
  endif


  s_time = anytim(time)
  
; --- intrpolate on CCD ---------------------------------------------------

  restgenx, dbver_ccd, s_ref_time_ccd, thickness_ccd, file = file_prefix + 'contam' + ps + 'xrt_contam_on_ccd'
  
  ccd = fix(interpol(thickness_ccd, s_ref_time_ccd, s_time)+0.5)*1.

; --- intrpolate on filter ------------------------------------------------

  filter_fw1 = 0.
  filter_fw2 = 0.

  restgenx, dbver_filter, s_ref_time_filter, thickness_filter, file = file_prefix + 'contam' + ps + 'xrt_contam_on_filter'

  if fw1 lt 0 or fw1 gt 5 then begin
    res = -1.
    ccd = -1.
    filter = -1.
    print, 'FW1 keyword should be 0, 1, 2, 3, 4 or 5.'
    goto, jump_end
  endif
  filter_fw1 = interpol(thickness_filter[*, 0, fw1], s_ref_time_filter, s_time)

  if fw2 lt 0 or fw2 gt 5 then begin
    res = -1.
    ccd = -1.
    filter = -1.
    print, 'FW2 keyword should be 0, 1, 2, 3, 4 or 5.'
    goto, jump_end
  endif
  filter_fw2 = interpol(thickness_filter[*, 1, fw2], s_ref_time_filter, s_time)

  filter = fix(filter_fw1+0.5)*1. + fix(filter_fw2+0.5)*1.

; --- intrpolate on CCD ---------------------------------------------------

  res = ccd + filter

; --- check the input time ------------------------------------------------

  n = where(s_time gt max(s_ref_time_ccd), count)
  if count ne 0 then begin
    message = [message, '']
    message = [message, '*****  WARNING from XRT_TIME2CONTAM.PRO  **********************************']
    message = [message, 'Contamination thickness on CCD after '+ anytim(s_ref_time_ccd[n_elements(s_ref_time_ccd)-1], /ecs)]
    message = [message, '    CANNOT be estimated in this version ('+dbver_ccd.ver+').']
    message = [message, 'Please wait for the update of database.']
    message = [message, '**********************************  WARNING from XRT_TIME2CONTAM.PRO  *****']

	res[n] = -1.
    ccd[n] = -1.
    filter[n] = -1.
  endif

; --- return result -------------------------------------------------------
jump_end:

  if n_elements(message) ge 2 then begin
    if message[0] eq 'XRT_TIME2CONTAM' then hprint, [message[1:*], '']
  endif

  return, res

END
