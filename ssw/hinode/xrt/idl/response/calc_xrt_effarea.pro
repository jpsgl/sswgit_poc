FUNCTION calc_xrt_effarea, trans_file=trans_file, qabort=qabort

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       CALC_XRT_EFFAREA
;
; CATEGORY:
;       
;       Instrument configuration
;
; PURPOSE:
;       
;       Get the XRT effective areas [cm^2] as a function of wavelength [A]
;       for every x-ray channel, for the flight configuration.
;       WARNING: This routine has been deprecated. See Note #1.
;
; CALLING SEQUENCE:
;
;       Result = CALC_XRT_EFFAREA([,qabort=qabort])
;       
; INPUTS:
;       
;       No parameters.
;
; KEYWORDS:
;
;       No keyword switches.
;
; OUTPUTS:
;
;       Result - [Mandatory] (structure array)
;                A structure array giving the XRT effective areas [cm^2]
;                as a function of wavelength [A] for every x-ray channel.
;                Each structure represents one channel. See examples.
;       QABORT - [Optional] (Boolean) Indicates that the program
;                exited gracefully without completing. (Might be
;                useful for calling programs.)
;                0: Program ran to completion.
;                1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> effarea = calc_xrt_effarea() 
;       IDL> help, effarea, /st
;       ** Structure <26677f4>, 7 tags, length=40052, data length=40052, refs=1:
;          TYPE            STRING    'eff_area'
;          CHANNEL_NAME    STRING    
;          WAVE            FLOAT     Array[5000]
;          WAVE_UNITS      STRING    'Angstroms'
;          EFF_AREA        FLOAT     Array[5000]
;          EFF_AREA_UNITS  STRING    'cm^2'
;          LENGTH          LONG     
;          DATA_FILES      STRING  
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) This routine has been deprecated. It is not compatible with 
;          the most recent calibrations, and does not account for the 
;          CCD contamination layer. The program <make_xrt_wave_resp.pro>
;          should be used instead.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-May-15' ;--- (M.Weber) Written. (Based heavily on
;                             earlier work, though.)
progver = 'v2008-Oct-01' ;--- (M.Weber) This routine has been
;                             deprecated. See Note #1.
;                      
;-
; =========================================================================


; === Initial setup ==============================

  prognam = 'CALC_XRT_EFFAREA'

  ;=== Set some keyword Booleans.
  qabort = 0B


; === Get channel transmissions ==================

  chn_trans = get_xrt_chn_trans(trans_file=trans_file, qabort=qabort)
  if qabort then begin
    return, 0
  endif


; === Get geometric aperture area ================

  geostr = get_xrt_aperture(/as_structure, qabort=qabort)
  if qabort then begin
    return, 0
  endif


; === Generate effective area ====================

  effarea = {type:'eff_area', channel_name:'', wave:fltarr(5000), $
             wave_units:'', eff_area:fltarr(5000),                $ 
             eff_area_units:'', length:1L, data_files:''           }
  effarea = replicate(effarea, n_elements(chn_trans))

  for ii = 0,(n_elements(chn_trans)-1) do begin
    len = chn_trans[ii].length

    effarea[ii].channel_name      = chn_trans[ii].channel_name
    effarea[ii].wave[0:len-1]     = chn_trans[ii].wave[0:len-1]
    effarea[ii].wave_units        = chn_trans[ii].wave_units
    effarea[ii].eff_area[0:len-1] = chn_trans[ii].trans[0:len-1] $
                                    * geostr.area
    effarea[ii].eff_area_units    = geostr.units
    effarea[ii].length            = chn_trans[ii].length
    effarea[ii].data_files        = chn_trans[ii].data_files[0]
  endfor


; === Finish =====================================

  return, effarea


END ;======================================================================
