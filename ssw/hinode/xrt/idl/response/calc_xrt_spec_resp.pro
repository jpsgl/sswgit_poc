FUNCTION calc_xrt_spec_resp, trans_file=trans_file, qabort=qabort, $
                             qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       CALC_XRT_SPEC_RESP
;
; CATEGORY:
;       
;       Instrument response
;
; PURPOSE:
;
;       Produce the spectral response for each XRT x-ray channel per
;       incident photon, as a function of wavelength.
;       WARNING: This routine has been deprecated. See Note #3.
;
; CALLING SEQUENCE:
;
;       Result = CALC_XRT_SPEC_RESP([,qabort=qabort] [,/qstop])
;       
; INPUTS:
;       
;       No parameters.
;
; KEYWORDS:
;
;       /QSTOP - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;
;       Result - [Mandatory] (structure array, [Nchannels])
;                The spectral response for each XRT x-ray channel per
;                incident photon, as a function of wavelength.
;                Units = 'el cm^2 sr ph^-1 pix^-1'.
;                See Note #1.
;                Here is a description of the "spec_resp" structure.
;                  TYPE            STRING    'spec_resp'
;                  CHANNEL_NAME    STRING  
;                  WAVE            FLOAT     Array[5000]
;                  WAVE_UNITS      STRING    'Angstroms'
;                  SPEC_RESP       FLOAT     Array[5000]
;                  SPEC_RESP_UNITS STRING    'el cm^2 sr ph^-1 pix^-1'
;                  LENGTH          LONG     
;                  DATA_FILES      STRING    
;                TYPE = 'spec_resp', common to all XRT spectrum-response
;                structures.
;                LENGTH = N_wavelengths (may be less than 5000); also
;                applies to SPEC_RESP.
;       QABORT - [Optional] (Boolean) Indicates that the program
;                exited gracefully without completing. (Might be
;                useful for calling programs.)
;                0: Program ran to completion.
;                1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> s_resp = calc_xrt_spec_resp()
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) A discussion of units.
;          It is easier to understand these units in the larger context.
;          For a spectral model S, such as is returned by 
;          <get_xrt_spec_genx>:  S(wave, T) ~ [ph cm^3 s^-1 sr^-1 A^-1].
;          For a spectral response R, such as this program returns:
;          R(wave) ~ [el cm^2 sr ph^-1 pix^-1], which says something about
;          how many electrons are generated in the CCD for a photon of
;          a given wavelength. Then:
;          S(wave, T) * R(wave) ~ [el A^-1 s^-1 pix^-1 (cm^-5)^-1]
;                               ~ [el A^-1 s^-1 pix^-1 EM^-1],
;          where EM ~ [cm^-5] is the "line of sight" (or "column")
;          emission measure. For a channel's temperature response F{T):
;          F(T) = integrate{ S(wave, T) * R(wave) * d(wave)}
;               ~ [el s^-1 pix^-1 EM^-1]
;          Note that one must assume a spectral model to calculate a
;          temperature response.
;
;          Going just a bit further, for an EM at T = T0, the signal G
;          observed is
;          G|T0 = F(T0) * EM|T0 ~ [el s^-1 pix^-1].
;          For an emission measure continuously distributed over a range
;          of temperatures T, one uses the differential emission measure:
;          DEM(T) ~ [cm^-5 K^-1].
;          Now the net signal G is
;          G = integrate{F(T) * DEM(T) * dT} ~ [el s^-1 pix^-1].
;
;       2) The solid angle L subtended by a pixel in a telescope:
;          L = (pixel area)/(focal length)^2 ~ [sr pix-1].
;          (L is a dimensionless number.)
;
;       3) This routine has been deprecated. It is not compatible with the
;          most recent calibrations, and can not account for the CCD
;          contamination layer. Use <make_xrt_wave_resp.pro> instead.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-May-16' ;--- (M.Weber) Written. (Based on heavily on
;                             earlier work, though.)
progver = 'v2008-Oct-01' ;--- (M.Weber) This routine has been deprecated.
;                             See Note #3.
;                      
;-
; =========================================================================


; === Initial setup ==============================

  ;=== Initialize program constants.
  prognam = 'CALC_XRT_SPEC_RESP'
  prognul = '                  '
  xrt_pix_len = 0.00135 ;; [cm]
  xrt_eV_per_el = 3.65  ;; [eV el^-1]
  h = 4.136e-15         ;; [eV s]
  c = 3.000e+18         ;; [Angstroms s^-1]

  ;=== Set some keyword Booleans.
  qstop    = keyword_set(qstop)
  qabort   = 0B


; === Check inputs ===============================

  ;; None currently.


; === Calculate spectral response per channel ====

  effarea = calc_xrt_effarea(trans_file=trans_file, qabort=qabort) ;; [cm^2]
  if qabort then begin
    return, 0
  endif
  nchn = n_elements(effarea)

  foclen = get_xrt_foclen(qabort=qabort)      ;; [cm]
  if qabort then begin
    return, 0
  endif

  eV_per_ph = effarea.wave  
  for ii = 0,(nchn-1) do begin
    eV_per_ph[0:effarea[ii].length-1,ii] =                         $
                      h * c / effarea[ii].wave[0:effarea[ii].length-1]
  end
  el_per_ph = eV_per_ph / xrt_eV_per_el  ;; [el ph^-1] Duh.

  ;=== Units = cm^2 * (str/pix). See Note #2.
  spec_resp = el_per_ph * effarea.eff_area * (xrt_pix_len/foclen)^2


; === Generate spec_resp structure ===============

  outvar = {type:'spec_resp', channel_name:'', wave:fltarr(5000), $
            wave_units:'', spec_resp:fltarr(5000),                $
            spec_resp_units:'el cm^2 sr ph^-1 pix^-1', length:1L, $
            data_files:''                                          }

  outvar = replicate(temporary(outvar), nchn)

  outvar.channel_name = effarea.channel_name
  outvar.wave         = effarea.wave
  outvar.wave_units   = effarea.wave_units
  outvar.spec_resp    = spec_resp
  outvar.length       = effarea.length
  outvar.data_files   = effarea.data_files


; === Finish =====================================

  if qstop then stop

  return, outvar


END ;======================================================================
