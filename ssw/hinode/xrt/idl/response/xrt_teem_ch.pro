PRO 	xrt_teem_ch, index1, data1, index2, data2, $
		te, em, et, ee, $
  		spectrum_file=spectrum_file, ch_version=ch_version, $
		hybrid=hybrid, photospheric=photospheric, $
  		bin=bin, trange=trange, no_threshold=no_threshold, $
  		te_err_threshold = te_err_threshold, $
  		photon_noise_threshold = photon_noise_threshold, $
  		mask1 = mask1, mask2 = mask2, eff_area = eff_area

; =========================================================================
;
;+
; PROJECT:
;
;       Solar-B / XRT 
;
; NAME:
;       
;       XRT_TEEM_CH
;
; CATEGORY:
;       
;       XRT CORONAL TEMPERATURE DIAGNOSTICS
;
; PURPOSE:
;
;       Calculate coronal temperature and emission measure using filter 
;       ratio method. Values are derived in log scale.
;       
; CALLING SEQUENCE:
;
;       XRT_TEEM_CH, index1, data1, index2, data2, te, em, et, ee, $
;       		 [,spectrum_file=spectrum_file] [,ch_version=ch_version]		
;          	         [,hybrid=hybrid] [,photospheric=photoshperic] 
;                 	 [,bin=bin] [,trange=trange] [,/no_threshold]
;		         [,te_err_threshold=te_err_threshold]
;                 	 [,photon_noise_threshold=photon_noise_threshold]
;                 	 [,mask1=mask1] [,mask2=mask2] [,eff_area=eff_area]
;
;
; INPUTS:
;       
;       INDEX1 - [Mandatory] (structure array) XRT index.
;       DATA1  - [Mandatory] (2 or 3-dim float array, [x, y, t]) 
;                 XRT *non-normalized* level1 data.
;       INDEX2 - [Mandatory] (structure array) XRT index.
;       DATA2  - [Mandatory] (2 or 3-dim float array, [x, y, t]) 
;                 XRT *non-normalized* level1 data.
;
; OUTPUTS:
;       
;       TE   - (2-dim float array, [x, y]) 
;               derived temperature in log scale [log K].
;       EM   - (2-dim float array, [x, y]) 
;               derived volume emission measure in log scale [log cm^-3].
;       ET   - (2-dim float array, [x, y]) 
;               error of temperature in log scale [log K].
;       EE   - (2-dim float array, [x, y]) 
;               error of volume emission measure in log scale [log cm^-3].
;
;
; OPTIONAL INPUT KEYWORD PARAMETERS:
;
;   	SPECTRUM_FILE	- [Optional] (scalar string) 
;              		specify the name of the solar spectrum files (with path).
;	CH_VERSION	- [Optional] (scalar string of length 4). Specify 
;			the desired version of solar spectrum files calculated with 
;			CHIANTI (Densities: 10^9 [cm^-3]). By default, the routine will 
;			use the latest version of Chianti that is available. These files
;			are stored in $SSW_IDL/XRT/idl/response/spectrum_files/. 
;			As of this writing, mid-2017 A.D., the versions of Chianti 
;			that are available for use by this routine are:
;			0601 = Version 6.0.1
;			0700 = Version 7.0.0
;			0710 = Version 7.1.0
;			0713 = Version 7.1.3
;			0800 = Version 8.0.0
;			list = List all available versions of Chianti. This will cause 
;			the routine to exit before calculating the temperature. 
;			The ionization equilibrium: chianti.ioneq.
;						The abundance: sun_coronal_1992_feldman_ext.       
;       HYBRID		- [Optional input] (boolean) Switch to use 
;			hybrid spectrum.
;       PHOTOSPHERIC    - [Optional input] (boolean) Switch to use 
;			photospheric spectrum.
;       BIN   		- [Optional] (long) Same as <xrt_teem.pro>. If set, data 
;			is binned by amount specified using rebin procedure prior to 
;			derivation of temperature.
;	TRANGE		- [Optional] Same as <xrt_teem.pro>.
;       /NO_THRESHOLD   - [Optional] Same as <xrt_teem.pro>. (Boolean) If set, 
;			no threshold is set. 
;       TE_ERR_THRESHOLD - [Optional input] (float) Same as <xrt_teem.pro>. Adjust 
;			the threshold ratio temperature error. Default is 0.10 [10%]. 
;       PHOTON_NOISE_THRESHOLD 	- [Optional input] (float) Same as <xrt_teem.pro>. Adjust 
;			the threshold ratio of photon noise to signal. Default is 
;			0.10 [10%]. 
;	MASK1		- [Optional input] Same as <xrt_teem.pro>.
;	MASK2		- [Optional input] Same as <xrt_teem.pro>.
;       EFF_AREA       	- [Optional] same as <xrt_teem.pro>. You can input the 
;			output (effective area) from <make_xrt_wave_resp.pro>
;
; EXAMPLES:
;      
;  		Use latest spectrum file, i.e., 'solspec_ch800_corona_chianti.genx'
;  		in $SSW_XRT/idl/response/spectrum_files.
;    		 IDL> xrt_teem_ch, index1, data1, index2, data2, te, em, et, ee
;
;
;  		Use spectrum file with hybrid abundance and Chianti 7.1.0, i.e.,
;  		$SSW_XRT/idl/response/spectrum_files/solspec_ch0800_hybrid_chianti.genx.
;     		IDL> xrt_teem_ch, index1, data1, index2, data2, te, em, et, ee, $
;				/hybrid,ch_version='0710'
;
;
;  		Use other spectrum file (file path required).
;     		IDL> sp_file='MyFavorite_Spectra.genx'
;    		 IDL> xrt_teem_ch, index1, data1, index2, data2, te, em, et, ee, $
;                spectrum_file=sp_file
;
;
; COMMON BLOCKS:
;
;       none 
;
; NOTES:
;
;       call xrt_teem.pro.
;
; CONTACT:
;	
;	Comments, feedback, and bug reports regarding this routine may be directed
;	to this email address:
;       
; MODIFICATION HISTORY:
;
; progver = 'v2017-Jan-11' Aki Takeda (MSU). 
;           Modified from xrt_teem_ch713.pro 
; 
progver='v2017-Jun-29' ;--- (P. R. Jibben (SAO)) Prepared routine for 
;			    distribution in SolarSoft.
;	
;
;-
; =========================================================================


;=== Initial setup ========================================================

; Path to the spectrum files.
  ps = path_sep()
 dir_spec = get_logenv('$SSW_XRT') + ps + 'idl' + ps + 'response' + ps + 'solar_spectrum' + ps

; Default spectra and settings to use. 
  spectra_ver='0800'
  sp_type='corona'
  spfile=dir_spec+'solspec_ch0800_corona_chianti.genx'
  
; Initializing switches
  listver=0B
  
;===== Addressing Keywords ================================================

  if keyword_set(spectrum_file) then begin
    spfile=spectrum_file
  endif else begin
  
    sfiles=file_search(dir_spec,'*.genx')
    sfiles=file_basename(sfiles)
    ChVers=strmid(sfiles,10,4)
    ChVers=chVers[uniq(chVers)] 
    
    if not keyword_set(ch_version) then $
      spectra_ver=string(max(fix(ChVers)),format='(I04)')
    
    if keyword_set(ch_version) then begin
      verloc=where(strmatch(ChVers,ch_version,/fold_case) eq 1,count)      
      if count eq 1 then spectra_ver=ChVers[verloc[0]] else listver=1B
      if (strmatch(ch_version,'list',/fold_case) eq 1 or listver eq 1) then begin
        box_message,['Available Chianti Versions:',ChVers]
        return
      endif
    endif
    
  if keyword_set(photospheric) then sp_type='photos'
  if keyword_set(hybrid) then sp_type='hybrid'
  spfile=dir_spec+'solspec_ch'+spectra_ver+'_'+sp_type+'_chianti.genx'
  
  if (file_test(spfile) eq 0) then begin
    box_message,['File Does Not Exist:',spfile, $
                  '......................................Aborting']
    return
  endif
  
  endelse

;===== Main Routine =======================================================

  restgen, te_spectrum, lambda_spectrum, spectrum, file = spfile

  help, file_basename(spfile), te_spectrum, lambda_spectrum, spectrum

  xrt_teem, index1, data1, index2, data2, te, em, et, ee, 			$
    lambda_spectrum = lambda_spectrum, te_spectrum = te_spectrum, 		$
    spectrum = spectrum, bin = bin, trange = trange, 				$
    no_threshold = no_threshold, te_err_threshold = te_err_threshold,		$
    photon_noise_threshold = photon_noise_threshold, 				$
    mask1 = mask1, mask2 = mask2, eff_area = eff_area




END ;======================================================================



