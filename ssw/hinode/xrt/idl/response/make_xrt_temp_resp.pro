FUNCTION make_xrt_temp_resp, wave_resp, emiss_model,               $
                             apec_default=apec_default,            $
                             chianti_default=chianti_default,      $
                             outfile=outfile, el_units=el_units,   $
                             verbose=verbose, quiet=quiet,         $
                             qabort=qabort, qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       MAKE_XRT_TEMP_RESP
;
; CATEGORY:
;       
;       Instrument response
;
; PURPOSE:
;
;       Produce the temperature response for each XRT x-ray channel,
;       assuming a spectral emission model. See Note #1 for an 
;       explanation of the calculations.
;       (This routine deprecates and replaces the older routine
;       <calc_xrt_temp_resp.pro>.)
;
; CALLING SEQUENCE:
;
;       Result = MAKE_XRT_TEMP_RESP( wave_resp,
;                  {[emiss_model] OR [/apec_default] OR [/chianti_default]}
;                  [,outfile=outfile] [,/el_units] [,/verbose]
;                  [,/quiet] [,qabort=qabort] [,/qstop] )
;
; INPUTS:
;
;       WAVE_RESP   - [Mandatory] (structure array, 
;                     {XRT_wave_resp_vNNNN} [Nchannels])
;                     This structure contains data and information about
;                     the effective areas and spectral response
;                     functions for a set of XRT x-ray channels. This is
;                     the input that describes the instrument response.
;                     For a more complete description, see the XRT
;                     Analysis Guide or the program header for
;                     <make_xrt_wave_resp.pro>.
;       EMISS_MODEL - [Optional*] (structure scalar, 
;                                {XRT_emiss_model_vNNNN} [Nchannels])
;                     (*) ONE (and only one) of these three inputs MUST
;                     be specified: {EMISS_MODEL, APEC_DEFAULT,
;                     CHIANTI_DEFAULT}.
;                     This structure contains data and information about
;                     a plasma emission model, as a function of
;                     wavelength and temperature. This is the input
;                     that associates spectra with temperatures,
;                     which allows one to "convert" an instrument's
;                     spectral response to a temperature response.
;                     For a more complete description, see the XRT
;                     Analysis Guide or the program header for
;                     <make_xrt_emiss_model.pro>.
;       OUTFILE     - [Optional] (string scalar)
;                     Write the constructed temperature response
;                     structure to the specified file, using 
;                     <savegenx.pro>. The extension ".geny" will be 
;                     automatically appended to the filename. See
;                     "Return" output for structure description.
;
; KEYWORDS:
;
;       /APEC_DEFAULT    - [Optional*] (Boolean)
;                          (*) ONE (and only one) of these three
;                          inputs MUST be specified: {EMISS_MODEL,
;                          APEC_DEFAULT, CHIANTI_DEFAULT}. This
;                          input keyword switch may be used to
;                          indicate that the XRT default emission
;                          model from APED/APEC should be utilized.
;                          (Some of the details of this model are returned
;                          in the TRESP.EMISS substructure of the
;                          function output.)
;       /CHIANTI_DEFAULT - [Optional*] (Boolean)
;                          [NOTE: This switch is not yet implemented!]
;                          (*) ONE (and only one) of these three
;                          inputs MUST be specified: {EMISS_MODEL,
;                          APEC_DEFAULT, CHIANTI_DEFAULT}. This
;                          input keyword switch may be used to
;                          indicate that the XRT default emission
;                          model from CHIANTI should be utilized.
;                          (Some of the details of this model are returned
;                          in the TRESP.EMISS substructure of the
;                          function output.)
;       /EL_UNITS        - [Optional] (Boolean) If set, then the units of
;                          the temperature response (in the TEMP_RESP array
;                          output) will be [el cm^5 s^-1 pix^-1]. 
;                          Otherwise, the default is to produce the
;                          the response in units of [DN cm^5 s^-1 pix^-1].
;       /VERBOSE         - [Optional] (Boolean) If set, print out extra
;                          information. Overrides "/quiet" (see Note #4).
;       /QUIET           - [Optional] (Boolean) If set, suppress messages
;                          (see Note #4).
;       /QSTOP           - [Optional] (Boolean) For debugging.
;
;
; OUTPUTS:
;
;       Return      - [Mandatory] (structure array,
;       (TRESP)                    {'<Anonymous'>} [Nchannels])
;                     This structure contains data and information about
;                     the temperature response for each XRT x-ray channel
;                     described by the WAVE_RESP input (which includes
;                     the contamination layers). A "channel" is
;                     primarily specified by a particular setting
;                     of the x-ray analysis filters and a CCD
;                     contamination thickness. More detail is located
;                     in the description of the fields below.
;
;                     Here is a listing of the fields of the
;                     {XRT_temp_resp} structure. Descriptions are provided
;                     further down.:
;
;                    TRESP {<Anonymous>}
;                    - TYPE            (string scalar)
;                    - TRS_STR_VERSION (string scalar)
;                    - TRS_STR_DESCR   (string scalar)
;                    - NAME            (string scalar)
;                    - TEMP            (float array, [Ntemps])
;                    - TEMP_UNITS      (string scalar)
;                    - TEMP_RESP       (float array, [Ntemps])
;                    - TEMP_RESP_UNITS (string scalar)
;                    - LENGTH          (long scalar)
;                    - WAVE_MIN        (float scalar)
;                    - WAVE_MAX        (float scalar)
;                    - CONFG  (structure scalar, {XRT_chn_config_ref_vNNNN})
;                    - CONTAM (structure scalar, {XRT_contam_ref_vNNNN})
;                    - FCONTAM (structure scalar, {XRT_fcontam_ref_vNNNN})
;                    - EFFAR  (structure scalar, {XRT_eff_area_ref_vNNNN})
;                    - SPRSP  (structure scalar, {XRT_spec_resp_ref_vNNNN})
;                    - EMISS  (structure scalar, {<Anonymous>})
;                    - HISTORY         (string array, [3])
;                    - COMMENTS        (string array, [5])
;
;                     Here are descriptions of the fields in the
;                     {XRT_temp_resp} structure.:
;
;                     TYPE: (string scalar)
;                          [Default value = 'XRT_temp_resp'.]
;                          This is the generic name of the structure
;                          that contains the temperature response
;                          functions and information. The version
;                          of the structure definition might evolve,
;                          but the TYPE should remain constant. See
;                          TRS_STR_VERSION. This field should NOT
;                          be changed.
;                     TRS_STR_VERSION: (string scalar)
;                          [Default value = '<Anonymous>'.]
;                          The "XRT_temp_resp" structure has an
;                          "anonymous" definition in the IDL sense.
;                          There is no version number associated
;                          with the "XRT_temp_resp" structure
;                          definition because some of its arrays
;                          must be permitted to have variable sizes
;                          to accomodate the EMISS_MODEL input.
;                          This field should NOT be changed.
;                     TRS_STR_DESCR: (string scalar)
;                          This is a sentence or two that describes
;                          the purpose of this structure type. This
;                          field should NOT be changed.
;                     NAME: (string scalar)
;                          This is a name for this temperature
;                          response for this channel and contamination
;                          thickness. On creation, it inherits the
;                          name of the WAVE_RESP structure that was
;                          used to create it, and adds the name of
;                          the EMISS_MODEL. This string concisely
;                          identifies this temperature response
;                          (although it may not be unique).  This
;                          field MAY be changed, but the default
;                          is recommended.
;                     TEMP: (float array, [Ntemps])
;                          The abscissa of the temperature response
;                          for this channel. The units are provided
;                          in the TEMP_RESP_UNITS field. The actual
;                          values may form a sequence of shorter
;                          length than the length of the TEMP array.
;                          The LENGTH field indicates the subarray
;                          of usable values, according to TEMP[0:LENGTH-1].
;                          Unused elements are set to 0.0.
;                     TEMP_UNITS: (string scalar)
;                          These are the units of the TEMP values,
;                          and MUST be equal to 'K' (degrees Kelvin).
;                     TEMP_RESP: (float array, [Ntemps])
;                          The temperature response function for
;                          this channel, this value indicates the
;                          level of CCD signal that is produced in
;                          one CCD pixel, in one second, by 1 cm^-5
;                          (column) emission measure of plasma at
;                          the corresponding temperature. The units
;                          are provided in the TEMP_RESP_UNITS
;                          field. See Note #1 for a discussion of
;                          the calculation of this function from
;                          the inputs, and how the units work. The
;                          actual values may form a sequence of
;                          shorter length than the length of the
;                          TEMP_RESP array. The LENGTH field indicates
;                          the subarray of usable values, according
;                          to TEMP_RESP[0:LENGTH-1].  Unused elements
;                          are set to 0.0.
;                     TEMP_RESP_UNITS: (string scalar)
;                          These are the units of the TEMP_RESP values. 
;                          The default units are 'DN cm^5 s^-1 pix^-1'
;                          because XRT data are typically presented in DN.
;                          However, the /EL_UNITS keyword may be used to
;                          get TEMP_RESP in units of 'el cm^5 s^-1 pix^-1'.
;                          See Note #1 for a discussion of how the
;                          units work.
;                     LENGTH: (long scalar)
;                          0 < LENGTH =< n_elements(TEMP)
;                          Indicates sublength of the TEMP and
;                          TEMP_RESP arrays that contains valid
;                          values, starting at the zero-th index.
;                          Only the range [0:LENGTH-1] of TEMP and
;                          TEMP_RESP will be utilized. Values outside
;                          this range may be zeroed. This value is
;                          set equal to the corresponding value in
;                          the EMISS_MODEL input. This field should
;                          NOT be changed.
;                     GAIN_USED: (float scalar)
;                          The camera gain has units of [el DN^-1],
;                          and is the conversion factor for the 
;                          temperature response function between units of
;                          [DN cm^5 s^-1 pix^-1] and [el cm^5 s^-1 pix^-1].
;                          If the gain was used to convert the units of
;                          the TEMP_RESP, then that value is recorded here.
;                          A value of GAIN_USED = -1.0 indicates that
;                          the units were not converted while 
;                          calculating TEMP_RESP from SPEC_RESP.
;                          (However, note that the units of SPEC_RESP
;                          may have been previously converted, so
;                          WAVE_RESP.SPRSP.GAIN_USED should also be 
;                          consulted.)
;                     WAVE_MIN: (float scalar)
;                          The temperature response function is
;                          formed by an integral over wavelength
;                          of the instrument spectral response
;                          (input in WAVE_RESP) with the spectral
;                          emission model (input in EMISS_MODEL).
;                          (See Note #3 for more details about the
;                          calculation.) The integration range only
;                          spans the logical intersection of the
;                          wavelength ranges of the two inputs. The
;                          WAVE_MIN and WAVE_MAX fields record the
;                          bounds of integration.
;                     WAVE_MAX: (float scalar)
;                          The temperature response function is
;                          formed by an integral over wavelength
;                          of the instrument spectral response
;                          (input in WAVE_RESP) with the spectral
;                          emission model (input in EMISS_MODEL).
;                          (See Note #3 for more details about the
;                          calculation.) The integration range only
;                          spans the logical intersection of the
;                          wavelength ranges of the two inputs. The
;                          WAVE_MIN and WAVE_MAX fields record the
;                          bounds of integration.
;                     CONFG: (structure scalar, {XRT_chn_config_ref_vNNNN})
;                          This structure contains information about
;                          the instrument components and the history
;                          of their role in the processing that has
;                          been performed on this channel to produce
;                          this temperature response function. All
;                          of its values are inherited from the
;                          corresponding "CONFG" substructure in
;                          WAVE_RESP. Large arrays (such as the
;                          channel transmission function) have been
;                          abridged, but there should be sufficient
;                          information in this structure to recover
;                          or reproduce the corresponding full
;                          channel structure (of type "XRT_chn_config").
;                          More information about the content of
;                          this "XRT_chn_config_ref" type of structure
;                          may be found in the XRT Analysis Guide
;                          or in the program header of
;                          <make_xrt_wave_resp.pro>.
;                     CONTAM: (structure scalar, {XRT_contam_ref_vNNNN})
;                          This structure contains information about
;                          the particular thickness of the CCD
;                          contamination layer (which affects the
;                          response of the channel) and the history
;                          of its role in the processing that has
;                          been performed on this channel to produce
;                          this temperature response function. All
;                          of its values are inherited from the
;                          corresponding "CONTAM" substructure in
;                          WAVE_RESP. Large arrays (such as the
;                          contamination's transmission function)
;                          have been abridged, but there should be
;                          sufficient information in this structure
;                          to recover or reproduce the corresponding
;                          full contamination structure (of type
;                          "XRT_contam"). More information about
;                          the content of this "XRT_contam_ref"
;                          type of structure may be found in the
;                          XRT Analysis Guide or in the program
;                          header of <make_xrt_wave_resp.pro>.
;                    FCONTAM: (structure scalar, {XRT_fcontam_ref_vNNNN})
;                          This structure contains information about
;                          the particular thickness of the filter1+2 
;                          contamination layers (which affects the
;                          response of the channel) and the history
;                          of its role in the processing that has
;                          been performed on this channel to produce
;                          this temperature response function. All
;                          of its values are inherited from the
;                          corresponding "FCONTAM" substructure in
;                          WAVE_RESP. Large arrays (such as the
;                          contamination's transmission function)
;                          have been abridged, but there should be
;                          sufficient information in this structure
;                          to recover or reproduce the corresponding
;                          full contamination structure (of type
;                          "XRT_fcontam"). More information about
;                          the content of this "XRT_fcontam_ref"
;                          type of structure may be found in the
;                          XRT Analysis Guide or in the program
;                          header of <make_xrt_wave_resp.pro>.
;
;                     EFFAR: (structure scalar, {XRT_eff_area_ref_vNNNN})
;                          This structure contains information about
;                          the channel's effective area and about
;                          the history of its role in the processing
;                          that has been performed on this channel
;                          to produce this temperature response
;                          function. Some of its values are
;                          inherited from the corresponding "EFFAR"
;                          substructure in WAVE_RESP. Large arrays
;                          (such as the effective area function)
;                          have been abridged, but there should be
;                          sufficient information in this structure
;                          to recover or reproduce the corresponding
;                          full effective area structure (of type
;                          "XRT_eff_area"). This abridged (i.e.,
;                          "reference") structure (of type
;                          "XRT_eff_area_ref") is defined in this
;                          program, so the fields are listed and
;                          described here.
;
;                          - TYPE            (string scalar)
;                          - EFF_STR_VERSION (string scalar)
;                          - EFF_STR_DESCR   (string scalar)
;                          - NAME            (string scalar)
;                          - HISTORY         (string array, [3])
;                          - COMMENTS        (string array, [5])
;
;                          EFFAR.TYPE: (string scalar)
;                            [The default value = 'XRT_eff_area_ref'.]
; 			     This is the generic name of the structure
; 			     that contains the abridged (i.e.,
;                            "reference") version of the channel's
;                            effective area. Abridged means that the
;                            actual function and other long arrays
;                            are not included. This "reference"
;                            structure only contains the descriptive
;                            information. The version of the structure
;                            definition might evolve, but the TYPE
;                            should remain constant. See EFF_STR_VERSION.
;                            The value is initialized at creation.
;                            This field should NOT be changed.
;                          EFFAR.EFF_STR_VERSION: (string scalar)
;                            [The default value = 'XRT_temp_resp_vNNNN',
;                            where NNNN is a 4-digit number.] This
;                            is the version number of the "XRT_eff_area_ref"
;                            structure definition. In addition to
;                            being recorded in this field, this is
;                            also the "unique structure name" in the
;                            IDL sense. The value is initialized at
;                            creation. This field should NOT be
;                            changed.
;                          EFFAR.EFF_STR_DESCR: (string scalar)
;                            This is a sentence or two that describes
;                            the purpose of this structure type. The
;                            value is initialized at creation. This
;                            field should NOT be changed.
;                          EFFAR.NAME: (string scalar)
;                            This is a name for the effective area
;                            for this channel and contamination
;                            thickness. This string should concisely
;                            identify this effective area
;                            (although it may not be unique). On
;                            creation, the value is inherited from
;                            the EFFAR.NAME field of the input
;                            WAVE_RESP. This field MAY be changed,
;                            but the default is recommended.
;                          EFFAR.HISTORY (string array, [3])
;                            This text array is for XRT programs to
;                            record any instance in which they have
;                            modified the content of the "XRT_eff_area_ref"
;                            structure (or parent "XRT_eff_area"
;                            structure) for this particular channel.
;                            On creation, some values are inherited
;                            from the EFFAR.HISTORY field of the input
;                            WAVE_RESP, and a line is added for the
;                            creation of the "XRT_eff_area_ref"
;                            structure in <make_xrt_temp_resp.pro>.
;                            An entry string is composed of (a) a
;                            program name, (b) the program's version/date,
;                            (c) colon separator, (d) Timestamp (in
;                            parentheses) of when the program completed
;                            the described action, and (e) a summation
;                            of what the program created or modified
;                            in the "XRT_eff_area_ref" structure.
;                            Values are inserted starting at the
;                            lowest address of this array.  Remaining
;                            unused addresses are filled with the
;                            empty string. Filled strings should NOT
;                            be overwritten. Users should use the
;                            EFFAR.COMMENTS field to add their own
;                            notations to this structure.
;                          EFFAR.COMMENTS (string array, [5])
;                            A tag for adding notations regarding the
;                            described effective area. Users and
;                            programs are encouraged to put content
;                            in the lower addresses, and assign empty
;                            strings {''} to the higher, unused
;                            addresses. On creation, the values from
;                            EFFAR.COMMENTS in the input WAVE_RESP
;                            are inherited.
;
;                     SPRSP: (structure scalar, {XRT_spec_resp_ref_vNNNN})
;                          This structure contains information about
;                          the channel's spectral response and about
;                          the history of its role in the processing
;                          that has been performed on this channel
;                          to produce this temperature response
;                          function. Some of its values are inherited
;                          from the corresponding "SPRSP" substructure
;                          in WAVE_RESP. Large arrays (such as the
;                          spectral response function) have been
;                          abridged, but there should be sufficient
;                          information in this structure to recover
;                          or reproduce the corresponding full
;                          spectral response structure (of type
;                          "XRT_spec_resp"). This abridged (i.e.,
;                          "reference") structure (of type
;                          "XRT_spec_resp_ref") is defined in this
;                          program, so the fields are listed and
;                          described here.
;
;                          - TYPE            (string scalar)
;                          - SRS_STR_VERSION (string scalar)
;                          - SRS_STR_DESCR   (string scalar)
;                          - NAME            (string scalar)
;                          - HISTORY         (string array, [3])
;                          - COMMENTS        (string array, [5])
;
;                          SPRSP.TYPE: (string scalar)
;                            [The default value = 'XRT_spec_resp_ref'.]
;                            This is the generic name of the structure
;                            that contains the abridged (i.e.,
;                            "reference") version of the channel's
;                            spectral response. Abridged means that
;                            the actual function and other long
;                            arrays are not included. This "reference"
;                            structure only contains the descriptive
;                            information. The version of the structure
;                            definition might evolve, but the TYPE
;                            should remain constant. (See
;                            SPRSP.SRS_STR_VERSION.) The value is
;                            initialized at creation. This field
;                            should NOT be changed.
;                          SPRSP.SRS_STR_VERSION: (string scalar)
;                            [The default value = 'XRT_spec_resp_ref_vNNNN',
;                            where NNNN is a 4-digit number.] This
;                            is the version number of the
;                            "XRT_spec_resp_ref" structure definition.
;                            In addition to being recorded in this
;                            field, this is also the "unique structure
;                            name" in the IDL sense. The value is
;                            initialized at creation. This field
;                            should NOT be changed.
;                          SPRSP.SRS_STR_DESCR: (string scalar)
;                            This is a sentence or two that describes
;                            the purpose of this structure type. The
;                            value is initialized at creation. This
;                            field should NOT be changed.
;                          SPRSP.NAME: (string scalar)
;                            This is a name for the spectral response
;                            for this channel and contamination
;                            thickness. This string should concisely
;                            identify this spectral response (although
;                            it may not be unique). On creation,
;                            the value is inherited from the
;                            SPRSP.NAME field of the input WAVE_RESP.
;                            This field MAY be changed, but the
;                            default is recommended.
;                          SPRSP.HISTORY (string array, [3])
;                            This text array is for XRT programs
;                            to record any instance in which they
;                            have modified the content of the
;                            "XRT_spec_resp_ref" structure (or the
;                            parent "XRT_spec_resp" structure) for
;                            this particular channel.  On creation,
;                            some values are inherited from the
;                            SPRSP.HISTORY field of the input
;                            WAVE_RESP, and a line is added for the
;                            creation of the "XRT_spec_resp_ref"
;                            structure in <make_xrt_temp_resp.pro>.
;                            An entry string is composed of (a) a
;                            program name, (b) the program's
;                            version/date, (c) colon separator, (d)
;                            Timestamp (in parentheses) of when the
;                            program completed the described action,
;                            and (e) a summation of what the program
;                            created or modified in the "XRT_spec_resp_ref"
;                            structure.  Values are inserted starting
;                            at the lowest address of this array.
;                            Remaining unused addresses are filled
;                            with the empty string. Filled strings
;                            should NOT be overwritten. Users should
;                            use the SPRSP.COMMENTS field to add
;                            their own notations to this structure.
;                          SPRSP.COMMENTS (string array, [5])
;                            A tag for adding notations regarding
;                            the described spectral response. Users
;                            and programs are encouraged to put
;                            content in the lower addresses, and
;                            assign empty strings {''} to the higher,
;                            unused addresses. On creation, the
;                            values from SPRSP.COMMENTS in the input
;                            WAVE_RESP are inherited.
;
;                     EMISS: (structure scalar, {<Anonymous>})
;                          This structure contains information about
;                          the plasma emission model (input
;                          EMISS_MODEL) and about the history of
;                          its role in the processing that has been
;                          performed to produce this temperature
;                          response function. Some of its values
;                          are inherited from the EMISS_MODEL input.
;                          Large arrays (such as the model's spectra)
;                          have been abridged, but there should be
;                          sufficient information in this structure
;                          to recover or reproduce the corresponding
;                          full spectral response structure (of
;                          type "XRT_emiss_model"). This abridged
;                          (i.e., "reference") structure (of type
;                          "XRT_emiss_model_ref") is defined in
;                          this program, so the fields are listed
;                          and described here.
;
;                          - TYPE            (string scalar)
;                          - EMS_STR_VERSION (string scalar)
;                          - EMS_STR_DESCR   (string scalar)
;                          - NAME            (string scalar)
;                          - ABUND_MODEL     (string scalar)
;                          - IONEQ_MODEL     (string scalar)
;                          - DENS_MODEL      (string scalar)
;                          - DATA_FILES      (string array, [5])
;                          - HISTORY         (string array, [3])
;                          - COMMENTS        (string array, [5])
;
;                          EMISS.TYPE: (string scalar)
;                            [The default value = 'XRT_emiss_model_ref'.]
;                            This is the generic name of the structure
;                            that contains the abridged (i.e.,
;                            "reference") version of the emission
;                            model. Abridged means that the actual
;                            spectra and other long arrays are not
;                            included. This "reference" structure
;                            only contains the descriptive information.
;                            The version of the structure definition
;                            might evolve, but the TYPE should
;                            remain constant. (See EMISS.EMS_STR_VERSION.)
;                            The value is initialized at creation.
;                            This field should NOT be changed.
;                          EMISS.EMS_STR_VERSION: (string scalar)
;                            [The default value = '<Anonymous>'.]
;                            The "XRT_emiss_model_ref" structure
;                            has an "anonymous" definition in the
;                            IDL sense.  There is no version number
;                            associated with the "XRT_emiss_model_ref"
;                            structure definition because some of
;                            its arrays must be permitted to have
;                            variable sizes. This field should NOT
;                            be changed.
;                          EMISS.EMS_STR_DESCR: (string scalar)
;                            This is a sentence or two that describes
;                            the purpose of this structure type. The
;                            value is initialized at creation. This
;                            field should NOT be changed.
;                          EMISS.NAME: (string scalar)
;                            This is a name for the emission model.
;                            This string should concisely identify
;                            this emission model (although it may
;                            not be unique). On creation of this
;                            field, the value is inherited from the
;                            NAME field of the input EMISS_MODEL.
;                            This field MAY be changed, but the
;                            default is recommended.
;                          EMISS.ABUND_MODEL: (string scalar)
;                            This is a brief description of what
;                            abundance model was used in the creation
;                            of the emission spectra. On creation
;                            of this field, this value is inherited
;                            from the ABUND_MODEL field of the input
;                            EMISS_MODEL. This field should NOT be
;                            changed.
;                          EMISS.IONEQ_MODEL: (string scalar)
;                            This is a brief description of what
;                            ionization equilibrium model was used
;                            in the creation of the emission spectra.
;                            On creation of this field, this value
;                            is inherited from the IONEQ_MODEL field
;                            of the input EMISS_MODEL. This field
;                            should NOT be changed.
;                          EMISS.DENS_MODEL: (string scalar)
;                            This is a brief description of the
;                            plasma density, emission measure, or
;                            differential emission measure that was
;                            used in the creation of the emission
;                            spectra. On creation of this field,
;                            this value is inherited from the
;                            DENS_MODEL field of the input EMISS_MODEL.
;                            This field should NOT be changed.
;                          EMISS.DATA_FILES (string array, [5])
;                            This is a list of filenames of reference
;                            data that were used in the creation
;                            of the emission spectra. This need not
;                            be an exhaustive list. It is primarily
;                            intended for listing any files that
;                            the model creator thinks are important
;                            for reference purposes. On creation
;                            of this field, this value is inherited
;                            from the DATA_FILES field of the input
;                            EMISS_MODEL.  This field should NOT
;                            be changed.
;                          EMISS.HISTORY (string array, [3])
;                            This text array is for XRT programs
;                            to record any instance in which they
;                            have modified the content of the
;                            "XRT_emiss_model_ref" structure (or
;                            the parent "XRT_emiss_model" structure)
;                            for this particular channel.  On
;                            creation, some values are inherited
;                            from the HISTORY field of the input
;                            EMISS_MODEL, and a line is added for
;                            the creation of the "XRT_emiss_model_ref"
;                            structure in <make_xrt_temp_resp.pro>.
;                            An entry string is composed of (a) a
;                            program name, (b) the program's
;                            version/date, (c) colon separator, (d)
;                            Timestamp (in parentheses) of when the
;                            program completed the described action,
;                            and (e) a summation of what the program
;                            created or modified in the
;                            "XRT_emiss_model_ref" structure. Values
;                            are inserted starting at the lowest
;                            address of this array. Remaining
;                            unused addresses are filled with the
;                            empty string. Filled strings should
;                            NOT be overwritten. Users should use
;                            the EMISS.COMMENTS field to add their
;                            own notations to this structure.
;                          EMISS.COMMENTS (string array, [5])
;                            A tag for adding notations regarding
;                            the described emission model. Users
;                            and programs are encouraged to put
;                            content in the lower addresses, and
;                            assign empty strings {''} to the higher,
;                            unused addresses. On creation, the
;                            values from COMMENTS in the input
;                            EMISS_MODEL are inherited.
;
;                     HISTORY: (string array, [3])
;                          This text array is for XRT programs to
;                          record any instance in which they have
;                          modified the content of the "XRT_temp_resp"
;                          structure for this particular channel.
;                          On creation, a line is added for the
;                          creation of the "XRT_temp_resp" structure
;                          in <make_xrt_temp_resp.pro>.  An entry
;                          string is composed of (a) a program name,
;                          (b) the program's version/date, (c) colon
;                          separator, (d) Timestamp (in parentheses)
;                          of when the program completed the described
;                          action, and (e) a summation of what the
;                          program created or modified in the
;                          "XRT_temp_resp" structure. Values are
;                          inserted starting at the lowest address
;                          of this array. Remaining unused addresses
;                          are filled with the empty string. Filled
;                          strings should NOT be overwritten. Users
;                          should use the COMMENTS field to add
;                          their own notations to this structure.
;                     COMMENTS: (string array, [5])
;                          A tag for adding notations regarding the
;                          temperature response described for this
;                          XRT channel. Users and programs are
;                          encouraged to put content in the lower
;                          addresses, and assign empty strings {''}
;                          to the higher, unused addresses.
;
;       QABORT      - [Optional] (Boolean) Indicates whether the program
;                     exited gracefully without completing. (Might be
;                     useful for calling programs.)
;                     0: Program ran to completion.
;                     1: Program aborted before completion.
;
; EXAMPLES:
;
;       Basic usage:
;       IDL> t_resp = make_xrt_temp_resp(w_resp, emiss_model)
;       IDL> help, t_resp
;       IDL> help, t_resp, /st
;
; COMMON BLOCKS:
;
;       none
;
; NOTES:
;
;       1) A discussion of the calculation of the temperature response 
;          function (and associated quantities and units).
;
;          w:      wavelength                    [Angstroms, A]
;          T:      temperature                   [degrees Kelvin, K]
;          S(w,T): spectral emission model       [ph cm^3 s^-1 sr^-1 A^-1]
;          R(w):   spectral response function    [el cm^2 sr ph^-1 pix^-1]
;          TR(T):  temperature response function [el cm^5 s^-1 pix^-1]
;          EM:     (column) emission measure              [cm^-5]
;          DEM(T): (column) differential emission measure [cm^-5 K^-1]
;          X:      CCD signal (accumulation rate)         [el s^-1 pix^-1]
;
;          A spectral emission model is a set of plasma emission
;          spectra for a set of temperatures. It contains information
;          from all the assumptions (e.g., elemental abundances,
;          ionization equilibrium state, et cetera) that go into
;          modeling the x-ray radiation from a coronal plasma. The
;          units are those of a photon intensity per column emission
;          measure [cm^-5].
;
;          The spectral response function indicates the detector
;          signal (in CCD electrons for XRT) produced by a photon
;          of given energy. It contains all of the information about
;          the channel response as a function of wavelength.
;
;          The temperature response function indicates the detector
;          signal that will be produced by unit emission measure
;          of plasma for a given temperature. It is calculated by
;          this integral relation:
;          TR(T) = integral[ S(w,T) * R(w) * dw ] 
;
;          Equivalently, it can be represented as linear algebra
;          (which is really how the computer calculates it):
;          TR[T1]  =  S[w1T1 w2T1 w3T1 ....]  #  R[w1] * dw
;            [T2]      [w1T2 w2T2 w3T2 ....]      [w2]
;            [T3]      [w1T3 w2T3 w3T3 ....]      [w3]
;            [..]      [.... .... .... ....]      [..]
;                   (... et cetera... )
;
;          How does this help calculate the observation of the
;          plasma model?  The emission spectra S(w,T) is a photon
;          intensity per column emission measure [cm^-5]. So, to
;          calculate a CCD signal (rate), which we call X, it is
;          still necessary to apply an emission measure EM. (A
;          column emission measure is an emission measure for a
;          plasma volume of differential cross-section, but integrated
;          along the line-of-sight.) This can be seen in the units
;          of TR:
;          TR  ~  [el cm^5    s^-1 pix^-1]
;                 [el (EM)^-1 s^-1 pix^-1]
;
;          For a plasma at temperature T0 with emission measure EM_T0,
;          the CCD signal (rate) X is a simple multiplication:
;          X = TR(T0) * EM_T0
;
;          For a plasma with a distribution of emission measures
;          over temperature, the emission measure EM is replaced
;          with the differential emission measure DEM(T). Now the
;          multiplication becomes an integral over temperature:
;          X = integral[ TR(T) * DEM(T) * dT ] 
;
;       2) This version of the program assumes the pixel size is
;          that of a "full-resolution" XRT CCD pixel. It does not yet
;          account for any pixel summing ("image binning").
;
;       3) The wavelength integral for calculating the temperature
;          response function (from Note #1) is:
;          TR(T) = integral[ S(w,T) * R(w) * dw ]
;
;          The emission model S(w,T) and the instrument's spectral
;          response R(w) are calculated independently, so the
;          software does not assume that their wavelength ranges
;          exactly match. If there is no overlap in wavelength
;          range, then the calculation is aborted and the next case
;          is addressed. If there *is* overlap, then the logical
;          intersection (i.e., overlap) of their wavelength ranges
;          is used to set the bounds of integration. Since these
;          bounds are calculated inside the program, their values
;          are recorded in the WAVE_MIN and WAVE_MAX fields of the
;          returned output (type "XRT_temp_resp" structure, defined
;          above).
;
;       4) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
;       5) This routine deprecates and replaces the older routine
;          <calc_xrt_temp_resp.pro>.
;
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       directed to this email address:
;                xrt_manager ~at~ head.cfa.harvard.edu
;
; MODIFICATION HISTORY:
;
progver = 'v2008-Sep-01' ;--- (M.Weber) Written. Derived from
;                             <calc_xrt_temp_resp.pro>, v2007-May-17.
progver = 'v2008-Oct-01' ;--- (M.Weber) Reviewed version.
progver = 'v2008-Nov-21' ;--- (M.Weber) Changed path separator from 
;                                       forward slash to path_sep().
progver = 'v2009-Dec-06' ;--- (S.Saar) Modified to take revised wave response
;                                      structure including filter contamination
;
;-
; =========================================================================



; === Initial setup ==============================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (q_vb eq 0)


  ;=== Initialize program constants.
  prognam = 'MAKE_XRT_TEMP_RESP'
  prognul = '                  '


  ;=== Announce version of program.
  if q_vb then box_message, prognam+': Running ' + progver + '.'
  if q_vb then print, prognam+': Performing initial setup...'


  ;=== Set some keyword Booleans.
  qapec     = keyword_set(apec_default)
  qchianti  = keyword_set(chianti_default)
  qoutfile  = keyword_set(outfile)
  qelunits  = keyword_set(el_units)
  qstop     = keyword_set(qstop)
  qabort    = 0B


  ;=== Some basic program values.
  ps = path_sep()
  emiss_dir = get_logenv('$SSW_XRT') + ps + 'idl' + ps + 'response' $
              + ps + 'emiss' + ps


  ;=== Tagnames to define structures later.
  ;=== Need to define them now for checking inputs.
  ea_tagnames = ['type', 'eff_str_version', 'eff_str_descr', 'name', $
                 'history', 'comments'                                ]
  sr_tagnames = ['type', 'srs_str_version', 'srs_str_descr', 'name', $
                 'gain_used', 'history', 'comments'                   ]
  em_tagnames = ['type', 'ems_str_version', 'ems_str_descr', 'name', $
                 'abund_model', 'ioneq_model', 'dens_model',         $
                 'data_files', 'history', 'comments'                  ]
  tr_tagnames = ['type', 'trs_str_version', 'trs_str_descr', 'name',   $
                 'temp', 'temp_units', 'temp_resp', 'temp_resp_units', $
                 'length', 'gain_used', 'wave_min', 'wave_max',        $
                 'confg', 'contam', 'fcontam', 'effar', 'sprsp',       $
                 'emiss', 'history', 'comments']
  wr_tagnames = ['type', 'wrs_str_version', 'wrs_str_descr', 'name', 'confg', $
                 'contam', 'fcontam', 'effar', 'sprsp', 'history', 'comments']
;  tr_tagnames = ['type', 'trs_str_version', 'trs_str_descr', 'name',   $
;                'temp', 'temp_units', 'temp_resp', 'temp_resp_units', $
;                'length', 'gain_used', 'wave_min', 'wave_max',        $
;                'confg', 'contam',                                    $
;                'effar', 'sprsp', 'emiss',                            $
;                'history', 'comments'                                  ]
;  wr_tagnames = ['type', 'wrs_str_version', 'wrs_str_descr', 'name',      $
;                 'confg', 'contam', 'effar', 'sprsp', 'history', 'comments']



  if q_vb then print, prognam+': Performing initial setup... OK'


; === Check inputs ===============================

  if q_vb then print, prognam+': Checking inputs...'

  ;=== Check number of inputs.
  if ((n_params() + (qapec or qchianti)) ne 2) then qabort = 1B
  if (qapec and qchianti) then qabort = 1B
  if (qabort eq 1) then begin
    if (not q_qt) then box_message, $
        [prognam+': Wrong number of inputs.', $
         prognul+'  See program header regarding input options for', $
         prognul+'  the emission model. Aborting...']
    return, 0
  endif

  ;=== Check type of WAVE_RESP.
  if not required_tags(wave_resp[0], wr_tagnames, missing=missing) $ 
      then qabort = 1B
  if (qabort eq 1) then begin
    if (not q_qt) then box_message, $
        [prognam+': Input WAVE_RESP has wrong type. ', $
         prognul+'  See program header and <make_xrt_wave_resp.pro>.', $
         prognul+'  Aborting...']
    return, 0
  endif


  ;=== Check type of WAVE_RESP.EFFAR.
  if not required_tags(wave_resp[0].effar, ea_tagnames, missing=missing) $
      then qabort = 1B
  if (qabort eq 1) then begin
    if (not q_qt) then box_message, $
        [prognam+': Input WAVE_RESP.EFFAR has wrong type. ', $
         prognul+'  See program header and <make_xrt_wave_resp.pro>.', $
         prognul+'  Aborting...']
    return, 0
  endif


  ;=== Check type of WAVE_RESP.SPRSP.
  if not required_tags(wave_resp[0].sprsp, sr_tagnames, missing=missing) $
      then qabort = 1B
  if (qabort eq 1) then begin
    if (not q_qt) then box_message, $
        [prognam+': Input WAVE_RESP.SPRSP has wrong type. ', $
         prognul+'  See program header and <make_xrt_wave_resp.pro>.', $
         prognul+'  Aborting...']
    return, 0
  endif

  ;=== Check type of OUTFILE.
  if qoutfile then begin
    if ((datatype(outfile[0]) ne 'STR') $
        or (n_elements(outfile) ne 1)) then begin
      if (q_qt eq 0) then box_message, $
          [prognam+': Output OUTFILE has wrong type. ', $
           prognul+'  See program header.', $
           prognul+'  Proceeding, but will not write to OUTFILE...']
      qoutfile = 0B
    endif
  endif


  if q_vb then print, prognam+': Checking inputs... OK'


; === Interpret emission model keywords. ===========

  if (qapec or qchianti) then begin

    ;; This is a temporary condition (hopefully).
    if (qchianti eq 1) then begin
      if (not q_qt) then box_message, $
        [prognam+': The CHIANTI_DEFAULT keyword is not yet implemented.', $
         prognul+'  Use APEC_DEFAULT or EMISS_MODEL. Aborting...']
      qabort = 1B
      return, 0
    endif

    if q_vb then print, prognam+': Getting emission model...'

    file_string = 'XRT_emiss_model.default_'
    if (qapec eq 1) then file_string = file_string + 'APEC' $
                    else file_string = file_string + 'CHIANTI'
    file0 = emiss_dir + file_string

    restgenx, file=file0, emiss_model

    if q_vb then print, prognam+': Getting emission model... OK'

  endif


; === Check the emission model input. ==============

  if q_vb then print, prognam+': Checking emission model...'

  ;=== Check type of EMISS_MODEL.
  if not required_tags(emiss_model, em_tagnames, missing=missing) $
      then qabort = 1B
  if qabort then begin
    if (not q_qt) then box_message, $
        [prognam+': Input EMISS_MODEL has wrong type. ', $
         prognul+'  See program header and <make_xrt_emiss_model.pro>.', $
         prognul+'  Aborting...']
    return, 0
  endif


  ;=== Check EMISS_MODEL whether scalar.
  if (n_elements(emiss_model) ne 1) then begin
    if (not q_qt) then box_message, $
        [prognam+': Input EMISS_MODEL may not be structure array. ', $
         prognul+'  See program header. Aborting...']
    qabort = 1B
    return, 0
  endif

  if q_vb then print, prognam+': Checking emission model... OK'


; === Define and initialize the substructures. =====

  if q_vb then print, prognam+': Initializing substructures...'

  nch = n_elements(wave_resp)   ;; Number of channels to process.
  ntm = emiss_model.tlength     ;; Number of temps in the emission model.

  ;=== Define the {XRT_eff_area_ref} stucture.
  eff_str_version = 'XRT_EFF_AREA_REF_V0001'
  ea_ref = create_struct(name=eff_str_version, ea_tagnames, $
                         '', '', '', '',                    $
                         strarr(3), strarr(5)                )

  ;; And initialize the tags.
  ea_ref.type            = 'XRT_EFF_AREA_REF'
  ea_ref.eff_str_version = eff_str_version
  ea_ref.eff_str_descr   = 'Records which effective area was used to '    $
                           + 'calculate the temperature response for an ' $
                           + 'XRT channel, for a given thickness of the ' $
                           + 'CCD contamination layer.'
  ea_ref = replicate(ea_ref, nch)
  ea_ref.name     = wave_resp.effar.name
  ea_ref.history  = wave_resp.effar.history
  ea_ref.comments = wave_resp.effar.comments


  ;=== Define the {XRT_spec_resp_ref} stucture.
  srs_str_version = 'XRT_SPEC_RESP_REF_V0001'
  sr_ref = create_struct(name=srs_str_version, sr_tagnames, $
                         '', '', '', '',                    $
                         -1.0, strarr(3), strarr(5)          )

  ;; And initialize the tags.
  sr_ref.type            = 'XRT_SPEC_RESP_REF'
  sr_ref.srs_str_version = srs_str_version
  sr_ref.srs_str_descr   = 'Records which spectral response function '    $
                           + 'was used to calculate the temperature '     $
                           + 'response function for an XRT channel, for ' $
                           + 'a given thickness of the CCD '              $
                           + 'contamination layer.'
  sr_ref = replicate(sr_ref, nch)
  sr_ref.name      = wave_resp.sprsp.name
  sr_ref.gain_used = wave_resp.sprsp.gain_used
  sr_ref.history   = wave_resp.sprsp.history
  sr_ref.comments  = wave_resp.sprsp.comments


  ;=== Define the {XRT_emiss_model_ref} stucture.
  ems_str_version = 'XRT_EMISS_MODEL_REF_V0001'
  em_ref = create_struct(name=ems_str_version, em_tagnames, $
                         '', '', '', '',                    $
                         '', '', '',                        $
                         strarr(5), strarr(3), strarr(5)     )

  ;; And initialize the tags.
  em_ref.type            = 'XRT_EMISS_MODEL_REF'
  em_ref.ems_str_version = ems_str_version
  em_ref.ems_str_descr   = 'Records which solar spectral emission model' $
                           + ' was used to calculate the temperature '   $
                           + 'response for an XRT channel.'
  em_ref.name            = emiss_model.name
  em_ref.abund_model     = emiss_model.abund_model
  em_ref.ioneq_model     = emiss_model.ioneq_model
  em_ref.dens_model      = emiss_model.dens_model
  em_ref.data_files      = emiss_model.data_files
  em_ref.history         = emiss_model.history
  em_ref.comments        = emiss_model.comments


  if q_vb then print, prognam+': Initializing substructures... OK'


; === Generate and initialize the  ===============
; === {XRT_temp_resp} structure. =================

  if q_vb then print, prognam+': Initializing the temp. structure...'


  ;=== Define the {XRT_temp_resp} stucture.
  trs_str_version = '<anonymous>'
  tresp = create_struct(tr_tagnames,                                 $
                        '', '', '', '',                              $
                        emiss_model.temp, '', emiss_model.temp, '',  $
                        0L, -1.0, 0.0, 0.0,                          $
                        wave_resp[0].confg, wave_resp[0].contam,     $
                        wave_resp[0].fcontam, ea_ref[0], sr_ref[0], em_ref, $
                        strarr(3), strarr(5)                          )

  ;; And initialize the tags.
  tresp.type            = 'XRT_TEMP_RESP'
  tresp.trs_str_version = trs_str_version
  tresp.trs_str_descr   = 'Provides temperature response function for '  $
                           + 'an XRT channel, for a given thickness of ' $
                           + 'the CCD contamination layer, and for a '   $
                           + 'given plasma emission model.'
  tresp.temp            = emiss_model.temp
  tresp.temp_units      = 'K'
  tresp.temp_resp       = 0.0
  tresp.temp_resp_units = 'DN cm^5 s^-1 pix^-1'
  tresp.length          = emiss_model.tlength
  tresp.gain_used       = -1    ; *** change from -1 default to 59.1 & back
  tresp.wave_min        = 0.0
  tresp.wave_max        = 0.0
  tresp.emiss           = em_ref

  tresp = replicate(tresp, nch)
  tresp.name            = wave_resp.name + '; ' + emiss_model.name
  copy_struct, wave_resp.confg,  tresp, select='confg',  /recur_to
  copy_struct, wave_resp.contam, tresp, select='contam', /recur_to
  copy_struct, wave_resp.fcontam, tresp, select='fcontam', /recur_to
  copy_struct, wave_resp.effar,  tresp, select='effar',  /recur_to
  copy_struct, wave_resp.sprsp,  tresp, select='sprsp',  /recur_to

  if q_vb then print, prognam+': Initializing the temp. structure... OK'


; === Calculate temperature responses ============

  if q_vb then print, prognam+': Calculating the T-responses...'

  ;; TR(T) = integral[ EM(w,T) * SR(w) * dw ] 
  ;; where TR = Temp.Resp., EM = Emiss.Model, SR = Spec.Resp.,
  ;; T = Temp, and w = wavelength.
  ;; We interpolate SR onto the wavelength grid of EM.
  ;; The integration bins are determined by the spacing of
  ;; EM's wavelength grid (which is not assumed to be regular).

  ;=== Some basic program values.
  ntm = emiss_model.tlength ;; Number of temps in the emission model.


  ; === Start wavelength grid for integration.
  ;; Pad ends for calculation of deltas.
  wlen          = emiss_model.wlength
  wave0         = [0.0, reform(emiss_model.wave[0:wlen-1]), 0.0]
  wave0[0]      = (2 * wave0[1]) - wave0[2]
  wave0[wlen+1] = (2 * wave0[wlen]) - wave0[wlen-1]
  d_wave0       = (shift(wave0,-1) - shift(wave0,1)) / 2.
  wave1         = wave0[1:wlen]
  d_wave1       = d_wave0[1:wlen]


  ; === Loop over WAVE_RESP array of channels 

  for ichn = 0,(nch-1) do begin

    if q_vb then print, prognam+': Working on channel ' +   $
                        strcompress(ichn+1,/rem) + ' of ' + $
                        strcompress(nch,/rem) + '.'

    ;; Get wavelength grid of SR.
    rlen = wave_resp[ichn].sprsp.length
    rwave0 = reform(wave_resp[ichn].sprsp.wave[0:rlen-1])

    ;; Make sure there is overlap between the grids.
    ss = where( (wave1 ge min(rwave0)) and (wave1 le max(rwave0)), count)
    if (count eq 0) then begin
      if q_vb then begin
        print, prognam+': The wavelength grids for EMISS_MODEL and '
        print, prognul+'  WAVE_RESP['+strcompress(ichn,/rem)+ $
                                      '].SPRSP do not overlap.' 
        print, prognul+'  Zeroing out the corresponding temp. response...'
      endif
      tresp[ichn].temp_resp = 0.0


    ;; If the grids overlap, then continue with calculation.
    endif else begin

      ;; Establish the new mutual wavelength grid, wave2.
      wave2 = wave1[ss]
      d_wave2 = d_wave1[ss]

      ;; Record integration range into TR structure.
      tresp[ichn].wave_min = min(wave2)
      tresp[ichn].wave_max = max(wave2)

      ;; Interpolate the SR onto wave2.
      spec_resp0 = reform(wave_resp[ichn].sprsp.spec_resp[0:rlen-1])
      spec_resp2 = interpol(spec_resp0, rwave0, wave2, /quad)

      ;; Prepare the emission spectrum.
      spectrum = emiss_model.spec[ss,0:ntm-1]

      ;; Conceptually, one wants to loop over the temperatures in
      ;; the emission spectrum, and perform each wavelength integral
      ;; independently. However, we can achieve the same thing with
      ;; some artful matrix multiplication...

      tr2 = transpose((spec_resp2 * d_wave2) # spectrum)

      ;; Assign results into TR structure.
      tresp[ichn].temp_resp[0:ntm-1] = tr2

    endelse    ;; (wavelength grids overlap)

  endfor ;; ("ichn" channel loop)


  if q_vb then print, prognam+': Calculating the T-responses... OK'


; === Deal with units ============================

  if q_vb then print, prognam+': Checking units...'

  ;; If there was no request to obtain "el..." units,
  ;; then convert to "DN..." units for the TEMP_RESP.
  if (qelunits eq 0) then begin

    tresp.temp_resp_units = 'DN cm^5 s^-1 pix^-1'

    ;; If SPEC_RESP was in [el cm^2 sr ph^-1 pix^-1],
    ;; then must apply gain to make TEMP_RESP have
    ;; units of [DN cm^5 s^-1 pix^-1].
    ss1 = where(wave_resp.sprsp.spec_resp_units       $
                eq 'el cm^2 sr ph^-1 pix^-1', count1)
    if (count1 gt 0) then begin

      ;; Assume the default (R-port) gains...
      gains = wave_resp[ss1].confg.ccd.gain_r

      ;; Apply gain values to SPEC_RESP.
      for ii = 0,(count1-1) do begin
        tresp[ss1[ii]].temp_resp = tresp[ss1[ii]].temp_resp / gains[ii]
      endfor

      ;; Record gain values used.
      tresp[ss1].gain_used = gains

    endif


  ;; Else if /el_units, 
  ;; then convert (or preserve) to the correct units.
  endif else begin

    tresp.temp_resp_units = 'el cm^5 s^-1 pix^-1'

    ;; If SPEC_RESP was in [DN cm^2 sr ph^-1 pix^-1],
    ;; then must unapply gain to make TEMP_RESP have
    ;; units of [el cm^5 s^-1 pix^-1].
    ss1 = where(wave_resp.sprsp.spec_resp_units       $
                eq 'DN cm^2 sr ph^-1 pix^-1', count1)
    if (count1 gt 0) then begin

      ;; Get gains from SPEC_RESP...
      gains = wave_resp[ss1].sprsp.gain_used

      ;; Apply gain values to TEMP_RESP.
      for ii = 0,(count1-1) do begin
        tresp[ss1[ii]].temp_resp = tresp[ss1[ii]].temp_resp * gains[ii]
      endfor

      ;; Record gain values used.
      tresp[ss1].gain_used = gains

    endif

  endelse


  if q_vb then print, prognam+': Checking units... OK'


; === Finish =====================================

  get_utc, utc, /stime
  tresp.history[0] = prognam+' '+progver+': ('+utc+' UTC) Generated {' $
                      + tresp.type+'} structure with name "'           $
                      + tresp.name+'".'

  if qoutfile then begin
    if q_vb then print, prognam+': Writing OUTFILE...'
      savegenx, file=outfile, tresp
    if q_vb then print, prognam+': Writing OUTFILE...OK'
  endif

  if q_vb then box_message, prognam+': ... Finished.'

  if qstop then stop

  return, tresp


END ;======================================================================
