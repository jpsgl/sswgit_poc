FUNCTION get_xrt_aperture, as_string=as_string,                   $
                           as_structure=as_structure, qabort=qabort

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       GET_XRT_APERTURE
;
; CATEGORY:
;       
;       Instrument configuration
;
; PURPOSE:
;       
;       Get the XRT x-ray aperture area for the flight configuration.
;       WARNING: This routine has been deprecated and is known to provide
;       incorrect information. The correct aperture area can be found in
;       the "XRT_wave_resp" array returned by <make_xrt_wave_resp.pro>.
;
; CALLING SEQUENCE:
;
;       Result = GET_XRT_APERTURE([,/as_string], [,/as_structure])
;       
; INPUTS:
;       
;       No parameters.
;
; KEYWORDS:
;
;       /AS_STRING    - [Optional] (Boolean)
;                       Option to return the aperture area in a string.
;       /AS_STRUCTURE - [Optional] (Boolean)
;                       Option to return the aperture area in a structure.
;
; OUTPUTS:
;
;       Result        - [Mandatory] (various) The default value is 
;                       returned as a double-float, in units of 'cm^2'. 
;                       Keywords allow for other formats.
;       QABORT        - [Optional] (Boolean) Indicates that the program
;                       exited gracefully without completing. (Might be
;                       useful for calling programs.)
;                       0: Program ran to completion.
;                       1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> help, get_xrt_aperture()
;       <Expression>    DOUBLE    =        2.5425000
;
;       As a string:
;       IDL> help, get_xrt_aperture(/as_string)
;       <Expression>    STRING    = '2.5425d cm^2'
;
;       As a structure:
;       IDL> help, get_xrt_aperture(/as_structure), /st
;       ** Structure <26683e4>, 2 tags, length=20, data length=20, refs=1:
;          AREA            DOUBLE           2.5425000
;          UNITS           STRING    'cm^2'
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) The geometric aperture area = 3.39 cm^2. However, there is
;          also a 0.75 masking coefficent applied for the trifoil.
;
;       2) WARNING: This routine has been deprecated and is known to provide
;          incorrect information. The correct aperture area can be found in
;          the "XRT_wave_resp" array returned by <make_xrt_wave_resp.pro>.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-May-15' ;--- (M.Weber) Written. (Based on heavily on
;                             earlier work, though.)
progver = 'v2008-Oct-01' ;--- (M.Weber) This routine has been 
;                             deprecated. See Note #2.
;                      
;-
; =========================================================================


;=== Initial setup =======================================

  qabort = 0B
  prognam = 'GET_XRT_APERTURE'

  ;=== Set some keyword Booleans.
  q_stc = keyword_set(as_structure)
  q_str = keyword_set(as_string) and (q_stc eq 0)
  q_flt = (q_stc eq 0) and (q_str eq 0)


;=== Format data ========================================

  case 1 of

    q_flt: aperture = 2.5425d ;; cm^2

    q_str: aperture = '2.5425d cm^2'

    q_stc: aperture = {area:2.5425d, units:'cm^2'}

    ;; Not sure how it could arrive here, but I am a big believer in
    ;; putting "else"s in "case"s.
    else:    begin
               print, prognam+': Unexpected crash in case statement.' $
                      + 'Aborting...'
               qabort = 1B
               return, 0
             end

  endcase


;=== Finish ==============================================

  return, aperture


END ;======================================================================
