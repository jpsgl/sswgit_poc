FUNCTION get_xrt_chn_trans, trans_file=trans_file,        $
                            verbose=verbose, quiet=quiet, $
                            qabort=qabort,   qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       GET_XRT_CHN_TRANS
;
; CATEGORY:
;       
;       Instrument response
;
; PURPOSE:
;       
;       Read in the net fractional transmission for each XRT x-ray channel.
;       WARNING: This routine has been deprecated. See Note #2.
;
; CALLING SEQUENCE:
;
;       Result = GET_XRT_CHN_TRANS([,trans_file=trans_file], [,/verbose] $
;                                  [,/quiet] [,/qstop] [,qabort=qabort]   )
;       
; INPUTS:
;       
;       No parameters.
;
; KEYWORDS:
;       
;       TRANS_FILE - [Optional] (string scalar)
;                    Filename of a file which contains a structure of 
;                    type="chn_trans". This file defaults to
;                    $SSW_XRT_RESPONSE/xrt_channel_transmissions.geny .
;                    (See "Result".)
;	/VERBOSE   - [Optional] (Boolean) If set, print out extra
;                    information. Overrides "/quiet" (see Note #3).
;       /QUIET     - [Optional] (Boolean) If set, suppress messages.
;                    (see Note #1.)
;       /QSTOP     - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;
;       Result - [Mandatory] (structure)
;                Here is an example of the "chn_trans" structure 
;                definition for <calc_xrt_chn_trans.pro> 
;                (progver="v2007-May-15"):
;                  TYPE            STRING    'chn_trans'
;                  CHANNEL_NAME    STRING    
;                  WAVE            FLOAT     Array[5000]
;                  WAVE_UNITS      STRING    'Angstroms'
;                  TRANS           FLOAT     Array[5000]
;                  LENGTH          LONG     
;                  DATA_FILES      STRING    
;                The wavelengths are Result.WAVE[0:Result.LENGTH-1].
;                The transmissions are Result.TRANS[0:Result.LENGTH-1].
;       QABORT - [Optional] (Boolean) Indicates that the program
;                exited gracefully without completing. (Might be
;                useful for calling programs.)
;                0: Program ran to completion.
;                1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> chn_trans = get_xrt_chn_trans()
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
;       2) This routine has been deprecated. It is not compatible with 
;          recent versions of the response routines, and does not account
;          for the CCD contamination layer. The channel transmissions are
;          now correctly accounted for by the program 
;          <make_xrt_wave_resp.pro> when it calculates the effective areas.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-May-15' ;--- (M.Weber) Written. (Based on heavily on
;                             earlier work, though.)
progver = 'v2007-Aug-15' ;--- (M.Weber) Switched to using <concat_dir>.
progver = 'v2008-Oct-01' ;--- (M.Weber) This routine has been deprecated.
;                             See Note #2.
;                      
;-
; =========================================================================


;=== Initial setup =======================================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (not q_vb)

  ;=== Initialize program constants.
  prognam = 'GET_XRT_CHN_TRANS'
  prognul = '                 '
  XRT_RESPONSE = get_logenv('$SSW_XRT_RESPONSE')
  def_file = 'xrt_channel_transmissions.geny'

  ;=== Announce version of <read_xrt.pro>.
  if q_vb then box_message, prognam+': Running ' + progver + '.'
  if q_vb then print, prognam+': Performing initial setup...'

  ;=== Set some keyword Booleans.
  q_file  = keyword_set(trans_file)
  qstop   = keyword_set(qstop)
  qabort  = 0B

  ;=== Test "trans_file".
  if q_file then begin
    if ((datatype(trans_file) ne 'STR') or (strlen(trans_file) le 2) $
        or (n_elements(trans_file) ne 1)) then begin
      trans_file = concat_dir(XRT_RESPONSE, def_file)
      if q_vb then print, prognam+': The provided TRANS_FILE is not a ' $
                          + 'simple, single filename.'
      if q_vb then print, prognul+'  Using the default file instead:'
      if q_vb then print, trans_file
    endif 
  endif else begin
    trans_file = concat_dir(XRT_RESPONSE, def_file)
  endelse

  if q_vb then print, prognul+'                          ...OK'


;=== Read in data ========================================

  if q_vb then print, prognam+': Reading in data...'

if qstop then stop

  restgenx, chn_trans, file=trans_file, inquire=q_vb, quiet=q_qt

  if required_tags(chn_trans,'type') then begin
    if (chn_trans[0].type eq 'chn_trans') then begin
      ;; OK
    endif else begin
      if (not q_qt) then box_message, prognam+': File does not contain' $
        + ' a recognized structure type. Aborting...'
      qabort = 1B
      return, 0
    endelse
  endif else begin
    if (not q_qt) then box_message, prognam+': File does not contain' $
      + ' a recognized structure type. Aborting...'
      qabort = 1B
      return, 0
  endelse

  if q_vb then print, prognul+'                 ...OK'


;=== Finish ==============================================

  return, chn_trans


END ;======================================================================
