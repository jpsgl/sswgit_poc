FUNCTION get_xrt_foclen, as_string=as_string, as_structure=as_structure, $
                         qabort=qabort

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       GET_XRT_FOCLEN
;
; CATEGORY:
;       
;       Instrument configuration
;
; PURPOSE:
;       
;       Get the XRT focal length for the flight configuration.
;
; CALLING SEQUENCE:
;
;       Result = GET_XRT_FOCLEN([,/as_string], [,/as_structure])
;       
; INPUTS:
;       
;       No parameters.
;
; KEYWORDS:
;
;       /AS_STRING    - [Optional] (Boolean)
;                       Option to return the focal length in a string.
;       /AS_STRUCTURE - [Optional] (Boolean)
;                       Option to return the focal length in a structure.
;
; OUTPUTS:
;
;       Result        - [Mandatory] (various) The default value is 
;                       returned as a double-float, in units of 'cm'. 
;                       Keywords allow for other formats.
;       QABORT        - [Optional] (Boolean) Indicates that the program
;                       exited gracefully without completing. (Might be
;                       useful for calling programs.)
;                       0: Program ran to completion.
;                       1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> help, get_xrt_foclen()
;       <Expression>    DOUBLE    =        270.04490
;
;       As a string:
;       IDL> help, get_xrt_foclen(/as_string)
;       <Expression>    STRING    = '270.0449 cm'
;
;       As a structure:
;       IDL> help, get_xrt_foclen(/as_structure), /st
;       ** Structure <2667af4>, 2 tags, length=20, data length=20, refs=1:
;          LENGTH          DOUBLE           270.04490
;          UNITS           STRING    'cm'
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) Source = Paul Reid's calculation from the optic center to the 
;          focal plane at infinite source distance, for the best on-axis 
;          focus.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-May-15' ;--- (M.Weber) Written. (Based on heavily on
;                             earlier work, though.)
;                      
;-
; =========================================================================


;=== Initial setup =======================================

  qabort = 0B
  prognam = 'GET_XRT_FOCLEN'

  ;=== Set some keyword Booleans.
  q_stc = keyword_set(as_structure)
  q_str = keyword_set(as_string) and (q_stc eq 0)
  q_flt = (q_stc eq 0) and (q_str eq 0)


;=== Format data ========================================

  case 1 of

    q_flt: foclen = 270.0449d ;; cm

    q_str: foclen = '270.0449 cm'

    q_stc: foclen = {length:270.0449d, units:'cm'}

    ;; Not sure how it could arrive here, but I am a big believer in
    ;; putting "else"s in "case"s.
    else:  begin
             print, prognam+': Unexpected crash in case statement.' $
                    + 'Aborting...'
             qabort = 1B
             return, 0
           end

  endcase


;=== Finish ==============================================

  return, foclen


END ;======================================================================
