
pro xrt_chan2fw, channel, fw1, fw2

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       XRT_CHAN2FW 
;
; CATEGORY:
;       
;       Instrument configuration
;
; PURPOSE:
;       
;       Get FW numerical settings from XRT channel structure 
;
; CALLING SEQUENCE:
;
;       XRT_CHAN2FW, channel, fw1, fw2, [qabort]
;       
; INPUTS:
;       
;       CHANNEL - [Mandatory] structure of type {XRT_chn_config_vNNNN} 
;                     This is a structure of type
;                     "XRT_chn_config" and with a structure name
;                     of the form {XRT_chn_config_vNNNN}. For a
;                     more complete description of this complicated
;                     structure, see the XRT Analysis Guide. The
;                     base set of channels is provided in the XRT SSWIDL
;                     tree by a file with a pathname like this:
;       $SSW_XRT/idl/response/channels/xrt_channels_vNNNN.geny
;
;
; KEYWORDS:
;
;       None
;
; OUTPUTS:
;
;          FW1  - [Mandatory] integer Filter wheel 1 numerical position 
;          FW2  - [Mandatory] integer Filter wheel 2 numerical position 
;       QABORT        - [Optional] (Boolean) Indicates that the program
;                       exited gracefully without completing. (Might be
;                       useful for calling programs.)
;                       0: Program ran to completion.
;                       1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> get_xrt_chan2fw, channel, fw1, fw2 
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) Does minimal error checking 
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2009-Nov-15' ;--- (K.Reeves) Written. 
progver = 'v2009-Dec-02' ;--- (S.Saar) added header, "Open" case, qabort,
;                        ;----  some error checking.
;                      
;-
; =========================================================================

prognam='XRT_CHAN2FW'
prognul='           '
qabort=0                                                   ; set abort flag = OK

if (datatype(channel) ne 'STC') then begin
   box_message,                   $
     [prognam+': Input CHANNEL must be a structure. ', $
     prognul+'  See program header. Aborting...'     ]
   qabort = 1B
endif else begin
   if required_tags(channel, 'NAME') eq 0 then begin
      box_message,                   $
        [prognam+': Input CHANNEL does not have required tags. ', $
        prognul+'  See program header. Aborting...'     ]
      qabort = 1B
   endif
endelse



;; Takes an xrt channel structure and returns the filter number on
;; each filter wheel.

if qabort ne 1 then begin 

  case channel.name of
     'Open': begin
        fw1 = 0
        fw2 = 0
     end
     'Al-mesh': begin 
        fw1 = 0 
        fw2 = 1
     end
     'Al-poly': begin 
        fw1 = 1 
        fw2 = 0
     end
     'C-poly': begin 
        fw1 = 2 
        fw2 = 0
     end
     'Ti-poly': begin 
        fw1 = 0 
        fw2 = 2
     end
     'Be-thin': begin 
        fw1 = 3 
        fw2 = 0
     end
     'Be-med': begin 
        fw1 = 4 
        fw2 = 0
     end
     'Al-med': begin 
        fw1 = 5 
        fw2 = 0
     end
     'Al-thick': begin 
        fw1 = 0 
        fw2 = 4
     end
     'Be-thick': begin 
        fw1 = 0
        fw2 = 5
     end
     'Al-poly/Al-mesh': begin 
        fw1 = 1 
        fw2 = 1
     end
     'Al-poly/Ti-poly': begin 
        fw1 = 1 
        fw2 = 2
     end
     'Al-poly/Al-thick': begin 
        fw1 = 1 
        fw2 = 4
     end
     'Al-poly/Be-thick': begin 
        fw1 = 1 
        fw2 = 5
     end
     'C-poly/Ti-poly': begin 
        fw1 = 2 
        fw2 = 2
     end
     'C-poly/Al-thick': begin 
        fw1 = 2
        fw2 = 4
     end
     else: begin 
         print, 'no valid filter name'
         qabort=1B
     end
  endcase
endif


END ;======================================================================
