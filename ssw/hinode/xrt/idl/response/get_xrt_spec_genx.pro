FUNCTION get_xrt_spec_genx, infile=infile, quiet=quiet, verbose=verbose, $
                            qabort=qabort, qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       
;       GET_XRT_SPEC_GENX
;
; CATEGORY:
;       
;       Instrument response
;
; PURPOSE:
;
;       Retrieve a spectral emission model which has been stored in a
;       standardized structure format. (See <mk_xrt_spec_genx.pro> to
;       put a model into the correct format.) If no INFILE, then return
;       the default XRT model. This approach enables a modular DIY 
;       approach to the XRT x-ray response functions.
;       WARNING: This routine has been deprecated. See Note #3.
;
; CALLING SEQUENCE:
;
;       Result = GET_XRT_SPEC_GENX([,infile=infile] [,/verbose] [,/quiet] 
;                          [,qabort=qabort] [,/qstop])
;       
; INPUTS:
;       
;       No parameters.
;
; KEYWORDS:
;
;       INFILE   - [Optional] (string scalar)
;                  If INFILE is not given, a default file will be used.
;                  A "genx" type file that contains an XRT structure
;                  of type="spectrum". These can be generated with
;                  <mk_xrt_spec_genx.pro>. (See that code for a 
;                  detailed description of the contents.) See "Result"
;                  for description of the "spectrum" structure.
;       /VERBOSE - [Optional] (Boolean) If set, print out extra
;                  information. Overrides "/quiet" (see Note #1).
;       /QUIET   - [Optional] (Boolean) If set, suppress messages
;                  (see Note #1).
;       /QSTOP   - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;
;       Result   - [Mandatory] (structure scalar)
;                  Return the constructed spectral emission model
;                  structure.
;                    TYPE            STRING    'spectrum'
;                    NAME            STRING
;                    WAVE            FLOAT     Array[5000]
;                    WAVE_UNITS      STRING    'Angstroms'
;                    WLENGTH         LONG
;                    TEMP            FLOAT     Array[50]
;                    TEMP_UNITS      STRING    'log K'
;                    TLENGTH         LONG
;                    SPEC            FLOAT     Array[5000, 50]
;                    SPEC_UNITS      STRING    'ph cm^3 s^-1 sr^-1 A^-1'
;                    DATA_FILES      STRING    Array[5]
;                    HISTORY         STRING    Array[5]
;                    COMMENTS        STRING    Array[5]
;                  TYPE = 'spectrum', common to all XRT spectrum structures.
;                  NAME = unique name/ID for a particular instance.
;                  WLENGTH = N_wavelengths (may be less than 5000).
;                  TLENGTH = N_temperatures (may be less than 50).
;       QABORT   - [Optional] (Boolean) Indicates that the program
;                  exited gracefully without completing. (Might be
;                  useful for calling programs.)
;                  0: Program ran to completion.
;                  1: Program aborted before completion.
;
; EXAMPLES:
;      
;       Basic usage:
;       IDL> spec = get_xrt_spec_genx()
;
;       Use an alternate (pre-processed) spectral model:
;       IDL> spec = get_xrt_spec_genx(infile='xrt_alternate_spec.geny')
;
; COMMON BLOCKS:
;
; 	none 
;
; NOTES:
;
;       1) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt and not q_vb")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not (q_qt or q_vb)")
;
;       2) This program will abort gracefully if it does not recognize 
;          the format or type of the spectrum structure read in.
;
;       3) This routine has been deprecated. It is not compatible with
;          the most recent response codes. Use <make_xrt_emiss_model.pro>
;          instead.
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be 
;       directed to this email address: 
;                xrt_manager ~at~ head.cfa.harvard.edu 
;       
; MODIFICATION HISTORY:
;
progver = 'v2007-May-16' ;--- (M.Weber) Written. (Based on heavily on
;                             earlier work, though.)
progver = 'v2007-Aug-15' ;--- (M.Weber) Switched to using <concat_dir>.
progver = 'v2008-Oct-01' ;--- (M.Weber) This routine has been deprecated.
;                             See Note #3.
;                      
;-
; =========================================================================


; === Initial setup ==============================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (q_vb eq 0)

  ;=== Initialize program constants.
  prognam = 'GET_XRT_SPEC_GENX'
  prognul = '                 '
  XRT_RESPONSE = get_logenv('$SSW_XRT_RESPONSE')
  def_infile = concat_dir(XRT_RESPONSE, 'xrt_apec_09_c.geny')

  ;=== Set some keyword Booleans.
  qinfile  = keyword_set(infile)
  qstop    = keyword_set(qstop)
  qabort   = 0B


; === Check inputs ===============================

  if qinfile then begin
    if ((datatype(infile) ne 'STR') or (n_elements(infile) ne 1)) then begin
      if (q_qt eq 0) then box_message, [prognam+': INFILE not a scalar ' $
                                                  + 'string. Aborting...']
      qabort = 1B
      return, 0
    endif

    if (not file_exist(infile)) then begin
      if (q_qt eq 0) then box_message, [prognam+': Cannot find INFILE. ' $
                                                          + 'Aborting...']
      qabort = 1B
      return, 0
    endif

  endif else infile = def_infile    


; === Read infile, check results =================

  restgenx, file=infile, spec

  if not required_tags(spec, 'type,name,wave,temp,spec,data_files,' $
                                              + 'history') then begin
    if (q_qt eq 0) then box_message, [prognam+': Do not recognize ' $
                                      + 'this structure. Aborting...']
      qabort = 1B
      return, 0
  endif

  if (spec.type[0] ne 'spectrum') then begin
    if (q_qt eq 0) then box_message, [prognam+': Do not recognize ' $
                                      + 'this structure. Aborting...']
      qabort = 1B
      return, 0
  endif


; === Finish =====================================

  if qstop then stop

  return, spec


END ;======================================================================
