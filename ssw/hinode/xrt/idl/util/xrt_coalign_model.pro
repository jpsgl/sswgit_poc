pro xrt_coalign_model, date0, c_xrt_n_a, c_xrt_n_b, c_xrt_e_a, c_xrt_e_b, $
  file= file, dir0=dir0, verbose=verbose

;+
; NAME:
;   XRT_COALIGN_MODEL
; PURPOSE:
;   Fine a suitable coalignment model file and read it.
; INPUTS:
;   date0 - the date of the dataset to be coaligned.
;            format is a string eg '20070325'
; OPTIONAL INPUTS:
;   file - the name of coalignment model file, such as
;             'alignment_model070325.txt'
;   dir0 - if need to change the directory definition for model file
; OUTPUTS:
;   Coefficients of polynominal fitting models
;     c_xrt_n_a - N-S direction, in reference of UFSS-A
;     c_xrt_n_b - N-S direction, in reference of UFSS-B
;     c_xrt_e_a - E-W direction, in reference of UFSS-A
;     c_xrt_e_b - E-W direction, in reference of UFSS-B
; HISTORY:
;   2007/05/03 T.Shimizu  initial for general use
;   2008/04/14 T.Shimizu  minar modification
;-

;dir0= '/home/shimizu/alignment/jitter/'   ; directory of alignment files
if keyword_set(dir0) then dir0= dir0 else $
dir0= getenv('SSWDB') + '/hinode/gen/alignment/model/'

;ymd0= '20'+ strmid(date0,0,2)+'-'+strmid(date0,2,2)+'-'+strmid(date0,4,2)
;jd0=  anytim2jd(ymd0)
;file0= dir0 + 'alignment_model' + date0 + '.txt'
if keyword_set(file) then file0= dir0 + file else begin

 ymd0= '20'+ strmid(date0,0,2)+'-'+strmid(date0,2,2)+'-'+strmid(date0,4,2)
 jd0=  anytim2jd(ymd0)

  files= find_file(dir0 + 'alignment_model*.txt')
  pos= strpos(files, 'model')
  siz= size(files)
  da= strarr(siz(1))
  for i=0, siz(1)-1 do da[i]= strmid(files[i],  pos[i] + 5, 6)
  ymd= '20'+ strmid(da,0,2)+'-'+strmid(da,2,2)+'-'+strmid(da,4,2)
  jd= anytim2jd(ymd)

  x= where(abs(jd.int - jd0.int) eq min(abs(jd.int - jd0.int)))   ; closest time

  file0= files(x[0])

endelse

if keyword_set(verbose) then print, '% Reading coalignment model file: ', file0
openr, unit, file0, /get_lun
 a= strarr(64)
 readf, unit, a
free_lun, unit

c_xrt_n_a = float(a[1:1+6])
c_xrt_n_b = float(a[9:9+6])
c_xrt_e_a = float(a[17:17+6])
c_xrt_e_b = float(a[25:25+6])

return
end
