pro xrt_find_files, start_time, end_time, files, level0=level0, $
                    quicklook = quicklook, archive_prefix =archive_prefix,$
                    loud=loud, qerror=qerror

; =========================================================================
;		
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;
;       XRT_FIND_FILES
;
; CATEGORY:
;
;       XRT utilities
;
;
; PURPOSE:
;
;      Return a list of files between start_time and end_time
;
;
; CALLING SEQUENCE:
;
;       XRT_FIND_FILES,start_time,end_time, files, /quicklook
;                               [,archive_prefix =archive_prefix]
;                               [,/loud]
;
; INPUTS:
;
;       start_time: (string or double) start of the time
;                   interval that is checked for missing images. Can be in 
;                   any format accepeted by ANYTIM.
;       end_time:   (string or double) end of the time
;                   interval that is checked for missing images. Can be in 
;                   any format accepeted by ANYTIM.
;
; KEYWORD INPUTS: 
;
;       level0:     If set, look in Level0 tree
;
;       quicklook:  If set, look in QLraw tree (default)
;
; OPTIONAL INPUTS:
;
;       archive_prefix: (string) Root of the directory trees where XRT
;                        images reside. The default
;                        is '/archive/hinode/xrt/QLraw'. Directory 
;                        structure is assumed to be in the form 
;                        xrtfiledir+'yyyy/mm/dd/Hxx00/'
;       loud:       (0 or 1) If set, debugging output is shown.
;
;
; OPTIONAL OUTPUTS:
;
;       qerror       - return an error if program does not execute
;                    properly
;                    1 - data directory not found
; EXAMPLE:
;
;       find all files between 2008/10/03 10:00 UT and 2008/10/04 10:00 UT
;       xrt_find_files,'03-OCT-2008 10:00','04-OCT-2008 10:00', files,$
;                      /quicklook
;
;
; COMMON BLOCKS:
;
; none
;
; KNOWN PROBLEMS:
;
; none
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       directed to this email address:
;                xrt_manager ~at~ head.cfa.harvard.edu
;
;
;
; MODIFICATION HISTORY:

progver='v2011.Feb.16'  ;--- K. Reeves (SAO) - adapted from 
;                            xrt_dv_find_missing_imgs_old, by P. Grigis
progver='v2011.Feb.22'  ;--- K. Reeves (SAO) Fixed bug when there are no
;                            files at times greater than the start
;                            time
progver='v2011.Jul.22'  ;--- K. Reeves (SAO) Added a check to make sure that
;                            data directory exists.  Also, made 'archive_prefix'
;                            full path to data for systems not at
;                            SAO, and added qerror keyword.
progver='v2011.Oct.10'  ;--- K. Reeves (SAO) Fixed off-by-one bug in loop over nhours - 
;                            one hour can cover two hourly files
;-
; =========================================================================


 ; define archive directory
  dir_root = fcheck(archive_prefix,'/archive/hinode/xrt/')

  if strmatch(archive_prefix,'/archive/hinode/xrt/') then begin
     if keyword_set(level0) then data_dir = 'level0' else data_dir = 'QLraw'
  endif else data_dir = ''

  dir = concat_dir(dir_root, data_dir)
   if not dir_exist(dir) then begin
      print, 'Data directory '+ dir +' not found.'
      print, 'Exiting...'
      qerror=1
      return
   endif

  ; OS dependent path separator
  ps=path_sep()

  ;convert inputs to anytim format: external representation
  s_time = anytim(start_time)
  e_time = anytim(  end_time)

  ;compute how many hours there are in the interval
  n_hours=ceil((e_time-s_time)/3600.)

  ;initialize file list
  allfiles=''

  ;hunt for files in the archive tree
  FOR i=0L,n_hours DO BEGIN 

     ;this hour
     thistime=anytim(s_time+3600.*i,/ex)

     ;create dir path
     thisdir=dir+ps+strtrim(thistime[6],2)+ps+string(thistime[5],format='(i2.2)') $
             +ps+string(thistime[4],format='(i2.2)')+ps+'H'+string(thistime[0],format='(i2.2)')+'00'
 

     IF keyword_set(loud) THEN print,thisdir

     ;find all FITS files in the dir
     thisfiles = find_files('*.fits', thisdir)

     ;if some files are found, add them to the list
     IF thisfiles[0] NE '' THEN allfiles=[allfiles,thisfiles]

  ENDFOR

  ;at least two images, remove dummy empty string as first element
  if n_elements(allfiles) gt 1 then files=allfiles[1:*] else files = ''
  
  start_num = anytim(start_time)

  end_num = anytim(end_time)

  file_times = anytim(xrt_filename2time(files))

  match1 = where(file_times ge start_num)
  if match1[0] ne -1 then begin
     files = files[match1]
     file_times = file_times[match1]
  
     match2 = where(file_times le end_num)
     if match2[0] ne -1 then files = files[match2] else files = ''
  endif else begin 
     files = ''
  endelse

  IF keyword_set(loud) THEN print,transpose(allfiles)

end
