;+
; NAME:
;   UFSS2SUN
; PURPOSE:
;   To convert the UFSS signal data into the most accurate sun 
;   angle. This function performs the conversion calculation performed 
;   on onboard AOCU.
; CALLING SEQUENCE:
;   theta = USFF2SUN(Nx, Ny, type)
; INPUT: 
;   Nx, Ny - raw output of UFSS sensor
;   type   - string, either 'A' or 'B', to select which UFSS sensor is 
;       used, UFSS-A or UFSS-B)
; OUTPUT:
;   theta  - an array that contains converted sun angles in the unit
;            of arcsec: [thetax,thetay]
; History:
;   draft   2007 Aug     J.Kotoku
;   Rev.1.0 2007 Aug 30  T.Shimizu, rearrange to fit to the current scheme
;   Rev.1.1 2008 Oct 08  T.Shimizu, revised for IDL ver 5
;-

FUNCTION READ_UFSS_C,filename
data = FLTARR(19,19)
file = findfile(filename)
OPENR, lun, file ,/get_lun
READF,lun,data
Cx = transpose(data)
FREE_LUN,lun
return, Cx
END

FUNCTION Xi, Na
return, fix(Na/2048.)
END

FUNCTION Yj, Na
return, fix(Na/2048.)
END

FUNCTION U, Na
return, Na/2048.- fix(Na/2048.)
END

FUNCTION Ba, Na
xi = Xi(Na)
u = U(Na)
IF xi eq 0 THEN BEGIN
    Ba0 = (1-u)^3
    Ba1 = 3.*u-9*u^2/2.+7.*u^3/4.
    Ba2 = 3.*u^2/2.-11.*u^3/12.
    Ba3 = u^3/6.
    Ba = [Ba0,Ba1,Ba2,Ba3]
    return,Ba
ENDIF
IF xi eq 1 THEN BEGIN
    Ba0 = (1-u)^3 /4.
    Ba1 = 7.*(1+u^3)/12.+u/4.-5.*u^2/4.
    Ba2 = 1/6.+(u+u^2-u^3)/2.
    Ba3 = u^3/6.
    Ba = [Ba0,Ba1,Ba2,Ba3]
    return,Ba
ENDIF
IF (2 le xi) and (xi le 13)  THEN BEGIN
    Ba0 = (1-u)^3 /6.
    Ba1 = 2/3.-u^2+u^3/2.
    Ba2 = 1/6.+(u+u^2-u^3)/2.
    Ba3 = u^3/6.
    Ba = [Ba0,Ba1,Ba2,Ba3]
    return,Ba
ENDIF
IF xi eq 14 THEN BEGIN
    Ba0 = (1-u)^3 /6.
    Ba1 = 2/3.-u^2+u^3/2.
    Ba2 = 1/6.+(u+u^2)/2.-7*u^3/12.
    Ba3 = u^3/4.
    Ba = [Ba0,Ba1,Ba2,Ba3]
    return,Ba
ENDIF
IF xi eq 15 THEN BEGIN
    Ba0 = (1-u)^3 /6.
    Ba1 = 7/12.-u/4.-5.*u^2/4.+11.*u^3/12.
    Ba2 = 1/4.+3.*(u+u^2)/4.-7.*u^3/4.
    Ba3 = u^3
    Ba = [Ba0,Ba1,Ba2,Ba3]
    return,Ba
ENDIF
END

FUNCTION  UFSS2SUN, nx, ny, type

dir0= getenv('SSWDB') + '/hinode/gen/alignment/model/'

IF type eq 'A' THEN BEGIN
    Cx = READ_UFSS_C(dir0+'UFSS_A_Cx.txt')
    Cy = READ_UFSS_C(dir0+'UFSS_A_Cy.txt')
ENDIF
IF type eq 'B' THEN BEGIN
    Cx = READ_UFSS_C(dir0+'UFSS_B_Cx.txt')
    Cy = READ_UFSS_C(dir0+'UFSS_B_Cy.txt')
ENDIF
                                                                                      
x = Xi(nx)
;x= fix(nx/2048.)
y = Yj(ny)
;y= fix(ny/2048.)
                                                                                      
Bx = Ba(nx)
By = Ba(ny)
TanThetaX = 0
TanThetaY = 0
FOR i=0,3 DO BEGIN
    FOR j=0,3 DO BEGIN
      ;  TanThetaX += Cx[x+i,y+j]*Bx[i]*By[j]
      ;  TanThetaY += Cy[x+i,y+j]*Bx[i]*By[j]
        TanThetaX = TanThetaX + Cx[x+i,y+j]*Bx[i]*By[j]
        TanThetaY = TanThetaY + Cy[x+i,y+j]*Bx[i]*By[j]
    ENDFOR
ENDFOR
theta = atan([TanThetaX,TanThetaY])*(180/!PI)*60*60
                                                                                      
return,theta
                                                                                      
END

