FUNCTION xrt_unc_rel,index,data,datap,vign_model,dark_type=dark_type, $ 
  four_type=four_type,strip_dark=strip_dark,quiet=quiet
; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       XRT_UNC_REL
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       To determine and return the uncertainy in the xrt data based on 
;       dark correction (lsback_away), nyquist noise correction (no_nyquist),
;       fourier filtering correction (xrt_fourier_vacuum), JPEG compression,
;       and vignetting.
;
; CALLING SEQUENCE:
;       uncert_arr=xrt_unc_rel(index,data,datap,vign_model,dark_type=dark_type,$
;           four_type=four_type,/strip_dark)
;
; INPUTS:
;          INDEX  - [Mandatory] 
;                    [Nz] dimensional index structure of level 0 data
;                    associated with DATA_IN and DARK_TYPE
;          DATA   - [Mandatory] 
;                    (3-dim float array, [Nx,Ny,Nz])
;                    The unprocessed (raw - level 0) image to be analyzed 
;                    (unprocessed=1) [DN]
;          DATAP  - [Mandatory] 
;                    (3-dim float array, [Nx,Ny,Nz])
;                    The processed (level 1) image corresponding to DATA [DN/s]
;      DARK_TYPE  - [Optional] [default=0] 
;                    Type of dark subtraction used. See lsback_away and xrt_prep
;                     for more details 
;      FOUR_TYPE  - [Optional] [default=0]
;                      Type of Fourier cleaning used. See xrt_fourier_vacuum.pro
;                       for details
;                   
; KEYWORDS:
;                      
;      STRIP_DARK - [Optional] 
;                      Flag concerning use of the strip darks [default=0].  
;                      If set, we will assume strip darks were used (they 
;                      have a slightly different uncertainty)
;          QUIET  - [Optional] 
;                      sets no printing of error messages 
;
; OUTPUTS:
;     UNCERT_ARR  - [Nx,Ny,Nz] float array of the RELATIVE uncertainty SQUARED
;
; EXAMPLES:
;
;   under_unc=xrt_undertainy(index,data,datap,vign_mod,dark_type=2, $ 
;                 four_type=2,/strip_dark)
;         returns an array of size(data) of the RELATIVE uncertainties SQUARED
;
; COMMON BLOCKS:
;       None.
; NOTES:
;     Does not take binning into account of the JPEG analysis ("yet")
;     ONLY RECOVERS THE RELATIVE UNCERTAINTY SQUARED.  The actual uncertainty 
;     can be recovered by unc^2=rel_unc^2*prepped_data^2 - This is why the 
;     program returns rel_unc^2, and not rel_unc.  xrt_unc_aki is currently a 
;     wrapper for this use.
;
;
; MODIFICATION HISTORY:
;
progver = 'v2011.July.18' ; --- (AKobelski) Written by AKobelski
progver = 'v2013.Feb.11' ; --- (SSaar) Altered to accept prepped image as
                         ; --- input as well (needed for Fourier error code)
progver = 'v2013.Jun.10' ; --- (AKobelski) Corrected the calculation to 
;                        ; --- normalize the rel_err by the vignetting 
;                        ; --- uncorrected data (as opposed to fully 
;                        ; --- uncorrected)
progver = 'v2013.Aug.31. ; --- (SSaar,JSlavin) added quiet switch
;
;-
; =========================================================================

if keyword_set(dark_type) then dktp=dark_type else dktp=0
if keyword_set(four_type) then frtp=four_type else frtp=0
if keyword_set(strip_dark) then strp=strip_dark else strp=0
if keyword_set(quiet) then q_qt=1 else q_qt=0
brk=0
if index.ec_imtyp eq 1 and q_qt eq 0 then $ 
  print,'Attempting to correct a dark frame...'

dark=xrt_undertainy(index,data,dark_type=dktp,four_type=frtp,strip_dark=strp) 

vign=xrt_vign_unc(index,quiet=q_qt)
jpg=jpeg_unc(index,data)
fferr=ffilter_err(datap,index)    ; Fourier filter error *rate* [DN/s]
texp=get_xrt_expdur(index, /sec)

rel_err= (temporary(jpg)^2 + temporary(dark)^2 + temporary(fferr*texp)^2)/ $ 
            float(datap*vign_model)^2+temporary(vign)^2
rel_err = float(rel_err)

return,rel_err


END
