function get_jpeg_unc,mm,qual,quiet=quiet
; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       GET_JPEG_UNC
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       To determine and return the uncertainy in the xrt data from the JPEG 
;       compression.  This function serves as the lookup table for jpeg_unc.pro
;
; CALLING SEQUENCE:
;       uncert_val=xrt_undertainy(mm,qual,/quiet)
;
; INPUTS:
;          MM     - [Mandatory] 
;                    [Nz] dimensional index array containing the macro pixel
;                    range, i.e. the maximum macropixel value - the minimum macro
;                    pixel value
;          QUAL   - [Mandatory] 
;                    (scalar)
;                    The Qvalued compression of the image being processed.
;                    This can be determined by:
;                   compression=(get_xrt_complevel(index)).compqfact
;                   
; KEYWORDS:
;         /QUIET   - [optional] enables quiet mode (no printing)
;
; OUTPUTS:
;     good_fit - [Nz] float array (or scalar...) of the jpeg compression 
;                uncertainty, as determined by the center of a gaussian fitting 
;                the average value of max-min errors within macropixels with 
;                the given mm.
;           
; EXAMPLES:
;
;
; COMMON BLOCKS:
;       None.
;
; NOTES:
;       Need to include the binning dependence.
;
; MODIFICATION HISTORY:
;
progver = 'v2011.July.18' ; --- (AKobelski) Written by AKobelski
progver = 'v2013.Feb.01' ; --- (S Saar) added quiet mode
;
;-
; =========================================================================
 
  if keyword_set(quiet) then q_qt=1 else q_qt=0 

  case qual of 
    50: begin
        ply=12.
        cutoff=0
        flat_val=12.
      end
    65: begin
      ply=[-0.481573,0.271544,-0.00648255,0.000101617,-9.49282e-7,$
        5.33988e-9,-1.82363e-11,3.69312e-14,-4.07136e-17,1.87929e-20]
      cutoff=378
      flat_val=9.48318;avg(350:400)
      end
    75: begin
      ply=[-0.125886,0.185941,-0.00294657,3.14739e-5,-2.20472e-7,$
        9.98487e-10,-2.87709e-12,5.07344e-15,-4.97955e-18,2.08052e-21]
      cutoff=475
      flat_val=7.4321;avg(450:475)
      end
    85: begin
      ply=[-.0403521,0.170855,-.00334726,4.18432e-5,-3.45394e-7,$
        1.87510e-9,-6.56483e-12,1.41939e-14,-1.71855e-17,8.89557e-21]
      cutoff=375
      flat_val=4.55106;avg(310:380)
      end
    90: begin
      ply=[-.298208,0.195721,-.00628192,0.000148311,-2.56535e-6,$
        3.01413e-8,-2.27592e-10,1.04751e-12,-2.66459e-15,2.86624e-18]
      cutoff=190
      flat_val=3.07025;avg(150:190)
      end
    92: begin
      ply=[-.295559,0.185933,-.00583421,0.000113539,-1.52283e-6,$
        1.43983e-8,-9.33111e-11,3.89080e-13,-9.30292e-16,9.62735e-19]
      cutoff=200
      flat_val=2.45152;avg(140:190)
      end
    95: begin
      ply=[-.341410,0.204188,-.00903790,0.000157361,2.20624e-6,$
        -1.67321e-7,3.60387e-9,-3.96033e-11,2.24208e-13,-5.18546e-16]
      cutoff=100
      flat_val=1.56126;avg(45:97)
      end
    98: begin
      ply=[0.]
      cutoff=1
      flat_val=0.7
      end;ply=[0.7]
   100: begin
      ply=[0.]
      cutoff=1
      flat_val=0.3
      end
   else: begin
        if q_qt eq 0 then print,'get_jpg_unc: Invalid JPEG Quality Input'
        return,-1
      end
  endcase
  ;print,'OK'

  good_fit=fltarr(n_elements(mm),/nozero)
  good_fit(*)=flat_val
  ss=where(mm lt cutoff,n_legit);
  if n_legit gt 0 then good_fit(ss)=poly(mm(ss),ply)
  ss_neg=where(good_fit lt 0,neg_cnt)
  if neg_cnt ne 0 then good_fit(ss_neg)=0
  return,good_fit


end
