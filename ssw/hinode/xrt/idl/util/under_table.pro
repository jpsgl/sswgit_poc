function under_table,dark_type,index,four_filt,n2,strip_flag=strip_flag
; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       UNDER_TABLE   
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       Effectively a lookup table to determine the dark and nyquist noise 
;       contained in xrt data.  Includes the corrections for the improved   
;       signal to noise of using fourier filtering and variations due to
;       different data compression levels.           
;
; CALLING SEQUENCE:
;       uncert_int=under_table(dark_type,index,four_filt,n2,/strip_flag)
;
; INPUTS:
;      DARK_TYPE  - [Mandatory] 
;                    Integer 0-2 of the dark correction used.
;       INDEX     - [Mandatory] 
;                    Index array for data in question.
;      FOUR_FILT  - [Mandatory]            
;                    Type of fourier correction used, 3 means no fourier filter 
;                       correction.  Improves the uncertainty
;                   
; KEYWORDS:
;                      
;      STRIP_FLAG - [Optional] 
;                      Flag concerning use of the strip darks.  Default 
;                       follows the standards.
;                       If set, we will assume strip darks were used (they 
;                       have a slightly different uncertainty)
;
; OUTPUTS:
;     UNCERT_INT  - Floating point scalar of the partial uncertainty
;
; EXAMPLES:
;       uncert_int=under_table(dark_type,index,four_filt,/strip_flag)
;
; COMMON BLOCKS:
;       None.
; NOTES:
;
;
; MODIFICATION HISTORY:
;
progver = 'v2011.Jun.28' ; --- (AKobelski) Written by AKobelski
progver = 'v2012.Nov.10' ; --- (SSaar) Significantly modified to include 
;                                       compression effects; streamlined
;
;-
; =========================================================================


;Check the inputs
if n_params() lt 3 then begin
  print,'Incorrect call to under_table.pro . Correct Usage:'
  print, '   uncert_int=under_table(dark_type,index,four_filt,/strip_flag)'
  return,-1
endif

; extract info from index
n1=index.naxis1
n2=index.naxis2
binning=index.chip_sum
comp=get_xrt_complevel(index)
q=comp.compqfact

; Set flag if strip darks
strip = 0
if keyword_set(strip_flag) then strip = 1

; compute correction to <sigma> if fourier filter is in use (four_filt=3)
if four_filt ne 3 then begin
   case binning of 
       1: if dark_type eq 1 then ffilt=0.935 else ffilt=0.763
       2: if dark_type eq 1 then ffilt=0.944 else ffilt=0.875
       4: if dark_type eq 1 then ffilt=0.956 else ffilt=0.895   ; interp
      else: if dark_type eq 1 then ffilt=0.974 else ffilt=0.914
   endcase
endif else ffilt=1.000

; compute correction to <sigma> if n_x and/or n_y <> 2048 
if n1 ne 2048 or n2 ne 2048 then begin
   cx=[1.003076d0,-0.00098327976d0]
   conx=poly(alog10(n1),cx)
   cy=[0.96479402d0,0.010715234d0]
   cony=poly(alog10(n2),cy)
   con=cony*conx
endif else con=1.

; if using full-frame dark
if strip eq 0 then begin
   case dark_type of 
      0: begin       ; default, combo
         case binning of 
            1: begin 
               case q of
                  90:  avsig=0.9602
                  98:  avsig=1.3239
                  else:   avsig=1.1030 
               endcase
               sigav=0.177   ;  sigav=0.211
            end
            2: begin 
               case q of
                  90:  avsig=0.9800
                  98:  avsig=1.4538
                  else:   avsig=1.1624 
               endcase
               sigav=0.557
            end
            4: begin
               case q of
                  98:  avsig=1.6640 
                  else:   avsig=1.1859
               endcase
               sigav=1.000    ;
            end
            8: begin
               case q of
                  50:  avsig=0.543 
                  98:  avsig=1.6903
                  else:   avsig=1.3348 
               endcase
               sigav=1.566    ; sigav=1.490   ; or 0.959
            end
            else: begin
               print,'Incorrect binning input into under_table'
               return,-1
            end
         endcase
      end                      ;    ending dt=0, binning case
      1: begin          ; median dark, kano
         case binning of 
            1: begin        ; 
               case q of
                  90:  avsig=2.099
                  92:  avsig=1.921
                  98:  avsig=1.824
                  else:   avsig=1.661
               endcase
               sigav=0.177
            end
            2: begin
               case q of
                  90:  avsig=0.9647
                  98:  avsig=1.3919
                  else:   avsig=1.1302
               endcase
               sigav=0.557
            end
            4: begin
               case q of
                  98:  avsig=1.872
                  else:   avsig=1.430
               endcase
               sigav=1.00 
            end
            8: begin   ; *  do these
               case q of
                  90:  avsig=0.9602
                  98:  avsig=1.3239
                  else:   avsig=1.1030
               endcase
               sigav=1.566    ; ok
            end
            else: begin
               print,'Incorrect binning input into under_table'
               return,-1
            end
         endcase                              ;  ending dt=1, binning case
      end
      2: begin                ; raw model
         case binning of 
            1: begin     ; *do an
               case q of
                  90:  avsig=0.9602
                  98:  avsig=1.3239
                  else:   avsig=1.1030
               endcase
               sigav=0.242
            end
            2: begin
               case q of
                  90:  avsig=0.9647
                  98:  avsig=1.3919
                  else:   avsig=1.1302
               endcase
               sigav=0.560
            end
            4: begin
               case q of
                  98:  avsig=1.872
                  else:   avsig=1.430
               endcase
               sigav=2.200 
            end
            8: begin
               case q of
                  50:  avsig=0.543 
                  98:  avsig=1.6903
                  else:   avsig=1.3348 
               endcase
               sigav= 2.919  ;  sigav=4.773  
            end
            else: begin
               print,'Incorrect binning input into under_table'
               return,-1
            end
         endcase ;ending dt=2, binning case
      end
      else: begin
         print,'Incorrect dark type input into under_table'
         return,-1
      end
   endcase                                    ; ending the dark_type case
endif else begin                                  ; strip eq 1
;------THE CASE STRIP DARKS WERE USED------------------------;
   case dark_type of 
      0: begin
         case binning of 
            1: begin   ; *
               case q of
                  90:  avsig=1.1386
                  98:  avsig=1.5700
                  else:   avsig=1.308 
               endcase
               sigav=0.189
            end
            2: begin   ; *
               case q of
                  90:  avsig=1.1978
                  98:  avsig=1.6516
                  else:   avsig=1.376  
               endcase
               sigav=0.530
            end
            4: begin                ; *
               case q of
                  98:  avsig=2.503 
                  else:   avsig=1.784 
               endcase
               sigav=1.088
            end
            8: begin
               case q of
                  50:  avsig=0.535 
                  98:  avsig=1.666 
                  else:   avsig=1.316 
               endcase
               sigav= 2.522    ;  or 2.792  
            end
            else: begin
               print,'Incorrect binning input into under_table'
               return,-1
            end
         endcase                                ; ending dt=0, binning case
      end
      1: begin   ; dt=1 kano
         case binning of 
            1: begin   ; *  need to finish these
               case q of
                  90:  avsig=1.1386
                  98:  avsig=1.5700
                  else:   avsig=1.308 
               endcase
               sigav=0.189
            end
            2: begin   ; *
               case q of
                  90:  avsig=1.1978
                  98:  avsig=1.6516
                  else:   avsig=1.376  
               endcase
               sigav=0.530
            end
            4: begin                ; *
               case q of
                  98:  avsig=2.503 
                  else:   avsig=1.784 
               endcase
               sigav=1.088
            end
            8: begin
               case q of
                  50:  avsig=0.535 
                  98:  avsig=1.666 
                  else:   avsig=1.316 
               endcase
               sigav=2.792  ; or 2.522  ; *
            end
            else: begin
               print,'Incorrect binning input into under_table'
               return,-1
            end
         endcase ;ending dt=1, binning case
      end
      2: begin    ; raw  
         case binning of 
            1: begin   ; *
               case q of
                  90:  avsig=1.1386
                  98:  avsig=1.5700
                  else:   avsig=1.308 
               endcase
               sigav=0.241
            end
            2: begin   ; *
               case q of
                  90:  avsig=1.1978
                  98:  avsig=1.6516
                  else:   avsig=1.376  
               endcase
               sigav=1.614
            end
            4: begin                ; *
               case q of
                  98:  avsig=2.503 
                  else:   avsig=1.784 
               endcase
               sigav=2.199
            end
            8: begin
               case q of
                  50:  avsig=0.535 
                  98:  avsig=1.666 
                  else:   avsig=1.316 
               endcase
               sigav=4.947
            end
            else: begin
               print,'Incorrect binning input into under_table'
               return,-1
            end
         endcase                                ; ending dt=2, binning case
      end
      else: begin
         print,'Incorrect dark type input into under_table'
         return,-1
      end
   endcase                                     ; ending the dark_type case
end    



return,sqrt(sigav^2+(avsig*ffilt*con)^2)
end
;
;
;
