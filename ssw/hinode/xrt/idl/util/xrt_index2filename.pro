FUNCTION xrt_index2filename, index, archive_path=archive_path,  $
         verbose=verbose, quiet=quiet, qabort=qabort, qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT (mandatory)
;
; NAME:
;       XRT_INDEX2FILENAME
;
; CATEGORY:
;       File I/O.
;
; PURPOSE:
;       Generate a default FITS filename per XRT image, based upon the
;       "index".
;
; CALLING SEQUENCE:
;
;       Result = XRT_INDEX2FILENAME, index [,/archive_path]       $
;                  [,/verbose] [,/quiet] [,/qstop] [,qabort=qabort]
;
; INPUTS:
;
;       INDEX         - [Mandatory] (structure, or structure array, [Nimg])
;                       The XRT data keywords. These come from XRT FITS 
;                       files in the header.
;
; KEYWORDS:
;
;       /ARCHIVE_PATH - [Optional] (Boolean, or non-zero value)
;                       (a) If set, the output filenames are given paths
;                       appropriate for an XRT-style archive directory 
;                       tree. (b) This keyword can also be used to
;                       return just the directory path (see Examples).
;       /VERBOSE      - [Optional] (Boolean) If set, print out extra
;                       information. Overrides "/quiet" (see Note #1).
;       /QUIET        - [Optional] (Boolean) If set, suppress messages
;                       (see Note #1).
;       /QSTOP        - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;
;       Return        - (string, or string array [Nimg])
;                       A string array of filenames corresponding to 
;                       the elements of the "index" input.
;       ARCHIVE_PATH  - [Optional] (string, or string array [Nimg])
;                       If this keyword is given a variable name with
;                       non-zero value as an input, then on return
;                       this variable will be a string list of the
;                       archive paths, without the filenames.
;
; EXAMPLES:
;
;       Get simple filenames without paths:
;       IDL> fnames = xrt_index2filename(index)
;
;       Get full pathnames into the site's XRT archive:
;       IDL> fnames = xrt_index2filename(index, /archive_path)
;
;       Same as previous, but also get the pathnames separately:
;       IDL> pathnames = 1  ;; Non-zero and defined variable name.
;       IDL> fnames = xrt_index2filename(index, archive_path=pathnames)
;       IDL> help, pathnames  ;; Prints path list.
;
; COMMON BLOCKS:
;
;       none
;
; NOTES:
;
;       1) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
; MODIFICATION HISTORY:
;
progver = 'v2007-Apr-10' ;--- (M.Weber, J.Cirtain (SAO)) Written. (Based on
;                             <sot_index2fileinfo.pro>, 2006-Jan-04.)
progver = 'v2007-May-09' ;--- (M.Weber (SAO)) Edited to use env-var.
;
progver = 'v2010-May-17' ;--- (E. DeLuca (SAO)) Now accepts DATA_LEV = 2
;                        Changed prognam from WRITE_XRT to XRT_INDEX2FILENAME
;
;-
; =========================================================================

;=== Initial setup =======================================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (not q_vb)

  ;=== Initialize program constants.
  prognam   = 'XRT_INDEX2FILENAME'
  L0_prefix = 'XRT'
  L1_prefix = 'L1_XRT'
  L2_prefix = 'L2_XRT'
;;  XRT_ARCHIVE = '/archive/hinode/xrt/' ;; Make this a site XRT env var!
  XRT_ARCHIVE = getenv('SSW_XRT_ARCHIVE')

  ;=== Announce version of <read_xrt.pro>.
  if q_vb then box_message, prognam+': Running ' + prognam + $
                            ' ' + progver + '.'
  if q_vb then print, prognam+': Performing initial setup...'


  ;=== Set some keyword Booleans.
  q_archive = keyword_set(archive_path)
  qstop     = keyword_set(qstop)
  qabort    = 0B

  ;=== Other useful stuff.
  prognam_len = strlen(prognam)
  prognam_nul = strmid('                                               ', $
                       0, prognam_len)
  n_img = n_elements(index)
  pdelim=get_delim()

  if q_vb then print, prognam_nul+'                          ...OK'


;=== Check inputs ========================================

  ;=== Check index structure for XRT ID.
  if not required_tags(index, 'date_obs,ec_fw1') then begin
    if (not q_qt) then box_message, prognam+': This is not an XRT index ' $
                                    + 'structure. Aborting.'
    qabort = 1B
    return, 0
  endif

if qstop then stop

  ;=== Check for mixture of Data Levels.
  aa = uniq(index[sort(index.data_lev)].data_lev)
  if (n_elements(aa) ne 1) then begin
    if (not q_qt) then box_message, prognam+': This index has a mixture ' $
                                    + 'of Data Levels. Aborting.'
    qabort = 1B
    return, 0
  end


;=== Generate filenames ==================================

  ;=== This makes the time string for the base of the filename.
  millis = ssw_strsplit(anytim(index.date_obs,/ecs),'.',/tail)
  fnames = time2file(index.date_obs,/sec) + '.' + strmid(millis,0,1) $
           + '.fits'

  ;=== Part of the filename depends upon the Data Level.
  case (index[0].data_lev) of
    0: begin
         fnames = L0_prefix + temporary(fnames)
         level_dir = 'level0' + pdelim
       end

    1: begin
         fnames = L1_prefix + temporary(fnames)
         level_dir = 'level1' + pdelim
       end

    2: begin
         fnames = L2_prefix + temporary(fnames)
         level_dir = 'level2' + pdelim
       end

    else: begin
            if (not q_qt) then box_message, prognam+': This index '  $
                    + 'does not have a recognized DATA_LEV. Aborting.'
            qabort = 1B
            return, 0
          end
  endcase

  ;=== If going to the archive, turn the time into a path.
  if q_archive then begin
    fecs=anytim(index.date_obs,/ecs)
    pdelim=get_delim()
    archive_path = XRT_ARCHIVE + level_dir + strmids(fecs,0,10) + pdelim $
                   + 'H' + strmid(fecs,11,2) + '00' + pdelim
    fnames = archive_path + temporary(fnames)
  end


;=== Finish ==============================================

  return, fnames


END ;======================================================================
