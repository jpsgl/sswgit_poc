FUNCTION xrtdisp, data, log=log, overwrite=overwrite

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       XRTDISP
;
; CATEGORY:
;       Data graphics.
;
; PURPOSE:
;
;       Adjust an XRT Level-1 image so that it will scale well in a 
;       display.
;
;       ONLY for display. Do NOT use this output for quantitative analysis!
;
; CALLING SEQUENCE:
;
;       Result = XRTDISP( data [,/log] [,/overwrite] )
;
; INPUTS:
;
;       DATA       - [Mandatory] (number array, [Nx, Ny, Nimg])
;                    A datacube of XRT Level-1 data (i.e., has been
;                    calibrated with <xrt_prep.pro>).
;
; KEYWORDS:
;
;       /LOG       - [Optional] (Boolean) If set, the result will
;                    be logarithmically scaled with the "alog" function.
;       /OVERWRITE - [Optional] (Boolean) If set, then the input DATA
;                    will be overwritten. See Examples.
;
; OUTPUTS:
;
;       Result     - [Mandatory] (float array, [Nx, Ny, Nimg])
;                    XRT Level-1 (prepped) data sometimes has negative 
;                    values. However, XRT data looks better when it has 
;                    been log-scaled or power-scaled, and these have 
;                    problems with negative values. The output only
;                    applies an additive shift to make the array positive
;                    definite. (Exception: the /LOG keyword also applies
;                    scaling, after the addition.)
;
; EXAMPLES:
;
;       Basic usage:
;       IDL> tvscl, alog(xrtdisp(data[*,*,2]))
;
;       Same thing this way:
;       IDL> tvscl, xrtdisp(data[*,*,2], /log)
;
;       Or a power-law scaling:
;       IDL> tvscl, xrtdisp(data[*,*,2])^0.35
;
;       For large datacubes that you only want to view, it might be useful 
;       to overwrite the datacube:
;       IDL> tvscl, alog(xrtdisp(data[*,*,2], /overwrite))
;
; COMMON BLOCKS:
;
;       none
;
; NOTES:
;
;       none
;
; MODIFICATION HISTORY:
;
progver = 'v2007-May-22' ;--- (M.Weber) Written.
;
;-
; =========================================================================

;; Bare bones coding here.

  q_overw = keyword_set(overwrite)
  q_log   = keyword_set(log)

  ds = size(data)
  nx = ds[1]
  ny = ds[2]
  ni = ds[3]
  if ds[0] eq 2 then ni = 1

  if q_overw then begin
    for ii = 0,(ni-1) do begin
      dmin = min(data[*,*,ii])
      data[*,*,ii]   = temporary(data[*,*,ii])   - dmin + 1
    endfor
    if q_log then data   = alog(temporary(data))
    return, data
   
  endif else begin
    retval = data
    for ii = 0,(ni-1) do begin
      dmin = min(retval[*,*,ii])
      retval[*,*,ii] = temporary(retval[*,*,ii]) - dmin + 1
    endfor
    if q_log then retval = alog(temporary(retval))
    return, retval
  endelse


;=== Finish ==============================================

  return, 0B


END ;======================================================================
