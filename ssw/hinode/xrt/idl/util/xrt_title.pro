FUNCTION xrt_title, h, listing=listing

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT 
;
; NAME:
;       XRT_TITLE
;
; CATEGORY:
;       Data information
;
; PURPOSE:
;       To summarize some image information in one string.
;
; CALLING SEQUENCE:
;
;       title = XRT_TITLE(image_header [,/listing])
;
; INPUTS:
;
;       H        - [Mandatory] (structure scalar)
;                  A structure representing the FITS header for a single
;                  XRT image. (Could be the "index" output of 
;                  <read_xrt.pro>.)
;
; KEYWORDS:
;
;       /LISTING - [Optional] (Boolean)
;                  Produce a more carefully formatted string.
;
; OUTPUTS:
;
;       return   - (String scalar)
;                  Summary of image information:
;                  filters, exposure duration, exposure time, FOV size.
;
; EXAMPLES:
;
;       Basic usage:
;       IDL> print, xrt_title(index[0])
;
; COMMON BLOCKS:
;       None.
;
; NOTES:
;       None.
;
; MODIFICATION HISTORY:
;
progver = 'v2007.Feb.16' ; --- (JWC) Written. Cleaned up by MW.
progver = 'v2007.May.09' ; --- (MW) Edited for index and catalog inputs.
;                              Added FOV size to non-listing output.
progver = 'v2007.Nov.08' ; --- (MW) Added array index column.
;
;-
; =========================================================================


  filter1 = h.EC_FW1_
  filter2 = h.EC_FW2_
  exptime = h.EXPTIME
  date    = anytim(h.DATE_OBS,/yohkoh,/trunc)
  nx      = h.NAXIS1
  ny      = h.NAXIS2
  id      = lindgen(n_elements(h))

  if keyword_set(listing) then begin

    title = string(id,'(i5)')+' '+date+' '+strpad(filter1,10,fill=' ')+$
        strpad(filter2,10,fill=' ')+$
        string(exptime,'(f10.2)')+$
        string(nx,'(i10)')+$
        string(ny,'(i10)')

  endif else begin
    title = strcompress(id,/rem)+' XRT '+filter1+'/'+filter2+' '+date+' '+$
        trim(exptime,'(f10.2)')+'s'+' '+strcompress(nx,/rem)+'x'+ $
        strcompress(ny,/rem)
  endelse


  return, title


END ;======================================================================
