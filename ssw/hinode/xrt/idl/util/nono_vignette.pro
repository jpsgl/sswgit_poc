
FUNCTION nono_vignette, image_in, index_in, model, hist=hist

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       NONO_VIGNETTE 
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       Remove Vignetting Function 
;
; CALLING SEQUENCE:
;       image_out = nono_vignette(image_in, index_in, [,model]) 
;
; INPUTS:
;       IMAGE_IN  - [Mandatory] (2-dim float array, [Nx,Ny])
;                   The image containing read-out signals.
;       INDEX_IN  - [Mandatory]  index structure for IMAGE_IN
;
; OUTPUTS:
;       IMAGE_OUT - [Mandatory] (2-dim float array, [Nx,Ny])
;                   The image with the fit to read-out signals removed.
;       MODEL     - [Optional] (2-dim float array, [Nx,Ny])
;                   The model for the vignetting function 
;  KEYWORDS:
;           HIST  - [Mandatory] (string array) Contains information
;                   on type of cleaning done and keyword values
;                   for later use in updating header information
;
; EXAMPLES:
;
;       Basic usage:
;       IDL> image_out = nono_vignette(image_in, index_in, hist=hist)
;
;       You want the vignetting model for some reason:
;      IDL> image_out = nono_vignette(image_in, index_in, model, hist=hist)
;
;
;  COMMON BLOCKS:
;       None.
;
; NOTES:
;
;
; MODIFICATION HISTORY:
;
progver ='v2008.May.13' ; --- (SSaar) Written by SSaar. 
;
progver ='v2010.Dec.06' ; --- (SSaar) Updated array positioning keywords used
;
progver = 'v2011.Jul.22 ; --- (SSaar) Further update to array position keywords 
;-
; =========================================================================

;===== Check if a dark  image; if not, do the correction
  imtyp=index_in.ec_imty_
  if imtyp ne 'dark' then begin

;===== Get dimensions & parameters of image.
     nx = index_in.naxis1
     ny = index_in.naxis2
     chipsum=index_in.chip_sum
     x0chip=index_in.pos_col 
     y0chip=index_in.pos_row 

;===== use basic formula to compute correction
;    graze_angle = 0.92515*60       ; best fit graze angle in arcmins (SS)
                                ; yields rms = 0.003145 with cal data
     graze_angle = 0.91*60         ; average graze angle in arcmins (LG)
                                ; yields rms = 0.003611 with cal data
;     x0 = 1020.                    ; vignetting center in x  (BI) 
;     y0 = 1124.                    ; vignetting center in y (BI)
     x0 = 1024./chipsum            ; vignetting center in x  (nominal) 
     y0 = 1024./chipsum            ; vignetting center in y (nominal)
     arcsec_per_pix = 1.0286*chipsum

; fix for different ROI, binning

     x=findgen(nx)  
     x=rebin(x,nx,ny) + x0chip/chipsum 
     y=findgen(ny)
     y=rotate(rebin(y,ny,nx),1) + y0chip/chipsum

     angle=sqrt((x-x0)^2 +(y-y0)^2)*arcsec_per_pix/60. ; field angle in arcmins
  
     vign = 1.0 - (2/3.)*(angle/graze_angle)     ; vignetting function

;===== Ratio, and you're done!
     image_out = image_in/vign  
     model = vign 
     hist='XRT_VIGNETTING: vignetting correction performed'

  endif else begin
;===== if you have a dark frame, do nothing 
     image_out = image_in
     model = image_in*0+1.
     hist='XRT_VIGNETTING: NO vignetting correction: dark frame!'
  endelse

  return, image_out
end
;
;
;
