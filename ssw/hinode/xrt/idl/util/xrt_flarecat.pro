function xrt_flarecat, t1, t2, class=class, noaa=noaa, all=all, catalog=catalog, download=download, quiet=quiet

;+
;Purpose: Query the XRT Flare Catalog.
;
;Inputs:
;	T1 = string, start time of search in any time accepted by anytim() (e.g. '2014/01/01 00:00:00'). 
;		Defaults to the earliest flare if an undefined variable is passed. 
;	T2 = string, end time of search. Defaults to latest flare if undefined. 
;
;Outputs:
;	Structure array with flare info if results were found or -1 if no results were found. 
;	See notes for what's contained in the structure. 
;
;Keywords:
;	CLASS = string, optional filter for GOES class (e.g. 'A','B', 'C', 'M', 'X')
;	NOAA = string or integer, option filter for NOAA AR number. 
;	ALL = boolean, set to return info on flares not observed by XRT. 
;	CATALOG = string, path to xrt_flarecat.sav file. The save file is included with this 
;			routine in SolarSoft, found here: ssw/hinode/xrt/idl/util/
;	DOWNLOAD - (Boolean)
;			Set to download the latest version of the catalog save file from the XRT Flare 
;			Catalog website: http://xrt.cfa.harvard.edu/flare_catalog/xrt_flarecat.sav
;			The save file is stored with this routine in the ssw/hinode/xrt/idl/util/ 
;			directory, but users may wish to download the file from the web if their SSW 
;			distribution is not entirely up-to-date or if they are using this function 
;			independent of SolarSoft.
;	QUIET = boolean, set to suppress messages.
;
;Calling Sequence:
;	IDL> flares = xrt_flarecat('2014/01/01 00:00:00', '2014/02/01 00:00:00')
;
;Notes: 
;	(1) The XRT Flare Catalog lives here: http://xrt.cfa.harvard.edu/flare_catalog/ and it is 
;	adapted from the Hinode Flare Catalog, which lives here: http://st4a.stelab.nagoya-u.ac.jp/hinode_flare/
;
;	(2) The structure tags in the output are defined as follows:
;			ID: Event ID corresponding to the Hinode Flare Catalog entry.
;			FLARECAT: URL link to the catalog website for that event.
;			START: Start time for flare (UT).
;			_END: End time for flare (UT).
;			PEAK: Time of peak GOES flux (UT).
;			LOC1: Location in heliographic coordinates.
;			CLASS: GOES X-ray class.
;			X: X distance from sun center (arcsec).
;			Y: X distance from sun center (arcsec).
;			SOT: Number of SOT images between start & end time that include the flaring region.
;			XRT: Number of XRT useful images (excludes darks, g-band, and flare patrol images).
;			EIS: Number of EIS observations between start & end time that include the flaring region.
;			RHESSI: Number of XRT images between start & end time that include the flaring region.
;			NORH: If populated, indicates date of Nobeyama Radioheliograph observation.
;			COMBOS: Filter and field-of-view combinations for XRT with the number of images for each.
;			MOVIES: Path(s) to GIF animation movies on XRT website, if applicable.
;			RESPONSE: Number of XRT images taken using the flare response program.
;			PRE_FLARE: Number of XRT images from the pre-flare buffer (only relevant if there was a flare response).
;			OBS_NOTE: Yes or no for if XRT caught the flare and the reason why the flare was missed if no.
;			REGION: NOAA region for that event or blank if the flare wasn't associated with a numbered region.
;			HEK: URL link to the HEK entry, if applicable.
;
;	(3) The COMBOS and MOVIES tags in the output structure may contain an arbitrary number 
;		of substrings concatenated together, delimited by the '|' character. These correspond 
;		to the Filter and FOV combinations available for that flare. To extract this 
;		information, you can do the following:
;
;			IDL> combos = strsplit(flares[0], '|', /extract)
;			IDL> for i=0, n_elements(combos)-1 do print, combos[i]
;
;		The movie paths can be extracted likewise. These are relative paths from the Flare 
;		Catalog Website. To get links to all of the movies for a certain event, you can do: 
;
;			IDL> movies = strsplit(flares[0], '|', /extract)
;			IDL> website = 'http://xrt.cfa.harvard.edu/flare_catalog/'
;			IDL> for i=0, n_elements(movies)-1 do print, website+movies[i]
;
;Modified: 
;	2014/10/28: Written. - Patrick McCauley 
;	2014/12/10: Added the DOWNLOAD keyword and fixed a bug. - McCauley
;-

if keyword_set(download) then begin
	url = 'http://xrt.cfa.harvard.edu/flare_catalog/xrt_flarecat.sav'
	oUrl = obj_new('IDLnetUrl')
	catalog = oUrl->IDLnetURL::Get(url=url, filename='xrt_flarecat.sav')
	obj_destroy, oUrl
endif

if not keyword_set(catalog) then begin
	ssw = get_logenv('SSW')
	catalog = ssw+'/hinode/xrt/idl/util/xrt_flarecat.sav'
endif 

if file_test(catalog) eq 0 then begin
	message, 'Catalog file not found: ssw/hinode/xrt/idl/util/xrt_flarecat.sav. Try rerunning with the /DOWNLOAD keyword.', /info
	return, -1
endif else restore, catalog

f = flarecat

if n_elements(t1) eq 0 then t1 = f[0].start
if n_elements(t2) eq 0 then t2 = f[n_elements(f)-1]._end

want = where(anytim(f.start) ge anytim(t1) AND anytim(f._end) le anytim(t2), count)

if count eq 0 then begin
	if not keyword_set(quiet) then message, 'No flares between '+strmid(anytim(t1, /ccsds), 0, 19)+' and '+strmid(anytim(t2, /ccsds), 0, 19), /info
	return, -1
endif

f = f[want]

if keyword_set(all) then all_msg = '' else all_msg = 'with XRT data'
if keyword_set(class) then class_msg = 'class '+class else class_msg = ''
if keyword_set(noaa) then noaa_msg = 'from AR '+trim(noaa) else noaa_msg = ''

if not keyword_set(all) then begin
	want = where(long(f.xrt) gt 0, count)
	if count eq 0 then begin
		if not keyword_set(quiet) then message, 'No flares with XRT data between '+strmid(anytim(t1, /ccsds), 0, 19)+' and '+strmid(anytim(t2, /ccsds), 0, 19)+' ('+trim(n_elements(f))+' without XRT data)', /info
		return, -1
	endif
	f = f[want]
endif

if keyword_set(class) then begin
	want = where(strupcase(class) eq strmid(f.class, 0, 1), count)
	if count eq 0 then begin
		if not keyword_set(quiet) then message, 'No '+class_msg+' flares '+all_msg+' between '+strmid(anytim(t1, /ccsds), 0, 19)+' and '+strmid(anytim(t2, /ccsds), 0, 19), /info
		return, -1
	endif
	f = f[want]
endif

if keyword_set(noaa) then begin
	want = where(trim(noaa) eq f.region, count)
	if count eq 0 then begin
		if not keyword_set(quiet) then message, 'No '+class_msg+' flares '+all_msg+' '+noaa_msg+' between '+strmid(anytim(t1, /ccsds), 0, 19)+' and '+strmid(anytim(t2, /ccsds), 0, 19), /info
		return, -1
	endif
	f = f[want]
endif

if not keyword_set(quiet) then begin 	
	classes = strmid(f.class, 0, 1)
	uniq_classes = classes[uniq(classes, sort(classes))]

	class_counts = intarr(n_elements(uniq_classes))
	for j=0, n_elements(uniq_classes)-1 do begin
		want = where(classes eq uniq_classes[j], count)
		class_counts[j] = count
	endfor

	summary = strjoin(uniq_classes+': '+trim(class_counts), ', ')
	
	message, trim(n_elements(f))+' '+class_msg+' flare(s) '+all_msg+' '+noaa_msg+' between '+strmid(anytim(t1, /ccsds), 0, 19)+' and '+strmid(anytim(t2, /ccsds), 0, 19), /info
	if not keyword_set(class) then message, 'Class counts... '+summary, /info
endif

return, f
end