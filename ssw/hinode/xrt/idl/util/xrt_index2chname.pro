FUNCTION xrt_index2chname, index, verbose=verbose, quiet=quiet, $
                           qabort=qabort, qstop=qstop

; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       XRT_INDEX2CHNAME
;
; CATEGORY:
;       Data information.
;
; PURPOSE:
;
;       Given an INDEX structure array for a set of images, return a
;       string array of the XRT channel names.
;
; CALLING SEQUENCE:
;
;       Result = XRT_INDEX2CHNAME( index [,/verbose] [,/quiet] [,/qstop]
;                                  [,qabort=qabort]                     )
;
; INPUTS:
;
;       INDEX    - [Mandatory] (structure array, [Nimg])
;                  The XRT data keywords. These come from the
;                  Level-0 FITS files in the header.
;
; KEYWORDS:
;
;       /VERBOSE - [Optional] (Boolean) If set, print out extra
;                  information. Overrides "/quiet" (see Note #1).
;       /QUIET   - [Optional] (Boolean) If set, suppress messages
;                  (see Note #1).
;       /QSTOP   - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;
;       Result   - [Mandatory] (string array, [Nimg])
;                  The string names for the XRT channels for each
;                  element in INDEX. These are usually quite similar
;                  to the name of the filter or filters of the image.
;                  (These names should be matchable to channel names
;                  in an XRT "temp_resp" array if temperature analysis
;                  is going to be performed; see <calc_xrt_temp_resp.pro>.)
;       QABORT   - [Optional] (Boolean) Indicates that the program
;                  exited gracefully without completing. (Might be
;                  useful for calling programs.)
;                  0: Program ran to completion.
;                  1: Program aborted before completion.
;
; EXAMPLES:
;
;       Basic usage:
;       IDL> names = xrt_index2chname(index)
;
; COMMON BLOCKS:
;
;       none
;
; NOTES:
;
;       1) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
; MODIFICATION HISTORY:
;
progver = 'v2007-May-22' ;--- (M.Weber) Written.
;
;-
; =========================================================================



;=== Initial setup =======================================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (not q_vb)

  ;=== Initialize program constants.
  prognam='XRT_INDEX2CHNAME'
  prognul='                '

  ;=== Set some keyword Booleans.
  qstop     = keyword_set(qstop)
  qabort    = 0B


;=== Check inputs ========================================

  ;=== Check number of inputs.
  if (n_params() ne 1) then begin
    if (not q_qt) then box_message,                                $
      [prognam+': Need to provide a single input: INDEX. Aborting.']
    qabort = 1B
    return, 0
  endif

  ;=== Check index structure for XRT ID.
  if not required_tags(index, 'EC_FW1_,EC_FW2_') then begin
    if (not q_qt) then box_message,                                      $
      [prognam+': This structure does not have the right tags. Aborting.']
    qabort = 1B
    return, 0
  endif


;=== Figure out channel names ============================

  ;=== Basically, take the filter wheel names,
  ;=== change "_" to "-", drop "Open",
  ;=== insert "/" as necessary.

  fw1 = index.ec_fw1_
  fw1 = str_replace(temporary(fw1), '_', '-')
  ss1 = where(fw1 eq 'Open', cnt1)
  if (cnt1 ge 1) then fw1[ss1] = ''

  fw2 = index.ec_fw2_
  fw2 = str_replace(temporary(fw2), '_', '-')
  ss2 = where(fw2 eq 'Open', cnt2)
  if (cnt2 ge 1) then fw2[ss2] = ''

  sep = strarr(n_elements(index)) + '/'
  ssop = where((fw1 eq '') or (fw2 eq ''), cntop)
  if (cntop ge 1) then sep[ssop] = ''

  chname = fw1 + sep + fw2


;=== Finish ==============================================

  return, chname


END ;======================================================================
