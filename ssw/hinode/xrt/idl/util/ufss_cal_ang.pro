;+
; NAME:
;   UFSS_CAL_ANG
; PURPOSE:
;   Return the AOCS pre-calibrated UFSS angle.  
;   This function performs the conversion calculation performed 
;   on onboard AOCU. The input is the angle (in deg) stored in HK2
;   FITS files.
; CALLING SEQUENCE:
;   theta = UFSS_CAL_ANG(ANGx, ANGy, type)
; INPUT: 
;   ANGx, ANGy - UFSS angles (in deg, after simple conversion) from 
;      HK2/AOCU1 FITS files. array can be accepted.
;   type   - string, either 'A' or 'B', to select which UFSS sensor is 
;       used, UFSS-A or UFSS-B)
; OUTPUT:
;   theta  - an array that contains converted sun angles in the unit
;            of arcsec: [thetax,thetay]
;       if ANGx/ANGy has N-dimension array, then theta is [2, N] array.
; History:
;   Rev.1.0 2007 Sept 1  T.Shimizu, make
;-

function UFSS_CAL_ANG, ANGx, ANGy, type

; check input parameters
nox= n_elements(ANGx)
noy= n_elements(ANGy)
if nox ne noy then $
 message, 'The input parameters ANGx and ANGy should have the same dimensions'

if type ne 'A' and type ne 'B' then $
 message, 'Incorrect parameter for type'+ string(type)

;
ANGcal= fltarr(2, nox)

; converting UFSS angle (deg -> DN -> arsec)
nx= (ANGx + 0.556)/3.39355d-5
ny= (ANGy + 0.556)/3.39355d-5

for i=0, nox-1 do begin

 re= ufss2sun(nx[i], ny[i], type)
 ANGcal[*, i]= re

endfor

return, ANGcal
end
