function history_wrap,line_in,quiet=quiet
;+
; NAME:
;       history_wrap
;
; PURPOSE:
;       Given a single line or an array of lines intended to be used for
;       HISTORY records in a FITS file, split the line(s) so that each 
;       line is no wider than 72 characters with "(cont'd)" prepended on 
;       any continuation lines. This will result in FITS HISTORY records that
;       will not be truncated when written to a fits file.
;
; INPUTS:
;       line_in         a string or string array to be possibly split
;
; OPTIONAL INPUTS:
;       quiet           if set then program will not warn of output lines 
;                       that are longer than width (which should only happen 
;                       if a line of input contains a word that is wider than 
;                       maximum width
;
; OUTPUTS:
;       An array (or single string) with each line limited to no longer than
;       the width set by the width keyword or the default value.
;
; LIMITATIONS:
;       No splitting is done on strings in line_in that are larger than 72
;       characters (that is 72 characters long with no whitespace) -- these
;       will be truncated.  This could be changed, though it would take some
;       work.
;
; MODIFICATION HISTORY:
;       Written 3 December 2010 by Jonathan D. Slavin (SAO)
;       Changed to put "(cont'd)" on wrapped lines - 6 December 2010 - (JDS) 
;
;-
width = 72
n_in = n_elements(line_in)
n_out = 0
for n = 0,n_in-1 do begin
    if strlen(line_in[n]) gt width then begin
        words = strsplit(line_in[n],' ',/extract)
        nwords = n_elements(words)
        lines = words[0]
        space_left = width - (strlen(lines) + 1)
        nlines = 0
        for nw = 1,nwords-1 do begin
            if strlen(words[nw]) gt space_left then begin
                ; start a new line
                lines = [lines,'(cont''d) '+words[nw]]
                nlines += 1
                space_left = width
            endif else begin
                lines[nlines] = lines[nlines]+ ' ' +words[nw] 
                space_left = width - (strlen(lines[nlines]) + 1)
            endelse
        endfor
        if n_out eq 0 then lines_out = lines else lines_out = $
            [lines_out,lines]
        n_out = n_elements(lines_out)
    endif else begin
        if n_out eq 0 then lines_out = line_in[n] else lines_out = $
            [lines_out,line_in[n]]
        n_out += 1
    endelse
endfor
for nl = 0,n_elements(lines_out)-1 do begin
    if strlen(lines_out[nl]) gt width then begin
        if not keyword_set(quiet) then print, $
            'HISTORY_WRAP: line exceeds desired width: '+lines_out[nl]
        lines_out[nl] = strmid(lines_out[nl],0,72)
    endif
endfor
if n_elements(lines_out) eq 1 then lines_out = lines_out[0]
return, lines_out
end
