function xrt_undertainy,index,data,dark_type=dark_type,four_type=four_type,$
  strip_dark=strip_dark
; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       XRT_UNDERTAINY
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       To determine and return the uncertainy in the xrt data based on 
;       dark correction (lsback_away), nyquist noise correction (no_nyquist)
;       and fourier filtering correction (xrt_fourier_vacuum).
;
; CALLING SEQUENCE:
;       uncert_arr=xrt_undertainy(index,data,dark_type)
;
; INPUTS:
;          INDEX  - [Mandatory] 
;                    [Nz] dimensional index structure of level 0 data
;                    associated with DATA_IN and DARK_TYPE
;          DATA   - [Mandatory] 
;                    (3-dim float array, [Nx,Ny,Nz])
;                    The unprocessed (raw - level 0) image to be analyzed (unprocessed=1) 
;      DARK_TYPE  - [Optional] [default=0] 
;                    Type of dark subtraction used. See lsback_away and xrt_prep
;                     for more details 
;      FOUR_TYPE  - [Optional] [default=0]
;                      Type of Fourer cleaning used. See xrt_fourier_vacuum.pro
;                       for details
;                   
; KEYWORDS:
;                      
;      STRIP_DARK - [Optional] 
;                      Flag concerning use of the strip darks.  Default follows
;                       the standards. 
;                       If set, we will assume strip darks were used (they 
;                       have a slightly different uncertainty)
;
; OUTPUTS:
;     UNCERT_ARR  - [Nx,Ny,Nz] float array of the uncertainty
;
; EXAMPLES:
;
;   under_unc=xrt_undertainy(index,data,dark_type=2,four_type=2,/strip_dark)
;             returns an array of size(data) of the dark/nyquist/fourier 
;             uncertainties 
;
; COMMON BLOCKS:
;       None.
; NOTES:
;
;
; MODIFICATION HISTORY:
;
;progver = 'v2011.Jun.28' ; --- (AKobelski) Written by AKobelski
progver = 'v2012.Nov.10' ; --- (SSaar) Altered to mesh with new under_table
;
;-
; =========================================================================

;===== Check the inputs ===================================
if keyword_set(dark_type) then dktp=dark_type else dktp=0
if keyword_set(four_type) then frtp=four_type else frtp=0
if keyword_set(strip_dark) then strp=1 else strp=0

if (dktp lt 0 OR dktp gt 2) then begin
  print, 'Incorrect Dark Type Used in xrt_undertainy.pro'
  return,-1
endif

if (frtp lt 0 OR frtp gt 3) then begin
  print, 'Incorrect Fourier Filtering type used in xrt_undertainy.pro'
  return,-1
endif

siz=size(data,/dim)
;===== Do the work ===========

if n_elements(index) gt 1 then nz=siz(2) else nz=1

unc_per_img=fltarr(nz)
for j=0,nz-1 do unc_per_img(j)= $ 
  under_table(dktp,index(j),frtp,strip_flag=strp)

out_unc=fltarr(siz(0),siz(1),nz)

for i=0,nz-1 do out_unc(*,*,i)=unc_per_img(i)


return,out_unc

end
