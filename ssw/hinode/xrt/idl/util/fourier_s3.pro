;+
PRO fourier_s3, X, A, F, pder

; Fourier series until 3rd order.
;
;-


  C = 0.5*A[0] + A[2]*cos(x) + A[4]*cos(2*x) +A[6]*cos(3*x)
  S =            A[1]*sin(x) + A[3]*sin(2*x) +A[5]*sin(3*x)
  F = C + S

;If the procedure is called with four parameters, calculate the
;partial derivatives.
  IF N_PARAMS() GE 4 THEN $
    pder = [[replicate(0.5, N_ELEMENTS(X))], [sin(x)], [cos(x)] $
    	, [sin(2*x)], [cos(2*x)], [sin(3*x)], [cos(3*x)] ]
END
