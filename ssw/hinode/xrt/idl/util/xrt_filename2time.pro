function xrt_filename2time, files, level1=level1

; =========================================================================
;		
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;
;       XRT_FIND_FILES
;
; CATEGORY:
;
;       XRT utilities
;
;
; PURPOSE:
;
;      Return a list of times from XRT filenames
;
;
; CALLING SEQUENCE:
;
;       times = XRT_FILENAME2TIME(files)
;
; INPUTS:
;
;       files: (string or array of strings) list of XRT filenames
;
;
; KEYWORD INPUTS: 
;
;       level1:     If set, assume level1 format filename (L1_XRTYYYYMMDD_HHMMSS)
;
; COMMON BLOCKS:
;
; none
;
; KNOWN PROBLEMS:
;
; none
;
; CONTACT:
;
;       Comments, feedback, and bug reports regarding this routine may be
;       directed to this email address:
;                xrt_manager ~at~ head.cfa.harvard.edu
;
;
;
; MODIFICATION HISTORY:

progver='v2011.Feb.16'  ;--- K. Reeves (SAO) - Written
;
progver='v2012.Jul.10'  ;--- K. Reeves (SAO) - Modified to include fractions of seconds.  Because sometimes it matters.
;
;
;-
; =========================================================================

  nfiles = n_elements(files)
  times = strarr(nfiles)

  if strmatch(files[0], '') then begin
     times = ''
  endif else begin
 
     for i=0,nfiles-1 do begin
        ;; OS independent path separator
        ps=path_sep()
     
        ;; split the file path into chunks
        path_chunks = strsplit(files[i], ps, /extract)

        ;; last chunk from file path with be filename
        filename = path_chunks(n_elements(path_chunks)-1)

        ;; put filename into format wanted by fid2time
        if keyword_set(leve1) then begin 
           filetime = '_' + strmid(filename, 6, 15) 
           decimal_sec = strmid(filename,18,2)
        endif else begin 
           filetime = '_' + strmid(filename,3,15)
           decimal_sec = strmid(filename,18,2)
        endelse

        ;; get time string from filetime
        times[i] = fid2time(filetime) + decimal_sec

     endfor
  endelse
  return, times
end
