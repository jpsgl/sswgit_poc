FUNCTION xrt_rollangle, index

;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       XRT_ROLLANGLE
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       Calculate Roll Angle difference between XRT and SDO
;
; CALLING SEQUENCE:
;       roll_angle = xrt_rollangle(xrt_index)
;
; INPUTS:
;       XRT_INDEX  - [Mandatory] XRT index structure
;
; OUTPUTS:
;       ROLL_ANGLE - Difference of roll angle between XRT and SDO (in degree)
;
;
; EXAMPLES:
;
;   Correct Index Structure
;   IDL> roll_angle = xrt_rollangle(xrt_index)
;   IDL> xrt_index.crota1=roll_angle
;   IDL> xrt_index.crota2=roll_angle
;
;   Correct image (xrt_img0) for coalignment with SDO images
;   IDL> xrt_img = rot(xrt_img0,-roll_angle)
;
; MODIFICATION HISTORY:
;
progver = 'v2014.09.??' ;--- (KY) Written.
;-

sz0=size(index)
if sz0(sz0(0)+1) eq 8 then tt=anytim(index.date_obs) $
                      else tt=index

nn=n_elements(index)
roll_ang0=fltarr(nn)

time1=anytim('10-mar-12')
time2=anytim(' 8-jun-12')
time3=anytim('21-jan-13')

; Case-1 (tt < time1)
tmp_ss=where(tt le time1,tmp_nn)
if tmp_nn ne 0 then roll_ang0(tmp_ss)= $
           0.314067+0.0850355*sin(1.99892e-07*tt(tmp_ss)+3.45231)
; Case-2 (time1 < tt < time2 or time3 < tt)
tmp_ss=where((tt gt time1 and tt lt time2) or tt gt time3,tmp_nn)
if tmp_nn ne 0 then roll_ang0(tmp_ss)= $
           0.314457-0.0712843*sin(1.99474e-07*tt(tmp_ss)+0.445648)
; Case-3 (time2 < tt < time3)
tmp_ss=where(tt gt time2 and tt le time3,tmp_nn)
if tmp_nn ne 0 then roll_ang0(tmp_ss)= $
           0.314457-0.0712843*sin(1.99474e-07*tt(tmp_ss)+0.445648) + $
           0.00833730+0.0472532*sin(7.27225e-05*tt(tmp_ss)+0.661514)

if nn eq 1 then roll_ang0=roll_ang0(0)

return,-roll_ang0

END
