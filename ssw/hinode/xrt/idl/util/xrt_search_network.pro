PRO xrt_search_network,enable=enable,disable=disable,server=server $
                      ,remote_xrt_archive=remote_xrt_archive $
                      ,local_xrt_archive=local_xrt_archive,quiet=quiet
; ============================================================================
;+
; NAME:
;
; XRT_SEARCH_NETWORK
;
; PURPOSE:
;
; This program enables or disables the ability of certain XRT routines to
; remotely access XRT data from a web server and copy it to a local archive.
; Therefore - two archives can be specified, a "remote archive" (HTTP site that
; hosts the files), and a "local archive" (local path to the user's drive to
; download data to). This routine only sets the paths - doesn't perform any
; download.
;
;
; CATEGORY:
;
; XRT data handling
;
; CALLING SEQUENCE:
;
; xrt_search_network, [/enable OR /disable], [/quiet], $
;                    [server=server OR remote_xrt_archive=remote_xrt_archive], $
;                     [local_xrt_archive=local_xrt_archive]
;
;
; INPUTS:
;
; server: a string designating a remote server that hosts XRT data. Valid
;         options are: SAO, LMSAL, SDC. Overridden by remote_xrt_archive.
;         Enabling  sets the environmental $XRT_SEARCH_NETWORK to 'ENABLE'.
;         Disabling sets the environmental $XRT_SEARCH_NETWORK to 'DISABLE'.
;
; remote_xrt_archive: a URL to the base root tree of the remote archive. If set,
;                     overrides "server". If neither remote_xrt_archive nor
;                     the server are set, the environmental $HINODE_DATA_URL 
;                     is read. If blank, the default is set to the SAO archive
;
; local_xrt_archive: this specifies the base root tree of the local archive. 
;                    XRT fits files will be copied in subdirectories 
;                    YYYY/MM/DD/HXXXX of this path. If the directory does not 
;                    exist, an attempt is made to create it.  If there's any 
;                    problem with the path, it defaults to the current 
;                    directory.  If local_xrt_archive is not specified, the 
;                    environmental $XRT_DATA is used.
;
;
; OPTIONAL INPUTS:
;
; 
;
; KEYWORD PARAMETERS:
; /enable: enables copying of XRT data from remote sites
; /disable: disable copying of XRT data from remote sites
; /quiet: if set, supresses informative output
;
; OUTPUTS:
;
; None (but if /quiet is not set, some status is printed on the command line)
;
; OPTIONAL OUTPUTS:
;
; None
;
; COMMON BLOCKS:
;
; None
;
; SIDE EFFECTS:
;
; Possibly creates a directory in the user's drive for downloading XRT data
; Possibly modifies the follwing environmental variables: 
;   $XRT_SEARCH_NETWORK
;   $XRT_DATA
;   $HINODE_DATA_URL
;
; RESTRICTIONS:
;
; None known
;
; PROCEDURE:
;
; Check that the inputs are well formed and sets environmentals,
; falling back to some defaults
;
; EXAMPLE:
;
;  xrt_search_network,/enable,server='SAO',local_xrt_archive='/home/username/'
;
; NOTE:
;
; This procedure originally written in 2009 by Paolo C Grigis.
; Send comments & bug reports to xrt_manager@cfa.harvard.edu
;
; MODIFICATION HISTORY:
;
progver = 'v2009.Nov.04' ; -- (Paolo Grigis) Written by PG.
progver = 'v2009.Dec.09' ; -- (SSaar) Standardized, added warning of default.
progver = 'v2010.Apr.01' ; -- (SSaar) changed the off state of 
                         ; --- $xrt_search_network = 'DISABLE' 
;
;-
; ============================================================================

;verbose?
v=1-keyword_set(quiet)


;enables or disable remote access
IF keyword_set(enable)  THEN BEGIN
   mklog,'XRT_SEARCH_NETWORK','ENABLE'
   IF v THEN print,'Searching Network enabled'
ENDIF
IF keyword_set(disable) THEN BEGIN
   mklog,'XRT_SEARCH_NETWORK','DISABLE'
   IF v THEN print,'Searching Network disabled'
ENDIF


;setup local archive directory

;get current directory (fallback in case of problems with
;the input directories)

cd,current=current_dir

;if no input is given 
IF exist(local_xrt_archive) EQ 0 THEN BEGIN 
 
   ;gets the current value of the environmental
   xrt_archive=get_logenv('XRT_DATA')
   ;if not valid, uses current dir instead
   IF is_blank(xrt_archive) THEN BEGIN
      xrt_archive=current_dir
   ENDIF 
      
ENDIF ELSE BEGIN 
   ;user has input a local file - this takes priority if valid
   IF size(local_xrt_archive,/tname) NE 'STRING' THEN BEGIN
      IF v THEN print,'Invalid local xrt archive directory: must be a string'      
      
      ;sets the current dir instead
      xrt_archive=current_dir
      
   ENDIF ELSE  xrt_archive=local_xrt_archive[0]
ENDELSE 

;check for existence of directory
IF file_exist(xrt_archive) EQ 0 THEN BEGIN
   ;not found? will attempt to create it
   IF v THEN print,'Directory '+local_xrt_archive[0]+' not found.'
   file_mkdir,xrt_archive

   ;check for failure
   IF file_exist(xrt_archive) EQ 0 THEN BEGIN
      IF v THEN print,'Could not create directory '+local_xrt_archive[0]
      xrt_archive=current_dir
   ENDIF
ENDIF 

;ready to set the environmental
mklog,'XRT_DATA', xrt_archive
IF v THEN $ 
  print,'Setting the local XRT archive directory to '+get_logenv('XRT_DATA')


;setup remote archive location
hinode_data_url=get_logenv('HINODE_DATA_URL')

;if user wants to set it - otherwise keep default
IF is_blank(hinode_data_url) OR exist(server) OR exist(remote_xrt_archive) $ 
  THEN BEGIN 

   ;if remote_xrt_archive is set and valid - use that
   IF size(remote_xrt_archive,/tname) EQ 'STRING' THEN BEGIN 
      hinode_data_url=remote_xrt_archive[0]
   ENDIF ELSE BEGIN 

      ;if server NOT OK use default (SAO)
      IF size(server,/tname) NE 'STRING' THEN BEGIN 
         hinode_data_url='http://kurasuta.cfa.harvard.edu'
         IF v then print,'Blank or incorrect server. ' + $
                           'Defaulting remote archive to SAO.' 
      ENDIF ELSE BEGIN 

         ;choose server, default SAO
         CASE strtrim(strupcase(server),2) OF
            'SAO' :   hinode_data_url='http://kurasuta.cfa.harvard.edu'
            'LMSAL' : hinode_data_url='http://sot.lmsal.com/data/'
            'SDC':    hinode_data_url='http://www.sdc.uio.no/vol/fits/'
            ELSE :    begin
               hinode_data_url='http://kurasuta.cfa.harvard.edu'
               IF v then print,'Unknown server. ' + $
                           'Defaulting remote archive to SAO.'
            ENDELSE
         ENDCASE 

      ENDELSE 
   ENDELSE 

ENDIF 

mklog,'HINODE_DATA_URL',hinode_data_url
IF v THEN  $ 
  print,'Setting the remote XRT archive to '+get_logenv('HINODE_DATA_URL')

END 

