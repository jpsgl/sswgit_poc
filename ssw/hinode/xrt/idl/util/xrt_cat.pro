PRO xrt_cat, time0, time1, outcat, ofiles, search_array=search_array,  $
             _extra=_extra, urls=urls, refresh=refresh, error=error, $
             temp=temp, verbose=verbose, quiet=quiet, qabort=qabort, $
             qstop=qstop, quicklook=quicklook

; =========================================================================
;+
;
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;
;       XRT_CAT
;
; CATEGORY:
;
;       File I/O.
;
; PURPOSE:
;
;       Read XRT catalog for time/time-range; optionally search.
;
; CALLING SEQUENCE:
;
;       XRT_CAT, time0, time1, catx [,ofiles] [,search_array=search_array]
;                [,_extra=_extra] [,urls=urls] [,refresh=refresh] 
;                [,matchrecs=matchrecs] [,error=error] [,temp=temp]  
;                [,/verbose] [,/quiet] [,/qstop] [,qabort=qabort]
;
; INPUTS:
;
;       TIME0        - [Mandatory] (a standard time format) 
;                      Start time of data range.
;
;       TIME1        - [Mandatory] (a standard time format) 
;                      End time of data range.
;
; KEYWORDS:
;
;       SEARCH_ARRAY - [Optional] (See <struct_where.pro>)
;                      Give a set of search parameters to further refine
;                      the data set.
;       _EXTRA       - [Optional] (various possible types)
;                      Unknown parameters assumed to be in 
;                      PARAM=VALUE pairs -> <struct_where.pro>.
;                      Can also use /FOLD_CASE as a keyword to make
;                      string searches case-insensitive.
;       /SIRIUS      - [Optional] (Boolean)
;                      If set, then return Level-0 (fully reformatted) data.
;                      This is the default. Overrides /QUICKLOOK.
;       /QUICKLOOK   - [Optional] (Boolean)
;                      If set, then return "QuickLook" data instead of
;                      Level-0 data. Quicklook data arrives earlier, but
;                      can be incomplete and may not contain most 
;                      accurate keywords. Is overridden by /SIRIUS.
;       /URLS        - [Optional] (Boolean)
;                      If set, then return OFILES as URLs to a remote
;                      data server. (Default is to give local full paths.)
;       REFRESH      - [Optional] (Boolean) If set, then refresh the cache 
;                      (i.e., re-read the catalog).
;       TEMP         - [Optional] (Boolean) If set, then use a temporary
;                      (i.e., offline/in-progress) catalog database.
;       /VERBOSE     - [Optional] (Boolean) If set, print out extra
;                      information. Overrides "/quiet" (see Note #2).
;       /QUIET       - [Optional] (Boolean) If set, suppress messages
;                      (see Note #2).
;       /QSTOP       - [Optional] (Boolean) For debugging.
;
; OUTPUTS:
;
;       CATX         - [Mandatory] (structure array, [Nimg])
;                      XRT data catalog records. Gives a subset of
;                      the data keywords suitable for selecting a 
;                      data subset or browsing the available data.
;                      Actual image data is not included.
;       OFILES       - [Optional] (string array, [Nimg])
;                      List of full path filenames for the Level-0 data
;                      matching the catalog. If /URL, then instead returns
;                      data paths to a remote data server. (See Note #1.)
;       ERROR        - [Optional] (scalar number) Returns the ERROR keyword
;                      value from a call to <read_genxcat.pro>.
;       QABORT       - [Optional] (Boolean) Indicates that the program
;                      exited gracefully without completing. (Might be
;                      useful for calling programs.)
;                      0: Program ran to completion.
;                      1: Program aborted before completion.
; EXAMPLES:
;
;       Basic call to get a catalog array:
;       IDL> t0 = '01:00 24-Apr-2007'
;       IDL> t1 = '03:00 24-Apr-2007'
;       IDL> xrt_cat, t0, t1, cat
;
; COMMON BLOCKS:
;
;       None
;
; NOTES:
;
;       1) This routine is basically a wrapper for <sot_cat.pro>.
;
;       2) A list of Level-0 data filenames corresponding to a catalog 
;          array can be generated with the routine <xrt_cat2files.pro>.
;          This list can be returned in the optional OFILES parameter of
;          <xrt_cat.pro> for the full data time-range, but sometimes a user 
;          wishes to further reduce the catalog array by selections, and 
;          then generate a filelist.
;
;       3) There are three levels of verbosity.
;          a) "verbose" = highest priority. All errors and messages are
;                         displayed. ("if q_vb")
;          b) "quiet" = lower priority. No errors or messages are
;                       displayed. ("if q_qt")
;          c) neither = lowest priority. All errors and some messages are
;                       displayed. ("if not q_qt")
;
; MODIFICATION HISTORY:
;
;      10-Apr-2006 - S.L.Freeland
;      19-jul-2006 - S.L.Freeland - xrt hook - use cache_data.pro
;                                   instead of explicit common - lookup
;                                   cache by instrument (sot/xrt)
;      16-oct-2006 - S.L.Freeland - change default (packages/solarb ->
;                                   $SSWDB/hinode based on start time)
progver = 'v2007-Mar-22' ;--- (J.W.Cirtain) Converted <sot_cat.pro> 
;                             to <xrt_cat.pro>.
progver = 'v2007-Apr-24' ;--- (M.Weber) Clean up and fixed various bugs.
progver = 'v2007-May-22' ;--- (M.Weber) Reverted to being a wrapper for
;                             <sot_cat.pro>. Added warning message for 
;                             /URLS. Added fixes for malformed filepaths:
;                             'xrt/level0', and 'd' for darks.
progver = 'v2007-May-23' ;--- (M.Weber) Fixed a bug where "outcat" was
;                             mislabeled.
progver = 'v2007-Jun-26' ;--- (M.Weber) Added error catch when <sot_cat>
;                             finds zero matches.
progver = 'v2007-Jul-06' ;--- (M.Weber) Added another error catch for 
;                             filenames.
progver = 'v2007-Jul-17' ;--- (M.Weber) Forced /level0 switch to <sot_cat>.
progver = 'v2007-Aug-14' ;--- (M.Weber) Removed "d" fix for filenames of 
;                             darks, because <sot_cat> now handles it
;                             correctly.
progver = 'v2007-Aug-15' ;--- (M.Weber) Removed kludge for the $XRT_DATA
;                             path.
progver = 'v2007-Nov-08' ;--- (M.Weber) Added pass throughs for urls,
;                             search_array, error, refresh. Removed
;                             warning about the /url keyword.
progver = 'v2009-May-05' ;--- (M.Weber) Fixed some bugs with the /quicklook
;                             interface to <sot_cat>.
progver = 'v2014-Jan-28' ;--- (P.McCauley) Switched QuickLook to QLraw for 
;                             for QuickLook flag. 
;-
; =========================================================================

  ;=== Set Booleans which control print statements.
  ;=== Keyword "verbose" overrules "quiet".
  q_vb = keyword_set(verbose)
  q_qt = keyword_set(quiet) and (not q_vb)

  ;=== Initialize program constants.
  prognam = 'XRT_CAT'
  prognul = '       '
  q_urls  = keyword_set(urls)
  ofiles  = ''
  qabort  = 0B
  qstop   = keyword_set(qstop)

  ;=== Pass work off to <sot_cat.pro>.
  sot_cat, time0, time1, outcat, ofiles, _extra=_extra, /XRT, $
           sirius=sirius, quicklook=quicklook, urls=q_urls,   $
           search_array=search_array, refresh=refresh, error=error

  ;=== Handle possible errors.
  if (datatype(outcat)  eq 'INT') then begin
    if not q_qt then box_message,                                        $
      [prognam+' No catalog entries were located. Aborting.']
    qabort = 1B
    return
  endif

  if (ofiles[0] ne '') then begin
    ;; Some obsolete stuff deleted here.
  endif else begin
    if not q_qt then box_message,                                        $
      [prognam+' No matching filenames were located. Continuing.']
  endelse

  if keyword_set(quicklook) and NOT keyword_set(urls) then $
  	ofiles = str_replace(ofiles, 'QuickLook', 'QLraw')

  ;=== Finish.


END ;======================================================================
