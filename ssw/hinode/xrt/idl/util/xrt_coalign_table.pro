pro xrt_coalign_table, date0, fit_aol, $
  xrt_fit_ax, xrt_fit_bx, xrt_fit_ay, xrt_fit_by, file= file, $
  valid= valid, offset= offset, dir0=dir0, verbose=verbose

;+
; NAME:
;   XRT_COALIGN_TABLE
; Purpose:
;   Read co-align table file specified with "date0" and return
;   table of orbital variation as a function of AOL deg.
; INPUTS:
;   date0 - the date of the coalignment file
;           example is '070214'
; OPTIONAL INPUTS:
;   file - the name of coalignment model file, such as
;             'alignment_model070325.txt'
;   dir0 - if need to change the directory definition for model file
; OUTPUTS:
;   fit_aol    - AOL (satellite latitude) value, deg
;   xrt_fit_ax - XRT orbital variation table in X (N-S) in reference of UFSS_A
;   xrt_fit_bx - XRT orbital variation table in X (N-S) in reference of UFSS_B
;   xrt_fit_ay - XRT orbital variation table in Y (E-W) in reference of UFSS_A
;   xrt_fit_by - XRT orbital variation table in Y (E-W) in reference of UFSS_B
; OPTIONAL OUTPUTS:
;   valid - 1= valid data, 0= invalid
;   offset - XRT disk center values when N=945deg or E=-945deg pointing
;      at AOL= 0 deg
; HISTORY:
;   2007.09.22  T.Shimizu   initial
;           25              added a warning message if the specified date
;                            is out of the verified observing period.
;        09.26  T.Shimizu   a bug fixed
;   2008.04.14  T.Shimizu   add dir0 option
;-


;dir0= '/home/shimizu/alignment/jitter/'   ; directory of alignment files
;dir0= '/soda/hinode/cmn/alignment/model/'
if keyword_set(dir0) then dir0= dir0 else $
  dir0= getenv('SSWDB') + '/hinode/gen/alignment/model/'   
   ; directory of alignment files

ymd0= '20'+ strmid(date0,0,2)+'-'+strmid(date0,2,2)+'-'+strmid(date0,4,2)
jd0=  anytim2jd(ymd0)

;
;  Warning message if the specified date is out of the verified observing period
;
;    verified period -  Nov 2006 - 6 May 2007

   jd1= anytim2jd('2006-11-01')
   jd2= anytim2jd('2007-05-06')

 if jd0.int le jd1.int and jd0.int ge jd2.int then $
   print, '% Warning: specified date is out of the verified observing period'
;
;

;file0= dir0 + 'alignment_model' + date0 + '.txt'
if keyword_set(file) then file0= dir0 + file else begin

  files= file_search(dir0 + 'alignment_table*.txt')
  pos= strpos(files, 'table')
  siz= size(files)
  da= strarr(siz(1))
  for i=0, siz(1)-1 do da[i]= strmid(files[i],  pos[i] + 5, 6)
  ymd= '20'+ strmid(da,0,2)+'-'+strmid(da,2,2)+'-'+strmid(da,4,2)
  jd= anytim2jd(ymd)

  x= where(abs(jd.int - jd0.int) eq min(abs(jd.int - jd0.int)))   ; closest time

  file0= files(x[0])

endelse

if keyword_set(verbose) then print, '% Reading coalignment model file: ', file0
openr, unit, file0, /get_lun
 a= strarr(364)
 readf, unit, a
free_lun, unit

; a[1]  valid or invalid
; a[2]  XRT address at co-alignment program run  at AOL=0 deg
; a[3]-a[363] - table

valid= strsplit(a[1], ',',/extract)
valid= fix(valid[1:4])   ; for XRT

offset=  strsplit(a[2], ',',/extract)
offset= float(offset[1:4])  ; for XRT

fit_aol= fltarr(361)
xrt_fit_ax= fltarr(361)
xrt_fit_bx= fltarr(361)
xrt_fit_ay= fltarr(361)
xrt_fit_by= fltarr(361)

for i= 0, 360 do begin
 dat= strsplit(a[i+3], ',',/extract)
 fit_aol[i]= float(dat[0])
 xrt_fit_ax[i]= float(dat[1])
 xrt_fit_bx[i]= float(dat[2])
 xrt_fit_ay[i]= float(dat[3])
 xrt_fit_by[i]= float(dat[4])
endfor

return
end
