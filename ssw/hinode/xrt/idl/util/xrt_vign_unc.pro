function xrt_vign_unc,index,quiet=quiet
; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       XRT_VIGN_UNC
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       Determine Uncertainty due to Vignetting Function 
;
; CALLING SEQUENCE:
;       image_out = xrt_vign_unc(index) 
;
; INPUTS:
;       INDEX  - [Mandatory]  index structure for image for which to estimate
;                           vignetting uncertainties
;
; OUTPUTS:
;       IMAGE_OUT - [Mandatory] (2-dim float array, [Nx,Ny])
;                   An uncertainty image of vignetting based uncertainties
;
; EXAMPLES:
;
;       Basic usage:
;       IDL> image_out = xrt_vign_unc(index)
;
;
;  COMMON BLOCKS:
;       None.
;
; NOTES:
;
;
; MODIFICATION HISTORY:
;
progver = 'v2011.July.6' ; --- (AKobelski) Written By Adam Kobelski
;                                   based on nono_vignette by SSaar
progver = 'v2013.Aug.20' ; --- (SSaar) Changed calibration from model based
;                                   on ground-based data (not the flight
;                                   optics) to an empirical calibration 
;                                   based on B. Ishibashi's vignetting
;                                   analysis (12 June 2008,rev 2)
progver = 'v2013.Aug.29' ; --- (SSaar) Simplified calibration; added quiet key
;
;
;-
; =========================================================================

;===== Check if a dark  image; if not, do the correction
  imtyp=index.ec_imtyp
  if imtyp ne 1 then begin

;===== Get dimensions & parameters of image.
    nx = index.naxis1
    ny = index.naxis2
    chipsum=index.chip_sum
    x0chip=index.p1col
    y0chip=index.p1row

;===== use basic formula to compute correction
    graze_angle = 0.91*60         ; average graze angle in arcmins (LG)
    x0 = 1024./chipsum            ; vignetting center in x  (nominal) 
    y0 = 1024./chipsum            ; vignetting center in y (nominal)
    arcsec_per_pix = 1.0286*chipsum

; fix for different ROI, binning

    x=findgen(nx)  
    x=rebin(x,nx,ny) + x0chip/chipsum 
    y=findgen(ny)
    y=rotate(rebin(y,ny,nx),1) + y0chip/chipsum

    angle=sqrt((x-x0)^2 +(y-y0)^2)*arcsec_per_pix/60. ; field angle in arcmins
  
    cv=[21.4681, -6.11904, 0.444524]
    vign_unc= 0.0045*(angle le 9.916) + $
       (angle ge 9.916)*(poly(angle,cv)/1000 -.0045)
      
  endif else begin
;===== if you have a dark frame, return zeroes. 
     if not keyword_set(quiet) then $ 
       print,'XRT_VIGN_UNC: NO vignetting correction: dark frame!'
     vign_unc=fltarr(index.naxis1,index.naxis2)
  endelse
  
  return,vign_unc



END
