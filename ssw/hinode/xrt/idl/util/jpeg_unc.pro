FUNCTION jpeg_unc,index,data
; =========================================================================
;+
; PROJECT:
;       Solar-B / XRT
;
; NAME:
;       JPEG_UNC
;
; CATEGORY:
;       Data calibration
;
; PURPOSE:
;       To determine and return the uncertainy in the xrt data based on 
;       the on-board JPEG compression. 
;
; CALLING SEQUENCE:
;       uncert_arr=jpeg_unc(index,data)
;
; INPUTS:
;          INDEX  - [Mandatory] 
;                    [Nz] dimensional index structure of level 0 data
;                    associated with DATA_IN and DARK_TYPE
;          DATA   - [Mandatory] 
;                    (3-dim float array, [Nx,Ny,Nz])
;                    The unprocessed (raw - level 0) image to be analyzed (unprocessed=1) 
;                   
; KEYWORDS:
;          None.            
;
; OUTPUTS:
;     UNCERT_ARR  - [Nx,Ny,Nz] float array of the uncertainty from JPEG compression
;
; EXAMPLES:
;
; COMMON BLOCKS:
;       None.
;
; NOTES:
;      - Does not take Binning into account.  Will be included "soon"
;      - Would like to remove a for loop or three.
;
; MODIFICATION HISTORY:
;
progver = 'v2011.July.18' ; --- (AKobelski) Written by AKobelski
progver = 'v2013.Feb.01' ; --- (S Saar) Added quiet keyword to get_jpeg_unc
;
;-
; =========================================================================

indim=size(data,/dimensions)

init=0

xit=indim(0)/8
yit=indim(1)/8


if n_elements(indim) gt 2 then zit = indim(2) else zit=1

tss=zit*yit*xit
out_unc=fltarr(indim)
compression=(get_xrt_complevel(index)).compqfact


for k=0,(zit-1) do begin
   for i=0,(xit-1) do begin
      loi=8*i & hii=8*(i+1)-1
      for j=0,(yit-1) do begin

        loj=8*j & hij=8*(j+1)-1
        sub8x8o=data[loi:hii,loj:hij,k]
        out_unc[loi:hii,loj:hij,k]=get_jpeg_unc((max(sub8x8o)-min(sub8x8o)), $
          compression(k),quiet=1)
      endfor
   endfor
endfor

return,out_unc

END
