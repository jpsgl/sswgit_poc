;+
; Project     : EIS
;
; Name        : EIS_LIST_RASTER
;
; Purpose     : List raster entries in EIS catalog.
;
; Category    : utility database
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : OBS = raster observation structure
;               COUNT = number of elements found
;
; Keywords    : ERR = error string
;               SEARCH = additional search string
;               (e.g. SEARCH='RAST_ID = 37')
;               FILES= list of matching filenames
;
; History     : 12-Sept-2007,  D.M. Zarro (ADNET) - Written
;               15-Mar-2009, Zarro (ADNET) 
;                - added capability to enter search strings as
;                  keywords (e.g. rast_id=37)
;
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;--------------------------------------------------------------------

pro eis_list_raster,tstart,tend,rasters,count,err=err,$
                  files=files,search=search,_extra=extra,verbose=verbose

;-- list main entries first

files=''
rasters=-1
eis_list_study,tstart,tend,obs,count,err=err,search=search,_extra=extra,verbose=verbose
if is_string(err) or (count eq 0) then return

;-- parse potential keywords

isearch=struct2str(extra)
if is_string(search) then rsearch=search
if is_string(isearch) then begin
 if is_string(rsearch) then rsearch=rsearch+','+isearch else rsearch=isearch
endif

;-- list matching rasters

rasters = {EIS_EXPER_LIST}
obj=obj_new('db_tools')
rsearch=match_tags(rsearch,rasters)
for i=0,count-1 do begin
 exper=rasters
 id=strtrim(obs[i].tl_id,2)
 s='TL_ID = '+id
 if is_string(rsearch) then s=s+','+trim(rsearch)

 if keyword_set(verbose) then begin
  message,'Searching parameters - '+s,/cont
 endif

 obj->db_rec_search, 'EIS_EXPERIMENT', s, exper, n,_extra=extra
 if n gt 0 then begin
  if exist(robs) then robs=[temporary(exper),temporary(robs)] else $
   robs=temporary(exper)
 endif
endfor

obj_destroy,obj
count=n_elements(robs)
if count eq 0 then return

rasters=temporary(robs)

if arg_present(files) then files=strtrim(rasters.filename,2)

return & end
