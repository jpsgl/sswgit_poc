;+
; Project     : EIS
;
; Name        : EIS_DBASE
;
; Purpose     : install site-specific EIS Database
;
; Category    : system
;                   
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : ISAS = set EIS database to ISAS 
;
; History     : 28-Jan-2003,  D.M. Zarro (EER/GSFC)  Written
;               23-Jan-2007, Zarro (ADNET/GSFC) 
;                - added site-specific directories
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro eis_dbase,_ref_extra=extra,verbose=verbose,user=user,isas=isas,nrl=nrl,ral=ral

defsysv,'!priv',2
verbose=keyword_set(verbose)

if keyword_set(isas) then begin
 isas='/home/flare/slb/eisco/planning_db/timeline_db'
 mklog,'ZDBASE_EIS',isas+':+$EIS_PLAN_DB:$EIS_CAT_DB'
 s=fix_zdbase(/eis,_extra=extra)
 if verbose then message,'setting ZDBASE_EIS for ISAS',/cont
 return
endif

if keyword_set(ral) then begin
 ral='/home/ccdjar/jar24/planning_db/timeline_db'
 mklog,'ZDBASE_EIS',ral+':+$EIS_PLAN_DB:$EIS_CAT_DB'
 s=fix_zdbase(/eis,_extra=extra)
 if verbose then message,'setting ZDBASE_EIS for RAL',/cont
 return
endif

;-- default to $SSW

delim=get_path_delim()
zdbase_eis=chklog('ZDBASE_EIS')
if is_blank(zdbase_eis) then $
 mklog,'ZDBASE_EIS','+$EIS_PLAN_DB'+delim+'$EIS_CAT_DB'
s=fix_zdbase(/eis,_extra=extra)
if verbose then message,'setting ZDBASE_EIS for SSW',/cont


return & end

