;+                                                                             >
; Project     : Solar-B/EIS                                                    >
;                                                                              >
; Name        : EIS_FITS_URL                                                     >
;                                                                              >
; Purpose     : return URL and path to EIS FITS data file for 
;               specified time
;                                                                              >
; Category    : sockets                                                        >
;                                                                              >
; Inputs      : DATE = time/date of file (e.g. 01:00 1-May-01)
;                      or filename EIS_010203_0300.fits                                                 >
;                                                                              >
; Outputs     : URL = of file location
;
; Keywords    : None 
;                                                                              >
; History     : 1-June-2006,  D.M. Zarro (L-3Com/GSFC), Written                 >
;                                                                              >
; Contact     : DZARRO@SOLAR.STANFORD.EDU                                      >

function eis_fits_url,date,err=err,_ref_extra=extra

;-- accept time or filename input

if is_blank(date) then return,''

if not valid_time(date,err=err) then begin
 ftime=file2time(date)
 if not valid_time(ftime,err=err) then return,''
endif else ftime=date

fid=time2fid(ftime)

url=eis_server(path=path,/full,_extra=extra,/no_check)

return,url+path+'/'+fid

end





