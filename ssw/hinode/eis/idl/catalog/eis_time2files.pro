;+
; Project     : EIS
;
; Name        : EIS_TIME2FILES
;
; Purpose     : Return URL locations of EIS raster files
;
; Category    : utility database
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : FILES = list of EIS file names with URL path
;               COUNT = number of elements found
;
; Keywords    : ERR = error string
;               SEARCH = additional search string
;               (e.g. SEARCH='RAST_ID = 37')
;              FILES= list of matching filenames
;
; History     : 13-March-2009,Zarro (ADNET) - Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;--------------------------------------------------------------------

function eis_time2files,tstart,tend,count,_ref_extra=extra

eis_list_raster,tstart,tend,rasters,count,files=files,_extra=extra
if count eq 0 then return,''
fids=parse_time(files)
url=eis_server(path=path,/full,_extra=extra,/no_check)
file_urls=url+path+'/'+string(fids.year,'(i4)')+'/'+$
          string(fids.month,'(i2.2)')+'/'+string(fids.day,'(i2.2)')+'/'+files

return,file_urls & end
