;+
; Project     : EIS
;
; Name        : EIS_LIST_EXPER
;
; Purpose     : List raster entries in EIS catalog.
;
; Category    : utility database
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : OBS = raster observation structure
;               COUNT = number of elements found
;
; Keywords    : ERR = error string
;               SEARCH = additional search string
;               (e.g. SEARCH='RAST_ID = 37')
;
; History     : 12-Sept-2007,  D.M. Zarro (ADNET) - Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;--------------------------------------------------------------------


pro eis_list_exper,tstart,tend,rasters,count,_ref_extra=extra

message,'Please use "eis_list_raster" instead',/cont

eis_list_raster,tstart,tend,rasters,count,_extra=extra

return & end
