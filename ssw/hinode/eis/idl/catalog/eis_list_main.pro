;+
; Project     : EIS
;
; Name        : EIS_LIST_MAIN
;
; Purpose     : List main entries in EIS main catalog.
;
; Category    : utility database
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : OBS = main observation structure
;               COUNT = number of elements found
;
; Keywords    : ERR = error string
;               SEARCH = additional search string (e.g. SEARCH='STUDY_ID = 37')
;
; History     : 6-Apr-2006,  D.M. Zarro (ADNET) - Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
;--------------------------------------------------------------------

pro eis_list_main,tstart,tend,obs,count,err=err,search=search,_ref_extra=extra

err=''
count=0

;-- check for valid time 

if valid_time(tstart)*valid_time(tend) eq 0 then begin
 err='invalid time input'
 pr_syntax,'eis_list_main,tstart,tend,obs,count'
 return
endif

t1=anytim2tai(tstart)
t2=anytim2tai(tend)

obj=obj_new('db_tools')
obs = {EIS_MAIN_LIST}
rsearch=match_tags(search,obs)

s = 'DATE_OBS > ' + TRIM (t1, '(F15.3)') + ', DATE_END < ' + TRIM (t2, '(F15.3)')
if is_string(rsearch) then s=s+','+trim(rsearch) 
obj->db_rec_search, 'EIS_MAIN', s, obs, count,_extra=extra
 
obj_destroy,obj

return & end
