;+
; Project     : EIS
;
; Name        : EIS_LIST_STUDY
;
; Purpose     : List study entries in EIS main catalog.
;
; Category    : utility database
;                   
; Inputs      : TSTART, TEND = start/end times for list
;
; Outputs     : OBS = main observation structure
;               COUNT = number of elements found
;
; Keywords    : ERR = error string
;               SEARCH = additional search string
;               (e.g. SEARCH='STUDY_ID = 37')
;               FILES = matching filenames
;
; History     : 6-Apr-2006,  D.M. Zarro (ADNET) - Written
;               15-Mar-2009, Zarro (ADNET)
;                - added capability to enter search strings as
;                  keywords (e.g. study_id=37)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU

;-
;--------------------------------------------------------------------

pro eis_list_study,tstart,tend,obs,count,err=err,$
             search=search,_extra=extra,files=files,verbose=verbose

err=''
count=0

;-- check for valid time 

if valid_time(tstart)*valid_time(tend) eq 0 then begin
 err='invalid time input'
 pr_syntax,'eis_list_study,tstart,tend,obs,count'
 return
endif

t1=anytim2tai(tstart)
t2=anytim2tai(tend)

;-- parse potential keywords 

isearch=struct2str(extra)
if is_string(search) then rsearch=search
if is_string(isearch) then begin
 if is_string(rsearch) then rsearch=rsearch+','+isearch else rsearch=isearch
endif

obj=obj_new('db_tools')
obs = {EIS_MAIN_LIST}
rsearch=match_tags(rsearch,obs)
s = 'DATE_OBS > ' + TRIM (t1, '(F15.3)') + ', DATE_END < ' + TRIM (t2, '(F15.3)')
if is_string(rsearch) then s=s+','+trim(rsearch) 
if keyword_set(verbose) then message,'Searching parameters - '+s,/cont

obj->db_rec_search, 'EIS_MAIN', s, obs, count,_extra=extra
 
obj_destroy,obj

if count gt 0 then files=strtrim(obs.filename,2)

return & end
