;+
; 
;             NAME : eis_slit_tilt
; 
;          PURPOSE : Calculates the slit tilt as a function position
;                    along the CCD.
;
; CALLING SEQUENCE : dw = eis_slit_tilt(yws,ny,long=long,short=short)
; 
;           INPUTS : yws - y window start, this parameter is in the
;                    header.
; 
;                    ny - the number of elements along y.
; 
;  OPTIONAL INPUTS : d - the structure returned from eis_getwindata.
; 
;         KEYWORDS : long - slit tilt for long band
;                    short - slit tilt for short band
; 
;          OUTPUTS : An estimate of the slit tilt in Angstroms.
;
; OPTIONAL OUTPUTS : locations - CCD Y positions (for plotting)
;                    info - information on the calculation
;
;    COMMON BLOCKS : none
; 
;          EXAMPLE : dw  = eis_slit_tilt(256,512,/short,locations=y)
;                    wvl = wvl - dw
;                    plot,y,dw
;                    ALTERNATIVE USAGE:
;                    d  = eis_getwindata(file,wave)
;                    dw = eis_slit_tilt(d,locations=y)
;
;    SPECIAL CALLS : Needs to have the file eis_slit_title.genx in the
;                    same directory.
;
;          WRITTEN : HPW : 17-MAY-2007
;                    HPW : 23-MAY-2007 : added long/short
;                    HPW : 25-JUN-2007 : modified for new calculations
;                    HPW : 07-JUL-2007 : long/short bug fixes
;                    HPW : 09-OCT-2007 : added 2" slit
; 
;-

function eis_slit_tilt,input,ny,long=long,short=short,locations=locations,$
                       info=info,slit=slit

  ;; --- check inputs
  if datatype(input) eq 'STC' then begin
    ny    = input.ny
    yws   = input.hdr[0].yws
    det   = strcompress(input.hdr[0].TWBND,/remove_all)
    short = 0 
    long  = 0
    slit  = str_replace(input.hdr[0].slit_id,'"')
    if det eq 'A' then long = 1 else short = 1
  endif else begin
    yws = input
    if not(keyword_set(slit)) then begin
      message,'SLIT NOT SPECIFIED, RETURING RESULTS FOR 1" SLIT',/info
      slit = '1'
    endif else slit = trim(slit)
  endelse

  case 1 of 
    keyword_set(short): i = 0
    keyword_set(long):  i = 1
    else: message,"EITHER LONG OR SHORT KEYWORD MUST BE USED"
  endcase

  ;; --- find the slit tilt file
  file = find_with_def('eis_slit_tilt.pro',!path)
  file = str_replace(file,'.pro','_s'+slit+'.genx')

  ;; --- read the fit, the fit starts at CCD pixel 0 but has been
  ;; derived using data from the middle of the detector
  restgen,fit,version,time_stamp,file=file

  fit = reform(fit[i,*])

  ;; -- calcultate the tilt in Angstroms as a function of pixel position
  y  = yws + findgen(ny)
  dw = poly(y,fit)

  ;; -- some optional output
  locations = y
  bands = ['SW','LW']
  info  = {version: version, time_stamp: time_stamp, fit: fit, $
           yws: yws, ny:  ny, band: bands[i]}

return,dw
end
