
;+
; NAME:
;       EIS_GETWINDATA
;
; PURPOSE:
;       Returns EIS data from one spectral window.
;
; CALLING SEQUENCE:
;       d = eis_getwindata(file, [iwin ,keywords])
;
; INPUTS:
;       FILE: input EIS fits file. See restrictions.
;
; OPT. INPUT: 
;
;       IWIN: scalar with the index of the desired windows (iwin <
;             25). This can also be a wavelength or as a string that
;             matches one of the line ids.
;
; OUTPUTS:
;       Structure with data and header information. 
;
; KEYWORDS:
;
; RESTRICTIONS: 
;       The name of the input only matters when trying to retrieve 
;       the errors. The routine expects a pair of L1 and ER files
;       (standard outputs from EIS_PREP) to correctly populate the 
;       tags. Otherwise it will return zeroes in the .ERR tag. Same 
;       will happen if input is a L0 file.
;
; WRITTEN: H.P.Warren. NRL. March 2007.
;
; MODIFICATION HISTORY:
;       2007/04/09 I. Ugarte-Urra. NRL. Added documentation.
;       2007/04/24 IUU. Interpret correctly if error file given as input.
;       2007/04/27 IUU. Release to SSW.
;       2007/05/09 HPW. Modified iwin input.
;       2007/05/15 HPW. Modified iwin input.
;       2007/06/28 HPW. Fixed bug related to reading error file.
;       2007/07/05 PRY. Changed way error filename is derived.
;       2007/10/17 HPW. Added file modification time to common block.
;
;-
;; **********************************************************************

function eis_getwindata,input_file,input_iwin

  if n_params() eq 2 then iwin = input_iwin

  ;; ----------------------------------------------------------------------
  ;; --- filename & window number

  ;; --- look for the file
  datfile = eis_getfilename(input_file)
  
  ;; --- In case input is error file, go find data
  break_file,datfile,disk,dir,name,ext
  if strpos(name,'er') ne -1 then begin
      datfile = str_replace(datfile,'_er_','_l1_')
      break_file,datfile,disk,dir,rename,ext
      message,name + ' is the error file.',/info 
      message,'Extracting data from: ' + rename, /info
  endif
  if not(file_exist(datfile)) then begin
    message,'no file found . . .',/info
    return,0
  endif

  if n_elements(iwin) eq 0 then begin  
    ;; --- select iwin if it is not input, no error checking otherwise
    message,'no window number input, select one . . .',/info
    wininfo = eis_get_wininfo(datfile,/list,nwin=nwin)
    iwin = 0
    read,iwin,prompt='Select a window to read [0...'+trim(nwin-1)+']> '
  endif else begin
    wininfo = eis_get_wininfo(datfile,nwin=nwin)
    ;; --- iwin has been input, check type
    if datatype(iwin) eq 'STR' then begin
      ;; --- string, match line id
      iwin = eis_get_iwin(datfile,iwin)
    endif else begin
      ;; --- match wavelength 
      if iwin gt 25 then begin
        iwin = eis_get_iwin(datfile,iwin)
      endif
    endelse
  endelse
  
  if iwin lt 0 or iwin ge nwin then begin
    message,'window out of range ('+trim(iwin)+') . . . ',/info
    return,0
  endif

  ;; ----------------------------------------------------------------------
  ;; --- read the object

  ;; --- record file name AND modification time
  info = file_info(datfile)
  datfile_s = datfile+','+trim(info.mtime)
  
  common eis_getwindata_com,last_file,data,data_er
  if n_elements(last_file) eq 0 then last_file = ''
  
  if last_file ne datfile_s then begin
    ;; --- destroy the old object
    if datatype(data) eq 'OBJ' then begin
      obj_destroy,data
      delvarx,data
    endif
    if datatype(data_er) eq 'OBJ' then begin
      obj_destroy,data_er
      delvarx,data_er
    endif

    ;; --- make a new object
    data = obj_new('eis_data',datfile)

    ;; --- look for error file
    basename=file_basename(datfile)
    dirname=file_dirname(datfile)
    errbasename=str_replace(basename,'l1','er')
    errfile=concat_dir(dirname,errbasename)

    if errfile eq datfile then errfile=''
    if file_exist(errfile) then data_er = obj_new('eis_data',errfile)

    last_file = datfile_s
 endif 

  ;; ----------------------------------------------------------------------
  ;; --- extract the data for this window
  windata = data->getwindata(iwin,data_er=data_er)   

return,windata
end
