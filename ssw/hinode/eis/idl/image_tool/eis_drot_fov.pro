;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_DROT_FOV
;
; Purpose     : Differentially rotate FOV structure
;
; Category    : Planning
;
; Syntax      : IDL> dfov=eis_drot_fov(fov,interval)
;
; Inputs      : FOV = FOV structure
;
; Outputs     : Diff rotated FOV structure
;
; Keywords    : See ROT_XY
;       
; History     : 16-May-2006, Zarro (L-3Com/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_drot_fov,fov,interval,tend=tend,_ref_extra=extra,offlimb=offlimb

if eis_limb_fov(fov) then return,fov

tstart=fov.date_obs
x=fov.x
y=fov.y

xx=(x[0]+x[1])/2.
width=(x[1]-x[0])
yy=(y[0]+y[2])/2.
height=(y[2]-y[0])

rr=rot_xy(xx,yy,interval,tstart=tstart,tend=tend,_extra=extra,offlimb=offlimb)

if offlimb then begin
 message,'FOV is off limb',/cont
 return,fov
endif

rx=rr[0,0]
ry=rr[0,1]

rfov=fov
pm=rx-width/2.
pp=rx+width/2.
rfov.x=[pm,pp,pp,pm,pm]

pm=ry-height/2.
pp=ry+height/2.
rfov.y=[pm,pm,pp,pp,pm]

if is_number(interval) then begin
 rfov.date_obs=anytim2utc(anytim2tai(rfov.date_obs)+interval,/vms)
endif else begin
 if valid_time(tend) then rfov.date_obs=anytim2utc(tend,/vms)
endelse

return,rfov

end




