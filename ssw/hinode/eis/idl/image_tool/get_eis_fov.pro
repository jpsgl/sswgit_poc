;+
; Project     : SOLAR-B/EIS
;
; Name        : GET_EIS_FOV
;
; Purpose     : Return EIS FOV structure
;
; Category    : Planning
;
; Syntax      : IDL> eis_get_fov,width,height
;
; Inputs      : None
;
; Outputs     : WIDTH, HEIGHT = width and height (arcsecs) of EIS fov
;
; Keywords    : None
;       
; History     : 20-Nov-2006, Zarro (ADNET/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro get_eis_fov,width,height

width=590.
height=512.

return

end





