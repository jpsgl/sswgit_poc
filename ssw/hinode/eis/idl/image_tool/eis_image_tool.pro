;+                                                                                        
; PROJECT:                                                                                
;       SOLAR-B                                                                           
;                                                                                         
; NAME:                                                                                   
;       EIS_IMAGE_TOOL                                                                    
;                                                                                         
; PURPOSE:                                                                                
;       Pointing tool for EIS planning tool                                               
;                                                                                         
; CALLING SEQUENCE:                                                                       
;       EIS_IMAGE_TOOL [, fits_file] [, point_stc=point_stc] [, start=start]              
;                  [, /reset]              
;                                                                                         
; INPUTS:                                                                                 
;       None required.                                                                    
;                                                                                         
; OPTIONAL INPUTS:                                                                        
;       FITS_FILE -- String scalar or array, list of FITS image files                     
;                                                                                         
; OUTPUTS:                                                                                
;       None.                                                                             
;                                                                                         
; KEYWORD PARAMETERS:                                                                     
;       POINT_STC -- pointing structure 
;                                                                                         
;          Note that values of POINT_STC structure can be returned to the                 
;          caller of IMAGE_TOOL if the MODAL kyeword is set, or it is returned            
;          as a UVALUE of a messenger of a calling widget.                                
;                                                                                         
;       AUTO_PLOT - Keyword used with POINT_STC. When Image Tool (and                     
;                   Pointing Tool) is running and is called again with
;                   a new              
;                   POINT_STC and with AUTO_PLOT set, the                                 
;                   corresponding pointing area(s) will be plotted                        
;                   automatically.                                                        
;                                                                                         
;       START -- Start time of a study, in TAI format; defaults to                        
;                current date and time. Note: If POINT_STC is passed                      
;                in and POINT_STC.DATE_OBS represents a valid TAI,                        
;                START will be overwritten by POINT_STC.DATE_OBS.                         
;       RESET -- If set, all images saved in image stack will be removed                  
;                                                                                         
; RESTRICTIONS:                                                                           
;       Cannot be run two copies simultaneously (guaranteed by the call to                
;       'XREGISTERED')                                                                    
;                                                                                         
; CATEGORY:                                                                               
;       Image processing, science planning                                                
;                                                                                         
; PREVIOUS HISTORY:                                                                       
;       Written August 29, 1994, by Liyun Wang, NASA/GSFC                                 
;                                                                                         
; MODIFICATION HISTORY:                                                                   
;       12-Jan-2006, Zarro(L-3Com/GSFC) - stripped, reworked, and renamed                 
;                                                                                         
; CONTACT:                                                                                
;       dzarro@solar.stanford.edu                                                         
;-                                                                                        
                                                                                          
PRO itool_display, image, MAX=cur_max, MIN=cur_min, relative=relative, $                  
                   _extra=_extra, csi=csi                                                 
   if not exist(image) then return

   noexact = (relative NE 1.0)                                                            
                                                                                          
   exptv, image, MAX=cur_max, MIN=cur_min, relative=relative, $                           
      noexact=noexact, xalign=0.65, yalign=0.8, _extra=_extra                             
                                                                                          
   get_tv_scale, sx, sy, daxis1, daxis2, jx, jy                                           
   rx = FLOAT(sx)/FLOAT(daxis1)                                                           
   ry = FLOAT(sy)/FLOAT(daxis2)                                                           
   temp = {drpix1:jx, drpix2:jy, daxis1:daxis1, daxis2:daxis2, $                          
           ddelt1:rx, ddelt2:ry}                                                          
   copy_struct, temp, csi                                                                 
END                                                                                       
                                                                                          
PRO itool_disp_plus, keep=keep, color=color, alt_csi=alt_csi                              
;---------------------------------------------------------------------------              
;  After calling itool_display, there might be other things like axes,                    
;  grids, etc., needed to be plotted. This routine does just that                         
;---------------------------------------------------------------------------              
@image_tool_com                                                                           
                                             
   IF N_ELEMENTS(alt_csi) EQ 0 THEN alt_csi = csi                                         
   IF alt_csi.flag THEN itool_plot_axes, csi=alt_csi, $                                   
      title=['Solar X', 'Solar Y']                                                        
   widget_control2,set_lat,get_value=temp                                                 
   if is_number(temp(0)) then begin                                                       
    temp_lat=nint(temp(0))                                                                
    if temp_lat ne del_lat then begin                                                     
     del_lat=temp_lat                                                                     
     widget_control2,set_lat,set_value=string(del_lat,'(i3)')                             
    endif                                                                                 
   endif                                                                                  
   widget_control2,set_long,get_value=temp                                                
   if is_number(temp(0)) then begin                                                       
    temp_long=nint(temp(0))                                                               
    if temp_long ne del_long then begin                                                   
     del_long=temp_long                                                                   
     widget_control2,set_long,set_value=string(del_long,'(i3)')                           
    endif                                                                                 
   endif                                                                                  
                                                                                          
   IF N_ELEMENTS(color) EQ 0 THEN BEGIN                                                   
      if alt_csi.flag then begin                                                          
       IF grid THEN itool_solar_grid, del_lat, del_long, date=disp_utc                    
       if noaa then itool_noaa,disp_utc                                                   
      endif                                                                               
      IF img_lock THEN orient_mark, csi=alt_csi                                           
   ENDIF ELSE BEGIN                                                                       
      if alt_csi.flag then begin                                                          
       IF grid THEN $                                                                     
         itool_solar_grid, del_lat, del_long, date=disp_utc, color=color                  
       if noaa then itool_noaa,disp_utc                                                   
      endif                                                                               
      IF img_lock THEN orient_mark, csi=alt_csi, color=color                              
   ENDELSE                                                                                

;-- plot FOV

   itool_plot_fov
   itool_copy_to_pix                                                                      
END                                                                                       

;-------------------------------------------------------------------------
;-- plot FOV, correcting for solar rotation

pro itool_plot_fov

@image_tool_com

if is_struct(fov_stc) then begin
 IF N_ELEMENTS(alt_csi) EQ 0 THEN alt_csi = csi
 offlimb=0b & r_offlimb=0b
 rfov_stc=fov_stc
 infov=eis_limb_fov(fov_stc,offlimb=offlimb)
 if ~exist(time_proj) then time_proj=~infov
 if ~offlimb and time_proj then rfov_stc=eis_drot_fov(fov_stc,tend=alt_csi.date_obs,offlimb=r_offlimb)
 
 widget_control2, point_wid.point_tproj, set_button=time_proj
 if offlimb or r_offlimb then begin
  widget_control2, point_wid.tproj_base,sensitive=0
  widget_control2,comment_id,$
   set_value=['Time projection feature disabled because',$
              'EIS FOV center is off the limb.']
 endif else begin
  widget_control2, point_wid.tproj_base,sensitive=1
  widget_control2,comment_id,set_value=''
 endelse
 temp = cnvt_coord([[rfov_stc.x], [rfov_stc.y]], csi=alt_csi, from=3, to=1)
 PLOTS, temp(*, 0), temp(*, 1), /dev,thick=2,color=!d.table_size-1
endif

return & end

;--------------------------------------------------------------------------               
pro itool_noaa,time   ;-- overlay NOAA info                                               
                                                                                          
common itool_noaa,last_time,last_nar                                                      
err=''                                                                                    
one_day=24.*3600.                                                                         
new_read=1                                                                                
                                                                                          
;-- bail out on bad time inputs                                                           
                                                                                          
dtime=anytim2tai(time,err=err)                                                            
if err ne '' then return                                                                  
                                                                                          
;-- bail out if !path doesn't have RD_NAR                                                 
                                                                                          
if not have_proc('get_nar') then begin                                                    
 xack,['NOAA Active Region coordinates information unavailable ',$                        
        'in current software environment.'],/suppress                                     
 return                                                                                   
endif                                                                                     
                                                                                          
;-- don't re-read if time hasn't changed by more than a 1/2 day                           
                                                                                          
old_read=exist(last_time) and (datatype(last_nar) eq 'STC')                               
if old_read then begin                                                                    
 diff=abs(last_time-dtime)                                                                
 if diff lt one_day/2. then begin                                                         
  new_read=0 & nar=last_nar & count=n_elements(last_nar)                                  
 endif                                                                                    
endif                                                                                     
                                                                                          
;-- read here                                                                             
                                                                                          
if new_read then begin                                                                    
 dprint,'% ITOOL_NOAA: reading NOAA DB'                                                   
 nar=call_function('get_nar',dtime,count=count,/nearest,/unique)                          
 if count gt 0 then begin                                                                 
  last_nar=nar & last_time=dtime                                                          
 endif                                                                                    
endif                                                                                     
                                                                                          
;-- have to rotate pointings to time of image                                             
                                                                                          
noaa_mess='NOAA Active Region coordinates unavailable for current time'                   
if count gt 0 then rnar=call_function('drot_nar',nar,dtime,count=count) else $            
 xack,noaa_mess,/suppress                                                                 
                                                                                          
;-- plot here                                                                             
                                                                                          
if count gt 0 then $                                                                      
 oplot_nar,rnar,charsize=2,font=0,charthick=1.5,color=!d.table_size-1 else $              
  xack,noaa_mess,/suppress                                                                
                                                                                          
return & end                                                                              
                                                                                          
;-------------------------------------------------------------------------                
                                                                                          
PRO itool_refresh, win_id=win_id                                                          
;---------------------------------------------------------------------------              
; Refresh the draw widget                                                                 
;---------------------------------------------------------------------------              
@image_tool_com
              
   if not exist(image_arr) then return                                                             
   if n_elements(win_id) eq 0 then win_id = root_win                                      
   setwindow, win_id
   if log_scaled then begin                                               
;    scl = float(!d.table_size - 1)/alog10(cur_max)                        
;    new_image = scl*alog10((image_arr > 1.0) < cur_max)                
    new_image = alog10(image_arr > 1.0)                
    lcur_max = max(new_image,min=lcur_max)
    itool_display, new_image, max=lcur_max, min=lcur_min, $              
     relative=exptv_rel, csi=csi                                     
   endif else begin
    itool_display,image_arr, max=cur_max, min=cur_min, $              
     relative=exptv_rel, csi=csi                                     
   endelse                                                            
   itool_disp_plus          
 ;  widget_control2, comment_id, set_value=''                                              
   itool_button_refresh                                                                   
   widget_control2, rot_longi_bt, sensitive=1                                             
   widget_control2, rot_solarx_bt, sensitive=1                                            
   widget_control2, rot_1pt_bt, sensitive=1                                               
   return & end
             
;---------------------------------------------------------------------------              
;  Plot image icons in the secondary graphic window                                       
;---------------------------------------------------------------------------              
PRO itool_icon_plot                                                                       

@image_tool_com                                                                           
   setwindow, icon_win                                                                    
   ERASE                                                                                  
   n_icons = N_ELEMENTS(img_icon)                                                         
   IF n_icons NE 0 THEN BEGIN                                                             
      ylow = icon_height-icon_size-1                                                      
      xlow = !x.window(0)+2                                                               
      FOR i=0, n_icons-1 DO BEGIN                                                         
         TV, img_icon(i).data, xlow, ylow                                                 
         ylow = ylow-icon_size-2 > 0                                                      
      ENDFOR                                                                              
   ENDIF                                                                                  
   setwindow, root_win                                                                    
END                                                                                       
                                                                                          
;---------------------------------------------------------------------------              
;  Plot a box around the image icon selected in the secondary graphic window              
;                                                                                         
;  Set REMOVE keyword to just remove the previous mark without plotting                   
;  new ones                                                                               
;---------------------------------------------------------------------------              
PRO itool_mark_icon, id, remove=remove                                                    

@image_tool_com                                                                           
   setwindow, icon_win                                                                    
   DEVICE, get_graphics=old, set_graphics=6                                               
                                                                                          
   IF N_ELEMENTS(id_prev) NE 0 THEN BEGIN                                                 
;---------------------------------------------------------------------------              
;     Remove previous icon mark                                                           
;---------------------------------------------------------------------------              
      PLOTS, px_icon, py_icon, /DEVICE                                                    
   ENDIF                                                                                  
   IF KEYWORD_SET(remove) THEN BEGIN                                                      
      delvarx, id_prev, px_icon, py_icon                                                  
      DEVICE, set_graphics=old                                                            
      RETURN                                                                              
   ENDIF                                                                                  
   xlow = !x.window(0)+1                                                                  
   xhigh = xlow+icon_size+1                                                               
   ylow = icon_height-(icon_size+2)*(id+1)                                                
   yhigh = ylow+icon_size+2                                                               
   px_icon = [xlow, xhigh, xhigh, xlow, xlow]                                             
   py_icon = [ylow, ylow, yhigh, yhigh, ylow]                                             
   PLOTS, px_icon, py_icon, /DEVICE                                                       
   DEVICE, set_graphics=old                                                               
   id_prev = id                                                                           
   setwindow, root_win                                                                    
END                                                                                       
                                                                                          
PRO itool_update_iconbt                                                                   
;---------------------------------------------------------------------------              
;  Update image stack icon buttons                                                        
;---------------------------------------------------------------------------              
@image_tool_com                                                                           
                                                                                          
  widget_control2,old_img_bt,update=0                                                     
   n_stack = N_ELEMENTS(img_icon)                                                         
                                                                                          
do_it=since_version('5.0')                                                                
;do_it=0                                                                                   
                                                                                          
   IF do_it THEN BEGIN                                                                    
;---------------------------------------------------------------------------              
;     Special treatment for IDL 5.0 to prevent the main window from growing               
;---------------------------------------------------------------------------              
      IF N_ELEMENTS(bt4icon) EQ 0 THEN BEGIN                                              
         bt4icon = LONARR(max_stack)                                                      
         str = blank(31)                                                                  
         FOR i=0, max_stack-1 DO BEGIN                                                    
            bt4icon(i) = WIDGET_BUTTON(old_img_bt, font=lfont, value=str)                 
            widget_control2, bt4icon(i), sensitive=0                                      
         ENDFOR                                                                           
      ENDIF                                                                               
      IF n_stack NE 0 THEN BEGIN                                                          
         FOR i=0, n_stack-1 DO BEGIN                                                      
            uvalue = 'OLD_IMG'+STRTRIM(i, 2)                                              
            value = img_icon(i).filename                                                  
            widget_control2, bt4icon(i), set_value=value, set_uvalue=uvalue, $            
               sensitive=1                                                                
         ENDFOR                                                                           
      ENDIF                                                                               
      ndead = max_stack-n_stack                                                           
                                                                                          
      IF ndead GT 0 THEN FOR i=0, ndead-1 DO $                                            
         widget_control2, bt4icon(i+n_stack), set_value='', sensitive=0                   
   ENDIF ELSE BEGIN                                                                       
;---------------------------------------------------------------------------              
;     Remove the previous buttons for old images                                          
;---------------------------------------------------------------------------              
      IF N_ELEMENTS(bt4icon) NE 0 THEN BEGIN                                              
         FOR i=0, N_ELEMENTS(bt4icon)-1 DO xkill, bt4icon(i)                              
      ENDIF                                                                               
                                                                                          
;---------------------------------------------------------------------------              
;     Put buttons for old image names back                                                
;---------------------------------------------------------------------------              
      IF n_stack NE 0 THEN BEGIN                                                          
         bt4icon = LONARR(n_stack)                                                        
         widget_control2, old_img_bt, sensitive=1                                         
         FOR i=0, n_stack-1 DO BEGIN                                                      
            uvalue = 'OLD_IMG'+STRTRIM(i, 2)                                              
            bt4icon(i) = WIDGET_BUTTON(old_img_bt, $                                      
                                       value=img_icon(i).filename, $                      
                                       uvalue=uvalue, font=lfont)                         
         ENDFOR                                                                           
      ENDIF                                                                               
   ENDELSE                                                                                
   widget_control2, old_img_bt, sensitive=(n_stack NE 0)                                  
   widget_control2,old_img_bt,update=1                                                    
                                                                                          
END                                                                                       
                                                                                          
PRO UPDATE_ROT_BUTTON                                                                     
; PURPOSE:                                                                                
;       Updates buttons and value of the rotation widget                                  
;                                                                                         
; EXPLANATION:                                                                            
;       Depending upon the time_gap value that's calculated or                            
;       entered, this routine will set "Forward" and "Backward" button                    
;       right, and updates the value in the text widget.                                  
;                                                                                         
; CALLING SEQUENCE:                                                                       
;       UPDATE_ROT_BUTTON                                                                 
;                                                                                         
@image_tool_com                                                                           
   IF (time_gap LT 0.0) THEN BEGIN                                                        
      rot_dir = -1                                                                        
      widget_control2, rot_mode(1), set_button=1                                          
      widget_control2, rot_mode(0), set_button=0                                          
   ENDIF ELSE BEGIN                                                                       
      rot_dir = 1                                                                         
      widget_control2, rot_mode(0), set_button=1                                          
      widget_control2, rot_mode(1), set_button=0                                          
   ENDELSE                                                                                
   widget_control2, rot_text, set_value=$                                                 
      num2str(ABS(time_gap/rot_unit), FORMAT='(f20.3)')                                   
END                                                                                       
                                                                                          
;----------------------------------------------------------------------                   
;  Main routine begins here                                                               
;----------------------------------------------------------------------                   
PRO EIS_IMAGE_TOOL, input_file, start=start_time, point_stc=point_stc, $                  
                reset=reset, _extra=extra,$
                auto_plot=auto_plot,center=center

@image_tool_com                                                                           
add_psys                                                                                  
fov_stc=eis_fov(center)

;--- convert EIS pointing structure to format recognized by IMAGE_TOOL
               
if have_tag(point_stc,'ras_width') then begin
 fov_stc=eis_fov(point_stc[0].sc_x,point_stc[0].sc_y,date_obs=point_stc[0].date_obs)
 point_stc=eis_itool_stc(point_stc)
 message,'Pointing request for '+point_stc.sci_spec+' passed via command-line',/cont
endif
 
;-- check if EIS FOV is on the limb

;-- Prevent two copies from running at the same time                         

   IF xregistered2('eis_image_tool') ne 0 THEN BEGIN                                      
      IF ~exist(point_stc) THEN BEGIN                                            
         MESSAGE, 'Another EIS_IMAGE_TOOL session seems to be running...', /cont          
         if exist(center) then begin
          itool_refresh
          itool_plot_fov
         endif
         RETURN                                                                           
      ENDIF
      IF NOT match_struct(point_stc, pointing_stc) THEN BEGIN                             
         message,'New pointing stucture identified',/cont
         pointing_stc = point_stc
         pt_fov_reset, pointing_stc, widgets=point_wid                                    
         tai_start = pointing_stc.date_obs                                                
         study_utc = tai2utc(tai_start, /ecs, /trunc)                                     
         widget_control2, start_text, set_value=study_utc
       ENDIF
       itool_refresh
 ;      itool_point_plot
                                                                               
 ;     IF KEYWORD_SET(auto_plot) AND $                                                     
 ;        tools(curr_tool).uvalue EQ 'ptool' THEN BEGIN                                    
 ;        IF auto_plot EQ 2 THEN itool_refresh                                             
 ;        itool_point_plot                                                                 
 ;    ENDIF         
      RETURN                                                                              
   ENDIF                                                                                  
                      
   mk_dfont,bfont=bfont,lfont=lfont                                                       
                                                                                          
;----------------------------------------------------------------------                   
;  by default, no effort of getting the center position of the solar                      
;  disc should be made, i.e., limb fitting is disabled.                                   
;----------------------------------------------------------------------                   

   fit_flag = 0                                                                           
   keep_csr = 0                                                                           
                 
   IF N_ELEMENTS(start_time) EQ 0 THEN BEGIN                                              
;----------------------------------------------------------------------                   
;     Use the current time as default for start_time                                      
;----------------------------------------------------------------------                   
      get_utc, tt, /external                                                              
      tai_start = utc2tai(tt)                                                             
   ENDIF ELSE BEGIN                                                                       
      tai_start = start_time                                                              
   ENDELSE                                                                                
 
;-- resolve pointing

   cando_pointing = 1                                                                  
   IF not is_struct(point_stc) THEN BEGIN                                               
    if not is_struct(pointing_stc) then mk_point_stc, pointing_stc,/fov                                                          
   ENDIF ELSE BEGIN                                                                       
    pointing_stc = point_stc                                                            
   ENDELSE
   IF pointing_stc.date_obs GT 0.d0 THEN tai_start = pointing_stc.date_obs             

;---------------------------------------------------------------------------              
;  Initializing some parameters                                                           
;---------------------------------------------------------------------------              
   delvarx, bt4icon                                                                       
   IF KEYWORD_SET(reset) THEN BEGIN                                                       
    xkill,/all                                                                            
    delvarx,info                                                                          
;    free_pointer,img_stack                                                                
    delvarx, help_stc, img_stack, image_arr, img_icon, $                                  
     prev_col, data_info, rot_unit, limbfit_flag                                          
    delvarx, curr_tool, show_src, log_scaled, boxed_cursor                                
;    free_pointer,img_handle                                                               
;    delvarx, img_handle                                                                   
   ENDIF                                                                                  
                                                                                          
   time_gap = 1.0                                                                         
   rot_dir = 1                                                                            
 ;  time_proj = 1                                                                          
   del_lat = 15                                                                           
   del_long = 15                                                                          
   ut_delay = 1.0                                                                         
   exit_ok = 1                                                                            
   max_stack = 12                                                                         
   icon_size = 50                                                                         
   icon_height = 450                                                                      
   fov_flag = 1                                                                           
   win_xs = 575                                                                           
   win_ys = 575                                                                           
   win_2nd = 354                                                                          
   pt_ok = 0                                                                              
   limbfit_flag = 0                                                                       
   can_zoom = 0                                                                           
   zoom_in = 0                                                                            
   align_flag = 1                                                                         
   pointing_go = 0                                                                        
   clevel = 5                                                                             
   help_mode = 0                                                                          
   synop_set = (GETENV('SYNOP_DATA') NE '')                                               
   summary_set = (GETENV('SUMMARY_DATA') NE '')                                           
   private_set = (GETENV('PRIVATE_DATA') NE '')                                           
                                                                                          
   IF N_ELEMENTS(exptv_rel) EQ 0 THEN exptv_rel = 0.99                                    
   IF N_ELEMENTS(binary_fits) EQ 0 THEN binary_fits = 0                                   
   IF N_ELEMENTS(prev_col) EQ 0 THEN prev_col = 0                                         
   IF N_ELEMENTS(rot_unit) EQ 0 THEN rot_unit = 1.0                                       
   IF N_ELEMENTS(show_src) EQ 0 THEN show_src = 1                                         
   IF N_ELEMENTS(grid) EQ 0 THEN grid = 0                                                 
   IF N_ELEMENTS(noaa) EQ 0 THEN noaa = 0                                                 
   IF N_ELEMENTS(log_scaled) EQ 0 THEN log_scaled = 0                                     
   IF N_ELEMENTS(dtype) EQ 0 THEN BEGIN                                                   
      IF synop_set THEN dtype = 1 ELSE dtype = 0                                          
   ENDIF                                                                                  
   summary = dtype-1                                                                      
                                                                                          
   IF N_ELEMENTS(track_cursor) EQ 0 THEN track_cursor = 1                                 
   IF N_ELEMENTS(boxed_cursor) EQ 0 THEN boxed_cursor = 0                                 
   IF N_ELEMENTS(limbfit_flag) EQ 0 THEN limbfit_flag = 0                                 
   IF N_ELEMENTS(MIN) NE 0 THEN cur_min = MIN                                             
   IF N_ELEMENTS(MAX) NE 0 THEN cur_max = MAX                                             
   IF N_ELEMENTS(src_name) EQ 0 THEN src_name = 'Unspecified'                             
   IF N_ELEMENTS(img_type) EQ 0 THEN img_type = 'Unknown'                                 
   IF N_ELEMENTS(scview) EQ 0 THEN scview = 0                                             
   IF N_ELEMENTS(mdi_view) EQ 0 THEN mdi_view = 0                                         
   IF N_ELEMENTS(curr_tool) EQ 0 THEN BEGIN                                               
      curr_tool = 0                                                                       
      prev_tool = 0                                                                       
   ENDIF                                                                                  
                                                                                          
;----------------------------------------------------------------------                   
;  Default color for drawing circular cursor:                                             
;----------------------------------------------------------------------                   
   l_color = !d.table_size-1                                                              
                                                                                          
 ;  DEVICE, get_screen_size=sz                                                             
 ;  IF (sz(0) GE 1280) AND (sz(1) GE 1024) THEN sz(*) = 0                                  
 ;  sz = sz < [1280, 1024]                                                                 
                                                                                          
   base0 = WIDGET_MBASE(title='', uvalue='UT_UPDATE', $                       
                 /column,_extra=extra, mbar=menu_row)                      
                                                                                          
                                                                                          
;----------------------------------------------------------------------                   
;  Pulldown button "File"                                                                 
;----------------------------------------------------------------------                   
                                                 
   file_bs = WIDGET_BUTTON(menu_row, value='File', /menu, font=bfont)                                         
   synop= widget_button(file_bs,value='Import Images',uvalue='IMPORT')
   img_quit = WIDGET_BUTTON(file_bs, value='Exit',uvalue='QUIT')
                                                                                          
;---------------------------------------------------------------------------              
;  Pull-down menu for tool switches                                                       
;---------------------------------------------------------------------------              
   tools = REPLICATE({uvalue:'', name:'', base:-1L, button:-1L},2)                       
   tools.uvalue = ['ptool','lftool']                                                             
   tools.name = [ 'Pointing Tool','Fitting Tool']
   tools.base = -1L+LONARR(N_ELEMENTS(tools))                                             
                                                                                          
;   tools_bt = WIDGET_BUTTON(menu_row, value='Tools', font=bfont, /menu)                       
                                                                                          
;   FOR i=0, N_ELEMENTS(tools)-1 DO BEGIN                                                  
;      tools(i).button = WIDGET_BUTTON(tools_bt, value=tools(i).name, $                    
;                                   uvalue=tools(i).uvalue)                                
;   ENDFOR                                                                                 
                                                                                                                                                                                   
;----------------------------------------------------------------------                   
;  Pulldown menu for "Options"                                                            
;----------------------------------------------------------------------                   
   opt_bs = WIDGET_BUTTON(menu_row, value='Options', /menu,font=bfont)               
                                                                                          
;----------------------------------------------------------------------                   
;     Submenu for "Image Manipulation"                                                    
;----------------------------------------------------------------------                   
;   log_scale = WIDGET_BUTTON(opt_bs, value='Log Scaling',uvalue='log_scale')              
   img_opt = WIDGET_BUTTON(opt_bs, value='Image Manipulation', /menu)                     
;   lock_bt = WIDGET_BUTTON(img_opt, value='Lock Orientation',uvalue='img_lock')           
;   temp = WIDGET_BUTTON(img_opt, value='Flip N/S', uvalue='flip_img')                     
;   temp = WIDGET_BUTTON(img_opt, value='Reverse W/E', uvalue='rvs_img')                   
;   temp = WIDGET_BUTTON(img_opt, value='Rotate 180'+STRING(176B),uvalue='rotate_img')     
;   temp = WIDGET_BUTTON(img_opt, value='Histogram Equalize',uvalue='hist_img')            
;   temp = WIDGET_BUTTON(img_opt, value='SigRange', uvalue='sig_img')                      
   temp = WIDGET_BUTTON(img_opt, value='Smooth', uvalue='smooth')                         
   temp = WIDGET_BUTTON(img_opt, value='Show Edge', uvalue='sobel')                       
   temp = WIDGET_BUTTON(img_opt, value='Plot Contour', uvalue='contour')                  
   temp = WIDGET_BUTTON(opt_bs, value='Set Minimum Value', uvalue='min_v')                
   temp = WIDGET_BUTTON(opt_bs, value='Set Maximum Value', uvalue='max_v')                
   temp = WIDGET_BUTTON(opt_bs, value='Reset Image Limits',uvalue='reset_limits')         
                                                                                          
;---------------------------------------------------------------------------              
;     Submenu for setting system variables                                                
;---------------------------------------------------------------------------              
   sys_var = WIDGET_BUTTON(opt_bs, value='Set System Variable', /menu)                    
   temp = WIDGET_BUTTON(sys_var, value='!P.Color',uvalue='p_color')                       
   temp = WIDGET_BUTTON(sys_var, value='!P.Backgroud', uvalue='p_bg')                     
   temp = WIDGET_BUTTON(sys_var, value='!P.CharSize', uvalue='p_cs')                      
   temp = WIDGET_BUTTON(sys_var, value='!P.CharThick', uvalue='p_ct')                     
   temp = WIDGET_BUTTON(sys_var, value='!P.TickLen', uvalue='p_tick')                     
   temp = WIDGET_BUTTON(opt_bs, value='Set Contour Level', uvalue='clevel')               
   temp = WIDGET_BUTTON(opt_bs, value='Change Cursor Color',uvalue='cursor_color')        
   cursor_shape = WIDGET_BUTTON(opt_bs, value='Dummy', uvalue='cursor')                   
   cursor_size = WIDGET_BUTTON(opt_bs, value='Set Boxed Cursor Size', $                   
                                  uvalue='cursor_size')                                   
   widget_control2, cursor_size, sensitive=boxed_cursor                                   
   temp = WIDGET_BUTTON(opt_bs, value='Set EXPTV Relative Size', $                        
                           uvalue='exptv')                                                
                                                                                          
;-- operations menu
                                                                                          
   opera_bs = WIDGET_BUTTON(menu_row, value='Operations', /menu, font=bfont)                  
 ;  save_img = WIDGET_BUTTON(opera_bs, value='Make Hard Copy', /menu)                     
 ;  tmp = WIDGET_BUTTON(save_img, value='Save Image in PS Format', $                      
 ;                      uvalue='PS_FORMAT')                                               
 ; tmp = WIDGET_BUTTON(save_img, value='Save Image as a JPEG File', $                     
 ;                     uvalue='save_jpeg')                                                
 ; tmp = WIDGET_BUTTON(save_img, value='Dump Image in PS Format', $                       
 ;                     uvalue='save_ps')                                                  
 ; tmp = WIDGET_BUTTON(save_img, value='Dump Whole Window in PS Format', $                
 ;                     uvalue='win_dump_ps')                                              
 ; tmp1 = WIDGET_BUTTON(save_img, value='Dump Whole Window as a JPEG File', $             
 ;                      uvalue='win_dump_jpeg')                                           
                                                                                          
                                                                                          
   line='-------------------------'                                                       
 ;temp = WIDGET_BUTTON(opera_bs, value='Spawn New Image Window', $                      
 ;                        uvalue='new_window')                                          
   tmp = WIDGET_BUTTON(opera_bs, value='Fancy Magnifier', $                               
                       uvalue='zoom_2') 
 ;  tmp = WIDGET_BUTTON(opera_bs, value='Fit Limb', $                               
 ;                      uvalue='lftool') 
                                                  

 ;  show_csi = WIDGET_BUTTON(opera_bs, value='Show CSI Structure', uvalue=$                
 ;                              'SHOW_CSI')                                                
 ;  temp = WIDGET_BUTTON(opera_bs, value='Show Image Info', uvalue='img_info')             
 ;  fits_header = WIDGET_BUTTON(opera_bs, value='Display FITS Header', $                   
 ;                                 uvalue='HEADER')                                        
 ;  modify_fh = WIDGET_BUTTON(opera_bs, value='Update FITS Header', $                      
 ;                            uvalue='modify_fh')                                          
 ;  write_fits = WIDGET_BUTTON(opera_bs, value='Save Image in FITS Format', $              
 ;                             uvalue='write_fits')                                        
 ;  rm_stack = WIDGET_BUTTON(opera_bs, value='Remove Image from Stack', $                  
 ;                              uvalue='rm_stack')                                         
 ;  tmp = widget_button(opera_bs,value='Flush ALL Images From Stack',uvalue='flush_stack') 
 ;  widget_control2, rm_stack, sensitive=0                                                 
                                                                                          
                                                                                          
;---------------------------------------------------------------------------              
;  Second row has two columns.                                                            
;---------------------------------------------------------------------------              
   base = WIDGET_BASE(base0, /row)                                                        
                                                                                          
;----------------------------------------------------------------------                   
;  Left column is for buttons and info messages, etc.                                     
;----------------------------------------------------------------------                   
   left_column = WIDGET_BASE(base, /column)                                               
                                                                                          
   button_base = WIDGET_BASE(left_column, /column,/frame)                                        
                                                                                          
   grid_base = WIDGET_BASE(button_base, /row)                                     
   temp = WIDGET_BASE(grid_base, /nonexclusive)                                   
   grid_bt = WIDGET_BUTTON(temp, value='Grid', uvalue='GRID', font=lfont)                
                                                                                          
   temp = WIDGET_BASE(grid_base, /row)                                                    
   tem = WIDGET_LABEL(temp, value='Lat.', font=lfont)                                     
   set_lat = WIDGET_TEXT(temp, value=num2str(del_lat, FORMAT='(i3)'), $                   
                         xsize=3, uvalue='del_lat', /editable, font=lfont)                
   tem = WIDGET_LABEL(temp, value=STRING(176B), font=lfont)                               
                                                                                          
   temp = WIDGET_BASE(grid_base, /row)                                                    
   tem = WIDGET_LABEL(temp, value='Long.', font=lfont)                                    
   set_long = WIDGET_TEXT(temp, value=num2str(del_long, FORMAT='(i3)'), $                 
                          xsize=3, uvalue='del_long', /editable, font=lfont)              
   tem = WIDGET_LABEL(temp, value=STRING(176B), font=lfont)                               
                                                                                          
  ;temp = WIDGET_BASE(grid_base, /nonexclusive, /frame)                                   
  ; noaa_bt = WIDGET_BUTTON(temp, value='NOAA', uvalue='NOAA', font=lfont)               

   temp = WIDGET_BASE(grid_base, /nonexclusive)                                   
   log_bt = WIDGET_BUTTON(temp, value='Log Scale', uvalue='log_scale', font=lfont)               
   
                                                                                 
;----------------------------------------------------------------------                   
;  Make "Cursor Position" button a pull-down menu button                                  
;----------------------------------------------------------------------

   junk_base = WIDGET_BASE(button_base, /row)                                             
   tmp = WIDGET_BASE(junk_base, /row)                                    
   csr_bt = WIDGET_BUTTON(tmp, value='Cursor Coords', /menu, font=lfont)     
   temp = WIDGET_BUTTON(csr_bt, value='In Device System', uvalue='mode_1')                
   temp = WIDGET_BUTTON(csr_bt, value='In Image Pixel System', uvalue='mode_2')           
   mode3_bt = WIDGET_BUTTON(csr_bt, value='In Solar Disc System', $                       
                             uvalue='mode_3')                                              
   mode4_bt = WIDGET_BUTTON(csr_bt, value='In Heliographic System', $                     
                             uvalue='mode_4')                                              
   cursor_track = WIDGET_BUTTON(csr_bt, value='Cursor Track', uvalue='cursor_track')      
   txt_id = WIDGET_TEXT(tmp, value='', xsize=25, font=lfont, $                         
                        /edit, uvalue='CURSOR_POS')
   modes=['device','image pixel','solar disc','heliographic']                                                                                                                               
                                                 
 ;  rot_bs = WIDGET_BASE(left_junk, /column)                                       
 ;  junk = WIDGET_BASE(rot_bs, /row)                                                       
 ;  temp = WIDGET_BUTTON(junk, value='Diff. Rotate', /menu, font=lfont)                    
 ;  rot_limb = WIDGET_BUTTON(temp, value='      ', uvalue='rot_limb')                      
 ;  tmp = WIDGET_BUTTON(temp, value='points on central meridian', $                        
 ;                      uvalue='rot_meridian')                                             
 ;  rot_longi_bt = WIDGET_BUTTON(temp, value='points on any longitude', $                  
 ;                               uvalue='rot_longi')                                       
 ;  rot_solarx_bt = WIDGET_BUTTON(temp, value='points on the same Solar-X', $              
 ;                                uvalue='rot_solarx')                                     
 ;  rot_1pt_bt = WIDGET_BUTTON(temp, value='one point', uvalue='rot_1pt')                  
 ;  widget_control2, rot_longi_bt, sensitive=1                                             
 ;  widget_control2, rot_solarx_bt, sensitive=1                                            
 ;  widget_control2, rot_1pt_bt, sensitive=1                                               
 ;  rot_reg_bt = WIDGET_BUTTON(temp, value='a region....', /menu)                          
 ;  tmp1 = WIDGET_BUTTON(rot_reg_bt, value='without remapping pixels', $                   
 ;                       uvalue='rot_reg')                                                 
 ;  tmp1 = WIDGET_BUTTON(rot_reg_bt, value='with pixels remapped', $                       
 ;                       uvalue='rot_regmap')                                              
 ;  tmp = WIDGET_BUTTON(temp, value='the whole image', uvalue='rot_img')                   
                                                                                          
 ;  rot_dir_bs = WIDGET_BASE(junk, /row)                                                   
 ;  temp = WIDGET_LABEL(rot_dir_bs, value='', font=lfont)                                 
   rot_mode = -1+LONARR(2)                                                                   
 ;  xmenu, ['WEST', 'EAST'], rot_dir_bs, /exclusive, $                                     
 ;     font=lfont, uvalue=['forward', 'backward'], /no_release, /row, $                    
 ;     buttons=rot_mode                                                                    
 ;  widget_control2, rot_mode(0), /set_button                                              
 ;  tmp = WIDGET_BASE(rot_bs, /row)                                                        
 ;  temp = WIDGET_LABEL(tmp, value=' ', font=lfont)                                        
 ;  rot_int = WIDGET_BUTTON(tmp, value='Interval', /menu, font=lfont)                      
 ;  temp = WIDGET_BUTTON(rot_int, value='To Current Starting Time', $                      
 ;                       uvalue='rot_now', font=lfont)                                     
                                                                                          
 ;  rot_text = WIDGET_TEXT(tmp, value=num2str(time_gap, FORMAT='(f20.3)'), $               
 ;                         /editable, xsize=6, uvalue='TIME_GAP', font=lfont)              
 ;  rot_unitb = cw_bselector2(tmp, ['Days', 'Hours'], uvalue='ROT_UNIT', $                 
 ;                            /return_index, font=lfont)                                   
 ;  IF rot_unit EQ 1.0 THEN widget_control2, rot_unitb, set_value=0 ELSE $                 
 ;     widget_control2, rot_unitb, set_value=1                                             
                                                                                          
;----------------------------------------------------------------------                   
;  Add start time of a study                                                              
;----------------------------------------------------------------------                   
;   tmp = WIDGET_BASE(junk_base, /row)     
;   temp_tt = WIDGET_BUTTON(junk, value='Obs. Time', /menu, font=lfont)                      
;   tmp_tt = WIDGET_BUTTON(temp_tt, value='Arbitrary Observation Time', $                  
;                          uvalue='any_study')                                             
;   study_start = WIDGET_BUTTON(temp_tt, value='Current Study Start Time', $               
;                               uvalue='study_start')                                      
;   IF N_ELEMENTS(point_stc) EQ 0 THEN $                                                   
;      widget_control2, study_start, sensitive=0                                           
;   start_text = WIDGET_TEXT(tmp, value=anytim2utc(tai_start, /ecs, /trunc), $             
;                            font=lfont, xsize=19, /edit)                                  
            
;-- Plot actions

   junk=widget_base(button_base,/row)
   left=widget_base(junk,/column)
   comment_id=widget_text(left,value='',xsize=40,ysize=5,font=lfont,/frame)              
   right=widget_base(junk,/column)
   tmp = WIDGET_BUTTON(right, value='Refresh',uvalue='REFRESH',font=lfont)
   zoom_bt = WIDGET_BUTTON(right, value='Zoom In',uvalue='zoom_in_out',font=lfont)
   color_bt = WIDGET_BUTTON(right, value='Colors', uvalue='xload',font=lfont)
                                                                                                                                                    
;---------------------------------------------------------------------------              
;  tool_holder is a widget base shared by several widget interface bases. Of              
;  course only one of these bases can be mapped at one time                               
;---------------------------------------------------------------------------              
   tool_holder = WIDGET_BASE(left_column, /frame)                                         
              
;----------------------------------------------------------------------                   
;  Pointing tool                                                                          
;----------------------------------------------------------------------                   
   tools(0).base = eis_itool_ptool(tool_holder, font=lfont,/no_quit)                          
   curr_tool=0                                                                            
   widget_control2, tools(curr_tool).base, map=1                                          

;----------------------------------------------------------------------                   
;  Limb fitter                                                                            
;----------------------------------------------------------------------                   
   tools(1).base = itool_limbfitter(tool_holder, font=lfont)                              
                                                                                          
;----------------------------------------------------------------------                   
;  Mangifier                                                                              
;----------------------------------------------------------------------                   
;  tools(2).base = itool_magnifier(tool_holder)                                           
                                                                                          
;---------------------------------------------------------------------------              
;  Profiler shares the same widget with magnifier                                         
;---------------------------------------------------------------------------              
;  tools(3).base = tools(2).base                                                          
                                                                                          
;---------------------------------------------------------------------------              
;  Image overlayer                                                                        
;---------------------------------------------------------------------------              
;  tools(4).base = itool_overlayer(tool_holder)                                           
                                                                                                                       
;----------------------------------------------------------------------                   
;  Right column is the base widget that holds the draw widget                             
;----------------------------------------------------------------------                   
                                                                                          
   right_column = WIDGET_BASE(base, /frame, /column) 
                                     
;   row33 = WIDGET_BASE(right_column, /row)                                                
                                                                                          
   label = WIDGET_BASE(right_column, /row)                                                
   src_bs = WIDGET_BASE(label, /row)                                              
                                                                                          
;   src_title = cw_bselector2(src_bs, ['IMAGE SOURCE', 'IMAGE TYPE'], $                   
;                             uvalue='src_title', /return_index, font=lfont)              
                                                                                          
   src_title =widget_label(src_bs,value='Image Source',font=lfont)                        
   src_text = WIDGET_TEXT(src_bs, value='Unspecified', xsize=10, $                        
                          font=lfont)                                                     
                                                           
   time_bs = WIDGET_BASE(label, /row)                                             
;   junk = WIDGET_BUTTON(time_bs, value='Image Time', /menu, font=lfont)                   
;   junk1 = WIDGET_BUTTON(junk, value='Arbitrary Image Time', $                            
;                         uvalue='disp_time')                                              
;   junk1 = WIDGET_BUTTON(junk, value='Current Image Time', $                              
;                         uvalue='img_time')                                               
                                       
   junk=widget_label(time_bs,value='Image Time',font=lfont)                                                   
   obs_text = WIDGET_TEXT(time_bs, value='', xsize=20, font=lfont)                        
                              
                                                            
   row31=widget_base(right_column,/row)                                                   
                                                                                          
   pc1=widget_base(row31,/row)                                                         
   draw_id = WIDGET_DRAW(pc1, /frame, ysize=win_ys, xsize=win_xs, $                       
                         uvalue='DRAW', retain=2, $                                       
                         /button_events)                                                  
   WINDOW, /free, /pixmap, xsize=win_xs, ysize=win_ys                                     
   pix_win = {xsize:win_xs, ysize:win_ys, id:!d.window}                                   
                                                                                          
 ;  row22 = WIDGET_BASE(pc1, /row)                                                         
                                                                                                                                                                                   
;---------------------------------------------------------------------------              
;  One more column for image icons                                                        
;---------------------------------------------------------------------------              
;   icon_temp = WIDGET_BASE(row31, /column, /frame, map=0)                                 
                                                                                          
;   old_img_bt = WIDGET_BUTTON(icon_temp, value='ICONS', /menu, font=lfont)                
                                                                                          
;   tmp = WIDGET_BASE(icon_temp, /row)                                                     
;   draw_icon = WIDGET_DRAW(tmp, xsize=icon_size+4, uvalue='draw_icon', $                  
;                           /button_events, ysize=icon_height)                             
                                                                                          
;   itool_update_iconbt                                                                    
                                                                                          
;----------------------------------------------------------------------                   
;  Now make all widgets alive                                                             
;----------------------------------------------------------------------                   
   widget_control2, base0, /realize                                                       
                                                                                          
;---------------------------------------------------------------------------              
;  Remember the messenger ID                                                              
;---------------------------------------------------------------------------              
   messenger = WIDGET_INFO(base0, /child)                                                 
                                                                                          
;---------------------------------------------------------------------------              
;  Disable the selection of the image display device via TVSELECT and                     
;  TVUNSELECT                                                                             
;---------------------------------------------------------------------------              
   tvdevice, /disable                                                                     
                                                                                          
   tool_title = 'EIS_IMAGE_TOOL   (Version 1.0)         '                                 
   get_utc, curr_ut, /ecs                                                              
   widget_control2, base0, tlb_set_title=tool_title+STRMID(curr_ut, 0, 19)+$           
         ' GMT'                                                                           
   widget_control2, base0, timer=DOUBLE(ut_delay)                                      
   widget_control2, draw_id, get_value=root_win, draw_motion=track_cursor                 
   widget_control2, draw_icon, get_value=icon_win                                         
   widget_control2, draw_2nd, get_value=root_2nd                                          
   setwindow, root_win                                                                    
                                                                                          
;   IF N_ELEMENTS(icon_stc)*N_ELEMENTS(image_2nd) NE 0 THEN itool_disp_2nd                 
                                                                                          
;----------------------------------------------------------------------                   
;  Now initialize cursor position display mode, d_mode. It is defined                     
;  as the following:                                                                      
;     d_mode = 1, in pixels of the graphic device with the origin at                      
;                 the lower left corner of the graphic window                             
;     d_mode = 2, in pixels in data coordinate system                                     
;     d_mode = 3, in arcsecs of the solar disc coordinate system                          
;     d_mode = 4, in longitude and latitude of the heliographic system                    
;----------------------------------------------------------------------                   
   IF N_ELEMENTS(d_mode) EQ 0 THEN d_mode = 3 ; default display mode                      
   widget_control2,csr_bt,tooltip='(in '+modes[d_mode-1]+' coordinate system)'                                      

   IF N_ELEMENTS(input_file) GT 0 THEN BEGIN                                              
      IF N_ELEMENTS(input_file) GT 1 THEN BEGIN                                           
;----------------------------------------------------------------------                   
;        Filenames are passed in from the caller. Select one file                         
;----------------------------------------------------------------------                   
         short_names = strip_dirname(input_file, path=dir_path)                           
         data_file = xsel_list(short_names)                                               
         IF data_file EQ '' OR data_file EQ ' ' THEN BEGIN                                
            popup_msg, 'You did not choose any image data.'                               
            data_file = input_file(0)                                                     
         ENDIF                                                                            
         data_file = concat_dir(dir_path(0), data_file)                                   
      ENDIF ELSE data_file = input_file                                                   
;----------------------------------------------------------------------                   
;     What is passed in data_file is only a string scalar, which is the                   
;     filename of the image to be loaded up                                               
;----------------------------------------------------------------------                   
      widget_control2, right_column, map=1                                                
      eis_itool_load_image, data_file(0), err=err                                         
      IF err EQ '' THEN BEGIN                                                             
         widget_control2,src_title,set_value=1-show_src                                   
         src_name = 'Unspecified'                                                         
         IF show_src THEN $                                                               
            widget_control2, src_text, set_value=src_name $                               
         ELSE $                                                                           
            widget_control2, src_text, set_value=img_type                                 
      ENDIF                                                                               
   ENDIF ELSE BEGIN                                                                       
      IF N_ELEMENTS(image_arr) NE 0 AND NOT KEYWORD_SET(reset) THEN BEGIN                 
;---------------------------------------------------------------------------              
;        Restore what was left from the previous run                                      
;---------------------------------------------------------------------------              
         widget_control2, draw_id, map=1                                                  
         tt=anytim2utc(disp_utc, /ecs, /trunc)   
;         doy=' (doy '+trim(string(utc2doy(tt)))+')'                                       
         widget_control2, obs_text, set_value=tt
         itool_refresh                                                                    
;         itool_icon_plot                                                                  
         IF N_ELEMENTS(rgb) NE 0 THEN TVLCT, rgb(*, 0), rgb(*, 1), rgb(*, 2)              
      ENDIF ELSE BEGIN                                                                    
;---------------------------------------------------------------------------              
;        First time to call and no image file name passed in; Load in                     
;        the SOHO logo                                                                    
;---------------------------------------------------------------------------              
         img_lock = 0                                                                     
;         look = loc_file('sohologo.gif', path=get_lib(), count=nf)                       
;         nf=1                                                                             
;         IF nf GT 0 THEN BEGIN                                                            
;            prev_file = 'sohologo'                                                        
;            src_name = ''                                                                 
;            img_type = 'GIF File'                                                         
;            read_gif, prev_file, image_arr, r, g, b                                      
;            image_arr=bytarr(128,128)                                                     
;            cur_min = MIN(image_arr)                                                      
;            cur_max = FIX(MAX(image_arr))                                                 
;            ncolor = !d.table_size-1 < 255                                                
;            r = r(0:ncolor)                                                              
;            g = g(0:ncolor)                                                              
;            b = b(0:ncolor)                                                              
;            IF cur_max LT ncolor THEN BEGIN                                              
;               r(cur_max:ncolor) = 255                                                   
;               g(cur_max:ncolor) = 255                                                   
;               b(cur_max:ncolor) = 255                                                   
;            ENDIF                                                                        
;            TVLCT, r, g, b                                                               
;            rgb = [[r], [g], [b]]                                                        
;            grid = 0                                                                     
;            d_mode = 2             
;            get_utc, img_utc, /ecs                                                        
;            disp_utc = img_utc                                                            
;            header_cur = ''            
;            csi = itool_new_csi()                                                         
;            gif_file = 1                                                                  
;            data_info = {binary:0, label:'', col:1, cur_col:1}                            
;            exptv_rel = 0.95                                                              
;            widget_control2, draw_id, map=1                                               
;            csi.date_obs = img_utc                                                        
;            itool_display, image_arr, MAX=cur_max, MIN=cur_min, $                         
;               relative=exptv_rel, csi=csi                                                
;            itool_copy_to_pix                                                             
;         ENDIF
          gif_file=0                                                                            
          csi = itool_new_csi()                         
          get_utc, img_utc, /ecs                                                        
          disp_utc = img_utc
          csi.date_obs = img_utc                                                        
          flash_msg, comment_id, $                                                         
            ['Please load in your images via:',$
             'File-> Import Images']                      
             num=2                                                                        
      ENDELSE                                                                             
   ENDELSE                                                                                

   itool_adj_ctable, /init                                                                
   itool_button_refresh                                                                   
        
;-- update pointing information
 
   if not valid_time(study_utc) then get_utc,study_utc,/ecs
   if is_struct(pointing_stc) then begin
    pt_fov_reset, pointing_stc, widgets=point_wid                                    
    tai_start = pointing_stc.date_obs                                                
    study_utc = tai2utc(tai_start, /ecs, /trunc)           
   endif
   widget_control2, start_text, set_value=study_utc

   xmanager,"eis_image_tool",base0,cleanup='eis_image_tool_cleanup',$
             /no_block,_extra=extra
                                                      
   dprint,'% out of IMAGE_TOOL'                                                           
END                                                                                       
