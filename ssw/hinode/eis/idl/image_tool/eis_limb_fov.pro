;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_LIMB_FOV
;
; Purpose     : check if solar limb is in EIS FOV
;
; Category    : Planning
;
; Syntax      : IDL> offlimb=eis_limb_fov(fov)
;
; Inputs      : FOV = FOV structure
;
; Outputs     : 1/0 if limb falls within FOV
;
; Keywords    : COFFLIMB = if FOV center lies off the limb
;       
; History     : 4-Sep-2006, Zarro (ADNET/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_limb_fov,fov,offlimb=offlimb

if not is_struct(fov) then return,fov

;-- check if any corner is off the limb.

x=fov.x
y=fov.y

c1=[x[0],y[0]]
c2=[x[1],y[1]]
c3=[x[2],y[2]]
c4=[x[3],y[3]]

solrad=960.
d1=sqrt(c1[0]^2 +c1[1]^2)
d2=sqrt(c2[0]^2 +c2[1]^2)
d3=sqrt(c3[0]^2 +c3[1]^2)
d4=sqrt(c4[0]^2 +c4[1]^2)

infov=(d1 gt solrad) or (d2 gt solrad) or (d3 gt solrad) or (d4 gt solrad)

;-- check if center is off the limb

c5=[(x[0]+x[1])/2., (y[1]+y[2])/2.]
d5=sqrt(c5[0]^2 +c5[1]^2)
offlimb=d5 gt solrad

return,infov

end
