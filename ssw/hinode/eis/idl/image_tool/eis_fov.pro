;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_FOV
;
; Purpose     : Return EIS FOV structure
;
; Category    : Planning
;
; Syntax      : IDL> fov=eis_fov()
;
; Inputs      : XCEN, YCEN = center coordinates (arcsecs) [def = 0,0]
;
; Outputs     : STC = EIS FOV structure
;
; Keywords    : DATE_OBS = time [def = current date/time]
;               FOV_WIDTH = EIS FOV width [arcsec]
;       
; History     : 16-Aug-2006, Zarro (ADNET/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_fov,xcen,ycen,date_obs=date_obs,fov_width=fov_width

pxcen=0. & pycen=0.
if n_elements(xcen) eq 2 then begin
 pxcen=xcen[0] & pycen=xcen[1]
endif else begin
 if is_number(xcen) then pxcen=xcen
 if is_number(ycen) then pycen=ycen
endelse

if (1-valid_time(date_obs)) then get_utc,date_obs,/ecs,/trunc
 
;-- establish EIS FOV, and override with supplied width

get_eis_fov,use_fov_width,fov_height
if exist(fov_width) then if fov_width gt 0. then use_fov_width=fov_width

mx=pxcen-use_fov_width/2.
px=pxcen+use_fov_width/2.
my=pycen-fov_height/2.
py=pycen+fov_height/2.

get_utc,utc

fov_stc={date_obs:date_obs,x:[mx,px,px,mx,mx],y:[my,my,py,py,my]}

return,fov_stc

end





