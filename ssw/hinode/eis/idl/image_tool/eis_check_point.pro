;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_CHECK_POINT
;
; Purpose     : Check if EIS raster pointing is within EIS FOV
;
; Category    : Planning
;
; Syntax      : IDL> valid=eis_check_point(point_stc,index)
;
; Inputs      : POINT_STC = EIS pointing structure
;               INDEX = index of structure (if more than one)
;
; Outputs     : VALID = 1/0 if valid/invalid
;
; Keywords    : TOLERANCE = tolerance in arcsecs to accept fit [def=0]
;       
; History     : 18-Sept-2006, Zarro (ADNET/GSFC) - written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_check_point,point_stc,index,tolerance=tolerance

;-- check inputs

if ~is_struct(point_stc) then return,0b
if ~exist(tolerance) then tolerance=0.
if ~have_tag(point_stc,'xcen') then return,0b

npoints=n_elements(point_stc)
indicies=indgen(npoints)

if is_number(index) then begin
 index= 0 > index < (npoints-1) 
 indicies=index
 npoints=1
endif

out=bytarr(npoints)
for i=0,npoints-1 do begin

 j=indicies[i]

 pointings=point_stc[j]
 width=pointings.ras_width
 height=pointings.ras_height
 x=float(pointings.xcen)
 y=float(pointings.ycen)

 max_x=x+width/2.
 min_x=x-width/2.
 max_y=y+height/2.
 min_y=y-height/2.

;-- determine current FOV limits

 sx=float(pointings.sc_x)
 sy=float(pointings.sc_y)
 sw=pointings.fov_width 
 sh=pointings.fov_height

 mx=sx-sw/2.
 px=sx+sw/2.
 my=sy-sh/2.
 py=sy+sh/2.

;-- now check if raster fits in fov

 d1=(max_x le px) or ((max_x - px) le tolerance)
 d2=(max_y le py) or ((max_y - py) le tolerance)
 d3=(min_x ge mx) or ((mx - min_x) le tolerance)
 d4=(min_y ge my) or ((my - min_y) le tolerance)

 out[i]=d1 and d2 and d3 and d4
endfor

if n_elements(out) eq 1 then out=out[0]

return,out

end






