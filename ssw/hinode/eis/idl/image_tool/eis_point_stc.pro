;+
; Project     : SOLAR-B/EIS
;
; Name        : EIS_POINT_STC
;
; Purpose     : Create an EIS pointing structure
;
; Category    : Planning
;
; Syntax      : IDL> stc=eis_point_stc(nrasters)
;
; Inputs      : NRASTERS = number of rasters
;
; Outputs     : EIS pointing structure with following tag definitions:
;               (arcsec units assumed)
;               ID: widget ID to for planning/image tool communication
;               DATE_OBS: date/time of start of raster (UT)
;               SCI_OBJ: science objective
;               SC_X/SC_Y: spacecraft X/Y pointing
;               FOV_WIDTH = EIS max FOV in X 
;               FOV_HEIGHT = EIS max FOV in Y
;               XCEN/YCEN = raster center pointing
;               COMPENSATION = 1 if spacecraft rotation compensation on
;               RAS_WIDTH/RAS_HEIGHT = raster width/height
;       
; History     : 12-May-2006, Zarro (L-3Com/GSFC) - written
;               6-May-2007, Zarro (ADNET) - added compensation tag
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function eis_point_stc,nrasters

 get_eis_fov,fov_width,fov_height

 point_stc={id:0l,date_obs:'',$
            sci_obj:'EIS Template Pointing',$
            sc_x:0.,sc_y:0.,$
            fov_width:fov_width,fov_height:fov_height,$
            xcen:0.,ycen:0.,compensation:1b,$
            ras_width:200.,ras_height:200.}

 if is_number(nrasters) then if nrasters gt 1 then $
  point_stc=replicate(point_stc,nrasters)

return,point_stc

end

