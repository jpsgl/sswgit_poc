
PRO solb_plot_pointing, filename

;+
; NAME
;
;    SOLB_PLOT_POINTING
;
; PROJECT
;
;    SOLAR-B
;
; EXPLANATION
;
;    Given a Solar-B pointing file, the pointing positions throughout the
;    time period covered by the file are plotted on top of a solar grid,
;    with the grid giving longitude and latitude positions.
;
; INPUTS
;
;    FILENAME  The name of a Solar-B pointing file.
;
; OUTPUTS
;
;    Makes a plot that is sent to the screen.
;
; CALLS
;
;    SOLB_READ_REPOINT, PLOT_HELIO
;
; HISTORY
;
;    Ver.1, 22-Jun-2006, Peter Young
;-



solb_read_repoint,filename,repoint

start_t=repoint.tstart
end_t=repoint.tend

start_tai=anytim(/tai,start_t)
END_tai=anytim(/tai,end_t)

nt=101
t=findgen(nt)/(nt-1)*(end_tai-start_tai) + start_tai

utc=tai2utc(t)
t=anytim(utc,/atime)

plot_helio,repoint.tstart

loadct,3

FOR i=0,nt-1 DO BEGIN
  xy=solb_spacecraft_pointing(filename,t[i])
  plots,xy[0],xy[1],psym=1,symsiz=2.0,col=150
ENDFOR

END
