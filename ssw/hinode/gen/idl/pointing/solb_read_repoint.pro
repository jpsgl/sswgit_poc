
pro solb_read_repoint, fname, repoint

;+
; NAME
;
;    SOLB_READ_REPOINT
;
; PROJECT
;
;    SOLAR-B
;
; EXPLANATION
;
;    Reads the Solar-B re-pointing plan file (text format) into an IDL
;    structure.
;
;    The re-pointing file is prepared by the Chief Planner and distributed
;    to the Chief Observers of each Solar-B instrument.
;
; INPUTS
;
;    FNAME  The name of re-pointing file.
;
; OUTPUTS
;
;    REPOINT  A structure containg the pointing information. The tags are:
;             .timeline  A structure (see below)
;             .curves    A structure (see below)
;             .ncurves   Integer. The number of different sets of tracking
;                        curves contained in the file.
;             .tstart    String. The start time of the time period covered
;                        by the re-pointing file.
;             .tend      String. The end time of the time period covered
;                        by the re-pointing file.
;
;             TIMELINE. This is a structure containing the timeline from the
;             file. It is an array with size of the same number of elements
;             in the timeline. The tags are:
;               .t      String. The time at which the event takes place.
;               .tai    Double. The time in TAI units.
;               .activity String. Describes the activity taking place.
;               .tc     Integer. The tracking curve associated with the change.
;               .lon    Float. The longitude (degrees) of the offset/position
;                       associated with the event.
;               .lat    Float. The latitude (degrees) of the offset/position
;                       associated with the event.
;
;             CURVES. This is a structure containing the sets of tracking
;             curves. It is an array of the same number of elements as the
;             number of sets of tracking curves. The tags are:
;               .tracks  A structure containing the data for each tracking
;                        curve. (See below.)
;               .valid_from The time from which the tracking curve set is
;                        valid. If set to '' then the tracking curve set is
;                        the initial one.
;               .status  If set to 0 then the tracking curve set is the
;                        initial one. Otherwise set to 1.
;
;             TRACKS. This is a structure containing the sets of tracking
;             curves. It is an array of size the number of tracking curves
;             tabulated (usually 4).
;               .n    Integer. The tracking curve number (1-4).
;               .t    String. The reference time for the tracking curve.
;               .tai  Double. the reference time in TAI units.
;               .long Float. The reference longitude (degrees).
;               .lat  Float. The reference latitude (degrees).
;               .x    Float. The heliocentric X-coordinate corresponding to
;                     (long,lat).
;               .y    Float. The heliocentric Y-coordinate corresponding to
;                     (long,lat).
;               .rot_rate Float. The rotation rate in degrees/second.
;
; PROGRAMMING NOTES
;
;    The pointing files consist of two main sections. The first is a timeline
;    identifying when re-pointings or memory uploads take place. The next
;    section consists of tabulations of tracking curve parameters for each
;    memory upload. There is always one more set of tracking curve parameters
;    than there are memory uploads, as there needs to be a previous set
;    corresponding to the end of the previous plan period.
;
;    The present routine has been written to read the three Example files
;    that were distributed to the MODA group on 19-Jun-2006.
; 
; HISTORY
;
;    Ver. 1, 20-Jun-2006, Peter Young
;-

repoint=0

openr,lin,fname,/get_lun

str1=''
readf,lin,str1
IF strmid(str1,0,2) NE '/*' THEN BEGIN
  print,'** error in file format'
  return
ENDIF

readf,lin,str1
tstart=strmid(str1,4)

readf,lin,str1
tend=strmid(str1,4)

readf,lin,str1   ; read comments
readf,lin,str1   ;

;;
;; SET UP STRUCTURE FOR TIMELINE
;;
str={t: '', tai: 0d0, activity: '', tc: -1, xoff:-1., yoff:-1.}
timeline=0

;;
;; READ THE TIMELINE
;;
tst1=0
act=''
time=''
WHILE tst1 EQ 0 DO BEGIN
  readf,lin,str1
  IF strmid(str1,0,2) EQ '/*' THEN BEGIN
    tst1=1
  ENDIF ELSE BEGIN
    act=strmid(str1,0,20)
    str.activity=trim(act)
   ;
    time=strmid(str1,20,20)
    str.t=trim(time)
    str.tai=anytim(time,/tai)
   ;
    tc_str=strmid(str1,40,4)
    IF trim(tc_str) NE '' THEN str.tc=fix(tc_str) ELSE str.tc=-1
   ;
    xoff_str=strmid(str1,44,13)
    yoff_str=strmid(str1,57,9)
    IF trim(xoff_str) NE '' THEN str.xoff=float(xoff_str) ELSE str.xoff=-1.
    IF trim(yoff_str) NE '' THEN str.yoff=float(yoff_str) ELSE str.yoff=-1.
   ;
    IF n_tags(timeline) EQ 0 THEN timeline=str ELSE timeline=[timeline,str]    
  ENDELSE
ENDWHILE

;;
;; SET UP THE STRUCTURE FOR STORING THE TRACKING CURVE DATA
;;   - at most 4 tracking curves are stored onboard
;;
str={n: 0, t: '', tai: 0d0, long: 0., lat: 0., x: 0., y: 0., rot_rate: 0.}
tracks=replicate(str,4)

;;
;; FIND THE NUMBER OF MEMORY UPLOADS (ni)
;;   - number of tracking curves is ni+1
;;
i=where(strmid(timeline.activity,0,15) EQ 'AOCS Mem-Upload',ni)

;;
;; SET UP THE STRUCTURE FOR STORING THE SETS OF TRACKING CURVES
;;
str2={tracks: tracks, valid_from: '', status: 0}
curves=replicate(str2,ni+1)

;;
;; READ THE DIFFERENT SETS OF TRACKING CURVES
;;
i=0
long=0.
lat=0.
rate=0.
count=0
date=''
WHILE eof(lin) NE 1 DO BEGIN
  readf,lin,str1
 ;
 ; -> this identifies a tracking parameter section in file
 ;
  bits=str_sep(strlowcase(str1),'tracking parameters')
 ;
  IF n_elements(bits) GT 1 THEN BEGIN
    tst1=0
    WHILE tst1 EQ 0 DO BEGIN
      readf,lin,str1
     ;
     ; -> the end of the tracking parameters can be flagged in 3 ways
     ;
      IF trim(str1) EQ '' OR strmid(str1,0,6) EQ '/* End' OR eof(lin) EQ 1 THEN BEGIN
        tst1=1
      ENDIF ELSE BEGIN
        bits=str_sep(str1,'Fm:')
        IF n_elements(bits) GT 1 THEN BEGIN
          curves[count].valid_from=trim(bits[1])
          curves[count].status=1
        ENDIF
        IF strlowcase(strmid(str1,0,5)) EQ 'track' THEN BEGIN
          reads,str1,format='(5x,i11,a20,f9.0,f12.0,f15.0)',i, $
               date,long,lat,rate
          curves[count].tracks[i-1].n=i
          curves[count].tracks[i-1].t=trim(date)
          curves[count].tracks[i-1].tai=anytim(trim(date),/tai)
          curves[count].tracks[i-1].long=long
          curves[count].tracks[i-1].lat=lat
          curves[count].tracks[i-1].rot_rate=rate
         ;
          xy=lonlat2xy([long,lat],trim(date))
          curves[count].tracks[i-1].x=xy[0]
          curves[count].tracks[i-1].y=xy[1]
        ENDIF
      ENDELSE
    ENDWHILE
    count=count+1
  ENDIF
ENDWHILE

free_lun,lin

repoint={timeline: timeline, curves: curves, ncurves: ni+1, $
         tstart: tstart, tend: tend}


END
