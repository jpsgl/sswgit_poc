;+
; Project     : SOHO - SUMER
;
; Name        : MK_SUMER_DBASE
;
; Purpose     : Make site database of SUMER FITS file locations
;
; Category    : analysis,planning
;
; Explanation : 
;
; Syntax      : IDL> mk_sumer_dbase,root,dbase,ext=ext,direc=direc
;
; Inputs      : ROOT = root or top directory containing FITS files
;                      (SUMER_FITS_DATA is checked if root is not given)
;
; Opt. Inputs : None
;
; Outputs     : DBASE = array of {file: FITS filename (with path);
;                                 time: FITS obs time (TAI)}
;
; Opt. Outputs: None
;
; Keywords    : EXT = FITS file extensions to search for (def =['.fits',',fts'])
;               DIREC = output list of directories searched under root. 
;                       (can also be input)
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-NOV-1998,  D.M. Zarro (SM&A),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
 

pro mk_sumer_dbase,root,dbase,ext,direc=direc

if datatype(ext) ne 'STR' then ext=['.fits','.fts']
if datatype(root) ne 'STR' then root=chklog('SUMER_FITS_DATA')
if root eq '' then begin
 message,'Need to define FITS directory SUMER_FITS_DATA',/cont
 return
endif

template={file:'',time:0d}
delvarx,dbase
if datatype(direc) eq 'STR' then all_dir=direc else $
 all_dir=find_all_dir('+'+root)
ndir=n_elements(all_dir)
next=n_elements(ext)
for i=0,ndir-1 do begin
 direc=all_dir(i)
 message,'searching '+direc,/cont
 for k=0,next-1 do begin
  list=loc_file(concat_dir(all_dir(i),'*'+ext(k)),count=count)
  if count gt 0 then begin
   times=file2time(list,out='tai')
   temp=replicate(template,count)
   temp.file=list
   temp.time=times
   dbase=merge_struct(dbase,temp)
  endif
 endfor
endfor

direc=all_dir
return & end


