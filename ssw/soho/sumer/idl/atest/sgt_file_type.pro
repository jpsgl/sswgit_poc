
;+
; NAME:
;
; sgt_file_type
;
; PURPOSE:
;
; Determines if a file is a fits file or a genx file.
;
; CATEGORY:
;
; Data handling.
;
; CALLING SEQUENCE:
;
; file_type = sgt_file_type(filename)
; 
; INPUTS:
;
; The file name.
;
; OPTIONAL INPUTS:
;
; None.
;       
; KEYWORD PARAMETERS:
;
; sumer: Returns SUMERGENX if the file is a genx file written with
;        wrt_sumer and YOHOGENX if the file was written with wrt_genx.
;
; OUTPUTS:
;
; A string which contains 'FITS', 'GENX', 'SUMERGENX', 'YOHKOHGENX',
; or 'UNKNOWN'.
;
; OPTIONAL OUTPUTS:
;
; None.
;
; COMMON BLOCKS:
;
; None.
;
; SIDE EFFECTS:
;
; None?
;
; RESTRICTIONS:
;
; Only identifies FITS files or files written with wrt_genx or
; wrt_sumer. Note that non-genx files that happen to have the first 4
; bytes equal to 0 will erronously be indentified as genx files. 
;
; PROCEDURE:
;
; The obvious: Reads the fits few bytes of the file and looks for the
;              FITS keyword SIMPLE. Alternatively, the version number
;              of the genx file is checked.
;
; EXAMPLE:
;
; IDL> file = 'MyFitsFile.file'
; IDL> print,sgt_file_type(file)
;      FITS
; IDL> file = 'MyGenxFile.file'
; IDL> print,sgt_file_type(file)
;      GENX
; IDL> file = 'MyUnknownFile.file'
; IDL> print,sgt_file_type(file)
;      UNKNOWN
;
; MODIFICATION HISTORY:
;
;   HPW 31-OCT-1996: Original version.
;
;   HPW 17-JUL-1997: Modified to identify new version of sumer genx
;                    file (version 998).
;
;   HPW 18-JUL-1997: Added SUMER keyword to allow the differentiation
;                    of SUMER genx and YOHKOH genx files.
;
;   DMZ 27-JAN-1999: Added /XDR open for GENX files
;-

function sgt_file_type,filename,sumer=sumer

n_files   = n_elements(filename) 
file_type = strarr(n_files)

for n=0,n_files-1 do begin

  ;; Look for SIMPLE keyword
  openr,unit,filename(n),/get_lun

  t = bytarr(6)
  readu,unit,t
  t = string(t)
  
  free_lun,unit
  
  if (t eq 'SIMPLE') then file_type(n) = 'FITS' else begin

    ;; Look for long integer of 0,1,2 (YOHKOH) or 999 or 998 (SUMER)
    openr,unit,filename(n),/get_lun,/xdr
    v  = 0L
    vs = [0,1,2,999,998]
    readu,unit,v
    free_lun,unit   
    match = where((v-vs) eq 0,count)

    if (count gt 0) then begin
      if keyword_set(SUMER) then begin
        if v gt 2 then file_type(n)='SUMERGENX' else file_type(n)="YOHKOHGENX"
      endif else begin
        file_type(n) = 'GENX' 
      endelse
    endif else file_type(n) = 'UNKNOWN'

  endelse

endfor

if n_files eq 1 then file_type = file_type(0)
    
return,file_type
end
