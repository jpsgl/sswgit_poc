
pro sumer_search_DISPLAY_RESULT
@sumer_search_block 
    count= n_elements(files)
select=replicate(0, count)
if files(0) ne '' then  begin 

      widget_control, text_id, set_value= $
     [ 'Total '+string(count, format='(i4)')+' files have been found.', $
        'Click on the list to select a file ', $
         'and double-click on it to cancel the selection.',  ' ', $
          '      Filename       Xcen Ycen Det/Slit'$
          +' Exp Wlmin Wlmax  Obs_seq'] 

    catalog = ' '+string(files, format='(a24)')+ $
          ''+string(logs.xcen, format='(i5)')+ ''+$
          string(logs.ycen,format='(i5)')+ $
         '   '+logs.detector+'/'+$
        logs.slit+'    '+string(logs.exptime, format='(i4)')+$
       '  '+ string(logs.wavemin, format='(i4)') $
       +'  ' + string(logs.wavemax, format='(i4)')+ $
     '    '+ string(logs.obs_seq, format='(a6)')+ ' '+logs.study_nm
 endif else begin
          widget_control, text_id, set_value=['','']
          widget_control, text_id, set_value= $
            'No '+ $
           ' files are selected.'
endelse
  
         widget_control, list_id, set_value=catalog
  
end
;+   
;
;                                        
;   NAME:
;     SUMER_SEARCH_PD_EVENT
;   PURPOSE:
;     Responses to pull-down events in SUMER_SEARCH      
;   CALLING SEQUENCE:
;     SUMER_SEARCH_PD_EVENT, event
;   INPUTS:
;     EVENT = a structure variable for an event 
;   OUTPUTS:
;     None
;   COMMON BLOCKS:
;     SUMER_SEARCH_BLOCK
;   SIDE EFFECTS:
;     Change parameters defined in common blocks 
;   REQUIRED NON-STADNARD SUBROUTINES:
;     XRASTER           MK_SUMER_CAT  LOCATE_DIR
;     HEADFITS          BREAK_PATH
;     SLOG_RDLOG   SLOG_GET_DAY  SLOG_GET_TIME
;   RESTRICTIONS :
;     SUMER log files should exist in the specified directory   
;   MODIFICATION HISTORY
;     March 1997    Jongchul Chae   
;-

PRO sumer_search_pd_Event, Event
@sumer_search_block

  CASE Event.Value OF 


  'Done': BEGIN
    done=1
    widget_control, event.top, /destroy
    END
   'Config.Display/Change':begin
     sumer_config    
     end
    'Config.Default':begin
      sumer_config_def
     end
    'Make Catalog' :begin
    
    ;widget_control, /hourglass
    ;catfile=pickfile(/write)
     ;mk_sumer_cat, sumer_dir+'/'+files, catfile
    end
   'Refresh': Begin
    select=select*0
    
    widget_control, text_id, set_value=''
    end
  
    'Data.Show Primary Header':begin
    if file eq '' then begin
         widget_control, text_id, set_value=['', '']
         widget_control, text_id, $
         set_value = ['You have not selected any file.', $
                  'First select a file from the list.']
    return
    endif
      widget_control, /hourglass
      fullfile=sumer_file(file,status=exist)
      if exist then begin
       xdisplayfile, text=headfits(fullfile), $
       title='Primary Header of '+file, group=event.top 
       return
      endif else  widget_control, text_id, set_value=$
      ['The file: '+file+ '  is not accessible.', $
        'Try the the option: Get Data']
   end
  'Data.Show Binary Header':begin
        if file eq '' then begin
        widget_control, text_id, set_value=['', '']
        widget_control, text_id, $
         set_value = ['You have not selected any file.', $
                  'First select a file from the list.']
        return
         endif
        widget_control, /hourglass
        fullfile=sumer_file(file,status=exist)
       if exist then xdisplayfile, text=headfits(fullfile, ext=1), $
       title='Binary Header of '+file, group=event.top $
     else widget_control, text_id, set_value=$
      ['The file: '+file+ '  is not accessible.', $
        'Try the the option: Get Data']
 
    end
      'Select All':begin
            select=select*0+1
            widget_control, text_id, set_value= $
            ['All the files in the list are selected.']
              end
       'De-select All': begin
             select=select*0
             widget_control, text_id, set_value= $
            ['No file in the list is selected.']

             end
     'Data.Get Data': begin
         widget_control, /hourglass
         ff= files(where(select eq 1, nf))
        if nf lt 1 then begin
          widget_control, text_id, set_value=['', '']

          widget_control, text_id, $
          set_value = ['You have not selected any file.', $
                  'First select a file from the list.']
         return
        endif
       
   
      widget_control, /hourglass
     text=replicate('', nf+2)
      for k=0, nf-1 do begin
      fullfile=sumer_file(ff(k),status=exist, /copy)
     
     
      if exist eq 1 then begin
      break_file, fullfile, disk_log, dir, filnam, ext
      text(k+2)='The file: '+filnam+ext +' is now on ' +disk_log+dir 
      endif else  text(k+2)='The file: '+ff(k) +' is not accessible.'
      widget_control, text_id, set_value=text
     
      endfor   
     end
    'Data.Look at Data' : begin
       ff= files(where(select eq 1, nf))
        if nf lt 1 then begin
          widget_control, text_id, set_value=['', '']

          widget_control, text_id, $
          set_value = ['You have not selected any file.', $
                  'First select a file from the list.']
         return
        endif
       
   
      widget_control, /hourglass
      for k=0, nf-1 do begin
      fullfile=sumer_file(ff(k),status=exist)
      if not exist then  begin
         widget_control, text_id, $
          set_value =  $
           ['The file: '+ff(k)+ '  is not available.', $
        'Try the the option: Get Data']
         return
      endif
      if n_elements(fullfiles) eq 0 then $
          fullfiles = fullfile else fullfiles =[fullfiles, fullfile]
      endfor
       sumer_tool, fullfiles, /reformat
      end


  'Search': BEGIN
   
       files=''
       catalog='' 
       nlogs=0
       widget_control, /hourglass
;  Time Manipulation

       if strlen(time1(0))  ne 13 then begin
       widget_control, text_id, set_value=['',''] 
       widget_control, text_id, set_value=['Error in  start time input.', $
             'Make the string have 13 characters.']
       return
       endif
   if strlen(time2(0))  ne 13 then begin
       widget_control, text_id, set_value=['',''] 
       widget_control, text_id, set_value=['Error in  end time input.', $
             'Make the string have 13 characters.']
       return
       endif


       y1 = fix(strmid(time1, 0,2)) 
       year1 = y1 + 1900+100*(y1 le 80)
       month1 = fix(strmid(time1, 2,2)) 
       day1   = fix(strmid(time1, 4,2))
       t1     = julday(month1, day1, year1 ) $
                -julday(1,1,1996,0,0,0) $
               +(float(strmid(time1, 7,2))+float(strmid(time1, 9, 2))/60. $
               +float(strmid(time1, 11, 2))/3600.)/24


       y2 = fix(strmid(time2, 0,2)) 
       year2 = y2 + 1900+100*(y2 le 80)
       month2 = fix(strmid(time2, 2,2)) 
       day2   = fix(strmid(time2, 4,2))
       t2     = julday(month2, day2, year2)-julday(1,1,1996) $
               +(float(strmid(time2, 7,2))+float(strmid(time2, 9, 2))/60. $
               +float(strmid(time2, 11, 2))/3600.)/24

        
       if t1 gt t2 then begin
          widget_control, text_id, set_value=['', '']
          widget_control, text_id, set_value=$
            ['The start time should be earlier than the end time.']
           return
        endif


    ;
; Determination of log files
;
       y=year1 & month=month1
        yy = string(y mod 100, format='(i2)')
        mm = string(month/10, format='(i1)')+ $
             string(month mod 10, format='(i1)')
       logfile = yy+mm
       while y lt year2 or month lt month2 do  begin 
         if month lt 12 then month=month+1 else begin
         y=y+1
         month=1
 
         endelse
        yy = string(y mod 100, format='(i2)')
        mm = string(month/10, format='(i1)')+ $
             string(month mod 10, format='(i1)')

         logfile=[logfile, $
         yy+ mm]      
       endwhile

       logfile = logfile+'.log'
       nlog = n_elements(logfile)
  
; Repeat search
 
     for ff=0, nlog-1 do begin
       tmp = concat_dir(log_dir,logfile(ff))
      
       if not file_exist(tmp) then begin
          widget_control, text_id, set_value=['','']
          widget_control, text_id, set_value= $
            'The log file: '+tmp+' does not exist!'
           goto, next
       endif
       
       slog_rdlog, tmp, logstr
       logstr.filename=strlowcase(logstr.filename)
       time=logstr.dstr
       y = fix(strmid(time, 0,2)) 
       year = y + 1900+100*(y le 80)
       month = fix(strmid(time, 2,2)) 
       day   = fix(strmid(time, 4,2))
       t= replicate(-julday(1,1,1996),n_elements(time))
       for k=0, n_elements(t)-1 do $
       t(k) = julday(month(k), day(k), year(k)) +t(k) 
       t =t+(float(strmid(time, 7,2))+float(strmid(time, 9, 2))/60. $
         +float(strmid(time, 11, 2))/3600.)/24

 ; Detemine the order of minimum and maximum wavelengths      

       ordermin = 1+ $
     (logstr.detector eq 'A') $
      *((logstr.wavemin lt 0.6*logstr.wavemax) + $
        (logstr.wavemin ge 0.6*logstr.wavemax)*(logstr.wavemin le 780.)) + $
      (logstr.detector eq 'B') $
      *((logstr.wavemin lt 0.6*logstr.wavemax) + $
        (logstr.wavemin ge 0.6*logstr.wavemax)*(logstr.wavemin le 660.))
      
      ordermax = 1+ $
     (logstr.detector eq 'A') $
      *((logstr.wavemin lt 0.6*logstr.wavemax)*0 + $
        (logstr.wavemin ge 0.6*logstr.wavemax)* $
          ((logstr.wavemax le 780.)+ $
           (logstr.wavemax gt 780.)*(ordermin eq 2))) + $
      (logstr.detector eq 'B') $
      *((logstr.wavemin lt 0.6*logstr.wavemax)*0 + $
        (logstr.wavemin ge 0.6*logstr.wavemax)*$
         ((logstr.wavemax le 660.)+ $
           (logstr.wavemax gt 660.)*(ordermin eq 2)))   
 
;  Determine the distance from  the diskcenter
   
       rcen = sqrt(float(logstr.xcen)^2 + float(logstr.ycen)^2)   

;  Create selection array based on the requisite cirteria

     condition =   $
     strpos(strlowcase(logstr.filename), strlowcase(fits_kind), 0) ne -1
     
     condition = condition and $
      (t ge t1) and (t le t2) 
      
     condition = condition  $
     and  (logstr.xcen ge xcen1) and (logstr.xcen le xcen2) $
     and  (logstr.ycen ge ycen1) and (logstr.ycen le ycen2) $
     and  (rcen ge rcen1) and (rcen le rcen2)  
     
     condition = condition $
     and    (logstr.wavemin*ordermin ge wl1) $
     and     (logstr.wavemax*ordermax le wl2) 
    
     condition = condition $
     and  (logstr.exptime ge exposure1) $
     and (logstr.exptime le exposure2)
     
; Add the optional criteria if specified
   print, 'seq_type+', seq_type
   if  detector ne '' then  condition=condition $
              and  (logstr.detector eq detector) 
   if object  ne '' then condition = condition and $
         strpos(strlowcase(logstr.sci_obj) ,object,0)  ne -1 
   if pop ne '' then condition = condition and $
        (fix(logstr.popudp) eq fix(pop))
   if seq_type  ne '' then condition = condition and $
         strpos(strlowcase(logstr.obs_seq),seq_type,0)  ne -1        
   if  slit ne '' then  condition=condition and  (logstr.slit eq slit) 

   if  scientist ne '' then  condition = condition and  $
         strpos(strlowcase(logstr.scientis), scientist, 0) ne -1
   if  studyname ne '' then  condition = condition and  $
        strpos(strlowcase(logstr.study_nm), studyname, 0) ne -1
   
   
   ss=where(condition, count)
   widget_control, text_id, set_value=$
     [string(count, format='(i5)')+' files are selected from '+logfile(ff)]
   if count ge 1 then begin
      
     if nlogs eq 0 then  logs=logstr(ss) $
     else logs=[logs, logstr(ss)] 
     nlogs=nlogs+count
    
     files = logs.filename
  
  endif
 next:
     endfor 
   select=replicate(0, n_elements(logs))

;  Display results 
    sumer_search_display_result
             
   
      
    END
'Sort.by Study Name': begin
   select=replicate(0, n_elements(logs))

     if n_elements(logs) eq 1 then return
     ss=sort(strlowcase(logs.study_nm))
     logs=logs(ss)
     files=logs.filename 
     sumer_search_display_result
     end
'Sort.by Wavelength': begin
  ;   select=replicate(0, n_elements(logs))

      if n_elements(logs) eq 1 then return
    ordermin = 1+ $
     (logs.detector eq 'A') $
      *((logs.wavemin lt 0.6*logs.wavemax) + $
        (logs.wavemin ge 0.6*logs.wavemax)*(logs.wavemin le 780.)) + $
      (logs.detector eq 'B') $
      *((logs.wavemin lt 0.6*logs.wavemax) + $
        (logs.wavemin ge 0.6*logs.wavemax)*(logs.wavemin le 660.))
      ss =sort(ordermin*logs.wavemin)
      logs=logs(ss)
      files=logs.filename
  
      sumer_search_display_result
      end
'Sort.by Time': begin
  ; select=replicate(0, n_elements(logs))

      if n_elements(logs) eq 1 then return
       time=logs.dstr
       y = fix(strmid(time, 0,2)) 
       year = y + 1900+100*(y le 80)
       month = fix(strmid(time, 2,2)) 
       day   = fix(strmid(time, 4,2))
       t= replicate(-julday(1,1,1996),n_elements(time))
       for k=0, n_elements(t)-1 do $
       t(k) = julday(month(k), day(k), year(k)) +t(k) 
       t =t+(float(strmid(time, 7,2))+float(strmid(time, 9, 2))/60. $
         +float(strmid(time, 11, 2))/3600.)/24
       ss=sort(time)
       logs=logs(ss)
       files=logs.filename
       sumer_search_display_result
       end


  ENDCASE
END

