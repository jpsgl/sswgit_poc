;+
;
; NAME : 
;        SUMER_TOOL
; PURPOSE:
;        Widget-based display of SUMER fits data
; CALLIING SEQUENCE:
;         SUMER_TOOL, files
; INPUTS :
;        None
; OPTIONAL INPUT:
;        FILES       SUMER fits file names to be displayed
;
; KEYWORDS :
;        EXTEN       The extension nember of a FITS table in the file
;                    to be read(optional input \  default=1).
;        GROUP       Widge ID of group leader (optional input)
;        REFORMAT    If set, the SUMER data are reformatted.          
; COMMON BLOCKS:
;        
; REQUIRED ROUTINES:
;        RDSUMEREXT  XRAS_MAIN_EVENT  SUMER_FULL_FILENAME
;        GET_COLOR   DISPLAY_IMA 
; RESTRICTIONS :
;        Only one call is allowed at the same time.
; MODIFICATION HISTORY
;        March 1997        Jongchul Chae
;        Novemebr 1998     JC      Change Name
;   
;-
PRO SUMER_TOOL, files1, exten=exten, GROUP=Group, reformat=reformat
common sumer_tool_block, xras_str, dstr
@monocolors_block

defsysv, '!sumer_config', exist=exist
if not exist then sumer_config_def
nf = n_elements(files1)

if nf ge 1 then begin
files=files1

 for k=0, nf-1 do begin
files(k) = sumer_file(files1(k), status=exist) 
if exist ne 1 then begin    
    tmp = widget_message(/error, 'The SUMER file: '+files1(k) $
         +' is not available!')
return
endif
endfor
endif   
flag=1
if nf gt 1 or keyword_set(reformat) then dstr =sumer_serial(files,  flag=flag, reformat=reformat)
if flag eq -1 then return
if nf eq 1 and not keyword_set(reformat) then dstr =sumer_fits(files(0))
if n_elements(dstr) eq 0 then return
if nf eq 0 then resume=1 else resume=0



font = '!6'
if n_elements(exten) eq 0 then exten=1
extension=exten
xbin=1
ybin=1

device, set_character_size=[8, 12]
image_kind=dstr.data_names
image_par=['intensity','shift','width', 'continuum']
right_button =['R: x  profile','R: y profile/spectrum', $
               'R: spectral profile', 'R: Spectrum(band)']
left_button=['L: img value']
middle_button=['Expand  x', ' Shrink x', 'Expand y', $
'Shrink  y', 'Expand wl', 'Shrink wl']  
ffonoff = strtrim(string(fxpar(dstr.h, 'FFONOFF')), 2)

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  XRAS_MAIN = WIDGET_BASE(GROUP_LEADER=Group, $
      ROW=15, $
      MAP=1, $
      TITLE='SUMER Tool (v3.0, by J. Chae)' , $
      UVALUE='XRAS_MAIN')

  BASE2 = WIDGET_BASE(XRAS_MAIN, $
      COLUMN=2, $
      MAP=1, $
      UVALUE='BASE2')

   
  MenuDesc3406 = [ $
      { CW_PDMENU_S,       0, 'Done' }, $ ;
      { CW_PDMENU_S,       0, 'IDL Prompt'}, $ 
      { CW_PDMENU_S,      1, 'Config' }, $ ;
      { CW_PDMENU_S,        0, 'Default'},  $
      { CW_PDMENU_S,        2, 'Display/Change'}, $
      { CW_PDMENU_S,       1, 'File'},  $
         { CW_PDMENU_S,       1,  'Header'}, $
             {CW_PDMENU_S,      0, 'Primary'}, $
             {CW_PDMENU_S,      2, 'Binary'}, $
        { CW_PDMENU_S,      0, 'Save Raster Image(s)'}, $
        { CW_PDMENU_S,      2, 'Save Spectrogram(s)'}, $
      { CW_PDMENU_S,       1,   'Data Handle'}, $
           {  CW_PDMENU_S,       1, 'Saturation'}, $
              {CW_PDMENU_S,         2, 'Correct'},$
           {  CW_PDMENU_S,       1, 'Flat Field'}, $
              {CW_PDMENU_S,         0, 'Correct'},$
              {CW_PDMENU_S,         2, 'Decorrect'},$
           {  CW_PDMENU_S,       1, 'Correct Distortion'}, $
              {CW_PDMENU_S,         0,'Both Directions'}, $
              {CW_PDMENU_S,         0,'X Direction Only'}, $
              {CW_PDMENU_S,         2,'Y Direction Only'}, $
           {  CW_PDMENU_S,       0, 'Fill Missing Data'}, $ 
           {  CW_PDMENU_S,       1, 'Smooth'}, $ 
              {CW_PDMENU_S,         0, 'X Direction'}, $
              {CW_PDMENU_S,         2, 'Y Direction'}, $             
           {  CW_PDMENU_S,       1, 'Coordinates'}, $
              { CW_PDMENU_S,         1, 'Units'}, $
                { CW_PDMENU_S,         0, 'Image Pixels'}, $
                { CW_PDMENU_S,         0, 'Detector Pixels'}, $
                { CW_PDMENU_S,         2, 'Physical Units'}, $
              { CW_PDMENU_S,        3, 'Magnification'},$
                { CW_PDMENU_S,         0, 'X'}, $
                { CW_PDMENU_S,         0, 'Y'}, $
                { CW_PDMENU_S,         2, 'W'}, $
           { CW_PDMENU_S,        0, 'Gray Scale Min & Max'}, $
           {  CW_PDMENU_S,       1, 'Reg. of Interest'}, $
              { CW_PDMENU_S,         0, 'Spatial Region'}, $
              { CW_PDMENU_S,         2, 'Spectral Lines'}, $
           { CW_PDMENU_S,        1,'Rotate'}, $
              {CW_PDMENU_S,          0, 'Flip N/S'}, $
              {CW_PDMENU_S,          2, 'Flip E/W'}, $            
           { CW_PDMENU_S,        0, 'Gaussian Fit'}, $
           { CW_PDMENU_S,        0, 'Band Filtering'}, $
          {  CW_PDMENU_S,       2, 'Mirror Scan Correction'}, $
      { CW_PDMENU_S,       0, 'Color' },$  ;
        ;   {   CW_PDMENU_S,   0, 'Standard Table'}, $
        ;   {   CW_PDMENU_S,   2, 'Add Mono Colors'}, $ 
      { CW_PDMENU_S,       1, 'Font'}, $
                { CW_PDMENU_S,    0, 'Select'}, $
                { CW_PDMENU_S,    0, 'Magnify'}, $
                { CW_PDMENU_S,    2, 'Reduce'}, $ 
      { CW_PDMENU_S,        3, 'Hardcopy'}, $
          {    CW_PDMENU_S,   0, 'Scan Image'}, $
          {    CW_PDMENU_S,   0, 'Spectrogram'}, $
          {    CW_PDMENU_S,   2, 'Spectral Profile'} $    
       ]



  XRAS_PD = CW_PDMENU( BASE2, MenuDesc3406, /RETURN_FULL_NAME, $
      UVALUE='XRAS_PD')

 TextVal328 = [ $
    '' ]
  TEXT_A = WIDGET_TEXT( XRAS_MAIN,VALUE=TextVal328, $
      UVALUE='TEXT_A', $
      XSIZE=50, $
      YSIZE=1)
 base52 = widget_base(XRAS_MAIN, col=3, $ 
      MAP=1, uvalue='BASE52', frame=1)
     
 FIELD_X = cw_field(base52, value='', $
      title='X :  ',   xsize=10, $
      string=1, noedit=1,  uvalue='FIELD_X') 
 FIELD_Y = cw_field(base52, value='', $
      title='Y :  ', xsize=10,$
      string=1, noedit=1,  $
      all_events=1, uvalue='FIELD_Y')     
  FIELD_VAL = cw_field(base52, value='', $
      title='VALUE : ', $
      string=1, xsize=20, noedit=1,$
      all_events=1,uvalue='FIELD_VAL')


  BASE16 = WIDGET_BASE(XRAS_MAIN, $
      col=2, $
      MAP=1, $
      UVALUE='BASE16')

  

  BASE34A = WIDGET_BASE(BASE16, $
      ROW=2, $
      MAP=1, $
      UVALUE='BASE34A')

  BASE34Aa= WIDGET_BASE(BASE34A, $
      col=2,  $
      MAP=1, $
      UVALUE='BASE34Aa')
  DROP_IMA_COL=widget_droplist(base34Aa, $
      uvalue='DROP_IMA_COL', $
      value=image_kind)
  DROP_IMA_PAR=widget_droplist(base34Aa, $
      uvalue='DROP_IMA_PAR', $
      value=image_par)  
 
 
  DRAW_IMA = WIDGET_DRAW( BASE34A, $
      BUTTON_EVENTS=1, scroll=1,x_scroll_size=300,  $
       y_scroll_size=400, $
      FRAME=1, $
      RETAIN=2, $
      UVALUE='DRAW_IMA', $
      XSIZE=600, $
      YSIZE=800)

  BASE34B = WIDGET_BASE(BASE16, $
      ROW=2, $
      MAP=1, $
      UVALUE='BASE34B')

  BASE34Ba= WIDGET_BASE(BASE34B, $
      row=1, $
      MAP=1, $
      UVALUE='BASE34Ba')
  DROP_Y_COL=widget_droplist(base34Ba, $
      uvalue='DROP_Y_COL', $
      value=image_kind)
 
  DRAW_Y = WIDGET_DRAW( BASE34B, $
      scroll=1,x_scroll_size=200,  $
       y_scroll_size=400, $
      FRAME=1, $
      RETAIN=2, $
      UVALUE='DRAW_IMA', $
      XSIZE=600, $
      YSIZE=800)



  BASE58 = WIDGET_BASE(XRAS_MAIN, $
      col=2, $
      MAP=1, $
      UVALUE='BASE58')


  DRAW_W = WIDGET_DRAW( BASE58, $
      FRAME=1, $
      scroll=1,x_scroll_size=430, y_scroll_size=250, $
      RETAIN=1, $
      UVALUE='DRAW_W', $
      XSIZE=1024+50, $
      YSIZE=360+50)
  BASE58A= WIDGET_BASE(BASE58, $
      row=2, $
      MAP=1, $
      UVALUE='BASE50A')
  DROP_W_COL=widget_droplist(base58A, $
      uvalue='DROP_W_COL', $
      value=image_kind)
   fit_menu=[$
    {CW_PDMENU_S,  3, 'LSQ Fit' }, $
      {  CW_PDMENU_S,  1, 'Single Point'}, $
         {CW_PDMENU_S, 0, '1 Line Fit'}, $
         {CW_PDMENU_S, 0, '2 Line Fit'} ,$
         {CW_PDMENU_S, 0, '3 Line Fit'} ,$
         {CW_PDMENU_S, 0, '4 line Fit'}, $
         {CW_PDMENU_S, 2, '5 line Fit'}, $ 
     { CW_PDMENU_S,   3, 'All Points'}, $
      {CW_PDMENU_S, 0, '1 Line Fit'}, $
         {CW_PDMENU_S, 0, '2 Line Fit'} ,$
         {CW_PDMENU_S, 2, '3 Line Fit'}  ]
 drop_w_fit=lonarr(3)
  drop_w_fit= cw_pdmenu(base58a, fit_menu, /return_full)
  
   

  WIDGET_CONTROL, XRAS_MAIN, /REALIZE
widget_id ={xras_main:xras_main, $
            xras_pd:xras_pd,$
            text_a:text_a, $
            FIELD_X:FIELD_X, FIELD_Y: FIELD_Y, $
            FIELD_VAL: FIELD_VAL, $
            IMA_COL:DROP_IMA_COL, IMA_PAR:DROP_IMA_PAR, $
            DRAW_IMA:draw_ima,  $
            Y_COL: DROP_Y_COL, $
            DRAW_Y:draw_y, $
            DRAW_W:DRAW_W, $
            W_COL:DROP_W_COL, $
             W_FIT : drop_w_fit}

if resume then begin
            xras_str.widget.xras_main=xras_main
            xras_str.widget.xras_pd=xras_pd
            xras_str.widget.text_a=text_a
            xras_str.widget.FIELD_X=FIELD_X
            xras_str.widget.FIELD_Y=FIELD_Y
            xras_str.widget.FIELD_VAL=FIELD_VAL
            xras_str.widget.IMA_COL=DROP_IMA_COL
            xras_str.widget.IMA_PAR=DROP_IMA_PAR
            xras_str.widget.DRAW_IMA=draw_ima
            xras_str.widget.Y_COL=DROP_Y_COL
            xras_str.widget.DRAW_Y=draw_y
            xras_str.widget.DRAW_W=DRAW_W
            xras_str.widget.W_COL=DROP_W_COL
            xras_str.widget.W_FIT=drop_w_fit
            
endif 
if not resume then begin

     DROP_IMA_COL_ID=0 & DROP_IMA_PAR_ID =0 
     DROP_Y_COL_ID = 0 & DROP_Y_PAR_ID =3 
     DROP_W_COL_ID = 0 &  DROP_W_PAR_ID =3

drop_id={IMA_COL:DROP_IMA_COL_ID, $
         IMA_PAR: DROP_IMA_PAR_ID, $
         y_col :  DROP_Y_COL_ID, $
         w_col : DROP_W_COL_ID}
              
 
xoff_plot_dev = 70
yoff_plot_dev = 70
xorig_img_pix = 0
yorig_img_pix = 0

xbin=1
ybin=1
icol=replicate(0, 4)
ipar=replicate(0,3)

xdim=dstr.dim(2)
ydim=dstr.dim(1)
x_curr = replicate(xdim/2, 4)
y_curr = replicate(ydim/2, 4)


ndata=dstr.ndata
wldim = dstr.dim(0)
roi_line = intarr(2, ndata)
roi_space=[0,0, xdim-1, ydim-1]

for i=0, ndata-1 do roi_line(*,i) =[0, wldim-1]
data_fit=fltarr(5, ydim, xdim, ndata)
data_fit(1, *, *, *) =total(dstr.data, 1)/float(dstr.amp)
data_fit(3, *, *, *) =  1./sqrt(2.*!pi) 

if wldim lt 1024 then rest_pixel = replicate(wldim*0.5, xdim, ndata) $
  else if  ndata gt 1 then rest_pixel =$
   replicate(1, xdim)#reform(dstr.refpixels(0,*), ndata) else $
rest_pixel =replicate(dstr.refpixels(0,0), xdim)

!p.charsize=1.0

     xvalues  = fltarr(xdim, ndata)
     yvalues  = fltarr(ydim, ndata)
     wlvalues = fltarr(wldim, ndata)
     wtitle = 'WAVELENGTH (IMAGE PIXELS)'
     ytitle = 'Y POSITION (IMAGE PIXELS)'
     if n_elements(dstr.dim) ge 3 then $
     xtitle = 'STEP NUMBER' else $
     xtitle = '' 

 
              
     for i=0, ndata-1 do begin
      xvalues(*,i) = findgen(xdim)
      yvalues(*,i) = findgen(ydim)
      wlvalues(*,i) = findgen(wldim)

     endfor

factor  = ((300.*dstr.deltas(1,0)/ydim)$
    <(200.*dstr.deltas(2,0)/xdim)<(200./wldim)>1.)

mag_factor_y=  factor
mag_factor_x = factor
mag_factor_w = factor

x=x_curr(0)
y=y_curr(0)
xmark = x*mag_factor_x+xoff_plot_dev+mag_factor_x/2.
ymark = y*mag_factor_y+yoff_plot_dev+mag_factor_y/2
;mark_on_image, xmark, ymark, /mark_only, color=green
;mark_on_image, xmark, ymark, /remove_only
plot_style, 0



xras_par ={xdim:xdim, ydim:ydim, wdim:wldim, $
           x_curr:x_curr, y_curr:y_curr, $
           roi_space:roi_space, roi_line:roi_line, $
           xoff_dev:xoff_plot_dev, yoff_dev:yoff_plot_dev, $
           mag_x : mag_factor_x,   $  
           mag_y : mag_factor_y,   $
           mag_w : mag_factor_w,   $
           flip_ns:1, $
            flip_ew:1, $
           xval : xvalues, $
           yval : yvalues, $
           wval : wlvalues, $
           xtitle:xtitle, $
           ytitle:ytitle, $
           wtitle:wtitle, $
           icol : icol, $
           ipar: ipar, $
           xbin:xbin, ybin:ybin, $
           data_fit:data_fit, $
           rest_pixel:rest_pixel }
 
minv = fltarr(4, ndata) & maxv=minv
minv(0,*)=1. & minv(1,*)=-20. & minv(2, *)= 10.  & minv(3, *)=-1
maxv(0,*)=3.  & maxv(1,*)=20. & maxv(2, *)= 50. & maxv(3, *)=2


wplot ={show_fit:0, ylog:0, charsize:1.0, xyratio:1.4}
imaplot ={charsize:1.0, minv:minv, $
         maxv:maxv }

xras_str = {widget:widget_id,  drop:drop_id, par:xras_par, $
            wplot:wplot, imaplot:imaplot}

loadct, 3
sumer_get_color

endif


widget_control, xras_str.widget.DRAW_W, set_draw_view=[0,0]
widget_control, xras_str.widget.DRAW_Y, set_draw_view=[20,0]
widget_control, xras_str.widget.DRAW_IMA, set_draw_view=[0,0]
widget_control, xras_str.widget.ima_col, set_drop=xras_str.par.icol(0)
widget_control, xras_str.widget.ima_par, set_drop=xras_str.par.ipar(0)
widget_control, xras_str.widget.y_col, set_drop=xras_str.par.icol(1)
widget_control, xras_str.widget.w_col, set_drop=xras_str.par.icol(3)


sumer_display_ima
sumer_display_w
sumer_display_spectrum
widget_control, xras_str.widget.XRAS_MAIN, sens=1



;-----------------------------------------------

next:

  XMANAGER, 'SUMER_TOOL_MAIN', XRAS_MAIN



END
