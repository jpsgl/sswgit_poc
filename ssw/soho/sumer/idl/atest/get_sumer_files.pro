;+
; Project     : SOHO-SUMER
;
; Name        : GET_SUMER_FILES
;
; Purpose     : find SUMER fits files
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : files=get_sumer_files(tstart,tend)
;
; Inputs      : TSTART,TEND = tstart/tend to search OR FILE = filename
;               If TEND is not entered, then all files on TSTART day will
;               be returned.
;
; Outputs     : FILES = full filenames found
;
; Keywords    : ERR = error string
;               COUNT = number of files found
;               INDEX = user-supplied index file
;               FLAT = get flat field files only
;               NEAREST = find file closest in time to TSTART
;               VERBOSE = print some messages
;               ALL = return all files
;               CHECK = check that files really exist
;               FITS or FTS for fits or .fts files (def=all)
;
; History     : Written 20 November 1998, D. Zarro (SM&A)
;
; Contact     : dzarro@solar.stanford.edu
;-


function get_sumer_files,tstart,tend,err=err,count=count,index=index,$
                 flat=flat,nearest=nearest,verbose=verbose,all=all,$
                 check=check,fits=fits,fts=fts
                
common get_sumer_files,dbase,index_file
on_error,1
err=''
files=''
nearest=keyword_set(nearest)
flat=keyword_set(flat)
verbose=keyword_set(verbose)
count=0 

if (not exist(tstart)) and (1-keyword_set(all)) then begin
 pr_syntax,'file=get_sumer_files(tstart,[tend,/near,/flat,/all]'
 return,files
endif

;-- check for user-supplied index

if datatype(index) eq 'STR' then begin
 chk=loc_file(index,count=count,err=err,/verb)
 if count eq 0 then return,files
endif else begin
 dbase_file=concat_dir('SUMER_DBASE','sumer_fits_dbase.gen')
 chk=loc_file(dbase_file,count=count)
 if count eq 0 then begin
  if verbose then message,'could not locate FITS database listing files in SUMER_FITS_DBASE',/cont
  return,files
 endif
endelse
      
new_index_file=chk(0)

;-- read if first time or index file changed

if datatype(index_file) ne 'STR' then index_file=''
do_read= (new_index_file ne index_file) or (datatype(dbase) ne 'STC')

if do_read then begin
 if verbose then print,'% GET_SUMER_FILES: reading FITS index '+chk(0)+'...' 
 restgen,dbase,file=chk(0)
endif
index_file=new_index_file

;-- check time inputs

if keyword_set(all) then begin
 tstart='1-jan-1994' & tend='1-jan-2020'
endif

err=''
t1=anytim2utc(tstart,err=err)

;-- check if filename was entered for TSTART

if (err ne '') then begin

 if (datatype(tstart) eq 'STR') then begin

  tfiles=loc_file(tstart,count=count)
  if count eq 0 then begin
   delvarx,tfiles
   for i=0,n_elements(tstart)-1 do begin
    temp=strlowcase(trim(tstart(i)))

    result=grep(temp,dbase.file,index=index)
    if result(0) eq '' then begin
     break_file,temp,fdsk,fdir,fname,fext
     if fext eq '.fits' then temp2=str_replace(temp,'fits','fts') else $
      if fext eq '.fts' then temp2=str_replace(temp,'fts','fits')
     result=grep(temp2,dbase.file,index=index)
    endif
    if result(0) ne '' then tfiles=append_arr(tfiles,result)
   endfor
   count=n_elements(tfiles)
  endif
  if count gt 0 then files=tfiles
  if count eq 1 then files=files(0)  
  return,files
 endif else get_utc,t1
endif

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 if not nearest then t1.time=0
 t2=t1 
 t2.mjd=t2.mjd+1
endif

;-- now search

files=''
t1=anytim2tai(t1)
t2=anytim2tai(t2)

;-- check for flatfield files

if flat then begin
 is_ff=strpos(strlowcase(dbase.file),'ff_')
 ifiles=where(is_ff gt -1,fcount)
 if fcount gt 0 then ff_files=(dbase)(ifiles) else begin
  message,'Could not locate any flatfield files',/cont
  return,files
 endelse
endif

if nearest then begin
 if flat then diff=abs(ff_files.time-t1) else diff=abs(dbase.time-t1)
 search=where( (diff eq min(diff)),count)
endif else begin
 if flat then $
  search=where( (is_ff gt -1) and (dbase.time le (t2 > t1)) and (dbase.time ge (t1 < t2)),count) else $
   search=where( (dbase.time le (t2 > t1)) and (dbase.time ge (t1 < t2)),count)
endelse

if count ne 0 then begin
 if nearest and flat then files=(ff_files.file)(search) else $
  files=(dbase.file)(search)
 forder = uniq([files],sort([files]))
 files=files(forder)
 count=n_elements(files)
endif

;-- exclude temporary processing files

if count gt 0 then begin
 break_file,files,dsk,dir,name,dext
 if keyword_set(fts) then ext='.fts'
 if keyword_set(fits) then ext='.fits'
 if datatype(ext) eq 'STR' then begin
  ok=where( (dext eq ext),count)
 endif else begin
  ok=where( (dext eq '.fts') or (dext eq '.fits'),count)
 endelse
 if count gt 0 then files=files(ok)
endif 

;-- now check if they are really there

if (count gt 0) and keyword_set(check) then begin
 for i=0,count-1 do begin
  chk=loc_file(files(i),count=found)
  if found then nfiles=append_arr(nfiles,files(i))
 endfor
 count=n_elements(nfiles)
 if count gt 0 then files=nfiles
endif

if count eq 1 then files=files(0)                        
if verbose then message,num2str(count)+' files found',/cont
if count eq 0 then begin
 message,'No files found',/cont
 files=''
endif

return,files & end
