;+
; Project     : SOHO-SUMER
;
; Name        : SUMER_CALIB
;
; Purpose     : calibrate SUMER fits files
;
; Category    : SUMER data analysis
;
; Explanation : 
;
; Syntax      : sumer_calib,file,index,data OR sumer_calib,index,data
;
; Examples    :
;
; Inputs      : INDEX,DATA = Yohkoh-style index and data cube
;               FILE = SUMER filename to read
;
; Opt. Inputs : None
;
; Outputs     : INDEX,DATA = Yohkoh-style index and data cube 
;               (if FILE is used).
;
; Opt. Outputs: None
;
; Keywords    : DESTRETCH = optional destretch image
;               FLAT = flatfield file name
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 20 November 1998, D. Zarro (SM&A)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro sumer_calib,file,index,data,destretch=destretch,verbose=verbose,flat=flat

on_error,1

file_entered=datatype(file) eq 'STR'
index_data=(datatype(file) eq 'STC') and (datatype(index) eq 'STC')

if (1-file_entered) and (1-index_data) then begin
 pr_syntax,'sumer_calib,file,index,data,[destretch=destretch]'
 return
endif

if not keyword_set(destretch) then message,'use /destretch to apply destretch',/cont

if file_entered then begin
 a=get_sumer_files(file,count=count,/verb,/check) 
 if count eq 0 then return
 message,'reading '+a+'...',/cont
 rd_sumer,a,index,data
endif else begin
 data=copy_var(index)
 index=copy_var(file)
endelse

;-- flatfield

message,'flatfielding...',/cont

sumer_flatfield,index,data,verbose=verbose,flat=flat

;-- destretch

if keyword_set(destretch) then begin
 message,'destretching...',/cont
 sumer_destretch,index,data
endif 

;-- copy variables into appropriate names
 
if index_data then begin
 file=copy_var(index)
 index=copy_var(data)
endif

return & end

