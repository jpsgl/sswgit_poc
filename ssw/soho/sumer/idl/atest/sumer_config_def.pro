pro sumer_config_def, dummy
;+
; NAME: SUMER_CONFIG_DEF
; PURPOSE:
;        Define the default SUMER configurations 
;-
cd, current=work_dir
defsysv, '!sumer_config', exist=exist

if exist eq 1 then s=execute('a=!sumer_config') $
   else   a={work_dir:'', $
             data_dir:'', $
              aux_dir:'', $
              access:'', $
             ftp_node:'', $ 
             ftp_os:'', $
             ftp_user:'', $
             ftp_passwd:''}

a.work_dir = work_dir
a.data_dir = work_dir
a.aux_dir  = getenv('SUMER_FITS_DBASE')
a.access   = 'ARCHIVE'
a.ftp_node = ''
a.ftp_os   = 'unix'
a.ftp_user = strlowcase(getenv('USER'))

       
if exist ne 1 then defsysv, '!sumer_config', a  else s=execute('!sumer_config=a')

end
   
