;+
;
; NAME :
;        SUMER_TOOL_PD_EVENT
; PURPOSE 
;        Responds to pull-down events in SUMER_TOOL
; CALLING SEQUENCE:
;        SUMER_TOOL_PD_EVENT,  event [, group=group]
; INPUTS:
;        EVENT       an event structure variable (given by XRASTER)
; OUTPUTS:
;        None
;        GROUP       widget ID of the group leader (optional input)
; COMMON BLOCKS:
;        SUMER_TOOL_BLOCK
;        
; SIDE EFFECTS:
;        Changes  some variables in the common blocks
;        Controls data manipulations
;        Controls various kinds of display 
;
; REQUIRED ROUTINES:
;        GET_COLOR  SAVE_RASTER  FLAT_FIELDING
;        SUM_COMPRESS1    SUM_DECOMPRESS1  IDL_VECTOR_FONT
;        XRASTER_HELP      
;        DISPLAY_IMA      DISPLAY_X    DISPLAY_Y  
;        DISPLAY_W
;        DISPLAY_SPECTRUM  MARK_ON_IMAGE
; MODIFICATION HISTORY
;        March 1997      Jongchul Chae
;        Feb 1999        Zarro, NASA/GSFC (added call to GET_SUMER_FLAT)
;- 
PRO SUMER_TOOL_PD_Event, Event, GROUP=group
common sumer_tool_block, xras, dstr
@monocolors_block
common measure_dist_block, xmm, ymm, volume
;par=xras.par
  CASE Event.Value OF 


  'File.Exit without IDL save': BEGIN
     delvarx, xras, dstr
     widget_control, event.top, /destroy
    return

    END

  'File.Exit with IDL save': BEGIN
     filename='idl.rst'
     xinput, filename, title='IDL save file', /modal, status=accept
     if not accept then return
     widget_control, /hourglass 
     save, xras, dstr, $
       filename=concat_dir(!sumer_config.work_dir, filename) 
     delvarx, xras, dstr
     widget_control, event.top, /destroy
    return

    END


  'IDL Prompt': Begin
     tmp=widget_message('To resume, type SUMER_TOOL on the IDL command line.')
     widget_control, event.top, /destroy
    return

    END

 'Config.Display/Change':begin
     sumer_config    
     end
    'Config.Default':begin
      sumer_config_def
     end

   'Color' : begin
    xloadct, group=event.top, /modal
    sumer_get_color
    end
 
    'File.Exten.Next' : Begin
          dstr.extension=(dstr.extension+1)<dstr.next
     end
    'File.Exten.Previous' : Begin
         dstr.extension=(dstr.extension-1)>1
     end
    'File.Exten.First' : begin
         dstr.extension=1
     end
    'File.Exten.Last' : begin
          dstr.extension = dstr.next
     end  
   'File.Show Primary Header': begin
     xdisplayfile,text=dstr.h, $
   title=' FITS Primary Header of '+dstr.file(0), $
                    group=event.top
     end
   'File.Show Binary Header':begin
     title='FITS Binary Header ('+strtrim(string(dstr.extension),2)+'/'+ $
            strtrim(string(dstr.next),2)+') of '+dstr.file(0)
    ; bh=headfits(dstr.file, exten=dstr.extension)
     xdisplayfile,text=dstr.bh,title=title, group=event.top
     end
   'File.Read Data': begin
     xras.par.icol =replicate(0,4) 
     rdsumerext, exten=dstr.extension
     widget_control, xras.widget.ima_col, set_droplist_select=xras.par.icol(0)  
     widget_control, xras.widget.X_COL, set_droplist_select=xras.par.icol(2)
     widget_control,xras.widget.Y_COL, set_droplist_select=xras.par.icol(1)
     widget_control, xras.widget.W_COL, set_droplist_select=xras.par.icol(3) 
 
    end
    'File.Save Raster Image(s)':begin
       status=replicate(1, dstr.ndata)
      xselect, dstr.data_names,  status, abort, /nonexclusive
      if abort then return
      sumer_raster_save, where(status eq 1)
     end
    'File.Save Spectrogram(s)':begin
            sumer_spectrogram_save
     end

   'Data Handle.Saturation.Correct' : begin
    widget_control, /hourglass
    dstr.data=dstr.data/dstr.amp
    dstr.amp=1.
     dstr=sumer_sat_cor(temporary(dstr))
    end
   'Data Handle.Flat Field.Correct' :begin
     flatfile=''
     if have_proc('get_sumer_flat') then $
      flatfile=call_function('get_sumer_flat',dstr.file(0),/compressed)
     if flatfile eq '' then flatfile=sumer_Pick_flat(dstr.file(0))
     sumer_flat_dstr, dstr, file=flatfile
     xras.par.data_fit(1, *, *, *) =total(dstr.data, 1)/float(dstr.amp)
     xras.par.data_fit(3, *, *, *) =  1./sqrt(2.*!pi)   
      ffonoff = 'On  ground'
     sumer_display_ima
     sumer_display_w  
    end
   'Data Handle.Flat Field.Decorrect' :begin
    widget_control, /hourglass
    sumer_flat_dstr, dstr, /decorr
   
      xras.par.data_fit(1, *, *, *) =total(dstr.data, 1)/float(dstr.amp)
      xras.par.data_fit(3, *, *, *) =  1./sqrt(2.*!pi)

    sumer_display_ima
    sumer_display_w
    end
    'Data Handle.Correct Distortion.Both Directions' :begin
     widget_control, /hourglass
     sumer_distort_dstr, dstr, xoff=0., yoff=0.
     xras.par.data_fit(1, *, *, *) =total(float(dstr.data), 1)/float(dstr.amp)
     xras.par.data_fit(3, *, *, *) =  1./sqrt(2.*!pi)

   
    end 
    'Data Handle.Correct Distortion.X Direction Only' :begin
     widget_control, /hourglass
     sumer_distort_dstr, dstr, yoff=1, xoff=0.
     xras.par.data_fit(1, *, *, *) =total(float(dstr.data), 1)/float(dstr.amp)
     xras.par.data_fit(3, *, *, *) =  1./sqrt(2.*!pi)
   
    end 
     'Data Handle.Correct Distortion.Y Direction Only' :begin
     widget_control, /hourglass
     sumer_distort_dstr, dstr, xoff=1, yoff=0.
     xras.par.data_fit(1, *, *, *) =total(float(dstr.data), 1)/float(dstr.amp)
     xras.par.data_fit(3, *, *, *) =  1./sqrt(2.*!pi)
   
    end 

    'Data Handle.Fill Missing Data' : begin
     intensity = total(total(dstr.data(*,*,*,*), 1),1)/dstr.amp
     
     sp = total(total(dstr.data, 3),2)/n_elements(dstr.data(0,*,*,0)) 
     for i=0, n_elements(dstr.data(0,0,*,0))-1 do $
        for l=0, n_elements(dstr.data(0,0,0,*))-1 do $
      if intensity(i,l) le 10 then     dstr.data(*,*, i,l) = $
     reform(sp(*, l))#replicate(1,n_elements(dstr.data(0,*,0,0)) ) 
        
    end
         
    'Data Handle.Smooth': begin
      tmp={X_smooth:xras.par.xbin, Y_smooth:xras.par.ybin}
      xstruct,  tmp,  title='Smoothing Lengths', /modal , nx=1, /edit
      xras.par.xbin=tmp.X_smooth
      xras.par.ybin=tmp.Y_smooth
  
      sumer_display_ima
      sumer_display_spectrum
      end
    'Data Handle.Coordinates.Image Pixels':begin
     xras.par.wtitle = 'WAVELENGTH (IMAGE PIXELS)'
     xras.par.ytitle = 'Y POSITION (IMAGE PIXELS)'
     if n_elements(dstr.dim) ge 3 then $
     XRAS.PAR.xtitle = 'STEP NUMBER' else $
     XRAS.PAR.xtitle = '' 
   
     for i=0, dstr.ndata-1 do begin
      xras.par.xval(*,i) = findgen(xras.par.xdim)
      xras.par.yval(*,i) = findgen(xras.par.ydim)
      xras.par.wval(*,i) = findgen(xras.par.wdim)
    endfor
     sumer_display_ima
     sumer_display_w 
    end
    'Data Handle.Coordinates.Detector Pixels':begin
     
     xras.par.wtitle = 'WAVELENGTH (DETECTOR PIXELS)'
     xras.par.ytitle = 'Y POSITION (DETECTOR PIXELS)'
     if n_elements(dstr.dim) ge 3 then $
     XRAS.PAR.xtitle = 'STEP NUMBER' else $
     XRAS.PAR.xtitle = ''
  
     for i=0, dstr.ndata-1 do begin
      xras.par.xval (*,i)= $
     (findgen(xras.par.xdim)-dstr.refpixels(2, i)+1)+dstr.sumpixels(2,i)-1
      xras.par.yval(*,i) =  $
    (findgen(xras.par.ydim)-dstr.refpixels(1,i)+1) $
   +dstr.sumpixels(1,i)-1
      xras.par.wval(*,i) =findgen(xras.par.wdim) $
               +(-dstr.refpixels(0,i)+1) +dstr.sumpixels(0, i)-1
       endfor
     sumer_display_ima
     sumer_display_w
     end
    'Data Handle.Coordinates.Physical Units':begin
     xras.par.wtitle = 'WAVELENGTH (A)'
     xras.par.ytitle = 'SOLAR_Y (ARCSEC)'
     if dstr.dim(2) ge 2 then begin
     tmp = fxbtdim(fxpar(dstr.bh, 'TDESC1'))
  
     XRAS.PAR.xtitle = tmp(2)+' ('
      tmp = fxbtdim(fxpar(dstr.bh, 'TCUNIT1'))
     if n_elements(tmp) eq 1 then  tmp = fxbtdim(fxpar(dstr.bh, 'TUNIT1'))
     xras.par.xtitle=xras.par.xtitle+tmp(2)+')'
     endif else XRAS.PAR.xtitle = ''
     large=xras.par.wdim eq 1024
     for i=0, dstr.ndata-1 do begin   
     xras.par.xval(*,i) = (findgen(xras.par.xdim)-dstr.refpixels(2, i)+1) $
                 *dstr.deltas(2,   i)+dstr.refpos(2,i)
      xras.par.yval(*,i) = $
      -(findgen(xras.par.ydim)-dstr.refpixels(1,i)+1) $
       *dstr.deltas(1,i) $
       +dstr.refpos(1,i)
    
      xras.par.wval(*,i) =$                   
    (findgen(xras.par.wdim) $
         -dstr.refpixels(0,i)+1) $
       *dstr.deltas(0,i)+dstr.refpos(0,i)
        
      endfor
        sumer_display_ima
       end
     'Data Handle.Magnification' : begin
      tmp={Mag_x: xras.par.mag_x, Mag_y: xras.par.mag_y, Mag_w: xras.par.mag_w}           
      xstruct, tmp, /edit, title='Magnification', /modal, nx=1
      xras.par.mag_x = tmp.Mag_x
      xras.par.mag_y = tmp.Mag_y
      xras.Par.mag_w = tmp.Mag_w 
      sumer_display_ima
      sumer_display_spectrum
      end 
      'Data Handle.Gray Scale Min & Max': begin
         tmp={Min:xras.imaplot.minv(xras.par.ipar(0), xras.par.icol(0)), $
              Max:xras.imaplot.maxv(xras.par.ipar(0), xras.par.icol(0))}
     
         xstruct, /edit, title='Gray Scale Min & Max', nx=1, tmp
         xras.imaplot.minv(xras.par.ipar(0), xras.par.icol(0))=tmp.Min
        
         xras.imaplot.maxv(xras.par.ipar(0), xras.par.icol(0))=tmp.Max
         sumer_display_ima
      end
     'Data Handle.Rotate.Flip N/S' : begin
      xras.par.flip_ns=xras.par.flip_ns*(-1)   
      end
      'Data Handle.Rotate.Flip E/W':begin
       xras.par.flip_ew=xras.par.flip_ew*(-1)
      end
    'Data Handle.Reg. of Interest.Spatial Region' : begin
      
       y1=0 & y2= xras.par.ydim-1
        x1=0 & x2= xras.par.xdim-1
       image = line_par_conv($
         ( xras.par.data_fit(*,y1:y2,x1:x2,xras.par.icol(0))), $
          dstr.refpos(0,xras.par.icol(0)), $  
          dstr.deltas(0,xras.par.icol(0)),  $
          xras.par.rest_pixel(xras.par.icol(0)))
       image=rotate(reform(image(xras.par.ipar(0)+1,*,*)),3)
       if xras.par.ipar(0) eq 0 then image=image /dstr.amp
      if xras.par.ipar(0) eq 0 then image=alog10(image>median(image)*0.1)
       
       zoom = [600/xras.par.xdim<5>1, 600/xras.par.ydim<5>1]

       tmp = xdefroi(bytscl(image, $
         xras.imaplot.minv(xras.par.ipar(0), xras.par.icol(0)), $
         xras.imaplot.maxv(xras.par.ipar(0), xras.par.icol(0))), zoom=zoom)

       xyindex, image, xa, ya
       xras.par.roi_space(0) = min(xa(tmp), max=m)
       xras.par.roi_space(2) = (m+1)<max(xa)
       xras.par.roi_space(3) = xras.par.ydim-1- min(ya(tmp), max=m)
       xras.par.roi_space(1) = xras.par.ydim-1- ((m+1)<max(ya))
         end
    'Data Handle.Reg. of Interest.Spectral Lines' : begin
         status=replicate(1, dstr.ndata)
      xselect, dstr.data_names,  status, abort, /nonexclusive
         xyindex, dstr.data(*,*,0,0), xa, ya
         for i=0, dstr.ndata-1  do if status(i) then  begin
            tmp = dstr.data(*,*, xras.par.x_curr(0), i)
          zoom =[(1024/xras.par.wdim)<3>1,(360/xras.par.ydim)<3>1]
          tmp = xdefroi(image_compress(tmp, 1, $
          /sqrt, maxbyte=ncolor1-1), zoom=zoom, $
           title=dstr.data_names(i))
           xras.par.roi_line(0, i) = min(xa(tmp), max=m)
           xras.par.roi_line(1,  i)= (m+1)<max(xa)
          endif
     end  
    'Data Handle.Mirror Scan Correction' : begin
    status=replicate(0, dstr.ndata)
    status(0)=1
  xselect,  dstr.data_names, status, abort,  $
        title='Select Reference for Scan Mirror Correction', /exclusive 
 if not abort then  begin
     line=where(status )  & line=line(0)
     xstart = xras.par.roi_space(0)
       xend =  xras.par.roi_space(2)
 y1 =  xras.par.roi_space(1)
      y2 = xras.par.roi_space(3)
    tmp =  median(xras.par.data_fit(2, y1:y2, xstart:xend, line))
   for x=0, xend-xstart do  $
        xras.par.rest_pixel(xstart+x, *) =  xras.par.rest_pixel(xstart+x, *)   $
               +(median(xras.par.data_fit(2, y1:y2, xstart+x, line))-tmp)
    endif
   end
    'Data Handle.Gaussian Fit':begin
     status=replicate(1, dstr.ndata)
      xselect, dstr.data_names,  status, abort, /nonexclusive
      if abort then return
      widget_control, /hourglass
      xyindex, fltarr(xras.par.wdim, xras.par.ydim), wa, ya
      xbin=xras.par.xbin
      ybin=xras.par.ybin
       xstart = xras.par.roi_space(0)
       xend =  xras.par.roi_space(2)
       y1 =  xras.par.roi_space(1)
      y2 = xras.par.roi_space(3)
     sigma = ybin 
    ker = exp(-0.5*((findgen(ybin*2)-ybin)/sigma)^2)
     ker = transpose(shift(ker, -ybin)/total(ker))
     for i=0, dstr.ndata-1 do  if  status(i) then for x=xstart, xend do  begin
      
       x1=(x-xbin/2)>0
       x2 =(x1+xbin-1)<(xras.par.xdim-1)
      x1=(x2-xbin+1)>0
      tmp=  total(reform(dstr.data(*,*, x1:x2, i), xras.par.wdim, $
                  xras.par.ydim, x2-x1+1), 3) /dstr.amp/(x2-x1+1)
         
      if ybin gt 1 then tmp=convol(tmp,ker, /edge_trun)
      s1 = xras.par.roi_line(0, i)
      s2 = xras.par.roi_line(1, i)
      fit_par=gaussfit_int(tmp(s1:s2, y1:y2))
     xras.par.data_fit(0:3, y1:y2, x,i) =  fit_par
  
   
      endfor
      xras.wplot.show_fit=1
     for i=0, dstr.ndata-1 do  $
 xras.par.rest_pixel(*, i) = median(xras.par.data_fit(2,y1:y2,xstart:xend,i))
     end
  'Data Handle.Band Filtering' : begin
     status=replicate(1, dstr.ndata)
      xselect, dstr.data_names,  status, abort, /nonexclusive
      if abort then return
      widget_control, /hourglass
      xyindex, fltarr(xras.par.wdim, xras.par.ydim), wa, ya
      xbin=xras.par.xbin
      ybin=xras.par.ybin
       xstart = xras.par.roi_space(0)
       xend =  xras.par.roi_space(2)
       y1 =  xras.par.roi_space(1)
      y2 = xras.par.roi_space(3)
     sigma = ybin 
     ker = exp(-0.5*((findgen(ybin*2)-ybin)/sigma)^2)
     ker = transpose(shift(ker, -ybin)/total(ker))
     for i=0, dstr.ndata-1 do  if  status(i) then for x=xstart, xend do  begin
      
       x1=(x-xbin/2)>0
       x2 =(x1+xbin-1)<(xras.par.xdim-1)
      x1=(x2-xbin+1)>0
      tmp=  total(reform(dstr.data(*,*, x1:x2, i)*1., xras.par.wdim, $
                  xras.par.ydim, x2-x1+1), 3) /dstr.amp/(x2-x1+1)
         
      if ybin gt 1 then tmp=convol(tmp,ker, /edge_trun)
      s1 = xras.par.roi_line(0, i)
      s2 = xras.par.roi_line(1, i)
     
     xras.par.data_fit(1, y1:y2, x,i) = $
        total(tmp(s1:s2, y1:y2),1) $
        /sqrt(2.*!pi)/xras.par.data_fit(3, y1:y2, x, i)
     
      endfor
 xras.wplot.show_fit=0

   end
  'Font.X Window Font' :begin
      widget_control, /hourglass
      widget_control, default_font=xfont()
   end
  'Font.Select':begin
     IDL_vector_font,  group=event.top

  end
   'Font.Magnify':begin
     !p.charsize=1.2*!p.charsize

   end
   'Font.Reduce':begin
     !p.charsize=!p.charsize/1.2
   end
  'Help':begin
    xraster_help  
    END
  'Hardcopy.X Profile':begin
    sumer_display_x, ps=1
     end
  'Hardcopy.Spectrogram (PS)' :begin
   sumer_hardcopy_spectrum
     end
  'Hardcopy.Spectrogram (GIF)' :begin
   sumer_gif_spectrum
     end

  'Hardcopy.Spectral Profile' :begin
   sumer_display_w, ps=1 
     end
    
  'Hardcopy.Scan Image (PS)' :begin
      sumer_hardcopy_ima
     end
 'Hardcopy.Scan Image (GIF)' :begin
      sumer_gif_ima
     end

  ENDCASE

  END


