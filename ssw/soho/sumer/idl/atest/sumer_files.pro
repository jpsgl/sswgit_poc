;+
; Project     : SOHO-SUMER
;
; Name        : SUMER_FILES
;
; Purpose     : find SUMER fits files
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : files=sumer_files(tstart,tend)
;
; Examples    :
;
; Inputs      : TSTART,TEND = tstart/tend to search
;
; Opt. Inputs : None
;
; Outputs     : FILES = filenames found
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               COUNT = number of files found
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 20 November 1998, D. Zarro (SM&A)
;
; Contact     : dzarro@solar.stanford.edu
;-


function sumer_files,tstart,tend,err=err,count=count

common sumer_files,dbase
on_error,1
err=''
files=''

chk=loc_file('SUMER_FITS_DBASE',count=count)
if count eq 0 then begin
 message,'could not locate FITS database listing files in SUMER_FITS_DBASE',/cont
 return,''
endif

if datatype(dbase) ne 'STC' then restore,chk(0)


;-- check time inputs

err=''
t1=anytim2utc(tstart,err=err)

;-- check if filename was entered for TSTART

if (err ne '') then begin
 if (datatype(tstart) eq 'STR') then begin
  for i=0,n_elements(tstart)-1 do begin
   temp=strlowcase(trim(tstart(i)))
   result=grep(temp,dbase.file,index=index)
   if result eq '' then begin
     break_file,temp,fdsk,fdir,fname,fext
     if fext eq '.fits' then temp=str_replace(temp,'fits','fts') else $
      if fext eq '.fts' then temp=str_replace(temp,'fts','fits')
     result=grep(temp,dbase.file,index=index)
   endif
   if result ne '' then tfiles=append_arr(tfiles,result)
  endfor
  count=n_elements(tfiles)
  if count gt 0 then files=tfiles
  if count eq 1 then files=files(0)
  return,files
 endif else get_utc,t1
endif

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
endif

;-- now search

files=''
t1=anytim2tai(t1)
t2=anytim2tai(t2)

search=where( (dbase.time le (t2 > t1)) and (dbase.time ge (t1 < t2)),count)
if count ne 0 then begin
 files=(dbase.file)(search)
 forder = uniq([files],sort([files]))
 files=files(forder)
 if n_elements(files) eq 1 then files=files(0)
endif

return,files & end
