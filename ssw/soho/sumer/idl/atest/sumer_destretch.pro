
;+
; NAME:
;
; sumer_destretch
;
; PURPOSE:
;
; Calls Tom Moran's destretch routine which does sub-pixel corrections
; for optical distortion for SOHO/SUMER observations.
;
; CATEGORY:
;
; Data analysis.
;
; CALLING SEQUENCE:
;
; sumer_destretch,index,data
; 
; INPUTS:
;
; The index and data structures returned from rd_sumer.
;
; OPTIONAL INPUTS:
;
; None.
;       
; KEYWORD PARAMETERS:
;
; verbose: if set an explanation of destretch will be printed 
;       and  program will keep user informed of progess
;
; OUTPUTS:
;
; The input data is overwritten with the corrected data.
;
; OPTIONAL OUTPUTS:
;
; None.
;
; COMMON BLOCKS:
;
; None.
;
; SIDE EFFECTS:
;
; None.
;
; RESTRICTIONS:
;
; The corrected data may correspond to a slightly different region of
; the detector than was input.
;
; PROCEDURE:
;
; Calls destretch. If a subsection of the detector is passed as input,
; the array is embedded in a full detector array and that is processed
; with destretch. Information about the processing is noted in the
; index structures using the OPTDIST and HISTORY tag names.
;
; EXAMPLE:
;
; IDL> file = "sum_960513_085055.fits"
; IDL> rd_sumer,file,index,data,data_cols=[1]
; IDL> flatfield,index,data
; IDL> sumer_destretch,index,data
;
; MODIFICATION HISTORY:
;
;   HPW 03-SEP-1996: 
;
;   TAK 13-NOV-1996: Verbose keyword controls the DESTRETCH QUIET
;                    keyword.
;
;   HPW 19-NOV-1996: Added check on detector.
;
;   HPW 10-OCT-1997: Fixed bug related to sgt_range.
;
;   HPW 18-DEC-1997: Modified the routine to handle files created from
;                    both real time data and cd-rom data.
;
;   DMZ 29-DEC-1998: renamed to SUMER_DESTRETCH
;-

pro sumer_destretch,index,data,verbose=verbose,show_exps=show_exps

;; check for binning
bin = sgt_bin(index)
mm  = where(bin ne 1 and bin ne 0,count)
if (count gt 0) then begin
  message,'Binned data should not be destretched.',/INFORMATIONAL
endif

;; check detector
detector = sgt_detector(index,/text)
if (detector ne 'A' and detector ne 'B') then begin
  message,'Data is from '+detector,/informational
  message,'Only data from detector A or B may processed with DESTRETCH.',/info
  return
endif

;; parameters
dims   = sgt_dims(index)
slit   = sgt_slit(index)
refpix = sgt_refpix(index)
slit   = sgt_slit(index)

n_col = n_tags(data)
for ic = 0,n_col-1 do begin

  n_exp = n_elements(data.(ic)(0,0,*))
  for ie=0,n_exp-1 do begin

    ;; compute optical distortion correction - check orientation of data
    time_1   = systime(1)
    data_in  = data.(ic)(*,*,ie)
    len      = strlen(index.gen.filename)
    data_ext = strupcase(strmid(index.gen.filename,len-3,3))
    if data_ext eq "FTS" then data_in = reverse(data_in,2)
    data_out = destretchn(data_in,slit,refpix(0,ic),detector,/QUIET)
    if data_ext eq "FTS" then data_out = reverse(data_out,2)
    method   = 'destretch'

    if keyword_set(VERBOSE) then begin
      time = systime(1) - time_1
      mm   = fix(time/60)
      ss   = round(time mod 60)
      print,' Exposure ',string(format='(i4)',ie+1),$
          ' of ',string(format='(i4)',n_exp)+' processed in '+$
          trim(mm,'(i2.2)')+":"+trim(ss,'(i2.2)')
    endif
    
    ;; overwrite old uncorrected data
    wr = (sgt_range(index,/spectral))(*,ic)
    yr = (sgt_range(index,/spatial))(*,ic)
    data.(ic)(*,*,ie) = data_out(wr(0):wr(1),yr(0):yr(1))

    ;; note processing in the index structure
    history = index.(ic).spectrum(ie).history
    if history eq '' then sep = '' else sep = ','
    index.(ic).spectrum(ie).history = history + sep + method
    index.(ic).spectrum(ie).optdist = method

    if keyword_set(SHOW_EXPS) then begin
      print,wr,yr
      nx = n_elements(data_in(*,0))
      ny = n_elements(data_in(0,*))
      setup_xwindow,nx,ny*2
      loadct,3
      tvscl,alog10(data_in > 1),0
      tvscl,alog10(data_out(wr(0):wr(1),yr(0):yr(1)) > 1),1
    endif

  endfor
  
endfor

return
end


