;+
;
; NAME :
;         SUMER_RASTER_SAVE
; PURPOSE :
;         Save raster images of line parameters into a standard fits file
; CALLING SEQUENCE:
;         SUMER
; INPUTS:
;          None
; OUPUTS:
;          None
; COMMON BLOCKS:
;         xraster_block
; SIDE EFFECTS:
;          A new standard fits file is created.
; REQUIRED SUBROUTINES:
;          fxaddpar fxhmake  WRITEFITS
; MODIFICATION HISTORY
;         May 1997       Jongchul Chae  adapted from  SAVE_RASTER
;
;- 
           
pro sumer_raster_save,  lines
common sumer_tool_block, xras, dstr
rastype=strupcase(strtrim(fxpar(dstr.h, 'RASTYPE'),2))
rasterfile= 'raster.fits'
rasterfile = pickfile(title='Select Raster Image Fits Filename for writing ',  $
    file=rasterfile)
par=xras.par

nline=n_elements(lines)
if nline eq 0 then return
y1=par.roi_space(1)
y2=par.roi_space(3)
x1=par.roi_space(0)
x2=par.roi_space(2)
xval =([x1, x2]-dstr.refpixels(2,0)+1)*dstr.deltas(2, 0) + dstr.refpos(2,0)
yval= ([y1, y2]-dstr.dim(1)/2)*dstr.deltas(1,0)+dstr.refpos(1,0)
nx=x2-x1+1
ny=y2-y1+1

image = fltarr(nx, ny, 4, nline)

for l=0, nline-1 do begin
 line=lines(l)
tmp = line_par_conv($
          par.data_fit(*,y1:y2,x1:x2, line), $
          dstr.refpos(0, line), $  
          dstr.deltas(0, line), reform(par.rest_pixel(x1:x2, line)))
 
image(*, *, 0, l) = rotate(reform(tmp( 0, *,*)), 3)   ; Contiuum
image(*, *, 1, l) =rotate(reform(tmp( 1, *,*)), 3)    ; Integrated Intensity
image(*, *, 2, l) = rotate(reform(tmp( 2, *,*)), 3)    ; Doppler Shift
image(*, *, 3, l) = rotate(reform(tmp( 3, *,*)), 3)    ; Doppler Width

endfor
h=dstr.h



fxhmake, header, image
fxaddpar, header, 'IMAGE_KIND',  'SUMER SCAN IMAGES'
fxaddpar, header, 'DATE_OBS', fxpar(h, 'DATE_OBS')

fxaddpar, header, 'XCEN', fxpar(h, 'XCEN'), $
         'in arcseconds from the sun center to the west'
fxaddpar, header, 'YCEN',  (yval(0)+yval(1))/2.  , $
        'in arcseconds from the sun center to the north'
fxaddpar, header, 'INSTRUME',fxpar(h,'INSTRUME')
fxaddpar, header,'DETECTOR',fxpar(h,'DETECTOR')
fxaddpar, header, 'SLIT', fxpar(h,'SLIT')
fxaddpar, header, 'EXPTIME',fxpar(h, 'EXPTIME'), 'seconds'
fxaddpar, header,'ROTCMP', fxpar(h, 'ROTCMP')
fxaddpar, header,'RASTYPE', rastype
if rastype eq 'RASTER' then  $
fxaddpar, header,'DESCR', '(SOLAR_X, SOLAR_Y,  LINE_PAR, LINE)',  $
'Data format description' $
else fxaddpar, header,'DESCR', '(TIME, SOLAR_Y,  LINE_PAR, LINE)', $
'Data format description'
unit = fxbtdim(fxpar(dstr.bh, 'TUNIT1'))
if n_elements(unit) lt 3 then unit = fxbtdim(fxpar(dstr.bh, 'TCUNIT1'))
fxaddpar, header, 'DELTAX',  dstr.deltas(2,0), 'in '+unit(2)
fxaddpar, header, 'DELTAY', dstr. deltas(1,0), 'in '+ unit(1)
fxaddpar, header, 'XVAL', xval(0), 'X value of lower left corner in '+ unit(2)
fxaddpar, header, 'YVAL', yval(0), 'Y value of lower left corner in '+ unit(2)
fxaddpar, header, 'XSMOOTH', xras.par.xbin, '  X-direction Smoothing'
fxaddpar, header,'YSMOOTH',  xras.par.ybin,  '  Y-direction  Smoothing'

for l=0, nline-1 do begin
 line=lines(l)
fxaddpar, header, 'LINE'+strtrim(string(l), 2), dstr.data_names(line), $
     'Approximate wavelength only for indentification' 
fxaddpar, header, 'DETPOS'+strtrim(string(l), 2), $
         dstr.sumpixels(0,line)-dstr.refpixels(0,line) $
            +median(par.rest_pixel(*, line))+par.roi_line(0, line), $
      'Detector pixel  correponding to the median position'
endfor
fxaddpar, header,  'COMMENT ', 'DATA(*,*,0,*) : Continuum Images  in counts/pixel'
fxaddpar, header,  'COMMENT ', 'DATA(*,*,1,*)  : Integrated Intensity in counts'
fxaddpar, header,  'COMMENT ', 'DATA(*,*,2,*)  : Doppler shift in km/s'
fxaddpar, header,  'COMMENT ', 'DATA(*,*,3,*)  : Doppler width in km/s'
filename=fxpar(h,'FILENAME')

 
fxaddpar, header, 'HISTORY',   $
    'produced by IDL  program  SUMER_RASTER_SAVE'
for  kk=0, n_elements(h)-1 do $
   fxaddpar, header, 'COMMENT ', h(kk) 

writefits, rasterfile, image, header
tmp =widget_message('Raster images are saved in the file '  $
 +rasterfile+'!', /information) 

end
