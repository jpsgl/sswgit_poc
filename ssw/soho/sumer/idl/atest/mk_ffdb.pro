;+
; Name:  mk_ffdb
;
; Purpose: create a SUMER flatfield database
;
; Category: SUMER Data handling.
;
; Calling Sequence: files=mk_ffdb()
; 
; Inputs: None
;
; Optional Inputs: None
;       
; Keywords : VERBOSE = set for messaging
;
; Restrictions : None
;
; Side Effects : None
;
; History : Written H. Warren (NRL)
;           Modified, 10-Jan-1999, Zarro (SMA/GSFC) -- added
;           ability to search in SOHO-ARCHIVE and for compressed versions
;
; Contact : dzarro@solar.stanford.edu
;-

function mk_ffdb,verbose=verbose,err=err

;; ----------------------------------------------------------------------
;; Find flat field files
;; ----------------------------------------------------------------------

               

nfiles=0

;; check first if compressed versions exist in $SUMER_DBASE (Zarro, Jan'99)

sumer_dbase=chklog('SUMER_DBASE')
if sumer_dbase ne '' then begin
 files=loc_file('sumff*',path=sumer_dbase,count=nfiles)
 if nfiles gt 0 then begin
  message,'located compressed SUMER flatfield files in '+sumer_dbase,/cont
  zipped=strpos(files(0),'.gz') gt -1
  catted=strpos(files(0),'.Z') gt -1
  if zipped then files=str_chop(files,'.gz') else files=str_chop(files,'.Z')
 endif
endif 

;; next check if flatfield files are online in SOHO-ARCHIVE (Zarro, Dec'98)
               
if nfiles eq 0 then begin
 if have_proc('get_sumer_files') then begin
  files=call_function('get_sumer_files',/flat,/all,count=nfiles,/check)
  if nfiles gt 0 then $
   message,'located SUMER flatfield files in SOHO-ARCHIVE',/cont
 endif
endif
               
;; otherwise check in SUMER_FLATFIELD

if nfiles eq 0 then begin
 SUMER_FLATFIELD = getenv("SUMER_FLATFIELD")
 if SUMER_FLATFIELD eq '' then begin
  err="The SUMER_FLATFIELD environment variable must be defined."
  message,err,/cont
  return,''
 endif

;; find flat field data files

 path   = concat_dir(SUMER_FLATFIELD,"sumff*")
 files  = findfile(path)
 nexist  = file_exist(files)
 nfiles = n_elements(files) 

 if nexist(0) eq 0 then begin
  err="No flat field images found."
  message,err,/cont
  message,"SUMER_FLATFIELD = "+SUMER_FLATFIELD,/cont
  return,''
 endif

endif

;; ----------------------------------------------------------------------
;; Read files
;; ----------------------------------------------------------------------

struct = {filename: '',$
          time:     '',$
          dday79:   0l,$
          detector: ''}
ffdb   = replicate(struct,nfiles)

if keyword_set(VERBOSE) then begin
  message,trim(nfiles)+" Flat field images found.",/INFORMATIONAL
  message,"Constructing database of flat field images . . . ",/INFORMATIONAL
endif

for n=0,nfiles-1 do begin
  tfile=files(n)
  rd_sumer,tfile,index,/nodata
  time_ex = filename2time(tfile)

  ffdb(n).filename = tfile
  ffdb(n).time     = fmt_tim(time_ex)
  ffdb(n).dday79   = anytim2dd79(time_ex)
  ffdb(n).detector = sgt_detector(index,/text)

  if keyword_set(VERBOSE) then begin
    fn  = str_sep(tfile,"/")
    fmt = '(2a25,a15)'
    if n eq 0 then print,format=fmt,"FILE NAME","FILE TIME","DETECTOR"
    print,format=fmt,fn(n_elements(fn)-1),$
        ffdb(n).time,ffdb(n).detector
  endif

endfor

return,ffdb
end
