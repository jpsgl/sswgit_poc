;+
; Project     : SOHO - SUMER
;
; Name        : GET_SUMER_FLAT
;
; Purpose     : Find nearest SUMER flatfield file to input file
;
; Category    : analysis,planning
;
; Syntax      : IDL> flat=get_sumer_flat(file)
;
; Inputs      : FILE = SUMER FITS filename or INDEX from RD_SUMER
;
; Outputs     : FLAT = name of nearest flatfield file
;
; History     : Version 1,  5-Feb-1999,  D.M. Zarro (SM&A),  Written
;               Based on MK_FFDB by H. Warren.
;               Version 2, 6-May-2002, Zarro (L-3Com/GSFC) - added
;               $SUMER_FLATFIELD to search path
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
 
function get_sumer_flat,file,err=err,_extra=extra

on_error,1
err=''
flat=''
dtype=datatype(file)
if (dtype ne 'STR') and (dtype ne 'STC') then begin
 pr_syntax,'flat=get_sumer_flat(file)'
 return,flat
endif

;-- read SUMER file

if dtype eq 'STR' then begin
 rd_sumer,file,index,err=err
 if err ne '' then begin
  message,err,/cont
  return,flat
 endif
endif else index=file

;-- examine flatfield database

ffdb = sumer_ffdb(err=err,_extra=extra)
if err ne '' then begin
 message,err,/cont
 return,flat
endif
        
;-- find closest flat field image

this_detector = sgt_detector(index,/text)
this_dday79   = anytim2dd79(sgt_time(index))
match = where(this_detector eq ffdb.detector,count)
if count eq 0 then begin
 err="No flat field files taken with detector "+this_detector
 message,err,/cont
 return,flat
endif
diff = min(abs(this_dday79-ffdb(match).dday79),pos_min)
flat = ffdb(match(pos_min)).filename

fn  = str_sep(flat,"/")
fn  = fn(n_elements(fn)-1)
fmt = '(2a25,a15)'
message,"Using the following flat field file:",/cont
print,format=fmt,fn,ffdb(match(pos_min)).time,ffdb(match(pos_min)).detector

flatfield=find_compressed(flat,err=err) 
if err ne '' then begin
 flat=str_replace(flat,'SUMER_DBASE','SUMER_FLATFIELD')
 flatfield=find_compressed(flat,err=err)
endif

return,flatfield & end


