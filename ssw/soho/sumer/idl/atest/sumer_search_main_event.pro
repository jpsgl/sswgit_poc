;+   
;
;                                        
;   NAME:
;     SUMER_SEARCH_MAIN_EVENT
;   PURPOSE:
;     Responses to main events in SUMER_SEARCH     
;   CALLING SEQUENCE:
;     SUMER_SEARCH_MAIN_EVENT, event
;   INPUTS:
;     EVENT = a structure variable for an event 
;   OUTPUTS:
;     None
;   COMMON BLOCKS:
;    SUMER_SEARCH_BLOCK
;   SIDE EFFECTS:
;     Change parameters defined in common blocks 
;   REQUIRED SUBROUTINES:
;     None: 
;   MODIFICATION HISTORY
;     March 1997    Jongchul Chae   
;-
pro sumer_search_main_event, event

WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

@sumer_search_block
    
  CASE Ev OF 
  'PDMENU': sumer_search_PD_Event, Event
  'LIST' : begin
        text=replicate('', 10)

     ifile=event.index
         if event.clicks eq 1 then begin
       file=files(ifile)  
       select(ifile)=1
        text(0)= 'The file '+ file+ $
        ' is selected, resulting in a total of ' $
       +strtrim(string(total(select), format='(i7)'),2)+ ' files.' 
  
     endif else begin
       select(ifile)=0
    text(0)= 'The file '+ $
      files(ifile)+ ' is de-selected, resulting in a total of ' $
   +strtrim(string(total(select), format='(i7)'),2)+ ' files.' 


     endelse
     s=where(select eq 1 , count)
     if count ge 1 then for k=0, count-1 do $
        text(k+2)=string(k, format='(i2)')+' '+catalog(s(k))

     widget_control, text_id, set_value= text

            end
  'FITS_KIND': BEGIN
      CASE Event.Value OF
      0: fits_kind='sum_' 
      1: fits_kind='sumff_' 
      2: fits_kind='sumhm_' 
      3: fits_kind='sumrsc_'
      4: fits_kind='sumfs_' 
      ELSE: Message,'Unknown button pressed'
      ENDCASE
     END

  'TIME1': BEGIN
     widget_control, event.id, get_value=time1
      time1=time1(0)
      ns=strlen(time1)
         END
  'TIME2': BEGIN
     widget_control, event.id, get_value=time2
     time2=time2(0) 
     ns=strlen(time2)
     
        END
  'XCEN1': BEGIN
     widget_control, event.id, get_value=xcen1 
      END
  'XCEN2': BEGIN
     widget_control, event.id, get_value=xcen2 
      END
  'YCEN1': BEGIN
     widget_control, event.id, get_value=ycen1

      END
  'YCEN2': BEGIN
     widget_control, event.id, get_value=ycen2 
      END
 'RCEN1': BEGIN
     widget_control, event.id, get_value=rcen1

      END
  'RCEN2': BEGIN
     widget_control, event.id, get_value=rcen2 
      END

  'WL1': BEGIN
     widget_control, event.id, get_value=wl1 
      END
  'WL2': BEGIN
     widget_control, event.id, get_value=wl2 
      END


    'EXPOSURE1' :begin
      widget_control, event.id,  get_value=exposure1
     end
    'EXPOSURE2' :begin
      widget_control, event.id,  get_value=exposure2
     end

  'SCIENTIST':begin
   
   widget_control, event.id, get_value=scientist
   scientist = strlowcase(strtrim(scientist(0),2))
    end
  'STUDY' :begin
     widget_control, event.id, get_value=studyname
    studyname=strlowcase(strtrim(studyname(0),2))
    end
  'OBJECT':begin
     widget_control, event.id, get_value=object
    object=strlowcase(strtrim(object(0),2))
       end
  'POPUDP' :begin
     widget_control, event.id, get_value=pop
     pop = strlowcase(strtrim(pop(0),2))
      END
       
  'SEQUENCE' :begin
     widget_control, event.id, get_value=seq_type
    seq_type=strlowcase(strtrim(seq_type(0), 2))
      
      END
  'SLIT' :begin
      widget_control, event.id, get_value=slit
   slit=strtrim(slit(0), 2)
    end
   'DETECTOR':Begin
      widget_control, event.id, get_value=detector
       detector=strupcase(strtrim(detector,2))
    detector=detector(0)

     end
ENDCASE
END


