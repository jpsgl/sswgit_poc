;+
;
;   NAME:   SUMER_SEARCH   
;   PURPOSE:
;          Widget-based Search for SUMER fits files
;   CALLING SEQUENCE: 
;          SUMER_SEARCH, filelist, catalog
;   INPUTS :
;          None
;   OPTIONAL OUTPUTS :
;          FILELIST :  an array of file names
;          CATALOG  :  an array of strings for basic information
;         
;   KEYWORDS :
;          None
;   COMMON BLOCKS
;          SUMER_SEARCH_BLOCK
;   REMARKS:
;          System variable !sumer_config should be difined.
;          To manipulate the variable, use the program sumer_config
;          
;          
;   REQUIRED SUNBROUTINES:
;         SUMER_SEARCH_MAIN_EVENT SUMER_SEARCH_PD_EVENT
;   MODIFICATION HISTORY
;          March 1997      Jongchul Chae  version 1.5 
;          November 1998   Jongchul Chae  version 2.0                  
;- 

PRO sumer_search,filelist, cat
;
@sumer_search_block

;---------------------------------------------------------------------------
;  Set font
;---------------------------------------------------------------------------
   bfont = '-adobe-courier-bold-r-normal--20-140-100-100-m-110-iso8859-1'
   bfont = (get_dfont(bfont))(0)

   lfont = '-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1'
   lfont = (get_dfont(lfont))(0)
   IF lfont EQ '' THEN lfont = 'fixed'

   lfont2 = '-misc-fixed-bold-r-normal--15-140-75-75-c-90-iso8859-1'
   lfont2 = (get_dfont(lfont2))(0)
   IF lfont2 EQ '' THEN lfont2 = 'fixed'

;---------------------------------------------------------------------------
bfont=lfont

;  Initialize SUMER Configuration

defsysv, '!sumer_config', exist=exist

stop,1
if not exist then sumer_config_def

;

widget_control, /hourglass
files=''
file=''
catalog=''
fits_kind='SUM_'
time1 = '960805_000000'
time2 = '960805_240000'
xcen1 = -2000.0
xcen2 =  2000.0
ycen1 = -2000.0
ycen2 =  2000.0
rcen1 =  0.
rcen2 =  2000.0 
detector =''
wl1 = 600.
wl2 =1700.
exposure1=0.
exposure2=1.e5
object=''
pop=''
 slit=''

seq_type=''
studyname=''
scientist=''
condition2=1
done=0

log_dir=!sumer_config.aux_dir


  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }

  base_main = WIDGET_BASE(GROUP_LEADER=Group,  ROW=30, MAP=1, $
      TITLE='SUMER  Search v2.0'+ '  (Contact J. Chae at chae@bbso.njit.edu)')

  tmp = [ $
      { CW_PDMENU_S,       0, 'Done' }, $ ;        0
      { CW_PDMENU_S,       1, 'Config'}, $ 
      { CW_PDMENU_S,       0, 'Default'}, $
      { CW_PDMENU_S,       2, 'Display/Change'}, $
      { CW_PDMENU_S,       0, 'Search' }, $ ;  
  ;    { CW_PDMENU_S,       0, 'Refresh' }, $ ; 
      { CW_PDMENU_S,       1, 'Sort'}, $
      { CW_PDMENU_S,        0, 'by Study Name'}, $
      { CW_PDMENU_S,        0, 'by Wavelength'}, $     
      { CW_PDMENU_S,        2, 'by Time'}, $
      { CW_PDMENU_S,        0, 'Select All'},$
      { CW_PDMENU_S,        0, 'De-select All'},$ 
     ; { CW_PDMENU_S,       0, 'Make Catalog'}, $
      { CW_PDMENU_S,       1, 'Data' }, $  
      { CW_PDMENU_S,        0, 'Show Primary Header'}, $
      { CW_PDMENU_S,        0, 'Show Binary Header'},$
      { CW_PDMENU_S,        0, 'Get Data'},$ 
      { CW_PDMENU_S,       2,  'Look at Data'} $       
     ]


  tmp = CW_PDMENU( base_main, tmp, /RETURN_FULL_NAME, $
         UVALUE='PDMENU', font=bfont)
  



  TEXT_ID = widget_text(base_main, xsize=80, ysize=5,  $
             value = [ '', $
      '    Welcome to SUMER_SEARCH. ' , '', $
      ' Specify the selection criteria in bellow and ', $
         ' click the SEARCH button in the above.', replicate('', 50)] $
       , font=lfont, /scroll)
  list_id =widget_list( base_main, xsize=80, ysize=10, uvalue='LIST',font=lfont)


  tmp = WIDGET_LABEL( base_main, VALUE=' Requisite Criteria ', $
                     frame=4, font=bfont)

  tmp = WIDGET_LABEL( base_main, $
           VALUE='1. Kind of SUMER FITS Files', font=bfont)

  tmp = [  'standard',  'flat', 'history', 'rear slit',  'full disk' ]
  tmp = CW_BGROUP( base_main, tmp, COLUMN=5,  EXCLUSIVE=1, $
      UVALUE='FITS_KIND', set_value=0, font=bfont)

  tmp = WIDGET_LABEL( base_main, $
      VALUE='2. Observing Date and Time (yymmdd_hhmmss)', font=bfont)

  BASE = WIDGET_BASE(base_main, $
      col=2,  MAP=1)
   
  
  tmp = CW_FIELD( BASE,VALUE=time1,  ROW=1,  string=1, all_EVENTS=1, $
      FRAME=0,  TITLE=' From ', UVALUE='TIME1',  XSIZE=13, font=bfont)

  tmp = CW_FIELD( BASE,VALUE=time2, ROW=1, string=1, all_EVENTS=1, $
      FRAME=0, TITLE='to ', UVALUE='TIME2', XSIZE=13, font=bfont)

  tmp = WIDGET_LABEL( base_main, $
      VALUE='3. Center of Observation Field of View (arcseconds)', font=bfont)

  BASE = WIDGET_BASE(base_main, $
      col=3, $
      MAP=1, $
      UVALUE='BASE71')
  base1 = widget_base(base, row=1, map=1)
 
   tmp = CW_FIELD( BASE1,VALUE=string(xcen1, format='(i5)'), $
      ROW=1, FLOAT=1, all_EVENTS=1, TITLE='X:', UVALUE='XCEN1', $
      xsize=7, font=bfont)

  
   tmp = CW_FIELD( BASE1,VALUE= string(xcen2, format='(i5)'), $
      ROW=1,  FLOAT=1,  all_EVENTS=1,  TITLE='to',  UVALUE='XCEN2', $
      xsize=7, font=bfont)

   base1 = widget_base(base, row=1, map=1)


    tmp = CW_FIELD( BASE1,VALUE=string(ycen1, format='(i5)'), $
      ROW=1,  FLOAT=1,  all_EVENTS=1, TITLE='Y: ', UVALUE='YCEN1', $
      xsize=7, font=bfont)

    tmp = CW_FIELD( BASE1,VALUE= string(ycen2, format='(i5)'), $
      ROW=1,  FLOAT=1, all_EVENTS=1, TITLE='to', $
      UVALUE='YCEN2',  xsize=7, font=bfont)

 base1 = widget_base(base, row=1, map=1)

   tmp = CW_FIELD( BASE1,VALUE=string(rcen1, format='(i5)'), $
      ROW=1, FLOAT=1, all_EVENTS=1, TITLE='R: ', UVALUE='RCEN1', $
      xsize=7, font=bfont)

  
   tmp = CW_FIELD( BASE1,VALUE= string(rcen2, format='(i5)'), $
      ROW=1,  FLOAT=1,  all_EVENTS=1,  TITLE='to',  UVALUE='RCEN2', $
      xsize=7, font=bfont)


  tmp = WIDGET_LABEL( base_main, $
      VALUE='4. 1st Order Wavelength (Angstrom)', font=bfont)

  BASE = WIDGET_BASE(base_main, $
      col=2,  MAP=1)
   
  
  tmp = CW_FIELD( BASE,VALUE=string(wl1, format='(f7.1)'), $
       ROW=1,  float=1, all_EVENTS=1, $
      FRAME=0,  TITLE=' From ', UVALUE='WL1',  XSIZE=7, font=bfont)

  tmp = CW_FIELD( BASE,VALUE=string(wl2,format='(f7.1)'), $
       ROW=1, float=1, all_EVENTS=1, $
      FRAME=0, TITLE='to ', UVALUE='WL2', XSIZE=7, font=bfont)

  tmp = WIDGET_LABEL( base_main, $
      VALUE='5. Exposure Time (seconds)', font=bfont)

   BASE = WIDGET_BASE(base_main, $
      col=2,  MAP=1)
   
  
  tmp = CW_FIELD( BASE,VALUE=string(exposure1, format='(f8.1)'), $
       ROW=1,  float=1, all_EVENTS=1, $
      FRAME=0,  TITLE=' From ', UVALUE='EXPOSURE1',  XSIZE=8, font=bfont)

  tmp = CW_FIELD( BASE,VALUE=string(exposure2,format='(f8.1)'), $
       ROW=1, float=1, all_EVENTS=1, $
      FRAME=0, TITLE='to ', UVALUE='EXPOSURE2', XSIZE=8, font=bfont)




  


 



  
 tmp = WIDGET_LABEL( base_main, $
       VALUE='Optional Criteria : case-insensitive ', $
             frame=4, font=bfont)
base = widget_base(base_main, col=3)

   tmp = cw_field(base, value=detector, $
      row=1, string=1, all_events=1, $
        title='- Detector(A/B) ? ', uvalue='DETECTOR', xsize=1,font=lfont)
   tmp = cw_field(base,  value=slit ,  $
       row=1,  string=1, all_events=1, $
      title='- Slit  Number ? ', uvalue='SLIT', xsize=1, font=lfont)
   tmp = cw_field(base, title='- POPUDP  Number? ', $
        row=1, value=pop,  string=1, all_events=1, $
        uvalue='POPUDP', xsize=15, font=lfont)
 
 
  base = widget_base(base_main, col=2)

  tmp =cw_field(base, title='- Scientist? ', $
       row=1,  value=scientist, string=1, $
       all_events=1, uvalue='SCIENTIST', font=lfont, xsize=15)
 
  tmp =cw_field(base, title='- Study Name? ', $
       row=1, value=studyname,  string=1, $
       all_events=1,   uvalue='STUDY', font=lfont, xsize=15)

  tmp =cw_field(base, title='- Object? ', $
       row=1,  value=object, string=1, $
       all_events=1,  uvalue='OBJECT', font=lfont, xsize=15)
 
  tmp = cw_field(base, title='- Observing Sequence? ', $
        row=1,   value=seq_type,  string=1, $
        all_events=1, uvalue='SEQUENCE',  font=lfont,xsize=15)  
  


  WIDGET_CONTROL, base_main, /REALIZE

  XMANAGER , 'SUMER_SEARCH_MAIN', base_main
  ss=where(select eq 1, count)
  if count ge 1 then filelist=files(ss) else filelist=''
  if count ge 1 then cat=catalog(ss) else cat=''
END
