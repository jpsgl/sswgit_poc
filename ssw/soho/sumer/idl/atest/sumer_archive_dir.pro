function sumer_archive_dir, year, month, os=os
;+
;  NAME: SUMER_ARCHIVE_DIR
;  Purpose: 
;           Determine a SUMER data directory in SOHO-archive
;  CALLING SEQUENCE:
;           dir = SUMER_ARCHIVE_DIR(YEAR, MONTH)
;  INPUT:   YEAR (integer value: e.g., 1996 or 96)
;           MONTH (integer value: 1,2,.., 12)
;  KEYWORD:
;           OS   operating system (either 'vms' or 'unix')
;                if not specfied, it is set to the machine OS.
;
;  HISTORY
;         1998 November  Chae
;-
if n_elements(os) eq 0 then begin
os=!version.os_family
endif 

smonths = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', $
          'jul', 'aug','sep','oct', 'nov', 'dec']
smonth = smonths(month-1)
syear = string((year mod 100)/10, format='(i1)')+ $
        string(year mod 10, format='(i1)')

dir=''
if os eq  'unix'  then   case smonth+syear of
    'jan96': dir = '/soho-arch01/private/data/processed/sumer/fits96/january'
    'feb96': dir = '/soho-arch01/private/data/processed/sumer/fits96/february'
    'mar96': dir = '/soho-arch01/private/data/processed/sumer/fits96/march'
    'apr96': dir = '/soho-arch01/private/data/processed/sumer/fits96/april'
    'may96': dir = '/soho-arch01/private/data/processed/sumer/fits96/may'
    'jun96': dir = '/soho-arch01/private/data/processed/sumer/fits96/june'
    'jul96': dir = '/soho-arch06/private/data/processed/sumer/fits96/tmpjul96'
    'aug96': dir = '/soho-arch06/private/data/processed/sumer/fits96/tmpaug96'
    'sep96': dir = '/soho-arch06/private/data/processed/sumer/fits96/tmpsep96'
    'oct96': dir = '/soho-arch06/private/data/processed/sumer/fits96/tmpoct96'
    'nov96': dir = '/soho-arch06/private/data/processed/sumer/fits96/tmpnov96'
    'dec96': dir = '/soho-arch06/private/data/processed/sumer/fits96/tmpdec96'
    'jan97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpjan97'
    'feb97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpfeb97'
    'mar97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpmar97'
    'apr97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpapr97'
    'may97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpmay97'
    'jun97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpjun97'
    'jul97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpjul97'
    'aug97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpaug97'
    'sep97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpsep97'
    'oct97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpoct97'
    'nov97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpnov97'
    'dec97': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpdec97'
    'feb98': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpfeb98'
    'apr98': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpapr98'
    'may98': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpmay98'
    'jun98': dir = '/soho-arch06/private/data/processed/sumer/fits97/tmpjun98'
     else  : dir = ''
   endcase          
if os eq  'vms'  then   case smonth+syear of
    'jan96': dir = 'arfits96:[january]'
    'feb96': dir = 'arfits96:[february]'
    'mar96': dir = 'arfits96:[march]'
    'apr96': dir = 'arfits96:[april]'
    'may96': dir = 'arfits96:[may]'
    'jun96': dir = 'arfits96:[june]'
    'jul96': dir = 'sohoarch_fits3:[fits96.tmpjul96]'
    'aug96': dir = 'sohoarch_fits3:[fits96.tmpaug96]'
    'sep96': dir = 'sohoarch_fits3:[fits96.tmpsep96]'
    'oct96': dir = 'sohoarch_fits3:[fits96.tmpoct96]'
    'nov96': dir = 'sohoarch_fits3:[fits96.tmpnov96]'
    'dec96': dir = 'sohoarch_fits3:[fits96.tmpdec96]'
    'jan97': dir = 'sohoarch_fits3:[fits97.tmpjan97]'
    'feb97': dir = 'sohoarch_fits3:[fits97.tmpfeb97]'
    'mar97': dir = 'sohoarch_fits3:[fits97.tmpmar97]'
    'apr97': dir = 'sohoarch_fits3:[fits97.tmpapr97]'
    'may97': dir = 'sohoarch_fits3:[fits97.tmpmay97]'
    'jun97': dir = 'sohoarch_fits3:[fits97.tmpjun97]'
    'jul97': dir = 'sohoarch_fits3:[fits97.tmpjul97]'
    'aug97': dir = 'sohoarch_fits3:[fits97.tmpaug97]'
    'sep97': dir = 'sohoarch_fits3:[fits97.tmpsep97]'
    'oct97': dir = 'sohoarch_fits3:[fits97.tmpoct97]'
    'nov97': dir = 'sohoarch_fits3:[fits97.tmpnov97]'
    'dec97': dir = 'sohoarch_fits3:[fits97.tmpdec97]'
    'feb98': dir = 'sohoarch_fits3:[fits97.tmpfeb98]'
    'apr98': dir = 'sohoarch_fits3:[fits97.tmpapr98]'
    'may98': dir = 'sohoarch_fits3:[fits97.tmpmay98]'
    'jun98': dir = 'sohoarch_fits3:[fits97.tmpjun98]'
     else  : dir = ''
   endcase
return, dir
end    
