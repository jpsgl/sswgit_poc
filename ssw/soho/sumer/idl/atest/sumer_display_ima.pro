;+
;
; NAME :  
;        SUMER_DISPLAY_IMA
; PURPOSE :
;        Display raster image (only for XRASTER)
; CALIING SEQUENCE :
;        SUMER_DISPLAY_IMA  [,ps=ps]
; INPUTS :
;        None
; OUPUTS :
;        None
; COMMON BLOCKS
;        XRASTER_WIDGET_BLOCK  XRASTER_DRAW_BLOCK
;        RDSUMEREXT_BLOCK      DROP_INDEX_BLOCK DRAW_BLOCK
;        XLOADCT_COM    MONOCOLORS_BLOCK
;        IDL_vector_font_comm
; KEYWORDS :
;        PS    If set to have non-zero value, the image is
;              displayed on POSTCRIPT device. 
; SIDE EFFECTS:
;        A Raster image is displayed either on X-window or POSTSCRIPT
; REQUIRED ROUTINES:
;        GET_IMAGE   HARDCOPY_MESSAGE       
;
; MODIFICATION HISTORY
;        March 1997    Jongchul Chae
;
;-
           
        
;        
pro   sumer_display_ima, ps=ps, annotate=annotate
common sumer_tool_block, xras, dstr
@monocolors_block
;@xloadct_com
par=xras.par
;ncolor1=!d.table_size

common IDL_vector_font_comm, draw_id, fontlist,idl_font
char0=!p.charsize
if n_elements(vtop) eq 0 then vtop=100.
white_in_normal = fix((ncolor1-1)/100.*vtop)
x=par.x_curr(0)
y=par.y_curr(0)


delta=dstr.deltas(*,par.icol(0))
xdel=delta(2)
ydel=delta(1) 

y1=par.roi_space(1)
y2=par.roi_space(3)
x1=par.roi_space(0)
x2=par.roi_space(2)

image = line_par_conv($
          par.data_fit(*,y1:y2,x1:x2,par.icol(0)), $
          dstr.refpos(0,par.icol(0)), $  
          dstr.deltas(0,par.icol(0)), (par.rest_pixel(x1:x2, par.icol(0))))

intensity =rotate(reform(image(1,*,*)),3)


xdim_roi=par.roi_space(2)-par.roi_space(0)+1
ydim_roi=par.roi_space(3)-par.roi_space(1)+1
max = xras.imaplot.maxv(xras.par.ipar(0), xras.par.icol(0))
min = xras.imaplot.minv(xras.par.ipar(0), xras.par.icol(0))
case par.ipar(0)+1 of
   1: begin
       image= intensity >1.<1.e8
         image1 = alog10(image)
         stitle='LOG (COUNTS)'
       title=dstr.data_names(par.icol(0))+' Intensity'
       end
  4: begin
    image =rotate(reform(image(0,*,*)),3)>0.001
   image1=alog10(image)
title=dstr.data_names(par.icol(0))+' Continuum'
stitle='LOG(COUNTS)'
    end
   else : begin

    image1 =rotate(reform(image(par.ipar(0)+1,*,*)),3)
     ff=par.xbin*par.ybin

    m= median(image1(where(intensity*ff ge 50.)))
       ss=where(intensity*ff lt 30., count)
    if count ge 1 then image1(ss) = m
       stitle='km/s'
      if par.ipar(0)+1 eq 2 then title=dstr.data_names(par.icol(0))+' Shift' 
      if par.ipar(0)+1 eq 3 then title=dstr.data_names(par.icol(0))+' Width' 
       end
endcase

if xras.par.flip_ns eq -1 then image1=rotate(image1,7)
if xras.par.flip_ew eq -1 then image1=rotate(image1, 5)

xsize_image = fix(xdim_roi*par.mag_x)>1
ysize_image =fix(ydim_roi*par.mag_y)>1
image1=congrid(image1, xsize_image, ysize_image )
;print, par.mag_y, par.mag_x, par.mag_w
plot_color=white_in_normal
if keyword_set(ps) then begin
  

dev_factor = (20.*1000/(xsize_image+2*par.xoff_dev))  $
           < (23.*1000/(ysize_image+2*par.yoff_dev))
 
xsize_cm  =(par.xoff_dev*2+xsize_image)*dev_factor/1000.
ysize_cm =(par.yoff_dev*2+ysize_image)*dev_factor/1000.
case par.ipar(0) of 
      3 :  kind='_b'+strtrim(string(par.icol(0)),2)
      0 : kind='_i'+ strtrim(string(par.icol(0)),2)
      1 : kind='_v'+ strtrim(string(par.icol(0)),2)
      2 :  kind='_w'+ strtrim(string(par.icol(0)),2)
endcase
xps_setup, ps_setup, /initial
ps_setup.printer='lpr -h '
cd, current=current
ps_setup.filename=concat_dir(current,'idl.ps')
ps_setup.hard=0
ps_setup.portrait=1
xps_setup, ps_setup
set_plot, 'ps'
;plot_style, 1
device, encapsulated = ps_setup.encapsulated, $
        portrait = ps_setup.portrait, $
        color=ps_setup.color, $
        filename=ps_setup.filename
device, bits_per_pixel=8, xoff=0.5, yoff=0.5, $
       xsize= xsize_cm, $
       ysize= ysize_cm 
device, /bold, /times, /isolatin1 
;plot_color=0
       
endif  else begin
widget_control, xras.widget.draw_ima, get_value=DRAW_IMA_id
                 wset, DRAW_IMA_id
                 erase
                 
dev_factor=1.
;plot_style, 0
plot_color=!d.table_size-1
endelse
 
char_dev_unit=vdf_height(!d.name)
pos = [par.xoff_dev, par.yoff_dev, xsize_image+par.xoff_dev, $
       ysize_image+par.yoff_dev]*dev_factor
char0=!p.charsize
!p.charsize = char0* $
    ((pos(2)+Pos(0)) >(pos(3)+pos(1)))/char_dev_unit/50.
     

  tv,  bytscl(image1, min, max, top=ncolor1-1), $
       pos(0), pos(1), xs=pos(2)-pos(0)+1, ys=pos(3)-pos(1)+1, dev=1

roi=par.roi_space
xr0_roi=[ par.xval(roi(0),par.icol(0)),par.xval(roi(2),par.icol(0))]
yr0_roi=[par.yval(roi(3),par.icol(0)), par.yval(roi(1), par.icol(0))]
yr=[yr0_roi(0) , yr0_roi(1)]
if xras.par.flip_ns eq -1 then  yr=rotate(yr,2)
xr=[xr0_roi(0), xr0_roi(1)]
if xras.par.flip_ew eq -1 then xr=rotate(xr,2)
 plot,  xr0_roi, yr0_roi,  xr=xr, yr=yr,$
  xst=1, yst=1, /nodata, /noerase, title=title, ticklen=-0.02, $
 position=pos, /dev, xtitle=par.xtitle, ytitle=par.ytitle, font=-1
  

if !d.name eq 'X' and keyword_set(annotate) then annotate 
!p.charsize=char0         

if keyword_set(ps) then begin

if 0 then begin
if (x-par.roi_space(0))*(x-par.roi_space(2)) le 0. and $
   (y-par.roi_space(1))*(x-par.roi_space(3)) then $
    plots, par.xval(x), par.yval(y), psym=1, symsize=2.0, thick=2.

set_plot, 'x'
base=widget_base(title='Image')


pos=pos/dev_factor*2
xsize=xsize_cm*1000/dev_factor*2
ysize=ysize_cm*1000/dev_factor*2    
draw=widget_draw(base,xsize=xsize, ysize=ysize, $
     button_events=1, motion_events=1)
widget_control,  base, /realize
widget_control, draw, get_value=draw_win
wset, draw_win
char_dev_unit=vdf_height(!d.name) 
;char0=!p.charsize
!p.charsize = char0* $
    ((pos(2)+Pos(0)) >(pos(3)+pos(1)))/char_dev_unit/50.

tv, replicate(!d.table_size-1, xsize, ysize)

color0=!p.color
!p.color=0
;image_scale, image1, pos, minvalue=min, maxvalue=max, $
;    maxbyte=ncolor1-1, stitle=stitle, no_scale=1
tv,  bytscl(image1, min, max, top=ncolor1-1), $
       pos(0), pos(1), xs=pos(2)-pos(0)+1, ys=pos(3)-pos(1)+1, dev=1


plot,  xr0_roi, yr0_roi, xr=xr, yr=yr, $
xst=1, yst=1, /nodata, /noerase, color=0.,title=title,   $
position=pos, /dev,  xtitle=par.xtitle, ytitle=par.ytitle, font=0


!p.color=color0
widget_control, base, /destroy
endif
set_plot, 'ps


device, /close
set_plot, 'x'
if ps_setup.hard then spawn, ps_setup.printer+ ' '+ ps_setup.filename
endif else begin
xmark = (x - xras.par.roi_space(0))*xras.par.mag_x+xras.par.xoff_dev
ymark = (-y+ xras.par.roi_space(3))*xras.par.mag_y+xras.par.yoff_dev
mark_on_image, xmark, ymark , color=green, /mark_only
endelse
 
pos=strpos(strlowcase(dstr.file(0)), 'sum_')
file1=strmid(dstr.file(0), pos, 21)
widget_control, xras.widget.TEXT_A, set_value='File :  ' +file1+ ',  Extension :  ' +$
          strtrim(string(dstr.extension),2) $
         +'/'+strtrim(string(dstr.next),2)

;plot_style,0
!p.charsize=char0
end


