;+
; Project     : SOHO - SUMER
;
; Name        : MK_SUMER_DBASE
;
; Purpose     : Make site database of SUMER FITS file locations
;
; Category    : analysis,planning
;
; Explanation : 
;
; Syntax      : IDL> mk_sumer_dbase,root,dbase,ext=ext,direc=direc
;
; Inputs      : ROOT = root or top directory containing FITS files
;                      (SUMER_FITS_DATA is checked if root is not given)
;
; Opt. Inputs : None
;
; Outputs     : DBASE = array of {file: FITS filename (with path);
;                                 time: FITS obs time (UTC)}
;
; Opt. Outputs: None
;
; Keywords    : EXT = FITS file extensions to search for (def =['.fits',',fts'])
;               DIREC = output list of directories searched under root. 
;                       
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-NOV-1998,  D.M. Zarro (SM&A),  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-
 

pro mk_sumer_dbase,root,dbase,ext,direc=direc

if datatype(ext) ne 'STR' then ext=['.fts','.fits']
if datatype(root) ne 'STR' then root='SUMER_FITS_DATA'
sroot=chklog(root)
if sroot(0) eq '' then begin
 message,'Need to define FITS directory SUMER_FITS_DATAxx',/cont
 return
endif

next=n_elements(ext)
template={file:'',time:0.d}
delvarx,dbase
nroot=n_elements(sroot)
for j=0,nroot-1 do begin
 all_dir=find_all_dir('+'+sroot(j))
 ndir=n_elements(all_dir)
 for i=0l,ndir-1 do begin
  message,'searching '+all_dir(i),/cont
  for k=0l,next-1 do begin
   list=loc_file(concat_dir(all_dir(i),'*'+ext(k)),count=count)
   if count gt 0 then begin
    times=fid2time(list,/tai)
    temp=replicate(template,count)
    temp.file=str_replace(list,sroot(j),root(j))
    temp.time=times
    dbase=merge_struct(dbase,temp)
   endif
  endfor
 endfor
 out_dir=append_arr(out_dir,all_dir)
endfor

direc=out_dir
return & end


