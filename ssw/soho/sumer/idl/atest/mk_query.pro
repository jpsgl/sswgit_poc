
;+
; NAME:
;
; mk_query
;
; PURPOSE:
;
; To retrieve information about data stored in a SUMER/SOHO FITS file
;
; CATEGORY:
;
; Data retrieval
;
; CALLING SEQUENCE:
;
; mk_query,filename,query
; 
; INPUTS:
;
; filename: The SUMER/FITS file that is to be examined.
;
; OPTIONAL INPUTS:
;
; None
;       
; KEYWORD PARAMETERS:
;
; silent : Suppresses output to the screen (the default).
;
; ext_max: Sets the maximum number of binary table extensions in the
;          file to be examined. Large numbers of BTE's (some files
;          seem to have hundreds) take a very long time to read.
;
; OUTPUTS:
;
; A description of the data stored in the FITS file is printed to the
; screen. Currently the colum name and dimensions are given as well as
; the maximum and minimum of the wavelength range and pointing.
;
; OPTIONAL OUTPUTS:
;
; query: A structure containing a description of the data stored in
;        the FITS file. An array of query structures will be returned
;        if there is more than one column.
;
;        {name:                   '',$   ;; name of the column
;         extension:               0,$   ;; binary table extension number
;         column:                  0,$   ;; column number
;         dims:            intarr(3),$   ;; dimensions of the column
;         dims_str:               '',$   ;; dimensions of the column
;         solar_x_min:           0.0,$   ;; min x pointing of the column
;         solar_x_max:           0.0,$   ;; max x pointing of the column
;         solar_y_min:           0.0,$   ;; min y pointing of the column
;         solar_y_max:           0.0,$   ;; max y pointing of the column
;         wave_min:              0.0,$   ;; min wavelength of the column
;         wave_max:              0.0}    ;; max wavelength of the column
;
; status: TRUE if the input file appears to contain valid data, FALSE
;         if some pathology has arisen.
;
; COMMON BLOCKS:
;
; None
;
; SIDE EFFECTS:
;
; ?
;
; RESTRICTIONS:
;
; All columns in the binary table extension except for those with
; TTYPE keywords sum_status, del_time, exptime, solar_x, and solar_y
; are considered to be data from the detector.
;
; PROCEDURE:
;
; Information is gathered from information in the binary table headers
; as well as from the solar_x and solar_y data columns.
;
; EXAMPLE:
;
; IDL> filename = 'sum_960513_085055.fits'
; IDL> mk_query,filename,query
; File       : sum_960513_085055.fits
; Detector   : A
; Slit       : <2> 1.0 * 300 centered
; MAX Exptime: 100.0 sec
; MIN Exptime: 100.0 sec
; C      Name    Dimensions    Wmin    Wmax     Xmin     Xmax     Ymin     Ymax
; 1 W_937.803    (50,360,5)  936.69  938.91    -1.50     4.50   400.12   400.12
; 2 W_933.380    (50,360,5)  932.27  934.49    -1.50     4.50   400.12   400.12
; 3 W_930.748    (50,360,5)  929.64  931.86    -1.50     4.50   400.12   400.12
; 4 W_926.226    (50,360,5)  925.11  927.34    -1.50     4.50   400.12   400.12
; 5 W_909.000    (50,360,5)  907.89  910.11    -1.50     4.50   400.12   400.12
; 6 W_944.520    (50,360,5)  943.41  945.63    -1.50     4.50   400.12   400.12
; IDL> help,/str,query
; ** Structure <21f720>, 24 tags, length=116, refs=1:
;   NAME            STRING    'W_937.803'
;   EXTENSION       INT              1
;   COLUMN          INT              1
;   DIMS            INT       Array(3)
;   DIMS_STR        STRING    '(50,360,5)'
;   SOLAR_X_MIN     FLOAT          -1.50000
;   SOLAR_X_MAX     FLOAT           4.50000
;   SOLAR_Y_MIN     FLOAT           400.125
;   SOLAR_Y_MAX     FLOAT           400.125
;   WAVE_MIN        FLOAT           936.691
;   WAVE_MAX        FLOAT           938.915
;   WAVE_MIN1       FLOAT           936.691
;   WAVE_MAX1       FLOAT           938.915
;   WAVE_MIN2       FLOAT           468.346
;   WAVE_MAX2       FLOAT           469.457
;   REF_PIX         INT            715
;   REF_VAL         FLOAT           937.803
;   DELTA           FLOAT         0.0444715
;   DETECTOR        STRING    'A'
;   MAX_EXPTIME     FLOAT           100.000
;   MIN_EXPTIME     FLOAT           100.000
;   SLIT            STRING    '<2> 1.0 * 300 centered'
;   SLIT_N          INT              2
;   FILENAME        STRING    'sum_960513_085055.fits'
;
; Note that query is an array of structures with six elements.
;
; MODIFICATION HISTORY:
;
;   HPW 02-JUL-1996: Orginal version.  
;
;   HPW 24-JUL-1996: Fixed a bug which prevented the routine from
;                    properly reading full sun, rear slit camera, and
;                    history memory files. Modified solar_y output.
;
;   HPW 29-JUL-1996: Changed the way detector data is identified in
;                    the binary table extension columns (see
;                    RESTRICTIONS), added status keyword, added code
;                    to handle files without values for important
;                    keywords, and fixed a bug which caused file units
;                    to remain open.
;
;   HPW 22-AUG-1996: Added the ext_max keyword which sets the upper
;                    limit on the number of binary table extensions to
;                    be read.
;
;   HPW 09-JUN-1997: Added work-around for files missing the EXTNAME
;                    keyword in the header.
;
;   HPW 20-JUN-1997: Modified to look for TSPIX first then
;                    TRPIX. TSPIX is used as the reference pixel in
;                    the final cdrom distribution of the data.
;
;   HPW 18-JUL-1997: Modified to use mk_query_genx if genx filenames are
;                    passed.
; 
;   HPW 08-AUG-1997: Modified to return the same structure as
;                    mk_query_genx and to allow for multiple filenames
;                    through the use of recursion.
;
;   HPW 04-FEB-1998: Modified to return the output as a string. Useful
;                    for printing the contents of files.
;
;   HPW 27-MAR-2002: Fixed bug related to TTYPE having null strings as
;                    entries. Introduced a new bug?
;
;-

pro mk_query,filename,output_query,n_ext=n_ext,silent=silent,status=status,$
             ext_max=ext_max,header=header

TRUE   = 1b
FALSE  = 0b
status = FALSE
output_query = 0

;; ----------------------------------------------------------------------
;; Check user input
;; ----------------------------------------------------------------------

nfiles = n_elements(filename) 

;; check for existence of files
for n=0,nfiles-1 do $
    if not(file_exist(filename(n))) then $
    message,"Specified file does not exist : "+filename(n)

;; check file types
filetypes = strarr(nfiles)
for n=0,n_elements(filename)-1 do filetypes(n) = sgt_file_type(filename(n))
u   = uniq(filetypes)
n_u = n_elements(u)
if n_u ne 1 then message,"Mixed file types. Specify either FITS or GENX files."

;; if files are genx files use mk_query_genx and return
filetype = (filetypes(u))(0)
if filetype eq "GENX" then begin
  mk_query_genx,filename,output_query,silent=silent,header=header
  return
endif

if (nfiles gt 1) then begin
  for n=0,nfiles-1 do begin
    mk_query,filename(n),struct,silent=silent,header=s
    if n eq 0 then qout = struct else qout = concat_struct(qout,struct)
    if n eq 0 then sout = s else sout = [sout,s]
  endfor
  output_query = qout
  header       = sout
  return
endif else filename = filename(0)

;; ----------------------------------------------------------------------
;; Read the primary header
;; ----------------------------------------------------------------------

;; read the primary header
header = headfits(filename)

;; check for the existence of binary table extensions
bin_ext = fxpar(header,'EXTEND')
if (bin_ext eq 0) then begin
  message,"No binary extensions found in file:",/CONTINUE
  message," "+filename,/CONTINUE
  return
endif

;; read the detector name
detector = fxpar(header,'DETECTOR')
pos      = strpos(detector,'>')
detector = strtrim((strmid(detector,pos+1,4)),2)

;; read the slit name
slit   = fxpar(header,'SLIT')
pos    = strpos(slit,'<')
slit_n = fix(strmid(slit,pos+1,1))
slit   = strtrim(slit,2)

;; ----------------------------------------------------------------------
;; Determine the number of binary table extensions
;; ----------------------------------------------------------------------

;; open the first binary table extension
extension = 1
header    = headfits(filename,exten=extension)

;; determine the number of binary table extensions
n_ext = fxpar(header,'EXTNAME')
if (!err eq -1) then begin
  message,"The keyword EXTNAME is missing from the header.",/CONTINUE
  message,"The actual number of binary tables can not be determined.",/CONTINUE
  message,"WARNING: One binary table is being assumed.",/CONTINUE
  n_ext = 1
endif else begin
  len   = strlen(n_ext)
  pos   = strpos(n_ext,'/')
  n_ext = fix(strmid(n_ext,pos+1,len-pos))  
endelse

;; set an upper limit on the number of bte's to be examined
if keyword_set(ext_max) then begin
  if (n_ext gt ext_max) then n_ext = ext_max
endif

;; ----------------------------------------------------------------------
;; Define a structure to hold the query results
;; ----------------------------------------------------------------------

q_struct = {name:		  '',$	 ;; name of the column
            extension:		   0,$   ;; binary table extension number
            column:		   0,$   ;; column number
            dims:   	   intarr(3),$   ;; dimensions of the column
            dims_str: 	          '',$   ;; dimensions of the column
            solar_x_min:         0.0,$   ;; min x pointing of the column
            solar_x_max:         0.0,$   ;; max x pointing of the column
            solar_y_min:         0.0,$   ;; min y pointing of the column
            solar_y_max:         0.0,$   ;; max y pointing of the column
            wave_min:		 0.0,$   ;; min wavelength of the column
            wave_max:		 0.0,$   ;; max wavelength of the column
            ;; EXTENSIONS TO ORIGINAL QUERY STRUCTURE
            wave_min1:		 0.0,$   ;; min first-order wavelength
            wave_max1:		 0.0,$   ;; max first-order wavelength
            wave_min2:		 0.0,$   ;; min second-order wavelength
            wave_max2:		 0.0,$   ;; max second-order wavelength
            ref_pix:               0,$   ;; reference pixel
            ref_val:             0.0,$   ;; reference pixel
            delta:               0.0,$   ;; reference pixel
            detector:             '',$   ;; detector
            max_exptime:         0.0,$   ;; max exposure time
            min_exptime:         0.0,$   ;; min exposure time
            slit:                 '',$   ;; slit
            slit_n:                0,$   ;; slit
            filename:             '',$   ;; absolute location of the file
            file_name:            ''}

;; ----------------------------------------------------------------------
;; Read information about data from the binary table extensions
;; ----------------------------------------------------------------------

for i=1,n_ext do begin

  ;; open the binary table extension
  fxbopen,unit,filename,i,header

  ;; --------------------------------------------------------------------
  ;; Use the keyword TTYPE to determine how many columns in the binary
  ;; table extension have detector data in them.
  ;; --------------------------------------------------------------------

  ;; check for the keyword TTYPE
  fxbfind,unit,'TTYPE',columns,values,n_found
  values = strtrim(values,2)

  ;; for pathological columns with no name
  noname = where(values eq '',n_noname)
  if (n_noname gt 0) then values(noname) = 'No_Name'

  ;; except for the following, all data columns are detector data
  ss1 = 'SUM_STATUS' & ss2 = 'DEL_TIME' & ss3 = 'EXPTIME'
  ss4 = 'SOLAR_X' & ss5 = 'SOLAR_Y'
  dd  = where((strpos(values,ss1) eq -1) and $
              (strpos(values,ss2) eq -1) and $
              (strpos(values,ss3) eq -1) and $
              (strpos(values,ss4) eq -1) and $              
              (strpos(values,ss5) eq -1),n_data)
   
  ;; create an array of structures to hold query data for the column
  query_c = replicate(q_struct,n_data) 

  ;; --------------------------------------------------------------------
  ;; Names of the columns
  ;; --------------------------------------------------------------------
  
  name = strtrim(values(dd),2)
  if (n_elements(name) eq 1) then name = name(0)
  query_c(*).name = name
  
  ;; --------------------------------------------------------------------
  ;; The binary table extension and column numbers
  ;; --------------------------------------------------------------------

  query_c(*).extension = i
  column = fix(dd) + 1
  if (n_elements(column) eq 1) then column = column(0)
  query_c(*).column = column

  ;; --------------------------------------------------------------------
  ;; Dimensions of the columns
  ;; --------------------------------------------------------------------

  fxbfind,unit,'TDIM',columns,values,n_found
  dims_str = strcompress(values(dd),/remove_all)
  if (n_elements(dims_str) eq 1) then dims_str = dims_str(0)
  query_c(*).dims_str = dims_str
  for j=0,n_data-1 do query_c(j).dims = fix(destring(dims_str(j)))

  ;; --------------------------------------------------------------------
  ;; Wavelength Range and other data
  ;; --------------------------------------------------------------------

  ;; reference pixel, reference value, and delta
  fxbfind,unit,'TSPIX',columns,rpix_values,n_found
  if n_found eq 0 then fxbfind,unit,'TRPIX',columns,rpix_values,n_found
  fxbfind,unit,'TRVAL',columns,rval_values,n_found
  fxbfind,unit,'TDELT',columns,delt_values,n_found

  ;; exposure times
  fxbfind,unit,'TTYPE',columns,values,n_found
  values = strcompress(values,/REMOVE_ALL)
  match = where(values eq 'EXPTIME')
  fxbread,unit,exptime_data,columns(match(0))

  ;; some pathological files have no values for RVAL in the header!
  rpix_values = strtrim(rpix_values,2)
  rval_values = strtrim(rval_values,2)
  delt_values = strtrim(delt_values,2)

  mm_rpix = where(rpix_values eq '',count_rpix)
  mm_rval = where(rval_values eq '',count_rval)
  mm_delt = where(delt_values eq '',count_delt)

  if (count_rpix gt 0) then begin
    fxbclose,unit
    message,'No value for RPIX in BTE header.',/CONTINUE
    return
  endif
  if (count_rval gt 0) then begin
    fxbclose,unit
    message,'No value for RVAL in BTE header.',/CONTINUE
    return
  endif
  if (count_delt gt 0) then begin
    fxbclose,unit
    message,'No value for DELT in BTE header.',/CONTINUE
    return
  endif

  for j=0,n_data-1 do begin
    rpix = destring(rpix_values(j))
    rval = destring(rval_values(j))
    delt = destring(delt_values(j))

    if (query_c(j).dims(0) ne 1024) then begin
      query_c(j).wave_min = (rval(0) - query_c(j).dims(0)*delt(0)/2)
      query_c(j).wave_max = (rval(0) + query_c(j).dims(0)*delt(0)/2)
    endif else begin
      query_c(j).wave_min = (rval(0) - (rpix(0)+1)*delt(0))
      query_c(j).wave_max = (rval(0) + (1024-rpix(0))*delt(0))
    endelse

    ;; determine order of wavelengths from delta
    if delt(0) lt 0.04 then wave2firstorder  = 2 else wave2firstorder  = 1
    if delt(0) lt 0.04 then wave2secondorder = 1 else wave2secondorder = 0.5

    query_c(j).wave_min1 = query_c(j).wave_min*wave2firstorder
    query_c(j).wave_max1 = query_c(j).wave_max*wave2firstorder
    query_c(j).wave_min2 = query_c(j).wave_min*wave2secondorder
    query_c(j).wave_max2 = query_c(j).wave_max*wave2secondorder

    query_c(j).ref_pix  = rpix(0)
    query_c(j).ref_val  = rval(0)
    query_c(j).delta    = delt(0)
    query_c(j).slit_n   = slit_n
    query_c(j).slit     = slit
    query_c(j).detector = detector
    query_c(j).filename = filename

    ;; replace slashes with %
    file_name = filename
    file_name = str_replace(file_name,'/','%')
    file_name = str_replace(file_name,'\','%')
    file_name = str_sep(file_name,'%')
    file_name = file_name(n_elements(file_name)-1)
    query_c(j).file_name = file_name    

    query_c(j).max_exptime = max(exptime_data(j,*))
    query_c(j).min_exptime = min(exptime_data(j,*))
    
  endfor

  ;; --------------------------------------------------------------------
  ;; Pointing
  ;; --------------------------------------------------------------------

  fxbfind,unit,'TTYPE',columns,values,n_found
  values = strcompress(values,/REMOVE_ALL)

  ;; solar x pointing
  match = where(values eq 'SOLAR_X')
  fxbread,unit,solar_x,columns(match(0))
  for j=0,n_data-1 do begin
    query_c(j).solar_x_min = min(solar_x(j,*))
    query_c(j).solar_x_max = max(solar_x(j,*))
  endfor

  ;; solar y pointing
  match = where(values eq 'SOLAR_Y')
  fxbread,unit,solar_y,columns(match(0))
  for j=0,n_data-1 do begin
    query_c(j).solar_y_min = min(solar_y(j,*))
    query_c(j).solar_y_max = max(solar_y(j,*))
  endfor

  ;; --------------------------------------------------------------------
  ;; Concatenate query structure
  ;; --------------------------------------------------------------------

  if (i eq 1) then begin
    q = [query_c]
  endif else begin
    q = [q,query_c]
  endelse

  ;; close unit
  fxbclose,unit

endfor

;; ----------------------------------------------------------------------
;; Print query information to the screen
;; ----------------------------------------------------------------------

fn = filename
sl = strlen(filename)
if sl gt 50 then fn = "... "+strmid(filename,sl-50,50)
n_cols = n_elements(q) 
s = strarr(n_cols+7)

s(0) = 'File       : '+fn
s(1) = 'Detector   : '+q(0).detector
s(2) = 'Slit       : '+q(0).slit
s(3) = 'MAX Exptime: '+trim(max(q.max_exptime),'(f10.1)')+" sec"
s(4) = 'MIN Exptime: '+trim(min(q.min_exptime),'(f10.1)')+" sec"
s(5) = string(format='(a2,a10,a14,2a8,4a9)',$
              'C','Name','Dimensions','Wmin',$
              'Wmax','Xmin','Xmax','Ymin','Ymax')
for j=0,n_cols-1 do begin
  name = q(j).name
  if keyword_set(default_wave) then begin
    wmin = q(j).wave_min & wmax = q(j).wave_max
  endif else begin
    wmin = q(j).wave_min1 & wmax = q(j).wave_max1
  endelse
  if (strlen(name) ge 11) then name = strmid(name,0,6)+'...'
  s(j+6) = string(format='(i2,a10,a14,2f8.2,4f9.2)',j+1,name,$
                  q(j).dims_str,wmin,wmax,$
                  q(j).solar_x_min,q(j).solar_x_max,$
                  q(j).solar_y_min,q(j).solar_y_max)
endfor    
s(n_cols+6) = ' '
if not(keyword_set(silent)) then for j=0,n_elements(s)-1 do print,s(j)

if (n_elements(q) eq 0) then  print,'No data in the FITS file'

if (n_elements(q) ne 0) then begin
  output_query = q
  header       = s
  status       = TRUE
endif 

return
end
