;+
;
; NAME : 
;        SUMER_FILE
; PURPOSE :
;        Check and/or get SUMER data file  and produce its full filename
;       
;        
; CALLING SEQUENCE:
;        Result = SUMER_FILE(FILENAME)
; INPUTS:
;        FILENAME    SUMER file name (full or simple)
; KEYWORDS: 
;        STATUS       is set to 1 (success) or 0 (failure)
;        DIRECTORY    a directory or a string array of directories.
;                     If specifed, those directories are searched.
;        COPY          if set, the SUMER data file is copied into  the
;                     directory !sumer_config.data_dir either via FTP
;                     or a direct copy.           
;               
; REQUIRED_ROUTINES:
;       BREAK_FILE, SUMER_DIR
; 
; MODIFICATION HISTORY
;       March 1997  Jongchul Chae
;       April 1997  CHAE              : function name chnge
;       November 1998   CHAE
;       1 December 1998, Zarro (SM&A) - added SOHO-ARCHIVE check
;       18 December 1998, Zarro (SM&A) -- added check for non-standard input
;     
;- 
function sumer_file,  filename, status=status, directory=directory, copy=copy


defsysv, '!sumer_config', exist=exist
if not exist then sumer_config_def
filename=filename(0)
status=1

; Step 1. Check as it is.

exist = file_exist(filename)

if exist then return, filename   

break_file, filename, disk_log, dir, file1, ext
file1=file1+ext
                                    

; Step 2. Check the given directory

nd= n_elements(directory) 
if nd ge 1 then for k=0, nd-1 do begin
file = concat_dir(directory(k), file1)
if file_exist(file) then return, file
endfor


; Step 3. Check the default data directory

file=concat_dir(!sumer_config.data_dir, file1)
if file_exist(file) then return, file 

; Step 4. Check the SOHO_ARCHIVE (SSW environment only)

chk=loc_file('$SUMER_FITS_DBASE',count=count)
if count gt 0 then begin 
 file=call_function('get_sumer_files',filename,count=status)
 if status gt 0 then return,file
endif

; Step 5. Check the archive data directory

if !sumer_config.access eq 'ARCHIVE' then begin

pos=strpos(file1, '_')
if pos gt -1 then begin
 year =  fix(strmid(file1, pos+1, 2))
 month = fix(strmid(file1, pos+3,2))
endif else begin
 status=0 & return,''
endelse
                               
dir = sumer_archive_dir(year, month)
file = concat_dir(dir, file1)
exist = file_exist(file)
if exist  then if not keyword_set(copy) then return, file else begin
case strlowcase(!version.os_family) of
    'unix': cp ='cp '
     'vms':cp='copy '
     else : return, file
endcase 
spawn, cp+file+' '+!sumer_config.data_dir
file=concat_dir(!sumer_config.data_dir, file1)
if file_exist(file) then return, file
endelse

endif

if keyword_set(copy) then begin

if !sumer_config.access eq 'AUTO-FTP' then  $
 ftp_getput, file1,   !sumer_config.ftp_node, /auto, $
                 local = !sumer_config.data_dir, $
         remote = sumer_archive_dir(year, month, os=!sumer_config.ftp_os)

if !sumer_config.access eq 'ANONY-FTP' then $
 ftp_getput, file1,   !sumer_config.ftp_node, /anony, $
                 local = !sumer_config.data_dir, $
         remote = sumer_archive_dir(year, month, os=!sumer_config.ftp_os)
if !sumer_config.access eq 'FTP' then $
 ftp_getput, file1,   !sumer_config.ftp_node, $
                 !sumer_config.ftp_user, !sumer_config.ftp_passwd, $
                 local = !sumer_config.data_dir, $
        remote = sumer_archive_dir(year, month, os=!sumer_config.ftp_os)
endif
file=concat_dir(!sumer_config.data_dir, file1)
if file_exist(file) then return, file


status=0 
return, ''
end


