;+
; Project     : SOHO - CDS
;
; Name        : XCAT
;
; Purpose     : widget interface to CDS AS-RUN catalog
;
; Category    : operations, widgets
;
; Explanation :
;
; Syntax      : IDL> XCAT
;
; Inputs      : None
;
; Opt. Inputs : TSTART,TEND = start/end times to list (any UT format)
;
; Outputs     : 
;
; Opt. Outputs: QL = CDS quicklook structure
;
; Keywords    : GROUP = widget ID of any calling widget
;               TSTART,TEND = start/end times to list (any UT format)
;               LAST = use last times specified by user
;
; Common      : XCAT - last saved AS-RUN listing
;
; Restrictions: None.
;
; Side effects: None.
;
; History     : Version 1,  2-Feb-1996,  D.M. Zarro.  Written
;               Version 2, 16-Jun-1996, Zarro, added call to CDS_SNAPSHOT
;		Version 3, 18-Jun-1996, William Thompson, GSFC
;			Added XACK if CDS_SNAPSHOT returns an error.
;		Version 4, 18-Jul-1996, William Thompson, GSFC
;			Call XCDS_SNAPSHOT instead of CDS_SNAPSHOT.
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

 pro xcat_event,event                         ;event driver routine

common xcat_com,cstart,cend,mdata,mlines,elines,edata,sbase,stags,mlabel,$
 info,wops1,wops2,elist,mlist,prog_num,ops1,ops2,addb,sav_db,cur_det,$
 viewb,fits_file,fits_dir,ftext,fdata,printb,struct,editb,saveb,event_top,$
 cur_acronym,stype,atype,last_mdata,copyb,xcat_lis,xcat_stc,cur_search,dtype

 event_top=event.top
 widget_control, event.id, get_uvalue = uservalue
 if (n_elements(uservalue) eq 0) then uservalue=''
 bname=strtrim(uservalue,2)

;-- quit here

 if bname(0) eq 'exit' then begin
  xtext_reset,[stype,atype,wops1,wops2,stype]
  xkill,event_top
  return
 endif

;-- XSTUDY

 if bname(0) eq 'xstudy' then begin
  xstudy,group=event_top
  return
 endif

;-- check time inputs

 relist=0 & force=0
 info={wops1:wops1,wops2:wops2,ops1:ops1,ops2:ops2}
 good=xvalidate(info,event)
 ops1=info.ops1 & ops2=info.ops2
 if good then relist=(event.id eq wops1) or (event.id eq wops2) else return

;-- check search string

 widget_control,stype,get_value=new_search
 new_search=strupcase(trim(new_search(0)))
 if (new_search ne cur_search) then begin
  widget_control,stype,set_value=new_search
  cur_search=new_search
  relist=1 & force=1
 endif
 
;-- check search acronym

 widget_control,atype,get_value=new_acronym
 new_acronym=strupcase(strmid(trim(new_acronym(0)),0,8))
 if (new_acronym ne cur_acronym) then begin
  widget_control,atype,set_value=new_acronym
  cur_acronym=new_acronym
  relist=1 
  if cur_search ne '' then force=1
 endif

;-- search examples 

 if bname(0) eq 'examples' then begin
  xcat_help,group=event_top
  return
 endif

;-- reset fields

 if bname(0) eq 'reset' then begin
  cur_search='' & cur_acronym=''
  widget_control,atype,set_value=''
  widget_control,stype,set_value=''
  cur_det=0
  if widget_info(dtype,/type) eq 8 then $
   widget_control,dtype,set_droplist_select=cur_det else $
    widget_control,dtype,set_value=cur_det
  relist=1 & force=1
 endif

;-- new detector 

 if bname(0) eq 'dtype' then begin
  new_det=(event.index)
  if new_det ne cur_det then begin
   cur_det=new_det
   relist=1
   if cur_search ne '' then force=1
  endif
 endif

;-- switch DB

 if bname(0) eq 'switch' then begin
  db_val=event.index
  if (db_val eq 0) and (which_zdbase() eq 'User') then return
  if (db_val eq 1) and (which_zdbase() eq 'CDS') then return
  err=''
  if db_val eq 0 then s=fix_zdbase(/user,err=err) else $
   s=fix_zdbase(/cds,err=err)
  if err eq '' then find_zdbase,type,err=err,/noretry,/cat
  if err ne '' then begin
   xack,str2lines(err),group=event_top
   if widg_type(event.id) eq 'DROPLIST' then $
    widget_control,event.id,set_droplist_select=1-db_val else $
     widget_control,event.id,set_value=1-db_val
   if db_val eq 1 then s=fix_zdbase(/user) else s=fix_zdbase(/cds)
   return
  endif
  sav_db=getenv('ZDBASE')
  relist=1 & force=1
 endif

;-- relist 

 if bname(0) eq 'relist' then begin
  relist=1 & force=1
 endif

 if relist then xcat_relist,force=force

;-- print main list

 if bname(0) eq 'print' then xcat_mlist,/print

 if bname(0) eq 'save' then xcat_mlist,/save

;-- edit comments database

 if bname(0) eq 'add' then begin
  xcomment,struct.seq_num,group=event_top,err=err
  if err ne '' then xack,err,group=event_top
 endif

;-- edit FITS header

 if bname(0) eq 'edit' then begin
  
  if not priv_zdbase(/cat) then begin
   xack,'Sorry, but you do not have priviledge to modify the AS-RUN catalog',$
    group=event_top,/icon
   return
  endif else !priv=3

  if datatype(struct) eq 'STC' then begin
   xkill,sbase,stags & new_struct=struct
   widget_control,/hour
   ins='Edit desired fields and press "COMMIT" to save the changes'
   xstruct,new_struct,nx=4,xsize=23,/noff,title='Raster-Level AS-RUN Entry',$
    group=event_top,/edit,/all,/modal,ins=ins
   if not match_struct(new_struct,struct) then begin
    widget_control,/hour
    xtext,'Please wait. Updating catalog database...',wbase=tbase,/just_reg
    temp=new_struct
    temp=rep_tag_value(temp,utc2tai(temp.date_obs),'date_obs')
    temp=rep_tag_value(temp,utc2tai(temp.date_end),'date_end')
    temp=rep_tag_value(temp,utc2tai(temp.obt_time),'obt_time')
    temp=rep_tag_value(temp,utc2tai(temp.obt_end),'obt_end')
    new_struct=rep_tag_value(temp,utc2tai(temp.date_mod),'date_mod')
    err=''
    clook=where(edata.seq_ind eq struct.seq_ind,cnt)
    if cnt eq 0 then begin
     err='Problems. Cannot locate original entry.'
    endif else begin
     orig_struct=edata(clook)
     status=call_function('mod_catalog',orig_struct,new_struct,err=err)
    endelse
    if err ne '' then begin
     xkill,tbase
     xack,err,group=event_top
    endif else begin
     orig_struct=edata(clook)
     copy_struct,new_struct,orig_struct
     edata(clook)=orig_struct
     xtext,'Update completed successfully',wbase=tbase,/wait,/append,/just_reg
     xcat_mlist,/force
    endelse
   endif else xtext,'No changes made',/wait,/just_reg
  endif
 endif

;-- view fits file

 if (bname(0) eq 'quick') or (bname(0) eq 'snap') or (bname(0) eq 'bail') then begin
  file_loc=chklog('CDS_FITS_DATA')
  if file_loc eq '' then begin
   xack,'CDS_FITS_DATA environmental/logical not defined',group=event_top
   return
  endif
  fits_full_name = find_with_def(fits_file,'CDS_FITS_DATA')
  if fits_full_name eq '' then begin
   xack,['Cannot locate FITS file: '+fits_file,$
         'in CDS_FITS_DATA --> '+chklog('CDS_FITS_DATA')],group=event_top
   return
  endif
  read_file=1
  qlmgr,fdata,valid
  if valid then begin
   last_file_name=strtrim(get_tag_value(fdata,/filename),2)
   if strtrim(fits_file,2) eq strtrim(last_file_name,2) then read_file=0
  endif
  if read_file then begin
   xtext,'Please wait. Reading FITS file',wbase=tbase,/just_reg
   widget_control,/hour
   delvarx,fdata
   fdata=readcdsfits(fits_full_name)
   xkill,tbase
   qlmgr,fdata,valid
   if not valid then begin
    err='Error reading FITS file'
    xack,err,group=event.top
    return
   endif
  endif

  case bname(0) of
   'bail'  : begin
     xtext_reset,[stype,atype,wops1,wops2]
     xkill,event_top
    end
   'quick' : dsp_menu,fdata,/nocheck,/nokeep
   else: begin
    err='' & xcds_snapshot,fdata,err=err,/modal,/own
    if err ne '' then xack,err,group=event_top
   endif
  endcase
  return
 endif

;-- list events

 if event.id eq mlist then begin
  prog_num=fix(bname(event.index))
  xcat_elist
 endif

 if event.id eq elist then begin
  fits_file=trim(bname(event.index))
  fits_full_name = find_with_def(fits_file,'CDS_FITS_DATA')
  break_file,fits_full_name,dsk,dir
  fits_dir=trim(dsk+dir)
  if xcat_stc then xcat_view
 endif

;-- auto-list

 if strmid(bname(0),0,4) eq 'auto' then xcat_lis=bname(0) eq 'auto_on'

;-- raster details

 if strmid(bname(0),0,3) eq 'stc' then begin
  xcat_stc=bname(0) eq 'stc_on'
  if not xcat_stc then begin
   xkill,sbase & delvarx,struct
  endif else xcat_view
 endif

 xcat_buttons

 return & end

;--------------------------------------------------------------------------- 

pro xcat_help,group=group

common xcat_help,tbase

if xalive(tbase) then begin
 xshow,tbase
 return
endif

examples=['Use commas to separate search strings.',$
          'Search is case-insensitive.','Some examples:','',$
          'SCI_OBJ = Active Region, STUDY_ID = 10','',$
          'XCEN > 100, YCEN > 200','',$
          'RAS_ID = 10']

xtext,examples,group=group,/no_print,/no_save,/no_find,space=2,$ 
     title='Search examples',xsize=50,wbase=tbase

return & end

;--------------------------------------------------------------------------- 

pro xcat_view

common xcat_com

if exist(edata) and (datatype(fits_file) eq 'STR') then begin
 elook=where(trim(edata.filename) eq fits_file,count)
 if count eq 1 then begin
  widget_control,/hour
  entry=edata(elook)
  entry=rep_tag_value(entry,tai2utc(entry.date_obs,/ecs),'date_obs')
  entry=rep_tag_value(entry,tai2utc(entry.date_end,/ecs),'date_end')
  entry=rep_tag_value(entry,tai2utc(entry.obt_time,/ecs),'obt_time')
  entry=rep_tag_value(entry,tai2utc(entry.obt_end,/ecs),'obt_end')
  struct=rep_tag_value(entry,tai2utc(entry.date_mod,/ecs),'date_mod')
  xstruct,struct,nx=4,xsize=23,/noff,title='Raster-Level AS-RUN Entry',$
   wbase=sbase,wtags=stags,/just_reg,group=event_top
 endif
endif
return & end

;--------------------------------------------------------------------------- 

pro xcat_init

common xcat_com

delvarx,fdata,struct
fits_file='' & prog_num=-1
fits_dir=''

return & end

;--------------------------------------------------------------------------- 


pro xcat_buttons

common xcat_com

if not exist(fits_dir) then fits_dir=''
widget_control,ftext,set_value=trim(concat_dir(fits_dir,fits_file))
widget_control,viewb,sensitive=(fits_file ne '')
widget_control,editb,sensitive=datatype(struct) eq 'STC'
widget_control,addb,sensitive=datatype(struct) eq 'STC'
widget_control,printb,sensitive=datatype(mlines) eq 'STR'
widget_control,saveb,sensitive=datatype(mlines) eq 'STR'

return & end

;--------------------------------------------------------------------------- 

pro xcat_relist,force=force

xcat_init
xcat_mlist,force=force
xcat_elist

return & end

;--------------------------------------------------------------------------- 

pro xcat_mlist,print=print,force=force,save=save

common xcat_com

;-- initialize

no_lines='No AS-RUN Entries in Main DB for specified interval and/or search string(s)'

;-- read main catalog

time_change=1
if exist(cstart) and exist(cend) then begin
 time_change=(cstart ne ops1) or (cend ne ops2)
endif

plabel='PROG #   DATE_OBS                  ACRONYM    DET   SCI_OBJ' 
widget_control,mlabel,set_value=plabel

if time_change or keyword_set(force) then begin
 widget_control,mlist,set_value=''
 widget_control,elist,set_value=' '
 xtext,'Please wait. Searching Main AS-RUN Catalog',wbase=tbase,/just_reg
 delvarx,mdata,mlines
 widget_control,/hour
 nfound=0
 if cur_search eq '' then begin
  list_main,ops1,ops2,mdata,nfound,/short
 endif else begin
  xcat_check,cur_search,ok_search,status=status,syntax=syntax
  if (not syntax) then begin
   list_main,ops1,ops2,mdata,nfound,/short,search=ok_search
  endif else begin
   if (not status) then begin
    xmess=['Invalid input search string:','',cur_search,'','Search not implemented']
    xkill,tbase
    xack,xmess,group=event_top
    return
   endif
  endelse
 endelse
 cstart=ops1 & cend=ops2
 if nfound gt 0 then last_mdata=mdata
 xkill,tbase
endif
nfound=n_elements(mdata)
if (nfound gt 0) then begin
 dets=['A','N','G']
 extra1='' & extra2=''
 if cur_det gt 0 then extra1='(mdata.detector eq dets(cur_det))'
 if cur_acronym ne '' then begin
  check=strpos(mdata.obs_prog,cur_acronym)
  extra2='(check gt -1)'
 endif
 if (extra1 ne '') or (extra2 ne '') then begin
  if (extra1 ne '') and (extra2 ne '') then action=extra1+' and '+extra2 else $
   action=extra1+extra2
  s='pick=where('+action+',nfound)'
  dprint,s
  s=execute(s)
  if nfound gt 0 then mdata=mdata(pick)
 endif
 if nfound gt 0 then begin
  widget_control,/hour
  acronym=mdata.obs_prog
  desc=strpad(strtrim(string(mdata.prog_num),2),6)+$
       ' | '+strtrim(tai2utc(mdata.date_obs,/ecs),2)
  desc=desc+' | '+strpad(strtrim(acronym,2),8,/after)
  desc=desc+' | '+strtrim(mdata.detector,2)+'IS'
  desc=desc+' | '+strpad(strmid(strtrim(mdata.sci_obj,2),0,50),50,/after)
  mlines=desc       
 endif
endif

;-- list main entries

if (nfound gt 0) and (keyword_set(print) or keyword_set(save)) then begin
 tstart=strmid(tai2utc(ops1,/ecs,/vms),0,17)
 tstop=strmid(tai2utc(ops2,/ecs,/vms),0,17)
 marker='-------------------------------------------------------------'
 plist=['CDS FITS FILE LISTING FOR PERIOD: '+tstart+' TO '+tstop,'']
 plabel='PROG #   DATE_OBS                  ACRONYM    DET   SCI_OBJ' 
 plist=[plist,'Printed on '+!stime,marker,'',plabel,'']
 if keyword_set(print) then xprint,array=[plist,mlines],group=event_top else begin
  home=chklog('HOME')
  file=concat_dir(home,'xcat.lis')
  fwrite=pickfile(group=event_top,path=home,/write,file=file)
  if strtrim(fwrite) ne '' then str2file,[plist,mlines],fwrite
  return
 endelse
 return
endif

if (nfound eq 0) then begin
 widget_control,mlist,set_value=no_lines
 widget_control,mlist,set_uvalue='-1'
endif else begin
 widget_control,mlist,set_value=mlines
 widget_control,mlist,set_uvalue=trim(string(mdata.prog_num))
endelse
widget_control,printb,sensitive=(nfound gt 0)
widget_control,saveb,sensitive=(nfound gt 0)

if (nfound gt 0) then begin
 clook=where(mdata.prog_num eq prog_num,count)
 if count eq 1 then widget_control,mlist,set_list_select=clook(0) else $
  prog_num=-1
endif

return & end

;--------------------------------------------------------------------------- 

pro xcat_elist

common xcat_com

if prog_num lt 0 then begin
 widget_control,elist,set_value='         '
 return
endif

no_lines='No AS-RUN Raster Entries in Experiment DB for specified Prog Num and/or search string(s)'

;-- relist experiments

relist=0
if exist(edata) then begin
 elook=where(edata.prog_num eq prog_num,count)
 if count eq 0 then relist=1
endif else relist=1 

if relist then begin
 delvarx,struct & xkill,sbase
 fits_file='' & fits_dir=''
 widget_control,elist,set_value='         '
 delvarx,edata,elines
 xtext,'Please wait. Searching Experiment AS-RUN Catalog',wbase=tbase,/just_reg
 widget_control,/hour
 nfound=0
 if cur_search eq '' then begin
  list_exper,prog_num,edata,nfound
 endif else begin
  xcat_check,cur_search,ok_search,/exp,syntax=syntax,status=status
  if (not syntax) then begin
   list_exper,prog_num,edata,nfound,search=ok_search
  endif else begin
   if (not status) then begin
    xmess=['Invalid input search string:','',cur_search,'','Search not implemented']
    xkill,tbase
    xack,xmess,group=event_top
    return
   endif
  endelse
 endelse
 nfound=n_elements(edata)
 if (nfound gt 0) then begin
  elines=strpad(strtrim(string(edata.seq_ind),2),6)+$
        ' | '+strtrim(tai2utc(edata.date_obs,/ecs),2) + $
        ' | '+strpad(strmid(strtrim(edata.sci_spec,2),0,20),20,/after)+$
        ' | '+strtrim(edata.filename,2) + '   | '+strtrim(edata.detector,2)+'IS'

 endif
 xkill,tbase
endif

nfound=n_elements(elines)
if (nfound eq 0) then begin
 widget_control,elist,set_value=no_lines
 widget_control,elist,set_uvalue=' '
endif else begin
 widget_control,elist,set_value=elines
 widget_control,elist,set_uvalue=trim(edata.filename)
endelse

if (nfound gt 0) then begin
 flook=where(strtrim(edata.filename,2) eq trim(fits_file),count)
 if count eq 1 then widget_control,elist,set_list_select=flook(0)
endif

return & end

;--------------------------------------------------------------------------- 

pro xcat,ql,tstart=tstart,tend=tend,group=group,modal=modal,last=last,reset=reset

common xcat_com

on_error,1

;-- defaults

set_plot,'x'
if not have_widgets() then begin
 message,'widgets unavailable',/cont
 return
endif

defsysv,'!def_gset_id',exists=defined
if not defined then defsysv,'!def_gset_id',22

;-- how was XCAT called?

caller=get_caller(stat) 
if stat and (not xalive(group)) then xkill,/all
modal=keyword_set(modal)
if (not modal) and (xregistered('xcat') ne 0) then return

;-- check databases

find_zdbase,cur_db_type,status=status,/cat,/off
if not status then begin
 zdbase=getenv('ZDBASE')
 emess=['MAIN and EXPERIMENT Catalogs not in: ',zdbase]
 xack,emess,group=group
 return
endif

if not exist(prog_num) then prog_num=-1
if datatype(fits_file) ne 'STR' then fits_file=''
ql_in=n_params() eq 1

;-- reconcile times

secs_per_day=24l*3600l
week=7l*secs_per_day
month=30l*secs_per_day
last=keyword_set(last)

get_utc,ops1 & ops1.time=0
ops1=(utc2tai(ops1)-week) > utc2tai(anytim2utc('2-dec-95')) 
get_utc,ops2 & ops2.time=0
ops2.mjd=ops2.mjd+1
ops2=utc2tai(ops2)
dtime=[ops1,ops2]
ctime=[0.d,0.d]
if exist(cstart) then ctime(0)=anytim2tai(cstart)
if exist(cend) then ctime(1)=anytim2tai(cend)

times=pick_times(tstart,tend,ctime=ctime,dtime=dtime,last=last)
ops1=times(0) & ops2=times(1)

relist=0
if datatype(sav_db) ne 'STR' then sav_db=getenv('ZDBASE')
cur_db=getenv('ZDBASE')
if cur_db ne sav_db then begin
 message,'ZDBASE changed!!!',/contin
 xcat_init & sav_db=cur_db & relist=1
endif

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

wbase=widget_base(title='XCAT',/column)


;-- operation buttons

row1=widget_base(wbase,/row,/frame)
exitb=widget_button(row1,value='Done',uvalue='exit',font=bfont,/no_rel)
printb=widget_button(row1,value='Print Main List',uvalue='print',font=bfont,/no_rel)
saveb=widget_button(row1,value='Save Main List',uvalue='save',font=bfont,/no_rel)
studyb=widget_button(row1,value='Launch XSTUDY',uvalue='xstudy',font=bfont)
editb=widget_button(row1,value='Edit Catalog',uvalue='edit',font=bfont,/no_rel)
addb=widget_button(row1,value='View/Add Comments',uvalue='add',font=bfont,/no_rel)

widget_control,printb,sensitive=0
widget_control,saveb,sensitive=0
widget_control,editb,sensitive=0
widget_control,addb,sensitive=0

;-- date/time fields

row2=widget_base(wbase,/column,/frame)

temp=widget_base(row2,/row)
tlabel=widget_label(temp,font=lfont,$
       value='* Edit Start/Stop Time fields to list Main AS-RUN entries for different periods')

temp=widget_base(row2,/row)
slabel=widget_label(temp,font=lfont,value='* Time Units:                  DD-MON-YR HH:MM:SS.MS')

trow=widget_base(row2,/row)
wops1=cw_field(trow, Title= 'Start Time:  ',value=' ',$
                    /ret, xsize = 24, font = bfont,field=bfont)

wops2=cw_field(trow,  Title='Stop Time:   ', value=' ',$
                    /ret, xsize = 24, font = bfont,field=bfont)

;-- detector and search fields

row3=widget_base(wbase,/row,/frame)
new_vers=float(strmid(!version.release,0,3)) ge 4.
choices=['NIS & GIS', 'NIS only', 'GIS only ']
if new_vers then $
 dtype=call_function('widget_droplist',row3,value=choices,font=bfont,$
  uvalue='dtype',title='Detector:') else $
 dtype = cw_bselector2(row3,choices,label_left='Detector:',$
                     /return_index, uvalue='dtype',font=bfont,/no_rel)

;-- Acronym

atype=cw_field(row3,  Title='Search Acronym: ', value=' ',uvalue='atype',$
                        xsize = 8, font = bfont,field=bfont,/ret)

rbutt=widget_button(row3,value='Reset Search Fields',uvalue='reset',font=bfont)


;-- search string

row3=widget_base(wbase,/row,/frame)
stype=cw_field(row3,  Title='Search String(s): ', value=' ',uvalue='stype',$
                        xsize = 50, font = bfont,field=bfont,/ret)

exam=widget_button(row3,value='Examples',uvalue='examples',font=bfont)

;-- switch DB

row3=widget_base(wbase,/row)
exclude=['mk_cds_plan_event']
chk=where(strlowcase(caller) eq exclude,cnt)
allow_db=cnt eq 0
choices=['Personal','Official']
db_val=(cur_db_type ne 'USER')
if not allow_db then begin
 mess='* Current Database: '+choices(db_val)
 tmp=widget_label(row3,value=mess,font=lfont)
endif else begin
 new_vers=float(strmid(!version.release,0,3)) ge 4.
 tmp=widget_label(row3,value='* Current Database: ',font=lfont)
 if new_vers then begin
  db_but=call_function('widget_droplist',row3,value=choices,$
   uvalue='switch',font=bfont) 
  widget_control,db_but,set_droplist_select=db_val
 endif else begin
  db_but=cw_bselector2(row3,choices,uvalue='switch',$
   /no_rel,font=bfont,/return_index)
  widget_control,db_but,set_value=db_val
 endelse
endelse


;-- some instructions

row4=widget_base(wbase,/row)
j1=widget_label(row4,value='* To relist Main AS-RUN Entries, press: ',font=lfont)
relistb=widget_button(row4,value='Relist',uvalue='relist',font=bfont,/no_rel)

if not exist(xcat_lis) then xcat_lis=1
row5=widget_base(wbase,/row)
j1=widget_label(row5,font=lfont,value='* Automatically list AS-RUN entries each time XCAT is started? ')
choices=['Yes','No']
xmenu,choices,row5,/row,/exclusive,uvalue=['auto_on','auto_off'],font=bfont,$
                buttons=abut,/no_rel
widget_control,abut(1-xcat_lis),/set_button

row7=widget_base(wbase,/row)
r2=widget_base(row7,/row)
ilabel=widget_label(r2,font=lfont,value='* To view FITS files, '+$
       'first select PROG # from list of AS-RUN entries and then select '+$
       'INDEX from list of RASTER-LEVEL entries')

row8=widget_base(wbase,/row)
if not exist(xcat_stc) then xcat_stc=0
j1=widget_label(row8,font=lfont,value='* Automatically view AS-RUN details for selected FITS file?   ')
choices=['Yes','No']
xmenu,choices,row8,/row,/exclusive,uvalue=['stc_on','stc_off'],font=bfont,$
                buttons=sbut,/no_rel
widget_control,sbut(1-xcat_stc),/set_button


row2=widget_base(wbase,row=1,/frame)
viewb=widget_button(row2,value='Read & View Current FITS File:',$
                    font=bfont,/no_rel,/menu)
snapb=widget_button(viewb,value='Snapshot?',$
                    font=bfont,/no_rel,uvalue='snap')
quickb=widget_button(viewb,value='Quicklook?',$
                    font=bfont,/no_rel,uvalue='quick')

if ql_in then begin
 bailb=widget_button(viewb,value='Read and Exit with QL data structure?',$
                     font=bfont,/no_rel,uvalue='bail')
endif

ftext=widget_text(row2,value='',font=bfont,xsize=40)
widget_control,viewb,sensitive=0


;-- list main CDS as-run entries on top
;-- list raster-level as-run entries on bottom

temp=wbase
mtop=widget_label(font=lfont,temp,value='MAIN CDS AS-RUN ENTRIES')

mlabel=widget_list(temp,value='',font=lfont,ysize=1,/frame,xsize=60)
mlist=widget_list(temp,value=' ',font=lfont,ysize=10)
etop=widget_label(temp,value='RASTER-LEVEL AS-RUN ENTRIES',font=lfont)
value='INDEX    DATE_OBS                  SCI_SPEC               FITS FILE  '+$
       '    DETECTOR'
elabel=widget_list(temp,value=value,font=lfont,ysize=1,/frame,xsize=20)
elist=widget_list(temp,value='   ',font=lfont,ysize=10)

;-- offset main base

xrealize,wbase,group=group,/center

if keyword_set(reset) then begin
 cur_acronym='' & cur_search='' & cur_det=0 & relist=1
endif

widget_control,wops1,set_value=tai2utc(ops1,/ecs,/vms)
widget_control,wops2,set_value=tai2utc(ops2,/ecs,/vms)
if not exist(cur_det) then cur_det=0
if not exist(cur_acronym) then cur_acronym=''
if not exist(cur_search) then cur_search=''
if widget_info(dtype,/type) eq 8 then $
 widget_control,dtype,set_droplist_select=cur_det else $
  widget_control,dtype,set_value=cur_det
widget_control,atype,set_value=cur_acronym
widget_control,stype,set_value=cur_search

if xcat_lis or relist then begin
 if xalive(group) then event_top=group
 xcat_mlist,force=relist
 xcat_elist
 xcat_buttons
endif else xcat_init

;-- pointer for FITS data 

make_pointer,unseen

if ql_in then begin
 qlmgr,ql,valid_in
 if valid_in then begin
  set_pointer,unseen,ql,/no_copy
  fdata=get_pointer(unseen,/no_copy)
 endif
endif

xmanager,'xcat',wbase,group=group,modal=modal

if (not modal) and (not xalive(group)) and xalive(wbase) then xmanager
if not xalive(wbase) then xshow,group

;-- copy FITS data from memory
;   (only pass QL out of common if user specifies it on command line)

if ql_in then begin
 qlmgr,fdata,valid
 if valid then begin
  set_pointer,unseen,fdata,/no_copy
  ql=get_pointer(unseen,/no_copy)
 endif
endif

free_pointer,unseen

return & end

