;+
; Project     : SOHO - CDS     
;                   
; Name        : XCAT_CHECK
;               
; Purpose     : check that for valid XCAT search strings
;               
; Category    : Planning
;               
; Explanation : 
;               
; Syntax      : IDL> xcat_check,in_search,out_search
;    
; Examples    : 
;
; Inputs      : IN_SEARCH = input search string (e.g. OBS_PROG='SYNOP')
;               
; Opt. Inputs : None
;               
; Outputs     : OUT_SEARCH = validated search string
;
; Opt. Outputs: None
;               
; Keywords    : EXPER = validate string for EXPERIMENT catalog
;               STATUS = 1 if at least one search item is valid
;               SYNTAX = 1 if there is a syntax error
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: None
;               
; History     : Version 1,  11-Mar-1997,  D M Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;- 
           
pro xcat_check,in_search,out_search,exper=exper,status=status,syntax=syntax

out_search=''
status=0 & syntax=0
if datatype(in_search) ne 'STR' then return
if trim(in_search) eq '' then begin
 status=1 & syntax=1 & return
endif

temp=trim(str2arr(strupcase(strcompress(in_search))))

nitems=n_elements(temp)

;-- setup valid search fields

main_tags=['PROG_NUM','PROG_ID','PROG_IND','STUDY_ID','STUDYVAR',$
           'OBS_PROG','DETECTOR','SCI_OBJ','SCI_SPEC','CMP_NO',$
           'OBJECT','OBJ_ID','DATE_OBS','DATE_END','OBT_TIME','OBT_END',$
           'XCEN','YCEN','ANGLE','IXWIDTH','IYWIDTH','SEQ_FROM','SEQ_TO',$
           'COMMENTS','DATE_MOD']

exper_tags=['PROG_NUM','PROG_ID','PROG_IND','N_REPEAT_S','STUDY_ID',$
            'STUDYVAR','OBS_PROG','SCI_OBJ','SCI_SPEC','CMP_NO',$
            'OBJECT','OBJ_ID','SEQ_NUM','OBS_SEQ','COUNT','SEQ_IND',$
            'RAS_ID','RAS_VAR','EXPTIME','OBS_MODE','DW_ID',$
            'DATE_OBS','DATE_END','OBT_TIME','OBT_END','XCEN',$
            'YCEN','ANGLE','IXWIDTH','IYWIDTH','INS_X0','INS_Y0',$
            'INS_ROLL','SC_X0','SC_ROLL','WAVEMIN','WAVEMAX',$
            'TRACKING','SER_ID','OPSLBITS','OPSRBITS',$
            'SLIT_POS','MIR_POS','EV_ENAB','COMP_ERR',$
            'VDS_PMCP','VDS_MODE','VDS_ORI','VDS_ACC','GSET_ID','DETECTOR',$
            'ZONE_ID','SLIT_NUM','FILENAME','SEQVALID','DATASRC','COMMENTS',$
            'DATE_MOD']

if keyword_set(exper) then tags=exper_tags else tags=main_tags
ntags=n_elements(tags)

;-- now do validation

delim=['=','<','>']

ndelim=n_elements(delim)
valid_tag=bytarr(nitems)
bad_input=bytarr(nitems)
for i=0,nitems-1 do begin
 tsplit=str2arr(temp(i),'=')
 if n_elements(tsplit) eq 1 then tsplit=str2arr(temp(i),'<')
 if n_elements(tsplit) eq 1 then tsplit=str2arr(temp(i),'>')
 tsplit=trim(tsplit)
 ok=where_vector(tsplit,tags,tcount)

;-- check for sensible fields

 if tcount eq 1 then begin
  if n_elements(tsplit) eq 1 then bad_input(i)=1
  if n_elements(tsplit) eq 2 then begin
   if (tsplit(0) ne tags(ok(0))) or (tsplit(1) eq '') then bad_input(i)=1
  endif
  if n_elements(tsplit) eq 3 then begin
   if (tsplit(1) ne tags(ok(0))) or (tsplit(0) eq '') or $
      (tsplit(2) eq '') or (strpos(temp(i),'>') gt -1) then bad_input(i)=1
  endif
 endif
 if (tcount eq 1) and (not bad_input(i)) then begin
  valid_tag(i)=1
  if exist(done) then chk=where(ok(0) eq done,dcount) else dcount=0
  if dcount eq 0 then begin
   if exist(valid) then valid=[valid,temp(i)] else valid=temp(i)
   if exist(done) then done=[done,ok(0)] else done=ok(0)
  endif
 endif else begin
  if keyword_set(exper) then atags=main_tags else atags=exper_tags
  acheck=where_vector(tsplit,atags,acount)
  if (acount gt 0) then valid_tag(i)=1
 endelse
endfor

if exist(valid) then out_search=arr2str(valid)

if max(valid_tag) eq 1 then status=1
if max(bad_input) eq 1 then syntax=1

dprint,'out_search: ',out_search

return & end

