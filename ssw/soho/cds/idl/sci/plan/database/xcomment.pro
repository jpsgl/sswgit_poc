;+
; Project     : SOHO - CDS
;
; Name        : XCOMMENT
;
; Purpose     : widget interface to comment database
;
; Category    : operations, widgets
;
; Explanation :
;
; Syntax      : IDL> XCOMMENT,SEQ_NUM
;
; Inputs      : SEQ_NUM - catalog sequence number
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : GROUP = widget ID of any calling widget
;
; Common      : None
;
; Restrictions: None.
;
; Side effects: None.
;
; History     : Version 1,  12-March-1996,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

 pro xcomment_event,  event                         ;event driver routine

 on_error,1

;-- extract values passed as UVALUES in bases

 widget_control, event.top, get_uvalue = unseen
 info=get_pointer(unseen,/no_copy)
 if datatype(info) ne 'STC' then return

 widget_control, event.id, get_uvalue = uservalue
 wtype=widget_info(event.id,/type)
 if (n_elements(uservalue) eq 0) then uservalue=''
 bname=strtrim(uservalue,2)

;-- button events

 case bname of 

  'done': xkill,event.top

  'add': begin
    err=''
    access=priv_zdbase(/cat,err=err)
    if not access then begin
     xack,err,group=event.top
    endif else begin
     instruct='Enter a comment [max 60 characters]'
     xinput,comment,instruct,group=event.top,max_len=info.max_len,$
            ysize=1,status=status
     if status then begin
      err='' & comm_stc={seq_num:info.seq_num,comm_no:-1l,comment:comment}
      xtext,'Updating Comments database...',/just_reg,wbase=tbase
      widget_control,/hour
      s=call_function('add_exp_comm',comm_stc,err=err)
      xkill,tbase
      if err ne '' then xack,err,group=event.top else xcomment_list,info
     endif
    endelse
   end

  else:do_nothing=1
 endcase

 set_pointer,unseen,info,/no_copy
 return & end

;--------------------------------------------------------------------------- 

pro xcomment_list,info

list_exp_comm,info.seq_num,comments,nf

if nf eq 0 then begin
 lines='No Comments for Sequence Number: '+strtrim(string(info.seq_num),2)
 sens=0 
endif else begin
 lines=strarr(nf)
 comm_num=strtrim(string(comments.comm_no,'(i3)'),2)
 for i=0,nf-1 do begin
  lines(i)=' '+strpad(comm_num(i),/aft,10)+strpad(comments(i).comment,60,/aft)
  sens=1
 endfor
endelse

widget_control,info.alist,sensitive=sens
widget_control,info.alist,set_value=lines
return & end

;--------------------------------------------------------------------------- 

pro xcomment,seq_num,group=group,err=err

on_error,1

;-- defaults

if not have_widgets() then begin
 err='widget unavailable'
 message,err,/cont
 return
endif

err=''
if not exist(seq_num) then begin
 err='SEQ_NUM not entered' 
 message,err,/cont & return
endif

zdbase=chklog('ZDBASE')
file = find_with_def('exper_comment.dbf','ZDBASE') 
if strtrim(file,2) eq '' then begin
 err='Cannot find comment database files in current ZDBASE'
 message,err,/cont
 return
endif

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

;-- make widgets

wbase=widget_base(title='SEQ_NUM: '+strtrim(string(seq_num),2),/column)

row1=widget_base(wbase,row=1,/frame)

;-- operation buttons

doneb=widget_button(row1,value='DONE',font=bfont,/no_rel,uvalue='done')
newb=widget_button(row1,value='ADD NEW COMMENT', uvalue='add',font=bfont,/no_rel)

;-- comment list

max_len=60
mlabel=strpad('COMM_NO',10,/aft) +' '+strpad('COMMENT',max_len,/aft)
alabel=widget_list(wbase,value=mlabel,font=lfont,ysize=1,/frame)
alist=widget_list(wbase,value='',font=lfont,ysize=20,xsize=70)
s=seq_num
list_exp_comm,s,comm,n_found

;-- realize and center main base

xrealize,wbase,group=group,/center 

;-- stuff info structure into pointer

make_pointer,unseen
info ={alist:alist,seq_num:long(seq_num),max_len:max_len}

xcomment_list,info

set_pointer,unseen,info,/no_copy
widget_control,wbase,set_uvalue=unseen

xmanager,'xcomment',wbase,group=group,/modal
if xalive(wbase) then xmanager
info=get_pointer(unseen,/no_copy)

;-- clean-up

free_pointer,unseen
xshow,group
 
return & end

