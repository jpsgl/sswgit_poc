;+
; Project     : SOHO-CDS
;
; Name        : GT_CDS_WINDOW
;
; Purpose     : useful function for getting CDS window number
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : window=gt_cds_window(ql) 
;               or,
;               window=gt_cds_window(ras_id,ras_var)
;
; Examples    :
;
; Inputs      : QL = CDS quicklook data structure
;               or,
;               RAS_ID = raster id
;               RAS_VAR = raster variation
;
; Opt. Inputs : None
;
;
; Outputs     : WINDOW = selected window number(s)
;
; Opt. Outputs: None
;
; Keywords    : NWIND = number of windows selected
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 October 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


function gt_cds_window,ql,ras_var,nwind=nwind,err=err,quiet=quiet

nwind=0
err=''
verbose=1-keyword_set(quiet)

;-- check nature of input

ras_inp=(n_params() eq 2) 
if ras_inp then begin
 if (not exist(ql)) or (not exist(ras_var)) then begin
  pr_syntax,'window=gt_cds_window(ras_id,ras_var)'
  return,-1
 endif
endif else begin
 if n_elements(ql) gt 1 then qlmgr,ql(0),valid else qlmgr,ql,valid
 if not valid then begin
  pr_syntax,'window=gt_cds_window(ql)'
  return,-1
 endif
endelse

if ras_inp then begin
 rast=call_function('get_cds_raster',ql,ras_var,err=err)
 if err ne '' then begin
  if verbose then message,err,/cont
  return,-1
 endif
 ll=rast.ll_id
 get_linelist,ll,line,err=err
 if err ne '' then begin
  if verbose then message,err,/cont
  return,-1
 endif
 labels=line.lines.linename
 nw=n_elements(labels)
 windows=indgen(nw)
endif else begin
 nw=n_elements(ql(0).detdesc.label)
 windows=indgen(nw)
 ok=where(ql(0).detdesc.ixstop(0) ge 0,nw)
 if nw eq 0 then begin
  err='no valid windows in QL'
  if verbose then message,err
  return,-1
 endif
 windows=windows(ok)
 labels=trim((ql(0).detdesc.label)(ok))
endelse

print,'WINDOW        LINE'
for i=0,nw-1 do begin
 print,string(windows(i),'(i2)'),'            '+labels(i)
endfor
print,'' 

if nw eq 1 then begin
 nwind=1
 return,windows(0)
endif

repeat begin
 count=0
 window='' & read,'* enter window # of line(s) [e.g, 0,1,2,3, "q" to quit, or "a" for all]: ',window
 window=strmid(trim(window),0,1)
 if (window ne 'a') and (window ne 'q') then begin
  iwindow=fix(str2arr(trim(window),delim=','))
  ok=where_vector(iwindow,windows,count)
 endif
endrep until ((window ne '') and (count gt 0)) or (window eq 'q') or (window eq 'a')

if window eq 'q' then begin
 nwind=0 & return,-1
endif

if window eq 'a' then window=windows else window=fix(str2arr(window))
chk=where_vector(window,windows,nwind)
if nwind gt 0 then window=windows(chk)
if nwind eq 1 then window=window(0)

return,window & end
