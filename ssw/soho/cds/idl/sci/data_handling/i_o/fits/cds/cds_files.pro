;+
; Project     : SOHO-CDS
;
; Name        : CDS_FILES
;
; Purpose     : find CDS files
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : files=cds_files(tstart,tend)
;
; Examples    :
;
; Inputs      : TSTART,TEND = tstart/tend to search
;
; Opt. Inputs : None
;
; Outputs     : FILES = filenames found
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;               STUDY = study acronym to search for
;               INFO = some study info (name, date/time etc)
;               COUNT = # of files found
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 March 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


function cds_files,tstart,tend,err=err,study=study,info=info,count=count

on_error,1
err=''
count=0
files=''

if getenv('CDS_FITS_DATA') eq '' then begin
 err='point environmental CDS_FITS_DATA to CDS data file directory'
 message,err,/cont
 return,''
endif


stat=fix_zdbase(/cds,err=err)
if not stat then return,''
set_cds_sdb

;-- check time inputs

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then get_utc,t1

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
endif

err=''
sexp='list_exper,t1,t2,obs,nobs,err=err'
if datatype(study) eq 'STR' then begin
 search='"obs_prog='+trim(strupcase(study))+'"'
 sexp=sexp+',search='+search
endif

status=execute(sexp)
stat=fix_zdbase(/orig)

if err ne '' then begin
 message,err,/cont
 return,''
endif

if nobs eq 0 then begin
 message,'No CDS files found -- check ZDBASE',/cont
 return,''
endif

files=strarr(nobs)
for i=0,nobs-1 do files(i)=find_with_def(obs(i).filename,'CDS_FITS_DATA')

ok=where(trim(files) ne '',count)
if count eq 0 then begin
 message,'No CDS files found -- check CDS_FITS_DATA',/cont
 return,''
endif

info=anytim2utc(obs.date_obs,/vms)+' '+obs.obs_prog

return,files & end
