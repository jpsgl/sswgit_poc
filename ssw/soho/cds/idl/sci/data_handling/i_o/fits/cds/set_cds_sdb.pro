;+
; Project     : SOHO - CDS     
;                   
; Name        : SET_CDS_SDB
;               
; Purpose     : append 'sdb' directories  to ZDBASE
;               
; Category    : Planning
;               
; Explanation : 
;               
; Syntax      : IDL> set_cds_sdb
;    
; Examples    : 
;
; Inputs      : None
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : None
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: Environment/logical ZDBASE is reset
;               
; History     : Version 1,  5-August-1997,  D M Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-            

pro set_cds_sdb,original=original,err=err

err=''
common set_cds_sdb,orig

if keyword_set(original) and exist(orig) then begin
 setenv,'ZDBASE='+orig
 return
endif

sdb=getenv('sdb')
if sdb eq '' then begin
 err='sdb environmental not defined'
 return
endif

zdb=getenv('ZDBASE')
if not exist(orig) then orig=zdb

spos=strpos(zdb,sdb)
if spos gt -1 then return

sdb=sdb+'/soho/cds/data/plan/database '
if zdb eq '' then zdbase=sdb else zdbase=zdb+':'+find_all_dir(sdb,/path)
setenv,'ZDBASE='+zdbase
dprint,getenv('ZDBASE')

return & end
