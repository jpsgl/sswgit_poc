;+
; Project     : SOHO-CDS
;
; Name        : GT_CDS_QL
;
; Purpose     : read a sequence of CDS FITS QL files into memory
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : data=gt_cds_ql(fstart,fend)
;
; Examples    :
;
; Inputs      : FSTART,FEND = start/end FITS file numbers
;
; Opt. Inputs : None
;
;
; Outputs     : DATA = array of quicklook structures
;
; Opt. Outputs: None
;
; Keywords    : CALIB = set to FlatField
;             : CLEAN = set to remove spikes
;             : PROMPT = set to prompt for window
;             : WINDOW = window to select
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 October 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function gt_cds_ql,fstart,fend,calib=calib,clean=clean,rem_anon=rem_anon,$
 array=array,window=window,old_storage=old_storage,max=imax,prompt=prompt

if not exist(fstart) then begin
 message,'Syntax: ql=get_cds_ql(fstart,fend,[calib=calib])',/cont
 return,0.
endif

set_cds_sdb
if keyword_set(old_storage) then begin
 curr_storage=trim(getenv('USE_NEW_STORAGE'))
 setenv,'USE_NEW_STORAGE='
endif

if n_elements(fstart) gt 1 then begin
 pvals=long(fstart)
endif else begin
 if not exist(fend) then fend=fstart
 pvals=long(fstart)+lindgen(long(fend)-long(fstart)+1)
endelse
icount=0
if not exist(imax) then imax=100
porder = uniq([pvals], sort([pvals])) 
pvals=pvals(porder)
np=n_elements(pvals)
sname=make_str(1,/noexe)

for i=0,np-1 do begin
 ival=pvals(i)
 list_exper,ival,rasters,nfound
 if nfound gt 0 then begin

;-- only copy rasters of a feather

  if not exist(ras_id) then begin
   ras_id=rasters(0).ras_id
   message,'extracting RAS_ID '+trim(string(ras_id)),/cont
  endif else begin
   if ras_id ne rasters(0).ras_id then begin
    message,'skipping RAS_ID '+trim(string(rasters(0).ras_id)),/cont
    goto,skip
   endif
  endelse

  if not exist(ras_var) then begin
   ras_var=rasters(0).ras_var
   message,'extracting RAS_VAR '+trim(string(ras_var)),/cont
  endif else begin
   if ras_var ne rasters(0).ras_var then begin
    message,'skipping RAS_VAR '+trim(string(rasters(0).ras_var)),/cont
    goto,skip
   endif
  endelse

  prompt=keyword_set(prompt)
  for j=0,nfound-1 do begin
   ifile=trim(rasters(j).filename)
   file=find_with_def(ifile,'CDS_FITS_DATA')
   if trim(file) ne '' then begin
    message,'reading '+ifile,/cont
    ql=readcdsfits(ifile,preselect=window)
    if not exist(window) and prompt then begin
     window=gt_cds_window(ql)
     if window lt 0 then begin
      message,'aborted',/cont
      return,0
     endif
     prompt=0
     ql=readcdsfits(ifile,preselect=window)
    endif
    qlmgr,ql,valid
    if valid then begin
     ql=create_struct(ql,name=sname)
     err=''
     if keyword_set(calib) then vds_calib,ql,err=err
     if keyword_set(clean) then cds_clean,ql
     if keyword_set(rem_anon) then ql=rem_anon_tag(temporary(ql))
     if keyword_set(array) then begin
      if (not exist(window)) then swindow=gt_cds_window(ql) else window=swindow
      boost_array,data,temporary(reform(gt_windata(ql,swindow,/nocheck,/quick,/no_copy)))
     endif else begin
      if not exist(data) then data=temporary(ql) else $
       data=[temporary(data),temporary(ql)]
     endelse
     icount=icount+1
     if icount ge imax then goto,done
    endif
   endif
  endfor
 endif
skip:
endfor

done:
if not exist(data) then begin
 data=0.
 message,'could not retrieve QL data',/cont
endif
if keyword_set(old_storage) then begin
 setenv,'USE_NEW_STORAGE='+curr_storage
endif

return,data

end


