;+
; Project     : SOHO-CDS
;
; Name        : READ_CDS
;
; Purpose     : read a sequence of CDS FITS files into QL structures
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : read_cds,files,ql
;
; Examples    :
;
; Inputs      : FILES = CDS file names
;
; Opt. Inputs : None
;
;
; Outputs     : DATA = array of quicklook structures
;
; Opt. Outputs: None
;
; Keywords    : See GT_CDS_QL
;
; Common      : None
;
; Restrictions: Could blow-up when mixing files of different raster/window
;               combinations.
;
; Side effects: None
;
; History     : Written 22 March 1998, D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro read_cds,files,ql

if datatype(files) ne 'STR' then begin
 pr_syntax,'read_cds,files,ql'
 return
end

nf=n_elements(files)
for i=0,nf-1 do begin
 err=''
 qt=readcdsfits(files(i),err=err)
 if err eq '' then ql=concat_struct(ql,temporary(qt))
endfor

return & end
