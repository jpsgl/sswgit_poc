;+
; Project     : SOHO-CDS
;
; Name        : CDS_COMPRESS
;
; Purpose     : read and compress a sequence of CDS FITS QL files
;
; Category    : data analysis
;
; Explanation : 
;
; Syntax      : cds_compress,fstart,fend
;
; Examples    :
;
; Inputs      : FSTART,FEND = start/end FITS file numbers
;
; Opt. Inputs : None
;
;
; Outputs     : Compressed FITS files in current directory
;
; Opt. Outputs: None
;
; Keywords    : OUTDIR = output directory for compressed files
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 June 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro cds_compress,fstart,fend,outdir=outdir,imax=imax

cd,curr=curr
if datatype(outdir) eq 'STR' then out_dir=outdir else out_dir=curr

if not chk_dir(out_dir) then begin
 message,'invalid output directory: '+outdir,/cont
 return
endif

if not test_open(out_dir) then begin
 message,'no write access to output directory: '+outdir,/cont
 return
endif

if not exist(fstart) then begin
 message,'Syntax: cds_compress,fstart,fend',/cont
 return
endif

if not exist(fend) then fend=fstart
if not exist(imax) then imax=1000
icount=0
for i=long(fstart),long(fend) do begin
 list_exper,i,rasters,nfound
 if nfound gt 0 then begin
  for j=0,nfound-1 do begin
   ifile=trim(rasters(j).filename)
   file=find_with_def(ifile,'CDS_FITS_DATA')
   if trim(file) ne '' then begin
    message,'compressing '+file,/cont
    spawn,'cp '+file+' '+outdir
    spawn,'compress '+concat_dir(out_dir,ifile)
    icount=icount+1
    if icount ge imax then return
   endif
  endfor
 endif
endfor

return
end


