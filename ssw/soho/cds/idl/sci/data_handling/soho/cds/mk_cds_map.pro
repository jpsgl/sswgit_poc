;+
; Project     : SOHO-CDS
;
; Name        : MK_CDS_MAP
;
; Purpose     : Make an image map from a CDS QL structure
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : map=mk_cds_map(ql,window)
;
; Examples    :
;
; Inputs      : QL = CDS quicklook data stucture
;               WINDOW = window number
;
; Opt. Inputs : None
;
; Outputs     : MAP ={data:data,xp:xp,yp:yp,id:id,time:time,soho:soho}
;               where
;               DATA  = 2d image array
;               XP,YP = 2d cartesian coordinate arrays
;               ID    = window ID label
;               TIME  = start time of image
;               SOHO  = 1, flag identifying that image is SOHO-viewed
;
; Opt. Outputs: None
;
; Keywords    : CLEAN = clean Cosmic ray hits
;               SUM   = sum intensities over window wavelength
;               UPDATE = update CDS pointing
;               DEBIAS = debias CDS data
;               CALIB = calibrate data
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 22 October 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function mk_cds_map,ql,window,clean=clean,sum=sum,update=update,$
 debias=debias,calib=calib

if datatype(ql) ne 'STC' then begin
 message,'enter a CDS QL structure',/cont
 return,0
endif

for i=0,n_elements(ql)-1 do begin
 aa=ql(i)
 if not exist(window) then window=gt_cds_window(aa)
 if window lt 0 then return,0

;-- update pointing

 if keyword_set(update) then begin
  err=''
  upd_cds_point,aa,err=err
  if err ne '' then message,err,/cont
 endif

;-- calibrate

 if keyword_set(calib) then begin
  err=''
  vds_calib,aa,err=err
  if err ne '' then message,err,/cont
 endif

;-- produce image

 w=gt_wlimits(aa,/wave,/quiet)
 wlim=w(0:1,window)
 mlam=total(wlim)/n_elements(wlim)
 if keyword_set(sum) then begin
  h=float(gt_bimage(aa,wlim(0),wlim(1),xs=xp,ys=yp,time=time))
 endif else begin
  h=float(gt_mimage(aa,mlam,xs=xp,ys=yp,time=time))
 endelse

;-- clean it

 if keyword_set(clean) then cds_clean_image,h

;-- make map

 timg={data:float(h),xp:float(xp),yp:float(yp),time:tai2utc(time(0,0),/vms,/ecs),$
      id:cds_wave_label((ql.detdesc.label)(window)),soho:1}

 img=concat_struct(img,timg)

endfor

return,img

end
