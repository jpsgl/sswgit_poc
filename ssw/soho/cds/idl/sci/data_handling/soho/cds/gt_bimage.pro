;+
; Project     : SOHO - CDS     
;                   
; Name        : GT_BIMAGE
;               
; Purpose     : Return wavelength band integrated image.
;               
; Explanation : GT_BIMAGE returns a two-dimensional image from a CDS
;               observation raster. The image consists of intensities
;               integrated over a given wavelength band, for all the X/Y image
;               positions in the raster.
;
;               The integration is performed in a naive way, with partially
;               covered pixels contributing with weight corresponding to the
;               fraction of the pixel covered by the integration area.
;
;               The total pixel weight for each position is returned through
;               the keyword WEIGHT. I.e., to get the average counts per pixel,
;               divide by the WEIGHT, but beware of division with zero when
;               data is missing. A warning is given if the weight varies more
;               than 0.5% from the expected value.
;
;               If pixel weights differ more than 1% from the expected value,
;               the pixels are flagged as missing.
;
;               The returned array has dimensions (X,Y) given by the size of
;               the data contained in the QLDS.
;
;               In addition to the image, values for the physical parameters
;               XSOLAR, YSOLAR, and TIME returned through output keywords as
;               two-dimensional arrays with the same size as the image.
;
; Use         : im = gt_bimage(QLDS,lambda)
;    
; Inputs      : QLDS: CDS QuickLook Data structure
;               
;               LAMBDA1,
;               LAMBDA2: The low and high limit of the wavelength integration
;                        area 
;
; Opt. Inputs : 
;               
; Outputs     : Two-dimensional image. Size determined by the content
;               of the QLDS. 
;               
; Opt. Outputs: XSOLAR : Solar Cartesian X (arcsecs)
;
;               YSOLAR : Solar Cartesian Y (arcsecs)
;
;               TIME   : Time of the exposures
;
;               WEIGHT : The (possibly fractional) number of dispersion pixels
;                        that have been included in the integration.
;               
; Keywords    : NOCHECK: Set to avoid call to QLMGR
;
;               NOPARCHECK: Set to avoid most parameter checking mostly for
;                           internal use.
;
; Calls       : ANYTIM2UTC(), GT_DETECTOR(), GT_DIMENSION(), GT_IIMAGE(),
;               GT_WAVEBAND(), GT_WINDOW(), LOAD_WAVECAL(), PARCHECK,
;               PIX2WAVE(), QLMGR, TRIM(), TYP(), WAVE2PIX()
;
; Common      : None.
;               
; Restrictions: QLDS == QL Data Structure.
;               
; Side effects: None known.
;               
; Category    : GET
;               
; Prev. Hist. : Previous version deleted with a tar cvf gt*.pro command!
;
; Written     : SVH Haugan, UiO, 2-November-1995
;               
; Modified    : Version 2, SVHH, UiO, 6 April 1996
;                         Changed logic for finding the initial image,
;                         to avoid calling gt_iimage too many times.
;                         Included keyword /QUICK for gt_iimage calls.
;               Version 3, SVHH, UiO, 14 April 1996
;                         Using LOAD_WAVECAL for wavelength calibrations.
;                         Also fixed typo that made it *not* compile.
;               Version 4, SVHH, UiO, 13 August 1996
;                       Corrected a bug caused by an improved implementation
;                       of the /QUICK keyword in gt_iimage.
;                       This routine now detects missing pixels, excludes
;                       them in the integration, and flags pixels with less
;                       than half the expected pixel weight as missing.
;               Version 5, SVHH, UiO, November-January 1997
;                       New, faster version (vectorized).
;               Version 6, SVHH, 18 February 1999
;                       Fixed bug with DETX > 2048 for GIS bands.
;               Version 7, DMZ, 1 April 1999
;                       Added ERR and /CONT
;               Version 8, Zarro (SM&A/GSFC), 19 Oct 1999
;                       Added size check on DL
;
; Version     : 8
;-            


FUNCTION gt_bimage,QLDS, LAMBDA1, lambda2, $
                   NOCHECK=NOCHECK, $
                   noparcheck=noparcheck, $
                   xsolar=solarx, ysolar=SOLARY, time=TIME, $
                   weight=weight,$
                   dety=dety, mirpix=mirpix,err=err ;; For debugging
  
  ON_ERROR,2
  err=''
  IF !debug GT 0 THEN ON_ERROR,0
  
; --------------------------------
; Parameter checking...
; --------------------------------
  
  IF NOT KEYWORD_SET(noparcheck) THEN BEGIN
     IF N_params() LT 3 THEN $
        MESSAGE,'Use: gt_bimage(QL_Data,lambda_low,lambda_high)'
     
     IF NOT Keyword_SET(NOCHECK) THEN BEGIN
        QLMGR,QLDS,valid
        IF NOT valid THEN begin
         err='First argument must be a QL data structure'
         message,err,/cont
         return,0
        endif
     EndIF
  ENDIF
  
  det = gt_detector(qlds)
  aa = STRING(197b)
  
  dummy = load_wavecal(det,anytim2utc(qlds.header.date_obs), $
                       cal_struct=qlds.wavecal)
  
  parcheck,lambda1,2,typ(/rea),0,'LAMBDA'
  band = gt_waveband(det,lambda1)
  IF band EQ 0 THEN begin
   err="Wavelength outside range for detector "+det
   message,err,/cont
   return,0
  endif

  
  parcheck,lambda2,2,typ(/rea),0,'LAMBDA'
  IF gt_waveband(det,lambda2) NE band THEN begin
     err="High/Low integration limits must be in the same detector band"
     message,err,/cont
     return,0
  endif
  
  detband = det+trim(band)
  
  dims = gt_dimension(qlds)
  
  IF lambda1 GT lambda2 THEN BEGIN
     MESSAGE,"Switching high/low integration limits",/continue
     temp = lambda1
     lambda1 = lambda2
     lambda2 = temp
  ENDIF
  
  lo = lambda1
  hi = lambda2
  
  detx = wave2pix(detband,lo)
  
  ;; First make sure that detx is valid, i.e., it's covered by a window.
  
FIND_LOWER_WINDOW:
  
  test = gt_window(qlds,band,detx,/loaded)
  IF test(0) EQ -1 THEN BEGIN
     

     IF N_ELEMENTS(MUSTFIND) GT 0 THEN begin
        err="Cannot locate window covering lower limit"
        message,err,/cont
        return,0
     endif
     
     IF (gt_window(qlds,band,detx-1,/loaded))(0) NE -1 THEN detx = detx-1 $
     ELSE IF (gt_window(qlds,band,detx+1,/load))(0) NE -1 THEN detx = detx+1 $
     ELSE begin
      err="Cannot locate a valid pixel near that wavelength"
      message,err,/cont
      return,0
     endelse
  END   
  test = test(0)
  
  ;; Now get a "sample" image, with wavelength info and supplying the
  ;; solar x/y and time. We extract the zero-offset image for the located
  ;; test window - to make sure once and for all whether this is a window
  ;; covering the lower end of our integration.
  
  im = gt_iimage(qlds,band=band,detx=detx,lambda=l, $
                 xsolar=solarx,ysolar=solary,time=time,/noparcheck)
  
  ;; Get delta lambda at that position...
  
  temp = DOUBLE(pix2wave(detband,[detx,detx+1]))
  dl = 0.5d * (temp(1)-temp(0))
  
  IF MAX(l)-dl GT lo THEN BEGIN
     DETX = DETX - 1
     MUSTFIND = 1
     GOTO,FIND_LOWER_WINDOW
  END
  
  ;; We now know that we can get off even if we cut the data at this DETX!
  
  catch,error
  IF error NE 0 THEN BEGIN
     catch,/cancel
     IF N_ELEMENTS(windata) NE 0 THEN st_windata,qlds,test,winda,/no_copy
     err="Internal error in gt_bimage"
     message,err,/cont
     return,0
  END
  
  windata = gt_windata(qlds,test,/no_copy)
  winsz = SIZE(windata)
  
  windetx = qlds.detdesc(test).detx MOD 2048
  start = detx - windetx
  
  endetex = wave2pix(detband,hi) + 1
  IF det EQ 'GIS' THEN endetex = endetex + 9
  stopp = (endetex - windetx) < (winsz(1)-1)
  
  winda = windata(start:stopp,*,*,*,*)
  
  st_windata,qlds,test,windata,/no_copy
  catch,/cancel
  
  winsz = SIZE(winda)
  
  detx = qlds.detdesc(test).detx + start +  $
     rebin(INDGEN(winsz(1),1,1),winsz(1),winsz(2),winsz(3))
  mirpix=rebin(INDGEN(1,winsz(2),1),winsz(1),winsz(2),winsz(3))
  dety = rebin(INDGEN(1,1,winsz(3)),winsz(1),winsz(2),winsz(3))
  
  IF det EQ 'NIS' THEN dety = temporary(dety) + qlds.detdesc(test).dety
  
  lambda = gt_lambda(qlds,detx,dety,mirpix)
  
  ;; Get the delta lambda values
  
  dl = (lambda - SHIFT(lambda,1))
  sz=size(dl)
  if sz(1) gt 1 then  dl(0,*,*,*,*) = dl(1,*,*,*,*)

  dl2 = 0.5d * dl
  
  lamhi = lambda+dl2
  lamlo = lambda-dl2
  
  WHILE MAX(lamhi) LT lo DO BEGIN
     err="Wavelength region covers more than one window!"
     message,err,/cont
     return,0
     ;; LATER: FIND additional windows until max limit reached...
  END
  
  missing = qlds.detdesc(test).missing
  
  ;; Final product
  
  missing_mask = winda NE missing
  
  ;; Pixels touched by the lower limit:
  
  wts_low = ((lamhi < hi)-lo)/dl * ( abs(lambda-lo) LT dl2 )
  
  above_mask = lamlo GE lo ;; Pixels definitely above lower limit
  
  ;; Pixels touched by the upper limit
  
  wts_high = (hi - lamlo)/dl * (above_mask AND abs(lambda-hi) LT dl2 )
  
  ;; Whole pixel contributions
  wts_whole = above_mask AND (lamhi LE hi)
  
  wts = missing_mask*(wts_low + wts_high + wts_whole)

  image = total(wts*winda,1)
  weight = total(wts,1)
  
  avgwt = average(weight)
  
  max_rel_dev = MAX(abs(weight-avgwt))/avgwt
  
  IF max_rel_dev GT 0.005 THEN BEGIN
     MESSAGE,("Pixel weights differ up to "+trim(FIX(max_rel_dev*100))+ $
              "% from the expected value."),/informational
     IF lo NE lambda1 OR hi NE lambda2 THEN  $
        MESSAGE,("This might have been caused by inappropriate limits" $
                ),/informational
  END

  
  image = image/(weight > 1e-9)
  
  missix = WHERE(abs(weight-avgwt)/avgwt GT 0.5,count)
  IF count GT 0 THEN BEGIN
     image(missix) = missing
     MESSAGE,trim(count)+" pixels have been flagged as missing",$
        /informational
  END

  RETURN,image
  
END
