;+
; Project     : SOHO - CDS     
;                   
; Name        : GT_CDS_TIME
;               
; Purpose     : Returns the time of a specified exposure
;               
; Explanation : This function is supposed to return the most correct time
;               coordinate for given data points in a QLDS.
;		
;               The parameter XIX, YIX, and TIX may be either arrays or a
;               scalars. All array inputs must be of the same size. The result
;               will be of the same size as the input, with the exception that
;               singular dimensions will be reform'ed away.
;
; Use         : GT_CDS_TIME(QLDS,XIX,YIX,TIX)
;    
; Inputs      : QLDS: CDS QuickLook Data structure
;
;               XIX: The X index of the exposure/data point for which the time
;                    is requested.
;
;               YIX: The Y index of the exposure/data point.
;
;               TIX: The Time index of the exposure/data point.
;
; Opt. Inputs : None.
;		
; Outputs     : Returns the time of the specified exposures.
;
; Opt. Outputs: None.
;
; Keywords    : NOCHECK: Set to avoid call to QLMGR
;
;               NOPARCHECK: Avoid all parameter checking
;
; Calls       : ANYTIM2UTC(), PARCHECK, QLMGR, UTC2TAI()
;
; Common      : None.
;               
; Restrictions: QLDS == QL Data Structure.
;               
; Side effects: None.
;               
; Category    : CDS, QuickLook, Data_Handling.
;               
; Prev. Hist. : 
;
; Written     : S.V.H. Haugan, 11-Sept-1995
;               
; Modified    : Version 2, SVHH, 20-December-1995
;                       Added NOPARCHECK keyword.
;               Version 3, SVHH, 27 February 1996
;                       ND based on N_elements(....axes) instead of 
;                       del_timedata's dimensionality.
;               Version 4, SVHH, 22 February 1996
;                       Added some support for mis-named axes.
;               Version 5, SVHH, 4 March 1996
;                       Fixed single-exposure case with misnamed axis.
;                       And re-fixed logic for NIS cases.
;               Version 6, SVHH, 12 April 1996
;                       Renamed gt_time -> gt_cds_time
;               Version 7, SVHH, 12 October 1996
;                       Detecting and propagating missing values.
;                       Checking that array inputs are always same size, and
;                       promoting non-array inputs to arrays.
;
; Version     : 7, 12 October 1996
;-            


FUNCTION GT_CDS_TIME,Q,XIX_IN,YIX_IN,TIX_IN,nocheck=nocheck,$
                     noparcheck=noparcheck
  
  On_Error,2
  IF !DEBUG gt 0 THEN On_Error,0
  
  IF KEYWORD_SET(noparcheck) THEN nocheck=1
  
  IF NOT Keyword_SET(NOCHECK) THEN BEGIN
     QLMGR,q,valid
     IF NOT valid THEN message,"First argument must be a QLDS"
  EndIF
  
  IF N_PARAMS() LT 4 THEN  $
     MESSAGE,"Use: GT_CDS_TIME(QLDS,XIX,YIX,TIX)"
  
  D = GT_DIMENSION(Q,/NOCHECK)
  
  IF NOT KEYWORD_SET(noparcheck) THEN begin
     parcheck,xix_in,2,typ(/NAT),[0,1,2,3,4],'XIX',MIN=0,MAX=(D.ssolar_x-1) > 0
     
     parcheck,yix_in,3,typ(/NAT),[0,1,2,3,4],'YIX',MIN=0,MAX=(D.ssolar_y-1) > 0
     
     parcheck,tix_in,3,typ(/NAT),[0,1,2,3,4],'TIX',MIN=0,MAX=(D.sdel_time-1)> 0
  END
  
  ;; Make local copies
  XIX = XIX_IN
  YIX = YIX_IN
  TIX = TIX_IN
  
  ;; Get sizes.
  szx = size(xix)
  szy = size(yix)
  szt = size(tix)
  
  ;; Drop data types
  szx = szx(0:szx(0))
  szy = szy(0:szy(0))
  szt = szt(0:szt(0))
  
  ;; Find out whether at least one input is an array
  mxdim = 0
  IF szx(0) GT 0 THEN mxdim = szx
  IF szy(0) GT mxdim(0) THEN mxdim = szy
  IF szt(0) GT mxdim(0) THEN mxdim = szt
  
  IF mxdim(0) NE 0 THEN BEGIN
     ;; At least one of the inputs were arrays -- all arrays should 
     ;; have same size
     IF szx(0) GT 0 AND (NOT same_data(szx,mxdim)) THEN $
        message,"All array inputs must have same size (xix)"
     IF szy(0) GT 0 AND (NOT same_data(szy,mxdim)) THEN $
        message,"All array inputs must have same size (yix)"
     IF szt(0) GT 0 AND (NOT same_data(szt,mxdim)) THEN $
        message,"All array inputs must have same size (yix)"
     ;; Make non-array inputs arrays
     IF szx(0) EQ 0 THEN xix = make_array(dimension=mxdim(1:mxdim(0)),$
                                          value=xix)
     IF szy(0) EQ 0 THEN yix = make_array(dimension=mxdim(1:mxdim(0)),$
                                          value=yix)
     IF szt(0) EQ 0 THEN tix = make_array(dimension=mxdim(1:mxdim(0)),$
                                          value=tix)
  END
  
  timedata = q.del_timedata
  
  ND = N_ELEMENTS(q.del_timedesc.axes)
  
  basetime = utc2tai(anytim2utc(q.header.date_obs))
  
  IF ND EQ 0 THEN BEGIN
     RETURN,basetime
  END
  
  IF nd EQ 1 THEN BEGIN
     CASE q.del_timedesc.axes OF
        'SOLAR_X': I = XIX
        'SOLAR_Y': I = YIX
        'DEL_TIME': I = TIX
        ELSE: BEGIN
           MESSAGE,"Non-standard axis name in del_time descriptor!",/inform
           IF gt_detector(q) EQ 'GIS' THEN BEGIN
              IF d.ssolar_x GT 1 THEN I = xix $
              ELSE IF d.ssolar_y GT 1 THEN I = yix $
              ELSE IF d.sdel_time GT 1 THEN I = tix  $
              ELSE IF d.ssolar_x EQ 1 THEN i = xix $
              ELSE IF d.ssolar_y EQ 1 THEN i = yix $
              ELSE IF d.sdel_time EQ 1 THEN i = tix $
              ELSE i = 0
           END ELSE BEGIN
              IF d.ssolar_x GT 1 THEN i = xix $
              ELSE IF d.sdel_time GT 1 THEN i = tix $
              ELSE i = 0
           END
        END
     END
     result = REFORM([timedata(I)])
  END ELSE IF nd EQ 2 THEN BEGIN
     CASE Q.DEL_TIMEDESC.AXES(0) OF
        'SOLAR_X': I = XIX
        'SOLAR_Y': I = YIX
        'DEL_TIME': I = TIX
        ELSE: BEGIN
           MESSAGE,"Non-standard axis name in del_time descriptor!",/inform
           IF gt_detector(q) EQ 'GIS' THEN BEGIN
              IF d.ssolar_x GT 1 THEN I = xix $
              ELSE IF d.ssolar_y GT 1 THEN I = yix  $
              ELSE IF d.sdel_time GT 1 THEN I = tix  $
              ELSE IF d.ssolar_x EQ 1 THEN i = xix $
              ELSE IF d.ssolar_y EQ 1 THEN i = yix  $
              ELSE IF d.sdel_time EQ 1 THEN i = tix $
              ELSE i = 0
           END ELSE BEGIN
              IF d.ssolar_x GT 1 THEN i = xix  $
              ELSE IF d.sdel_time GT 1 THEN i = tix $
              ELSE i = 0
           END
        END
     END
     
     CASE Q.DEL_TIMEDESC.AXES(1) OF
        'SOLAR_X': J = XIX
        'SOLAR_Y': J = YIX
        'DEL_TIME': J = TIX
        ELSE: BEGIN
           MESSAGE,"Non-standard axis name in del_time descriptor!",/inform
           IF gt_detector(q) EQ 'GIS' THEN BEGIN
              IF d.sdel_time GT 1 THEN J = tix $
              ELSE IF d.ssolar_y GT 1 THEN J = yix $
              ELSE IF d.ssolar_x GT 1 THEN J = xix $
              ELSE IF d.ssolar_x EQ 1 THEN j = xix $
              ELSE IF d.ssolar_y EQ 1 THEN j = yix  $
              ELSE IF d.sdel_time EQ 1 THEN j = tix $
              ELSE j = 0
           END ELSE BEGIN
              IF d.sdel_time GT 1 THEN j = tix $
              ELSE j = 0
           END
        END
     END
     
     result = REFORM([timedata(I,J)])
  END ELSE BEGIN
     
     ;; I don't think this should ever occur, but...
     
     CASE Q.DEL_TIMEDESC.AXES(0) OF
        'SOLAR_X': I = XIX
        'SOLAR_Y': I = YIX
        'DEL_TIME': I = TIX
        ELSE: BEGIN
           MESSAGE,"Non-standard axis name in del_time descriptor!",/inform
           I = xix
        END
     END
     
     CASE Q.DEL_TIMEDESC.AXES(1) OF
        'SOLAR_X': J = XIX
        'SOLAR_Y': J = YIX
        'DEL_TIME': J = TIX
        ELSE: BEGIN
           MESSAGE,"Non-standard axis name in del_time descriptor!",/inform
           J = yix
        END
     END
     
     CASE Q.DEL_TIMEDESC.AXES(2) OF
        'SOLAR_X': K = XIX
        'SOLAR_Y': K = YIX
        'DEL_TIME': K = TIX
        ELSE: BEGIN
           MESSAGE,"Non-standard axis name in del_time descriptor!",/inform
           K = tix
        END
     END
     
     result = REFORM([timedata(I,J,K)])
  END
  
  missix = where(result EQ q.del_timedesc.missing,nmiss)
  result = temporary(result)+basetime
  
  IF nmiss NE 0 THEN result(missix) = q.del_timedesc.missing
  
  IF N_ELEMENTS(result) EQ 1 THEN RETURN,result(0)  $
  ELSE RETURN,result
END

