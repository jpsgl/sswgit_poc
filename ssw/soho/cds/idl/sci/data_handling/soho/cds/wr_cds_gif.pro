;+
; Project     : SOHO-CDS
;
; Name        : WR_CDS_GIF
;
; Purpose     : write CDS snapshot GIS files
;
; Category    : imaging
;
; Explanation : 
;
; Syntax      : IDL> wr_cds_gif,fstart,fend
;
; Examples    :
;
; Inputs      : FSTART,FEND = start/end FITS files numbers
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : XSIZE,YSIZE = pixel size of GIF files [def= 640,512]
;               COLOR = color table to load
;               LIS_GIF = file name with listing of GIF file names [def='gis.lis']
;               DIR_GIF = directory location of GIF files [def=curr]
;               ROTATE = direction for rotating images
;               PICT   = make PICT files instead
;               FRAME  = use FRAME number in output file name
;               ZBUFF  = use ZBUFFER
;
; History     : Written 22 December 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


pro wr_cds_gif,fstart,fend,err=err,$
 lis_gif=lis_gif,dir_gif=dir_gif,xsize=xsize,ysize=ysize,color=color,$
 pict=pict,frame=frame,_extra=extra,zbuff=zbuff

common wr_cds_gif,aa,last_file

if not exist(fstart) then begin
 message,'Syntax: wr_cds_gif,fstart,fend',/cont
 return
endif

err=''
psave=!d.name
cd,cur=cur_dir
if not exist(xsize) then xsize=640
if not exist(ysize) then ysize=512
if not exist(color) then color=3
if datatype(lis_gif) eq 'STR' then lgif=lis_gif else lgif='gif.lis'
if datatype(dir_gif) eq 'STR' then dgif=dir_gif else dgif=cur_dir

ok=test_open(dgif,/write,err=err)
if not ok then return

zbuff=keyword_set(zbuff)
if zbuff then begin
 set_plot,'z'
 device,set_res=[xsize,ysize]
endif

loadct,color
giflist=concat_dir(dgif,lgif)
openw,unit,giflist,/get_lun
if not exist(fend) then fend=fstart
if keyword_set(pict) then ext='.pict' else ext='.gif'
use_frame=keyword_set(frame)

k=0
for i=long(fstart),long(fend) do begin
 read_fit=1
 list_exper,i,rasters,nfound
 if nfound gt 0 then begin
  for j=0,nfound-1 do begin
   ifile=trim(rasters(j).filename)
   break_file,ifile,dsk,dir,name
   ofile=concat_dir(dgif,name+ext)
   file=find_with_def(ifile,'CDS_FITS_DATA')
   if trim(file) ne '' then begin
    dprint,'file,ofile ',file,' ',ofile
    if exist(last_file) and exist(aa) then read_fit=(file ne last_file)
    if read_fit then begin
     delvarx,aa & aa=0b
     dprint,'% reading...'
     aa=readcdsfits(file)
     last_file=file
     upd_cds_point,aa
     err=''
     vds_calib,aa,err=err
    endif
    cds_snapshot,aa,_extra=extra
    a=tvrd()
    if zbuff then device,/close
    if use_frame then ofile=concat_dir(dgif,'frame'+trim(string(k))+ext) 
    k=k+1
    if keyword_set(pict) then write_pict,ofile,a else write_gif,ofile,a
    printf,unit,ofile
   endif
  endfor
 endif
endfor
close,unit & free_lun,unit
message,'GIF filenames written to: '+giflist,/cont
set_plot,psave

return & end

