;+
; Project     : SOHO - CDS     
;                   
; Name        : GT_MIMAGE
;               
; Purpose     : Return monochromatic image.
;               
; Explanation : GT_MIMAGE returns a two-dimensional image from a CDS
;               observation raster. The image consists of intensities at a
;               specific wavelength, for all the X/Y image positions in the
;               raster.
;
;               The image is formed by interpolating linearly between
;               neighbouring dispersion pixels, using the best current values
;               for the pixel/wavelength relationship.
;
;               The returned array has dimensions (X,Y) given by the size of
;               the data contained in the QLDS.
;
;               In addition to the image, values for the physical parameters
;               XSOLAR, YSOLAR, and TIME is returned through output keywords
;               as two-dimensional arrays with the same size as the image.
;
; Use         : im = gt_mimage(QLDS,lambda)
;    
; Inputs      : QLDS: CDS QuickLook Data structure
;               
;               LAMBDA: The wavelength of the monochromatic image.
;
; Opt. Inputs : [SAMPLE]: Not implemented yet.
;               
; Outputs     : Two-dimensional image. Size determined by the content
;               of the QLDS. 
;               
; Opt. Outputs: XSOLAR : Solar Cartesian X (arcsecs)
;
;               YSOLAR : Solar Cartesian Y (arcsecs)
;
;               TIME   : Time of the exposures
;               
; Keywords    : NOCHECK: Set to avoid call to QLMGR
;
;               NOPARCHECK: Set to avoid most parameter checking mostly for
;                           internal use.
;
;               QUICK : Set to avoid calculating physical parameters
;                       XSOLAR, YSOLAR and TIME.
;
; Calls       : ANYTIM2UTC(), GT_DETECTOR(), GT_DIMENSION(), GT_IIMAGE(), 
;               GT_WAVEBAND(), GT_WINDOW(), LOAD_WAVECAL(), PARCHECK, 
;               QLMGR, TRIM(), TYP(), WAVE2PIX()
;
; Common      : None.
;               
; Restrictions: QLDS == QL Data Structure.
;               
; Side effects: None known.
;               
; Category    : GET
;               
; Prev. Hist. : 
;
; Written     : SVH Haugan, UiO, 1-November-1995
;               
; Modified    : Version 1.1, SVHH, 2-November-1995,
;                         Removed LAMBDA output keyword from documentation
;                         and added "Calls" documentation field.
;
;               Version 2, SVHH, 6 April 1996
;                         Added keyword QUICK. Using /QUICK in calls
;                         to gt_iimage. Calculating wwidth from ixstart/stop.
;                         Tried to speed up things a bit.
;               Version 3, SVHH, 14 April 1996
;                         Using LOAD_WAVECAL().
;
; Version     : 3, 14 April 1996
;-            


FUNCTION gt_mimage,QLDS, LAMBDA, $
                   NOCHECK=NOCHECK, $
                   noparcheck=noparcheck, $
                   xsolar=solarx, ysolar=SOLARY, time=TIME, $
                   quick=quick,$
                   dety=dety, mirpix=mirpix ;; For debugging
  
  ON_ERROR,2
  IF !debug GT 0 THEN ON_ERROR,0
  
; --------------------------------
; Parameter checking...
; --------------------------------
  
  IF NOT KEYWORD_SET(noparcheck) THEN BEGIN
     IF N_params() LT 2 THEN $
        MESSAGE,'Use: gt_mimage(QL_Data,lambda)'
     
     IF NOT Keyword_SET(NOCHECK) THEN BEGIN
        QLMGR,QLDS,valid
        IF NOT valid THEN MESSAGE,'First argument must be a QL data structure'
     EndIF
  ENDIF
  
  det = gt_detector(qlds)
  aa = STRING(197b)
  
  dummy = load_wavecal(det,anytim2utc(qlds.header.date_obs), $
                       cal_struct = qlds.wavecal)
  
  parcheck,lambda,2,typ(/rea),0,'LAMBDA'
  band = gt_waveband(det,lambda)
  IF band EQ 0 THEN MESSAGE,"Wavelength outside range for detector "+det
  
  dims = gt_dimension(qlds)
  detx = wave2pix(det+trim(band),lambda)
  
  ;; First make sure that detx is valid
  
  test = gt_window(qlds,band,detx,/loaded)
  IF test(0) EQ -1 THEN BEGIN
     IF (gt_window(qlds,band,detx-1))(0) NE -1 THEN      detx = detx-1 $
     ELSE IF (gt_window(qlds,band,detx+1))(0) NE -1 THEN detx = detx+1 $
     ELSE MESSAGE,"Cannot locate a valid pixel near that wavelength"
  END   
  
  
  stop = 0
  
  quick = KEYWORD_SET(quick)
  
  im1 = gt_iimage(qlds,band=band,detx=detx,lambda=l1, quick = quick,$
                  xsolar=solarx,ysolar=solary,time=time,/noparcheck)
  
  WHILE MAX(l1) GT lambda DO BEGIN
     detx = detx - 1
     IF (gt_window(qlds,band,detx,/loaded))(0) EQ -1 THEN BEGIN
        MESSAGE,("Cannot find an image with lambda <= "+ $
                 trim(lambda)+aa),/informational
        lambda = MAX(l1)
        detx = detx+1
        MESSAGE,"Using lambda="+trim(lambda)+aa,/informational
     END ELSE BEGIN
        im1 = gt_iimage(qlds,band=band,detx=detx,lambda=l1, $
                        /quick,/noparcheck)
     END
  END
  
  image = REPLICATE(-10.121,dims.ssolar_x,dims.ssolar_y)
  
  WHILE MIN(image) EQ -10.121 DO BEGIN 
     l0 = l1
     im0 = im1
     detx = detx+1
     IF (gt_window(qlds,band,detx))(0) EQ -1 THEN BEGIN
        MESSAGE,("Cannot find an image with lambda >= "+ $
                 trim(lambda)+aa),/informational
        lambda = MIN(l1)
        detx = detx-1
        MESSAGE,"Using lambda="+trim(lambda)+aa,/informational
     END ELSE BEGIN
        im1 = gt_iimage(qlds,band=band,detx=detx,lambda=l1, $
                        /quick)
     END
     ix = WHERE(l0 LE lambda AND l1 GE lambda,count)
     IF count GT 0 THEN BEGIN
        l0i = l0(ix)
        l1i = l1(ix)
        ii = WHERE(l1i EQ l0i,count)
        IF count GT 0 THEN l1i(ii) = l0i(ii)+1.0   ;; Avoid division by 0
        over = 1.0d/(l1i-l0i)
        wt0 = (l1i-lambda)*over
        IF count GT 0 THEN wt0(ii) = 0.5
        wt1 = 1.0d - wt0
        image(ix)  = wt0*im0(ix) + wt1*im1(ix)
     ENDIF
  END

  RETURN,image
END
