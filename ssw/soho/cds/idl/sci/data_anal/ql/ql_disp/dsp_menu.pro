;+
; Project     : SOHO - CDS     
;                   
; Name        : dsp_menu
; 
; Purpose     : Selection of display modes for CDS QL data.
;               
; Explanation : Creates a menu with selections for the different display
;		modes possible for the CDS QuickLook Data Structure
;		passed as a parameter.
;
;		It's also possible to initiate reading of another
;		FITS file with new data, to display more than one
;		data set at the same time.
;
;               If you supply more than one QLDS, there will be one menu for
;               each one.
;               
; Use         : DSP_MENU,QLDS [, QLDS2, QLDS3, ...]
;    
; Inputs      : QLDS : 	A CDS QuickLook Data structure containing data
;			from  CDS FITS level-1 file, as returned
;			by READCDSFITS(). See "CDS QL data format".
;               
; Opt. Inputs : Up to 15 data structures may be supplied.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : GROUP_LEADER: 
;			Set to indicate that this window should
;			have another (top-level) menu as a group
;			leader. This has implications for how
;			the QLDS is stored during the lifetime
;			of the menu.
;
;		NOCHECK:
;			Set to avoid checking the QLDS by a call
;			to QLMGR. Used by the QL routines to speed
;			up processing.
;
;               MODAL: Make the widget modal.
;
;               DELETE: Make the widget delete the QLDS when it dies.
;
; Calls       : assign_nocopy, bell, cdslog, cdsnotify, concat_dir(),
;               cw_loadct(), default, delvarx, detdesc(), dsp_info, dsp_movie
;               dsp_point, dsp_wav, dspexp, dspspec, exist(), file_exist(),
;               gt_detector(), gt_dimension(), qlds_checkin, qlds_checkin,
;               qlds_report, qlmgr, trim(), vds_calib, vds_debias, widg_help,
;               widget_base(), xcds_snapshot, xmanager, xpl_struct
;
; Common      : QLSAVE: See QLDS_CHECKIN
;               
; Restrictions: The QLDS must adhere to the "CDS QL data format".
;               
; Side effects: Initiates the QLMGR, if not already initiated.
;		Kills any previous menu for this particular data
;		structure, taking the same position on the screen.
;               
; Category    : CDS, QuickLook, Display 
;               
; Prev. Hist. : Idea/setup by Paal Brekke
;
; Written     : Stein Vidar Hagfors Haugan, October 1993
;               
; Modified    : SVHH, 29 October 1993 - Added CLEANUP to xmanager call.
;		PB,	 November 1993 - Added several new features
;			(Full spectrum, NIS Movie..)
;		SVHH, 20 November 1993 - Brushed up documentation and
;			nomenclature (as much as possible), moved the 
;			selection of spectral windows into a separate
;			window popping up when needed.
;
;		SVHH, Feb-March 1994, - Added structure browsing,
;			skipped spectral window selection for waveslice,
;			included file name and Sci-obj. on top of the menu,
;			added QL Menu (to display other files).
;
;		PB,  July 8 1994  - Added help button
;		PB,  Aug 24 1994  - Fixed bug: Added keyword X/Yoffset 
;                       in call to Xselect, changed size of movie-image.
;               PB,   Sept 21 - 94    Added Help using WIDG_HELP
;		SVHH, 16-May-1995 - Version 2.7
;			Added XPL_STRUCT button
;               PB, February 26 1996, Check on dimension of qlds
;  			before starting movie.
;                       Added buttons for VDS_DEBIAS and CDS_CALIB
;               PB, March 3 1996, Notify user if VDS_DEBIAS already has
;                       been applied
;               Version 4, SVHH, 20 May 1996
;                       Added 'Quit and free space' option at request
;                       from Bill.
;               Version 5, SVHH, 22 May 1996
;                       Added MODAL and DELETE options, trying to be clever
;                       about when the user should be allowed to delete
;                       the data.
;               Version 6, SVHH, 18 June 1996
;                       Switched to dsp_wav instead of dspwav.
;                       Using dsp_movie for movies.
;                       Added XCAT and Pickfits possibilities, and took
;                       away the ql_menu option.
;               Version 7, SVHH, 29 July 1996
;                       Added CW_LOADCT for color table manipulation,
;                       and added XCDS_SNAPSHOT button.
;               Version 8, SVHH, 8 August 1996
;                       Added IMAGE_TOOL button, and the 'Enable structure
;                       browser' feature to shorten startup time.
;                       Added VDS_CALIB call as well.
;                       (Added group in dsp_point call 15 August 1996)
;               Version 9, SVHH, 19 August 1996
;                       Added DSP_AUX call.
;               Version 10, SVHH, 3 October 1996
;                       Added multi-menu startup capability.
;                       Added pulldown menu for starting up dsp_wav with
;                       variable number of windows. Added test call to
;                       pickfits to see if lfitslist.txt could be found.
;               Version 11, SVHH, 16 December 1996
;                       Complete redesign of the text + layout of buttons
;               Version 12, SVHH, 7 January 1997
;                       Enabled XCDS_COSMIC button.
;               Version 13, SVHH, 9 January 1997
;                       De-sensitizing some more buttons for 2D data.
;               Version 14, SVHH, 25 March 1997
;                       Added /OWN_WINDOW switch in XCDS_SNAPSHOT call.
;               Version 15, SVHH, 28 April 1997
;                       Sending GROUP_LEADER info to DSP_AUX to avoid dangling
;                       windows that do not control the QLDS.
;               Version 16, SVHH, 29 May 1997
;                       Added DSP_CUBE.
;                       
; Version     : 16, 29 May 1997
;- 

PRO DSP_MENU_EVENT, EV
  COMMON QLSAVE,QLDS,SAVED
  
  IF !DEBUG NE 0 THEN On_Error,0
  child	= widget_info(ev.top, /child)
  widget_control,child,Get_UVALUE=stat ;;,/NO_COPY
  
  qlds_checkin, widget=ev.top
  
  widget_control,ev.id,get_uvalue=uval

  CASE uval OF
     
; General features
     
  'HELP':BEGIN
     widg_help,'dsp_menu',title = 'Help: Main QL Menu',group=ev.top
     ENDCASE 
     
  'QUIT':BEGIN
     ;; If this is the GROUP LEADER (check if ev.top.uvalue is a 
     ;; structure instead of a long)  we must ensure to 
     ;; return the QLDS parameter sent to the window creation 
     ;; routine.
     
     IF KEYWORD_SET(stat.delete) THEN delvarx,qlds
     widget_control,ev.top,/destroy
     RETURN
     ENDCASE
     
     
  'DELETE':BEGIN
     delvarx,qlds
     widget_control,ev.top,/destroy
     RETURN
     ENDCASE 
     
  'KEEP':BEGIN
     widget_control,ev.top,/destroy
     RETURN
     ENDCASE
     
  'XCAT' :BEGIN &  xcat,group=ev.top & END
     
  'PICKFITS' :BEGIN & pickfits,group=ev.top & END

; Color table
     
  'XLOAD':BEGIN & xload & END
     
  'XLOADCT':BEGIN & xloadct & END
     
  'XPALETTE':BEGIN & xpalette & END
     
; Display modes
     
  'SNAPSHOT':BEGIN 
;     DEVICE,window_state=wstate
;     IF wstate(0) EQ 1 THEN wset,0 $
;     ELSE                   WINDOW,0
     xcds_snapshot,qlds,/modal,/own_window
     ENDCASE
     
  'DSP_WAV1':BEGIN & dsp_wav,qlds,1,group_leader=ev.top,/nocheck,/nokeep & END
  'DSP_WAV2':BEGIN & dsp_wav,qlds,2,group_leader=ev.top,/nocheck,/nokeep & END
  'DSP_WAV3':BEGIN & dsp_wav,qlds,3,group_leader=ev.top,/nocheck,/nokeep & END
  'DSP_WAV4':BEGIN & dsp_wav,qlds,4,group_leader=ev.top,/nocheck,/nokeep & END
  'DSP_WAV5':BEGIN & dsp_wav,qlds,5,group_leader=ev.top,/nocheck,/nokeep & END
  'DSP_WAV6':BEGIN & dsp_wav,qlds,6,group_leader=ev.top,/nocheck,/nokeep & END
  'DSP_WAV7':BEGIN & dsp_wav,qlds,7,group_leader=ev.top,/nocheck,/nokeep & END
     
  'DSP_CUBE1':BEGIN & dsp_cube,qlds,1,group=ev.top & END
  'DSP_CUBE2':BEGIN & dsp_cube,qlds,2,group=ev.top & END
  'DSP_CUBE3':BEGIN & dsp_cube,qlds,3,group=ev.top & END
  'DSP_CUBE4':BEGIN & dsp_cube,qlds,4,group=ev.top & END
  'DSP_CUBE5':BEGIN & dsp_cube,qlds,5,group=ev.top & END
  'DSP_CUBE6':BEGIN & dsp_cube,qlds,6,group=ev.top & END
     
  'DSPEXP':BEGIN & dspexp,qlds,group_leader=ev.top,/nocheck & END
     
  'DSPSPEC':BEGIN & dspspec,qlds,group_leader=ev.top,/nocheck & END
     
  'DSP_MOVIE':BEGIN & dsp_movie,qlds & END
     
; Auxiliary data     

  'IMAGE_TOOL':BEGIN
     dsp_point,qlds,group=ev.top
     ENDCASE
     
  'DSP_AUX':BEGIN & dsp_aux,qlds,group_leader=ev.top & END
     
  'INFO':BEGIN
     qlds_checkin,widget=ev.top
     dsp_INFO,qlds,group_leader=ev.top,/nocheck
     ENDCASE
      
     
  'INFO_WINDOWS':BEGIN
     qlds_checkin,widget=ev.top
     dsp_INFO,qlds,/no_basic,/windows,group_leader=ev.top,/nocheck
     ENDCASE
      
      
  'INFO_AUX':BEGIN
     qlds_checkin,widget=ev.top
     dsp_INFO,qlds,/no_basic,/aux,group_leader=ev.top,/nocheck
     ENDCASE
     
  'SBROWSER':BEGIN
     base = widget_info(ev.id,/parent)
     widget_control,ev.id,/destroy
     widget_control,/hourglass
     IF since_version('4.0.1') THEN widget_control,ev.top,update=0
     xpl_struct,qlds,on_base = base,title='Structure Browser'
     IF since_version('4.0.1') THEN widget_control,ev.top,update=1
     ENDCASE
     
; Data processing

  'DEBIAS':BEGIN
     widget_control,/hourglass
     ERRMSG = ''
     vds_debias,qlds, ERRMSG=ERRMSG      
     IF ERRMSG NE '' THEN BEGIN
        cdsnotify,' WARNING !!'
        bell
        bell
        cdsnotify,ERRMSG
        widget_control,ev.id,/destroy
     ENDIF ELSE BEGIN
        bell
        cdslog,'The CCD biases has been been removed from data'
        widget_control,ev.id,/destroy
     ENDELSE
     ENDCASE
     
  'XCDS_COSMIC':BEGIN
     qlds_checkin,widget=ev.top
     xcds_cosmic,qlds
     ENDCASE
        
  'CALIBRATE':BEGIN
     widget_control,/hourglass
     ERRMSG = ''
     vds_calib,qlds, ERRMSG=ERRMSG      
     IF ERRMSG NE '' THEN BEGIN
        cdsnotify,' WARNING !!'
        bell
        bell
        cdsnotify,ERRMSG
     ENDIF ELSE BEGIN
        bell
        cdslog,'The NIS data have been calibrated'
        widget_control,ev.id,/destroy
     ENDELSE
     ENDCASE
     
  'XCDS_ANALYSIS':BEGIN
     qlds_checkin,widget=ev.top
     xcds_analysis,dummy,extradummy,qlds=qlds
     ENDCASE
     
  END
 
  IF xalive(child) THEN widget_control,child,Set_UVALUE=stat ;,/NO_COPY
  
END






PRO dsp_menu,qlds,q1,q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14,$
       GROUP_LEADER=GROUP_LEADER,NOCHECK=NOCHECK,$
       NOKEEP=NOKEEP,MODAL=MODAL,DELETE=DELETE,$
       JUST_REGISTER=JUST_REGISTER
  
  common QLSAVE,saveqlds,SAVED
  common QLMGR,QL_ID,QL_DATA
  
  On_Error,2
  IF N_params()	lt 1 THEN $
     message,'Use: DSP_MENU, QL_Data_structure'
  
  IF NOT Keyword_SET(nocheck) THEN BEGIN
     QLMGR,qlds,valid
     IF NOT valid THEN	message,' Parameter is not a CDS QL data structure'
  ENDIF
  
  ;; 
  ;; In case dsp_menu is called with more than one qlds, we have to
  ;; start several dsp_menus, one for each qlds.
  ;; 
  
  IF N_params() GT 1 THEN BEGIN
     FOR i = 1,N_params()-1 DO BEGIN
        dummy = execute("assign_nocopy,next,q"+trim(i))
        qlmgr,next,valid
        IF NOT valid THEN BEGIN
           ;; IF there is a problem, we have to take back the previously
           ;; registered copies...
           FOR jj = 1,i-1 DO BEGIN
              dummy = execute("qn=q"+trim(jj))
              dummy = execute("qlds_fetch,qn,q"+trim(jj))
              widget_control,QL_DATA(qn).dsp_menubase,/destroy,bad_id = bad
           END
           ;; Get back the argument causing problems
           dummy = execute("assign_nocopy,q"+trim(i)+",next")
           message,"Argument no. "+trim(jj)+" is not a valid qlds"
        END
        ;; Take note of ql_no of the qlds that is about to disappear
        dummy = execute("q"+trim(i)+" = next.ql_no")
        dsp_menu,next,/just_register,delete=0
     END
  END
  
  
  default,DELETE,0
  default,MODAL,0
  
  break_file,qlds.header.filename,disk,dir,filnam,ext
  title	= filnam
  
;
; Creation of new top level base (alter only title or /row/column etc.)
;
  
  IF since_version('4.0') THEN ex = {title:title,base_align_left:0} $
  ELSE                         ex = {title:title}
  
  ex = create_struct(ex,'COLUMN',1,'XPAD',1,'YPAD',1,'SPACE',1)
  
  IF n_elements(group_leader) EQ 1 THEN $
     ex = create_struct(ex,'GROUP_LEADER',group_leader,'UVALUE',group_leader)
  
  base=widget_base(_extra=ex)
  
  
;
; Register the new base number in QLMGR data
;
  QL_DATA(qlds.QL_NO).dsp_menubase = base
  
  
;-------------------------------------
; BUILDING THE MENU
;-------------------------------------
  
  frame = 2
  
  ;;
  ;; Text field on top
  ;; 
  
  IF since_version('4.0') THEN left = {align_left:1}
  
  dummy	= widget_label(base,value='File: '+filnam,_extra=left)
  date_s = utc2dow(anytim2utc(qlds.header.date_obs),/string,/abbreviated)+$
     ' '+anytim2utc(qlds.header.date_obs,/date_only,/stime)
  dummy = widget_label(base,value='Date: '+date_s,_extra=left)
  dummy	= widget_label(base,value='Sci_obj: '+trim(qlds.header.sci_obj),$
                       _extra = left)
 
  ;;
  ;; "File" menu with help/exit/browse other files
  ;;
  
  IF since_version('4.0') THEN leftcol = {column:1,base_align_left:1} $
  ELSE                         leftcol = {column:1}
  
  f_menu = widget_base(base,_extra=leftcol,frame=frame,ypad=1,space=1)
  
  dummy = widget_label(f_menu,value='General features')
  
  f_menu2 = widget_base(f_menu,/row,ypad=1)
  f_menu3 = widget_base(f_menu2,/column,xpad=10,ypad=1)
  f_menu4 = widget_base(f_menu2,_extra=leftcol,ypad=1,space=1)
  
  W1=widget_button(f_menu4,VALUE="Help",uvalue='HELP')
  
  EXIT_MENU = ['"End Session"  {',$
               '"Quit Display menu" QUIT',$ ;Do whatever stat.delete tag says
               '}']
  
  IF NOT KEYWORD_SET(delete) then BEGIN
     ;; Should the user be allowed to delete the data?
     allow_delete = 1
     
     ;; If dsp_menu is called interactively : NO
     help,calls=calls
     caller = (str_sep(calls(1),' '))(0)
     IF N_ELEMENTS(calls) LE 2 OR caller EQ 'DSP_MENU' THEN allow_delete = 0
     
     ;; If we're a modal widget: NO
     IF KEYWORD_SET(modal) THEN allow_delete = 0
     
     ;; If we have a group leader taking care of the qlds: NO
     IF N_ELEMENTS(group_leader) EQ 1 THEN allow_delete = 0
     
     IF allow_delete THEN BEGIN
        EXIT_MENU = ['"End Session"  {',$
                     '"Exit and DELETE from memory (free space)" DELETE',$
                     '"Exit and KEEP in memory (recoverable)" KEEP',$
                     '}']        
     END
  END
  
  XPdMenu,EXIT_MENU,f_menu4
  
  w1 = widget_button(f_menu4,value='Browse other fits files',menu=2)
  w2 = widget_button(w1,value='XCAT',UVALUE="XCAT")
  TEST_FILE='' & PICKFITS,TEST_FILE=TEST_FILE
  IF test_file NE '' THEN  $
     w2 = widget_button(w1,value='Pickfits',uvalue="PICKFITS")
  
  ;;
  ;; Color table manipulation
  ;; 
  
  c_menu = widget_base(base,_extra=leftcol,frame=frame,xpad=1,ypad=1,space=1)
  
  dummy = widget_label(c_menu,value='Color table manipulation')
  
  c_menu2 = widget_base(c_menu,/row,ypad=1,space=1)
  c_menu3 = widget_base(c_menu2,/column,xpad=10,ypad=1)
  c_menu4 = widget_base(c_menu2,_extra=leftcol,ypad=1,space=1)
  
  w1 = widget_button(c_menu4,value='XLOAD/XLOADCT/XPALETTE',menu=2)
  w2 = widget_button(w1,value='XLOAD',uvalue='XLOAD')
  w2 = widget_button(w1,value='XLOADCT',uvalue='XLOADCT')
  w2 = widget_button(w1,value='XPALETTE',uvalue='XPALETTE')
  
  f = concat_dir(GETENV('SSW_SETUP_DATA'), 'color_table.eit')
  IF NOT file_exist(f) THEN f = ''
  w1 = cw_loadct(c_menu4,xsize=200,/frame,file=f)
  
  ;;
  ;; DISPLAY MODES
  ;;
  
  dim = gt_dimension(qlds)
  
  d_menu = widget_base(base,_extra=leftcol,frame=frame,xpad=1,ypad=1,space=1)
  
  dummy = widget_label(d_menu,value='Data display modes:')
  
  d_menu2 = widget_base(d_menu,/row,ypad=1,space=1)
  d_menu3 = widget_base(d_menu2,/column,xpad=10,ypad=1)
  d_menu4 = widget_base(d_menu2,_extra=leftcol,ypad=1,space=1)
  
  w1 = widget_button(d_menu4,value='Snapshot',uvalue='SNAPSHOT')
  
  IF dim.ssolar_x GT 1 AND dim.ssolar_y GT 1 THEN BEGIN
     W1=widget_button(d_menu4,VALUE="Image + spectrum",menu=2)
     w2 = widget_button(w1,value='1 window',uvalue='DSP_WAV1')
     w2 = widget_button(w1,value='2 windows',uvalue='DSP_WAV2')
     w2 = widget_button(w1,value='3 windows',uvalue='DSP_WAV3')
     w2 = widget_button(w1,value='4 windows',uvalue='DSP_WAV4')
     w2 = widget_button(w1,value='5 windows',uvalue='DSP_WAV5')
     w2 = widget_button(w1,value='6 windows',uvalue='DSP_WAV6')
     w2 = widget_button(w1,value='7 windows',uvalue='DSP_WAV7')
  ENDIF
  
  IF dim.ndims GT 1 THEN BEGIN
     w1 = widget_button(d_menu4,value="Data cube viewer",menu=2)
     w2 = widget_button(w1,value='1 window',uvalue='DSP_CUBE1')
     w2 = widget_button(w1,value='2 windows',uvalue='DSP_CUBE2')
     w2 = widget_button(w1,value='3 windows',uvalue='DSP_CUBE3')
     w2 = widget_button(w1,value='4 windows',uvalue='DSP_CUBE4')
     w2 = widget_button(w1,value='5 windows',uvalue='DSP_CUBE5')
     w2 = widget_button(w1,value='6 windows',uvalue='DSP_CUBE6')
  ENDIF 
  
  W1=widget_button(d_menu4,VALUE="All data, selected windows",UVALUE="DSPEXP")
  IF dim.swavelnth LE 1 OR dim.ssolar_y LE 1 THEN widget_control,W1,SENSITIVE=0

  W1=widget_button(d_menu4,VALUE="Full detector view",uvalue='DSPSPEC')
  
  W1=widget_button(d_menu4,VALUE="Animated sequences",uvalue="DSP_MOVIE")
  IF (dim.ssolar_x GT 1) + (dim.ssolar_y GT 1) + (dim.sdel_time GT 1) LT 2 $
     THEN widget_control,w1,sensitive=0
  
  ;;
  ;; Auxiliary data
  ;; 
  
  a_menu = widget_base(base,_extra=leftcol,frame=frame,xpad=1,ypad=1,space=1)
  
  dummy = widget_label(a_menu,value='Auxiliary data')
  
  a_menu2 = widget_base(a_menu,/row,ypad=1,space=1)
  a_menu3 = widget_base(a_menu2,/column,xpad=10,ypad=1)
  a_menu4 = widget_base(a_menu2,_extra=leftcol,ypad=1,space=1)
  
  w1 = widget_button(a_menu4,Value='Show pointing w/image_tool',$
                     uvalue='IMAGE_TOOL')
  
  w1 = widget_button(a_menu4,value='Plot auxiliary data',uvalue='DSP_AUX')
  IF DIM.NDIMS LT 3 THEN widget_control,W1,SENSITIVE=0
  
  
  W1 = widget_button(a_menu4,value="General (header) information",menu=2)
  w2 = widget_button(w1,value="General info",uvalue="INFO")
  w2 = widget_button(w1,value="Window info",uvalue="INFO_WINDOWS")
  w2 = widget_button(w1,value="Auxiliary info",uvalue="INFO_AUX")
  
  w1 = widget_button(a_menu4,value='Enable structure browser',$
                     uvalue="SBROWSER")
  
  ;;
  ;; Data processing
  ;;
  
  p_menu = widget_base(base,_extra=leftcol,frame=frame,xpad=1,ypad=1,space=1)
  
  dummy = widget_label(p_menu,value='Data processing/analysis')
  
  p_menu2 = widget_base(p_menu,/row,ypad=1,space=1)
  p_menu3 = widget_base(p_menu2,/column,xpad=10,ypad=1)
  p_menu4 = widget_base(p_menu2,_extra=leftcol,ypad=1,space=1)
  
  IF gt_detector(qlds) EQ 'NIS' THEN BEGIN
     
     IF qlds.detdesc(0).units EQ 'ADC' THEN $
        w1 = widget_button(p_menu4,VALUE="Debias data (VDS_DEBIAS)",$
                         UVALUE="DEBIAS") 
     
     IF strpos(qlds.detdesc(0).units,'ADC') NE -1 THEN BEGIN
        w1 = widget_button(p_menu4,value='Remove cosmic rays',$
                           uvalue = 'XCDS_COSMIC')
        w1 = widget_button(p_menu4,UVALUE="CALIBRATE",$
                           VALUE="Flatfield/calibrate data (VDS_CALIB)")
     END
  END
  
  ;;
  ;; Data analysis
  ;;
  
  w1 = widget_button(p_menu4,value='XCDS_ANALYSIS',uvalue='XCDS_ANALYSIS')
  

  widget_control, base,	/REALIZE
  
;
; Set user value of top base to qlds
;
  
  ql_no	= qlds.QL_NO
  qlds_report,base,qlds,store =	(N_elements(GROUP_LEADER) eq 0)
  
  child	= widget_info(base,/child)
  
  widget_control,child,Set_UVALUE={delete:delete}
  
  default,just_register,0
  
  Xmanager,'DSP_MENU',base,CLEANUP='qlmgrclean',modal=modal, $
     just_reg=just_register
  
  ;; Should we attempt to return the supplied data to the caller?
  
  ret_data =  $
     (KEYWORD_SET(modal) $                ;; Modal means return data to caller
      OR N_elements(GROUP_LEADER) EQ 0) $ ;; No leader means return to caller
     AND NOT keyword_set(just_register) $ ;; Fallthrough means nokeep
     AND NOT Keyword_SET(nokeep) $        ;; But don't care if nokeep is set
     AND NOT KEYWORD_SET(DELETE)          ;; Or if DELETE is set.
  
  ;; Try to return data, but don't complain if it's been deleted
  IF ret_data THEN BEGIN  
     WHILE widget_info(base,/valid_id)	NE 0 DO	Xmanager
     qlds_checkin,ql_no,/test
     IF exist(saveqlds) THEN assign_nocopy,qlds,saveqlds
     IF N_params() GT 1 THEN BEGIN
        FOR i = 1,N_params()-1 DO BEGIN
           dummy = execute("qn=q"+trim(i)) ;; Get's the ql_no for q<n>
           qlds_checkin,qn,/test
           IF exist(saveqlds) THEN BEGIN
              dummy = execute("assign_nocopy,q"+trim(i)+",saveqlds")
           END
        END
     END
  END
  
END
