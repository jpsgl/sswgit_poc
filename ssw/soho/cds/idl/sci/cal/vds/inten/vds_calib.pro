	PRO VDS_CALIB, DATA, PROGVOL, MODE, NOBURNIN=NOBURNIN, ERRMSG=ERRMSG,$
			EXPTIME_CORR=EXPTIME_CORR,THROUGHPUT=THROUGHPUT, $
			NOFLAT=NOFLAT, SLIT6=SLIT6, NOPRESLIT6=NOPRESLIT6
;+
; Project     :	SOHO - CDS
;
; Name        :	VDS_CALIB
;
; Purpose     :	Applies calibration factors to VDS images.
;
; Explanation :	This procedure takes VDS images read with READCDSFITS and
;		applies the appropriate calibration factors.  If a correction
;		for the CCD bias as a function of readout quadrant has no
;		already been applied, then VDS_DEBIAS will be called.  First
;		the images are divided by the appropriate parts of the flat
;		field image.  The exposure time is corrected and divided into
;		the data.  If necessary, a nonlinear correction term is
;		applied.  Finally, the VDS throughput is divided into the data
;		to convert from ADC to photon-events.
;
; Use         :	VDS_CALIB, DATA  [, PROGVOL  [, MODE ]]
;
; Inputs      :	DATA	= Structure variable as read by READCDSFITS.
;
; Opt. Inputs :	PROGVOL	= The programmable voltage setting controlling the
;			  voltage across the MCP.  Allowed values are 0-255.
;			  If not passed, then taken from DATA.HEADER.VDS_PMCP.
;
;		MODE	= The VDS readout mode.  Allowed values are 1-7.  If
;			  not passed, then taken from DATA.HEADER.VDS_MODE
;
; Outputs     :	DATA	= The calibrated data will be returned as a new
;			  structure variable with the same format as the input.
;			  The units of the calibrated data will be
;			  photon-events/pixel/sec.
;
; Opt. Outputs:	None.
;
; Keywords    :	NOBURNIN = If set, then the burn-in correction is not applied.
;			   This is mainly used when analyzing the burn-in
;			   characteristics in AN_NIMCP.
;
;		NOPRESLIT6 = If set, then the burnin from the use of slit 6
;			   prior to the accident is not removed from the data.
;			   Only valid for post-recovery data.  This is mainly
;			   used for analyzing the burn-in characteristics.
;
;		EXPTIME_CORR = Returns the corrected exposure time used.
;
;		THROUGHPUT   = Returns the VDS throughput used.
;
;		SLIT6  = If set, then an adjustment is made for the estimated
;			 burn-in effect of the use of 90 arcsecond wide "movie"
;			 slit.  This correction is based on an analysis of the
;			 average behavior of the 584 line as seen in the daily
;			 synoptic images.  The amount of exposure elsewhere is
;			 assumed to be proportional to an average slit 6
;			 quiet-sun profile.
;
;			 Note that the use of the /SLIT6 keyword is unrelated
;			 to whether or not the current observation uses slit 6.
;			 Instead, it is intended to correct, as best as
;			 possible, for the cummulative effect that slit 6 has
;			 on the detector over time.
;
;			 As of 21 September 1999, the default is for SLIT6 to
;			 be set for pre-accident data.  To not apply this
;			 correction, use SLIT6=0.
;
;			 As of 29 August 2000, the default is for SLIT6 to
;			 be set for post-recovery data.  To not apply this
;			 correction, use SLIT6=0.
;
;		ERRMSG = If defined and passed, then any error messages will be
;			 returned to the user in this parameter rather than
;			 depending on the MESSAGE routine in IDL.  If no errors
;			 are encountered, then a null string is returned.  In
;			 order to use this feature, ERRMSG must be defined
;			 first, e.g.
;
;				ERRMSG = ''
;				VDS_CALIB, ERRMSG=ERRMSG, ...
;				IF ERRMSG NE '' THEN ...
;
;               NOFLAT = Do not apply the flat field (or burn-in) correction (see
;                        also the NOBURNIN keyword).
;
; Calls       :	FIND_WITH_DEF, FXREAD, FXPAR, AVERAGE, TRIM, CONCAT_DIR,
;		DATATYPE, REARRANGE, REP_TAG_VALUE, SAFE_EXP
;
; Common      :	None.
;
; Restrictions:	Before this procedure can be used, a correction for the readout
;		bias in the four quadrants of the CCD--which may also include
;		a scattered light component--must be applied.  If not already
;		done, then this routine will attempt to use the automatic
;		facility of VDS_DEBIAS.
;
; Side effects:	None.
;
; Category    :	Calibration, VDS, Intensity.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 17 February 1994.
;
; Modified    :	Version 1, William Thompson, GSFC, 17 February 1994.
;		Version 2, William Thompson, GSFC, 2 March 1994.
;			Changed to use XY instead of YZ coordinate system.
;		Version 3, William Thompson, GSFC, 19 October 1995
;			Brought up to date with current FITS format
;		Version 4, William Thompson, GSFC, 26 October 1995
;			Make sure that DETDATA has the right dimensions.
;		Version 5, William Thompson, GSFC, 28 December 1995
;			Make PROGVOL an optional input parameter. 
;		Version 6, William Thompson, GSFC, 7 February 1996
;			Keep track of missing pixels, and change the missing
;			pixels flag.
;		Version 7, William Thompson, GSFC, 29 February 1996
;			Check to make sure that VDS_DEBIAS has been called
;			first, and that this routine has not already been
;			called.  Add ERRMSG keyword.
;		Version 8, William Thompson, GSFC, 3 April 1996
;			Don't reverse calibration images in Y direction.
;		Version 9, William Thompson, GSFC, 6 May 1996
;			Take readout mode into account in calibration.  Added
;			optional parameter MODE.
;		Version 10, William Thompson, GSFC, 8 August 1996
;			Rewrote to process windows one-by-one.  Will make it
;			easier when QL structure is changed.
;		Version 11, S.V.H. Haugan, UiO, 9 August 1996
;		        Windows stored with st_windata, QL data storage system
;                       should be transparent. No references to ixstart/stop.
;               Version 12, S.V.H. Haugan, UiO, 12 August 1996
;                       A reference to ixstop is actually needed to determine
;                       whether data window has been read by readcdsfits.
;		Version 13, William Thompson, GSFC, 12 September 1996
;			Added call to GET_NIMCP.  Temporarily disabled flat
;			field.
;		Version 14, William Thompson, GSFC, 16 October 1996
;			Reinstated vertical reversal of flat field and
;			nonlinear images.  Reenabled flat field correction.
;		Version 15, William Thompson, GSFC, 28 October 1996
;			Don't stop if data has already been calibrated.
;		Version 16, William Thompson, GSFC, 13 December 1996
;			Also correct for small offset in every fourth pixel in
;			the top half of the CCD.
;
;			*** NOTE:  Only tested for readout mode 2. ***
;
;		Version 17, William Thompson, GSFC, 25 February 1997
;			Added keyword NOBURNIN
;		Version 18, 13-Jun-1997, William Thompson, GSFC
;			Call SAFE_EXP.
;		Version 19, William Thompson, GSFC, 4 August 1997
;			Change units of all windows
;		Version 20, GDZ, UCLAN 28 October 1997 
;			Added the outputs  EXPTIME_CORR,THROUGHPUT
;               Version 21, CDP, Added in flight OV, HeI, MgIX flatfield.  18-Nov-97
;		Version 22, 14-Jan-1998, William Thompson, GSFC
;			Include tilt in burn-in calculation
;		Version 23, 04-Feb-1998, William Thompson, GSFC
;			Fixed bug involving non-linear correction with binning.
;		Version 24, 23-Apr-1998, William Thompson, GSFC
;			Fixed bug involving multiple NIMCP entries for the same
;			line.
;		Version 25, 04-May-1998, William Thompson, GSFC
;			Added keyword SLIT6
;		Version 26, 02-Jun-1998, William Thompson, GSFC
;			Use SHAPE parameter from GET_NIMCP.
;		Version 27, 7-Jul-1998, Zarro (SAC/GSFC)
;			Fixed name conflict with variable AVG and
;                       Astronomy routine of same name.
;		Version 28, 04-Jan-1999, William Thompson, GSFC
;			Call VDS_BURNIN_ORIG or VDS_BURNIN_NEW depending on
;			observation date.
;		Version 29, 15-Jan-1999, William Thompson, GSFC
;			Read alternate flat-field and non-linearity images for
;			post-recovery data.
;		Version 30, 24-Sep-1999, William Thompson, GSFC
;			Provide error message if alternate non-linearity file
;			not found.  Added keyword NOPRESLIT6.
;		Version 31, 04-Jan-2000, William Thompson, GSFC
;			Fixed bug in correction for small offset in every
;			fourth pixel when vertical binning is used.
;               Version 32, 16-May-2001, CDP, RAL
;                       Removed /FLIGHT keyword and added /NOFLAT.
;		Version 33, 27-Sep-2002, William Thompson, GSFC
;			Use FIND_WITH_DEF instead of CONCAT_DIR
;               Version 34, 8-Apr-2005, William Thompson
;                       Fix erroneous application of non-linearity correction.
;
; Version     : Version 34, 8-Apr-2005
;-
;
	ON_ERROR, 2
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 1 THEN BEGIN
	    MESSAGE = 'Syntax:  VDS_CALIB, DATA  [, PROGVOL  [, MODE ]]'
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that it is VDS data we're dealing with.
;
	IF DATA.HEADER.DETECTOR NE 'NIS' THEN BEGIN
	    MESSAGE = 'Cannot use this routine with GIS data'
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that the debiasing has been done first.  If it hasn't, then
;  attempt to do it.  Also, check to see if the VDS calibration has already
;  been done.
;
	IF DATA.DETDESC(0).UNITS EQ 'ADC' THEN BEGIN
	    MESSAGE = ''
	    VDS_DEBIAS, DATA, ERRMSG=MESSAGE
	    IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	END ELSE IF DATA.DETDESC(0).UNITS NE 'DEBIASED-ADC' THEN BEGIN
	    MESSAGE = 'Data has already been calibrated for the VDS.'
	    CONTINUE = 1
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  If PROGVOL was not passed, then extract it from the data structure.  Do the
;  same with the readout mode.
;
	IF N_PARAMS() EQ 1 THEN PROGVOL = DATA.HEADER.VDS_PMCP
	IF N_PARAMS() LT 2 THEN MODE    = DATA.HEADER.VDS_MODE
;
;  Make sure that the programmable voltage setting is an allowed value, and
;  determine the index associated with it.
;
	IF (PROGVOL LT 0) OR (PROGVOL GT 255) THEN BEGIN
		MESSAGE = 'PROGVOL must be between 0 and 255'
		GOTO, HANDLE_ERROR
	END ELSE IF PROGVOL GT 250 THEN BEGIN
		VOLT_I1 = 4
	END ELSE BEGIN
		VOLT_I1 = FIX(PROGVOL/50)
	ENDELSE
	VOLT_I2 = VOLT_I1 + 1
	DVOLT = PROGVOL/50. - VOLT_I1
;
;  Read in the correction factor for the exposure time, interpolating if
;  necessary.
;
	GET_LUN, UNIT
	OPENR, UNIT, FIND_WITH_DEF('time_correction.dat', '$CDS_VDS_CAL_INT')
	CORR = FLTARR(2,6)
	READF, UNIT, CORR
	CLOSE, UNIT
	TIME_CORR = CORR(1, VOLT_I1)
	IF DVOLT NE 0 THEN TIME_CORR = TIME_CORR*(1-DVOLT) +	$
		CORR(1, VOLT_I2)*DVOLT
	EXPTIME_CORR = DATA.HEADER.EXPTIME + TIME_CORR
;
;  Read in the appropriate flat field image, interpolating if necessary.
;
	VOLTAGES = [600,678,756,834,912,990]
	VDS_READ_FLAT, FLAT, VOLTAGES(VOLT_I1), MODE, DATA.HEADER.DATE_OBS
	IF DVOLT NE 0 THEN BEGIN
		VDS_READ_FLAT, FLAT2, VOLTAGES(VOLT_I2), MODE
		FLAT = FLAT*(1-DVOLT) + FLAT2*DVOLT
	ENDIF
	FLAT = REVERSE(FLAT,2)

;
;  Add flight flatfield if requested (check applicability)
;
;        IF KEYWORD_SET(FLIGHT) THEN BEGIN
;           T0 = ANYTIM2TAI('1997-6-1')
;           T1 = ANYTIM2TAI('1997-12-1')
;           TOBS = ANYTIM2TAI(DATA.HEADER.DATE_OBS)
;           IF TOBS GE T0 AND TOBS LE T1 THEN BEGIN
;    O V
;              FFOV = FIND_WITH_DEF('iff_ov_97_9.fits', '$CDS_VDS_CAL_INT')
;              IF FFOV EQ '' THEN MESSAGE, $
;                   'Unable to find in-flight OV flat field image'
;              FXREAD, FFOV, OV
;              FLAT(975:1019,340:482) = OV
;    He I
;              FFOV = FIND_WITH_DEF('iff_he_97_9.fits', '$CDS_VDS_CAL_INT')
;              IF FFOV EQ '' THEN MESSAGE, $
;                   'Unable to find in-flight HeI flat field image'
;              FXREAD, FFOV, HE
;              FLAT(588:632,336:478) = HE
;    Mg IX
;              FFOV = FIND_WITH_DEF('iff_mg_97_9.fits', '$CDS_VDS_CAL_INT')
;              IF FFOV EQ '' THEN MESSAGE, $
;                  'Unable to find in-flight MgIX flat field image'
;              FXREAD, FFOV, MG
;              FLAT(840:884,586:728) = MG
;           ENDIF ELSE BEGIN
;              MESSAGE = 'Data outside recommended time range ' +	$
;		      '(1-Jun-97 --> 1-Dec-97)'
;              MESSAGE, MESSAGE, /CONTINUE
;           ENDELSE
;        ENDIF
;
;  Depending on the observation date, either call VDS_BURNIN_ORIG or
;  VDS_BURNIN_NEW.
;
	MESSAGE = ''
	IF ANYTIM2UTC(DATA.HEADER.DATE_OBS,/CCSDS) LT '1998-10-01' THEN BEGIN
	    VDS_BURNIN_ORIG, DATA, FLAT, NOBURNIN=NOBURNIN, FLIGHT=FLIGHT, $
		    SLIT6=SLIT6, ERRMSG=MESSAGE
	END ELSE BEGIN
	    VDS_BURNIN_NEW, DATA, FLAT, NOBURNIN=NOBURNIN, FLIGHT=FLIGHT, $
		    SLIT6=SLIT6, NOPRESLIT6=NOPRESLIT6, ERRMSG=MESSAGE
	ENDELSE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
;
;  Read in the nonlinearity correction factor.
;
	BINNING = (DATA.DETDESC.BINX > 1) * (DATA.DETDESC.BINY > 1)
        IF MAX(DATA.DETDESC.MAX / BINNING) GT 300 THEN BEGIN
	    FILE0 = 'nonlinear_factor'
	    IF ANYTIM2UTC(DATA.HEADER.DATE_OBS,/CCSDS) GT '1998-10-01' THEN $
		    FILE = FILE0 + '2' ELSE FILE = FILE0
	    SAVE_FILE = FILE + '.fits'
	    FILE = FIND_WITH_DEF(SAVE_FILE, '$CDS_VDS_CAL_INT')
	    IF FILE EQ '' THEN BEGIN
		MESSAGE, /CONTINUE, 'File ' + SAVE_FILE + ' not found.  ' + $
			'Using file ' + FILE0 + '.fits instead.'
		FILE = FIND_WITH_DEF(FILE0+'.fits', '$CDS_VDS_CAL_INT')
	    ENDIF
	    FXREAD, FILE, NONLIN
	    NONLIN = REVERSE(NONLIN,2)
	ENDIF
;
;  Read in the throughput.
;
	OPENR, UNIT, FIND_WITH_DEF('vds_throughput.dat', '$CDS_VDS_CAL_INT')
	THRU = FLTARR(2,6)
	READF, UNIT, THRU
	FREE_LUN, UNIT
	THROUGHPUT = THRU(1, VOLT_I1)
	IF DVOLT NE 0 THEN THROUGHPUT = THROUGHPUT*(1-DVOLT) +	$
		THRU(1, VOLT_I2)*DVOLT
;
;  Determine the number of windows, and step through them.
;
	NWIN = N_ELEMENTS(DATA.DETDESC)
        FOR IWIN = 0,NWIN-1 DO $
           IF DATA.DETDESC(IWIN).IXSTOP(0) GE 0 THEN BEGIN
;
;  Extract the data array from the input structure.  Save the dimensions for
;  later.  Convert to at least floating point.
;  Matches ST_WINDATA,DATA,IWIN,ARRAY
           
	    ARRAY = DETDATA(DATA, IWIN+1)
            DESC  = DETDESC(DATA, IWIN+1)
	    SA = SIZE(ARRAY)
	    IF DATATYPE(ARRAY,2) LT 4 THEN ARRAY = FLOAT(ARRAY)
;
;  Save the positions of any missing pixels.
;
	    MISSING = DESC.MISSING
	    W_MISSING = WHERE(ARRAY EQ MISSING, N_MISSING)
;
;  Extract the start and stop indices, and the detector positions.
;  When ST_WINDATA is used to store data, only the size matters
;
	    ISTART = REPLICATE(0,4)
	    ISTOP  = REPLICATE(0,4)  &  ISTOP(0) = SA(1:SA(0))-1
	    DX = DESC.DETX
	    DY = DESC.DETY
	    BINX = DESC.BINX > 1
	    BINY = DESC.BINY > 1
;
;  Divide the flat field image into the array.
;
	    I1 = 0  &  NI = ISTOP(0)-ISTART(0)+1  &  I2 = NI-1
	    J1 = 0  &  NJ = ISTOP(1)-ISTART(1)+1  &  J2 = NJ-1
	    K1 = 0  &  NK = ISTOP(2)-ISTART(2)+1  &  K2 = NK-1
	    L1 = 0  &  NL = ISTOP(3)-ISTART(3)+1  &  L2 = NL-1
;
;  Depending on the axis titles, determine the relationship between the data
;  and the flat field image.  There are two possibilities: either the first (I)
;  axis is wavelength, and the third (K) is solar Y, or the wavelength
;  dimension is missing.
;
	    AXES = DESC.AXES
	    DEBLIP = 0
	    IF AXES(0) EQ 'WAVELNTH' THEN BEGIN
		SUBFLAT = FLAT(DX:DX+NI*BINX-1, DY:DY+NK*BINY-1)
;
;  If the image was binned, then bin (average) the flat field image as well.
;
		IF (BINX GT 1) OR (BINY GT 1) THEN	$
			SUBFLAT = REBIN(SUBFLAT,NI,NK)
;
;  If some part of the window is in the upper half of the CCD, and the data has
;  not been binned in X, then form a correction for every fourth pixel.
;
		IF (BINX EQ 1) AND ((DY+NK*BINY-1) GE 512) THEN BEGIN
		    DEBLIP = 1
		    JBLIP = (INDGEN(NK) + DY) GE 512
		    IBLIP = (INDGEN(NI) + DX) # JBLIP
		    BLIP = ((IBLIP MOD 4) EQ 1) * 0.18*BINY
		ENDIF
;
;  If necessary, extend the flat field correction image in the second
;  dimension.  Do the same for the blip correction image.
;
		IF J1 NE J2 THEN BEGIN
		    SUBFLAT = REFORM(SUBFLAT,N_ELEMENTS(SUBFLAT), /OVERWRITE)
		    SUBFLAT = TEMPORARY(SUBFLAT) # REPLICATE(1,NJ)
		    SUBFLAT = REFORM(SUBFLAT,NI,NK,NJ,/OVERWRITE)
		    SUBFLAT = REARRANGE(SUBFLAT, [1,3,2])
		    IF DEBLIP THEN BEGIN
			BLIP = REFORM(BLIP,N_ELEMENTS(BLIP), /OVERWRITE)
			BLIP = TEMPORARY(BLIP) # REPLICATE(1,NJ)
			BLIP = REFORM(BLIP,NI,NK,NJ,/OVERWRITE)
			BLIP = REARRANGE(BLIP, [1,3,2])
		    ENDIF
		ENDIF
;
;  If necessary, extend the flat field correction image in the fourth
;  dimension.  Do the same for the blip correction image.
;
		IF L1 NE L2 THEN BEGIN
		    SUBFLAT = REFORM(SUBFLAT,N_ELEMENTS(SUBFLAT), /OVERWRITE)
		    SUBFLAT = TEMPORARY(SUBFLAT) # REPLICATE(1,NL)
		    SUBFLAT = REFORM(SUBFLAT,NI,NJ,NK,NL,/OVERWRITE)
		    IF DEBLIP THEN BEGIN
			BLIP = REFORM(BLIP,N_ELEMENTS(BLIP), /OVERWRITE)
			BLIP = TEMPORARY(BLIP) # REPLICATE(1,NL)
			BLIP = REFORM(BLIP,NI,NJ,NK,NL,/OVERWRITE)
		    ENDIF
		ENDIF
;
;  If the wavelength dimension is not first, the second (J) dimension is
;  assumed to be the solar Y dimension.
;
	    END ELSE BEGIN
		SUBFLAT = FLAT(DX:DX+BINX-1, DY:DY+NJ*BINY - 1)
;
;  If the image was binned, then bin (average) the flat field image as well.
;
		IF (BINX GT 1) OR (BINY GT 1) THEN	$
			SUBFLAT = REBIN(SUBFLAT,1,NJ)
;
;  If necessary, extend the flat field correction image in the first
;  dimension.
;
		IF I1 NE I2 THEN BEGIN
			SUBFLAT = REFORM(SUBFLAT,N_ELEMENTS(SUBFLAT),	$
				/OVERWRITE)
			SUBFLAT = REPLICATE(1,NI) # TEMPORARY(SUBFLAT)
		ENDIF
;
;  If necessary, extend the flat field correction image in the third
;  dimension.
;
		IF K1 NE K2 THEN BEGIN
			SUBFLAT = REFORM(SUBFLAT,N_ELEMENTS(SUBFLAT),	$
				/OVERWRITE)
			SUBFLAT = TEMPORARY(SUBFLAT) # REPLICATE(1,NK)
			SUBFLAT = REFORM(SUBFLAT,NI,NJ,NK,/OVERWRITE)
		ENDIF
	    ENDELSE
;
;  If necessary, correct every fourth pixel in the top half of the CCD.
;
	    IF DEBLIP THEN ARRAY(I1,J1,K1,L1) =		$
		    ARRAY(I1:I2,J1:J2,K1:K2,L1:L2) + BLIP
;
;  Divide in the flat field image.
;
            IF NOT KEYWORD_SET(NOFLAT) THEN BEGIN
	       ARRAY(I1,J1,K1,L1) = ARRAY(I1:I2,J1:J2,K1:K2,L1:L2) / SUBFLAT
            ENDIF
;
;  Get the exposure time in seconds from the data structure, add the correction
;  factor, and divide into the array.  The units are now ADC/pixel/sec.
;
	    ARRAY = ARRAY / (DATA.HEADER.EXPTIME + TIME_CORR)
;
;  If the rates are high enough, apply a correction for nonlinearity.  Below
;  rates of 300 ADC/pixel/sec the calculated nonlinear component is less than
;  one part in 10000, and so can be safely ignored.
;
	    IF MAX(ARRAY) GT (300*BINX*BINY) THEN BEGIN
;
;  To apply the correction, step through each of the windows separately.
;
		A = ARRAY(I1:I2,J1:J2,K1:K2,L1:L2)
		W = WHERE(ARRAY GT 300*BINX*BINY, COUNT)
;
;  If a correction needs to be applied for a specific subwindow, then extract
;  the corresponding area in the nonlinear correction image.  Once again, there
;  are two possibilities.
;
		IF (COUNT GT 0) AND (N_ELEMENTS(NONLIN) NE 0) THEN BEGIN
		    IF AXES(0) EQ 'WAVELNTH' THEN BEGIN
			SUBNONLIN = NONLIN(DX:DX+NI*BINX-1, DY:DY+NK*BINY-1)
;
;  If the image was binned, then bin (average) the nonlinear correction image
;  as well.
;
			IF (BINX GT 1) OR (BINY GT 1) THEN	$
				SUBNONLIN = REBIN(SUBNONLIN,NI,NK)
;
;  If necessary, extend the nonlinear correction in the second and fourth
;  dimensions.
;
			IF J1 NE J2 THEN BEGIN
			    SUBNONLIN = REFORM( /OVERWRITE, SUBNONLIN, $
				    N_ELEMENTS(SUBNONLIN))
			    SUBNONLIN = TEMPORARY(SUBNONLIN) # REPLICATE(1,NJ)
			    SUBNONLIN = REFORM(SUBNONLIN,NI,NK,NJ,/OVERWRITE)
			    SUBNONLIN = REARRANGE(SUBNONLIN, [1,3,2])
			ENDIF
;
			IF L1 NE L2 THEN BEGIN
			    SUBNONLIN = REFORM(SUBNONLIN,	$
				    N_ELEMENTS(SUBNONLIN), /OVERWRITE)
			    SUBNONLIN = TEMPORARY(SUBNONLIN) # REPLICATE(1,NL)
			    SUBNONLIN = REFORM(SUBNONLIN,NI,NJ,NK,NL, $
				    /OVERWRITE)
			ENDIF
;
;  If the wavelength dimension is not first, the second (J) dimension is
;  assumed to be the solar Y dimension.
;
		    END ELSE BEGIN
			SUBNONLIN = NONLIN(DX:DX+BINX-1, DY:DY+NJ*BINY - 1)
;
;  If the image was binned, then bin (average) the nonlinear correction image
;  as well.
;
			IF (BINX GT 1) OR (BINY GT 1) THEN	$
				SUBNONLIN = REBIN(SUBNONLIN,1,NJ)
;
;  If necessary, extend the nonlinear correction image in the first
;  dimension.
;
			IF I1 NE I2 THEN BEGIN
			    SUBNONLIN = REFORM(SUBNONLIN,	$
				    N_ELEMENTS(SUBNONLIN), /OVERWRITE)
			    SUBNONLIN = REPLICATE(1,NI) # TEMPORARY(SUBNONLIN)
			ENDIF
;
;  If necessary, extend the nonlinear correction image in the third
;  dimension.
;
			IF K1 NE K2 THEN BEGIN
			    SUBNONLIN = REFORM(SUBNONLIN,	$
				    N_ELEMENTS(SUBNONLIN), /OVERWRITE)
			    SUBNONLIN = TEMPORARY(SUBNONLIN) # REPLICATE(1,NK)
			    SUBNONLIN = REFORM(SUBNONLIN,NI,NJ,NK,/OVERWRITE)
			ENDIF
	    	    ENDELSE
;
;  Apply the nonlinear correction, but only at the points that really need it.
;  This is a computationally intensive procedure.
;
		    AA = ARRAY(W) / FLOAT(BINX*BINY)
		    ARRAY(W) = BINX * BINY *	$
			    (AA + ( (AA>1) / SUBNONLIN(W) )^4.1945)
		ENDIF
	    ENDIF
;
;  Divide the throughput into the image.  The units are now
;  photon-events/pixel/sec.
;
	    ARRAY = ARRAY / THROUGHPUT
;
;  Make sure that ARRAY has the proper dimensions.
;
	    ARRAY = REFORM(ARRAY, SA(1:SA(0)), /OVERWRITE)
;
;  Replace any missing pixels with the value -100.
;
	    IF N_MISSING GT 0 THEN ARRAY(W_MISSING) = -100.0
;
;  Put the data back into the structure.
;
            ST_WINDATA,DATA,IWIN,ARRAY,/NO_COPY
            
;
;  Change the missing pixel flag in the structure.
;
	    DATA.DETDESC(IWIN).MISSING = -100.0
	ENDIF		;IWIN valid
;
;  Change the label for the units.
;
	DATA.DETDESC.UNITS = 'PHOTON-EVENTS/PIXEL/SEC'
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'VDS_CALIB: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, CONTINUE=KEYWORD_SET(CONTINUE)
;
;  Exit point.
;
FINISH:
	RETURN
	END
