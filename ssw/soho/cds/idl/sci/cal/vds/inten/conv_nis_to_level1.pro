;+
; Project     :	SOHO - CDS
;
; Name        :	CONV_NIS_TO_LEVEL1
;
; Purpose     :	Converts NIS FITS files to Level-1 format
;
; Category    :	SOHO, CDS, Calibration
;
; Explanation :	This routine converts a CDS/NIS Level-0 FITS file into Level-1
;               format.  Data are processed through the standard calibration
;               routines to update the pointing information, remove cosmic
;               rays, apply the radiometric calibration, and reinterpolate the
;               data to remove the spectral slant and line tilts.
;
;               The resulting FITS files are readable by the same READCDSFITS
;               routine used for the Level-0 FITS files.  There are a few
;               differences between the Level-0 and Level-1 files which should
;               be noted:
;
;               * The keyword DATASRC will read "Level 1 - " followed by the
;                 original value of DATASRC.  This is the primary mechanism
;                 used to distinguish Level-0 and Level-1 files.
;
;               * The boolean keywords ROTATED and ALIGNED are added to the
;                 header to mark whether or not NIS_ROTATE was applied, and
;                 whether the /ALIGN keyword was used.
;
;               * Keywords are added to the header to record the SOHO orbital
;                 position in several coordinate systems (GEI,GSE,GSM,HAE),
;                 plus DSUN_OBS, HGLN_OBS, HGLT_OBS, CRLN_OBS, CRLT_OBS.
;
;               * HISTORY records are added to the header to record the actions
;                 applied to the data.
;
;               * Units strings (TUNIT, TCUNI, etc.) are brought up-to-date
;                 with the World Coordinate System (WCS) specification.
;
;               * The SOHO-specific coordinate keywords are augmented by
;                 standard WCS keywords giving the same information.  This is
;                 done *only* for the actual data windows, and not for the
;                 auxiliary information.
;
;               * A "grism" projection is used in the WCS system for the
;                 spectral axis.  This allows the curvature of the spectral
;                 plate scale to be modeled.  The WCS parameters for the
;                 spectral axis will differ slightly from the TRVAL and TDELT
;                 equivalents.
;
;               * Missing pixels are marked as IEEE "Not-a-Number" (NaN).  This
;                 follows the FITS specification for floating-point data.
;
; Syntax      :	CONV_NIS_TO_LEVEL1, FILENAME
;
; Examples    :	CONV_NIS_TO_LEVEL1,'s840r00.fits'  ;creates 's840r00l1.fits'
;
; Inputs      :	FILENAME = The name of the file to process.
;
; Opt. Inputs :	None.
;
; Outputs     :	A new file is created with "l1" appended to the filename.
;
; Opt. Outputs:	None.
;
; Keywords    :	OUTPATH = Path to write the output file to.  If not passed, the
;                         file is written to the current directory.
;
;               ALT_SLIT6 = If set, call VDS_CALIB with /ALT_SLIT6 option
;
; Calls       :	BREAK_FILE, READCDSFITS, GET_UTC, DATATYPE, VDS_DEBIAS,
;               GT_WINDATA, GT_WINDESC, AVERAGE, CDS_CLEAN_EXP, CDS_CLEAN,
;               VDS_CALIB, NIS_CALIB, NIS_ROTATE, FIND_WITH_DEF, FXHREAD,
;               FXADDPAR, GET_ORBIT, FXPAR, FXHMAKE, FXWRITE, FXBHMAKE,
;               DELVARX, FXBADDCOL, BOOST_ARRAY, NTRIM, PIX2WAVE,
;               WCS_FIT_GRISM, FXBOPEN, FXBREAD, FXBCLOSE, TAG_NAMES,
;               FXBCREATE, FXBWRITE, FXBFINISH
;
; Common      :	None.
;
; Restrictions:	Access to the SOHO orbit files is required to add the orbital
;               data to the header.
;
;               The processing will vary somewhat depending on the type of
;               data.  CDS_CLEAN_EXP is used to remove cosmic rays from
;               unbinned data, while CDS_CLEAN is used for binned data.
;               NIS_ROTATE is not applied for certain exotic compression
;               schemes.  NIS_CALIB is not applied to FULLCCD files.  The
;               HISTORY statements record exactly which routines were applied.
;
; Side effects:	If no VDS background windows are found in the input file, then
;               backgrounds are estimated from the data, and the biases used
;               are documented in a HISTORY record.
;
;               For certain types of rasters, such as sit-and-stare
;               observations, the NIS_ROTATE routine will generate several
;               informational error messages about /MIRROR_SHIFT being ignored.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 19-Mar-2008, William Thompson, GSFC
;               Version 2, 10-Apr-2008, WTT, fixed bug with single exposures
;               Version 3, 15-Apr-2008, WTT, fixed bug with padded data
;               Version 4, 07-Jul-2008, WTT, keep track of unprocessed files
;               Version 5, 31-Jul-2008, WTT, Use /OLDSYS to avoid memory leak
;               Version 6, 16-Oct-2008, WTT, Increase memory limit to 64 MB
;               Version 7, 20-Oct-2008, WTT, Don't call CDS_CLEAN for single
;                                            exposures
;               Version 8, 15-Jul-2010, WTT, Added /ALT_SLIT6 option
;
; Contact     :	WTHOMPSON
;-
;
pro conv_nis_to_level1, filename, outpath=outpath, alt_slit6=alt_slit6
;
if n_params() ne 1 then begin
    message = 'Syntax: CONV_NIS_TO_LEVEL1, FILENAME'
    goto, handle_error
endif
;
;  Form the output filename (without path) from the input filename.
;
break_file, filename, disk, dir, name, ext
outfile = name + 'l1.fits'
;
;  Read in the FITS file.
;
message = ''
qlds = readcdsfits(filename, /oldsys, memlimit=2L^26, errmsg=message)
if message ne '' then goto, handle_error
if qlds.header.detector ne 'NIS' then begin
    message = 'File ' + name + ext + ' is not an NIS file'
    goto, handle_error
endif
;
;  Keep track of the actions applied to these data.
;
get_utc, today, /vms, /truncate
history = 'The following procedures were applied ' + today
;
;  Remove the detector bias.
;
if datatype(qlds.background,1) eq 'Structure' then begin
    vds_debias, qlds, errmsg=message
    if message ne '' then goto, handle_error
    history = [history, 'Applied VDS_DEBIAS']
end else begin
    bias = replicate(9999, 4)
    for i=0,n_elements(qlds.detdesc)-1 do begin
        aa = min( good_pixels( gt_windata(qlds,i), missing=-1) )
        dx = qlds.detdesc[i].detx
        dy = qlds.detdesc[i].dety
        case 1 of
            (dx lt 512) and (dy ge 512): bias[0] = bias[0] < aa
            (dx ge 512) and (dy ge 512): bias[1] = bias[1] < aa
            (dx lt 512) and (dy lt 512): bias[2] = bias[2] < aa
            (dx ge 512) and (dy lt 512): bias[3] = bias[3] < aa
        endcase
    endfor
    if bias[0] eq 9999 then bias[0] = bias[1]
    if bias[1] eq 9999 then bias[1] = bias[0]
    if bias[2] eq 9999 then bias[2] = bias[3]
    if bias[3] eq 9999 then bias[3] = bias[2]
    w = where(bias eq 9999, count)
    if count gt 0 then begin
        bavg = average(bias,missing=9999)
        bias[w] = bavg
    endif
    vds_debias, qlds, bias, errmsg=message
    if message ne '' then goto, handle_error
    history = [history, 'Applied VDS_DEBIAS using estimated biases']
    history = [history, string(bias, format="('Biases: ', 4F15.3)")]
endelse
;
;  Take out cosmic rays.
;
if qlds.header.comp_id lt 6 then begin
    cds_clean_exp, qlds
    history = [history, 'Applied CDS_CLEAN_EXP']
end else if (qlds.header.comp_id eq 8) and (qlds.header.nx gt 1) then begin
    cds_clean, qlds
    history = [history, 'Applied CDS_CLEAN']
endif
;
;  Apply the detector calibration.
;
vds_calib, qlds, alt_slit6=alt_slit6, errmsg=message
if message ne '' then goto, handle_error
history = [history, 'Applied VDS_CALIB']
;
;  Apply the radiometric calibration, except for full CCD images.
;
ny = qlds.detdesc.ixstop[2] - qlds.detdesc.ixstart[2] + 1
if max(ny) lt 1024 then begin
    nis_calib, qlds, errmsg=message
    if message ne '' then goto, handle_error
    history = [history, 'Applied NIS_CALIB']
;
;  Correct for the spectral tilts and mirror effects.
;
    nwave = qlds.detdesc.ixstop[0] - qlds.detdesc.ixstart[0] + 1
    if min(nwave) gt 1 then begin
;
;  Don't apply /MIRROR_SHIFT when only one exposure was taken.
;
        mirror_shift = qlds.header.expcount gt 1
        nis_rotate, qlds, /cubic, mirror_shift=mirror_shift, /bottom, /align, $
          errmsg=message
        if message ne '' then goto, handle_error
        histline = 'Applied NIS_ROTATE, /CUBIC, /BOTTOM, /ALIGN'
        if mirror_shift then histline = histline + ', /MIRROR_SHIFT'
        history = [history, histline]
    endif
endif
;
;  Read in the original primary FITS header.
;
infile = find_with_def(filename, '$CDS_FITS_DATA', '.fits', /reset)
openr, in, infile, /get_lun
fxhread, in, header
free_lun, in
;
;  Update the FILENAME and DATASRC keywords.
;
fxaddpar, header, 'filename', outfile
fxaddpar, header, 'datasrc', 'Level 1 - ' + fxpar(header, 'datasrc')
;
;  Update the pointing keywords.
;
fxaddpar, header, 'ins_x0',   float(qlds.header.ins_x0)
fxaddpar, header, 'ins_y0',   float(qlds.header.ins_y0)
fxaddpar, header, 'ins_roll', float(qlds.header.ins_roll)
fxaddpar, header, 'xcen',     float(qlds.header.xcen)
fxaddpar, header, 'ycen',     float(qlds.header.ycen)
fxaddpar, header, 'angle',    float(qlds.header.angle)
fxaddpar, header, 'ixwidth',  float(qlds.header.ixwidth)
fxaddpar, header, 'iywidth',  float(qlds.header.iywidth)
fxaddpar, header, 'sc_x0',    float(qlds.header.sc_x0)
fxaddpar, header, 'sc_y0',    float(qlds.header.sc_y0)
fxaddpar, header, 'sc_roll',  float(qlds.header.sc_roll)
;
;  Add the SOHO orbit information, if available.
;
orbmsg = ''
orbit = get_orbit(qlds.header.date_obs, errmsg=orbmsg)
if orbmsg eq '' then begin
    comment = 'Geocentric equatorial inertial'
    fxaddpar, header, 'geix_obs', orbit.gci_x*1000, comment + ' X'
    fxaddpar, header, 'geiy_obs', orbit.gci_y*1000, comment + ' Y'
    fxaddpar, header, 'geiz_obs', orbit.gci_z*1000, comment + ' Z'
;
    comment = 'Geocentric solar ecliptic'
    fxaddpar, header, 'gsex_obs', orbit.gse_x*1000, comment + ' X'
    fxaddpar, header, 'gsey_obs', orbit.gse_y*1000, comment + ' Y'
    fxaddpar, header, 'gsez_obs', orbit.gse_z*1000, comment + ' Z'
;
    comment = 'Geocentric solar magnetic'
    fxaddpar, header, 'gsmx_obs', orbit.gsm_x*1000, comment + ' X'
    fxaddpar, header, 'gsmy_obs', orbit.gsm_y*1000, comment + ' Y'
    fxaddpar, header, 'gsmz_obs', orbit.gsm_z*1000, comment + ' Z'
;
    comment = 'Heliocentric Aries Ecliptic'
    fxaddpar, header, 'haex_obs', orbit.hec_x*1000, comment + ' X'
    fxaddpar, header, 'haey_obs', orbit.hec_y*1000, comment + ' Y'
    fxaddpar, header, 'haez_obs', orbit.hec_z*1000, comment + ' Z'
;
    dsun_obs = sqrt(orbit.hec_x^2 + orbit.hec_y^2 + orbit.hec_z^2)*1000
    fxaddpar, header, 'dsun_obs', dsun_obs, 'Distance from Sun'
    hgln_obs = (orbit.hel_lon_soho - orbit.hel_lon_earth) * !radeg
    lon_obs = orbit.hel_lon_soho * !radeg
    lat_obs = orbit.hel_lat_soho * !radeg
    fxaddpar, header, 'hgln_obs', hgln_obs, 'Stonyhurst heliographic longitude'
    fxaddpar, header, 'hglt_obs', lat_obs,  'Stonyhurst heliographic latitude'
    fxaddpar, header, 'crln_obs', lon_obs,  'Carrington heliographic longitude'
    fxaddpar, header, 'crlt_obs', lat_obs,  'Carrington heliographic latitude'
endif
;
;  Add keywords for ROTATED and ALIGNED.
;
rotated = 'F'
if tag_exist(qlds, 'rotated') then if qlds.rotated then rotated = 'T'
fxaddpar, header, 'rotated', rotated, 'True if NIS_ROTATE applied'
aligned = 'F'
if tag_exist(qlds, 'aligned') then if qlds.aligned then aligned = 'T'
fxaddpar, header, 'aligned', aligned, 'True if NIS1 and NIS2 windows aligned'
;
;  Add the HISTORY records.
;
for i=0,n_elements(history)-1 do fxaddpar, header, 'HISTORY', history[i]
;
;  Extract the TTYPE values from the headers.
;
ttype = strtrim(fxpar(qlds.hdrtext, 'ttype*'))
;
;  Initialize the output file with the modified primary header.  Create a
;  binary table extension header.
;
fxhmake, header, /date, /extend
if datatype(outpath,1) eq 'String' then outfile = concat_dir(outpath,outfile)
fxwrite, outfile, header
fxbhmake, header, 1, 'DATA', 'CDS data'
;
;  Step through the data windows, and add a column for each window.  Store the
;  column numbers in DATA_COL.
;
nwindows = n_elements(qlds.detdesc)
delvarx, data_col
for iwindow = 0,nwindows-1 do begin
    aa = gt_windata(qlds, iwindow, /nopadding)
    dd = gt_windesc(qlds, iwindow)
    dim = dd.ixstop - dd.ixstart + 1
    sz = size(aa)
    if sz[0] lt n_elements(dim) then begin
        dim[0] = sz[1:sz[0]]
        aa = reform(aa, dim, /overwrite)
    endif
    fxbaddcol, col, header, aa, dd.label, 'Data window'
    boost_array, data_col, col
    scol = ntrim(col)
;
;  Add the associated keywords for this column.
;
;  Bring the units string in compliance with the WCS specification.
;
    units = strlowcase(dd.units)
    parts = strsplit(units,'/^',/extract)
    w = where(parts eq 'photons', count)
    if count gt 0 then parts[w] = 'photon'
    w = where(strmid(parts,0,3) eq 'sec', count)
    if count gt 0 then parts[w] = 's'
    w = where(strmid(parts,0,6) eq 'sterad', count)
    if count gt 0 then parts[w] = 'sr'
    w = where(strmid(parts,0,3) eq 'ang', count)
    if count gt 0 then parts[w] = 'Angstrom'
    w = where(parts eq 'ergs', count)
    if count gt 0 then parts[w] = 'erg'
    units = parts[0]
    for j=1,n_elements(parts)-1 do begin
        if valid_num(parts[j]) then units=units+'^' else units=units+'/'
        units = units + parts[j]
    endfor
    fxaddpar, header, 'TUNIT'+scol, units, 'Units of column ' + scol
;
    naxes = n_elements(dd.axes)
    if naxes eq 1 then tdesc = dd.axes else begin
        tdesc = '('
        for j=0,naxes-2 do tdesc = tdesc + dd.axes[j] + ','
        tdesc = tdesc + dd.axes[naxes-1] + ')'
    endelse
    fxaddpar, header, 'TDESC'+scol, tdesc, 'Axis labels for column ' + scol
;
    w = where(ttype eq dd.label)
    tcuni = strlowcase(fxpar(qlds.hdrtext, 'TCUNI'+ntrim(w[0]+1)))
    ang = strpos(tcuni, 'angstrom')
    if ang ge 0 then strput, tcuni, 'A', ang
    fxaddpar, header, 'TCUNI'+scol, tcuni, 'Axis units for column ' + scol
;
    amin = min(good_pixels(aa, missing=-100), max=amax)
    fxaddpar, header, 'TDMIN'+scol, amin, 'Minimum value in column ' + scol
    fxaddpar, header, 'TDMAX'+scol, amax, 'Maximum value in column ' + scol
;
    fxaddpar, header, 'TDETX'+scol, dd.detx, 'Detector start column'
    fxaddpar, header, 'TDETY'+scol, dd.dety, 'Detector start row'
;
    if naxes eq 1 then begin
        trpix = '1'
        trval = ntrim(dd.origin)
    end else begin
        trpix = '('
        trval = '('
        for j=0,naxes-2 do begin
            trpix = trpix + '1,'
            trval = trval + ntrim(dd.origin[j]) + ','
        endfor
        trpix = trpix + '1)'
        trval = trval + ntrim(dd.origin[naxes-1]) + ')'
    endelse
    fxaddpar, header, 'TRPIX'+scol, trpix, 'Reference pixel for column ' + scol
    fxaddpar, header, 'TRVAL'+scol, trval, $
      'Preliminary reference values, col. ' + scol
;
    if naxes eq 1 then tdelt = ntrim(dd.spacing) else begin
        tdelt = '('
        for j=0,naxes-2 do tdelt = tdelt + ntrim(dd.spacing[j]) + ','
        tdelt = tdelt + ntrim(dd.spacing[naxes-1]) + ')'
    endelse
    fxaddpar, header, 'TDELT'+scol, tdelt, $
      'Preliminary axis increments, col. ' + scol
;
    if naxes eq 1 then trota = ntrim(dd.rotation) else begin
        trota = '('
        for j=0,naxes-2 do trota = trota + ntrim(dd.rotation[j]) + ','
        trota = trota + ntrim(dd.rotation[naxes-1]) + ')'
    endelse
    fxaddpar, header, 'TROTA'+scol, trota, $
      'Preliminary axis rotations, col. ' + scol
;
    fxaddpar, header, 'TWAVE'+scol, float(dd.wavelength), $
      'Principal wavelength in Angstroms'
    fxaddpar, header, 'TWMIN'+scol, float(dd.wavemin), $
      'Preliminary minimum wavelength in Angstroms'
    fxaddpar, header, 'TWMAX'+scol, float(dd.wavemax), $
      'Preliminary maximum wavelength in Angstroms'
    fxaddpar, header, 'TWBND'+scol, dd.waveband, 'Wavelength band'
    fxaddpar, header, 'TGORD'+scol, dd.gr_order, 'Grating order'
    fxaddpar, header, 'TBINX'+scol, dd.binx, 'Detector binning across columns'
    fxaddpar, header, 'TBINY'+scol, dd.biny, 'Detector binning across rows'
;
;  Add in the WCS coordinate keywords.
;
    fxaddpar, header, 'WCSN'+scol, 'Helioprojective-Cartesian'
    tdesc = fxbtdim(tdesc)
    for j=0,n_elements(tdesc)-1 do begin
        jcol = ntrim(j+1)
        ctyp = tdesc[j]
        case ctyp of
            'WAVELNTH': ctyp = 'WAVE-GRI'
            'SOLAR_X':  ctyp = 'HPLN-TAN'
            'SOLAR_Y':  ctyp = 'HPLT-TAN'
            'DEL_TIME': ctyp = 'TIME'
            ELSE:
        endcase
        fxaddpar, header, jcol+'CTYP'+scol, ctyp, $
          'Axis label along axis '+jcol+' for column '+scol
    endfor
;
    tcuni = fxbtdim(tcuni)
    for j=0,n_elements(tcuni)-1 do begin
        jcol = ntrim(j+1)
        cuni = tcuni[j]
        if cuni eq 'second' then cuni = 's'
        fxaddpar, header, jcol+'CUNI'+scol, cuni, $
          'Axis units along axis '+jcol+' for column '+scol
    endfor
;
    for j=0,n_elements(tcuni)-1 do begin
        jcol = ntrim(j+1)
        fxaddpar, header, jcol+'CRPX'+scol, 1, $
          'Reference pixel along axis '+jcol+' for column '+scol
    endfor
;
    trval = float(fxbtdim(trval))
    for j=0,n_elements(trval)-1 do begin
        jcol = ntrim(j+1)
        fxaddpar, header, jcol+'CRVL'+scol, trval[j], $
          'Reference value along axis '+jcol+' for column '+scol
    endfor
;
    tdelt = float(fxbtdim(tdelt))
    for j=0,n_elements(tdelt)-1 do begin
        jcol = ntrim(j+1)
        fxaddpar, header, jcol+'CDLT'+scol, tdelt[j], $
          'Pixel size along axis '+jcol+' for column '+scol
    endfor
;
;  Add in the PC matrix.
;
    nn = n_elements(tdesc)
    pc = fltarr(nn,nn)
    for j=0,nn-1 do pc[j,j] = 1
    ix = where(tdesc eq 'SOLAR_X')
    iy = where(tdesc eq 'SOLAR_Y')
    angle = qlds.header.angle * !dpi / 180.d0
    cosa = cos(angle)
    sina = sin(angle)
    ratio = tdelt[iy] / tdelt[ix]
    pc[ix,ix] =  cosa
    pc[ix,iy] = -sina * ratio
    pc[iy,ix] =  sina / ratio
    pc[iy,iy] =  cosa
    for j=0,nn-1 do begin
        jcol = ntrim(j+1)
        for k=0,nn-1 do begin
            kcol = ntrim(k+1)
            fxaddpar, header, jcol+kcol+'PC'+scol, pc[j,k], $
              'Transformation matrix for column ' + scol
        endfor
    endfor
;
;  Add in the GRISM parameters.
;
    case dd.waveband of
        1: begin & band='n1' & gr=4.0d6 & end
        2: begin & band='n2' & gr=2.4d6 & end
    endcase
    nwave = dd.ixstop[0] - dd.ixstart[0] + 1
    pix = findgen(nwave)
    wave = pix2wave(band, pix+dd.detx)
    iw = where(tdesc eq 'WAVELNTH')
    wcol = ntrim(iw+1)
    if n_elements(wave) gt 1 then begin
        param = [wave[0], wave[1]-wave[0], gr*dd.gr_order, 0.55, 0, 0]
        wcs_fit_grism, pix, wave, param, coord_type='wave', cunit='Angstrom', $
          lambda=[0.1,0.01,0,0.1,0,0], max_iter=10000, acc=1e-10, /noprint
;
;  Fit again to make sure that CRVAL and CDELT are correct.
;
        wcs_fit_grism, pix, wave, param, coord_type='wave', cunit='Angstrom', $
          lambda=[0.1,0.01,0,0,0,0], max_iter=10000, acc=1e-10, /noprint
        alpha = asin(param[3]) * 180.d0 / !dpi
    end else begin
        param = [wave[0], float(tdelt[iw]), gr*dd.gr_order, 0, 0, 0]
        alpha = 0.0
    endelse
;
    fxaddpar, header, wcol+'CRVL'+scol, param[0]        ;Replace value
    fxaddpar, header, wcol+'CDLT'+scol, param[1]        ;Replace value
    fxaddpar, header, wcol+'V'+scol+'_0', gr, $
      'Grating parameter for column '+scol
    fxaddpar, header, wcol+'V'+scol+'_1', dd.gr_order, $
      'Grating order for column '+scol
    fxaddpar, header, wcol+'V'+scol+'_2', alpha, $
      'Fitted angle of incidence for column '+scol
    fxaddpar, header, wcol+'V'+scol+'_3', 1.0, $
      'Zeroth order refractive term for column '+scol
    fxaddpar, header, wcol+'V'+scol+'_4', 0.0, $
      'First order refractive term for column '+scol
    fxaddpar, header, wcol+'V'+scol+'_5', 0.0, $
      'Out of plane angle for column '+scol
    fxaddpar, header, wcol+'V'+scol+'_6', param[5], $
      'Fitted angle to camera axis for column '+scol
endfor
;
;  Find the background windows from the original file, and copy them verbatim
;  into the output file.  Save the column numbers in BACK_COL
;
wback = where(ttype eq 'BACKGROUND', nback)
if nback gt 0 then begin
    delvarx, back_col
    param = ['TUNIT', 'TNULL', 'TDESC', 'TCUNI', 'TDMIN', 'TDMAX', $
             'TDETX', 'TDETY', 'TRPIX', 'TRVAL', 'TDELT', 'TROTA', $
             'TWAVE', 'TWMIN', 'TWMAX', 'TWBND', 'TGORD', 'TBINX', 'TBINY']
    fxbopen, in, infile, 1
    for i=0,nback-1 do begin
        j = wback[i] + 1
        fxbread, in, aa, j
        fxbaddcol, col, header, aa, 'BACKGROUND', 'Data window'
        boost_array, back_col, col
        scol = ntrim(col)
        jcol = ntrim(j)
        for iparam=0,n_elements(param)-1 do begin
            value = fxpar(qlds.hdrtext, param[iparam]+jcol, comment=comment)
            if param[iparam] eq 'TCUNI' then begin
                value = strlowcase(value)
                ang = strpos(value, 'angstrom')
                if ang ge 0 then strput, value, 'A', ang
            endif
            if param[iparam] eq 'TUNIT' then value='adu'
            fxaddpar, header, param[iparam]+scol, value, comment
        endfor
    endfor
    fxbclose, in
endif
;
;  Find the structure elements between DEL_TIMEDATA and EV_VALIDDATA.  These
;  should be in pairs with names ending in DATA and DESC respectively.  Add a
;  column for each DATA structure.  Save the column numbers in AUX_COL.
;
tags = tag_names(qlds)
i0 = (where(tags eq 'DEL_TIMEDATA'))[0]
i1 = (where(tags eq 'EV_VALIDDATA'))[0]
delvarx, aux_col
for i=i0,i1,2 do begin
    aa = qlds.(i)
    dd = qlds.(i+1)
    label = strtrim(dd.label)
    if label+'DATA' ne tags[i] then begin
        help, label
        help, tags[i]
        message = "Label doesn't match"
        goto, handle_error
    endif
    case label of
        'DEL_TIME': cmnt = 'Delta times in seconds from start of observation'
        'INS_X':    cmnt = 'Preliminary ins. pointing in Solar X'
        'INS_Y':    cmnt = 'Preliminary ins. pointing in Solar Y'
        'OPSLBITS': cmnt = 'OPS L status bits'
        'OPS_L':    cmnt = 'OPS L Coordinate'
        'OPSRBITS': cmnt = 'OPS R status bits'
        'OPS_R':    cmnt = 'OPS R Coordinate'
        'SLITBITS': cmnt = 'Slit position status bits'
        'SLIT_POS': cmnt = 'Slit position'
        'MIR_POS':  cmnt = 'Mirror position'
        'PTCHBITS': cmnt = 'Sun sensor pitch status bits'
        'PITCH':    cmnt = 'Sun sensor pitch'
        'YAW_BITS': cmnt = 'Sun sensor yaw status bits'
        'YAW':      cmnt = 'Sun sensor yaw'
        'EV_LEVEL': cmnt = 'Event level'
        'ONBOARDX': cmnt = 'Onboard calc. Solar west (X) pointing'
        'ONBOARDY': cmnt = 'Onboard calc. Solarnorth (Y) pointing'
        'EV_RECOG': cmnt = 'Event recognition enabled'
        'EV_DETEC': cmnt = 'Event detected'
        'EV_VALID': cmnt = 'Event solar coordinates valid'
    endcase
;
;  Check to see if the datatype is logical or not.
;
    jcol = ntrim((where(ttype eq dd.label))[0] + 1)
    tform = fxpar(qlds.hdrtext, 'TFORM'+jcol)
    logical = strpos(tform, 'L') gt 0
;
    fxbaddcol, col, header, aa, label, cmnt, logical=logical
    boost_array, aux_col, col
    scol = ntrim(col)
;
;  Add the associated keywords for this column.
;
    units = strlowcase(dd.units)
    if strmid(units,0,3) eq 'sec' then units = 's'
    fxaddpar, header, 'TUNIT'+scol, units, 'Units of column ' + scol
;
    tnull = fxpar(qlds.hdrtext, 'TNULL'+jcol, count=nnull)
    if nnull gt 0 then fxaddpar, header, 'TNULL'+scol, tnull, $
      'Null value for column ' + scol
;
    test = fxpar(qlds.hdrtext, 'TDESC'+jcol, count=count)
    if count gt 0 then fxaddpar, header, 'TDESC'+scol, dd.axes[0], $
      'Axis labels for column ' + scol
;
    test = fxpar(qlds.hdrtext, 'TCUNI'+jcol, count=count)
    if count gt 0 then begin
        tcuni = strlowcase(fxpar(qlds.hdrtext, 'TCUNI'+jcol))
        ang = strpos(tcuni, 'angstrom')
        if ang ge 0 then strput, tcuni, 'A', ang
        fxaddpar, header, 'TCUNI'+scol, tcuni, 'Axis units for column ' + scol
    endif
;
    amin = fxpar(qlds.hdrtext, 'TDMIN'+jcol, count=count)
    if count gt 0 then begin
        if nnull eq 0 then tnull = -1
        amin = min(good_pixels(aa,missing=tnull), max=amax)
        if datatype(amin,1) eq 'Byte' then begin
            amin = fix(amin)
            amax = fix(amax)
        endif
        fxaddpar, header, 'TDMIN'+scol, amin, 'Minimum value in column ' + scol
        fxaddpar, header, 'TDMAX'+scol, amax, 'Maximum value in column ' + scol
    endif
;
    test = fxpar(qlds.hdrtext, 'TRPIX'+jcol, count=count)
    if count gt 0 then fxaddpar, header, 'TRPIX'+scol, '1', $
      'Reference pixel for column ' + scol
    test = fxpar(qlds.hdrtext, 'TRVAL'+jcol, count=count)
    if count gt 0 then fxaddpar, header, 'TRVAL'+scol, ntrim(dd.origin[0]), $
      'Preliminary reference values, col. ' + scol
    test = fxpar(qlds.hdrtext, 'TDELT'+jcol, count=count)
    if count gt 0 then fxaddpar, header, 'TDELT'+scol, ntrim(dd.spacing[0]), $
      'Preliminary axis increments, col. ' + scol
    test = fxpar(qlds.hdrtext, 'TROTA'+jcol, count=count)
    if count gt 0 then fxaddpar, header, 'TROTA'+scol, $
      ntrim(qlds.header.angle), 'Preliminary axis rotations, col. ' + scol
endfor
;
;  Create the binary table.
;
fxbcreate, unit, outfile, header
;
;  Write out the data arrays.
;
for i = 0,nwindows-1 do begin
    aa = gt_windata(qlds,i,/nopadding)
    fxbwrite, unit, aa, data_col[i], 1, nanvalue=-100
endfor
;
;  Copy over the background data.
;
if nback gt 0 then begin
    fxbopen, in, infile, 1
    for i=0,nback-1 do begin
        j = wback[i] + 1
        fxbread, in, aa, j
        fxbwrite, unit, aa, back_col[i], 1, nanvalue=-1
    endfor
    fxbclose, in
endif
;
;  Write out the auxilliary data.
;
j = 0
for i=i0,i1,2 do begin
    aa = qlds.(i)
    fxbwrite, unit, aa, aux_col[j], 1
    j = j + 1
endfor
;
;  Close the output file.
;
fxbfinish, unit
return
;
;  Error handling point.
;
handle_error:
message, message, /continue
openw, lun, '$HOME/cds_unprocessed.txt', /append, /get_lun
printf, lun, filename
free_lun, lun
;
end
