	PRO NIS_CALIB, DATA, ERGS=ERGS, STERADIANS=STERADIANS,		$
		ANGSTROMS=K_ANGSTROM, SLITVAR=K_SLITVAR,		$
		SECOND_ORDER=SECOND_ORDER, ERRMSG=ERRMSG, _EXTRA=_EXTRA
;+
; Project     :	SOHO - CDS
;
; Name        :	NIS_CALIB
;
; Purpose     :	Applies calibration factors to NIS images.
;
; Explanation :	Applies the calibration factors for the NIS spectrograph to CDS
;		data.  The resulting data is then in physical units.  If the
;		calibration routines for the VDS detector, VDS_DEBIAS and
;		VDS_CALIB, have not already been called, then this routine will
;		call them automatically.
;
;		A number of databases are consulted to retrieve calibration
;		parameters to apply to the data.  These databases are found in
;		the "calib" subdirectory of the CDS database tree.
;
;		The default units for the returned data are
;
;			PHOTONS/CM^2/SEC/ARCSEC^2
;
;		However, there are other options, which are controllable
;		through keywords.  For example, using the call
;
;			NIS_CALIB, DATA, /ERGS, /STERAD, /ANG
;
;		would return the data in the units
;
;			ERGS/CM^2/SEC/STERAD/ANGSTROM
;
;		Note, however, that the /ANGSTROM keyword is ignored for
;		certain data, as explained below.
;
;		It's important to realize that the units (without /ANGSTROM)
;		only apply when all the data in the line profile is added
;		together.
;
; Use         :	NIS_CALIB, DATA
;
; Inputs      :	DATA	= Structure variable as read by READCDSFITS.
;
; Opt. Inputs :	None.
;
; Outputs     :	DATA	= The calibrated data will be returned as a new
;			  structure variable with the same format as the input.
;
; Opt. Outputs:	None.
;
; Keywords    :	There are a number of options for the units that the data can
;		be returned in.  The following keywords can be used to control
;		the units.  The default is "PHOTONS/CM^2/SEC/ARCSEC^2".
;
;		ERGS	   = If set, then the data is returned in units of ergs
;			     instead of photons.
;
;		STERADIANS = If set, then the data is returned as per steradian
;			     instead of per arcsecond^2.
;
;		ANGSTROMS  = If set, then the data is returned as per Angstrom.
;			     This keyword is ignored when the data has been
;			     summed over wavelength (SUMLINE or SUMWIN
;			     compression) or when the instrument is operated in
;			     spectroheliogram mode with the 90x240 arcsecond
;			     slit.
;
;		Additional keywords are:
;
;		SLITVAR	= If set, then the instrumental intensity variation
;			  along the slit will be removed from the data.
;
;			  As of 21 September 1999, the default is for SLITVAR
;			  to be set.  To not apply this correction, use
;			  SLITVAR=0.
;
;		SECOND_ORDER = If set, then the second-order correction is
;			  applied to the data rather than the first order
;			  correction.  The best way to use this is to first
;			  make a copy of the data, and then apply the first
;			  order correction to one copy, and the second order
;			  correction to the other, e.g.
;
;				VDS_CALIB, DATA
;				DATA2 = COPY_QLDS(DATA)
;				NIS_CALIB, DATA, ...
;				NIS_CALIB, DATA2, /SECOND_ORDER, ...
;
;			  *** WARNING ***   When this keyword is used, all
;			  NIS-2 windows are processed in second order.
;
;		ERRMSG	= If defined and passed, then any error messages will
;			  be returned to the user in this parameter rather than
;			  depending on the MESSAGE routine in IDL.  If no
;			  errors are encountered, then a null string is
;			  returned.  In order to use this feature, ERRMSG must
;			  be defined first, e.g.
;
;				ERRMSG = ''
;				NIS_CALIB, ERRMSG=ERRMSG, ...
;				IF ERRMSG NE '' THEN ...
;
;		In addition, any keyword used by VDS_CALIB can also be passed,
;		such as /SLIT6.
;
; Calls       :	PIX2WAVE, GET_EFFICIENCY, GET_WAVE_EFF, GET_EFF_AREA,
;		FILL_MISSING, DETDATA, DETDESC, ST_WINDATA
;
; Common      :	None.
;
; Restrictions:	Before this procedure can be used, the calibration for the VDS
;		detector must be applied, via the VDS_DEBIAS and VDS_CALIB
;		routines.  If not already done, then this routine will attempt
;		to use the automatic facilities of those routines.
;
; Side effects:	None.
;
; Category    :	Calibration, NIS, Intensity.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 16 September 1996
;
; Modified    :	Version 1, William Thompson, GSFC, 16 September 1996
;		Version 2, William Thompson, GSFC, 22 October 1996
;			Call MESSAGE with /CONTINUE
;		Version 3, William Thompson, GSFC, 28 October 1996
;			Only use /CONTINUE if data has already been calibrated.
;		Version 4, William Thompson, GSFC, 21 January 1997
;			Corrected bug when only one exposure was taken.
;		Version 5, William Thompson, GSFC, 4 August 1997
;			Change units of all windows
;		Version 6, William Thompson, GSFC, 9 October 1998
;			Added keyword SLITVAR
;		Version 7, William Thompson, GSFC, 2 September 1999
;			Allow all VDS_CALIB keywords to pass through.
;		Version 8, William Thompson, GSFC, 21 September 1999
;			Provide error message if cdhsstate database not found
;			Make /SLITVAR the default.
;		Version 9, William Thompson, GSFC, 07-Aug-2002
;			Added keyword SECOND_ORDER
;               Version 10, 11-Mar-2008, WTT, Return for level-1 files
;               Version 11, 16-May-2011, WTT, Fix bug when /ANGSTROM and
;                       /SECOND_ORDER are used together.  Previous versions of
;                       the program would divide by the first-order wavelength,
;                       resulting in brightnesses a factor of 2 too low.
;
; Version     : Version 11, 16-May-2011
;-
;
	ON_ERROR, 2
	MESSAGE = ''
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 1 THEN BEGIN
	    MESSAGE = 'Syntax:  NIS_CALIB, DATA'
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that it is VDS data we're dealing with.
;
	IF DATA.HEADER.DETECTOR NE 'NIS' THEN BEGIN
	    MESSAGE = 'Cannot be used with GIS data'
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that the VDS calibration has been done first.  If it hasn't, then
;  attempt to do it.  Also, check to see if the NIS calibration has already
;  been done.
;
	UNITS = DATA.DETDESC(0).UNITS
	IF STRPOS(UNITS,'ADC') NE -1 THEN BEGIN
	    MESSAGE = ''
	    VDS_CALIB, DATA, _EXTRA=_EXTRA, ERRMSG=MESSAGE
	    IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	END ELSE IF (STRPOS(STRUPCASE(UNITS),'CM^2') NE -1) THEN BEGIN
	    MESSAGE = 'Data has already been calibrated for the NIS.'
	    CONTINUE = 1
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Determine whether or not the result should be returned as per Angstrom.
;  Ignore the /ANGSTROM keyword if the data is summed over wavelength, or if
;  operated in spectroheliogram mode.
;
	ANGSTROM = KEYWORD_SET(K_ANGSTROM)
	IF ANGSTROM THEN BEGIN
	    IF (DATA.HEADER.COMP_ID EQ 6) AND (DATA.HEADER.VDS_ORI EQ 0) $
		    THEN BEGIN
		MESSAGE, 'Summed over wavelength, ignoring /ANGSTROMS ' + $
			'keyword', /CONTINUE
		BELL
		ANGSTROM = 0
	    ENDIF
;
	    IF DATA.HEADER.COMP_ID EQ 7 THEN BEGIN
		MESSAGE, 'Summed over wavelength, ignoring /ANGSTROMS ' + $
			'keyword', /CONTINUE
		BELL
		ANGSTROM = 0
	    ENDIF
;
	    IF DATA.HEADER.SLIT_NUM EQ 6 THEN BEGIN
		MESSAGE, 'Spectroheliogram mode (slit 6), ignoring ' +	$
			'/ANGSTROMS keyword', /CONTINUE
		BELL
		ANGSTROM = 0
	    ENDIF
	ENDIF
;
;  Get the wavelength calibrations in NIS1 and NIS2.
;
	WAVE1 = PIX2WAVE('N1', INDGEN(1024))
	WAVE2 = PIX2WAVE('N2', INDGEN(1024))
	WAVE4 = WAVE2 / 2
;
;  Get the overall efficiency for each spectrum.
;
	GET_EFFICIENCY, DATA.HEADER.DATE_OBS, 'N1', EFF1, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	GET_EFFICIENCY, DATA.HEADER.DATE_OBS, 'N2', EFF2, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	GET_EFFICIENCY, DATA.HEADER.DATE_OBS, 'N4', EFF4, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
;
;  Also get the effective response of the instrument as a function of
;  wavelength.
;
	GET_WAVE_EFF, DATA.HEADER.DATE_OBS, 'N1', C1, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	GET_WAVE_EFF, DATA.HEADER.DATE_OBS, 'N2', C2, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	GET_WAVE_EFF, DATA.HEADER.DATE_OBS, 'N4', C4, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	EFF1 = EFF1(0) * POLY(WAVE1, C1)
	EFF2 = EFF2(0) * POLY(WAVE2, C2)
	EFF4 = EFF4(0) * POLY(WAVE4, C4)
	IF KEYWORD_SET(ERGS) THEN BEGIN
	    EFF1 = EFF1 * WAVE1 * 5.035E7
	    EFF2 = EFF2 * WAVE2 * 5.035E7
	    EFF4 = EFF4 * WAVE4 * 5.035E7
	ENDIF
;
;  Combine the two into a single 1024x1024 array.
;
	EFF = FLTARR(1024,1024)
	EFF(*,512:*) = EFF1 # REPLICATE(1,512)
	EFF(*,0:511) = EFF2 # REPLICATE(1,512)
	IF KEYWORD_SET(SECOND_ORDER) THEN	$
		EFF(*,0:511) = EFF4 # REPLICATE(1,512)
;
;  If the STERADIANS keyword was set, then multiply by the conversion between
;  steradians and arcseconds.
;
	IF KEYWORD_SET(STERADIANS) THEN EFF = EFF * (!PI/(180.*60.^2))^2
;
;  Get the effective areas as a function of mirror position.
;
	GET_EFF_AREA, DATA.HEADER.DATE_OBS, 'N1', EFF_AREA1, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	GET_EFF_AREA, DATA.HEADER.DATE_OBS, 'N2', EFF_AREA2, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
;
;  Get the mirror position data, and interpolate any missing values.
;
	MIR_POS = DATA.MIR_POSDATA
	IF N_ELEMENTS(MIR_POS) GT 1 THEN	$
		FILL_MISSING, MIR_POS, DATA.MIR_POSDESC.MISSING
	MIR_POS = 0 > MIR_POS < 255
;
;  Get the information needed to remove the instrumental intensity variation
;  along the slit.
;
	IF N_ELEMENTS(K_SLITVAR) EQ 1 THEN SLITVAR = K_SLITVAR ELSE SLITVAR = 1
	IF KEYWORD_SET(SLITVAR) THEN BEGIN
	    P_MG = [0.209739, -0.0197366, 0.000499764, -1.63603e-06]
	    P_OV = [4.41134,  -0.126212,  0.00108363,  -2.44066e-06]
	    PAR1MG = [7.9317160, -0.046669731, 6.9053410e-05]
	    PAR2MG = [-3.2443119, 0.011758351, -1.0472979e-05]
	    PAR2OV = [0.90745100, -0.0033384999, 3.1203014e-06]
;
;  Get the slant information.  Normally taken from state database, but in case
;  sites do not have that, then hard code here.
;
	    IF FIND_WITH_DEF('cdhsstate.dbf','$ZDBASE') NE '' THEN BEGIN
		GET_VDS_SLITPOS, DATA.HEADER.DATE_OBS, DESC
		N1_CEN_S = DESC.N1_CEN_S  &  N1_CEN_L = DESC.N1_CEN_L
		N2_CEN_S = DESC.N2_CEN_S  &  N2_CEN_L = DESC.N2_CEN_L
	    END ELSE BEGIN
		MESSAGE, /CONTINUE, 'Database cdhsstate not found, ' + $
			'continuing with default values'
		N1_CEN_S = 643  &  N1_CEN_L = 660
		N2_CEN_S = 402  &  N2_CEN_L = 412
	    ENDELSE
	ENDIF
;
;  Determine the number of windows, and step through them.
;
	NWIN = N_ELEMENTS(DATA.DETDESC)
        FOR IWIN = 0,NWIN-1 DO $
		IF DATA.DETDESC(IWIN).IXSTOP(0) GE 0 THEN BEGIN
;
;  Extract the data array from the input structure.  Save the dimensions for
;  later.  Convert to at least floating point.
;  Matches ST_WINDATA,DATA,IWIN,ARRAY
           
	    ARRAY = DETDATA(DATA, IWIN+1)
            DESC  = DETDESC(DATA, IWIN+1)
	    SA = SIZE(ARRAY)
	    IF DATATYPE(ARRAY,2) LT 4 THEN ARRAY = FLOAT(ARRAY)
;
;  Save the positions of any missing pixels.
;
	    MISSING = DESC.MISSING
	    W_MISSING = WHERE(ARRAY EQ MISSING, N_MISSING)
;
;  Extract the start and stop indices, and the detector positions.
;  When ST_WINDATA is used to store data, only the size matters
;
	    ISTART = REPLICATE(0,4)
	    ISTOP  = REPLICATE(0,4)  &  ISTOP(0) = SA(1:SA(0))-1
	    DX = DESC.DETX
	    DY = DESC.DETY
	    BINX = DESC.BINX > 1
	    BINY = DESC.BINY > 1
;
	    I1 = 0  &  NI = ISTOP(0)-ISTART(0)+1  &  I2 = NI-1
	    J1 = 0  &  NJ = ISTOP(1)-ISTART(1)+1  &  J2 = NJ-1
	    K1 = 0  &  NK = ISTOP(2)-ISTART(2)+1  &  K2 = NK-1
	    L1 = 0  &  NL = ISTOP(3)-ISTART(3)+1  &  L2 = NL-1
;
;  From the DETY value, one can determine the spectral range, and which
;  effective area to use.  Also determine the pixel width in Angstroms.
;
	    SG = 'N' + TRIM(2-FIX(DY/512))
	    IF SG EQ 'N1' THEN BEGIN
		EFF_AREA = EFF_AREA1(MIR_POS)
		DWAVE = BINX * (WAVE1(DX+NI*BINX-1) - WAVE1(DX)) /	$
			FLOAT(NI*BINX-1)
	    END ELSE BEGIN
		EFF_AREA = EFF_AREA2(MIR_POS)
		DWAVE = BINX * (WAVE2(DX+NI*BINX-1) - WAVE2(DX)) /	$
			FLOAT(NI*BINX-1)
            ENDELSE
            IF KEYWORD_SET(SECOND_ORDER) THEN DWAVE = DWAVE / 2
;
;  Depending on the axis titles, determine the relationship between the data
;  and the response image.  The first (I) axis is wavelength, and the third (K)
;  is solar Y.
;
	    AXES = DESC.AXES
	    SUBEFF = EFF(DX:DX+NI*BINX-1, DY:DY+NK*BINY-1)
;
;  If SLITVAR has been selected, then calculate the effect the slit would have,
;  and modify the response image accordingly.
;
	    IF KEYWORD_SET(SLITVAR) THEN BEGIN
		IF SG EQ 'N1' THEN BEGIN
		    N_CEN_S = N1_CEN_S
		    N_CEN_L = N1_CEN_L
		    PAR_MG = PAR1MG
		    PAR_OV = 0
		END ELSE BEGIN
		    N_CEN_S = N2_CEN_S
		    N_CEN_L = N2_CEN_L
		    PAR_MG = PAR2MG
		    PAR_OV = PAR2OV
		ENDELSE
		XX = (N_CEN_L - N_CEN_S) / 1023.
		XX = (DX + FINDGEN(NI*BINX)) # REPLICATE(XX, NK*BINY)
		YY = DY - N_CEN_S + 71.
		YY = REPLICATE(1, NI*BINX) # (YY + FINDGEN(NK*BINY))
		YY = TEMPORARY(YY) - TEMPORARY(XX)
		XX = PIX2WAVE(SG,DX+FINDGEN(NI*BINX)) # REPLICATE(1, NK*BINY)
		A_MG = POLY(XX,PAR_MG)
		A_OV = POLY(XX,PAR_OV)
		TEMP = (1. - A_MG - A_OV) + A_MG * POLY(YY,P_MG) +	$
			A_OV * POLY(YY,P_OV)
		SUBEFF = TEMPORARY(SUBEFF) * TEMPORARY(TEMP)
	    ENDIF
;
;  If the image was binned, then bin (average) the response image as well.
;
	    IF (BINX GT 1) OR (BINY GT 1) THEN	$
		    SUBEFF = REBIN(SUBEFF,NI,NK)
;
;  If necessary, extend the response image in the second dimension.
;
	    IF J1 NE J2 THEN BEGIN
		SUBEFF = REFORM(SUBEFF,N_ELEMENTS(SUBEFF), /OVERWRITE)
		SUBEFF = TEMPORARY(SUBEFF) # EFF_AREA
		SUBEFF = REFORM(SUBEFF,NI,NK,NJ,/OVERWRITE)
		SUBEFF = REARRANGE(SUBEFF, [1,3,2])
	    ENDIF
;
;  If necessary, extend the response image in the fourth dimension.
;
	    IF L1 NE L2 THEN BEGIN
		SUBEFF = REFORM(SUBEFF,N_ELEMENTS(SUBEFF), /OVERWRITE)
		SUBEFF = TEMPORARY(SUBEFF) # EFF_AREA
		SUBEFF = REFORM(SUBEFF,NI,NJ,NK,NL,/OVERWRITE)
	    ENDIF
;
;  Determine the size of a pixel in square arcseconds.
;
	    CASE DATA.HEADER.SLIT_NUM OF
		1:  SXWIDTH = 2
		2:  SXWIDTH = 4
		3:  SXWIDTH = 8
		4:  SXWIDTH = 2
		5:  SXWIDTH = 4
		6:  SXWIDTH = 1.68*BINX	;Per pixel, in spectroheliogram mode
		ELSE: BEGIN
			MESSAGE = 'Unrecognized slit number'
			GOTO, HANDLE_ERROR
			END
	    ENDCASE
	    SUBEFF = SUBEFF * SXWIDTH * 1.68*BINY
;
;  If data is to be returned as per Angstrom, then include the pixel width in
;  Angstroms.
;
	    IF ANGSTROM THEN SUBEFF = SUBEFF * DWAVE
;
;  Divide in the response image.
;
	    ARRAY(I1,J1,K1,L1) = ARRAY(I1:I2,J1:J2,K1:K2,L1:L2) / SUBEFF
;
;  Make sure that ARRAY has the proper dimensions.
;
	    ARRAY = REFORM(ARRAY, SA(1:SA(0)), /OVERWRITE)
;
;  Replace any missing pixels with the value -100.
;
	    IF N_MISSING GT 0 THEN ARRAY(W_MISSING) = -100.0
;
;  Put the data back into the structure.
;
            ST_WINDATA,DATA,IWIN,ARRAY,/NO_COPY
;
;  Change the missing pixel flag in the structure.
;
	    DATA.DETDESC(IWIN).MISSING = -100.0
	ENDIF		;IWIN valid
;
;  Change the labels for the units.  This should apply to all windows, so that
;  there is no possibility of confusion.
;
	IF KEYWORD_SET(ERGS) THEN UNITS = 'ERGS' ELSE UNITS = 'PHOTONS'
	UNITS = UNITS + '/CM^2/SEC'
	IF KEYWORD_SET(STERADIANS) THEN UNITS = UNITS + '/STERAD' ELSE $
		UNITS = UNITS + '/ARCSEC^2'
	IF KEYWORD_SET(ANGSTROM) THEN UNITS = UNITS + '/ANGSTROM'
	DATA.DETDESC.UNITS = UNITS
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'NIS_CALIB: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, CONTINUE=KEYWORD_SET(CONTINUE)
;
;  Exit point.
;
FINISH:
	RETURN
	END
