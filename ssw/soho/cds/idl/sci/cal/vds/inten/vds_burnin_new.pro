	PRO VDS_BURNIN_NEW, DATA, FLAT, NOBURNIN=NOBURNIN, ERRMSG=ERRMSG, $
		FLIGHT=FLIGHT, SLIT6=K_SLIT6, NOPRESLIT6=NOPRESLIT6
;+
; Project     :	SOHO - CDS
;
; Name        :	VDS_BURNIN_NEW
;
; Purpose     :	Adds burn-in to flat field for post-recovery VDS images.
;
; Explanation :	Called from VDS_CALIB to modify the flat field to include the
;		burn-in effects for VDS data taken after recovery of the SOHO
;		spacecraft in October 1998.
;
; Use         :	VDS_BURNIN_NEW, DATA, FLAT
;
; Inputs      :	DATA	= Structure variable as read by READCDSFITS.
;
;		FLAT	= Flat field data from VDS_CALIB.
;
; Opt. Inputs :	None.
;
; Outputs     :	FLAT	= The modified flat field.
;
; Opt. Outputs:	None.
;
; Keywords    :	NOBURNIN = If set, then the burn-in correction is not applied.
;			   This is mainly used when analyzing the burn-in
;			   characteristics in AN_NIMCP.
;
;		NOPRESLIT6 = If set, then the burnin from the use of slit 6
;			   prior to the accident is not removed from the data.
;			   This is mainly used for analyzing the burn-in
;			   characteristics.
;
;		SLIT6  = If set, then an adjustment is made for the estimated
;			 burn-in effect of the use of 90 arcsecond wide "movie"
;			 slit.  This correction is based on an analysis of the
;			 average behavior of the 584 line as seen in the daily
;			 synoptic images.  The amount of exposure elsewhere is
;			 assumed to be proportional to an average slit 6
;			 quiet-sun profile.
;
;			 Note that the use of the /SLIT6 keyword is unrelated
;			 to whether or not the current observation uses slit 6.
;			 Instead, it is intended to correct, as best as
;			 possible, for the cummulative effect that slit 6 has
;			 on the detector over time.
;
;			 If both NOBURNIN and SLIT6 is set, then only the
;			 burnin from the use of slit 6 prior to the accident
;			 will be removed from the data.
;
;			 As of 29 August 2000, the default is for SLIT6 to
;			 be set.  To not apply this correction, use SLIT6=0.
;
;		ERRMSG = If defined and passed, then any error messages will be
;			 returned to the user in this parameter rather than
;			 depending on the MESSAGE routine in IDL.  If no errors
;			 are encountered, then a null string is returned.  In
;			 order to use this feature, ERRMSG must be defined
;			 first, e.g.
;
;				ERRMSG = ''
;				VDS_CALIB, ERRMSG=ERRMSG, ...
;				IF ERRMSG NE '' THEN ...
;
;               FLIGHT = Use Sep 1997 flight flatfield for OV, HeI and MgIX
;
; Calls       :	FIND_WITH_DEF, FXREAD, SAFE_EXP
;
; Common      :	None.
;
; Restrictions:	Should only be called from VDS_CALIB.
;
; Side effects:	None.
;
; Category    :	Calibration, VDS, Intensity.
;
; Prev. Hist. :	Previously done within VDS_CALIB
;
; Written     :	William Thompson, GSFC, 4 Jan 1999
;
; Modified    :	Version 1, 04-Jan-1999, William Thompson, GSFC
;		Version 2, 30-Sep-1999, William Thompson, GSFC
;			Fixed bug where post-recovery burnin was vignetted.
;			Changed way that pre-recovery SLIT6 effect is
;			estimated.  Added keyword NOPRESLIT6.
;			Provide error messages if needed files not found.
;		Version 3, 12-Jan-2000, William Thompson, GSFC
;			Use broadened Gaussians when NIMCP.POST=2
;		Version 4, 20-Jul-2000, William Thompson, GSFC
;			Support additional SLIT6 pre-accident calibration files
;			beyond version 1.  For versions 2 and beyond, multiply
;			by 0.813822, as with the narrow slit burn-ins.
;		Version 5, 29-Aug-2000, William Thompson, GSFC
;			Apply SLIT6 post-recovery burn-in corrections.
;			Make /SLIT6 the default.
;		Version 6, 30-Apr-2002, William Thompson, GSFC
;			Include break point for slit6 use in post-recovery
;			period.  Use latest defaults.
;		Version 7, 21-Aug-2003, William Thompson, GSFC
;			Support additional post-recovery function
;
; Version     : Version 7, 21-Aug-2003
;-
;
	ON_ERROR, 2
	IF N_ELEMENTS(K_SLIT6) EQ 1 THEN SLIT6 = K_SLIT6 ELSE SLIT6 = 1
;
;  Get the slant information.  Normally taken from state database, but in case
;  sites do not have that, then hard code here.
;
	IF FIND_WITH_DEF('cdhsstate.dbf','$ZDBASE') NE '' THEN BEGIN
	    GET_VDS_SLITPOS, DATA.HEADER.DATE_OBS, DESC
	    N1_CEN_S = DESC.N1_CEN_S  &  N1_CEN_L = DESC.N1_CEN_L
	    N2_CEN_S = DESC.N2_CEN_S  &  N2_CEN_L = DESC.N2_CEN_L
	END ELSE BEGIN
	    MESSAGE, /CONTINUE, 'Database cdhsstate not found, ' + $
		    'continuing with default values'
	    N1_CEN_S = 643  &  N1_CEN_L = 660
	    N2_CEN_S = 402  &  N2_CEN_L = 412
	ENDELSE
;
;  Specify the original slant parameters, before SOHO loss of contact.
;
	N1_CEN_S0 = 643  &  N1_CEN_L0 = 660
	N2_CEN_S0 = 402  &  N2_CEN_L0 = 412
;
;  Get the tilt information.
;
	GET_TILTCAL, 1, DATA.HEADER.DATE_OBS, TILT1
	GET_TILTCAL, 2, DATA.HEADER.DATE_OBS, TILT2
;
;  Define a vignetted slit.
;
	GSLIT = REPLICATE(1., 151)
	GSLIT(0:6) = (1 - COS(!PI*(FINDGEN(7)/6.))) / 2.
	GSLIT(144:*) = REVERSE(GSLIT(0:6))
;
;  Correct the flat field image for the burn in factors.
;
	IF NOT KEYWORD_SET(NOBURNIN) THEN BEGIN
	    MSG = ''
	    GET_NIMCP, DATA.HEADER.DATE_OBS, NIMCP, ERRMSG=MSG
	    IF MSG NE '' THEN MESSAGE, MSG, /CONTINUE ELSE BEGIN
		X = FINDGEN(1024)
		Y1 = REPLICATE(1.D0, 1024, 512)
		Y2 = Y1
;
;  Avoid those lines already done in /FLIGHT mode.
;
		DOIT = INTARR(N_ELEMENTS(NIMCP))+1
		IF KEYWORD_SET(FLIGHT) THEN BEGIN
		    DONE_DET = ['N1','N2','N2']
		    DONE_PIX = [862, 610, 997]
		    FOR J = 0,N_ELEMENTS(DONE_DET)-1 DO BEGIN
			WW = WHERE((NIMCP.DETECTOR EQ DONE_DET(J)) AND	$
				(ABS(NIMCP.CENTER - DONE_PIX(J)) LT 1), COUNT)
			IF COUNT EQ 1 THEN DOIT(WW) = 0
		    ENDFOR
		ENDIF
;
		FOR I=0,N_ELEMENTS(NIMCP)-1 DO BEGIN
		    IF DOIT(I) THEN BEGIN
			X1 = (NIMCP(I).CENTER - 50) > 0
			X2 = (X1 + 101) < 1023
			X0 = NIMCP(I).CENTER
			XX = (X(X1:X2) - X0) / NIMCP(I).WIDTH
;
			IF NIMCP(I).POST EQ 2 THEN BEGIN
			    IF NIMCP(I).DETECTOR EQ 'N1' THEN BEGIN
				WING = 0.8
				ASYM = 1.0
			    END ELSE BEGIN
				WING = 0.317
				ASYM = 0.279
			    ENDELSE
			    COMP_BGAUSS, XX, [NIMCP(I).DEPTH,0,1,WING,ASYM], G
;
;  If the SHAPE parameter is non-zero, then modify the profile accordingly.
;  First, readjust the width so that the FWHM doesn't change.
;
			END ELSE BEGIN
			    IF NIMCP(I).SHAPE NE 0 THEN BEGIN
				SHAPE = ABS(NIMCP(I).SHAPE)
				XX = XX * (1 + SQRT(1 + 2*SHAPE/ALOG(2))) / 2.
				XX = XX^2 / (ABS(XX) + SHAPE)
			    ENDIF
			    G = NIMCP(I).DEPTH * SAFE_EXP(-XX^2/2)
			ENDELSE
;
			IF NIMCP(I).POST NE 0 THEN	$
				G = G # REPLICATE(1.0, 151) ELSE G = G # GSLIT
			IF NIMCP(I).DETECTOR EQ 'N1' THEN BEGIN
			    TILT = POLY(X0, TILT1)
			    IF NIMCP(I).POST NE 0 THEN Y0 = N1_CEN_S +	  $
				    (N1_CEN_L-N1_CEN_S) * X0 / 1023.	  $
				     ELSE Y0 = N1_CEN_S0 +		  $
				    (N1_CEN_L0-N1_CEN_S0) * X0 / 1023.
			    Y0 = ROUND(Y0 - 512)
			    KX = [[-TILT*75, TILT], [1, 0]]
			    KY = [[0, 1], [0, 0]]
			    G = POLY_2D(G, KX, KY, 1)
			    Y1(X1:X2,Y0-75:Y0+75) = Y1(X1:X2,Y0-75:Y0+75) + G
			END ELSE BEGIN
			    TILT = POLY(X0, TILT2)
			    IF NIMCP(I).POST NE 0 THEN Y0 = N2_CEN_S +	  $
				    (N2_CEN_L-N2_CEN_S) * X0 / 1023.	  $
				     ELSE Y0 = N2_CEN_S0 +		  $
				    (N2_CEN_L0-N2_CEN_S0) * X0 / 1023.
			    Y0 = ROUND(Y0)
			    KX = [[-TILT*75, TILT], [1, 0]]
			    KY = [[0, 1], [0, 0]]
			    G = POLY_2D(G, KX, KY, 1)
			    Y2(X1:X2,Y0-75:Y0+75) = Y2(X1:X2,Y0-75:Y0+75) + G
			ENDELSE
		    ENDIF
		ENDFOR
		FLAT(*,512:*) = FLAT(*,512:*) * Y1
		FLAT(*,0:511) = FLAT(*,0:511) * Y2
	    ENDELSE
	ENDIF
;
;  Remove the estimated slit 6 burn-in from prior to the accident.
;
	IF (KEYWORD_SET(SLIT6) OR (NOT KEYWORD_SET(NOBURNIN))) AND	$
		(NOT KEYWORD_SET(NOPRESLIT6)) THEN BEGIN
	    CALIB = 1
	    FILE = ''
	    TRY_NEXT = 1
	    WHILE (FILE EQ '') AND (CALIB GE 1) DO BEGIN
		NAME = 'synop_cal_pre' + NTRIM(CALIB) + '.dat'
		FILE = FIND_WITH_DEF(NAME, '$CDS_VDS_CAL_INT')
		IF (FILE EQ '') THEN BEGIN
		    CALIB = CALIB - 1
		    TRY_NEXT = 0
		END ELSE IF TRY_NEXT THEN BEGIN
		    FILE = ''
		    CALIB = CALIB + 1
		ENDIF
	    ENDWHILE
;
	    IF FILE NE '' THEN BEGIN
		OPENR, UNIT, FILE, /GET_LUN
		LINE = 'String'
		READF, UNIT, LINE
		TAI0 = UTC2TAI(LINE)
		READF, UNIT, LINE
		TAI1 = UTC2TAI(LINE)
		READF, UNIT, LINE
		CORR = DOUBLE(LINE)
		READF, UNIT, LINE
		FREE_LUN, UNIT
		LINE = STRCOMPRESS(STRTRIM(LINE,2))
		PD = DOUBLE(STR_SEP(LINE,' '))
	    END ELSE BEGIN
		MESSAGE, /CONTINUE, 'Unable to find file ' +	$
			'synop_cal_pre1.dat, continuing with ' +$
			'default values'
		TAI0 = UTC2TAI('1996-03-21T12:00')
		TAI1 = UTC2TAI('1997-08-24T18:30')
		CORR = 0.28742297
		PD = [0., -6.1323707D-09, 2.3794758D-17]
		CALIB = 2
	    ENDELSE
	    IF CALIB GE 2 THEN PD = 0.813822 * PD
;
	    DTAI = UTC2TAI('1998-Jun-24 11:45')
	    IF DTAI GT TAI1 THEN DTAI = TAI1 + (DTAI-TAI1)*CORR
	    DTAI = DTAI - TAI0
;
	    FILE = FIND_WITH_DEF('avg_slit6.fits', '$CDS_VDS_CAL_INT')
	    IF FILE NE '' THEN FXREAD, FILE, AVG_VAL ELSE BEGIN
		MESSAGE, /CONTINUE, 'File avg_slit6.fits not found.' +$
			'  No pre-accident SLIT6 correction can be made.'
		AVG_VAL = REPLICATE(0.,1024,2)
	    ENDELSE
	    AVG_VAL = POLY(AVG_VAL*DTAI, PD) < 0
;
;  First, do NIS-1.
;
	    KX = REPLICATE(0*TILT1(0), N_ELEMENTS(TILT1)+1,	$
		    N_ELEMENTS(TILT1)+1)
	    KY = KX
	    KX(0,1) = 1
	    Y0 = (N1_CEN_S0 + N1_CEN_L0) / 2. - 512
	    SLANT = (N1_CEN_L0 - N1_CEN_S0) / 1024.
	    FOR I = 0,N_ELEMENTS(TILT1)-1 DO BEGIN
		KX(0,I) = KX(0,I) - TILT1(I)*(Y0 - SLANT*511.5)
		KX(0,I+1) = KX(0,I+1) - SLANT*TILT1(I)
		KX(1,I) = KX(1,I) + TILT1(I)
	    ENDFOR
	    KY(0,0) = [[SLANT*511.5, 1], [-SLANT, 0]]
	    G = FLTARR(512)
	    G(Y0-75) = GSLIT
	    G = AVG_VAL(*,0) # G
	    G = POLY_2D(G, KX, KY, 1)
	    FLAT(*,512:*) = FLAT(*,512:*) * (1 + G)
;
;  Next, do NIS-2.
;
	    KX = REPLICATE(0*TILT2(0), N_ELEMENTS(TILT2)+1,	$
		    N_ELEMENTS(TILT2)+1)
	    KY = KX
	    KX(0,1) = 1
	    Y0 = (N2_CEN_S0 + N2_CEN_L0) / 2.
	    SLANT = (N2_CEN_L0 - N2_CEN_S0) / 1024.
	    FOR I = 0,N_ELEMENTS(TILT2)-1 DO BEGIN
		KX(0,I) = KX(0,I) - TILT2(I)*(Y0 - SLANT*511.5)
		KX(0,I+1) = KX(0,I+1) - SLANT*TILT2(I)
		KX(1,I) = KX(1,I) + TILT2(I)
	    ENDFOR
	    KY(0,0) = [[SLANT*511.5, 1], [-SLANT, 0]]
	    G = FLTARR(512)
	    G(Y0-75) = GSLIT
	    G = AVG_VAL(*,1) # G
	    G = POLY_2D(G, KX, KY, 1)
	    FLAT(*,0:511) = FLAT(*,0:511) * (1 + G)
	ENDIF
;
;  If the SLIT6 keyword was set, then adjust the data for the estimated burn-in
;  from the use of slit 6.
;
	IF KEYWORD_SET(SLIT6) AND (NOT KEYWORD_SET(NOBURNIN)) THEN BEGIN
	    CALIB = 1
	    FILE = ''
	    TRY_NEXT = 1
	    WHILE (FILE EQ '') AND (CALIB GE 1) DO BEGIN
		NAME = 'synop_cal_post' + NTRIM(CALIB) + '.dat'
		FILE = FIND_WITH_DEF(NAME, '$CDS_VDS_CAL_INT')
		IF (FILE EQ '') THEN BEGIN
		    CALIB = CALIB - 1
		    TRY_NEXT = 0
		END ELSE IF TRY_NEXT THEN BEGIN
		    FILE = ''
		    CALIB = CALIB + 1
		ENDIF
	    ENDWHILE
;
	    IF FILE NE '' THEN BEGIN
		OPENR, UNIT, FILE, /GET_LUN
		LINE = 'String'
		READF, UNIT, LINE
		TAI0 = UTC2TAI(LINE)
		READF, UNIT, LINE
		LINE = STRCOMPRESS(STRTRIM(LINE,2))
		PD0 = DOUBLE(STR_SEP(LINE,' '))
		READF, UNIT, LINE
		TAI1 = UTC2TAI(LINE)
		READF, UNIT, LINE
		LINE = STRCOMPRESS(STRTRIM(LINE,2))
		PD1 = DOUBLE(STR_SEP(LINE,' '))
		IF NOT EOF(UNIT) THEN BEGIN
		    READF, UNIT, LINE
		    TAI2 = UTC2TAI(LINE)
		    READF, UNIT, LINE
		    CORR = DOUBLE(LINE)
		END ELSE BEGIN
		    TAI2 = UTC2TAI('2000-10-28T21:24')
		    CORR = 0.37890455
                ENDELSE
                IF NOT EOF(UNIT) THEN BEGIN
                    READF, UNIT, LINE
                    TAI13 = UTC2TAI(LINE)
                    READF, UNIT, LINE
                    TAI3 = UTC2TAI(LINE)
                    READF, UNIT, LINE
                    LINE = STRCOMPRESS(STRTRIM(LINE,2))
                    PD3 = DOUBLE(STR_SEP(LINE,' '))
                END ELSE TAI13 = -1
		FREE_LUN, UNIT
	    END ELSE BEGIN
		MESSAGE, /CONTINUE, 'Unable to find file ' +	$
			'synop_cal_post1.dat, continuing with ' +$
			'default values'
		TAI0 = UTC2TAI('1998-10-20')
		PD0 = [-0.21872588D0, 0.38634808D0, 1361070.8D0]
		TAI1 = UTC2TAI('1999-02-03')
		PD1 = [-0.37388750D0, 0.32717568D0, 14486281.D0]
		TAI2 = UTC2TAI('2000-10-28T21:24')
		CORR = 0.37890455
	    ENDELSE
;
	    DTAI = UTC2TAI(DATA.HEADER.DATE_OBS)
	    IF DTAI GT TAI2 THEN DTAI = TAI2 + (DTAI-TAI2)*CORR
	    IF DTAI GT TAI1 THEN BEGIN
		DTAI = DTAI - TAI1
		PD = PD1
	    END ELSE BEGIN
		DTAI = DTAI - TAI0
		PD = PD0
	    ENDELSE
;
	    FILE = FIND_WITH_DEF('avg_slit6_post.fits', '$CDS_VDS_CAL_INT')
	    IF FILE NE '' THEN FXREAD, FILE, AVG_VAL ELSE BEGIN
		MESSAGE, /CONTINUE, 'File avg_slit6_post.fits not found.' +$
			'  No post-recovery SLIT6 correction can be made.'
		AVG_VAL = REPLICATE(0.,1024,2)
	    ENDELSE
            DTAI = AVG_VAL*DTAI
	    AVG_VAL = NIMCP_DEPTH_FUNCT(DTAI, PD) < 0
            IF TAI13 GT 0 THEN BEGIN
                W = WHERE(DTAI GT TAI13-TAI3, COUNT)
                IF COUNT GT 0 THEN      $
                        AVG_VAL(W) = NIMCP_DEPTH_FUNCT(DTAI(W),PD3) < 0
            ENDIF
;
;  First, do NIS-1.
;
	    KX = REPLICATE(0*TILT1(0), N_ELEMENTS(TILT1)+1,	$
		    N_ELEMENTS(TILT1)+1)
	    KY = KX
	    KX(0,1) = 1
	    Y0 = (N1_CEN_S + N1_CEN_L) / 2. - 512
	    FOR I = 0,N_ELEMENTS(TILT1)-1 DO BEGIN
		KX(0,I) = KX(0,I) + TILT1(I)*Y0
		KX(1,I) = KX(1,I) - TILT1(I)
	    ENDFOR
	    KY(0,0) = [[0, 1], [0, 0]]
	    G = AVG_VAL(*,0) # REPLICATE(1,512)
	    G = POLY_2D(G, KX, KY, 1)
	    FLAT(*,512:*) = FLAT(*,512:*) * (1 + G)
;
;  Next, do NIS-2.
;
	    KX = REPLICATE(0*TILT2(0), N_ELEMENTS(TILT2)+1,	$
		    N_ELEMENTS(TILT2)+1)
	    KY = KX
	    KX(0,1) = 1
	    Y0 = (N2_CEN_S + N2_CEN_L) / 2.
	    FOR I = 0,N_ELEMENTS(TILT2)-1 DO BEGIN
		KX(0,I) = KX(0,I) + TILT2(I)*Y0
		KX(1,I) = KX(1,I) - TILT2(I)
	    ENDFOR
	    KY(0,0) = [[0, 1], [0, 0]]
	    G = AVG_VAL(*,1) # REPLICATE(1,512)
	    G = POLY_2D(G, KX, KY, 1)
	    FLAT(*,0:511) = FLAT(*,0:511) * (1 + G)
	ENDIF
;
	RETURN
	END
