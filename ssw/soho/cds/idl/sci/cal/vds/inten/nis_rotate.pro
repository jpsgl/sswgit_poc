	PRO NIS_ROTATE, DATA, BILINEAR=K_BILINEAR, CUBIC=CUBIC, BOTTOM=BOTTOM
;+
; Project     :	SOHO - CDS
;
; Name        :	NIS_ROTATE
;
; Purpose     :	Applies slant and tilt corrections to VDS images.
;
; Explanation :	Applies the slant and tilt corrections of VDS_ROTATE to all the
;		windows in a CDS/NIS data structure.  The tilt values are taken
;		from GET_TILTCAL.
;
; Use         :	NIS_ROTATE, DATA
;
; Inputs      :	DATA	= Structure variable as read by READCDSFITS.
;
; Opt. Inputs :	None.
;
; Outputs     :	DATA	= The calibrated data will be returned as a new
;			  structure variable with the same format as the input.
;
; Opt. Outputs:	None.
;
; Keywords    :	BILINEAR = Allows bilinear interpolation during rectification.
;			   This is the default.  If nearest-neighbor
;			   interpolation is desired, then set BILINEAR=0.
;
;               CUBIC    = Allows cubic interpolation during rectification
;
;                          (NOTE: both of these change the intensity values.)
;
;		BOTTOM	 = If set, then the data is aligned so that the bottom
;			   of the slit is aligned with the bottom of the array.
;			   Ignored unless the data contains a full spectral
;			   readout.
;
;		ERRMSG = If defined and passed, then any error messages will be
;			 returned to the user in this parameter rather than
;			 depending on the MESSAGE routine in IDL.  If no errors
;			 are encountered, then a null string is returned.  In
;			 order to use this feature, ERRMSG must be defined
;			 first, e.g.
;
;				ERRMSG = ''
;				NIS_ROTATE, ERRMSG=ERRMSG, ...
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	VDS_CALIB, TAG_EXIST, GET_TILTCAL, GT_WINDATA, GT_WINDESC, 
;		DATATYPE, VDS_ROTATE, ST_WINDATA, ADD_TAG
;
; Common      :	None.
;
; Restrictions:	Before this procedure can be used, the calibration for the VDS
;		detector must be applied, via the VDS_DEBIAS and VDS_CALIB
;		routines.  If not already done, then this routine will attempt
;		to use the automatic facilities of those routines.
;
; Side effects:	None.
;
; Category    :	Calibration, VDS
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 6 February 1998
;
; Modified    :	Version 1, 6-Feb-1998, William Thompson, GSFC
;		Version 2, 5-Oct-1998, William Thompson, GSFC
;			Added keyword MISSING to call to VDS_ROTATE
;		Version 3, 4-Dec-1998, William Thompson, GSFC
;			Call GT_WINDATA and ST_WINDATA with NOPAD.
;			This fixes problem when full spectral readout and
;			windows are mixed in the same study (e.g. SERTSCAL).
;
; Version     : Version 3, 4-Dec-1998
;-
;
	ON_ERROR, 2
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 1 THEN BEGIN
	    MESSAGE = 'Syntax:  NIS_ROTATE, DATA'
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that it is VDS data we're dealing with.
;
	IF DATA.HEADER.DETECTOR NE 'NIS' THEN BEGIN
	    MESSAGE = 'Cannot use this routine with GIS data'
	    GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that the VDS calibration has been done first.  If it hasn't, then
;  attempt to do it.  Also, check to see if the VDS rotation has already been
;  done.
;
	UNITS = DATA.DETDESC(0).UNITS
	IF STRPOS(UNITS,'ADC') NE -1 THEN BEGIN
	    MESSAGE = ''
	    VDS_CALIB, DATA, ERRMSG=MESSAGE
	    IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	END ELSE IF TAG_EXIST(DATA, 'ROTATED') THEN BEGIN
	    IF DATA.ROTATED EQ 1 THEN BEGIN
		MESSAGE = 'Data has already been rotated'
		CONTINUE = 1
		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
;  Determine the BILINEAR keyword.
;
	IF N_ELEMENTS(K_BILINEAR) EQ 1 THEN BEGIN
	    BILINEAR = K_BILINEAR
	END ELSE IF NOT KEYWORD_SET(CUBIC) THEN BILINEAR = 1
;
;  Get the tilt calibrations.
;
	MESSAGE = ''
	GET_TILTCAL, 1, DATA.HEADER.DATE_OBS, TILT1, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
	GET_TILTCAL, 2, DATA.HEADER.DATE_OBS, TILT2, ERRMSG=MESSAGE
	IF MESSAGE NE '' THEN GOTO, HANDLE_ERROR
;
;  Determine the number of windows, and step through them.
;
	NWIN = N_ELEMENTS(DATA.DETDESC)
        FOR IWIN = 0,NWIN-1 DO $
           IF DATA.DETDESC(IWIN).IXSTOP(0) GE 0 THEN BEGIN
;
;  Extract the data array from the input structure.  Save the dimensions for
;  later.  Convert to at least floating point.
;          
	    ARRAY = GT_WINDATA(DATA, IWIN, /NOPAD)
            DESC  = GT_WINDESC(DATA, IWIN)
	    SA = SIZE(ARRAY)
	    IF DATATYPE(ARRAY,2) LT 4 THEN ARRAY = FLOAT(ARRAY)
;
;  Determine if the data array is in NIS1 or NIS2.
;
	    IF DESC.DETY GE 512 THEN BEGIN
		BAND = 1
		TILT = TILT1
	    END ELSE BEGIN
		BAND = 2
		TILT = TILT2
	    ENDELSE
;
;  Get the appropriate velocity tilt value.  If the whole spectrum is being
;  rotate, then use the polynomial expression.
;
	    WIDTH = DESC.IXSTOP(0) - DESC.IXSTART(0) + 1
	    IF WIDTH EQ 1024 THEN VEL_TILT = TILT ELSE	$
		    VEL_TILT = POLY(DESC.DETX + (WIDTH-1.)/2, TILT)
;
;  Call VDS_ROTATE to rotate the data.
;
	    VDS_ROTATE, ARRAY, ARRAY, BAND, BILINEAR=BILINEAR, CUBIC=CUBIC, $
		    BOTTOM=BOTTOM, VEL_TILT=VEL_TILT, MISSING=DESC.MISSING
;
;  Put the data back into the structure.
;
            ST_WINDATA,DATA,IWIN,ARRAY,/NO_COPY,/NOPAD
	ENDIF		;IWIN valid
;
;  Add the tag "ROTATED".
;
	IF TAG_EXIST(DATA, 'ROTATED') THEN DATA.ROTATED = 1 ELSE	$
		DATA = ADD_TAG(DATA, 1, 'ROTATED')
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'NIS_ROTATE: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, CONTINUE=KEYWORD_SET(CONTINUE)
;
;  Exit point.
;
FINISH:
	RETURN
	END
