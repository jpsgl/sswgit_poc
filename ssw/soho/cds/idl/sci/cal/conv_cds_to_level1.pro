;+
; Project     :	SOHO - CDS
;
; Name        :	CONV_CDS_TO_LEVEL1
;
; Purpose     :	Converts CDS FITS files to Level-1 format
;
; Category    :	SOHO, CDS, Calibration
;
; Explanation :	This routine converts a series of CDS Level-0 FITS files into
;               Level-1 format.  The actual work is done in the routines
;               CONV_NIS_TO_LEVEL1 and CONV_GIS_TO_LEVEL1.
;
; Syntax      :	CONV_CDS_TO_LEVEL1, DIRECTORY
;
; Examples    :	CONV_NIS_TO_LEVEL1, '/archive/cdsfits1'
;
; Inputs      :	DIRECTORY = The directory to process.
;
; Opt. Inputs :	None.
;
; Outputs     :	New files are created with "l1" appended to the filename, and
;               placed within the final archive tree.  The final archive is
;               organized by year, month, and day of the start of the study
;               that each file belongs to.
;
; Opt. Outputs:	None.
;
; Keywords    :	REPLACE = If set, then already existing level-1 files are
;                         reprocessed and replaced.  The normal behavior is to
;                         ignore files which have already been processed to
;                         make it easier to restart previously incomplete
;                         processing.
;
;
; Env. Vars.  : CDS_FITS_L1_DATA is the top archive directory.  Yearly
;               subdirectories are created under this, followed by monthly and
;               daily subdirectories, e.g. $CDS_FITS_L1_DATA/2007/01/27/
;
; Calls       :	GET_TEMP_DIR, CONCAT_DIR, FXHREAD, FXPAR, GET_MAIN, TAI2UTC,
;               FILE_EXIST, MK_DIR, BREAK_FILE, CONV_NIS_TO_LEVEL1
;
; Common      :	None.
;
; Restrictions:	Access to the SOHO orbit files is required to add the orbital
;               data to the header.  See the individual NIS and GIS routines
;               for more information.
;
;               The routine CONV_GIS_TO_LEVEL1 does not yet exist.
;
; Side effects:	See the individual NIS and GIS routines for more information.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 21-Mar-2008, William Thompson, GSFC
;               Version 2, 07-Jul-2008, WTT, use long integer for loop
;
; Contact     :	WTHOMPSON
;-
;
pro conv_cds_to_level1, directory, replace=replace
;
if n_params() ne 1 then message, 'Syntax: CONV_CDS_TO_LEVEL1, DIRECTORY'
;
;  Get the top archive directory.
;
archive = getenv('CDS_FITS_L1_DATA')
if archive eq '' then message, 'CDS_FITS_L1_DATA not defined'
;
;  Get a temporary directory for assembling the files.
;
tempdir = get_temp_dir()
;
;  Get all the files in the input directory, and step through the files.
;
files = file_search(concat_dir(directory, '*.fits'))
n_files = n_elements(files)
for ifile=0L,n_files-1 do begin
    filename = files[ifile]
;
;  Read in the FITS header, and get the detector and program number.
;
    openr, unit, filename, /get_lun
    fxhread, unit, header
    free_lun, unit
    prog_num = fxpar(header, 'prog_num')
    detector = strtrim(fxpar(header, 'detector'))
;
;  From the program number, get the start date of the overall observation
;  program.
;
    get_main, prog_num, obs
    date_obs = tai2utc(obs.date_obs,/ext)
;
;  Form the path to use to store the data.  If the directories do not yet
;  exist, create them.
;
    path = concat_dir(archive, string(date_obs.year,  format='(I4.4)'))
    if not file_exist(path) then mk_dir, path
    path = concat_dir(path,    string(date_obs.month, format='(I2.2)'))
    if not file_exist(path) then mk_dir, path
    path = concat_dir(path,    string(date_obs.day,   format='(I2.2)'))
    if not file_exist(path) then mk_dir, path
;
;  Form the name of the output file.
;
    break_file, filename, disk, dir, name, ext
    outfile = name + 'l1.fits'
;
;  Unless the /REPLACE keyword was passed, ignore files which have already been
;  processed.
;
    if keyword_set(replace) then test=0 else $
      test = file_exist(concat_dir(path, outfile))
    if not test then begin
;
;  Call the appropriate program to process the file.  Create the new file in a
;  temporary directory, and then move it over to the archive when complete.
;
        print, filename
        case detector of
            'NIS': conv_nis_to_level1, filename, outpath=tempdir
            'GIS': print, 'Not yet implemented for GIS'
        endcase
        tempfile = concat_dir(tempdir, outfile)
        if file_exist(tempfile) then file_move, tempfile, path, /overwrite
    endif
endfor
;
end
