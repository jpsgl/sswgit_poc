;+
; Project     :	SOHO - CDS
;
; Name        :	GET_VDS_SLITPOS
;
; Purpose     :	Gets VDS state database information.
;
; Explanation :	This returns a structure containing information about various 
;               quasi-static VDS parameters. 
;
; Use         : GET_VDS_SLITPOS, DATE, DESC, ERRMSG=ERRMSG
;
; Inputs      : DATE	= The date/time value that the user wishes the state
;			  for.  This can be in any of the standard CDS time
;			  formats.
;
; Opt. Inputs : None.
;
; Outputs     : DESC	= Structure containing the state description.  It
;			  contains the following tags:
;
;			DATE	   = Start date and time for the period for
;				     which the parameters in this record are
;				     valid.  The period ends at the start
;				     date/time of the next record.  This is a
;				     double precision TAI value.  If no entries
;				     are found, then a simplified structure is
;				     returned with this set to zero.
;			N1_CEN_S   = Center slit position for NIS1 at the short
;				     wavelength end.
;			N1_CEN_L   = Center slit position for NIS1 at the long
;				     wavelength end.
;			N2_CEN_S   = Center slit position for NIS2 at the short
;				     wavelength end.
;			N2_CEN_L   = Center slit position for NIS2 at the long
;				     wavelength end.
;			NIS_HT	   = NIS slit height.
;
; Opt. Outputs:	None.
;
; Keywords    : 
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       GET_STATE, ERRMSG=ERRMSG, ... 
;                       IF ERRMSG NE '' THEN ...
;
; Calls       :	DATATYPE, CP_GET_HISTORY, ANYTIM2UTC, UTC2TAI.
;                
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Command preparation.
;
; Prev. Hist. :	Adapted from CP_GET_STATE by Martin Carter.
;
; Written     :	William Thompson, GSFC, 22 February 1996
;
; Modified    :	Version 1, William Thompson, GSFC, 22 February 1996
;			Adapted from CP_GET_STATE.  Only returns readout mode
;			and MCP voltage.
;		Version 2, William Thompson, GSFC, 29 February 1996
;			Added NIS_HT to output structure.
;
; Version     :	Version 2, 29 February 1996
;-
;**********************************************************

PRO get_vds_slitpos, date, desc, ERRMSG=ERRMSG

	ON_ERROR, 2
  ;
  ;  Define a null result.  If the routine is successful, this will be updated
  ;  later.
  ;
	desc = {date: 0}
  ;
  ;  Check the number of parameters.
  ;
        IF N_PARAMS() NE 2 THEN BEGIN
		MESSAGE = 'Syntax:  GET_VDS_SLITPOS, DATE, DESC'
		GOTO, HANDLE_ERROR
	ENDIF
  ;
  ;  Check the input parameter.
  ;
	IF N_ELEMENTS(date) NE 1 THEN BEGIN
		MESSAGE = 'DATE must be a scalar'
		GOTO, HANDLE_ERROR
	ENDIF
  ;
  ;  Convert the date to TAI format.
  ;
	IF DATATYPE(date,1) EQ 'Double' THEN tai = date ELSE	$
		tai = UTC2TAI(date)
  ;
  ; get VDS parameters from state database

  ; get N1_CEN_S

  s = cp_get_history ( {mnemonic:'N1_CEN_S', pnumber:0}, /QUIET )

  IF s(0).mnemonic EQ 'Unknown' THEN BEGIN
    MESSAGE = 'PARAMETER N1_CEN_S NOT FOUND IN CDHS STATE DATABASE'
    GOTO, HANDLE_ERROR
  ENDIF

  ; look for dates before given date for item

  list = WHERE ( UTC2TAI ( ANYTIM2UTC ( s.date ) ) LE tai, count )

  IF count EQ 0 THEN BEGIN
      message, /informational, 'Using first value for N1_CEN_S'
      list = where( s.date eq min(s.date), count )
  ENDIF

  ; get latest item

  s = s ( list(count-1) )

  n1_cen_s = s.active
  st_date = UTC2TAI ( ANYTIM2UTC ( s.date ) )

  ; get N1_CEN_L

  s = cp_get_history ( {mnemonic:'N1_CEN_L', pnumber:0}, /QUIET )

  IF s(0).mnemonic EQ 'Unknown' THEN BEGIN
    MESSAGE = 'PARAMETER N1_CEN_L NOT FOUND IN CDHS STATE DATABASE'
    GOTO, HANDLE_ERROR
  ENDIF

  ; look for dates before given date for item

  list = WHERE ( UTC2TAI ( ANYTIM2UTC ( s.date ) ) LE tai, count )

  IF count EQ 0 THEN BEGIN
      message, /informational, 'Using first value for N1_CEN_L'
      list = where( s.date eq min(s.date), count )
  ENDIF

  ; get latest item

  s = s ( list(count-1) )

  n1_cen_l = s.active
  st_date = st_date > UTC2TAI ( ANYTIM2UTC ( s.date ) )

  ; get N2_CEN_S

  s = cp_get_history ( {mnemonic:'N2_CEN_S', pnumber:0}, /QUIET )

  IF s(0).mnemonic EQ 'Unknown' THEN BEGIN
    MESSAGE = 'PARAMETER N2_CEN_S NOT FOUND IN CDHS STATE DATABASE'
    GOTO, HANDLE_ERROR
  ENDIF

  ; look for dates before given date for item

  list = WHERE ( UTC2TAI ( ANYTIM2UTC ( s.date ) ) LE tai, count )

  IF count EQ 0 THEN BEGIN
      message, /informational, 'Using first value for N2_CEN_S'
      list = where( s.date eq min(s.date), count )
  ENDIF

  ; get latest item

  s = s ( list(count-1) )

  n2_cen_s = s.active
  st_date = st_date > UTC2TAI ( ANYTIM2UTC ( s.date ) )

  ; get N2_CEN_L

  s = cp_get_history ( {mnemonic:'N2_CEN_L', pnumber:0}, /QUIET )

  IF s(0).mnemonic EQ 'Unknown' THEN BEGIN
    MESSAGE = 'PARAMETER N2_CEN_L NOT FOUND IN CDHS STATE DATABASE'
    GOTO, HANDLE_ERROR
  ENDIF

  ; look for dates before given date for item

  list = WHERE ( UTC2TAI ( ANYTIM2UTC ( s.date ) ) LE tai, count )

  IF count EQ 0 THEN BEGIN
      message, /informational, 'Using first value for N2_CEN_L'
      list = where( s.date eq min(s.date), count )
  ENDIF

  ; get latest item

  s = s ( list(count-1) )

  n2_cen_l = s.active
  st_date = st_date > UTC2TAI ( ANYTIM2UTC ( s.date ) )

  ; get NIS_HT

  s = cp_get_history ( {mnemonic:'NIS_HT', pnumber:0}, /QUIET )

  IF s(0).mnemonic EQ 'Unknown' THEN BEGIN
    MESSAGE = 'PARAMETER NIS_HT NOT FOUND IN CDHS STATE DATABASE'
    GOTO, HANDLE_ERROR
  ENDIF

  ; look for dates before given date for item

  list = WHERE ( UTC2TAI ( ANYTIM2UTC ( s.date ) ) LE tai, count )

  IF count EQ 0 THEN BEGIN
      message, /informational, 'Using first value for NIS_HT'
      list = where( s.date eq min(s.date), count )
  ENDIF

  ; get latest item

  s = s ( list(count-1) )

  nis_ht = s.active
  st_date = st_date > UTC2TAI ( ANYTIM2UTC ( s.date ) )

  ; set up output structure

  desc = {date: st_date,	$
          n1_cen_s: n1_cen_s,	$
	  n1_cen_l: n1_cen_l,	$
          n2_cen_s: n2_cen_s,	$
	  n2_cen_l: n2_cen_l,	$
	  nis_ht: nis_ht}
  
  RETURN
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_VDS_SLITPOS: '+MESSAGE $
		ELSE MESSAGE, MESSAGE
END
