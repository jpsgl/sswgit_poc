;+
; Project     :	SOHO - CDS
;
; Name        :	CP_GET_HISTORY()
;
; Purpose     :	Retrieves history for a given command mnemonic and parameter number from the CDHS state database.
;
; Explanation : Returns a structure array containing the values associated with the 
;               user supplied command mnemonic and parameter number. If no entries found then 
;               returns a structure with command mnemonic : "Unknown" and displays a popup message .
;
; Use         : < st = cp_get_history ( struct ) >
;
; Inputs      : struct = input structure containing parameter command mnemonic and number
;                 struct.mnemonic = parameter mnemonic
;                 struct.pnumber  = parameter number for this entry
;
; Opt. Inputs : None.
;
; Outputs     : struct = structure array of type st_cdhsstate containing command mnemonic and parameter values.                        
;                 struct.mnemonic = parameter mnemonic
;                 struct.date     = date values last changed
;                 struct.numberp  = no. of parameters associated with mnemonic
;                 struct.pnumber  = parameter number for this entry
;                 struct.load     = flag indicating whether value requires loading
;                 struct.delay    = delay in secs associated with command
;                 struct.active   = value for parameter
;                 struct.default  = default value for parameter
;                 struct.comment  = comment on parameter
;
; Opt. Outputs:	None.
;
; Keywords    : QUIET : suppresses popup message
;               GROUP : group for popup message box.
;
; Calls       :	dbopen, db_info, dbfind, dbext, dbclose, rem_fst.
;                
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Command preparation.
;
; Prev. Hist. :	None.
;
; Written     :	Version 0.0, Martin Carter, RAL, 24/5/94
;
; Modified    :	Version 0.1, MKC, 11/8/95
;                            If no entries returns Unknown mnemonic
;                            Added comment field to structure.
;                            Explicitly extracted mnemonic from database.
;               Version 0.2, MKC, 25/8/95
;                            Modified so that input structure needs only mnemonic 
;                            and pnumber tags.
;               Version 0.3, MKC, 19/12/95
;                 Added delay tag.
;
; Version     :	Version 0.3, 19/12/95
;-
;**********************************************************

FUNCTION cp_get_history, struct, GROUP=group, QUIET=quiet

  ; set up structure 

  s = { st_cdhsstate, date:'', mnemonic:'Unknown', pnumber:0, numberp:1, $
                      load:0, delay:0, active:0L, default:0L, comment:'' }

  ; open state database 

  dbopen, 'cdhsstate'

  ; check database has some entries
  ; else dbfind will flag error

  nentries = db_info ( 'entries' )

  IF nentries(0) EQ 0 THEN BEGIN
    
    ; display message 

    IF NOT KEYWORD_SET(quiet) THEN $      
      popup_msg, ['No entries', $
                  'found'], $
                  title = 'WARNING MESSAGE', /modal, GROUP=group

    RETURN, s

  ENDIF

  ; set up search criteria

  search = 'MNEMONIC=' + struct.mnemonic +',PNUMBER=' + STRTRIM(struct.pnumber,1)

  list = dbfind ( search, /SILENT)

  ; list is array of entries in database
  ; if no entries then list contains a zero
  ; valid entries start at 1 

  IF list(0) EQ 0 THEN BEGIN

    ; display message 

    IF NOT KEYWORD_SET(quiet) THEN $      
      popup_msg, ['No entries', $
                  'found'], $
                  title = 'WARNING MESSAGE', /modal, GROUP=group

    RETURN, s

  ENDIF

  nentries = N_ELEMENTS ( list )

  ; set up structure array
  ; NB must not modify input structure

  s = REPLICATE ( s, nentries )

  ; extract entries
  ; NB cannot use structure directly since scalars passed by value
  ;    results are arrays

  dbext, list, 'DATE,MNEMONIC,PNUMBER,NUMBERP,LOAD,DELAY,ACTIVE,DEFAULT,COMMENT',$
                date,mnemonic,pnumber,numberp,load,delay,active,default,comment

  ; close database

  dbclose, 'cdhsstate'

  ; convert single entry arrays to scalars
  ; NB This works because items are all scalars

  IF N_ELEMENTS(date) EQ 1 THEN inds=0 ELSE inds=INDGEN(nentries)

  date     = date(inds)
  mnemonic = mnemonic(inds)
  pnumber  = pnumber(inds)
  numberp  = numberp(inds)
  load     = load(inds)
  delay    = delay(inds)
  active   = active(inds)
  default  = default(inds)
  comment  = comment(inds)

  ; return values in date order

  s.date     = date
  s.mnemonic = mnemonic
  s.pnumber  = pnumber
  s.numberp  = numberp
  s.load     = load
  s.delay    = delay
  s.active   = active
  s.default  = default
  s.comment  = comment

  RETURN, s
  
END


