;+
; Project     :	SOHO - CDS
;
; Name        :	XSTUDY
;
; Purpose     : Widget interface to CDS database catalog
;
; Explanation : None
;
; Use         : XSTUDY
;
; Inputs      : None.
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: STUDY = study selected from within widget
;
; Keywords    : GROUP = widget ID of calling program
;               SELECT = permit selection of studies
;               RELIST = relist fundamental study list each time
;               NOWARN = turn of warning for multiple XCAMP copies
;
; Calls       : Many
;
; Common      : XSTUDY_COM
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Widgets, planning
;
; Prev. Hist. : None.
;
; Written     :	August 1, 1994, DMZ (ARC)
;
; Version     : Version 1.0
;-

   pro xstudy_event,event

common xstudy_com,comment,searb,rvlabels,$
    slist,svlist,svbase,tbase,import_dir,$
    rvlist,rvbase,sel_only,title_id,det,$
    svtags,rvtags,rv_index,ll_base,dw_base,$
    study_id,studyvar,rv_rasters,v_studies,study_def,$
    view_svar,view_rvar,view_ll,view_dw,$
    stitle,sobs_prog,sv_desc,ttitle,tobs_prog,tsv_desc,$
    f_studies,slits,xstudy_list,export_dir,idsort,sav_db

;-- text events

    widget_control,ttitle,get_value=temp & stitle=temp(0)
    widget_control,ttitle,set_value=temp(0)

    widget_control,tobs_prog,get_value=temp & sobs_prog=temp(0)
    widget_control,tobs_prog,set_value=temp(0)

    widget_control,tsv_desc,get_value=temp & sv_desc=temp(0)
    widget_control,tsv_desc,set_value=temp(0)

    widget_control, event.id, get_uvalue = uservalue
    if (n_elements(uservalue) eq 0) then uservalue=''
    bname=strtrim(uservalue,2)

;-- auto-list

    if strmid(bname(0),0,4) eq 'auto' then begin
     xstudy_list=bname(0) eq 'auto_on'
    endif

;-- sorting

    if strmid(bname(0),0,4) eq 'sort' then begin
     idsort=bname(0) eq 'sort_id'
     xstudy_fsearch,event
    endif

;-- filter studies
    
    look=where(bname eq ['TITLE','OBS_PROG','SV_DESC'],cnt)
    if cnt gt 0 then xstudy_fsearch,event

    if bname eq 'DTYPE' then begin
     dets=['A','N','G']
     det=dets(event.index)
    endif

;-- study brief

    if bname eq 'BRIEF' then begin
     ok_to_list=0
     if exist(f_studies) and exist(study_id) and exist(title_id) then begin
      alook=where((study_id eq f_studies.study_id) $
              and (title_id eq f_studies.title_id),cnt)
      if cnt eq 1 then begin
       acronym=trim(f_studies(alook(0)).obs_prog)
       study_brief,acronym,save_text=save_text,/quiet
       ok_to_list=1
       if n_elements(save_text) gt 1 then $
        xtext,save_text,group=event.top,wbase=tbase,/just_reg else $
        xack,acronym+' not found in STUDY_BRIEF file',group=event.top
      endif
     endif
     if not ok_to_list then begin
      xack,'Please choose a study ID to list.',group=event.top
      return
     endif
    endif

;-- switch DB

    if bname eq 'SWITCH_DB' then begin
     db_val=event.index
     if (db_val eq 0) and (which_zdbase() eq 'User') then return
     if (db_val eq 1) and (which_zdbase() eq 'CDS') then return
     err=''
     if db_val eq 0 then s=fix_zdbase(/user,err=err) else $
      s=fix_zdbase(/cds,err=err)
     if err ne '' then begin
      xack,err,group=event.top
      if widget_info(event.id,/type) eq 8 then $
       widget_control,event.id,set_droplist_select=1-db_val else $
        widget_control,event.id,set_value=1-db_val
      return
     endif
     xtext,'Please wait. Relisting...',wbase=wbase,/just_reg
     sav_db=getenv('ZDBASE')
     xstudy_reset
     xstudy_fsearch,event,/relist
     xkill,wbase
    endif

;-- handle bookmarks

    if bname eq 'ADD_BOOK' then begin
     xcds_book,study_def,group=event.top,/add,err=err
    endif

    if bname eq 'VIEW_BOOK' then begin
     xcds_book,bstudy,group=event.top,/select,/remove,/clear,err=err
     if err eq '' then begin
      if exist(bstudy) then begin
       xstudy_update,bstudy,found=found
       if not found then begin
        xstudy_reset
        xstudy_fsearch,event,/relist
        xstudy_update,bstudy
       endif
      endif
     endif
    endif

;-- export study

    if bname eq 'EXPORT' then begin
     ok_to_export=0
     if exist(study_def) then begin
      if (study_def.study_id gt -1) and (study_def.studyvar gt -1) then $
       ok_to_export=1
     endif
     if not ok_to_export then begin
      xack,'Please select a both a Study ID and a variation to export.',group=event.top
      return
     endif
     if datatype(export_dir) ne 'STR' then begin
      export_dir=trim(getenv('CDS_EXPORT_W'))
      cd,curr=curr
      if export_dir eq '' then export_dir=curr
     endif
     xinput,export_dir,'Enter output directory for study definition export file:',$
      group=event.top,status=status,max_len=80
     if status then begin
      if not chk_dir(export_dir,out_dir) then begin
       xack,'Non-existent directory: '+export_dir,group=event.top
       return
      endif
      ok=test_open(export_dir,/write,/quiet,err=err)
      if not ok then begin
       xack,'No write access to: '+export_dir,group=event.top
       return
      endif
      export_dir=out_dir
      xtext,'Please wait. Exporting study to file...',/just_reg,wbase=wbase
      widget_control,/hour
      err=''
      export_study,study_def.study_id,study_def.studyvar,err=err,file=file,$
                   path=export_dir
      xkill,wbase
      if err ne '' then xack,err,group=event.top else $
       xack,'Study definition written to: '+file,group=event.top
     endif
    endif

;-- import study

    if bname eq 'IMPORT' then begin
     if datatype(import_dir) ne 'STR' then begin
      cd,curr=curr
      import_dir=trim(getenv('CDS_IMPORT'))
      if import_dir eq '' then begin
       if datatype(export_dir) eq 'STR' then begin
        if trim(export_dir) ne '' then import_dir=export_dir
       endif
      endif
      if import_dir eq '' then import_dir=curr
     endif
     imp_file=pickfile(group=event.top,filter='*.def',$
              title='Enter study definition file name to import',path=import_dir)
     if trim(imp_file) eq '' then return
     err=''
     xtext,'Please wait. Importing study...',/just_reg,wbase=wbase
     widget_control,event.top,/hour,sensitive=0
     import_study,imp_file,imp_study,err=err
     xkill,wbase
     widget_control,event.top,/sensitive
     if err ne '' then xack,err,group=event.top else begin
      xstudy_update,imp_study,found=found
      if not found then begin
       xstudy_reset
       xstudy_fsearch,event,/relist
       xstudy_update,imp_study
      endif
     endelse
    endif

;-- search for Fundamental studies

    if bname eq 'RESET' then begin
     xstudy_reset
     xstudy_fsearch,event,/relist
    endif

    if (bname eq 'SEARCH') then xstudy_fsearch,event

;-- view buttons

    clook=where(strlowcase(bname) eq ['view_svar','view_rvar','view_ll','view_dw'],cnt)
    if cnt gt 0 then begin
     s=execute(bname+'=event.select')
     if (bname ne 'VIEW_SVAR') and (n_elements(rv_rasters) gt 1) then widget_control,comment,set_value= 'Please select a raster'
    endif

;-- exit and/or select 

    if (bname eq 'EXIT') or (bname eq 'SELECT') then begin

     if (bname eq 'EXIT') then delvarx,study_def

     if (bname eq 'SELECT') then begin
      ok_to_sel=0
      if exist(study_def) then begin
       if (study_def.study_id gt -1) and (study_def.studyvar gt -1) then $
        ok_to_sel=1
      endif
      if not ok_to_sel then begin
       xack,'Please choose both a study ID and a variation.',group=event.top
       return
      endif
     endif

     xtext_reset,[tobs_prog,ttitle,tsv_desc]
     xkill,event.top
;     view_svar=0 & view_rvar=0 & view_ll=0 & view_dw=0
     return
    endif
    
    if bname eq 'HELP' then $
     widg_help,'xstudy',/modal,xtextsize=70,font='9x15bold',$
      tfont='9x15bold'

;-- fundamental list

    if (event.id eq slist) then begin
     fstudy=f_studies(event.index)
     study_id=fstudy.study_id
     title_id=fstudy.title_id
     xstudy_vsearch
     xstudy_vlist
     if n_elements(v_studies) eq 1 then begin
      studyvar=v_studies(0).studyvar
      xstudy_rlist
     endif
    endif

;-- variation list

    if (event.id eq svlist) then begin
     studyvar=v_studies(event.index).studyvar
     xstudy_rlist
    endif

;-- update existing raster variation, line list, and data window widgets

    if (event.id eq rvlist) then rv_index=event.index

;-- view study variation

    if exist(study_def) then begin
     if view_svar then begin
      study_temp=study_def
      if tag_exist(study_temp,'duration0') then begin
;       new_tag=trim(string(study_temp.duration0/60.,'(f10.2)'))+' (mins)'
       new_tag=sec2dhms(study_temp.duration0)
       study_temp=rep_tag_value(study_temp,new_tag,'duration0')
      endif
      if tag_exist(study_def,'duration1') then begin
;       new_tag=trim(string(study_temp.duration1/60.,'(f10.2)'))+' (mins)'
       new_tag=sec2dhms(study_temp.duration1)
       study_temp=rep_tag_value(study_temp,new_tag,'duration1')
      endif
      xshow,svbase
      xstruct,rem_tag(study_temp,'RASTERS'),/just_reg,$
              wbase=svbase,wtags=svtags,group=event.top,title=' '
     endif else xhide,svbase
    endif

;-- view raster variation

    if exist(rv_rasters) and exist(rv_index) then begin

     if view_rvar then begin
      xshow,rvbase
      xstruct,rv_rasters(rv_index),nx=3,wbase=rvbase,wtags=rvtags,$
             xsize=30,group=event.top,title=' ',/just_reg
     endif else xhide,rvbase

     if (view_ll) then begin
      xshow,ll_base
      ll_id=rv_rasters(rv_index).ll_id
      get_linelist,ll_id,linelist
      if tag_exist(linelist,'lines') then begin
       xlist,linelist.lines,group=event.top,/just_reg,$
        title='LL_ID:            '+string(ll_id),wbase=ll_base
      endif else begin
       widget_control,comment,set_value= 'No line list defined'
      endelse
     endif else xhide,ll_base

     if view_dw then begin
      xshow,dw_base
      dw_id=rv_rasters(rv_index).dw_id
      get_datawin,dw_id,datawin
      if tag_exist(datawin,'wins') then begin
       xlist,datawin.wins,/just_reg,group=event.top,$
                 title='DW_ID:             '+string(dw_id),$
                 wbase=dw_base
      endif else begin
       widget_control,comment,set_value= 'No data window defined'
      endelse
     endif else xhide,dw_base

    endif

    return & end

;--------------------------------------------------------------------------

pro xstudy_reset

common xstudy_com

sobs_prog='' & widget_control,tobs_prog,set_value=sobs_prog
stitle=''  & widget_control,ttitle,set_value=stitle
sv_desc=''  & widget_control,tsv_desc,set_value=sv_desc
delvarx,study_def,rv_rasters,study_id,studyvar,title_id
xhide,dw_base
xhide,ll_base
xhide,svbase
xhide,rvbase
dummy=get_cds_study(/init)
dummy=get_cds_raster(/init)

return & end

;--------------------------------------------------------------------------

pro xstudy_fsearch,event,relist=relist

common xstudy_com

if datatype(event) eq 'STC' then begin
 ok=(stitle ne '') or (sobs_prog ne '') or (sv_desc ne '') or (det ne '')
 if not ok  and (event.id eq searb) then begin
  xack,'Please enter keywords to search on.',group=event.top,/icon
  return
 endif
endif

widget_control,/hour
find_cds_study,f_studies,nfound=nfound,/fund_only,err=err,relist=relist,$
 title_name=stitle,sv_desc=sv_desc,obs_prog=sobs_prog,det=det
if nfound eq 0 then begin
 widget_control,comment,set_value=err
 widget_control,slist,set_value=''
 widget_control,svlist,set_value=''
endif else xstudy_flist

return & end

;--------------------------------------------------------------------------

pro xstudy_update,bstudy,found=found

common xstudy_com
found=0
if exist(bstudy) and exist(f_studies) then begin
 widget_control,/hour
 study_id=bstudy.study_id
 title_id=bstudy.title_id
 slook=where( (study_id eq f_studies.study_id) and $
              (title_id eq f_studies.title_id), cnt)
 if cnt gt 0 then begin
  widget_control,slist,set_list_select=slook(0)
  studyvar=bstudy.studyvar
  xstudy_vsearch
  xstudy_vlist
  if exist(v_studies) then begin
   vlook=where(studyvar eq v_studies.studyvar,cnt)
   if cnt gt 0 then begin
    widget_control,svlist,set_list_select=vlook(0)
    found=1
    study_def=bstudy
    xstudy_rlist
   endif
  endif
 endif
endif

return & end

;--------------------------------------------------------------------------

pro xstudy_flist      ;-- list fundamental studies
   
common xstudy_com

delvarx,study_def
widget_control,/hour
nfound=n_elements(f_studies)
if nfound eq 0 then return

if idsort then s=sort([f_studies.study_id]) else s=sort([f_studies.obs_prog])
f_studies=f_studies(s)
study_tit=''
study_vals=string(f_studies.study_id,'(i4)')+'         '
study_tit=string(f_studies.title_id,'(i4)')+'          '
obs_prog=strpad(strtrim(f_studies.obs_prog,2),8,/aft)
title=strtrim(f_studies.title,2)
item=obs_prog+' / '+title
fields=study_vals+study_tit+item

if nfound eq 1 then begin
 study_id=f_studies.study_id
 title_id=f_studies.title_id
endif

widget_control,comment,set_value='Following studies found in database'
widget_control,slist,set_value=fields
widget_control,svlist,set_value=''
widget_control,rvlist,set_value=''
widget_control,comment,set_value= 'Please choose a study ID',/app

return & end

;--------------------------------------------------------------------------

pro xstudy_vsearch

common xstudy_com

xtext,'Please wait. Searching Variation Database...',wbase=wbase,/just_reg
widget_control,rvlist,set_value=''
widget_control,svlist,set_value=''
widget_control,/hour
delvarx,rv_index
xhide,dw_base
xhide,ll_base
xhide,svbase
xhide,rvbase
find_cds_study,v_studies,title_id=title_id,nfound=nvfound,err=err,$
 title_name=stitle,obs_prog=sobs_prog,sv_desc=sv_desc,det=det
xkill,wbase

if nvfound eq 0 then begin
 study_def=get_cds_study(title_id,/use_tit,/add_var)
 widget_control,comment,set_value=err
 widget_control,svlist,set_value=''
 delvarx,v_studies
endif

return & end

;--------------------------------------------------------------------------

pro xstudy_vlist

common xstudy_com

nvfound=n_elements(v_studies)
if nvfound gt 0 then begin
 v_study=v_studies
 svdesc=string(v_study.studyvar,'(i4)')+'            '+$
  strtrim(v_study.sv_desc,2)+' / '+strtrim(zone_def_label(v_study.zone_id),2)
 field=svdesc
 widget_control,svlist,set_value=field
 if (nvfound eq 1) then studyvar=v_study.studyvar
 if exist(studyvar) then begin
  vlook=where( (studyvar eq v_studies.studyvar), cnt)
  if cnt gt 0 then widget_control,svlist,set_list_select=vlook(0) else delvarx,studyvar
 endif  
 if exist(studyvar) then begin
  get_it=1
  if datatype(study_def) eq 'STC' then $
   get_it=(study_def.title_id ne title_id) or $
          (study_def.study_id ne study_id) or $
          (study_def.studyvar ne studyvar) 
  if get_it then study_def=get_cds_study(study_id,studyvar,/add_var)

  if sel_only then widget_control,comment,$
   set_value='To select this study, press: "Select and Exit"' else $
    widget_control,comment,set_value='Please choose a study variation (STUDYVAR)'
 endif else begin
  widget_control,comment,set_value= 'Please choose a study variation (STUDYVAR)'
  delvarx,study_def
 endelse
endif

return & end

;--------------------------------------------------------------------------

pro xstudy_rlist

common xstudy_com

widget_control, /hour
xtext,'Please wait. Searching Raster Database...',wbase=wbase,/just_reg
delvarx,rv_rasters
widget_control,rvlist,set_value=''
delvarx,rv_index
xhide,dw_base
xhide,ll_base
xhide,rv_base
get_it=1
if datatype(study_def) eq 'STC' then $
 get_it=(study_def.title_id ne title_id) or $
        (study_def.study_id ne study_id) or $
        (study_def.studyvar ne studyvar) 
if get_it then study_def=get_cds_study(study_id,studyvar,/add_var)

;-- look for raster fields

if not tag_exist(study_def,'RASTERS') then begin
 widget_control,comment,set_value= 'No valid rasters found'
 xkill,wbase & return
endif

;-- show rasters

t_rasters=study_def.rasters
nvras=n_elements(t_rasters)

;-- make list of all raster variations for selected studyvar

ptype=['Offset ','Fixed  ','Deferred']
last_id=-1 & last_var=-1
mid=strpos(rvlabels,'ID')
mvar=strpos(rvlabels,'VAR')
mdet=strpos(rvlabels,'DET')
mslit=strpos(rvlabels,'SLIT')
mwid=strpos(rvlabels,'WIDTH')
mhi=strpos(rvlabels,'HEIGHT')
mpo=strpos(rvlabels,'POINT')
mx=strpos(rvlabels,'X')-3
my=strpos(rvlabels,'Y')-3
mrep=strpos(rvlabels,'REPS')
mexp=strpos(rvlabels,'EXP')
mdur=strpos(rvlabels,'DUR')
mrate=strpos(rvlabels,'TEL')
base=strpad('',strlen(rvlabels))
for i=0,nvras-1 do begin
 ras_id=t_rasters(i).ras_id
 ras_var=t_rasters(i).ras_var
 ras_pnt=ptype(t_rasters(i).pointing+1)
 ras_inx=string(fix(t_rasters(i).ins_x),'(i5)')
 ras_iny=string(fix(t_rasters(i).ins_y),'(i5)')
 ras_rep=string(t_rasters(i).n_repeat_r,'(i3)')
 if (ras_id ne last_id) or (ras_var ne last_var) then $
  v_rasters=get_cds_raster(ras_id,ras_var)
 if v_rasters.ras_id gt -1 then begin
  ras_det=trim(v_rasters.detector)+'IS'
  ras_slit=slits(v_rasters.slit_num-1)
  ras_rate='   '+trim(v_rasters.tel_rate)
  item=base
  raster_size,v_rasters,width=width,height=height
  strput,item,string(ras_id,'(i3)'),mid-1
  strput,item,string(ras_var,'(i3)'),mvar-1
  strput,item,ras_det,mdet
  strput,item,ras_slit,mslit
  strput,item,string(fix(width),'(i4)'),mwid
  strput,item,string(fix(height),'(i4)'),mhi
  strput,item,ras_pnt,mpo
  strput,item,ras_inx,mx
  strput,item,ras_iny,my
  strput,item,ras_rep,mrep
  strput,item,string(abs(v_rasters.exptime),'(f7.2)'),mexp
  new_dur=sec2dhms(abs(v_rasters.duration))
  strput,item,new_dur,mdur
  strput,item,ras_rate,mrate
  if not exist(rv_rasters) then begin
   rv_rasters=v_rasters
   rvfields=item
  endif else begin
   rv_rasters=[temporary(rv_rasters),v_rasters]
   rvfields=[temporary(rvfields),item]
  endelse
 endif
 last_id=ras_id & last_var=ras_var
endfor
if exist(rv_rasters) then widget_control,rvlist,set_value=rvfields
if sel_only then widget_control,comment,$
 set_value='To select this study, press: "Select and Exit"' else begin
 if view_rvar or view_ll or view_dw or exist(rv_rasters) then $
  widget_control,comment,set_value= 'Please choose a raster ID ',/app else $
   widget_control,comment,set_value='Ready for another selection'
endelse

xkill,wbase

return & end

;-----------------------------------------------------------------

pro xstudy,study,group=group,select=select,relist=relist,modal=modal,$
           nowarn=nowarn

common xstudy_com

   select_windows
   if not have_widgets() then begin
    message,'widgets are unavailable',/cont
    return
   endif

   caller=get_caller(stat)
   sel_only=keyword_set(select)
   modal=sel_only

   if stat or (strpos(caller,'XSTUDY') gt -1) then xkill,/all
   if (xregistered('xstudy') ne 0) then begin
    if (1-keyword_set(nowarn)) then xack,'Only one copy of XSTUDY can run.'
    xshow,get_handler_id('xstudy')
    return
   endif

;-- check ZDBASE

   find_zdbase,db_type,status=status,/def
   if not status then return

;-- initialize

   delvarx,dw_base,ll_base,svbase,rvbase
   if not exist(view_svar) then view_svar=0 
   if not exist(view_rvar) then view_rvar=0
   if not exist(view_dw) then view_dw=0
   if not exist(view_ll) then view_ll=0

   mk_dfont,bfont=bfont,lfont=lfont

;-- button widgets (EXIT, HELP, ACTIVITY, and SELECT)

   mk_dfont,bfont=bfont,lfont=lfont

   main = widget_base(title ='CDS STUDY DATABASE SEARCH TOOL',/column)

   row1=widget_base(main, /row, /frame)

   exitb=widget_button(row1,value='Exit',uvalue='EXIT',/no_release,$
                      font=bfont)

   if sel_only then begin
    selb=widget_button(row1,value='Select and Exit',uvalue='SELECT',$
                      /no_release,font=bfont)
   endif

   temp=widget_button(row1,value='Bookmarks',/no_rel,/menu,font=bfont)
   abook=widget_button(temp,value='Add Study to Bookmarks',/no_rel,uvalue='ADD_BOOK',font=bfont)
   vbook=widget_button(temp,value='View Study Bookmarks',uvalue='VIEW_BOOK',/no_rel,font=bfont)

   temp=widget_button(row1,value='Study Brief',/no_rel,uvalue='BRIEF',font=bfont)
   importb=widget_button(row1,value='Import',uvalue='IMPORT',/no_rel,font=bfont)

   exportb=widget_button(row1,value='Export',uvalue='EXPORT',/no_rel,font=bfont)

   helpb=widget_button(row1,value='Help',uvalue='HELP',/no_release,$
                      font=bfont)

;-- permit DB changes

   rowf=widget_base(main, /column,/frame)
   exclude=['mk_study_event','mk_cds_plan_event','xcat_event']
   chk=where(strlowcase(caller) eq exclude,cnt)
   allow_db=cnt eq 0
   choices=['Personal','Official']
   db_val=(db_type ne 'USER')
   temp1=widget_base(rowf,/row)
   if not allow_db then begin
    mess='* Using '+choices(db_val)+' Database'
    tmp=widget_label(temp1,value=mess)
   endif else begin
    new_vers=float(strmid(!version.release,0,3)) ge 4.
    tmp=widget_label(temp1,value='Using ')
    if new_vers then begin
     db_but=call_function('widget_droplist',temp1,value=choices,$
      uvalue='SWITCH_DB',font=bfont) 
     widget_control,db_but,set_droplist_select=db_val
    endif else begin
     db_but=cw_bselector2(temp1,choices,uvalue='SWITCH_DB',$
      /no_rel,font=bfont,/return_index)
     widget_control,db_but,set_value=db_val
    endelse
    tmp=widget_label(temp1,value=' Database')
   endelse

;-- filter menu

   if not exist(sv_desc) then sv_desc='' 
   if not exist(sobs_prog) then sobs_prog='' 
   if not exist(stitle) then stitle=''
   det='A'

   temp1=widget_base(rowf,/row)
   mess='* To search study database, enter search text in fields below and then press "Search"'
   junk=widget_label(temp1,value=mess)


   temp1=widget_base(rowf,/row)

   tobs_prog=cw_field(temp1, Title= 'ACRONYM:',fieldfont=lfont,value=sobs_prog,$
                     /ret, xsize = 8,font=bfont,uvalue='OBS_PROG')

   ttitle=cw_field(temp1, Title=    'TITLE: ',fieldfont=lfont,value=stitle,$
                     /ret, xsize = 8,uvalue='TITLE',font=bfont)

   tsv_desc=cw_field(temp1, Title='SV_DESC: ',fieldfont=lfont,value=sv_desc,$
                     /ret, xsize = 16,uvalue='SV_DESC',font=bfont)

   temp1=widget_base(rowf,/row)

   searb=widget_button(temp1,value='Search',uvalue='SEARCH',/no_release,$
                      font=bfont)
   
   resetb=widget_button(temp1,value='Reset',uvalue='RESET',/no_release,$
                      font=bfont)


;   dtype = cw_bselector2(temp1, ['NIS & GIS', 'NIS only', 'GIS only '],label_left='DET:',$
;                     /return_index, /no_rel, uvalue='DTYPE:   ',ids=p_ids,font=lfont)

;   widget_control,dtype,sensitive=0

;-- auto list

    if not exist(xstudy_list) then xstudy_list=0
    if keyword_set(relist) then xstudy_list=1
    r1=widget_base(rowf,/row)
    j1=widget_label(r1,value='* Automatically re-search fundamental studies each time XSTUDY is started? ')
    choices=['Yes','No']
    xmenu,choices,r1,/row,/exclusive,uvalue=['auto_on','auto_off'],font=bfont,$
                buttons=abut,/no_rel
    widget_control,abut(1-xstudy_list),/set_button

;-- sort mode

    if not exist(idsort) then idsort=0
    r2=widget_base(rowf,/row)
    j2=widget_label(r2,value='* Sort on: ')
    choices=['Study ID','Study Acronym']
    xmenu,choices,r2,/row,/exclusive,uvalue=['sort_id','sort_acro'],font=bfont,$
                buttons=abut2,/no_rel
    widget_control,abut2(1-idsort),/set_button

;-- view menu

   row2=widget_base(main, /row,/frame)
   values=['Study','Raster','Line List','Data Window']
   uvalues=strupcase(['view_svar','view_rvar','view_ll','view_dw'])
   slabel=widget_label(row2,value='* View --->')
   xmenu,values,row2,uvalue=uvalues,/nonexclusive,/row,font=bfont,buttons=butt
   widget_control,butt(0),set_button=view_svar
   widget_control,butt(1),set_button=view_rvar
   widget_control,butt(2),set_button=view_ll
   widget_control,butt(3),set_button=view_dw

;-- comment window

   comment = widget_text(main,ysize=2,value=' ',font=lfont)

;-- fundamental study list widget

   flabel=' STUDY_ID     TITLE_ID     ACRONYM  /  TITLE       '
   slabel=widget_label(main,value='SELECT FROM THESE FUNDAMENTAL STUDIES',font=lfont)
   slist1=widget_list(main,value=flabel,font=lfont)
   slist=widget_list(main,ysize=6,font=lfont)
   widget_control,slist,set_value=''

;-- study variation list widget

   svlabel=widget_label(main,value='SELECT FROM THESE STUDY VARIATIONS',font=lfont)
   svlist1=widget_list(main,value=' STUDYVAR             SV_DESC  /   ZONE_ID',ysize=1,font=lfont)
   svlist=widget_list(main,ysize=4,font=lfont)
   widget_control,svlist,set_value=''

;-- raster list widget

  slits=['2x2   ',$
         '4x4   ',$
         '8x51  ',$
         '2x240 ',$
         '4x240 ',$
         '90x240']

   rvlabel=widget_label(main,value='RASTERS FOR SELECTED STUDY',font=lfont)
   rvlabels='  ID   VAR  DET  SLIT"  WIDTH"  HEIGHT"  POINTING    X"     Y"   REPS   EXP(SECS)  DURATION    TEL_RATE'
   rvlist1=widget_list(main,value=rvlabels,ysize=1,font=lfont)
   rvlist=widget_list(main,ysize=6,font=lfont)
   widget_control,rvlist,set_value=''

;-- check DB

  cur_db=getenv('ZDBASE')
  reset=0
  if not exist(sav_db) then sav_db=cur_db
  if cur_db ne sav_db then begin
   sav_db=cur_db & reset=1
   message,'resetting...',/cont
  endif
  if reset then xstudy_reset

;-- realize widgets

   widget_control,main,/realize

;-- list fundamental studies

   xstudy_fsearch,relist=(xstudy_list or reset)

;-- restore last selection

   if exist(study_id) and exist(title_id) and exist(f_studies) then begin
    slook=where( (study_id eq f_studies.study_id) and $
                 (title_id eq f_studies.title_id), cnt)
    if cnt gt 0 then widget_control,slist,set_list_select=slook(0)
    xstudy_vlist
   endif
   rvtags=0 & svtags=0

;--- manage widgets


   expr="xmanager,'xstudy',main,modal=modal,group=group"
   if (idl_release(lower=5,/incl)) and (1-keyword_set(select)) then $
    expr=expr+',/no_block'
   s=execute(expr)
   xmanager_reset,main,modal=modal,group=group,crash='xstudy'

;-- return selected studies

   if exist(study_def) and not xalive(main) then study=study_def

   return
   end

