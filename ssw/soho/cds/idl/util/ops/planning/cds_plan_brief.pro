
PRO cds_plan_brief, startdis, stopdis, details, group=group, text=text, $
                    nobs=nobs, detector=detector, sci_plan=sci_plan,$
                    acronym=acronym, title=title,init=init,point=point
;+
; PROJECT:
;       SOHO - CDS
;
; NAME:
;       CDS_PLAN_BRIEF
;
; PURPOSE:
;       Read detail (or other) plans from DB and display them in text widget
;
; CATEGORY:
;       planning, utility
;
; SYNTAX:
;       cds_plan_brief, startdis, stopdis [, group=group]
;
; INPUTS:
;       STARTDIS - Start date/time in any CDS time format
;       STOPDIS  - Stop date/time in any CDS time format
;
; OPTIONAL INPUTS:
;       DETAILS - Array of detail plan structures. If passed, no DB
;                 read will be conducted; Useful for just displaying
;                 details plans during planning.
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       NOBS     - Number of detailed plans found in between STARTDIS
;                  and STOPDIS 
;       GROUP    - ID of widget who serves as the group leader
;       TEXT     - Just make a text file without popping up a widget
;       DETECTOR - Set this keyword to include detector list of the
;                  study (this can be slow)
;       SCI_PLAN - Set this keyword to list SCI plan only
;       ACRONYM  - Set this keyword to show acronym (or obs_prog)
;       TITLE    - Set this keyword to show title (i.e., SCI_SPEC)
;       INIT     - Set this to initialize XTEXT
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       The calling widget program is put on hold till the pop up
;       widget is killed.
;
; HISTORY:
;       Version 1, April 11, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, May 2, 1996, Liyun Wang, GSFC/ARC
;          Added DETECTOR and SCI_PLAN keywords
;       Version 3, October 1, 1996, Liyun Wang, NASA/GSFC
;          Added ACRONYM keyword
;       Version 4, October 9, 1996, Liyun Wang, NASA/GSFC
;          Added TITLE keyword
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;

   COMMON cds_plan_brief,wbase

   if keyword_set(init) then xkill,wbase

   if not exist(startdis) or not exist(stopdis) then return

   
   IF NOT KEYWORD_SET(text) THEN WIDGET_CONTROL, /hour
   nobs = N_ELEMENTS(details)
   IF nobs EQ 0 THEN BEGIN
      IF NOT KEYWORD_SET(sci_plan) THEN BEGIN 
         IF KEYWORD_SET(point) THEN $
            rd_plan, details, startdis, stopdis, /force, nobs=nobs, $
            type=0, inst='C'$
         ELSE $
            list_detail, startdis, stopdis, details, nobs
      ENDIF ELSE list_plan, startdis, stopdis, details, nobs, inst='C'
   ENDIF

   IF nobs EQ 0 THEN RETURN
   itime = get_plan_itime(details)

   IF NOT KEYWORD_SET(sci_plan) THEN BEGIN 
      out = '  DATE     START     END    DURATION   ID  '
      tout = '     SCI_OBJECT'
      IF KEYWORD_SET(detector) THEN out = out+'DET '
      IF KEYWORD_SET(acronym) THEN out = out+' ACRONYM '
      IF KEYWORD_SET(title) THEN out = out+'    TITLE'
      out = [out+tout, '']
   ENDIF ELSE out = ['  DATE     START     END   DURATION    SCI_OBJECT', '']

   tm = SORT([details.(itime)])
   FOR i = 0, nobs-1 DO BEGIN
      plan = details(i)

      IF NOT KEYWORD_SET(sci_plan) AND tag_exist(plan,'TIME_TAGGED') THEN BEGIN 
         IF plan.time_tagged EQ 0 THEN ttag = '*' ELSE ttag = ' '
;---------------------------------------------------------------------------
;        Get detector and obs_prog
;---------------------------------------------------------------------------
         det_str = ''
         op_str = ''
         IF KEYWORD_SET(detector) OR KEYWORD_SET(acronym) THEN BEGIN
            get_study_par, plan.study_id, plan.studyvar, det=det, $
               acronym=obs_prog
            IF KEYWORD_SET(detector) THEN BEGIN 
               IF det(0) EQ '' THEN det_str = 'E' ELSE BEGIN
                  ii = WHERE(det EQ 'N', cnt)
                  IF cnt EQ N_ELEMENTS(ii) THEN det_str = 'N' ELSE $
                     IF cnt EQ 0 THEN det_str = 'G' ELSE det_str = 'N&G'
               ENDELSE
               det_str = strpad(det_str, 3, /after)
            ENDIF
            IF KEYWORD_SET(acronym) THEN op_str = strpad(obs_prog, 8, /after)
         ENDIF
         sid = strpad(STRTRIM(STRING(plan.study_id), 2), 3)
         svar = strpad(STRTRIM(STRING(plan.studyvar), 2), 2, /after)
         IF KEYWORD_SET(title) THEN $
            spec = strpad(STRTRIM(plan.sci_spec), 12, /after)
      ENDIF

      day = STRMID(anytim2utc(plan.(itime), /date, /ecs),2,20)
      tos = anytim2utc(plan.(itime), /time, /ecs, /trun)
      toe = anytim2utc(plan.(itime+1), /time, /ecs, /trun)
      dur = strpad(sec2dhms(plan.(itime+1)-plan.(itime)),9, /after)
      sci = STRTRIM(plan.sci_obj,2)
      
      IF NOT KEYWORD_SET(sci_plan) AND $
         tag_exist(plan, 'TIME_TAGGED') THEN BEGIN 
         arr_tmp = [day,ttag+tos,toe,dur+sid+'/'+svar]
         IF KEYWORD_SET(detector) THEN arr_tmp = [arr_tmp, det_str]
         IF KEYWORD_SET(acronym) THEN arr_tmp = [arr_tmp, op_str]
         IF KEYWORD_SET(title) THEN arr_tmp = [arr_tmp, spec]
         line = arr2str([arr_tmp, sci], delim=' ') 
      ENDIF ELSE $
         line=arr2str([day,tos,toe,dur,sci], delim=' ')
         
      out = [out, line]
   ENDFOR
   IF KEYWORD_SET(text) THEN BEGIN
      home=getenv('HOME')
      OPENW, unit,concat_dir(home,'cds_plan_brief.txt'), /GET_LUN
      FOR i= 0, N_ELEMENTS(out)-1 DO PRINTF, unit, out(i)
      CLOSE, unit
      FREE_LUN, unit
      PRINT, 'Output file cds_plan_brief.txt created in your login directory.'
   ENDIF ELSE BEGIN
      if keyword_set(sci_plan) then field='SCIENCE' else field='DETAILED'
      xtext, out, title='List of Saved '+field+' Plans', group=group,wbase=wbase
   ENDELSE
   RETURN
END

