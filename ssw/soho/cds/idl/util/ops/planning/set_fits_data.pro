;+
; Project     : SOHO - CDS
;
; Name        : SET_CDS_DATA
;
; Purpose     : Define CDS_FITS_DATA environment variable to
;               available disks, e.g.,
;               setenv,'CDS_FITS_DATA=/cdsfits2/arch/fits:/cdsfits/arch/fits'
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : IDL>set_cds_data
;
; Inputs      : None
;
; Opt. Inputs : DEF_PATH = default path for CDS_FITS_DATA [def = current]
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : IMAX = max disk number to search [def=20]
;               FIRST = put default path first
;               ROOT = lead root directory (def = '/') 
;
; Common      : None
;
; Restrictions: Assumes FITS disk names are mounted as 
;               '/cdsfits#/arch/fits' where # = 2-n
;
; Side effects: CDS_FITS_DATA is defined
;
; History     : Version 1,  31-May-1997,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


pro set_fits_data,def_path,imax=imax,first=first,root=root

if n_elements(imax) eq 0 then imax=25
cd,curr=cur_path
if datatype(def_path) eq 'STR' then begin
 clook=loc_file(def_path,count=count)
 if count ne 0 then cur_path=def_path
endif

if datatype(root) ne 'STR' then root=''
for i=1,imax do begin
 case i of
  1: next=root+'/cdsfits'
  2: next=root+'/cdsfits2'
  else: next=root+'/cdsfits'+trim(string(i))+'/arch/fits'
 endcase
 clook=loc_file(next,count=count)
 if count eq 1 then begin
  if exist(path) then path=next+':'+path else path=next
 endif
endfor

full_path=cur_path
if exist(path) then begin
 if keyword_set(first) then full_path=cur_path+':'+path else $
  full_path=path+':'+cur_path
endif

setenv,'CDS_FITS_DATA='+full_path
return & end
