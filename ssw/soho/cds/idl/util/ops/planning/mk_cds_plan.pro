;+
; PROJECT:
;       SOHO - CDS
;
; NAME:
;       MK_CDS_PLAN
;
; PURPOSE: 
;       Create/edit detailed, science, alternative and flag plan for CDS
;
; CATEGORY:
;       Widgets, planning
; 
; EXPLANATION:
;       
; SYNTAX: 
;       MK_CDS_PLAN
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS: 
;       TSTART - display start time (can be string [e.g. 96/1/1] or TAI format) 
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       GROUP    - event id of widget that calls MK_CDS_PLAN
;       RESET    - purge studies/plan saved in memory
;       NODETACH - Keep the graphic window from being detached from
;                  the main widget if set
;       LOCK -   - LOCK planning databases
;
; COMMON:
;       MK_CDS_COM
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, November 10, 1994, D. M. Zarro, GSFC/ARC. Written
;
; CONTACT:
;       Dominic M. Zarro, GSFC/ARC (zarro@smmdac.nascom.nasa.gov)
;-

pro mk_cds_plan_event,  event                         

;-- event handler routine

@mk_cds_com

;-- intercept pointing trigger from IMAGE_TOOL

itool_reg=(xregistered('image_tool',/noshow) gt 0)
if itool_reg and (!d.window ne win_ed) then begin
 if win_itool lt 0 then win_itool=!d.window
endif
;if (win_itool gt -1) and (itool_reg) and (!d.window ne win_itool) then begin
; dprint,'%resetting IMAGE_TOOL window...'
; setwindow,win_itool
;endif

if event.id eq itool_mess then begin
 mk_cds_receive
 if not itool_reg then begin
  widget_control,imageb,set_value='Image Tool'
  widget_control,imageb,set_uvalue='point'
 endif
 return
endif

;-- do not accept MK_PLAN events during ZOOM

if xregistered('xzoom_plan') gt 0 then widget_control,mk_cds_base,sensitive=0

;-- get widget UVALUE

widget_control, event.id, get_uvalue = uvalue

if not exist(uvalue) then uvalue=''
type=get_plan_type(plan,err=err)
if (type eq -1) then begin
 xack,err,group=mk_cds_base,/icon
 return
endif

;-- update UT time (get timing parameters from child of parent base)
;   Also save latest configuration in case of a crash

if uvalue eq 'ut_update' then begin
 child=widget_info(mk_cds_base,/child)
 widget_control,child,get_uvalue=timer_info
 get_utc, curr_ut, /ecs,/vms
 widget_control,mk_cds_base, $
            tlb_set_title = timer_info.plan_title+strmid(curr_ut,0,20)
 widget_control,mk_cds_base, timer = timer_info.ut_delay

 db_access=mk_plan_priv(/nocheck,/quiet)
 if do_lock and (not locked) and db_access then begin
  lock_zdbase,'mk_plan_lock',/daily,expire=30.*60.,lock=lock_file,$
   /quiet,status=locked
 endif

 if (not updated) and (not stored) then begin
  rcustom=custom_stc
  if exist(plan) then last_rplan=plan else last_rplan=0
  if exist(details) then rdetails=details else rdetails=0
  if exist(last_details) then last_rdetails=last_details else last_rdetails=0
  rm_file,mk_plan_crash
  mk_plan_crash=crash_file+'-'+trim(str_format(utc2tai(curr_ut)))
  dprint,'% MK_CDS_PLAN: auto-saving...'
  save,rdetails,last_rdetails,last_rplan,rcustom,file=mk_plan_crash
  stored=1
 endif
 return
endif

;-- for consistency

itime=get_plan_itime(plan)
if exist(use_gis) then if use_gis then use_gis=tag_exist(plan,'GSET_ID')
if type eq 2 then is_eng=0

;-- user clicked in DRAW widget

help,uvalue
if (uvalue eq 'draw') then mk_cds_draw,event

;-- grab IMAGE_TOOL window

if uvalue eq 'grab_image' then begin
 x2ps,window=win_itool,/print
 return
endif

;-- refresh display

if (uvalue eq 'refresh') then begin
 mk_cds_start,time_change=time_change
 if not time_change then mk_cds_plot
 return
endif

;-- change units of displayed duration

if uvalue eq 'dur_unit' then begin
 dur_index=event.index
 new_dur_unit=([1., 60.0, 3600.0])(dur_index)
 new_dur_unit=new_dur_unit(0)
 if new_dur_unit ne dur_unit then begin
  if xalive(pduration) then begin 
   widget_control,pduration,get_value=dur
   new_val=float(dur(0))*dur_unit/new_dur_unit
   if dur_index eq 3 then new_val=sec2dhms(new_val*3600.d) else $
    new_val=string(new_val,'(f12.2)')
   widget_control,pduration,set_value=new_val
  endif
  if xalive(sduration) then begin 
   widget_control,sduration,get_value=dur
   new_val=float(dur(0))*dur_unit/new_dur_unit
   if dur_index eq 3 then new_val=sec2dhms(new_val*3600.d) else $
    new_val=string(new_val,'(f12.2)')
   widget_control,sduration,set_value=new_val
  endif
  dur_unit=new_dur_unit
 endif
 return
endif

;-- hard copy display

if uvalue eq 'hard' then begin
 xps_setup,pst,group=mk_cds_base,status=status
 if not status then return
 widget_control,/hour
 ps,pst.filename,color=pst.color,port=pst.portrait,encap=pst.encapsulated,$
 copy=pst.copy,inter=pst.interpolate
 mk_cds_plot
 psclose         
 if pst.hard then begin
  err=''
  send_print,pst.filename,que=pst.printer,delete=pst.delete,err=err
  if err eq '' then xtext,'   Print job sent successfully   ',/just_reg,wait=1
 endif
 return
endif

;-- zooming in on plans

if uvalue eq 'apply_zoom' then begin
 child=widget_info(messenger,/child)
 if child gt 0 then begin
  widget_control,child,get_uvalue=zoom_info
  custom_stc.startdis=zoom_info.tstart
  custom_stc.stopdis=zoom_info.tstop
  mk_cds_read
  mk_cds_plot,/time_change
  mk_cds_button
 endif
 return
endif

;-- moving/copying/deleting a set of plans

chk=where(uvalue eq ['apply_move','apply_copy','apply_delete'],cnt)
if cnt gt 0 then begin
 child=widget_info(messenger,/child)
 if child gt 0 then begin
  widget_control,child,get_uvalue=zoom_info
  cfind=mk_plan_find(details,zoom_info.tstart,zoom_info.tstop,$
                     count=count,rest=keep,rcount=rcount)
  if count gt 0 then begin
   pamount=zoom_info.tdur
   dprint,'% MK_CDS_PLAN: move amount = ',pamount
   err=''
   db_details=details
   mk_cds_save
   pmin=min(details(cfind).(itime))
   pmax=max(details(cfind).(itime+1))

   temp=details(cfind)
   case uvalue of
    'apply_delete': begin   
      if (zoom_info.tstart eq custom_stc.startdis) and (zoom_info.tstop eq custom_stc.stopdis) then begin
       val=xanswer('You are about to delete all displayed entries.',$
            instruct='Are you sure?',group=mk_cds_base)
      endif else val=1
      if val then begin
       if rcount gt 0 then details=details(keep) else delvarx,details
      endif else goto,bail
    end
    'apply_move': begin
      details(cfind).(itime)=temp.(itime)+pamount
      details(cfind).(itime+1)=temp.(itime+1)+pamount
     end
    else: begin
     temp.(itime)=temp.(itime)+pamount
     temp.(itime+1)=temp.(itime+1)+pamount
     details=[details,temp]
    end
   endcase

   if exist(details) then begin
    smin=min(details(cfind).(itime)) 
    smax=max(details(cfind).(itime+1))
   endif else begin
    smin=pmin
    smax=pmax
   endelse

   tcal=pmin < smin
   pmess='Please wait. Performing operation...'
   xtext,pmess,wbase=tbase,/just_reg,/hour
   mk_plan_order,details,ndetails,custom_stc.startdis,custom_stc.stopdis,err=err,tgap=tgap,$
    tcal=tcal,group=mk_cds_base
   if err eq '' then begin
    updated=0 & stored=0 & delvarx,hplan
    if exist(ndetails) then details=ndetails else delvarx,details
    mk_plan_load,type,details
    mk_cds_plot
    mk_cds_refresh
    mk_cds_sensit
    xtext,[pmess,'Operation completed successfully.'],/wait,/just_reg,wbase=tbase
   endif else begin
    xack,err,group=mk_cds_base
bail:if exist(db_details) then details=db_details
   endelse
  endif else xack,'No entries in selected range.',group=mk_cds_base
 endif
 return
endif

;-- quit XZOOM_PLAN

chk=where(uvalue eq ['quit_zoom','quit_move','quit_copy','quit_delete'],cnt)
if cnt gt 0 then begin
 mk_cds_plot
 return
endif

;-- view structure details

if uvalue eq 'show_stc' then begin
 show_stc = event.select
 if not show_stc then xkill, wbase
 return
endif

;-- copy SCI plan info 

if uvalue eq 'get_sci' then begin
 get_sci=event.select
 return
endif

;-- auto copy pointing to IMAGE_TOOL

if uvalue eq 'itool_point' then begin
 itool_point=event.select
 return
endif

;------------------------------ FILE CONTROL PARAMETERS --------------------

;-- run XCAMP

if uvalue eq 'xcamp' then begin
 xcamp,/last,tstart=custom_stc.startdis,tend=custom_stc.stopdis,group=mk_cds_base
 return
endif

;-- run XCAT

if uvalue eq 'xcat' then begin
 t1=tai2utc(custom_stc.startdis)
 t1.time=0
 t2=tai2utc(custom_stc.stopdis)
 t2.time=0
 if t2.mjd le t1.mjd then t2.mjd=t1.mjd+1
 xcat,tstart=t1,tend=t2,/last,group=mk_cds_base
 return
endif

;-- run XCAT summary

if uvalue eq 'xcat_sum' then begin
 widget_control,/hour
 xtext,'Please wait. Producing XCAT summary listing...',wbase=tbase,/just_reg
 widget_control,mk_cds_base,sensitive=0
 t1=tai2utc(custom_stc.startdis)
 t1.time=0
 t2=tai2utc(custom_stc.stopdis)
 t2.time=0
 if t2.mjd le t1.mjd then t2.mjd=t1.mjd+1
 err=''
 plan_xcat_summ,t1,t2,err=err
 widget_control,mk_cds_base,/sensitive
 xkill,tbase
 if err ne '' then xack,err
 return
endif

;-- list detailed plans

if uvalue eq 'dlist' then begin
 cds_plan_brief,custom_stc.startdis,custom_stc.stopdis,$
  group=mk_cds_base,sci_plan=(type eq 2),/det,/acr
 return
endif

;-- xdoc

if uvalue eq 'xdoc' then begin
 xdoc,group=mk_cds_base
 return
endif

;-- help

if uvalue eq 'help' then begin
 widg_help,'mk_cds_plan.hlp',group=mk_cds_base,/modal,/hier,$
                    font='9x15bold',tfont='9x15bold',xtextsize=70
 return
endif

;-- write highlighted entry to file

if uvalue eq 'dump' then begin
 inst='C'
 if exist(hplan) then begin
  dplan=mk_plan_corr(hplan)
  keys=['det','flag','sci','alt']
  key=keys(type) & dfile=''
  call_it='show_plan,dplan,file=dfile,/quiet,inst=inst,/keep,err=err,/'+key
  err=''
  s=execute(call_it)
  if dfile eq '' then err='Could not write file'
  if (err ne '') then begin
   xack,err,group=mk_cds_base,/icon
   return
  endif
  mplan=rd_ascii(dfile)
  dump_mess='Following plan info will be printed to file: '+dfile
  amess=[dump_mess,'         ',mplan]
  value=xanswer(amess,group=mk_cds_base,instruct='Print?') 
  if value then xprint,dfile,group=mk_cds_base else $
   s=delete_file(dfile,/noconf)
 endif else begin
  xack,'No plan entry loaded',group=mk_cds_base,/icon
 endelse
 return
endif

;-- export plans

if uvalue eq 'export' then begin
 mk_cds_update
 t1=tai2utc(custom_stc.startdis)
 t1.time=0
 t2=tai2utc(custom_stc.stopdis)
 t2.time=0
 if t2.mjd le t1.mjd then t2.mjd=t1.mjd+1
 xport,t1,t2,group=mk_cds_base,/modal
 return
end

;-- import plans

if uvalue eq 'import' then begin
 warn='WARNING: Imported plans will overwrite existing plans.'
 val=xanswer(warn,group=mk_cds_base,instruct='Continue?')
 if val then begin
  ipath=trim(getenv('CDS_IMPORT'))
  epath=trim(getenv('CDS_EXPORT_W'))
  cd,curr=curr
  path=ipath
  if path eq '' then path=epath
  if path eq '' then path=curr
  imp_file=pickfile(group=mk_cds_base,filter='*.plan',$
             title='Enter plan file name to import',path=path)
  if trim(imp_file) ne '' then begin
   err=''
   xtext,'Please wait. Importing plans...',/just_reg,wbase=tbase
   widget_control,mk_cds_base,/hour,sensitive=0
   import_plan,imp_file,err=err
   xkill,tbase
   if err ne  '' then xack,err,group=mk_cds_base else begin
    mk_cds_reset
    mk_cds_reread
   endelse
   widget_control,mk_cds_base,/sensitive
  endif
 endif
 return
endif

;-- study briefing 

if uvalue eq 'brief' then begin
 ok_to_list=0
 if exist(sel_study) then begin
  acronym=trim(sel_study.obs_prog)
  study_brief,acronym,save_text=save_text,/quiet
  ok_to_list=1
  if n_elements(save_text) gt 1 then begin
   xtext,save_text,wbase=fbase,group=mk_cds_base,/no_print,/no_save,/no_find 
   return
  endif else begin
   xack,acronym+' not found in STUDY_BRIEF file',group=mk_cds_base
  endelse
 endif
 if not ok_to_list then begin
  xack,'Please select a study.',group=mk_cds_base
 endif
 return
endif

;-- check some input widgets in case user forgot to hit return

no_check=where(uvalue eq ['remove','undo','sci_obj','sci_spec','obj_id','cmp_no','reset'],cnt)
if cnt eq 0 then mk_cds_check,event

;------------------------ DATABASE CONTROL PARAMETERS ------------------

;-- switch plan type 

if uvalue eq 'switch_ptype' then begin
 case (event.index) of
  0: if new_edit_item(custom_stc, 'CDS-DET') eq 0 then return
  1: if new_edit_item(custom_stc, 'CDS-FLAG') eq 0 then continue=1
  2: if new_edit_item(custom_stc, 'CDS-SCI') eq 0 then return
  3: if new_edit_item(custom_stc, 'CDS-ALT') eq 0 then return
  else: return
 endcase

;-- save latest changes

 curr_items=custom_stc.req_items
 nplot=n_elements(curr_items)
 new_type=get_plan_type(curr_items(nplot-1))

 mk_cds_switch,new_type
 type=new_type
 itime=get_plan_itime(type)

;-- ensure that DETAILS appears above FLAG

 if (type eq 1) then begin
  dlook=where(curr_items ne 'CDS-DET',cnt)
  if cnt gt 0 then curr_items=curr_items(dlook) 
  curr_items=[curr_items,'CDS-DET']     
  nplot=n_elements(curr_items)
  curr_items([nplot-2,nplot-1])=curr_items([nplot-1,nplot-2])
  custom_stc=rep_tag_value(custom_stc,curr_items,'req_items')
  delvarx,ctime,etime
 endif

 pindex=0 & rindex=0
 mk_cds_read,/check
 mk_cds_hplan
 mk_cds_plot
 mk_cds_refresh
 mk_cds_sensit
endif

;-- switch database type (official CDS/ personal USER)

if uvalue eq 'switch_db' then begin
 if (event.index eq 0 and strupcase(which_zdbase()) eq 'USER') or $
    (event.index eq 1 and strupcase(which_zdbase()) eq 'CDS') then return
 err=''
 mk_cds_update

 if event.index eq 0 then begin
  stat=fix_zdbase(/user,err=err)
 endif else begin 
  stat=fix_zdbase(/cds,err=err)
 endelse
 
 if not stat then begin
  xack, err, group=mk_cds_base, /icon
  if widget_info(db_type,/type) eq 8 then widget_control,db_type,set_droplist_select=1-event.index else $
   widget_control,db_type,set_value=1-event.index
  return
 endif
 delvarx,ctime,etime,hplan
 sav_db=getenv('ZDBASE')

;-- check if DB is locked

 if do_lock then begin
  lock_zdbase,'mk_plan_lock',/daily,expire=30.*60.,lock=lock_file,$
   status=locked
 endif
 db_access=mk_plan_priv(err=err,lock=lock_file,/quiet)
 if not db_access then $
  xack,[err,'','You will not be allowed to edit plans.',''],group=mk_cds_base
 mk_cds_reset
 mk_cds_reread
endif

;-- quit here and purge DB

if uvalue eq 'done' then begin
 if not updated then begin
  value=xanswer('Latest changes have not been saved.',$
                instruct='Save before exiting?',group=mk_cds_base)
  if value then mk_cds_update
 endif
 
 if db_access then begin
  if not exist(last_purge) then last_purge=1
  if (last_purge eq 0) or (last_purge gt 10) then begin
   message,'purging DB...',/cont
   xtext,'Please wait. Purging Databases...',/just_reg,wbase=tbase,/hour
   !priv=3
   types=[0,1,2,3]
   for i=0,n_elements(types)-1 do s=call_function(get_plan_funct(types(i),/prg))
   last_purge=0
   xkill,tbase
  endif
  last_purge=last_purge+1
 endif

 if itool_reg then begin
  value=xanswer('IMAGE_TOOL is still running.',$
                 instruct='Quit IMAGE_TOOL?',group=mk_cds_base)
  itool_base=get_handler_id('IMAGE_TOOL')
  if value then xkill,itool_base else xshow,itool_base
 endif

 xkill,mk_cds_base,wbase,tbase

 return
endif

;-- check Raster modes

if uvalue eq 'rate_check' then begin

 if not exist(details) then begin
  xack,'No Detailed CDS plans during displayed interval.'
  return
 endif

 if not exist(dsn) then begin
  rd_resource,dsn,custom_stc.startdis,custom_stc.stopdis,nres=ndsn
  if ndsn eq 0 then begin
   xack,'No DSN schedule available during displayed interval.'
   return
  endif
 endif

 rate_mess='Please wait. Checking raster modes against DSN telemetry submodes...'
 xtext,rate_mess,/just_reg,wbase=tbase,/hour
 flagged=check_cds_modes(details,dsn,count=count,/verb,prob=prob)
 if count eq 0 then begin
  xtext,[rate_mess,'All planned raster modes consistent with DSN telemetry rates.'],wbase=tbase,/just_reg,wait=3
  return
 endif
 xkill,tbase
 warnings=['Following studies have problems with mismatched raster and DSN telemetry rates:','']
 nok=where(flagged eq 1)
 for i=0,count-1 do begin
  index=nok(i)
  bplan=details(index)
  warn=anytim2utc(bplan.(itime),/vms)+' ['+trim(bplan.sci_obj)+'] -> '+prob(index)
  warnings=[warnings,warn]
 endfor
 xack,warnings
 return
endif

;-- reset plan

if (uvalue eq 'reset') then begin
 if not updated then begin
  proc=xanswer('WARNING: Current plans have not been saved.',$
                group=mk_cds_base,instruct='Proceed with reset?')
  if not proc then return
 endif
 mk_cds_reset
 mk_cds_reread
endif

;------------------------------ PLAN TIME CONTROL PARAMETERS --------------------

;-- change plan duration (SCIENCE only)

if (uvalue eq 'pduration') then begin
 widget_control,pduration,get_value=dur
 dur=dur_unit*float(trim(dur(0))) > 0.
 cur_dur=(plan.(itime+1)-plan.(itime)) > 0.
 if cur_dur ne dur then begin
  etime=plan.(itime)+dur
  plan.(itime+1)=etime
  mk_cds_etime
 endif
endif

;------------------------------ DISPLAY CONTROL PARAMETERS --------------------

;-- change start time of display

if uvalue eq 'enter' then mk_cds_start

if uvalue eq 'zoom_in' then begin
 mk_cds_update,/noplot
 mk_cds_plot
 xzoom_plan,group=mk_cds_base,info=zoom_info,messenger=messenger,$
  tstart=custom_stc.startdis,tstop=custom_stc.stopdis
endif

;-- change duration of display

if uvalue eq 'tshift' then begin
 nshift=n_elements(sec_spans)
 if event.index lt (nshift-1) then begin
  old_sdur=(custom_stc.stopdis-custom_stc.startdis)
  mk_cds_startdis,change=time_change
  new_sdur = sec_spans(event.index)
  replot=(old_sdur ne new_sdur) or time_change
  if replot then begin
   mk_cds_update
   custom_stc.stopdis =custom_stc.startdis+new_sdur
   mk_cds_read
   mk_cds_plot,/time_change
   mk_cds_button
  endif
 endif
endif

;-- shift display window

time_shift = ['BACKWK', 'BACKDAY', 'BACKHR', 'FORWK', 'FORDAY', 'FORHR']
sec_shift = [7., 1.d, 3600./secs_day]*secs_day
sec_shift = [-sec_shift, sec_shift]
chk = where(uvalue eq time_shift, cnt)

if (chk(0) ge 0) then begin
 cdur=(custom_stc.stopdis-custom_stc.startdis)
 mk_cds_startdis
 new_start=custom_stc.startdis+sec_shift(chk(0))
 custom_stc.startdis=new_start
 custom_stc.stopdis=new_start+cdur
 mk_cds_update
 mk_cds_read
 mk_cds_plot,/time_change
 mk_cds_button
endif

;-- step thru plans

if strmid(uvalue,0,4) eq 'step' then begin
 index=mk_plan_where(hplan,details)
 if (index eq -1) then index=mk_plan_where(ctime,details)
 start_index=mk_plan_where(custom_stc.startdis,details,status=in_db)
 if not in_db then start_index=start_index+1
 stop_index=mk_plan_where(custom_stc.stopdis,details,status=in_db)
 nplans=n_elements(details)
 case uvalue of
  'step_back' : nindex=index-1
  'step_next' : nindex=index+1
  'step_first' : nindex=start_index
  'step_last': nindex=stop_index
  else: return
 endcase
 nindex=0 > nindex < (nplans-1)
 nindex=start_index > nindex < stop_index 
 if nindex ne index then begin
  mk_cds_warn,skip=skip
  if skip then return
  hplan=details(nindex)
  plan=hplan
  ctime=plan.(itime)
  mk_cds_plot
  mk_cds_refresh
  mk_cds_sensit
  if itool_point then mk_cds_send
 endif
endif

;-- customize display

if uvalue eq 'customize' then begin
 mk_plan_custom,custom_stc,status=status,group=mk_cds_base,/modal,/notime
 if not status then return
 tacc=custom_stc.troff*60.
 nocolor=custom_stc.nocolor
 delvarx,hplan
 curr_items=custom_stc.req_items
 nplot=n_elements(curr_items)
 ttype = get_plan_type(curr_items(nplot-1))
 mk_cds_switch, ttype
 type=ttype
 itime=get_plan_itime(type)
 if widget_info(ptype_bt,/type) eq 8 then widget_control, ptype_bt, set_droplist_select=type else $
  widget_control, ptype_bt, set_value=type
 mk_cds_update
 mk_cds_read,/check
 mk_cds_plot
 mk_cds_refresh
 mk_cds_sensit
endif

;-------------- RASTER CONTROL PARAMETERS ----------------------------

;-- raster slider (show pointings for each raster)

if (uvalue eq  'rslider') then begin
 widget_control,rslider,get_value=rindex
 mk_cds_point
endif


;-------------- POINTING CONTROL PARAMETERS ----------------------------

;-- feature track

if uvalue eq  'tracking_yes' then plan.tracking=1
if uvalue eq  'tracking_no' then plan.tracking=0

;-- rotate pointing

if (strmid(uvalue,0,4) eq 'rotp') then begin
 perr=''
 plan=mk_plan_corr(plan)
 index=mk_plan_where(plan,details)
 if index gt 0 then last_plan=mk_plan_corr(details(index-1))
 if index lt (n_elements(details)-1) then next_plan=mk_plan_corr(details(index+1))

;-- check for previous or following plans

 if uvalue eq 'rotp_next' then begin
  if not exist(next_plan) then begin
   xack,'No entry follows current plan.',/icon
   return
  endif
 endif 

 if (uvalue ne 'rotp') then begin
  if not exist(last_plan) then begin
   xack,'No entry precedes current plan.',/icon
   return
  endif
 endif

;-- don't rotate, but inherit previous or next pointing instead

 fmess=''
 case uvalue of

  'rotp_prev': begin
    err=''
    get_cds_xy,last_plan,xlast,ylast,/last,err=err
    if err ne '' then begin
     xack,err
     return
    endif
    plan.pointings.ins_x=xlast
    plan.pointings.ins_y=ylast
   end

  'rotp_next': begin
    err=''
    get_cds_xy,next_plan,xnext,ynext,/first,err=err
    if err ne '' then begin
     xack,err
     return
    endif
    plan.pointings.ins_x=xnext
    plan.pointings.ins_y=ynext
   end

;-- else rotate

  else: begin
    fmess=[perr,'Please wait. Rotating...                       ']
    xtext,fmess,/just_reg,wbase=tbase,ysize=10
    widget_control,/hour
    if (uvalue eq 'rotp_last') or (uvalue eq 'rotp_match') then $
     if exist(last_plan) then ref_plan=last_plan 
    rot_plan=rot_cds_xy(plan,err=err,rplan=ref_plan,$
             match=uvalue eq 'rotp_match',warn=warn)
    if err ne '' then begin
     xkill,tbase
     xack,err,group=mk_cds_base
     return
    endif
    if exist(warn) then begin
     options=['Continue','Abort']
     val=xchoice(warn,options,group=mk_cds_base,/row)
     if val eq 1 then begin
      xkill,tbase & return
     endif
    endif
    rot_plan=mk_plan_xy(rot_plan)
    if match_struct(rot_plan.pointings,plan.pointings) then begin
     fmess=[fmess,'These coordinates have already been rotated.']
     xtext,fmess,/just_reg,wbase=tbase,/wait
     return
    endif else plan=rot_plan
    if plan.tracking then begin
     wmess=['WARNING: Feature Tracking is already enabled for this study.',$
           'Strongly recommend switching Feature Tracking OFF when using rotation compensation.']
     xack,wmess
    endif
    fmess=[fmess,'Coordinate rotation completed.']
    xtext,fmess,/just_reg,wbase=tbase,/wait
   end
 endcase

;-- decide whether to hold in memory or replace current entry

 mk_cds_rep,fmess,status=status,wbase=tbase
 if itool_point and status then mk_cds_send
 endif


;-- recover last IMAGE_TOOL pointing
  
if (uvalue eq 'lastp') and (type ne 2) then begin
 if exist(last_point) then begin
  xlist,last_point,index,/modal,/sel,group=mk_cds_base,/clear,$
     title='IMAGE_TOOL Pointing Histort',/remo
  if index gt -1 then begin
   xp=last_point(index).ins_x
   yp=last_point(index).ins_y
   plan=mk_plan_corr(plan)
   plan.pointings(pindex).ins_x=xp
   plan.pointings(pindex).ins_y=yp
   plan=mk_plan_xy(plan)
   mk_cds_rep,status=status
   if itool_point and status then mk_cds_send
  endif
 endif else xack,'No IMAGE_TOOL pointing history available.'
endif

;-- send pointing to IMAGE_TOOL

if uvalue eq 'send_point' then mk_cds_send,auto=1

;-- change number of engineering study pointings (0 or 1)

if (uvalue eq 'e_pointings') then begin
 npoint=event.index
 if npoint ne plan.n_pointings then begin
  plan.n_pointings=npoint
  mk_cds_sdur
  mk_cds_point
  if itool_point then mk_cds_send
 endif
endif

;-- show deferred pointings

if (uvalue eq 'pslider') then begin
 widget_control,rslider,get_value=rindex
 rindex = (rindex > 0)
 widget_control,pslider,get_value=pindex
 mk_cds_point
endif

;-- change deferred pointing

if uvalue eq 'point' then begin
 mk_point_stc,point_stc
 point_stc.date_obs=custom_stc.startdis
 if exist(plan) then begin
  valid_plan=1
  if type ne 2 then valid_plan=(plan.study_id gt -1)
  if valid_plan then begin
   plan=mk_plan_corr(plan) 
   point_stc=mk_plan_point(plan)
   point_stc.date_obs=plan.(itime)
   if new_study then point_stc.date_obs=ctime
  endif
 endif
 widget_control,imageb,set_value='Send Point'
 widget_control,imageb,set_uvalue='send_point'
 xack,imess,group=mk_cds_base,/sup
 point_stc.messenger = itool_mess
 if not itool_reg then begin
  xtext,'Please wait while IMAGE_TOOL is being loaded...',wbase=tbase,/just_reg,/hour,wait=1
 endif
 image_tool,point=point_stc,auto=2
 xkill,tbase
 mk_cds_plot
 return
endif

;-- SCI Plan pointing

if uvalue eq 'bxcen' then begin
 text=plan.xcen
 xinput,text,'Enter X-coordinates, e.g., 400,500.. [50 char max]',group=mk_cds_base,$
    /modal,max_len=50,status=status
 if status then begin
  plan.xcen=text
  widget_control,/hour
  plan=mk_plan_xy(plan,'(i8)')
  widget_control,xcen,set_value=plan.xcen
 endif
endif

if uvalue eq 'bycen' then begin
 text=plan.ycen
 xinput,text,'Enter Y-coordinates, e.g., 400,500.. [50 char max]',group=mk_cds_base,$
    /modal,max_len=50,status=status
 if status then begin
  plan.ycen=text
  widget_control,/hour
  plan=mk_plan_xy(plan,'(i8)')
  widget_control,ycen,set_value=plan.ycen
 endif
endif

;-------------------GIS CONTROL PARAMETERS ------------------------------------

;-- get ZONE_ID and GIS setup

chk=strpos(uvalue,'NDATA_')
if (chk gt -1) and (not is_eng) then begin
 under=strpos(uvalue,'_')
 item=strmid(uvalue,under+1,4)

 if zone_def_id(item) eq 0 then return
 mk_cds_zone,old_zone_id
 if exist(fil_id) then old_fil_id=fil_id
 if exist(ffb) then old_ffb=ffb
 fil_id=0 & delvarx,ffb

;-- look for FILAMENT keyword

 fil=strpos(item,'FI')
 if fil gt -1 then begin
  if not use_gis then begin
   xack,['No GIS rasters in currently selected study.'],group=mk_cds_base,/icon
   return
  endif
  if strmid(uvalue,under+1,1) eq 'N' then ffb=-1
  if strmid(uvalue,under+1,1) eq 'P' then ffb=1
  fil_id=fix(strmid(uvalue,under+2,1))
 endif
 widget_control,fil_id_lab,set_value=trim(string(fil_id,'(i4)'))

 zone_id=zone_def_id(item) 
 zmess=trim(string(zone_id))+' - '+trim(zone_def_label(zone_id))
 widget_control,zmode,set_value=zmess

;-- update ZONE_ID in pointing structure

 if plan.n_pointings gt 0 then plan.pointings(0).zone_id=zone_id

;-- instruct user to re-select GSET ID

 if use_gis then begin
  new_ffb=0 & new_fil_id=0 & new_zone_id=0
  if exist(old_zone_id) then new_zone_id=(zone_id ne old_zone_id)
  if exist(old_ffb) and exist(ffb) then new_ffb=(old_ffb ne ffb)
  if exist(old_fil_id) and exist(fil_id) then new_fil_id=(old_fil_id ne fil_id)
  if new_ffb or new_fil_id or new_zone_id then begin
   plan.gset_id=-1
   widget_control,gset_id,set_value=trim(string(plan.gset_id,'(i4)'))
   widget_control,fil_id_lab,set_value=trim(string(fil_id,'(i4)'))
   mk_cds_gis
  endif
 endif

endif

;-- select GIS setup ID

if uvalue eq  'gset_id' then mk_cds_gis

if uvalue eq 'get_raw_yes' then plan.get_raw=event.select

;-------------------------- FLAG CONTROL PARAMETERS ---------------------

;-- FLAG receipt pointing
 
if uvalue eq 'spoint' then begin
 plan.n_pointings=1
 widget_control,n_pointings,set_value='1'
 mk_cds_point
end

if uvalue eq  'repoint' then begin 
 plan.n_pointings=0
 plan.repoint=event.select 
 widget_control,n_pointings,set_value='0'
 mk_cds_point
end

if uvalue eq 'nrepoint' then begin 
 plan.n_pointings=0
 plan.repoint=1-event.select 
 widget_control,n_pointings,set_value='0'
 mk_cds_point
endif

;-- FLAG master pointing

if uvalue eq 'SOHO-wide' then begin
 if event.select and tag_exist(plan,'FLAG_MASTER') then begin
  plan.flag_master=abs(plan.flag_master)
 endif
endif

if uvalue eq 'CDS-local' then begin
 if event.select and tag_exist(plan,'FLAG_MASTER') then begin
  plan.flag_master=-abs(plan.flag_master)
 endif
endif

;-- FLAG event ID

if uvalue eq  'flag_master' then begin
 id_list='ID:'+sindgen(5)
 text=id_list(abs(plan.flag_master))
 temp = xsel_list(id_list, group = mk_cds_base, title = 'FLAG MASTER ID', $
                    subtitle = 'Available Event Generator IDs', $
                    initial = text, status = status, /index,/no_remove)
   
 if not status then return
 if plan.flag_master lt 0 then plan.flag_master=-temp else $
 plan.flag_master=temp
 widget_control,flag_master,set_value=trim(num2str(temp))
endif

;-------------------------- PLAN DESCRIPTION PARAMETERS ---------------------

;-- Time-Tag study

if uvalue eq 'time_tag_yes' then plan.time_tagged=1b
if uvalue eq 'time_tag_no' then plan.time_tagged=0b

;-- Enforce 5% rule?

if uvalue eq  'rule_yes' then rule_on=1 
if uvalue eq  'rule_no' then rule_on=0

;-- CMP_NO

if uvalue eq 'cmpb' then mk_cds_cmp,/select

if uvalue eq  'cmp_no'  then begin
 widget_control,event.id,get_value=temp
 plan.cmp_no=temp(0)
endif

;-- PROG_ID

if uvalue eq 'prgb' then begin
 pid=plan.prog_id
 xprogram,pid,group=mk_cds_base,status=status
 if status then begin
  plan.prog_id=pid
  widget_control,prog_id,set_value=trim(num2str(plan.prog_id))
 endif
endif  

;-- Disturbances

if uvalue eq  'bdisturbances' then begin
 text=plan.disturbances
 xinput,text,'Enter description for Disturbances [50 char max]',group=mk_cds_base,$
    /modal,max_len=50,status=status
 if status then begin
  plan.disturbances=text
  widget_control,disturbances,set_value=text
 endif
endif

if uvalue eq  'disturbances' then begin
 widget_control,event.id,get_value=temp
 plan.disturbances=temp(0)
end
 
;-- Notes

if uvalue eq 'bnotes' then begin
 text=plan.notes
 xinput,text,'Enter Notes [50 char max]',group=mk_cds_base,$
    /modal,max_len=50,status=status
 if status then begin
  plan.notes=text
  widget_control,notes,set_value=text
 endif
endif

if uvalue eq 'notes' then begin
 widget_control,event.id,get_value=temp
 plan.notes=temp(0)
endif
 
;-- Object ID

if uvalue eq 'bobj_id' then begin

 if not exist(obj_list) then obj_list=''
 if exist(details) then obj_list=[details.obj_id,obj_list]
 if exist(obj_list) then begin
  obj_list=trim(obj_list)
  obj_list=get_uniq_list(obj_list)
  no_blanks=where(obj_list ne '',cnt)
  if cnt gt 0 then obj_list=obj_list(no_blanks) else delvarx,obj_list
 endif

 if exist(obj_list) then begin
  text = plan.obj_id
  if trim(text) eq '' then text=obj_list(0)
  temp = xsel_list(obj_list, group = mk_cds_base, title = 'OBJ_ID', $
                    subtitle = "History of Object ID's", $
                    initial = text, /update, status = status)
  temp=strmid(trim(temp),0,6)
 endif else begin
  temp=plan.obj_id
  xinput,temp,'Enter Object ID [6 char max]',group=mk_cds_base,$
    /modal,max_len=6,status=status
  temp=strmid(trim(temp),0,6)
  if status then obj_list=temp
 endelse

 if status then begin
  plan.obj_id =temp 
  widget_control,obj_id,set_value=plan.obj_id
 endif
endif

if uvalue eq  'obj_id'  then begin
 widget_control,event.id,get_value=temp
 plan.obj_id=strmid(trim(temp(0)),0,6)
endif
 
;-- Object list

if uvalue eq  'object' then begin
 mk_cds_obj,status=status
 if not status then return
endif

;-- Science OBJ

if uvalue eq 'bsci_obj' then begin
 mk_cds_sci,status=status
 if not status then return
endif

if uvalue eq 'sci_obj'  then begin
 widget_control,event.id,get_value=temp
 plan.sci_obj = strmid(trim(temp(0)),0,50)
endif
 
;-- Specific Science OBJ

if uvalue eq 'bsci_spec' then begin

 if not exist(spec_list) then spec_list=''
 if exist(details) then spec_list=[details.sci_spec,spec_list]
 if exist(spec_list) then begin
  spec_list=trim(spec_list)
  spec_list=get_uniq_list(spec_list)
  no_blanks=where(spec_list ne '',cnt)
  if cnt gt 0 then spec_list=spec_list(no_blanks) else delvarx,spec_list
 endif

 if exist(spec_list) then begin
  text = trim(plan.sci_spec)
  temp = xsel_list(spec_list, group = mk_cds_base, title = 'SCI_SPEC', $
                    subtitle = 'Recent Science Specific Objectives', $
                    initial = text, /update, status = status)
  temp=strmid(trim(temp),0,50)
 endif else begin
  temp=plan.sci_spec
  xinput,temp,'Enter Specific Science Objective [50 char max]',group=mk_cds_base,$
    /modal,max_len=50,status=status
  temp=strmid(trim(temp),0,50)
  if status then spec_list=temp
 endelse

 if status then begin
  plan.sci_spec =temp 
  widget_control,sci_spec,set_value=plan.sci_spec
 endif
endif

if uvalue eq  'sci_spec' then begin
 widget_control,event.id,get_value=temp
 plan.sci_spec = strmid(trim(temp(0)),0,50)
endif

;-------------------------- EDITING OPERATIONS --------------------------


edit_ops=['update','add','move','copy','save','remove','rkap','mk_study',$
          'clone','swap',$
          'upd_sci','move_plans','copy_plans','delete_plans']

elook=where(uvalue eq edit_ops,ecount)

if (ecount gt 0) then begin
 if not db_access then begin
  xack,db_err,group=mk_cds_base,/icon
  return
 endif
endif

;-- get a study from study DB 

if (uvalue eq 'get_study') or (uvalue eq 'get_book') then begin

;-- remind user to save latest edits

 mk_cds_warn,skip=skip
 if skip then return
 if uvalue eq 'get_study' then xstudy,db_study,group=mk_cds_base,/sel else begin
  xcds_book,db_study,group=mk_cds_base,/sel,/remove,/clear
 endelse

;-- check for variations and rasters

 if exist(db_study) then begin
  if not tag_exist(db_study,'STUDYVAR') then $
   message,'selected study does not have variations',/cont
  if not tag_exist(db_study,'RASTERS') then begin
   message,'selected study does not have rasters',/cont
   db_study.n_raster_def=0
  endif

;-- check for valid telemetry mode

  if db_study.n_raster_def gt 0 then begin
   get_raster_par,db_study.rasters,tel=tel
   chk=where(tel eq 'M',count)
   if count ne db_study.n_raster_def then begin
    serr=['Warning: Study - '+db_study.obs_prog+' contains High and/or Low rate telemetry modes.']
    xack,serr
   endif
  endif

;-- copy study parameters to plan

  def_inst_plan,plan,type=type,/init
  def_cds_study,sel_study,/rasters
 
;-- append raster info

  if tag_exist(db_study,'RASTERS') then $
   sel_study=rep_tag_value(sel_study,db_study.rasters,'RASTERS')
  copy_struct,db_study,sel_study
  plan.study_id=sel_study.study_id
  plan.studyvar=sel_study.studyvar
  err=''
  get_study_par,study=sel_study,reps=reps,gis=use_gis,slits=slits,$
                eng=is_eng,err=err,dets=dets,zone_id=zone_id
  if err ne '' then message,err,/cont

  if not exist(reps) then plan.n_rasters1=0 else $
   plan.n_rasters1=reps(n_elements(reps)-1) 
  if is_eng then plan.n_pointings=(plan.n_pointings < 1)
  if is_eng then plan.n_pointings=0
  if (sel_study.var_point eq 'N') then plan.n_pointings=0 else begin
   if tag_exist(plan,'pointings') then plan.pointings.zone_id=zone_id
  endelse
  

;-- set start time

  if exist(ctime) then plan.(itime)=ctime else plan.(itime)=custom_stc.startdis

;-- set stop time

  if (type ne 0) and exist(etime) then plan.(itime+1)=etime
  pindex=0 & rindex=0

;-- hard wire some common studies

  mk_cds_special

;-- construct SCI_SPEC

  if trim(plan.sci_spec) eq '' then plan.sci_spec=get_plan_spec(plan)

;-- instruct user on what to do 

  mk_cds_sdur
  mk_cds_refresh
  mk_cds_sensit
  xack,cmess,group=mk_cds_base,instruct='GO',/suppress
  new_study=1 
  if itool_point then mk_cds_send
 endif

endif

;-- create a new study

if uvalue eq 'mk_study' then begin
 mk_study,group=mk_cds_base,/reset,/modal
 dprint,'%out of MK_STUDY'
 mk_cds_plot
 return
endif

;-- check study for any missing components

clook=where(uvalue eq ['add','move'],count)
if count ne 0 then begin

;-- ensure that a valid study is loaded

 if (type ne 2) then begin
  if (plan.study_id lt 0) or (plan.studyvar lt 1) then begin
   xack,err_mess0,group=mk_cds_base,/icon
   return
  endif
 endif

;-- start time entered?

 if (not exist(ctime)) and (event.id ne repb) and (event.id ne appb) then begin
  xack,err_mess1,group=mk_cds_base,/icon
  return
 endif

;-- ensure that end time is greater than start time

 if type ne 0 then begin
  if (plan.(itime+1) le plan.(itime)) and (plan.(itime+1) le ctime)  then begin
   xack,err_mess2,group=mk_cds_base,/icon
   return
  endif
 endif

;-- warn about tagging first study

 if (type eq 0) then begin
  if not plan.time_tagged then begin
   dstart=anytim2utc(plan.(itime))
   if dstart.time eq 0 then begin
    xack,'Studies commencing at 0:00 UT will be Time-Tagged',/suppress
   endif
  endif
 endif

;-- move study?

 if uvalue eq 'move' then mk_cds_move,replace=event.id eq repb

;-- append study?

 if (event.id eq appb) then mk_cds_add,/append

;-- does CTIME overlap existing plan?

 if (event.id eq insb) then mk_cds_insert

endif

;-- move/copy/delete a set of plans

chk=where(uvalue eq ['move_plans','copy_plans','delete_plans'],cnt)
if cnt gt 0 then begin
 mk_cds_plot
 s='xzoom_plan,group=mk_cds_base,info=zoom_info,messenger=messenger'
 s=s+',tstart=custom_stc.startdis,tstop=custom_stc.stopdis,/'+strmid(uvalue,0,3)
 s=execute(s)
 return
endif

;-- CLONING

if uvalue eq 'clone' then begin
 tstart=custom_stc.startdis
 xclone_plan,tstart,group=mk_cds_base,/modal,err=err,$
  def_clone='CDS-SCI',status=status
 return
endif

;-- COPY

if uvalue eq 'copy' then begin 
 info = {row:0, startdis:custom_stc.startdis,stopdis:custom_stc.stopdis}
 widget_control, mk_cds_draw, draw_button = 0
 msg = ['Please press and drag the LEFT mouse button to move the plan entry',$
        'and click the RIGHT mouse button when done.']
 xtext,msg,/just_reg,wbase=tbase
 xshow,mk_cds_draw
 wid = [date_obs, time_obs,date_end,time_end]
 widget_control,mk_cds_base,sensitive=0
 mk_cds_window
 tplan = mk_plan_copy(hplan, info=info, new_row=new_row, err=err, wid = wid)
 widget_control,mk_cds_base,sensitive=1
 xkill,tbase
 widget_control, mk_cds_draw, draw_button = 1
 if err ne '' then return
 if tplan.(itime) gt custom_stc.stopdis or tplan.(itime+1) lt custom_stc.startdis then begin
  msg = 'You have to drop the plan entry inside the display window!'
  xack,msg,group=mk_cds_base,/icon
  return
 endif

;-- plan being moved outside the plan row

 if new_row ne 0 then begin
  msg = 'You have to drop the plan entry to the plan row!'
  xack,msg,group=mk_cds_base,/icon
  return
 endif

;-- add new plan or move it graphically

 plan = tplan 
 ctime=plan.(itime)
 if event.id eq gmov then mk_cds_move else mk_cds_add

endif

;-- Update DB with edited plans

if uvalue eq 'update' then mk_cds_update,/mess,/refresh

;-- REMOVE a plan entry 

if uvalue eq 'remove' then begin
 db_details=details
 mk_cds_save
 mk_plan_rem,hplan,details,err=err
 if err ne '' then goto,error_exit

 if use_gis then gset=plan.gset_id
 tmax1=get_plan_tt(plan.(itime),db_details,gset=gset)
 tmax2=get_plan_tt(plan.(itime),details,gset=gset)
 tmax=tmax1 > tmax2
 tcal=plan.(itime)

 xtext,mod_mess,wbase=tbase,/just_reg
 widget_control,mk_cds_base,/hour
 mk_plan_order,details,ndetails,custom_stc.startdis,custom_stc.stopdis,err=err,tgap=tgap,$
  tcal=tcal,tmax=tmax,group=mk_cds_base
 if err ne '' then goto,error_exit

 if exist(ndetails) then details=ndetails else delvarx,details
 mk_plan_load,type,details
 updated=0 & stored=0
 delvarx,hplan
 rem_mess='Plan entry removed successfully.'
 xtext,[mod_mess,rem_mess],/wait,/just_reg,wbase=tbase

 error_exit: xkill,tbase
 if err ne '' then begin
  if exist(db_details) then details=db_details
  xack,err,group=mk_cds_base
 endif else begin
  mk_cds_plot
  mk_cds_refresh
  mk_cds_sensit
 endelse

endif

;-- UNDO last operation

if uvalue eq 'undo' then mk_cds_undo

;-- switch plan entries

if uvalue eq 'swap' then begin
 xshow,mk_cds_draw
 xtext,'Use cursor to select plan entry to swap with highlighted plan.',$
  /no_find,/no_print,instruct='Cancel',$
  wbase=swap_base,ysize=10,space=1,group=mk_cds_base,/no_save
 return
endif 

;-- turn INSERT mode on

if uvalue eq 'insert_yes' then insert_on=event.select

;-- read KAP

if uvalue eq 'rkap' then begin
 mk_cds_update
 span=7
 xset_value,span,group=mk_cds_base,min=1,max=20,font=bfont,$
  instruct='Enter number of days to update starting from '+$
            anytim2utc(custom_stc.startdis,/date,/vms),status=status
 if not status then return
 temp=tai2utc(custom_stc.startdis)
 temp.time=0 & temp=utc2tai(temp)
 mess = 'Please wait. Checking latest KAP file...'
 xtext, mess, /just_reg, wbase=tbase, title='UPDATE_KAP'
 widget_control,/hour,mk_cds_base,sensitive=0
 update_kap, temp, ignore='CDS',status=status,span=fix(span),back=0
 widget_control,mk_cds_base,/sensitive
 xtext,[mess,'KAP update completed.'],wbase=tbase,/just_reg,wait=2
 xshow,mk_cds_base
 if status then mk_cds_reread
endif

;-- write IAP

if uvalue eq 'xiap' then begin
 mk_cds_update
 tstop=custom_stc.stopdis-secs_day
 if custom_stc.stopdis-custom_stc.startdis le secs_day then tstop=custom_stc.startdis ;-- at least a day
 xiap,custom_stc.startdis,tstop,group=mk_cds_base,/last
endif

;-- update SCIENCE plan

if uvalue eq 'upd_sci' then begin
 mk_cds_update

 dlook=mk_plan_find(cds_detail,custom_stc.startdis,custom_stc.stopdis,count=dcount)
 if (dcount eq 0) then begin
  xack,'No DETAILED plans are in the displayed interval',group=mk_cds_base
  return
 endif

 warn='This update will rewrite the SCIENCE plan database during the displayed interval'
 instruct='Proceed with SCIENCE plan update?'
 value=xanswer(warn,group=mk_cds_base,instruct=instruct,/suppress,/check)
 if value then begin
  if exist(cds_science) then old_science=cds_science
  slook=mk_plan_find(cds_science,custom_stc.startdis,custom_stc.stopdis,count=scount,$
                     rcount=rcount,rest=rest)
  if type eq 2 then mk_cds_save
  mk_cds_update
  upd_mess='Please wait. Updating SCIENCE database...'
  xtext,upd_mess,/hour,wbase=tbase,/just_reg
  if scount gt 0 then temp_science=cds_science(slook)
  dprint,'% # of science plans updated = ',scount

;-- only update SCI plans within display window

  upd_sci_plan,cds_detail(dlook),temp_science,err=err
  suc_mess='SCIENCE update completed.'
  if rcount gt 0 then begin
   cds_science=mk_plan_sort(concat_struct(cds_science(rest),temp_science),/uniq)
  endif else cds_science=temp_science
  xtext,[upd_mess,suc_mess],/wait,/just_reg,wbase=tbase
  err=''
  if err ne '' then begin
   xack,err,group=mk_cds_base
   if exist(old_science) then cds_science=old_science
  endif else begin
   if (type eq 2) then begin
    mk_plan_unload,type,details
    delvarx,hplan
    updated=1
   endif
   mk_cds_plot
   mk_cds_refresh
   mk_cds_sensit
  endelse
 endif
endif

;-- run XCPT

if uvalue eq 'xcpt' then begin
 mk_cds_update
 tstop=custom_stc.stopdis-secs_day
 if custom_stc.stopdis-custom_stc.startdis le secs_day then tstop=custom_stc.startdis+secs_day ;-- at least a day
 xcpt,custom_stc.startdis,custom_stc.stopdis,group=mk_cds_base,/modal,/last
 return
endif

xkill,tbase

return & end

;======================================================================

pro mk_cds_detach             ;-- create detachable window

@mk_cds_com
 
if not detached then return

widget_control,mk_cds_base,/realize, tlb_get_size=gsize, $
   tlb_get_offset=goff
cds_draw_base = widget_base(title=' ', $
 event_pro='mk_cds_redraw', $
 /tlb_size_events, yoff=goff(1)+gsize(1), xoff=goff(0))
mk_cds_draw = widget_draw(cds_draw_base, xsize=gsize(0), ysize=ysize, /button_event, $
                            retain=2,/motion)
widget_control, cds_draw_base, /realize
widget_control, mk_cds_draw, get_value=win_ed

return & end

;======================================================================

pro mk_cds_zone,zone_id     

;-- get correct ZONE_ID. First check SEL_STUDY.ZONE_ID, then check
;   PLAN.POINTINGS(0).ZONE_ID for planner updated value.

@mk_cds_com 

if datatype(sel_study) ne 'STC' then return
zone_id=sel_study.zone_id

ok=where(type eq [0,1,3],cnt)
if cnt eq 0 then return

if (sel_study.var_point eq 'Y') and (plan.n_pointings gt 0) then begin
 if tag_exist(plan,'pointings') then begin
  tzone=plan.pointings(0).zone_id
  if tzone gt 0 then zone_id=tzone
 endif
endif

return & end

;======================================================================

pro mk_cds_special    

;-- hardwire special studies (SYNOP, NIMCP, GIMCP) with commonly
;   used parameter fields.

@mk_cds_com

;-- only do it if not inheriting from SCI_PLAN

if (get_sci) or (type eq 2) then return

zone_id=sel_study.zone_id
if (zone_id ge 1) and (zone_id le 6) then plan.object=zobjects(zone_id-1)

if is_eng then begin
 plan.sci_obj=strmid(trim(sel_study.title),0,50)
 plan.sci_spec=strmid(trim(sel_study.sv_desc),0,50)
 plan.cmp_no=0
 plan.object='QS'
endif

if strpos(strupcase(sel_study.title),'ANYTIME') gt -1 then begin
 plan.sci_obj='QCM Logging'
 plan.object='QS'
 plan.cmp_no=0
endif

if strpos(strupcase(sel_study.obs_prog),'SYNOP') gt -1 then begin
 plan.sci_obj='Synoptic Meridian Images'
 plan.object='SYN'
 plan.cmp_no=1
endif

if strpos(strupcase(sel_study.obs_prog),'GIMCP') gt -1 then begin
 plan.sci_obj='GIS MCP Engineering'
 plan.object='QS'
 plan.cmp_no=0
endif

if strpos(strupcase(sel_study.obs_prog),'NIMCP') gt -1 then begin
 plan.sci_obj='NIS MCP Engineering'
 plan.object='QS'
 plan.cmp_no=0
endif

if strpos(strupcase(sel_study.obs_prog),'CCD') gt -1 then begin
 plan.sci_obj='CCD Engineering'
 plan.object='QS'
 plan.cmp_no=0
endif


return & end

;======================================================================

pro mk_cds_read,force=force,no_type=no_type,check=check

;-- check STARTDIS
 
if keyword_set(check) then mk_cds_startdis

;-- read databases

@mk_cds_com

if not exist(no_type) then no_type=-1
chk_mess = 'Please wait. Reading plan databases...'                
xtext, chk_mess, wbase=tbase, /just_reg
widget_control, /hour
mk_plan_read,custom_stc,force=force,/no_tape,no_type=no_type
delvarx,sdb_details
if type ne no_type then begin
 mk_plan_unload,type,details
 mk_cds_last
endif
xkill,tbase
return & end

;======================================================================

pro mk_cds_last

;-- save last unedited copy of details

@mk_cds_com

if exist(details) then last_details=details else delvarx,last_details

return & end

;======================================================================

pro mk_cds_reset

;-- reset plan fields

@mk_cds_com

delvarx,details,rplan,sobj_list,spec_list 
sdb_empty=0
def_inst_plan,plan,type=type,/init
delvarx,hplan
def_cds_study,sel_study,/rasters
get_study_par,study=sel_study,reps=reps,gis=use_gis,slits=slits,dets=dets,$
 eng=is_eng,err=err 
if err ne '' then message,err,/cont
rindex=0 & pindex=0 

return & end

;======================================================================

pro mk_cds_reread    

;-- reread databases

@mk_cds_com
mk_plan_reset
mk_cds_read,/force,/check
mk_cds_hplan
mk_cds_plot
mk_cds_refresh
mk_cds_sensit
updated=1 
rm_file,mk_plan_crash

return & end

;======================================================================

pro mk_cds_undo  

;-- undo most recent operation

@mk_cds_com

undo=1
if not exist(sdb_details) then begin
 if sdb_empty eq 0 then begin
  xtext,'No operation to undo',/just_reg,wait=2
  undo=0
 endif
endif

if undo then begin
 undo_mess='Please wait. Undoing last operation...  '
 xtext,undo_mess,wbase=tbase,/just_reg
 if exist(sdb_details) then details=sdb_details else delvarx,details
 mk_plan_load,type,details
 updated=0 & stored=0
 if exist(s_hplan) then hplan=s_hplan else delvarx,hplan
 if exist(hplan) then plan=hplan
 xkill,tbase
 mk_cds_save
endif

mk_cds_plot
mk_cds_refresh
mk_cds_sensit

return & end

;======================================================================

pro mk_cds_hplan

;-- recover highlighted plan from current cursor position

@mk_cds_com


if exist(ctime) then begin
 index=mk_plan_where(ctime,details,status=in_db)
 if in_db then begin
  hplan=details(index) 
  if not new_study then plan=hplan
  if type gt 1 then etime=hplan.(itime+1)
 endif
endif

return & end

;======================================================================

pro mk_cds_cmp,select=select,status=status,force=force

;-- prompt for CMP_NO

@mk_cds_com

cmp=plan.cmp_no
title='Please select a Campaign number'
xcamp,cmp,/last,tstart=custom_stc.startdis,tend=custom_stc.stopdis,$
         group=mk_cds_base,err=err,status=status,title=title,$
         select=select,force=force
if status then begin
 plan.cmp_no=cmp
 widget_control,cmp_no,set_value=trim(num2str(plan.cmp_no))
endif

return & end

;======================================================================

pro mk_cds_field,status=status

;-- check for mandatory fields (SCI_OBJ, CMP_NO, OBJ_ID, GSET_ID)

@mk_cds_com

;-- check SCI_PLAN first

if get_sci then plan=get_plan_sci(plan)

;-- SCI_OBJ entered

if trim(plan.sci_obj) eq '' then begin
 instruct='A Science Objective is required'
 mk_cds_sci,status=status,instruct=instruct
 if not status then return
endif

;-- CAMPAIGN NO entered

status=1
if (plan.cmp_no lt 0) then begin
 mk_cds_cmp,/force,status=status
 if not status then return
endif else begin
 mk_plan_camp,plan,err=err,day=1
 if err ne '' then begin
  val=xanswer(err,instruct='Continue this editing operation?',group=mk_cds_base,/suppress,/check)
  if val eq 0 then begin
   status=0
   return
  endif
 endif
endelse

;-- GSET entered

if use_gis then begin
 if plan.gset_id lt 1 then begin
  mk_cds_gis,status=status
  if not status then return
 endif
endif

;-- OBJECT entered (optional)

if (trim(plan.object) eq '') then begin
 instruct='Select an OBJECT type'
 mk_cds_obj,status=status,instruct=instruct
 if not status then return
endif

return & end

;======================================================================

pro mk_cds_gis,status=status

;-- prompt for GSET_ID

@mk_cds_com

if not exist(fil_id) then fil_id=0
widget_control,/hour
list_gset,fil_id,gsets,n_found,ffb=ffb 

;-- filter out SLIT_NUM and ZONE_ID

gis_avail=0
mk_cds_zone,zone_id
if n_found gt 0 then begin
 if exist(slits) and exist(dets) then begin 
  gis_det=where(dets eq 'G',cnt)
  gis_avail=(cnt gt 0)
  if gis_avail then begin
   gis_slits=slits(gis_det)
   gis_slits=gis_slits(uniq([gis_slits],sort([gis_slits])))
   dprint,'% gis_slits ',gis_slits
   if n_elements(gis_slits) gt 1 then $
    message,'study has more than one slit #: '+arr2str(gis_slits),/cont
   ok=where(gsets.zone_id eq zone_id,cnt)
   gis_avail=(cnt gt 0)
   if gis_avail then begin
    gsets=gsets(ok)
    ok=where_vector(gis_slits,gsets.slit_num,cnt)
    gis_avail=(cnt gt 0)
    if gis_avail then gsets=gsets(ok)
   endif
  endif
 endif
endif

;-- next filter out DET_USED

if gis_avail then begin
 delvarx,ok
 gis_dets=get_cds_gis(sel_study)
 ndets=n_elements(gis_dets)
 for i=0,n_elements(gsets)-1 do begin
  err=''
  det_used=get_gis_det(gsets(i).gset_id,err=err)
  if err eq '' then begin
   cross_check=where( (gis_dets eq det_used) eq 1,count)
   if count eq ndets then if not exist(ok) then ok=i else ok=[ok,i]
  endif
 endfor
 if exist(ok) then gsets=gsets(ok) else gis_avail=0 
endif

if gis_avail then begin
 gtags=['GSET_ID','SLIT_NUM','ZONE_ID','FIL_ID','FFB','DET_USED','GSET_DESC']
 gtitle="Select from the following GSET ID's"
 s=sort([gsets.gset_id])
 gsets=gsets(reverse([s]))
 desc=gsets.gset_desc
 desc(1:3,*)=''
 gsets.gset_desc=desc
 xlist,gsets,index,group=mk_cds_base,/modal,/sel,tags=gtags,title=gtitle,/center
 if index eq -1 then return
 plan.gset_id=gsets(index).gset_id
 temp=gsets(index).gset_desc(0)
 star=strpos(temp,'*')
 star=star(0)
 if star eq 0 then begin
  xack,['WARNING: selected GSET ID '+trim(plan.gset_id)+' is no longer current.','Use at own risk.'],$
       group=mk_cds_base
 endif
 widget_control,gset_id,set_value=trim(string(plan.gset_id,'(i4)'))
 status=1

;-- update OBJECT

endif else begin
 err=["No GSET ID's available for: ",' ']
 if exist(fil_id) then err=[err,'Filament ID: '+trim(string(fil_id,'(i2)'))]
 if exist(gis_slits) then err=[err,'Slit_Num:    '+arr2str(gis_slits,delim=' ',/trim)]
 if exist(zone_id) then err=[err,'Zone ID:     '+trim(string(zone_id,'(i2)'))]
 err=[err,'','Suggest you try a different ZONE_ID']
 xack,err,group=mk_cds_base,/icon
 status=0
endelse

return & end

;======================================================================

pro mk_cds_obj,status=status,instruct=instruct

;-- prompt for OBJ_ID

@mk_cds_com

status=1

if not exist(objects) then begin
 list_object,objects,nobj
 if nobj eq 0 then delvarx,objects else begin
  top=['QS','AR','CH','LMB','SYN','COR']
  tobj=where_vector(top,trim(objects.object),rest=rest)
  objects=objects([tobj,rest])
 endelse
endif

nobj=n_elements(objects)
if nobj eq 0 then begin
 xack,'No Objects in DB',group=mk_cds_base
endif else begin
 if exist(instruct) then title=instruct
 xlist,objects,index,/select,group=mk_cds_base,/modal,/center,title=title
 if index gt -1 then begin
  sobject=trim(objects(index).(1))
  plan.object=trim(objects(index).(0))
  widget_control,object,set_value=plan.object
 endif
endelse

return & end

;======================================================================

pro mk_cds_rule,status=status

;-- check that 5% rule is obeyed

@mk_cds_com

status=1
if not rule_on then return
if type ne 0 then return
if not plan.time_tagged then return

oh_time=get_cds_oh(ctime,details,status=status,tgap=tgap)

if not status then begin
 wmess=['                 --  5% rule violated --                         ',$
        '',$
       'New time-tagged entry may interrupt previous entry. ',$
       '',$
       'Start time should be greater than: '+anytim2utc(oh_time,/vms)]
 options=['Accept New Start Time','Ignore warning','Abort']
 val=xchoice(wmess,options,group=mk_cds_base,/row)
 if val eq 0 then begin
  ctime=oh_time & status=1
 endif
 if val eq 1 then status=1
endif

return & end

;======================================================================

pro mk_cds_insert

@mk_cds_com

clook=mk_plan_where(ctime,details,status=in_db)
if in_db then begin
 iplan=details(clook(0))
 omess=['Added entry will overlap an existing entry: ',$
        ' ',$
        'STARTING AT:       '+anytim2utc(iplan.(itime),/ecs),$
        'SCIENCE OBJECTIVE: '+trim(iplan.sci_obj)]

 if type ne 2 then begin
  istudy=get_cds_study(iplan.study_id,iplan.studyvar,err=err)
  if err ne '' then message,err,/cont
  acronym='ACRONYM:           '+trim(istudy.obs_prog)
  omess=[omess,acronym]
 endif

 omess=[omess,'','What do you wish to do?']
 options=['Replace existing entry','Interrupt existing entry with new entry',$
          'Append new entry to existing entry',$
          'Abort this operation']

 choice=xchoice(omess,options,group=mk_cds_base)
 case choice of
  0:  begin
       hplan=iplan
       mk_cds_move,/rep,/nowarn
      end
  1:  mk_cds_add
  2:  mk_cds_add,/app
 else: mk_cds_refresh
 endcase

endif else mk_cds_add

return & end

;======================================================================

pro mk_cds_add,append=append,err=err

;-- ADD plan entry to memory copy of PLAN

@mk_cds_com
err=''

if not exist(ctime) then begin
 err='Plan start time not entered'
 goto, error_exit
endif

;-- add at CTIME or append

append=keyword_set(append)
if (type eq 0) then plan.time_tagged=1-append

if append then begin
 aindex=mk_plan_where(ctime,details,status=in_db)
 atime=custom_stc.startdis
 if type eq 0 then agap=tgap/2. else agap=tgap
 if aindex gt -1 then atime=(details(aindex).(itime+1)+agap) > custom_stc.startdis 
 dprint,'% appending entry...'
endif else begin
 mk_cds_rule,status=status
 if not status then return
 atime=ctime
endelse

plan.(itime)=atime

;-- check for mandatory fields

mk_cds_field,status=status
if not status then return

widget_control,pduration,get_value=dur
dur=dur_unit*float(trim(dur(0))) > 0.
plan.(itime+1)=atime+dur
if (type eq 1) and exist(etime) then plan.(itime+1)=etime

;-- save local copy

if exist(details) then db_details=details
mk_cds_save

if plan.(itime) eq custom_stc.startdis then append=0

mk_plan_add,plan,details,err=err,insert=insert_on,tgap=tgap
if err ne '' then goto,error_exit

xtext,mod_mess,wbase=tbase,/just_reg
widget_control,mk_cds_base,/hour
tcal=plan.(itime)
if use_gis then gset=plan.gset_id
tmax1=get_plan_tt(plan.(itime),db_details,gset=gset)
tmax2=get_plan_tt(plan.(itime),details,gset=gset)
tmax=(tmax1 > tmax2)

mk_plan_order,details,ndetails,custom_stc.startdis,custom_stc.stopdis,err=err,tgap=tgap,$
        tcal=tcal,tmax=tmax,group=mk_cds_base
if err ne '' then goto,error_exit

if exist(ndetails) then details=ndetails else delvarx,details
mk_plan_load,type,details
updated=0 & stored=0

delvarx,hplan
nindex=mk_plan_where(plan,details,status=in_db)
if not in_db then if exist(aindex) then nindex=aindex+1
hplan=details(nindex) & plan=hplan

xtext,[mod_mess,succ_mess],/wait,/just_reg,wbase=tbase
ctime=hplan.(itime+1)
delvarx,etime,last_etime

error_exit: xkill,tbase

if err ne '' then begin
 if exist(db_details) then details=db_details
 xack,err,group=mk_cds_base
endif else begin
 new_study=0
 mk_cds_plot
 mk_cds_refresh
 mk_cds_sensit
endelse

return & end

;======================================================================

pro mk_cds_startdis,change=change

@mk_cds_com

old_startdis=custom_stc.startdis
widget_control,disp_obs,get_value=new_date
new_date=trim(new_date(0))
err='' & new_startdis=utc2tai(anytim2utc(new_date,err=err))
if err ne '' then begin
 xack,+'DISPLAY START field: '+err,group=mk_cds_base
 custom_stc.startdis=old_startdis
endif else custom_stc.startdis=new_startdis

widget_control,disp_obs,set_value=tai2utc(custom_stc.startdis,/ecs,/vms)
change=old_startdis ne custom_stc.startdis

return & end

;======================================================================

pro mk_cds_start,time_change=time_change

;-- check display start time

@mk_cds_com

cdur=(custom_stc.stopdis-custom_stc.startdis)
mk_cds_startdis,change=time_change

if time_change then begin
 custom_stc.stopdis=custom_stc.startdis+cdur
 mk_cds_update
 mk_cds_read
 mk_cds_plot,/time_change
 mk_cds_button
endif

return & end

;======================================================================

pro mk_cds_check,event 


;-- check text widgets in case user forgot to hit return

@mk_cds_com

if not db_access then return

recal_point=0 
recal_rep=0

;-- # of raster repeats

if (type ne 2) then begin
 widget_control,n_rasters1,get_value=tvalue
 wvalue=fix(tvalue(0))
 if wvalue ne float(tvalue(0)) then widget_control,n_rasters1,set_value=trim(num2str(wvalue))
 if not is_eng then begin
  if wvalue le 0 then begin
   xack,['You must have at least one repeat of the last raster.',$
         'Number of repeats will be set to 1.'],group=mk_cds_base
   wvalue=1
   widget_control,n_rasters1,set_value='1'
  endif
 endif else begin
  if wvalue ne 0 then begin
   xack,['Engineering studies cannot have repeated rasters.',$
         'Number of repeats will be set to 0.'],group=mk_cds_base
   wvalue=0
   widget_control,n_rasters1,set_value='0'
  endif
 endelse
 if wvalue ne plan.n_rasters1 then begin
  dprint,'% n_rasters changed, old,new:',plan.n_rasters1,wvalue
  plan.n_rasters1=wvalue
  recal_rep=1
 endif

;-- Deferred pointing

 if (sel_study.var_point eq 'Y') then begin

;-- for non-ENG studies, check individual rasters for
;   deferred pointing modes

  if (not is_eng) then begin
   pointing=sel_study.rasters.pointing
   cur_point=pointing(rindex)
   def_point=(cur_point eq 1)
  endif else begin
   def_point=(plan.n_pointings eq 1)
  endelse 

  if def_point and (plan.n_pointings gt 0) then begin

   widget_control,ins_x,get_value=xp
   if trim(xp(0)) eq '' then xp=0
   xp=float(str_format(xp(0),'(f8.2)'))
   cur_xp=plan.pointings(pindex).ins_x
   new_xpoint=(cur_xp ne xp)
   if new_xpoint then dprint,cur_xp,xp

   widget_control,ins_y,get_value=yp
   if trim(yp(0)) eq '' then yp=0
   yp=float(str_format(yp(0),'(f8.2)'))
   cur_yp=plan.pointings(pindex).ins_y
   new_ypoint=(cur_yp ne yp)
   if new_ypoint then dprint,cur_yp,yp

   if (new_xpoint) or (new_ypoint) then begin
    err=''
    valid=valid_cds_point(xp,yp,err=err)
    if not valid then begin

     case 1 of
      (new_xpoint) and (not new_ypoint): begin
       xack,['X coordinate outside allowable pointing limit.',err],$
        group=mk_cds_base,/icon
       xp=cur_xp
      end

      (new_ypoint) and (not new_xpoint): begin
       xack,['Y coordinate outside allowable pointing limit.',err],$
        group=mk_cds_base,/icon
       yp=cur_yp
      end

     else: begin
      xack,['(X-Y) coordinates outside allowable pointing limits.',err],$
       group=mk_cds_base,/icon
       xp=cur_xp & yp=cur_yp
      end
     endcase

    endif else begin
     plan.pointings(pindex).ins_x=xp
     plan.pointings(pindex).ins_y=yp
     dprint,'% pointing coords changed'
     recal_point=1
    endelse

   endif
   widget_control,ins_x,set_value=string(xp,'(f8.2)')
   widget_control,ins_y,set_value=string(yp,'(f8.2)')
  endif

;-- # of pointings changed?

  if (type ne 1) and (not is_eng) then begin
   widget_control,n_pointings,get_value=tpoint
   npoint=fix(tpoint(0))
   if npoint ne float(tpoint(0)) then widget_control,n_pointings,set_value=trim(num2str(npoint))
   if (npoint le 0) then begin
    xack,['You must have at least one pointing for a variable pointing study.',$
           'Number of pointings will be set to 1.'],group=mk_cds_base
    npoint=1
    widget_control,n_pointings,set_value='1'
   endif

   if (plan.n_pointings ne npoint) then begin
    dprint,'% pointing changed, old, new:',plan.n_pointings,npoint
    cur_pnt=(plan.n_pointings > 1)
    cur_point=plan.pointings(0:cur_pnt-1)
    final_point=plan.pointings(cur_pnt-1)

;-- extend or shorten pointing structure to new size
    
    if (npoint gt cur_pnt) then begin
;     clear_point=clear_struct(cur_point(0))
     nextra=npoint-cur_pnt
     extra_point=replicate(final_point,nextra)
     new_point=concat_struct(cur_point,extra_point)
    endif else new_point=cur_point(0:npoint-1)

    plan=rep_tag_value(plan,new_point,'POINTINGS')
    dprint,'% n_pointings changed, old,new:',plan.n_pointings,npoint

    plan.n_pointings=npoint
    recal_point=1
    widget_control,pslider,set_slider_min=0,set_slider_max=1,sensitive=0
    pindex=0
    if (npoint gt 1) then begin
     widget_control,pslider,set_slider_min=0,set_slider_max=npoint-1,/sens
     pindex= 0 > (pindex < (npoint-1))
     widget_control,pslider,set_value=pindex
    endif
    mk_cds_point
   endif
  endif 
 endif
endif

;-- # of study repeats

if (type eq 0) or (type eq 3) then begin
 widget_control,n_repeat_s,get_value=tvalue
 wvalue=fix(tvalue(0))
 if wvalue ne float(tvalue(0)) then widget_control,n_repeat_s,set_value=trim(num2str(wvalue))
 if wvalue le 0 then begin
  xack,['You cannot have less than zero repeats of a study.',$
        'Number of repeats will be set to 1.'],group=mk_cds_base
  wvalue=1
  widget_control,n_repeat_s,set_value='1'
 endif

;-- check if number of study repeats can be made equivalent to repeating
;   last raster. This works only if one raster and pointing is being used.

 if (wvalue gt 1) and (sel_study.n_raster_def eq 1) and (plan.n_pointings eq 1) then begin
  curr_rep=plan.n_rasters1
  new_rep=wvalue*curr_rep
  rmess='Plan entries with single rasters and pointings can be repeated by repeating the last raster.'
  rmess=[rmess,'Last raster will be repeated: '+$
         trim(string(wvalue))+' x '+trim(string(curr_rep))+' = '+$
         trim(string(new_rep))+' times.']
  xack,rmess,group=mk_cds_base
  plan.n_repeat_s=1
  plan.n_rasters1=new_rep
  widget_control,n_rasters1,set_value=trim(num2str(new_rep))
  widget_control,n_repeat_s,set_value='1'
  recal_rep=1
  wvalue=1
 endif

 if wvalue ne plan.n_repeat_s then begin
  dprint,'% n_repeat_s changed, old,new:',plan.n_repeat_s,wvalue
  plan.n_repeat_s=wvalue
  recal_rep=1
 endif
endif

if recal_point or recal_rep then begin
 mk_cds_sdur
 widget_control,date_end,set_value=tai2utc(plan.(itime+1),/ecs,/date,/vms)
 widget_control,time_end,set_value=tai2utc(plan.(itime+1),/ecs,/time)
endif

;-- check CUR_ and END_ time/duration widgets

recal_time=0
if (event.id ne mk_cds_draw) and (event.id ne pduration) then begin
 
;-- cursor START time 

 day=tai2utc(custom_stc.startdis) &  day.time=0 & day=utc2tai(day)
 widget_control,date_cur,get_value=dcur
 widget_control,time_cur,get_value=tcur
 err=''
 temp=utc2tai(trim(dcur(0))+' '+trim(tcur(0)),err=err)
 if err ne '' then begin
  xack,'CUR_TIME field: '+err,group=mk_cds_base
  if exist(ctime) then begin
   widget_control,date_cur,set_value=tai2utc(ctime,/ecs,/date,/vms)
   widget_control,time_cur,set_value=tai2utc(ctime,/ecs,/time)
  endif
 endif else begin
  if type eq 1 then begin
   do_nothing=1
   mk_cds_flag,err=err
   if err ne '' then return
  endif else begin
   do_it= (1- exist(ctime))
   if exist(ctime) then do_it=(temp ne ctime)
   if do_it then begin
    ctime=temp
    widget_control,date_cur,set_value=tai2utc(temp,/ecs,/date,/vms)
    widget_control,time_cur,set_value=tai2utc(temp,/ecs,/time)
    mk_cds_erase,last_cpos
    x2=(ctime-day)/3600.d
    oplot,[x2,x2],[.02,.98],linestyle=1,color=2*(1-nocolor)
    last_cpos=x2
    last_ctime=ctime
    recal_time=1
   endif
  endelse
 endelse

;-- END time affects SCIENCE only

 mk_cds_dtime

endif

if recal_point or recal_time then mk_cds_sensit

;-- SCIENCE plan pointing

if type eq 2 then begin
 widget_control,xcen,get_value=xp
 xp=xp(0)
 widget_control,ycen,get_value=yp
 yp=yp(0)
 if (xp ne plan.xcen) or (yp ne plan.ycen) then begin
  recal_point=1
  plan.xcen=xp
  plan.ycen=yp
  plan=mk_plan_xy(plan,'(i8)')
  widget_control,xcen,set_value=plan.xcen
  widget_control,ycen,set_value=plan.ycen
 endif
endif

;-- update IMAGE_TOOL?

if recal_point and itool_point then mk_cds_send

return & end

;======================================================================

pro mk_cds_dtime

;-- check for valid END time widget input

@mk_cds_com
if (type gt 1) then begin
 widget_control,date_end,get_value=dend
 widget_control,time_end,get_value=tend
 err=''
 temp=utc2tai(trim(dend(0))+' '+trim(tend(0)),err=err)
 if err ne '' then begin
  xack,'STOP_TIME field: '+err,group=mk_cds_base
  if exist(etime) then begin
   widget_control,date_end,set_value=tai2utc(etime,/ecs,/date,/vms)
   widget_control,time_end,set_value=tai2utc(etime,/ecs,/time)
  endif
 endif else begin
  do_it= (1-exist(etime))
  if exist(etime) then do_it=(temp ne etime) and (ctime gt temp)
  if do_it then begin
   etime=temp
   dprint,'% changing PDUR'
   mk_cds_pdur
   mk_cds_etime
  endif
 endelse
endif

return & end

;======================================================================

pro mk_cds_sdur,quiet=quiet

;-- update study duration widget

@mk_cds_com

if type eq 2 then return 
pmess='Please wait. Computing pointing delays...'
if not keyword_set(quiet) then xtext,pmess,wbase=tbase,/just_reg
widget_control,/hour
dur=get_cds_dur(plan,point=(type eq 0))
widget_control,sduration,set_value=string(dur/dur_unit,'(f12.2)')

if type eq 0 then begin
 plan.(itime+1)=plan.(itime)+dur
 widget_control,pduration,set_value=string(dur/dur_unit,'(f12.2)')
endif

xkill,tbase
return & end

;======================================================================

pro mk_cds_pdur  

;-- update plan duration widget

@mk_cds_com

if (type gt 1) and exist(ctime) and exist(etime) then begin
 check=mk_plan_where(ctime,details,status=in_db)
 if in_db then begin
  if etime gt plan.(itime) then cur_dur=etime-plan.(itime) 
 endif else begin
  if etime gt ctime then cur_dur=etime-ctime
 endelse
 if exist(cur_dur) then begin
  cur_dur=cur_dur > 0.
  widget_control,pduration,set_value=string(cur_dur/dur_unit,'(f12.2)')
 endif
 plan.(itime+1)=etime
endif

return & end

;======================================================================

pro mk_cds_etime  

;-- update END time widget

@mk_cds_com

if not exist(etime) then return
day=tai2utc(custom_stc.startdis) &  day.time=0 & day=utc2tai(day)
widget_control,date_end,set_value=tai2utc(etime,/ecs,/date,/vms)
widget_control,time_end,set_value=tai2utc(etime,/ecs,/time)
mk_cds_erase,last_epos
x2=(etime-day)/3600.d
oplot,[x2,x2],[.02,.98],linestyle=2,color=2*(1-nocolor)
last_epos=x2
last_etime=etime

return & end

;======================================================================

pro mk_cds_move,replace=replace,err=err,nowarn=nowarn,noplot=noplot,$
                quiet=quiet,use_ctime=use_ctime,status=status


;-- replace/move plan entry (replace is a MOVE without changing start time)

@mk_cds_com

err=''
status=0
loud=1-keyword_set(quiet)

;-- find location of old plan in DB

index=mk_plan_where(hplan,details,status=in_db)
if not in_db then begin
 err='Entry not in DB'
 goto, error_exit
endif

;-- check for mandatory fields

mk_cds_field,status=status
if not status then return

if exist(details) then db_details=details

if not exist(hplan) or not exist(plan) then begin
 err='Cannot find plan entry to replace'
 goto,error_exit
endif

replace=keyword_set(replace)

;-- check if user wants to replace existing entry (keeping same time)

if replace then begin
 plan.(itime)=hplan.(itime) 
 same=mk_plan_comp(plan,hplan)
 if same then begin
  mk_cds_refresh
  same_mess='New plan entry same as existing. Not replaced.'
  xtext,same_mess,/just_reg,/wait
  status=0
  return
 endif
 if not keyword_set(nowarn) then begin
  temp=trim(hplan.sci_obj)
  rmess=['Modified plan will replace currently highlighted plan: ',$
         ' ',$
         'STARTING AT:       '+anytim2utc(hplan.(itime),/ecs),$
         'SCIENCE OBJECTIVE: '+temp]
  if type ne 2 then begin
   old_study=get_cds_study(hplan.study_id,hplan.studyvar,err=err)
   if err ne '' then message,err,/cont
   acronym='ACRONYM:           '+trim(old_study.obs_prog)
   rmess=[rmess,acronym]
  endif
  value=xanswer(rmess,group=mk_cds_base,instruct='Proceed with replace?',/suppress,/check)
  if not value then begin
   mk_cds_refresh
   return
  endif
 endif
endif else begin
 plan.(itime)=ctime
 mk_cds_rule,status=status
 if not status then return
 plan.(itime)=ctime
 widget_control,pduration,get_value=dur
 dur=dur_unit*float(trim(dur(0))) > 0.
 plan.(itime+1)=ctime+dur
 if (type eq 1) and exist(etime) then plan.(itime+1)=etime
endelse

;-- save copy of DETAILS DB

mk_cds_save

;-- move and/or replace with modified entry 

zplan=details(index)
mk_plan_rem,zplan,details,err=err
if err ne '' then goto,error_exit

mk_plan_add,plan,details,err=err,insert=insert_on,tgap=tgap
if err ne '' then goto,error_exit

if type eq 0 then begin
 tmax1=get_plan_tt(zplan.(itime),db_details,gset=zplan.gset_id)
 tmax2=get_plan_tt(plan.(itime),db_details,gset=plan.gset_id)
 tmax3=get_plan_tt(zplan.(itime),details,gset=zplan.gset_id)
 tmax4=get_plan_tt(plan.(itime),details,gset=plan.gset_id)
 tmax=max([tmax1,tmax2,tmax3,tmax4])
endif

if loud then xtext,mod_mess,wbase=tbase,/just_reg
widget_control,/hour
tcal=zplan.(itime) < plan.(itime)
err=''
mk_plan_order,details,ndetails,custom_stc.startdis,custom_stc.stopdis,tgap=tgap,tcal=tcal,$
              err=err,tmax=tmax,group=mk_cds_base
if err ne '' then goto,error_exit

if exist(ndetails) then details=ndetails else delvarx,details
mk_plan_load,type,details
updated=0 & stored=0

;-- replot plan
 
delvarx,hplan
use_ctime=keyword_set(use_ctime)
if use_ctime and exist(ctime) then htime=ctime else htime=plan
index=mk_plan_where(htime,details,status=in_db)
if in_db then begin
 dprint,'% MK_CDS_MOVE: found HPLAN'
 hplan=details(index) & plan=hplan
endif
if replace then rep_mess='Plan entry replaced successfully.' else $
 rep_mess='Plan entry moved successfully.'
if loud then xtext,[mod_mess,rep_mess],/wait,/just_reg,wbase=tbase
status=1
error_exit: xkill,tbase

if err ne '' then begin
 if exist(db_details) then details=db_details
 xack,err,group=mk_cds_base
endif else begin
 new_study=0
 if not keyword_set(noplot) then begin
  mk_cds_plot
  mk_cds_refresh
  mk_cds_sensit
 endif
endelse

return & end

;======================================================================

pro mk_cds_sci,status=status,instruct=instruct

;-- prompt for SCI_OBJ

@mk_cds_com

status=0
if not exist(sobj_list) then sobj_list=''
if exist(details) then sobj_list=[details.sci_obj,sobj_list]

if exist(sobj_list) then begin
 sobj_list=trim(sobj_list)
 sobj_list=get_uniq_list(sobj_list)
 no_blanks=where(sobj_list ne '',cnt)
 if cnt gt 0 then sobj_list=sobj_list(no_blanks) else delvarx,sobj_list
endif

if exist(sobj_list) then begin
 text = trim(plan.sci_obj)

;  if (type ne 2) then text=sel_study.title 

 if datatype(instruct) eq 'STR' then subtitle=instruct else $
  subtitle='Select or Enter a Science Objective'
 temp = xsel_list(sobj_list, group = mk_cds_base, subtitle =subtitle, $
                    title = 'Recent Science Objectives', $
                    initial =text, /update, status = status)
 temp=strmid(trim(temp),0,50)
endif else begin
 temp=plan.sci_obj
 xinput,temp,'Enter Science Objective [50 char max]',group=mk_cds_base,/noblank,$
  /modal,max_len=50,status=status,title='A Science Objective is required'
 temp=strmid(trim(temp),0,50)
 if status then sobj_list=temp
endelse

if status then begin
 plan.sci_obj = temp
 widget_control,sci_obj,set_value=plan.sci_obj
endif

return & end

;======================================================================

pro mk_cds_save 

;-- save latest copy of DETAIL for later undoing

@mk_cds_com

if exist(details) then begin
 sdb_details=details
 sdb_empty=0
endif else begin
 sdb_empty=1 & delvarx,sdb_details 
endelse
if exist(hplan) then s_hplan=hplan else delvarx,s_hplan

return & end

;======================================================================

pro mk_cds_swap,x,event

;-- cursor handler for SWAP mode

@mk_cds_com

if (event.press eq 0) or (event.release eq 1) then return

if not exist(hplan) then return

day = tai2utc(custom_stc.startdis) & day.time=0 & day = utc2tai(day)
wtime=double(day+x*3600.d)

;-- check if cursor is in DETAILS 

windex=mk_plan_where(wtime,details,status=in_db)
if (not in_db) then return

;-- only swap different plans

swap_plan=details(windex)

if mk_plan_comp(swap_plan,hplan,/end_time) then begin
 xtext,'Identical entry. No swap performed.',/just_reg,$
        wbase=swap_base,/app,space=0
 return
endif

oplot_splan,swap_plan,custom_stc.startdis,$
 color=6*(1-nocolor),spec=(type ne 2),window=win_ed


pmess='Please wait. Performing swap...'
xtext,pmess,/just_reg,/hour,wbase=swap_base,/app,space=1

;-- switch start times and preserve time-tags

p1=hplan
if type eq 0 then odur=p1.orig_dur else odur=p1.(itime+1)-p1.(itime)
p1.(itime)=swap_plan.(itime)
p1.(itime+1)=swap_plan.(itime+1)
p2=swap_plan
if type eq 0 then odur=p2.orig_dur else odur=p2.(itime+1)-p2.(itime)
p2.(itime)=hplan.(itime)
p2.(itime+1)=hplan.(itime+1)

if tag_exist(hplan,'TIME_TAGGED') then begin
 p1.time_tagged=swap_plan.time_tagged
 p2.time_tagged=hplan.time_tagged
endif

;-- now do the swap

if exist(details) then db_details=details
mk_cds_save
hindex=mk_plan_where(hplan,details,status=in_db)
tcal=hplan.(itime) < swap_plan.(itime)
details(hindex)=p2
details(windex)=p1

;-- take care of GSET_IDs

if type eq 0 then begin
 tmax1=get_plan_tt(hplan.(itime),db_details,gset=hplan.gset_id)
 tmax2=get_plan_tt(swap_plan.(itime),db_details,gset=swap_plan.gset_id)
 tmax3=get_plan_tt(hplan.(itime),details,gset=hplan.gset_id)
 tmax4=get_plan_tt(swap_plan.(itime),details,gset=swap_plan.gset_id)
 tmax=max([tmax1,tmax2,tmax3,tmax4])
endif 

mk_plan_order,details,ndetails,custom_stc.startdis,custom_stc.stopdis,err=err,tgap=tgap,$
 tcal=tcal,group=mk_cds_base,tmax=tmax

if err eq '' then begin
 updated=0 & stored=0 & delvarx,hplan
 if exist(ndetails) then details=ndetails else delvarx,details
 mk_plan_load,type,details
 xtext,['Swap completed successfully.'],/wait,/just_reg,wbase=swap_base,/app,space=1
endif else begin
 xack,err,group=mk_cds_base
 if exist(db_details) then details=db_details
endelse

xkill,swap_base
mk_cds_hplan
mk_cds_plot
mk_cds_refresh
mk_cds_sensit

return & end

;======================================================================

pro mk_cds_zoom,x,event

;-- cursor handler for ZOOM widget

@mk_cds_com

if (event.press eq 0) or (event.release eq 1) then return

day = tai2utc(custom_stc.startdis) & day.time=0 & day = utc2tai(day)

white=!d.table_size-1
if event.press eq 1 then begin
 mk_cds_erase,last_lpos
 ltime=double(day+x*3600.d)
 oplot,[x,x],[.02,.98],linestyle=0,color=white*(1-nocolor)
 last_lpos=x
 widget_control,zoom_info.(0),set_value=tai2utc(ltime,/ecs,/vms)
endif

if event.press eq 4 then begin
 mk_cds_erase,last_rpos
 rtime=double(day+x*3600.d)
 oplot,[x,x],[.02,.98],linestyle=2,color=white*(1-nocolor)
 last_rpos=x
 widget_control,zoom_info.(1),set_value=tai2utc(rtime,/ecs,/vms)
endif

return & end

;======================================================================

pro mk_cds_draw,event

;-- cursor handler for DRAW widget

@mk_cds_com

if xregistered('mk_cds_plan',/noshow) ne 1 then return
if datatype(event) ne 'STC' then return

if tag_exist(event,'type') then begin
 if event.type eq 2 then begin
  mk_cds_window
  return
 endif
endif

if (event.press eq 0) or (event.release eq 1) then return
if (xregistered('xzoom_plan') eq 0) then mk_cds_check,event

mk_cds_window
data_x = (float(event.x)/!d.x_vsize - !x.s(0))/!x.s(1)
data_y = (float(event.y)/!d.y_vsize - !y.s(0))/!y.s(1)
x = data_x(0)
y = data_y
x = !x.crange(0) > x < !x.crange(1)
day = tai2utc(custom_stc.startdis) & day.time=0 & day = utc2tai(day)

if y lt 0.0 then return

;-- in SWAP mode?

if xalive(swap_base) then begin
 mk_cds_swap,x,event
 return
endif

;-- in ZOOM mode?

if xregistered('xzoom_plan') then begin
 mk_cds_zoom,x,event
 return
endif

curr_items=custom_stc.req_items
row = fix(y)
nplot=n_elements(curr_items)
if row gt nplot-1 then return
row_id = curr_items(nplot-row-1)
bb= grep(row_id, ['Telemetry', 'Command'], /exact)
res_row = (bb(0) ne '')

;-- delete old cursor position in EDIT row 0

if (row eq 0) and (type ne 1) then begin
 if event.press eq 1 then mk_cds_erase,last_cpos
 if event.press eq 4 then mk_cds_erase,last_epos
endif

;-- clicked in a non-editing row

if (row gt 0) and (event.press ne 1) then return
if (row eq 0) and (event.press ne 1) and (type lt 2) then return
if (row gt 0) then begin
 if exist(last_row) then if res_row ne last_row then $
  mk_cds_erase,last_spos,last_row
 stime = double(day+x*3600.d)
 aa = grep(row_id, curr_items(0:nplot-2),/exact)
 if aa(0) ne ''  then begin

;---------------------------------------------------------------------------
;           draw a line at selected time
;---------------------------------------------------------------------------

  if not res_row then begin
   oplot,[x, x], [row+0.02, row+0.99], linestyle=3, color=2*(1-nocolor)
   last_spos=x
   last_row=row
  endif
            
;---------------------------------------------------------------------------
;           search for the plan entries to be displayed as a structure
;---------------------------------------------------------------------------


  if grep('DET', row_id) ne '' and exist(cds_detail) then begin
   tplan = cds_detail
   title = 'CDS Details Plan'
  endif else if grep('SCI', row_id) ne '' and exist(cds_science) then begin
   tplan = cds_science
   title = 'CDS Science Plan'
  endif else if grep('FLA', row_id) ne '' and exist(cds_flag) then begin
   tplan = cds_flag
   title = 'CDS Flag Plan'
  endif else if grep('ALT', row_id) ne '' and exist(cds_alt) then begin
   tplan = cds_alt
   title = 'CDS Alt Details Plan'
  endif else begin
   if show_stc then begin
    if res_row then begin
     show_res_stc, mk_cds_base, stime, y, bb(0),wbase=wbase,wtags=wtags,/just_reg
    endif else begin
     row_id=get_soho_inst(row_id)
     if custom_stc.dtype eq 0 and exist(soho_splan) then begin
      jj = where(soho_splan.instrume eq strmid(row_id,0,1))
      if jj(0) ge 0 then begin
       tplan = soho_splan(jj)
       title = 'SOHO Science plan'
      endif
     endif
     if custom_stc.dtype eq 1 and exist(soho_dplan) then begin
      jj = where(soho_dplan.instrume eq strmid(row_id, 0, 1))
      if jj(0) ge 0 then begin
       tplan = soho_dplan(jj)
       title = 'SOHO Details Plan'
      endif
     endif
    endelse
   endif
  endelse

;-- show SOHO instrument plans other than one being edited

  if exist(tplan) then begin
   jtime = get_plan_itime(tplan)
   tlook = where((stime lt tplan.(jtime+1)) and $
                 (stime ge tplan.(jtime)), nobs)
   if tlook(0) ge 0 then begin
    rplan=tplan(tlook(0))
    if show_stc then xstruct, mk_plan_conv(rplan, /nopoint), nx=1, $
          wbase=wbase, wtags=wtags, xsize=25,$
          title=title,group=mk_cds_base,/center,/just_reg
   endif
  endif

 endif

 return
endif

;-- if SCIENCE or ALT, user can set end time by using right mouse button

if (event.press eq 4) and (type gt 1) then begin
 etime=round_off(double(day+x*3600.d),tacc)
 last_etime=etime
 oplot,[x,x],[.02,.98],linestyle=2,color=2*(1-nocolor)
 last_epos=x
 widget_control,date_end,set_value=tai2utc(etime,/ecs,/date,/vms)
 widget_control,time_end,set_value=tai2utc(etime,/ecs,/time)
 mk_cds_pdur
 return
endif

;-- get new cursor start position

ctime=double(day+x*3600.d)

;-- check if cursor CTIME is in DETAILS 

index=mk_plan_where(ctime,details,status=in_db)
ctime=round_off(ctime,tacc)

;-- if FLAG, then CTIME/ETIME must match what is in DETAILS

if type eq 1 then begin
 mk_cds_flag,err=err
 if err ne '' then return
end else begin

;-- User can set START and END times from REFERENCE plot (under construction)
             
 if n_elements(cur_items) gt 1 then begin
  if (data_y(1) gt 1) and (data_y(1) lt 2) then begin
   rtype=get_plan_type(cur_items(1))
   if rtype gt -1 then begin
    rdef=get_plan_def(rtype)
    if rdef ne '' then begin
     status=execute('refs=cds_'+rdef)
     if status then begin
      clook=mk_plan_where(ctime,refs,status=in_db)
      if in_db then begin
       mk_cds_erase,last_epos
       rtime=get_plan_itime(refs)
       ctime=refs(clook).(rtime)
       etime=refs(clook).(rtime+1)
       plan.(itime)=ctime
       plan.(itime+1)=etime
       x=(etime-day)/3600.d
       last_etime=etime
       oplot,[x,x],[.02,.98],linestyle=2,color=2*(1-nocolor)
       last_epos=x
       widget_control,date_end,set_value=tai2utc(etime,/ecs,/date,/vms)
       widget_control,time_end,set_value=tai2utc(etime,/ecs,/time)
      endif
     endif
    endif
   endif
  endif
 endif

;-- draw a line at selected time

 oplot,[x,x],[.02,.98],linestyle=1,color=2*(1-nocolor)
 last_cpos=x
 last_ctime=ctime
 widget_control,date_cur,set_value=tai2utc(ctime,/ecs,/date,/vms)
 widget_control,time_cur,set_value=tai2utc(ctime,/ecs,/time)
endelse

;-- highlight selected study in yellow

if in_db then begin
 zplan=details(index)  
 if  exist(hplan) then same=mk_plan_comp(zplan,hplan,/end_time) else same=0
 if not same then begin
  if exist(hplan) then begin
   pcolor = stc_color.cds
   if custom_stc.show_ntt and tag_exist(hplan,'TIME_TAGGED') then begin
    if not hplan.time_tagged then pcolor = stc_color.ntt
   endif
   oplot_splan,hplan,custom_stc.startdis,$
    window=win_ed,color=pcolor*(1-nocolor),spec=(type ne 2)
  endif

;-- warn if switching to a new entry

  mk_cds_warn,replaced,skip=skip
  if skip then return

;-- unhighlight replaced entry

  if replaced and exist(hplan) then begin
   if custom_stc.show_ntt and tag_exist(hplan,'TIME_TAGGED') then begin
    if not hplan.time_tagged then pcolor = stc_color.ntt
   endif
   oplot_splan,hplan,custom_stc.startdis,$
    window=win_ed,color=pcolor*(1-nocolor),spec=(type ne 2)
  endif

  mk_cds_hplan

  if exist(hplan) then begin
   plan=hplan
   if itool_point then mk_cds_send
  endif
  rindex=0 & pindex=0
 endif
 if exist(hplan) then oplot_splan,hplan,custom_stc.startdis,$
  window=win_ed,color=2*(1-nocolor),spec=(type ne 2)
 if not same then mk_cds_refresh

endif

mk_cds_sensit

return & end

;======================================================================

pro mk_cds_warn,saved,noplot=noplot,skip=skip,saving=saving

;-- warn planner to save latest edits when switching to a new entry

@mk_cds_com

if not exist(saving) then saving=0
saved=0 & skip=0

if not db_access then begin
 updated=1
 return
endif

;-- make sure new study doesn't clobber existing study

if (type ne 2) and new_study and (sel_study.study_id gt -1) then begin
 if (not saving) then begin
  mess=['Leaving last loaded study:',$
        '',$
        'ACRONYM:           '+trim(sel_study.obs_prog),$
        'TITLE:             '+trim(sel_study.title),$
        ' ',$
        'What do you wish to do?']

  options=['Go back and continue editing last loaded study',$
          'Skip last loaded study and continue onto next selected study']
  choice=xchoice(mess,options,group=mk_cds_base)
  if choice eq 0 then begin
   skip=1 & mk_cds_plot
  endif else begin
   new_study=0 & mk_cds_refresh
  endelse
 endif
 return
endif
            
if exist(hplan) then begin
 plan_s=plan
 plan_s.(itime)=hplan.(itime)
 if (type eq 1) then plan_s.(itime+1)=hplan.(itime+1)

 ident=mk_plan_comp(hplan,plan_s,dtag=dtag)
 if not ident then begin
  dprint,'% dtag: ',dtag
  warn_mess=['Leaving current plan entry:',$
             ' ',$
             'STARTING AT:       '+anytim2utc(plan.(itime),/ecs),$
             'SCIENCE OBJECTIVE: '+trim(plan.sci_obj)]
  if type ne 2 then begin
   acronym='ACRONYM:           '+trim(sel_study.obs_prog)
   warn_mess=[warn_mess,acronym]
  endif
  value=xanswer(warn_mess,instruct='Apply latest edits to this entry?',group=mk_cds_base)
  if value then begin
   mk_cds_move,/replace,err=err,/nowarn,noplot=noplot
   if err eq '' then saved=1
  endif else plan=hplan
 endif
endif

return & end

;======================================================================

pro mk_cds_redraw, event   

;-- event handler for the detached graphic window

@mk_cds_com

if xregistered('mk_cds_plan',/noshow) ne 1 then return
if datatype(event) ne 'STC' then return
if not tag_exist(event, 'type') then begin
 if xalive(mk_cds_draw) then begin
  widget_control, mk_cds_draw, xsize=event.x, ysize=event.y
  mk_cds_plot
 endif
endif else begin
 mk_cds_draw, event
endelse

return & end

;======================================================================

pro mk_cds_flag,err=err 

;-- get nearest DETAILED entry for FLAG

@mk_cds_com

;-- check if any DETAILS entries appear in plot window

err=''
in_db=0
if exist(cds_detail) then begin
 index=mk_plan_where(ctime,cds_detail,status=in_db)
 dobs=cds_detail
endif

if not in_db then begin
 err='' & list_detail,custom_stc.startdis,custom_stc.stopdis,dobs,nobs,err=err
 if nobs eq 0 then begin
  err='No DETAILS plans found during current display period.'
  xack,[err,'You must specify these before defining FLAGS.'],$
       group=mk_cds_base
  return
 endif
 index=mk_plan_where(ctime,dobs,status=in_db)
endif

if in_db then dobs=dobs(index) else begin
 err='No DETAILS plans near specified time.'
 xack,[err,'You must define a DETAILS plan before associating a FLAG.'],$
       group=mk_cds_base
 delvarx,ctime
 return
endelse

mk_cds_erase,last_cpos
mk_cds_erase,last_epos
dtime=get_plan_itime(dobs)
ctime=dobs.(dtime)
last_ctime=ctime
day=tai2utc(custom_stc.startdis) &  day.time=0 & day=utc2tai(day)
x=(ctime-day)/3600.d
oplot,[x,x],[.02,.98],linestyle=1,color=2*(1-nocolor)
last_cpos=x
etime=dobs.(dtime+1)
x2=(etime-day)/3600.d
oplot,[x2,x2],[.02,.98],linestyle=2,color=2*(1-nocolor)
last_etime=etime
last_epos=x2
widget_control,date_cur,set_value=tai2utc(ctime,/ecs,/date,/vms)
widget_control,time_cur,set_value=tai2utc(ctime,/ecs,/time)
widget_control,date_obs,set_value=tai2utc(ctime,/ecs,/date,/vms)
widget_control,time_obs,set_value=tai2utc(ctime,/ecs,/time)
widget_control,date_end,set_value=tai2utc(etime,/ecs,/date,/vms)
widget_control,time_end,set_value=tai2utc(etime,/ecs,/time)
plan.(itime)=ctime
plan.(itime+1)=etime

return & end

;======================================================================

pro mk_cds_erase,cpos,row 

;-- erase cursor lines

@mk_cds_com

mk_cds_window
if not exist(row) then row=0
if not exist(cpos) then return

white=!d.table_size-1
day=tai2utc(custom_stc.startdis) & day.time=0 & day=utc2tai(day)
if exist(details) then begin
 itime=get_plan_itime(details)
 times=[custom_stc.startdis,details.(itime),details.(itime+1),custom_stc.stopdis]
 times=(times-day)/3600.d
 chk=where(str_format(cpos) eq str_format(times),cnt)
endif else cnt=0
if cnt eq 0 then oplot,[cpos,cpos],row+[.02,.98],color=nocolor*white

return & end

;======================================================================

pro mk_cds_button

;-- sensitize widget buttons

@mk_cds_com

;-- valid highlighted plan to edit?

valid_hplan=0
if exist(hplan) then begin
 valid_hplan=( (hplan.(itime) ge custom_stc.startdis) and (hplan.(itime) le custom_stc.stopdis)) $
                or $
             ( (hplan.(itime+1) ge custom_stc.startdis) and (hplan.(itime+1) le custom_stc.stopdis)) $
                or $
             ( (hplan.(itime) le custom_stc.startdis) and (hplan.(itime+1) ge custom_stc.stopdis))

endif

;-- valid edited plan to write?

valid_plan=0
if exist(plan) then begin
 valid_plan=( (plan.(itime) ge custom_stc.startdis) and (plan.(itime) le custom_stc.stopdis)) $
                or $
            ( (plan.(itime+1) ge custom_stc.startdis) and (plan.(itime+1) le custom_stc.stopdis)) $
                or $
            ( (plan.(itime) le custom_stc.startdis) and (plan.(itime+1) ge custom_stc.stopdis))
endif

;-- valid cursor time?
 
valid_ctime=0
if not exist(ctime) then ctime=custom_stc.startdis 
if type ne 1 then $
 valid_ctime= (ctime ge custom_stc.startdis) and (ctime le custom_stc.stopdis) else valid_ctime=1

;-- is cursor on a highlighted entry?

index=mk_plan_where(ctime,details,status=in_db)

;-- are there any plan entries in window?

valid_details=0
if exist(details) then begin
 clook= where(  $
     ( (details.(itime) ge custom_stc.startdis) and (details.(itime) le custom_stc.stopdis) ) or $
     ( (details.(itime+1) ge custom_stc.startdis) and (details.(itime+1) le custom_stc.stopdis) ) $
     ,cnt)
 valid_details=(cnt gt 0)
endif

;-- add button

if type eq 0 then tt_mess='[Time-Tagged]' else tt_mess=''
widget_control,insb,set_value='Insert At Cursor Position (CUR_TIME) '+tt_mess
widget_control,insb,sensitive=valid_ctime
if type eq 0 then tt_mess='[Not Time-Tagged]' else tt_mess=''
if in_db then app_mess='Append to Highlighted Entry ' else begin
 no_entry=0
 if index eq -1 then no_entry=1 else $
  if details(index).(itime+1) le custom_stc.startdis then no_entry=1
 if no_entry then app_mess='Insert at Start of Displayed Interval ' else  $
  app_mess='Append To Last Entry Before Cursor Position '
 if no_entry and (type eq 0) then tt_mess='[Time-Tagged]'
endelse
widget_control,appb,set_value=app_mess+tt_mess
widget_control,appb,sensitive=valid_ctime and (type ne 1)

;-- move button

widget_control,movb,sensitive=valid_plan and valid_hplan

widget_control,mmov,/sensitive
;if valid_hplan then begin
; if (ctime eq hplan.(itime)) or (ctime eq hplan.(itime+1) ) then begin
;  widget_control,mmov,sensitive=0
; endif
;endif

widget_control,gmov,sensitive=(type ne 1)

;-- remove

widget_control,remb,sensitive=valid_hplan

;-- copy

widget_control,copb,sensitive= valid_hplan and (type ne 1) 

;-- replace

widget_control,repb,sensitive=valid_plan and valid_hplan

;-- study details

widget_control,listb,sensitive=valid_details
widget_control,studyb,sensitive=(type ne 2)

;-- dump

widget_control,dumpb,sensitive=valid_hplan and (type lt 2)

;-- undo

widget_control,undb,sensitive=(exist(sdb_details) or (sdb_empty eq 1) )

;-- save button

widget_control,savb,sensitive=(not updated)

widget_control,swapb,sensitive=valid_hplan

widget_control,scloneb,sensitive=(type eq 2)

widget_control,rcheckb,sensitive=(type eq 0) or (type eq 1) or (type eq 3)

if not valid_hplan then delvarx,hplan

return & end

;======================================================================

pro mk_cds_sensit 

;-- sensitize widgets
   
@mk_cds_com

mk_cds_button

;-- study specific widgets

if xalive(time_tagged_base) then widget_control,time_tagged_base,map=(type eq 0)

if type ne 2 then begin
 err=''
; widget_control,get_raw_id(0),sensitive=use_gis and ((type eq 0) or (type eq 3))
 widget_control,gset_base,sensitive=use_gis
 if (type eq 0) or (type eq 3) then value='edit' else value='noedit'
 widget_control,n_repeat_s,set_value=value
 widget_control,flag_base,map=(type eq 0)
 widget_control,repoint_base,map=(type eq 1)
 widget_control,srep,map=(type eq 0) or (type eq 3)
 widget_control,nrastb,sensitive=(not is_eng)
 if is_eng then value='noedit' else value='edit'
 widget_control,n_rasters1,set_value=value
 widget_control,zbase,sensitive=(not is_eng) 
 widget_control,zbutt,sensitive=(not is_eng) 

;-- sensitize N_POINTINGS and LAST POINTINGS

 if (type eq 0) then begin
  if (sel_study.var_point eq 'Y') then value='edit' else value='noedit'
   widget_control,n_pointings,set_value=value
 endif

endif

widget_control,get_study,sensitive=(type ne 2)
widget_control,mk_study,sensitive=(type ne 2)
widget_control,grab,sensitive=itool_reg

;-- make plan duration and end time editable for type > 1

if (type gt 1) then value='edit' else value='noedit'
widget_control,date_obs,set_value='noedit'
widget_control,time_obs,set_value='noedit'
widget_control,date_end,set_value=value
widget_control,time_end,set_value=value
widget_control,pduration,set_value=value

return & end

;==============================================================================

pro mk_cds_switch,new_type 

;-- switch database type (personal/official)

@mk_cds_com

delvarx,rplan
if type ne new_type then begin
 mk_cds_update,/noplot
 delvarx,hplan
 def_inst_plan,plan,type=new_type,/init
 def_cds_study,sel_study,/rasters
 delvarx,sdb_details & sdb_empty=0
 mk_plan_load,type,details
endif

;-- map base appropriate to SCIENCE

if new_type eq 2 then begin
 if type ne 2 then begin
  widget_control,dbase,map=0
  widget_control,sbase,/map
  widget_control,sbase,get_uvalue=input_struct 
@unpack_struct
 endif

;-- map base appropriate to DETAILS, FLAG, or ALT

endif else begin
 if type eq 2 then begin
  widget_control,sbase,map=0
  widget_control,dbase,/map
  widget_control,dbase,get_uvalue=input_struct
@unpack_struct
 endif
endelse


return & end

;============================================================================

pro mk_cds_refresh

;-- refresh widget fields with current plan parameters

@mk_cds_com 

;-- check type of plan entry

if type lt 0 then return

widget_control,/hour
;-- update plan times

if plan.(itime) le 0 then begin
 if exist(custom_stc.startdis) then plan.(itime)=custom_stc.startdis else begin
  get_utc,utc 
  plan.(itime)=utc2tai(utc)
 endelse
endif

;-- update study parameters

if type ne 2 then begin

 read_study=0
 if exist(sel_study) then begin
  if ((sel_study.study_id) ne (plan.study_id)) or $
     ((sel_study.studyvar) ne (plan.studyvar)) then read_study=1
 endif else read_study=1
 if read_study then begin
  sel_study=get_cds_study(plan.study_id,plan.studyvar,err=err)
  if err ne '' then message,err,/cont
  err=''
  get_study_par,study=sel_study,reps=reps,gis=use_gis,slits=slits,$
                eng=is_eng,err=err,dets=dets
  if err ne '' then message,err,/cont
 endif

;-- update duration and raster slider

 if plan.orig_dur eq 0. then dur=get_cds_dur(plan,point=(type eq 0)) else dur=plan.orig_dur
 widget_control,sduration,set_value=string(dur/dur_unit,'(f12.2)')
 widget_control,rslider,set_slider_min=0,set_slider_max=1,sensitive=0
 nrast=0
 widget_control,cbase,map=1
 widget_control,ebase,map=0
 if (not is_eng) and (sel_study.var_point eq 'N') then $
  act_point=sel_study.n_raster_def else $
   act_point=plan.n_pointings
 widget_control,n_pointings,set_value=trim(num2str(act_point))
 if (not is_eng) then begin 
  rasters=sel_study.rasters
  nrast=n_elements(rasters)
  if nrast gt 1 then begin
   widget_control,rslider,set_slider_min=0,set_slider_max=nrast-1,/sensit
  endif else rindex=0 
  widget_control,rslider,set_value=rindex
 endif else begin
  if (type ne 1) then begin
   widget_control,cbase,map=0
   widget_control,ebase,map=1
   if widget_info(e_pointings,/type) eq 8 then $
    widget_control,e_pointings,set_droplist_select=(plan.n_pointings) < 1 else $
     widget_control,e_pointings,set_value=(plan.n_pointings) < 1
  endif
 endelse
; widget_control,n_rasters,set_value=trim(num2str(nrast))
 widget_control,n_rasters1,set_value=trim(num2str(plan.n_rasters1))

;-- update pointing slider
 
 widget_control,pslider,set_slider_min=0,set_slider_max=1,sensitive=0
 npoint=plan.n_pointings
 if npoint gt 1 then begin
  widget_control,pslider,set_slider_min=0,set_slider_max=npoint-1,/sensi
 endif else pindex=0
 pindex= 0 > (pindex < (npoint-1))
 widget_control,pslider,set_value=pindex 

;-- update remaining parameters

 widget_control,study_id,set_value=trim(num2str(plan.study_id))
 widget_control,studyvar,set_value=trim(num2str(plan.studyvar))
 widget_control,tracking(1-fix(plan.tracking)),/set_button

 widget_control,study_title,set_value=strpad(strmid(trim(sel_study.title),0,40),40,/after)
 widget_control,obs_prog,set_value=sel_study.obs_prog
 if tag_exist(plan,'N_REPEAT_S') then widget_control,n_repeat_s,set_value=trim(num2str(plan.n_repeat_s))
 if tag_exist(plan,'TIME_TAGGED') then widget_control,time_tagged(1-fix(plan.time_tagged)),/set_button

;-- get IEF_ID from first raster in study

 if (not is_eng) and (tag_exist(plan,'FLAG_MASTER')) then begin
  if plan.flag_master eq 0 then begin
   if (not is_eng) and (sel_study.studyvar gt -1) then begin
    ras=get_cds_raster(sel_study.rasters(0).ras_id,sel_study.rasters(0).ras_var)
    if tag_exist(ras,'IEF_ID') then plan.flag_master=ras.ief_id
   endif
  endif
  widget_control,flag_master,set_value=trim(num2str(abs(plan.flag_master)))
  local=(plan.flag_master lt 0)
  wide=(plan.flag_master gt 0)
  widget_control,flag_type(0),set_button=0
  widget_control,flag_type(1),set_button=0
  case 1 of
   local: widget_control,flag_type(1),/set_button
   wide: widget_control,flag_type(0),/set_button
   else:do_nothing=1
  endcase
 endif

;-- GIS stuff

 if tag_exist(plan,'GSET_ID') then widget_control,gset_id,set_value=trim(string(plan.gset_id,'(i4)'))
; if tag_exist(plan,'GET_RAW') then widget_control,get_raw_id(0),set_button=fix(plan.get_raw)

;-- update RASTER pointing info

 mk_cds_point

;-- update ZONE_ID

 mk_cds_zone,zone_id
 zmess=trim(string(zone_id))+' - '+trim(zone_def_label(zone_id))
 widget_control,zmode,set_value=zmess

;-- update corresponding GIS setups

 if use_gis then begin
  fil_id=0 & delvarx,ffb
  zlabel=zone_def_label(zone_id)
  if strpos(zlabel,'#1') gt -1 then fil_id=1
  if strpos(zlabel,'#2') gt -1 then fil_id=2
  if strpos(zlabel,'Neg') gt -1 then ffb=-1
  if strpos(zlabel,'Pos') gt -1 then ffb=1
  if exist(fil_id) then widget_control,fil_id_lab,set_value=trim(string(fil_id,'(i4)'))
 endif

endif

;-- update times and durations

if plan.(itime+1) lt plan.(itime) then plan.(itime+1)=plan.(itime)

dur_unit=(dur_unit > 1.)
ii = where(float(dur_unit) eq [1.,60.,3600.], cnt)
if ii(0) ge 0 then  begin
 if widget_info(dur_unitb,/type) eq 8 then widget_control,dur_unitb, set_droplist_select=ii(0) else $
  widget_control,dur_unitb,set_value=ii(0)
endif

plan.(itime+1) = plan.(itime+1) > plan.(itime)
cur_dur=(plan.(itime+1)-plan.(itime))
plan.(itime+1)=plan.(itime)+cur_dur
widget_control,pduration,set_value=string(cur_dur/dur_unit,'(f12.2)')

if exist(ctime) then begin
 widget_control,date_cur,set_value=tai2utc(ctime,/ecs,/date,/vms)
 widget_control,time_cur,set_value=tai2utc(ctime,/ecs,/time)
endif

widget_control,date_obs,set_value=tai2utc(plan.(itime),/ecs,/date,/vms)
widget_control,time_obs,set_value=tai2utc(plan.(itime),/ecs,/time)

widget_control,date_end,set_value=tai2utc(plan.(itime+1),/ecs,/date,/vms)
widget_control,time_end,set_value=tai2utc(plan.(itime+1),/ecs,/time)

;-- update rest of plan descriptions

widget_control,prog_id,set_value=trim(num2str(plan.prog_id))
widget_control,sci_obj,set_value=strmid(trim(plan.sci_obj),0,40)
widget_control,sci_spec,set_value=strmid(trim(plan.sci_spec),0,40)
widget_control,object,set_value=plan.object
widget_control,obj_id,set_value=plan.obj_id
widget_control,cmp_no,set_value=trim(num2str(plan.cmp_no))

;-- SCIENCE only

if (type eq 2) then begin
 widget_control,xcen,set_value=plan.xcen
 widget_control,ycen,set_value=plan.ycen
 widget_control,notes,set_value=plan.notes
 widget_control,disturbances,set_value=plan.disturbances
endif


return & end

;============================================================================

pro mk_cds_repoint

;-- handle repoints for FLAG studies 

@mk_cds_com

if type ne 1 then return 

widget_control,n_pointings,set_value='noedit'
widget_control,n_pointings,set_value=trim(string(plan.n_pointings,'(i2)'))
if sel_study.var_point eq 'Y' then begin      
 widget_control,repoint,/sensitive
 if plan.n_pointings eq 0 then begin
  if plan.repoint then $
   widget_control,rmode,set_value='Coordinates Supplied By Flag Master' else $
    widget_control,rmode,set_value=['Coordinates Supplied By Flag Master',$
                                    '(No Repoint If Outside FOV)']
  widget_control,ins_x,set_value='noedit'
  widget_control,ins_y,set_value='noedit'
  widget_control,ins_x,set_value=''
  widget_control,ins_y,set_value=''
 endif else begin
  widget_control,rmode,set_value='Coordinates Supplied By Planner'
  widget_control,ins_x,set_value='edit'
  widget_control,ins_y,set_value='edit'
  widget_control,ins_x,set_value= $
                   string(plan.pointings(pindex).ins_x,'(f8.2)')
  widget_control,ins_y,set_value= $
                   string(plan.pointings(pindex).ins_y,'(f8.2)')
 endelse
 widget_control,point_base,sensitive=(plan.n_pointings gt 0)
endif else begin
 widget_control,rmode,set_value=['Coordinates Supplied By Study',$
                                 '(Fixed)']
 widget_control,repoint,sensitive=0
endelse
return & end

;==============================================================================

pro mk_cds_send,verify=verify,auto=auto

;-- send latest plan pointing information to IMAGE_TOOL

@mk_cds_com

if not exist(auto) then auto=2
if itool_reg then begin
 pmess='Plan pointing parameters have changed.'
 val=1
 if keyword_set(verify) then val=xanswer(pmess,instruct='Send to IMAGE_TOOL?',group=mk_cds_base) 
 if val then begin
  tplan=plan
  if (is_eng) then begin
   if (plan.n_pointings eq 0) then begin
    mk_cds_eng,xp,yp,err=err
    tplan.pointings(0).ins_x=xp
    tplan.pointings(0).ins_y=yp
   endif
  endif
  point_stc=mk_plan_point(mk_plan_corr(tplan))
  point_stc.messenger = itool_mess
  image_tool,point=point_stc,auto=auto
 endif
endif
mk_cds_window
return & end

;==============================================================================

pro mk_cds_receive

;-- receive latest plan pointing information from IMAGE_TOOL

@mk_cds_com
   
widget_control,itool_mess, get_uvalue=point_stc
mk_cds_corr,point_stc,verify=1-itool_reg
mk_cds_refresh
mk_cds_plot
mk_cds_sensit

return & end

;==============================================================================

pro mk_cds_corr,point_stc,verify=verify

;-- process latest pointing changes

@mk_cds_com

;-- was valid study ever loaded into ITOOL

verify=keyword_set(verify)
if (type ne 2) then begin
 invalid_plan=plan.study_id lt 0
 invalid_point=point_stc.std_id lt 0
 if invalid_point or invalid_plan then begin

  if (not invalid_plan) and invalid_point then begin
   xack, ['No Detailed study currently loaded into IMAGE_TOOL. Pointing not updated.'],$
           group=mk_cds_base
  endif
 
  if (not invalid_point) and invalid_plan then begin
   xack, ['No Detailed study currently loaded into MK_PLAN. Pointing not updated.'],$
           group=mk_cds_base
  endif
  return
 endif
endif
 
;-- did N_POINTINGS change?

have_pointing = tag_exist(plan,'pointings')
if is_eng and have_pointing then begin
 if (plan.n_pointings eq 0) then begin
  message,'Fixed engineering pointing',/cont
  return
 endif
endif

if have_pointing and (not is_eng) then begin
 if not point_stc.do_pointing then point_stc.n_pointings=0
 if point_stc.n_pointings ne plan.n_pointings then begin
  if itool_reg then begin
   state=trim(plan.sci_obj)
   if trim(plan.sci_spec) ne '' then state=state+'-'+trim(plan.sci_spec)
   xack, ['Number of received deferred pointings incompatible',$
           'with current plan entry:','',state],group=mk_cds_base
  endif
  return
 endif
endif

;-- did actual pointings change?

if type eq 2 then format='(i8)'
plan=mk_plan_xy(mk_plan_corr(plan),format)
old_point_stc=mk_plan_point(plan)
new_point_stc=mk_plan_xy(point_stc,format)
if match_struct(old_point_stc,point_stc,exclude=0) then begin
 message,'IMAGE_TOOL pointing unchanged',/cont
 return
endif

if (type ne 2) then begin
 if point_stc.std_id ne plan.study_id then begin
  if itool_reg then begin
   wmess='Current STUDY_ID in MK_PLAN entry differs from that in IMAGE_TOOL.'
   val=xanswer(wmess,instruct='Proceed with pointing change?',$
               group=mk_cds_base,/supp,/check)
  endif else val=0
  if not val then return
 endif
endif

new_plan=plan
xp=point_stc.pointings.ins_x
yp=point_stc.pointings.ins_y
if type ne 2 then begin
 new_plan.pointings.ins_x=xp
 new_plan.pointings.ins_y=yp
endif else begin
 new_plan.xcen=xp
 new_plan.ycen=yp
endelse
new_plan=mk_plan_xy(new_plan)
  
;-- give user option to save pointing
  
if type eq 2 then begin
 point_change=(plan.xcen ne new_plan.xcen) or (plan.ycen ne new_plan.ycen)
endif else begin
 point_change=1-match_struct(plan.pointings,new_plan.pointings,dtag=dtag)
 dprint,'%dtag: ',dtag
endelse

if point_change then begin
 if verify then begin
  state=plan.sci_obj
  imess=['New pointings received for: ',' ',state]
  instruct='Apply them to current entry?'
  value=xanswer(imess,instruct=instruct,group=mk_cds_base)
 endif else value=1

 if value then begin

;-- update IMAGE tool pointing history

  if type ne 2 then begin
   xp=new_plan.pointings.ins_x
   yp=new_plan.pointings.ins_y
   for i=0,n_elements(xp)-1 do begin
    if exist(last_point) then begin
     lastx=last_point.ins_x
     lasty=last_point.ins_y
     check_it=where((xp(i) eq lastx) and (yp(i) eq lasty),count)
     have_it=(count gt 0)
    endif else have_it=0
    if not have_it then begin
     last_point=concat_struct(last_point,{ins_x:xp(i),ins_y:yp(i)})
    endif
   endfor
  endif
  plan=new_plan
  mk_cds_rep
 endif 
endif

return & end

;==============================================================================

pro mk_cds_rep,mess,status=status,wbase=tbase                   ;-- replace current plan

@mk_cds_com

replace_it=0 & status=0
if not new_study then begin
 if exist(hplan) then begin
  if (type ne 2) then begin
   replace_it= (hplan.study_id eq plan.study_id) and $
               (hplan.studyvar eq plan.studyvar)
  endif else replace_it=1
 endif
endif 

mk_cds_sdur,/quiet
mk_cds_point

if replace_it then begin
 if datatype(mess) eq 'STR' then fmess=mess else fmess=''
 xtext,[fmess,'Replacing highlighted plan...','   '],wbase=tbase,/just_reg
 mk_cds_move,/replace,/nowarn,/use_ctime,status=status,/quiet
 xkill,tbase
endif else begin
 if type eq 2 then begin
  widget_control,xcen,set_value=plan.xcen
  widget_control,ycen,set_value=plan.ycen
 endif
endelse

return & end

;==============================================================================

pro mk_cds_eng,xp,yp,err=err  

;-- inherit ENG pointing from following or prior study

@mk_cds_com

err=''
if (type ne 0) or (not is_eng) then return

xp=0. & yp=0.

lindex=mk_plan_where(plan,details,status=in_db)
if in_db then begin
 nindex=lindex+1
 lindex=lindex-1
endif else nindex=lindex+2
 
;-- check following plan

np=n_elements(details)
if (nindex le (np-1)) then begin
 err=''
 get_cds_xy,details(nindex),xp,yp,err=err,/first
endif

;-- else check prior plan

use_prior=((err ne '') or (nindex gt (np-1))) and (lindex gt -1)
if use_prior then get_cds_xy,details(lindex),xp,yp,err=err,/last

dprint,use_prior,xp,yp

return & end

;==============================================================================

pro mk_cds_point  

;-- update widget fields with pointing information

@mk_cds_com

if (not exist(sel_study)) or (type eq 2) then return

;-- first show raster pointings defined for study

popts=['Offset','Fixed','Deferred']
pvals=[-1,0,1]

;-- load individual raster pointings

if (not is_eng) then begin 
 rasters=sel_study.rasters
 popt=rasters(rindex).pointing
 clook=where(popt eq pvals,cnt)
 widget_control,pmode,set_value=trim(popts(clook(0)))
 xp=rasters(rindex).ins_x
 yp=rasters(rindex).ins_y
 rbase=where(popt ne -1,cp)
 if cp gt 0 then begin
  xp=xp+rasters(rbase).ins_x
  yp=yp+rasters(rbase).ins_y
 endif
 widget_control,ins_x,set_value=string(xp,'(f8.2)')
 if (popt eq 1) then eval='edit' else eval='noedit'
 widget_control,ins_y,set_value=string(yp,'(f8.2)')
endif else begin
 widget_control,pmode,set_value='No Rasters'
 widget_control,rslider,sensitive=0
 if (plan.n_pointings eq 0) then begin
  eval='noedit'
  mk_cds_eng,xp,yp,err=err
  xp=string(xp,'(f8.2)')
  yp=string(yp,'(f8.2)')
  widget_control,ins_x,set_value=xp
  widget_control,ins_y,set_value=yp
 endif else eval='edit'
endelse
widget_control,ins_x,set_value=eval
widget_control,ins_y,set_value=eval
widget_control,point_base,sensitive=(eval eq 'edit')

;-- override with deferred pointings 

npoint=(plan.n_pointings > 1)
cds_point=plan.pointings(0:npoint-1)
if not exist(popt) then popt=1
if (popt eq 1) and (plan.n_pointings gt 0) then begin
 widget_control,ins_x,set_value=string(cds_point(pindex).ins_x,'(f8.2)')
 widget_control,ins_y,set_value=string(cds_point(pindex).ins_y,'(f8.2)')
endif

;-- Pointing for FLAGS treated separately

if (type eq 1) then mk_cds_repoint else widget_control,rmode,set_value=''

return & end

;============================================================================

pro mk_cds_window

@mk_cds_com

if not exist(color_set) then color_set=0
if (!d.window ne win_ed) and (win_ed gt -1) and (!d.name eq 'X') then begin
 dprint,'% resetting MK_PLAN window..'
 smart_window,win_ed & color_set=0
endif

if not color_set then begin
 tvlct,r,g,b,/get
 if (n_elements(r) gt 3) and (n_elements(g) gt 4) and (n_elements(b) gt 5) then $
  color_set=(r(3) eq 255) and (g(4) eq 255) and (b(5) eq 255)
 if not color_set then begin
  dprint,'% resetting colors..'
  set_line_color
  color_set=1
 endif
endif

return & end

;============================================================================

pro mk_cds_tchange,t1,t2,time_change       ;-- check if time changed

@mk_cds_com

time_change=0
if exist(startdis) and exist(stopdis) then begin
 time_change=(t1 ne startdis) or (t2 ne stopdis)
endif

return & end

;============================================================================

pro mk_cds_plot,time_change=time_change,nohighlight=nohighlight

;-- plot plans and timelines

@mk_cds_com

if not xalive(mk_cds_draw) then mk_cds_detach
if (win_ed lt 0) or (not xalive(mk_cds_base)) then return

mk_cds_window
time_change=keyword_set(time_change)

;-- Postscript plot?

if (!d.name eq 'PS') then begin
 font_sav=!p.font & !p.font=0 
endif

;-- plot the damn mess

if custom_stc.show_ntt then ucolor = stc_color.ntt else ucolor = 0
plot_splan,custom_stc.startdis,custom_stc.stopdis,plan_items=custom_stc.req_items,nocolor=nocolor,$
   now=custom_stc.show_now, day=custom_stc.show_day, color=stc_color, $
   local=custom_stc.show_local, ucolor=ucolor, dtype=custom_stc.dtype,$
   window=win_ed

;-- update highlighted plan entry

if (!d.name eq 'X') and not keyword_set(nohighlight) then begin
 if type eq 0 then mk_cds_erase,last_cpos
 if (type gt 0) then mk_cds_erase,last_epos
 index=mk_plan_where(hplan,details,match)
 if match then oplot_splan,hplan,custom_stc.startdis,color=2*(1-nocolor),$
  window=win_ed,spec=(type ne 2)
 day=tai2utc(custom_stc.startdis) &  day.time=0 & day=utc2tai(day)
 if exist(ctime) and not time_change then begin
  cpos=(ctime-day)/3600.d
  oplot,[cpos,cpos],[.02,.98],linestyle=1,color=2*(1-nocolor)
  last_cpos=cpos
 endif
 if exist(etime) and (type ne 0) and (not time_change) then begin
  epos=(etime-day)/3600.d
  oplot,[epos,epos],[.02,.98],linestyle=2,color=2*(1-nocolor)
  last_epos=epos
 endif
endif

;-- update start time of plot

err=''
val=anytim2utc(custom_stc.startdis,/ecs,/vms,err=err)

;-- update duration

cdur=(custom_stc.stopdis-custom_stc.startdis)
ii=where(cdur eq sec_spans,cnt)
if widget_info(tspan,/type) eq 8 then set_funct='set_droplist_select' else set_funct='set_value'
if cnt gt 0 then set_value='ii(0)' else set_value='n_elements(sec_spans)-1'
s=execute('widget_control, tspan,'+set_funct+'='+set_value)

widget_control,disp_obs,set_value=val

;-- set things back

if exist(font_sav) then !p.font=font_sav
xshow,mk_cds_draw
xkill,tbase

return & end

;=============================================================================

pro mk_cds_update,err=err,noplot=noplot,mess=mess,refresh=refresh

;-- update database with latest edits (a true save)

@mk_cds_com

err=''

mk_cds_warn,noplot=noplot,skip=skip,/saving
if skip then return
sav_mess=['Please wait. Saving latest changes...    ']

if not updated then begin
 if not db_access then begin
  xack,db_err,group=mk_cds_base,/icon
  updated=1
  return
 endif
 xtext,sav_mess,wbase=tbase,/just_reg
 widget_control,/hour
 err=''
 mk_plan_write,details,last_details,/nocheck,err=err,/retry,/noloop
 if exist(last_details) then details=last_details else delvarx,details
 err=''
 if err eq '' then begin
  updated=1 
  mk_plan_load,type,details
  rm_file,mk_plan_crash
  xtext,[sav_mess,'Save completed.'],wbase=tbase,/just_reg
 endif else xack,'Encountered some problems saving plans',group=mk_cds_base
 if keyword_set(refresh) then mk_cds_plot
 xshow,mk_cds_draw
endif else begin
 if keyword_set(mess) then $
  xtext,'Current Plans already saved.',/wait,/just_reg
endelse

xkill,tbase,/wait
return & end

;=============================================================================

pro mk_cds_widgets

;-- create MK_PLAN widgets

@mk_cds_com

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

;-- create main base

new_vers=float(strmid(!version.release,0,3)) ge 4.
device, get_screen_size=sz
if (sz(0) ge 1280) and (sz(1) ge 1024) then sz(*) = 0
sz = sz < [1280, 1024]

mk_cds_base = widget_base(title ='', /column,uvalue='ut_update',$
                          x_scroll=sz(0), y_scroll=sz(1))
;-- row 1

row1=widget_base(mk_cds_base,/row)
mk_cds_ops,row1

buff=widget_base(mk_cds_base)
mk_cds_dbase,buff,dbase,map=0,lfont=lfont,bfont=bfont
mk_cds_sbase,buff,sbase,map=0,lfont=lfont,bfont=bfont

;-- time control widgets

row2=widget_base(mk_cds_base,/row)   

;-- change display start

shift_base = widget_base(row2, /row,/frame)
shift_values=['1 Hour Forward','1 Day Forward ','1 Week Forward']
shift_values=[shift_values,'1 Hour Backward','1 Day Backward ','1 Week Backward']
shift_values=strpad(shift_values,17,/after)
shift_uvalues=['FORHR','FORDAY','FORWK','BACKHR','BACKDAY','BACKWK']
shiftb = widget_button(shift_base,font=bfont,value='SHIFT ',/menu)
for i=0,n_elements(shift_values)-1 do begin
 temp=widget_button(shiftb,value=shift_values(i),uvalue=shift_uvalues(i),font=bfont)
endfor
temp=widget_label(shift_base,value='DISPLAY START', font=lfont)

temp = widget_base(row2, /row,/frame)
wherep=widget_button(temp,value='REPLOT',uvalue='refresh',$
                    font=bfont,/no_rel)
junk=widget_label(temp,value=' DISPLAY WINDOW',font=lfont)

temp = widget_base(row2, /row,/frame)
zoomb=widget_button(temp,value='ZOOM',uvalue='zoom_in',$
                    font=bfont,/no_rel)
junk=widget_label(temp,value=' DISPLAY WINDOW',font=lfont)

;-- step thru entries

step_base=widget_base(row2,/row,/frame)
stepb=widget_button(step_base,value='STEP',font=bfont,/menu,/no_rel)
slab=widget_label(step_base,value=' THRU PLANS',font=lfont)
step_next=widget_button(stepb,value='Step to next entry',uvalue='step_next',font=bfont,/no_rel)
step_back=widget_button(stepb,value='Step to previous entry',uvalue='step_back',font=bfont,/no_rel)
step_first=widget_button(stepb,value='Step to first entry',uvalue='step_first',font=bfont,/no_rel)
step_last=widget_button(stepb,value='Step to last entry',uvalue='step_last',font=bfont,/no_rel)

;-- status row

scbase = widget_base(mk_cds_base,/column)
row0=widget_base(scbase,/row)   
temp=widget_base(row0,/row,/frame)   
messenger=temp
junk=widget_label(temp,value='STATUS -->',font=lfont)

;-- current DB edit

col1=widget_base(row0,/column,/frame)
row1 = widget_base(col1, /row)

temp = widget_label(row1, value='EDITING', font=lfont)
choices=['DETAILS','FLAG   ','SCIENCE','ALT    ']

if new_vers then $
 ptype_bt = call_function('widget_droplist',row1,value=choices,uvalue='switch_ptype',font=bfont) else $
  ptype_bt=cw_bselector2(row1,choices,$
              /no_rel,/return_index, uvalue='switch_ptype',font=bfont)
temp = widget_label(row1, value='PLAN', font=lfont)

row2 = widget_base(col1, /row)
tmp = widget_label(row2, value='USING', font=lfont)
choices=strupcase(['Personal', 'Official'])
if new_vers then $
 db_type=call_function('widget_droplist',row2,value=choices,uvalue='switch_db',font=bfont) else $
  db_type = cw_bselector2(row2,choices, /return_index,$
                /no_rel,uvalue='switch_db',font=bfont)

tmp = widget_label(row2, value='DATABASE', font=lfont)

;-- Display start time

col2=widget_base(row0,/column,/frame)
row1 = widget_base(col2, /row)
start_lab=widget_label(row1,value='DISPLAY START:',font=lfont)
disp_obs = cw_text(row1, value=' ',xsize=24,font=lfont,/edit,uvalue='enter')

;-- Change display duration

row2 = widget_base(col2, /row)
tspan_value = ['3 hours', '6 hours ', '12 hours', '1 day  ', '1.5 days ', $
               '2 days ', '3 days  ', '4 days  ', '5 days ', '6 days   ',$
               '7 days  ','ZOOMED   ']
tmp = widget_label(row2, value='DISPLAY DURATION: ', font=lfont)
if new_vers then begin
 tspan=call_function('widget_droplist',row2,value=tspan_value,$
                       uvalue='tshift',font=bfont) 
endif else begin
 tspan =cw_bselector2(row2, tspan_value, uvalue='tshift',$
                        /no_rel,font=bfont,/return_index)
endelse

;-- control buttons

temp = widget_base(row0, /nonexclusive,/frame,/column)
show_stcb = widget_button(temp, value='AUTO VIEW UPPER DISPLAY PARAMETERS', uvalue='show_stc', $
                          font=lfont)
widget_control, show_stcb, set_button=show_stc

copy_sci = widget_button(temp, value='AUTO INHERIT SCI_PLAN PARAMETERS',uvalue='get_sci',$
                          font=lfont)
widget_control,copy_sci,set_button=get_sci

copy_point = widget_button(temp, value='AUTO COPY POINTING TO IMAGE_TOOL',uvalue='itool_point',$
                          font=lfont)
             
widget_control,copy_point,set_button=itool_point


;temp = widget_base(row1, /nonexclusive,/frame)
;insertb = widget_button(temp, value='INSERT MODE', uvalue='insert_yes', $
;                          font=lfont)


;-- set up plot window
;-- (bottom row is editable and DETAILS by default)
;-- (optional top rows are for reference and SCIENCE by default)       

xsize=1000 & ysize=250
if not detached then begin
 mk_cds_draw = widget_draw(mk_cds_base,ysize=ysize, /button_event,xsize=xsize,$
	       		      retain=2,uvalue='draw',/motion)
endif

;-- realize bases

mk_detail_base=mk_cds_base

mk_cds_detach

;-- show current UT

ut_delay=1.d
if float(strmid(!version.release,0,3)) le 3.5 then begin
 plan_title = 'CDS Plan Builder  '
 widget_control,mk_cds_base,tlb_set_title=plan_title
endif else begin
 get_utc, curr_ut, /ecs,/vms
 plan_title = 'CDS Plan Builder Version 20    Current UT: '
 widget_control,mk_cds_base, tlb_set_title = plan_title+strmid(curr_ut,0,20)
 widget_control,mk_cds_base, timer =ut_delay
endelse
child=widget_info(mk_cds_base,/child)
widget_control,child,$
 set_uvalue={ut_delay:ut_delay,plan_title:plan_title}

return & end

;=============================================================================

pro mk_cds_ops,row1    ;-- operations buttons

@mk_cds_com

temp1=widget_base(row1,/frame,/row)
itool_mess=temp1

doneb=widget_button(temp1,value='Done',font=bfont,/no_rel,uvalue='done')

;-- special operations

operations=widget_button(temp1,value='Operations',font=bfont,/menu,/no_rel)

rcheckb=widget_button(operations,value='CHECK Raster Modes Against DSN Telemetry Rates',uvalue='rate_check',font=bfont,/no_rel)

scloneb=widget_button(operations,value='CLONE A Set Of Plans',uvalue='clone',font=bfont,/no_rel)

scopyb=widget_button(operations,value='COPY A Set Of Plans',uvalue='copy_plans',font=bfont,/no_rel)

customize=widget_button(operations,value='CUSTOMIZE Display Window (DSN, etc)',uvalue='customize',font=bfont,/no_rel)


sdelb=widget_button(operations,value='DELETE A Set Of Plans',uvalue='delete_plans',font=bfont,/no_rel)


grab=widget_button(operations,value='HARD Copy IMAGE_TOOL Window',uvalue='grab_image',font=bfont)

hardb=widget_button(operations,value='HARD Copy Timeline Window',uvalue='hard',font=bfont,/no_rel)

importb=widget_button(operations,value='IMPORT Plans',uvalue='import',font=bfont)

listb=widget_button(operations,value='LIST Currently Saved Plans',uvalue='dlist',font=bfont,/no_rel)

mk_study=widget_button(operations,value='MK_STUDY - Create New Study',uvalue='mk_study',font=bfont,/no_rel)

smoveb=widget_button(operations,value='MOVE A Set Of Plans',uvalue='move_plans',font=bfont,/no_rel)

dumpb=widget_button(operations,value='PRINT Highlighted Plan Entry To File',uvalue='dump', $
                  font=bfont,/no_rel)
      
scat=widget_button(operations,value='PRINT Plan Summary For XCAT',uvalue='xcat_sum', $
                  font=bfont,/no_rel)
      
studyb=widget_button(operations,value='PRODUCE Study Briefing',uvalue='brief',font=bfont,/no_rel)

refb=widget_button(operations,value='REFRESH Display',uvalue='refresh',font=bfont,/no_rel)

resb=widget_button(operations,value='RESET Fields & REREAD Databases',uvalue='reset',font=bfont,/no_rel)



updb=widget_button(operations,value='UPDATE SCIENCE Plan With DETAILS Plan',uvalue='upd_sci',font=bfont,/no_rel) 

rkap = widget_button(operations, value='UPDATE_KAP - Get latest DSN, etc',uvalue='rkap',font=bfont,/no_rel)

campb= widget_button(operations,value='XCAMP - View And Update Campaigns',uvalue='xcamp',font=bfont,/no_rel)

catb=widget_button(operations,value='XCAT  - View FITS Catalog/Data',uvalue='xcat',font=bfont,/no_rel) 

cptb=widget_button(operations,value='XCPT  - Run Command Preparation Tool',uvalue='xcpt',font=bfont,/no_rel) 

xdocb=widget_button(operations,value='XDOC  - Browse Software Tree',uvalue='xdoc',font=bfont,/no_rel)

iap=widget_button(operations,value='XIAP  - Write Instrument Activity Plan',uvalue='xiap',font=bfont,/no_rel)

exportb=widget_button(operations,value='XPORT - Export Plans',uvalue='export',font=bfont,/no_rel)

get_study=widget_button(temp1,value='Load New Study',/menu,font=bfont,/no_rel)
g1=widget_button(get_study,value='--> From Databases',uvalue='get_study',font=bfont,/no_rel)
g2=widget_button(get_study,value='--> From Bookmarks (Faster)',uvalue='get_book',font=bfont,/no_rel)

imageb=widget_button(temp1,value='Image Tool',uvalue='point',font=bfont,/no_rel) 

helpb=widget_button(temp1,value='Help',uvalue='help',font=bfont,/no_rel)

;-- editing buttons

temp2=widget_base(row1,/frame,/row)

addb=widget_button(temp2,value='Add',uvalue='add',font=bfont,/menu,/no_rel)
insb=widget_button(addb,value='Add At Cursor Position (CUR_TIME) ',uvalue='add',/no_rel,font=bfont)
appb=widget_button(addb,value='Add After Last Entry ',uvalue='add',/no_rel,font=bfont)
copb =widget_button(temp2,value='Copy',uvalue='copy',font=bfont,/no_rel)

movb=widget_button(temp2,value='Move',uvalue='move',font=bfont,/menu,/no_rel)
mmov=widget_button(movb,value='Move to New Cursor Time Position',uvalue='move',/no_rel,font=bfont)
gmov=widget_button(movb,value='Move Graphically',uvalue='copy',/no_rel,font=bfont)
remb=widget_button(temp2,value='Remove',uvalue='remove',font=bfont,/no_rel)
repb=widget_button(temp2,value='Replace',uvalue='move',font=bfont,/no_rel)
swapb=widget_button(temp2,value='Swap',uvalue='swap',font=bfont,/no_rel)
savb=widget_button(temp2,value='Save',uvalue='update',font=bfont,/no_rel)
undb=widget_button(temp2,value='Undo',uvalue='undo',font=bfont,/no_rel)

return & end

;=============================================================================

pro mk_cds_plan,tstart,group=group,restore=restore,over=over,$
      reset=reset,err=err,nodetach=nodetach,_extra=extra,lock=lock

;-- driver program

@mk_cds_com

err=''
select_windows
if not have_widgets() then message,'widgets are unavailable'

;-- create lock file

do_lock=keyword_set(lock)
over=keyword_set(over)
lock_zdbase,'mk_plan_lock',/daily,expire=30.*60.,lock=lock_file,$
 status=locked,over=over,check=1-do_lock

db_access=mk_plan_priv(err=err,lock=lock_file,/quiet,/init)
if not db_access then $
 xack,[err,'','You will not be allowed to edit plans.',''],/suppress,group=group

;-- check out DB. 

find_zdbase,cur_db_type,status=status,/detail,file=db_file
if status eq 0 then return
zdbase=getenv('ZDBASE')
message,'using DB from --> '+zdbase,/contin
cur_db=zdbase
if  datatype(sav_db) ne 'STR' then sav_db = zdbase
if  cur_db ne sav_db then begin
 message, 'ZDBASE changed!!!', /contin
 reset=1 & sav_db=cur_db
endif 

;-- load user start and stop times

if datatype(custom_stc) eq 'STC' then begin
 old_startdis=custom_stc.startdis 
 old_stopdis=custom_stc.stopdis
endif else begin
 get_utc,utc
 utc.time=0
 old_startdis=utc2tai(utc)
 old_stopdis=old_startdis+secs_day
endelse

if n_elements(tstart) ne 0 then begin
 err=''
 new_startdis=utc2tai(anytim2utc(tstart,/ecs,err=err)) 
 if err ne '' then message,'invalid START time entered'
endif else new_startdis=old_startdis

old_dur=(old_stopdis-old_startdis) > 3600.d
new_stopdis=new_startdis+old_dur
time_change=(old_startdis ne new_startdis) or (old_stopdis ne new_stopdis)
if not time_change then mk_cds_tchange,new_startdis,new_stopdis,time_change

;-- full reset or partial recovery?

crash_file=concat_dir(getenv('HOME'),'.mk_plan')
if not exist(first_time) then first_time=1
reset=keyword_set(reset) or exist(mk_soho_base) 
if not exist(updated) then updated=1
if not exist(stored) then begin
 if not updated then stored=0 else stored=1
endif
resume=0 & recover=0
if (not reset) then begin

;-- check for a previously crashed run

 recover=0
 if db_access and (first_time or keyword_set(restore)) then begin
  mk_plan_crash=crash_file+'-*'
  chk=loc_file(mk_plan_crash,count=count)
  if count gt 0 then begin
   first_time=0
   mk_plan_crash=chk(count-1)
   cpos=strpos(mk_plan_crash,'-')
   if cpos gt -1 then begin
    curr_ut=tai2utc(double(strmid(mk_plan_crash,cpos+1,strlen(mk_plan_crash))),/vms)
    mess=['Located save file from a previously failed or unsaved run at: ',$
         '',$
         curr_ut]
    instruct='Restore contents and resume MK_PLAN?
    recover=xanswer(mess,instruct=instruct,group=group)
   endif
  endif
  if recover then begin
   xtext,'Please wait. Restoring last run...',wbase=tbase,/just_reg
   widget_control,/hour
   restore,file=mk_plan_crash
   if datatype(rcustom) eq 'STC' then begin
    custom_stc=rcustom
    curr_items=custom_stc.req_items
    nplot=n_elements(curr_items)
    type=get_plan_type(curr_items(nplot-1))
   endif
   delvarx,details,last_details,plan
   if datatype(last_rplan) eq 'STC' then plan=last_rplan
   if datatype(rdetails) eq 'STC' then details=rdetails
   if datatype(last_rdetails) eq 'STC' then last_details=last_rdetails
   updated=0 & stored=0 
   mk_plan_load,type,details 
   xkill,tbase
  endif else reset=1
  rm_file,crash_file+'-*'
 endif

;-- check for previously unsaved run

 if time_change and (not recover) and (not updated) then begin
  mess=['Previous MK_PLAN session was not saved.']
  instruct='Resume it before changing to a new time?'
  resume=xanswer(mess,instruct=instruct,group=group)
  if not resume then reset=1
 endif

endif

;-- reset here
 
if reset then begin
 message,'resetting...',/cont
 type=0
 dummy=get_cds_study(/init)
 dummy=get_cds_raster(/init)
 def_inst_plan,plan,type=type,/init
 def_cds_study,sel_study,/rasters
 delvarx,hplan
 mk_plan_reset
 delvarx,mk_soho_base,details
 updated=1
endif

if resume or recover then begin
 new_startdis=custom_stc.startdis
 new_stopdis=custom_stc.stopdis
endif

if datatype(custom_stc) NE 'STC' then begin
 req_items = ['CDS-SCI','CDS-DET']
 custom_stc={startdis:new_startdis,stopdis:new_stopdis,req_items:req_items,$
             show_now:1,show_day:1,show_ntt:1,show_local:1,$
             dtype:0, troff:1.0,nocolor:0}
endif else begin
 custom_stc.startdis=new_startdis
 custom_stc.stopdis=new_stopdis
endelse
tacc=custom_stc.troff*60.
nocolor=custom_stc.nocolor

;-- initialize 

if n_elements(dur_unit) eq 0 then dur_unit=1.
pindex=0 & rindex=0 & color_set=0
if not exist(rule_on) then rule_on=1
if not exist(show_stc) then show_stc= 0 
win_itool=-1 & itool_reg=0
if not exist(get_sci) then get_sci=0
if not exist(itool_point) then itool_point=1
curr_items=custom_stc.req_items
nplot=n_elements(curr_items)
type = get_plan_type(curr_items(nplot-1))
last_type = get_plan_type(plan)
if last_type ne type then def_inst_plan,plan,type=type
if datatype(sel_study) ne 'STC' then sel_study={study_id:-1} 
if sel_study.study_id eq -1 then def_cds_study,sel_study,/rast
itime=get_plan_itime(type)
;delvarx,ctime,rplan,hplan,etime
plan=mk_plan_corr(plan)
stc_color = get_inst_color(/stc)
delvarx,sdb_details & sdb_empty=0
insert_on=0
if not exist(new_study) then new_study=0
detached=1-keyword_set(nodetach)

;-- recover useful study parameters

err='' 
get_study_par,study=sel_study,reps=reps,gis=use_gis,slits=slits,$
 eng=is_eng,err=err,dets=dets

if err ne '' then message,err,/cont

;-- save initial color table and load B/W

if not xalive(group) then xkill,/all
if not exist(rs) then tvlct,rs,gs,bs,/get  
loadct,0,/silent

;-- create widgets here

mk_cds_widgets

;-- map appropriate bases and unpack widget ID's from common

if type eq 2 then begin
 widget_control,sbase,/map
 widget_control,sbase,get_uvalue=input_struct 
endif else begin
 widget_control,dbase,/map
 widget_control,dbase,get_uvalue=input_struct 
endelse
@unpack_struct

;-- read DB here

dprint,'time_change,reset,first_time,resume,recover',$
        time_change,reset,first_time,resume,recover


if updated or recover then begin
 force=(reset or first_time)
 if recover then no_type=type
 mk_cds_read,force=force,no_type=no_type
endif

;-- recover last cursor positions

if exist(last_ctime) then begin
 ok=(last_ctime ge custom_stc.startdis) and (last_ctime le custom_stc.stopdis)
 if ok then ctime=last_ctime
endif

if exist(last_etime) then begin
 ok=(last_etime ge custom_stc.startdis) and (last_etime le custom_stc.stopdis)
 if ok then etime=last_etime
endif

;-- recover last highlighted plan

if exist(ctime) then last_ctime=ctime
if exist(hplan) then begin
 index=mk_plan_where(hplan,details,status=in_db)
 if not in_db then delvarx,hplan
endif
if not exist(hplan) then mk_cds_hplan 
if exist(hplan) then ctime=hplan.(itime)
if not exist(ctime) then ctime=custom_stc.startdis
if exist(last_ctime) then begin
 if abs(last_ctime-ctime) gt 10.*secs_day then ctime=custom_stc.startdis
endif

;-- now plot plans 

cleanplot
mk_cds_plot
mk_cds_refresh
mk_cds_sensit

;-- set current status

dtype=(cur_db_type eq 'CDS')
if widg_type(db_type) eq 'DROPLIST' then widget_control,db_type,set_droplist_select=dtype else $
 widget_control,db_type,set_value=dtype

if widg_type(ptype_bt) eq 'DROPLIST' then  widget_control, ptype_bt, set_droplist_select=type else $
 widget_control, ptype_bt, set_value=type

if type eq 0 then widget_control,rule_but(1-rule_on),/set_button
 
;-- give planner some starting help

if first_time and db_access then begin
 if type eq 2 then mess=start_mess_s else mess=start_mess
 widget_control,mk_cds_base,sensitive=0
 xack,mess,group=mk_cds_base,instruct='GO',/supp
 widget_control,mk_cds_base,sensitive=1
endif
first_time=0

;-- manage widgets

xmanager,'mk_cds_plan',mk_cds_base,group=group
dprint,'%MK_CDS_PLAN: out of first XMANAGER'

xmanager_reset,mk_cds_base,group=group,crash='mk_cds_plan'

dprint,'%MK_CDS_PLAN: out of second XMANAGER'

;-- cleanup

if not xalive(mk_cds_base) then begin
 if detached then set_line_color else if exist(rs) then tvlct,rs,gs,bs
 smart_window,win_ed,/rem
 if keyword_set(lock) then unlock_zdbase,lock_file
endif

return & end

