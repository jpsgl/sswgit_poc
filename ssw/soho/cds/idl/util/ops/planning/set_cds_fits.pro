;+
; Project     : SOHO - CDS
;
; Name        : SET_CDS_FITS
;
; Purpose     : Define CDS_FITS_DATA environment variable to
;               available disks
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : IDL>set_cds_fits
;
; Inputs      : None
;
; Opt. Inputs : DEF_PATH = default path for CDS_FITS_DATA [def = current]
;
; Keywords    : IMAX = max disk number to search [def=20]
;               FIRST = put default path first
;               ROOT = lead root directory (def = '/') 

; Side effects: CDS_FITS_DATA is defined
;
; History     : Version 1,  31-May-1997,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


pro set_cds_fits,def_path,imax=imax,first=first,root=root

if n_elements(imax) eq 0 then imax=30
cd,curr=cur_path
if datatype(def_path) eq 'STR' then begin
 clook=loc_file(def_path,count=count)
 if count ne 0 then cur_path=def_path
endif

if datatype(root) ne 'STR' then root='/service/cdso9'
if not is_dir(root) then return

dirs=root+'/arch'+trim(sindgen(imax))
valid=is_dir(dirs)
ok=where(is_dir(dirs),count)
if count eq 0 then begin
 message,'no valid archive directories found',/cont
 return
endif

path=arr2str(dirs(ok),delim=':')

full_path=cur_path
if exist(path) then begin
 if keyword_set(first) then full_path=cur_path+':'+path else $
  full_path=path+':'+cur_path
endif

setenv,'CDS_FITS_DATA='+full_path
return & end
