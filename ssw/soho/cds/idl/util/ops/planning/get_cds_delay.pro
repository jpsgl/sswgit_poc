;+
; Project     : SOHO - CDS
;
; Name        : GET_CDS_DELAY
;
; Purpose     : get delay (secs) due to pointing and slit changes between
;               two successive CDS studies
;
; Category    : planning
;
; Explanation : computes delay when changing pointing and slit from
;               one plan entry to another
;
; Syntax      : IDL> delay=get_cds_delay(last_plan,next_plan)
;
; Inputs      : LAST_PLAN = last plan structure
;             : NEXT_PLAN = next plan structure
;
; Opt. Inputs : None
;
; Outputs     : seconds delay between two successive CDS plan entries
;
; Opt. Outputs: None
;
; Keywords    : ERR = error string
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  1-Sep-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_cds_delay,last_plan,next_plan,err=err

err=''  & delay=0

;-- if LAST_PLAN doesn't exist, use HOME position

home_pos=get_cds_home()
last_x=home_pos.solarx
last_y=home_pos.solary
last_slit=home_pos.slitn
next_x=last_x
next_y=last_y
next_slit=last_slit

if not exist(last_plan) then begin
 dprint,'% GET_CDS_DELAY: using HOME position for first plan entry'
endif else begin
 err=''
 get_study_par,plan=last_plan,slits=last_slits,err=err
 if err ne '' then return,delay
 last_slit=last_slits(n_elements(last_slits)-1)
 get_cds_xy,last_plan,last_x,last_y,/last,err=err
 if err ne '' then return,delay
endelse

if not exist(next_plan) then begin
 dprint,'% GET_CDS_DELAY: using HOME position for second plan entry'
endif else begin
 err=''
 get_study_par,plan=next_plan,slits=next_slits,err=err
 if err ne '' then return,delay
 next_slit=next_slits(0)
 get_cds_xy,next_plan,next_x,next_y,/first,err=err
 if err ne '' then return,delay
endelse

;-- compute pointing delay

pdelay=0.

if (last_x ne next_x) or (last_y ne next_y) and have_proc('get_ops_pos') then begin
 perr=''
 get_ops_pos,last_x,last_y,last_opsl,last_opsr,err=perr
 if perr eq '' then begin
  get_ops_pos,next_x,next_y,next_opsl,next_opsr,err=perr
  if perr eq '' then begin
   delay_l=get_ops_delay(last_opsl,next_opsl,0)
   delay_r=get_ops_delay(last_opsr,next_opsr,1)
   pdelay=float(delay_l + delay_r)/1000.
  endif
 endif
endif

;-- compute slit delay

sdelay=0.
if (last_slit ne next_slit) and have_proc('get_slitn_delay') then begin
 sdelay=sdelay+call_function('get_slitn_delay',last_slit,next_slit)/1000.
 dprint,'% GET_CDS_DELAY: adding slit delay'
endif
delay=pdelay+sdelay

return,delay & end

