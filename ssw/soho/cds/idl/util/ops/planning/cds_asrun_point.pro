;+
; Project     : SOHO - CDS
;
; Name        : CDS_ASRUN_POINT
;
; Purpose     : create map structures for ASRUN CDS pointings from catalog
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : IDL> cds_asrun_point,cmap,tstart,tend
;
; Inputs      : TSTART = plot start time [def=current date]
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # of pointings found
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-May-1998,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro cds_asrun_point,cmap,tstart,tend,count=count,err=err

common cds_asrun_point,plans,last_start,last_end

on_error,1

err=''
count=0
delvarx,cmap

;-- save original ZDBASE

status=fix_zdbase(/original,err=err)
if not status then begin
 message,err,/cont
 return
endif

;-- error trap

error=0
catch,error
if error ne 0 then begin
 err=strmessage(error)
 message,err,/cont
 goto,bailout
endif

;-- go to official ZDBASE

err1='' & err2=''
status=fix_zdbase(/cds,err=err1)
set_cds_sdb,err=err2
if (err1 ne '') and (err2 ne '') then begin
 message,err1,/cont
 goto,bailout
endif

;-- check time inputs

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then begin
 get_utc,t1
 t1.time=0
endif

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
 t2.time=0
endif

;-- only re-read if time changed

new_start=anytim2tai(t1) & new_end=anytim2tai(t2)
time_change=1
if exist(last_start) and exist(last_end) and exist(plans) then $
 time_change=(new_start ne last_start) and (new_end ne last_end)
if time_change then begin
 list_exper,t1,t2,plans,count
 last_start=new_start & last_end=new_end
 if count eq 0 then delvarx,plans
endif else count=n_elements(plans)

if count eq 0 then begin
 message,'No ASRUN plans during specified times',/cont
 return
endif

;-- make pointing structures

for i=0,count-1 do begin
 time=anytim2utc(plans(i).date_obs,/vms)
 ras_id=plans(i).ras_id
 ras_var=plans(i).ras_var
 study=get_cds_study(plans(i).study_id,plans(i).studyvar)
 eng=study.n_raster_def eq 0 
 fixed=study.var_point eq 'N'
 temp=make_map(fltarr(2,2),xc=plans(i).xcen,yc=plans(i).ycen,$
               dx=plans(i).ixwidth/2.,dy=plans(i).iywidth/2.,$
               time=time,/soho,id=plans(i).obs_prog,eng=eng,fixed=fixed)
 cmap=merge_struct(cmap,temp)
endfor

;-- restore original ZDBASE

bailout: status=fix_zdbase(/original)
catch,/cancel

return & end

