;+
; Project     : SOHO - CDS
;
; Name        : PLOT_CDS_POINT
;
; Purpose     : Plot CDS pointings on latest EIT image
;
; Category    : planning
;
; Explanation : Latest EIT image read from SUMMARY_DATA or PRIVATE_DATA
;
; Syntax      : IDL> plot_cds_point,tstart,tend
;
; Inputs      : TSTART = plot start time [def=current date]
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Keywords    : ASRUN = plot actual pointing based on CATALOG
;               FILE  = FITS file for image [if EIT not wanted]
;               MAP   = map image [if FILE or EIT not wanted]
;               WAVE  = EIT wavelength 304/195/284/171 [def=304]
;               QUIET = switch off message outputs
;               EXCLUDE = study acronyms to exclude, e.g [SYNOP, FULLCCD]
;               DEF_EXCLUDE = exclude default studies (engineering, SYNOP)
;               NOENG = exclude engineering studies 
;               GSIZE = [nx,ny] dimensions of GIF image [def=512,512]
;               BACK = # of days backward to check for EIT
;               ROTATE = solar rotate pointings to image time
;               NAR  = oplot NOAA AR labels
;               LAST = reuse last base image
;               LIMB = set to plot a limb
;               GRID = grid size in degrees
;               NOIMG = set to not plot image (just limb)
;               CHARSIZE = size of NOAA AR labels [def=1.5]
;               OUT_DIR = output directory
;               OUT_FILE = output file
;               FORMAT = png, gif, or jpeg for output plot file
;
; History     : Written, 20-May-1998,  D.M. Zarro.
;             : Modified, 30-Nov-1999, Zarro (SM&A/GSFC) - add /LIMB
;             : Added JPEG/PNG support, 24 Oct 2000, Zarro (EIT/GSFC)
;             : Added CHMOD g+w to output graphics files, 20 June 2001, 
;                 Zarro (EIT/GSFC)
;             : Added CHARSIZE keyword, 8 Dec 2001, Zarro (L3-Com/GSFC)
;             : Changed EIT from half to full res images, 15 Mar 2002, Zarro (L3-Com/GSFC
;             : Modified to search EIT QL archive, 22 Aug 2006, Zarro (ADNET/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro plot_cds_point,tstart,tend,gsize=gsize,log=log,out_dir=out_dir,$
     file=file,map=map,wave=wave,path=path,reuse=reuse,out_file=out_file,$
     nar=nar,quiet=quiet,err=err,rotate=rotate,last=last,_extra=extra,$
     limb=limb,grid=grid,noimg=noimg,charsize=charsize,verbose=verbose,$
     format=format,back=back


common plot_cds_point,last_map,last_log,last_header,dsave

verbose=keyword_set(verbose)
quiet=1-verbose
limb_only=keyword_set(noimg)

;-- check time inputs

t1=get_def_times(tstart,tend,dend=t2,/round)
tstamp=anytim2utc(t1,/vms,/date)
if is_string(format) then begin

;-- determine output directory

 use_dir=curdir()
 temp=file_break(out_file,/path)
 if is_string(temp) then use_dir=temp
 if is_dir(out_dir) then use_dir=out_dir
 if ~is_dir(use_dir) then begin
  message,'undefined directory '+use_dir,/cont
  return
 endif
 if ~write_dir(use_dir) then begin
  message,'no write access to '+use_dir,/cont
  return
 endif

;-- determine output filename

 use_file='cds_point_'+date_code(t1)
 if is_string(out_file) then use_file=file_break(out_file,/no_ext)
 use_file=concat_dir(use_dir,use_file+'.'+format)
endif
         
;-- use a Z-buffer

if ~exist(dsave) and (!d.name ne 'Z') then dsave=!d.name

xsize=512 & ysize=512
if not exist(gsize) then zsize=[xsize,ysize] else $
 zsize=[gsize(0),gsize(n_elements(gsize)-1)]


if is_string(format) then begin
 psave=!p.color
 ncolors=!d.table_size
 set_plot,'z',/copy
 device,/close,set_resolution=zsize,set_colors=ncolors
 !p.color=ncolors-1
endif

;-- check for FITS file input

if not limb_only then begin
 do_log=1b
 reuse=keyword_set(reuse) or keyword_set(last)
 if keyword_set(reuse) and valid_map(last_map) then begin
  map=last_map
  if is_string(last_header) then header=last_header
  if exist(last_log) then do_log=last_log
 endif
 if exist(log) then do_log=(0b> log < 1b)

 if not valid_map(map) then begin
  if datatype(file) eq 'STR' then fits2map,file,map,err=err
 endif

;-- if FITS file or map structure not input, check EIT archive
;   latest EIT files.
;   If no files found, try MDI, else just plot limb

 err='' & files='' & type=''

 if not valid_map(map) then begin
  day_secs=24*3600.
  st1=t1
  st2=t2
  if is_number(back) then st1=st1-day_secs*abs(back)
  st1=anytim2utc(st1,/vms)
  st2=anytim2utc(st2,/vms)
  if verbose then message,'searching data between: '+st1+' -- '+st2,/cont
  if exist(wave) then begin
   files=eit_files(st1,st2,wave=wave,/lz)
   if is_blank(files) then files=eit_files(st1,st2,wave=wave,/quick)
  endif
  if is_blank(files) then begin
   if verbose then message,'searching for any EIT wavelength',/cont
   files=eit_files(st1,st2,/lz)
   if is_blank(files) then files=eit_files(st1,st2,/quick)
  endif

  if is_string(files) then begin
   obj=obj_new('eit')
   file=max(files)
   obj->read,file
   map=obj->get(/map)
   header=obj->get(/header)
   do_log=1b
   obj_destroy,obj
  endif
 endif

;-- check for bad data

 if valid_map(map) then begin
  dmax=max(map.data,min=dmin)
  if dmin eq dmax then begin
   if verbose then message,'invalid EIT image',/cont
   delvarx,map
  endif
 endif

 if valid_map(map) then begin
  last_map=map 
  last_log=do_log
  last_header=header
  if is_string(header) then load_eit_color,header
 endif

endif

;-- try MDI as last ditch attempt

do_it=0
if do_it and ~valid_map(map) then begin
 do_log=0b
 if verbose then message,'no matching EIT files found, trying MDI',/cont
 files=mdi_latest() 
 if is_string(files) then begin
  times=fid2time(files,/tai)
  chk=where_times(times,tstart=anytim2tai(st1),tend=anytim2tai(st2),count=count)
  if count gt 0 then begin
   if verbose then message,'found matching MDI',/cont
   mfile=files[chk[count-1]]
   mdi=obj_new('mdi')
   mdi->read,mfile
   map=mdi->get(/map)
   obj_destroy,mdi
   if stregex(map.id,'intensitygram',/bool,/fold) then loadct,1 else loadct,0
  endif
 endif
endif

if not valid_map(map) then limb_only=1b else rtime=map.time

if limb_only then begin
 rotate=0b
 rtime=t1
endif

if limb_only then begin
 plot_helio,t1,grid=grid
endif else begin
 plot_map,map,xsize=zsize(0),ysize=zsize(1),$
     log=do_log,/square,_extra=extra,grid=grid,title=tstamp
endelse

;-- read CDS plans

tplans=rd_cds_point(t1,t2,count=pcount,time=rtime,rotate=rotate,_extra=extra)
if pcount gt 0 then begin                          
 for i=0,pcount-1 do begin
  plot_map,tplans(i),/over,/bord,/quiet,_extra=extra,lcolor=0,/no_drotate
 endfor
 dprint,'% PLOT_CDS_POINT: ',tplans(0).time,' -- ',tplans(pcount-1).time
endif

;-- add Active Regions

do_nar=keyword_set(nar)
if do_nar then begin
 if have_proc('get_nar') then begin
  nar=call_function('get_nar',rtime,quiet=quiet,count=count,/nearest,/unique)
  if count gt 0 then nar=call_function('drot_nar',nar,rtime,count=count) 
  if not exist(charsize) then charsize=1.5
  if count gt 0 then oplot_nar,nar,charsize=charsize
 endif
endif

;-- save file

if is_string(format) then begin
 temp=tvrd()
 device,/close
 tvlct,rs,gs,bs,/get
 if verbose then message,'saving to '+use_file,/cont
 write_image,use_file,format,temp,rs,gs,bs,_extra=extra,quality=100
 chmod,use_file,/g_write,u_write
endif

if exist(dsave) then set_plot,dsave
if exist(psave) then !p.color=psave

return & end
