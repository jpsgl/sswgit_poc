;+
; Project     :	SOHO - CDS
;
; Name        :	GET_CDS_STUDY
;
; Purpose     :	get a CDS study from database
;
; Explanation :	speeds up GET_STUDY by saving each study in common
;
; Use         :	STUDY=GET_CDS_STUDY(STUDY_ID,STUDYVAR)
;
; Inputs      :	STUDY_ID  = study id
;
; Opt. Inputs : STUDYVAR = study variation
;
; Outputs     :	STUDY = study definition 
;               (only fundamental part is returned if STUDYVAR not entered)
;
; Opt. Outputs:	None.
;
; Keywords    :	USE_TITLE = set to search on TITLE_ID in place of STUDY_ID
;               ERR = error string
;               INIT = initialize commons and return
;               ADD_VAR = add STUDYVAR tag (if not present)
;               FUND_ONLY = return fundamental part only
;               VERBOSE = verbose output 
;
; Calls       :	GET_STUDY
;
; Common      :	GET_CDS_STUDY_COM
;                 
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 28 March 1995
;-
;--------------------------------------------------------------------

pro reset_cds_study                  ;--initialize pointers

common get_cds_study,study_com,fstudy_com,sav_sdb,last_fstudy,last_study

if datatype(fstudy_com) eq 'STC' then free_pointer,fstudy_com.pointer
if datatype(study_com) eq 'STC' then free_pointer,study_com.pointer
delvarx,study_com,fstudy_com,last_fstudy,last_study

return & end

;--------------------------------------------------------------------


function get_cds_study,study_id,studyvar,use_title=use_title,err=err,$
         init=init,add_var=add_var,fund_only=fund_only,verbose=verbose

common get_cds_study
err=''


fund_only=((1-exist(studyvar)) and exist(study_id)) or keyword_set(fund_only)
def_cds_study,init_study,fund_only=fund_only,/rast

if keyword_set(init) then begin
 reset_cds_study
 return,init_study
endif

use_title=keyword_set(use_title)
if not exist(study_id) then study_id=-1
if study_id lt 0 then return,init_study

;-- check if database changed

cur_db=getenv('ZDBASE')
if datatype(sav_sdb) ne 'STR' then sav_sdb=cur_db
if cur_db ne sav_sdb then begin
 message,'ZDBASE changed!!!',/contin
 reset_cds_study
 sav_sdb=cur_db
endif

if keyword_set(verbose) then begin
 if exist(studyvar) then $
  dprint,'% GET_CDS_STUDY: being called for STUDY_ID, STUDYVAR',study_id,studyvar else $
   dprint,'% GET_CDS_STUDY: being called for STUDY_ID ',study_id
endif

;-- check common first

if use_title then key_param='title_id' else key_param='study_id'
rdb=1 & found=0
if fund_only then begin
 if exist(last_fstudy) then s=execute('found=study_id eq last_fstudy.'+key_param)
 if found then rdb=0
 if (not found) and exist(fstudy_com) then begin
  tcom=fstudy_com
  s=execute('clook=where(study_id eq fstudy_com.'+key_param+', cnt)')
  if cnt gt 0 then rdb=0
 endif
endif else begin
 if exist(last_study) then $
  s=execute('found=study_id eq last_study.'+key_param+' and studyvar eq last_study.studyvar')
 if found then rdb=0
 if (not found) and exist(study_com) then begin
  tcom=study_com
  s=execute('clook=where(study_id eq study_com.'+key_param+' and studyvar eq study_com.studyvar, cnt)')
  if cnt gt 0 then rdb=0
 endif
endelse

;-- read study from DB and save in common

again:
if rdb then begin
 err=''
 if fund_only then $
  get_f_study,study_id,study,use_title=use_title,err=err else $
   get_study,study_id,studyvar,study,use_title=use_title,err=err
 if err eq '' then begin
  make_pointer,pointer
  if fund_only then pstudy={fstudy_com,study_id:study.study_id,title_id:study.title_id,pointer:pointer} else $
   pstudy={study_com,study_id:study.study_id,title_id:study.title_id,studyvar:study.studyvar,pointer:pointer}
  set_pointer,pointer,study
  tcom=concat_struct(tcom,pstudy)
 endif else begin
  err=err+' STUDY_ID - '+trim(string(study_id))+', STUDYVAR - '+trim(string(studyvar))
  message,err,/cont & return,init_study
 endelse
endif else begin
 if found then begin
  if fund_only then study=last_fstudy else study=last_study
 endif else begin
  study=get_pointer(tcom(clook(0)).pointer,undefined=undefined)
  if undefined then begin
   rdb=1
   goto,again
  endif
 endelse
endelse

;-- update common

if exist(tcom) then if fund_only then fstudy_com=tcom else study_com=tcom 

if study.study_id gt -1 then begin
 if fund_only then last_fstudy=study else last_study=study
endif

;-- add STUDYVAR tag

if (keyword_set(add_var)) and (not tag_exist(study,'STUDYVAR')) then begin
 index=where(tag_names(study) eq 'N_RASTER_DEF',count)
 if count eq 0 then index=6
 study=add_tag(study,-1,'STUDYVAR',index=index(0))
endif


return,study & end

