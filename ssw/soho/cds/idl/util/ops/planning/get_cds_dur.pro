;+
; Project     :	SOHO - CDS
;
; Name        :	GET_CDS_DUR
;
; Purpose     :	compute study duration based on CDS plan details
;
; Explanation :	computes delays caused by OPS and slit movements
;
; Use         :	DUR=GET_CDS_DUR(PLAN)
;
; Inputs      :	PLAN = CDS detailed plan entry
;               
;
; Opt. Inputs : None
;
; Outputs     :	DUR = duration of study + extra from variable rasters
;
; Opt. Outputs:	None.
;
; Keywords    : POINT = set to compute delay for repointing
;               ERR = error string
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 9 January 1995.
;-


function get_cds_dur,plan,point=point,err=err

;-- recover duration of CDS planned study (allowing for variable repeats of last
;   raster)

dur=0.
err=''
type=get_plan_type(plan)
if (type lt 0) then return,0.
itime=get_plan_itime(plan)
if itime gt -1 then dur=(plan.(itime+1)-plan.(itime)) > 0.
if (type eq 2) or (not tag_exist(plan,'STUDY_ID')) or (itime lt 0) or $
   (not tag_exist(plan,'STUDYVAR')) then begin
 message,'No STUDY_ID/STUDYVAR associated with PLAN',/cont
 return,dur
endif

;-- study repeats?

nreps=1
if type ne 1 then nreps=float(plan.n_repeat_s) > 1.

;-- get raster parameters for planned study

get_study_par,plan=plan,study=study,rdur=rdur,reps=reps,$
         slits=slit,sdur=sdur,err=err
if err ne '' then return,dur

;-- check for engineering study

if study.n_raster_def eq 0 then begin
 sdur=nreps*sdur
 plan.orig_dur=sdur
 return,sdur
endif

if sdur eq 0 then return,sdur

rasters=study.rasters
nrast=n_elements(rasters)
last_rast_dur=rdur(nrast-1)/reps(nrast-1)

;-- if only one raster, then must repeat it at least once

if plan.n_rasters1 eq 0 then begin
 if nrast eq 1 then plan.n_rasters1=1
endif
npoints=float(plan.n_pointings) > 1.

;-- get slit numbers and compute delays for changing slits

temp=intarr(nrast) & sdelay=0.
for i=0,nrast-1 do begin
 if i gt 0 then begin
  if slit(i) ne slit(i-1) then $
    sdelay=sdelay+get_slitn_delay(slit(i-1),slit(i))/1000.
 endif
endfor
idelay=0.
if slit(0) ne slit(nrast-1) then $
 idelay=get_slitn_delay(slit(nrast-1),slit(0))/1000.
slit_delay=nreps*npoints*sdelay+(npoints+nreps-2)*idelay
dprint,'slit_delay',slit_delay

if (not keyword_set(point)) then begin
 if nrast gt 1 then bdur=total(rdur(0:nrast-2)) else bdur=0.
 dur=bdur+float(plan.n_rasters1)*last_rast_dur
 total_dur=dur*nreps*npoints+slit_delay
endif else begin

;-- go thru each raster, summing delay time from last pointing
 
 dur=0. & err=''
 get_cds_xy,plan,x,y,study=study,err=err
 if err ne '' then return,dur
 for j=0,npoints-1 do begin
  for i=0,nrast-1 do begin
   xp=x(j,i) & yp=y(j,i)

;-- record start pointing of study

   if (i eq 0) and (j eq 0) then begin
    start_xp=xp & start_yp=yp
    delay=0. 
   endif else begin
    if (last_x ne xp) or (last_y ne yp) then begin
     delay=0
     perr=''
     get_ops_pos,xp,yp,opsl,opsr,err=perr
     if perr eq '' then begin
      get_ops_pos,last_x,last_y,last_opsl,last_opsr,err=perr
      if perr eq '' then begin
       delay_l=get_ops_delay(last_opsl,opsl,0)
       delay_r=get_ops_delay(last_opsr,opsr,1)
       delay=float(delay_l + delay_r)/1000.
      endif
     endif
    endif else delay=0.
   endelse

;-- allow for variable repeats of last raster 

   if i eq (nrast-1) then ras_dur=plan.n_rasters1*last_rast_dur else $
    ras_dur=rdur(i)

   dur=dur+delay+ras_dur

;-- save current pointing

   last_x=xp & last_y=yp

;-- record end pointing of study

   if (i eq (nrast-1)) and (j eq (npoints-1)) then begin
    end_xp=xp & end_yp=yp 
   endif

  endfor
 endfor

;-- next compute time to position from study end to study start position

 if (end_xp ne start_xp) or (end_yp ne start_yp) then begin
  perr='' & inter_delay=0
  get_ops_pos,start_xp,start_yp,start_opsl,start_opsr,err=perr
  if perr eq '' then begin
   get_ops_pos,end_xp,end_yp,end_opsl,end_opsr,err=perr
   if perr eq '' then begin
    delay_l=get_ops_delay(end_opsl,start_opsl,0)
    delay_r=get_ops_delay(end_opsr,start_opsr,1)
    inter_delay=float(delay_l + delay_r)/1000.
   endif
  endif
 endif else inter_delay=0.
 total_dur=nreps*dur+(nreps-1)*inter_delay+slit_delay
endelse 

if tag_exist(plan,'ORIG_DUR') then plan.orig_dur=total_dur
dprint,'%GET_CDS_DUR: total_dur -',total_dur
return,total_dur & end


