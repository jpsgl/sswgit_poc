;+
; Project     : SOHO - CDS     
;                   
; Name        : SET_CDS_SDB
;               
; Purpose     : append 'sdb' directories  to ZDBASE
;               
; Category    : Planning
;               
; Syntax      : IDL> set_cds_sdb
;    
; Side effects: Environment/logical ZDBASE is reset
;               
; History     : Version 1,  5-August-1997,  D M Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-            

pro set_cds_sdb,original=original,err=err,cds=cds

;-- only do this if within SSW

defsysv,'!ys_dpath',exists=defined
if not defined then return

err=''
common set_cds_zdb,orig_zdbase

if keyword_set(original) and is_string(orig_zdbase) then begin
 def_dirlist,'ZDBASE',orig_zdbase
 return
endif

status=0b
if keyword_set(cds) then status=fix_zdbase(/cds) 
if not status then status=fix_zdbase(/user)

zdb=chklog('ZDBASE')
if is_blank(orig_zdbase) then orig_zdbase=zdb

;-- check for SDB databases

sdb=chklog('sdb')
case 1 of
 is_dir('$sdb',out=out): sdb = out
 is_dir('$SSWDB',out=out): sdb=out
 is_dir('$SDB',out=out): sdb=out
 else: begin
  err='sdb environmental not defined'
  return
 end
endcase

sdb_dir=local_name(sdb+'/soho/cds/data/plan/database')
if not is_dir(sdb_dir) then return

;-- check if SDB already appended

spos=strpos(zdb,sdb_dir)
if spos gt -1 then return

sdbase='+'+sdb_dir
delim=get_path_delim()
if is_blank(zdb)  then zdbase=sdbase else zdbase=zdb+delim+sdbase

mklog,'ZDBASE',zdbase
exp_zdbase

dprint,chklog('ZDBASE')

return & end
