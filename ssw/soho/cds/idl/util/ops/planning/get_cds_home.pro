;+
; Project     : SOHO - CDS
;
; Name        : GET_CDS_HOME
;
; Purpose     : get CDS home position
;
; Category    : planning
;
; Explanation : calls GET_HOME_POS and saves result in COMMON for later
;               speed reading.
;
; Syntax      : IDL> home=get_cds_home()
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : Home position structure with pointing, slit, gset_id etc
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : GET_CDS_HOME - last HOME position
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  27-Jan-1996,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_cds_home,dummy

common get_cds_home,home_pos,sav_db

on_error,1

;-- re-read if DB changed

if not exist(sav_db) then sav_db=getenv('ZDBASE')
if (not exist(home_pos)) or (getenv('ZDBASE') ne sav_db) then home_pos=get_home_pos()

return,home_pos & end

