;+
; Project     :	SOHO - CDS
;
; Name        :	MK_STUDY
;
; Purpose     : Create CDS studies
;
; Use         : mk_study
;
; Inputs      : None.
;
; Opt. Inputs : STUDY = complete study definition
;
; Outputs     : STUDY = new study
;
; Opt. Outputs: None.
;
; Keywords    :
;               GROUP    = event id of widget that calls mk_study
;
; Explanation : Creates a new CDS study by combining rasters or modifying
;               a user-supplied structure definition.
;
; Calls       : Many routines
;
; Common      : MK_STUDY_COM0
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Widgets, planning
;
; Prev. Hist. : None.
;
; Modified    : Changed location of .pdm new data file.  CDP, 29-Jul-96
;
; Written     : Zarro ARC/GSFC) 10 October 1994
;
; Version     : Version 2, 29-Jul-96
;-

;============================================================================

pro mk_study_event,  event                         ;event driver routine

common mk_study_com0,prast,vrast,sel_praster,sel_vraster,out_raster,sval,$
 prlist,vrlist,srlist,wtags,wbase,wvtags,wvbase,remb,addb,writeb,$
 sname,stext,svtext,out_study,view_details,xptext,yptext,gdef,rptext,$
 info,repb,cds_det,cbutt,pbutt,rbutt,tittext,sidtext,svatext,n_repeat_r,$
 pcol1,nrepb,popt,popts,sdur,ztext,ibutt,zlabel,texist,lfont,export_dir,$
 tried_and_failed,slit_no,tplanb2,cur_db,sel_study,sav_db,relist_xstudy,$
 id_text,var_text

widget_control, event.id, get_uvalue = uservalue

if (exist(uservalue) eq 0) then uservalue=''
wtype=widget_info(event.id,/type)

;-- disallow zero repeat rasters

widget_control,n_repeat_r,get_value=value
value=strtrim(value(0),2)
if (value eq '') or (value eq '0') then value='1'
if exist(sval) and exist(out_raster) then begin
 widget_control,n_repeat_r,set_value=value
 out_raster(sval).n_repeat_r=long(value)
 err='' & get_raster_par,out_raster,sdur=dur,err=err
 widget_control,sdur,set_value=string(dur,'(f10.2)')
endif

;-- raster stuff

if exist(sval) and exist(out_raster) then begin          

 if event.id eq xptext then begin
  widget_control,xptext,get_value=value
  xcen=strtrim(value(0),2)
  if xcen ne '-' then begin
   xp=float(xcen) & yp=float(out_raster(sval).ins_y)
   valid=valid_cds_point(xp,yp)
   if not valid then begin
    xack,'X-coordinate is outside allowable pointing limits',group=event.top,$
     /icon
    widget_control,xptext,set_value=string(out_raster(sval).ins_x,'(f8.2)')
   endif else out_raster(sval).ins_x=xp
  endif
 endif

 if event.id eq yptext then begin
  widget_control,yptext,get_value=value
  ycen=strtrim(value(0),2)
  if ycen ne '-' then begin
   yp=float(ycen) & xp=float(out_raster(sval).ins_x)
   valid=valid_cds_point(xp,yp)
   if not valid then begin
    xack,'Y-coordinate is outside allowable pointing limits',group=event.top,$
     /icon
    widget_control,yptext,set_value=string(out_raster(sval).ins_y,'(f8.2)')
   endif else out_raster(sval).ins_y=yp
  endif
 endif
   
 if (event.id eq n_repeat_r) then begin
  widget_control,n_repeat_r,get_value=value
  value=long(strtrim(value(0),2))
  if value eq 0 then value=1
  out_raster(sval).n_repeat_r=value
  err='' & get_raster_par,out_raster,sdur=dur,err=err
  widget_control,sdur,set_value=string(dur,'(f10.2)')
 endif

endif

;-- button events

bname=strtrim(uservalue,2)

;-- ZONE ID

chk=strpos(bname,'NDATA_')
if chk gt -1 then begin
 under=strpos(bname,'_')
 out_study.zone_id=zone_def_id(strmid(bname,under+1,4))
 widget_control,ztext,set_value=zone_def_label(out_study.zone_id)
endif

;-- study category

clook=where(bname eq ['S','C','T'],cnt)
if cnt gt 0 then begin
 out_study.category=bname
endif

;-- pointing type

clook=where(bname eq popts,cnt)
if cnt gt 0 then begin
 popt=popts(clook(0))
 pvals=[-1,0,1]
 if exist(out_raster) and exist(sval) then begin
  out_raster(sval).pointing=pvals(clook(0))
  if popt eq 'deferred' then begin
   out_raster(sval).ins_x=0.
   out_raster(sval).ins_y=0.
  endif
 endif
 mk_study_srlist
endif

;-- detector 

clook=where(bname eq ['NIS','GIS'],cnt)
if cnt gt 0 then begin
 cds_det=strmid(bname,0,1)
 mk_study_plist
endif

;-- operation type

clook=where(bname eq ['add','remove','create','replace'],cnt)
if cnt gt 0 then begin
 if exist(out_study) then begin
  out_study.study_id=-1 & out_study.studyvar=-1 
  out_study.title_id=-1
  widget_control,sidtext,set_value='-1'
  widget_control,svatext,set_value='-1'
  widget_control,tittext,set_value='-1'
 endif
endif

case bname of 

;-- dump to file

  'dump' : begin
    if exist(out_raster) and exist(out_study) then begin
     if (out_study.study_id ne -1) and (out_study.studyvar ne -1) and $
      (tag_exist(out_study,'RASTERS')) then $
      dstudy=out_study else dstudy=mk_cds_study(out_study,out_raster)
     err=''
     dfile=''
     widget_control,event.top,/hour
     show_study,dstudy,file=dfile,/quiet,/keep,err=err
     if dfile eq '' then err='Could not write file'
     if err ne '' then xack,err,group=event.top else begin
      mstudy=rd_ascii(dfile)
      dmess='Following study info will be printed to file: '+dfile
      amess=[dmess,'    ',mstudy]
      value=xanswer(amess,group=event.top,instruct='Print? ') 
      if value then xprint,dfile,group=event.top else s=delete_file(dfile,/noconf)
     endelse
    endif else xack,'No study and/or rasters created',group=event.top
   end

;-- filter out slit number

  'pick_slit' : begin
    slit_no=event.index
    mk_study_plist
   end
 
;-- view study DB

  'views'  : begin 
    delvarx,sel_study
    xstudy,sel_study,group=event.top,/select,relist=relist_xstudy
    if exist(sel_study) then begin
     if (sel_study.study_id eq 0) or (not tag_exist(sel_study,'RASTERS')) then begin
      xack,'Selected study is an engineering study. Cannot proceed.',group=event.top,/icon
     endif else mk_study_update
    endif
   end

  'svtitle' : begin
    if event.id eq svtext then begin
     widget_control,event.id,get_value=text
     out_study.sv_desc=strmid(strtrim(text(0),2),0,65)
    endif else begin
     text=out_study.sv_desc
     xinput,text,$
      ['Enter study variation description [65 char max]',$
       'e.g. "Variation 2, 10 rasters"'],group=event.top,$
     /modal,max_len=65,tfont=lfont
     out_study.sv_desc=text
     widget_control,svtext,set_value=text
    endelse
   end

  'stitle' : begin
    if event.id eq stext then begin
     widget_control,event.id,get_value=text
     out_study.title=strmid(strtrim(text(0),2),0,80)
    endif else begin
     text=out_study.title
     xinput,text,$
      ['Enter study title [80 char max]',$
       'e.g. "Coronal Hole Boundary Study"'],group=event.top,$
     /modal,max_len=80,tfont=lfont
     out_study.title=text
     widget_control,stext,set_value=text
    endelse
   end

  'obs_prog' : begin
    if event.id eq sname then begin
     widget_control,event.id,get_value=text
     out_study.obs_prog=strmid(strtrim(text(0),2),0,8)
    endif else begin
     text=out_study.obs_prog
     xinput,text,$
      ['Enter study name abbreviation [8 char max]',$
       'e.g. "SYNOP"'],group=event.top,$
     /modal,max_len=8,tfont=lfont
     out_study.obs_prog=text
     widget_control,sname,set_value=text
    endelse
   end

;-- export study

  'export': begin
    ok_to_export=0
    if exist(out_study) then begin
     if (out_study.study_id gt -1) and (out_study.studyvar gt -1) then $
      ok_to_export=1
    endif
    if not ok_to_export then begin
     xack,'You must first CREATE and SAVE a study before exporting it.',group=event.top
    endif else begin
     if datatype(export_dir) ne 'STR' then begin
      export_dir=trim(getenv('CDS_EXPORT_W'))
      cd,curr=curr
      if export_dir eq '' then export_dir=curr
     endif
     xinput,export_dir,'Enter output directory for study definition export file:',$
      group=event.top,status=status,max_len=80
     if status then begin
      if not chk_dir(export_dir,out_dir) then begin
       xack,'Non-existent directory: '+export_dir,group=event.top
       return
      endif
      ok=test_open(export_dir,/write,/quiet,err=err)
      if not ok then begin
       xack,'No write access to: '+export_dir,group=event.top
       return
      endif
      export_dir=out_dir
      xtext,'Please wait. Exporting study to file...',/just_reg,wbase=tbase
      widget_control,/hour
      err=''
      export_study,out_study.study_id,out_study.studyvar,err=err,file=file,$
                  path=export_dir
      xkill,tbase
      if err ne '' then xack,err,group=event.top else $
       xack,'Study definition written to: '+file,group=event.top
     endif
    endelse
   end

;-- exit button

  'done'   : begin
    status=priv_zdbase(/quiet,/def)
    if exist(tried_and_failed) then begin
     if not tried_and_failed and status and exist(out_raster) and (out_study.study_id eq -1) and (out_study.studyvar eq -1) then begin
      instruct='Do you wish to save current study to DB'
      value=xanswer(instruct,/center,group=event.top)
      if value then goto,write
     endif
    endif
    xtext_reset,[stext,sname,svtext,n_repeat_r,xptext,yptext,id_text,var_text]
    xkill,event.top
    view_details=0
   end

;-- replace

  'replace' : begin
    if exist(sel_vraster) and exist(out_raster) and exist(sval) then begin
     out_raster(sval).ras_id=sel_vraster.ras_id
     out_raster(sval).ras_var=sel_vraster.ras_var
     mk_study_srlist
    endif
   end
    
;-- refresh 

  'reset' : begin 
    delvarx,out_raster,sel_praster,sel_vraster,sval,gdef,popt
    xhide,wvbase
    xhide,wbase
    def_cds_study,out_study
    mk_study_refresh
    mk_study_plist,proceed=proceed
    if proceed then mk_study_vlist
   end

;-- write new study

  'write'  : begin
    write: db=chklog('ZDBASE')
    if db eq '' then begin
     xack,'CDS database environment/logical variable is undefined',$
        /icon,group=event.top
     goto,quit
    endif

;-- check write priviledge 

    err=''
    status=priv_zdbase(/quiet,/def,err=err)
    if not status then begin
     xack,[err,'You do not have priviledge to modify current DB.',$
           'Try switching to your personal DB.'],group=event.top,/icon
     goto,quit
    endif

;-- any rasters?

    if not exist(out_raster) then begin
     xack,'You have not specified any rasters',$
             group=event.top,/icon
     goto,quit
    endif

;-- write fundamental and variation parts out (force user to enter titles)

    out_study=mk_cds_study(out_study,out_raster)
    nok=(strtrim(out_study.title,2) eq '') or $
        (strtrim(out_study.obs_prog,2) eq '') or $
        (strtrim(out_study.sv_desc,2) eq '')
    if nok then begin
     emess='Please complete the study TITLE and ACRONYM fields'
     xack,emess,space=1,/icon,group=event.top
     goto,quit
    endif

;-- check if fundamental study already exists. If so, then
;   use it's study_id number

    in_fdb=0 & in_vdb=0 & tried_and_failed=0
    fail_mess='STUDY_ID not updated in DB'
    check_mess='Now checking fundamental DB...'
    widget_control,info,set_value=check_mess
    widget_control,event.top,/hour
    err=''
    s1=add_f_study(out_study,err=err,study_id=dbstudy_id)
    if s1 eq 0 then begin
     widget_control,info,set_value=err,/app
     widget_control,info,set_value=fail_mess,/app
     if (dbstudy_id gt -1) then begin
      out_study.study_id=dbstudy_id 
      in_fdb=1
     endif else begin
      xack,[fail_mess,err],group=event.top,/icon,space=1
      tried_and_failed=1
      goto,quit
     endelse
    endif else begin
     write_mess='Fundamental DB successfully updated'
     widget_control,info,set_value=write_mess,/append
    endelse
    widget_control,sidtext,set_value=num2str(out_study.study_id)

;-- check TITLE_ID

    if out_study.title_id eq -1 then begin
     fail_mess='TITLE_ID not updated in DB'
     check_mess='Now checking title DB...'
     widget_control,info,set_value=check_mess,/app
     err=''
     s2=call_function('add_t_study',out_study,err=err,title_id=dbtitle_id)
     if s2 eq 0 then begin
      widget_control,info,set_value=err,/app
      widget_control,info,set_value=fail_mess,/app
      if (dbtitle_id gt -1) then out_study.title_id=dbtitle_id
     endif else begin
      write_mess='TITLE_ID DB successfully updated'
      widget_control,info,set_value=write_mess,/append
     endelse
    endif
    widget_control,tittext,set_value=num2str(out_study.title_id)

;-- add study variation

    fail_mess='STUDYVAR not updated in DB'
    check_mess='Now checking variation DB...'
    widget_control,info,set_value=check_mess,/app
    vstudy=out_study
    if out_study.n_raster_def gt 0 then begin
     nrast=n_elements(out_study.rasters)
     rvs=replicate({cds_v_st_ras,ras_var:0,n_repeat_r:0},nrast)
     copy_struct,out_study.rasters,rvs
     vstudy=rep_tag_value(vstudy,rvs,'RASTERS')
    endif
    err=''
    s2=add_v_study(vstudy,err=err,study=dbstudy)
    if s2 eq 0 then begin
     widget_control,info,set_value=err,/app
     widget_control,info,set_value=fail_mess,/app
     if n_elements(dbstudy) eq 2 then begin
      out_study.study_id=dbstudy(0)
      out_study.studyvar=dbstudy(1)
      in_vdb=1
     endif else begin
      xack,[fail_mess,err],group=event.top,/icon,space=1
      tried_and_failed=1
      goto,quit
     endelse
    endif else begin
     write_mess='Variation DB successfully updated'
     widget_control,info,set_value=write_mess,/append
    endelse
    comp_mess='Writing to DB completed'
    widget_control,info,set_value=comp_mess,/append
    if vstudy.studyvar gt -1 then out_study.studyvar=vstudy.studyvar 
    widget_control,svatext,set_value=num2str(out_study.studyvar)
    p1='' & p2=''
    if (in_fdb and in_vdb) then begin
     add_mess=out_study.obs_prog+' already in DB'
    endif else begin
     add_mess=[out_study.obs_prog+' successfully written to DB:          ']
     if in_fdb then p1=' (already in DB)' 
     if in_vdb then p2=' (already in DB)' 
    endelse
    add_mess=[add_mess,'    STUDY_ID = '+strtrim(string(out_study.study_id),2)+p1]
    add_mess=[add_mess,'    STUDYVAR = '+strtrim(string(out_study.studyvar),2)+p2]
    widget_control,svtext,set_value=out_study.sv_desc
    xack,add_mess,group=event.top
    relist_xstudy=1
   end

;-- remove selected raster

  'remove'  : begin
    if exist(out_raster) and exist(sval) then begin
     nrast=n_elements(out_raster)
     clook=where(sval ne indgen(nrast),cnt)    
     if cnt gt 0 then out_raster=out_raster(clook) else $
      delvarx,out_raster
;,sel_vraster,sel_praster
     mk_study_srlist
    endif
   end

;-- add selected raster (actually insert after last highlighted value)

  'add'  : begin
    if exist(sel_vraster) then begin
     ras_id=sel_vraster.ras_id
     ras_var=sel_vraster.ras_var
     temp={CDS_STUDY_RAS,ras_id:ras_id,ras_var:ras_var,pointing:1,ins_x:0.,ins_y:0.,n_repeat_r:1}
     nrast=n_elements(out_raster)
     if (exist(sval) eq 0) then sval=nrast-1
     if sval ge (nrast-1) then begin
      out_raster=concat_struct(out_raster,temp)
      sval=n_elements(out_raster)-1
     endif else begin
      part1=concat_struct(out_raster(0:sval),temp)
      out_raster=concat_struct(part1,out_raster(sval+1:*))
     endelse
     mk_study_srlist
    endif else begin
     widget_control,info,set_value='Please select a raster and variation to add',/append
    endelse
   end

;-- create new raster

  'create' : begin

    if exist(sel_praster) then ras_id=sel_praster.ras_id
    if exist(sel_vraster) then begin
     ras_id=sel_vraster.ras_id
     ras_var=sel_vraster.ras_var
    endif

;-- translate DB structure into MK_RASTER structure

    xtext,'Please wait. Loading MK_RASTER...',/just_reg,wbase=tbase,wait=3
    widget_control,event.top,/hour
    if exist(ras_id) and exist(ras_var) and (event.id eq tplanb2) then begin
     err=''
     rdef=get_cds_raster(ras_id,ras_var,err=err)
     if err eq '' then begin
      status=execute('gdef={TP_OBS_REC}')
      if not status then begin
       tplan_struct
       gdef={TP_OBS_REC}
      endif

;-- copy raster definitions

      copy_struct,rdef,gdef,/recur_to

     endif
      
    endif else delvarx,gdef

    mk_raster,gdef,group=event.top,/modal
    case cur_db of
     'USER': s=fix_zdbase(/user) 
     'CDS' : s=fix_zdbase(/cds)
     else:do_nothing=1
    endcase
    xshow,event.top
    if (xregistered('mk_raster') ne 0) then begin
     message,'MK_RASTER is running...',/contin
     xshow,get_handler_id('mk_raster')
     return
    endif else begin
     ras_id=gdef.raster_p.ras_id
     ras_var=gdef.raster_v.ras_var
     mk_study_plist,ras_id,proceed=proceed
     if proceed then mk_study_vlist,ras_id,ras_var
    endelse
   end

;-- help button

  'help'   : widg_help,'mk_study.hlp',group=event.top,/modal,$
              font='9x15bold',tfont='9x15bold',xtextsize=70

;-- view raster details

  'details'   : begin
    if event.select eq 0 then begin
     xhide,wvbase
     xhide,wbase
     view_details=0
    endif else view_details=1
   end
  else:  do_nothing=1

endcase

;-- list of primary rasters

if event.id eq prlist then begin
 xhide,wvbase
 sel_praster=prast(event.index)
 mk_study_vlist,sel_praster.ras_id
 widget_control,addb,sensitive=0
 widget_control,repb,sensitive=0
 delvarx,sel_vraster
endif

;-- list of raster variations

if event.id eq vrlist then begin
 sel_vraster=vrast(event.index)
 widget_control,addb,sensitive=1
 widget_control,repb,sensitive=1
 if not exist(out_raster) then begin
  zone_desc=strtrim(strupcase(strmid(str_pick(sel_vraster.rv_desc,'<','>'),0,4)),2)
  if zone_desc ne '' then begin
   out_study.zone_id=zone_def_id(zone_desc)
   widget_control,ztext,set_value=zone_def_label(out_study.zone_id)
  endif
 endif
endif

;-- new selected rasters

if (event.id eq srlist) and exist(out_raster) then begin
 if exist(sval) then begin
  osval=sval
  oras_id=out_raster(sval).ras_id
  oras_var=out_raster(sval).ras_var
  ofraster=get_cds_raster(oras_id,oras_var)
  ocds_det=ofraster.detector
 endif
 sval=event.index
 ras_id=out_raster(sval).ras_id
 ras_var=out_raster(sval).ras_var
 if exist(osval) then begin
  if (ras_id ne oras_id) or (ras_var ne oras_var) or (ocds_det ne cds_det) then begin 
   fraster=get_cds_raster(ras_id,ras_var)
   cds_det=fraster.detector
;   mk_study_plist,ras_id,proceed=proceed
;   if proceed then mk_study_vlist,ras_id,ras_var
  endif
  mk_study_srlist,/nolist
 endif
endif

;-- structure details

if view_details then begin
 if (bname eq 'details') or widg_type(event.id) eq 'LIST' then begin
  widget_control,event.top,tlb_get_size=wsize,tlb_get_off=woff
  if exist(sel_praster) then xstruct,sel_praster,nx=1,/just_reg,xsize=35,$
            wtags=wtags,wbase=wbase,xoff=woff(0)+.5*wsize(0),yoff=woff(1),$
            group=event.top
  if exist(sel_vraster) then xstruct,sel_vraster,/just_reg,xsize=35,nx=1,$
           group=event.top,$
           wtags=wvtags,wbase=wvbase,xoff=woff(0)+.5*wsize(0),yoff=woff(1)+.5*wsize(1)
 endif
endif

quit: if xalive(tplanb2) then widget_control,tplanb2,sensitive=exist(sel_vraster)
return & end

;============================================================================

pro mk_study_update

common mk_study_com0

out_raster=sel_study.rasters 
nrast=n_elements(out_raster)
def_cds_study,out_study,rast=nrast
copy_struct,sel_study,out_study
sval=0
ras_id=out_raster(0).ras_id
ras_var=out_raster(0).ras_var
fraster=get_cds_raster(ras_id,ras_var)
cds_det=fraster.detector
mk_study_plist,ras_id,proceed=proceed
if proceed then mk_study_vlist,ras_id,ras_var
mk_study_refresh

return & end

;============================================================================

pro mk_study_refresh              ;-- refresh mk_study widgets

common mk_study_com0

if datatype(out_study) eq 'STC' then begin
 for i=0,2 do widget_control,cbutt(i),set_button=0
 clook=where(strtrim(out_study.category,2) eq ['S','C','T'],cnt)
 if cnt gt 0 then widget_control,cbutt(clook(0)),set_button=1
 widget_control,stext,set_value=strtrim(out_study.title,2)
 widget_control,svtext,set_value=strtrim(out_study.sv_desc,2)
 widget_control,sname,set_value=strtrim(out_study.obs_prog,2)
 widget_control,sidtext,set_value=num2str(out_study.study_id)
 widget_control,tittext,set_value=num2str(out_study.title_id)
 widget_control,svatext,set_value=num2str(out_study.studyvar)
 widget_control,ztext,set_value=zone_def_label(out_study.zone_id)
endif

mk_study_srlist

return & end

;=========================================================================

pro mk_study_plist,ras_id,nolist=nolist,proceed=proceed  ;-- refresh primary raster list

common mk_study_com0

proceed=1
widget_control,vrlist,set_value='' 
widget_control,vrlist,sensitive=0 
delvarx,sel_vraster,sel_praster

if not exist(slit_no) then slit_no=0
widget_control,addb,sensitive=0
widget_control,repb,sensitive=0
widget_control,ibutt(0),set_button=0
widget_control,ibutt(1),set_button=0
widget_control,ibutt(cds_det eq 'G'),/set_button

if not keyword_set(nolist) then begin
 err=''
 list_f_raster,prast,nfound,detector=cds_det,err=err

;-- filter user selected SLIT number

 if slit_no gt 0 then begin
  slook=where(prast.slit_num eq slit_no,nfound)
  if nfound gt 0 then prast=prast(slook)
 endif

 if nfound gt 0 then begin
  widget_control,prlist,$
   set_value='RAS_ID:'+strtrim(string(prast.ras_id),2)+'/'+strtrim(prast.ras_desc,2)
  widget_control,prlist,/sensitive
 endif else begin
  widget_control,prlist,set_value='NO FUNDAMENTAL RASTERS'
  widget_control,prlist,sensitive=0
  proceed=0
  return
 endelse
endif

if exist(ras_id) then begin
 clook=where(ras_id eq prast.ras_id,cnt)
 if cnt gt 0 then begin
  widget_control,prlist,set_list_select=clook(0)
  sel_praster=prast(clook(0))
 endif
endif

return & end

;============================================================================

pro mk_study_vlist,ras_id,ras_var,nolist=nolist  ;-- refresh raster variation list

common mk_study_com0

if not keyword_set(nolist) and exist(ras_id) then begin
 list_v_raster,ras_id,vrast,nfound
 if nfound gt 0 then begin
  widget_control,vrlist,set_value='RAS_VAR: '+strtrim(string(vrast.ras_var),2)+' / '+strtrim(vrast.rv_desc,2)
  widget_control,vrlist,/sensitive
 endif else begin
  widget_control,vrlist,set_value='NO RASTER VARIATIONS'
  widget_control,vrlist,sensitive=0
  return
 endelse
endif else widget_control,vrlist,set_value=''

if exist(ras_var) then begin
 widget_control,addb,/sensitive
 widget_control,repb,/sensitive
 clook=where(ras_var eq vrast.ras_var,cnt)
 if cnt gt 0 then begin
  widget_control,vrlist,set_list_select=clook(0)
  sel_vraster=vrast(clook(0))
 endif
endif

return & end

;============================================================================

pro mk_study_srlist,nolist=nolist              ;-- refresh new raster list

common mk_study_com0

if exist(popt) eq 0 then popt='deferred'
for i=0,2 do widget_control,pbutt(i),set_button=0

if exist(out_raster) then begin
 nrast=n_elements(out_raster)
 ind=lindgen(nrast)

 if not keyword_set(nolist) then begin
  widget_control,srlist,set_value=strtrim(string(ind),2)+') '+$
  'RAS_ID: '+strtrim(string(out_raster.ras_id),2)+ $
           '/RAS_VAR: '+strtrim(string(out_raster.ras_var),2)
 endif

 if exist(sval) then begin
  sval= 0 > sval < (nrast-1)
  widget_control,srlist,set_list_select=sval
  widget_control,xptext,set_value=string(out_raster(sval).ins_x,'(f8.2)')
  widget_control,yptext,set_value=string(out_raster(sval).ins_y,'(f8.2)')
  widget_control,rptext,set_value=num2str(sval)
  widget_control,id_text,set_value=num2str(out_raster(sval).ras_id)
  widget_control,var_text,set_value=num2str(out_raster(sval).ras_var)
  widget_control,n_repeat_r,set_value=num2str(out_raster(sval).n_repeat_r)
  point=out_raster(sval).pointing
  clook=where(point eq [-1,0,1],cnt)
  if cnt gt 0 then begin
   widget_control,pbutt(clook(0)),/set_button
   widget_control,pcol1,sensitive=(clook(0) ne 2)
  endif
  widget_control,pbutt(0),sensitive=(sval gt 0)
 endif

 err=''
 get_raster_par,out_raster,sdur=dur,err=err
 widget_control,sdur,set_value=string(dur,'(f10.2)')
 widget_control,repb,/sensitive
 widget_control,remb,/sensitive
endif else begin
 widget_control,srlist,set_value=''
 widget_control,xptext,set_value=''
 widget_control,yptext,set_value=''
 widget_control,rptext,set_value=''
 widget_control,id_text,set_value=''
 widget_control,var_text,set_value=''
 widget_control,n_repeat_r,set_value='1'
 widget_control,sdur,set_value='0.0'
 widget_control,pbutt(0),sensitive=0
 widget_control,pbutt(2),/set_button
 widget_control,pcol1,sensitive=0
 widget_control,remb,sensitive=0
 widget_control,repb,sensitive=0
endelse

return & end

;============================================================================

pro mk_study,in_study,group=group,reset=reset,err=err,_extra=extra,modal=modal

common mk_study_com0

on_error,0

select_windows
err=''
if not have_widgets() then begin
 err='widgets are unavailable'
 message,err,/cont
 return
endif

caller=get_caller(stat)
if stat or (strpos(caller,'MK_STUDY') gt -1) then xkill,/all
if (xregistered('mk_study') ne 0) then begin
 xack,'Only one copy of MK_STUDY can run.'
 xshow,get_handler_id('mk_study')
 return
endif

delvarx,wbase,wtags,wvbase,wvtags
if keyword_set(reset) then reset=1 else reset=0

;-- check out DB

find_zdbase,cur_db,status=status,/def
if status eq 0 then return
message,'using DB from: '+get_environ('ZDBASE'),/contin

case cur_db of
 'USER': zmess='Using Personal CDS Database'
 'CDS' : zmess='Using Official CDS Database'
 else:   zmess='Using Unrecognized Database'
endcase

;-- did DB change?

reset=0
cur_db=getenv('ZDBASE')
if not exist(sav_db) then sav_db=cur_db
if cur_db ne sav_db then begin
 sav_db=cur_db & reset=1
endif

if reset then begin
 delvarx,out_study,in_study
 delvarx,out_raster,sel_praster,sel_vraster,sval,gdef,popt,sel_study
 dprint,'% MK_STUDY: resetting...'
endif

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

;-- create main base

main = widget_base(title ='CDS STUDY BUILDER',/column)

;-- row 1

row1=widget_base(main,/row)

exitb=widget_button(row1,value='Exit',font=bfont,uvalue='done',/no_release)

tplanb=widget_button(row1,value='Create New Rasters',/menu,/no_release,font=bfont)
tplanb1=widget_button(tplanb,value='Start from scratch',uvalue='create',/no_release,font=bfont)
tplanb2=widget_button(tplanb,value='Start with selected RAS ID/VAR as input',uvalue='create',/no_release,font=bfont)
widget_control,tplanb2,sensitive=exist(out_raster)

tmp=widget_button(row1,value='View/Select Study From DB',uvalue='views', $
                  /no_release,font=bfont)

helpb=widget_button(row1,value='Help',uvalue='help',/no_release,font=bfont)

row2=widget_base(main,/row)

refb=widget_button(row2,value='Reset Fields',uvalue='reset',/no_release,font=bfont)

tmp=widget_button(row2,value='View New Study',uvalue='dump', $
                  /no_release,font=bfont)

tmp=widget_button(row2,value='Save New Study',uvalue='write',$
                  /no_release,font=bfont)

tmp=widget_button(row2,value='Export New Study',uvalue='export',$
                  /no_release,font=bfont)

;-- row 2

;-- info window

m1='Use mouse to select from fundamental rasters and variations'
m2='Press Add to include highlighted raster and its variation'
info=widget_text(main,value=[m1,m2],ysize=4,/scroll,/frame,font=lfont,xsize=80)

;-- study titles 

row=widget_base(main,/column,/frame)

temp=widget_base(row,/row)

junk1=widget_base(temp,/row)
label=widget_button(junk1,value='STUDY TITLE:',uvalue='stitle',font=bfont,/no_rel)
stext=widget_text(junk1,value='',xsize=20,font=lfont,/edit,/all,uvalue='stitle')

junk2=widget_base(temp,/row)
label=widget_button(junk2,value='STUDY ACRONYM:',font=bfont,uvalue='obs_prog',/no_rel)
sname=widget_text(junk2,value='',xsize=8,font=lfont,/edit,/all,uvalue='obs_prog')

temp=widget_base(row,/row)

junk1=widget_base(temp,/row)
label=widget_button(junk1,value='STUDY_VAR DESC:',uvalue='svtitle',font=bfont,/no_rel)
svtext=widget_text(junk1,value='',xsize=20,font=lfont,/edit,/all,uvalue='svtitle')

;-- ZONE ID stuff

junk2=widget_base(temp,/row)

source=find_with_def('newdata_selection.pdm',!path)
if trim(source) eq '' then $
 source=find_with_def('newdata_selection.pdm','CDS_PLAN_TECH')
if trim(source) ne '' then begin
 xpd=rd_ascii(source(0))
 xpd(0)='"ZONE_ID:" {'
 xpdmenu,xpd,junk2,font=bfont
endif else zbutt=widget_label(junk2,value='NO ZONE FILE',/frame,font=lfont)

ztext=widget_text(junk2,value='',xsize=20,font=lfont)

row3=widget_base(main,/row)

col1=widget_base(row3,/column,/frame)


;-- detector choice

row=widget_base(col1,/row)

temp=widget_base(row,/row,/frame)
label=widget_label(temp,value='DETECTOR:',font=lfont)
xmenu,['NIS','GIS'],temp,/row,/exclusive,font=lfont,$
      uvalue=['NIS','GIS'],buttons=ibutt,/no_release

;-- slit choice

temp=widget_base(row,/row,/frame)
label=widget_label(temp,value='SLIT:',font=lfont)

slit_choices=['ALL SLITS   ',$
       '1 - (2x2)   ',$
       '2 - (4x4)   ',$
       '3 - (8x51)  ',$
       '4 - (2x240) ',$
       '5 - (4x240) ',$
       '6 - (90x240)']

new_vers=float(strmid(!version.release,0,3)) ge 4.
if new_vers then slitb=call_function('widget_droplist',temp,$
 value=slit_choices,uvalue='pick_slit',font=bfont) else $
  slitb = cw_bselector2(temp, slit_choices,/no_rel,$
   /return_index, uvalue='pick_slit',font=bfont)



;-- line list choice

;temp=widget_base(row,/row,/frame)
;label=widget_label(temp,value='Line List Choice:',font=lfont)


;-- get primary rasters

temp=widget_base(col1,/row,/frame)
label=widget_label(temp,$
       value='FUNDAMENTAL RASTERS IN DATABASE:',font=lfont)
prlist=widget_list(col1,value=' ',ysize=5,font=lfont)

;-- list of raster variations

temp=widget_base(col1,/row,/frame)
label=widget_label(temp,$
      value='RASTER VARIATIONS FOR HIGHLIGHTED FUNDAMENTAL RASTER:',font=lfont)
vrlist=widget_list(col1,value='',ysize=5,font=lfont)

;-- study names/id's

row=widget_base(col1,/column,/frame)
zlabel=widget_label(row,value=strupcase(zmess),font=lfont)
temp=widget_base(row,/row)
temp1=widget_base(temp,/row)
label=widget_label(temp1,value='STUDY_ID :',font=lfont)
sidtext=cw_text(temp1,value='   ',xsize=3,font=lfont,edit=0)
temp1=widget_base(temp,/row)
label=widget_label(temp1,value='TITLE_ID :',font=lfont)
tittext=cw_text(temp1,value='   ',xsize=3,font=lfont,edit=0)
temp1=widget_base(temp,/row)
label=widget_label(temp1,value='STUDY_VAR:',font=lfont)
svatext=cw_text(temp1,value='   ',font=lfont,xsize=3,edit=0)

xmenu,['Science','Calibration','Test'],row,/row,/exclusive,/no_release,buttons=cbutt,$
      uvalue=['S','C','T'],font=lfont

;-- raster/instrument details

xmenu,['View raster details'],row,/column,/nonexclusive,$
      uvalue=['details'],buttons=rbutt,font=lfont
view_details=0

;-- list of selected raster/variations

col2=widget_base(row3,/column,/frame)

;-- column 2

rowb=widget_base(col2,row=2,/frame)
row1=widget_label(rowb,value=strupcase('Raster Manipulation Buttons:'),font=lfont)

row2=widget_base(rowb,/row)
addb=widget_button(row2,value='Add ',uvalue='add',/no_release,font=bfont)
remb=widget_button(row2,value='Remove',uvalue='remove',/no_release,font=bfont)
repb=widget_button(row2,value='Replace',uvalue='replace',/no_release,font=bfont)


temp=widget_base(col2,/frame,/row)
label=widget_label(temp,value='RASTERS DEFINED FOR STUDY:',font=lfont)
srlist=widget_list(col2,value='',ysize=8,font=lfont)

;-- current raster #

rast=widget_base(col2,/column)
temp1=widget_base(rast,/row)
r1=widget_base(temp1,/row)
rindex=widget_label(r1,value=strupcase('Current raster #'),font=lfont)
rptext=widget_text(r1,value='   ',xsize=3,font=lfont)

r2=widget_base(temp1,/row)
rindex_id=widget_label(r2,value=strupcase('ID'),font=lfont)
id_text=widget_text(r2,value='   ',xsize=3,font=lfont)

r3=widget_base(temp1,/row)
rindex_var=widget_label(r3,value=strupcase('Var'),font=lfont)
var_text=widget_text(r3,value='   ',xsize=3,font=lfont)


nrepb=widget_base(rast,/row)
junk=widget_label(nrepb,value=strupcase('Repeat current raster'),font=lfont)
n_repeat_r=widget_text(nrepb,value='1',xsize=4,/edit,font=lfont)
junk=widget_label(nrepb,value='TIMES',font=lfont)

temp=widget_base(rast,/row)
junk=widget_label(temp,value=strupcase('Study duration (secs)'),font=lfont)
sdur=widget_text(temp,value='     ',xsize=10,font=lfont)

;-- pointing

pnt=widget_base(col2,/column,/frame)

pl=widget_label(pnt,value='POINTING MODE',font=lfont)
popts=['offset','fixed','deferred']
xmenu,['Offset','Fixed','Deferred'],pnt,/row,/exclusive,buttons=pbutt,$
      uvalue=popts,/no_release,font=lfont,frame=0

pcol1=widget_base(pnt,/column)

temp2=widget_base(pcol1,column=2)
junk=widget_base(temp2,row=2)
lab=widget_label(junk,value= 'X (ARCSEC):    ',font=lfont)
lab2=widget_label(junk,value='(+WEST/-EAST)  ',font=lfont)
xptext=widget_text(temp2,value='',/editable,/all,xsize=8,font=lfont)

temp2=widget_base(pcol1,column=2)
junk=widget_base(temp2,row=2)
lab=widget_label(junk,value= 'Y (ARCSEC):    ',font=lfont)
lab2=widget_label(junk,value='(+NORTH/-SOUTH)',font=lfont)
yptext=widget_text(temp2,value='',/editable,/all,xsize=8,font=lfont)

;-- did user enter a study on command line?

if (datatype(in_study) eq 'STC') then begin
 if tag_exist(in_study,'RASTERS') then out_study=in_study
endif
if datatype(out_study) ne 'STC' then def_cds_study,out_study
delvarx,out_raster
if tag_exist(out_study,'RASTERS') then begin
 if tag_exist(out_study.rasters,'RAS_VAR') then out_raster=out_study.rasters 
endif
def_cds_study,temp
copy_struct,out_study,temp & out_study=temp

;-- which detector

if not exist(cds_det) then cds_det='N'
if exist(out_raster) then begin
 sval=0
 ras_id=out_raster(0).ras_id
 ras_var=out_raster(0).ras_var
 fraster=get_cds_raster(ras_id,ras_var)
 if tag_exist(fraster,'DETECTOR') then cds_det=fraster.detector
endif

;-- refresh list widgets

mk_study_plist,ras_id,proceed=proceed
if proceed then mk_study_vlist,ras_id,ras_var
mk_study_refresh

;-- realize and manage

widget_control,main,/realize                       

expr="xmanager,'mk_study',main,modal=modal,group=group"
if (idl_release(lower=5,/incl)) and (n_params() ne 1) then $
 expr=expr+',/no_block'
s=execute(expr)
xmanager_reset,main,modal=modal,group=group,crash='mk_study'

;-- return new study

if not xalive(main) then begin
 study_id=out_study.study_id
 studyvar=out_study.studyvar
 title_id=out_study.title_id
 out_study=mk_cds_study(out_study,out_raster)
 out_study.study_id=study_id
 out_study.studyvar=studyvar
 out_study.title_id=title_id
 in_study=out_study
endif

return & end


