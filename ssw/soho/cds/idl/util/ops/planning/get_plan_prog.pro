;+
; Project     : SOHO - CDS
;
; Name        : GET_PLAN_PROG
;
; Purpose     : return program names for set of plans
;
; Category    : planning
;
; Syntax      : IDL> progs=get_plan_prog(details)
;
; Inputs      : DETAILS = detailed plan entries
;
; Outputs     : PROGS = string array of program names
;
; Keywords    : ERR = error string
;                                                                        
; History     : Written,  14-May-2001, Zarro (EITI/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_plan_prog,details,err=err

err=''

if not exist(details) then return,''
type=get_plan_type(details)
if type ne 0 then return,''

ndetails=n_elements(details)
progs=strarr(ndetails)
for i=0,ndetails-1 do begin
 splan=get_cds_study(details(i).study_id,details(i).studyvar)
 if have_tag(splan,'obs_prog') then progs(i)=splan.obs_prog
endfor

return,trup(progs) & end

