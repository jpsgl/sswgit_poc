;+
; Project     : SOHO - CDS
;
; Name        : RD_CDS_POINT
;
; Purpose     : Read CDS pointings for specified times
;
; Category    : planning
;
; Explanation : Read planned or ASRUN database
;
; Syntax      : IDL> plans=rd_cds_point(tstart,tend)
;
; Inputs      : TSTART = plot start time [def=current date]
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Outputs     : PLANS = plan structures stored as MAPS
;
; Opt. Outputs: None
;
; Keywords    : ASRUN = plot actual pointing based on CATALOG
;               QUIET = switch off message outputs
;               EXCLUDE = study acronyms to exclude, e.g [SYNOP, FULLCCD]
;               DEF_EXCLUDE = exclude default studies (engineering, SYNOP)
;               NOENG = exclude engineering studies
;               ROTATE = solar rotate pointings to TIME
;               TIME = time to rotate pointings to
;               COUNT = # of plans found
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-May-1998,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function rd_cds_point,tstart,tend,err=err,exclude=exclude,$
     def_exclude=def_exclude,rotate=rotate,$
     asrun=asrun,time=time,noeng=noeng,quiet=quiet,count=count

on_error,1

;-- default studies to exclude

def_prog=['synop','eng','fullccd','nimcp','gimcp','fsun','usun']
count=0
quiet=keyword_set(quiet)
loud=1-quiet

;-- check time inputs

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then get_utc,t1
t1.time=0

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
endif
t2.time=0

asrun=keyword_set(asrun)
planned=1-asrun

;-- make pointing structures

if planned then cds_plan_point,plans,t1,t2,count=count else $
 cds_asrun_point,plans,t1,t2,count=count
if (count eq 0) or not exist(plans) then begin
 message,'No CDS plans found for input times',/cont
 count=0
 return,-1
endif

if loud then message,'building pointing structures...',/cont

;-- check what studies to exclude

tplans=plans
if (datatype(exclude) eq 'STR') or keyword_set(def_exclude) then begin
 if datatype(exclude) ne 'STR' then exclude=def_prog
 for i=0,n_elements(exclude)-1 do begin
  d=grep(exclude(i),tplans.id,/exclude,index=index)
  if min(index) eq -1 then begin
   err='warning, all studies excluded from plot'
   message,err,/cont
   count=0
   return,-1
  endif
  tplans=tplans(index)
 endfor
endif

;-- solar rotate to input time (only deferred pointing studies)

rplans=tplans
if exist(time) and keyword_set(rotate) then begin
 have_fixed=tag_exist(tplans,'fixed')
 if have_fixed then begin
  no_rotate=where(tplans.fixed,are_fixed)
  do_rotate=where(tplans.fixed eq 0,not_fixed)
  if are_fixed gt 0 then fixed_plans=tplans(no_rotate)
  if not_fixed gt 0 then $
   rot_plans=drot_map_fast(tplans(do_rotate),time=time,/no_rtime)
  rplans=merge_struct(fixed_plans,rot_plans)
 endif
endif

count=n_elements(rplans)
return,rplans & end

