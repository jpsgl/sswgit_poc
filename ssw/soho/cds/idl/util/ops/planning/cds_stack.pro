;+
; Project     : SOHO - CDS
;
; Name        : CDS_STACK
;
; Purpose     : store CDS QL structures in a pointer stack
;
; Category    : CDS planning
;
; Syntax      : IDL> cds_stack,ql,[/set,/get,file=file,/clear]
;
; Inputs      : QL = CDS QL data structure (if /GET)
;
; Outputs     : QL = retrieved structure (if /SET and FILE)
;
; Keywords    : SET = insert QL in stack
;               GET = retrieve filename in keyword FILE from stack
;               CLEAR = empty stack
;               FILE = string filename to retrieve
;               STATUS =1/0 if success/failure
;               MAX_SIZE= maximum size of stack [def=25]
;
; History     : Version 1,  1-Sept-1999,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-


pro free_stack,stack                 ;-- free-up stack memory

if datatype(stack) eq 'STC' then begin
 if tag_exist(stack,'pointer') then begin
  nstack=n_elements(stack)
  for i=0,nstack-1 do begin
   temp=get_pointer(stack(i).pointer,/no_copy)
   delvarx,temp
   free_pointer,stack(i).pointer
  endfor
  delvarx,stack 
 endif
endif
return & end

;------------------------------------------------------------------------------
            
pro cds_stack,ql,clear=clear,file=file,set=set,get=get,status=status,$
              max_size=max_size

common cds_stack,stack
status=0b
stack_exists=datatype(stack) eq 'STC'

;-- clear stack 

if keyword_set(clear) then begin
 free_stack,stack
 status=1b
 return
endif
            
;-- insert into stack

curr_size=n_elements(stack)
if not exist(max_size) then max_size=25

if keyword_set(set) then begin
 qlmgr,ql,valid
 if not valid then return
 file=trim(get_tag_value(ql,/filename))
 break_file,file,dsk,dir,name,ext
 name=trim(name)
 in_stack=0b

 if stack_exists then begin
  slook=where(stack.file eq name,count)
  in_stack=count gt 0 
 endif

;-- if already in stack, just update pointer value and return

 if in_stack then begin
  ptr=stack(slook(0)).pointer 
  set_pointer,ptr,ql
  status=1b
  return
 endif

;-- if not in stack, append a new record if max_size is not reached
;-- if max_size is reached, recycle pointer from first record to conserve
;-- memory

 recycle=curr_size ge max_size
 if recycle then ptr=stack(0).pointer else make_pointer,ptr
 if valid_pointer(ptr) then begin
  set_pointer,ptr,ql
  record={cds_stack,file:name,pointer:ptr}

  if recycle then begin 
   nstart=curr_size-max_size+1
   if nstart gt 1 then begin
    old_stack=stack(1:nstart-1)
    free_stack,old_stack
   endif
   stack=stack(nstart:curr_size-1)
  endif

  stack=merge_struct(stack,record)
  dprint,'% CDS_STACK: stack size = '+num2str(n_elements(stack))   
  status=1b
 endif
 return
endif
 
;-- retrieve from stack

if keyword_set(get) and (datatype(file) eq 'STR') and stack_exists then begin
 break_file,file,dsk,dir,name,ext
 name=trim(name)
 slook=where(stack.file eq name,count)
 if count gt 0 then begin
  ql=get_pointer(stack(slook(0)).pointer,status=status)
 endif
endif
if not status then delvarx,ql else $
 dprint,'% CDS_STACK: '+file+' retrieved from stack.'
 
return & end

