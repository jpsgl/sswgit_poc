;+
; Project     : SOHO - CDS
;
; Name        : GET_GIS_DELAY
;
; Purpose     : to get CDS GIS LUT delay
;
; Category    : database, planning
;
; Explanation : 
;
; Syntax      : IDL> delay=get_gis_delay(gset_id)
;
; Inputs      :	GSET_ID = gset_id id
;
; Opt. Inputs :	None.
;
; Outputs     :	See keywords.
;
; Opt. Outputs:	None.
;
; Keywords    :	ERR = error string
;
; Common      : GET_GIS_DELAY - misc state DB stuff
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  1-Jan-1996,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function get_gis_delay,gset_id,err=err

on_error,1

common get_gis_delay,ss

gis_delay=0.
if not have_proc('cp_get_entry') then return,gis_delay

if not exist(ss) then ss=call_function('cp_get_entry','GISLTDLY', [0])
err=''
get_gset,gset_id,gset_def,err=err
if err eq '' then begin
 dets_used=where(gset_def.det_used eq 1,dcount)
 if dcount gt 0 then gis_delay=ss.active*dcount/1000.
endif

return,gis_delay & end

