;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN
;
; Purpose     : Shell program to run MK_CDS_PLAN or MK_SUMER_PLAN
;
; Use         : MK_PLAN
;
; Inputs      : TSTART, TSTOP = start, stop times to create plan
;               (can be string [e.g. 96/1/1] or TAI format)
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : Inherited from lower level routines
;
; Explanation : A wrapper routine around MK_PLAN_CDS and MK_CDS_SUMER
;
; Calls       : Many routines
;
; Common      : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Widgets, planning
;
; Prev. Hist. : None.
;
; History     : Version 1, Zarro (ARC/GSFC) 19 April 1995, written
;               Version 2, Liyun Wang, GSFC/ARC, August 30, 1995
;                  Renamed from MK_DETAIL
;-

pro mk_plan, tstart, tend, _extra=extra,test=test

if which_inst() eq 'C' then begin
  qsave=!quiet
  !quiet=1
  if keyword_set(test) then mk_cds_dir
  caller=get_caller(status)
  nokill=(status eq 0)
  mk_cds_plan,tstart,_extra=extra,nokill=nokill
  if keyword_set(test) then mk_cds_dir,/reset
  !quiet=qsave
endif

if which_inst() eq 'S' then mk_sumer_plan, tstart, _extra=extra

return & end

