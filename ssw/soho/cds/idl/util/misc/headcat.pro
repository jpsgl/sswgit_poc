;+
; Project     : SOHO - CDS     
;                   
; Name        : HEADCAT
;               
; Purpose     : Loads CDS header catalogue
;               
; Explanation : 
;               
; Use         : IDL> headcat,  cat
;    
; Inputs      : None
;               
; Opt. Inputs : None
;               
; Outputs     : cat - returns the catalogue in a structure array. If the YEAR 
;                     keyword is not used then the total catalogue is loaded.
;               
; Opt. Outputs: None
;               
; Keywords    : HELP   - only list available tags
;               YEAR   - load only this year's data
;               LATEST - load only catalogue of last 7 days
;
; Calls       : None
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: None
;               
; Category    : Help
;               
; Prev. Hist. : None
;
; Written     : C D Pike,  RAL,  10-May-97
;               
; Modified    : Include year 1999 option
;               Include year 2000 option
;               Include year 2001 option
;               Include year 2002 option
;               Include year 2003 option
;               Include year 2004 and beyond option
;
; Version     : Version 7, 05-Jan-2004
;-            

pro headcat,  cat,  help=help, year=year, latest=latest

if keyword_set(help) or n_params() eq 0 then begin
   print,  'Available structure tags are...'
   print,  'ANGLE, CATEGORY, CMP_NAME, CMP_NO, COMMENTS, COMP_ERR, COMP_ID'
   print,  'COMP_OPT, DATASRC, DATE, DATE_END, DATE_OBS, DETECTOR, DEXSIZE'
   print,  'DW_ID, EXPCOUNT, EXPTIME, FILENAME, GSET_ID, HEL_LAT, HEL_LONG'
   print,  'INS_ROLL, INS_X0, INS_Y0'
   print,  'IXWIDTH, IYWIDTH, LL_ID, MIR_POS, NWINDOWS, NX, NY'
   print,  'OBJECT, OBJ_ID, OBS_PROG, OBT_END, OBT_TIME, OPS_L, OPS_R'
   print,  'PROG_NUM, RADIUS, RAS_ID, RAS_VAR, SCI_OBJ, SCI_SPEC'
   print,  'SEQ_IND, SER_ID, SLIT_NUM, SLIT_POS, STUDYVAR, STUDY_ID, TITLE'
   print,  'TITLE_ID, TRACKING, VALID, VDS_ACC, VDS_MODE, VDS_PMCP, VDS_ORI'
   print,  'WAVE, XCEN, XSTEP, YCEN, YSTEP'
   print,' '
   return
endif

;
;  initialise the variables to be retrieved.
;
cat = 0

;
;  pickup the latest weekly data if requested
;
if keyword_set(latest) then begin
   file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.weekly')
   if file_exist(file) then begin
      print, 'Restoring weekly CDS FITS header catalogue....'
      restore, file
   endif else begin
      print,'Header catalogue file does not exist ('+file+')'
   endelse
   return
endif 

;
;  restore the correct year file
;
;  default is all
;
if n_elements(year) eq 0 then year = 0

yr = year
if year ge 2000 then begin
   yr = year - 2000
endif else begin
   if year gt 1900 then yr = year - 1900
endelse

case 1 of
   (yr eq 96): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.96')
           if file_exist(file) then begin
              print, 'Restoring 1996 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat96)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 97): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.97')
           if file_exist(file) then begin
              print, 'Restoring 1997 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat97)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 98): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.98')
           if file_exist(file) then begin
              print, 'Restoring 1998 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat98)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 99): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.99')
           if file_exist(file) then begin
              print, 'Restoring 1999 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat99)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 0): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.00')
           if file_exist(file) then begin
              print, 'Restoring 2000 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat00)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 1): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.01')
           if file_exist(file) then begin
              print, 'Restoring 2001 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat01)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 2): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.02')
           if file_exist(file) then begin
              print, 'Restoring 2002 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat02)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 3): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.03')
           if file_exist(file) then begin
              print, 'Restoring 2003 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat03)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 4): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.04')
           if file_exist(file) then begin
              print, 'Restoring 2004 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat04)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 5): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.05')
           if file_exist(file) then begin
              print, 'Restoring 2005 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat05)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 6): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.06')
           if file_exist(file) then begin
              print, 'Restoring 2006 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat06)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   (yr eq 7): begin
           file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.07')
           if file_exist(file) then begin
              print, 'Restoring 2007 CDS FITS header catalogue....'
              restore, file
              cat = temporary(cat07)
           endif else begin
              print,'Header catalogue file does not exist ('+file+')'
              cat = -1
           endelse
   end 
   else:   begin
              bell
              print,'Sorry, cannot load complete catalogue'
              file = concat_dir('$CDS_HEAD_CAT', 'cds_header_cat.98')
              if file_exist(file) then begin
                 print, 'Restoring 1998 CDS FITS header catalogue....'
                 restore, file
                 cat = temporary(cat98)
              endif else begin
                 print,'Header catalogue file does not exist ('+file+')'
                 cat = -1
              endelse
           end
endcase

end
