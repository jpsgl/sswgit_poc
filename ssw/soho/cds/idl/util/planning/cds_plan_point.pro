;+
; Project     : SOHO - CDS
;
; Name        : CDS_PLAN_POINT
;
; Purpose     : create map structures for planned CDS pointings
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : IDL> cds_plan_point,cmap,tstart,tend
;
; Inputs      : TSTART = plot start time [def=current date]
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # of pointings found
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-May-1998,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro cds_plan_point,cmap,tstart,tend,count=count,err=err

on_error,1

err=''

;-- save original ZDBASE

status=fix_zdbase(/original,err=err)
if not status then begin
 message,err,/cont
 return
endif

;-- error trap

error=0
catch,error
if error ne 0 then begin
 err='An error occurred'
 message,err,/cont
 goto,bailout
endif

;-- go to official ZDBASE

status=fix_zdbase(/cds,err=err)
if not status then begin
 message,err,/cont
 goto,bailout
endif

;-- check time inputs

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then begin
 get_utc,t1
 t1.time=0
endif

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
 t2.time=0
endif

rd_plan,plans,t1,t2,nobs=count,type=0
if count eq 0 then begin
 message,'No detailed plans during specified times',/cont
 return
endif

;-- make pointing structures

delvarx,cmap
for i=0,count-1 do begin
 study=get_cds_study(plans(i).study_id,plans(i).studyvar)
 get_cds_xy,plans(i),x,y,/one
 get_cds_fov,plans(i),width,height,/one
 time=anytim2utc(plans(i).date_obs,/vms)
 eng=study.n_raster_def eq 0
 npoints=n_elements(x)
 for k=0,npoints-1 do begin
  temp=make_map(fltarr(2,2),xc=x(k),yc=y(k),dx=width(k)/2.,dy=height(k)/2.,$
                 time=time,/soho,id=study.obs_prog,eng=eng)
  if exist(cmap) then cmap=merge_struct(cmap,temp) else cmap=temp
 endfor
endfor

;-- restore original ZDBASE

bailout: status=fix_zdbase(/original)

return & end

