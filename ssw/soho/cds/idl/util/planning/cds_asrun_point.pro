;+
; Project     : SOHO - CDS
;
; Name        : CDS_ASRUN_POINT
;
; Purpose     : create map structures for ASRUN CDS pointings from catalog
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : IDL> cds_asrun_point,cmap,tstart,tend
;
; Inputs      : TSTART = plot start time [def=current date]
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : COUNT = # of pointings found
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-May-1998,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro cds_asrun_point,cmap,tstart,tend,count=count,err=err

on_error,1

err=''

;-- save original ZDBASE

status=fix_zdbase(/original,err=err)
if not status then begin
 message,err,/cont
 return
endif

;-- error trap

error=0
catch,error
if error ne 0 then begin
 err='An error occurred'
 message,err,/cont
 goto,bailout
endif
;-- go to official ZDBASE

status=fix_zdbase(/cds,err=err)
if not status then begin
 message,err,/cont
 goto,bailout
endif

;-- check time inputs

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then begin
 get_utc,t1
 t1.time=0
endif

err=''
t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
 t2.time=0
endif

list_exper,t1,t2,plans,count
if count eq 0 then begin
 message,'No ASRUN plans during specified times',/cont
 return
endif

;-- make pointing structures

delvarx,cmap
for i=0,count-1 do begin
 time=anytim2utc(plans(i).date_obs,/vms)
 temp=make_map(fltarr(2,2),xc=plans(i).xcen,yc=plans(i).ycen,$
               dx=plans(i).ixwidth/2.,dy=plans(i).iywidth/2.,$
               time=time,/soho,id=plans(i).obs_prog)
 if exist(cmap) then cmap=merge_struct(cmap,temp) else cmap=temp
endfor

;-- restore original ZDBASE

bailout: status=fix_zdbase(/original)

return & end

