;+
; Project     : SOHO - CDS     
;                   
; Name        : CDS_TILT_COR
;               
; Purpose     : Produce tilt corrected CDS wavelength scale
;               
; Category    : Analysis
;               
; Explanation : 
;               
; Syntax      : IDL> wave=cds_tilt_cor(a,window)
;    
; Examples    : 
;
; Inputs      : A = QL structure to read
;               WINDOW = QL window to select
;               
; Opt. Inputs : None
;               
; Outputs     : WAVE = tilt corrected wavelength
;
; Opt. Outputs: None
;               
; Keywords    : WINDOW = window to fit (input)
;               CALIB = set to calibrate spectra before fitting
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: None
;               
; History     : Version 1,  17-Jan-1998,  D M Zarro.  Written
;               Based upon SETUPAN by SVH
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-            


function cds_tilt_cor,a,window

if not exist(window) then win=gt_cds_window(a) else win=window

if win lt 0 then return,-1

da = gt_windata(a,win)
sz = size(da)

if sz(0) eq 4 then begin
   detx = a.detdesc(win).detx + dimrebin(lindgen(sz(1),1,1,1),sz(1:4))
   dety = a.detdesc(win).dety + dimrebin(lindgen(1,1,sz(3),1),sz(1:4))
end else begin
   detx = a.detdesc(win).detx + dimrebin(lindgen(sz(1),1,1),sz(1:3))
   dety = a.detdesc(win).dety + dimrebin(lindgen(1,1,sz(3)),sz(1:3))
end

;; Which detector are we using?

nis = a.header.detector EQ 'NIS' 

if nis then begin

; Calculate tilt for all pixels

 time=gt_start(a,/vms) 
 det = (['N1','N2'])(a.detdesc(win).dety LT 512)
 if det eq 'N1' then begin
  dcoeff=[0.0075942d, -6.135603e-6]
  err=''
  get_tiltcal,1,time,coeff,err=err
  if err ne '' then coeff=dcoeff
 endif else begin
  dcoeff=[0.00397,-4.763135e-6,4.764016e-9]
  get_tiltcal,2,time,coeff,err=err
  if err ne '' then coeff=dcoeff
 endelse
 med_dety = median(dety(0,0,*))
endif else begin
 tilt=0 & dety=0.
 det = (['G1','G2','G3','G4'])(detx(0)/2048) 
 med_dety=0.
endelse
tilt = poly(coeff,detx)

;; Find the median detector y position - use as zero point for tilt correction

ndetx = detx + (dety-med_dety) * tilt
return,float(pix2wave(det,ndetx,/nolimit))

end

