pro get_eit_data,date,rem_machine,rem_name,rem_dir,local_only=local_only
;+
; NAME:
;	get_eit_data
;
; PURPOSE:
;	This procedure copies all full field images from specified
;   date to either local directory or FTP's to remote machine.
;
; CALLING SEQUENCE:
;       get_eit_data,date,['remote-machine'],['remote-login'],['remote_dir',]
;           [,/local]
;
; INPUTS:  
;    date            specified day of interest, will use LZ data if available
;    rem_machine     remote machine name for FTP
;    rem_name        remote login name for FTP
;    rem_dir         remote directory name for FTP
;    local           set if only want to copy files to local directory
;
; KEYWORD PARAMETERS: None
;
; OUTPUTS:
;	Copies files to either local directory or remote machine
;
; EXAMPLES:
;       Copy all EIT full field images from Jan 3, 1997 to my subdirectory
;       data:
;         get_eit_data,'3-jan-97','eitv2.nascom.nasa.gov','newmark','data'
;
; COMMON BLOCKS: none.
;
; SIDE EFFECTS:
;
; CALLS: eit_catrd
;
; MODIFICATION HISTORY:
; 	Written by:  J. Newmark 	Date.  Jan 1997
;-

utc_lz = anytim2utc('25-nov-96')
utc_date = anytim2utc(date)
if utc_date.mjd le utc_lz.mjd then begin
  lz = 1 
  template = 'efz'
endif else begin
  lz = 0
  template = 'efr'
endelse 
fnames = ''
files = eit_catrd(object='full FOV',date,lz=lz,file=fnames)
if files(0) eq '' and lz then begin
    template = 'efr'
    files = eit_catrd(object='full FOV',date,file=fnames)
endif
path = eit_file2path(fnames(0))
path = strmid(path,0,strpos(path,template))
numf = n_elements(fnames)
ans = ''
if keyword_set(local_only) then begin
  cd, path,current = current
  if !version.os eq 'vms' then cp_cmd = 'copy ' else cp_cmd = 'cp '
  for i = 0,numf-1 do spawn, cp_cmd + fnames(i) + ' ' + current
  cd, current
  print, 'Transferred files to your local directory'
endif else begin
  read, ans,prompt='Will transfer '+strtrim(numf,2)+' files, continue(y/n)?'
  if strlowcase(ans) eq 'y' then begin

    if n_elements(rem_machine) eq 0 then begin
      rem_machine = ''
      read, rem_machine,prompt='Please enter your complete remote machine name: '
    endif
    if n_elements(rem_name) eq 0 then begin
      rem_name = ''
      read, rem_name,prompt='Please enter your remote machine login name: '
    endif
    if n_elements(rem_dir) eq 0 then begin
      rem_dir = ''
      read, rem_dir,prompt='Please enter your remote directory name: '
    endif

    outfile = 'ftp_eit.'+str_replace(anytim2utc(date,/ecs,/date),'/','')
    openw,lun,outfile,/get_lun
    printf,lun,'open '+rem_machine
    printf,lun,rem_name
    printf,lun,'binary'
    printf,lun,'prompt'
    printf,lun,'cd '+rem_dir
    printf,lun,'lcd '+ path
    for i = 0,numf-1 do printf,lun,'put '+fnames(i)
    printf,lun,'quit'
    free_lun,lun

    print,'At FTP prompt enter your PASSWORD as appropriate'

    spawn,'ftp < '+ outfile
    print,'Transferred files'

  endif else print,'No files transferred'
endelse

end
