pro lasteit, index, data, number, $
        w171=w171, w195=w195, w284=w284, w304=w304, wall=wall, wave=wave, $
        norebin=norebin, notv=notv, rebin=rebin, 	       $  ;
	uleft=uleft, uright=uright, lleft=lleft, lright=lright, $
	noscale=noscale, refresh=refresh, fill_cube=fill_cube, $
        quality=quality, prepit=prepit, $
        deriv_hist=deriv_hist, sobel_weight=sobel_weight       ; <<sobel_scale 
;+
;   Name: lasteit
;
;   Purpose: read and display most recent EIT images
;
;   Input Parameters:
;      number - number of images to read (default=1)
;
;   Output Parameters:
;      info - EIT instrument parameter (string array, 1 per image) 
;      data - EIT image or image cube
;
;   Keyword Parameters:
;      /w171, /w195, /w284, /304    - desired wavelength (default=171)
;      /wall		            - show all four wavel.
;      /norebin - return full size    (default reduced to 512x512)
;      rebin    - size to rebin to     (assumed sqare!)
;      /notv    - just return the data
;      quality  - minimum quality "accepted" (see data_quality.pro)
;                 (or, if set as flag, default=90%)
;
;   Calling Sequence:
;      lasteit [info, data, number, /w171, /w195, /w284, /304]
;
;   Calling Examples:
;      lasteit			   		; display most recent 171 image
;      lasteit,/w195		   		; ditto for 195
;      lasteit,info,data,5,/w304   		; most recent 5 304 images
;				   		;    return info and data 
;      lasteit,/wall				; most recent all wavelenghts
;      lasteit,index,data,50, rebin=256,/quality   ; last "50" @171, 
;			                           ; rebin->256, quality>90
;                                                    
;
;   History:
;      5-feb-1996 (S.L.Freeland)
;     23-apr-1996 (s.l.freeland) tvscl display if /noscale set
;      3-mar-1997 (S.L.Freeland) use genx (IDL eit_struct based) catalog 
;                                instead of old ascii catalog
;     14-mar-1997 (S.L.Freeland) insure chronological order on output
;                                add QUALITY keyword and function
;     23-jul-1997 (S.L.Freeland) apply QUALITY *BEFORE* FILL
;      6-Feb-1998 (S.L.Freeland) allow Al +1 (entrance filter degradation)
;     17-Oct-1998 (S.L.Freeland) enable PREPIT , optional call to sobel_scale
;     29-Oct-1998 (S.L.Freeland) pass DERIV_HIST to sobel_scale
;
;   Restrictions:
;      Full disk only, Files in $EIT_QUICKLOOK
;-
common lasteit_blk, last_index, last_text
  
;   wdef display parameters             ; specify display quadrant for window
; -----------------------------------------------
uleft=keyword_set(uleft)
lleft=keyword_set(lleft)
lright=keyword_set(lright)
uright=1-(uleft or lleft or lright)	; default
; -----------------------------------------------

notv=keyword_set(notv)
tvit=1-notv

if notv and n_params() eq 0 then begin
   message,/info,"You requested NOTV but did not supply output parameters...
   return
endif
; -------- /WALL logic (recursive) --------------
swaves=strtrim([171,195,284,304],2)
quad=['uleft','uright','lleft','lright']
if keyword_set(wall) then begin			; recurse
   if not keyword_set(rebin) then rebin=384     ; smaller
   for i=0,3 do exe=execute("lasteit,rebin=rebin,/w" + swaves(i) + ",/" + quad(i)) 
   return
endif
; -----------------------------------------------

if not keyword_set(norebin) and not keyword_set(rebin) then rebin=512
if not keyword_set(number) then number=1

eit_genx_cat,cat_name=cat_name,/name_only                     ; genx catalog? 
refresh=keyword_set(refresh) or n_elements(last_index) eq 0 

if refresh then begin
  case 1 of
     file_exist(cat_name): eit_genx_cat,/full_fov, last_index, last_text
     else: begin
        message,/info,"No GENX-QKL catalog " + cat_name + " online, reading recent headers...
        read_eit,eit_files(last=2), last_index
        last_text=get_infox(last_index,/fmt_tim, $
          'WAVELNTH,FILTER,naxis1,naxis2,EXPDUR,OBJECT', $
           format='a,a,a,a,a,a')        
     endcase
  endcase
endif 

case 1 of
   keyword_set(wave):          ; user passed explicitly
   keyword_set(w195): wave=195
   keyword_set(w284): wave=284
   keyword_set(w304): wave=304
   else: wave=171
endcase

ss=where(last_index.WAVELNTH eq wave and $
        (last_index.filter eq 'Clear' or last_index.filter eq 'Al +1') and $
	 last_index.object eq 'full FOV',count)

if count eq 0 then begin
   message,/info,"Sorry, no images with requested parameters..."
   return
endif

if number gt count then message,/info,"Sorry, Only " + $
   strtrim(count,2) + " images match your parameters"

number=number < count

; read the data arrays
ss=(ss(count-number:count-1))

; ------------ read "selected" files -> 3D + structure via read_eit ---------
files=eit_file2path(last_index(ss).filename)
files=files(sort(files))
read_eit, files, index, data, outsize=rebin
info=last_text(ss)

if keyword_set(prepit) then begin 
   box_message,'Prepping data...'
   eit_prep, index, data=data, outindex, outdata
   data=temporary(outdata)>0
   data(where(data eq min(data)))=0
   noscale=1
   case 1 of 
      keyword_set(sobel_weight): data=sobel_scale(index,data,$
         sobel_weight=sobel_weight,minper=5,hi=2000)
      else:  data=bytscl(alog10(data >10 < 2000))   
   endcase
endif
; ------- cleanup ------- 

scale=1-keyword_set(noscale)
if scale then begin
   message,/info,"NOTE: Scaling dark subtracted data"
   message,/info,"(use /noscale to suppress scaling)"
   data=bytscl(alog10((data - eit_dark()) > 1))
endif

bytit=tvit and (1-scale)
; ----------------------------------------------------------------------
if keyword_set(quality) then begin
   message,/info,"applying quality filter..."
   if quality eq 1 then qlim=90 else qlim=quality
   good=where(data_quality(data,/hist,quality_thresh=qlim),sscnt)

   if sscnt eq 0 then begin
      message,/info,"None of the images meet your quality else  begin
   endif else begin
      if sscnt eq n_elements(ss) then begin
          message,/info,"All images OK..." 
      endif else begin
         message,/info,"Some Bad image found, filtering..."
         index=index(good) 
         data=temporary(data(*,*,good))
      endelse
      if keyword_set(fill_cube) and sscnt gt 1 then eit_fill_cube,data  
   endelse
endif

; ------------------------- display on request ----------------------------
if tvit then begin
   if n_elements(rebin) eq 0 then message,/info,"Not displaying due to large size..." else begin
     eit_colors,wave
     if number eq 1 then begin
         wdef, xx, uright=uright, uleft=uleft, lleft=lleft, lright=lright, im=data
         call_procedure,(['tv','tvscl'])(bytit),data
         xyouts, 5,5,strcompress(info(0)), $
            size=1.5*((size(data))(1)/512.) < 2.,/device
      endif else begin
         xstepper,data, info
      endelse
   endelse
endif 
; ----------------------------------------------------------------------

return
end
