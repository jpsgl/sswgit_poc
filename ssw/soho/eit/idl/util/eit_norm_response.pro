function eit_norm_response,date_obs,inwave,fits_header,scale=scale,$
         verbose=verbose
;+
;NAME
;   eit_norm_response
;PURPOSE 
;  compute a normalized response for each eit band  
;INPUTS
;   date_obs  = date of observation
;   inwave    = wavelength of observation
;   fits_header = header to obtain pointing/size information
;OUTPUTS
;   returns normalization factor to be applied to measured DN level
;   on given date such that all EIT data is normalized to 2-Feb-1996.
;KEYWORDS
;   verbose = set for testing 
;   scale   = return relative scale 
;WARNINGS
;   this routine may not always have the most recent normalizations
;   especially just following a bakeout.
;PROCEDURE
;   Uses flat field determined on 24-Jun-1998. Linearly scales
;   to response curves. 171 and 195 scaled as 171, 304 and 284 scaled
;   as 304.
;MODIFICATIONS
;   2000-Jan-05 - Completely new algorithm, etc. J. Newmark
;   2000-Jul-26 - Use last computed fit up to present date, J. Newmark
;   2000-Sep-14 - Bug fix for binned images, J. Newmark
;   2000-Oct-02 - Make compatible with NRL header, N. Rich
;
;-
common eit_norm_blk, wavelength, date, sflat 
if not keyword_set(verbose) then verbose = 0
if n_elements(wavelength) eq 0 then wavelength = 0
if n_elements(date) eq 0 then date = 0

ddir = getenv('SSW_EIT_RESPONSE')
filein = concat_dir(ddir,'fit_resp.dat')
;filein = 'fit_resp.dat'

if inwave eq 304 or inwave eq 284 then lwave = 304 else lwave = 171

mjd_1996_0 = 50083.0d0*8.64d4
temp_date = anytim2utc(date_obs)
temp_obs = (8.64d4*temp_date.mjd + 1.e-3*temp_date.time) - mjd_1996_0
readcol,filein,wave,start_time,end_time,coeff_0,coeff_1,coeff_2,$
         format='I,D,D,D,D,D',/silent

if lwave eq 171 then top = n_elements(end_time)/2. else top = n_elements(end_time)
temp_obs = temp_obs < round(end_time(top-1))
index=where(wave eq lwave and round(temp_obs) ge round(start_time) and $
            round(temp_obs) le round(end_time))
index=index(0)

if index(0) ne -1 then begin
    nfactor = coeff_0(index)+coeff_1(index)*temp_obs +$
                  coeff_2(index)*temp_obs*temp_obs  
endif else nfactor = 0.99
if nfactor eq 1.0 then nfactor = 0.99

offset = 0.313                    ;normalization for 24-jun-1998
scale = (nfactor - offset)/(1-nfactor)

if lwave ne wavelength or temp_obs ne date then begin
    if verbose then message, /info, 'Reading in flat...'
    wavelength = lwave
    date = temp_obs
    if is_gsfcvms() then begin
      flat_file = 'eit_disk:[eit.reform.flat]eitflat.dat' 
      release = !version.release
      version_release = 100*strmid(release, 0, 1) + $
         10*strmid(release, 2, 1) + strmid(release, 4, 1)
      if version_release le 503 then begin
         openr, flat_unit, flat_file, /get_lun, /block
      endif else begin
         openr, flat_unit, flat_file, /get_lun, /block, /vax_float
      end
    endif else begin
      flat_file = concat_dir(getenv('SSW_EIT_RESPONSE'),'eitflat.dat')
;      flat_file ='eitflat.dat'
      openr,flat_unit,flat_file,/get_lun,/xdr                         
    endelse                     
    sflat = fltarr(1024, 1024)                                   
    readu, flat_unit, sflat              
    close, flat_unit & free_lun, flat_unit        
endif else if verbose then message, /info, 'Using pre-read normalization'

utc_repoint = anytim2utc('1996/04/16 23:30')
t_repoint = (8.64d4*utc_repoint.mjd + 1.e-3*utc_repoint.time) - mjd_1996_0
if temp_obs le t_repoint then sflat = shift(temporary(sflat),0,75)

x_bin = 1 & y_bin = 1
if n_elements(fits_header) ne 0 then begin
   n_x = eit_fxpar(fits_header,'NAXIS1')
   n_y = eit_fxpar(fits_header,'NAXIS2')
endif else begin
   n_x = 1024
   n_y = 1024
endelse
corner_offset = [-1, -1, -20, -20]
if ((n_x + n_y) lt 2048) then begin
   corner = intarr(4)
   corner(0) = EIT_FXPAR(fits_header, 'P1_X')
   corner(1) = EIT_FXPAR(fits_header, 'P2_X')
   corner(2) = EIT_FXPAR(fits_header, 'P1_Y')
   corner(3) = EIT_FXPAR(fits_header, 'P2_Y')
   IF datatype(EIT_FXPAR(fits_header, 'OBJECT')) NE 'STR' THEN BEGIN
	; Must be NRL header
   	corner(0) = FXPAR(fits_header, 'P1ROW')
   	corner(1) = FXPAR(fits_header, 'P2ROW')
   	corner(2) = FXPAR(fits_header, 'P1COL')
   	corner(3) = FXPAR(fits_header, 'P2COL')
   ENDIF
   if (corner(0) mod 2) and (corner(1) mod 2) then corner_offset = [-1,-2,-20,-20]
   corner = corner + corner_offset
endif else corner = [0, 1023, 0, 1023]

nx_grid = corner(1) - corner(0) + 1
if nx_grid gt n_x then x_bin = nx_grid/n_x

flat = sflat(corner(0):corner(1), corner(2):corner(3))
if (x_bin ne 1) then flat = rebin(temporary(flat), n_x, n_y)

return, (flat+scale)/(scale+1) > 0.03

end



