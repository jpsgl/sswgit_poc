function is_gsfcvms
; +
;   Name: is_gsfcvms
;
;   Purpose: single point maintenance of this list
;       add to list as required
;     
;   Calling Example:
;      if is_gsfcvms() then ...
;-
gstat=0
hosts=['gavroc','xanado','eitv0','eitv','magda','eitv1']  
if !version.os eq 'vms' then begin
     stat = trnlog('sys$node',thishost)
     if stat eq 1 then thishost = strlowcase(strmid(thishost(0),0,$
        strpos(thishost(0),':')))
     stat = where(hosts eq thishost)
     if stat(0) ne -1 then gstat = 1
endif
return, gstat
end
