pro eit_getlimb, eitfiles, xx, yy, rr, $
   fit=fit, find=find, guess=guess, log=log, update=update, $
      image=eitimage, header=header, outdir=outdir, re_fit = re_fit
;+
;   Name: eit_getlimb
;
;   Purpose: get the limb parameters from EIT FITS or fit limb if not present
;
;   Input Parameters:
;      eitfiles - EIT FITS file list
;
;   Output Parameters:
;     xx -  sun center, full res EIT pixels
;     yy -  sun center, full res EIT pixels
;     rr -  S radius,   full res EIT pixels
;      
;   Keyword Parameters:
;      fit   -  switch, if set, call fit_limb (default if no FITS params)
;      find  -  switch, if set, call find_limb
;      guess -  switch, if set, return guess (for fast prototyping)
;      update - switch, if set, update FITS file (add point info to header)
;      re_fit - switch, if set redo fitting even if in header
;
;   Calling Sequence:
;      eit_getlimb, eitfiles, xx,yy,rr [/fit, /find, /guess, /update]
;
;   Method:
;      get parameters from FITS file, fit_limb or find_limb , or guess
;  
;   History:
;      1-Feb-1996 - S.L.Freeland
;     15-Feb-1996 - S.L.Freeland - minor adjustments
;      3-Sep-1996 - J. Newmark -updated for whole mission paramters
;-
utc_repoint = anytim2utc('1996/04/16 23:30')
t_repoint = utc_repoint.mjd + 1.d-3*utc_repoint.time/86400.

eitpix=eit_pixsize()		; full res EIT pixel

nimg=n_elements(eitfiles)

xx=fltarr(nimg) & yy=fltarr(nimg) & rr=fltarr(nimg)	; output params

findit=keyword_set(find)
update=keyword_set(update)

pntkey=  ['CRPIX1','CRPIX2','RADIUS']
comments=['Sun Center X, EIT Pixels', 'Sun Center Y, EIT Pixels','Solar Radius'] 

npnt=n_elements(pntkey)
nvals=fltarr(npnt)
found=intarr(npnt)

findit  = keyword_set(find)
fitit   = keyword_set(fit)
guessit = keyword_set(guess)

finder=(["fit_limb,eitimage,xn,yn,rn,/quiet,pixel=eitpix, init=[bguess(0:1),bguess(2)*eitpix]", $
         "find_limb,eitimage,xn,yn,rn"])(findit)
fcom="(LIMB FINDER: " + (strsplit(finder,','))(0) + ")"

if update then begin				; output file names
   break_file, eitfiles, ll,pp,ff,ee,vv
   files=ff+ee+vv
   if not keyword_set(outdir) then outdir=pp
   onames=concat_dir(outdir,files)
endif

for i=0,nimg-1 do begin
;  try to get values from header 
   header=headfits(eitfiles(i))			   ; header first
   date_obs=eit_fxpar(header,'DATE_OBS')
   utc_obs = anytim2utc(date_obs)
   t_obs = utc_obs.mjd + 1.d-3*utc_obs.time/86400.
   if t_obs lt t_repoint then bguess=[506, 587, 371.] else $
               bguess=[506,511,371.]		

   x_sum = 1 & y_sum = 1
   realx=eit_fxpar(header,'p2_x') - eit_fxpar(header,'p1_x')+1
   if realx ne 1 then x_sum = fix(realx/eit_fxpar(header,'naxis1'))
   realy=eit_fxpar(header,'p2_y') - eit_fxpar(header,'p1_y')+1
   if realy ne 1 then y_sum = fix(realy/eit_fxpar(header,'naxis2'))
   bguess(0) = bguess(0)/x_sum & bguess(1) = bguess(1)/y_sum
   if x_sum eq y_sum then begin
       bguess(2) = bguess(2)/x_sum
       eitpix=eitpix*x_sum
   endif

;  ------------- first, check FITS header for pointing ---------------
   if 1-(findit or fitit)  then begin
      for j=0, npnt-1 do begin
         nvals(j)=(fxpar(header,pntkey(j),count=cnt))(0)
         found(j)=cnt ne 0
      endfor
      xx(i) = nvals(0) & yy(i) = nvals(1) & rr(i) = nvals(2)
   endif
   if keyword_set(re_fit) then found = found * 0
;  ---------------------------------------------------------------

;  -------------- if not in header, call limb fitter/finder --------
   ok=0
   case 1 of 
      total(found) eq 3: ok=1		; dont update (already encoded)
      guessit: begin 
         xx(i)=bguess(0) & yy(i) = bguess(1) & rr(i)=bguess(2)
         pcom="(ESTIMATED)"
      endcase
      else: begin 
         pcom=fcom
         message,/info,"Finding EUV limb for file: " + eitfiles(i)
         eitimage=readfits(eitfiles(i))
         exe=execute(finder)
         xx(i) = xn & yy(i) = yn & rr(i) = rn
      endcase
   endcase
;  ----------------------------------------------------------------

;  ------------- update FITS file if changed and requested --------
   if update and not ok then begin
      if n_elements(eitimage) eq 0 then eitimage=readfits(eitfiles(i), header)
      nvals=[xx(i),yy(i),rr(i)]
      for j=0,npnt-1 do fxaddpar,header,pntkey(j),nvals(j),comments(j)+pcom
      file_delete,onames(i)				; remove old
      message,/info,"Writing/Updating file: " + onames(i)
      fxwrite, onames(i), header, eitimage
   endif
;  ---------------------------------------------------------------
endfor

; return scaler for single image
if n_elements(xx) eq 1 then begin
   xx=xx(0)
   yy=yy(0)
   rr=rr(0)
endif

return
end
