function eit_struct, number, catalog=catalog, version=version, $
	oldversion=olversion, update=update, ncomment=ncomment
;+
;   Name: eit_struct
;
;   Purpose: return eit_structure 
;
;   Input Parameters:
;      number (optional) - number structures returned  - default is one  
;  
;   Keyword Parameters:
;      catalog   - if set, return catalog structure (subset)
;      ncomment -  if set, number of COMMENT records to include
;
;   Calling Sequence:
;      str=eit_struct( [number , ncomment=xx ]  )
;
;   History:
;      27-apr-1996 S.L.Freeland (map FITs header)
;      24-may-1996 S.L.Freeland (add .VERSION, make .MJD long)
;      15-aug-1996 S.L.Freeland (Version 2, naxis3 - add /CATALOG switch)
;      27-oct-1996 S.L.Freeland (typo (BCALE->BSCALE))
;      11-dec-1996 S.L.Freeland (add /CATALOG keyword for testing)
;      14-mar-1997 J.S.Newmark  (add history to obtain version number)
;      11-apr-1997 S.L.Freeland (add NCOMMENT keyword , a few new tags)
;      14-apr-1997 J.S.Newmark  (add sci-obj field to catalog)
;      05-may-1997 J.S. Newmark (add s/c pointing info tags)
;      26-mar-1998 J.S. Newmark (add handling for corrected date_obs)
;-
pad_size=128						; pad catalog struct
version=2

common	eit_struct_blk, str, strnc, catstr

if keyword_set(oldversion) then version=oldversion

if n_elements(str) eq 0 then begin 					
   case version of 
   1: str={							$

        version:1,						$

;	----------- fits ----------------------------
	simple:'',bitpix:0b, naxis:2,naxis1:0,naxis2:0,		$
        bcale:0., bzero:0., bunit:'',				$

;       ----------- soho ----------------------------
;
        date:'', mjd:0l, day:0, time:0l,			$
        time_obs:'',date_obs:'',				$
	filename:'',				                $
        origin:'', telescop:'', instrume:'', object:'',		$
        sci_obj:'', obs_prog:'',				$

;       -------- pointing -----------------------
	ctype1:'',ctype2:'',					$
	crpix1:0., crpix2:0., crval1:0., crval2:0., 		$
        cdelt1:0., cdelt2:0., solar_r:0., solar_b0:0.,		$

;	----------- eit -----------------------------
	wavelnth:0,						$
	filter:'', exptime:0.,  expmode:'',			$
	cftemp:0., ccdtemp:0., 					$
     
        comment:strarr(30)} 		;,history:strarr(30)}

   2: str={							$

        version:2,						$

;	----------- fits ----------------------------
	simple:'', bitpix:0b, 					$
        naxis:3,naxis1:0,naxis2:0,naxis3:0,			$
        bscale:0., bzero:0., bunit:'',				$

;       ----------- soho ----------------------------
;
        date:'', mjd:0l, day:0, time:0l,			$
        time_obs:'',date_obs:'',				$
	filename:'',				                $
        origin:'', telescop:'', instrume:'', object:'',		$
        sci_obj:'', obs_prog:'',				$

;       -------- pointing -----------------------
	ctype1:'',  ctype2:'', ctype3:'',			$
	crpix1:0.,  crpix2:0., crpix3:0.,  			$
        crval1:0.,  crval2:0., crval3:0., 		        $
        cdelt1:0.,  cdelt2:0., cdelt3:0.,			$
        solar_r:0., solar_b0:0., sc_x0:0.,	                $
        sc_y0:0.,   sc_roll:0., hec_x:0.,                       $
        hec_y:0.,   hec_z:0.,   car_rot:0.,                     $

;	----------- eit -----------------------------
	wavelnth:0,						$
	filter:'', exptime:0., expdur:0.0, expmode:'',		$
	cftemp:0., ccdtemp:0., datasrc:'',			$
        P1_X:0,P2_X:0,P1_Y:0,P2_Y:0, NMISSB:0,                  $
        corr_obs:'',                                            $

        comment:strarr(300) , history:strarr(1)}
    else: message,/info,"Unexpected version number: " + strtrim(version,2)
   endcase
endif

outstr=str

if n_elements(ncomment) eq 1 then begin
   if ncomment eq 0 then outstr=rem_tag(str,'comment') else $
      outstr=rep_tag_value(str,strarr(ncomment),'comment')
endif

if keyword_set(catalog) and n_elements(catstr) eq 0 or keyword_set(update) then begin
   outstr = $
       {nx:             0b,                     $       ; num pixel x / 32
        ny:             0b,                     $       ; num pixel y / 32
        nx_sum:         0b,                     $       ; x pixel summing
        ny_sum:         0b,                     $       ; y pixel summing
        file_time:      {mjd:0, sec:0l},        $       ; (see comment above)
        wave:           bytarr(4),              $       ; sector+171
                                                        ;  (0=not used),  
                                                        ;       1:171, 2:195,
                                                        ;       3:284, 3:304
        filter:         0b,                     $       ; filter
        obs_time:       {mjd:0, sec:0l},        $       ; (see comment above)
        object:         0,                      $       ; feature planned
        program:        0L,                     $       ; obs prog #
        exp_time:       0,                      $       ; exposure in 1/32 unit
        file_name:      bytarr(18),             $       ; FITS file name
        image_of_seq:   0,                      $       ; (not used for LZ)
        n_leb_proc:     0b,                     $       ; # of LEB proc steps
        leb_proc:       bytarr(8),              $       ; LEB process steps
        temp:           intarr(2),              $       ; data value CF and CCD
        n_wave:         0b,                     $       ; (only in LZ)
        n_repeat:       0,                      $       ; (only in LZ)
        version:        0b,                     $       ; nibbl for major,minor
        low_x:          0,                      $       ; low-x corner of image
        high_x:         0,                      $       ; high-x corner of image
        low_y:          0,                      $       ; low-y corner of image
        high_y:         0,                      $       ; high-y corner of image
        sci_obj:        bytarr(24),             $       ; from iap
        pad:            bytarr(18)}                     ; pad

        catstr=outstr
endif



if n_elements(number) gt 0 then outstr=replicate(outstr,long(number))

return,outstr
end


