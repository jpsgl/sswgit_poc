; EIT_RESCALE
; EIT_RESCALE drops the minimum value of a byte array movie sequence to 0, and
; rescales the movie sequence.
; D.M. fecit, 1996 June 2.
;
function eit_rescale, b0, minim = minim, maxim = maxim
;
if not keyword_set(minim) then minim = min(b0)
;
if keyword_set(maxim) then max_scale = float(max) else max_scale = 255.
;
b0 = b0 - minim & scl = max_scale/max(b0)
b0 = byte(scl*b0)
;
return, b0 & end
