function eit_point,date,wave,bin=bin
;+ 
; EIT_POINT
; 
; Purpose - return EIT pointing = Sun center, for a specific date and
;           wavelength
;
; Inputs: date, wavelength (uses average if not defined)
; Option Inputs: bin - if set uses binning (default = 1)
; Output: XCEN, YCEN in coordinate system starting at (0,0)
; Usage:
;     xy = eit_point('23-dec-1996',195)
;     print,xy
;        505.12 514.36
;         
; Created: 17 Feb 1998. J. Newmark
; Modified: 23 Jul 1998 J. Newmark - completely redone
; Modified: 17 Aug 1998 J. Newmark - new fit
; Modified: 04 Dec 1998 J. Newmark - added pointing after recovery 
;                                  - added bin keyword
; Modified: 03 Feb 1999 J. Newmark - new fit
; Modified: 06 Aug 1999 J. Newmark - new fit
; Modified: 20 Oct 1999 J. Newmark - new fit
; Modified: 02 Feb 2000 J. Newmark - new fit
; Modified: 19 Sep 2000 J. Newmark - new fit
; Modified: 28 Nov 2000 J. Newmark - new fit
;-

new_date = anytim2utc(date)
if n_elements(wave) eq 0 then wave = 0
if n_elements(bin) eq 0 then bin = 1

utc_repoint = anytim2utc('1996/04/16 23:30')
t_repoint = utc_repoint.mjd + 1.d-3*utc_repoint.time/86400.

; offpointed after ESRs, only full fov images
uesr2_s = anytim2utc('1997/11/20 06:57')
uesr2_e = anytim2utc('1997/11/20 14:45')
uesr3_s = anytim2utc('1998/03/04 12:51')
uesr3_e = anytim2utc('1998/03/04 23:03')
esr2_s = uesr2_s.mjd + 1.d-3*uesr2_s.time/86400.
esr2_e = uesr2_e.mjd + 1.d-3*uesr2_e.time/86400.
esr3_s = uesr3_s.mjd + 1.d-3*uesr3_s.time/86400.
esr3_e = uesr3_e.mjd + 1.d-3*uesr3_e.time/86400.

urecover = anytim2utc('1998/10/13')
recover = urecover.mjd + 1.d-3*urecover.time/86400.

urecover2 = anytim2utc('1999/02/01')
recover2 = urecover2.mjd + 1.d-3*urecover2.time/86400.

ushift3 = anytim2utc('1999/03/24')
shift3 = ushift3.mjd + 1.d-3*ushift3.time/86400. 

ushift4 = anytim2utc('1999/11/15')
shift4 = ushift4.mjd + 1.d-3*ushift4.time/86400. 

t_obs = new_date.mjd + 1.d-3*new_date.time/86400.

;xcen calculation
case 1 of
 t_obs ge shift4:   xcen = 505.5
 t_obs ge recover2: xcen = 505.17
 t_obs ge recover:  xcen = 505.3 
 else: xcen = poly(new_date.mjd - 50084,[505.51267,-0.0019158333,2.2924399e-06])
endcase
if wave eq 304 or wave eq 171 then xcen = xcen + 0.5
xcen = long((xcen+0.005)*100.)/100.

;ycen calculation
if (t_obs le t_repoint) or (t_obs gt esr2_s and t_obs le esr2_e) or $
      (t_obs gt esr3_s and t_obs le esr3_e) then ycen = 587.8 else begin
           t_obs = new_date.mjd - 50084
           recover = recover - 50084
           recover2 = recover2 - 50084
           shift3 = shift3 - 50084
           case 1 of 
             t_obs lt 616: fit = [513.24468,0.0054164804,-6.4048387e-06] 
             t_obs lt recover: fit = [365.08002,0.58495656,-0.00075611558,3.2361247e-07]
             t_obs lt recover2: fit = 515.5
             t_obs lt shift3: fit = 515.9
             t_obs lt 1481: fit = [511.967, 0.00293872]
             t_obs lt 1846: begin
                      fit = [516.297,-0.00696556,2.83379e-05]
                      t_obs = t_obs - 1480
                   end
             else: fit = 517.3
           endcase
           ycen = poly(t_obs,fit)
           if wave eq 284 or wave eq 171 then ycen = ycen + 0.5
           ycen = long((ycen+0.005)*100.)/100.
endelse


return,[xcen,ycen]/bin
end
