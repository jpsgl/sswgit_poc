pro eit_qklcat, contents, catfile=catfile, 			$
   ascii=ascii, binary=binary, generate=generate, 		$
   wave=wave, dateobs=dateobs, timeobs=timeobs, file=file, 	$
   naxis1=naxis1, naxis2=naxis2, filter=filter, exptime=exptime, $
   last=last

;+
;   Name: eit_mkqklcat
;
;   Purpose: create, update, or read an index/catalog file of current EIT quicklook files
;
;   Output Parameters:
;      contents - current contents (string array or structure array)
;
;   History:
;      17-feb-1996 S.L.Freeland 
;       3-jun-1996 S.L.Freeland - permit vector (EIT_QUICKLOOK00,01,02...)
;       7-aug-1996 J. Newmark   - permit both vector and scalar EIT_QUICK*
;      20-aug-1996 S.L.Freeland - add /LAST keyword (unix only)
;      23-oct-1996 S.L.Freeland - Only catalog past 10 days.
;-
; define catalog name
binary=keyword_set(binary)
catname='eit_qkl_cat.' + (['ascii','binary'])(binary)
catdir=get_logenv('SSW_SITE_LOGS')
if not keyword_set(catfile) then catfile=concat_dir(catdir,catname)

generate= 1-file_exist(catfile) or keyword_set(generate)

if generate then eitfiles=eit_files(last=10)

; get current contents
case 1 of 
   binary: begin
   endcase
   else: begin
      case 1 of 
      keyword_set(last) and 1-(is_member(!version.os,['vms'],/ignore)): $
         spawn,['tail','-' + strtrim(last,2),catfile],/noshell,contents      
      else: contents=rd_tfile(catfile)
      endcase
   endcase
endcase

if generate then begin
   fsize=file_stat(eitfiles,/size)
   bad= (fsize eq 0) or (strpos(eitfiles,';') ne -1)
   badss=where(bad,bcnt)
   if bcnt gt 0 then begin
      message,/info,"Bad file sizes:"
      prstr,'   ' + strrep_logenv(eitfiles(badss),'EIT_QUICKLOOK')
      eitfiles=eitfiles(where(1-bad)) 
   endif

   info=get_eit_info(eitfiles)
   top=get_logenv('EIT_QUICKLOOK0*')
   topenvs=get_logenv('EIT_QUICKLOOK0*',/env)
   if top(0) eq '' then begin
       top=get_logenv('EIT_QUICKLOOK')
       topenvs=get_logenv('EIT_QUICKLOOK',/env)
   endif 
   full_name=strarr(n_elements(eitfiles))

   for i=0, n_elements(top)-1 do begin
       ss=where(strpos(eitfiles,top(i)) ne -1, sscnt)
;      replace path with appropriate environmental
       if sscnt gt 0 then full_name(ss)=strrep_logenv(eitfiles(ss),topenvs(i))
   endfor
   file_append,catfile,info + '  ' + full_name,/new
endif


if 1-binary then begin
; return info (ascii only for now)
   out=strtrim(str2cols(contents),2)		; split into columns
;  extract fields -> output keyword
   order=['sfile','dateobs','timeobs','naxis1','naxis2','filter','wave']
   order=[order,'exptime','expmode','ccdt','file']
   for i=0,n_elements(order)-1 do exe=execute(order(i)+"=reform(out(i,*))")
endif

return
end
