function    eit_degrid_smooth, image, residue=residue, grid=grid
;+
;
; NAME:
;    eit_degrid_smooth
; PURPOSE:
;    A. Maucherat's first cut at the degridding software.
; CALLING SEQUENCE:
;    clean_image = eit_degrid_smooth(dirty_image [,residue=residue, 
;                   grid=grid])
; INPUTS:
;    dirty_image = dark-subtracted image.
; OUTPUT:
;    clean_image = alog10(degridded image)
;
; OPTIONAL OUTPUTS:
;    residue     = residue of degridding algorithm 
;    grid        = Calculated grid
; 
; RESTRICTIONS:
;    Can't handle images smaller than 42x42 pixels.  
;
; TO DO:
;  * This routine doesn't conserve counts.  
;
;  * I wonder how well this code deals with small images.
;
;  * Need to keep a history of this in the FITS header.
;
; MODifICATION HISTORY:
;    15-Mar-1996 - (AM)  - Written 
;     9-Apr-1996 - (BNH) - Cleaned up to make slightly more readable, 
;                          turned into a function call,  Added some
;                          comments and streamlined some code.
;    10-Apr-1996 - (BNH) - Added support for sub_images.
;			 - Image is converted to a FLOAT on entry.
;-

; 	/EDGE_TRUNCATE didn't exist in SMOOTH() before IDL 4.0.1. 
IF (!version.release GE '4.0.1') THEN $
    smooth_flags = {edge_truncate: 1} $
ELSE BEGIN 
    smooth_flags =  {edge_truncate: 0}
    message, /info, '/EDGE TRUNCATE turned off.'
END

sz = size(image)
xsize = sz(1) 
ysize = sz(2)

if (xsize lt 42 or ysize lt 42) then begin
    message, /info, "Image too small to degrid."
    return, 0
endif

image=alog10(float(image)>.1)

eitmoy=smooth(image,23,_EXTRA=smooth_flags)   ; Boxcar smooth
maximage = max(eitmoy) * 0.875		; Maucherat Constant

subs = where(eitmoy gt maximage)	; Find the high points
eitmoy(subs) = image(subs)		; Replace them with the 
					; unsmoothed values

eitmoy=smooth(eitmoy,13,_EXTRA=smooth_flags)	; Smooth 'em again
;************************************************************
                                        ; eitmoy is the twice-smoothed
					; original image
grid  = image-eitmoy			; diff = original - smoothed
eitp9 = fltarr(9)			; Magic Array

;
;  EXPANDED_IMG:  This is just the grid image with an expanded border
;  around each side.  Speeds up computing time (due to gymnatics that 
;  take place around the edges) significantly.
;
expanded_img = fltarr(xsize+42*2 ,ysize+42*2)
expanded_img(42:xsize+41, 42:ysize+41) = grid			   ; Center
expanded_img(0:41,42:ysize+41) = reverse(grid(0:41,*),1)	   ; Left
expanded_img(xsize+42:*,42:ysize+41)=reverse(grid(xsize-42:*,*))   ; Right
expanded_img(42:xsize+41,ysize+42:*)=reverse(grid(*,ysize-42:*),2) ; Top
expanded_img(42:xsize+41,0:41)=reverse(grid(*,0:41),2)		   ; bottom

for j = 42,ysize+41 do begin
    for i = 42,xsize+41 do begin
        eitp9(0) = expanded_img(i-42,j)
        eitp9(5) = expanded_img(i,j-42)

        eitp9(1) = expanded_img(i-21,j)
        eitp9(6) = expanded_img(i,j-21)

        eitp9(2) = expanded_img(i,j)

        eitp9(3) = expanded_img(i+21,j)
        eitp9(4) = expanded_img(i,j+21)
        
        eitp9(4) = expanded_img(i+42,j)
        eitp9(8) = expanded_img(i,j+42)

        grid(i-42,j-42) = median(eitp9)
    endfor
endfor

edija = image-grid

;
; Generate residue to see possible defects
;
eitmoy = smooth(edija,23)
residue = edija-eitmoy 

return, edija

end

