pro eit_fulldisk, t0, t1, index, data, $
        w171=w171, w195=w195, w284=w284, w304=w304, wall=wall, wave=wave, $
        norebin=norebin, notv=notv, rebin=rebin, display=display,      $  ;
	uleft=uleft, uright=uright, lleft=lleft, lright=lright, $
	noscale=noscale, refresh=refresh, fill_cube=fill_cube, $
        quality=quality, nmissb=nmissb, $
        hours=hours, sizes=sizes, count=count
;+
;   Name: eit_fulldisk
;
;   Purpose: read,  return and optionally display Full Disk EIT images
;
;   Input Parameters:
;      t0 - start time of interest
;      t1 - stop  time of interest
;
;   Output Parameters:
;      index - EIT instrument parameters (structures from read_eit)
;      data -  EIT image or image cube
;
;   Keyword Parameters:
;      /w171, /w195, /w284, /304    - desired wavelength (default=171)
;      /wall		            - show all four wavel.
;      /norebin - return full size    (default reduced to 512x512)
;      rebin    - size to rebin to     (assumed sqare!)
;      /notv    - just return the data
;      /display - if  set, display data (via xstepper(
;      nmissb   - maxmimum number of missing blocks to accept
;      quality  - (empirical data check) minimum quality "accepted" 
;                 (see data_quality.pro, or, if set as flag, default=90%)
;      sizes    -  sizes to accept (ex: 256,512,1024) - default=all full fov
;      hours    - if set, limit cadence to this number of hours
;                 (see grid_data.pro and timegrid.pro for generic details)
;      count    - number of files found matching criteria
;
;   Calling Sequence:
;      eit_fulldisk, t0, t1, index, data  [, /w171, /w195, /w284, /304]
;
;   Calling Examples:
;
;   eit_fulldisk,t0,t1,index,data,/w304                ; 304 full between t0&t1 
;   eit_fulldisk,t0,t1,index,data,hour=4,/w195,nmiss=0 ; 195, perfect only,
;                                                      ;   4-hour cadence  
;   eit_fulldisk,t0,t1,index,data,hour=4,/w195, $      ; same as previous
;        nmissb=0, sizes=[512,1024]                    ; but ignore 256x256 etc
;   eit_fulldisk,t0,t1,index,data,/w171, $         ; good to perfect 171s
;        nmissb=2,sizes=1024                       ; only 1024x1024       
;   eit_fulldisk,t0,t1,index,/w284                 ; INDEX only (no data read)
;   
;   History:
;      29-may-1997 - S.L.Freeland - from 'lasteit.pro'
;                    (and asap, lasteit will just call this routine)
;       2-Apr-1998 - S.L.Freeland - added COUNT output keyword
;
;   Restrictions:
;      Full disk only, Files in $EIT_QUICKLOOK
;
;   Calls:
;      read_eit, eit_files, eit_file2path, get_eit_info, 
;      grid_data, anytim, delvarx, [wdef, xstepper....]
;     
;   Common Blocks:
;      eit_fulldisk_blk - speed up subsequent wavelenths for same time interva;
;-
common eit_fulldisk_blk, t0c, t1c, last_index
if n_elements(t0c) eq 0 then begin
   t0c=''
   t1c=''
endif  
;   wdef display parameters             ; specify display quadrant for window
; -----------------------------------------------
uleft=keyword_set(uleft)
lleft=keyword_set(lleft)
lright=keyword_set(lright)
uright=1-(uleft or lleft or lright)	; default
; -----------------------------------------------

case 1 of 
    data_chk(t0,/undefined): begin
       t0=reltime(/yest,/day_only)                     
       t1=ut_time() 
    endcase
    data_chk(t1,/undefined): t1=timegrid(t0,/day,/string)      ;  24 hours
    else:
endcase
   
tvit=keyword_set(display) or (n_params() le 2)
notv=1-tvit

if notv and n_params() eq 0 then begin
   message,/info,"You requested NOTV but did not supply output parameters...
   return
endif
; -------- /WALL logic (recursive) --------------
swaves=strtrim([171,195,284,304],2)
quad=['uleft','uright','lleft','lright']
if keyword_set(wall) then begin			; recurse
   if not keyword_set(rebin) then rebin=384     ; smaller
   for i=0,3 do exe=execute("lasteit,rebin=rebin,/w" + swaves(i) + ",/" + quad(i)) 
   return
endif
; -----------------------------------------------

norebin=keyword_set(norebin)
case 1 of
   keyword_set(rebin):
   n_elements(sizes) eq 1: begin
      norebin=sizes(0) eq 1024
   endcase
   else: rebin=512
endcase

if not keyword_set(number) then number=1

;eit_genx_cat,cat_name=cat_name,/name_only                     ; genx catalog? 

t0x=anytim(t0,/yohkoh)
t1x=anytim(t1,/yohkoh)

refresh=keyword_set(refresh) or n_elements(t0c) eq 0 or $ 
   ((t0c ne t0x) and (t1c ne t1x))

if refresh then begin
  files=eit_files(t0,t1)
  break_file,files,ll,pp,ff,ee,vv
  fnames=ff+ee
  uss=uniqo(fnames,sort(fnames))
  read_eit,files(uss), last_index
  last_text=get_eit_info(last_index)
  t0c=t0x
  t1c=t1x
endif 

case 1 of
   keyword_set(wave):          ; user passed explicitly
   keyword_set(w195): wave=195
   keyword_set(w284): wave=284
   keyword_set(w304): wave=304
   else: wave=171
endcase

ok = last_index.WAVELNTH eq wave and $
     (last_index.filter eq 'Clear' or last_index.filter eq 'Al +1') and $
	 last_index.object eq 'full FOV'
if data_chk(nmissb,/defined) then ok = ok and (last_index.nmissb le nmissb)

sok=intarr(n_elements(ok))

if n_elements(sizes) eq 0 then sok=sok+1 else $
   for i=0,n_elements(sizes)-1 do sok= sok or $
       (( last_index.naxis1 eq sizes(i) ) and ( last_index.naxis2 eq sizes(i)))
ok=ok and sok
ss=where(ok,count)

if count eq 0 then begin
   message,/info,"Sorry, no images with requested parameters..."
   return
endif

if keyword_set(hours) then ss=ss(grid_data(last_index(ss),hours=hours))

; ------------ read "selected" files -> 3D + structure via read_eit ---------
files=eit_file2path(last_index(ss).filename)
files=files(sort(files))

delvarx,index, data
if n_elements(files) eq 1 then files=files(0)
read_eit, files, index, data, outsize=rebin, nodata=(n_params() le 3)
info=get_eit_info(index)

if data_chk(data,/defined) then begin
; ------- cleanup ------- 
if keyword_set(fill_cube) and count gt 1 then eit_fill_cube,data  ; fill cube from neighbors
scale=1-keyword_set(noscale)
if scale then begin
   message,/info,"NOTE: Scaling dark subtracted data"
   message,/info,"(use /noscale to suppress scaling)"
   data=bytscl(alog10((data - eit_dark()) > 1))
endif
bytit=tvit and (1-scale)
; ----------------------------------------------------------------------
if keyword_set(quality) then begin
   message,/info,"applying quality filter..."
   if quality eq 1 then qlim=90 else qlim=quality
   good=where(data_quality(data,/hist,quality_thresh=qlim),sscnt)

   if sscnt eq 0 then begin
      message,/info,"None of the images meet your quality else  begin
   endif else begin
      if sscnt eq n_elements(ss) then begin
          message,/info,"All images OK..." 
      endif else begin
         message,/info,"Some Bad image found, filtering..."
         index=index(good) 
         data=temporary(data(*,*,good))
      endelse
   endelse
endif

; ------------------------- display on request ----------------------------
if tvit then begin
   if n_elements(rebin) eq 0 then message,/info,"Not displaying due to large size..." else begin
     eit_colors,wave
     if number eq 1 then begin
         wdef, xx, uright=uright, uleft=uleft, lleft=lleft, lright=lright, im=data
         call_procedure,(['tv','tvscl'])(bytit),data
         xyouts, 5,5,strcompress(info(0)), $
            size=1.5*((size(data))(1)/512.) < 2.,/device
      endif else begin
         xstepper,data, info
      endelse
   endelse
endif 
endif
; ----------------------------------------------------------------------

return
end
