;+
; Project     : HESSI
;
; Name        : EIT_PARTIAL
;
; Purpose     : Update EIT INDEX for partial
;               fov images with instrument pointing
;
; Category    : Ancillary GBO Synoptic
;
; Syntax      : IDL> index=eit_partial(index,header=header)
;
; Inputs      : INDEX = index structure
;
; Outputs     : INDEX with updated CRPIX/CRVAL/XCEN/YCEN values
;
; Keywords    : HEADER with updated CRPIX/CRVAL/XCEN/YCEN values
;
; History     : Written 20 March 2002, D. Zarro, L-3Com/GSFC
;               Modified 13 June 2005, Zarro (L-3Com/GSFC) 
;                - added check for header
;
; Contact     : dzarro@solar.stanford.edu
;-

function eit_partial,index,header=header,verbose=verbose

if ~exist(index) then return,''
nindex=n_elements(index)
if ~have_proc('eit_point') then return,index
if ~is_struct(index) then return,index

;-- check if partial frame image needs pointing update

do_partial=0b
if have_tag(index,'object') then do_partial=stregex(index.object,'partial',/fold,/bool)
if do_partial and have_tag(index,'history') then begin
 chk=where(stregex(index.history,'pointing update',/fold,/bool),count)
 do_partial=(count eq 0)
endif 

if ~do_partial then return,index
if keyword_set(verbose) then message,'Determining pointing for partial image...',/cont

;-- update CDELT1

nindex=index
cdelt=call_function('eit_pixsize')
nindex=rep_tag_value(nindex,cdelt,'cdelt1')
nindex=rep_tag_value(nindex,cdelt,'cdelt2')

;-- Sun center in arcsec (XCEN/YCEN)

nindex=rep_tag_value(nindex,0.,'xcen')
nindex=rep_tag_value(nindex,0.,'ycen')

have_com=0b
have_p1=have_tag(nindex,'p1_x')
if ~have_p1 then begin
 if have_tag(nindex,'comment') then begin
  stc=stc_key(nindex.comment)
  have_com=have_tag(stc,'p1_x')
  if ~have_com then return,nindex
 endif
endif

point=call_function('eit_point',nindex.date_obs,nindex.wavelnth)
point=comdim2(point)
sx=point[0]
sy=point[1]

if have_p1 then begin
 fx=(nindex.p1_x+nindex.p2_x)/2.
 fy=(nindex.p1_y+nindex.p2_y)/2.
endif

if have_com then begin
 stc=stc_key(nindex.comment)
 fx=(float(stc.p1_x)+float(stc.p2_x))/2.
 fy=(float(stc.p1_y)+float(stc.p2_y))/2.
endif

nindex.xcen=(fx-sx)*cdelt
nindex.ycen=(fy-sy)*cdelt

;-- update CRPIX/CRVAL

nindex=rep_tag_value(nindex,0.,'crval1')
nindex=rep_tag_value(nindex,0.,'crval2')
nindex=rep_tag_value(nindex,0.,'crpix1')
nindex=rep_tag_value(nindex,0.,'crpix2')

nindex.crpix1=comp_fits_crpix(nindex.xcen,nindex.cdelt1,nindex.naxis1,nindex.crval1)
nindex.crpix2=comp_fits_crpix(nindex.ycen,nindex.cdelt2,nindex.naxis2,nindex.crval2)

;-- update history

history=nindex.history
nhist=n_elements(history)
nhistory=strarr(nhist+1)
nhistory[0:nhist-1]=nindex.history
nhistory[nhist]='Pointing update'
nindex=rep_tag_value(nindex,nhistory,'history')

;-- return HEADER in keyword output

if arg_present(header) then header=struct2fitshead(nindex) 

return,nindex

end

