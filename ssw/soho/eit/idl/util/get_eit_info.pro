function get_eit_info, inrecs, short=short, $
   out_style=out_style, fmt_time=fmt_time,  $
   day_only=day_only, time_only=time_only, date_only=date_only, msec=msec
;+
;   Name: get_eit_info
;
;   Purpose: provide 'standard' 1-line summaries of EIT index/fits file headers
;
;   Input Parameters:
;      inrecs - EIT structures (via read_eit) or fits file names
;
;   Keyword Parameters:
;      short     - if set, provide a "shorter" summary
;      fmt_time  - output TIME format (default=ECS) - (see anytim OUT_STYLE)
;      out_style - synonym for FMT_TIME (see anytim OUT_STYLE) 
;
;   History:
;      15-mar-1997 S.L.Freeland
;      18-mar-1997 S.L.Freeland - allow user FMT_TIME&OUT_STYLE, 
;                                 set default time output to -> ECS
;
;   Method:
;     just setup can call <get_infox> for some EIT "standards"
;-
day_only =keyword_set(day_only)
time_only=keyword_set(time_only)
date_only=keyword_set(date_only)

; select output time format
case 1 of 
   data_chk(fmt_time,/scalar,/string):                       ; user FMT_TIME
   data_chk(out_style,/scalar,/string): fmt_time=out_style   ; user OUT_STYLE
   else: fmt_time='ECS'                                      ; Default=ECS
endcase

if data_chk(inrecs,/struct) then index=inrecs else $
   read_eit,inrecs,index

nindex=n_elements(index)

fields='WAVELNTH,NAXIS1,EXPDUR'
fmt='i3.3,i4.4,f5.1'

if date_only then fields=''
case 1 of
   day_only or time_only or date_only:
   keyword_set(short): 
   else: begin
      fields='WAVELNTH,FILTER,NAXIS1,NAXIS2,EXPDUR'
      fmt=arr2str(replicate('a',n_elements(str2arr(fields))))
   endif
endcase

retval=get_infox(index,fields,format=fmt,fmt_tim=fmt_time, $
   time=time_only, day=day_only, msec=msec)


return,strtrim(retval,2)
end
