pro mk_lasteit_movie, waves=waves, outsize=outsize, mpeg=mpeg, nimg=nimg, $
    _extra=_extra, htmldoc=htmldoc, moviedir=moviedir, rename=rename, $ 
    deriv_hist=deriv_hist, java=java, $
    prepit=prepit, sobel_weight=sobel_weight
;+
;   Name: mk_lasteit_movie
;
;   Purpose: recent EIT full disk movies->WWW  (illustrate image2movie...)
;
;   Input Parameters:
;      NONE
;
;   Optional Keyword Parameters:
;      waves   - array of EIT wavelengths to include (default = all)
;      outsize - output size of movie frames (default=280)
;      mpeg    - if set, make the movies MPEG (Default = gif animation)
;      nimg    - number of images to include (most recent NN for each WAVE)
;                (default=30)
;
;   Calling Examples:
;      IDL> mk_lasteit_movie                       
;      IDL> mk_lasteit_movie, outsize=100, nimg=100, waves=[195,304], /mpeg
;
;   Calls:
;      html_doc, file_append, lasteit, eit_colors, data2files, image2movie...
;      http_names, prstr, strjustify
;
;   History:
;     15-mar-1997 - S.L.Freeland 
;     18-mar-1997 - S.L.Freeland - change movie label times to ECS format
;                                  thumbnail frame size ->  80x 80
;                                  [via image2movie parameters]
;                                  add rename as last step (assume cron only)
;     21-mar-1997 - S.L.Freeland - if batch mode, move to scratch area 1st
;     16-jul-1997 - S.L.Freeland - add  /JAVA option (pass to image2movie)
;     22-jul-1997 - S.L.Freeland - add /loop switch in image2movie,/gif call 
;     19-Mar-1997 - S.L.Freeland - boost contrast
;     24-Oct-1998 - S.L.Freeland - pass PREPIT and SOBEL_WEIGHT->lasteit
;     28-Oct-1998 - S.L.Freeland - pass DERIV_HIST to lasteit     
;
;   Restrictions:
;     User needs write access to $path_http/... area
;     Assume html HEADER and TRAILER template files are available
;     If desired, a call to eit_prep would be inserted after <lasteit> call
;-
dtemp=!d.name
set_plot,'z'                                   ; use zbuffer
if keyword_set(get_logenv('ssw_batch')) then $ ; batch job? move to scratch
   cd,concat_dir('$path_http','movie_scratch')

; ----------------- define defaults -------------------------------------------
if n_elements(moviedir) eq 0 then moviedir=concat_dir('$path_http','movies')
if n_elements(nimg) eq 0 then nimg=30
if n_elements(outsize) eq 0 then outsize=256
if n_elements(waves) eq 0 then waves=[171,195,284,304] & swaves=strtrim(waves,2)
root=strmid(ex2fid(anytim2ex(ut_time())),0,6)
mtype=(['gifanim','mpeg'])(keyword_set(mpeg))
mtype=([mtype,'java'])(keyword_set(java))
; --------------------------------------------------------------------------

; -------- start building a WWW document ----------------------
if not data_chk(htmldoc,/scalar,/string) then htmldoc = concat_dir( $
   '$path_http',arr2str(['eit_fullfov',mtype,root],'_')+'.html') 
html_doc,htmldoc,template='eit_fovmovie_header.html',/header  ; add header 
; --------------------------------------------------------------------------

; ------ loop through specified wavelenths --------------------------

for i=0,n_elements(waves)-1 do begin

;  ----- get last (NIMG) full disk WAVE(i) images, quality>95%, fill --------
   lasteit,/notv,index,data,nimg,wave=waves(i), /fill, quality=95, $
      prepit=prepit, sobel_weight=sobel_weight, deriv_hist=deriv_hist
;  -------------------------------------------------------------------------

;  ----------- load EIT standard color tables for WAVE(i) ------------------
   eit_colors,waves(i),r,g,b                                ; WAVE->RGB
   tvlct,r,g,b,/get
;  -------------------------------------------------------------------------

;  ------------ convert 3D cube -> temporary files -------------------------
   data2files,data,times=index,/gif,file=files              ; 3D->files
;  -------------------------------------------------------------------------

;  --- convert file sequence -> movie of MTYPE, w/thumbnail and HTML --------
   image2movie, files, r,g,b, outsize=outsize, /thumbnail, mpeg=mpeg, $
     java=java, $
     movie_name=concat_dir(moviedir,arr2str(['eit',swaves(i),root],'_')), $ 
     uttimes=index, label=get_eit_info(index,/short), html=html,/inctime, $
     fmt_time='ECS',thumbsize=100, loop=loop
;  ---------------------------------------------------------------------------

;  -------- append HTML for this movie to top level HTML doc -----------------
   file_append,htmldoc, ["<p> Wave: "+ swaves(i)+ "&#197", html]       
;  ---------------------------------------------------------------------------

endfor
; --------------------------------------------------

set_plot,dtemp                                            ; restore plot dev.
html_doc,htmldoc,template='eit_trailer.html'              ; append WWW trailer
prstr,strjustify(['MOVIE URL:',http_names(htmldoc)],/box) ; show user top URL


; -------------- rename file (cron job only) -------------------
if keyword_set(rename) then begin                     ; set via cron job
   permname=str_replace(htmldoc,'_'+root,'')          ; 'standard' name
   file_delete,permname                               ; remove old
   spawn,['/bin/cp','-p',htmldoc,permname],/noshell   ; spawn copy command
endif
; ----------------------------------------------------------------
return
end
