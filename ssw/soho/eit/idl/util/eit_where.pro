function eit_where, item, startt, stopt,  count, $
       wave=wave, xsize=xsize, ysize=ysize, $
       files=files, w171=w171, w195=w195, w284=w284, w304=w304
;+
;   Name: eit_where
;
;   Purpose: eit files matching given wavelenth, size...
;
;   Input Parameters:
;       [eitinfo] - 
;
;   Output Parameters:
;       eitinfo - (if not defined on input) - 1 line summary of all online files
;       count   - number which match 
;
;   Function Returns:
;      subscripts (ss) vector of matching elements
;
;   Calling Sequence:
;      ss=eit_where( [/w171, /w195, /w284, /304, isize=[x,y], files=files]
;   Prototype:
;-

eit_qklcat, item , wave=cwave, naxis1=nx, naxis2=ny, file=files, $
   time=time, date=date

; --------------------  check wave keywords -------------------------
waves=[171,195,284,304]
swaves=strtrim(waves,2)
for i=0,3 do $
   exe=execute("if keyword_set(w" + swaves(i)+") then wave=waves(i)")
; -----------------------------------------------------

; defaults
if not keyword_set(xsize) then xsize=1024
if not keyword_set(ysize) then ysize=xsize
if n_elements(wave) eq 0 then wave=171

sss=lindgen(n_elements(time))
if n_elements(startt) eq 1 and n_elements(stopt) eq 1 then begin
   ss=sel_timrange(date + ' ' + time, startt,stopt,/between)
   if ss(0) eq -1 then begin
      message,/info,"No records 
      return,ss				;** early exit
   endif
endif

ss=where(wave eq fix(cwave) and xsize eq fix(nx) and ysize eq fix(ny),count)

prstr,strjustify(["Searching for image parameters: ", "Wavelength: " + strtrim(wave,2), $
  "Size: " + strtrim(xsize,2) + ' X ' + strtrim(ysize,2),"Number of matches: "+ strtrim(count,2)],/box)

if ss(0) eq -1 then files='' else files=files(ss)

return,ss
end
