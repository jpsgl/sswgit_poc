; --------------- form and spawn commands ------------
pro eit_fdspawn, oldnames, newnames, link=link , copy=copy
case 1 of
  keyword_set(copy): command='cp -p '
  keyword_set(link): command='ln -s '                     
  else: command='ln -s '                     
endcase
cmds=command + oldnames + ' ' + newnames

for i=0,n_elements(cmds)-1 do begin
  file_delete,newnames(i)
  print,strmid(cmds(i),0,6)+ ' --> ' + newnames(i)
  spawn,cmds(i)
endfor
end
; --------------------

pro eit_fulldiskdb,  t0,  t1, topdir=topdir, $
  copy=copy, link=link, $
  waves=waves, $
  eit_prep=eit_prep,  prefix=prefix, $
  hours=hours, nmissb=nmissb, sizes=sizes, $
  incwave=incwave
;+
;   Name: eit_fulldiskdb
;
;   Purpose: generate EIT full disk data base (calls eit_fulldisk)
;
;   Input Parameters:
;      t0, t1 - start and stop time
;
;   Keyword Parameters:
;      copy   - switch - if set, COPY the files to $EIT_FULLDB/YYYYMM
;      links  - switch - if set, symbolic links only
;      nmissb - missing block limit (see eit_fulldisk.pro)
;      hours  - limit cadence in hours (see eit_fulldisk.pro, grid_data.pro)
;      sizes  - limit sizes (256, 512, 1024, etc. see eit_fulldisk.pro)
;      waves  - optional wave array (default=[171,195,284,304]
;      eit_prep - if set, prep the output (overrides COPY and LINKS)
;
;   History:
;      29-may-1997 - S.L.Freeland, written
;
;   Restrictions:
;      many - assumed running on a machine with all EIT data online
;
;   Side Effects:
;     may create required subdirectories ($EIT_FULLDB/YYYYMM)
;-
if not keyword_set(prefix) then prefix=''
if data_chk(t0,/undefined) then begin
   message,/info,"Need a time...
   return
endif

; define top level
case 1 of
  data_chk(topdir,/string): 
  get_logenv('EIT_FULLDB') ne '': topdir=get_logenv('EIT_FULLDB')
  else: topdir=concat_dir('$SSWDB/soho/eit','daily_full')
endcase

day0=gt_day(t0,/string)
if n_params() eq 1 then t1=timegrid(day0,/day)
day1=gt_day(t1,/string)

grid=timegrid(day0,day1,/day,/string)
filename=time2file(grid)
yyyymm=strmid(filename,0,6)
alldirs=concat_dir(topdir,yyyymm)

mdirs=yyyymm(uniq(yyyymm))
odirs=concat_dir(topdir,mdirs)
needit=where(1-file_exist(odirs,/dir),ncnt)
if ncnt gt 0 then begin
   prstr,strjustify(['Creating subdirectories:',odirs(needit)],/box)
   for i=0,ncnt-1 do spawn,'mkdir -p ' + odirs(needit(i))
endif

incwave=keyword_set(incwave)

linkit=keyword_set(link)
prepit=keyword_set(eit_prep)
copyit=keyword_set(copy)

for  i=0,n_elements(grid)-2 do begin
   prstr,strjustify(['Processing Times: ' + grid(i) + ' -TO- ' + grid(i+1)],/box)
   if n_elements(waves) eq 0 then waves=strtrim([171,195,284,304],2)
   prstr,strjustify(['Output directory: '+alldirs(i)],/box)
   for j=0,n_elements(waves)-1 do begin
      cmd='eit_fulldisk,grid(i),grid(i+1),index,' +  $
          'hours=hours,nmissb=nmissb,sizes=sizes,/w'+waves(j)
      print,'CMD> '+cmd
      estat=execute(cmd)
      fnames=gt_tagval(index,/filename)
      oldnames=eit_file2path(fnames)
      if incwave then prefix='efr_'+waves(j)+'_'      
      newnames=concat_dir(alldirs(i),str_replace(fnames,'efr', prefix))
      case  1 of 
         copyit: eit_fdspawn, oldnames, newnames,/copy
         linkit: eit_fdspawn, oldnames, newnames,/link
         prepit: begin
            message,/info,"eit_prep not yet supported..."
         endcase
      else: message,/info,"please specify ONE { /copy, /link, /eit_prep }
      endcase
   endfor
endfor

return
end
