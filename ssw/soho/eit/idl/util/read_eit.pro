pro read_eit, files, index, data, header=header, nodata=nodata, dstop=dstop, $
              outsize=outsize, comment=comment, ccnts=ccnts
;+
;   Name: read_eit
;
;   Purpose: read eit image(s) into 2D or 3D array & map header->structure 
;
;   Input Parameters:
;      files - FITS file name(s) to read
;      
;   Output Parameters:
;      index - IDL structure containing  FITs information (vector)
;      data  - 2D or 3D array of EIT images
;      nodata - switch, if set, only read and map headers (faster)
;
;   Calling Sequence:
;      read_eit, files, index , /nodata		 ; only read headers
;      read_eit, files, index, data		 ; return 2D or 3D cube
;      read_eit, files, index, data, outsize=256 ; rebin all DATA to outsize
;
;   Keyword Parameters:
;      outsize (input)  - output size (rebinning) - scalar size or [x,y]
;      header  (output) - FITS header of last file in filelist
;      comment (output) - COMMENT records for all input files 
;      ccnts   (output) - used with COMMENT in call to 'mreadfits_info'
;
;   History:
;      27-apr-1996 S.L.Freeland 
;       9-May-1996 S.L.Freeland - pass NODATA through to mreadfits
;      24-May-1996 S.L.Freeland - update MJD tag
;      20-aug-1996 S.L.Freeland - fill in expdur tag (exptime+shutclose)
;      22-oct-1996 S.L.Freeland - add 3D file ("lz") support
;      23-oct-1996 S.L.Freeland - use anytim(ints,out_style='utc_int')
;                                 (in place of anytim2utc(stringtime) )
;      10-dec-1996 S.L.Freeland - protect against old QKL (no SHUTTER CLOSE...)
;      14-jan-1997 S.L.Freeland - update DATE_OBS -> CCSDS (soho 'standard')
;                                 add INITIALIZE keyword in eit_fxpar 3D call
;      15-jan-1997 S.L.Freeland - add OUTSIZE (pass to mreadfits)
;      28-jan-1997 S.L.Freeland - update for enhanced mreadfits (some 3D work)
;                                 (removes single 3D file restriction)
;      11-apr-1997 S.L.Freeland - avoid carrying large coment tag around
;      12-apr-1997 S.L.Freeland - add documentation, COMMENT and CCNTS output
;       7-may-1997 S.L.Freeland - extract non-3D "COMMENT KEYWORDS"
;                                 (protect against SHUTTER CLOSE ss problem?)
;      29-may-1997 S.L.Freeland - make new stuff work for single input file!
;      23-feb-1998 J.S. Newmark - add path finding for filenames
;      26-mar-1998 J.S. Newmark - add handling for corrected date_obs
;      06-apr-1998 J.S. Newmark - fix pointing info for old data
;       8-Feb-1999 S.L.Freeland - resolve times
;      22-apr-1999 S.L.Freeland - only fill date_obs from coor_obs if not null
;      28-jul-1999 J.S.Newmark  - fix bug in corr_obs field
;      15-Jan-2002 S.L.Freeland - handle case of mixed corr_obs (null and non-null)
;
;   Method:
;      use mreadfits.pro for reading and structure mapping
;   
;   Restrictions:
;-

stat = is_fits(files(0))
if stat eq 0 then begin
  filenames = eit_file2path(files) 
  stat = is_fits(filenames(0))
  if stat eq 0 then begin
      filenames = eit_file2path(files,/collapse)
      stat= is_fits(filenames(0))
      if stat eq 0 then begin
         message,'The files do not appear to be FITS files.',/info
         return
      endif
  endif
endif else filenames = files

mreadfits, filenames, index, data, strtemplate=eit_struct(ncomment=20), header=header, $
   nodata=(keyword_set(nodata) or n_params() le 2), /quiet, outsize=outsize,   $
   comment=comment, ccnts=ccnts

nfiles=n_elements(files)                          ; number files
nindex=n_elements(index)                          ; number index records

; ------------------ special EIT COMMENT/KEYWORD handle ---------------
;                       7-may-97 - S.L.Freeland
;
; define mapping between tagname and header/comment name ;
tagnam=str2arr('P1_X,P2_X,P1_Y,P2_Y,NMISSB,CORR_OBS')                
parnam=str2arr('P1_X,P2_X,P1_Y,P2_Y,N_MISSING_BLOCKS,CORRECTED DATE_OBS')  

for i=0,n_elements(tagnam)-1 do begin
   ss=where(strpos(index.comment,parnam(i) ) ne -1,cnt)
   if cnt eq nindex then begin
       tvalue=strsplit(((index.comment))(ss),'=',/tail)    
       if parnam(i) eq 'CORRECTED DATE_OBS' then begin
           rep_a = where(strpos(tvalue,"'") ne -1,repcnt)
           if repcnt ne 0 then tvalue(rep_a) = str_replace(tvalue(rep_a),"'","")
           tvalue = strtrim(tvalue,2)
       endif else tvalue = fix(tvalue)  
       if nindex eq 1 then tvalue=tvalue(0)
       index.(tag_index(index(0),tagnam(i)))= tvalue
   endif
endfor
; ----------------------------------------------------------------------

; ----------------------------- handle 3D files ----------------------------
;
;   28-jan-97 - S.L.Freeland - simplified to take advantage of 3D work now
;               done within mreadfits.pro
; --------------------------------------------------------------------------

if nfiles ne nindex then begin                    ; Implies at least 1 3D
   funiq=uniq(index.filename,/first)              ;    Uniq filenames  
   ss3d=where(index(funiq).naxis3 gt 0,ss3dcnt)   ;    3D file transitions

   ;  define mapping between eit_fxpar call and FITS/STR tag name
   sub_par  =strupcase(str2arr("start_time,exp_time,wav,fltr,n_mb,corr start_time" ))
   fit_par  =strupcase(str2arr("time_obs,exptime,wavelnth,filter,nmissb,corr_obs"))

   for i=0,ss3dcnt-1 do begin                          ; for each 3D file
       s0=funiq(ss3d(i)) & s1=s0+index(s0).naxis3-1    ; index subscripts
       version=float(strmid(index(i).history,8,3))
       for j=0,n_elements(sub_par)-1 do begin          ; for each multi-param.
          subpar=sub_par(j)
          comm3d=mreadfits_info(comment,ccnts,ss3d(i))
          phead=['NAXIS   = 3','NAXIS3  = '+strtrim(index(funiq(ss3d(i))).naxis3 ,2)] 
          if subpar eq 'CORR START_TIME' and version lt 2.0 then subval='' $
             else subval=eit_fxpar([phead,temporary(comm3d)], subpar, image='all')
          index(s0:s1).(tag_index(index(0),fit_par(j)))=subval
       endfor

       ; ---- adjust 3D tags for consistency with "standard" EIT tags ---
       index(s0:s1).date_obs=gt_day(index(s0:s1).time_obs,/string)
       index(s0:s1).time_obs=gt_time(index(s0:s1).time_obs,/string)
       ; -----------------------------------------------------------------
  endfor
endif
; --------------------------- end of 3D handle -----------------------------

; update DAY and TIME fields so YOHKOH and SOHO dbase routines (utplot et al)

; -- CORR_OBS -> DATE_OBS - minor mod, S.L.F. 22-apr-1999 -
;                           assure CORR_OBS is non null
corrobs=gt_tagval(index,/corr_obs,missing='')
ssco=where(corrobs ne '',cocnt)
if cocnt gt 0 then index(ssco).date_obs=corrobs(ssco) 
delvarx,corrobs

if strlen(gt_tagval(index(0),/date_obs)) ge 22 then begin 
   ints=anytim(index.date_obs,/ints)
endif else begin    
   ints=anytim2ints( index.time_obs + ' ' + index.date_obs) ; string-> DAY&TIME
   index.date_obs=anytim(ints,out_style='CCSDS')
endelse
utc =anytim(ints,out_style='utc_int')                    ; DAY -> MJD
index.time=ints.time
index.day =ints.day
index.mjd = utc.mjd


; -------- get shutter dwell and calculate expdur ----------------

index.expdur = index.exptime
version = float(strmid(eit_fxpar(index,'history'),8,3))
early = where(version lt 2.0,ecnt)

if ecnt gt 0 then begin
   sct=eit_fxpar(index(early),'SHUTTER CLOSE TIME')>2.
   if n_elements(sct) eq n_elements(early) then begin
      if n_elements(sct) eq 1 then sct = sct(0)   ;DMZ/JSN 03/37/98
      index(early).expdur = index(early).expdur + sct
   endif else begin
      message,/info,"Warning - mismatch between SHUTTER CLOSE TIME / EARLY"
   endelse
endif   

; -------- fix for pointing in early versions of data ----------------
early = where(version lt 2.6,ecnt)
if ecnt gt 0 then begin
    scale = float(round(index(early(0)).cdelt1/eit_pixsize()))
    index(early).cdelt1 = eit_pixsize()*scale
    index(early).cdelt2 = eit_pixsize()*scale
    for ii = 0,n_elements(ecnt) -1 do begin
        xy = eit_point(index(early(ii)).date_obs)/scale
        index(early(ii)).crpix1 = xy(0)
        index(early(ii)).crpix2 = xy(1)
    endfor
endif

return
end
