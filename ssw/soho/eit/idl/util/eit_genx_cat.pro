pro eit_genx_cat, index, text, cat_name=cat_name, full_fov=full_fov, $
   name_only=name_only, generate=generate, first=first, last=last
;+
;   Name: eit_genx_cat
;
;   Purpose: generate and/or read local eit qkl catalog
;
;   Input Parameters:
;      NONE
;
;   Output Parameters:
;      index - IDL 'eit_struct.pro' style structures for last few days
;      text  - text version of index (via get_infox)
;
;   Keyword Parameters:
;      cat_name  - (output) - name of current catalog
;      name_only - (input)  - switch, just return catalog name via CAT_NAME
;      full_fov  - (input)  - if set, read FULL FOV catalog 
;
;      generate  - (input)  - switch, GENERATE catalog (default is just read)
;                             (generally , via CRON job)
;      last      - (input)  - only with GENERATE, number of days to include 
;                             (most recent NN=LAST days, default=2)
;
;   Calling Sequence:
;      eit_genx_cat, eitstructs, eitinfo            ; most recent few days
;      eit_genx_cat, eitstructs, eitinfo, /full_fov ; same, only full FOV (speed)
;
;      eit_genx_cat,/generate           ; Make it, via priviledged cron job
;
;   History:
;      3-mar-1997 - generate/read EIT_STRUCT (via read_eit.pro) based catalog 
;                   (replace old "temporary" ascii catalog)
;     11-mar-1997 - make a smaller full fov also
;
;   Calls:
;      eit_files, read_eit, savegen OR restgen
;  
;   Method:
;      uses 'genx' files - see SAVEGEN and RESTGEN for more details
;-
if not keyword_set(last) then last=2

genxcat='eit_qklcat.genx'
cat_name=concat_dir('$SSW_SITE_LOGS',genxcat)   
full_name=str_replace(cat_name,'qkl','qkl_fullfov')
whichcat=([cat_name,full_name])(keyword_set(full_fov))

if keyword_set(name_only) then return           ;*** early exit!! - name only

if keyword_set(generate) then begin   
   files=eit_files(last=last<7)                 ; 1 week at most
   read_eit,files,index                         ; read headers
   text=get_infox(index,/fmt_tim, $
	  'WAVELNTH,FILTER,naxis1,naxis2,EXPDUR,OBJECT', $
        format='a,a,a,a,a,a')
   savegen,file=cat_name,index,text
   ssfull=where(index.object eq 'full FOV')
   savegen,file=full_name,index(ssfull),text(ssfull)
endif else restgen,file=whichcat, index, text

return
end
