function eit_files, t0, t1, quicklook=quicklook, last=last, newest=newest, lz=lz, $
		    collapse=collapse, fulldisk=fulldisk, wave=wave, $
		    matchfiles=matchfiles, delta=delta, debug=debug,quiet=quiet
;+
;   Name: eit_files
;
;   Purpose: return online eit file names
;
;   Input Parameters:
;      t0, t1 - Optional start time and stop time 
;      
;   Keyword Parameters:
;      last - number of days to look at (Most recent N days)
;      collapse - SWITCH, if set, all files in one directory
;      full_disk - if $EIT_FULLDISK is defined, use that tree
;      matchfiles - instead of t0/t1, pass in list of filenames/times
;                   and get best match (at same or different wavelength)
;      delta - only if MATCHFILES input, deltaT(sec) between desired/return
;
;   Calling Sequence:
;      files=eit_files
;
;   Restrictions:
;      assume environmental EIT_QUICKLOOK points to collapsed tree or top of tree
;
;   History: 
;      15-jan-1996 (S.L.Freeland) for LPARL access
;      14-feb-1996 (S.L.Freeland) allow uncollapsed tree , LAST keyword 
;       3-Jun-1996 (S.L.Freeland) check EIT_QUICKLOOKnn
;       7-aug-1996 (J. Newmark) add lz (level-zero) keyword and associated
;                        lz queries, allow one or more QUICKLOOK archives
;      25-mar-1997 (S.L.Freeland) - add COLLAPSE keyword and function
;                                   recurse w/collapse when required 
;                                   add T0 and T1 parameters
;       6-apr-1997 (S.L.Freeland) - trim files to true t0 and t1 if supplied
;       1-may-1997 (S.L.Freeland) - fix problem if auto-collapse search tried
;                                   (recursive logic)
;       3-jun-1997 (S.L.Freeland) - add /FULLDISK and WAVE keywords
;                                   (required $EIT_FULLDISK defined)
;      11-jun-1997 (S.L.Freeland) - fix of "minor" 3-jun bug (/full)
;      26-Jun-1997 (S.L.Freeland) - add MATCHFILES(in) and DELTA(out)
;      13-oct-1997 (S.L.Freeland) - fixed problem with LAST 
;       8-Apr-1998 (S.L.Freeland) - use current directory if topdir not set
;      10-sep-1998 (S.L.Freeland) - permit only t0
;      15-feb-1999  (S.L.Freeland)   - use qkl if lz not found
;      23-apr-1999 (S.L.Freeland)   - fix problem for small time ranges
;                                     which span a month (subdirectory) boundry
;                                    Allow time vector as t0 (use matchfiles logic)
;       2-Jan-1999  slf - quick y2k fix (still need to look at year spanning)
;       15-Aug-2001 Zarro (EITI/GSFC) - modified to return blank string when
;                                    no files found
;       1-Sept-2001, Zarro (EITI/GSFC) - added /QUIET
;-
quicklook=keyword_set(quicklook)
fulldisk=keyword_set(fulldisk) or keyword_set(matchfiles)
collapse=keyword_set(collapse) or $
	  strlowcase(get_logenv('EIT_DATA_STYLE')) eq 'collapsed'
if keyword_set(newest) then last=newest/15 + 2
quiet=keyword_set(quiet)

if keyword_set(last) then begin
   t1=ut_time()
   t0=timegrid(t1,day=-(last),/string)
   fulldisk=1
   collapse=1
endif else lz=(1-fulldisk) and (1-quicklook)

if n_elements(t0) gt 1 and n_elements(t1) eq 0 then begin
   t0temp=t0                         ; dont clobber input 
   matchfiles=time2file(t0)          ; use matchfiles logic
endif

only_one=n_elements(matchfiles) eq 1
if only_one then matchfiles=replicate(matchfiles,2)
case 1 of
   data_chk(matchfiles,/undefined):
   data_chk(matchfiles,/string): begin
      ftimes=file2time(matchfiles,out='yohkoh')
      time_window,ftimes,t0,t1
      t0=timegrid(t0,hour=-6,/string)
      t1=timegrid(t1,hour=6,/string)
   endcase
   data_chk(matchfiles,/struct): begin
      ftimes=matchfiles           
      time_window,ftimes,t0,t1
      t0=timegrid(t0,hour=-6,/string)
      t1=timegrid(t1,hour=6,/string)
   endcase  
endcase   

file_template='efr*'

case  1 of 
   keyword_set(lz): begin
     eittop=get_logenv('EIT_LZ') 
     file_template='efz*'
   endcase
;  ------------------- eit full disk ------------------
   fulldisk: begin
      eittop=get_logenv('EIT_FULLDISK')
      if eittop eq '' then message,"NO $EIT_FULLDISK"
      if data_chk(t1,/undef) then t1=timegrid(t0,hours=24,/string)
      days=timegrid(t0,t1,hour=6)
      yyyymm=strmid(time2file(days),0,6)
      yyyymm=yyyymm(uniq(yyyymm))
      if keyword_set(wave) then swave=strtrim(wave,2) else swave='*'
      if keyword_set(debug) then stop
      eitfiles=file_list(concat_dir(eittop, yyyymm), 'ef*'+swave+'_'+'*',/cd,quiet=quiet)
      fids=strsplit(eitfiles,'_',/last,/tail)
      t0f=time2file(t0,delim='.')
      t1f=time2file(t1,delim='.')
      ss=where(fids ge t0f and fids le t1f, sscnt)
      retval=''
      if sscnt eq 0 then begin
       message,/info,"No files betweent t0 and t1"
       eitfiles=''
      endif else begin
	if n_elements(ftimes) eq 0 then retval=eitfiles(ss) else begin
             newtimes=file2time(eitfiles(ss),out_style='ints')
	     sss=tim2dset(newtimes,ftimes,delta=delta)
             if only_one then sss=sss(0)
             retval=eitfiles(ss(sss))
          endelse
      endelse
      if keyword_set(newest) then begin
         nf=n_elements(retval)
         retval=retval(nf-newest>0:nf-1)   
      endif
      case 1 of 
         n_elements(t0temp) gt 0: t0=t0temp              ; restore input
         n_params() eq 1: begin 
             ss=tim2dset(file2time(retval,out='int'),t0)
             retval=retval(ss(0))
         endcase
         else:
      endcase
      if n_elements(retval) eq 1 then retval=retval(0) 
      return,retval
;   -------------------------------------------------------------
    endcase
   else: eittop=get_logenv('EIT_QUICKLOOK0*')         ;default = QUICKLOOK
endcase

if eittop(0) eq '' then eittop=get_logenv('EIT_QUICKLOOK') ;one archive


if eittop(0) eq '' then begin
   curfiles=findfile()
   efz=wc_where(curfiles,'ef?????????.??????',/case_ignore,mcount)
   if mcount gt 0 then begin
      if n_elements(t0) eq 0 then t0= '1-jan-1997'
      if n_elements(t1) eq 0 then t1=ut_time()
      eittop=curdir()
      collapse=1              
   endif else begin
      message,/info,"Top level not defined..."
      return,''
   endelse
endif 

delim=(['/',':'])(!version.os eq 'VMS')
grid=timegrid(t0,t1,/day,/string,/quiet)


subdir= strmid(time2file(grid(0)),0,2) + strmid(grid,7,2) + delim + $
     string(month_id(strmid(grid,3,3)),format='(i2.2)') 

lz_tmp=str_sep(subdir(0),delim)
lz_start=lz_tmp(0)+lz_tmp(1)+string(strmid(grid(0),0,2), format='(i2.2)')
if keyword_set(lz) then subdir=subdir(uniq(string(month_id(strmid(grid,3,3)),$
       format='(i2.2)'))) else subdir = subdir + delim + $
          string(strmid(grid,0,2), format='(i2.2)') 

case 1 of
   keyword_set(collapse): begin
        filter=strcompress(str_replace(subdir,delim,' '),/remove)
	eitfiles=file_list(eittop(0),file_template+filter+'*',quiet=quiet)
     endcase
     else: begin
        eitdir=concat_dir(eittop(0),subdir)
        for i=1,n_elements(eittop)-1 do eitdir=[eitdir,concat_dir(eittop(i),subdir)]
        which=where(file_exist(eitdir),dircnt)
        if dircnt eq 0 then begin
	   message,/info,"No directories found - trying COLLAPSED style...
           eitfiles=eit_files(t0,t1, quicklook=quicklook, last=last, newest=newest, lz=lz,/collapse)
        endif else eitfiles=file_list(eitdir(which),file_template,/cd,quiet=quiet)
        if eitfiles(0) eq '' and (1-quicklook) then begin
                if not quiet then box_message,'No LZ files, trying quicklook'   
                eitfiles=eit_files(t0,t1,/quicklook)
        endif

   endcase
endcase

;if keyword_set(lz) and eitfiles(0) ne '' then begin
;   ok=where(strpos(eitfiles,lz_start) ne -1)
;   eitfiles=eitfiles(ok(0):n_elements(eitfiles)-1)
;endif

if trim(eitfiles(0)) eq '' then return,eitfiles

if keyword_set(newest) then begin
   nf=n_elements(eitfiles)
   eitfiles=eitfiles(nf-newest>0:nf-1)   
endif

if n_elements(t1) gt 0 then begin
   times=file2time(eitfiles,out='ints')
   ss=sel_timrange(times,t0,t1,/between)
   if ss(0) eq -1 then begin
      message,/info,"No files between your start and stop" 
      eitfiles=''
   endif else eitfiles=eitfiles(ss)
endif

return,eitfiles
end
