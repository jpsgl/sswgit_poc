pro eit_dump, filenam, post=post, gif=gif, jpeg = jpeg, encap=encap, $
       printer=printer,$
       outdir=outdir, window=window, surround=surround, file=file,$
       annotate=annotate, image_no=image_no
;+
;   Name: 
;         eit_dump
;   Purpose: produce postscript or gif files
;
;   Input Parameters:
;     filenam - FITS file of image
;
;   Output Parameters:
;      file - name format is efr'date'_wave.'ps or gif'
;      
;   Keyword Parameters:
;     post    - set for postscript output - (default)
;     gif     - set for gif output -image only no labels
;     jpeg    - set for JPEG output - image only, no labels
;     encap   - set for encapsulated postscript output
;     printer - set for printing to value
;     outdir   - set for output directory of saved file 
;     window  - set to dump current window
;     surround - set to replace missing blocks with surrounding ones
;     file    - set for output file name
;     annotate -set to string if wish annotated PS output for window dump
;     image_no -set for image_no in 3d LZ file
;
;   Calling Sequence:
;      eit_dump,'efr19960702.123456',/post  
;
;   Method:
;      read FITS file, load proper color table, subtract background, 
;      replace missing blocks, degrid, bytscl(alog10(image)), save 
;      in selected format
;  
;   History:
;     2-Jul-1996 - J. Newmark  - created
;    22-Jul-1996 - J. Newmark  - allow for capture of IDL window
;    23-Jan-1997 - J. Newmark  - allow file keyword for output
;    18-Feb-1997 - J. Newmark  - call eit_image for processing, add
;                                annotate keyword
;    19-May-1997 - J. Newmark  - add image_no keyword
;    1997 May 22 - D.M. fecit  - added JPEG keyword
;-

 if keyword_set(gif) or keyword_set(jpeg) then post = 0 else post = 1
 if keyword_set(post) then post = 1
 if keyword_set(encap) then post = 1 else encap = 0
 if n_elements(image_no) eq 0 then image_no = 0
 if not keyword_set(outdir) then if !version.os eq 'vms' then $
      outdir = '[eit.pr]' else begin
              cd, current = outdir
              outdir = outdir+'/'
      endelse
 if keyword_set(window) then begin
    winnum = !d.window
    device = !d.name
    if device eq 'X' then wset,winnum
    case 1 of
      post: big_gif_file_name = 'window_'+strtrim(winnum,2)+'.ps'
      gif:  big_gif_file_name = 'window_'+strtrim(winnum,2)+'.gif'
      jpeg: big_jpeg_file_name = 'window_' + strtrim(winnum, 2) + '.jpg'
    endcase
;
    if keyword_set(file) then begin
         big_gif_file_name = file
         big_jpeg_file_name = file
      end
    end
;
    tvlct, r, g, b, /get
;
    a2 = tvrd()
    set_plot,'ps'
    device,/inches,xoff=1.,xsize=6.5,yoff=3.5,ysize=6.5,/palatino,/isolatin
    if post then begin
         if encap then device,/color,bits=8,file=big_gif_file_name(0),/encap $
              else device,/color,bits=8,file=big_gif_file_name(0)    
         tvlct,r,g,b 
         tv,a2
         if keyword_set(annotate) then begin
             for k = 0,n_elements(annotate)-1 do xyouts,3.25*2540,$
               (-1-k*0.5)*2540,annotate(k),/device,align=0.5,chars=2,charth=1.5 
         endif
    endif else if gif then begin
      write_gif, big_gif_file_name(0), a2, r,g,b
       print,'%EIT_DUMP-I-GIF, wrote ' + big_gif_file_name
    endif else if jpeg then begin
      image = [[[r(a2)]], [[g(a2)]], [[b(a2)]]]
      write_jpeg, big_jpeg_file_name(0), image, true = 3, qual = 90
      print, '%EIT_DUMP-I-JPEG, wrote ' + big_jpeg_file_name(0) + '.'
   end
    device,/close
    set_plot,device
 endif else begin
  wave = ['171', '195', '284', '304']
  wave_names = ['Fe IX/X','Fe XII','Fe XV','He II']
  semi_colon = string(59b)
  s = filenam  
  n_file = n_elements(s)
  found = intarr(4)
  date_string = strarr(4) 
;
  stat = is_fits(s)
  if stat ne 1 then s = eit_file2path(s) 
  h = headfits(s)
  this_wave = strtrim(eit_fxpar(h,'WAVELNTH',image_no=image_no),2)
  for i_wave = 0, 3 do if this_wave eq wave(i_wave) then j_wave = i_wave
  time_obs=eit_fxpar(h,'DATE_OBS',image_no=image_no)
;
  if not found(j_wave) then begin
     found(j_wave) = 1
     print, '%FOUND_' + wave(j_wave) + ', file = ' + s
     lower_case_file = strlowcase(s)
     ich_0 = strpos(lower_case_file, 'efr')
     if ich_0 eq -1 then ich_0 = strpos(lower_case_file, 'efz')
     ending = strpos(lower_case_file, '.')
     if ending eq -1 then ending = strlen(lower_case_file)
     ich_1 = ending - 1
     nch = ich_1 - ich_0 + 1
     file_name = strmid(lower_case_file,ich_0,nch)
;
     if keyword_set(surround) then $
         a2 = eit_image(s,/surround,/flat,dark=eit_dark(),image_no=image_no) $
     else a2 = eit_image(s,repl=eit_dark(),/flat,dark=eit_dark(),$
               image_no=image_no)
;
     file_name=concat_dir(outdir,file_name)
     case 1 of
       post: big_gif_file_name = file_name  + '_' + wave(j_wave) + '.ps'
       gif:  big_gif_file_name = file_name  + '_' + wave(j_wave) + '.gif'
       jpeg: big_gif_file_name =file_name  + '_' + wave(j_wave) + '.jpg'
     endcase
     if keyword_set(file) then big_gif_file_name = file
     device = !d.name
     font = !p.font
     set_plot,'ps'
     !p.font = 0  
     angstrom = 'A'   
     if post then begin
          if encap then device,/color,bits=8,file=big_gif_file_name(0),/encap $
              else device,/color,bits=8,file=big_gif_file_name(0)    
          angstrom=string(197b)
     endif else device,/color,bits=8
     device,/Palatino,/isolatin,/inches,xoff=1.,xsize=6.5,yoff=3.5,ysize=6.5
     loadct,42+j_wave,file=getenv('coloreit')
     tvlct,r,g,b,/get
     text1 = 'SOHO - EIT '+ wave_names(j_wave) +' '+wave(j_wave)+angstrom
     text2 = strmid(anytim2utc(time_obs,/ecs),0,16) +' UT' 
     if post then begin
         tv,a2
         xyouts,3.25*2.54*1000,-1.*2.54*1000,text1,/device,align=0.5,chars=2,$
             charth=1.5 
         xyouts,3.25*2.54*1000,-1.5*2.54*1000,text2,/device,align=0.5,chars=2,$
             charth=1.5 
     endif else if gif then begin
       write_gif, big_gif_file_name(0), a2, r,g,b
        print,'%EIT_DUMP-I-GIF, wrote ' + big_gif_file_name
     endif else if jpeg then begin
       image = [[[r(a2)]], [[g(a2)]], [[b(a2)]]]
       write_jpeg, big_gif_file_name(0), image, true = 3, qual = 90
       print, '%EIT_DUMP-I-JPEG, wrote ' + big_gif_file_name(0) + '.'
     end
     device,/close
     set_plot,device
     !p.font = font 
   end
  endelse
  if keyword_set(printer) and post then begin
       if !version.os eq 'vms' then spawn, 'print /queue = ' + $
                  printer + ' ' + big_gif_file_name(0) else $
              spawn, 'lpr -P' + printer + ' '+ big_gif_file_name(0)
       print,'%EIT_DUMP sent file to printer ',printer
  endif
end
