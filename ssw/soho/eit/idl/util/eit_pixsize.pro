;+
; NAME
;  EIT_PIXSIZE
;  return pixel scale, i.e. arcsec/pixel
;  "/pix=(0.206265/focal length)*pixel size microns
;       =(0.206265/1.652)*21=2.62
;
;   However, a problem arises with comparison of features with other
;   SOHO instruments. Part of the problem is caused by the physical
;   size difference of the solar disk in the photoshphere and transition
;   region and corona. Part of the problem is unknown. It may be that 
;   physical size of the pixels is 20.9 microns.
;
;-
FUNCTION eit_pixsize
RETURN,2.62
END
