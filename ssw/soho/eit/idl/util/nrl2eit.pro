function nrl2eit,nrlhdr
;+
; NAME:
;	NRL2EIT
;
; PURPOSE:
;       To make a NRL formatted EIT FITS file compatible with EIT software.
;
; CATEGORY:
;	FITS processing
;
; CALLING SEQUENCE:
;       eithdr = nrl2eit(nrlhdr)
;
; INPUTS:  NRL formatted FITS header
;
; KEYWORD PARAMETERS: none
;
; OUTPUTS:
;	FITS header comptaible with EIT software.
;
; COMMON BLOCKS: none.
;
; SIDE EFFECTS: none.
;
; RESTRICTIONS: none.
;
; PROCEDURE:
;          The only changes needed are to add new keywords to the 
;          header which already contains this info. The necessary info
;          is grabbed out and re-written.
;
; MODIFICATION HISTORY:
; 	Written by:  J. Newmark 	Date.  Oct 08 1996
;	2001.6.21, N.Rich - Fixed date_obs and object
;       2002.8.06 J. Newmark, fixed EXPTIME
;
;-

eithdr = nrlhdr
wave = fxpar(nrlhdr,'SECTOR')
wave = fix(strmid(wave,0,3))
fxaddpar, eithdr,'WAVELNTH',wave
date_obs = fxpar(nrlhdr,'DATE-OBS')
fxaddpar, eithdr,'DATE_OBS',anytim2utc(date_obs,/ccsds)
;if eit_fxpar(eithdr,'NAXIS1') + eit_fxpar(eithdr,'NAXIS2') eq 2048 $
IF fxpar(nrlhdr,'R2COL') - fxpar(nrlhdr,'R1COL') $
  +fxpar(nrlhdr,'R2ROW') - fxpar(nrlhdr,'R1ROW')  EQ 2046 $
   then object = 'Full FOV' else object = 'Partial FOV' 
fxaddpar, eithdr,'OBJECT',object
fxaddpar, eithdr,'COMMENT','SHUTTER CLOSE TIME = ' +$
    strtrim(fxpar(nrlhdr,'EXP3'),2) + ' s'
fxaddpar, eithdr,'comment','P1_X =    ' + strtrim(fxpar(nrlhdr,'P1ROW'),2)
fxaddpar, eithdr,'comment','P2_X =    ' + strtrim(fxpar(nrlhdr,'P2ROW'),2)
fxaddpar, eithdr,'comment','P1_Y =    ' + strtrim(fxpar(nrlhdr,'P1COL'),2)
fxaddpar, eithdr,'comment','P2_Y =    ' + strtrim(fxpar(nrlhdr,'P2COL'),2)
fxaddpar, eithdr,'HISTORY','Processed at NRL, header converted by NRL2EIT'
return, eithdr
end

