pro eit_postage,indate,lz=lz,gif=gif,use_ct=use_ct

;+
; NAME        : 
;              EIT_POSTAGE
; PURPOSE     : display small images of whole day on one screen
;
; CATEGORY    : image summary
;
; EXPLANATION :  
;
; SYNTAX      : eit_postage
;
; EXAMPLES    : none
;
; CALLED BY   :
;
; CALLS TO    : put, eit_catrd, label_image, eit_file2path, readfits
;
; ARGUMENTS   :
;       INDATE  - string of the day to display
;
; KEYWORDS    :
;       LZ      - if set then display level zero data, not quicklook data
;       GIF     - if set then output a GIF file
;       USE_CT  - if set creates a color table for each wavelength
;
; OUTPUTS     : IF GIF set then a file name eit_YYYY-DD-MM.gif
;
; COMMON      : none
;
; RESTRICTIONS: none
;
; SIDE EFFECTS: 
;      If USE_CT keyword is set will mess up the color tables. User
;      should reset them by running loadct,0 after using this routine
;
; PROJECT     : SOHO - EIT
;
; HISTORY     : V1,  Jeff Newmark, 24-jan-1997
;               1997 Feb 21 - J. Newmark use multiple color tables
;		1997 Mar 17 - E. Einfalt add titles
;
; CONTACT     : newmark@eitv2.nascom.nasa.gov
;-

defsysv,'!image',exist=ok
if not ok then begin
     imagelib
     devicelib
endif

if n_elements(lz) eq 0 then lz = 0 else lz = 1
list = eit_catrd(indate,lz=lz,/raw)
files = eit_file2path(strlowcase(list.file_name))
nfiles = n_elements(files)
nimgs = total(list.n_repeat >1)
data = fltarr(256,256,nimgs)
times = strarr(nimgs)
waves = intarr(nimgs)
num = 0

for i = 0, nfiles -1 do begin
;  print,'Reading '+files(i)
  orig = readfits(files(i),hdr) 
  n_repeat = (list(i).n_repeat>1)
  orig = congrid(temporary(orig),256,256,n_repeat)
  orig = alog10((orig-848)>1)
  data(0,0,num) = orig
  time = strmid(eit_fxpar(hdr,'date_obs',image_no='all'),11,8)
  if i eq 0 then times = time else times = [times,time]
  wave = eit_fxpar(hdr,'WAVELNTH',image_no='all')
  if i eq 0 then waves = wave else waves = [waves,wave]
  num = num + n_repeat 
  orig = 0
endfor

type_data = ['Raw FITS', 'LZ FITS']
if n_elements(indate) eq 0 then indate = anytim2utc(!stime,/ecs,/date)
title = type_data(keyword_set(lz)) + '  ' + indate

if keyword_set(gif) then begin
   device = !d.name
   set_plot, 'Z' 
   device, set_resolution = [1024,1024]
endif else window, /free, xs = 1024, ys = 1024, title = title

wave=[171,195,284,304]
eitcolors = getenv('coloreit')
ncolors=!d.n_colors/4

for i = 0, nimgs-1 do begin
  itable=(where(waves(i) eq wave))(0)
  if keyword_set(use_ct) then begin
     loadct,file=eitcolors,itable+42,ncolors=ncolors,bottom=itable*ncolors,$
           /silent
     dimage = bytscl(data(*,*,i), top = ncolors-1) + byte(itable*ncolors)
     put, dimage, i+1, nimgs, /noexact, relat=0.85, /noscale
     dimage = 0
  endif else $
     put, data(*,*,i), i+1, nimgs, /noexact, relat = 0.85
  label_image, times(i), /below
endfor

if keyword_set(gif) then begin
  img = tvrd()
  tvlct, r,g,b,/get
  fname = 'eit_' + anytim2utc(indate,/ccsds,/date) + '.gif'
  write_gif, fname,img,r,g,b
  set_plot, device
endif

return
end
