
function eit_getcal, date
;+
;NAME
;   eit_getcal
;PURPOSE
;  computes a calibration lamp for a given date
;INPUTS
;   date_obs  = date of observation
;OUTPUTS
;   returns a computed calibration lamp interpolated or extrapolated form the closed calibration lamps
;KEYWORDS
;   none
;WARNINGS
;   still in test
;   may no return correct values for the most recent bakeout periods
;PROCEDURE
;   interpolates or extrapolates from the closest calibration lamps
;CREATION
;   18-May-2001. F. Auchere
;MODIFICATIONS
;   12-Dec 2001. F. Auchere call to eit_find_last_cal added to avoid
;                blow up when no recent cal lamp is found
;   12-Jan 2003. F. Auchere changed a b and c variables type to float
;   06-Sep 2004. F. Auchere correct calls to eit_fillmissingblocks
;-

  next_start = ' '
  next_end = ' '
  previous_start = ' '
  previous_end = ' '
  next_start2 = ' '
  next_end2 = ' '
  previous_start2 = ' '
  previous_end2 = ' '
  next_cal = eit_findcalgroup(date, 1, start_date=next_start, end_date=next_end)
  previous_cal = eit_findcalgroup(date, -1, start_date=previous_start, end_date=previous_end)

  if (n_elements(next_cal) eq 1) and (n_elements(previous_cal) eq 1) then return, eit_fillmissingblocks(eit_find_last_cal(date))

; jsn 15-Oct-2001 test, extrapolations appear to greatly enhance noise

  if (n_elements(next_cal) eq 1) then return, eit_fillmissingblocks(previous_cal)

  if (n_elements(next_cal) eq 1) then begin

    previous_cal2 = eit_findcalgroup(previous_end, -1, start_date=previous_start2, end_date=previous_end2)
    if (n_elements(previous_cal2) eq 1) then return, eit_fillmissingblocks(previous_cal) $
    else begin
      t1 = utc2sec(anytim2utc(previous_start))
      t2 = utc2sec(anytim2utc(previous_end2))
      if ((t1 - t2) gt 86400.0*2.0) then begin
        t = (utc2sec(anytim2utc(date)) - t1)<(86400.0*30.0d)
        return, eit_fillmissingblocks(previous_cal + (previous_cal2 - previous_cal)*t/(t2 - t1))
      endif else return, eit_fillmissingblocks(previous_cal)
    endelse

  endif

  if (n_elements(previous_cal) eq 1) then begin

    next_cal2 = eit_findcalgroup(next_end, 1, start_date=next_start2, end_date=next_end2)
    if (n_elements(next_cal2) eq 1) then return, eit_fillmissingblocks(next_cal) $
    else begin
      t1 = utc2sec(anytim2utc(next_start))
      t2 = utc2sec(anytim2utc(next_end2))
      if ((t2 - t1) gt 86400.0*2.0) then begin
        return, eit_fillmissingblocks(next_cal + (next_cal2 - next_cal)*(utc2sec(anytim2utc(date)) - t1)/(t2 - t1))
      endif else return, eit_fillmissingblocks(next_cal)
    endelse

  endif

  t = utc2sec(anytim2utc(date))
  t1 = utc2sec(anytim2utc(previous_end))
  t2 = utc2sec(anytim2utc(next_start))

  previous_cal = eit_fillmissingblocks(previous_cal)
  next_cal = eit_fillmissingblocks(next_cal)

;;;temporarily turns off any 3 points interpolation _ F. Auchere 14 janvier 2003
  ;if (n_elements(next_cal2) eq 1) and (n_elements(previous_cal2) eq 1) then $

  return, previous_cal + (next_cal - previous_cal)*(t - t1)/(t2 - t1)


  previous_cal2 = eit_findcalgroup(previous_end, -1, start_date=previous_start2, end_date=previous_end2)
  next_cal2 = eit_findcalgroup(next_end, 1, start_date=next_start2, end_date=next_end2)

  if n_elements(next_cal2) ne 1 then begin
    tnc2 = utc2sec(anytim2utc(next_start2))
    if ((tnc2 - t2) lt 86400.0*3.0) then next_cal2 = 1
  endif
  if n_elements(previous_cal2) ne 1 then begin
    tpc2 = utc2sec(anytim2utc(previous_end2))
    if ((t1 - tpc2) lt 86400.0*3.0) then previous_cal2 = 1
  endif

  if (n_elements(next_cal2) eq 1) then begin
    thirdcal = previous_cal2
    t3 = utc2sec(anytim2utc(previous_end2))
  endif else begin
    if (n_elements(previous_cal2) eq 1) then begin
      thirdcal = next_cal2
      t3 = utc2sec(anytim2utc(next_start2))
    endif else begin
      if (t - tpc2) lt (tnc2 - t) then begin
        thirdcal = previous_cal2
        t3 = tpc2
      endif else begin
        thirdcal = next_cal2
        t3 = tnc2
      endelse
    endelse
  endelse

  b = float(((previous_cal - thirdcal)*(t1^2.0 - t2^2.0) - (previous_cal - next_cal)*(t1^2.0 - t3^2.0))/((t1 - t3)*(t1^2.0 - t2^2.0) - (t1 - t2)*(t1^2.0 - t3^2.0)))
  a = float(((previous_cal - next_cal) - b*(t1 - t2))/(t1^2.0 - t2^2.0))
  c = float(previous_cal - a*t1^2.0d - b*t1)

  return, eit_fillmissingblocks(a*t^2.0 + b*t + c)


end

