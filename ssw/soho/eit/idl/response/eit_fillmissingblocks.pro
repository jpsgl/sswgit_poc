function eit_fillmissingblocks, img, bin=bin
;+
;NAME
;   eit_fillmissingblocks
;PURPOSE
;   a fast removal of missing blocks designed for calibration lamps
;INPUTS
;   img = input image
;OUTPUTS
;   returns the image with missing blocks replaced
;KEYWORDS
;   bin = binning of the input image. must be 16 (2x2 binned image) or 32 (full res)
;PROCEDURE
;   replace missing blocks with the nearest valib blocks
;CREATION
;   18-May-2001. F. Auchere
;MODIFICATIONS
;   12-Aug 2002. F. Auchere bug fixed
;   28-jul-2003. F. Auch�re changed replacement by nearby missing block by
;                           a biliear interpolation
;-

  sz = size(img)
  if n_elements(sz) eq 6 then nimg = sz[3] else nimg = 1
  fill = img

  if keyword_set(bin) then bsize = 16.0 else bsize = 32.0

  nx = sz[1]
  ny = sz[2]
  xblocks = nx/bsize
  yblocks = ny/bsize

  x = findgen(xblocks) # replicate(1.0, yblocks)
  y = replicate(1.0, xblocks) # findgen(yblocks)

  blocks = rebin(fix(img), xblocks, yblocks, nimg)

  for ni = 0, nimg-1 do begin

    missing = where(blocks[*, *, ni] eq 0, nmiss)
    good = where(blocks[*, *, ni] gt 0, ngood)
    if (nmiss ge 1) and (ngood ge 1) then begin

      xmiss = x[missing]*bsize
      ymiss = y[missing]*bsize

      xb = (findgen(bsize)/bsize) # replicate(1.0, bsize)
      yb = transpose(xb)

      for nm = 0, nmiss-1 do begin
        corners = fltarr(2, 2)
        corners[0, 0] = img[(xmiss[nm]-1)>0, (ymiss[nm]-1)>0]
        corners[0, 1] = img[(xmiss[nm]-1)>0, (ymiss[nm]+bsize)<(ny-1)]
        corners[1, 0] = img[(xmiss[nm]+bsize)<(nx-1), (ymiss[nm]-1)>0]
        corners[1, 1] = img[(xmiss[nm]+bsize)<(nx-1), (ymiss[nm]+bsize)<(ny-1)]
        good = where(corners gt 0, count, complement=bad)
        if count lt 4 then corners[bad] = median(corners[good])
        fill[xmiss[nm]:xmiss[nm]+bsize-1, ymiss[nm]:ymiss[nm]+bsize-1] = interpolate(corners, xb, yb)
      endfor

    endif

  endfor

  return, fill

end
