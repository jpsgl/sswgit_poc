function eit_find_last_cal, date
;+
;NAME
;   eit_find_last_cal
;PURPOSE 
;   finds the latest group of calibration lamps
;INPUTS
;   date  = date of observation
;OUTPUTS
;   returns the latest averaged calibration lamp
;KEYWORDS
;   none
;CREATION
;   12-Dec-2001. F. Auchere
;MODIFICATIONS
;   12-Aug-2002. F. Auchere : added fill in of missing blocks.
;   11-Jul-2003. J. Newmark : Windows compatibility
;-

  list = eit_calgroups()
  delim = get_delim()

  t0 = double(utc2sec(anytim2utc('01-jan-1996 00:00:00')))
  cal_end = strmid(list, 41, 20)
  etcal = double(utc2sec(anytim2utc(cal_end))) - t0
  time = double(utc2sec(anytim2utc(date))) - t0

  cal = where((etcal-time) lt 0, cb)

  if cb ge 1 then begin
  img = readfits(getenv('SSWDB')+delim+'soho'+delim+'eit'+delim+'calibrate'+$
         delim+strmid(list[cal[cb-1]], 0, 15), /silent)
    return, eit_fillmissingblocks(img>0)    
  endif else return, -1

end
