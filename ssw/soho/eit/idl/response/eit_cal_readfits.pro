;+
; Project     : SOHO/EIT
;
; Name        : EIT_CAL_READFITS
;
; Purpose     : Read EIT Calibration FITS file. 
;               Checks local directory and then remote server. 
;
; Category    : Analysis
;
; Inputs      : FILE = EIT calibration file name
;
; Outputs     : DATA = EIT calibration array
;
; Keywords    : None
;
; History     : 15-Nov-2007,  D.M. Zarro (ADNET)  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU


function eit_cal_readfits,file,_ref_extra=extra,err=err

err=''
if is_blank(file) then begin
 err='Missing input file name'
 message,err,/cont
 return,-1
endif

;-- read if found locally

chk=file_search(file,count=count)
if count eq 1 then return,readfits(file[0],_extra=extra)

;-- search and download remote copy if SEARCH_NETWORK environment
;   variable is defined

;rsearch=is_string(chklog('SEARCH_NETWORK'))
;if ~rsearch then begin
; err='Cannot find file - '+file
; message,err,/cont
; return,-1
;endif

;-- try remote server

server=eit_server(network=network,/full)
if ~network then begin
 err='Could not connect to remote server'
 message,err,/cont
 return,-1
endif

lfile=file_basename(file[0])
rfile=server+'/sdb/soho/eit/calibrate/'+lfile

sock_copy,rfile,out_dir=get_temp_dir(),local_file=cfile
chk=file_search(cfile,count=count)
if count eq 1 then return,readfits(cfile[0],_extra=extra)

err='Failed to download calibration files'
message,err,/cont

return,-1

end
