function eit_ring_flux, infile, inner_rad, outer_rad, eastonly=eastonly,$
           westonly=westonly
;+
; NAME:
;   eit_ring_flux
;
; PURPOSE:
;   Calculate the total flux (DN) per second with a given annulus in 
;   an EIT full field image.
;
; CALLING SEQUENCE:
;    flux = eit_ring_flux(fits_file, inner_rad, outer_rad)
;
; INPUTS:
;    infile      = FITS file of full field EIT image
;    inner_rad   = inner edge for annulus in solar radii
;    outer_rad   = outer edge for annulus in solar radii
;
; OUTPUTS:
;    flux        = total flux per second in given annulus
;
; OPTIONAL INPUTS: 
;    eastonly    = set if want semi-annulus on east limb
;    westonly    = set if want semi-annulus on west limb
;
; EXAMPLES:
;     Return flux within annulus 1.13-1.17 Rsun:
;         flux = eit_ring_flux(infile,1.13,1.17)
;     Return all flux on disk:
;         flux = eit_ring_flux(infile,0,1.0)
;     Return all flux off disk:
;         flux = eit_ring_flux(infile,1.001,2.0)
;
; MODIFICATION HISTORY
;    1997 Jan 08 - (JSN) - Written
;    1997 Feb 13 - J. Newmark - add east and west keywords
;    1997 Mar 17 - J. Newmark - account for change in exptime parameter
;    1997 Apr 10 - J. Newmark - use normalize exptime, normalize filter
;                               in eit_prep
;
; COMMON BLOCKS: none
; NOTES: this procedure will fill in missing blocks with the /surround
;        option, i.e. with average of surrounding blocks.
;        
;        If image taken using AL+1 Filter will normalize to CLEAR.
;-

if inner_rad ge outer_rad then begin
  message, /info, 'The outer edge for the annulus must be greater than'+$
   ' the inner edge'
  return, 0
endif

eit_prep, infile, outhdr, outimage, /surround, /normalize, /filter_norm

if strupcase(eit_fxpar(outhdr,'object')) ne 'FULL FOV' then begin
   message, /info,'The input file is not a FULL FOV image'
   return, 0
endif 
nx = eit_fxpar(outhdr,'naxis1')
xcen = eit_fxpar(outhdr,'crpix1')
ycen = eit_fxpar(outhdr,'crpix2')
cdelt = eit_fxpar(outhdr,'cdelt1')

dist_circle, circle, nx, xcen, ycen
circle = circle * cdelt
sol_rad = eit_fxpar(outhdr,'solar_r') * cdelt

half = nx/2.
if keyword_set(eastonly) then begin
   circle = circle(0:half-1,*)
   outimage = outimage(0:half-1,*)
endif
if keyword_set(westonly) then begin
   circle = circle(half:nx-1,*)
   outimage = outimage(half:nx-1,*)
endif

ring = where(circle ge inner_rad*sol_rad and circle le outer_rad*sol_rad)

if ring(0) ne -1 then flux = total(outimage(ring)) else begin
  message, /info,'There are no pixels within given annulus'
  return, 0
endelse

return, flux
end
