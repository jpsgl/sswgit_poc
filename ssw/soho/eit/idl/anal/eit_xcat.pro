;------------------------------------------------------------------------------
;+
; NAME        : 
;               EIT_XCAT
; PURPOSE     : Interactive Widget interface to LZ catalog
;
; CATEGORY    : Catalogs
;
; EXPLANATION : This function calls EIT_CATRD with interactive + LZ switch
;
; SYNTAX      : list = eit_xcat(filename=filename,tai=tai)
;
; EXAMPLES    : 
;                IDL> file=''
;                IDL> list=eit_xcat(file=file)
; 
; CALLED BY   : top level
;
; CALLS TO    : eit_catrd
;
; ARGUMENTS   : none
;
; KEYWORDS    : 
;	FILENAME  IF present, then return the fits file names matching 
;		  the entries in the returned variable.
;	TAI      IF present, then return the TAI time matching 
;		 the entries in the returned variable.
;
; OUTPUTS     : 
;	Return a string array of catalog entries,or a raw IDL structure 
;       of entries. Optional outputs include list of filenames or tai times
;
; COMMON      : none
;
; RESTRICTIONS: none
;
; SIDE EFFECTS: none
;
; PROJECT     : SOHO - EIT
;
; HISTORY     : J. Newmark 22-Aug-96
;
; CONTACT     : newmark@eitv2.nascom.nasa.gov
;-
FUNCTION eit_xcat,filename=filename,tai=tai

list=eit_catrd(/lz,/interactive,filename=filename,tai=tai)
 
RETURN,list
END
