; EIT_IMAGE
; EIT_IMAGE uses READFITS from the IDL astronomy library to read a "corrected"
; SOHO EIT image file in FITS format, and to display the
; image, scaled to its own maximum.
; D.M. fecit, 26 September, 1993.
; Added replacement_value, fill_block, and dark_current keywords, 1996 January
; 25. D.M. fecit.
; Changed to custom EIT color tables, 1996 February 3. D.M. fecit.
; XPOS and YPOS keywords added 1996 May 16. D.M. fecit.
; Filename argument broadened, 1996 September 1. D.M. fecit.
;
;+
; NAME:
;	EIT_IMAGE
; PURPOSE:
;	Read a SOHO Extreme ultraviolet Imaging Telescope (EIT) image
;       from a FITS file, and return the byte-scaled image.
;
; CALLING SEQUENCE:
;	scaled_image =	EIT_IMAGE(filename, [original_image =
;			original_image,] [scale_factor = scale_factor,]
;			[top = top], [flat=flat],
;			[image_time = image_time,] [header = header],
;			[show_image = show_image], [replacement_value =
;			replacement_value], [fill_block = fill_block],
;			[surround = surround], [adjust = adjust],
;			[dark_current = dark_current], [v_offset = v_offset],
;			[no_degrid = no_degrid], [half = half],
;			[quarter = quarter], [no_rescale = no_rescale],
;			[xpos = xpos, ypos = ypos], [image_no=image_no])
;
; INPUTS:
;	FILENAME = String containing the name of the FITS file to be read.
;			Optionally, this can instead be the string returned
;			by EIT_CATRD for a file.
;
; OUTPUTS:
;	Result = byte-scaled image array (usually 1024 x 1024)
;;
; KEYWORDS:
;	show_image - if present and non-zero, display the image on the
;			currently selected graphics device
;	original_image - original, floating-point version of contents of file
;	scale_factor = scaling for logarithmic scaling on the current X
;			device
;	top - maximum value to which to scale (useful when scaling a series
;			of images to the same maximum)
;	image_time = string concatenating DATE-OBS and TIME-OBS from the
;			FITS header
;	header = FITS header (string array)
;	replacement_value - DN with which to replace missing data block pixels
;	fill_block - intarr(32, 32) with which to replace missing data blocks
;	surround - if set, replace missing block data with the average of
;			surrounding blocks
;	adjust - multiplicative factor by which to adjust for strange exposure
;			times
;	dark_current - dark current value to be subtracted when image is
;			scaled (Note: if replacement_value is defined and
;			dark_current is not, the dark current is set to the
;			replacement value.)
;	v_offset - if show is set, the vertical offset for display (useful
;			for displays incapable of displaying the full image)
;	no_degrid - if set, do not remove the grid, otherwise remove by
;			the method of F. Clette
;	half - if set, rebin image to half size
;	quarter - if set, rebin to quarter size
;	xpos = x position of image window
;	ypos = y position of image window
;       image_no = selected image from a multiple image LZ file
;       flat - if set flat field the image
;	no_rescale - set if image is not to be rescaled into 0 - 255 range
;
; EXAMPLE:
;	Find the most current EIT image on the EIT image directory,
;	and display it on the current device:
;
;	s = findfile("reformat_dir:ef*.*")
;	n = n_elements(s)
;	a = EIT_IMAGE(s(n - 1), /show_image)
;
; RESTRICTIONS:
;	None.
;
; NOTES:
;	None.
;
; PROCEDURES USED:
;	READFITS (Astronomy library)
;	RAISE_MISSING_BLOCKS
;	EIT_DEGRID
;       EIT_FLAT
;
; MODIFICATION HISTORY:
;
;	D.M. fecit, from SXT_IMAGE, 25 October, 1993.
;
;	Changed to ignore v_offset unless n_y eq 1024. D.M. fecit, 1996/02/25.
;
;	Call EIT_DEGRID instead of reinventing the wheel. D.M. fecit,
;	   1996/03/30.
;
;       1996-Aug-20 JSN added support for 3-D LZ data
;
;	Changed filename argument handling to accept "brief" catalog lines.
;	   D.M. fecit, 1996/09/01.
;
;       1996-Sep-25 J Newmark added flat fielding
;
;	1996/10/20	Changed scaling to use only southernmost 15/16
;			of a full-frame image (to avoid scaling 284 A,
;			clear filter pos. images to the light leak).
;			D.M. fecit.
;       1996/12/04 J. Newmark - removed last traces of find_keyword
;
;	1996/12/23	Added QUARTER keyword.			D.M. fecit.
;
;	1997/02/05	Added NO_RESCALE keyword.		D.M. fecit.
;
;       1998/03/27 J Newmark change code to use NSLICE keyword in
;                      READFITS routine for 3D images
;
;       1998 Jun 3  - D. Zarro (SAC/GSFC) 
;                                -  sprinkled liberally with calls to temporary
;
;       2000 Aug 31 J. Newmark - include flat fielding
;       2001 Jun 4 J. Newmark - only use original grid
;-
function eit_image, filename, original_image = original_image, $
                    scale_factor = scale_factor, top = top, $
                    image_time = image_time, degrid_image = degrid_image, $
                    header = header, show_image = show_image, $
                    replacement_value = replacement_value, $
                    fill_block = fill_block, surround = surround, $
                    n_block = n_block, flat=flat, $
                    dark_current = dark_current, v_offset = v_offset, $
                    no_degrid = no_degrid, half = half, quarter = quarter, $
                    xpos = xpos, ypos = ypos, image_no = image_no, $
		    no_rescale = no_rescale
;
sector = ['171', '195', '284', '304'] & table = [1, 8, 41, 3]
;
stat = is_fits(filename)
if not stat then filename = eit_file2path(filename)
;
fits_header = headfits(filename)
n_x = EIT_FXPAR(fits_header, 'NAXIS1')  
n_y = EIT_FXPAR(fits_header, 'NAXIS2')
naxis3 = EIT_FXPAR(fits_header, 'NAXIS3')
if strpos(strlowcase(filename), 'efz') ge 0 then final = 1 else final = 0
;
p1_x = EIT_FXPAR(fits_header, 'P1_X') & p2_x = EIT_FXPAR(fits_header, 'P2_X')
p1_y = EIT_FXPAR(fits_header, 'P1_Y') & p2_y = EIT_FXPAR(fits_header, 'P2_Y')
full_frame = (p1_x eq 1) and (p2_x eq 1024) and (p1_y eq 20) and (p2_y eq 1043)
;
; select only 1 subimage
;
three_d=0
if naxis3 ge 1 then begin
   three_d=1
   if n_elements(image_no) eq 0 then read,image_no,prompt=$
     'Enter selected image slice 0-'+strtrim(naxis3-1)+':'
   a = readfits(filename, /silent, nslice = image_no)
endif else begin
   a = readfits(filename, /silent) 
endelse
;
; print, '%EIT_DISPLAY-D-FILE_PROVENANCE, final = ', final
;
original_image = a
;
utc_date_19960327 = anytim2utc('1996/03/27')
;
; Degridding: do by default, get sector and filter wheel position from
; FITS header.
;
if not keyword_set(no_degrid) then degrid = 1 else degrid = 0
;
; Replace missing data blocks if either replacement value or fill_block
; keyword is specified.
;
if keyword_set(dark_current) then begin
   dark = dark_current
endif else dark = 0
if keyword_set(replacement_value) then begin
   a = RAISE_MISSING_BLOCKS(a, replacement_value = replacement_value, $
         n_block = n_block, /no_copy)
   if n_block gt 0 then print, '%EIT_IMAGE-I-N_BLOCKS, ' + $
      strtrim(n_block, 2)  + ' blocks replaced'
   dark = replacement_value
endif else if keyword_set(fill_block) then begin
   a = RAISE_MISSING_BLOCKS(a, fill_block = fill_block, n_block = n_block, $
         /no_copy)
endif else if keyword_set(surround) then begin
   if not degrid then begin
      a = RAISE_MISSING_BLOCKS(a, /surround, n_block = n_block, /no_copy)
   endif else begin
      a = RAISE_MISSING_BLOCKS(a, repl = dark, n_block = n_block, /no_copy)
   end
end
a = a - dark
;
if degrid then begin
;
   if not three_d then a = EIT_DEGRID(a, fits_header, final = final, $
                   /no_copy) else $
       a = EIT_DEGRID(a, fits_header, final = final, image_no = image_no, $
                    /no_copy)
;
   if keyword_set(surround) then begin
      a = RAISE_MISSING_BLOCKS(a, /surround, n_block = n_block, /no_copy)
   end
;
end
;
; flat field not done by default yet
;
if keyword_set(flat) then begin
   if not three_d then a = EIT_FLAT(a, fits_header, /no_copy) else $
       a = EIT_FLAT(a, fits_header, image_no = image_no, /no_copy)
       ww = eit_fxpar(fits_header, 'WAVELNTH')
       dd = eit_fxpar(fits_header, 'DATE_OBS')
       a = temporary(a) /  eit_norm_response(dd,ww,fits_header)
end
;
if keyword_set(half) then begin
   a = rebin(a, n_x/2, n_y/2, /sample)
   n_x = n_x/2 & n_y = n_y/2
   window_scale = 0.5
endif else if keyword_set(quarter) then begin
   a = rebin(a, n_x/4, n_y/4, /sample)
   n_x = n_x/4 & n_y = n_y/4
   window_scale = 0.25
endif else begin
   window_scale = 1.0
   half = 0 & quarter = 0
end
;
if keyword_set('degrid_image') then degrid_image = a
;
max_color = float(min([!d.n_colors, !d.table_size]) - 1)
;
; Get the wavelength from the FITS header.
;
if not three_d then wave = strtrim(eit_fxpar(fits_header, 'WAVELNTH'), 2) $
   else begin
      wave = eit_fxpar(fits_header, 'WAV', image_no = image_no)
         wave = strtrim(wave(0), 2)
end
for j_wave = 0, 3 do if wave eq sector(j_wave) then i_wave = j_wave
;
if keyword_set(top) then begin
   top_value = top
endif else begin
   if full_frame then begin
      max_row = (15*n_y)/16
      top_value = max(a(*, 0:max_row - 1))
   endif else top_value = max(a)
;
; Post-apocalyptic light leak Al +1 kluge, until we do better
;
;  if fix(wave) eq 171 then top_value = 1500.
;  if fix(wave) eq 195 then top_value = 2500.
;  if fix(wave) eq 284 then top_value = 1750.
;  if fix(wave) eq 304 then top_value = 2750.
;
; Post-REAL-pockyclypse values: bakeout of a lifetime
;
   bin = round(p2_x/n_x)^2
   if fix(wave) eq 171 then top_value = 6000.*bin
   if fix(wave) eq 195 then top_value = 7000.*bin
   if fix(wave) eq 284 then top_value = 6000.*bin
   if fix(wave) eq 304 then top_value = 7000.*bin
end
;
; Post 2001 February 8 offpoint: more counts
;
   top_value = 5*top_value
;
if wave ne 304 then begin
   scl = max_color/(alog10(top_value) - 1.)
endif else begin
   scl = max_color/alog10(top_value)
end
scale_factor = scl
if wave ne 304 then begin
   b = byte(scl*(alog10(temporary((a > 10.) < top_value))-1.))
endif else begin
   b = byte(scl*alog10(temporary((a > 1.) < top_value)))
end
sz_b = size(b) & n_x = sz_b(1) & n_y = sz_b(2)
;
; Get time and date from FITS header.
;
time = EIT_FXPAR(fits_header, 'TIME-OBS')
;
date = EIT_FXPAR(fits_header, 'DATE-OBS')
;
; Set possible output keyword values.
;
date_string = anytim2utc(strmid(date + ' ' + time, 0, 19), /ecs)
date_string = strmid(date_string, 0, 19)
image_time = date_string
header = fits_header
;
if keyword_set(show_image) then begin
;
   title = 'SOHO EIT ' + wave + ' A image, ' + date_string
   if !d.name eq 'X' then begin
;
      nx = eit_fxpar(fits_header, 'NAXIS1')
      ny = eit_fxpar(fits_header, 'NAXIS2')
;
; changed window index from !d.window +1 to /free
; changed file name for loadct from logical coloreit to
; getenv('coloreit') for UNIX compatibility
;   1996 February 9 - J. Newmark
;
      window_position = 0
      if keyword_set(xpos) then window_position = window_position + 1
      if keyword_set(ypos) then window_position = window_position + 2
      case window_position of
         0: window, /free , xsize = window_scale*nx, ysize = window_scale*ny, $
               title = title
         1: window, /free , xsize = window_scale*nx, ysize = window_scale*ny, $
               xpos = xpos - 1, title = title
         2: window, /free , xsize = window_scale*nx, ysize = window_scale*ny, $
               ypos = ypos - 1, title = title
         3: window, /free , xsize = window_scale*nx, ysize = window_scale*ny, $
               xpos = xpos - 1, ypos = ypos - 1, title = title
      endcase
      loadct, 42 + i_wave, file = getenv('coloreit')
   end
;
   if (keyword_set(v_offset) and n_y eq 1024) then begin
;
      utc_repoint = anytim2utc('1996/04/16 23:30')
      t_repoint = utc_repoint.mjd + 1.d-3*utc_repoint.time/86400.
      date_obs = eit_fxpar(fits_header, 'DATE_OBS', image_no = image_no)
      utc_obs = anytim2utc(date_obs)
      t_obs = utc_obs.mjd + 1.d-3*utc_obs.time/86400.
;
;     print, '%EIT_IMAGE-D-TIMES, t_obs, t_repoint = ', t_obs, t_repoint
;
      if t_obs lt t_repoint then begin
         vertical_offset = v_offset
      endif else begin
         vertical_offset = min([v_offset, 23])
      end
   endif else begin
      vertical_offset = 0
   end
;
; output vertical offset if asked - JSN 6/12/96
;
   v_offset = vertical_offset
;  print, '%EIT_IMAGE-D-V_OFF, v_off = ' + strtrim(vertical_offset, 2)
;
; Rescale to get the maximum possible dynamic range.
;
   if not keyword_set(no_rescale) then no_rescale = 0
   if not no_rescale then b = EIT_RESCALE(b)
;
   tv, b, 0, -vertical_offset
end
;
return, b & end
