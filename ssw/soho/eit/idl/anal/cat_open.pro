;+
; NAME        : 
;              CAT_OPEN
; PURPOSE     : Open and associate catalog to structure.
;
; CATEGORY    : EIT Catalog
;
; EXPLANATION : 
;
; SYNTAX      : 
;
; EXAMPLES    : none
; 
; CALLED BY   :
;
; CALLS TO    : none
;
; ARGUMENTS   :	none
;
; KEYWORDS    : 
;	FILE	: A string, the catalog file name
;	LONG	: When set, don't sort or remove duplicates in catalog
;
; OUTPUTS     :
;	Returns the associated catalog
;
;	LUN	: the logical unit that is associated with the cat file
;	N_REC	: the total number of cat entries in the catalog, including
;		  duplicates but excluding the header.
;	U_REC	: the number of cat entries to be used. If /LONG is set
;		  this is the same as N_REC
;	RECORDS : an long array of the records of the catalog in the selected 
;		  order (or unordered).
;
; COMMON      : none
;
; RESTRICTIONS: none
;
; SIDE EFFECTS: none
;
; PROJECT     : SOHO - EIT
;
; HISTORY     : V1,  Elaine Einfalt (HSTX)
;             : V1.1 D.M. fecit, to account for different lengths of
;		   structures
;             : V1.2 JSN fixed byte padding, necessary for UNIX version
;                  6/12/96
;	      : V2.1 Einfalt reduced pad to make room for 24-char SCI_OBJ
;			     Since SCI_OBJ can be all 0B, it's a direct steal.
;             ; V2.2 JSN -fixed little_endian vs big_endian bug for Sun's
;                  20-jan-1998
; CONTACT     : eit@xanado.nascom.nasa.gov
;-

function CAT_OPEN,     file  = file,		 $		; input
		       long  = long,		 $		; input
		       lz    = lz,		 $		; input
		       lun   = cat_unit, 	 $		; output
		       n_rec = n_rec,		 $		; output
		       u_rec = u_rec,		 $		; output
		       records = records			; ouput

 if not(keyword_set(lz)) then begin
   ich = strpos(strupcase(file), '.CAT') & date = strmid(file, ich - 8, 8)
   ecs_date = strmid(date, 0, 4) + '/' + strmid(date, 4, 2) + '/' + $
   		strmid(date, 6, 2)
   cat_date = anytim2utc(ecs_date) 
   date_19960327 = anytim2utc('1996/03/27')		; fix bad pad
   date_19960523 = anytim2utc('1996/05/23')		; added p1r,p1c/p2r,p2c

   ; Times are {MJD from 1995 jan 1, seconds from beginning of day} (except that
   ; the mjd's in the if statement below are real MJD's).

   ; Lines commented out incorrectly had a breaks at both 3/27 and 5/23
   ; but only need the one break since always have 8 new bytes for read
   ;
   ;if cat_date.mjd ge date_19960523.mjd then begin
   ;   pad = bytarr(42)
   ;endif else if cat_date.mjd ge date_19960327.mjd then begin
   ;   pad = bytarr(50)
   ;endif else begin
   ;   pad = bytarr(64)
   ;end
   if cat_date.mjd ge date_19960327.mjd then begin
      pad = bytarr(18)			; until SCI_OBJ this was 42
   endif else begin
      pad = bytarr(56)
   end
 endif else pad = bytarr(18)		; until SCI_OBJ this was 42

 cat_entry = 					$
       {nx:		0b,			$	; num pixel x / 32
	ny:		0b,			$	; num pixel y / 32
	nx_sum:		0b,			$	; x pixel summing
	ny_sum:		0b, 			$	; y pixel summing
	file_time:	{mjd:0, sec:0l}, 	$	; (see comment above)
	wave:		bytarr(4),		$	; sector+171
							;  (0=not used),  
							;	1:171, 2:195,
							;	3:284, 3:304
	filter:		0b,			$	; filter
	obs_time:	{mjd:0, sec:0l},	$	; (see comment above)
	object:		0,			$	; feature planned
	program:	0L, 			$	; obs prog #
	exp_time:	0,			$	; exposure in 1/32 unit
	file_name:	bytarr(18),		$	; FITS file name
	image_of_seq:	0,			$	; (not used for LZ)
	n_leb_proc:	0b, 			$	; # of LEB proc steps
	leb_proc:	bytarr(8),		$	; LEB process steps
	temp:		intarr(2),		$	; data value CF and CCD
	n_wave:		0b,		 	$	; (only in LZ)
	n_repeat:	0, 			$	; (only in LZ)
	version:	0b, 			$	; nibbl for major,minor
        low_x:          0,                      $       ; low-x corner of image
        high_x:         0,                      $       ; high-x corner of image
        low_y:          0,                      $       ; low-y corner of image
        high_y:         0,                      $       ; high-y corner of image
	sci_obj:	bytarr(24),		$	; from iap
	pad:		pad}				; pad

;1997/10/27 (einfalt) if !version.os eq 'vms' then begin

  print,'%CAT_OPEN, using ' + file
  if !version.os eq 'vms' and (is_gsfcvms() ne 3) then begin

      openr, cat_unit, file, /shared, /get_lun	     ; open catalog
      w = assoc(cat_unit,lonarr(32))   &   header = w(0)  ; read the cat header
      n_rec = header(0)				     ; get # recs in this cat

      cat = assoc(cat_unit, cat_entry)		     ; see above for structure
      entry = replicate(cat_entry, n_rec)

     for i = 1l, n_rec do entry(i-1) = cat(i)	    ; remember, don't do header

 endif else begin

; test for little_endian or big_endian
     test = 1
     byteorder, test, /ntohs
     lendian = test ne 1

     openr, cat_unit, file, /get_lun	     ; open catalog

     stat=fstat(cat_unit)
     nsz=stat.size/128
     cat=bytarr(128,nsz)
     readu,cat_unit,cat
     header = cat(*,0)
     n_rec=fix(header,0)
     rev_swap,n_rec

     entry = replicate(cat_entry, n_rec)

     ; Note: When this particular catalog record structure is created in VMS,
     ;	     then the nested/imbedded structures, like file_time, are padded
     ;	     to a VMS byte boundary.  On the UNIX systems this padding is not
     ;	     there.  So what appears to be a 128 byte structure in VMS
     ;	     only uses 115 bytes on a UNIX system.


     for i = 1l, n_rec do begin
         if keyword_set(lz) then begin
             rec=cat(*,i)
             top=115			
         endif else begin
           if cat_date.mjd ge date_19960327.mjd then begin
             rec=cat(*,i)
             top=115
; fixed unnecessary break -JSN 6/12/96
;         endif else if cat_date.mjd ge date_19960327.mjd then begin
;           rec=cat(*,i)
;           top=115
           endif else begin
             rec=cat(*,i*2)
             top=127
           end
         endelse
         entry(i-1).nx = rec(0) 
         entry(i-1).ny = rec(1)
         entry(i-1).nx_sum = rec(2)
         entry(i-1).ny_sum = rec(3)
         entry(i-1).file_time.mjd = fix(rec,4) 
         entry(i-1).file_time.sec = long(rec,6)
         entry(i-1).wave = rec(10:13)
         entry(i-1).filter = rec(14)
         entry(i-1).obs_time.mjd = fix(rec,15)
         entry(i-1).obs_time.sec = long(rec,17)
         entry(i-1).object = fix(rec,21)
         entry(i-1).program = long(rec,23)
         entry(i-1).exp_time = fix(rec,27)
         entry(i-1).file_name = rec(29:46)
         entry(i-1).image_of_seq = fix(rec,47)
         entry(i-1).n_leb_proc = rec(49)
         entry(i-1).leb_proc = rec(50:57)
         entry(i-1).temp = [fix(rec,58),fix(rec,60)]
         entry(i-1).n_wave = rec(62)
         entry(i-1).n_repeat = fix(rec,63)
         entry(i-1).version = rec(65)
         entry(i-1).low_x = fix(rec,66)
         entry(i-1).high_x = fix(rec,68)
         entry(i-1).low_y= fix(rec,70)
         entry(i-1).high_y = fix(rec,72)
         entry(i-1).sci_obj = rec(74:97)
         entry(i-1).pad = rec(98:top)		; until SCI_OBJ this was 74
         if not lendian then begin
            dum = entry(i-1)
            rev_swap,dum
            entry(i-1) = dum
         endif
     endfor 
 endelse

 obs_time = { mjd : entry.obs_time.mjd + 49718, $
	      time: entry.obs_time.sec * 1000L}
 tai_arr  = utc2tai(anytim2utc(obs_time))  

 if not(keyword_set(long)) then begin		; long format will not sort
						; or remove duplicates.
    ordered = sort(tai_arr)			; Sorted but may have duplicates
    uni = uniq(tai_arr(ordered))		; get rid of duplicates

    records = ordered(uni) + 1			; ordered and unique indices,
                           			; offset because 1st record
						; is the header

 endif else records = lindgen(n_rec)+1		; use original order

 u_rec = n_elements(records)			; # of record to consider

 free_lun, cat_unit

return, entry
end
