function eit_flux,wav_band,te,filter=filter,dn=dn,inspec=inspec,$
    edensity=edensity,pressure=pressure,wavestep=wavestep,inwave=inwave,$
    expotime=expotime,dem_name=dem_name,$
    wmin=wmin,wmax=wmax, instr=instr, volume=volume
;+
; NAME:
;  eit_flux
; PURPOSE:
;  Specify Te + filter + DEM ==> compute DN 
;  Specify Te + filter + DN ==> compute DEM
;
; CALLING SEQUENCE:
;      dn_s = eit_flux(171,1.e6,filter='CLEAR',edensity=1e9)
;  or
;      dem  = eit_flux(195,1.5e6,filter='AL+1',edensity=1.5e9,dn=1000,
;               expotime=5)
;
; INPUTS:
;    wav_band: EIT bandpass, one of 171,195,284,304
;    te:   Temperature, may be a vector
;
; OUTPUTS:
;   This function returns DN/s/pixel.
;   If DN keyword is set, will return Column Emission Measure cm^-5
;              this value represents an isothermal emission measure
;              approximation. We include a factor 0f 0.7 to account
;              for the Potasch approximation that lines really form
;              over +/- 0.15 dex. If one wanted to compare this to a
;              differential emission measure (DEM) then you should 
;              multiply this by 1./0.3
;
; OPTIONAL INPUT KEYWORDS:
;    DN: if set returns Emission Measure, if vector should correspond
;        to elements of temperature
;    filter:  default is CLEAR position can also specify AL+1 or AL+2
;    wavestep: wavelength bin for spectrum calculation, default=0.2Ang   
;    Specify one of below:
;          Edensity: electron density in emitting region (cm^-3)
;          Pressure: electron pressure (cm^-3 K)
;          Note- these can be single valued or arrays corresponding to 
;             the specified temperatures
;          these are used in the population calculations 
;    Can specify both of below:
;        inspec: an input spectrum, useful if have spectra already calculated
;         waves: wavelengths corresponding to inspec
;    dem_name = differential emission measure file - temp vs dem, can
;        also be a float value=alog10(dem) which correspond to temps.
;        set to "unit" if just need unit emission measure - UNIT=1e26
;    wmin = minimum wavelength of interest (default = 165A)
;    wmax = maximum wavelength of interest (default = 350A)
;    expotime = exposure time if data set not normalized
;
; OPTIONAL OUTPUT KEYWORDS:
;
; RESTRICTIONS:
; ****  This routine requires the CHIANTI package. Please run the
;       following command before using this routine:
;            ssw_packages,/chianti,/prepend
;
;   Presently this reads the Instrument Data from the EIT Sensitivity
;   files on the SSW as provided by J. Newmark. This section is
;   modularized so as to facilitate updates or even new instruments.
;
;   This program runs fairly slow since it is calculating the spectrum
;   from 165-350 A. A better way to run this is to calculate the spectrum
;   using CHIANTI_spec once for the density and temps (or pressure) you wish 
;   and pass it into this program.
;
; MODIFICATION HISTORY:
;   1996 Oct 30, J.S. Newmark, Written.
;   1998 Jan 26  J.S. Newmark, V. 2.0 delete wavestep in
;                     calculation - should not be included, auto add chianti
;                     to path
;   1999 May 11  J.S. Newmark, V 3.0 New calibration
;   1999 Jun 08  J.S. Newmark, Clarify return value of Emission measure 
;   1999 Nov 17  J.S. Nemwark, V 3.1 New Calibration
;-

; check inputs, convert if necessary
ntemps = n_elements(te)
CASE 1 OF
  keyword_set(edensity): begin
        if n_elements(edensity) ne 1 and n_elements(edensity) ne ntemps then $
          begin
          message,/info,'The dimensions of DENSITY must be 1 or equal to TEMPERATURE'
          return,0
        endif
        edens_arr = fltarr(ntemps)+edensity
      end
  keyword_set(pressure): begin
        if n_elements(pressure) ne 1 and n_elements(pressure) ne ntemps then $
          begin
          message,/info,'The dimensions of PRESSURE must be 1 or equal to TEMPERATURE'
          return,0
        endif
        edens_arr = pressure / te
      end
  else: begin
       message,/info,'You must specify either the Electron Density (EDENSITY) or'+$
         ' the Pressure (PRESSURE = cm^-3 K)'
       return,0
      end
ENDCASE

if not keyword_set(instr) then instr = 'eit' else instr = strlowcase(instr)
defsysv,'!xuvtop',exist=yes_chianti
if not yes_chianti then ssw_packages,/chianti    

if not keyword_set(wmin) then wmin = 165 
if not keyword_set(wmax) then wmax = 350

; calculate spectrum for given temperature
IF n_elements(wavestep) EQ 0 THEN wavestep = 1.0

IF n_elements(inspec) EQ 0 THEN BEGIN
    spectrum = call_function('chianti_spec',wmin,wmax,wavestep,te,waves,edensity=edens_arr,$
             /noverbose) 
ENDIF ELSE BEGIN
    IF n_elements(inwave) EQ 0 THEN BEGIN
       message,/info,'If specifying input spectrum must also specify waves'
       return,0
    ENDIF
    wrange = where(inwave ge wmin and inwave le wmax)
    waves = inwave(wrange)
    spectrum = inspec(wrange,*)
ENDELSE

; Read in instrument specific calibrations
case 1 of
  instr eq 'eit': instr_par = eit_parms(waves,wav_band,filter)
;  instr eq 'calroc': instr_par = calroc_parms(waves,wav_band)
;  instr eq 'lasco': instr_par = lasco_parms()
;  instr eq 'sem': instr_par = sem_parms(waves)
  else: instr_par = 1
endcase
;

; Calculations of DN/S/pixel or Column Emission Measure
;
;   computation of electrons/pixel/sec - based upon note of Ken Dere
;   - "Spectral Response of the EIT"
;
IF n_elements(expotime) EQ 0 THEN expotime = 1.

; Read in Differential Emisssion Measure:
IF n_elements(dn) EQ 0 THEN BEGIN
  IF n_elements(dem_name) EQ 0 THEN begin
    stat = execute('path = expand_path(!xuvtop) + "/dem"')
    dem_name = pickfile(path=path,filter='*.dem',title='Select DEM File')
  ENDIF
  IF datatype(dem_name) EQ 'STR' THEN BEGIN
     if dem_name eq 'unit' then dem_int = fltarr(n_elements(te))+26. $
       else begin
          read_dem,dem_name,dem_t,dem,dem_ref 
          dem_int = spline(dem_t,dem,alog10(te)) + alog10(te)
       endelse
  ENDIF ELSE dem_int = dem_name

  dem_int = 10^dem_int

  sz = size(dem_int)
  if sz(0) eq 2 then begin
        eps = fltarr(sz(1),sz(2),n_elements(te)) 
        for i = 0, n_elements(te) - 1 do $
              eps(0,0,i) = total(instr_par * spectrum(*,i) ) * dem_int(*,*,i)
  endif else begin
        eps = fltarr(n_elements(te))
        for i = 0, n_elements(te) - 1 do $
                eps(i) = total(instr_par * spectrum(*,i) ) * dem_int(i)
  endelse
  flux = eps * expotime

ENDIF ELSE BEGIN

  eps  = dn / expotime
  flux = fltarr(n_elements(te))
  for i = 0,n_elements(te)-1 do begin
    flux(i) = eps(i) / total(instr_par * spectrum(*,i) )
  endfor
  flux = 0.7*flux
  if keyword_set(volume) then begin
       AU = 1.496e13  ;cm
       pix_arc=eit_pixsize()*3.1416/(60.0*60.0*180.0) 
       pix_len=pix_arc*au 
       flux = temporary(flux)/(pix_len*pix_len)
  endif

ENDELSE

if n_elements(flux) eq 1 then flux = flux(0)
RETURN,flux
END


