; RAISE_MISSING_BLOCKS
; RAISE_MISSING_BLOCKS sets the values of missing pixel blocks in an EIT
; image to a fixed value or a block from elsehwere in the image (e.g. a dark
; corner).
; D.M. fecit, 1996 January 19.
;
function raise_missing_blocks, image, replacement_value = replacement_value, $
   fill_block = fill_block, surround = surround, n_block = n_block
;
a = image
;
if keyword_set(replacement_value) then begin
   replace = 1 & fill = 0 & adjacent = 0
endif else if keyword_set(fill_block) then begin
   replace = 0 & fill = 1 & adjacent = 0
endif else if keyword_set(surround) then begin
   replace = 0 & fill = 0 & adjacent = 1
endif else begin
   print, 'RAISE_MISSING_BLOCKS-E-NOKEY, you must supply a keyword.'
   return, image
end
;
x0 = indgen(32)*32 & y0 = x0
sz_a = size(a) & nx = sz_a(1) & ny = sz_a(2)
n_bl_x = nx/32 & n_bl_y = ny/32
;
if replace then begin
   replacement_block = intarr(32, 32) + replacement_value
   surround = 0
endif else if fill then begin
   replacement_block = fill_block & surround = 0
end
;
n_block = 0
for j = 0, n_bl_y - 1 do begin
   for i = 0, n_bl_x - 1 do begin
      if total(a(x0(i):x0(i) + 31, y0(j):y0(j) + 31)) eq 0 then begin
         if surround then begin
            background = 0.0 & n_blocks_in_sum = 0
            lower_x = max([0, i - 1]) & upper_x = min([i + 1, n_bl_x - 1])
            lower_y = max([0, j - 1]) & upper_y = min([j + 1, n_bl_y - 1])
            for iy = lower_y, upper_y do begin
               for ix = lower_x, upper_x do begin
                  nearby_block = total(a(32*ix:32*ix + 31, 32*iy:32*iy + 31))
                  if nearby_block gt 0 then begin
                     background = background + nearby_block
                     n_blocks_in_sum = n_blocks_in_sum + 1
                  end
               end
            end
            background = fix(background/(n_blocks_in_sum*1024.))
            replacement_block = intarr(32, 32) + background
         end
         a(x0(i), y0(j)) = replacement_block
         n_block = n_block + 1
      end
   end
end
;
return, a & end
