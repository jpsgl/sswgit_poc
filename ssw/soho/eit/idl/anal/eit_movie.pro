; EIT_MOVIE
; EIT_MOVIE uses XINTERANIMATE to display movies from IDL save files of
; 3-dimensional byte arrays of images.
; Note: Assumes (i) file name is of the form 'movie_www_whatever.save'
; where www is the wavelength, and (ii) the image array is called b0.
; D.M. fecit, maxima in celeritate, 1996 January 31.
;
pro eit_movie, filename
;
wavelength = [171, 195, 284, 304]
;
restore, /verbose, filename
;
j_wave = -1
print, '%EIT_MOVIE-D-WAVE, wave = ' + strtrim(wave, 2) + '.'
for i_wave = 0, 3 do if wave eq wavelength(i_wave) then j_wave = i_wave
if j_wave ge 0 then color = 42 + j_wave else color = wave
;
sz_b = size(b0) & n_frame = sz_b(3)
;
xinteranimate, set = sz_b(1:3), title = title
for i_frame = 0, n_frame - 1 do begin
   xinteranimate, frame = i_frame, image = b0(*, *, i_frame)
end
;
; changed file name for loadct from logical coloreit to
; getenv('coloreit') for compatibility with unix. feb. 9 1996 -J. Newmark
;
if color gt 40 then begin
   loadct, color, file = getenv('coloreit')
endif else begin
   loadct, color
end
;
xinteranimate, /keep
;
return & end
