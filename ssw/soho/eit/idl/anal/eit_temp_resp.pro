function eit_temp_resp,wave,pressure=pressure,temp=temp
;
; +
; NAME:
;  eit_temp_resp
; PURPOSE
;  Compute EIT temperature response (EM=1.e26 cm^-5) for each filter
;  Useful on its own as well an example of calling EIT_FLUX 
; CALLING SEQUENCE:
;  response = eit_temp_resp(wavelngth,pressure=pressure,temp=temp)
; INPUT:
;   wavelength
;   pressure (used only for level population calculations - not
;                    sensitive to this)
; OUTPUT:
;   response
;   temperature array
; HISTORY
;   created 8-Dec-1997 J. Newmark
;   1999-May-11 J. Newmark New data file
; -

defsysv,'!xuvtop',exist=yes_chianti
if not yes_chianti then ssw_packages,/chianti,/prepend

if not keyword_set(eldens) then pressure = 1.e15

;specs = [1e8,5e8,1e9,5e9,1e10,5e10,1e11]
;spec_choice = where(specs eq eldens)
restore,concat_dir(getenv('SSW_EIT_RESPONSE'),'chianti_spec.save')
;stat = execute("inspec = "+ 'spec'+strtrim(spec_choice(0),2)) 

response = eit_flux(wave,temp,pressure=pressure,dem_name='unit',inspec=spec0,$
   inwave=waves)

return,response
end


