; FAKE_MISSING_BLOCKS
; FAKE_MISSING_BLOCKS sets the values of missing pixel blocks in an EIT
; image to a fixed value or a block from elsehwere in the image (e.g. a dark
; corner).
; D.M. fecit, 1996 January 19.
;
pro fake_missing_blocks, a, index, block_size = block_size, $
   before = before, after = after, n_block = n_block
;
if not keyword_set(block_size) then block_size = 32
if not keyword_set(before) then before = 0
if not keyword_set(after) then after = 0
;
x0 = indgen(32)*block_size & y0 = x0
sz_a = size(a) & nx = sz_a(1) & ny = sz_a(2)
n_bl_x = nx/block_size & n_bl_y = ny/block_size
;
n_block = 0 & bs = block_size & bsm1 = block_size - 1

for j = 0, n_bl_y - 1 do begin
   for i = 0, n_bl_x - 1 do begin
      t_block = total(a(x0(i):x0(i) + bsm1, y0(j):y0(j) + bsm1, index))
      if t_block eq 0 then begin
         if before then begin
            replacement_block = $
               a(x0(i):x0(i) + bsm1, y0(j):y0(j) + bsm1, index - 1)
         endif else if after then begin
            replacement_block = $
               a(x0(i):x0(i) + bsm1, y0(j):y0(j) + bsm1, index + 1)
         end
         a(x0(i), y0(j), index) = replacement_block
         n_block = n_block + 1
      end
   end
end
;
return & end
