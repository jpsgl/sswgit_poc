;+
; NAME        : CAT_TO_TEXT
;
; PURPOSE     : Convert "raw" catalog entries into string array
;
; CATEGORY    :
;
; EXPLANATION : 
;
; SYNTAX      : 
;
; EXAMPLES    : none
; 
; CALLED BY   :
;
; CALLS TO    : none
;
; ARGUMENTS   :	none
;
; KEYWORDS    : none
;
; OUTPUTS     : none
;
; COMMON      : none
;
; RESTRICTIONS: none
;
; SIDE EFFECTS: none
;
; PROJECT     : SOHO - EIT
;
; HISTORY     : V1,  Elaine Einfalt (HSTX)
;		1996 March 20, einfalt - modified filter labels to mathc reality
;		1996 May  einfalt - modification to text printout display,
;				    added summing info,
;				    changed pixels to blocks, added readout area
;				    change order of some things, added /SHORT.
;		1997 March 11, einfalt - handle decimal exposure times for
;					 version 2.0 and above
;
; CONTACT     : eit@xanado.nascom.nasa.gov
;-

function cat_to_text, entry, short=short, lz=lz, web=web

; if web version put filename first
   if keyword_set(web) then begin
     filename = strupcase(entry.file_name) 
     text =  filename+'   '
   endif else text=''
 
   ; Convert EIT time to CDS time

   time = {mjd:entry.obs_time.mjd+49718, time:entry.obs_time.sec*1000L}

   vms_time = anytim2utc( utc2tai(time), /vms)		; time in vms format
   text = text+ strmid(vms_time,7,4) + '-' + strmid(vms_time,3,3) + '-' + $
	       	string(strmid(vms_time,0,2),'(i2.2)') + ' ' +$
		strmid(vms_time,12,8) 

   exptime = float(entry.exp_time)		; so can divide by 10. later

   version_no = (ishft(entry.version,-4) and '0f'x) + $	   ; convert version #
		(entry.version and '0f'x)/10.		   ; to major.minor

   w_ver_2 = where(version_no ge 2.0,count)
   if count gt 0 then exptime(w_ver_2) = exptime(w_ver_2) /10.

   expos_time = string(exptime, '(f6.1)') + ' s'
   text = text + expos_time 

   ; For this code only use the 1st wave
   waves   = fix(entry.wave(0))
   w_waves = where(waves ne 0, nwaves)
   if nwaves gt 0 then waves(w_waves) = waves(w_waves) + 170
   sec_wave=strtrim(fix(waves),2)

   w_info=where(entry.n_wave gt 1,num_info)
   if num_info gt 0 and not keyword_set(short) then begin
       for i=0,num_info-1 do begin
           numw=entry(w_info(i)).n_wave
           for j=1,numw-1 do sec_wave(w_info(i))=sec_wave(w_info(i))+','+$
              strtrim(fix(entry(w_info(i)).wave(j))+170,2)
       endfor
   endif

   filter = replicate('?      ', n_elements(entry.filter))

   ; old way   filt_str = ['Clear  ', 'Al sup ', 'Al+2 ','Al+1 ', 'Al inf ']
   filt_str = ['Al+1 ', 'Blk Est',  'Clear  ', 'Blk Wst', 'Al+2 ' ]
   w_filter = where(entry.filter ge 0 and entry.filter le 4, n_filt)
   if n_filt gt 0 then filter(w_filter) = filt_str(entry(w_filter).filter)
   
   sec_filt = sec_wave + '::' + filter 
   text = text + '  ' + sec_filt 

;   nsize =  strtrim(fix(entry.nx_sum)>1,2) + 'x' + $
   nsize =  string(fix(entry.nx_sum)>1,'(i2)') + 'x' + $
		'('+string(fix(entry.nx),'(i2)') + ',' + $
		string(fix(entry.ny),'(i2)') +')'
   text = text + nsize

   readout = '@(' + string(entry.low_x, '(i4)')  + ',' + $
	     string(entry.low_y, '(i4)')  + '/' + $
	     string(entry.high_x, '(i4)') + ',' + $
	     string(entry.high_y, '(i4)') + ')'
   text = text + ' ' + readout 

   
   if keyword_set(lz) then begin
        n_info=replicate('',n_elements(entry.n_repeat))
        w_info=where(entry.n_repeat gt 0,num_info)
        if num_info gt 0 then n_info(w_info)=$
              strtrim(entry(w_info).n_repeat,2)+'img'
        text=text+' '+ n_info
   endif
   ; Don't make the text line too long so return now.

   if keyword_set(short) then return,text


   ; Note: the 1st ip is really "noop" not null

   ip = ['','/badcol','/intmin','/rt1bit','/pixsum','/radspk','/sqrt', $
          '/Pyrmid', '/intsum', '/intdf1', '/intdf2', '/nocomp', '/rice',   $
          '/adct', '/dcmp', '/synstp', '/intrBP', '/intrDP', $
	  '/notuse', '/debug', '/headr', '/Dbug0', '/Dbug1', '/Dbug2', $
	  '/Dbug3', '/mask', '/nomask', '/noocc', '/Dbug4', '/InvMsk', $
	  '/NoInvM', '/DecArr', '/resetB', '/clrROI', '/thresh']

   lp = [ 	'Req Periph', 'M1 Measure', 'Wobb FP 1', 'Wobb FP 2', $
		'Dark', 'FP C+C', 'contin', 'FP Scan', 'Normal', 'Dump', $
		'grd-perph', 'ld perph', 'Def Param', 'Sect Seq', $
		'FP, Coord', 'Concurr', 'Cal lmp', 'Schd Mech', $
		'inter-sum', 'Seq Imag', 'Op/Cl M1']

    ; The entry.leb_proc tag has 8 bytes in it, that's why divide by 8
   process = ip(entry.leb_proc)  &  n_proc = n_elements(process(0,*))
   proc_step = reform(process(0,*),n_proc)
   for i = 1, 7 do proc_step =  proc_step + reform(process(i,*),n_proc) 
   proc_step = strmid(proc_step,1,max(strlen(proc_step)))

   if keyword_set(lz) then leb_list = proc_step else $
          leb_list = lp(entry.program-1) + '::' +proc_step

   text = text + '  ' + leb_list

   if not keyword_set(web) then begin
     filename = strupcase(entry.file_name) 
     text = text + '  ' + filename
   endif

return, text
end
