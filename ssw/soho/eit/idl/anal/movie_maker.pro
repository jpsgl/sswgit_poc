;+
; NAME:
;	MOVIE_MAKER
;
; PURPOSE:
;      This procedure will create EIT movies (IDL Save Sets).
;      The user interactively selects a series of FITS images,
;      scales them, and saves them.
;
; CATEGORY:
;	WIDGET interface
;
; CALLING SEQUENCE:
;       MOVIE_MAKER
;
; INPUTS:  None. The default is to list files from todays QKL catalog.
;          User can select date and/or LZ catalog as well as other
;          selection criteria.
;
; KEYWORD PARAMETERS: None
;
; OUTPUTS:
;	This routine creates an IDL save set in MOVIE_DIR or current
;       directory.
;
; COMMON BLOCKS:
;  common filestuff1,dirspec,flist,index,list7,save_file
;  common parms,dateit,dateend,nxit,nyit,waveit,outfit,halfres,sclit,rbin,qres,
;         reproducible_top_value
;  common parms2,fresit,fmark,use_qkl,use_lz,new_flist,save_index,numfiles
;  common params,date_start,date_end,n_x,n_y,wavel,top_val
;  common ratio_par,ratioit,save_195,save_171,s195,s171,mk_ratio,$
;         title_start,title_end,stamp_time,mk_diff
;
; SIDE EFFECTS:
;	unsure if common block must be re-initialized after each run.
;
; RESTRICTIONS:
;	Serious memory hog, especially if display movies
;
; PROCEDURE:
;       A widget is displayed. The user uses the various text widgets
;       to input selection criterion. The user can display or delete
;       any of the selected images. The user selects the scaling procedure,
;       setting a minimum value and maximum value, the movie is then
;       saved to an IDL save set in movie_dir or current directory.
;
; MODIFICATION HISTORY:
; 	Written by:  J. Newmark 	Date.  Apr. 1996
;	Ratio movie change:	D.M. fecit	1996 July 12
;       Scaling changes J. Newmark              1996 Jul 17
;	Changed RESCALE calls to EIT_RESCALE.	D.M. fecit	1996 Sept. 13
;       Significant changes:
;       Changed date format in title, added hourglass. J. Newmark 1996 Sept 27
;       Add flat fielding, allow inclusion of 1024x1024 images in
;         sequences of 512x512. J. Newmark 1996 Sept. 27
;       Add Histogram Scaling, changed scaling algorithm to account
;         for total exptime, added comments. J. Newmark 1996 Sep 30
;       v2.0 1996 October 01
;       Cleaned up code, combined scaleit, roiit J. Newmark 1996 Oct 2
;       Handle Level-Zero Data - Substantial changes to work with
;          files which contain 3D data. J Newmark 1996 Oct 7
;       Allow input of image size in blocks if lt 32, list number of
;          selected files.  J. Newmark 1996 Oct 08
;       v3.0 1996 October 08
;       Desensitize SAVE MOVIE button until after images are scaled.
;          J. Newmark 1996 December 04
;       Change scaling 284 images, too faint for alog10. J. Newmark 1996 Dec 08
;       USe fake_missing_blocks,/high for ratio movies J. Newmark 1996 Dec 09
;       Change rebin from 512x512 to 2 times. J Newmark 1996 10 Dec
;       Add Quarter resolution output. J Newmark 1996 Dec 23
;       Remove darks, cal lamps from movies J Newmark 1997 Jan 16
;       Add inclusion of 4x4 binned with full field J. Newmark 1997 Jan 28
;       Changed/fixed scaling for image totals J. Newmark 1997 Jan 30
;       Don't use images with many missing blocks in histogram scale.
;                  J. Newmark 1997 Apr 14
;       Add ability to make CRUDE difference movies. J. Newmark 1997 Apr 21
;	Made default for rebin /SAMPLE.		D.M. fecit	1997 May 23
;       Save time stamp in STRARR in save set. J. Newmark 1997 OCt. 30
;-
;------------------------------------------------------------------------------
; subprocedure to kill the help window
;------------------------------------------------------------------------------
PRO killhelp1
COMMON helpshare,helpbase
WIDGET_CONTROL, helpbase, /DESTROY
END
;------------------------------------------------------------------------------
; subwidget to display a help window with text output
;------------------------------------------------------------------------------
PRO helpout1
COMMON helpshare,helpbase
helparray = strarr(32)
helparray(0)='           This is the EIT Movie Maker.'
helparray(1)='  '
helparray(2)=' A widget is displayed. The user uses the various text widgets'
helparray(3)=' to input selection criterion. The user can display or delete'
helparray(4)=' any of the selected images. The images are then scaled and'
helparray(5)=' saved to an IDL save set in movie_dir.'
helparray(6)='  '
helparray(7)=' Quick tutorial: '
helparray(8)='   a) Select start date, end date'
helparray(9)='   b) Select X,Y sizes of input images, pixels or blocks lt 32'
helparray(10)='   c) Select wavelength '
helparray(11)='   d) Enter output file name'
helparray(12)='   Extras:
helparray(13)='   e) toggle default scaling or scale so min=0'
helparray(14)='   f) toggle output array size for full resolution, half'
helparray(15)='       resolution, 512x512(useful for magnifying small fields'
helparray(16)='       or Include full Res (1024) images with binned (512) ones'
helparray(17)='   g) toggle Save 171 or Save 195 or No Save - this is necessary'
helparray(18)='       if a ratio of 195/171 movie will be created'
helparray(19)=' '
helparray(20)='   Running: '
helparray(21)='   h) Click Update Listing to generate file list - scan through'
helparray(22)='       list, can display or delete selected images '
helparray(23)='   i) Set scaling - Auto Scale - will display a histogram of '
helparray(24)='       values from the central 100x100 of each image, select a'
helparray(25)='       minimum value to get rid of images with many dropouts'
helparray(26)='       ROI scale - user defined Region Of Interest for scaling'
helparray(27)='       Hist scale - interactively set max using histogram'
helparray(28)='   j) Click Save Movie - can also View most recently saved Movie'
helparray(29)='   k) Making Ratio movies - First create both 171 and 195 '
helparray(30)='      with the appropriate Save 171,195 selection made'
helparray(31)='      after both are made simply Click Make Ratio'

helpbase = WIDGET_BASE(TITLE = 'Movie Maker Help', /FRAME, /COLUMN, $
	XOFFSET = 350, YOFFSET = 150)
finishbutton = WIDGET_BUTTON(helpbase, VALUE = 'Click here when done', $
	UVALUE = 'donehelp')
helptext = WIDGET_TEXT(helpbase,VALUE= helparray,XSIZE= 55,YSIZE= 20,/SCROLL)
WIDGET_CONTROL, helpbase, /REALIZE
XMANAGER, 'helpout', helpbase, EVENT_HANDLER='MOVMK_event',MODAL = helpbase
END

;------------------------------------------------------------------------------
; procedure to display selected image - uses eit_image
;------------------------------------------------------------------------------

pro showeit1
common filestuff1,dirspec,flist,index,list7,save_file
common parms2,fresit,fmark,use_qkl,use_lz,new_flist,save_index,numfiles,ids
;
  if n_elements(index) eq 0 then index = 0
  if use_lz then f_template = 'efz' else f_template = 'efr'
  filename = strmid(flist(index),strpos(strlowcase(flist(index)),f_template),18)
  if !version.os eq 'vms' then  filename = eit_file2path(filename,/gavroc) $
        else filename = eit_file2path(strlowcase(filename))
  if is_fits(filename) then begin
        hdr = headfits(filename)
        naxis = eit_fxpar(hdr,'NAXIS')
;
; for LZ data with multiple images select 1 from list or ALL
        if naxis eq 3 then begin
          nimgs = eit_fxpar(hdr,'NAXIS3')
          comms = eit_fxpar(hdr,'COMMENT')
          start = where(strpos(comms,'BEGIN MULTIPLE') ne -1)
          out = strarr(nimgs+7)
          out(0) = 'Please select from the list of images below or ALL'
          out(1) = '      Image numbers range from 0-'+strtrim(nimgs-1,2)
          out(3:nimgs+6) = comms(start(0):start(0)+3+nimgs)
          answer = 'ALL'
          xin, answer,out,status=status
          if strupcase(answer(0)) eq 'ALL' and status then begin
            window, /free,xs=1024,ys=1024
            orig = readfits(filename)
            orig = alog10((orig-848)>1)
            wv = eit_fxpar(hdr,'WAV',image=0)
            sector = ['171', '195', '284', '304']
            for j_wave = 0, 3 do if strtrim(wv(0),2) eq sector(j_wave) then $
                i_wave = j_wave
            loadct,42 + i_wave,file=getenv('coloreit')
            times = eit_fxpar(hdr,'START_TIME',image_no='all')
            times = strmid(times,11,8)
            for i = 0,nimgs-1 do begin
                put, orig(*,*,i),i+1,nimgs,/noexact,relat=0.85
                label_image, times(i)
            endfor
          endif else begin
            while not status do xin, answer,out,status=status
            if strupcase(answer(0)) eq 'ALL' then answer(0) = '0'
            image_no = fix(answer(0))
            img = eit_image(filename, repl = eit_dark(), dark = eit_dark(), $
                    /show, v_off=46, image_no=image_no, /flat)
          endelse
        endif else img = eit_image(filename, dark = eit_dark(), /show, $
            v_off=46, /flat) 
  endif else message, /continue, 'File is not FITS.'
end

;------------------------------------------------------------------------------
; procedure to auto-scale images - uses central 100x100 pixels or if
;       roiit is set then a mouse defined region of interest - user
;       defined maximum can be set with histogram of first image in sequence
;------------------------------------------------------------------------------
pro scaleit, hist=hist, roiit=roiit
common filestuff1,dirspec,flist,index,list7,save_file
common parms,dateit,dateend,nxit,nyit,waveit,outfit,halfres,sclit,rbin,qres, $
       reproducible_top_value
common parms2,fresit,fmark,use_qkl,use_lz,new_flist,save_index,numfiles,ids
common params,date_start,date_end,n_x,n_y,wavel,top_val

 utc_obe_good = anytim2utc('1996/07/18')
 t_obe = utc_obe_good.mjd + 1.d-3*utc_obe_good.time/86400

 if use_lz then f_template = 'efz' else f_template = 'efr'
 if n_elements(n_x) eq 0 then widget_control, nxit,get_value=n_x
 if n_elements(n_y) eq 0 then widget_control, nyit,get_value=n_y
 n_x = fix(n_x(0))
 n_y = fix(n_y(0))
 if n_x lt 32 then n_x = n_x*32
 if n_y lt 32 then n_y = n_y*32
 new_flist = flist
 sz = n_elements(flist)
 save_index = intarr(sz)
 tots = fltarr(sz)
 tvals = fltarr(sz)
 fmark = intarr(sz)
 if keyword_set(roiit) then begin         ; obtain indices from mouse input
   showeit1
   indices = defroi(n_x(0),n_y(0))
   wdelete, !d.window
 endif
 widget_control, /hourglass
;
; for each file, read image, sum central 100x100 pixels, set top scale
; to mean+5*sdev or user selected point on histogram
; changed to sum top 15/16 of image
;
 new_i = -1
 for i = 0, sz -1 do begin
     print, '%MOVIE_MAKER-I-FRAME, processing frame ' + strtrim(i, 2) + '.'
     fname = strmid(flist(i),strpos(strlowcase(flist(i)),f_template),18)
     if !version.os eq 'vms' then  fname = eit_file2path(fname,/gavroc)$
        else fname = eit_file2path(strlowcase(fname))
;
; 1996 July 17 - JSN changed readfits to eit_image and got top scale
;      dum_image=readfits(strlowcase(fname),hdr)
;
; add stuff for 3d LZ stuff
      index = 0
      n_3d = 1
      hdr = headfits(strlowcase(fname))
      naxis = eit_fxpar(hdr,'NAXIS')
;
; handle multiple images per file, do not a priori know how many images
      if naxis eq 3 then begin
        if n_elements(wavel) eq 0 then widget_control,waveit,get_value=wavel
        if n_elements(wavel) eq 0 then wavel = 'all'
        if strlowcase(wavel(0)) eq 'all' then $
            index = intarr(eit_fxpar(hdr,'naxis3')) else begin
               wave = fix(wavel(0))
               index = eit_lzsort(strlowcase(fname),wave)
            endelse
        index = reverse(index)
        n_3d = n_elements(index)
        if n_3d gt 1 then begin
          tx = new_i +1
          num_flist = n_elements(new_flist)
          if tx + 1 eq num_flist then $
            new_flist = [new_flist(0:tx),replicate(new_flist(tx),n_3d-1)] else $
            new_flist = [new_flist(0:tx),replicate(new_flist(tx),n_3d-1),$
               new_flist(tx+1:n_elements(new_flist)-1)]
          tots = [tots,fltarr(n_3d-1)]
          tvals = [tvals,fltarr(n_3d-1)]
          fmark = [fmark,intarr(n_3d-1)]
          save_index = [save_index,intarr(n_3d-1)]
        endif
      endif
      object = eit_fxpar(hdr,'OBJECT')
      if n_x eq 1024 or object eq 'full FOV' then $
          xcen = eit_fxpar(hdr,'CRPIX1') else xcen = n_x/2
      if xcen eq 0 then xcen = n_x/2
      if n_y eq 1024 or object eq 'full FOV' then $
         ycen = eit_fxpar(hdr,'CRPIX2') else ycen = n_y/2
      if ycen eq 0 then ycen = n_y/2
;
; loop over number of images in each file
      for loop = 0, n_3d -1 do begin
        new_i = new_i + 1
        bimage = eit_image(strlowcase(fname),degrid=dum_image,dark=$
            eit_dark(), repl=eit_dark(), image_no = index(loop), /flat)
        if naxis eq 2 then exp_time = eit_fxpar(hdr,'exptime') else $
            exp_time = eit_fxpar(hdr,'exp_time',image_no=index(loop))
;
; Modified for new definition of EXPTIME in V2.0 and later FITS headers.
;
        version = float(strmid(eit_fxpar(hdr,'history'),8,3))
        if version(0) ge 2.0 then shut_time = 0 else begin
              utc_date_obs = anytim2utc(eit_fxpar(hdr,'date_obs',$
                    image_no=index(loop)))
              t_obs =  utc_date_obs.mjd + 1.d-3*utc_date_obs.time/86400
              shut_time = eit_fxpar(hdr,'SHUTTER CLOSE TIME')
              if shut_time le 0 or t_obs lt t_obe then shut_time = 2.1
        endelse
        exptime = exp_time + shut_time
;
        dum_image = temporary(dum_image)/float(exptime(0))
;        if fresit and eit_fxpar(hdr,'naxis1') eq 1024 then fmark(new_i) = 1
        if fresit then begin
           case eit_fxpar(hdr,'naxis1') of
              1024 : fmark(new_i) = 1
              256  : fmark(new_i) = 2
              else:
           endcase
        endif
        if keyword_set(roiit) then tots(new_i) = total(dum_image(indices)) $
        else begin
               max_row = (15*eit_fxpar(hdr,'naxis2'))/16
               tots(new_i) = total(dum_image(*,0:max_row-1))

;           if eit_fxpar(hdr,'naxis1') lt 101 or eit_fxpar(hdr,'naxis2') lt 101 then $
;               tots(new_i) = total(dum_image) else  $ 
;               tots(new_i) = total(dum_image(xcen-50:xcen+49,ycen-50:ycen+49))

        endelse
        sv = stdev(dum_image,moms)
;        tvals(new_i) = min([moms+5*sv,max(dum_image)])
        tvals(new_i) = max(dum_image)
; remove darks, cal lamps- set to zero
;
        if strupcase(object) eq 'DARK' or strupcase(object) eq $
           'CALIBRATION LAMP' then tots(new_i) = 0
        save_index(new_i) = index(loop)
        dum_image = 0
        bimage = 0
      endfor
;
 endfor
; if fresit then begin                ;scale full-res to binned
;     fffr = where(fmark eq 1)
;     ffhr = where(fmark eq 0)
;     ffqr = where(fmark eq 2)
;     if ffqr(0) ne -1 then begin
;        if fffr(0) ne -1 then begin
;           tots(fffr) = tots(fffr)*16
;           tvals(fffr) = tvals(fffr)*16
;        endif
;        if ffhr(0) ne -1 then begin
;           tots(ffhr) = tots(ffhr)*4
;           tvals(ffhr) = tvals(ffhr)*4
;        endif
;     endif else if fffr(0) ne -1 then begin
;           tots(fffr) = tots(fffr)*4
;           tvals(fffr) = tvals(fffr)*4
;        endif
; endif
 dum = widget_event(/nowait)
 widget_control, hourglass=0
 window, /free,xs=512,ys=512
 yrp=minmax(tots)
 plot, psym=10,indgen(n_elements(tots)),tots,yr=[(yrp(0)-0.2*yrp(0))>0,yrp(1)+0.2*yrp(1)]
 xyouts, 100,340,'Select a min level',/device,charsiz=1.5
 cursor, x_val,bot_val, wait = 4, /data & wait, 0.25 
 xyouts, 100, 400, 'Select a max level.', /dev, charsize = 1.5
 cursor, x_val, my_top_val, wait = 4, /data
 wdelete, !d.window
 keep = where((tots gt bot_val) * (tots lt my_top_val))
 new_flist = new_flist(keep)
 save_index = save_index(keep)
 fmark = fmark(keep)
 sdev = stdev(tvals(keep),mean)
; top_val = min([max(tvals(keep)),mean+3*sdev])
 top_val = max(tvals(keep))
 if fresit then top_val = top_val*4.
 if qres then top_val = top_val*16.

; user selected maximum from histogram
 if keyword_set(hist) then begin
    ibad = -1
    repeat begin
      ibad = ibad + 1
      fname = strmid(new_flist(ibad),strpos(strlowcase(new_flist(ibad)),f_template),18)
      if !version.os eq 'vms' then  fname = eit_file2path(fname,/gavroc)$
          else fname = eit_file2path(strlowcase(fname))
;
; check for too many missing blocks
      hdr = headfits(strlowcase(fname))
      naxis = eit_fxpar(hdr,'NAXIS')
      if naxis eq 2 then nmb = eit_fxpar(hdr,'n_missing_blocks') else $
          nmb = eit_fxpar(hdr,'n_missing_blocks',image_no=save_index(ibad))
      bad = nmb / (eit_fxpar(hdr,'BLOCKS_HORZ') * eit_fxpar(hdr,'BLOCKS_VERT'))
    endrep until bad lt 0.1
;
    dum_image = eit_image(strlowcase(fname), degrid = d_image, $
            dark = eit_dark(), repl = eit_dark(), header = hdr, $
            image_no=save_index(ibad), /flat)
    naxis = eit_fxpar(hdr,'NAXIS')
    if naxis eq 2 then exp_time = eit_fxpar(hdr,'exptime') else $
            exp_time = eit_fxpar(hdr,'exp_time',image_no=save_index(ibad))
;
; Modified for new definition of EXPTIME in V2.0 and later FITS headers.
;
    version = float(strmid(eit_fxpar(hdr,'history'),8,3))
    if version(0) ge 2.0 then shut_time = 0 else begin
              utc_date_obs = anytim2utc(eit_fxpar(hdr,'date_obs',$
                    image_no=save_index(ibad)))
              t_obs =  utc_date_obs.mjd + 1.d-3*utc_date_obs.time/86400
              shut_time = eit_fxpar(hdr,'SHUTTER CLOSE TIME')
              if shut_time le 0 or t_obs lt t_obe then shut_time = 2.1
    endelse
    exptime = exp_time + shut_time
;
    sdev = stdev(d_image,mean)
    window, /free,xs=512,ys=512
    plot_io, histogram(d_image,bin=5,omin=omin,omax=omax),$
       psym=10,xtitle='BIN',yrange=[1,max(d_image)],/ystyle
    axis, xaxis=1,xtitle='Flux',xra=[omin,omax], /xstyle
    tmax = strtrim(mean+5*sdev)
    xyouts, 100,400,'Select a max level, suggest='+tmax,/device,charsiz=1.5
    if reproducible_top_value then begin
       read, 'Max value?      ', top_val
       top_val = top_val/exptime(0)
       if fresit then top_val = top_val*4.
       if qres then top_val = top_val*16.
    endif else begin
       cursor, top_val,y_val,/data 
       top_val = (top_val*5+omin)/exptime(0)
       if fresit then top_val = top_val*4.
       if qres then top_val = top_val*16.
    end
    wdelete, !d.window
 endif 

 widget_control,ids(6),/sensitive
 widget_control,ids(7),/sensitive
end

;------------------------------------------------------------------------------
; procedure to save movie
;------------------------------------------------------------------------------
pro mysave_movie
common filestuff1,dirspec,flist,index,list7,save_file
common parms,dateit,dateend,nxit,nyit,waveit,outfit,halfres,sclit,rbin,qres, $
       reproducible_top_value
common parms2,fresit,fmark,use_qkl,use_lz,new_flist,save_index,numfiles,ids
common params,date_start,date_end,n_x,n_y,wavel,top_val
common ratio_par,ratioit,save_195,save_171,s195,s171,mk_ratio,$
        title_start,title_end,stamp_time,mk_diff

   utc_obe_good = anytim2utc('1996/07/18')
   t_obe = utc_obe_good.mjd + 1.d-3*utc_obe_good.time/86400
   if use_lz then f_template = 'efz' else f_template = 'efr'
   outdir = getenv('movie_dir')
   widget_control,outfit,get_value=tsave
   save_file = outdir(0)+tsave(0)
;
; create ratio movie from previously saved movies
   if mk_ratio then begin
      sz1 = size(s195)
      sz2 = size(s171)
      if sz1(0) eq 0 or sz2(0) eq 0 then begin
         print,'You must first create a 171 and a 195 movie'
         mk_ratio = 0
         return
      endif
      num = min([sz1(3),sz2(3)])
; replace missing blocks
      for i = 1, num - 1 do fake_missing_blocks, s171, i, /before, $
         block = 16
      fake_missing_blocks, s171, 0, /after, block = 16
      for i = 1, num - 1 do fake_missing_blocks, s195, i, /before, $
         block = 16
      fake_missing_blocks, s195, 0, /after, block = 16
;
      ratio = s195(*,*,0:num-1)/float(s171(*,*,0:num-1))
      s195 = 0
      s171 = 0
;     scl = 235.
;     b0 = byte(scl*(ratio < 1))
;
; D.M. fecit, 1996 July 12, to make the ratio a little more viewable.
;
      b0 = bytscl(((ratio > 0.20) < 2.25), top = !d.n_colors - 1)
;
      ratio = 0
      wave = 3
      dtstart = anytim2utc(title_start(0))
      dtend = anytim2utc(title_end(0))
      if dtend.mjd-dtstart.mjd gt 2 then begin
          title_start = anytim2utc(title_start(0),/ecs,/date)
          title_end = anytim2utc(title_end(0),/ecs,/date)
      endif
      title = 'SOHO-EIT '+'195/171 A '+strtrim(title_start(0),2)+$
           ' - '+strtrim(title_end(0),2) 
      sz_b = size(b0)  
      n_x_actual = sz_b(1) & n_y_actual = sz_b(2)
      if n_x_actual le 128 then x0 = 0 else x0 = 5
      if n_y_actual eq 1024 then y0 = 60 else if n_y_actual le 128 then $
        y0 = 2 else y0 = 5
      sz_b = size(b0) & n_y_actual = sz_b(2)
      if n_y_actual eq 1024 then y0 = 60 else y0 = 5
;      minb0 = min(b0(where(b0 gt 0)))
      b0 = EIT_RESCALE(temporary(b0),min=0)
; change rebin from 512x512 to *2
      nx=sz_b(1)
      if rbin then b0 = rebin(temporary(b0),nx*2,n_y_actual*2,num, /sample)
;
      b0 = time_stamp(b0, wave, stamp_time, x0, y0)
      save, /xdr, file = save_file(0), wave, title, b0, stamp_time
      b0 = 0
      mk_ratio = 0
      print, '%MOVIE_MAKER-S-SAVED, wrote movie ', save_file(0)
      return
   endif
   if n_elements(wavel) eq 0 then widget_control, waveit,get_value=wavel
   if n_elements(wavel) eq 0 then wavel = 'all'
   if strlowcase(wavel(0)) eq 'all' then wave = 0 else wave = fix(wavel(0))
;
; added for difference movie
   sz = n_elements(new_flist) - mk_diff
   if fresit then halfres = 0
   case 1 of
     halfres: b0 = bytarr(n_x/2,n_y/2,sz) 
     qres:    b0 = bytarr(n_x/4,n_y/4,sz)
     fresit:  begin
           ffqr = where(fmark eq 2,qcnt)
           if qcnt ne 0 then b0 = bytarr(256,256,sz) $
               else b0 = bytarr(512,512,sz) 
        end
     else:    b0 = bytarr(n_x,n_y,sz) 
   endcase       
   nblock = intarr(sz)
   if save_171 then s171 = fix(b0)
   if save_195 then s195 = fix(b0)
   stamp_time = strarr(sz)
   if n_elements(fmark) eq 0 then fmark = intarr(sz)
   widget_control, /hourglass
   max_color = float(min([!d.n_colors, !d.table_size]) - 1)
;
; read and scale each image in movie
;
; add for difference movie
   sz =sz + mk_diff
   for i = 0, sz -1 do begin
      print, '%MOVIE_MAKER-I-FRAME, processing frame ' + strtrim(i, 2) + '.'
      fname = strmid(new_flist(i),strpos(strlowcase(new_flist(i)),f_template),18)
      if !version.os eq 'vms' then  fname = eit_file2path(fname,/gavroc)$
        else fname = eit_file2path(strlowcase(fname))
      if halfres then fmark(i) = 1

      if fresit then if qcnt ne 0 then begin
          case fmark(i) of
            1: begin
                fmark(i) = 0
                qres = 1
               end
            0: begin
                fmark(i) = 1
                qres = 0
               end
            2: begin
                fmark(i) = 0
                qres = 0
               end
          endcase
      endif

      image = eit_image(strlowcase(fname),repl=eit_dark(),dark=eit_dark(),$
                    n_block=dum,degrid_image=iarr,header=hdr,/flat,$
                    image_time=dum_time,top=top_val,half=fmark(i),$
                    image_no=save_index(i),quarter=qres)  
      naxis = eit_fxpar(hdr,'NAXIS')
      if naxis eq 2 then exp_time = eit_fxpar(hdr,'exptime') else begin
            exp_time = eit_fxpar(hdr,'exp_time',image_no=save_index(i))
            dum_time = eit_fxpar(hdr,'start_time',image_no=save_index(i))
      endelse
;
; Modified for new definition of EXPTIME in V2.0 and later FITS headers.
;
      version = float(strmid(eit_fxpar(hdr,'history'),8,3))
      if version(0) ge 2.0 then shut_time = 0 else begin
              utc_date_obs = anytim2utc(eit_fxpar(hdr,'date_obs',$
                    image_no=save_index(i)))
              t_obs =  utc_date_obs.mjd + 1.d-3*utc_date_obs.time/86400
              shut_time = eit_fxpar(hdr,'SHUTTER CLOSE TIME')
              if shut_time le 0 or t_obs lt t_obe then shut_time = 2.1
      endelse
      exptime = exp_time + shut_time
      iarr = temporary(iarr)/float(exptime(0))
      if fresit and fmark(i) then iarr = temporary(iarr)*4.
      if fresit and qres then iarr = temporary(iarr)*16.
      if n_elements(top_val) eq 0 then top_val = max(iarr) 
      if wave(0) eq 284 then begin
         iarr = temporary(iarr)*10
         top_val2 = top_val*10
      endif else top_val2 = top_val
;
; add in difference movie
      if mk_diff then begin
         if i ne 0 then begin
            sz_a = size(iarr) & bx = fltarr(sz_a(1), sz_a(2), 2)
            bx(0,0,0) = iarr_plus1
            bx(0,0,1) = iarr < top_val2
            fake_missing_blocks, bx ,0, /after, block = 8
            fake_missing_blocks, bx ,1, /before, block = 8
            iarr_plus1 = bx(*,*,0) - bx(*,*,1)
            b0(*,*,sz-1-i) = bytscl(iarr_plus1,min=-5,max=5)
         endif
         iarr_plus1 = iarr < top_val2
         if i ne sz -1 then begin
           stamp_time(sz-2-i) = anytim2utc(dum_time,/ecs,/trunc)
         endif
      endif else begin
        scl = max_color/alog10(top_val2)
;        if wave(0) eq 304 or wave(0) eq 284 then low = 0.03 else low = 1 
        case wave(0) of
            304: low = 0.2
            284: low = 0.1
            else: low = 1
        endcase
        if low lt 1 then begin
           b0(*,*,sz-1-i) = bytscl(scl*alog10(temporary((iarr > low) < top_val2)))
        endif else begin
           b0(*,*,sz-1-i) = byte(scl*alog10(temporary((iarr > low) < top_val2)))
        endelse
        stamp_time(sz-1-i) = anytim2utc(dum_time,/ecs,/trunc)
        nblock(sz-1-i) = dum
      endelse
      if save_171 then s171(*,*,sz-1-i) = iarr
      if save_195 then s195(*,*,sz-1-i) = iarr
      iarr= 0
      image = 0
      if i eq 0 then title_end = anytim2utc(sxpar(hdr,'DATE_OBS'),/ecs,/trunc) 
      if i eq sz -1 then title_start = anytim2utc(sxpar(hdr,'DATE_OBS'),$
              /ecs,/trunc) 
   endfor
   numrep = where(nblock gt 0,cnt)
;
; fill in any missing blocks
;   if halfres then block_size = 16 else block_size = 32 
   block_size = 16
   if cnt gt 0 then for i = 0,cnt-1 do begin
       if numrep(i) eq 0 then $
          fake_missing_blocks,b0, numrep(i), /after, block_size = block_size $
          else fake_missing_blocks,b0,numrep(i), /before, block_size=block_size
   endfor
   if save_171 then $
     if cnt gt 0 then for i = 0,cnt-1 do begin
       if numrep(i) eq 0 then $
          fake_missing_blocks,s171,numrep(i), /after, block_size=block_size $
          else fake_missing_blocks,s171,numrep(i), /before, $
            block_size = block_size
     endfor
   if save_195 then $
     if cnt gt 0 then for i = 0,cnt-1 do begin
       if numrep(i) eq 0 then $
          fake_missing_blocks, s195,numrep(i),/after,block_size=block_size $
          else fake_missing_blocks, s195,numrep(i),/before,block_size=block_size
     endfor
   dtstart = anytim2utc(title_start(0))
   dtend = anytim2utc(title_end(0))
   if dtend.mjd-dtstart.mjd gt 2 then begin
       title_start = anytim2utc(title_start(0),/ecs,/date)
       title_end = anytim2utc(title_end(0),/ecs,/date)
   endif else if dtend.mjd-dtstart.mjd eq 0 then $
       title_end = strmid(anytim2utc(title_end(0),/ecs,/time,/trunc),0,5)
;
   case wave(0) of
      171: lines = ' Fe IX/X '
      195: lines = ' Fe XII '
      284: lines = ' Fe XV '
      304: lines = ' He II/Si XI '
     else: lines = ''
   endcase
;
   start_len = strlen(title_start(0)) & end_len = strlen(title_end(0))
   if start_len eq 19 then begin
      title_start(0) = strmid(title_start(0), 0, 16)
   end
   if end_len eq 19 then begin
      title_end(0) = strmid(title_end(0), 0, 16)
   end
   title = 'SOHO-EIT '+ lines + strtrim(wavel(0), 2) + 'A  '+ $
      strtrim(title_start(0),2) + ' - ' + strtrim(title_end(0),2) + ' UT' 
   sz_b = size(b0)  
   n_x_actual = sz_b(1) & n_y_actual = sz_b(2)
   if n_x_actual le 128 then x0 = 0 else x0 = 5
   if n_y_actual eq 1024 then y0 = 60 else if n_y_actual le 128 then $
        y0 = 2 else y0 = 5
;  if n_y eq 1024 then y0 = 60 else y0 = 5
;
; rescale from movie min to movie max if set
   if sclit then begin
;      minb0 = min(b0(where(b0 gt 0)))
      b0 = EIT_RESCALE(temporary(b0),min=0)
   endif
; change rebin from 512x512 to *2
   nx = sz_b(1)
   if rbin then b0 = rebin(temporary(b0), nx*2, n_y_actual*2, sz, /sample)
;
   b0 = time_stamp(b0,wave,stamp_time, x0, y0)
;
; Greyscale replaced by wavelength-appropriate color table, 1997 August
; D.M. fecit.
;
;   if mk_diff then wave = 0
; 
  save, /xdr,file=save_file(0), wave, title, b0, stamp_time
   b0 = 0
   widget_control, hourglass=0
   print, '%MOVIE_MAKER-S-SAVED, wrote movie ', save_file(0)
   widget_control,ids(6),sensitive=0
   widget_control,ids(7),sensitive=0
 end

;------------------------------------------------------------------------------
; event handler
;------------------------------------------------------------------------------

PRO MOVMK_Event, Event
common filestuff1,dirspec,flist,index,list7,save_file
common parms,dateit,dateend,nxit,nyit,waveit,outfit,halfres,sclit,rbin,qres, $
       reproducible_top_value
common parms2,fresit,fmark,use_qkl,use_lz,new_flist,save_index,numfiles,ids
common params,date_start,date_end,n_x,n_y,wavel,top_val
common ratio_par,ratioit,save_195,save_171,s195,s171,mk_ratio,$
        title_start,title_end,stamp_time,mk_diff
;
;
  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  CASE Ev OF 

  'donehelp': killhelp1        ;kill help widget
  
  'halfit': BEGIN              ;select out size              
      halfres = 0
      rbin = 0
      fresit = 0
      qres = 0
      CASE Event.Value OF
      0: halfres = 0
      1: halfres = 1           ;select half resolution output
      2: qres = 1              ;select quarter resolution output
      3: rbin = 1              ;rebin to 512x512
      4: fresit = 1            ;select inclusion of full res & binned  
      ENDCASE
      END

  'scls': BEGIN               ;select rescaling from 0-max
      CASE Event.Value OF
      0: sclit = 0
      1: sclit = 1
      ENDCASE
      END

  'ratioit': BEGIN            ;save either 171 or 195 images for
      CASE Event.Value OF     ;making ratio movie
      0: begin
         save_171 = 1
         save_195 = 0
         end
      1: begin
         save_171 = 0
         save_195 = 1
         end
      2: begin
         save_171 = 0
         save_195 = 0
         end
      ENDCASE
      END

  'BGROUP3': BEGIN
      CASE Event.Value OF
      0: BEGIN                                         ;get image list
          widget_control, dateit, get_value=date_start
          widget_control, dateend, get_value=date_end
          widget_control, nxit, get_value=n_x
          widget_control, nyit, get_value=n_y
          widget_control, waveit, get_value=wavel
          widget_control, /hourglass
          n_x = fix(n_x(0))
          n_y = fix(n_y(0))
          if n_x lt 32 then n_x = n_x*32
          if n_y lt 32 then n_y = n_y*32
          if fresit then begin
            flist = eit_catrd(timerange=[date_start,date_end],wave=wavel(0),$
                 object='full fov',lz=use_lz)
            null = where(flist ne '',found)
            if found ne 0 then flist = flist(null)
;            f1 = ''
;            f2 = ''
;            flist = eit_catrd(timerange=[date_start,date_end],wave=wavel(0),$
;                 nx=1024,ny=1024,file=f1,lz=use_lz)
;            null = where(flist ne '')
;            flist = flist(null)
;            f1 = f1(null)
;            flist2 = eit_catrd(timerange=[date_start,date_end],wave=wavel(0),$
;                 nx=512,ny=512,xbin=2,ybin=2,file=f2)
;            null = where(flist2 ne '')
;            flist2 = flist2(null)
;            f2 = f2(null)
;            if flist(0) ne '' and flist2(0) ne '' then begin
;                 flist = [flist2,flist]
;                 flist = flist(sort([f2,f1]))
;            endif
          endif else begin
              flist = eit_catrd(timerange=[date_start,date_end],wave=wavel(0),$
                 nx=n_x,ny=n_y,lz=use_lz)
              good = where(flist ne '',found)
              if found ne 0 then flist = flist(good)
          endelse
;
          flist = flist(where(strpos(flist, 'D:') lt 0))
;
          if flist(0) ne '' then flist = reverse(flist)
          widget_control, hourglass=0
          widget_control, numfiles,set_value=n_elements(flist)
          widget_control, list7,set_value=flist
        END
      1: showeit1                           ;display selected image
      2: begin                              ;delete selected image
           sz = n_elements(flist)
           newlist = strarr(sz-1)
           case index of
             0: newlist = flist(1:sz-1)
             sz-1: newlist = flist(0:sz-2)
             else: begin
                     newlist(0:index-1) = flist(0:index-1)
                     newlist(index:sz-2) = flist(index+1:sz-1)
                   end
           endcase
           flist = newlist
           widget_control, numfiles,set_value=n_elements(flist)
           widget_control, list7,set_value=flist
         end
      3: scaleit			   ;auto-scale images(central 100x100)
      4: scaleit,/roiit                    ;use region of interest scaling
      5: scaleit,/hist                     ;histogram defined max
      6: mysave_movie                      ;save movie
      7: begin
          save_171 = 0
          save_195 = 0
          mk_diff = 1
          mysave_movie
          mk_diff = 0
         end
      8: begin                             ;save ratio movie
          mk_ratio = 1
          widget_control, /hourglass
          mysave_movie
          widget_control, hourglass = 0
         end
      ELSE: Message, /continue,'Unknown button pressed'
      ENDCASE
      END
      
  'BGROUP4': BEGIN
      CASE Event.Value OF
      0: eit_movie, save_file(0)                  ;view last made movie
      1: XLOADCT, file=getenv('coloreit')         ;xloadct
      2: WIDGET_CONTROL, /DESTROY, event.top     ;exit
      3: helpout1                                ;help
      ELSE: Message, /continue,'Unknown button pressed'
      ENDCASE
      END

  'LIST7': begin
             index = event.index                 ;get index for file choice
             if event.clicks eq 2 then showeit1
           end
  ENDCASE
END

;------------------------------------------------------------------------------
; top level routine - set up main widget interface as well as initial values
;------------------------------------------------------------------------------

PRO movie_maker, GROUP = Group, rtv = rtv
common filestuff1,dirspec,flist,index,list7,save_file
common parms,dateit,dateend,nxit,nyit,waveit,outfit,halfres,sclit,rbin,qres, $
       reproducible_top_value
common parms2,fresit,fmark,use_qkl,use_lz,new_flist,save_index,numfiles,ids
common ratio_par,ratioit,save_195,save_171,s195,s171,mk_ratio,$
        title_start,title_end,stamp_time,mk_diff
;
  IF N_ELEMENTS(Group) EQ 0 THEN GROUP = 0

  reproducible_top_value = keyword_set(rtv)

  MOVMK = WIDGET_BASE(GROUP_LEADER=Group, COLUMN=1, MAP=1, $
      TITLE='EIT Movie Maker', UVALUE='MOVMK')

  mk_ratio = 0
  mk_diff = 0
  Btns459 = [ 'Update Listing','Display Image','Delete Image','Auto Scale',$
       'ROI Scale','Hist Scale','Save Movie','Save Diff','Make Ratio']
  BGROUP3 = CW_BGROUP( MOVMK, Btns459, ROW=1, UVALUE='BGROUP3', ids=ids)
  widget_control,ids(6),sensitive=0
  widget_control,ids(7),sensitive=0
  BTNS460 = ['View Movie','XLOADCT','Done','Help' ]
  BGROUP4 = CW_BGROUP( MOVMK, Btns460, ROW=1, UVALUE='BGROUP4')

  child = WIDGET_BASE(MOVMK, /row)
  dateit = cw_field(child,VALUE=strmid(!stime,0,11),title='Select Start Date:',$
      UVALUE='dateit',YSIZE=1,xsize=20)
  dateend = cw_field(child,VALUE=strmid(!stime,0,11),title='Select End Date:',$
      UVALUE='dateend',YSIZE=1,xsize=20)
  use_qkl = 1 & use_lz = 0

  child = WIDGET_BASE(MOVMK, /row)
  nxit = cw_field(child,VALUE=1024,title='Select X size:',$
      UVALUE='nx',YSIZE=1,xsize=5)
  nyit = cw_field(child,VALUE=1024,title='Select Y size:',$
      UVALUE='ny',YSIZE=1,xsize=5)
  waveit = cw_field(child,VALUE=' ',title='Select Wavelength:',$
      UVALUE='waveit',YSIZE=1,xsize=5)
  sclit = 0
  scls = cw_bgroup(child, ['Std. Scale','Rescale min-to-max'],/exclusive,$
              /frame,/row,uvalue='scls',set_value=0)
 
  child = WIDGET_BASE(MOVMK, /row)
  halfres = 0
  rbin = 0
  fresit = 0
  qres = 0
  halfit = cw_bgroup(child, ['Full Res','Half Res','Quarter Res',$
            'Rebin Twice Size','Include Full+Binned'],/exclusive,/frame,$
            /row,uvalue='halfit',set_value=0)
  save_171 = 0
  save_195 = 0
  ratioit = cw_bgroup(child, ['Save 171','Save 195','No save'],/exclusive,$
              /frame,/row,uvalue='ratioit',set_value=2)

  child = WIDGET_BASE(MOVMK, /row)
  dum = anytim2utc(!stime,/ecs)
  tdum = strmid(dum,0,4)+strmid(dum,5,2)+strmid(dum,8,2)
  save_file = 'ems'+tdum+'_xxx.save'
  outfit = cw_field(child,VALUE=save_file,title='Movie File Name:',$
      UVALUE='outfit',YSIZE=1,xsize=30)

  LABEL16 = WIDGET_LABEL( MOVMK, UVALUE='LABEL16', $
      VALUE='List of Files for Movie')
  
;list widget - displays todays QKL files by default
  flist = eit_catrd()
  if flist(0) ne '' then flist = reverse(flist)
  ys = n_elements(flist)
  numfiles = cw_field(movmk,value=ys,uvalue='numfiles',ysize=1,xsize=5,$
       title='Number Selected Files:',/noedit)
  LIST7 = WIDGET_LIST( MOVMK,VALUE=flist, UVALUE='LIST7', YSIZE=min([10,ys]))

  WIDGET_CONTROL, MOVMK, /REALIZE
  XMANAGER, 'MOVMK', MOVMK
END
