;+
; Project     : SOHO - EIT
;
; Name        : 
;	MK_EIT_L1
; Purpose     : 
;	Create Level 1 FITS files via eit_prep. Demo program to show usage.
; Use         : 
;	mk_eit_l1,date,[qkl=qkl]
;        IDL> mk_eit_l1,'20-mar-1997'
;        IDL> mk_eit_l1,['5-mar-1997','10-mar-1997']
;
;
; Inputs      : 
;        DATE : Date in any accepted format or 2 element array consisting
;               of start and stop dates.
; Opt. Inputs :
;        List = input file listing if do not want all files for a given day 
;        QKL  = if keyword set then use quicklook rather than level zero data
;        fullfov = set if want only full FOV images
;        response_norm = if set normalize response
;
; Outputs     : 
;         Create a single output FITS file for each image. The processing
;         is done by EIT_PREP. A background is subtracted off, image is
;         degridded, flat fielded, and normalized
;
; Calls       : EIT_PREP, EIT_CATRD, EIT_FXPAR
; Common      : 
;	None.
; Restrictions: 
;	None.
; Side effects: 
; Category    : 
;	Data Handling, I/O, FITS.
; Written     : 1997 March 20, J. Newmark
;               1997 July 23 Added fullfov keyword, use /nodata
;                            in eit_prep to save memory J Newmark
;               1997 Aug 14 Added fullres keyword J. Newmark
;               1997 Sep 23 added input LIST keyword, fixed partial fov bug. J. Newmark
;               1998 Jan 21 added response_norm keyword J. Newmark
;-
;------------------------------------------------------------------------------
;
;
pro mk_eit_l1,date_obs,qkl=qkl,fullfov=fullfov,fullres=fullres,list=list,$
              response_norm=response_norm

if not keyword_set(qkl) then lz = 1 else lz = 0
if keyword_set(response_norm) then response_norm = 1 else response_norm = 0
if n_elements(list) eq 0 then $
  if n_elements(date_obs) eq 1 then list=eit_catrd(date_obs,lz=lz) $
       else list=eit_catrd(timerange=date_obs,lz=lz)
files=eit_file2path(list)
numf = n_elements(files)

naxis3 = intarr(numf)
naxis2 = intarr(numf)
for i = 0, numf -1 do begin
     hdr = headfits(files(i))
     naxis2(i) = eit_fxpar(hdr,'naxis2')
     sz = eit_fxpar(hdr,'p2_x') + eit_fxpar(hdr,'p2_y') - eit_fxpar(hdr,'p1_x') $
         - eit_fxpar(hdr,'p1_y') + 2
     naxis3(i) = eit_fxpar(hdr,'naxis3')
     if naxis3(i) eq 0 then naxis3(i) = naxis3(i) + (sz ne 2048) 
;     naxis3(i) = eit_fxpar(hdr,'naxis3')
endfor

if keyword_set(fullres) then full = where(naxis3 eq 0 and naxis2 eq 1024) $
   else full = where(naxis3 eq 0)
if full(0) ne -1 then eit_prep,files(full),/outfits,/normalize,/filter_norm,$
                        /nodata,response_norm=response_norm

if keyword_set(fullfov) then return

part = where(naxis3 ge 1)
if part(0) ne -1 then begin
  for i =0,n_elements(part) -1 do begin
      for j=0,naxis3(part(i))-1 do eit_prep,files(part(i)),/normalize,$
           /outfits,/filter_norm,image_no=j,/nodata,response_norm=response_norm
  endfor
endif

end

