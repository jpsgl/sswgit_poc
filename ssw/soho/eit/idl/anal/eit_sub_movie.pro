pro eit_sub_movie,header,movie,tstart,tstop,ref_helio=ref_helio,ref_date=ref_date,$
    outsize=outsize,wave=wave,lz=lz,object=object,arcsec=arcsec,list=list
;+
; NAME:
;	EIT_SUB_MOVIE
;
; PURPOSE:
;       Create a movie of a subfield image from a sequence of full
;       field iages, tracking the region of interest with solar
;       rotation.
;
; CATEGORY:
;	FITS processing
;
; CALLING SEQUENCE:
;       EIT_SUB_MOVIE outheader,outmovie,start time,stop time [,/arcsec]
;           [,ref_helio=ref_helio] [,ref_date=ref_date] [,wave=wave]
;           [,outsize=outsize] [,object=object] [,list=list]
;
; INPUTS:
;   tstart    I  = start time
;   tstop     I  = stop time
; 
; OPTIONAL INPUT KEYWORD PARAMETERS: 
;   ref_helio I  = heliocentric Center of FOV (lon,lat)
;   arcsec    I  = set if ref_helio is in arcsec coords (lon-lat) instead of 
;                  helio lon-lat
;   ref_date  I  = reference time/date of center
;   outsize   I  = number of pixels for output image (x,y)
;   wave      I  = wavelength of images (default=195)
;   object    I  = object type (e.g. 'full fov') default = all images
;   list      I  = input list instead of time range
;
; OUTPUTS:
;   header    O  = output header (structure)
;   movie     O  = output processed subfield 3D data array
;
; COMMON BLOCKS: none.
;
; SIDE EFFECTS: 
;
; RESTRICTIONS:
;
; PROCEDURE:
;       Read in catalog data. Read in selected images and headers.
;       Force solar pointing over short period of images to be constant,
;       this eliminates the "bad" limb fits. Extract selected FOV, tracking
;       solar rotation.
;
; MODIFICATION HISTORY:
; 	Written by:  J. Newmark 	Date.  September 1997
;       24-Nov-1997  J. Newmark   -  Account for CRPIX1,2 offset in FITS header
;       07-Apr-1998  J. Newmark   - update
;-

if not keyword_set(wave) then wave = 195
if not keyword_set(object) then object = 0

if n_elements(list) eq 0 then $
   list = eit_catrd(timerange=[tstart,tstop],wave=wave,object=object)
read_eit,eit_file2path(list),idx1,data1,outsize=[1024,1024]
;
; use single image x,y center since limb fits vary
; subtract 1 since going from FITS to IDL. Add back in later.
;
crpix1 = median(idx1.crpix1 - 1)
idx1.crpix1 = crpix1
crpix2 = median(idx1.crpix2 - 1)
idx1.crpix2 = crpix2
;
; extract chosen field of view
ssw_track_fov,idx1,temporary(data1),idx2,data2,ref_helio=ref_helio,$
     ref_date=ref_date,outsize=outsize,arcsec=arcsec

sz = size(data2)
for i = 1,sz(3)-1 do fake_missing_blocks,data2,i,/before,block=16
fake_missing_blocks,data2,0,/after,block=16

eit_prep,idx2,header,movie,data=temporary(data2),/normalize

idx2.crpix1 = idx2.crpix1 + 1
idx2.crpix2 = idx2.crpix2 + 1

end
