function eit_temp,dem=dem,inratio=inratio,pressure=pressure,pre_calc=pre_calc,$
     show=show, dn171=dn171,dn195=dn195,dn284=dn284,expotime=expotime,$
     r195_to_171=r195_to_171,r284_to_195=r284_to_195
;+
; NAME:
;  eit_temp
; PURPOSE:
;  Specify  195/171 or 284/195 ratio or individual DN values ==> compute TEMP, 
;                                                     emission measure
;  NOTE: THIS IS MAINLY A PLACEHOLDER, HOWEVER IT MAY BE USEFUL.
;
; CALLING SEQUENCE:
;         temp=eit_temp(dn171=dn171,dn195=dn195,expotime=expotime,$
;                       inratio=inratio,dn284=dn284]
;
; INPUTS:
;    MUST SPECIFY EITHER
;      dn171,dn195,dn284 = observed flux in image, best if corrected by
;                    eit_prep, must have background subtracted
; OR   
;      inratio     = calculated 195/171 or 284/195 ratio, e.g. from eit_ratio
;
;      pre_calc    = MUCH FASTER - use pre-calculated spectra if online.
;      show        = set if want to see temperature dependence upon ratio
;      expotime = total exposure time (including shutter close time)
;                 if specifying non-normalized DN values
;      r195_to_171 = (default) return temperature based upon 195/171 ratio
;      r284_to_195 =           return temperature based upon 284/195 ratio
;    
; OUTPUTS:
;   This function returns temperature and emission measure (cm^-5).
;   IF a dn195 level is not input the output emission measure is
;   normalized to 1 DN a observed in 195.
;
;   temperature    = computed temperature for given ratio
;   dem            = column emission measure (cm^-5)
;                    value returned can be compared to what one
;                    would get from a true DEM calculation, i.e.
;                    we return the Isothermal Emission measure *0.7/0.3
;
; RESTRICTIONS:
;   Default: Pressure for level populations = (Ne*T) 1e15 K cm^-3
;                       (not very sensitive to this)
;
;   VERY Slow if calculating spectra from scratch.
;
;   Pre_calc: this uses a set of pre-calculated spectra for a
;             pressure of 1e15 cm^-3 K and spanning emperatures
;             from 30,000-9.0e6 K. These are then interpolated to
;             obtain the final answer.
;
; MODIFICATION HISTORY:
;   1997 Feb 05, J.S. Newmark, Written.
;   1997 Sep 29, Version 2.0 Substantial re-write to work with CHIANTI
;                 J. Newmark
;   1997 Oct 06, Version 2.1 J. Newmark, use fact that temperature is
;                slowly varying and dem is linearly proportional to
;                signal, use histograms - speed up for large inputs
;   1998 Feb 13, Add error checking, put floor on arrays of 1 DN, max 
;                ratio = 5
;   1999 May 07, J.S. Newmark - new calibration
;   1999 Jun 08, Version 3.0, J. Newmark
;                BUG FIX for Emission measure calculation
;-

defsysv,'!xuvtop',exist=yes_chianti
if not yes_chianti then ssw_packages,/chianti,/append

if not keyword_set(expotime) then expotime = 1.
if not keyword_set(pressure) then pressure = 1.e15
;
; below is for full version when CHIANTI is available
;
if keyword_set(pre_calc) then begin
   restore,concat_dir(getenv('SSW_EIT_RESPONSE'),'chianti_spec.save')
;   restore,'dn2.save'
   inspec = temporary(spec0)
   pressure = pressure(0)
endif else begin
   temp=indgen(20)*0.1+4.6
   temp = 10^temp
endelse

resp195 = eit_flux(195,temp,pressure=pressure,dem_name='unit',inspec=inspec,$
   inwave=waves)

if not keyword_set(r284_to_195) then begin
     resp171 = eit_flux(171,temp,pressure=pressure,dem_name='unit',inspec=inspec,$
        inwave=waves)
     ratio = resp195 / resp171 
     title = 'EIT 195/171 Ratio'
     if not keyword_set(inratio) then inratio = (dn195>1) / float(dn171>1)
     yr = [1e-3,4.5]
endif else begin
     resp284 = eit_flux(284,temp,pressure=pressure,dem_name='unit',inspec=inspec,$
        inwave=waves)
     ratio = resp284 / resp195
     title = 'EIT 284/195 Ratio'
     if not keyword_set(inratio) then inratio = (dn284>.1) / float(dn195>1)
     yr = [1e-3,1]
endelse

atemp = alog10(temp)
if keyword_set(show) then plot,atemp,ratio,yr=yr,$
   title=title,xtitle='Log Temp (K)',ytitl='Ratio'

dum = min(ratio,minsub)
dum = max(ratio(minsub:n_elements(ratio)-1),maxsub)
maxsub = maxsub + minsub

; use histogram to speed up calculations if input is large, e.g.
;    whole images
if n_elements(inratio) eq 1 then inratio = fltarr(1) + inratio
vals = histogram(inratio<5,bin=0.01,min=0,reverse=ind)
num = n_elements(vals)
hist_ratio = findgen(num)*0.01
;
his_temp = interpol(atemp(minsub:maxsub),ratio(minsub:maxsub),hist_ratio)
his_temp = 10^his_temp

temp = inratio
for i = 0,num-1 do if ind(i) ne ind(i+1) then $
                     temp(ind(ind(i):ind(i+1)-1)) = his_temp(i)

if n_elements(dn195) ne 0 then begin
    newspec = interp2d(inspec,waves,atemp,waves,alog10(his_temp),/grid)
;
; calculate "unit" emission measure and use fact that dem varies
;  linearly with input signal
;
    his_dem = eit_flux(195,his_temp,pressure=pressure,dn=intarr(num)+1,$
                 inspec=newspec,inwave=waves)
    dem = inratio
    for i = 0,num-1 do if ind(i) ne ind(i+1) then $
                         dem(ind(ind(i):ind(i+1)-1)) = his_dem(i)
    dem = dem * (dn195>1)
endif

return,temp
end


