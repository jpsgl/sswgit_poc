;+
; NAME        : CAT_DIRECTORY
;
; PURPOSE     : Locate requested catalog
;
; CATEGORY    : EIT Catalog
;
; EXPLANATION : 
;
; SYNTAX      : 
;
; EXAMPLES    : none
; 
; CALLED BY   : EXP
;
; CALLS TO    : none
;
; ARGUMENTS   :	
;	IN_DATE	:   If present, a string date in the format "yyyy/mm/dd"
;		   of the daily catalog to be searched. 
;		   Default is the date of the most recent catalog.
;		   New feature for the EIT_DISPLAY program is that the DATE
;		   argument can be used to input a psuedo YESTER keyword as 
;		   a string. Using "/Y" or "Y=", do not spell out YESTER
;		   (ex: print,catrd_daily("/y") for yesterday or
;		        print,catrd_daily("y=3") for three days ago.

;
; KEYWORDS    
;	YESTER  If present, indicates the number of 'days/catalogs' backward
;		in time you want as the catalog input.  Note that this is
;		really the number of backward catalogs, so days may not
;		line up as expected, but they should.
;	CUR_DIR_CNT	: Return the index of DIR for the selected catalog.
;	USE_LAST_REC	: Return which record is to be displayed first.
;			  0 = once cat file is found display first rec
;			  1 = once cat file is found display last rec
;	NO_NEXT		: if present, don't just supply the next available
;			  catalog, return a null string.
;	DIR_CNT		: Return the number of catalogs found.
;
; OUTPUTS     :
;	Returns the results of the find_file on the catalogs found.
;
; COMMON      : none
;
; RESTRICTIONS: none
;
; SIDE EFFECTS: none
;
; PROJECT     : SOHO - EIT
;
; HISTORY     : V1,  Elaine Einfalt (HSTX)
;		1996 march 31, added /NO_NEXT
;		1996 May 13, allow YESTER pseudo format in the DATE argument
;               1996 May 24, Sam Freeland - for UNIX, read dirs from file
;               1997 Jan 16, Sam Freeland - handle all cats in $EIT_QLK_CATALOG
;               1997 Jan 21, J. Newmark - handle specific is_gsfcvms case
;                    (including not all SSW), and generic VMS installation 
;		1997 Oct 27, Einfalt - modified to distinguish between
;		     Goaddard-like VMS with local catalog and with out catalogs
;		     If VMS without catalogs, must define logical EIT_QUICKLOOK
;		     like: $DEFINE/syst/exec/trans=conc -
;				EIT_QUICKLOOK DNFS5:[QUICKLOOK.]
;               2002 Aug 14, F. Auchere changed calls to findfile to calls to
;                    find_file to fix a long catalog list problem
;
; CONTACT     : eit@xanado.nascom.nasa.gov
;-

function CAT_DIRECTORY, in_date, cur_dir_cnt  = cur_dir_cnt,    $
		                 use_last_rec = use_last_rec,  	$
			         yester       = yester,		$
				 no_next      = no_next,	$
			         dir_cnt      = dir_cnt,        $
                                 testing      =testing

; S.L.Freeland - Jan 16, 1997 - Permit all UNIX catalogs in $EIT_QKL_CATALOG
; NOTE - after verifying single UNIX directory - the UNIX and VMS cases can
;        be made compatible (ie, the case statement could vanish)
;        if CAT_DIR: <->    EIT_QKL_CATALOG via <file_list>

testing = keyword_set(testing)
oldlist = concat_dir(getenv('SSW_SITE_SETUP'),'eit_catdir.list') ; backward compatible
case 1 of 
   (is_gsfcvms() and 1) : begin                         ; Goddard-like VMS
      if is_gsfcvms() eq 3 then begin			; VMS w/o local catalog
        disk = getenv('EIT_QUICKLOOK')      ; so use SOHO ARCHIVE
        dir = find_file(disk+'[000000...]*.cat_copy', count=dir_cnt)
      endif else begin					; VMS with local cats
         disk = 'CAT_DIR:' 
         dir = find_file(disk+'*.cat_copy', count = dir_cnt)
      endelse
   endcase

   getenv('EIT_QKL_CATALOG') ne '': begin               ; 1 directory
      if testing then message,/info,"Using one directory"    
      dir = concat_dir(getenv('EIT_QKL_CATALOG'),'*.cat_copy')
      dir = find_file(dir)    
      exist = where(dir ne '', dir_cnt)                  
      if dir_cnt eq 0 then begin                            ; empty protect
         message,/info,"EIT_QKL_CATALOG defined but empty"
         return,''
      endif
   endcase   

   file_exist(oldlist): begin                               ; backward compat
        if testing then message,/info,"Using list file"             
        dir = rd_tfile(oldlist)                               ; (use list file)
        dir_cnt = n_elements(dir)                
   endcase

   else: begin                                              ; backward compat
        if !version.os eq 'vms' then dir_str = '[...]' $
           else dir_str = '/*/*/*/'
	if is_gsfcvms() eq 3 then dir_str = '[000000...]'
        disk = getenv('EIT_QUICKLOOK') + dir_str            ; (regenerate)
        dir = find_file(disk+'*.cat_copy', count=dir_cnt)
   endcase     
endcase

 if dir_cnt eq 0 then begin			; found no catalogs here
    print,'>>> NO CATALOGS (*.CAT) FOUND AT DIRECTORY ' + DISK
    return, dir
 endif
 ; If no specific date in requested then use most recent catalog
 ;    (displaying last record available at the time the cat is openned)
 ; When a date is requested and has a cat file, use it
 ;    (displaying first record of this cat file)
 ; When a date is requested and there is no cat file, use next cat file
 ;    (displaying first record of this cat file)

 if n_elements(in_date) eq 0 then begin			; no date input

    if n_elements(yester) eq 0 then cur_dir_cnt = dir_cnt-1 $ ; use most recent cat
    else cur_dir_cnt = (dir_cnt-yester-1) > 0   	; use n-yester-1, if any
    use_last_rec = 1                                    ; start with last rec

 endif else if in_date eq 'yester' then begin		; pseudo yester keyword
							; used in date argument
    cur_dir_cnt = (dir_cnt-yester-1) > 0   		; use n-yester-1
    use_last_rec = 0                                    ; start with first rec

 endif else begin

     ; Get the cat filenames as tai times for comparasions

     break_file, dir, dummy, dummy, fdate, dummy, dummy, dummy

     fdate = strmid(fdate,0,4) + '/' + strmid(fdate,4,2) + '/' + $
	     strmid(fdate,6,4) 


     tai_dir  = utc2tai(anytim2utc(fdate))		; dir files as tai

     if valid_time(in_date) then $			; check users time
        tai_date = utc2tai(anytim2utc(in_date)) $	; use input as tai
     else begin						; use input bad format
	print,' ** ERROR in time format in DATE argument, use YYYY/MM/DD.'
	print,' ** PROVIDING most recent catalog.'
        cur_dir_cnt = dir_cnt-1        		; use most recent cat
        use_last_rec = 0                                ; start with first rec

	return, dir
     endelse

     after = where(tai_dir ge tai_date, n_after)	; look for cat file

     if n_after gt 0 then begin				; exact or next cat
        if keyword_set(no_next) then $			; exact date only
	   if tai_date ne tai_dir(after(0)) then return,''

	cur_dir_cnt  = after(0) 			; so use it
	use_last_rec = 0				; start with first rec
     endif else begin					; no cats after in_date
        if keyword_set(no_next) then return,''		; exact date only

	cur_dir_cnt  = dir_cnt - 1			; use most recent cat
	use_last_rec = 1				; start with last rec
     endelse


 endelse
return, dir
end
