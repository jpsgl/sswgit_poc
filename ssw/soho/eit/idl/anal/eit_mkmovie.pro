pro eit_mkmovie,list,outsize=outsize,mpeg=mpeg,gif=gif,wave=wave,movie_name=movie_name
;+
; NAME:
;	EIT_MKMOVIE
;
; PURPOSE:
;       Create a GIF or MPEG movie from an Input FITS file list or byte-scaled
;       data cube
;
; CATEGORY:
;	Animation
;
; CALLING SEQUENCE:
;       eit_mkmovie,input [,outsize=outsize][,/mpeg][,/gif][,wave=wave][movie_name=name]
;
; INPUTS:
;   list     I  = listing of LZ or QKL data files or a bytescaled datacube as
;                 may come from an existing IDL save set.
; 
; OPTIONAL INPUT KEYWORD PARAMETERS: 
;   wave     I  = wavelength
;   outsize  I  = 2 dimensional array for output size of image
;   movie_name I = output movie name (default derived from input data)
;   gif      I  = select if want gif animation
;   mpeg     I  = select if want mpeg animation
;
; OUTPUTS:
;   Creates either a GIF or MPEG file
;
; COMMON BLOCKS: none.
;
; SIDE EFFECTS: temporarily creates GIF files for each image
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
; 	Written by:  J. Newmark 	Date.  September 1997
;-

mpeg = keyword_set(mpeg)
gif = keyword_set(gif)
if not mpeg and not gif then gif = 1

if datatype(list) eq 'STR' then begin  ; input is file listing
   stat = is_fits(list(0))
   if not stat then list = eit_file2path(list)
   read_eit,list,index,data
   index.naxis = 2
   if n_elements(wave) ne 0 then begin
      good = where(index.wavelnth eq wave)
      index = index(good)
      data = temporary(data(*,*,good))
   endif else wave = index(0).wavelnth
   eit_prep,index,data=temporary(data),outindex,outdata,/normalize,/surround
   scl = min(outdata)>1e-4
   data = bytscl(alog10(temporary(outdata)>max([scl,0.1])))
   times = anytim2utc(outindex.date_obs,/ccsds,/truncate,/noz)
   label = 1
   autoname = 0
endif else begin                       ; otherwise input is a byte-scaled data cube
   data = temporary(list)
   times = 0
   label = ''
   autoname = 1
   if n_elements(wave) eq 0 then wave = 3
endelse
if wave gt 42 then eit_colors,wave,r,g,b else begin
   loadct,wave
   tvlct,r,g,b,/get
endelse
;
; adjust colors if on X
if !d.name eq 'X' then begin
   wdisplay,data(*,*,0)
   xmessage,wait=4,'Use XLOADCT to make any necessary color adjustments.'
   xloadct,file=getenv('coloreit'),/modal
   tvlct,r,g,b,/get
endif
data2files,data,r,g,b,filenames=filenames,/gif,times=times,autoname=autoname
if not keyword_set(outsize) then begin
   sz=size(data)
   outsize=[sz(1),sz(2)]
endif
if n_elements(movie_name) eq 0 then begin
    if autoname then movie_name = 'eit_'+ strtrim(wave,2) else $
        movie_name = 'eit_' + strmid(filenames(0),0,9) + strtrim(wave,2)
endif
image2movie,filenames,/loop,gif=gif,mpeg=mpeg,outsize=outsize,/nothumbnail,$
   movie_name=movie_name,label=label,uttimes=times
rm_file,filenames

end
