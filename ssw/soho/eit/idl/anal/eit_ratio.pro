function eit_ratio,file1,file2,show=show,xpixs=xpixs,ypixs=ypixs,$
    magnify=magnify,image1_no=image1_no,image2_no=image2_no,$
    min_ratio=min_ratio,max_ratio=max_ratio,temp_bar=temp_bar, $
    pixmap = pixmap
;+
;   Name: 
;         eit_ratio
;   Purpose: create a ratio of two images and optionally display it
;
;   Input Parameters:
;     file1 - FITS file of image 1 = numerator
;     file2 - FITS file of image 2 = denominator
;
;   Output Parameters:
;      eit_ratio returns ratio array
;      
;   Keyword Parameters:
;    show - set if want output displayed
;    xpixs, ypixs- set if want a subset pixel range of image,
;               format is xpixs=[xstart,xstop] 
;    magnify - set to integer multiple for output REBIN size
;    image[1,2]_no - set for image # of 3D LZ file
;    temp_bar - set to display a temperature bar on bottom of image
;
;   Calling Sequence:
;      eit_ratio,'efr19960924.123456','efr19960924.654321',/show,$
;           magnify=3,xpixs=[100,300],ypixs=[100,300]  
;
;   RESTRICTIONS:
;       the temperature calibration is preliminary and not very
;       accurate.
;
;   Method:
;      read FITS file, load proper color table, subtract background, 
;      replace missing blocks, degrid, flat field, take ratio, subset if 
;      desired, rebin if desired,optionally display
;  
;   History:
;     24-Sep-1996 - J. Newmark  - created
;     08-Dec-1996 - J. Newmark  - took out all processing steps and
;                      directly called routine EIT_PREP
;     1997/02/06   Added missing block replacement (unit ratio) and
;		   MIN_RATIO, MAX_RATIO keywords for display.	D.M. fecit
;     1997/02/10   J. Newmark - added "rough" temperature bar.
;     1997/09/19   D.M. fecit - added PIXMAP keyword
;     1997/10/01   J. Newmark - new call to eit_temp
;-
  eit_prep, file1,h1,a1,/float,/normalize,image_no=image1_no

  eit_prep, file2,h2,a2,/float,/normalize,image_no=image2_no

  sz_a = size(a1) & bx = fltarr(sz_a(1), sz_a(2), 2)
  bx(0, 0, 0) = a1 & bx(0, 0, 1) = a2
  fake_missing_blocks, bx, 0, /after & fake_missing_blocks, bx, 1, /before
  a1 = bx(*, *, 0) & a2 = bx(*, *, 1)

  ratio = a1 / (a2 > 1)
  
  if keyword_set(xpixs) then ratio = ratio(xpixs(0):xpixs(1),*)
  if keyword_set(ypixs) then ratio = ratio(*,ypixs(0):ypixs(1))
  sz = size(ratio)
  if keyword_set(magnify) then ratio = rebin(ratio,magnify*sz(1),$
       magnify*sz(2),/sample)

  if keyword_set(show) then begin
      defsysv,'!image',exist=ok
      if not ok then begin
          imagelib
          devicelib
      endif
      if not keyword_set(pixmap) then pixmap = 0
      sz = size(ratio)
      if !d.name eq 'X' then window, /free, xs = sz(1),ys = sz(2), pix = pixmap
      if keyword_set(min_ratio) then begin
         bottom = min_ratio
      endif else begin
         bottom = min(ratio) > 0
      end
      if keyword_set(max_ratio) then begin
         topval = max_ratio
      endif else begin
         topval = 2*median(ratio)
      end
      b0 = bytscl(((ratio > bottom) < topval), top = !d.n_colors - 1)
      loadct, 3
      tv, b0
      if keyword_set(temp_bar) then begin
        wave1 = eit_fxpar(h1,'wavelnth')
        wave2 = eit_fxpar(h2,'wavelnth')
        case 1 of
          wave1 eq 195 and wave2 eq 171: begin
                 tratio = (indgen(25)+1)*0.1
                 r824 = 0
              end
          wave1 eq 284 and wave2 eq 195: begin
                 tratio = (indgen(25)+1)*0.02
                 r284 = 1
              end
          else: begin
             message,/info, 'Temperature bar only defined for 195/171 or 284/195'
             return,ratio
             end
        endcase
        temp = eit_temp(inratio = tratio,r284=r284,/pre_calc)
        if bottom ne 0 then bottom = eit_temp(inratio = bottom,r284=r284,/pre_calc)
        topval = eit_temp(inratio = topval,r284=r284,/pre_calc)
        temp = temp(where(temp gt bottom(0) and temp lt topval(0)))
        tickv = bytscl(temp, top = !d.n_colors -1 )
        tnames = strmid(strtrim(temp/1.e6,2),0,4)
        title = 'Temperature (x10^6 K)'
        color_bar,b0,sz(1)/1.5,30,60,15,title=title,ticks=n_elements(tickv),$
             tickv=tickv,tickname=tnames,/above
      endif
  endif
  return, ratio
end
