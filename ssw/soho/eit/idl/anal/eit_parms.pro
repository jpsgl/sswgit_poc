function eit_parms,inwaves,band,filter,text,header,units=units
;+
; NAME:
;  eit_parms
; PURPOSE:
;  Generally used as a sub-routine in EIT_FLUX, EIT_TEMP. This
;  routine reads the instrument calibration parameters.
;
; CALLING SEQUENCE:
;   Area = eit_parms(waves,band,filter,text,header,[units=units],
;
; INPUTS:
;   waves:   wavelength array of intest
;   band:    bandpass (e.g. 171,195,284,304)
;   filter:  Filter wheel selection, default = 'CLEAR=clr', 
;            can select 'al1', 'al2'
;
; OUTPUTS:
;   area:    The effective area per wavelength, default units =
;             cm^2 dn photons^-1
;
; OPTIONAL INPUT KEYWORDS:
;
; OPTIONAL OUTPUT KEYWORDS:
;   text:      returns text description of calibration
;   header:    returns file header information
;   units:     returns output units
;
; RESTRICTIONS:
;   The SOHO EIT calibration is presently being upgraded. This file
;   may change. Updates will be posted on EIT homepage.
;
; MODIFICATION HISTORY:
;   1999 May 11, J.S. Newmark, Written.
;   1999 November 12, J.S. Newmark, New Calibration - 
;            Dere et al. Sol.Phys. 2000 
;            Modifications from this include far wings and
;            second order He II in 304 bandpass. These modifications
;            have almost negligible effect on final output.
;
;            Graphically see calibration, Dere = solid, extra = dashed:
;               http://umbra.nascom.nasa.gov/eit/images/calib.ps          
;-
text = ['SOHO EIT Calibration','Data from Dere et al., Solar Physics, 2000.',$
        'Modifications to original data by J. Newmark.','Modifications '+$
        'include far wings and second order He II in 304 bandpass.',$
        'Note these modifications have negligible effect on final output.',$
        'Output units = cm^2 dn photons^-1']

units = 'cm^2 dn photons^-1'

if band(0) le 3 then begin
   band_order = [171,195,284,304]
   band = band_order(band(0))
endif

case 1 of 
  n_elements(filter) eq 0: filter = 'clr'
  strlowcase(filter) eq 'clear': filter = 'clr'
  strlowcase(filter) eq 'al+1': filter = 'al1'
  strlowcase(filter) eq 'al+2': filter ='al2'
  strpos(strlowcase(filter),'blk') ne -1: filter = 'al1'
  else:
endcase
filter = strlowcase(filter)
if filter ne 'clr' and filter ne 'al1' and filter ne 'al2' then begin
     read,filter,prompt='Filter must be either clr, al1, al2: '
endif 

file = 'eit_'+strtrim(band,2)+'_'+filter+'_sens.fits'
file = concat_dir(getenv('SSW_EIT_RESPONSE'),file)
area = readfits(file,header,/silent)
range = minmax(area(*,0))

if n_elements(inwaves) eq 0 then inwaves = findgen(186)+165
area = interpol(area(*,1),area(*,0),inwaves)
bad = where(inwaves lt range(0) or inwaves gt range(1))
if bad(0) ne -1 then area(bad) = 0

return,area 
end
