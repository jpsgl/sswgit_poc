	FUNCTION GET_SC_ATT, DATE, TYPE, ERRMSG=ERRMSG, RETAIN=RETAIN
;+
; Project     :	SOHO - CDS
;
; Name        :	GET_SC_ATT()
;
; Purpose     :	Get the SOHO spacecraft attitude.
;
; Category    :	Class3, Orbit
;
; Explanation :	Read the definitive attitude file to get the spacecraft
;		pointing and roll information.  If no definitive file is found,
;		then the nominal values are returned as "Predictive".
;
; Syntax      :	Result = GET_SC_ATT( DATE  [, TYPE ] )
;
; Examples    :	
;
; Inputs      :	DATE	= The date/time value to get the attitude information
;			  for.  Can be in any CDS time format.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a structure containing the
;		spacecraft attitude information.  It contains the following
;		tags:
;
;			SC_AVG_PITCH_ECLIP
;			SC_AVG_ROLL_ECLIP
;			SC_AVG_YAW_ECLIP
;			SC_AVG_PITCH
;			SC_AVG_ROLL
;			SC_AVG_YAW
;			GCI_AVG_PITCH
;			GCI_AVG_ROLL
;			GCI_AVG_YAW
;			GSE_AVG_PITCH
;			GSE_AVG_ROLL
;			GSE_AVG_YAW
;			GSM_AVG_PITCH
;			GSM_AVG_ROLL
;			GSM_AVG_YAW
;			SC_STD_DEV_PITCH
;			SC_STD_DEV_ROLL
;			SC_STD_DEV_YAW
;			SC_MIN_PITCH
;			SC_MIN_ROLL
;			SC_MIN_YAW
;			SC_MAX_PITCH
;			SC_MAX_ROLL
;			SC_MAX_YAW
;
;		All parameters are in radians.
;
; Opt. Outputs:	TYPE	= Returns whether predictive or definitive data was
;			  used to calculate the result.  Returned as either
;			  "Definitive" or "Predictive".
;
; Keywords    :	RETAIN	= If set, then the FITS attitude file will be left
;			  open.  This speeds up subsequent reads.
;
;		ERRMSG	= If defined and passed, then any error messages will
;			  be returned to the user in this parameter rather than
;			  depending on the MESSAGE routine in IDL.  If no
;			  errors are encountered, then a null string is
;			  returned.  In order to use this feature, ERRMSG must
;			  be defined first, e.g.
;
;				ERRMSG = ''
;				Result = GET_SC_ATT( ERRMSG=ERRMSG, ... )
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR, FXBOPEN, FXBREAD
;
; Common      :	Private common block GET_SC_ATT is used to keep track of the
;		attitude file opened when the RETAIN keyword is used.
;
; Restrictions:	The attitude entries for the time closest to that requested is
;		used to calculate the parameters.  Since the attitude data
;		is calculated every 10 minutes, this should be correct within
;		+/-5 minutes.  No attempt is made to interpolate to closer
;		accuracy than that.
;
; Side effects:	Any data with too much variation (max-min) in the attitude data
;		are rejected.  The limits are one arcminute in pitch and yaw,
;		and one degree in roll.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 04-Dec-1995, William Thompson, GSFC
;		Version 2, 07-Dec-1995, William Thompson, GSFC
;			Returns full attitude file--renamed version 1 to
;			GET_SC_POINT.
;			Added keyword RETAIN
;		Version 3, 12-Dec-1995, William Thompson, GSFC
;			Fixed bug with LAST_FILE not being initialized.
;		Version 4, 24-Jan-1996, William Thompson, GSFC
;			Filter out bad attitude data
;		Version 5, 08-Mar-1996, William Thompson, GSFC
;			Include ecliptic attitude as well as that relative to
;			solar north.
;			Removed keyword OLD.
;		Version 6, 03-Sep-1996, William Thompson, GSFC
;			Also look for filename with a dollar sign
;			prepended--this is how VMS treats uppercase filenames.
;		Version 7, 11-Oct-1996, William Thompson, GSFC
;			Only prepend $ in VMS.
;		Version 8, 22-Jan-1997, William Thompson, GSFC
;			Modified to reflect reorganization of attitude files.
;
; Contact     :	WTHOMPSON
;-
;
	ON_ERROR, 2
	COMMON GET_SC_ATT, LAST_FILE, UNIT, ROWS, TIME
;
;  Make sure that LAST_FILE is defined.
;
	IF N_ELEMENTS(LAST_FILE) EQ 0 THEN LAST_FILE = ''
;
;  Initialize RESULT.  If a definitive attitude file is found, then this will
;  be updated.
;
	RESULT = {SOHO_SC_ATT,			$
		SC_AVG_PITCH_ECLIP:	0.0,	$
		SC_AVG_ROLL_ECLIP:	0.0,	$
		SC_AVG_YAW_ECLIP:	0.0,	$
		SC_AVG_PITCH:		0.0,	$
		SC_AVG_ROLL:		0.0,	$
		SC_AVG_YAW:		0.0,	$
		GCI_AVG_PITCH:		0.0,	$
		GCI_AVG_ROLL:		0.0,	$
		GCI_AVG_YAW:		0.0,	$
		GSE_AVG_PITCH:		0.0,	$
		GSE_AVG_ROLL:		0.0,	$
		GSE_AVG_YAW:		0.0,	$
		GSM_AVG_PITCH:		0.0,	$
		GSM_AVG_ROLL:		0.0,	$
		GSM_AVG_YAW:		0.0,	$
		SC_STD_DEV_PITCH:	0.0,	$
		SC_STD_DEV_ROLL:	0.0,	$
		SC_STD_DEV_YAW:		0.0,	$
		SC_MIN_PITCH:		0.0,	$
		SC_MIN_ROLL:		0.0,	$
		SC_MIN_YAW:		0.0,	$
		SC_MAX_PITCH:		0.0,	$
		SC_MAX_ROLL:		0.0,	$
		SC_MAX_YAW:		0.0}
	TYPE = "Predictive"
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 1 THEN BEGIN
		MESSAGE = 'Syntax:  Result = GET_SC_ATT( DATE  [, TYPE ] )'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make up to two passes through the software.  In the first pass, look in the
;  top level directory.  In the second pass, if needed, try appending the year
;  to the directory.
;
	USE_YEAR = 0
	TEMP = ANYTIM2UTC(DATE,/EXT)
	S_YEAR = TRIM(TEMP.YEAR)
START_PASS:
;
;  Form the filename for the definitive attitude file.
;
	PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
	PATH = CONCAT_DIR(PATH, 'definitive', /DIR)
	IF USE_YEAR THEN PATH = CONCAT_DIR(PATH, S_YEAR, /DIR)
	NAME = 'SO_AT_DEF_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.FITS'
	FILENAME = CONCAT_DIR(PATH, NAME)
;
;  Look for any files that match the search criteria.
;
	FILES = FINDFILE(FILENAME, COUNT=COUNT)
	IF COUNT EQ 0 THEN BEGIN
	    IF OS_FAMILY() EQ 'vms' THEN BEGIN
		FILENAME = CONCAT_DIR(PATH, '$'+NAME)
		FILES = FINDFILE(FILENAME, COUNT=COUNT)
	    ENDIF
	    IF (COUNT EQ 0) AND USE_YEAR THEN GOTO, FINISH ELSE BEGIN
		USE_YEAR = 1
		GOTO, START_PASS
	    ENDELSE
	ENDIF
;
;  A file was found.  Read in the one with the highest version number.
;
	TYPE = "Definitive"
	IF COUNT GT 1 THEN FILES = FILES(REVERSE(SORT(FILES)))
	IF FILES(0) NE LAST_FILE THEN BEGIN
		IF LAST_FILE NE '' THEN FXBCLOSE, UNIT
		FXBOPEN, UNIT, FILES(0), 1
		LAST_FILE = FILES(0)
;
;  Read in the year and the time.  Filter out any entries with a zero year, or
;  with too much variation in the attitude data.  Allow up to one degree
;  variation in the roll, and one arcmin variation in the pitch and yaw.
;
		FXBREAD, UNIT, YEAR, 'YEAR'
		FXBREAD, UNIT, TIME, 'ELLAPSED MILLISECONDS OF DAY'
		FXBREAD, UNIT, PMIN, 'SC MIN PITCH'
		FXBREAD, UNIT, RMIN, 'SC MIN ROLL'
		FXBREAD, UNIT, YMIN, 'SC MIN YAW'
		FXBREAD, UNIT, PMAX, 'SC MAX PITCH'
		FXBREAD, UNIT, RMAX, 'SC MAX ROLL'
		FXBREAD, UNIT, YMAX, 'SC MAX YAW'
;
		ROWS = INDGEN(N_ELEMENTS(YEAR)) + 1
		W = WHERE((YEAR NE 0) AND (!RADEG*(RMAX-RMIN) LT 1) AND	$
			(60*!RADEG*(YMAX-YMIN) LT 1) AND	$
			(60*!RADEG*(PMAX-PMIN) LT 1), COUNT)
		IF COUNT EQ 0 THEN BEGIN
			MESSAGE = 'Empty data file or no good data'
			GOTO, HANDLE_ERROR
		ENDIF
		ROWS = ROWS(W)
		TIME = TIME(W)
	ENDIF
;
;  Find the closest entry to the target time.
;
	TARGET = ANYTIM2UTC(DATE)
	TARGET = TARGET.TIME
	DIFF = ABS(TIME - TARGET)
	MINDIF = MIN(DIFF, W)
	ROW = ROWS(W)
;
;  Read in the spacecraft attitude information.
;
	FXBREAD, UNIT, SC_AVG_PITCH_ECLIP, 'SC AVG PITCH ECLIPTIC (RAD)', ROW
	FXBREAD, UNIT, SC_AVG_ROLL_ECLIP,  'SC AVG ROLL ECLIPTIC(RAD)',	  ROW
	FXBREAD, UNIT, SC_AVG_YAW_ECLIP,   'SC AVG YAW ECLIPTIC(RAD)',	  ROW
	FXBREAD, UNIT, SC_AVG_PITCH,	'SC AVG PITCH (RAD)',	ROW
	FXBREAD, UNIT, SC_AVG_ROLL,	'SC AVG ROLL (RAD)',	ROW
	FXBREAD, UNIT, SC_AVG_YAW,	'SC AVG YAW (RAD)',	ROW
	FXBREAD, UNIT, GCI_AVG_PITCH,	'GCI AVG PITCH',	ROW
	FXBREAD, UNIT, GCI_AVG_ROLL,	'GCI AVG ROLL',		ROW
	FXBREAD, UNIT, GCI_AVG_YAW,	'GCI AVG YAW',		ROW
	FXBREAD, UNIT, GSE_AVG_PITCH,	'GSE AVG PITCH',	ROW
	FXBREAD, UNIT, GSE_AVG_ROLL,	'GSE AVG ROLL',		ROW
	FXBREAD, UNIT, GSE_AVG_YAW,	'GSE AVG YAW',		ROW
	FXBREAD, UNIT, GSM_AVG_PITCH,	'GSM AVG PITCH',	ROW
	FXBREAD, UNIT, GSM_AVG_ROLL,	'GSM AVG ROLL',		ROW
	FXBREAD, UNIT, GSM_AVG_YAW,	'GSM AVG YAW',		ROW
	FXBREAD, UNIT, SC_STD_DEV_PITCH,'SC STD DEV PITCH',	ROW
	FXBREAD, UNIT, SC_STD_DEV_ROLL,	'SC STD DEV ROLL',	ROW
	FXBREAD, UNIT, SC_STD_DEV_YAW,	'SC STD DEV YAW',	ROW
	FXBREAD, UNIT, SC_MIN_PITCH,	'SC MIN PITCH',		ROW
	FXBREAD, UNIT, SC_MIN_ROLL,	'SC MIN ROLL',		ROW
	FXBREAD, UNIT, SC_MIN_YAW,	'SC MIN YAW',		ROW
	FXBREAD, UNIT, SC_MAX_PITCH,	'SC MAX PITCH',		ROW
	FXBREAD, UNIT, SC_MAX_ROLL,	'SC MAX ROLL',		ROW
	FXBREAD, UNIT, SC_MAX_YAW,	'SC MAX YAW',		ROW
;
;  Store the result in the output structure.
;
	RESULT.SC_AVG_PITCH_ECLIP	= SC_AVG_PITCH_ECLIP
	RESULT.SC_AVG_ROLL_ECLIP	= SC_AVG_ROLL_ECLIP
	RESULT.SC_AVG_YAW_ECLIP		= SC_AVG_YAW_ECLIP
	RESULT.SC_AVG_PITCH	= SC_AVG_PITCH
	RESULT.SC_AVG_ROLL	= SC_AVG_ROLL
	RESULT.SC_AVG_YAW	= SC_AVG_YAW
	RESULT.GCI_AVG_PITCH	= GCI_AVG_PITCH
	RESULT.GCI_AVG_ROLL	= GCI_AVG_ROLL
	RESULT.GCI_AVG_YAW	= GCI_AVG_YAW
	RESULT.GSE_AVG_PITCH	= GSE_AVG_PITCH
	RESULT.GSE_AVG_ROLL	= GSE_AVG_ROLL
	RESULT.GSE_AVG_YAW	= GSE_AVG_YAW
	RESULT.GSM_AVG_PITCH	= GSM_AVG_PITCH
	RESULT.GSM_AVG_ROLL	= GSM_AVG_ROLL
	RESULT.GSM_AVG_YAW	= GSM_AVG_YAW
	RESULT.SC_STD_DEV_PITCH	= SC_STD_DEV_PITCH
	RESULT.SC_STD_DEV_ROLL	= SC_STD_DEV_ROLL
	RESULT.SC_STD_DEV_YAW	= SC_STD_DEV_YAW
	RESULT.SC_MIN_PITCH	= SC_MIN_PITCH
	RESULT.SC_MIN_ROLL	= SC_MIN_ROLL
	RESULT.SC_MIN_YAW	= SC_MIN_YAW
	RESULT.SC_MAX_PITCH	= SC_MAX_PITCH
	RESULT.SC_MAX_ROLL	= SC_MAX_ROLL
	RESULT.SC_MAX_YAW	= SC_MAX_YAW
	GOTO, FINISH
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_SC_ATT: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the FITS file and return the result.
;
FINISH:
	IF NOT KEYWORD_SET(RETAIN) THEN BEGIN
		IF FXBISOPEN(UNIT) THEN FXBCLOSE, UNIT
		UNIT = -1
		LAST_FILE = ''
	ENDIF
	RETURN, RESULT
;
	END
