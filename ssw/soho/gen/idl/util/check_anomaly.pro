;+
; Project     : SOHO - CDS
;
; Name        : CHECK_ANOMALY
;
; Purpose     : check directory location of anomaly database file: anomaly.db
;
; Category    : operations
;
; Explanation :
;
; Syntax      : IDL> check_anomaly,file_loc,status=status
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : FILE_LOC = directory location of file
;
; Opt. Outputs: None
;
; Keywords    : STATUS=1/0 for found/not found
;               WRITE = test for write access
;
; Common      : None
;
; Restrictions: environment/logical SOHO_ANOMALY pointing to location
;               of anomaly.db
;
; Side effects: if not defined, sets above environmental to:
;               /soho-archive/private/operations/anomalies
;
; History     : Version 1,  21-Dec-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro check_anomaly,file_loc,status=status,err=err,write=write

status=1
err=''
file_loc=chklog('SOHO_ANOMALY')
if file_loc eq '' then begin
 s=find_with_def('anomaly.dbf','ZDBASE')
 if s ne '' then break_file,s,dsk,def else begin
  message,'Anomaly DB directory not defined',/cont
  def='/soho-archive/private/operations/anomalies'
  message,'--> assuming: '+def,/cont,/noname
 endelse
 mklog,'SOHO_ANOMALY',def
 file_loc=def
endif

;-- check directory location 

check_loc=loc_file(file_loc,count=count)
if count eq 0 then begin
 err='Could not find Anomaly DB directory -- SOHO-ARCHIVE server may be down'
 message,err,/cont
 status=0
 return
endif

;-- check for write access

if keyword_set(write) then begin
 wr_access=test_open(file_loc,/write)
 if wr_access then !priv=3 else begin
  err='Write access to Anomaly DB directory denied'
  message,err,/cont
  status=0
  return
 endelse
endif

defsysv,'!priv',3
return & end

