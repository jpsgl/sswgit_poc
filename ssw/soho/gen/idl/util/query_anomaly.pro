;---------------------------------------------------------------------------
; Document name: query_anomaly.pro
; Created by:    Liyun Wang, GSFC/ARC, January 31, 1996
;
; Last Modified: Thu Jan 23 18:04:08 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO query_anomaly, tstart, tend, file=file, open=open, closed=closed, $
                   id=id, directory=directory
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       QUERY_ANOMALY
;
; PURPOSE:
;       Query Anomaly rrcords and write result to a file
;
; CATEGORY:
;       Anomaly report
;
; SYNTAX:
;       query_anomaly, tstart, tend
;
; INPUTS:
;       TSTART -
;       TEND   -
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       FILE   - Name of output file to which the anomaly contents are written
;       OPEN   - Set this keyword to search for "opened" anomaly reports
;       CLOSED - Set this keyword to search for anomaly reports that
;                have been "closed"
;       ID     - ID of anomaly to be searched
;       DIRECTORY - Directory where FILE is written
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, January 31, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, January 23, 1997, Liyun Wang, NASA/GSFC
;          Added DIRECTORY keyword
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2
   IF N_ELEMENTS(file) EQ 0 THEN file = 'query_anomaly.txt'
   IF N_ELEMENTS(directory) EQ 0 THEN BEGIN
      directory = concat_dir(GETENV('SYNOP_DATA'),'.images/plan_form',/dir)
      IF NOT chk_dir(directory) THEN CD, curr=directory
   ENDIF

   setenv, 'ZDBASE='+getenv('SOHO_ANOMALY')
   ffile = concat_dir(directory, file)
   OPENW, unit, ffile, /GET_LUN
   IF KEYWORD_SET(id) NE 0 THEN BEGIN
;---------------------------------------------------------------------------
;     get a single anomaly entry
;---------------------------------------------------------------------------
      err = ''
      get_anomaly, LONG(id), anomaly, err=err
      IF err NE '' THEN BEGIN
         PRINTF, unit, '0'
         GOTO, finished
      ENDIF
      n_found = 1
   ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;     get a set of anomaly entries
;---------------------------------------------------------------------------
      IF KEYWORD_SET(open) EQ 0 THEN open = 0
      IF KEYWORD_SET(closed) EQ 0 THEN closed = 0
      IF N_ELEMENTS(tstart) EQ 0 THEN BEGIN
         day = anytim2utc(!stime)
         day.time = 0
         tstart = utc2tai(day)
      ENDIF ELSE tstart = utc2tai(tstart)
      IF N_ELEMENTS(tend) EQ 0 THEN tend = tstart+3600.d0*24 ELSE $
         tend = utc2tai(tend)
      IF tend LT tstart THEN BEGIN
         temp = tstart
         tstart = tend
         tend = temp
      ENDIF

      list_anomaly, tstart, tend, anomaly, n_found, open=open, closed=closed

      IF n_found EQ 0 THEN BEGIN
         PRINTF, unit, '0'
         GOTO, finished
      ENDIF
   ENDELSE

   s = STRING(176B)
   FOR i=0, n_found-1 DO BEGIN
      id_str = STRTRIM(STRING(anomaly(i).id),2)
      time = tai2utc(anomaly(i).time,/ecs, /truncate)
      open = STRTRIM(STRING(anomaly(i).open),2)
      long_str = STRCOMPRESS(arr2str([id_str, time, anomaly(i).system, $
                 anomaly(i).origin, anomaly(i).anomaly, open, $
                 anomaly(i).action, anomaly(i).impact, $
                 anomaly(i).resolution], delim=s))
      PRINTF, unit, long_str
   ENDFOR

finished:
   CLOSE, unit
   FREE_LUN, unit
   RETURN
END

;---------------------------------------------------------------------------
; End of 'query_anomaly.pro'.
;---------------------------------------------------------------------------
