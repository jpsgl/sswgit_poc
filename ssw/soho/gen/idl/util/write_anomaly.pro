;---------------------------------------------------------------------------
; Document name: write_anomaly.pro
; Created by:    Liyun Wang, GSFC/ARC, January 19, 1996
;
; Last Modified: Thu Jan 23 18:21:06 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       WRITE_ANOMALY
;
; PURPOSE:
;       Write output of perl program "anomaly_report" to database
;
; CATEGORY:
;       SOC operation.
;
; SYNTAX:
;       write_anomaly [, io_file=io_file]
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       IO_FILE - Name of file for input and output
;       DIRECTORY - Directory where FILE is written       
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       An environment variable, ANOMALY_ERROR, is set if any error
;       occurs so that an appropriate action can be taken
;
; HISTORY:
;       Version 1, January 19, 1996, Liyun Wang, GSFC/ARC. Written
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;

; PRO write_anomaly, time, system, author, anomaly, id=id, action=action,$
;                    impact=impact, resolution=resolution, output=output, $
;                    open=open

PRO write_anomaly, io_file=io_file, directory=directory
   
   COMMON for_write_anomaly, anomaly_stc, aid
   ON_ERROR, 2
   error = ''
   IF N_ELEMENTS(io_file) EQ 0 THEN io_file = 'anomaly.txt'
   IF N_ELEMENTS(directory) EQ 0 THEN BEGIN
      directory = concat_dir(GETENV('SYNOP_DATA'), '.images/plan_form', /dir)
      IF NOT chk_dir(directory) THEN CD, curr=directory
   ENDIF  
   ffile = concat_dir(directory, io_file)
   
   lines = rd_ascii(ffile)
;---------------------------------------------------------------------------
;  Remove the input file
;---------------------------------------------------------------------------
   spawn, 'rm -f '+ffile
   
;---------------------------------------------------------------------------
;  Parse the input contents
;---------------------------------------------------------------------------
   FOR i=0, N_ELEMENTS(lines)-1 DO BEGIN
      j = STRPOS(lines(i),':')
      field = STRLOWCASE(STRMID(lines(i), 0, j))
      value = STRMID(lines(i),j+1, 600)
      CASE (field) OF
         'time': time = STRTRIM(value,2)
         'id': id = LONG(FIX(value))
         'system': system = STRTRIM(value,2)
         'author': author = STRTRIM(value,2)
         'anomaly': anomaly = STRTRIM(value,2)
         'action': action = STRTRIM(value,2)
         'impact': impact = STRTRIM(value,2)
         'resolution': resolution = STRTRIM(value,2)
         'open': open = FIX(value)
         ELSE:
      ENDCASE
   ENDFOR
   
   OPENW, unit, ffile, /GET_LUN

   sav_db = chklog('ZDBASE')
   setenv, 'ZDBASE='+getenv('SOHO_ANOMALY')

;    IF N_PARAMS() NE 4 THEN BEGIN
;       error = 'Sytax error. Need four input parameters.'
;       PRINTF, error
;       GOTO, finish
;    ENDIF

   IF N_ELEMENTS(action) EQ 0 THEN action = ''
   IF N_ELEMENTS(impact) EQ 0 THEN impact = ''
   IF N_ELEMENTS(resolution) EQ 0 THEN resolution = ''
   IF N_ELEMENTS(open) EQ 0 THEN open = 1

   time = utc2tai(anytim2utc(time, err=error))
   IF error NE '' THEN BEGIN
      PRINTF, unit, error
      GOTO, finish
   ENDIF
   
   IF N_ELEMENTS(id) NE 0 THEN aid = LONG(id) ELSE aid = -1L

;   IF N_ELEMENTS(aid) EQ 0 THEN aid = -1L

   new_stc = {id:id, time:time, system:system, origin:author, $
              anomaly:anomaly, action:action, impact:impact, $
              resolution:resolution, open:open}

   add_ok = N_ELEMENTS(anomaly_stc) EQ 0
   IF NOT add_ok THEN add_ok = NOT match_struct(anomaly_stc, new_stc)

   IF add_ok THEN BEGIN
      s = add_anomaly(new_stc, errmsg=error)
      IF error NE '' THEN PRINT, unit, error ELSE BEGIN
         PRINTF, unit, 'Your anomaly report has been successfully '+$
            'written to the database.<p>'
         PRINTF, unit, 'The anomaly ID of your report is '+$
            STRTRIM(STRING(new_stc.id), 2)+'.'
         anomaly_stc = new_stc
;         aid = anomaly_stc.id
      ENDELSE
   ENDIF ELSE BEGIN
      PRINTF, unit, 'Anomaly report #'+STRTRIM(aid, 2)+' already added.'
   ENDELSE

finish:

   CLOSE, unit
   FREE_LUN, unit

   mklog,'ZDBASE', sav_db
   
   RETURN

END

;---------------------------------------------------------------------------
; End of 'write_anomaly.pro'.
;---------------------------------------------------------------------------
