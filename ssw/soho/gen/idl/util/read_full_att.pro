	PRO READ_FULL_ATT, DATE, DATA, HEADER, ERRMSG=ERRMSG,	$
		FILENAME_PASSED=FILENAME_PASSED
;+
; Project     :	SOHO - CDS
;
; Name        :	READ_FULL_ATT
;
; Purpose     :	Reads a SOHO full resolution attitude file.
;
; Category    :	Class3, Coordinates
;
; Explanation :	Reads a SOHO full resolution attitude file.  The user can
;		either request the data by date or by filename.
;
; Syntax      :	READ_FULL_ATT, DATE, DATA  [, HEADER ]
;		READ_FULL_ATT, FILENAME, DATA,  [ HEADER, ]  /FILENAME_PASSED
;
; Examples    :	READ_FULL_ATT, '1996/05/11', DATA, HEADER
;		READ_FULL_ATT, 'SO_AT_FTR_19951202_V04.TXT', DATA, /FILENAME
;
; Inputs      :	DATE	 = Date to read attitude file for.  Can be in any of
;			   the standard CDS time formats.
;
; Opt. Inputs :	FILENAME = Name of file to read.  Can be passed in place of the
;			   DATE parameter if /FILENAME_PASSED is used.
;
; Outputs     :	DATA	 = A structure containing the attitude data, with the
;			   following tags:
;
;				MSEC	= Number of milliseconds into the day
;				ROLL	= Spacecraft roll
;				PITCH	= 
;
; Opt. Outputs:	HEADER	 = The file header
;
; Keywords    :	FILENAME_PASSED = If set, then the filename was passed rather
;				  than the date.
;
;		ERRMSG = If defined and passed, then any error messages will be
;			 returned to the user in this parameter rather than
;			 depending on the MESSAGE routine in IDL.  If no errors
;			 are encountered, then a null string is returned.  In
;			 order to use this feature, ERRMSG must be defined
;			 first, e.g.
;
;				ERRMSG = ''
;				READ_FULL_ATT, ERRMSG=ERRMSG, ... 
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR
;
; Common      :	None.
;
; Restrictions:	The environment variable ANCIL_DATA must be set.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 23-May-1996, William Thompson, GSFC
;		Version 2, 03-Sep-1996, William Thompson, GSFC
;			Also look for filename with a dollar sign
;			prepended--this is how VMS treats uppercase filenames.
;		Version 3, 11-Oct-1996, William Thompson, GSFC
;			Only prepend $ in VMS.
;
; Contact     :	WTHOMPSON
;-
;
	ON_ERROR, 2
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 2 THEN BEGIN
		MESSAGE = 'Syntax:  READ_FULL_ATT, DATE, DATA  [, HEADER ]'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make up to two passes through the software.  In the first pass, look in the
;  top level directory.  In the second pass, if needed, try appending the year
;  to the directory.
;
	USE_YEAR = 0
	IF NOT KEYWORD_SET(FILENAME_PASSED) THEN BEGIN
	    TEMP = ANYTIM2UTC(DATE,/EXT)
	    S_YEAR = TRIM(TEMP.YEAR)
	ENDIF
START_PASS:
;
;  If the date was passed, then form the filename for the definitive attitude
;  file.
;
	IF KEYWORD_SET(FILENAME_PASSED) THEN FILENAME = DATE ELSE BEGIN
	    PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
	    PATH = CONCAT_DIR(PATH, 'full', /DIR)
	    IF USE_YEAR THEN PATH = CONCAT_DIR(PATH, S_YEAR, /DIR)
	    NAME = 'SO_AT_FTR_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.TXT'
	    FILENAME = CONCAT_DIR(PATH, NAME)
;
;  Look for any files that match the search criteria.
;
	    FILES = FINDFILE(FILENAME, COUNT=COUNT)
	    IF COUNT EQ 0 THEN BEGIN
		IF OS_FAMILY() EQ 'vms' THEN BEGIN
		    FILENAME = CONCAT_DIR(PATH, '$'+NAME)
		    FILES = FINDFILE(FILENAME, COUNT=COUNT)
		ENDIF
		IF (COUNT EQ 0) AND USE_YEAR THEN BEGIN
		    MESSAGE = 'Unable to find attitude file for date ' + $
			    ANYTIM2UTC(DATE,/VMS)
		    GOTO, HANDLE_ERROR
		END ELSE BEGIN
		    USE_YEAR = 1
		    GOTO, START_PASS
		ENDELSE
	    ENDIF
;
;  A file was found.  Read in the one with the highest version number.
;
	    IF COUNT GT 1 THEN FILES = FILES(REVERSE(SORT(FILES)))
	    FILENAME = FILES(0)
	ENDELSE
;
;  Open up the file, and read in the header.
;
	MESSAGE = 'Error reading full attitude file'
	ON_IOERROR, HANDLE_ERROR
	OPENR, UNIT, FILENAME, /GET_LUN
	HEADER = LONARR(5)
	READU, UNIT, HEADER
	IEEE_TO_HOST, HEADER
;
;  Read in the data.
;
	STAT = FSTAT(UNIT)
	N_REC = (STAT.SIZE/20) - 1
	DATA = {MSEC: 0L, PITCH: 0.0, YAW: 0.0, ROLL: 0.0, FLAG: 0L}
	DATA = REPLICATE(DATA,N_REC)
	READU, UNIT, DATA
	IEEE_TO_HOST, DATA
	GOTO, FINISH
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'READ_FULL_ATT: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the file and return the result.
;
FINISH:
	IF N_ELEMENTS(UNIT) EQ 1 THEN FREE_LUN, UNIT
	RETURN
	END
