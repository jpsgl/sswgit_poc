	PRO READ_SC_ATT, DATE, DATA, ERRMSG=ERRMSG,	$
		FILENAME_PASSED=FILENAME_PASSED
;+
; Project     :	SOHO - CDS
;
; Name        :	READ_SC_ATT
;
; Purpose     :	Read SOHO spacecraft attitude files.
;
; Category    :	Class3, Orbit, Coordinates
;
; Explanation :	Read the definitive attitude file to get the spacecraft
;		pointing and roll information.
;
; Syntax      :	READ_SC_ATT, DATE, DATA
;		READ_SC_ATT, FILENAME, DATA, /FILENAME_PASSED
;
; Examples    :	READ_SC_ATT, '1996/05/11', DATA
;		READ_SC_ATT, 'SO_AT_DEF_19951202_V01.FITS', DATA, /FILENAME
;
; Inputs      :	DATE	= The date to get the attitude information for.  Can be
;			  in any CDS time format.
;
; Opt. Inputs :	None.
;
; Outputs     :	DATA	= A structure containing the spacecraft attitude
;			  information.  It contains the following tags:
;
;				TIME (milliseconds of day)
;				SC_AVG_PITCH_ECLIP
;				SC_AVG_ROLL_ECLIP
;				SC_AVG_YAW_ECLIP
;				SC_AVG_PITCH
;				SC_AVG_ROLL
;				SC_AVG_YAW
;				GCI_AVG_PITCH
;				GCI_AVG_ROLL
;				GCI_AVG_YAW
;				GSE_AVG_PITCH
;				GSE_AVG_ROLL
;				GSE_AVG_YAW
;				GSM_AVG_PITCH
;				GSM_AVG_ROLL
;				GSM_AVG_YAW
;				SC_STD_DEV_PITCH
;				SC_STD_DEV_ROLL
;				SC_STD_DEV_YAW
;				SC_MIN_PITCH
;				SC_MIN_ROLL
;				SC_MIN_YAW
;				SC_MAX_PITCH
;				SC_MAX_ROLL
;				SC_MAX_YAW
;
;			  All parameters are in radians.
;
; Opt. Outputs:	None.
;
; Keywords    :	FILENAME_PASSED = If set, then the filename was passed rather
;				  than the date.
;
;		ERRMSG	= If defined and passed, then any error messages will
;			  be returned to the user in this parameter rather than
;			  depending on the MESSAGE routine in IDL.  If no
;			  errors are encountered, then a null string is
;			  returned.  In order to use this feature, ERRMSG must
;			  be defined first, e.g.
;
;				ERRMSG = ''
;				READ_SC_ATT, ERRMSG=ERRMSG, ...
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR, FXBOPEN, FXBREAD
;
; Common      :	None.
;
; Restrictions:	The environment variable ANCIL_DATA must be defined.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 23-May-1996, William Thompson, GSFC
;		Version 2, 03-Sep-1996, William Thompson, GSFC
;			Also look for filename with a dollar sign
;			prepended--this is how VMS treats uppercase filenames.
;		Version 3, 11-Oct-1996, William Thompson, GSFC
;			Only prepend $ in VMS.
;		Version 4, 22-Jan-1997, William Thompson, GSFC
;			Modified to reflect reorganization of attitude files.
;
; Contact     :	WTHOMPSON
;-
;
;
	ON_ERROR, 2
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 2 THEN BEGIN
		MESSAGE = 'Syntax:  READ_SC_ATT, DATE, DATA'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make up to two passes through the software.  In the first pass, look in the
;  top level directory.  In the second pass, if needed, try appending the year
;  to the directory.
;
	USE_YEAR = 0
	IF NOT KEYWORD_SET(FILENAME_PASSED) THEN BEGIN
	    TEMP = ANYTIM2UTC(DATE,/EXT)
	    S_YEAR = TRIM(TEMP.YEAR)
	ENDIF
START_PASS:
;
;  If the date was passed, then form the filename for the definitive attitude
;  file.
;
	IF KEYWORD_SET(FILENAME_PASSED) THEN FILENAME = DATE ELSE BEGIN
	    PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
	    PATH = CONCAT_DIR(PATH, 'definitive', /DIR)
	    IF USE_YEAR THEN PATH = CONCAT_DIR(PATH, S_YEAR, /DIR)
	    NAME = 'SO_AT_DEF_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.FITS'
	    FILENAME = CONCAT_DIR(PATH, NAME)
;
;  Look for any files that match the search criteria.
;
	    FILES = FINDFILE(FILENAME, COUNT=COUNT)
	    IF COUNT EQ 0 THEN BEGIN
		IF OS_FAMILY() EQ 'vms' THEN BEGIN
		    FILENAME = CONCAT_DIR(PATH, '$'+NAME)
		    FILES = FINDFILE(FILENAME, COUNT=COUNT)
		ENDIF
		IF (COUNT EQ 0) AND USE_YEAR THEN BEGIN
		    MESSAGE = 'Unable to find attitude file for date ' + $
			    ANYTIM2UTC(DATE,/VMS)
		    GOTO, HANDLE_ERROR
		END ELSE BEGIN
		    USE_YEAR = 1
		    GOTO, START_PASS
		ENDELSE
	    ENDIF
;
;  A file was found.  Read in the one with the highest version number.
;
	    IF COUNT GT 1 THEN FILES = FILES(REVERSE(SORT(FILES)))
	    FILENAME = FILES(0)
	ENDELSE
;
;  Open the FITS binary table.
;
	FXBOPEN, UNIT, FILENAME, 1
;
;  Read in the year and the time.  Filter out any entries with a zero year.
;
	FXBREAD, UNIT, YEAR, 'YEAR'
	FXBREAD, UNIT, TIME, 'ELLAPSED MILLISECONDS OF DAY'
	FXBREAD, UNIT, SC_AVG_PITCH_E,	'SC AVG PITCH ECLIPTIC (RAD)'
	FXBREAD, UNIT, SC_AVG_ROLL_E,	'SC AVG ROLL ECLIPTIC(RAD)'
	FXBREAD, UNIT, SC_AVG_YAW_E,	'SC AVG YAW ECLIPTIC(RAD)'
	FXBREAD, UNIT, SC_AVG_PITCH,	'SC AVG PITCH (RAD)'
	FXBREAD, UNIT, SC_AVG_ROLL,	'SC AVG ROLL (RAD)'
	FXBREAD, UNIT, SC_AVG_YAW,	'SC AVG YAW (RAD)'
	FXBREAD, UNIT, GCI_AVG_PITCH,	'GCI AVG PITCH'
	FXBREAD, UNIT, GCI_AVG_ROLL,	'GCI AVG ROLL'
	FXBREAD, UNIT, GCI_AVG_YAW,	'GCI AVG YAW'
	FXBREAD, UNIT, GSE_AVG_PITCH,	'GSE AVG PITCH'
	FXBREAD, UNIT, GSE_AVG_ROLL,	'GSE AVG ROLL'
	FXBREAD, UNIT, GSE_AVG_YAW,	'GSE AVG YAW'
	FXBREAD, UNIT, GSM_AVG_PITCH,	'GSM AVG PITCH'
	FXBREAD, UNIT, GSM_AVG_ROLL,	'GSM AVG ROLL'
	FXBREAD, UNIT, GSM_AVG_YAW,	'GSM AVG YAW'
	FXBREAD, UNIT, SC_STD_DEV_PITCH,'SC STD DEV PITCH'
	FXBREAD, UNIT, SC_STD_DEV_ROLL,	'SC STD DEV ROLL'
	FXBREAD, UNIT, SC_STD_DEV_YAW,	'SC STD DEV YAW'
	FXBREAD, UNIT, SC_MIN_PITCH,	'SC MIN PITCH'
	FXBREAD, UNIT, SC_MIN_ROLL,	'SC MIN ROLL'
	FXBREAD, UNIT, SC_MIN_YAW,	'SC MIN YAW'
	FXBREAD, UNIT, SC_MAX_PITCH,	'SC MAX PITCH'
	FXBREAD, UNIT, SC_MAX_ROLL,	'SC MAX ROLL'
	FXBREAD, UNIT, SC_MAX_YAW,	'SC MAX YAW'
;
	W = WHERE(YEAR NE 0, COUNT)
;
;  Initialize RESULT.  If a definitive attitude file is found, then this will
;  be updated.
;
	DATA = {TIME:			TIME(W),		$
		SC_AVG_PITCH_E:		SC_AVG_PITCH_E(W),	$
		SC_AVG_ROLL_E:		SC_AVG_ROLL_E(W),	$
		SC_AVG_YAW_E:		SC_AVG_YAW_E(W),	$
		SC_AVG_PITCH:		SC_AVG_PITCH(W),	$
		SC_AVG_ROLL:		SC_AVG_ROLL(W),		$
		SC_AVG_YAW:		SC_AVG_YAW(W),		$
		GCI_AVG_PITCH:		GCI_AVG_PITCH(W),	$
		GCI_AVG_ROLL:		GCI_AVG_ROLL(W),	$
		GCI_AVG_YAW:		GCI_AVG_YAW(W),		$
		GSE_AVG_PITCH:		GSE_AVG_PITCH(W),	$
		GSE_AVG_ROLL:		GSE_AVG_ROLL(W),	$
		GSE_AVG_YAW:		GSE_AVG_YAW(W),		$
		GSM_AVG_PITCH:		GSM_AVG_PITCH(W),	$
		GSM_AVG_ROLL:		GSM_AVG_ROLL(W),	$
		GSM_AVG_YAW:		GSM_AVG_YAW(W),		$
		SC_STD_DEV_PITCH:	SC_STD_DEV_PITCH(W),	$
		SC_STD_DEV_ROLL:	SC_STD_DEV_ROLL(W),	$
		SC_STD_DEV_YAW:		SC_STD_DEV_YAW(W),	$
		SC_MIN_PITCH:		SC_MIN_PITCH(W),	$
		SC_MIN_ROLL:		SC_MIN_ROLL(W),		$
		SC_MIN_YAW:		SC_MIN_YAW(W),		$
		SC_MAX_PITCH:		SC_MAX_PITCH(W),	$
		SC_MAX_ROLL:		SC_MAX_ROLL(W),		$
		SC_MAX_YAW:		SC_MAX_YAW(W)}
	GOTO, FINISH
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'READ_SC_ATT: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the FITS file and return the result.
;
FINISH:
	IF FXBISOPEN(UNIT) THEN FXBCLOSE, UNIT
	RETURN
;
	END
