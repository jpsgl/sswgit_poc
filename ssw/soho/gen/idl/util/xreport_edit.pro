;+
; Project     : SOHO - CDS
;
; Name        : XREPORT_EDIT
;
; Purpose     : widget interface to add/edit entry in anomaly database
;
; Category    : operations,widgets
;
; Explanation : widget interface to add/edit entry in anomaly database
;
; Syntax      : IDL> XREPORT_EDIT,ID
;
; Inputs      : ID = anomaly ID to edit (-1 signals new ID to add)
;
; Opt. Inputs : 
;
; Outputs     : ID = new ID number (if added)
;
; Opt. Outputs: None
;
; Keywords    : GROUP = widget ID of any calling widget
;               MODAL = set to freeze calling widget
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  20-Dec-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro xreport_edit_event,event

child=widget_info(event.top,/child)
widget_control,child, get_uvalue = unseen
info=get_pointer(unseen,/no_copy)
if datatype(info) ne 'STC' then return 

widget_control, event.id, get_uvalue = uservalue
if (n_elements(uservalue) eq 0) then uservalue=''
still_alive=1 

;-- get timer event first

if (uservalue eq 'ut_update') then begin
 get_utc, curr_ut, /ecs
 widget_control,event.top, $
            tlb_set_title = info.ptitle+strmid(curr_ut,0,19)
 widget_control,event.top, timer =info.ut_delay
 goto,time_out
endif

;-- next get pointer values

nx=info.nx 

;-- validate input values

field_vals=['origin','desc','action','impact','res']

chk=where(trim(uservalue) eq field_vals,cnt)
if cnt eq 0 then begin
 last_date='' & last_time=''
 if info.entry.time gt 0 then begin
  last_date=anytim2utc(info.entry.time,/ecs,/vms,/date)
  last_time=strmid(anytim2utc(info.entry.time,/ecs,/time),0,5)
 endif
 widget_control,info.wdate,get_value=dstring
 widget_control,info.wtime,get_value=tstring
 dstring=strtrim(dstring(0),2)
 tstring=strtrim(tstring(0),2)
 if (dstring ne '') or (tstring ne '') then begin
  err=''
  stime=anytim2utc(dstring+' '+tstring,err=err,/ecs)
  if err ne '' then begin
   xack,err,group=event.top,/modal,/icon 
   widget_control,info.wdate,set_value=last_date
   widget_control,info.wtime,set_value=last_time
   goto,time_out
  endif else begin
   stime=strmid(strtrim(stime,2),0,16)
   info.entry.time=utc2tai(stime)
   date=anytim2utc(stime,/ecs,/date,/vms)
   time=strmid(anytim2utc(stime,/ecs,/time),0,5)
   widget_control,info.wdate,set_value=date
   widget_control,info.wtime,set_value=time
  endelse
 endif
endif

;-- button event

bname=strtrim(uservalue,2)
case bname of 

 'print': xprint,array=text_anomaly(info.entry,/all),group=event.top,instruct=''

 'mail' : begin
   xmail,array=text_anomaly(info.entry,/all),group=event.top,status=status
  end

 'write': begin
   file=concat_dir(getenv('HOME'),'xreport_anomaly.dat')
   fwrite=pickfile(group=event.top,/write,file=file,path=getenv('HOME'))
   if strtrim(fwrite,2) ne '' then str2file,text_anomaly(info.entry,/all),fwrite
  end

 'desc': begin
   widget_control,event.id,get_value=in_string
   info.entry.anomaly=str2lines(in_string,/rev)
  end

 'action': begin
   widget_control,event.id,get_value=in_string
   info.entry.action=str2lines(in_string,/rev)
  end

 'impact': begin
   widget_control,event.id,get_value=in_string
   info.entry.impact=str2lines(in_string,/rev)
  end

 'res': begin
   widget_control,event.id,get_value=in_string
   info.entry.resolution=str2lines(in_string,/rev)
  end

 'origin': begin
   widget_control,event.id,get_value=in_string
   info.entry.origin=strmid(strtrim(in_string(0),2),0,20)
  end
 
 'return': begin

;-- check if saved before returning

   quit=1
   xreport_check,info.entry,saved
   desc=strtrim(info.entry.anomaly,2)
   ok_to_save=(info.entry.time gt 0) and (desc ne '') and (not saved)
   if ok_to_save then begin
    save_it=xanswer('Entry not saved. Do you wish to save it?',$
             group=event.top)
    quit=not save_it
   endif 
   if quit then begin
    xtext_reset,info
    xkill,event.top & still_alive=0
   endif
  end

 'dateb': begin
   widget_control,/hour
   xcalendar,last_date,group=event.top,/ut
   new_time=utc2tai(anytim2utc(last_date + ' ' + last_time))
   info.entry.time=new_time
   xreport_fill,info
  end

 'reset': begin
   info=rep_tag_value(info,info.start_entry,'entry')
   xreport_fill,info
  end

 'help' : begin

itext=['To fill out an Anomaly report, enter:',$
       '  ',$
       '- the Date and Time that the anomaly occurred ',$
       '- your Name in the Originator field',$
       '- the System for which the anomaly occurred',$
       '- A short Description of the anomaly.',$
       '- The Actions, Impacts, and Resolution fields are optional',$
       '  and can be entered at a later time',$
       ' ',$
       'Use the CLEAR button to initialize all fields.']
rtext= 'Use the RESTORE button to restore all fields to original settings.'
stext='Use the SAVE button to save the anomaly report'

   def_anomaly,blank_entry
   if not match_struct(info.start_entry,blank_entry) then itext=[itext,rtext]
   itext=[itext,stext]
   xack,itext,group=event.top,instruct='DISMISS'
  end

 'clear': begin
   def_anomaly,entry
   info=rep_tag_value(info,entry,'entry')
   xreport_fill,info
  end

 'inst': info.entry.system=info.inst(event.index)

 'save': begin
   xreport_check,info.entry,saved
   if saved then begin
    xack,'Identical entry already saved',group=event.top
   endif else begin
    entry=info.entry
    if event.id eq info.addb then entry.id=-1
    xtext,'Please wait, saving...',wbase=wbase,/just_reg
    widget_control,/hour
    err='' & s=add_anomaly(entry,err=err)
    xkill,wbase
    if s eq 0 then begin
     xack,err,group=event.top
    endif else begin
     info.entry=entry
     info=rep_tag_value(info,entry,'start_entry')
    endelse
   endelse
  end

  else: do_nothing=1

endcase

if still_alive then xreport_butt,info

time_out: set_pointer,unseen,info,/no_copy

return & end

;----------------------------------------------------------------

pro xreport_check,entry,saved,notime=notime ;-- check if anomaly entry is saved

saved=0 & err=''
get_anomaly,entry.id,temp,err=err

widget_control,/hour

if err eq '' then begin
 if keyword_set(notime) then begin
  t1=rem_tag(entry,'time')
  t2=rem_tag(temp,'time')
 endif else begin
  t1=entry
  t2=temp
 endelse
 saved=match_struct(t1,t2)
endif

return & end
 
;----------------------------------------------------------------

pro xreport_fill,info

;-- time widgets

stime=info.entry.time
if stime gt 0 then begin
 widget_control,info.wdate,set_value=anytim2utc(stime,/ecs,/date,/vms)
 widget_control,info.wtime,set_value=strmid(anytim2utc(stime,/ecs,/time),0,5)
endif else begin
 widget_control,info.wdate,set_value='  '
 widget_control,info.wtime,set_value='  '
endelse

;-- text widgets

widget_control,info.wid,set_value=strtrim(string(info.entry.id),2)
clook=where(strtrim(info.entry.system,2) eq strtrim(info.inst,2),count)
if count gt 0 then ival=clook(0) else ival=0
if widget_info(info.insb,/type) eq 8 then $
 widget_control,info.insb,set_droplist_select=ival else $
  widget_control,info.insb,set_value=ival

nx=info.nx
a=str2lines(info.entry.anomaly,len=nx)
widget_control,info.worig,set_value=info.entry.origin
widget_control,info.wdesc,set_value=a
widget_control,info.wact,set_value=str2lines(info.entry.action,len=nx)
widget_control,info.wimp,set_value=str2lines(info.entry.impact,len=nx)
widget_control,info.wres,set_value=str2lines(info.entry.resolution,len=nx)


stats=['Closed','Open']
widget_control,info.wstat,set_value=stats(info.entry.open)

return & end

;----------------------------------------------------------------

pro xreport_butt,info

def_anomaly,blank_entry
same=match_struct(info.start_entry,info.entry)
blank=match_struct(blank_entry,info.entry)
widget_control,info.printb,sensitive=1-blank
widget_control,info.mailb,sensitive=1-blank
widget_control,info.writeb,sensitive=1-blank
widget_control,info.repb,sensitive=(info.entry.id gt 0) and (1-same)
if xalive(info.resetb) then widget_control,info.resetb,sensitive=1-same

if info.start_entry.id eq -1 then $
 rtext='Replace Previous Anomaly With New Entry' else $
  rtext='Replace Anomaly ID '+strtrim(string(info.start_entry.id),2)+' With New Entry'

widget_control,info.repb,set_value=rtext
return & end

;----------------------------------------------------------------

pro xreport_edit,id,group=group,modal=modal,err=err

on_error,1

;-- defaults

if not have_widgets() then begin
 message,'widgets unavailable',/cont
 return
endif

if not exist(id) then id=-1
if id eq -1 then def_anomaly,entry else begin
 err=''
 get_anomaly,id,entry,err=err
 if err ne '' then return
endelse

start_entry=entry
mk_dfont,bfont=bfont

wbase=widget_base(/column)


;-- operation buttons

row1=widget_base(wbase,/row,/frame)
fileb=widget_button(row1,value='FILE',font=bfont,/no_rel,/menu)
mailb=widget_button(fileb,value='Mail Anomaly',uvalue='mail',font=bfont,/no_rel)
printb=widget_button(fileb,value='Print Anomaly',uvalue='print',font=bfont,/no_rel)
writeb=widget_button(fileb,value='Write Anomaly To File',uvalue='write',font=bfont,/no_rel)
returnb=widget_button(fileb,value='Return',uvalue='return',font=bfont,/no_rel)
helpb=widget_button(row1,value='HELP',uvalue='help',font=bfont,/no_rel)
clearb=widget_button(row1,value='CLEAR FIELDS',uvalue='clear',font=bfont,/no_rel)


def_anomaly,blank_entry
if not match_struct(entry,blank_entry) then begin
 resetb=widget_button(row1,value='RESTORE PREVIOUS FIELDS',uvalue='reset',font=bfont,/no_rel)
 widget_control,resetb,sensitive=0
endif else resetb=0
t1=widget_base(row1,/row)

saveb=widget_button(t1,value='PRESS TO SAVE TO DB',font=bfont,/no_rel,/menu)
addb=widget_button(saveb,value='Save As New Anomaly',uvalue='save',font=bfont,/no_rel)
repb=widget_button(saveb,uvalue='save',font=bfont,/no_rel)

check_anomaly,f,status=write_access,/write
widget_control,saveb,sensitive=write_access
widget_control,clearb,sensitive=write_access

;svtext=widget_text(t1,value='      ',font=bfont,xsize=10)

;-- date/time field

temp=widget_base(wbase,/row)
t1=widget_base(temp,/row)
dlab=widget_label(t1,value='Anomaly ',font=bfont)
dateb=widget_button(t1,value='Date:',uvalue='dateb',font=bfont,/no_rel,/frame)
wdate=widget_text(t1,value='',uvalue='date',xsize=12,font=bfont,/edit)
t2=widget_base(temp,/row)
time=widget_label(t2,value='Time:',font=bfont)
wtime=widget_text(t2,value='',uvalue='time',xsize=6,font=bfont,/edit)
ut=widget_label(t2,value='UT',font=bfont)

;-- text widgets

nx=80 & ny=4

worig=cw_field(wbase, Title= 'Originator:     ',value='',/all,$
                   uvalue='origin', xsize = 20, font = bfont,field=bfont)

;-- instrument choices

inst=strpad(['NONE  ','ECS','FOT',get_soho_inst(/sort)],10,/after)
new_vers=float(strmid(!version.release,0,3)) ge 4.
if new_vers then begin
 insb=call_function('widget_droplist',wbase,value=trim(inst),$
  uvalue='inst',font=bfont,title='System:         ')
endif else $
 insb = cw_bselector2(wbase, inst,/return_name, uvalue='inst',$
                    /return_index,/no_rel,font=bfont,label_left='System:         ')

wid=cw_field(wbase, Title=   'Anomaly ID:     ',value='',$
                      xsize = 10, font = bfont,/noedit,field=bfont)

wstat=cw_field(wbase, Title= 'Anomaly Status: ',value='',$
                      xsize = 10, font = bfont,/noedit,field=bfont)

buff=' '
temp=widget_base(wbase,/row,/frame)
t1=widget_label(temp,value='Anomaly Description:',font=bfont)
wdesc=widget_text(wbase,xsize=nx,value=buff,ysize=ny,uvalue='desc',/edit,$
                  /all,/scroll,font=bfont)

temp=widget_base(wbase,/row,/frame)
t1=widget_label(temp,value='Action Taken:',font=bfont)
wact=widget_text(wbase,xsize=nx,value=buff,ysize=ny,uvalue='action',$
                 /edit,/scroll,/all,font=bfont)

temp=widget_base(wbase,/row,/frame)
t1=widget_label(temp,value='Impacts:',font=bfont)
wimp=widget_text(wbase,xsize=nx,value=buff,ysize=ny,uvalue='impact',$
                 font=bfont,/edit,/scroll,/all)

temp=widget_base(wbase,/row,/frame)
t1=widget_label(temp,value='Anomaly Resolution:',font=bfont)
wres=widget_text(wbase,xsize=nx,value=buff,ysize=ny,uvalue='res',$
                 font=bfont,/edit,/scroll,/all)
 
;-- realize and center main base

xrealize,wbase,group=group,/screen

;-- set up timer

ut_delay=1.d
title='SOHO ANOMALY REPORT FORM '
widget_control,wbase,set_uvalue='ut_update'
if float(strmid(!version.release,0,3)) le 3.5 then begin
 ptitle=title
 widget_control,wbase,tlb_set_title=ptitle
endif else begin
 get_utc, curr_ut, /ecs
 ptitle = title+'    Current UT: '
 widget_control,wbase, tlb_set_title = ptitle+strmid(curr_ut,0,19)
 widget_control,wbase, timer =ut_delay
endelse

;-- stuff info structure into pointer and store in child of main base

make_pointer,unseen
info={saveb:saveb,worig:worig,insb:insb,inst:inst,wdate:wdate,wid:wid,$
      wtime:wtime,wdesc:wdesc,wact:wact,wimp:wimp,wres:wres,start_entry:start_entry,entry:start_entry,$
      wstat:wstat,svtext:1,resetb:resetb,nx:nx,addb:addb,repb:repb,$
      ut_delay:ut_delay,ptitle:ptitle,printb:printb,mailb:mailb,writeb:writeb}

xreport_fill,info
xreport_butt,info

set_pointer,unseen,info,/no_copy
child=widget_info(wbase,/child)
widget_control,child,set_uvalue=unseen

xmanager,'xreport_edit',wbase,group=group,/modal
if xalive(wbase) then xmanager

info=get_pointer(unseen,/no_copy)
free_pointer,unseen
if datatype(info) eq 'STC' then begin
 entry=info.entry
 id=entry.id
endif

xshow,group
 
return & end


