;+
; Project     : SOHO - CDS
;
; Name        : XREPORT
;
; Purpose     : widget interface to anomaly database
;
; Category    : operations, widgets
;
; Explanation :
;
; Syntax      : IDL> XREPORT
;
; Inputs      : None
;
; Opt. Inputs : TSTART, TEND = start and end times to view (any format)
;
; Outputs     : 
;
; Opt. Outputs: None
;
; Keywords    : GROUP = widget ID of any calling widget
;               MODAL = set to freeze calling widget
;
; Common      : XREPORT,CSTART,CEND -- last report times
;
; Restrictions: None.
;
; Side effects: None.
;
; History     : Version 1,  21-Dec-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

 pro xreport_event,  event                         ;event driver routine

 on_error,1

;-- extract values passed as UVALUES in bases

 widget_control, event.top, get_uvalue = unseen
 info=get_pointer(unseen,/no_copy)
 if datatype(info) ne 'STC' then return 

 widget_control, event.id, get_uvalue = uservalue
 wtype=widget_info(event.id,/type)
 if (n_elements(uservalue) eq 0) then uservalue=''
 bname=strtrim(uservalue,2)

 quit=0

 new_vers=float(strmid(!version.release,0,3)) ge 4.
 new_vers=0
 if new_vers then begin
  catch,err
  if err ne 0 then begin
   message,!err_string,/cont
   quit=1 & goto,quit
  endif
 endif

;-- quit here

 if bname(0) eq 'exit' then begin
  quit=1 & goto,quit
 endif

;-- validate input times in TEXT widgets

 good=xvalidate(info,event,trim=17,time_change=time_change)
 if good then begin
  if (event.id eq info.wops1) or (event.id eq info.wops2) then begin
   if time_change then xreport_list,info
  endif
 endif

;-- list event

 if event.id eq info.alist then info.sel_id=fix(bname(event.index))

;-- button events

;-- check if instrument button pressed

 blook=where(event.id eq info.bids,bcount)
 if bcount gt 0 then begin
  info.bstate(blook(0))=event.select
  ok=where(info.bstate eq 1,icount)
  if icount gt 0 then begin
   sinst=info.sinst
   info=rep_tag_value(info,sinst(ok),'inst')
  endif
 endif

;-- check if status button pressed

 blook=where(bname(0) eq ['Open','Closed','Both'],bcount)
 if bcount gt 0 then begin
  if event.select eq 1 then begin
   info.stsearch=bname(0)
   xreport_list,info
  endif
 endif

 case bname(0) of 

  'print': xreport_list,info,/print
   
  'mail': xreport_list,info,/mail

  'write': xreport_list,info,/write
 
  'add': begin
    stat=test_open(concat_dir(getenv('SOHO_ANOMALY'),'anomaly.db'),/write,/quiet)
    if not stat then begin
     xack,'Privilege to update Anomaly DB is denied',group=event.top
    endif else begin
     id=-1
     xreport_edit,id,group=event.top
     info.sel_id=id
     xreport_list,info
    endelse
   end

  'edit': begin
    id=info.sel_id
    xreport_edit,id,group=event.top
    info.sel_id=id
    xreport_list,info
   end

  'remove': begin
    dmess=['You are about to permanently delete a record.',$
           'Are you sure?']
    delete=xanswer(dmess,group=event.top)
    if delete then begin
     err='' 
     ok=del_anomaly(info.sel_id,err=err)
     if not ok then begin
      xack,err,group=event.top
     endif else begin
      info.sel_id=-1
      xreport_list,info
     endelse
    endif
   end

  'relist': xreport_list,info
 
  'all_on': begin
    nbids=n_elements(info.bids)
    for i=0,nbids-1 do widget_control,info.bids(i),/set_butt
    info.inst='' & info.bstate=replicate(1,nbids)
    info.sel_id=-1
    xreport_list,info
   end

   'all_off': begin
    nbids=n_elements(info.bids)
    for i=0,nbids-1 do widget_control,info.bids(i),set_button=0
    info.bstate=replicate(0,nbids)
    info.inst='-1'
    info.sel_id=-1
    xreport_list,info
   end

  else:do_nothing=1
 endcase

;-- update living widgets

 quit:
 widget_control,info.editb,sensitive=(info.sel_id gt -1)
 widget_control,info.remb,sensitive=(info.sel_id gt -1) and info.write_access
 if quit then begin
  xtext_reset,info
  xkill,event.top
 endif
 set_pointer,unseen,info,/no_copy
 return & end

;--------------------------------------------------------------------------- 

pro xreport_list,info,print=print,mail=mail,write=write

tstart=strmid(tai2utc(info.ops1,/ecs,/vms),0,17)
tstop=strmid(tai2utc(info.ops2,/ecs,/vms),0,17)

widget_control,info.wops1,set_value=tstart
widget_control,info.wops2,set_value=tstop

open=(info.stsearch eq 'Open') or (info.stsearch eq 'Both')
closed=(info.stsearch eq 'Closed') or (info.stsearch eq 'Both')

lines='No Entries Found'

widget_control,/hour
list_anomaly,info.ops1,info.ops2,sdata,nfound,open=open,closed=closed,$
             system=info.inst

widget_control,info.printb,sensitive=(nfound gt 0)
widget_control,info.writeb,sensitive=(nfound gt 0)
widget_control,info.mailb,sensitive=(nfound gt 0)
print_it=keyword_set(print)
mail_it=keyword_set(mail)
write_it=keyword_set(write)

if nfound gt 0 then begin

 if print_it or mail_it or write_it then begin
  marker='-------------------------------------------------------------'
  plist=['SOHO ANOMALY LISTING FOR PERIOD: '+tstart+' TO '+tstop,'']
  plist=[plist,'Printed on '+!stime,marker,'']
  for i=0,nfound-1 do plist=[plist,text_anomaly(sdata(i),/all),marker]
 endif else begin
  lines=strmid(tai2utc(sdata.time,/ecs,/vms),0,17)+$
         ' | '+strpad(strtrim(string(sdata.id),2),3)+$
         ' | '+strpad(sdata.system,10,/aft)+ $
         ' | '+strmid(strtrim(sdata.anomaly,2),0,80)+'...' 
  widget_control,info.alist,set_uvalue=strtrim(string(sdata.id),2)
 endelse
endif

if nfound gt 0 then begin
 if print_it then begin
  xprint,array=plist,group=info.wbase,instruct=''
  return
 endif
 if mail_it then begin
  xmail,array=plist,group=info.wbase,status=status
  return
 endif
 if write_it then begin
  file=concat_dir(chklog('HOME'),'xreport_lis.dat')
  fwrite=pickfile(group=info.wbase,path=chklog('HOME'),/write,file=file)
  if strtrim(fwrite) ne '' then str2file,plist,fwrite
  return
 endif
endif

widget_control,info.alist,set_value=lines
widget_control,info.alist,sensitive=(nfound gt 0)

if (nfound gt 0) then begin
 clook=where(info.sel_id eq sdata.id,count)
 if count eq 1 then widget_control,info.alist,set_list_select=clook(0)
endif

return & end

;--------------------------------------------------------------------------- 

pro xreport_clean,id

dprint,'% XREPORT: cleaning up...'

widget_control,id,get_uvalue=unseen
info=get_pointer(unseen,/no_copy)
if datatype(info) eq 'STC' then begin
 if info.write_access then s=prg_anomaly()
 mklog,'ZDBASE',info.orig_db
 xtext_reset,info
 set_pointer,unseen,info,/no_copy
endif
return & end

;--------------------------------------------------------------------------- 

pro xreport,tstart,tend,group=group,modal=modal,last=last

common xreport_com,cstart,cend,orig_db

on_error,1

;-- defaults

set_plot,'x'
if not have_widgets() then begin 
 message,'widgets unavailable',/cont
 return
endif

;-- kill any dead apps

caller=get_caller(status)
if status then xkill,/all

;-- go to anomaly database

check_anomaly,db_loc,status=status
if not status then return
if not exist(orig_db) then orig_db=get_environ('ZDBASE')
mklog,'ZDBASE',db_loc

secs_per_day=24l*3600l
week=7l*secs_per_day
last=keyword_set(last)

get_utc,ops1 & ops1.time=0
ops1=(utc2tai(ops1)-week) > utc2tai(anytim2utc('2-dec-95')) 
get_utc,ops2 & ops2.time=0
ops2.mjd=ops2.mjd+1
ops2=utc2tai(ops2)
dtime=[ops1,ops2]
ctime=dtime
if exist(cstart) then ctime(0)=anytim2tai(cstart)
if exist(cend) then ctime(1)=anytim2tai(cend)

times=pick_times(tstart,tend,ctime=ctime,dtime=dtime,last=last)
ops1=times(0) & ops2=times(1)

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

wbase=widget_base(title='XREPORT',/column)

row1=widget_base(wbase,row=1,/frame)

;-- operation buttons

fileb=widget_button(row1,value='FILE',font=bfont,/no_rel,/menu)
mailb=widget_button(fileb,value='Mail Current List',uvalue='mail',font=bfont,/no_rel)
printb=widget_button(fileb,value='Print Current List',uvalue='print',font=bfont,/no_rel)
writeb=widget_button(fileb,value='Write Current List To File',uvalue='write',font=bfont,/no_rel)
exitb=widget_button(fileb,value='Exit',uvalue='exit',font=bfont,/no_rel)

newb=widget_button(row1,value='ADD NEW ENTRY',uvalue='add',font=bfont,/no_rel)
editb=widget_button(row1,value='VIEW/EDIT SELECTED ENTRY',uvalue='edit',font=bfont,/no_rel)
remb=widget_button(row1,value='REMOVE SELECTED ENTRY',uvalue='remove',font=bfont,/no_rel)
check_anomaly,f,status=write_access,/write
widget_control,newb,sensitive=write_access
widget_control,remb,sensitive=0
widget_control,editb,sensitive=0

;-- date fields

row1=widget_base(wbase,row=1)

temp=widget_base(row1,/column,/frame)

rowt=widget_base(temp,/row)
tlabel=widget_label(rowt,font=lfont,$
       value='Edit Start/Stop Time fields to list Anomalies for different periods:')


trow=widget_base(temp,/row)
wops1=cw_field(trow, Title= 'Start Time:  ',value='',$
                    /ret, xsize = 18, font = bfont,field=bfont)

wops2=cw_field(trow,  Title='Stop Time:   ', value='',$
                    /ret, xsize = 18, font = bfont,field=bfont)

rowt=widget_base(temp,/row)
slabel=widget_label(rowt,font=lfont,value='Time Units:                  DD-MM-YR HH:MM')

stsearch='Open'
choices=['Open','Closed','Both']
xmenu,choices,row1,font=bfont,title='Search on Status:',$
 /exclusive,/row,uvalue=choices,buttons=sbutt
widget_control,sbutt(0),/set_bu

;-- instrument choices (initially all ON)


ititle='SOHO SYSTEMS'
sinst=['ECS','FOT',get_soho_inst(/sort)]
xmenu,sinst,wbase,font=bfont,row=1,title=ititle,$
 /nonexclusive,buttons=bids,frame=0
nbids=n_elements(bids)
bstate=intarr(nbids)+1
for i=0,nbids-1 do widget_control,bids(i),/set_button

temp=widget_base(wbase,/row)
allb=widget_button(temp,font=bfont,/no_rel,value='All Systems ON',uvalue='all_on')
allb=widget_button(temp,font=bfont,/no_rel,value='All Systems OFF',uvalue='all_off')
relist=widget_button(temp,value='RELIST ANOMALIES',uvalue='relist',font=bfont,/no_rel)

;-- anomaly list

mlabel=strpad('DATE/TIME',17,/aft) + $
         ' | '+strpad('ID',3)+$
         ' | '+strpad('SYSTEM',10,/aft)+ $
         ' | '+strpad('ANOMALY',80,/aft)
alabel=widget_list(wbase,value=mlabel,font=lfont,ysize=1,/frame)

status=1
alist=widget_list(wbase,value=' ',font=lfont,ysize=25)

;-- realize and center main base

xrealize,wbase,group=group,/center 

;-- stuff info structure into pointer

make_pointer,unseen

info ={wops1:wops1,wops2:wops2,ops1:ops1,ops2:ops2,remb:remb,editb:editb,alist:alist,sel_id:-1,$
       sinst:sinst,inst:'',bids:bids,bstate:bstate,$
       status:status,trow:trow,stsearch:stsearch,printb:printb,writeb:writeb,$
       mailb:mailb,wbase:wbase,orig_db:orig_db,write_access:write_access}

xreport_list,info

set_pointer,unseen,info,/no_copy
widget_control,wbase,set_uvalue=unseen

xmanager,'xreport',wbase,group=group,/modal,cleanup='xreport_clean'
if xalive(wbase) then xmanager

info=get_pointer(unseen,/no_copy)
free_pointer,unseen

;-- save last times

if datatype(info) eq 'STC' then begin
 cstart=info.ops1
 cend=info.ops2
endif

xshow,group


return & end

