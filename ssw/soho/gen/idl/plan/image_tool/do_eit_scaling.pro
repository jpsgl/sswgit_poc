;+
; Project     : SOHO-CDS
;
; Name        : DO_EIT_SCALING
;
; Purpose     : scale EIT images (degrid, take log, etc) 
;
; Category    : planning
;
; Explanation : 
;
; Syntax      : data=do_eit_scaling(data,header,index=index)
;
; Examples    :
;
; Inputs      : DATA = data array
;               HEADER = FITS header (if not entered check INDEX)
;
; Opt. Inputs : 
;
; Outputs     : 
;
; Opt. Outputs: DATA = scaled EIT data array
;
; Keywords    : INDEX = index structure (used if HEADER not entered)
;               NO_COPY = input data is destroyed to conserve memory
;               LOG = log10 scale image
;               NORMALIZE = normalize images by exposure time
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 1 June 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function do_eit_scaling,data,header=header,index=index,no_copy=no_copy,$
                        log=log,normalize=normalize

common do_eit_scaling,have_eit_prep

on_error,1
err=''
no_copy=keyword_set(no_copy)

;-- check inputs

if not exist(data) then begin
 err='Invalid input data'
 pr_syntax,'data=do_eit_scaling(data,header=header,[index=index])
 return,-1
endif

header_input=datatype(header) eq 'STR'
index_input=datatype(index) eq 'STC'

;-- check which EIT degridder is available. Ideally use EIT_PREP, but
;   resort to ITOOL_EIT_DEGRID if unavailable

if not exist(have_eit_prep) then begin
 which,/all,'eit_prep',out=out
 have_eit_prep=trim(out(0)) ne ''
endif

use_eit_prep=have_eit_prep
if no_copy then image=copy_var(data) else image=data

;-- if using EIT_PREP and HEADER was input instead of INDEX, then have
;   to convert it

if use_eit_prep then begin
 dprint,'% DO_EIT_SCALING: calling EIT_PREP'
 if (1-index_input) and (header_input) then begin
  eitstr = call_function('eit_struct',ncomment=20)
stop
  index = fitshead2struct(header,eitstr) 
 endif
 call_procedure,'eit_prep',index,data=image,ni,image,normalize=normalize,/adjust
endif else begin
 if (1-header_input) then begin
  err='EIT header was not entered'
  message,err,/cont
  return,-1
 endif
 dark=848
 image=temporary(image)-dark
 image=temporary(image) > 0
 image=itool_eit_degrid(temporary(image),header)
endelse

if keyword_set(log) then begin
 image=temporary(image) > 1.
 image=alog10(temporary(image))
endif

return,image & end

