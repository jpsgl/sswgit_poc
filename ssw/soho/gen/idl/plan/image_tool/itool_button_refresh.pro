;---------------------------------------------------------------------------
; Document name: itool_button_refresh.pro
; Created by:    Liyun Wang, NASA/GSFC, January 29, 1997
;
; Last Modified: Wed Sep 24 15:51:33 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO itool_button_refresh
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       ITOOL_BUTTON_REFRESH
;
; PURPOSE: 
;       Make sure all buttons appear properly
;
; CATEGORY:
;       image tool
; 
; SYNTAX: 
;       itool_button_refresh
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       None.
;
; COMMON:
;       @image_tool_com
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, January 29, 1997, Liyun Wang, NASA/GSFC. Written
;          Extracted from image_tool.pro
;       Version 2, March 20, 1997, Liyun Wang, NASA/GSFC
;          Modified to allow quiting Image_tool when Pointing Tool is running
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
@image_tool_com
   ON_ERROR, 2
   can_send = 0
   IF N_ELEMENTS(pointing_stc) NE 0 THEN BEGIN
      can_send = WIDGET_INFO(pointing_stc.messenger, /valid)
   ENDIF

;   WIDGET_CONTROL, opt_bs, sensitive=(limbfit_flag EQ 0)

   pt_ok = cando_pointing AND (limbfit_flag EQ 0) AND csi.flag
;   WIDGET_CONTROL, pointing_button, sensitive = cando_pointing AND $
;      (limbfit_flag EQ 0) AND csi.flag
;   WIDGET_CONTROL, ptool, sensitive=(pt_ok)
   WIDGET_CONTROL, ptool_send, sensitive=(pt_ok AND can_send)
;   WIDGET_CONTROL, fit_bs, sensitive=(limbfit_flag EQ 0)

;   WIDGET_CONTROL, img_quit, sensitive=(limbfit_flag EQ 0)

   WIDGET_CONTROL, img_sel_bt, sensitive=binary_fits EQ 1

   WIDGET_CONTROL, rot_mode(1), set_button=(rot_dir EQ -1)
   WIDGET_CONTROL, rot_mode(0), set_button=(rot_dir EQ 1)

;   WIDGET_CONTROL, reset_img, sensitive = 0

   WIDGET_CONTROL, fits_header, sensitive=(gif_file NE 1)
   WIDGET_CONTROL, modify_fh, sensitive=csi.flag
   WIDGET_CONTROL, write_fits, sensitive=(csi.flag EQ 1)

;   WIDGET_CONTROL, load_img_bt, sensitive=(limbfit_flag EQ 0)
   WIDGET_CONTROL, button_base, sensitive=(limbfit_flag EQ 0)
   WIDGET_CONTROL, zoom_bt, sensitive=(limbfit_flag EQ 0 AND can_zoom EQ 1)
   WIDGET_CONTROL, rot_reg_bt, sensitive=(limbfit_flag EQ 0 AND can_zoom EQ 1)

   IF N_ELEMENTS(sc_view) NE 0 THEN $
      WIDGET_CONTROL, sc_view, sensitive=csi.flag, set_button=(scview EQ 1)
   WIDGET_CONTROL, rot_bs, sensitive=csi.flag
   WIDGET_CONTROL, mode3_bt, sensitive=csi.flag
   WIDGET_CONTROL, mode4_bt, sensitive=csi.flag
   WIDGET_CONTROL, show_csi, sensitive=csi.flag
   WIDGET_CONTROL, grid_bt, set_button=grid AND csi.flag
   WIDGET_CONTROL, grid_base, sensitive=csi.flag
;   WIDGET_CONTROL, save_img, sensitive=!d.window EQ root_win

   WIDGET_CONTROL, cursor_size, sensitive=boxed_cursor
   IF show_src THEN $
      WIDGET_CONTROL, src_text, set_value=src_name $
   ELSE $
      WIDGET_CONTROL, src_text, set_value=img_type
   WIDGET_CONTROL, obs_text, set_value=anytim2utc(disp_utc, /ecs, /trunc)

   WIDGET_CONTROL, log_scale, sensitive=(log_scaled EQ 0)

   IF boxed_cursor THEN $
      WIDGET_CONTROL, cursor_shape, set_value='Use Cross-hair Cursor' $
   ELSE $
      WIDGET_CONTROL, cursor_shape, set_value='Use Boxed Cursor'

   IF track_cursor THEN BEGIN
      WIDGET_CONTROL, cursor_track, set_value='Manual Tracking'
   ENDIF ELSE BEGIN
      WIDGET_CONTROL, txt_id, set_value=''
      WIDGET_CONTROL, cursor_track, set_value='Auto Tracking'
   ENDELSE

   IF rot_dir EQ 1 THEN $
      WIDGET_CONTROL, rot_limb, set_value='points on the east limb' $
   ELSE $
      WIDGET_CONTROL, rot_limb, set_value='points on the west limb'

   IF zoom_in THEN BEGIN
      WIDGET_CONTROL, zoom_bt, set_value='Zoom Out '
   ENDIF ELSE BEGIN
      WIDGET_CONTROL, zoom_bt, set_value=' Zoom In '
   ENDELSE

   WIDGET_CONTROL, rot_img90, sensitive=(img_lock EQ 0)
   WIDGET_CONTROL, rot_img45, sensitive=(img_lock EQ 0)
   WIDGET_CONTROL, rot_img45n, sensitive=(img_lock EQ 0)
   IF img_lock THEN BEGIN
      WIDGET_CONTROL, lock_bt, set_value='Unlock Orientation'
   ENDIF ELSE BEGIN
      WIDGET_CONTROL, lock_bt, set_value='Lock Orientation'
   ENDELSE
   WIDGET_CONTROL, lock_bt, sensitive=csi.flag

   CASE (d_mode) OF
      1: WIDGET_CONTROL, txt_lb, set_value='(in device coordinate system)'
      2: WIDGET_CONTROL, txt_lb, set_value='(in image pixel coordinate system)'
      3: WIDGET_CONTROL, txt_lb, set_value='(in solar disc coordinate system)'
      4: WIDGET_CONTROL, txt_lb, set_value='(in heliographic coordinate system)'
      ELSE:
   ENDCASE
   
END

;---------------------------------------------------------------------------
; End of 'itool_button_refresh.pro'.
;---------------------------------------------------------------------------
