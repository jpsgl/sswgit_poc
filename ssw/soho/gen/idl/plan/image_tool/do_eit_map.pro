;+
; Project     : SOHO-CDS
;
; Name        : DO_EIT_MAP
;
; Purpose     : make EIT maps 
;
; Category    : planning
;
; Explanation : wrapper around mk_eit_map that works within SSW and CDS-SOFT
;
; Syntax      : emap=do_eit_map(data,header,index=index)
;
; Examples    :
;
; Inputs      : DATA = data array
;               HEADER = FITS header (if not entered check INDEX)
; Opt. Inputs : 
;
; Outputs     : DATA = scale EIT image
;
; Opt. Outputs: 
;
; Keywords    : INDEX = index structure (used if HEADER not entered)
;               OUTSIZE = output image size
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 1 June 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function do_eit_map,data,header=header,index=index,outsize=outsize

on_error,1
err=''

;-- check inputs

header_input=datatype(header) eq 'STR'
index_input=datatype(index) eq 'STC'

if (not exist(data)) or ((1-header_input) and (1-index_input)) then begin
 err='Invalid input data'
 pr_syntax,'data=do_eit_map(data,header=header,[index=index])
 return,-1
endif

if index_input then return,mk_eit_map(index,data,outsize=outsize) else $
 return,mk_eit_map(data,header,outsize=outsize)

end

