;---------------------------------------------------------------------------
; Document name: itool_img_src.pro
; Created by:    Liyun Wang, GSFC/ARC, March 12, 1996
;
; Last Modified: Fri Sep 26 09:28:01 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       SOHO
;
; NAME:
;       ITOOL_IMG_SRC()
;
; PURPOSE: 
;       Return a string of an appropriate image source
;
; CATEGORY:
;       image_tool, utility
; 
; EXPLANATION:
;       
; SYNTAX: 
;       Result = itool_img_src(str [, header=header])
;
; INPUTS:
;       STR - String scalar indicating an abbreviated name for image source
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       RESULT - Full name of image source
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       HEADER - If present, must be a string array from the FITS header
;
; COMMON:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, March 12, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, July 30, 1996, Liyun Wang, NASA/GSFC
;          Added Kiepenheuer Institute and Pic du Midi Observatory
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
FUNCTION itool_isrc, str
;---------------------------------------------------------------------------
;   Convert an abbreviated image source string to its full-name string
;---------------------------------------------------------------------------
   CASE (STRUPCASE(str)) OF
      'KBOU': RETURN, 'Space Environment Lab'
      'KHMN': RETURN, 'Holloman AFB'
      'HTPR': RETURN, 'Haute-Provence'
      'LEAR': RETURN, 'Learmonth Observatory'
      'MITK': RETURN, 'Mitaka, Japan'
      'MLSO': RETURN, 'Mauna Loa Solar Obs. at HAO'
      'NOBE': RETURN, 'Nobeyama Radio Observatory'
      'PDMO': RETURN, 'Pic du Midi Observatory'
      'MEUD': RETURN, 'Obs. of Paris at Meudon'
      'ONDR': RETURN, 'Ondrejov'
      'KSAC': RETURN, 'Nat. Solar Obs. at Sac. Peak'
      'BBSO': RETURN, 'Big Bear Solar Observatory'
      'KPNO': RETURN, 'Nat. Solar Obs. at Kitt Peak'
      'MEES': RETURN, 'Mees Solar Observatory'
      'MWNO': RETURN, 'Mt. Wilson Observatory'
      'YOHK': RETURN, 'Yohkoh Soft-X Telescope'
      'KANZ': RETURN, 'Kanzelhohe Solar Observatory'
      'KISF': RETURN, 'Kiepenheuer Institute'
      'SCDS': RETURN, 'SOHO CDS'
      'SSUM': RETURN, 'SOHO SUMER'
      'SEIT': RETURN, 'SOHO EIT'
      'SLAS': RETURN, 'SOHO LASCO'
      'SUVC': RETURN, 'SOHO UVCS'
      'SMDI': RETURN, 'SOHO MDI'
      ELSE: RETURN, ''
   ENDCASE
END

FUNCTION itool_img_src, str, header=header
   ON_ERROR, 2
   IF datatype(str) NE 'STR' THEN isrc = '' ELSE isrc = itool_isrc(str)
   IF isrc NE '' THEN RETURN, isrc
   isrc = 'Unspecified'
   IF N_ELEMENTS(header) EQ 0 THEN BEGIN
      MESSAGE, 'Unknown image source.', /cont
      RETURN, isrc
   ENDIF
   type_list = ['ORIGIN','TELESCOP']
   FOR i=0, N_ELEMENTS(type_list)-1 DO BEGIN
      tmp = STRUPCASE(STRTRIM(STRING(fxpar(header, type_list(i))), 2))
      IF !err NE -1 THEN BEGIN
         CASE (tmp) OF
            'KPNO-IRAF': RETURN, itool_isrc('kpno')
            'KPNO': RETURN, itool_isrc('kpno')
            'KBOU': RETURN, itool_isrc('kbou')
            'KHMN': RETURN, itool_isrc('khmn')
            'LEAR': RETURN, itool_isrc('lear')
            'KSAC': RETURN, itool_isrc('ksac')
            'BBSO': RETURN, itool_isrc('bbso')
            'MLSO': RETURN, itool_isrc('mlso')
            'MWNO': RETURN, itool_isrc('mwno')
            'MT. WILSON': RETURN, itool_isrc('mwno')
            'KIS 15 cm Coude': RETURN, itool_isrc('kisf')
            'NOBEYAMA RADIO OBS': RETURN, itool_isrc('nobe')
            'PIC DU MIDI OBSERVATORY': RETURN, itool_isrc('pdmo')
            'Meudon Observatory': RETURN, itool_isrc('meud')
            'SOHO': BEGIN 
               tmp = fxpar(header, 'INSTRUME')
               IF !err NE -1 THEN RETURN, 'SOHO '+tmp ELSE RETURN, 'SOHO'
            END
            ELSE:
         ENDCASE
      ENDIF
   ENDFOR
   MESSAGE, 'Unknow image source.', /cont
   RETURN, isrc
END

;---------------------------------------------------------------------------
; End of 'itool_img_src.pro'.
;---------------------------------------------------------------------------
