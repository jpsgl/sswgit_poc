;----------------------------------------------------------------------
; Document name: cnvt_coord.pro
; Created by:    Liyun Wang, GSFC/ARC, November 16, 1994
;
; Last Modified: Wed Aug 28 09:20:35 1996 (lwang@achilles.nascom.nasa.gov)
;----------------------------------------------------------------------
;
FUNCTION CNVT_COORD, bb, csi=csi, from=from, to=to, date=date, $
                     off_limb=off_limb
;+
; PROJECT:
;       SOHO
;
; NAME:
;       CNVT_COORD()
;
; PURPOSE:
;       Conversion between any 2 of 4 coord systems for solar images
;
; EXPLANATION:
;       For image displaying, especially for the solar images, there
;       can be at least four coordinate systems involved, and they are:
;
;           1. Device coordinate system, in device pixels
;           2. Data (image) coordinate system, in data pixels
;           3. Solar disk coordinate system, in arc seconds
;           4. Heliographic coordinate system, in degrees
;
;       This routine can do the conversion between any two of
;       them. For the solar disk system (with origin at the disk
;       center), the positive directions of the X and Y axes point to
;       the west limb and north limb, respectively.
;
; CALLING SEQUENCE:
;       Result = CNVT_COORD(bb, csi=csi, from=from, to=to)
;
; INPUTS:
;       BB  -- 2-element vector or 2xN array containing coordinates of the
;              point to be converted. Depending on the coordinate system,
;              the values can be:
;           
;              [x, y] in device pixels for device system (1)
;              [x, y] in data pixels for data (image) system (2)
;              [x, y] in arc seconds for solar disk system (3)
;              [lat, long] in degrees for heliographic system (4)
;
;       CSI -- Coordinate system information structure that contains some
;              basic information of the coordinate systems involved. It should
;              have the following 14 tags:
;
;              XD0 -- X position of the first pixel of the
;                     image (lower left corner), in device pixels
;              YD0 -- Y position of the first pixel of the
;                     image (lower left corner), in device pixels
;              XU0 -- X position of the first pixel of the image (lower 
;                     left corner), in user (or data) pixels. 
;              YU0 -- Y position of the first pixel of the image (lower 
;                     left corner), in user (or data) pixels
;              MX  -- X size of the image in device pixels
;              MY  -- Y size of the image in device pixels
;              RX  -- ratio of SX/MX, (data unit)/(device pixel), 
;                     where SX is the image size in X direction in data pixels
;              RY  -- ratio of SY/MY, (data unit)/(device pixel), 
;                     where SY is the image size in Y direction in data pixels
;              X0  -- X position of the reference point in data pixels
;              Y0  -- Y position of the reference point in data pixels
;              XV0 -- X value of the reference point in absolute units
;              YV0 -- Y value of the reference point in absolute units
;              SRX -- scaling factor for X direction in arcsec/(data pixel)
;              SRY -- scaling factor for Y direction in arcsec/(data pixel)
;
;              Note: Units used for XV0 and YV0 are arc seconds in
;                    case of solar images. If the reference point is
;                    the solar disk center, XV0 = YV0 = 0.0. The
;                    reference point can also be the first pixel of
;                    the image (i.e., the pixel on the lower-left
;                    corner of the image).
;                    When the whole image is displayed, XU0 and YU0 are all
;                    equal to 0; for subimages, XU0 and YU0 may not be zero.
;
;       FROM -- Code for the original coordinate system (see explanation)
;       TO   -- Code for the new coordinate system to be converted
;               to. Possible values are the same as those of FROM
;       DATE -- date in CCSDS or ECS time format; required for conversion
;               between heliographic coordinate system to or from
;               other system.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       RESULT -- A 2-element vector, containing new coordinates;
;                 refer to the input description of BB for returned
;                 values for different coordinate systems. A scalar
;                 value of -1 will be returned for a non-valid
;                 conversion or wrong type of data.
;
; OPTIONAL OUTPUTS:
;       OFF_LIMB -- Flag which is true if the coordinates are beyond
;                   the solar limb. Only used for converting to the
;                   heliographic system
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       ARCMIN2HEL, HEL2ARCMIN
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;
; HISTORY:
;       Version 1, November 16, 1994, Liyun Wang, GSFC/ARC, Written.
;       Version 2, August 28, 1996, Liyun Wang, NASA/GSFC
;          Used ROUND (instead of FIX) to get coordinates for device
;             and data systems
;
; VERSION:
;       Version 2, August 28, 1996
;-
   ON_ERROR, 1
   IF N_ELEMENTS(from) EQ 0 OR N_ELEMENTS(to) EQ 0 THEN $
      MESSAGE, 'Syntax: cnvt_coord, aa,bb, from=from, to=to'
   IF from EQ to THEN BEGIN
      MESSAGE, 'No need to make a conversion.', /cont
      RETURN, -1
   ENDIF
   code = from*10+to
   sz = SIZE(bb)
   IF sz(0) NE 1 AND sz(1) NE 2 THEN BEGIN
      MESSAGE, 'Input parameter must be a 2-element vector or 2xN matrix.', $
         /cont
      RETURN, -1
   ENDIF
   IF sz(1) NE 2 THEN BEGIN
      MESSAGE, 'Input parameter must be a 2-element vector or 2xN matrix.', $
         /cont
      RETURN, -1
   ENDIF
   IF sz(0) EQ 2 THEN n_pts = sz(2) ELSE n_pts = 1
   IF datatype(csi) NE 'STC' THEN BEGIN
      MESSAGE, 'CSI must be a coordinate system info structure.', /cont
      RETURN, -1
   ENDIF
   off_limb = INTARR(n_pts)
   aa = FLOAT(bb)
   CASE (code) OF
      12: BEGIN
         ix = ROUND(csi.rx*(aa(0, *)-csi.xd0)+csi.xu0)
         iy = ROUND(csi.ry*(aa(1, *)-csi.yd0)+csi.yu0)
         RETURN, [ix, iy]
      END
      13: BEGIN
         aa(0, *) = csi.rx*(aa(0, *)-csi.xd0)+csi.xu0
         aa(1, *) = csi.ry*(aa(1, *)-csi.yd0)+csi.yu0
         ix = FLOAT(csi.srx*(aa(0, *)-csi.x0)+csi.xv0)
         iy = FLOAT(csi.sry*(aa(1, *)-csi.y0)+csi.yv0)
         RETURN, [ix, iy]
      END
      14: BEGIN
         aa(0, *) = csi.rx*(aa(0, *)-csi.xd0)+csi.xu0
         aa(1, *) = csi.ry*(aa(1, *)-csi.yd0)+csi.yu0
         ix = FLOAT(csi.srx*(aa(0, *)-csi.x0)+csi.xv0)/60.0
         iy = FLOAT(csi.sry*(aa(1, *)-csi.y0)+csi.yv0)/60.0
         temp = arcmin2hel(ix, iy, date=date, off_limb=off_limb)
         RETURN, temp
      END
      21: BEGIN
         ix = (csi.xd0+(aa(0, *)-csi.xu0)/csi.rx) > 0
         iy = (csi.yd0+(aa(1, *)-csi.yu0)/csi.ry) > 0
         RETURN, [ROUND(ix), ROUND(iy)]
      END
      23: BEGIN
         ix = FLOAT(csi.srx*(aa(0, *)-csi.x0)+csi.xv0)
         iy = FLOAT(csi.sry*(aa(1, *)-csi.y0)+csi.yv0)
         RETURN, [ix, iy]
      END
      24: BEGIN
         ix = FLOAT(csi.srx*(aa(0, *)-csi.x0)+csi.xv0)/60.0
         iy = FLOAT(csi.sry*(aa(1, *)-csi.y0)+csi.yv0)/60.0
         temp = arcmin2hel(ix, iy, date=date, off_limb=off_limb)
         RETURN, temp
      END
      32: BEGIN
         ix = csi.x0+FLOAT((aa(0, *)-csi.xv0)/csi.srx)
         iy = csi.y0+FLOAT((aa(1, *)-csi.yv0)/csi.sry)
         RETURN, [ROUND(ix), ROUND(iy)]
      END
      31: BEGIN
         aa(0, *) = csi.x0+FLOAT((aa(0, *)-csi.xv0)/csi.srx)
         aa(1, *) = csi.y0+FLOAT((aa(1, *)-csi.yv0)/csi.sry)
         ix = (csi.xd0+(aa(0, *)-csi.xu0)/csi.rx) > 0
         iy = (csi.yd0+(aa(1, *)-csi.yu0)/csi.ry) > 0
         RETURN, [ROUND(ix), ROUND(iy)]
      END
      34: BEGIN
         temp = arcmin2hel(aa(0, *)/60.0, aa(1, *)/60.0, date=date, $
                           off_limb=off_limb)
         RETURN, temp
      END
      42: BEGIN
         temp = 60.0*hel2arcmin(aa(0, *), aa(1, *), date=date)
         ix = csi.x0+FLOAT((temp(0, *)-csi.xv0)/csi.srx)
         iy = csi.y0+FLOAT((temp(1, *)-csi.yv0)/csi.sry)
         RETURN, [ROUND(ix), ROUND(iy)]
      END
      43: BEGIN
         temp = 60.0*hel2arcmin(aa(0, *), aa(1, *), date=date)
         RETURN, temp
      END
      41: BEGIN
         aa = 60.0*hel2arcmin(aa(0, *), aa(1, *), date=date)
         aa(0, *) = csi.x0+FLOAT((aa(0, *)-csi.xv0)/csi.srx)
         aa(1, *) = csi.y0+FLOAT((aa(1, *)-csi.yv0)/csi.sry)
         ix = (csi.xd0+(aa(0, *)-csi.xu0)/csi.rx) > 0
         iy = (csi.yd0+(aa(1, *)-csi.yu0)/csi.ry) > 0
         RETURN, [ROUND(ix), ROUND(iy)]
      END
      ELSE: RETURN, -1
   ENDCASE
END

;----------------------------------------------------------------------
;    Testing part
;----------------------------------------------------------------------
; PRO aaa, code, bb, out=out
;    sx = 512 & sy = 512 & mx = 512 & my = 512
;    jx = 27 & jy = 26 & kx = 0 & ky = 0
;    x0 = 257 &  y0 = 257
;    xv0 = 0.0 & yv0 = 0.0
;    rx = float(sx)/float(mx) &  ry = float(sy)/float(my)
;    srx = 4.91 & sry = srx
;    date = '1993/02/06 02:51:44.035'
;    angles = pb0r(date)
;    rx0 = 60.*angles(2)/srx &  ry0 = 60.*angles(2)/sry
;    from = FIX(code/10)
;    to = code-from*10
;    cc = {jx:jx, jy:jy, kx:kx, ky:ky, rx:rx, ry:ry, mx:mx, my:my, $
;          x0:x0, y0:y0, xv0:xv0, yv0:yv0, srx:srx, sry:sry}
;    out = cnvt_coord(bb, csi=cc, from=from, to=to, date=date)
;    PRINT, out
; END
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of 'cnvt_coord.pro'.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
