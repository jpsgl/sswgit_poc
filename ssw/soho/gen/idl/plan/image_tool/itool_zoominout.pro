;---------------------------------------------------------------------------
; Document name: itool_zoominout.pro
; Created by:    Liyun Wang, NASA/GSFC, January 29, 1997
;
; Last Modified: Wed Jun 11 15:37:16 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO itool_zoominout, event
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       ITOOL_ZOOMINOUT
;
; PURPOSE: 
;       Event handler for zooming in/out
;
; CATEGORY:
;       image tool
; 
; SYNTAX: 
;       itool_zoominout, event
;
; INPUTS:
;       EVENT - Event structure
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       None.
;
; COMMON:
;       @image_tool_com
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, January 29, 1997, Liyun Wang, NASA/GSFC. Written
;          Extracted from image_tool.pro
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
@image_tool_com
   ON_ERROR, 1
   IF zoom_in THEN BEGIN
;---------------------------------------------------------------------------
;     Zoom out
;---------------------------------------------------------------------------
      zoom_in = 0
;---------------------------------------------------------------------------
;     Just in case csi.srx, and csi.sry are changed, update that
;---------------------------------------------------------------------------
      IF (csi.srx NE csi_sv.srx) OR (csi.sry NE csi_sv.sry) THEN BEGIN
         csi_sv.x0 = csi.x0
         csi_sv.y0 = csi.y0
         csi_sv.srx = csi.srx
         csi_sv.sry = csi.sry
      ENDIF
      csi = csi_sv
      image_arr = img_sv
      cur_min = min_sv
      cur_max = max_sv
;      WIDGET_CONTROL, draw_icon, draw_button=1
      WIDGET_CONTROL, comment_id, set_value=''
      itool_refresh
   ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;     Zoom in
;---------------------------------------------------------------------------
      zoom_in = 1
      delvarx, initial
      csi_sv = csi
      img_sv = image_arr
      min_sv = cur_min
      max_sv = cur_max
      IF !d.window NE root_win THEN setwindow, root_win
      WIDGET_CONTROL, rot_bs, sensitive=0
      WIDGET_CONTROL, grid_base, sensitive=0
      WIDGET_CONTROL, draw_id, draw_button=0
;      WIDGET_CONTROL, draw_icon, draw_button=0
      xack, ['Caution: You are about to enter a special mode which requires', $
             '   your attention. Instructions on how to operate in this mode', $
             '   will be shown in the message window. ', '', $
             '   To get out of this mode, you need to (perhaps repeatedly)', $
             '   press the *right* mouse button.'], $
         group=event.top, /modal, instru='Proceed', /suppress
      WIDGET_CONTROL, comment_id, $
         set_value='Please press and drag the left button to move '+ $
         'the box, middle to resize, and right to select.'
;----------------------------------------------------------------------
;     xll, yll, xur, yur are coordinates of lower left and upper
;     right corners of the subimage position in the original
;     image array
;----------------------------------------------------------------------
      image_tmp = BYTSCL(image_arr, MIN=cur_min, MAX=cur_max)
      image_arr = tvsubimage(image_tmp, xll, xur, yll, yur)
      cur_min = MIN(image_arr)
      cur_max = MAX(image_arr)
      WIDGET_CONTROL, comment_id, set_value=$
         'To zoom out, please press the "Zoom Out" button under "Zoom".'
      itool_display, image_arr, min=cur_min, max=cur_max, csi=csi, $
         relative=exptv_rel
      p_ref = cnvt_coord([xll+1, yll+1], from=2, to=3, csi=csi, date=date)
      temp = {xu0:xll+1, yu0:yll+1, x0:xll+1, y0:yll+1, $
              xv0:p_ref(0), yv0:p_ref(1)}
      copy_struct, temp, csi
      itool_disp_plus, /keep
      WIDGET_CONTROL, draw_id, draw_button=1
   ENDELSE
END

;---------------------------------------------------------------------------
; End of 'itool_zoominout.pro'.
;---------------------------------------------------------------------------
