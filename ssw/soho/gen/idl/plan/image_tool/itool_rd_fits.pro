;+
; PROJECT:
;       SOHO - CDS
;
; NAME:
;       ITOOL_RD_FITS
;
; PURPOSE:
;       Driver program of FXREAD and CDS_IMAGE to read any FITS file
;
; EXPLANATION:
;
; CALLING SEQUENCE:
;       itool_rd_fits, data_file, image, header, min=min, max=max, $
;                      image_max=image_max, image_min=image_min $
;                      [,errmsg=errmsg]
;
; INPUTS:
;       DATA_FILE - name of FITS file
;
; OPTIONAL INPUTS:
;       COLUMN - Column number of data in FITS binary table. This parameter
;                has no effect on "plain" FITS files, and will cause CDS_IMAGE
;                to be called directly with its value if passed.
;       GROUP  - Group ID of the group leader.
;
; OUTPUTS:
;       IMAGE  - A 2-D image array
;       HEADER - Header of the image
;
; OPTIONAL OUTPUTS:
;       DATA_INFO - A structure that indicates name and column number of all
;                   images contained in one FITS file. This is generally for
;                   FITS files with a binary table. It show have the following
;                   tags:
;
;                      BINARY  - Integer scalar with value 1/0 indicating if
;                                the data_file contains binary table or not
;                      COL     - Integer vector that indicates all column
;                                numbers for the data
;                      LABEL   - String vector showing the label of data in
;                                each column
;                      CUR_COL - Current column of data being read
;
;       CSI     - Part of CSI structure. It's important to get this structure
;
; KEYWORD PARAMETERS:
;       IMAGE_MIN - Minimum value of the image array
;       IMAGE_MAX - Maximum value of the image array
;       ERRMSG    - Error message returned (null string if no error)
;       STATUS    - Flag (0/1) of failure/success
;
; CALLS:
;       XSEL_ITEM, FXREAD, CDS_IMAGE, FXPAR, FXHREAD, BELL
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;
; PREVIOUS HISTORY:
;       Extracted from IMAGE_TOOL.PRO, February 27, 1995, Liyun Wang, NASA/GSFC
;
; MODIFICATION HISTORY:
;       Version 1, created, Liyun Wang, NASA/GSFC, February 27, 1995
;       Version 2, Liyun Wang, NASA/GSFC, May 11, 1995
;          Rotated loaded image if keyword CROTA or CROTA1 is found in the
;             header and its value is not zero
;       Version 3, April 1, 1996, Liyun Wang, NASA/GSFC
;          Modified so that point of view is set based on the loaded image
;       Version 4, July 30, 1996, Liyun Wang, NASA/GSFC
;          Fixed a bug occurred when dealing with compressed FITS files
;       Version 5, August 13, 1997, Liyun Wang, NASA/GSFC
;          Took out IMG_UTC keyword (now in CSI.DATE_OBS)
;          Uncompress compressed file to /tmp directory 
;       Version 6, April 28, 1998, Zarro, SAC/GSFC
;          Added patch for getting updated EIT pointing information
;
; VERSION:
;       Version 6
;-
;-------------------------------------------------------------------------

pro itool_read_eit,d_file,csi  ;-- patch for updating with correct EIT header

common eit_path,have_eit_reader

if (datatype(d_file) ne 'STR') or datatype(csi) ne 'STC' then return

if not exist(have_eit_reader) then $
 have_eit_reader=find_with_def('read_eit.pro',!path) ne ''

if tag_exist(csi,'ORIGIN') then begin
 org=strupcase(csi.origin)
 is_eit=strpos(org,'EIT')
 if is_eit and have_eit_reader then begin
  message,'applying EIT FITS header patch...',/cont
  call_procedure,'read_eit',d_file,index,/nodata
  if datatype(index) eq 'STC' then begin
   copy_struct,index,csi
   csi.origin=org
  endif
 endif
endif
return & end

;-------------------------------------------------------------------------

PRO itool_rd_fits, data_file, image, header, group=group, data_info=data_info,$
                   image_max=image_max, image_min=image_min, errmsg=errmsg, $
                   column=column, csi=csi, status=status

   ON_ERROR, 2
   IF N_PARAMS() LT 3 THEN BEGIN
      MESSAGE,'Usage: itool_rd_fits, file, image, header [,keyword=]',/cont
      RETURN
   ENDIF
   errmsg = ''
   status = 1

;----------------------------------------------------------------------
;  Adding checks to see if the file is compressed. Uncompress such files
;  to /tmp directory (write privilege required)
;----------------------------------------------------------------------
   sep_filename, data_file, dsk, direc, file, ext
   n_ext = N_ELEMENTS(ext)
   IF n_ext GE 2 THEN BEGIN
      IF (ext(n_ext-1) EQ "Z" OR ext(n_ext-1) EQ "gz") THEN BEGIN
         MESSAGE, 'Uncompressing '+data_file+'...', /info
         d_file = concat_dir('/tmp',arr2str([file,ext(0:n_ext-2)],'.'))
         SPAWN,'/usr/local/bin/gunzip '+data_file+' -c > '+d_file
         recomp = 1
      ENDIF ELSE d_file = data_file
   ENDIF ELSE d_file = data_file

;---------------------------------------------------------------------------
;  Open the file and read the header.  If the number of axes is non-zero, then
;  the data is assumed to be in the main part of the FITS file.  Otherwise, the
;  data will be in a FITS binary table.
;---------------------------------------------------------------------------
   OPENR, unit, d_file, /GET_LUN, /BLOCK
   fxhread, unit, header
   FREE_LUN, unit

;---------------------------------------------------------------------------
;  Determine point of view; Earth view assumed first
;---------------------------------------------------------------------------
   use_earth_view
   telescope = fxpar(header, 'TELESCOP')
   IF !err NE -1 THEN IF trim(telescope) EQ 'SOHO' THEN use_soho_view

   naxis = fxpar(header,'NAXIS')
   csi = itool_set_csi(header, err=errmsg, file=d_file)

;-- apply special patch for EIT to get updated CRPIX, CDELT values

   itool_read_eit,d_file,csi

   IF naxis EQ 2 THEN BEGIN
      fxread, d_file, image, header, errmsg = errmsg
      IF errmsg NE '' THEN BEGIN
         bell
         popup_msg, 'FXREAD: '+errmsg, title = 'FXREAD ERROR'
         status = 0
         RETURN
      ENDIF

      IF csi.crota NE 0.0  THEN BEGIN
;---------------------------------------------------------------------------
;        Rotate the image to make solor north pole straight up or down
;---------------------------------------------------------------------------
         MESSAGE, 'Rotating image to make solar poles straight up/down...',$
            /cont
         crpix1 = fxpar(header, 'CRPIX1')
         crpix2 = fxpar(header, 'CRPIX2')
         IF NOT num_chk(crpix1) AND NOT num_chk(crpix2) THEN BEGIN
            image = rot(temporary(image), -csi.crota, 1, crpix1, crpix2, /pivot, $
                        /cubic, missing=0)
         ENDIF ELSE BEGIN
            image = rot(temporary(image), -csi.crota, /cubic, missing=0)
         ENDELSE
         csi.crota = 0.0
      ENDIF
      
      IF csi.reflect EQ 1 THEN BEGIN
;---------------------------------------------------------------------------
;        The image is upside down. Flip it
;---------------------------------------------------------------------------
         MESSAGE, 'Image upside down. Flipping it...', /cont
         image = reverse(ROTATE(TEMPORARY(image), 2))
         csi.crpix2 = csi.naxis2-1-csi.crpix2
         csi.reflect = 0
      ENDIF

      data_info = {binary:0, label:'', col:1, cur_col:1}
   ENDIF ELSE BEGIN
      IF naxis EQ 0 THEN BEGIN
         IF fxpar(header,'EXTEND') EQ 1 THEN BEGIN
;---------------------------------------------------------------------------
;           File contains extension. Assume that the binary table is
;           the first extension in the file
;---------------------------------------------------------------------------
            fxbopen, unit, d_file, 1, extheader
            fxbfind, extheader, 'TDETX', data_col, values, ndata
            IF ndata LT 1 THEN MESSAGE, 'No detector data found'
            fxbfind, extheader, 'TTYPE', col, ttype, ncol,''
            FOR i = 0, ndata-1 DO BEGIN
               IF N_ELEMENTS(image_list) EQ 0 THEN $
                  image_list = ttype(i) $
               ELSE $
                  image_list = [image_list, ttype(i)]
            ENDFOR
            IF N_ELEMENTS(column) EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;              Select what column to read in.  this can be either a character
;              string or an integer.
;---------------------------------------------------------------------------
               IF !d.window NE -1 THEN $
                  n_col = xsel_item(image_list, $
                                    title=['Press the button', $
                                           'below to select data'], $
                                    group=group)$
               ELSE BEGIN
                  OPENW, outunit, filepath(/terminal), /more, /GET_LUN
                  FOR i = 0,ndata-1 DO BEGIN
                     PRINTF,outunit,col(i),'   ',ttype(i)
                     IF !err EQ 1 THEN GOTO, done
                  ENDFOR
done:	          FREE_LUN, outunit
                  n_col = ''
                  WHILE n_col EQ '' DO READ,'Enter column to read: ',n_col
                  IF valid_num(n_col) THEN n_col = LONG(n_col)
               ENDELSE
               IF n_col EQ -1 THEN BEGIN
                  errmsg = 'Operation is canceled.'
                  RETURN
               ENDIF
               cur_col = col(n_col)
            ENDIF ELSE BEGIN
               cur_col = column
            ENDELSE
            fxbclose, unit
            column = cur_col
            cds_image, d_file, data_stc, cur_col
            data_info = {binary:1, label:image_list, col:col, cur_col:cur_col}
            image = data_stc.array
            sz = SIZE(image)
            csi.naxis1 = sz(1)
            csi.naxis2 = sz(2)
            csi.crpix1 = 1
            csi.crpix2 = 1
            csi.imagtype = data_stc.label(cur_col)
            header = data_stc.header
;---------------------------------------------------------------------------
;           In this case, the reference point is the first pixel of the image
;---------------------------------------------------------------------------
            angles = pb0r(csi.date_obs)
            csi.crval1 = data_stc.origin(0)
            csi.crval2 = data_stc.origin(1)
            csi.cdelt1 = data_stc.spacing(0)
            csi.cdelt2 = data_stc.spacing(1)
            csi.radius = 60.*angles(2)
            csi.flag = 1
         ENDIF ELSE BEGIN
            status = 0
            errmsg = 'Keyword EXTEND not found'
         ENDELSE
      ENDIF ELSE BEGIN
         errmsg = 'NAXIS has to be either 2 or 0!'
         status = 0
      ENDELSE
   ENDELSE

   IF N_ELEMENTS(recomp) THEN BEGIN
      IF (ext(n_ext-1) EQ 'Z') THEN $
         SPAWN,'compress '+d_file $
      ELSE SPAWN,'/usr/local/bin/gzip '+d_file
   ENDIF

   image_min = MIN(image)
   image_max = MAX(image)-1.0

END

;---------------------------------------------------------------------------
; End of 'itool_rd_fits.pro'.
;---------------------------------------------------------------------------
