;---------------------------------------------------------------------------
; Document name: load_eit_color.pro
; Created by:    Liyun Wang, NASA/GSFC, July 15, 1997
;
; Last Modified: Tue Jul 15 16:54:06 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO load_eit_color, header
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       LOAD_EIT_COLOR
;
; PURPOSE: 
;       Load EIT color table based on the wavelength (in FITS header)
;
; CATEGORY:
;       Utility
; 
; SYNTAX: 
;       load_eit_color, header
;
; INPUTS:
;       HEADER - FITS header, string array
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       None.
;
; COMMON:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, July 15, 1997, Liyun Wang, NASA/GSFC. Written
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 1
   
   IF N_ELEMENTS(header) NE 0 THEN BEGIN
      wavelength = fxpar(header, 'WAVELNTH')
      IF !err EQ -1 THEN wavelength = 304
   ENDIF ELSE wavelength = 304
   
   color_ok = 0
;---------------------------------------------------------------------------
;  Load different color table for different wavelengths. If something
;  goes wrong, load the default red temparature color table
;---------------------------------------------------------------------------
   ssw_data = GETENV('SSW_SETUP_DATA')
   IF ssw_data NE '' THEN BEGIN
      eit_ctable = concat_dir(ssw_data, 'color_table.eit')
      IF file_exist(eit_ctable) THEN BEGIN
         wlist = [171, 195, 284, 304]
         i_wave = WHERE(wlist EQ wavelength)
         IF i_wave(0) GE 0 THEN BEGIN
            loadct, 42+i_wave(0), file=eit_ctable
            color_ok = 1
         ENDIF
      ENDIF
   ENDIF
   IF NOT color_ok THEN BEGIN
      MESSAGE, 'No EIT color table found. Default color table loaded.', $
         /cont
      loadct, 3
   ENDIF
END

;---------------------------------------------------------------------------
; End of 'load_eit_color.pro'.
;---------------------------------------------------------------------------
