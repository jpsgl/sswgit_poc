;---------------------------------------------------------------------------
; Document name: itool_plot_axes.pro
; Created by:    Liyun Wang, GSFC/ARC, March 13, 1995
;
; Last Modified: Tue Sep  3 14:59:26 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO itool_plot_axes, csi=csi, title=title
;+
; PROJECT:
;       SOHO - CDS
;
; NAME:
;       ITOOL_PLOT_AXES
;
; PURPOSE:
;       Plot axes and labels around the current displayed image
;
; EXPLANATION:
;
; CALLING SEQUENCE:
;       itool_plot_axes, csi=csi
;
; INPUTS:
;       CSI - Coordinate system info structure
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       TVAXIS
;
; COMMON BLOCKS:
;       ITOOL_AXES_COM (used by SET_POINT_BASE in mk_point_base.pro)
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;
; PREVIOUS HISTORY:
;       Written March 13, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1, created, Liyun Wang, GSFC/ARC, March 13, 1995
;       Version 2, Liyun Wang, GSFC/ARC, April 11, 1995
;          Made data coordinate system established after calling this routine
;       Version 3, August 28, 1996, Liyun Wang, NASA/GSFC
;          Used XRANGE and YRANGE keywords (instead of ORIGIN and
;             SCALE) in call to TVAXIS for better accuracy
;
; VERSION:
;       Version 3, August 28, 1996
;-
;
   COMMON itool_axes_com, x_range, y_range
   ON_ERROR, 2
   
   temp = cnvt_coord([csi.xd0, csi.yd0], from=1, to=3, csi=csi)
   x0 = temp(0)
   y0 = temp(1)
   
   temp = cnvt_coord([csi.xd0+csi.mx-1, csi.yd0+csi.my-1], $
                     from=1, to=3, csi=csi)
   x1 = temp(0)
   y1 = temp(1)
   
   x_range = [x0, x1]
   y_range = [y0, y1]
   
;---------------------------------------------------------------------------
;  The DATA keyword is used for establishing the data coordinate system
;---------------------------------------------------------------------------
   tvaxis, xrange=x_range, yrange=y_range, /data
   
;---------------------------------------------------------------------------
;  I want ticks to be plotted outward
;---------------------------------------------------------------------------
   saved_p = !p
   !p.ticklen = -0.01
   tvaxis, xaxis=0, xtitle=title(0)
   tvaxis, xaxis=1, /noxlabel
   tvaxis, yaxis=0, ytitle=title(1)
   tvaxis, yaxis=1, /noylabel

   !p = saved_p

END

;---------------------------------------------------------------------------
; End of 'itool_plot_axes.pro'.
;---------------------------------------------------------------------------
