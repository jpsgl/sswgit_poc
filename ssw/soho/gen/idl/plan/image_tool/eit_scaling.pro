FUNCTION eit_scaling, image, header, min_val=min_val, max_val=max_val, $
                      color_only=color_only
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       EIT_SCALING()
;
; PURPOSE:
;       Rescale an image based on EIT image scaling algorithm
;
; CATEGORY:
;       Image, utility
;
; EXPLANATION:
;
; SYNTAX:
;       Result = eit_scaling(image)
;
; INPUTS:
;       IMAGE   - 2D image array; may be rescaled
;       HEADER  - String vector holding header of EIT FITS file
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       RESULT  - Rescaled image array
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       MIN_VAL    - Named output variable, new miminum value in IMAGE
;       MAX_VAL    - Named output variable, new maxinum value in IMAGE
;       COLOR_ONLY - Set this keyword to just get EIT color table
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       Environment variable SSW_EIT must be properly defined to get
;       EIT routines and color tables 
;
; SIDE EFFECTS:
;       Input IMAGE array is rescaled.
;
; HISTORY:
;       Version 1, March 8, 1996, Liyun Wang, NASA/GSFC. Written
;       Version 2, April 22, 1996, Liyun Wang, NASA/GSFC
;          Applied degridding algoritum before rescaling
;       Version 3, September 30, 1996, Liyun Wang, NASA/GSFC
;          Added COLOR_ONLY keyword
;       Version 4, October 23, 1997, Liyun Wang, NASA/GSFC
;          Calls EIT_DEGRID directly if SSW_EIT is defined
;	Version 5, 31-Oct-1997, William Thompson, GSFC
;	   Make sure that EIT_DEGRID is actually in the path.
;	Version 6, William Thompson, GSFC, 8 April 1998
;		Changed !D.N_COLORS to !D.TABLE_SIZE for 24-bit displays
;       Version 7, Zarro (SAC/GSFC) - saved check for EIT_DARK in common
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2

   common eit_scaling,have_eit_dark

   IF NOT KEYWORD_SET(color_only) THEN BEGIN
      IF chk_dir(GETENV('SSW_EIT')) EQ 1 THEN BEGIN
;---------------------------------------------------------------------------
;        If in the path, use EIT routines to degrid the image
;---------------------------------------------------------------------------

        if not exist(have_eit_dark) then $
         have_eit_dark=FIND_WITH_DEF('eit_dark.pro', !PATH) NE ''
        if have_eit_dark then begin
          test=execute('image = TEMPORARY(image)-eit_dark()')
          test=execute('image = eit_degrid(TEMPORARY(image), header)')
;---------------------------------------------------------------------------
;	 Otherwise, use itool_eit_degrid.
;---------------------------------------------------------------------------
	END ELSE BEGIN
          image = TEMPORARY(image)-848
          image = itool_eit_degrid(TEMPORARY(image), header)
	ENDELSE
      ENDIF ELSE BEGIN
         image = TEMPORARY(image)-848
      ENDELSE
      max_val = MAX(image)
      scl = FLOAT(!d.table_size - 1)/ALOG10(max_val)
      image = scl*ALOG10((TEMPORARY(image) > 1.) < max_val)
      max_val = MAX(image)
      min_val = MIN(image)
   ENDIF

   load_eit_color, header
   RETURN, image
END

;---------------------------------------------------------------------------
; End of 'eit_scaling.pro'.
;---------------------------------------------------------------------------
