;---------------------------------------------------------------------------
; Document name: get_source_stc.pro
; Created by:    Liyun Wang, GSFC/ARC, August 16, 1995
;
; Last Modified: Fri Sep 26 09:28:08 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
FUNCTION get_source_stc, summary=summary
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       GET_SOURCE_STC()
;
; PURPOSE: 
;       Get a source structure based on the datatype. 
;
; CATEGORY:
;       Planning, Image_tool
; 
; EXPLANATION:
;       This routine returns a structure that refelct the image
;       sources of the solar image FITS files
;
; SYNTAX: 
;       Result = get_source_stc()
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       RESULT - A structure that contains the following tag names:
;          NAME    - Name of image sources to be shown
;          DIRNAME - the actual directory name for the image sources
;                    under the SYNOP_DATA or SUMMARY_DATA direcotry
;
;          By default, RESUTL is a source structure for synoptic data
;          unless the keyword SUMMARY is set.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       SUMMARY - Set this keyword to 1 or 2 for a source structure for SOHO
;                 summary or private data
;
; COMMON:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, August 16, 1995, Liyun Wang, GSFC/ARC. Written
;       Version 2, June 17, 1996, Liyun Wang, GSFC/ARC
;          Added Pic du Midi Observatory
;       Version 3, July 30, 1996, Liyun Wang, NASA/GSFC
;          Added Kiepenheuer Institute for Solar Physics
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 1

   IF NOT KEYWORD_SET(summary) THEN BEGIN
      name = ['Yohkoh Soft-X Telescope', $
              'Big Bear Solar Observatory',$
              'Kitt Peak National Observatory', $
              'Learmonth Observatory, Australia',$
              'Mt. Wilson Observatory', $
              'Space Environment Lab',$
              'Mauna Loa Solar Observatory',$
              'Holloman AFB',$
              'Mees Solar Observatory',$
              'Nobeyama Radio Observatory',$
              'Sacramento Peak Observatory',$
              'Pic du Midi Observatory',$
              'Obs. of Paris at Meudon', $
              'Kiepenheuer Inst. for Solar Phys.', $
              'Kanzelhohe Solar Observatory',$
              'Other Institutes']
      dirname = ['yohk', 'bbso', 'kpno', 'lear', 'mwno', 'kbou', $
                 'mlso', 'khmn', 'mees', 'nobe', 'ksac', 'pdmo', $
                 'meud', 'kisf', 'kanz', 'other']
      sources = {dir_index:0, name:name, dirname:dirname}
   ENDIF ELSE BEGIN
      name = ['SOHO-CDS',$
              'SOHO-SUMER',$
              'SOHO-EIT',$
              'SOHO-LASCO',$
              'SOHO-UVCS',$
              'SOHO-MDI',$
              'SOHO-GOLF',$
              'SOHO-SWAN',$
              'SOHO-VIRGO',$
              'SOHO-CEPAC',$
              'SOHO-CELIAS']
      dirname = ['cds', 'sumer', 'eit', 'lasco', 'uvcs', 'mdi', 'golf', $
                 'swan', 'virgo', 'cepac', 'celias']
      sources = {dir_index:0, name:name, dirname:dirname}
   ENDELSE
   RETURN, sources
END

;---------------------------------------------------------------------------
; End of 'get_source_stc.pro'.
;---------------------------------------------------------------------------
