;---------------------------------------------------------------------------
; Document name: itool_draw.pro
; Created by:    Liyun Wang, NASA/GSFC, January 29, 1997
;
; Last Modified: Thu Jun 12 10:47:34 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO itool_draw, event
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       ITOOL_DRAW
;
; PURPOSE:
;       Handling draw events from the main graphics window
;
; CATEGORY:
;       image tool
;
; SYNTAX:
;       itool_draw, event
;
; INPUTS:
;       EVENT - Event structure
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       None.
;
; COMMON:
;       @image_tool_com
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, January 29, 1997, Liyun Wang, NASA/GSFC. Written
;          Extracted from image_tool.pro
;       Version 2, March 6, 1997, Liyun Wang, NASA/GSFC
;          Implemented differential rotation indicator for any constant
;             longitudinal points and points at the same Solar X value
;       Version 3, June 12, 1997, Liyun Wang, NASA/GSFC
;          Changed call from CROSS_HAIR to ITOOL_CROSS_HAIR
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
@image_tool_com
   ON_ERROR, 1
   cx = event.x & cy=event.y

   IF !d.window NE root_win THEN setwindow, root_win

   IF track_cursor THEN cursor_info, cx, cy, txt_id, csi=csi, $
      d_mode=d_mode, date=img_utc
   IF event.type EQ 0 THEN BEGIN
      cursor_info, cx, cy, txt_id, csi=csi, d_mode=d_mode, date=img_utc

;---------------------------------------------------------------------------
;     Deal with some rotation related draw events
;---------------------------------------------------------------------------
      CASE (rot_1pt) OF
         1: BEGIN
            rot_1pt = 0
            old_pos = [cx, cy]
            tmp = cnvt_coord([cx, cy], csi=csi, from=1, to=3)
            IF SQRT(tmp(0)*tmp(0)+tmp(1)*tmp(1)) GT csi.radius THEN BEGIN
               flash_msg, comment_id, num=3, $
                  'You must click inside the solar disc!'
               RETURN
            ENDIF
            helio = cnvt_coord(old_pos, csi=csi, from=1, to=4, date=disp_utc)
;----------------------------------------------------------------------
;           When converting rotated point(s) back, new time should be used
;----------------------------------------------------------------------
            msec = LONG(time_gap*8640000.0) ; in milliseconds
            cur_time = anytim2utc(disp_utc)
            cur_time.time = cur_time.time+msec(0)
            new_date = anytim2utc(cur_time, /external)
            helio(1) = helio(1)+diff_rot(time_gap, helio(0), /synodic)
            IF (90.0-helio(1)) LE 0 THEN BEGIN
               flash_msg, comment_id, 'The point will be off the limb!'
               RETURN
            ENDIF
            itool_restore_pix, pix_win
            new_pos = cnvt_coord(helio, csi=csi, from=4, to=1, date=new_date)
            PLOTS, [old_pos(0), new_pos(0)], [old_pos(1), new_pos(1)], $
               /dev, color=!d.n_colors-1
            itool_copy_to_pix
            itool_cross_hair, new_pos(0), new_pos(1), cursor_wid, cursor_ht, $
               cursor_unit, csi=csi, color=l_color, /keep, $
               boxed_cursor=boxed_CURSOR, pixmap=pix_win
            WIDGET_CONTROL, comment_id, set_value=''
         END
         2: BEGIN
            rot_1pt = 0
            old_pos = [cx, cy]
            tmp = cnvt_coord([cx, cy], csi=csi, from=1, to=3)
            IF SQRT(tmp(0)*tmp(0)+tmp(1)*tmp(1)) GT csi.radius THEN BEGIN
               flash_msg, comment_id, num=3, $
                  'You must click inside the solar disc!'
               RETURN
            ENDIF
            helio = cnvt_coord(old_pos, csi=csi, from=1, to=4, date=disp_utc)
            lat = 2*FINDGEN(85)-84.0
            longi = FLTARR(N_ELEMENTS(lat))
            longi(*) = helio(1)
            temp = TRANSPOSE([[lat], $
                              [longi+diff_rot(time_gap, lat, /synodic)]])
            itool_restore_pix, pix_win
            itool_rotplot, temp
            itool_copy_to_pix
         END
         3: BEGIN
            rot_1pt = 0
            old_pos = [cx, cy]
            tmp = cnvt_coord([cx, cy], csi=csi, from=1, to=2)
            rpixel = LONG(csi.radius/csi.srx)
            xx2 = (tmp(0)-csi.x0)^2
            rr2 = rpixel*rpixel
            IF xx2 GE rr2 THEN BEGIN
               flash_msg, comment_id, num=3, $
                  'The point you select must go through the solar disc!'
               RETURN
            ENDIF
            square = SQRT(rr2-xx2)
            offset = 5
            y1 = FIX(csi.y0-square)+offset
            y2 = FIX(csi.y0+square)-offset
            x0 = tmp(0)
            ydev = y1+INDGEN(y2-y1+1)
            xdev = INTARR(N_ELEMENTS(ydev))
            xdev(*) = x0
            helio = cnvt_coord(TRANSPOSE([[xdev], [ydev]]), csi=csi, from=2, $
                               to=4, date=disp_utc)
            lat = helio(0,*) & lat = lat(*)
            longi = helio(1,*) & longi = longi(*)
            ii = WHERE(lat LE 85., count)
            IF count GT 0 THEN BEGIN
               lat = lat(ii)
               longi = longi(ii)
               temp = TRANSPOSE([[lat], $
                                 [longi+diff_rot(time_gap, lat, /synodic)]])
               xdev = xdev(ii)
               ydev = ydev(ii)
               tmp = cnvt_coord(TRANSPOSE([[xdev], [ydev]]), csi=csi, $
                  from=2, to=1)
               itool_restore_pix, pix_win
               PLOTS, tmp(0, *), tmp(1, *), /dev, linestyle=2
               itool_rotplot, temp
               itool_copy_to_pix
            ENDIF
         END
         ELSE:
      ENDCASE

      IF limbfit_flag EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;        Plot cross-hair cursor: Click left button to plot new cursor,
;        middle button to plot a non-removable cursor
;---------------------------------------------------------------------------
         IF event.press EQ 1 THEN BEGIN
            itool_cross_hair, cx, cy, cursor_wid, cursor_ht, cursor_unit, $
               csi=csi, color=l_color, boxed_cursor=boxed_cursor, $
               pixmap=pix_win
         ENDIF ELSE IF event.press EQ 2 THEN BEGIN
            itool_cross_hair, cx, cy, cursor_wid, cursor_ht, cursor_unit, $
               csi=csi, color=l_color, boxed_cursor=boxed_cursor, /keep, $
               pixmap=pix_win
         ENDIF
      ENDIF
      IF csi.flag THEN BEGIN
         initial = cnvt_coord([cx, cy], csi=csi, from=1, to=3)
      ENDIF
   ENDIF

END

;---------------------------------------------------------------------------
; End of 'itool_draw.pro'.
;---------------------------------------------------------------------------
