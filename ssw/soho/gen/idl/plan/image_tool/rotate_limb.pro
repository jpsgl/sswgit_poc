;---------------------------------------------------------------------------
; Document name: rotate_limb.pro
; Created by:    Liyun Wang, GSFC/ARC, May 5, 1995
;
; Last Modified: Fri Jan 31 13:19:51 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
FUNCTION rotate_limb, time_gap
;+
; PROJECT:
;       SOHO - CDS
;
; NAME:	
;       ROTATE_LIMB()
;
; PURPOSE:
;       Make a 2xN array that results from rotating points on the limb
;
; EXPLANATION:
;       
; CALLING SEQUENCE: 
;       Result = rotate_limb(rot_dir, time_gap)
;
; INPUTS:
;       TIME_GAP   - Time interval (in days) over which the rotation is made;
;                    the sign of TIME_GAP determines whether the rotation is
;                    forward or backward
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       RESULT - A 2xN array: result(0,*) latitude of points in degrees
;                             result(1,*) longitude of points in degrees
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS: 
;       None.
;
; CALLS:
;       None.
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       
; PREVIOUS HISTORY:
;       Written May 5, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1, created, Liyun Wang, GSFC/ARC, May 5, 1995
;
; VERSION:
;       Version 1, May 5, 1995
;-
;
   ON_ERROR, 2
   IF N_ELEMENTS(time_gap) EQ 0 THEN BEGIN
      MESSAGE, 'Syntax: RESULT = rotate_limb(time_gap)'
      RETURN, -1
   ENDIF
   
   FOR i = -85, 85, 5 DO BEGIN
      IF N_ELEMENTS(lat) EQ 0 THEN lat = FLOAT(i) ELSE $
         lat = [lat, FLOAT(i)]
   ENDFOR
   
   n_pnt = N_ELEMENTS(lat)
   longi = FLTARR(n_pnt) 
   IF time_gap GT 0 THEN longi(*) = -90.0 ELSE longi(*) = 90.0

   FOR i = 0, n_pnt-1 DO BEGIN
      longi(i) = longi(i)+diff_rot(time_gap, lat(i), /synodic)
   ENDFOR
   
   RETURN, TRANSPOSE([[lat], [longi]])
END

;---------------------------------------------------------------------------
; End of 'rotate_limb.pro'.
;---------------------------------------------------------------------------
