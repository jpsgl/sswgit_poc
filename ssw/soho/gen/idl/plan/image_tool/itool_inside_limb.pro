;---------------------------------------------------------------------------
; Document name: itool_inside_limb.pro
; Created by:    Liyun Wang, GSFC/ARC, April 3, 1996
;
; Last Modified: Thu Apr  4 09:34:32 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
FUNCTION itool_inside_limb, ppx, ppy, csi=csi, error=error
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       ITOOL_INSIDE_LIMB()
;
; PURPOSE: 
;       Detect if points defined by ppx and ppy are within the limb
;
; CATEGORY:
;       Utility, Image Tool
; 
; SYNTAX: 
;       result = itool_inside_limb(ppx, ppy, csi=csi)
;
; INPUTS:
;       PPX - X position of points in data pixels
;       PPY - Y position of points in data pixels 
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       RESULT - 1 if all points are within the limb, otherwise 0. For
;                syntax errors, a -1 is returned
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       CSI - Coordinate system info structure; required
;       ERROR - Error message; a null string if no error occurs
;
; COMMON:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, April 3, 1996, Liyun Wang, GSFC/ARC. Written
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2
   error = ''
   npt = N_ELEMENTS(ppx) 
   IF N_ELEMENTS(ppy) NE npt THEN BEGIN
      error = 'Two input parameters must have the same dimension!'
      MESSAGE, error, /cont
      RETURN, -1
   ENDIF
   
   IF datatype(csi) NE 'STC' THEN BEGIN
      error = 'Keyword CSI must be a CSI structure.'
      MESSAGE, error, /cont
      RETURN, -1
   ENDIF
      
;---------------------------------------------------------------------------
;  Get solar disc center position in image pixels
;---------------------------------------------------------------------------
   x0 = ROUND(csi.x0-csi.xv0/csi.srx)
   y0 = ROUND(csi.y0-csi.yv0/csi.sry)
   
;---------------------------------------------------------------------------
;  Get solar disc radius in image pixels
;---------------------------------------------------------------------------
   r0 = 2.0*csi.radius/(csi.srx+csi.sry)

;---------------------------------------------------------------------------
;  Get indices of points which are outside the disc
;---------------------------------------------------------------------------
   dist = SQRT((ppx-x0)^2+(ppy-y0)^2)
   ii = WHERE(dist LE r0, cnt)
   
   IF cnt EQ npt THEN RETURN, 1 ELSE RETURN, 0
      
END

;---------------------------------------------------------------------------
; End of 'itool_inside_limb.pro'.
;---------------------------------------------------------------------------
