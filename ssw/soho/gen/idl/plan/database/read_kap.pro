FUNCTION READ_KAP, FILENAME, ERRMSG=ERRMSG, IGNORE=IGNORE, NOPURGE=NOPURGE
;+
; Project     :	SOHO - CDS/SUMER
;
; Name        :	READ_KAP()
;
; Purpose     :	Reads in a KAP file.
;
; Explanation :	Reads in a KAP file, and stores the information in the
;		appropriate databases.
;
; Use         :	Result = READ_KAP( FILENAME )
;
;		Result = READ_KAP( FILENAME, IGNORE='CDS')
;		Result = READ_KAP( FILENAME, IGNORE='SUMER')
;
; Inputs      :	FILENAME = The name of the file to read.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    :	IGNORE	 = If passed, then contains the name or code value (see
;			   GET_INSTRUMENT) of an instrument to ignore when
;			   reading in SCIPLAN entries.  This is so that one can
;			   avoid overwriting one's own SCIPLAN entries, if
;			   desired.
;
;		ERRMSG   = If defined and passed, then any error messages will
;			   be returned to the user in this parameter rather
;			   than being handled by the IDL MESSAGE utility.  If
;			   no errors are encountered, then a null string is
;			   returned.  In order to use this feature, the string
;			   ERRMSG must be defined first, e.g.,
;
;				ERRMSG = ''
;				Result = READ_KAP( FILENAME, ERRMSG=ERRMSG )
;				IF ERRMSG NE '' THEN ...
;
;               NOPURGE  = set to not automatically purge the DB's each time
;                          (useful when calling the routine in a loop)
;
; Calls       :	OPEN_KAP, READ_KAP_ITEM, CLR_RESOURCE, CLR_TEL_MODE,
;		CLR_TEL_SUBMODE, CLR_OTHER_OBS, ADD_RESOURCE, ADD_TEL_MODE,
;		ADD_TEL_SUBMODE, ADD_OTHER_OBS, UPD_PLAN, UPD_SOHO_DET,
;		PRG_PLAN, PRG_SOHO_DET, LAST_KAP_VERS, SET_KAP_VERS
;
; Common      :	None.
;
; Restrictions:	When using the CDS database system, !PRIV must be 3 or greater
;		to use this routine.  This restriction is enforced except in
;		VMS, where it is assumed that the Oracle-based SUMER system is
;		used instead.
;
; Side effects:	None.
;
; Category    :	Planning, science.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 3 April 1995
;
; Modified    :	Version 1, William Thompson, GSFC, 10 April 1995
;		Version 2, William Thompson, GSFC, 11 April 1995
;			Added IGNORE keyword
;		Version 3, William Thompson, GSFC, 12 April 1995
;			Added support for PROGRAM item.
;		Version 4, William Thompson, GSFC, 13 April 1995
;			Added support for OTHER_OBS item.
;		Version 5, William Thompson, GSFC, 19 May 1995
;			Modified to ignore MDI's "Struct_Prog" program which
;			overlaps other MDI programs.
;		Version 6, William Thompson, GSFC, 22 May 1995
;			Modified to write to database at end, to speed up.
;		Version 7, William Thompson, GSFC, 24 May 1995
;			Added called to PRG_ routines.
;			Added call to SET_KAP_VERS.
;		Version 8, William Thompson, GSFC, 8 August 1995
;			Modified to ignore MDI's "FD_Mag" program which
;			overlaps other MDI programs.
;		Version 9, Dominic Zarro, GSFC, 8 March 1996
;			Added reading of DELAYED commanding DB
;               Version 10, Dominic Zarro, GSFC, 16 June 1996
;                       Added NOPURGE keyword and clearing of SCI and DET
;                       databases
;
; Version     :	Version 10
;-
;
	ON_ERROR, 2
;
;  Make sure that UNIT is initialized to -1 so that errors can be handled
;  correctly.
;
	UNIT = -1
;
;  Check the number of parameters.
;
	IF N_PARAMS() NE 1 THEN BEGIN
		MESSAGE = 'Syntax:  Result = READ_KAP( FILENAME )'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that FILENAME is a character string scalar.
;
	IF N_ELEMENTS(FILENAME) NE 1 THEN BEGIN
		MESSAGE = 'FILENAME must be a scalar'
		GOTO, HANDLE_ERROR
	END ELSE IF DATATYPE(FILENAME,1) NE 'String' THEN BEGIN
		MESSAGE = 'FILENAME must be a character string'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that one has sufficient privilege to run the software.
;
	IF !VERSION.OS NE 'vms' THEN BEGIN
	    TEST = EXECUTE('PRIV = !PRIV')
	    IF PRIV LT 3 THEN BEGIN
		MESSAGE = '!PRIV must be 3 or greater to run this routine'
		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
;  If the IGNORE keyword was passed, then determine which instrument this
;  refers to.
;
	IF N_ELEMENTS(IGNORE) EQ 1 THEN BEGIN
		GET_INSTRUMENT, IGNORE, INS, ERRMSG=ERRMSG
		IF N_ELEMENTS(ERRMSG) NE 0 THEN IF ERRMSG(0) NE '' THEN BEGIN
			MESSAGE = ERRMSG
			GOTO, HANDLE_ERROR
		ENDIF
		INS_IGNORE = INS.CODE
	END ELSE INS_IGNORE = 'None'
;
;  Extract a list of all resource types.
;
	LIST_RES_TYPE, RES_TYPES
;
;  Open the input file.
;
	OPEN_KAP, UNIT, FILENAME, HEADER, ERRMSG=ERRMSG
	IF UNIT LT 0 THEN BEGIN
		MESSAGE = 'Unable to open file ' + FILENAME
		GOTO, HANDLE_ERROR
	ENDIF
	PRINT, 'Reading file ' + FILENAME
;
;  Get the start and end times, and clear the relevant databases.
;
	START_TIME = ''
	END_TIME = ''
	FOR I=0,N_ELEMENTS(HEADER)-2 DO BEGIN
		LINE = HEADER(I)
		SEP = STRPOS(LINE,'=')
		IF (SEP LE 0) OR (SEP GE STRLEN(LINE)) THEN BEGIN
			MESSAGE = 'Header line is not in keyword=value format'
			GOTO, HANDLE_ERROR
		ENDIF
		KEYWORD = STRTRIM( STRMID(LINE,0,SEP), 2)
		VALUE   = STRTRIM( STRMID(LINE,SEP+1, STRLEN(LINE)-SEP-1), 2)
		IF KEYWORD EQ 'STARTIME' THEN START_TIME = VALUE
		IF KEYWORD EQ 'ENDTIME'  THEN END_TIME   = VALUE
	ENDFOR
;
	IF START_TIME EQ '' THEN BEGIN
		MESSAGE = 'Start time not found in header'
		GOTO, HANDLE_ERROR
	ENDIF
;
	IF END_TIME EQ '' THEN BEGIN
		MESSAGE = 'End time not found in header'
		GOTO, HANDLE_ERROR
	ENDIF
;
	IF NOT CLR_RESOURCE(START_TIME, END_TIME, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to clear resource database'
		GOTO, HANDLE_ERROR
	ENDIF
;
	IF NOT CLR_TEL_MODE(START_TIME, END_TIME, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to clear telemetry mode database'
		GOTO, HANDLE_ERROR
	ENDIF
;
	IF NOT CLR_TEL_SUBMODE(START_TIME, END_TIME, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to clear telemetry submode database'
		GOTO, HANDLE_ERROR
	ENDIF
;

	IF NOT CLR_NRT_RES(START_TIME, END_TIME, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to clear NRT reserved database'
		GOTO, HANDLE_ERROR
	ENDIF
;
	IF WHICH_INST() NE 'S' THEN BEGIN
         IF NOT call_function('CLR_INST_DLYD',START_TIME, END_TIME, ERRMSG=ERRMSG) THEN BEGIN
	        MESSAGE = 'Unable to clear Delayed commanding database'
          	GOTO, HANDLE_ERROR
         ENDIF
	ENDIF
;
	IF NOT CLR_OTHER_OBS(START_TIME, END_TIME, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to clear other observatories database'
		GOTO, HANDLE_ERROR
        ENDIF

;
;  Keep track of how many items of each type have been read in.
;
	N_SCIPLAN = 0
	N_PROGRAM = 0
	N_TLM_MODE = 0
	N_TLM_SUBMODE = 0
	N_RESOURCE = 0
	N_NRT_RESERVED = 0
	N_DELAYED_CMD = 0
	N_OTHER_OBS = 0
;
;  Read in the KAP file item by item until the end is reached.
;
	TYPE = 'Test'
	WHILE TYPE NE '' DO BEGIN
		READ_KAP_ITEM2, UNIT, TYPE, ITEM, ERRMSG=ERRMSG
		IF N_ELEMENTS(ERRMSG) NE 0 THEN IF ERRMSG NE '' THEN	$
			GOTO, HANDLE_ERROR
;
;  Collect all the SCIPLAN entries.  However, ignore any MDI entries with the
;  mnemonic "Struct_Prog", or entries for the instrument that was selected to
;  be ignored.
;
		IF TYPE EQ 'SCIPLAN' THEN BEGIN
		    IF (ITEM.INSTRUME NE INS_IGNORE) AND		$
                       (ITEM.START_TIME LT  ITEM.END_TIME) AND $
			    ((ITEM.INSTRUME NE 'M') OR			$
			    ((ITEM.MNEMONIC NE 'Struct_Prog') AND	$
			    (ITEM.MNEMONIC NE 'FD_Mag'))) THEN BEGIN
			IF N_SCIPLAN EQ 0 THEN SCIPLAN = ITEM ELSE	$
				SCIPLAN = [SCIPLAN, ITEM]
			N_SCIPLAN = N_SCIPLAN + 1
		    ENDIF
		ENDIF
;
;  Collect all the PROGRAM entries.
;
		IF TYPE EQ 'PROGRAM' THEN BEGIN
		    IF (ITEM.INSTRUME NE INS_IGNORE) AND		$
                       (ITEM.DATE_OBS LT  ITEM.DATE_END) THEN BEGIN
           	        IF N_PROGRAM EQ 0 THEN PROGRAM = ITEM ELSE	$
			        PROGRAM = [PROGRAM, ITEM]
		        N_PROGRAM = N_PROGRAM + 1
                    ENDIF
		ENDIF
;
;  Collect all the TLM_MODE and TLM_SUBMODE entries.
;
		IF TYPE EQ 'TLM_MODE' THEN BEGIN
		    IF N_TLM_MODE EQ 0 THEN TLM_MODE = ITEM ELSE	$
			    TLM_MODE = [TLM_MODE, ITEM]
		    N_TLM_MODE = N_TLM_MODE + 1
		ENDIF
;
		IF TYPE EQ 'TLM_SUBMODE' THEN BEGIN
		    IF N_TLM_SUBMODE EQ 0 THEN TLM_SUBMODE = ITEM ELSE	$
			    TLM_SUBMODE = [TLM_SUBMODE, ITEM]
		    N_TLM_SUBMODE = N_TLM_SUBMODE + 1
		ENDIF
;
;  Collect all the general SOHO resource entries.
;
		IF (WHERE(TYPE EQ RES_TYPES))(0) NE -1 THEN BEGIN
		    IF N_RESOURCE EQ 0 THEN RESOURCE = ITEM ELSE	$
			    RESOURCE = [RESOURCE, ITEM]
		    N_RESOURCE = N_RESOURCE + 1
		ENDIF

;
;  Collect all the INST_NRT_RESERVED entries.
;
		IF TYPE EQ 'INST_NRT_RESERVED' THEN BEGIN
		    IF N_NRT_RESERVED EQ 0 THEN NRT_RESERVED = ITEM ELSE $
			    NRT_RESERVED = [NRT_RESERVED, ITEM]
		    N_NRT_RESERVED = N_NRT_RESERVED + 1
		ENDIF

;
;  Collect all the INST_DELAYED_CMD entries.
;
		IF TYPE EQ 'INST_DELAYED_CMD' THEN BEGIN
		    IF N_DELAYED_CMD EQ 0 THEN DELAYED_CMD = ITEM ELSE $
			    DELAYED_CMD = [DELAYED_CMD, ITEM]
		    N_DELAYED_CMD = N_DELAYED_CMD + 1
		ENDIF
;
;
;  Collect all the OTHER_OBS entries.
;
		IF TYPE EQ 'OTHER_OBS' THEN BEGIN
		    IF N_OTHER_OBS EQ 0 THEN OTHER_OBS = ITEM ELSE	$
			    OTHER_OBS = [OTHER_OBS, ITEM]
		    N_OTHER_OBS = N_OTHER_OBS + 1
		ENDIF
;
	ENDWHILE
;
;-- Update SCIENCE and DETAILED Plans for those instruments that submitted
;   IAP's. Make sure that old plans are removed.

        inst=get_soho_inst(/short)
        ninst=n_elements(inst)
	if n_sciplan gt 0 then begin
	 print, '-- updating science plan'
         for i=0,ninst-1 do begin
          ilook=where(sciplan.instrume eq inst(i),icount)
          if icount gt 0 then begin
           dprint,'-- purging SCIENCE plan for '+get_soho_inst(inst(i))
           list_plan,start_time,end_time,plans,nobs,inst=inst(i)
           if (nobs gt 0) then begin
            clook=where( (plans.start_time ge anytim2tai(start_time)) and $
                         (plans.start_time lt anytim2tai(end_time)),count)
            if count gt 0 then begin
             plans=plans(clook)
             for k=0,count-1 do s=del_plan(plans(k).start_time,inst=inst(i))
            endif
           endif
          endif
         endfor
         s=upd_plan(sciplan, errmsg=errmsg)
	endif

;-- Don't do this on SUMER

	if n_program gt 0 then begin
	 print, '-- updating detailed science plan'
         if which_inst() ne 'S' then begin
          for i=0,ninst-1 do begin
           ilook=where(program.instrume eq inst(i),icount)
           if icount gt 0 then begin
            dprint,'-- purging DETAILED plan for '+get_soho_inst(inst(i))
            list_soho_det,start_time,end_time,dets,nobs,inst=inst(i)
            if (nobs gt 0) then begin
             clook=where( (dets.date_obs ge anytim2tai(start_time)) and $
                          (dets.date_end lt anytim2tai(end_time)),count)
             if count gt 0 then begin
              dets=dets(clook)
              for k=0,count-1 do s=call_function('del_soho_det',dets(k).date_obs,inst=inst(i))
             endif
            endif
           endif
          endfor
         endif
	 s=upd_soho_det(program, errmsg=errmsg)
	endif
;
	IF N_TLM_MODE GT 0 THEN BEGIN
	    PRINT, '-- Updating telemetry modes'
	    IF NOT ADD_TEL_MODE(TLM_MODE, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to insert telemetry ' + $
			'mode change'
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
	IF N_TLM_SUBMODE GT 0 THEN BEGIN
	    PRINT, '-- Updating telemetry submodes'
	    IF NOT ADD_TEL_SUBMODE(TLM_SUBMODE, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to insert telemetry ' + $
			'submode change'
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
	IF N_RESOURCE GT 0 THEN BEGIN
	    PRINT, '-- Updating resources'
	    IF NOT ADD_RESOURCE(RESOURCE, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to insert resource'
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
	IF N_NRT_RESERVED GT 0 THEN BEGIN
	    PRINT, '-- Updating NRT reserved times'
	    IF NOT ADD_NRT_RES(NRT_RESERVED, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to insert NRT reserved time'
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
	IF N_DELAYED_CMD GT 0 THEN BEGIN
	    PRINT, '-- Updating Delayed commanding times'
	    IF NOT ADD_INST_DLYD(DELAYED_CMD, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to insert Delayed commanding time'
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
	IF N_OTHER_OBS GT 0 THEN BEGIN
	    PRINT, '-- Updating other observatories'
	    IF NOT ADD_OTHER_OBS(OTHER_OBS, ERRMSG=ERRMSG) THEN BEGIN
		MESSAGE = 'Unable to insert item for ' + $
			'another observatory'
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
;  Purge the appropriate databases.
;
        IF NOT KEYWORD_SET(NOPURGE) THEN BEGIN
         PRINT, '-- Purging databases'
	 IF NOT PRG_PLAN    (ERRMSG=MESSAGE) THEN GOTO, HANDLE_ERROR
	 IF NOT PRG_SOHO_DET(ERRMSG=MESSAGE) THEN GOTO, HANDLE_ERROR
        ENDIF
;
;  Parse the filename to determine the version number, assuming a filename of
;  the form ECSyyyymmddvvv.KAP.
;
	VERS = KAP_VERS(FILENAME)
	IF VERS GE 0 THEN BEGIN
	    CUR_VERS = LAST_KAP_VERS(START_TIME)
	    IF NOT SET_KAP_VERS(START_TIME, VERS) THEN BEGIN
		MESSAGE = 'Unable to change version from ' + TRIM(CUR_VERS) + $
			' to ' + TRIM(VERS) + ' for date ' + START_TIME
;		GOTO, HANDLE_ERROR
	    ENDIF
	ENDIF
;
;  Signal success.
;
	RESULT = 1
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF UNIT GT 0 THEN FREE_LUN, UNIT
	IF N_ELEMENTS(ERRMSG) EQ 0 THEN MESSAGE, MESSAGE, /CONTINUE	$
		ELSE IF ERRMSG EQ '' THEN ERRMSG = 'READ_KAP: ' + MESSAGE
	RESULT = 0
;
;  Return whether successful or not.
;
FINISH:	
	RETURN, RESULT
	END
