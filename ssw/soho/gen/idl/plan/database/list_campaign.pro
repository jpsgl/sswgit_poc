	PRO LIST_CAMPAIGN, START, EEND, CAMPAIGNS, N_FOUND, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	LIST_CAMPAIGN
;
; Purpose     :	List all the SoHO campaigns for a date range.
;
; Explanation :	Extracts all the SoHO campaign entries which intersect an
;		input time range.
;
; Use         :	LIST_CAMPAIGN, START, END, CAMPAIGNS, N_FOUND
;
; Inputs      :	START, END = The range of date/time values to use in searching
;			     the database.  Can be in any standard CDS time
;			     format.
;
; Opt. Inputs :	None.
;
; Outputs     :	CAMPAIGNS = A structure variable containing the following tags
;			    for each flag campaign period:
;
;			CMP_NO	 = Unique identifier number.
;			CMP_NAME = Name of the campaign.
;			CMP_TYPE = Campaign type, e.g. JOP, Intercal, etc.
;			DATE_OBS = Starting date for the observing campaign
;			DATE_END = Ending data for the campaign
;			OBSERVER = Observer in overall charge of the campaign
;
;		N_FOUND	= Number of campaigns found.
;
; Opt. Outputs:	None.
;
; Keywords    :
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       LIST_CAMPAIGN, ERRMSG=ERRMSG, ...
;                       IF ERRMSG NE '' THEN ...
;
;
; Calls       :	DATATYPE, DBOPEN, DBFIND, DBEXT, DBCLOSE, TRIM
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	If the number of campaigns found is zero, then the output
;		parameter CAMPAIGNS is not modified.
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 26 July 1994
;
; Modified    :	Version 1.0, William Thompson, GSFC, 27 July 1994
;               Version 2, Liyun Wang, GSFC/ARC, September 22, 1994
;                  Added the keyword ERRMSG.
;		Version 3, William Thompson, GSFC, 21 November 1994
;			Modified so that items which only touch requested range
;			are excluded.
;		Version 4, 23-Sep-1996, William Thompson, GSFC
;			Added campaign type.
;
; Version     :	Version 4, 23-Sep-1996
;-
;
	ON_ERROR, 2
;
;  Check the number of parameters.
;
        IF N_PARAMS() LT 4 THEN BEGIN
           MESSAGE = 'Syntax:  LIST_CAMPAIGN, START, END, CAMPAIGNS, N_FOUND'
           IF N_ELEMENTS(ERRMSG) NE 0 THEN BEGIN
              ERRMSG = MESSAGE
              RETURN
           ENDIF ELSE MESSAGE, MESSAGE
        ENDIF
;
;  Check the input parameters.
;
	N_FOUND = 0
	IF N_ELEMENTS(START) NE 1 THEN BEGIN
           MESSAGE = 'START must be a scalar'
           IF N_ELEMENTS(ERRMSG) NE 0 THEN $
              ERRMSG = MESSAGE $
           ELSE MESSAGE, MESSAGE, /CONTINUE 
           GOTO, FINISH
	END ELSE IF N_ELEMENTS(EEND) NE 1 THEN BEGIN
           MESSAGE = 'END must be a scalar'
           IF N_ELEMENTS(ERRMSG) NE 0 THEN $
              ERRMSG = MESSAGE $
           ELSE MESSAGE, MESSAGE, /CONTINUE 
           GOTO, FINISH
	ENDIF
;
;  Convert the input dates to TAI format.
;
	IF DATATYPE(START,1) EQ 'Double' THEN TAI_START = START ELSE	$
		TAI_START = UTC2TAI(START)
	IF DATATYPE(EEND,1) EQ 'Double' THEN TAI_END = EEND ELSE	$
		TAI_END = UTC2TAI(EEND)
;
;  Open the campaign database.
;
	DBOPEN, 'campaign'
;
;  Find all the entries in the requested time range.
;
	ENTRIES = DBFIND('DATE_END>' + TRIM(TAI_START,'(F15.3)') +	$
		',DATE_OBS<' + TRIM(TAI_END,'(F15.3)'), /SILENT)
	IF !ERR EQ 0 THEN BEGIN
		N_FOUND = 0
		GOTO, FINISH
	END ELSE BEGIN
		ENTRIES = ENTRIES(UNIQ(ENTRIES))
		N_FOUND = N_ELEMENTS(ENTRIES)
	ENDELSE
;
;  Extract the requested entries, sorted by start times.
;
	ENTRIES = DBSORT(ENTRIES,'date_obs')
	DBEXT, ENTRIES, 'cmp_no,cmp_name,cmp_type,date_obs,date_end,observer',$
		CMP_NO, CMP_NAME, CMP_TYPE, DATE_OBS, DATE_END, OBSERVER
;
;  Remove any entries that only touch the requested time range.
;
	W = WHERE((DATE_END NE TAI_START) AND (DATE_OBS NE TAI_END), N_FOUND)
	IF N_FOUND EQ 0 THEN GOTO, FINISH
;
;  Define the structure that the data will be returned in.
;
	CAMPAIGNS = {CMP_NO: 0,		$
		CMP_NAME: 'String',	$
		CMP_TYPE: 'String',	$
		DATE_OBS: 0.0D0,	$
		DATE_END: 0.0D0,	$
		OBSERVER: 'String'}
;
	IF N_FOUND EQ 1 THEN BEGIN
		CAMPAIGNS.CMP_NO    = CMP_NO(W(0))
		CAMPAIGNS.CMP_NAME  = CMP_NAME(W(0))
		CAMPAIGNS.CMP_TYPE  = CMP_TYPE(W(0))
		CAMPAIGNS.DATE_OBS  = DATE_OBS(W(0))
		CAMPAIGNS.DATE_END  = DATE_END(W(0))
		CAMPAIGNS.OBSERVER  = OBSERVER(W(0))
	END ELSE BEGIN
		CAMPAIGNS = REPLICATE(CAMPAIGNS, N_FOUND)
		CAMPAIGNS.CMP_NO    = CMP_NO(W)
		CAMPAIGNS.CMP_NAME  = CMP_NAME(W)
		CAMPAIGNS.CMP_TYPE  = CMP_TYPE(W)
		CAMPAIGNS.DATE_OBS  = DATE_OBS(W)
		CAMPAIGNS.DATE_END  = DATE_END(W)
		CAMPAIGNS.OBSERVER  = OBSERVER(W)
	ENDELSE
;
;  Close the database and return.
;
FINISH:
	DBCLOSE
;
	RETURN
	END
