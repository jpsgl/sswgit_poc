	PRO LIST_PLAN, START, EEND, OBS, N_FOUND, INSTRUMENT=INSTRUMENT, $
               ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	LIST_PLAN
;
; Purpose     :	List the SoHO science plan for a given date range.
;
; Explanation :	Extracts all the SoHO science plan entries which intersect an
;		input time range.
;
; Use         :	LIST_PLAN, START, END, OBS, N_FOUND
;
; Inputs      :	START, END = The range of date/time values to use in searching
;			     the database.  Can be in any standard CDS time
;			     format.
;
; Opt. Inputs :	None.
;
; Outputs     :	OBS	= A structure variable containing the following tags
;			  for each planned observation:
;
;			STRUCT_TYPE  = The character string 'SOHO-PLAN-LIST'.
;			INSTRUME     = Single letter code specifying the
;				       instrument.
;			SCI_OBJ      = Science objective from the daily science
;				       meeting
;			SCI_SPEC     = Specific science objective from meeting
;			NOTES	     = Further notes about the observation
;			START_TIME   = Date/time of beginning of observation,
;				       in TAI format
;			END_TIME     = Date/time of end of observation, in TAI
;				       format
;			OBJECT	     = Code for object planned to be observed
;			OBJ_ID	     = Object identification
;			PROG_ID	     = Program ID, linking one or more studies
;				       together
;			CMP_NO	     = Campaign number
;			XCEN	     = Center(s) of instrument FOV along X
;				       axis, given as a character string.
;			YCEN	     = Center(s) of instrument FOV along Y
;				       axis, given as a character string.
;			DISTURBANCES = Description of any disturbances
;
;		N_FOUND	= Number of planned observations found.
;
; Opt. Outputs:	None.
;
; Keywords    :	
;      INSTRUMENT = If passed, then the name or code value for the
;       	    instrument to retrieve the plan for.  Otherwise,
;		    the plans for all the instruments are retrieved.
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       LIST_PLAN, ERRMSG=ERRMSG, ... 
;                       IF ERRMSG NE '' THEN ...
;
;
; Calls       :	DATATYPE, DBOPEN, DBFIND, DBEXT, DBCLOSE, TRIM, GET_INSTRUMENT
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	If the number of planned observations found is zero, then the
;		output parameter OBS is not modified.
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 26 July 1994
;
; Modified    :	Version 1, William Thompson, GSFC, 3 August 1994
;               Version 2, Liyun Wang, GSFC/ARC, September 22, 1994
;                  Added the keyword ERRMSG.
;		Version 3, William Thompson, GSFC, 21 November 1994
;			Modified so that items which only touch requested range
;			are excluded.
;		Version 4, William Thompson, GSFC, 26 January 1995
;			Added tag STRUCT_TYPE
;		Version 5, William Thompson, GSFC, 8 May 1995
;			Modified to pay attention to DELETED field in database
;		Version 8, William Thompson, GSFC, 18 August 1995
;			Added tags XCEN, YCEN
;
; Version     :	Version 8, 18 August 1995
;-
;
	ON_ERROR, 2
;
;  Check the number of parameters.
;
	N_FOUND = 0
        IF N_PARAMS() LT 4 THEN BEGIN
           MESSAGE = 'Syntax:  LIST_PLAN, START, END, OBS, N_FOUND'
	   GOTO, HANDLE_ERROR
        ENDIF
;
;  Check the input parameters.
;
	IF N_ELEMENTS(START) NE 1 THEN BEGIN
           MESSAGE = 'START must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(EEND) NE 1 THEN BEGIN
           MESSAGE = 'END must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  If passed, then interpret the INSTRUMENT keyword parameter.
;
	IF N_ELEMENTS(INSTRUMENT) GT 1 THEN BEGIN
           MESSAGE = 'INSTRUMENT must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(INSTRUMENT) NE 0 THEN BEGIN
		IF DATATYPE(INSTRUMENT,1) NE 'String' THEN BEGIN
                   MESSAGE = 'INSTRUMENT must be a character string'
                   GOTO, HANDLE_ERROR
		ENDIF
		GET_INSTRUMENT, INSTRUMENT, INS_DESC
		IF INS_DESC.CODE EQ '' THEN BEGIN
                   MESSAGE = 'Instrument '+INSTRUMENT+' not recognized'
                   GOTO, HANDLE_ERROR
		ENDIF
	ENDIF
;
;  Convert the input dates to TAI format.
;
	IF DATATYPE(START,1) EQ 'Double' THEN TAI_START = START ELSE	$
		TAI_START = UTC2TAI(START)
	IF DATATYPE(EEND,1) EQ 'Double' THEN TAI_END = EEND ELSE	$
		TAI_END = UTC2TAI(EEND)
;
;  Open the science plan database.
;
	DBOPEN, 'sci_plan'
;
;  Find all the entries in the requested time range.
;
	TEST = 'END_TIME>' + TRIM(TAI_START,'(F15.3)') + ',START_TIME<' + $
		TRIM(TAI_END,'(F15.3)') + ',DELETED=N'
	IF N_ELEMENTS(INSTRUMENT) EQ 1 THEN TEST = TEST + ',INSTRUME=' + $
		INS_DESC.CODE
	ENTRIES = DBFIND(TEST, /SILENT)
	IF !ERR EQ 0 THEN BEGIN
		N_FOUND = 0
		GOTO, FINISH
	END ELSE BEGIN
		ENTRIES = ENTRIES(UNIQ(ENTRIES))
		N_FOUND = N_ELEMENTS(ENTRIES)
	ENDELSE
;
;  Extract the requested entries, sorted by start times.
;
	ENTRIES = DBSORT(ENTRIES,'start_time')
	DBEXT, ENTRIES, 'instrume,sci_obj,sci_spec,notes,start_time,end_time',$
		INSTRUME, SCI_OBJ, SCI_SPEC, NOTES, START_TIME, END_TIME
	DBEXT, ENTRIES, 'object,obj_id,prog_id,cmp_no,xcen,ycen,disturbances',$
		OBJECT, OBJ_ID, PROG_ID, CMP_NO, XCEN, YCEN, DISTURBANCES
;
;  Remove any entries that only touch the requested time range.
;
	W = WHERE((END_TIME NE TAI_START) AND (START_TIME NE TAI_END), N_FOUND)
	IF N_FOUND EQ 0 THEN GOTO, FINISH
;
;  Define the structure that the data will be returned in.
;
	OBS = { STRUCT_TYPE: 'SOHO-PLAN-LIST',	$
		INSTRUME: 'S',		$
		SCI_OBJ: 'String',	$
		SCI_SPEC: 'String',	$
		NOTES: 'String',	$
		START_TIME: 0.0D0,	$
		END_TIME: 0.0D0,	$
		OBJECT: 'String',	$
		OBJ_ID: 'String',	$
		PROG_ID: 0,		$
		CMP_NO: 0,		$
		XCEN: 'String',		$
		YCEN: 'String',		$
		DISTURBANCES: 'String'}
;
	IF N_FOUND EQ 1 THEN BEGIN
		OBS.INSTRUME	= INSTRUME(W(0))
		OBS.SCI_OBJ	= SCI_OBJ(W(0))
		OBS.SCI_SPEC	= SCI_SPEC(W(0))
		OBS.NOTES	= NOTES(W(0))
		OBS.START_TIME	= START_TIME(W(0))
		OBS.END_TIME	= END_TIME(W(0))
		OBS.OBJECT	= OBJECT(W(0))
		OBS.OBJ_ID	= OBJ_ID(W(0))
		OBS.PROG_ID	= PROG_ID(W(0))
		OBS.CMP_NO	= CMP_NO(W(0))
		OBS.XCEN	= XCEN(W(0))
		OBS.YCEN	= YCEN(W(0))
		OBS.DISTURBANCES= DISTURBANCES(W(0))
	END ELSE BEGIN
		OBS = REPLICATE(OBS, N_FOUND)
		OBS.INSTRUME	= INSTRUME(W)
		OBS.SCI_OBJ	= SCI_OBJ(W)
		OBS.SCI_SPEC	= SCI_SPEC(W)
		OBS.NOTES	= NOTES(W)
		OBS.START_TIME	= START_TIME(W)
		OBS.END_TIME	= END_TIME(W)
		OBS.OBJECT	= OBJECT(W)
		OBS.OBJ_ID	= OBJ_ID(W)
		OBS.PROG_ID	= PROG_ID(W)
		OBS.CMP_NO	= CMP_NO(W)
		OBS.XCEN	= XCEN(W)
		OBS.YCEN	= YCEN(W)
		OBS.DISTURBANCES= DISTURBANCES(W)
	ENDELSE
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'LIST_PLAN: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the database and return.
;
FINISH:
	DBCLOSE
;
	RETURN
	END
