	PRO READ_KAP_ITEM, UNIT, TYPE, ITEM, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS/SUMER
;
; Name        :	READ_KAP_ITEM
;
; Purpose     :	Reads an item from a KAP
;
; Explanation :	Reads the next recognizable item from an ECS Keyword-formatted
;		Activity Plan (KAP) file.  The file must have been opened first
;		with OPEN_KAP.  If the end of the file is reached, the file is
;		closed automatically.
;
; Use         :	READ_KAP_ITEM, UNIT, TYPE, ITEM
;
;		OPEN_KAP, UNIT, FILENAME
;		TYPE = 'Test'
;		WHILE TYPE NE '' THEN BEGIN
;			READ_KAP_ITEM, UNIT, TYPE, ITEM
;			...
;		ENDWHILE
;
; Inputs      :	UNIT	 = Unit number from OPEN_KAP
;
; Opt. Inputs :	None.
;
; Outputs     :	TYPE	 = The type of item read in.  If nothing was found,
;			   then this will be the null string.
;		ITEM	 = Structure containing the item read in.  The format
;			   of this structure will depend on the type of item.
;
; Opt. Outputs:	None.
;
; Keywords    :	ERRMSG   = If defined and passed, then any error messages will
;			   be returned to the user in this parameter rather
;			   than being handled by the IDL MESSAGE utility.  If
;			   no errors are encountered, then a null string is
;			   returned.  In order to use this feature, the string
;			   ERRMSG must be defined first, e.g.,
;
;				ERRMSG = ''
;				READ_KAP_ITEM, UNIT, TYPE, ITEM, ERRMSG=ERRMSG
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	The file must have been opened with OPEN_KAP.
;
; Side effects:	None.
;
; Category    :	Planning, science
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 16 February 1995
;
; Modified    :	Version 1, William Thompson, GSFC, 31 March 1995
;		Version 2, William Thompson, GSFC, 13 April 1995
;			Fixed bug with OTHER_OBS
;		Version 3, William Thompson, GSFC, 19 May 1995
;			Remove trailing spaces from mnemonic.
;		Version 4, William Thompson, GSFC, 22 May 1995
;			Modified so that all returned structures are named.
;		Version 5, William Thompson, GSFC, 9 August 1995
;			Modified so that STATUS is assumed to be CONFIRMED
;			unless specified otherwise.
;		Version 6, William Thompson, GSFC, 18 August 1995
;			Added tags XCEN, YCEN for SCIPLAN entries.
;		Version 7, William Thompson, GSFC, 5 February 1996
;			Added NOTES tag to KAP_PROGRAM structure.
;			Added STATUS tag to KAP_ACTIVITY structure.
;
; Version     :	Version 7, 5 February 1996
;-
;
	ON_ERROR, 2
;
;  Check the input parameters.
;
	IF N_PARAMS() NE 3 THEN BEGIN
		MESSAGE = 'Syntax:  READ_KAP_ITEM, UNIT, TYPE, ITEM'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Keep reading lines from the KAP file until a recognizable item is found.
;
	LINE = ''
	TYPE = ''
	WHILE (TYPE EQ '') AND (NOT EOF(UNIT)) DO BEGIN
		READF, UNIT, LINE
;
;  Test the line read in against the various possibilities.
;
		IF STRMID(LINE,0,7) EQ 'SCIPLAN' THEN BEGIN
			TYPE = 'SCIPLAN'
			MNEMONIC = STRMID(LINE,8,STRLEN(LINE)-8)
		END ELSE IF STRMID(LINE,0,7) EQ 'PROGRAM' THEN BEGIN
			TYPE = 'PROGRAM'
			MNEMONIC = STRMID(LINE,8,STRLEN(LINE)-8)
		END ELSE IF STRMID(LINE,0,8) EQ 'ACTIVITY' THEN BEGIN
			TYPE = 'ACTIVITY'
			MNEMONIC = STRMID(LINE,9,STRLEN(LINE)-9)
		END ELSE IF STRTRIM(LINE,0) EQ 'INST_IEE_MASTER' THEN BEGIN
			TYPE = 'INST_IEE_MASTER'
		END ELSE IF STRTRIM(LINE,0) EQ 'INST_IEE_RECEIVER' THEN BEGIN
			TYPE = 'INST_IEE_RECEIVER'
		END ELSE IF STRTRIM(LINE,0) EQ 'INST_NRT_SESSION' THEN BEGIN
			TYPE = 'INST_NRT_SESSION'
		END ELSE IF STRTRIM(LINE,0) EQ 'INST_NRT_RESERVED' THEN BEGIN
			TYPE = 'INST_NRT_RESERVED'
		END ELSE IF STRTRIM(LINE,0) EQ 'INST_DELAYED_CMD' THEN BEGIN
			TYPE = 'INST_DELAYED_CMD'
		END ELSE IF STRTRIM(LINE,0) EQ 'INST_TSTOL_EXECUTION' THEN $
				BEGIN
			TYPE = 'INST_TSTOL_EXECUTION'
		END ELSE IF STRMID(LINE,0,11) EQ 'DSN_Contact' THEN BEGIN
			TYPE = 'DSN_CONTACT'
			MNEMONIC = STRMID(LINE,12,STRLEN(LINE)-12)
		END ELSE IF STRTRIM(LINE,0) EQ 'SVM_Reserved' THEN BEGIN
			TYPE = 'SVM_RESERVED'
		END ELSE IF STRTRIM(LINE,0) EQ 'Payload_Reserved' THEN BEGIN
			TYPE = 'PAYLOAD_RESERVED'
		END ELSE IF STRTRIM(LINE,0) EQ 'Throughput_RCR' THEN BEGIN
			TYPE = 'THROUGHPUT_RCR'
		END ELSE IF STRTRIM(LINE,0) EQ 'Throughput_NoRCR' THEN BEGIN
			TYPE = 'THROUGHPUT_NORCR'
		END ELSE IF STRTRIM(LINE,0) EQ 'Spacecraft_Maneuver' THEN BEGIN
			TYPE = 'SPACECRAFT_MANEUVER'
		END ELSE IF STRTRIM(LINE,0) EQ 'Clock_Adjust' THEN BEGIN
			TYPE = 'CLOCK_ADJUST'
		END ELSE IF STRTRIM(LINE,0) EQ 'TLM_Tape_Dump' THEN BEGIN
			TYPE = 'TLM_TAPE_DUMP'
		END ELSE IF STRTRIM(LINE,0) EQ 'TLM_MDI_M' THEN BEGIN
			TYPE = 'TLM_MDI_M'
		END ELSE IF STRTRIM(LINE,0) EQ 'TLM_MDI_H' THEN BEGIN
			TYPE = 'TLM_MDI_H'
		END ELSE IF STRTRIM(LINE,0) EQ 'TLM_Mode' THEN BEGIN
			TYPE = 'TLM_MODE'
		END ELSE IF STRMID(LINE,0,11) EQ 'TLM_Submode' THEN BEGIN
			TYPE = 'TLM_SUBMODE'
			SUBMODE = FIX(STRMID(LINE,12,1))
		END ELSE IF STRMID(LINE,0,9) EQ 'Other_Obs' THEN BEGIN
			TYPE = 'OTHER_OBS'
			MNEMONIC = STRMID(LINE,10,STRLEN(LINE)-10)
		ENDIF
	ENDWHILE
;
;  Remove any trailing spaces from the mnemonic.
;
	IF N_ELEMENTS(MNEMONIC) EQ 1 THEN MNEMONIC = STRTRIM(MNEMONIC, 0)
;
;  If the end-of-file was reached, then simply close the file and return.
;
	IF TYPE EQ '' THEN BEGIN
		FREE_LUN, UNIT
		RETURN
	ENDIF
;
;  Define the structure that goes with the input type, and set the default
;  values.  Note that the default value for INSTRUME in 'X' because if the
;  instrument is not specified, then the item probably refers to the ECS.
;
	CASE TYPE OF
		'SCIPLAN': ITEM = {KAP_SCIPLAN,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$
				INSTRUME: 'X',		$
				SCI_OBJ: '',		$
				SCI_SPEC: '',		$
				OBJECT: '',		$
				OBJ_ID: '',		$
				NOTES: '',		$
				PROG_ID: 0,		$
				CMP_NO: 0,		$
				XCEN: '',		$
				YCEN: '',		$
				DISTURBANCES: '',	$
				DATE_MOD: 0.0D0,	$
				MNEMONIC: MNEMONIC}
		'PROGRAM': ITEM = {KAP_PROGRAM,		$
				DATE_OBS: 0.0D0,	$
				DATE_END: 0.0D0,	$
				INSTRUME: 'X',		$
				OBS_PROG: '',		$
				SCI_OBJ: '',		$
				SCI_SPEC: '',		$
				OBJECT: '',		$
				OBJ_ID: '',		$
				XCEN: '',		$
				YCEN: '',		$
				ANGLE: '',		$
				IXWIDTH: '',		$
				IYWIDTH: '',		$
				NOTES: '',		$
				PROG_ID: 0,		$
				CMP_NO: 0,		$
				DISTURBANCES: '',	$
				JITTER_LIMIT: 0,	$
				MNEMONIC: MNEMONIC}
		'ACTIVITY': ITEM = {KAP_ACTIVITY,	$
				INSTRUME: 'X',		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$
				AMOUNT: 0,		$
				STATUS: 'CONFIRMED',	$
				MNEMONIC: MNEMONIC}
		'INST_IEE_MASTER': ITEM = {KAP_MASTER,	$
				INSTRUME: 'X',		$
				MSTR_TYPE: '',		$
				MSTR_START: 0.0D0,	$
				MSTR_STOP: 0.0D0,	$
				STATUS: 'CONFIRMED'}
		'INST_IEE_RECEIVER': ITEM = {KAP_RECEIVER,	$
				INSTRUME: 'X',		$
				RCVR_START: 0.0D0,	$
				RCVR_STOP: 0.0D0,	$
				STATUS: 'CONFIRMED'}
		'INST_NRT_SESSION': ITEM = {KAP_NRT_SESS,	$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$
				INSTRUME: 'X',		$
				IWS_ID: '',		$
				CMD_RATE: 0,		$
				STATUS: 'CONFIRMED'}
		'INST_NRT_RESERVED': ITEM = {KAP_NRT_RES,	$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$
				INSTRUME: 'X',		$
				CMD_RATE: 0,		$
				STATUS: 'CONFIRMED'}
		'INST_DELAYED_CMD': ITEM = {KAP_DLYD_COM,	$
				EARLIEST: 0.0D0,	$
				LATEST: 0.0D0,		$
				INSTRUME: 'X',		$
				NUM_CMDS: 0}
		'INST_TSTOL_EXECUTION': ITEM = {KAP_TSTOL_EXEC,	$
				PROC_NAME: '',		$
				EARLIEST: 0.0D0,	$
				LATEST: 0.0D0,		$
				INSTRUME: 'X',		$
				DURATION: 0}
		'DSN_CONTACT': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}
		'SVM_RESERVED': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}
		'PAYLOAD_RESERVED': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}
		'THROUGHPUT_RCR': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}
		'THROUGHPUT_NORCR': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}
		'SPACECRAFT_MANEUVER': ITEM = {KAP_SC_MAN,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$
				NOTES: ''}
		'CLOCK_ADJUST': ITEM = {KAP_CLOCK_ADJ,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$	;Placeholder
				TYPE: ''}
		'TLM_TAPE_DUMP': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}		;Placeholder
		'TLM_MDI_M': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}		;Placeholder
		'TLM_MDI_H': ITEM = {KAP_RESOURCE,	$
				RES_TYPE: TYPE,		$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0}		;Placeholder
		'TLM_MODE': ITEM = {KAP_TLM_MOD,	$
				MODE: '',		$
				START_TIME: 0.0D0}
		'TLM_SUBMODE': ITEM = {KAP_TLM_SUB,	$
				START_TIME: 0.0D0,	$
				SUBMODE: SUBMODE}
		'OTHER_OBS': ITEM = {KAP_OTHER_OBS,	$
				START_TIME: 0.0D0,	$
				END_TIME: 0.0D0,	$
				TELESCOP: '',		$
				SCI_OBJ: '',		$
				SCI_SPEC: '',		$
				OBJECT: '',		$
				OBJ_ID: '',		$
				NOTES: '',		$
				PROG_ID: 0,		$
				CMP_NO: 0,		$
				DISTURBANCES: '',	$
				DATE_MOD: 0.0D0,	$
				MNEMONIC: MNEMONIC}
	ENDCASE
;
;  Read the keywords associated with the item until either a blank line or the
;  end of the file is reached.
;
	WHILE (STRTRIM(LINE) NE '') AND (NOT EOF(UNIT)) DO BEGIN
		READF, UNIT, LINE
;
;  Parse the line to extract the keyword and the value.
;
		IF STRTRIM(LINE) NE '' THEN BEGIN
		SEP = STRPOS(LINE,'=')
		IF (SEP LE 0) OR (SEP GE STRLEN(LINE)) THEN BEGIN
			MESSAGE = 'Line is not in keyword=value format'
			GOTO, HANDLE_ERROR
		ENDIF
		KEYWORD = STRTRIM( STRMID(LINE,0,SEP), 2)
		VALUE   = STRTRIM( STRMID(LINE,SEP+1,		$
			50 < (STRLEN(LINE)-SEP-1)), 2)
;
;  Depending on the keyword, store the information in the output structure.
;
		CASE KEYWORD OF
;
;  Times are converted to TAI format.
;
			'STARTIME': IF TYPE EQ 'PROGRAM' THEN		$
				ITEM.DATE_OBS   = UTC2TAI(VALUE) ELSE	$
				ITEM.START_TIME = UTC2TAI(VALUE)
;
			'ENDTIME': IF TYPE EQ 'PROGRAM' THEN		$
				ITEM.DATE_END   = UTC2TAI(VALUE) ELSE	$
				ITEM.END_TIME   = UTC2TAI(VALUE)
;
			'MSTR_START': ITEM.MSTR_START = UTC2TAI(VALUE)
			'MSTR_STOP':  ITEM.MSTR_STOP  = UTC2TAI(VALUE)
;
			'RCVR_START': ITEM.RCVR_START = UTC2TAI(VALUE)
			'RCVR_STOP':  ITEM.RCVR_STOP  = UTC2TAI(VALUE)
;
			'EARLIEST': ITEM.EARLIEST = UTC2TAI(VALUE)
			'LATEST':   ITEM.LATEST   = UTC2TAI(VALUE)
;
			'DATE_MOD': ITEM.DATE_MOD = UTC2TAI(VALUE)
;
;  The instrument name is converted to its code value.
;
			'INSTRUME': BEGIN
				GET_INSTRUMENT, VALUE, INS
				ITEM.INSTRUME = INS.CODE
				END
;
;  The same is done with the object name.
;
			'OBJECT': BEGIN
				GET_OBJECT, VALUE, OBJ
				ITEM.OBJECT = OBJ.OBJECT
				END
;
;  Certain parameters need to be converted to integers.
;
			'AMOUNT':   ITEM.AMOUNT	  = FIX(VALUE)
			'CMD_RATE': ITEM.CMD_RATE = FIX(VALUE)
			'CMP_NO':   ITEM.CMP_NO	  = FIX(VALUE)
			'DURATION': ITEM.DURATION = FIX(VALUE)
			'PROG_ID':  ITEM.PROG_ID  = FIX(VALUE)
			'NUM_CMDS':  ITEM.NUM_CMDS = FIX(VALUE)
			'JITTER_LIMIT': ITEM.JITTER_LIMIT = FIX(VALUE)
;
;  The rest of the keywords are just taken as they are.
;
			'ANGLE':     ITEM.ANGLE		= VALUE
			'AP_ID':     ITEM.AP_ID		= VALUE
			'DISTURB':   ITEM.DISTURBANCES	= VALUE
			'IWS_ID':    ITEM.IWS_ID	= VALUE
			'IXWIDTH':   ITEM.IXWIDTH	= VALUE
			'IYWIDTH':   ITEM.IYWIDTH	= VALUE
			'MODE':	     ITEM.MODE		= VALUE
			'MSTR_TYPE': ITEM.MSTR_TYPE	= VALUE
			'NOTES':     ITEM.NOTES		= VALUE
			'OBJ_ID':    ITEM.OBJ_ID	= VALUE
			'OBS_PROG':  ITEM.OBS_PROG	= VALUE
			'ORIG_ID':   ITEM.ORIG_ID	= VALUE
			'PROC_NAME': ITEM.PROC_NAME	= VALUE
			'SCI_OBJ':   ITEM.SCI_OBJ	= VALUE
			'SCI_SPEC':  ITEM.SCI_SPEC	= VALUE
			'STATUS':    ITEM.STATUS	= VALUE
			'TELESCOP':  ITEM.TELESCOP	= VALUE
			'TYPE':	     ITEM.TYPE		= VALUE
			'XCEN':	     ITEM.XCEN		= VALUE
			'YCEN':	     ITEM.YCEN		= VALUE
			ELSE: BEGIN
				MESSAGE = 'Unrecognized keyword ' + KEYWORD
				GOTO, HANDLE_ERROR
				END
		ENDCASE
		ENDIF
	ENDWHILE
	RETURN
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) EQ 0 THEN MESSAGE, MESSAGE, /CONTINUE	$
		ELSE ERRMSG = 'READ_KAP_ITEM: ' + MESSAGE
	FREE_LUN, UNIT
	RETURN
	END
