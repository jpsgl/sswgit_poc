	PRO GET_INSTRUMENT, INSTRUMENT, DESC, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	GET_INSTRUMENT
;
; Purpose     :	Extracts a SOHO instrument name and code from the database
;
; Explanation :	This routine extracts parameters which associates a SOHO
;		instrument name with a code value.
;
; Use         :	GET_INSTRUMENT, INSTRUMENT, DESC
;
; Inputs      :	INSTRUMENT = Either the instrument name, or the one letter code
;			     value.
;
; Opt. Inputs :	None.
;
; Outputs     :	DESC = Structure containing the instrumention description.  It
;		       contains the following tags:
;
;			CODE = Single letter instrument code.
;			NAME = The name of the instrument.
;
; Opt. Outputs:	None.
;
; Keywords    :
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       GET_INSTRUMENT, ERRMSG=ERRMSG, ... 
;                       IF ERRMSG NE '' THEN ...
;
;
; Calls       :	DATATYPE, DBOPEN, DBFIND, DBEXT, DBCLOSE
;
; Common      :	Common block GET_INSTRUMENT contains all the instruments found
;		the first time this routine is called.  Subsequent calls go to
;		this common block rather than directly to the file.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning, Databases.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 26 July 1994
;
; Modified    :	Version 1.0, William Thompson, GSFC, 3 August 1994
;               Version 2, Liyun Wang, GSFC/ARC, September 22, 1994
;                  Added the keyword ERRMSG.
;		Version 3, William Thompson, GSFC, 28 April 1995
;			Returns an error string when no entry is found.
;		Version 4, William Thompson, GSFC, 22 May 1995
;			Made output a named structure.
;		Version 5, William Thompson, GSFC, 14 August 1995
;			Added common block GET_INSTRUMENT
;
; Version     :	Version 5, 14 August 1995
;-
;
	COMMON GET_INSTRUMENT, CODES, NAMES
	ON_ERROR, 2
;
;  Define a null result.  If the routine is successful, this will be updated
;  later.
;
	DESC = {INS_CODE, CODE: '', NAME: ''}
;
;  Check the number of parameters.
;
        IF N_PARAMS() NE 2 THEN BEGIN
           MESSAGE = 'Syntax:  GET_INSTRUMENT, INSTRUMENT, DESC'
	   GOTO, HANDLE_ERROR
        ENDIF
;
;  Check the input parameter INSTRUMENT.
;
	IF DATATYPE(INSTRUMENT,1) NE 'String' THEN BEGIN
           MESSAGE = 'INSTRUMENT must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(INSTRUMENT) NE 1 THEN BEGIN
           MESSAGE = 'INSTRUMENT must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  If the routine has not already been called, then open the instrument
;  database, and extract all the entries.  Store them in the common block.
;
	IF N_ELEMENTS(CODES) EQ 0 THEN BEGIN
		DBOPEN, 'instrument'
		N_FOUND = DB_INFO('entries','instrument')
		IF N_FOUND EQ 0 THEN BEGIN
			DBCLOSE
			GOTO, FINISH
		ENDIF
		ENTRIES = LINDGEN(N_FOUND) + 1
		DBEXT, ENTRIES, 'code,name', CODES, NAMES
		CODES = STRTRIM(CODES,2)
		NAMES = STRTRIM(NAMES,2)
		DBCLOSE
	ENDIF
;
;  Depending on the number of letters in INSTRUMENT, search on either the CODE
;  or NAME field.
;
	INS = STRTRIM(STRUPCASE(INSTRUMENT),2)
	IF STRLEN(INS) EQ 1 THEN BEGIN
		W = WHERE(INS EQ CODES, COUNT)
	END ELSE BEGIN
		W = WHERE(INS EQ NAMES, COUNT)
	ENDELSE
;
;  If no entries were found, then return immediately.
;
	IF COUNT EQ 0 THEN BEGIN
		MESSAGE = 'Instrument not recognized'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Define the output structure.
;
	DESC = {INS_CODE, CODE: CODES(W(0)), NAME: NAMES(W(0))}
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_INSTRUMENT: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
FINISH:
	RETURN
	END
