	FUNCTION PRG_PLAN, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	PRG_PLAN()
;
; Purpose     :	Purges old and deleted SOHO science plan records
;
; Explanation :	This routine removes all entries from the SoHO/CDS science
;		plan database which are marked for deletion.  Later, this
;		routine may be updated to age off old entries from the database
;		as well.
;
; Use         :	PRG_PLAN
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    :
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       Result = PRG_PLAN( ERRMSG=ERRMSG )
;                       IF ERRMSG NE '' THEN ...
;
; Calls       :	DATATYPE, DBOPEN, DBFIND, DBEXT, DBCLOSE, TRIM
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning, Databases.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 8 May 1995
;
; Modified    :	Version 1, William Thompson, GSFC, 8 May 1995
;
; Version     :	Version 1, 8 May 1995
;-
;
	ON_ERROR, 2
;
;  Initialize RESULT to represent non-success.  If the routine is successful,
;  this value will be updated below.
;
	RESULT = 0
;
;  Make sure that the user has privilege to write into the database.
;
	IF !PRIV LT 3 THEN BEGIN $
           MESSAGE = '!PRIV must be 3 or greater to write into the database'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  Open the database for update, and search for entries which are marked for
;  deletion.  If any are found, then delete them.
;
	DBOPEN, 'sci_plan', 1
	ENTRIES = DBFIND('DELETED=Y', /SILENT)
	IF !ERR GT 0 THEN DBDELETE, ENTRIES
;
;  Signal success.
;
	RESULT = 1
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'PRG_PLAN: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the database, and return.
;
FINISH:
	DBCLOSE
;
	RETURN, RESULT
	END
