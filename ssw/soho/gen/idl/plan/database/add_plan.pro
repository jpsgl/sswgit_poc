	FUNCTION ADD_PLAN, DEF, INSTRUMENT=INSTRUMENT, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	ADD_PLAN()
;
; Purpose     :	Adds a SoHO/CDS science plan record to the database
;
; Explanation :	This procedure takes a SoHO/CDS science plan entry and
;		adds it to the database "sci_plan".  This database contains
;		a series of such entries, making a historical list.
;
; Use         :	Result = ADD_PLAN( DEF )
;
;		IF NOT ADD_PLAN( DEF ) THEN ...
;
; Inputs      :	DEF = This is an anonymous structure containing the following
;		      tags:
;
;			SCI_OBJ      = Science objective from the daily science
;				       meeting
;			SCI_SPEC     = Specific science objective from meeting
;			NOTES	     = Further notes about the observation
;			START_TIME   = Date/time of beginning of observation,
;				       in TAI format
;			END_TIME     = Date/time of end of observation, in TAI
;				       format
;			OBJECT	     = Code for object planned to be observed
;			OBJ_ID	     = Object identification
;			PROG_ID	     = Program ID, linking one or more studies
;				       together
;			XCEN	     = Center(s) of instrument FOV along X
;				       axis, given as a character string.
;			YCEN	     = Center(s) of instrument FOV along Y
;				       axis, given as a character string.
;			CMP_NO	     = Campaign number
;			DISTURBANCES = Description of any disturbances
;
;		      It can also be an array of such structures.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    :	INSTRUMENT = Instrument to add plan entry for.  Can be passed
;			     either as the instrument name or as a single
;			     character code value.  Normally, this routine is
;			     used for adding CDS records.  However, the use of
;			     the INSTRUMENT keyword allows it to be used with
;			     the SOC planning tool.
;
;		ERRMSG	   = If defined and passed, then any error messages
;			     will be returned to the user in this parameter
;			     rather than depending on the MESSAGE routine in
;			     IDL.  If no errors are encountered, then a null
;			     string is returned.  In order to use this feature,
;			     ERRMSG must be defined first, e.g.
;
;				ERRMSG = ''
;				Result = ADD_PLAN( ERRMSG=ERRMSG, ... )
;				IF ERRMSG NE '' THEN ...
;
;
; Calls       :	DATATYPE, DBOPEN, DBBUILD, DBCLOSE, TRIM, GET_OBJECT,
;		GET_PROGRAM, GET_CAMPAIGN
;
; Common      :	None.
;
; Restrictions:	The INSTRUMENT keyword must *only* be used with the SOC
;		planning tool.  Its use disables some CDS-specific checks.
;
;		Only this routine or DEL_PLAN can be used to add or delete
;		science plan descriptions to or from the database.
;		Modifying the database by hand could corrupt its integrity.
;
;		The data types and sizes of the structure elements must match
;		the definitions in the database.
;
;		!PRIV must be 2 or greater to use this routine.
;
; Side effects:	The dates in the structure are rounded off to millisecond
;		accuracy.
;
; Category    :	Planning, Databases.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 26 July 1994
;
; Modified    :	Version 1, William Thompson, GSFC, 2 August 1994
;               Version 2, Liyun Wang, GSFC/ARC, September 22, 1994
;                  Added the keyword ERRMSG.
;		Version 3, William Thompson, GSFC, 8 November 1994
;			Modified how GET_OBJECT is called.
;		Version 4, William Thompson, GSFC, 13 December 1994
;			Modified to write times out to millisecond accuracy.
;		Version 5, William Thompson, GSFC, 20 December 1994
;			Changed OBJECT to three characters
;		Version 6, William Thompson, GSFC, 8 May 1995
;			Modified to pay attention to DELETED field in database
;		Version 7, William Thompson, GSFC, 16 May 1995
;			Added keyword INSTRUMENT
;		Version 8, William Thompson, GSFC, 22 May 1995
;			Made INS variable a named structure.
;		Version 9, William Thompson, GSFC, 29 June 1995
;			Changed so that time in structure is rounded off to
;			millisecond accuracy.
;		Version 10, William Thompson, GSFC, 30 August 1995
;			Added tags XCEN, YCEN
;			Allow input to be an array
;		Version 11, Zarro, GSFC, 14 January 1997
;			Made CMP_NO = 1 a valid ID to allow adding
;                       EIT plans
;
; Version     :	Version 11
;-
;
	ON_ERROR, 2
;
;  Initialize RESULT to represent non-success.  If the routine is successful,
;  this value will be updated below.
;
	RESULT = 0
;
;  Check the input parameters
;
        IF N_PARAMS() NE 1 THEN BEGIN
           MESSAGE = 'Syntax:  Result = ADD_PLAN(DEF)'
	   GOTO, HANDLE_ERROR
        ENDIF
;
;  Make sure that the user has privilege to write into the database.
;
	IF !PRIV LT 2 THEN BEGIN $
           MESSAGE = '!PRIV must be 2 or greater to write into the database'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  Check each of the structure components to verify that it is of the correct
;  type and size.
;
	DEF0 = DEF(0)
;
	IF DATATYPE(DEF0.SCI_OBJ,1) NE 'String' THEN BEGIN
           MESSAGE = 'Tag SCI_OBJ must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.SCI_OBJ) NE 1 THEN BEGIN
           MESSAGE = 'Tag SCI_OBJ must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.SCI_OBJ)) GT 50 THEN BEGIN $
           MESSAGE = 'Tag SCI_OBJ must be 50 characters or less'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.SCI_SPEC,1) NE 'String' THEN BEGIN
           MESSAGE = 'Tag SCI_SPEC must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.SCI_SPEC) NE 1 THEN BEGIN
           MESSAGE = 'Tag SCI_SPEC must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.SCI_SPEC)) GT 50 THEN BEGIN
           MESSAGE = 'Tag SCI_SPEC must be 50 characters or less'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.NOTES,1) NE 'String' THEN BEGIN
           MESSAGE = 'Tag NOTES must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.NOTES) NE 1 THEN BEGIN
           MESSAGE = 'Tag NOTES must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.NOTES)) GT 50 THEN BEGIN
           MESSAGE = 'Tag NOTES must be 50 characters or less'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.START_TIME,1) NE 'Double' THEN BEGIN
           MESSAGE = 'Tag START_TIME must be a double precision number'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.START_TIME) NE 1 THEN BEGIN
           MESSAGE = 'Tag START_TIME must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.END_TIME,1) NE 'Double' THEN BEGIN
           MESSAGE = 'Tag END_TIME must be a double precision number'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.END_TIME) NE 1 THEN BEGIN
           MESSAGE = 'Tag END_TIME must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.OBJECT,1) NE 'String' THEN BEGIN
           MESSAGE = 'Tag OBJECT must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.OBJECT) NE 1 THEN BEGIN
           MESSAGE = 'Tag OBJECT must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.OBJECT)) GT 3 THEN BEGIN
           MESSAGE = 'Tag OBJECT must be 3 characters or less'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.OBJ_ID,1) NE 'String' THEN BEGIN
           MESSAGE = 'Tag OBJ_ID must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.OBJ_ID) NE 1 THEN BEGIN
           MESSAGE = 'Tag OBJ_ID must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.OBJ_ID)) GT 6 THEN BEGIN
           MESSAGE = 'Tag OBJ_ID must be 6 characters or less'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.PROG_ID,1) NE 'Integer' THEN BEGIN
           MESSAGE = 'Tag PROG_ID must be a short integer'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.PROG_ID) NE 1 THEN BEGIN
           MESSAGE = 'Tag PROG_ID must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.CMP_NO,1) NE 'Integer' THEN BEGIN
           MESSAGE = 'Tag CMP_NO must be a short integer'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.CMP_NO) NE 1 THEN BEGIN
           MESSAGE = 'Tag CMP_NO must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.XCEN,1) NE 'String' THEN BEGIN
	   MESSAGE = 'Tag XCEN must be a character string'
	   GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.XCEN) NE 1 THEN BEGIN
	   MESSAGE = 'Tag XCEN must be a scalar'
	   GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.XCEN)) GT 50 THEN BEGIN
	   MESSAGE = 'Tag XCEN must be 50 characters or less'
	   GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.YCEN,1) NE 'String' THEN BEGIN
	   MESSAGE = 'Tag YCEN must be a character string'
	   GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.YCEN) NE 1 THEN BEGIN
	   MESSAGE = 'Tag YCEN must be a scalar'
	   GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.YCEN)) GT 50 THEN BEGIN
	   MESSAGE = 'Tag YCEN must be 50 characters or less'
	   GOTO, HANDLE_ERROR
	ENDIF
;
	IF DATATYPE(DEF0.DISTURBANCES,1) NE 'String' THEN BEGIN
           MESSAGE = 'Tag DISTURBANCES must be a character string'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(DEF0.DISTURBANCES) NE 1 THEN BEGIN
           MESSAGE = 'Tag DISTURBANCES must be a scalar'
           GOTO, HANDLE_ERROR
	END ELSE IF MAX(STRLEN(DEF.DISTURBANCES)) GT 50 THEN BEGIN
           MESSAGE = 'Tag DISTURBANCES must be 50 characters or less'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  If the INSTRUMENT keyword was passed, then make sure that the instrument
;  code is valid.
;
	IF N_ELEMENTS(INSTRUMENT) EQ 1 THEN BEGIN
	    GET_INSTRUMENT, INSTRUMENT, INS
	    IF INS.CODE EQ '' THEN BEGIN
	        MESSAGE = 'Instrument ' + INSTRUMENT + ' not found in database'
	        GOTO, HANDLE_ERROR
	    ENDIF
;
;  Otherwise, assume CDS.
;
	END ELSE BEGIN
	    INS = {INS_CODE, CODE: 'C', NAME: 'CDS'}
;
;  If implicitly CDS, then make sure that the program ID matches an entry in
;  the CDS program database.  This check is not done with the SOC planning
;  tool, even if the entry is for CDS.
;
	    FOR I = 0, N_ELEMENTS(DEF)-1 DO BEGIN
		IF DEF(I).PROG_ID NE 0 THEN BEGIN
		    GET_PROGRAM, DEF(I).PROG_ID, TEMP
		    IF TEMP.PROG_ID LE 0 THEN BEGIN
			MESSAGE = 'Program ID ' + STRTRIM(DEF(I).PROG_ID,2) + $
				' not found in database' 
			GOTO, HANDLE_ERROR
		    ENDIF
		ENDIF
	    ENDFOR
	ENDELSE
;
;  Make sure that the campaign number matches an entry in the SoHO campaign
;  database.
;
	FOR I = 0, N_ELEMENTS(DEF)-1 DO BEGIN

	    IF DEF(I).CMP_NO NE 0 THEN BEGIN
                ERR='' & ICMP=DEF(I).CMP_NO
		GET_CAMPAIGN, ICMP, TEMP,ERR=ERR
		IF (TEMP.CMP_NO LE 0) and (ICMP NE 1) THEN BEGIN
		    MESSAGE = 'Campaign number ' + STRTRIM(DEF(I).CMP_NO,2) + $
			    ' not found in database'
		    MESSAGE,MESSAGE,/CONT
		ENDIF
	    ENDIF

;
;  Make sure that the object code matches an entry in the database.
;
	    GET_OBJECT, DEF(I).OBJECT, TEMP
	    IF TEMP.OBJECT EQ '' THEN BEGIN
		MESSAGE = 'Object code ' + DEF(I).OBJECT + $
			' not found in database'
		GOTO, HANDLE_ERROR
	    ENDIF
	    IF I EQ 0 THEN OBJ = REPLICATE(TEMP,N_ELEMENTS(DEF)) ELSE	$
		OBJ(I) = TEMP
;
;  Make sure that the stop date is greater than the start date.
;
	    IF DEF(I).END_TIME LE DEF(I).START_TIME THEN BEGIN
		MESSAGE = 'The stop date must be after the start date'
		GOTO, HANDLE_ERROR
	    ENDIF
	ENDFOR
;
;  Reformat the times to millisecond accuracy.  This is necessary so that
;  the times are written out in a controlled way.
;
	DEF.START_TIME = DOUBLE( STRING( DEF.START_TIME, FORMAT='(F15.3)' ))
	DEF.END_TIME   = DOUBLE( STRING( DEF.END_TIME,   FORMAT='(F15.3)' ))
;
;  Make sure that the proposed entries don't overlap with each other.
;
	FOR I = 0, N_ELEMENTS(DEF)-1 DO BEGIN
	    W = WHERE((DEF(I).START_TIME LT DEF.END_TIME) AND	$
		    (DEF(I).END_TIME GT DEF.START_TIME), COUNT)
	    IF COUNT GT 1 THEN BEGIN
		MESSAGE = 'Proposed ' + INS.NAME + ' entries at time ' + $
			TAI2UTC(DEF(I).START_TIME,/ECS,/TR) + ' to ' +	$
			TAI2UTC(DEF(I).END_TIME,/ECS,/TR) + ' and ' +	$
			TAI2UTC(DEF(W(1)).START_TIME,/ECS,/TR) + ' to ' + $
			TAI2UTC(DEF(W(1)).END_TIME,/ECS,/TR) +		$
			' overlap each other.'
		GOTO, HANDLE_ERROR
	    ENDIF
	ENDFOR
;
;  Open the science plan database for write access.
;
	DBOPEN, 'sci_plan', 1
;
;  Make sure that the proposed entries do not overlap any of the existing
;  entries.
;
	MIN_START_TIME = MIN(DEF(W).START_TIME)
	MAX_END_TIME   = MAX(DEF(W).END_TIME)
	ENTRIES = DBFIND('END_TIME>' + TRIM(MIN_START_TIME,'(F15.3)') + $
		',START_TIME<' + TRIM(MAX_END_TIME,'(F15.3)') +	$
		',INSTRUME=' + INS.CODE + ',DELETED=N', /SILENT)
	IF !ERR EQ 0 THEN BEGIN
		N_FOUND = 0
	END ELSE BEGIN
		ENTRIES = ENTRIES(UNIQ(ENTRIES))
		N_FOUND = N_ELEMENTS(ENTRIES)
		DBEXT, ENTRIES, 'start_time,end_time', START_TIME, END_TIME
	ENDELSE
;
;  It's okay if the entries in the database only touches the time range of the
;  new data.  Start by sorting the entries by the start time, so that one only
;  has to check the first and last entry.
;
	IF N_FOUND GT 0 THEN BEGIN
		IF N_FOUND GT 1 THEN BEGIN
			S = SORT(START_TIME)
			ENTRIES    = ENTRIES(S)
			START_TIME = START_TIME(S)
			END_TIME   = END_TIME(S)
		ENDIF
		IF START_TIME(N_FOUND-1) EQ MAX_END_TIME THEN	$
			N_FOUND = N_FOUND - 1
		IF END_TIME(0) EQ MIN_START_TIME THEN N_FOUND = N_FOUND - 1
		IF N_FOUND GT 0 THEN BEGIN
		    MESSAGE = 'Definition would overlap one in the database'
		    GOTO, HANDLE_ERROR
		ENDIF
	ENDIF
;
;  Add the entries.
;
	DBBUILD, REPLICATE(INS.CODE, N_ELEMENTS(DEF)), DEF.SCI_OBJ,	$
		DEF.SCI_SPEC, DEF.NOTES, DEF.START_TIME, DEF.END_TIME,	$
		OBJ.OBJECT, DEF.OBJ_ID, DEF.PROG_ID, DEF.CMP_NO, DEF.XCEN, $
		DEF.YCEN, DEF.DISTURBANCES, REPLICATE('N', N_ELEMENTS(DEF)), $
		STATUS=STATUS
	IF STATUS EQ 0 THEN BEGIN
           MESSAGE = 'Write to sci_plan database was not successful'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  Signal success.
;
	RESULT = 1
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'ADD_PLAN: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the database, and return whether the routine was successful or not.
;
FINISH:
	DBCLOSE
;
	RETURN, RESULT
	END
