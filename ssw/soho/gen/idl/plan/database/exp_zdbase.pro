;+
; Project     : SOHO - CDS     
;                   
; Name        : EXP_ZDBASE
;               
; Purpose     : expand ZDBASE into component directories
;               
; Category    : Planning
;               
; Explanation : 
;               
; Syntax      : IDL> exp_zdbase
;    
; Examples    : 
;
; Inputs      : None
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : RESET = set to redo
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: Environment/logical ZDBASE set to expanded directories
;               
; History     : Version 1,  5-August-1997,  D M Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-            

pro exp_zdbase,reset=reset

common expand_zdbase,done
if not exist(done) then done=0

zdbase=getenv('ZDBASE')
if trim(zdbase) eq '' then return

if done eq 0 then begin
 db=str2arr(zdbase,delim=':')
 ndb=n_elements(db)
 new=''
 for i=0,ndb-1 do begin
  p=arr2str(find_all_dir(db(i)),delim=':')
  new=new+p
 endfor
 setenv,'ZDBASE='+new
 done=1
endif

return  & end

