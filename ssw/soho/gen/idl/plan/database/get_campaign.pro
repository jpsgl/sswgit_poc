	PRO GET_CAMPAIGN, CMP_NO, DEF, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	GET_CAMPAIGN
;
; Purpose     :	Extracts a SOHO campaign definition from the database
;
; Explanation :	This routine extracts parameters which defines a SOHO
;		multi-observation campaign.
;
; Use         :	GET_CAMPAIGN, CMP_NO, DEF
;
; Inputs      :	CMP_NO	= The unique campaign ID.
;
; Opt. Inputs :	None.
;
; Outputs     :	DEF = Structure containing the campaign definition.  It
;		      contains the following tags:
;
;			CMP_NO	   = Unique identifier number.  If no matches
;				     are found, then a simpler structure is
;				     returned with this set to -1.
;			CMP_NAME   = Name of the campaign.
;			CMP_DESC   = Up to five lines of text describing the
;				     campaign.
;			DATE_OBS   = Starting date for the observing campaign
;			DATE_END   = Ending data for the campaign
;			OBSERVER   = Observer in overall charge of the campaign
;			INSTITUTES = Institutions involved in the campaign.
;
;		      Optionally, there can also be another tag
;
;			COMMENT  = A list of comments pertaining to this
;				   campaign.
;
;		      INSTITUTES is also a structure with the following tags:
;
;			INSTITUT = Name of the institute
;			OBSERVER = Name of the observer at the institute
;
; Opt. Outputs:	None.
;
; Keywords    :	
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       GET_CAMPAIGN, ... ERRMSG=ERRMSG, ... 
;                       IF ERRMSG NE '' THEN ...
;
;
; Calls       :	DATATYPE, DBOPEN, DBFIND, DBEXT, DBCLOSE
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning, Databases.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 26 July 1994
;
; Modified    :	Version 1, William Thompson, GSFC, 29 July 1994
;               Version 2, Liyun Wang, GSFC/ARC, September 22, 1994
;                  Added the keyword ERRMSG.
;		Version 3, William Thompson, GSFC, 29 December 1994
;			Fixed bug when inputs are bytes.
;		Version 4, William Thompson, GSFC, 28 April 1995
;			Returns an error string when no entry is found.
;		Version 5, William Thompson, GSFC, 26 September 1995
;			Fixed bug with DBEXT always returning arrays.
;		Version 6, William Thompson, GSFC, 10 September 1996
;			Take care of case where there are no entries in the
;			institutes catalog.
;		Version 7, 23-Sep-1996, William Thompson, GSFC
;			Added campaign type.
;		Version 8, William Thompson, GSFC, 24 September 1996
;			Change COMMENTS to COMMENT.
;			Return CMP_DESC as variable number of elements
;			Fix bug with OBSERVER
;		Version 9, Zarro, GSFC, 21 November 1996
;                       Trimmed string fields and moved CMP_NO to first
;                       tag location (consistent with documentation and SUMER)
;               Version 10, Zarro, GSFC, 21 November 1996
;                       Checked for scalar COMMENT and returned as COMMENT(0)
;
; Version     :	Version 10, 21 November 1996
;-
;
	ON_ERROR, 2
;
;  Define a null result.  If the routine is successful, this will be updated
;  later.
;
	DEF = {CMP_NO: -1}
;
;  Check the number of parameters.
;
        IF N_PARAMS() NE 2 THEN BEGIN
           MESSAGE = 'Syntax:  GET_CAMPAIGN, CMP_NO, DEF'
	   GOTO, HANDLE_ERROR
        ENDIF
;
;  Check the input parameter CMP_NO.
;
	TYPE = DATATYPE(CMP_NO,2)
	IF TYPE EQ 0 THEN BEGIN
           MESSAGE = 'CMP_NO is undefined'
           GOTO, HANDLE_ERROR
	END ELSE IF TYPE GE 4 THEN BEGIN
           MESSAGE = 'CMP_NO must be an integer'
           GOTO, HANDLE_ERROR
	END ELSE IF N_ELEMENTS(CMP_NO) NE 1 THEN BEGIN
           MESSAGE = 'CMP_NO must be a scalar'
           GOTO, HANDLE_ERROR
	ENDIF
;
;  Open the database.
;
	DBOPEN, 'campaign'
;
;  Search on the CMP_NO field.
;
	ENTRIES = DBFIND('CMP_NO='+STRTRIM(LONG(CMP_NO),2), /SILENT)
;
;  If no entries were found, then return immediately.
;
	IF !ERR EQ 0 THEN BEGIN
		MESSAGE = 'Campaign ID ' + TRIM(LONG(CMP_NO)) + ' not found'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Extract the relevant entry from the database.
;
	DBEXT, ENTRIES(0), 'cmp_name,cmp_type,cmp_desc,date_obs,date_end', $
		CMP_NAME, CMP_TYPE, DESC, DATE_OBS, DATE_END
	DBEXT, ENTRIES(0), 'observer,comments', OBSERVER, COMMENTS
	COMMENTS = COMMENTS(0)
;
;  Rearrange the campaign description into a series of lines.
;
	CMP_DESC = STRARR(5)
	FOR I = 0,4 DO CMP_DESC(I) = STRTRIM(STRMID(DESC,80*I,80))
	LAST = 0 > MAX(WHERE(STRLEN(STRTRIM(CMP_DESC,2)) NE 0))
	IF LAST EQ 0 THEN CMP_DESC = CMP_DESC(0) ELSE	$
		CMP_DESC = CMP_DESC(0:LAST)
;
;  Read the information about the institutes.
;
	INSTITUTES = {CDS_INSTITUTE,	$
		INSTITUT: '',	$
		OBSERVER: ''}
;
	DBOPEN, 'institutes'
	ENTRIES = DBFIND('CMP_NO='+STRTRIM(LONG(CMP_NO),2), /SILENT)
	IF ENTRIES(0) NE -1 THEN BEGIN
	    ENTRIES = DBSORT(ENTRIES,'inst_ind')
	    DBEXT, ENTRIES, 'institut,observer', INSTITUT, I_OBSERVER
;
;  Define the structure for the institute information.
;
            INSTITUT=trim(INSTITUT)
            I_OBSERVER=trim(I_OBSERVER)
	    IF N_ELEMENTS(INSTITUT) EQ 1 THEN BEGIN
		    INSTITUTES.INSTITUT = INSTITUT(0)
		    INSTITUTES.OBSERVER = I_OBSERVER(0)
	    END ELSE BEGIN
		    INSTITUTES = REPLICATE(INSTITUTES, N_ELEMENTS(INSTITUT))
		    INSTITUTES.INSTITUT = INSTITUT
		    INSTITUTES.OBSERVER = I_OBSERVER
	    ENDELSE
	ENDIF
;
;  If there are comments, then read them.
;
	IF COMMENTS EQ 'Y' THEN BEGIN
		DBOPEN, 'campaign_c'
		ENTRIES = DBFIND('CMP_NO='+STRTRIM(LONG(CMP_NO),2), /SILENT)
		ENTRIES = DBSORT(ENTRIES,'comm_ind')
		DBEXT, ENTRIES, 'comment', COMMENT
	ENDIF
;
;  Define the output structure.
;

	IF COMMENTS EQ 'Y' THEN BEGIN
                IF N_ELEMENTS(COMMENT) EQ 1 THEN COMMENT=COMMENT(0)
		DEF = {	CMP_NO: CMP_NO(0),	$
                        CMP_NAME: CMP_NAME(0),	$
			CMP_TYPE: CMP_TYPE(0),	$
			CMP_DESC: CMP_DESC,	$
			DATE_OBS: DATE_OBS(0),	$
			DATE_END: DATE_END(0),	$
			OBSERVER: OBSERVER(0),	$
			INSTITUTES: INSTITUTES,	$
			COMMENT: COMMENT}
	END ELSE BEGIN
		DEF = {	CMP_NO: CMP_NO(0),	$
                        CMP_NAME: CMP_NAME(0),	$
			CMP_TYPE: CMP_TYPE(0),	$
			CMP_DESC: CMP_DESC,	$
			DATE_OBS: DATE_OBS(0),	$
			DATE_END: DATE_END(0),	$
			OBSERVER: OBSERVER(0),	$
			INSTITUTES: INSTITUTES}
	ENDELSE

;  Trim string fields

        TRIM_CAMPAIGN,DEF
	GOTO, FINISH
;
;  Error handling point.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_CAMPAIGN: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the database, and return.
;
FINISH:
	DBCLOSE
;
	RETURN
	END
