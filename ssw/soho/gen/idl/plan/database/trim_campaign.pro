;+
; Project     : SOHO-CDS
;
; Name        : TRIM_CAMPAIGN
;
; Purpose     : trim all blank strings in Campaign structure
;
; Category    : planning
;
; Explanation :
;
; Syntax      : TRIM_CAMPAIGN,CMP
;
; Examples    :
;
; Inputs      : CMP = campaign structure
;
; Opt. Inputs : None
;
; Outputs     : CMP = trimmed campaign structure
;
; Opt. Outputs:
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: CMP string fields are trimmed
;
; History     : Written 20 November 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro trim_campaign,campaign

if datatype(campaign) eq 'STC' then begin
 ntags=n_elements(tag_names(campaign))
 for i=0,ntags-1 do if datatype(campaign.(i)) eq 'STR' then campaign.(i)=trim(campaign.(i))
 inst=campaign.institutes
 if datatype(inst) eq 'STR' then campaign.institutes=trim(inst) else begin
  nis=n_elements(inst)
  for i=0,nis-1 do begin
   campaign.institutes(i).institut=trim(campaign.institutes(i).institut)
   campaign.institutes(i).observer=trim(campaign.institutes(i).observer)
  endfor
 endelse
endif

return & end
