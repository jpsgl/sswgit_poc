;+
; Project     : CDS
;
; Name        : GET_DSN
;
; Purpose     : copy latest 'dsn_merged.dat' file
;
; Category    : planning
;
; Syntax      : IDL> get_dsn
;
; Inputs      : None
;
; Outputs     : DSN = DSN string array
;
; Keywords    : None
;
; Restrictions: Unix only
;
; History     : Written 7 August 2001, D. Zarro (EITI/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro get_dsn,dsn

ldir0='/soho-archive/public/incoming/dsn'          ; default
ldir1='$CDS_DATA/plan/database/daily'              ; for CIDL
ldir2='$SSWDB/soho/cds/data/plan/database/daily'   ; for SSWIDL

case 1 of
 test_dir(ldir0,/quiet): ldir=ldir0
 test_dir(ldir1,/quiet): ldir=ldir1
 test_dir(ldir2,/quiet): ldir=ldir2
 else: ldir=get_temp_dir()
endcase
 
;-- create 'ftp' object

a=obj_new('ftp')               

;-- connect to 'soc'

a->open,'soc.nascom.nasa.gov'

;-- set copy properties (remote and local file directory)

a->setprop,/verb,rfile='dsn_merged.dat',rdir='/actplan',$
           ldir=ldir,/ascii,user='cds',pass='8cds8'

;-- copy file

a->mget

;-- clean up

obj_destroy,a

dsn_file=loc_file('dsn_merged.dat',path=ldir,count=count)
dprint,'% dsn_file: ',dsn_file
if count eq 0 then begin
 message,'"dsn_merged.dat" file not found',/cont
endif else begin
 if n_params() eq 1 then dsn=rd_ascii(dsn_file[0])
endelse

return & end

