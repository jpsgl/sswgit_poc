	FUNCTION ADD_TEL_SUBMODE, TM, ERRMSG=ERRMSG
;+
; Project     :	SOHO - CDS
;
; Name        :	ADD_TEL_SUBMODE()
;
; Purpose     :	Adds an entry to the telemetry submode database.
;
; Explanation :	This procedure adds an entry for a telemetry submode change at
;		a given time to the "telem_submode" database.
;
; Use         :	Result = ADD_TEL_SUBMODE( TM )
;
;		IF ADD_TEL_SUBMODE( TM ) EQ 0 THEN ...
;
; Inputs      :	TM = A structure variable containing the following tags:
;
;			SUBMODE	   = The telemetry submode being changed to.
;				     Can be 1, 2, 3, or 4.
;
;			START_TIME = The start time of the telemetry submode
;				     change
;
;		     It can also be an array of such structures.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a logical value representing
;		whether or not the operation was successful, where 1 is
;		successful and 0 is unsuccessful.
;
; Opt. Outputs:	None.
;
; Keywords    :	
;       ERRMSG    = If defined and passed, then any error messages will be
;                   returned to the user in this parameter rather than
;                   depending on the MESSAGE routine in IDL.  If no errors are
;                   encountered, then a null string is returned.  In order to
;                   use this feature, ERRMSG must be defined first, e.g.
;
;                       ERRMSG = ''
;                       Result = ADD_TEL_SUBMODE( ERRMSG=ERRMSG, ... )
;                       IF ERRMSG NE '' THEN ...
;
; Calls       :	DATATYPE, DBOPEN, DB_INFO, DBBUILD, DBCLOSE, DBFIND, DBMATCH
;
; Common      :	None.
;
; Restrictions:	Only this routine or CLR_TEL_SUBMODE can be used to add or
;		delete telemetry submodes to the database.  Modifying the
;		database by hand could corrupt its integrity.
;
;		The data types and sizes of the structure elements must match
;		the definitions in the database.
;
;		!PRIV must be 2 or greater to use this routine.
;
; Side effects:	None.
;
; Category    :	Planning, Databases.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 3 April 1995
;
; Modified    :	Version 1, William Thompson, GSFC, 3 April 1995
;		Version 2, William Thompson, GSFC, 22 May 1995
;			Modified to allow array inputs.
;
; Version     :	Version 2, 22 May 1995
;-
;
	ON_ERROR, 2
;
;  Check the input parameters
;
	IF N_PARAMS() NE 1 THEN BEGIN
		MESSAGE = 'Syntax:  Result = ADD_TEL_SUBMODE(TM)'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make sure that the user has privilege to write into the database.
;
	IF !PRIV LT 2 THEN BEGIN
	   MESSAGE = '!PRIV must be 2 or greater to write into the database'
	   GOTO, HANDLE_ERROR
	ENDIF
;
;  Check each of the structure components to verify that it is of the correct
;  type and size.
;
	FOR I = 0, N_ELEMENTS(TM)-1 DO BEGIN
	    IF DATATYPE(TM(I).SUBMODE,1) NE 'Integer' THEN BEGIN
	       MESSAGE = 'Tag SUBMODE must be a short integer'
	       GOTO, HANDLE_ERROR
	    END ELSE IF N_ELEMENTS(TM(I).SUBMODE) NE 1 THEN BEGIN
	       MESSAGE = 'Tag SUBMODE must be a scalar'
	       GOTO, HANDLE_ERROR
	    END ELSE IF (TM(I).SUBMODE LT 1) OR (TM(I).SUBMODE GT 4) THEN BEGIN
	       MESSAGE = 'Tag SUBMODE must be 1, 2, 3, or 4'
	       GOTO, HANDLE_ERROR
	    ENDIF
;
	    IF DATATYPE(TM(I).START_TIME,1) NE 'Double' THEN BEGIN
	       MESSAGE = 'Tag START_TIME must be a double precision number'
	       GOTO, HANDLE_ERROR
	    END ELSE IF N_ELEMENTS(TM(I).START_TIME) NE 1 THEN BEGIN
	       MESSAGE = 'Tag START_TIME must be a scalar'
	       GOTO, HANDLE_ERROR
	    ENDIF
	ENDFOR
;
;  Open the database for write access.
;
	DBOPEN, 'telem_submode', 1
;
;  Append the data to the opened database.
;
	DBBUILD, TM.START_TIME, TM.SUBMODE, STATUS=STATUS
	IF STATUS EQ 0 THEN BEGIN
	   MESSAGE = 'Write to telemetry submode database was not successful'
	   GOTO, HANDLE_ERROR
	ENDIF
;
	RESULT = 1
	GOTO, FINISH
;
;  Handle any errors encountered.
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN		$
		ERRMSG = 'ADD_TEL_SUBMODE: ' + MESSAGE ELSE MESSAGE, MESSAGE 
	RESULT = 0
;
;  Close the database, and return whether the routine was successful or not.
;
FINISH:
	DBCLOSE
;
	RETURN, RESULT
	END
