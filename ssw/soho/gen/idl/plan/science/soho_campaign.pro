;---------------------------------------------------------------------------
; Document name: SOHO_CAMPAIGN.PRO
; Created by:    Liyun Wang, NASA/GSFC, March 4, 1997
;
; Last Modified: Tue Mar  4 09:58:35 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO soho_campaign, tstart=tstart, tend=tend, file=file, noidl=noidl
;+
; PROJECT:
;       SOHO
;
; NAME:
;       SOHO_CAMPAIGN
;
; PURPOSE:
;       Make KAP style of list of SOHO campaign entries
;
; CATEGORY:
;       Planning
;
; SYNTAX:
;       soho_campaign
;
; INPUTS:
;       None required.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       An ASCII file containing detailed contents of campaign
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       TSTART - Beginning of date/time values to use in searching the
;                database. If missing, DEC-1-1956 is assumed.
;       TEND   - End of date/time values to use in searching the database.
;                If missing, Dec-31-2006 is assumed.
;       FILE   - Name of the output file, default to 'soho_campaign.txt'
;       NOIDL  - Set this keyword not to use the IDL database
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, March 4, 1997, Liyun Wang, NASA/GSFC. Written
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2
   err = ''
   IF N_ELEMENTS(tstart) NE 0 THEN BEGIN
      tstart = anytim2utc(tstart, err=err, /ecs)
      IF err NE '' THEN tstart = '1956/12/1'
   ENDIF ELSE tstart = '1956/12/1'
   err = ''
   IF N_ELEMENTS(tend) NE 0 THEN BEGIN
      tend = anytim2utc(tend, err=err, /ecs)
      IF err NE '' THEN tend = '2006/12/31'
   ENDIF ELSE tend = '2006/12/31'
   
   IF N_ELEMENTS(file) EQ 0 THEN file = 'soho_campaign.txt'
   
   IF KEYWORD_SET(noidl) THEN BEGIN
;---------------------------------------------------------------------------
;     Use SOHO campaign Perl database
;---------------------------------------------------------------------------
      loc = GETENV('SOHO_EAP')
      IF trim(loc) EQ '' THEN BEGIN
         MESSAGE, 'Env. variable SOHO_EAP for SOHO Campaign database '+$
            'not defined', /cont
         RETURN
      ENDIF
      loc = concat_dir(loc, 'campaign', /dir)
      cmp_file = concat_dir(loc, 'soho_campaign.dat')
      clook = loc_file(cmp_file, count=count)
      IF count EQ 0 THEN BEGIN
         MESSAGE, 'SOHO Campaign database file not found', /cont
         RETURN
      ENDIF
      
      OPENW, unit, file, /GET_LUN, err=err
      IF err NE 0 THEN BEGIN
         MESSAGE, 'Cannot open '+file+' for writing.', /cont
         RETURN
      ENDIF
      records = rd_ascii(clook(0))
      nrd = N_ELEMENTS(records)
      PRINT, 'Total of '+STRTRIM(nrd, 2)+' campaign entries found.'
      delim = '~'
      FOR i=0, nrd-1 DO BEGIN
         entry = str2arr(records(i), delim=delim)
         PRINTF, unit, 'CMP_NO   = '+trim(entry(0))
         PRINTF, unit, 'CMP_NAME = '+trim(entry(2))
         PRINTF, unit, 'CMP_TYPE = '+trim(entry(1))
         PRINTF, unit, 'DESCRIPT = '+STRMID(trim(entry(2)),0,68)
         PRINTF, unit, 'OBSERVER = '+trim(entry(4))
         PRINTF, unit, 'COMMENT  = '+STRMID(trim(entry(5)),0,68)
         PRINTF, unit, 'INSTITUT = '+trim(entry(6))
         PRINTF, unit, 'DATE_OBS = '+trim(entry(7))
         PRINTF, unit, 'DATA_END = '+trim(entry(8))
         PRINTF, unit, ''
      ENDFOR
      FREE_LUN, unit
      PRINT, 'Output file '+file+' created.'
      RETURN
   ENDIF
   
;---------------------------------------------------------------------------
;  Use IDL campaign database
;---------------------------------------------------------------------------
   list_campaign, tstart, tend, cmp, n, err=err
   IF n EQ 0 THEN BEGIN
      MESSAGE, 'No campaign entries found for the given period of time.',$
         /continue
      RETURN
   ENDIF
   
   PRINT, 'Total of '+STRTRIM(n, 2)+' campaign entries found.'
   tmp = SORT(cmp.cmp_no)
   cmp = cmp(tmp)
   

   OPENW, unit, file, /GET_LUN, err=err
   IF err NE 0 THEN BEGIN
      MESSAGE, 'Cannot open '+file+' for writing.', /cont
      RETURN
   ENDIF
   tags = TAG_NAMES(cmp)
   ntags = N_ELEMENTS(tags)
   FOR i=0, n-1 DO BEGIN
      tmp = cmp(i)
      FOR j=0, ntags-1 DO BEGIN
         IF datatype(tmp.(j)) EQ 'DOU' THEN $
            PRINTF, unit, strpad(tags(j), 9, /after)+'= '+$
            anytim2utc(tmp.(j), /ecs, /truncate) $
         ELSE $
            PRINTF, unit, strpad(tags(j), 9, /after)+'= '+STRTRIM(tmp.(j), 2)
      ENDFOR
      PRINTF, unit, ''
      PRINTF, unit, ''
   ENDFOR
   FREE_LUN, unit
   PRINT, 'Output file '+file+' created.'
END

;---------------------------------------------------------------------------
; End of 'SOHO_CAMPAIGN.PRO'.
;---------------------------------------------------------------------------
