;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       MK_PLAN_PRIV()
;
; PURPOSE:
;       Check to see if the user has the privilege of editing plan
;
; EXPLANATION:
;
; CALLING SEQUENCE:
;       Result = mk_plan_priv(time)
;
; TIMES:
;       TIME - can be a plan entry or a time in any valid format
;
; OPTIONAL TIMES:
;       None.
;
; OUTPUTS:
;       RESULT - 0/1 indicating if the privilege is granted
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       ERR   - any error message
;       LOCK - any LOCK file name (input only)
;       INIT  - initialize common
;       NOCHECK - don't check for LOCK file
; CALLS:
;       PRIV_ZDBASE (a dummy function for SUMER)
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Planning
;
; PREVIOUS HISTORY:
;       Written June 28, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1, created, Liyun Wang, GSFC/ARC, June 28, 1995
;       Version 2, Liyun Wang, GSFC/ARC, April 26, 1996
;          Modified such that restriction applies only to SUMER
;       Version 3, May 1, 1996, Liyun Wang, GSFC/ARC
;          Allowed editing entries passed current UT
;       Version 4, November 28, 1996, Zarro GSFC
;          Added check for database locking
;
; VERSION:
;       Version 4
;-

function mk_plan_priv,time,err=err,quiet=quiet,lock=lock,init=init,$
                      nocheck=nocheck
                                        
common mk_plan_priv,dbf

if keyword_set(init) then delvarx,dbf
status=1
err=''

if datatype(lock) eq 'STR' then lock_file=trim(lock) else lock_file=''

if which_inst() eq 'C' then begin

;-- check if this DB has been checked before

 zdbase=getenv('ZDBASE') & db_checked=0
 if datatype(dbf) eq 'STC' then begin
  dlook=where(dbf.zdbase eq zdbase,count)
  if count gt 0 then begin
   lock_file=dbf(dlook(0)).lock_file
   access=dbf(dlook(0)).access
   db_checked=1
  endif
 endif

;-- save for later use

 if not db_checked then begin
  access=call_function('priv_zdbase', /daily,quiet=quiet,err=err) 
  dbt={zdbase:zdbase,lock_file:lock_file,access:access}
  dbf=concat_struct(dbf,dbt)
 endif

 status=access

;-- check if ACCESS is allowed

 if not status then begin
  err = 'Priviledge to modify current DB denied.'
  return,status
 endif

;-- check if LOCKED

 if not keyword_set(nocheck) then begin
  status=check_lock(lock_file,err=err,/quiet)
  if not status then return,status
 endif

 if not n_params() then return,status
endif

;---------------------------------------------------------------------------
;  Remove any restriction
;---------------------------------------------------------------------------

return,1
   
;---------------------------------------------------------------------------
;  Following codes never get executed
;---------------------------------------------------------------------------
   
   status = 0
   
;---------------------------------------------------------------------------
;  Now use the PLAN_EDIT env variable to control privilege of editing plan
;  entries past current UT
;---------------------------------------------------------------------------

   chk_priv = FIX(getenv('PLAN_EDIT'))
   IF chk_priv NE 0 THEN RETURN, 1

   IF datatype(time) EQ 'STC' THEN BEGIN
      itime = get_plan_itime(time)
      ptime = time.(itime+1)
   ENDIF ELSE BEGIN
      ptime = anytim2utc(time)
      ptime = utc2tai(ptime)
   ENDELSE

;---------------------------------------------------------------------------
;  For CDS, allow user to edit past times in their personal database
;  regardless of PLAN_EDIT
;---------------------------------------------------------------------------
   IF which_inst() EQ 'C' THEN BEGIN 
;      official = 1
;      official = STRUPCASE(CALL_FUNCTION('which_zdbase')) EQ 'CDS'
;      IF NOT official THEN status = 1
      status = 1
   ENDIF

   IF NOT status THEN BEGIN
      get_utc, cur_time
      cur_time = utc2tai(cur_time)
      status = (ptime GT cur_time)
   ENDIF

   IF NOT status THEN BEGIN
      err = 'Sorry, but you cannot edit before current UT!'
      MESSAGE, err, /cont
   ENDIF
   
   RETURN, status
END

