;+
; PROJECT:
;       SOHO
;
; NAME:
;       MK_PLAN_FORM
;
; PURPOSE:
;       Make SOHO plan form in PNG format
;
; CATEGORY:
;       Utility, planning
;
; SYNTAX:
;       mk_plan_form
;
; OPTIONAL INPUTS:
;       TSTART - Time of beginning of display (in UTC format)
;
; KEYWORDS:
;       ITEMS    - String scalar, items to be plotted (separated by
;                  commas, e.g., 'CDS,SUMER,EIT,Telemetry')
;       DUR      - Time span (in days) of the plan form (default to 1 day)
;       DTYPE    - Set this keyword to plot DET plans
;       GSIZE    - String scalar, size of output PNG file, in 'mmmxnnn' format
;       REVERSE  - Set this keyword to make white background plot
;       DIR      - Output directory
;       PS       - Produce .ps file
;       HTML     - Produce HTML version
;       FORCE    - Set this keyword to force reading from DB
;       OTHER    - set to include other instruments (VIRGO, GOLF, COSTEP)
;       ERR      - error string
;
; COMMON:
;       @MK_PLAN_SHARED
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       Color table may be modified
;
; HISTORY:
;       Version 1, January 22, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, January 26, 1996, Liyun Wang, GSFC/ARC
;          Added REVERSE keyword
;       Version 3, March 4, 1996, Liyun Wang, GSFC/ARC
;          Added FORCE keyword
;       Version 4, March 25, 1996, Liyun Wang, GSFC/ARC
;          Added ASCII keyword to allow creating plan entries in ASCII format
;       Version 5, April 25, 1996, Liyun Wang, GSFC/ARC
;          Modified such that entries for CELIAS, SWAN, and ERNE appear
;             as JOP, SOC, and CAMPAIGN respectively
;       Version 6, Nov 18, 1996, Zarro, GSFC/ARC
;          Removed VIRGO, GOLF, and COSTEP from list (never used)
;       Version 7, Nov 26, 1996, Zarro, GSFC/ARC
;          Added check for write permission for PNG file and ERR keyword
;       Version 8, August 20, 1997, Liyun Wang, NASA/GSFC
;          Removed ASCII and HTML keywords making HTML the only output format
;       Version 9, Nov 27, 2000, Zarro (EIT/GSFC)
;          Switched to PNG output
;       Version 10, Feb 20, 2001, Zarro (EITI/GSFC)
;          Added JPEG option
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
PRO mk_plan_form, tstart, dur=dur, dtype=dtype, items=items, direc=direc, $
                  gsize=gsize, file=file, reverse=reverse, ps=ps, $
                  force=force, other=other,err=err,html=html,$
                  png=png

err=''
@mk_plan_shared
   COMMON mk_soho_html, custom_stc

   ON_ERROR, 2
   IF N_ELEMENTS(file) EQ 0 THEN $
      file = 'plan_form'
   IF N_ELEMENTS(direc) EQ 0 THEN BEGIN
      direc = concat_dir(GETENV('SYNOP_DATA'),'.images/plan_form',/dir)
      IF NOT chk_dir(direc) THEN CD, curr=direc
   endif

;-- check for write permission

   out_jpeg = concat_dir(direc, file+'.jpeg')
   if not test_open(out_jpeg,/write,err=err) then return

   IF KEYWORD_SET(ps) THEN out_ps = concat_dir(direc, file+'.ps')
   if keyword_set(html) then out_html = concat_dir(direc, file+'.html') 
   
   sav_db=chklog('ZDBASE')

;---------------------------------------------------------------------------
;  Make sure we are reading the official SOHO database
;---------------------------------------------------------------------------

   IF NOT fix_zdbase(/soho,err=err) THEN RETURN

   IF N_ELEMENTS(gsize) EQ 0 THEN BEGIN
      xsize = 800
      ysize = 600
   ENDIF ELSE BEGIN
      sz = str2arr(gsize, 'x')
      xsize = sz(0)
      ysize = sz(1)
   ENDELSE

   one_day = 24.d*3600.d

   IF N_ELEMENTS(dur) EQ 0 THEN dur = 1.0
   span = dur*one_day

   IF N_ELEMENTS(sdur) EQ 0 THEN sdur = span
   IF sdur NE span THEN BEGIN
      sdur = span
   ENDIF

   IF N_ELEMENTS(tstart) NE 0 THEN BEGIN
      tstart = utc2tai(tstart)
   ENDIF ELSE BEGIN
      day = anytim2utc(!stime)
      day.time = 0
      tstart = utc2tai(day)
   ENDELSE
   tstop = tstart+sdur

   IF N_ELEMENTS(startdis) EQ 0 THEN startdis = tstart
   IF N_ELEMENTS(stopdis) EQ 0 THEN stopdis = tstop

   IF N_ELEMENTS(items) EQ 0 THEN BEGIN
;    refs = ['Command', 'Support', 'CELIAS', 'ERNE']
     refs = ['Command','CELIAS']
    if not keyword_set(other) then $
     exclude=['CELIAS', 'SWAN','ERNE','GOLF','VIRGO','COSTEP'] else $
      exclude=['CELIAS', 'SWAN','ERNE'] 
    refs=[refs,get_soho_inst(exclude=exclude)]
   ENDIF ELSE BEGIN
      refs = str2arr(items, ',')
   ENDELSE
   
;---------------------------------------------------------------------------
;  Make conversion for JOP, SOC, and CAMPAIGN
;---------------------------------------------------------------------------
   clook = WHERE(strupcase(refs) EQ 'JOP', cnt)
   IF cnt EQ 1 THEN refs(clook(0)) = 'CELIAS'
   clook = WHERE(strupcase(refs) EQ 'SOC', cnt)
   IF cnt EQ 1 THEN refs(clook(0)) = 'SWAN'
   clook = WHERE(strupcase(refs) EQ 'CAMPAIGN', cnt)
   IF cnt EQ 1 THEN refs(clook(0)) = 'ERNE'
   
   IF KEYWORD_SET(dtype) THEN dplan = 1 ELSE dplan = 0

   nrefs = N_ELEMENTS(refs)
   ops_mode = 0

   custom_stc = {startdis:tstart, stopdis:tstop, req_items:refs, $
                 show_now:0, show_day:1, show_local:0, ops_mode:ops_mode, $
                 dtype:dplan, troff:1.0, ext_cursor:0}

   mk_plan_read, custom_stc, force=keyword_set(force) 

   is_data=mk_plan_exist(custom_stc)

   if (is_data) then begin

;-- load colors and switch to Z-buffer

      sav_dev = !d.name
      sav_color=!p.color
      set_plot, 'z',/copy
      set_line_color
      ncolors=!d.table_size
      device,/close, set_resolution=[xsize, ysize],set_colors=ncolors
   
      !p.color=ncolors-1
      if keyword_set(reverse) then begin
       bg_color = !p.background
       !p.background = ncolors-1
       !p.color = 0
      endif

;-- plot timeline

      color_stc = get_inst_color(/stc)
      plot_splan, wind=win_index, now=custom_stc.show_now, color=color_stc, $
         plan_items=custom_stc.req_items, days=custom_stc.show_day, $
         local=custom_stc.show_local, dtype=custom_stc.dtype
      image = tvrd()
      device,/close
      tvlct, r, g, b, /get

;-- write png

      if keyword_set(png) then begin
       out_png = concat_dir(direc, file+'.png')
       if idl_release(upper=5.3,/inc) then $
        write_png,out_png,rotate(image,7),r,g,b else $
         write_png,out_png,image,r,g,b
       espawn,'chmod ug+w '+out_png,/noshell
      endif

;-- write jpeg
    
      h=mk_24bit(image,r,g,b)
      write_jpeg,out_jpeg,h,/true,quality=100
      espawn,'chmod ug+w '+out_jpeg,/noshell

      if keyword_set(reverse) then !p.background = bg_color
      set_plot, sav_dev
      !p.color=sav_color

      if keyword_set(html) then begin
       mk_soho_target,tstart,out_html,days=dur
      endif

;-- write postscript
  
      if keyword_set(ps) then begin
       sav_dev = !d.name
       set_plot, 'ps'
       color_stc = get_inst_color(/stc)
       device, file=out_ps
       plot_splan, wind=win_index, now=custom_stc.show_now, color=color_stc,$
           plan_items=custom_stc.req_items, days=custom_stc.show_day, $
            local=custom_stc.show_local, dtype=custom_stc.dtype
       device, /close
       set_plot, sav_dev
      endif
   endif else begin
      openw, unit, out_jpeg, /get_lun
      printf, unit, 'no entries found for requested period.'
      close, unit
      free_lun, unit
   endelse

   mklog,'zdbase',sav_db
   return
   end
