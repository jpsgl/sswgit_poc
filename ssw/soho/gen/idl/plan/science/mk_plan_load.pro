;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_PLAN_LOAD
;               
; Purpose     : load contents of MK_PLAN_SHARED common blocks
;               
; Category    : Planning
;               
; Explanation : Depending on DB and instrument type, loads PLANS array
;               into corresponding  MK_PLAN_SHARED variables
;               
; Syntax      : IDL> mk_plan_load,type,plans
;    
; Examples    : 
;
; Inputs      : TYPE  - 0,1,2,3 for PLANS, FLAG, SCIENCE, and ALT
;               PLANS - structure array with plans of requested type
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : INST = instrument to update for type 2 [def = 'C' or 'S'] 
;
; Common      : MK_PLAN_SHARED
;               
; Restrictions: This is not standalone program, but is run in conjunction with
;               MK_PLAN and MK_DETAIL for CDS and SUMER only
;               
; Side effects: Variables in MK_PLAN_SHARED are modified
;               
; History     : Version 1,  21-Jun-1995,  D M Zarro.  Written
;
; Contact     : DMZARRO
;-            

pro mk_plan_load,type,plans,inst=inst

@mk_plan_shared

;-- which instrument ('S' for SUMER, 'C' for CDS)

if not keyword_set(inst) then inst=which_inst() else $
 inst=strupcase(strmid(inst,0,1))

if exist(plans) then begin
 case type of
  0: if inst eq 'S' then sumer_detail=plans else cds_detail=plans
  1: cds_flag=plans
  2: begin
   if inst eq 'S' then sumer_science=plans 
   if inst eq 'C' then cds_science=plans
   if exist(soho_splan) then begin
    slook=where(soho_splan.instrume ne inst,scnt)
    if scnt eq 0 then soho_splan=plans else $
     soho_splan=concat_struct(soho_splan(slook),plans)
   endif else soho_splan=plans
  end
  3: cds_alt =plans
  4: begin
   if exist(soho_dplan) then begin
    slook=where(soho_dplan.instrume ne inst,scnt)
    if scnt eq 0 then soho_dplan=plans else $
     soho_dplan=concat_struct(soho_dplan(slook),plans)
   endif else soho_dplan=plans
  end

  else:  do_nothing=1
 endcase
endif else begin

 case type of 
  0: if inst eq 'S' then delvarx,sumer_detail else delvarx,cds_detail
  1: delvarx,cds_flag
  2: begin
   if inst eq 'S' then delvarx,sumer_science
   if inst eq 'C' then delvarx,cds_science
   if exist(soho_splan) then begin
    slook=where(soho_splan.instrume ne inst,scnt)
    if scnt gt 0 then soho_splan=soho_splan(slook) else delvarx,soho_splan
   endif
  end
  3: delvarx,cds_alt
  4: begin
   if exist(soho_dplan) then begin
    slook=where(soho_dplan.instrume ne inst,scnt)
    if scnt gt 0 then soho_dplan=soho_splan(slook) else delvarx,soho_dplan
   endif
  end
  else: do_nothing=1
 endcase
endelse

return & end

