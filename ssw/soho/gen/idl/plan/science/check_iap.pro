;+
; Project     : SOHO-CDS
;
; Name        : CHECK_IAP
;
; Purpose     : Check validity of campaign numbers in IAP file
;
; Category    : planning
;
; Explanation :
;
; Syntax      : CHECK_IAP,FILE
;
; Examples    :
;
; Inputs      : FILE = IAP (or KAP) file to check
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs:
;
; Keywords    : ALERT = set to send e-mail to instrument planners
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 20 November 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro check_iap,file,err=err,alert=alert,_extra=extra,wmess=wmess,missing=missing

err=''
screen=1
if datatype(wmess) eq 'STR' then begin
 if trim(wmess(0)) eq '' then screen=0
endif

;-- error checks

if datatype(file) ne 'STR' then begin
 message,'syntax: CHECK_IAP,FILE',/cont
 return
endif

chk=loc_file(file,count=count)
if count eq 0 then begin
 err='IAP/KAP file not found: '+file
 goto,quit
endif

;-- look for ZDBASE

if which_inst() eq 'C' then begin
 call_procedure,'find_zdbase',cur_db_type,status=status,/res
 if not status then begin
  err='Campaign database files not found'
  goto,quit
 endif
endif

;-- read IAP/KAP file

error=0
cont=rd_ascii(file,error=error)
if error then begin
 err='Error reading IAP/KAP file'
 goto,quit
endif

;-- user can select to skip particular plan type (e.g. SCI) by setting 
;   /nosci on command line.

if get_caller() eq 'UPDATE_KAP' then message,'checking KAP...',/cont
skip_sci=0 & skip_pro=0
if datatype(extra) eq 'STC' then begin
 skip_keys=strupcase(tag_names(extra))
 find_sci=where(strpos(skip_keys,'NOS') gt -1,pcount)
 skip_sci=(pcount gt 0)
 find_pro=where(strpos(skip_keys,'NOP') gt -1,pcount)
 skip_pro=(pcount gt 0)
endif
if skip_pro and skip_sci then begin
 err='Cannot skip both plan types'
 goto,quit
endif

;-- convert contents to structure

iap2stc,cont,stc
scount=n_elements(stc)
if scount eq 0 then begin
 err='No valid IAP/KAP entries found'
 goto,quit
endif

if skip_pro then keep=where(strpos(stc.type,'SCIPLAN_') gt -1,scount) else $
 if skip_sci then keep=where(strpos(stc.type,'PROGRAM_') gt -1,scount)
if scount eq 0 then begin
 if skip_pro then err='No valid SCIPLAN entries found' else $
  err='No valid PROGRAM entries found' 
 goto,quit
endif
if exist(keep) then stc=stc(keep)

;-- list campaigns relevant to this file

tstart=anytim2utc(stc(0).startime)
tend=anytim2utc(stc(scount-1).endtime)
tstart.time=0
tend.time=0
if tstart.mjd eq tend.mjd then tend.mjd=tend.mjd+1
list_campaign,tstart,tend,cmp,ncmp
if ncmp eq 0 then begin
 err='No campaigns scheduled during IAP/KAP file period'
 goto,quit
endif

;-- pull out participating instruments for each campaign

partic=strarr(ncmp)
for i=0,ncmp-1 do begin
 err=''
 get_campaign,cmp(i).cmp_no,def,err=err
 if err eq '' then begin
  if tag_exist(def,'INSTITUTES') then begin
   if tag_exist(def.institutes,'INSTITUT') then begin
    ilist=strmid(trim(def.institutes.institut),0,3)
    if not exist(clist) then clist=ilist else clist=[clist,ilist]
    partic(i)=trim(arr2str(trim(def.institutes.institut),delim=' '))
   endif
  endif
 endif
endfor
if exist(clist) then clist=strupcase(trim(clist))

;-- pull out entries for each instrument
;-- user can select to skip particular instrument (e.g. MDI) by setting 
;   /nomdi on command line.

do_inst=strupcase(['cds','sumer','mdi','eit','uvcs','lasco'])
nmail=n_elements(do_inst)
zmail='zarro@smmdac.nascom.nasa.gov'
mail_inst=['master@cds10.nascom.nasa.gov','planner@sumop1.nascom.nasa.gov',$
           'mdi-ops@mdisas.nascom.nasa.gov','eit@xanado.nascom.nasa.gov',$
           'michels@uvcs14.nascom.nasa.gov','planner@lasco10.nascom.nasa.gov']
mail_inst(5)=zmail
blank=strpad('',22,/after)
whoami=get_user_id()
ninst=n_elements(do_inst)
line='-----------------------------------------------------------------------'

for i=0,ninst-1 do begin
 inst=do_inst(i) & delvarx,wmess
 skip_inst=0
 if exist(skip_keys) then begin
  find_skip=where(strpos(skip_keys,strmid(inst,0,3)) gt -1,scount)
  skip_inst=(scount gt 0)
 endif
 if not skip_inst then begin
  icheck=where(strpos(stc.instrume,inst) gt -1,icount)
  if icount gt 0 then begin
   istc=stc(icheck)
   nstc=n_elements(istc)
   for k=0,nstc-1 do begin
    if (long(istc(k).cmp_no) ne 0) and (long(istc(k).cmp_no) ne 1) then begin
     clook=where(long(istc(k).cmp_no) eq long(cmp.cmp_no),count)
     if count eq 0 then begin
      warn=strpad(istc(k).type,20,/after)+' at '+istc(k).startime+' : '
      send_warn=1
      if long(istc(k).cmp_no) eq -1 then begin
       if exist(clist) then begin
        ilook=where(inst eq clist,icount)
        send_warn=(icount gt 0)
        warn2='No Campaign Number' 
       endif
      endif else begin
       warn2='Invalid Campaign Number - '+string(long(istc(k).cmp_no),'(i4)')
      endelse
      if send_warn then begin
       warn=warn+warn2
       if not exist(wmess) then wmess=warn else wmess=[wmess,warn]
      endif
     endif
    endif
   endfor

;-- check if user forgot to participate in scheduled campaign

   if not exist(wmess) then begin
    scheck=where(strpos(partic,inst) gt -1,scount)
    if scount gt 0 then begin
     ipar=cmp(scheck).cmp_no
     icheck=where_vector(long(istc.cmp_no),long(ipar),icount,rest=rest,rcount=nrest)
     if nrest gt 0 then begin
      wmess=['',"IAP file did not include the following CMP_NO's:",'',$
             string(ipar(rest)),'','which involve '+inst+'.']
     endif
    endif
   endif

   if exist(wmess) then begin
    head=[line,'Examination of the most recent '+inst+' IAP file for '+anytim2utc(stc(0).startime,/date,/vms,/ecs)]
    head=[head,'','indicates the following errors:',' ']
    tail=['','Note that the following Campaigns are scheduled around '+anytim2utc(stc(0).startime,/date,/vms,/ecs)+':']
    tail=[tail,'','CMP_NO TYPE           NAME','']
    for j=0,ncmp-1 do begin
     if trim(partic(j)) ne '' then parti='('+partic(j)+')' else parti=''
     tail=[tail,string(long(cmp(j).cmp_no),'(i4)')+'   '+$
      strpad(trim(cmp(j).cmp_type),12,/after)+'   '+trim(cmp(j).cmp_name)]
     if trim(parti) ne '' then tail=[tail,blank+parti,''] else tail=[tail,'']
    endfor
    tail=[tail,'','Please update and resubmit your IAP file.']
    tail=[tail,'','If a plan entry is not supporting a Campaign then enter:']
    tail=[tail,'','CMP_NO = 0 for the entry in the IAP file. ']
    tail=[tail,'','If you are unsure about which Campaign number to use, visit: ']
    tail=[tail,'','---> http://sohodb.nascom.nasa.gov/cgi-bin/soho_campaign_search']
    tail=[tail,line]
    wmess=[head,wmess,tail]
   endif
  endif else begin
   if keyword_set(missing) then begin
    item=''
    if skip_pro then item=' with SCI_PLAN entries ' else $
     if skip_sci then item=' with PROGRAM entries '
    wmess=[line,inst+ ' did not submit an IAP file'+item+' for '+anytim2utc(tstart,/ecs,/vms,/date),line]
   endif
  endelse
 endif

;-- send output

 if exist(wmess) then begin
  if keyword_set(alert) then begin
   mlook=where(inst eq do_inst,ic)
   if ic gt 0 then send_mail,array=wmess,add=mail_inst(mlook(0))
  endif else if screen then print,wmess
 endif

endfor

quit: 
if err ne '' then message,err,/cont
if exist(sav_db) then setenv,'ZDBASE='+sav_db

return & end
 

