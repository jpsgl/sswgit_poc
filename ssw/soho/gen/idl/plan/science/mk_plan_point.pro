;
FUNCTION MK_PLAN_POINT, plan, error=error
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       MK_PLAN_POINT()
;
; PURPOSE:
;       Shell program for making a pointing structure for use with IMAGE_TOOL
;
; EXPLANATION:
;
; CALLING SEQUENCE:
;       Result = MK_PLAN_POINT(plan)
;
; INPUTS:
;       PLAN - Structure defining a DET or SCI plan entry
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       RESULT - Pointing structure that has the following tags:
;
;          MESSENGER  - ID of widget in the caller that triggers a
;                       timer event in the planning tool to signal the
;                       completion of pointing; must be a widget that
;                       does not usually generate any event
;          INSTRUME   - Code specifying the instrument; e.g., 'C' for CDS
;          SCI_SPEC   - Science specification
;          STD_ID     - Study ID   
;          G_LABEL    - Generic label for the pointing; e.g., 'RASTER'
;          X_LABEL    - Label for X coordinate of pointing; e.g., 'INS_X'
;          Y_LABEL    - Label for Y coordinate of pointing; e.g., 'INS_Y'
;          DATE_OBS   - Date/time of beginning of observation, in TAI format
;          DO_POINTING- An integer of value 0 or 1 indicating whether pointing
;                       should be handled at the planning level (i.e., by
;                       IMAGE_TOOL); default is set to 1.
;          N_POINTINGS- Number of pointings to be performed by IMAGE_TOOL
;          POINTINGS  - A structure array (with N_POINTINGS elements) of type
;                       "DETAIL_POINT" to be handled by IMAGE_TOOL. It has
;                       the following tags:
;
;                       POINT_ID - A string scalar for pointing ID
;                       INS_X    - X coordinate of pointing area center in arcs
;                       INS_Y    - Y coordinate of pointing area center in arcs
;                       WIDTH    - Area width (E/W extent)  in arcsec
;                       HEIGHT   - Area height (N/S extent) in arcsec
;                       ZONE     - Description of the zone for pointing
;                       OFF_LIMB - An interger with value 1 or 0 indicating
;                                  whether or not the pointing area should
;                                  be off limb
;          N_RASTERS  - Number of rasters for each pointing (this is
;                       irrelevant to the SUMER)
;          RASTERS    - A structure array (N_RASTERS-element) of type
;                       "RASTER_POINT" that contains raster size and pointing
;                       information (this is irrelevant to the SUMER). It has
;                       the following tags:
;
;                       POINTING - Pointing handling code; valis
;                                  values are: 1, 0, and -1
;                       INS_X    - Together with INS_Y, the pointing to use
;                                  when user-supplied values are not
;                                  allowed.  Only valid when POINTING=0
;                                  (absolute) or POINTING=-1 (relative to
;                                  1st raster).
;                       INS_Y    - ...
;                       WIDTH    - Width (E/W extent) of the raster, in arcs
;                       HEIGHT   - Height (N/S extent) of the raster, in arcs
;
;      Note: For the case of CDS, pointings.width, pointings.height,
;            pointings.ins_x, and pointings.ins_y should match the first
;            raster's rasters.width, rasters.height, rasters.ins_x, and
;            rasters.ins_y, respectively.
;
;      If an error occurs, a -1 is returned
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       ERROR - Any error message returned; null string if no error
;
; CALLS:
;       None.
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;
; PREVIOUS HISTORY:
;       Written December 28, 1995, Liyun Wang, NASA/GSFC
;
; MODIFICATION HISTORY:
;       Version 1, Liyun Wang, NASA/GSFC, December 28, 1995
;       Version 2, March 5, 1996, Liyun Wang, NASA/GSFC
;          Made instrument specific function calls with CALL_FUNCTION
;       Version 3, May 1, 1996, Liyun Wang, NASA/GSFC
;          Added ERROR keyword
;          Fixed bug for multi-pointing SCI entries
;       Version 4, February 18, 1997, Liyun Wang, NASA/GSFC
;          Added two tags in output structure: SCI_SPEC and STD_ID
;       Version 5, March 22, 1997, Zarro, NASA/GSFC
;          Cleaned up
;       Version 6, May 20, 1998, Zarro, SAC/GSFC
;          Allowed for CDS-ALT and -FLAG plan pointings
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2
   error = ''
   IF datatype(plan) NE 'STC' THEN BEGIN
      error = 'Require a plan structure!'
      MESSAGE, error, /cont
      RETURN, -1
   ENDIF

   IF tag_exist(plan,'POINTINGS') AND tag_exist(plan,'STRUCT_TYPE') THEN BEGIN
;---------------------------------------------------------------------------
;     Must be a DET plan
;---------------------------------------------------------------------------
      CASE (plan.struct_type) OF
         'CDS-DETAIL': point_stc = CALL_FUNCTION('get_cds_point',plan)
         'CDS-FLAG': point_stc = CALL_FUNCTION('get_cds_point',plan)
         'CDS-ALT': point_stc = CALL_FUNCTION('get_cds_point',plan)
         'SUMER-DETAIL': point_stc = CALL_FUNCTION('get_sumer_point',plan)
         ELSE: BEGIN
            error = 'Unsupported plan structure type: '+plan.struct_type
            MESSAGE, error, /continue
            RETURN, -1
         END
      ENDCASE
   ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;     Must be a non-DET plan; Number of pointings is determined by
;     XCEN, YCEN, IXWIDTH, and IYWIDTH, which are all given as string scalars
;---------------------------------------------------------------------------
      IF NOT tag_exist(plan,'XCEN') THEN BEGIN
         error = 'Invalid SCI plan structure!'
         MESSAGE, error, /cont
         RETURN, -1
      ENDIF

      n_pointings = N_ELEMENTS(str2arr(plan.xcen,',')) > $
         N_ELEMENTS(str2arr(plan.ycen,','))
      mk_point_stc, point_stc, n_pointings=n_pointings

      instrume = which_inst()
      type=get_plan_type(plan)
      point_stc.instrume = 'O'
      IF instrume NE '' THEN point_stc.instrume = instrume
      IF (type eq 2) or (type eq 4) then point_stc.instrume=plan.instrume
      itime = get_plan_itime(plan)
      IF tag_exist(plan, 'sci_obj') THEN $
         point_stc.sci_spec = plan.sci_obj $
      ELSE $
       point_stc.sci_spec = 'Untitled'
      point_stc.date_obs = plan.(itime)
      point_stc.g_label = 'INDEX'
      point_stc.x_label = 'XCEN'
      point_stc.y_label = 'YCEN'

      FOR i=0, n_pointings-1 DO $
         point_stc.pointings(i).point_id = '  '+STRTRIM(i,2)

      IF n_pointings GT 1 THEN BEGIN
         IF tag_exist(plan, 'IXWIDTH') THEN BEGIN
            point_stc.do_pointing = 0
            point_stc.pointings(*).width = FLOAT(str2arr(plan.ixwidth, ','))
         ENDIF ELSE $
            point_stc.pointings(*).width = 100
         IF tag_exist(plan, 'IYWIDTH') THEN BEGIN
            point_stc.do_pointing = 0
            point_stc.pointings(*).height = FLOAT(str2arr(plan.iywidth, ','))
         ENDIF ELSE $
            point_stc.pointings(*).height = 100
         xbuff=fltarr(n_pointings)
         xbuff(0)=FLOAT(str2arr(plan.xcen, ','))
         point_stc.pointings(*).ins_x = xbuff
         ybuff=fltarr(n_pointings)
         ybuff(0)=FLOAT(str2arr(plan.ycen, ','))
         point_stc.pointings(*).ins_y =ybuff
      ENDIF ELSE BEGIN
         IF tag_exist(plan, 'IXWIDTH') THEN BEGIN
            point_stc.do_pointing = 0
            point_stc.pointings.width = FLOAT(plan.ixwidth)
         ENDIF ELSE $
            point_stc.pointings.width = 100
         IF tag_exist(plan, 'IYWIDTH') THEN BEGIN
            point_stc.do_pointing = 0
            point_stc.pointings.height = FLOAT(plan.iywidth)
         ENDIF ELSE $
            point_stc.pointings.height = 100
         if trim(plan.xcen) eq '' then plan.xcen=0.
         if trim(plan.ycen) eq '' then plan.ycen=0.
         point_stc.pointings.ins_x = FLOAT(plan.xcen)
         point_stc.pointings.ins_y = FLOAT(plan.ycen)
      ENDELSE
   ENDELSE

   RETURN, point_stc
END

;---------------------------------------------------------------------------
; End of 'mk_plan_point.pro'.
;---------------------------------------------------------------------------
