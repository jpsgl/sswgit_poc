;+
; Project     :	SOHO - CDS
;
; Name        : MK_PLAN_CUSTOM 
;
; Purpose     : widget interface for customizing MK_DETAIL 
;
; Use         : MK_PLAN_CUSTOM ,S, GROUP=GROUP, MODAL=MODAL
;
; Inputs      : See output
;
; Opt. Inputs : None.
;
; Outputs     : S - Structure that has following tags:
;                   STARTDIS, STOPDIS - the selected TAI start and stop times
;                   REQ_ITEMS  - string array of selected DB items
;                   SHOW_NOW   - Flag (1/0) to show current UT
;                   SHOW_DAY   - Flag (1/0) to show day boundary
;                   SHOW_LOCAL - Flag (1/0) to show local time
;                   SHOW_NTT   - Flag (1/0) to highlight Non-time-tagged items
;                   DTYPE      - SOHO plan type to show. 0: SCI, 1: DET
;                   TROFF      - Cursor time roundoff, in minutes
;                   NOCOLOR    - 1/0 color off/on 
;
; Opt. Outputs: None.
;
; Keywords    : 
;               GROUP = widget id of calling widget program
;               MODAL = freeze calling program
;               STATUS = 0 if user quit and wants to abort
;               NOTIME = prevent customizing time parameters
;               
; Explanation : User can customize features such as START/STOP times,
;               which DB entry to EDIT (e.g. DETAILS, FLAG...) or 
;               display as a reference (e.g. DSN, MDI, CDS, SUMER...)
;
; Calls       : None.
;
; Common      : MK_PLAN_CUSTOM 
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Planning, widgets
;
; Prev. Hist. : None.
;
; Written     :	Zarro (ARC/GSFC) 20 April 1995
;               
; Modified    : version 1.1, Zarro (ARC/GSFC) 11 Nov 1995 -- added /NOTIME
;
; Version     : 1.1
;-

   pro mk_plan_custom_event, event

   on_error, 1

   widget_control, event.top, get_uvalue=unseen
   info=get_pointer(unseen,/no_copy)
   if datatype(info) ne 'STC' then return

   widget_control, event.id, get_uvalue=uvalue
   if not exist(uvalue) then uvalue=''
   uvalue = STRTRIM(uvalue, 2)
   
;-- check time strings

   if not info.notime then begin
    widget_control, info.wstartdis, get_value=tstring
    err = ''
    time = utc2tai(tstring, err=err)
    if err ne '' then begin
     xack, err, group=event.top, /modal
    endif else begin
     info.startdis = time
     info.stopdis = time+info.sdur
     widget_control,info.wstartdis,set_value=tai2utc(info.startdis,/ecs,/vms)
     widget_control, info.wstopdis, set_value=tai2utc(info.stopdis, /ecs,/vms)
    endelse

    widget_control, info.wstopdis, get_value=tstring
    err = ''
    time = utc2tai(tstring, err=err)
    if err ne '' then begin
     xack, err, group=event.top, /modal
    endif else begin
     info.stopdis = time
     info.startdis = time-info.sdur
     widget_control,info.wstartdis,set_value=tai2utc(info.startdis,/ecs,/vms)
     widget_control, info.wstopdis, set_value=tai2utc(info.stopdis, /ecs,/vms)
    endelse

;--  Change duration of plot window

    chk = where(info.tspan_uvalue eq uvalue, cnt)
    if cnt gt  0 then begin
     info.sdur = info.sec_spans(chk(0))
     info.stopdis = info.startdis+info.sdur
     widget_control, info.wstopdis, set_value=tai2utc(info.stopdis, /ecs,/vms)
    endif

   endif

   case (uvalue) OF
      'SHOW_NOW': info.show_now = event.select
      'SHOW_NTT': info.show_ntt = event.select
      'SHOW_DAY': info.show_day = event.select
      'SHOW_LOCAL': info.show_local = event.select
      'NOCOLOR':if tag_exist(info,'NOCOLOR') then info.nocolor=event.select
      'SCIPLAN': info.dtype = 0
      'DETPLAN': info.dtype = 1
      'TIME_ROUND': info.troff = info.tround(event.index)
      else:
   endcase

;---------------------------------------------------------------------------
;  EDIT or DISPLAY
;---------------------------------------------------------------------------

   edit = strpos(uvalue, 'E_') eq 0
   ref = strpos(uvalue, 'R_') eq 0

   if edit then begin
    nrefs = n_elements(info.refs) 
    ii = where(info.curr_ed eq info.refs,count)
    if count gt 0 then widget_control,info.rbutt(ii(0)),/sensitive
    do_ed = strmid(uvalue, 2, strlen(uvalue))
    bv = grep(do_ed, info.edits, index=ii)
    if ii(0) ge 0 then begin
     info.curr_ed = info.edits(ii(0))
     ii = where(info.refs eq info.curr_ed,count)
     if count gt 0 then begin
      info.curr_idx(ii(0)) = 0
      widget_control, info.rbutt(ii(0)), sensitive=0, set_button=0
     endif
    endif
   endif

   if ref then begin
    req_uvalue = strmid(uvalue, 2, strlen(uvalue))
    ii = where(info.refs eq req_uvalue)
    if ii(0) ge 0 then begin
     i = ii(0)
     if info.curr_idx(i) ne 0 then begin
      info.curr_idx(i) = 0
     endif else begin
      info.curr_idx(i) = 1
     endelse
     widget_control, info.rbutt(i), set_button=info.curr_idx(i)
    endif
   endif

;---------------------------------------------------------------------------
;  CANCEL or ACCEPT
;---------------------------------------------------------------------------

   quit_flag = 0
   if (uvalue eq 'CANCEL') or (uvalue eq 'ACCEPT') then begin
      if uvalue eq 'CANCEL' then begin
         info.status = 0 & quit_flag=1
      endif else begin
         if info.startdis ge info.stopdis then begin
            err = 'Start time must be less than Stop time' 
            xack, err, group=event.top
         endif else quit_flag = 1
      endelse
   endif

   set_pointer,unseen,info,/no_copy
   if quit_flag then xkill, event.top
   return & end

;--------------------------------------------------------------------------- 

   pro mk_plan_custom, struct, status=status, group=group, modal=modal, $
                    font=font, err=err,notime=notime


   on_error,1

   if not have_widgets() then begin
    err='widgets unavailable'
    message,err,/cont
    return
   endif
   caller=get_caller(stat)
   if stat and (not xalive(group)) then xkill,/all

;-- default setup

   if datatype(font) eq 'STR' then font=bfont
   mk_dfont,bfont=bfont,lfont=lfont

   special=['CELIAS','SWAN','ERNE']
   fot=['Telemetry','Command']
   si=get_soho_inst(/sort,exclude=['SUMER','CDS','SWAN','CELIAS','ERNE'])
   edits = ''
   cds_plans = ['CDS-DET', 'CDS-SCI', 'CDS-FLAG', 'CDS-ALT']
   sumer_plans = ['SUMER-DET', 'SUMER-SCI']

   def_time = utc2tai(!stime) & secs_per_day=24.*3600.d

;-- which instrument?

   inst = which_inst()

   if inst eq 'C' then begin 
    edits = cds_plans
    sumer_plans = 'SUMER'
    plans=[sumer_plans,si]
    s=sort([plans])
    plans=plans(s)
    refs = [fot,special,'Support',plans,cds_plans]
   endif

   if inst eq 'S' then begin 
    edits = sumer_plans
    cds_plans = 'CDS'
    plans=[cds_plans,si]
    s=sort([plans])
    plans=plans(s)
    refs = [fot,special,'Support',plans, sumer_plans]
   endif
   nrefs = n_elements(refs) 
   
   def_struct = {startdis:def_time, stopdis:def_time+secs_per_day, $
                 show_now:0, show_day:1, show_ntt:0, show_local:1, $
                 req_items:refs, dtype:0, troff:1.0,nocolor:0}

   tspan_value = ['3 hours  ', '6 hours  ', '12 hours', '1 day  ', '1.5 days', $
                  '2 days  ', '3 days  ', '4 days  ', '5 days  ', '6 days  ', '7 days  ']
   tspan_uvalue = ['3HRS', '6HRS', '12HRS', '1DAY', '1_5DAY', '2DAY', '3DAY', $
                   '4DAY', '5DAY', '6DAY', '7DAY']
   sec_spans = [3., 6., 12., 24., 36., 48., 72., 96., 120., 144., 168.]*3600.d0

;-- check what user input

   valid_input = 0
   if datatype(struct) eq 'STC' then valid_input = tag_exist(struct,'REQ_ITEMS')
   if valid_input then stc = struct else stc = def_struct

   curr_idx = intarr(n_elements(refs))

   prev = stc.req_items 
   for i=0, n_elements(prev)-1 do begin
    ii = where(refs eq prev(i))
    if ii(0) ge 0 then curr_idx(ii(0)) = 1
   endfor
   req_items=stc.req_items
   n = n_elements(req_items) 
   curr_ed = req_items(n-1)

   wbase = widget_base(title='MK_PLAN_CUSTOM', /column)

   row1 = widget_base(wbase, /row, /frame)
   cancel = widget_button(row1, value='Cancel', uvalue='CANCEL',font=bfont,/no_rel)
   accept = widget_button(row1, value='Accept', uvalue='ACCEPT',font=bfont,/no_rel)

;-- does user want control time?

   if not keyword_set(notime) then begin
    row2 = widget_base(wbase, /column, /frame)
   
    temp = widget_base(row2, /row)
    tmp = widget_label(temp, value='Plot Start Time', font=bfont)
    wstartdis = widget_text (temp, uvalue='START_TEXT', /edit,$
                           xsize=24,font=lfont, $
                           value=anytim2utc(stc.startdis, /ecs,/vms))

    temp = widget_base(row2, /row)
    tmp = widget_label(temp, value='Plot Stop  Time', font=bfont)
    wstopdis = widget_text (temp, uvalue='STOP_TEXT', /edit, xsize=24, $
                          font=lfont,$
                          value=anytim2utc(stc.stopdis, /ecs,/vms))

    sdur = stc.stopdis-stc.startdis

    temp = widget_base(row2, /row)
    tmp = widget_label(temp, value='      Time Span', font=bfont)
    tspan = cw_bselector2(temp, tspan_value, return_uvalue=tspan_uvalue, $
                        ids=buttons,/no_rel,font=bfont)
    ii = where(sec_spans eq sdur, cnt)
    if ii(0) gt  0 then begin
      widget_control, tspan, set_value=ii(0)
    endif else begin

;---------------------------------------------------------------------------
;     Time span not compatible with any of those in the list. Default it
;     to 1 day and change stopdis accordingly
;---------------------------------------------------------------------------

       widget_control, tspan, set_value=ii(3)
       sdur = sec_spans(3)
       stc.stopdis = stc.startdis+sdur
       widget_control, wstopdis, set_value=tai2utc(doUBLE(stc.stopdis), /vms,/ecs)
    endelse
   endif else begin
    wstopdis=1 & wstartdis=1 & tspan=1 & sdur=1
   endelse

   lb = ['5 sec', '10 sec', '30 sec', '1 min', '5 min', '10 min', '30 min']
   tround = [5.0, 10., 30., 60.0, 300.0, 600.0, 1800.]/60.0

   temp = widget_base(wbase, /row, /frame)
   tmp = widget_label(temp, value='Cursor Time Roundoff ', font=bfont)
   tmp = cw_bselector2(temp, lb, /return_index, font=bfont, $
                         uvalue='TIME_ROUND',/no_rel)
   ii = where(stc.troff eq tround, cnt)
   if ii(0) gt  0 then widget_control, tmp, set_value=ii(0)
   
   temp = widget_base(wbase, column=1, /frame)

   xmenu, ['Show Current UT', 'Show Local Time Axis', 'Show Day Boundaries', $
           'Highlight Non-Time-Tagged Plans','Turn off color'], temp,font=bfont,$
      uvalue=['SHOW_NOW', 'SHOW_LOCAL', 'SHOW_DAY', 'SHOW_NTT','NOCOLOR'], $
      buttons=showb, /nonexclusive, /column


   widget_control, showb(0), set_button=stc.show_now
   widget_control, showb(1), set_button=stc.show_local
   widget_control, showb(2), set_button=stc.show_day
   widget_control, showb(3), set_button=stc.show_ntt
   if tag_exist(stc,'NOCOLOR') then $
    widget_control, showb(4), set_button=stc.nocolor else $
     widget_control,showb(4),sensitive=0

;---------------------------------------------------------------------------
;  Choose Plan type
;---------------------------------------------------------------------------

   temp = widget_base(wbase, /row, /frame)
   tmp = widget_label(temp, value='SOHO Plan Type', font=bfont)
   xmenu, ['Science', 'Detailed'], temp, uvalue=['SCIPLAN', 'DETPLAN'], $
      /exclusive, /row, buttons=ptype, /frame,font=bfont
   widget_control, ptype(stc.dtype), set_button=1

;-- instrument inputs

   row3 = widget_base(wbase, /column)

;-- reference items

   c1 = widget_base(row3, /column, /frame)
   tmp = widget_label(c1, value='Reference Items', font=bfont)

   drefs=refs
   clook=where(drefs eq 'CELIAS',cnt)
   drefs(clook(0))='JOP'
   clook=where(drefs eq 'SWAN',cnt)
   drefs(clook(0))='SOC'
   clook=where(drefs eq 'ERNE',cnt)
   drefs(clook(0))='Campaign'
   clook=where(drefs eq 'Command',cnt)
   drefs(clook(0))='DSN'

   xmenu, drefs, c1, column=4, /nonexclusive, uvalue='R_'+refs, $
          buttons=rbutt,font=bfont

;-- editable items
   
   c2 = widget_base(row3, /column, /frame)
   tmp = widget_label(c2, value='Editable Item', font=bfont)
   xmenu, edits, c2, column=4, /exclusive, uvalue='E_'+edits, buttons=ebutt,$
         /no_rel,font=bfont

   ii = where(curr_idx ne 0)
   if ii(0) ge 0 then begin
    for j=0, n_elements(ii)-1 do begin
     widget_control, rbutt(ii(j)), set_button=1
    endfor
   endif

   clook = where(refs eq curr_ed, cnt)
   if cnt gt  0 then begin
    widget_control, rbutt(clook(0)), sensitive=0, set_button=0
    curr_idx(clook(0)) = 0
   endif

   clook = where(edits eq curr_ed, cnt)
   if cnt gt  0 then widget_control, ebutt(clook(0)), /set_butt

;-- realize and position widgets

   xrealize,wbase,group=group,/center

;-- pass common info into hidden widget base

   make_pointer,unseen
   plot_info = {rbutt:rbutt, ebutt:ebutt, showb:showb, refs:refs, $
                edits:edits, wstartdis:wstartdis, wstopdis:wstopdis, $
                tspan:tspan, status:1, sdur:sdur, sec_spans:sec_spans, $
                tspan_uvalue:tspan_uvalue, curr_idx:curr_idx, $
                curr_ed:curr_ed, tround:tround,notime:keyword_set(notime)}

   info = join_struct(stc, plot_info)

   set_pointer,unseen,info,/no_copy
   widget_control, wbase, set_uvalue=unseen

   xmanager, 'mk_plan_custom', wbase, group=group,/modal
   if xalive(wbase) then xmanager
   xshow, group

;-- return customized changes

   info=get_pointer(unseen,/no_copy)
   free_pointer,unseen
   if datatype(info) ne 'STC' then status=0 else begin
    status = info.status
    drefs=info.refs
   endelse
   if status then begin
    ii = where(info.curr_idx ne 0)
    split_struct, info, 'rbutt', struct, rest
    if ii(0) ge 0 then begin
     temp = [drefs(ii), info.curr_ed]
     struct = rep_tag_value(struct, temp, 'REQ_ITEMS')
    endif else begin
     struct = rep_tag_value(struct, info.curr_ed, 'REQ_ITEMS')
    endelse
   endif

   return & end
