;+
; Project     : SOHO - CDS
;
; Name        : MK_PLAN_RECOVER
;
; Purpose     : recover from a MK_PLAN crash
;
; Category    : operations, planning
;
; Explanation : call this program as a last resort if MK_PLAN crashes 
;               inexplicably and does not recover on it's own. Run it
;               as follows:
;
;               IDL> retall
;               IDL> mk_plan_recover
;                            
;               This program will execute special versions of 'dbext'
;               and 'dbupdate' that will attempt to restore the integrity
;               of the planning databases by deleting corrupted records
;               that do not have matching index and data records in their
;               respective .dbx and .dbf files. Use with extreme caution.
;
;               In case of an unrecoverable MK_PLAN crash, it is recommended
;               that you do not exit the current IDL session. Try re-starting
;               MK_PLAN one more time. If MK_PLAN still fails, then try running
;               MK_PLAN_RECOVER. If MK_PLAN recovers successfully, then 
;               press SAVE to save the recovered entries. 
;
; Inputs      : None
;
; Outputs     : None
;
; Keywords    : RESET 
;
; History     : Written, 21-Oct-2000,  D.M. Zarro (EIT/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro mk_plan_recover,reset=reset

common mk_plan_recover,changed,old_path

if keyword_set(reset) and exist(changed) then begin
 if changed and exist(old_path) then begin
  dprint,'%resetting PATH'
  changed=0 
  mklog,'DEBUG',''
  !path=old_path
  delvarx,old_path
 endif
 return
endif

if not exist(changed) then changed=0
new_dir='/tmp_mnt/usr/users/master/zarro'
chk=strpos(!path,new_dir)
changed=chk eq 0
if changed then return

ok=test_open(new_dir)
if ok then begin
 files=loc_file('*.pro',path=new_dir,count=count)
 if count gt 0 then begin
  in_path=strpos(new_dir,!path) gt -1
  if not in_path then begin
   old_path=!path
   !path=new_dir+':'+!path
   changed=1
   dprint,'%setting PATH'
   mklog,'DEBUG','1'
  endif
 endif
endif

recompile,'dbext.pro'
recompile,'dbupdate.pro'

if get_caller() eq '' then mk_plan,/reset

return & end

