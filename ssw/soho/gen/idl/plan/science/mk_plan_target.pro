;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       MK_PLAN_TARGET
;
; PURPOSE:
;       Create an ASCII file listing all SOHO plan targets
;
; CATEGORY:
;       Planning, utility
;
; EXPLANATION:
;       This program creates ASCII files listing all SOHO planning
;       related targets from a given starting DATE over a specific
;       DURATION in days. Each file created contains SOHO plans and
;       DSN schedule for a specific operational day and is named as
;       soho_plan.YYYYMMDD, where YYYYMMDD represents the date.
;
; SYNTAX:
;       mk_plan_target [, date] [, duration=duration]
;
; INPUTS:
;       None required.
;
; OPTIONAL INPUTS:
;       DATE - Starting date (in any ECS format) on which the targets
;              to be listed; if missing, the current date is assumed
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       DURATION  - Duration in days over which the plan targets are
;                   listed. Defaults to 1 days
;       DIRECTORY - Name of the directory in which output files are created;
;                   if missing, the current IDL working directory is used
;
;       VERBOSE   - Set this keyword to get more informative prompts
;                   from the program
;
;       SCIPLAN   - print SCI_PLAN (default = DETAILS)
;
;       NOUPDATE  - set to not update from KAP
;
;       TOTERM    - print output to term
;
;       NODSN     - don't list DSN
;
;       OFILE     - output filename [if one day]
;
;       ERR       - error string
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, March 22, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, April 26, 1996, Liyun Wang, GSFC/ARC
;          Modified so that targets for CELIAS, SWAN, and ERNE appear
;             as JOP, SOC, and Campaign, respectively
;       Version 3, June 6 1996, Dominic Zarro, GSFC
;          Modified to show DETAILED pointing and FOV
;          Added call to UPDATE_KAP
;       Version 4, November 26 1996, Dominic Zarro, GSFC
;          Added file/directory write protection checks and ERR keyword
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
PRO sep_line, unit, length
   line = '-----------------------------------------------------------'+$
      '------------------'
   IF N_ELEMENTS(length) EQ 0 THEN PRINTF, unit, line ELSE $
      PRINTF, unit, STRMID(line,0,length)
END

PRO print_entry, unit, entry,inst=inst
   itime=get_plan_itime(entry)
   ind = '   '
   ind2 = ind+ind
   dur = sec2dhms(LONG(entry.(itime+1)-entry.(itime)) > 0)
   if datatype(inst) ne 'STR' then inst=''
   PRINTF, unit, ind2+                  inst+' Start: '+anytim2utc(entry.(itime), /ecs, /trun)+$
      ',  End: '+anytim2utc(entry.(itime+1), /ecs, /trun)+$
      ',  Dur: '+dur

   IF tag_exist(entry, 'SCI_OBJ') THEN BEGIN
      printf,unit,ind
      PRINTF, unit, ind2+                  'Sci Obj:    '+trim(entry.sci_obj)

      IF tag_exist(entry, 'OBJECT') THEN BEGIN
       object=strmid(trim(entry.object),0,2)
       err=''
       if object ne '' then begin
        get_object,object,desc,err=err
        if err eq '' then begin
         printf,unit,ind
         PRINTF, unit, ind2+                  'Object:     '+trim(desc.object_desc)
        endif
       endif
      endif

      IF tag_exist(entry, 'OBJ_ID') THEN BEGIN
       obj_id=trim(entry.obj_id)
       if obj_id ne '' then begin
        printf,unit,ind
        PRINTF, unit, ind2+                  'Object ID:  ',trim(entry.obj_id)
       endif
      endif

      if tag_exist(entry,'XCEN') then begin
       xpoint=str2arr(entry.xcen,delim=',')
       ypoint=str2arr(entry.ycen,delim=',')
       np=n_elements(xpoint) < n_elements(ypoint)
       printf,unit,ind
       printf,unit,ind2+'Center Coordinates           Field Of View'
       printf,unit,ind2+'     (arcsecs)                 (arcsecs)
       printf,unit,ind2+'  XCEN      YCEN            WIDTH    HEIGHT'
       xwidth=strarr(np) & ywidth=xwidth
       if tag_exist(entry,'IXWIDTH') then begin
        xwidth=str2arr(entry.ixwidth,delim=',')
        ywidth=str2arr(entry.iywidth,delim=',')
       endif
       for i=0,np-1 do $
        PRINTF, unit,'    '+string(xpoint(i),'(f8.2)')+'  '+$ 
                           string(ypoint(i),'(f8.2)')+'         '+$
                           string(xwidth(i),'(f8.2)')+'  '+$
                           string(ywidth(i),'(f8.2)')
      endif
      if tag_exist(entry,'ANGLE') then begin
       angle=str2arr(entry.angle,delim=',')
       clook=where(angle ne 0,cnt)
       if cnt gt 0 then begin
        nangle=n_elements(angle)
        printf,unit,ind2
        printf,unit,ind2,'Roll Angle relative to N (deg):   '
        for i=0,nangle-1 do $
         PRINTF, unit,ind2,'                               ',string(angle(i),'(f6.1)')
       endif
      endif


     sep_line,unit
   ENDIF
END

PRO mk_plan_target, date, duration=duration, directory=directory,$
   back=back,nodsn=nodsn,ofile=ofile,err=err,$
   toterm=toterm,verbose=verbose,sciplan=sciplan,noupdate=noupdate

   tofile=not keyword_set(toterm)
   err=''

;---------------------------------------------------------------------------
;  Make sure the SOHO planning database is used
;---------------------------------------------------------------------------

   IF NOT fix_zdbase(/soho,err=err) THEN BEGIN
      if tofile then begin
       PRINT, 'You don''t have access to the SOHO planning database.'
       PRINT, 'I depend on that database. I quit!'
      endif
      RETURN
   ENDIF

   verbose = KEYWORD_SET(verbose)
   file = 'soho_plan'
   IF N_ELEMENTS(directory) NE 0 THEN BEGIN
      IF chk_dir(directory) THEN file = concat_dir(directory, file) ELSE begin
       if tofile then begin
         PRINT, 'Sorry, directory '+directory+$
         ' does not exist. Using current IDL dir..'
       endif 
     endelse
   ENDIF

;-- define start/stop times
   
   if not exist(duration) then duration=1
   duration=(duration > 1)
   if not exist(back) then back=0
   get_utc, curr_time
   IF N_ELEMENTS(date) EQ 0 THEN BEGIN
      wdate = curr_time
   ENDIF ELSE wdate = anytim2utc(date)
   inst_list = get_soho_inst(/short)
   wdate.time = 0L
   wdate.mjd=wdate.mjd-back
   tstart = wdate
   

;---------------------------------------------------------------------------
;  Begin writing files
;---------------------------------------------------------------------------

   if not keyword_set(noupdate) then update_kap,wdate,span=duration,back=back
   FOR k=0, duration-1 DO BEGIN
      date_str = anytim2utc(wdate, /ecs, /date)
      date_code = STRMID(date_str, 0, 4)+STRMID(date_str, 5, 2)+$
         STRMID(date_str, 8, 2)
      wdate.time = 1000*86400L
      if tofile then begin
       if (duration eq 1) and datatype(ofile) eq 'STR' then $
        file_name=ofile else file_name = file+'.'+date_code
       if not test_open(file_name,/write,err=err) then return
       IF verbose THEN PRINT, 'Working on day '+date_str+'...'
      endif
      tend = wdate
      if not keyword_set(nodsn) then begin
       IF verbose and tofile THEN PRINT,'Getting DSN schedule...'
       list_dsn, tstart, tend, dsn, n_dsn
      endif else n_dsn=0
      if keyword_set(sciplan) then begin
       IF verbose and tofile THEN PRINT,'Getting Science plans...'
       list_plan, tstart, tend,details, n_sci
      endif else begin
       IF verbose and tofile THEN PRINT,'Getting Detailed plans...'
       list_soho_det, tstart, tend, details, n_sci
      endelse  


      if tofile then OPENW, unit, file_name, /GET_LUN else unit=-1
      sep_line, unit
      PRINTF, unit, 'SOHO Planning Schedule for '+date_str+$
         '       Updated at '+$
         anytim2utc(curr_time, /ecs, /truncate)+' UT'
      printf,unit,' '
      printf,unit,'Problems? Contact: dzarro@solar.stanford.edu '
      printf,unit,'                                OR '
      printf,unit,'                  lwang@achilles.nascom.nasa.gov'
      printf,unit,' ' 
      sep_line, unit

      IF (n_sci EQ 0) AND (n_dsn EQ 0) THEN BEGIN
         PRINTF, unit, ''
         PRINTF, unit, 'No targets found in DB.'
         GOTO, finish
      ENDIF
      IF n_dsn NE 0 THEN BEGIN
;---------------------------------------------------------------------------
;        Print DSN contact times
;---------------------------------------------------------------------------
         PRINTF, unit, ''
         word = 'DSN Contacts:'
         PRINTF, unit, word
         sep_line, unit, STRLEN(word)
         FOR i=0, n_dsn-1 DO print_entry, unit, dsn(i)
      ENDIF
      IF n_sci NE 0 THEN BEGIN
;---------------------------------------------------------------------------
;        Print scientific plan targets
;---------------------------------------------------------------------------
         FOR i=0, N_ELEMENTS(inst_list)-1 DO BEGIN
            ii = WHERE(details.instrume EQ inst_list(i))

            IF ii(0) GE 0 THEN BEGIN
               inst = get_soho_inst(inst_list(i))
               if keyword_set(sciplan) then begin
                CASE (inst) OF
                  'CELIAS': inst = 'JOP'
                  'SWAN': inst = 'SOC'
                  'ERNE': inst = 'Campaign'
                  ELSE: 
                ENDCASE
               endif
               plans = details(ii)
               PRINTF, unit, ''
               word = inst+':'
               PRINTF, unit, word
               sep_line, unit, STRLEN(word)
               FOR j=0, N_ELEMENTS(plans)-1 DO print_entry, unit,plans(j),inst=inst
            ENDIF ELSE BEGIN
               IF verbose and tofile THEN $
                  PRINT, 'No '+get_soho_inst(inst_list(i))+' today.'
            ENDELSE
         ENDFOR
      ENDIF

finish:
      if tofile then begin
       CLOSE, unit
       FREE_LUN, unit
       IF verbose THEN PRINT, 'Output file '+file_name+' created.'
      endif
      tstart = tend
      wdate.mjd = wdate.mjd+1L
      wdate.time = 0L
   ENDFOR
END

;---------------------------------------------------------------------------
; End of 'mk_plan_target.pro'.
;---------------------------------------------------------------------------
