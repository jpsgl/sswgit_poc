;+
; Project     : SOHO/CDS
;
; Name        : UPDATE_CDS_PLANS
;
; Purpose     : Update SOHO Plan DB with CDS Plan DB
;
; Category    : planning
;
; Syntax      : IDL> upd_cds_plans,tstart,tend
;
; Inputs      : TSTART,TEND - start,end times to update
;
; Keywords    : VERBOSE - output
;               PURGE   - purge DB
;
; History     : Written 20 Apr 2007, D. Zarro (ADNET)
;
; Contact     : dzarro@solar.stanford.edu
;-


pro update_cds_plans,tstart,tend,err=err,verbose=verbose,purge=purge,_extra=extra

common upd_cds_plans,save_db
if ~exist(save_db) then save_db=getenv('ZDBASE')

error=0
catch,error
if error ne 0 then begin
 message,err_state(),/cont
 catch,/cancel
 mklog,'ZDBASE',save_db
endif

verbose=keyword_set(verbose) 
err=''

dstart=get_def_times(tstart,tend,dend=dend,_extra=extra,/round)

if verbose then begin
 message,'processing plans between '+anytim2utc(dstart,/vms)+' and '+anytim2utc(dend,/vms),/cont
endif

;-- get CDS details 

s=fix_zdbase(/cds,err=err)
if ~s then return

list_detail,dstart,dend,plans,nplans
if nplans eq 0 then begin
 message,'no detailed plans during specified times',/cont
 return
endif

details=list_to_detail(plans)

;-- create new CDS science and details plans 

def_inst_plan,splans,type=2,/list,inst='C',rep=nplans
def_inst_plan,sdetails,type=4,/list,inst='C',rep=nplans

struct_assign,details,splans,/nozero
struct_assign,details,sdetails,/nozero

for i=0,nplans-1 do begin
 get_cds_xy,details[i],xcen,ycen,/string,/one
 get_cds_fov,details[i],width,height,/string,/one
 xcen=strmid(trim(xcen),0,49)
 ycen=strmid(trim(ycen),0,49)
 width=strmid(trim(width),0,49)
 height=strmid(trim(height),0,49)
 splans[i].xcen=xcen
 splans[i].ycen=ycen
 splans[i].start_time=details[i].date_obs
 splans[i].end_time=details[i].date_end

 sdetails[i].xcen=xcen
 sdetails[i].ycen=ycen
 sdetails[i].ixwidth=width
 sdetails[i].iywidth=height
endfor

;-- get and delete SOHO science and details plans

s=fix_zdbase(/soho,err=err)
if ~s then return

!priv=3

list_soho_det,dstart,dend,cdetails,ncds,instrum='C'
if ncds gt 0 then begin
 if verbose then message,'deleting '+trim(ncds)+' detailed plans',/cont
 for i=0,ncds-1 do s=del_soho_det(cdetails[i].date_obs,instru='C') 
endif

list_plan,dstart,dend,cplans,ncds,instrum='C'
if ncds gt 0 then begin
 if verbose then message,'deleting '+trim(ncds)+' science plans',/cont
 for i=0,ncds-1 do s=del_plan(cplans[i].start_time,instru='C') 
endif

;-- update with new CDS plans

s=upd_soho_det(sdetails)
s=upd_plan(splans)

if keyword_set(purge) then begin
 s=prg_soho_det()
 s=prg_plan()
endif

mklog,'ZDBASE',save_db

return & end




