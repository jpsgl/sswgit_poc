;+
; Project     :	SOHO - CDS
;
; Name        :	XZOOM_PLAN
;
; Purpose     :	zoom in on plot window
;
; Explanation : 
;
; Use         :	XZOOM_PLAN
;
; Inputs      : None.
;
; Opt. Inputs : None.
;
; Outputs     :	INFO =  {T1 : widget ID of start time widget
;                        T2 : widget ID of end time widget
;                        TSTART: start time
;                        TSTOP : stop time
;                        TDUR : duration of MOVE or COPY
;                        TBASE: main base ID}
; Opt. Outputs:	None.
;
; Keywords    : GROUP=      group leader
;               MESSENGER = widget ID to communicate with external event
;                           handler (must have valid child)
;               MOVE = ZOOM application to be used as a MOVE
;               COPY = ZOOM application to be used as a COPY
;               DEL =  ZOOM application to be used as a DELETE
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	May 31, 1996
;-

pro xzoom_plan_event, event

common xzoom_com,ldur,lunit

widget_control,event.top,get_uvalue=unseen
stc=get_pointer(unseen,/no_copy)
if datatype(stc) ne 'STC' then return

widget_control, event.id, get_uvalue = uvalue
if not exist(uvalue) then uvalue=''
uvalue=strtrim(uvalue,2)

;-- push main widget to foreground

if (uvalue eq 'push') then begin
 xshow,event.top
 widget_control,event.id,timer = 1
 goto,exit
endif

;-- duration unit

if uvalue eq 'dur_unit' then begin
 widget_control,event.id,get_value=dur_index
 new_dur_unit=(stc.dur_units)(dur_index)
 if new_dur_unit ne stc.dur_unit then begin
  widget_control,stc.wdur,get_value=tdur
  tdur=tdur(0)
  if trim(tdur) eq '' then tdur=0.
  tdur=float(tdur)
  new_dur=tdur*stc.dur_unit/new_dur_unit
  widget_control,stc.wdur,set_value=string(new_dur,'(g10.3)')
  stc.dur_unit=new_dur_unit
 endif
endif

;-- duration value

if xalive(stc.wdur) then begin
 widget_control,stc.wdur,get_value=tdur
 tdur=tdur(0)
 if trim(tdur) eq '' then tdur=0.
 tdur=float(tdur)
 stc.tdur=tdur*stc.dur_unit
 widget_control,stc.wdur,set_value=string(tdur,'(g10.3)')
endif

bvalue=stc.bvalues

;-- reset 

if uvalue eq bvalue(2) then begin
 widget_control,stc.(0),set_value=tai2utc(stc.ostart,/ecs,/vms)
 widget_control,stc.(1),set_value=tai2utc(stc.ostop,/ecs,/vms)
endif

;-- undo last

if uvalue eq bvalue(3) then begin
 if xalive(stc.messenger) then begin
  widget_control,stc.messenger,set_uvalue=uvalue,timer=1
 endif
endif 

;-- apply

if uvalue eq 'ops1' then good=xvalidate(stc,event,/diff)
if uvalue eq 'ops2' then good=xvalidate(stc,event,/diff)

if uvalue eq bvalue(1) then begin
 valid=xvalidate(stc,event,/diff)
 if valid then begin
  if xalive(stc.wdur) then begin
   if (stc.tdur eq 0.) then begin
    xack,'A non-zero amount is required',group=event.top,/icon
    valid=0
   endif
  endif
  if valid and xalive(stc.messenger) then begin
   widget_control,stc.messenger,set_uvalue=uvalue,timer=1
   child=widget_info(stc.messenger,/child)
   if child gt 0 then widget_control,child,set_uvalue=stc
   if xalive(stc.bundo) then widget_control,stc.bundo,/sensitive
  endif
 endif
endif

;-- cancel 

if uvalue eq bvalue(0) then begin
 if xalive(stc.group) then widget_control,stc.group,/sensitive
 xtext_reset,stc
 xkill,event.top
 if xalive(stc.messenger) then $
  widget_control,stc.messenger,set_uvalue=uvalue,timer=1
endif


exit: 
lunit=stc.dur_unit
ldur=stc.tdur
set_pointer,unseen,stc,/no_copy

return & end

;=======================================================================

pro xzoom_plan,group=group,messenger=messenger,info=info,modal=modal,$
                 tstart=tstart,tstop=tstop,move=move,copy=copy,delete=delete

common xzoom_com

if not have_widgets() then return
case 1 of
 keyword_set(move): opr=' MOVE '
 keyword_set(copy): opr=' COPY '
 keyword_set(delete): opr=' DELETE ' 
 else: opr=' ZOOM '
endcase

;-- instructions

instruct=['Use LEFT  mouse button to mark'+opr+'start time',$
          'Use RIGHT mouse button to mark'+opr+'stop time ']

;-- load fonts

mk_dfont,bfont=bfont,lfont=lfont

;-- group and messenger ID's

if not xalive(messenger) then messenger=0
caller=get_caller(stat)
if stat and (not xalive(group)) then xkill,/all
if not xalive(group) then group=0
  
;-- make base

tbase=widget_base(/column,title=' ')

for i=0,n_elements(instruct)-1 do begin
 if trim(instruct(i)) ne '' then begin
  label=widget_label(tbase,value=instruct(i),font=lfont)
 endif
endfor

;-- time widgets

trow=widget_base(tbase,/row)
t1=cw_field(trow, Title= 'Start Time:  ',value=' ',uvalue='ops1',$
                    /ret, xsize = 23, font = bfont,field=bfont)

t2=cw_field(trow,  Title='Stop Time:   ', value=' ',uvalue='ops2',$
                    /ret, xsize = 23, font = bfont,field=bfont)


;-- duration widget

day_secs=24.*3600.
dur_units=[1.0, 60.0, 3600.0,day_secs,7*day_secs]
wdur=0l & tdur=0.
if exist(ldur) then tdur=ldur
if exist(lunit) then dur_unit=lunit else dur_unit=60.

if (trim(opr) eq 'MOVE') or (trim(opr) eq 'COPY') then begin
 unit_str = strpad(['Seconds', 'Minutes', 'Hours','Days','Weeks'],7,/after)
 dbase=widget_base(tbase,/row)
 dlab=widget_label(dbase,value='Enter '+opr+' Amount: ',font=bfont)
 value=string(tdur/dur_unit,'(g10.3)')
 wdur =widget_text(dbase, value=value, xsize=10,font=bfont,$
                   /editable,uvalue='dur')
 dur_unitb = cw_bselector2(dbase, unit_str, uvalue='dur_unit', $
                             /return_index,font=bfont,/no_rel)
 ii = (where(float(dur_unit) eq dur_units))(0)
 if ii ge 0 then widget_control, dur_unitb, set_value=ii
endif

;-- button widgets

brow=widget_base(tbase,/row,space=100)
bvalues=['quit','apply','reset','undo']
svalues=strupcase(bvalues)+opr
bvalues=bvalues+'_'+strlowcase(trim(opr))
bvalues(3)='undo'

b1=widget_button(brow,value=svalues(0),uvalue=bvalues(0),font=bfont,/no_rel)
b2=widget_button(brow,value=svalues(1),uvalue=bvalues(1),font=bfont,/no_rel)
b3=widget_button(brow,value=svalues(2)+'LIMITS',uvalue=bvalues(2),font=bfont,/no_rel)

if trim(opr) ne 'ZOOM' then begin
 b4=widget_button(brow,value='UNDO LAST',uvalue=bvalues(3),font=bfont,/no_rel)
 widget_control,b4,sensitive=0
endif else b4=0l

;-- save input times in info structure

info={t1:t1,t2:t2,tstart:0.d,tstop:0.d,tbase:tbase,bvalues:bvalues,$
      wdur:wdur,tdur:tdur,dur_unit:dur_unit,dur_units:dur_units,$
      bundo:b4}

info.tstart=utc2tai(!stime)
if exist(tstart) then begin
 err=''
 wstart=anytim2utc(tstart,err=err)
 if err eq '' then info.tstart=utc2tai(wstart)
endif
wstart=tai2utc(info.tstart,/vms,/ecs)
widget_control,info.(0),set_value=wstart

info.tstop=utc2tai(!stime)
if exist(tstop) then begin
 err=''
 wstop=anytim2utc(tstop,err=err)
 if err eq '' then info.tstop=utc2tai(wstop)
endif
wstop=tai2utc(info.tstop,/vms,/ecs)
widget_control,info.(1),set_value=wstop

info=add_tag(info,info.tstart,'OSTART')
info=add_tag(info,info.tstop,'OSTOP')

;-- save group and messenger id's from caller

info=add_tag(info,messenger,'MESSENGER')
info=add_tag(info,group,'GROUP')

;-- set up timer 

new_vers=float(strmid(!version.release,0,3)) gt 3.5 

;-- realize base and modalize caller

widget_control,tbase,/realize
if new_vers then widget_control,trow, timer=1,set_uvalue='push'
if xalive(group) then widget_control,group,sensitive=0

;-- save info structure in pointer

stc=info
make_pointer,unseen
set_pointer,unseen,stc,/no_copy
widget_control,tbase,set_uvalue=unseen
xmanager,'xzoom_plan',tbase,group=group,modal=modal

if xalive(tbase) then begin
 xmanager
 stc=get_pointer(unseen,/no_copy)
 free_pointer,unseen
endif

if datatype(stc) eq 'STC' then info=stc

return & end

