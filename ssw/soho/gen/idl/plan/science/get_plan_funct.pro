;+
; Project     :	SOHO - CDS
;
; Name        :	GET_PLAN_FUNCT
;
; Purpose     : Get DEL, ADD, LIST function for a given plan structure
;
; Use         : FUNCT=GET_PLAN_FUNCT(PLAN)
;
; Inputs      : PLAN = plan structure definition 
;                        OR
;               TYPE = 0,1,2,3 for DETAIL/FLAG/SCIENCE/ALT
;
; Opt. Inputs : None.
;
; Outputs     : FUNCT= function name
;
; Opt. Outputs: None.
;
; Keywords    : DELETE for delete function (e.g. del_plan)
;               ADD for add function
;               LIST for list function
;               GET for get function
;               PRG for purge function
;               ERR = error message
;
; Explanation : None.
;
; Calls       : None.
;
; Common      : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Planning
;
; Prev. Hist. : None.
;
; Written     :	Version 1, Zarro (ARC/GSFC) 12 May 1995
;-


function get_plan_funct,plan,_extra=extra,err=err

on_error,1

err=''
err_mess='syntax --> FUNCT=GET_PLAN_FUNCT(PLAN,[/DEL,/ADD,/LIS,/PRG])'

if n_elements(plan) eq 0 then begin
 err=err_mess & message,err,/cont & return,''
endif
if datatype(plan) eq 'STC' then type=get_plan_type(plan,err=err) else type=fix(plan)
if type eq -1 then return,''

if not exist(extra) then begin
 err=err_mess
 message,err,/cont
 return,''
endif

tags=strtrim(tag_names(extra),2)
functs=['del','add','get','list','prg']
names=['detail','flag','plan','alt','soho_det']
match,strupcase(strmid(functs,0,3)),strmid(tags,0,3),p,q

if n_elements(q) gt 1 then begin
 err='only single keywords accepted'
 message,err,/cont
 return,''
endif

if q(0) eq -1 then begin
 err='unsupported keyword'
 message,err,/contin
 return,''
endif

return,functs(p(0))+'_'+names(type)

end

