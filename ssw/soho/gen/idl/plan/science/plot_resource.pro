PRO plot_resource, resource, startdis, row=row, color=color, $
                   charsize=charsize
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       PLOT_RESOURCE
;
; PURPOSE:
;       Plots any of given resource item on the resource row
;
; CATEGORY:
;       Planning, Science
;
; EXPLANATION:
;       This routine plots any of the resource items on the science
;       plan display. Currently, DSN contact times, commanding times
;       (throughput RCR), and delayed commanding times (payload_reserved)
;       should be plotted on the same row; Other resources should be
;       plotted on the other row.
;
; SYNTAX:
;       plot_resource, resource, startdis, row
;
; INPUTS:
;       RESOURCE - Array of structure that has at least the following
;                  tags:
;          RES_NAME   - Resource type (name of resource item)
;          START_TIME - The beginning times of the resource item, in TAI
;          END_TIME   - The end times of the resource item, in TAI
;
;       STARTDIS - Start time of the display, in any CDS time format
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       ROW      - row number in which the item is plotted (default: 0)
;       COLOR    - Index of color to be used for plotting (not used for DSN)
;       CHARSIZE - The character size for the text labels.  If not
;                  passed, then determined from !P.CHARSIZE.
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       PLOT_FRAME must be called first.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, August 2, 1995, Liyun Wang, GSFC/ARC. Written
;       Version 2, August 14, 1995, Liyun Wang, GSFC/ARC
;          Changed positional parameter ROW to keyword parameter
;       Version 3, November 10, 1995, Liyun Wang, GSFC/ARC
;          Changed so that a fixed of %5 distance of top subrow and
;             bottom subrow to the edge of the row is reserved
;	Version 4, William Thompson, GSFC, 8 April 1998
;		Changed !D.N_COLORS to !D.TABLE_SIZE for 24-bit displays
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 1
;---------------------------------------------------------------------------
;  Check the number of parameters
;---------------------------------------------------------------------------
   np = N_PARAMS()
   IF np LT 2 THEN BEGIN
      MESSAGE, 'Syntax:  PLOT_RESOURCE, RESOURCE, STARTDIS, ROW', /cont
      RETURN
   ENDIF
   IF N_ELEMENTS(row) EQ 0 THEN row = 0
   IF N_ELEMENTS(color) EQ 0 THEN color = !d.table_size-1

   err = ''
   datastart = anytim2utc(startdis, err=err)
   IF err NE '' THEN BEGIN
      MESSAGE, err, /cont
      RETURN
   ENDIF
   datastart.time = 0
   daystart = utc2tai(datastart) ; start of STARTDIS in TAI

   res_type = (resource.res_name)(0)
   if not tag_exist(resource,'START_TIME') then return
   dprint, 'Plotting '+res_type+'...'
   num = N_ELEMENTS(resource)
   t1 = (resource.start_time-daystart)/3600.d0
   t2 = (resource.end_time-daystart)/3600.d0

;---------------------------------------------------------------------------
;  Change number of sub rows in the resource row
;---------------------------------------------------------------------------
   num_rows = 4
   
;---------------------------------------------------------------------------
;  Sub-row width refelcts the fact that there is 5% of space reserved
;  in the bottom and top of the row
;---------------------------------------------------------------------------
   srow_wid = 0.9/num_rows

;---------------------------------------------------------------------------
;  There are three types of resources: Telemetry, commanding, and
;  ground based support. 
;---------------------------------------------------------------------------
   CASE (res_type) OF
      'TLM_MDI_M': sub_row = 0
      'TLM_MDI_H': sub_row = 1
      'TLM_TAPE_DUMP': sub_row = 2
      'DSN_CONTACT': BEGIN
         sub_row = 3
         y1 = row+0.05+sub_row*srow_wid
         y2 = y1+srow_wid
         submodes = resource.submode
         FOR i=0, num-1 DO BEGIN
            x1 = t1(i) > !x.crange(0)
            x2 = t2(i) < !x.crange(1)
;---------------------------------------------------------------------------
;           set color to match the submodes
;---------------------------------------------------------------------------
            CASE (submodes(i)) OF
               1: dcolor = 9     ; brown (?)
               2: dcolor = 7     ; purple, EIT prime
               3: dcolor = 3     ; red, CDS prime
               4: dcolor = 8     ; pink, SUMER prime
               5: dcolor = 5     ; blue, standard submode
               6: dcolor = 6     ; orange EIT/LASCO (no SUMER)
               ELSE: dcolor = !p.color  ; foreground color (usually white)
            ENDCASE
            if color eq 0 then dcolor=0
            IF x2 GT x1 THEN polyfill, [x1, x2, x2, x1], [y1, y1, y2, y2], $
               color=dcolor
         ENDFOR
         RETURN
      END
      'SVM_RESERVED': sub_row = 0
      'NRT_RESERVED': sub_row = 3
      'PAYLOAD_RESERVED': sub_row = 1
      'THROUGHPUT_RCR': sub_row = 2
      'THROUGHPUT_NORCR': sub_row = 2
      'SUPPORT': BEGIN
         label = resource.telescop
         plot_support, label, t1, t2, row, char=charsize, color=color
         RETURN
      END
      ELSE:return
   ENDCASE

   y1 = row+0.05+sub_row*srow_wid
   y2 = y1+srow_wid
   FOR i=0, num-1 DO BEGIN
      x1 = t1(i) > !x.crange(0)
      x2 = t2(i) < !x.crange(1)
      IF x2 GT x1 THEN polyfill, [x1, x2, x2, x1], [y1, y1, y2, y2],color=color
   ENDFOR

   RETURN
END

