;---------------------------------------------------------------------------
; Document name: get_soho_inst.pro
; Created by:    Liyun Wang, GSFC/ARC, January 15, 1996
;
; Last Modified: Fri Apr 26 15:21:57 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
FUNCTION get_soho_inst, name, short=short, exclude=exclude, $
                        sorting=sorting, error=error
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       GET_SOHO_INST()
;
; PURPOSE: 
;       Return one or more SOHO instrument names
;
; CATEGORY:
;       Utility, planning
; 
; SYNTAX: 
;       Result = get_soho_inst([name])
;
; EXAMPLES:
;       PRINT, get_soho_inst()       ; print all SOHO instruments in long form
;       PRINT, get_soho_inst(/short) ; print all inst. in short form
;       print, get_soho_inst('SWAN') ; print 'W'
;       PRINT, get_soho_inst('W')    ; print 'SWAN'
;       print, get_soho_inst('W',/short) ; print 'W'
;
; INPUTS:
;       None required. 
;
; OPTIONAL INPUTS: 
;       NAME - String scalar or array, instrument name in either short
;              or long form.  
;              If NAME is in short form, output will be in long form
;              (unless keyword SHORT is set); if NAME is in long form,
;              output will be in short form. If NAME is missing, all
;              instrument  names (except those listed in EXCLUDE
;              keyword) will be returned  
;
; OUTPUTS:
;       RESULT - Returned instrument name in full form (default) or
;                short name (if SHORT keyword is set)
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       ALL     - Set this keyword to return names of all SOHO instruments
;       SHORT   - Set this keyword to return inst name in short format
;                (1 character)
;       EXCLUDE - List of instrument names to be excluded from the
;                 returned list. If keyword SHORT is set, EXCLUDE
;                 should also in short form, otherwise it should be in
;                 full form
;       SORTING - Set this keyword to sort instrument list (in long form) 
;                 alphabetically
;       ERROR   - String scaler containing any returned error
;                 message. If no error occurs, it's a null string.
;
; COMMON:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, January 15, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, January 16, 1996, Liyun Wang, GSFC/ARC
;          Added SORTING keyword
;          Keyword SHORT will force returned name to be short now
;       Version 3, April 26, 1996, Liyun Wang, GSFC/ARC
;          Modified so that NAME can be a string array as well
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2
   error = ''
   inst_list = ['CDS', 'SUMER', 'UVCS', 'LASCO', 'EIT', 'MDI', 'CELIAS',$
                'VIRGO', 'SWAN', 'GOLF', 'COSTEP', 'ERNE']
   short_list = ['C', 'S', 'U', 'L', 'E', 'M', 'I', 'V', 'W', 'G', 'O', 'R']
   IF KEYWORD_SET(sorting) THEN BEGIN
      ii = SORT(inst_list)
      inst_list = inst_list(ii)
      short_list = short_list(ii)
   ENDIF
   IF N_PARAMS() EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;     Return all instrument name
;---------------------------------------------------------------------------
      IF KEYWORD_SET(short) THEN BEGIN
         IF N_ELEMENTS(exclude) NE 0 THEN BEGIN
            FOR i=0, N_ELEMENTS(exclude)-1 DO BEGIN
               ii = WHERE(exclude(i) NE short_list)
               IF ii(0) GE 0 THEN short_list = short_list(ii)
            ENDFOR
         ENDIF
         RETURN, short_list
      ENDIF ELSE BEGIN
         IF N_ELEMENTS(exclude) NE 0 THEN BEGIN
            FOR i=0, N_ELEMENTS(exclude)-1 DO BEGIN
               ii = WHERE(exclude(i) NE inst_list)
               IF ii(0) GE 0 THEN inst_list = inst_list(ii)
            ENDFOR
         ENDIF 
         RETURN, inst_list
      ENDELSE
   ENDIF ELSE BEGIN
      IF datatype(name) NE 'STR' THEN BEGIN
         error = 'Wrong input parameter type. String type required.'
         RETURN, ''
      ENDIF ELSE BEGIN
         inst_in = STRUPCASE(STRTRIM(name,2))
         out = ''
         FOR j=0, N_ELEMENTS(name)-1 DO BEGIN
            IF STRLEN(inst_in(j)) EQ 1 THEN BEGIN
               ii = WHERE(inst_in(j) EQ short_list)
               IF ii(0) GE 0 THEN BEGIN
                  IF KEYWORD_SET(short) THEN $
                     out = [out, short_list(ii(0))] $
                  ELSE $
                     out = [out, inst_list(ii(0))]
               ENDIF ELSE BEGIN
                  error = 'No such SOHO instrument: '+name(j)
                  MESSAGE, error, /cont
               ENDELSE
            ENDIF ELSE BEGIN
               temp = grep(name(j), inst_list, index=ii)
               IF ii(0) GE 0 THEN out = [out, short_list(ii(0))] ELSE BEGIN
                  error = 'No such SOHO instrument: '+name(j)
                  MESSAGE, error, /cont
               ENDELSE
            ENDELSE
         ENDFOR
         CASE (N_ELEMENTS(out)) OF
            1: RETURN, out
            2: RETURN, out(1)
            ELSE: RETURN, out(1:*)
         ENDCASE
      ENDELSE
   ENDELSE
   
END
;---------------------------------------------------------------------------
; End of 'get_soho_inst.pro'.
;---------------------------------------------------------------------------
