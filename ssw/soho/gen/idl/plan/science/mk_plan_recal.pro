;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_RECAL
;
; Purpose     :	Recompute DETAILS based on time-tagged information
;
; Explanation :	Recalculates start and stop times of CDS plan entries
;
; Use         :	MK_PLAN_RECAL, DETAILS, NDETAILS
;
; Inputs      :	DETAILS = structure array of detailed plan entries
;
; Opt. Inputs : STARTDIS = if set, do not auto move non-tt plans before STARTDIS
;
; Outputs     :	NDETAILS = time-shifted plan entries
;
; Opt. Outputs:	None.
;
; Keywords    : TGAP = minimum gap between successive entries [def = 1.e-2]
;               TCAL = start time at which to start recalculation 
;                        [def = start of first plan]
;               TMAX = start time of last plan entry to check [def = last plan in DETAILS]
;               ERR = error string
;               SERIOUS = if set, then returned error is pretty serious
;                    and plan cannot be written as is.
;
; Calls       :	Assorted structure handling routines
;
; Common      :	None.
;
; Restrictions:	Works for DETAILS plan entries only
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 9 January 1995.
;-

pro mk_plan_recal,details,ndetails,startdis,stopdis,tgap=tgap,tcal=tcal,tmax=tmax,$
                  err=err,serious=serious

;-- recalculate DETAIL plan entries using time-tagged information

err='' & serious=0
delvarx,ndetails
if not exist(details) then return
type=get_plan_type(details)
if type ne 0 then return
itime=get_plan_itime(details)
if n_elements(tgap) eq 0 then tgap=1.d-2
sdetails=mk_plan_sort(details,/uniq,/str)
np=n_elements(sdetails)
tlast=min(sdetails.(itime))-tgap
tinfinite=10.*max(sdetails.(itime+1))
tbound=-tinfinite

dprint,'% MK_PLAN_RECAL: checking plan for pointing changes and GIS studies'

;-- identify CDS GIS and engineering studies

cds=(which_inst () eq 'C')
if cds then begin
 gset_ids=sdetails.gset_id
 use_gis=(gset_ids gt -1)
 is_eng=(sdetails.study_id eq 0)
endif

if not mk_plan_priv(tbound) then begin
 get_utc,cur_time
 cur_ut=utc2tai(cur_time)
 tbound=cur_ut > tbound
endif

;-- check each entry, and speed things up by only checking entries later
;   than TCAL and before LAST plan entry

last_plan_time=max(sdetails.(itime+1))
if not exist(tcal) then tcal=min(sdetails.(itime))
if not exist(tmax) then tmax=last_plan_time
if tmax le 0. then tmax=last_plan_time

cfind=where( (str_format(sdetails.(itime)) lt str_format(tcal)) and $
             (str_format(sdetails.(itime+1)) lt str_format(tcal)),cnt)

if cnt gt 1 then begin
 iend=cfind(cnt-2)
 istart=cfind(cnt-1)
 ndetails=sdetails(0:iend) 
 dprint,'% MK_PLAN_RECAL: Skipping first '+strtrim(string(cnt-1),2)+' entries..'
 tlast=max(ndetails.(itime+1)) > tlast
endif else istart=0

check=where( str_format(sdetails.(itime)) lt str_format(tmax),cnt)
if cnt eq 0 then clast=np-1 else begin
 dprint,'% MK_PLAN RECAL: stopping after ',tai2utc(tmax,/ecs)
 clast=check(cnt-1)
endelse

for i=istart,clast do begin

 if i gt 0 then last_plan=sdetails(i-1)
 
 spec=strtrim(sdetails(i).sci_spec,2)
 curr_plan=sdetails(i)
 if i lt (np-1) then begin
  rest=sdetails(i+1:np-1)
  tt=where(rest.time_tagged eq 1,cnt)
  if cnt gt 0 then tnext=min(rest(tt).(itime)) else tnext=tinfinite
 endif else tnext=tinfinite

;-- if CDS engineering study, check if next plan pointing is required,
;   otherwise use prior

 if cds then begin
  if is_eng(i) and (curr_plan.n_pointings eq 0) then begin
   xp=0. & yp=0. & cerr=''
   if (i lt (np-1)) then call_procedure,'get_cds_xy',sdetails(i+1),xp,yp,/first,err=cerr
   use_prior=((cerr ne '') or (i eq (np-1))) and (i gt 0)
   if use_prior then begin
    cerr=''
    call_procedure,'get_cds_xy',last_plan,xp,yp,/last,err=cerr
   endif
   if cerr eq '' then begin
    curr_plan.pointings(0).ins_x=xp
    curr_plan.pointings(0).ins_y=yp
   endif
  endif
 endif
  
;-- check if time-tagged (only shift back if projected time is after current UT)

 time_tagged=(curr_plan.time_tagged eq 1)
 tstart=curr_plan.(itime)
 if exist(startdis) then cstart=startdis else cstart=tstart

 cur_day=tai2utc(cstart) & cur_day.time=0 
 cur_day=utc2tai(cur_day)

 if exist(stopdis) then begin
  end_day=tai2utc(stopdis) & end_day.time=0
  end_day=utc2tai(end_day)
 endif else begin
  end_day=tai2utc(cstart) & end_day.time=0 & end_day.mjd=end_day.mjd+1
  end_day=utc2tai(end_day)
 endelse

 if not time_tagged then begin
  if i eq 0 then tlast=cur_day-tgap
  proj_tstart=tlast+tgap  

;-- don't project current untagged plans to previous day

  if (proj_tstart le cur_day) and (tstart ge cur_day) then begin
   proj_tstart=cur_day & curr_plan.time_tagged=1
  endif else begin

;-- don't project next days untagged plans to current day

   if (proj_tstart le end_day) and (tstart ge end_day) then begin
    proj_tstart=end_day & curr_plan.time_tagged=1
   endif

  endelse

  if proj_tstart ge tbound then tstart=proj_tstart

;  if i gt 0 then if cds then if is_eng(i-1) then curr_plan.time_tagged=1
 endif

 if str_format(tstart) lt str_format(tnext) then begin

;-- recover duration of study corresponding to this plan entry

  com_dur=1
  if tag_exist(curr_plan,'ORIG_DUR') then begin
   if curr_plan.orig_dur ne 0. then begin
    sdur=curr_plan.orig_dur & com_dur=0
   endif
  endif
  if com_dur then begin
   derr=''
   sdur=mk_plan_dur(curr_plan,/point,/quiet,err=derr) 
   if derr ne '' then begin
    serious=1 & err=derr & return
   endif
  endif

  if cds then begin
  
;-- add pointing/slit delays between studies

   perr='' & pdelay=0
   pdelay=call_function('get_cds_delay',last_plan,curr_plan,err=perr)
   if perr ne '' then begin
    serious=1 & err=perr & return
   endif
   sdur=sdur+pdelay

;-- Add in LUT calculation delays for CDS GIS studies. 
;   I hope this doesn't cause more problems.
;   Only need to recompute LUT if GSET ID is new or changed from last GIS study

   if use_gis(i) then begin
    add_dur=0 & last_gset_id=-1
    if i eq 0 then add_dur=1 else begin
     clook=where(use_gis(0:i-1),cnt)
     if cnt eq 0 then add_dur=1 else begin
      last_gset_id=gset_ids(clook(cnt-1))
      if gset_ids(i) ne last_gset_id then add_dur=1
     endelse
    endelse
    if add_dur then begin
     lut_delay=call_function('get_gis_delay',gset_ids(i))
     dprint,'% MK_PLAN_RECAL: GSET ID change ',last_gset_id,' --> ',gset_ids(i)
     dprint,'% MK_PLAN_RECAL: adding GIS LUT delay ',lut_delay

;-- if previous study is an engineering study (e.g. QCM) then we have
;   to allow for possible early loading of GSET LUT's

     delay_added=0 & err=''
     if exist(last_plan) then begin
      if (last_plan.study_id eq 0) and (curr_plan.time_tagged) then begin
       ncurr=n_elements(ndetails)
       if ncurr gt 0 then tprev=ndetails(ncurr-1).date_end else tprev=tstart
       new_tstart=(tprev+lut_delay+tgap) > tstart
       if str_format(new_tstart) ge str_format(tnext) then begin
        perr=['GIS study: '+spec+' cannot be loaded at '+anytim2utc(tstart,/vms,/trunc),$
              'because it will be interrupted by a subsequent time-tagged study.']
        serious=1 & err=perr & return
       endif else begin
        tstart=new_tstart
        dprint,'% MK_PLAN_RECAL: delaying start of GIS study following QCM'
        if ncurr gt 0 then ndetails(ncurr-1).date_end=tprev+lut_delay
        delay_added=1
       endelse
      endif
     endif
     if not delay_added then sdur=sdur+lut_delay
    endif
   endif

  endif

;-- Extend duration as far as next time-tagged event allows
;   If this is a GIS study, it should not be interrupted.

  new_tend=tstart+sdur
  if cds then begin
   if use_gis(i) and (new_tend gt (tnext-tgap)) then begin
    perr=['GIS study: '+spec+' cannot be loaded at '+anytim2utc(tstart,/vms,/trunc),$
          'because it will be interrupted by a subsequent time-tagged study.']
    serious=1 & err=perr & return
   endif
  endif
  tend=new_tend < (tnext-tgap)
  if (tend-tstart) gt tgap then begin
   temp=tai2utc(tstart)
   if temp.time eq 0 then curr_plan.time_tagged=1
   curr_plan.(itime)=tstart
   curr_plan.(itime+1)=tend
   if not exist(ndetails) then ndetails=curr_plan else $
    ndetails=concat_struct(temporary(ndetails),curr_plan)
   
;-- next entry will start after previous one (unless time-tagged)

   tlast=tend
  endif else serr='Duration of this entry will be less than zero: '+spec
 endif else serr='Cannot fit this entry. It will be removed: '+spec

;-- record any errors

 if exist(serr) then begin
  err=[err,serr]
  delvarx,serr
 endif

endfor

;-- add in remaining entries

if (clast lt (np-1)) then begin
 if not exist(ndetails) then ndetails=sdetails(clast+1:np-1) else $
  ndetails=concat_struct(temporary(ndetails),temporary(sdetails(clast+1:np-1)))
endif

nerr=n_elements(err)
if nerr gt 1 then err=err(1:nerr-1)

return & end

