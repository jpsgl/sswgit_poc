	FUNCTION GET_SC_POINT, DATE, TYPE, ERRMSG=ERRMSG, RETAIN=RETAIN, $
		NOCORRECT=NOCORRECT, SUB_DATE=SUB_DATE,		$
		FIND_CLOSEST=FIND_CLOSEST
;+
; Project     :	SOHO - CDS
;
; Name        :	GET_SC_POINT()
;
; Purpose     :	Get the SOHO spacecraft pointing.
;
; Category    :	Class3, Orbit
;
; Explanation :	Read the definitive attitude file to get the spacecraft
;		pointing and roll information.  If no definitive file is found,
;		then the nominal values are returned as "Predictive".
;
;		Note that the difference between GET_SC_ATT and GET_SC_POINT is
;		that GET_SC_POINT converts the information in the attitude file
;		into a format suitable for use in the FITS headers.
;
; Syntax      :	Result = GET_SC_POINT( DATE  [, TYPE ] )
;
; Examples    :	PNT = GET_SC_POINT(DATE_OBS)
;		FXADDPAR, HEADER, 'SC_X0', PNT.SC_X0
;		FXADDPAR, HEADER, 'SC_Y0', PNT.SC_Y0
;		FXADDPAR, HEADER, 'SC_ROLL', PNT.SC_ROLL
;
; Inputs      :	DATE	= The date/time value to get the pointing information
;			  for.  Can be in any CDS time format.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a structure containing the
;		spacecraft pointing information.  It contains the following
;		tags:
;
;			SC_X0	= Spacecraft pointing in the solar X direction,
;				  in arcseconds from the center of the sun.
;			SC_Y0	= Spacecraft pointing in the solar Y direction
;			SC_ROLL = Spacecraft roll in degrees counter-clockwise.
;
; Opt. Outputs:	TYPE	= Returns whether predictive or definitive data was
;			  used to calculate the result.  Returned as either
;			  "Definitive" or "Predictive".
;
; Keywords    :	RETAIN	= If set, then the FITS attitude file will be left
;			  open.  This speeds up subsequent reads.
;
;		NOCORRECT = If set, then the correction for the repointing of
;			    16-Apr-1996 is not applied.  This allows one to
;			    check the uncorrected values against the corrected
;			    ones.
;
;		ERRMSG	= If defined and passed, then any error messages will
;			  be returned to the user in this parameter rather than
;			  depending on the MESSAGE routine in IDL.  If no
;			  errors are encountered, then a null string is
;			  returned.  In order to use this feature, ERRMSG must
;			  be defined first, e.g.
;
;				ERRMSG = ''
;				Result = GET_SC_POINT( ERRMSG=ERRMSG, ... )
;				IF ERRMSG NE '' THEN ...
;
;		FIND_CLOSEST = If set, then the closest attitude file to the
;			  date requested is found.
;
;               SUB_DATE= If data from a date other than was called is used,
;                         this keyword returns the date used. Otherwise 
;			  returns ''.
;
; Calls       :	GET_SC_ATT, DELVARX, SOCK_LIST
;
; Common      :	GET_SC_POINT is an internal common block.
;
; Restrictions:	The attitude entries for the time closest to that requested is
;		used to calculate the parameters.  Since the attitude data
;		is calculated every 10 minutes, this should be correct within
;		+/-5 minutes.  No attempt is made to interpolate to closer
;		accuracy than that.
;
; Side effects:	Currently, predictive data is always 0 for all angles.  This is
;		the nominal pointing for SOHO.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 04-Dec-1995, William Thompson, GSFC
;		Version 2, 06-Dec-1995, William Thompson, GSFC
;			Corrected calculation of output parameters.
;			Renamed to GET_SC_POINT
;			Use GET_SC_ATT to read file.
;			Added keyword RETAIN
;		Version 3, 24-Jan-1996, William Thompson, GSFC
;			Temporary fix to return only predictive data until the
;			problems with the attitude files are resolved.
;		Version 4, 08-Mar-1996, William Thompson, GSFC
;			Removed temporary fix, and corrected sign of roll.
;		Version 5, 04-Apr-1996, William Thompson, GSFC
;			Corrected sign of pitch.
;		Version 6, 08-Jul-1996, William Thompson, GSFC
;			Corrected for repointing of spacecraft 16-Apr-1996
;		Version 7, 01-Oct-2001, Nathan Rich, NRL
;			Add SUB_DATE, FIND_CLOSEST keywords
;               Version 8, 01-Jul-2003, William Thompson, GSFC
;                       Read nominal_roll_attitude.dat
;
; Contact     :	WTHOMPSON
;-
;
	ON_ERROR, 2
	COMMON GET_SC_POINT, LAST_READ, ROLL_DATE, ROLL_VALUE
;
;  Initialize RESULT.  If a definitive attitude file is found, then this will
;  be updated.
;
	RESULT = {SOHO_SC_PNT, SC_X0: 0.0, SC_Y0: 0.0, SC_ROLL: 0.0}
	TYPE = "Predictive"
	SUB_DATE=''
;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 1 THEN BEGIN
            MESSAGE = 'Syntax:  Result = GET_SC_POINT( DATE  [, TYPE ] )'
            GOTO, HANDLE_ERROR
	ENDIF
;
;  Read in the attitude information.
;
	A = GET_SC_ATT(DATE, TYPE, ERRMSG=ERRMSG, RETAIN=RETAIN,	$
		SUB_DATE=SUB_DATE, FIND_CLOSEST=FIND_CLOSEST)
	IF N_ELEMENTS(ERRMSG) NE 0 THEN BEGIN
            MESSAGE = ERRMSG
            IF MESSAGE NE "" THEN GOTO, HANDLE_ERROR
	ENDIF
;
;  If definitive data, then process the data.
;
        IF TYPE EQ 'Definitive' THEN BEGIN
;
;  If after 1996-04-17, then put in an offset value for the pitch.
;
            IF NOT KEYWORD_SET(NOCORRECT) AND	$
		(ANYTIM2UTC(DATE, /CCSDS) GT '1996-04-16T23:25:00.000Z') THEN $
			A.SC_AVG_PITCH = A.SC_AVG_PITCH + 198 / (!RADEG * 3600)
;
;  Convert the pitch and yaw into arcseconds, and correct for the roll.
;
            X0 = -A.SC_AVG_YAW   * !RADEG * 3600
            Y0 = -A.SC_AVG_PITCH * !RADEG * 3600
            COS_R = COS(A.SC_AVG_ROLL)
            SIN_R = SIN(A.SC_AVG_ROLL)
            RESULT.SC_X0 =  X0*COS_R + Y0*SIN_R
            RESULT.SC_Y0 = -X0*SIN_R + Y0*COS_R
;
;  Convert the roll into degrees.
;
            RESULT.SC_ROLL = -A.SC_AVG_ROLL * !RADEG
            GOTO, FINISH
;
;  Otherwise, read the predictive attitude file.
;
        END ELSE BEGIN
;
;  Get the current date as a Modified Julian Day number.
;
            GET_UTC, UTC, ERRMSG=ERRMSG
            IF N_ELEMENTS(ERRMSG) NE 0 THEN IF ERRMSG[0] NE '' THEN $
                    GOTO, HANDLE_ERROR
            TODAY = UTC.MJD
;
;  If the current date is larger than the date stamp for the last time the file
;  was read, or if the file hasn't been read yet, then reread the file.
;
            IF N_ELEMENTS(LAST_READ) EQ 0 THEN LAST_READ = 0
            IF TODAY GT LAST_READ THEN BEGIN
;
;  Read the data file, using either NFS or HTTP.
;
                PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
                PATH = CONCAT_DIR(PATH, 'roll', /DIR)
                FILENAME = FIND_WITH_DEF('nominal_roll_attitude.dat', PATH, '')
                IF FILENAME NE '' THEN BEGIN
                    OPENR, UNIT, FILENAME, /GET_LUN
                    LINE = 'String'
                    DELVARX, LINES
                    LAST_READ = 0
                    WHILE NOT EOF(UNIT) DO BEGIN
                        READF, UNIT, LINE
                        BOOST_ARRAY, LINES, LINE
                    ENDWHILE
                    FREE_LUN, UNIT
;
;  If the file couldn't be found locally, try using HTTP.
;
                END ELSE BEGIN
                    FILENAME = 'http://soho.nascom.nasa.gov/data/' +    $
                            'ancillary/attitude/roll/nominal_roll_attitude.dat'
                    SOCK_LIST, FILENAME, HLINES, ERR=ERR
                    IF ERR EQ '' THEN BEGIN
                        HEADER_FOUND = 0
                        FOR I_LINE = 0,N_ELEMENTS(HLINES)-1 DO BEGIN
                            LINE = STRCOMPRESS(STRTRIM(HLINES[I_LINE],2))
                            IF (LINE EQ '') AND (NOT HEADER_FOUND) THEN BEGIN
                                HEADER_FOUND = 1
                                LINES = HLINES[I_LINE+1:*]
                            ENDIF
                        ENDFOR
                    END ELSE DELVARX, LINES
                ENDELSE
;
;  Parse the lines read in.
;
                IF N_ELEMENTS(LINES) GT 0 THEN BEGIN
                    LAST_READ = 0
                    DELVARX, ROLL_DATE, ROLL_VALUE
                    FOR I_LINE = 0,N_ELEMENTS(LINES)-1 DO BEGIN
                        LINE = STRCOMPRESS(STRTRIM(LINES[I_LINE],2))
                        IF (LINE NE '') AND (STRMID(LINE,0,1) NE '#') THEN $
                                BEGIN
                            SPACE = STRPOS(LINE,' ',/REVERSE_SEARCH)
                            BOOST_ARRAY, ROLL_DATE,     $
                                    ANYTIM2UTC(STRMID(LINE,0,SPACE),/CCSDS)
                            BOOST_ARRAY, ROLL_VALUE,    $
                                    FLOAT(STRMID(LINE,SPACE+1,STRLEN(SPACE)))
                        ENDIF
                    ENDFOR
                    LAST_READ = TODAY
                ENDIF
            ENDIF
;
;  Determine the nominal pointing from the common block.
;
            IF LAST_READ GT 0 THEN BEGIN
                W = MAX(WHERE(ROLL_DATE LE ANYTIM2UTC(DATE,/CCSDS)))
                IF W GT 0 THEN RESULT.SC_ROLL = ROLL_VALUE[W]
            ENDIF
        ENDELSE
        GOTO, FINISH
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_SC_POINT: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Return the result.
;
FINISH:
	RETURN, RESULT
	END
