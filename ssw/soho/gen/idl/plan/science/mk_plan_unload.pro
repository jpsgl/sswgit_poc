;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_PLAN_UNLOAD
;               
; Purpose     : Unload contents of MK_PLAN_SHARED common blocks
;               
; Category    : Planning
;               
; Explanation : Depending on DB and instrument type, unloads plan entries
;               such as CDS_DETAILS into variable PLANS for
;               use by MK_DETAIL.
;               
; Syntax      : IDL> mk_plan_unload,type,plans
;    
; Examples    : 
;
; Inputs      : TYPE  - 0,1,2,3 for DETAILS, FLAG, SCIENCE, and ALT
;               
; Opt. Inputs : None
;               
; Outputs     : PLANS - structure array with plans of requested type
;               
; Opt. Outputs: None
;               
; Keywords    : None
;
; Common      : MK_PLAN_SHARED
;               
; Restrictions: This is not standalone program, but is run in conjunction with
;               MK_PLAN and MK_DETAIL for CDS and SUMER only
;               
; Side effects: None
;               
; History     : Version 1,  21-Jun-1995,  D M Zarro.  Written
;
; Contact     : DMZARRO
;-            

pro mk_plan_unload,type,plans

@mk_plan_shared

;-- check instrument

inst=which_inst()

delvarx,plans
case type of
  0: begin
   if inst eq 'S' and exist(sumer_detail) then plans=sumer_detail
   if inst eq 'C' and exist(cds_detail) then plans=cds_detail
  end

  1: if exist(cds_flag) then plans=cds_flag

  2: begin
   if inst eq 'C' and exist(cds_science) then plans=cds_science
   if inst eq 'S' and exist(sumer_science) then plans=sumer_science
  end

  3: if exist(cds_alt) then plans=cds_alt

  else: begin message,'unsupported data type',/contin & return & end
endcase


return & end
