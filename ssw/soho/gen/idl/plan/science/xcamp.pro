;+
; Project     : SOHO - CDS
;
; Name        : XCAMP
;
; Purpose     : widget interface to campaign database
;
; Category    : operations, widgets
;
; Explanation :
;
; Syntax      : IDL> XCAMP
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : ID = selected campaign ID
;
; Opt. Outputs: None
;
; Keywords    : TSTART,TEND = start/end times to list
;               GROUP = widget ID of any calling widget
;               STATUS = 1/0 if selection was made/not made
;               LAST = set to use last times saved in COMMON
;               SELECT = permit a selection
;               FORCE = force a selection
;               NOWARN = turn of warning for multiple XCAMP copies
;
; Restrictions: None.
;
; Side effects: None.
;
; History     : Version 1,  21-Sept-1996,  D.M. Zarro.  Written
;               Version 2, October 16, 1996, Liyun Wang, NASA/GSFC
;                  Reverse sorted campaign list 
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

;--------------------------------------------------------------------------- 

pro xcamp_options_event,event

widget_control,event.top,get_uvalue=unseen
info=get_pointer(unseen,/no_copy)
if datatype(info) ne 'STC' then return

widget_control,event.id,get_uvalue=uservalue
if (n_elements(uservalue) eq 0) then uservalue=''
bname=trim(uservalue)

;-- check what items to list

slook=where(event.id eq info.sbuttons,count)
if count eq 1 then info.sbut(slook)=event.select

tlook=where(event.id eq info.tbuttons,count)
if count gt 0 then info.tsort=tlook(0)

if event.id eq info.rbuttons(0) then info.rsort=event.select
if event.id eq info.rbuttons(1) then info.rsort=1-event.select

if event.id eq info.dbuttons(0) then info.dsort=event.select
if event.id eq info.dbuttons(1) then info.dsort=1-event.select

;-- quit here

if bname eq 'idone' then begin
 wopt=where(info.sbut eq 1,count)
 if count eq 0 then begin
  xack,'At least one item must be selected'
  info.status=0
 endif else begin
  info.status=1
  xkill,event.top
 endelse
endif

if bname eq 'cdone' then begin
 info.status=0
 xkill,event.top
endif

set_pointer,unseen,info,/no_copy

return & end

;------------------------------------------------------------------------------

pro xcamp_options,info,group=group

ibase=widget_base(/column,title="XCAMP OPTIONS")
mk_dfont,bfont=bfont
lfont=''
first=widget_base(ibase,/row)
hideb=widget_button(first,value='Apply',uvalue='idone',font=bfont,/frame)
canb=widget_button(first,value='Cancel',uvalue='cdone',font=bfont,/frame)

row1=widget_base(ibase,/column,/frame)


choices=['CMP_NO','CMP_TYPE','CMP_NAME','OBSERVER NAME','START TIME','END TIME','PARTICIPANTS']
nchoices=n_elements(choices)

title='Choose which items to list:'
xmenu2,choices,row1,/column,/nonexclusive,font=bfont,buttons=sbuttons,$
      title=title

for i=0,n_elements(sbuttons)-1 do widget_control,sbuttons(i),set_button=info.sbut(i)

row2=widget_base(ibase,/column,/frame)

xmenu2,['START TIME','CMP_NO','OBSERVER NAME'],row2,/row,/exclusive,font=bfont,/no_rel,$
       buttons=tbuttons,title='Sort by: '
widget_control,tbuttons(info.tsort),/set_button

xmenu2,['YES','NO'],row2,/row,/exclusive,font=bfont,/no_rel,$
       buttons=rbuttons,title='Reverse sort? '
widget_control,rbuttons(1-info.rsort),/set_button

xmenu2,['YES','NO'],row2,/row,/exclusive,font=bfont,/no_rel,$
       buttons=dbuttons,title='Include campaigns with missing START/STOP times? '
widget_control,dbuttons(1-info.dsort),/set_button


;-- save TEMP INFO  in pointer for passing to event loop

temp={sbut:info.sbut,tsort:info.tsort,rsort:info.rsort,dsort:info.dsort,$
     sbuttons:sbuttons,rbuttons:rbuttons,tbuttons:tbuttons,$
     dbuttons:dbuttons,status:0}

make_pointer,unseen
set_pointer,unseen,temp,/no_copy
widget_control, ibase, set_uvalue=unseen

;-- realize and manage widgets

xrealize,ibase,group=group,/center 
xmanager,'xcamp_options',ibase,group=group,/modal
xmanager_reset,wbase,group=group,/modal
xshow,group

temp=get_pointer(unseen,/no_copy)
free_pointer,unseen
if datatype(temp) eq 'STC' then copy_struct,temp,info

return & end

;------------------------------------------------------------------------------

 pro xcamp_event,  event                         ;event driver routine

common xcamp_com2,cstart,cend,sdata,lines,instit,type,sav_db,info

 widget_control, event.id, get_uvalue = uservalue
 if (n_elements(uservalue) eq 0) then uservalue=''
 bname=trim(uservalue)

;-- quit here

 quit=0 
 if (bname(0) eq 'exit') then quit=1

 if (bname(0) eq 'cancel') then begin
  info.sel_id=-1
  quit=1
 endif
 
 if (bname(0) eq 'select') then begin
  if (info.sel_id lt 0) then begin
   xack,'Please select a Campaign.',group=event.top
   return
  endif else quit=1
 endif

 if quit then begin
  xtext_reset,info
  xkill,event.top
  return
 endif

;-- validate input times in TEXT widgets

 good=xvalidate(info,event,time_change=time_change,/date)
 if good then begin
  if (event.id eq info.wops1) or (event.id eq info.wops2) then begin
   if time_change then xcamp_list
  endif
 endif

;-- help

 if bname(0) eq 'help' then xack,info.help,group=event.top

;-- OPTIONS

 if bname(0) eq 'options' then begin
  info_sav=info
  xcamp_options,info,group=event.top
  if (1-info.status) then info=info_sav
  if not match_struct(info,info_sav) then begin
   xcamp_label
   xcamp_list
  endif
 endif

;-- output

 if bname(0) eq 'print' then xcamp_list,/print
 if bname(0) eq 'write' then xcamp_list,/write
 if bname(0) eq 'mail' then xcamp_list,/mail
                        
;-- update databases

 if bname(0) eq 'update' then begin
  warn=['This operation will update your local campaign database',$
        'with the latest campaign information provided by the SOC.',$
        'The update may take a while.']
  val=xanswer(warn,group=event.top,instruct='Do you wish to proceed?')
  if val then begin
;   xtext,'Please wait. Updating...',/just_reg,wbase=tbase
   widget_control,/hour
   update_campaign,err=err,/progress,tstart=info.ops1,tend=info.ops2,/verb
   if err ne '' then xack,err,group=event.top else begin
    message,'resetting...',/cont
    xcamp_list,/reset
    xtext,'Update Completed.',/just_reg,wait=1
   endelse
   xkill,tbase
   xshow,event.top
  endif

 endif
  
;-- view institutes

 if bname(0) eq 'view' then info.lview=event.select

;-- reset

 if bname(0) eq 'reset' then begin
  type=''
  xcamp_list,/reset
 endif

 if bname(0) eq 'search' then begin
  widget_control,info.search,get_value=temp
  new_type=strupcase(trim(temp(0)))
  if new_type ne type then begin
   type=new_type
   xcamp_list
  endif
 endif

;-- list event

 if event.id eq info.alist then info.sel_id=fix(bname(event.index))

 ;-- update living widgets

 if xalive(info.viewb) then widget_control,info.viewb, sensitive=info.sel_id gt -1
 if (info.lview) then begin
  if (info.sel_id gt -1) then begin
   err=''
   if info.sel_id ne info.def.cmp_no then begin
    get_campaign,info.sel_id,def,err=err
    if err eq '' then info=rep_tag_value(info,def,'def')
   endif
   if err eq '' then begin
    lbase=info.lbase
    xlist,rep_tag_name(info.def.institutes,'INSTITUT','PARTICIPANTS'),$
     group=event.top,/just_reg,$
     pad=10,wbase=lbase,title='CMP_NO: '+string(info.def.cmp_no)
    info.lbase=lbase
    cbase=info.cbase
    if tag_exist(info.def,'COMMENT') then $
     comments=str2lines(info.def.comment,length=50) else comments='No comments'
    xtext,comments,xsize=50,/no_find,/no_print,/no_save,group=event.top,$
     wbase=cbase,title='COMMENTS - CMP_NO:'+string(info.def.cmp_no),$
     space=2,/just_reg
    info.cbase=cbase
   endif
  endif
 endif else xhide,[info.lbase,info.cbase]

 return & end

;--------------------------------------------------------------------------- 

pro xcamp_label

common xcamp_com2

wlabel=info.mlabel
nopt=n_elements(info.mlabel)
wopt=where(info.sbut eq 1,count)
if count eq 0 then begin
 xack,'At least one item must be selected'
 return
endif
info.wlabel=arr2str(wlabel(wopt),delim='')
widget_control,info.alabel,set_value=info.wlabel
return & end

;--------------------------------------------------------------------------- 

pro xcamp_list,reset=reset,write=write,mail=mail,print=print

common xcamp_com2

cur_db=getenv('ZDBASE')
if datatype(sav_db) ne 'STR' then sav_db=cur_db
db_change=sav_db ne cur_db
if db_change then sav_db=cur_db

time_change=1
if exist(cstart) then time_change=cstart ne info.ops1
if not time_change then if exist(cend) then time_change=cend ne info.ops2
wstart=strmid(anytim2utc(info.ops1,/date,/vms),0,11)
wend=strmid(anytim2utc(info.ops2,/date,/vms),0,11)
widget_control,info.wops1,set_value=wstart
widget_control,info.wops2,set_value=wend

relisting=0
nfound=n_elements(sdata)
if time_change or db_change or keyword_set(reset) then begin
 relisting=1
 widget_control,/hour
 dprint,'% XCAMP: relisting...'
 find_campaign,info.ops1,info.ops2,sdata,nfound
 cstart=info.ops1 & cend=info.ops2
endif 

;-- search on type

lines='No entries found for specified interval and/or type'
no_lines=lines
if datatype(type) ne 'STR' then type='' else type=trim(strupcase(type))
if nfound gt 0 then begin
 ctype=tag_exist(sdata,'CMP_TYPE')
 no_camp=clear_struct(sdata(0))
 if ctype then no_camp.cmp_type='Not Supporting'
 no_camp.cmp_name='None'
 no_camp.observer='None'
 have_syn=where(sdata.cmp_no eq 1,count)
 if count eq 0 then begin
  synop=no_camp
  synop.cmp_name='Synoptic Program'
  synop.observer='All'
  synop.cmp_no=1
  synop.cmp_type='Synoptic'
  tdata=[no_camp,synop,sdata]
 endif else tdata=[no_camp,sdata]

 if (trim(type) ne '') then begin
  if ctype then begin
   tpos=strpos(strupcase(trim(tdata.cmp_type)),type)
   found=where((tpos gt -1) or (tdata.cmp_no eq 0),nfound)
   if nfound gt 0 then tdata=tdata(found) 
  endif
 endif else nfound=n_elements(tdata)

endif

widget_control,info.search,set_value=type

;-- output options

widget_control,info.printb,sensitive=(nfound gt 0)
widget_control,info.writeb,sensitive=(nfound gt 0)
widget_control,info.mailb,sensitive=(nfound gt 0)
print_it=keyword_set(print)
mail_it=keyword_set(mail)
write_it=keyword_set(write)

;-- check for zero START/STOP times

if nfound gt 0 then begin
 if not info.dsort then begin
  ok_times=where( (tdata.date_obs gt 0) and $
                  (tdata.date_end gt 0),nfound)
  if nfound gt 0 then tdata=tdata(ok_times)
 endif
endif

if nfound gt 0 then begin
 lines=replicate('',nfound)
 if info.sbut(0) then lines=lines+strpad(fstring(tdata.cmp_no,'(i4)'),9,/aft)+' '
 date_obs=tdata.date_obs
 date_end=tdata.date_end
 bad_start=where(tdata.date_obs eq 0,n1)
 bad_end=where(tdata.date_end eq 0,n2)
 date_obs=strpad(anytim2utc(tdata.date_obs,/vms,/date),14,/aft)
 date_end=strpad(anytim2utc(tdata.date_end,/vms,/date),14,/aft)
 if n1 gt 0 then date_obs(bad_start)=strpad('',14)
 if n2 gt 0 then date_end(bad_end)=strpad('',14)
 if info.sbut(1) then begin
  if ctype then lines=lines+strpad(strmid(trim(tdata.cmp_type),0,20),20,/aft)+' ' else $
   lines=lines+strpad('',20)+' '
 endif

 if info.sbut(2) then lines=lines+strpad(strmid(trim(tdata.cmp_name),0,30),30,/aft)+' '
 if info.sbut(3) then lines=lines+strpad(strmid(trim(tdata.observer),0,30),30,/aft)+' '
 if info.sbut(4) then lines=lines+date_obs+' '
 if info.sbut(5) then lines=lines+date_end

;-- treat INSTITUTES special

 if info.sbut(6) then begin
  if relisting or (n_elements(instit) ne n_elements(lines)) then begin
   xtext,'Please wait. Checking "Participant" Database...',wbase=tbase,/just_reg
   widget_control,/hour
   instit=strarr(nfound)
   for i=0,nfound-1 do begin
    err=''
    get_campaign,tdata(i).cmp_no,cdata,err=err
    if err eq '' then begin
     if tag_exist(cdata,'INSTITUTES') then begin
      ins=cdata.institutes
      if tag_exist(ins,'INSTITUT') then begin
       instit(i)=arr2str(trim(ins.institut),delim=',')
      endif
     endif
    endif
   endfor
   xkill,tbase
  endif

  lines=lines+strpad(instit,30,/aft)
 endif

;-- sort

 if nfound gt 1 then begin
  case info.tsort of
   0: ii=sort(tdata.date_obs) 
   1: ii=sort(tdata.cmp_no)
   2: ii=uniq([tdata.observer],sort([tdata.observer])) 
   else: do_nothing=1
  endcase 
  if info.rsort then ii=reverse(ii)
  tdata = tdata(ii)
  lines = lines(ii)
 endif

 if print_it or mail_it or write_it then begin
  bar='-------------------------------------------------------------------------------------'
  keep=where(tdata.cmp_no gt 1,count)
  if count gt 0 then plines=lines(keep) else plines=no_lines
  output=['SOHO Campaign Listing created on: '+anytim2utc(!stime,/vms),'',$
          'Period Covered: '+anytim2utc(info.ops1,/vms,/date)+' to '+anytim2utc(info.ops2,/vms,/date),bar,$
          info.wlabel,'',plines]
  case 1 of
   print_it: xprint,array=output,group=info.wbase,instruct=''
   mail_it: xmail,array=output,group=info.wbase
   write_it: begin
    file=mk_temp_file('xcamp.lis',path=path)
    fwrite=pickfile(group=info.wbase,path=path,/write,file=file)
    if trim(fwrite) ne '' then str2file,output,fwrite
   end
   else:return
  endcase
  return
 endif

 widget_control,info.alist,set_uvalue=trim(string(tdata.cmp_no))
endif

widget_control,info.alist,set_value=lines
widget_control,info.alist,sensitive=(nfound gt 0)
if (nfound gt 0) then begin
 clook=where(info.sel_id eq tdata.cmp_no,count)
 if count eq 1 then begin
  widget_control,info.alist,set_list_select=clook(0)
  if xalive(info.viewb) then widget_control,info.viewb,/sensitive
 endif
endif

return & end

;---------------------------------------------------------------------------

pro xcamp,id,tstart=tstart,tend=tend,group=group,err=err,status=status,$
          last=last,title=title,force=force,select=select,reset=reset,$
          nowarn=nowarn

common xcamp_com2

;-- defaults

err=''
select_windows
if not have_widgets() then begin 
 message,'widgets unavailable',/cont
 return
endif

;-- help

chelp=['The listed campaigns are scheduled to occur during the specified times.',$
       'To participate, please select a Campaign Number.',$
       'To not participate, select Campaign Number 0.']

;-- kill any dead apps

status=0
caller=get_caller(stat)
if stat or (strpos(caller,'XCAMP') gt -1) then xkill,/all
if (xregistered('xcamp') ne 0) then begin
 if (1-keyword_set(nowarn)) then xack,'Only one copy of XCAMP can run.'
 xshow,get_handler_id('xcamp')
 return
endif

inst = which_inst() 

;-- check out DB.

if inst ne 'S' then begin
 call_procedure, 'find_zdbase', cur_db_type, status=stat, /daily
 if stat eq 0 then return
endif

secs_per_day=24l*3600l
week=7l*secs_per_day

month=30l*secs_per_day
last=keyword_set(last)

get_utc,ops1 & ops1.time=0
ops1=(utc2tai(ops1)-week) > utc2tai(anytim2utc('2-dec-95')) 
get_utc,ops2 & ops2.time=0
ops2.mjd=ops2.mjd+1
ops2=utc2tai(ops2)
dtime=[ops1,ops2]
ctime=[0.d,0.d]
if exist(cstart) then ctime(0)=anytim2tai(cstart)
if exist(cend) then ctime(1)=anytim2tai(cend)
times=pick_times(tstart,tend,ctime=ctime,dtime=dtime,last=last)
ops1=times(0) & ops2=times(1)

;-- load fonts

mk_dfont,lfont=lfont,bfont=bfont

if not exist(title) then title='XCAMP'
wbase=widget_base(title=title,/column)

row1=widget_base(wbase,row=1,/frame)

;-- operation buttons

force=keyword_set(force)
select=keyword_set(select) or force

if select then begin
 selb=widget_button(row1,value='Accept Selection',uvalue='select',font=bfont,/no_rel)
 if not force then canb=widget_button(row1,value='Cancel Selection',uvalue='cancel',font=bfont,/no_rel)
endif else begin
 exitb=widget_button(row1,value='Done',uvalue='exit',font=bfont,/no_rel) 
endelse

output=widget_button(row1,value='Output',font=bfont,/menu)
printb=widget_button(output,value='Print List',font=bfont,uvalue='print')
mailb=widget_button(output,value='Mail List',font=bfont,uvalue='mail')
writeb=widget_button(output,value='Save List',font=bfont,uvalue='write')

upd=widget_button(row1,value='Update Databases',uvalue='update',font=bfont,/no_rel)


if inst ne 'S' then begin
 xmenu, 'View Campaign Details', row1, /row, /nonexclusive, /frame, $
      buttons=viewb, uvalue='view', font=bfont
 viewb = viewb(0)
 widget_control,viewb, sensitive=0
endif else viewb = -1

optb=widget_button(row1,value='Options',uvalue='options',font=bfont)
if select then helpb=widget_button(row1,value='Help',uvalue='help',font=bfont)

;-- date fields

row2=widget_base(wbase,/row)
temp=widget_base(row2,/column,/frame)
rowt=widget_base(temp,/row)
tlabel=widget_label(rowt,$
       value='Edit Start/Stop Time fields to list Campaigns for different periods:')

rowt=widget_base(temp,/row)
wops1=cw_field(rowt, Title= 'Start Time:  ',value='',$
                    /ret, xsize = 11, font = bfont,field=bfont)

wops2=cw_field(rowt,  Title='Stop Time:   ', value='',$
                    /ret, xsize = 11, font = bfont,field=bfont)

;-- search field

temp=widget_base(row2,/column,/frame)
rowt=widget_base(temp,/row)

searb=widget_button(rowt,value='Search',font=bfont,uvalue='search')
resetb=widget_button(rowt,value='Reset',uvalue='reset',font=bfont)
rowt=widget_base(temp,/row)
searchl=widget_label(rowt,value='CMP. TYPE:',font=bfont)
search=widget_text(rowt,value='',uvalue='search',/edit,font=bfont,xsize=8)

new=widget_base(wbase,/column)
alabel=widget_list(new,value='',font=lfont,ysize=1,/frame,xsize=140)
alist=widget_list(new,value=' ',font=lfont,ysize=25,xsize=140)

;-- create info structure for passing into event loop

def={cmp_no:-1}
if exist(id) then sel_id=id else sel_id=-1
mlabel=[strpad('CMP_NO',10,/aft),strpad('CMP_TYPE',21,/aft),$
        strpad('CMP_NAME',31,/aft),strpad('OBSERVER',31,/aft),$
        strpad('START',15,/aft),strpad('END',14,/aft),strpad('PARTIC.',15,/aft)]
nlabel=n_elements(mlabel)
mopt=replicate(1,nlabel)
mopt(nlabel-1)=0

;-- restore last used options

tsort=1 & rsort=0 & dsort=1
if datatype(info) eq 'STC' then begin
 if tag_exist(info,'sbut') then mopt=info.sbut
 if tag_exist(info,'tsort') then tsort=info.tsort
 if tag_exist(info,'rsort') then rsort=info.rsort
 if tag_exist(info,'dsort') then dsort=info.dsort
endif

info ={wops1:wops1,wops2:wops2,ops1:ops1,ops2:ops2,status:0,$
       viewb:viewb,alist:alist,sel_id:sel_id,help:chelp,$
       wbase:wbase,lview:0,def:def,lbase:0l,cbase:0l,search:search,$
       printb:printb,writeb:writeb,mailb:mailb,mlabel:mlabel,$
       sbut:mopt,alabel:alabel,wlabel:'',tsort:tsort,rsort:rsort,dsort:dsort}

;-- realize and center main base

xrealize,wbase,group=group,/center 

xcamp_label
xcamp_list,reset=reset

xmanager,'xcamp',wbase,modal=select,group=group
xmanager_reset,wbase,group=group,modal=select,crash='xcamp'

;-- save last times

if not xalive(wbase) then begin
 if datatype(info) eq 'STC' then begin
  status=(info.sel_id gt -1)
  if status then id=info.sel_id
 endif
 if not exist(id) then id=-1
endif

return & end
