;+
; Project     :	SOHO - CDS/SUMER
;
; Name        :	PLOT_SPLAN
;
; Purpose     :	Plot SOHO SCIENCE plan for a given date range.
;
; Explanation :
;
; Use         :	PLOT_SPLAN
;
; Inputs      :	
;
; Opt. Inputs : T1,T2 = range of date/time values to plot
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :
;               OVER     = overlay on existing plot.
;          	CHARSIZE = charsize for text labels.
;               COLOR    = Instrument color structure whose tag names are
;                          names of SOHO instruments. Values for those tags
;                          correspond to color indices to be used for plotting
;                          science plans for those instrument. If COLOR is not
;                          passed in, color #1 will be used for all instrument
;               WINDOW   = window index for output
;               LOCAL    = LOCAL
;               NOW      = plot current time
;               PLAN_ITEMS = plan items to be plotted
;               RES_ITEMS = resources (e.g., MDI, S/C Maneuver) to be plotted
;               ROW_COLORS = row color array = [color1,color2...] up to NITEMS
;                           (only useful for overriding COLOR)
;               UCOLOR = color to highlight un-time-tagged entries
;               DTYPE  = set for SOHO-DETAILED plan
;               NOCOLOR = if set, then color is switched off
;
; Common      :	MK_PLAN_SHARED
;
; Written     :	Dominic Zarro (ARC)
;
; Modified:   : Liyun Wang, GSFC/ARC, April 24, 1995
;                  Added keyword ITEMS
;               Liyun Wang, GSFC/ARC, April 25, 1995
;                  Added keyword RESOURCES
;               Zarro, GSFC/ARC, May 1, 1995
;                  Changed SCI_OBJ to SCI_SPEC in CDS timeline plot
;               Liyun Wang, GSFC/ARC, August 24, 1995
;                  Plotted plan items color coded for each instrument
;               Liyun Wang, GSFC/ARC, January 24, 1996
;                  Fixed a bug that won't plot resources in the bottom row
;               Liyun Wang, GSFC/ARC, March 1, 1996
;                  Took out thick=1 restriction when plotting
;                     day-boundary lines 
;               Liyun Wang, GSFC/ARC, Liyun Wang, GSFC/ARC
;                  Plotted XTITLE with a truncated 1-sec accuracy
;               Liyun Wang, GSFC/ARC, Liyun Wang, GSFC/ARC
;                  Change X title to VMS time format
;               Zarro, GSFC/SAC, 1 August 1997
;                  Added optional T1,T2 arguments
;		Yurow, Ronald  EITI, 5 October 2001\
;		   Added line to plot DSN 27 passes if DSN-27 resource
;		   exists.
;               Zarro (EER/GSFC), July 6, 2003 - added call to GET_SOHO_ROLL
;
; Version     :	Version 10, 5 October 2001
;-

pro plot_splan,t1,t2,over=over, color=color, charsize=charsize, window=window, $
                _extra=extra, local=local, now=now, plan_items=plan_items, $
                days=days, row_colors=row_colors, ucolor=ucolor, dtype=dtype,$
                nocolor=nocolor

;-- update SOHO_ORIENT based on date

mklog,'SOHO_ORIENT',get_soho_roll(t1)

@mk_plan_shared

;-- check if START/STOP time passed as arguments, otherwise use from COMMON

   err1=''
   temp1=anytim2tai(t1,err=err1)
   err2=''
   temp2=anytim2tai(t2,err=err2)
   if (err1 eq '') and (err2 eq '') then begin
    tstart=temp1 & tstop=temp2
   endif else begin
    if n_elements(startdis)*n_elements(stopdis) eq 0 then begin
     message, 'invalid time inputs', /cont
     return
    endif
    tstart = anytim2tai(startdis)
    tstop = anytim2tai(stopdis)
   endelse

   smart_window,window

;-- if background is white then don't use color

   if (!d.name eq 'X') and keyword_set(nocolor) then dcolor=0 else dcolor=1
   if not dcolor then begin
    sav_back=!p.background
    !p.background=!d.table_size
   endif

   ucolor=0
   if datatype(color) eq 'STC' then begin
    if tag_exist(color,'NTT') then ucolor=color.ntt
   endif

   datastart = tai2utc(tstart)
   start_hour = datastart.time / 3600000. ; start display at this hour
   stop_hour = start_hour + ((tstop - tstart) / 3600d) ; end display hour

   datastart.time = 0           ; only want day part
   daystart = utc2tai(datastart) ; start of STARTDIS in TAI

;---------------------------------------------------------------------------
;  Default row items
;---------------------------------------------------------------------------
   IF N_ELEMENTS(plan_items) EQ 0 THEN $
      plan_items = ['Telemetry', 'Command', 'Support', 'SUMER-Sci', $
                    'SUMER-Det', get_soho_inst(exclude='SUMER')]
   r_items = reverse(plan_items)

   IF NOT exist(charsize) THEN BEGIN
      IF !p.charsize NE 0.0 THEN charsize = !p.charsize ELSE charsize = 1.2
   ENDIF

   maxcharsize = 5.0*charsize

;   IF N_ELEMENTS(window) GT 0 THEN BEGIN
;      IF (window GT -1) AND (!d.name EQ 'X') THEN setwindow, window, /show
;   ENDIF

;---------------------------------------------------------------------------
;  Plot frame
;---------------------------------------------------------------------------
   IF NOT KEYWORD_SET(over) THEN BEGIN
      ylabels = r_items
      det_plan=keyword_set(dtype)
      clook=where(ylabels eq 'CELIAS',cnt)
      if (cnt gt 0) then begin
       if (not det_plan) then ylabels(clook(0))='JOP' else ylabels(clook(0))=''
      endif
      clook=where(ylabels eq 'SWAN',cnt)
      if (cnt gt 0) then begin
       if (not det_plan) then ylabels(clook(0))='SOC' else ylabels(clook(0))=''
      endif
      clook=where(ylabels eq 'ERNE',cnt)
      if (cnt gt 0) then begin
       if (not det_plan) then ylabels(clook(0))='Campaign' else ylabels(clook(0))=''
      endif
      clook=where(ylabels eq 'Command',cnt)
      if (cnt gt 0) then ylabels(clook(0))='DSN'

      ok_labels=where(ylabels ne '',cnt)
      call_proc='plot_utframe, start_hour, stop_hour, startdis, '+$
         'charsize=charsize,'+$
         'xtitle=tai2utc(tstart, /vms, /truncate)+ " UT",'+$
         'local=local, ylabels=ylabels'
      if not dcolor then call_proc=call_proc+',color=0'
      s=execute(call_proc)
   ENDIF

;---------------------------------------------------------------------------
;  Get row numbers for Telemetry and Commanding resources
;---------------------------------------------------------------------------
   tmp = grep('Telemetry', r_items, index=aa)
   IF aa(0) GE 0 THEN tlm_row = aa(0)

   tmp = grep('Command', r_items, index=aa)
   IF aa(0) GE 0 THEN cmd_row = aa(0)

;---------------------------------------------------------------------------
;  Plot in TLM row
;---------------------------------------------------------------------------
   IF exist(tlm_row) THEN BEGIN
      IF exist(tape_dump) THEN plot_resource, tape_dump, startdis, $
         row=tlm_row, color=!p.color*dcolor 
      IF exist(nrt) THEN plot_resource, nrt, startdis, row=tlm_row,color=3*dcolor
      IF exist(mdi_h) THEN plot_resource, mdi_h, startdis, row=tlm_row, color=9*dcolor
      IF exist(mdi_m) THEN plot_resource, mdi_m, startdis, row=tlm_row, color=9*dcolor
      IF exist(dsn_27)THEN plot_resource, dsn_27, startdis, row=tlm_row, color=9*dcolor
   ENDIF

;---------------------------------------------------------------------------
;  Plot in CMD row
;---------------------------------------------------------------------------
   IF exist(cmd_row) THEN BEGIN
      IF exist(dsn) THEN plot_resource, dsn, startdis,row=cmd_row,color=dcolor
      IF exist(svm) THEN plot_resource, svm, startdis, row=cmd_row,color=2*dcolor
      IF exist(trcr) THEN plot_resource, trcr, startdis, row=cmd_row,color=4*dcolor
      IF exist(dlydtimes) THEN plot_resource, dlydtimes, startdis, $
         row=cmd_row, color=10*dcolor
      IF exist(tnorcr) THEN plot_resource, tnorcr, startdis, row=cmd_row, $
         color=3*dcolor
   ENDIF

;---------------------------------------------------------------------------
;  Plot Ground support
;---------------------------------------------------------------------------
   IF exist(support) THEN BEGIN
      tmp = grep('Support', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         IF datatype(color) EQ 'STC' THEN BEGIN
            IF tag_exist(color, 'GROUND') THEN pcolor = color.ground ELSE $
               pcolor = 1
         ENDIF ELSE pcolor = 1  ; white
         plot_resource, support, startdis, row=aa(0), color=pcolor*dcolor, $
            charsize=charsize
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot CDS Flag
;---------------------------------------------------------------------------
   IF exist(cds_flag) THEN BEGIN
      tmp = grep('CDS-FLAG', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         plan_start = (cds_flag.rcvr_start - daystart) / 3600d
         plan_stop = (cds_flag.rcvr_stop  - daystart) / 3600d
         plan_study = cds_flag.sci_spec
         IF datatype(color) EQ 'STC' THEN $
            pcolor = color.cds ELSE pcolor = 4
         plot_item, plan_start, plan_stop, aa(0), plan_study, $
            maxcharsize=maxcharsize, color=pcolor*dcolor
         inst_name = 'C'
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot CDS Science
;---------------------------------------------------------------------------
   IF exist(cds_science) THEN BEGIN
      tmp = grep('CDS-SCI', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         IF datatype(color) EQ 'STC' THEN pcolor = color.cds
         plan_start = (cds_science.start_time - daystart) / 3600d
         plan_stop = (cds_science.end_time  - daystart) / 3600d
         plan_study = cds_science.sci_obj
         IF datatype(color) EQ 'STC' THEN $
            pcolor = color.cds ELSE pcolor = 1
         plot_item, plan_start, plan_stop, aa(0), plan_study, $
            maxcharsize=maxcharsize, color=pcolor*dcolor
         cds_sci = 1
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot CDS Detail
;---------------------------------------------------------------------------

   IF exist(cds_detail) THEN BEGIN
      tmp = grep('CDS-DET', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         plan_start = (cds_detail.date_obs - daystart) / 3600d
         plan_stop = (cds_detail.date_end  - daystart) / 3600d
         plan_study = cds_detail.sci_spec
         plan_tt = cds_detail.time_tagged
         IF datatype(color) EQ 'STC' THEN $
            pcolor = color.cds ELSE pcolor = 2
         plot_item, plan_start, plan_stop, aa(0), plan_study, tt=plan_tt, $
            maxcharsize=maxcharsize, color=pcolor*dcolor, ucolor=ucolor*dcolor
         inst_name = 'C'
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot CDS Alt
;---------------------------------------------------------------------------
   IF exist(cds_alt) THEN BEGIN
      tmp = grep('CDS-ALT', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         plan_start = (cds_alt.start_time - daystart) / 3600d
         plan_stop = (cds_alt.end_time  - daystart) / 3600d
         plan_study = cds_alt.sci_spec
         IF datatype(color) EQ 'STC' THEN $
            pcolor = color.cds ELSE pcolor = 2
         plot_item, plan_start, plan_stop, aa(0), plan_study, $
            maxcharsize=maxcharsize, color=pcolor*dcolor
         inst_name = 'C'
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot SUMER Details
;---------------------------------------------------------------------------
   IF exist(sumer_detail) THEN BEGIN
      tmp = grep('SUMER-DET', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         plan_start = (sumer_detail.date_obs - daystart) / 3600d
         plan_stop = (sumer_detail.date_end  - daystart) / 3600d
         plan_study = sumer_detail.sci_spec
         plan_tt = sumer_detail.time_tagged
         IF datatype(color) EQ 'STC' THEN $
            pcolor = color.sumer ELSE pcolor = 2
         plot_item, plan_start, plan_stop, aa(0), plan_study, tt=plan_tt, $
            maxcharsize=maxcharsize, color=pcolor*dcolor, ucolor=ucolor*dcolor
         inst_name = 'S'
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot SUMER Science
;---------------------------------------------------------------------------
   IF exist(sumer_science) THEN BEGIN
      tmp = grep('SUMER-SCI', r_items, index=aa)
      IF aa(0) GE 0 THEN BEGIN
         IF datatype(color) EQ 'STC' THEN pcolor = color.sumer
         plan_start = (sumer_science.start_time - daystart) / 3600d
         plan_stop = (sumer_science.end_time  - daystart) / 3600d
         plan_study = sumer_science.sci_obj
         IF datatype(color) EQ 'STC' THEN $
            pcolor = color.sumer ELSE pcolor = 1
         plot_item, plan_start, plan_stop, aa(0), plan_study, $
            maxcharsize=maxcharsize, color=pcolor*dcolor
         sumer_sci = 1
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Plot "other" instruments' plans
;---------------------------------------------------------------------------
   IF KEYWORD_SET(dtype) THEN BEGIN
      IF exist(soho_dplan) THEN BEGIN
         plan = soho_dplan
         tags = TAG_NAMES(plan)
         ilabel_backup = (WHERE(tags EQ 'SCI_OBJ'))(0)
         ilabel = (WHERE(tags EQ 'SCI_SPEC'))(0)
      ENDIF
   ENDIF ELSE BEGIN
      IF exist(soho_splan) THEN BEGIN
         plan = soho_splan
         tags = TAG_NAMES(plan)
         ilabel = (WHERE(tags EQ 'SCI_OBJ'))(0)
      ENDIF
   ENDELSE

   IF datatype(plan) EQ 'STC' THEN BEGIN
      itime = get_plan_itime(plan, err=err)
      IF itime GE 0 THEN BEGIN
         IF NOT exist(inst_name) THEN BEGIN
            inst = get_soho_inst(/short)
         ENDIF ELSE BEGIN
            IF inst_name EQ 'C' THEN $
               inst = get_soho_inst(/short, exclude='C') $
            ELSE $
               inst = get_soho_inst(/short, exclude='S')
         ENDELSE
         ninst = N_ELEMENTS(inst)
         IF exist(color) THEN BEGIN
            IF datatype(color) EQ 'STC' THEN tcolor = color ELSE pcolor = color
         ENDIF ELSE pcolor = 1

         FOR j=0, ninst-1 DO BEGIN
            aa = -1
            IF inst(j) EQ 'C' THEN BEGIN
               IF NOT exist(cds_sci) THEN BEGIN
                  IF exist(tcolor) THEN pcolor = color.cds
                  tmp = grep('CDS', r_items, index=aa, /exact)
               ENDIF
            ENDIF ELSE IF inst(j) EQ 'S' THEN BEGIN
               IF NOT exist(sumer_sci) THEN BEGIN
                  IF exist(tcolor) THEN pcolor = color.sumer
                  tmp = grep('SUMER', r_items, index=aa, /exact)
               ENDIF
            ENDIF ELSE BEGIN
               IF exist(tcolor) THEN pcolor = get_inst_color(inst(j), color)
               tmp = grep(get_soho_inst(inst(j)), r_items, index=aa, /exact)
            ENDELSE
            row = aa(0)
            IF row GE 0 THEN BEGIN
               ii = WHERE(plan.instrume EQ inst(j), nplans)
               IF nplans GT 0 THEN BEGIN
                  i_plans = plan(ii)
                  FOR i=0, nplans-1 DO BEGIN
                     tplan = i_plans(i)
                     plan_start = tplan.(itime)
                     plan_stop = tplan.(itime+1)
                     plan_study = tplan.(ilabel)
                     IF STRTRIM(plan_study) EQ '' AND KEYWORD_SET(dtype) THEN $
                        plan_study = tplan.(ilabel_backup)
;---------------------------------------------------------------------------
;                    Arrays are in TAI format, convert to hours since start
;                    STARTDIS day
;---------------------------------------------------------------------------
                     plan_start = (plan_start - daystart) / 3600d
                     plan_stop = (plan_stop  - daystart)  / 3600d
                     plot_item, plan_start, plan_stop, row, plan_study, $
                        maxcharsiz=maxcharsize, color=pcolor*dcolor
                  ENDFOR
               ENDIF
            ENDIF
         ENDFOR
      ENDIF
   ENDIF

   IF KEYWORD_SET(now) THEN BEGIN
;---------------------------------------------------------------------------
;     Plot current time
;---------------------------------------------------------------------------
      get_utc, utc
      curr = (utc2tai(utc)-daystart)/3600.d
      OPLOT, [curr, curr], !y.crange, linestyle=3, color=2*dcolor
   ENDIF

   IF KEYWORD_SET(days) THEN BEGIN
;---------------------------------------------------------------------------
;     Plot day boundaries
;---------------------------------------------------------------------------
      startday = tai2utc(startdis)
      startday.time = 0
      startday = utc2tai(startday)
      stopday = tai2utc(stopdis)
      stopday.time = 0
      stopday = utc2tai(stopday)
      day_mark = (startday-daystart)/3600.d
      pstart = MIN(!x.crange)
      pend = MAX(!x.crange)
      WHILE (day_mark LE pend) DO BEGIN
         IF (day_mark NE pend AND day_mark NE pstart) THEN $
            OPLOT, [day_mark, day_mark], !y.crange, color=1*dcolor, $
            linestyle=5
         day_mark = day_mark+24.d
      ENDWHILE
   ENDIF

   if exist(sav_back) then !p.background=sav_back
   smart_window,window,/set
   return & end
