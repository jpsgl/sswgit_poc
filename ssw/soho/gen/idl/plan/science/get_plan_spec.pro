;+
; Project     : SOHO - CDS     
;                   
; Name        : get_plan_spec
;               
; Purpose     : construct SCI_SPEC title for PLAN
;               
; Category    : Planning
;               
; Explanation : If SCI_SPEC is blank then construct it by
;               concatanating the STUDY TITLE and STUDY VAR fields.
;               
; Syntax      : IDL> sci_spec=get_plan_spec(plan)
;    
; Examples    : 
;
; Inputs      : PLANS - plans structure array 
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : None.
;
; Common      : None.
;               
; Restrictions: Not used for SCIENCE plans (obviously)
;               
; Side effects: None.
;               
; History     : Version 1,  1-July-1995,  D M Zarro.  Written
;
; Contact     : DMZARRO
;-            


function get_plan_spec,plan

title=''
if tag_exist(plan,'SCI_SPEC') then title=plan.sci_spec
type=get_plan_type(plan,inst=inst)
if (type ne 2) and (type ge 0) then begin
 nobs=n_elements(plan)
 for i =0,nobs-1 do begin
  study_id=(plan.study_id)(i)
  if (study_id gt -1) and (strtrim(title(i),2) eq '') then begin
   if (inst eq 'C') then begin
    studyvar=(plan.studyvar)(i)
    study_def=call_function('get_cds_study',study_id,studyvar)
    studyvar=study_def.studyvar
    study_title=strtrim(study_def.obs_prog,2)
    sv_desc='v'+strtrim(string(studyvar),2)
    study_var_tit='/'+sv_desc
   endif
   if inst eq 'S' then begin
    study_var_tit='' 
    study_def=call_function('get_sumer_study',study_id)
    study_title=strtrim(study_def.title,2)
   endif
   if study_def.study_id gt -1 then $
    title(i) =strtrim(study_title+study_var_tit,2)
  endif
 endfor
 if nobs eq 1 then title=title(0)
endif

return,strmid(title,0,50)  & end
