;+
; Project     : SOHO - CDS
;
; Name        : CHECK_KAP_ANOMALY
;
; Purpose     : check lines in KAP files for anomalous activity keywords
;
; Category    : operations, planning
;
; Explanation : Certain KAP files produced by ECS do not appear to conform
;               to Interface Control Document conventions. 
;
; Syntax      : IDL> anomalous=check_kap_anomaly(line)
;
; Inputs      : LINE = line in KAP file
;
; Opt. Inputs : None
;
; Outputs     : 
;
; Opt. Outputs: ANOMALOUS = 1 if line contains ACTIVITY keyword and there
;               is a resource associated with it. The following resources
;               should not be associated with an activity:
;               SVM_reserved, PAYLOAD_reserved, TLM_Tape_Dump, MDI_H, MDI_M,
;               Throughput_RCR, Clock_Adjust, Spacecraft_Maneuver.
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: If present and anomalous, the ACTIVITY keyword is removed from line
;
; History     : Version 1,  26-Feb-1995,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function check_kap_anomaly,line

on_error,1

anomalous=0

if datatype(line) ne 'STR' then return,0

input_line=line

activity=strpos(strupcase(line),'ACTIVITY')

dsn=strpos(strupcase(line),'DSN')
if (activity gt -1) and (dsn gt -1) then line='DSN_contact'

svm=strpos(strupcase(line),'SVM')
if (activity gt -1) and (svm gt -1) then line='SVM_Reserved'

payload=strpos(strupcase(line),'PAYLOAD')
if (activity gt -1) and (payload gt -1) then line='Payload_Reserved'

through=strpos(strupcase(line),'THROUGH')
if (activity gt -1) and (through gt -1) then line='Throughput_RCR'

tape=strpos(strupcase(line),'TAPE')
if (activity gt -1) and (tape gt -1) then line='TLM_Tape_Dump'

mdi_m=strpos(strupcase(line),'MDI_M')
if (activity gt -1) and (mdi_m gt -1) then line='TLM_MDI_M'

mdi_h=strpos(strupcase(line),'MDI_H')
if (activity gt -1) and (mdi_h gt -1) then line='TLM_MDI_H'

clock=strpos(strupcase(line),'CLOCK')
if (activity gt -1) and (clock gt -1) then line='Clock_Adjust'

space=strpos(strupcase(line),'SPACE')
if (activity gt -1) and (space gt -1) then line='Spacecraft_Maneuver'

;-- look for special SOC keywords

jop=strpos(strupcase(line),'JOP')
ical=strpos(strupcase(line),'ICAL')
soc=strpos(strupcase(line),'SOC')
cmp=strpos(strupcase(line),'CMP')
if (activity gt -1) then begin
 if (jop gt -1) or (ical gt -1) or (soc gt -1) or (cmp gt -1) then begin
  line=trim(line)
  line=strep(line,'ACTIVITY','SCIPLAN')
 endif
endif

return,fix(input_line ne line) & end

