;+
; Project     : SOHO-CDS
;
; Name        : MK_PLAN_CAMP
;
; Purpose     : Check validity of campaign ID
;
; Category    : planning
;
; Explanation :
;
; Syntax      : MK_PLAN_CAMP,PLAN
;
; Examples    :
;
; Inputs      : PLAN = plan structure to check
;
; Opt. Inputs : None
;
;
; Outputs     : None
;
; Opt. Outputs:
;
; Keywords    : ERR = invalid error message
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 28 October 1996, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro mk_plan_camp,plan,err=err,_extra=extra,status=status

;-- check input

err='' & status=1
type=get_plan_type(plan)
if type lt 0 then return
itime=get_plan_itime(plan)
if (itime lt 0) then return
if not tag_exist(plan,'CMP_NO') then begin
 err='Input plan lacks CMP_NO field'
 return
endif

if (plan.cmp_no eq 1) or (plan.cmp_no eq 0) then return

;-- if plan.cmp_no gt 0 then check validity

tstart=anytim2utc(plan.(itime))
tend=anytim2utc(plan.(itime+1))
dprint,'% MK_PLAN_CAMP: Checking campaign #...'
find_campaign,tstart,tend,cmp,nc
if nc gt 0 then begin
 cur_cmp=cmp.cmp_no
 clook=where(plan.cmp_no eq cur_cmp,count)
 if count eq 0 then begin
  err='Campaign No. '+trim(string(plan.cmp_no))+' does not match campaigns during planned interval.'
  status=0
 endif
endif

return & end










