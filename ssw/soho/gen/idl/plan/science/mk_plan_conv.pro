;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_CONV
;
; Purpose     : convert plan time tags from TAI to UTC
;
; Use         : NPLAN=MK_PLAN_CONV(PLAN)
;
; Inputs      : PLAN = plan structure definition
;
; Opt. Inputs : ITIME = index of time field
;
; Outputs     : OPLAN = same as PLAN but with time fields in UTC string format
;
; Opt. Outputs: None.
;
; Keywords    : NOPOINT = if set, then remove pointing tags
;
; Explanation : None.
;
; Calls       : None.
;
; Common      : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Planning
;
; Prev. Hist. : None.
;
; Written     :	Version 1, Zarro (ARC/GSFC) 29 January 1995.
;-


function mk_plan_conv,plan,itime=itime,nopoint=nopoint,err=err

on_error,1
err=''

if n_elements(itime) eq 0 then begin
 type=get_plan_type(plan)
 if type eq -1 then return,plan
 itime=get_plan_itime(plan)
 if itime eq -1 then return,plan
 index=itime
endif else begin
 tags=tag_names(plan)
 if datatype(itime) eq 'STR' then begin
  clook=where(strupcase(strtrim(itime,2)) eq tags,cnt)
  if cnt eq 0 then return,plan
  index=clook(0) 
 endif else index=itime
endelse

nplan=plan

if keyword_set(nopoint) then nplan=rem_tag(nplan,'POINTINGS')

;-- replace TAI time fields with UTC string equivalents

for i=0,n_elements(nplan)-1 do begin
 tplan=rep_tag_value(nplan(i),tai2utc(nplan(i).(index),/ecs),index)
 tplan=rep_tag_value(tplan,tai2utc(tplan.(index+1),/ecs),index+1)
 if tag_exist(tplan,'INSTRUME') then begin
  err=''
  get_instrument,tplan.instrume,desc,err=err
  if err eq '' then tplan.instrume=desc.name
 endif
 oplan=concat_struct(oplan,tplan)
endfor

return,oplan & end

