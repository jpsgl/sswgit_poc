;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       KAP_CONTENTS()
;
; PURPOSE:
;       Read in the latest KAP (or CAP or IAP) file for the given date
;
; CATEGORY:
;       Science Planning
;
; EXPLANATION:
;
; SYNTAX:
;       Result = kap_contents(date)
;
; INPUTS:
;       DATE - Date for which the KAP file is read; can be in any CDS
;              time format
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       result - String array containing contents of the KAP (default)
;                or CAP or IAP file; a null string will be returned if
;                error occurs for any reason
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       ERROR - Named variable containing any error message. If no
;               error occurs, ERROR will be a null string
;       IAP   - Set this keyword to read the latest IAP file
;       CAP   - Set this keyword to read the latest CAP file
;       VERSION - A named variable containing the file version
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, August 21, 1995, Liyun Wang, GSFC/ARC. Written
;       Version 2, September 1, 1995, Liyun Wang, GSFC/ARC
;          Added keyword VERSION and keywords IAP, CAP so that CAP and
;             IAP files can be read as well
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;

FUNCTION kap_contents, date, error=error, iap=iap, cap=cap, version=version,$
        file=file

   error = ''
   version = -1
   date_str = date_code(date, error=error)
   file=''

   IF NOT KEYWORD_SET(IAP) AND NOT KEYWORD_SET(CAP) THEN BEGIN
      valid_env = 'SOHO_EAP'
      pattern = '*.KAP'
   ENDIF ELSE IF KEYWORD_SET(IAP) THEN BEGIN
      inst = which_inst()
      IF inst EQ 'C' THEN valid_env = 'CDS_IAP_W'
      IF inst EQ 'S' THEN valid_env = 'SUMER_IAP'
      pattern = '*.IAP'
   ENDIF ELSE BEGIN
      valid_env = 'SOHO_EAP'
      pattern = '*.CAP'
   ENDELSE

   path = STRTRIM(getenv(valid_env),2)
   IF path EQ '' THEN BEGIN
      error = valid_env+' not defined.'
      RETURN, ''
   ENDIF

   files = loc_file('*'+date_str+pattern, path=path,count=count)

   IF count eq 0 THEN BEGIN
      error = 'No files found for '+anytim2utc(date,/ecs,/date)
      RETURN, ''
   ENDIF

   index = STRPOS(files(0), date_str)
   vers = FIX(STRMID(files, index+8, 3))
   version = MAX(vers)
   
   ii = (WHERE(vers EQ version))(0)
   file = files(ii)

   OPENR, unit, file, /GET_LUN, error=error
   IF error NE 0 OR unit LT 0 THEN BEGIN
      error = 'Unable to open file '+file
      RETURN, ''
   ENDIF

   dprint, 'Reading '+file+'...'
   line = ''
   WHILE (NOT EOF(unit)) DO BEGIN
      READF, unit, line
      IF N_ELEMENTS(contents) EQ 0 THEN contents = line ELSE $
         contents = [contents, line]
   ENDWHILE
   FREE_LUN, unit
   RETURN, contents
END

;---------------------------------------------------------------------------
; End of 'kap_contents.pro'.
;---------------------------------------------------------------------------
