;---------------------------------------------------------------------------
; Document name: mk_plan_shift.pro
; Created by:    Liyun Wang, GSFC/ARC, January 6, 1995
;
; Last Modified: Fri May 26 11:59:10 1995 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       MK_PLAN_SHIFT
;
; PURPOSE:
;       Event handler of draw events for MK_SOHO's "Shift" button
;
; INPUTS:
;       EVENT - Event structure generated from the draw widget of
;               MK_SOHO_PLAN or MK_SUMER_PLAN 
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS: 
;       None.
;
; CALLS:
;       TAI2UTC, UTC2TAI, DELVARX
;
; COMMON BLOCKS:
;       BUTTONS - Internal common block used by mk_plan_shift
;       Others  - Listed in mk_soho_com.pro (CDS version) or in
;                 mk_sumer_com.pro (SUMER version)  
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Planning
;
; PREVIOUS HISTORY:
;       Written January 6, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1, January 6, 1995, Created
;       Version 2, Liyun Wang, GSFC/ARC, April 28, 1995
;          Improved the way of handling the dragging mode
;
; VERSION:
;       Version 2, April 28, 1995
;-
;
PRO plan_shift_ctime, xpos, low=low
;---------------------------------------------------------------------------
;  Routine to update content of date_obs and time_obs, if LOW is set; or
;  date_end, time_end if LOW is not set
;
;  XPOS is in units of hours
;---------------------------------------------------------------------------
   COMMON saved_startdis, my_start, day
   COMMON for_shift, startdis, stopdis, date_obs, time_obs, date_end, time_end
   IF N_ELEMENTS(my_start) EQ 0 THEN BEGIN
      my_start = startdis
      day=tai2utc(startdis) &  day.time=0
      day=utc2tai(day)
   ENDIF
   IF my_start NE startdis THEN BEGIN
      my_start = startdis
      day=tai2utc(startdis) &  day.time=0
      day=utc2tai(day)
   ENDIF
   cur_time=day+xpos*3600.d
   IF KEYWORD_SET(low) THEN BEGIN
      WIDGET_CONTROL,date_obs,set_value=tai2utc(cur_time,/ecs,/date)
      WIDGET_CONTROL,time_obs,set_value=tai2utc(cur_time,/ecs,/time)
   ENDIF ELSE BEGIN
      WIDGET_CONTROL,date_end,set_value=tai2utc(cur_time,/ecs,/date)
      WIDGET_CONTROL,time_end,set_value=tai2utc(cur_time,/ecs,/time)
   ENDELSE
END

PRO plan_shift_utime, tstart, tstop
;---------------------------------------------------------------------------
;  Routine to update contents of both (date_obs, time_obs) and (date_end,
;  time_end)
;
;  TSTART and TSTOP are all in units of hours
;---------------------------------------------------------------------------
   COMMON saved_startdis, my_start, day
   COMMON for_shift, startdis, stopdis, date_obs, time_obs, date_end, time_end
   IF N_ELEMENTS(my_start) EQ 0 THEN BEGIN
      my_start = startdis
      day=tai2utc(startdis) &  day.time=0
      day=utc2tai(day)
   ENDIF
   IF my_start NE startdis THEN BEGIN
      my_start = startdis
      day=tai2utc(startdis) &  day.time=0
      day=utc2tai(day)
   ENDIF
   start_time = day+tstart*3600.d
   end_time   = day+tstop*3600.d
   WIDGET_CONTROL,date_obs,set_value=tai2utc(start_time,/ecs,/date)
   WIDGET_CONTROL,time_obs,set_value=tai2utc(start_time,/ecs,/time)
   WIDGET_CONTROL,date_end,set_value=tai2utc(end_time,/ecs,/date)
   WIDGET_CONTROL,time_end,set_value=tai2utc(end_time,/ecs,/time)
END

PRO mk_plan_shift, event, shift_stc=shift_stc
;---------------------------------------------------------------------------
; Event handler of the draw widget for manipulating a selected entry
;
; This routine should only be called when chg_flag is 1
;---------------------------------------------------------------------------
   COMMON buttons, btn_l, btn_m, btn_r, a00, b00, mx0, low_end, old_dev
   COMMON for_shift, startdis, stopdis, date_obs, time_obs, date_end, time_end

   IF datatype(shift_stc) NE 'STC' THEN BEGIN
      MESSAGE, 'Requires a Shift STC..', /cont
      RETURN
   ENDIF
   startdis = shift_stc.startdis
   stopdis = shift_stc.stopdis
   date_obs = shift_stc.date_obs
   time_obs = shift_stc.time_obs
   date_end = shift_stc.date_end
   time_end = shift_stc.time_end
   IF shift_stc.row LT 0 THEN BEGIN
      y_row = !y.crange 
   ENDIF ELSE BEGIN
      y_row = [shift_stc.row+0.01,shift_stc.row+0.99]
   ENDELSE
   IF N_ELEMENTS(btn_l) EQ 0 THEN BEGIN
      btn_l = 0
      btn_m = 0
      btn_r = 0
   ENDIF
   lstyle = 1
   color = !d.n_colors-1
   temp = CONVERT_COORD(event.x,event.y,/device,/to_data)
   cx = temp(0) & cy = temp(1)
   IF event.type EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;     One of buttons is down (pressed), start dragging mode
;---------------------------------------------------------------------------
      IF event.press EQ 1 AND shift_stc.chg_flag EQ 1 THEN BEGIN
         btn_l = 1
         mx0 = cx
         a00 = shift_stc.htime(0)
         b00 = shift_stc.htime(1)
         IF ABS(mx0-a00) LT ABS(mx0-b00) THEN $
            low_end = 1 ELSE low_end = 0
         DEVICE, get_graphics = old_dev, set_graphics = 6
         WIDGET_CONTROL, shift_stc.chgb, sensitive = 0
      ENDIF
      IF event.press EQ 2 THEN BEGIN
         btn_m = 1
         mx0 = cx
         a00 = shift_stc.htime(0)
         b00 = shift_stc.htime(1)
         DEVICE, get_graphics = old_dev, set_graphics = 6
         WIDGET_CONTROL, shift_stc.chgb, sensitive = 0
      ENDIF
      IF event.press EQ 4 THEN btn_r = 1
   ENDIF ELSE IF event.type EQ 1 THEN BEGIN
;---------------------------------------------------------------------------
;     One of buttons is up (released), end dragging mode
;---------------------------------------------------------------------------
      IF event.release EQ 1 AND shift_stc.chg_flag EQ 1 THEN BEGIN
         btn_l = 0
         shift_stc.htime = [a00,b00]
      ENDIF
      IF event.release EQ 2 THEN BEGIN
         btn_m = 0
         shift_stc.htime = [a00,b00]
      ENDIF
      IF event.press EQ 4 THEN btn_r = 0
      WIDGET_CONTROL, shift_stc.chgb, sensitive = 1
      IF N_ELEMENTS(old_dev) NE 0 THEN BEGIN
         DEVICE, set_graphics = old_dev
         delvarx, old_dev
      ENDIF
   ENDIF
   IF event.type EQ 2 AND N_ELEMENTS(old_dev) NE 0 THEN BEGIN
;---------------------------------------------------------------------------
;     Deal with the motion event when dragging mode is on
;---------------------------------------------------------------------------
      dx = cx-mx0
      IF btn_l AND shift_stc.chg_flag EQ 1 THEN BEGIN
         dx = cx-mx0
         IF low_end THEN BEGIN
            PLOTS, [a00,a00], y_row, linestyle=lstyle, noclip=0, $
               color = color
            EMPTY
            a00 = (shift_stc.htime(0)+dx) < b00
            IF a00 LT shift_stc.tlimit(0) THEN a00 = shift_stc.tlimit(0)
            PLOTS, [a00,a00], y_row, linestyle=lstyle, noclip=0, $
               color = color
            plan_shift_ctime, a00, /low
         ENDIF ELSE BEGIN
            PLOTS, [b00,b00], y_row, linestyle=lstyle, noclip=0, $
               color = color
            EMPTY
            b00 = (shift_stc.htime(1)+dx) > a00
            IF b00 GT shift_stc.tlimit(1) THEN b00 = shift_stc.tlimit(1)
            PLOTS, [b00,b00], y_row, linestyle=lstyle, noclip=0, $
               color = color
            plan_shift_ctime, b00
         ENDELSE
         WAIT, 0.1
      ENDIF
      IF btn_m THEN BEGIN
         PLOTS, [a00,a00], y_row, linestyle=lstyle, noclip=0, $
            color = color, $
            clip = [shift_stc.tlimit(0),y_row(0),shift_stc.tlimit(1),y_row(1)]
         PLOTS, [b00,b00], y_row, linestyle=lstyle, noclip=0, $
            color = color, $
            clip = [shift_stc.tlimit(0),y_row(0),shift_stc.tlimit(1),y_row(1)]
         EMPTY
         temp = shift_stc.htime(1)-shift_stc.htime(0)
         IF shift_stc.t_flag(0) EQ 1 THEN BEGIN
;---------------------------------------------------------------------------
;           a00 can be less than tlimit(0)
;---------------------------------------------------------------------------
            a00 = (shift_stc.htime(0)+dx) < shift_stc.tlimit(1)
            b00 = a00+temp
            IF shift_stc.t_flag(1) EQ 0 AND b00 GT shift_stc.tlimit(1) THEN $
               b00 = shift_stc.tlimit(1)
            IF b00 LT shift_stc.tlimit(0) THEN BEGIN
               b00 = shift_stc.tlimit(0)
               a00 = b00-temp
            ENDIF
         ENDIF ELSE BEGIN
            a00 = shift_stc.tlimit(0) > (shift_stc.htime(0)+dx) < $
               shift_stc.tlimit(1)
            b00 = a00+temp
            IF shift_stc.t_flag(1) EQ 0 AND b00 GT shift_stc.tlimit(1) THEN $
               b00 = shift_stc.tlimit(1)
         ENDELSE
         PLOTS, [a00,a00], y_row, linestyle=lstyle, noclip=0, $
            color = color, $
            clip = [shift_stc.tlimit(0),y_row(0),shift_stc.tlimit(1),y_row(1)]
         PLOTS, [b00,b00], y_row, linestyle=lstyle, noclip=0, $
            color = color, $
            clip = [shift_stc.tlimit(0),y_row(0),shift_stc.tlimit(1),y_row(1)]
         plan_shift_utime, a00, b00
         WAIT, 0.1
      ENDIF
      IF btn_r THEN BEGIN
      ENDIF
   ENDIF
   RETURN
END

;---------------------------------------------------------------------------
; End of 'mk_plan_shift.pro'.
;---------------------------------------------------------------------------
