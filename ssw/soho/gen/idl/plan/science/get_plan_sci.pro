;+
; Project     :	SOHO - CDS
;
; Name        :	GET_PLAN_SCI
;
; Purpose     :	replace blank plan fields with SCI_PLAN values
;
; Explanation :
;
; Use         :	OPLAN=GET_PLAN_SCI(PLAN)
;
; Inputs      :	PLAN = plan structure (scalar)
;
; Opt. Inputs :	None.
;
; Outputs     :	OPLAN = modified plan structure with blank fields
;               replaced by corresponding SCI_PLAN fields
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	GET_PLAN
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC),  18 March 1995
;-

function get_plan_sci,plan

on_error,1

type=get_plan_type(plan,inst=inst)
if type eq 2 then return,plan

if (type eq -1) then begin
 if exist(plan) then return,plan else return,type
endif

oplan=plan

;-- inherit SCIENCE plan

itime=get_plan_itime(plan)

if (oplan.(itime) gt 0.) and (type ne 2) then begin
 message,'checking SCIENCE plan...',/con
 err='' & get_plan,inst,oplan.(itime),splan,err=err
 if err eq '' then begin
  ptags=tag_names(oplan)
  stags=tag_names(splan)
  for i=0,n_elements(ptags)-1 do begin
   clook=where(ptags(i) eq stags,cnt)
   if cnt gt 0 then begin
    field=oplan.(i)
    copy=0
    if datatype(field) eq 'STR' then copy=strtrim(field,2) eq ''
    if copy then oplan.(i)=splan.(clook(0))
    if ptags(i) eq 'CMP_NO' then begin
     if oplan.cmp_no lt 0 then oplan.cmp_no=splan.cmp_no
    endif
   endif
  endfor
 endif
endif

return,oplan & end

