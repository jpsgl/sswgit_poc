;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       XCLONE_PLAN 
;
; PURPOSE:
;       Widget program for CLONE_PLAN.
;
; EXPLANATION:
;
; CALLING SEQUENCE:
;       xclone_plan  [,date=date]
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       DATE - Date/Time in any CDS format, the default date for which
;              a cloned plan is made
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       STATUS - A structure that has the following tags:
;                STATUS  - 1 for success, 0 for failure
;                CLONED  - A string scalar or vector indicating type
;                          of plans being cloned
;                CLONE_ERR - A string scalar or vector, error messages
;                            passed back from CLONE_PLAN
;
; KEYWORD PARAMETERS:
;       ERRMSG    - Error message returned
;       GROUP     - widget id of calling widget program
;       MODAL     - set freeze calling program
;       SOHO_SCI  - Clone SOHO wide Science plans
;       DEF_CLONE - Default plan type to be cloned
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       Plan entries in database may be modified.
;
; CATEGORY:
;       Planning, widgets
;
; PREVIOUS HISTORY:
;       Written May 2, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1, created, Liyun Wang, GSFC/ARC, May 2, 1995
;       Version 2, Liyun Wang, GSFC/ARC, May 11, 1995
;          Added keyword ERRMSG and STATUS
;       Version 3, Liyun Wang, GSFC/ARC, June 28, 1995
;          Added privilege check
;       Version 4, August 30, 1995, Liyun Wang, GSFC/ARC
;          Added SOHO_SCI keyword for cloning SOHO wide SCI plans
;          Added DEF_CLONE keyword
;          Added a button to clone plans between two dates
;          Took away FONT keyword
;
; VERSION:
;       Version 4, August 30, 1995
;-

;---------------------------------------------------------------------------
;  Event handler
;---------------------------------------------------------------------------
PRO xclone_plan_event, event

   ON_ERROR,1

   WIDGET_CONTROL, event.top, get_uvalue = unseen

   info=get_pointer(unseen,/no_copy)
   if datatype(info) ne 'STC' then return

;-- check time strings

   widget_control,info.from_str,get_value=in_string 
   err=''
   stime=anytim2utc(in_string(0),err=err,/vms,/date,/ecs) 
   if err ne '' then xack,err,group=event.top,/modal else begin
    info.from_date=stime 
    widget_control,info.from_str,set_value=info.from_date
   endelse

   widget_control,info.to_str,get_value=in_string 
   err=''
   stime=anytim2utc(in_string(0),err=err,/vms,/date,/ecs) 
   if err ne '' then xack,err,group=event.top,/modal else begin
    info.to_date=stime 
    widget_control,info.to_str,set_value=info.to_date 
   endelse

   WIDGET_CONTROL, event.id, get_uvalue = uvalue
   IF (N_ELEMENTS(uvalue) EQ 0) THEN uvalue=''
   wtype=WIDGET_INFO(event.id,/type)
   uvalue=STRTRIM(uvalue,2)

   CASE (uvalue) OF

    'DONE': xkill,event.top

    'BETWEEN': info.between = event.select

    'WARN': info.warn = 1-event.select

    'GO': BEGIN
     info=rep_tag_value(info,'','clone_err')
     info=rep_tag_value(info,'','cloned')

     IF NOT mk_plan_priv(info.to_date,err=err) THEN begin
      xack,err,/icon,group=event.top
     endif else begin

;---------------------------------------------------------------------------
;        Do the cloning here
;---------------------------------------------------------------------------

      plans_idx = info.plans_idx
      plans = info.plans
      aa = WHERE(plans_idx EQ 1, cnt)
      IF (cnt GT 0) THEN BEGIN
       if info.warn then begin
        do_it = xanswer(['Cloned plans may overwrite existing ' + $
                         'plans in database.', 'Are you sure?'], $
                          group=event.top)
       endif else do_it=1
       IF do_it THEN BEGIN
        WIDGET_CONTROL, info.mess, set_value='Please wait. Cloning....'
        WIDGET_CONTROL, /hour
        WIDGET_CONTROL, event.top, sensitive=0
        FOR i=0, N_ELEMENTS(info.plans)-1 DO BEGIN
         IF plans_idx(i) EQ 1 THEN BEGIN
          inst = STRMID(plans(i),0,1)
          chk = STRPOS(plans(i), '-')
          IF chk GT -1 THEN BEGIN
;---------------------------------------------------------------------------
;                       Plan type is determined from its string
;---------------------------------------------------------------------------
           type_str = STRMID(plans(i), chk+1, STRLEN(plans(i)))
           CASE (type_str) OF
            'DET': type = 0
            'FLAG': type = 1
            'SCI': type = 2
            'ALT': type = 3
            ELSE: type = 2
           ENDCASE
          ENDIF ELSE type = 2 ; SOHO SCI plans
          err = ''
          cmd = 'mk_plan_clone, info.from_date, info.to_date, '+$
                'err=err, between=info.between, type=type, inst=inst'
          s = EXECUTE(cmd)
          IF err EQ '' THEN BEGIN
           cloned = info.cloned
           aa = grep(plans(i), cloned,/exact)
           IF aa EQ '' THEN BEGIN
;---------------------------------------------------------------------------
;                             Record cloned plan
;---------------------------------------------------------------------------
            IF cloned(0) EQ '' THEN cloned(0) = plans(i) ELSE cloned = [cloned, plans(i)]
            info = rep_tag_value(info, cloned, 'CLONED')
           ENDIF
          ENDIF ELSE BEGIN
           clone_err = info.clone_err
           IF clone_err(0) EQ '' THEN clone_err(0) = err ELSE clone_err = [clone_err, err]
           info = rep_tag_value(info, clone_err, 'CLONE_ERR')
           widget_control,info.mess,set_value=strtrim(err,2),/app
          ENDELSE
         ENDIF
        ENDFOR
        WIDGET_CONTROL, info.mess, set_value='Cloning completed', /append
        WIDGET_CONTROL, event.top, sensitive=1,/show
;---------------------------------------------------------------------------
;                             Plot cloned plans
;---------------------------------------------------------------------------

        IF XREGISTERED('mk_cds_plan',/noshow) then call_procedure,'mk_cds_plot',/nohigh
        IF XREGISTERED('mk_sumer_plan',/noshow) then call_procedure,'mk_sumer_plot',/nohigh
        IF XREGISTERED('mk_soho',/noshow) then call_procedure,'mk_soho_plot',/nohigh
        xshow,event.top
       ENDIF
      ENDIF
     ENDELSE
     if (info.cloned)(0) eq '' then begin
      info.errmsg = 'No plans cloned.'
      WIDGET_CONTROL, info.mess, set_value=info.errmsg
     ENDIF
    END

    ELSE: do_nothing=1
   ENDCASE

   bv = grep(uvalue, info.plans, index = ii,/exact)
   IF ii(0) GE 0 THEN BEGIN
    i = ii(0)
    IF info.plans_idx(i) THEN info.plans_idx(i) = 0 else info.plans_idx(i) = 1
    WIDGET_CONTROL, info.pbuttons(i), set_button = info.plans_idx(i)
   ENDIF

   set_pointer,unseen,info,/no_copy
   RETURN
END

FUNCTION nextday, today
;---------------------------------------------------------------------------
;  Get next day's date string from a given date string
;---------------------------------------------------------------------------
   time = anytim2utc(today)
   time.mjd = time.mjd+1
   time = anytim2utc(time, /ecs, /date)
   RETURN, time
END

;---------------------------------------------------------------------------
;  The main program begins here
;---------------------------------------------------------------------------
PRO xclone_plan,date, group=group, modal=modal, reset=reset, $
                 errmsg=errmsg, status=status, soho_sci=soho_sci, $
                 def_clone=def_clone

;---------------------------------------------------------------------------
;  This common block is only used for saving current setting
;---------------------------------------------------------------------------
   COMMON for_xclone_plan, plans_idx, plan_type_old
   ON_ERROR,1

   IF NOT HAVE_WIDGETS() THEN begin
    errmsg='widgets unavailable'
    message,errmsg,/cont
    return
   endif

   mk_dfont,lfont=lfont,bfont=bfont

;---------------------------------------------------------------------------
;  Determine default date
;---------------------------------------------------------------------------
   IF N_ELEMENTS(date) EQ 0 THEN get_utc, date ELSE date = anytim2utc(date)
   from_date = anytim2utc(date, /ecs, /date,/vms)
   date1 = date & date1.mjd = date1.mjd+1
   to_date = anytim2utc(date1, /ecs, /date,/vms)

   wbase=WIDGET_BASE(title='XCLONE_PLAN ',column=1)

   bt_base=WIDGET_BASE(wbase,row=1,/frame)
   done=WIDGET_BUTTON(bt_base,value='Done',uvalue='DONE',font=bfont,/no_rel)
   go=WIDGET_BUTTON(bt_base,value='Go',uvalue='GO',font=bfont,/no_rel)

   row2=WIDGET_BASE(wbase,row=2,/frame)

   from_str=cw_field(row2, Title='Date to Clone From:',value=from_date,$
                   uvalue='FROM_DATE', /ret, xsize = 12, field=bfont,font = bfont)

   to_str=cw_field(row2,Title='Date to Clone To:  ', value=to_date,$
                    uvalue='TO_DATE', /ret, xsize = 12, field=bfont,font = bfont)

   row3=WIDGET_BASE(wbase,row=2,/frame)

   tmp = WIDGET_BASE(row3, /nonexclusive)
   temp = WIDGET_BUTTON(tmp, value='Fill dates in between', uvalue='BETWEEN',$
                       font=bfont,/no_rel)
   WIDGET_CONTROL, temp, set_button=0

   tmp = WIDGET_BASE(row3, /nonexclusive)
   temp = WIDGET_BUTTON(tmp, value='Turn off overwrite warning', uvalue='WARN',$
                       font=bfont,/no_rel)
   WIDGET_CONTROL, temp, set_button=0

   row4=WIDGET_BASE(wbase,column=1,/frame)

   cds_plans=['CDS-SCI','CDS-DET','CDS-FLAG','CDS-ALT']
   sumer_plans=['SUMER-SCI','SUMER-DET']
   soho_plans = get_soho_inst(/sort)

;---------------------------------------------------------------------------
;  What plan type?
;---------------------------------------------------------------------------
   IF KEYWORD_SET(soho_sci) THEN BEGIN
      plans = soho_plans
      plan_type = 'SOHO'
   ENDIF ELSE BEGIN
      inst = which_inst()
      IF inst EQ 'C' THEN BEGIN
         plans = cds_plans
         plan_type = 'CDS'
      ENDIF
      IF inst EQ 'S' THEN BEGIN
         plans = sumer_plans
         plan_type = 'SUMER'
      ENDIF
   ENDELSE

   reset = KEYWORD_SET(reset)
   IF N_ELEMENTS(plan_type_old) NE 0 THEN BEGIN
      IF plan_type NE plan_type_old THEN reset = 1
   ENDIF
   plan_type_old = plan_type

   IF N_ELEMENTS(plans_idx) EQ 0 OR reset THEN BEGIN
      plans_idx = INTARR(N_ELEMENTS(plans))
      plans_idx(*) = 1
   ENDIF

   IF datatype(def_clone) EQ 'STR' THEN BEGIN
      plans_idx(*) = 0
      FOR i=0, N_ELEMENTS(def_clone)-1 DO BEGIN
         IF grep(def_clone(i), plans, index=ii,/exact) NE '' THEN $
            plans_idx(ii(0)) = 1
      ENDFOR
   ENDIF

   IF KEYWORD_SET(soho_sci) THEN $
      temp = WIDGET_LABEL(row4, value='Instruments to Clone') $
   ELSE $
      temp = WIDGET_LABEL(row4, value='Type of Plans to Clone',font=bfont)

   c2=WIDGET_BASE(row4, /row)
   xmenu,plans, c2, /row, /nonexclusive, uvalue=plans, font=bfont,$
      buttons=pbuttons

   FOR i = 0, N_ELEMENTS(plans)-1 DO BEGIN
      WIDGET_CONTROL, pbuttons(i), set_button = plans_idx(i)
   ENDFOR

   mess = WIDGET_TEXT(wbase, ysize=4, value='', /frame, font=lfont,/scroll)

;-- realize and center main base

   xrealize,wbase,group=group,/center 

   update = 0

   info = {plans:plans, plans_idx:plans_idx, update:update, cloned:'',$
           clone_err:'', pbuttons:pbuttons, to_date:to_date, $
           from_date:from_date, from_str:from_str, to_str:to_str, $
           mess:mess, errmsg:'',between:0, bt_base:bt_base,warn:1}

   make_pointer,unseen
   set_pointer,unseen, info, /no_copy
   WIDGET_CONTROL, wbase, set_uvalue = unseen
   XMANAGER,'xclone_plan',wbase,group=group,/modal
   xmanager_reset,wbase,group=group,/modal

   info=get_pointer(unseen,/no_copy)
   free_pointer,unseen
   if datatype(info) eq 'STC' then begin
    errmsg = info.errmsg
    cloned = info.cloned
    clone_err = info.clone_err
    plans_idx = info.plans_idx
   endif else begin
    cloned='' & clone_err='' & cloned=''
   endelse
   status = {status:0, cloned:cloned, clone_err:clone_err}
   IF cloned(0) NE '' THEN status.status = 1

   return & end

