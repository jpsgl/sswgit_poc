;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_WHERE
;
; Purpose     :	Find where input TIME or PLAN is relative to PLANS
;
; Explanation :
;
; Use         :	INDEX=MK_PLAN_WHERE(TIME,PLANS)
;
; Inputs      :
;               TIME = input time 
;                        OR
;                      PLAN structure (TIME = start time of PLAN)
;               PLANS = structure array of plan entries to search
;
; Opt. Inputs : None.
;
; Outputs     :	INDEX = index of PLANS entry in which TIME falls
;                                   OR
;                       index of last PLANS element if TIME is between entries
;
; Opt. Outputs:	MATCH = 1 if entered PLAN exactly matches what is in PLANS 
;
; Keywords    : STATUS = 1 if TIME falls in PLANS, 0 otherwise
;               FLOATING  = set to do checking in floating point
;               ERR = error string
;               LAST = use END time of input plan as reference point
;               STR = set to check in string format
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;               Modified, 18 Aug 1996, Zarro (GSFC) - made /STR the default
;
; Version     :	May 31, 1995
;-

function mk_plan_where,time,plans,match,status=status,err=err,$
                       floating=floating,last=last,str=str

index=-1 & status=0 & err='' & match=0
type=get_plan_type(plans)
if (not exist(time)) or (type eq -1) then return,index

;-- get tag indicies for PLANS start and end times

itime=get_plan_itime(plans) 
pstart=(plans.(itime))
pend=(plans.(itime+1))

;-- convert input TIME to TAI format (check if entered as a structure)

ptype=get_plan_type(time)
if ptype eq -1 then begin
 ptime=anytim2tai(time,err=err)
 if err ne '' then begin
  help,time
  return,index
 endif
endif else begin
 pitime=get_plan_itime(time)
 ptime=(time.(pitime))
 petime=time.(pitime+1)
endelse

;-- check if ptime falls within a plan entry

if keyword_set(floating) then str=0 else str=1
case 1 of
 keyword_set(str): begin
  ptime=str_format(ptime)
  pstart=str_format(pstart)
  pend=str_format(pend)
 end
 keyword_set(floating): begin
  ptime=float(ptime)
  pstart=float(pstart)
  pend=float(pend)
 end
 else:do_nothing=1
endcase

clook=where( ((ptime gt pstart) and (ptime lt pend)) or $
              (ptime eq pstart) or (ptime eq pend), cnt)

if cnt gt 0 then begin
 if cnt gt 1 then begin
  message,'possible overlapping entry',/cont
 endif
 index=clook(cnt-1) & status=1
 if (ptype gt -1) and (n_params() eq 3) then match=mk_plan_comp(time,plans(index))
endif else begin

;-- else return the index of most recent plan

 clook=where( ptime gt pend,cnt)
 if cnt gt 0 then index=clook(cnt-1) 
endelse
 
return,index & end

