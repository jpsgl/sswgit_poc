;+
; Project     : SOHO-CDS
;
; Name        : MK_BBSO_IMAGES
;
; Purpose     : Driver to create BBSO H-alpha WWW images for a given date
;
; Category    : planning
;
; Explanation : calls FTP_BBSO_IMAGES to get BBSO Full-Disk H-alpha JPEG files
;
; Syntax      : mk_bbso_images,date
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: None
;
; Keywords    : BACK = # of days to lock back 
;               MAIL = send some mail
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 14 May 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro mk_bbso_images,date,back=back,mailto=mailto

err=''

synop_data=trim(getenv('SYNOP_DATA'))
if synop_data eq '' then begin
 message,'Undefined $SYNOP_DATA',/cont
 return
endif
if not test_open(synop_data,/write) then return
if not exist(back) then back=0
cdate=anytim2utc(date,err=err)
if err ne '' then get_utc,cdate
for i=0,back do begin
 do_date=cdate
 do_date.mjd=do_date.mjd-i
 hcode=strmid(date_code(do_date),2,100)
 outdir='$SYNOP_DATA/gif/'+hcode
 chk_loc=loc_file(outdir,count=dcount)
 if dcount eq 0 then spawn,'mkdir '+outdir
 files=ftp_bbso_images(do_date,/loud,outdir=outdir,/kill,count=count,err=err)
 if count gt 0 then begin
  if exist(lfiles) then lfiles=[lfiles,files] else lfiles=files
 endif
endfor

if exist(mailto) then begin
 run_id=['Results of MK_BBSO_IMAGES run on '+anytim2utc(cdate,/vms)+':','']
 if exist(lfiles) then $
  mess=['Following BBSO files created: ','',lfiles] else $
   mess=['No new BBSO files created']
 if datatype(mailto) eq 'STR' then recip=mailto else recip=get_user_id()
 send_mail,array=[run_id,mess],address=recip
endif


return & end

