;+
; Project     : SOHO-CDS
;
; Name        : GET_IAP
;
; Purpose     : get latest IAP files from SOC
;
; Category    : planning
;
; Explanation :
;
; Syntax      : get_iap,date,files=files
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : None
;
; Opt. Outputs:
;
; Keywords    : FILES = retrieved files
;               COUNT = # of files found
;               VERBOSE = print IAP files found
;               ERR = error string
;               OUT_DIR = output directory for IAP files
;
; Common      : None
;
; Restrictions: Unix only (with FTP access to SOC)
;
; Side effects: None
;
; History     : Written 9 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro get_iap,date,files=files,count=count,verbose=verbose,err=err,$
                 out_dir=out_dir,_extra=extra

count=0 & err='' & delvarx,files

;-- UNIX?

if strupcase(os_family()) ne 'UNIX' then begin
 err='Sorry, UNIX systems only'
 message,err,/cont & return
endif

;-- write files to HOME directory if no permission in current directory

home=getenv('HOME')
cd,cur=cur_dir
if datatype(out_dir) ne 'STR' then out_dir=cur_dir
if not test_open(out_dir,/write,err=err,/quiet) then out_dir=home

;-- what date?

err=''
get_utc,cur_date
cdate=anytim2utc(date,err=err)
if err ne '' then cdate=cur_date
cdate.time=0

;-- select specific instruments?

inst=strmid(strupcase(get_soho_inst()),0,3)
if datatype(extra) eq 'STC' then begin
 tags=strmid(tag_names(extra),0,3)
 iget=where_vector(tags,inst,tcount)
 if tcount gt 0 then inst=inst(iget) 
endif
ninst=n_elements(inst)

;-- construct input file for FTP

espawn,'which ncftp2',out
if strpos(out(0),'not found') gt -1 then ftp='ftp' else ftp='ncftp2'

verbose=keyword_set(verbose)
if verbose then message,'retrieving IAP files for '+anytim2utc(cdate,/ecs,/date),/cont
rdate=date_code(cdate)
ftp_com=concat_dir(getenv('HOME'),'ftp.com')
get_com='get '
openw,unit,ftp_com,/get_lun
printf,unit,'as'
printf,unit,'lcd '+out_dir
if ftp eq 'ftp' then begin
 printf,unit,'prompt'
 get_com='mget '
endif
printf,unit,'cd /iws_files/input_actplan'
for i=0,ninst-1 do printf,unit,get_com+inst(i)+rdate+'*.IAP'
printf,unit,'quit'
free_lun,unit

;-- pipe to FTP

cmd=ftp+' soc.nascom.nasa.gov < '+ftp_com
print,'%'+cmd
espawn,cmd,out

;-- found files?

iap=concat_dir(out_dir,'*'+rdate+'*.IAP')
chk=loc_file(iap,count=count)

if count gt 0 then begin
 for i=0,ninst-1 do begin
  ipos=where(strpos(chk,inst(i)) gt -1,icount)
  if icount gt 0 then begin
   spawn,'ls -t1 '+concat_dir(out_dir,inst(i)+rdate+'*.IAP'),out
   kmax=out(0)
   found=chk(ipos) & sf=sort(found) & imax=found(sf) 
   imax=imax(icount-1)
   if ftp eq 'ftp' then kmax=imax else begin
    if imax ne kmax then begin
     message,'IAP Version number not sequential for '+inst(i),/cont
     print,'imax,kmax: ',imax,kmax
    endif
   endelse
   del=where(kmax ne found,dcount)
   if dcount gt 0 then rm_file,found(del)
   if not exist(use_this) then use_this=kmax else use_this=[use_this,kmax]
  endif
 endfor
endif else begin
 err='No IAP files found for: '+anytim2utc(cdate,/ecs,/date)
 message,err,/cont
endelse

if exist(use_this) then begin
 files=use_this
 if verbose then print,files
 count=n_elements(files)
 err=''
endif

rm_file,ftp_com
return & end

