;+
; Project     : SOHO-CDS
;
; Name        : GET_NEUTRAL
;
; Purpose     : get latest NEUTRAL-line GIF from EIT MAC
;
; Category    : planning
;
; Explanation :
;
; Syntax      : get_neutral,file,date
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: FILE = found filename
;
; Keywords    : OUT_DIR = output directory for file [def = current]
;               ERR = error string
;               COUNT = no fo files found
;
; Common      : None
;
; Restrictions: Unix only (with FTP access to MAC)
;
; Side effects: None
;
; History     : Written 18 August 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro get_neutral,date,file,err=err,out_dir=out_dir,count=count,verbose=verbose

count=0 & err='' & file=''

;-- UNIX?

if strupcase(os_family()) ne 'UNIX' then begin
 err='Sorry, UNIX systems only'
 message,err,/cont & return
endif

;-- write files to HOME directory if no permission in current directory

home=getenv('HOME')
cd,cur=cur_dir
if datatype(out_dir) ne 'STR' then out_dir=cur_dir
if not test_open(out_dir,/write,err=err,/quiet) then out_dir=home

;-- what date?

err=''
get_utc,cur_date
cdate=anytim2utc(date,err=err)
if err ne '' then cdate=cur_date
cdate.time=0

;-- construct input file for FTP

espawn,'which ncftp2',out
if (strpos(out(0),'not found') gt -1) or (trim(out(0)) eq '') then ftp='ftp' else ftp='ncftp2'
ftp='ftp'

verbose=keyword_set(verbose)
ngif='neutral_'+trim(date_code(anytim2utc(cdate,/ecs,/date)))+'.gif'
if verbose then message,'retrieving NEUTRAL-line GIF file: '+ngif,/cont
rdate=date_code(cdate)
ftp_com=concat_dir(getenv('HOME'),'ftp.com')
get_com='get '
openw,unit,ftp_com,/get_lun
printf,unit,'bi'
printf,unit,'lcd '+out_dir
printf,unit,'cd ../Trillian/neutral'
printf,unit,get_com+ngif
printf,unit,'quit'
free_lun,unit

;-- pipe to FTP

server='eitmac.nascom.nasa.gov'
cmd=ftp+' '+server+' < '+ftp_com
print,'%'+cmd
espawn,cmd,out

;-- found files?

fgif=concat_dir(out_dir,ngif)
chk=loc_file(fgif,count=count)

if count eq 0 then begin
 err='No NEUTRAL-line GIF file found for: '+anytim2utc(cdate,/ecs,/date)
 message,err,/cont
endif else file=chk(0)

rm_file,ftp_com
return & end

