;---------------------------------------------------------------------------
; Document name: show_res_stc.pro
; Created by:    Liyun Wang, GSFC/ARC, November 10, 1995
;
; Last Modified: Tue Mar 26 09:25:34 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       SHOW_RES_STC
;
; PURPOSE:
;       Show resource structure selected from the display
;
; CATEGORY:
;       Science planning, utility
;
; EXPLANATION:
;
; SYNTAX:
;       show_res_stc, group, ctime, ypos, row_title
;
; EXAMPLES:
;       (when handling draw events)
;       bb = grep(row_id, ['Telemetry', 'Command'], /exact)
;       IF bb(0) NE '' THEN BEGIN
;          rtime = DOUBLE(ROUND(day+x*3600.d))
;          show_res_stc, event.top, rtime, y, bb(0)
;       ENDIF
;
; INPUTS:
;       GROUP - ID of the widget who serves as a group leader
;       CTIME - Cursor time in TAI
;       YPOS  - Coordinate in Y direction, in device pixels
;       ROW_TITLE - Title of the resource row (either 'Telemetry' or 'Command')
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       WBASE    - Parent widget base into which place structure
;       WTAGS    - Text widget ID's for each tag
;       JUST_REG - If set, just register the widget
;
; COMMON:
;       @mk_plan_shared, FOR_RES_SHOW
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, November 10, 1995, Liyun Wang, GSFC/ARC. Written
;       Version 2, January 4, 1996, Liyun Wang, GSFC/ARC
;          Added DURATION tag in displayed resource structure
;       Version 3, January 16, 1996, Liyun Wang, GSFC/ARC
;          Added keywords WBASE, WTAGS, and JUST_REG
;          Made wiget title to reflect the resource name
;       Version 4, March 26, 1996, Liyun Wang, GSFC/ARC
;          Modified such that duration of resource items is displayed
;             in DHMS format
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;

;---------------------------------------------------------------------------
;  Auxilary routine
;---------------------------------------------------------------------------
PRO res_show, res_name, rtime, group
   COMMON for_res_show, register, tmp_wbase, tmp_wtags
   IF N_ELEMENTS(res_name) NE 0 THEN BEGIN
      tlook = WHERE((rtime LE res_name.end_time) AND $
                    (rtime GE res_name.start_time), cnts)
      IF cnts GT 0 THEN BEGIN
;---------------------------------------------------------------------------
;        Add DURATION tag to the structure and display it
;---------------------------------------------------------------------------
         temp = res_name(tlook(0))
         dur = sec2dhms(LONG((temp.end_time-temp.start_time) > 0.d0))
         temp = add_tag(temp, dur, 'DURATION')
         if tag_exist(temp,'SUBMODE') then begin
          temp=rep_tag_value(temp,get_soho_submode(temp.submode),'submode')
         endif
         IF register THEN $
            xstruct, mk_plan_conv(temp), group=group, title=temp.res_name, $
            wbase=tmp_wbase, wtags=tmp_wtags, /just_reg, /center $
         ELSE $
            xstruct, mk_plan_conv(temp), wbase=tmp_wbase, wtags=tmp_wtags, $
            group=group, title=temp.res_name, /center
      ENDIF
   ENDIF
END

;---------------------------------------------------------------------------
;  Main routine
;---------------------------------------------------------------------------
PRO show_res_stc, group, rtime, y, label, wbase=wbase, wtags=wtags, $
                  just_reg=just_reg      
@mk_plan_shared
   COMMON for_res_show, register, tmp_wbase, tmp_wtags
   ON_ERROR, 2
   IF N_PARAMS() NE 4 THEN BEGIN
      MESSAGE,'Syntax: show_res_stc, group, rtime, y, label', /cont
      RETURN
   ENDIF
   register = KEYWORD_SET(just_reg) 
   IF N_ELEMENTS(wbase) NE 0 THEN tmp_wbase = wbase
   IF N_ELEMENTS(wtags) NE 0 THEN tmp_wtags = wtags
   num_rows = 4
   srow_wid = 0.9/num_rows
   temp = (y-FIX(y)-0.05)/srow_wid
   IF temp LT 0.0 THEN RETURN
   sub_row = FIX(temp)
   
   IF STRUPCASE(label) EQ 'TELEMETRY' THEN BEGIN
      CASE sub_row OF
         0: res_show, mdi_m, rtime, group
         1: res_show, mdi_h, rtime, group
         2: res_show, tape_dump, rtime, group
         3: res_show, nrt, rtime, group
         ELSE:
      ENDCASE
   ENDIF

   IF STRUPCASE(label) EQ 'COMMAND' THEN BEGIN
      CASE sub_row OF
         0: res_show, svm, rtime, group
         1: BEGIN
            res_show, cmdtimes, rtime, group
            res_show, dlydtimes, rtime, group
         END
         2: BEGIN
            res_show, trcr, rtime, group
            res_show, tnorcr, rtime, group
         END
         3: begin
             res_show, dsn, rtime, group
         END
         ELSE:
      ENDCASE
   ENDIF
   IF N_ELEMENTS(tmp_wtags) NE 0 THEN wtags = tmp_wtags
   IF N_ELEMENTS(tmp_wbase) NE 0 THEN wbase = tmp_wbase
END
;---------------------------------------------------------------------------
; End of 'show_res_stc.pro'.
;---------------------------------------------------------------------------
