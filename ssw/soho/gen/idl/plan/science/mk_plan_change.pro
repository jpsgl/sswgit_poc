;+
; Project     :	SOHO - CDS/SUMER
;
; Name        :	MK_PLAN_CHANGE
;
; Purpose     :	change PLAN plan entries 
;
; Explanation :	changes start and stop times of PLAN plan entries so
;               that they don't overlap 
;
; Use         :	MK_PLAN_CHANGE,PLAN,NPLAN,STARTDIS,STOPDIS
;
; Inputs      :	PLAN = structure array of PLAN plan entries
;
; Opt. Inputs : STARTDIS, STOPDIS = lower and upper time limits within which
;               to constrain recalculated entries.
;
; Outputs     :	NPLAN = time-changed plan entries
;
; Opt. Outputs:	None.
;
; Keywords    : TGAP = minimum gap between successive entries [def = 1.d-2]
;               TCAL = start time at which to start recalculation 
;                        [def = start of first plan]
;
; Common      :	None.
;
; Restrictions:	For SCIENCE plan entries only
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Version 1.0, Dominic Zarro (ARC), 9 January 1995
;                                            
; Modification: Version 2, Liyun Wang, GSFC/ARC, May 23, 1995
;               Gereralized for use by supported instruments
;
; Version     :	Version 2, May 23, 1995
;-

pro mk_plan_change,plan,nplan,startdis,stopdis,tgap=tgap,tcal=tcal,err=err

on_error,1

err=''
delvarx,nplan
if not exist(plan) then return

type=get_plan_type(plan)
if (type eq -1) then begin
 message,'Unsupported data type',/contin 
 return
endif

if (type lt 2) then begin
 nplan=plan
 message,'SCIENCE and/or ALT plan only',/contin & return
endif

;-- sort and initialize

itime = get_plan_itime(plan)
if n_elements(tgap) eq 0 then tgap=1.d-2
splan=mk_plan_sort(plan,/str,/uniq)
np=n_elements(splan)
tlast=min(splan.(itime))-tgap
tinfinite=100.*max(splan.(itime+1))
if n_elements(startdis) gt 0 then tlast=tlast < (startdis - tgap)
;if n_elements(stopdis) gt 0 then tinfinite=stopdis
if n_elements(tcal) eq 0 then tcal=min(splan.(itime))

;-- check each entry

dprint,'% MK_PLAN_CHANGE: Checking entries...'
cfind=where( (splan.(itime) lt tcal) and (splan.(itime+1) lt tcal),cnt)
if cnt gt 1 then begin
 iend=cfind(cnt-2)
 istart=cfind(cnt-1)
 nplan=splan(0:iend) 
 dprint,'%Skipping first '+strtrim(string(cnt-1),2)+' entries..'
 tlast=max(nplan.(itime+1)) > tlast
endif else istart=0

for i=istart,np-1 do begin
 curr_plan=splan(i)
 spec=strtrim(curr_plan.sci_obj)
 curr_dur=curr_plan.(itime+1)-curr_plan.(itime)
 tstart=curr_plan.(itime) > (tlast+tgap)        
 if i lt (np-1) then tnext=splan(i+1).(itime) else tnext=tinfinite
 tend=(tstart+curr_dur) < (tnext-tgap)
 if (tend-tstart) gt tgap then begin
  t1=anytim2utc(tstart) & t2=anytim2utc(tend)
  ndays=t2.mjd-t1.mjd
  if ndays gt 1 then begin  ;-- divide up plans than span day boundaries
   for k=0,ndays do begin
    dprint,k
    if k eq 0 then p=tstart else p=q+tgap
    temp=tai2utc(p) & temp.time=0 & temp.mjd=temp.mjd+1
    if k eq ndays then q=tend else q=utc2tai(temp)-tgap
    curr_plan.(itime)=p
    curr_plan.(itime+1)=q
    if (q-p) gt 60. then begin
     if not exist(nplan) then nplan=curr_plan else $
      nplan=concat_struct(temporary(nplan),curr_plan)
    endif
   endfor
  endif else begin
   curr_plan.(itime)=tstart
   curr_plan.(itime+1)=tend
   if not exist(nplan) then nplan=curr_plan else $
    nplan=concat_struct(temporary(nplan),curr_plan)
  endelse
  tlast=tend
 endif else serr='Duration of this entry will be less than zero: '+spec

;-- record any errors

 if exist(serr) then begin
  err=[err,serr]
  delvarx,serr
 endif

endfor

nerr=n_elements(err)
if nerr gt 1 then err=err(1:nerr-1)

return & end

