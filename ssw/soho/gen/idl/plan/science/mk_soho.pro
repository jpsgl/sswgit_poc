;---------------------------------------------------------------------------
; Document name: mk_soho.pro
; Created by:    Liyun Wang, GSFC/ARC, May 15, 1995
;
; Last Modified: Fri Aug 30 15:15:06 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       SOHO
;
; NAME:
;       MK_SOHO
;
; PURPOSE:
;       Widget interface for displaying/editing SOHO SCI plans
;
; EXPLANATION:
;
; CALLING SEQUENCE:
;       mk_soho [,tstart]
;
; INPUTS:
;       None required.
;
; OPTIONAL INPUTS:
;       TSTART - start date to display/edit
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;	GROUP  = event id of calling parent widget.
;       MODAL  = make parent widget insensitive when MK_SOHO is called
;                as child.
;       EDIT   = Start the program in edit mode
;       RESET  = Force the database to be read again
;       DETACH = Separate the display window from the main widget if set
;
; COMMON BLOCKS:
;       Defined in mk_soho_com.pro
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Planning Tool
;
; PREVIOUS HISTORY:
;       Written May 15, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1.0, created, Liyun Wang, GSFC/ARC, May 15, 1995
;          Adopted from MK_PLAN
;       Version 1.1, Liyun Wang, GSFC/ARC, May 30, 1995
;          Added plan cloning feature
;          Added COPY plan feature
;       Version 2.0, Liyun Wang, GSFC/ARC, June 9, 1995
;          Improved the functionality of the "Add" button
;          Added graphically moving function
;          Added option to choose unit of plan duration (sec, min, hour)
;          Made it possible to specify start/stop/duration of a plan entry
;             via mouse or directly editing of widget content
;       Version 2.1, Liyun Wang, GSFC/ARC, June 27, 1995
;          Replaced calls to MK_PLAN_CHK (which no longer exists) with
;             MK_PLAN_WHERE
;       Version 2.2, July 20, 1995, Liyun Wang, GSFC/ARC
;          Fixed a bug of repeatedly removing and adding entries
;       Version 2.3, July 24, 1995, Liyun Wang, GSFC/ARC
;          Added DETACH keyword to separate the display window
;          Added UNDO feature
;       Version 3.0, August 10, 1995, Liyun Wang, GSFC/ARC
;          Plotted telemtry resources and commanding resources on
;             different row
;          Added MDI science plan row
;          Added button to read the latest KAP files
;          Added feature of writing CAP files
;          Added feature of cloning one selected entry to all instrument
;          Added feature of deleting entries that are identical to all
;             instruments
;       Version 3.1, August 16, 1995, Liyun Wang, GSFC/ARC
;          Added XCEN and YCEN fields to the widget
;          Added cursor time round off mechanism
;       Version 3.2, August 21, 1995, Liyun Wang, GSFC/ARC
;          Added feature of displaying the latest KAP file
;          Added capability of displaying SOHO wide detailed plan entries
;       Version 3.3, August 31, 1995, Liyun Wang, GSFC/ARC
;          Made cursor line extendable and color coded for different
;             instruments in display mode
;          Got rid of TSTOP positional parameter
;       Version 3.4, October 10, 1995, Liyun Wang, GSFC/ARC
;          Started using the XPS_SETUP to handle making hard copies in
;             PS fromat
;          Fixed a bug that would crash the program if the middle
;             button is clicked earlier than the left button
;       Version 3.5, November 10, 1995, Liyun Wang, GSFC/ARC
;          Added capability of showing resource structures
;       Version 3.6, November 20, 1995, Liyun Wang, GSFC/ARC
;          Modified so that the user is warned when he leaves a
;             modified, unsaved plan entry
;       Version 3.7, December 4, 1995, Liyun Wang, GSFC/ARC
;          Added inherit feature
;       Version 3.8, December 14, 1995, Liyun Wang, GSFC/ARC
;          Fixed a bug that soho_splan is not updated properly when
;             shifting the timeline
;          Fixed problem encountered when deleting all entries
;             starting at the same time
;       Version 4.0, December 28, 1995, Liyun Wang, GSFC/ARC
;          Added feature to perform pointing with IMAGE_TOOL
;       Version 4.1, January 12, 1996, Liyun Wang, GSFC/ARC
;          Fixed bug that causes crash in editing mode if no instrume
;             is chosen
;          Added option to insert entry at beginning of display
;       Version 4.2, January 15, 1996, Liyun Wang, GSFC/ARC
;          Added current UT to the widget title
;          Implemented feature to add a series plans
;          Force a reset when it sees the change of ZDBASE
;       Version 4.3, January 25, 1996, Liyun Wang, GSFC/ARC
;          Added inheriting stop time as start time
;       Version 4.4, January 30, 1996, Liyun Wang, GSFC/ARC
;          Implemented zoom in/out feature
;       Version 4.5, March 18, 1996, Liyun Wang, GSFC/ARC
;          Fixed a bug that new entry always defaults to a CDS plan
;       Version 4.6, April 10, 1996, Liyun Wang, GSFC/ARC
;          Modified such that DB is not read when changing mode unless necesary
;          Made Pointing Tool available to SOHO details plans as well
;          Added feature of tracing the cursor time
;       Version 4.7, May 1, 1996, Liyun Wang, GSFC/ARC
;          Lifted restriction of disallowing edit of entries before
;             current UT
;          Fixed a bug of unabling to show multiple pointings in SOHO
;             DET plans
;       Version 4.8, August 20, 1996, Liyun Wang, NASA/GSFC
;          Replaced calls to "XTEXT, /just_reg" with XMESSAGE to avoid
;             hanging of IDL
;          Added option to turn off color in display -- DMZ
;
; VERSION:
;       Version 4.8, August 20, 1996
;-
;

;-----------------------------------------------------------------------------
; Following programs are purely local to MK_SOHO
;-----------------------------------------------------------------------------

PRO entry_clone, iplan, inst_list, error=error, status=status
;---------------------------------------------------------------------------
;
; NAME:
;       ENTRY_CLONE
;
; PURPOSE:
;       Clone a given plan entry to other instruments
;
; SYNTAX:
;       entry_clone, plan, inst_list
;
; INPUTS:
;       PLAN - Plan structure to be cloned
;       INST_LIST - String array, listing names of instruments to
;                   which PLAN is clone
;
; SIDE EFFECTS:
;       SOHO_PLANS is changed
;
@mk_soho_com

   ON_ERROR, 1
   status = 0

   IF datatype(plan) NE 'STC' THEN BEGIN
      error = 'Wrong type of input parameter'
      MESSAGE, error, /cont
      RETURN
   ENDIF
   IF NOT exist(inst_list) THEN inst_list = get_soho_inst()

   inst = iplan.instrume
   tplan = iplan
;---------------------------------------------------------------------------
;  Add the plan to the given instrument
;---------------------------------------------------------------------------
   FOR i=0, N_ELEMENTS(inst_list)-1 DO BEGIN
      curr_inst = get_soho_inst(inst_list(i))
      IF curr_inst NE inst THEN BEGIN
         mk_soho_extplan, soho_splan, curr_inst, inst_plans, other_plans
         tplan.instrume = get_soho_inst(instrume)
         mk_soho_save
         IF exist(inst_plans) THEN db_inst_plans = inst_plans
         mk_plan_add, tplan, inst_plans, err=err, /insert
         IF err NE '' THEN BEGIN
            IF NOT exist(error) THEN error = err ELSE error = [error,err]
            inst_plans = db_inst_plans
         ENDIF ELSE BEGIN
            status = 1
            WIDGET_CONTROL, /hour
            mk_plan_change, inst_plans, ninst_plans, startdis, tgap=tgap
            mk_plan_write, ninst_plans, db_inst_plans
            IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
            mk_soho_update
         ENDELSE
      ENDIF
   ENDFOR
;---------------------------------------------------------------------------
;  Back to original instrument
;---------------------------------------------------------------------------
   mk_soho_extplan, soho_splan, inst, inst_plans, other_plans
   RETURN
END

PRO mk_soho_tplot
;---------------------------------------------------------------------------
;  Plot ctime and/or etime
;---------------------------------------------------------------------------
@mk_soho_com
   mk_soho_eraseline, last_cpos, last_row
   mk_soho_eraseline, last_epos, last_row
   day = tai2utc(startdis)
   day.time=0
   day=utc2tai(day)
   IF exist(ctime) THEN BEGIN
      cpos = (ctime-day)/3600.d
      OPLOT, [cpos, cpos], last_row+[.02, .98], linestyle=1, $
         color=2*(1-nocolor)
      last_cpos = cpos
   ENDIF
   IF exist(etime) THEN BEGIN
      epos = (etime-day)/3600.d
      OPLOT, [epos, epos], last_row+[.02, .98], linestyle=2, $
         color=2*(1-nocolor)
      last_epos = epos
   ENDIF
END

PRO mk_soho_eraseline, pos, row
@mk_soho_com
   IF exist(pos) THEN BEGIN
      yy = row+[0.02, 0.98]
      OPLOT, [pos, pos], yy, color=nocolor*(!d.table_size-1)
      delvarx, pos
   ENDIF
END

PRO mk_soho_remove_all, iplan, inst_list, error=error
@mk_soho_com

   IF datatype(plan) NE 'STC' THEN BEGIN
      error = 'Wrong type of input parameter'
      MESSAGE, error, /cont
      RETURN
   ENDIF
   IF NOT exist(inst_list) THEN inst_list = get_soho_inst(/short)

   tplan = iplan
;---------------------------------------------------------------------------
;  Remove all plans that match tplan for all instruments
;---------------------------------------------------------------------------
   FOR i=0, N_ELEMENTS(inst_list)-1 DO BEGIN
      curr_inst = get_soho_inst(inst_list(i))
      tplan.instrume = curr_inst
      mk_soho_extplan, soho_splan, curr_inst, inst_plans, other_plans
      IF exist(inst_plans) THEN BEGIN
         db_inst_plans = inst_plans
         index = WHERE(inst_plans.(itime) EQ tplan.(itime))
         IF index(0) GE 0 THEN BEGIN
            mk_soho_save
            mk_plan_rem, inst_plans(index), inst_plans, err=err
            IF err NE '' THEN BEGIN
               IF NOT exist(error) THEN error = err ELSE error = [error, err]
               inst_plans = db_inst_plans
            ENDIF ELSE BEGIN
               mk_plan_change, inst_plans, ninst_plans, startdis
               mk_plan_write, ninst_plans, db_inst_plans
               mk_soho_update
            ENDELSE
         ENDIF
      ENDIF
   ENDFOR
END

PRO mk_soho_sciobj, event
@mk_soho_com
   IF exist(soho_splan) THEN $
      sobj_list = get_uniq_list(soho_splan.sci_obj, orig=sobj_list)

   IF exist(sobj_list) THEN BEGIN
      text = (plan.sci_obj)(0)
      temp = xsel_list(sobj_list, group=event.top, title='SCI_OBJ', $
                       subtitle='Available Science Objectives', $
                       initial=text, /update, status=status)
   ENDIF ELSE BEGIN
      temp = plan.sci_obj
      xinput, temp, 'Enter Science Objective [50 char max]', max_len=50, $
         group=event.top, /modal, bfont=''
      temp = STRTRIM(temp, 2)
      IF temp NE '' THEN sobj_list = temp
   ENDELSE
   plan.sci_obj = STRTRIM(temp(0),2)
   WIDGET_CONTROL, sci_obj, set_value=plan.sci_obj
   RETURN
END

FUNCTION mk_soho_notify
;---------------------------------------------------------------------------
;  A routine to notify the user if hplan and plan are different and
;  the user is about to abandon plan (by selecting other plan entries)
;---------------------------------------------------------------------------
@mk_soho_com
   IF user_mode GE 1 THEN RETURN, 0
   IF N_ELEMENTS(hplan)*N_ELEMENTS(plan) EQ 0 THEN RETURN, 0
   IF match_struct(hplan, plan) THEN RETURN, 0
   RETURN, (NOT xanswer('Leaving editing plan... Are you sure?'))
END

PRO mk_soho_draw, event
;---------------------------------------------------------------------------
;  Draw event handler
;---------------------------------------------------------------------------
@mk_soho_com

   IF event.type EQ 2 AND chg_flag NE 1 THEN BEGIN
;---------------------------------------------------------------------------
;     Motion events
;---------------------------------------------------------------------------
      data_x = (FLOAT(event.x)/!d.x_vsize - !x.s(0))/!x.s(1)
      x = round_off(data_x, custom_stc.troff/60.0)
      x = !x.crange(0) > x < !x.crange(1)
      day = tai2utc(startdis) & day.time=0
      day = utc2tai(day)
      time = tai2utc(DOUBLE(ROUND(day+x*3600.d)), /ecs, /trunc)
      IF N_ELEMENTS(draw_time) NE 0 THEN $
         XYOUTS, 0, 0, draw_time, /DEVICE, color=!p.background
      XYOUTS, 0, 0, time, /DEVICE, color=!p.color
      draw_time = time
      RETURN
   ENDIF
   
   IF event.press NE 0 AND (chg_flag EQ 0) THEN BEGIN
      data_x = (FLOAT(event.x)/!d.x_vsize - !x.s(0))/!x.s(1)
      data_y = (FLOAT(event.y)/!d.y_vsize - !y.s(0))/!y.s(1)
      x = round_off(data_x, custom_stc.troff/60.0)
      y = data_y
      x = !x.crange(0) > x < !x.crange(1) ; force click in bounds
      day = tai2utc(startdis) & day.time=0
      day = utc2tai(day)

      IF do_zoom GE 1 THEN BEGIN
;---------------------------------------------------------------------------
;        Set Zoom in window size
;---------------------------------------------------------------------------
         ttime(do_zoom-1) = DOUBLE(ROUND(day+x*3600.d))
         OPLOT, [x, x], !y.crange, linestyle=1, color=!p.color*(1-nocolor)
         do_zoom = do_zoom+1
         IF do_zoom GT 2 THEN BEGIN
            IF ttime(1) GT ttime(0) THEN BEGIN
               startdis = ttime(0)
               stopdis = ttime(1)
            ENDIF ELSE BEGIN
               startdis = ttime(1)
               stopdis = ttime(0)
            ENDELSE
            WIDGET_CONTROL, start_text2, set_value=$
               tai2utc(startdis, /ecs, /trun)
            WIDGET_CONTROL, stop_text, set_value=$
               tai2utc(stopdis, /ecs, /truncate)
            WIDGET_CONTROL, span_text, set_value=$
               STRTRIM(num2str((stopdis-startdis), forma='(f20.2)'), 2)
            do_zoom = 0
            mk_soho_wrefresh
            mk_soho_plot
         ENDIF ELSE BEGIN
            WIDGET_CONTROL, start_text2, set_value=$
               tai2utc(ttime(0), /ecs, /trun)
            WIDGET_CONTROL, stop_text, set_value=''
            WIDGET_CONTROL, span_text, set_value=''
         ENDELSE
         RETURN
      ENDIF

;---------------------------------------------------------------------------
;     Which row the mouse cursor is clicked
;---------------------------------------------------------------------------
      row = FIX(y)
      n_items = N_ELEMENTS(custom_stc.req_items)
      IF row GE 0 AND row LE n_items-1 AND y GE 0.0 THEN $
         row_id = custom_stc.req_items(n_items-row-1) $
      ELSE RETURN

      plot_line = 0
      aa = grep(row_id, get_soho_inst(), /exact)
      IF aa(0) NE '' THEN BEGIN
         IF exist(last_cpos) AND event.press NE 4 THEN BEGIN
            IF custom_stc.ext_cursor AND custom_stc.ops_mode NE 0 THEN $
               yy = !y.crange $
            ELSE $
               yy = last_row+[0.02, 0.98]
            OPLOT, [last_cpos, last_cpos], yy, color=nocolor*(!d.table_size-1)
         ENDIF
         plot_line = 1
         tinst = get_soho_inst(aa(0))
         IF custom_stc.ops_mode EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;           Edit mode
;---------------------------------------------------------------------------
            IF aa(0) NE instrume THEN BEGIN
;---------------------------------------------------------------------------
;              Choosing different instrument
;---------------------------------------------------------------------------
               IF event.press EQ 4 THEN BEGIN
;---------------------------------------------------------------------------
;                 Get the reference plan
;---------------------------------------------------------------------------
                  ref_inst = aa(0)
                  IF NOT exist(soho_splan) THEN BEGIN
                     delvarx, rplan
                     RETURN
                  ENDIF
                  tmp = WHERE(soho_splan.instrume EQ get_soho_inst(ref_inst))
                  IF tmp(0) EQ -1 THEN BEGIN
                     delvarx, rplan
                     RETURN
                  ENDIF
                  mk_soho_eraseline, last_rpos, last_rrow
                  ctime = DOUBLE(ROUND(day+x*3600.d))
                  tplan = soho_splan(tmp)
                  tlook = WHERE((ctime LT tplan.(itime+1)) AND $
                                (ctime GE tplan.(itime)), nobs)
                  IF tlook(0) GE 0 THEN BEGIN
                     rplan = tplan(tlook(0))
                  ENDIF ELSE delvarx, rplan
                  OPLOT, [x, x], row+[0.02, 0.98], linestyle=3, $
                     color=2*(1-nocolor)
                  last_rpos = x
                  last_rrow = row
                  mk_soho_button
                  RETURN
               ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;                 User changes instrument with left or middle button
;---------------------------------------------------------------------------
                  delvarx, rplan
                  mk_soho_eraseline, last_rpos, last_rrow
                  IF mk_soho_notify() THEN RETURN
                  delvarx, saved_plans, s_hplan
                  sdb_empty = 0
                  IF instrume NE '' THEN last_inst = instrume
                  instrume = aa(0)
                  mk_soho_extplan, soho_splan, instrume, inst_plans, $
                     other_plans
                  IF exist(plan) THEN BEGIN
                     plan.instrume = get_soho_inst(instrume)
                  ENDIF ELSE BEGIN
                     def_inst_plan, plan, inst=get_soho_inst(instrume), $
                        type=2, start=startdis
                  ENDELSE
                  IF exist(hplan) THEN BEGIN
;---------------------------------------------------------------------------
;                    Un-highlight previous hplan
;---------------------------------------------------------------------------
                     cidx = (WHERE(TAG_NAMES(color_stc) EQ $
                                   last_inst, cnt))(0)
                     IF cnt GT 0 THEN color = color_stc.(cidx) ELSE color = 1
                     oplot_splan, hplan, startdis, color=color*(1-nocolor), $
                        row=last_row
                  ENDIF
                  mk_soho_eraseline, last_spos, last_row
                  mk_soho_eraseline, last_epos, last_row
                  delvarx, hplan
               ENDELSE
            ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;              Dealing with the same instrument
;---------------------------------------------------------------------------
               delvarx, rplan
               mk_soho_eraseline, last_rpos, last_rrow
               IF (event.press EQ 4) THEN BEGIN
;---------------------------------------------------------------------------
;                 User sets end time by using right mouse button
;---------------------------------------------------------------------------
                  etime = DOUBLE(ROUND(day+x*3600.d))
                  plot_line = 0
                  mk_soho_eraseline, last_epos, last_row
                  last_epos = x
                  last_etime = etime
                  last_row = row
                  OPLOT, [last_epos, last_epos], row+[0.02, 0.98], $
                     linestyle=2, color=2*(1-nocolor)
                  plan.(itime+1) = DOUBLE(etime)
                  WIDGET_CONTROL, date_end, set_value=tai2utc(etime, $
                                                              /ecs, /date)
                  WIDGET_CONTROL, time_end, set_value=tai2utc(etime, $
                                                              /ecs, /time)
                  tdur = (plan.(itime+1)-plan.(itime))/dur_unit > 0.d0
                  WIDGET_CONTROL, pduration, set_value=$
                     STRTRIM(num2str(tdur, FORMAT='(f20.2)'), 2)
                  mk_soho_button
                  RETURN
               ENDIF
               IF (event.press EQ 2) THEN BEGIN
;---------------------------------------------------------------------------
;                 User changes the start time of current entry with
;                 middle button
;---------------------------------------------------------------------------
                  ctime = DOUBLE(ROUND(day+x*3600.d))
                  plot_line = 0
                  plan.(itime) = DOUBLE(ctime)
                  WIDGET_CONTROL, date_obs, set_value=tai2utc(ctime, $
                                                              /ecs, /date)
                  WIDGET_CONTROL, time_obs, set_value=tai2utc(ctime, $
                                                              /ecs, /time)
                  tdur = (plan.(itime+1)-plan.(itime))/dur_unit > 0.d0
                  WIDGET_CONTROL, pduration, set_value=$
                     STRTRIM(num2str(tdur, FORMAT='(f20.2)'), 2)
                  IF nocolor THEN color = !d.table_size-1 ELSE color = 0
                  IF exist(last_spos) THEN OPLOT, [last_spos, last_spos], $
                     last_row+[0.02, 0.98], color=nocolor*(!d.table_size-1)
                  OPLOT, [x, x], row+[0.02, 0.98], linestyle=3, $
                     color=2*(1-nocolor)
                  last_spos = x
                  last_row = row
               ENDIF
            ENDELSE
            ctime = DOUBLE(ROUND(day+x*3600.d))
            IF ctime LE startdis OR ctime GE stopdis THEN RETURN
;---------------------------------------------------------------------------
;           Check if in inst_plans
;---------------------------------------------------------------------------
            n_plans = 0
            IF exist(inst_plans) THEN BEGIN
               tlook = WHERE((ctime LE inst_plans.(itime+1)) AND $
                             (ctime GE inst_plans.(itime)), n_plans)
            ENDIF
            IF n_plans GT 0 THEN BEGIN
;---------------------------------------------------------------------------
;              Highlight selected plan in yellow
;---------------------------------------------------------------------------
               zplan = inst_plans(tlook(0))
               IF exist(hplan) THEN BEGIN
                  same = match_struct(zplan, hplan)
               ENDIF ELSE same = 0
               IF NOT same THEN BEGIN
                  IF mk_soho_notify() THEN RETURN
                  cidx = (WHERE(TAG_NAMES(color_stc) EQ instrume, cnt))(0)
                  IF cnt GT 0 THEN color = color_stc.(cidx) ELSE color = 1
                  IF exist(hplan) THEN $
                     oplot_splan, hplan, startdis, color=color*(1-nocolor), $
                     row=row
                  plan = zplan
                  hplan = zplan
                  mk_soho_eraseline, last_epos, row
                  mk_soho_eraseline, last_spos, row
               ENDIF
               IF exist(hplan) THEN BEGIN
                  oplot_splan, hplan, startdis, color=2*(1-nocolor), row=row
               ENDIF
            ENDIF
            mk_soho_wrefresh
            mk_soho_button
         ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;           Display mode
;---------------------------------------------------------------------------
            ctime = DOUBLE(ROUND(day+x*3600.d))
            IF ctime LE startdis OR ctime GE stopdis THEN RETURN
            instrume = aa(0)
            inst = get_soho_inst(/short)
            clook = WHERE(tinst EQ inst, cnt)
            IF custom_stc.ops_mode EQ 1 THEN BEGIN
               IF exist(soho_splan) THEN tplan = soho_splan
            ENDIF ELSE BEGIN
               IF exist(soho_dplan) THEN tplan = soho_dplan
            ENDELSE
            IF cnt GT 0 AND exist(tplan) THEN BEGIN
               itime = get_plan_itime(tplan)
               tlook = WHERE((tinst EQ tplan.instrume) AND $
                             (ctime LT tplan.(itime+1)) AND $
                             (ctime GE tplan.(itime)), nobs)
               IF nobs GT 0 THEN BEGIN
                  plan = tplan(tlook(0))
                  mk_soho_wrefresh
               ENDIF ELSE mk_soho_wrefresh, /empty
            ENDIF
         ENDELSE
      ENDIF
      bb = grep(row_id, ['Telemetry', 'Command'], /exact)
      IF bb(0) NE '' THEN BEGIN
         rtime = DOUBLE(ROUND(day+x*3600.d))
         show_res_stc, event.top, rtime, y, bb(0), /just_reg, wbase=wbase, $
            wtags=wtags
      ENDIF
      IF plot_line THEN BEGIN
;---------------------------------------------------------------------------
;        Draw a line at selected time
;---------------------------------------------------------------------------
         IF custom_stc.ops_mode NE 0 AND custom_stc.ext_cursor THEN BEGIN
            yy = !y.crange
            color = get_inst_color(instrume, color_stc, def_color=2)
         ENDIF ELSE BEGIN
            yy = row+[0.02, 0.98]
            color = 2
         ENDELSE
         OPLOT, [x, x], yy, linestyle=1, color=color*(1-nocolor)
         last_cpos = x
         last_ctime = ctime
         last_row = row
      ENDIF
   ENDIF
   IF chg_flag NE 0 THEN BEGIN
      mk_plan_shift, event, shift_stc=shift_stc
      RETURN
   ENDIF
END

PRO mk_soho_redraw, event
;---------------------------------------------------------------------------
;  Event handler for the detached graphic window
;---------------------------------------------------------------------------
@mk_soho_com
   IF NOT tag_exist(event, 'type') THEN BEGIN
;---------------------------------------------------------------------------
;     it is a resizing event
;---------------------------------------------------------------------------
      WIDGET_CONTROL, draw_id_soho, xsize=event.x, ysize=event.y
      mk_soho_plot
   ENDIF ELSE BEGIN
      mk_soho_draw, event
   ENDELSE
END

PRO mk_soho_save
;---------------------------------------------------------------------------
;  Save latest copy of inst_plans for later undoing
;---------------------------------------------------------------------------
@mk_soho_com
   IF exist(inst_plans) THEN BEGIN
      saved_plans = inst_plans
      sdb_empty = 0
   ENDIF ELSE BEGIN
      sdb_empty = 1
      delvarx, saved_plans
   ENDELSE
   IF exist(hplan) THEN s_hplan = hplan ELSE delvarx, s_hplan
   RETURN
END

PRO mk_soho_move, event, replace=rep
;---------------------------------------------------------------------------
;  Move highlighted entry
;
;  IF REPLACE keyword is not set, plan start time will be set to ctime
;  and stop time is set accordingly based on plan duration
;---------------------------------------------------------------------------
@mk_soho_com
   write_mess = 'Please wait. Updating Database....'

   IF NOT exist(hplan) OR NOT exist(plan) THEN BEGIN
;---------------------------------------------------------------------------
;     Both hplan and plan have to exist for "move" to work
;---------------------------------------------------------------------------
      dprint, 'Either no plan exists or no selected plan.'
      xkill, tbase
      RETURN
   ENDIF

   replace = KEYWORD_SET(rep)

;---------------------------------------------------------------------------
;  Find location of old plan in DB
;---------------------------------------------------------------------------
   index = mk_plan_where(hplan, inst_plans, status=in_db)
   IF NOT in_db THEN BEGIN
      WIDGET_CONTROL, comment, set_value='Entry not in DB', /app
      xkill, tbase
      RETURN
   ENDIF

;---------------------------------------------------------------------------
;  Prepare for move (if from the "change" button, from_change is set)
;---------------------------------------------------------------------------
   IF NOT replace THEN BEGIN
      cur_dur = (plan.(itime+1)-plan.(itime)) > 0.
      plan.(itime) = DOUBLE(ctime)
      plan.(itime+1) = plan.(itime)+cur_dur
   ENDIF

;---------------------------------------------------------------------------
;  Find location of old plan in DB
;---------------------------------------------------------------------------
   index = mk_plan_where(hplan, inst_plans, status=in_db)
   IF NOT in_db THEN BEGIN
      WIDGET_CONTROL, comment, set_value='Entry not in DB', /app
      xkill, tbase
      RETURN
   ENDIF

;---------------------------------------------------------------------------
;  Replace with modified entry
;---------------------------------------------------------------------------
   db_inst_plans = inst_plans
   mk_soho_save
   zplan = inst_plans(index)
   mk_plan_rem, zplan, inst_plans, err=err
   IF err NE '' THEN BEGIN
      xack, err, /icon
      WIDGET_CONTROL, comment, set_value=err, /app
      inst_plans = db_inst_plans
      xkill, tbase
      RETURN
   ENDIF

   mk_plan_add, plan, inst_plans, err=err
   IF err NE '' THEN BEGIN
      xack, err, /icon
      WIDGET_CONTROL, comment, set_value=err, /app
      inst_plans = db_inst_plans
      xkill, tbase
      RETURN
   ENDIF
   xmessage, write_mess, wbase=tbase
   WIDGET_CONTROL, event.top, /hour

   mk_plan_change, inst_plans, ninst_plans, startdis, tgap=tgap
   mk_plan_write, ninst_plans, db_inst_plans
   IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
   mk_soho_update

;---------------------------------------------------------------------------
;  Replot plan
;---------------------------------------------------------------------------
   delvarx, hplan
   index = mk_plan_where(plan, inst_plans, status=in_db)
   IF in_db THEN BEGIN
      hplan = inst_plans(index) & plan=hplan
   ENDIF
   rep_mess = 'Plan entry replaced/moved successfully'
   WIDGET_CONTROL, comment, set_value=rep_mess, /append
   xmessage, [write_mess, rep_mess], wbase=tbase
   mk_soho_wrefresh
   mk_soho_plot
   mk_soho_button
   delvarx, etime, last_etime, last_epos, last_cpos, last_spos
END

PRO mk_soho_update, plot=plot
;---------------------------------------------------------------------------
;  Update plan COMMONS. Only need to be called whenever there is any
;  plan entry being added or removed
;---------------------------------------------------------------------------
@mk_soho_com
   IF exist(inst_plans) THEN BEGIN
      IF instrume EQ 'CDS' THEN cds_science = inst_plans
      IF instrume EQ 'SUMER' THEN sumer_science = inst_plans
   ENDIF

   IF exist(other_plans) THEN BEGIN
      IF exist(inst_plans) THEN $
         soho_splan = concat_struct(other_plans, inst_plans) $
      ELSE $
         soho_splan = other_plans
   ENDIF ELSE BEGIN
      IF exist(inst_plans) THEN $
         soho_splan = inst_plans $
      ELSE delvarx, soho_splan
   ENDELSE

   IF KEYWORD_SET(plot) THEN BEGIN
      mk_soho_plot
      delvarx, ctime, last_cpos, last_epos
   ENDIF
   RETURN
END

PRO mk_soho_button, skip=skip
;---------------------------------------------------------------------------
;  Routine to make buttons look right
;
;  If the SKIP keyword is set, edit buttons are skipped for updating
;---------------------------------------------------------------------------
@mk_soho_com

;---------------------------------------------------------------------------
;  Selected a plan?
;---------------------------------------------------------------------------
   valid_plan = exist(plan)
      
;---------------------------------------------------------------------------
;     Deal with those edit buttons
;---------------------------------------------------------------------------
   IF custom_stc.ops_mode EQ 0 AND NOT KEYWORD_SET(skip) THEN BEGIN

;---------------------------------------------------------------------------
;     valid highlighted plan to edit?
;---------------------------------------------------------------------------
      valid_hplan = 0
      IF exist(hplan) THEN BEGIN
         valid_hplan = ((hplan.(itime) GE startdis) AND $
                        (hplan.(itime) LE stopdis)) OR $
                       ((hplan.(itime+1) GE startdis) AND $
                        (hplan.(itime+1) LE stopdis))
      ENDIF

      chg_hplan = 0
      IF valid_plan AND exist(hplan) THEN chg_hplan = match_struct(hplan, plan)

;---------------------------------------------------------------------------
;     valid cursor time?
;---------------------------------------------------------------------------
      valid_ctime = 0
      IF NOT exist(ctime) THEN ctime = startdis
      valid_ctime = (ctime GE startdis) AND (ctime LE stopdis)

;---------------------------------------------------------------------------
;     is cursor on a higlighted entry?
;---------------------------------------------------------------------------
      index = mk_plan_where(ctime, inst_plans, status=in_inst_plans)

;---------------------------------------------------------------------------
;     are there any plan entries in window?
;---------------------------------------------------------------------------
      valid_inst_plans = exist(inst_plans)
;       valid_inst_plans = 0
;       IF exist(inst_plans) THEN BEGIN
;          clook = WHERE(((inst_plans.(itime) GE startdis) AND $
;                         (inst_plans.(itime) LE stopdis) ) OR $
;                        ((inst_plans.(itime+1) GE startdis) AND $
;                         (inst_plans.(itime+1) LE stopdis) ), cnt)
;          valid_inst_plans = (cnt GT 0)
;       ENDIF

;---------------------------------------------------------------------------
;     Is time of last plan greater than current UT
;---------------------------------------------------------------------------
;       get_utc, utc
;       cur_ut = utc2tai(utc)
;       IF index EQ -1 THEN ok_to_append = (startdis GT cur_ut) ELSE $
;          ok_to_append = inst_plans(index).(itime+1) GT cur_ut

;       ok_to_edit = 0
;       IF exist(hplan) THEN ok_to_edit = hplan.(itime+1) GT cur_ut

;       ok_to_insert = (ctime GT cur_ut)
      ok_to_append = 1
      ok_to_edit = 1
      ok_to_insert = 1
      
;       IF FIX(getenv('PLAN_EDIT')) THEN BEGIN
;          ok_to_append = 1
;          ok_to_edit = 1
;          ok_to_insert = 1
;       ENDIF

      WIDGET_CONTROL, addb, sensitive=valid_ctime AND (chg_flag EQ 0) $
         AND ok_to_append
      WIDGET_CONTROL, instb, sensitive=valid_ctime AND (chg_flag EQ 0) $
         AND ok_to_insert

      app2last = 0
      IF in_inst_plans THEN BEGIN
         WIDGET_CONTROL, addb, set_value='Append to Current Entry Block'
      ENDIF ELSE BEGIN
         IF exist(inst_plans) THEN BEGIN
            ii = (WHERE(inst_plans.(itime+1) LT ctime))(0)
            app2last = ii GE 0
         ENDIF
         IF app2last THEN $
            WIDGET_CONTROL, addb, set_value='Append to Previous Entry' $
         ELSE $
            WIDGET_CONTROL, addb, set_value='Insert at Beginning of Display'
      ENDELSE

      WIDGET_CONTROL, movt, sensitive=(chg_flag EQ 0) AND chg_hplan $
         AND ok_to_edit

      WIDGET_CONTROL, movb, sensitive=valid_ctime AND ok_to_insert
      
;      WIDGET_CONTROL, gmov, sensitive=(stopdis GT cur_ut OR $
;                                       FIX(GETENV('PLAN_EDIT'))) AND $
;                                       valid_hplan
      
      WIDGET_CONTROL, gmov, sensitive=valid_hplan

      WIDGET_CONTROL, remb, sensitive=(chg_flag EQ 0) AND chg_hplan $
         AND ok_to_edit

      WIDGET_CONTROL, repb, sensitive=(chg_flag EQ 0) AND valid_plan $
         AND exist(hplan) AND ok_to_edit AND NOT chg_hplan

      WIDGET_CONTROL, chgb, sensitive=chg_hplan AND valid_ctime AND $
         ok_to_edit AND valid_hplan

      WIDGET_CONTROL, copy, sensitive=(chg_flag EQ 0) AND chg_hplan AND $
         ok_to_edit

      WIDGET_CONTROL, cleb, sensitive=(valid_inst_plans AND (chg_flag EQ 0))

;      WIDGET_CONTROL, cleb, sensitive=(valid_inst_plans AND (chg_flag EQ 0)) $
;         AND (startdis GT cur_ut OR FIX(GETENV('PLAN_EDIT')))
      
      WIDGET_CONTROL, inherit, sensitive=(N_ELEMENTS(rplan) NE 0 AND $
                                          user_mode NE 0)

      WIDGET_CONTROL, btbase, sensitive=(chg_flag EQ 0)
      WIDGET_CONTROL, row2, sensitive=(chg_flag EQ 0)
      WIDGET_CONTROL, row3, sensitive=(chg_flag EQ 0)
   ENDIF

   itool_on = xregistered('image_tool',/noshow)
   WIDGET_CONTROL, ptool, sensitive=(itool_on EQ 0)
   WIDGET_CONTROL, pnt_send, sensitive=(itool_on AND valid_plan)
   WIDGET_CONTROL, undob, sensitive=(exist(saved_plans) OR sdb_empty)
   WIDGET_CONTROL, clone, sensitive=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, eclone, sensitive=(exist(hplan) AND $
                                      custom_stc.ops_mode EQ 0)

   WIDGET_CONTROL, pmove, sensitive=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, obj_id, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, cmp_no, editable=custom_stc.ops_mode EQ 0

   WIDGET_CONTROL, date_obs, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, time_obs, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, date_end, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, time_end, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, date_cur, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, time_cur, editable=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, pduration, editable=custom_stc.ops_mode EQ 0

   WIDGET_CONTROL, sci_obj, editable=custom_stc.ops_mode EQ 0, $
      all_text=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, sci_spec, editable=custom_stc.ops_mode EQ 0, $
      all_text=custom_stc.ops_mode EQ 0
   WIDGET_CONTROL, disturb, editable=custom_stc.ops_mode EQ 0, $
      all_text=custom_stc.ops_mode EQ 0
   IF custom_stc.ops_mode NE 2 THEN BEGIN
      WIDGET_CONTROL, notes, editable=custom_stc.ops_mode EQ 0, $
         all_text=custom_stc.ops_mode EQ 0
   ENDIF

   WIDGET_CONTROL, ebase, map=(custom_stc.ops_mode EQ 0)

   RETURN
END

PRO mk_soho_wrefresh, empty=empty
;---------------------------------------------------------------------------
;  Routine to update widget contents
;
;  The EMPTY keyword will clear up all text widgets
;---------------------------------------------------------------------------
@mk_soho_com

   IF custom_stc.ops_mode EQ 0 THEN BEGIN
      mess_txt = $
         ['You are in Edit Mode. To add a new plan entry, fill in '+$
          'necessary plan fields and press the Add button; to select ', $
          'an existing plan entry for editing, point the cursor over it '+$
          'and click the left mouse button.']
   ENDIF ELSE BEGIN
      mess_txt = 'You are in Display Mode. To reveal contents of a plan '+$
         'entry, point the cursor over it and click any mouse button.'
   ENDELSE
   WIDGET_CONTROL, comment, set_value=mess_txt

   WIDGET_CONTROL, inst_txt, set_value=instrume
   IF N_ELEMENTS(ctime) NE 0 THEN BEGIN
      WIDGET_CONTROL, date_cur, set_value=anytim2utc(ctime, /ecs, /date)
      WIDGET_CONTROL, time_cur, set_value=anytim2utc(ctime, /ecs, /time)
   ENDIF

   IF NOT KEYWORD_SET(empty) THEN BEGIN
      IF NOT exist(plan) THEN RETURN
      itime = get_plan_itime(plan)
      IF itime LT 0 THEN RETURN
      WIDGET_CONTROL, object, set_value=STRTRIM(plan.object,2)
      WIDGET_CONTROL, prog_id, set_value=STRTRIM(num2str(plan.prog_id), 2)
      WIDGET_CONTROL, obj_id, set_value=STRTRIM(plan.obj_id,2)
      WIDGET_CONTROL, cmp_no, set_value=STRTRIM(num2str(plan.cmp_no), 2)
      WIDGET_CONTROL, date_obs, set_value=tai2utc(plan.(itime), /ecs, /date)
      WIDGET_CONTROL, time_obs, set_value=tai2utc(plan.(itime), /ecs, /time)
      WIDGET_CONTROL, date_end, set_value=tai2utc(plan.(itime+1), /ecs, /date)
      WIDGET_CONTROL, time_end, set_value=tai2utc(plan.(itime+1), /ecs, /time)
      dur = (plan.(itime+1)-plan.(itime))/dur_unit
      WIDGET_CONTROL, pduration, set_value=$
         STRTRIM(num2str(dur, FORMAT='(f20.2)'), 2)
      WIDGET_CONTROL, sci_obj, set_value=STRTRIM(plan.sci_obj,2)
      WIDGET_CONTROL, sci_spec, set_value=STRTRIM(plan.sci_spec,2)
      WIDGET_CONTROL, disturb, set_value=STRTRIM(plan.disturbances,2)
      IF plan.xcen NE '' THEN $
         WIDGET_CONTROL, xcen, set_value=STRTRIM(plan.xcen, 2)
      IF plan.ycen NE '' THEN $
         WIDGET_CONTROL, ycen, set_value=STRTRIM(plan.ycen, 2)
      IF custom_stc.ops_mode NE 2 THEN BEGIN
         WIDGET_CONTROL, notes, set_value=STRTRIM(plan.notes,2)
      ENDIF ELSE BEGIN
         tmp = str2arr(plan.xcen,',')
         WIDGET_CONTROL, npoint_txt, set_value=$
            STRTRIM(STRING(N_ELEMENTS(tmp)),2) 
         WIDGET_CONTROL, obsprog, set_value=STRTRIM(plan.obs_prog, 2)
         WIDGET_CONTROL, ixwidth, set_value=STRTRIM(plan.ixwidth,2)
         WIDGET_CONTROL, iywidth, set_value=STRTRIM(plan.iywidth,2)
         WIDGET_CONTROL, angle, set_value=STRTRIM(plan.angle,2)
         WIDGET_CONTROL, jitter, $
            set_value=STRTRIM(num2str(plan.jitter_limit),2)
      ENDELSE
   ENDIF ELSE BEGIN
      WIDGET_CONTROL, object, set_value=''
      WIDGET_CONTROL, prog_id, set_value=''
      WIDGET_CONTROL, obj_id, set_value=''
      WIDGET_CONTROL, cmp_no, set_value=''
      WIDGET_CONTROL, date_obs, set_value=''
      WIDGET_CONTROL, time_obs, set_value=''
      WIDGET_CONTROL, date_end, set_value=''
      WIDGET_CONTROL, time_end, set_value=''
      WIDGET_CONTROL, sci_obj, set_value=''
      WIDGET_CONTROL, sci_spec, set_value=''
      WIDGET_CONTROL, disturb, set_value=''
      WIDGET_CONTROL, pduration, set_value=''
      WIDGET_CONTROL, xcen, set_value=''
      WIDGET_CONTROL, ycen, set_value=''
      IF custom_stc.ops_mode NE 2 THEN BEGIN
         WIDGET_CONTROL, notes, set_value=''
      ENDIF ELSE BEGIN
         WIDGET_CONTROL, obsprog, set_value=''
         WIDGET_CONTROL, ixwidth, set_value=''
         WIDGET_CONTROL, iywidth, set_value=''
         WIDGET_CONTROL, npoint_txt, set_value=''
         WIDGET_CONTROL, angle, set_value=''
         WIDGET_CONTROL, jitter, set_value=''
      ENDELSE
   ENDELSE
   WIDGET_CONTROL, start_text, set_value=tai2utc(startdis, /ecs, /truncate)
   RETURN
END

PRO mk_soho_plot, nohighlight=nohighlight
;---------------------------------------------------------------------------
;  Routine to plot and optionally read science plans
;
;  Use NOHIGHLIGHT keyword to avoid highlighting plan entry (useful
;  when writing GIF files)
;---------------------------------------------------------------------------

@mk_soho_com

   IF stopdis LE startdis THEN stopdis = startdis+sdur

   sdur = stopdis-startdis

   plot_items = custom_stc.req_items
   set_line_color

   IF (!d.name EQ 'PS') THEN BEGIN
      font_sav = !p.font
      !p.font=0
   ENDIF

   plot_splan, wind=win_index, now=custom_stc.show_now, color=color_stc, $
      plan_items=custom_stc.req_items, days=custom_stc.show_day, $
      local=custom_stc.show_local, dtype=custom_stc.dtype, nocolor=nocolor

   IF exist(hplan) AND NOT KEYWORD_SET(nohighlight) AND (!d.name EQ 'X') $
      THEN BEGIN
      index = mk_plan_where(hplan, inst_plans, match, /FLOAT)
      IF match THEN BEGIN
         inst_name = get_soho_inst(hplan.instrume)
         aa = grep(inst_name, plot_items, index=row_tmp)
         curr_row = N_ELEMENTS(plot_items)-row_tmp-1
         IF curr_row GE 0 THEN oplot_splan, hplan, startdis, $
            color=2*(1-nocolor), row=curr_row
      ENDIF ELSE delvarx, hplan
   ENDIF

   IF exist(font_sav) THEN !p.font = font_sav
   RETURN
END

PRO mk_soho_switch, new_mode
;---------------------------------------------------------------------------
;  Routine to switch operation modes
;---------------------------------------------------------------------------
@mk_soho_com
   switch_yes = 0
   IF N_ELEMENTS(curr_mode) EQ 0 THEN curr_mode = -1
   switch_yes = new_mode NE curr_mode
   IF switch_yes THEN BEGIN
      WIDGET_CONTROL, omode, set_value=new_mode, /hour
      delvarx, plan, hplan, ctime, inst_plans
      xmessage, 'Reading data from DB...', wbase=tbase
      IF new_mode EQ 2 THEN BEGIN
         IF N_ELEMENTS(soho_dplan) EQ 0 THEN BEGIN
            mk_plan_read, custom_stc, /force
         ENDIF
         WIDGET_CONTROL, sbase, map=0
         WIDGET_CONTROL, dbase, /map
         WIDGET_CONTROL, dbase, get_uvalue=input_struct
         IF exist(soho_dplan) THEN itime = get_plan_itime(soho_dplan)
      ENDIF ELSE BEGIN
         IF N_ELEMENTS(soho_splan) EQ 0 THEN BEGIN
            mk_plan_read, custom_stc, /force
         ENDIF
         WIDGET_CONTROL, dbase, map=0
         WIDGET_CONTROL, sbase, /map
         WIDGET_CONTROL, sbase, get_uvalue=input_struct
         IF exist(soho_splan) THEN itime = get_plan_itime(soho_splan)
      ENDELSE
      xkill, tbase
@unpack_struct
   ENDIF

   curr_mode = new_mode

   IF curr_mode NE 2 THEN BEGIN
      IF instrume EQ '' THEN BEGIN
;---------------------------------------------------------------------------
;        Determine the default instrument row (will be CDS if found)
;---------------------------------------------------------------------------
         n_items = N_ELEMENTS(custom_stc.req_items)
         aa = (grep('CDS', custom_stc.req_items, /exact, index=idx))(0)
         IF aa NE '' THEN BEGIN
            instrume = 'CDS'
            IF N_ELEMENTS(last_row) EQ 0 THEN last_row = (n_items-1-idx > 0)
         ENDIF ELSE BEGIN
            aa = grep(custom_stc.req_items(n_items-1), get_soho_inst(), /exact)
            IF aa(0) NE '' THEN instrume = custom_stc.req_items(n_items-1)
            IF N_ELEMENTS(last_row) EQ 0 THEN last_row = 0
         ENDELSE
      ENDIF
      mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
      IF curr_mode EQ 0 THEN BEGIN
         def_inst_plan, plan, type=2, inst=get_soho_inst(instrume), $
            start=startdis
         ctime = startdis
         mk_soho_button
      ENDIF ELSE BEGIN
         delvarx, plan, hplan, ctime, inst_plans
         instrume = ''
      ENDELSE
   ENDIF

   ii = (WHERE(dur_unit EQ ['1.0', '60.0', '3600.0']))(0)
   IF ii GE 0 THEN WIDGET_CONTROL, dunit, set_value=ii

   IF switch_yes THEN mk_soho_plot
   mk_soho_wrefresh, empty=custom_stc.ops_mode NE 0
   RETURN
END

PRO mk_soho_event, event
;---------------------------------------------------------------------------
;  Event handler
;---------------------------------------------------------------------------
@mk_soho_com

   xkill, tbase
   WIDGET_CONTROL, event.id, get_uvalue=uvalue
   write_mess = 'Please wait. Updating Database....'

;---------------------------------------------------------------------------
;  Deal with timer event
;---------------------------------------------------------------------------
   IF event.id EQ event.top THEN BEGIN
      get_utc, curr_ut, /ecs, /truncate
      WIDGET_CONTROL, event.top, $
         tlb_set_title=tool_title+curr_ut+' GMT'
      WIDGET_CONTROL, event.top, timer=1.d0
      RETURN
   END

;---------------------------------------------------------------------------
;  Force user to exit "Shift" mode by pressing "Commit" button.
;---------------------------------------------------------------------------
   IF (chg_flag NE 0) THEN BEGIN
      IF (event.id NE chgb) AND (event.id NE draw_id_soho) THEN BEGIN
         WIDGET_CONTROL, comment, set_value='Press "Commit" to proceed..'
         RETURN
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Deal with event that is triggered by pointing tool
;---------------------------------------------------------------------------
   IF event.id EQ ebase THEN BEGIN 
      IF custom_stc.ops_mode EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;        Only allow changing pointing parameters in edit mode
;---------------------------------------------------------------------------
         WIDGET_CONTROL, ebase, get_uvalue=point_stc
         IF N_ELEMENTS(plan) EQ 0 THEN RETURN
         plan_sv = plan
         plan.xcen = point_stc.pointings.ins_x
         plan.ycen = point_stc.pointings.ins_y
         IF exist(hplan) THEN BEGIN
            he_match = match_struct(plan, hplan)
            IF NOT he_match THEN BEGIN
               IF xanswer(['Message from MK_SOHO:', $
                           'New pointing parameter received. Accept it?'], $
                          group=event.top, /beep, /center) THEN BEGIN
                  WIDGET_CONTROL, event.top, /map, /show
                  mk_soho_move, event, /replace
                  mk_soho_button
                  RETURN
               ENDIF ELSE plan = plan_sv
            ENDIF
         ENDIF
      ENDIF
      mk_soho_plot
      mk_soho_button
      RETURN
   ENDIF

;---------------------------------------------------------------------------
;  Change duration of plot window
;---------------------------------------------------------------------------
   time_spans = ['3HRS', '6HRS', '12HRS', '1DAY', '1_5DAY', '2DAY', '3DAY', $
                 '4DAY', '5DAY', '6DAY', '7DAY']
   sec_spans = [3., 6., 12., 24., 36., 48., 72., 96., 120., 144., 168.]*3600.d0
   chk = WHERE(uvalue EQ time_spans, cnt)
   change_dur = (cnt GT 0)

   IF change_dur THEN BEGIN
      sdur = sec_spans(chk(0))
      custom_stc.stopdis = startdis+sdur
   ENDIF

;---------------------------------------------------------------------------
;  Shift plot window around in time (Note: month is 30 days)
;---------------------------------------------------------------------------
   spd = 24.d*3600.d
   time_shift = ['BACKMON', 'BACKWK', 'BACKDAY', 'BACKHR', $
                 'FORMON', 'FORWK', 'FORDAY', 'FORHR']
   sec_shift = [30., 7., 1.d, 3600./spd]*spd
   sec_shift = [-sec_shift, sec_shift]

   chk = WHERE(uvalue EQ time_shift, cnt)
   change_start = (cnt GT 0)
   IF change_start THEN BEGIN
      custom_stc.startdis = startdis+sec_shift(chk(0))
      custom_stc.stopdis = custom_stc.startdis+sdur
   ENDIF

;---------------------------------------------------------------------------
;  Refresh plot window
;---------------------------------------------------------------------------
   IF (change_dur) OR (change_start) THEN BEGIN
      WIDGET_CONTROL, /hour
      xmessage, 'Reading data from DB...', wbase=tbase
      mk_plan_read, custom_stc
      mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
      mk_soho_plot
      mk_soho_wrefresh
      xkill, tbase
      RETURN
   ENDIF

   CASE uvalue OF
      'send': mk_plan_send, plan, ebase
      
      'csr_track': BEGIN
         IF track_cursor EQ 1 THEN BEGIN
            track_cursor = 0
            WIDGET_CONTROL, draw_id_soho, draw_motion=0
            WIDGET_CONTROL, csr_track, set_value='Trace Cursor Time'
            IF N_ELEMENTS(draw_time) NE 0 THEN $
               XYOUTS, 0, 0, draw_time, /DEVICE, color=!p.background
         ENDIF ELSE BEGIN
            track_cursor = 1
            WIDGET_CONTROL, draw_id_soho, draw_motion=1
            WIDGET_CONTROL, csr_track, set_value='Stop Tracing Cursor Time'
         ENDELSE
      END

      'inherit': BEGIN
         rtime = get_plan_itime(rplan)
         sav_plan = plan
         CASE event.id OF
            inhb_start: BEGIN
;---------------------------------------------------------------------------
;              inherit start time only
;---------------------------------------------------------------------------
               plan.(itime) = rplan.(rtime)
               IF NOT exist(hplan) THEN BEGIN
                  ctime = plan.(itime)
                  delvarx, etime
                  mk_soho_tplot
               ENDIF
            END
            inhb_stop: BEGIN
;---------------------------------------------------------------------------
;              inherit stop time only
;---------------------------------------------------------------------------
               plan.(itime+1) = rplan.(rtime+1)
               IF NOT exist(hplan) THEN BEGIN
                  etime = plan.(itime+1)
                  delvarx, ctime
                  mk_soho_tplot
               ENDIF
            END
            inhb_2start: BEGIN
;---------------------------------------------------------------------------
;              inherit stop time as start time
;---------------------------------------------------------------------------
               plan.(itime) = rplan.(rtime+1)
               IF NOT exist(hplan) THEN BEGIN
                  ctime = plan.(itime)
                  delvarx, etime
                  mk_soho_tplot
               ENDIF
            END
            inhb_ton: BEGIN
;---------------------------------------------------------------------------
;              inherit start and stop times only
;---------------------------------------------------------------------------
               plan.(itime) = rplan.(rtime)
               plan.(itime+1) = rplan.(rtime+1)
               IF NOT exist(hplan) THEN BEGIN
                  ctime = plan.(itime)
                  etime = plan.(itime+1)
                  mk_soho_tplot
               ENDIF
            END
            inhb_not: BEGIN
;---------------------------------------------------------------------------
;              inherit all except time
;---------------------------------------------------------------------------
               IF tag_exist(plan, 'POINTINGS') AND $
                  tag_exist(rplan, 'POINTINGS') THEN $
                  plan = rep_tag_value(plan, rplan.pointings, 'POINTINGS')
               copy_struct, rplan, plan
               plan.(0) = sav_plan.(0)
               plan.instrume = sav_plan.instrume
               plan.(itime) = sav_plan.(itime)
               plan.(itime+1) = sav_plan.(itime+1)
            END
            ELSE: BEGIN
;---------------------------------------------------------------------------
;              inherit all including time
;---------------------------------------------------------------------------
               IF tag_exist(plan, 'POINTINGS') AND $
                  tag_exist(rplan, 'POINTINGS') THEN $
                  plan = rep_tag_value(plan, rplan.pointings, 'POINTINGS')
               copy_struct, rplan, plan
               plan.(0) = sav_plan.(0)
               plan.instrume = sav_plan.instrume
               plan.(itime) = rplan.(rtime)
               plan.(itime+1) = rplan.(rtime+1)
               IF NOT exist(hplan) THEN BEGIN
                  ctime = plan.(itime)
                  etime = plan.(itime+1)
                  mk_soho_tplot
               ENDIF
            END
         ENDCASE
         IF plan.sci_spec EQ '' THEN plan.sci_spec = sav_plan.sci_spec
         IF exist(hplan) THEN BEGIN
            he_match = match_struct(plan, hplan)
            mk_soho_move, event, /replace
         ENDIF ELSE BEGIN
            he_match = 0
            mk_soho_wrefresh
         ENDELSE
         mk_soho_button
      END

      'ZOOM': BEGIN
         IF zoom EQ 1 THEN BEGIN
;---------------------------------------------------------------------------
;           Do zoom out, restore startdis and stopdis
;---------------------------------------------------------------------------
            zoom = 0
            startdis = startdis_sv
            stopdis = stopdis_sv
            WIDGET_CONTROL, start_text, set_value=$
               tai2utc(startdis, /ecs, /truncate)
            WIDGET_CONTROL, zoom_bt, set_value='Zoom In'
            mk_soho_plot
         ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;           Do zoom in, select new startdis and stopdis
;---------------------------------------------------------------------------
            zoom = 1
            startdis_sv = startdis
            stopdis_sv = stopdis
            WIDGET_CONTROL, zoom_bt, set_value='Zoom Out'
            WIDGET_CONTROL, start_text2, set_value=tai2utc(startdis, $
                                                           /ecs, /trun)
            WIDGET_CONTROL, stop_text, set_value=tai2utc(stopdis, $
                                                         /ecs, /truncate)
            WIDGET_CONTROL, span_text, set_value=$
               STRTRIM(num2str((stopdis-startdis), forma='(f20.2)'), 2)

            WIDGET_CONTROL, comment, set_value=$
               'Please click a mouse button to select edges of zoom-in window'
            do_zoom = 1
         ENDELSE
         ok = (zoom EQ 0)
         WIDGET_CONTROL, zoom_in, map=(NOT ok)
         WIDGET_CONTROL, zoom_out, map=ok
         WIDGET_CONTROL, resb, sensitive=ok
         WIDGET_CONTROL, custb, sensitive=ok
         WIDGET_CONTROL, kapb, sensitive=ok
         WIDGET_CONTROL, quit, sensitive=ok
         RETURN
      END

      'modes': BEGIN
         custom_stc.ops_mode = event.index
         IF custom_stc.ops_mode EQ 2 THEN $
            custom_stc.dtype = 1 $
         ELSE $
            custom_stc.dtype = 0
         WIDGET_CONTROL, /hour
         mk_soho_switch, custom_stc.ops_mode
      END

      'umode': BEGIN
         IF user_mode EQ 0 THEN BEGIN
            user_mode = 1
            WIDGET_CONTROL, umode, set_value='Switch to Novice Mode'
         ENDIF ELSE BEGIN
            user_mode = 0
            WIDGET_CONTROL, umode, set_value='Switch to Advanced Mode'
         ENDELSE
      END

      'view_kap': BEGIN
         WIDGET_CONTROL, /hour
         err = ''
         kap_file = get_latest_kap(startdis, status=status, err=err)
         IF status GT 0 THEN BEGIN
            text = rd_ascii(kap_file)
            title = 'KAP File for '+anytim2utc(startdis, /ecs, /date)
            xtext, text, group=event.top, /modal, title=title, $
               font='fixed', space=0
         ENDIF ELSE BEGIN
            xmessage, err, WAIT=2
         ENDELSE
      END

      'kap': BEGIN
         WIDGET_CONTROL, /hourglass
         update_kap, startdis, status=status, back=0
         IF status THEN BEGIN
            xmessage, 'Retrieving data from database....', wbase=tbase
            mk_plan_read, custom_stc, /force
            IF custom_stc.ops_mode NE 2 THEN $
               mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
            mk_soho_plot
            xkill, tbase
         ENDIF
         RETURN
      END

      'cap': BEGIN
         IF exist(soho_splan) THEN BEGIN
            xiap, startdis, /cap, group=event.top, /modal, title='XCAP'
         ENDIF ELSE BEGIN
            xmessage, 'There is nothing to write!', wbase=tbase, WAIT=2.0
         ENDELSE
         RETURN
      END

;---------------------------------------------------------------------------
;     Save display
;---------------------------------------------------------------------------
      'PS_FORMAT': BEGIN
         IF N_ELEMENTS(ps_stc) EQ 0 THEN xps_setup, ps_stc, /initial
         tmp = date_code(tai2utc(startdis))
         IF tmp NE '' THEN ps_stc.filename = 'plan'+tmp+'.ps' ELSE $
            ps_stc.filename = 'plan.ps'
         xps_setup, ps_stc,group=event.top, status=status
         IF status THEN BEGIN
            ps, ps_stc.filename, color=ps_stc.color, copy=ps_stc.copy, $
               encapsulated=ps_stc.encapsulated, $
               interpolate=ps_stc.interpolate, portrait=ps_stc.portrait
            color_stc = get_inst_color(/stc)
            mk_soho_plot
            IF ps_stc.hard THEN BEGIN
               psplot, delete=ps_stc.delete, queue=ps_stc.printer
            ENDIF ELSE BEGIN
               psclose
            ENDELSE
            color_stc = get_inst_color(/stc)
         ENDIF
      END

      'save_gif': BEGIN
         IF event.id EQ mk_pform THEN BEGIN
            IF ABS(sdur-86400.d0) GT 10.0 THEN BEGIN
               xack, ['Please set the display time span to 1 day; also make', $
                      'sure that the display time starts at 0:00 UT.'], $
                  wbase=tbase, group=event.top, /modal
               RETURN
            ENDIF
         ENDIF
         day_sv = custom_stc.show_day
         custom_stc.show_day = 0
         mk_soho_plot, /nohi
         dcode = date_code(startdis)
         IF dcode NE '' THEN gif_file = 'plan'+STRMID(dcode,2,10)+'.gif' ELSE $
            gif_file = 'plan.gif'
         xinput, gif_file, 'Enter GIF filename', group=event.top, /modal,$
            status=status
         IF NOT status THEN RETURN
         IF gif_file EQ '' THEN gif_file = 'plan.gif'
         WIDGET_CONTROL, /hourglass
         x2gif, gif_file
         custom_stc.show_day = day_sv
         IF event.id EQ mk_pform THEN BEGIN
            path = getenv('SYNOP_DATA')
            IF path EQ '' THEN BEGIN
               xmessage,'Variable SYNOP_DATA not set!', WAIT=2.0
               RETURN
            ENDIF
            IF !version.os NE 'vms' THEN BEGIN
               path = path+'/.planning'
               tmp = '/tmp/oo'
               suffix = ' > /dev/null) >& '+tmp+'; cat '+tmp+'; rm -f '+$
                  tmp+'`; echo $a'
               cmd = 'set a = `(mv -f '+gif_file+' '+path+suffix
               spawn, cmd, result
               IF result(0) NE '' THEN BEGIN
                  xack, ['Plan form not copied to '+path+'.', $
                         'Error: '+result(0), $
                         'You have to do it manually outside of IDL.'], $
                     group=event.top, /modal
                  RETURN
               ENDIF
            ENDIF ELSE BEGIN
               xmessage, ['Sorry, but you cannot write a plan form from', $
                          'a machine running VMS.'], WAIT=2.0
               RETURN
            ENDELSE
         ENDIF
         mk_soho_plot
      END

;---------------------------------------------------------------------------
;     Run IMAGE_TOOL
;---------------------------------------------------------------------------
      'SUN': BEGIN
         WIDGET_CONTROL, /hour
         IF NOT exist(plan) THEN BEGIN
            image_tool, start=startdis, group=event.top, /modal
            mk_soho_plot
         ENDIF ELSE BEGIN
            point_stc = mk_plan_point(plan)
            point_stc.messenger = ebase
            WIDGET_CONTROL, ptool, sensitive=0
            image_tool, point=point_stc, start=startdis, group=event.top
         ENDELSE
      END

      'QUIT': BEGIN
         xkill, event.top, wbase
      END

      'HELP': BEGIN
         widg_help, 'mk_soho', TITLE='MK_SOHO HELP', group=event.top, $
            font='9x15bold', /modal, subtopic='Overview'
         RETURN
      END

      'CUSTOM': BEGIN
         WIDGET_CONTROL, /hour
         mk_soho_custom, custom_stc, status=status, group=event.top, /modal
         IF NOT status THEN RETURN
         IF tag_exist(custom_stc, 'NOCOLOR') THEN $
            nocolor = custom_stc.nocolor ELSE nocolor = 0
         sec_spans = [3., 6., 12., 24., 36., 48., 72., 96., 120., $
                      144., 168.]*3600.d0
         ii = WHERE((custom_stc.stopdis-custom_stc.startdis) EQ sec_spans, cnt)
         IF ii(0) GT 0 THEN WIDGET_CONTROL, tspan, set_value=ii(0)
         delvarx, hplan, last_cpos, last_spos, last_epos
         IF N_ELEMENTS(curr_mode) EQ 0 THEN curr_mode = -1
         switch_yes =  custom_stc.ops_mode NE curr_mode
         IF switch_yes THEN mk_soho_switch, custom_stc.ops_mode ELSE BEGIN
          mk_plan_read, custom_stc
          mk_soho_plot
         ENDELSE
         WIDGET_CONTROL, start_text, set_value=tai2utc(startdis,/ecs, /trun)
      END

      'START_TEXT': BEGIN
         WIDGET_CONTROL, start_text, get_value=new_start
         err = ''
         time = utc2tai(new_start, err=err)
         IF err NE '' THEN BEGIN
            xack, err, group=event.top, /modal
            RETURN
         ENDIF
         WIDGET_CONTROL, /hour
         custom_stc.startdis = time
         custom_stc.stopdis = custom_stc.startdis+sdur
         xmessage, 'Retrieving data from database....', wbase=tbase
         mk_plan_read, custom_stc
         mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
         xkill, tbase
         mk_soho_plot
      END

;       'STOP_TEXT': BEGIN
;          WIDGET_CONTROL, stop_text, get_value=new_stop
;          err = ''
;          time_tmp = utc2tai(new_stop, err=err)
;          IF err NE '' THEN BEGIN
;             xack, err, group=event.top, /modal
;             RETURN
;          ENDIF
;          custom_stc.stopdis = time_tmp
;          custom_stc.startdis = custom_stc.stopdis-sdur
;          mk_plan_read, custom_stc
;          mk_soho_plot
;       END

      'DRAW': BEGIN
         mk_soho_draw, event
      END

      'refresh': BEGIN
         mk_soho_plot
      END

      'reset': BEGIN
         IF custom_stc.ops_mode EQ 2 THEN type = 4 ELSE type = 2
         WIDGET_CONTROL, /hour
         def_inst_plan, plan, type=type, /init, start=startdis, $
            inst=get_soho_inst(instrume)
         mk_plan_reset
         delvarx, hplan, ctime, inst_plans, curr_mode
         xmessage, 'Retrieving data from database....', wbase=tbase
         mk_plan_read, custom_stc, /force
         IF custom_stc.ops_mode NE 2 THEN $
            mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
         mk_soho_plot
         mk_soho_button
         mk_soho_wrefresh
         xkill, tbase
      END

;---------------------------------------------------------------------------
;     User changes duration display unit
;---------------------------------------------------------------------------
      'dur_unit': BEGIN
         CASE (event.index) OF
            0: BEGIN
               dur_unit = 1.0
            END
            1: BEGIN
               dur_unit = 60.0
            END
            2: BEGIN
               dur_unit = 3600.0
            END
         ENDCASE
         mk_soho_wrefresh
      END

      ELSE:
   ENDCASE

   IF custom_stc.ops_mode NE 0 THEN BEGIN
      IF WIDGET_INFO(event.top, /valid) THEN BEGIN
         mk_soho_button
         xkill, tbase
      ENDIF
      RETURN
   ENDIF

;---------------------------------------------------------------------------
;  All events below are for edit mode only
;---------------------------------------------------------------------------

   IF uvalue EQ 'undo' THEN BEGIN
      IF NOT exist(saved_plans) THEN BEGIN
         IF sdb_empty EQ 0 THEN BEGIN
            xmessage, 'No operation to undo', wbase=tbase, WAIT=2
            RETURN
         ENDIF
      ENDIF
      WIDGET_CONTROL, /hourglass
      undo_mess='Please wait. Undoing last operation...'
      xmessage, undo_mess, wbase=tbase
      mk_plan_write, saved_plans, inst_plans
      mk_soho_update
      IF exist(s_hplan) THEN BEGIN
         hplan = s_hplan
         plan = hplan
      ENDIF
      IF NOT exist(inst_plans) THEN BEGIN
         delvarx, hplan
         def_inst_plan, plan, type=2, /init, start=startdis, $
            inst=get_soho_inst(instrume)
      ENDIF
      delvarx, saved_plans
      xmessage, [undo_mess, 'Undo successful.'], wbase=tbase
      mk_soho_plot
      mk_soho_wrefresh
      mk_soho_button
   ENDIF

;---------------------------------------------------------------------------
;  Ensure that all required fields are filled in
;---------------------------------------------------------------------------
   clook = WHERE(uvalue EQ ['insert', 'append', 'move', 'shift','replace'], $
                 count)
   IF count NE 0 THEN BEGIN
;---------------------------------------------------------------------------
;     Ensure that SCI_OBJ field is filled
;---------------------------------------------------------------------------
      IF STRTRIM(plan.sci_obj, 2) EQ '' THEN BEGIN
         IF exist(ctime) THEN BEGIN
            cplan = plan & cplan.(itime)=ctime
            cplan = get_plan_sci(cplan)
            cplan.(itime) = plan.(itime)
            plan = cplan
         ENDIF

         IF STRTRIM(plan.sci_obj, 2) EQ '' THEN BEGIN
            WIDGET_CONTROL, comment, $
               set_value='You must enter a non-empty string for SCI_OBJ!'
            REPEAT mk_soho_sciobj, event UNTIL STRTRIM(plan.sci_obj, 2) NE ''
            WIDGET_CONTROL, comment, set_value=''
         ENDIF
      ENDIF
;---------------------------------------------------------------------------
;     Ensure that end time is greater than start time
;---------------------------------------------------------------------------
      IF (plan.(itime+1)) LE (plan.(itime)) THEN BEGIN
         xack, 'Plan STOP_TIME must be greater than START_TIME', $
            group=event.top, /modal, instruct='OK'
         RETURN
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  Process text widget for date/time
;---------------------------------------------------------------------------
   IF uvalue EQ 'date_cur' OR uvalue EQ 'time_cur' THEN BEGIN
;---------------------------------------------------------------------------
;     User changes cursor time
;---------------------------------------------------------------------------
      WIDGET_CONTROL, date_cur, get_value=date
      WIDGET_CONTROL, time_cur, get_value=time
      date = STRTRIM(date(0), 2)
      time = STRTRIM(time(0), 2)
      IF date EQ '' THEN WIDGET_CONTROL, date_obs, get_value=date
      IF time EQ '' THEN WIDGET_CONTROL, time_obs, get_value=time
      in_string = STRTRIM(date(0), 2)+' '+STRTRIM(time(0), 2)
      IF NOT valid_time(in_string, err) THEN BEGIN
         xack, err, group=event.top, /modal
         RETURN
      ENDIF
      ctime = utc2tai(in_string)
      IF exist(last_cpos) THEN BEGIN
         IF exist(inst_plans) THEN BEGIN
            chk = WHERE(FLOAT(last_ctime) EQ $
                        FLOAT([inst_plans.(itime), inst_plans.(itime+1)]), cnt)
         ENDIF ELSE cnt = 0
         IF cnt EQ 0 THEN $
            OPLOT, [last_cpos, last_cpos], last_row+[0.02, 0.98], $
            color=nocolor*(!d.table_size-1)
      ENDIF
      day = tai2utc(startdis) & day.time=0 & day=utc2tai(day)
      x = (ctime-day)/3600.d
      OPLOT, [x, x], last_row+[0.02, 0.98], color=2*(1-nocolor), linestyle=1
      last_cpos = x
      last_ctime = ctime
      WIDGET_CONTROL, date_cur, set_value=tai2utc(ctime, /ecs, /date)
      WIDGET_CONTROL, time_cur, set_value=tai2utc(ctime, /ecs, /time)
   ENDIF

;---------------------------------------------------------------------------
;  User changes start time
;---------------------------------------------------------------------------
   obs_look = WHERE(uvalue EQ ['date_obs', 'time_obs'], cnt1)
   IF (cnt1 EQ 1) THEN BEGIN
      WIDGET_CONTROL, date_obs, get_value=date
      WIDGET_CONTROL, time_obs, get_value=time
      in_string = STRTRIM(date(0), 2)+' '+STRTRIM(time(0), 2)
      IF NOT valid_time(in_string, err) THEN BEGIN
         xack, err, group=event.top, /modal
         RETURN
      ENDIF
      ctime = utc2tai(in_string)
      plan.(itime) = utc2tai(in_string)
      IF exist(last_spos) THEN BEGIN
         OPLOT, [last_spos, last_spos], last_row+[0.02, 0.98], $
            color=nocolor*(!d.table_size-1)
      ENDIF
      day = tai2utc(startdis) & day.time=0 & day=utc2tai(day)
      x = (ctime-day)/3600.d
      OPLOT, [x, x], last_row+[0.02, 0.98], color=2*(1-nocolor), linestyle=3
      last_spos = x
      last_ctime = ctime
      WIDGET_CONTROL, date_obs, set_value=tai2utc(plan.(itime), /ecs, /date)
      WIDGET_CONTROL, time_obs, set_value=tai2utc(plan.(itime), /ecs, /time)
      tdur = (plan.(itime+1)-plan.(itime))/dur_unit > 0.d0
      WIDGET_CONTROL, pduration, set_value=$
         STRTRIM(num2str(tdur, FORMAT='(f20.2)'), 2)
      mk_soho_wrefresh
   ENDIF

;---------------------------------------------------------------------------
;  User changes end time
;---------------------------------------------------------------------------
   end_look = WHERE(uvalue EQ ['date_end', 'time_end'], cnt2)
   IF (cnt2 EQ 1) THEN BEGIN
      WIDGET_CONTROL, date_end, get_value=date
      WIDGET_CONTROL, time_end, get_value=time
      in_string = STRTRIM(date(0), 2)+' '+STRTRIM(time(0), 2)
      IF NOT valid_time(in_string, err) THEN BEGIN
         xack, err, group=event.top, /modal
         RETURN
      ENDIF
      etime = utc2tai(in_string)
      plan.(itime+1) = etime
      IF exist(last_epos) THEN BEGIN
         OPLOT, [last_epos, last_epos], last_row+[0.02, 0.98], $
            color=nocolor*(!d.table_size-1)
      ENDIF
      day = tai2utc(startdis) & day.time=0 & day=utc2tai(day)
      x = (etime-day)/3600.d
      OPLOT, [x, x], last_row+[0.02, 0.98], color=2*(1-nocolor), linestyle=2
      last_epos = x
      last_etime = etime
      WIDGET_CONTROL, date_end, set_value=tai2utc(plan.(itime+1), /ecs, /date)
      WIDGET_CONTROL, time_end, set_value=tai2utc(plan.(itime+1), /ecs, /time)
      tdur = (plan.(itime+1)-plan.(itime))/dur_unit > 0.d0
      WIDGET_CONTROL, pduration, set_value=$
         STRTRIM(num2str(tdur, FORMAT='(f20.2)'), 2)
   ENDIF

;---------------------------------------------------------------------------
;  User changes plan duration (and therefore end time)
;---------------------------------------------------------------------------
   IF uvalue EQ 'pduration' THEN BEGIN
      WIDGET_CONTROL, pduration, get_value=tdur
      dur = tdur(0) > 0.0
      etime = plan.(itime)+DOUBLE(dur)*dur_unit
      plan.(itime+1) = etime
      IF exist(last_epos) THEN BEGIN
         OPLOT, [last_epos, last_epos], last_row+[0.02, 0.98], $
            color=nocolor*(!d.table_size-1)
      ENDIF
      day = tai2utc(startdis) & day.time=0 & day=utc2tai(day)
      x = (etime-day)/3600.d
      OPLOT, [x, x], last_row+[0.02, 0.98], color=2*(1-nocolor), linestyle=2
      last_epos = x
      last_etime = etime
      WIDGET_CONTROL, date_end, set_value=tai2utc(etime, /ecs, /date)
      WIDGET_CONTROL, time_end, set_value=tai2utc(etime, /ecs, /time)
      WIDGET_CONTROL, pduration, set_value=$
         STRTRIM(num2str(dur, FORMAT='(f20.2)'), 2)
      uvalue = 'date_end'
   ENDIF

;---------------------------------------------------------------------------
;  Copy a plan
;---------------------------------------------------------------------------
   IF uvalue EQ 'copy' THEN BEGIN
      info = {row:last_row, startdis:startdis, stopdis:stopdis}
      WIDGET_CONTROL, draw_id_soho, draw_button=0
      WIDGET_CONTROL, ebase, sensitive=0
      msg = ['Please press and drag the left mouse button to move the plan entry', $
             'and click the right mouse button when done.']
      WIDGET_CONTROL, comment, set_value=msg
      wid = [date_obs, time_obs, date_end, time_end]
      xshow, draw_id_soho
      tplan = mk_plan_copy(hplan, info=info, new_row=new_row, err=err, wid=wid)
      WIDGET_CONTROL, draw_id_soho, draw_button=1
      WIDGET_CONTROL, ebase, sensitive=1
      IF err NE '' THEN RETURN
      IF FIX(getenv('PLAN_EDIT')) EQ 0 THEN BEGIN
         get_utc, utc
         cur_ut = utc2tai(utc)
         IF tplan.(itime) LT cur_ut THEN BEGIN
            xack,'You cannot move or copy the plan entry before current UT!',$
               group=event.top, /modal,/icon
            RETURN
         ENDIF
      ENDIF

      IF tplan.(itime) GT stopdis OR tplan.(itime+1) LT startdis THEN BEGIN
         msg = 'You have to drop the plan entry inside the display window!'
         WIDGET_CONTROL, comment, set_value=msg
         RETURN
      ENDIF

      IF new_row NE last_row THEN BEGIN
         IF event.id EQ gmov THEN BEGIN
            msg = 'You cannot move this plan entry to another instrument'
            WIDGET_CONTROL, comment, set_value=msg
            RETURN
         ENDIF
;---------------------------------------------------------------------------
;        Copied to another instrument or no where
;---------------------------------------------------------------------------
         n_items = N_ELEMENTS(custom_stc.req_items)
         IF new_row GE 0 AND new_row LE n_items-1 THEN $
            row_id = custom_stc.req_items(n_items-new_row-1)
         aa = grep(row_id, get_soho_inst(), /exact)
         IF aa(0) EQ '' THEN BEGIN
            msg = 'You must drop the plan entry to a valid instrument row!'
            WIDGET_CONTROL, comment, set_value=msg
            RETURN
         ENDIF ELSE BEGIN
            IF instrume NE '' THEN last_inst = instrume
            mk_soho_update
            instrume = aa(0)
            mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
            tplan.instrume = get_soho_inst(instrume, /short)
            last_row = new_row
         ENDELSE
      ENDIF
      IF event.id EQ gmov THEN BEGIN
         cur_dur = (plan.(itime+1)-plan.(itime)) > 0.
         plan.(itime) = tplan.(itime)
         plan.(itime+1) = plan.(itime)+cur_dur
         mk_soho_move, event, /replace
      ENDIF ELSE BEGIN
         ctime = tplan.(itime)
         plan = tplan
         ready = 1
         IF exist(inst_plans) THEN BEGIN
            ii = WHERE((ctime LE inst_plans.(itime+1)) AND $
                       (ctime GE inst_plans.(itime)), cnt)
            IF cnt GT 0 THEN BEGIN
               tplan = inst_plans(ii(0))
               tplan = mk_plan_tail(tplan, inst_plans, err=err)
               IF err NE '' THEN BEGIN
                  PRINT, err
                  RETURN
               ENDIF
               dur = (plan.(itime+1)-plan.(itime)) > 0.0
               plan.(itime) = tplan.(itime+1)
               plan.(itime+1) = plan.(itime)+dur
            ENDIF
         ENDIF
         uvalue = 'insert'
         hplan = plan
      ENDELSE
   ENDIF

;---------------------------------------------------------------------------
;  Add plan to DB
;---------------------------------------------------------------------------
   IF uvalue EQ 'insert' OR uvalue EQ 'append' THEN BEGIN
      err_mess1 = ['You have not entered a START time for this entry.', $
                   'Use the LEFT mouse button to mark the start time.']
      err_mess2 = ['You either have not entered a STOP time for this entry,', $
                   'or your STOP time is earlier than the START time.', $
                   'Use the RIGHT mouse button to mark the stop time.']
      plan_sv = plan
      
;---------------------------------------------------------------------------
;     start time entered?
;---------------------------------------------------------------------------
      IF uvalue EQ 'insert' AND NOT exist(ready) THEN BEGIN
         IF NOT exist(ctime) THEN BEGIN
            xack, err_mess1, group=event.top, /modal
            RETURN
         ENDIF
         plan.(itime) = DOUBLE(ctime)
         WIDGET_CONTROL, pduration, get_value=tdur
         dur = DOUBLE(tdur(0))*dur_unit > 0.d0
         IF dur LE 0.d0 THEN BEGIN
            xack, 'You should set a new STOP_TIME.', group=event.top, $
               /modal, /icon
            RETURN
         ENDIF
         plan.(itime+1) = plan.(itime)+dur
      ENDIF
      IF uvalue EQ 'append' THEN BEGIN
;---------------------------------------------------------------------------
;        Append a plan entry to the current entry block
;---------------------------------------------------------------------------
         dur = (plan.(itime+1)-plan.(itime)) > 0.d0
         IF dur EQ 0.d0 THEN BEGIN
            xack, err_mess2, group=event.top, /modal
            RETURN
         ENDIF

         IF app2last THEN BEGIN
;---------------------------------------------------------------------------
;           Append to previous entry
;---------------------------------------------------------------------------
            IF NOT exist(inst_plans) THEN BEGIN
               plan.(itime) = startdis
            ENDIF ELSE BEGIN
               ii = WHERE(inst_plans.(itime+1) LT ctime)
               IF ii(0) LT 0 THEN plan.(itime) = startdis ELSE BEGIN
                  tplans = inst_plans(ii)
                  plan.(itime) = MAX(tplans.(itime+1))
               ENDELSE
            ENDELSE
         ENDIF ELSE BEGIN
            IF N_ELEMENTS(inst_plans) NE 0 THEN BEGIN
               ii = (WHERE(inst_plans.(itime+1) LT ctime))(0)
               at_start = (ii LT 0)
            ENDIF ELSE at_start = 1
            IF at_start THEN BEGIN
;---------------------------------------------------------------------------
;              Insert at startdis
;---------------------------------------------------------------------------
               plan.(itime) = startdis
            ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;              Append to current entry block
;---------------------------------------------------------------------------
               tplan = mk_plan_tail(hplan, inst_plans, err=err)
               IF err NE '' THEN RETURN
               plan.(itime) = tplan.(itime+1)
            ENDELSE
         ENDELSE
         dprint, 'Using calculated time...'
         plan.(itime+1) = plan.(itime)+dur
      ENDIF

;---------------------------------------------------------------------------
;     now add new one
;---------------------------------------------------------------------------
      mk_soho_save
      IF exist(inst_plans) THEN db_inst_plans = inst_plans
      mk_plan_add, plan, inst_plans, err=err
      IF err NE '' THEN BEGIN
         WIDGET_CONTROL, comment, set_value=err, /app
         xack, err, /icon
         IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
         RETURN
      ENDIF
      xmessage, write_mess, wbase=tbase
      WIDGET_CONTROL, event.top, /hour
      mk_plan_change, inst_plans, ninst_plans, startdis, tgap=tgap
      HELP, ninst_plans, /str
      mk_plan_write, ninst_plans, db_inst_plans
      IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
      mk_soho_update

;---------------------------------------------------------------------------
;     replot plan
;---------------------------------------------------------------------------
      delvarx, hplan
      index = mk_plan_where(plan, inst_plans, status=in_db)
      IF in_db THEN BEGIN
         hplan = inst_plans(index) & plan=hplan
      ENDIF ELSE BEGIN
         PRINT, 'How come the added plan entry is not in DB??'
      ENDELSE

      IF err EQ '' THEN BEGIN
         succ_mess = 'Plan entry written successfully'
         WIDGET_CONTROL, comment, set_value=succ_mess, /append
         xmessage, [write_mess, succ_mess], wbase=tbase
         WAIT, 1
      ENDIF
      mk_soho_wrefresh
      mk_soho_plot
      mk_soho_button
      IF exist(hplan) THEN $
         oplot_splan, hplan, startdis, color=2*(1-nocolor), row=last_row

      delvarx, etime, last_etime, last_epos, last_cpos, last_spos
      xkill, tbase
      RETURN
   END

   IF uvalue EQ 'replace' THEN BEGIN
;---------------------------------------------------------------------------
;     Replace the highlighted plan with the current one
;---------------------------------------------------------------------------
      IF NOT exist(hplan) OR NOT exist(plan) THEN RETURN
      same = match_struct(plan, hplan)
      IF same THEN BEGIN
         WIDGET_CONTROL, comment, set_value=$
            'New entry same as existing; not replaced', /app
         RETURN
      ENDIF ELSE dprint, 'Replacing the highlighted plan with the ' + $
         'current one...'

;---------------------------------------------------------------------------
;     Get plan start time from widget
;---------------------------------------------------------------------------
      WIDGET_CONTROL, date_obs, get_value=date
      WIDGET_CONTROL, time_obs, get_value=time
      in_string = STRTRIM(date(0), 2)+' '+STRTRIM(time(0), 2)
      IF NOT valid_time(in_string, err) THEN BEGIN
         xack, err, group=event.top, /modal
         RETURN
      ENDIF
      plan.(itime) = utc2tai(in_string)

;---------------------------------------------------------------------------
;     Determine plan end time from duration widget
;---------------------------------------------------------------------------
      WIDGET_CONTROL, pduration, get_value=tdur
      dur = tdur(0) > 0.0
      plan.(itime+1) = plan.(itime)+DOUBLE(dur)*dur_unit

      mk_soho_move, event, /replace
   ENDIF

;---------------------------------------------------------------------------
;  Shift highlighted entry
;---------------------------------------------------------------------------
   IF uvalue EQ 'shift' THEN BEGIN
      IF chg_flag EQ 0 THEN BEGIN
;---------------------------------------------------------------------------
;           Set up things for a special draw widget event to change
;           current plan entry.
;---------------------------------------------------------------------------
         dprint, 'Entering shifting mode...'
         chg_flag = 1
         mk_soho_button
         IF exist(last_cpos) THEN BEGIN
            IF exist(inst_plans) THEN BEGIN
               chk = WHERE(last_ctime EQ [inst_plans.(itime), $
                                          inst_plans.(itime+1)], cnt)
            ENDIF ELSE cnt = 0
         ENDIF
         WIDGET_CONTROL, chgb, set_value='Commit'
         WIDGET_CONTROL, draw_id_soho, draw_motion=1
         WIDGET_CONTROL, comment, set_value=$
            ['Press left mouse button to change duration, and', $
             'middle button to shift the highlighted plan']
         iplan = mk_plan_where(hplan, inst_plans, status=in_db)
         t_flag = [0, 0]
         IF in_db THEN BEGIN
            IF iplan(0) EQ 0 THEN BEGIN
               t_low = startdis
               t_flag(0) = 1
            ENDIF ELSE BEGIN
               t_low = inst_plans(iplan(0)-1).(itime+1)
            ENDELSE
            IF iplan(0) EQ N_ELEMENTS(inst_plans)-1 THEN BEGIN
               t_high = stopdis
               t_flag(1) = 1
            ENDIF ELSE BEGIN
               t_high = inst_plans(iplan(0)+1).(itime)
            ENDELSE
            day = tai2utc(startdis) & day.time=0 & day=utc2tai(day)
            tlimit = ([t_low, t_high]-day)/3600.d
            htime = ([hplan.(itime), hplan.(itime+1)]-day)/3600.d
            shift_stc = {htime:htime, tlimit:tlimit, startdis:startdis, $
                         stopdis:stopdis, t_flag:t_flag, chg_flag:chg_flag, $
                         date_obs:date_obs, time_obs:time_obs, $
                         date_end:date_end, time_end:time_end, $
                         row:last_row, chgb:chgb}
         ENDIF
      ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;        Commit the changes
;---------------------------------------------------------------------------
         chg_flag = 0
         dprint, 'End shifting mode...'
         mk_soho_button
         WIDGET_CONTROL, chgb, set_value='Shift'
         WIDGET_CONTROL, draw_id_soho, draw_motion=(track_cursor NE 0)
         day = tai2utc(startdis) & day.time=0
         day = utc2tai(day)
         secs = DOUBLE(shift_stc.htime(*)*3600)
         IF plan.(itime) NE (day+secs(0)) OR $
            plan.(itime+1) NE (day+secs(1)) THEN BEGIN
            IF xanswer(['Plan observation time has been changed.', $
                        'Do you really want this change?'], /beep, $
                       group=event.top) THEN BEGIN
               plan.(itime) = day+secs(0)
               plan.(itime+1) = day+secs(1)
               same = match_struct(plan, hplan)
               IF same THEN BEGIN
                  MESSAGE, 'Plan not changed', /contin
                  RETURN
               ENDIF
               mk_soho_move, event, /replace
            ENDIF ELSE BEGIN
               mk_soho_plot
               IF exist(hplan) THEN $
                  oplot_splan, hplan, startdis, color=2*(1-nocolor), $
                  row=last_row
            ENDELSE
         ENDIF
      ENDELSE
   ENDIF

   CASE uvalue OF
;---------------------------------------------------------------------------
;     Edit buttons
;---------------------------------------------------------------------------
      'object': BEGIN
         list_object, objects, nobj
         IF nobj EQ 0 THEN BEGIN
            WIDGET_CONTROL, comment, set_value='No Objects in DB', /append
         ENDIF ELSE BEGIN
            xlist, objects, index, /select, group=event.top, /modal, font=''
            IF index GT -1 THEN BEGIN
               sobject = STRTRIM(objects(index).(1), 2)
               plan.object = STRTRIM(objects(index).(0), 2)
               WIDGET_CONTROL, object, set_value=plan.object
            ENDIF
         ENDELSE
      END

      'obj_id_txt' : BEGIN
         WIDGET_CONTROL, obj_id, get_value=text
         plan.obj_id = STRMID(text(0), 0, 6)
      END
      'obj_id' : BEGIN
         text = plan.obj_id
         IF text EQ '' THEN WIDGET_CONTROL, obj_id, get_value=text
         text = text(0)
         xinput, text, 'Enter Object ID [6 char max]', group=event.top, $
            /modal, max_len=6, bfont=''
         plan.obj_id = text
         WIDGET_CONTROL, obj_id, set_value=text
      END
      'prog_id': BEGIN
         list_program, programs, nprog
         IF nprog EQ 0 THEN BEGIN
            WIDGET_CONTROL, comment, set_value='No Programs in DB', /append
            plan.prog_id = 0
         ENDIF ELSE BEGIN
            xlist, programs, index, /select, group=event.top, /modal, font=''
            IF index GT -1 THEN BEGIN
               plan.prog_id = programs(index).prog_id
            ENDIF
         ENDELSE
         WIDGET_CONTROL, prog_id, set_value=num2str(plan.prog_id)
      END
      'cmpb': BEGIN
         list_campaign, startdis, stopdis, campaigns, ncamp
         IF ncamp EQ 0 THEN BEGIN
            WIDGET_CONTROL, comment, set_value=$
               'No Campaigns during displayed period', /append
            plan.cmp_no = 0
         ENDIF ELSE BEGIN
            xlist, mk_plan_conv(campaigns), index, /select, group=event.top, $
               /modal, font=''
            IF index GT -1 THEN BEGIN
               plan.cmp_no = campaigns(index).cmp_no
            ENDIF
         ENDELSE
         WIDGET_CONTROL, cmp_no, set_value=num2str(plan.cmp_no)
      END
      'sci_obj': mk_soho_sciobj, event
      'sobj_text': BEGIN
         WIDGET_CONTROL, sci_obj, get_value=str
         plan.sci_obj = STRTRIM(str(0), 2)
      END
      'sci_spec' : BEGIN
         IF N_ELEMENTS(spec_list) EQ 0 THEN BEGIN
            IF exist(soho_splan) THEN $
               spec_list = get_uniq_list(soho_splan.sci_spec) $
            ELSE $
               spec_list = ''
         ENDIF ELSE BEGIN
            IF exist(soho_splan) THEN $
               spec_list = get_uniq_list(soho_splan.sci_spec, orig=spec_list)
         ENDELSE
         text = (plan.sci_spec)(0)
         temp = xsel_list(spec_list, group=event.top, title='SCI_SPEC', $
                          subtitle='Available Science Specifications', $
                          initial=text, /update, status=status)
         plan.sci_spec = temp
         WIDGET_CONTROL, sci_spec, set_value=temp
      END
      'spec_text': BEGIN
         WIDGET_CONTROL, sci_spec, get_value=str
         plan.sci_spec = STRTRIM(str(0), 2)
      END
      'disturbances' : BEGIN
         text = plan.disturbances
         xinput, text, 'Enter description for Disturbances [50 char MAX]', $
            group=event.top, /modal, max_len=50, bfont=''
         plan.disturbances = text
         WIDGET_CONTROL, disturb, set_value=text
      END
      'disturb_text': BEGIN
         WIDGET_CONTROL, disturb, get_value=str
         plan.disturbances = STRTRIM(str(0), 2)
      END
      'notes': BEGIN
         text = plan.notes
         xinput, text, 'Enter Notes [50 char max]', group=event.top, $
            /modal, max_len=50, bfont=''
         plan.notes = text
         WIDGET_CONTROL, notes, set_value=text
      END
      'notes_text': BEGIN
         WIDGET_CONTROL, notes, get_value=str
         plan.notes = STRTRIM(str(0), 2)
      END
      'xcen': BEGIN
         WIDGET_CONTROL, xcen, get_value=str
         plan.xcen = STRTRIM(str(0), 2)
      END
      'ycen': BEGIN
         WIDGET_CONTROL, ycen, get_value=str
         plan.ycen = STRTRIM(str(0), 2)
      END

;---------------------------------------------------------------------------
;     Clone plans
;---------------------------------------------------------------------------
      'clone': BEGIN
         err = ''
         date = tai2utc(startdis)
         date.time = 0L
         date.mjd = date.mjd+1L
         xclone_plan, date, /soho, group=event.top, /modal, $
            err=err, status=status
         IF err NE '' THEN BEGIN
            xack, err, group=event.top
            RETURN
         ENDIF
         IF NOT status.status THEN RETURN
         WIDGET_CONTROL, event.top, /hour
         mk_soho_button
         mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
      END

;---------------------------------------------------------------------------
;     Move a series plans
;---------------------------------------------------------------------------
      'pmove': BEGIN
         IF exist(inst_plans) THEN db_inst_plans = inst_plans
         mk_soho_save
         err = ''
         curr_items = custom_stc.req_items
         nplot = N_ELEMENTS(curr_items)
         IF exist(ctime) THEN tstart = ctime ELSE tstart = startdis
         xmove_plan, tstart, group=event.top, /soho, $
            /modal, err=err, status=status, def_move=instrume
         IF err NE '' THEN BEGIN
            xack, err, group=event.top, /icon
            IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
            RETURN
         ENDIF
         IF NOT status.status THEN RETURN
         delvarx, hplan
         mk_soho_button
         mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
      END

;---------------------------------------------------------------------------
;     Clone entry
;---------------------------------------------------------------------------
      'eclone': BEGIN
         IF NOT exist(hplan) THEN RETURN
         WIDGET_CONTROL, event.top, /hour
         mk_soho_edp, hplan, soho_splan, error=error, status=status, /modal,$
            group=event.top
         IF status THEN BEGIN
            mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
            mk_soho_plot
            mk_soho_button
         ENDIF ELSE IF error(0) NE '' THEN xmessage, error, WAIT=2
      END

;---------------------------------------------------------------------------
;     Remove the same entry for all instruments
;---------------------------------------------------------------------------
      'remove_all': BEGIN
         IF  NOT exist(hplan) THEN RETURN
         IF xanswer(['This is an undoable operation.',$
                     'Are you sure you want to do it?'], $
                    /beep) EQ 0 THEN RETURN
         WIDGET_CONTROL, event.top, /hour
         xmessage,'Deleting similar entries from all instruments...', $
            wbase=tbase
         mk_soho_remove_all, hplan, get_soho_inst(/short), error=error
         xkill, tbase
         IF exist(error) THEN xmessage, error, WAIT=2
         mk_plan_reset
         delvarx, hplan
         mk_plan_read, custom_stc
         mk_soho_extplan, soho_splan, instrume, inst_plans, other_plans
         mk_soho_plot
         mk_soho_button
         delvarx, saved_plans
      END

;---------------------------------------------------------------------------
;     remove a plan entry
;---------------------------------------------------------------------------
      'remove' : BEGIN

;---------------------------------------------------------------------------
;        was it ever written?
;---------------------------------------------------------------------------
         index = mk_plan_where(plan, inst_plans, status=in_db)
         IF NOT in_db THEN BEGIN
            WIDGET_CONTROL, comment, set_value='Current entry not yet in DB', /app
            xkill, tbase
            RETURN
         ENDIF

         db_inst_plans = inst_plans
         mk_soho_save
         mk_plan_rem, hplan, inst_plans, err=err
         IF err NE '' THEN BEGIN
            WIDGET_CONTROL, comment, set_value=err, /app
            xack, err, /icon
            inst_plans = db_inst_plans
            xkill, tbase
            RETURN
         ENDIF
         xmessage, write_mess, wbase=tbase
         WIDGET_CONTROL, event.top, /hour
         mk_plan_change, inst_plans, ninst_plans, startdis
         mk_plan_write, ninst_plans, db_inst_plans
         IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
         mk_soho_update

;---------------------------------------------------------------------------
;        initialize and replot plan
;---------------------------------------------------------------------------
         def_inst_plan, plan, type=2, /init, start=startdis, $
            inst=get_soho_inst(instrume)
         delvarx, hplan
         rem_mess = 'Plan entry removed successfully'
         WIDGET_CONTROL, comment, set_value=rem_mess, /append
         xmessage, [write_mess, rem_mess], wbase=tbase & WAIT, 1
         mk_soho_wrefresh
         mk_soho_plot
         mk_soho_button
      END

;---------------------------------------------------------------------------
;     clear the database
;---------------------------------------------------------------------------
      'clear_all' : BEGIN
         IF exist(inst_plans) THEN BEGIN
            instruct = $
               ['You are about to delete all planned entries',$
                '(for current instrument) that are loaded in', $
                'the memory. Are you sure?']
            IF NOT xanswer(instruct, group=event.top, /flash, /beep) THEN $
               RETURN
            mk_soho_save
            db_inst_plans = inst_plans
            delvarx, inst_plans
            xmessage, write_mess, wbase=tbase
            WIDGET_CONTROL, event.top, /hour
            mk_plan_write, inst_plans, db_inst_plans
            mk_soho_update
            def_inst_plan, plan, type=2, /init, start=startdis, $
               inst=get_soho_inst(instrume)
            delvarx, hplan, ctime, etime
            mk_soho_wrefresh
            mk_soho_plot
            mk_soho_button
         ENDIF
      END

      'clear_in_display' : BEGIN
         IF exist(inst_plans) THEN BEGIN
            instruct = $
               ['You are about to delete all entries in the display from', $
                'the database. Are you sure?']
            IF NOT xanswer(instruct, group=event.top, /flash, /beep) THEN $
               RETURN
            db_inst_plans = inst_plans
            mk_soho_save
;---------------------------------------------------------------------------
;           Get plan entries outside the display window
;---------------------------------------------------------------------------
            ii = WHERE(inst_plans.(itime) LT startdis OR $
                       inst_plans.(itime+1) GT stopdis)
            IF ii(0) GE 0 THEN inst_plans = inst_plans(ii) ELSE $
               delvarx, inst_plans
            IF N_ELEMENTS(inst_plans) NE N_ELEMENTS(db_inst_plans) THEN BEGIN
               xmessage, write_mess, wbase=tbase
               WIDGET_CONTROL, event.top, /hour
               mk_plan_write, inst_plans, db_inst_plans
               IF exist(db_inst_plans) THEN inst_plans = db_inst_plans
               mk_soho_update
               def_inst_plan, plan, type=2, /init, start=startdis, $
                  inst=get_soho_inst(instrume)
               delvarx, hplan, ctime, etime
               mk_soho_wrefresh
               mk_soho_plot
               mk_soho_button
            ENDIF
         ENDIF
      END

      'move': mk_soho_move, event

      ELSE:
   ENDCASE

   IF WIDGET_INFO(event.top, /valid) THEN BEGIN
      mk_soho_button
      xkill, tbase
   ENDIF

   RETURN
END

;---------------------------------------------------------------------------
;  Main program starts here
;---------------------------------------------------------------------------
PRO mk_soho, tstart, group=group, modal=modal, edit=edit, reset=reset,$
             nodetach=nodetach, nores=nores

   ON_ERROR, 1

@mk_soho_com

   tgap = 1.d-3

   set_plot, 'x'

   IF NOT HAVE_WIDGETS() THEN MESSAGE, 'needs IDL widgets'
   IF N_PARAMS() GT 1 THEN $
      MESSAGE, 'Syntax: MK_SOHO [, start_date]'

   reset = KEYWORD_SET(reset) OR exist(mk_detail_base)

   nodetach = KEYWORD_SET(nodetach)
   IF KEYWORD_SET(modal) THEN nodetach = 1

   IF (NOT KEYWORD_SET(group))  THEN BEGIN
      xkill, /all
   ENDIF ELSE BEGIN
      IF xregistered('mk_soho') THEN BEGIN
         RETURN
      ENDIF
   ENDELSE

;---------------------------------------------------------------------------
;  Initializing some variables
;---------------------------------------------------------------------------
   IF N_ELEMENTS(user_mode) EQ 0 THEN user_mode = 0
   IF N_ELEMENTS(track_cursor) EQ 0 THEN track_cursor = 0
   
   chg_flag = 0
   zoom = 0
   do_zoom = 0
   ttime = DBLARR(2)
   instrume = ''
   def_inst_plan, plan, type=2
   itime = get_plan_itime(plan)
   sdb_empty = 0
   delvarx, hplan, saved_plans, s_hplan

;---------------------------------------------------------------------------
;  Get color structure
;---------------------------------------------------------------------------
   color_stc = get_inst_color(/stc)

   one_day = 24.d*3600.d

   IF N_ELEMENTS(sdur) EQ 0 THEN sdur = one_day

;---------------------------------------------------------------------------
;  The variable STARTDIS is the first time point displayed in the draw
;  window and is in TAI form.  Likewise, STOPDIS is the last displayed
;  time point.
;---------------------------------------------------------------------------
   IF N_ELEMENTS(tstart) NE 0 THEN BEGIN
      new_startdis = utc2tai(tstart)
   ENDIF ELSE BEGIN
      IF N_ELEMENTS(startdis) EQ 0 THEN BEGIN ;-- start of current day
         day = anytim2utc(!stime)
         day.time = 0
         new_startdis=utc2tai(day)
      ENDIF ELSE new_startdis = startdis
   ENDELSE

   new_stopdis = new_startdis+sdur

   IF reset THEN BEGIN
      dprint, 'Reseting...'
      mk_plan_reset
      delvarx, mk_detail_base, curr_mode, custom_stc, hplan
   ENDIF

;---------------------------------------------------------------------------
;  Structure for customizing displays
;---------------------------------------------------------------------------
   IF datatype(custom_stc) NE 'STC' THEN BEGIN
      soho_inst = ['CELIAS','CDS','SUMER','EIT','LASCO','UVCS','MDI','TRACE']
      refs = ['Command', soho_inst]
      IF KEYWORD_SET(nores) THEN refs = soho_inst
      nrefs = N_ELEMENTS(refs)
      IF KEYWORD_SET(edit) THEN ops_mode = 0 ELSE ops_mode = 1
      custom_stc = {startdis:new_startdis, stopdis:new_stopdis, $
                    req_items:refs, show_now:1, show_day:1, show_local:1, $
                    ops_mode:ops_mode, dtype:0, troff:1.0, ext_cursor:0, $
                    nocolor:0}
   ENDIF ELSE BEGIN
      custom_stc.startdis = new_startdis
      custom_stc.stopdis = new_stopdis
      IF KEYWORD_SET(edit) THEN BEGIN
         ops_mode = 0
         custom_stc.ops_mode = ops_mode
      ENDIF
   ENDELSE

   IF NOT exist(nocolor) THEN nocolor = 0
   IF tag_exist(custom_stc, 'NOCOLOR') THEN nocolor = custom_stc.nocolor

;---------------------------------------------------------------------------
;  Create the widget interface
;---------------------------------------------------------------------------
   bfont = '-adobe-courier-bold-r-normal--20-140-100-100-m-110-iso8859-1'
   bfont = (get_dfont(bfont))(0)

   ffont= get_dfont('-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1')
   IF ffont(0) EQ '' THEN ffont = 'fixed'
   lfont = ffont(0)

   DEVICE, get_screen_size = sz
   IF (sz(0) GE 1280) AND (sz(1) GE 1024) THEN sz(*) = 0
   sz = sz < [1280,1024]

   base = WIDGET_BASE(title='', /column, yoff=10, xoff=100, uvalue='UT_UPDATE')

   row1 = WIDGET_BASE(base, /row, space=20)

   btbase = WIDGET_BASE(row1, /row, /frame)
   file = WIDGET_BUTTON(btbase, value='File', /menu, font=bfont)

   temp = WIDGET_BUTTON(file, value='Save Plot', /menu, font=bfont)

   tmp = WIDGET_BUTTON(temp, value='in PS format', uvalue='PS_FORMAT')

   tmp = WIDGET_BUTTON(temp, value='as a GIF file', uvalue='save_gif')

   tmp = WIDGET_BUTTON(file, value='View Latest KAP', uvalue='view_kap',$
                       font=bfont)
   kapb = WIDGET_BUTTON(file, value='Update DB with KAP', uvalue='kap', $
                         font=bfont)
   tmp = WIDGET_BUTTON(file, value='Write CAP', uvalue='cap', font=bfont)
   mk_pform = WIDGET_BUTTON(file, value='Make Plan Form', $
                            uvalue='save_gif',font=bfont)
   quit = WIDGET_BUTTON(file, value='Quit', uvalue='QUIT', font=bfont, $
                        resource='QuitButton')

   temp = WIDGET_BUTTON(btbase, value='Options', /menu, font=bfont)

   zoom_bt = WIDGET_BUTTON(temp, value='', uvalue='ZOOM', font=bfont)
   IF zoom THEN $
      WIDGET_CONTROL, zoom_bt, set_value='Zoom Out' $
   ELSE $
      WIDGET_CONTROL, zoom_bt, set_value='Zoom In'

   clone = WIDGET_BUTTON(temp, value='Clone Plans', $
                         uvalue='clone', font=bfont)
   eclone = WIDGET_BUTTON(temp, value='Clone Entry', uvalue='eclone',$
                          font=bfont)
   pmove = WIDGET_BUTTON(temp, value='Move A Series Plans', uvalue='pmove',$
                        font=bfont)

   inherit = WIDGET_BUTTON(temp, value='Inherit Reference Plan', /menu, $
                           font=bfont)
   inhb_start = WIDGET_BUTTON(inherit, value='Start Time Only', $
                              uvalue='inherit', font=bfont)
   inhb_stop = WIDGET_BUTTON(inherit, value='Stop Time Only', $
                              uvalue='inherit', font=bfont)
   inhb_ton = WIDGET_BUTTON(inherit, value='Start and Stop Times Only', $
                            uvalue='inherit', font=bfont)
   inhb_2start = WIDGET_BUTTON(inherit, value='Stop Time as Start Time', $
                               uvalue='inherit', font=bfont)
   inhb_not = WIDGET_BUTTON(inherit, value='All Parameters Except Time',$
                            uvalue='inherit', font=bfont)
   inhb_all = WIDGET_BUTTON(inherit, value='All Parameters', $
                            uvalue='inherit', font=bfont)
   WIDGET_CONTROL, inherit, sensitive=0

   ptool = WIDGET_BUTTON(temp, value='Run Image Tool', uvalue='SUN', $
                         font=bfont)
   pnt_send = WIDGET_BUTTON(temp, value='Send to Pointing Tool', $
                            uvalue='send', font=bfont)

   custb = WIDGET_BUTTON(temp, value='Customize Display', uvalue='CUSTOM', $
                         font=bfont)
   resb = WIDGET_BUTTON(temp, value='Reset Plan', uvalue='reset', font=bfont)
   tmp = WIDGET_BUTTON(temp, value='Refresh Display', uvalue='refresh', $
                       font=bfont)
   umode = WIDGET_BUTTON(temp, value='', uvalue='umode',font=bfont)
   IF user_mode EQ 0 THEN $
      WIDGET_CONTROL, umode, set_value='Switch to Advanced Mode' $
   ELSE $
      WIDGET_CONTROL, umode, set_value='Switch to Novice Mode'
   
   csr_track = WIDGET_BUTTON(temp, value='', uvalue='csr_track', font=bfont)
   IF track_cursor EQ 1 THEN $
      WIDGET_CONTROL, csr_track, set_value='Stop Tracing Cursor Time' $
   ELSE $
      WIDGET_CONTROL, csr_track, set_value='Trace Cursor Time'
   
   tmp = WIDGET_BUTTON(btbase, value='Help', uvalue='HELP', font=bfont)

   ebase = WIDGET_BASE(row1, /row, /frame)

   tmp1 = WIDGET_BUTTON(ebase, value='Add', /menu, font=bfont)
   instb = WIDGET_BUTTON(tmp1, value='Insert at Cursor Position', $
                         uvalue='insert')
   addb = WIDGET_BUTTON(tmp1, value='Append to Current Entry Block', $
                        uvalue='append')
   repb = WIDGET_BUTTON(ebase, value='Replace', uvalue='replace', font=bfont)
   copy = WIDGET_BUTTON(ebase, value='Copy', uvalue='copy', font=bfont)

   movt = WIDGET_BUTTON(ebase, value='Move', font=bfont, /menu)
   movb = WIDGET_BUTTON(movt, value='to Current Cursor Position', $
                        uvalue='move')
   gmov = WIDGET_BUTTON(movt, value='Graphically', uvalue='copy')

   chgb = WIDGET_BUTTON(ebase, value='Shift', uvalue='shift', font=bfont)

   remb = WIDGET_BUTTON(ebase, value='Remove', /menu, font=bfont)
   tmp1 = WIDGET_BUTTON(remb, value='Highlighted Entry', uvalue='remove')
   tmp1 = WIDGET_BUTTON(remb, value='All Entries Starting at the Same Time', $
                        uvalue='remove_all')

   cleb = WIDGET_BUTTON(ebase, value='Clear', /menu, font=bfont)
   tmp = WIDGET_BUTTON(cleb, value='All Entries in Display', $
                        uvalue='clear_in_display')
   tmp = WIDGET_BUTTON(cleb, value='All Entries in Memory', uvalue='clear_all')
   undob = WIDGET_BUTTON(ebase, value='Undo', uvalue='undo', font=bfont)

   WIDGET_CONTROL, ebase, map=custom_stc.ops_mode EQ 0

;---------------------------------------------------------------------------
;  The base for fields
;---------------------------------------------------------------------------
   row2 = WIDGET_BASE(base)

   IF N_ELEMENTS(dur_unit) EQ 0 THEN dur_unit = 1.0
   mk_soho_sbase, row2, sbase, dur_unit=dur_unit, map=0, lfont=lfont
   mk_soho_dbase, row2, dbase, dur_unit=dur_unit, map=0, lfont=lfont

   IF custom_stc.ops_mode NE 2 THEN BEGIN
      WIDGET_CONTROL, sbase, /map
      WIDGET_CONTROL, sbase, get_uvalue=input_struct
   ENDIF ELSE BEGIN
      WIDGET_CONTROL, dbase, /map
      WIDGET_CONTROL, dbase, get_uvalue=input_struct
   ENDELSE
@unpack_struct

;---------------------------------------------------------------------------
;  Display controling and mode changing
;---------------------------------------------------------------------------

   row3 = WIDGET_BASE(base)

   zoom_out = WIDGET_BASE(row3, /row, map=0)
   zoom_in = WIDGET_BASE(row3, /row, map=0)

;---------------------------------------------------------------------------
;  Buttons for zoom-out mode
;---------------------------------------------------------------------------
   col = WIDGET_BASE(zoom_out, /row, /frame)

   tmp = WIDGET_BUTTON(col, value='<MONTH', uvalue='BACKMON', font=lfont)
   tmp = WIDGET_BUTTON(col, value='<WEEK', uvalue='BACKWK', font=lfont)
   tmp = WIDGET_BUTTON(col, value='<DAY ', uvalue='BACKDAY', font=lfont)
   tmp = WIDGET_BUTTON(col, value='<HOUR', uvalue='BACKHR', font=lfont)

   tspan_value = ['3 HOURS', '6 HOURS', '12 HOURS', '1 DAY', '1.5 DAYS', $
                  '2 DAYS', '3 DAYS', '4 DAYS', '5 DAYS', '6 DAYS', '7 DAYS']
   tspan_uvalue = ['3HRS', '6HRS', '12HRS', '1DAY', '1_5DAY', '2DAY', '3DAY', $
                   '4DAY', '5DAY', '6DAY', '7DAY']
   sec_spans = [3., 6., 12., 24., 36., 48., 72., 96., 120., 144., 168.]*3600.d0

   col = WIDGET_BASE(zoom_out, /row, /frame)

   tmp = WIDGET_LABEL(col, value='DISPLAY START', font=lfont)
   start_text = WIDGET_TEXT(col, value='', xsize=19, /editable, $
                            uvalue='START_TEXT', font=lfont)

   tmp = WIDGET_LABEL(col, value=' SPAN', font=lfont)
   tspan = cw_bselector2(col, tspan_value, return_uvalue=tspan_uvalue, $
                        ids=buttons, font=lfont)
   ii = WHERE(sdur EQ sec_spans, cnt)
   IF ii(0) GT 0 THEN WIDGET_CONTROL, tspan, set_value=ii(0)

   col = WIDGET_BASE(zoom_out, /row, /frame)

   tmp = WIDGET_BUTTON(col, value='HOUR>', uvalue='FORHR', font=lfont)
   tmp = WIDGET_BUTTON(col, value=' DAY>', uvalue='FORDAY', font=lfont)
   tmp = WIDGET_BUTTON(col, value='WEEK>', uvalue='FORWK', font=lfont)
   tmp = WIDGET_BUTTON(col, value='MONTH>', uvalue='FORMON', font=lfont)

   col = WIDGET_BASE(zoom_out, /row, /frame)
   omode = cw_bselector2(col, /return_index, uvalue='modes', font=lfont, $
                        ['EDIT SCI_PLAN', 'DISPLAY SCI_PLAN', $
                         'DISPLAY DET_PLAN'])
   WIDGET_CONTROL, omode, set_value=custom_stc.ops_mode
   tmp = WIDGET_LABEL(col, value='MODE', font=lfont)

;---------------------------------------------------------------------------
;  Buttons for zoom-in mode
;---------------------------------------------------------------------------
   col = WIDGET_BASE(zoom_in, /row, /frame)
   tmp = WIDGET_LABEL(col, value='ZOOM-IN MODE:   ',font=bfont)
   tmp = WIDGET_LABEL(col, value='DISPLAY START', font=lfont)
   start_text2 = WIDGET_TEXT(col, value='', xsize=19, $
                            uvalue='START_TEXT', font=lfont)

   tmp = WIDGET_LABEL(col, value='   DISPLAY END', font=lfont)
   stop_text = WIDGET_TEXT(col, value='', xsize=19, $
                            uvalue='STOP_TEXT', font=lfont)

   tmp = WIDGET_LABEL(col, value='   DISPLAY SPAN', font=lfont)
   span_text = WIDGET_TEXT(col, value='', xsize=10, $
                            uvalue='SPAN_TEXT', font=lfont)
   tmp = WIDGET_LABEL(col, value='(secs)', font=lfont)

   WIDGET_CONTROL, zoom_out, map=(zoom EQ 0)
   WIDGET_CONTROL, zoom_in, map=(zoom NE 0)

;---------------------------------------------------------------------------
;  Last row is for comment widget
;---------------------------------------------------------------------------
   comment = WIDGET_TEXT(base, ysize=2, /scroll, font=lfont)

   ysize = 500
   IF nodetach THEN BEGIN
      draw_id_soho = WIDGET_DRAW(base, ysize=ysize, xsize=700, /button_event, $
                            retain=2, /frame, uvalue='DRAW')
   ENDIF

   WIDGET_CONTROL, base, /realize, tlb_get_size=gsize, tlb_get_offset=goff

   tool_title = 'SOHO Science Planning Tool  (Version 4.8)         '
   IF !version.release LE '3.5.1' THEN BEGIN
      WIDGET_CONTROL, base, tlb_set_title = tool_title
   ENDIF ELSE BEGIN
      get_utc, curr_ut, /ecs, /truncate
      WIDGET_CONTROL, base, tlb_set_title = tool_title+curr_ut+' GMT'
      WIDGET_CONTROL, base, timer = 1.d0
   ENDELSE

   IF NOT nodetach THEN BEGIN
      draw_base = WIDGET_BASE(title='Display for MK_SOHO', group=base, $
                              event_pro='mk_soho_redraw', /tlb_size_events, $
                              yoff=goff(1)+0.85*gsize(1),xoff=goff(0))
      draw_id_soho = WIDGET_DRAW(draw_base, xsize=gsize(0), ysize=ysize, $
                                 /button_event, uvalue='DRAW')
      WIDGET_CONTROL, draw_base, /realize
   ENDIF

   WIDGET_CONTROL, draw_id_soho, get_value=win_index, $
      draw_motion=(track_cursor NE 0)
   setwindow, win_index

;---------------------------------------------------------------------------
;  Draw all instruments' plans for the input operational day
;---------------------------------------------------------------------------
   WIDGET_CONTROL, /hour
   xmessage, 'Reading PLAN database. Please standby....', wbase=tbase
   mk_plan_read, custom_stc, force=reset
   curr_mode = custom_stc.ops_mode
   mk_soho_switch, custom_stc.ops_mode
   mk_soho_button
   xkill, tbase
   
;---------------------------------------------------------------------------
;  Set color table to B/W and define colors for different plotting purpose
;---------------------------------------------------------------------------
   loadct, 0, /silent

   mk_soho_plot

   mk_soho_base = base
   IF KEYWORD_SET(modal) AND N_ELEMENTS(group) NE 0 THEN BEGIN
      IF WIDGET_INFO(LONG(group), /valid) THEN $
         WIDGET_CONTROL, group, sensititve=0
   ENDIF
   XMANAGER, 'mk_soho', base, group=group
   IF N_ELEMENTS(group) NE 0 THEN IF WIDGET_INFO(LONG(group), /valid) THEN $
      WIDGET_CONTROL, group, sensititve=1

   IF (NOT KEYWORD_SET(group)) THEN XMANAGER

   RETURN
END

;---------------------------------------------------------------------------
; End of 'mk_soho.pro'.
;--------------------------------------------------------------------------
