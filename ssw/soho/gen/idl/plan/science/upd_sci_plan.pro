;+
; Project     : SOHO-CDS
;
; Name        : UPD_SCI_PLAN
;
; Purpose     : Update SCIENCE plan with DETAILS plan 
;
; Category    : planning
;
; Explanation : Use info from DETAILS plan to update SCI_PLAN
;
; Syntax      : upd_sci_plan,iap
;
; Examples    :
;
; Inputs      : DETAILS = DETAILS plan array
;               SCIENCE = SCIENCE array to update
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: Science plan database will be modified
;
; History     : Written 9 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro upd_sci_plan,details,science,err=err

;-- convert to structure

err=''
if get_plan_type(details) ne 0 then begin
 err='Input plan not of type details'
 message,err,/cont
 return
endif

;-- copy common tags

ndetails=n_elements(details)
def_inst_plan,new_sci,type=2,rep=ndetails
type=new_sci.struct_type
det_plan=rep_tag_name(details,'date_obs','start_time')
det_plan=rep_tag_name(det_plan,'date_end','end_time')
copy_struct,det_plan,new_sci
new_sci.struct_type=type

;-- pull out pointing

if which_inst() eq 'S' then pfunct='get_sumer_xy' else pfunct='get_cds_xy'
for i=0,ndetails-1 do begin
 call_procedure,pfunct,details(i),x,y
 xcen=str_deblank(x,'(i8)')
 ycen=str_deblank(y,'(i8)')
 temp_sci=new_sci(i)
 temp_sci.xcen=xcen
 temp_sci.ycen=ycen
 new_sci(i)=mk_plan_xy(temp_sci)
endfor

mk_plan_write,new_sci,science,/retry,/noloop,/nocheck,err=err

return & end


