;---------------------------------------------------------------------------
; Document name: UPDATE_CAMPAIGN.PRO
; Created by:    Liyun Wang, NASA/GSFC, September 9, 1996
;
; Last Modified: Wed Sep 25 15:52:01 1996 (LWANG@sumop1.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO update_campaign, id, error=error
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       UPDATE_CAMPAIGN
;
; PURPOSE:
;       Read the ASCII campaign file and update campaign database
;
; CATEGORY:
;       Planning, database
;
; SYNTAX:
;       update_campaign
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       ID - ID of a campaign to be inserted/updated
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       ERROR - Named variable, error message returned. A null string is
;               returned if no error occurs
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       Requires campaign ASCII database file located in SOHO_EAP directory
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, September 9, 1996, Liyun Wang, NASA/GSFC. Written
;       Version 2, September 19, 1996, Liyun Wang, NASA/GSFC
;          Modified such that an empty CMP_END will cause CMP_END to
;             be set to 2005/12/31
;       Version 3, 23-Sep-1996, William Thompson, GSFC
;          Changed COMMENTS to COMMENT.  Added CMP_TYPE to CDS structure.
;       Version 4, September 23, 1996, Liyun Wang, NASA/GSFC
;          Added optional input parameter: ID
;       Version 5, September 24, 1996, Liyun Wang, NASA/GSFC
;          Made campaign structure for SUMER similar to that for CDS
;          Filled multiple institutes fields for CDS
;          Taken out TEST keyword
;       Version 6, Sept 24 1996, Zarro, ARC
;          Made CDS campaign structure compatible with GET_CAMPAIGN
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 2

   error = ''

   inst = which_inst()
   IF inst NE 'S' THEN BEGIN
;---------------------------------------------------------------------------
;     check priviledge
;---------------------------------------------------------------------------
      status = CALL_FUNCTION('priv_zdbase', /daily, err=error)
      IF NOT status THEN BEGIN
         MESSAGE, error, /cont
         RETURN
      ENDIF
      s=execute("defsysv,'!priv',3")
   ENDIF


   loc = GETENV('SOHO_EAP')
   IF trim(loc) EQ '' THEN BEGIN
      error = 'Env. variable SOHO_EAP for SOHO Campaign database not defined'
      MESSAGE, error, /cont
      RETURN
   ENDIF
   loc=concat_dir(loc,'campaign',/dir)
   cmp_file = concat_dir(loc, 'soho_campaign.dat')
   clook = loc_file(cmp_file, count=count)
   IF count EQ 0 THEN BEGIN
      error = 'SOHO Campaign database file not found'
      RETURN
   ENDIF

   records = rd_ascii(clook(0))
   nrd = N_ELEMENTS(records)
   delim = '~'

   has_id = N_ELEMENTS(id) NE 0

   MESSAGE, 'Updating...', /cont, /info

   FOR i=0, nrd-1 DO BEGIN
      entry = str2arr(records(i), delim=delim)
      n = N_ELEMENTS(entry)
      IF n LT 9 THEN BEGIN
         MESSAGE, 'Invalid data record: has '+STRTRIM(STRING(n),2)+$
            ' fields; require at least 9 fields.', /cont
      ENDIF ELSE BEGIN
         cmp_no = FIX(entry(0))
         go_on = 1
         IF has_id THEN IF cmp_no NE id THEN go_on = 0
         IF go_on THEN BEGIN
            type = entry(1)
            cmp_name = entry(2)

;---------------------------------------------------------------------------
;           Make campaign description string array with maximum 80 character
;           for each element, trim the trailing blanks
;---------------------------------------------------------------------------
            cmp_desc = STRTRIM(str2lines(entry(3), length=80))
            IF N_ELEMENTS(cmp_desc) GT 5 THEN cmp_desc = cmp_desc(0:4)

            observer = entry(4)
            comment = entry(5)
            institutes = entry(6)

            IF entry(7) EQ '' THEN $
               date_obs = 0.d0 $
            ELSE BEGIN
               err = ''
               date_obs = utc2tai(entry(7), err=err)
               IF err NE '' THEN BEGIN
                  date_obs = 0.d0
                  MESSAGE, 'Campaign '+entry(0)+': Invalid DATE_OBS; '+$
                     'set to 0.0 TAI', /cont
               ENDIF
            ENDELSE

            IF entry(8) EQ '' THEN $
               date_end = utc2tai('2005/12/31') $
            ELSE BEGIN
               err = ''
               date_end = utc2tai(entry(8), err=err)
               IF err NE '' THEN BEGIN
                  date_end = utc2tai('2005/12/31')
                  MESSAGE, 'Campaign '+entry(0)+': Invalid DATE_END; '+$
                     'set to 2005/12/31', /cont
               ENDIF
            ENDELSE

            IF inst EQ 'S' THEN $
               inst_stc = trim(institutes) $
            ELSE BEGIN
;---------------------------------------------------------------------------
;              CDS case
;---------------------------------------------------------------------------
               inst_stc = {CDS_INSTITUTE, institut:'', observer:''}
               a = str2arr(institutes, delim=';')
               nc = N_ELEMENTS(a)
               IF nc GT 1 THEN inst_stc = REPLICATE(inst_stc, nc)

               FOR k=0, nc-1 DO BEGIN
                  b = str2arr(a(k), delim=':')
                  IF N_ELEMENTS(b) EQ 2 THEN BEGIN
                     inst_stc(k).institut = trim(b(0))
                     inst_stc(k).observer = trim(b(1))
                  ENDIF ELSE $
                     inst_stc(k).institut = a(k)
               ENDFOR
            ENDELSE
            cmp_stc = {cmp_no:cmp_no, cmp_name:cmp_name, $
                       cmp_type:type, cmp_desc:cmp_desc, $
                       date_obs:date_obs, date_end:date_end, $
                       observer:observer, institutes:inst_stc, $
                       comment:comment}
            cmp_str = STRTRIM(cmp_stc.cmp_no, 2)
            err = ''
            get_campaign, cmp_stc.cmp_no, dd, err=err
            IF err EQ '' THEN BEGIN
               IF NOT tag_exist(dd, 'COMMENT') THEN $
                  dd = add_tag(dd, '', 'COMMENT')
               IF inst NE 'S' THEN BEGIN
                  cmp_no = dd.cmp_no
                  dd = rem_tag(dd, 'CMP_NO')
                  dd = add_tag(dd, cmp_no, 'CMP_NO', index=-1)
               ENDIF
               IF NOT match_struct(dd, cmp_stc) THEN BEGIN
                  err = ''
                  MESSAGE, 'Replacing campaign #'+cmp_str+'...', /cont
                  IF NOT mod_campaign(cmp_stc, err=err) THEN $
                     MESSAGE, cmp_str+': '+err, /cont
               ENDIF
            ENDIF ELSE BEGIN
               err = ''
               MESSAGE, 'Adding campaign #'+cmp_str+'...', /cont
               IF NOT add_campaign(cmp_stc, err=err) THEN $
                  MESSAGE, cmp_str+': '+err, /cont
            ENDELSE
            IF has_id THEN IF cmp_no EQ id THEN GOTO, finished
         ENDIF
      ENDELSE
   ENDFOR

finished:
   MESSAGE, 'All done.', /cont, /info
   RETURN
END

;---------------------------------------------------------------------------
; End of 'UPDATE_CAMPAIGN.PRO'.
;---------------------------------------------------------------------------
