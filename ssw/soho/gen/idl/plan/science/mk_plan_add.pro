;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_ADD
;
; Purpose     :	Add PLAN entry to PLANS array
;
; Explanation :
;
; Use         :	MK_PLAN_ADD,PLAN,PLANS
;
; Inputs      :
;               PLAN = plan structure to add
;               PLANS = structure array of plan entries
;
; Opt. Inputs : None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    : INSERT = set to insert instead of ADD
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	PLANS will be modified
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 9 January 1995.
;-

pro mk_plan_add,plan,plans,err=err,insert=insert,tgap=tgap,_extra=extra

err=''
if n_elements(tgap) eq 0 then tgap=1.d-2

if not mk_plan_priv(plan,err=err) then return

;-- check for SCIENCE objective

if strtrim(plan.sci_obj,2) eq '' then begin
 err='You must enter a Science Objective'
 return
endif

;-- check for valid GIS Setup ID

type=get_plan_type(plan)
cds=which_inst() eq 'C'
if cds and (type ne 2) then begin
 call_procedure, 'get_study_par', plan=plan, gis=use_gis,err=err
 if err eq '' then begin
  if use_gis then begin
   message,'validating GIS setup ID...',/cont
   err=''
   call_procedure, 'get_gset',plan.gset_id,temp,err=err
   if err ne '' then begin
    err=err+'. Please re-select GSET ID'
    return
   endif
  endif
 endif
endif

if not exist(plans) then begin
 plans=plan & return
endif

;-- check if already added

index=mk_plan_where(plan,plans,status=in_db)
same=0
if in_db then same=mk_plan_comp(plan,plans(index))

if not same then begin

 if keyword_set(insert) then begin 

  if in_db then begin

;-- split underlying plan in two

   itime=get_plan_itime(plan)
   type=get_plan_type(plan)
   old_plan=plans(index)
   plan1=old_plan
   plan1.(itime+1)=plan.(itime)-tgap
   plan3=old_plan
   plan3.(itime)=plan.(itime+1)+tgap
   mk_plan_rem,old_plan,plans          ;-- remove underlying plan

;-- add new plan + old parts

   if (plan1.(itime+1)-plan1.(itime)) gt tgap then plans=concat_struct(plans,plan1)  

;-- if DETAILS, then ensure that inserted plan is TIME-TAGGED and second part
;    of underlying PLAN follows as UNTIME-TAGGED

   if type eq 0 then begin
    plan.time_tagged=1
    plan3.time_tagged=0 
   endif

   if (plan.(itime+1)-plan.(itime)) gt tgap then plans=concat_struct(plans,plan)
   if (plan3.(itime+1)-plan3.(itime)) gt tgap then plans=concat_struct(plans,plan3)
  endif
 endif
 if not exist(plans) then plans=plan else $
  plans=concat_struct(temporary(plans),plan,err=err) 
 plans=mk_plan_sort(temporary(plans),/str)
endif else err='Current entry already added' 

return & end



