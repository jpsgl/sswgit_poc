;+
; Project     : SOHO - CDS
;
; Name        : PLOT_CDS_POINT
;
; Purpose     : Plot CDS pointings on latest EIT image
;
; Category    : planning
;
; Explanation : Latest EIT image read from SUMMARY_DATA or PRIVATE_DATA
;
; Syntax      : IDL> plot_cds_point,tstart,tend
;
; Inputs      : TSTART = plot start time [def=current date]
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Keywords    : ASRUN = plot actual pointing based on CATALOG
;               FILE  = FITS file for image [if EIT not wanted]
;               MAP   = map image [if FILE or EIT not wanted]
;               WAVE  = EIT wavelength 304/195/284/171 [def=304]
;               QUIET = switch off message outputs
;               EXCLUDE = study acronyms to exclude, e.g [SYNOP, FULLCCD]
;               DEF_EXCLUDE = exclude default studies (engineering, SYNOP)
;               NOENG = exclude engineering studies 
;               GIF = GIF output filename [def= cds_point_datecode.gif]
;               GSIZE = [nx,ny] dimensions of GIF image [def=512,512]
;               BACK = # of days backward to check for EIT
;               ROTATE = solar rotate pointings to image time
;               NAR  = oplot NOAA AR labels
;               LAST = reuse last base image
;               LIMB = set to plot a limb
;               GRID = grid size in degrees
;               NOIMG = set to not plot image (just limb)
;               CHARSIZE = size of NOAA AR labels [def=1.5]
;
; History     : Written, 20-May-1998,  D.M. Zarro.
;             : Modified, 30-Nov-1999, Zarro (SM&A/GSFC) - add /LIMB
;             : Added JPEG/PNG support, 24 Oct 2000, Zarro (EIT/GSFC)
;             : Added CHMOD g+w to output graphics files, 20 June 2001, 
;                 Zarro (EIT/GSFC)
;             : Added CHARSIZE keyword, 8 Dec 2001, Zarro (L3-Com/GSFC)
;             : Changed EIT from half to full res images, 15 Mar 2002, Zarro (L3-Com/GSFC
;             : Modified to search EIT QL archive, 22 Aug 2006, Zarro (ADNET/GSFC)
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro plot_cds_point,tstart,tend,gsize=gsize,back=back,log=log,$
     file=file,gif=gif,map=map,wave=wave,path=path,reuse=reuse,$
     nar=nar,quiet=quiet,err=err,rotate=rotate,last=last,_extra=extra,$
     limb=limb,grid=grid,noimg=noimg,jpeg=jpeg,png=png,charsize=charsize,verbose=verbose



common plot_cds_point,last_map,last_log,last_header

verbose=keyword_set(verbose)
quiet=1-verbose

limb_only=keyword_set(noimg)
         
;-- check time inputs

t1=get_def_times(tstart,tend,dend=t2,/round)
if is_number(back) then tback=back else tback=3
t1_plans=t1
t1=t2-tback*24.*3600.

;-- read CDS plans

tplans=rd_cds_point(t1_plans,t2,count=pcount,time=rtime,rotate=rotate,_extra=extra)

;-- make GIF/JPEG/PNG file?

do_gif=0 & do_jpeg=0 & do_png=0

;if keyword_set(gif) and idl_release(lower=5.4,/inc) then begin
; message,'GIF no longer supported for this IDL release, using PNG instead' ,/cont
; png=1 & gif=0
;endif

dcode=date_code(t1)
if keyword_set(gif) then begin
 if have_proc('write_gif') then begin
  if datatype(gif) eq 'STR' then gfile=gif else $
  gfile=concat_dir(curdir(),'cds_point_'+dcode+'.gif')
 endif else message,'"write_gif" not supported on this system',/cont
endif

if keyword_set(jpeg) then begin
 if datatype(jpeg) eq 'STR' then jfile=jpeg else $
  jfile=concat_dir(curdir(),'cds_point_'+dcode+'.jpeg')
endif

if keyword_set(png) then begin
 if have_proc('write_png') then begin
  if datatype(png) eq 'STR' then pfile=png else $
   pfile=concat_dir(curdir(),'cds_point_'+dcode+'.png')
 endif else message,'"write_png" not supported on this system',/cont
endif

if exist(gfile) then begin
 break_file,gfile,dsk,dir,name
 outdir=trim(dsk+dir)
 if outdir eq '' then outdir=curdir()
 do_gif=test_dir(outdir)
endif

if exist(jfile) then begin
 break_file,jfile,dsk,dir,name
 outdir=trim(dsk+dir)
 if outdir eq '' then outdir=curdir()
 do_jpeg=test_dir(outdir)
endif

if exist(pfile) then begin
 break_file,pfile,dsk,dir,name
 outdir=trim(dsk+dir)
 if outdir eq '' then outdir=curdir()
 do_png=test_dir(outdir)
endif

;-- if making GIF file use a Z-buffer

dsave=!d.name & psave=!p.color
xsize=512 & ysize=512
ncolors=!d.table_size
if not exist(gsize) then zsize=[xsize,ysize] else $
 zsize=[gsize(0),gsize(n_elements(gsize)-1)]

if do_gif or do_jpeg or do_png then begin
 set_plot,'z',/copy
 device,/close,set_resolution=zsize,set_colors=ncolors
 !p.color=ncolors-1
endif
         
;-- check for FITS file input

if not limb_only then begin

 do_log=1b
 reuse=keyword_set(reuse) or keyword_set(last)
 if keyword_set(reuse) and valid_map(last_map) then begin
  map=last_map
  if is_string(last_header) then header=last_header
  if exist(last_log) then do_log=last_log
 endif
 if exist(log) then do_log=(0b> log < 1b)

 if not valid_map(map) then begin
  if datatype(file) eq 'STR' then fits2map,file,map,err=err
 endif

;-- if FITS file or map structure not input, check EIT archive
;   latest EIT files.
;   If no files found, try MDI, else just plot limb

 err='' & files='' & type=''

 if not valid_map(map) then begin
  t1=anytim2utc(t1,/vms)
  t2=anytim2utc(t2,/vms)
  if verbose then message,'searching data between: '+t1+' -- '+t2,/cont
  if exist(wave) then begin
   files=eit_files(t1,t2,wave=wave,/lz)
   if is_blank(files) then files=eit_files(t1,t2,wave=wave,/quick)
  endif
  if is_blank(files) then begin
   if verbose then message,'searching for any EIT wavelength',/cont
   files=eit_files(t1,t2,/lz)
   if is_blank(files) then files=eit_files(t1,t2,/quick)
  endif

  if is_string(files) then begin
   obj=obj_new('eit')
   file=max(files)
   obj->read,file
   map=obj->get(/map)
   header=obj->get(/header)
   do_log=1b
   obj_destroy,obj
  endif
 endif

;-- check for bad data

 if valid_map(map) then begin
  dmax=max(map.data,min=dmin)
  if dmin eq dmax then begin
   if verbose then message,'invalid EIT image',/cont
   delvarx,map
  endif
 endif

 if valid_map(map) then begin
  last_map=map 
  last_log=do_log
  last_header=header
  if is_string(header) then load_eit_color,header
 endif

endif

;-- try MDI as last ditch attempt

if not valid_map(map) then begin
 do_log=0b
 if verbose then message,'no matching EIT files found, trying MDI',/cont
 files=mdi_latest() 
 if is_string(files) then begin
  times=fid2time(files,/tai)
  chk=where_times(times,tstart=anytim2tai(t1),tend=anytim2tai(t2),count=count)
  if count gt 0 then begin
   if verbose then message,'found matching MDI',/cont
   mfile=files[chk[count-1]]
   mdi=obj_new('mdi')
   mdi->read,mfile
   map=mdi->get(/map)
   obj_destroy,mdi
   if stregex(map.id,'intensitygram',/bool,/fold) then loadct,1 else loadct,0
  endif
 endif
endif

if not valid_map(map) then limb_only=1b else rtime=map.time
if limb_only then begin
 rotate=0b
 rtime=t1
endif

                               
;-- plot limb if requested or input map is not valid

if not valid_map(map) then limb_only=1b
                            
if limb_only then begin
 plot_helio,t1,grid=grid
 smart_window,!d.window,/set
endif else begin
 plot_map,map,xsize=zsize(0),ysize=zsize(1),log=do_log,/square,_extra=extra,grid=grid
endelse

if pcount gt 0 then begin                          
 dprint,!d.window          
 for i=0,pcount-1 do begin
  plot_map,tplans(i),/over,/bord,/quiet,window=!d.window,_extra=extra,lcolor=0
 endfor
 dprint,'% PLOT_CDS_POINT: ',tplans(0).time,' -- ',tplans(pcount-1).time
endif

;-- add Active Regions

do_nar=keyword_set(nar)
if do_nar then begin
 if have_proc('get_nar') then begin
  nar=call_function('get_nar',rtime,quiet=quiet,count=count,/nearest,/unique)
  if count gt 0 then nar=call_function('drot_nar',nar,rtime,count=count) 
  if not exist(charsize) then charsize=1.5
  if count gt 0 then oplot_nar,nar,charsize=charsize
 endif
endif

if do_gif or do_jpeg or do_png then begin
 temp=tvrd()
 device,/close
 tvlct,rs,gs,bs,/get
 if do_gif then begin
  if verbose then message,'writing GIF file to -> '+gfile,/cont
  write_gif,gfile,temp,rs,gs,bs
  espawn,'chmod g+w '+gfile
 endif

 if do_png then begin
  if verbose then message,'writing PNG file to -> '+pfile,/cont
  if idl_release(upper=5.3,/inc) then $
   write_png,pfile,rotate(temp,7),rs,gs,bs else $
    write_png,pfile,temp,rs,gs,bs
  espawn,'chmod g+w '+pfile
 endif

 if do_jpeg then begin
  temp=mk_24bit(temp,rs,gs,bs)
  if verbose then message,'writing JPEG file to -> '+jfile,/cont
  write_jpeg,jfile,temp,/true
  espawn,'chmod g+w '+jfile
 endif

 set_plot,dsave
 !p.color=psave
endif

return & end
