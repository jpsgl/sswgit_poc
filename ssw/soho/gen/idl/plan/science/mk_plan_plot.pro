;+
; Project     : SOHO - CDS
;
; Name        : MK_PLAN_PLOT
;
; Purpose     : Plot timeline for arbitrary plan type and instruments
;
; Category    : planning
;
; Syntax      : IDL>mk_plan_plot,tstart,tend
;
; Inputs      : TSTART = plot start time
;
; Opt. Inputs : TEND = plot end time [def = 24 hour window]
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : TYPE= plan type = 0/1/2/3/4 for DETAILS/FLAG/SCIENCE/ALT/SOHO
;                     (def = [0,2])
;               INST = SOHO instrument [def = 'CDS']
;               GIF  = make GIF file
;               FILE = GIF file name [def = mk_plan_plot.gif]
;               SIZE = plot size = [1000,250]
;               RES  = include spacecraft resources (DSN, SVM)
;               JEPG = make JPEG file
;
; History     : Version 1,  1-Aug-1997,  D.M. Zarro.  Written
;               Vers. 2, 28-Aug-01, Zarro (EITI/GSFC) - added JPEG support
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro mk_plan_plot,tstart,tend,type=type,inst=inst,res=res,size=gsize,$
     file=file,err=err,gif=gif,font=font,jpeg=jpeg

common mk_plan_plot,ws


gif=keyword_set(gif)
jpeg=keyword_set(jpeg)

if gif and jpeg then begin
 message,'use /GIF or /JPEG',/cont
 return
endif

;-- check inputs

err=''
t1=anytim2utc(tstart,err=err)
if err ne '' then begin
 message,'Syntax -> mk_plan_plot,tstart,tend',/cont
 return
endif

t2=anytim2utc(tend,err=err)
if err ne '' then begin
 t2=t1 & t2.mjd=t2.mjd+1
endif

@mk_plan_shared
startdis=anytim2tai(t1) & stopdis=anytim2tai(t2)
if stopdis le startdis then begin
 stopdis=startdis+24.d*3600.
 t2=stopdis
endif

;-- load plans

if not exist(type) then type=[0,2]
if not exist(inst) then inst='CDS'

for i=0,n_elements(type)-1 do begin
 for j=0,n_elements(inst)-1 do begin
  rd_plan,plan,t1,t2,type=type(i),inst=inst(j),/force,/nopoint,/quiet,$
         nobs=nobs
  mk_plan_load,type(i),plan,inst=inst(j)
  item=get_soho_inst(strmid(inst(j),0,1))
  if (type(i) lt 4) then begin
   def=strmid(get_plan_def(type(i)),0,3)
   if ((which_inst() eq 'C') and (item eq 'CDS')) or $
      ((which_inst() eq 'S') and (item eq 'SUMER')) then item=item+'-'+def
  endif
  if not exist(req_items) then req_items=item else req_items=[item,req_items]
 endfor
endfor

;-- load resources

if keyword_set(res) then begin
 rd_resource,dsn,t1,t2,/force,/dsn
 rd_resource,svm,t1,t2,/force,/svm
 rd_resource, trcr,t1,t2,/force,/_rcr
 if exist(req_items) then req_items=['Command',req_items] else req_items='Command'
endif

if not exist(req_items) then begin
 err='No plan entries to plot'
 message,err,/cont
endif
dprint,'% Plotting: ',req_items

;-- define window

xsize=1000 & ysize=250
if exist(gsize) then begin
 xsize=gsize(0)
 if n_elements(gsize) gt 1 then ysize=gsize(1)
endif

sav_dev=!d.name & sav_color=!p.color
sav_font=!p.font
ncolors=!d.table_size

zsize=[xsize,ysize]
if exist(font) then !p.font=font

if gif or jpeg then begin
 set_plot,'z',/copy
 device,/close,set_resolution=zsize,set_colors=ncolors
 !p.color=ncolors-1
endif else begin
 if !d.name eq 'X' then get_xwin,ws,xsize=xsize,ysize=ysize,/retain
endelse

;-- now plot

set_line_color
plot_splan,t1,t2,plan_items=req_items,color=get_inst_color(/stc)

;-- make GIF/JPEG file

if gif and ( (not have_proc('write_gif')) or since_version('5.4')) then begin
 message,'GIF format not supported - using JPEG',/cont
 gif=0 & jpeg=1
endif

if gif or jpeg then begin
 temp=tvrd()
 device,/close
 tvlct,rs,gs,bs,/get
 if gif then ext='.gif' else ext='.jpeg'
 if datatype(file) ne 'STR' then begin
  file=mk_temp_file('mk_plan_plot'+ext)
  message,'writing file to -> '+file,/cont
 endif
 if gif then write_gif,file,temp,rs,gs,bs else begin
  temp=mk_24bit(temp,rs,gs,bs)
  write_jpeg,file,temp,/true
 endelse
 device,/close
 set_plot,sav_dev
endif

;-- set things back

if !d.name eq 'X' then begin
 if is_wopen(ws) then wshow,ws
endif

!p.font=sav_font
!p.color=sav_color

return & end


