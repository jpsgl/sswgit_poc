;+
; Project     : SOHO-CDS
;
; Name        : UPDATE_SOHO_TARGETS
;
; Purpose     : Driver to update SOHO target pages
;
; Category    : planning
;
; Explanation : calls MK_SOHO_TARGET
;
; Syntax      : update_soho_target
;
; Opt. Inputs : DATE = date to retrieve
;
; Keywords    : MAIL = send some mail
;
; Restrictions: Unix only
;
; History     : Written 14 May 1998 D. Zarro, SAC/GSFC
;               Modified, April 14, 2007, Zarro (ADNET)
;                - forced reading of SOHO/SSW database
;
; Contact     : dzarro@solar.stanford.edu
;-

pro update_soho_targets,date,mailto=mailto,noaa=noaa

synop_data=trim(getenv('SYNOP_DATA'))
if synop_data eq '' then begin
 message,'Undefined $SYNOP_DATA',/cont
 return
endif

if (1-write_dir(synop_data)) then begin
 message,'No write access to '+synop_data
 return
endif

message,'Updating SOHO WWW target pages...',/cont

direc=concat_dir(synop_data,'.targets')
yesterday='yesterday.html'
today='today.html'
tomorrow='tomorrow.html'
dayafterto='dayafterto.html'

err=''
tt=anytim2utc(date,err=err,/date)
if err ne '' then get_utc,tt,/date
tt.time=0
tt.mjd=tt.mjd-1
mk_soho_targets,tt,yesterday,dir=direc,noaa=noaa,next=today,err=err
if is_string(err) then return
mk_plan_form,tt,dir=direc,/force,/gif,/dtype,file=yesterday

tt.mjd=tt.mjd+1
mk_soho_targets,tt,today,dir=direc,noaa=noaa,next=tomorrow,prev=yesterday
mk_plan_form,tt,dir=direc,/force,/gif,/dtype,file=today

tt.mjd=tt.mjd+1
mk_soho_targets,tt,tomorrow,dir=direc,next=dayafterto,prev=today
mk_plan_form,tt,dir=direc,/force,/gif,/dtype,file=tomorrow

tt.mjd=tt.mjd+1
mk_soho_targets,tt,dayafterto,dir=direc,prev=tomorrow
mk_plan_form,tt,dir=direc,/force,/gif,/dtype,file=dayafterto

if exist(mailto) then begin
 mess='UPDATE_SOHO_TARGETS completed on: '+anytim2utc(!stime,/vms)
 if datatype(mailto) eq 'STR' then recip=mailto else recip=get_user_id()
 send_mail,array=mess,address=recip
endif

return & end

