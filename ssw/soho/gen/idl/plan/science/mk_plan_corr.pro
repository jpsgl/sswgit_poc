;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_CORR 
;
; Purpose     :	correct plan entry so that PLAN.N_POINTINGS matches number of elements of PLAN.POINTINGS
;
; Explanation :	
;
; Use         :	PLAN=MK_PLAN_CORR(PLAN)
;
; Inputs      :	PLAN = input plan entry
;
; Opt. Inputs :	None.
;
; Outputs     :	CPLAN = corrected plan entry 
;
; Opt. Outputs: None.
;
; Keywords    :	UTC = convert time fields to UTC
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC),  10 March 1995
;-


function mk_plan_corr,plan,utc=utc,err=err

err=''

if n_elements(plan) ne 0 then cplan=plan else cplan=-1
if n_elements(plan) ne 1 then begin
 err='SYNTAX --> CPLAN=MK_PLAN_CORR(PLAN)'
 message,err,/cont & return,cplan
endif

if tag_exist(plan,'POINTINGS') then begin
 old_point=cplan.pointings
 npoint=(cplan.n_pointings > 1)
 cpoint=n_elements(old_point)
 if npoint lt cpoint then begin
  point=cplan.pointings
  cplan=rep_tag_value(cplan,point(0:npoint-1),'POINTINGS')
 endif else if (npoint gt cpoint) then cplan.n_pointings=cpoint
endif

if keyword_set(utc) then cplan=mk_plan_conv(cplan)

return,cplan & end
