;+
; Project     : SOHO-CDS
;
; Name        : FIX_IAP
;
; Purpose     : Update KAP file with new IAP info
;
; Category    : planning
;
; Explanation :
;
; Syntax      : fix_kap,iap
;
; Examples    :
;
; Inputs      : IAP = IAP filenames
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : OUTDIR = output directory [def = current]
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 9 June 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


pro fix_kap,iap,outdir=outdir

if datatype(iap) ne 'STR' then begin
 message,'syntax --> fix_iap,iap, [outdir=outdir]',/cont
 return
endif

;-- output dir

if datatype(outdir) eq 'STR' then begin
 out_dir=outdir
 if not chk_dir(out_dir) then begin
  message,'invalid output directory: '+outdir,/cont
  return
 endif
 if not test_open(out_dir) then begin
  message,'no write access to output directory: '+outdir,/cont
  return
 endif
endif else begin
 cd,curr=curr
 if test_open(curr,/write) then out_dir=curr else out_dir=getenv('HOME')
endelse

;-- determine new IAP start time and instrument

f=loc_file(iap(0),count=count,/verb)
if count eq 0 then return

iap_arr=rd_ascii(f)
startime=get_keyword(iap_arr,'startime')
if startime(0) eq '' then begin
 message,'No STARTIME keyword in IAP file',/cont
 return
endif
message,'IAP STARTIME  - '+startime(0),/info

inst=get_keyword(iap_arr,'instrume')
if inst(0) eq '' then begin
 message,'No INSTRUME keyword in IAP file',/cont
 return
endif

iorder = uniq([inst],sort([inst]))
if n_elements(iorder) gt 1 then begin
 message,'Invalid IAP file -- contains more than one instrument',/cont
 return
endif

iap_inst=strupcase(inst(0))
message,'IAP instrument - '+iap_inst,/info

;-- strip off IAP header

iap_list=str_space(iap_arr)
sz=size(iap_list)
if sz(0) eq 1 then iap_list=reform(iap_list,sz(1),1) else $
 iap_list=iap_list(*,1:*)

sz=size(iap_list)
if sz(0) eq 1 then iap_list=reform(iap_list,sz(1),1)

;-- read corresponding KAP file 

kap=get_latest_kap(startime(0),err=err)
if err ne '' then begin
 message,err,/cont
 return
endif

message,'fixing '+kap+'...',/info
kap_arr=rd_ascii(kap)
kap_list=str_space(kap_arr)

;-- strip out old IAP instrument

sz=size(kap_list)
if sz(0) eq 1 then kap_list=reform(kap_list,sz(1),1)
sz=size(kap_list)
np=sz(2)

if np eq 1 then begin
 message,'Invalid KAP file - no entries found',/cont
 return
endif

;-- remove old INSTRUMENT entries

for i=0,np-1 do begin
 chunk=kap_list(*,i)
 temp=strupcase(strcompress(chunk,/rem))
 old_ipos=strpos(temp,'INSTRUME='+iap_inst)
 found_ipos=where(old_ipos gt -1,icount)
 if icount eq 0 then boost_array,new_kap_list,chunk
endfor

;-- append new IAP

sz=size(iap_list)
for i=0,sz(2)-1 do boost_array,new_kap_list,iap_list(*,i)

;-- now sort on times

sz=size(new_kap_list)
times=strarr(sz(2))
for i=1,sz(2)-1 do begin
 startime=get_keyword(new_kap_list(*,i),'startime')
 if startime(0) ne '' then times(i)=startime else begin
  earliest=get_keyword(new_kap_list(*,i),'earliest')
  if earliest(0) ne '' then times(i)=earliest
 endelse
endfor
stimes=sort([times])
new_kap_list=new_kap_list(*,stimes)

;-- update creation date

date_cre=get_keyword(new_kap_list(*,0),'date_cre',index=index)
if date_cre(0) ne '' then begin
 get_utc,new_date,/ecs,/trun
 date_line=new_kap_list(index(0),0)
 new_kap_list(index(0),0)=str_replace(date_line,date_cre(0),new_date)
endif

;-- now write new file

break_file,kap,dsk,dir,new_kap_file,ext
len=strlen(new_kap_file)
base_name=strmid(new_kap_file,0,11)
vers=fix(strmid(new_kap_file,12,len))+1
new_name=base_name+string(vers,'(i3.3)')
temp=concat_dir(out_dir,new_name+ext)

message,'writing new --> '+temp,/info

openw,lun,temp,/get_lun
for i=0,sz(2)-1 do begin
 chunk=new_kap_list(*,i)
 for k=0,sz(1)-1 do begin
  piece=chunk(k)
  if trim(piece) ne '' then printf,lun,piece
 endfor
 printf,lun,''
endfor

close,lun & free_lun,lun

return & end




