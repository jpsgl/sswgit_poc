;+
; Project     :	SOHO - CDS/SUMER
;
; Name        :	MK_PLAN_REM
;
; Purpose     :	Remove PLAN entry from given list of PLANS
;
; Explanation :
;
; Use         :	MK_DETAIL_REM, PLAN, PLANS
;
; Inputs      :
;               PLAN = plan structure
;               PLANS = structure array of plan entries
;
; Opt. Inputs : None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    : ERR  = Any error message of operation
;
; Calls       :	EXIST, MK_PLAN_CHK, DELVARX
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	PLANS will be modified.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, May 18, 1995
;-

   pro mk_plan_rem, plan, plans, err=err

   on_error,1

   err=''

   if not mk_plan_priv(plan,err=err) then return

   if exist(plans) then begin

;-- check if in plans

    remove=mk_plan_where(plan, plans, status=in_db)
      
    if not in_db then begin
     err='plan entry not present'
     message,err,/cont
     return
    endif 

    same=mk_plan_comp(plan,plans(remove))
    if not same then begin
     err='Could not find plan to remove'
     message,err,/cont
     return
    endif

;-- wipe plans clean if only one element to delete
      
    np=n_elements(plans)
    if np eq 1 then begin 
     delvarx, plans
     return
    endif

    array=indgen(np)
    keep=where(array ne remove,cnt)
    if cnt eq 0 then begin
     err='No entries to delete'
     message,err,/cont
    endif else plans=plans(keep)
   endif

   return & end

