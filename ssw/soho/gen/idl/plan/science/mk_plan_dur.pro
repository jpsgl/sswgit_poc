;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_DUR
;
; Purpose     :	compute study duration based on plan details
;
; Explanation :	
;
; Use         :	DUR=MK_PLAN_DUR(PLAN)
;
; Inputs      :	PLAN = CDS or SUMER detailed plan entry
;               
;
; Opt. Inputs : None
;
; Outputs     :	DUR = duration of study + extra from variable rasters
;
; Opt. Outputs:	None.
;
; Keywords    : POINT = 1 to switch on pointing calculation (CDS only)
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 9 January 1995.
;-


function mk_plan_dur,plan,point=point,quiet=quiet

case which_inst() of
 'C': return,call_function('get_cds_dur',plan,point=point,quiet=quiet)
 'S': return,call_function('mk_sumer_dur',plan)
 else: begin
  message,'unsupported instrument',/cont
  return,0
 end
endcase

end

