;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_SBASE
;
; Purpose     :	makes a widget interface to CDS or SUMER Science Plan
;
; Explanation :	
;
; Use         :	MK_PLAN_SBASE,PARENT,SBASE
;
; Inputs      :	PARENT = parent widget upon which to construct widgets
;
; Opt. Inputs :	None.
;
; Outputs     :	SBASE  = created child widget 
;
; Opt. Outputs:	None.
;
; Keywords    :	MAP = 1/0 for visible/invisible
;               LFONT = font for label widgets
;               BFONT = font for button widgets
;               CW    = CW use compound widgets for TEXT widgets
; Calls       :	
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC),  22 January 1995
;-

   pro mk_plan_sbase, parent, sbase, map=map, lfont=lfont, bfont=bfont, cw=cw

   if n_elements(map) eq 0 then map = 1
   mk_dfont,lfont=lfont,bfont=bfont

   if keyword_set(cw) then wfunct='cw_text' else wfunct='widget_text'

   sbase = WIDGET_BASE(parent,/column, map=map)

   row=widget_base(sbase,/row)

;---------------------------------------------------------------------------
;  Sci_obj sci_spec, disturbances, and notes
;---------------------------------------------------------------------------

   col1 = WIDGET_BASE(row, /column,/frame)

   row1=WIDGET_BASE(col1,/row)

   label = WIDGET_BUTTON(row1, value=' SCI_OBJ:', font=bfont,uvalue='bsci_obj',/no_rel)
   sci_obj = WIDGET_TEXT(row1, value='', xsize=40,font=lfont,/edit,/all,uvalue='sci_obj')

   row2=WIDGET_BASE(col1,/row)

   label = WIDGET_BUTTON(row2, value='SCI_SPEC:', font=bfont, uvalue='bsci_spec',/no_rel)
   sci_spec = WIDGET_TEXT(row2, value='', xsize=40,font=lfont,/edit,uvalue='sci_spec',/all)

   row3=WIDGET_BASE(col1,/row)

   label = WIDGET_BUTTON(row3, value=' DISTURB:',font=bfont, $
                         uvalue='bdisturbances',/no_rel)
   disturbances = WIDGET_TEXT(row3, value='', xsize=40, font=lfont,$
                              /edit,/all,uvalue='disturbances')

   row4=WIDGET_BASE(col1,/row)

   label = WIDGET_BUTTON(row4, value='   NOTES:', font=bfont,uvalue='bnotes',/no_rel)
   notes = WIDGET_TEXT(row4, value='', xsize=40,font=lfont,/edit,/all,uvalue='notes')

;---------------------------------------------------------------------------
;  XCEN and YCEN
;---------------------------------------------------------------------------

   row5=WIDGET_BASE(col1,/row)
   lab=widget_button(row5,     value='    XCEN:',font=bfont,uvalue='bxcen')
   xcen = WIDGET_TEXT(row5, value='', xsize=40,font=lfont,/edit)
   lab2=widget_label(row5,value='Arcsecs (+W/-E)',font=lfont)

   row6=WIDGET_BASE(col1,/row)
   lab=widget_button(row6,     value='    YCEN:',font=bfont,uvalue='bycen')
   ycen = WIDGET_TEXT(row6, value='', xsize=40,font=lfont,/edit)
   lab2=widget_label(row6,value='Arcsecs (+N/-S)',font=lfont)
 
;---------------------------------------------------------------------------
;  Object, obj_id, prog_id, and cmp_no
;---------------------------------------------------------------------------

   col2 = WIDGET_BASE(row, /column,/frame)

   row1=WIDGET_BASE(col2,/row)
   objectb = WIDGET_BUTTON(row1, value=' OBJECT:', /no_rele, $
                           uvalue='object', font=bfont)
   object = CW_TEXT(row1, value='      ', xsize=6, font=lfont)

   row2=WIDGET_BASE(col2,/row)
   label=WIDGET_BUTTON(row2,value=      ' OBJ_ID:',font=bfont,$
                       uvalue='bobj_id', /no_rel)
   obj_id=WIDGET_TEXT(row2,value='',xsize=6,font=lfont,/edit,/all,$
                      uvalue='obj_id')

   row3=WIDGET_BASE(col2,/row)
   progb=WIDGET_BUTTON(row3,value=      'PROG_ID:',uvalue='prgb',$
                       font=bfont,/no_rel)
   prog_id=CW_TEXT(row3,value='',xsize=6,font=lfont,uvalue='prog_id')

   row4=WIDGET_BASE(col2,/row)
   cmpb=WIDGET_BUTTON(row4,value=       ' CMP_NO:',uvalue='cmpb',$
                      font=bfont,/no_rel)
   cmp_no=cw_text(row4,value='',xsize=6,font=lfont,uvalue='cmp_no')


;---------------------------------------------------------------------------
;  Start and stop time, cur_time, and plan duration
;---------------------------------------------------------------------------

   col3 = WIDGET_BASE(row, /column,/frame)

;---------------------------------------------------------------------------
;  Start date/time should not be editable
;---------------------------------------------------------------------------

   row1=WIDGET_BASE(col3,/row)
   start_lab = WIDGET_LABEL(row1, value='START_TIME:', font=lfont)
   date_obs = call_function(wfunct,row1, value='       ',xsize=11,font=lfont,/edit)
   time_obs = call_function(wfunct,row1, value='       ',xsize=12,font=lfont,/edit)

;---------------------------------------------------------------------------
;  Stop time for Science plan can be editable (required REPLACE to take into
;  effect)
;---------------------------------------------------------------------------

   row2=WIDGET_BASE(col3,/row)
   stop_lab = WIDGET_LABEL(row2, value= '  END_TIME:', font=lfont)
   date_end = call_function(wfunct,row2, xsize=11,/edit,uvalue='date_end',font=lfont)
   time_end = call_function(wfunct,row2, xsize=12,/edit,uvalue='time_end',font=lfont)

   row3=WIDGET_BASE(col3,/row)
   cur_lab = WIDGET_LABEL(row3, value=  '  CUR_TIME:', font=lfont)
   date_cur = call_function(wfunct,row3, xsize=11, /edit,font=lfont,uvalue='date_cur')
   time_cur = call_function(wfunct,row3, xsize=12, /edit,font=lfont,uvalue='time_cur')

   pdurb=WIDGET_BASE(col3,/row)
   temp = WIDGET_LABEL(pdurb, value=    '  PLAN DUR:', font=lfont)
   pduration = call_function(wfunct,pdurb, value='', xsize=12,font=lfont,/editable,uvalue='pduration',/all)

   unit_str = strpad(['Seconds', 'Minutes', 'Hours'],8,/after)
   new_vers=float(strmid(!version.release,0,3)) ge 4.
   if new_vers then $
    dur_unitb=call_function('widget_droplist',pdurb,value=unit_str,uvalue='dur_unit',font=bfont) else $
     dur_unitb = cw_bselector2(pdurb, unit_str, uvalue='dur_unit', /no_rel,/return_index,font=bfont)
   
;---------------------------------------------------------------------------
;  Save widget id's in structure
;---------------------------------------------------------------------------
   wids = { prog_id:prog_id, $
            sci_obj:sci_obj, $
            sci_spec:sci_spec, $
            notes:notes, $
            object:object, $
            obj_id:obj_id, $
            cmp_no:cmp_no, $
            disturbances:disturbances, $
            start_lab:start_lab, $
            stop_lab:stop_lab, $
            date_obs:date_obs, $
            date_end:date_end, $
            time_obs:time_obs, $
            time_end:time_end, $
            date_cur:date_cur, $
            time_cur:time_cur, $
            cur_lab:cur_lab, $
            pduration:pduration, $
            dur_unitb:dur_unitb,$
            xcen:xcen,$
            ycen:ycen}

   WIDGET_CONTROL, sbase, set_uvalue=wids

   RETURN
END

