;+
; Project     : SOHO-CDS
;
; Name        : UPDATE_CDS_TARGETS
;
; Purpose     : Driver to update CDS target pages
;
; Category    : planning
;
; Explanation : calls PLOT_CDS_POINT
;
; Opt. Inputs : DATE = date to process
;
; Restrictions: Unix only
;
; History     : Written 14 July 2001 D. Zarro (EITI/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

pro update_cds_targets,date,verbose=verbose

if (1-write_dir('$SYNOP_DATA',out=synop_data)) then begin
 message,'No write access to '+synop_data,/cont
 return
endif

cds_db='/eofcs/data/plan/database'
sdb='/sdb/soho/cds/data/plan/database'
case 1 of
 is_dir(cds_db): zdbase='+'+cds_db
 is_dir(sdb)   : zdbase='+'+sdb
else: begin
 message,'Cannot find CDS databases',/cont
 return
 end
endcase

mklog,'ZDBASE_CDS','+'+cds_db
mklog,'ZDBASE',zdbase

message,'Updating CDS target pages...',/cont

direc=concat_dir(synop_data,'.targets/pointings')

days=['yesterday','today','tomorrow','dayafterto']

files=concat_dir(direc,days)

err=''
tt=anytim2utc(date,err=err,/date)
if err ne '' then get_utc,tt,/date
tt.time=0

tt.mjd=tt.mjd-1
verbose=keyword_set(verbose)

for i=0,n_elements(days)-1 do begin
 if verbose then begin
  message,'processing '+files(i)+' on ',/cont
  message,anytim2utc(tt,/vms,/date),/cont,/noname
 endif
 plot_cds_point,tt,out_file=files(i),/nar,/rotate,charsize=1.2,$
                wave=195,verbose=verbose,format='gif',back=4
 tt.mjd=tt.mjd+1
endfor

return & end

