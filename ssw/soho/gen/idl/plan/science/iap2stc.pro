;+
; Project     : SOHO-CDS
;
; Name        : IAP2STC
;
; Purpose     : convert IAP file (or array) to structure
;
; Category    : planning
;
; Explanation :
;
; Syntax      : iap2stc,iap,stc
;
; Examples    :
;
; Inputs      : IAP = IAP filename (or array read from IAP)
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 9 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro iap2stc,iap,struct          ;-- convert IAP contents to structure

if not exist(iap) then begin
 message,'Syntax: iap2stc,iap,struct',/cont
 return
endif

;-- file or array entered

file_entered=(datatype(iap) eq 'STR') and (n_elements(iap) eq 1)
if file_entered then begin
 chk=loc_file(iap,count=count)
 if count gt 0 then fiap=rd_ascii(iap) else begin
  message,'Cannot locate file: '+iap,/cont
  return
 endelse
endif else fiap=iap

niap=n_elements(fiap)

delvarx,struct
bstc={TYPE:'',STARTIME:'',ENDTIME:'',SCI_OBJ:'',SCI_SPEC:'',INSTRUME:'',XCEN:'',YCEN:'',$
      IXWIDTH:'',IYWIDTH:'',ANGLE:'',OBJECT:'',OBJ_ID:'',CMP_NO:'',PROG_ID:''}
sel_tags=tag_names(bstc)
ntags=n_elements(sel_tags)
for i=0,niap-1 do begin
 spos=(strpos(fiap(i),'PROGRAM_') gt -1) or (strpos(fiap(i),'SCIPLAN_') gt -1)
 if spos then begin
  stc=bstc
  stc.(0)=fiap(i)
  repeat begin
   i=i+1
   if (i lt niap) then begin
    line=fiap(i)
    for k=0,ntags-1 do begin
     stpos=strpos(line,sel_tags(k)) gt -1
     if stpos then begin
      equ=strpos(line,'=')
      stag=strmid(line,equ+1,strlen(line))
      stc.(k)=stag
     endif
    endfor
   endif
   epos=trim(fiap(i)) eq ''
  endrep until (epos or i eq (niap-1))
  if epos then i=i-1
  if trim(stc.cmp_no) eq '' then stc.cmp_no='-1'
  struct=concat_struct(struct,stc)
 endif
endfor

if exist(struct) then struct.instrume=strupcase(trim(struct.instrume))
return & end

