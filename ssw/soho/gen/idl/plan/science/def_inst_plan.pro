;+
; Project     :	SOHO - CDS
;
; Name        :	DEF_INST_PLAN
;
; Purpose     : Define a plan structure
;
; Use         : DEF_INST_PLAN,PLAN
;
; Inputs      : None.
;
; Opt. Inputs : PLAN = plan structure definition
;
; Outputs     : PLAN = structure definition
;
; Opt. Outputs: None.
;
; Keywords    : TYPE = 0/1/2 for DETAILS/FLAG/SCIENCE [def=0]
;               INIT = initialize times of input PLAN [def=yes]
;               INST = instrument type ('C', 'S', for CDS, SUMER, etc)
;               START_TIME = Date/Time in TAI, initial time tag
;               LIST = set to make structure LIST type
;               REP = no to replicate by
;
; Explanation : None.
;
; Calls       : None.
;
; Common      : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Planning
;
; Prev. Hist. : None.
;
; Written     :	Zarro (ARC/GSFC) 29 January 1995
;
; Modification: Liyun Wang, GSFC/ARC, May 17, 1995
;               Added START_TIME keyword
; 
; Version     : Version 1.1, May 17, 1995
;-

pro def_inst_plan,plan,type=type,init=init,inst=inst,list=list,$
             n_pointings=n_pointings,start_time=start_time,rep=rep

on_error,1

IF N_ELEMENTS(start_time) NE 0 THEN BEGIN
   IF DATATYPE(START_TIME,1) NE 'Double' THEN BEGIN
      MESSAGE, 'START_TIME must be double precision', /cont
      start_time = 0.d0
   ENDIF
ENDIF ELSE start_time = 0.d0

if exist(plan) then plan_sav=plan
delvarx,plan

;-- default to CDS_DETAILS

types=['DETAILS','FLAG','SCIENCE','ALT']
if (datatype(inst) eq 'STR') then begin
 inst=strupcase(strmid(strtrim(inst,2),0,1))
 get_instrument,inst,desc
 name=strtrim(desc.name,2)
endif else begin
 inst=which_inst()
 if inst eq 'S' then name='SUMER' else name='CDS'
endelse

if n_elements(type) eq 0 then type=0


case type of 
                                                
 0: begin                                  ;--DETAILS
     case inst of
        'C' : call_procedure, 'def_cds_detail', plan, n_pointings=n_pointings
        'S' : call_procedure, 'def_sumer_detail', plan, n_pointings=n_pointings
        else: begin 
         message, 'DETAILS unsupported for '+name,/cont
        end
     endcase
    end

 1: begin                                  ;--FLAG
     if inst ne 'C' then begin
      message,'FLAG unsupported for '+name,/cont
      return
     endif
     call_procedure, 'def_cds_detail', plan, n_pointings=n_pointings
     plan=rep_tag_name(plan,'date_obs','rcvr_start')
     plan=rep_tag_name(plan,'date_end','rcvr_stop')
     plan=rem_tag(plan,'time_tagged')
     plan=rep_tag_name(plan,'flag_master','repoint')
     plan=rep_tag_value(plan,0b,'repoint')
     plan=rem_tag(plan,'n_repeat_s')
     plan=rem_tag(plan,'get_raw')
     struct_type=name+'-'+types(type)
     plan.struct_type=struct_type
    end

 2: begin                                  ;--SCIENCE
     plan =  {  STRUCT_TYPE:'SOHO-PLAN' ,$
                INSTRUME:inst           ,$
                SCI_OBJ:''              ,$
                SCI_SPEC:''             ,$
                NOTES:''                ,$
                START_TIME: start_time  ,$
                END_TIME: start_time    ,$
                OBJECT: ''              ,$
                OBJ_ID: ''              ,$
                PROG_ID:0               ,$
                CMP_NO: -1              ,$
                XCEN: '0.0'             ,$
                YCEN: '0.0'             ,$
                DISTURBANCES:''         }
 end

 3: begin                                  ;--ALT
     if inst ne 'C' then begin
      message,'ALT unsupported for '+name,/cont
      return
     endif
     call_procedure, 'def_cds_detail', plan, n_pointings=n_pointings
     plan=rep_tag_name(plan,'date_obs','start_time')
     plan=rep_tag_name(plan,'date_end','end_time')
     plan=rem_tag(plan,'time_tagged')
     plan=rem_tag(plan,'flag_master')
     struct_type=name+'-'+types(type)
     plan.struct_type=struct_type
    end

 4: begin
     plan = { STRUCT_TYPE: 'SOHO-DET',$
              INSTRUME: inst,$
              DATE_OBS: start_time,$
              DATE_END: start_time,$
              SCI_OBJ: '',$
              SCI_SPEC: '',$
              OBS_PROG: '',$
              PROG_ID: 0,$
              CMP_NO: -1,$
              OBJECT: '',$
              OBJ_ID: '',$
              XCEN: '',$
              YCEN: '',$
              ANGLE: '',$
              IXWIDTH: '',$
              IYWIDTH: '',$
              DISTURBANCES: '',$
              JITTER_LIMIT: 0}
  end
 else: begin
  message,'unsupported DB type',/contin
  return
 end
endcase

;-- recover original times

if not keyword_set(init) then begin
 if exist(plan_sav) then begin
  type_sav=get_plan_type(plan_sav,inst=inst_sav)
  itime=get_plan_itime(plan_sav)
  if (type eq type_sav) and (inst eq inst_sav) then begin
   plan.(itime)=plan_sav(0).(itime)
   plan.(itime+1)=plan_sav(0).(itime+1)
  endif
 endif
endif

if keyword_set(list) then plan.struct_type=plan.struct_type+'-LIST'

if exist(rep) then plan=replicate(plan,long(rep))

return & end

