;+
; Project     :	SOHO - CDS
;
; Name        :	LIST_TO_DETAIL
;
; Purpose     :	convert LIST structure to DETAILS structure
;
; Explanation :
;
; Use         :	DETAIL=LIST_TO_DETAIL(LIST)
;
; Inputs      :	PLAN = list structure (scalar or array)
;
; Opt. Inputs :	None.
;
; Outputs     :	DETAIL = details structure
;
; Opt. Outputs:	NOBS = number of observations
;
; Keywords    :	SORT = set to sort in time
;
; Calls       :	GET_DETAIL, GET_FLAG
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC),  18 March 1995
;-

function list_to_detail,plan,sorting=sorting,nobs

on_error,1
type=get_plan_type(plan)
if type eq -1 then begin
 if exist(plan) then return,plan else return,type
endif

itime=get_plan_itime(plan)
nobs=n_elements(plan)
if strpos(plan(0).struct_type,'LIST') lt 0 then begin
 message,'input plan not of type LIST',/contin
 return,plan
endif

funct=get_plan_funct(type,/get)

if funct eq '' then return,plan
message,'retrieving pointing...',/cont

inst=which_inst()
if inst eq 'C' then begin
 max_point=max(plan.n_pointings) > 1
 def_inst_plan,base,n_pointings=max_point,inst='C',type=type
 buff=replicate(base,nobs)
endif

for i=0,nobs-1 do begin
 if (type eq 0) and inst eq 'C' then begin
  keep=i lt (nobs-1)
  call_procedure,funct,plan(i).(itime),entry,keep=keep
 endif else call_procedure,funct,plan(i).(itime),entry 

 if inst eq 'C' then begin
  copy_struct,entry,base
  buff(i)=base
 endif else begin
  if not exist(buff) then buff=entry else $
   buff=concat_struct(temporary(buff),entry)
 endelse
endfor

;-- sort in time

if keyword_set(sorting) and (exist(buff)) then begin
 buff=mk_plan_sort(temporary(buff),/uniq,/str)
endif

nobs=n_elements(buff)
if nobs eq 0 then buff=-1

return,buff
end
