;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_PLAN_COMP
;               
; Purpose     : compare contents of two plans
;               
; Category    : Planning
;               
; Explanation : 
;               
; Syntax      : IDL> status=mk_plan_comp(plan1,plan2)
;    
; Examples    : 
;
; Inputs      : PLAN1, PLAN2 = input plans to check
;               
; Opt. Inputs : None
;               
; Outputs     : STATUS = 1 if perfect match, 0 otherwise
;
; Opt. Outputs: None
;               
; Keywords    : DTAG = tag name where first difference is found
;               END_TIME = check plan end times (by def, end times for
;                          DETAILS are not checked).
;               TRUNCATE = truncate MSECS off times
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: None
;               
; History     : Version 1,  4-May-1996,  D M Zarro.  Written
;
; Contact     : DMZARRO
;-            

function mk_plan_comp,plan1,plan2,dtag=dtag,end_time=end_time,$
                      truncate=truncate

dtag=''
type1=get_plan_type(plan1)
type2=get_plan_type(plan2)
err='syntax -> MATCH=MK_PLAN_COMP(P1,P2)'
if (type1 ne type2) or (type1 eq -1) or (type2 eq -1) or $
   (n_elements(plan1) ne 1) or (n_elements(plan2) ne 1) then begin
 help,type1,type2,plan1,plan2,/st
stop
 message,err,/cont & return,0
endif

itime=get_plan_itime(plan1)

;-- check all tags except time and pointings

tags=tag_names(plan1)
point=where('POINTINGS' eq tags,pcount)
status=match_struct(plan1,plan2,exclude=[itime,itime+1,point],dtag=dtag)
if not status then return,0

;-- check pointings

if pcount gt 0 then begin
 p1=plan1.pointings & p2=plan2.pointings
 n1=plan1.n_pointings & n2=plan2.n_pointings
 if (n1 gt 0) and (n2 gt 0) then begin
  status=match_struct(p1(0:n1-1),p2(0:n2-1),dtag=dtag)
  if not status then return,0
 endif
endif

;-- check times

if keyword_set(truncate) then format='(f15.0)' else format='(f15.3)'
dtag=tags(itime)
status=str_format(plan1.(itime),format) eq str_format(plan2.(itime),format)
if not status then return,0

;-- skip END time if DETAILS plan

if (type1 gt 0) or (keyword_set(end_time)) then begin
 dtag=tags(itime+1)
 status=str_format(plan1.(itime+1),format) eq str_format(plan2.(itime+1),format)
 if not status then return,0
endif

;-- must be a match

dtag=''
return,1  & end
