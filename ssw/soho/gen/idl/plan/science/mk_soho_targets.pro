;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       MK_SOHO_TARGETS
;
; PURPOSE:
;       Create an HTML file listing all SOHO plan targets
;
; CATEGORY:
;       Planning, utility
;
; EXPLANATION:
;       This program creates an HTML target page for SOHO plan
;       entries for given start DATE.
;       The created file contains SOHO plans and
;       DSN schedule for a specific operational day.
;
; SYNTAX:
;       mk_soho_target [, date]
;
; INPUTS:
;       None required.
;
; OPTIONAL INPUTS:
;       DATE - Starting date (in any ECS format) on which the targets
;              to be listed; if missing, the current date is assumed
;       FILE - output file name [def = soho_plan.YYYYMMDD, where YYYYMMDD
;              represents the date 
; KEYWORDS:
;
;       DIRECTORY - Name of the directory in which output files are created;
;                   if missing, the current IDL working directory is used
;
;       VERBOSE   - Set this keyword to get more informative prompts
;                   from the program
;
;       SCIPLAN   - print SCI_PLAN (default = DETAILS)
;
;       NODSN     - don't list DSN
;
;       ERR       - error string
;
;       NOAA      - get latest NOAA line drawing 
;       
;       DAYS      - # of DAYS to list [def=1]
;
; HISTORY:
;       Version 1, March 22, 1996, Liyun Wang, GSFC/ARC. Written
;       Version 2, April 26, 1996, Liyun Wang, GSFC/ARC
;          Modified so that targets for CELIAS, SWAN, and ERNE appear
;             as JOP, SOC, and Campaign, respectively
;       Version 3, June 6 1996, Dominic Zarro, GSFC
;          Modified to show DETAILED pointing and FOV
;          Added call to UPDATE_KAP
;       Version 4, November 26 1996, Dominic Zarro, GSFC
;          Added file/directory write protection checks and ERR keyword
;       Version 5, Jan 6 1997, Dominic Zarro, GSFC
;          Made HTML the norm
;       Modified, 23 Feb 2007, Zarro (ADNET/GSFC) 
;         - added link to planning timeline graphic 
;       Modified, April 14, 2007, Zarro (ADNET)
;        - forced reading of SOHO database

; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-

     pro print_entry,unit,entry,inst=inst

     itime=get_plan_itime(entry)
     ind = '  '
     ind2 = ind
     dur = sec2dhms(LONG(entry.(itime+1)-entry.(itime)) > 0)
     if datatype(inst) ne 'STR' then inst=''

;-- row 1

     printf,unit,'<table border=0 cellspacing=0 cellpadding=1 bgcolor="lightgrey"> 
     printf,unit,'<tr>
     printf,unit,'<th align=left>'+inst+' Start:'
     printf,unit,'<td>'+anytim2utc(entry.(itime), /vms, /trun)
     printf,unit,'<th> End:'
     printf,unit,'<td>'+anytim2utc(entry.(itime+1), /vms, /trun)

;-- row 2

     printf,unit,'<tr>
     printf,unit,'<th align=left>Duration: 
     printf,unit,'<td colspan=4>'+dur

;-- row 3

     if tag_exist(entry, 'SCI_OBJ') then begin
      sci_obj=trim(entry.sci_obj)
      if sci_obj ne '' then begin
       printf,unit,'<tr>
       printf,unit,'<th align=left> Sci. Obj:   
       printf,unit,'<td colspan=4> '+sci_obj
      endif
     endif

     if tag_exist(entry, 'SCI_SPEC') then begin
      sci_spec=trim(entry.sci_spec)
      if sci_spec ne '' then begin
       printf,unit,'<tr>
       printf,unit,'<th align=left> Spec. Obj:   
       printf,unit,'<td colspan=4> '+sci_spec
      endif
     endif

     if tag_exist(entry, 'OBS_PROG') then begin
      obs_prog=trim(entry.obs_prog)
      if obs_prog ne '' then begin
       printf,unit,'<tr>
       printf,unit,'<th align=left> Obs. Prog:   
       printf,unit,'<td colspan=4>'+obs_prog
      endif
     endif

;-- OBJECT

     if tag_exist(entry, 'OBJECT') then begin
      object=strmid(trim(entry.object),0,3)
      err=''
      if object ne '' then begin
       get_object,object,desc,err=err
       if err eq '' then output=desc.object_desc else output=entry.object
       printf,unit,'<tr>
       printf,unit,'<th align=left> Object:
       printf,unit,'<td colspan=4>'+trim(output)

       search_str='syn'
       if (inst eq 'UVCS') and (strpos(strlowcase(object),search_str) gt -1) then begin
        printf,unit,'<b><a href="http://cfa-www.harvard.edu/~yko/index.html">'
        printf,unit,'<font size=-1>[Pointings]</font></a></b>
       endif        
      endif
     endif

;-- OBJ_ID

     if tag_exist(entry, 'OBJ_ID') then begin
      obj_id=trim(entry.obj_id)
      if obj_id ne '' then begin
       printf,unit,'<tr>
       printf,unit,'<th align=left> Object ID:
       printf,unit,'<td colspan=4>'+obj_id
      endif
     endif

;-- CMP_NO

     if tag_exist(entry,'CMP_NO') and tag_exist(entry,'CMP_NAME') then begin
      cmp_no=long(str2arr(entry.cmp_no))
      cmp_no=cmp_no(0)
      if cmp_no gt 1 then begin
       err=''
       get_campaign,cmp_no,cmp,err=err
       if err eq '' then begin
        num=trim(string(cmp_no,'(i4)'))
        num='<a href=http://sohodata.nascom.nasa.gov/cgi-bin/soho_campaign_select?'+num+'>'+num+'</a>'
        printf,unit,'<tr>
        printf,unit,'<th align=left> Campaign '+num+' :
        printf, unit,'<td colspan=4>'+trim(cmp.cmp_name)
       endif
      endif
     endif

;-- NOTES

;     if tag_exist(entry, 'NOTES') then begin
;      notes=trim(entry.notes)
;      if notes ne '' then begin
;       printf,unit,'<tr>
;       printf,unit,'<th align=left> Notes:   
;       printf,unit,'<td colspan=4>'+notes
;      endif
;     endif

     printf,unit,'</table>

;-- Pointing

     xpoint='' & ypoint='' & xwidth='' & ywidth='' & angle=''
     if tag_exist(entry,'XCEN') then xpoint=str2arr(entry.xcen,delim=',') 
     if tag_exist(entry,'YCEN') then ypoint=str2arr(entry.ycen,delim=',')
     if tag_exist(entry,'IXWIDTH') then xwidth=str2arr(entry.ixwidth,delim=',')
     if tag_exist(entry,'IYWIDTH') then ywidth=str2arr(entry.iywidth,delim=',')
     if tag_exist(entry,'ANGLE') then angle=str2arr(entry.angle,delim=',')
     ok=where(trim(angle) ne '',na)
     add_angle=(na gt 0)
     nxp=n_elements(xpoint) & nyp=n_elements(ypoint)
     nxw=n_elements(xwidth) & nyw=n_elements(ywidth)
     np=max([nxp,nyp,nxw,nyw,na])
     printf,unit,'<br><table border=0 cellspacing=5 cellpadding=0>'
     printf,unit,'<tr><th colspan=2> Center Coordinates 
     printf,unit,'<th colspan=2> Field Of View 
     if add_angle then printf,unit,'<th colspan=1> Roll Angle
     printf,unit,'<tr>'
     printf,unit,'<td align=center colspan=2> (arcsecs)'
     printf,unit,'<td align=center colspan=2> (arcsecs)'
     if add_angle then printf,unit,'<td align=center colspan=1> (degrees)'
     printf,unit,'<tr>'
     printf,unit,'<th>X-cen'  
     printf,unit,'<th>Y-cen'
     printf,unit,'<th>Width' 
     printf,unit,'<th>Height'
     printf,unit,'<th>'


     for i=0l,np-1 do begin
      printf,unit,'<tr>'
      if i lt nxp then xp=xpoint(i) else xp=''
      if not is_number(xp) then xp='N/A' else xp=string(xp,'(f8.2)')
      if i lt nyp then yp=ypoint(i) else yp=''
      if not is_number(yp) then  yp='N/A' else yp=string(yp,'(f8.2)')
      if i lt nxw then xw=xwidth(i) else xw=''
      if not is_number(xw) then xw='N/A' else xw=string(xw,'(f8.2)')
      if i lt nyw then yw=ywidth(i) else yw=''
      if not is_number(yw) then yw='N/A' else yw=string(yw,'(f8.2)')
      printf, unit,'<td align=center>'+trim(xp)
      printf, unit,'<td align=center>'+trim(yp)
      printf, unit,'<td align=center>'+trim(xw)
      printf, unit,'<td align=center>'+trim(yw)
      if add_angle then begin
       if i lt na then roll=angle(i) else roll=''
       if not is_number(roll) then roll='0.0' else roll=string(roll,'(f6.1)')
       printf, unit,'<td align=center>'+trim(roll)
      endif
     endfor
     printf, unit,'</table>

     return & end

;-----------------------------------------------------------------------------

     pro print_dsn, unit,dsn
     smodes=['','LASCO/EIT (Hi)','CDS (Hi)','SUMER (HI)','Nominal','LASCO/EIT (Extra)']
     ns=n_elements(smodes)

     ndsn=n_elements(dsn)
     if ndsn eq 0 then return
     itime=get_plan_itime(dsn)
     printf,unit,'<center><hr><br>'
     head='<h1> <font color = red > DSN Contacts </font> </h1>'
     printf,unit,head
     printf,unit,'<table align=center border=1 cellspacing=0 cellpadding=0 bgcolor="lightyellow"> 
     printf,unit,'<tr><th> Start'
     printf,unit,'<th> End'
     printf,unit,'<th> Duration'
     printf,unit,'<th> Submode'
     for i=0l,ndsn-1 do begin
      dmode=get_soho_submode(dsn(i).submode)
      dur = trim(sec2dhms(LONG(dsn(i).(itime+1)-dsn(i).(itime)) > 0))
      printf,unit,'<tr>'
      printf,unit,'<td align=center>'+trim(anytim2utc(dsn(i).(itime),/trun,/vms))
      printf,unit,'<td align=center>'+trim(anytim2utc(dsn(i).(itime+1),/trun,/vms))
      printf,unit,'<td align=center>'+dur
      printf,unit,'<td align=left>'+dmode
     endfor
     printf,unit,'</tr></table>'
     printf,unit,'<br><hr>'
     printf,unit,'</center>'

     return & end

;-----------------------------------------------------------------------------

   pro mk_soho_targets, date,file,directory=directory,$
    nodsn=nodsn,err=err,noaa=noaa,previous=previous,next=next,$
    verbose=verbose,sciplan=sciplan,soho=soho,days=days,term=term,$
    head=head

   std=keyword_set(term)
   verbose=keyword_set(verbose)
   noaa=keyword_set(noaa)
   add_previous=datatype(previous) eq 'STR'
   add_next=datatype(next) eq 'STR'

;-- determine output directory

   if (not std) then begin
    cd,curr=cdir     
    if (datatype(directory) ne 'STR') then begin
     if datatype(file) eq 'STR' then begin
      break_file,file,fdsk,fdir,fname,fext
      fdir=trim(fdsk+fdir)
      if fdir ne '' then directory=fdir else directory=cdir
     endif else directory=cdir
    endif 
    
    if test_dir(directory) then odir=directory else odir=cdir
    if not test_dir(odir,err=err) then begin
     message,err,/cont
     return
    endif
   endif

;-- ensure SSW SOHO planning database is used

   save_db=getenv('ZDBASE')
   err=''
   s=fix_zdbase(/soho,err=err)
   if ~s then begin
    message,err,/cont
    return
   endif

;-- construct output filename
   
   get_utc, curr_time
   err=''
   odate=anytim2utc(date,err=err)
   if err ne '' then odate=curr_time
   odate.time = 0L
   date_str = anytim2utc(odate, /ecs, /date)
   date_code = strmid(date_str, 0, 4)+strmid(date_str, 5, 2)+strmid(date_str, 8, 2)
   if not std then begin 
    if datatype(file) eq 'STR' then begin
     break_file,file,dsk,dir,dfile,dext
     ofile=concat_dir(odir,dfile+dext)
    endif else ofile=concat_dir(odir,'soho_plan'+'_'+date_code+'.html')
   endif

;-- read DB's

   if verbose then message,'Working on day '+date_str+'...',/cont

   if not exist(days) then days=1
   tstart=odate
   tend=tstart & tend.mjd=tend.mjd+long(days)

   long_list = get_soho_inst(exclude=['ERNE','SWAN','CELIAS'])
   short_list=get_soho_inst(long_list,/short)
   nplans=0 & n_dsn=0

   if not keyword_set(nodsn) then begin
    if verbose then message,'Getting DSN schedule...',/cont
    list_dsn, tstart, tend, dsn, n_dsn
   endif
    
;-- list PLANS

   list_plan, tstart, tend,sci_plans,nsci
   list_soho_det, tstart, tend,det_plans, ndet

   if keyword_set(sciplan) then begin
    if verbose then message,'Getting Science plans...',/cont
    if nsci gt 0 then plans=sci_plans
    nplans=nsci
   endif else begin
    if verbose then message,'Getting Detailed plans...',/cont
    if ndet gt 0 then plans=det_plans
    nplans=ndet
    
;-- use Science plan entries for instruments that don't have Detailed plans

    if nsci gt 0 then begin
     if ndet gt 0 then begin
      ilook=where_vector(det_plans.instrume,short_list,count,rest=rest,$
                         rcount=rcount)
      if (rcount gt 0) then begin
       rinst=short_list(rest)
       sci_inst=sci_plans.instrume
       slook=where_vector(rinst,sci_plans.instrume,count)
       if count gt 0 then rplans=sci_plans(slook)
      endif
     endif else rplans=sci_plans
    endif
   endelse


;-- get latest NOAA synoptic line drawing
;-- check date of image relative to current day and name it
;-- appropriately. 

   quiet=1-verbose
   ncount=0 
   if noaa then begin
    get_noaa,nfile,odate,count=ncount,back=4,out=odir,gdate=gdate,$
              quiet=quiet,err=err

    if (ncount gt 0) and (err eq '') then begin
 ;    rebin_gif,nfile,nfile,.9,size=psize
     get_utc,curr_time
     tdiff=odate.mjd-curr_time.mjd
     case tdiff of
      -1: gname='yesterday.gif'
       0: gname='today.gif'
       1: gname='tomorrow.gif'
       2: gname='dayafterto.gif'
     else: nothing=0
     endcase
     break_file,nfile,dsk,dir,dfile,dext
     if exist(gname) then begin
      noaa_file=concat_dir(dsk+dir,gname)
      espawn,'mv -f '+nfile+' '+noaa_file,/noshell
     endif else begin
      gname=dfile+dext
      noaa_file=nfile 
     endelse
    endif
   endif

   if not std then openw, unit,ofile, /get_lun else unit=-1
   if std and keyword_set(head) then begin
    printf,unit,'Content-type: text/html'
    printf,unit,'          '
    printf,unit,'          '
   endif
   printf,unit,'<html>'
   printf,unit,'<head>'
   printf,unit,'<title>SOHO TARGETS</title>'
   printf,unit,'<meta http-equiv="Expires" content="-1">'
   printf,unit,'<meta http-equiv="Pragma" content="no-cache">'
   printf,unit,'<meta http-equiv="Cache-Control" content="no-cache">'
   printf,unit,'<meta http-equiv="Refresh" content=300>'
   printf,unit,'<script type="text/javascript" src="./popup.js">'
   printf,unit,'</script>'
   printf,unit,'</head>'
   printf,unit,'<body bgcolor=#ffffff text="#000000" vlink="blue" link="blue">'
   printf,unit,'<font face="Arial,Helvetica" >
   printf,unit,'<table border=0 cellspacing=5 cellpadding=0><tr>'
   printf,unit,'<td><img src=http://sohowww.nascom.nasa.gov/gif/sohologo120.gif> </td>'
   printf,unit,'<td></td><td></td><td> <h1> SOHO Targets: '+anytim2utc(date_str,/vms,/date)+'</h1></td>'
   printf,unit,'</tr></table>'
   printf,unit,'<br>'
   printf,unit,'<center><table>
   printf,unit,'<th align=left> Last Updated:'
   printf,unit,'<td>'+anytim2utc(curr_time, /vms, /truncate)+' UT'
   printf,unit,'<tr>'
;   printf,unit,'<th align=left> Questions or Problems?'
;   printf,unit,'<td> <a href=mailto:dzarro@solar.stanford.edu> dzarro@solar.stanford.edu </a> 
   printf,unit,'</table>'
   printf,unit,'<br><br>'
   utc=anytim2utc(odate) & utc.time=0
   tom=utc & tom.mjd=tom.mjd+1 & tom=trim(anytim2utc(tom,/vms,/date))
   yes=utc & yes.mjd=yes.mjd-1 & yes=trim(anytim2utc(yes,/vms,/date))
   if add_previous then begin
    printf,unit,'<a href=./'+trim(previous)+'> [ '+yes+' ]'
   endif
   if add_next then begin
    printf,unit,'<a href=./'+trim(next)+'> [ '+tom+' ]'
   endif
;   printf,unit,'<br><hr>'
   printf,unit,'</center>'
   do_plans=exist(plans) or exist(rplans)
   if do_plans then begin
    if exist(plans) then dinst=plans.instrume
    if exist(rplans) then if exist(dinst) then $
     dinst=[dinst,rplans.instrume] else dinst=rplans.instrume
    dorder = uniq([dinst], sort([dinst]))
    dinst=dinst(dorder)
   endif

   if (1-do_plans) and (n_dsn eq 0) then begin
    printf, unit,'<br>'
    printf, unit,'<center> No targets found in Database. </center>'
    printf, unit,'<br>'
    printf,unit,'<center> <hr> </center>'
   endif else begin

;-- output index anchors

    if do_plans then begin
     ok=where_vector(short_list,dinst,count)
     if count gt 0 then begin
      ivalid=get_soho_inst(dinst(ok))
      for k=0l,count-1 do begin
       item='<a href = "#'+ivalid(k)+'">'+ivalid(k)+'</a>'
      if k eq 0 then str=item else str=str+' '+item
      endfor
      printf,unit,'<br>'
      printf,unit,'<center> '+str+' </center>'
      printf,unit,'<br>'
      if exist(gname) then begin
       printf,unit,'<center> <a href=./'+gname+' > [NOAA Synoptic Drawing: '+gdate+' ] </a> </center>'
       printf,unit,'<br>'
      endif
     endif
    endif
    pfile='./'+file_break(file,/no_ext)+'.gif'
    plan="'"+pfile+"','650','850'"
    printf,unit,'<center><a href="'+pfile+'">[Planning Timeline] </a>'
    printf,unit,'</center>'

;-- output DSN

    print_dsn,unit,dsn
;    printf,unit,'<font face="Arial,Helvetica" size=2 >


;-- output instrument plans

    if do_plans then begin
     for i=0l, n_elements(short_list)-1 do begin
      ilook = where(dinst eq short_list(i),count)
    
      if count gt 0 then begin
       inst = long_list(i)
       if keyword_set(sciplan) then begin
        case (inst) OF
         'CELIAS': inst = 'JOP'
         'SWAN': inst = 'SOC'
         'ERNE': inst = 'Campaign'
         else: 
        endcase
       endif

       pcount=0 & rcount=0
       if exist(plans) then begin
        finst=where(short_list(i) eq plans.instrume,pcount)
        if pcount gt 0 then dplans=plans(finst)
       endif

       if exist(rplans) then begin
        finst=where(short_list(i) eq rplans.instrume,rcount)
        if rcount gt 0 then dplans=rplans(finst)
       endif

       if (rcount gt 0) and (pcount gt 0) then $
        message,'weird problem...',/cont
       printf,unit,'<a name="'+inst+'">'
       printf,unit,'<br>'
       word='<center> <h1> <font color = red > '+inst+' </font>'
       printf, unit, word
       if inst eq 'CDS' then begin
        if is_string(file) then begin
         break_file,file,dsk,dir,pname
         pname=concat_dir('pointings',pname+'.html')
         aname=concat_dir(directory,pname)
         count=1
;         chk=loc_file(aname,count=count)
         if count gt 0 then begin
          pfile='./'+pname
          printf,unit,'<b><a href="'+pfile+'">'
          printf,unit,'<font size=-1>[Pointings]</font></a></b>
         endif
        endif
       endif
       printf,unit,'</h1></center>
       printf,unit,'<center> <hr> </center>'
       for j=0l, n_elements(dplans)-1 do begin
        print_entry, unit,dplans(j),inst=inst
        printf,unit,'<br>'
        printf,unit,'<center> <hr> </center>'
       endfor
      endif
     endfor
    endif
   endelse

   printf,unit,'</body>'
   printf,unit,'<head><meta http-equiv="Expires" content="-1">'
   printf,unit,'<meta http-equiv="Pragma" content="no-cache">'
   printf,unit,'<meta http-equiv="Cache-Control" content="no-cache"></head>'

   printf,unit,'</html>'

   if not std then begin
    close, unit
    free_lun, unit
   endif
   if not std then begin
    if verbose then message,'Output file '+ofile+' created.',/cont
    if strupcase(os_family()) ne 'VMS' then espawn,'chmod ug+w '+ofile,out,/noshell
   endif

   if exist(noaa_file) then begin
    if verbose then message,'Output NOAA file '+noaa_file+' created.',/cont
    if strupcase(os_family()) ne 'VMS' then espawn,'chmod ug+w '+noaa_file,out,/noshell
   endif
    
   mklog,'ZDBASE',sav_db

   return & end

