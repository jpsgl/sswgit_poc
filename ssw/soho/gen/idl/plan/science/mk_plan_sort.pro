;+
FUNCTION mk_plan_sort, plans, err, unique=unique, by_time=by_time, utc=utc,$
                     by_sci_obj=by_sci_obj, floating=floating, itime=itime,$
                     str=str
;+
; PROJECT:
;       SOHO - CDS
;
; NAME:	
;       MK_PLAN_SORT()
;
; PURPOSE:
;       Sort a list of plans by time
;
; EXPLANATION:
;       
; CALLING SEQUENCE: 
;       Result = sort_plan(plans [, err])
;
; INPUTS:
;       PLANS - List of plan entries
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       RESULT - Sorted plan entries if no error occurs, otherwise the input
;                PLANS is intact or -1 for syntax error
;
; OPTIONAL OUTPUTS:
;       ERR    - Any error message returned
;
; KEYWORD PARAMETERS: 
;       BY_TIME = sort by time [def]
;       BY_SCI_OBJ = sort by SCI objective
;       UNIQUE = sort and select unique entries
;       FLOATING = sort on floating point times [to avoid possible roundoff]
;       ITIME = position of time tag 
;       UTC   = sort on UTC times
;       STR   = sort on string times (same as UTC)
;
; CALLS:
;       EXIST, GET_PLAN_TYPE
;
; COMMON BLOCKS:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Utility, Planning
;
; PREVIOUS HISTORY:
;       Written May 18, 1995, Liyun Wang, GSFC/ARC
;
; MODIFICATION HISTORY:
;       Version 1, created, Liyun Wang, GSFC/ARC, May 18, 1995
;       Version 1.1, Zarro, ARC, June 6, 1995 -- added UNIQUE
;       Version 1.2, Zarro, ARC, Nov  1, 1995 -- remove side effect
;                    of modifying input plans
;       Version 2, Zarro, ARC, March 26, 1996 -- added UTC keyword
;       Version 3, Zarro, ARC, Sept 5, 1996   -- made /STR the default
; VERSION:
;       Version 3
;-
;
   ON_ERROR, 1
   err = ''
   nplan = N_ELEMENTS(plans) 
   IF nplan EQ 0 THEN BEGIN
      err = 'Syntax error: result = mk_plan_sort(plans, /by_time, [/by_sci_obj])'
      MESSAGE, err, /cont
      RETURN, -1
   ENDIF

   IF nplan EQ 1 then RETURN,plans

   if keyword_set(by_sci_obj) or keyword_set(floating) then str=0 else str=1

   BY_TIME=KEYWORD_SET(BY_TIME) or KEYWORD_SET(STR)
   BY_SCI_OBJ=KEYWORD_SET(BY_SCI_OBJ)

   IF (NOT BY_TIME) AND (NOT BY_SCI_OBJ) THEN BY_TIME=1
   IF BY_TIME AND BY_SCI_OBJ THEN MESSAGE,'Sorting by time only',/cont


   if not exist(itime) then itime = get_plan_itime(plans)
   if itime eq -1 then return,plans

   CASE 1 of
      BY_TIME: BEGIN
         mstart=plans.(itime)
         IF KEYWORD_SET(utc) or KEYWORD_SET(str) THEN mstart = STR_FORMAT(mstart) else $
          IF KEYWORD_SET(floating) THEN mstart = FLOAT(mstart) 
         IF KEYWORD_SET(unique) THEN sorder = uniq([mstart], SORT([mstart])) $
         ELSE sorder=SORT([mstart])
      END
      BY_SCI_OBJ:BEGIN
         ii = WHERE(TAG_NAMES(plans) EQ 'SCI_OBJ', count)
         IF count GT 0 THEN BEGIN
            sci_obj = plans.sci_obj

            IF nplan GT 1 THEN sorder = SORT([sci_obj])
         ENDIF ELSE BEGIN
            err = 'Tag name SCI_OBJ not found in plan.'
            MESSAGE,err,/contin
            RETURN, plans
         ENDELSE
      END
      ELSE: BEGIN
         err='Unsupported keyword.'
         MESSAGE,err,/contin
         RETURN, plans
      END
   ENDCASE

   IF exist(sorder) THEN return,plans(sorder) else return,plans
END

;---------------------------------------------------------------------------
; End of 'mk_plan_sort.pro'.
;---------------------------------------------------------------------------
