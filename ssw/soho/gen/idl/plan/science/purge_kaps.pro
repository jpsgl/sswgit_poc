;+
; Project     : SOHO
;
; Name        : PURGE_KAPS
;
; Purpose     : Remove old versions of KAP files on archive. 
;
; Category    : planning
;
; Explanation : Uses DATE_CREATED field in header of KAP file to
;		determine most recent file.  Does not use version
;		number in file name.  
;
; Syntax      : purge_kaps, startday=xxx, endday=xxx
;
; Examples    : purge_kaps, startday='97/1/1', endday='97/8/1'
;
; Inputs      : 
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : STARTDAY - (required) first day to purge KAP for, yy/mm/dd 
;		ENDDAY - (required) last day to purge KAP for, yy/mm/dd 
;
; Common      : None
;
; Restrictions: Requires that SOHO_KAP or SOHO_EAP environment variable be 
;		set to point to location of KAP files on archive.  Requires 
;		write access to KAP directory on archive.
;		Requires write access to /tmp for temporary storage of most
;		recent KAP file (and that /tmp have at least ~60K free space)
;
; Side effects: None
;
; Written July 8, 1997  Kim Tolbert, HSTX
;
; Contact     : kim@ecsman.nascom.nasa.gov
;-


pro purge_kaps, startday=startday, endday=endday

if not (keyword_set(startday) and keyword_set(endday)) then $ 
   message,"Usage:  purge_kaps, startday='yy/mm/dd', endday='yy/mm/dd'"

kap_dir=getenv('SOHO_KAP')
if kap_dir eq '' then kap_dir=getenv('SOHO_EAP')
if kap_dir eq '' then message,"Can't find KAP directory. Aborting..."

; Convert start and end times to seconds, and check for validity
startsec = utime (startday, error=error1)
endsec = utime (endday,   error=error2)
if (error1 or error2) then  message, 'Error in start or end time. Aborting...'

; loop through requested days
currsec = startsec
while currsec le endsec do begin
   s = (strsplit (anytim (currsec, out='hxrbs'), ',', /head)) (0)
   print, 'Working on day ',s

   fullfile = get_latest_kap (s, status=status)
   file = strsplit (fullfile, '/', /tail)
   if (status eq 1) then begin

;     Move most recent file to temporary location.
      cmd = '\mv ' + fullfile + ' /tmp/' + file
      espawn, cmd, result,/noshell
      if (result(0) ne '') then goto, errorexit

;     Delete all KAP's for current day on directory kap_dir.
      wildcard = '*' + strmid(s,0,2) + strmid(s,3,2) + strmid(s,6,2) + '*KAP'
      cmd = '\rm ' + kap_dir + '/' + wildcard
      espawn, cmd, result,/noshell
      if (result(0) ne '') then print, result 

;     Move most recent file back to kap_dir 
      cmd = '\mv /tmp/' + file + ' ' + fullfile
      espawn, cmd, result,/noshell
      if (result(0) ne '') then goto, errorexit

   endif else print,'No KAP file for day ' + s
   

;  Increment to next day.
   currsec = currsec + 86400.d0

endwhile
goto, getout

errorexit:
print, result
print, 'Command was:  ', cmd
print, 'Aborting...'
goto, getout

getout:
return
end

