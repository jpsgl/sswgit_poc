;+
; Project     :	SOHO - CDS
;
; Name        : XIAP
;
; Purpose     : widget interface to WRITE_IAP and WRITE_CAP
;
; Use         : XIAP
;
; Inputs      : None.
;
; Opt. Inputs : OPS_DAY1 = operational start day in any CDS UTC format.
;               OPS_DAY2 = operational stop day in any CDS UTC format.
;
; Outputs     : STATUS = 1 if successful, 0 otherwise
;
; Opt. Outputs: None.
;
; Keywords    : 
;               OBSERVER = observer name 
;               COMMENT = optional comment
;               CAP = set to write Composite Activity plan instead of 
;                     Instrument plan
;               TITLE = Title of widget; defaults to 'XIAP'
;               
; Explanation : None.
;
; Common      : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Help
;
; Prev. Hist. : None.
;
; Written     :	Zarro (ARC/GSFC) 27 March 1995
;
; Version     : 1
;-

;----------------------------------------------------------------------------

 pro xiap_write,info

 info.write_done=0
 info=rep_tag_value(info,'','iap_file')
 if info.cap then ftype='CAP' else ftype='IAP'
 widget_control,info.wmess,set_value='Please wait. Writing '+ftype+'...'
 widget_control,info.wbase,/hour
 err='' 
 tstart=utc2tai(info.ops1)
 tstop=utc2tai(info.ops2)
 if tstop lt tstart then sign=-1 else sign=1
 day_secs=24.d*3600.d
 tnext=tstart
 if info.cap then xwrite='write_cap' else xwrite='write_iap'
 express=xwrite+'(+tnext,observer=info.obs,comment=info.com,err=err,file=file' 
 if info.cap then express=express+')' else express=express+',/noconcat)'
 repeat begin
  file=''
  s=execute('info.status='+express)
  if info.status then begin
   if exist(suc_file) then suc_file=[suc_file,file] else suc_file=file
  endif
  widget_control,info.wmess,set_value=err,/app
  tnext=tnext+sign*day_secs
 endrep until (tnext gt tstop)
 widget_control,info.wbase,/show
 if not exist(suc_file) then suc_file='' 
 info.write_done=1
 info=rep_tag_value(info,suc_file,'iap_file')

 if (info.iap_file(0) ne '') then begin
  widget_control,info.wmess,set_value='Write completed',/app
 endif else begin 
  widget_control,info.wmess,set_value='No IAP files written',/app
 endelse

 return & end

;----------------------------------------------------------------------------

 pro xiap_event,  event                         ;event driver routine

 common xiap,info

 widget_control, event.top, get_uvalue = unseen
 info=get_pointer(unseen,/no_copy)
 if datatype(info) ne 'STC' then return

 if info.cap then ftype='CAP' else ftype='IAP'

 if (n_elements(uservalue) eq 0) then uservalue=''
 wtype=widget_info(event.id,/type)
 widget_control, event.id, get_uvalue = uservalue

;-- button widgets

 bname=strtrim(uservalue,2)

;-- validate time inputs

 if (bname eq 'view') or (bname eq 'write') then begin
  good=xvalidate(info,event,/date,/bytag)
  if not good then goto,done
 endif

 case bname of 

  'reset'  : begin
    info.ops1=info.def_ops1
    info.ops2=info.def_ops2
    info=rep_tag_value(info,'','com')
    info.obs=info.def_obs
    widget_control,info.wobs,set_value=info.def_obs
    widget_control,info.wops1,set_value=info.def_ops1
    widget_control,info.wops2,set_value=info.def_ops2
    widget_control,info.wmess,set_value=''
   end

  'quit'   : begin
    quit=1
    if not info.write_done then begin     
     if info.cap then ftype='CAP' else ftype='IAP'
     iwrite=xanswer('You probably forgot to write a new '+ftype+' file.',$
                      instruct='Write it before exiting?',group=event.top)
     if iwrite then begin
      xiap_write,info
      quit=0
     endif
    endif
    if quit then begin
     xtext_reset,info
     xkill,event.top  
    endif
   end

   'observer' : begin
    widget_control,event.id,get_value=tt
    info.obs = strmid(trim(tt(0)),0,50)
   end

  'comment' : begin
    tt = info.com
    xinput,tt,'Enter Comments [50 char max]',group=event.top,$
      /modal,max_len=50,ysize=10
    info=rep_tag_value(info,tt,'com')
   end

  'view': begin
    tstart=anytim2tai(info.ops1)
    tstop=anytim2tai(info.ops2)
    if tstop lt tstart then sign=-1 else sign=1
    day_secs=24.d*3600.d
    tnext=tstart
    if info.cap then sub='submit_cap' else sub='submit_iap'
    repeat begin
     instruct='Exit'
     file=find_kap_file(tnext,iap=1-info.cap,cap=info.cap,ver=ver,err=err)
     status=1
     if ver lt 0 then begin
      widget_control,info.wmess,set_value=err,app=(tnext ne tstart)
      widget_control,info.wmess,set_value='May need to rewrite.',/app
     endif else begin
      if event.id eq info.ftpb then begin
       widget_control,info.wmess,set_value='Submitting: '+file,app=(tnext ne tstart)
       widget_control,/hour
       ok=call_function(sub,file,err=err)
       if not ok then mess='Submission failed for: '+file else $
        mess='Submitted: '+file
       widget_control,info.wmess,set_value=mess,/app
       info.write_done=1
      endif else begin
       xtext,rd_ascii(file),group=event.top,/modal,next=(tnext lt tstop),status=status,$
       title=ftype+' for '+anytim2utc(tnext,/ecs,/date,/vms),instruct=instruct
      endelse
     endelse
     tnext=tnext+sign*day_secs
    endrep until (tnext gt tstop) or (not status)
   end

  'write': xiap_write,info

  else:do_nothing=1

 endcase

 done: set_pointer,unseen,info,/no_copy

 return & end

;--------------------------------------------------------------------------- 

PRO xiap,ops_day1,ops_day2,status,comment=comment,observer=observer,$
         group=group,modal=modal,font=font,cap=cap,$
         title=title,last=last

common xiap

;-- defaults

if not have_widgets() then begin
 message,'widgets unavailable',/cont
 return
endif

caller=get_caller(stat)
if (stat) and (not xalive(group)) then xkill,/all 

status=0
write_done = 0
iap_file = ''

cap=keyword_set(cap)

;-- set up default times

get_utc,def_ops1
def_ops1.time=0
def_ops2=def_ops1
dtime=[utc2tai(def_ops1),utc2tai(def_ops2)]
def_ops1=anytim2utc(def_ops1,/ecs,/vms,/date)
def_ops2=anytim2utc(def_ops2,/ecs,/vms,/date)

;-- restore last time

ctime=[0.d,0.d]
if datatype(info) eq 'STC' then begin
 ctime(0)=anytim2tai(info.ops1)
 ctime(1)=anytim2tai(info.ops2)
endif

;-- choose start/stop times

times=pick_times(ops_day1,ops_day2,ctime=ctime,dtime=dtime,last=last)
def_ops1=tai2utc(times(0),/ecs,/vms,/date)
def_ops2=tai2utc(times(1),/ecs,/vms,/date)

if datatype(comment) eq 'STR' then def_com=comment
if datatype(def_com) ne 'STR' then def_com='   '
if datatype(title) ne 'STR' then title='XIAP'
if datatype(observer) eq 'STR' then def_obs=observer 
if datatype(def_obs) ne 'STR' then begin
 if strupcase(!version.os) ne 'VMS' then spawn,'whoami',def_obs else begin
  if which_inst() eq 'S' then def_obs='SUMER-STAFF' else def_obs='CDS-STAFF'
 endelse
 def_obs=strupcase(def_obs(0))
endif

;-- make widgets

if datatype(font) eq 'STR' then bfont=font
mk_dfont,bfont=bfont

wbase=widget_base(title=title,/column)

row1=widget_base(wbase,row=1,/frame)

quit=widget_button(row1,value='QUIT',uvalue='quit',font=bfont,/no_rel)
reset=widget_button(row1,value='RESET',uvalue='reset',font=bfont,/no_rel)
viewb=widget_button(row1,value='VIEW',uvalue='view',font=bfont,/no_rel)

ok=widget_button(row1,value='WRITE',uvalue='write',font=bfont,/no_rel)

ftpb=widget_button(row1,value='SUBMIT',uvalue='view',font=bfont,/no_rel)


row2=widget_base(wbase,row=3,/frame)

wops1=cw_field(row2, Title='START OPS DAY:  ',value=def_ops1,field=bfont,$
                   uvalue='ops_day1', /ret, xsize = 12, font = bfont)

wops2=cw_field(row2,  Title='END OPS DAY:    ', value=def_ops2,field=bfont,$
                    uvalue='ops_day2', /ret, xsize = 12, font = bfont)

row3=widget_base(wbase,/row)
butt=widget_label(row3,value='OBSERVER:',font=bfont)
wobs=widget_text(row3,value=def_obs,xsize=20,$
       font=bfont,uvalue='observer',/all,/edit)

row4=widget_base(wbase,/row)
butt=widget_button(row4,value='PRESS TO READ OR ENTER COMMENTS ',font=bfont,$
                   uvalue='comment',/no_rel)

wmess=widget_text(wbase,value='   ',ysize=5,xsize=50,/scroll)

;-- realize and center main base

xrealize,wbase,group=group,/center 

;-- stuff info structure into pointer

make_pointer,unseen
info = {obs:def_obs,ops1:def_ops1,ops2:def_ops2,com:def_com,$
        def_obs:def_obs,def_ops1:def_ops1,def_ops2:def_ops2,$
        wobs:wobs,wops1:wops1,wops2:wops2,wmess:wmess,$
        iap_file:iap_file,write_done:write_done,status:status,$
        ftpb:ftpb,viewb:viewb,cap:cap,wbase:wbase}

set_pointer,unseen,info,/no_copy
widget_control,wbase,set_uvalue=unseen

xmanager,'xiap',wbase,/modal,group=group
xmanager_reset,wbase,/modal,group=group,crash='xiap'

if not xalive(wbase) then begin
 info=get_pointer(unseen,/no_copy)
 free_pointer,unseen
 xshow,group
 status=0
 if datatype(info) eq 'STC' then status=info.status
endif

return & end

