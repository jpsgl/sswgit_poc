;---------------------------------------------------------------------------
; Document name: PLOT_CMDTIMES.PRO
; Created by:    Liyun Wang, GSFC/ARC, April 18, 1995
;
; Last Modified: Thu Apr 20 19:40:41 1995 (LWANG@sumop1.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
PRO PLOT_CMDTIMES, T1, T2, row, color=color
;+
; Project     :	SOHO - CDS/SUMER
;
; Name        :	PLOT_CMDTIMES
;
; Purpose     :	Plots commanding times on science plan.
;
; Explanation :	
;
; Use         :	PLOT_CMDTIMES, T1, T2, ROW
;
; Inputs      :	T1 = Array containing the beginning times of each commanding
;                    schedule period, in hours
;		T2 = Array containing the end times, in hours.
;               ROW = row in which to plot [def=0]
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	COLOR - Index of color to be used for plotting
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	PLAN_FRAME must be called first.
;
; Side effects:	None.
;
; Category    :	Planning, Science.
;
; Prev. Hist. :	None.
;
; Written     :	Liyun Wang, GSFC/ARC, April 18, 1995
;
; Modified    :	Version 1, Liyun Wang, GSFC/ARC, April 18, 1995
;
; Version     :	Version 1, April 18, 1995
;-

   ON_ERROR, 2
;
;  Check the number of parameters.
;
   IF N_PARAMS() NE 3 THEN $
      MESSAGE, 'Syntax: PLOT_CMDTIMES, T1, T2, ROW'

   IF N_ELEMENTS(ROW) EQ 0 THEN ROW=0
   IF N_ELEMENTS(color) EQ 0 THEN color = !d.n_colors-1
   
   Y1 = ROW+.25
   Y2 = ROW+.75

   FOR I = 0,N_ELEMENTS(T1)-1 DO BEGIN
      X1 = T1(I) > !X.CRANGE(0)
      X2 = T2(I) < !X.CRANGE(1)
      IF X2 GT X1 THEN POLYFILL, [X1,X2,X2,X1], [Y1,Y1,Y2,Y2], $
         color = color
   ENDFOR

   RETURN
END

;---------------------------------------------------------------------------
; End of 'PLOT_CMDTIMES.PRO'.
;---------------------------------------------------------------------------
