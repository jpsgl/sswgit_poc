;---------------------------------------------------------------------------
; Document name: mk_plan_html.pro
; Created by:    Liyun Wang, NASA/GSFC, July 25, 1996
;
; Last Modified: Wed Aug 20 14:15:11 1997 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       SOHO
;
; NAME:
;       MK_PLAN_HTML
;
; PURPOSE:
;       Make list of SOHO plans of today in HTML table format
;
; CATEGORY:
;       Planning, Utility
;
; SYNTAX:
;       mk_plan_html [, dir=dir] [, pdate]
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       PDATE - Date of plan schedule, can be in any CDS time format. If
;               passed, it overwrites keyword YESTERDAY or TOMORROW
;
; OUTPUTS:
;       HTML document named 'soho_today.html', 'soho_yesterday.html'
;          (when keyword YESTERDAY is set),  or 'soho_tomorrow.html'
;          (when keyword TOMORROW is set)
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       DIR       - Name of directory in which output file is written
;       YESTERDAY - Make list of plans for yesterday (if PDATE not passed)
;       TOMORROW  - Make list of plans for tomorrow (if PDATE not passed)
;       VERBOSE   - Set this keyword to print output file name on the screen
;       ERR       - Error string
;
; COMMON:
;       PLAN_HTML
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, July 25, 1996, Liyun Wang, NASA/GSFC. Written
;       Version 2, August 13, 1996, Liyun Wang, NASA/GSFC
;          Converted pointing parameters in f15.1 format if they are
;             floating point numbers (e.g., EIT entries)
;       Version 3, August 13, 1996, Liyun Wang, NASA/GSFC
;          Improved appearence for multi-pointing entries
;       Version 4, August 23, 1996, Liyun Wang, NASA/GSFC
;          Added optional input parameter PDATE
;       Version 5, November 26, 1996, Zarro, NASA/GSFC
;          Added output file write protection check and ERR keyword
;       Version 6, August 20, 1997, Liyun Wang, NASA/GSFC
;          Added campaign numbers and their links to campaign database
;             pages on output 
;
; CONTACT:
;       Liyun Wang, NASA/GSFC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;

PRO html_table_header, unit, new=new
   IF KEYWORD_SET(new) THEN BEGIN
      PRINTF, unit, '</table><br>'
      PRINTF, unit, '<table border=3>'
   ENDIF
   PRINTF, unit, '<tr>'
   PRINTF, unit, '<th rowspan=2>'+$
      arr2str(['Instrument', 'Program', 'CMP<br>NO'], $
              delimit='</th><th rowspan=2>')+'</th>'
   PRINTF, unit, '<th colspan=2>Obs. Time (UT)</th>'
   PRINTF, unit, '<th rowspan=2>'+$
      arr2str(['Duration', 'Object'], $
              delimit='</th><th rowspan=2>')+'</th>'
   PRINTF, unit, '<th colspan=2>Coordinates (arcsecs)</th>'
   PRINTF, unit, '<th colspan=2>Field of View (arcsecs)</th>'
   PRINTF, unit, '</tr><tr>'
   list = ['Start', 'End', 'X', 'Y', 'Width', 'Height']
   PRINTF, unit, '<th>'+arr2str(list, delimit='</th><th>')+'</th></tr>'
END

PRO html_items, unit, title, entries
;---------------------------------------------------------------------------
; INPUTS:
;     UNIT    - Output unit number to which the file is written
;     TITLE   - Title of the entries
;     ENTRIES - Array of plan entry structure;
;---------------------------------------------------------------------------
   COMMON plan_html, first_done
   n = N_ELEMENTS(entries)
   IF n GT 10 AND first_done EQ 1 THEN html_table_header, unit, /new
   CASE (title) OF
      'CDS': title = '<b><a href=http://orpheus.nascom.nasa.gov/cds/cds_homepage.html>CDS</a></b>'
      'EIT': title = '<b><a href=http://umbra.nascom.nasa.gov/eit/>EIT</a></b>'
      'JOP': title = '<b><a href=http://sohowww.nascom.nasa.gov/operations/JOPs/>JOP</a></b>'
      'CAMPAIGN': title = '<b><a href=http://sohowww.nascom.nasa.gov/operations/campaigns/>Campaigns</a></b>'
      'UVCS': title = '<b><a href=http://cfa-www.harvard.edu/uvcs/>UVCS</a></b>'
      'SUMER': title = '<b><a href=http://hydra.mpae.gwdg.de/mpae_projects/SUMER/sumer.html>SUMER</a></b>'
      'LASCO': title = '<b><a href=http://lasco-www.nrl.navy.mil/lasco.html>LASCO</a></b>'
      ELSE:
   ENDCASE

;---------------------------------------------------------------------------
;  Figure out how many extra lines are needed if multi-pointing
;  entries exist
;---------------------------------------------------------------------------
   np = 0
   IF tag_exist(entries(0), 'SCI_OBJ') THEN BEGIN
      xx = entries.xcen
      ii = WHERE(STRPOS(xx, ',') NE -1)
      IF ii(0) GE 0 THEN BEGIN
         xx_1 = xx(ii)
         nl = N_ELEMENTS(xx_1)
         FOR i=0, nl-1 DO BEGIN
            np = np+N_ELEMENTS(str_index(xx_1(i), ','))+1
         ENDFOR
         np = np-nl
      ENDIF
   ENDIF

   PRINTF, unit, '<tr><td align=center rowspan='+STRTRIM(STRING(n+np+1), 2)+$
      '>'+title+'</td></tr>'
   FOR i=0, n-1 DO BEGIN
      PRINTF, unit, '<tr align=center>'
      entry = entries(i)
      itime = get_plan_itime(entry, err=err)
      dur = sec2dhms(LONG(entry.(itime+1)-entry.(itime)) > 0)
      tstart = STRMID(anytim2utc(entry.(itime), /ecs, /trun), 2, 20)
      tstop = STRMID(anytim2utc(entry.(itime+1), /ecs, /trun), 2, 20)
      prog = 'N/A'
      object = ''
      cmp_no = STRTRIM(STRING(entry.cmp_no),2)
      IF cmp_no NE '0' THEN $
         cmp_no = "<a href='http://sohodb.nascom.nasa.gov/cgi-bin/soho_campaign_display/"+cmp_no+"'>"+cmp_no+"</a>"
      npoint = 1
      IF tag_exist(entry, 'SCI_OBJ') THEN BEGIN
         prog = STRTRIM(STRMID(entry.sci_obj, 0, 40), 2)
         err = ''
         get_object, entry.object, obj_stc, err=err
         IF err EQ '' THEN BEGIN
            object = STRTRIM(obj_stc.object_desc, 2)
            IF object EQ '' THEN object = 'N/A'
         ENDIF ELSE object = 'N/A'
         list = [prog, cmp_no, tstart, tstop, dur, object]
         IF (STRPOS(entry.xcen, ','))(0) NE -1 THEN BEGIN
;---------------------------------------------------------------------------
;           The current entry has more than one pointing values
;---------------------------------------------------------------------------
            xcen = STRTRIM(str2arr(entry.xcen, ','),2)
            ycen = STRTRIM(str2arr(entry.ycen, ','),2)
            npoint = N_ELEMENTS(xcen)
            list_pt = [[xcen], [ycen]]
         ENDIF ELSE BEGIN
            IF (STRPOS(entry.xcen, '.'))(0) NE -1 THEN BEGIN
               xcen = STRTRIM(STRING(entry.xcen, format='(f15.1)'), 2)
               ycen = STRTRIM(STRING(entry.ycen, format='(f15.1)'), 2)
            ENDIF ELSE BEGIN
               xcen = STRTRIM(entry.xcen, 2)
               ycen = STRTRIM(entry.ycen, 2)
               IF xcen EQ '' THEN xcen = 'N/A'
               IF ycen EQ '' THEN ycen = 'N/A'
            ENDELSE
            list = [list, xcen, ycen]
         ENDELSE
      ENDIF
      IF tag_exist(entry, 'IXWIDTH') THEN BEGIN
         IF (STRPOS(entry.ixwidth, ','))(0) NE -1 THEN BEGIN
            width = STRTRIM(str2arr(entry.ixwidth, ','),2)
            height = STRTRIM(str2arr(entry.iywidth, ','),2)
            tmp = N_ELEMENTS(width)
            IF tmp LT npoint THEN npoint = tmp
            width = width(0:npoint-1)
            height = height(0:npoint-1)
            list_pt = [[list_pt(0:npoint-1,*)], [width], [height]]
         ENDIF ELSE BEGIN
            IF (STRPOS(entry.ixwidth, '.'))(0) NE -1 THEN BEGIN
               width = STRTRIM(STRING(entry.ixwidth, format='(f15.1)'), 2)
               height = STRTRIM(STRING(entry.iywidth, format='(f15.1)'), 2)
            ENDIF ELSE BEGIN
               width = STRTRIM(entry.ixwidth, 2)
               height = STRTRIM(entry.iywidth, 2)
               IF width EQ '' THEN width = 'N/A'
               IF height EQ '' THEN height = 'N/A'
            ENDELSE
            list = [list, width, height]
         ENDELSE
      ENDIF
      IF npoint GT 1 THEN BEGIN
;---------------------------------------------------------------------------
;        Deal with multi-pointing entries
;---------------------------------------------------------------------------
         sr = STRTRIM(STRING(npoint), 2)
         PRINTF, unit, '<td rowspan='+sr+'>'+$
            arr2str(list, delimit='</td><td rowspan='+sr+'>')+'</td>'
         PRINTF, unit,'<td>'+arr2str(TRANSPOSE(list_pt(0,*)), $
                                     delimit='</td><td>')+'</td></tr>'
         FOR j=1, npoint-1 DO BEGIN
            PRINTF, unit, '<tr align=center>'
            PRINTF, unit,'<td>'+arr2str(TRANSPOSE(list_pt(j,*)), $
                                        delimit='</td><td>')+'</td>'
         ENDFOR
      ENDIF ELSE $
         PRINTF, unit, '<td>'+arr2str(list, delimit='</td><td>')+'</td>'

      PRINTF, unit, '</tr>'
      first_done = 1
   ENDFOR
END

PRO mk_plan_html, pdate, dir=dir, tomorrow=tomorrow, yesterday=yesterday, $
                  verbose=verbose,err=err
   COMMON plan_html, first_done
   ON_ERROR, 2

   first_done = 0
   IF N_ELEMENTS(dir) EQ 0 THEN dir = '/tmp'

   get_utc, curr_time
   IF N_ELEMENTS(pdate) EQ 0 THEN BEGIN
      pdate = curr_time
      IF KEYWORD_SET(tomorrow) THEN BEGIN
         pdate.mjd = pdate.mjd+1L
         file = 'soho_tomorrow.html'
      ENDIF ELSE IF KEYWORD_SET(yesterday) THEN BEGIN
         pdate.mjd = pdate.mjd-1L
         file = 'soho_yesterday.html'
      ENDIF ELSE file = 'soho_today.html'
   ENDIF ELSE BEGIN
      pdate = anytim2utc(pdate)
      file = 'soho_plan.html'
   ENDELSE
   ffile = concat_dir(dir, file)
   if not test_open(ffile,/write,err=err) then return

   date_str = anytim2utc(pdate, /ecs, /date)
   tstart = utc2tai(anytim2utc(date_str))
   dur = 86400.d0
   tstop = tstart + dur

;---------------------------------------------------------------------------
;  Set up output file, etc.
;---------------------------------------------------------------------------

   OPENW, unit, ffile, /GET_LUN
   PRINTF, unit, ''
   PRINTF, unit, '<html>'
   PRINTF, unit, '<!--'
   PRINTF, unit, '--------------------------------------------------------'
   PRINTF, unit, 'Document name: '+file
   PRINTF, unit, 'Created by:    mk_plan_html.pro
   PRINTF, unit, '--------------------------------------------------------'
   PRINTF, unit, '-->'
   PRINTF, unit, '<head>'
   PRINTF, unit, '<title>SOHO Operations Schedule For '+date_str+'</title>'
   PRINTF, unit, '</head>'
   PRINTF, unit, '<body link=#23238E vlink=#23238E bgcolor=#a0eed0>'
   PRINTF, unit, '<center><h1>SOHO Operations Schedule For '+date_str+'</h1></center>'

;---------------------------------------------------------------------------
;  Read in detail plan entries
;---------------------------------------------------------------------------
   list_soho_det, tstart, tstop, plan, ndobs

   IF ndobs NE 0 THEN BEGIN
      PRINTF, unit, '<table border=3>'
      PRINTF, unit, '<caption align=top><b>(Updated: '+$
         anytim2utc(curr_time, /ecs, /truncate)+$
         ' UT)</caption></b>'

      html_table_header, unit
      itime = get_plan_itime(plan, err=err)
      IF itime LT 0 THEN RETURN

      inst = get_soho_inst(/short)
      ninst = N_ELEMENTS(inst)

      FOR j=0, ninst-1 DO BEGIN
         name_inst = get_soho_inst(inst(j))
         ii = WHERE(plan.instrume EQ inst(j), nplans)
         IF nplans GT 0 THEN BEGIN
            plans = plan(ii)
            CASE (name_inst) OF
               'CELIAS': inst_str = 'JOP'
               'SWAN': inst_str = 'SOC'
               'ERNE': inst_str = 'CAMPAIGN'
               ELSE: inst_str = name_inst
            ENDCASE
            iname = inst_str
            html_items, unit, iname, plans
         ENDIF
      ENDFOR
      PRINTF, unit, '</table><p>'
   ENDIF ELSE BEGIN
      PRINTF, unit, '<hr>Plans for '+date_str+' are yet to be made.<p>'
   ENDELSE

   PRINTF, unit, '<hr><address>
   PRINTF, unit, '<B>Author:</B> <br>
   PRINTF, unit, '<a href="http://orpheus.nascom.nasa.gov/~lwang">Liyun Wang</a>, Code 682.3 '
   PRINTF, unit, '(<a href="mailto:Liyun.Wang.1@gsfc.nasa.gov">'+$
      'Liyun.Wang.1@gsfc.nasa.gov</a>)<p>'
   PRINTF, unit, ''
   PRINTF, unit, '<B>Responsible NASA Official:</B><br>'
   PRINTF, unit, 'Arthur I. Poland, Code 682.1'
   PRINTF, unit, '(<a href="mailto:Arthur.I.Poland.1@gsfc.nasa.gov">'+$
      'Arthur.I.Poland.1@gsfc.nasa.gov</a>)<p>'
   PRINTF, unit, ''
   PRINTF, unit, '<B>Created:</B> '+anytim2utc(curr_time, /ecs, /trun)+' UT<p>'
   PRINTF, unit, '</address>'
   PRINTF, unit, ''
   PRINTF, unit, '</body>'
   PRINTF, unit, '</html>'
   CLOSE, unit
   FREE_LUN, unit
   IF KEYWORD_SET(verbose) THEN PRINT, 'File '+ffile+' created.'
   RETURN
END

;---------------------------------------------------------------------------
; End of 'mk_plan_html.pro'.
;---------------------------------------------------------------------------
