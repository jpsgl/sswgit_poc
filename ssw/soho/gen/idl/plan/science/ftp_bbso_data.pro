;+
; Project     : SOHO-CDS
;
; Name        : FTP_BBSO_DATA
;
; Purpose     : FTP BBSO H-alpha files for a given date
;
; Category    : planning
;
; Explanation : FTP's BBSO Full-Disk H-alpha JPEG files to /tmp
;               and renames them to SOHO convention
;
; Syntax      : files=ftp_bbso_data(date)
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: FILES = found and renamed filenames
;
; Keywords    : OUTDIR = output directory for file [def = current]
;               ERR = error string
;               COUNT = no of files copied
;               CLOBBER= set to clobber previously copied files
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 14 May 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function ftp_bbso_data,date,count=count,err=err,_extra=extra,loud=loud,$
                      outdir=outdir,clobber=clobber
on_error,1
err=''
count=0
files=''

server='ftp.bbso.njit.edu'
get_dir='pub/fulldisk/halpha

;-- default to current date

err=''
hdate=anytim2utc(date,err=err)
if err ne '' then get_utc,hdate
hcode=date_code(hdate)

;-- construct filenames to copy

year=strmid(hcode,2,2)
month=fix(strmid(hcode,4,2))
day=strmid(hcode,6,2)

months  = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c' ]
get_files='hd'+months(month-1)+day+year+'r.jpg'

dprint,'% get_files: ',get_files

spawn,'rm -f /tmp/'+get_files
smart_ftp,server,get_files,get_dir,files=files,count=count,$
          loud=loud,err=err,outdir='/tmp',_extra=extra

if err ne '' then return,files

if keyword_set(loud) then begin
 if count eq 0 then message,'No files found for '+anytim2utc(hdate,/date,/vms),/cont
endif

;-- rename files and move to desired directory

clobber=keyword_set(clobber)
if count gt 0 then begin
 for i=0,count-1 do begin
  break_file,files(i),fdsk,fdir,fname,ext
  imonth=strmid(fname,2,1)
  chk=where(imonth eq months,ic)
  if ic gt 0 then month=string(chk(0)+1,format='(i2.2)') else month='??'
  day=strmid(fname,3,2)
  year='19'+strmid(fname,5,2)
  new_file=concat_dir(outdir,'bbso_halph_fd_'+year+month+day+'_0000')+ext
  chk=loc_file(new_file,count=fcount)
  if (fcount eq 0) or clobber then spawn,'mv -f '+files(i)+' '+new_file
  files(i)=new_file 
 endfor
endif
return,files & end

