;+
; Project     : SOHO-CDS
;
; Name        : GET_NOAA
;
; Purpose     : get latest NOAA NEUTRAL-line GIF from EIT MAC
;
; Category    : planning
;
; Explanation :
;
; Syntax      : get_noaa,file,date
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: FILE = found filename
;
; Keywords    : OUT_DIR = output directory for file [def = current]
;               ERR = error string
;               COUNT = no fo files found
;               BACK= no of days back to look
;               GDATE = actual date of found GIF file
;
; Common      : None
;
; Restrictions: Unix only (with FTP access to MAC)
;
; Side effects: None
;
; History     : Written 18 August 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

pro get_noaa,file,date,err=err,out_dir=out_dir,count=count,$
  quiet=quiet,back=back,gdate=gdate,alert=alert

count=0 & err='' & file=''
if not exist(back) then back=0
verbose=1-keyword_set(quiet)

;-- UNIX?

if strupcase(os_family()) ne 'UNIX' then begin
 err='Sorry, UNIX systems only'
 message,err,/cont & return
endif

;-- check output directory

cd,curr=cdir
if datatype(out_dir) eq 'STR' then begin
 chk1=chk_dir(out_dir)
 if not chk1 then begin
  err='invalid output directory'
  message,err,/cont
  return
 endif
endif else out_dir=cdir

if not test_open(out_dir,err=err,/write) then begin
 message,err+' to '+out_dir,/cont
 return
endif

;-- check if EITMAC is alive and well
;-- but first kill any zombie ftp processes 

kill_job,'eitmac',verb=verbose

server='eitmac.nascom.nasa.gov'
check_ftp,server,alive,err=err,quiet=quiet
if not alive then begin
 if keyword_set(alert) then send_mail,array=err,add='zarro@smmdac'
 return
endif

;-- what date?

err=''
get_utc,cur_date
cdate=anytim2utc(date,err=err)
if err ne '' then cdate=cur_date
cdate.time=0

;-- construct input file for FTP

espawn,'which ncftp2',out
if (strpos(out(0),'not found') gt -1) or (trim(out(0)) eq '') then ftp='ftp' else ftp='ncftp2'
ftp='ftp'

ngif='neutral_'+trim(date_code(anytim2utc(cdate,/ecs,/date)))+'.gif'
if verbose then message,'retrieving NEUTRAL-line GIF file: '+ngif,/cont
rdate=date_code(cdate)
ftp_com=concat_dir(getenv('HOME'),'ftp.com')
get_com='get '
openw,unit,ftp_com,/get_lun
printf,unit,'bi'
printf,unit,'lcd '+out_dir
printf,unit,'cd ../Trillian/neutral'
printf,unit,get_com+ngif
printf,unit,'quit'
free_lun,unit

;-- pipe to FTP

cmd=ftp+' '+server+' < '+ftp_com
print,'% '+cmd
espawn,cmd,out

;-- found files?

fgif=concat_dir(out_dir,ngif)
chk=loc_file(fgif,count=count)

gerr1='No NEUTRAL-line GIF file found for: '+anytim2utc(cdate,/ecs,/date)
gerr2='Invalid NEUTRAL-line GIF file found for: '+anytim2utc(cdate,/ecs,/date)
if count eq 0 then begin
 err=gerr1
 message,err,/cont
 if exist(back) then begin
  if (back gt 0) then begin
   sdate=cdate
   sdate.mjd=sdate.mjd-1
   sback=back-1
   message,'checking back one day',/cont
   get_noaa,file,sdate,err=err,out_dir=out_dir,count=count,$
    quiet=quiet,back=sback,gdate=gdate
  endif
 endif
endif else begin
 file=chk(0)
 if not valid_gif(file) then begin 
  err=gerr2
  message,err,/cont
 endif else begin
  err=''
  gdate=anytim2utc(cdate,/vms,/date)
  message,'found NOAA file on '+gdate,/cont
 endelse
endelse

rm_file,ftp_com
return & end

