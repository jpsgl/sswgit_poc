;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_CLONE
;
; Purpose     :	clone a plan from one OPS day to another
;
; Explanation :
;
; Use         :	MK_PLAN_CLONE,OPS1,OPS2
;
; Inputs      :	OPS1, OPS2 = source and destination OPS days to
;               clone from and to, respectively
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	
;               TYPE = 0,1,2,3 for DETAILS, FLAG, SCIENCE, and ALT  
;               (default is SCIENCE database)
;               DETAILS = set for DETAILS
;               FLAG = set for FLAG
;               SCIENCE = set for SCIENCE
;               INST = instrument name 
;               BETWEEN = clone all days between OPS1 and OPS2
;               DAYS = days to clone from starting day [def = all days]
;               TGAP = secs time gap between adjoining plans [def = 1.d-3]
;
; Common      :	MK_PLAN_SHARED (needed to communicate with MK_PLAN/MK_SOHO)
;
; Restrictions:	None.
;
; Side effects:	entire plans for days from OPS to OPS2 are modified
;
; Category    :	Planning, Database.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC),  1 May 1995
;-

pro mk_plan_clone,ops1,ops2,type=type,err=err,instrument=instrument,$
              details=details,flag=flag,science=science,alt=alt,days=days,$
              between=between,verbose=verbose,tgap=tgap

@mk_plan_shared

on_error,1
err=''
verb=keyword_set(verbose)

;-- check priviledge for CDS users

if which_inst() eq 'C' then begin
 status=call_function('priv_zdbase',/daily,/quiet,err=err)
 if not status then return
endif

;-- data type to clone [def = 2 for SCIENCE]

if n_elements(type) eq 0 then begin
 case 1 of
  keyword_set(details): type= 0
  keyword_set(flag): type= 1
  keyword_set(science): type= 2
  keyword_set(alt): type=3
  else: type=2
 endcase
endif

;-- which instrument?  (must enter one for SCIENCE plans)

if datatype(instrument) eq 'STR' then cinst=get_soho_inst(instrument,/short) else $
 cinst=which_inst()

if (type ne 2) and ( (cinst ne 'S') and (cinst ne 'C') ) then begin
 err='Cannot clone this DB type for requested instrument'
 if verb then message,err,/cont & return
endif

;-- figure out which days to clone

day_secs=3600.d*24.d
if exist(days) then tdays=days else tdays=1

;-- start day

if exist(ops1) then begin
 if datatype(ops1) eq 'FLO' then ops1=double(ops1)
 tstart=anytim2utc(ops1)
 tstart.time=0 & tstart=utc2tai(tstart)
endif else begin
 message,'syntax --> MK_PLAN_CLONE,OPS1,OPS2,[TYPE=TYPE, DAYS=DAYS]',/contin
 return
endelse

;-- stop day

if exist(ops2) then begin
 if datatype(ops2) eq 'FLO' then ops2=double(ops2)
 tstop=anytim2utc(ops2)
 tstop.time=0 & tstop=utc2tai(tstop)
 tdays=(tstop-tstart)/day_secs
endif else begin
 tstop=tstart+tdays*day_secs
endelse

if tstart eq tstop then begin
 err='No point cloning the same day'
 if verb then message,err,/contin & return
endif

if not mk_plan_priv(tstop) then begin
 err='Cannot clone plans before current UT'
 if verb then message,err,/contin & return
endif

if verb then begin
 message,'cloning DB type: '+string(type),/cont
 message,'cloning INST type: '+cinst,/cont,/noname
 message,'cloning '+string(tdays,'(i4)')+' days to '+tai2utc(tstop,/ecs),/cont,/noname
 message,'reading source plans...',/cont,/noname
endif 

;-- first get plans for source day

rd_plan,splans,tstart,tstart+day_secs,nobs=nobs,inst=cinst,type=type,/force,/quiet
if nobs eq 0 then begin
 get_instrument,cinst,desc
 err='No '+desc.name+' '+get_plan_def(type)+' plans on source day'
 message,err,/contin & return
endif

;-- crop off plans that fall outside source day

itime=get_plan_itime(splans)
if n_elements(tgap) eq 0 then tgap=1.d-3
if splans(0).(itime) le tstart then splans(0).(itime)=tstart+tgap
if splans(nobs-1).(itime+1) ge (tstart+day_secs) then $
  splans(nobs-1).(itime+1)=tstart+day_secs-tgap

;-- get appropriate DB Delete and Add functions

add_funct=get_plan_funct(type,/add)
del_funct=get_plan_funct(type,/del)

;-- next list plans for target day
;-- if /BETWEEN is set then all days between OPS1 and including OPS2 will be 
;   cloned with plans from OPS1.

between=keyword_set(between)
tnext=tstart
if tstop lt tstart then sign=-1 else sign=1
repeat begin
 if between then tnext=tnext+sign*day_secs else tnext=tstop
 rd_plan,tplans,tnext,tnext+day_secs,nobs=nobs,inst=cinst,type=type,/force,/quiet

 tdiff=tnext-tstart
 nplans=splans
 nplans.(itime)=nplans.(itime)+tdiff
 nplans.(itime+1)=nplans.(itime+1)+tdiff
 
;-- delete all target plans

 dprint,tdiff/day_secs
 if nobs ne 0 then begin
  dprint,'%MK_PLAN_CLONE: deleting target plans...'
  for i=0,nobs-1 do begin
   if (type eq 2) and (which_inst() ne 'S') then begin
    inst=tplans(0).instrume
    status=call_function(del_funct,tplans(i).(itime),err=err,inst=inst)
   endif else begin
    status=call_function(del_funct,tplans(i).(itime),err=err)
   endelse
  endfor

;--- prepend and append plans that fall outside target day

  if tplans(0).(itime) le tnext then begin
   first_plan=tplans(0) & first_plan.(itime+1)=tnext-tgap
   if abs(first_plan.(itime+1)-first_plan.(itime)) ge tgap then $
    nplans=concat_struct(first_plan,nplans)
  endif

  if tplans(nobs-1).(itime+1) ge (tnext+day_secs) then begin
   last_plan=tplans(nobs-1) & last_plan.(itime)=tnext+day_secs
   if type eq 0 then last_plan.time_tagged=1
   if abs(last_plan.(itime+1)-last_plan.(itime)) ge tgap then $
    nplans=concat_struct(nplans,last_plan)
  endif

 endif

;-- actual cloning is done here

 dprint,'%MK_PLAN_CLONE: cloning source plans...'
 if (type eq 2) and (which_inst() ne 'S') then begin
  inst=nplans(0).instrume
  status=call_function(add_funct,nplans,err=err,inst=inst) 
 endif else begin
  status=call_function(add_funct,nplans,err=err)
 endelse

endrep until (tnext eq tstop)

;-- update MK_PLAN_SHARED 

if exist(startdis) and exist(stopdis) then begin
 rd_plan,dplans,startdis,stopdis,type=type,inst=cinst,nobs=nobs,/quiet,/day
 mk_plan_load,type,dplans,inst=cinst
endif

message,'all done',/contin

return & end

