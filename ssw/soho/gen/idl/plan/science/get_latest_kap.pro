;+
; Project     : SOHO - CDS
;
; Name        : GET_LATEST_KAP
;
; Purpose     : find most recently created KAP file for a given date
;
; Category    : operations, planning
;
; Explanation :
;
; Syntax      : IDL> latest_kap_file=get_latest_kap_file(cur_date)
;
; Inputs      : CUR_DATE = date to search for
;
; Opt. Inputs : None
;
; Outputs     : LATEST_KAP_FILE = most recently created KAP file 
;
; Opt. Outputs: 
;
; Keywords    : CREATE = creation date of file
;               PATH   = path to search [def = SOHO_EAP]
;               PREFIX = file prefix to search [def = ECS]
;               SUFFIX = file suffix to search [def = KAP]
;               STATUS = 1/0 if success/failure
;               ERR = error messages (blank if none)
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Version 1,  26-June-1996,  D.M. Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-



function get_latest_kap,cur_date,create=create,status=status,path=path,$
                        prefix=prefix,err=err,suffix=suffix

vms=os_family() eq 'vms'
err=''
status=0 & create='' & latest_kap_file=''
if datatype(prefix) ne 'STR' then prefix='ECS' else prefix=trim(prefix)
if datatype(suffix) ne 'STR' then suffix='KAP' else suffix=trim(suffix)
if datatype(path) ne 'STR' then path = getenv('SOHO_EAP')
ymd = date_code(cur_date)
wild=concat_dir(path,'*'+prefix+ymd+'*.'+suffix)
kap_files=loc_file(wild,count=fcount)
dprint,'% GET_LATEST_KAP: checking '+ymd

;-- search for DATE_CRE field in KAP files

key='DATE_CRE='
key_len=strlen(key)

if fcount gt 0 then begin
 if vms then cmd='search '+wild+' key' else cmd='grep -i '+key+' '+wild
 spawn,cmd,out,count=scount
 if scount gt 0 then begin
  cpos=strpos(out,key)
  clook=where(cpos gt -1,count)
  if count gt 0 then begin
   out=out(clook)
   cpos=cpos(clook)
   files=kap_files(clook)
   dcreate=strarr(count)
   for k=0,count-1 do dcreate(k)=strmid(out(k),cpos(k)+key_len,strlen(out(k)))
   dcreate=trim(dcreate)
   dmax=max(anytim2tai(dcreate),ii)
   latest_kap_file=files(ii)
   create=anytim2utc(dcreate(ii),/ecs)
   status=1
  endif else err='no creation date in file on '+ymd
 endif else err='cannot read file on '+ymd
endif else err='cannot find files for '+ymd

if err ne '' then begin
 message,err,/cont
 err='% GET_LATEST_KAP:'+err
endif

return,latest_kap_file & end

