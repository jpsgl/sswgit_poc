;+
; Project     :	SOHO - CDS
;
; Name        :	GET_PLAN_TYPE
;
; Purpose     : Get type of plan structure
;
; Use         : TYPE=GET_PLAN_TYPE(PLAN)
;
; Inputs      : PLAN = plan structure definition
;                            OR
;                      string definition
;
; Opt. Inputs : None.
;
; Outputs     : TYPE = 0/1/2/3/4/5 for
;                      DETAILS/FLAG/SCIENCE/ALT/SOHO_DETAILS/RESOURCES
;                    = -1 if invalid
;
; Opt. Outputs: None.
;
; Keywords    : INST = instrument type ('C', 'S', for CDS, SUMER, etc)
;
; Explanation : None.
;
; Calls       : None.
;
; Common      : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Planning
;
; Prev. Hist. : None.
;
; Written     :	Version 1, Zarro (ARC/GSFC) 29 January 1995.
;-


function get_plan_type,plan,inst=inst,err=err
                  
on_error,1

err=''
err_mess='GET_PLAN_TYPE: unsupported input plan type'
type=-1 & inst='' 

if not exist(plan) then return,type

;-- check for string input

if datatype(plan) eq 'STR' then begin
 ptype=strupcase(plan)
 case 1 of
  strpos(ptype,'DET') gt -1 : type=0
  strpos(ptype,'FLA') gt -1 : type=1
  strpos(ptype,'SCI') gt -1 : type=2
  strpos(ptype,'ALT') gt -1 : type=3
  strpos(ptype,'SOH') gt -1 : type=4
  strpos(ptype,'RES') gt -1 : type=5
 else: err=err_mess
 endcase
endif

;-- check for structure input

if datatype(plan) eq 'STC' then begin
 if not tag_exist(plan,'STRUCT_TYPE') then begin
  if not tag_exist(plan,'RES_NAME') then serr=err_mess else type=5
 endif else begin                                             
  struct_type=strupcase(strtrim(plan(0).(0),2))
  if strpos(struct_type,'SOHO') gt -1 then name=strtrim(plan(0).instrume,2) else $
   name=strtrim(struct_type,2)
  inst=strmid(name,0,1)
  get_instrument,inst,desc

  tags=tag_names(plan)
  case 1 of

   strpos(struct_type,'DETAIL') gt -1: type=0

   strpos(struct_type,'FLAG') gt -1  : type=1

   strpos(struct_type,'SOHO-PLAN') gt -1: type=2

   strpos(struct_type,'ALT') gt -1: type=3

   strpos(struct_type,'SOHO-DET') gt -1: type=4

   else: err=err_mess+' '+struct_type
  endcase
 endelse
endif

return,type & end


