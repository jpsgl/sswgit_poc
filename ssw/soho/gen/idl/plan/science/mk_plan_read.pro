;+
; Project     : SOHO - CDS
;
; Name        : MK_PLAN_READ
;
; Purpose     : general reader for PLAN Database
;
; Category    : Planning
;
; Explanation :
;
; Syntax      : IDL> mk_plan_read,info
;
; Examples    :
;
; Inputs      : INFO - Info structure that should have at least the following
;                      required tags:
;               STARTDIS, STOPDIS - the selected TAI start and stop times
;               Req_ITEMS  - string array of selected DB items
;               DTYPE      - SOHO plan type to show. 0: SCI, 1: DET
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : TIME_CHANGE - 1 if INFO.STARTDIS/STOPDIS differs from
;                             values in MK_PLAN_SHARED.
;               FORCE - force a reread  of the DB
;               NO_MDI - skip reading MDI
;               NO_TAPE - skip reading tape dump
;               NO_TYPE - plan type to skip reading
;
; Common      : MK_PLAN_SHARED - shared variables with MK_SOHO, MK_CDS_PLAN
;               MK_PLAN_READ - useful database flags
;
; Restrictions:
;
; Side effects: STARTDIS and STOPDIS in MK_PLAN_SHARED updated by values
;               in INFO
;
; History     : Version 1,  1-Jul-1995,  D M Zarro.  Written
;               Version 2, January 23, 1996, Liyun Wang, GSFC/ARC
;                  Added NO_X keyword
;               Version 3, January 27, 1996, D, Zarro (ARC/GSFC)
;                  Sped up reading of resource DB
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

pro mk_plan_read, info, force=force, time_change=time_change, $
                  no_mdi=no_mdi,no_tape=no_tape,no_type=no_type,_extra=extra

@mk_plan_shared

common mk_plan_read,sav_db,dsn_tried,nrt_tried,mdi_h_tried,mdi_m_tried,$
       support_tried,tnorcr_tried,trcr_tried,svm_tried,tape_tried,dlyd_tried,$
       soho_det_tried

;-- Set these flags whenever resource DB is read.
;   Don't read DB again unless /TIME_CHANGE or /FORCE  is set

if not exist(dsn_tried) then dsn_tried=0
if not exist(nrt_tried) then nrt_tried=0
if not exist(mdi_h_tried) then mdi_h_tried=0
if not exist(mdi_m_tried) then mdi_m_tried=0
if not exist(support_tried) then support_tried=0
if not exist(tape_tried) then tape_tried=0
if not exist(trcr_tried) then trcr_tried=0
if not exist(tnorcr_tried) then tnorcr_tried=0
if not exist(svm_tried) then svm_tried=0
if not exist(dlyd_tried) then dlyd_tried=0
if not exist(soho_det_tried) then soho_det_tried=0

   
force = keyword_set(force)

;---------------------------------------------------------------------------
;  Did database change?
;---------------------------------------------------------------------------

   if  datatype(sav_db) ne 'STR' then sav_db = getenv('ZDBASE')
   cur_db = getenv('ZDBASE')
   if  cur_db ne sav_db then begin
      message, 'ZDBASE changed!!!', /contin
      force = 1 & sav_db=cur_db
   endif 

;---------------------------------------------------------------------------
;  Did time interval change?
;---------------------------------------------------------------------------

   exist_times=exist(startdis) and exist(stopdis)
   if exist_times then $
    time_change = (info.startdis ne startdis) or (info.stopdis ne stopdis) else $
     time_change=1

   if  time_change then begin
    dprint, '%MK_PLAN_READ: time interval has changed'
    startdis = info.startdis & stopdis=info.stopdis
   endif 

   inst = which_inst()
   req_items = info.req_items
   dprint, '%Requested DB items: ', req_items
   should_read = (force or time_change)

;-- which plan type to skip
   
   if not exist(no_type) then no_type=-1
   if no_type gt -1 then dprint,'% MK_PLAN_READ: skipping type '+trim(string(no_type))

;---------------------------------------------------------------------------
;  DETAILS Plans
;---------------------------------------------------------------------------

   if (no_type ne 0) then begin
    if (grep('DET', req_items))(0) ne '' then begin
     if inst eq 'C' then begin
      if should_read or (not exist(cds_detail)) then $
       rd_plan, cds_detail, startdis, stopdis, type=0, force=force,/day
     endif 
     if inst eq 'S' then begin
      if should_read or (not exist(sumer_detail)) then $
       rd_plan, sumer_detail, startdis, stopdis, type=0, force=force,/day
     endif 
    endif 
   endif

;---------------------------------------------------------------------------
;  FLAG Plans (CDS only)
;---------------------------------------------------------------------------

   if (no_type ne 1) then begin
    if  (grep('FLA', req_items))(0) ne '' then begin
     if should_read or (not exist(cds_flag)) then $
      rd_plan, cds_flag, startdis, stopdis, type=1, force=force,/day
    endif 
   endif

;---------------------------------------------------------------------------
;  SCIENCE Plans
;---------------------------------------------------------------------------

   if (no_type ne 2) then begin
    if (grep('SCI', req_items))(0) ne '' then begin
     if inst eq 'C' then begin
      if should_read or (not exist(cds_science)) then $
       rd_plan, cds_science, startdis, stopdis, type=2, force=force,/day
     endif 
     if inst eq 'S' then begin
      if should_read or (not exist(sumer_science)) then $
        rd_plan, sumer_science, startdis, stopdis, type=2,force=force,/day
     endif 
    endif
   endif 

;---------------------------------------------------------------------------
;  ALT Plans (CDS only)
;---------------------------------------------------------------------------

   if (no_type ne 3) then begin
    if (grep('ALT', req_items))(0) ne '' then begin
     if should_read or (not exist(cds_alt)) then $
      rd_plan, cds_alt, startdis, stopdis, type=3, force=force,/day
    endif 
   endif

;---------------------------------------------------------------------------
;  Telemetry row data
;---------------------------------------------------------------------------

   if (grep('Telemetry', req_items))(0) ne '' then begin

;    if should_read or (not nrt_tried) then begin
;     rd_resource, nrt, startdis, stopdis, /nrt, force=force,/day
;     nrt_tried=1
;    endif
   
    if not keyword_set(no_mdi) then begin
     if should_read or (not mdi_m_tried) then begin
      rd_resource, mdi_m, startdis, stopdis, /mdi, force=force,/day
      mdi_m_tried=1
     endif
    
     if should_read or (not mdi_h_tried) then begin
      rd_resource, mdi_h, startdis, stopdis, /mdi_h, force=force,/day
      mdi_h_tried=1
     endif
    endif

    if (not keyword_set(no_tape)) and (should_read or (not tape_tried)) then begin
     rd_resource, tape_dump, startdis, stopdis, /tape, force=force,/day
     tape_tried=1
    endif

   endif 

;---------------------------------------------------------------------------
;  Commanding row data
;---------------------------------------------------------------------------

   if (grep('Command', req_items))(0) ne '' then begin

;    if should_read or (not dlyd_tried) then begin
;     rd_resource, dlydtimes, startdis, stopdis, /dlyd, force=force,/day
;     dlyd_tried=1
;    endif 

    if should_read or (not dsn_tried) then begin
     rd_resource, dsn, startdis, stopdis, /dsn, force=force,/day
     dsn_tried=1
    endif

    if should_read or (not svm_tried) then begin
     rd_resource, svm, startdis, stopdis, /svm, force=force,/day
     svm_tried=1
    endif


    if should_read or (not trcr_tried) then begin
     rd_resource, trcr, startdis, stopdis, /_rcr, force=force,/day
     trcr_tried=1
    endif

;    if should_read or (not tnorcr_tried) then begin
;     rd_resource, tnorcr, startdis, stopdis, /_norcr, force=force,/day
;     tnorcr_tried=1
;    endif

   endif 

;---------------------------------------------------------------------------
;  Ground support
;---------------------------------------------------------------------------

   if  (grep('Support', req_items))(0)  ne '' then begin
    if should_read or (not support_tried) then begin
     rd_resource, support, startdis, stopdis, /sup, force=force,/day
     support_tried=1
    endif
   endif 

;---------------------------------------------------------------------------
;  Read Sci or det plans for other instruments
;---------------------------------------------------------------------------

   check=where_vector(req_items,get_soho_inst(),/trim,count)
   read_sci=(count gt 0)

;-- don't read unless specifically requested 
;   (CDS_SCI, CDS_DET, SUMER_SCI, SUMER_DET already handled above)

   if read_sci then begin

    if (info.dtype eq 0) then begin
     if should_read or (not exist(soho_splan)) then begin
      list_plan, startdis, stopdis, soho_splan, nsobs
      if nsobs gt 0 then begin
       soho_splan.struct_type = 'SOHO-PLAN' 
       print, '% MK_PLAN_READ: Read '+STRTRIM(STRING(nsobs),2)+$
              ' SOHO SCI plans...'
      endif else dprint,'% MK_PLAN_READ: No SOHO SCI plan entries found.'
     endif 
    endif

    if (info.dtype eq 1) then begin
     if  should_read or (not soho_det_tried) then begin
      dprint, '% MK_PLAN_READ:Reading SOHO DET plans...'
      list_soho_det, startdis, stopdis, soho_dplan, ndobs
      soho_det_tried=1
      if  ndobs gt 0 then begin
       soho_dplan.struct_type = 'SOHO-DET' 
       dprint, '% MK_PLAN_READ: Read '+STRTRIM(STRING(ndobs),2)+$
               ' SOHO DET plans...'
      endif else dprint,'% MK_PLAN_READ: No SOHO DET plan entries found.'
     endif
    endif

   endif 

   return & end
