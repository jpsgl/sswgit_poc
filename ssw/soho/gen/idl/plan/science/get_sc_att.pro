	FUNCTION GET_SC_ATT, DATE, TYPE, ERRMSG=ERRMSG, RETAIN=RETAIN,	$
		SUB_DATE=sub_date, FIND_CLOSEST=FIND_CLOSEST
;+
; Project     :	SOHO - CDS
;
; Name        :	GET_SC_ATT()
;
; Purpose     :	Get the SOHO spacecraft attitude.
;
; Category    :	Class3, Orbit
;
; Explanation : Read the definitive or predictive attitude file to get the
;               spacecraft pointing and roll information.  If no file is found,
;               then all parameters are returned as zero, and the type is
;               returned as "Predictive".
;
; Syntax      :	Result = GET_SC_ATT( DATE  [, TYPE ] )
;
; Examples    :	
;
; Inputs      :	DATE	= The date/time value to get the attitude information
;			  for.  Can be in any CDS time format.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a structure containing the
;		spacecraft attitude information.  It contains the following
;		tags:
;
;			SC_AVG_PITCH_ECLIP
;			SC_AVG_ROLL_ECLIP
;			SC_AVG_YAW_ECLIP
;			SC_AVG_PITCH
;			SC_AVG_ROLL
;			SC_AVG_YAW
;			GCI_AVG_PITCH
;			GCI_AVG_ROLL
;			GCI_AVG_YAW
;			GSE_AVG_PITCH
;			GSE_AVG_ROLL
;			GSE_AVG_YAW
;			GSM_AVG_PITCH
;			GSM_AVG_ROLL
;			GSM_AVG_YAW
;			SC_STD_DEV_PITCH
;			SC_STD_DEV_ROLL
;			SC_STD_DEV_YAW
;			SC_MIN_PITCH
;			SC_MIN_ROLL
;			SC_MIN_YAW
;			SC_MAX_PITCH
;			SC_MAX_ROLL
;			SC_MAX_YAW
;
;		All parameters are in radians.
;
; Opt. Outputs:	TYPE	= Returns whether predictive or definitive data was
;			  used to calculate the result.  Returned as either
;			  "Definitive", "PredictFile", or "Predictive".  The
;			  first two are used if a suitable file is found, while
;			  the last is used if no file is found.  A type of
;			  "Predictive" is only suitable prior to the Bogart
;			  mission.
;
; Keywords    :	RETAIN	= If set, then the FITS attitude file will be left
;			  open.  This speeds up subsequent reads.
;
;		FIND_CLOSEST = If set, then the closest attitude file to the
;			  date requested is found.  Only useful for dates prior
;			  to the Bogart mission.
;
;		SUB_DATE= If data from a date other than was called is used,
;			  this keyword returns the date used. Otherwise returns
;			  ''.
;
;		ERRMSG	= If defined and passed, then any error messages will
;			  be returned to the user in this parameter rather than
;			  depending on the MESSAGE routine in IDL.  If no
;			  errors are encountered, then a null string is
;			  returned.  In order to use this feature, ERRMSG must
;			  be defined first, e.g.
;
;				ERRMSG = ''
;				Result = GET_SC_ATT( ERRMSG=ERRMSG, ... )
;				IF ERRMSG NE '' THEN ...
;
; Calls       :	CONCAT_DIR, FXBOPEN, FXBREAD
;
; Common      :	Private common block GET_SC_ATT is used to keep track of the
;		attitude file opened when the RETAIN keyword is used.
;
; Restrictions:	The attitude entries for the time closest to that requested is
;		used to calculate the parameters.  Since the attitude data
;		is calculated every 10 minutes, this should be correct within
;		+/-5 minutes.  No attempt is made to interpolate to closer
;		accuracy than that.
;
; Side effects:	Any data with too much variation (max-min) in the attitude data
;		are rejected.  The limits are one arcminute in pitch and yaw,
;		and one degree in roll.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 04-Dec-1995, William Thompson, GSFC
;		Version 2, 07-Dec-1995, William Thompson, GSFC
;			Returns full attitude file--renamed version 1 to
;			GET_SC_POINT.
;			Added keyword RETAIN
;		Version 3, 12-Dec-1995, William Thompson, GSFC
;			Fixed bug with LAST_FILE not being initialized.
;		Version 4, 24-Jan-1996, William Thompson, GSFC
;			Filter out bad attitude data
;		Version 5, 08-Mar-1996, William Thompson, GSFC
;			Include ecliptic attitude as well as that relative to
;			solar north.
;			Removed keyword OLD.
;		Version 6, 03-Sep-1996, William Thompson, GSFC
;			Also look for filename with a dollar sign
;			prepended--this is how VMS treats uppercase filenames.
;		Version 7, 11-Oct-1996, William Thompson, GSFC
;			Only prepend $ in VMS.
;		Version 8, 22-Jan-1997, William Thompson, GSFC
;			Modified to reflect reorganization of attitude files.
;		Version 9, 30-Jan-1997, William Thompson, GSFC
;			Fixed VMS bug introduced in version 8
;		Version 10, Apr-2000, N Rich, NRL
;			Search first by month for speed; Add SUB_DATE keyword; 
;			Use year if month not found
;		Version 11, 14-Apr-2000, William Thompson, GSFC
;			Added keyword FIND_CLOSEST
;		Version 12, 15-May-2002, N Rich, NRL
;			Fix FIND_CLOSEST bug by moving operation into 
;			'IF COUNT EQ 0 ...' statement
;               Version 13, 01-Nov-2010, William Thompson, GSFC
;                       Look in both definitive and predictive directories
;
; Contact     :	WTHOMPSON
;-
;
	ON_ERROR, 2
	COMMON GET_SC_ATT, LAST_FILE, UNIT, ROWS, TIME
;
;  Make sure that LAST_FILE is defined.
;
	IF N_ELEMENTS(LAST_FILE) EQ 0 THEN LAST_FILE = ''
;
;  Initialize RESULT.  If a definitive attitude file is found, then this will
;  be updated.
;
	RESULT = {SOHO_SC_ATT,			$
		SC_AVG_PITCH_ECLIP:	0.0,	$
		SC_AVG_ROLL_ECLIP:	0.0,	$
		SC_AVG_YAW_ECLIP:	0.0,	$
		SC_AVG_PITCH:		0.0,	$
		SC_AVG_ROLL:		0.0,	$
		SC_AVG_YAW:		0.0,	$
		GCI_AVG_PITCH:		0.0,	$
		GCI_AVG_ROLL:		0.0,	$
		GCI_AVG_YAW:		0.0,	$
		GSE_AVG_PITCH:		0.0,	$
		GSE_AVG_ROLL:		0.0,	$
		GSE_AVG_YAW:		0.0,	$
		GSM_AVG_PITCH:		0.0,	$
		GSM_AVG_ROLL:		0.0,	$
		GSM_AVG_YAW:		0.0,	$
		SC_STD_DEV_PITCH:	0.0,	$
		SC_STD_DEV_ROLL:	0.0,	$
		SC_STD_DEV_YAW:		0.0,	$
		SC_MIN_PITCH:		0.0,	$
		SC_MIN_ROLL:		0.0,	$
		SC_MIN_YAW:		0.0,	$
		SC_MAX_PITCH:		0.0,	$
		SC_MAX_ROLL:		0.0,	$
		SC_MAX_YAW:		0.0}
	TYPE = "Predictive"
	SUB_DATE=''			;

;
;  Check the number of parameters.
;
	IF N_PARAMS() LT 1 THEN BEGIN
		MESSAGE = 'Syntax:  Result = GET_SC_ATT( DATE  [, TYPE ] )'
		GOTO, HANDLE_ERROR
	ENDIF
;
;  Make up to two passes through the software.  In the first pass, look in the
;  top level directory.  In the second pass, if needed, try appending the year
;  to the directory.
;
	USE_YEAR = 0
	TEMP = ANYTIM2UTC(DATE,/EXT)
	S_YEAR = TRIM(TEMP.YEAR)
START_PASS:
;
;  Form the filename for the definitive attitude file.
;
	FILETYPE = "Definitive"
	PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
	PATH = CONCAT_DIR(PATH, 'definitive', /DIR)
	IF USE_YEAR THEN PATH = CONCAT_DIR(PATH, S_YEAR, /DIR)
	NAME = 'SO_AT_DEF_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.FITS'
	FILENAME = CONCAT_DIR(PATH, NAME)
;
;  Look for any files that match the search criteria.
;
	FILES = FINDFILE(FILENAME, COUNT=COUNT)

	IF COUNT EQ 0 and OS_FAMILY() EQ 'vms' THEN BEGIN
		FILENAME = CONCAT_DIR(PATH, '$'+NAME)
		FILES = FINDFILE(FILENAME, COUNT=COUNT)
	ENDIF

; ** NBR, 3/24/99	Find closest file to DATE

	IF COUNT EQ 0 and KEYWORD_SET(FIND_CLOSEST) THEN BEGIN
		targdate = ANYTIM2CAL(DATE,FORM=8,/DATE)
		month = STRMID(targdate,0,6) ; using month to make faster
		avail = FINDFILE(CONCAT_DIR(PATH,'SO_AT_DEF_'+month+'*FITS'),COUNT=COUNT)
		IF avail(0) EQ '' THEN BEGIN		; ** NBR, 2/2000
		    year = STRMID(month,0,4)
		    avail = FINDFILE(CONCAT_DIR(PATH,'SO_AT_DEF_'+year+'*FITS'))
		ENDIF
		IF avail(0) EQ '' THEN gdate = targdate ELSE BEGIN
		    stapos = STRPOS(avail(0),'_V') - 8
		    avail = STRMID(avail,stapos,4) + '-' +	$
			    STRMID(avail,stapos+4,2) + '-' +	$
			    STRMID(avail,stapos+6,2)
		    avail_tai = UTC2TAI(avail)
		    TARGET = ANYTIM2UTC(DATE)
		    date_tai = UTC2TAI(TARGET)
		    diffd = ABS(avail_tai - date_tai)
		    mindifd = MIN(diffd,subs)
		    gdate = anytim2cal(utc2str(tai2utc(avail_tai(subs))),form=8,/date)
		    NAME = 'SO_AT_DEF_' + gdate + '_V*.FITS'
	    ;	NAME = 'SO_AT_DEF_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.FITS'
	    ENDELSE
	    IF gdate NE targdate THEN SUB_DATE=strmid(gdate,0,4)+'-'+	$
		strmid(gdate,4,2)+'-'+strmid(gdate,6,2)
	    FILENAME = CONCAT_DIR(PATH, NAME)
	    FILES = FINDFILE(FILENAME, COUNT=COUNT)
	ENDIF
; **

;
;  No definitive file was found.  Form the filename for the predictive orbit
;  file.
;
        IF COUNT EQ 0 THEN BEGIN
            FILETYPE = "PredictFile"
            PATH = CONCAT_DIR('$ANCIL_DATA', 'attitude', /DIR)
            PATH = CONCAT_DIR(PATH, 'predictive', /DIR)
            IF USE_YEAR THEN PATH = CONCAT_DIR(PATH, S_YEAR, /DIR)
            NAME = 'SO_AT_PRE_' + ANYTIM2CAL(DATE,FORM=8,/DATE) + '_V*.FITS'
            FILENAME = CONCAT_DIR(PATH, NAME)
;
;  Look for any files that match the search criteria.
;
            FILES = FINDFILE(FILENAME, COUNT=COUNT)
            IF COUNT EQ 0 and OS_FAMILY() EQ 'vms' THEN BEGIN
                FILENAME = CONCAT_DIR(PATH, '$'+NAME)
                FILES = FINDFILE(FILENAME, COUNT=COUNT)
            ENDIF
;
;  If not found in the top directory, try a year subdirectory.
;
            IF COUNT EQ 0 THEN BEGIN
                IF USE_YEAR THEN GOTO, FINISH ELSE BEGIN
                    USE_YEAR = 1
                    GOTO, START_PASS
                ENDELSE
            ENDIF
        ENDIF
;
;  A file was found.  Read in the one with the highest version number.
;
	TYPE = FILETYPE
	IF COUNT GT 1 THEN FILES = FILES(REVERSE(SORT(FILES)))
	IF FILES(0) NE LAST_FILE THEN BEGIN
		IF LAST_FILE NE '' THEN FXBCLOSE, UNIT
		FXBOPEN, UNIT, FILES(0), 1
		LAST_FILE = FILES(0)
		break_file,LAST_FILE,dlog,dir,fname,sfx
		print, 'Using ',fname+sfx,' for attitude info for ' +	$
			anytim2utc(date,/ccsds,/date) + '.'
;
;  Read in the year and the time.  Filter out any entries with a zero year, or
;  with too much variation in the attitude data.  Allow up to one degree
;  variation in the roll, and one arcmin variation in the pitch and yaw.
;
		FXBREAD, UNIT, YEAR, 'YEAR'
		FXBREAD, UNIT, TIME, 'ELLAPSED MILLISECONDS OF DAY'
		FXBREAD, UNIT, PMIN, 'SC MIN PITCH'
		FXBREAD, UNIT, RMIN, 'SC MIN ROLL'
		FXBREAD, UNIT, YMIN, 'SC MIN YAW'
		FXBREAD, UNIT, PMAX, 'SC MAX PITCH'
		FXBREAD, UNIT, RMAX, 'SC MAX ROLL'
		FXBREAD, UNIT, YMAX, 'SC MAX YAW'
;
		ROWS = INDGEN(N_ELEMENTS(YEAR)) + 1
		W = WHERE((YEAR NE 0) AND (!RADEG*(RMAX-RMIN) LT 1) AND	$
			(60*!RADEG*(YMAX-YMIN) LT 1) AND	$
			(60*!RADEG*(PMAX-PMIN) LT 1), COUNT)
		IF COUNT EQ 0 THEN BEGIN
			MESSAGE = 'Empty data file or no good data'
			GOTO, HANDLE_ERROR
		ENDIF
		ROWS = ROWS(W)
		TIME = TIME(W)
	ENDIF
;
;  Find the closest entry to the target time.
;
	TARGET = ANYTIM2UTC(DATE)
	TARGET = TARGET.TIME
	DIFF = ABS(TIME - TARGET)
	MINDIF = MIN(DIFF, W)
	ROW = ROWS(W)
;
;  Read in the spacecraft attitude information.
;
	FXBREAD, UNIT, SC_AVG_PITCH_ECLIP, 'SC AVG PITCH ECLIPTIC (RAD)', ROW
	FXBREAD, UNIT, SC_AVG_ROLL_ECLIP,  'SC AVG ROLL ECLIPTIC(RAD)',	  ROW
	FXBREAD, UNIT, SC_AVG_YAW_ECLIP,   'SC AVG YAW ECLIPTIC(RAD)',	  ROW
	FXBREAD, UNIT, SC_AVG_PITCH,	'SC AVG PITCH (RAD)',	ROW
	FXBREAD, UNIT, SC_AVG_ROLL,	'SC AVG ROLL (RAD)',	ROW
	FXBREAD, UNIT, SC_AVG_YAW,	'SC AVG YAW (RAD)',	ROW
	FXBREAD, UNIT, GCI_AVG_PITCH,	'GCI AVG PITCH',	ROW
	FXBREAD, UNIT, GCI_AVG_ROLL,	'GCI AVG ROLL',		ROW
	FXBREAD, UNIT, GCI_AVG_YAW,	'GCI AVG YAW',		ROW
	FXBREAD, UNIT, GSE_AVG_PITCH,	'GSE AVG PITCH',	ROW
	FXBREAD, UNIT, GSE_AVG_ROLL,	'GSE AVG ROLL',		ROW
	FXBREAD, UNIT, GSE_AVG_YAW,	'GSE AVG YAW',		ROW
	FXBREAD, UNIT, GSM_AVG_PITCH,	'GSM AVG PITCH',	ROW
	FXBREAD, UNIT, GSM_AVG_ROLL,	'GSM AVG ROLL',		ROW
	FXBREAD, UNIT, GSM_AVG_YAW,	'GSM AVG YAW',		ROW
	FXBREAD, UNIT, SC_STD_DEV_PITCH,'SC STD DEV PITCH',	ROW
	FXBREAD, UNIT, SC_STD_DEV_ROLL,	'SC STD DEV ROLL',	ROW
	FXBREAD, UNIT, SC_STD_DEV_YAW,	'SC STD DEV YAW',	ROW
	FXBREAD, UNIT, SC_MIN_PITCH,	'SC MIN PITCH',		ROW
	FXBREAD, UNIT, SC_MIN_ROLL,	'SC MIN ROLL',		ROW
	FXBREAD, UNIT, SC_MIN_YAW,	'SC MIN YAW',		ROW
	FXBREAD, UNIT, SC_MAX_PITCH,	'SC MAX PITCH',		ROW
	FXBREAD, UNIT, SC_MAX_ROLL,	'SC MAX ROLL',		ROW
	FXBREAD, UNIT, SC_MAX_YAW,	'SC MAX YAW',		ROW
;
;  Store the result in the output structure.
;
	RESULT.SC_AVG_PITCH_ECLIP	= SC_AVG_PITCH_ECLIP
	RESULT.SC_AVG_ROLL_ECLIP	= SC_AVG_ROLL_ECLIP
	RESULT.SC_AVG_YAW_ECLIP		= SC_AVG_YAW_ECLIP
	RESULT.SC_AVG_PITCH	= SC_AVG_PITCH
	RESULT.SC_AVG_ROLL	= SC_AVG_ROLL
	RESULT.SC_AVG_YAW	= SC_AVG_YAW
	RESULT.GCI_AVG_PITCH	= GCI_AVG_PITCH
	RESULT.GCI_AVG_ROLL	= GCI_AVG_ROLL
	RESULT.GCI_AVG_YAW	= GCI_AVG_YAW
	RESULT.GSE_AVG_PITCH	= GSE_AVG_PITCH
	RESULT.GSE_AVG_ROLL	= GSE_AVG_ROLL
	RESULT.GSE_AVG_YAW	= GSE_AVG_YAW
	RESULT.GSM_AVG_PITCH	= GSM_AVG_PITCH
	RESULT.GSM_AVG_ROLL	= GSM_AVG_ROLL
	RESULT.GSM_AVG_YAW	= GSM_AVG_YAW
	RESULT.SC_STD_DEV_PITCH	= SC_STD_DEV_PITCH
	RESULT.SC_STD_DEV_ROLL	= SC_STD_DEV_ROLL
	RESULT.SC_STD_DEV_YAW	= SC_STD_DEV_YAW
	RESULT.SC_MIN_PITCH	= SC_MIN_PITCH
	RESULT.SC_MIN_ROLL	= SC_MIN_ROLL
	RESULT.SC_MIN_YAW	= SC_MIN_YAW
	RESULT.SC_MAX_PITCH	= SC_MAX_PITCH
	RESULT.SC_MAX_ROLL	= SC_MAX_ROLL
	RESULT.SC_MAX_YAW	= SC_MAX_YAW
	GOTO, FINISH
;
HANDLE_ERROR:
	IF N_ELEMENTS(ERRMSG) NE 0 THEN ERRMSG = 'GET_SC_ATT: ' + MESSAGE $
		ELSE MESSAGE, MESSAGE, /CONTINUE
;
;  Close the FITS file and return the result.
;
FINISH:
	IF NOT KEYWORD_SET(RETAIN) THEN BEGIN
		IF FXBISOPEN(UNIT) THEN FXBCLOSE, UNIT
		UNIT = -1
		LAST_FILE = ''
	ENDIF
	RETURN, RESULT
;
	END
