;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_PLAN_XY
;               
; Purpose     : format plan pointing fields
;               
; Category    : Planning
;               
; Explanation : 
;               
; Syntax      : IDL> rplan=mk_plan_xy(plan)
;    
; Examples    : 
;
; Inputs      : PLAN = plan structure (can be array)
;               
; Opt. Inputs : FORMAT = format for pointing [def = '(f8.0)']
;               
; Outputs     : RPLAN = plan structures with appropriately formatted 
;                       INS_X,INS_Y or XCEN,YCEN fields
;
; Opt. Outputs: None
;               
; Keywords    : None
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: None
;               
; History     : Version 1,  17-July-1996,  D M Zarro.  Written
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-            

function mk_plan_xy,plan,format

;-- check inputs

if datatype(plan) ne 'STC' then if exist(plan) then return,plan else return,-1
type=get_plan_type(plan)
if type eq -1 then return,plan
if datatype(format) ne 'STR' then sform='(f8.0)' else sform=format
np=n_elements(plan)
rplan=plan

;-- take care of science plans first

if type eq 2 then begin
 if tag_exist(rplan,'XCEN') then begin
  xcen=str_deblank(rplan.xcen,sform)
  rplan.xcen=strmid(xcen,0,50)
  ycen=str_deblank(rplan.ycen,sform)
  rplan.ycen=strmid(ycen,0,50)
  comma=strpos(rplan.xcen,',')
  if (comma eq -1) and trim(rplan.xcen) eq '' then rplan.xcen=strpad('0',50,/after)
  comma=strpos(rplan.ycen,',')
  if (comma eq -1) and trim(rplan.ycen) eq '' then rplan.ycen=strpad('0',50,/after)
 endif
endif else begin

;-- now the rest

 if tag_exist(rplan,'POINTINGS') then begin
  rplan.pointings.ins_x=float(str_format(rplan.pointings.ins_x,sform))
  rplan.pointings.ins_y=float(str_format(rplan.pointings.ins_y,sform))
 endif

endelse

return,rplan & end

