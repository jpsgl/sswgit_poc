;---------------------------------------------------------------------------
; Document name: get_inst_color.pro
; Created by:    Liyun Wang, GSFC/ARC, August 31, 1995
;
; Last Modified: Fri Apr 26 15:44:08 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;
FUNCTION get_inst_color, instrume, color_stc, def_color=def_color, stc=stc
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       GET_INST_COLOR()
;
; PURPOSE:
;       Get color code for the given SOHO instrument
;
; CATEGORY:
;       Utility, planning
;
; EXPLANATION:
;
; SYNTAX:
;       Result = get_inst_color('C', color_stc)
;       Result = get_inst_color(/stc)
;
; INPUTS:
;       INSTRUME  - String scalar, instrument name in short format
;       COLOR_STC - Color structure that has instrument names as part
;                   of its tags. If not passed in or passed in as an
;                   invalid color structure, color #2 will be returned.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       RESULT    - Index of color, or complete color structure if
;                   keyword STC is set
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       DEF_COLOR - Default index of color if something goes wrong;
;                   default is set to 2
;       STC       - Set this keyword to return complete color structure
;
; COMMON:
;       None.
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, August 31, 1995, Liyun Wang, GSFC/ARC. Written
;       Version 2, January 16, 1996, Liyun Wang, GSFC/ARC
;          Added STC keyword to return color structure
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-
;
   ON_ERROR, 1

   IF KEYWORD_SET(stc) THEN BEGIN
;---------------------------------------------------------------------------
;     Color defined by SET_LINE_COLOR:
;        0: black   1: white   2: yellow   3: red     4: green    5: blue
;        6: orange  7: purple  8: magenta  9: brown  10: turquoise
;---------------------------------------------------------------------------
      dc = !p.color
      color_stc = {cds:3, sumer:4, uvcs:6, eit:7, lasco:8, mdi:2, costep:dc, $
                   virgo:dc, golf:dc, celias:dc, swan:dc, erne:dc, ground:dc, $
                   ntt:10}
      RETURN, color_stc
   ENDIF

   IF N_ELEMENTS(def_color) EQ 0 THEN def_color = 2

   IF datatype(color_stc) NE 'STC' OR datatype(instrume) NE 'STR' THEN $
      RETURN, def_color
   inst = get_soho_inst(instrume)
   IF inst EQ '' THEN RETURN, def_color
   status = EXECUTE('color=color_stc.'+inst)
   IF status THEN RETURN, color ELSE RETURN, def_color

END

;---------------------------------------------------------------------------
; End of 'get_inst_color.pro'.
;---------------------------------------------------------------------------
