;---------------------------------------------------------------------------
; Document name: mk_soho_custom.pro
; Created by:    Liyun Wang, GSFC/ARC, April 20, 1995
;
; Last Modified: Mon Jun 24 12:33:45 1996 (lwang@achilles.nascom.nasa.gov)
;---------------------------------------------------------------------------
;+
; PROJECT:
;       SOHO - CDS/SUMER
;
; NAME:
;       MK_SOHO_CUSTOM
;
; PURPOSE: 
;       Widget interface for customizing MK_SOHO
;
; CATEGORY:
;       Planning, widgets
; 
; EXPLANATION:
;       User can customize features such as START/STOP times, which DB
;       entry to EDIT (e.g. DETAILS, FLAG...) or display as a
;       reference (e.g. DSN, MDI, CDS, SUMER...) 
;
; SYNTAX: 
;       mk_soho_custom, s, group=group, modal=modal
;
; INPUTS:
;       See OUTPUTS.
;
; OPTIONAL INPUTS: 
;       None.
;
; OUTPUTS:
;       S - Structure that has following tags:
;           STARTDIS, STOPDIS - the selected TAI start and stop times
;           REQ_ITEMS  - string array of selected DB items
;           SHOW_NOW   - Flag (1/0) to show current UT
;           SHOW_DAY   - Flag (1/0) to show day boundary
;           SHOW_LOCAL - Flag (1/0) to show local time
;           NOCOLOR    - Flag (1/0) to turn off/on color plot
;           OPS_MODE   - Operation mode. 0: edit SCI, 1: display SCI
;                        2: display DET
;           DTYPE      - SOHO plan type to show. 0: SCI, 1: DET
;           TROFF      - Cursor time roundoff, in minutes
;           EXT_CURSOR - Flag (1/0) to extend cursor line
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS: 
;       GROUP = widget id of calling widget program
;       MODAL = freeze calling program
;       STATUS = 0 if user quit and wants to abort
;
; COMMON:
;       None.
;
; RESTRICTIONS: 
;       None.
;
; SIDE EFFECTS:
;       None.
;
; HISTORY:
;       Version 1, April 20, 1995, Liyun Wang, GSFC/ARC. Written
;       Version 2, January 24, 1996, Liyun Wang, GSFC/ARC
;          Modified such that no carriage return is needed for start
;             time string
;       Version 3, March 13, 1996, Liyun Wang, GSFC/ARC
;          Made the widget centered at screen
;       Version 4, April 25, 1996, Liyun Wang, GSFC/ARC
;          Modified such that entries for CELIAS, SWAN, and ERNE appear
;             as JOP, SOC, and CAMPAIGN respectively
;       Version 5, June 24, 1996, Liyun Wang, GSFC/ARC
;          Added option to turn of color when drawing plan entries
;
; CONTACT:
;       Liyun Wang, GSFC/ARC (Liyun.Wang.1@gsfc.nasa.gov)
;-

PRO mk_soho_custom_event, event

   ON_ERROR, 1

   WIDGET_CONTROL, event.top, get_uvalue=unseen
   IF NOT WIDGET_INFO(unseen, /valid) THEN BEGIN
      MESSAGE, 'Invalid UNSEEN widget ID', /cont
      RETURN
   ENDIF

   WIDGET_CONTROL, unseen, get_uvalue=info, /no_copy
   WIDGET_CONTROL, event.id, get_uvalue=uvalue
   uvalue = STRTRIM(uvalue, 2)
   wtype = WIDGET_INFO(event.id, /type)

   CASE (uvalue) OF
      'SHOW_NOW': info.show_now = event.select
      'SHOW_NTT': info.show_ntt = event.select
      'SHOW_DAY': info.show_day = event.select
      'NOCOLOR': info.nocolor = event.select
      'SHOW_LOCAL': info.show_local = event.select
      'EXT_CURSOR': info.ext_cursor = event.select
      'modes': info.ops_mode = event.index
      'TIME_ROUND': info.troff = info.tround(event.index)
      ELSE:
   ENDCASE
   
;---------------------------------------------------------------------------
;  Change duration of plot window
;---------------------------------------------------------------------------
   chk = WHERE(info.tspan_uvalue EQ uvalue, cnt)
   IF cnt GT 0 THEN BEGIN
      info.sdur = info.sec_spans(chk(0))
      info.stopdis = info.startdis+info.sdur
;      WIDGET_CONTROL, info.wstopdis, set_value=tai2utc(info.stopdis, /ecs)
   ENDIF

   IF (uvalue EQ 'START_TEXT') THEN BEGIN
      WIDGET_CONTROL, event.id, get_value=tstring
      err = ''
      time = utc2tai(tstring, err=err)
      IF err NE '' THEN BEGIN
         xack, err, group=event.top, /modal
         WIDGET_CONTROL, event.id, set_value=tai2utc(info.startdis, /ecs)
      ENDIF

      info.startdis = time
      info.stopdis = time+info.sdur
;      WIDGET_CONTROL, info.wstopdis, set_value=tai2utc(info.stopdis, /ecs)
   ENDIF

;---------------------------------------------------------------------------
;  Select plot items
;---------------------------------------------------------------------------
   IF grep('R_', uvalue) NE '' THEN BEGIN
      req_uvalue = STRMID(uvalue, 2, STRLEN(uvalue))
      ii = WHERE(info.refs EQ req_uvalue)
      IF ii(0) GE 0 THEN BEGIN
         i = ii(0)
         IF info.curr_idx(i) NE 0 THEN BEGIN
            info.curr_idx(i) = 0
         ENDIF ELSE BEGIN
            info.curr_idx(i) = 1
         ENDELSE
         WIDGET_CONTROL, info.rbutt(i), set_button=info.curr_idx(i)
      ENDIF
   ENDIF

;---------------------------------------------------------------------------
;  CANCEL or ACCEPT
;---------------------------------------------------------------------------
   quit_flag = 0
   IF (uvalue EQ 'CANCEL') OR (uvalue EQ 'ACCEPT') THEN BEGIN
      IF uvalue EQ 'CANCEL' THEN BEGIN
         info.status = 0
         quit_flag=1
      ENDIF ELSE BEGIN
         WIDGET_CONTROL, info.wstartdis, get_value=tstring
         err = ''
         time = utc2tai(tstring, err=err)
         IF err NE '' THEN BEGIN
            xack, err, group=event.top, /modal
            WIDGET_CONTROL, info.wstartdis, $
               set_value=tai2utc(info.startdis, /ecs)
            RETURN
         ENDIF
         info.startdis = time
         info.stopdis = time+info.sdur
         IF info.startdis GE info.stopdis THEN BEGIN
            err = 'Start time must be less than Stop time' 
            xack, err, group=event.top
         ENDIF ELSE quit_flag = 1
      ENDELSE
   ENDIF

   WIDGET_CONTROL, unseen, set_uvalue=info, /no_copy

   IF quit_flag THEN xkill, event.top

   RETURN
END

;--------------------------------------------------------------------------- 

PRO mk_soho_custom, struct, status=status, group=group, modal=modal, $
                    font=font, just_reg=just_reg, err=err


   ON_ERROR, 1

   IF NOT HAVE_WIDGETS() THEN MESSAGE, 'widgets unavailable'
   IF NOT xalive(group) THEN xkill, /all

   bfont = '-adobe-courier-bold-r-normal--20-140-100-100-m-110-iso8859-1'
   IF datatype(font) EQ 'STR' THEN bfont = (get_dfont(font))(0)
   get_screen, fspace, fxpad, fypad

   lfont = '-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1'
   lfont = (get_dfont(lfont))(0)
   IF lfont EQ '' THEN lfont = 'fixed'
   
   refs = ['Telemetry', 'Command', 'Support', 'CELIAS', 'SWAN', 'ERNE', $
           get_soho_inst(exclude=['CELIAS', 'SWAN', 'ERNE'])]
   nrefs = N_ELEMENTS(refs) 

   def_time = utc2tai(!stime) & secs_per_day=24.*3600.d

   def_struct = {startdis:def_time, stopdis:def_time+secs_per_day, $
                 show_now:0, show_day:1, ops_mode:1, show_local:1, $
                 nocolor:0, req_items:refs, dtype:0, troff:1.0, ext_cursor:0}

   valid_input = 0
   IF datatype(struct) EQ 'STC' THEN $
      valid_input = tag_exist(struct, 'REQ_ITEMS')
   IF valid_input THEN stc = struct ELSE stc = def_struct

   curr_idx = INTARR(N_ELEMENTS(refs))

   prev = stc.req_items 

   FOR i=0, N_ELEMENTS(prev)-1 DO BEGIN
      ii = WHERE(refs EQ prev(i))
      IF ii(0) GE 0 THEN curr_idx(ii(0)) = 1
   ENDFOR

   wbase = WIDGET_BASE(title='MK_SOHO_CUSTOM', /column)

   row1 = WIDGET_BASE(wbase, /row, /frame)
   cancel = WIDGET_BUTTON(row1, value='Cancel', uvalue='CANCEL', font=bfont, $
                          resource='QuitButton')
   accept = WIDGET_BUTTON(row1, value='Accept', uvalue='ACCEPT', font=bfont, $
                          resource='AcceptButton')

;---------------------------------------------------------------------------
;  Choose Plan type
;---------------------------------------------------------------------------
   col = WIDGET_BASE(wbase, /row, /frame)
   tmp = WIDGET_LABEL(col, value='Operation Mode ', font=bfont)
   tmp = cw_bselector(col, /return_index, uvalue='modes', font=lfont, $
                      ['Edit SCI_PLAN', 'Display SCI_PLAN', $
                       'Display DET_PLAN'])
   WIDGET_CONTROL, tmp, set_value=stc.ops_mode
   
   row2 = WIDGET_BASE(wbase, /column, /frame)
   tmp = WIDGET_LABEL(row2, value='Display', font=bfont)
   
   temp = WIDGET_BASE(row2, /row)
   tmp = WIDGET_LABEL(temp, value='Start Time', font=bfont)
   wstartdis = WIDGET_TEXT(temp, uvalue='START_TEXT', /edit, $
                           value=STRTRIM(anytim2utc(stc.startdis, /ecs, $
                                                    /truncate), 2))
   IF N_ELEMENTS(sdur) EQ 0 THEN sdur = stc.stopdis-stc.startdis

   tspan_value = ['3 hours', '6 hours', '12 hours', '1 day', '1.5 days', $
                  '2 days', '3 days', '4 days', '5 days', '6 days', '7 days']
   tspan_uvalue = ['3HRS', '6HRS', '12HRS', '1DAY', '1_5DAY', '2DAY', '3DAY', $
                   '4DAY', '5DAY', '6DAY', '7DAY']
   sec_spans = [3., 6., 12., 24., 36., 48., 72., 96., 120., 144., 168.]*3600.d0
   temp = WIDGET_BASE(row2, /row)
   tmp = WIDGET_LABEL(temp, value=' Time Span', font=bfont)
   tspan = cw_bselector(temp, tspan_value, return_uvalue=tspan_uvalue, $
                        ids=buttons)
   ii = WHERE(sec_spans EQ sdur, cnt)
   IF ii(0) GT 0 THEN BEGIN
      WIDGET_CONTROL, tspan, set_value=ii(0)
   ENDIF ELSE BEGIN
;---------------------------------------------------------------------------
;     Time span not compatible with any of those in the list. Default it
;     to 1 day and change stopdis accordingly
;---------------------------------------------------------------------------
      WIDGET_CONTROL, tspan, set_value=ii(3)
      sdur = sec_spans(3)
      stc.stopdis = stc.startdis+sdur
;      WIDGET_CONTROL, wstopdis, set_value=tai2utc(DOUBLE(stc.stopdis), /ecs)
   ENDELSE
   
   lb = ['5 sec', '10 sec', '30 sec', '1 min', '5 min', '10 min', '30 min']
   tround = [5.0, 10., 30., 60.0, 300.0, 600.0, 1800.]/60.0

   temp = WIDGET_BASE(wbase, /row, /frame)
   tmp = WIDGET_LABEL(temp, value='Cursor Time Roundoff ', font=bfont)
   tmp = cw_bselector(temp, lb, /return_index, font=lfont, $
                      uvalue='TIME_ROUND')
   ii = WHERE(stc.troff EQ tround, cnt)
   IF ii(0) GT 0 THEN WIDGET_CONTROL, tmp, set_value=ii(0)
   
   temp = WIDGET_BASE(wbase, column=1, /frame)

   xmenu, ['Show Current UT', 'Show Local Time Axis', $
           'Show Day Boundaries', 'Turn Off Color', 'Extend Cursor Line'], $
      temp, uvalue=['SHOW_NOW', 'SHOW_LOCAL', 'SHOW_DAY', 'NOCOLOR', $
                    'EXT_CURSOR'], $
      buttons=showb, /nonexclusive, /column

   WIDGET_CONTROL, showb(0), set_button=stc.show_now
   WIDGET_CONTROL, showb(1), set_button=stc.show_local
   WIDGET_CONTROL, showb(2), set_button=stc.show_day
   WIDGET_CONTROL, showb(3), set_button=stc.nocolor
   WIDGET_CONTROL, showb(4), set_button=stc.ext_cursor

;---------------------------------------------------------------------------
;  Plotting items
;---------------------------------------------------------------------------
   c1 = WIDGET_BASE(wbase, column=1, /frame)
   tmp = WIDGET_LABEL(c1, value='Items to Be Displayed', font=bfont)
   
;---------------------------------------------------------------------------
;  Make the conversion for JOP, SOC, and CAMPAIGN
;---------------------------------------------------------------------------
   drefs=refs
   clook=where(drefs eq 'CELIAS',cnt)
   drefs(clook(0))='JOP'
   clook=where(drefs eq 'SWAN',cnt)
   drefs(clook(0))='SOC'
   clook=where(drefs eq 'ERNE',cnt)
   drefs(clook(0))='Campaign'
   
   xmenu, drefs, c1, column=3, /nonexclusive, uvalue='R_'+refs, buttons=rbutt

   ii = WHERE(curr_idx NE 0)
   IF ii(0) GE 0 THEN BEGIN
      FOR j=0, N_ELEMENTS(ii)-1 DO BEGIN
         WIDGET_CONTROL, rbutt(ii(j)), set_button=1
      ENDFOR
   ENDIF

   valid = 0 
   IF xalive(group) THEN offsets = get_cent_off(wbase, group, /center, $
                                                valid=valid) 
   IF valid THEN BEGIN
      xoff = offsets(0) & yoff=offsets(1)
      WIDGET_CONTROL, wbase, /realize, tlb_set_xoff=xoff, $
         tlb_set_yoff=yoff, /map
   ENDIF ELSE WIDGET_CONTROL, wbase, /realize, /map 

;-- pass common info into hidden widget base

   unseen = WIDGET_BASE()
   plot_info = {rbutt:rbutt, howb:showb, refs:refs, wstartdis:wstartdis, $
                tspan:tspan, status:1, $
                sdur:sdur, sec_spans:sec_spans, tspan_uvalue:tspan_uvalue, $
                curr_idx:curr_idx, tround:tround}

   info = join_struct(stc, plot_info)

   WIDGET_CONTROL, unseen, set_uvalue=info, /no_copy
   WIDGET_CONTROL, wbase, set_uvalue=unseen

   xmanager, 'mk_soho_custom', wbase, group=group, modal=modal, $
      just_reg=just_reg
   xshow, group

   WIDGET_CONTROL, unseen, get_uvalue=info, /no_copy
   xkill, unseen
   status = info.status
   IF status THEN BEGIN
      split_struct, info, 'rbutt', struct, rest
      ii = WHERE(info.curr_idx NE 0)
      IF ii(0) GE 0 THEN BEGIN
         temp = info.refs(ii)
         struct = rep_tag_value(struct, temp, 'REQ_ITEMS')
      ENDIF
      IF struct.ops_mode NE 2 THEN struct.dtype = 0 ELSE struct.dtype = 1
   ENDIF

   IF NOT xalive(group) AND NOT KEYWORD_SET(just_reg) THEN XMANAGER

   RETURN
END

;---------------------------------------------------------------------------
; End of 'mk_soho_custom.pro'.
;---------------------------------------------------------------------------
