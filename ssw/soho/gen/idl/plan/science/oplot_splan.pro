;+
; Project     :	SOHO - CDS
;
; Name        :	OPLOT_SPLAN
;
; Purpose     :	oplots individual planning items on plan timeline
;
; Explanation :	A wrapper around PLOT_ITEM
;
; Use         :	OPLOT_SPLAN, PLAN,ROW
;
; Inputs      :	PLAN = input plan structure
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	CHARSIZE = character size to use in displaying
;		COLOR	    = The color in which the item should be drawn.
;               SPEC        = use specific science objective as label
;		ROW         = row in which to overplot [def = 0]
;               WINDOW      = window index in which to plot
;
; Calls       :	PLOT_ITEM
;
; Common      :	None.
;
; Restrictions:	PLOT_FRAME must be called first.
;
; Side effects:	None.
;
; Category    :	Planning, Science.
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro, 16 June 1995.
;
; Modification: Liyun Wang, March 1, 1996
;               Set character size consistantly with PLOT_SPLAN
;
; VERSION     :	Version 2.0, March 1, 1996
;-
;

PRO oplot_splan, plan, startdis, row=row, color=color, spec=spec, $
                 charsize=charsize, WINDOW=WINDOW

   ON_ERROR, 1
   IF datatype(plan) NE 'STC' THEN RETURN
   IF NOT exist(charsize) THEN BEGIN
      IF !p.charsize NE 0.0 THEN charsize = !p.charsize ELSE charsize = 1.2
   ENDIF
   charsize = 5.0*charsize
   
   IF N_ELEMENTS(WINDOW) GT 0 THEN BEGIN
      IF (WINDOW GT -1) AND (!d.name EQ 'x') THEN BEGIN
         if window ne !d.window then setwindow, WINDOW, /show
      ENDIF
   ENDIF

   tstart = utc2tai(anytim2utc(startdis))
   datastart = tai2utc(tstart)
   start_hour = datastart.time / 3600000. 
   datastart.time = 0 
   daystart = utc2tai(datastart)
   itime = get_plan_itime(plan)
   IF N_ELEMENTS(row) EQ 0 THEN row = 0
   IF itime GT -1 THEN BEGIN
      label = plan.sci_obj
      IF KEYWORD_SET(spec) AND tag_exist(plan, 'SCI_SPEC') THEN $
         label = plan.sci_spec
      plan_start = (plan.(itime) - daystart) / 3600d
      plan_end = (plan.(itime+1) - daystart) / 3600d
      plot_item, plan_start, plan_end, row, label, $
         color=color, maxcharsize=charsize
   ENDIF

   RETURN
   
END
