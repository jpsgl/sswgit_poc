;+
; Project     : SOHO - CDS     
;                   
; Name        : MK_PLAN_RESET
;               
; Purpose     : reset contents of MK_PLAN_SHARED common blocks
;               
; Category    : Planning
;               
; Explanation : 
;               
; Syntax      : IDL> mk_plan_reset
;    
; Examples    : 
;
; Inputs      : None
;               
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;               
; Keywords    : None
;
; Common      : MK_PLAN_SHARED
;               
; Restrictions: None
;               
; Side effects: Variables in MK_PLAN_SHARED are undefined
;               
; History     : Version 1,  4-Aug-1995,  D M Zarro.  Written
;
; Contact     : DMZARRO
;-            

pro mk_plan_reset

@mk_plan_shared
delvarx, cds_detail, cds_flag, cds_science, sumer_detail, sumer_science,cds_alt
delvarx, soho_splan,soho_dplan

return & end





