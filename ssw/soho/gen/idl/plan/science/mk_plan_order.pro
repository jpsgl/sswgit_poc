;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_ORDER
;
; Purpose     :	orders plan entries according to time-tags and durations
;
; Explanation :	
;
; Use         :	MK_PLAN_ORDER, PLAN, NPLAN, STARTDIS, STOPDIS
;
; Inputs      : PLAN = structure array of plan entries
;
; Opt. Inputs : STARTDIS, STOPDIS = lower and upper time limits within which
;               to constrain recalculated entries.
;
; Outputs     :	NPLAN = time-shifted plan entries
;
; Opt. Outputs:	None.
;
; Keywords    : TGAP = time gap between adjacent entries [def = 1.d-3 s]
;               TCAL = start time at which to start recalculation 
;                        [def = start of first plan]
;               TMAX = start time of last plan entry to check [def = last plan]
;               GROUP = main base ID if called from a widget app
;
; Calls       :	MK_PLAN_RECAL (for DETAILS type=0) else MK_PLAN_CHANGE 
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning
;
; Prev. Hist. :	None.
;
; Written     :	Dominic Zarro (ARC)
;
; Version     :	Version 1.0, 24 May 1995.
;-

pro mk_plan_order,plan,nplan,startdis,stopdis,err=err,tgap=tgap,$
                  tcal=tcal,tmax=tmax,mark=mark,group=group,serious=serious

err='' & serious=0
type=get_plan_type(plan) & if type eq -1 then return

;-- mark plot

if keyword_set(mark) and exist(startdis) then begin
 day=tai2utc(startdis) &  day.time=0 & day=utc2tai(day)
 if exist(tcal) then begin
  x=(tcal-day)/3600.d
  oplot,[x,x],[.02,.98],linestyle=4,color=4
 endif

 if exist(tmax) then begin
  if tmax gt 0. then begin
   x=(tmax-day)/3600.d
   oplot,[x,x],[.02,.98],linestyle=5,color=4
  endif
 endif
endif

if (type eq 0) then $
 mk_plan_recal,plan,nplan,startdis,stopdis,tgap=tgap,tcal=tcal,tmax=tmax,err=err,serious=serious else $
  mk_plan_change,plan,nplan,startdis,stopdis,tgap=tgap,tcal=tcal,err=err

if err(0) ne '' then begin
 if xalive(group) then begin
  if serious then begin
   xack,err,group=group
   err='Last Operation will be aborted.'
  endif else begin
   value=xanswer(err,group=group,instruct='Proceed? ') 
   if value then err='' else err='Last operation will be aborted.'
  endelse
 endif else begin
  if serious then message,arr2str(err),/cont else begin
   nold=n_elements(plan)
   nnew=n_elements(nplan)
   if nold gt nnew then begin
    if nnew gt nold then dprint,'%',num2str(nnew-nold),' plans would be added'
    if nnew lt nold then dprint,'%',num2str(nold-nnew),' plans would be deleted'
    err='Cannot modify current entry without removing one or more entries. Insufficient space.'
   endif
  endelse
 endelse
endif

return & end

