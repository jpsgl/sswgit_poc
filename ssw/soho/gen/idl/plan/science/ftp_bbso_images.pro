;+
; Project     : SOHO-CDS
;
; Name        : FTP_BBSO_IMAGES
;
; Purpose     : FTP BBSO H-alpha files for a given date
;
; Category    : planning
;
; Explanation : FTP's BBSO Full-Disk H-alpha JPEG files to /tmp
;               and renames them to SOHO convention
;
; Syntax      : files=ftp_bbso_images(date)
;
; Examples    :
;
; Inputs      : None
;
; Opt. Inputs : DATE = date to retrieve
;
; Outputs     : 
;
; Opt. Outputs: FILES = found and renamed filenames
;
; Keywords    : OUTDIR = output directory for file [def = current]
;               ERR = error string
;               COUNT = no of files copied
;               CLOBBER= set to clobber previously copied files
;               BACK= # of days backward to look [def=0]
;
; Common      : None
;
; Restrictions: Unix only 
;
; Side effects: None
;
; History     : Written 14 May 1998 D. Zarro, SAC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-

function ftp_bbso_images,date,count=count,err=err,_extra=extra,quiet=quiet,$
                      outdir=outdir,clobber=clobber,back=back
on_error,1
err=''
count=0
files=''
if not exist(back) then back=0
quiet=keyword_set(quiet)
loud=1-quiet
server='ftp.bbso.njit.edu'
get_dir='pub/fulldisk/halpha

;-- check write access

if datatype(outdir) ne 'STR' then put_dir=curdir() else put_dir=outdir
if not test_dir(put_dir,quiet=quiet,err=err) then return,files

;-- default to current date

cerr=''
hdate=anytim2utc(date,err=cerr)
if cerr ne '' then get_utc,hdate

;-- construct filenames to copy

months  = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c' ]
for i=0,back do begin
 tdate=hdate
 tdate.mjd=tdate.mjd-i
 hcode=date_code(tdate)
 year=strmid(hcode,2,2)
 month=fix(strmid(hcode,4,2))
 day=strmid(hcode,6,2)
 tfiles='hd'+months(month-1)+day+year+'r.jpg'
 if i eq 0 then get_files=tfiles else get_files=[get_files,tfiles]
endfor

dprint,'% get_files: ',get_files

smart_ftp,server,get_files,get_dir,files=files,count=count,$
          quiet=quiet,err=err,outdir='/tmp',_extra=extra

if err ne '' then return,files

if loud then begin
 if count eq 0 then message,'No files found for '+anytim2utc(hdate,/date,/vms),/cont
endif

;-- rename files and move to desired directory

clobber=keyword_set(clobber)
if count gt 0 then begin
 for i=0,count-1 do begin
  break_file,files(i),fdsk,fdir,fname,ext
  imonth=strmid(fname,2,1)
  chk=where(imonth eq months,ic)
  if ic gt 0 then month=string(chk(0)+1,format='(i2.2)') else month='??'
  day=strmid(fname,3,2)
  year='19'+strmid(fname,5,2)
  new_file=concat_dir(put_dir,'bbso_halph_fd_'+year+month+day+'_0000')+ext
  chk=loc_file(new_file,count=fcount)
  if (fcount eq 0) or clobber then spawn,'mv -f '+files(i)+' '+new_file
  spawn,'rm -f '+files(i)
  files(i)=new_file 
 endfor
endif
return,files & end

