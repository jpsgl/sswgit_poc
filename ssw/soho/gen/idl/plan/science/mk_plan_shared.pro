;+
; Project     :	SOHO - CDS
;
; Name        :	MK_PLAN_SHARED
;
; Purpose     :	Common blocks for MK_SOHO, MK_PLAN, & MK_DETAIL
;
; Explanation : 
;
; Use         : N.A.
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Called from :	N.A.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Category    :	Planning, science
;
; Prev. Hist. :	None.
;
; Written     :	Version 1, Liyun Wang, GSFC/ARC, June 9, 1995
;
; Version     :	Version 1
;-

COMMON mk_plan_shared, startdis, stopdis, sdur, color_stc, $
   dsn, support, mdi_m, cmdtimes, dlydtimes, $
   nrt, svm, tape_dump, trcr, tnorcr, mdi_h, $
   cds_detail, cds_flag, cds_science, cds_alt, $
   sumer_detail, sumer_science, $
   soho_splan, soho_dplan, $
   mk_detail_base, mk_soho_base
