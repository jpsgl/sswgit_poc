PRO plot_support, label, t1, t2, row, tn=tn, charsize=charsize, color=color
;+
; Project     :	SOHO - CDS
;
; Name        :	PLOT_SUPPORT
;
; Purpose     :	Plots ground support times on science plan.
;
; Explanation :	This routine plots the times of available ground support at
;		several places around the world in the appropriate space within
;		the science plan display.  A line is drawn showing the range of
;		times of daylight at various areas, with noon marked, and the
;		title of the area displayed next to the line.
;
; Use         :	PLOT_SUPPORT, LABEL, T1, T2, ROW
;
; Inputs      :	LABEL	= Array containing the labels for the different ground
;			  observatory areas, e.g.
;
;				LABEL = ['Canaries','West U.S.','Hawaii'].
;
;		T1	= Start time for each observatory area.
;		T2	= End time for each observatory area.
;               ROW     = row in which to plot data
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	CHARSIZE = The character size for the text labels.  If not
;			   passed, then determined from !P.CHARSIZE.
;               COLOR = color to plot
;		TN	= Time of local noon for each observatory area.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	All the input arrays must have the same number of dimensions.
;		PLAN_FRAME must be called first.
;
; Side effects:	None.
;
; Category    :	Planning, Science.
;
; Prev. Hist. :	None.
;
; Written     :	William Thompson, GSFC, 15 July 1993.
;
; Modified    :	Version 1, William Thompson, GSFC, 15 July 1993.
;             :	Version 2, Dominic Zarro GSFC, 29 June 1995.
;                --changed name to PLOT_SUPPORT and added ROW argument
;               Version 3, Liyun Wang, GSFC/ARC, August 4, 1995
;                  Made Tn as an optional keyword
;
; Version     :	Version 3
;-
;
   ON_ERROR, 1

;---------------------------------------------------------------------------
;  check the number of parameters
;---------------------------------------------------------------------------
   IF N_PARAMS() LT 4 THEN BEGIN
      MESSAGE, 'syntax:  plot_support, label, t1, t2, row', /cont
      RETURN
   END

;---------------------------------------------------------------------------
;  get the character size
;---------------------------------------------------------------------------
   IF N_ELEMENTS(charsize) EQ 0 THEN BEGIN
      IF !p.charsize GT 0 THEN charsize = !p.charsize ELSE charsize = 1
   ENDIF
;   IF NOT exist(tn) THEN tn = STRARR(N_ELEMENTS(t1))

;---------------------------------------------------------------------------
;  define the character offsets in the x and y directions
;---------------------------------------------------------------------------
   dx = !d.x_ch_size * charsize * 2 / (!x.s(1) * !d.x_size)
   dy = !d.y_ch_size * charsize / (3 * !y.s(1) * !d.y_size)

   xmin = !x.crange(0)
   xmax = !x.crange(1)
   IF N_ELEMENTS(row) EQ 0 THEN row = 0
   IF N_ELEMENTS(color) EQ 0 THEN color = !d.n_colors
   nsort = uniq([label], SORT([label]))
   lsort = label(nsort)
   ns = N_ELEMENTS(nsort)
   disp = 1./(ns+1)
   rpos = row+(1.+FINDGEN(ns))*disp
   
   FOR i=0, N_ELEMENTS(t1)-1 DO BEGIN
;---------------------------------------------------------------------------
;  plot the support information for each location 
;---------------------------------------------------------------------------
      k = WHERE(label(i) EQ lsort)
      y0 = rpos(k)
      x1 = t1(i)
      x2 = t2(i)
      IF exist(tn) THEN BEGIN
;---------------------------------------------------------------------------
;        plot the noon marker only if tn exists
;---------------------------------------------------------------------------
         xn = tn(i)
         IF (xn GE xmin) AND (xn LE xmax) THEN	$
            xyouts, xn, y0-dy, 'N', align=0.5, $
            charsize=charsize, font=-1, color=color
         OPLOT, [x1, xn-dx], [y0, y0], psym=0, linestyle=0, color=color
         OPLOT, [xn+dx, x2], [y0, y0], psym=0, linestyle=0, color=color
      ENDIF ELSE BEGIN
         OPLOT, [x1, x2], [y0, y0], psym=0, linestyle=0, color=color
      ENDELSE
      
;---------------------------------------------------------------------------
;     plot the support extent lines
;---------------------------------------------------------------------------
      OPLOT, [x1, x1], [y0-0.1, y0+0.1], psym=0, linestyle=0, color=color
      OPLOT, [x2, x2], [y0-0.1, y0+0.1], psym=0, linestyle=0, color=color
      
;---------------------------------------------------------------------------
;     plot the label
;---------------------------------------------------------------------------
      IF (x2 GE xmin) AND (x2 LE xmax) THEN BEGIN
         xyouts, x2, y0-dy, ' '+label(i), align=0, $
            charsize=charsize, font=-1, color=color
      END ELSE IF (x1 GE xmin) AND (x1 LE xmax) THEN BEGIN
         xyouts, x1, y0-dy, label(i)+' ', align=1, $
            charsize=charsize, font=-1, color=color
      ENDIF
   ENDFOR
   END
