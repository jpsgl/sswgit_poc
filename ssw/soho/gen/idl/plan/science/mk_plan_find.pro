;+
; Project     : SOHO-CDS
;
; Name        : MK_PLAN_FIND
;
; Purpose     : Find all plans between STARTDIS/STOPDIS (inclusive)
;
; Category    : planning
;
; Explanation :
;
; Syntax      : find=mk_plan_find(plan,startdis,stopdis)
;
; Examples    :
;
; Inputs      : PLAN = plan structure array to check
;
; Opt. Inputs : None
;
; Outputs     : FIND = indicies of plans within STARTDIS/STOPDIS window
;
; Opt. Outputs:
;
; Keywords    : COUNT = # of plans found
;               RCOUNT = # of plans outside window
;               REST = indicies of plans outside window
;
; Common      : None
;
; Restrictions: None
;
; Side effects: None
;
; History     : Written 28 January 1997, D. Zarro, ARC/GSFC
;
; Contact     : dzarro@solar.stanford.edu
;-


function mk_plan_find,plans,startdis,stopdis,count=count,$
                      rcount=rcount,rest=rest

count=0
if (not exist(plans)) or (not exist(startdis)) or (not exist(stopdis)) then return,-1
itime=get_plan_itime(plans)

tstart=str_format(startdis)
tstop=str_format(stopdis)
index=lindgen(n_elements(plans))
dstart=str_format(plans.(itime))
dstop=str_format(plans.(itime+1))
dlook=where( ((dstart ge tstart) and (dstart lt tstop)) or $
             ((dstart lt tstart) and (dstop gt tstart)) or $
             ((dstart le tstart) and (dstop ge tstop)), count)

index=lindgen(n_elements(plans))
ilook=where_vector(dlook,index,rest=rest,rcount=rcount)
                      
return,dlook & end

  