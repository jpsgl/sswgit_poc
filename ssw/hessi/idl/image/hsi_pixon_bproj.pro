
;+

function hsi_pixon_bproj,vrate,dobj,iobj,a2d,harmonics,vanilla=vanilla, $
                         setunits=setunits,useunits=useunits, $
                         smoothpatts=smoothpatts,diffres=diffres, $
                         pixon_sizes=pixon_sizes, pixonmap=pixonmap, $
                         reset_smoothpatts=reset_smoothpatts,nonorm=nonorm, $
                         nocheck=nocheck,smpattwritedir=smpattwritedir, $
                         not_quiet=not_quiet, $
                         no_spatial_frequency_weight=no_spatial_frequency_weight

;PROJECT
;     HESSI
;NAME:
;     HSI_PIXON_BPROJ
;PURPOSE:
;     Get a back projection without any calibration effects which are present
;     in the GetBProj code.  This is used for the Pixon gradient calculations.
;CATEGORY:
;CALLING SEQUENCE:
;     bproj = hsi_pixon_bproj(vrate,dobj,iobj,a2d,harmonics)
;INPUTS:
;     vrate = data vector
;     dobj = data object
;     iobj = CE object
;     a2d = list of a2d's (from hsi_pixon_get_data.pro)
;     harmonics = list of harmonics's for each a2d
;                 (from hsi_pixon_get_data.pro)
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS
;     /vanilla = Use the direct back projection, w/o all the normalizations
;                used in the object version.
;     /useunits = apply units to vrate.  Should match hsi_pixon_residuals.
;     /setunits = initialize units.
;     /nonorm = Do not use normalization from hsi_dirty_norm
;     /gaussian = use Gaussian statistics rather than Poisson statistics
;     /smoothpatts = Use smoothed mod patts in the back projection (slow)
;     diffres = resolution for differential smoothing.  Differentially
;               smoothed patterns are not stored and are recomputed at each
;               call.
;     /reset_smoothpatts = recompute smoothed patterns
;     /nocheck = do not waste time checking whether or not to recompute the 
;                smoothed modulation patterns.  Careful with this one!  
;                The /reset_smoothpatts keyword overrides this one.
;     pixon_sizes = vector of pixon sizes.  If set, recompute the smoothed
;                   patterns.
;     smpattwritedir = If new smoothed patterns are calculated and this
;                      keyword is set to a valid directory, then the 
;                      new smoothed patterns will be saved in that directory. 
;OUTPUTS:
;     bproj = back projected image
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;PROCEDURE:
;MODIFICATION HISTORY:
;     T. Metcalf 1999-Dec-08
;     T. Metcalf 2000-Feb-23 Added byte_cosine keyword.
;     T. Metcalf 2000-Sep-20 Complete overhaul to use the new annular sector 
;                            mod patts.
;     T. Metcalf 2000-Oct-20 Moved pattern smoothing code to 
;                            hsi_pixon_smooth_patterns.pro
;     T. Metcalf 2001-Aug-29 Changed mpd logic
;     T. Metcalf 2002-Nov-08 Included livetime in units and removed
;                            ndet factor kludge which was no longer
;                            required since I now use the
;                            /no_spatial_freq keyword.
;     T. Metcalf 2005-May-09 Added /no_rate_correct in call to bproj object
;     T. Metcalf 2005-Jul-01 Fixed typo, it is /norate_correct not
;                            /no_rate_correct. 
;     K. Tolbert 2008-Dec-02 Fixed crash with time_range
;-

   ; If this common block is changed, it must also be changed in 
   ; hsi_pixon_free_mem.pro
   common hsi_pixon_bproj_private,units,smoothed_patterns,save_pixon_sizes,npixons,savempd

   nx = long((iobj->get(/rmap_dim))[0])
   ny = long((iobj->get(/rmap_dim))[1])

   low_memory = 0  ; If set, do not save the transpose arrays when smoothing.  Could
                   ; be slower IF the transpose is actually used, but saves a factor
                   ; of two in memory.   When is the transpose actully used??
                   ; Setting this causes a memory leak with cmapb_ptr and/or smapb_ptr!

   if n_elements(diffres) NE 0 then usediffres = 1 else usediffres = 0
   check_smoothpatts = NOT keyword_set(nocheck) OR keyword_set(reset_smoothpatts)

   if keyword_set(smoothpatts) AND check_smoothpatts then begin
      ; Do we need to compute the smoothed patterns?
      if keyword_set(reset_smoothpatts) then recompute_smoothed_patts = 1 $
      else recompute_smoothed_patts = 0
      if n_elements(pixon_sizes) GT 0 then begin
         if n_elements(save_pixon_sizes) GT 0 then begin
            if n_elements(pixon_sizes) NE n_elements(save_pixon_sizes) then $
               recompute_smoothed_patts=1 $
            else if max(pixon_sizes-save_pixon_sizes) GT 0 then recompute_smoothed_patts=1
         endif else recompute_smoothed_patts=1
         ; The mpd stuff is not well tested yet.  TRM  2000-10-25
         ;mpd = {hsi_modul_pattern_control}
         mpd = {image_dim:iobj->get(/image_dim), $ 
                pixel_size:iobj->get(/pixel_size), $
                pixel_scale:iobj->get(/pixel_scale), $
                r0_offset:iobj->get(/r0_offset)}
         mpd_names = tag_names(mpd)
         ; Fill the mod patt definition structure
         ;for i=0L,n_tags(mpd)-1L do check = execute('mpd.(i) = iobj->get(/'+mpd_names[i]+')')
         if NOT recompute_smoothed_patts then begin
            if n_elements(savempd) LE 0 then begin
               recompute_smoothed_patts=1
            endif else begin
               for i=0L,n_tags(mpd)-1L do begin
                  for j=0L,n_elements(mpd.(i))-1L do begin
                     if (mpd.(i))[j] NE (savempd.(i))[j] then begin
                        recompute_smoothed_patts=1
                     endif
                  endfor
               endfor
            endelse
         endif
         if recompute_smoothed_patts then begin
            hsi_pixon_smooth_patterns,iobj, $
                                      pixon_sizes, $
                                      smoothed_patterns, $
                                      writedir=smpattwritedir, $
                                      low_memory=low_memory, $
                                      quiet = 1-keyword_set(not_quiet)
            save_pixon_sizes = pixon_sizes
            savempd = mpd
         endif
      endif else message,/info,'WARNING: smoothpatts set, but pixon_sizes not passed in.  NO SMOOTHING'
   endif

   celist = iobj->GetData(class_name='hsi_calib_eventlist')
   scelist = size(celist)
   det_index_mask=iobj->get(/det_index_mask)
   test = where(det_index_mask, ndetectors)
   if n_elements(units) LE 0 or keyword_set(setunits) then begin
      message,/info,'Resetting units'
      if n_elements(units) GT 0 then begin
         for i=0L,n_elements(units)-1L do ptr_free,units[i]
      endif
      units = ptrarr(scelist[1])
      time_bin = float(iobj->get(/time_bin_def)*iobj->get(/time_bin_min))/2L^20
      ttime = iobj->Get(/time_range)
      ttime = ttime[1]-ttime[0]  ; total observation time
      ;det_index_mask=iobj->get(/det_index_mask)
      ;test = where(det_index_mask, ndetectors)
      ;message,/info,string(ndetectors)+' '+string(ttime)
      for i=0,scelist[1]-1 do begin  ; Is this the right order???
         j = 0 ; no harmonics index in the new scheme
         if ptr_valid(celist[i,j]) then begin
            qtrans = (*celist[i,j]).gridtran
            livetime = (*celist[i,j]).livetime
            flux_var = (*celist[i,j]).flux_var
            this_det_eff = ((iobj->get(/cbe_det_eff)).rel)[i MOD 9]
            ; These units seem upside down, but they are only used for
            ; the gradient calculation and the r2s array which is passed
            ; in at the data has units of 1/fobs.  These units must be
            ; consistent with those in hsi_pixon_residuals and must equal
            ; whatever is in hsi_pixon_residuals multiplied by
            ; time_bin[i MOD 9]*qtrans
          ;  units[i,j] = $
          ;     ptr_new(time_bin[i MOD 9]*qtrans)  ; image units ph/coll/sec
;            units[i,j] = $
;               ptr_new(time_bin[i MOD 9]*qtrans*livetime*flux_var*this_det_eff/ndetectors)  ; image units ph/coll/sec
            ;units[i,j] = $
            ;   ptr_new(time_bin[i MOD 9]*qtrans*livetime/ndetectors)  ; image units ph/coll/sec
            units[i,j] = $
               ptr_new(time_bin[i MOD 9]*qtrans*livetime)  ; image units ph/coll/sec
            ;units[i,j] = $
            ;   ptr_new(time_bin[i MOD 9]*qtrans*livetime*flux_var*this_det_eff)  ; image units ph/coll/sec
            ;units[i,j] = $
            ;   ptr_new(time_bin[i MOD 9] * (*celist[i,j]).livetime)
         endif
      endfor
   endif

   bproj = fltarr(nx,ny)

   if keyword_set(smoothpatts) then npixons = n_elements(save_pixon_sizes) $
   else npixons = 1L
   mpatt = iobj->getdata(class_name='hsi_modul_pattern')
   for ipixon = 0L,npixons-1L do begin
      if keyword_set(smoothpatts) then begin
         ssmask = where(abs((pixonmap-save_pixon_sizes[ipixon])/save_pixon_sizes[ipixon]) LE 0.01,nssmask)
      endif else begin
         nssmask = 1L
      endelse
      if nssmask GT 0 then begin
         i1 = 0L
         for i=0,n_elements(a2d)-1 do begin
            nharmonics = iobj->get(/max_harm)
            ;j=0  ; no harmonic index in the new scheme
            if keyword_set(smoothpatts) then begin
               old_cmap_ptr = (*mpatt[a2d[i]]).cmap_ptr
               old_smap_ptr = (*mpatt[a2d[i]]).smap_ptr
               old_cmapb_ptr = (*mpatt[a2d[i]]).cmapb_ptr
               old_smapb_ptr = (*mpatt[a2d[i]]).smapb_ptr
               old_weight_map_ptr = (*mpatt[a2d[i]]).weight_map_ptr
               ; Set smoothed pointers
               if NOT usediffres then begin
                  (*mpatt[a2d[i]]).cmap_ptr = $
                     (*smoothed_patterns[a2d[i],ipixon]).cmap_ptr
                  (*mpatt[a2d[i]]).smap_ptr = $
                     (*smoothed_patterns[a2d[i],ipixon]).smap_ptr
                  (*mpatt[a2d[i]]).cmapb_ptr = $
                     (*smoothed_patterns[a2d[i],ipixon]).cmapb_ptr
                  (*mpatt[a2d[i]]).smapb_ptr = $
                     (*smoothed_patterns[a2d[i],ipixon]).smapb_ptr
               endif else begin
                  hsi_pixon_smooth_patterns,iobj, $
                                   save_pixon_sizes[ipixon], $
                                   diffsmoothed_patterns, $
                                   this_det=a2d[i], $
                                   ;this_harm=0,$
                                   diffresindex=diffres,$
                                   diffresvalue=save_pixon_sizes[diffres], $
                                   writedir=smpattwritedir, $
                                   low_memory=low_memory, $
                                   /quiet
                  if keyword_set(not_quiet) then begin
                     if i eq 0 then begin
                        print,format='($,"|.")' 
                     endif else begin 
                        print,format='($,".")'
                     endelse
                  endif
                  (*mpatt[a2d[i]]).cmap_ptr = $
                     (*diffsmoothed_patterns[0,0]).cmap_ptr
                  (*mpatt[a2d[i]]).smap_ptr = $
                     (*diffsmoothed_patterns[0,0]).smap_ptr
                  (*mpatt[a2d[i]]).cmapb_ptr = $
                     (*diffsmoothed_patterns[0,0]).cmapb_ptr
                  (*mpatt[a2d[i]]).smapb_ptr = $
                     (*diffsmoothed_patterns[0,0]).smapb_ptr
               endelse
               (*mpatt[a2d[i]]).weight_map_ptr = ptr_new()
            endif
            ndah = n_elements(*dobj[a2d[i]])
            i2 = i1 + ndah -1L

            rate = vrate[i1:i2]
            if keyword_set(useunits) then begin
               rate = rate*(*units[a2d[i]])
               if n_elements((*units[a2d[i]])) NE i2-i1+1 then begin
                  message,/info,'WARNING: problem with unit array'
               endif
            endif
            ;use_rate = iobj->get(/use_rate)
            ;iobj->set,use_rate=0
            ;use_cull = iobj->get(/use_cull)
            ;iobj->set,use_cull=0
            if NOT keyword_set(vanilla) then begin
               btemp = iobj->GetData(class_name='hsi_bproj', $
                                     /NORATE_CORRECT, $
                                     this_det=a2d[i], $
                                     ;this_harmonic=0, $
                                     vrate=rate,flatfield=1, $
                                     no_spatial_frequency_weight=no_spatial_frequency_weight)
            endif else begin
               btemp = iobj->GetData(class_name='hsi_bproj', $
                                      /NORATE_CORRECT, $
                                      this_det=a2d[i], $
                                      ;this_harmonic=0, $
                                      vrate=rate,flatfield=0, $
                                      no_spatial_frequency_weight=no_spatial_frequency_weight)
               if NOT keyword_set(nonorm) then $
                  btemp = btemp / hsi_dirty_norm(iobj,/polar)
            endelse
            ;iobj->set,use_rate=use_rate
            ;iobj->set,use_cull=use_cull
            ; This corrects an offset which is applied in hsi_annsec_bproj
            if usediffres then begin
               btemp = btemp - total(rate)
            endif
            i1 = i1 + ndah
            if keyword_set(smoothpatts) then begin
               bproj[ssmask] = bproj[ssmask]+btemp[ssmask]
               if usediffres then begin
                   ; Won't be used again, so free the memory
                   if ptr_valid((*mpatt[a2d[i]]).cmap_ptr) then $
                       ptr_free,(*mpatt[a2d[i]]).cmap_ptr
                   if ptr_valid((*mpatt[a2d[i]]).smap_ptr) then $
                       ptr_free,(*mpatt[a2d[i]]).smap_ptr
                   if ptr_valid((*mpatt[a2d[i]]).cmapb_ptr) then $
                       ptr_free,(*mpatt[a2d[i]]).cmapb_ptr
                   if ptr_valid((*mpatt[a2d[i]]).smapb_ptr) then $
                       ptr_free,(*mpatt[a2d[i]]).smapb_ptr
               endif
               ; Reset pointers to unsmoothed patterns
               (*mpatt[a2d[i]]).cmap_ptr = old_cmap_ptr
               (*mpatt[a2d[i]]).smap_ptr = old_smap_ptr
               if keyword_set(low_memory) and $
                  ptr_valid((*mpatt[a2d[i]]).cmapb_ptr) then begin
                  ;stop
                  ptr_free,(*mpatt[a2d[i]]).cmapb_ptr
               endif
               if keyword_set(low_memory) and $
                  ptr_valid((*mpatt[a2d[i]]).smapb_ptr) then begin
                  ;stop
                  ptr_free,(*mpatt[a2d[i]]).smapb_ptr
               endif
               (*mpatt[a2d[i]]).cmapb_ptr = old_cmapb_ptr
               (*mpatt[a2d[i]]).smapb_ptr = old_smapb_ptr
               if ptr_valid((*mpatt[a2d[i]]).weight_map_ptr) then begin
                  if (*mpatt[a2d[i]]).weight_map_ptr NE old_weight_map_ptr then $
                     ptr_free,(*mpatt[a2d[i]]).weight_map_ptr
               endif
               (*mpatt[a2d[i]]).weight_map_ptr = old_weight_map_ptr
            endif else begin
               bproj = bproj + btemp
            endelse
         endfor
         if i2 NE n_elements(vrate)-1 then message,'vrate had the wrong dimension'
      endif
   endfor

   return,bproj

end


