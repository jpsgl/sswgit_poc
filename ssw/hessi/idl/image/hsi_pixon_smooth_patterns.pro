;+

pro hsi_pixon_smooth_patterns,iobj,pixon_sizes,smoothed_patterns, $
                              writedir=writedir, $
                              this_det=this_det,this_harm=this_harm, $
                              diffresindex=diffres,diffresvalue=diffresvalue, $
                              quiet=quiet, low_memory=low_memory

;NAME:
;     hsi_pixon_smooth_patterns
;PURPOSE:
;     Compute, read and write smoothed universal modulation patterns
;CATEGORY:
;     HESSI Pixon image reconstruction
;CALLING SEQUENCE:
;     hsi_pixon_smooth_patterns,iobj,pixon_sizes,smoothed_patterns
;INPUTS:
;     iobj = image object
;     pixon_sizes = list of pixon_sizes
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS
;     writedir = directory to save smoothed patterns (only written if they
;                 need to bo computed)
;     this_det = list of detectors to compute or read
;     this_harm = list of harmonics to compute of read
;     diffresindex = to use differential smoothing this is set to the
;                    index of the pixon scale to be differenced.  This is the
;                    index into *complete* pixon_sizes list as used in the
;                    hsi_pixon_local_smooth routine.
;     diffresvalue = pixon size which corresponds to diffresindex
;     /quiet = work quietly
;     /low_momory = Don't save transpose arrays (causes a mysterious memory
;                   leak as of 2000-Oct-20, so don't use this).
;OUTPUTS:
;     smoothed_patterns = pointer array for smoothed patterns.  If this array
;                         has pointers to valid objects on input, they are 
;                         freed before setting to new smoothed patterns
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;PROCEDURE:
;MODIFICATION HISTORY:
;     T. Metcalf 2000-Oct-20
;     T. Metcalf 2001-Aug-29 Changed mpd logic.
;     T. Metcalf 2003-Mar-18 Changed logic so that if a smoothed mod
;                            patt is not read in successfully, then it
;                            is computed.  Previously, if there was an
;                            error in the smoothed patt file, this 
;                            routine would stop with an error.
;     K. Tolbert 2010-Jan-04 Fixed bug where detector 9 alone didn't work.  
;                            Also changed repeat loop to simpler test for valid ptrs.
;     Richard Schwartz, 2011-Mar-02 Set sfile='' so won't use the smoothed modulation pattern
;                            files in SSWDB. They're the wrong dimension.
;-



   smpattfile_version = 'v1'
   compress = 'BYTE'  ; Can be 'FLOAT', 'LONG', 'INT' or 'BYTE'.  
                     ; 'BYTE' has better compression of files
                     ; but 'LONG' is more accurate.  'FLOAT'
                     ; effectively turns off compression.
                     ; IDL saves int's as long's so there is
                     ; not much gain in using int instead of long.

   if strlowcase(!version.os_family) EQ 'windows' then $
      dirsep = '\' $
   else $
      dirsep = '/'

   if keyword_set(writedir) then begin
      if strmid(writedir,strlen(writedir)-1,1) NE dirsep then $
         writedir=writedir+dirsep
   endif

   ; First look for a file.  Generate file name.

   ;mpd = {hsi_modul_pattern_control}
   ;mpd_names = tag_names(mpd)
   ;mpd_names = mpd_names[sort(mpd_names)]
   ;;filename = 'hsi_pixon_smoothed_pattern'
   ;filename = 'hsismp'
   ;for i=0L,n_elements(mpd_names)-1L do begin
   ;   ; Here I assume that these values are constant for a particular
   ;   ; image object and do not change with detector or harmonic number.
   ;   if mpd_names[i] NE 'XYOFFSET' AND $
   ;      mpd_names[i] NE 'FACTOR_BY' AND $
   ;      mpd_names[i] NE 'MODPAT_SKIP' then begin
   ;      check = execute('value = iobj->get(/'+mpd_names[i]+')')
   ;      case strupcase(mpd_names[i]) of
   ;         'IMAGE_DIM':   mname = 'ID'
   ;         'PIXEL_SIZE':  mname = 'SZ'
   ;         'PIXEL_SCALE': mname = 'SC'
   ;         'R0_OFFSET':   mname = 'R0'
   ;         'XAXIS_FOV':   mname = 'XF'
   ;         else:        mname = mpd_names[i]
   ;      endcase
   ;      filename = filename + mname
   ;      for j=0L,n_elements(value)-1L do begin
   ;          v = value[j]
   ;          sv = size(v,/tname)
   ;          if sv eq 'FLOAT' OR sv eq 'DOUBLE' then v = long(v*100.+0.5)
   ;          if j NE 0 then filename = filename + 'x'
   ;          filename = filename + $
   ;                     strtrim(strcompress(string(v),/remove_all),2)
   ;      endfor
   ;   endif
   ;endfor

   ; First look for a file.  Generate file name.

   mpd_names = ['IMAGE_DIM','PIXEL_SIZE','PIXEL_SCALE','R0_OFFSET']
   ;filename = 'hsi_pixon_smoothed_pattern'
   filename = 'hsismp'
   for i=0L,n_elements(mpd_names)-1L do begin
      ; Here I assume that these values are constant for a particular
      ; image object and do not change with detector or harmonic number.
      check = execute('value = iobj->get(/'+mpd_names[i]+')')
      case strupcase(mpd_names[i]) of
         'IMAGE_DIM':   begin mname = 'ID' & mtype='fix' & end
         'PIXEL_SIZE':  begin mname = 'SZ' & mtype='float' & end
         'PIXEL_SCALE': begin mname = 'SC' & mtype='float' & end
         'R0_OFFSET':   begin mname = 'R0' & mtype='float' & end
         else:         message,'Error: unknown name setting up smoothed pattern filename'
      endcase
      filename = filename + mname
      for j=0L,n_elements(value)-1L do begin
          v = value[j]
          if mtype EQ 'fix' then v = long(v+0.5) $
          else if mtype EQ 'float' then v = double(v)
          sv = size(v,/tname)
          if sv eq 'FLOAT' OR sv eq 'DOUBLE' then v = long(v*100.+0.5)
          if j NE 0 then filename = filename + 'x'
          filename = filename + $
                     strtrim(strcompress(string(v),/remove_all),2)
      endfor
   endfor
   filenames = replicate(filename,n_elements(pixon_sizes))
   npixons = n_elements(pixon_sizes)
   for i=0L,npixons-1L do begin
      ;pixon_size_name = '_PIXON_SIZE_'
      pixon_size_name = 'PS'
      filenames[i] = filenames[i] + pixon_size_name + $
                    strtrim(strcompress(string(long(pixon_sizes[i]*100.+0.5)),/remove_all),2)
   endfor

   ;Compute and save the smoothed modulation patterns

   mapptr = iobj->getdata(class_name='hsi_modul_pattern')
;   idettry=-1
;   REPEAT begin
;      idettry = idettry + 1
;      if ptr_valid(mapptr[idettry,0]) then $
;         tags = tag_names(*mapptr[idettry,0])
;   endrep UNTIL ptr_valid(mapptr[idettry,0]) OR idettry EQ 8
;   if idettry EQ 8 then $
;      message,'Could not find valid pointer in modul pattern class!'
;      
   ; 4-jan-10, Kim. Replace above block with this simpler code.  Above was wrong because failed on Detector 9. 
   if total(ptr_exist_arr(mapptr)) eq 0 then message,'Could not find valid pointer in modul pattern class!'
   
   smapptr = size(mapptr)
   ndet = smapptr[1] & nharm = smapptr[2]
   if n_elements(this_det) LE 0 then this_det = indgen(ndet)
   if n_elements(this_harm) LE 0 then this_harm = indgen(nharm)

   ndet = n_elements(this_det)
   ;nharm = n_elements(this_harm)
   nharm = iobj->get(/max_harm)

   if n_elements(smoothed_patterns) GT 0 then begin
      if NOT keyword_set(quiet) then message,/info,'Freeing old smoothed_patterns array'
      for i=0L,n_elements(smoothed_patterns)-1L do begin
         if ptr_valid(smoothed_patterns[i]) then begin
            for j=0L,n_tags(*smoothed_patterns[i])-1L do begin
               if ptr_valid((*smoothed_patterns[i]).(j)) then $
                   ptr_free,(*smoothed_patterns[i]).(j)
            endfor
            ptr_free,smoothed_patterns[i]
         endif
      endfor
   endif 
   if n_elements(smoothed_patterns) NE ndet*npixons then $
      smoothed_patterns = ptrarr(ndet,npixons)
   nxmapold = 0L & nymapold = 0L
   for iidet = 0L,ndet-1L do begin
      idet = this_det[iidet]
      ;if NOT keyword_set(quiet) then print,idet+1,format='($,"Smoothing modulation pattern ",i2," ")'
      if ptr_valid(mapptr[idet]) then begin
         cmap_ptr = (*mapptr[idet]).cmap_ptr
         smap_ptr = (*mapptr[idet]).smap_ptr
         mapsize = size(*cmap_ptr)
         if mapsize[0] NE 3 then message,'Something is very strange about the size of the mod patts'
         nxmap = mapsize[1] & nymap = mapsize[2]
         for iharm = 0L,nharm-1L do begin
            for ipixon=0L,npixons-1L do begin
               file = strtrim(strcompress(filenames[ipixon]+'DT'+string(long(idet))+'HM'+string(long(iharm))+compress,/remove_all),2)
               if n_elements(diffres) GT 0 then $
                  file = strtrim(strcompress(file+'DR'+string(long(diffresvalue*100+0.5)),/remove_all),2)
               file = file + smpattfile_version
               sswdb = getenv('SSWDB')
               if sswdb then $
                  if strmid(sswdb,strlen(sswdb)-1,1) NE dirsep then $
                     sswdb=sswdb+dirsep
               nsubdir = strpos(file,'SZ')
               subdir = strmid(file,0,nsubdir)+dirsep
               lookdirs = [sswdb+'hessi'+dirsep+'imaging'+dirsep+'pixons']
               lookdirs = lookdirs + (dirsep+subdir)
               if keyword_set(writedir) then lookdirs = [writedir,lookdirs]
;               sfile = file_list(lookdirs,file+'.geny',/cd,/quiet)
               sfile=''
               mod_patt_read_ok = 0
               if sfile[0] NE '' then begin
                  mod_patt_read_ok = 1
                  if NOT keyword_set(quiet) AND $
                     ipixon EQ 0 AND $
                     iharm EQ 0 AND $
                     iidet EQ 0 then begin
                     message,/info,'Reading smoothed modulation patterns'
                  endif
                  if NOT keyword_set(quiet) AND $
                     ipixon EQ 0 AND $
                     iharm EQ 0 then begin
                     print,idet+1,format='($,"Reading modulation pattern ",i2," ")'
                  endif
                  delvarx,smoothed_cmap,smoothed_smap
                  ;message,/info,'Restoring '+sfile[0]
                  restgenx,file=sfile[0],smoothed_cmap,smoothed_smap
                  ; Recover the float arrays from the byte scaled arrays
                  case compress of 
                     'BYTE': begin
                        smoothed_cmap = float(smoothed_cmap)/127.5 - 1.0
                        smoothed_smap = float(smoothed_smap)/127.5 - 1.0
                        end
                     'INT' : begin
                        smoothed_cmap = float(smoothed_cmap)/32767.0
                        smoothed_smap = float(smoothed_smap)/32767.0
                        end
                     'LONG' : begin
                        smoothed_cmap = float(double(smoothed_cmap)/2147483647.d0)
                        smoothed_smap = float(double(smoothed_smap)/2147483647.d0)
                        end
                     'FLOAT': begin 
                        end  ; do nothing, already float type
                     else: begin
                            mod_patt_read_ok = 0
                            message,/info,'Bad value of compress parameter (restore)'
                        end
                  endcase
                  if mod_patt_read_ok then begin
                     szcmap = size(smoothed_cmap)
                     szsmap = size(smoothed_smap)
                     if szcmap[1] NE mapsize[1] OR szcmap[2] NE mapsize[2] OR $
                        szsmap[1] NE mapsize[1] OR szsmap[2] NE mapsize[2] then begin
                         ;stop
                         mod_patt_read_ok = 0
                         message,/info,'Restored map is the wrong dimension, recomputing'
                     endif
                  endif
               endif 
               if NOT mod_patt_read_ok then begin  ; could not read smoothed pattern so need to compute it
                  if NOT keyword_set(quiet) AND $
                     ipixon EQ 0 AND $
                     iharm EQ 0 AND $
                     iidet EQ 0 then begin
                     message,/info,'Computing smoothed modulation patterns'
                  endif
                  if NOT keyword_set(quiet) AND $
                     ipixon EQ 0 AND $
                     iharm EQ 0 then begin
                     print,idet+1,format='($,"Smoothing modulation pattern ",i2," ")'
                  endif
                  temp_pixonmap = fltarr(nxmap,nymap)+pixon_sizes[ipixon]
                  ; Must set fft=0 since the cmap and smap
                  ; arrays are not the same size as the image
                  ; array.
                  tsmoothed_cmap = hsi_pixon_local_smooth((*cmap_ptr)[*,*,iharm],temp_pixonmap,pixon_sizes,fft=0,diffres=diffres)
                  tsmoothed_smap = hsi_pixon_local_smooth((*smap_ptr)[*,*,iharm],temp_pixonmap,pixon_sizes,fft=0,diffres=diffres)
                  if keyword_set(writedir) then begin
                     ;message,/info,'Writing '+file
                     ; Arrays are byte scaled to save disk space
                     case compress of
                        'BYTE': begin
                           smoothed_cmap = bytscl(tsmoothed_cmap,max=1.0,min=-1.0)
                           smoothed_smap = bytscl(tsmoothed_smap,max=1.0,min=-1.0)
                           usecompress=1
                           end
                        'INT': begin 
                           smoothed_cmap = fix(((tsmoothed_cmap*32767.)>(-32767.))<32767.)
                           smoothed_smap = fix(((tsmoothed_smap*32767.)>(-32767.))<32767.)
                           usecompress = 1
                           end
                        'LONG': begin
                           smoothed_cmap = long(((tsmoothed_cmap*2147483647.0d0)>(-2147483647.d0))<2147483647.0d0)
                           smoothed_smap = long(((tsmoothed_smap*2147483647.0d0)>(-2147483647.d0))<2147483647.0d0)
                           usecompress = 1
                           end
                        'FLOAT': begin 
                           usecompress = 0  ; Doesn't help with float type, just wastes
                                            ; time
                           end ; do nothing, already float type
                        else: message,'Bad value of compress parameter (save)'
                     endcase
                     savegenx,smoothed_cmap,smoothed_smap, $
                          file=writedir+file,compress=usecompress
                  endif
                  smoothed_cmap = temporary(tsmoothed_cmap)
                  smoothed_smap = temporary(tsmoothed_smap)
               endif
               if NOT ptr_valid(smoothed_patterns[iidet,ipixon]) then begin
                  smoothed_patterns[iidet,ipixon] = $
                     ptr_new({ $
                               cmap_ptr: ptr_new(make_array(mapsize[1],mapsize[2],mapsize[3],size=size(smoothed_cmap))), $
                               smap_ptr: ptr_new(make_array(mapsize[1],mapsize[2],mapsize[3],size=size(smoothed_smap))), $
                               cmapb_ptr: ptr_new(make_array(mapsize[1],mapsize[2],mapsize[3],size=size(smoothed_cmap))), $
                               smapb_ptr: ptr_new(make_array(mapsize[1],mapsize[2],mapsize[3],size=size(smoothed_smap))) $
                             })
               endif
               (*(*smoothed_patterns[iidet,ipixon]).cmap_ptr)[*,*,iharm] = smoothed_cmap
               (*(*smoothed_patterns[iidet,ipixon]).smap_ptr)[*,*,iharm] = smoothed_smap
               (*(*smoothed_patterns[iidet,ipixon]).cmapb_ptr)[*,*,iharm] = transpose(smoothed_cmap)
               (*(*smoothed_patterns[iidet,ipixon]).smapb_ptr)[*,*,iharm] = transpose(smoothed_smap)
               if keyword_set(low_memory) then begin
                  ptr_free,(*smoothed_patterns[iidet,ipixon]).cmapb_ptr
                  ptr_free,(*smoothed_patterns[iidet,ipixon]).smapb_ptr
               endif
               ;tvscl,*cmap_ptr,0,0
               ;tvscl,*smap_ptr,nxmap,0
               ;tvscl,smoothed_cmap,nxmap*2,0
               ;tvscl,smoothed_smap,nxmap*3,0
               nxmapold = nxmap & nymapold = nymap
               if NOT keyword_set(quiet) then print,format='($,".")'
            endfor
         endfor
      endif
      if NOT keyword_set(quiet) then print
   endfor


end
