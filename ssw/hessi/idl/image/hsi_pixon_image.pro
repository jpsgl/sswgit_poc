
;+
;NAME:
;   HSI_PIXON_IMAGE
;PURPOSE:
;     RHESSI Image reconstruction using the fractal pixon basis of Pina and
;     Puetter.
;CATEGORY:
;CALLING SEQUENCE:
;     image = hsi_pixon_image(countrate_obj,sigma[,residual,gof,error])
;INPUTS:
;     countrate_obj = HESSI calib event list
;     sigma = error on the data in "data", same size as data.
;             This is a very important parameter since it
;             is used to find the resolution of the image ...
;             get the best estimate you can!
;             Otherwise use /poisson.
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS
;     sensitivity = sensitivity in creating the pixon map.
;                   default = 0.,
;                   0.0 is most sensitive, >3.0 is very insensitive.  The
;                   default was arrived at after much testing and simulation.
;                   Don't mess with it unless you know what you are doing.
;                   If sensitivity is set too small, you may get spurious
;                   sources; too large and you may miss real sources.
;     guess = initial guess for reconstructed image (def = compute with
;             hxt_qlook).  If guess is not nx by ny or if all elements
;             of guess are LE 0, then the guess is not used.
;     resolution = Minimum resolution in pixels (def = 1).
;     pixon_sizes = an integer list of pixon sizes (resolutions) to use.
;                   default = powers of sqrt(2.0).  Slower, but more robust,
;                   would be something like pixon_sizes=indgen(17).
;     /snr = Weight the residuals by the signal-to-noise ratio.
;     /chisqr = Use chi square statistics rather that poisson statistics.
;               sigma will be used if set.  If sigma is not set,
;               the Poisson statistics ala /poisson are used.
;     /poisson = Use Poisson statistics rather than chi^2.  If this keyword
;                is set, sigma is not used.
;     iterate = returns with the number of iterations used at the final
;               resolution.
;     outresolution = returns with the final resolution, i.e. the minimum
;                     pixon size.
;     /notty = Not running on a tty.  Set this for background jobs.
;     /quiet = Work quietly.
;     btot_in = An estimate of the total # of counts in the image
;                 The default is a weighted sum of the counts in the SC's
;     smpattwritedir = If new smoothed patterns are calculated and this
;                      keyword is set to a valid directory, then the
;                      new smoothed patterns will be saved in that directory.
;                      This keyword is passed through to hsi_pixon_btot and
;                      from there to hsi_pixon_smooth_patterns.
;     /xycoordinate = if set, return the image in XY coordinates rather that
;                     annsec coordinates.
;OUTPUTS:
;     image = reconstructed image
;OPTIONAL OUTPUTS:
;     residual = data residuals
;     gof = goodness of fit
;     error = Outputs the estimated error on the reconstructed image
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;     Convolutions are done with FFT so the procedure is considerably
;     faster when the image size is a power of 2.
;
;     The FFT also wraps the edges, so emission at the edges is not
;     treated properly.  This could be corrected by taking the /fft out
;     of all the calls to hsi_pixon_local_smooth, but that is much
;     slower.  It could also be fixed by setting guard_width>0.
;PROCEDURE:
;MODIFICATION HISTORY:
;     T. Metcalf  1999-Aug-20 Converted HXT code for HESSI use.
;     T. Metcalf  2002-Oct-31 Changed the way the BG pixon is treated
;                             when not included in the pixon_sizes
;                             list.  Also, reset the pseudo to the
;                             image before each image iteration.
;     T. Metcalf  2002-Nov-08 Sometime in the past few months the
;                             normalization of the back projection
;                             changed. This messed up the gradient
;                             calculation here.  Fixed by including the
;                             /no_spatial_freq keyword in broj call.
;     T. Metcal f 2002-Nov-13 Removed the error calculation.  It
;                             should be done at a higher level using
;                             hsi_calc_image_error.pro
;     T. Metcalf 2004-Feb-28  Added the dfpmin option using bit 3 in
;                             dosnr.
;     T. Metcalf 2004-Nov-08  Added error checking around dfpmin
;                             call.
;     T. Metcalf 2007-Jan-07  Added /old to delvarx call so that memory won't be freed
;     T. Metcalf 2007-Mar-19  Guard against crash when pmaptime is undefined
;	  4-may-2010, richard.schwartz@nasa.gov change from number of data pts, nd, to nodropout, the number of
;		true datapoints, the ones with finite livetimes
;	  7-may-2010, richard.schwartz@nasa.gov, added NORM_NOZERO keyword to hsi_pixon_image with default of 1
;		default is to use it with the new way to use NODROPOUT referenced above.
;-


function hsi_pixon_gof_func,image,gradient,nosmooth=nosmooth,local=lcsin,reset=reset,pixon=pixon,residual_image=residual_image,strictgradient=strictgradient

; This function is called in the conjugate gradient minimization.  It is a mess
; because the only way to pass parameters is through common blocks.  Yuck.
;
; If the gradient parameter is present, the chi^2 and gradient of chi^2 are
; computed, else only chi^2 is computed.
;

      common hsi_pixon_gof_private,nx,ny,nxy, $
                                   expansion,tty,not_quiet, $
                                   usepoisson,uselogarithm,usemem, $
                                   old_lcs, old_a2d, $
                                   snr_sigma2, $
                                   local,pimage,pfraction,imax, $
                                   iobj,dobj,data,a2d,harmonics, $
                                   tot_smoothed_patts,sfilename,firstgradcall, $
                                   pixon_sizes,smpattwritedir,bobj,bflat,celist,scelist


      residual_image=0

      if n_elements(lcsin) EQ nxy then lcs = lcsin else lcs = local

      if keyword_set(reset) then begin
         old_lcs = make_array(size=size(lcs),value=0L)
         firstgradcall = 1
      endif

      if uselogarithm then $
         eimage = exp((reform(uselogarithm*image,nx,ny)<Imax)>(-20.)) $
      else begin
         ; positivity constraint w/ MEM
         mximage = max(image)
         if mximage LE 0.0 then image(*) = Imax/1.e32
         eimage = reform(image>(mximage/1.e32),nx,ny)<Imax
      endelse

      ; Apply the pixon map (local correlation scales) to image to obtain
      ; the smoothed image, pimage.

      pimage = hsi_pixon_local_smooth(eimage,lcs,pixon_sizes,fft=1)

      ; MEM Prior
      if usemem then begin
         ptotal = total(pimage)
         lambda = 2.0/ptotal
         mimage = alog((pimage>(max(pimage)/1.e32))/(ptotal*pfraction))
      endif

      ; Update the residuals and the GOF using the smoothed image

      residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj,a2d,harmonics, $
                                      modprofile=recon_data,nounits=0, $
                                      background=bobj)

      ; add in the background so that the algorithm strives to remove
      ; the correlation and bias  with the image rather than the background,
      ; if at all possible
      ;bresiduals = residuals + bflat

      if n_params() EQ 2 then begin
         gof = hsi_pixon_gof_calc(data,recon_data,residuals,snr_sigma2,r2s, $
                                  poisson=usepoisson,iobj=iobj,dobj=dobj, $
                                  a2d=a2d,harmonics=harmonics)
      endif else begin
         gof = hsi_pixon_gof_calc(data,recon_data,residuals,snr_sigma2, $
                                  poisson=usepoisson)
      endelse

      if usemem then gof = gof + lambda*total(pimage*mimage)

      ; Add total flux adjustment ... unbias residuals.  TRM 1997-01-22

      beta = 1.0/total(1./snr_sigma2)
      gof = gof + beta*(total(residuals))^2

      ; Add autocorr adjustment ... eliminate correlations in the residuals

      ;i1 = 0L
      ;for i=0L,n_elements(a2d)-1L do begin  ; Is this the right order???
      ;   this_a2d = a2d[i]
      ;   for j=0L,n_elements(*harmonics[i])-1L do begin
      ;      this_harmonic = (*harmonics[i])[j]
      ;      ndah = n_elements((*celist[this_a2d,this_harmonic]).count)
      ;      fr = fft(residuals[i1:i1+ndah-1],1)
      ;      acorr = float(fft(fr*conj(fr),-1))
      ;      acorr[0] = 0.0  ; The [0] piece is just chi squared
      ;      gof = gof + sqrt(total((snr_sigma2[i1:i1+ndah-1]*acorr)^2)) / $
      ;                  n_elements(a2d)^2
      ;      i1 = i1 + ndah
      ;   endfor
      ;endfor

      if tty then begin
         tvscl,rebin(pimage,nx*expansion,ny*expansion,/sample),nx*0+20,0
         xyouts,nx*expansion/2.+20,0.,/device,align=0.5, $
            strcompress(string(gof)+' '+string(max(pimage))+' '+ $
                        string(min(pimage)))
      endif

      ; Compute gradient of GOF, if requested

      if n_params() EQ 2 then begin

            if n_elements(old_lcs) NE nxy then $
               old_lcs = make_array(size=size(lcs),value=0L)
            if n_elements(old_a2d) LE 0 then old_a2d = a2d
            differ = where(lcs NE old_lcs,ndiffer)

            if ndiffer GT 0 OR n_elements(a2d) NE n_elements(old_a2d) then begin
               old_lcs = lcs
               old_a2d = a2d

               ;******************************************
               ; Set up smoothed mod patterns here
               ;******************************************

              ; sfilename = hsi_pixon_smooth_patterns(iobj,lcs,pixon_sizes,a2d,harmonics,/fast,quiet=NOT not_quiet)

               ; use smoothed patterns here.  If someone hits ctrl-C while
               ; the filename is set to the smoothed patterns, later processes
               ; could be using the wrong mod patts.  How can this be fixed?

              ; filename = iobj->Get(/modpat_filename)   ; save old file name
              ; iobj->Set,modpat_filename=sfilename   ; use smoothed modpatts
               tot_smoothed_patts = hsi_pixon_bproj(make_array(size=size(r2s),value=1.0), $
                                                    dobj,iobj,a2d,harmonics,/vanilla,useunits=1, $
                                                    setunits=firstgradcall,/smoothpatts, $
                                                    pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
                                                    pixonmap=lcs,/nocheck, $
                                                    reset_smoothpatts=firstgradcall,/nonorm, $
                                                    smpattwritedir=smpattwritedir,not_quiet=not_quiet,/no_spatial_freq)
              ; iobj->Set,modpat_filename=filename; restore unsmoothed mod patts
               if keyword_set(firstgradcall) then firstgradcall=0
            endif

            ; use smoothed patterns here. If someone hits ctrl-C while
            ; the filename is set to the smoothed patterns, later processes
            ; could be using the wrong mod patts.  How can this be fixed?

         ;   filename = iobj->Get(/modpat_filename)   ; save old file name
         ;   iobj->Set,modpat_filename=sfilename      ; use smoothed modpatts
            gradient = hsi_pixon_bproj(r2s,dobj,iobj,a2d,harmonics, $
                                       vanilla=1,useunits=1, $
                                       setunits=firstgradcall, $
                                       /nocheck, $
                                       reset_smoothpatts=firstgradcall, $
                                       pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
                                       pixonmap=lcs, $
                                       smoothpatts=1,/nonorm, $
                                       smpattwritedir=smpattwritedir,not_quiet=not_quiet,/no_spatial_freq)

          ;  iobj->Set,modpat_filename=filename       ; restore unsmoothed mod patts
            if keyword_set(firstgradcall) then firstgradcall=0

            ;; Test Gradient.  Normally commented out
            ;tgradient = fltarr(nx,ny)
            ;txlist=[32,31,30,29,28,32];,32]
            ;tylist=[32,31,30,29,28,27];,16]
            ;for i=0L,n_elements(txlist)-1L do begin
            ;   tx=txlist[i]
            ;   ty=tylist[i]
            ;   teimage = double(eimage)
            ;   delta = teimage[tx,ty]*0.1d0
            ;   teimage[tx,ty] = teimage[tx,ty]+delta
            ;   tpimage = hsi_pixon_local_smooth(teimage,lcs,pixon_sizes,fft=1)
            ;   tresiduals = hsi_pixon_residuals(reform(tpimage,nxy),dobj,iobj,a2d,harmonics, $
            ;                                    modprofile=trecon_data,nounits=0,background=bobj)
            ;   gof1 = hsi_pixon_gof_calc(data,recon_data,residuals,snr_sigma2, $
            ;                            r2s,poisson=usepoisson,iobj=iobj,dobj=dobj, $
            ;                            a2d=a2d,harmonics=harmonics)
            ;   gof2 = hsi_pixon_gof_calc(data,trecon_data,tresiduals,snr_sigma2, $
            ;                            tr2s,poisson=usepoisson,iobj=iobj,dobj=dobj, $
            ;                            a2d=a2d,harmonics=harmonics)
            ;   print,(gof2-gof1)/delta,(reform(gradient,nx,ny))[tx,ty]
            ;   tgradient[tx,ty] = (gof2-gof1)/delta
            ;   tss = where(tgradient NE 0.,ntss)
            ;   ;plot,tgradient[tss],gradient[tss],psym=1
            ;endfor
            ;print,mean(gradient[tss]/tgradient[tss]), $
            ;    stddev(gradient[tss]/tgradient[tss])
            ;stop

            if NOT keyword_set(strictgradient) then begin
               ; gradient of unbiased residuals term.

               gradient = gradient+(2.0*beta*total(residuals))*tot_smoothed_patts

               if usemem then begin
                     gradient = gradient + lambda*hsi_pixon_local_smooth((1.+mimage),lcs,pixon_sizes,fft=1)
               endif
            endif

            if tty then begin
               tvscl,rebin(reform(gradient,nx,ny),nx*expansion,ny*expansion,/sample),nx*expansion+20,0
               xyouts,1.5*nx*expansion+20,0.,/device,align=0.5, $
                      strcompress(string(max(gradient))+' '+string(min(gradient)))
            endif

            ; If the log of the image is being used, then the gradient should
            ; be multiplied by the image itself since d/d(ln x) = x * d/dx.

            if keyword_set(uselogarithm) then $
               gradient=uselogarithm*gradient*eimage
            ;if keyword_set(uselogarithm) then $
            ;   gradient=uselogarithm*gradient*pimage
            ;if keyword_set(uselogarithm) then $
            ;   gradient=uselogarithm*gradient* $
            ;            hsi_pixon_local_smooth(eimage,fltarr(nx,ny)+min(lcs), $
            ;                         pixon_sizes,fft=1)

        endif

      ;if NOT finite(gof) then stop
      ;if n_elements(gradient) GT 0 then $
      ;   if min(finite(gradient)) LE 0 then stop

      return,gof

end

;-----------------------------------------------

function hsi_pixon_dfp_grad_func,image
   common hsi_pixon_dfp_private,dfpscale
   gof = hsi_pixon_gof_func(image*dfpscale[1],gradient)/dfpscale[0]
   return,gradient*(dfpscale[1]/dfpscale[0])
end

function hsi_pixon_dfp_gof_func,image
   common hsi_pixon_dfp_private,dfpscale
   gof = hsi_pixon_gof_func(image*dfpscale[1])/dfpscale[0]
   return,gof
end

;-----------------------------------------------

function hsi_pixon_map,image,iobj,local,data,dobj,sobj,snr_sigma2,resolution,nx,ny,nxy,nd, $
                       pixon_sizes,a2d,harmonics,pfraction=pfraction, $
                       uselogarithm=uselogarithm,Imax=Imax,npixons=npixons, $
                       usepoisson=usepoisson,sensitivity=sensitivity, $
                       a2d_resolution=a2d_resolution, $
                       not_quiet=not_quiet,tty=tty,expansion=expansion, $
                       fast=fast,smpattwritedir=smpattwritedir, $
                       nolastprint=nolastprint,bobj=bobj,bflat=bflat

;NAME:
;     HSI_PIXON_MAP
;PURPOSE:
;     Computes the pixon map
;CATEGORY:
;CALLING SEQUENCE:
;     pmap = hsi_pixon_map(image,local,data,snr_sigma2,resolution, nx,ny,nd,
;                          guard_width,patt,pixon_sizes)
;INPUTS:
;     image = the current image estimate
;     local = the old pixon map
;     data = the data
;     snr_sigma2 = 1/sigma^2 for each data point
;     resolution = the new pixon size to try at each pixel
;     nx,ny,nd = the dimensions of the image and data
;     pixon_sizes = the list of all pixon sizes
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS:
;     sensitivity = sensitivity in creating the pixon map.
;                   default = 0.0,
;                   0.0 is most sensitive, 3.0 is very insensitive.
;     /uselogarithm = the image is the log of the true image
;     Imax = the maximum allowed intensity in image.
;     /usepoisson = Use Poisson distribution for GOF
;     /fast = Use the fast algorithm to compute the pixon map.  It is
;             approximate, but MUCH faster than the exact algorithm.
;OUTPUTS:
;     pmap = the new pixon map
;     npixons = the number of pixons in the computed pixon map (keyword)
;     pfraction = the fraction of a pixon at each pixel
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;PROCEDURE:
;MODIFICATION HISTORY:
;     T. Metcalf 1995-08-14
;                1995-08-22 Modified calculation of change in DOF.
;                           Added calculation of npixons.
;                1995-09-01 Added calculation of pfraction, the fraction
;                           of a pixon at each pixel.
;                1996-03-06 Allow the pixon map to be reset every time
;                           this routine is called (not currently used).
;                           Also allow pixon size to drop by more than 1 step.
;                           Add confidence level to chi^2 test.
;                1996-06-17 Added "fill" to deal with the edges when the
;                           guard width size is less than the largest pixon.
;     T. Metcalf 1996-06-21 Keep track of pixon changes as the pixon map is
;                           calculated (dr2s stuff).
;     T. Metcalf 1996-07-05 Added median filter at the end.  Also changed to
;                           recompute non-background pixons from scratch
;                           at every step.
;     T. Metcalf 1996-07-08 Took out median filter.  It wasn't necessary with
;                           the recompute at every step.
;     T. Metcalf 1996-07-26 -sigma set to sqrt(2).
;                           -Order of x,y and i loops reversed.
;                           -Pixon resolution is frozen if gof goes below nd.
;                           -edgefill set to 1.0 since /fft is in calls to
;                            hsi_pixon_local_smooth.
;     T. Metcalf 1996-08-02 -Added the sensitivity keyword.
;     T. Metcalf 1996-10-01 -Added call to hxt_error.
;     T. Metcalf 1997-01-22 -Changed default sensitivity to 0.0
;
;     T. Metcalf 1999-Dec-13 - Adapted hxt_pixon_map for HESSI use.
;     T. Metcalf 2000-Feb-23 - Added fast keyword with much faster calculation
;                              of the pixon map.
;     T. Metcalf 2002-Nov-12 - Fixed bug in calculation of pfraction.


   tlocal = local
   nx1 = nx-1
   ny1 = ny-1

   xyindex = lindgen(nx,ny)

   if n_elements(sensitivity) NE 1 then sensitivity = 0.  ;1./sqrt(2.) ;;1.4142135
   sensitivity = float(sensitivity[0])

   if uselogarithm then $
      eimage = exp((reform(uselogarithm*image,nx,ny)<Imax)>(-20)) $
   else $
      eimage = reform(image>(max(image)/1.e32),nx,ny)<Imax

   top = max(pixon_sizes)
   revpixon_sizes = pixon_sizes(rotate(sort(pixon_sizes),2))
   npixon_sizes = n_elements(pixon_sizes)
   if npixon_sizes GT 1 then nextop = revpixon_sizes(1) else nextop = top

   ; Get ipixon, the index of resolution in pixon_sizes
   junk = min(abs(pixon_sizes - resolution),ipixon)

   ; Get list of pixon sizes to consider (all larger than (or =) resolution).
   ; These sizes will be tested for each pixel.

   ; -----------------------------------------------------

   ; Choose one of the following blocks, comment out the others.
   ; The selections go grom most robust to fastest.

   ; check of all possible pixon sizes

   ;itestsizes = where(pixon_sizes GE pixon_sizes(ipixon),ntestsizes)
   ;; Reset the pixon map except where it is background pixon
   ;if pixon_sizes(ipixon) LT nextop then begin
   ;   foreground = where(tlocal LT nextop,nforeground)
   ;   if nforeground GE 1 then tlocal(foreground) = nextop
   ;endif

   ; check only sizes within 5 of current size

   ;itestsizes = where((pixon_sizes GE pixon_sizes(ipixon)) AND $
   ;                   (pixon_sizes LE (pixon_sizes(ipixon)+5)),ntestsizes)

   ; checks only the smallest and the background and preserves all larger scales.
   ;itestsizes = where(pixon_sizes EQ pixon_sizes(ipixon) OR $
   ;                   pixon_sizes EQ top,ntestsizes)

   ; checks only the smallest and preserves all larger scales unless we are at
   ; the top 3 sizes and then it also checks if BG is better.
   ;if pixon_sizes[ipixon] GE revpixon_sizes[2] then $
   ;   itestsizes = where(pixon_sizes EQ pixon_sizes(ipixon) OR $
   ;                      pixon_sizes EQ top,ntestsizes) $
   ;else $
   ;   itestsizes = where(pixon_sizes EQ pixon_sizes[ipixon],ntestsizes)

   ; checks only the smallest and preserves all larger scales.
   itestsizes = where(pixon_sizes EQ pixon_sizes(ipixon),ntestsizes)

   ; -----------------------------------------------------

   ; Apply the pixon map (local correlation scales) to image to obtain
   ; the smoothed image, pimage.  Also sets psf etc.

   pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,psf,sdels,xcen,ycen,fft=1)

   npsf = n_elements(psf(0,0,*))

   ; Lookup table: which pixon shape function corresponds to each resolution

   slookup = lonarr(npixon_sizes)
   for i=0L,npixon_sizes-1L do begin
      junk = min(abs(sdels*2-pixon_sizes(i)),isdels)
      slookup(i) = isdels
   endfor

   ipsfnew = slookup(ipixon)

   ; Change in DOF lookup table.  Change in DOF when changing from any valid
   ; old resolution sized pixons to the size we are checking

   ; The number of DOFs is 1/total(psf) at each resolution.  Hence the
   ; change in DOFs is 1/total(psf1) - 1/total(psf2).  PSF's set to max
   ; value one here.

   totpsf = dblarr(npsf)         ; Get integral of re-normalized PSF's
   for i=0L,npsf-1L do begin
      totpsf(i)=total(double(psf[*,*,i])/max(psf[*,*,i]))
   endfor
   dDOFlookup = dblarr(npixon_sizes,ntestsizes)   ; must be initialized to zero
   for i=0L,ntestsizes-1L do begin
      good = where(pixon_sizes NE pixon_sizes[itestsizes[i]],ngood)
      if ngood GT 0 then $
         dDOFlookup(good,i) = $
            (1.d0/totpsf[slookup[itestsizes[i]]] - 1.d0/totpsf[slookup[good]])
   endfor

   ; Loop through every pixel.  Check if a change in resolution
   ; is supported by the data.  If it is, the GOF will decrease more
   ; than the increase in DOF.

   npixons = 0.0d0
   dr2s = dblarr(nd)

   pfraction = double(local)  ; Just to make it the right size.
   pfraction[*] = 1.d0/max(totpsf)  ; For the guard ring.
   for i=0L,npixon_sizes-1 do begin ; set pfraction for the current local
      good = where(abs(local-pixon_sizes[i]) LT 0.1,ngood)
      if ngood GT 0 then pfraction[good] = 1.d0/totpsf[slookup[i]]
   endfor

   ; The pixon sizes must be reverse sorted so that we always
   ; end up with the largest acceptable pixon.
   sortsizes = rotate(sort(pixon_sizes(itestsizes)),2)  ; reverse sort
   ; Old_test_size is the pixon size below which we no longer check backfround
   ; pixels.  I.e. if a pixel is a background pixel (largest pixon size) then
   ; it will not be tested further once the resolution is below old_test_size.
   ; if set to revpixon_sizes(2) then back ground pixels will be checked for
   ; the largest, 2nd largest, and 3rd largest, etc.
   ;old_test_size = revpixon_sizes(1<(npixon_sizes-1)) ; worth a try
   old_test_size = revpixon_sizes(2<(npixon_sizes-1)) ; HXT standard
   ;old_test_size = revpixon_sizes(5<(npixon_sizes-1)) ; very conservative
   ; New_test_size is the pixon size above which we check if a pixel wants to
   ; become a background pixel.
   ;new_test_size = revpixon_sizes(4<(npixon_sizes-1)) ; 5 is very conservative
   new_test_size = revpixon_sizes(2<(npixon_sizes-1)) ; 5 is very conservative
   ;new_test_size = revpixon_sizes(npixon_sizes-1)  ; turn this feature off

   ;pimage = hsi_pixon_local_smooth(eimage,tlocal,pixon_sizes,fft=1)
   ;residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj,a2d,harmonics,modprofile=recon_data,nounits=0,background=bobj)
   ;gof = hsi_pixon_gof_calc(data,recon_data,residuals,snr_sigma2,r2s, $
   ;                         poisson=usepoisson,iobj=iobj,dobj=dobj, $
   ;                         a2d=a2d,harmonics=harmonics)

   lastpercentdone = -1L

   for i=0L,ntestsizes-1L do begin

      if not_quiet then begin
         print,i,pixon_sizes(itestsizes(sortsizes(i))),format='(i4,f9.3," ",$)'
      endif
      ;if keyword_set(fast) then print

      pixel_size = min(iobj->Get(/pixel_size))  ; in arcseconds
      resarcsec = pixon_sizes[itestsizes(sortsizes(i))]*pixel_size  ; pixon size in arcsec
      ; This is potentially slower than it should be because it is does not
      ; account for the different resolution of the harmonics
      if keyword_set(fast) then begin
         ; The fast algorithm is fast so don't need the speed increase.
         ; Just use all the a2d's
         ssa2d = lindgen(n_elements(a2d))
      endif else begin
         ssa2d = where(a2d_resolution[a2d,0] GE resarcsec/3.,nssa2d)
      endelse
      a2d_reduced = a2d[ssa2d]
      harmonics_reduced = harmonics[ssa2d]
      data_reduced = hsi_pixon_reduced_data(dobj, $
                                            a2d_reduced, $
                                            harmonics_reduced)
      snr_sigma2_reduced = hsi_pixon_reduced_data(sobj, $
                                            a2d_reduced, $
                                            harmonics_reduced)
      ;bflat_reduced = hsi_pixon_reduced_data(bobj, $
      ;                                      a2d_reduced, $
      ;                                      harmonics_reduced)

      ; Apply pixon map

      pimage = hsi_pixon_local_smooth(eimage,tlocal,pixon_sizes,fft=1)
      residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj,a2d_reduced,harmonics_reduced,modprofile=recon_data,nounits=0,background=bobj)
      gof = hsi_pixon_gof_calc(data_reduced,recon_data,residuals,snr_sigma2_reduced,r2s, $
                               poisson=usepoisson)

      ;if gof GE n_elements(residuals) then begin
      if 1 then begin

         newres = pixon_sizes(itestsizes(sortsizes(i)))

         if keyword_set(fast) then begin
            ; If tlocal is everywhere equal to newres, then skip the
            ; calculation.
            if (min(tlocal) NE newres OR max(tlocal) NE newres) then begin
            ; watch out that the diffnewres index has the same meaning as
            ; the indices into pixon_sizes in hsi_pixon_local_smooth.pro
               junk = min(abs(pixon_sizes[sort(pixon_sizes)]-newres),diffnewres)
               ;message,/info,'Running GC before pixon map'
               ;heap_gc,/verbose
               bpdiff = hsi_pixon_bproj(r2s,dobj,iobj,a2d_reduced,harmonics_reduced, $
                                        /vanilla,/useunits, $
                                        /smoothpatts,diffres=diffnewres, $
                                        pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
                                        pixonmap=tlocal,/nonorm,/nocheck, $
                                        smpattwritedir=smpattwritedir,not_quiet=not_quiet,/no_spatial_freq)
               ;message,/info,'Running GC after pixon map'
               ;heap_gc,/verbose
               dGOF = eimage*bpdiff   ; Unit problem here??
               dDOF = fltarr(nx,ny)
               for ioldpixon = 0L,n_elements(pixon_sizes)-1L do begin
                  oldres = pixon_sizes(ioldpixon)
                  ssold = where(tlocal EQ oldres,nss)
                  if nss GT 0 then begin
                     ipsfold = slookup(ioldpixon)
                     dDOF[ssold] = dDOFlookup(ioldpixon,sortsizes(i))
                     if sensitivity GE 0.0 then begin
                        signDOF = 2*(dDOF[ssold] GE 0.)-1
                        sigmaDOF = 1.+signDOF*sensitivity/sqrt(2.0d0/totpsf(ipsfold))
                        dDOF[ssold] = dDOF[ssold]*sigmaDOF
                     endif
                  endif
               endfor
               ; Test all scales and allow pixon size to go up or down
               if sensitivity LT 0.0 then $
                  good = where(dGOF GT dDOF/(1.-sensitivity) AND $
                               tlocal NE newres,ngood) $
               else $
                  good = where(dGOF GT dDOF AND tlocal NE newres,ngood)
               ; Pixon size can only get smaller
               ;good = where(dGOF GT dDOF AND tlocal GT newres,ngood)
               if ngood GT 0 then begin
                  junk = min(abs(pixon_sizes - newres),inewpixon)
                  ipsfnew = slookup(inewpixon)
                  tlocal[good]=newres
                  pfraction[good] = 1.0/totpsf[ipsfnew]
               endif
            endif
         endif else begin

            for y=0L,ny-1L do begin
               for x=0L,nx-1L do begin

                  if not_quiet then begin
                     ; Print some diagnostics to let the user know how things are
                     ; progressing.
                     percentdone = long(100.*float(y*nx+x+1)/float(nxy))
                     if percentdone MOD 5L EQ 0L then begin
                        if percentdone NE lastpercentdone then begin
                           lastpercentdone = percentdone
                           if percentdone MOD 10L EQ 0L then begin
                              if percentdone LT 100 then $
                                 print,percentdone,format='(i2,$)' $
                              else $
                                 print,percentdone,format='(i3,$)'
                           endif else $
                              print,format='(".",$)'
                        endif
                     endif
                  endif

                  oldres = tlocal(x,y)
                  junk = min(abs(pixon_sizes - oldres),ioldpixon)
                  ipsfold = slookup(ioldpixon)
                  pfraction(x,y) = 1.0d0/totpsf(ipsfold)
                  dr2s(*) = 0.0

                  ;newres = pixon_sizes(itestsizes(sortsizes(i)))
                  toldres = tlocal(x,y)
                  tfraction = pfraction(x,y)

                  ; is this an isolated background pixon?  If a bg pixon is not
                  ; isolated, it is checked to see if it should be changed,
                  ; otherwise it is not checked at all once old_test_size is
                  ; reached.  If newres is background, and we're lower than
                  ; new_test_size, the pixels are normally not checked (for
                  ; speed).  However, if adjacent to a background pixel it
                  ; will be checked.  Note: after a few tests this doesn't seem
                  ; to do anything.  It is just the same as completely ignoring
                  ; background pixels once we reach old_test_size and
                  ; new_test_size.

                  ;isolatedbg = 0
                  ;adjacentbg = 0
                  ;if oldres EQ top then begin
                  ;   if min(tlocal((x-1)>0:(x+1)<nx1, $
                  ;                 (y-1)>0:(y+1)<ny1)) EQ top then $
                  ;      isolatedbg = 1
                  ;endif
                  ;if newres EQ top then begin
                  ;    if max(tlocal((x-1)>0:(x+1)<nx1, $
                  ;                 (y-1)>0:(y+1)<ny1)) EQ top then $
                  ;       adjacentbg = 1
                  ;endif

                  isolatedbg = 1 ; Made no difference so turn off test (it's slow)
                  adjacentbg = 0

                  ; (newres NE oldres) since we should only change the resolution.
                  ; ((pixon_sizes(ipixon) GE new_test_size) OR (newres NE top) OR adjacentbg) so that we
                  ; don't set anything to the background pixon after reach new_test_size.
                  ; ((pixon_sizes(ipixon) GE old_test_size) OR (oldres NE top))
                  ; ((pixon_sizes(ipixon) GE old_test_size) OR (NOT isolatedbg))
                  ; so that we check every pixel to see if it wants to become a
                  ; background pixon until we reach old_test_size and then
                  ; leave isolated background pixels alone from then on (this
                  ; restriction is only for speed).

                  if (newres NE oldres) AND $
                     ((newres NE top) OR adjacentbg OR (pixon_sizes(ipixon) GE new_test_size)) AND $
                     ((oldres NE top) OR (NOT isolatedbg) OR (pixon_sizes(ipixon) GE old_test_size)) then begin
                     tlocal(x,y) = newres   ; Try new resoltuion
                     junk = min(abs(pixon_sizes - newres),inewpixon)
                     ipsfnew = slookup(inewpixon)
                     maxres = max([oldres,newres])
                     xyr = long([-long(maxres/2L)-1,+long(maxres/2L)+1])
                     xr = (xyr > (-x)) < (nx1-x)
                     yr = (xyr > (-y)) < (ny1-y)
                     psfx = xcen+xr
                     psfy = ycen+yr
                     pattx = x+xr
                     patty = y+yr
                     ;nxs = xr(1)-xr(0)+1
                     ;nys = yr(1)-yr(0)+1
                     ; edgefill should not be used if /FFT is set in hsi_pixon_local_smooth
                     edgefill = 1.0   ; for /fft set edgefill to 1.0
                     ;;edgefill = (nxs*nys)/(2.*maxres+1.)^2  ; approximate pixon footpoint by a square for speed (edgefill=1 except at the edges)

                     ; ds has the "wrong" sign so that dGOF will be
                     ; positive.  This means
                     ; that dRESID also has the "wrong" sign and
                     ; should be subtracted from r2s below.

                     ds = (psf(psfx(0):psfx(1),psfy(0):psfy(1),ipsfold) - $
                           psf(psfx(0):psfx(1),psfy(0):psfy(1),ipsfnew))

                     ; With the /noresidual keyword set, hsi_pixon_resdiuals
                     ; returns the mod profile.

                     dRESID = eimage(x,y) * $
                              hsi_pixon_residuals(reform(ds,n_elements(ds)), $
                                            dobj,iobj, $
                                            a2d_reduced,harmonics_reduced, $
                                            /noresidual, $
                                            sspatt=xyindex[pattx[0]:pattx[1], $
                                                           patty[0]:patty[1]],nounits=0,background=bobj)

                     dGOF = total(r2s*dRESID)
                     dDOF = dDOFlookup(ioldpixon,sortsizes(i))*edgefill


                     signDOF = 2*(dDOF GE 0.)-1

                     sigmaDOF = 1.+signDOF*sensitivity/sqrt(2.0d0/totpsf(ipsfold))

                     dDOF = dDOF*sigmaDOF

                     if (dGOF GE dDOF) then begin
                        pfraction(x,y) = double(edgefill)/totpsf(ipsfnew)  ; keep new resolution

                          ; It is not necessary to keep track of changes
                          ; to r2s.  This just forces the image to be
                          ; updated as the pixon map is calculated and
                          ; really the image should be held fixed.


                    ;    if (n_elements(r2s) NE n_elements(dRESID)) then $
                    ;       message,'ERROR: dRESID and r2s do not have the same number of elements'
                        if keyword_set(usepoisson) then begin
                           dr2s = 2.0*dRESID*(1.0-0.5*r2s)^2/data_reduced
                        endif else begin
                           dr2s = dRESID*2.0*snr_sigma2_reduced  ; save to update r2s
                        endelse

                        ;; Code to test whether dr2s is correct.  Normally this
                        ;; bit of code is commented out
                        ;if y GT newres AND y LT ny-newres AND $
                        ;   x GT newres AND x LT nx-newres then begin
                        ;   otlocal = tlocal
                        ;   otlocal(x,y) = toldres
                        ;   opimage = hsi_pixon_local_smooth(eimage,otlocal,pixon_sizes,fft=1)
                        ;   npimage = hsi_pixon_local_smooth(eimage,tlocal,pixon_sizes,fft=1)
                        ;   oresiduals = hsi_pixon_residuals(reform(opimage,nxy), $
                        ;                                   dobj,iobj,a2d,harmonics, $
                        ;                                   modprofile=orecon_data,nounits=0,background=bobj)
                        ;   nresiduals = hsi_pixon_residuals(reform(npimage,nxy), $
                        ;                                   dobj,iobj,a2d,harmonics, $
                        ;                                   modprofile=nrecon_data,nounits=0,background=bobj)
                        ;   ogof = hsi_pixon_gof_calc(data,orecon_data,oresiduals, $
                        ;                            snr_sigma2,or2s, $
                        ;                            poisson=usepoisson,iobj=iobj,dobj=dobj, $
                        ;       a2d=a2d,harmonics=harmonics)
                        ;   ngof = hsi_pixon_gof_calc(data,nrecon_data,nresiduals, $
                        ;                            snr_sigma2,nr2s, $
                        ;                            poisson=usepoisson,iobj=iobj,dobj=dobj, $
                        ;       a2d=a2d,harmonics=harmonics)
                        ;   test_dr2s = or2s-nr2s ; compare this to dr2s
                        ;   tvscl,opimage-npimage
                        ;   plot,test_dr2s[0:1000]
                        ;   oplot,dr2s[0:1000],color=128
                        ;   plot,oresiduals-nresiduals
                        ;   oplot,dRESID,color=128
                        ;   plot,nr2s[0:100]
                        ;   oplot,(r2s-dr2s)[0:100],color=128
                        ;   stop
                        ;endif


                        r2s = r2s - dr2s  ; update r2s
                     endif else begin
                        tlocal(x,y) = toldres                              ; Restore old resolution
                        pfraction(x,y) = tfraction
                     endelse

                  endif
               endfor
               ;;r2s = r2s - dr2s  ; update r2s
               ;;npixons = npixons + pfraction(x,y)
            endfor
         endelse   ; End of SLOW version
      endif

      ; Reset r2s periodically.  This shouldn't be necessary since I
      ; keep track of changes to r2s with dr2s below.  However, dr2s may
      ; build up error so reseting every now and again seems wise.
      ; If usepoisson is set, recon_data should be recomputed every time
      ; the pixon map changes, but that is too slow.

      pimage = hsi_pixon_local_smooth(eimage,tlocal,pixon_sizes,fft=1)
      residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj,a2d_reduced,harmonics_reduced,modprofile=recon_data,nounits=0,background=bobj)
      gof = hsi_pixon_gof_calc(data_reduced,recon_data,residuals,snr_sigma2_reduced,r2s, $
                               poisson=usepoisson,iobj=iobj,dobj=dobj, $
                            a2d=a2d,harmonics=harmonics)
      if not_quiet then $
         print,gof/n_elements(residuals),format='(" ",f12.3)'
      if tty then $
         tv,rebin(bytscl(tlocal,min=newres, $
                               max=max(pixon_sizes), $
                               top=!d.table_size-2), $
                               nx*expansion,ny*expansion,/sample), $
                  nx*expansion+20,ny*expansion
   endfor
   if not_quiet AND NOT keyword_set(nolastprint) then $
      print
   npixons = total(pfraction)

   ; Smooth the pixon map by the current resolution to get rid of too
   ; small structure

   ;background = where(tlocal EQ top,nbackground)
   ;foreground = where(tlocal NE top,nforeground)

   ;;tlocal = smooth(tlocal,ceil(pixon_sizes(ipixon))>2,/edge_truncate,/nan)<tlocal
   ;tlocal = median(tlocal,ceil(pixon_sizes(ipixon))>2)<tlocal

   ;; Can't smooth to a background pixon since background pixons
   ;; are not always checked in the next iteration.

   ;bad = where(tlocal(foreground) GE (top+nextop)/2.0,nbad)
   ;if nbad GT 0 then tlocal(foreground(bad)) = nextop

   ;; Make sure the smoothing did not mess with the background
   ;; pixons

   ;if nbackground GT 0 then tlocal(background) = top

   return,tlocal

end

;-----------------------------------------------

function hsi_pixon_image,iobjin,sigmai,residual,rgof,error, $
                iterate=iteration, $
                quiet=quiet, $
                NORM_NOZERO=NORM_NOZERO,$ ;if set, normalize stat by number non-zero exp values
                resolution=minresin,snr=dosnr,guess=guessin, $
                pixon_sizes=inpixon_sizes,notty=notty, $
                outresolution=outresolution,poisson=poisson,chisqr=chisqr, $
                sensitivity=sensitivity,pixonmap=pixonmap,btot_in=btot_in, $
                trueimage=trueimage,smpattwritedir=insmpattwritedir, $
                progress_bar=progress_bar, $	;kim
                xycoordinate=xycoordinate

   common hsi_pixon_dfp_private,dfpscale
   common hsi_pixon_gof_private,nx,ny,nxy, $
                                expansion,tty,not_quiet, $
                                usepoisson,uselogarithm,usemem, $
                                old_lcs, old_a2d, $
                                snr_sigma2, $
                                local,pimage,pfraction,imax, $
                                iobj,dobj,datac,a2d,harmonics, $
                                tot_smoothed_patts,sfilename,firstgradcall, $
                                pixon_sizes,smpattwritedir,bobj,bflat,celist,scelist

   message,/info,'Last modification 2005-Mar-15.'
   ;message,/info,'Image Units: photons/coll/sec'


   ; Set up
   default, NORM_NOZERO, 1 ;ras, default is to count only the used data, zero predicted counts are not used
   ;previously, every data bin, even with zero pred counts (zero livetime) were counted
   ;number of valid bins is used to normalize/reduce the Cstat, 7-may-2010, ras
   ;does not affect the images! so we don't need to have a user path (control parameter) to go
   ;back to the old way

   uselogarithm = 0   ; -1,0,+1 ONLY.  -1 = ln(1/I); 0 = linear; +1 = +ln(I)
   usemem = 1

   iobj = iobjin  ; copies pointer only

   if keyword_set(insmpattwritedir) then smpattwritedir = insmpattwritedir

   ; For some reason if the pixon code is called directly after the
   ; forward fitting code, things are messed up.  Hence this call.
   ; This also forces the calculation of mod patts etc. if they are
   ; not already computed.
   junk = iobj->getdata(image_algorithm='back projection')  ; do a back projection to make sure all is set up

   celist = iobj->GetData(class_name='hsi_calib_eventlist')
   scelist  = size(celist)

   ; Choose one of these statements
   ;if (keyword_set(poisson) AND NOT keyword_set(chisqr)) OR n_elements(sigmai) NE n_elements(data) then $
   ;   usepoisson = 1 else usepoisson=0  ; chisqr default
   if (keyword_set(chisqr) AND NOT keyword_set(poisson)) AND n_elements(sigmai) eq n_elements(data) then $
      usepoisson = 0 else usepoisson=1  ; poisson default

   if NOT keyword_set(quiet) then begin
      if keyword_set(usepoisson) then message,/info,'Poisson statistics' $
      else message,/info,'chisqr statistics'
   endif

   usederivative = 1
   usedouble = 0
   not_quiet = 1-keyword_set(quiet)
   if keyword_set(notty) then tty = 0 else tty = 1

   if tty then begin
      ; save the color table and set it to something else
      ; This prevents an interaction with the HESSI GUI
      tvlct,/get,rsave,gsave,bsave
      loadct,5
   endif

   old_lcsgoffunc = 0  ; reset

   ; dosnr has become a flag variable with each bit turning on some
   ; feature.
   ; bit 0 = keyword_set(dosnr AND 1) = use SNR
   ; bit 1 = keyword_set(dosnr AND 2) = use full pixon map calculation
   ; bit 2 = keyword_set(dosnr AND 4) = do background estimation
   ; bit 3 = keyword_set(dosnr AND 8) = use dfpmin instead of conj grad

   if n_elements(dosnr) LE 0 then dosnr = 0

   if keyword_set(dosnr AND 1) then use_snr = 1 else use_snr = 0
   if keyword_set(use_snr) then message,/info,'Using SNR weighting'

   if keyword_set(dosnr AND 8) then use_dfpmin=1 else use_dfpmin=0


   dobj = hsi_pixon_get_data(iobj, $
                             a2dindex=a2d, $
                             nodropout = nresid, $;this is the total number of usable data bins
                             harmonics=harmonics, $
                             dflat=data,sigma=sigma,bobj=bobj,bflat=bflat, $
                             /nounits,snr=dosnr)

   szdobj = size(dobj)
   nd = n_elements(data)
   nx = long((iobj->get(/rmap_dim))[0])
   ny = long((iobj->get(/rmap_dim))[1])
   nxy = nx*ny

   expansion = long(256./nx + 0.5)>1L ;4

   if tty then begin
      ;message,/info,'Type "S" to skip to the next resolution.'
      ;message,/info,'Type "Q" to exit and return the latest image.'
      window,/free,xsize=nx*expansion*2+40,ysize=ny*expansion*2, $
             title='RHESSI Pixon Image Reconstruction'
   endif

   if min(data) LT 0 then $
      message,/info,'Negative data ... bad background subtraction?'

   ;if n_elements(minresin) ne 1 then minres = 0L $
   ;else minres = (long(minresin)>1L)<8L              ; Minimum local smoothing
   if n_elements(minresin) ne 1 then minres = 1.0 $
   else minres = (float(minresin)>1.)<8.              ; Minimum local smoothing
   top = (sqrt(nxy)/4)>minres                        ; Maximum local smoothing
   bgpixon_size = (sqrt(nxy)/2)>minres        ; Size of a background pixon

   if keyword_set(inpixon_sizes) then begin

      if size(inpixon_sizes,/tname) EQ 'POINTER' then begin
         if ptr_valid(inpixon_sizes) then $
            pixon_sizes= *(inpixon_sizes[0])
      endif else begin
         pixon_sizes=inpixon_sizes
      endelse
      top = max(pixon_sizes) <top
   endif

   if n_elements(pixon_sizes) LE 0 then begin
      ;;pixon_sizes = findgen(128)           ; Linear spaced pixons
      pixon_sizes = [sqrt(2.0)^findgen(25)]  ; logarithmic spaced pixons
   endif

   pixon_sizes = rotate(pixon_sizes(sort(pixon_sizes)),2)
   good = where(ceil(pixon_sizes) GE minres and pixon_sizes LE top)
   if good(0) LT 0 then message,'No valid pixon sizes.'
   pixon_sizes = pixon_sizes[good]
   if n_elements(inpixon_sizes) LE 0 then pixon_sizes = [bgpixon_size,pixon_sizes]
   max_pixon = max(pixon_sizes) ; The largest size in the iteration.
   ; A large background pixon should always be available, even if the user
   ; does not want to use it in the iteration
   if pixon_sizes[0] LT bgpixon_size then pixon_sizes = [bgpixon_size,pixon_sizes]
   pixon_sizes = squeeze(pixon_sizes)
   if n_elements(inpixon_sizes) LE 0 then inpixon_sizes = pixon_sizes ; output

   pixstring = ""
   for i=0,n_elements(pixon_sizes)-1 do pixstring = pixstring+" "+string(pixon_sizes(i),format="(f20.3)")
   message,/info,strcompress('Pixon Sizes: '+pixstring)

   good = where(data GT 0.,ngood)
   negative_data = where(data LT 0.,nnegdata)
   if nnegdata GT 0 then $
      message,/info,strcompress('Correcting '+string(nnegdata)+' negative data points')
   if ngood GT 0 then mindata = (min(data(good))/1000.)<(0.001) else begin
      message,'ERROR: all data is zero'
   endelse
   if (usedouble) then datac=double(data>mindata) else datac=data>mindata  ; protect the input data from change
   if n_elements(continued) ne 1 then continued = 0
   ;local = replicate(max_pixon,nx,ny)
   local = replicate(bgpixon_size,nx,ny)
   npixons = 2.72*float(nxy)/float(bgpixon_size^2)

   if n_elements(sigmai) EQ nd then sigma=sigmai
   snr = replicate(1.,nd)   ; No SNR weighting
   if use_snr then snr = sqrt(data)
   ;4-may-2010 ras, mod to support NORM_NOZERO
   nresid = float( keyword_set(NORM_NOZERO) ? nresid : nd)
   tsnr = total(snr) / nresid

   snr_sigma2 = snr/(sigma^2)
   if (usedouble) then pimage = dblarr(nx,ny) else pimage = fltarr(nx,ny)
   if (usedouble) then image  = dblarr(nx,ny) else image  = fltarr(nx,ny)

   ; Convert datac to data structure

   i1 = 0L
   ;sobj = ptrarr(27,5)
   sobj = ptrarr(szdobj[1],szdobj[2])
   for i=0,n_elements(a2d)-1 do begin
      nharmonics = n_elements(*harmonics[i])
      for j=0,nharmonics-1 do begin
         ndah = n_elements(*dobj[a2d[i],(*harmonics[i])[j]])
         i2 = i1 + ndah - 1L
         *dobj[a2d[i],(*harmonics[i])[j]] = datac[i1:i2]
         sobj[a2d[i],(*harmonics[i])[j]] = ptr_new(snr_sigma2[i1:i2])
         i1 = i1 + ndah
      endfor
   endfor

   ; Get a rough solution.

   if size(guessin,/tname) EQ 'POINTER' then begin
      if ptr_valid(guessin) then begin
         if max(*(guessin[0])) GT 0 then guess = *(guessin[0])
      endif
   endif else begin
      if n_elements(guessin) EQ nx*ny then begin
         if max(guessin) GT 0 then guess = guessin
     endif else if size(guessin,/n_dimensions) EQ 2 then $
     if n_elements(hsi_xy2annsec(guessin,iobj)) EQ nx*ny then begin
         if max(guessin) GT 0 then guess = hsi_xy2annsec(guessin,iobj)
     endif
   endelse

   if n_elements(trueimage) EQ nx*ny then begin
      if max(trueimage) GT 0 then guess = trueimage
   endif
   if n_elements(guess) NE nx*ny then begin
      if NOT keyword_set(quiet) then $
         message,/info,'Computing initial guess ...'
      guess = iobj->GetData( class_name= 'hsi_bproj' )
   endif

   ; If the total flux is not passed in, try to calculate it.
   if (keyword_set(btot_in)) then begin
      btot = btot_in
   ENDIF ELSE begin
      if NOT keyword_set(quiet) then $
         message,/info,'Estimating total flux '+!stime
      ;btot = hsi_btot(iobj,/fast,quiet=quiet)
      det_index_mask=iobj->get(/det_index_mask)
      junk = where( det_index_mask, ndet)
      btot = ndet*ndet*total(iobj->GetData( class_name= 'hsi_bproj' )) / $
             hsi_dirty_norm(iobj,/polar)
      if NOT finite(btot) then btot = 0.0
      if btot LE 0.0 then delvarx,btot
   endelse

   image = guess
   image = image > (max(image)/10000.)

   if n_elements(btot) GT 0 then image = image*btot(0)/total(image)

   Imax = max(image)*1.e9  ; Limit to image brightness

   ; Use log(image) so that even when the "image" is negative the
   ; actual image is always positive.

   if uselogarithm then begin
      image = uselogarithm*alog(double(image)>(max(image)/10000.))
      Imax = alog(Imax)
   endif

   if tty then begin
      erase
      xyouts,nx*expansion*0L+10,ny*expansion*1.5-5L,/device,align=0.5,orientation=90, $
         'Pseudo Image',chars=1.5
      xyouts,nx*expansion*2L+30,ny*expansion*1.5-5L,/device,align=0.5,orientation=90, $
         'Pixon Map',chars=1.5
      xyouts,nx*expansion*0L+10,ny*expansion*0.5-5L,/device,align=0.5,orientation=90, $
         'Image',chars=1.5
      xyouts,nx*expansion*2L+30,ny*expansion*0.5-5L,/device,align=0.5,orientation=90, $
         'Gradient',chars=1.5
   endif

   ; Initialize the iteration.

   if keyword_set(trueimage) then begin
      ; This is only used for testing
      truetemp = fltarr(nx,ny)
      truetemp[0,0] = trueimage
      trueimage = temporary(truetemp)
      tresiduals = hsi_pixon_residuals(reform(trueimage,nxy),dobj,iobj, $
                                       a2d,harmonics,modprofile=trecon_data, $
                                       /setunits,nounits=0,background=bobj)
      tgof = hsi_pixon_gof_calc(datac,trecon_data,tresiduals,snr_sigma2, $
                                poisson=usepoisson, nresid=nresid, norm_nozero=norm_nozero)

      ; If the next lines are used, then trueimage is used as the simulation
      ; ----------
      datac = randomp(seed,trecon_data)>.1

      dobj = hsi_pixon_fill_dobj(datac, iobj, a2d, harmonics)
      ;image = trueimage  ; make guess "perfect"
      image = fltarr(nx,ny)+total(image)/nx/ny  ; Make crappy guess
      btot = total(image)
      tresiduals = hsi_pixon_residuals(reform(trueimage,nxy),dobj,iobj, $
                                       a2d,harmonics,modprofile=trecon_data,nounits=0,background=bobj)
      tgof = hsi_pixon_gof_calc(datac,trecon_data,tresiduals,snr_sigma2, $
                                poisson=usepoisson, nresid=nresid, norm_nozero=norm_nozero)
      if uselogarithm then begin
         image = uselogarithm*alog(double(image)>(max(image)/10000.))
         Imax = alog(Imax)
      endif
      ; ----------

      if tty then begin
         wold = !d.window
         device,get_screen_size=scrsz
         window,/free,xsize=fix(640*(scrsz[1]/1200.)), $
                      ysize=fix(960*(scrsz[1]/1200.)), $
                      title='RHESSI Pixon Modulation Profiles'
         rwindow = !d.window
         wset,rwindow
         hsi_pixon_plot_residuals, trecon_data, dobj, tresiduals, $
                                   snr_sigma2, a2d, harmonics, $
                                   poisson=usepoisson, bobj=bobj,$
                                   NORM_NOZERO=NORM_NOZERO
         wset,wold
      endif
   endif

   junk = hsi_pixon_local_smooth(image,local,pixon_sizes,/recompute,fft=1) & junk = 0
   ;npixons = nd
   pfraction = double(local)   ; fraction of a pixon at each pixel
   pfraction(*) = double(npixons)/n_elements(local)

   smpattstart = systime(1)
   igof = hsi_pixon_gof_func(image,gradient,/reset)  ; sets initial pimage, etc.
   smpattstop = systime(1)
   smpatttime = (smpattstop-smpattstart)
   converge = 0.

   residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj, $
                                   a2d,harmonics,modprofile=recon_data, $
                                   /setunits,nounits=0,background=bobj)
   gof = hsi_pixon_gof_calc(datac,recon_data,residuals,snr_sigma2,r2s, $
                            poisson=usepoisson,/setunits,iobj=iobj,dobj=dobj, $
                            a2d=a2d,harmonics=harmonics, nresid=nresid, norm_nozero=norm_nozero)  ; also sets units
   rgof = gof/tsnr
   best = {image:pimage,gof:rgof,resolution:max_pixon,pixonmap:local}

   ; Set the DC component, if it is not already set, by unbiasing the
   ; residuals (approximate)

   if n_elements(btot) LE 0 then begin
      ; If we don't have a good DC value, try to adjust the intensites to
      ; fix this
      if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
      else eimage = (image>(max(image)/1.e32))<Imax
      eimage = eimage * total(datac) / total(recon_data)
      pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)
      btot = total(eimage)
      if uselogarithm then image = uselogarithm*alog(eimage) $
      else image = eimage
   endif

   if tty then begin
      if uselogarithm then $
         tvscl,rebin(exp(((uselogarithm*image)<Imax)>(-20.)),nx*expansion,ny*expansion,/sample),nx*0+20,ny*expansion $
      else $
         tvscl,rebin(image,nx*expansion,ny*expansion,/sample),nx*0+20,ny*expansion
   endif
   if not_quiet then begin
      print
      print,'   GOF:'+strcompress(' '+string(rgof))+'  Unmodulated Brightness: '+strcompress(string(total(pimage)))
   endif

   iterate_pixon_map = 0L   ; Iteration control parameters

   tolerance = 1.e-4 ;0.01

   ; Store master copies since only a subset will be used for each resolution
   a2d_master = a2d
   harmonics_master = harmonics
   delvarx,a2d,harmonics,/old  ; Remove a2d and harmonics, but do *not* free the pointer.  Hence /old.
   datac_master = datac
   snr_sigma2_master = snr_sigma2

   progbaropen = 0
   progstart = systime(1)

RESTART:

   pixon_map_no_change = 0L

   ; a2d resolution in arcseconds
   ;;separation = 1600.0 ; in mm
   ;;pitch = separation*(hsi_grid_parameters()).pitch/3600./!radeg
   ;;a2dr = 0.5*atan(pitch/separation)*!radeg*3600.  ; in arcsec
   a2dr = 0.5*((hsi_grid_parameters()).pitch)
   a2d_resolution = fltarr(szdobj[1],szdobj[2])
   for i=0,szdobj[1]-1 do begin  ; Account for higher harmonics too
      for j=0,szdobj[2]-1 do begin
         a2d_resolution[i,j] = a2dr[i MOD 9]/float(j+1)
      endfor
   endfor

   for iresolution=0L,n_elements(pixon_sizes)-1L do begin

      if pixon_sizes[iresolution] GT max_pixon+0.1 then continue

      ; Select the a2d values for this resolution

      pixel_size = min(iobj->Get(/pixel_size))  ; in arcseconds
      resarcsec = pixon_sizes[iresolution]*pixel_size  ; pixon size in arcsec
      ; The a2d's are not used until their resolution will contribute.
      ; res divisor adjusts resarcsec to give the resolution acheivable.
      ; For HXT resdivisor is about 3.  I.e. the 15" grids can reveal 5"
      ; structure.  I don't know about HESSI.  Here the divisor goes from
      ; 1 to 3 as the iteration progresses for increased speed.

      ;resdivisor = float(fix(3.*sqrt(float(iresolution)/float(n_elements(pixon_sizes)-1L))+0.5)>1.)
      resdivisor = 3.

      ; Get meaningful resolutions to speed up the reconstruction
      ; No point in wasting time with grids that have no information for
      ; the current resolution.

      if NOT keyword_set(quiet) then print
      if n_elements(harmonics) GT 0 then ptr_free,harmonics
      delvarx,a2d,harmonics,/old
      for i=0,n_elements(a2d_master)-1 do begin
         nharmonics = n_elements(*harmonics_master[i])
         delvarx,hindex
         for j=0,nharmonics-1 do begin
            ; sigmod is true if there is significant modulation
            ;smfact = 1.02
            smfact = 1.03  ; TRM 2002-Jan-11
            sigmod = stddev(*dobj[a2d_master[i],(*harmonics_master[i])[j]]) GE $
                     smfact*sqrt(mean((*dobj[a2d_master[i],(*harmonics_master[i])[j]])))
            ;if NOT sigmod AND iresolution EQ 0 then begin
            ;   message,/info, $
            ;   strcompress('No significant modulation in ' + $
            ;   string(a2d_master[i]+1) + ' ' + $
            ;   string((*harmonics_master[i])[j]) + ': ' + $
            ;   string(stddev(*dobj[a2d_master[i],(*harmonics_master[i])[j]])) + $
            ;   ' < ' + $
            ;   string(smfact*sqrt(mean((*dobj[a2d_master[i],(*harmonics_master[i])[j]])))))
            ;endif else begin
            ;   if NOT keyword_set(quiet) AND iresolution EQ 0 then begin
            ;      message,/info, $
            ;      '   '+strcompress('Significant modulation in ' + $
            ;      string(a2d_master[i]+1) + ' ' + $
            ;      string((*harmonics_master[i])[j]) + ': ' + $
            ;      string(stddev(*dobj[a2d_master[i],(*harmonics_master[i])[j]])) + $
            ;      ' > ' + $
            ;      string(smfact*sqrt(mean((*dobj[a2d_master[i],(*harmonics_master[i])[j]])))))
            ;   endif
            ;endelse
            ; Here the a2d-harmonic is included if the current
            ; pixon size (resarcsec), adjusted by resdivisor,
            ; is smaller than the
            ; grid resolution.  All are included for the last 2 iterations.
            ; a2d-harmonics are never used if they have no significant
            ; modulation.
            ; 2002-02-12 TRM Removed the sigmod test.  Just use whatever
            ; the user passed in.
            ;;if (a2d_resolution[a2d_master[i],(*harmonics_master[i])[j]] GE resarcsec/resdivisor OR iresolution GE n_elements(pixon_sizes)-2L) AND sigmod then begin
            if (a2d_resolution[a2d_master[i],(*harmonics_master[i])[j]] GE resarcsec/resdivisor OR iresolution GE n_elements(pixon_sizes)-2L) then begin
               if n_elements(hindex) LE 0 then hindex=(*harmonics_master[i])[j] $
               else hindex = [hindex,(*harmonics_master[i])[j]]
            endif
         endfor
         if n_elements(hindex) GT 0 then begin
            if n_elements(a2d) LE 0 then a2d=a2d_master[i] $
            else a2d = [a2d,a2d_master[i]]
            if n_elements(harmonics) LE 0 then harmonics = ptr_new(hindex) $
            else harmonics = [harmonics,ptr_new(hindex)]
         endif
      endfor

      ; If none, at least use the largest resolution.
      ; This is just slow, not wrong.
      if n_elements(a2d) LE 0 then begin
         ;if ngood LE 0 then junk = max(a2d_resolution[a2d_master,0],good)
         junk = max(a2d_resolution[a2d_master,0],good)
         a2d = a2d_master[good]
         harmonics = [ptr_new((*harmonics_master[good])[0])]
         message,/info,'All available resolutions are too small.  Using largest.  Reconstruction may take longer than expected.'
      endif

      ; Now we have to set the flat arrays, data and snr_sigma2, to the
      ; right subset of the a2d's and harmonics
      ;for i=0,n_elements(a2d)-1 do begin
      ;   nharmonics = n_elements(*harmonics[i])
      ;   for j=0,nharmonics-1 do begin
      ;      ndah = n_elements(*dobj[a2d[i],(*harmonics[i])[j]])
      ;      dtemp = *dobj[a2d[i],(*harmonics[i])[j]]
      ;      stemp = *sobj[a2d[i],(*harmonics[i])[j]]
      ;      if i EQ 0 and j EQ 0 then begin
      ;         datac=dtemp
      ;         snr_sigma2=stemp
      ;      endif else begin
      ;         datac=[datac,dtemp]
      ;         snr_sigma2 = [snr_sigma2,stemp]
      ;      endelse
      ;   endfor
      ;endfor
      datac = hsi_pixon_reduced_data(dobj,a2d,harmonics)
      snr_sigma2 = hsi_pixon_reduced_data(sobj,a2d,harmonics)
      if NOT keyword_set(quiet) then $
         message,/info,"Currently using detectors's: " + $
         strjoin(strcompress(string(a2d+1),/remove_all)," ")
      nd = n_elements(datac)

      if iresolution LT 0L then begin  ; Start with a GOF/MEM fit when iresolution starts at -1
         local(*) = 1.
         resolution = 1.
         npixons = n_elements(image)
      endif else if iresolution EQ 0L then begin
         resolution = pixon_sizes[iresolution]
         ;local(*) = resolution
         ;npixons = 2.72*float(nxy)/float(resolution^2)
         ;npixons = 2.72*float(nxy)/float(bgpixon_size^2)
      endif else begin
         resolution = pixon_sizes[iresolution]
      endelse

      ; Correct the DC component

      ;if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
      ;else eimage = (image>(max(image)/1.e32))<Imax
      ;pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)
      ;residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj,a2d,harmonics,modprofile=recon_data,nounits=0,background=bobj)
      ;;eimage = eimage * (total(data)/total(recon_data))
      ;if uselogarithm then image = uselogarithm*alog(eimage) else image = eimage

      ; Use the logarithmic fit to start, then switch to no log for the
      ; final solution

   ;   if uselogarithm then begin
   ;      ;message,/info,strcompress('Switching from logarithmic to linear solution '+string(top)+' '+string(resolution))
   ;      save_uselogarithm = uselogarithm
   ;      uselogarithm = 0
   ;      image = exp(((uselogarithm*image)<Imax)>(-20.0))
   ;      Imax = exp(Imax)
   ;   endif else begin
   ;      ;message,/info,strcompress('Switching from linear to logarithmic solution '+string(top)+' '+string(resolution))
   ;      uselogarithm = save_uselogarithm  ; either +1 or -1
   ;      if max(image) LE 0.0 then image(*) = Imax/1.e32
   ;      image = uselogarithm*alog(image>(max(image)/1.e32))
   ;      Imax = alog(Imax)
   ;   endelse

      if not_quiet then begin
         print
         print,'RESOLUTION = ',strcompress(string(resolution),/remove_all)
         print
      endif

      if 0 then begin
         ;if keyword_set(dosnr AND 2) then begin ; full pixon map calculation
         ; Reset the pseudo-image to the image at the start of
         ; each resolution.  This should not really be necessary.
         if uselogarithm then eimage=exp(((uselogarithm*image)<Imax)>(-20.0)) $
         else eimage = (image>(max(image)/1.e32))<Imax
         pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)
         image = pimage
      endif

      iteration = 0L

      best_last_res = best

      REPEAT begin   ; Iterate to the solution for this resolution

         old_rgof     = rgof
         old_converge = converge
         old_pimage   = pimage
         old_image    = image

         if (iterate_pixon_map OR (iteration EQ 0L)) AND $
            ;(iresolution GE 0) AND $
            (abs(resolution-bgpixon_size) GT 0.1) then begin
            ;(resolution NE max_pixon) then begin

            ; Get new pixon map

            last_local = local
            pmapstart = systime(1)
            if keyword_set(dosnr AND 2) then begin  ; Do a full pixon map calculation?
               local[*] = pixon_sizes[0]  ; the biggest pixon
               for ilocal = 1L,n_elements(pixon_sizes)-1 do begin
                  if pixon_sizes[ilocal] GE resolution-0.01 then begin
                     local = hsi_pixon_map(image,iobj,local, $
                                           datac,dobj,sobj,snr_sigma2, $
                                           pixon_sizes[ilocal], $
                                           nx,ny,nxy,nd,pixon_sizes,a2d,harmonics, $
                                           uselogarithm=uselogarithm,Imax=Imax, $
                                           npixons=npixons,pfraction=pfraction, $
                                           usepoisson=usepoisson, $
                                           sensitivity=sensitivity, $
                                           a2d_resolution=a2d_resolution, $
                                           not_quiet=not_quiet,tty=tty, $
                                           expansion=expansion,fast=1, $
                                           smpattwritedir=smpattwritedir, $
                                           /nolastprint,bobj=bobj,bflat=bflat)
                   endif
               endfor
               if NOT keyword_set(quiet) then print
            endif else begin
               local = hsi_pixon_map(image,iobj,local, $
                                     datac,dobj,sobj,snr_sigma2,resolution, $
                                     nx,ny,nxy,nd,pixon_sizes,a2d,harmonics, $
                                     uselogarithm=uselogarithm,Imax=Imax, $
                                     npixons=npixons,pfraction=pfraction, $
                                     usepoisson=usepoisson, $
                                     sensitivity=sensitivity, $
                                     a2d_resolution=a2d_resolution, $
                                     not_quiet=not_quiet,tty=tty, $
                                     expansion=expansion,fast=1, $
                                     smpattwritedir=smpattwritedir,bobj=bobj,bflat=bflat)
            endelse
            pmapstop = systime(1)
            if n_elements(pmaptime) LE 0 then pmaptime = pmapstop-pmapstart $
            else pmaptime = pmaptime + pmapstop-pmapstart
            ; Calculate when the pixon map last changed
            if min(local EQ last_local) then $
               pixon_map_no_change = pixon_map_no_change + 1L $
            else pixon_map_no_change = 0L

            if tty then begin
               ; Plot pixon map and color bar
               tv,rebin(bytscl(alog(local),min=alog(resolution), $
                                     max=alog(max(pixon_sizes)), $
                                     top=!d.table_size-2), $
                                     nx*expansion,ny*expansion,/sample), $
                  nx*expansion+20,ny*expansion
               mx = 10
               my = ny*expansion
               tickv = pixon_sizes[where(pixon_sizes-resolution GE -0.01,nticks)]
               color_bar,mx,my,(nx*expansion)*2+20-mx,ny*expansion,color=0, $
                         min=resolution,max=max(pixon_sizes),top=!d.table_size-2, $
                         tickv=tickv,ticks=nticks-1,type=1
            endif

            ; Figure out what fraction of the intensity is in the smallest
            ; spatial scale.
            if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
            else eimage = (image>(max(image)/1.e32))<Imax
            pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)

            ss_sm_scale = where(abs(local-min(local)) LE 0.1,nssss)
            if nssss GT 0 then begin
               sfraction = total(eimage[ss_sm_scale])/total(eimage)
            endif else begin
               sfraction = 0.0
            endelse
            ;message,/info,'Sfraction is '+ $
            ;        string(sfraction)+' '+ $
            ;        string(nssss)+' '+ $
            ;        string(resolution)

            ; Reset the pseudo image to the image at the start of each
            ; iteration.  This prevents the model from ending up providing
            ; an intensity determination rather than a scale determination.
            ; This is equivalent to smoothing the psuedo image at the start
            ; of each iteration.  This should probably be done at every
            ; step, but it is much faster not to do this once the model
            ; has converged.  LE 2 is the bare minimum.  LE 3 or LE 4
            ; might be more robust, but slower.  This is not done after
            ; the bg_pixon size since that just wipes out sidelobes. It
            ; is also not done if the model does not appear to be setting
            ; intensities (sfraction test).
            if pixon_map_no_change LE 2L AND $
               sfraction LT 0.5 AND $
               abs(resolution-pixon_sizes[1]) GT 0.1 then begin
               if uselogarithm then $
                  image = uselogarithm*alog(double(pimage)>(max(pimage)/10000.)) $
               else $
                  image = pimage
               ;message,/info,'Copying image to pseudo image.'
            endif ;else message,/info,'NOT Copying image to pseudo image.'

         endif

         ; Get a background estimate from the residuals by fitting
         ; sine waves with the spin period and twice the spin period.
         ; The residuals are phase binned before doing the fits.
         if (iteration GE 1L or (use_dfpmin and iresolution GT 0)) AND $
           ((iteration LE 2L-use_dfpmin) OR (iresolution EQ 0))then begin
            ; The first iteration generally gets the total flux right so we
            ; should not do the background iteration on the first pass.
            if keyword_set(dosnr AND 4) then begin
               if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
               else eimage = (image>(max(image)/1.e32))<Imax
               pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)
               ; if background=bobj is passed to hsi_pixon_residuals, then
               ; /noadd should NOT be set in hsi_pixon_fit_bgrnd, otherwise
               ; /noadd should be set.
               residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj, $
                                               a2d,harmonics,modprofile=recon_data, $
                                               nounits=0);,background=bobj)
               ; There is a minus sign due to the definition of
               ; residuals in hsi_pixon_residuals.pro where the
               ; residual is mod_profile-fobs but the background
               ; is fobs-mod_profile.
               bobj = hsi_pixon_fit_bgrnd(-residuals,iobj,bobj,dobj,celist, $
                                          a2d,harmonics,bflat=bflat, $
                                          tty=tty,bwindow=bwindow,/noadd)
              ;; Test code.  Check that hsi_pixon_fit_bgrnd works right
              ;stop
              ;this_a2d = a2d[0]
              ;this_harmonic = (*harmonics[a2d[0]])[0]
              ;phase = ((*celist[a2d[0],0]).roll_angle); MOD (2.0*!pi)
              ;testr = 10.0+6.*sin(phase+1.3)+3.*sin(2.*phase+2.6)
              ;test = hsi_pixon_fit_bgrnd(testr,iobj,bobj,dobj,celist,/noadd, $
              ;                           [a2d[0]],harmonics[a2d[0]],tty=1)
              ;good = where(*test[this_a2d,this_harmonic] NE 0.0)
              ;plot,phase[good],testr[good]
              ;oplot,phase[good],-(*test[this_a2d,this_harmonic])[good],color=128

            endif
         endif

         ; Now that we have the new pixon map, go back to the full
         ; set of data for the image iteration.

         ;if n_elements(a2d) LT n_elements(a2d_master) then begin
         ;   a2d = a2d_master
         ;   if n_elements(harmonics) GT 0 then ptr_free,harmonics
         ;   harmonics = ptrarr(n_elements(harmonics_master))
         ;   for iharm=0,n_elements(harmonics_master)-1 do $
         ;      harmonics[iharm] = ptr_new([*harmonics_master[iharm]])
         ;   datac = datac_master
         ;   snr_sigma2 = snr_sigma2_master
         ;   nd = n_elements(datac)
         ;   message,/info,"Reverting to A2D's: " + $
         ;                 strjoin(strcompress(string(a2d+1),/remove_all)," ") + $
         ;                 " for image iteration"
         ;endif


         ; Iterate to the best image given the current pixon map.
         ; Choose conjugate gradient or simplex minimization

         imgstart = systime(1)

         if keyword_set(use_powell) then begin
            ; ***********************
            ; * Powell Minimization *
            ; ***********************
            xi = fltarr(nxy,nxy)
            for ixi=0L,nxy-1L do xi[ixi,ixi] = image[ixi]
            tempimage = reform(image,nxy)
            powell,tempimage,xi,tolerance,mgof,'hsi_pixon_gof_func'
            image = reform(tempimage,nx,ny)
            converge = 0.0
         endif else if keyword_set(use_dfpmin) then begin
            ; **********************************
            ; * DFP Minimization               *
            ; **********************************
            ; dfpmin requries things to be of order 1.
            ;dfpscale = [tsnr*nd,max(image)]
            dfpscale = [tsnr*nresid,max(image)] ;4-may-2010, ras
            dfpimage = image/dfpscale[1]
            if iteration EQ 0 then $
               last_mgof = hsi_pixon_dfp_gof_func(dfpimage)*dfpscale[0]
            catch,error_status
            if error_status NE 0 then begin
               message,/info,'WARNING: Error in DFPMIN, skipping this resolution: '+ $
                             !error_state.msg
               converge = 0.0
            endif else begin
               except = !except
               !except=0
               dfpmin,dfpimage,1.e-7,mgof, $
                      'hsi_pixon_dfp_gof_func','hsi_pixon_dfp_grad_func', $
                      tolx=1.e-7,iter=iter,eps=(machar()).eps
               check = check_math(mask=32) ; Ignore underflows
               !except = except
               image = dfpimage*dfpscale[1]
               mgof = mgof * dfpscale[0]
               if mgof EQ 0.0 then converge = 0.0 $
               else converge = abs(mgof-last_mgof)/mgof
               last_mgof = mgof
            endelse
            catch,/cancel
         endif else begin
            ; **********************************
            ; * Conugate Gradient Minimization *
            ; **********************************

            minf_conj_grad,image,mgof,converge,func_name='hsi_pixon_gof_func', $
               use_deriv=usederivative,tolerance=tolerance,init=(iteration EQ 0)

         endelse

         ; **********************************
         ; * Downhill Gradient Minimization *
         ; **********************************

         ;minf_downhill,image,mgof,converge,func_name='hsi_pixon_gof_func', $
         ;   use_deriv=usederivative,tolerance=tolerance

         ; ************************
         ; * Simplex Minimization *
         ; ************************

         ;delvarx,simplex   ; don't want to define this at startup
         ;agof = hsi_pixon_gof_func(image,agradient)
         ;scale = agof/sqrt((agradient*agradient)>1.e-9)
         ;amimage = amoeba(tolerance,function_name='hsi_pixon_gof_func', $
         ;               function_value=mgof,ncalls=ncalls,simplex=simplex, $
         ;               nmax=200,p0=reform(image,nxy),scale=scale)
         ;converge = 0.
         ;message,/info,strcompress('Amoeba used ' + string(ncalls) + $
         ;              ' function calls')
         ;image = reform(simplex[*,0],nx,ny)   ; best guess
         ;if n_elements(amimage) LE 1 then $
         ;   message,/info,'Amoeba failed to converge' $
         ;else begin
         ;   message,/info,strcompress('Check simplex: '+ $
         ;      string(min(aimage))+' '+ $
         ;      string(max(aimage))+' '+ $
         ;      string(min(image))+' '+ $
         ;      string(max(image)))
         ;endelse

         imgstop = systime(1)
         lastimgtime = imgstop - imgstart
         if n_elements(totalimgtime) LE 0 then totalimgtime = 0.0d0
         totalimgtime = totalimgtime + lastimgtime

         if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
         else eimage = (image>(max(image)/1.e32))<Imax
         pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)

         residuals = hsi_pixon_residuals(reform(pimage,nxy),dobj,iobj,a2d,harmonics,modprofile=recon_data,nounits=0,background=bobj)
         gof = hsi_pixon_gof_calc(datac,recon_data,residuals,snr_sigma2, $
                                  poisson=usepoisson, nresid=nresid, norm_nozero=norm_nozero)
         ; Residuals for the pseudo-image
         presiduals = hsi_pixon_residuals(reform(eimage,nxy),dobj,iobj,a2d,harmonics,modprofile=precon_data,nounits=0,background=bobj)
         pgof = hsi_pixon_gof_calc(datac,precon_data,presiduals,snr_sigma2, $
                                   poisson=usepoisson, nresid=npresid)

         rgof = gof/tsnr
         prgof = pgof/tsnr

         cancelled = 0
         if tty then begin   ; Plot results
            tvscl,rebin(eimage,nx*expansion,ny*expansion,/sample),nx*0+20,ny*expansion
            xyouts,nx*expansion*0.5+20,ny*expansion,/device,align=0.5,strcompress(string(max(eimage)))
            wold = !d.window   ; Plot residuals
            need_new_window = 0
            if n_elements(rwindow) LE 0 then need_new_window = 1 $
            else begin
               device, WINDOW = w
               if rwindow GT n_elements(w)-1 then need_new_window = 1 $
               else if NOT w[rwindow] then need_new_window = 1
            endelse
            if need_new_window then begin
               device,get_screen_size=scrsz
               window,/free,xsize=fix(640*(scrsz[1]/1200.)), $
                      ysize=fix(960*(scrsz[1]/1200.)), $
                      title='RHESSI Pixon Modulation Profiles'
               rwindow = !d.window
            endif
            wset,rwindow
            hsi_pixon_plot_residuals, recon_data, dobj, residuals, $
                                      snr_sigma2,a2d, harmonics, $
                                      poisson=usepoisson, bobj=bobj,$
                                      NORM_NOZERO=NORM_NOZERO
            wset,wold

            if progress_bar then begin	;kim
               ; Update the progress widget
               if progbaropen EQ 0 then begin
                  progbar, progobj, /init
                  progbaropen = 1
                  time_bin_def = iobj->get(/time_bin_def)
                  rtime_bin_def = rotate(time_bin_def,2)
                  timebintrack = [float(total(rtime_bin_def[a2d]))]
               endif
               if (iteration EQ 0) AND (iresolution GT 0) then begin
                  timebintrack = [timebintrack,float(total(rtime_bin_def[a2d]))]
               endif
               if resolution GT minres then iterstop = 15 else iterstop = 40

               ;fudgepower = float(n_elements(pixon_sizes)-iresolution-1)
               ;timebinfudge = float(total(rtime_bin_def[a2d])) / $
               ;               float(total(rtime_bin_def[a2d_master]))
               timebinavg = total(timebintrack)/float(iresolution+1.)
               timebintavgHI = float(total(timebintrack) + total(rtime_bin_def[a2d_master])*(n_elements(pixon_sizes)-iresolution-1.0))/float(n_elements(pixon_sizes))
               timebintavgLO = float(total(timebintrack) + total(rtime_bin_def[a2d])*(n_elements(pixon_sizes)-iresolution-1.0))/float(n_elements(pixon_sizes))
               timebintavg = (timebintavgHI + timebintavgLO)/2.0
               timebinfudge = timebinavg / timebintavg
               ;timebinfudge = total(timebintrack)/(total(timebintrack)+n_elements(pixon_sizes)-1-iresolution)
               totaltime = (systime(1)-progstart)>1.0d0
               if iteration EQ 0 then begin
                  if n_elements(pmaptime) LE 0 then begin
                     estpmaptime = n_elements(a2d)*smpatttime/float(n_elements(a2d_master)*n_elements(pixon_sizes))
                     pmapfraction = estpmaptime/(lastimgtime*iterstop+estpmaptime)
                  endif else begin
                     pmapfraction = pmaptime/((totaltime-lastimgtime))
                  endelse
               endif
               if iresolution EQ 0 then $
                  pmappercent = 0.0 $
               else $
                  pmappercent = total(n_elements(a2d)*(lindgen(iresolution)+1)) / $
                                total(n_elements(a2d_master)*(lindgen(n_elements(pixon_sizes)-1)+1))
               imgpercent = timebinfudge*float(iresolution+(iteration+1.0)/float(iterstop))/float(n_elements(pixon_sizes))
               pmapfraction = (pmapfraction<0.999)>0.001
               pmappercent = (pmappercent<1.0)>0.0
               imgpercent = (imgpercent<1.0)>0.0

               if iresolution EQ 0 then begin
                  time_per_dot = smpatttime/float(n_elements(a2d_master)*n_elements(pixon_sizes))
               endif else begin
                  if n_elements(pmaptime) LE 0 then $
                     time_per_dot = 0.0 $
                  else $
                     time_per_dot = pmaptime/total(n_elements(a2d)*(lindgen(iresolution)+1))
               endelse
               totalnumdots = total(n_elements(a2d_master)*(lindgen(n_elements(pixon_sizes)-1)+1))
               percent = pmapfraction*pmappercent + (1.0-pmapfraction)*imgpercent
               ;print,'Before iteration: ',percent,pmapfraction
               mollifier = 0.1
               for pmapiter=0L,10L do begin  ; iterate a few times
                  esttotaltime = totaltime/(percent>0.0001)
                  pmapfraction = mollifier*totalnumdots*time_per_dot/esttotaltime+(1.0-mollifier)*pmapfraction
                  percent = pmapfraction*pmappercent + (1.0-pmapfraction)*imgpercent
               endfor
               ;print,'After iteration: ',percent,pmapfraction

               eta = (1.0-percent)*(totaltime)/(percent>0.0001)
               eta_hour = strtrim(string(long(eta / 3600.0)),2)
               eta_min  = strtrim(string(long((eta - eta_hour*3600.)/60.0)),2)
               eta_sec  = strtrim(string(long(eta - eta_hour*3600. - eta_min*60.)),2)
               if strlen(eta_hour) LE 1 then eta_hour = '0'+eta_hour
               if strlen(eta_min) LE 1 then eta_min = '0'+eta_min
               if strlen(eta_sec) LE 1 then eta_sec = '0'+eta_sec
               eta_str = strcompress(string(eta_hour)+':'+string(eta_min)+':'+string(eta_sec),/remove_all)
               ;if (iresolution EQ 0) then eta_str='<calibrating>'

               elapsed = totaltime
               elapsed_hour = strtrim(string(long(elapsed / 3600.0)),2)
               elapsed_min  = strtrim(string(long((elapsed - elapsed_hour*3600.)/60.0)),2)
               elapsed_sec  = strtrim(string(long(elapsed - elapsed_hour*3600. - elapsed_min*60.)),2)
               if strlen(elapsed_hour) LE 1 then elapsed_hour = '0'+elapsed_hour
               if strlen(elapsed_min) LE 1 then elapsed_min = '0'+elapsed_min
               if strlen(elapsed_sec) LE 1 then elapsed_sec = '0'+elapsed_sec
               elapsed_str = strcompress(string(elapsed_hour)+':'+string(elapsed_min)+':'+string(elapsed_sec),/remove_all)

              ; print,'timebintrack: ',timebintrack,timebinfudge
              ; message,/info,'Pmapfraction: '+string(pmapfraction) + ' ' + $
              ;               'Pmappercent: '+string(pmappercent)  + ' ' + $
              ;               'Imgpercent: '+string(imgpercent)  + ' ' + $
              ;               'TimeFudge: '+string(timebinfudge) + ' ' + $
              ;               'Percent: '+string(percent) + ' ' + $
              ;               eta_str + ' ' + elapsed_str
               progbar,progobj,/update,percent=(percent*100.)<100., $
                  message_text= $
                     ['Pixon Image Reconstruction.','Resolution: ' + $
                     strcompress(string(resolution),/remove_all) + $
                     ', C: '+strcompress(string(rgof/n_elements(residuals)),/remove_all), $
                     ', C: '+strcompress(string(rgof/nresid),/remove_all), $
                     'Est. Time to Completion: '+eta_str]
               progbar,  progobj, cancel=cancelled
               if keyword_set(cancelled) then begin
                  message,/info,'Pixon image reconstruction cancelled by user'
                  progbar,progobj,/destroy
                  progbaropen = 0
               endif
            endif
         endif

         if not_quiet then begin ; Print status
            ;print,strcompress(string(iteration)+' GOF: '+string(rgof/n_elements(residuals))+ $
            print,strcompress(string(iteration)+' GOF: '+string(rgof/nresid)+ $
                 ', convergence='+string(converge)+ $
                 ', npixons='+string(npixons)+ $
                 ;', Pseudo GOF='+string(pgof/tsnr/n_elements(presiduals))+ $
                 ', Pseudo GOF='+string(pgof/tsnr/npresid)+ $
                 ', Btot='+string(total(pimage))+$
                 ', Bias='+string(total(residuals)))
         endif

         ;;if rgof LE best.gof then begin
            best.gof = rgof
            best.image = pimage
            best.resolution = resolution
            best.pixonmap = local
         ;;endif

         iteration = iteration + 1L

         ; A 'Q' at the keyboard exits the program gracefully.

         kbrd_quit=0
         kbrd_character=''
         ;if tty then begin
         ;   kbrd_character = get_kbrd(0)
         ;   kbrd_quit = (kbrd_character EQ 'Q') $
         ;endif else begin
         ;   kbrd_character = ''
         ;   kbrd_quit=0
         ;endelse

         if ((iteration GT 40) OR kbrd_quit OR cancelled) then begin
            print
            print,'CAUTION: Not fully converged'
            print
            goto,FINISH
         endif

      ;endrep UNTIL ((((converge LT tolerance) and (old_converge LT tolerance))) and $
      ;              (iteration GT 1)) OR (converge LE 0.0) OR $
      ;             ((iteration GT 30) AND (resolution GT minres))

      endrep UNTIL (converge LE 0.0 or (use_dfpmin AND converge LE 1.e-3 AND resolution GT minres)) OR $
                   ((iteration GT 15) AND (resolution GT minres)) OR $
                   ;(iteration GT 1) OR $
                   (kbrd_character EQ 'S')
      ;message,/info,'WARNING: Iterations stopped after 1 for testing'
      if not_quiet then begin
         print
         print,strtrim(strcompress('End of Resolution ' + $
                 strtrim(strcompress(string(resolution),/remove_all),2) + $
                 ;'.   C = '+string(rgof/n_elements(residuals))),2)
                 '.   C = '+string(rgof/nresid)),2)
         print
      endif

   endfor


FINISH:
   bestimage = best.image
   if keyword_set(xycoordinate) then $
      bestimage = hsi_annsec2xy(bestimage,iobj)
   ;; Error code is now obsolete.  Get the error from the image object using
   ;; hsi_calc_image_error.pro
   ;if n_params() GE 5 then begin
   ;   if NOT keyword_set(quiet) then begin
   ;      print
   ;      print,'ERROR ESTIMATE'
   ;      print
   ;   endif
   ;
   ;   if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
   ;   else eimage = (image>(max(image)/1.e32))<Imax
   ;   pimage = hsi_pixon_local_smooth(eimage,local,pixon_sizes,fft=1)
   ;   gof0 = hsi_pixon_gof_func(image,gradient,/strictgradient)
   ;   error = hsi_image_error(iobj,pimage,gradient=gradient, $
   ;                           pixonmap=best.pixonmap, $
   ;                           poisson=usepoisson,bobj=bobj, $
   ;                           pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
   ;                           alg_used = 'HSI_PIXON', $
   ;                           algunit=hessi_constant(/detector_area)* $
   ;                                   (iobj->get(/info, /cbe_det_eff)).avg)
   ;   ;if keyword_set(xycoordinate) then error = hsi_annsec2xy(error,iobj)
   ;endif

   if NOT keyword_set(quiet) then print

   outresolution = min(best.pixonmap)
   pixonmap = best.pixonmap
   ;rgof = best.gof/n_elements(residuals)
   rgof = best.gof/nresid
   residual = residuals

   if tty then begin
      ;restore the saved color table
      tvlct,rsave,gsave,bsave
   endif

   if progbaropen then progbar,progobj,/destroy

   return,bestimage

end
