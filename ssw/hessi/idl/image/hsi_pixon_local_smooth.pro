
; Copyright (c) 1999 Thomas R. Metcalf , Lockheed Martin Advanced Technology
; Center, Dept. L9-41, Bldg. 252, 3251 Hanover St., Palo Alto, CA  94304


function fftconvol,fft1,fft2

   ; FFT convolution.  The inputs are assumed already fourier transformed.

   return,float(fft(fft1*fft2,-1))

end

;+

function hsi_pixon_local_smooth,image,lcs,inpixon_sizes,sfunctions,sdels,xcen,ycen, $
                      noconserve=noconserve,recompute=recompute,binup=binup, $
                      fftconvolution=fftconvolution,quiet=quiet,diffres=diffres, $
                      nodiff=nodiff

;NAME:
;     HSI_PIXON_LOCAL_SMOOTH
;PURPOSE:
;     Smooth an image locally using a local correlation scale
;CATEGORY:
;CALLING SEQUENCE:
;     smoothed = hsi_pixon_local_smooth(image,lcs,pixon_sizes,nx=nx,ny=ny)
;INPUTS:
;     image = image to be smoothed
;     lcs = array specifying the local correlation scale (in pixels)
;     pixon_sizes = List of valid pixon sizes.  All elements of lcs
;                   should have a value from this list.
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS
;     /fftconvolution = Use FFT for the convolutions rather than CONVOL.
;                       This is considerably faster when the size of
;                       the image is a power of 2.
;     /recompute = Force the smoothing functions to be recomputed.  This
;                  should be used on the first call to
;                  hsi_pixon_local_smooth since the smoothing
;                  functions are stored in a common block 
;                  and you may be using old smoothing functions unless
;                  you recompute on the first call.
;     /noconserve = Alternate smoothing algortihm: does not conserve
;                   counts.
;     diffres = If set, then the smoothing will be
;               done with the difference between the smoothing funciton for
;               the resolution given by lcs and the resolution given by
;               diffres.  Diffres should be a scalar index into the pixon
;               sizes.  This is used for the calculation of a pixon map.
;               This only works if /fftconvolution is set!!!
;     /nodiff = do not calculate the differences in the PSF's.  This is only
;               valid on the first call or when /recompute is set.  If
;               /nodiff is set, the diffres keyword will not work.  The setup
;               for the differences can be slow so set this keyword if you
;               will not need them.
;OUTPUTS:
;     sfunctions = the smoothing functions
;     sdels = the size of the smoothing functions
;     smoothed = locally smoothed image
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;     Be sure to use /recompute on the first call!
;EXAMPLE:
;     simage = hsi_pixon_local_smooth(image,local_scale(image), $
;                                     long(alog10(sqrt(nx*ny))/alog10(2.0d0)+0.5))
;PROCEDURE:
; Apply the pixon map (local correlation scales) to image to obtain
; the smoothed image, pimage. 
;MODIFICATION HISTORY:
;      T. Metcalf June 11, 1994
;      1994-09-23 TRM Fixed bug which did not conserve counts in the image.
;                     Added logarithmic spacing of scales.
;      1994-10-26 TRM Use logarithmic spacing of pixon sizes and use
;                     parabolic pixons.
;      1995-08-22 TRM No more logarithmic pixon spacing.  Use exactly the
;                     pixons which are passed to the routine.  Things
;                     like logarithmic spacing should be dealt with at a
;                     higher level.
;
;                     Add the recompute keyword so the routine does not have
;                     to waste time checking if the smoothing functions need
;                     to be recomputed.  The user must know when the functions
;                     should be recomputed!
;      1995-10-12 TRM Added FFT convolution option.  But it turned out to 
;                     be slower, so it's not recommended.
;      1996-06-17 TRM Added /edge_truncate keyword to CONVOL calls.  This
;                     handles the edges much more efficiently than a guard
;                     ring.
;      1996-06-18 TRM Added binup factor to increase the accuracy of the 
;                     pixon shape functions.  This allows a finer resolution in
;                     pixon sizes.
;                     FFT convolution is faster now.
;      1996-09-14 TRM -Change to compute psf and fftpsf each time the psf's
;                      are recomputed.  Thus, without recomputing, the routing
;                      can be called with or without the /fft keyword.
;                     -Changed from double to float.
;      2000-02-18 TRM Added diffres keyword.
;      2001-02-21 TRM Fixed a bug that added a 1 pixel shift
;-

   common hsi_loc_smth_private,psf,fftpsf,nx,ny,xcenter,ycenter,dels,max_del, $
                           min_del,nlev,diffpsffft,diffpsf

   ; Compute the smoothing functions if they don't already exist

   spsf = size(psf)
   if n_elements(diffres) GT 0 then usediffres = 1 else usediffres = 0

   if spsf(0) NE 3 OR keyword_set(recompute) then begin

      if n_elements(inpixon_sizes) LE 0 then pixon_sizes=squeeze(lcs) $
      else pixon_sizes = inpixon_sizes
      pixon_sizes = pixon_sizes(sort(pixon_sizes))

      max_del = MAX(pixon_sizes)/2.0
      min_del = MIN(pixon_sizes)/2.0

      if n_elements(binup) LE 0 then begin
         binup = 11L   ; Can be changed, but larger binup gives more accuracy
                       ; e.g. a value of 11 gives pixon shape function resolution
                       ; down to 1/11 of a pixel (the smallest change in pixon
                       ; size that means anything).  MUST BE ODD.
      endif

      dels = pixon_sizes/2.0
      nlev = n_elements(dels)

      if n_elements(dels) GT 0 AND NOT keyword_set(quiet) then $
         message,/info,strcompress('Recomputing smoothing functions '+ $
                                    string(nlev)+' '+string(n_elements(dels)) +$
                                    string(2.*max(dels)+0.5)+' '+ $
                                    string(2.*max_del+0.5)+' '+ $
                                    string(binup))

      nxy = long(max(dels)) + 1   ; TRM added +1 1996-06-18
      xcenter = nxy & ycenter = nxy
      nx = nxy*2L+1L  ; always odd
      ny = nx
      nxfft = n_elements(lcs(*,0))
      nyfft = n_elements(lcs(0,*))
      xcenterfft = nx/2L & ycenterfft = ny/2L
      ;psf = fltarr(nxfft,nyfft,nlev)
      psffft = fltarr(nxfft,nyfft)
      fftpsf = complexarr(nxfft,nyfft,nlev)
      if NOT keyword_set(nodiff) then begin
         diffpsffft = complexarr(nxfft,nyfft,nlev,nlev)
         diffpsf = fltarr(nx,ny,nlev,nlev)
      endif
      ;psf = dblarr(nx,ny,nlev)
      psf = fltarr(nx,ny,nlev)

      dist2 = double(nddist2([nx,ny]*binup, [xcenter,ycenter]*binup+long(binup/2)))
      ;
      ; Loop through the set of base radii of the pixons
      ;

      ;;xtemp = lindgen(nx*binup,ny*binup) MOD long(nx*binup)
      ;;ytemp = lindgen(nx*binup,ny*binup) / long(nx*binup)

      FOR i = 0L, nlev-1L DO BEGIN

         pixon = pxn_psf(dist2, 1.0, dels(i)*binup)  ; parabolic

         ;;pixon = dist2 & pixon(*) = 0            ; uniform square
         ;;good = where(abs(xtemp-xcenter*binup) LE dels(i)*binup AND $
         ;;             abs(ytemp-ycenter*binup) LE dels(i)*binup, ngood)
         ;;if ngood GT 0 then pixon(good) = 1

         pixon = rebin(pixon,nx,ny)
         pixon = pixon/TOTAL(pixon)
         psf(*,*,i) = pixon
        ; if keyword_set(fftconvolution) then begin
            psffft(*) = 0.
            ; TRM 2001-02-21 Changed the +2 to a +1 to fix a shift that was
            ;                being added to the array.
            ;psffft((nxfft-nx+2)/2:(nxfft+nx+2)/2-1,(nyfft-ny+2)/2:(nyfft+ny+2)/2-1) = pixon
            psffft((nxfft-nx+1)/2:(nxfft+nx+1)/2-1,(nyfft-ny+1)/2:(nyfft+ny+1)/2-1) = pixon
            ;;fftpsf(*,*,i) = fft(wrap(psffft),1)
            fftpsf(*,*,i) = fft(shift(psffft,(nxfft+1)/2,(nyfft+1)/2),1)
        ; endif
      ENDFOR
      psf = reform(psf,nx,ny,nlev)  ; TRM added this stuff 2000-Feb-18
      if NOT keyword_set(nodiff) then begin
         psffft1 = fltarr(nxfft,nyfft)
         psffft2 = fltarr(nxfft,nyfft)
         for i=0L,nlev-1L do begin
            for j=0L,nlev-1L do begin
               psffft1(*) = 0.
               psffft1((nxfft-nx+2)/2:(nxfft+nx+2)/2-1, $
                      (nyfft-ny+2)/2:(nyfft+ny+2)/2-1) = psf[*,*,i]
               psffft2(*) = 0.
               psffft2((nxfft-nx+2)/2:(nxfft+nx+2)/2-1, $
                      (nyfft-ny+2)/2:(nyfft+ny+2)/2-1) = psf[*,*,j]
               diff = psffft2-psffft1
               diffpsffft[*,*,j,i] = fft(shift(diff,(nxfft+1)/2,(nyfft+1)/2),1)
               diffpsf[*,*,j,i] = psf[*,*,j] - psf[*,*,i]
            endfor
         endfor
      endif
   endif

   if n_params() GT 3 then begin
      sfunctions = psf
      sdels = dels
      xcen = xcenter
      ycen = ycenter
   endif

   if keyword_set(fftconvolution) and keyword_set(noconserve) then $
      fftimage = fft(image,1)

   ;dimage = double(image)
   ;pimage = make_array(size=size(image),value=0.,/double)
   dimage = float(image)
   pimage = make_array(size=size(image),value=0.,/float)
   timage = pimage

   for i = 0L,nlev-1L do begin
       if i EQ 0 then ll = 0. else ll = ul              ; lower limit for this smoothing scale
       if i EQ nlev-1L then ul = 2.0*dels(i)+1.0 $
       else ul = dels(i)+dels(i+1)   ; upper limit for this smoothing scale
       ;sz = long(dels(i)+1)  ; TRM Added +1 1996-09-17
       sz = long(dels(i))+1  ; TRM Added  1996-09-17
       if usediffres then sz = sz > (long(dels(diffres))+1)  ; use maximum
       pgood = where((lcs GE ll) AND (lcs LT ul),ngood)
       if ngood GT 0 then begin
          if keyword_set(noconserve) then begin
             if keyword_set(fftconvolution) then begin
                if NOT usediffres then $
                   pimage(pgood) = (fftconvol(fftimage,fftpsf(*,*,i)))(pgood) $
                else begin ; TRM 2000-Fev-18
                   pimage(pgood) = (fftconvol(fftimage,diffpsffft(*,*,i,diffres)))(pgood)
                endelse
             endif else begin
                if NOT usediffres then $
                   pimage(pgood) = (convol(dimage,psf(xcenter-sz:xcenter+sz,ycenter-sz:ycenter+sz,i),/center,/edge_truncate))(pgood) $
                else $
                   pimage(pgood) = (convol(dimage,diffpsf(xcenter-sz:xcenter+sz,ycenter-sz:ycenter+sz,i,diffres),/center,/edge_truncate))(pgood)
             endelse
             ;pimage(pgood) = (convol(dimage,psf(*,*,i),/center,/edge_truncate))(pgood)
          endif else begin
             timage(*) = 0.0d0
             timage(pgood) = image(pgood)
             if keyword_set(fftconvolution) then begin
                if NOT usediffres then $
                   pimage = pimage + fftconvol(fft(timage,1),fftpsf(*,*,i)) $
                else begin ; TRM 2000-Feb-18
                   pimage = pimage + fftconvol(fft(timage,1),diffpsffft(*,*,i,diffres)) 
                endelse
             endif else begin
                if NOT usediffres then $
                   pimage = pimage + convol(timage,psf(xcenter-sz:xcenter+sz,ycenter-sz:ycenter+sz,i),/center,/edge_truncate) $
                else begin
                   pimage = pimage + convol(timage,diffpsf(xcenter-sz:xcenter+sz,ycenter-sz:ycenter+sz,i,diffres),/center,/edge_truncate)
                endelse
             endelse
             ;pimage = pimage + convol(timage,psf(*,*,i),/center,/edge_truncate)
          endelse
       endif
   endfor

   return,pimage

end
