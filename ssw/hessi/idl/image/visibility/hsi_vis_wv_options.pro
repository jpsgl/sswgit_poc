;+
; Name: 
;   hsi_vis_wv_options
;
; Purpose: 
;   Widget to set parameters specific to vis_wv image algorithm. Called from
;   hsi_ui_img.  Is a modal widget.
;
; Calling sequence:  
;   new_vals = hsi_vis_wv_options (struct, group=group)
;
; Input arguments:
;   struct - structure containing the values that this widget handles (normally
;     struct is the entire image control structure)
;   group - widget id of calling widget
;
; Output: structure containing new values of uv parameters
;
;
; Written:  
;   by M.A. Duval-Poo 30-Oct-2017
; Modifications:
;   01-Nov-2017, Kim. Changed FIVE_CS to VIS_WV
;-
;===========================================================================

; hsi_five_options_widget_update updates the widgets with the current values of parameters

pro hsi_vis_wv_options_widget_update, state

widget_control, state.w_nscales, set_value=trim(state.vals.vis_wv_nscales, '(i5)')
widget_control, state.w_lam, set_value=trim(state.vals.vis_wv_lam, '(f5.3)')
widget_control, state.w_niter, set_value=trim(state.vals.vis_wv_niter, '(i5)')
widget_control, state.w_autolam, set_button = state.vals.vis_wv_autolam
widget_control, state.w_silent, set_button = state.vals.vis_wv_silent

end

;-----

; hsi_vis_wv_options_event handles events from the widget

pro hsi_vis_wv_options_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue

cancel = 0
exit = 0

case uvalue of

	'reset': begin
		def = hsi_vis_wv_parameters()
		state.vals.vis_wv_nscales = def.nscales
		state.vals.vis_wv_lam = def.lam
		state.vals.vis_wv_niter = def.niter
		state.vals.vis_wv_autolam = def.autolam
		state.vals.vis_wv_silent = def.silent

		hsi_vis_wv_options_widget_update, state
		end

	'cancel': begin
		state.vals = state.orig_vals
		cancel = 1
		exit = 1
		end

	'accept': begin
		exit = 1
		end
		
	'autolam':  state.vals.vis_wv_autolam = event.select

	'silent':  state.vals.vis_wv_silent = event.select

	else:

endcase

if not cancel then begin
	widget_control, state.w_nscales, get_value=vis_wv_nscales
	state.vals.vis_wv_nscales = vis_wv_nscales
	
	widget_control, state.w_lam, get_value=vis_wv_lam
	state.vals.vis_wv_lam = vis_wv_lam
	
	widget_control, state.w_niter, get_value=vis_wv_niter
	state.vals.vis_wv_niter = vis_wv_niter
	
	hsi_vis_wv_options_widget_update, state
endif

; Store values in pointer so we can get them back to calling program after destroy widget
*state.ptr = state.vals

if exit then widget_control, event.top, /destroy else $
	widget_control, event.top, set_uvalue=state

end

;-----

; hsi_vis_wv_options is the main routine that presents the vis_wv options widget

function hsi_vis_wv_options, control, group=group

vals = str_subset (control, $
	['vis_wv_nscales', $
	   'vis_wv_lam', $
	   'vis_wv_niter', $
	   'vis_wv_autolam', $
	   'vis_wv_silent' ])

tlb = widget_base (group=group, $
					title='5-CS algorithm options', $
					/base_align_center, $
					/column, $
					ypad=5, $
					space=5, $
					/frame, $
					/modal )

w_main = widget_base (tlb, $
					/column, $
					/frame )
			
w_nscales = cw_field (w_main, $
			  /string, $
			  title='Number Wavelet Scales: ', $
			  xsize=8, $
			  value=' ', $
			  uvalue='none', $
			  /return_events )
			  
w_lam = cw_field (w_main, $
			  /string, $
			  title='Regularization Parameter (lambda): ', $
			  xsize=8, $
			  value=' ', $
			  uvalue='none', $
			  /return_events )
			  
w_niter = cw_field (w_main, $
			  /string, $
			  title='Max Iterations: ', $
			  xsize=8, $
			  value=' ', $
			  uvalue='none', $
			  /return_events )
			  
w_opt_base = widget_base (w_main, $
			  /nonexclusive, $
			  /column)

w_autolam = widget_button (w_opt_base, $
			  value='Estimate lambda', uvalue='autolam' )

w_silent = widget_button (w_opt_base, $
			  value='Silent', uvalue='silent' )

;w_opt_base = widget_base (w_main, $
;					/nonexclusive, $
;					/column)

w_button_base = widget_base (tlb, $
					/row, $
					space=20 )

w_reset = widget_button (w_button_base, $
					value='Reset to Defaults', uvalue='reset' )

w_cancel = widget_button (w_button_base, $
					value='Cancel', uvalue='cancel')

w_accept = widget_button (w_button_base, $
					value='Accept', uvalue='accept')

state = { $
	vals: vals, $
	orig_vals: vals, $
	ptr: ptr_new(vals), $  ; just used to get values back here after destroy widget
	w_nscales:w_nscales, $
	w_lam:w_lam, $
	w_niter:w_niter, $
	w_autolam:w_autolam, $
	w_silent:w_silent }

hsi_vis_wv_options_widget_update, state

if xalive(group) then begin
	widget_offset, group, xoffset, yoffset, newbase=tlb
	widget_control, tlb, xoffset=xoffset, yoffset=yoffset
endif

widget_control, tlb, /realize

widget_control, tlb, set_uvalue=state

xmanager, 'hsi_vis_wv_options', tlb

vals = *state.ptr
ptr_free, state.ptr

return, vals

end
