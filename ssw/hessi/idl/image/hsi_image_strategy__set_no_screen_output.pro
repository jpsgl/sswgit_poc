;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI IMAGE
;
; PURPOSE:
;   Turns off all parameters that control sending output to the screen for currently
;   selected image algorithm.  Used for batch processing.
;
; CATEGORY:
;       HESSI Imaging
;
; CALLING SEQUENCE:
;       image_obj -> set_no_screen_output
;
; KEYWORDS:
;
; EXAMPLES:
;
; HISTORY:
;  Written: 13-Jul-2001, Kim Tolbert
;
;-
;
;--------------------------------------------------------------------

pro hsi_image_strategy::set_no_screen_output

alg = HSI_Get_Alg_Name( self->get(/image_algorithm) )

self -> set, progress_bar = 0, show_images=0

case alg of

	'Back Projection':

	'Clean':  self -> set, clean_show_maps=0, $
		clean_show_chi=0, $
		clean_mark_box=0, $
		clean_progress_bar=0

	'MEM Sato': self -> set, sato_show_image=0, $
		sato_progress_bar=0

	'MEM VIS': self -> set, vis_show_image=0, $
		vis_progress_bar=0

	'Pixon': self -> set, pixon_noplot=1, $
		pixon_progress_bar=0

	'Forward Fit': self -> set, ff_testplot=0, $
		ff_progress_bar=0

	else:

endcase

end
