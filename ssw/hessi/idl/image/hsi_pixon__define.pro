;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI PIXON CLASS DEFINITION
;
; PURPOSE:
;       Provides HESSI pixon data structures and functions.
;
; CATEGORY:
;       HESSI Imaging
;
; CONSTRUCTION:
;
; GENERIC METHODS:
;
; OBJECT SPECIFIC METHODS:
;
; CONTROL PARAMETERS:
;
; RESTRICTIONS:
;
; EXAMPLES:
;
;
; SEE ALSO:
;       hsi_image
;
; HISTORY:
;       T. Metcalf  2001 Feb 27
; Modifications:
;   22-May-2001, Kim.  Init and use progress_bar control parameter.
;   27-Mar-2002, TRM.  Change sensitivity default to 1.0
;   08-May-2002, TRM   Changed sesitivity default back to 0.0 now the
;                      the data gap problem is being corrected.
;   16-Mar-2004, TRM   Added support for full_pm_calc,
;                      background_model, and variable_metric
;	6-jul-2007, RAS, reset to previous FLATFIELD after pixon alg is run
;	10-may-2010, RAS, use hsi_pixon_control() for initialization of params
;	15-JAN-2015, RAS, set pixon_sizes into USED_PIXON_SIZES an info parameter and not into
;	  the control parameter in the alg_hook, otherwise the user will not understand the inconsistent
;	  results because the pixon_sizes would depend on the previous run and might not me the same as
;	  with an undefined pixon_sizes input!!
;	27-Jan-2015, RAS, use control params, variable_metric, full_pm_calc, and background_model to set and unset
;	  bits in snr
;--------------------------------------------------------------------

FUNCTION HSI_Pixon::INIT, _EXTRA=_extra

control = hsi_pixon_control()	;ras, 10-may-2010
;control.progress_bar = 1		;kim
;control.sensitivity = 0.0
RETURN, self->HSI_Image_Alg::INIT( 'pixon', $ ; acs
                                   CONTROL=control, $ ;kim
                                   INFO={hsi_pixon_info}, $
                                   _EXTRA=_extra )

END
;--------------------------------------------------------------------

PRO HSI_Pixon::Image_Alg_Hook, param, image_out, param_out, _EXTRA = _extra

; this param has pixon as prefix in the original struct def
param = Rep_Tag_Name( param, 'SIZES', 'PIXON_SIZES' )

if self->get(/verbose) then quiet = 0 ELSE quiet = 1
source = self->Get( /SOURCE )
progress_bar = self->get(/progress_bar)  ;kim
xycoordinate = self->get(/xycoordinate) > 0
old_flatfield= Self->Get(/FLATFIELD) ; ras

if ptr_valid(param.pixon_sizes) then pixon_sizes = *param.pixon_sizes $
else if keyword_set(param.pixon_sizes) then $
   message,/info,'The pixon_sizes parameter should be a pointer to an array of pixon sizes (IGNORED)'
if ptr_valid(param.guess) then guess = *param.guess $
else if keyword_set(param.guess) then $
   message,/info,'The guess parameter should be a pointer to an image array (IGNORED)'

; Flags are passed to hsi_pixon_image using bits in the snr keyword
; bit 0 = snr weighting
; bit 1 = full pixon map calculation
; bit 2 = use background model
; bit 3 = use variable metric iteration scheme
;if keyword_set(param.full_pm_calc) then param.snr or=  2 else param.snr xor= 2
;if keyword_set(param.background_model) then param.snr or=  4 else param.snr xor= 4
;if keyword_set(param.variable_metric) then param.snr or=  8 else param.snr xor= 8 ;So-called FAST setting on GUI

image_out = HSI_Pixon_Image( source, sigmai, residual, rgof, error, $
                             iterate=iterate, $
                             quiet=quiet, $
                             resolution=param.resolution, $
                             snr=param.snr, $
                             guess=guess, $
                             pixon_sizes=pixon_sizes, notty=param.noplot, $
                             outresolution=outresolution, /poisson, $
                             sensitivity=param.sensitivity, $
                             pixonmap=pixonmap, $
                             smpattwritedir=param.smpattwritedir, $
                             progress_bar=progress_bar, $	;kim
                             xycoordinate=xycoordinate,  $
                             full_pm_calc = param.full_pm_calc,  $
                             background_model = param.background_model, $
                             variable_metric = param.variable_metric );, $
                            ; bobj=bobj) ;added background pointer structure, ras, 30-apr-2007

param_out =  {HSI_Pixon_Info}
;;param_out.SENSITIVITY   = param.sensitivity
param_out.RESIDUAL      = ptr_new(residual)
param_out.RGOF          = rgof
;;param_out.ERROR         = ptr_new(error)
param_out.ITERATE       = iterate
param_out.OUTRESOLUTION = outresolution
param_out.PIXONMAP      = ptr_new(pixonmap)
param_out.USED_PIXON_SIZES   = ptr_new(pixon_sizes)

self->set,pixon_sizes = ptr_new(pixon_sizes)
self->set,/no_update, FLATFIELD=old_flatfield ;ras, 6-jul-2

END

;--------------------------------------------------------------------

PRO HSI_Pixon__Define

self = {HSI_Pixon, $
        INHERITS HSI_Image_Alg }

END
