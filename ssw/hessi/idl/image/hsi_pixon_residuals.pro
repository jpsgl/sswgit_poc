
;+

function hsi_pixon_residuals,image,dobj,ceobj,a2d,harmonics, $
                             modprofile=modprofile,sspatt=sspatt, $
                             noresidual=noresidual,setunits=setunits, $
                             nounits=nounits,background=bobj

;NAME:
;     hsi_pixon_residuals
;PURPOSE:
;     compute the data residuals for an image
;CATEGORY:
;CALLING SEQUENCE:
;     residuals = hsi_pixon_residuals(image,dobj,ceobj,a2d,harmonics)
;INPUTS:
;     image = the image. fltarr(nx*ny)
;     dobj = pixon data object, array of pointers, [27,5]
;     ceobj = modulation pattern object
;     a2d = list of valid a2d's from hsi_pixon_get_data
;     harmonics = list of valid harmonics from hsi_pixon_get_data
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS
;     modprofile = returns with the reconstructed data.
;                  residuals = data-modprofile.
;     sspatt = index array giving the image pixels used if an image subset is
;              being passed in.
;     /noresidual = return the modulation profile rather than the residual.
;                   this is slightly faster than using the modprofile keyword
;                   since the residual is never computed in this case.
;OUTPUTS:
;     residuals = vector of residuals, [nd]
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;PROCEDURE:
;MODIFICATION HISTORY:
;     T. Metcalf 1999-Dec-09
;     19-jun-2002, ras, protect against division by 0 in computation of nunits
;-

   common hsi_pixon_residuals_private,units,nunits

   setup = 1  ; flag to check if need to set up arrays
   setupunits = 1

   if n_elements(units) LE 0 then begin
      szdobj = size(dobj)
      ;units = ptrarr(27,5)
      units = ptrarr(szdobj[1])
   endif
   if n_elements(nunits) LE 0 then begin
      szdobj = size(dobj)
      ;units = ptrarr(27,5)
      nunits = ptrarr(szdobj[1])
   endif
   for i=0,n_elements(a2d)-1 do begin
      nharmonics = n_elements(*harmonics[i])
      for j=0,nharmonics-1 do begin
         if NOT keyword_set(noresidual) then fobs = *dobj[a2d[i],(*harmonics[i])[j]]
         ; Next lines adjust units, but this should not be done here???
         ; I think these corrections should be applied to the modulation
         ; patterns when they are calculated.
         if keyword_set(setunits) OR $
           ((NOT ptr_valid(units[a2d[i],(*harmonics[i])[j]]))) OR $
           ((NOT ptr_valid(nunits[a2d[i],(*harmonics[i])[j]])))then begin
            if setupunits then begin
               ; integration time in seconds.  This give the resulting image
               ; in units of counts/second at the detectors.
               time_bin = float(ceobj->get(/time_bin_def)*ceobj->get(/time_bin_min))/2L^20
               ;celist = ceobj->getcalibeventlist()
               celist = ceobj->GetData(class_name='hsi_calib_eventlist')
               det_eff = (ceobj->get(/cbe_det_eff)).rel
               setupunits = 0
               message,/info,'Resetting units'
            endif
            ; Since the data are in counts, multiply the image (counts/sec)
            ; by time.  If we wanted the image in counts/sec/cm**2, then we
            ; would have to *multiply* by area as well as time here.  The
            ; units variable converts the image units to counts at the
            ; detector.
            qtrans = (*celist[a2d[i],(*harmonics[i])[j]]).gridtran
            ; Can throw in a qtrans factor to get counts per second falling
            ; on the grid, rather than at the detector.  Just multiply the
            ; units below by qtrans.

            ; hsi_annsec_profile multiplies the image by
            ; time_bin*livetime*gridtran.  So the units on the image are
            ; assumed to be cnts/sec before the grid.  I can make a units
            ; adjustment with my units variable if this is not what is desired.
            ; For example if units=ndetectors, then the image unit will be
            ; in cnts/sec/detector.

            ; Assume that the image is in units of
            ; counts/detector.  We need to convert this to
            ; counts over the time_bin intervals for
            ; comparison to the actual data
;            ttime = (ceobj->Get(/time)).time_range
            ttime = (ceobj->Get(/time_range))
            ttime = ttime[1]-ttime[0]  ; total observation time
            det_index_mask=ceobj->get(/det_index_mask)
            test = where( det_index_mask, ndet)
            ; Since hsi_annsec_profile applies some units we have to
            ; counteract this if /nounits is set.
            nunits[a2d[i],(*harmonics[i])[j]] = $
               ptr_new(f_div( 1.0,(time_bin[a2d[i] MOD 9] * qtrans * $
                       (*celist[a2d[i],(*harmonics[i])[j]]).livetime * $
                       ((*celist[a2d[i],(*harmonics[i])[j]]).flux_var) * $
                        det_eff[a2d[i] MOD 9])))

            ; For consistency, whatver the units are here, they must be
            ; set in hsi_pixon_bproj to be whatever it is here multiplied
            ; by time_bin*gridtran which is done in the
            ; hsi_annsec_profile routine.
            units[a2d[i],(*harmonics[i])[j]] = ptr_new(1.)
            ;units[a2d[i],(*harmonics[i])[j]] = $
            ;   ptr_new(time_bin[a2d[i] MOD 9] * $
            ;           (*celist[a2d[i],(*harmonics[i])[j]]).livetime)
            ;if i EQ n_elements(a2d)-1 AND j EQ nharmonics-1 then $
            ;   message,/info,'Image units: counts/second at the detector'
         endif

         ;mtemp = ceobj->GetData(CLASS_NAME = 'HSI_MODUL_PROFILE', $
         ;                       vimage=image, $
         ;                       this_det=a2d[i], $
         ;                       this_harmonic=(*harmonics[i])[j])
         time_unit=ceobj->get(/time_unit)
         checkvar, time_unit, 16
         mtemp = hsi_annsec_profile(image, $
                                    ceobj, $
                                    this_det=a2d[i], $
                                    this_harmonic=(*harmonics[i])[j], $
                                    time_unit=time_unit)
         if keyword_set(nounits) then begin
            mtemp = mtemp * (*nunits[a2d[i],(*harmonics[i])[j]])
         endif else begin
            mtemp = mtemp * (*units[a2d[i],(*harmonics[i])[j]])
         endelse
         ; Account for background
         if keyword_set(bobj) then begin
            if ptr_valid(bobj[a2d[i],(*harmonics[i])[j]]) then begin
               mtemp = mtemp + (*bobj[a2d[i],(*harmonics[i])[j]])
            endif
         endif
         if NOT keyword_set(noresidual) then begin
            rtemp = mtemp - fobs
            ;bad = where(fobs LE 0.1,nbad)   ; Fix the dropouts
            ;if nbad GT 0 then rtemp[bad] = 0.0
         endif
         if setup then begin
            modprofile = mtemp
            if NOT keyword_set(noresidual) then residuals = rtemp
            setup = 0
         endif else begin
            modprofile = [modprofile,mtemp]
            if NOT keyword_set(noresidual) then residuals = [residuals,rtemp]
         endelse
      endfor
   endfor

   if keyword_set(noresidual) then return,modprofile

   return,residuals

end
