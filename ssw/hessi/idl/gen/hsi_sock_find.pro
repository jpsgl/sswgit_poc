;+
; Project     : HESSI
;
; Name        : HSI_SOCK_FIND
;
; Purpose     : Find input HESSI files on remote server
;
; Category    : Utility, sockets
;
; Syntax      : IDL> found=hsi_sock_find(file)
;
; Inputs      : FILE = file name to search for 
;                      [e.g. hsi_20020310_232000_002.fits]
;
; Outputs     : FOUND = found file with URL path and directory
;
; History     : Written 21 March 2002, D. Zarro (L-3Com/GSFC)
;
; Contact     : dzarro@solar.stanford.edu
;-

function hsi_sock_find,file,err=err

err=''
if is_blank(file) then return,''

;-- ping nearest server

server=hsi_sock_server(path=path,err=err,/verbose)

if err ne '' then return,''

;-- get time of file

fid=obj_new('fid')
time=fid->file2time(file)
obj_destroy,fid

;-- directory to search

fid=get_fid(time,/full,delim='/',/no_next)

;-- search via sockets

break_file,file,dsk,dir,name,ext
dfile=name+ext
for i=0,n_elements(fid)-1 do begin
 rfiles=sock_find(server,dfile,path=path+'/'+fid[i],count=rcount)
 if rcount gt 0 then files=append_arr(files,rfiles)
endfor

count=n_elements(files)
if count eq 0 then begin
 message,file+' not found on http://'+server,/cont
 return,''
endif
return,'http://'+server+files 

end


