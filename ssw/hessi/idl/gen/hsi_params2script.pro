;+
; PROJECT:
;       HESSI
;
; NAME:
;       hsi_params2script
;
; PURPOSE: Generate a script that creates an object and sets parameters into it with  values
;	that are currently changed from default values in the input object.
;
; EXPLANATION:
;	If /all_params is set, then script includes all control parameters requested (either all, or all in params
;	list passed in), not just changed params.
;
;	Currently this is called from the GUI, and the list of parameters controlled by the GUI is passed in.
;	Note - a script generated without a list of parameters currently will not run correctly - need to exclude some
;	parameters, e.g.if obs_time_interval is set, don't want to set filename because it unsets obs_time_interval.
;
; CATEGORY:
;
; CALLING SEQUENCE:
;       result = hsi_params2script(obj)
;
; INPUTS:
;	obj - input object.
;
; OPTIONAL INPUTS:
;	params - list of parameter names to set in the output script.  If not provided, params is set to
;	  names of all control parameters in object.
;
; OUTPUTS:
;	The function returns a string array containing the lines of the script.  Also writes them in the
;	  output file selected.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;	outfile - Name of output file to write script to.  If not set, dialog pops up.
;	all_params - include all parameters in params list provided (if none, then
;	   all control parameters in object) in script instead of just parameters that are different
;	   from default.
;	multi_times, multi_ebands - temporary, until we start using hsi_multi_image object
;
; COMMON BLOCKS:
;       None.
;
; PROCEDURE:
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; EXAMPLES:
;	params = ['obs_time_interval', 'time_range', 'det_index_mask', 'energy_band']
;	script = hsi_params2script (image_obj, params, outfile='test.pro')
;
;	Result is a script that looks something like this:
;		obj = hsi_image()
;		obj-> set, obs_time_interval= [' 3-Nov-2003 09:41:16.000', ' 3-Nov-2003 10:03:44.000']
;		obj-> set, time_range= [622.00000D, 626.00000D]
;		obj-> set, det_index_mask= [[0B, 0B, 0B, 0B, 1B, 1B, 1B, 1B, 0B], [0B, 0B, 0B, 0B, 0B, 0B, $
;		 0B, 0B, 0B], [0B, 0B, 0B, 0B, 0B, 0B, 0B, 0B, 0B]]
;		obj-> set, energy_band= [6.00000, 50.0000]
;
; SEE ALSO:
;
; WRITTEN: Kim Tolbert, 2-Jan-2004
;
; MODIFICATIONS:
;  3-May-2004. For multi_image objects, was removing xyoffset from params to set.  Don't know why.
;    That line is commented out now.
;	21-Sep-2006, Kim.  Added lots of comments to put in header of script explaining how to use it.
;-
;
; multi stuff is just temporary since we're not actually using an hsi_multi_image object yet in
; the GUI.  When we are, remove all the multi stuff in here.  Right now for multi images, always
; pass in both multi_times and multi_ebands even if one is single.


function hsi_params2script, obj, params, outfile=outfile, all_params=all_params, $
	multi_times=multi_times, multi_ebands=multi_ebands

changed_only = keyword_set(all_params) eq 0

class =  strlowcase( obj_class(obj) )

if size(outfile, /tname) ne 'STRING' then begin
	outfile = class + '_script.pro'
	outfile = dialog_pickfile (path=curdir(), filter='*.pro', $
		file=outfile, $
		title = 'Select output file',  $
		group=group, $
		get_path=path)
endif

break_file, outfile, disk, dir, script_name, ext

control = obj -> get(/control)

; if use_flare_xyoffset is set, don't set xyoffset
if tag_exist(control, 'use_flare_xyoffset') then $
	if control.use_flare_xyoffset then control = rem_tag(control,'xyoffset')

; if sp_chan_binning is set, then don't set sp_energy_binning
if tag_exist(control, 'sp_chan_binning') then $
	if control.sp_chan_binning[0] ne 0 then control = rem_tag(control,'sp_energy_binning')

tags = strlowcase(tag_names(control))

multi = n_elements(multi_times) gt 2 or n_elements(multi_ebands) gt 2 ? 1 : 0

if changed_only then begin
	def_obj = obj_new(class)
	if class eq 'hsi_image' then def_obj -> set, image_algorithm=control.image_algorithm
	def_control = def_obj -> get(/control)
	def_tags = strlowcase(tag_names(def_control))
endif

if multi then begin
	class = 'hsi_multi_image'
;	control = rem_tag(control, 'xyoffset')
	control = rem_tag(control, 'energy_band')
	control = rem_tag(control, 'im_energy_binning')
	tags = strlowcase(tag_names(control))
endif

data_type = ssw_strsplit(class, 'hsi_', /tail)
chg_text = changed_only ? 'only parameters changed from default settings.' : 'all control parameters.'

out = ['; ' + class + ' script - created ' + systime(0) + ' by hsi_params2script', $
	'; Includes ' + chg_text, $
	';', $
	'; Note: This script simply sets control parameters in the '+class + ' object as they', $
	';  were when you wrote the script.  To make the object do anything in this script, ', $
	';  you need to add some action commands.', $
	';  For instance, the command', $
	';   ' + data_type + '= obj -> getdata()', $
	'; would tell the object to generate the ' + data_type + '.', $
	';', $
	'; For a complete list of control parameters look at the tables in', $
	'; http://hesperia.gsfc.nasa.gov/ssw/hessi/doc/hsi_params_all.htm', $
	';', $
	'; There are several ways to use this script (substitute the name of your', $
	'; script for hsi_xx_script in the examples below):', $
	';', $
	'; A. Execute it as a batch file via @hsi_xx_script', $
	'; or', $
	'; B. Compile and execute it as a main program by:', $
	';    1. Uncomment the "end" line', $
	';    2. .run hsi_xx_script    (or click Compile, then Execute in the IDLDE)', $
	';    3. Use .GO to restart at the beginning of the script', $
	'; or', $
	'; C. Compile and run it as a procedure by:', $
	';    1. Uncomment the "pro" and "end" lines.', $
	';    2. .run hsi_xx_script    (or click Compile in the IDLDE)', $
	';    3. hsi_xx_script, obj=obj', $
	';', $
	'; Once you have run the script (via one of those 3 methods), you will have an ', $
	'; '+class+' object called obj that is set up as it was when you wrote the script.', $
	'; You can proceed using obj from the command line or the hessi GUI.  To use ', $
	'; it in the GUI, type', $
	';   hessi,obj', $
	';', $
	';', $
	'; To run this script as a procedure, uncomment the "pro" line and ', $
	'; the last line (the "end" line).', $
	';pro ' + script_name + ', obj=obj']

out = [out, $
	';', $
	'obj = ' + class + '()' ]

; if user didn't pass in list of params, set params to all tags in control structure
if not exist(params) then begin
	params = tags
	params_is_tags = 1
	countc = 1
endif else params_is_tags = 0

for i = 0,n_elements(params)-1 do begin
	qc = params_is_tags ? i : where (strlowcase(params[i]) eq tags, countc)
	if countc gt 0 then begin
		if changed_only then begin
			qd = where (strlowcase(params[i]) eq def_tags, countd)
			if countd gt 0 then begin
				; since image_alg is already set into obj (above), it won't appear to be changed, so always include
				if params[i] ne 'image_algorithm' then $
					if same_data(control.(qc[0]), def_control.(qd[0])) then continue	;skip if same
			endif else continue
		endif
		val = control.(qc[0])
		; We need absolute times to be anytim ascii format (not seconds) or
		;   we will lose precision.
		; This is a kludge because we're assuming that all time params have 'time' in name
		;   and are double.  If < 1.e6, it's a relative time.
		if size(val, /tname) eq 'DOUBLE' and strpos(params[i], 'time') ne -1 then begin
			if val[0] gt 1.e6 then val = anytim(val,/vms)
		endif
		sval = val2string(val)
		if sval[0] ne 'BAD' then out = [out, 'obj-> set, ' + params[i] + '= ' + sval ]
	endif
endfor

if multi then begin
	;If duration of all time intervals is same, then can set im_time_interval to duration
	; and set multi_time_range to full range of intervals.  Otherwise have to use
	; im_time_interval to spell out all intervals
	if n_elements(multi_times) gt 2 then begin
		dur = multi_times[1,*] - multi_times[0,*]
		q = where (abs(dur - dur[0]) gt .0001, count)
		if count eq 0 then begin	; all durations are same
			multi_time_range = minmax(multi_times)
			; use average dur to round out teeny differences (e.g. to get .8 instead of .7999999)
			out = [out, 'obj->set, im_time_interval=' + val2string(average(dur)), $
			'obj-> set, multi_time_range=' + val2string(anytim(multi_time_range,/vms))]
		endif
	endif
	if not exist(multi_time_range) then $
		out = [out, 'obj-> set, im_time_interval=' + val2string(anytim(multi_times,/vms)) ]

	out = [out, $
		 'obj-> set, im_energy_binning=' + val2string(multi_ebands), $
		 "obj-> set, multi_fits_file='hsi_imagecube_test.fits'" ]
endif

case data_type of
	'image': begin
		out = [out, $
		'', $
		'; Uncomment any of the following lines to take the action on obj', $
		'; Note: these are just examples.  There are many more options.', $
		';data = obj-> getdata()    ; retrieve the last image made', $
		';data = obj-> getdata(use_single=0)  ; retrieve all images in cube', $
		';obj-> plot               ; plot the last image', $
		';obj-> plotman            ; plot the last image in plotman', $
		';obj-> plotman, /choose   ; choose which image(s) in cube to plot in plotman']
		end
	'spectrum': begin
		out = [out, $
		'', $
		'; Uncomment any of the following lines to take the action on obj', $
		'; Note: these are just examples.  There are many more options.', $
		';data = obj->getdata()    ; retrieve the spectrum data', $
		';obj-> plot               ; plot the spectrum', $
		';obj-> plotman            ; plot the spectrum in plotman', $
		';obj-> plot, /pl_time     ; plot the time history', $
		';obj-> plotman, pl_time   ; plot the time history in plotman']
		end
	else:
endcase


out = [out, $
	';', $
	'; To run this script as a main program or procedure, uncomment the "end" line below.', $
	'; (To run as a procedure also uncomment the "pro" line above.)', $
	';end' ]

; Wrap long lines on ', '
out = wrap_txt(out, delim=', ', length=90)

if outfile ne '' then begin
	wrt_ascii, out, outfile, err_msg=err_msg
	if err_msg eq '' then print,'Wrote commands in script file ' + outfile
endif else print,'No output file name selected. No file written.'

if exist(def_obj) then obj_destroy, def_obj

return,out
end


