;+
;Name: hsi_theses2html
;
;Purpose: Create html list of RHESSI theses for RHESSI web site
;  (see comments near the end for how to make a table)
;
;Method: Start with Excel spreadsheet qingrong.csv originally created by Qingrong Chen (he found the original theses be searching web manually,
; they're hard to find that way, so I sent a request to max millenium list for recent degrees - worked well.)
; 
; To update
;   1. Manually update the spreadsheet with new theses (on Windows machine) - should be alphatically ordered within each year for
;      each degree type.
;   2. Manually do a global replace of commas with dollar signs (so that in the csv file, the commas will be delimiters) (but don't
;      save .xlsx file with $s)
;   3. Save the spreadsheet to a CSV file qingrong.csv. Copy the csv and xlsx files to 
;      /var/www/html/rhessi3/news-and-resources/theses/qingrong.csv (must close Excel before copy will work).
;   4. Run this program on hesperia as kim in dir /home/kim/editweb via the call hsi_theses2html
;   
; This program reads in qingrong.csv, Converts the $s back to commas, break the theses up into the different 
; types of degrees, and the years within each degree and makes an html at 
; http://hesperia.gsfc.nasa.gov/rhessi3/news-and-resources/theses/index.html.
; (If that file already exists, the old one is first copied to a file name containing the date.)
; Also makes a plot and table and links to them from the html page.
; 
; Should be run on hesperia by account kim (or an account that has permission to write in web area, 
; /var/www/html/rhessi3/news-and-resources/theses) from the directory /home/kim/editweb
;
; Input Keywords:
;   checkurl - if set, just check that the URLs in the csv file are valid URLs and return.
;   values - return number of each type of degree for each year [nyear, ntype] (last column is total for year)
;   
;Written: Kim Tolbert, Jan 2014
;Modifications:
; 22-Jan-2016, Make a plot and a table of results and link to them from html.
; 16-Mar-2016, Changed link to table from a jpeg thumbnail of text (ugly) to text (after figuring out how to align)
; 25-Jan-2017, Expanded header doc
;-

pro hsi_theses2html, checkurl=checkurl, values=numyr_save

qingrong_data = rd_ascii('/var/www/html/rhessi3/news-and-resources/theses/qingrong.csv')
qingrong_data = str2cols(qingrong_data,',', /unaligned)
qin = str_replace(qingrong_data, '$',',')
numtotal = n_elements(qin[0,*])

if keyword_set(checkurl) then begin
  for ii=0,numtotal-1 do begin
    if ~sock_check(qin[7,ii]) then print,'Bad URL: ',  qin[*,ii]
    print,''
  endfor  
  return
endif

codes = [150, 154, 192, 214, 224, 225, 228, 229, 232, 233, 237, 252]
chars = [' ', 's', 'A', 'O', 'a', 'a', 'a', 'a', 'e', 'e', 'i', 'u']
ncodes = n_elements(codes)

; first replace any of the non-printing codes with the closest characters
for ii = 0,numtotal-1 do begin
  for mm=0,7 do begin
    bb = byte(qin[mm,ii])
    for mc = 0,ncodes-1 do begin
      qc = where(bb eq codes[mc],nc)
      if nc gt 0 then bb[qc] = byte(chars[mc])
    endfor
    qin[mm,ii] = string(bb)
  endfor
endfor
 
types = ['PhD', 'Master', 'Diploma', 'Bachelor', 'Senior']
ttypes = ['PhD', "Master's", 'Diploma', "Bachelor's", 'Senior']

q = where (qin[5,*] eq 'AST', numast)
q = where (qin[5,*] eq 'GRB', numgrb)
q = where (qin[5,*] eq 'INS', numins)
q = where (qin[5,*] eq 'SOL', numsol)
q = where (qin[5,*] eq 'TGF', numtgf)


out = ['<div class="csc-header csc-header-n1">', $
  '<h1 style="text-align:center;" class="csc-firstHeader">All RHESSI PhD, Master, Diploma, Bachelor and Senior Theses (' + trim(numtotal) + ')</h1></div>', $
  '', $
  '<h2 align="center">Last updated: ' + strmid(!stime, 0, 11) + '</h2>', $
  '<br>', $
  '', $
  '<table border="0" cellspacing="1" width="80%" align="center">', $
  '<tr>', $
  '<td width="55%">', $
  '<h2><a href="#thtype">thtype Theses (numth)</a></h2>', $
  '<h2><a href="#thtype">thtype Theses (numth)</a></h2>', $
  '<h2><a href="#thtype">thtype Theses (numth)</a></h2>', $
  '<h2><a href="#thtype">thtype Theses (numth)</a></h2>', $
  '<h2><a href="#thtype">thtype Theses (numth)</a></h2>', $
  '<h2><a href="#Summary">Summary by Institution and Location</a></h2>', $
  '</td>', $
  '<td width="45%">', $
  '<h2>Codes:</h2>', $
  '<h3>&nbsp;&nbsp; AST - Astrophysics (' + trim(numast) + ')</h3>', $
  '<h3>&nbsp;&nbsp; GRB - Gamma-ray Burst (' + trim(numgrb) + ')</h3>', $
  '<h3>&nbsp;&nbsp; INS - Instrumentation (' + trim(numins) + ')</h3>', $
  '<h3>&nbsp;&nbsp; SOL - Solar (' + trim(numsol) + ')</h3>', $
  '<h3>&nbsp;&nbsp; TGF - Terrestrial Gamma-ray Flashes (' + trim(numtgf) + ')</h3>', $
  '</td>', $
  '</tr>', $
  '</table>']
;  '<br>', $
;  '<p align="center">', $
;  '<a href="theses.png">', $
;  '<img border="0" src="theses_thumb.png" width="133" height="106"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $
;  '<a href="theses_table.html">', $
;  '<img border="0" src="theses_table_thumb.jpg" width="122" height="90"></a>', $
;  '</p>']
  
plot_table = ['<br>', $
  '<div align="center">', $
  '<center>', $
  '<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="400" align="center">', $
  '<tr>', $
  '<td width="50%" align="center">', $
  '<a href="theses.png">', $
  '<img border="0" src="theses_thumb.png" width="133" height="106"></a>', $
  '</td>', $
  '<td width="50%" style="vertical-align:middle; font-size: 13px" align="center">', $
  '<a href="theses_table.html"><b>Table<br>of RHESSI<br>Theses<br>per<br>Year</b></a>', $
  '</td>', $
  '</tr>', $
  '</table>', $
  '</center>', $
  '</div>']

out = [out, plot_table]
  
pretype = ['<br>', $
    '<div class="boxed">', $
    '<div class="csc-header csc-header-n1"><a name=thtype></a>', $
    '<div class="boldcenter16">RHESSI thtype Theses, Total = numth</div>', $
    '</div>', $
    '<p class="bodytext">', $
    ' ', $
    '<ul>']

posttype = '</ul></div><br>'
    
preyear = ['<div class="csc-header csc-header-n1">', $
  '<h2 class="csc-firstHeader">yyyy - nnnn RHESSI thtype Theses</h2>', $
  '</div>', $
  '<p class="bodytext">', $
  ' ', $
  '<ol style="list-style-type: decimal">']
preyear_notfirst = '<hr class="hrthin">'
postyear = '</ol>'

titlelink = '<br>[code] <a href="link" target="_blank">name</a></li>'
titlenolink = '<br>[code] name</li>

presummary = [' ', $
  '<br>', $
  '<br>', $
  '<div class="boxed">', $
  '<br>', $
  '<div class="csc-header csc-header-n1"><a name=Summary></a>', $
  '<div class="boldcenter16">RHESSI Theses by Institution (' + trim(numtotal) + ')</div>', $
  '</div>', $
  ' ', $
  '<div align="center">', $
  '<table border="0" cellspacing="1" width="70%" class="fnt_sz_12">', $
  ' ']
inst = '<tr> <td width="70%">inst</td>     <td width="20%">area</td>    <td width="10%">num</td> </tr>'
postinst = ['</table>', $
  ' ', $
  '</div>', $
  '<br>', $
  '<div class="csc-header csc-header-n1"><a name=Summary></a>', $
  '<div class="boldcenter16">RHESSI Theses by Location (' + trim(numtotal) + ')</div>', $
  '</div>', $
  ' ', $
  '<div align="center">', $
  '<table border="0" cellspacing="1" width="30%" class="fnt_sz_12">', $
  ' ']
area = '<tr> <td width="70%">area</td>   <td width="30%">num</td> </tr>'
postarea = ['</table>', $
  ' ', $
  '</div><br><br></div><br>']

inumline = where(strpos(out, '#thtype') ne -1) ; line number of lines with type of thesis and number for header

; Save numbers for table. First column is year, then # for each type, then total for year
numyr_save = intarr(41, n_elements(types)+2)

for it = 0,n_elements(types)-1 do begin
  qtype = where(strpos(qin[2,*],types[it]) ne -1, numth)
  out[inumline[it]] = str_replace(out[inumline[it]], 'thtype', ttypes[it])
  out[inumline[it]] = str_replace(out[inumline[it]], 'numth', trim(numth))
  type_head = str_replace(pretype, 'thtype', ttypes[it])
  type_head = str_replace(type_head, 'numth', trim(numth))
  if numth eq 1 then type_head = str_replace(type_head, 'Theses', 'Thesis')
  out = [out, type_head]
  thist = qin[*,qtype] 
  
  first = 1
  for iyear = 2030,1990,-1 do begin
    qyear = where(thist[1,*] eq trim(iyear), numyr)
    numyr_save[iyear-1990, 0] = iyear
    numyr_save[iyear-1990, it+1] = numyr
    if numyr gt 0 then begin
      if ~first then out = [out, preyear_notfirst]
      first = 0
      year_head = str_replace(preyear, 'yyyy', trim(iyear))
      year_head = str_replace(year_head, 'nnnn', trim(numyr))
      year_head = str_replace(year_head, 'thtype', ttypes[it])
      if numyr eq 1 then year_head = str_replace(year_head, 'Theses', 'Thesis')
      out = [out, year_head]
      this = thist[*,qyear]
      for ii = 0,numyr-1 do begin               
        if this[7,ii] eq '' then title = str_replace(titlenolink, 'name', this[6,ii]) else begin
          title = str_replace(titlelink, 'link', this[7,ii])        
          title = str_replace(title, 'name', this[6,ii])          
        endelse
        title = str_replace(title, 'code', this[5,ii])    
        out = [out, '<li>' + arr2str([this[0,ii], this[1,ii], this[3,ii]], ', '), title]        
      endfor
      out = [out, postyear]
    endif
  endfor
    
  out = [out, posttype]    
endfor

out = [out, presummary]
uniq_inst = get_uniq(qin[3,*], sorder, count=count)
for ii=0,count-1 do begin
  q = where(qin[3,*] eq uniq_inst[ii], nq)
  thisinst = str_replace(inst, 'inst', uniq_inst[ii])
  thisinst = str_replace(thisinst, 'area', qin[4,sorder[ii]])
  thisinst = str_replace(thisinst, 'num', trim(nq))
  out = [out, thisinst]
endfor
out = [out, postinst]
uniq_area = get_uniq(qin[4,*], count=count)
for ii=0,count-1 do begin
  q = where(qin[4,*] eq uniq_area[ii], nq)
  thisarea = str_replace(area, 'area', uniq_area[ii])
  thisarea = str_replace(thisarea, 'num', trim(nq))
  out = [out, thisarea]
endfor
out = [out, postarea]


; Put total for each year in last column
numyr_save[*,6] = total(numyr_save[*,1:5],2)

hsi_thesestable, values=numyr_save
hsi_thesesplot,values=numyr_save, /copy

outfile = '/var/www/html/rhessi3/news-and-resources/theses/theses_list.html'

if file_exist(outfile) then file_copy, outfile, str_replace(outfile,'.html','_' + time2file(!stime) + '.html'), /verbose
print,'Writing output file ' + outfile
prstr,out, file=outfile

end