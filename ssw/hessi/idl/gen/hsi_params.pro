;+
; Name: hsi_params
; Purpose: Define structure with information for all HESSI control and info parameters
; Calling Sequence: hsi_params, param_struct
; 
; NOTE:  THIS ROUTINE IS GENERATED AUTOMATICALLY BY HSI_PARAMS_WRITE_PRO.  Do not edit.
;-

pro hsi_params, d

k = -1
d = replicate({hsi_params_str}, 2000)

; Control parameters for hsi_aspect_solution
k = k + 1
d[k].name = 'aspect_mode'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'aspect_cntl_level'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '-2 - 10'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Control amount of diagnostic information presented. Higher ' + $
    'number = more.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'aspect_sim'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, use simulated aspect solution'
d[k].more_doc = ''

k = k + 1
d[k].name = 'aspect_time_range'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Time interval to calculate aspect for. If not set, defaults ' + $
    'to obs_time_interval'
d[k].more_doc = ''

k = k + 1
d[k].name = 'as_interpol'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'quad'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Set to quad to enable quadratic interpolation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'as_no_extrapol'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, don''t extrapolate aspect solution to times not ' + $
    'covered.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ras_time_extension'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'double(2)'
d[k].units = 'sec'
d[k].default = '[-1400,1400]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Seconds to add to beginning/end of aspect time range'
d[k].more_doc = ''

k = k + 1
d[k].name = 'saszero'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, assume a perfect aspect solution'
d[k].more_doc = ''

k = k + 1
d[k].name = 'as_spin_period'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'double'
d[k].units = 'sec'
d[k].default = '4.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Spacecraft spin period for aspect calculations'
d[k].more_doc = ''

k = k + 1
d[k].name = 'as_roll_offset'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'double'
d[k].units = 'radian'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If as_roll_solution=''FIX'', then this is roll angle at ' + $
    'beginning of file.'' '
d[k].more_doc = ''

k = k + 1
d[k].name = 'as_roll_solution'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'DBASE'
d[k].range = '"DBASE","FIX, PMT, RAS"'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Method for determining roll angle'
d[k].more_doc = 'Options are:<P>DBASE - read roll solution from the provided ' + $
    'database<P>FIX - fixed spin period, use AS_SPIN_PERIOD and ' + $
    'AS_ROLL_OFFSET<P>PMT - use the PMTRAS to generate the roll ' + $
    'solution<P>RAS - use the CCD-RAS to generate the roll ' + $
    'solution'

k = k + 1
d[k].name = 'as_point_solution'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'SAS'
d[k].range = '"SAS, FSS"'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sc_sun_offset'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'float(2)'
d[k].units = ''
d[k].default = '[0.,0.]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = '+N,+W offset of S/C from Sun center'
d[k].more_doc = ''

k = k + 1
d[k].name = 'equat_ns'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = '=1 equatorial North, =0 solar North'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pmtras_diagnostic'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'uint'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Controls diagnostic output from pmtras_analysis'
d[k].more_doc = 'PMTRAS_DIAGNOSTIC is a bit flag for selecting the diagnostic ' + $
    'information produced by the pmtras_analysis ' + $
    'procedure.<P>Default is 0 - no plots. Unless /no_starid is ' + $
    'set, then default is 512. Note that 2^11 (2048) enables ' + $
    'print diagnostics, all others are plots. <P>Set the ' + $
    'following bit (or add values to set multiple bits) for the ' + $
    'following diagnostics: <P>Window 1: 2 Fractional Time ' + $
    'Interval Distribution<P>Window 2: 4 Blip Total vs ' + $
    'Time<P>Window 3: 8 Preliminary Timing Residuals<P>Window 4: ' + $
    '16 Alignment in PMTRAS_ALIGNMENT_ANALYSIS<P>Window 5: 32 ' + $
    'Intensity/Phase Grouping in PMTRAS_GROUP_BLIPS<P>Window 6: ' + $
    '64 Observed vs Predicted intensity<P>Window 7: 128 ' + $
    'File-Averaged Phase Residuals<P>Window 8: 256 Background vs ' + $
    'Time<P>Window 9: 512 Preliminary Estimate of Rotation ' + $
    'Period<P>Window 10: 1024 Phase Histogram in ' + $
    'PMTRAS_GROUP_BLIPS<P>Window 11: 2048 Enables printed ' + $
    'diagnostics.<P>Window 12: 4096 Blip Total Distribution in ' + $
    'PMTRAS_HISTPLOT<P>Window 13: 8192 Bliptot vs Timing ' + $
    'Correction'

; Control parameters for hsi_binned_eventlist
k = k + 1
d[k].name = 'front_segment'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use detector front segments'
d[k].more_doc = ''

k = k + 1
d[k].name = 'rear_segment'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use detector rear segments'
d[k].more_doc = ''

k = k + 1
d[k].name = 'im_energy_binning'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Defines energy binning. If scalar, interpret as code for ' + $
    'predefined energy bins, otherwise as energy edges'
d[k].more_doc = 'Defines the energy binning for making images. <P>If this is ' + $
    'a single value (scalar), the energy edges are read from the ' + $
    'corresponding ct_edges table in the dbase. To see a ' + $
    'description of the codes, look at the file ' + $
    '$SSW/hessi/dbase/energy_binning.txt. <P>If it is a 1D array ' + $
    'with M values, it considers each value as the edge of a ' + $
    '(contiguous) energy channel (therefore there will be M-1 ' + $
    'energy bins). <P>If it is a 2 x N array, then each 2-element ' + $
    'vector is a separate energy channel, and there will be N ' + $
    'energy bins. <P><P>IM_ENERGY_BINNING is used in conjunction ' + $
    'with EB_INDEX. EB_INDEX is the index into the energy bin ' + $
    'array (starting at 0) to use. ENERGY_BAND will be set to the ' + $
    'energy edges actually used.<P><P>The advantage of using ' + $
    'IM_ENERGY_BINNING and EB_INDEX instead of ENERGY_BAND is ' + $
    'that it will be faster if you are doing multiple energy bins ' + $
    'for a time range, since the binned eventlist will only be ' + $
    'called once for a given time range. <P><P>NOTE: Using ' + $
    'ENERGY_BAND still works for setting the energy band. ' + $
    'However, once you have set IM_ENERGY_BINNING in a given ' + $
    'object, you can no longer use ENERGY_BAND to set the energy ' + $
    'edges.'

k = k + 1
d[k].name = 'time_bin_def'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = 'N/A'
d[k].default = '[1,1,2,4,8,8,16,32,64]'
d[k].range = '1 - 2^6'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Factors multiplying time_bin_min to define ' + $
    'detector-dependent time bin size'
d[k].more_doc = '>use_auto_time_bin'

k = k + 1
d[k].name = 'time_bin_min'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'binary microsec (2^-20 sec)'
d[k].default = '1024'
d[k].range = '1 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Scalar time bin size to be multiplied by time_bin_def to set ' + $
    'time bins for each detector'
d[k].more_doc = '>use_auto_time_bin'

k = k + 1
d[k].name = 'det_index_mask'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte(9)'
d[k].units = 'N/A'
d[k].default = '[0,0,0,1,1,1,1,1,0]'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Flags for selecting detectors'
d[k].more_doc = 'Detector / segment selection is handled differently by the ' + $
    'different objects.<P>NOTE: A2D_INDEX_MASK is bytarr(27) ' + $
    'which should be all 1s. The user should never set this. ' + $
    'Selection is done through the following parameters for each ' + $
    'type of object:<P>For Imaging: <br>DET_INDEX_MASK - ' + $
    'bytarr(9,3) for the 9 detectors, 3 harmonics. Set to 1 to ' + $
    'select. <br>FRONT - If set, then use front segment. <br>REAR ' + $
    '- If set, then use rear segment. <P>For Spectrum and ' + $
    'Lightcurve: <br>SEG_INDEX_MASK - bytarr(18) first 9 are for ' + $
    'front segments of the 9 detectors, second 9 are for rear ' + $
    'segments.<br>DET_INDEX_MASK - bytarr(9) for the 9 detectors. ' + $
    'If set, selects front and rear segments<br>NOTE: Use ' + $
    'SEG_INDEX_MASK or DET_INDEX_MASK, not both'

; Control parameters for hsi_spectrogram
k = k + 1
d[k].name = 'poisson'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_energy_binning'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '14'
d[k].range = '0 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Defines energy binning. If scalar, interpret as code for ' + $
    'predefined energy bins, otherwise as energy edges'
d[k].more_doc = 'Defines the energy binning of the spectra. <P>If this is a ' + $
    'single value (scalar), the energy edges are read from the ' + $
    'corresponding ct_edges table in the dbase. To see a ' + $
    'description of the codes, look at the file ' + $
    '$SSW/hessi/dbase/energy_binning.txt. <P>If it is a 1D array ' + $
    'with M values, it considers each value as the edge of a ' + $
    '(contiguous) energy channel (therefore there will be M-1 ' + $
    'energy bins). <P>If it is a 2 x N array, then each 2-element ' + $
    'vector is a separate energy channel, and the resulting ' + $
    'spectrum has N energy bins.<P>If sp_chan_binning is not 0, ' + $
    'then sp_chan_binning takes precedence over ' + $
    'sp_energy_binning.'

k = k + 1
d[k].name = 'seed'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'coincidence_flag'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, divide the spectrogram into coincident and ' + $
    'anti-coincident events'
d[k].more_doc = '<h3 align="center">Anticoincidence</h3> <p>In the telemetry, ' + $
    'all events are recorded with energy, segment and time ' + $
    'information. In the software, when a front and rear segment ' + $
    'event are recorded in the same detector (sub-collimator) ' + $
    'within a narrow time window, those front-segment events are ' + $
    'flagged as being coincident (probably non-solar).</p> <p>The ' + $
    'following types of front-segment data can be obtained:<br> ' + $
    '&nbsp;&nbsp;anticoincident - front segment events are only ' + $
    'included if there is NO coincident rear event<br> ' + $
    '&nbsp;&nbsp;total - all front-segment events independent of ' + $
    'any rear-segment events (sum of anticoincident and ' + $
    'coincident events)<br> If you want to obtain coincidence ' + $
    'spectrum alone, you must use the command line to take the ' + $
    'difference of the total and anticoincidence spectra. </p> ' + $
    '<p>The image and spectrum objects handle anticoincidence ' + $
    'slightly differently.<p>SPECTRUM OBJECT <p>There are two ' + $
    'control parameters that dictate what front-segment spectrum ' + $
    'is returned by the object:<br> COINCIDENCE_FLAG - controls ' + $
    'what is stored internally<br> &nbsp;&nbsp;0 - (default) only ' + $
    'the anticoincident events are stored<br> &nbsp;&nbsp;1 - the ' + $
    'anticoincident and coincident events are stored ' + $
    '(separately)<br> SUM_COINCIDENCE - controls what is returned ' + $
    'and displayed<br> &nbsp;&nbsp;0 - (default) only ' + $
    'anticoincident events<br> &nbsp;&nbsp;1 - the sum of the ' + $
    'anticoincident and coincident events (this forces ' + $
    'COINCIDENCE_FLAG to be 1 also)</p> <p>The default is to ' + $
    'store and return the anticoincident events. If you set ' + $
    'COINCIDENCE_FLAG=1 (either explicitly or by setting ' + $
    'SUM_COINCIDENCE=1), then the data must be reaccumulated, but ' + $
    'once both anticoincident and coincident data have been ' + $
    'stored, changing these parameters does not require ' + $
    'reprocessing.</p> <p>IMAGE OBJECT<p>In the image object, ' + $
    'COINCIDENCE_FLAG has no effect. SUM_COINCIDENCE controls ' + $
    'whether to use the anticoincidence events alone ' + $
    '(SUM_COINCIDENCE=0, the default), or the sum of the ' + $
    'anticoincident and coincident events (SUM_COINCIDENCE=1). If ' + $
    'you change the value of SUM_COINCIDENCE, the data must be ' + $
    'reaccumulated.<br><p>For rear-segment spectra, there is one ' + $
    'control parameter (we recommend not changing this from the ' + $
    'default of 1):</p> <p>REAR_NO_ANTI <br> &nbsp;&nbsp; 0 - ' + $
    'anticoincidence flag is used to remove counts from rear ' + $
    'segments<br> &nbsp;&nbsp; 1 - (default) do not remove counts ' + $
    'from rear segments based on coincidence flag</p>'

k = k + 1
d[k].name = 'other_a2d_index_mask'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte(27)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Set which other a2ds to return spectrum for'
d[k].more_doc = ''

k = k + 1
d[k].name = 'seg_index_mask'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte(18)'
d[k].units = 'N/A'
d[k].default = 'All 0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Flags for selecting detector and segment'
d[k].more_doc = '>det_index_mask'

k = k + 1
d[k].name = 'rebin_method'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Place-holder for spectral dependent rebinning'
d[k].more_doc = ''

k = k + 1
d[k].name = 'rebin_poisson_flag'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Place-holder for poisson option for rebinning'
d[k].more_doc = ''

k = k + 1
d[k].name = 'rebin_seed'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Place-holder for poisson seed option for rebinning'
d[k].more_doc = ''

k = k + 1
d[k].name = 'contig_energy_edges'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, force energy bins to be contiguous'
d[k].more_doc = ''

k = k + 1
d[k].name = 'livetime_enable'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'default is to process and use livetime'
d[k].more_doc = ''

k = k + 1
d[k].name = 'align512'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Align time edges to 512 busec boundaries used with livetime'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sum_flag'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, sum the spectrograms over collimators'
d[k].more_doc = 'If COINCIDENCE_FLAG is set, but SUM_COINCIDENCE is not set, ' + $
    'then SUM_FLAG will cause collimators to be summed, but ' + $
    'coincidence and and anti_coincidence spectrum will not be ' + $
    'summed.'

k = k + 1
d[k].name = 'sum_coincidence'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, sum the anti-coincidence and coincidence spectrum'
d[k].more_doc = '>coincidence_flag'

k = k + 1
d[k].name = 'rear_no_anti'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, don''t remove counts from rear segments based on ' + $
    'coincidence flag'
d[k].more_doc = '>coincidence_flag'

k = k + 1
d[k].name = 'sp_dp_cutoff'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'This functionality is disabled 17-nov-02'
d[k].more_doc = ''

k = k + 1
d[k].name = 'decimation_correct'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, make front decimation correction'
d[k].more_doc = 'The decimation_correct parameter enables decimation ' + $
    'correction for front segments only.<P>To enable rear segment ' + $
    'decimation correction, set the rear_decimation_correct ' + $
    'parameter to 1.'

k = k + 1
d[k].name = 'rear_decimation_correct'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 -1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, make rear decimation correction'
d[k].more_doc = '>decimation_correct'

k = k + 1
d[k].name = 'decim_apar'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '[1.,2.,1.,4.,400.,4.]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Spectral parameters (vth+bpow) for decimation factor ' + $
    'interpolation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_cull'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, culling is enabled. Default is 1 for imaging, 0 for ' + $
    'spectrum.'
d[k].more_doc = 'USE_CULL and CULL_FRAC are used together. <P>When USE_CULL ' + $
    'is set, any time bin with a datagap deadtime larger than the ' + $
    'fraction defined by CULL_FRAC will have its count(s) ' + $
    'value(s) set to 0 and its livetime set to 0. '

k = k + 1
d[k].name = 'cull_frac'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = ''
d[k].default = '1.0'
d[k].range = '0. - 1.'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Fraction for defining data gaps for culling'
d[k].more_doc = '>use_cull'

k = k + 1
d[k].name = 'use_total_count'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'For developers only. Total_count is normally not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clear_halfscale'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, remove half-scale artifact from the rear high-gain ' + $
    'a2d of all detectors'
d[k].more_doc = ''

; Control parameters for hsi_spectrum
k = k + 1
d[k].name = 'sp_chan_binning'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 8192'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If non-zero, energy bins are on channel boundaries. Scalar ' + $
    'grouping factor. '
d[k].more_doc = 'Defines the channel binning. If > 0, then binning is on ' + $
    'natural energy boundaries (PHA binning), i.e. the channels ' + $
    'are the numbers encoded in the telemetry for each event. ' + $
    'SP_CHAN_BINNING is a grouping factor, so if it is set to N, ' + $
    'then each group of N PHA channels is summed to obtain the ' + $
    'resultant spectrum. <P>Setting SP_CHAN_BINNING to 1 returns ' + $
    'a spectrum of 8192 channels; setting it to 4 returns 2048 ' + $
    'grouped channels. <P>If SP_CHAN_BINNING is 0, then ' + $
    'SP_ENERGY_BINNING parameter controls energy binning.'

k = k + 1
d[k].name = 'sp_chan_min'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_chan_max'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_bin_time_unit'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, sp_time_interval in l64 word as time unit.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_time_interval'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = 'sec'
d[k].default = '4.'
d[k].range = '0 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Defines time interval(s) for spectra. If scalar, interpret ' + $
    'as width, otherwise as time bin edges'
d[k].more_doc = '>obs_time_interval'

k = k + 1
d[k].name = 'sp_semi_calibrated'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, calibrates the count rate spectrum using only the ' + $
    'diagonal elements of the response matrix'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_data_structure'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, then GETDATA returns a structure with data (counts, ' + $
    'rate, or flux) and errors.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_data_unit'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'Counts'
d[k].range = 'Counts, Rate, Flux'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Type of data to return in spectrum - counts, counts/sec, or ' + $
    'counts/sec/cm^2/keV'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_error_out'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'det_index_mask'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'byte(9)'
d[k].units = 'N/A'
d[k].default = 'All 0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Flags for selecting detectors'
d[k].more_doc = '>det_index_mask'

k = k + 1
d[k].name = 'flare_start_time'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'double'
d[k].units = ''
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Include flare start and end time to exclude from corrections'
d[k].more_doc = ''

k = k + 1
d[k].name = 'flare_end_time'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'double'
d[k].units = ''
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'for attenuator and grid trans'
d[k].more_doc = ''

k = k + 1
d[k].name = 'flare_bck_time'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'xyoffset'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = '[600,200]'
d[k].range = '0 - 1000'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Offset of source from Sun center'
d[k].more_doc = ''

; Control parameters for hsi_srm
k = k + 1
d[k].name = 'ct_edges'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '2'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Define counts binning'
d[k].more_doc = 'If a single number, refers to a particular standard binning. ' + $
    'If a string, will be taken to refer to an input file. ' + $
    'Otherwise will be a (N+1) vector containing boundaries for N ' + $
    'channels of the counts side of the matrices (in that order). ' + $
    'If any of the use_saved[]s are defined, the binning of that ' + $
    'will override what is supplied.'

k = k + 1
d[k].name = 'ph_edges'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Define photon binning'
d[k].more_doc = 'If a single number, refers to a particular standard binning. ' + $
    'If a string, will be taken to refer to an input file. ' + $
    'Otherwise will be a (N+1) vector containing boundaries for N ' + $
    'channels of the photon side of the matrices (in that order). ' + $
    'If any of the use_saved[]s are defined, the binning of that ' + $
    'will override what is supplied.'

k = k + 1
d[k].name = 'use_segment'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte(18)'
d[k].units = 'N/A'
d[k].default = '[bytarr(9)+1,bytarr(9)]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, include the corresponding one of 18 detector ' + $
    'segments'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sep_dets'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, return separate matrices for each detector used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sep_virds'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, return separate matrices for each virtual detector'
d[k].more_doc = 'If set, return separate matrices for each virtual detector ' + $
    '(if /sep_dets set) or else separate sums of each virtual ' + $
    'detector type (front,rear,f/r coincidence, rear neighbor ' + $
    'coincidence, and "other")'

k = k + 1
d[k].name = 'astro'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, treat as an astrophysical source (out-of-aperture)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dead_seg'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte(18)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, consider the corresponding one of 18 detector ' + $
    'segments to be passive'
d[k].more_doc = ''

k = k + 1
d[k].name = 'simplify'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = 'bytarr(10)'
d[k].range = '0 - 3'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Code to select submatrix terms to use'
d[k].more_doc = 'Simplify is a ten element byte array where each element ' + $
    'controls the calculation of each of 10 submatrices that make ' + $
    'up the response matrix. <P> The ten submatrices are: ' + $
    '<BR>&nbsp;&nbsp;0 -Grid-pair transmission <BR>&nbsp;&nbsp;1 ' + $
    '- Attenuator transmission <BR>&nbsp;&nbsp;2 - Blanket ' + $
    'transmission <BR>&nbsp;&nbsp;3 - Det. resp. to modulated ' + $
    'photons <BR>&nbsp;&nbsp;4 - Det. resp. to scattering in 2nd ' + $
    'grid and attenuator <BR>&nbsp;&nbsp;5 - Det. resp. to ' + $
    'spacecraft scatter <BR>&nbsp;&nbsp;6 - Det. resp. to Earth ' + $
    'scattering <BR>&nbsp;&nbsp;7 - Detector resolution (w/ ' + $
    'radiation damage) <BR>&nbsp;&nbsp;8 - Extra broadening (no ' + $
    'longer in use) <BR>&nbsp;&nbsp;9 - Detector LLD threshold ' + $
    'cutoff <P> The value in each element is 0,1,2, or 3 and ' + $
    'means <BR>&nbsp;&nbsp; 0 - Full calculation of diagonal, off ' + $
    'diagonal terms <BR>&nbsp;&nbsp;1 - Off diagonal terms are an ' + $
    'approximation - for most submatrices this will mean an ' + $
    'average of all segments of the right kind (f,r,coinc.) ' + $
    'instead of detector-specific results <BR>&nbsp;&nbsp;2 - ' + $
    'Diagonal terms only <BR>&nbsp;&nbsp;3 - Diagonal terms only, ' + $
    'and all = 1 (or zero if it is a normally off-diagonal ' + $
    'matrix)'

k = k + 1
d[k].name = 'atten_state'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Attenuator setting'
d[k].more_doc = ''

k = k + 1
d[k].name = 'earth_position'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '180.'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Earth Position'
d[k].more_doc = 'Earth position. If scalar, it is a spin-integrated zenith ' + $
    'angle. If an array of two values, the second is the azimuth ' + $
    'angle (from spacecraft +x). <P>Default is azimuth averaged, ' + $
    'zenith=180 (Earth behind).'

k = k + 1
d[k].name = 'offax_position'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'degrees'
d[k].default = '.25'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Angle between source and imaging axis'
d[k].more_doc = 'If scalar > 0, offax_position is a single radius angle in ' + $
    'degrees and azimuth average. If an array of two values, then ' + $
    'zenith and azimuth angles. <P>This is used in the detector ' + $
    'response calculations.<P>Since the axis wobbles with time, ' + $
    'offax_position in an average angle.'

k = k + 1
d[k].name = 'compute_offax_position'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, compute offax_position.'
d[k].more_doc = 'The offax_position is needed for a small correction to the ' + $
    'SRM.<P>An aspect solution is needed for computing ' + $
    'offax_position. If you are creating an SRM for a period ' + $
    'during which there is no aspect solution, you should turn ' + $
    'off compute_offax_position so that the SRM creation doesn''t ' + $
    'abort. You can set offax_position manually (by retrieving it ' + $
    'for a nearby time that does have an aspect solution and ' + $
    'setting it in your object) if you think it''s an important ' + $
    'factor.'

k = k + 1
d[k].name = 'srm_xyoffset'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = '[0,0]'
d[k].range = '0 - 1000'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Flare position from Sun center'
d[k].more_doc = ''

k = k + 1
d[k].name = 'integrator'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Assumed power law index over whole spectrum'
d[k].more_doc = 'Assumed power law index over whole spectrum for purposes of ' + $
    'splitting channels into "subchannels", calculating the ' + $
    'response for each, weighting by the assumed powerlaw, and ' + $
    'summing back together.'

k = k + 1
d[k].name = 'srm_aspect_time'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Time used to get aspect for offax_position in SRM ' + $
    'calculation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_gain_time'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Time used to get resolution for SRM calculation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_use_saved'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = 'bytarr(10)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'For each submatrix to be read from a saved file, set to the ' + $
    'filename'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_make_saved'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = 'bytarr(10)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'For each submatrix to be written to a saved file, set to the ' + $
    'filename'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_lambda'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '0.'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Characterizes radiation damage pulse shape tailing'
d[k].more_doc = ''

k = k + 1
d[k].name = 'print_subrms'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, print the submatrices to the screen. Beware: may be ' + $
    'large.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'verbose'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 -1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, print periodic status updates'
d[k].more_doc = ''

k = k + 1
d[k].name = 'display_result'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, plot the final matrices'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pha_on_row'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, units of srm are [count energy, photon energy, ' + $
    'matrix number]. If not set, count and photon indices ' + $
    'reversed.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_input_units'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'photons cm**(-2)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_units'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'counts cm**(-2) kev**(-1)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'srt_filename'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'srt_*.dat'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Subcollimator response table'
d[k].more_doc = ''

k = k + 1
d[k].name = 'apt_filename'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'apt_*.txt'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'insert_kedge'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, add K-edge for Ge, Mo, Tu to ph_edges'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_direct'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, use new (9/2009) high-energy matrix to generate ' + $
    'rear-segment DRM'
d[k].more_doc = ''

; Control parameters for hsi_visibility
k = k + 1
d[k].name = 'vis_input_fits'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Input visibility FITS file name'
d[k].more_doc = 'Name of a FITS File containing a visibility bag for a single ' + $
    'or multiple times and energies.<P>Using a visibility file as ' + $
    'input saves time because we don''t need to read the raw ' + $
    'telemetry files again.<P>If the file is not in the current ' + $
    'directory, include the file path in this file name. <P>When ' + $
    'VIS_INPUT_FITS is set to a file, all of the control ' + $
    'parameters that visibilities depend on are set as they were ' + $
    'when the file was made. You are not allowed to change most ' + $
    'of the control parameter. Info parameters are also filled in ' + $
    'from the file.<P>To resume using the raw files as input, set ' + $
    'VIS_INPUT_FITS to a null string. '

k = k + 1
d[k].name = 'vis_chi2lim'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '1.0e+9'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Visibilities with chi2 fit values > vis_chi2lim are ' + $
    'rejected.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_edit'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, visibilities are edited to remove those with ' + $
    'inadequate phase coverage or bad fits'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_conjugate'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, visibility conjugates are combined'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_normalize'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, visibilities are normalized'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_max_corr'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.25'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Maximum allowable fractional difference of one detector''s ' + $
    'total flux from the average'' '
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_photon2el'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '2'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Ratio of electron to photon energy bin edges and number'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_type'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'photon'
d[k].range = '"photon", "regularized electron", "regularized photon"'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Type of visibilities'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_out_filename'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set to a string, a FITS file will be written containing ' + $
    'the visibility bag'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_plotfit'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, plot fitted visibility vs phase'
d[k].more_doc = ''

; Control parameters for hsi_vismod_pattern
; Control parameters for hsi_bproj
; Control parameters for hsi_bproj_strategy
k = k + 1
d[k].name = 'flatfield'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, enable flatfielding in BPROJ'
d[k].more_doc = 'If set, removes the DC component of the back projection and ' + $
    'corrects for the pixel-to-pixel modulation variance'

k = k + 1
d[k].name = 'vrate'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_rate'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use rate-enabled back projection'
d[k].more_doc = 'Set this flag to turn on rate-enabled back projection. This ' + $
    'is particularly critical to set when the deadtimes become ' + $
    'greater than 1/2. For dirty maps made with USE_RATE set to 0 ' + $
    'the true source positions may have the lowest value in the ' + $
    'field. Setting USE_RATE to 1 corrects that and the source ' + $
    'positions will again have the highest values and appear ' + $
    'brightest in the dirty map images. For the CLEAN algorithm ' + $
    'to work the source positions must have the highest values.'

k = k + 1
d[k].name = 'weight'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'natural_weighting'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, all collimators have equal weight in forming BPROJ ' + $
    'map'
d[k].more_doc = '>spatial_frequency_weight'

k = k + 1
d[k].name = 'uniform_weighting'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, each collimator''s weight in forming BPROJ map is ' + $
    '1/FWHM'
d[k].more_doc = '>spatial_frequency_weight'

k = k + 1
d[k].name = 'spatial_frequency_weight'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Detector weighting for BPROJ map. Not settable by user'
d[k].more_doc = 'The SPATIAL_FREQUENCY_WEIGHT parameter specifies the ' + $
    'weighting applied to each collimator when combining them to ' + $
    'form a back projection image. SPATIAL_FREQUENCY_WEIGHT is ' + $
    'not set directly by the user, but is controlled through the ' + $
    'following three parameters: <P>UNIFORM_WEIGHTING - If set, ' + $
    'then weight each collimator by the inverse of its FWHM ' + $
    '<P>NATURAL_WEIGHTING - If set, all collimators have equal ' + $
    'weight <P>TAPER - (scalar) If nonzero, a factor to apply to ' + $
    'SPATIAL_FREQUENCY_WEIGHT determined as follows for each ' + $
    'detector: <br>&nbsp;&nbsp; exp ((-.89 * taper/FWHM)^2 < ' + $
    '50.)<P>Note 1: UNIFORM_WEIGHTING preferentially weights the ' + $
    'noisy high-frequency components. <P>Note 2: ' + $
    'NATURAL_WEIGHTING is better for extended sources while ' + $
    'UNIFORM_WEIGHTING provides greater sensitivity for compact ' + $
    'sources. Taper can be used to smooth out the image to avoid ' + $
    'over-resolution.'

k = k + 1
d[k].name = 'taper'
d[k].class = 'hsi_bproj_strategy'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = '0. - 10.'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Tapering factor for weighting grids in forming BPROJ map'
d[k].more_doc = '>spatial_frequency_weight'

; Control parameters for hsi_forwardfit
k = k + 1
d[k].name = 'ff_n_par'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '4'
d[k].range = '4,6,7'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Number of free parameters per gaussian component'
d[k].more_doc = 'Number of free parameters per gaussian component: 4 for ' + $
    'gaussian, 6 for elliptical, 7 for curved elliptical'

k = k + 1
d[k].name = 'ff_n_gaussians'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '>1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Number of gaussian components'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_nitmax'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '10'
d[k].range = '>3'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Max number of iterations'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_ftol'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0001'
d[k].range = '>0'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Tolerance for convergence'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_min_sigma'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '1.02'
d[k].range = '>1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Minimum sigma in modulation for a detector to be included'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_minsep'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'arcsec'
d[k].default = '0.0'
d[k].range = '>0'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Minimum separation required between centroids of adjacent ' + $
    'gaussians'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_fixed_pixel'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, disable automated pixel adjustment'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_pixelized'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Not implemented yet'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_testplot'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show intermediate plots'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_savefile'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, save intermediate plots (PS file) and parameters ' + $
    '(IDL save file)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_savename'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = 'hsi_forwardfit.sav'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Name of file to write if ff_savefile is set'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_progress_bar'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show progress bar with cancel option'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_init_set'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Choose method for initializing parameters'
d[k].more_doc = 'ff_init_set is used to choose the method for initializing ' + $
    'parameters<P> (for example - the position of gaussian ' + $
    'sources)<P>0 - determined from maxima in backprojection ' + $
    'maps<P>1 - pre-estimated from previous run saved in file ' + $
    'ff_last.sav'

k = k + 1
d[k].name = 'ff_background_map'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, forward-fit includes background of unmodulated ' + $
    'sources'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_background_det'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, forward-fit includes detector background in time ' + $
    'profiles'
d[k].more_doc = ''

; Control parameters for hsi_image_file
k = k + 1
d[k].name = 'use_all_info_params'
d[k].class = 'hsi_image_file'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, read all info parameters from image FITS file.'
d[k].more_doc = 'For speed, only a few summary info parameters are read from ' + $
    'an image FITS file. These summary info parameters are in a ' + $
    'separate single extension in the FITS file and include ' + $
    'times, energy bands, and a few other items for every image. ' + $
    'The rest of the info parameters for each image are in ' + $
    'separate extensions, one for each image. If ' + $
    'USE_ALL_INFO_PARAMS is set, then all of these separate ' + $
    'extensions are read, and the info parameters in the object ' + $
    'are filled in for each image. This can take a while for ' + $
    'large image cube files.'

k = k + 1
d[k].name = 'im_input_fits'
d[k].class = 'hsi_image_file'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Input image FITS file name'
d[k].more_doc = 'Name of a single- or multi-image FITS File.<P>If the file is ' + $
    'not in the current directory, include the file path in this ' + $
    'file name. <P>When IM_INPUT_FITS is set to a file, all of ' + $
    'the control parameters are set as they were when the file ' + $
    'was made. You are not allowed to change any control ' + $
    'parameter. Info parameters are filled in from the file ' + $
    '(either all of the info parameters, or just a few depending ' + $
    'on the setting of USE_ALL_INFO_PARAMS).<P>To resume using ' + $
    'the raw files as input, set IM_INPUT_FITS to a blank, or set ' + $
    'the IM_ALLOW_REPROCESS parameter.'

; Control parameters for hsi_image_raw
k = k + 1
d[k].name = 'im_time_interval'
d[k].class = 'hsi_image_raw'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Time Interval(s) for images'
d[k].more_doc = '>im_time_interval'

k = k + 1
d[k].name = 'im_time_bin'
d[k].class = 'hsi_image_raw'
d[k].main_object = 'Image'
d[k].type = 'double'
d[k].units = 'sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Width of bins to divide im_time_intervals into'
d[k].more_doc = '>im_time_interval'

k = k + 1
d[k].name = 'im_time_ref'
d[k].class = 'hsi_image_raw'
d[k].main_object = 'Image'
d[k].type = 'double'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

; Control parameters for hsi_image_single
k = k + 1
d[k].name = 'flare_id_nr'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'image_algorithm'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'Bproj'
d[k].range = 'BProj, Clean, Pixon, Forward Fit, mem_njit, vis_fwdfit, uv_smooth'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Algorithm to use in image reconstruction'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nvis_min'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '10'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Minimum number of good visibilities required to proceed with ' + $
    'image generation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'noquintic_interp'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, don''t use quintic interpolation when converting ' + $
    'image from annsec to xy coordinates'
d[k].more_doc = 'Non-visibility image algorithms generate the image first in ' + $
    'annular sector coordinates and then convert the image to xy ' + $
    'coordinates before returning it to the user. The IDL routine ' + $
    'TRIGRID is used for the intepolation to the new coordinate ' + $
    'system. Quintic polynomial interpolation (the default) ' + $
    'sometimes computes negative values for some pixels. Set ' + $
    'noquintic_interp to 1, to disable quintic interpolation and ' + $
    'use linear interpolation.'

k = k + 1
d[k].name = 'imaging_method'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'profile_window'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '-1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Window number to draw profiles in'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'profile_show_plot'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, enable plots of predicted and observed profiles ' + $
    'while making image'
d[k].more_doc = '<h3 align="center">Image modulation profiles</h3>We compare ' + $
    'the modulation profiles predicted by the image to the ' + $
    'observed modulation profiles to determine how good the image ' + $
    'is. There are two tools - one computes the goodness of fit ' + $
    'between the predicted and observed profiles, as well as the ' + $
    'fit coefficients. The other performs a Monte Carlo analysis ' + $
    'by varying the predicted profile, xp, by poisson statistics, ' + $
    'calculating the goodness of fit of the varied xp to xp, ' + $
    'doing this a set number of times, fitting a Gaussian to the ' + $
    'distribution, and returning the center of the Gaussian and ' + $
    'the FWHM.<P><br>The following control parameters are ' + $
    'available for these tests:<P>profile_show_plot - If set, ' + $
    'show plots of predicted and observed profiles while making ' + $
    'image. Default is 0.<P>profile_plot_rate - Units for profile ' + $
    'plots. 0 means plot counts/bin; 1 means plot counts/sec. ' + $
    'Default is 1<P>profile_plot_resid - If set, plot profile ' + $
    'residuals. Default=0<P>profile_window - Internal variable to ' + $
    'keep track of window to plot profiles in<P>mc_ntrials - ' + $
    'Number of trials to run in the Monte Carlo ' + $
    'analysis<P>mc_show_plot - If set, show plots of Monte Carlo ' + $
    'distribution of C-statistic while making image. Note - this ' + $
    'only has an effect when profile_show_plot is 1, or when ' + $
    'profile plots are requested using the hsi_image_profile_plot ' + $
    'routine after an image was made.<P>To display the profile ' + $
    'and Monte Carlo plots after an image is made, use the ' + $
    'hsi_image_profile_plot routine with the appropriate ' + $
    'keywords.<P><br>Info parameters resulting from profile ' + $
    'tests:<br>(Note: Replace x by the algorithm prefix - clean, ' + $
    'sato, vis, pixon, ff, vf, nj, or uv)<P>x_profile_tot_cstat - ' + $
    'Overall C-statistic from the comparison of expected to ' + $
    'observed modulation profiles for all selected ' + $
    'detectors.<P>x_profile_cstat - C-statistic from the ' + $
    'comparison of each separate detector''s expected and ' + $
    'observed modulation profile.<P>x_profile_coeff - Constant, ' + $
    'slope, and correlation coefficient between the expected ' + $
    'profile and observed profile for each detector. Apply the ' + $
    'constant and slope to the expected profile to match the ' + $
    'observed profile. Note that the constant is in units of ' + $
    'counts / bin.The correlation coefficient gives an overall ' + $
    'estimate of the correlation between the expected and ' + $
    'observed profiles.<P>x_profile_mc_tot_cstat - Most probable ' + $
    'C-statistic for all selected detectors, from Monte Carlo ' + $
    'analysis<P>x_profile_mc_cstat - Most probable C-statistic ' + $
    'for each separate selected detector, from Monte Carlo ' + $
    'analysis<P>x_profile_mc_tot_fwhm - FWHM of Monte Carlo ' + $
    'distribution for all selected detectors<P>x_profile_mc_fwhm ' + $
    '- FWHM of Monte Carlo distribution fo each separate selected ' + $
    'detector<P><br>Limitations based on availability of ' + $
    'calibrated eventlist (and therefore the profiles):<P>1. ' + $
    'Image cubes for visibility-based algorithms can not make ' + $
    'this comparison.<P>2. For image cubes for non-vis ' + $
    'algorithms, profiles can only be plotted while generating ' + $
    'the images,not afterwards.'

k = k + 1
d[k].name = 'profile_plot_rate'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, plot count/sec in profile plot'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'profile_plot_resid'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, plot profile residuals instead of profiles ' + $
    'themselves'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'mc_ntrials'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Number of Monte Carlo trials to determine probability of ' + $
    'C-statistic'
d[k].more_doc = ''

k = k + 1
d[k].name = 'mc_show_plot'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show Monte Carlo distribution of C-statistic for ' + $
    'separate and combined detectors'
d[k].more_doc = ''

; Control parameters for hsi_image_strategy
k = k + 1
d[k].name = 'im_time_bin'
d[k].class = 'hsi_image_strategy'
d[k].main_object = 'Image'
d[k].type = 'double'
d[k].units = 'sec'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Width of bins to divide im_time_intervals into'
d[k].more_doc = '>im_time_interval'

k = k + 1
d[k].name = 'im_time_ref'
d[k].class = 'hsi_image_strategy'
d[k].main_object = 'Image'
d[k].type = 'double'
d[k].units = ''
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'full_info'
d[k].class = 'hsi_image_strategy'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, write all info params in FITS file'
d[k].more_doc = 'To reduce the size of image FITS files, if this flag is set ' + $
    'to 0 (the default), then ''CLEAN'' info arrays with more ' + $
    'than 40 elements are not written in the image FITS file.If ' + $
    'full_info is set to 1, then all info parameters are written ' + $
    'in the FITS file.'

; Control parameters for hsi_pixon
k = k + 1
d[k].name = 'pixon_resolution'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'pixel'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Minimum resolution to search'
d[k].more_doc = 'Minimum value of pixon_sizes in pixels. Any structure at ' + $
    'scales less than this pixon size are not allowed. Set to ' + $
    'larger value to smooth out the final pixon map to avoid ' + $
    'over-resolution. <P>For example, use 1 arcsec pixels and set ' + $
    'pixon_resolution to 3.0 or 4.0 to smooth out the image to 3 ' + $
    'or 4 arcsecs.<P>This is rather restrictive. A more subtle ' + $
    'and possibly better way to avoid over-resolution is to use ' + $
    'the pixon_sensitivity parameter.'

k = k + 1
d[k].name = 'pixon_snr'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Bit map for controlling pixon options.'
d[k].more_doc = 'If set to 1, weight the residuals by the signal-to-noise ' + $
    'ration. <P>You can set 3 other parameters by setting ' + $
    'specific bits in PIXON_SNR, however it is more ' + $
    'straightforward to set those parameters directly. See ' + $
    'PIXON_FULL_PM_CALC, PIXON_BACKGROUND_MODEL, and ' + $
    'PIXON_VARIABLE_METRIC. <P>If you want to set all 4 options ' + $
    'through this one parameter, PIXON_SNR, then these are the ' + $
    'bits you need to set: <P> Bit 0 - (pixon_snr OR 1, or just ' + $
    'set pixon_snr=1) Weight the residuals by the signal-to-noise ' + $
    'ratio.<P>Bit 1 - (pixon_snr OR 2) Do a more complete but ' + $
    'slower calculation of the pixon map. Same as ' + $
    'PIXON_FULL_PM_CALC.<P>Bit 2 - (pixon_snr OR 4) Attempt a ' + $
    'background estimation from the residuals. Same as ' + $
    'PIXON_BACKGROUND_MODEL. <P>Bit 3 - (pixon_snr OR 8) Use the ' + $
    'fast algorithm. Same as PIXON_VARIABLE_METRIC.'

k = k + 1
d[k].name = 'pixon_background_model'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, attempt a background estimation from the residuals.'
d[k].more_doc = 'If set, attempt a background estimation from the ' + $
    'residuals.<P>Setting bit 2 of pixon_snr (pixon_snr OR 4) has ' + $
    'the same effect as setting this keyword.'

k = k + 1
d[k].name = 'pixon_variable_metric'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, use the fast algorithm.  Requires lots of memory.'
d[k].more_doc = 'If set, use the fast algorithm. It is 2 to 4 times faster, ' + $
    'but requires a lot of memory. For a 64x64 image, you need ' + $
    '300MB or more of memory. For a 128x128 image, you will ' + $
    'probably need 3 to 5 GB. The memory required scales as the ' + $
    'fourth power of the linear image dimension.<P>Setting bit 3 ' + $
    'of pixon_snr (pixon_snr OR 8) has the same effect as setting ' + $
    'this keyword.'

k = k + 1
d[k].name = 'pixon_full_pm_calc'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, do a more complete but slower calculation of the ' + $
    'pixon map.'
d[k].more_doc = 'If set, do a more complete but slower calculation of the ' + $
    'pixon map.<P>Setting bit 1 of pixon_snr (pixon_snr OR 2) has ' + $
    'the same effect as setting this keyword.'

k = k + 1
d[k].name = 'pixon_guess'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = 'Back projection image'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Image used as initial guess for pixon reconstruction'
d[k].more_doc = 'Image used as initial guess for pixon reconstruction. The ' + $
    'default guess is the back projection image. <P>If guess is ' + $
    'not nx by ny or if all elements of guess are LE 0, then the ' + $
    'guess is not used.'

k = k + 1
d[k].name = 'pixon_sizes'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'pixel'
d[k].default = 'Powers of sqrt(2.0)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Array of pixon resolutions to use.'
d[k].more_doc = 'A list of pixon sizes (resolutions) to use in units of ' + $
    'pixels. The smallest value is set by pixon_resolution. For ' + $
    'slower, but more robust, solution, set pixon_sizes = ' + $
    'findgen(32) +1.'

k = k + 1
d[k].name = 'pixon_sensitivity'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Sensitivity used in creating pixon map. 0. is most ' + $
    'sensitive, > 3. is very insensitive.'
d[k].more_doc = 'The sensitivity in creating the pixon map. 0.0 is most ' + $
    'sensitive, >3.0 is very insensitive. The default is 0.0, ' + $
    'i.e. the default is to give the most structure possible in ' + $
    'the image. If you get images with structure that you think ' + $
    'are spurious, try increasing this parameter to 0.2 or 0.5 to ' + $
    'eliminate the over-resolution.<P>The default was arrived at ' + $
    'after much testing and simulation. Don''t mess with it ' + $
    'unless you know what you are doing. If sensitivity is set ' + $
    'too small, you may get spurious sources; too large, and you ' + $
    'may miss real sources.<P>Note: The pixon_resolution ' + $
    'parameter can also be used to control the spatial scale of ' + $
    'structure the pixon code will look for.'

k = k + 1
d[k].name = 'pixon_noplot'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, do not show intermediate plots'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixon_smpattwritedir'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Directory to save new smoothed patterns in'
d[k].more_doc = 'String variable specifying directory name. Default is empty ' + $
    'string. If new smoothed patterns are calculated and ' + $
    'pixon_smpattwritedir is set to a valid directory, then the ' + $
    'new smoothed patterns will be saved in that directory.'

k = k + 1
d[k].name = 'pixon_progress_bar'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show progress bar with cancel option'
d[k].more_doc = ''

; Control parameters for hsi_psf
k = k + 1
d[k].name = 'xy_pixel'
d[k].class = 'hsi_psf'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'xy position of the pixel for which psf will be processed'
d[k].more_doc = ''

k = k + 1
d[k].name = 'psf_no_sum'
d[k].class = 'hsi_psf'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, get separate psf for each detector'
d[k].more_doc = 'If set, the psfs are returned by detectors. If not set, the ' + $
    'psfs are summed over the chosen detectors. This is not a ' + $
    'true control parameter, it does not add to the general list ' + $
    'and must be set specifically for this class, with for ' + $
    'instance<P>ooo->get( class_name = ''hsi_psf'' )ooo->set, ' + $
    '/psf_no_sum'

; Control parameters for hsi_obs_summary
k = k + 1
d[k].name = 'filename'
d[k].class = 'hsi_obs_summary'
d[k].main_object = 'Obs'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'File to retrieve data from.'
d[k].more_doc = '>obs_time_interval'

k = k + 1
d[k].name = 'obs_time_interval'
d[k].class = 'hsi_obs_summary'
d[k].main_object = 'Obs'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Absolute time interval to retrieve data for.'
d[k].more_doc = '>obs_time_interval'

k = k + 1
d[k].name = 'data_dir'
d[k].class = 'hsi_obs_summary'
d[k].main_object = 'Obs'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Use with filename param. Directory obs summ file is in.'
d[k].more_doc = ''

; Control parameters for hsi_qlook
k = k + 1
d[k].name = 'class_name'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Class name to retrieve data for.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'version'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'version number'
d[k].more_doc = ''

k = k + 1
d[k].name = 'id_string'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'An identifying string'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vers_info'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Version number for info'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vers_data'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Version number for data'
d[k].more_doc = ''

k = k + 1
d[k].name = 'obs_time_interval'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Absolute time interval to retrieve data for.'
d[k].more_doc = '>obs_time_interval'

k = k + 1
d[k].name = 'filename'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'File (including path) to retrieve data from. Ignored if ' + $
    'obs_time_interval is set.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'energy_band'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'float(2)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'time_range'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Absolute time range of data to retrieve. Used only if ' + $
    'filename is set.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nodata'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'read_from_archive'
d[k].class = 'hsi_qlook'
d[k].main_object = 'Obs'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

; Control parameters for hsi_simulation
k = k + 1
d[k].name = 'sim_time_unit'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'binary microsec (2^-20 sec)'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Time unit of the time tag in the eventlist.'
d[k].more_doc = 'sim_time_unit is the time unit, in binary microseconds of ' + $
    'the .time tag in the eventlist structure. The default value ' + $
    'is 1. It is a good idea to set both sim_time_unit and ' + $
    'time_unit to the same value for all simulations. Note that ' + $
    'for simulations longer than 2048.0 seconds, a larger value ' + $
    'of sim_time_unit is needed. If the time range passed in is ' + $
    'lnger that 2048.0 seconds, then the time_unit will be ' + $
    'increased to 16. '

k = k + 1
d[k].name = 'sim_ut_ref'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'ANYTIM'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Reference time for simulation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_time_range'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'sec'
d[k].default = '[0,4]'
d[k].range = '--'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Time range of simulation, relative to sim_ut_ref'
d[k].more_doc = 'time range, in seconds, relative to the reference time ' + $
    'sim_ut_ref for the simulation. Note that sim_time_range is ' + $
    'not required if a time array or time profile is passed in. '

k = k + 1
d[k].name = 'sim_use_spectrum'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, use an input spectrum or model spectrum to create ' + $
    'simulations.'
d[k].more_doc = 'If set, use an input spectrum or model spectrum to create ' + $
    'simulations. If set, must also set sim_spec_model and ' + $
    'sim_spec_pars, or sim_photon_flux and sim_photon_energy ' + $
    'keywords. '

k = k + 1
d[k].name = 'sim_photons_per_coll'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'photons/sec/coll'
d[k].default = '10000.'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Set overall flux level of simulated data'
d[k].more_doc = 'sim_photons_per_coll, a floating point number, is used to ' + $
    'control the overall flux level of the simulated data. The ' + $
    'units are in photons per second per collimator incident on ' + $
    'the spacecraft, with a default value of 10000. This is dealt ' + $
    'with differently in the new software; in previous versions, ' + $
    'sim_photons_per_coll referred to photon counts, after the ' + $
    'energy response matrix of the given detector was accounted ' + $
    'for, now this number refers to the total photon flux ' + $
    'incident before taking the detector response into account. ' + $
    'This means that there are fewer counts registered in the new ' + $
    'version for the same value of sim_photons_per_coll. This is ' + $
    'a scalar. This parameter is ignored if sim_use_spectrum is ' + $
    'set. Note that this is the total value of photons per ' + $
    'detector, if there are multple sources in the image then the ' + $
    'counts are divided up between the sources, ' + $
    'photons_per_source=sim_photons_per_coll*model.amplitude/total(model.amplitude) ' + $
    '<P>If no control parameters are set, then the simulation ' + $
    'will run using sim_photons_per_coll = 1.0e4, as always. '

k = k + 1
d[k].name = 'sim_photon_flux'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'photons/cm^2/sec/keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Photon flux array, (nenergies, ntimes, nsources)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_photon_energy'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'photon energies for the input photon flux'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_photon_times'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Time array (rel to sim_ut_ref) for the input photon flux'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_photon_tprofile'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Time profile to use when shape of spectrum doesn''t vary ' + $
    'with time'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_spec_pars'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Spectral parameters for the given model'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_spec_model'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'f_bpow'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Spectral model'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_interp_tprofile'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, interpolate time profile to smaller bins'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_interp_dt'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'sec'
d[k].default = '1/128'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Time interval for interpolating option (sim_interp_tprofile)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_just_background'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, only background counts are produced'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_background'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Factor to multiply background spectrum given by ' + $
    'HESSI_BACKGROUND'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_time_unit'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'binary microsec (2^-20 sec)'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_time_unit, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_ut_ref'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_ut_ref, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_time_range'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_time_range, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_use_bkgd_spectrum'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_use_spectrum, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_photons_per_coll'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_photons_per_coll, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_photon_flux'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_photon_flux, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_photon_energy'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_photon_energy, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_photon_times'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_photon_times, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_tprofile'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_photon_tprofile, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_spec_model'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_spec_model, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_spec_pars'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_spec_pars, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_interp_tprofile'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_interp_tprofile, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_bkgd_interp_dt'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Same meaning as sim_interp_dt, for background'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_a2d_index_mask'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte(27)'
d[k].units = 'N/A'
d[k].default = 'All 1s'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Detector/segment selection'
d[k].more_doc = 'sim_a2d_index_mask is a 27 element byte array that controls ' + $
    'which detector / segment data is used in the simulation. Set ' + $
    'to 1 to use the detector / segment. The first 9 elements ' + $
    'refer to the front segments of detectors 1-9, the next 9 ' + $
    'elements of the array refer to the low energy portion of the ' + $
    'rear segments of detectors 1-9, and the last 9 elements ' + $
    'refer to the high energy portion of the rear segments for ' + $
    'detectors 1-9. '

k = k + 1
d[k].name = 'sim_pixel_size'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'arcsec'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Pixel size when using a model image'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_xyoffset'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(2)'
d[k].units = 'N/A'
d[k].default = '[600,200]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Center of the simulation box'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_model'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Model for source for simulation'
d[k].more_doc = 'sim_model can be used to pass in a structure (labelled ' + $
    'gaussian_source_str) which specifies a 2d Gaussian source. ' + $
    'The Gaussian is specified by its amplitude, x and y standard ' + $
    'deviation (in arcseconds), its position relative to the ' + $
    'center of the image (in arcseconds), and its tilt angle. The ' + $
    'amplitude refers to the total photon flux from the source, ' + $
    'not the peak value. The maximum value of amplitude is 1, if ' + $
    'you want two sources of equal strength, set both amplitudes ' + $
    'to 1.0. See the demo program <A ' + $
    'href="http://sprg.ssl.berkeley.edu/~jimm/hessi/hsi_sim_flare.html#sim_2gaussians.pro">sim_2gaussians.pro ' + $
    '</A>in the <A ' + $
    'href="http://sprg.ssl.berkeley.edu/~jimm/hessi/hsi_sim_flare.html">hsi_sim_flare ' + $
    '</A>document. The model also may be of the type ' + $
    '{curved_gaussian_source_str}, which has a curvature value in ' + $
    'addition to the above parameters. The model can also be ' + $
    'given as an array with zeroes everywhere except at the ' + $
    'location of point sources. The non zero values should be ' + $
    'specified in normalized units, with a maximum of 1.0. The ' + $
    'default model is a two-component gaussian source with equal ' + $
    'amplitudes, and xy standard deviations of 0.001 arcsec, to ' + $
    'simulate point sources. '

k = k + 1
d[k].name = 'sim_energy_band'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = '[6.,100.]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Energy band for simulation'
d[k].more_doc = 'sim_energy_band is the energy band for the simulation, the ' + $
    'default is [6.0, 100.0] keV. If sim_energy_band has n+1 ' + $
    'elements, then the simulation routines are run n times, once ' + $
    'for each energy band. This is the way to simulate large ' + $
    'energy ranges;e.g., if you need to do the range from 3 to ' + $
    '1000 keV, you can pass in sim_energy_band=[3, 20, 50, 100, ' + $
    '200, 400, 600, 800, 1000] (or something similar) so that the ' + $
    'variation of the grid responses with energy can be accounted ' + $
    'for properly. '

k = k + 1
d[k].name = 'sim_gain_generation'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Code for gain-generation.  0=old, 1=new, 2=on-orbit?'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_gain_time_wanted'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_diagonal_srm'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = 'o - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, use diagonal detector response matrix for the ' + $
    'simulation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_max_size'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '128'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Default 1d max dimension'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_saszero'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, aspect solution is constant with pointing=[0,0] and ' + $
    'roll_period=4'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_srt_filename'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Grid response data file to use.  Default is latest version.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_seed'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Seed for random number routines. Set it to reproduce ' + $
    'results.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_nbuff'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '5.e5'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max number of photons processed at one time. Set higher if ' + $
    'you have more memory.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_ssr_state'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Measure of how full SSR memory is.  0=empty, 8=full'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_atten_state'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0,1,2,3'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Attenuator state. 0=no attenuators, 1=Thin in, thick out, ' + $
    '2=thick in, thin out, 3=both in.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_quiet'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, supress diagnostic print statements in ' + $
    'hsi_model_to_score'
d[k].more_doc = ''

; Control parameters for hsi_artifact_mask
k = k + 1
d[k].name = 'pad'
d[k].class = 'hsi_artifact_mask'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = ''
d[k].default = '2'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Pads in channel space so that artifact mask works OK at ' + $
    'other times'
d[k].more_doc = ''

k = k + 1
d[k].name = 'time'
d[k].class = 'hsi_artifact_mask'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = '23-jul-2002'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Time for which artifact mask will be created'
d[k].more_doc = ''

k = k + 1
d[k].name = 'edges'
d[k].class = 'hsi_artifact_mask'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = 'Binning code 11'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Count energy edges for which artifact mask will be created ' + $
    '(2xn or n)'
d[k].more_doc = ''

; Control parameters for hsi_pileup
k = k + 1
d[k].name = 'pileup_modamp'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Correction factor for pileup'
d[k].more_doc = 'Average modulation amplitude during the accumulation. When ' + $
    'high, the pileup correction is more severe than would be ' + $
    'expected for thataverage livetime.'

k = k + 1
d[k].name = 'pileup_threshold'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.95'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Livetime fraction threshold for pileup correction'
d[k].more_doc = 'Time intervals with livetime fraction greater than the ' + $
    'pileup_threshold will have pileup correction computed (if ' + $
    'pileup_correct is set). '

k = k + 1
d[k].name = 'pileup_correct'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, make pileup correction'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pileup_tweak'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.6'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Controls intensity of pileup correction'
d[k].more_doc = 'Controls intensity of pileup correction, .9 means less ' + $
    'correction, 1.1 means more correction, but can be any ' + $
    'number.'

k = k + 1
d[k].name = 'pileup_smooth'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, smooth the spectrum rebinned to fine edgesh'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pileup_seg_index_mask'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'byte(18)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Reflects value of seg_index_mask'
d[k].more_doc = ''

; Control parameters for hsi_xyoffset
k = k + 1
d[k].name = 'xyoffset'
d[k].class = 'hsi_xyoffset'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = '[600.,200.]'
d[k].range = '0 - 1000'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Offset of map center from Sun center'
d[k].more_doc = '>use_flare_xyoffset'

k = k + 1
d[k].name = 'flare_xyoffset'
d[k].class = 'hsi_xyoffset'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = '[0.,0.]'
d[k].range = '0 - 1000'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_flare_xyoffset'
d[k].class = 'hsi_xyoffset'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use flare position from catalog for xyoffset.'
d[k].more_doc = 'If use_flare_xyoffset is set, then xyoffset is automatically ' + $
    'set to the position in the flare catalog of the flare ' + $
    'closest in time to your requested time. <P>If you set ' + $
    'xyoffset manually, then use_flare_xyoffset is turned off. ' + $
    'You must reset use_flare_xyoffset to 1 to return to ' + $
    'automatically retrieving the flare position.'

; Control parameters for hsi_annsec_pattern
k = k + 1
d[k].name = 'factor_by'
d[k].class = 'hsi_annsec_pattern'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'modpat_skip'
d[k].class = 'hsi_annsec_pattern'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Controls the interpolation of modulation patterns'
d[k].more_doc = 'Used to control how fine the interpolation of the modulation ' + $
    'patterns is when constructing the image. The default of 1 ' + $
    'means use every modulation pattern available (best ' + $
    'resolution). Increasing modpat_skip results in faster image ' + $
    'reconstruction with some loss of resolution in the image. ' + $
    'Setting modpat_skip as high as 12 usually results in an ' + $
    'acceptable image.'

k = k + 1
d[k].name = 'r0_offset'
d[k].class = 'hsi_annsec_pattern'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'arcsec'
d[k].default = '2560.'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Distance between annular sector axis and map center'
d[k].more_doc = 'R0_offset is the distance in arcseconds between the annular ' + $
    'sector axis (not nececessarily the telescope axis or ' + $
    'sun-center) and the map center. r0_offset must be greater ' + $
    'than the half-length of the FOV along the radial directions. ' + $
    'The default is image_dim[0] * pixel_size[0] * pixel_scale * ' + $
    '10.'

; Control parameters for hsi_calib_eventlist
k = k + 1
d[k].name = 'im_time_interval'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Time Interval(s) for images'
d[k].more_doc = 'IM_TIME_INTERVAL and IM_TIME_BIN work together to define ' + $
    'time bins for images.<P>IM_TIME_INTERVAL can be set to edges ' + $
    'of contiguous bins or to start/end pairs of bins. For ' + $
    'example: <P> o->set, im_time_interval=''20-feb-02 11:06:'' + ' + $
    '[''00'',''04'',''08'',''10'']<P> o->set, ' + $
    'im_time_interval=''20-feb-02 11:06:'' + <P> [ ' + $
    '[''00'',''04''], [''08'',''12''], [''16'',20''] ]<P><P>Or ' + $
    'set IM_TIME_INTERVAL to a start/end time, and IM_TIME_BIN to ' + $
    'the width in seconds to divide IM_TIME_INTERVAL into, ' + $
    'e.g.<P> o->set, im_time_interval=''20-feb-02 11:'' + ' + $
    '[''06:00'',''06:18'']<P> o->set, im_time_bin=4<P><P>While ' + $
    'making images, tb_index keeps track of which time interval ' + $
    'is currently being processed.<P><P>When retrieving images, ' + $
    'use t_idx to specify which time interval to retrieve the ' + $
    'image or parameters for, e.g. to get the image for the ' + $
    'fourth time interval:<P> o->set, t_idx=3<P> image = ' + $
    'o->getdata)<P>Both tb_index and t_idx start at 0.'

k = k + 1
d[k].name = 'tb_index'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Index for which of the time intervals to use'
d[k].more_doc = '>im_time_interval'

k = k + 1
d[k].name = 'cbe_filename'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Calib eventlist filename (input and/or output file)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_auto_time_bin'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                '
d[k].purpose = 'If set, time binning is automatically set based on flare ' + $
    'offset and cbe_digital_quality parameter'
d[k].more_doc = '<h3 align="center">Intermediate Time Binning for ' + $
    'Imaging</h3>The following parameters control intermediate ' + $
    'time binning for imaging:<P>use_auto_time_bin - If set, the ' + $
    'time binning is set automatically, based on flare offset, ' + $
    'cbe_digital_quality, and cbe_time_bin_floor. If set to 0, ' + $
    'then time binning is determined from time_bin_min, ' + $
    'time_bin_def, cbe_time_bin_floor, and cbe_power_of_two. ' + $
    '<P>time_bin_min - Scalar time bin size in binary ' + $
    'microseconds (2^-20 sec). This scalar is multiplied by ' + $
    'time_bin_def to set time bins for each detector. (Used only ' + $
    'if use_auto_time_bin=0) <P>time_bin_def - Array of 9 factors ' + $
    'to multiply time_bin_min by to define time bin size for each ' + $
    'detector. (Used only if use_auto_time_bin=0) ' + $
    '<P>cbe_powers_of_two - If set, forces multipliers of ' + $
    'time_bin_min to be powers of 2.<P>cbe_time_bin_floor - Array ' + $
    'of 9 values in binary microseconds giving minimum value ' + $
    'allowed for the time bin for each detector, whether ' + $
    'use_auto_time_bin is set, or the bins are computed from ' + $
    'time_bin_min * time_bin_def. <P>cbe_digital_quality - A ' + $
    'value between .1 and .99 that will be used to help determine ' + $
    'bin size for each detector if use_auto_time_bin is ' + $
    'set.<P><P>When making an image, the software accumulates ' + $
    'data for all 9 detectors. To save space, you can set ' + $
    'cbe_time_bin_floor to large values (2^16) for detectors you ' + $
    'are not interested in.'

k = k + 1
d[k].name = 'cbe_digital_quality'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.95'
d[k].range = '.1 - .99'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Used with flare offset to automatically determine size of ' + $
    'time bins '
d[k].more_doc = '>use_auto_time_bin'

k = k + 1
d[k].name = 'cbe_powers_of_two'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, forces multipliers on time_bin_min to be powers of 2'
d[k].more_doc = '>use_auto_time_bin'

k = k + 1
d[k].name = 'cbe_time_bin_floor'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = 'binary microsec (2^-20 sec)'
d[k].default = '[0,0,0,0,0,0,0,0,0]'
d[k].range = '0 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Minimum value allowed for time bin for each detector.'
d[k].more_doc = '>use_auto_time_bin'

k = k + 1
d[k].name = 'user_flux_var'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'User-provided smoothing profile'
d[k].more_doc = '>use_flux_var'

k = k + 1
d[k].name = 'use_flux_var'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, attempt to reconstruct flux variations unmarred by ' + $
    'grid modulation'
d[k].more_doc = 'If USE_FLUX_VAR is set, then hsi_calib_eventlist::DEMODULATE ' + $
    'attempts to construct a demodulated profile of the true flux ' + $
    'variation in the energy band, unmarred by grid modulation. ' + $
    '<P>Used with SMOOTHING_TIME and USER_FLUX_VAR ' + $
    'parameters.<P>The SMOOTHING_TIME is in seconds, and is used ' + $
    'with the IDL smooth function, as part of this algorithm. The ' + $
    'SMOOTHING_TIME should not be longer than the accumulation ' + $
    'interval. If you want to use your own smoothing profile, ' + $
    'determine it on regular bins, and load it into ' + $
    'USER_FLUX_VAR. To subsequently disable that, and usethe ' + $
    'algorithm''s value, set USER_FLUX_VAR to a scalar value, ' + $
    'preferably either 1 or 0.'

k = k + 1
d[k].name = 'smoothing_time'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'sec'
d[k].default = '.5'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Smoothing time for flux variation profile. Used only if ' + $
    'use_flux_var is set.'
d[k].more_doc = '>use_flux_var'

k = k + 1
d[k].name = 'srt_filename'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'srt_2*.dat'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Subcollimator response table'
d[k].more_doc = ''

k = k + 1
d[k].name = 'eb_index'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Selects the energy bin to use.'
d[k].more_doc = 'EB_INDEX is the index into the energy bin array (starting at ' + $
    '0) to use. Used in conjunction with IM_ENERGY_BINNING. ' + $
    'IM_ENERGY_BINNING defines energy binning for making images. ' + $
    '<P>ENERGY_BAND will be set to the energy edges actually ' + $
    'used.<P><P>The advantage of using IM_ENERGY_BINNING and ' + $
    'EB_INDEX instead of ENERGY_BAND is that it will be faster if ' + $
    'you are doing multiple energy bins for a time range, since ' + $
    'the binned eventlist will only be called once for a given ' + $
    'time range. '

k = k + 1
d[k].name = 'energy_band'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(2)'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Min and max energy value of the analysis interval'
d[k].more_doc = 'The ENERGY_BAND parameter in the hsi_calib_eventlist class ' + $
    'is used for imaging. <P>ENERGY_BAND is no longer used at all ' + $
    'for the lightcurve and spectrum objects. For lightcurve and ' + $
    'spectrum, the default value is [0.,0.] and should not be ' + $
    'changed. <P>For lightcurve, use LTC_ENERGY_BAND to set ' + $
    'energy bands.<P>For spectrum, use SP_ENERGY_BINNING to set ' + $
    'energy bands.<P><P>For image, use ENERGY_BAND to set the ' + $
    'min, max of the energies to use, but see IM_ENERGY_BINNING ' + $
    'and EB_INDEX for more options for setting energy bands for ' + $
    'images.'

k = k + 1
d[k].name = 'use_time_window'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(3)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_phz_stacker'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use phase stacker'
d[k].more_doc = '<h3 align="center">Phase Stacker</h3>Normal calibrated ' + $
    'eventlist used in imaging has a 1-D array that has phase, ' + $
    'roll, counts, etc information for each time bin. The phase ' + $
    'stacker converts that to a 2-D array of integrated counts in ' + $
    'roll angle bins vs phase bins. There is a separate 2-D array ' + $
    'for each detector. These are the parameters that control ' + $
    'phase stacker operations: <P>&nbsp;&nbsp; use_phase_stacker ' + $
    '=1 means enable the phase stacker <P>Controlling the number ' + $
    'of phase bins: <P>&nbsp;&nbsp; phz_n_phase_bins - Number of ' + $
    'phase bins. Default is 12.<P>Controlling the number of roll ' + $
    'bins: <P>&nbsp;&nbsp; phz_n_roll_bins_control - Array of 9 ' + $
    'values specifying the number of roll bins for each grid. ' + $
    'Default is 0. which means automatically calculate the number ' + $
    'of roll bins. This is the recommended mode (automatic ' + $
    'calculation of number of roll bins) <P>&nbsp;&nbsp; ' + $
    'phz_n_roll_bins_min - Array of 9 values specifying the ' + $
    'minimum number of roll bins for each grid<P>&nbsp;&nbsp; ' + $
    'phz_n_roll_bins_max - Array of 9 values specifying the ' + $
    'maximum number of roll bins for each grid<P>&nbsp;&nbsp; ' + $
    'phz_radius - Radius in arcsec of the extent of the source. ' + $
    'This is used to calculate the number of roll bins if ' + $
    'phz_n_roll_bins_control is 0. <P>&nbsp;&nbsp; ' + $
    'phz_report_roll_bins = 1 means print the number of roll bins ' + $
    'used in IDL output log. <P>&nbsp;&nbsp; phz_n_roll_bins_info ' + $
    '- Info parameter containing number of roll bins used for ' + $
    'each detector.'

k = k + 1
d[k].name = 'cb_coef'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used. Coefficients that may be used down the road'
d[k].more_doc = ''

k = k + 1
d[k].name = 'user_hook'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set turn on process in user_hook'
d[k].more_doc = ''

k = k + 1
d[k].name = 'imaging_power_law'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '4.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Power-law index used in hessi_grm'
d[k].more_doc = 'Power-law index passed to hessi_grm used to compute ' + $
    'modulation and transmission of x-rays through the grid based ' + $
    'on the material properties weighted over the photon ' + $
    'distribution.'

k = k + 1
d[k].name = 'use_local_average'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, subtract smoothed rate from count rate to reduce ' + $
    'background effects'
d[k].more_doc = 'USE_LOCAL_AVERAGE, LOCAL_AVERAGE_FREQUENCY, and ' + $
    'AUTO_FREQUENCY are used together.<P>USE_LOCAL_AVERAGE - this ' + $
    'turns on the strategy that compensates for the time-variable ' + $
    'background by averaging on a time scale shorter than the ' + $
    'observation but long with respect to a modulation ' + $
    'cycle.<P>LOCAL_AVERAGE_FREQUENCY - the values of the time ' + $
    'width used in the Gaussian convolution in units of frequency ' + $
    'with respect to the rotation period. It is a vector with ' + $
    'dimensions of [9, 3] for each grid and harmonic. So a value ' + $
    'of 16 for a rotation period of 4.0 seconds is 0.25 ' + $
    'seconds.<P>AUTO_FREQUENCY - If set, use distance from spin ' + $
    'axis to determine the values for local_average_frequency.'

k = k + 1
d[k].name = 'local_average_frequency'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(9,3)'
d[k].units = 'Freq. wrt rotation period'
d[k].default = '16 (grids 1-8), 4 (grid 9)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Time width for local averaging, (9x3)- 9 detectors, 3 ' + $
    'harmonics.'
d[k].more_doc = '>use_local_average'

k = k + 1
d[k].name = 'auto_frequency'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, use distance from spin axis to determine averaging ' + $
    'frequency'
d[k].more_doc = '>use_local_average'

k = k + 1
d[k].name = 'max_harmonic'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Maximum harmonic to use in modulation patterns and imaging'
d[k].more_doc = ''

k = k + 1
d[k].name = 'r_threshold'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = 'fltarr(9)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Radius threshold in degrees. Suppress counts when radial ' + $
    'offset of image relative to imaging axis is < r_threshold '
d[k].more_doc = ''

k = k + 1
d[k].name = 'cbe_max_corr'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'cbe_multi_atten_threshold'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '-1.00000'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Enable multi attenuator state correction to cbe.gridtran for ' + $
    'energy bands that start at this energy. -1.0 is fully ' + $
    'disabled. Min suggested theshold is 12 keV. '
d[k].more_doc = ''

k = k + 1
d[k].name = 'xyoffset'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = '[600.,200.]'
d[k].range = '0 - 1000'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Offset of map from Sun center'
d[k].more_doc = '>use_flare_xyoffset'

; Control parameters for hsi_eventlist
k = k + 1
d[k].name = 'eventlist_strategy'
d[k].class = 'hsi_eventlist'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'HSI_EVENTLIST_PACKET'
d[k].range = 'HSI_EVENTLIST_PACKET, HSI_EVENTLIST_SIMULATION'
d[k].control_or_info = 'Control'
d[k].level = 'N/A'
d[k].purpose = 'Strategy used by GETDATA to get event lists'
d[k].more_doc = ''

; Control parameters for hsi_eventlist_file
k = k + 1
d[k].name = 'ev_filename'
d[k].class = 'hsi_eventlist_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Name of eventlist file to use as input'
d[k].more_doc = ''

; Control parameters for hsi_eventlist_file_sim
k = k + 1
d[k].name = 'obs_time_interval'
d[k].class = 'hsi_eventlist_file_sim'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

; Control parameters for hsi_eventlist_strategy
k = k + 1
d[k].name = 'a2d_index_mask'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte(27)'
d[k].units = 'N/A'
d[k].default = 'BytArr(27) + 1B'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Mask for the selection of a2d events'
d[k].more_doc = '>det_index_mask'

k = k + 1
d[k].name = 'energy_band'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(2)'
d[k].units = 'keV'
d[k].default = '[12,25]'
d[k].range = '1 - 15000'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Not used.'
d[k].more_doc = 'The ENERGY_BAND parameter in the hsi_calib_eventlist class ' + $
    'is used for imaging. <P>ENERGY_BAND is no longer used at all ' + $
    'for the lightcurve and spectrum objects. For lightcurve and ' + $
    'spectrum, the default value is [0.,0.] and should not be ' + $
    'changed. <P>For lightcurve, use LTC_ENERGY_BAND to set ' + $
    'energy bands.<P>For spectrum, use SP_ENERGY_BINNING to set ' + $
    'energy bands.<P><P>For image, use ENERGY_BAND to set the ' + $
    'min, max of the energies to use, but see IM_ENERGY_BINNING ' + $
    'and EB_INDEX for more options for setting energy bands for ' + $
    'images.'

k = k + 1
d[k].name = 'gain_time_wanted'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'gain_generation'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1000'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'time_range'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'sec or ANYTIM'
d[k].default = '[0,4]'
d[k].range = '0 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Time range to accumulate relative to obs_time_interval, or ' + $
    'absolute'
d[k].more_doc = '>obs_time_interval'

k = k + 1
d[k].name = 'number_of_half_rotations'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'time_unit'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = 'binary microsec (2^-20 sec)'
d[k].default = '1'
d[k].range = '1 - 32'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Time unit used in the eventlist time tags '
d[k].more_doc = 'Time unit must be 1 for coincidence. Defaults to 1 for ' + $
    'spectrum and image, 16 for lightcurve.'

k = k + 1
d[k].name = 'no_livetime'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'ct_interpolate'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'deflt_atten_state'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Use this value for attenuator state if can''t read it from ' + $
    'packet. Used with sim data.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'fr_deadtime_window'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, remove rear events that occur without possibility of ' + $
    'a front veto because of data gap. Used only for polarization ' + $
    'analysis.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'extend_time_range'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = ''
d[k].default = '2.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Internal parameter - extends time to find dropouts'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dp_cutoff_max'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'sec'
d[k].default = '0.05'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Dropout cutoff, max duration for dropout to be recognized'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_cutoff_min'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = 'sec'
d[k].default = '0.0008'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Dropout cutoff, min duration for dropout to be recognized'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_cutoff_coeff'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = ''
d[k].default = '4.5'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Used in function to determine min datagap duration'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_cutoff_xp'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = ''
d[k].default = '-0.9'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Used in function to determine min datagap duration'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_enable'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, handle dropouts'
d[k].more_doc = 'If set, enable data dropout handling software. A number of ' + $
    'parameters control how data gaps are dealt with.<P><P> ' + $
    'dp_enable = 1 means enable dropout handling<P> ' + $
    'dp_extend_def, dp_extend_sec, dp_extend_utlim: These three ' + $
    'parameters work together. For each detector, if the data ' + $
    'time is < dp_extend_utlim, extend the gap by dp_extend_def, ' + $
    'if data time is > dp_extend_utlim, extend the gap by ' + $
    'dp_extend_sec<P>dp_lld, dp_uld: Events that occur in ' + $
    'channels outside the range dp_lld and dp_uld will not end a ' + $
    'data gap (separate controls for each detector)<P><P>The ' + $
    'following parameters are used to determine the minimum ' + $
    'datagap duration in b usec based on rate, r, in /busec via ' + $
    'this equation: <P> coeff*c^(1+xp) * r^xp > (min*c) < ' + $
    '(max*c))<P> where c is 2.^20 and r is raw data rate in ' + $
    'counts/det/bsec<P>dp_cutoff_max - max duration for dropout ' + $
    'to be recognized<P>dp_cutoff_min - min duration for dropout ' + $
    'to be recognized<P>dp_cutoff_coeff - normalization of ' + $
    'dropout curve<P>dp_cutoff_xp - exponent'

k = k + 1
d[k].name = 'dp_extend_def'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(9)'
d[k].units = 'sec'
d[k].default = 'fltarr(9)'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Extend datagap by this if data time is before ' + $
    'dp_extend_utlim'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_extend_sec'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(9)'
d[k].units = 'sec'
d[k].default = '[.01,.01,.01,01,.02,.01,.02,.02,.02]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Extend datagap by this if data time is after dp_extend_utlim'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_extend_utlim'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(9)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Time for each detector to decide whether to extend datagap ' + $
    'by dp_extend_def or dp_extend_sec'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'min_time_4_off'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Minimum time without events to consider detectors off'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dp_lld'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int(9)'
d[k].units = ''
d[k].default = '51,49,52,54,52,44,50,46,40'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Events at this channel or below don''t end datagap'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'dp_uld'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'long(9)'
d[k].units = ''
d[k].default = 'onarr(9)+6000'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Events at this channel or above don''t end datagap'
d[k].more_doc = '>dp_enable'

k = k + 1
d[k].name = 'no_csa_dropout'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If this is set, then dropoutlist compiled w/o using'
d[k].more_doc = ''

; Control parameters for hsi_lightcurve
k = k + 1
d[k].name = 'ltc_energy_band'
d[k].class = 'hsi_lightcurve'
d[k].main_object = 'Lightcurve'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = '[3,15000]'
d[k].range = '1 - 15000'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Energy band(s) for the lightcurve'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ltc_time_range'
d[k].class = 'hsi_lightcurve'
d[k].main_object = 'Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'sec'
d[k].default = '[0,0]'
d[k].range = '0 - 1.e9'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Time range for the light curve relative to obs_time_interval'
d[k].more_doc = '>obs_time_interval'

k = k + 1
d[k].name = 'ltc_time_resolution'
d[k].class = 'hsi_lightcurve'
d[k].main_object = 'Lightcurve'
d[k].type = 'float'
d[k].units = 'sec'
d[k].default = '.1'
d[k].range = '.001 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Lightcurve time bin size'
d[k].more_doc = '>obs_time_interval'

; Control parameters for hsi_modul_pattern
k = k + 1
d[k].name = 'imaging_strategy'
d[k].class = 'hsi_modul_pattern'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'hsi_annsec_pattern'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

; Control parameters for hsi_modul_pattern_strategy
k = k + 1
d[k].name = 'image_dim'
d[k].class = 'hsi_modul_pattern_strategy'
d[k].main_object = 'Image'
d[k].type = 'int(2)'
d[k].units = 'pixel'
d[k].default = '[64,64]'
d[k].range = '16 - 1024'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Dimensions of image'
d[k].more_doc = 'IMAGE_DIM defines the total number of pixels used in the x ' + $
    'and y dimensions.<P>PIXEL_SIZE defines the size in arcsec of ' + $
    'each pixel in x and y. <P>The total size of the ' + $
    'reconstructed image is IMAGE_DIM times ' + $
    'PIXEL_SIZE.<br>Example: if image_dim=[32,64] and ' + $
    'pixel_size=[4,4] then image will be 128 X 256 arcsec'

k = k + 1
d[k].name = 'pixel_scale'
d[k].class = 'hsi_modul_pattern_strategy'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '1.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Scale factor for pixel_size'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixel_size'
d[k].class = 'hsi_modul_pattern_strategy'
d[k].main_object = 'Image'
d[k].type = 'float(2)'
d[k].units = 'arcsec'
d[k].default = '[4,4]'
d[k].range = '1 - 100'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'X,Y size of the image pixel'
d[k].more_doc = '>image_dim'

; Control parameters for hsi_modul_profile
k = k + 1
d[k].name = 'vimage'
d[k].class = 'hsi_modul_profile'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

; Control parameters for hsi_monitor_rate
k = k + 1
d[k].name = 'mr_time_range'
d[k].class = 'hsi_monitor_rate'
d[k].main_object = 'Obs'
d[k].type = 'double(2)'
d[k].units = 'sec'
d[k].default = '[0,0]'
d[k].range = '0 - 100'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used.  Use obs_time_interval to specify time range.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'no_102'
d[k].class = 'hsi_monitor_rate'
d[k].main_object = 'Obs'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, read app id 152, otherwise 102'
d[k].more_doc = ''

k = k + 1
d[k].name = 'det_index_mask'
d[k].class = 'hsi_monitor_rate'
d[k].main_object = 'Obs'
d[k].type = 'byte(18)'
d[k].units = 'N/A'
d[k].default = '[0,0,0,1,1,1,1,1,0]'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Not used'
d[k].more_doc = ''

; Control parameters for hsi_packet
k = k + 1
d[k].name = 'file_type'
d[k].class = 'hsi_packet'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'fits'
d[k].range = 'fits, gse, itos, raw'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'File type '
d[k].more_doc = ''

; Control parameters for hsi_packet_file
k = k + 1
d[k].name = 'check_bad_packet'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'adp_test'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte(10)'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, some selection tests are done for the aspect data ' + $
    'processor '
d[k].more_doc = ''

k = k + 1
d[k].name = 'app_id'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = 'All'
d[k].range = '0 - 300'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Only packets with this or these app id(s) are returned ' + $
    '(scalar or vector)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'obs_time_interval'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = '[0.d,0.d]'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Absolute time interval to retrieve data for.'
d[k].more_doc = 'obs_time_interval is the overall interval to retrieve data ' + $
    'for. The file containing the time will automatically be ' + $
    'found and set into the filename parameter.<P>Note: Absolute ' + $
    'time format means a fully referenced time in any ANYTIM ' + $
    'format, e.g. ''3-mar-05 12:36:44''. If you supply an ' + $
    'absolute time as a number, it is interpreted as seconds ' + $
    'since 1-jan-1979 00:00 (and should be double ' + $
    'precision).<P>Specifying the time range and time bins is ' + $
    'done differently for each object class.<P><b>IMAGE ' + $
    'OBJECT:</b> No longer uses obs_time_interval (except for ' + $
    'backward compatibility). Use im_time_interval and ' + $
    'im_time_bin. <P><b>SPECTRUM OBJECT:</b> Use ' + $
    'obs_time_interval, time_range, and/or sp_time_interval. ' + $
    '<P>obs_time_interval - start/end of overall time interval in ' + $
    'absolute time format <P>time_range - normally [0.,0.] ' + $
    'meaning use all of obs_time_interval. Or, can specify a ' + $
    'start/end time in seconds relative to start of ' + $
    'obs_time_interval to use. Or can specify an absolute ' + $
    'interval, which will supercede ' + $
    'obs_time_interval.<P>sp_time_interval - If scalar, then this ' + $
    'is width of bins in seconds. If an array, then defines the ' + $
    'edges of the time bins, and must be in absolute ' + $
    'time.<P><b>LIGHTCURVE OBJECT:</b> Use obs_time_interval, ' + $
    'ltc_time_range, time_range, and/or ltc_time_resolution. ' + $
    '<P>obs_time_interval - start/end of overall time interval in ' + $
    'absolute time format <P>ltc_time_range - (interchangeable ' + $
    'with time_range) Normally [0.,0.] meaning use all of ' + $
    'obs_time_interval. Or, can specify a start/end time in ' + $
    'seconds relative to start of obs_time_interval to use. Or ' + $
    'can specify an absolute interval, which will supercede ' + $
    'obs_time_interval<P>ltc_time_resolution - width of time bins ' + $
    'in seconds. <P><b>OBSERVING SUMMARY OR QUICKLOOK:</b> Use ' + $
    'obs_time_interval to specify overall time. <P>Example: <br>o ' + $
    '-> set, obs_time_interval= [''20-feb-02 11:06'', ''20-feb-02 ' + $
    '11:08'']<br>o -> set, time_range=[20,40]'

k = k + 1
d[k].name = 'packet_time_range'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'packet_per_bunch'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '5000'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Number of packets to read in a each read operation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'filename'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = 'None'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Name(s) of the level-0/telemetry data file(s)'
d[k].more_doc = 'Specifies the filename containing the level-0 data. The ' + $
    'filename may be of type FITS, ITOS, GSE, SMEX or RAW. ' + $
    '<br>Note: users should normally select data by setting a ' + $
    'time interval through the obs_time_interval and/or ' + $
    'time_range parameters, which will automatically set the ' + $
    'correct filename.'

; Control parameters for hsi_phz_stacker
k = k + 1
d[k].name = 'phz_n_roll_bins_min'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = ''
d[k].default = '12'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Minimum number of roll bins for each grid'
d[k].more_doc = '>use_phz_stacker'

k = k + 1
d[k].name = 'phz_n_roll_bins_max'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = ''
d[k].default = '64'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Maximum number of roll bins for each grid'
d[k].more_doc = '>use_phz_stacker'

k = k + 1
d[k].name = 'phz_n_roll_bins_control'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Number of rolls bins for each grid'
d[k].more_doc = '>use_phz_stacker'

k = k + 1
d[k].name = 'phz_report_roll_bins'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, report number of roll bins used'
d[k].more_doc = '>use_phz_stacker'

k = k + 1
d[k].name = 'phz_n_phase_bins'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = ''
d[k].default = '12'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Number of phase bins'
d[k].more_doc = '>use_phz_stacker'

k = k + 1
d[k].name = 'phz_radius'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'arcsec'
d[k].default = '60'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Radius of extent of source'
d[k].more_doc = '>use_phz_stacker'

k = k + 1
d[k].name = 'reference_position_angle'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'radians'
d[k].default = '0.000000'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Align initial grid position to this angle from solar north ' + $
    'in phz_stacker'
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_reference_position_angle'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, use the reference position angle to align initial ' + $
    'grid position in phz_stacking.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'use_phase_conjugation'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, use conjugate arithmetic to aggregate stacked phases ' + $
    'prior to obtaining visibilities, Not used for now.'
d[k].more_doc = ''

; Control parameters for hsi_sohdata
k = k + 1
d[k].name = 'soh_time_range'
d[k].class = 'hsi_sohdata'
d[k].main_object = 'soh'
d[k].type = 'double(2)'
d[k].units = 'sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                '
d[k].purpose = 'Time range for the SOH data relative to obs_time_interval '
d[k].more_doc = ''

k = k + 1
d[k].name = 'soh_label'
d[k].class = 'hsi_sohdata'
d[k].main_object = 'soh'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Pointer to a string array with the keywords wanted'
d[k].more_doc = ''

; Control parameters for hsi_memvis
k = k + 1
d[k].name = 'vis_show_image'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show intermediate images'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_chi_limit'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '1.1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Chi-squared value at which image reconstruction will stop'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_lambda_max'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '20'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max value of lambda allowed '
d[k].more_doc = 'During image reconstruction, each lambda iteration ' + $
    'corresponds to a weakening of the smoothness constraint, ' + $
    'allowing a better fit (smaller chi-square) to the data. This ' + $
    'parameter sets the maximum value that the lambda iteration ' + $
    'can reach at which point mem_vis will exit and return the ' + $
    'last image. Note that the lambda iteration step is not ' + $
    'fixed, it accelerates as the image reconstruction ' + $
    'progresses.'

k = k + 1
d[k].name = 'vis_no_chi2'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, mem_vis iterates to vis_lambda_max, and ignores ' + $
    'vis_chi_limit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_iter_max'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '150'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max number of iterations for a given lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_delta_max'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.001'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max value of delta for a switch to new lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_itgain'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.3'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Initial value of the iteration gain'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_lnorm'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '1.e-5'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Initial strength of the entropy constraint'
d[k].more_doc = 'Controls the initial strength of the entropy constraint. A ' + $
    'small value corresponds to a very strong smoothness ' + $
    'constraint, and a large value corresponds to a very weak ' + $
    'constraint. A large value allows for very fast ' + $
    'reconstruction of an image of point sources, but will result ' + $
    'in very poor broken up images if the sources are extended. ' + $
    'The default is currently 1e-5, which is a low value allowing ' + $
    'for safe, but slow, reconstruction of most sources.'

k = k + 1
d[k].name = 'vis_init_btot'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, initialize the value of btot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_progress_bar'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, display progress bar with cancel option'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_sys_err'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.01'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Systematic error term'
d[k].more_doc = ''

; Control parameters for hsi_mem_sato
k = k + 1
d[k].name = 'sato_show_image'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show intermediate images'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_chi_limit'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '1.03'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Chi-squared value at which image reconstruction will stop'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_lambda_max'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '20'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max value of lambda allowed'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_no_chi2'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, do not used the chi^2 to stop'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_iter_max'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '30'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max number of iterations for a given lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_delta_max'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.03'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Max value of delta for a switch to new lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_itgain'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.3'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Initial value of the iteration gain'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_lnorm'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Normalization factor for lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_init_btot'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If 1 initialize the value of btot'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_progress_bar'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, display progress bar with cancel button'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_sys_err'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.01'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Systematic error term'
d[k].more_doc = ''

; Control parameters for hsi_clean
k = k + 1
d[k].name = 'clean_niter'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '100'
d[k].range = '1 - ?'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Max number of iterations'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_more_iter'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, resume cleaning from previous results'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_negative_max_test'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, stop iterating when highest absolute comes from a ' + $
    'negative peak'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_chi_sq_min_test'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_frac'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.05'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Fraction of map maximum that defines level for real features ' + $
    '(gain)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_chi_sq_crit'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '-1.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Stop iterating when chi square reaches this value '
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_no_chi2'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'If set, do not used the chi^2 to stop'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_show_maps'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, show intermediate maps'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_show_n_maps'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Show every n''th map while iterating'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_show_map_xdim'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1024'
d[k].range = 'pixel'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'X size of plot window for intermediate results'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_show_chi'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, plot chi-square while iterating'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_show_n_chi'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Show every n''th chi-square plot while iterating'' '
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_box'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Define clean boxes (where we expect sources to be)'
d[k].more_doc = '<h3 align="center">CLEAN Boxes</h3><P>Clean boxes are used ' + $
    'in the CLEAN image reconstruction algorithm to define the ' + $
    'locations where we expect source(s) to be found. There are ' + $
    'two ways to set clean boxes - using clean_box to define ' + $
    'rectangular areas, or clean_cw_nop and clean_cw_list to ' + $
    'define regions of any shape.<P>clean_cw_nop, clean_cw_list: ' + $
    'This method can handle clean boxes that are any shape. ' + $
    'clean_cw_list is a (2,n) array of x,y coordinates that ' + $
    'define the vertices of each clean box. clean_cw_nop is a ' + $
    'vector giving the number of elements in clean_cw_list that ' + $
    'correspond to each clean box. For example, if cw_nop_list is ' + $
    'a (2,9) and cw_nop = [4,5], then clean_cw_list[*,0:3] are ' + $
    'the vertices for the first clean box (a triangle), and ' + $
    'clean_cw_list[*,[4:8] are the vertices for the second clean ' + $
    'box (a rectangle).<P>clean_box: clean_box can only be used ' + $
    'to define rectangular clean boxes. clean_box is a (4,n) ' + $
    'array, where the 4 elements of the first dimension are xlo, ' + $
    'ylo, xhi, yhi, and the second dimension is the box number. ' + $
    '<P>If clean_box, clean_cw_nop, and clean_cw_list are all ' + $
    'defined, clean_cw_nop/clean_cw_list take priority.<P>Note: ' + $
    'the graphical method for setting clean boxes (by setting ' + $
    'clean_mark_box to 1) uses only clean_cw_nop/clean_cw_list. ' + $
    'It does not know about any boxes set via the clean_box ' + $
    'parameter.'

k = k + 1
d[k].name = 'clean_cw_list'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Define clean boxes (where we expect sources to be)'
d[k].more_doc = '>clean_box'

k = k + 1
d[k].name = 'clean_cw_nop'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Define clean boxes (where we expect sources to be)'
d[k].more_doc = '>clean_box'

k = k + 1
d[k].name = 'clean_cw_inverse'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use area outside clean box instead of inside'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_progress_bar'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, display progress bar with cancel button'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_mark_box'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, graphically mark clean boxes on dirty map'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_media_mode'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, final image is just the component map (residual map ' + $
    'is not added to it).'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_beam_width_factor'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '1.0'
d[k].range = '1. - 3.'
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Factor applied to clean_beam convolving beam for source ' + $
    'appearance. Larger value narrows the beam.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_old_regress_method'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'Use old method of regression.'
d[k].more_doc = '>clean_regress_combine'

k = k + 1
d[k].name = 'clean_regress_combine'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, use regression to combine component map and residual ' + $
    'map.'
d[k].more_doc = '<h3 align="center">Using regression to form final CLEAN ' + $
    'image</h3><P>If CLEAN_REGRESS_COMBINE is set to 0 (the ' + $
    'default) and clean_media_mode is not set, the component map ' + $
    'and residual map are added to form the final map.<P>If ' + $
    'CLEAN_REGRESS_COMBINE is set, regression is used to compute ' + $
    'the coefficents for combining the component and residual ' + $
    'maps to produce an image whose modulation profile most ' + $
    'closely approximates the observed modulation ' + $
    'profile.<P>Using CLEAN_REGRESS_COMBINE=1 is ' + $
    'recommended.<P>On 8-Apr-2013, we improved the method for ' + $
    'computing the coefficients. To use the old way, set ' + $
    'CLEAN_OLD_REGRESS_METHOD to 1. Both methods are explained ' + $
    'below.<P>The final map is A*Component_Map + B*Resid_Map. ' + $
    '<P>CLEAN_OLD_REGRESS_METHOD = 0 (default) New method: ' + $
    '<br>Coefficient A is determined from a linear regression of ' + $
    'the counts expected from the Component_Map against the ' + $
    'observed counts. Coefficient B is determined from a linear ' + $
    'regression of the counts of the Resid_Map against the ' + $
    'observed counts MINUS A times the counts from the Component ' + $
    'Map (i.e. Resid_Map counts are regressed against the ' + $
    'residual counts, calculated from the observed counts minus ' + $
    'the newly scaled component ' + $
    'counts).<P>CLEAN_OLD_REGRESS_METHOD = 1 Old method: ' + $
    '<br>Coefficients A and B are computed in one linear ' + $
    'regression operation of the counts from the Component_Map ' + $
    'and Resid_Map against the observed counts.'

; Control parameters for hsi_vis_fwdfit
k = k + 1
d[k].name = 'vf_nophase'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, forces all input phases to zero'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_circle'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, forces sources to be circular'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_noerr'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, forces fit to ignore input statistical errors'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_absolute'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, generate fit by minimizing the sum of ABS(input ' + $
    'visibility - model visibility)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_maxiter'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = ''
d[k].default = '2000'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Maximum number of iterations per stage'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_srcin'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Optional input source structure array to serve as a starting ' + $
    'point'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_multi'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, fit to a double source'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_loop'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, fit to a loop'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_noplotfit'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, suppress plotfit display'
d[k].more_doc = ''

; Control parameters for hsi_mem_njit
k = k + 1
d[k].name = 'nj_ferr'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Allowed flux error.  If 0.0, use tol*flux'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_tol'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.03'
d[k].range = ''
d[k].control_or_info = 'Control'
d[k].level = 'Advanced'
d[k].purpose = 'Tolerance for convergence'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_show_maps'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '0'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Expert'
d[k].purpose = 'If set, show intermediate plots'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_progress_bar'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'Not used'
d[k].more_doc = ''

; Control parameters for hsi_uv_smooth
k = k + 1
d[k].name = 'uv_show_vismap'
d[k].class = 'hsi_uv_smooth'
d[k].main_object = 'Image'
d[k].type = 'byte'
d[k].units = ''
d[k].default = '1'
d[k].range = '0 - 1'
d[k].control_or_info = 'Control'
d[k].level = 'Standard'
d[k].purpose = 'If set, plot the visibility sampling on the u,v plane'
d[k].more_doc = ''

; Info parameters for hsi_aspect_solution
k = k + 1
d[k].name = 'avg_spinperiod'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'as_quality'
d[k].class = 'hsi_aspect_solution'
d[k].main_object = 'Image'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_clean
k = k + 1
d[k].name = 'clean_resid_map'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Residual map in polar coordinates'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_component_map'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Clean component map in polar coordinates'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_source_map'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Clean source map in polar coordinates'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_cleaned_map'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Cleaned map in polar coordinates'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_components'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_windows'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_ed_flux'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_last_iter'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Last iteration # (starting from 0)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_chi_sq_tot'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final chi square'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_chi_sq_value'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final chi square for each subcollimator for each iteration'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_chi_sq_iter'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_chi_window'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '-1'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Index of show_chi plot window'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_map_window'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '-1'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Index of show_map plot window'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_normalization'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_lt_correction'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = '0.000000'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Livetime correction, used internally when restarting from ' + $
    'last clean'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_stop'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Reason for stopping iterations'
d[k].more_doc = ''

k = k + 1
d[k].name = 'clean_info_control'
d[k].class = 'hsi_clean'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Contains all the control parameters when image was made. ' + $
    'Used internally.'
d[k].more_doc = ''

; Info parameters for hsi_srm
k = k + 1
d[k].name = 'geom_area'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Geometrical area of each detector'
d[k].more_doc = 'Geometrical area of each detector assumed in generating the ' + $
    'srm in these units: it is a circle of radius 7.1 cm ' + $
    '(regardless of front or rear segment - this makes the front ' + $
    'segments less "efficient" than you might expect, since their ' + $
    'true diameter is about 6.1 cm)'

k = k + 1
d[k].name = 'info_ph_edges'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Photon energy binning used in SRM'
d[k].more_doc = ''

k = k + 1
d[k].name = 'info_ct_edges'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Count energy binning used in SRM'' '
d[k].more_doc = ''

k = k + 1
d[k].name = 'info_srm_ct_edges'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'keV'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Count energy edges used internally in hessi_build_srm to ' + $
    'build srm'
d[k].more_doc = ''

k = k + 1
d[k].name = 'info_apt_filename'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'APT filename used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'srm_used_aspect_time'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'diagonal_obj'
d[k].class = 'hsi_srm'
d[k].main_object = 'Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Internal storage for the diagonal srm object'
d[k].more_doc = ''

; Info parameters for hsi_vis_fwdfit
k = k + 1
d[k].name = 'vf_srcout'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure(s) containing fitted source parameters'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_sigma'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure(s) containing sigmas on fitted source parameters'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_niter'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of iterations done in fit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_redchi2'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Reduced chi-square of fit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_nfree'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of free parameters in fit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vf_qflag'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Quality flag. Bits indicate the type of problem found. 0 ' + $
    'means good fit'
d[k].more_doc = 'Quality flag for vis_fwdfit fit. Check bits as follows: ' + $
    '<P>IF (qflag AND 1) = 1 then warning is: MARGINAL DETECTION ' + $
    'of at least one source component<P>IF (qflag AND 2) = 1 then ' + $
    'warning is: FIT IS SUSPECT since at least one fitted ' + $
    'parameter is at edge of its range.<P>IF (qflag AND 4) = 1 ' + $
    'then warning is: FIT IS UNSTABLE due to shallow chisq ' + $
    'minimum.<P>IF (qflag AND 8) = 1 then warning is: LARGE ' + $
    'UNCERTAINTY in at least one fitted parameter.'

k = k + 1
d[k].name = 'vf_vis_window'
d[k].class = 'hsi_vis_fwdfit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Window ID for visibility fit plot'
d[k].more_doc = ''

; Info parameters for hsi_visibility
k = k + 1
d[k].name = 'vis_det_index_mask_used'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'byte(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Byte array showing which detectors had acceptable ' + $
    'visibilities and were used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_corr_factors'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Normalization correction factors for each detector'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_map'
d[k].class = 'hsi_visibility'
d[k].main_object = 'Image'
d[k].type = 'objref'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Not used'
d[k].more_doc = ''

; Info parameters for hsi_bproj
k = k + 1
d[k].name = 'bproj_alg_available'
d[k].class = 'hsi_bproj'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_forwardfit
k = k + 1
d[k].name = 'ff_chi_ff'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'C-statistic of 9 detectors'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_chiav_ff'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Average of C-statistic of detectors used in reconstruction'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_phot_sec'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'photons/sec'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Incident photon rate (photons/s)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_back_sec'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Background photon rate (photons/s SC)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_cts_sec'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Detected count rate (cts/s)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_peak_flux'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'cts/(s cm^2 asec^2)'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Normalized gaussian amplitude of peak of image ' + $
    '(photons/s/cm2)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ff_coeff_ff'
d[k].class = 'hsi_forwardfit'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Coefficients'
d[k].more_doc = 'Coefficients in array of form fltarr(npar,n_gaussians) where ' + $
    'npar is the number of parameters (4 for gaussian, 6 for ' + $
    'elliptical, 7 for curved elliptical) and n_gaussians is the ' + $
    'number of gaussian components used. <P>The parameters are: ' + $
    '<br>1) the relative flux amplitude,<br>2) the gaussian width ' + $
    '(in arcsec) <br>3) the x- and <br>4) the y-position of the ' + $
    'gaussian center relative to the map center (in arcsec)<br>5) ' + $
    'the eccentricity for elliptical gaussians ' + $
    '[e=ywidth/xwidth-1]<br>6) the tilt angle of elliptical ' + $
    'gaussian (deg counterclockwise from x-axis)<br>7) curvature ' + $
    'ratio, i.e. gaussian width/curvature radius'

; Info parameters for hsi_image_alg
k = k + 1
d[k].name = 'clean_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'clean_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'clean_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'clean_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'clean_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'clean_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'clean_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'ff_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'nj_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'pixon_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'sato_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'uv_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vf_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Overall goodness of fit of image from comparison of ' + $
    'predicted and observed profiles'' '
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Goodness of fit of image for each detector from comparison ' + $
    'of predicted and observed profiles'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_coeff'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(3,9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Consant, slope, correlation of fit of predicted to observed ' + $
    'profile'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_mc_tot_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_mc_cstat'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo C-statistic for each detector'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_mc_tot_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for all detectors'
d[k].more_doc = '>profile_show_plot'

k = k + 1
d[k].name = 'vis_profile_mc_fwhm'
d[k].class = 'hsi_image_alg'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Monte Carlo FWHM for each detector'
d[k].more_doc = '>profile_show_plot'

; Info parameters for hsi_image_single
k = k + 1
d[k].name = 'algorithm_used'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'algorithm_units'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'alg_unit_scale'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixel_area'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'image_units'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'algorithm_available'
d[k].class = 'hsi_image_single'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_image_strategy
k = k + 1
d[k].name = 'n_images_done'
d[k].class = 'hsi_image_strategy'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'im_out_fits_filename'
d[k].class = 'hsi_image_strategy'
d[k].main_object = 'Image'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set to a string, a FITS file will be written containing ' + $
    'the image (or image cube)'
d[k].more_doc = ''

; Info parameters for hsi_mem_njit
k = k + 1
d[k].name = 'nj_flux'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Estimate of total flux used as input to mem_njit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_flux_err'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Allowed flux error used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_iter'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of iterations performed'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_chi2_nrm'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Reduced chi-square of fit'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_flux_nrm'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Ratio of total flux of model to total observed flux'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_alpha'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'First Lagrange multiplier used in maximizing objective ' + $
    'function'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nj_beta'
d[k].class = 'hsi_mem_njit'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Second Lagrange multiplier used in maximizing objective ' + $
    'function'
d[k].more_doc = ''

; Info parameters for hsi_pixon
k = k + 1
d[k].name = 'pixon_residual'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Data residuals'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixon_bobj'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Background data used in pixon'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixon_rgof'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final c-statistic (goodness of fit)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixon_error'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Array of estimated uncertainty of reconstructed image'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixon_iterate'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of iterations used at final resolution'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixon_outresolution'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '-'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Minimum pixon size used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'pixonmap'
d[k].class = 'hsi_pixon'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'pixel'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Map giving spatial scale used at each pixel'
d[k].more_doc = ''

; Info parameters for hsi_uv_smooth
k = k + 1
d[k].name = 'uv_window'
d[k].class = 'hsi_uv_smooth'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Window number for plot of visibility sampling in uv plane'
d[k].more_doc = ''

; Info parameters for hsi_ephemeris
; Info parameters for hsi_flarelist
k = k + 1
d[k].name = 'n_flares'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of HESSI flares in catalog'
d[k].more_doc = ''

k = k + 1
d[k].name = 'list_start_time'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'string'
d[k].units = 'CCSDS format'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Start time of HESSI flare catalog'
d[k].more_doc = ''

k = k + 1
d[k].name = 'list_end_time'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'string'
d[k].units = 'CCSDS format'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'End time of HESSI flare catalog'
d[k].more_doc = ''

k = k + 1
d[k].name = 'nflags'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = '0 - 32'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of flare data flags'
d[k].more_doc = ''

k = k + 1
d[k].name = 'flag_ids'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'string(32)'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'String identifiers for each flare flag'
d[k].more_doc = ''

k = k + 1
d[k].name = 'simulated_data'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, data is simulated'
d[k].more_doc = ''

k = k + 1
d[k].name = 'concat_flag'
d[k].class = 'hsi_flarelist'
d[k].main_object = 'Flarelist'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, flare list has been concatenated from level 0 FITS ' + $
    'files.'
d[k].more_doc = ''

; Info parameters for hsi_full_rate
k = k + 1
d[k].name = 'n_energy_bands'
d[k].class = 'hsi_full_rate'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of energy bands'
d[k].more_doc = ''

k = k + 1
d[k].name = 'energy_edges'
d[k].class = 'hsi_full_rate'
d[k].main_object = 'Obs'
d[k].type = 'float(10)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Energy bin edges'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dim2_unit'
d[k].class = 'hsi_full_rate'
d[k].main_object = 'Obs'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Detector id'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dim2_ids'
d[k].class = 'hsi_full_rate'
d[k].main_object = 'Obs'
d[k].type = 'string(18)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Segment Ids'
d[k].more_doc = ''

; Info parameters for hsi_modvariance
k = k + 1
d[k].name = 'variance_nbin'
d[k].class = 'hsi_modvariance'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'the number of time bins per spin period'
d[k].more_doc = ''

k = k + 1
d[k].name = 'energy_edges'
d[k].class = 'hsi_modvariance'
d[k].main_object = 'Obs'
d[k].type = 'float(2)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'The energy bin edges'
d[k].more_doc = ''

; Info parameters for hsi_obssummflag
k = k + 1
d[k].name = 'nflags'
d[k].class = 'hsi_obssummflag'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'the number of data flags'
d[k].more_doc = ''

k = k + 1
d[k].name = 'flag_ids'
d[k].class = 'hsi_obssummflag'
d[k].main_object = 'Obs'
d[k].type = 'string(32)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'string identifiers for each data flag,'
d[k].more_doc = ''

; Info parameters for hsi_obssumm
k = k + 1
d[k].name = 'summary_start_time'
d[k].class = 'hsi_obssumm'
d[k].main_object = 'Obs'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Start time of summary data.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'summary_end_time'
d[k].class = 'hsi_obssumm'
d[k].main_object = 'Obs'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Start time of summary data.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'simulated_data'
d[k].class = 'hsi_obssumm'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, data is simulated.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'concat_flag'
d[k].class = 'hsi_obssumm'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, summaries are concatenated.'
d[k].more_doc = ''

; Info parameters for hsi_obssummrate
k = k + 1
d[k].name = 'seg_index_mask'
d[k].class = 'hsi_obssummrate'
d[k].main_object = 'Obs'
d[k].type = 'byte(18)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'detector ids'
d[k].more_doc = ''

k = k + 1
d[k].name = 'n_energy_bands'
d[k].class = 'hsi_obssummrate'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'the number of energy bands'
d[k].more_doc = ''

k = k + 1
d[k].name = 'energy_edges'
d[k].class = 'hsi_obssummrate'
d[k].main_object = 'Obs'
d[k].type = 'float(10)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'the energy bin edges'
d[k].more_doc = ''

; Info parameters for hsi_particlerate
; Info parameters for hsi_qlook_image
k = k + 1
d[k].name = 'n_images'
d[k].class = 'hsi_qlook_image'
d[k].main_object = 'Obs'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'the number of images'
d[k].more_doc = ''

k = k + 1
d[k].name = 'start_time'
d[k].class = 'hsi_qlook_image'
d[k].main_object = 'Obs'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'start time'
d[k].more_doc = ''

k = k + 1
d[k].name = 'end_time'
d[k].class = 'hsi_qlook_image'
d[k].main_object = 'Obs'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'concat_flag'
d[k].class = 'hsi_qlook_image'
d[k].main_object = 'Obs'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, data is concatenated.'
d[k].more_doc = ''

; Info parameters for hsi_qlook_monitor_rate
; Info parameters for hsi_qlook_packet_rate
k = k + 1
d[k].name = 'app_id'
d[k].class = 'hsi_qlook_packet_rate'
d[k].main_object = 'Obs'
d[k].type = 'int(47)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'app_ids'
d[k].more_doc = ''

; Info parameters for hsi_qlook_pointing
k = k + 1
d[k].name = 'a1'
d[k].class = 'hsi_qlook_pointing'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '10.'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Scaling factor; Pointing = a1*data+a0'
d[k].more_doc = ''

k = k + 1
d[k].name = 'a0'
d[k].class = 'hsi_qlook_pointing'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '-1280.'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Scaling factor; Pointing = a1*data+a0'
d[k].more_doc = ''

; Info parameters for hsi_qlook_roll_angle
k = k + 1
d[k].name = 'a1'
d[k].class = 'hsi_qlook_roll_angle'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '2*pi*255'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Scaling factor; Roll Angle = a1*data+a0'
d[k].more_doc = ''

k = k + 1
d[k].name = 'a0'
d[k].class = 'hsi_qlook_roll_angle'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Scaling factor; Roll Angle = a1*data+a0'
d[k].more_doc = ''

; Info parameters for hsi_qlook_roll_period
k = k + 1
d[k].name = 'a1'
d[k].class = 'hsi_qlook_roll_period'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '.1'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Scaling factor; Roll Period = a1*data+a0'
d[k].more_doc = ''

k = k + 1
d[k].name = 'a0'
d[k].class = 'hsi_qlook_roll_period'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Scaling factor; Roll Period = a1*data+a0'
d[k].more_doc = ''

; Info parameters for hsi_qlook_spin_axis
; Info parameters for hsi_simulation
k = k + 1
d[k].name = 'sim_out_time_unit'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sim_ut_ref'
d[k].class = 'hsi_simulation'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'ANYTIM'
d[k].default = '--'
d[k].range = '--'
d[k].control_or_info = 'Info'
d[k].level = 'Advanced'
d[k].purpose = 'Reference time for simulation'
d[k].more_doc = ''

; Info parameters for hsi_artifact_mask
k = k + 1
d[k].name = 'artifact_time'
d[k].class = 'hsi_artifact_mask'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Time used to create artifact mask'
d[k].more_doc = ''

k = k + 1
d[k].name = 'artifact_edges'
d[k].class = 'hsi_artifact_mask'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Energy edges used to create artifact mask'
d[k].more_doc = ''

; Info parameters for hsi_pileup
k = k + 1
d[k].name = 'pileup_index'
d[k].class = 'hsi_pileup'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Indices of spectra with pileup correction'
d[k].more_doc = ''

; Info parameters for hsi_spectrogram
k = k + 1
d[k].name = 'binning'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Binning scheme for spectrograms'
d[k].more_doc = ''

k = k + 1
d[k].name = 'this_det_index'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Det_index_mask used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'this_a2d_index'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'A2d_index_mask used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'this_seg_index'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Seg_index_mask used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'units_spectrogram'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Units of spectrum'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dim_spectrogram'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Dimensions of spectrogram data array'
d[k].more_doc = ''

k = k + 1
d[k].name = 'total_count'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sec2time_unit'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_atten_state'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'RAS, 17-feb-2002'
d[k].more_doc = ''

k = k + 1
d[k].name = 'interval_atten_state'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'atten state for each time interval, 0,1,2,3, -1 for ' + $
    'transition'
d[k].more_doc = ''

k = k + 1
d[k].name = 'shutter_correction'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Fractional transmission through shutters ' + $
    '(energy,det,attenstate)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'dropout'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'livetime_arr'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'livetime_ctr'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Livetime fraction from the livetime counters. Only one ' + $
    'component of final livetime.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'offset_gain_str'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'used_xyoffset'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'float(2)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Value of xyoffset used.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'decim_table'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure describing times, energies, and weights of ' + $
    'decimation'
d[k].more_doc = ''

k = k + 1
d[k].name = 'decim_warning'
d[k].class = 'hsi_spectrogram'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Message if decimation is faulty'
d[k].more_doc = ''

; Info parameters for hsi_spectrum
k = k + 1
d[k].name = 'mask_flag'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = 'A2D, DET, SEG'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Detector selection flag used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_seg_index_mask'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_det_index_mask'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_data_de'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'sp_data_dt'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'num_rm'
d[k].class = 'hsi_spectrum'
d[k].main_object = 'Spectrum'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'number of detectors/segments used for averaging to "per ' + $
    'detector"'
d[k].more_doc = ''

; Info parameters for hsi_annsec_pattern
k = k + 1
d[k].name = 'rmap_dim'
d[k].class = 'hsi_annsec_pattern'
d[k].main_object = 'Image'
d[k].type = 'int(2)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'n_modpat'
d[k].class = 'hsi_annsec_pattern'
d[k].main_object = 'Image'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_binned_eventlist
k = k + 1
d[k].name = 'n_bin'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9,3)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of bins for each detector and harmonics'
d[k].more_doc = ''

k = k + 1
d[k].name = 'binned_n_event'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Total counts in each detector for selected energy range'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ut_binned_eventlist'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'be_time_info'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'valid_binned_index'
d[k].class = 'hsi_binned_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_calib_eventlist
k = k + 1
d[k].name = 'cbe_det_eff'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Structure containing average det efficiency and relative eff ' + $
    'for 9 dets'
d[k].more_doc = ''

k = k + 1
d[k].name = 'offaxis_disp'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'image_atten_state'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Attenuator state during image'
d[k].more_doc = ''

k = k + 1
d[k].name = 'phz_n_roll_bins_info'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of roll bins used for each grid'
d[k].more_doc = ''

k = k + 1
d[k].name = 'off_det_index'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'If set, then no valid times for this detector'
d[k].more_doc = ''

k = k + 1
d[k].name = 'binned_n_event'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'long(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Total counts in each detector for selected energy range'
d[k].more_doc = ''

k = k + 1
d[k].name = 'cbe_det_index_mask_used'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'byte(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Not used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'cbe_corr_factors'
d[k].class = 'hsi_calib_eventlist'
d[k].main_object = 'Image'
d[k].type = 'float(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'expert'
d[k].purpose = 'If not zeros or 1s, divide relative det. efficiency by ' + $
    'these. (really a control param)'
d[k].more_doc = ''

; Info parameters for hsi_eventlist_file
; Info parameters for hsi_eventlist_file_sim
k = k + 1
d[k].name = 'obs_time_interval'
d[k].class = 'hsi_eventlist_file_sim'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'Expert'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_eventlist
k = k + 1
d[k].name = 'eventlist_strategy'
d[k].class = 'hsi_eventlist'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'string'
d[k].units = 'N/A'
d[k].default = 'HSI_EVENTLIST_PACKET'
d[k].range = 'HSI_EVENTLIST_PACKET, HSI_EVENTLIST_SIMULATION'
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Strategy used by GETDATA to get event lists'
d[k].more_doc = ''

; Info parameters for hsi_eventlist_packet
; Info parameters for hsi_eventlist_strategy
k = k + 1
d[k].name = 'n_event'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'channel_range'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'coincidence_mask'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Not Used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'info_dp_cutoff'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'sec'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Threshold for defining a data gap.'
d[k].more_doc = ''

k = k + 1
d[k].name = 'info_dp_lld'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'int(9)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'User selectable gap closing lld. Not used as of Oct-02'
d[k].more_doc = ''

k = k + 1
d[k].name = 'absolute_time_range'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'sec since 1979/1/1 00'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Time range of data that was used'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ev_ut_ref'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'lt_ut_ref'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'struct(1)'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'csa_lld'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'last2'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Used internally'
d[k].more_doc = ''

k = k + 1
d[k].name = 'ev_sim_data'
d[k].class = 'hsi_eventlist_strategy'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = ''
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_fits
k = k + 1
d[k].name = 'simulated_data'
d[k].class = 'hsi_fits'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_lightcurve
; Info parameters for hsi_modul_pattern
k = k + 1
d[k].name = 'img_strategy_available'
d[k].class = 'hsi_modul_pattern'
d[k].main_object = 'Image'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_monitor_rate
k = k + 1
d[k].name = 'dtime'
d[k].class = 'hsi_monitor_rate'
d[k].main_object = 'Obs'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'mon_ut_ref'
d[k].class = 'hsi_monitor_rate'
d[k].main_object = 'Obs'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'ncyles'
d[k].class = 'hsi_monitor_rate'
d[k].main_object = 'Obs'
d[k].type = 'long'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_packet_file
k = k + 1
d[k].name = 'file_location'
d[k].class = 'hsi_packet_file'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_packet
k = k + 1
d[k].name = 'file_time_range'
d[k].class = 'hsi_packet'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double(2)'
d[k].units = 'ANYTIM'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Start/end time of file specified in filename parameter'
d[k].more_doc = ''

k = k + 1
d[k].name = 'n_packet'
d[k].class = 'hsi_packet'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Number of packets in the file (or in selection)'
d[k].more_doc = ''

k = k + 1
d[k].name = 'packet_ref_time'
d[k].class = 'hsi_packet'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'double'
d[k].units = 'N/A'
d[k].default = '0.0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'simulated_data'
d[k].class = 'hsi_packet'
d[k].main_object = 'Image, Spectrum, Lightcurve'
d[k].type = 'byte'
d[k].units = 'N/A'
d[k].default = '0'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_sohdata
k = k + 1
d[k].name = 'soh_absolute_time_range'
d[k].class = 'hsi_sohdata'
d[k].main_object = 'soh'
d[k].type = 'double(2)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'soh_time_array'
d[k].class = 'hsi_sohdata'
d[k].main_object = 'soh'
d[k].type = 'pointer'
d[k].units = 'se'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Array of collect times for each packet'
d[k].more_doc = ''

k = k + 1
d[k].name = 'soh_info'
d[k].class = 'hsi_sohdata'
d[k].main_object = 'soh'
d[k].type = 'pointer'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

k = k + 1
d[k].name = 'soh_data_files'
d[k].class = 'hsi_sohdata'
d[k].main_object = 'soh'
d[k].type = 'struct(1)'
d[k].units = 'N/A'
d[k].default = ''
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = ''
d[k].more_doc = ''

; Info parameters for hsi_memvis
k = k + 1
d[k].name = 'vis_lambda'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_iter'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of iterations at last lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_chi2'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of chi^2'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_delta'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of delta'
d[k].more_doc = ''

k = k + 1
d[k].name = 'vis_btot'
d[k].class = 'hsi_memvis'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of btot, the total brightness'
d[k].more_doc = ''

; Info parameters for hsi_mem_sato
k = k + 1
d[k].name = 'sato_lambda'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_iter'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'int'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of iterations at last lambda'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_chi2'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of chi^2'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_delta'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of delta'
d[k].more_doc = ''

k = k + 1
d[k].name = 'sato_btot'
d[k].class = 'hsi_mem_sato'
d[k].main_object = 'Image'
d[k].type = 'float'
d[k].units = 'N/A'
d[k].default = '--'
d[k].range = ''
d[k].control_or_info = 'Info'
d[k].level = 'N/A'
d[k].purpose = 'Final value of btot, the total brightness'
d[k].more_doc = ''


d = d[0:k]
end
