;+
; Name: hsi_params_write_pro
;
; Project:  HESSI
;
; Purpose: Write procedure hsi_params which defines structure with information
;	for all HESSI control and info parameters
;
; Method:  This routine generates the code in hsi_params.pro.
;	It  examines the routines in the HESSI branch of the
;	ssw tree, and attempts to find all of the control and info parameters used.
;	Then it looks at the structure defined in hsi_params_manual for
;	information that can't be generated automatically (like descriptions).  From those
;	two sources, all of the known information is accumulated, and the routine hsi_params
;	is written to create a structure with that information.
;
;	After calling hsi_params_write_pro to write the hsi_params.pro routine, you should
;	call hsi_params_write_htm.  hsi_params_write_htm will hsi_params to construct the
;	structure with all of the parameter information and generate html documentation for them.
;
;	These programs should be run periodically to keep the documentation up-to-date.
;	
;	See hsi_params_explanation.txt for a full explanation of how to update the tables.
;
; Written:  Kim Tolbert Nov 2001
; Modifications:
;    29-Jan-03 Kim Prepended $ to HESSI_PATH in hsi_loc_file call
;    07-May-09 Kim Added mem_njit and vis_fwdfit
;    29-Oct-2010, Kim. Added uv_smooth.  Also, for info parameters in hsi_image_alg class,
;      modify structure to add prefix to each tag for all algs.
;
;-


; break long text into strings with length <=60
function hsi_break_string, text, name

out_text = str2lines(text, length=60)
phead = "d[k]." + name + " = '"
ptail = "'"
nlines = n_elements(out_text)
if nlines gt 1 then begin
	phead = [phead, replicate("    '", nlines-1)]
	ptail = [replicate(" ' + $",nlines-1), ptail]
endif
return, phead + out_text + ptail
end

pro hsi_get_ci_params, fullfiles, c_or_i, list

skip_files = ['hsi_gse', 'hsi_grid', 'hsi_as_control', $	; skip files containing these strings
	'hsi_qlookrateperdet', 'hsi_qlook_summary_page', $
	'hsi_image_file_info', 'hsi_multi_image', 'hsi_image_cube', $
	'hsi_image_movie', 'hsi_image_info', 'hsi_image_control', $
	'hsi_image_client', 'hsi_fastrate', 'hsi_spectrum_fitspaste', 'hsi_fwdfit_parameters'  ]

skip_params = ['dim1_ids', 'dim1_unit', 'ut_ref', $  ; skip these params
	'n_time_intv', 'time_intv', 'n_sample']

break_file, fullfiles, disk, dir, filename, ext

; same file may be in regular directories and atest.  Use atest version.
q = find_dup(filename)
if q[0] ne -1 then begin
	for i=0,n_elements(q)-1 do begin
		w = where(filename eq filename[q[i]], count)
		if count gt 2 then message,'Aborting  - more than two files called ' + filename[q[i]]
		z = where (strpos(fullfiles(w), 'atest') ne -1, count)
		if count eq 0 then message,'Aborting - two files with same name, but neither in atest ' + fullfiles[w]
		if count eq 2 then message,'Aborting - two files with same name, both in atest ' + fullfiles[w]
		rem = z[0] eq 0 ? 1 : 0
		rem_ind = append_arr(rem_ind, w[rem])
	endfor
	remove, rem_ind, fullfiles
	break_file, fullfiles, disk, dir, filename, ext
endif

; Use only files whose name starts with 'hsi'
ind = where (stregex (filename, '^hsi', /boolean, /fold_case) eq 1, count)
if count ne 0 then fullfiles = fullfiles(ind)

; eliminate files to skip
for i = 0,n_elements(skip_files)-1 do begin
	ind = where (strpos(fullfiles, skip_files[i]) eq -1, count)
	if count ne 0 then fullfiles = fullfiles(ind)
endfor

break_file, fullfiles, disk, dir, filename, ext

; ok, now have files we want to use.  Get structure and class names

; structure name is the part of the file name before __define
str_names = ssw_strsplit (filename, '__define')

; class name is the part of the name before control__define (or inpars, or parameters or outpars)
; some have _control, some just control, so after removing control__define, remove trailing _ if any.
if c_or_i eq 'Control' then begin
	temp = repstr(filename, 'parameters__define', 'control__define')
	temp = repstr(temp, 'inpars__define', 'control__define')
	classes = ssw_strsplit(temp, 'control__define')
endif else begin
	temp = repstr(filename, 'outpars__define', 'info__define')
	classes = ssw_strsplit(temp, 'info__define')
endelse
q = where (strlastchar(classes) eq '_', count)
if count gt 0 then begin
	tail = ssw_strsplit (classes[q], '_', /tail, head=head)
	classes[q] = head
endif

; image alg parameters have name changed by adding prefix
image_alg_classes = ['hsi_clean', 'hsi_mem_sato', 'hsi_memvis', 'hsi_pixon', 'hsi_forwardfit', $
  'hsi_vis_fwdfit', 'hsi_fwdfit', 'hsi_mem_njit', 'hsi_uv_smooth']
image_alg_prefixes = ['clean', 'sato', 'vis', 'pixon', 'ff', 'vf', 'vf', 'nj', 'uv']

; get correspondence between each class name and the main object it is used for
main_obj_file = 'hsi_params_main_object.txt'
filename = file_search(main_obj_file, count=count)
if count eq 0 then $
	filename = hsi_loc_file (main_obj_file, path=concat_dir('$HESSI_PATH','idl\gen'), count=count, /no_dialog)
if count eq 0 then message,'No hsi_params_main_object.txt file found.  Aborting.'
object_table = strtrim( strcompress( rd_ascii(filename) ), 2)
object_table = str2cols (object_table, ' ', /unaligned)
object_table_n = fix (total(strpos(object_table, '') ne -1, 1) - 1)

dirs = expand_path('+'+get_logenv('HESSI_PATH') + '\idl')

; get structure from manually edited hsi_params, so we can keep manually edited values
hsi_params_manual, man

; Loop through the structures we've found
for i = 0, n_elements(str_names)-1 do begin
	class = classes[i]
	list = [list + $
		'', $
		'; ' + c_or_i + ' parameters for ' + class]

	; if there is an initialization function for this class, run that, otherwise just
	; make a structure from the class name
	init_file = find_files (str_names[i] + '.pro', dirs)
	run_init = init_file[0] eq '' ? 0 : 1
	cmd = run_init ? 'str = ' + str_names[i] + '()' : 'str = {' + str_names[i] + '}'
	ok = execute(cmd)

	ntags = n_tags (str)
	if ntags eq 0 then begin
		print,'No tags at all in structure ' + str_names[i] + '.  Skipping.'
		goto, skip_str
	endif
	tnames = strlowcase(tag_names (str))

	; in order to find purpose in comments, read the full file and remove comment and null lines
	; also read initialization file if any.
	lines = rd_ascii(fullfiles[i])
	lines = strnocomment(lines, /remove_nulls, /leave_inline_comments)
	if run_init then begin
		init_lines = rd_ascii (init_file[0])
		init_lines = strnocomment (init_lines, /remove_nulls, /leave_inline_comments)
	endif

	; remove tags in structure that aren't explictly listed in file (i.e. get rid of
	; those that are in structure due to an inheritance).  Get rid of comments first
	; so we aren't fooled by a tag name inside of comments.

	lines_str = strlowcase(arr2str(strnocomment(lines),delim=''))
	for j = 0,ntags-1 do $
		if strpos(lines_str, tnames[j]) eq -1 then str = rem_tag(str, tnames[j])

  ; for hsi_image_alg class, info parameters will have alg prefix added to them
  ; so take basic structure, and make new structure that is the concatenation
  ; of that structure with each alg prefixed to tag names for all algs.
  if class eq 'hsi_image_alg' then begin
    pref = get_uniq(image_alg_prefixes)
    z = tag_prefix(str,pref[0])
    for ii=1,n_elements(pref)-1 do z = join_struct(z, tag_prefix(str,pref[ii]))
    str = z
  endif
  
	ntags = n_tags (str)
	if ntags eq 0 then begin
		print,'No tags left in structure ' + str_names[i] + '.  Skipping.'
		goto, skip_str
	endif
	tnames = tag_names (str)


	;image alg classes have prefix added to name
	if is_member(class, image_alg_classes) then begin
		q = where(image_alg_classes eq class)
		newnames = tag_names( tag_prefix(str, image_alg_prefixes[q]) )
	endif else newnames = tnames

	if ntags gt 0 then begin
		for j = 0,ntags-1 do begin
			name = strlowcase(tnames[j])
			if is_member(name, skip_params) then goto, skip_param
			newname = strlowcase(newnames[j])
			sz = size(str.(j),/st)
			type = sz.type_name
			if sz.n_dimensions gt 0 then begin
				dim = strtrim(sz.dimensions,2)
				type = type + '('
				for m = 0,sz.n_dimensions-1 do type = type + dim[m] + ','
				type = strmid(type, 0, strlen(type)-1) + ')'
			endif
			type = strlowcase(type)

			; only try to get default if run_init is set (we ran an initialization file)
			; can only list default if data type is not ptr, obj, or str, and n_elements is 1
			default = ''
			if run_init then begin
				can_do = (sz.type gt 0 and sz.type lt 8) or (sz.type gt 11)
				if can_do and (sz.n_dimensions eq 0) then begin
					default = str.(j)
					; if type is byte, make it fixed before converting to string or it's wrong
					if sz.type eq 1 then default = fix(default)
					default = strlowcase(strtrim(default, 2))
				endif
			endif

			; try to get purpose from comment in line.  first make sure parameter was listed in
			; file (could have been inherited).  If ran an initialization routine, then let that
			; comment override comment in __define file.
			purpose = ''
			ind = stregex(lines, name, /fold_case, /boolean)
			q = (where(ind ne 0, count))[0]
			;if class eq 'hsi_obssummrate' then stop
			if count gt 0 then if strpos(lines[q],';') ne -1 then $
				purpose = strtrim(ssw_strsplit (lines[q], ';', /tail),2)
			if purpose[0] eq '' and run_init then begin
				ind = stregex(init_lines, name, /fold_case, /boolean)
				q = (where(ind ne 0, count))[0]
				if count gt 0 then if strpos(init_lines[q],';') ne -1 then $
					purpose = strtrim(ssw_strsplit (init_lines[q], ';', /tail),2)
			endif
			if purpose[0] ne '' then purpose = repchar (purpose[0], "'", '"')

			; a few params in spectrum_control also apply to lightcurve, but not all
			sp_add_to_lc = ['sp_semi_calibrated', 'sp_data_structure', 'sp_data_unit']
			q = where (object_table[0,*] eq class, count)
			if count gt 0 then begin
				q = q[0]
				main_object = arr2str( object_table[1:object_table_n[q], q], ', ' )
				if is_member(newname,sp_add_to_lc) then main_object=main_object + ', Lightcurve'
			endif else message,'class ' + class + ' not found in object_table.'

			units = ''
			range = ''
			level = c_or_i eq 'Control' ? 'Expert' : 'N/A' ;default for control is expert
			more_doc = ''

			; use values from previous hsi_params file if they're there.
			q = where (man.name eq newname and man.class eq class, count)
			if count gt 0 then begin

				;first print message if default found from .pro's is different from what's in
				;hsi_params_manual
				;print,class, name, default
				if is_number(default) then if float(default) ne float(man[q[0]].default) then begin
					print, 'Default for ' + newname + '(class=' + class + ') has changed from ' + $
						man[q[0]].default + ' to ' + default + '  @@@@@@@@@'
				endif

				units = man[q[0]].units
				default = man[q[0]].default
				range = man[q[0]].range
				level = man[q[0]].level
				purpose = man[q[0]].purpose
				more_doc = man[q[0]].more_doc
			endif else begin
				w = where (man.name eq newname,c)
				if c eq 0 then begin
					print,''
					print,'Found new parameter:  ' + newname + '   class:  '+class+'   !!!!!!!!!!!!!!!!!!!!!!!!!!'
					print,' '
				endif else begin
					print,''
					print,'Parameter in a different class  !!!!!!!!!!!!'
					print,'  Parameter: ' + newname + '   New class: ' + class + ''
					print,'  Previous class(es): ' + arr2str(man[w].class, ', ')
					print,''
				endelse
			endelse

			; break long purpose into strings with length <=60
			purpose = repstr (purpose, "'", "''")
			more_doc = repstr (more_doc, "'", "''")
			purpose = hsi_break_string(purpose, 'purpose')
			more_doc = hsi_break_string(more_doc, 'more_doc')

			list = [list + $
			'', $
			'k = k + 1', $
			"d[k].name = '" + newname + "'", $
			"d[k].class = '" + class + "'", $
			"d[k].main_object = '" + main_object + "'", $
			"d[k].type = '" + type + "'", $
			"d[k].units = '" + units + "'", $
			"d[k].default = '" + default + "'", $
			"d[k].range = '" + range + "'", $
			"d[k].control_or_info = '" + c_or_i + "'", $
			"d[k].level = '" + level + "'", $
			purpose, $
			more_doc, $
			'' ]
		skip_param:
		endfor
	endif

skip_str:

endfor

end

;-----

pro hsi_params_write_pro

dirs = expand_path('+'+get_logenv('hessi_path') + '\idl')
c_files = find_files('*control__define.pro', dirs)
more_files = find_files('*inpars__define.pro', dirs)
if more_files[0] ne '' then c_files = [c_files, more_files]
more_files = find_files('*parameters__define.pro', dirs)
if more_files[0] ne '' then c_files = [c_files, more_files]

i_files = find_files('*info__define.pro', dirs)
more_files = find_files('*outpars__define.pro', dirs)
if more_files[0] ne '' then i_files = [i_files, more_files]

list = [';+', $
	'; Name: hsi_params', $
	'; Purpose: Define structure with information for all HESSI control and info parameters', $
	'; Calling Sequence: hsi_params, param_struct', $
	'; ', $
	'; NOTE:  THIS ROUTINE IS GENERATED AUTOMATICALLY BY HSI_PARAMS_WRITE_PRO.  Do not edit.', $
	';-', $
	'', $
	'pro hsi_params, d', $
	'', $
	'k = -1', $
	'd = replicate({hsi_params_str}, 2000)', $
	'']

hsi_get_ci_params, c_files, 'Control', list
hsi_get_ci_params, i_files, 'Info', list

list = [list, $
	'', $
	'd = d[0:k]', $
	'end']

prstr, list, file='hsi_params.pro'
prstr, list, file='hsi_params_copy.pro'

;check whether there are some parameters in hsi_params_manual that are no longer needed
resolve_routine, 'hsi_params'
hsi_params,d & hsi_params_manual,dm
n = n_elements(d) & nm = n_elements(dm)
print,'There are ', n, ' in new hsi_params, ', nm, ' in hsi_params_manual.'
;if nm gt n then begin
	for i = 0,nm-1 do begin
		q = where (dm[i].name eq d.name and dm[i].class eq d.class, count)
		if count eq 0 then print, dm[i].name + ' in class ' + dm[i].class + ' should be removed from hsi_params_manual.'
		if count gt 1 then print, strtrim(count,2) + ' Duplicates.  ' + dm[i].name + ' in class ' + dm[i].class
	endfor
;endif

end

