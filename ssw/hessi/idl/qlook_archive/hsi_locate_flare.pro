;+
;	Pro twod_subs,ij,ipix,jpix,i,j
; Returns (i,j) the two-d subscripts of position ij in an (ipix,jpix) matrix
;-
Pro Twod_subs, ij, ipix, jpix, i, j

   j = ij/ipix
   i = ij-j*ipix

   Return
END
;---------------------------------------------------------------------------
; Document name: hsi_locate_flare.pro
; Created by:    Jim McTiernan, May 14, 2002
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
; NAME:
;       HSI_LOCATE_FLARE
; PURPOSE: 
;       Given the time_range and energy_band, returns the flare
;       position
; CATEGORY:
;       flare_list
; CALLING SEQUENCE: 
;       success = hsi_locate_flare(time_range, energy_band)
; INPUTS:
;       time_range= the time interval for position finding
;       energy_band= the energy band
; OUTPUTS:
;       success= 1 if it worked, 0 if not
;       xy= the flare location in arcsec from sun center
; KEYWORDS:
;       scale_factor = k, see step 4 below
;       quiet = run quietly 
;       spin_axis (output) = the spin axis for the interval
;       plot = if set, put up a plot
;       test_pmtras = If set, do a test for good PMTRAS solution
;       image_obj = the image_object used
;       return_image_obj = Only return the image_obj keyword, if this
;                          is set.
; PROCEDURE:
;  Here is a first cut at the promised outline of an hsi_flare_finder
;  algorithm.  It's purpose is to determine the approximate location of the
;  brightest real source on the full disk, hopefully distinguishing this
;  from any on-axis artifact or mirror source.
;
;  1. Choose an appropriate time/energy window.  Longer time windows (~1
;  minute) are better since it helps to smear out the mirror source.
;  2. Determine the center of rotation from the aspect solution.
;  3. Using 16 arcsecond pixels, make a 128x128 backprojection map using
;  subcollimator 9 only. 
;  4. Excluding any pixel within k*FWHM of the rotation center, where k is
;  a parameter (~0.5 to 1) and FWHM is the FWHM of the subcollimator,
;  determine the location of the brightest pixel in the remaining map.  
;  5. Repeat steps 3 and 4 for subcollimators 8,7,6 and 5.
;  6. Determine XY0 = [MEDIAN(x), MEDIAN(y)], where x,y are each 5-element
;  vectors containing the map peak locations.  This will give a robust
;  centroid, provided at least 3 of the map peaks are correct.
;  7. Select those map peaks that are within a radial distance, R, from XY0
;  where R is a parameter (~2 arcminutes).
;  8. If there are fewer than 3 maps satisfing this condition, algorithm
;  fails and a failure code can be returned.
;  9. Otherwise, determine the average, X,Y of the selected map peaks. 
;  This is the result.
;
;  In the calling program, if the algorithm fails, you might just redo the
;  call to hsi_flare_finder using a longer integration time (MIN(entire
;  flare, 5 minutes).  If this case fails too, you should just set flare
;  location = 0,0 and label the flare catalog as 'no location available' or
;  somesuch.
;
;  The main differences between this and our current scheme is the use of 5
;  subcollimators, longer integration times, suppressing the on-axis
;  source, and testing for internal agreement.  There are variations on
;  this theme, (eg using subcollimators 7,8,9 and requiring 2/3 to be
;  consistent), comparing multiple times, etc, but this first cut seemed a
;  reasonable mix of complexity and promise.  
;  gordon
; HISTORY:
;       Version 1, May 14, 2002, 
;           jmm, jimm@ssl.berkeley.edu
;       Switched the order of images, put check for good position
;       inside the loop, if it succeeds early, it will return,
;       jmm,17-feb-2003
;       Added a catch procedure for bug handling, 6-mar-2003
;       Fixed bug that occasionally returns the spin axis as the
;       answer, jmm, 13-nov-2003
;       Added the spin_axis keyword, to return the spin axis
;       31-dec-2003, jmm, added image_obj input and output
;       2-feb-2005, jmm, if test_pmtras is set, will return the
;       position, but with suspect_position set to 1
;       Changed npix_test variable to use fwhm[coll0[2]]/2, or detector
;       7, instead of detector 0, which results in 360 arcseconds for
;       the test. Now sources must be within about 60
;       arcsec. jmm,23-dec-2006
;       Fixed case for no image -- catch statement was not picking this 
;       up, jmm, 20-oct-2010
;       Guards against index overflow caused by spin axis calculations
;       too near to the edge of the image, jmm, 25-oct-2010
;-
Function hsi_locate_flare, time_range, energy_band, xy, spin_axis = avp, $
                           scale_factor = scale_factor, quiet = quiet, $
                           plot = plot, test_pmtras = test_pmtras, $
                           image_obj = image_obj, confirm = confirm, $
                           suspect_solution = suspect_solution, $
                           _extra = _extra
   
  success = 0b
  jfile_open = 0b
  suspect_solution = 0b
  avp = -1
;Error handler, this should be able do deal with pmtras stops
  err = 0
  catch, err
  If(err Ne 0) Then Begin
    catch, /cancel
    Print, 'Error'
    help, /last_message, output = err_msg
    For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
;If the test_pmtras journal file is open, close it, and remove it
    If(jfile_open Eq 1) Then Begin
      journal
      jfile_open = 0b
    Endif
    If(is_string(jfile)) Then Begin
      cmd = '/bin/rm '+jfile
      message, /info, 'Spawning: '+cmd
      spawn, cmd
    Endif
    print, 'Returning 0'
    xy = [0, 0]
    Return, 0b
  Endif

  If(keyword_set(quiet)) Then notquiet = 0 Else notquiet = 1
  If(keyword_set(scale_factor)) Then k = scale_factor Else k = 1
  grid_pars = hsi_grid_parameters()
  fwhm = grid_pars.pitch/2.0
  If(notquiet) Then Begin
    message, /info, 'Time Range:'
    For j = 0, 1 Do print, anytim(/ccsds, time_range[j])
  Endif
  tr0 = anytim(time_range)
;Check for a suspect roll solution:
  suspect_solution = 0b
  If(keyword_set(test_pmtras)) Then Begin
;first test for roll_db solution
    pmtras_solution = hsi_pmtras_lookup(tr0, ROLL_STATUS = roll_status)
    If(roll_status Eq 0) Then Begin
      test_roll_tr = [tr0[0]-1400.0, tr0[1]+1400.0]
;You need a packet object
      opak = obj_new('hsi_packet')
      pak = opak -> getdata(app_id = 154, obs_time_interval = test_roll_tr)
      temp_extra = {packet_time_range:test_roll_tr}
    Endif Else Begin
      test_roll_tr = tr0
    Endelse
    jfile = '/tmp/locate_flare_pmtras_test_'+time2file(tr0[0])
;note that this will only allow two jobs, for a given time interval on
;the same machine, which is ok by me
    If(file_exist(jfile)) Then Begin
      jfile = jfile+'_xxx'
      If(file_exist(jfile)) Then message, jfile+' Exists'
    Endif
    journal, jfile
    jfile_open = 1b
    If(roll_status Eq 0) Then Begin
      pmtras_analysis, packet_object = opak, pmtras_solution = sol, $
        _extra = temp_extra
    Endif Else Begin
      pmtras_analysis, rdb_time_range = test_roll_tr, pmtras_solution = sol
    Endelse
    journal
    jfile_open = 0b
;Information about the pmtras solution is in the file, read it
    pxp = findfile(jfile)
    If(is_string(pxp)) Then Begin
      mess = rd_tfile(pxp)
      nmess = n_elements(mess)
      For j = 0l, nmess-1 Do Begin
        print, mess[j]
        suspect_test0 = strpos(mess[j], 'Roll solution is suspect')
        suspect_test1 = strpos(mess[j], 'ROLL SOLUTION MUST EXTRAPOLATE')
        If(suspect_test0[0] Ne -1) Or (suspect_test1[0] Ne -1) $
          Then suspect_solution = 1b
      Endfor
    Endif
    cmd = '/bin/rm '+jfile
    message, /info, 'Spawning: '+cmd
    spawn, cmd
    If(Not keyword_set(bad_roll_ok)) Then Begin
      If(is_struct(sol) Eq 0) Then Begin
        message, 'Bad Roll Solution --- No Position Found'
      Endif
    Endif
    If(Obj_valid(opak)) Then obj_destroy, opak
  Endif
;Ok, start with images
  npix = 128
  gs = 16. + fltarr(2)          ; pixel_size
  r0_offset = 10.*npix*gs[0]
  xyoffset = [0.0, 1.0]
  xy = [0.0, 0.0]
;Grid points
  xgrid = gs[0]*(findgen(npix)-npix/2)+gs[0]/2.0
  ygrid = gs[1]*(findgen(npix)-npix/2)+gs[1]/2.0
;Use 5 detectors
  coll0 = [8, 7, 6, 5, 4, 3]
  ncoll0 = n_elements(coll0)
; create image object here
  If(obj_valid(image_obj)) Then fso = image_obj $
  Else fso = hsi_image()
  fso -> set, xyoffset = xyoffset, $
;    r0_offset = r0_offset, $
    pixel_size = gs, $
    image_dim = [npix, npix], $
    im_time_interval = tr0, $
    energy_band = energy_band, $
    use_auto_time_bin = 0b
;  fso -> set, obs_time_interval = tr0
;  fso -> set, time_range = [0, tr0[1]-tr0[0]]
  If(Not keyword_set(plot)) Then fso -> set_no_screen_output
  maxqs = fltarr(2, ncoll0) & maxqs[*] = -9999
  If(notquiet) Then Begin
    message, /info, 'FLARE TIME RANGE:'
    print, anytim(tr0[0], /ccsds), ' -- ', anytim(tr0[1], /ccsds)
  Endif
  npix_test = fwhm[coll0[2]]/gs[0]
  npix_test = npix_test > 2
  For j = 0, ncoll0-1 Do Begin
    det_index_mask = bytarr(18)
    det_index_mask[coll0[j]] = 1
    xyfs = fso -> getdata(/weight, det_index_mask = det_index_mask, $
                          _extra = _extra)
    xyfs = xyfs > 0.0
    If(total(abs(xyfs)) Le 0.0) Then Begin 
      message, /info, 'No Image for detector: '+string(coll0[j]+1)
      Continue
    Endif
;zero out the image_axis, this call to hsi_aspect_solution may change
;I already have the aspect solution only need to do this once:
    If(j eq 0) Then Begin
      fsa = fso -> get(class_name = 'hsi_aspect_solution', /object_ref)
      ppp = fsa -> getdata()
      If(is_struct(ppp)) Then Begin
        avp = rhessi_get_spin_axis_position(tr0, aspect_obj = fsa)
        If(n_elements(avp) Eq 1 And avp[0] Eq -1) Then Begin
          message, 'Bad Spin Axis --- No Position Found'
        Endif
      Endif Else Begin
        message, 'Bad Aspect Solution --- No Position Found'
      Endelse
      If(notquiet) Then print, 'SPIN AXIS:', avp
    Endif
    xyaxis0 = max(where(find_ix(avp[0], xgrid) eq 1))
    xyaxis1 = max(where(find_ix(avp[1], ygrid) eq 1))
    xyaxis = [xyaxis0, xyaxis1] > 1 ;this is necessary for -1 cases
    xyaxis = xyaxis < (npix-2)
;Find the maximum, exclude points which are k*fwhm[coll0[j]]
    npix_out = k*fwhm[coll0[j]]/gs[0]
    npix_out = npix_out > 1
    xyfs0 = xyfs
    xx0 = (xyaxis[0]-npix_out) > 0 ;jmm, 25-oct-2010
    xx1 = (xyaxis[0]+npix_out) < (npix-1)
    yy0 = (xyaxis[1]-npix_out) > 0
    yy1 = (xyaxis[1]+npix_out) < (npix-1)
    xyfs[xx0:xx1, yy0:yy1] = 0
;New, 9-oct-2006, jmm, try hsi_map_evaluator here
    If(total(xyfs) Gt 0) Then Begin
      (fso -> getstrategy()) -> setdata, xyfs
      hsi_map_evaluator, fso, output = otpx
      If(is_struct(otpx)) Then maxqs[*, j] = otpx.xypeak
    Endif
    If(notquiet) Then print, maxqs
;Check what is closest
    If(j Ge 2) Then Begin
      okimg = bytarr(j+1)
      For k = 0, j Do Begin
        If(maxqs[0, k] Gt -9990 And maxqs[1, k] Gt -9990) Then okimg[k] = 1b
      Endfor
      ok0 = where(okimg Eq 1, nok0)
      If(nok0 Gt 2) Then Begin
        xy0 = [median(maxqs[0, ok0]), median(maxqs[1, ok0])]
        x0 = xy0[0]-npix_test*gs[0]
        x1 = xy0[0]+npix_test*gs[0]
        y0 = xy0[1]-npix_test*gs[1]
        y1 = xy0[1]+npix_test*gs[1]
        okfit_xy = bytarr(j+1)
        For k = 0, j Do Begin
          If(maxqs[0, k] Gt -9990 And maxqs[1, k] Gt -9990) Then Begin
            If(maxqs[0, k] Ge x0 And maxqs[0, k] Le x1 And $
               maxqs[1, k] Ge y0 And maxqs[1, k] Le y1) Then okfit_xy[k] = 1b
          Endif
        Endfor
        ok = where(okfit_xy Eq 1, nok)
        If(nok Ge 3) Then Begin
          success = 1b
          maxqs_out = total(maxqs[*, ok], 2)/nok
          xy = [maxqs_out[0], maxqs_out[1]]
          message, /info, 'Successful'
          ptim, time_range
          print, xy
          goto, get_out
        Endif Else Begin
          success = 0b
          xy = [0.0, 0.0]
          message, /info, 'Unsuccessful'
          ptim, time_range
        Endelse
      Endif
    Endif
  Endfor
  get_out: 
  If(success Eq 1 And keyword_set(confirm)) Then Begin   ;one last check
;need det 9 again
    det_index_mask = bytarr(18)
    det_index_mask[8] = 1
    fso -> set, xyoffset = xy & fso -> set, pixel_size = [2.5, 2.5]
    xyfs = fso -> getdata(det_index_mask = det_index_mask)
    yyy = hsi_source_confirm(fso, threshold = 0.45)
    If(yyy.test Eq 0) Then Begin
      message, /info, 'Position Confimration Failed'
    Endif Else message, /info, 'Position Confimration succeeded'
    print,  yyy
  Endif
  return, success
END


;---------------------------------------------------------------------------
; End of 'hsi_locate_flare.pro'.
;---------------------------------------------------------------------------
