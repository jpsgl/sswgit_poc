;+
;NAME:
; excess_intervals
;PURPOSE:
; find and flag intervals with count rates above a threshold. 
;CALLING SEQUENCE:
; excess_intervals, data, threshold, fcount, $
;                         interval_flag, Quiet = quiet, $
;                         min_pts_between = min_pts_between
;INPUT:
; data = Count rate
; threshold = the threshold to measure by, an
;             array of n_elements(data).
;OUTPUT:
; fcount = number of events, solar
; interval_flag = lonarr(n_elements(data)), set to zero if
;                 there is no emission, set to N for the
;                 Nth set of contiguous points with excess
;                 emission.
;KEYWORDS:
; quiet = if set, run quietly
; min_pts_between = the min number of points between flares
;HISTORY:
; Hacked from hsi_excess_intervals,pro 19-jun-2006, jmm
;-
Pro excess_intervals, data0, threshold0, fcount, $
                      interval_flag, Quiet = quiet, $
                      min_pts_between = min_pts_between, $
                      threshold_tweak = threshold_tweak, $
                      _extra = _extra

  If(keyword_set(min_pts_between)) Then mpb = min_pts_between $
  Else mpb = 0
  fcount = 0l
  n = n_elements(data0)
  interval_flag = lonarr(n)
  tdata = data0 & tdata[*] = 0  ;this will be data over the threshold
  If(keyword_set(threshold_tweak)) Then twk = threshold_tweak Else twk = 1.2
;Here we will drop the data gaps (data gt 0)
  okx = where(data0 Gt 0, nokx)
  If(okx[0] Ne -1) Then Begin
    data = data0[okx]
    threshold = threshold0[okx]
  Endif Else Begin
    If(Not keyword_set(quiet)) Then message, /info, 'No Data'
    Goto, im_done
  Endelse
;Find excess counts, ratio
  c1 = (data Gt threshold)
  ww = where(c1)
  If(ww[0] Eq -1) Then Begin
    If(Not keyword_set(quiet)) Then $
      message, /info, 'No excess counts'
    Goto, IM_DONE
  Endif
;if data is above the threshold, then it
;counts until it's below the original threshold
  j = ww[0]                     ;1st point
 ;Compare with this value
  loopx: 
  test0 = data Gt threshold[j]*twk
  fcount = fcount+1
  temp_st_en, test0[j:*], st_j, en_j, ok = ok ;if you are here, there must be data, not necessarily
  If(ok[0] Eq -1) Then Begin    ;you are finished, what do you do now?
    Goto, im_done
  Endif
  nintv = n_elements(st_j)
  If(nintv Gt 1) Then Begin
;check to see if the next interval is too close
    i1 = 0
    temp_loop: 
    i1 = i1+1
    If(st_j[i1]-en_j[0] Lt mpb) Then Begin
      en_j[0] = en_j[i1]
      If(i1 Lt nintv-1) Then Goto, temp_loop
    Endif
  Endif
;only the first interval
  st_j = st_j[0]+j
  en_j = en_j[0]+j
;Fill flag array and keep going, be careful to put the flags in the
;right place relative to the original data set
  interval_flag[okx[st_j]:okx[en_j]] = fcount
 ;Go to the next possible threshold
  If(en_j Lt nokx-1) Then Begin
;Note that data[en_j+1] should be LE threshold, but not necessarily
    j = en_j+1
    If(j Lt nokx-1) Then Begin
      ww = where(c1[j:*])
      If(ww[0] Ne -1) Then Begin
        j = j+ww[0]
        Goto, loopx
      Endif
    Endif
  Endif
;Reset fcount
  im_done: 
  fcount = max(interval_flag)
;get threshold-subtracted data
  If(fcount Gt 0) Then Begin
    For l = 1, fcount Do Begin
      ssl = where(interval_flag Eq l,  nssl)
      If(nssl Gt 0) Then tdata[ssl] = data0[ssl]-threshold0[ssl[0]]
    Endfor
  Endif
  Return
End
