;---------------------------------------------------------------------------
 ; Document name: hsi_calib_eventx__define.pro
 ; Created by:    Andre Csillaghy, January 10, 2000
 ;
 ; Last Modified: Tue May  9 13:25:04 2000 (csillag@soleil)
 ;---------------------------------------------------------------------------
 ;
 ;+
 ; PROJECT:
 ;       HESSI
 ;
 ; NAME:
 ;       HSI_CALIB_EVENT__DEFINE
 ;
 ; PURPOSE:
 ;
 ;
 ; CATEGORY:
 ;
 ;
 ; CALLING SEQUENCE:
 ;       hsi_calib_event__define,
 ;
 ; INPUTS:
 ;
 ;
 ; OPTIONAL INPUTS:
 ;       None.
 ;
 ; OUTPUTS:
 ;       None.
 ;
 ; OPTIONAL OUTPUTS:
 ;       None.
 ;
 ; KEYWORDS:
 ;       None.
 ;
 ; COMMON BLOCKS:
 ;       None.
 ;
 ; PROCEDURE:
 ;
 ; RESTRICTIONS:
 ;       None.
 ;
 ; SIDE EFFECTS:
 ;       None.
 ;
 ; EXAMPLES:
 ;
 ;
 ; SEE ALSO:
 ;
 ; HISTORY:
 ;       Version 1, January 10, 2000, A Csillaghy, csillag@ssl.berkeley.edu
 ;  8-jun-2001, richard.schwartz@gsfc.nasa.gov
 ;   added flux_var - this factor combines detector efficiency
 ;   with intrinsic source variation across time interval
 ;  16-mar-2004, ras,  added fresh documentation
 ;	13-apr-2007, ras, expand for harmonics, modamp3, phase_map_ctr3
 ;-


 PRO hsi_calib_eventx__define
 ;IDL> help,{hsi_calib_eventx},/st
;** Structure HSI_CALIB_EVENT, 11 tags, length=44, data length=44:
;   DX              FLOAT          0.000000   - arcseconds
;   DY              FLOAT          0.000000   - arcseconds
;   ROLL_ANGLE      FLOAT          0.000000   - radians
;   MODAMP          FLOAT          0.000000   - number normally - 0-1.
;   PHASE_MAP_CTR   FLOAT          0.000000   - radians
;   GRIDTRAN        FLOAT          0.000000   - number normally - 0-1.
;   FLUX_VAR        FLOAT          0.000000   - number normally - 0-1.
;   BACKGROUND      FLOAT          0.000000   - counts, same as counts, number
;   TIME            LONG                 0    - in binary microseconds X time_unit
;   COUNT           FLOAT          0.000000   - counts, number,
;   LIVETIME        FLOAT          0.000000   - fraction 0.-1.

 dummy =  {hsi_calib_eventx, $
 dx: 0.0, $
 dy: 0.0, $
 roll_angle: 0.0, $
 modamp: fltarr(3), $


 phase_map_ctr: fltarr(3), $
 gridtran: 0.0, $
 flux_var: 0.0, $
 background: 0.0, $
 ;Inherits hsi_harm_bin,$
 INHERITS hsi_time_bin }

 END


 ;---------------------------------------------------------------------------
 ; End of 'hsi_calib_event__define.pro'.
 ;---------------------------------------------------------------------------

