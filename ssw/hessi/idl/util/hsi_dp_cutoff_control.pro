;+
;
; Name: hsi_dp_cutoff_control()
;
; Purpose:
;	Function with parameters used to determine the
;	minimum datagap duration based on rate, r, in /sec
;
; Inputs:
;	None
;
; Call:
;	par = hsi_dp_cutoff_control()
;
; History
;	22-feb-2007, richard.schwartz@gsfc.nasa.gov
;-



function hsi_dp_cutoff_control

;temp = {hsi_dp_cutoff, dp_cutoff_max:0.0, dp_cutoff_min:0.0, dp_cutoff_coeff:0.0, dp_cutoff_xp:0.0}
return, {hsi_dp_cutoff, 0.050, .000800, 4.5, -0.9}

end