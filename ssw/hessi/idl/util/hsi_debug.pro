;---------------------------------------------------------------------------
; Document name: hsi_debug.pro
; Created by:    Andre Csillaghy, February 21, 2002
;
; Last Modified: Thu Feb 21 17:27:30 2002 (csillag@delsol)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HSI_DEBUG
;
; PURPOSE:
;
;
; CATEGORY:
;
;
; CALLING SEQUENCE:
;       hsi_debug [,level]
;
; INPUTS:
;
;
; OPTIONAL INPUTS:
;       LEVEL - debug level to set (default is 10)
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORDS:
;       None.
;
; COMMON BLOCKS:
;       None.
;
; PROCEDURE:
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; EXAMPLES:
;
;
; SEE ALSO:
;
; HISTORY:
;       Version 1, February 21, 2002,
;           A Csillaghy, csillag@ssl.berkeley.edu
;
;  25-Oct-2002, Kim - added level argument.  Previously always set to 10.
;
;-
;

PRO hsi_debug, level

if not exist(level) then level = 10
setenv, 'SSW_FRAMEWORK_DEBUG=' + trim(level)
setenv, 'SSW_FRAMEWORK_VERBOSE=' + trim(level)

END


;---------------------------------------------------------------------------
; End of 'hsi_debug.pro'.
;---------------------------------------------------------------------------
