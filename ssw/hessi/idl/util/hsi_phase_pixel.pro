
;+
; PROJECT:
;	HESSI
; NAME:
;	HSI_PHASE_PIXEL
; PURPOSE:
;	This function combines the pixel phase and map center phase as
;	a function of angle and applies it to a cosine function.
; CATEGORY:
;	HESSI, IMAGE
;
; CALLING SEQUENCE:
;
;
; CALLS:
;
; INPUTS:
;	Roll_bin - calibrated event list binned in roll angle for 1 collimator
;	and 1 harmonic.
;	Grid_angle - orientation angle in radian of collimator
;	pixel - 2x npixel map in asec
;	harm_ang_pitch - angular pitch of grid in asec divided by harmonic number.
;
; OPTIONAL INPUTS:
;	NOBYTE - doesn't bytescale returned values.
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	Version 1, richard.schwartz@gsfc.nasa.gov
;	May 17, 1999.
;-
function hsi_phase_pixel, roll_angle, grid_angle, pixel, harm_ang_pitch
  
  twopi = !pi * 2.0

  phase_pixel = 	(twopi/harm_ang_pitch)* ( $
    (pixel[*,0])# cos(roll_angle - grid_angle) - $
    (pixel[*,1])[*]# sin(roll_angle - grid_angle) )
  return, phase_pixel
end





