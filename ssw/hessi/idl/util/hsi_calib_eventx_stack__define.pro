;---------------------------------------------------------------------------
 ; Document name: hsi_calib_eventx_stack__define.pro
 ; Created by:    RAS, MAY 21, 2003
 ;
 ;
 ;---------------------------------------------------------------------------
 ;
 ;+
 ; PROJECT:
 ;       HESSI
 ;
 ; NAME:
 ;       HSI_CALIB_EVENTX_STACK__DEFINE
 ;
 ; PURPOSE:
 ;
 ;
 ; CATEGORY:
 ;
 ;
 ; CALLING SEQUENCE:
 ;       help,{hsi_calib_eventx_stack}
 ;
 ; INPUTS:
 ;
 ;
 ; OPTIONAL INPUTS:
 ;       None.
 ;
 ; OUTPUTS:
 ;       None.
 ;
 ; OPTIONAL OUTPUTS:
 ;       None.
 ;
 ; KEYWORDS:
 ;       None.
 ;
 ; COMMON BLOCKS:
 ;       None.
 ;
 ; PROCEDURE:
 ;
 ; RESTRICTIONS:
 ;       None.
 ;
 ; SIDE EFFECTS:
 ;       None.
 ;
 ; EXAMPLES:
 ;
 ;
 ; SEE ALSO:
 ;
 ; HISTORY:
 ;     Created 21-may-2003, ras, based on G Hurford and E Schmall stackers
 ;		20-apr-2007, ras, included harmonic info for modamp and phase_map_ctr
 ;-


 PRO hsi_calib_eventx_stack__define

 dummy =  {hsi_calib_eventx_stack, $
 roll_angle: 0.0, $
 modamp: fltarr(3), $
 phase_map_ctr: fltarr(3), $
 gridtran: 0.0, $
 flux_var:0.0, $
 background: 0.0, $
 count: 0.0, $
 livetime: 0.0, $
 gap: 0b  }

 END


 ;---------------------------------------------------------------------------
 ; End of 'hsi_calib_eventx__define.pro'.
 ;---------------------------------------------------------------------------

