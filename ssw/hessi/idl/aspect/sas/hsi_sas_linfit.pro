;+
; PROJECT:
;       HESSI
;
; NAME:
;       hsi_sas
;
; PURPOSE:
;       Calculate the pointing in S/C coordinate system.
;       i.e. position of Sun center with respect to imaging axis.
;
; CATEGORY:
;       aspect
;
; CALLING SEQUENCE:
;
;
; INPUTS:
;       LIMBP  list of measured limb pixels
;
; OPTIONAL INPUTS:
;       CNTL_LEVEL level of quality controll
;
; OUTPUTS:
;       XYSC   pointing in the imaging coordinate system
;       TIME   array of time offsets in units of 2^(-7) sec.
;       T0     time of first pointing vector
;
; OPTIONAL OUTPUTS:
;       QUALITY
;
; KEYWORDS:
;       None.
;
; COMMON BLOCKS:
;       None.
;
; PROCEDURE:
;
;
; RESTRICTIONS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; EXAMPLES:
;
;
; SEE ALSO:
;
;
; HISTORY:
;       Version 1, Martin.Fivian@psi.ch, Thu Feb  1 19:29:57 MET 2001
;         Fri Mar 23 18:19:08 MET 2001, make the parameter TIME a long
;       Version 2, Martin.Fivian@psi.ch, Tue Jul 10 16:48:59 MET DST 2001
;         - accurate SAS solution only within smaller time_range,
;           otherwise use approximation given by triggered pixel
;       Fri Jul 13 19:14:25 MET DST 2001, replaced hessi_data_paths(), use $HSI_ASPECT
;       Fri Sep 21 18:14:20 MET DST 2001, use time_range in sctime/L64 format
;       Tue Sep 25 14:32:59 MET DST 2001, round up on tr_cycle=... +[0,1]
;       Wed Oct  3 19:28:10 MET DST 2001, since hsi_sas_cog is now returning
;         the values of the SAS image plane, change the sign of the xysc
;         to go to the imaging coordinate system.
;       Tue Jan  8 15:46:28 MET 2002, overflow of TIME corrected
;       Version 3, Martin.Fivian@psi.ch, Tue Feb 12 22:12:05 PST 2002
;         - integrate first real data
;         - implement switches for different options
;       Tue Apr  2 15:53:53 MET DST 2002, triangle already in arcsec from hsi_sas_cog
;       Version 4, Martin.Fivian@psi.ch, Tue Apr  9 17:23:07 MET DST 2002
;         - partially restructured
;         - linear limbfit using a threshold
;       Tue Apr 23 21:16:06 MET DST 2002, calculate overall SAS quality
;       Fri Nov  1 20:56:21 PST 2002, provide sign of limb to hsi_sas_linfit.pro
;
;-


pro hsi_sas, limbp, xysc, time, t0, $
                    time_range=time_range, quality=quality, $
                    mode=mode, limb_selection=limb_selection

; set the switches
;
if not keyword_set(mode) then mode='model'

case strlowcase(mode) of
  'pixel' : dummy=0
  'linfit': dummy=0
  'model' : dummy=0
  else    : mode='pixel'
endcase

if not keyword_set(limb_selection) then begin
  if (!HSI_ASPECT.PT.ID gt 79 and !HSI_ASPECT.PT.ID lt 90) then $ ;for simulations
    limb_selection=1 $
  else $
    limb_selection=2
endif

; read the limb model functions

filename='as_limbmodel.xdr'
file = loc_file(path = getenv('HSI_ASPECT'), filename, count=file_count)
model=fltarr(400,10)
openr,lun,file,/GET_LUN
readu,lun,model
free_lun,lun
byteorder,model,/xdrtof

; estimate limb position
;  ?1: using poly_fit, needs loop for all limbs
;  ?2: for all limbs: m=mean gradient=total((p-shift(p,1))[1:*]/(nopixel-1)
;                     q=mean y-offset=total of all pixel(p-p#*m)/nopixel
;                     pos=estimated position= (thr-q)/m
;      with p=pixel value and p#=pixel number
;  ?3: using number of triggered pixel, but subtract 1 for negative limbs
;  ?4: for mode='linfit', don't do anything

if (strlowcase(mode) ne 'linfit') then begin
  index=where(limbp[1,*] eq 0)
  limbp[4,index]=limbp[4,index]-1
endif

; select limbs
;  ?1: select cycle with 3 times 2 limbs
;  ?2: assume all cycles have 3 times 2 limbs

case limb_selection of
1: begin
     firstl=0
     while (limbp[3,firstl] ne limbp[3,firstl+5]) do firstl=firstl+1
     lastl=(size(limbp))[2]-1
     while (limbp[3,lastl ] ne limbp[3,lastl -5]) do lastl =lastl-1

     if (!HSI_ASPECT.cntl_level gt 1) then $
       message,'Skipping Limbs (begin,end):' $
               +strtrim(firstl)+strtrim((size(limbp))[2]-1-lastl),/info

     limbp=(temporary(limbp))[*,firstl:lastl]

     index0pos=where(limbp[1,*] eq 1 and limbp[2,*] eq 0)  ; SAS 0, pos limb
     index0neg=where(limbp[1,*] eq 0 and limbp[2,*] eq 0)  ; SAS 0, neg limb
     index1pos=where(limbp[1,*] eq 1 and limbp[2,*] eq 1)  ; SAS 1, pos limb
     index1neg=where(limbp[1,*] eq 0 and limbp[2,*] eq 1)  ; SAS 1, neg limb
     index2pos=where(limbp[1,*] eq 1 and limbp[2,*] eq 2)  ; SAS 2, pos limb
     index2neg=where(limbp[1,*] eq 0 and limbp[2,*] eq 2)  ; SAS 2, neg limb
   end
2: begin
     index0pos=hsi_sas_limbsel(limbp)
     index0neg=index0pos+1
     index1pos=index0pos+2
     index1neg=index0pos+3
     index2pos=index0pos+4
     index2neg=index0pos+5
     if (!HSI_ASPECT.cntl_level gt 1) then $
       message,'SAS from cycles with 6 limbs',/info
   end
endcase

; calculate approximate chord length

if (strlowcase(mode) eq 'model') then begin
  chord0=float(limbp[4,index0neg] - limbp[4,index0pos])
  chord1=float(limbp[4,index1neg] - limbp[4,index1pos])
  chord2=float(limbp[4,index2neg] - limbp[4,index2pos])
endif

; fit all limb positions

if (strlowcase(mode) eq 'model') then begin
  m=(size(limbp))[1]-5  ; no of pixels/limb
  n=(size(limbp))[2]    ; no of limbs
  posindex=5+indgen(m)
  negindex=5+m-1-indgen(m)
  ; scale model or pixel values
  model=model*800
endif

; create array of offsets
position_0pos=fltarr(n_elements(index0pos))
position_0neg=fltarr(n_elements(index0neg))
position_1pos=fltarr(n_elements(index1pos))
position_1neg=fltarr(n_elements(index1neg))
position_2pos=fltarr(n_elements(index2pos))
position_2neg=fltarr(n_elements(index2neg))

;;if (n_elements(time_range) eq 2) then begin  ; !!! only if time_range is defined

case strlowcase(mode) of
  'model' : begin
    ; call limbfit (pixels,chordlength,sign output: pos)
    tr_cycle=(hsi_any2sctime(time_range,/L64)-hsi_sctime_convert(t0,/L64))/8192+[0,1]

    i0pos=where(limbp[3,index0pos] gt tr_cycle[0] and limbp[3,index0pos] lt tr_cycle[1])
    i0neg=where(limbp[3,index0neg] gt tr_cycle[0] and limbp[3,index0neg] lt tr_cycle[1])
    i1pos=where(limbp[3,index1pos] gt tr_cycle[0] and limbp[3,index1pos] lt tr_cycle[1])
    i1neg=where(limbp[3,index1neg] gt tr_cycle[0] and limbp[3,index1neg] lt tr_cycle[1])
    i2pos=where(limbp[3,index2pos] gt tr_cycle[0] and limbp[3,index2pos] lt tr_cycle[1])
    i2neg=where(limbp[3,index2neg] gt tr_cycle[0] and limbp[3,index2neg] lt tr_cycle[1])

    hsi_sas_limbfit,(limbp[*,index0pos[i0pos]])[posindex,*],sign=1,chord0,model, pos_0pos
    hsi_sas_limbfit,(limbp[*,index0neg[i0neg]])[negindex,*],sign=0,chord0,model, pos_0neg
    hsi_sas_limbfit,(limbp[*,index1pos[i1pos]])[posindex,*],sign=1,chord1,model, pos_1pos
    hsi_sas_limbfit,(limbp[*,index1neg[i1neg]])[negindex,*],sign=0,chord1,model, pos_1neg
    hsi_sas_limbfit,(limbp[*,index2pos[i2pos]])[posindex,*],sign=1,chord2,model, pos_2pos
    hsi_sas_limbfit,(limbp[*,index2neg[i2neg]])[negindex,*],sign=0,chord2,model, pos_2neg

    position_0pos[i0pos]=pos_0pos
    position_0neg[i0neg]=pos_0neg
    position_1pos[i1pos]=pos_1pos
    position_1neg[i1neg]=pos_1neg
    position_2pos[i2pos]=pos_2pos
    position_2neg[i2neg]=pos_2neg
  end
  'linfit': begin
      hsi_sas_linfit,limbp[4,index0pos],limbp[5:*,index0pos],position_0pos, +1
      hsi_sas_linfit,limbp[4,index0neg],limbp[5:*,index0neg],position_0neg, -1
      hsi_sas_linfit,limbp[4,index1pos],limbp[5:*,index1pos],position_1pos, +1
      hsi_sas_linfit,limbp[4,index1neg],limbp[5:*,index1neg],position_1neg, -1
      hsi_sas_linfit,limbp[4,index2pos],limbp[5:*,index2pos],position_2pos, +1
      hsi_sas_linfit,limbp[4,index2neg],limbp[5:*,index2neg],position_2neg, -1
      position_0pos=-position_0pos
      position_1pos=-position_1pos
      position_2pos=-position_2pos
  end
  else: 
endcase

; calculate the mid-limb positions

mid0=(limbp[4,index0pos]-position_0pos + limbp[4,index0neg]+position_0neg)/2
mid1=(limbp[4,index1pos]-position_1pos + limbp[4,index1neg]+position_1neg)/2
mid2=(limbp[4,index2pos]-position_2pos + limbp[4,index2neg]+position_2neg)/2

; for radii and triangle, re-calculate the chord length

ch0=limbp[4,index0neg]+position_0neg - limbp[4,index0pos]-position_0pos
ch1=limbp[4,index1neg]+position_1neg - limbp[4,index1pos]-position_1pos
ch2=limbp[4,index2neg]+position_2neg - limbp[4,index2pos]-position_2pos

; calculate intersections and center of gravity

mid0=reform(mid0,n_elements(mid0),/overwrite) *!HSI_ASPECT.SAS.SCALE0
mid1=reform(mid1,n_elements(mid1),/overwrite) *!HSI_ASPECT.SAS.SCALE1
mid2=reform(mid2,n_elements(mid2),/overwrite) *!HSI_ASPECT.SAS.SCALE2
ch0 =reform(ch0 ,n_elements(ch0) ,/overwrite) *!HSI_ASPECT.SAS.SCALE0
ch1 =reform(ch1 ,n_elements(ch1) ,/overwrite) *!HSI_ASPECT.SAS.SCALE1
ch2 =reform(ch2 ,n_elements(ch2) ,/overwrite) *!HSI_ASPECT.SAS.SCALE2
hsi_sas_cog,mid0,mid1,mid2,ch0,ch1,ch2,xysc,triangle=triangle,radii=radii

; flip the SAS coorinates because of lens
xysc=-xysc

; write time array
;  ?1: we assumed: 128Hz, 6 good limbs for all cycles
;  6 good limbs for all processed cycles

;?1? time=lindgen( (size(xysc))[1] )

;time=long(limbp[3,index0pos])
;index=where(time lt 0,count)
;while (count ne 0) do begin
;  time[index[0]:*]=time[index[0]:*] + ishft(1L,16)
;  index=where(time lt 0,count)
;endwhile

if (size(time,/type) eq 0) then begin  ; if time not defined
  time=long(limbp[3,index0pos])
  index=where( (time-shift(time,1))[1:*] lt 0, count)
  for i=0,count-1 do $
    time[index[i]+1:*]=time[index[i]+1:*] + ishft(1L,16)
endif else $
  time=time[index0pos]

; write quality if needed

if (!HSI_ASPECT.cntl_level gt 3) then begin
  ptr_free,quality.radii
  ptr_free,quality.triangle
; ptr_free,quality.sas_quality

  quality.radii   =ptr_new(intarr(1,6))
  quality.triangle=ptr_new(intarr(1))
  (*quality.radii)    =temporary(radii)
  (*quality.triangle) =triangle

  ; calculate overall SAS quality
  quality.sas_error=(sqrt( moment(triangle^2) ))[0]
  message,'average relative SAS error [arcsec] : '+string(quality.sas_error),/info
; quality.sas_quality=ptr_new(intarr(1))
; (*quality.sas_quality)=exp(-triangle^2/2.5)
; index=where(abs(triangle) gt 0.4)
; (*quality.sas_quality)[index]=0
endif

end
