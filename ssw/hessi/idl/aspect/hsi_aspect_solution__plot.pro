;+
; Name:  hsi_aspect_solution::plot
;
; Purpose:  Plot method for hsi_aspect_solution object.
;
; Calling sequence:  obj -> plot
;	_extra keywords are set in object, and used in plot command.
;
; Keyword Inputs:
;	xrange, yrange - x,y range for aspect plot
;	plot_triangle - if set, also plots size of triangles (for measure of
;	  quality of aspect (< .5 is OK)
;	triangle_yrange - yrange to use for triangle plot
;
; Outputs:  Creates new window for aspect plot, and if plot_triangle is set, another window
;	for the triangle plot.  Aspect plot shows imaging axis, spin axis, and solar limb.
;
; History:  Written Kim, 25-Apr-2007
; Modifications:
;  8-May-2007, Kim.  Some options are just keywords to getdata, and don't get set in
;    object, so don't use set for _extra, pass in getdata call, and also pass to get_spin_axis
;
;-
;============================================================================


pro hsi_aspect_solution::plot, xrange=xrange, yrange=yrange, $
	plot_triangle=plot_triangle, triangle_yrange=triangle_yrange, $
	_extra=_extra


;if keyword_set(_extra) then self -> set, _extra=_extra

;get aspect solution
as = self->getdata(_extra=_extra)

if size(as, /tname) eq 'STRUCT' then begin

	time_range = hsi_sctime2any(as.t0) + minmax(as.time)*2.d^(-7.)
	atime_range = anytim(time_range, /vms)

	if keyword_set(plot_triangle) then begin
		quality = self->get(/as_quality)
		if keyword_set(plot_triangle) then begin
		; SAS reduced triangle
		;
		window,/free
		yr = keyword_set(triangle_yrange) ? triangle_yrange : [-2.,2.]
		utplot,double(as.time)*2d^(-7), *quality.triangle, time_range[0], $
		     ystyle=1, yrange=yr, psym=-4, $
		     ytitle='arcsec', $
		     title='SAS :  Size of Reduced Triangle'
		endif

	endif

	title='Aspect Solution'

	;RHESSI pointing
	x=as.pointing[*,0]
	y=as.pointing[*,1]

	spin_axis = self->get_spin_axis(spin_period=spin_period, _extra=_extra)

	tvlct,rr,gg,bb,/get
	loadct,5

	checkvar, xrange, [-1200,1200]
	checkvar, yrange, [-1200,1200]

	; get a window, or if none, create one 500x500
	window,/free, xsize=600,ysize=600

	limb = (get_rb0p(atime_range[0]))(0)
	ang = findgen(3601)/3600.*2.*!pi


	plot, x, y, $
		xtitle='Heliocentric X (arcsec)',ytitle='Heliocentric Y (arcsec)',$
		xrange=xrange, yrange=yrange, title=title, _extra=_extra
	oplot, spin_axis[*,0], spin_axis[*,1], psym=-1, color=122
	oplot, limb*cos(ang), limb*sin(ang), color=200

	avsp = trim(average(spin_axis,1), '(2f8.2)')
	legend, [atime_range[0]+' - '+atime_range[1], $
			'Average Spin Axis: ' + arr2str(avsp,', ') + ' arcsec', $
			'Spin Period: ' + trim(spin_period, '(f8.2)') + ' sec', $
			'Image Axis', 'Spin Axis', 'Solar Limb'], $
		linestyle=[-99,-99,-99,0,0,0], color=[255,255,255,255,122,200], $
		psym=[1,1,1, -3, -1, -3], box=0,  _extra=_extra

	tvlct,rr,gg,bb


endif else begin

	message,'No aspect solution available.', /info

endelse

end