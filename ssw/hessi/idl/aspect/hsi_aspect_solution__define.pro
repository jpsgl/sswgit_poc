;---------------------------------------------------------------------------
; Document name: hsi_aspect_solution__define.pro
; Created by:    Andre, April 29, 1999
;
; Last Modified: Tue Feb  4 14:50:58 PST 2003 (mfivian@ssl.berkeley.edu)
; Mon Sep 30 18:11:19 PDT 2002
; Thu Oct 10 18:18:42 MET DST 2002
; 25-Oct-2002, Kim.  Added @hsi_insert_catch in getdata
; Mon Nov 18 18:56:08 PST 2002, Errors and Warnings in Getdata and Process
; Mon Dec  9 18:32:20 PST 2002, set 0's for /SASZERO
; Tue Feb  4 14:50:58 PST 2003, optionally return RAS solution
; Thu Jul 31 00:00:00 PDT 2003, various changes for off-pointing software
; Fri Aug 15 17:29:15 PDT 2003, no switches to ASPECT_SIM=1
; Fri Oct 17 18:57:43 PDT 2003, interpolation interval and extrapolation switch
; Fri Nov 21 15:59:55 PST 2003, new version;
;                               limb_selection=3, other than 6-limb solution
;                               sas_mode: pixel,model,linfit,fitpos1
; Mon Jan 12 19:09:20 PST 2004, default limb_selection=5
; Thu Jan 15 20:30:32 PST 2004, pass this_unit_time to hsi_as_deltapol
; RAS Aug 18              2014, aspect solution gaps no longer force an abort, info
;								is passed on to the caller for decisions
;---------------------------------------------------------------------------

FUNCTION Hsi_Aspect_Solution::INIT, $
            SOURCE=source, $
            _EXTRA=_extra

IF NOT Keyword_Set( SOURCE ) THEN source = HSI_Packet()

ret=self->Framework::INIT( CONTROL = HSI_Aspect_solution_Control(), $
                           INFO    = {hsi_aspect_solution_info}, $
                           SOURCE=source )

if not keyword_set(self.limb_selection) then self.limb_selection=5
if not keyword_set(self.sas_mode)       then self.sas_mode='linfit'

IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

RETURN, ret

END

;---------------------------------------------------------------------------

FUNCTION Hsi_Aspect_Solution::AS_CHECK_TIME_RANGE

; return value =1 --> aspect_time_range is set successfully
; return value =0 --> no usefull time range found

; get all time ranges
;
abs_time_range=self->get(/absolute_time_range)
as_time_range=self->get(/aspect_time_range)
obs_time_interval=self->get(/obs_time_interval)

;check for absolute time range
;
if valid_range(abs_time_range) then begin
  if valid_range(as_time_range) then begin
    if (as_time_range[0] gt abs_time_range[0]  or $
        as_time_range[1] lt abs_time_range[1]) then begin
      self->framework::set,aspect_time_range=abs_time_range
    endif else $
      if valid_range(obs_time_interval) then begin
        if (as_time_range[0] lt obs_time_interval[0]  or $
            as_time_range[1] gt obs_time_interval[1]) then begin
          self->framework::set,aspect_time_range=abs_time_range
        endif
      endif
  endif else $
    self->framework::set,aspect_time_range=abs_time_range
  return,1
endif $

; check for obs_time_interval
;
else if valid_range(obs_time_interval) then begin
  if valid_range(as_time_range) then begin
    if (obs_time_interval[1]-obs_time_interval[0] ge as_time_range[0] and $
        obs_time_interval[1]-obs_time_interval[0] ge as_time_range[1]) then begin
      self->framework::set,aspect_time_range=obs_time_interval[0]+as_time_range
    endif else $
      if (as_time_range[0] lt obs_time_interval[0]  or $
          as_time_range[1] gt obs_time_interval[1]) then begin
        self->framework::set,aspect_time_range=obs_time_interval
      endif
  endif else $
    self->framework::set,aspect_time_range=obs_time_interval
  return,1
endif $

; check for file_time_range
;
else begin
  file_time_range=self->get(/file_time_range)
  if valid_range(file_time_range) then begin
    if valid_range(as_time_range) then begin
      if (as_time_range[0] lt file_time_range[0]  or $
          as_time_range[1] gt file_time_range[1]) then begin
        self->framework::set,aspect_time_range=file_time_range
      endif
    endif else $
      self->framework::set,aspect_time_range=file_time_range
    return,1
  endif
endelse

; there is no valid time range
; (one gets here only from the section where file_time_range is checked)
;
return,0

END

;---------------------------------------------------------------------------

FUNCTION HSI_Aspect_Solution::GetData, $
            THIS_TIME=this_time, $
            THIS_UNIT_TIME=this_unit_time, $
            THIS_UT_REF=this_ut_ref, $
            SAS_only=SAS_only, RAS_only=RAS_only, $
            _EXTRA=_extra

@hsi_insert_catch

IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA =  _extra

; set aspect_cntl_level to a minimum according to self.debug
;
if ( self.debug gt 0 and self->get(/aspect_cntl_level) lt 2 ) then $
  self->set,aspect_cntl_level=2
cntl_level=self->get(/ASPECT_CNTL_LEVEL)
debug     =self->get(/DEBUG)
verbose   =self->get(/VERBOSE)

if ( self->get(/SASZERO) ) then begin

  if (cntl_level gt 0) then message,'Creating SASZERO solution',/info

  if ( not self->as_check_time_range() ) then begin
    if (cntl_level gt -1) then $
      message,'Try to take aspect_time_range without checks',/info
  endif

  time_range=self->get(/aspect_time_range)

  if valid_range(time_range) then begin
    spin_period = self->get(/as_spin_period)
    delta_time  = hsi_any2sctime(time_range[1],/L64) $
                 -hsi_any2sctime(time_range[0],/L64)

    aspect_data = {time: long( [0,ishft(delta_time,-13)] ), $
                   t0: hsi_any2sctime(time_range[0],/ful), $
                   pointing: fltarr(2,2), $
                   roll: [ 0, delta_time*2d^(-19)*!DPI/spin_period ] }
  endif else $
    aspect_data = -1

endif else begin

  aspect_data = self->Framework::GetData( _EXTRA=_extra, $
                                          SAS_only=SAS_only, RAS_only=RAS_only)

endelse

; interpolate the data points
;
IF ( (n_elements(THIS_TIME) ne 0) and (size(aspect_data,/type) eq 8) ) THEN BEGIN

    ; calculate difference between UT_REF and aspect t0 in bms S/C time
    ;
    time_ref_shift = hsi_sctime_convert(hsi_any2sctime(this_ut_ref),/L64) $
                    -hsi_sctime_convert(aspect_data.t0,/L64)

    ; convert the relative aspect solution times to be relative to the time
    ; given by UT_REF
    ;
    as_time = ( aspect_data.time*2d^13 - time_ref_shift ) / this_unit_time

    as_inter=replicate({hsi_aspect}, n_elements(this_time) )

    if not (str_index(self->get(/as_interpol),'quad') lt 0) then $
      int_q=1 $
    else $
      int_q=0

    if ( self->get(/as_point_solution) eq 'FSS' ) then begin
      self->fss_model1, pointing=aspect_data.pointing, roll=aspect_data.roll, $
                        xc=xc, yc=yc, radius=radius, phase=r_angle

      coeff=linfit(aspect_data.ras_sol.reltime, aspect_data.ras_sol.posn_angle)
      omega=coeff[1] *this_unit_time*2d^(-20)

      xc_int     = interpol(xc,     as_time, this_time)
      yc_int     = interpol(yc,     as_time, this_time)
      radius_int = interpol(radius, as_time, this_time)

      phase      = ( ((r_angle - omega * as_time) mod (2*!pi)) + (2*!pi) ) mod (2*!pi)
      n          = n_elements(phase)
      if ( n gt 10 ) then phase_shift= !pi - mean(phase[n/2-5:n/2+5]) $
                     else phase_shift= !pi -      phase[n/2]
      phase      = ( phase - phase_shift + (2*!pi) ) mod (2*!pi)
      phase_int  = interpol(phase, as_time, this_time) + phase_shift

;     xysc        = [ [xc_int + radius_int*sin(phase_int)], $
;                     [yc_int + radius_int*cos(phase_int)] ]

      toffset=double(hsi_sctime_convert(aspect_data.ras_sol.t0_ref,/l64) $
                    -hsi_sctime_convert(hsi_any2sctime(this_ut_ref),/l64))*2d^(-20)

      as_inter.phi=interpol( aspect_data.ras_sol.posn_angle, $
                             aspect_data.ras_sol.reltime, $
                             double(this_time)*2d^(-20)*this_unit_time -toffset)
;     xysc =hsi_as_fss2sas( $
;                          [[xc_int + radius_int*sin(omega*this_time + phase_int)], $
;                           [yc_int + radius_int*cos(omega*this_time + phase_int)]] )
      xysc =  [[xc_int + radius_int*sin(omega*this_time + phase_int)], $
               [yc_int + radius_int*cos(omega*this_time + phase_int)]]

      pointing=hsi_as_convert(xysc,as_inter.phi)
      as_inter.dx = pointing[*,0]
      as_inter.dy = pointing[*,1]
      return, as_inter
    endif

    if (keyword_set(SAS_only) OR not keyword_set(RAS_only)) then begin
      as_inter.dx  =Interpol( aspect_data.pointing[*,0], as_time, this_time, quadratic=int_q )
      as_inter.dy  =Interpol( aspect_data.pointing[*,1], as_time, this_time, quadratic=int_q )
    endif
    if ( self->get(/SASZERO) ) then begin
      as_inter[*].dx = 0  ; quadr. interpol. returns NaN if all inputs are 0's
      as_inter[*].dy = 0
    endif
    if (keyword_set(RAS_only) OR not keyword_set(SAS_only)) then begin
      as_inter.phi =Interpol( aspect_data.roll, as_time, this_time )
    endif

;   as_inter.delta=hsi_as_deltapol(as_time, this_time, unit_time=this_unit_time)
    as_inter.p_error=self->pointing_error(as_time, this_time, $
                                          unit_time=this_unit_time, $
                                          t0=this_ut_ref )
    as_inter.r_error=1./60*!dtor

    RETURN, as_inter

ENDIF ELSE BEGIN

    RETURN, aspect_data

ENDELSE

END

;---------------------------------------------------------------------------

PRO HSI_Aspect_Solution::Process, _EXTRA = _extra, $
                                  ras_sol= ras_sol


cntl_level=self->get(/ASPECT_CNTL_LEVEL)

;debug     =self->get(/DEBUG)
; acs 2004-04-21 now we use self.debug
debug =  self.debug
verbose   =self->get(/VERBOSE)
no_extra  =self->get(/AS_NO_EXTRAPOL)

; acs 2004-04-21 just to check when it gets here....
if self.debug gt 0 then begin
  message,  'PROCESSING THE ASPECT SOLUTION ************************',  /info,  /cont
endif

aspect_sim = self->Get( /ASPECT_SIM )

; set the output structure to a default value
;   indicating is has not been calculated yet
;
aspect_str=-1

IF aspect_sim NE 0 THEN BEGIN

  message,'Simulate Aspect Solution',/info

  ; find the appropriate time range
  ; 1. check methode as_check_time_range()
  ; 2. check for aspect_time_range
  ; if still not set: return -1.
  ;

  if ( not self->as_check_time_range() ) then begin
    if (cntl_level gt -1) then $
      message,'Try to take aspect_time_range without checks',/info
  endif

  time_range=self->get(/aspect_time_range)

  if valid_range(time_range) then begin

    spin_period = self->get(/as_spin_period)

    hsi_as_sim_packet, time_range=time_range, t0=t0, $
                       pointing=pointing, roll=roll, $
                       /no_packet, $
                       omega=1.0/spin_period
    time=lindgen(n_elements(roll))

    aspect_str= {time: Reform( time ), t0: t0, $
                 pointing: pointing, roll: Reform( roll ) }
  endif

endif else begin

  if self->as_check_time_range() then begin

;   after272 = self->Get( /AFTER272 )

    packet_obj = self->Get( /OBJECT, CLASS='HSI_PACKET' )

    ; make sure aspect packets are accessible
    ;
;    if not obj_valid(packet_obj) then begin
;        msg="Can't read aspect packets. Packet object not valid."
;        @hsi_message
;    endif else begin
;        filename = packet_obj->Get( /FILENAME )
;        if ( size( filename, /TYPE ) eq 10 ) THEN begin
;          msg="Can't read aspect packets. File not found."
;          @hsi_message
;        endif else $
;          if ( filename[0] eq '' ) then begin
;            msg="Can't read aspect packets. File not found."
;            @hsi_message
;          endif
;    endelse

; kludge to allow aspect solution for the case where there is no
; packet object, such as in simulated data case, jmm, 11-sep-2003
    if not obj_valid(packet_obj) then begin
      time_range = self -> get(/aspect_time_range)
      if valid_range(time_range) then begin
        packet_obj = hsi_packet(obs_time_interval = time_range)
      endif
    endif
    filename = packet_obj -> Get( /FILENAME )
    if ( size( filename, /TYPE ) eq 10 ) THEN begin
      msg = "Can't read aspect packets. File not found."
      @hsi_message
    endif else $
      if ( filename[0] eq '' ) then begin
      msg = "Can't read aspect packets. File not found."
      @hsi_message
    endif
;End kludge

    control_aspect={hsi_as_control}
    control_param =self->get(/control,/this_class)
    control=join_struct(control_aspect, control_param)
    if (strpos(strlowcase(self->get(/as_roll_solution)),'fix') ne -1) then $
      control.roll_offset_ref= (self->get(/file_time_range))[0]            ; can take a long time
    control.limb_selection = self.limb_selection
    control.sas_mode       = self.sas_mode
    control.norm_radii     = self.norm_radii
    if (self->get(/aspect_sim)) then control.sas_mode='model'

    HSI_Aspect, PACKET_OBJ=packet_obj, $
        TIME=time, tref=t0, POINTING=pointing, ROLL=roll, QUALITY=quality, $
        CONTROL=control, $
        t0ras=t0ras, sol_roll=ras_sol, $
        _EXTRA=_extra
;       AFTER272=after272

;   aspect_str = {time: Reform( time ), t0: t0, $
;                 pointing: pointing, roll: Reform( roll ), $
;                 quality: Reform( quality ) }

    if keyword_set(ras_sol) then begin
      aspect_str = {time: time , t0: t0, pointing: pointing, roll: roll, $
                    ras_sol: ras_sol, t0ras: t0ras }
    endif else begin
      aspect_str = {time: time , t0: t0, pointing: pointing, roll: roll }
    endelse

    ; test if aspect solution covers the whole aspect time range
    ;
    time_range = self->get(/aspect_time_range)
;    sol_range  = hsi_sctime2any( $
;                 hsi_sctime_add( long64(minmax(time)*2d^3), t0, time_unit=2L^10) )
;    if not keyword_set(no_extra) then $
;      v_range=[0,0] $
;    else $
;      v_range    = value_locate( sol_range, time_range)
;    if ( (v_range[0] ne 0) or (v_range[1] ne 0) ) then begin
;      message,'Solution Range: '+(anytim(sol_range,/ecs))[0] + $
;              ' to ' + (anytim(sol_range,/ecs))[1], /info
;      self->SetData, -1, /no_last_update      ; !!! by acs, do we want that ?
;      msg='Aspect solution not covering the full analysis time range.'
;      message, msg
	 sol_time_sec = hsi_sctime2any(hsi_sctime_add( long64(time)*8, t0, time_unit=2L^10))
	dtime = max( sol_time_sec[1:*] - sol_time_sec )
	if dtime gt 0.256 then message,/info,'Aspect solution gap(s) of '+string(dtime)+' sec'

                   ;      @hsi_message
                   ; acs 2005-03-15 needs the return because otherwise it continues
                   ; generating the image (hsi_message has a /cont when debug is not 0)
    ;endif

  endif

endelse

if ( n_elements(roll) gt 1 and n_elements(time) gt 1 ) then begin
  self->set, avg_spinperiod = 2.*!pi / (linfit(double(time)/128, roll))[1]
endif

self->SetData, aspect_str
self->set, as_quality=quality

END

;---------------------------------------------------------------------------

PRO Hsi_Aspect_Solution__Define

self = {Hsi_Aspect_Solution, $
        Absolute_Time_range: [0d,0d], $

        abs_pointing: [0d,0d], $
        SAS_only    : 0, $
        RAS_only    : 0, $

        limb_selection : 0, $
        sas_mode       : ' ', $
        norm_radii     : 0, $

        INHERITS Framework }

END

;---------------------------------------------------------------------------
; End of 'hsi_aspect_solution__define.pro'.
;---------------------------------------------------------------------------
