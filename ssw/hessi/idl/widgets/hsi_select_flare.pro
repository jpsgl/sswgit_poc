;+
; Name: hsi_select_flare
;
; Project:  HESSI
;
; Purpose:  Function to select flares based on ranges of flare parameters.  Finds flares
;   that meet the specified criteria and returns an array of flare numbers, or an array
;	of structures containing all parameters for selected flares,
;   or an array of strings listing the selected flares and their parameters.
;
; Calling sequence:  list = hsi_select_flare()
;
; Input keywords:
;   Append '_range' to any tag in extended catalog structure to form allowed
;     keywords for specifying the range you want for that tag.  Value is
;     range of values to select for that tag item.
;     Extended catalog structure is returned by a call like f = hsi_read_flarelist() and
;     contains original items in catalog plus a few derived items.
;     Times should be specified as [start,stop] in any anytim format
;     Examples:
;      ID_NUMBER_RANGE=[m,n] (for flares with id_number between m and n)
;      START_TIME_RANGE=['2000/9/1 01:00', '2000/9/1 02:00']
;      TOTAL_COUNTS_RANGE=[5000,20000]
;      RADIAL_DIST_RANGE=[400,800]
;   FORMATTED - if set, return ascii list of selected flares and parameters.  Otherwise
;     just returns array of flare numbers.  (Note - can pass in keywords like /noheader
;     and /notrailer that will get passed on to hsi_format_flare)
;   STRUCTURE - if set, return structure with all parameters for selected flares.
;	TIME_RANGE - overall range of times to select in anytim format,
;		e.g. ['14-feb-2002','15-feb-2002']
;		If this range is set, then ranges for start, peak, end time of flare are ignored.
;		Any flare that overlaps with this time_range is selected.
;	HOUR_RANGE - range of hours to select (regardless of date), e.g. [2,4]
;	FLAG_INCL - String array of flag codes that flare must have to be selected.
;		Type print,hsi_flare_flag_code(/expand,/sort) to see the list of flare flags.
;		e.g. ['A2', 'DF']
;	FLAG_EXCL - String array of flag codes. If flare has any of these, it won't be selected.
;		Type print,hsi_flare_flag_code(/expand,/sort) to see the list of flare flags.
;		e.g. ['A2', 'DF']
;	SEL_TEXT -
;   QUIET - if set, then don't print error messages
;   _EXTRA - any keywords to pass on to formatting routine, e.g. sort field, sort order
;		see arguments to hsi_format_flare for options
;
; Output keywords:
;	SEL_TEXT - string array describing how many flares were selected, and what the search
;		criteria were.  If there are no search criteria, then sel_text is blank.
;   ERR_MSG - error message string if any
;
; Written:  Kim Tolbert, 26-Nov-2000
;Modification:
;   7-Feb-2001 - allow min and max of range to be equal
;   24-Mar-2001, Kim.  Added structure keyword
;   29-Dec-2001, Kim.  Instead of passing in each parameter_range keyword, use
;      _extra
;	28-Jan-2003, Kim.  Added time_range, hour_range, flag_incl and flag_excl
;		keywords and added selection on those additional items.  Also added sel_text
;		keyword to return a text string indicating search criteria.
;		Also, made change to accomodate searching on energy_hi field (which is a range,
;		not a scalar)
;	29-Oct-2006, Kim. Corrected header doc. Flag_incl, flag_excl should be string arrays.
;	22-June-2007, Kim.  Changed indgen to lindgen
;	4-Feb-2008, Kim.  Corrected search when value is non-scalar (like energy_hi)
;
;-
;=============================================================================

function hsi_select_flare, $
	formatted=formatted, $
	structure=structure, $
	time_range=time_range, $
	hour_range=hour_range, $
	flag_incl=flag_incl, $
	flag_excl=flag_excl, $
	sel_text=sel_text, $
	err_msg=err_msg, quiet=quiet, _extra=_extra

loud = not keyword_set(quiet)

flare_data = hsi_read_flarelist(err_msg=err_msg)

sel_text = 'No flares found that meet these requirements:'

if err_msg ne '' then return, -1

tags = strlowcase (tag_names(flare_data))
ind = lindgen(n_elements(flare_data))

if keyword_set(_extra) then begin
	for i = 0,n_elements(tags)-1 do begin
		range_name = tags[i] + '_range'

	    if tag_exist(_extra, range_name, index=index) then begin

	    	range = _extra.(index)

			if array_match (range, [0,0]) then goto, end_loop

	    	if n_elements(range) ne 2 then begin
	    		err_msg = [err_msg, 'Error - range for ' + range_name + ' does not have 2 elements.']
	    		goto, end_loop
	    	endif

			if strpos(range_name, 'time') ne -1 then begin
				; if overall time range is set, don't select on flare start,peak or end times
				if keyword_set(time_range) then goto, end_loop
				range = anytim(range, error=error)
				if error then begin
					err_msg = [err_msg, 'Error in times for ' + range_name]
					goto, end_loop
				endif
				rmax = max(range, min=rmin)
				arange = anytim(rmin,/vms) + ' to ' + anytim(rmax,/vms)
			endif else begin
				; allow ranges that are [max,min] as well as [min,max]
				rmax = max(range, min=rmin)
				arange = trim(rmin) + ' to ' + trim(rmax)
			endelse

			sel_text = append_arr(sel_text, strcapitalize(tags[i]) + ' Limits: ' + arange)

		; actually, I guess it's OK if they're equal.  2/7/01
		;	if rmax eq rmin then begin
		;		err_msg = [err_msg, 'Error - min and max of range for ' + range_name  + ' are equal.']
		;		goto, end_loop
		;	endif

			; Some search fields are not scalars.  For scalars, just search for value inside range
			; For non-scalars, search for max ge min of range and min le max of range -
			; Corrected 4-feb-2008 - now check min ge min or range and max le max of range
			if n_elements(flare_data[0].(i)) eq 1 then $
	    		q = where (flare_data[ind].(i) ge rmin and flare_data[ind].(i) le rmax, count) $
	    	else $
	    		q = where ((flare_data.(i))[0,ind] ge rmin and (flare_data.(i))[1,ind] le rmax, count)
	    	if count gt 0 then ind = ind[q] else return, -1
	    endif
	end_loop:
	endfor
endif

if keyword_set(time_range) then begin
	range = anytim(time_range, error=error)
	if error then begin
		err_msg = [err_msg, 'Error in times for overall time range selected.']
	endif else begin
		rmax = max(range, min=rmin)
		arange = anytim(rmin,/vms) + ' to ' + anytim(rmax,/vms)
		sel_text = append_arr(sel_text, 'Overall Time Limits: ' + arange)
		q = where (flare_data[ind].end_time ge rmin and flare_data[ind].start_time le rmax, count)
		if count gt 0 then ind = ind[q] else return, -1
	endelse
endif

if keyword_set(hour_range) then begin
	if not array_match(hour_range, [0,0]) then begin
		rmax = max(hour_range, min=rmin)
		arange = trim(rmin) + ' to ' + trim(rmax)
		rmin = rmin*3600.d0 & rmax = rmax * 3600.d0
		sel_text = append_arr(sel_text, 'Hour Limits: ' + arange)
		q = where (anytim(flare_data[ind].end_time, /time_only) ge rmin and $
					anytim(flare_data[ind].start_time, /time_only) le rmax, count)
		if count gt 0 then ind = ind[q] else return, -1
	endif
endif

if keyword_set(flag_incl) then begin
	fl_flags = hsi_get_flare_flags (flare_data[ind].id_number, /all)
	new_ind = indgen(n_elements(fl_flags))
	sel_text = append_arr(sel_text, 'Flags Required: ' + arr2str(flag_incl,','))
	for i = 0, n_elements(flag_incl)-1 do begin
		q = where (strpos(fl_flags[new_ind], flag_incl[i]) ne -1, count)
		if count gt 0 then new_ind = new_ind[q] else return, -1
	endfor
	ind = ind[new_ind]
endif

if keyword_set(flag_excl) then begin
	fl_flags = hsi_get_flare_flags (flare_data[ind].id_number, /all)
	new_ind = indgen(n_elements(fl_flags))
	sel_text = append_arr(sel_text, 'Flags Excluded: ' + arr2str(flag_excl,','))
	for i = 0, n_elements(flag_excl)-1 do begin
		q = where (strpos(fl_flags[new_ind], flag_excl[i]) eq -1, count)
		if count gt 0 then new_ind = new_ind[q] else return, -1
	endfor
	ind = ind[new_ind]
endif

if n_elements(sel_text) eq 1 then sel_text = '' else $
	sel_text[0] = trim(n_elements(ind)) + ' flares found that meet the following requirements:'

if n_elements(err_msg) gt 1 then err_msg = err_msg[1:*]

if loud and err_msg[0] ne '' then prstr, err_msg, /nomore

if keyword_set(formatted) then $
	return, hsi_format_flare(flares=flare_data[ind].id_number, _extra=_extra)

if keyword_set(structure) then return, flare_data[ind]

return, flare_data[ind].id_number

end