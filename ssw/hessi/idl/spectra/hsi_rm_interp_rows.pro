;+  
; PROJECT: HESSI
; NAME:  hsi_rm_interp_rows
;
; PURPOSE:
;  Creates a response matrix row by row by interpolation from a file full
;  of parameters of fits to Monte Carlo simulations.  Used by several
;  of the response submatrix routines (hsi_rm_*****).
;
; CATEGORY: HESSI spectral analysis
;
; CALLING SEQUENCE:
;
;  pro hsi_rm_interp_rows, energies, interpfile, modelvals, $
;     subrm_type,out_edges=out_edges, linear=linear, diagonly=diagonly,
;     earthangle = earthangle
;
; INPUTS:
;
;    energies       Incident photon energies desired (mean energies
;                      of bins)
;    interpfile     File containing the fit parameters for this
;                   submatrix, either for a particular virtual detector or
;                   averaged over a set of nine of them (ie all front or all
;                   rear). 
;
;    subrm_type     Type of interaction represented by the
;                   submatrix (=0: detector can be hit first; =1: can't)
;     
; OUTPUTS:
;
;    modelvals      Array containing the matrix created (or a vector of
;                    photopeak values if diagonly is set).
;
; OPTIONAL INPUTS:
;
;    out_edges      Count spectrum edges for the desired row; if
;                     not entered, they are the usual default 9000
;    linear         Use linear interpolation for all parameters 
;                     between the energies in interpfile; default
;                     is logarithmic (power-law) interpolation
;    diagonly       Return diagonal (photopeak) value only
;    earthangle     Passed through to hsi_rm_interp_readfile
;                     for picking an angle in Earth scattering
;
; CALLS:  hsi_rd_ct_edges, hsi_get_e_edges, hsi_rm_fit_eboundaries,
;         hsi_rm_interp_readfile, hsi_rm_interp_fit,
;         hsi_rm_interp_calc,hsi_correct_peaks
;
;
; MODIFICATION HISTORY: 
;
; DMSmith 10-Sep-1999   VERSION 1.0
; cPbL: 20Apr2001 Added "otherpeaks" capability
; cPbL: 25Apr2001 Small changes throughout interpolation codes to get
;       a working solution around line crossings.
; cPbL:22Aug01: seg argument removed. The "dat" output of _readfile now
;               contains the fit for just one virtual detector.
; cPbL:Nov01: Resolved  major problems with all diagonal element
;             interpolations.  This was due to the 3-bin photopeak,
;             and the fact that loginterp(sum) is not
;             sum(loginterp). Fixed by adding the three bins together
;             at beginning.
; DMSmith 23-Dec-01 pass through index "earthangle" for hsi_rm_earth
;                     to hsi_rm_interp_readfile
; DMSmith 24-Dec-01 add kalpha escape peak
; DMSmith 6-Feb-03 Made sure escape peaks get spread over all
;   applicable channels when top channel is broader than lower
; DMSmith 6-Feb-03 Added broadened Gaussian template for annihilation line
; DMSmith 22-Nov-03 Fixed an error introduced with the modification of
;  the escape peak code on 6-Feb-03.  Escape peak was getting lost
;  when photopeak channels were narrower than escape peak channels.
; DMSmith 28-Nov-03 Added call to hsi_correct_peaks() for
;  refinement of the escape peak amplitudes (and later the others)
; DMSmith 19-Jan-04 Fixed a bug involving the interpretation of
;  "findppeak" -- the photopeak term was not being added into the
;  lowest-energy bin for non-diagonal response.
; DMSmith 24-May-04 Extend the correction of 6-Feb-03/22-Nov-03 to
;  the k-shell escape peak at low energies in addition to the two
;  511 keV escape peaks.
; DMSmith 17-Aug-04 Fixed the 19-Jan-04 fix so that it doesn't break
;  when there is only one energy channel (n_elements(out_edges) EQ 2)
;  (see the code that adds extra elements to replace the
;  wrapped-around ends of shift1 and shift2)
;
;------------------------------------------------------------
;-

pro hsi_rm_interp_rows, energies, interpfile, modelvals, $
   subrm_type,out_edges=out_edges, linear=linear, diagonly=diagonly,$
   earthangle = earthangle

  e511 =  511.025
  ekalpha = 9.88 
  ekedge = 11.1 

; Pick a default out_edges. Use the high scale if there're any high energies
IF (NOT keyword_set(out_edges)) then begin
  if (energies[n_elements(energies)-1] GT 3000.) THEN $
         out_edges = hsi_get_e_edges(/high_scale) $     
  ELSE   out_edges = hsi_get_e_edges()
endif

linear = fcheck(linear,0)
diagonly = fcheck(diagonly,0)

hsi_rd_ct_edges,out_edges,out_edges ; If out_edges is a code, get the array

    
;Will it be a real matrix or a vector of diagonal elements?
if (diagonly) then modelvals = dblarr(n_elements(energies)) else $
                   modelvals = dblarr(n_elements(energies), $
                                      n_elements(out_edges)-1)

;Epsilon is a small energy change so that when the input energy is at
;a channel boundary (as it often will be) it falls in the same channel
;that it does in the simulations.  This puts the lines in the right
;(or rather a consistent) set of channels.  Note it is subtracted for
;the 511 keV line and added for the lines which are proportional to
;the input energy (photopeak and escape peaks):

epsilon = 0.001

;Because the 511 keV line resulting from annihilation in the
;spacecraft has an intrinsic width of 2.5 keV FWHM, let's make up
;a Gaussian template of this width for use in adding in this peak:

local511 = where(energies GT 500. and energies LT 521., nlocal)
if nlocal GT 0 then begin
   template511 = fltarr(n_elements(energies))
   localedges = out_edges[[local511,max(local511)+1]]
   gint = gaussint( (localedges - 511.0)/(2.5/2.355) )
   spec511 = shift(gint,-1)-gint
   template511[local511] = spec511[0:n_elements(spec511)-2]
endif

;Read data file of parameters of nearby Montecarlo runs:
hsi_rm_interp_readfile,interpfile,dat,nume,earthangle=earthangle

shift1 = shift(out_edges,-1)
shift2 = shift(out_edges,1)
;make 1 more low-energy value:
shift2[0] = out_edges[0] - (out_edges[1]-out_edges[0])
n = n_elements(out_edges)-1
;make 1 more high-energy value:
shift1[n] = out_edges[n] + (out_edges[n]-out_edges[n-1])
 
;Now loop through row by row (for each input energy):

for j=0,n_elements(energies)-1 do begin
  
  energy = energies[j]
  one_row = dblarr(n_elements(modelvals[j,*]))
  
  ;Interpolate to parameter values for this energy:
  hsi_rm_interp_fit,dat,nume,energy,result,linear=linear
  
  ;Correct ppeak if necessary:
  ppeak = hsi_correct_peaks(result.ppeak,[energy,energy,energy],0)

  ;Get energy boundaries between regions:
  hsi_rm_fit_eboundaries,eboundary,energy,neb,subrm_type
  
  if (result.numbands NE neb-1) then begin
      message,/info,'Inconsistency between number of energy bands and number'
      message,/info,'    expected for this energy: ' + string(energy) + $
                    ' .  Check if'
      message,/info,'    this energy is covered in ' + interpfile
      message,'Aborting...'
      return
  endif

  if (not diagonly) then begin

   ;Calculate interpolated continuum values for this energy:
   for i=0,result.numbands-1 do begin
     hsi_rm_interp_calc,one_row,eboundary[i],eboundary[i+1], $
                   result,i,out_edges
     modelvals[j,*] = one_row
   endfor

   findppeak = where(out_edges GE energy + epsilon,nfpp)
   if nfpp GT 0 then findppeak = findppeak[0]-1

   ;Calculate and restore k-alpha escape peak:
   ;(IF NECESSARY, SPREAD OUT ESCAPE PEAK FROM A BROAD BIN INTO
   ;NARROW BINS AT LOWER ENERGIES):

   if (energy GT ekedge) then begin
      chan_kepeak = where(out_edges GE energy-ekalpha + epsilon)
      chan_kepeak = chan_kepeak[0]-1
      if (chan_kepeak GE 0) then begin
          ppedges = out_edges[findppeak:findppeak+1]
          keedges = ppedges - ekalpha
          ke_subset = where(shift1 GE keedges[0] and shift2 LE keedges[1],nkes)
          if nkes GE 2 then begin
              hsi_rebinner,result.kepeak,keedges,ke_rebin,out_edges[ke_subset]
              modelvals[j, ke_subset[0:n_elements(ke_subset)-2]] = $
                 modelvals[j, ke_subset[0:n_elements(ke_subset)-2]] + ke_rebin
          endif
     endif
   endif

   ;before adding in the escape peaks, correct their magnitude via a
   ;relation derived from comparing ground data with GEANT runs:

   sepeak = hsi_correct_peaks(result.sepeak,energy,1)
   depeak = hsi_correct_peaks(result.depeak,energy,2)
   flux_511 = hsi_correct_peaks(result.flux_511,energy,3)

   ;Calculate and restore interpolated lines: 511, single & double escape
   if (energy GT 2.*e511) then begin

       if nlocal GT 0 then $
          modelvals[j,*] = modelvals[j,*] + template511*flux_511

       if nfpp GE 0 then begin
          ;IF NECESSARY, SPREAD OUT ESCAPE PEAKS FROM A BROAD BIN INTO
          ;NARROW BINS AT LOWER ENERGIES:
          ppedges = out_edges[findppeak:findppeak+1]

          seedges = ppedges - e511
          se_subset = where(shift1 GE seedges[0] and shift2 LE seedges[1],nses)
          if nses GE 2 then begin
              hsi_rebinner,sepeak,seedges,se_rebin,out_edges[se_subset]
              modelvals[j, se_subset[0:n_elements(se_subset)-2]] = $
                 modelvals[j, se_subset[0:n_elements(se_subset)-2]] + se_rebin
          endif

          deedges = ppedges - 2.*e511
          de_subset = where(shift1 GE deedges[0] and shift2 LE deedges[1],ndes)
          if ndes GE 2 then begin
              hsi_rebinner,depeak,deedges,de_rebin,out_edges[de_subset]
              modelvals[j, de_subset[0:n_elements(de_subset)-2]] = $
                 modelvals[j, de_subset[0:n_elements(de_subset)-2]] + de_rebin
          endif

       endif else begin
          chan_sepeak = where(out_edges GE energy-e511 + epsilon)
          chan_sepeak = chan_sepeak[0]-1
          if (chan_sepeak GE 0) then $
            modelvals[j, chan_sepeak]=modelvals[j, chan_sepeak] + sepeak
       
          chan_depeak = where(out_edges GE energy-2.*e511 + epsilon)
          chan_depeak = chan_depeak[0]-1
          if (chan_depeak GE 0) then $
            modelvals[j, chan_depeak]=modelvals[j, chan_depeak] + depeak
       endelse
    
              
   ENDIF ;   (energy GT 2.*e511)

   ;Calculate and restore fluorescence lines: "otherpeaks" (photoelectric K/L etc) ; 20Apr2001
   FOR k=0, result.n_otherpeaks-1 DO begin
      chanOP = where(out_edges GE result.otherpeaksE[k] - epsilon)
      chanOP = chanOP[0]-1
      if (chanOP GE 0) then begin
          modelvals[j, chanOP]=modelvals[j, chanOP] + result.otherpeaksN[k]
      endif
   ENDFOR                          ; k

 ENDIF ; not diagonly


 if (diagonly) then begin

    ; For diagonly, include some of the
    ; small-scattered counts as well as the true
    ; diagonal element:

    modelvals[j] = ppeak[0]+ppeak[1]+ppeak[2]

 endif else BEGIN ; Construct full sRM matrix

    ; For full matrix put the small-scattered
    ; counts as well as the true diagonal element
    ; all into the diagonal element 

    if (findppeak GE 0) then $ ; photopeak is somewhere in matrix
       modelvals[j, findppeak] = $
        modelvals[j, findppeak] + ppeak[0]+ppeak[1]+ppeak[2]
endelse

endfor

end


