function hsi_filter, _extra=_extra

return, obj_new('hsi_filter', _extra=_extra)

end

;---------------------------------------------------------------------------

function hsi_filter_control
var = {hsi_filter_control}
var.energy = ptr_new(findgen(100)+3.5)
var.center_thick_ratio = 1.0
var.det_index = 0
var.atten_state = 1
var.blanket_coeff = 1.0  ;added 1-may-2008
return, var
end
;---------------------------------------------------------------------------
pro hsi_filter_control__define

d = {hsi_filter_control, $
	energy: ptr_new(), $
	center_thick_ratio: 0.0,$
	det_index:0, $
	atten_state:0.0, $
	blanket_coeff:0.0 };added 1-may-2008, raise to increase low energy attenuation

end
;---------------------------------------------------------------------------
pro hsi_filter_info__define

d = {hsi_filter_info, $
	blanket: ptr_new(0), $
	info_state: ptr_new(0) }

end

;---------------------------------------------------------------------------
; Document name: hsi_filter__define.pro
; Created by:    Andre Csillaghy, March 4, 1999
;
; Last Modified: Mon Apr 23 14:19:32 2001 (csillag@soleil)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI FRAMEWORK TEMPLATE CLASS
;
; PURPOSE:
;
;       Compute hessi attenuator transmission
;
; CATEGORY:
;       Spectra
;
; CONTRUCTION:
;       o = Obj_New( 'hsi_filter' )
;       or
;       o = hsi_filter( )
;
; INPUT (CONTROL) PARAMETERS:
;       Defined in {hsi_filter_control}
;
; SEE ALSO:
;       hsi_filter_control__define
;       hsi_filter_control
;       hsi_filter_info__define
;
; HISTORY:
;       Development for Release 5.1 January-March 2001
;           A Csillaghy, csillag@ssl.berkeley.edu
;
;-
;


;--------------------------------------------------------------------

FUNCTION hsi_filter::INIT, $
	;SOURCE = source, $
	_EXTRA=_extra


;(*
; Here we pass the control and info parameters to the framework, and,
; optionally, the source object.
;*)


RET=self->Framework::INIT( CONTROL = hsi_filter_control(), $
                           INFO={hsi_filter_info}, $
                           ;SOURCE=source, $
                           _EXTRA=_extra )
;(*
; here you may include all operations that may be necessary at initialization.
; *)

RETURN, RET

END

;--------------------------------------------------------------------


PRO hsi_filter::Process, $

	_EXTRA=_extra




energy = Self->Get( /Energy )
det_index = Self->Get( /Det_Index )
atten_state = Self->Get( /Atten_State )
center_thick_ratio = Self->Get(/Center_Thick_Ratio)


data = hsi_shutter_transmission( energy, det_index, atten_state, $
    center_thick_ratio=center_thick_ratio,$
    _extra=_extra)



self->SetData, data


END
;--------------------------------------------------------------------
function hsi_filter::blanket_transmission, energy

;;if not keyword_set( self.blanket ) then begin
;;	bfname = hsi_loc_file(PATH='$HSI_SPEC','hessi_blanket_transmission.txt')
;;	self.blanket  = ptr_new((rd_tfile(/conver,bfname, 2))[*,1:*])
;;	endif
energy = exist(energy) ? energy : Self->Get(/energy)
;;b = *self.blanket
;;fudge  = exp(xsec(b[0,1:*]>1.01<.999e3, 6,'pe')*.015)
;;return, interpol((*self.blanket)[1,1:*]*fudge, (*self.blanket)[0,1:*], energy)
blanket_coeff = Self->Get(/blanket_coeff) ;nominal value is 1.0
return, hsi_blanket_model( energy, coeff=blanket_coeff )  ;ras, 28-apr-2008
end
;--------------------------------------------------------------------


FUNCTION hsi_filter::GetData, $
	blanket=blanket,$
	                  _EXTRA=_extra

data=self->Framework::GetData( _EXTRA = _extra )

if keyword_set(blanket) then data = data*self->blanket_transmission()

RETURN, data
;
END

;--------------------------------------------------------------------



;PRO hsi_filter::Set, $
;       PARAMETER=parameter, $
;       _EXTRA=_extra
;
;
;
;IF Keyword_Set( PARAMETER ) THEN BEGIN
;
;    ; first set the parameter using the original Set
;    self->Framework::Set, PARAMETER = parameter
;
;    ; then take some action that depends on this parameter
;    Take_Some_Action, parameter
;
;ENDIF
;
;
;IF Keyword_Set( _EXTRA ) THEN BEGIN
;    self->Framework::Set, _EXTRA = _extra
;ENDIF
;
;END

;---------------------------------------------------------------------------

;(*
; This shows how to configure the Get function.
; The Get function needs to be modified only in very special cases,
; e.g. if you need to modify a value before passing in back to the
; user.  This is not recommended, however. In any case, you should add two
; keyword variables NOT_FOUND and FOUND that must be passed to the Get
; function in Framework. It is important that self->Framework::Get(...
; ) is called (see end of the routine) such that it can search for
; further parameters in other classes.
; *)

;FUNCTION hsi_filter::Get, $
;                  NOT_FOUND=NOT_found, $
;                  FOUND=found, $
;                  PARAMETER=parameter, $
;                  _EXTRA=_extra
;
;;; not_found and found are needed by Framework::Get() to pass parameters
;;; back
;;
;;;(*
;;; you should change PARAMETER to whatever your paraneter name is
;;; *)
;;
;;IF Keyword_Set( PARAMETER ) THEN BEGIN
;;    parameter_local=self->Framework::Get( /PARAMETER )
;;    ; (*
;;    ; here do whatever needs to be done with parameter as a control
;;    ; *)
;;    Do_Something_With_Parameter, parameter_local
;;ENDIF
;;
;;; here pass the control back to the original Get function. Dont forget
;;; to have NOT_FOUND and FOUND passed to the Get function
;RETURN, self->Framework::Get( PARAMETER = parameter, $
;                              NOT_FOUND=not_found, $
;                              FOUND=found, _EXTRA=_extra )
;END

;---------------------------------------------------------------------------

PRO hsi_filter__Define

self = {hsi_filter, $
		blanket: ptr_new(), $
        INHERITS Framework }

END


;---------------------------------------------------------------------------
; End of 'hsi_filter__define.pro'.
;---------------------------------------------------------------------------

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;+
; Project:
;	XRAY
;
; NAME:
;	Energy_res
; PURPOSE:
;	This procedure generates a matrix of gaussian pulse-shapes which can
;	then multiply a matrix of energy-losses to form a full pulse-height
;	matrix.
;
; CATEGORY:
;	MATH, CALIBRATION, INSTRUMENT, DETECTORS, RESPONSE, SPECTROSCOPY
;
; CALLING SEQUENCE:
;	PULSE_SPREAD, INPUT, PSM, INMATRIX, OUTMATRIX
; EXAMPLES:
;	pulse_spread, input_psm, pulse_shape, eloss_mat.eloss_mat, drm
;
; CALLS:
;	EDGE_PRODUCTS, CHKARG, F_DIV
;
; INPUTS:
;
; OPTIONAL INPUTS:
;
;
; OUTPUTS:
;       PSM - pulse-shape matrix, square or SPARSE
;
; OPTIONAL OUTPUTS:
;
;
; KEYWORDS:
;	SPARSE - if set, then psm is a sparse matrix
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	The GAUSSINT function is used to construct the point-spread function.  GAUSSINT is
;	the integral over the normally used GAUSSIAN function and is the correct function
;	where the Gaussian is a valid approximation only when the output channels are
;	narrow wrt the resolution.  Also, if INMATRIX is given, an efficient matrix
;	multiplication is performed on large matrices, multiplying only over the
;	non-zero elements of INMATRIX, useful when INMATRIX is mainly the photoefficiency
;	without a Compton tail.
;
; MODIFICATION HISTORY:
;	16-jun-2006, ras
;-


function energy_res, _extra=_extra

return, obj_new('energy_res',_extra=_extra)
end
;---------------------------------------------------------------------------
function energy_res_control
var = {energy_res_control}
var.ein = ptr_new(get_edges(/edges_2, findgen(101)+3.5))
var.eout = ptr_new(*var.ein)

var.e_vs_fwhm = ptr_new(findgen(100)+1.)
var.fwhm_vs_e = ptr_new(0.8 + fltarr(100))
var.sig_lim   = 6.0


return, var
end
;---------------------------------------------------------------------------
pro energy_res_control__define

d = {energy_res_control, $
	ein: ptr_new(), $
	eout: ptr_new(),$
	e_vs_fwhm: ptr_new(), $
	fwhm_vs_e: ptr_new(), $
	sig_lim: 0.0 	}

end
;---------------------------------------------------------------------------
pro energy_res_info__define

d = {energy_res_info, $
	info_state: ptr_new(0) }

end

;---------------------------------------------------------------------------
; Document name: energy_res__define.pro
; Created by:    Andre Csillaghy, March 4, 1999
;
; Last Modified: Mon Apr 23 14:19:32 2001 (csillag@soleil)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI FRAMEWORK TEMPLATE CLASS
;
; PURPOSE:
;
;       Compute hessi attenuator transmission
;
; CATEGORY:
;       Spectra
;
; CONTRUCTION:
;       o = Obj_New( 'energy_res' )
;       or
;       o = energy_res( )
;
; INPUT (CONTROL) PARAMETERS:
;       Defined in {energy_res_control}
;
; SEE ALSO:
;       energy_res_control__define
;       energy_res_control
;       energy_res_info__define
;
; HISTORY:
;       Development for Release 5.1 January-March 2001
;           A Csillaghy, csillag@ssl.berkeley.edu
;
;-
;


;--------------------------------------------------------------------

FUNCTION energy_res::INIT, $
	;SOURCE = source, $
	_EXTRA=_extra


;(*
; Here we pass the control and info parameters to the framework, and,
; optionally, the source object.
;*)
RET=self->Framework::INIT( CONTROL = energy_res_control(), $
                           INFO={energy_res_info}, $
                           ;SOURCE=source, $
                           _EXTRA=_extra )
;(*
; here you may include all operations that may be necessary at initialization.
; *)

RETURN, RET

END



;--------------------------------------------------------------------


PRO energy_res::Process, $

             _EXTRA=_extra



ein  = Self->Get(/ein) ;2 x N
eout = Self->Get(/eout) ;2 x M
fwhm_vs_e = Self->Get(/fwhm_vs_e)
e_vs_fwhm = Self->Get(/e_vs_fwhm)

sig_lim = self->Get(/sig_lim)
ninput = n_elements( ein(0,*))
noutput= n_elements( eout(0,*))
edge_products, ein, width=wein, mean=emin
edge_products, eout,  mean=emout
sigmax = interpol(fwhm_vs_e, e_vs_fwhm, emin)/2.36
sigrow = abs((rebin( emout, noutput, ninput)-transpose(rebin(emin,ninput,noutput))) $
	/ transpose( rebin(sigmax, ninput, noutput)) )
psm = fltarr(noutput,ninput)

res_elem = f_div( sigmax, wein )
w1 = where( res_elem ge 2.0, nw1)
w2 = where( res_elem lt 2.0, nw2)

if nw1 ge 1 then for i=0,nw1 - 1 do begin
		ss = where(sigrow[*,w1[i]] le sig_lim, nss)
		if nss eq 0 then ss = lindgen(noutput)
        psm[ss,w1[i]]= (gaussint((eout[1,ss]-emin[w1[i]])/sigmax[w1[i]]) $
             -   gaussint( (eout[0,ss]-emin[w1[i]])/sigmax[w1[i]] ))[*]
        endfor
if nw2 ge 1 then for i=0,nw2 -1 do begin
	ss = where(sigrow[*,w2[i]] le sig_lim, nss)
	if nss eq 0 then ss = lindgen(noutput) else begin

		if ss[0] ge 1 then ss = [ss[0]-1,ss]
		if ss[nss-1] lt (noutput-1) then ss=[ss,ss[nss-1]+1]
		endelse
	nss  = n_elements(ss)
	nbins = ceil(1./res_elem[w2[i]]*4.0)
	enew = interpol( ein[*,w2[i]], nbins+1)
	edge_products, enew, mean=emnew
	emnew=rebin(reform(emnew,1,nbins),nss,nbins)
	e1 = rebin((eout[1,ss])[*],nss,nbins)
	e0 = rebin((eout[0,ss])[*],nss,nbins)
	psm[ss,w2[i]]= rebin( gaussint( (e1-emnew)/sigmax[w2[i]]) $
			  -  gaussint( (e0-emnew)/sigmax[w2[i]]), nss)

	endfor


self->SetData, psm


END


;
;---------------------------------------------------------------------------

PRO energy_res__Define

self = {energy_res, $
        INHERITS Framework }

END


;---------------------------------------------------------------------------
; End of 'energy_res__define.pro'.
;---------------------------------------------------------------------------


;+
;
;Name: HSI_DRM_MOD_CONTROL__DEFINE
;
;Purpose: Definition for control parameters for
;	HSI_DRM_MOD class
;
;drmobj    = Self->Get(/drmobj)
;fwhm_frac = Self->Get(/fwhm_frac)
;offset    = Self->Get(/gain_offset)
;center_thick_ratio = Self->Get(/center_thick_ratio)
;time_wanted = Self->Get(/time_wanted)
;Created:
;	26-jun-2006, richard.schwartz@gsfc.nasa.gov
;-


pro hsi_drm_mod_control__define

dmy = {hsi_drm_mod_control, $
		spex_drm: obj_new(), $
		atten_state: 0, $   ;ras, 10-jul-2006
		fwhm_frac: 0.0, $
		;gain_offset: 0.0, $
		center_thick_ratio: 0.0, $
		blanket_coeff:0.0, $1-may-2008
		time_wanted: ptr_new() $
		}


end

function hsi_drm_mod_control
var = {hsi_drm_mod_control}
var.fwhm_frac = 1.0
var.atten_state = 1  ;ras, 10-jul-2006
;var.gain_offset = 0.0
var.center_thick_ratio = 1.0
var.time_wanted= ptr_new('20-feb-2002 11:06:00')
var.spex_drm = obj_new('framework') ;just to keep from blowing up
var.blanket_coeff = 1.0 ;1-may-2008, ras
return, var
end


pro hsi_drm_mod_info__define
	;Get some stored info parameters
	;fwhm0 = Self->Get(/fwhm0)
	;emin0 = Self->Get(/emin0)
	;time_wanted0 = Self->Get(/time_wanted0)

a ={hsi_drm_mod_info, $
	fwhm0: ptr_new(/alloc), $
	emin0: ptr_new(/alloc), $
	time_wanted0: ptr_new(/alloc) $
	}


end

;---------------------------------------------------------------------------
; Document name: hsi_drm_mod__define.pro
; Created by:    Richard Schwartz, 23-jun-2006
;
; Last Modified: 23-jun-2006
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HSI_DRM_MOD CLASS
;
; PURPOSE:
;
;       This class allows us to quickly build a modifiable
;		drm object for use with the apply_drm function
;		in ospex.  Several detector parameters can be
;		changed on the fly and the modified drm is produced
;		quickly and efficiently
;
;
; CATEGORY:
;       Objects
;
; CONTRUCTION:
;       o = Obj_New( 'hsi_drm_mod' )
;       or
;       o = hsi_drm_mod( )
; EXAMPLE:
;       new_spectrum=o->getdata(photon_spectrum=photon_spectrum, gain_offset=gain_offset)
;
; INPUT (CONTROL) PARAMETERS:
;       Defined in {hsi_drm_mod_control}
; RESTRICTIONS:
;		To be used only at low energy (<100 keV) with 1 hessi front segment's response.
;
;
; SEE ALSO:
;       hsi_drm_mod_control__define
;       hsi_drm_mod_control
;       hsi_drm_mod_info__define
;
; HISTORY:
;
;       Developed June 2006
;           richard.schwartz@gsfc.nasa.gov,
;		10-july-2006, added atten_state to control parameters
;		so we could receive atten_state from the calling program
;		where the real atten_state is known.
;		9-Aug-2006, Kim.  Fixed check for dets used (added str2arr) and call message
;		  without /cont.  Also modified text of error messages for > 1 det, or det # gt 8.
;		30-Oct-2006, Kim.  Added getdata method.  Moved last part of process to getdata so
;		  if the only thing that changed was photon_spectrum or gain, don't need to go
;		  through process again.  Means you have to use photon_spectrum, gain_offset args
;		  in call to getdata though.  They are no longer control params.
;	    2-oct-2009, ras, account for diagonal response vectors as well as diagonal response
;		matrices where the only non-zeroes are on the diagonal
;		24-Apr-2012, Kim.  Added getobj method
;		31-may-2012, ras, need to distinguish between ct_edges and ein edges
;		as they can be different in the low energy range since introducing the INSERT_KEDGE
;		control parameter to {hsi_srm_control}. Now there will be more edges on the
;		photon side as we break the response at the detector and grid edges
;		04-Oct-2012, Kim and Richard.  Added check for whether data is image_cube. If so, then change
;		  simplify for scatter matrices (4,5,6) to 2, and set offax and earth position to defaults.
;		25-Feb-2013, RAS, insert_kedge value made consistent with original drm file, otherwise
;			inconsistent edges will be used which put spikes into the response
;
;-
;


;--------------------------------------------------------------------

FUNCTION hsi_drm_mod::INIT, SOURCE = source, _EXTRA=_extra


;IF NOT Obj_Valid( source ) THEN BEGIN
;    source =  obj_new( 'hsi_drm_mod_source' )
;ENDIF


RET=self->Framework::INIT( CONTROL = hsi_drm_mod_control(), $
                           INFO={hsi_drm_mod_info}, $
                           ;SOURCE=source, $
                           _EXTRA=_extra )
self.srmlld = ptr_new(/alloc)
self.ct_edges = ptr_new(/alloc)
self.ein = ptr_new(/alloc)

RETURN, RET

END

;--------------------------------------------------------------------
; Function to return the various srm objects stored as properties

function hsi_drm_mod::getobj, $
  eres=eres, $        ; return energy resolution matrix object
  fltr=fltr, $        ; return attenuator and blanket object
  det=det, $          ; return detector response object
  grtrans=grtrans, $  ; return grid transmission object
  lld=lld             ; return lower level discriminator object

if keyword_set(eres) then return, self.eresobj ;computes energy resolution matrix
if keyword_set(fltr) then return, self.fltrobj ;attenuator and blanket computation
if keyword_set(det) then return, self.detobj ;detector response object
if keyword_set(grtrans) then return, self.osrm ;grid transmission object
if keyword_set(lld) then return, self.lldobj ;lower level discriminator object
end

;--------------------------------------------------------------------

FUNCTION hsi_drm_mod::GetData, $
	photon_spectrum=photon_spectrum, $
	gain_offset=gain_offset, $
	_EXTRA=_extra

if keyword_set(_extra) then self -> set, _extra=_extra

checkvar, gain_offset, 0.

drm = self -> framework::getdata()

ct_edges = *self.ct_edges
;31-may-2012, ras, need to distinguish between ct_edges and ein edges
;as they can be different in the low energy range since introducing the INSERT_KEDGE
;control parameter to {hsi_srm_control}. Now there will be more edges on the
;photon side as we break the response at the detector and grid edges
wctedg   = get_edges(/width, ct_edges)
nctedg   = n_elements(wctedg)

ein = *self.ein
wein = get_edges(ein, /width)

; if photon_spectrum=1., set to an array of 1.s
photon_spectrum = (n_elements(photon_spectrum) eq 1 and photon_spectrum[0] eq 1.) ? $
	 1.+0.*wein : photon_spectrum

cspectrum = f_div(drm#(photon_spectrum*wein), wein) ;cts/keV

;offset defined as positive shift
cspectrum = interp2integ(ct_edges+gain_offset, ein, cspectrum)/wctedg
cspectrum = cspectrum * (*self.srmlld) ;counts/cm2/s/keV

result = cspectrum  * self.area * wctedg

output = result[0:nctedg-1]
return, output
end

;--------------------------------------------------------------------
; From HESSI_BUILD_SRM
;        0 Full calculation of diagonal, off diagonal terms
;        1 Off diagonal terms are an approximation - for most submatrices
;          this will mean an average of all segments of the right kind
;          (f,r,coinc.) instead of detector-specific results
;        2 Diagonal terms only
;        3 Diagonal terms only, and all = 1 (or zero if
;                           it's a normally off-diagonal matrix)
;
;                     The 9 submatrices are:
;                       0 Grid-pair transmission
;                       1 Attenuator transmission
;                       2 Blanket transmission
;                       3 Det. resp. to modulated photons
;                       4 Det. resp. to scattering in 2nd grid and attenuator
;                       5 Det. resp. to spacecraft scatter
;                       6 Det. resp. to Earth scattering
;                       7 Detector resolution (w/ radiation damage)
;                       8 Extra broadening (no longer in use)
;                       9 Detector LLD threshold cutoff

PRO hsi_drm_mod::Process, $

             _EXTRA=_extra

Self->Set, _extra=_extra

drmobj    = Self->Get(/spex_drm)
if not obj_isa( drmobj,'SPEX_DRM') then begin
;need drmobj, not passed yet.
	message,/continue,'Must Set a valid SPEX_DRM object: obj->Set->spex_drm=spex_drm_class_object'
	Self->SetData, -1
	endif

dataobj = drmobj->get(/spex_drm_dataobj)  ; get spex_data object from spex_drm object
image_cube = dataobj -> is_image_input()

fwhm_frac = Self->Get(/fwhm_frac)
;offset    = Self->Get(/gain_offset)
center_thick_ratio = Self->Get(/center_thick_ratio)
blanket_coeff      = Self->Get(/blanket_coeff)
time_wanted = Self->Get(/time_wanted)
;photon_spectrum=Self->Get(/photon_spectrum)
;print,' fwhm, offset, thick', fwhm_frac, offset, center_thick_ratio

if not OBJ_ISA(self.eresobj, 'ENERGY_RES') $
	then self.eresobj = energy_res()
if not OBJ_ISA(self.fltrobj, 'HSI_FILTER') then $
	self.fltrobj = hsi_filter()
eresobj  = self.eresobj
fltrobj  = self.fltrobj


default, etrans, 11.0 ;transition from small sens area and large
default, esigma, 2.0 ;scaling parameter for trans from fwhm_min to fwhm

simplify = bytarr(10)

det_index = hessi_id2index( str2arr(drmobj->get(/spex_drm_detused), ' ') )
if n_elements( det_index ) ne 1 then $
    message, 'Can not use drm_mod with more than one detector segment.  Aborting.'

if det_index[0] gt 8 then $
    message, 'Selected detector segment must be <= 8.  Aborting.'

det_index = det_index[0]
use_vird = bytarr(18)
use_vird[det_index]=1b

if not OBJ_ISA( self.osrm, 'HSI_SRM') then begin
	self.osrm= hsi_srm()
	osrm=self.osrm

	simplify = simplify * 0+3
	simplify[0]=2
	osrm->set, simplify=simplify
	osrm->set, compute_offax = 0

	endif

simplify = bytarr(10)+3
simplify[ [3,4,5,6] ] = image_cube ? [0,2,2,2] : 0
if not OBJ_ISA( self.detobj, 'HSI_SRM') then begin
	detobj=hsi_srm()
	self.detobj = detobj
	detobj->set, simplify=simplify
	detobj->set, compute_offax = 0
	endif else self.detobj->set, simplify=simplify

if not OBJ_ISA(self.lldobj, 'HSI_SRM') then begin
	lldobj=hsi_srm()
	self.lldobj = lldobj

	simplify = simplify * 0 +3
	simplify[9]=2
	lldobj->set, simplify=simplify, compute_offax = 0
	endif

detobj=self.detobj
osrm  =self.osrm
lldobj=self.lldobj
extnam   = 'SRM Object Parameters'
drmfile  = drmobj->get(/spex_drmfile)
extnum   = get_fits_extno(drmfile, extnam)
drmpar   = mrdfits(drmfile, extnum, /silent)
;if insert_kedge wasn't allowed originally, or not imagined, we
;can't allow it here
insert_kedge = get_tag_value(drmpar, /insert_kedge, /quiet)



; get offax_position and earth_position from SRM object param extension of FITS file.  If image cube
; input then set them to defaults.
if image_cube then begin
  defaults = hsi_srm_control()
  offax_position = *defaults.offax_position
  earth_position = *defaults.earth_position
endif else begin
  offax_position = drmpar.offax_position
  earth_position = drmpar.earth_position
endelse

ct_edges = drmobj->Get(/spex_drm_ct_edges)
if size(/tname, ct_edges) eq 'POINTER' then begin
    olddrm   = drmobj->getdata()
    ct_edges = drmobj->Get(/spex_drm_ct_edges)
    endif

atten_state = self->Get(/atten_state) ;ras, 10-jul-2006


ein = drmobj->Get(/spex_drm_ph_edges)
edge_products, ein, edges_1=ein1, mean=emin, width=wein, edges_2=ein

nph= n_elements(ein1)

osrm->set,use_seg=use_vird, offax_position= offax_position, $
	ct_edges = ein1, ph_edges= ein1, insert_kedge = insert_kedge
gtran = osrm->getdata()

id    = lindgen(nph-1)*nph
;gtran may be an diagonal array or vector, account for either case
;ras, 2-oct-2009
gridtran =size(gtran,  /n_dim) eq 1 ? gtran : gtran[id]*wein

fltrobj->Set, energy = emin, det_index=det_index, atten_state=atten_state, $
	center_thick_ratio = center_thick_ratio, blanket_coeff=blanket_coeff


att_blkt = fltrobj->getdata(/blanket) ; includes blanket_coeff strength factor on "material"

	;endif
detobj->set,use_seg=use_vird, ct_edges = ein1, ph_edges=ein1, /pha_on_row,$
	earth_position = earth_position, insert_kedge = insert_kedge
srmf = (detobj->getdata())[*, 0:nph-2]
self.area = detobj->get(/geom_area)


lldobj->set,use_seg=use_vird, ct_edges = ein1, ph_edges=ein1, /pha_on_row,$
	insert_kedge = insert_kedge
lldata = lldobj->getdata()
;lldata may be an diagonal array or vector, account for either case
;ras, 2-oct-2009
*self.srmlld = size(/n_dim, lldata) ? lldata : (lldobj->getdata())[id]*wein

;Get some stored info parameters
fwhm0 = Self->Get(/fwhm0)
emin0 = Self->Get(/emin0)
time_wanted0 = Self->Get(/time_wanted0)

if not keyword_set(fwhm0) or $
	not same_data(emin, emin0) or not same_data(time_wanted,time_wanted0) then begin

	resolutions_default = hsi_default_resolutions(time_wanted=time_wanted)
	fwhm = hsi_calc_resolutions(emin, resolutions_default, det_index)
	Self->Set, time_wanted0=time_wanted
	Self->Set, fwhm0=fwhm
	Self->Set, emin0=emin
	endif else fwhm = fwhm0


mixcoef = gaussint( (emin-etrans)/esigma )
fwmix = (fwhm * fwhm_frac)*(1.-mixcoef) + fwhm * mixcoef


eresobj->set, ein=ein, eout=ein, e_vs_fwhm=emin, fwhm_vs_e = fwmix, sig_lim=8.,$
	insert_kedge = insert_kedge
psm = eresobj->getdata()

nwein = n_elements(wein)
wwein    = rebin( wein, nwein, nwein)

trans = rebin(transpose(gridtran*att_blkt), nwein,nwein)

subsrm = (srmf*wwein)*trans
drm =psm# subsrm

self->setdata, drm

*self.ct_edges = ct_edges
*self.ein = ein

;cspectrum = f_div(drm#(photon_spectrum*wein), wein) ;cts/keV
;
;;offset defined as positive shift
;cspectrum = interp2integ(ein, ein+offset, cspectrum)/wein
;cspectrum = cspectrum * srmlld ;counts/cm2/s/keV
;
;result = cspectrum  * area * wein
;
;output = result[0:nctedg-1]

;self->SetData, output

END

;---------------------------------------------------------------------------

PRO hsi_drm_mod__Define

self = {hsi_drm_mod, $
	eresobj: obj_new(), $ ;computes energy resolution matrix
	fltrobj: obj_new(), $ ;attenuator and blanket computation
	detobj: obj_new(),$   ;detector response object
	osrm: obj_new(), $    ;grid transmission object
	lldobj: obj_new(), $  ;lower level discriminator object
	srmlld: ptr_new(), $
	area: 0., $
	ct_edges: ptr_new(), $
	ein: ptr_new(), $
        INHERITS Framework }

END

;---------------------------------------------------------------------------
; End of 'hsi_drm_mod__define.pro'.
;---------------------------------------------------------------------------
