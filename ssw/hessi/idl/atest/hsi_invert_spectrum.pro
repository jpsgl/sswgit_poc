;+
;Hacked from hsi_qlook_call_ospex, this inverts the response matrix and
;returns photon spectra.
;-
Pro hsi_invert_spectrum, specfile, srmfile, st_time, en_time, $
                         spectrum, uspectrum, ebands, srate_out = srate_out, $
                         rate_out = rate_out, brate_out = brate_out, $
                         sbrate_out = sbrate_out, tdata_out = tdata_out, $
                         seg_index_mask = seg_index_mask, sys_err = sys_err, $
                         minchan = minchan, _extra = _extra
;Be sure not to have any windows...
  set_logenv, 'OSPEX_NOINTERACTIVE', '1'
;Gotta have a good catch
  spectrum = -1 & uspectrum = -1
  rate_out = -1 & brate_out = -1 & sbrate_out = -1 & tdata_out = -1
  err = 0
  catch, err
  If(err Ne 0) Then Begin
    catch, /cancel
    Print, 'Oops'
    If(obj_valid(o)) Then obj_destroy, o
    spectrum = -1 & uspectrum = -1
    rate_out = -1 & brate_out = -1 & sbrate_out = -1 & tdata_out = -1
    help, /last_message, output = err_msg
    For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
    print, 'Returning'
    Return
  Endif
  If(keyword_set(sys_err)) Then serr = sys_err Else serr = 0.20 ;estimated
  If(keyword_set(seg_index_mask)) Then Segi = seg_index_mask $
  Else segi = [1, 0, 1, 1, 0, 1, 0, 0, 1, bytarr(9)]
;First check the input
  If(is_string(specfile) Eq 0) Then message, 'No specfile input'
  If(is_string(srmfile) Eq 0) Then message, 'No srmfile input'
;It's possible to have only one srmfile
  n = n_elements(specfile)
  If(n Gt 1) Then Begin
    If(n_elements(srmfile) Ne n) Then srmfile = replicate(srmfile[0], n)
  Endif
  ntimes = n_elements(st_time)
  tr = dblarr(2, ntimes)
  tr[0, *] = anytim(st_time)
  tr[1, *] = anytim(en_time)
;Get the object, do the fit
  o = ospex(/no_gui)
  o -> set, spex_specfile = specfile[0], spex_drmfile = srmfile[0], $
    spex_fit_time_interval = tr
  d = o -> getdata(class = 'spex_data')
  odrm = o -> get(class = 'spex_drm', /object_reference)
  dummy = odrm -> getdata()     ;need this to get the area
  i = o -> get()                ;gives info parameters
;check rate data
  If(is_struct(d) Eq 0) Then message, 'No data'
  If(total(d.data) Le 0) Then message, 'No Non-zero data'
;d.data is counts, corrected for live time
  nchan = n_elements(d.data[*, 0])
  nt = n_elements(d.data[0, *])
  ebands = [reform(i.spex_ct_edges[0, *]), i.spex_ct_edges[1, nchan-1]]
;grab the background spectrum, for each time
;  hsi_new_spec_bck, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
;    seg_index_mask = segi
;  hsi_qlook_spec_bck, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
;    seg_index_mask = segi
;  hsi_spec_bck, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
;    seg_index_mask = segi, /vs_time
  hsi_spec_bck2, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
    seg_index_mask = segi
;brate1 and sbrate1 are not per detector anymore, 18-nov-2005, jmm
  bcounts1 = rebin(brate1, nchan, nt, /sample)*d.ltime
  sbcounts1 = rebin(sbrate1, nchan, nt, /sample)*d.ltime
;Do the background subtraction
  rate_out = d.data/d.ltime
  srate_out = sqrt(rate_out/d.ltime)+serr*rate_out
  brate_out = brate1
  sbrate_out = sbrate1
  d.data = d.data-bcounts1
  d.edata = sqrt(d.edata^2+sbcounts1^2+serr^2*(d.data+bcounts1)^2)
;Reset the data
  o -> replacedata, d.data, /orig, /counts
  o -> replacedata, d.edata, /orig, /error
;Set some defaults
  o -> set, spex_erange = [min(i.spex_ct_edges), max(i.spex_ct_edges)], $
    fit_function = 'vth+3pow', spex_fit_manual = 0, mcurvefit_itmax = 50
  o -> set, _extra = _extra     ;eranges, fit_comp_free, etc...
;hmmm, here you need to initialize paramters, get flux_conversion
;factors, do the fits in a loop
  ntimes = n_elements(st_time)
;Do each interval
  d = o -> getdata(class = 'spex_data')
  i = o -> get()                ;gives info parameters
  e = 0.5*total(i.spex_ct_edges, 1)
  nch = n_elements(e)
  spectrum = fltarr(nch, ntimes) & uspectrum = spectrum
  tdata_out = spectrum & tdata_out[*] = 0
;minchan give the minimum energy channel to use
  If(keyword_set(minchan)) Then mch = minchan Else mch = 0
  For j = 0, ntimes-1 Do Begin
;Get ok channels for spectral fit
;You have to sum the data here
    in_this_intv = where(i.spex_ut_edges[0, *] Ge st_time[j] And $
                         i.spex_ut_edges[0, *] lt en_time[j], nin_this_intv)
    If(nin_this_intv Eq 0) Then $
      message, /info, 'No Counts in interval'+string(j)
;be sure to use count rates
    ddata = d.data[*, in_this_intv]/d.ltime[*, in_this_intv]
    sddata = d.edata[*, in_this_intv]^2/d.ltime[*, in_this_intv]^2
    If(nin_this_intv Gt 1) Then Begin ;average rate
      ddata = total(ddata, 2)/float(nin_this_intv)
      sddata = sqrt(total(sddata^2, 2))/float(nin_this_intv)
    Endif
;here I have to do a fit to get the filters? But only once...
    If(j Eq 0) Then Begin
      o -> set, fit_comp_param = [0.10, 1.0], spex_erange = [3.0, 12.0], $
        fit_comp_free = [1, 1, 0, 0, 0, 0, 0, 0, 0]
      o -> dofit, spex_intervals_tofit = j
    Endif
;Now, invert the matrix
;get the appropriate att_state for this interval.
    odrm = o -> get(class = 'spex_drm', /object_reference)
    filters = o -> get(/spex_fitint_filter)
    If(filters[0] Ne -1) Then Begin
      drm = odrm -> getdata(this_filter = filters[j])
    Endif Else drm = odrm -> getdata()
    drm = drm[mch:nch-1, mch:nch-1] ;square matrix
    rdrm = invert(drm)
    phflux = rdrm#ddata[mch:*]
    cfactor = fltarr(nch-mch)
    ddata = ddata[mch:*]
    okch = where(ddata Gt 0.0, nokch)
    If(nokch Gt 0) Then cfactor[okch] = phflux[okch]/ddata[okch]
    sphflux = sqrt(cfactor^2*sddata^2)
    spectrum[mch:*, j] = phflux
    uspectrum[mch:*, j] = sphflux
    tdata_out[mch:*, j] = drm#phflux
  Endfor
  If(obj_valid(o)) Then obj_destroy, o
  Return
End



