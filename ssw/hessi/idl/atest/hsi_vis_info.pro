;+
;
; Name: hsi_vis_info
;
; Purpose: Return a structure providing information about either a visibility structure or file
;
; Calling sequence: struct = hsi_vis_info(visin)
;
; Input Argument: 
;   visin - visibility structure or name of visibility FITS file
;
; Output Argument:  
;   visobj - vis object that was created if we read a file (optional)
; 
; Input Keyword Arguments:
;   quiet - if set, don't print messages about incorrect input
;
; Output:
;   structure containing time bin array, energy bin array, units, type of vis, detectors used, normalization factors, and
;      a string giving time range and energy range
;
; Example:
;   s = hsi_vis_info(v)
;   help,s
;     ** Structure <24a80950>, 10 tags, length=536, data length=526, refs=1:
;        NVIS            LONG              2274
;        NTIMES          LONG                 1
;        NENERGIES       LONG                10
;        TYPE            STRING    'photon'
;        UNITS           STRING    'Photons cm!u-2!n s!u-1!n'
;        TBINS           DOUBLE    Array[2]
;        EBINS           FLOAT     Array[2, 10]
;        DETS            INT       Array[5]
;        NORM_PH_FACTOR  FLOAT     Array[9, 10]
;        SUMMARY         STRING    '25-Sep-2011 03:32:00.000-03:32:30.000 10-30 keV'
;
; Written: Kim Tolbert, 22-Jun-2017
; Modifications:
;   09-Jul-2018, Kim. Changed order of minmax(trim(ebins)) to trim(minmax(ebins))
;   17-Jul-2018, Kim. Added visobj argument and quiet keyword 
;
;-

function hsi_vis_info, visin, visobj, quiet=quiet

  quiet = keyword_set(quiet)

  visobj = -1

  if is_struct(visin) then begin
    vis = visin
  endif else begin
    if file_exist(visin) then begin
      if get_fits_extno(visin, 'visibility') ne -1 then begin
        visobj = hsi_visibility(vis_input_fits=visin)
        vis = visobj->getdata(/all)
        ; if visobj argument was passed in, then it has a life outside this routine, so don't destroy
        if ~arg_present(visobj) then destroy, visobj
      endif else begin
        if ~quiet then message, 'File is not a valid visibility file: ' + visin, /info
        return, -1
      endelse
    endif else begin
      if ~quiet then message,'Please enter either a visibility structure or a visibility file name.', /info
      return, -1
    endelse
  endelse

  nvis = n_elements(vis)

  tbins = get_uniq_range(vis.trange)
  ebins = get_uniq_range(vis.erange)
  nt  = n_elements(tbins[0,*])
  neb = n_elements(ebins[0,*])

  dets = get_uniq(vis.isc)

  norm_ph_factor = fltarr(9, neb, nt)

  if have_tag(vis,'norm_ph_factor') then begin
    for it=0,nt-1 do begin
      for ie=0,neb-1 do begin
        for idet=0,8 do begin
          q = where(vis.trange[0] eq tbins[0,it] and vis.erange[0] eq ebins[0,ie] and vis.isc eq idet, c)
          if c gt 0 then norm_ph_factor[idet, ie, it] = vis[q[0]].norm_ph_factor
        endfor
      endfor
    endfor
  endif

  summary = anytim(min(tbins), /vms) + '-' + anytim(max(tbins), /time_only, /vms) + ' ' + $
    arr2str( trim(minmax(ebins)), '-') + ' keV'
  struct = {nvis: nvis, $
    ntimes: nt, $
    nenergies: neb, $
    type: vis[0].type, $
    units: vis[0].units, $
    tbins: tbins, $
    ebins: ebins, $
    dets: dets, $
    norm_ph_factor: norm_ph_factor, $
    summary: summary}

  return, struct
end