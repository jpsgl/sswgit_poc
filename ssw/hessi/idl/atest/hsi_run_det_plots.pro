; Run rhessi detector plots for all flares in the last 10 days and puts new plot locations in catalog for Browser.
; This is run in a cron job under softw on hesperia.
; 
; To run for a different time interval, call hsi_spectrum_mk_det_plots directly with /archive, e.g.
; hsi_spectrum_mk_det_plots, tr=['20-jun-2014','26-jun-2014'], /archive
; 
; Kim Tolbert, 24-July-2014
; 
; 
; This is how I ran the whole mission one day at a time, changing time and tend to do all years in batches:
;time = anytim('1-jan-2002')
;tend = anytim('1-jan-2004')
;while time lt tend do begin tr=time & hsi_spectrum_mk_det_plots, tr=tr, /archive & time = time+86400. & endwhile

!quiet=1
cd,'/home/softw/hessi/det_plots'

; set time range to current date minus 11 days through yesterday
tr = anytim(!stime, /date) + [-10.*86400., -1.]
message, /cont, 'Running hsi_spectrum_mk_det_plots to make detector spectrum plots for ' + format_intervals(tr,/ut,/end_date)
hsi_spectrum_mk_det_plots, tr=tr, /archive

end
