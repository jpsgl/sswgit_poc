
;+
;NAME:
;   HSI_PIXON_IMAGE
;PURPOSE:
;     RHESSI Image reconstruction using the fractal pixon basis of Pina and
;     Puetter.
;CATEGORY:
;CALLING SEQUENCE:
;     image = hsi_pixon_image(countrate_obj, sigma[, residual, gof, error])
;INPUTS:
;     countrate_obj = HESSI calib event list
;     sigma = error on the data in "data", same size as data.
;             This is a very important parameter since it
;             is used to find the resolution of the image ...
;             get the best estimate you can!
;             Otherwise use /poisson.
;OPTIONAL INPUT PARAMETERS:
;KEYWORD PARAMETERS
;     sensitivity = sensitivity in creating the pixon map.
;                   default = 0.,
;                   0.0 is most sensitive, >3.0 is very insensitive.  The
;                   default was arrived at after much testing and simulation.
;                   Don't mess with it unless you know what you are doing.
;                   If sensitivity is set too small, you may get spurious
;                   sources; too large and you may miss real sources.
;     guess = initial guess for reconstructed image (def = compute with
;             hxt_qlook).  If guess is not nx by ny or if all elements
;             of guess are LE 0, then the guess is not used.
;     resolution = Minimum resolution in pixels (def = 1).
;     pixon_sizes = an integer list of pixon sizes (resolutions) to use.
;                   default = powers of sqrt(2.0).  Slower, but more robust,
;                   would be something like pixon_sizes=indgen(17).
;     /snr = Weight the residuals by the signal-to-noise ratio.
;     /chisqr = Use chi square statistics rather that poisson statistics.
;               sigma will be used if set.  If sigma is not set,
;               the Poisson statistics ala /poisson are used.
;     /poisson = Use Poisson statistics rather than chi^2.  If this keyword
;                is set, sigma is not used.
;     iterate = returns with the number of iterations used at the final
;               resolution.
;     outresolution = returns with the final resolution, i.e. the minimum
;                     pixon size.
;     /notty = Not running on a tty.  Set this for background jobs.
;     /quiet = Work quietly.
;     btot_in = An estimate of the total # of counts in the image
;                 The default is a weighted sum of the counts in the SC's
;     dummydir = replaced unused smpattwritedir which was never really used
;     /xycoordinate = if set, return the image in XY coordinates rather that
;                     annsec coordinates.
;     full_pm_calc - implement full pixon map computation, overrides dosnr bit 2(value 2)
;     background_model - if set, include background model, overrides dosnr bit 3(value 4)
;     variable_metric - if set, use dfpmin for solution, overrides dosnr bit 4(value 8)
;
;OUTPUTS:
;     image = reconstructed image
;OPTIONAL OUTPUTS:
;     residual = data residuals
;     gof = goodness of fit
;     error = Outputs the estimated error on the reconstructed image
;COMMON BLOCKS:
;SIDE EFFECTS:
;RESTRICTIONS:
;     Convolutions are done with FFT so the procedure is considerably
;     faster when the image size is a power of 2.
;
;     The FFT also wraps the edges, so emission at the edges is not
;     treated properly.  This could be corrected by taking the /fft out
;     of all the calls to hsi_pixon_local_smooth, but that is much
;     slower.  It could also be fixed by setting guard_width>0.
;PROCEDURE:
;MODIFICATION HISTORY:
;     T. Metcalf  1999-Aug-20 Converted HXT code for HESSI use.
;     T. Metcalf  2002-Oct-31 Changed the way the BG pixon is treated
;                             when not included in the pixon_sizes
;                             list.  Also, reset the pseudo to the
;                             image before each image iteration.
;     T. Metcalf  2002-Nov-08 Sometime in the past few months the
;                             normalization of the back projection
;                             changed. This messed up the gradient
;                             calculation here.  Fixed by including the
;                             /no_spatial_freq keyword in broj call.
;     T. Metcal f 2002-Nov-13 Removed the error calculation.  It
;                             should be done at a higher level using
;                             hsi_calc_image_error.pro
;     T. Metcalf 2004-Feb-28  Added the dfpmin option using bit 3 in
;                             dosnr.
;     T. Metcalf 2004-Nov-08  Added error checking around dfpmin
;                             call.
;     T. Metcalf 2007-Jan-07  Added /old to delvarx call so that memory won't be freed
;     T. Metcalf 2007-Mar-19  Guard against crash when pmaptime is undefined
;      4-may-2010, richard.schwartz@nasa.gov change from number of data pts, nd, to nodropout, the number of
;      true datapoints, the ones with finite livetimes
;      7-may-2010, richard.schwartz@nasa.gov, added NORM_NOZERO keyword to hsi_pixon_image with default of 1
;      default is to use it with the new way to use NODROPOUT referenced above.
;      30-dec-2014, richard.schwartz@nasa.gov, rearrange computation of pixon sizes so that the integer values are
;       really integers! -
;       pixon_sizes = [sqrt(2.0)^findgen(25)]  ; logarithmic spaced pixons
;       pixon_sizes = 2.0 ^(findgen(25)/2.)    ;keeps the even index values true integers
;       ras, 20-jan-2014 - make this more efficient by passing cbe_ptr (cbe pointers)
;      	and map_ptr (modpat pointers) in gof_func
;      27-jan-2015, insert keywords that match control parameters controlling bits of
;      snr(dosnr), full_pm_calc, background_model, and variable_metric (use_dfpmin and incorrectly called fast method)
;      15-jun-2015, ras, unified managment of graphics windows to reuse the previous instance of the map, rate and background
;      windows.  Added hsi_pixon_windows_init
;      28-mar-2017, ras, get calib_eventlist object and use it to get possibly modified det_eff using
;        ocbe->get_det_eff()
;      05-apr-2017, ras, replaced all instances of "a2d" with "det", all commas followed by a space
;      17-jun-2017, ras, modified to support cartesian and annsec modpats
;      10-sep-2017, ras, removed some unused commented blocks
;      27-oct-2017, RAS, vis_obj added for vis cbe support, _extra_extra added for keyword inheritance
;      15-nov-2017, RAS, propagating corrfac and removing smpattwritedir and cleaning out detritus, added
;      dummydir as common block placeholder for smpattwritedir, simplified usepoisson outcome, added hsi_pixon_dosnr
;      to manage those selection bits, added hsi_pixon_progbar to avoid clutter
;
;
;-
pro hsi_pixon_windows_init, Self, xsize = xsize, ysize = ysize, $
  title = title, $
  rate = rate, map=map, background = background, $
  windows = ww ;using ww for the windows structure to increase readability (I hope)

  device, get_screen_size=scrsz
  ww = is_struct( ww ) ? ww  : {hsi_pixon_windows} ;['map', 'rate', 'background']
  choice = keyword_set( rate ) + keyword_set( background ) * 2
  windex = ww.(choice)
  reusemapwindows = Self->Get(/pixon_reusemapwindows )
  new_window = ( choice eq 1 and ww.map eq ww.rate ) || ( choice eq 2 and ( ww.map eq ww.background or ww.rate eq ww.background ) )

  if new_window || windex eq 0  || ~reusemapwindows || ~is_wopen( windex ) then begin
    window, /free, xsize = xsize, ysize = ysize, title = title
    ww.(choice) = !d.window
  endif



end

function hsi_pixon_gof_func, image, gradient, nosmooth=nosmooth, local=lcsin, reset=reset, pixon=pixon, residual_image=residual_image, strictgradient=strictgradient

  ; This function is called in the conjugate gradient minimization.  It is a mess
  ; because the only way to pass parameters is through common blocks.  Yuck.
  ;
  ; If the gradient parameter is present, the chi^2 and gradient of chi^2 are
  ; computed, else only chi^2 is computed.
  ;
  ;
  common hsi_pixon_gof_private, nx, ny, nxy, $
    expansion, tty, not_quiet, $
    usepoisson, uselogarithm, usemem, $
    old_lcs, old_det, $
    snr_sigma2, $
    local, pimage, pfraction, imax, $
    iobj, dobj, corrfac,  data, det, harmonics, $
    tot_smoothed_patts, sfilename, firstgradcall, $
    pixon_sizes, dummydir, bobj, bflat, cbe_ptr, scelist, $
    map_ptr, time_unit, time_bin, det_eff ;added 5-jan-2015


  residual_image=0

  if n_elements(lcsin) EQ nxy then lcs = lcsin else lcs = local

  if keyword_set(reset) then begin
    old_lcs = make_array(size=size(lcs), value=0L)
    firstgradcall = 1
  endif

  if uselogarithm then $
    eimage = exp((reform(uselogarithm*image, nx, ny)<Imax)>(-20.)) $
  else begin
    ; positivity constraint w/ MEM
    mximage = max(image)
    if mximage LE 0.0 then image[*] = Imax/1.e32
    eimage = reform(image>(mximage/1.e32), nx, ny)<Imax
  endelse

  ; Apply the pixon map (local correlation scales) to image to obtain
  ; the smoothed image, pimage.

  pimage = hsi_pixon_local_smooth(eimage, lcs, pixon_sizes, fft=1)

  ; MEM Prior
  if usemem then begin
    ptotal = total(pimage)
    lambda = 2.0/ptotal
    mimage = alog((pimage>(max(pimage)/1.e32))/(ptotal*pfraction))
  endif

  ; Update the residuals and the GOF using the smoothed image

  residuals = hsi_pixon_residuals(reform(pimage, nxy), dobj, iobj, corrfac,  det, harmonics, $
    modprofile=recon_data, nounits=0, $
    background=bobj, cbe_ptr = cbe_ptr, map_ptr = map_ptr, $
    time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)

  ; add in the background so that the algorithm strives to remove
  ; the correlation and bias  with the image rather than the background,
  ; if at all possible
  ;bresiduals = residuals + bflat

  if n_params() EQ 2 then begin
    gof = hsi_pixon_gof_calc(data, recon_data, residuals, snr_sigma2, r2s, $
      poisson=usepoisson, iobj=iobj, dobj=dobj, $
      det=det, harmonics=harmonics )
  endif else begin
    gof = hsi_pixon_gof_calc(data, recon_data, residuals, snr_sigma2, $
      poisson=usepoisson )
  endelse

  if usemem then gof = gof + lambda*total(pimage*mimage)

  ; Add total flux adjustment ... unbias residuals.  TRM 1997-01-22

  beta = 1.0/total(1./snr_sigma2)
  gof = gof + beta*(total(residuals))^2

  if tty then begin

    tvscl, rebin(pimage, nx*expansion, ny*expansion, /sample), nx*0+20, 0
    xyouts, nx*expansion/2.+20, 0., /device, align=0.5, $
      strcompress(string(gof, format='(g10.3)')+' '+string(max(pimage), format='(g8.3)')+' '+ $
      string(min(pimage), format = '(g10.3)'))
  endif

  ; Compute gradient of GOF, if requested

  if n_params() EQ 2 then begin

    if n_elements(old_lcs) NE nxy then $
      old_lcs = make_array(size=size(lcs), value=0L)
    if n_elements(old_det) LE 0 then old_det = det
    differ = where(lcs NE old_lcs, ndiffer)

    if ndiffer GT 0 OR n_elements(det) NE n_elements(old_det) then begin
      old_lcs = lcs
      old_det = det

      ;******************************************
      ; Set up smoothed mod patterns here
      ;******************************************
      ; use smoothed patterns here.  If someone hits ctrl-C while
      ; the filename is set to the smoothed patterns, later processes
      ; could be using the wrong mod patts.  How can this be fixed?
      tot_smoothed_patts = hsi_pixon_bproj(make_array(size=size(r2s), value=1.0), $
        dobj, iobj, det, harmonics, /vanilla, useunits=1, $
        setunits=firstgradcall, /smoothpatts, $
        pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
        pixonmap=lcs, /nocheck, $
        reset_smoothpatts=firstgradcall, /nonorm, $
        ;ras, 2-jan-2014 - make this more efficient by passing cbe_ptr (cbe pointers)
        cbe_ptr = cbe_ptr, $ ;since we have them pass them on where they can be used
        dummydir=dummydir, not_quiet=not_quiet, /no_spatial_freq, $
        map_ptr = map_ptr, $
        time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
      ; iobj->Set, modpat_filename=filename; restore unsmoothed mod patts
      if keyword_set(firstgradcall) then firstgradcall=0
    endif

    ; use smoothed patterns here. If someone hits ctrl-C while
    ; the filename is set to the smoothed patterns, later processes
    ; could be using the wrong mod patts.  How can this be fixed?

    gradient = hsi_pixon_bproj(r2s, dobj, iobj,  det, harmonics, $
      vanilla=1, useunits=1, $
      setunits=firstgradcall, $
      /nocheck, $
      reset_smoothpatts=firstgradcall, $
      pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
      pixonmap=lcs, $
      cbe_ptr = cbe_ptr, $
      smoothpatts=1, /nonorm, $
      dummydir=dummydir, not_quiet=not_quiet, /no_spatial_freq, $
      map_ptr = map_ptr, $
      time_unit = time_unit, time_bin = time_bin, det_eff = det_eff )

    if keyword_set(firstgradcall) then firstgradcall=0


    if NOT keyword_set(strictgradient) then begin
      ; gradient of unbiased residuals term.

      gradient = gradient+(2.0*beta*total(residuals))*tot_smoothed_patts

      if usemem then begin
        gradient = gradient + lambda*hsi_pixon_local_smooth((1.+mimage), lcs, pixon_sizes, fft=1)
      endif
    endif

    if tty then begin
      tvscl, rebin(reform(gradient, nx, ny), nx*expansion, ny*expansion, /sample), nx*expansion+20, 0
      xyouts, 1.5*nx*expansion+20, 0., /device, align=0.5, $
        strcompress(string(max(gradient))+' '+string(min(gradient)))
    endif

    ; If the log of the image is being used, then the gradient should
    ; be multiplied by the image itself since d/d(ln x) = x * d/dx.

    if keyword_set(uselogarithm) then $
      gradient=uselogarithm*gradient*eimage


  endif

  ;if NOT finite(gof) then stop
  ;if n_elements(gradient) GT 0 then $
  ;   if min(finite(gradient)) LE 0 then stop

  return, gof

end

;-----------------------------------------------

function hsi_pixon_dfp_grad_func, image
  common hsi_pixon_dfp_private, dfpscale
  gof = hsi_pixon_gof_func(image*dfpscale[1], gradient)/dfpscale[0]
  return, gradient*(dfpscale[1]/dfpscale[0])
end

function hsi_pixon_dfp_gof_func, image
  common hsi_pixon_dfp_private, dfpscale
  gof = hsi_pixon_gof_func(image*dfpscale[1])/dfpscale[0]
  return, gof
end

;-----------------------------------------------

function hsi_pixon_map, image, iobj, local, data, dobj, sobj, corrfac, snr_sigma2, resolution, nx, ny, nxy, nd, $
  pixon_sizes, det, harmonics, pfraction=pfraction, $
  uselogarithm=uselogarithm, Imax=Imax, npixons=npixons, $
  usepoisson=usepoisson, sensitivity=sensitivity, $
  det_resolution=det_resolution, $
  not_quiet=not_quiet, tty=tty, expansion=expansion, $
  fast=fast, dummydir=dummydir, $
  nolastprint=nolastprint, bobj=bobj, bflat=bflat, $
  map_ptr = map_ptr, cbe_ptr = cbe_ptr, $
  time_unit = time_unit, time_bin = time_bin, pixel_size = pixel_size, det_eff = det_eff

  ;NAME:
  ;     HSI_PIXON_MAP
  ;PURPOSE:
  ;     Computes the pixon map
  ;CATEGORY:
  ;CALLING SEQUENCE:
  ;     pmap = hsi_pixon_map(image, local, data, snr_sigma2, resolution, nx, ny, nd,
  ;                          guard_width, patt, pixon_sizes)
  ;INPUTS:
  ;     image = the current image estimate
  ;     local = the old pixon map
  ;     data = the data
  ;     snr_sigma2 = 1/sigma^2 for each data point
  ;     resolution = the new pixon size to try at each pixel
  ;     nx, ny, nd = the dimensions of the image and data
  ;     pixon_sizes = the list of all pixon sizes
  ;OPTIONAL INPUT PARAMETERS:
  ;KEYWORD PARAMETERS:
  ;     sensitivity = sensitivity in creating the pixon map.
  ;                   default = 0.0,
  ;                   0.0 is most sensitive, 3.0 is very insensitive.
  ;     /uselogarithm = the image is the log of the true image
  ;     Imax = the maximum allowed intensity in image.
  ;     /usepoisson = Use Poisson distribution for GOF
  ;     /fast = Use the fast algorithm to compute the pixon map.  It is
  ;             approximate, but MUCH faster than the exact algorithm.
  ;             Tom M. set FAST to 1 in the calling programs so this is the defacto value
  ;OUTPUTS:
  ;     pmap = the new pixon map
  ;     npixons = the number of pixons in the computed pixon map (keyword)
  ;     pfraction = the fraction of a pixon at each pixel
  ;COMMON BLOCKS:
  ;SIDE EFFECTS:
  ;RESTRICTIONS:
  ;PROCEDURE:
  ;MODIFICATION HISTORY:
  ;     T. Metcalf 1995-08-14
  ;                1995-08-22 Modified calculation of change in DOF.
  ;                           Added calculation of npixons.
  ;                1995-09-01 Added calculation of pfraction, the fraction
  ;                           of a pixon at each pixel.
  ;                1996-03-06 Allow the pixon map to be reset every time
  ;                           this routine is called (not currently used).
  ;                           Also allow pixon size to drop by more than 1 step.
  ;                           Add confidence level to chi^2 test.
  ;                1996-06-17 Added "fill" to deal with the edges when the
  ;                           guard width size is less than the largest pixon.
  ;     T. Metcalf 1996-06-21 Keep track of pixon changes as the pixon map is
  ;                           calculated (dr2s stuff).
  ;     T. Metcalf 1996-07-05 Added median filter at the end.  Also changed to
  ;                           recompute non-background pixons from scratch
  ;                           at every step.
  ;     T. Metcalf 1996-07-08 Took out median filter.  It wasn't necessary with
  ;                           the recompute at every step.
  ;     T. Metcalf 1996-07-26 -sigma set to sqrt(2).
  ;                           -Order of x, y and i loops reversed.
  ;                           -Pixon resolution is frozen if gof goes below nd.
  ;                           -edgefill set to 1.0 since /fft is in calls to
  ;                            hsi_pixon_local_smooth.
  ;     T. Metcalf 1996-08-02 -Added the sensitivity keyword.
  ;     T. Metcalf 1996-10-01 -Added call to hxt_error.
  ;     T. Metcalf 1997-01-22 -Changed default sensitivity to 0.0
  ;
  ;     T. Metcalf 1999-Dec-13 - Adapted hxt_pixon_map for HESSI use.
  ;     T. Metcalf 2000-Feb-23 - Added fast keyword with much faster calculation
  ;                              of the pixon map.
  ;     T. Metcalf 2002-Nov-12 - Fixed bug in calculation of pfraction.
  ;     R  Schwartz 20-jan-2014 - pass in fixed object values for speed improvement
  ;       map_ptr = map_ptr, cbe_ptr = cbe_ptr, time_unit = time_unit, time_bin = time_bin, $
  ;        pixel_size = pixel_size, det_eff = det_eff
  ;         removed old commented blocks trial code


  tlocal = local
  nx1 = nx-1
  ny1 = ny-1

  xyindex = lindgen(nx, ny)

  if n_elements(sensitivity) NE 1 then sensitivity = 0.  ;1./sqrt(2.) ;;1.4142135
  sensitivity = float(sensitivity[0])

  if uselogarithm then $
    eimage = exp((reform(uselogarithm*image, nx, ny)<Imax)>(-20)) $
  else $
    eimage = reform(image>(max(image)/1.e32), nx, ny)<Imax

  top = max(pixon_sizes)
  revpixon_sizes = pixon_sizes[rotate(sort(pixon_sizes), 2)]
  npixon_sizes = n_elements(pixon_sizes)
  if npixon_sizes GT 1 then nextop = revpixon_sizes[1] else nextop = top

  ; Get ipixon, the index of resolution in pixon_sizes
  junk = min(abs(pixon_sizes - resolution), ipixon)

  ; Get list of pixon sizes to consider (all larger than (or =) resolution).
  ; These sizes will be tested for each pixel.

  ; -----------------------------------------------------
  ; checks only the smallest and preserves all larger scales.
  itestsizes = where(pixon_sizes EQ pixon_sizes[ipixon], ntestsizes)

  ; -----------------------------------------------------

  ; Apply the pixon map (local correlation scales) to image to obtain
  ; the smoothed image, pimage.  Also sets psf etc.

  pimage = hsi_pixon_local_smooth(eimage, local, pixon_sizes, psf, sdels, xcen, ycen, fft=1)

  npsf = n_elements(psf[0, 0, *])

  ; Lookup table: which pixon shape function corresponds to each resolution

  slookup = lonarr(npixon_sizes)
  for i=0L, npixon_sizes-1L do begin
    junk = min(abs(sdels*2-pixon_sizes[i]), isdels)
    slookup[i] = isdels
  endfor

  ipsfnew = slookup[ipixon]

  ; Change in DOF lookup table.  Change in DOF when changing from any valid
  ; old resolution sized pixons to the size we are checking

  ; The number of DOFs is 1/total(psf) at each resolution.  Hence the
  ; change in DOFs is 1/total(psf1) - 1/total(psf2).  PSF's set to max
  ; value one here.

  totpsf = dblarr(npsf)         ; Get integral of re-normalized PSF's
  for i=0L, npsf-1L do begin
    totpsf[i]=total(double(psf[*, *, i])/max(psf[*, *, i]))
  endfor
  dDOFlookup = dblarr(npixon_sizes, ntestsizes)   ; must be initialized to zero
  for i=0L, ntestsizes-1L do begin
    good = where(pixon_sizes NE pixon_sizes[itestsizes[i]], ngood)
    if ngood GT 0 then $
      dDOFlookup[good, i] = $
      (1.d0/totpsf[slookup[itestsizes[i]]] - 1.d0/totpsf[slookup[good]])
  endfor

  ; Loop through every pixel.  Check if a change in resolution
  ; is supported by the data.  If it is, the GOF will decrease more
  ; than the increase in DOF.

  npixons = 0.0d0
  dr2s = dblarr(nd)

  pfraction = double(local)  ; Just to make it the right size.
  pfraction[*] = 1.d0/max(totpsf)  ; For the guard ring.
  for i=0L, npixon_sizes-1 do begin ; set pfraction for the current local
    good = where(abs(local-pixon_sizes[i]) LT 0.1, ngood)
    if ngood GT 0 then pfraction[good] = 1.d0/totpsf[slookup[i]]
  endfor

  ; The pixon sizes must be reverse sorted so that we always
  ; end up with the largest acceptable pixon.
  sortsizes = rotate(sort(pixon_sizes[itestsizes]), 2)  ; reverse sort
  ; Old_test_size is the pixon size below which we no longer check background
  ; pixels.  I.e. if a pixel is a background pixel (largest pixon size) then
  ; it will not be tested further once the resolution is below old_test_size.
  ; if set to revpixon_sizes(2) then back ground pixels will be checked for
  ; the largest, 2nd largest, and 3rd largest, etc.
  ;old_test_size = revpixon_sizes(1<(npixon_sizes-1)) ; worth a try
  old_test_size = revpixon_sizes[2<(npixon_sizes-1)] ; HXT standard

  ;new_test_size = revpixon_sizes(4<(npixon_sizes-1)) ; 5 is very conservative
  new_test_size = revpixon_sizes[2<(npixon_sizes-1)] ; 5 is very conservative
  ;new_test_size = revpixon_sizes(npixon_sizes-1)  ; turn this feature off


  lastpercentdone = -1L

  for i=0L, ntestsizes-1L do begin

    if not_quiet then begin
      print, i, pixon_sizes[itestsizes[sortsizes[i]]], format='(i4, f9.3, " ", $)'
      endif
    ;if keyword_set(fast) then print

    pixel_size = min(iobj->Get(/pixel_size))  ; in arcseconds
    resarcsec = pixon_sizes[itestsizes[sortsizes[i]]]*pixel_size  ; pixon size in arcsec
    ; This is potentially slower than it should be because it is does not
    ; account for the different resolution of the harmonics
    if keyword_set(fast) then begin
      ; The fast algorithm is fast so don't need the speed increase.
      ; Just use all the det's
      ssdet = lindgen(n_elements(det))
    endif else begin
      ssdet = where(det_resolution[det, 0] GE resarcsec/3., nssdet)
    endelse
    det_reduced = det[ssdet]
    ;harmonics_reduced = harmonics[ssdet]
    data_reduced = hsi_pixon_reduced_data(dobj, $
      det_reduced) ;, $
      ;harmonics_reduced)
    snr_sigma2_reduced = hsi_pixon_reduced_data(sobj, $
      det_reduced);, $
      ;harmonics_reduced)
    ;bflat_reduced = hsi_pixon_reduced_data(bobj, $
    ;                                      det_reduced, $
    ;                                      harmonics_reduced)

    ; Apply pixon map

    pimage = hsi_pixon_local_smooth(eimage, tlocal, pixon_sizes, fft=1)
    residuals = hsi_pixon_residuals(reform(pimage, nxy), dobj, iobj, corrfac,  det_reduced, harmonics_reduced, modprofile=recon_data, nounits=0, $
      background=bobj, $
      cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
    gof = hsi_pixon_gof_calc(data_reduced, recon_data, residuals, snr_sigma2_reduced, r2s, $
      poisson=usepoisson)

    ;if gof GE n_elements(residuals) then begin
    if 1 then begin

      newres = pixon_sizes[itestsizes[sortsizes[i]]]

      if keyword_set(fast) then begin
        ; If tlocal is everywhere equal to newres, then skip the
        ; calculation.
        if (min(tlocal) NE newres OR max(tlocal) NE newres) then begin
          ; watch out that the diffnewres index has the same meaning as
          ; the indices into pixon_sizes in hsi_pixon_local_smooth.pro
          junk = min(abs(pixon_sizes[sort(pixon_sizes)]-newres), diffnewres)
          ;message, /info, 'Running GC before pixon map'
          ;heap_gc, /verbose
          bpdiff = hsi_pixon_bproj(r2s, dobj, iobj,  det_reduced, harmonics_reduced, $
            /vanilla, /useunits, $
            /smoothpatts, diffres=diffnewres, $
            pixon_sizes=pixon_sizes[sort(pixon_sizes)], $
            pixonmap=tlocal, /nonorm, /nocheck, $
            dummydir=dummydir, not_quiet=not_quiet, /no_spatial_freq, $
            cbe_ptr = cbe_ptr, map_ptr = map_ptr, $
            time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
          ;message, /info, 'Running GC after pixon map'
          ;heap_gc, /verbose
          dGOF = eimage*bpdiff   ; Unit problem here??
          dDOF = fltarr(nx, ny)
          for ioldpixon = 0L, n_elements(pixon_sizes)-1L do begin
            oldres = pixon_sizes[ioldpixon]
            ssold = where(tlocal EQ oldres, nss)
            if nss GT 0 then begin
              ipsfold = slookup[ioldpixon]
              dDOF[ssold] = dDOFlookup[ioldpixon, sortsizes[i]]
              if sensitivity GE 0.0 then begin
                signDOF = 2*(dDOF[ssold] GE 0.)-1
                sigmaDOF = 1.+signDOF*sensitivity/sqrt(2.0d0/totpsf[ipsfold])
                dDOF[ssold] = dDOF[ssold]*sigmaDOF
              endif  ;sensitivity
            endif ;nss
          endfor
          ; Test all scales and allow pixon size to go up or down
          if sensitivity LT 0.0 then $
            good = where(dGOF GT dDOF/(1.-sensitivity) AND $
            tlocal NE newres, ngood) $
          else $
            good = where(dGOF GT dDOF AND tlocal NE newres, ngood)
          ; Pixon size can only get smaller
          ;good = where(dGOF GT dDOF AND tlocal GT newres, ngood)
          if ngood GT 0 then begin
            junk = min(abs(pixon_sizes - newres), inewpixon)
            ipsfnew = slookup[inewpixon]
            tlocal[good]=newres
            pfraction[good] = 1.0/totpsf[ipsfnew]
          endif ;ngood
        endif    ;tlocal
      endif else begin ;end FAST

        for y=0L, ny-1L do begin
          for x=0L, nx-1L do begin

            if not_quiet then begin
              ; Print some diagnostics to let the user know how things are
              ; progressing.
              percentdone = long(100.*float(y*nx+x+1)/float(nxy))
              if percentdone MOD 5L EQ 0L then begin
                if percentdone NE lastpercentdone then begin
                  lastpercentdone = percentdone
                  if percentdone MOD 10L EQ 0L then begin
                    if percentdone LT 100 then $
                      print, percentdone, format='(i2, $)' $
                    else $
                      print, percentdone, format='(i3, $)'
                    endif else $
                    print, format='(".", $)'
                  endif
              endif
            endif

            oldres = tlocal[x, y]
            junk = min(abs(pixon_sizes - oldres), ioldpixon)
            ipsfold = slookup[ioldpixon]
            pfraction[x, y] = 1.0d0/totpsf[ipsfold]
            dr2s[*] = 0.0

            ;newres = pixon_sizes(itestsizes(sortsizes(i)))
            toldres = tlocal[x, y]
            tfraction = pfraction[x, y]

            ; is this an isolated background pixon?  If a bg pixon is not
            ; isolated, it is checked to see if it should be changed,
            ; otherwise it is not checked at all once old_test_size is
            ; reached.  If newres is background, and we're lower than
            ; new_test_size, the pixels are normally not checked (for
            ; speed).  However, if adjacent to a background pixel it
            ; will be checked.  Note: after a few tests this doesn't seem
            ; to do anything.  It is just the same as completely ignoring
            ; background pixels once we reach old_test_size and
            ; new_test_size.

            isolatedbg = 1 ; Made no difference so turn off test (it's slow)
            adjacentbg = 0


            if (newres NE oldres) AND $
              ((newres NE top) OR adjacentbg OR (pixon_sizes[ipixon] GE new_test_size)) AND $
              ((oldres NE top) OR (NOT isolatedbg) OR (pixon_sizes[ipixon] GE old_test_size)) then begin
              tlocal[x, y] = newres   ; Try new resoltuion
              junk = min(abs(pixon_sizes - newres), inewpixon)
              ipsfnew = slookup[inewpixon]
              maxres = max([oldres, newres])
              xyr = long([-long(maxres/2L)-1, +long(maxres/2L)+1])
              xr = (xyr > (-x)) < (nx1-x)
              yr = (xyr > (-y)) < (ny1-y)
              psfx = xcen+xr
              psfy = ycen+yr
              pattx = x+xr
              patty = y+yr
              ;nxs = xr(1)-xr(0)+1
              ;nys = yr(1)-yr(0)+1
              ; edgefill should not be used if /FFT is set in hsi_pixon_local_smooth
              edgefill = 1.0   ; for /fft set edgefill to 1.0
              ;;edgefill = (nxs*nys)/(2.*maxres+1.)^2  ; approximate pixon footpoint by a square for speed (edgefill=1 except at the edges)

              ; ds has the "wrong" sign so that dGOF will be
              ; positive.  This means
              ; that dRESID also has the "wrong" sign and
              ; should be subtracted from r2s below.

              ds = (psf[psfx[0]:psfx[1], psfy[0]:psfy[1], ipsfold] - $
                psf[psfx[0]:psfx[1], psfy[0]:psfy[1], ipsfnew])

              ; With the /noresidual keyword set, hsi_pixon_resdiuals
              ; returns the mod profile.

              dRESID = eimage[x, y] * $
                hsi_pixon_residuals(reform(ds, n_elements(ds)), $
                dobj, iobj, corrfac,  $
                det_reduced, harmonics_reduced, $
                /noresidual, $
                sspatt=xyindex[pattx[0]:pattx[1], $
                patty[0]:patty[1]], nounits=0, background=bobj, $
                cbe_ptr = cbe_ptr, map_ptr = map_ptr, $
                time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)

              dGOF = total(r2s*dRESID)
              dDOF = dDOFlookup[ioldpixon, sortsizes[i]]*edgefill


              signDOF = 2*(dDOF GE 0.)-1

              sigmaDOF = 1.+signDOF*sensitivity/sqrt(2.0d0/totpsf[ipsfold])

              dDOF = dDOF*sigmaDOF

              if (dGOF GE dDOF) then begin
                pfraction[x, y] = double(edgefill)/totpsf[ipsfnew]  ; keep new resolution

                ; It is not necessary to keep track of changes
                ; to r2s.  This just forces the image to be
                ; updated as the pixon map is calculated and
                ; really the image should be held fixed.


                ;    if (n_elements(r2s) NE n_elements(dRESID)) then $
                ;       message, 'ERROR: dRESID and r2s do not have the same number of elements'
                if keyword_set(usepoisson) then begin
                  dr2s = 2.0*dRESID*(1.0-0.5*r2s)^2/data_reduced
                endif else begin
                  dr2s = dRESID*2.0*snr_sigma2_reduced  ; save to update r2s
                endelse




                r2s = r2s - dr2s  ; update r2s
              endif else begin
                tlocal[x, y] = toldres                              ; Restore old resolution
                pfraction[x, y] = tfraction
              endelse

            endif
          endfor
          ;;r2s = r2s - dr2s  ; update r2s
          ;;npixons = npixons + pfraction(x, y)
        endfor
      endelse   ; End of SLOW version
    endif

    ; Reset r2s periodically.  This shouldn't be necessary since I
    ; keep track of changes to r2s with dr2s below.  However, dr2s may
    ; build up error so reseting every now and again seems wise.
    ; If usepoisson is set, recon_data should be recomputed every time
    ; the pixon map changes, but that is too slow.

    pimage = hsi_pixon_local_smooth(eimage, tlocal, pixon_sizes, fft=1)
    residuals = hsi_pixon_residuals(reform(pimage, nxy), dobj, iobj, corrfac,  det_reduced, harmonics_reduced, modprofile=recon_data, $
      nounits=0, background=bobj, $
      cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
    gof = hsi_pixon_gof_calc(data_reduced, recon_data, residuals, snr_sigma2_reduced, r2s, $
      poisson=usepoisson, iobj=iobj, dobj=dobj, $
      det=det, harmonics=harmonics)
    if not_quiet then $
      print, gof/n_elements(residuals), format='(" ", f12.3)'
    if tty then $
      tv, rebin(bytscl(tlocal, min=newres, $
      max=max(pixon_sizes), $
      top=!d.table_size-2), $
      nx*expansion, ny*expansion, /sample), $
      nx*expansion+20, ny*expansion
  endfor
  if not_quiet AND NOT keyword_set(nolastprint) then $
    print
  npixons = total(pfraction)


  return, tlocal

end

;-----------------------------------------------

function hsi_pixon_image, self, sigmai, residual, rgof, error,  $
  vis_obj = vis_obj, $ ;added 27-oct-2017, RAS, to support vis cbe method
  iterate = iteration,  $
  quiet = quiet,  $
  
  NORM_NOZERO = NORM_NOZERO, $ ;if set,  normalize stat by number non-zero exp values
  resolution = minresin, snr = dosnr, guess = guessin,  $
  pixon_sizes = inpixon_sizes, notty = notty,  $
  outresolution = outresolution, poisson = poisson, chisqr = chisqr,  $
  sensitivity = sensitivity, pixonmap = pixonmap, btot_in = btot_in,  $
  trueimage = trueimage, dummydir = indummydir,  $
  progress_bar = progress_bar,  $	;kim
  xycoordinate = xycoordinate,  $
  full_pm_calc = full_pm_calc,  $
  background_model = background_model, $
  variable_metric = variable_metric, $
  windows = windows, $
  _extra = _extra ;added keyword inheritance, 27-oct-2017, RAS


  common hsi_pixon_dfp_private, dfpscale
  common hsi_pixon_gof_private, nx, ny, nxy, $
    expansion, tty, not_quiet, $
    usepoisson, uselogarithm, usemem, $
    old_lcs, old_det, $
    snr_sigma2, $
    local, pimage, pfraction, imax, $
    iobj, dobj, corrfac,  datac, det, harmonics, $
    tot_smoothed_patts, sfilename, firstgradcall, $
    pixon_sizes, dummydir, bobj, bflat, cbe_ptr, scelist, $
    map_ptr, time_unit, time_bin, det_eff

  message, /info, 'Last modification 2005-Mar-15.'

  iobj = self->Get( /obj, class='hsi_modpat_products')
  ;060815
  is_annsec = stregex( /boo, /fold, hsi_get_modpat_strategy( iobj ), 'annsec')

  ; Set up
  default, NORM_NOZERO, 1 ;ras, default is to count only the used data, zero predicted counts are not used
  ;previously, every data bin, even with zero pred counts (zero livetime) were counted
  ;number of valid bins is used to normalize/reduce the Cstat, 7-may-2010, ras
  ;does not affect the images! so we don't need to have a user path (control parameter) to go
  ;back to the old way
  

  uselogarithm = 0   ; -1, 0, +1 ONLY.  -1 = ln(1/I); 0 = linear; +1 = +ln(I)
  usemem = 1

  ;iobj = self  ; copies pointer only - iobj pulled above

  if keyword_set(indummydir) then dummydir = indummydir

  ; For some reason if the pixon code is called directly after the
  ; forward fitting code, things are messed up.  Hence this call.
  ; This also forces the calculation of mod patts etc. if they are
  ; not already computed.
  junk = iobj->getdata(image_algorithm='back projection')  ; do a back projection to make sure all is set up
  ocbe = iobj->Get(/obj, class_name='hsi_calib_eventlist')  ;ras, 28-mar-2017

  cbe_ptr = ocbe->GetData()  ;ras, 28-mar-2017
  scelist  = size(cbe_ptr)
  corrfac = hsi_get_corrfactors( self, _extra = _extra, /noset )
  help, corrfac
  print, corrfac
  map_ptr = iobj->getdata( class_name ='hsi_modul_pattern')
  time_unit = iobj->get(/time_unit)
  time_bin = hsi_get_time_bins( iobj )

  det_eff = ocbe->Get_det_eff()  ;ras, 28-mar-2017


  ; Choose one of these statements
  ;if (keyword_set(poisson) AND NOT keyword_set(chisqr)) OR n_elements(sigmai) NE n_elements(data) then $
  ;   usepoisson = 1 else usepoisson=0  ; chisqr default
  default, poisson, 0
  default, chisqr, 0
  case 1 of
    poisson: usepoisson = 1
    chisqr: usepoisson = 0
    else: usepoisson = 1
  endcase

  if NOT keyword_set(quiet) then begin
    if keyword_set(usepoisson) then message, /info, 'Poisson statistics' $
    else message, /info, 'chisqr statistics'
  endif

  usederivative = 1
  usedouble = 0
  not_quiet = 1-keyword_set(quiet)
  if keyword_set(notty) then tty = 0 else tty = 1

  if tty then begin
    ; save the color table and set it to something else
    ; This prevents an interaction with the HESSI GUI
    tvlct, /get, rsave, gsave, bsave
    loadct, 5
  endif

  old_lcsgoffunc = 0  ; reset
  ;all of these options for dosnr managed in the following call
  hsi_pixon_dosnr, dosnr, variable_metric, use_dfpmin, full_pm_calc, background_model, use_snr

  dobj = hsi_pixon_get_data(iobj, $
    detindex=det, $
    nodropout = nresid, $;this is the total number of usable data bins
    harmonics=harmonics, $
    dflat=data, sigma=sigma, bobj=bobj, bflat=bflat, $
    /nounits, snr=dosnr)

  szdobj = size(dobj)
  nd = n_elements(data)

  map_dim = long( is_annsec ? iobj->get(/rmap_dim)  : iobj->get(/image_dim ))
  nx = map_dim[0]
  ny = map_dim[1]
  nxy = nx*ny

  expansion = long(256./nx + 0.5)>1L ;4
  time_string = format_intervals( ( self -> get(/absolute_time_range)), /trunc, /ut)
  en_string = format_intervals(self -> get(/im_eband_used), format='(i)') + ' keV'
  time_en_title = ' '+time_string+' '+en_string


  if tty  then begin
    device, get_screen_size=scrsz
    hsi_pixon_windows_init, Self, xsize = nx*expansion*2+40, ysize = ny*expansion*2+20, $
      title = 'Pixon Image ', /map, windows = windows
    hsi_pixon_windows_init, Self, xsize = fix(640*(scrsz[1]/1200.)), ysize = fix(960*(scrsz[1]/1200.)), $
      title = 'Pixon Profiles ' , /rate,  windows = windows
    if background_model then $
      hsi_pixon_windows_init, Self, xsize = fix(640*(scrsz[1]/1200.)), ysize = fix(960*(scrsz[1]/1200.)), $
      title = 'Pixon Background Fit ',  /background, windows = windows


    wset, windows.map
    erase
    xyouts, 0, ny*expansion * 2.0 + 3, /device, align = 0.0, charsiz = 1.5, time_en_title
    xyouts, nx*expansion*0L+10, ny*expansion*1.5-5L, /device, align=0.5, orientation=90, $
      'Pseudo Image', chars=1.5
    xyouts, nx*expansion*2L+30, ny*expansion*1.5-5L, /device, align=0.5, orientation=90, $
      'Pixon Map', chars=1.5
    xyouts, nx*expansion*0L+10, ny*expansion*0.5-5L, /device, align=0.5, orientation=90, $
      'Image', chars=1.5
    xyouts, nx*expansion*2L+30, ny*expansion*0.5-5L, /device, align=0.5, orientation=90, $
      'Gradient', chars=1.5
  endif
  time_en_title = ' ( ' + time_en_title + ' )'

  if min(data) LT 0 then $
    message, /info, 'Negative data ... bad background subtraction?'

  ;if n_elements(minresin) ne 1 then minres = 0L $
  ;else minres = (long(minresin)>1L)<8L              ; Minimum local smoothing
  if n_elements(minresin) ne 1 then minres = 1.0 $
  else minres = (float(minresin)>1.)<8.              ; Minimum local smoothing
  top = (sqrt(nxy)/4)>minres                        ; Maximum local smoothing
  bgpixon_size = (sqrt(nxy)/2)>minres        ; Size of a background pixon

  if keyword_set(inpixon_sizes) then begin

    if size(inpixon_sizes, /tname) EQ 'POINTER' then begin
      if ptr_valid(inpixon_sizes) then $
        pixon_sizes= *(inpixon_sizes[0])
    endif else begin
      pixon_sizes=inpixon_sizes
    endelse
    top = max(pixon_sizes) <top
  endif

  if n_elements(pixon_sizes) LE 0 then begin
    ;;pixon_sizes = findgen(128)           ; Linear spaced pixons
    pixon_sizes = [sqrt(2.0)^findgen(25)]  ; logarithmic spaced pixons
    ;pixon_sizes = 2.0 ^(findgen(25)/2.)    ;keeps the even index values true integers
  endif

  pixon_sizes = pixon_sizes[reverse( sort(pixon_sizes) ) ]
  good = where(ceil(pixon_sizes) GE minres and pixon_sizes LE top)
  if good[0] LT 0 then message, 'No valid pixon sizes.'
  pixon_sizes = pixon_sizes[good]
  if n_elements(inpixon_sizes) LE 0 then pixon_sizes = [bgpixon_size, pixon_sizes]
  default, PRE_2015_MAX_PIXON, 0 ;Set this to 1 and recompile if you want the previous, inconsistent, setting
  ;of the MAX_PIXON value
  if PRE_2015_MAX_PIXON then max_pixon = max(pixon_sizes) ; The largest size in the iteration.
  ; A large background pixon should always be available, even if the user
  ; does not want to use it in the iteration
  if pixon_sizes[0] LT bgpixon_size then pixon_sizes = [bgpixon_size, pixon_sizes]
  pixon_sizes = squeeze(pixon_sizes)
  if n_elements(inpixon_sizes) LE 0 then inpixon_sizes = pixon_sizes ; output
  if ~PRE_2015_MAX_PIXON then max_pixon = max(pixon_sizes) ; The largest size in the iteration.;;;;;;;;;;moved for consistency

  pixstring = ""
  for i=0, n_elements(pixon_sizes)-1 do pixstring = pixstring+" "+string(pixon_sizes[i], format="(f20.3)")
  message, /info, strcompress('Pixon Sizes: '+pixstring)

  good = where(data GT 0., ngood)
  negative_data = where(data LT 0., nnegdata)
  if nnegdata GT 0 then $
    message, /info, strcompress('Correcting '+string(nnegdata)+' negative data points')
  if ngood GT 0 then mindata = (min(data[good])/1000.)<(0.001) else begin
    message, 'ERROR: all data is zero'
  endelse
  if (usedouble) then datac=double(data>mindata) else datac=data>mindata  ; protect the input data from change
  if n_elements(continued) ne 1 then continued = 0
  ;local = replicate(max_pixon, nx, ny)
  local = replicate(bgpixon_size, nx, ny)
  npixons = 2.72*float(nxy)/float(bgpixon_size^2)

  if n_elements(sigmai) EQ nd then sigma=sigmai
  snr = replicate(1., nd)   ; No SNR weighting
  if use_snr then snr = sqrt(data)
  ;4-may-2010 ras, mod to support NORM_NOZERO
  nresid = float( keyword_set(NORM_NOZERO) ? nresid : nd)
  tsnr = total(snr) / nresid

  snr_sigma2 = snr/(sigma^2)
  if (usedouble) then pimage = dblarr(nx, ny) else pimage = fltarr(nx, ny)
  if (usedouble) then image  = dblarr(nx, ny) else image  = fltarr(nx, ny)

  ; Convert datac to data structure

  i1 = 0L
  ;sobj = ptrarr(27, 5)
  sobj = ptrarr(szdobj[1], szdobj[2])
  for i=0, n_elements(det)-1 do begin
    ;nharmonics = n_elements(*harmonics[i])
    ;for j=0, nharmonics-1 do begin
      ndah = n_elements(*dobj[det[i]])
      i2 = i1 + ndah - 1L
      *dobj[det[i]] = datac[i1:i2]
      sobj[det[i]] = ptr_new(snr_sigma2[i1:i2])
      i1 = i1 + ndah
    ;endfor
  endfor

  ; Get a rough solution.

  if size(guessin, /tname) EQ 'POINTER' then begin
    if ptr_valid(guessin) then begin
      if max(*(guessin[0])) GT 0 then guess = *(guessin[0])
    endif
  endif else begin
    if n_elements(guessin) EQ nx*ny then begin
      if max(guessin) GT 0 then guess = guessin
    endif else if size(guessin, /n_dimensions) EQ 2 then $
      if n_elements(hsi_xy2annsec(guessin, iobj)) EQ nx*ny then begin
      if max(guessin) GT 0 then guess = hsi_xy2annsec(guessin, iobj)
    endif
  endelse

  if n_elements(trueimage) EQ nx*ny then begin
    if max(trueimage) GT 0 then guess = trueimage
  endif
  if n_elements(guess) NE nx*ny then begin
    if NOT keyword_set(quiet) then $
      message, /info, 'Computing initial guess ...'
    guess = iobj->GetData( class_name= 'hsi_bproj' )
  endif

  ; If the total flux is not passed in, try to calculate it.
  if (keyword_set(btot_in)) then begin
    btot = btot_in
  ENDIF ELSE begin
    if NOT keyword_set(quiet) then $
      message, /info, 'Estimating total flux '+!stime
    ;btot = hsi_btot(iobj, /fast, quiet=quiet)
    det_index_mask=iobj->get(/det_index_mask)
    junk = where( det_index_mask, ndet)
    btot = ndet*ndet*total(iobj->GetData( class_name= 'hsi_bproj' )) / $
      hsi_dirty_norm(iobj, polar = is_annsec)
    if NOT finite(btot) then btot = 0.0
    if btot LE 0.0 then delvarx, btot
  endelse

  image = guess
  image = image > (max(image)/10000.)

  if n_elements(btot) GT 0 then image = image*btot[0]/total(image)

  Imax = max(image)*1.e9  ; Limit to image brightness

  ; Use log(image) so that even when the "image" is negative the
  ; actual image is always positive.

  if uselogarithm then begin
    image = uselogarithm*alog(double(image)>(max(image)/10000.))
    Imax = alog(Imax)
  endif


  ; Initialize the iteration.

  if keyword_set(trueimage) then begin
    ; This is only used for testing
    truetemp = fltarr(nx, ny)
    truetemp[0, 0] = trueimage
    trueimage = temporary(truetemp)
    tresiduals = hsi_pixon_residuals(reform(trueimage, nxy), dobj, iobj, corrfac,  $
      det, harmonics, modprofile=trecon_data, $
      /setunits, nounits=0, background=bobj, $
      cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
    tgof = hsi_pixon_gof_calc(datac, trecon_data, tresiduals, snr_sigma2, $
      poisson=usepoisson, nresid=nresid, norm_nozero=norm_nozero)

    ; If the next lines are used, then trueimage is used as the simulation
    ; ----------
    datac = randomp(seed, trecon_data)>.1

    dobj = hsi_pixon_fill_dobj[datac, iobj, det, harmonics]
    ;image = trueimage  ; make guess "perfect"
    image = fltarr(nx, ny)+total(image)/nx/ny  ; Make crappy guess
    btot = total(image)
    tresiduals = hsi_pixon_residuals(reform(trueimage, nxy), dobj, iobj, corrfac,  $
      det, harmonics, modprofile=trecon_data, nounits=0, background=bobj, $
      cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
    tgof = hsi_pixon_gof_calc(datac, trecon_data, tresiduals, snr_sigma2, $
      poisson=usepoisson, nresid=nresid, norm_nozero=norm_nozero)
    if uselogarithm then begin
      image = uselogarithm*alog(double(image)>(max(image)/10000.))
      Imax = alog(Imax)
    endif
    ; ----------

    if tty then begin

      wset, windows.rate
      hsi_pixon_plot_residuals, trecon_data, dobj, tresiduals, $
        snr_sigma2, det, harmonics, $
        poisson=usepoisson, bobj=bobj, $
        NORM_NOZERO=NORM_NOZERO, $
        time_en_title = time_en_title

    endif
  endif

  junk = hsi_pixon_local_smooth(image, local, pixon_sizes, /recompute, fft=1) & junk = 0
  ;npixons = nd
  pfraction = double(local)   ; fraction of a pixon at each pixel
  pfraction[*] = double(npixons)/n_elements(local)

  smpattstart = systime(1)
  if tty then wset, windows.map
  igof = hsi_pixon_gof_func(image, gradient, /reset)  ; sets initial pimage, etc.
  smpattstop = systime(1)
  smpatttime = (smpattstop-smpattstart)
  converge = 0.

  residuals = hsi_pixon_residuals(reform(pimage, nxy), dobj, iobj, corrfac,  $
    det, harmonics, modprofile=recon_data, $
    /setunits, nounits=0, background=bobj, $
    cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
  gof = hsi_pixon_gof_calc(datac, recon_data, residuals, snr_sigma2, r2s, $
    poisson=usepoisson, /setunits, iobj=iobj, dobj=dobj, $
    det=det, harmonics=harmonics, nresid=nresid, norm_nozero=norm_nozero)  ; also sets units
  rgof = gof/tsnr
  best = {image:pimage, gof:rgof, resolution:max_pixon, pixonmap:local}

  ; Set the DC component, if it is not already set, by unbiasing the
  ; residuals (approximate)

  if n_elements(btot) LE 0 then begin
    ; If we don't have a good DC value, try to adjust the intensites to
    ; fix this
    if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
    else eimage = (image>(max(image)/1.e32))<Imax
    eimage = eimage * total(datac) / total(recon_data)
    pimage = hsi_pixon_local_smooth(eimage, local, pixon_sizes, fft=1)
    btot = total(eimage)
    if uselogarithm then image = uselogarithm*alog(eimage) $
    else image = eimage
  endif

  if tty then begin
    wset, windows.map
    if uselogarithm then $
      tvscl, rebin(exp(((uselogarithm*image)<Imax)>(-20.)), nx*expansion, ny*expansion, /sample), nx*0+20, ny*expansion $
    else $
      tvscl, rebin(image, nx*expansion, ny*expansion, /sample), nx*0+20, ny*expansion
  endif
  if not_quiet then begin
    print
    print, '   GOF:'+strcompress(' '+string(rgof))+'  Unmodulated Brightness: '+strcompress(string(total(pimage)))
  endif

  iterate_pixon_map = 0L   ; Iteration control parameters

  tolerance = 1.e-4 ;0.01

  ; Store master copies since only a subset will be used for each resolution
  det_master = det
  ;harmonics_master = harmonics
  delvarx, det,  /old  ; Remove det and harmonics, but do *not* free the pointer.  Hence /old.
  datac_master = datac
  snr_sigma2_master = snr_sigma2
  pixel_size = min(iobj->Get(/pixel_size))  ; in arcseconds

  progbaropen = 0
  progstart = systime(1)


  RESTART:

  pixon_map_no_change = 0L

  ; det resolution in arcseconds
  ;;separation = 1600.0 ; in mm
  ;;pitch = separation*(hsi_grid_parameters()).pitch/3600./!radeg
  ;;detr = 0.5*atan(pitch/separation)*!radeg*3600.  ; in arcsec
  ;detr = 0.5*((hsi_grid_parameters()).pitch)
  det_resolution = 0.5*((hsi_grid_parameters()).pitch)
;  for i=0, szdobj[1]-1 do begin  ; Account for higher harmonics too
;    ;for j=0, szdobj[2]-1 do begin
;      det_resolution[i] = detr[i MOD 9]
;    ;endfor
;  endfor

  for iresolution=0L, n_elements(pixon_sizes)-1L do begin

    if pixon_sizes[iresolution] GT max_pixon+0.1 then continue

    ; Select the det values for this resolution


    resarcsec = pixon_sizes[iresolution]*pixel_size  ; pixon size in arcsec
    ; The det's are not used until their resolution will contribute.
    ; res divisor adjusts resarcsec to give the resolution acheivable.
    ; For HXT resdivisor is about 3.  I.e. the 15" grids can reveal 5"
    ; structure.  I don't know about HESSI.  Here the divisor goes from
    ; 1 to 3 as the iteration progresses for increased speed.

    ;resdivisor = float(fix(3.*sqrt(float(iresolution)/float(n_elements(pixon_sizes)-1L))+0.5)>1.)
    resdivisor = 3.

    ; Get meaningful resolutions to speed up the reconstruction
    ; No point in wasting time with grids that have no information for
    ; the current resolution.

    if NOT keyword_set(quiet) then print
    ;if n_elements(harmonics) GT 0 then ptr_free, harmonics
    delvarx, det, /old
    for i=0, n_elements(det_master)-1 do begin
      ;nharmonics = n_elements(*harmonics_master[i])
      ;delvarx, hindex
      ;for j=0, nharmonics-1 do begin
        ; sigmod is true if there is significant modulation
        ;smfact = 1.02
        smfact = 1.03  ; TRM 2002-Jan-11
        sigmod = stddev(*dobj[det_master[i]]) GE $
          smfact*sqrt(mean((*dobj[det_master[i]])))
        if (det_resolution[det_master[i]] GE resarcsec/resdivisor OR iresolution GE n_elements(pixon_sizes)-2L) then begin
;          if n_elements(hindex) LE 0 then hindex=(*harmonics_master[i])[j] $
;          else hindex = [hindex, (*harmonics_master[i])[j]]
        endif
      ;endfor
;      if n_elements(hindex) GT 0 then begin
        if n_elements(det) LE 0 then det=det_master[i] $
        else det = [det, det_master[i]]
;        if n_elements(harmonics) LE 0 then harmonics = ptr_new(hindex) $
;        else harmonics = [harmonics, ptr_new(hindex)]
      ;endif
    endfor

    ; If none, at least use the largest resolution.
    ; This is just slow, not wrong.
    if n_elements(det) LE 0 then begin
      ;if ngood LE 0 then junk = max(det_resolution[det_master, 0], good)
      junk = max(det_resolution[det_master, 0], good)
      det = det_master[good]
      ;harmonics = [ptr_new((*harmonics_master[good])[0])]
      message, /info, 'All available resolutions are too small.  Using largest.  Reconstruction may take longer than expected.'
    endif

    datac = hsi_pixon_reduced_data(dobj, det);, harmonics)
    snr_sigma2 = hsi_pixon_reduced_data(sobj, det);, harmonics)
    if NOT keyword_set(quiet) then $
      message, /info, "Currently using detectors's: " + $
      strjoin(strcompress(string(det+1), /remove_all), " ")
    nd = n_elements(datac)

    if iresolution LT 0L then begin  ; Start with a GOF/MEM fit when iresolution starts at -1
      local[*] = 1.
      resolution = 1.
      npixons = n_elements(image)
    endif else if iresolution EQ 0L then begin
      resolution = pixon_sizes[iresolution]

    endif else begin
      resolution = pixon_sizes[iresolution]
    endelse


    if not_quiet then begin
      print
      print, 'RESOLUTION = ', strcompress(string(resolution), /remove_all)
      print
    endif

    if 0 then begin
      ;if keyword_set(dosnr AND 2) then begin ; full pixon map calculation
      ; Reset the pseudo-image to the image at the start of
      ; each resolution.  This should not really be necessary.
      if uselogarithm then eimage=exp(((uselogarithm*image)<Imax)>(-20.0)) $
      else eimage = (image>(max(image)/1.e32))<Imax
      pimage = hsi_pixon_local_smooth(eimage, local, pixon_sizes, fft=1)
      image = pimage
    endif

    iteration = 0L

    best_last_res = best

    REPEAT begin   ; Iterate to the solution for this resolution

      old_rgof     = rgof
      old_converge = converge
      old_pimage   = pimage
      old_image    = image
      if tty then wset, windows.map

      if (iterate_pixon_map OR (iteration EQ 0L)) AND $
        ;(iresolution GE 0) AND $
        (abs(resolution-bgpixon_size) GT 0.1) then begin
        ;(resolution NE max_pixon) then begin

        ; Get new pixon map

        last_local = local
        pmapstart = systime(1)
        if full_pm_calc then begin  ; Do a full pixon map calculation?
          local[*] = pixon_sizes[0]  ; the biggest pixon
          for ilocal = 1L, n_elements(pixon_sizes)-1 do begin
            if pixon_sizes[ilocal] GE resolution-0.01 then begin
              local = hsi_pixon_map(image, iobj, local, $
                datac, dobj, sobj, snr_sigma2, $
                pixon_sizes[ilocal], $
                nx, ny, nxy, nd, pixon_sizes, det, harmonics, $
                uselogarithm=uselogarithm, Imax=Imax, $
                npixons=npixons, pfraction=pfraction, $
                usepoisson=usepoisson, $
                sensitivity=sensitivity, $
                det_resolution=det_resolution, $
                not_quiet=not_quiet, tty=tty, $
                expansion=expansion, fast=1, $
                dummydir=dummydir, $
                /nolastprint, bobj=bobj, bflat=bflat, $
                time_unit = time_unit, time_bin = time_bin, $
                cbe_ptr = cbe_ptr, map_ptr = map_ptr, $
                det_eff = det_eff, $
                pixel_size = pixel_size)
            endif
          endfor
          if NOT keyword_set(quiet) then print
        endif else begin
          lllocal = local
          local = hsi_pixon_map(image, iobj, local, $
            datac, dobj, sobj, corrfac, snr_sigma2, resolution, $
            nx, ny, nxy, nd, pixon_sizes, det, harmonics, $
            uselogarithm=uselogarithm, Imax=Imax, $
            npixons=npixons, pfraction=pfraction, $
            usepoisson=usepoisson, $
            sensitivity=sensitivity, $
            det_resolution=det_resolution, $
            not_quiet=not_quiet, tty=tty, $
            expansion=expansion, fast=1, $
            dummydir=dummydir, bobj=bobj, bflat=bflat, $
            cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, $
            time_bin = time_bin, det_eff = det_eff, $
            pixel_size = pixel_size)
        endelse
        pmapstop = systime(1)
        if n_elements(pmaptime) LE 0 then pmaptime = pmapstop-pmapstart $
        else pmaptime = pmaptime + pmapstop-pmapstart
        ; Calculate when the pixon map last changed
        if min(local EQ last_local) then $
          pixon_map_no_change = pixon_map_no_change + 1L $
        else pixon_map_no_change = 0L

        if tty then begin
          ; Plot pixon map and color bar
          wset, windows.map
          tv, rebin(bytscl(alog(local), min=alog(resolution), $
            max=alog(max(pixon_sizes)), $
            top=!d.table_size-2), $
            nx*expansion, ny*expansion, /sample), $
            nx*expansion+20, ny*expansion
          mx = 10
          my = ny*expansion
          tickv = pixon_sizes[where(pixon_sizes-resolution GE -0.01, nticks)]
          color_bar, mx, my, (nx*expansion)*2+20-mx, ny*expansion, color=0, $
            min=resolution, max=max(pixon_sizes), top=!d.table_size-2, $
            tickv=tickv, ticks=nticks-1, type=1
        endif

        ; Figure out what fraction of the intensity is in the smallest
        ; spatial scale.
        if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
        else eimage = (image>(max(image)/1.e32))<Imax
        pimage = hsi_pixon_local_smooth(eimage, local, pixon_sizes, fft=1)

        ss_sm_scale = where(abs(local-min(local)) LE 0.1, nssss)
        if nssss GT 0 then begin
          sfraction = total(eimage[ss_sm_scale])/total(eimage)
        endif else begin
          sfraction = 0.0
        endelse

        ; Reset the pseudo image to the image at the start of each
        ; iteration.  This prevents the model from ending up providing
        ; an intensity determination rather than a scale determination.
        ; This is equivalent to smoothing the psuedo image at the start
        ; of each iteration.  This should probably be done at every
        ; step, but it is much faster not to do this once the model
        ; has converged.  LE 2 is the bare minimum.  LE 3 or LE 4
        ; might be more robust, but slower.  This is not done after
        ; the bg_pixon size since that just wipes out sidelobes. It
        ; is also not done if the model does not appear to be setting
        ; intensities (sfraction test).
        if pixon_map_no_change LE 2L AND $
          sfraction LT 0.5 AND $
          abs(resolution-pixon_sizes[1]) GT 0.1 then begin
          if uselogarithm then $
            image = uselogarithm*alog(double(pimage)>(max(pimage)/10000.)) $
          else $
            image = pimage
          ;message, /info, 'Copying image to pseudo image.'
        endif ;else message, /info, 'NOT Copying image to pseudo image.'

      endif

      ; Get a background estimate from the residuals by fitting
      ; sine waves with the spin period and twice the spin period.
      ; The residuals are phase binned before doing the fits.
      if (iteration GE 1L or (use_dfpmin and iresolution GT 0)) AND $
        ((iteration LE 2L-use_dfpmin) OR (iresolution EQ 0))then begin
        ; The first iteration generally gets the total flux right so we
        ; should not do the background iteration on the first pass.
        if background_model then begin
          if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
          else eimage = (image>(max(image)/1.e32))<Imax
          pimage = hsi_pixon_local_smooth(eimage, local, pixon_sizes, fft=1)
          ; if background=bobj is passed to hsi_pixon_residuals, then
          ; /noadd should NOT be set in hsi_pixon_fit_bgrnd, otherwise
          ; /noadd should be set.
          residuals = hsi_pixon_residuals(reform(pimage, nxy), dobj, iobj, corrfac,  $
            det, harmonics, modprofile=recon_data, $
            nounits=0, $
            cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
          ; There is a minus sign due to the definition of
          ; residuals in hsi_pixon_residuals.pro where the
          ; residual is mod_profile-fobs but the background
          ; is fobs-mod_profile.

          bobj = hsi_pixon_fit_bgrnd(-residuals, iobj, bobj, dobj, cbe_ptr, $
            det, harmonics, bflat=bflat, $
            self = self, $
            time_en_title = time_en_title, $
            tty=tty, windows = windows, /noadd)
          if tty then wset, windows.map ;default graphic



        endif
      endif


      ; Iterate to the best image given the current pixon map.
      ; Choose conjugate gradient or simplex minimization

      imgstart = systime(1)

      if keyword_set(use_powell) then begin
        ; ***********************
        ; * Powell Minimization *
        ; ***********************
        xi = fltarr(nxy, nxy)
        for ixi=0L, nxy-1L do xi[ixi, ixi] = image[ixi]
        tempimage = reform(image, nxy)
        powell, tempimage, xi, tolerance, mgof, 'hsi_pixon_gof_func'
        image = reform(tempimage, nx, ny)
        converge = 0.0
      endif else if keyword_set(use_dfpmin) then begin
        ; **********************************
        ; * DFP Minimization               *
        ; **********************************
        ; dfpmin requries things to be of order 1.
        ;dfpscale = [tsnr*nd, max(image)]
        dfpscale = [tsnr*nresid, max(image)] ;4-may-2010, ras
        dfpimage = image/dfpscale[1]
        if iteration EQ 0 then $
          last_mgof = hsi_pixon_dfp_gof_func(dfpimage)*dfpscale[0]
        catch, error_status
        if error_status NE 0 then begin
          message, /info, 'WARNING: Error in DFPMIN, skipping this resolution: '+ $
            !error_state.msg
          converge = 0.0
        endif else begin
          except = !except
          !except=0
          dfpmin, dfpimage, 1.e-7, mgof, $
            'hsi_pixon_dfp_gof_func', 'hsi_pixon_dfp_grad_func', $
            tolx=1.e-7, iter=iter, eps=(machar()).eps
          check = check_math(mask=32) ; Ignore underflows
          !except = except
          image = dfpimage*dfpscale[1]
          mgof = mgof * dfpscale[0]
          if mgof EQ 0.0 then converge = 0.0 $
          else converge = abs(mgof-last_mgof)/mgof
          last_mgof = mgof
        endelse
        catch, /cancel
      endif else begin
        ; **********************************
        ; * Conugate Gradient Minimization *
        ; **********************************

        minf_conj_grad, image, mgof, converge, func_name='hsi_pixon_gof_func', $
          use_deriv=usederivative, tolerance=tolerance, init=(iteration EQ 0)

      endelse


      imgstop = systime(1)
      lastimgtime = imgstop - imgstart
      if n_elements(totalimgtime) LE 0 then totalimgtime = 0.0d0
      totalimgtime = totalimgtime + lastimgtime

      if uselogarithm then eimage = exp(((uselogarithm*image)<Imax)>(-20.0)) $
      else eimage = (image>(max(image)/1.e32))<Imax
      pimage = hsi_pixon_local_smooth(eimage, local, pixon_sizes, fft=1)
      ;help, total( pimage ) , total( local )

      residuals = hsi_pixon_residuals(reform(pimage, nxy), dobj, iobj, corrfac,  det, harmonics, modprofile=recon_data, nounits=0, background=bobj, $
        cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
      gof = hsi_pixon_gof_calc(datac, recon_data, residuals, snr_sigma2, $
        poisson=usepoisson, nresid=nresid, norm_nozero=norm_nozero)
      ; Residuals for the pseudo-image
      presiduals = hsi_pixon_residuals(reform(eimage, nxy), dobj, iobj, corrfac,  det, harmonics, modprofile=precon_data, nounits=0, background=bobj, $
        cbe_ptr = cbe_ptr, map_ptr = map_ptr, time_unit = time_unit, time_bin = time_bin, det_eff = det_eff)
      pgof = hsi_pixon_gof_calc(datac, precon_data, presiduals, snr_sigma2, $
        poisson=usepoisson, nresid=npresid)

      rgof = gof/tsnr
      prgof = pgof/tsnr

      cancelled = 0
      if tty then begin   ; Plot results
        wset, windows.map
        tvscl, rebin(eimage, nx*expansion, ny*expansion, /sample), nx*0+20, ny*expansion
        xyouts, nx*expansion*0.5+20, ny*expansion, /device, align=0.5, strcompress(string(max(eimage)))
        ;;;;;;;;;;;;;;;;;;;;;;060915 ras


        wset, windows.rate
        hsi_pixon_plot_residuals, recon_data, dobj, residuals, $
          snr_sigma2, det, harmonics, $
          poisson=usepoisson, bobj=bobj, $
          NORM_NOZERO=NORM_NOZERO, $
          time_en_title = time_en_title
        wset, windows.map ;default graphic

@hsi_pixon_progbar 
      endif

      if not_quiet then begin ; Print status
        ;print, strcompress(string(iteration)+' GOF: '+string(rgof/n_elements(residuals))+ $
        print, strcompress(string(iteration)+' GOF: '+string(rgof/nresid)+ $
          ', convergence='+string(converge)+ $
          ', npixons='+string(npixons)+ $
          ;', Pseudo GOF='+string(pgof/tsnr/n_elements(presiduals))+ $
          ', Pseudo GOF='+string(pgof/tsnr/npresid)+ $
          ', Btot='+string(total(pimage))+$
          ', Bias='+string(total(residuals)))
      endif

      ;;if rgof LE best.gof then begin
      best.gof = rgof
      best.image = pimage
      best.resolution = resolution
      best.pixonmap = local
      ;;endif

      iteration = iteration + 1L

      ; A 'Q' at the keyboard exits the program gracefully.

      kbrd_quit=0
      kbrd_character=''
      ;endelse

      if ((iteration GT 40) OR kbrd_quit OR cancelled) then begin
        print
        print, 'CAUTION: Not fully converged'
        print
        goto, FINISH
      endif

    endrep UNTIL (converge LE 0.0 or (use_dfpmin AND converge LE 1.e-3 AND resolution GT minres)) OR $
      ((iteration GT 15) AND (resolution GT minres)) OR $
      ;(iteration GT 1) OR $
      (kbrd_character EQ 'S')
    ;message, /info, 'WARNING: Iterations stopped after 1 for testing'
    if not_quiet then begin
      print
      print, strtrim(strcompress('End of Resolution ' + $
        strtrim(strcompress(string(resolution), /remove_all), 2) + $
        ;'.   C = '+string(rgof/n_elements(residuals))), 2)
        '.   C = '+string(rgof/nresid)), 2)
      print
    endif

  endfor


  FINISH:
  bestimage = best.image
  if keyword_set(xycoordinate) then $
    bestimage = hsi_annsec2xy(bestimage, iobj)


  if NOT keyword_set(quiet) then print

  outresolution = min(best.pixonmap)
  pixonmap = best.pixonmap
  ;rgof = best.gof/n_elements(residuals)
  rgof = best.gof/nresid
  residual = residuals

  if tty then begin
    ;restore the saved color table
    tvlct, rsave, gsave, bsave
  endif

  if progbaropen then progbar, progobj, /destroy

  return, bestimage

end
