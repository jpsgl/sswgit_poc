;---------------------------------------------------------------------------
; Document name: hsi_qlook_aspect.pro
; Created by:    Jim McTiernan, May 17, 2001
;
; Last Modified: Sat Sep 21 11:18:10 2002 (jimm@ice)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
; NAME:
;       HSI_QLOOK_ASPECT
; PURPOSE: 
; CATEGORY:
; CALLING SEQUENCE: 
; hsi_qlook_aspect, as_time_intv = as_time_intv, $
;                   pnt_intv = pnt_intv, $
;                   Quiet = quiet, $
;                   obs_time_interval = obs_time_interval, $
;                   obs_summ_obj = obs_summ_obj, $
;                   n_sample = n_sample, $ ;for the aspect solution
;                   ref_time = ref_time, $
;                   file_time_intv = file_time_intv, $
;                   data = data, $
;                   npts_1_pointing = npts_1_pointing, $
;                   use_roll_db = use_roll_db, $
; INPUTS:
; via obs_time_interval or obs_summ_obj keywords
; OUTPUTS:
; in common block hsi_qlook_aspect_soln
; KEYWORDS: 
; obs_time_interval = a time range, preferably full orbit
; obs_summ_obj = an hsi_obs_summ_soc object, used in pipeline processing
; as_time_intv = time interval for roll period solution, default is 20
;                seconds
; pnt_intv = time interval for pointing, roll_angle solutions, default
;            is 1.0 second
; quiet = if set, not so much screen output
; n_sample = a binning value for roll period solution, the solution is
;            averaged of this many points (default of 128) then
;            interpolated to the interval given by as_time_intv
; ref_time = a reference time, passed into hsi_set_file_time, the
;            default is '4-jul-2000'
; file_time_intv = file times are integral numbers of this interval
;                  starting at the ref_time. the default is 20.0
;                  seconds. Don't change this
; npts_1_pointing = number of 4 second intervals for each aspect
;                   solution, the default is 30 (two minutes)
; use_roll_db = if set, use the roll database, otherwise calculate
;               PMTRAS solution directly
; COMMON BLOCKS:
; COMMON hsi_qlook_aspect_soln, hsi_qlook_as_struct ;for output
;  hsi_qlook_as_struct = {xy:pointing (heliocentric), $
;                         t_roll:roll period, $
;                         p_roll:roll phase (0 to 2*pi, same as roll angle)
;                         dt_aspect: 20 sec, $
;                         ut_ref_aspect: start time, $
;                         ntimes_aspect: number of roll_periods, $
;                         roll0: roll angle (0 to 2*!pi), $
;                         dt_pnt: 1 sec, $
;                         ntimes_pnt: number of pointing, roll angles,
;                         q: data quality small is best}
; HISTORY:
;       Version 1, May 17, 2001, 
;           jmm, jimm@ssl.berkeley.edu
;       Now runs off of data archive, obs_time_interval must be set,
;       jmm, 25-may-2002
;       Added catch for errors, jmm, 6-mar-2003
;       Split the pointing calculation into 2 minute intervals, Added
;       spin period calculation, jmm, 25-mar-2003
;       Added nolookup keyword for pmtras solution, jmm, 16-Oct-2003
;       Rewrite to do pmtras solution with 2 minute cadence, and add
;       roll phase output, jmm, 25-jul-2018
;-
;
PRO hsi_qlook_aspect, as_time_intv = as_time_intv, $
                      pnt_intv = pnt_intv, $
                      Quiet = quiet, $
                      obs_time_interval = obs_time_interval, $
                      obs_summ_obj = obs_summ_obj, $
                      n_sample = n_sample, $ ;for the aspect solution
                      ref_time = ref_time, $
                      file_time_intv = file_time_intv, $
                      data = data, $
                      npts_1_pointing = npts_1_pointing, $
                      use_roll_db = use_roll_db, $
                      _extra = _extra
                      
;Hsi_qlook_aspect_soln holds the aspect solution at 1 second intervals
  COMMON hsi_qlook_aspect_soln, hsi_qlook_as_struct

  IF(KEYWORD_SET(quiet)) THEN notquiet = 0 ELSE notquiet = 1
  t00 = !Stime
  IF(notquiet) THEN print, 'Aspect start:  ', t00
  t00 = anytim(t00)
;Initialize
  hsi_qlook_as_struct = -1
  xy = -1 & t_roll = -1 & roll0 = -1 & dt_aspect = 0 & ut_ref_aspect = 0
  ntimes_aspect = 0 & xy_sig = -1 & dt_pnt = 0 & ntimes_aspect = 0 & p_roll = -1
;Error handler, this should be able do deal with pmtras stops
  err_xxx = 0
  catch, err_xxx
  IF(err_xxx NE 0) THEN BEGIN
     catch, /cancel
     Print, 'Error'
     help, /last_message, output = err_msg
     FOR j = 0, n_elements(err_msg)-1 DO print, err_msg[j]
     print, 'Returning'
     hsi_qlook_as_struct = -1
     RETURN
  ENDIF
;Keywords
  IF(KEYWORD_SET(n_sample)) THEN nbin = n_sample ELSE nbin = 128
  IF(KEYWORD_SET(as_time_intv)) THEN dt_aspect = as_time_intv[0] $
  ELSE dt_aspect = 20.0d0
  IF(KEYWORD_SET(file_time_intv)) THEN dt_files = file_time_intv[0] $
  ELSE dt_files = 20.0d0
  IF(keyword_set(pnt_intv)) THEN dt_pnt = pnt_intv[0] $
  ELSE dt_pnt = 1.0
  IF(keyword_set(use_roll_db)) THEN nolookup = 0 ELSE nolookup = 1
;Do a pointing solution every 2 minutes
  If(Keyword_set(npts_1_pointing)) Then npts1 = npts_1_pointing $
  Else npts1 = 30
;You need to figure out what time you need the solution for
  IF(keyword_set(obs_summ_obj)) THEN BEGIN
     oflag = obs_summ_obj -> get(/obs_summ_flag)
     IF(obj_valid(oflag)) THEN BEGIN
        finfo = oflag -> get(/info)
        oti = finfo.ut_ref+[0.0, finfo.time_intv*finfo.n_time_intv]
     ENDIF ELSE BEGIN
        message, /info, 'No Obs Summ_flag, Returning'
        return
     ENDELSE
  ENDIF ELSE IF(keyword_set(obs_time_interval)) THEN BEGIN
;If obs_time_interval is set, then use that to get obs_summ_flag object
     oti = anytim(obs_time_interval)
     oflag = hsi_obs_summ_flag()
     dflag = oflag -> getdata(obs_time_interval = oti)
     IF(is_struct(dflag)) THEN BEGIN
        finfo = oflag -> get(/info)
        oti = finfo.ut_ref+[0.0, finfo.time_intv*finfo.n_time_intv]
     ENDIF ELSE BEGIN
        message, /info, 'No Obs Summ_flag, Returning'
        return
     ENDELSE
  ENDIF ELSE BEGIN  
     message, /info, 'No Obs Summary, or Obs_tim_interval set, Returning'
     return
  ENDELSE
;It's possible that the obs_summ_soc and obs_time interval are both set
  IF(keyword_set(obs_time_interval)) THEN oti = anytim(obs_time_interval)   
;All files start an integral number of dt from the start of the day
  oti[0] = hsi_set_file_time(oti[0], time_intv = dt_files, $
                             ref_time = ref_time)
  dt_total = oti[1]-oti[0]
  ntimes_pnt = long(dt_total/dt_pnt)
  IF((dt_total MOD dt_pnt) NE 0.0) THEN ntimes_pnt = ntimes_pnt+1
  oti[1] = oti[0]+ntimes_pnt*dt_pnt
  ntimes_aspect = long(ntimes_pnt*(dt_pnt/dt_aspect))
  ntimes_aspect = ntimes_aspect > 1
  npnt = long(dt_aspect/dt_pnt)
;Initialize output arrays, there will always be an output:
  t_roll = fltarr(ntimes_aspect)
  p_roll = fltarr(ntimes_aspect)
  xy = fltarr(2, ntimes_pnt)
  roll0 = fltarr(ntimes_pnt)
  t_roll0 = fltarr(ntimes_pnt)
  q = bytarr(ntimes_pnt)+255
  ut_ref_aspect = oti[0]
;tim_bins0, tim_arr0 refer to roll0, tim_arr to t_roll, tim_arr,
;tim_arr0 are center times
  time_bins0 = ut_ref_aspect+dt_pnt*findgen(ntimes_pnt+1)
  tim_arr0 = ut_ref_aspect+dt_pnt*findgen(ntimes_pnt)+dt_pnt/2.0
  tim_arr = ut_ref_aspect+dt_aspect*findgen(ntimes_aspect)+dt_aspect/2.0
  IF(notquiet) THEN message, 'Qlook Aspect_time_range:', /info
  IF(notquiet) THEN ptim, oti
;Create the aspect solution object
  o = obj_new('hsi_aspect_solution')
;create a packet object for pmtras analysis
  opak = obj_new('hsi_packet')
  IF(getenv('HSI_USE_ASPECT_SIM') Eq 'true') THEN BEGIN
     message, /info, 'SIMULATED ASPECT SOLUTION'
     o -> set, aspect_sim = 1b
;Uses the archive, 25-may-2002
     data = o -> getdata(obs_time_interval = oti, $
                         aspect_time_range = oti, $
                         _extra = _extra)
;Accumulate the averages
     IF(datatype(data) EQ 'STC') THEN BEGIN
        ndata = n_elements(data.roll)
;the period comes from the change in the roll angle
        droll_dt = (data.roll[1:*]-data.roll)
        droll_dt = [droll_dt, droll_dt[ndata-2]]
        dt = (data.time[1:*]-data.time)
        dt = [dt, dt[ndata-2]]/float(nbin)
        ok_roll = where(droll_dt GT 0 AND dt GT 0)
;temporary, 13-dec-2001
;      data.time = lindgen(n_elements(data.time))
        IF(ok_roll[0] NE -1) THEN BEGIN
           true_t0 = hsi_sctime2any(data.t0)+data.time[ok_roll]/float(nbin)
           temp_pnt = data.pointing[ok_roll, *]
           droll_dt = droll_dt[ok_roll]/dt[ok_roll]
;Use histogram with reverse indices
           histo0 = histogram(true_t0, min = oti[0], max = oti[1], $ 
                              binsize = dt_pnt, reverse_indices = r)
           n_histo0 = n_elements(histo0)
           n_histo0 = n_histo0 < ntimes_pnt
           FOR j = 0l, n_histo0-1l DO BEGIN
              IF(r[j] NE r[j+1]) THEN BEGIN
                 ss = r[r[j]:r[j+1]-1]
                 nnr = float(r[j+1]-r[j])
                 FOR k = 0, 1 DO BEGIN
                    xy[k, j] = total(temp_pnt[ss, k])/nnr
                 ENDFOR
                 t_roll0[j] = total(droll_dt[ss])/nnr
              ENDIF
           ENDFOR
;roll0 is the roll angle at the start of each interval
           roll0 = interpol(data.roll[ok_roll], true_t0, $ 
                            ut_ref_aspect+dt_pnt*findgen(ntimes_pnt))
           ok_roll1 = where(t_roll0 GT 0)
           IF(ok_roll1[0] NE -1) THEN BEGIN
              t_roll0[ok_roll1] = 2.0*!pi/t_roll0[ok_roll1]
;Grab every (dt_aspect/dt_pnt)th point for roll period
              ss_roll = npnt*lindgen(ntimes_aspect)
              xss_roll = where(ss_roll LE ntimes_pnt-1, nss_roll)
              IF(nss_roll GT 0) THEN BEGIN
                 ss_roll = ss_roll[xss_roll]
                 t_roll = t_roll0[ss_roll]
                 p_roll = roll0[ss_roll]
              ENDIF ELSE BEGIN
                 ntimes_aspect = 1
                 t_roll = t_roll0[0]
                 p_roll = roll0[0]
              ENDELSE
           ENDIF ELSE IF(notquiet) THEN message, /info, 'No Good roll angles'
        ENDIF ELSE IF(notquiet) THEN message, /info, 'No Good roll angles'
     ENDIF ELSE BEGIN 
        IF(notquiet) THEN BEGIN 
           message, /info, 'No Aspect data'
           print, anytim(oti[0], /ccsds)+' TO '+anytim(oti[1], /ccsds)
        ENDIF
        RETURN
     ENDELSE
  ENDIF ELSE BEGIN
     IF(notquiet) THEN message, /info, 'TRUE ASPECT SOLUTION'
     o -> set, aspect_sim = 0b
;Uses the archive, 25-may-2002
;SAS is done only for daylight periods, PMTRAS for the whole thing
     sunlight = oflag -> get(flag_name = 'SC_IN_SUNLIGHT')
     sun1 = oflag -> get(flag_name = 'ECLIPSE_FLAG')
;Rewrite, everything in 2 minute intervals, the end will overlap with
;the next orbit, but I don't think that will matter
     tarr = oflag -> get(/time_array)
     dt2 = npts1*finfo.time_intv
     nt2 = ceil((oti[1]-oti[0])/dt2)
     t2arr = oti[0]+dt2*dindgen(nt2)
     count = 0L
     tcount = 0L
     rcount = 0L
     FOR l = 0, nt2-1 DO BEGIN
        otil = t2arr[l]+[0.0, dt2]
;What obs_summ_flag points are in this interval?
        ss_intv = where(tarr Ge otil[0] And tarr Lt otil[1], nss_intv)
;Is there sunlight?
        insun = where(sunlight[ss_intv] GT 99 AND sun1[ss_intv] EQ 0, ninsun)
        IF(ninsun GT 0) THEN BEGIN
           data = o -> getdata(obs_time_interval = otil, $
                               aspect_time_range = otil, $
                               aspect_cntl_level = 5, $
                               _extra = _extra)
;Accumulate the pointing averages
           IF(is_struct(data)) THEN BEGIN
              IF(count EQ 0) THEN BEGIN
                 xy_all = data.pointing
                 true_t0 = hsi_sctime2any(data.t0)+data.time/float(nbin)
              ENDIF ELSE BEGIN
                 xy_all = [temporary(xy_all), data.pointing]
                 true_t0 = [temporary(true_t0), $
                            hsi_sctime2any(data.t0)+data.time/float(nbin)]
              ENDELSE
              count = count+1
              iqual = o -> get(/as_quality)
              IF(is_struct(iqual)) THEN BEGIN
                 IF(ptr_valid(iqual.triangle)) THEN BEGIN
                    IF(tcount EQ 0) THEN triangle = *iqual.triangle $
                    ELSE triangle = [temporary(triangle), *iqual.triangle]
                    tcount = tcount+1
                 ENDIF
              ENDIF
           ENDIF
        ENDIF ELSE data = -1
;Roll angle is interpolated directly into the output arrays
        IF(is_struct(data) && tag_exist(data, 'RAS_SOL')) THEN BEGIN
           rsol = data.ras_sol
        ENDIF ELSE BEGIN
;If it's nighttime, call pmtras_analysis directly
           pak = opak -> getdata(app_id = 154, $
                                 obs_time_interval = [otil[0]-1400.0, otil[1]+1400.0])
           IF(is_struct(pak) && n_elements(pak) GT 10) THEN BEGIN
              pmtras_analysis, packet_object = opak, pmtras_solution = rsol, $
                               nolookup = nolookup
           ENDIF ELSE rsol = -1
        ENDELSE
        IF(is_struct(rsol)) THEN BEGIN
;short dt_pnt intervals in this time range
           ss_intv0 = where(tim_arr0 Ge otil[0] And tim_arr0 Lt otil[1], nss_intv0)
;long dt_aspect intervals in this time range
           ss_intv1 = where(tim_arr Ge otil[0] And tim_arr Lt otil[1], nss_intv)
           true_trsol = hsi_sctime2any(rsol.t0_ref)+rsol.reltime
           droll_dt = deriv(rsol.reltime, rsol.posn_angle)
           okroll = where(finite(droll_dt) AND droll_dt GT 0, nokroll)
           IF(nokroll GT 0) THEN BEGIN
              droll_dt1 = 2.0*!pi/droll_dt[okroll]
              t_roll[ss_intv1] = interpol(droll_dt1, true_trsol[okroll], tim_arr[ss_intv1])
              p_roll[ss_intv1] = interpol(rsol.posn_angle, true_trsol, tim_arr[ss_intv1]) MOD (2.0*!pi)
              roll0[ss_intv0] = interpol(rsol.posn_angle, true_trsol, tim_arr0[ss_intv0]) MOD (2.0*!pi)
           ENDIF
           rcount = rcount+1
        ENDIF ELSE IF(notquiet) THEN BEGIN
           message, /info, 'No Roll Solution'
           ptim, otil
        ENDIF
     ENDFOR
;dump the objects
     IF(obj_valid(o)) THEN obj_destroy, o
     IF(obj_valid(opak)) THEN obj_destroy, opak
;Put pointing data in arrays for output
     IF(count GT 0 AND tcount GT 0) THEN BEGIN
        ndata = n_elements(true_t0)
;Use histogram with reverse indices
        histo0 = histogram(true_t0, min = oti[0], max = oti[1], $ 
                           binsize = dt_pnt, reverse_indices = r)
        n_histo0 = n_elements(histo0)
        n_histo0 = n_histo0 < ntimes_pnt
        FOR j = 0l, n_histo0-1l DO BEGIN
           IF(r[j] NE r[j+1]) THEN BEGIN
              ss = r[r[j]:r[j+1]-1]
              nnr = float(r[j+1]-r[j])
              FOR k = 0, 1 DO BEGIN
                 xy[k, j] = total(xy_all[ss, k])/nnr
              ENDFOR
              IF(triangle[0] NE -1) THEN BEGIN
                 IF(nnr EQ 1.0) THEN BEGIN
                    qtmp = abs(triangle[ss]) < 255.0
                 ENDIF ELSE BEGIN
                    qtmp = (sqrt(moment(triangle[ss]^2) ))[0] < 255.0
                 ENDELSE
                 q[j] = qtmp
              ENDIF
           ENDIF
        ENDFOR
     ENDIF ELSE IF(notquiet) THEN BEGIN
        message, /info, 'No Pointing Data'
        print, anytim(oti[0], /ccsds)+' TO '+anytim(oti[1], /ccsds)
     ENDIF
  ENDELSE
  hsi_qlook_as_struct = {xy:xy, t_roll:t_roll, dt_aspect:dt_aspect, $
                         ut_ref_aspect:ut_ref_aspect, p_roll:p_roll, $
                         ntimes_aspect:ntimes_aspect, roll0:roll0, $
                         dt_pnt:dt_pnt, ntimes_pnt:ntimes_pnt, q:q}
  RETURN
END

;---------------------------------------------------------------------------
; End of 'hsi_qlook_aspect.pro'.
;---------------------------------------------------------------------------
