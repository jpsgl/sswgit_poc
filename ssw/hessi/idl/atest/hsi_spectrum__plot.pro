;============================================================================
;+
; PROJECT:  HESSI
;
; NAME:  hsi_spectrum__plot
;
; PURPOSE:  plot method for hsi_spectrum object
;
; CATEGORY:
;
; CALLING SEQUENCE:  obj -> plot
;
; INPUTS:  none
;
; OPTIONAL INPUTS (KEYWORDS):
;	any keywords to set in the hsi_spectrum object, plus keywords
;	for any flags to display (options are  /saa, /night, /flare, /decimation,
;	/attenuator, /fast_rate)
;
;	pl_energy - if set, plots Flux vs Energy for multiple times (default)
;	pl_time - if set, plots Flux vs Time for multiple energy bands
;	pl_spec - if set, plots Spectrogram
;	pl_ltime_used - if set, plots livetime used (includes decimation and gaps)
;	pl_ltime_ctr - if set, plots corrected livetime counter
;	chan_in_kev - if set, and sp_chan_binning params is set in object, then
;	    spectrum x axis is keV instead of chan #.
;	xlog, ylog - if set, make that axis log
;	dim1_use - elements into third dimension (times for energy plot, energies for
;		time plot) to use
;	sepdet_bin - energy or time bin number to show when detectors are not summed (can
;		only show one energy or time bin in that case, since detector # is 3rd dimension)
;	plotman_obj - if called from plotman, this is the plotman_obj reference (not for users)
;	show_flags_obj - an existing hsi_show_flags object.  Otherwise creates a fresh one.
;	status - 0/1 = failure/success
;	err_msg - error message if any
; Also can pass in most of the plot keywords, as well as xyplot/utplot
; object keywords (e.g. dim1_sum)
;
; OUTPUTS:  None
;
; OPTIONAL OUTPUTS:  None
;
; Examples:
;	o -> plot, /dim1_sum ; combines time bins
;   o -> plot, /pl_time, /dim1_sum	;combines energy bins
;   o -> plot, /pl_ltime_used, charsize=1.5
;   o -> plot, /pl_time, sepdet_bin=4 ; if separate dets, plots energy bin 4
; Calls:
;
; COMMON BLOCKS: None
;
; PROCEDURE:
;
; RESTRICTIONS: None
;
; SIDE EFFECTS: None.
;
; EXAMPLES:
;
; HISTORY:
;	Written Kim, Mar 2001
;	Modifications:
;	30-Aug-2001, Kim.  Make histogram style plot the default.  Check if _extra has psym tag, and
;	  if not, add psym=10 to _extra.
;	18-Feb-2002, Kim.  Added date to label
;	5-Mar-2002, Kim.  Added option to display flags
;	30-Mar-2002, Kim.  Return error status if data (variable spec) is not a structure.
;   28-May-2002, Kim.  Use absolute_time_range instead of obs_time_interval for finding flags
;	04-oct-2002, Kim.  Removed xmargin=[12,3] because then stacked plots don't line up
;	10-Dec-2002, Kim.  Added common to save plot object if spectrogram plot.  This is very bad, but
;		until we have a new spectrogram plotter, need to be able to reconstruct how the axes were
;		drawn, so that the right-click point in plotman will work, and that's done in a method in the
;		plot obj. Also, now I destroy the plot object if not saving it.  This plugs a memory leak.
;	15-Jan-2002, Kim.  Histogram plots don't work with nsum>1 in datplot (called by xyplot).  So if
;		energies or times are evenly spaced, call xyplot (or utplot) with the average energy or time
;		instead of a low and high edge so datplot won't get called (will use IDL's histogram plotting)
;	05-Feb-2003, Kim.  Previously only worked for sp_data_structure=1, sp_data_unit='Flux'.  Now
;		handles whatever is set in object.
;	16-Feb-2003, Kim.  Enabled option to plot separate detectors (use only first time bin for
;		spectrum plots, only first energy bin for time plots).  Also handle indices (potentially
;		energy,time,det,coinc, in different orders depending on whether structure or not, and
;		what flags are set) in a safer way.
;	18-Feb-2003, Kim.  Enabled option to plot by channel number instead of energy in keV
;	25-Feb-2003, Kim.  Added chan_in_kev keyword.  If set and sp_chan_binning is set in object, then
;	    spectrum x axis is keV instead of chan #.
;	19-Mar-2003, Kim.  When getting info from saved_data, previously energy was always xaxis,
;		times always yaxis.  Now depends on what kind of plot we're plotting (because of a change
;       in how plotman__new_panel saves the panel).
;	30-Jul-2004, Kim. utplot object now expects a utbase, and x array relative to utbase
;	27-Apr-2005, Kim.  Added pl_ltime_used, pl_ltime_ctr keywords.  Also changed to always expect
;		data in structure (sp_data_str=1).  Added sepdet_bin keyword - previously always plotted
;		first time or energy bin when sep det, now can specify which single bin to plot.
;	13-May-2005, Kim. Fixed crash when plotting spectrum with only one energy bin
;   2-Jun-2005, Kim.  Added sum_coincidence test to coincidence_flag test
;	20-Oct-2006, Kim.  Don't need to check for coinc any more, due to change Richard made -
;		now getdata with structure never has the dimension for coinc.  (if sum_coinc set, the data
;		will be the sum of coinc and anti-coinc, if not, data is always just anti-coinc
;	4-Oct-2007, Kim. Added legend_loc keyword and made it default to 2 (upper right) for energy plot
;	10-Aug-2009, Kim.  Destroy objects before exiting (memory leak).
;	4-Oct-2010, Kim. If doing plotman plot, set utrange=timerange. xyplot options didn't have correct times.
;
;-
;============================================================================

pro hsi_spectrum::plot, $
	pl_energy=pl_energy, $
	pl_time=pl_time,$
	pl_spec=pl_spec, $
	pl_ltime_used=pl_ltime_used, $
	pl_ltime_ctr=pl_ltime_ctr, $
	chan_in_kev=chan_in_kev, $
	xlog=xlog, ylog=ylog, $
	legend_loc=legend_loc, $
	dim1_use=dim1_use, $
	sepdet_bin=sepdet_bin, $
	plotman_obj=plotman_obj, $
	show_flags_obj=flags_obj, $
	status=status, err_msg=err_msg, $
	_extra=_extra

status = 0
err_msg = 'No data to plot.'
from_saved = 0

checkvar, sepdet_bin, 0
local_flags_obj = 0

pl_energy = keyword_set(pl_energy)
pl_time = keyword_set(pl_time)
pl_spec = keyword_set(pl_spec)
pl_ltime_used = keyword_set(pl_ltime_used)
pl_ltime_ctr = keyword_set(pl_ltime_ctr)
pl_ltime = pl_ltime_used or pl_ltime_ctr
if not (pl_energy or pl_time or pl_spec or pl_ltime) then pl_energy = 1

if keyword_set(dim1_use) then dim1_use_input = dim1_use

if keyword_set(_extra) then begin
	if tag_exist(_extra, 'saved_data') then begin

		if ptr_exist (_extra.saved_data.data) then begin
			from_saved = 1
			specdata = *_extra.saved_data.data
			if size(/tname, specdata) ne 'STRUCT' then goto, checkerror
			if pl_energy then begin
				energies = *_extra.saved_data.xaxis
				times = *_extra.saved_data.yaxis
			endif else begin
				times = *_extra.saved_data.xaxis
				energies = *_extra.saved_data.yaxis
			endelse
			control = *_extra.saved_data.control
			info = *_extra.saved_data.info
			status = 1
		endif
	endif
endif

if not exist(specdata) then begin
	; force sp_data_struct=1, then reset to original value.  Need structure output here.
	sp_data_struct_save = self -> get(/sp_data_struct)
	specdata = self -> getdata(_extra=_extra, /sp_data_struct)
	self -> set, sp_data_struct = sp_data_struct_save
	if size(/tname, specdata) ne 'STRUCT' then goto, checkerror
	energies = self -> getaxis(/energy, /edges_2)
	control = self -> get(/control)
	info = self -> get(/info)
	status = 1
endif

checkerror:
if not status then goto, getout

case 1 of
	pl_ltime_used: spec = specdata.ltime
	pl_ltime_ctr: spec = info.livetime_ctr
	else: begin
		case control.sp_data_unit of
			'Counts': spec = specdata.counts
			'Rate': spec = specdata.rate
			'Flux': spec = specdata.flux
		endcase
		end
endcase
if not from_saved then times = specdata.ut

n_t = n_elements(times[0,*])
n_e = n_elements(energies[0,*])

; 20-Oct-2006 Richard changed coinc output - for structure, there's never a dimension for
; coincidence any more.  If sum_coinc is set, the data is the sum of coinc and anti_coinc.
; If coinc is set but sum_coinc isn't set, just get anti-coinc.

; Here's what we start with:
	; For summed det,
	;	If coincidence flag isn't set:
	;		data array is [nenergy, ntime],
	;		ltime array is [ntime] if no decimation, [nenergy,ntime] if decimation
	;	If coincidence flag is set:  20-Oct-2006 NOTE - don't have to check anymore
	;		then if sum_coincidence is set, data is [nenergy,ntime]
	;		and if sum_coincidence is not set, data is [nenergy,2,ntime]
	; For sep det, data array is [nenergy, ndet, ntime],
	;   ltime array is [1,ndet,ntime] if no decimation, [nenergy,ndet,ntime] if decimation
	;	(If coincidence is set, sum_coinc is always set for sep dets, so dimensions are
	;	stil [nenergy, ndet, ntime].  i.e. can't get sep coinc for sep dets)
	; livetime_ctr is always [ntime,18]

; Here's what we want spec to be for plotting:
	; For spectrum plot:
	;  If summed det,  [nenergy, ntime]
	;  If sep det, [nenergy, ndet] (use only first time bin)

	; For counts or photons time plot:
	;  If summed det, [ntime, nenergy]
	;  If sep det, [ntime, ndet] (use only first energy bin)

	; For livetime_used time plot:
	;  If summed det, then
	;    If no decimation, [ntime]
	;    If decimation, [ntime,nenergy]
	;  If sep det, then
	;    If no decimation, [ntime,ndet]
	;    If decimation, [ntime, ndet] (use first energy bin only)
	;
	; For livetime_ctr time plot:
	;  Always [ntime, 18]

; I don't think sep coinc has ever been tested
; Sep det and sep coinc doesn't work - just gives sep det
; If sep coinc, then data is [energy, coinc, time]

summed_det = pl_ltime_ctr ? 0 : control.sum_flag
dim1_is_energy = 0
decim = 0

if summed_det then begin		; detectors are summed

	dim1_is_det = 0
	dim1_enab_sum = 1
	det_string = hsi_a2d_list(control.det_index_mask, control.seg_index_mask, $
		control.a2d_index_mask)

;	20_Oct-2006 - Don't check for coinc any more.
;	if have separate coinc and anti-coinc, we'll just plot the anti_coinc (index 0)
;	if control.coincidence_flag and (not control.sum_coincidence) then $
;		spec = spec[*,0,*] else spec = spec[*,*,0]

	case 1 of
		pl_time: begin
			dim1_is_energy = 1
			spec = transpose(spec)
			end
		pl_spec: begin
			dim1_is_energy = 1
			spec = transpose(spec)
			end
		pl_ltime: begin
			if size(spec,/n_dim) eq 2 then begin
				decim=1
				spec = transpose(spec)
				dim1_is_energy = 1
			endif
			end
		else:
	endcase

;	if not pl_energy then begin
;		if not pl_ltime then dim1_is_energy = 1
;		if pl_ltime and (size(spec,/n_dim) eq 2) then dim1_is_energy = 1
;		if dim1_is_energy then spec = transpose(spec)  ; want [time,energy]
;	endif

endif else begin					; detectors are separate

	if pl_spec then begin
		err_msg = 'Invalid options - can not plot spectrogram with sum detectors disabled.'
		goto, getout
	endif
	dim1_is_det = 1
	det_string = pl_ltime_ctr ? hsi_a2d_list(bytarr(9)+1, bytarr(18)+1,bytarr(27)+1,/separate) $
		: hsi_a2d_list(control.det_index_mask, control.seg_index_mask, $
		control.a2d_index_mask, /separate)
	dim1_id = det_string
	dim1_name = 'Detector/Segment '
	dim1_unit = ''
	dim1_sum = 0
	if tag_exist(_extra, 'DIM1_SUM', /quiet) then _extra.dim1_sum = 0
	dim1_enab_sum = 0
	checkvar, dim1_use, indgen(n_elements(dim1_id))
	weighted_sum = 0
	case 1 of
		pl_energy: spec = spec[*,*,sepdet_bin]
		pl_time: spec = transpose(spec[sepdet_bin,*,*])  ; want [time,det]
		pl_spec: spec = transpose(spec[sepdet_bin,*,*])  ; want [time,det]
		pl_ltime_used: begin
			if n_elements(spec[*,0,0]) gt 1 then decim = 1
			bin = decim ? sepdet_bin : 0
			spec = transpose(spec[bin,*,*])  ; want [time,det]
			end
		pl_ltime_ctr:	; it's already [time,det]
	endcase
endelse

spec = reform(spec)  ; eliminate dimensions of 1

checkvar, ylog, 1
status = 0
err_msg = ''

if not tag_exist(_extra, 'PSYM', /quiet) then _extra = add_tag (_extra, 10, 'PSYM')

label = anytim(times[0,0],/vms,/date)
if not dim1_is_det then label = [label, 'Detectors: ' + det_string ]

what = control.sp_semi_calibrated ? 'Photon' : 'Count'
case control.sp_data_unit of
	'Counts': what = what + 's'
	'Rate': what = what + ' Rate'
	'Flux': what = what + ' Flux'
endcase

chan = control.sp_chan_binning ne 0

if chan then begin
	if control.sp_semi_calibrated or control.sp_data_unit eq 'Flux' then begin
		err_msg = 'Error - can not plot by channel with semi_cal or Flux selected.'
		goto, getout
	endif
endif

data_unit=info.units_spectrogram

if pl_energy then begin

	; X axis is energy

	checkvar, xlog, 1
	checkvar, legend_loc,2

	if chan then begin
		if keyword_set(chan_in_kev) then begin
			title = 'RHESSI ' + what + ' vs Channel in keV'
			xtitle = 'Channels (keV)'
			a2d = where(control.seg_index_mask, count)
			if count eq 0 or count gt 1 then begin
				err_msg = 'Error - can only plot channels in keV for a single detector/segment.'
				goto, getout
			endif
			e = hsi_get_e_edges(gain_generation=1000, gain_time_wanted=times[0,0], a2d_index=a2d[0])
			energies[0,*] = e[energies[0,*]]
			energies[1,*] = e[energies[1,*]]
			if xlog then begin
				q = where (energies[0,*] gt 0)
				energies = energies[*,q]
				spec = spec[q,*]
			endif
		endif else begin
			title = 'RHESSI ' + what + ' vs Channel'
			xtitle = 'Channel Number'
		endelse
	endif else begin
		title = 'RHESSI ' + what + ' vs Energy'
		xtitle = 'Energy (keV)'
	endelse

	if dim1_is_det then begin
		label = [label, anytim(times[0,sepdet_bin],/vms,/time) + ' - ' + anytim(times[1,sepdet_bin],/vms,/time) ]
	endif else begin
		; do weighted sum (weighted by time intervals) if flux or rate, otherwise not weighted sum
		weighted_sum = control.sp_data_unit eq 'Counts' ? 0 : 1

		if not exist(dim1_use) then dim1_use = indgen(n_elements(times[0,*]))
		dim1_vals = anytim(times,/vms)
		dim1_id = reform(anytim(times[0,*],/vms,/time) + ' - ' + anytim(times[1,*],/vms,/time))
		dim1_name = 'Time Intervals'
		dim1_unit = ''
	endelse

	; if energies are evenly spaced, use center of bins so histogram plotting works better
	evals = energies
	if n_elements(evals)/2 eq 1 then begin
		err_msg = 'Error - Can not plot vs energy with only one energy bin.'
		goto, getout
	endif
	mm = minmax(evals[1,*] - evals[0,*])
	if mm[0] eq mm[1] then evals = average(reform(evals,2,n_e),1) ; reform in case n_e=1


	obj = obj_new('xyplot',  evals,  spec, $
		status=status, err_msg=err_msg)

endif else begin

	; X axis is time.  Plotting a time profile, a spectrogram, or the livetime

	utbase=min(anytim(times))
	times = average (reform(anytim(times),2,n_t), 1)
	if n_elements(times) eq 1 then begin
		err_msg = 'Error - Can not plot vs time with only one time bin.'
		goto, getout
	endif
	if tag_exist(_extra,'timerange',/quiet) then if total(_extra.timerange) ne 0. then timerange=_extra.timerange
	if not exist(timerange) then timerange = hsi_get_time_range(control, info)

	if not keyword_set(flags_obj) then begin
	  flags_obj = hsi_show_flags(_extra=_extra)
	  local_flags_obj = 1
	endif
	flags_selected = flags_obj -> get(/selected)
	if total (flags_selected) gt 0 then begin
		flag_text = flags_obj -> getlabel (timerange, n_bar=n_bar)
		label[0] = label[0] + '   ' + flag_text
		if n_bar gt 0 then label = [replicate('',n_bar), label]
	endif

	checkvar, xlog, 0

	;hsi_energy_list, energies, dim1_en_id, dim1_en_name

	if chan then begin
		dim1_en_id = 'Channels ' + format_intervals(energies, format='(i6)')
		dim1_en_name = 'Channel Numbers'
		dim1_en_unit = 'channel'
	endif else begin
		dim1_en_id = format_intervals(energies, format='(f9.1)') + ' keV'
		dim1_en_name = 'Energy Channels'
		dim1_en_unit = 'keV'
	endelse

	if dim1_is_det then begin
		ny = n_elements(dim1_id)
		label = [label, dim1_en_id[sepdet_bin]]
	endif else if dim1_is_energy then begin
		ny = n_e
		dim1_unit = dim1_en_unit
		dim1_id = dim1_en_id
		dim1_name = dim1_en_name
		checkvar, dim1_use, indgen(ny)
		dim1_vals = energies
		endif

	case 1 of

		pl_time: begin			; Plotting a time profile
			; do weighted sum (weighted by energy intervals) if flux, otherwise not weighted sum
			weighted_sum = control.sp_data_unit eq 'Flux' ? 1 : 0
			title = 'RHESSI ' + what + ' vs Time'
;			; if times are evenly spaced, use center of bins so histogram plotting works better
;			mm = minmax(times[1,*] - times[0,*])
;			if mm[0] eq mm[1] then times = average(times,1)
			obj = obj_new('utplot', times-utbase, spec, $
				utbase=anytim(utbase,/vms), status=status, err_msg=err_msg)
			end

		pl_spec: begin			; Plotting a spectrogram
			title = 'RHESSI ' + what + ' Spectrogram'
			obj = obj_new('specplot', times-utbase, spec, $
			;	reform(transpose(spec), n_t, ny), $
				utbase=anytim(utbase,/vms), status=status, err_msg=err_msg)
			dim1_vals = average(dim1_vals,1)
			end

		pl_ltime_used: begin	; Plotting livetime used
			title = 'RHESSI Livetime Used'
			data_unit = 'Livetime Fraction'
			; in no energy dependence (decim=1), then don't want energy info in label
			if not decim then  label = anytim(times[0,0],/vms,/date)
			checkvar, ylog, 0
			dim1_enab_sum = 0
			dim1_sum = 0
			if not dim1_is_det then dim1_use = exist(dim1_use_input) ? dim1_use_input : 0
			obj = obj_new('utplot', times-utbase, spec, $
				utbase=anytim(utbase,/vms), status=status, err_msg=err_msg)
			end

		pl_ltime_ctr: begin		; Plotting livetime_ctr
			title = 'RHESSI Livetime Corrected Counter'
			data_unit = 'Livetime Fraction'
			label = anytim(times[0,0],/vms,/date)
			checkvar, ylog, 0
			obj = obj_new('utplot', times-utbase, spec, $
				utbase=anytim(utbase,/vms), status=status, err_msg=err_msg)
			end

	endcase
endelse

if not status then return

obj -> set, $
	id=title, $
	data_unit=data_unit, $
	dim1_vals=dim1_vals, $
	dim1_id=dim1_id, $
	dim1_unit=dim1_unit, $
	dim1_sum=dim1_sum, $
	dim1_enab_sum=dim1_enab_sum, $
	weighted_sum=weighted_sum, $
	dim1_use = dim1_use, $
	legend_loc=legend_loc, $
	label=label, $
	_extra=_extra, $
	status=status, err_msg=err_msg

if not status then return

; increase left margin (default is 10,3) because depending on length of y tic labels, sometimes label
; is off left of screen.
;!x.margin=[12,3] 		; removed 4-oct-02 because then stacked plots don't line up
obj -> plot, xtitle=xtitle, xlog=xlog, ylog=ylog, _extra=_extra, status=status, err_msg=err_msg

if not status then goto, getout

if not pl_energy then if total(flags_selected) gt 0 then flags_obj -> display, timerange

; if we're using plotman, have to reset the things we might have changed here
if is_class(plotman_obj, 'plotman', /quiet) then $
	plotman_obj -> set, $
		dim1_name=dim1_name, $
		dim1_ids=dim1_id, $
		dim1_use=dim1_use, $
		dim1_enab_sum=dim1_enab_sum, utrange=timerange

getout:
destroy, obj
if local_flags_obj then destroy,flags_obj  ; don't destroy if it gets passed out
if err_msg ne '' then begin
	message, err_msg, /cont
	status = 0
endif
end
