;+
; Name: select_channel_colors method for plotman
;
; Purpose: Widget interface to allow user to select color for
;	each channel separately.  Sets dim1_color in plotman object.
;
; Method: Called from plotman_xyoptions normally
;
; Written: Kim Tolbert 13-Jul-2003
;
;-

;---- update widgets with current color selection

pro plotman_select_channel_colors_update, state

for i = 0,n_elements(state.w_ids)-1 do begin
	ind = state.dim1_colors[i] - state.ncolors - 1
	widget_control, state.w_ids[i], set_droplist_select=ind
endfor

end

;----- event handler for plotman::select_channel_colors

pro plotman_select_channel_colors_event, event

widget_control, event.top, get_uvalue=state

widget_control, event.id, get_uvalue=uvalue

exit = 0

case uvalue of
	'color': begin
		q = where (event.id eq state.w_ids)
		state.dim1_colors[q[0]] = event.index + state.ncolors + 1
		end

	'reset': begin
		plot_defaults = state.plotman_obj -> get(/plot_defaults)
		state.dim1_colors = *plot_defaults.dim1_colors
		end

	'cancel': begin
		state.dim1_colors = state.orig_dim1_colors
		exit = 1
		end

	'accept': exit = 1

endcase

widget_control, event.top, set_uvalue=state
plotman_select_channel_colors_update, state

if exit then begin
	if not same_data(state.dim1_colors, state.orig_dim1_colors) then $
		state.plotman_obj -> set, dim1_colors=state.dim1_colors
	widget_control, event.top, /destroy
endif

end

;----- main program

pro plotman::select_channel_colors,group=group

cn = self -> get(/color_names)

dim1_colors =  self -> get(/dim1_colors)
ncolors = self -> get(/ncolors)

dim1_ids = self -> get(/dim1_ids)
nchan = n_elements(dim1_ids)
nchan_use = nchan < 17

if not keyword_set(group) then group=self->get(/plot_base)

tlb = widget_base (group_leader=group, $
					title='Select Colors for each Channel', $
					/column, $
					space=10, $
					/modal )

w_box = widget_base (tlb, /column, /frame, space=1)

w_ids = lonarr(nchan_use)
for i=0,nchan_use - 1 do begin
	w_row = widget_base(w_box, /row, space=20)
	w_ids[i] = widget_droplist (w_row, title=dim1_ids[i]+':    ', value=tag_names(cn), uvalue='color')
endfor

w_buttons = widget_base(tlb, /row, space=15)

w_reset = widget_button (w_buttons, value='Reset to Defaults', uvalue='reset')
w_cancel = widget_button (w_buttons, value='Cancel', uvalue='cancel')
w_accept = widget_button (w_buttons, value='Accept and Close', uvalue='accept')

widget_offset, parent, newbase=tlb, xoffset, yoffset

widget_control, tlb, xoffset=xoffset, yoffset=yoffset

widget_control, tlb,  /realize

state = {w_ids:w_ids, $
	dim1_colors: dim1_colors, $
	orig_dim1_colors: dim1_colors, $
	ncolors: ncolors, $
	plotman_obj: self }

plotman_select_channel_colors_update, state

widget_control, tlb, set_uvalue=state

xmanager, 'plotman_select_channel_colors', tlb

end