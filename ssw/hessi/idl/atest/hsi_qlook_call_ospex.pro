;+
;NAME:
; hsi_qlook_call_ospex
;PURPOSE:
; Given spectrum file, accompanying srm file, and start and end times,call
; ospex and get output;
;CALLING SEQUENCE:
Pro hsi_qlook_call_ospex, specfile, srmfile, st_time, en_time, $
                          o, spex_file, spex_plot_file, apar_in = apar_in, $
                          seg_index_mask = seg_index_mask, $
                          spectrum_file_dir = spectrum_file_dir, $
                          spectrum_plot_dir = spectrum_plot_dir, $
                          _extra = _extra
;INPUT:
; specfile = input fits file for the count spectra
; srmfile = input fits file for the response matrix, can be a
;            scalar, then all of the fits use the one srm file
; st_times, en_times = start and end times for intervals
;OUTPUT:
; o = the ospex object
; spex_file = the name of the output fits file
; spex_plot_file = the name(s) of output plot file(s)
;KEYWORDS;
; apar_in = initial fit parameters, for testing
;HISTORY:
; 19-jul-2004, jmm, jimm@ssl.berkeley.edu
; Added time information, 21-jul-2004, jmm.
; Added the output file names
;-
;Be sure not to have any windows...
  set_logenv, 'OSPEX_NOINTERACTIVE', '1'
;Gotta have a good catch
  o = -1 & spex_file = -1 & spex_plot_file = -1
  err = 0
  catch, err
  If(err Ne 0) Then Begin
    catch, /cancel
    Print, 'Oops'
    If(obj_valid(o)) Then obj_destroy, o
    o = -1 & spex_file = -1 & spex_plot_file = -1
    help, /last_message, output = err_msg
    For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
    print, 'Returning'
    Return
  Endif
  If(keyword_set(spectrum_file_dir)) Then spexfiledir = spectrum_file_dir $
  Else spexfiledir = '$HSI_QLOOK_SPECTRUM_FILE_DIR'
  If(keyword_set(spectrum_plot_dir)) Then spexplotdir = spectrum_plot_dir $
  Else spexplotdir = '$HSI_QLOOK_SPECTRUM_PLOT_DIR'
  ntimes = n_elements(st_time)
  tr = dblarr(2, ntimes)
  tr[0, *] = anytim(st_time)
  tr[1, *] = anytim(en_time)
  If(keyword_set(seg_index_mask)) Then Segi = seg_index_mask $
  Else Begin
    segi = [1, 0, 1, 1, 0, 1, 0, 0, 1, bytarr(9)]
    If(min(tr) Gt anytim('5-mar-2011 17:43:00')) Then segi[8] = 0
  Endelse
;First check the input
  If(is_string(specfile) Eq 0) Then message, 'No specfile input'
  If(is_string(srmfile) Eq 0) Then message, 'No srmfile input'
;It's possible to have only one srmfile
  n = n_elements(specfile)
  If(n Gt 1) Then Begin
    If(n_elements(srmfile) Ne n) Then srmfile = replicate(srmfile[0], n)
  Endif
;Get the object, do the fit
  o = ospex(/no_gui)
  o -> set, spex_specfile = specfile, spex_drmfile = srmfile, $
    spex_fit_time_interval = tr
;Set the background time interval to the first 4 seconds and last 4
;seconds
  ttemp = dblarr(2, 2)
  ttemp[*, 0] = min(tr)+[0.0, 4.0]
  ttemp[*, 1] = max(tr)+[-4.0, 0.0]
  o -> set, spex_bk_sep = 0
  o -> set, spex_bk_order = 1
  o -> set, spex_bk_time_interval = ttemp
  d = o -> getdata(class = 'spex_data')
  b = o -> getdata(class = 'spex_bk')
  odrm = o -> get(class = 'spex_drm', /object_reference)
  dummy = odrm -> getdata()     ;need this to get the area
  i = o -> get()                ;gives info parameters
;check rate data
  If(is_struct(d) Eq 0) Then message, 'No data'
  If(total(d.data) Le 0) Then message, 'No Non-zero data'
;d.data is counts, corrected for live time
  nchan = n_elements(d.data[*, 0])
  nt = n_elements(d.data[0, *])
  ebands = [reform(i.spex_ct_edges[0, *]), i.spex_ct_edges[1, nchan-1]]
;grab the background spectrum, for each time
;  hsi_new_spec_bck, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
;    seg_index_mask = segi
;counts
;  hsi_qlook_spec_bck, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
;    seg_index_mask = segi
  hsi_spec_bck, i.spex_ut_edges, ebands, brate1, sbrate1, /sum_flag, $
    seg_index_mask = segi;, /vs_time
  If(brate1[0] Ne -1) Then Begin
;Reset the background data
    o -> replacedata, brate1*d.ltime, /bkdata, /counts
    o -> replacedata, sbrate1*d.ltime, /bkdata, /error
  Endif Else message, /info, 'Could not get Background spectrum'
;Set some defaults
  o -> set, spex_erange = [min(i.spex_ct_edges), max(i.spex_ct_edges)], $
    fit_function = 'vth+3pow', spex_fit_manual = 0, mcurvefit_itmax = 50
  o -> set, _extra = _extra     ;eranges, fit_comp_free, etc...
;hmmm, here you need to initialize paramters, get flux_conversion
;factors, do the fits in a loop
  ntimes = n_elements(st_time)
;plot stuff
  set_plot, 'Z' & device, set_resolution = [600, 1200]
  !p.background = 255 & !p.color = 0
  !p.charsize = 1.5 & !x.charsize = 1.5 & !y.charsize = 1.5
  hsi_linecolors
  tvlct, r, g, b, /get
  spex_plot_file = strarr(ntimes) ;1 for each fit
;Do each interval
  For j = 0, ntimes-1 Do Begin
    ok_channels = hsi_qlook_okch(o, st_time[j], en_time[j], _extra = _extra)
    If(ok_channels[0] Ne -1) Then Begin
      erange_in = [min(i.spex_ct_edges[0, ok_channels]), $
                   max(i.spex_ct_edges[1, ok_channels])]
      print, 'Erange: ', erange_in
      If(keyword_set(apar_in)) Then Begin
        apar_2 = apar_in 
        fc_free = bytarr(8)
        dothefit = 1b
      Endif Else Begin
;Really force a thermal component by doing a fit for energy lt 12 keV
;to a thermal model
        apar_2a = hsi_qlook_init_apar(o, j, st_time[j], en_time[j], $
                                      erange = [3, 12], $
                                      ok_channels = ok_channels, $
                                      free = [1, 1, 0, 0, 0, 0, 0, 0, 0])
        apar_2a[0] = apar_2a[0]/3.0 ;kludge
        apar_2a = [apar_2a, 0.0, 0.0]
        apar_2a[3:8] = [1.0e-20, 1.0, 10.0, 1.0, 100.0, 1.0]
        print, 'Initial thermal pars:', apar_2a
        o -> set, fit_comp_param = apar_2a, spex_erange = [3.0, 12.0], $
          fit_comp_free = [1, 1, 0, 0, 0, 0, 0, 0, 0]
        o -> dofit, spex_intervals_tofit = j
        apar_2a = o -> get(/fit_comp_param)
        apar_2 = apar_2a        ;will be the initial pars...
;        set_plot, 'x' & !p.multi = intarr(5) & o -> plot_spectrum, /show_fit
        If(erange_in[1] Le 10.0) Then Begin ;done
          fc_free = [1, 1, 0, 0, 0, 0, 0, 0, 0]
          dothefit = 0b
          apar_2[3:8] = [1.0e-20, 1.0, 10.0, 1.0, 100.0, 1.0]
        Endif Else Begin
;Now initially a bpow fit from 10 to 100 keV
          apar_x = hsi_qlook_init_apar(o, j, st_time[j], en_time[j], $
                                       erange = [10, 100], $
                                       ok_channels = ok_channels, $
                                       free = [0, 0, 0, 1, 1, 1, 1])
          kx = apar_x[3] & mx = apar_x[4]
          ebr = apar_x[5] & mxu = apar_x[6]
;force a low energy break at 10 keV and fix the spectrum below at 1.5
          k1p5 = kx*(50.0/10.0)^(mx-1.5)
          apar_2[3:8] = [k1p5, 1.5, 10.0, mx, ebr, mxu]
          dothefit = 1b
          If(erange_in[1] Le 40.0) Then Begin
            fc_free = [1, 1, 0, 1, 0, 1, 1, 0, 0] ;fix low pl, not break, bpow
            apar_2[7:8] = [400.0, 1.0]
          Endif Else fc_free = [1, 1, 0, 1, 0, 1, 1, 1, 1] ;3pow
        Endelse
      Endelse

;minima and maxima, jmm, 8-nov-2015
      apar_nth_min = [1.0e-20,  1.5,   5.0,  2.0,  10.0,  2.5]
      apar_nth_max = [1.0e+20, 12.0, max(erange_in)-5.0, 10.0, 400.0, 12.0]
      apar_th_min = [1.0e-10, 0.25, 0.01]
      apar_th_max = [1.0e+10, 8.00, 10.0]

      apar_2 = apar_2 > [apar_th_min, apar_nth_min]
      apar_2 = apar_2 < [apar_th_max, apar_nth_max]
      print, 'Initial apar', apar_2
      o -> set, fit_comp_param = apar_2
      o -> set, fit_comp_minima = [apar_th_min, apar_nth_min]
      o -> set, fit_comp_maxima = [apar_th_max, apar_nth_max]
      o -> set,  spex_erange = erange_in
      o -> set,  fit_comp_free = fc_free
      o -> dofit, spex_intervals_tofit = j
;      set_plot, 'x' & !p.multi = intarr(5) & o -> plot_spectrum, /show_fit
;      set_plot, 'z'
;plot the fit
      !p.multi = [0, 1, 3]
;      o -> plot_spectrum, /photon, /show_fit
;      o -> plot_spectrum, /show_fit
;      o -> plot_summ, xplot = 'energy', yplot = 'resid'
;From Kim, June-2005
      emm = o -> get(/spex_erange)
      o -> plot_spectrum, /photon, /show_fit, /no_plotman, $
        spex_units = 'flux', /bksub, /overlay_back,  $
        dim1_colors = [0, 2], fit_color = [6, 3, 5], xrange = emm, $
        /no_timestamp
      o -> plot_spectrum, /show_fit, /no_plotman, spex_units = 'flux', $
        /bksub, /overlay_back,  dim1_colors = [0, 2], fit_color = [6, 3, 5], $
        xrange = emm, /no_timestamp
      o -> plot_summ, xplot = 'energy', yplot = 'resid', /no_plotman, $
        xrange = emm, /no_timestamp
;Filename
      pfile = 'hsi_qlspc_'+time2file(st_time[j], /sec)+'.png'
      spex_plot_file[j] = concat_dir(spexplotdir, pfile)
      write_png, spex_plot_file[j], tvrd(), r, g, b
    Endif Else message, /info, 'No Fit'
  Endfor
;Save the fit
  spex_file = 'hsi_qlspc_'+time2file(st_time[0], /sec)+'.fits'
  spex_file = concat_dir(spexfiledir, spex_file)
  o -> savefit, outfile = spex_file
  !p.multi = intarr(5)          ;reset this!@!!!!
  !p.charsize = 1.0 & !x.charsize = 1.0 & !y.charsize = 1.0
  Return
End



