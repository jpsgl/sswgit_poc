;+
; PROJECT:
;	HESSI
; NAME:
;	HSI_CLEAN_PARAMETERS
;
; PURPOSE:
;	Returns the structure which initializes the clean parameters.
;
; CATEGORY:
;	Util
;
; CALLING SEQUENCE:
;	clean_params = hsi_clean_parameters()
;
; CALLS:
;	none
;
; INPUTS:
;       none
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none, function returns parameter structure.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	Version 1, richard.schwartz@gsfc.nasa.gov, 26-apr-1999.
;	Version 2, richard.schwartz, 1-nov-1999.
;	Version 3, sa"m, 31-jan-2000.
;	10-Apr-2003, Kim.  Added cw_inverse
; 20-Mar-2007, Kim, changed niter to 100 (from 33) and frac to.05 from .10
; 16-Sep-2008, Kim.  Added chi_sq_min_test, show_n_maps, show_n_chi
;	9-jul-2009, ras, added beam_width_factor to condition output map with convolving beam
;		1-apr-2013, ras, removed ref to params not used, sigma, sigma_beam
;			sigma_factor, tau, lambda, mu, nu
;			added OLD_REGRESS_METHOD - known as CLEAN_OLD_REGRESS_METHOD externally
;     11-mar-2016, ras, allow input_dirty_map through control parameters
; 2-apr-2018, RAS, ('CLEAN_')REGRESS_METHOD is now a string with five possible values
;   clean_old_regress_method and clean_rsd_regress_method have no effect now and have been
;   removed. 
;
;-
function hsi_clean_parameters, dummy

; Returns a structure with the following parameters needed by
; hsi_map_clean.pro.

s = {hsi_clean_parameters}
s.niter = 100
s.more_iter = 0
s.negative_max_test = 1
s.chi_sq_min_test = 0
s.frac  = 0.05
s.chi_sq_crit= -1.0

s.beam_width_factor = 1.0
;sa"m more paramters added
s.no_chi2=1
s.show_maps=1
s.show_n_maps = 1
s.show_map_xdim=1024
s.show_chi=1
s.show_n_chi = 1
;s.nwindow=0
s.clean_box=ptr_new(0)
s.cw_list=ptr_new(0)
s.cw_nop=ptr_new(0)
s.cw_inverse=0

s.mark_box = 0	;kim
s.progress_bar = 1
s.media_mode=0
; Possible values for (clean_)regress_combine are chosen from 
; 'old_scaled_resid', 'full_resid', 'scaled_resid', 'no_resid', 'disable'
s.regress_combine = 'disable' 
s.input_dirty_map = ptr_new( 0.0 )
return,s

end






