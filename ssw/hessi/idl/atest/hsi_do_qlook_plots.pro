;---------------------------------------------------------------------------
; Document name: hsi_do_qlook_plots.pro
; Created by:    Jim McTiernan, January 17, 2002
;
; Last Modified: Tue Sep 17 13:57:01 2002 (jimm@ice)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
; NAME:
;       HSI_DO_QLOOK_PLOTS
; PURPOSE: 
;       Create Qlook plots from objects, count rate, image, spectra
;       Creates an HTML file with links to the plot files
; CATEGORY:
;       Qlook_archive
; CALLING SEQUENCE: 
;       hsi_do_qlook_plots, oobs, oimg, ospc, html_file, $ 
;           rate_plot_file, img_plot_file, spc_plot_file, $
;           quiet=quiet, qlook_plot_dir=qlook_plot_dir
; INPUTS:
;       oobs=hsi_obs_summ_soc object
;       oimg=An array of hsi_image objects
;       ospc=An array of hsi_spectrum objects
; OUTPUTS:
;       html_file=an HTML file with links to the files
;       rate_plot_file=the filename for the rate plots
;       img_plot_file=An array of filenames for the image plots
;       spc_plot_file=An array of filenames for the spectrum plots
; KEYWORDS: 
;       quiet=if set, no diagnostic output
;       qlook_plot_dir=the directory for the plot files, 
; HISTORY:
;       Version 1, January 17, 2002, 
;           jmm, jimm@ssl.berkeley.edu
;       Added rate_per_det plots, 18-mar-2002
;       Explicitly do set_plot,'z' and set_resolution, 16-feb-2005,
;       jmm
;       Added expand_plotdir option, for reprocessing, 13-oct-2009,
;       jmm
;-
PRO hsi_do_qlook_plots, oobs, oimg, ospc, $ 
       rate_per_det_obj, monitor_rate_obj, html_file, $
       crate_plot_file_file, rate_plot_file, $
       img_pfile, spc_pfile, $ 
       rate_per_det_plot_file, part_rate_plot_file, $
       quiet=quiet, qlook_plot_dir=qlook_plot_dir, $
       prefix_html=prefix_html, $
       use_gif=use_gif, expand_plotdir=expand_plotdir, $
       _extra=_extra
   
   html_file = -1
   rate_plot_file = -1
   crate_plot_file = -1
   rate_per_det_plot_file = -1
   img_pfile = -1
   spc_pfile = -1
   set_plot, 'Z'
   device, set_resolution = [640, 480]
   print, "!p.charsize = ", !p.charsize
   print, "!x.charsize = ", !x.charsize
   print, "!y.charsize = ", !y.charsize
;Write out plot files, if there is data
   IF(obj_valid(oobs) EQ 0) THEN BEGIN
      IF(NOT keyword_set(quiet)) THEN BEGIN
         message, /info, 'No Obs_summ_soc Object, Returning'
      ENDIF
      return
   ENDIF
   rate_obj = oobs -> get(/obs_summ_rate)
   IF(obj_valid(rate_obj) EQ 0) THEN BEGIN
      IF(NOT keyword_set(quiet)) THEN BEGIN
         message, /info, 'No valid rate_obj, returning'
      ENDIF
      return
   ENDIF
   rate_data = rate_obj -> get(/data)
   IF(datatype(rate_data) NE 'STC') THEN BEGIN
     IF(NOT keyword_set(quiet)) THEN BEGIN
       message, /info, 'No valid rate_obj, returning'
     ENDIF
     return
   ENDIF
   If(Keyword_set(use_gif)) then sfx = 'gif' else sfx = 'png'
;you need SAA, flare, and eclipse times
   oflag = oobs -> get(/obs_summ_flag)
   timarr0 = oflag -> get(/time_array)
   saa_flag = oflag -> get(/saa_flag)
   ecl_flag = oflag -> get(/eclipse_flag)
   flr_flag = oflag -> get(/flare_flag)
   temp_st_en, saa_flag, saa_st, saa_en, ok=ok_saa
   temp_st_en, ecl_flag, ecl_st, ecl_en, ok=ok_ecl
   temp_st_en, flr_flag, flr_st, flr_en, ok=ok_flr
;
   oinfo = oobs -> get(/info)
   sttime = time2file(anytim(oinfo.summary_start_time), /sec)
   rate_plot_file0 = 'hsi_'+sttime+'_rate.'+sfx
   crate_plot_file0 = 'hsi_'+sttime+'_corrected_rate.'+sfx
   IF(keyword_set(qlook_plot_dir)) THEN plotdir = qlook_plot_dir $
   ELSE plotdir = '$HSI_QLOOK_PLOT_DIR'
   If(keyword_set(expand_plotdir)) Then Begin ;jmm, 13-oct-2009
     date = strmid(sttime, 0, 8)
     pplotdir = hsi_mk_dbase_dir(date, plotdir)
     plotdir = pplotdir
   Endif
   rate_plot_file = concat_dir(plotdir, rate_plot_file0)
   crate_plot_file = concat_dir(plotdir, crate_plot_file0)
;set up colors from Kim
   tvlct, r_orig, g_orig, b_orig, /get
   hsi_linecolors
   !p.background = 255
   !p.color = 0
   dim1_colors = indgen(20)+1
   dim1_colors[0] = 0
   IF(datatype(rate_data) EQ 'STC') THEN BEGIN
      !p.multi = intarr(5)
      flag_changes = oflag -> changes()
      rate_obj -> plot, dim1_colors = dim1_colors, /ylog, $
        flag_changes = flag_changes, $
        flag_colors = [8, 4, 6, 9, 7, 12, 11, 2, 3], $
        /saa, /night, /flare, /atten, $
        /corrected_countrate, obs_summ_flag_obj = oflag, legend_loc = 6

      if(keyword_set(use_gif)) then write_gif, crate_plot_file, tvrd(), r, g, b $
      else write_png, crate_plot_file, tvrd(), r, g, b
      IF(NOT keyword_set(quiet)) THEN $
         message, /info, 'Created: '+crate_plot_file
      rate_obj -> plot, dim1_colors = dim1_colors, /ylog, $
        flag_changes = flag_changes, $
        flag_colors = [8, 4, 6, 9, 7, 12, 11, 2, 3], $
        /saa, /night, /flare, /decim, /rear_decim, /atten, $
        obs_summ_flag_obj = oflag, legend_loc = 6
      tvlct, r, g, b, /get
      if(keyword_set(use_gif)) then write_gif, rate_plot_file, tvrd(), r, g, b $
      else write_png, rate_plot_file, tvrd(), r, g, b
      IF(NOT keyword_set(quiet)) THEN $
         message, /info, 'Created: '+rate_plot_file
   ENDIF ELSE BEGIN
      IF(NOT keyword_set(quiet)) THEN BEGIN
         message, /info, 'No Obs_summ_rate data'
         return
      ENDIF
   ENDELSE
;particle rates
   part_rate_plot_file0 = 'hsi_'+sttime+'_part_rate.'+sfx
   part_rate_plot_file = concat_dir(plotdir, part_rate_plot_file0)
   IF(obj_valid(monitor_rate_obj)) THEN BEGIN
      timarr = monitor_rate_obj -> get(/time_array)
      !p.multi = [0, 1, 2]
      FOR j=0, 1 DO BEGIN
         monitor_rate_obj -> plot, dim1_colors = dim1_colors, $ 
            tag_name = 'particle_rate', channel=j, /ylog
;plot saa's,  ecl's, flares
         IF(ok_saa[0] NE -1) THEN BEGIN
            FOR k=0, n_elements(saa_st)-1 DO BEGIN 
               xxx = [timarr0[saa_st[k]], timarr0[saa_st[k]]]-timarr0[0]
               yyy = 10^[!cymin, !cymax]
               oplot, xxx, yyy, linestyle=2
               xxx = [timarr0[saa_en[k]], timarr0[saa_en[k]]]-timarr0[0]
               oplot, xxx, yyy, linestyle=2
               xp = timarr0[saa_st[k]]-timarr[0]+10.0
               yp = 0.7*(yyy[1]-yyy[0])+yyy[0]
               xyouts, xp, yp, 'S'
            ENDFOR
         ENDIF
         IF(ok_ecl[0] NE -1) THEN BEGIN
            FOR k=0, n_elements(ecl_st)-1 DO BEGIN 
               xxx = [timarr0[ecl_st[k]], timarr0[ecl_st[k]]]-timarr0[0]
               yyy = 10^[!cymin, !cymax]
               oplot, xxx, yyy, linestyle=1
               xxx = [timarr0[ecl_en[k]], timarr0[ecl_en[k]]]-timarr0[0]
               oplot, xxx, yyy, linestyle=1
               xp = timarr0[ecl_st[k]]-timarr[0]+10.0
               yp = 0.7*(yyy[1]-yyy[0])+yyy[0]
               xyouts, xp, yp, 'E'
            ENDFOR
         ENDIF 
         IF(ok_flr[0] NE -1) THEN BEGIN
            FOR k=0, n_elements(flr_st)-1 DO BEGIN 
               xxx = [timarr0[flr_st[k]], timarr0[flr_st[k]]]-timarr0[0]
               yyy = 10.0^[!cymin, !cymax]
               oplot, xxx, yyy
               xxx = [timarr0[flr_en[k]], timarr0[flr_en[k]]]-timarr0[0]
               oplot, xxx, yyy
               xp = timarr0[flr_st[k]]-timarr[0]+10.0
               yp = 0.7*(yyy[1]-yyy[0])+yyy[0]
               xyouts, xp, yp, 'F'
            ENDFOR
         ENDIF
      ENDFOR
      tvlct, r, g, b, /get
      if(keyword_set(use_gif)) then write_gif, part_rate_plot_file, tvrd(), r, g, b $
      else write_png, part_rate_plot_file, tvrd(), r, g, b
   ENDIF
;rates per detector, with monitor rates
   rate_per_det_plot_file0 = 'hsi_'+sttime+ $
      ['_front_rate.'+sfx, '_rear_rate.'+sfx]
   rate_per_det_plot_file = concat_dir(plotdir, rate_per_det_plot_file0)
   IF(obj_valid(rate_per_det_obj) AND obj_valid(monitor_rate_obj)) THEN BEGIN
      timarr = rate_per_det_obj -> get(/time_array)
      rate_per_det_data = rate_per_det_obj -> get(/data)
      monrate_data = monitor_rate_obj -> get(/data)
      IF(datatype(rate_per_det_data) EQ 'STC' AND $
         datatype(monrate_data) EQ 'STC') THEN BEGIN
         IF(n_elements(monrate_data) GT 1 AND $
            n_elements(rate_per_det_data) GT 1) THEN BEGIN
            !p.multi = [0, 3, 3]
            detid = ['1f', '2f', '3f', '4f', '5f', '6f', '7f', '8f', '9f', $
                     '1r', '2r', '3r', '4r', '5r', '6r', '7r', '8r', '9r']
            FOR l=0, 1 DO BEGIN
               j0 = l*9
               j1 = l*9+8
               FOR j=j0, j1 DO BEGIN
                  y = hsi_obs_summ_decompress(rate_per_det_data.countrate[j])
                  utplot, anytim(/yoh, timarr), y, yrange=[1.0, 4.0*max(y)], $
                     title='Det '+detid[j]+ $
                     ' BLACK = Events, RED = LLD', $
                     ytitle='Counts/sec', /ylog
                  outplot, anytim(/yoh, timarr), color=6, $
                     hsi_monitor_rate_decompress(monrate_data.shaper_valid[j])
;plot saa's,  ecl's, flares
                  IF(ok_saa[0] NE -1) THEN BEGIN
                     FOR k=0, n_elements(saa_st)-1 DO BEGIN 
                        xxx = [timarr0[saa_st[k]], timarr0[saa_st[k]]]-timarr0[0]
                        yyy = 10.0^[!cymin, !cymax]
                        oplot, xxx, yyy, linestyle=2
                        xxx = [timarr0[saa_en[k]], timarr0[saa_en[k]]]-timarr0[0]
                        oplot, xxx, yyy, linestyle=2
                        xp = timarr0[saa_st[k]]-timarr[0]+10.0
                        yp = 0.7*(yyy[1]-yyy[0])+yyy[0]
                        xyouts, xp, yp, 'S'
                     ENDFOR
                  ENDIF
                  IF(ok_ecl[0] NE -1) THEN BEGIN
                     FOR k=0, n_elements(ecl_st)-1 DO BEGIN 
                        xxx = [timarr0[ecl_st[k]], timarr0[ecl_st[k]]]-timarr0[0]
                        yyy = 10.0^[!cymin, !cymax]
                        oplot, xxx, yyy, linestyle=1
                        xxx = [timarr0[ecl_en[k]], timarr0[ecl_en[k]]]-timarr0[0]
                        oplot, xxx, yyy, linestyle=1
                        xp = timarr0[ecl_st[k]]-timarr[0]+10.0
                        yp = 0.7*(yyy[1]-yyy[0])+yyy[0]
                        xyouts, xp, yp, 'E'
                     ENDFOR
                  ENDIF 
                  IF(ok_flr[0] NE -1) THEN BEGIN
                     FOR k=0, n_elements(flr_st)-1 DO BEGIN 
                        xxx = [timarr0[flr_st[k]], timarr0[flr_st[k]]]-timarr0[0]
                        yyy = 10.0^[!cymin, !cymax]
                        oplot, xxx, yyy
                        xxx = [timarr0[flr_en[k]], timarr0[flr_en[k]]]-timarr0[0]
                        oplot, xxx, yyy
                        xp = timarr0[flr_st[k]]-timarr[0]+10.0
                        yp = 0.7*(yyy[1]-yyy[0])+yyy[0]
                        xyouts, xp, yp, 'F'
                     ENDFOR
                  ENDIF
               ENDFOR
               wpw = tvrd()
               if(keyword_set(use_gif)) then write_gif, rate_per_det_plot_file[l], wpw, r, g, b $
               else write_png, rate_per_det_plot_file[l], wpw, r, g, b
            ENDFOR
            !p.multi = intarr(5)
         ENDIF
      ENDIF
   ENDIF
   tvlct, r_orig, g_orig, b_orig
   
;Write out image and spectrum files, if there's data there
   IF(total(obj_valid(oimg)) EQ 0) THEN BEGIN
      IF(NOT keyword_set(quiet)) THEN message, /info, 'No Qlook_image Object'
      ok_img = -1
   ENDIF ELSE BEGIN
      n = n_elements(oimg)
      img_pfile0 = strarr(n)
      img_pfile = img_pfile0
      ok_img = bytarr(n)
      FOR j=0, n-1 DO BEGIN
         IF(obj_valid(oimg[j])) THEN BEGIN
            ok_img[j] = 1b
            itime = oimg[j] -> get(/absolute_time_range)
            eband = oimg[j] -> get(/energy_band)
            eband = strcompress(string(fix(eband)), /remove_all)
            img_pfile0[j] = 'hsi_qlookimg_'+time2file(itime[0], /seconds)+$
               '_'+eband[0]+'_'+eband[1]+'.'+sfx
            oimg[j] -> plot
            img_pfile[j] = concat_dir(plotdir, img_pfile0[j])
            if(keyword_set(use_gif)) then write_gif, img_pfile[j], tvrd() $
            else write_png, img_pfile[j], tvrd()
            IF(NOT keyword_set(quiet)) THEN $
               message, /info, 'Created: '+img_pfile[j]
         ENDIF
      ENDFOR
   ENDELSE
   
   IF(total(obj_valid(ospc)) EQ 0) THEN BEGIN
      IF(NOT keyword_set(quiet)) THEN message, /info, 'No Qlook_Spectrum Object'
      ok_spc = -1
   ENDIF ELSE BEGIN
      n = n_elements(ospc)
      spc_pfile0 = strarr(n)
      spc_pfile = spc_pfile0
      ok_spc = bytarr(n)
      FOR j=0, n-1 DO BEGIN
         IF(obj_valid(ospc[j])) THEN BEGIN
            ok_spc[j] = 1b
            itime = ospc[j] -> get(/absolute_time_range)
            spc_pfile0[j] = 'hsi_qlook_spc_'+time2file(itime[0], /seconds)+'.'+sfx
            ospc[j] -> plot
            spc_pfile[j] = concat_dir(plotdir, spc_pfile0[j])
            if(keyword_set(use_gif)) then write_gif, spc_pfile[j], tvrd() $
            else write_png, spc_pfile[j], tvrd()
            IF(NOT keyword_set(quiet)) THEN $
               message, /info, 'Created: '+spc_pfile[j]
         ENDIF
      ENDFOR
   ENDELSE
   
   html_file0 = 'hsi_'+sttime+'_qlook_plot.html'
   html_file = concat_dir(plotdir, html_file0)
;Write html file
;   IF(keyword_set(prefix_html)) THEN predir = prefix_html $
;      ELSE predir = 'http://hessi.sprg.ssl.berkeley.edu/data/metadata/latest/'
;   openw, unit, html_file, /get_lun
;   printf, unit, '<HTML>'
;   printf, unit, '<HEAD>'
;   printf, unit, '<TITLE>'+html_file0+'</TITLE>'
;   printf, unit, '</HEAD>'
;   printf, unit, '<BODY>'
;   printf, unit, '<H2>'+'QuickLook Plot Files: '+file_id+'</H2>'
;   printf, unit, '<p> <A HREF='+concat_dir(predir,rate_plot_file0)+'>'
;   printf, unit, rate_plot_file0+'</A>'
   
;   ok_img_1 = where(ok_img EQ 1)
;   IF(ok_img_1[0] NE -1) THEN BEGIN
;      FOR j=0, n-1 DO BEGIN
;         IF(ok_img[j] EQ 1) THEN BEGIN
;            printf, unit, '<p> <A HREF='+concat_dir(predir, img_pfile0[j])+'>'
;            printf, unit, img_pfile0[j]+'</A>'
;         ENDIF
;      ENDFOR
;   ENDIF
   
;   ok_spc_1 = where(ok_spc EQ 1)
;   IF(ok_spc_1[0] NE -1) THEN BEGIN
;      FOR j=0, n-1 DO BEGIN
;         IF(ok_spc[j] EQ 1) THEN BEGIN
;            printf, unit, '<p> <A HREF='+concat_dir(predir, spc_pfile0[j])+'>'
;            printf, unit, spc_pfile0[j]+'</A>'
;         ENDIF
;      ENDFOR
;   ENDIF
   
;   printf, unit, '<p> '+!stime
;   printf, unit, '</BODY>'
;   printf, unit, '</HTML>'
;   free_lun, unit
   
END


;---------------------------------------------------------------------------
; End of 'hsi_do_qlook_plots.pro'.
;---------------------------------------------------------------------------
