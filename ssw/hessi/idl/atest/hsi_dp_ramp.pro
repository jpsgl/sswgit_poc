;+
; :Description:
;    Returns ramp intervals.  Look for gt ntest consecutive high values to find ramps.
;
; :Params:
;    evchannel from - eventlist structure with channel, time, and a2d_index
;        IDL> help, ev,/st
;        ** Structure HSI_EVENT, 4 tags, length=12, data length=8:
;        TIME            LONG                 0
;        A2D_INDEX       BYTE         8
;        CHANNEL         INT             26
;        COINCID_MASK    BYTE         0;
;    time  - time in busec for evchannel
;    cfi   - gain coefficients
; :Keywords:
;    ntest - consecutive high values
;    channel_max - top ramp value, default, 1000
;    chan_min    - median of chan distribution, default from ev.channel
;    chan_lim    - bottom channel for ramp test
;    ch_bulk_lim - upper limit on energies used for median
;
; :Author: raschwar, 29-jan-2017
;  11-may-2017, ras, removed alternate chan_lim, 
;-
function hsi_dp_ramp, evchannel,time, cfi, ntest=ntest, $
  channel_max = channel_max, chan_min = chan_min, $
  chan_lim = chan_lim, $
  ch_bulk_lim = ch_bulk_lim, _extra=_extra

  two20 = 2d0^20
  default, ntest, 4
  default, channel_max, 1000
  ec = [0,evchannel,0] < channel_max
  nec = n_elements( ec )
  q  = where( ec lt ch_bulk_lim, nq)

  default, chan_min, median( ec[q]  )       ;bulk channel
  ;help, chan_min
  default, chan_lim, chan_min + 2* stddev( ec< 100) ;thresh for high chan
  if ec[nec-1] le chan_lim then ec[nec-1] = chan_lim + 1 ; terminate the end with an up or a down
  high = where( ec gt chan_lim, nq )
  break = where( high[1:*]-high gt 1, nbreak)
  ;test the chains
  istart = [high[0], high[break+1]]
  iend   = high[break]
  if n_elements(iend) lt n_elements(istart) and last_item(istart) eq (nec-1) then istart = istart[0:n_elements( istart )-2]
  istart = istart[1:*]-1 & iend = iend[1:*]
  test   = where( ( iend - istart ) ge (ntest-1), ntest)
  if ntest ge 1 then begin
    istart = istart[ test ]
    iend   = iend[ test]

    if ntest gt 1 then begin
      ;Test to extend istart
      z = where( evchannel[istart-1] gt chan_lim and (istart-1) ne 0, nz)
      if nz ge 1 then istart[z]--
      ;Test to extend iend. When ecc reaches chan_min
      qmin = where( ec[1:*] le chan_min, nqmin )
      iend = qmin[ value_locate( qmin, iend ) + 1]

    endif
    ;ramps = transpose( [[istart], [iend]])
    ;merge ramps
    rmask = bytarr(nec)
    istart <=iend
    for i=0,n_elements(istart)-1 do rmask[istart[i]:iend[i]]=1
    find_changes, rmask,index,state, index2d=ramps
    z=where( state, nz)
    if nz ge 1 then ramps = ramps[*,z] else ramps=0




  endif else ramps = 0
  ;Merge any ramp overlap

  return, ramps
end

;function hsi_dp_ramp, evchannel, time, cfi, ntest=ntest, $
; 
;  channel_max = channel_max, chan_min = chan_min, $
;  chan_lim = chan_lim, $
;  peak = kvpeak, $
;  ch_bulk_lim = ch_bulk_lim,  _extra=_extra
;
;  two20 = 2d0^20
;  default, ntest, 5
;  default, channel_max, 1000
;  default, kvpeak, 50.0 ;keV
;  ;channel peak
;  peak = (kvpeak - cfi[0] ) / cfi[1]
;  
;  ec = [0,evchannel,0] < channel_max
;  nec = n_elements( ec )
;  q  = where( ec lt ch_bulk_lim and ec gt 0, nq)
;  
;  ntest_use = ntest
;  ;t7   = find_cthresh(ec, 0.7)
;
;  default, chan_min, median( ec[q]  )       ;bulk channel
;  ;help, chan_min
;  default, chan_lim, chan_min + 2* stddev( ec< 100) ;> (25.-cfi[0])/cfi[1] ;thresh for high chan
;  hh=histogram(ec,min=0,max=1000)
;  thh=total(hh,/cum)
;  thh=thh/1./last_item( thh )
;  ;11-may-2017, ras, removed alternate chan_lim, 
;  ;chan_lim = value_locate( thh, 0.9)
;  ;if ec[nec-1] le chan_lim then ec[nec-1] = chan_lim + 1 ; terminate the end with an up or a down
;  high = where( ec gt chan_lim, nq )
;  break = where( high[1:*]-high gt 1, nbreak)
;  ;test the chains
;  istart = [high[0], high[break+1]]
;  iend   = high[break]
;  if n_elements(iend) lt n_elements(istart) and last_item(istart) eq (nec-1) then istart = istart[0:n_elements( istart )-2]
;  istart = istart[1:*]-1 & iend = iend[1:*]
;  htest  = histogram( ( iend - istart ), min= 0, max = 10, rev=rtest)
;  test   = where( ( iend - istart ) ge (ntest_use-1), ntesthi)
;
;  if ntesthi ge 1 then begin
;    istart = istart[ test ]
;    iend   = iend[ test]
;
;    if ntesthi gt 1 then begin
;      ;Test to extend istart
;      z = where( evchannel[istart-1] gt chan_lim and (istart-1) ne 0, nz)
;      if nz ge 1 then istart[z]--
;      ;Test to extend iend. When ecc reaches chan_min
;      qmin = where( ec[1:*] le chan_min, nqmin )
;      iend = qmin[ value_locate( qmin, iend ) + 1]
;
;    endif
;
;
;    ;ramps = transpose( [[istart], [iend]])
;    ;merge ramps
;    rmask = bytarr(nec)
;    istart <=iend
;    for i=0,n_elements(istart)-1 do rmask[istart[i]:iend[i]]=1
;    find_changes, rmask,index,state, index2d=ramps
;    z=where( state, nz)
;    if nz ge 1 then begin
;      ramps = ramps[*,z]
;      ramps[0,*]++
;      z = where( ec[ramps[0,*]]>ec[ramps[0,*]+1]>ec[ramps[0,*]+2] gt ch_bulk_lim, nz)
;      if nz ge 1 then ramps = ramps[*,z]
;    endif
;  endif
;  if ~keyword_set(nz) then ramps = 0
;  if ~exist( ramps ) then ramps = 0
;
;  ;Merge any ramp overlap
;
;  return, ramps
;end
