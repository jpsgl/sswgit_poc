;---------------------------------------------------------------------------
; Document name: hsi_spectrogram__define.pro
; Created by:    Andre, June 1999
; Time-stamp: <Mon Apr 18 2005 08:13:59 csillag tournesol.local>
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
;
; NAME:
;       SPECTROGRAM CLASS DEFINITION
;
; PURPOSE:
;       Generates and provides access to count rate spectrograms. This
;       is an abstract class inherited by hsi_spectrum and
;       hsi_lightcurve.
;
; CATEGORY:
;       HSI_Spectrogram (hessi/idl/spectra)
;
; CONSTRUCTION:
;       obj = Obj_New( 'hsi_spectrogram' )
;
; (INPUT) CONTROL PARAMETERS DEFINED IN THIS CLASS:
;
;      poisson (byte)
;      seed (float)
;      coincidence_flag (byte) : Default is 0.  Accumulate only
;                                anti-coincdent events. If set, then accumulate
;                     anti and coincident events into separate spectra.
;
;      other_a2d_index_mask (Bytarr(27)) : set this to define for
;                                          which other a2d the
;                                          spectrograms must be generated
;      sum_flag (byte): set this if you want to sum the spectrograms
;                       for the detectors/segments into a single value.
;      sum_coincidence: sum over coincidence condition
;      To avoid reprocessing the eventlist when changing sum_coincidence,
;      you must set coincidence_flag to 1. coincidence_flag will be set to 1
;      when sum_coincidence is set.
;
;      To see a list of all control parameters, use
;      o->Print, /CONTROL_ONLY
;      To assign a value to a control parameter, use
;      o->Set, KEYWORD=value
;      To retrieve the value of a control parameter, use
;      value=o->Get( /KEYWORD )
;
; (OUTPUT) INFORMATIONAL PARAMETERS DEFINED IN THIS CLASS:
;
;      binning: the binning structure passed to
;               Self->Histogram, which actually do the binning
;
;      To see a list of all info parameters, use
;      o->Print, /INFO_ONLY
;      To retrieve the value of an info parameter, use
;      value=o->Get( /KEYWORD )
;
; METHODS DEFINED OR OVERRIDDEN IN THIS CLASS::
;       GetData       Retrieves a spectrogram
;       Write         Writes a FITS binary table extension
;
; KEYWORDS:
;       EDGE_AXIS: (GetData) Set this to return the axis values at the
;                  edges of the bins on the spectrogram instead of the
;                  values at the center of the bins. This returns a 2x
;                  N array. Only with XAXIS or YAXIS
;       THIS_A2D_INDEX: (GetData) returns the spectrogram for this
;                       specific set of a2ds. Oonly with sum_flag=0
;       THIS_DET_INDEX: (GetData) returns the spectrogram for this
;                       specific set of detectors. Oonly with sum_flag=0
;       THIS_SEG_INDEX: (GetData)returns the spectrogram for this
;                       specific set of segments. Only with sum_flag=0
;       XAXIS: (GetData) Retrieves the energy axis of the spectrogram
;                        (1 dim, middle of the bin)
;       YAXIS: (GetData) Retrieves the time axis of the spectrogram
;                        (1 dim, middle of the bin)
; CLASS RELATIONSHIPS:
;       Framework: parent class
;       HSI_Eventlist: source class
;
; RELATED DATA STRUCTURES:
;       {hsi_spectrogram_control}
;       {hsi_spectrogram_info}
;
; SEE ALSO:
;        http://hessi.ssl.berkeley.edu/software/reference.html#spectrogram
;        framework__define
;        hsi_eventlist__define
;        spectrogram_control__define
;        spectrogram_info__define
;        hsi_lightcurve__define
;        hsi_spectrum__define
;
; HISTORY:
; ;
; 17-apr-2013, ras. The block to check the coincidence conditions has been moved to spectrogram
; 19-mar-2013, ras, added warning message about positive value in offset, probably bad table entries
;	hope to have this cleared up shortly,
;	9-sep-2009, ras, added rear_no_anti test to prevent rears from being removed from
;	the spectrum output
;
;  27-oct-2008, revised hsi_dropout_build to fix problems in aligning dropouts with their
;		proper time intervals
; 14-jul-2008, decim_table, to account for multiple weight states
;		within a rear decimation interval
;
;	3-oct-2007, ras, run the half-scale arfifact filter always, ->clear_halfscale, ev
;   8-jun-2007, ras, a drop in using FR_DEADTIME_WINDOW to help mark mcconnell's polarization analysis
;   16-apr-2007, ras, move sum_flag=1 from init to hsi_spectrum_control() for
;   spectrum and lightcurve
;   1-apr-2007, ras, don't fill livetime into empty sample bins for times gt 0.1 sec in
;     livetime-merge-datagaps

;   1-sep-2005, added hsi_spectrogram::fill_livetime to better identify times
;     of low deadtime when there are sparse numbers of reported livetime values.
;   21-jul-2005, ras added hsi_spectrogram::reform_struct to transform {a:fltarr(3), b:fltarr(3)} to
;     replicate( {a:0.0, b:0.0}, 3).  Used to transform SP_ATTEN_STATE
;   5-jul-2005, pileup tasks moved to new pileup obj inside of hsi_spectrum
;   30-jun-2005, ras, fixed bug decim_correct_ltime that only corrected first
;     interval of observation set with only a single decimation interval
;   22-may-2005, ras, fix bug forcing 0 livetime for rear when fronts are off
;     added rear decimation correction, attempt multiple decim states correction
;     across time interval - not reliable.
;   18-apr-2005, acs eliminate ::lookup and leave its task to hsi_eventlist_xxx.
;                this proc was used in ::decim_time_range, so the changes are there too
;   4-mar-2005, ras, added keep_dropout to call to process_hook_post
;     this faciliates saving livetime info if needed for diagnostics in binned_eventlist
;   15-feb-2005, ras, using floor function in time histogram to prevent
;     doubling of counts due to long function, i.e. long([-.5,.5]) are both 0!
;     which causes event doubling in first binned_eventlist bin!!
;   29-mar-2004, ras, fixed ptrarr call bug (pre 5.5) from previous fix
;   18-mar-2004, ras, move livetime_ctr=livetime_arr to position before
;     datagaps merged into livetime_arr so livetime_ctr retains its original
;     purpose of recording the counter livetime with no datagap considerations.
;   6-dec-2003 ras, fixed parentheses on test for gain_generation and gain_time_wanted
;   6-nov-2003, ras, Added protection against overlapping decimation states in the
;       same time bins
; Oct-2003, RAS, incorporate decimation correction routines within hsi_spectrogram__define.pro
;   fixed bug on non-contiguous time intervals.  Previously, livetime and datagap software
;   were not correctly accumulated if the time intervals weren't contiguous.
;    may-2003, acs, minor adaptations for event list files and simulations
; 23-dec-2002, ras, acs, integrate dropout_list from eventlist getdata()
; 20-DEC-2002, RAS, made det_index_mask/seg_index_mask more robust
;   in SET by using IS_NONZERO instead of KEYWORD_SET
; 20-dec-2002, ras, save livetime from counter in livetime_ctr in info
; 25-Oct-2002, Kim.  Added @hsi_insert_catch in get, getdata, get_timebin
; 1-sep-02, ras, checks size of energy axis before deleting top channel as unwanted
; 14-aug-02, ras, added propagation of GAIN_TIME_WANTED into hsi_get_e_edges
;     cleaned up channel selection and channel fractions for
;     normal energy intervals
; 21-jun-02, ras, ensure that the time_binning groups have either double precision
;   or long64 if the accumulation exceeds 2048 seconds, 2^31-1 bmicroseconds
; 10-jun-02, ras, now setdata to -1 for null eventlist
; 21-may-02, ras, adding livetime accumulation
; 17-may-02, ras, in Group, used long64 on time bin
; to protect against long time range overflows using long.
; 1-apr-2002, ras, added ENERGY and UT to GETAXIS keywords
;   Extract eventlist.time at full resolution, accumulate using time_unit
;   Must have full resolution to compute dropouts.
; March 19, 2002, ras, keep from crashing on empty eventlists.
; Release 6.1 March 4, 2002 - ras, fixed bugs to support sp_chan_binning
; Release 5.1 March 29, 2001 fixed bug with sp_time_interval.
;       Release 5.1 November 2000 rewritten for the fast Self->Histogram
;       Release 5, May-June 2000, ACS
;                  coincident event spectra +  packet bunches
;       Release 4, December 1999-February 2000
;           A Csillaghy, csillag@ssl.berkeley.edu (ACS)
;---------------------------------------------------------------------------

FUNCTION Hsi_Spectrogram::INIT, $
                    SOURCE=source, $
                    CONTROL=control, $
                    INFO=info, $
                    _EXTRA=_extra

IF NOT Obj_Valid(  source ) THEN source = HSI_Eventlist()
CheckVar, control, {hsi_spectrogram_control}
CheckVar, info, {hsi_spectrogram_info}

ret=self->Framework::INIT( SOURCE = source, CONTROL=control, INFO=info )

;16-apr-2007, ras, move sum_flag=1 from init to hsi_spectrum_control() for
;spectrum and lightcurve
;self->Set, SUM_FLAG = 1
self->Set, COINCIDENCE_FLAG =  0

IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

RETURN, ret

END

;-----------------------------------------
;This module selects arrays dimensioned by Energy, Time, A2d, Coinc
;on the basis of the A2D chosen using DET_INDEX_MASK or SEG_INDEX_MASK
;Can be used to select accumulated spectra, diagonal efficiencies, or
;even full response matrices (substituting Photon energy axis into
;normal time axis).

Pro HSI_SPECTROGRAM::A2D_SELECT, hsi_spectrogram, $
       THIS_DET_INDEX=this_det_index, $
       THIS_A2D_INDEX=this_a2d_index, $
       THIS_SEG_INDEX=this_seg_index, $
       SUMSTYLE=sumstyle, $
       ARFSTYLE = arfstyle

;SUMSTYLE controls whether the det_index or seg_index method requires summing, or
;only selection.  And Summing may be over the full a2d_list or partial.
;Adding counts is over the full a2d_list.
;Adding efficiency is over no more than two of the a2d_list, and
;choosing flux and rate normalization is only selection, not summing.
;So for summing counts (spectrogram input), SUMSTYLE is 2, use all a2d_list
;For efficiency, SUMSTYLE is 1
;For unit selection, SUMSTYLE is 0.
;SUMSTYLE EQ 2 - ADD INPUT FOR ALL SELECTED A2D TOGETHER - For Seg_index Selection
;   Add subarrays for all 3 A2D
;SUMSTYLE EQ 1 - ADD Front + Rear Input Arrays together
;SUMSTYLE EQ 0 - Don't add inputs together
;ARFSTYLE EQ 1-  FOR DET_INDEX, ARF detector index means the same, so just select
;           For SEG_INDEX, use detector index to return ARF array

sp_size = Size( /struct, hsi_spectrogram )
ncoinc = sp_size.dimensions[3]

a2d_index = Where( self->Get( /A2D_INDEX_MASK ), na2d )
CheckVar,sumstyle, 2 ;Default is for adding counts
If Keyword_Set( ARFSTYLE) then begin
    sumstyle = 0
    arfstyle = 1
    endif else arfstyle = 0

; access by detector: Add front a2d and both rear a2d for each det.
IF NOT EXIST( THIS_DET_INDEX ) THEN BEGIN
    det_index = Where( self->Get( /DET_INDEX_MASK ), ndet )
ENDIF ELSE BEGIN
    det_index =  this_det_index
    ndet = N_Elements( det_index )
ENDELSE
IF ndet NE 0 THEN BEGIN
    spectro= Fltarr( sp_size.dimensions[0], sp_size.dimensions[1]>1, ndet, ncoinc>1, /NOZERO )
    FOR i=0, ndet-1 DO BEGIN
        detdet = det_index[i]
        a2d_list = Where_Arr( a2d_index, [detdet, detdet+9, detdet+18] )
        a2d_list= sumstyle eq 2? a2d_list:(sumstyle eq 1? a2d_list[0:1]:a2d_list[0])
        spectro[*, *, i, * ] = N_Elements( a2d_list ) GT 1 ? $
           Total( hsi_spectrogram[*, *, a2d_list, * ], 3 ) : $
           hsi_spectrogram[*, *, a2d_list, * ]

    ENDFOR
    hsi_spectrogram = spectro
ENDIF ELSE BEGIN

    IF NOT EXIST( THIS_SEG_INDEX ) THEN BEGIN
        seg_index = Where( self->Get( /SEG_INDEX_MASK ), nseg )
                                ; access by segment
    ENDIF ELSE BEGIN
        seg_index = this_seg_index
        nseg = N_Elements( seg_index )
    ENDELSE
    seg_index = arfstyle ? seg_index mod 9 : seg_index
    IF nseg NE 0 THEN BEGIN
        spectro= Fltarr( sp_size.dimensions[0], sp_size.dimensions[1]>1, nseg, ncoinc>1, /NOZERO )
        FOR i=0, nseg-1 DO BEGIN
            segseg = seg_index[i]

            IF segseg GE 9 THEN segseg = [ segseg, segseg + 9 ]
            a2d_list = where_arr( a2d_index, segseg )
            ;a2d_list=N_Elements( segseg ) EQ 1 ? $
            ;    Where( a2d_index EQ segseg ) : $
            ;    Where_Arr( a2d_index, segseg )
            a2d_list=sumstyle ge 1? a2d_list:a2d_list[0]
            spectro[*, *, i, * ] = N_Elements( a2d_list ) GT 1 ? $
                 Total( hsi_spectrogram[*, *, a2d_list, * ], 3 )   : $
                 hsi_spectrogram[*, *, a2d_list, * ]

        ENDFOR
        hsi_spectrogram = spectro

    ENDIF ELSE IF EXIST( THIS_A2D_INDEX ) THEN BEGIN

        a2d_index_mask = Where( self->Get( /A2D_INDEX_MASK ), na2d )
        a2d_list = Where_Arr( a2d_index_mask, this_a2d_index, a2d_list  )
        hsi_spectrogram = hsi_spectrogram[*, *, a2d_list, * ]

    ENDIF

ENDELSE

END

;---------------------------------------------------------------------------

FUNCTION HSI_Spectrogram::Get, BINNING = binning, $
               SP_DP_CUTOFF = sp_dp_cutoff, $
               _EXTRA=_extra, $
            ENERGY_ONLY = ENERGY_ONLY, $
            FOUND=found, NOT_FOUND=NOT_found

@hsi_insert_catch

IF Keyword_Set( BINNING ) THEN BEGIN
    self->Control2Binning, ENERGY_ONLY = ENERGY_ONLY ;Control2binning must be defined by every sub-class of spectrogram
    ;control2binning is not defined  here.
ENDIF

;ras, 17 nov 2002 disable;
If 0 then begin ;If keyword_set( SP_DP_CUTOFF ) then begin
    dp_cutoff = Self->Framework::Get(/dp_cutoff)

    sp_dp_cutoff = Self->Framework::Get(/sp_dp_cutoff)
    if sp_dp_cutoff gt 0.0 then $
       if sp_dp_cutoff lt dp_cutoff then Self->Set, dp_cutoff = sp_dp_cutoff
    endif

RETURN, self->Framework::Get( BINNING = binning, $
                                       SP_DP_CUTOFF = SP_DP_CUTOFF, $
                              _EXTRA=_extra, FOUND=found, NOT_FOUND=NOT_found )

END

;---------------------------------------------------------------------------
;This function preserves the size of the existing control parameter
;by loading the Input_value into the existing variable.
Function HSI_Spectrogram::Loadvar, Input_value, _extra=_extra
  old_value = Self->Get(_extra=_extra)
  old_value[0]= input_value[*]
  return, old_value
  end
;---------------------------------------------------------------------------
PRO HSI_Spectrogram::Set, $
       A2D_INDEX_MASK=a2d_index_mask, $
       DET_INDEX_MASK=det_index_mask, $
       SEG_INDEX_MASK=seg_index_mask, $

       SUM_FLAG= sum_flag, $
       _EXTRA=_extra, DONE=done, NOT_FOUND=NOT_found



; det index and seg index do not triggering any processing. It is
; changes to a2d_index cause changes in processing.
If IS_NONZERO( A2D_INDEX_MASK) then $
    A2D_INDEX_MASK=Self->Loadvar( a2d_index_mask, /A2D_INDEX_MASK)

IF IS_NONZERO( DET_INDEX_MASK ) OR IS_NONZERO( SEG_INDEX_MASK ) THEN BEGIN
    if IS_NONZERO(DET_INDEX_MASK) then $
      det_index_mask = Self->loadvar(det_index_mask, /det_index_mask) else $
      seg_index_mask = Self->loadvar(seg_index_mask, /seg_index_mask)
    a2d_index_mask = Self->Get(/a2d_index_mask) or $
    HSI_DetSeg2A2d( DET_INDEX_MASK=det_index_mask, SEG_INDEX_MASK=seg_index_mask )
    IF IS_NONZERO( DET_INDEX_MASK ) THEN BEGIN
        self->Framework::Set, DET_INDEX_MASK = det_index_mask, SEG_INDEX_MASK=bytarr(18), /NO_UPDATE
    ENDIF ELSE BEGIN
        self->Framework::Set, DET_INDEX_MASK = bytarr(9), SEG_INDEX_MASK=seg_index_mask, /NO_UPDATE
    ENDELSE
ENDIF ELSE IF IS_NONZERO( A2D_INDEX_MASK ) THEN BEGIN
    self->Framework::Set, DET_INDEX_MASK = bytarr(9), SEG_INDEX_MASK=Bytarr(18), /NO_UPDATE
ENDIF

IF N_Elements( SUM_FLAG ) NE 0  then $
    Self->Framework::Set, /NO_UPDATE, SUM_FLAG = byte(sum_flag>0B < 1B)

self->Framework::Set, BINNING = binning, $
    A2D_INDEX_MASK=a2d_index_mask, $
    _EXTRA=_extra, FOUND=found, NOT_FOUND=NOT_found, DONE=done


END
;---------------------------------------------------------------------------
; 17-apr-2013, ras. This block checks the coincidence conditions
PRO HSI_Spectrogram::Set_Coincidence, $

       COINCIDENCE_FLAG = coincidence_flag, $
       SUM_COINCIDENCE  = sum_coincidence, $
       no_no_update     = no_no_update, $ ;if set (binned_eventlist does this) then any change is treated normally
       GROUPED          = grouped, $ ;if set, sum_coincidence sets coincidence_flag
        fw_set_id=fw_set_id

if ~(exist(coincidence_flag) or exist(sum_coincidence)) then return
; Check the coincidence control parameters and insure that they are consistent
default, coincidence_flag, self->get( /coincidence_flag )
default, sum_coincidence, self->get( /sum_coincidence )

dim_spectrogram = Self->Get(/dim_spectrogram)
have_coincidence= 0
if n_elements(dim_spectrogram) eq 4 then have_coincidence = dim_spectrogram[3] ge 2

no_update = have_coincidence ;coincidence_flag EQ ( coincidence_flag OR sum_coincidence )

coincidence_flag = coincidence_flag OR sum_coincidence
if keyword_set(no_no_update) then no_update = 0 ;use this for binned eventlist
if keyword_set(Grouped) then coincidence_flag = sum_coincidence
Self->FRAMEWORK::Set, $
  SUM_COINCIDENCE=sum_coincidence, $
  NO_UPDATE=no_update, $
  COINCIDENCE_FLAG=coincidence_flag, $
  fw_set_id = fw_set_id

END


;---------------------------------------------------------------------------

FUNCTION Hsi_Spectrogram::GetAxis, XAXIS = xaxis, YAXIS=yaxis, $
 UT= ut, ENERGY=energy, $ ras, 1-apr-2002
 MEAN=mean, GMEAN=gmean, $
 WIDTH=width, EDGES_1=edges_1, EDGES_2=edges_2, $
 EDGE_AXIS=edge_axis, _EXTRA=_extra

@hsi_insert_catch

 Self->Set, _EXTRA=_extra
 IF Keyword_Set( UT ) or Keyword_Set( ENERGY )then begin
     xaxis = keyword_set( energy)
     yaxis = keyword_set( ut )
     endif
 binning = self->Get( /BINNING, ENERGY_ONLY=ENERGY )

 IF Keyword_Set( YAXIS ) THEN BEGIN
     IF ChkTag( binning, 'NEW_TIME_CAL' ) THEN BEGIN
         axis = binning.new_time_cal
         ENDIF ELSE BEGIN
         abs_time_range = self->Get( /ABSOLUTE_TIME_RANGE )
         IF ChkTag( binning, 'TIME_GROUP' ) THEN BEGIN
             axis = binning.time_group + abs_time_range[0]
             ENDIF ELSE BEGIN

             time_binning = Self->Tbin_prep()
             axis = time_binning.newcal
             ENDELSE
         ENDELSE
     ENDIF ELSE BEGIN
     IF Tag_Exist( binning, 'PREDEFINED_CHANNEL_BINNING' ) THEN BEGIN
         HSI_RD_CT_Edges, binning.PREDEFINED_CHANNEL_BINNING, axis
         endif else begin

      ;If we already have the channel_cal_new, no need to get a more
      ;complicated version
         channel_binning = Tag_Exist( binning,'CHANNEL_CAL_NEW')? $
             binning:Self->Chan_Prep()

           If  Tag_exist( channel_binning, 'CHANNEL_CAL_NEW') then $

         axis = float( binning.channel_cal_new )
         If Tag_Exist( channel_binning, 'ADDZERO') then begin
             remove_first_chan = keyword_set( channel_binning.addzero )
             checkvar, remove_last_chan, remove_first_chan
             channel_regroup = channel_binning.regroup
             i0 = remove_first_chan
             ;i1 = n_elements(channel_regroup[0,*]) - 1 - remove_last_chan
             i1 = n_elements(channel_regroup[0,*]) - 1
             if keyword_set( channel_regroup ) then begin
                 channel_regroup = channel_regroup + remove_first_chan
                 channel_regroup[1,*] = channel_regroup[1,*] +1
                 axis = (channel_binning.newcal)[channel_regroup]
                 endif else axis = channel_binning.newcal[i0:*]
             axis=float(axis)
             endif
         endelse

     ENDELSE

 nels = N_Elements( axis )
 IF nels GT 1 THEN BEGIN
     Edge_Products, axis, MEAN=this_mean, GMEAN=this_gmean, $
     WIDTH=this_width, EDGES_1=this_edges_1, EDGES_2=this_edges_2
     this_width = float( this_width )
     CASE 1 OF
         Keyword_Set( EDGES_1 ): RETURN, this_edges_1
         Keyword_Set( FULL ): RETURN, {MEAN:this_mean, GMEAN:this_gmean, $
         WIDTH:this_width, $
         EDGES_1:this_edges_1, EDGES_2:this_edges_2}
         Keyword_Set( EDGES_2 ): RETURN, this_edges_2
         Keyword_Set( GMEAN ): RETURN, this_gmean
         Keyword_Set( WIDTH ): RETURN, this_width
         ELSE: RETURN, this_mean
         ENDCASE
     ENDIF ELSE RETURN, axis

 END


;---------------------------------------------------------------------------
;;Placeholder for daughter classes
;FUNCTION Hsi_Spectrogram::GetPileup, _extra=_extra
;
;return,0
;end
;---------------------------------------------------------------------------

FUNCTION Hsi_Spectrogram::GetData, $
            THIS_A2D_INDEX=this_a2d_index, $
            THIS_DET_INDEX=this_det_index, $
            THIS_SEG_INDEX=this_seg_index, $
            XAXIS=xaxis, $
            YAXIS=yaxis, $
            ;NOPILEUP_CORRECT=NOPILEUP_CORRECT, $ ;ras 13-aug-03
            _EXTRA=_extra

@hsi_insert_catch

; first redirect if axes requested
IF Keyword_Set( XAXIS ) OR Keyword_Set( YAXIS ) THEN $
    RETURN, self->GetAxis( XAXIS=xaxis, YAXIS=yaxis, _EXTRA=_extra )

sp_need_update = self->need_update()
hsi_spectrogram=self->Framework::GetData( _EXTRA = _extra )
; returns -1 if hsi_spectrogram has no dimensions acs 2002-03-06
IF size( hsi_spectrogram, /n_dim ) EQ 0 THEN return, -1



; we could pass this with _extra, but for  a reason I dont know I like
; this better (dont want to pass all the other keywords too)
self->A2D_SELECT, hsi_spectrogram, $
    THIS_A2D_INDEX=this_a2d_index, $
    THIS_DET_INDEX=this_det_index, $
    THIS_SEG_INDEX=this_seg_index

RETURN, hsi_spectrogram

END

;---------------------------------------------------------------------------
function  hsi_spectrogram::tbin_prep, _extra=_extra

IF keyword_set( _extra ) then Self->Set, _EXTRA=_extra


;input = Self->Get(/binning,   /time_bin_min, /time_bin_def)
binning = Self->Get(/binning) ;
time_unit = Self->Get(/time_unit)
ut_ref    = Self->Get(/ut_ref)
time_range = Self->Get(/absolute_time_range) - ut_ref

;binning = input.binning

binned_eventlist = gt_tagval( binning,'BINNED_EVENTLIST',missing=0 )
if keyword_set( binned_eventlist ) then return, binned_eventlist



binning_info  = add_tag( binning, 0.0d0, 'ut_ref0' )  ;ras - 7/23/2001
time_binning    = Self->get_timebin(binning_info, $
  time_range, ut_ref, time_unit, tbinning)
;
;
;If there are any time gaps or overlaps, the final time summations
;are defined in time_final_regroup
;

time_regroup = gt_tagval(tbinning,'REGROUP',missing=0)


return, add_tag( time_binning, time_regroup, 'REGROUP')
end
;---------------------------------------------------------------------------
function hsi_spectrogram::Get_Offset_Gain
;A little mini-object to manage the parameters that determine gain

gain_generation = Self->Get(/gain_generation)
gain_time_wanted = Self->Get(/gain_time_wanted)
gain_time_wanted = keyword_set( gain_time_wanted )? gain_time_wanted : $
      (Self->Get(/absolute_time_range))[0]
offset_gain_str = Self->Get(/offset_gain_str)
;RAS, 6-dec-2003 fixed parentheses on test for gain_generation and gain_time_wanted
if (not (gain_generation eq offset_gain_str.gain_generation)) or $
   (not (gain_time_wanted eq offset_gain_str.gain_time_wanted)) then begin
    offset_gain          = float( hsi_get_e_edges( gain_generation = gain_generation,$
    gain_time_wanted=gain_time_wanted, /coeff))
    Self->Set, offset_gain_str = {hsi_offset_gain_str, offset_gain, gain_generation, $
        gain_time_wanted}

    endif else offset_gain = offset_gain_str.offset_gain

return, offset_gain

end
;---------------------------------------------------------------------------
function  hsi_spectrogram::chan_prep, _EXTRA=_extra

IF keyword_set( _extra ) then Self->Set, _EXTRA=_extra

input = Self->Get(/a2d_index_mask, /binning)

these_a2d        = where( input.a2d_index_mask )
binning          = input.binning





; acs added time_unit and time range, to make the procedure
; independent from the object. We pass eventlists where we have
; already selected the events to include. This is easier
; because of all the keywords for coincidence checking (see the hsi_spectrogram__define)

nkeep = 0

constant = hessi_constant()

na2d    = n_elements( these_a2d )
checkvar, ut_ref0, ut_ref   ;ras - 7/23/2001

;
; From the specification of the new channel boundaries, return
; the grouped channel ids. For each Ei and Ei+1 (for new channel i)
; find the ID[i] and ID[i+1] wholly contained with the energy bin, or just greater
; than the edge, Ei or Ei+1.  Also, return all bins, which include an edge (Ei).
; If should be a unique set of channel id's. Bin IDs are associated with the
; index of the leading boundary.
;
binning_info  = binning  ; preserve the original binning, if needed.
Self->chan_overlap_fix, binning

channel_regroup = gt_tagval(binning,'REGROUP',missing=0)
offset_gain = Self->Get_offset_gain()



addzero = 1
Self->group_Multi, binning, these_a2d, offset_gain, channel_binning, $
addzero=addzero
;NEW_GAIN and gain_time_wanted added ras 22-feb-2001, gain_time_wanted still unused

channel_binning = add_tag( channel_binning, $
channel_regroup,   'REGROUP')
channel_binning = add_tag( channel_binning, addzero, 'addzero')
;
;

return, channel_binning
end



;---------------------------------------------------------------------------

PRO Hsi_Spectrogram::Process_Hook_Pre, $
       _EXTRA=_extra

END


;---------------------------------------------------------------------------
function hsi_spectrogram::reform_struct, var, tag, error=error
; Purpose     : Convert a structure with tag arrays to a structure array
;               where each tag is an element of each tag array. The output
;               structure array will have a dimension equal to the dimension
;               of the tag arrays. If tags have different dimensions, then
;               the output structure will have a dimension equal to the tag
;               with the maximum dimension.
;
; Example     : IDL> stc={a:findgen(100),b:findgen(100)}
;               IDL> out=self->reform_struct(stc)
;               IDL> help,out
;               OUT    STRUCT    = -> <Anonymous> Array[100]
    if n_elements( var ) gt 1 then return, var
    error = 1

    tags = tag_names( var )
    default, tag, tags[0]
    test = have_tag( var, tag, i)
    if not test then begin
       message,/continue,tag + ' not found'

       return, var
       endif

    nel = n_elements( var.(i) )
    ntag = n_elements(tags)
    nels = lonarr( ntag)
    for i = 0, ntag-1 do nels[i] = n_elements( var.(i))
    sel = where( nels eq nel, nsel)
    var1 = str_subset( var, tags[sel])
    base = str_tagarray2scalar( var1, 0)
    var2 = replicate( base, nel)
    for i=0, nsel-1 do var2.(i) = var1.(i)
    error = 0
    return, var2
    end

;---------------------------------------------------------------------------

PRO Hsi_Spectrogram::Process, $
Keep_dropout = Keep_dropout, $
 _EXTRA=_extra

Self->Process_hook_pre, _EXTRA=_extra

 IF keyword_set( _extra ) then Self->Set, _EXTRA=_extra

 IF self.debug GT 0 THEN Message, 'Processing ...', /INFO
;Message, 'Processing ...', /INFO

 binning = Self->Get( /BINNING )
 ;We might need these someday to assign fractional counts
 poisson = self->Get( /POISSON )
 seed = self->Get( /SEED )


 ; the spectrogram gets formed *only* for the a2ds, not for the dets or
 ; segs. The dets/segs are selected in this class' getdata.
 a2d_index_mask = self->Get( /A2D_INDEX_MASK )
 these_a2d = Where( a2d_index_mask, na2d )
 coincidence_flag =  self->Get( /COINCIDENCE_FLAG )
 rear_no_anti = Self->Get(/rear_no_anti)
 source = self->Get( /SOURCE )
 source->Set,energy_band=fltarr(2)
 n_channel_max = hessi_constant(/n_channel_max)
 ;Get all of the channel binning information needed. Put in in the
 ;channel_binning structure.  Needs to be done only once.  Gain and channels are
 ;fixed for a single spectrogram.
 ;
 channel_binning = Self->Chan_Prep()
 keep = gt_tagval( channel_binning, 'KEEP', missing=0)
 ;channel_binning groups and time_binning groups may both
 ;be non-uniform across a2d. Here we compute the number of
 ;time and energy bins for each a2d. Unselected a2d's will have zeroes.
 time_unit = self->Get( /TIME_UNIT )



 sp_dp_cutoff = Self->Get(/sp_dp_cutoff) ;if sp_dp_cutoff is gt 0 and  lt dp_cuff, then the dropoutlist will reprocess
 first_bunch = 1

 REPEAT BEGIN


     eventlist = $
         source->GetData( COINCIDENT_EVENT=-1, $ ;All events pass with mask
                          DONE=done, $
                          FIRST_BUNCH=first_bunch, $
                          NEXT_BUNCH=next_bunch, $
                          THIS_TIME_RANGE_EXTENSION=$
                          Self->Get(/extend_time_range), $ expand eventlist time range by two seconds
                                ;to accommodate possible dropout(datagap) extension
                          /stream_format )

                          livetimelist = source->GetData( /livetime )



                          if size(eventlist, /n_dim) eq 0 then begin
                              Self->Framework::SetData, -1 ;ras, 10-jun-02
                              return ;kim 12-apr-2002
                          endif

         ;This must be here as well for each bunch! RAS, 7/23/2001  ;ras - 7/23/2001
         ut_ref = Self->Get(/ut_ref, class='hsi_eventlist')


         IF N_Elements( ut_ref0 ) EQ 0 THEN BEGIN
             ; we need to wait until here to get ut ref, which is defined only
             ; after the eventlist has been read. That's why we have to have
             ; this in the loop, unfortunately.
             ; We have to define several arrays which we'll use to block
             ; the spectrogram by a2d
             ; ALL_CUMUL_A2D_SEL - The cumulative total energy&time bins for the selected a2d
             ; ALL_CUMUL_A2D     - The cumulative total energy&time bins for each of the a2d
             ; NCHAN_A2D_SEL     - The number of energy bins for the selected a2d
             ; NCHAN_A2D     - The number of energy bins for each the a2d
             ; BIN_A2D       - The number of time bins for each of the a2d

             ut_ref0 = self->Get( /UT_REF )   ;ras - 7/23/2001

;tbin_prep cannot be called here until the first ut_ref has been found
;This is a bad dependency that should be cleaned up sometime, ras, 8-oct-02
             time_binning = Self->Tbin_Prep( )
             ;Build the livetime_arr, samples, and deadacc_arr if needed
          Self->accbin, time_binning, livetime_arr, samples, deadacc_arr

             channel_regroup = channel_binning.regroup ; moved ras, 4-mar-2002
; First block to support energy spectrum, second clause to support sp_chan_binning
             if size(keep, /tname)  eq 'STRUCT' then begin


             nchan_a2d_sel = channel_binning.nchan_a2d ;Self->nchan_a2d(channel_binning)
             endif else nchan_a2d_sel = lonarr( na2d ) + n_elements( channel_binning.newcal )


             time_cumul_a2d = $
             keyword_set( gt_tagval( time_binning,'TIME_CUMUL_A2D',missing=0)) ? $
             time_binning.time_cumul_a2d : $
              [0, long(total(/cum, /double, (n_elements( time_binning.group)-1) * a2d_index_mask))]
             bin_a2d=time_cumul_a2d[1:*]-time_cumul_a2d
             ;The totals in time and energy bins for the selected a2d
             all_cumul_a2d_sel = [0,long(total(/cum, /double, bin_a2d[these_a2d] * nchan_a2d_sel))]
             ;The totals in time and energy bins for all the a2d
             NCHAN_A2D = long(a2d_index_mask)*0
             NCHAN_A2D[these_a2d] = nchan_a2d_sel
             all_cumul_a2d     =   [0,long(total(/cum, /double, bin_a2d * NCHAN_A2D))]

             nbin          = last_item( time_cumul_a2d )
             nspectro = last_item( all_cumul_a2d ) ;total chan & tbin for all detector, not
             ;summed over coincidence
             ;Get the attenuator state and save it

         ev = self->get(/obj, class='hsi_eventlist')
         simulated_data = ev->Get(/info, /simulated_data) >0 ;ras, 1-aug-03
         if obj_class( ev ) eq 'HSI_EVENTLIST' then begin
          sp_atten_state = simulated_data ? $
          {time:ut_ref0, state:1b} : ev->getdata(/atten_state)
          if size(/tname, sp_atten_state) eq  'STRUCT'  then begin
            sp_atten_state = Self->reform_struct( sp_atten_state)
            nstate = n_elements(sp_atten_state)
            ;Only keep atten_state values that persist longer than 1 sec.
            if nstate ge 2 then begin
             at_state = sp_atten_state.state
             at_state = [at_state[0], at_state]
             abs_time = Self->Get(/absolute_time)+ (Self->Get(/extend_time_range)*[-1,1])
             abs_time[1] = abs_time[1] > last_item(sp_atten_state.time)
             abs_time[0] = abs_time[0]< sp_atten_state[0].time
             at_time  = [abs_time[0], sp_atten_state.time, abs_time[1]]

             ;The first time and last time are assumed valid

             state_valid =  get_uniq([0,where(at_time[2:*] - at_time[1:*] gt 1.0)])
             nstate_valid = n_elements( state_valid)
             sp_atten_state = sp_atten_state[state_valid]

             endif

            endif

            Self->Set, SP_ATTEN_STATE= sp_atten_state
            endif

             ENDIF

         first_bunch = 0
         next_bunch = 0


         ;Build the deadtime in each timebin, eventlist.time must have full resolution

; dropout handling

         droplist = source->getdata( /dropout_list )

         IF Size( droplist, /type ) ne 8 then BEGIN
; we need to prevent this in case hsi_eventlist_file is used
             droplist = HSI_Dropoutlist( eventlist, ut_ref, saved_reset, obj=source, done=done )
         ENDIF
;8-jun-2007, ras, a drop in to help mark mcconnell's polarization analysis
;   normally fr_deadtime_window won't be gt 0
         if Self->Get(/fr_deadtime_window) gt 0 then $
            self->clear_rear_events, eventlist, ut_ref, droplist


         HSI_dropout_build, droplist, ut_ref, deadacc_arr, $
                        obj=self, sp_dp_cutoff=sp_dp_cutoff

         time_offset =  ut_ref-ut_ref0


;print, livetimelist.time[0:3]
         Self->MKlivetime, livetimelist, $
             time_binning, time_offset, livetime_arr, samples

         If Self->Get(/livetime_enable) then begin
             Self->Find_detector_off, livetimelist, $
                 last_time_front, done, detector_off_front, $
                 this_seg_index_mask=[bytarr(9)+1b, bytarr(9)]

             Self->Find_detector_off, livetimelist, $
                 last_time_rear, done, detector_off_rear, $
                 front=0, $
                 this_seg_index_mask=[bytarr(9), bytarr(9)+1b]
         endif
         ;;*************
        If Self->Get(/clear_halfscale) then $
        Self->Clear_halfscale, eventlist
        ;;*************

         test = size( eventlist,/tname ) eq 'STRUCT'
         if test then begin
         ;We probably should extract the ULD and CSA from the Self->histogram
         ;But for now, we'll harvest them here. This divides the
         ;eventlist into valid events and uld/csa events
           count = n_elements( eventlist.channel )
           ;count = n_elements( eventlist )
           ehist = histogram( count gt 1 ? $
           eventlist.channel : eventlist.channel+lonarr(1) , $ ;protect against one event, ras, 1-feb-02
              min=-8192, max=8191, bin=8192, reverse_indices=rev)

          eventlist.time = eventlist.time / time_unit

         ;RAS disable this unused and dangerous concatenation block 27-jun-2003
           if 0 then begin ;if ehist[0] ge 1 then begin
               n_uld_csa = Self->Ev_ref( eventlist,rev[rev[0]:rev[1]-1])
               ;eventlist[ rev[rev[0]:rev[1]-1]]
               n_uld_csa = hsi_ev2ev(n_uld_csa)
               uld_csa = exist( uld_csa ) ? $
               [uld_csa, n_uld_csa ] : n_uld_csa

               endif
           eventlist = ehist[1] ge 1 ? Self->Ev_ref( eventlist, rev[rev[1]:rev[2]-1] ) : 0 ;
           ;eventlist[ rev[rev[1]:rev[2]-1]    ] : 0
         count = ehist[1]
           endif


         IF count Ge 1   THEN BEGIN

             hsi_spectrogram = $
             Self -> histogram( $
             eventlist, $
             channel_binning, $
             time_binning, $
             time_offset, $
             time_cumul_a2d, $
             all_cumul_a2d, $
             nchan_a2d_sel, $
             coincidence =coincidence_flag, $
             input = hsi_spectrogram, $
             rear_no_anti = rear_no_anti, $
             binned_eventlist_flag= be_flag )

       ;
             ENDIF
         ;ENDFOR



     next_bunch = 1

     END UNTIL fcheck( done, 1)


 IF N_Elements( hsi_spectrogram ) EQ 0 THEN BEGIN
     self->Framework::SetData, -1
     ENDIF ELSE BEGIN
     hsi_spectrogram = reform(/over, hsi_spectrogram, nspectro, coincidence_flag +1)

     time_regroup = time_binning.regroup
;First block supports sp_chan_binning, second block supports
;sp_energy_binning

;Reformat for SP_CHAN_BINNING
    if  size(/tname,KEEP) ne 'STRUCT' then begin
        all_out = nchan_a2d * bin_a2d
        all_cum_out = long( [0, total( /cum,/double,all_out)])
        nchan_out=nchan_a2d[0] ;all should be the same, this is sp_chan_binning
        nspectro_out = last_item( all_cum_out )
        hsi_spectrogram = reform( /over, hsi_spectrogram, nchan_out, $
        last_item( time_cumul_a2d ), coincidence_flag + 1 )
        endif else begin

;Now reformat and rebin for SP_ENERGY_BINNING
        all_select = channel_binning.all_select
        all_frac   = channel_binning.all_frac
        nchan_out  = n_elements( all_select[*,0] )
        all_out = nchan_out * bin_a2d[these_a2d]
        all_cum_out = long([0,total(/cum, /double, all_out)])
        nspectro_out = last_item( all_cum_out )

        ;We have the spectrogram in energy, time, a2d bins, but not reformatted
        ;because there may be differences in the number of energy or time bins per a2d.
        ;The next step is REBIN. Here, energy bins are split on the basis of their weighting
        ;fraction computed in SPECTROGRAM::Group.  The default weighting fraction is just
        ;the linear factor determined from the channel energy boundaries and the final
        ;channel energy boundary which lies between the two native channel pha boundaries.
        ;After this step, there will be an identical number of energy channels for each
        ;selected a2d.  The new channels are loaded into the bottom (first channels)
        ;of the old array, and then extracted to avoid creating a new array.


        hsi_spectrogram = float( hsi_spectrogram )

        out = fltarr( last_item(all_cum_out)*  (coincidence_flag + 1) )

        for k=0, coincidence_flag do begin
            for i=0, na2d-1 do begin
                a2d = these_a2d[i]

                nbin0 = time_cumul_a2d[a2d] * (coincidence_flag + 1)
                nbin = bin_a2d[a2d] * (coincidence_flag + 1)

                temp = Self->REBIN( $
                all_select[*,i], all_frac[*,*,i], $
                reform( hsi_spectrogram[ all_cumul_a2d_sel[i]:all_cumul_a2d_sel[i+1]-1,k], $
                nchan_a2d_sel[i], bin_a2d[a2d] ))
                ;print,i,k,total(temp), all_cum_out[i]+nspectro_out*k
                ;if a2d eq 17 then stop
                out_temp = fltarr(nchan_out, bin_a2d[i])
                out_temp[0,0] = temp
                out[all_cum_out[i] + nspectro_out * k] = out_temp[*]

                endfor
            endfor
        hsi_spectrogram = reform( temporary( $
        out[0:(coincidence_flag+1)*nspectro_out-1]), nchan_out, $
        last_item( time_cumul_a2d ), coincidence_flag + 1 )
        endelse



;Get the Total counts in each channel. Sum over channels and hold
     ;in pha_total_vs_time, organize by a2d in Process_Post_Hook

;Regroup the time bins into their final groups

;At this point the data are in non-overlapping energy bins, the same
;bins for all a2d.  The time bins are arrayed in succession for each selected a2d,
;and finally comes the coincidence index.  There may be overlapping spectral bins in the
;final array, or overlapping time bins. We'll sum the base energy channels into the
;final energy channels, and save a similar operation for time bins until post_process
;if needed.
;
;On entry, hsi_spectrogram is binned ( nchan, nbin x na2d, ncoinc)
out = 0
hsi_spectrogram = Self->TIME_REGROUP( $
                  hsi_spectrogram,  time_regroup, time_cumul_a2d)

;Group the accumulated bins to the prescription in time_regroup
Pha_total_vs_time = Total( hsi_spectrogram,1 )

nbin2 = n_elements(hsi_spectrogram ) / (coincidence_flag + 1) /nchan_out

remove_first_chan = keyword_set( channel_binning.addzero )
;Put this kluge in because we have no answer for getaxis(/energy) for sp_channel_binning
If Self->Get(/sp_chan_binning) eq 0 then begin
    energy = Self->GetAxis(/energy)

;Are there enough channels to clip the last one?
    remove_last_chan = (((size(/dim, hsi_spectrogram))[0] -size(/dim, energy) ) ge 2 )[0]
    endif else remove_last_chan = 1
case 1 of
   keyword_set( channel_regroup ): begin

      nchan2 = n_elements( channel_regroup ) / 2

      out = fltarr(nchan2, nbin2, coincidence_flag + 1)
      ;If remove_first_chan is set, then add 1 to channel_regroup
      ;
      channel_regroup = channel_regroup + remove_first_chan
      one_chan_bin = channel_regroup[0,*]-channel_regroup[1,*]
      for i =0L,nchan2-1 do $
      if one_chan_bin[i] ne 0 then $
       out[i,*,*] = total( hsi_spectrogram[channel_regroup[0,i]:channel_regroup[1,i],*,*],1) $
       else $
       out[i,*] = hsi_spectrogram[channel_regroup[0,i],*,*]
      nchan_out = nchan2
      hsi_spectrogram = temporary( out )
     end
     (remove_first_chan or remove_last_chan): begin
      i0 = remove_first_chan
      i1 = nchan_out - 1 - remove_last_chan
      hsi_spectrogram =  hsi_spectrogram[i0:i1,*,*,*]
      nchan_out = nchan_out- remove_first_chan - remove_last_chan
      end
      endcase



     if Size(samples,/tname) eq 'POINTER' then begin
       valid = where( ptr_valid( samples ), nvalid )
       if nvalid gt 0 then for i=0,nvalid-1 do begin

        *livetime_arr[valid[i]]= f_div( *livetime_arr[valid[i]],$
        *samples[valid[i]])
        ;ras, 1-aug-03 - trap simulated_data
        if simulated_data then if total(*samples[valid[i]]) eq 0 then $
            *livetime_arr[valid[i]] = (*livetime_arr[valid[i]] + 1.0)<1.0
        endfor

       ptr_free, samples

       endif else begin
        livetime_arr = f_div( livetime_arr, samples)
        if simulated_data then if total(samples) eq 0 then $
            livetime_arr = (livetime_arr + 1.0 )<1.0
        endelse
     ;ras, 1-aug-03 - end trap for simulated data


     Self->Set, livetime_arr = livetime_arr
     detector_off = { front:fcheck(detector_off_front,0), $
                  rear:fcheck(detector_off_rear,0) }
     If Self->Get(/livetime_enable) then begin
       livetime_ctr = livetime_arr
       If size(/tname, livetime_ctr) eq 'POINTER' then begin
         ;livetime_ctr = ptrarr( size(/dim, livetime_arr)) fails pre 5.5
         ;Fixed 29-mar-2004, ras
         pdim = size(/dim, livetime_arr)
         livetime_ctr = ptrarr( pdim[0], pdim[1])
         pvalid = where( ptr_valid( livetime_arr), nvalid)
         for ivld=0,nvalid-1 do $
          livetime_ctr[pvalid[ivld]] = ptr_new(*livetime_arr[pvalid[ivld]])
         endif

        Self->Set, livetime_ctr = livetime_ctr ;note to self
        ;change livetime_regroup so I can use it on livetime_ctr and
        ;livetime_arr independently.  It's not critical now because
        ;livetime_ctr is a diagnostic only.
        Self->Livetime_merge_datagaps, detector_off, time_binning
        ;For non-contiguous bins, regroup livetime by averaging into final time bins
        ;Added by RAS, 3-oct-2003
        Self->livetime_regroup,  time_binning

        endif else $
        Self->Set, livetime_arr = 1.0, livetime_ctr=1.0

     ;Clear the datagaps, maybe.  keep them around for bin culling
     ;
     use_cull = Self->Get(/use_cull)
     If Not Keyword_set(Keep_dropout) and (not use_cull) then Self->Set, dropout=0.0

     Self->Process_Hook_Post, hsi_spectrogram, pha_total_vs_time, $
     nchan_out=nchan_out,  $
     dim_spectrogram=dim_spectrogram, $
     channel_binning = channel_binning, $
     time_binning=time_binning, $
     time_cumul_a2d = time_cumul_a2d, $
     ut_ref0=ut_ref0,$
     keep_dropout=keep_dropout  ;added 4-mar-2005




     ENDELSE


 END


;---------------------------------------------------------------------------

 ;+
 ; PROJECT:
 ;  HESSI
 ; NAME:
 ;  HSI_SPECTROGRAM::histogram
 ;
 ; PURPOSE:
 ;  This function returns a spectrogram from an eventlist
 ;  structure for a single a2d_index.
 ;
 ;

 ; CALLING SEQUENCE:
 ;  spectrogram = HSI_Spectrogram::histogram( $
 ;   eventlist, chan_binning, time_binning, time_offset, $
 ;   time_cumul_a2d, $
 ;   input = spectrogram, $
 ;   coincidence_value = coincidence_value, $
 ;   binned_eventlist_flag = binned_eventlist_flag)
 ; CALLS:
 ;  HSI_GET_E_EDGES, WHERE_ARR, INTERPOL8, EDGE_PRODUCTS, TAG_EXIST, FIND_IX, F_DIV
 ;  GT_TAGVAL, HSI_SCTIME_DIFF, HSI_ANY2SCTIME
 ;
 ; INPUTS:
 ;  This_a2d  - Creating spectrogram from eventlist for THIS_A2D. i.e. the detector/pha index.
 ;   Eventlist - eventlist structure
 ;
 ;  Binning - A structure describing the channel and time binning scheme.
 ;   A structure with these tags:

 ;   Time_range
 ;       Time_unit
 ;       Ut_ref - eventlist reference time
 ;       Ut_ref0- spectrogram reference time
 ;
 ; OPTIONAL KEYWORD, INPUTS:
 ;       NEW_GAIN - keyword for hsi_get_e_edges().
 ;   seed
 ;   poisson
 ;   notest
 ;   method
 ;   errmsg
 ;   DEFAULT_TIME_MAX
 ;   BINNED_EVENTLIST_FLAG - If set, spectrogram is for binned eventlist
 ;     final spectrogram assembled as pointer array.
 ;
 ; OUTPUTS:
 ;       Function returns a spectrogram with the prescribed binning.
 ;
 ; PROCEDURE:
 ;  Based on the HIST_2D function.  The original channels and
 ;  times are mapped into the indices of the spectrogram.  Then the
 ;  1d histogram is called to make the spectrogram.  For original channels
 ;  which overlap two new channels, and intermediate spectrogram is built.
 ;  The values in the overlapping spectrogram are pro-rata distributed into
 ;  their new channels either statistically as whole counts (needs work) or
 ;  as fractional counts.
 ;
 ; MODIFICATION HISTORY:
 ;  Version 1, richard.schwartz@gsfc.nasa.gov
 ;  18-dec-2000.
 ;  Version 1.1, First released, 2-feb-2000
 ;  Version 2.0, hessi release 5. RAS, 26-oct-2000
 ;  move functionality into subroutines.  this code strictly for manament ov
 ;  variable.
 ;  Version 3.0, allow energy bins. RAS, 3-jan-2001.
 ;  Release 5.1, added NEW_GAIN, RAS, 22-FEB-2001.
 ;   23-jul-2001. Added ut_ref0 and associated operations.
 ;  11-sep-2001, ras, major revision.  this routine only manages the re-identification of the
 ;  channel, time, and a2d with the new spectrogram indices, and builds a 1-d spectrogram
 ;  which can be mapped into a 3 d histogram or 2 d histogram pointers for the binned eventlist.
 ;  25-oct-2001 - Integrated into hsi_spectrogram__define.pro
 ;  23-feb-2002 - ras - use long64 for value_locate and use (le -1) for wout test
 ;  30-jan-2003 - ras - change condition for using longwords instead of L64 words
 ;   in value_locate to map times into bins.
 ;  15-feb-2005 - ras - changing long to floor function for building time index
 ;   for histograms.  long([-0.6, 0.6]) yields [0,0] while floor gives the correct [-1,0]
 ;	9-sep-2009, ras, added rear_no_anti test to prevent rears from being removed from
 ;	the spectrum output
 ;-

 Function HSI_Spectrogram::histogram, $
 eventlist, $
 chan_binning, $
 time_binning, $
 time_offset, $
 time_cumul_a2d, $
 all_cumul_a2d, $
 nchan_a2d_sel, $
 input = spectrogram, $
 coincidence_flag = coincidence_flag, $
 binned_eventlist_flag = binned_eventlist_flag,$
 rear_no_anti = rear_no_anti

 a2d_index_mask = Self->Get(/A2D_INDEX_MASK)
 these_a2d = Where( A2D_INDEX_MASK , na2d )
 ut_ref = Self->Get(/ut_ref)
 time_unit = Self->Get(/time_unit)

 nkeep = 0

 constant = hessi_constant()


 ;nselect = n_elements(eventlist)
 nselect = n_elements(eventlist.channel)


 not_these_a2d = where( a2d_index_mask eq 0b, not_na2d)
 n_channel_max = long ( constant.n_channel_max)

 nspectro = last_item(all_cumul_a2d) ;moved ras, 28-jan-2002

 ;Prevent empty eventlist crash
 if size(/tname, eventlist) eq 'STRUCT' then begin
     ;channel = eventlist.channel
     time_spec = eventlist.time  ;+ lonarr(nselect)

     a2d_index = eventlist.a2d_index
     coincid_mask = eventlist.coincid_mask
     if rear_no_anti then begin
     	rears = where(a2d_index ge 9, nrears)
     	if nrears ge 1 then coincid_mask[rears] = 0
     	endif


     ;
     ;If there are any channel gaps or overlaps, the final channel summations
     ;are defined in channel_final_regroup
     ;
     nspec         = n_elements(chan_binning.group) ; numbers of output spectral bins for 1 a2d
     ;
     ;If there are any time gaps or overlaps, the final time summations
     ;are defined in time_final_assign
     ;
     time_bin_busec_a2d    = gt_tagval( time_binning, 'time_bin_busec_a2d', missing=0)

     ;
     ;First block is to handle the binned eventlist.  These are time bins of
     ;fixed size, or grouping factor,  in time_unit, but may be different for each a2d.
     ;The grouping factor should be consistent for each collimator, i.e. the same for a2d mod 9.
     ;The grouping factor should be small enough to be consistent with the finest harmonic
     ;required, but this routine should not know anything about the harmonics.
     if keyword_set( time_bin_busec_a2d) then begin
         binned_eventlist_flag = 1

         bin_a2d=time_cumul_a2d[1:*]-time_cumul_a2d

         time_start = floor(/l64, (ut_ref-time_binning.abs_time_range[0] )*2.0^20/time_unit)
         ;change long() to Floor() to prevent double counting around 0
         time_spec[0] = floor(/l64,   (time_spec + time_start) / $
         (time_bin_busec_a2d[a2d_index]/time_unit)   )

         endif else begin

         ;This block handles the case of the spectrogram where the time binning is the same
         ;for each a2d, but this binning may be completely arbitrary, and is given by
         ;time_binning.group in time_unit.
;         time_reference = long64([time_binning.group[0]-10000L, time_binning.group] - $
;         (time_offset*2.d0^20/time_unit))
         time_reference = Floor(/L64, [time_binning.group[0]-10000L, time_binning.group] - $
         (time_offset*2.d0^20/time_unit))
         lim_time = minmax( time_reference )
         ;If the abs(time reference) is everywhere less than 2l^31-1, then we can use longs
         if (abs(lim_time[1])>abs(lim_time[0])) lt 2147483646L then $
            time_reference =long(time_reference)
         time_spec[0]   =  $
         value_locate( time_reference, time_spec) - 1
         binned_eventlist_flag = 0 ; not a binned eventlist, normal spectrogram.
         endelse

     ;Time_cumul_a2d is an array which keeps the total cumulative number of time bins as a function
     ;of the a2d_index.  This is needed for the final mapping between the tuples for
     ; (a2d_index, time, channel) and the spectrogram index.  The spectrogram index is used outside of
     ;this routine to make either a 3d histogram or an array of pointers (by det_index and harmonic) of
     ;1 or 2 d histograms for the binned eventlist.


     nbin          = last_item( time_cumul_a2d )




     out_of_range = 0 - ( last_item( time_cumul_a2d) + nspec )
     if not binned_eventlist_flag then begin
    nbgroup = n_elements(time_binning.group) -1
    outtest = value_locate( time_spec, [0, nbgroup-1])
    outsel =  outtest[0] gt -1 ? $
      [lindgen(outtest[0]+1), lindgen(nselect-outtest[1]+1)+outtest[1]] : $
      lindgen((nselect-outtest[1])>1)+outtest[1]

    wout = where( (time_spec[outsel] le -1 ) or $
    (time_spec[outsel] ge (n_elements(time_binning.group)-1)), nout)
    if nout ge 1 then wout = outsel[wout] < (nselect-1)


    endif else wout = where( (time_spec lt 0) or (time_spec ge bin_a2d[a2d_index]), nout)


     ;all unselected a2d should be out_of_range

     all_cumul_a2d_mod = all_cumul_a2d
     if not_na2d ge 1 then all_cumul_a2d_mod[not_these_a2d] = -4*nspectro

     ;Put this in to fix problem using only selected a2d
     nchan_a2d = lonarr(n_elements(a2d_index_mask))
     nchan_a2d[these_a2d] = nchan_a2d_sel

     ;nchan_cumul = long( [0,total(nchan_a2d_sel,/cum)])
     all_cumul_a2d_mod = all_cumul_a2d_mod - long( [0,total(nchan_a2d,/cum, /double)])

     total_chan_a2d = [0,long( total(/cum, /double, a2d_index_mask))]  * n_channel_max
     time_spec[0]   =  value_locate( chan_binning.all_chan_group[*], eventlist.channel + $
        total_chan_a2d[a2d_index] ) + time_spec * nchan_a2d_sel[a2d_index] + $
        all_cumul_a2d_mod[a2d_index]

     endif

     checkvar, spectrogram, lonarr( (coincidence_flag+1) * nspectro )

     wcoinc = where(coincid_mask, ncoinc)
    if ncoinc ge 1 then $
    time_spec[wcoinc] = time_spec[wcoinc] + coincid_mask[wcoinc] * nspectro

     ;time_spec[0] = time_spec + coincid_mask * nspectro


if nout ge 1 then time_spec[wout] = -1

 spectrogram[0] = $
 histogram( input = spectrogram, time_spec, bin=1, $
    min=0L,  max= n_elements(spectrogram)-1)

 return, spectrogram
 end


;---------------------------------------------------------------------------
Function HSI_SPECTROGRAM::Ev_Ref, ev, subarray
;Assumes 4 TAGS, TIME, A2D_INDEX, CHANNEL, COINCID_MASK
Return, { time:(ev.time)[subarray], $
         a2d_index:(ev.a2d_index)[subarray], $
         channel:(ev.channel)[subarray], $
         coincid_mask:(ev.coincid_mask)[subarray] }

end

 ;---------------------------------------------------------------------------
;Here we make the final organization of the spectrogram into channel, time, a2d, coinc
;The spectrogram enters with channels, including the lld and uld bins
;First we have to account for any regrouping of the time bins, some bins
;may be non-overlapping and can be formed from the overlapping set.

 PRO Hsi_Spectrogram::Process_Hook_post, $
 hsi_spectrogram, pha_total_vs_time,$
 nchan_out=nchan_out, $
 dim_spectrogram=dim_spectrogram, $
 time_cumul_a2d=time_cumul_a2d, $
 channel_binning = channel_binning, $
 time_binning=time_binning, $
 _extra=_extra



 ; the spectrogram get formed *only* for the a2ds, not for the dets or
 ; segs. The dets/segs are selected in this class' getdata.

 these_a2d = Where( self->Get( /A2D_INDEX_MASK ), na2d )
 coincidence_flag = self->Get( /COINCIDENCE_FLAG )
 nbin2 = n_elements(hsi_spectrogram ) / (coincidence_flag + 1) /nchan_out / na2d

 ;Get all of the channel binning information needed. Put in in the
 ;channel_binning structure.  Needs to be done only once.  Gain and channels are
 ;fixed for a single spectrogram.
 ;

 hsi_spectrogram = reform(/over, hsi_spectrogram, nchan_out, $
 nbin2, na2d, coincidence_flag + 1)
 total_count = reform( /over, $
 pha_total_vs_time, nbin2, na2d, coincidence_flag + 1)
 ;Remove bottom and top channel if addzero is set
 dim_spectrogram = lonarr(4)+1
 dim_spectrogram[0] = size(/dim, hsi_spectrogram)


 self->Framework::SetData, hsi_spectrogram


 self->Framework::Set, DIM_SPECTROGRAM = dim_spectrogram, $
 total_count = total_count

 END
 ;---------------------------------------------------------------------------



;---------------------------------------------------------------------------

PRO Hsi_Spectrogram::Write, _EXTRA = _extra

flux = self->GetData( _EXTRA=_extra )

size_flux = Size( flux )
other_flux = Fltarr( size_flux[1], size_flux[3], size_flux[2] )

FOR i=0, size_flux[2]-1 DO BEGIN
    other_flux[ *, *, i ] = flux[*, i, *]
ENDFOR

time_range =  self->Get( /TIME_RANGE )
time_duration = time_range[1] - time_range[0]

date_obs = self->Get( /UT_REF )

new_time_cal = self->Get( /NEW_TIME_CAL )
channel_cal_new = self->Get( /CHANNEL_CAL_NEW )

Spectra2FITS, new_time_cal, other_flux, date_obs, EDGES=channel_cal_new

END

;---------------------------------------------------------------------------
 ;+
 ; PROJECT:
 ;  HESSI
 ; NAME:
 ;  HSI_SPECTROGRAM::group_MULTI
 ;
 ; PURPOSE:
 ;  This function returns the channel IDs needed to build the 3D spectrogram.
 ;
 ; CATEGORY:
 ;  HESSI, UTIL, SPECTRA
 ;
 ; CALLING SEQUENCE:
 ;; Self->group_Multi, binning, these_a2d, offset_gain, channel_binning, $
 ;  addzero=addzero;

 ;
 ; CALLS:
 ;  Self->group
 ;
 ; INPUTS:
 ;
 ;
 ;  Binning - A structure describing the channel and time binning scheme.
 ;  A structure with these tags:
 ;  THESE_A2D_INDEX - select events with these a2d_indices.
 ;   The binning scheme is described by 1 of 3 sets of tags.
 ;
 ;  (1)
 ;  CHANNEL_CAL_NEW- A 1-d or 2-d array of edges in keV.
 ;  with the optional tag:
 ;    CHANNEL_CAL- A calibration vector of the channel edges in keV.
 ;    If CHANNEL_CAL  is not provided, hsi_get_e_edges is used.
 ;  or
 ;  (2)
 ;  CHANNEL_BIN  - A scalar grouping value.
 ;  with the  optional tags:
 ;    CHANNEL_MIN  - minimum channel value to use, default is 0.
 ;    CHANNEL_MAX  - maximum channel value to use, default is max found.
 ;  or
 ;  (3)
 ;  CHANNEL_GROUP  - A 1-d or 2-d array of channel bin edges.
 ;
 ;  Channel_cal - a vector giving the current calibration.
 ;  Channel_cal has two forms
 ;  1. A 1 or 2-d vector giving the explicit edges for each channel.
 ;  2. A 2 element array, usually used for time bins.  First element is
 ;  the reference time for CHANNEL_CAL_NEW at the time when the CHANNEL_CAL is 0.

 ; OPTIONAL KEYWORD, INPUTS:
 ;
 ; OUTPUTS:
 ;     Function returns a spectrogram with the prescribed binning.
 ;
 ; OPTIONAL OUTPUTS:
 ;  none
 ;
 ; KEYWORDS INPUTS:
 ;  ADDZERO - if set, add 0 to each group row.  Facilitates binning all events from eventlist.
 ;    Unneeded if accounted for in calling routine.
 ;  NEW_B INNING - binning structure after modifications. Reports vectors needed in
 ;  histogram
 ;
 ; COMMON BLOCKS:
 ;  none
 ;
 ; SIDE EFFECTS:
 ;  none
 ;
 ; RESTRICTIONS:
 ;  none
 ;
 ; PROCEDURE:
 ;
 ; MODIFICATION HISTORY:
 ;  Release 5.1, ras 5-sep-2001
 ;  24-oct-2001, ras, resolve binning.channel_cal_new to 1d channel_cal_new
 ;  25-oct-2001, ras, integrated into hsi_spectrogram__define.pro object file.
 ;	07-oct-2014, ras, modified warning message about positive gain offsets
 ;-

 pro  hsi_spectrogram::group_multi,  binning, these_a2d, offset_gain, $
 channel_binning, addzero=addzero

 checkvar, addzero, 0

 cal_is_scale = n_elements( offset_gain ) eq 54
 If cal_is_scale then begin
 	z = where( offset_gain[*,0] gt 0, nz)
 	if nz ge 1  then begin
 		message, /info, '!!!!!!!! Warning !!!!!!!!!!!! '
 		message, /info, '!!!!!!!! The gain offset for detector segment(s) '+arr2str(string(z+1),',') + ' !!!!!!!!!!!!!!!!!!!'
 		message, /info, '!!!!!!!! is(are) positive for this time. Normally, this value is negative and so !!!!!!!!!!!!!!!!!!!' ;
 		message, /info, '!!!!!!!! there may be an error in the table as this would put positive energies at channels lt 0. !!!!!!!!!'
 		message, /info, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 		endif
 		endif
 checkvar, these_a2d, 0

 n_channel_max = long( hessi_constant(/n_channel_max) )

 na2d = n_elements( these_a2d )



 Case 1 of

   tag_exist(binning, 'CHANNEL_CAL_NEW' ): begin


     a2d = these_a2d[0]
     edge_products, binning.channel_cal_new, edges_1=channel_cal_new
     channel_cal_new = addzero ? $
     [-1000.0, channel_cal_new] : channel_cal_new
     this_binning0 = {channel_cal_new: channel_cal_new, channel_cal:offset_gain[a2d,*]}
     this_binning0 = Self->group( this_binning0, a2d, /cal_is_scale)
     group = this_binning0.group ;will need to add the lld channel

     ;Just in case the range goes to very high energy, we need to repeat this for the first
     ;high range a2d
     a2dhi = 18
     this_binning_hi = {channel_cal_new: channel_cal_new, channel_cal:offset_gain[a2dhi,*]}
     this_binning_hi = Self->group( this_binning_hi, a2dhi, /cal_is_scale)

     ngroup = n_elements( group ) > n_elements( this_binning_hi.group )
     nselect = n_elements( this_binning0.newcal )
     ;nselect = n_elements( this_binning0.keep.select) > n_elements( this_binning_hi.keep.select)
     ;nselect = (nselect + 2) * 2 ;overkill to be safe.  really should use pointers and an object

     all_group = lonarr( ngroup * na2d * 2) ;factor of two is overkill to be safe
     ;fill all_group and then truncate, instead of concatenate
     igroup = n_elements( group ) ;starting index

     all_frac = tag_exist( this_binning0.keep, 'FRAC' ) ? $
     reproduce( fltarr(nselect, 3), na2d) : 0
     all_select = tag_exist( this_binning0.keep,'SELECT')? $
     reproduce(fltarr(nselect), na2d) : 0
     all_group[0] = group
     nchan_a2d    = lonarr( na2d )
     nchan_a2d[0] = this_binning0.keep.nchan

     if na2d ge 1 then $
     for i=0,na2d-1 do begin

         if i eq 0 then this_binning = this_binning0 else begin
            a2d = these_a2d[i]
            this_binning = {channel_cal_new: channel_cal_new, channel_cal:offset_gain[a2d,*]}

            this_binning = Self->group( this_binning, a2d, /cal_is_scale)
            endelse

         ;will need to add the lld channel
         if keyword_set(all_frac) then all_frac[0,0,i]= this_binning.keep.frac
         if keyword_set(all_select) then all_select[0,i]= this_binning.keep.select
        if Keyword_set(nchan_a2d) then nchan_a2d[i]=this_binning.keep.nchan
         if i ge 1 then begin
            all_group[igroup] = this_binning.group + i * n_channel_max
         ;Special values for energy channels that are beyond the range of
         ;this A2D
         ;

            igroup = n_elements( this_binning.group ) + igroup
            endif
         endfor
     channel_binning = this_binning0
     all_group = all_group[0:igroup-1]
     end

   ;For  CHANNEL_BIN  or CHANNEL_GROUP cases
   else: begin
     if tag_exist( binning, 'CHANNEL_BIN') then $
     this_binning = add_tag( binning, [0.0, 1.0], 'channel_cal')

     this_binning = Self->group( this_binning, 0, /cal_is_scale)
     group = addzero ? [0L, this_binning.group] : this_binning.group*1L
     ngroup = n_elements( group )
     all_group = ( reproduce( group, na2d) + $
     rebin(lindgen(1,na2d)*n_channel_max, ngroup,na2d))[*]
     ;CHANGE NEWCAL if ADDZERO is set, this is to be consistent with the
     ;CHANNEL_CAL_NEW case used with SP_ENERGY_BINNING.  In hsi_spectrogram::chan_prep
     ;channel_binning.NEWCAL is used to determine the spectral bins for all methods.
     ;
     channel_binning = addzero ? $
     rep_tag_value(this_binning, [0,this_binning.newcal], 'NEWCAL') : this_binning

     end


   endcase

 channel_binning = add_tag( channel_binning, fcheck( all_group, 0), 'all_chan_group')

 if keyword_set(all_frac) then $
 channel_binning = add_tag( channel_binning, $
 all_frac, 'all_frac')
 if keyword_set(all_select) then $
 channel_binning = add_tag( channel_binning, $
 all_select, 'all_select')
 if keyword_set(nchan_a2d) then $
 channel_binning = add_tag( channel_binning, $
 nchan_a2d, 'nchan_a2d')

 end

 ;---------------------------------------------------------------------------

 ;+
 ; PROJECT:
 ;  HESSI
 ; NAME:
 ;  HSI_SPECTROGRAM::GET_TIMEBIN
 ;
 ; PURPOSE:
 ;  This function returns a structure with the binning information
 ;  needed to build a spectrum with arbitrary time edges.
 ;
 ;
 ; CATEGORY:
 ;  HESSI, UTIL, SPECTRA
 ;
 ; CALLING SEQUENCE:
 ;  result_str = hsi_spectrogram::get_timebin( binning, time_range,
 ;   ut_ref, time_unit, tbinning=tbinning)
 ;
 ; CALLS:
 ;  HSI_SPECTROGRAM::group
 ;
 ; INPUTS:
 ;
 ;
 ;  Binning - A structure describing the channel and time binning scheme.
 ;  - The time binning scheme is described by 1 of 3 sets of tags.
 ;  (1)
 ;  NEW_TIME_CAL- A 1-d or 2-d array of edges in sec or an anytim
 ;  or hessi time structure.
 ;  The hsi_eventlist object provides the event timing in seconds.
 ;
 ;  or
 ;  (2)
 ;  TIME_BIN  - A scalar grouping value.
 ;  with optional tags:
 ;  TIME_MIN  - minimum time to use in units of hsi_eventlist.
 ;  TIME_MAX  - maximum time to use in units of hsi_eventlist.
 ;  or
 ;  (3)
 ;  TIME_GROUP  - A 1-d or 2-d array of time bin edges in sec.
 ;   The binning scheme is described by 1 of 3 sets of tags.

 ;  Time_range - Time_range gives the min and max times requested relative to Ut_ref.
 ;  Units are seconds.
 ;  Ut_ref   - Reference time for Time_range. Must be in format which is Anytim
 ;  readable.
 ;  Time_unit  - time_unit of eventlist, number of b-microseconds
 ;  Tbinning   - derived binning passed to Self->group.  Passes back
 ;  FINAL_ASSIGN tag used with overlapping or missing times.
 ; OPTIONAL KEYWORD, INPUTS:
 ;
 ; OUTPUTS:
 ;     Function returns a spectrogram with the prescribed binning.
 ;
 ; KEYWORDS INPUTS:
 ;  NOPARTIAL - if set, group ids are set to closest value.  There is
 ;  no tracking of partial bins.
 ;
 ;
 ; MODIFICATION HISTORY:
 ;  Version 1, richard.schwartz@gsfc.nasa.gov
 ;  20-oct-2000, based on elements of early versions of histogram
 ;  25-oct-2001, integrated with hsi_spectrogram__define.pro
 ;-

function hsi_spectrogram::get_timebin, binning, time_range, ut_ref, time_unit, tbinning

@hsi_insert_catch

 ;time_unit = eventlist->get(/time_unit)
 ;ut_ref  = eventlist->get(/ut_ref)
 ; acs took that off they are passed by the caller
 ;obs_time = Self->Get(/obs_time_interval)
 ;self->Set, time_range = minmax( time_binnning.newcal ) - obs_time[0]
case 1 of
    tag_exist(binning, 'new_time_cal'): begin
        tbinning = {channel_cal_new: anytim(binning.new_time_cal) , $
                    channel_cal:[ anytim( ut_ref), 2d^20/time_unit]}
        group = Self->group( tbinning, /nopartial )
    end

    tag_exist(binning, 'time_bin'): begin

        time_range1=tag_exist( binning, 'ut_ref0')? $ ;ras - 7/23/2001
            time_range-binning.ut_ref0:time_range ;ras - 7/23/2001
        tbinning = {channel_bin: binning.time_bin, $
                    channel_max:time_range1[1]*2d^20/time_unit, $
                    channel_min:time_range1[0]*2d^20/time_unit, $
                    channel_cal:[anytim( ut_ref), 2d^20/time_unit]}
        group = Self->group( tbinning, /cal_is_scale)

     end
   tag_exist(binning,'time_group'): begin
     tbinning = {channel_group:binning.time_group}
     group = Self->group( tbinning, n_channel_max=last_item(binning.time_group) )

     end
   endcase


 return,exist(group)? group : 0
 end

 ;---------------------------------------------------------------------------

;------------------------------------------------------------------------------------------
;+
 ;For a channel_val bin with boundaries X1 and X2, and a
 ;channel_cal_new bin with boundaries Y1 and Y2 where Y1 is ge X1
 ; frac = (((X2<Y2) - Y1) >0.0) / (X2-X1)
 ;
 ;-
function hsi_spectrogram::bin_frac, X1, X2,  Y1


Y2 = Y1[1:*]
N  = n_elements( X2 )

denom = X2-X1
w0 = where( Y1 ge X1, n0)
frac = denom * 0.0

if n0 ge 1 then frac[w0] = ((X2[w0]<Y2[w0]) - Y1[w0]) >0.0

w1 = where( Y1[0:N-1] lt X1[0:N-1], n1)

if n1 ge 1 then frac[w1] = ( (X2[w1]<Y2[w1]) - X1[w1]) > 0.0

frac = f_div( frac, denom )
return, frac
end
;------------------------------------------------------------------------------------------

 ;+
 ; PROJECT:
 ;  HESSI
 ; NAME:
 ;  Self->group
 ;
 ; PURPOSE:
 ;  This function returns a structure with the binning information
 ;  needed to build a spectrum with arbitrary energy edges.
 ;
 ;
 ; CATEGORY:
 ;  HESSI, UTIL, SPECTRA
 ;
 ; CALLING SEQUENCE:
 ;  result_str = Self->group( binning_str, channel_cal)
 ;
 ; CALLS:
 ;  EDGE2BIN, GT_TAGVAL, HSI_GET_E_EDGE, HESSI_CONSTANT,CHECKVAR,TAG_EXIST,EDGE_PRODUCTS
 ;  UNIQ,F_DIV, MINMAX
 ;
 ; INPUTS:
 ;
 ;
 ;  Binning - A structure describing the channel and time binning scheme.
 ;  A structure with these tags:
 ;  THIS_A2D_INDEX - select events with this a2d_index.
 ;   The binning scheme is described by 1 of 3 sets of tags.
 ;
 ;  (1)
 ;  CHANNEL_CAL_NEW- A 1-d or 2-d array of edges in keV.
 ;  with the optional tag:
 ;    CHANNEL_CAL- A calibration vector of the channel edges in keV.
 ;    If CHANNEL_CAL  is not provided, hsi_get_e_edges is used.
 ;  or
 ;  (2)
 ;  CHANNEL_BIN  - A scalar grouping value.
 ;  with the  optional tags:
 ;    CHANNEL_MIN  - minimum channel value to use, default is 0.
 ;    CHANNEL_MAX  - maximum channel value to use, default is max found.
 ;  or
 ;  (3)
 ;  CHANNEL_GROUP  - A 1-d or 2-d array of channel bin edges.
 ;
 ;  Channel_cal - a vector giving the current calibration.
 ;  Channel_cal has two forms
 ;  1. A 1 or 2-d vector giving the explicit edges for each channel.
 ;  2. A 2 element array, usually used for time bins.  First element is
 ;  the reference time for CHANNEL_CAL_NEW at the time when the CHANNEL_CAL is 0.

 ; OPTIONAL KEYWORD, INPUTS:
 ;
 ; OUTPUTS:
 ;     Function returns a spectragram with the prescribed binning.
 ;
 ; OPTIONAL OUTPUTS:
 ;  none
 ;
 ; KEYWORDS INPUTS:
 ;  NOPARTIAL - if set, group ids are set to closest value.  There is
 ;  no tracking of partial bins.
 ;  NEW_GAIN -passed onto hsi_get_e_edges
 ;  gain_time_wanted -passed onto hsi_get_e_edges
 ; COMMON BLOCKS:
 ;  none
 ;
 ; SIDE EFFECTS:
 ;  none
 ;
 ; RESTRICTIONS:
 ;  none
 ;
 ; PROCEDURE:
 ;
 ; MODIFICATION HISTORY:
 ;  Release 5.1, ras 23-feb-2001, added new_gain
 ;  Version 1, richard.schwartz@gsfc.nasa.gov
 ;  20-oct-2000, based on elements of early versions of histogram
 ;  24-oct-2001, use value_locate to obtain Ical, may not even be used. ras.
 ;  25-oct-2001, integrated with hsi_spectrogram__define.pro
 ;  4-oct-2002, ras, completely revised method for assigning fractions for
 ;   partial bins.
 ;-

 function HSI_SPECTROGRAM::group,  binning, this_a2d, $

 nopartial=nopartial, n_channel_max=n_channel_max, cal_is_scale=cal_is_scale

 Self->chan_overlap_fix, binning

 cal_is_scale = keyword_set(cal_is_scale)
 checkvar, this_a2d, 0

 n_channel_max =hessi_constant(/n_channel_max)
 channel_cal = gt_tagval( binning,'CHANNEL_CAL',found=found_channel_cal)
 if not found_channel_cal then begin
    gain_time_wanted = Self->Get(/gain_time_wanted)
    gain_time_wanted = keyword_set( gain_time_wanted )? gain_time_wanted : $
      (Self->Get(/absolute_time_range))[0]
    channel_cal = float( call_function('hsi_get_e_edges',a2d_index=this_a2d,$
    high_scale=(this_a2d gt 17), gain_generation =Self->Get(/gain_generation),$
    gain_time_wanted=gain_time_wanted ))
    endif
 keep_str = 0
 nopartial = keyword_set(nopartial)

 case 1 of
   tag_exist(binning,'CHANNEL_CAL_NEW'): begin

     channel_cal_new = binning.channel_cal_new

     edge_products, channel_cal_new, edges_1=channel_cal_new
     edge_products, channel_cal, edges_1=channel_cal
     case 1 of
         nopartial: begin

           ;Used for setting time bins
           ;channel_cal[0] is the time for channel_cal_new at ut_ref and
           ;channel_cal[1] is the bins/second if
           ;the unit of channel_cal_new is in seconds. If Channel_cal_new
           ;is in seconds from 1-jan-1979, then
           ;channel_cal[0] is ut_ref for the eventlist.  If it is from
           ;the start of the day, then it
           ;is given by anytim(ut_ref,/time), i.e. ut_ref in seconds from
           ;the start of the day.
           ;
           channel_group = (channel_cal_new - channel_cal[0]) *  channel_cal[1]
          ;Change to long64 to support longer time accumulations, ras, 21-jun-02
           channel_group = long64( channel_group+0.5 )
           mmax_channel = minmax( channel_group )

           end
         else: begin

           if not cal_is_scale then $
           channel_cal_new = (channel_cal_new > channel_cal[0]) < last_item(channel_cal) ;*
           nchan = n_elements( channel_cal_new ) -1



           edge2bin, channel_cal_new, channel_cal, chan_group1, cal_is_scale = cal_is_scale

;;;;;;;;;;;;;;;;;;;; ras 12-aug-02
; Changes made to fix binning fractions assigned to last channels
; and to prevent crashes from channel_cal[0] gt 0. (which shouldn't occur).

           chan_group2 = chan_group1+1 < last_item(chan_group1)
           channel_group = [([ transpose(chan_group1), transpose((chan_group2))])[*], $
            last_item( chan_group1 )-1] >0 < n_channel_max
           ;For the case of high energy ranges, let the maximum channel be what it is and fix the result
           ;later, ras, 23-oct-2001
           ;> 0 < (( max(channel_cal_new) lt 2000.) ? n_channel_max : 1000000L)
           ;> 0 <  n_channel_max
           channel_group = channel_group[uniq( channel_group, sort( channel_group ))]


           ;Index of upper bound of straddle interval
           ;keep_select = lindgen(nchannel_group/2)*2+1
           ;index of upper bound of straddle interval


           channel_val =  float( channel_cal[0]+channel_cal[1]*channel_group )

           ;;;;;;;; ras - 4-oct-02 ;;;;;;;;
       nchannel_group = n_elements(channel_group)
 ;Get channel_val indices before each new channel low bound
       If n_elements(channel_val) eq 1 then channel_val = fltarr(2)
       select = (value_locate( channel_val, channel_cal_new) ) >0
       ;The bin in channel_val just below a specific channel_cal_new
       ;is identified by SELECT.  Each of the new bins in channel_cal_new
       ;derive their counts from up to three bins in channel_val.  The
       ;fraction assigned is given by FRACLO for SELECT
       ;FRAC for (SELECT+1), and FRACHI for (SELECT+2)
       ;This has to work with SPECTROGRAM::REBIN()

       fraclo = self->bin_frac( channel_val[select], $
              channel_val[select+1],channel_cal_new>0)
       frac   = self->bin_frac( channel_val[select+1], $
              channel_val[select+2],channel_cal_new>0)
       frachi = self->bin_frac( channel_val[select+2], $
           channel_val[select+3],channel_cal_new>0)

;;;;;;;;;;;;;;;;;;;; ras 12-aug-02
;Remove any channels ge 8192
          out_chan = where( channel_group lt n_channel_max, nsave)
          channel_group_old = channel_group
          channel_group = channel_group[out_chan]
          keep_these = where( select le out_chan[ nsave-1], nchan )
          nchan = n_elements(channel_group)
           keep_str = {select: select[keep_these], $
             frac:[[fraclo[keep_these]],[frac[keep_these]],[frachi[keep_these]]], nchan:nchan}

           ;
           ; The channel_group is a vector of ordered indices into the old channels which
           ; give the intermediate channel boundaries.  Some of the channels straddle the new channel
           ; edges, and others are wholly contained within the new channels.  There is no more than
           ; a single grouping e.g. [5,9], wholly contained within a pair of new channel edges.

           mmax_channel = minmax( channel_group )



           end
         endcase

     end

   tag_exist(binning,'channel_bin'): begin

     channel_bin = binning.channel_bin
     channel_max = gt_tagval(binning,'CHANNEL_MAX', default=n_channel_max)

     channel_min = gt_tagval(binning,'CHANNEL_MIN',default=0)

     ;ras, 17-may-02 change long(channel_bin) to long64(channel_bin)
     channel_bin = long64(channel_bin) > 1 < (channel_max-channel_min)
     ;channel_bin = long(channel_bin) > 1 < (channel_max-channel_min)
     num_new_channel = 1 + (channel_max-channel_min) / channel_bin
     channel_group  = channel_bin*lindgen(num_new_channel) + $
         channel_min
     if cal_is_scale then channel_cal_new = channel_group/channel_cal[1]+channel_cal[0] else $
     channel_cal_new = channel_cal[channel_group]

     mmax_channel = minmax( channel_group )

     end

   tag_exist(binning,'channel_group'): begin

     channel_group = binning.channel_group
     edge_products, binning.channel_group, edges_1=channel_group
     mmax_channel = minmax( channel_group )
     channel_cal_new   = channel_group
     end




   endcase



 if n_elements(mmax) eq 0 then mmax = minmax( channel_group )

 return, {  group: channel_group, $
 oldcal: (cal_is_scale? poly(channel_group, channel_cal) : channel_cal[channel_group]), $
 newcal: channel_cal_new, $
 mmax_channel:mmax_channel, keep:keep_str }


 end

;------------------------------------------------------------------------------------------


 ;+
 ; NAME:
 ;  HSI_SPECTROGRAM::rebin
 ;
 ; PURPOSE:
 ;  This procedure moves fractions of the counts boundary channels into the remaining channels.
 ;
 ;
 ;
 ; CALLING SEQUENCE:
 ;  spectrogram_out = hsi_spectrogram::rebin( select, frac, spectrogram )
 ;
 ; OUTPUTS:
 ;     Spectrogram is returned with fewer channels and counts rebinned.
 ;
 ;-

 function hsi_spectrogram::rebin,  select, frac,  spectrogram

 select  = select
 frclo = frac[*,0]
 frc   = frac[*,1]
 frchi = frac[*,2]
 dim   = long([1,1])
 dim[0] = size(/dim, spectrogram)
 nbin  = dim[1]
 msel  = max( select, imax)
 select = select[0:imax]
 nselect = n_elements( select )
 out =rebin(frclo[0:nselect-1],nselect,nbin)*spectrogram[select  ,* ] + $
      rebin(  frc[0:nselect-1],nselect,nbin)*spectrogram[select+1,* ] + $
      rebin(frchi[0:nselect-1],nselect,nbin)*spectrogram[select+2,* ]

 ;For the special case where the energy_band requested is below the
 ;bottom channel, move the total counts in spectrogram, to the first
 ;index of the out array. In this case, frac will all be zero, ras, 8-aug-02
 If total( abs(frac) ) eq 0 then begin
    out = out * 0.0
    out[0,*] = spectrogram
    endif
 return, out
 end

;------------------------------------------------------------------------------------------



 ;---------------------------------------------------------------------------
 ;+

 ; NAME:
 ;  HSI_SPECTROGRAM::channel_regroup
 ;
 ; PURPOSE:
 ;  This function returns the final summation over fine
 ;  energy/time bins.
 ;

 ; CALLING SEQUENCE:
 ;  final_spectrogram = HSI_SPECTROGRAM::regroup( $
 ;  spectrogram, channel_info, $
 ;  channel_regroup, time_regroup )

 ; INPUTS:
 ;     Spectrogram - spectrogram with fine cells.
 ;  Channel_info - structure with info keywords. Gives NEWCAL tag
 ;  gives number of energy boundaries used with input spectrogram.
 ;  Channel_regroup - element [i,j] gives channels to sum for
 ;  final spectrogram.  First index, I, gives start and end channel indices
 ;  for final channel J.
 ;  Time_regroup - analog of Channel_regroup for time.
 ;

 ;
 ;
 ; PROCEDURE:
 ;  A fine cell(bin/channel) spectrogram is summed into the grouped
 ;  cells given by CHANNEL_FINAL_ASSIGN and TIME_FINAL_ASSIGN.
 ;
 ; MODIFICATION HISTORY:
 ;  Version 1, richard.schwartz@gsfc.nasa.gov
 ;  22-jan-2000.
 ;  17-apr-2001. Protect against total over array without the requested dimension.
 ;
 ;-



 function HSI_SPECTROGRAM::channel_regroup, hsi_spectrogram,  $
 channel_regroup,  time_regroup, nchan_out, time_cumul_a2d

 coincidence_flag = Self->Get(/coicidence_flag)
 these_a2d = where( Self->Get(/a2d_index_mask), na2d)

 if keyword_set( channel_regroup ) then begin
  nchan2 = n_elements( channel_regroup ) / 2
  nchan  = nchan_out
  out = fltarr(nchan2, last_item( time_cumul_a2d ), coincidence_flag + 1)
  one_chan_bin = channel_regroup[0,*]-channel_regroup[1,*]
  for i =0L,nchan2-1 do out[i,*,*] = (one_chan_bin[i] ne 0) ? $
       total( hsi_spectrogram[channel_regroup[0,i]:channel_regroup[1,i],*,*],1) : $
       hsi_spectrogram[channel_regroup[0,i],*,*]
  hsi_spectrogram = temporary( out )
  endif

 ;There could be time overlap, so look at time_regroup to get final time bins
 if keyword_set( time_regroup ) then begin
   nbin2 = n_elements( time_regroup )/2 * na2d
   out = fltarr( (size(/dim, hsi_spectrogram))[0], nbin2, coincidence_flag+1 )
   one_time_bin = time_regroup[0,*]   -time_regroup[1,*]
   for i=0L,nbin2-1 do out[*,i,*] = (one_time_bin[i] ne 0 ) ? $
   total( hsi_spectrogram[*, time_regroup[0,i]:time_regroup[1,i],*],2) : $
    hsi_spectrogram[*, time_regroup[0,i],*]

   hsi_spectrogram = temporary( out )
   endif

 return, hsi_spectrogram
 end
;---------------------------------------------------------------------------
;+

; NAME:
; HSI_SPECTROGRAM::CHAN_OVERLAP_FIX
;
; PURPOSE:
; This function returns the bookkeeping structure needed to
; account for overlapping channels or timebins.
;
; CALLING SEQUENCE:
; Self->chan_overlap_fix, binning_str
;
;-

pro HSI_SPECTROGRAM::chan_overlap_fix,  binning



binning_tag = ['CHANNEL_CAL_NEW','CHANNEL_GROUP']
in_tag      = tag_names( binning )
this_tag    = where_arr( binning_tag, in_tag, ntag)

if ntag eq 0 then return

binning_tag = binning_tag[this_tag]
cal         = reform( gt_tagval( binning, binning_tag ))

if n_elements(cal) le 2 then return

case size(/n_dim, cal) of

  1: wtest = where(cal[1:*] - cal[*] lt 0.0, test)
  2: wtest = where(cal[0,1:*] ne cal[1,*], test)
  else:
  endcase

if (test<1) then begin

  cal_2    = cal( uniq(cal, sort(cal)))


  final_cal=value_locate( cal_2, cal) ;describes mapping into final channels.
  final_cal[1,*]= final_cal[1,*]-1
  binning = add_tag(rem_tag(add_tag( binning, final_cal, 'REGROUP'), $
  binning_tag), cal_2, binning_tag)


  endif


end
;---------------------------------------------------------------------------
;+
;Name: HSI_SPECTROGRAM_DECIM_CORRECT
;   Correct spectrum for decimation
;   Works best on narrow energy bins.
;   Input for binned eventlist is binned eventlist, not ltime.
;   livetime in binned eventlist is already expanded.
;History
;   richard.schwartz@gsfc.nasa.gov, 17-jun-2003
;   16-sep-2003, ras, fixed rebin call to old 5.4 style
;-
pro hsi_spectrogram::decim_correct, ltime, dim_spectrum



;Get the times, decimation states, atten states for all intervals
Self->Set, decim_table = $
'Either no decimation needed or decimation correction is disabled'
decimation_correct = Self->Get(/decimation_correct)
rear_decimation_correct = Self->Get(/rear_decimation_correct)
any_decim_correct = decimation_correct or rear_decimation_correct
Self->Set,decim_warning=''
;Do we want to try to or can we correct for decimation?
If any_decim_correct eq 0 or  getenv('HSI_USE_SIM') eq 'true' then return
decim_time = Self->decim_time_range(ch_atten, exclude_atten)

rear_decim_time = $
    Self->decim_time_range(rear_decim_correct=rear_decimation_correct)

 If size(/tname, decim_time) eq 'INT' then if decim_time EQ -1 then begin
    message,/info, 'Observing summary not found. Cannot compute decimation without IDPU version'
    return ;Need a self-contained method for extracting IDPU version from packet headers.
    end

 Is_Front_Decimated = size(/tname,decim_time) eq 'STRUCT' ? $
    byte( total(abs(decim_time.channel*(decim_time.weight-1))) < 1) : 0
 Is_Rear_Decimated = size(/tname, rear_decim_time) eq 'STRUCT'
 If (Is_Front_Decimated or Is_Rear_Decimated) and any_decim_correct then begin

     corr_table = Self->Decim_table(decim_time, rear_decim_time,$
      rout=rear_corr_table, _extra=_extra)

      rear_segment = Self->Get(/rear_segment)
      ;rear_segment will be -1 unless it's binned_eventlist
      ;then rear_segment will be 1 only if it's for the REAR_SEGMENT

      ;
      ;If it's binned_eventlist then only 1 of next two ops are done
      ;
      Is_Rear_Decimated = Rear_segment eq 0? 0 :Is_rear_decimated
      Is_Front_Decimated = Rear_segment eq 1 ? 0 :Is_Front_Decimated
      If (REAR_SEGMENT ne 1) and Is_Front_Decimated then $
        Self->decim_correct_ltime, decim_time, $
           dim_spectrum, corr_table, ltime, /front
      If (REAR_SEGMENT ne 0) and  Is_Rear_Decimated then $
        Self->decim_correct_ltime, rear_decim_time, $
           dim_spectrum, rear_corr_table, ltime, front=0


    endif

end

;----------------------------------------------------------------------
;Self->decim_correct_ltime, decim_time, dim_spectrum, corr_table, ltime, front
pro hsi_spectrogram::decim_correct_ltime,$
    decim_time, $
    dim_spectrum,$
    corr_table, $
    ltime, $
    front=front

ut = Self->GetAxis(/ut, /edges_2)
id_off = 9*(1-front)
indx = indgen(9)+id_off
;Start with the default case of a single decimation interval
dtm = decim_time.start_time ;these must span the absolute time interval
ndtm = n_elements(dtm)
sel  =0
if size(/tname, ltime) ne 'POINTER' THEN BEGIN;Expand livetime
    ;Use old style call to rebin explictly using the dimensions
    ;dim_spectrum is four or less so this should always work
    ;with ones for the degenerate dimensions
    temp = lonarr(6)+1
    temp[0]=dim_spectrum

    ltime = rebin(  ltime, $
    temp[0],temp[1],temp[2],temp[3], temp[4], temp[5] ) ; dim_spectrum )


;fixed this line 30-jun-2005, ras
    di = value_locate([dtm, last_item(ut)>last_item(dtm)], ut) >0 < (ndtm-1)
    ;where di[0,*] eq di[1,*] they use one of the default
    ;correction tables, where they aren't equal we have
    ;to build a time weighted one.

    d = histogram( (di[1,*]-di[0,*] <1),min=0,max=1, bin=1,rev=rd)
    ;Start with d[0] for no difference
    if d[0] ge 1 then begin
        s0 = rd[rd[0]:rd[1]-1]
        ;Get the tables needed for each spectrum
        h  = histogram( di[0,s0], bin=1, min=0,max=ndtm-1, rev=rh)
        for i=0,ndtm-1 do begin
            if h[i] ge 1 then begin
               sel = s0[rh[rh[i]:rh[i+1]-1]]
            correction = rebin(  reform( corr_table[*,*,i], dim_spectrum[0], 1, 9), $
                   dim_spectrum[0], h[i], 9 )

            for j=0, dim_spectrum[3]-1 do ltime[*,sel, indx,j]  = $
            f_div( ltime[*, sel, indx,j] , correction )


            endif
            endfor

       endif
       if d[1] ge 1 then begin ;correct multiple decimation by time weighting only
        s1 = rd[rd[1]:rd[2]-1]
        for i=0,d[1]-1 do begin
            ;For each interval identify the run of correction tables and time weights
            tdi = di[*,s1[i]] ;working on this interval, s1[i]
            tut = ut[*,s1[i]] ;this time range
            message,'Multiple Decimation States correction across this interval. Caution.'$
                +anytim(/yoh,tut[0])+' - '+anytim(/yoh,tut[1]),/info
            dur = float(tut[1]- tut[0])
            ;These tables
            num_dtm = tdi[1]-tdi[0]+1
            tidx = tdi[0] + indgen(num_dtm)
            dtmi = dtm[tidx]
            f0 = float((dtmi[1] - tut[0]))
            fL = float((tut[1] - dtmi[num_dtm-1]))

            correction =  f0/corr_table[*,*,tidx[0]] + fL/corr_table[*,*,tidx[num_dtm-1]]
            if num_dtm ge 3 then begin
               fi = float((dtmi[2:*]-dtmi[1:*]))

               for j = 0, num_dtm-3 do $
               correction = correction + fi[j]/corr_table[*,*,tidx[1]+j]
               endif
            correction = dur / correction
            for j=0, dim_spectrum[3]-1 do ltime[*,s1[i],indx,j] = $
                reform( reform(ltime[*,s1[i],indx,j]) / correction,dim_spectrum[0],1,9)

         endfor

       endif





;    sel=n_elements(ut) gt 2 ? value_locate( ut[0,*], decim_time.start_time) : 0
;    sel = get_edges( [sel, dim_spectrum[1] ], /edges_2 ) >0
;    ;Add protection against overlapping decimation states in the
;    ;same time bins, (>sel[0,*] added 6-nov-2003), ras
;    sel[1,*] = sel[1,*] - 1 > 0 > sel[0,*]
;
;    nsel = n_elements( decim_time )
;    add_text = keyword_set(front) ? 'Front Decimation: ' : 'Rear Decimation: '
;    If nsel gt n_elements(sel[0,*]) then begin
;           decim_warning = $
;           add_text+'Fewer time bins than decimation intervals. Expect poor correction'
;        message, /info, decim_warning
;        Self->set, decim_warning = decim_warning
;        nsel = nsel < n_elements(sel[0,*])
;        endif
;    If nsel ge 2 then if min( sel[0,1:*] - sel[0,*]) eq 0 then begin
;          decim_warning = add_text + 'Decimation states overlap time intervals. Decimation '+$
;          'correction is indeterminate'
;        message, /info, decim_warning
;        Self->set, decim_warning = decim_warning
;
;        endif
;    indx = indgen(9)+id_off
;    for i=0,nsel-1 do begin
;
;        correction = rebin(  reform( corr_table[*,*,i], dim_spectrum[0], 1, 9), $
;                   dim_spectrum[0], sel[1,i] - sel[0,i]+1, 9 )
;
;        for j=0, dim_spectrum[3]-1 do ltime[*,sel[0,i]:sel[1,i], indx]  = $
;        f_div( ltime[*, sel[0,i]:sel[1,i], indx] , correction )
;
;        endfor

ENDIF ELSE BEGIN



    for id=0,8 do begin
       this_be = *ltime[id]

       this_ut    = get_edges(/edges_2, *ut[id])
       sel=n_elements(ut) gt 2 ? value_locate( this_ut[0,*], decim_time.start_time) : 0
       dim_spectrum = [n_elements(this_be[0].livetime),$
       n_elements( this_be.livetime[0] )]
       sel = get_edges( [sel, dim_spectrum[1]], /edges_2 )
       sel[1,*] = sel[1,*] - 1 > 0

       nsel = n_elements( decim_time )


         for i=0,nsel-1 do begin

         correction = rebin( corr_table[*,id,i], $
                     dim_spectrum[0], sel[1,i] - sel[0,i]+1)

         this_be[sel[0,i]:sel[1,i]].livetime  = $
         f_div( this_be[sel[0,i]:sel[1,i]].livetime , correction )

         endfor
       ;Return the modified livetime to the pointer
       *ltime[id] = this_be
       ENDFOR
    ENDELSE
end
;----------------------------------------------------------------------

;+
;Name: HSI_SPECTROGRAM_DECIM_TABLE
;   This function computes the decimation correction table
;   One entry for every front segment for every channel,weight,time entry in decim_time
;   Works best on narrow energy bins.
;History
;	ras, modifiied 14-jul-2008 to account for multiple weight states
;		within a rear decimation interval
;   richard.schwartz@gsfc.nasa.gov, 17-jun-2003
;-
function hsi_spectrogram::decim_table, decim_time, rear_decim_time,$
    rout=rout, _extra=_extra

energy2d = Self->GetAxis( /energy, /edges_2)
offset_gain = Self->Get_offset_gain()
decim_apar = Self->Get( /decim_apar )
If n_elements(decim_apar) eq 1  then decim_apar = [1.,2.,1.,4.,400.,4.]
decim_apar = decim_apar > $
    [1e-5,0.5,1e-10,1.2,10.,1.2] < [1e5,20.,1e6,12,1e4,12.] ;six parameters for f_vth_bpow

if size(/tname, decim_time) eq 'STRUCT' then begin
    out =  hsi_decim_correction( energy2d, decim_time, offset_gain, $
        apar=decim_apar, /front, etab=etab, frac=frac, ffff=ffff, _extra=_extra)
    decim_time.correction = ffff ;intermediate value between decimation weight and one
    decim_time.energy = etab
    endif


;5-may-2005 - extend this parameter to include rear decimation inforout = 0

if size(/tname, rear_decim_time) eq 'STRUCT' then begin
    rout =  hsi_decim_correction( energy2d, rear_decim_time, offset_gain, $
    apar=decim_apar, front=0, etab=etab, frac=frac, ffff=ffff, _extra=_extra)
    rear_decim_time.correction = ffff ;intermediate value between decimation weight and one
    rear_decim_time.energy = etab
    endif
;5-may-2005 - extend this parameter to include rear decimation info

decim_table =   {front:decim_time, rear:rear_decim_time}
Self->set, decim_table = decim_table ;store corrections
return, out
end

;------------------------------------------------------------------------



;------------------------------------------------------------------------


;Get the times, decimation states, atten states for all intervals
;11-sep-2003 ras, check for decimation turn on 19-mar-02 05:38 UT
function hsi_spectrogram::decim_time_range, obj=obj, $
    ch_atten, exclude_atten, wselect = wselect, $
    rear_decim_correct=rear_decim_correct

cf = Self->Get_offset_gain()
cf = float(cf)
absolute_time = Self->get(/absolute_time)
last_time = absolute_time[1]


if keyword_set( obj ) then Self=obj
flag = Self->get_obs(/changes)
params = 0
;ras, 4-may-2005 add rear decimation
if keyword_set(rear_decim_correct) then begin
	r128 = stc2arr( flag.rear_dec_chan_128)
	rkev = stc2arr( flag.rear_dec_kev)
	rwgt = stc2arr( flag.rear_dec_weight)
    nwgt= n_elements(rwgt) ; should be the longest vector array, every time r128 changes, rkev, and rwgt
    ;change.  however, rwgt can change without changing the others.
	rear = replicate( {start_times:0.0d0, end_times:0.0d0, chan128:0b, kev:0b, weight:0b}, nwgt)
	rear.start_times = rwgt.start_times
	rear.end_times  = rwgt.end_times
	rear.weight = rwgt.state
	match, r128.start_times, rear.start_times, suba, subb, count=count
	rear[subb].chan128 = r128[suba].state
	rear[subb].kev          = rkev[suba].state
	wfill = where( rear.weight gt 1 and rear.chan128 eq 0, nfill)
	if nfill ge 1 then begin
		wfrm = wfill-1
		test =1
		while test ge 1 do begin
			wtst = where( rear[wfrm].chan128 eq 0, test)
			if test ge 1 then wfrm[wtst] = wfrm[wtst]-1
			endwhile
		rear[wfill].chan128 = rear[wfrm].chan128
		rear[wfill].kev          = rear[wfrm].kev
		endif
;    rear = rep_tag_name( flag.rear_dec_chan_128,'state','chan128')
;    rear = add_tag( rear,flag.rear_dec_kev.state, 'kev')
;    rear = add_tag( rear,flag.rear_dec_weight.state,'weight')

    ;if rear.start_times eq -1 then begin
        rear[0] .start_times= absolute_time[0]
        rear[n_elements(rear)-1].end_times = absolute_time[1]


      ;ras, 4-may-2005 add rear decimation
    ;ras, 4-may-2005 add rear decimation

    rparam = 0

    valid_rear = where(rear.end_times ge absolute_time[0] and $
        rear.start_times le absolute_time[1], nvrear)

    rear.start_times = rear.start_times > absolute_time[0]

    rear.end_times   =  rear.end_times  < absolute_time[1]


    if nvrear ge 1 then begin

        rparam = replicate( {start_time:0.0d0, end_time:0.0d0, $
             channel:0, weight:0, atten:0, $
            energy:fltarr(9),$ ;Energy of channel for each rear detector
            correction:fltarr(9) $;livetime correction factor for energy channel boundary
             }, nvrear)
        rparam.start_time = (rear.start_times)[valid_rear]
        rparam.end_time = (rear.end_times)[valid_rear]
        rparam.channel = (128 * rear.chan128-1)[valid_rear]
        rparam.weight = (rear.weight)[valid_rear] >1.
        rparam.energy = size(/tname, rparam) NE 'STRUCT' ? rparam : $
        cf[9:17,1]#(rparam.channel+1) + cf[9:17,0]#(rparam.channel*0.+1.) >0.0

       params = rparam
        endif
;ras, 4-may-2005 add rear decimation

    endif else begin

    ;Error condition for flag, when an observing summary can't be found
    if size(/tname, flag) eq 'STRUCT' then begin
     idpu_version = flag.idpu_control.state
        wnz = where(idpu_version gt 0, nnz)
        idpu_version = nnz ge 1 ? $
            {version:idpu_version[wnz],time:flag.idpu_control.start_times[wnz]} $
            : $
            {version:8, time:flag.idpu_control.start_times[0]}
        endif else begin

        pk   = Self->Get(/obj,class='HSI_PACKET')
        idpu = pk->GetIDPU()
        if idpu[0] eq -1 then begin
           message,/info, 'Cannot obtain IDPU version, return -1'
           return, -1
           endif

        idpu_version = {version: idpu[0], time: (pk->get(/obs_time))[0]}
        endelse

    ;Idpu can't change more than once

    ;atten_ssr = Self->lookup(/atten_state,/ssr_state)
    ; acs 2005-04-18 - replace the call to llokup with the call to the eventlist
    ; which shoudl be managing that situation. in this way the source of the data
    ; can be either an eventlist file or a telemetry file.
    source = self->get( /source )
    ;These are filtered atten_states.  only states lasting longer than 1 second are included
    atten = self->get( /sp_atten_state )

    ;atten = atten_ssr.atten_state
    ;First find times when atten.state eq 4
    sel = where( atten.state eq 4, nexclude)
    exclude_atten = 0
    if nexclude ge 1 then begin
        exclude_atten = replicate({ start_time:0.0d0, end_time:0.0d0 },nexclude)
        exclude_atten.start_time= (atten.time)[sel]
        exclude_atten.end_time = ([atten.time,last_time])[sel+1]
        endif

    ;sel = where( atten.state lt 4, natten)
    sel = where( atten.state le 4, natten)
    if natten ge 1 then $
        atten =atten[sel]

    ;ssr = atten_ssr. ssr_state
    ; acs 2005-04-18, see comment above
    ssr = source->getdata( /ssr )

    alltime = [atten.time, ssr.time]
    alltime = alltime[ sort(alltime) ]
    alltime = alltime[ uniq(alltime)]
    ntime = n_elements( alltime )
    issr = n_elements( ssr.time) eq 1 ? 0*fix(alltime) : value_locate( ssr.time, alltime )
    iatten = n_elements( atten.time ) eq 1 ? 0*fix(alltime) :value_locate( atten.time, alltime)
    params = replicate( {start_time:0.0d0, end_time:0.0d0, $
        ssr:0b, atten:0b, idpu:0, channel:0, weight:0,$
        energy:fltarr(9),$ ;Energy of channel for each front detector
        correction:fltarr(9) $;livetime correction factor for energy channel boundary
         }, ntime)
    params.start_time = alltime
    params.end_time = ([alltime,last_time])[1:*]
    params.ssr  = ssr.state[issr]
    params.atten = atten[iatten].state
    params.idpu = idpu_version.version[0]

    valid_front = where(params.end_time ge absolute_time[0] and $
        params.end_time le absolute_time[1], nvfront)

    if nvfront ge 1 then params = params[valid_front] else message,/info,$
       'No valid parameter time ranges for front decimation. Decimation correction should be faulty'

    for i=0,nvfront-1 do begin
        hsi_front_decimation, params[i].atten, params[i].ssr, params[i].idpu, weight, channel
        params[i].weight = weight[0]
        params[i].channel = channel[0]
        endfor
    ch_weight = 1000000L*params.weight + params.channel
    find_changes, params.atten, index_atten
    ch_atten = params[index_atten]

    ;Remove 4's
    no4 = where( params.atten lt 4, nno4)
    if nno4 ge 1 then begin
         params = params[no4]
         ch_weight = ch_weight[no4]
         endif

    ;
    find_changes, ch_weight, index

    params = params[index]
    ch_weight = ch_weight[index]

    ;Finally, check start and end times
    ;First start should be at interval start, every end time should be the next start time
    ;last end time should be interval end
    params[0].start_time = absolute_time[0]
    n = n_elements(index)
    if n ge 2 then params[0:n-1].end_time = [params[1:n-1].start_time, absolute_time[1]] $
        else params.start_time = absolute_time[0]
    ;Check for start of real decimation on 19-mar-02 05:38
    decim_turnon = anytim( '19-mar-02 05:38:00' )
    bdt = where( params.end_time lt decim_turnon, nbdt)
    if nbdt ge 1 then begin
        params[bdt].channel = 0
        params[bdt].weight = 1.
        endif

    ;Move energy value computation here from decim_table and hsi_decim_correction

    params.energy=cf[0:8,1]#(params.channel+1) + cf[0:8,0]#(params.channel*0.+1.) > 0.0
    endelse

return, params

end

;-------------------------------------------------------------------------

;+
; PROJECT:  HESSI
;
; NAME: hsi_spectrogram__get_obs
;
; PURPOSE: Get_obs method for hsi_spectrogram object to quickly retrieve observing summary data
;   for the image time period.
;
;   Retrieves the hsi_obs_source object which is a source of the eventlist object,
;   and calls getdata on it with whatever keywords user specified.  Note that the sub_time_interval
;   keyword is set to the actual time interval used for the spectrogram (not obs_time_interval); the
;   ql data returned is only for the sub_time_interval (except for /changes keyword -  that returns
;   the flag changes for the entire obs_time_interval).
;
; KEYWORDS:
;   Any keywords accepted by getdata in the hsi_obs_source object (see hsi_obs_source header)
;
; HISTORY:
;   Written: Kim Tolbert, 9-Apr-2003
;   21-aug-2003, RAS, modified to expand obs summary time range as needed and beyond
;
;---------------------------------------------------------------------------

function hsi_spectrogram::get_obs, _extra=_extra

obs = self -> get(/obj, class='hsi_obs_source')
;Set obs_time, expand 500 seconds
atr = self->Get(/absolute_time_range)
oti = obs->Get(/obs_time_interval)
if abs( total( oti - limits([atr,oti]))) ne 0 then $
    obs->set,obs_time_interval=atr+[-500.,500.]
out = obs -> getdata(sub_time_interval=self->get(/absolute_time_range), _extra=_extra)

if size(/tname, out) ne 'STRUCT' then begin
    message,/info,'Could not find observing summary data'
    return, -1
    end
return, out

end
;---------------------------------------------
;+
;Purpose:
;   Retrieve values and times of change from the lookup table
;   Mainly used to retrieve ATTEN_STATE, SSR_STATE, ATTEN_STATE
;   Only APP_ID 100 is used.
;   Don't use for collect_time, app_id, or bad_pak
;   Internal routine for experts only
;
; Example:
;   IDL> self = hsi_spectrum(obs_time='20-aug-02 '+['08:00','10:00'])
;   IDL> ptim,self->get(/abs)
;   20-Aug-2002 08:00:00.000 20-Aug-2002 10:00:00.000
;   IDL> state = self ->lookup(/ssr, /atten)
;   IDL> help,state
;   STATE           STRUCT    = -> <Anonymous> Array[1]
;   IDL> help,state,/st
;   ** Structure <272ad88>, 2 tags, length=176, data length=162, refs=1:
;      ATTEN_STATE     STRUCT    -> <Anonymous> Array[1]
;      SSR_STATE       STRUCT    -> <Anonymous> Array[1]
;   IDL> help,state.atten_state
;   <Expression>    STRUCT    = -> <Anonymous> Array[1]
;   IDL> help,state.atten_state.state
;   <Expression>    BYTE      = Array[17]

;

; CONTENTS OF THE LOOKUP TABLE
;IDL> help,{hsi_lookup_table},/st
;% Compiled module: HSI_LOOKUP_TABLE__DEFINE.
;** Structure HSI_LOOKUP_TABLE, 7 tags, length=24, data length=15:
;   APP_ID          INT              0
;   COLLECT_TIME    DOUBLE          0.00000000
;   BAD_PAK         BYTE         0
;   ATTEN_STATE     BYTE         0
;   FR_ENABLE       BYTE         0
;   IN_SUN          BYTE         0
;   SSR_STATE       BYTE         0

; History:
;   Richard.Schwartz@gsfc.nasa.gov, 8-apr-03
;-

function hsi_spectrogram::lookup, _extra= extra ;, $
;   ATTEN_STATE=atten_state, $
;            SSR_STATE=ssr_state, $
;            INSUN_STATE=insun_state, etc.


    etags = tag_names(extra)

    pk_obj= self->Get(/obj, CLASS='HSI_PACKET')
    lookup_table = PK_OBJ->GetData( /LOOKUP_TABLE )
    ltags = tag_names( lookup_table )
    arg = fix_extra( extra, ltags)
    trange = Self->Get(/absolute_time)
    selection = where( lookup_table.collect_time ge trange[0] and $
    lookup_table.collect_time le trange[1] and $
    lookup_table.app_id eq 100, nselection)
    this_LT = lookup_table[selection]
    narg = n_tags( arg )
    sel = where_arr( ltags, tag_names(arg), narg)
    if narg eq 0 then return, -1
    for i=0, narg-1 do begin
        find_changes, this_lt.(sel[i]), index, state
        temp = {time:this_lt[index].collect_time, state:state}
        out_state = exist( out_state) ? add_tag( out_state,temp,ltags[sel[i]]) : $
           create_struct( (tag_names(arg))[i], temp)
        endfor

    return, out_state


end
;+
;Name: hsi_spectrogram__livetime
;
;Purpose: An included file to hold all of the spectrogram livetime and datagap modules
;History:
;   9-Oct-2003 richard.schwartz@gsfc.nasa.gov - first built this file
;-






;+
;Loop over accumulation event and livetime lists
;Loop over each segment
;Enter livetime with list of datagap times and datagap deadtime array.
;Identify livetime samples (512 usec) outside of datagaps
;Sum livetime values into livetime array.  Sum number of sample in each livetime bin into samples array.
;For each livetime bin with no sample, but a datagap value less than the total duration of the interval, average the
;two surrounding valid livetime values.
;Finally, normalize the livetimes by the datagaps
;20-jul-02, ras, add normalization for detection of intervals where the detector
;   is turned off.
;Add protection for one sel1 value, ras, 13-aug-02.
;Ensure that ut_off.e is after ut_off.s, ras, 14-aug-02.
;12-sep-02, ras, fixed a bad bug identified by Ed Schmahl
;   had not been using the sel0 and sel1 arrays correctly, had forgotten
;   that they were indices on the array wv, so the interpolation for unknown values
;   was totally messed up.
; 6-nov-02, ras, defend against nwv of 0
; 9-oct-2003, ras, revise time bin (ut) management to be more efficient
; 23-mar-2005, ras, change interpolation scheme for intervals without reported livetime
; 1-apr-2007, ras, don't fill livetime into empty sample bins for times gt 0.1 sec
;-
Pro hsi_spectrogram::livetime_merge_datagaps, detector_off, time_binning

If Not (Self->Get(/Livetime_enable) and Self->Get(/dp_enable) ) $
    then return

livetime_arr = self->get(/livetime_arr)
dropout = self->get(/dropout)
is_ptr = 0
If Size(/tname, dropout) eq 'POINTER' then begin
    ;segs = where( ptr_valid(dropout) and ptr_valid(livetime_arr), nsegmnt)
    segs = where(  ptr_valid(livetime_arr), nlsegmnt)
    dsegs = where(  ptr_valid(dropout), ndsegmnt)

    is_ptr = 1
    endif else begin
    diml = size(/dim, livetime_arr)
    dimd = size(/dim, dropout)
    nlsegmnt = diml[1]
    ndsegmnt = dimd[1]
    endelse

ut_ptr = is_ptr ? Self->Getaxis(/ut ) : 0
ut = is_ptr ? 1 : get_edges(/edges_2, time_binning.newcal)
tbin = is_ptr? (self->get(/time_bin_def) * self->get(/time_bin_min))/2.^20 : fltarr(9) + min(ut[1,*]-ut[0,*])
tbinsmall = tbin lt (8192./2.^20)
for iseg=0, nlsegmnt-1 do begin

    if is_ptr then begin
        ;livetime 0-1
        seg = segs[iseg]
        lseg=*livetime_arr[seg]   >0.0 <1.0
        ;accumulated dropouts, 0-tdur, where tdur is the interval duration in seconds.
        ;no dropouts for rear segments now, ras, 23-mar-2005
        dseg=iseg lt ndsegmnt ? *dropout[seg] :lseg*0.0
        endif else begin
           seg = iseg
           lseg = livetime_arr[*, seg]
           dseg= seg lt ndsegmnt ? dropout[*,seg] :lseg*0.0
           endelse


;For ptr you need to extract a different ut for each
    ut = is_ptr? get_edges(*(ut_ptr)[seg mod 9],/edges_2)  : ut

    wseg =float( get_edges(ut,/width) )
    nseg = n_elements( wseg )
    ;normalize to 0-1.
    dseg=dseg/wseg  >0.0 <1.0

    ;Find the accumulation intervals where there is less than a total datagap
    cull_frac = self->Get(/CULL_FRAC) ; RAS, 16-MAR-2005, FILL IN LIVETIME
    ;WHEN THE AVG LIVETIME IS GT 50% AND DSEG IS LT CULL_FRAC
    ;Have to adjust by limit in readouts of no more than 1 every 512 usec
    ;Don't apply fill for intervals longer than 0.1 second, ever


    wv=where( dseg  lt CULL_frac and wseg lt 0.1, nwv) ;and wseg condition; added 1-apr-2007, ras

    if (nwv ge 1) then lseg[wv] = self->fill_livetime(lseg[wv],fill_zero=tbinsmall[iseg mod 9])


    ;Now, normalize all intervals by the datagap livetime

    lseg = lseg *( 1.-dseg) >0.0 < 1.0

    ;Here we set the livetime to zero for intervals where the detectors are off
    ;Assume for now that all segments are used
    If size(/tname, detector_off) eq 'STRUCT' then begin
       ut_off = seg le 8 ? detector_off.front : detector_off.rear
       if size(/tname, ut_off) eq 'STRUCT' then begin
       noff = n_elements( ut_off )
       ut_off.s = ut_off.s > ut[0,0] < ut[1, nseg-1]
       ut_off.e = ut_off.e < ut[1,nseg-1] > ut_off.s
       wlseg = lseg *0.0 + 1.0

       wstr = n_elements(ut[0,*]) eq 1? lonarr(noff) :value_locate( ut[0,*], ut_off.s)
       wend = n_elements(ut[0,*]) eq 1? lonarr(noff) :value_locate( ut[0,*], ut_off.e)
       for ioff = 0L, noff -1 do begin
         ei = wend[ioff] & si = wstr[ioff]
         weight = fltarr( ei-si+1 ) + 1
         weight[0] = float( (ut[1,si]-ut_off[ioff].s) / (ut[1,si]-ut[0,si]) )
         if ei gt si then weight[ei-si] = $
          float( (ut_off[ioff].e - ut[0,ei]) / (ut[1,ei]-ut[0,ei]) )
         if ei eq si then weight = $
          float( (ut_off[ioff].e-ut_off[ioff].s) / (ut[1,ei]-ut[0,ei]) )
         wlseg[si:ei] = wlseg[si:ei] - weight
         endfor
       lseg = lseg * float( wlseg )
       endif
       endif
    if is_ptr then $

    *livetime_arr[seg]   = lseg[*]  $

    else livetime_arr[0, seg] = lseg[*]


    endfor

Self->set, livetime_arr = livetime_arr

end
;----------------------------------------------------------------
function hsi_spectrogram::fill_livetime,  lseg,  fill_zero=fill_zero

;If fill_zero is set, then empty values are assumed to contain 0 deadtime, full livetime
;which is about 99%.  Not 100%, using discrete peak value as measured.
default, fill_zero, 1



       ;We want to fill in livetime where it is 0 by interpolating from
       ;surrounding values, but only when there isn't a total dropout, the wv selection
       ;
       hh = histogram( (lseg gt 0),min=0, max=1, rev=rz)
       ;If both aren't defined, give up
       if hh[0] gt 0 and hh[1] gt 0 then begin
         sel0= rz[rz[0]:rz[1]-1]
         sel1= rz[rz[1]:rz[2]-1]
         ;Find the index in sel1 preceding each sel0
         ;Add protection for one sel1 value, ras, 13-aug-02.
         locate_lz = n_elements(sel1) eq 1? 0 : value_locate( sel1, sel0) > 0
         locate_lz_nxt = (locate_lz + 1) < (n_elements(sel1)-1)
         ;Compute the average of the nearest neighbors
         lseg1 = lseg[sel1]
         replace_lz = fill_zero? 0.99 : 0.5 * ( lseg1[locate_lz] + lseg1[locate_lz_nxt])
         ;
         lseg[sel0] = replace_lz
       endif

return, lseg
end


;----------------------------------------------------------------

;+
; PROJECT:
;   HESSI
; NAME:
;   hsi_spectrogram::ACCBIN
;
; PURPOSE:
;   Returns arrays or pointers of accumulation bins
;   dimensioned by energy/time/segment as needed
; CATEGORY:
;   SPECTRA
;
; CALLING SEQUENCE:
;
;
; CALLS:
;   none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   none
;
; MODIFICATION HISTORY:
;   Version 1, richard.schwartz@gsfc.nasa.gov
;   May 21, 2002
;   25-jul-02, accumulate dropouts for front and rear
;-
pro hsi_spectrogram::accbin, time_binning, $
    livetime_arr, $
    samples, $
    deadacc_arr

If keyword_set(_extra) then Self->Set, _extra=_extra


If (not exist( livetime_arr)) and (not exist( samples )) and (not exist( deadacc_arr)) then begin



    ntime = n_elements( time_binning.newcal ) -1
    livetime_arr= fltarr(ntime, 18)
    deadacc_arr= livetime_arr[*,0:8]
    samples = livetime_arr

endif

end

;---------------------------------------------------------------------------
 ;+

 ; NAME:
 ;  hsi_spectrogram::livetime_regroup
 ;
 ; PURPOSE:
 ;  This procedure regroups the livetime_arr and livetime_ctr
 ;  info parameters into their final bins from their
 ;  accumulation bins.  Since these are averaged values, they
 ;  must be averaged by time.
 ;

 ; CALLING SEQUENCE:
 ;  Self->livetime_regroup

 ; INPUTS:
 ;   Time_regroup - array with bin information
 ;     Livetime_arr and livetime_ctr info parameters
 ;
 ;
 ;

 ;
 ;
 ; PROCEDURE:
 ;  A fine cell(bin/channel) array is summed into the grouped
 ;  cells given by  TIME_REGROUP.
 ;
 ; MODIFICATION HISTORY:
 ;  Version 1, richard.schwartz@gsfc.nasa.gov
 ;  2-oct-2003
 ;
 ;-



 pro hsi_spectrogram::livetime_regroup,  $
    time_binning
if keyword_set( time_binning.regroup ) then begin

utags = ['LIVETIME_ARR','LIVETIME_CTR']

for itag=0,n_elements(utags)-1 do begin


  temp = utags[itag] eq 'LIVETIME_ARR' ? Self->Get(/LIVETIME_ARR) : Self->Get(/LIVETIME_CTR)
  na2d = (size(/dim, temp))[1]

 ;There could be time overlap, so look at time_binning.regroup to get final time bins

   nbin2 = n_elements( time_binning.regroup )/2
   out = fltarr( nbin2, na2d )

   ir = where( time_binning.regroup[1,*] eq time_binning.regroup[0,*], nir)
   if nir ge 1 then begin
   ;No summing time bins, just mapping one set of bins to another
        out[ir,*] = temp[time_binning.regroup[0,*], *]


        endif

    ir = where( time_binning.regroup[1,*] gt time_binning.regroup[0,*], nir)
    ;Some bins may overlap and be composed of more than one interval
    ;so we have to compute a weighted average
    if nir ge 1 then begin
       wght = time_binning.newcal[1:*]-time_binning.newcal
        regroup = time_binning.regroup[*, ir]
        nb = regroup[1,*] - regroup[0,*]
        for i=0L,nir-1 do begin
           tb = regroup[0,i] + lindgen(nb)

           out[ir[i],*] =   wght[tb]# temp[tb,*] / total(wght[tb])

           endfor

       endif

       if utags[itag] eq 'LIVETIME_ARR' then $
        Self->Set, livetime_arr = out else Self->Set,livetime_ctr=out


   endfor
endif
 end


;---------------------------------------------------------------------------
;---------------------------------------------------------------------------

 ;+
 ; PROJECT:
 ;  HESSI
 ; NAME:
 ;  hsi_spectrogram::MKLIVETIME
 ;
 ; PURPOSE:
 ;  This procedure computes the measured livetime integrated
 ;  over the accumulation bins.
 ;
 ;

 ; CALLING SEQUENCE:
;   obj->mklivetime, $
;
;   ltlist, $
;   time_binning, $
;   time_offset, $
;   livearr, $
;   samples;
 ; CALLS:
 ;
 ;
 ; INPUTS:
 ;  Ltlist - livetime structure
    ; ** Structure <997a018>, 3 tags, length=901252, data length=901250, refs=2:
    ;   TIME            LONG      Array[128750]
    ;   COUNTER         INT       Array[128750]
    ;   SEG_INDEX       BYTE      Array[128750]
 ;  Time_binning - time_binning structure from tbin_prep()
 ;  Time_offset - Time difference between current packet-bunch's reference time
 ;   and the one that started the accumulation
 ;
 ; OPTIONAL KEYWORD, INPUTS:
 ;
 ; OUTPUTS:
  ; Livearr - summed livetime fractions
 ;  Samples - number of samples used for livearr
;
 ; PROCEDURE:
;
;The data input structure, ltlist, consists of a time vector, counter vector, and seg_index vector.
;The structure is input in streaming format, each tag is a separate array of equal length, and the
;arrays are synchronized, i.e. time[i], counter[i], seg_index[i] all refer to the same livetime readout.
;To accumulate the livetime, the data for each seg_index (0-17) are processed separately. The
;reverse_indices in histogram are used to obtain the indices to use for a given seg_index.
;Using value_locate an index ( this_time_spec ) is generated for each time by comparing to the times
;of the accumulation bins. Then histogram is run on this_time_spec to obtain the number of samples of
;livetime for each time accumulation bin.  That sample number is saved in the samples array. To obtain
;the average of the livetime in each accumulation bin, the counter (modified to the true livetime fraction)
;is summed cumulatively using total(/cum).  The total for each bin is obtained by differencing based on the
;number of samples in each bin, and this total in the bin is saved in the livetime array.
;
 ;
 ;
 ; MODIFICATION HISTORY:
 ;  7-jul-2003, major bug fix.  was starting the running total of the
 ;  counter at the start of the ltlist.counter even though there would
 ;  always be a two second pad (extend_time_range) at the start of the
 ;  array.  Usually on the second call, there wouldn't be any pad, all of the
 ;  times would be after the start of the accumulation array
 ;
;   may 25, 2003, acs minor change to fix a bug
 ;  May 21, 2002, ras
 ;  16-may-2003, ras, support streaming structure of livetime as well as packetized
 ;-

 pro hsi_spectrogram::mklivetime, $

    ltlist, $
    time_binning, $
    time_offset, $
    livearr, $
    samples

 is_binned_eventlist = 'POINTER' eq Size( livearr, /tname )


 a2d_index_mask = Self->Get(/A2D_INDEX_MASK)
 these_a2d = Where( A2D_INDEX_MASK , na2d )

 these_seg = hsi_a2d2seg( these_a2d )
 nseg = n_elements( these_live)

 lt_ut_ref = hsi_sctime2any( Self->Get(/lt_ut_ref))
 ;Compute the difference between the livetime reference time
 ;and the packet buffer reference time
 ;time_offset is computed wrt to the packet buffer reference time UT_REF
 delta_offset = Self->Get(/ut_ref) - lt_ut_ref
 time_unit = self->get(/time_unit)
 lt_time_unit = 512 ;default for livetime time


 nkeep = 0

 constant = hessi_constant()

;Prevent empty ltlist crash
 if size(/tname, ltlist) eq 'STRUCT' then BEGIN

; acs changed the place of this it was too early
     nselect = n_elements(ltlist.time)

     if chktag(ltlist, 'seg_index') then begin


         time_spec = ltlist.time  + lonarr(nselect)

         seg_index = ltlist.seg_index

     ;If there are any time gaps or overlaps, the final time summations
     ;are defined in time_final_assign
     ;
         time_bin_busec_a2d = gt_tagval( time_binning, 'time_bin_busec_a2d', missing=0)

     ;
     ;First block is to handle the binned ltlist.  These are time bins of
     ;fixed size, or grouping factor,  in time_unit, but may be different for each a2d.
     ;The grouping factor should be consistent for each collimator, i.e. the same for a2d mod 9.
     ;The grouping factor should be small enough to be consistent with the finest harmonic
     ;required, but this routine should not know anything about the harmonics.
         if is_binned_eventlist then begin

             time_start = long64((lt_ut_ref-time_binning.abs_time_range[0] )*2.0^20/lt_time_unit)
         ;Time_start is in units of lt_time_unit, this is the time reference for
         ;time_spec relative to the start of the time_range
         ;Next, with time_spec in units of lt_time_unit, add it to the time_start
         ;and identify it with it's been in the binned eventlist
             time_spec[0] = long(   (time_spec + time_start) / $
                                    (time_bin_busec_a2d[seg_index]/lt_time_unit)   )

         endif else begin

         ;This block handles the case of the spectrogram where the time binning is the same
         ;for each a2d, but this binning may be completely arbitrary, and is given by
         ;time_binning.group in time_unit.
         ;tspec=time_spec
             time_spec[0] = value_locate( $
                  long64([time_binning.group[0]-10000d0, time_binning.group]*time_unit - $
                         (time_offset-delta_offset)*2.d0^20)/lt_time_unit, time_spec) - 1

         endelse


         s = histogram( ltlist.seg_index, min=0, max=17, rev=rev)
         seg_sel = where( s < 1 )
         w = where_arr( seg_sel, these_seg, nsel )
         seg_sel = seg_sel[w]

         for i=0, nsel-1 do begin

             this_seg = seg_sel[i]
             select = rev[rev[this_seg]:rev[this_seg+1]-1]
             use_seg = 1
             If Is_binned_eventlist then use_seg = ptr_valid( livearr[this_seg] )
             If use_seg then begin
                 livetime=is_binned_eventlist ? *livearr[this_seg] : livearr[*, this_seg]
                 sample_seg=is_binned_eventlist ? *samples[this_seg] : samples[*, this_seg]
                 nbin = n_elements(livetime)



                 ord = sort(time_spec[ select])
                 this_time_spec = time_spec[select[ord]]
                 counter = (ltlist.counter)[select[ord]]*1.0
                 select_time = where( this_time_spec ge 0, nselect_time)
                 if nselect_time ge 1 then begin
                     this_time_spec = this_time_spec[select_time[0]:*]
                     counter = counter[select_time[0]:*]

             ;
             ; Convert the counter value into actual livetime using David Smith's function
             ; Then, average over the livetime samples in each interval using the total(/cumul) function
             ;
             ;
                 counter = hsi_corrected_livetime(  counter, this_seg,/raw)

                     counter = [0.0, total(counter, /double, /cum)]
                     h = histogram( this_time_spec, bin=1, min=0, max=nbin-1, r=r)
                     htot = [0, total( h, /cum, /double)]


                     new_livetime = float(counter[htot[1:*]]-counter[htot])
                     sample_seg = sample_seg + float(h)
                     livetime = livetime + new_livetime


                     If is_binned_eventlist then *livearr[this_seg] = livetime else $
                         livearr[0, this_seg]=livetime
                     If is_binned_eventlist then *samples[this_seg] = sample_seg else $
                         samples[0, this_seg]=sample_seg
                 endif
             endif
         endfor

     ;Time_cumul_seg is an array which keeps the total cumulative number of time bins as a function
     ;of the seg_index.  This is needed for the final mapping between the tuples for
     ; (seg_index, time, channel) and the livearr  index.
     ENDIF
 ENDIF
                                ;return, livearr
end
;---------------------------------------------------------------------------
 ;+

 ; NAME:
 ;  HSI_SPECTROGRAM::time_regroup
 ;
 ; PURPOSE:
 ;  This function returns the final summation over fine
 ;  time bins.
 ;

 ; CALLING SEQUENCE:
 ;  final_spectrogram = HSI_SPECTROGRAM::Time_regroup( $
 ;  spectrogram, time_regroup,  time_cumul_a2d )

 ; INPUTS:
 ;     Spectrogram - spectrogram with fine cells.
 ;
 ;  Time_regroup - analog of Channel_regroup for time.
 ;

 ;
 ;
 ; PROCEDURE:
 ;  A fine cell(bin/channel) spectrogram is summed into the grouped
 ;  cells given by  TIME_REGROUP.
 ;
 ; MODIFICATION HISTORY:
 ;  Version 1, richard.schwartz@gsfc.nasa.gov
 ;  22-jan-2000.
 ;  17-apr-2001. Protect against total over array without the requested dimension.
 ;
 ;-



 function hsi_spectrogram::time_regroup, hsi_spectrogram,  $
   time_regroup,  time_cumul_a2d

 if keyword_set( time_regroup ) then begin
  coincidence_flag = Self->Get(/coincidence_flag)
  these_a2d = where( Self->Get(/a2d_index_mask), na2d)
  nchan =  (size(/dim, hsi_spectrogram))[0]
  hsi_spectrogram = reform( hsi_spectrogram, nchan, $
  time_cumul_a2d[1], na2d, coincidence_flag+1,/over)


 ;There could be time overlap, so look at time_regroup to get final time bins

   nbin2 = n_elements( time_regroup )/2
   out = fltarr(nchan, nbin2, na2d, coincidence_flag+1 )
   one_time_bin = time_regroup[0,*]   -time_regroup[1,*]
   for i=0L,nbin2-1 do out[*,i,*,*] = (one_time_bin[i] ne 0 ) ? $
   total( hsi_spectrogram[*, time_regroup[0,i]:time_regroup[1,i],*],2) : $
    hsi_spectrogram[*, time_regroup[0,i],*,*]

   hsi_spectrogram = temporary( out )
   ;
   ;hsi_spectrogram = reform( hsi_spectrogram,/over, nchan, na2d * nbin2, coincidence_flag+1 )
   endif

 return, hsi_spectrogram
 end





;---------------------------------------------------------------------------
PRO Hsi_Spectrogram__Define

; this is the data structure residing in self.data:

self = {Hsi_Spectrogram, $
        INHERITS Framework }

END


;---------------------------------------------------------------------------
; End of 'spectrogram__define.pro'.
;---------------------------------------------------------------------------


















