;+
; PROJECT:
;       HESSI
;
; NAME:
;       HSI_PIXON_INFO__DEFINE
;
; PURPOSE:
;       Defines the data structure for the pixon info parameters
;
; CATEGORY:
;       Imaging
;
; CALLING SEQUENCE:
;       struct = {hsi_pixon_info}
;
; TAG NAMES:
;           sensitivity
;           residual
;           rgof
;           error
;           iterate
;           outresolution
;           pixonmap
;           
; SEE ALSO:
;       hsi_pixon_control
;       hsi_pixon__define
;
; HISTORY:
;       T. Metcalf  2001-Feb-27
;       T. Metcalf  2002-Feb-14 Removed sensitivity and pixon_sizes
;                               since they are really control parameters
; 30-Apr-2010, Kim. Added inherits hsi_image_alg_info
; 15-Jan-2015, RAS, included used_pixon_sizes to differentiate info from control param
;   having the routine reuse the last pixon_sizes as was being done leads to inconsistent results
;   for reasons that are difficult for a user to understand
; ;                               
;-
;

PRO HSI_Pixon_info__define

;struct =  {HSI_Pixon_Info, $
;           sensitivity   :0.0, $
;           residual      :ptr_new(), $
;           rgof          :0.0, $
;           error         :ptr_new(), $
;           iterate       :0L , $
;           outresolution : 0.0, $
;           pixonmap      :ptr_new(), $
;           pixon_sizes   :ptr_new()}


struct =  {HSI_Pixon_Info, $
           residual      :ptr_new(), $
           bobj			 :ptr_new(), $ ;added background construction, 30-apr-2007, ras
           rgof          :0.0, $
           error         :ptr_new(), $
           iterate       :0L , $
           outresolution :0.0, $
           pixonmap      :ptr_new(), $
           used_pixon_sizes: ptr_new(), $
           inherits hsi_image_alg_info }

END

