;+
; Project: RHESSI
; 
; Name: hsi_spectrum_check_dets
; 
; Purpose: Plot count flux and photon flux (semi-calibrated) spectrum of all detectors for a selected time interval
; 
; Explanation:  Use this routine to plot the count and photon (semi-calibrated) spectrum of all RHESSI detectors separately 
;  during the time interval of interest to help understand the detector performance at that time.  These plots
;  may help you determine which detectors are OK to use for imaging and/or spectroscopy and in what energy range (see
;  RHESSI nugget of September 2014 for more info on interpreting plots).  These plots are generated automatically through
;  the wrapper routine hsi_spectrum_mk_det_plots for every concatenated flare in the RHESSI flare catalog and archived 
;  at http://hesperia.gsfc.nasa.gov/rhessi_det_plots/
;    
;  A RHESSI spectrum object will be set up with your time interval and energy bins that are .3 keV wide up to 10 keV, and 
;  then increase logarithmically up to 300 keV.
;  
;  The default output is three plots in a HESSI GUI PLOTMAN window (if a GUI exists it will use it, otherwise it starts a new one) -
;  the observing summary plot for the entire orbit containing your selected time (with dashed lines indicating your time interval), 
;  the count flux spectrum of all 9 detectors, and the semi-calibrated photon spectrum of all 9 detectors.
;  
;  In addition to (or instead of) the GUI plots, you can choose to plot the count and photon spectra in a stacked
;  plot on the screen (simple plot window), a PNG file, or a PostScript file.  
;  
; Calling sequence: hsi_spectrum_check_dets, time=time [,dur=dur, screen=screen, ps=ps, png=png, out_dir=out_dir, $
;                                            no_gui=no_gui, no_obs=no_obs, obj=spec_obj, out_file=out_file]
; 
; Sample calls:
;   To plot the observing summary and count and photon spectra in three panels in the GUI for a 1-minute time interval centered
;   on 11-jun-2014  05:34:14:
;     hsi_spectrum_check_dets,time='11-jun-2014  05:34:14'
;     
;   To plot the stacked count and photon spectra in a simple window on the screen (no GUI) for the same interval:
;     hsi_spectrum_check_dets,time='11-jun-2014  05:34:14', /no_gui
;     
;   To plot the stacked count and photon spectra in a PNG file (no GUI) for the same interval:
;     hsi_spectrum_check_dets,time='11-jun-2014  05:34:14', /no_gui, /png
;     
;   To plot just the count and photon spectra (no observing summary) in the GUI and a PNG file for the specified time interval:
;     hsi_spectrum_check_dets,time=['11-jun-2014 05:33:44','11-jun-2014 05:34:44'], /png, /no_obs
;     
;   To plot the stacked count and photon spectra in a PS file (no GUI) for a 20-second time interval centered on 8-jul-2013 01:22:05:
;     hsi_spectrum_check_dets,time='8-jul-2013 01:22:05', dur=20., /no_gui, /ps
;        
; Input Keywords:
;   time - time to plot in anytim format. 2-element array of start / end time, or scalar. If scalar,
;          a time interval centered on time of duration dur sec is plotted.  No default. You should probably choose a time near
;          the peak of the flare you're interested in analyzing.  You are responsible for ensuring that your time interval does
;          not cross an attenuator state change.
;   dur - duration in seconds of interval to plot, interval centered on time (used only if time is scalar). Default is 60 sec.
;   screen - 0/1 means don't/do make plot in simple window on screen (Default is 0.  If no_gui=1, ps=0, and png=0, default is 1.)
;   ps - 0/1 means don't/do make PS plot file. Default is 0. 
;   png - 0/1 means don't/do make PNG plot file. Default is 0. 
;   out_dir - Directory to write ps or png files in Default is current directory.
;   no_gui - If set, don't make plots in hessi GUI.  Default is 0.
;   no_obs - If set, don't make observing summary plot. (only used if no_gui not set) Default is 0.
;   
; Output keywords:
;   obj - reference to the spectrum object created.  If using GUI, (no_gui not set), then you can 
;     always get this object reference from the hessi GUI via hessi_data, spec=spec_obj
;   out_file - Returns name of output plot file written
;  
; Outputs:  Unless disabled, a hessi GUI instance is started with the spectrum object created here 
;   as input, (or the spectrum object is retrieved from an existing hessi GUI) the spectrum is accumulated and plotted, 
;   and a PS file is created (unless disabled via ps=0).  PS file name is hsi_sepdet_spectrum_yyyymmdd_hhmmsstohhmmss.ps.  
;   If png is enabled, a PNG file is created named hsi_sepdet_spectrum_yyyymmdd_hhmmsstohhmmss.png 
; 
; Written: Kim Tolbert, 13-jun-2014
; Modifications:  
; 16-Jun-2014, Kim. Changed handling of the spectrum object. If want hessi GUI (no_gui not set) then
;   get the spectrum object out of it (either an existing hessi GUI, or start one here and use that).
;   If no_gui set, then just create a new spectrum object.  In either case, pass out the spectrum
;   object in spec_obj. 
;   Added dur, no_obs, and out_file keywords.
; Aug-2014, Kim. Move hsi_linecolors call to after setting device and cleanplot call to before restoring device so 
;   doesn't crash in batch mode
; 9-Sep-2014, Kim. Added screen keyword, limit yrange to 1.e-3, change atten cutoff to 6 for not A0, add more labels,
;  use tick_label_exp function to force exponential labeling of y axis, and changed energy bin definition.
; 11-Feb-2015, Kim. Handle errors resulting in no plot file better. 
; 11-Dec-2015, Kim. When plotting in the GUI, added plots in counts and count rate in count space, previously just count flux 
;  Also, made text for 'Generated by...' a little bigger (was .8, now 1.*charsize)
; 
;-------------------------------------------------------------------------------
                                                                                                   
pro hsi_spectrum_check_dets, time=time_in, dur=dur, screen=screen, ps=ps, png=png, out_dir=out_dir, $
  no_gui=no_gui, no_obs=no_obs, obj=spec_obj, out_file=out_file

checkvar, png, 0
checkvar, ps, 0
checkvar, screen, 0
checkvar, no_gui, 0
checkvar, no_obs, 0
checkvar, dur, 60.
checkvar, out_dir, curdir()

if screen then png = 0
if screen then ps = 0
if png and ps then png = 0
if no_gui and ~ps and ~png then screen = 1

yes_gui = ~no_gui                            

if ~exist(time_in) or n_elements(time_in) gt 2 then begin
  message, /cont, "Please enter time as a scalar or a 2-element array, e.g. time='8-jul-2013 01:22:20'"
  return
endif

if yes_gui then begin
  if ~xregistered('hessi') then hessi
  hessi_data, plotman=plotman_obj, spec=spec_obj, obs=obs_obj
endif else spec_obj = hsi_spectrum()
  
time_range = anytim(time_in)

; if input time was scalar, create time range of dur seconds centered on time.
if n_elements(time_range) eq 1 then time_range = time_range  + dur * [-.5,.5]    
                                                                         
spec_obj-> set, decimation_correct= 1                                                             
spec_obj-> set, rear_decimation_correct= 0                                                        
spec_obj-> set, obs_time_interval= time_range    
spec_obj-> set, pileup_correct= 0                                                                 
spec_obj-> set, seg_index_mask= [1B, 1B, 1B, 1B, 1B, 1B, 1B, 1B, 1B, 0B, 0B, 0B, 0B, 0B, 0B, 0B, 0B, 0B]

; To choose channel binning, uncomment the following. NOTE: can't do semi_cal option below with channel binning
;spec_obj-> set, sp_chan_binning= 1
;spec_obj-> set, sp_chan_min= 0
;spec_obj-> set, sp_chan_max= 1000
;spec_obj-> set, sp_data_unit= 'Rate'

; To choose kev binning use the following                                                                                     
spec_obj-> set, sp_chan_binning= 0                                                                
spec_obj-> set, sp_chan_max= 0                                                                    
spec_obj-> set, sp_chan_min= 0                                                                    
spec_obj-> set, sp_data_unit= 'Flux'  
; To select energies from a modified code 22, uncomment the following:                                                           
;spec_obj-> set, sp_energy_binning= [1.0002D, 1.333D, 1.667D, 2.0D, 2.3339D, $ 
; 2.6670001D, 3.0D, 3.3339D, 3.667D, 4.0D, 4.333D, 4.667D, $       
; 5.0D, 5.3330002D, 5.6669998D, 6.0D, 6.3330002D, 6.6669998D, 7.0D, $       
; 7.3330002D, 7.6669998D, 8.0D, 8.3330002D, 8.6669998D, 9.0D, 9.3330002D, $       
; 9.6669998D, 10.0D, 10.333000D, 10.667000D, 11.0D, 11.333000D, 11.667000D, $       
; 12.0D, 12.333000D, 12.667000D, 13.0D, 13.333000D, 13.667000D, 14.0D, $       
; 14.333000D, 14.667000D, 15.0D, 16.0D, 17.0D, 18.0D, 19.0D, $       
; 20.0D, 21.0D, 22.0D, 23.0D, 24.0D, 25.0D, 26.0D, $       
; 27.0D, 28.0D, 29.0D, 30.0D, 31.0D, 32.0D, 33.0D, $       
; 34.0D, 35.0D, 36.0D, 37.0D, 38.0D, 39.0D, 40.0D, $       
; 41.0D, 42.0D, 43.0D, 44.0D, 45.0D, 46.0D, 47.0D, $       
; 48.0D, 49.0D, 50.0D, 55.0D, 60.0D, 65.0D, 70.0D, $       
; 75.0D, 80.0D, 85.0D, 90.0D, 95.0D, 100.0D, 110.0D, $       
; 120.0D, 130.0D, 140.0D, 150.0D, 160.0D, 170.0D, 180.0D, $       
; 190.0D, 200.0D, 210.0D, 220.0D, 230.0D, 240.0D, 250.0D, $       
; 260.0D, 270.0D, 280.0D, 290.0D, 300.0D]
                                 
; This will set the energy bins to 120 logarithmically spaced bins from 1 to 300, with .3 keV bins around 10 keV                                 
;nb = 120
;ii = indgen(nb)
;e0=1.0002 & e1=300.
;len = (alog10(e1) - alog10(e0)) / nb
;eb = transpose(10. ^ ( [ [alog10(e0) + ii*len], [alog10(e0) + (ii+1)*len] ] ))

e = 1.0002 + indgen(30)*.333
nb = 109
ii = indgen(nb)
e0=max(e) & e1 = 300
len = (alog10(e1) - alog10(e0)) / nb
eb = [e, 10.^(alog10(e0) + (ii+1)*len)]

spec_obj-> set, sp_energy_binning = eb
                         
spec_obj-> set, sp_semi_calibrated= 1B   ; Note - can't use semi-cal option with channel binning                                                         
spec_obj-> set, sp_time_interval=time_range      
spec_obj-> set, sum_flag= 0                                                                       
spec_obj-> set, time_range= [0.00D, 0.00D]                                            
spec_obj-> set, use_flare_xyoffset= 1                                                             
spec_obj-> set, sum_coincidence= 0 

start_time = (anytim (time_range[0], /vms, /trunc))[0]
desc_obs = 'HESSI Count Rate ' + start_time
desc_spec_photon = 'HESSI Photon Flux vs Energy ' + start_time
desc_spec_count_count = 'HESSI Counts vs Energy ' + start_time
desc_spec_count_rate = 'HESSI Count Rate vs Energy ' + start_time
desc_spec_count_flux = 'HESSI Count Flux vs Energy ' + start_time

if yes_gui then begin
  
  if ~no_obs then begin
    hsi_get_orbit_times,time_range, orb_start=os, orb_end=oe, count=oc, /overlap
    oti = [min(os), max(oe)]
    obs_obj->set,obs_time_interval = oti
    plotman_obj->new_panel, input=obs_obj, desc=desc_obs, class_name='hsi_obs_summ_rate', /dim1_enab_sum, $
        plot_type='utplot', /ylog,/night, /atten, /flare, $
        addplot_name='oplot_xmarkers', addplot_arg={intervals: time_range, ut: 1}
;    obs_obj->plotman, plotman_obj=plotman_obj, /ylog, desc=desc_obs, /night, /atten, /flare, $
;      addplot_name='oplot_xmarkers', addplot_arg={intervals: time_range, ut: 1}
  endif

  message, /cont, 'Accumulating...'

  spec_obj->plotman, /pl_energy, plotman_obj=plotman_obj, legend_loc=2, xrange=[1.,300.], $
    desc=desc_spec_photon, ytickformat='tick_label_exp'
  spec_obj-> set, sp_semi_calibrated= 0B
  spec_obj->plotman, /pl_energy, plotman_obj=plotman_obj, legend_loc=2, xrange=[1.,300.], $
    desc=desc_spec_count_count, sp_data_unit='count', ytickformat='tick_label_exp'
    spec_obj->plotman, /pl_energy, plotman_obj=plotman_obj, legend_loc=2, xrange=[1.,300.], $
    desc=desc_spec_count_rate, sp_data_unit='rate', ytickformat='tick_label_exp'
  spec_obj->plotman, /pl_energy, plotman_obj=plotman_obj, legend_loc=2, xrange=[1.,300.], $
    desc=desc_spec_count_flux, sp_data_unit='flux', ytickformat='tick_label_exp'
  
  
  ; set current obs time interval into overall time interval in hessi GUI
  hessi_set_new_time, time_range
  
endif

if screen or ps or png then begin

  spec_obj-> set, sp_semi_calibrated= 0B
  data = spec_obj->getdata(sp_data_struct=0)
  if data[0] eq -1 then begin
    error = 1
    goto, after_plot
  endif else error = 0
  
  col = indgen(9)+1
  col[0]=0
  !p.multi=[0,1,2]
  
  if ps or png then begin
   save_dev = !d.name 
    stime = time2file(time_range[0],/sec)
    etime = strmid(time2file(time_range[1],/sec), 9, 6)
    file_base = 'hsi_sepdet_spectrum_' + stime + 'to' + etime
  endif  
  
  case 1 of
    screen: begin
      !p.background = 1
      hsi_linecolors, /pastel
      tvlct, rr, gg, bb, /get
      rr[255]=0  &  gg[255]=0  &  bb[255]=0
      tvlct, rr, gg, bb
      !p.thick = 3
      charsize=1.3
      end
    ps: begin
      out_file = concat_dir(out_dir, file_base + '.ps')
      ps, out_file, /land, /color
      hsi_linecolors, /pastel
      !p.thick = 8
      !x.thick=2
      !y.thick=2
      !p.font=0
      charsize=1.2
      end
    png: begin
      out_file = concat_dir(out_dir, file_base + '.png')
      set_plot,'z'
      device, set_resolution = [640,600]
      hsi_linecolors, /pastel
      tvlct, rr, gg, bb, /get
      rr[255]=0  &  gg[255]=0  &  bb[255]=0
      tvlct, rr, gg, bb
      !p.background = 1 ; white in hsi_linecolors
      !p.thick = 3
      charsize=.9
      end
  endcase

  title = 'Count and Photon Spectra for RHESSI Front Segments'
  enmid = spec_obj->getaxis(/xaxis,/mean)
  
  ; To determine yrange for count plot, look at values greater than 0. (for simplicity set 0s to NaNs), and 
  ; look at data values within energy plot range.
  qnan = where(data le 0., knan)
  if knan gt 0 then data[qnan] = !values.f_nan
  qen = where(enmid gt 1. and enmid lt 100.)
  yrange = minmax(data[qen,*,*], /nan) > 1.e-3
  
  atten = spec_obj->get(/interval_atten_state)
  atten_label = 'Att A' + trim(fix(atten.state)) ; was byte - makes unprintable chars  
  atten_energy = fix(atten.state) eq 0 ? 3. : 6.
  
  decim_table = spec_obj->get(/decim_table)
  decim = decim_table.front.weight
  decim_label = 'FDecim ' + strcompress(arr2str(decim,','), /remove_all)
  
  spec_obj->plot, /pl_energy,  legend_loc=2, xrange=[1.,100.], /xst, dim1_color=col, charsize=charsize, $
    yrange=yrange, ymargin=[0,2], title=title, xtitle='', xtickname=strarr(30)+' ', ytickformat='tick_label_exp'
  oplot, [atten_energy,atten_energy], crange('y'), thick=1
  color_box, x=[1.,atten_energy],/line_fill, orient=45, spacing=1.5, thick=1
  yr = !y.crange  ; returns log of plotted y axis limits
  ypos = 10.^(yr[0] + (yr[1]-yr[0])/13.)
  xpos = atten_energy + (atten_energy lt 10 ? .3 : 3)
  xyouts, xpos, ypos, 'Includes Background!C' + atten_label + ', ' + decim_label, align=0, /data, charsize=charsize
   
  spec_obj-> set, sp_semi_calibrated= 1B
  data = spec_obj->getdata(sp_data_struct=0)
  ; To determine yrange for photon plot, look at values greater than 0. (for simplicity set 0s to NaNs), and 
  ; look at data values above the atten_energy cutoff. Make high y limit one decade bigger than max for energies
  ; in plot range above atten_energy, and use ystyle=1.
  qnan = where(data le 0., knan)
  if knan gt 0 then data[qnan] = !values.f_nan
  qen = where(enmid gt atten_energy and enmid lt 100.)
  yrange = minmax(data[qen,*,*], /nan) > 1.e-3
  yrange[1] = 10.*yrange[1]  
  spec_obj->plot, /pl_energy,  legend_loc=2, xrange=[1.,100.], /xst, dim1_color=col, charsize=charsize, $
    yrange=yrange, ymargin=[4,0], title='', /ystyle, ytickformat='tick_label_exp'
  oplot, [atten_energy,atten_energy], crange('y'), thick=1
  color_box, x=[1.,atten_energy],/line_fill, orient=45, spacing=1.5, thick=1
  xpos = atten_energy + (atten_energy lt 10 ? .3 : 3)
  yr = !y.crange  ; returns log of plotted y axis limits
  ypos = 10.^(yr[0] + (yr[1]-yr[0])/25.)
  xyouts, xpos, ypos, 'Semi-calibrated', align=0, /data, charsize=charsize

  xyouts, 0., .01, 'Generated by hsi_spectrum_check_dets.pro', charsize=1.*charsize, /norm, align=0.
  
  case 1 of 
    ps: psclose
    png: write_png, out_file, tvrd(), rr,gg,bb 
    else:        
  endcase
  
  after_plot:
  if error then begin
    message, /cont, 'Error accumulating data. No plot.' 
    out_file = ''
  endif else begin
    if ps or png then message, /cont, 'Wrote plot file ' + out_file
    cleanplot, /silent
    if ps or png then set_plot, save_dev
  endelse

endif
                                                                                                      
end                                                                                         
