FUNCTION hsi_vis_fwdfit_func, jdum, srcparm
  ;+
  ; Calculates expected visibilities for a source described by array, srcparm,
  ;    at specified u,v points.
  ; Uses common block, uvdata, to specify u,v values.
  ; By convention, it returns a 2*nvis element vector corresponding to all real then all imaginary components.
  ; Number of components is implied by size of srcparm array, which has 6 elements per component.
  ;
  ; jdum is an npt-element dummy vector, required by the AMOEBA_C conventions.
  ;
  ; 23-May-05     Initial version for a single circular gaussian. (ghurford@ssl.berkeley.edu)
  ;  6-Aug-05 gh  Add support for ellimptical gaussian.
  ;  9-Aug-05 gh  Add provision to avoid EXP underflows.
  ;               Revise handling of ellipse geometry
  ; 10-Aug-05 gh  Improve nomenclature
  ; 15-Aug-05 gh  Correct bug affecting projected cross-section of ellipse
  ;               Add provision to avoid underflows at low but nonzero eccentricity
  ; 24-Aug-05 gh  Generalize to support multicomponent sources
  ; 15-Oct-05 gh  Adapt to support 'Cartesian' representation of source ellipticity
  ;  9-Dec-05 gh  Adapt to revised format of source parameter vector.
  ;               Add support for loop sources
  ; 16-Jan-06 gh  Add support for albedo.
  ; 10-Jan-2017, Kim. Ensure that eccen is 0. for circle components
  ; 24-Jul-2017, Kim. Remove restriction on only one loop allowed (and no other sources)
  ; 01-Sep-2017, Kim. Add ability to correct select sources for albedo. Added alb_apply_index_orig to common -
  ;                   tells us which of the original sources to apply albedo to. 
  ;                   Separate albedo source from primary sources, keep track of which primary sources to
  ;                   apply albedo to even while turning loop into multiple circles.
  ; 19-Sep-2017, Kim. Error in ellipse computation corrected (thanks to Pascal Saint-Hilaire)
  ; 26-Sep-2017, RAS. Included Pascal's source material for ellipse computation
  ;-
  COMMON uvdata, u, v, pa, mapcenter, alb_apply_index_orig
  
;  if srcparm[13] gt -16. then stop    ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  alb_apply_index = alb_apply_index_orig ; protect original value in common
  
  TWOPI           = 2.*!PI
  CONSTANT        = -!PI^2/(4*ALOG(2.))
  ALBCONSTANT     = -0.5 * SQRT(ABS(constant))
  nvis            = N_ELEMENTS(jdum)/2
  obs             = FLTARR(nvis*2)
  ;
  nsrc            = N_ELEMENTS(srcparm)/10
  parmarray       = REFORM(srcparm, 10, nsrc)               ; need a 2-d structure for generality
  
  albedo_index = WHERE(parmarray[0,*] EQ 4, yes_albedo, compl=iprimary, ncompl=nprimary)    ; yes_albedo=0 if there is no albedo, albedo_index is index of the albedo component, if any
  IF yes_albedo GT 1 THEN MESSAGE, ' By convention, there should only be 1 albedo source component.'
  if nprimary eq 0 then message, ' Must have at least one non-albedo source.'

  parm_alb = parmarray[*, albedo_index]
  parmarray = parmarray[*, iprimary]
  nsrc = n_elements(parmarray[0,*])

          ;; Replace loop parameters with those of a set of circular sources.   -- LOOP MUST BE THE ONLY SOURCE
          ;IF srcparm[0] EQ 3 THEN BEGIN
          ;    ellstr      = hsi_vis_fwdfit_array2structure(srcparm, [0,0])  ; mapcenter is arbitrarily set to 0
          ;    ellstr.srctype = 'ellipse'
          ;    loopstr     = hsi_vis_fwdfit_makealoop(ellstr)
          ;    parmarray   = hsi_vis_fwdfit_structure2array(loopstr, [0,0])    ; same arbitrary mapcenter is taken out again
          ;    nsrc        = N_ELEMENTS(parmarray)/10
          ;    parmarray   = REFORM(parmarray,10,nsrc)
          ;ENDIF
          ;
  ; Replaced the above loop calculation with this.  Now can have more than one loop. First find which
  ; sources are loops. Save them in parm_loop, and save the non-loop ones in parmuse. Then for each
  ; loop, call makealoop to make the multiple circles for the loop - those will be in parmarray_mcirc, append
  ; that array to parmuse, and then when done with all loops, reform back to [10,nsrc], where nsrc is now
  ; the sum of the non-loop sources plus the number of circles used to make up the loop source(s). Keep track
  ; as we're going of which sources should have the albedo correction.
  ; This can be done this way because it doesn't matter what order the sources in parmarray are (i.e. don't have 
  ; to maintain user's order).
  iloop = where(parmarray[0,*] EQ 3, compl=inotloop, ncompl=nnotloop, nloop)
  if nloop gt 0 then begin
    parm_loop = parmarray[*,iloop]
    if nnotloop gt 0 then parmuse = (parmarray[*,inotloop])[*] ; make into a 1-d array
    alb_apply_index = where(is_member(inotloop, alb_apply_index_orig), kq) ; alb_apply_index will contain indices of parmuse to apply albedo to
    loop_alb = is_member(iloop, alb_apply_index_orig)
    nsrc = nnotloop
    for i = 0,nloop-1 do begin
      loopstr      = hsi_vis_fwdfit_array2structure(parm_loop[*,i], [0,0])  ; mapcenter is arbitrarily set to 0
      mcircstr     = hsi_vis_fwdfit_makealoop(loopstr)
      parmarray_mcirc   = hsi_vis_fwdfit_structure2array(mcircstr, [0,0])    ; same arbitrary mapcenter is taken out again
      parmuse = append_arr(parmuse, parmarray_mcirc)
      if loop_alb[i] then alb_apply_index = [alb_apply_index, i]
      nsrc = nsrc + n_elements(mcircstr)
    endfor
    parmarray = reform(parmuse, 10, nsrc)
  endif
  
  yes_albedo = yes_albedo and (max(alb_apply_index) ne -1)  ; Ensure that yes_albedo is 1 only if there are also sources to apply it to

  ; If correcting for albedo, calculate albedo phase and position angle for modifying sources

  IF yes_albedo THEN BEGIN
    Megam2arcsec    = 1.4                             ; nominal Mm to arcsec conversion factor
    solradius       = 960.                              ; nominal
    albratio        = parm_alb[8]
    height          = parm_alb[9] * Megam2arcsec
    xashift         = -((mapcenter[0]/solradius < 1.) > (-1.)) * height        ; expected shift in albedo centroid from primary centroid (note - sign)
    yashift         = -((mapcenter[1]/solradius < 1.) > (-1.)) * height
    relalbphase     = TWOPI * (u*xashift[0] + v*yashift[0])                    ; nvis element array of albedo phase relative to primary phase (radians)
    albpa           = ATAN(mapcenter[1], mapcenter[0])                        ; angle of long axis, measured E of N (radians)
  ENDIF
  ;
  ; Begin loop over primary source components
  FOR n = 0, nsrc-1 DO BEGIN              ; loop over non-albedo components
    flux            = parmarray[1,n]
    srcx            = parmarray[2,n]
    srcy            = parmarray[3,n]
    srcfwhm         = parmarray[4,n]
    eccos           = parmarray[5,n]
    ecsin           = parmarray[6,n]
    ecmsr           = SQRT(eccos^2 + ecsin^2)
    eccen           = parmarray[0,n] eq 1 ? 0. : SQRT(1 - EXP(-2*ecmsr)) ; for circle, set eccen to 0.
    IF eccen GT 0.001 THEN srcpa = ATAN(ecsin, eccos) * !RADEG ELSE srcpa = 0  ; PA of long axis, relative to solar N(deg)
    phase           = TWOPI * (u*srcx + v*srcy)


    ; Ellipse calculations
    IF eccen LT 0.001 THEN eccen = 0             ; avoids underflows in calculating eccen^2
    
    ; Replaced the following 5 lines with the next 4 lines 19-Sep-2017. Ellipse calculation was wrong - was computing a 
    ; dumbell shape instead of an ellipse. Pascal found this error back in 2012, but we only implemented it in 2017. 
          ;    relpa           =  !pi/2. - (srcpa - pa) * !DTOR          ; PA of long axis, rel. to uv point PA (radians)
              ; Note that spatial resolution is orthogonal to uv PA !!    
          ;    b =  srcfwhm * (1.-eccen^2)^0.25
          ;    fwhmeff2        = b^2 / (1 - (eccen*COS(!PI/2 - relpa))^2)         ; nvis-element vector
          ;    term            = CONSTANT * (u^2 + v^2) * fwhmeff2 > (-20)           ; set a lower bound to avoid underflows
    ;
    ; Reference for the Fourier Transform of the elliptical Gaussian and the representation
    ;For FT of an ellipse (into an ellipse in Fourier space):
    ;https://en.wikipedia.org/wiki/Fourier_transform
    ;See table towards the end for 2-D gaussians -- use the "unitary, ordinary frequency" column.
    ;
    ;For ellipse business:
    ;https://en.wikipedia.org/wiki/Ellipse

    uv_b= 1./srcfwhm * (1.-eccen^2)^0.25
    relpa  =  (!pi/2. - (srcpa - pa)) * !DTOR ; PA of long axis, rel. to uv point PA (radians), note: orthogonal to uv PA !!
    fwhmeff2= uv_b^2 / (1 - (eccen*COS(relpa))^2)
    term  = CONSTANT * (u^2 + v^2) / fwhmeff2 > (-20)
    
    
    relvis          = EXP(term)                                       ; nvis-element vector
    obs[0:nvis-1]   = obs[0:nvis-1] + flux * relvis * COS(phase)    ; Each component is added to previous sum
    obs[nvis:*]     = obs[nvis:*]   + flux * relvis * SIN(phase)

    ; If requested, add a convolved albedo visibility, implemented as the product of the primary and albedo relative visibilities
    IF yes_albedo && is_member(n, alb_apply_index) THEN BEGIN
      relalbpa        = albpa - pa*!DTOR                                ; Albedo pa relative to uv point PA (radians)
      albscale        = height[0] * ABS(COS(!PI/2-relalbpa))                    ; Effective scale of albedo in direction of resolution for each uv point
      term            = albconstant * SQRT(u^2+v^2) * albscale > (-20)
      relalbvis       = EXP(term)
      obs[0:nvis-1]   = obs[0:nvis-1] + flux*relvis*albratio[0]*relalbvis * COS(phase+relalbphase)
      obs[nvis:*]     = obs[nvis:*]   + flux*relvis*albratio[0]*relalbvis * SIN(phase+relalbphase)
    ENDIF
  ENDFOR
  
  RETURN, obs
END