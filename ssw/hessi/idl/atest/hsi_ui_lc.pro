;+
; Name: hsi_ui_lc
;
; Purpose: Widget interface to HESSI lightcurve object
;
; Written: Kim Tolbert, 2000
; Modifications:
;   11-Mar-2001.  Enabled plotting multiple detectors for one energy band
;   24-Mar-2001.  Call hsi_get_time range instead of hsi_get_image_abs_time
;	1-May-2001. Don't need to set params in plotman obj after call to hsi_do_plotman
;	9-Sep-2001, make w_sum a keyword in call to hsi_coll_list_widget
;   29-Dec-2001, make time res a pulldown in lc widget intead of just displaying it and having
;      a 'change' button that brings up the lcmisc widget
;	30-Sep-2002, Kim.  In reset_times event, check if flarenumber changed, not whole structure
;	   (match_struct returns 0 if there are Nan's in structure on Unix)
;-

pro hsi_ui_lc_getparams, obj, params

;print,obj -> get(/det_index)
;print,obj -> get(/seg_index)

; if det_index_mask is set, then use it and set both segments for any selected colls on
; Otherwise use seg_index_mask, but if seg_index_mask is all zeros, then set all det/segs on.
det = obj -> get(/det_index)
if total(det) ne 0 then obj -> set, seg_index = det # [1,1] else begin
	seg = obj -> get(/seg_index)
	if total(seg) eq 0 then obj -> set, seg_index=intarr(18)+1
endelse

control = obj -> get (/control_only)
info = obj -> get(/info)

;control.time_range = control.obs_time_interval

params = { $
	control: control, $
	info: info}

end

;-----

pro hsi_ui_lc_widget_update, state

params = state.params
w = state.widgets

widget_control, w.w_obs_time_label0, set_value=state.obs_time_label[0]
widget_control, w.w_obs_time_label1, set_value=state.obs_time_label[1]

stime = anytim (hsi_get_time_range (params.control, params.info), /vms)
value = 'Lightcurve Time Interval: ' + stime[0] + '  -  ' + stime[1]
widget_control, w.w_time, set_value=value

fm = '(f9.1)'
ltc_en = params.control.ltc_energy_band
if size(ltc_en, /n_dimensions) eq 1 then begin
	nen = n_elements(ltc_en)
	ltc_en = transpose([ [ltc_en[0:nen-2]], [ltc_en[1:nen-1]] ])
endif
senergy = strtrim (string (ltc_en, format=fm), 2)
senergy = reform(senergy, size(ltc_en, /dim))
value = senergy[0,*] + '  -  ' + senergy[1,*]
selected = widget_info (w.w_energy, /droplist_select)
numlist = widget_info (w.w_energy, /droplist_number)
widget_control, w.w_energy, set_value=value
if numlist eq n_elements(value) then set = selected else set = 0
widget_control, w.w_energy, set_droplist_select=set
widget_control, w.w_nenergy, set_value='# Bands: ' + strtrim(n_elements(value), 2)

segs = reform(params.control.seg_index_mask, 9, 2)
colls = total (segs, 2) gt 0

for ic = 0,8 do begin
	widget_control, w.w_colldisp[ic,0], sensitive=colls[ic]
	widget_control, w.w_colldisp[ic,1], sensitive=segs[ic,0]
	widget_control, w.w_colldisp[ic,2], sensitive=segs[ic,1]
endfor

enab = ['Disabled (only first energy band will be used)', 'Enabled']
widget_control, w.w_sum, set_value='Sum Detectors: ' + enab[params.control.sum_flag]

stimeres = strtrim (string (params.control.ltc_time_resolution, format='(f10.3)'), 2)
time_range = anytim(stime)
nbins = (time_range[1] - time_range[0]) / params.control.ltc_time_resolution
snbins = strtrim (string (nbins, format='(i6)'), 2)
value = '       # Bins = ' + snbins
widget_control, w.w_nbins, set_value=value

widget_control, w.w_semical, set_button=params.control.sp_semi_calibrated

sel = where (state.flags_obj -> get(/selected) eq 1, count)
codes = state.flags_obj -> get(/codes)
val = count gt 0 ? arr2str(codes[sel],', ') : 'None'
widget_control, w.w_flags, set_value='Show Flags: ' + val

end

;-----

pro hsi_ui_lc_event, event

widget_control, event.top, get_uvalue=state

; check if user clicked X in top right corner of window, and if so quit

if tag_names(event,/struc) eq 'WIDGET_KILL_REQUEST' then goto, exit

if tag_names(event,/struc) ne 'WIDGET_TIMER' then begin
	;in case user changed any parameters from command line, make sure we're up to date
	hsi_ui_lc_getparams, state.data_obj, params
	state = rep_tag_value (state, params, 'params')
	; clear any error messages in message widget
	if xalive(state.w_message) then widget_control, state.w_message, set_value=''
endif

widget_control, event.id, get_uvalue=uvalue

_extra = -1
chg_msg = ''

case uvalue of

	'reset_times': begin
		widget_control,	state.widgets.w_obs_time_label0, timer = 3.
		if same_data(state.obs_time_interval, *state.ptr_obs_time_interval) and $
			same_data (state.flare_str.id_number, (*state.ptr_flare_str).id_number) then return
		obs = *state.ptr_obs_time_interval
		state.data_obj -> set, obs_time_interval = obs
		state.data_obj -> set, time_range = obs - obs[0]
		state.obs_time_interval = obs
		state.flare_str = *state.ptr_flare_str
		state.obs_time_label = hsi_ui_time_label (state.obs_time_interval, state.flare_str, /twolines)
		hsi_ui_lc_getparams, state.data_obj, params
		state = rep_tag_value (state, params, 'params')
		hsi_ui_lc_widget_update, state
		end


	'manoptions': begin
		answer = dialog_message (['Caution!  This option is for experienced users only.', $
			'Setting parameters wrong could make the object unusable.', $
			'', 'Do you want to continue?'], $
			/question, title='Warning!')
		if strlowcase(answer) eq 'yes' then begin
			widget_control, /hourglass
			control = state.params.control
			xstruct, control, $
						/edit, $
						group=event.top, $
						title='Lightcurve Control Parameters', $
						nx=4, xsize=10, /center, $
						/modal, $
						status=status
			if status then _extra=control
		endif
		end

	'defstandard': begin
		widget_control, /hourglass
		temp_obj = obj_new('hsi_lightcurve')
		_extra = temp_obj -> get(/control)
		; don't set times to default. Set to times when we entered spectrum widget.
		if tag_exist(_extra,'obs_time_interval') then begin
			obs = state.params.control.obs_time_interval
			_extra.obs_time_interval = obs
			_extra.time_range = obs - obs[0]
		endif
		obj_destroy, temp_obj
		end

	'changetime': begin
		obs = state.params.control.obs_time_interval
		new_vals = hsi_time_widget (event.top, state.params.control.time_range[*,0]+obs[0], $
			valid_range=obs, chg_msg=chg_msg)
		_extra = {time_range: new_vals[*,0] - obs[0]}
		end

	'changeenergy': begin
		obs = state.params.control.obs_time_interval
		new_vals = hsi_sel_intervals (group=event.top, $
			input_intervals=state.data_obj -> getaxis(/yaxis, /edges_2), $
			/energy, $
			msg_label = 'Choose energy bands to create lightcurve for.', $
			valid_range=[1.,100000.], obs_time_interval=obs, chg_msg=chg_msg, $
			plotman_obj=state.plotman_obj, $
			stored_intervals_obj=state.intervals_obj)
		_extra = {ltc_energy_band: new_vals}
		a=xregistered('hsi_ui_lc')
		end

	'changecoll': begin
		new_vals = hsi_coll_widget (state.params, group=event.top, /notime, /noharm, chg_msg=chg_msg)
		_extra = new_vals
		end

	'time_res': _extra = {ltc_time_resolution: event.value}

	'semical': _extra = {sp_semi_calibrated: event.select}

	'change_flags':  state.flags_obj -> select_widget, group=event.top

	'params': state.data_obj -> params

	'doplot': begin
		start = (anytim (hsi_get_time_range (state.params.control, state.params.info), /vms, /trunc))[0]
		hsi_do_plotman, state, tagname='data_obj', group=event.top, plot_type='utplot', $
			desc='HESSI Lightcurve ' + start, show_flags_obj=state.flags_obj
		end

	'stop':  begin
		obj = state.data_obj
		stop
		end

	'help': gui_help, 'gui_lightcurve_help.htm'

	'close':  goto, exit

	else:
endcase

if chg_msg[0] ne '' then ok = dialog_message(chg_msg, title='Warning message')

if size(_extra, /tname) eq 'STRUCT' then begin
	state.data_obj -> set, _extra=_extra
	hsi_ui_lc_getparams, state.data_obj, params
	state = rep_tag_value (state, params, 'params')
endif

if xalive(event.top) then begin
	hsi_ui_lc_widget_update, state
	widget_control, event.top, set_uvalue=state
endif

return

exit:

	widget_control, event.top, /destroy
	if state.standalone then free_var, state, exclude=['data_obj','data']
	return

end

;-------------------


pro hsi_ui_lc, input_obj, $
	obs_time_interval=ptr_obs_time_interval, $
	flare_str=ptr_flare_str, $
	intervals_obj=intervals_obj, $
	group=group, $
	error=error

error = 0

if xregistered ('hsi_ui_lc') then begin
	xshow,'hsi_ui_lc', /name
	return
endif

if keyword_set(ptr_obs_time_interval) then obs_time_interval = *ptr_obs_time_interval
if keyword_set(ptr_flare_str) then flare_str = *ptr_flare_str

if not hsi_chk_obj ( input_obj, 'HSI_LIGHTCURVE', data_obj, obs_time_interval=obs_time_interval) then return

hsi_ui_lc_getparams, data_obj, params

obs_time_label = hsi_ui_time_label (obs_time_interval, flare_str, /twolines)

flags_obj = hsi_show_flags()

space = 5
ypad = 5
xpad = 5

hsi_ui_getfont, font, big_font

widget_control, default_font = font


; if standalone, position at 50,50.  If not, after create base, offset it from parent
; before realizing it.
xoffset = 50  & yoffset = 50

w_top_base = widget_base (group=group, /column, $
					title=hsi_widget_title('Lightcurves'), $
					xoffset=xoffset, $
					yoffset=yoffset, $
					/tlb_kill )

w_base = widget_base ( w_top_base, $
					/column, $
					/frame, $
					space=space, $
					xpad=xpad, $
					ypad=ypad )

w = widget_label( w_base, $
					value='LIGHTCURVES', $
					/align_center, $
					font=big_font )

w_obs_time_label0 = widget_label (w_base, $
					value=obs_time_label[0], uvalue='reset_times', /align_left)
w_obs_time_label1 = widget_label (w_base, $
					value=obs_time_label[1], uvalue='reset_times', /align_left)

w = widget_label(w_base, value='  ')

w_time_base = widget_base (w_base, /row, /frame, space=20)
w_time = widget_label (w_time_base, value='', /dynamic_resize)
tmp = widget_button (w_time_base, value='Change...', uvalue='changetime')

w_res_base = widget_base (w_base, /row, space=20, /frame)
w_time_res = cw_edroplist ( w_res_base, $
					value=params.control.ltc_time_resolution, $
					format='(f12.3)', $
					xsize=7, $
					label='Time Resolution (s): ', $
					drop_values=[.01, .062, .1, .125, .2, .25, .5, 1., 2., 4.], $
					uvalue='time_res' )
w_nbins = widget_label (w_res_base, value='', /dynamic_resize, /align_left)

w_energy_base = widget_base (w_base, /row, /frame, space=15)
w_energy = widget_droplist (w_energy_base, $
	title='Energy Bands (keV): ', $
	/dynamic_resize, $
	value='', uvalue='energylist')
w_nenergy = widget_label (w_energy_base, $
	value='# Bands: 1', $
	/dynamic_resize)
tmp = widget_button (w_energy_base, value='Change...', uvalue='changeenergy')

hsi_coll_list_widget, w_base, w_colldisp, w_sum=w_sum

w_sf_base = widget_base (w_base, /row, /frame, space=20)
w_semical_base = widget_base (w_sf_base, /row, /nonexclusive)
w_semical = widget_button (w_semical_base, value='Use Semi-calibrated Data', uvalue='semical')
w_flag_base = widget_base (w_sf_base, /row, /frame, space=10)
w_flags = widget_label (w_flag_base, value='Show Flags:  None', /dynamic_resize)
tmp = widget_button (w_flag_base, value='Change', uvalue='change_flags')

w_button_base1 = widget_base ( w_base, 	/row, 	/align_center, 	space=15 )

temp = widget_button (w_button_base1, value='Parameter Summary', uvalue='params')
temp = widget_button (w_button_base1, value='Reset to defaults', uvalue='defstandard')
temp = widget_button (w_button_base1, value='Set params manually', uvalue='manoptions')

w_button_base2 = widget_base ( w_base, 	/row, 	/align_center, 	space=15 )

tmp = widget_button (w_button_base2, value='Plot Lightcurve', uvalue='doplot' )
tmp = widget_button (w_button_base2, value='Help', uvalue='help' )
tmp = widget_button (w_button_base2, value='Close', uvalue='close' )

valid_plotman = 0
if xalive(group) then begin
	widget_control, group, get_uvalue=mw_state
	if size(mw_state,/tname) eq 'STRUCT' then begin
		if tag_exist(mw_state,'plotman_obj') then begin
			plotman_obj = mw_state.plotman_obj
			w_message = mw_state.widgets.w_message
			valid_plotman = 1
		endif
	endif
endif
if not valid_plotman then plotman_obj = obj_new()

; message widget id was defined if we already had a plotman object defined.  Otherwise,set to 0.
; Check if xalive before using it.
checkvar, w_message, 0L

widgets = {w_top_base: w_top_base, $
	w_obs_time_label0: w_obs_time_label0, $
	w_obs_time_label1: w_obs_time_label1, $
	w_time: w_time, $
	w_energy: w_energy, $
	w_nenergy: w_nenergy, $
	w_colldisp: w_colldisp, $
	w_sum: w_sum, $
	w_time_res: w_time_res, $
	w_nbins: w_nbins, $
	w_semical: w_semical, $
	w_flags: w_flags}

state = {data_obj: data_obj, $
		obs_time_label: obs_time_label, $
		obs_time_interval: obs_time_interval, $
		flare_str: flare_str, $
		ptr_obs_time_interval: ptr_obs_time_interval, $
		ptr_flare_str: ptr_flare_str, $
		intervals_obj: intervals_obj, $
		standalone: xalive(group) ne 1, $
		params: params, $
		widgets:widgets, $
		plotman_obj: plotman_obj, $
		flags_obj: flags_obj, $
		w_message: w_message }

hsi_ui_lc_widget_update, state

if xalive(group) then begin
	widget_offset, group, xoffset, yoffset, newbase=w_top_base
	widget_control, w_top_base, xoffset=xoffset, yoffset=yoffset
endif

widget_control, w_top_base, /realize, /hourglass

widget_control, w_obs_time_label0, timer=0.

widget_control, w_top_base, set_uvalue=state

xmanager, 'hsi_ui_lc', w_top_base, /no_block

end
