;+
; PROJECT:
;	HESSI
;
; NAME:
;   hsi_vis_fwdfit
;
; PURPOSE:
;   RHESSI forward fit imaging algorithm based on visibilities
;
; CATEGORY:
;   hessi/idl/image/visibility/gh
;
; CALLING SEQUENCE:
;   hsi_vis_fwdfit, visin0 [, NOPHASE=nophase]     [, CIRCLE=circle]    [, MAXITER = maxiter] [, ABSOLUTE=absolute]   $
;                          [, NOERR=noerr]         [, SRCIN = srcstrin] [, SRCOUT=srcstrout]  [, MULTI=multi]         $
;                          [, FITSTDDEV=fitstddev] [, LOOP=loop]        [, SHOWMAP=showmap]   [, NOPLOTFIT=noplotfit] $
;                          [, QFLAG=qflag]         [, ALBEDO=albedo]    [, SYSERR=syserr]     [, NOEDIT=noedit] $
;                          [, _EXTRA=extra]
;
; CALLS:
;   hsi_vis_select                      [to calculate position angles of uv points]
;   hsi_vis_fwdfit_fixedconfig          [to optimize parameters for a given source configuration]
;   hsi_vis_fwdfit_func                 [to calculate model visibilities for a given set of source parameters]
;   hsi_vis_fwdfit_plotfit              [to generate plotted display of fit]
;   hsi_vis_fwdfit_print                [Generates printed display of source structure]
;   hsi_vis_structure2array             [Converts source structure to an array for amoeba_c]
;   hsi_vis_src_structure__define       [Defines source structure format]
;
; INPUTS:
;   visin0 = an array of visibiltiy structures, each of which is a single visibility measurement.
;               visin0 is not modified by hsi_vis_fwdfit
;
; OPTIONAL INPUTS:
;	See keywords.
;
; OUTPUTS:
;   Prints fit parameters in log window.
;
; OPTIONAL OUTPUTS:
;   See keywords.
;
; KEYWORDS:
;   /CIRCLE = fits visibilities to a single, circular gaussian.  (Default is an elliptical gaussian)
;   /LOOP   = Fits visibilities to a single curved elliptical gaussian.
;   /MULTI  = fits visibilities to a pair of circular gaussians.
;   /ALBEDO = adds a combined albedo source to the other fitted components. (Not yet fully reliable.)
;   SRCIN   = specifies an array of source structures, (one for each source component) to use as a starting point.
;
;   /NOERR forces fit to ignore input statistical errors. (Default is to use statistical errors.)
;   SYSERR is an estimate of the systematic errors, expressed as a fraction of the amplitude. Default = 0.05
;
;   /NOFIT just creates the uvdat COMMON block but suppresses all other outputs.  No fitting is done.
;   /NOEDIT suppresses the default editing and coonjugate combining of the input visibilities.
;   /NOPHASE forces all input phases to zero.
;   /ABSOLUTE generates fit by minimizing the sum of ABS(input visibility - model visibility). Default = 0
;   MAXITER sets maximum number of iterations per stage (default = 2000)
;
;   SRCOUT names a source structure array to receive the fitted source parameters.
;   FITSTDDEV returns sigma in fitted quantities in SRCOUT.
;   QFLAG returns a quality flag whose bits indicate the type of problem found.  qflag=0 ==> fit appears ok.
;   REDCHISQ names a variable to receive the reduced chi^2 of fit.
;   NITER returns number of iterations done in fit.
;   /NOPLOTFIT suppresses plotfit display.    Default is to generate this display.
;   /SHOWMAP generates a PLOTMAN display of final map
;   /PLOTMAN uses the PLOT_MAP routine instead of plotman to display the final map if /SHOWMAP is set.
;   ID = a character string used to label plots.  (Start time is always shown.)
;   FIT_MASK = 10 element array.  0/1 means fix or fit corresponding element in src structure. (Removed 21-jul-2017)
;
; _EXTRA keyword causes inheritance of additional keywords
;
; COMMON BLOCKS:
;	uvdata
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;   hsi_vis_fwdfit is still under development - should be used with caution and results reviewed critically.
;   Chi^2 output values are indeterminate if either /ABSOLUTE or /NOERR is set.
;   Bad fits are usually flagged with a warning message.
;
; MODIFICATION HISTORY:
; 23-May-05         Initial version (ghurford@ssl.berkeley.edu) assumes one circular gaussian
; 12-Jun-05 gh      First running version.
;                   Added plot of fitted vs observed amplitudes.
;                   Added /NOPHASE keyword
; 11-Jul-05 gh      Continue debugging.  Single source flux and diameter are ok.
;  1-Aug-05 gh      Major rewrite to use new {hsi_vis} visibility structure as input.
;  5-Aug-05 gh      Explicitly set output to 1 plot per page.
;                   Set initial step size to more reasonable values
;                   Suppress detailed printout from AMOEBA_C
;                   Improve plotted output
;                   Seems ok for locating and characterizing a single gaussian
;  6-Aug-05 gh      Improve printed output.
;                   Add support for elliptical gaussians
;                   Add /CIRCLE switch to force circular sources
;  9-Aug-05 gh      Fit circular source, then adapt to elliptical shape.
; 10-Aug-05 gh      Improve default elliptical step size
;                   Improve nomenclature and labels.
; 15-Aug-05 gh      Add MAXITER keyword
; 18-Sep-05 gh      Adapt to simplified visibility format.
; 22-Sep-05 gh      Add ABSOLUTE keyword
;                   Improve choice of parameter limits.
;                   Display reduced CHI^2.
; 27-Sep-05 gh      Add NOERR keyword.  (Default is to use statistical errors.)
; 15-Oct-05 gh      Use a 'Cartesian' representation to define ellipticity parameters.
;                   Fix display bug for PA > 180.
; 06-Nov-05 gh      Break out fitting to a fixed configuration by using module, hsi_vis_fwdfit_fixedconfig
;                   Set fitting accuracy to SQRT(machine tolerance)
;                   Fit elliptical sources in a single step, rather than doing a circular source first
; 07-Nov-05 gh      Add SRCIN keyword
;                   Break out printing display to hsi_vis_fwdfit_print
;                   Generalize code for multiple sources
; 08-Nov-05 gh      Add SRCOUT keyword
; 09-Nov-05 gh      Break out hsi_vis_structure2array
; 10-Nov-05 gh      Add /MULTI keyword
; 13-Nov-05 gh      Add srctype tag to source component structure
;                   Change xyoffset tag in source component structure to be sun center instead of map center
; 20-Nov-05 gh      Pass maxiter on to hsi_vis_fwdfit_fixedconfig
;                   Use average instead of maximum observed amplitude as initial flux guess.
; 21-Nov-05 gh      Add FITSTDDEV keyword
;  9-Dec-05 gh      Adapt to source structure definition as documented 9Dec05
; 12-Dec-05 gh      Add /LOOP keyword
;                   Add /SHOWMAP keyword
; 13-Dec-05 gh      Eliminate displays of interim fit results
;                   Add /NOPLOTFIT keyword to suppress plot display of fit.
;                   Check input visibilities for consistent times, energies and xyoffsets.
; 14-Dec-05 gh      Print time and energy ranges.
; 15-Dec-05 gh      Display fit quality warnings.
;                   Add QFLAG keyword
; 14-Jan-06 gh      Add mapcenter to uvdata common block
; 16-Jan-05 gh      Add ALBEDO keyword.
; 18-Jan-06 gh      Change default maxiter from 2000 to 500, in view of successful 'fresh start' strategy with amoeba_c
; 16-Feb-06 gh      Combine any conjugate input visibilities.
;                   Add SYSERR keyword parameter to incorporate a rough estimate of systematic errors.
;  7-Mar-06 gh      Add /NOEDIT keyword, but default to pre-editing visibility data.
;  8-Mar-06 gh      Correct bug which bypassed time-consistency check.
;           gh/psh  Add time to SHOWMAP call to get correct limb position
; 28-Mar-06 ejs/gh  Reimplement EJS's changes which added REDCHISQ, NOFIT and _EXTRA keywords.
; 24-May-06 ejs     Put keyword NOFIT into argument list
; 19-Jun-2008, Kim  Add redchisq, niter, nfree to keyword arguments. Changed order of keywords to group input/output.
;                   Also, even if noerr is set, assign all 0s to fitstddev structure so it's defined.
;                   Added nfree to calling args for hsi_vis_fwdfit_fixedconfig
; 18-Jan-08 Kim     Added fit_mask keyword to control which params to fit
; 30-Jun-08 Kim     Added fit_mask keyword.  THIS IS TEMPORARY until a more complete solution to fit params is implemented.
; 22-Jan-09 Kim     Construct srcparm_mask for multiple source input
;  4-May-09 Kim     Call with _REF_EXTRA instead of _EXTRA so plot routine can return window ID it created
;  6-Aug-13 Kim     Clean up code - changed keyword_set(xx) ne 0 to just keyword_set(xx), and instead of using
;                   variables names srcstr1,2,3, just always use srcstr.
; 28-Dec-16 Kim     Added derived_parms keyword (to pass out loop parameters)
; 06-Jul-17 Kim     Create source structure using procedure ( hsi_vis_src_structure()) instead of just 
;                   structure definition to handle initializing new fitmask to all 1s. Removed fit_mask 
;                   keyword and its use since now we use the fitmask that's in the source structure.
;-


PRO hsi_vis_fwdfit, visin0, $
  SRCIN = srcstrin, MULTI=multi, CIRCLE=circle, LOOP=loop, ALBEDO=albedo, $
  NOPHASE=nophase, MAXITER = maxiter, ABSOLUTE=absolute, SYSERR=syserr, $
  NOEDIT=noedit, NOERR=noerr, NOFIT=nofit, $
  SHOWMAP=showmap, NOPLOTFIT=noplotfit, $
  SRCOUT=srcstrout, FITSTDDEV=fitstddev, QFLAG=qflag, REDCHISQ=redchisq, NITER=niter, NFREE=nfree, $
  derived_parms=derived_parms, $
  _REF_EXTRA=extra
  ;
  ; Preset constants and masks
  TWOPI       =  2. * !PI
  DEFAULT, noerr,     0
  DEFAULT, maxiter,  500
  DEFAULT, syserr,    0.05
  DEFAULT, noedit,    0

  point_mask  = [0,1,1,1,0, 0,0,0,0,0]
  circ_mask   = [0,1,1,1,1, 0,0,0,0,0]
  ellip_mask  = [0,1,1,1,1, 1,1,0,0,0]
  loop_mask   = [0,1,1,1,1, 1,1,1,0,0]
  albedo_mask = [0,0,0,0,0, 0,0,0,1,1]
  ;
  ; Verify that all visibilities have the same xyoffset, energy and time ranges.
  nvisin0 = N_ELEMENTS(visin0)
  IF nvisin0 EQ 0 THEN MESSAGE, 'STOPPING: Input visibility structure is empty.'
  dummy = WHERE(visin0.xyoffset NE visin0[0].xyoffset, ndiff)
  IF ndiff GT 0 THEN MESSAGE,             'STOPPING: INPUT VISIBILITIES HAVE INCONSISTENT XYOFFSETS'
  dummy = WHERE(visin0.erange NE visin0[0].erange, ndiff)
  IF ndiff GT 0 THEN PRINT,               'CAUTION:  INPUT VISIBILITIES HAVE INCONSISTENT ENERGY RANGES'
  dummy = WHERE(visin0.trange NE visin0[0].trange, ndiff)
  IF ndiff GT 0 THEN PRINT,               'CAUTION:  INPUT VISIBILITIES HAVE INCONSISTENT TIME RANGES'
  ;
  ; Do a default edit and then combine conjugate visibilities, if necessary.
  IF noedit EQ 0 THEN visin = hsi_vis_combine(hsi_vis_edit(visin0), /CONJ) $
  ELSE visin = visin0
  nvis    = N_ELEMENTS(visin)
  IF nvis NE nvisin0 THEN PRINT, nvisin0, nvis, FORMAT="('Combined ', I4, ' conjugate visibilities into', I4)"
  ;
  ; Adjust the input visibility errors to include a systematic term.
  visamp          = ABS(visin.obsvis)                              ; input amplitudes
  visin.sigamp    = SQRT(visin.sigamp^2  + syserr^2 * visamp^2)
  ;
  ; Process semi-adjustable input parameters and define a common array to pass uv data
  IF KEYWORD_SET(circle) AND KEYWORD_SET(multi)  THEN MESSAGE, '/CIRCLE and /MULTI switches are incompatible.'
  IF KEYWORD_SET(loop)   AND KEYWORD_SET(multi)  THEN MESSAGE,   '/LOOP and /MULTI switches are incompatible.'
  IF KEYWORD_SET(loop)   AND KEYWORD_SET(circle) THEN MESSAGE,  '/LOOP and /CIRCLE switches are incompatible.'
  COMMON uvdata, u,v,pa, mapcenter
  ;
  ; Extract the visibility components and convert them to a 1-D vector
  ; By convention, input to fitting algorithm will be a 2*nvis element vector (all real then all imaginary components)
  nvis        = N_ELEMENTS(visin)
  npt         = 2*nvis
  jdum = FINDGEN(npt)                         ; dummy 'x' values used in fitting routine
  IF KEYWORD_SET(nophase) EQ 0 THEN BEGIN        ; normal
    visx    = FLOAT(visin.obsvis)
    visy    = IMAGINARY(visin.obsvis)
  ENDIF ELSE BEGIN                               ; Set phase to zero if /NOPHASE is set
    visx    = ABS(visin.obsvis)
    visy    = FLTARR(nvis)
  ENDELSE
  visxyobs    = [visx, visy]                   ; convert to a 2*nvis vector
  amp0      = AVERAGE(visamp)                     ; initial flux guess = average amplitude
  IF noerr NE 0 THEN error = FLTARR(npt) + 1. ELSE error  = [visin.sigamp, visin.sigamp]   ; default is to use errors
  ;
  ; Prepare contents of common block to convey slit position angles for all uv points and define a common block to convey u,v values.
  mapcenter   = visin[0].xyoffset                         ; 2-element vector,  the same for all visibilities
  dummy       = hsi_vis_select (visin, PAOUT=paout)       ; Calculate position angles
  u           = visin.u
  v           = visin.v
  pa          = paout              ; position angleS of uv points (E of N)
  IF MAX(paout GT 180.) THEN PRINT, 'HSI_VIS_FWDFIT recommends combining conjugate points.
  ;
  ; IF NOFIT is set, can return now that the common block is defined.
  IF KEYWORD_SET(nofit) THEN RETURN
  ;
  ; If an initial source structure is not provided, define and initialize such a structure.
  ;   Initial parameters are a 10" diameter elliptical gaussian with flux=largest amplitude, located at map center.
  IF N_ELEMENTS(srcstrin) EQ 0 THEN BEGIN
    IF KEYWORD_SET(circle) THEN firsttype = 'circle' ELSE firsttype = 'ellipse'
    srcstr0             = hsi_vis_src_structure()
    srcstr0.srctype     = firsttype
    srcstr0.srcflux     = amp0
    srcstr0.srcx        = mapcenter[0]
    srcstr0.srcy        = mapcenter[1]
    srcstr0.srcfwhm     = 10.
  ENDIF ELSE srcstr0      = srcstrin
  ;
  ; Set masks for a circular or elliptical source.
  PRINT
  nsrc                    = N_ELEMENTS(srcstr0)
  ;IF N_ELEMENTS(srcstr0) GT 1 THEN srcparm_mask = [circ_mask, circ_mask] $
  ;ELSE BEGIN
  ;    IF srcstr0.srctype EQ 'circle'  THEN srcparm_mask =  circ_mask
  ;    IF srcstr0.srctype EQ 'ellipse' THEN srcparm_mask = ellip_mask
  ;    IF srcstr0.srctype EQ 'loop'    THEN srcparm_mask =  loop_mask
  ;ENDELSE
  ;
  for i = 0,nsrc-1 do begin
    case srcstr0[i].srctype of
      'circle':  srcparm_mask = append_arr(srcparm_mask, circ_mask)
      'ellipse': srcparm_mask = append_arr(srcparm_mask, ellip_mask)
      'loop':    srcparm_mask = append_arr(srcparm_mask, loop_mask)
    endcase
  endfor

  ; Fit this configuration
  srcstr = hsi_vis_fwdfit_fixedconfig(visxyobs, error, srcstr0, srcparm_mask, mapcenter,  $
    MAXITER=maxiter, REDCHISQ=redchisq, NITER=niter, NFREE=nfree, _EXTRA=extra)
  ;
  ; If multiple sources are assumed, bifurcate the elliptical source and re-fit
  IF KEYWORD_SET(multi) AND nsrc EQ 1 THEN BEGIN
    srcstr = hsi_vis_fwdfit_bifurcate(srcstr)
    srcparm_mask    = [circ_mask,circ_mask]
    srcstr         = hsi_vis_fwdfit_fixedconfig( visxyobs, error, srcstr, srcparm_mask, mapcenter, $
      MAXITER=maxiter, REDCHISQ=redchisq, NITER=niter, NFREE=nfree, _EXTRA=extra)
  ENDIF
  ;
  ; If loop source was assumed, re-fit the elliptical gaussian.
  IF KEYWORD_SET(loop) AND srcstr[0].srctype EQ 'ellipse' THEN BEGIN
    srcstr.srctype = 'loop'
    srcparm_mask = loop_mask
    srcstr = hsi_vis_fwdfit_fixedconfig( visxyobs, error, srcstr, srcparm_mask, mapcenter, $
      MAXITER=maxiter, REDCHISQ=redchisq, NITER=niter, NFREE=nfree, _EXTRA=extra)
  ENDIF
  ;
  ; If albedo option is chosen, add an albedo source component and refit.
  IF KEYWORD_SET(albedo) THEN BEGIN
    albstr0                 = hsi_vis_src_structure()
    albstr0.srctype         = 'albedo'
    albstr0.albedo_ratio    = 0.1
    albstr0.srcheight       = 10.
    srcstr                  = [srcstr, albstr0]
    srcparm_mask            = [srcparm_mask, albedo_mask]
    srcstr                 = hsi_vis_fwdfit_fixedconfig( visxyobs, error, srcstr, srcparm_mask, mapcenter, $
      MAXITER=maxiter, REDCHISQ=redchisq, NITER=niter, NFREE=nfree)
  ENDIF
  srcstrout = srcstr
  ;
  ; Print and plot final results
  PRINT
  PRINT, ANYTIM(visin[0].trange, /ECS)
  PRINT, visin[0].erange, FORMAT="('Energy range:', 2F7.1, ' keV')"
  hsi_vis_fwdfit_print, srcstr, _EXTRA=extra
  IF KEYWORD_SET(noplotfit) EQ 0 THEN hsi_vis_fwdfit_plotfit, visin, srcstr, mapcenter, _EXTRA=extra
  ;
  ; Calculate statistical uncertainty in fitted parameters.
  qflag = 0                       ; a quality flag, 0 = fit is probably ok
  IF KEYWORD_SET(noerr) THEN BEGIN
    fitstddev = hsi_vis_src_structure()
    fitstddev.srctype = 'std.dev'
  ENDIF ELSE BEGIN
    fitstddev = hsi_vis_fwdfit_sigmacalc(visxyobs, error, srcstr, srcparm_mask, mapcenter, QFLAG=qflag, TRIAL_RESULTS=trial_results, _EXTRA=extra)
    hsi_vis_fwdfit_print, fitstddev, /COMPACT, _EXTRA=extra
    PRINT
  ENDELSE

  derived_parms = hsi_vis_fwdfit_derived_parameters(srcstr, trial_results)
  PRINT, 'Reduced chi2 = ', redchisq
  IF niter EQ maxiter   THEN PRINT, 'WARNING: NO CONVERGENCE after', niter, ' iterations.' ELSE $
    PRINT, 'Converged after', niter, ' iterations.'
  IF (qflag AND 1) NE 0 THEN PRINT, 'WARNING: MARGINAL DETECTION of at least one source component.'
  IF (qflag AND 2) NE 0 THEN PRINT, 'WARNING: FIT IS SUSPECT since at least one fitted parameter is at edge of its range.'
  IF (qflag AND 4) NE 0 THEN PRINT, 'WARNING: FIT IS UNSTABLE due to shallow chisq minimum.'
  IF (qflag AND 8) NE 0 THEN PRINT, 'WARNING: LARGE UNCERTAINTY in at least one fitted parameter.'
  PRINT
  IF KEYWORD_SET(showmap) THEN hsi_vis_fwdfit_showmap, srcstr, mapcenter, TIME=visin0[0].trange[0], _EXTRA=extra
END



