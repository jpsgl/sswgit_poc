FUNCTION hsi_vis_edit, visin, SUBCOLL=subcoll, CHI2LIM=chi2lim, ENERGY_BAND=energy_band, QUIET=quiet
  ;
  ; Returns an edited version of the input visibility structure, visin.
  ; Input visibility structure is not modified.
  ; Removes bad visibility points (as indicated by visin.vis.chi2 values) that are -ve.
  ; Optionally retains only visibilities from specified subcollimators. (Default=retall all subcollimators.)
  ; Optionally removes visibilities whose chi^2 is too large).
  ;
  ; KEYWORDS:
  ;	SUBCOLL = 9-element vector, corresponding to the 9 subcollimators.
  ;					0==> discard data for this subcollimator.
  ;					Otherwise amplitudes and errors are multiplied by the nonzero value.
  ;				Defaults is equivalent to [1,1,1, 1,1,1, 1,1,1] (accept all subcollimators)
  ; 	CHI2LIM	= maximum acceptable chi^2 value (defaults to 5)
  ;	ENERGY_BAND = 2-element vector specifying an energy band which returned visibilities must match. Default=all energies.
  ;
  ; 31-Aug-05 gh	Initial version.  (ghurford@ssl.berkeley.edu)
  ; 18-Sep-05 gh	Adapted to simplified visibility structure.
  ; 27-Sep-05 gh	Reject values where chi2=0, which correspond to only a fit to a limited number of phase points.
  ; 15-Dec-05 gh	Rewrite to suppport additional selection criteria.
  ;				Add ENERGY_BAND keyword.
  ; 30-Apr-09 Kim Added quiet keyword
  ; 16-Jun-2017, Kim. Added time and energy to message about number of vis accepted
  ;
  ; Identify visibilities with non-negative chi2 values.
  DEFAULT, chi2lim, 1.E9				; any very large number will do
  DEFAULT, quiet, 0
  ok = WHERE(visin.chi2 GT 0 AND visin.chi2 LE chi2lim, nok)
  IF nok EQ 0 THEN BEGIN
    PRINT, 'HSI_VIS_EDIT found no visibilities with acceptable chi2.'
    RETURN, -1
  ENDIF
  vis = visin[ok]
  ;
  ; Identify visibilities with acceptable subcollimator.
  IF N_ELEMENTS(subcoll) NE 0 THEN BEGIN
    IF N_ELEMENTS(subcoll) NE 9 THEN MESSAGE, 'Illegal SUBCOLL value'
    nok = 0
    FOR j = 0, 8 DO BEGIN
      IF subcoll[j] NE 0 THEN BEGIN
        jok = WHERE(vis.isc EQ j, njok)
        IF njok GT 0 THEN BEGIN
          IF nok EQ 0 THEN ok = jok ELSE ok = [ok, jok]
          nok = nok + njok
        ENDIF
      ENDIF
    ENDFOR
    IF nok EQ 0 THEN BEGIN
      PRINT, 'HSI_VIS_EDIT found no good visibilities with acceptable subcollimators.'
      RETURN, -1
    ENDIF
    vis = vis[ok]
  ENDIF
  ;
  ; Identify visibilities with acceptable energy_band.
  IF N_ELEMENTS(energy_band) NE 0  THEN BEGIN
    IF N_ELEMENTS(energy_band) NE 2 THEN MESSAGE, 'Illegal ENERGY_BAND value.'
    ok = WHERE(vis.erange[0] EQ energy_band[0] AND vis.erange[1] EQ energy_band[1], nok)
    IF nok EQ 0 THEN BEGIN
      PRINT, 'HSI_VIS_EDIT found no good visibilities with acceptable energy_band.'
      RETURN, -1
    ENDIF
    vis = vis[ok]
  ENDIF
  ;
  if ~quiet then message, '     ' + hsi_vis_time_en(vis) + ': Accepted ' + trim(nok) + ' of ' + trim(n_elements(visin)) + ' input visibilities.', /info
  RETURN, vis
END