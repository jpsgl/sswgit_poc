;---------------------------------------------------------------------------
; Document name: hsi_data_gap_control__define.pro
; Created by:    Richard Schwartz
;
;
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       Datagap CONTROL STRATEGY CONTROL STRUCTURE INITIALIZATION
;
; PURPOSE:
;       Sets the datagap control parameters 
;
; CATEGORY:
;       Utilities (hessi/util)
;
; CALLING SEQUENCE:
;       result = {hsi_data_gap_control}
;
; OUTPUTS:
;       result: a variable of type {hsi_data_gap_control}, withthe
;               default values
;
; SEE ALSO:
;    IDL> help, hsi_dp_cutoff_control()
;    ** Structure HSI_DP_CUTOFF, 4 tags, length=16, data length=16:
;    DP_CUTOFF_MAX   FLOAT         0.0500000
;    DP_CUTOFF_MIN   FLOAT       0.000800000
;    DP_CUTOFF_COEFF FLOAT           4.50000
;    DP_CUTOFF_XP    FLOAT         -0.900000
;    IDL> help, hsi_dp_extend_control()
;    ** Structure HSI_DP_EXTEND, 7 tags, length=256, data length=252:
;    DP_EXTEND_UTLIM DOUBLE    Array[9]
;    DP_EXTEND_SEC   FLOAT     Array[9]
;    DP_EXTEND_DEF   FLOAT     Array[9]
;    DP_APPEND_DEF   FLOAT     Array[9]
;    DP_PREPEND_DEF  FLOAT     Array[9]
;    DP_PREPEND_NVALID
;    INT       Array[9]
;    DP_APPEND_NVALID
;    INT       Array[9];
;
; HISTORY:
;
;     19-sep-02, ras, moved dp_enable, dp_cutoff, and extend_time_range here
;      from hsi_spectrogram_control
;     1-oct-02, ras, add dp_lld, channel thresholds for events that close datagaps
;   24-oct-02, ras, added deflt_atten_state for simulation files
;     21-feb-03, ras, dp_lld[6] raised from 50 to 65.  Detector 7 has a higher threshold.
;   18-apr-03, ras, DP_CUTOFF raised from .01 to .03 seconds after examining
;   dropout behavior w/o csa events.
;   11-apr-05, ras, added dp_extend, this is used to extend the datagaps this many SECONDS
;     after the otherwise determined conclusion.  And it sets the coinc flag for
;     any events within any datagap intervals.
; 11-jan-2007, ras, change dp_extend to a pointer for allow more complex data structures for
;   dp_extend
; 15-mar-2007, ras, break out dp_extend to two arrays, dp_extend_sec - duration in seconds
;   and dp_extend_utlim - anytim compatible date for using extend logic to wipe out post
;   gap spikes
;   break out dp_cutoff to four dp_cutoff_xxx to characterize the function hsi_dp_cutoff
;IDL> help, hsi_dp_cutoff(),/st
;** Structure HSI_DP_CUTOFF, 4 tags, length=16, data length=16:
;   MAX             FLOAT         0.0500000
;   MIN             FLOAT       0.000800000
;   COEFF           FLOAT           4.50000
;   XP              FLOAT         -0.900000
;; 26-oct-2015, richard.schwartz@nasa.gov, data gap (dropout) control parameter defaults built in hsi_data_gap_control()
;  22-feb-2017, richard.schwartz@nasa.gov, extend from 9 elements for lld and uld to 18 to cover rear det off periods
;  with 0 livetime and no events.
;  13-apr-2017, ras, added ramp_peak and ramp_ntest used to control ramp detection
;+

pro hsi_data_gap_control__define

d = {hsi_data_gap_control, $
  inherits hsi_dp_cutoff, $
  inherits hsi_dp_extend, $
  extend_time_range: 0.0, $
  dp_enable: 0, $
  dp_lld: intarr(18), $
  dp_uld: lonarr(18), $
  ramp_peak: 0.0, $ ;channel in keV threshold for start of ramp, higher than bulk threshold
  ramp_ntest: 0, $ ;number of consecutive values over buld threshold required to be a ramp
  atten0_test: 0.0, $;if counter livetime is below this threshold in att0, disable data during
  ;ramp epoch after 2014
  no_csa_dropout: 0 } ;If this is set, then dropoutlist compiled w/o using
  ;csa even if csa event are in datastream.
  
  end

