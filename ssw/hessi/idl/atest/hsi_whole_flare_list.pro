;+
; Updates the whole mission flare list, or the qflare_list file
; Hacked from hsi_contact2fits, Added deal with qflare_list.fits.
; 3-oct-2002, jmm, jimm@ssl.berkeley.edu
; Rewritten to deal with multiple files, 27-mar-2003,jmm
; Changed to call hsi_mult_flare_list_inp, and replaces the flare
; list, doexn't concat, and will always replace old data
; Added time range and /months_only to hsi_mult_flare_list_inp call,
; to get only the flares for a proper month.
; Added long_archive, to keep from overwriting full flare list file
;-
Pro hsi_whole_flare_list, oflr, time_range, archive = archive, $
                          do_catalog = do_catalog, $
                          filedb_dir = filedb_dir, filedb_pub = filedb_pub, $
                          long_archive = long_archive

  oti = anytim(time_range)
  If(keyword_set(filedb_dir)) Then fdbdir = filedb_dir $
  Else fdbdir = '$HSI_FILEDB_DIR'
  If(keyword_set(filedb_pub)) Then fdbdir_pub = filedb_pub $
  Else fdbdir_pub = '$HSI_FILEDB_PUB'

  hessi_all_list = hsi_mult_flare_list_inp(filedb_dir = fdbdir, $
                                           time_range = oti, $
                                           qtime_range = qtime_range, $
                                           /months_only)
  If(obj_valid(hessi_all_list)) Then dflr_old = hessi_all_list -> get(/data) $
  Else dflr_old = -1

;Is there any new data here
  If(obj_valid(oflr) Eq 0) Then dflr = -1 $
  Else dflr = oflr -> get(/data)
  If(is_struct(dflr_old) Eq 0) And (is_struct(dflr) Eq 0) Then Begin
    message, /info, 'No Flare List...'
    Return
  Endif
  If(is_struct(dflr_old) Eq 0) Then hessi_all_list = oflr Else Begin
 ;get the old flares not in this time range
    ok = where(dflr_old.start_time Lt oti[0] Or $
               dflr_old.start_time Gt oti[1], nok)
    If(nok Eq 0) Then Begin
      If(is_struct(dflr)) Then hessi_all_list = oflr Else Begin
        message, /info, 'No Flare List...'
        Return
      Endelse
    Endif Else Begin
      info = hessi_all_list -> get(/info)
      dflr_old = dflr_old[ok]   ;only keep these
      If(is_struct(dflr) Eq 0) Then dflr = temporary(dflr_old) $
      Else dflr = [temporary(dflr_old), temporary(dflr)]
      ss = sort(dflr.start_time)
      dflr = dflr[ss]
      nn = n_elements(dflr)
      info.n_flares = nn
      info.list_start_time = anytim(dflr[0].start_time, /ccsds)
      info.list_end_time = anytim(dflr[nn-1].end_time, /ccsds)
      hessi_all_list -> set, info = info, data = dflr
    Endelse
  Endelse
;Now, the only part of the list that needs outputting is that for the
;time range given by hsi_qflare_list, which contains the flares just
;gotten, and/or the flares in the qflare list, which may not have been
;picked up by the primary process:
  If(total(qtime_range) Gt 0.0) Then Begin
    qtime_range = [min([qtime_range, oti]), max([qtime_range, oti])]
  Endif Else qtime_range = oti
  If(keyword_set(long_archive)) Then Begin
;Only output for this time range, but in the flare list file, not the
;qflare_list file
    message, /info, 'Update Flare_list File'
    hsi_write_all_flare_list, hessi_all_list, /temp, filedb_dir = fdbdir, $
      filename = 'hessi_flare_list.fits', time_range = qtime_range
    wait, 1.0
    hsi_write_txt_flare_list, hessi_all_list, /temp, $
      filedb_dir = fdbdir_pub, $
      filename = 'hessi_flare_list.txt', time_range = qtime_range
  Endif Else If(keyword_set(do_catalog)) Then Begin
;Only output for this time range
    message, /info, 'Update Qflare_list File'
    hsi_write_all_flare_list, hessi_all_list, /temp, filedb_dir = fdbdir, $
      filename = 'hsi_qflare_list.fits', time_range = qtime_range
  Endif Else Begin
    message, /info, 'Update Whole Mission Flare List'
    hsi_write_all_flare_list, hessi_all_list, /temp, filedb_dir = fdbdir, $
      filename = 'hessi_flare_list.fits', time_range = qtime_range
    hsi_write_all_flare_list, hessi_all_list, /temp, $
      filedb_dir = fdbdir_pub, $
      filename = 'hessi_flare_list.fits', time_range = qtime_range
;Kim's text file, wait a second just to be sure that the temp
;fits file has been moved
    wait, 1.0
    hsi_write_txt_flare_list, hessi_all_list, /temp, $
      filedb_dir = fdbdir_pub, $
      filename = 'hessi_flare_list.txt', time_range = qtime_range
;A text file with the whole flare list, jmm, 13-aug-2003
;Fixed to explicitly use the entire flare list as just written out
    hsi_read_all_flare_list, all_list_really, filedb_dir = fdbdir
    hsi_write_txt_flare_list, all_list_really, /temp, $
      filedb_dir = fdbdir_pub, /all
    If(obj_valid(all_list_really)) Then obj_destroy, all_list_really
  Endelse

Return
End

