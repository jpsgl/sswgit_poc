;---------------------------------------------------------------------------

 ;+
 ; PROJECT:
 ;  HESSI
 ; NAME:
 ;  HSI_SPECTROGRAM::MKLIVETIME
 ;
 ; PURPOSE:
 ;  This procedure computes the measured livetime integrated
 ;	over the accumulation bins.
 ;
 ;

 ; CALLING SEQUENCE:
 ;
 ; CALLS:
 ;
 ;
 ; INPUTS:
 ;
 ;
 ; OPTIONAL KEYWORD, INPUTS:
 ;
 ; OUTPUTS:
 ;
 ; Detector_off - a structure indicating intervals when detectors are turned off.
 ; PROCEDURE:
 ;
 ;
 ; MODIFICATION HISTORY:
;   may 25, 2003, acs minor change to fix a bug
 ;  May 21, 2002, ras
 ;  16-may-2003, ras, support streaming structure of livetime as well as packetized
 ;-

 pro HSI_Spectrogram::mklivetime, $

 	ltlist, $
 	time_binning, $
 	time_offset, $
 	livearr, $
 	samples

 is_binned_eventlist = 'POINTER' eq Size( livearr, /tname )


 a2d_index_mask = Self->Get(/A2D_INDEX_MASK)
 these_a2d = Where( A2D_INDEX_MASK , na2d )

 these_seg = hsi_a2d2seg( these_a2d )
 nseg = n_elements( these_live)

 ut_ref = hsi_sctime2any( Self->Get(/lt_ut_ref))
 time_unit = self->get(/time_unit)
 lt_time_unit = 512 ;default for livetime time


 nkeep = 0

 constant = hessi_constant()
 
;Prevent empty ltlist crash
 if size(/tname, ltlist) eq 'STRUCT' then BEGIN 
     
; acs changed the place of this it was too early
     nselect = n_elements(ltlist.time)

     if chktag(ltlist, 'seg_index') then begin


         time_spec = ltlist.time  + lonarr(nselect)

         seg_index = ltlist.seg_index

     ;If there are any time gaps or overlaps, the final time summations
     ;are defined in time_final_assign
     ;
         time_bin_busec_a2d = gt_tagval( time_binning, 'time_bin_busec_a2d', missing=0)

     ;
     ;First block is to handle the binned ltlist.  These are time bins of
     ;fixed size, or grouping factor,  in time_unit, but may be different for each a2d.
     ;The grouping factor should be consistent for each collimator, i.e. the same for a2d mod 9.
     ;The grouping factor should be small enough to be consistent with the finest harmonic
     ;required, but this routine should not know anything about the harmonics.
         if is_binned_eventlist then begin

             time_start = long64((ut_ref-time_binning.abs_time_range[0] )*2.0^20/lt_time_unit)
         ;Time_start is in units of lt_time_unit, this is the time reference for
         ;time_spec relative to the start of the time_range
         ;Next, with time_spec in units of lt_time_unit, add it to the time_start
         ;and identify it with it's been in the binned eventlist
             time_spec[0] = long(   (time_spec + time_start) / $
                                    (time_bin_busec_a2d[seg_index]/lt_time_unit)   )

         endif else begin

         ;This block handles the case of the spectrogram where the time binning is the same
         ;for each a2d, but this binning may be completely arbitrary, and is given by
         ;time_binning.group in time_unit.
         ;tspec=time_spec
             time_spec[0] = value_locate( $
                  long64([time_binning.group[0]-10000d0, time_binning.group]*time_unit - $
                         time_offset*2.d0^20)/lt_time_unit, time_spec) - 1

         endelse

         s = histogram( ltlist.seg_index, min=0, max=17, rev=rev)
         seg_sel = where( s < 1 )
         w = where_arr( seg_sel, these_seg, nsel )
         seg_sel = seg_sel[w]

         for i=0, nsel-1 do begin

             this_seg = seg_sel[i]
             select = rev[rev[this_seg]:rev[this_seg+1]-1]
             use_seg = 1
             If Is_binned_eventlist then use_seg = ptr_valid( livearr[this_seg] )
             If use_seg then begin
                 livetime=is_binned_eventlist ? *livearr[this_seg] : livearr[*, this_seg]
                 sample_seg=is_binned_eventlist ? *samples[this_seg] : samples[*, this_seg]
                 nbin = n_elements(livetime)

                 ord = sort(time_spec[ select])
         ;
         ; Convert the counter value into actual livetime using David Smith's function
         ; Then, average over the livetime samples in each interval using the total(/cumul) function
         ;
         ;counter = hsi_corrected_livetime(  (ltlist[select])[ord].counter, this_seg,/raw)
         counter = hsi_corrected_livetime(  (ltlist.counter)[select[ord]], this_seg,/raw)
         ;Subtract away the average livetime to increase resolution ont the cumulative sum
                 avg_livetime = avg( counter )
                 avg_livetime = 0.0
                 counter = counter - avg_livetime
                 counter = [0.0, total(counter, /double, /cum)]
                 h = histogram( time_spec[select], bin=1, min=0, max=nbin-1, r=r)
                 htot = [0, total( h, /cum, /double)]
         

                 new_livetime = float(counter[htot[1:*]]-counter[htot])
                 sample_seg = sample_seg + float(h)
                 livetime = livetime + new_livetime


                 If is_binned_eventlist then *livearr[this_seg] = livetime else $
                     livearr[0, this_seg]=livetime
                 If is_binned_eventlist then *samples[this_seg] = sample_seg else $
                     samples[0, this_seg]=sample_seg
             endif
         endfor
         
     ;Time_cumul_seg is an array which keeps the total cumulative number of time bins as a function
     ;of the seg_index.  This is needed for the final mapping between the tuples for
     ; (seg_index, time, channel) and the livearr  index.
     ENDIF
 ENDIF 
                                ;return, livearr
end

 ;---------------------------------------------------------------------------

