;+
;NAME:
; hsi_xy_test_new
;PURPOSE:
; Finds flare position in the 12 to 25 keV energy band, also verifies
; position for a larger energy.
; Switched to using hsi_flare_position_image to verify higher energy
; emission, jmm, 5-mar-2015.
; Added a test to hsi_flare_vfy, if no position is found using
; imaging, this will return a yes if hsi_flare_vfy returns a value.
;-
Pro Hsi_xy_test_new, trange, peak_time0, obs_summ_obj, ok_out, $
                     oti_out, xy_out, $
                     echannel = echannel, $
                     image_tintv = image_tintv, $
                     min_image_intv = min_image_intv, $
                     Quiet = quiet, $
                     spin_axis = spin_axis, $
                     verify = verify, $
                     st_time_in = st_time_in, $
                     en_time_in = en_time_in, $
                     xy_in = xy_in, $
                     sigma_limit = sigma_limit, $
                     radius_limit = radius_limit, $
                     n_pos_test = n_pos_test, $
                     suspect_solution = suspect_solution, $
                     _extra = _extra


  oti_out = [0.0d0, 0.0d0]    ;initialize
  xy_out = [0.0, 0.0]
  ok_out = 0b
  err = 0
  catch, err
  If(err Ne 0) Then Begin
    catch, /cancel
    Print, 'Error'
    help, /last_message, output = err_msg
    print, err_msg
    print, 'Returning 0'
    oti_out = [0.0d0, 0.0d0]    ;initialize
    xy_out = [0.0, 0.0]
    ok_out = 0b
    Return
  Endif
  If(obj_valid(image_obj) Eq 0) Then image_obj = -1
  If(keyword_set(echannel)) Then ch0 = echannel Else ch0 = 2
  If(keyword_set(quiet)) Then notquiet = 0 Else notquiet = 1
  If(keyword_set(image_tintv)) Then tintv = image_tintv $
  Else tintv = 180.0            ;on each side of the peak
  If(keyword_set(min_image_intv)) Then min_tintv = min_inage_intv $
  Else min_tintv = 30.0
  If(Not keyword_set(n_pos_test)) Then n_pos_test = 3
  If(keyword_set(radius_limit)) Then rvalue = radius_limit Else rvalue = 60.0
  If(keyword_set(sigma_limit)) Then tvalue = sigma_limit Else tvalue = 3.0
;first call is to hsi_ok_intv
  otix = anytim(trange)
  pktim = anytim(peak_time0)
  If(keyword_set(st_time_in) And keyword_set(en_time_in)) Then Begin
    st_time = st_time_in
    en_time = en_time_in
  Endif Else Begin
    hsi_ok_intv, otix, tintv, tarrx, ss_stx, ss_enx, st_time, en_time, $
      xrate_out, obs_summ_obj = obs_summ_obj, dt_min = 8.0, $
      /max_size, _extra = _extra
  Endelse
;If there are good intervals, you may want the one with the most counts
  robj = obs_summ_obj -> get(/obs_summ_rate)
  rinfo = robj -> get(/info)
  rates_ebands = rinfo.energy_edges
  ern = rates_ebands[ch0:ch0+1]
  nrates_ebands = n_elements(rates_ebands)-1
  fobj = obs_summ_obj -> get(/obs_summ_flag)
  rates = robj -> corrected_countrate(obs_summ_flag_obj = fobj)
  tim_arr = robj->get(/time_array)
  If(st_time[0] Ne -1) Then Begin
    nintv = n_elements(st_time)
    If(nintv Gt 1) Then Begin
      xcounts = fltarr(nintv)
      For j = 0, nintv-1 Do Begin
        ss = where(tim_arr Ge st_time And tim_arr Lt en_time, nss)
        If(nss Gt 0) Then xcounts[j] = total(rates[ch0, ss])*rinfo.time_intv
      Endfor
      ss_xc = sort(xcounts)
      st_timex = st_time[ss_xc]
      en_timex = en_time[ss_xc]
      xcounts = xcounts[ss_xc]
    Endif Else Begin
      st_timex = st_time & en_timex = en_time
    Endelse
  Endif Else Begin
     message, 'No good time intervals'
  Endelse
;Is the peak time in one of the ok intervals?
  ok_if = where(pktim Ge st_timex And pktim Lt en_timex, nok_if)
  If(nok_if Gt 0) Then Begin
    st_time1 = (pktim - tintv) > st_timex[ok_if]
    en_time1 = (pktim + tintv) < en_timex[ok_if]
;Try this interval first
    If(st_time1-st_timex[0] Ne 0.0 And en_time1-en_timex[0] Ne 0.0) Then Begin
      st_timex = [st_time1, st_timex]
      en_timex = [en_time1, en_timex]
    Endif
  Endif
     
  If(obj_valid(image_obj) Eq 0) Then image_obj = hsi_image()
  If(keyword_set(verify)) Then Begin
;only keep intervals inside the input time range
;If otix is completely out of the range, then use st_timex, en_timex,
;jmm, 1-oct-2014
    oti = [st_timex[0], en_timex[0]]
    If(otix[0] Ge min(st_timex) And otix[1] Le max(en_timex)) Then Begin
       oti = otix ;both ok
    Endif Else If(otix[0] Ge min(st_timex) And otix[1] Gt max(en_timex)) Then Begin
       oti = [otix[0], max(en_timex)] ;low is ok
    Endif Else If(otix[0] Lt min(st_timex) And otix[1] Lt max(en_timex)) Then Begin
       oti = [min(st_timex), otix[1]] ;High is ok
    Endif Else Begin
       oti = [st_timex[0], en_timex[0]] ;None is ok
    Endelse
    If(ch0 Gt 5) Then Begin
      front_segment = 0
      rear_segment = 1
      If(ch0 Ge 6) Then det_mask_in = [bytarr(5), 1b, 0b, 0b, 1b] $
      Else det_mask_in = [bytarr(5), 1b, 1b, 1b, 1b]
    Endif Else Begin
      front_segment = 1
      rear_segment = 0
      det_mask_in = [bytarr(4), bytarr(5)+1]
    Endelse
    hsi_flare_position_image, oti, ern, xy, ok_position = ok, $
                              image_obj = image_obj, $
                              spin_axis = spin_axis, $
                              quiet = quiet, $
                              det_mask_in = det_mask_in, $
                              front_segment = front_segment, $
                              rear_segment = rear_segment, $
                              xy_in = xy_in, pix_in = 5.0
    r = sqrt(total((xy-xy_in)^2))
    print,  r
    If(ok Ne 0 And r Le rvalue) Then Begin
      message, /info, 'Position Success: xy:'
      print, xy
      ok_out = 1b
      xy_out = xy
      oti_out = oti
    Endif Else If(ch0 Eq 5) Then Begin ;try rear dets
      front_segment = 0
      rear_segment = 1
      det_mask_in = [bytarr(5), 1b, 1b, 1b, 1b]
      hsi_flare_position_image, oti, ern, xy, ok_position = ok, $
                                image_obj = image_obj, $
                                spin_axis = spin_axis, $
                                quiet = quiet, $
                                det_mask_in = det_mask_in, $
                                front_segment = front_segment, $
                                rear_segment = rear_segment, $
                                xy_in = xy_in, pix_in = 5.0
      r = sqrt(total((xy-xy_in)^2))
      If(ok Ne 0 And r Le rvalue) Then Begin
        message, /info, 'Position Success: xy:'
        print, xy
        ok_out = 1b
        xy_out = xy
        oti_out = oti
      Endif Else Begin
        message, /info, 'Position Failure: xy:'
        print, xy
        ok_out = 0b
      Endelse
    Endif Else ok_out = 0b
  Endif Else Begin
     npt = n_elements(st_timex) < n_pos_test
     ok = 0
     For k = 0, npt-1 Do Begin
        oti = [st_timex[k], en_timex[k]]
        ok = hsi_locate_flare(oti, ern, xy, $
                              image_obj = image_obj, $
                              spin_axis = spin_axis, $
                              suspect_solution = suspect_solution, $
                              quiet = quiet, _extra = _extra)
        If(ok gt 0) Then goto,  no_more_tests
     Endfor
     no_more_tests:
     If(ok Gt 0) Then Begin
        oti_out = oti
        xy_out = xy
        ok_out = 1b
     Endif Else ok_out = 0b
;Last chance
     If(ok_out Eq 0b) Then Begin
        message, /info, 'Last chance: '
        ptim, trange
        print, ern
        a = hsi_flare_vfy(trange, ern)
        If(a gt 0) Then Begin
;use the flare_position_image to guess
           oti = [st_timex[0], en_timex[0]]
           hsi_flare_position_image, oti, ern, xy_o, ok_position = ok_position
           ok_out = 1b
           If(ok_position) Then Begin
              xy_out = xy_o 
              oti_out = oti
           Endif Else Begin
              xy_out = [0.0, 0.0]
              oti_out = anytim(trange)
           Endelse
        Endif
     Endif
  Endelse
;Right before returning
  If(obj_valid(image_obj)) Then obj_destroy, image_obj
  
End
