;Hacked from clean_spikes.pro, despikes an array by comparing actual
;values to smoothed values, then replaces the missing data with
;interpolated values. 
Function temp_despike, array, t, nsmooth = nsmooth, spike_thresh = spike_thresh

  array_out = array             ;clean copy
  If(keyword_set(nsmooth)) Then ns = nsmooth Else ns = 15
  If(keyword_set(spike_thresh)) Then ft = float(spike_thresh) Else ft = 10.0
  If(n_elements(array) Lt ns*11) Then Return, array_out
  tarray = smooth(array, ns)
  bad = abs(array) Ge ft*abs(tarray) Or abs(array) Le abs(tarray)/ft
  wbad = where(bad Ne 0, nbad)
  wok = where(bad Eq 0, nok)
  If(nok Gt 0 && nbad Gt 0) Then $
    array_out[wbad] = interpol(array[wok], t[wok], t[wbad])
  Return, array_out
End
;Smooths data, checks to see if there is enough
Function temp_smooth, data, smpts
  n = n_elements(data)
  If(n Gt 5*smpts) Then Begin
    return, smooth(data, smpts)
  Endif Else return, data
End
;temp_running_avg will average N points before a given point
;Note that the first N points will not be too useful...
;Starts at the 2nd point back...28-mar-2002,jmm
Function Temp_running_avg, a, n, sigma
  If(n Le 0) Then Begin
    sigma = -1
    Return, a
  Endif
  na = n_elements(a)
  If(na Lt n) Then Begin
    sigma = -1
    Return, a
  Endif
  temp_array = replicate(a[0], n, na)
  x1 = lindgen(na)-n & x1  = x1 > 0
  x2 = lindgen(na)-1 & x2 = x2 > 0
  For j = 1l, na-1l DO temp_array[0:x2[j]-x1[j], j] = a[x1[j]:x2[j]]
  b = avsig_1(temporary(temp_array), sigma, dimen = 1)
  If(na Gt n) Then Begin 
    b[0:n-1] = b[n-1] 
    sigma[0:n-1] = sigma[n-1] 
  Endif Else Begin
    b[*] = b[na-1]
    sigma[*] = sigma[na-1]
  Endelse
  Return, b
End
;+
;NAME:
; hsi_flare_list_fill
;PURPOSE:
; Finds flares from countrates
;CALLING SEQUENCE:
Pro hsi_flare_list_fill, obs_summ_obj, otp_struct, $
                         Quiet = quiet, $
                         channel = channel, $
                         nbsigs = nbsigs, $
                         min_cps_threshold = min_cps_threshold, $
                         nbpts = nbpts, $
                         min_flare_pts = min_flare_pts, $
                         all_det_tolerance = all_det_tolerance, $
                         smooth_pts = smooth_pts, $
                         add_stop = add_stop, $
                         min_gapsize = min_gapsize, $
                         ns_cutoff = ns_cutoff, $ ;added here, 31-mar-2011
                         _extra = _extra
;INPUT:
; obs_summ_obj = the obs_summ_soc_object for the given time
;                interval. This is designed to run for single orbits.
;OUTPUT:
; otp_struct = {ch0:ch0, st_fl:st_fl, en_fl:en_fl, $
;               solar_flag_1:solar_flag_1, bck_in:bck_in, $

;               deti:deti, ndets:ndets, eb:rates_ebands[ch0:ch0+1]}
;
;               where ch0 is the energy channel used, st_fl and en_fl
;               are start and end subscripts for each flare,
;               solar_flag_1 is set to 1 if the source is solar,
;               bck_in are bckground count levels, xy_ch0 is the
;               position of the flare, deti are the detectors used,
;               ndets the number of detectors, eb is the energy range
;               used to find the flare.
;KEYWORDS:
; quiet = if set, run (relatively) quietly
; channel = the obs_summ_rate channel used for the list, the default
;           is 2 (12 to 25 keV)
; nbsigs = the excess rate must be nbsigs*sigma_background for an
;          event to be possible, the default is 3
; min_cps_threshold = a minimum count rate threshold for flares, the
;                     default is 5.0
; all_det_tolerance = if the average relative count rate in a detector
;                     is larger than this number, then the event is
;                     not counted. The default is 50%
; smooth_pts = a number of points for smoothing purposes, the default
;              is 15
; min_gapsize = min_gapsize, the minimum gap size in data points, that
;               a flare can cover, the flare is split if a gap is
;               larger than this. The default is 75 (5 minutes)
; ns_cutoff = Due to popular demand, flares that have no good position
;             are not included if they do not have a peak count rate
;             above this value, the default is 80.0
; nbpts = the number of points for the running average used to test
;         for excess counts above background, default is 45, or 3
;         minutes
;HISTORY:
; rewritten from original, sep-oct-2006, jmm, jimm@ssl.berkeley.edu
; Another version, here split_flare and flat_check are called always,
; and the default channel is set to 1, jmm, 5-nov-2007
; tweaked split_flare process, which will now only split peaks if the
; minimum in the middle is relatively low (1/e times the peak) or flat
; (longer than 5 minutes), cleaned up a few more things, jmm,
; 4-sep-2009
; tweaked split_flare process again, now peaks are split if min value
; in between is 1/2 the value of the first peak, or if the min value
; drops below twice the threshold level. Oct-2010, jmm
; Added ns_cutoff, 31-mar-2011, jmm
; set default for nbpts to 45, 24-jan-2018, jmm
;-
  otp_struct = -1
  If(keyword_set(quiet)) Then notquiet = 0 Else notquiet = 1
;nbsigs is the number of sigmas above the count rate for the threshold
  If(Not keyword_set(nbsigs)) Then nbsigs = 3.0
;Background points
  If(Not keyword_set(nbpts)) Then nbpts = 45 ;3 minutes, jmm, 2018-01-24
;smoothing parameter   
  If(keyword_set(smooth_pts)) Then smpts = smooth_pts $
  Else smpts = 15
;peaks of Less than min_dt seconds will not count
  If(keyword_set(min_flare_pts)) Then min_dt = 4.0*min_flare_pts $
  Else min_dt = 8.0
;Pts for the attenuator state test, 
  If(keyword_set(n_atten_test)) Then natten = n_atten_test Else natten = 8
;Threshold count rates must be greater than min_cps_threshold
  If(keyword_set(min_cps_threshold)) Then minct = min_cps_threshold $
  Else minct = 5.0
  If(keyword_set(all_det_tolerance)) Then adt = all_det_tolerance $
  Else adt = 50
  If(keyword_set(min_gapsize)) Then mingap = min_gapsize Else mingap = 75 ;5 minutes
  If(keyword_set(ns_cutoff)) Then nscut = ns_cutoff Else nscut = 80 ;cps
;Start here
  ooinfo = obs_summ_obj -> get(/info)
;you need stuff from the obs summary object
  obs_summ_flag_obj = obs_summ_obj -> get(/obs_summ_flag)
  saa_flag = obs_summ_flag_obj -> get(/saa_flag)
  gap_flag = obs_summ_flag_obj -> get(/gap_flag)
  eclipse_flag = obs_summ_flag_obj -> get(/eclipse_flag)
  atten_state = obs_summ_flag_obj -> get(/attenuator_state)
  max_det_vs_tot = obs_summ_flag_obj -> get(/max_det_vs_tot)
;Get the count rates      
  obs_summ_rate_obj = obs_summ_obj -> get(/obs_summ_rate)
  rinfo = obs_summ_rate_obj -> get(/info)
  rates_ebands = rinfo.energy_edges
  nrates_ebands = n_elements(rates_ebands)-1
  tim_arr = obs_summ_rate_obj -> get(/time_array)
  ntimes = rinfo.n_time_intv
  dt = rinfo.time_intv
  If(keyword_set(channel)) Then ch0 = channel Else ch0 = 1 ;6 to 12
  energy_range_found = rates_ebands[ch0:ch0+1]
  rates = obs_summ_rate_obj -> $ ;nchan by ntimes
    corrected_countrate(obs_summ_flag_obj = obs_summ_flag_obj)
  rates_u = obs_summ_rate_obj -> get(/countrate)
;transopse here, to avoid all sorts of transposing later
  rates = transpose(rates)
  rates_u = transpose(rates_u)
;Only bother for the good data
  If(total(rates[*, ch0]) Le 0) Then Begin
    message, /info, 'No Count rates' & return
  Endif
;count rate correction factor, for uncertainties,
  good0 = where(rates Gt 0)
  cfactor = rates & cfactor[*] = 1.0
  cfactor[good0] = rates_u[good0]/rates[good0]
;Detector information, only fronts are used....
  seg_index_mask = rinfo.seg_index_mask
  deti = where(seg_index_mask[0:8] Eq 1, ndets)
  If(ndets Eq 0) Then Begin
    message, /info, 'No Detectors?' & return
  Endif
;Alt threshold values, these are used to decide if a flare may be 'solar'
  hsi_alt_threshold, obs_summ_obj, xdata1, ex_xdata1, _extra = _extra

;Set bad data to 0
  cgood = (saa_flag Eq 0) And (gap_flag Eq 0) And $
    (rates[*, ch0] Gt 0) And (cfactor[*, ch0] Gt 0)
  good = where(cgood Eq 1)
  bad = where(cgood Eq 0)
  If(good[0] Eq -1) Then Begin
    message, /info, 'No good data' & return
  Endif
  If(bad[0] Ne -1) Then Begin
    rates[bad, *] = 0.0
    rates_u[bad, *] = 0.0
    cfactor[bad, *] = 1.0
    xdata1[bad] = 0.0
  Endif
; Save corrected unsmoothed rates for output
  rates_x = rates
; need nighttime
  nighttime = where(eclipse_flag Eq 1)
;Get background rate, one overall daylight, one running average
  ex_rate = rates & ex_rate[*] = 0
;Upper limit to daytime_bck, to avoid missing flares that start at
;daytime=start and last the whole orbit This has to be a function of
;energy
  bck_daytime_limits = [5.0, 20.0, 10.0, 40.0, 80.0, 50.0, 50.0, 50.0, 2.0]
  max_threshold_level = 200.0   ;test this value, ex_rate must be LT this
  temp_st_en, cgood, st_good, en_good
  For i = 0, nrates_ebands-1 Do Begin
    good = where(rates[*, i] Gt 0)
    If(good[0] Ne -1) Then Begin
      rates[good, i] = temp_despike(rates[good, i], tim_arr[good])
      ex_ratei = temp_running_avg(rates[good, i], nbpts)
      bx_ratei = temp_running_avg(rotate(rates[good, i], 2), nbpts)
      ex_rate[good, i] = ex_ratei
;Use hsi_obs_bck0 for min daylight background
      rate_temp = rates[*, i]
      rate_temp[nighttime] = 0.0
      hsi_obs_bck0, -1, rate_temp, dt, bck_daytime, bck_ssd, /quiet
      bck_daytime = bck_daytime < bck_daytime_limits[i]
;Reset rate
      ex_rate[*, i] = ex_rate[*, i] > bck_daytime
;get threshold_rate
      ex_rate[*, i] = ex_rate[*, i]+$
        nbsigs[0]*sqrt(ex_rate[*, i]/(dt*ndets*cfactor[*, i]))
;don't let ex_rate get too big
      ex_rate[*, i] = ex_rate[*, i] < max_threshold_level
;the cfactor in sigma is in the square root because ex_rate already
;has a cfactor in it.
;Smooth data if necessary, if you're here you know there is good data
      For j = 0, n_elements(st_good)-1 Do Begin
        rates[st_good[j]:en_good[j], i] = $
          temp_smooth(rates[st_good[j]:en_good[j], i], smpts)
      Endfor
    Endif
  Endfor
;smooth xdata1
  For j = 0, n_elements(st_good)-1 Do Begin
    xdata1[st_good[j]:en_good[j]] = $
      temp_smooth(xdata1[st_good[j]:en_good[j]], smpts)
  Endfor
;find intervals with emission, set night time counts to 0 here
  cbad = (cgood Eq 0) Or (Eclipse_flag Eq 1)
  bad = where(cbad, nbad)
  If(nbad Gt 1) Then Begin
    rates[bad, *] = 0.0
    xdata1[bad] = 0.0
  Endif
;Get the flares, if any
  excess_intervals, rates[*, ch0], ex_rate[*, ch0], n_flares, interval_flag, $
    _extra = _extra
  If(n_flares Gt 0) Then Begin
;Drop data gaps longer than 5 minutes
    If(nbad Gt 1) Then Begin
      temp_st_en, cbad, st_bad, en_bad
      For j = 0, n_elements(st_bad)-1 Do Begin
        If(en_bad[j]-st_bad[j] Gt mingap) Then $
           interval_flag[st_bad[j]:en_bad[j]] = 0
      Endfor
;now reset the interval flag
      cflare = interval_flag Gt 0
      ok_flare = where(cflare)
      If(ok_flare[0] Eq -1) Then Begin
        message,  /info, 'No Flares'
        otp_struct = -1
        Return
      Endif
      temp_st_en, cflare, st_fl, en_fl
      iflag0 = interval_flag &  iflag0[*] = 0
      n_flares = n_elements(st_fl)
      For l = 0, n_flares-1 Do iflag0[st_fl[l]:en_fl[l]] = l+1
      interval_flag = temporary(iflag0)
    Endif
;split_flare and check for flat intervals between, use extra smoothing
    rates_ddd = temp_smooth(rates[*, ch0], 2.0*smpts+1)
;    rates_ddd = rates[*, ch0]
    lcl = 0l
    For l = 1, n_flares Do Begin
      ssl = where(interval_flag Eq l,  nssl)
      If(nssl Gt 0) Then Begin
        temp_split_flare, rates_ddd[ssl], x0, x1, $
          ck_thresh = ex_rate[ssl[0], ch0], smpts = 2.0*smpts+1, _extra = _extra
        If(lcl Eq 0) Then Begin
          ss_st = x0+ssl[0] & ss_en = x1+ssl[0] ;start and end subscripts
        Endif Else Begin
          ss_st = [ss_st, x0+ssl[0]] & ss_en = [ss_en, x1+ssl[0]]
        Endelse
        lcl = lcl+1
      Endif
    Endfor
    If(lcl Gt 0) Then Begin
      n_flares = n_elements(ss_st)
      st_fl = ss_st & en_fl = ss_en
    Endif Else n_flares = 0l    ;this should never happen
    hsi_flare_atten_check, atten_state, st_fl, en_fl, _extra = _extra
    If(n_elements(st_fl) Eq 1 And st_fl[0] Eq -1) Then Begin
      message, /info, 'No Flares after atten_state Check, impossible'
      Return
    Endif
;check duration, max_det_vs_tot and min counts
    n_flares = n_elements(st_fl)
    ok_dur = bytarr(n_flares) & ok_all = ok_dur & ok_minct = ok_dur
    For l = 0, n_flares-1 DO Begin
      nss = en_fl[l]-st_fl[l]+1
      If(nss Gt 0) Then Begin
        ss = st_fl[l]+lindgen(nss)
        duration = dt*nss
        If(duration Ge min_dt) Then ok_dur[l] = 1
;Verify that 1 detector's count rate isn't out of wack
        test_value = total(max_det_vs_tot[st_fl[l]:en_fl[l]])/nss
        If(test_value Lt adt[0]) Then ok_all[l] = 1
;check peak uncorrected rate versus minct
        pk_rate = max(rates_u[st_fl[l]:en_fl[l], ch0])
        If(pk_rate Gt minct) Then ok_minct[l] = 1b
      Endif Else Begin
        message, /info, 'No Points in Flare Interval?' 
        ok_dur[l] = 0
      Endelse
    Endfor
    flares = where(ok_dur Eq 1 And ok_all Eq 1 And ok_minct Eq 1, n_flares)
    If(n_flares Eq 0) Then Begin
      If(notquiet) Then Begin
        message, /info, 'No Flares found with dt Gt '+$
          strcompress(string(fix(min_dt)))+' sec, all dets,'+$
          ' with CPS gt'+strcompress(string(fix(minct)))
      Endif
      Return
    Endif Else Begin
      st_fl = st_fl[flares]
      en_fl = en_fl[flares]
    Endelse
;Ok, now reset the interval flag given st_fl and en_fl
    iflag0 = interval_flag &  iflag0[*] = 0
    n_flares = n_elements(st_fl)
    For l = 0, n_flares-1 Do iflag0[st_fl[l]:en_fl[l]] = l+1
;Fill all of the other info, solar or not, position, etc..
;get excess interval info for all energies Gt ch0
    If(ch0 Lt nrates_ebands-1) Then Begin
      iflag = intarr(ntimes, nrates_ebands-1-ch0)
      For i = ch0+1, nrates_ebands-1 Do Begin
        excess_intervals, rates[*, i], ex_rate[*, i], n_fli, $
          interval_flag, _extra = _extra
        If(n_fli Gt 0) Then Begin
          iflag[*, i-ch0-1] = interval_flag
        Endif
      Endfor
    Endif Else iflag = -1
;Call hsi_locate_flare to see if you can get a flare position
;for each flare, if so then it's a flare, also try
;hsi_flare_vfy, which may indicate a flare even if there is no
;position
    solar_flag_1 = bytarr(n_flares)
    ch_top = intarr(n_flares)
    xy_ch0 = fltarr(2, n_flares)
    oti_ch0 = dblarr(2, n_flares)
    roffset_ch0 = fltarr(n_flares)
;set solar flag using xdata1, and its threshold
    solar_flag = bytarr(ntimes)
    sok = where(xdata1 Gt ex_xdata1, nsok)
    If(nsok Gt 0) Then solar_flag[sok] = 1
    For l = 0, n_flares-1 Do Begin
;only bother if solar_flag is not zero in this interval
;test the peak, for nscut check, use uncorrected rate
      pk_rate_u = max(rates_u[st_fl[l]:en_fl[l], ch0]) 
      pk_rate = max(rates[st_fl[l]:en_fl[l], ch0], pkpt)
      pktm = tim_arr[pkpt+st_fl[l]]+dt/2.0
      ftr = [tim_arr[st_fl[l]], tim_arr[en_fl[l]]+dt]
      sflag1 = where(solar_flag[st_fl[l]:en_fl[l]] Gt 0)
      sflag1a = hsi_flare_vfy(tim_arr[[st_fl[l], en_fl[l]]], energy_range_found, nsigma = 5.0)
      If(sflag1[0] Eq -1 And sflag1a Eq 0) Then Begin ;just skip to the next flare
        If(pk_rate_u Lt nscut) Then solar_flag_1[l] = 255 ;will get rid of this
      Endif Else Begin
;test for ok intervals
        hsi_ok_intv, ftr, 300.0, tarrx, ss_stx, ss_enx, ist_time, ien_time, $
                     obs_summ_obj = obs_summ_obj, dt_min = 8.0, /max_size, $
                     /check_packets, _extra = _extra
;For each interval, do a roll solution test
        ntx = n_elements(ss_stx)
        roll_flag = bytarr(ntx)
        For j = 0, ntx-1 Do $
           roll_flag[j] = hsi_pmtras_test([ist_time[j],ien_time[j]])
        ok_roll = where(roll_flag Eq 0, nok_roll)
        If(nok_roll Gt 0) Then Begin
           ist_time = ist_time[ok_roll]
           ien_time = ien_time[ok_roll]
        Endif Else Begin        ;try this once more, shorter time, without /max_size set
        hsi_ok_intv, ftr, 180.0, tarrx, ss_stx, ss_enx, ist_time, ien_time, $
                     obs_summ_obj = obs_summ_obj, dt_min = 8.0, $
                     /check_packets, _extra = _extra
           ntx = n_elements(ss_stx)
           roll_flag = bytarr(ntx)
           For j = 0, ntx-1 Do $
              roll_flag[j] = hsi_pmtras_test([ist_time[j],ien_time[j]])
           ok_roll = where(roll_flag Eq 0, nok_roll)
           If(nok_roll Gt 0) Then Begin
              ist_time = ist_time[ok_roll]
              ien_time = ien_time[ok_roll]
           Endif Else Begin
              message, /info, 'Not a good time for roll solutions'
              suspect_solution = 1b
           Endelse
        Endelse     
;Try position
        hsi_xy_test_new, ftr, pktm, obs_summ_obj, okxy_12, $
          oti_12, xy_12, echannel = ch0, quiet = quiet, $
          st_time_in = ist_time, en_time_in = ien_time, _extra = _extra
;New, check AIA data for flare position
        xy_aia = -1
        If(pktm Gt anytim('1-jan-2011 00:00')) Then Begin
           xy_aia = hsi_hek_ftest(ftr, pktm, _extra = _extra)
           If(n_elements(xy_aia) Gt 1) Then Begin
              If(okxy_12 Eq 0) Then Begin
                 message, /info, 'No RHESSI position, Using AIA position: '
                 print, xy_aia
                 okxy_12 = 1b
                 xy_12 = xy_aia
                 If(ftr[1]-ftr[0] Gt 600.0) Then Begin
                    oti_12 = pktm+[-300.0, 300.0] ;five minutes around the peak time
                 Endif Else oti_12 = ftr
              Endif; Else Begin
                ; rdiff = sqrt(total((xy_aia-xy_12)^2))
                ; If(rdiff Gt 120.0) Then Begin ;use AIA
                ;    message, /info, 'Maybe bad RHESSI position, Using AIA position: '
                ;    print, xy_aia
                ;    okxy_12 = 1b
                ;    xy_12 = xy_aia
                ;    If(ftr[1]-ftr[0] Gt 600.0) Then Begin
                ;       oti_12 = pktm+[-300.0, 300.0] ;five minutes around the peak time
                ;    Endif Else oti_12 = ftr
                ; Endif
              ;Endelse
           Endif
        Endif
;If hsi_xy_test_new returns a position, Flare!!
        If(okxy_12) Then Begin
          solar_flag_1[l] = 1
          oti_ch0[*, l] = oti_12
          xy_ch0[*, l] = xy_12
;for each energy band above ch0, test for emission, try position
          yes_hix = bytarr(nrates_ebands)
          yes_hix[0:ch0] = 1
          For i = ch0+1, nrates_ebands-1 Do Begin
            print, 'TESTING:', rates_ebands[i:i+1]
            oki = where(iflag[st_fl[l]:en_fl[l]] Gt 0, noki)
            If(noki Gt 0 And yes_hix[i-1] Eq 1) Then Begin
              st_fli = st_fl[l]+oki[0] & en_fli = st_fl[l]+oki[noki-1]
              ftr = [tim_arr[st_fli], tim_arr[en_fli]+dt]
              pk_rate = max(rates[st_fli:en_fli, i], pkpt)
              pktm = tim_arr[pkpt+st_fli]+dt/2.0
              hsi_xy_test_new, ftr, pktm, obs_summ_obj, okxy_i, $
                otii, xyi, echannel = i, quiet = quiet, $
                st_time_in = ist_time, en_time_in = ien_time, $
                /verify, xy_in = xy_ch0[*, l], _extra = _extra
              If(okxy_i Gt 0) Then Begin
                yes_hix[i] = 1
;Here you are explicitly setting position and image_time to the
;highest energy detected
;                oti_ch0[*, l] = otii
;                xy_ch0[*, l] = xyi
              Endif
            Endif
          Endfor
;here you are guaranteed to have xy_ch0, oti_ch0
          roffset_ch0[l] = hsi_radial_offset(absolute_time = oti_ch0[*, l], $
                                             xyoffset = xy_ch0[*, l])
          ch_top[l] = max(where(yes_hix Gt 0))
        Endif Else Begin
          solar_flag_1[l] = 2 ;here you may have a flare, but cannot verify position
          If(pk_rate_u Lt nscut) Then solar_flag_1[l] = 255 ;will get rid of this
        Endelse
      Endelse
    Endfor
;Create structures for background info
    bck_in_proto = {bck_rate_time:dblarr(2), bck_rate:fltarr(2)}
    bck_in = replicate(bck_in_proto, n_flares)
    For l = 0, n_flares-1 DO Begin
      If(solar_flag_1[l] Eq 1) Then Begin ;background was used
        bck_ss = [st_fl[l]-nbpts, st_fl[l]-1]
        bck_ss = bck_ss > 0
        nbck = bck_ss[1]-bck_ss[0]+1
        If(nbck Eq 1) Then Begin
          bck_rate = total(rates[bck_ss[0], ch0])
        Endif Else Begin
          bck_rate = total(rates[bck_ss[0]:bck_ss[1], ch0])/nbck
        Endelse
        bck_in[l].bck_rate = [bck_rate, sqrt(bck_rate/rinfo.time_intv)]
        bck_in[l].bck_rate_time = [tim_arr[bck_ss[0]], $
                                   tim_arr[bck_ss[1]]+rinfo.time_intv]
      Endif
    Endfor
    keep_sflag = where(solar_flag_1 Ne 255, nkeep_sflag)
    If(nkeep_sflag Gt 0) Then Begin
      ks = keep_sflag
      otp_struct = {ch0:ch0, st_fl:st_fl[ks], en_fl:en_fl[ks], $
                    solar_flag:solar_flag, solar_flag_1:solar_flag_1[ks], $
                    bck_in:bck_in[ks], xy_ch0:xy_ch0[*, ks], $
                    hi_channel:ch_top[ks], oti_ch0:oti_ch0[*, ks], $
                    deti:deti, ndets:ndets, eb:rates_ebands[ch0:ch0+1], $
                    roffset_ch0:roffset_ch0[ks]}

    Endif Else otp_struct = -1
  Endif Else otp_struct = -1
End
