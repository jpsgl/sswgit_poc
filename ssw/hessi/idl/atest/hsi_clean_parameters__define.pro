;+
; PROJECT:
;	HESSI
; NAME:
;	HSI_CLEAN_PARAMETERS__DEFINE
;
; PURPOSE:
;
;
; CATEGORY:
;	Util
;
; CALLING SEQUENCE:
;
;
; CALLS:
;	none
;
; INPUTS:
;       none
;
; OPTIONAL INPUTS:
;	none
;
; OUTPUTS:
;       none, function returns parameter structure.
;
; OPTIONAL OUTPUTS:
;	none
;
; KEYWORDS:
;	none
; COMMON BLOCKS:
;	none
;
; SIDE EFFECTS:
;	none
;
; RESTRICTIONS:
;	none
;
; PROCEDURE:
;	none
;
; MODIFICATION HISTORY:
;	Version 1, richard.schwartz@gsfc.nasa.gov, 26-apr-1999.
;	Version 2, richard.schwartz, 1-nov-1999.
;	10-Apr-2003, Kim.  Added cw_inverse
; 16-Sep-2008, Kim.  Added chi_sq_min_test, show_n_maps, show_n_chi
;	9-jul-2009, ras, added beam_width_factor to condition output map with convolving beam
;	30-Apr-2010, Kim, added regress_combine to use regress to combine component and residual map
;		1-apr-2013, ras, removed ref to params not used, sigma, sigma_beam,
;			sigma_factor, tau, lambda, mu, nu, nwindow
;			added OLD_REGRESS_METHOD - known as CLEAN_OLD_REGRESS_METHOD externally
;     11-mar-2016, ras, allow input_dirty_map through control parameters
;  15-nov-2017, ras, added rsd_regress_method,  if set, use residuals in the regression model explicitly, RAS, 6 nov 2017, default 0
;set this to get the regression method used prior to 15-nov-2017
; 2-apr-2018, RAS, ('CLEAN_')REGRESS_METHOD is now a string with four possible values
;   clean_old_regress_method and clean_rsd_regress_method have no effect now. They have
;   been removed. 
;-
pro hsi_clean_parameters__define

dummy = {hsi_clean_parameters, $
	niter: 0, $
	more_iter: 0, $
	negative_max_test: 0, $
	chi_sq_min_test: 0, $
	frac: 0.0, $
	chi_sq_crit: 0.0, $
	no_chi2: 0, $
	show_maps: 0, $
	show_n_maps: 0, $
	show_map_xdim: 0 , $
	show_chi: 0, $
	show_n_chi: 0, $
	clean_box: ptr_new() , $
	cw_list: ptr_new(), $
	cw_nop: ptr_new(), $
	cw_inverse: 0, $
	progress_bar: 0 , $
	mark_box: 0 , $
	media_mode: 0, $
	;These don't rep
	beam_width_factor: 0.0, $ ;narrows the clean_beam convolving beam for source appearance
	
	regress_combine: '', $
	input_dirty_map: ptr_new()}
	 
end
