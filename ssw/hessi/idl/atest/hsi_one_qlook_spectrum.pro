;+
;NAME:
; hsi_one_qlook_spectrum
;PURPOSE:
; Given a flare list entry,
; Create a set of quick-look spectra 
;CALLING SEQUENCE:
Pro hsi_one_qlook_spectrum, data, $
                            dt_use = dt_use, $
                            dt_min = dt_min, $
                            spectrum_file_dir = spectrum_file_dir, $
                            spectrum_plot_dir = spectrum_plot_dir, $
                            specfile_dir = specfile_dir, $
                            srmfile_dir = srmfile_dir, $
                            filedb_dir = filedb_dir, $
                            no_filedb_write = no_filedb_write, $
                            no_database_management = no_database_management, $
                            quiet = quiet, $
                            _extra = _extra
;INPUT:
; data = a structure of type {hsi_flarelistdata} containing a
;                    flare list data for a flare, one at a time, please
;OUTPUT:
; spec_obj = an spectrum object, includes the spectrum, of course
;KEYWORDS:
; dt_use = the nominal time interval, the default is 60.0 sec if the
;          flare is more than 10 minutes long, 20 sec if the flare is
;          between 2 and 10 minutes long, 12 sec for shorter than 2
;          mins, passed into the hsi_ok_intv call.
; dt_min = the minimum time interval, the default is 12.0 sec if the
;          flare is more than 10 minutes long, 8 sec if the flare is
;          less than 10 minutes long
; spectrum_file_dir = a directory for the output fits files, the
;                     default is '$HSI_QLOOK_SPECTRUM_FILE_DIR'
;                     The files are in yyyy/mm/dd subdirectories
; spectrum_plot_dir = a directory for the output fits files, the
;                     default is '$HSI_QLOOK_SPECTRUM_PLOT_DIR'
;                     The files are in yyyy/mm/dd/flare_id
;                     subdirectories.
; no_filedb_write = if set, don't write a new filedb or delete old
;                   files, this is for testing purpoese, when you're
;                   not messing with the database
; no_database_management = if set, don't do any filedb, or plotting or
;                          anything, just the spectra, thanks...
;HISTORY:
; 10-may-2001,  jmm, jimm@ssl.berkeley.edu
; 13-nov-2001, jmm, Gotta use the filename, changed seg_index to
; seg_index_mask
; 28-may-2002, jmm, Uses obs_time_interval instead of filename
; 2-nov-2004, jmm, Completely rewritten, and designed to be called
;                  from hsi_do_qlook_image, to only have 2 processes
;                  running at one time.
; 5-nov-2004, jmm, Changed the name to hsi_one_qlook_spectrum
; 21-nov-2005, jmm, added no_filedb_write keyword, for testing
; 6-oct-2009, jmm, Calculates photon flux and total photon fluence,
;                  writes back into flare list.
;
;-
  If(keyword_set(spectrum_file_dir)) Then spexfiledir = spectrum_file_dir $
  Else spexfiledir = '$HSI_QLOOK_SPECTRUM_FILE_DIR'
  If(keyword_set(spectrum_plot_dir)) Then spexplotdir = spectrum_plot_dir $
  Else spexplotdir = '$HSI_QLOOK_SPECTRUM_PLOT_DIR'
  If(Keyword_set(specfile_dir)) Then specdir = specfile_dir $
  Else specdir = '$HSI_QLOOK_SPECFILE_DIR'
  If(Keyword_set(srmfile_dir)) Then srmdir = srmfile_dir $
  Else srmdir = '$HSI_QLOOK_SRMFILE_DIR'
  dir_old = '$HSI_OLD_LEVEL0_DIR'
;Check data input
  If(is_struct(data) Eq 0) Then message, 'No data structure input'
  If(n_elements(data) Gt 1) Then message, /info, 'One flare at a time please'
  dflr = data[0]
  fid = dflr.id_number
  fid_str = strcompress(string(fid), /remove_all)
  date = strmid(time2file(dflr.start_time), 0, 8)
;catch
  err_xxx = 0
  catch, err_xxx
  If(err_xxx Ne 0) Then Begin
     catch, /cancel
     Print, 'Error'
     help, /last_message, output = err_msg
     For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
     print, 'Returning'
;Move the specfile,
     If(is_string(specfile)) Then Begin ;not necessarily true
        full_specdir = hsi_mk_dbase_dir(date, specdir)
        cmd = '/bin/mv '+specfile+' '+full_specdir
        If(Not keyword_set(quiet)) Then message, /info, 'Spawning: '+cmd
        spawn, cmd
     Endif
;Move the srmfile,
     If(is_string(srmfile)) Then Begin ;this has gotta be true
        full_srmdir = hsi_mk_dbase_dir(date, srmdir)
        cmd = '/bin/mv '+srmfile+' '+full_srmdir
        If(Not keyword_set(quiet)) Then message, /info, 'Spawning: '+cmd
        spawn, cmd
     Endif
;Delete files if necessary
     If(is_string(del_file)) Then Begin
        For j = 0, n_elements(del_file)-1 Do Begin
           cmd = '/bin/mv '+del_file[j]+' '+dir_old
           If(Not keyword_set(quiet)) Then $ 
              message, /info, 'Spawning: '+cmd
           spawn, cmd
        Endfor
     Endif
     Return
  Endif
;That's it, get the spectral data
  oti = [dflr[0].start_time, dflr[0].end_time]
  hsi_qspec, oti, specfile, srmfile, specfile_dir = specfile_dir, $
             srmfile_dir = srmfile_dir, _extra = _extra
  If(is_string(specfile) Eq 0) Then message, 'Bad spectrum file'
  If(is_string(srmfile) Eq 0) Then message, 'Bad srm file'
;Given the size of your flare, choose an interval size, 
  duration = oti[1]-oti[0]
  If(keyword_set(dt_use)) Then dt = dt_use Else dt = 60.0
  If(keyword_set(dt_min)) Then dtmin = dt_min Else dtmin = 8.0
;Get ok intervals
  hsi_ok_intv, oti, dt, tim_arr, ss_st, ss_en, st_time, en_time, $
               dt_min = dtmin, /check_packets, /no_aspect, ss_ok = ss_ok, _extra = _extra
  If(ss_st[0] Eq -1) Then message, 'No good spectral intervals'
;still going?
  hsi_qlook_call_ospex, specfile, srmfile, st_time, en_time, o, $
                        spex_file, spex_plot_file, spectrum_file_dir = spectrum_file_dir, $
                        spectrum_plot_dir = spectrum_plot_dir, $
                        brate_out = brate1, sbrate_out = sbrate1, extra = _extra
;1-oct-2009, add inverted photon spectra, compiled with 4 second time
;resolution for the good intervals
  If(ss_ok[0] Ne -1) Then Begin
     st_timex = tim_arr[ss_ok]
     en_timex = st_timex+4.0
     hsi_invert_spectrum, specfile, srmfile, st_timex, en_timex, $
                          spectrum, uspectrum, ebands, $
                          brate_in = brate1, sbrate_in = sbrate1, $
                          tdata_out = tdata_out, $
                          minchan = 3, _extra = _extra
;Compile 'photon_flux' for total and peak, and efold
     If(spectrum[0] Ne -1) Then Begin
;first integrate over energy
        spectrum = spectrum > 0.0
        de = ebands[1:*]-ebands
        tspec = reform(de#spectrum)
        utspec = reform(sqrt(de#uspectrum^2))
;Get peak values
        pk_tspec = max(tspec, pkpt)
        upk_tspec = utspec[pkpt]
;For total and efolding values, interpolate to 1 second time resolution, 
        nss_ok = n_elements(ss_ok)
        If(nss_ok Eq 1) Then Begin
           tot_tspec = pk_tspec*4.0 & utot_tspec = upk_tspec*4.0
        Endif Else Begin
           ct = tim_arr-tim_arr[0]+2.0 ;center time
           nsec = 1L+long(max(ct)-min(ct))
           ct1 = dindgen(nsec)+ct[0]
;If you had bins they'd be from 1.5 second on...
           tspec1 = interpol(tspec[ss_ok], ct[ss_ok], ct1)
           utspec1 = interpol(utspec[ss_ok], ct[ss_ok], ct1)
           tot_tspec = total(tspec1)
           utot_tspec = sqrt(total(utspec^2))
;Now from wherever the value goes from 1/e times the peak to 1/e times
;the peak, since peaks are split, then you are safe doing this:
           over_e = where(tspec1 Gt pk_tspec/exp(1.0), nover_e)
           If(nover_e Gt 0) Then Begin
              e_tspec = total(tspec1[over_e])
              ue_tspec = sqrt(total(utspec[over_e]^2))
           Endif Else Begin     ;this should never happen...
              e_tspec = pk_tspec & ue_tspec = upk_tspec
           Endelse
           dflr.peak_phflux = pk_tspec
           dflr.tot_phfluence = tot_tspec
           dflr.e_phfluence = e_tspec
           dflr.peak_phflux_sigma = upk_tspec
           dflr.tot_phfluence_sigma = utot_tspec
           dflr.e_phfluence_sigma = ue_tspec
           data[0] = dflr       ;to make sure the change gets into the qflare_list file
        Endelse
     Endif
  Endif

;Database management
  If(keyword_set(no_database_management)) Then Begin
     message, /info, 'No Database management, Returning'
     Return
  Endif
  If(is_string(spex_file)) Then Begin
;Do you have a file for this flare id? If so, get the version number
;and set filedb_status to -1
     qls_fb = hsi_mult_filedb_inp(filedb_dir = filedb_dir, $
                                  file_type = 'QLOOK_SPECTRUM', $
                                  quiet = quiet, $
                                  qfiledb_trange = qls_qfb_trange, $
                                  /dont_copy_old_files, $
                                  _extra = _extra)
     If(is_struct(qls_fb)) Then Begin
        nqfb = n_elements(qls_fb)
        fb_fid = strarr(nqfb)
        For j = 0, nqfb-1 Do Begin
           ppp = strsplit(qls_fb[j].file_id, '_', /extract)
           fb_fid[j] = ppp[2]
        Endfor
        hfile = where(fb_fid Eq fid And qls_fb.status_flag Ge 0, nhfile)
        If(nhfile Gt 0) Then Begin
           verno = hsi_get_file_verno(qls_fb[hfile].file_id)
           vvv = max(verno, max_verno)
           verno = vvv[0]
           new_verno = verno+1
           qls_fb[hfile].status_flag = -1
           del_file = qls_fb[hfile].file_id
;;Set the status flag to -1, and delete files
           qls_fb[hfile].status_flag = -1
           del_file = qls_fb[hfile].file_id
        Endif Else new_verno = 0
     Endif Else new_verno = 0
     filex0 = 'hsi_qlspc_'+fid_str+'_000.fits'
     filex0 = hsi_set_file_verno(filex0, new_verno)
     file_dir = hsi_mk_dbase_dir(date, spexfiledir)
     filex = concat_dir(file_dir, filex0)
     If(is_string(del_file)) Then begin
        del_file = concat_dir(file_dir, del_file)
     Endif
     cmd = '/bin/mv '+spex_file+' '+filex
     If(Not keyword_set(quiet)) Then message, /info, 'Spawning: '+cmd
     spawn, cmd
;Get a new filedb structure
     qls_fb_new = hsi_filedb_version_control()
     qls_fb_new.file_id = filex0
;You need a time range, use the flare time range
     qls_fb_new.start_time = dflr.start_time
     qls_fb_new.end_time = dflr.end_time
;Next, append the filedb
     If(is_struct(qls_fb)) Then Begin
        qls_fb = [qls_fb, qls_fb_new]
     Endif Else qls_fb = qls_fb_new
;Only output for this time range and qtime_range
     If(qls_qfb_trange[0] Ne -1 And total(qls_qfb_trange) Gt 0) Then Begin
        all_times = [qls_qfb_trange, qls_fb_new.start_time, $
                     qls_fb_new.end_time]
     Endif Else all_times = [qls_fb_new.start_time, qls_fb_new.end_time]
     all_time_range = [min(all_times), max(all_times)]
;Write the new filedb, if asked for
     If(Not keyword_set(no_filedb_write)) Then Begin
        hsi_filedb_sort, qls_fb
        ok = where(qls_fb.status_flag Ge 0, nok)
        If(nok Gt 0) Then Begin
           hsi_filedb_write, qls_fb[ok], filedb_dir = filedb_dir, $
                             filename = 'hsi_qlspc_filedb.fits', $
                             filedb_trange = all_time_range, /temp
           fdbdir_pub = '$HSI_FILEDB_PUB'
           hsi_filedb_write, qls_fb[ok], filedb_dir = fdbdir_pub, $
                             filename = 'hsi_qlspc_filedb.fits', $
                             filedb_trange = all_time_range, /temp
        Endif
     Endif
  Endif
;Move the plot files
  If(is_string(spex_plot_file)) Then Begin
     plot_file_dir = hsi_mk_dbase_dir(date, spexplotdir)
;    plot_file_dir = concat_dir(plot_file_dir, fid_str)
     test = findfile(plot_file_dir, count = bb)
     If(bb eq 0) Then Begin     ;Either the dir isn't there or there are no files
        If(Not keyword_set(quiet)) Then $
           message, /info, 'Creating: '+plot_file_dir
        spawn, 'mkdir '+plot_file_dir
     Endif Else If(Not keyword_set(quiet)) Then $
        message, /info, plot_file_dir+' Exists'
;There may be more than one
     np = n_elements(spex_plot_file)
     For j = 0, np-1 Do Begin
        If(is_string(spex_plot_file[j])) Then Begin 
           cmd = '/bin/mv '+spex_plot_file[j]+' '+plot_file_dir
           If(Not keyword_set(quiet)) Then message, /info, 'Spawning: '+cmd
           spawn, cmd
        Endif
     Endfor
  Endif
;Move the specfile,
  If(is_string(specfile)) Then Begin ;this has gotta be true
     full_specdir = hsi_mk_dbase_dir(date, specdir)
     cmd = '/bin/mv '+specfile+' '+full_specdir
     If(Not keyword_set(quiet)) Then message, /info, 'Spawning: '+cmd
     spawn, cmd
  Endif
;Move the srmfile,
  If(is_string(srmfile)) Then Begin ;this has gotta be true
     full_srmdir = hsi_mk_dbase_dir(date, srmdir)
     cmd = '/bin/mv '+srmfile+' '+full_srmdir
     If(Not keyword_set(quiet)) Then message, /info, 'Spawning: '+cmd
     spawn, cmd
  Endif
;Delete files if necessary
  If(Not keyword_set(no_filedb_write)) Then Begin
     If(is_string(del_file)) Then Begin
        For j = 0, n_elements(del_file)-1 Do Begin
           cmd = '/bin/rm '+del_file[j]
           If(Not keyword_set(quiet)) Then $ 
              message, /info, 'Spawning: '+cmd
           spawn, cmd
        Endfor
     Endif
  Endif
;Dude, you're finished, clean up objects
  If(obj_valid(o)) Then obj_destroy, o
  free_all_lun
  heap_gc
  Return       
  End
;---------------------------------------------------------------------------
; End of 'hsi_one_qlook_spectrum.pro'.
;---------------------------------------------------------------------------
