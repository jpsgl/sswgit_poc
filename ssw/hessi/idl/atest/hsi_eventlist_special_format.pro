;+
;Name: HSI_EV2EV
;
;Purpose: This function makes and eventlist over a narrow time window and adds
;	special tags to help understand what may be saturating events that overload
;	either or both the analog and digital electronics
;
;Useage:
;
;		IDL> event_time = '2003-Sep-02 06:42:17.253' ; center of eventlist time
;		IDL> dt = 0.2 ;interval length in seconds
;		IDL> out = hsi_eventlist_special_format( event_time, 0.2 )
;		IDL> help, out
;		OUT             STRUCT    = -> <Anonymous> Array[1]
;		IDL> help, out
;		OUT             STRUCT    = -> <Anonymous> Array[1]
;		IDL> help, out,/st
;		** Structure <77bb178>, 2 tags, length=41072, data length=30028, refs=1:
;		   EV_SPECIAL      STRUCT    -> <Anonymous> Array[1004]
;		   TIMESTAMP       STRUCT    -> TIMESTAMP Array[76]
;		IDL> timestamp = out.timestamp & evs = out.ev_special
;		IDL> help, timestamp, evs
;		TIMESTAMP       STRUCT    = -> TIMESTAMP Array[76]
;		EVS             STRUCT    = -> <Anonymous> Array[1004]
;		IDL> help, timestamp, evs,/st
;		** Structure TIMESTAMP, 2 tags, length=12, data length=12:
;		   PACKET_REF      LONG      Array[2]  ;packet number and position in packet
;		   VALUE           LONG          29662676 ;timestamp value from data longword from packet
;		** Structure <74da888>, 8 tags, length=40, data length=29, refs=2:
;		   TIME            LONG                 0
;		   A2D_INDEX       BYTE         9
;		   CHANNEL         INT            191
;		   COINCID_MASK    BYTE         0
;		   TIME_OFFSET     LONG64                   -112092
;		   ENERGY_KEV      FLOAT           63.5668
;		   PACKET_REF      LONG      Array[2]   ;packet number and position in packet
;		   SEG_ID          BYTE         9
;
;		NB- the energies defined for reset and uld events are as follows
;			ev_special[zuf].energy_keV = 5e3 ;front uld
;			ev_special[zur].energy_keV = 3e4 ;rear uld
;			ev_special[zr].energy_keV  = 6e4 ;reset any segment
;
;	IDL> for i=380,420 do print, ev[i].time_offset, ev[i].energy_kev, ev[i].seg_id, ev[i].packet_ref
;                  -665      60000.0   4           1         132
;                  -663      30000.0  13           1         133
;                  -662      60000.0   8           1         134
;                  -663      60000.0   5           1         135
;                  -662      30000.0  14           1         136
;                  -660      60000.0   4           1         137
;                  -659      60000.0   5           1         138
;                  -660      60000.0   8           1         139
;                  -659      60000.0   2           1         140
;                  -658      30000.0  17           1         141
;                  -657      60000.0   5           1         142
;                  -657      60000.0   8           1         143
;                  -656      60000.0   2           1         144
;                  -655      30000.0  12           1         145
;                  -657      60000.0   4           1         146
;                  -655      5000.00   5           1         147
;                  -653      30000.0  16           1         148
;                  -654      5000.00   8           1         149
;                  -652      60000.0   5           1         150
;                  -652      30000.0  15           1         151
;                  -656      30000.0  11           1         152
;                  -650      30000.0   9           1         153
;                   370      60000.0   2           1         154
;                   370      5000.00   3           1         155
;                   371      5000.00   4           1         156
;                   374      60000.0   5           1         157
;                   377      60000.0   4           1         158
;                   377      60000.0   3           1         159
;                   379      60000.0   6           1         160
;                   376      5000.00   2           1         161
;                   380      30000.0  15           1         162
;                   381      30000.0   9           1         163
;                   380      60000.0   2           1         164
;                   381      60000.0   4           1         165
;                   380      60000.0   6           1         166
;                   384      5000.00   6           1         167
;                   385      60000.0   0           1         168
;                   385      30000.0  13           1         169
;                   386      60000.0   5           1         170
;                   387      60000.0   4           1         171
;                   388      60000.0   6           1         172
;
;
;Input:
;	eventlist_time - anytim() readable format, get eventlist centered on this time
;	interval length - in seconds. length of eventlist in time centered on eventlist_time
;Optional Keyword Inputs
;	obj - pass in or return the packet object used - app_id 100 only
;	packet_time - times in hsi_sctime formats in the packet headers
;History:
;	2002, richard.schwartz@nasa.gov
;-
function hsi_eventlist_special_format, event_time, dt, $
	obj=obj, $
	packet_time = packet_time, $
	_extra=_extra

nobj = arg_present( obj )
if ~is_object( obj ) || ~is_obj( obj->get(/obj, class= 'hsi_packet')) then obj=hsi_packet()
obj->set, app_id = 100
obj->Set, _extra=_extra

default, dt, .2 ;0.2 total seconds around event_time
dt = dt > 0.2
if keyword_set( event_time) then begin
	obs_time= anytim( event_time ) + dt[0] * [-0.5, 0.5]
	obj->set, obs_time_interval = obs_time
	endif
sdata = obj->getdata()
pd = obj->getdata(class='hsi_packet')
if nobj then obj_destroy, obj

hsi_app100_unpack, pd, packet_collect_time, ev, ut_ref, last, nbad=nbad
if nbad ge 1 then message,'Timestamps out of order. investigate by hand. Indicates bad telemetry'
data = long( pd.data[6:*], 0, 270 * n_elements(pd) )
byteorder, data, /ntohl
source = mask( data, 27, 5)
ztstamp = where( source eq 31, nzt, comp=zevent)
ev_special = hsi_ev2ev( ev )
ev_special = ev_special[0:last] ;the remainder were timestamps
if nzt gt 0 then begin
	timestamp  = replicate( {timestamp, packet_ref: lonarr(2), value: 0L }, nzt)
	timestamp.value = mask( data[ztstamp], 0, 27 )
	timestamp.packet_ref[0] = ztstamp / 270
	timestamp.packet_ref[1] = ztstamp mod 270
	endif


ev_special = add_tag( ev_special, 0LL, 'time_offset' )
ev_special = add_tag( ev_special, 0.0, 'energy_keV' )
ev_special = add_tag( ev_special, lonarr(2), 'packet_ref') ;packet number, valid data num in packet
ev_special = add_tag( ev_special, 0B, 'seg_id' )

packet_time64 = hsi_sctime_convert( packet_collect_time, /L64)
event_time64 = hsi_any2sctime( event_time, /L64 )
time64 = hsi_sctime_convert(/L64, ev_special.time ) + hsi_sctime_convert( /L64, ut_ref )
dt_time64 = time64 - event_time64
dt_packet_time64 = packet_time64 - event_time64

ev_special.time_offset = dt_time64
a2d = ev_special.a2d_index
chn = ev_special.channel

;
;Energy in keV
;Get coefficients
cf = hsi_get_e_edges(/coef, gain_time_wanted = ( anytim(obs_time, /utc_int) )[0] )
;cf is 27 x 2, 27 a2d x [energy offset in keV, gain in keV/channel]
zuf = where( chn eq -1 and a2d lt 9, nzuf) ;ULD
zur = where( chn eq -1 and a2d ge 9, nzur)
zr  = where( chn eq -2, nzr) ;Reset/Overload

ev_special.energy_keV = cf[a2d, 0] + cf[a2d, 1] * chn
ev_special[zuf].energy_keV = 5e3
ev_special[zur].energy_keV = 3e4
ev_special[zr].energy_keV  = 6e4

ev_special.packet_ref[0] = zevent / 270
ev_special.packet_ref[1] = zevent mod 270
npd = last_item( ev_special.packet_ref[0] )
for i= 0, npd do begin
	z = where( ev_special.packet_ref[0] eq i)
	ev_special[z].packet_ref[1] = sort(z)
	endfor

ev_special.seg_id = (a2d mod 9 ) + 9 * (((a2d-8)<1)>0)

return, exist( timestamp ) ? { ev_special: ev_special, timestamp: timestamp } : ev_special
end