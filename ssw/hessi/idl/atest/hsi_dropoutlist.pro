;+
; :Description:
;    Returns ramp intervals.  Look for gt ntest consecutive high values to find ramps.
;
; :Params:
;    evchannel from - eventlist structure with channel, time, and a2d_index
;        IDL> help, ev,/st
;        ** Structure HSI_EVENT, 4 tags, length=12, data length=8:
;        TIME            LONG                 0
;        A2D_INDEX       BYTE         8
;        CHANNEL         INT             26
;        COINCID_MASK    BYTE         0;
; :Keywords:
;    ntest - consecutive high values
;    channel_max - top ramp value, default, 1000
;    chan_min    - median of chan distribution, default from ev.channel
;    chan_lim    - bottom channel for ramp test
;    ch_bulk_lim - upper limit on energies used for median
;
; :Author: raschwar, 29-jan-2017
;-
function hsi_dp_ramp, evchannel, ntest=ntest, $
  channel_max = channel_max, chan_min = chan_min, $
  chan_lim = chan_lim, $
  ch_bulk_lim = ch_bulk_lim

  two20 = 2d0^20
  default, ntest, 5
  default, channel_max, 1000
  ec = [0,evchannel,0] < channel_max
  nec = n_elements( ec )
  q  = where( ec lt ch_bulk_lim, nq)

  default, chan_min, median( ec[q]  )       ;bulk channel
  ;help, chan_min
  default, chan_lim, chan_min + 2* stddev( ec< 100) ;thresh for high chan
  if ec[nec-1] le chan_lim then ec[nec-1] = chan_lim + 1 ; terminate the end with an up or a down
  high = where( ec gt chan_lim, nq )
  break = where( high[1:*]-high gt 1, nbreak)
  ;test the chains
  istart = [high[0], high[break+1]]
  iend   = high[break]
  if n_elements(iend) lt n_elements(istart) and last_item(istart) eq (nec-1) then istart = istart[0:n_elements( istart )-2]
  istart = istart[1:*]-1 & iend = iend[1:*]
  test   = where( ( iend - istart ) ge (ntest-1), ntest)
  if ntest ge 1 then begin
    istart = istart[ test ]
    iend   = iend[ test]

    if ntest gt 1 then begin
      ;Test to extend istart
      z = where( evchannel[istart-1] gt chan_lim and (istart-1) ne 0, nz)
      if nz ge 1 then istart[z]--
      ;Test to extend iend. When ecc reaches chan_min
      qmin = where( ec[1:*] le chan_min, nqmin )
      iend = qmin[ value_locate( qmin, iend ) + 1]

    endif
    ;ramps = transpose( [[istart], [iend]])
    ;merge ramps
    rmask = bytarr(nec)
    istart <=iend
    for i=0,n_elements(istart)-1 do rmask[istart[i]:iend[i]]=1
    find_changes, rmask,index,state, index2d=ramps
    z=where( state, nz)
    if nz ge 1 then ramps = ramps[*,z] else ramps=0




  endif else ramps = 0
  ;Merge any ramp overlap

  return, ramps
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro hsi_dropoutlist_livetimelist_zero, seg=seg,  lt_ut_ref_sec79, livetimelist, drop_out, ut_ref, $
  rev = rev, hseg=hseg ;double(drop_out)/two20+ut_ref
  two20 = 2.0d0^20
  drop_out_sec = drop_out / two20 + ut_ref

  if ~exist(hseg) then hseg = histogram( min = 0, max=17, livetimelist.seg_index, rev = rev )
  if hseg[seg]<1 then begin
    z = reverseindices( rev, seg, count = count )
    if count ge 1 then begin
      time = livetimelist[z].time / 2048.0d0 + lt_ut_ref_sec79

      ig = where( value_locate( drop_out_sec[*], time ) mod 2 +1 eq 1, nig) ; ig are in gaps
      ;set counter to -5 during gaps, new, ras, 2-feb-2017
      if nig ge 1 then livetimelist[z[ig]].counter = -5 ;zero out livetimelist values in gaps
    endif
  endif
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pro hsi_dropout_extend_4_valid, time, channel, drop_out, dpxi=dpxi
  ;extend dropout intervals in both directions so that they start from a valid events and end in valid events

  ;channel has csa, uld, and only valid channels above LLD but there may be zero channels so should


  ;
  z = where(channel gt 0, nz) ;already cleared events below lld thresh
  channelz = channel[z]
  timez = time[z]
  drop0 = dpxi.dp_prepend_nvalid<1 ? timez[ (value_locate( time[z], drop_out[0,*]))[*] - dpxi.dp_prepend_nvalid > 0] : reform( drop_out[0,*] )
  drop1 = dpxi.dp_append_nvalid<1 ? timez[ (value_locate( time[z], drop_out[1,*]))[*] + dpxi.dp_append_nvalid < (nz-1)] : reform( drop_out[1,*] )
  drop_out = transpose( [[ drop0 ], [ drop1 ]] )

  ;Clean up overlap, ras, 1-feb-2017
  if n_elements( drop_out ) ge 4 then begin
    over = where( drop_out[1,*] ge drop_out[0, 1:*], nover )

    if nover ge 1 then begin
      drop_out[1, over] = drop_out[1, over+1]
      tst = reform( drop_out[0,*] )
      ten = reform( drop_out[1,*] )
      remove, nover+1, tst, ten
      drop_out = transpose( [[tst],[ten]] )
    endif
  endif
  ;
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;+
;
;Name: HSI_DROPOUT_REMOVE
;
;Purpose: This routine removes all data from the spectral accumulations that
; fall into the datagaps.  At the tail of the datagaps when counts first
; appear, the energies of otherwise valid events seem to be on an elevated
; baseline that decays within 10 msec of the turnon. In the future we may
; fit that baseline evolution and capture these events but for now we remove
; them to keep them from distorting the spectra.  We remove them by setting
; their channel number to -3. Those are removed when building the spectrogram in the same way as the uld and csa.
; Changed to -3 for easy recognition when analyzing the eventlist, RAS, 24-jan-2017
;Inputs:
; Time - event times in the units of the DROP_OUTs (datagaps)
; Channel - corresponding energy channel of events
; Drop_out - 2xN array of datagap times, long or long65
;
;
;Look for events inside of the extended datagaps
;If there are events, remove them
;History:
; 26-mar-2007, ras
;-

pro  hsi_dropoutlist_remove, time, channel, drop_out, coinc_offset = coinc_offset

  default, coinc_offset, -5
  if (size(drop_out))[0] eq 1 then begin
    idl0 = value_locate(drop_out[*],time)
    coinc = where(idl0 eq 0, ncoinc)
  endif else begin

    even = value_locate(drop_out,time)

    ;set coinc if an event falls within a dropout
    coinc = where( (even mod 2) eq 0, ncoinc)

    ;if ir eq 3 and ncoinc gt 0 then stop
  endelse
  ;"mirror" the channel to its negative value with the coinc_offset added. The coinc_offset (nominal -5) is a negative number
  ;in the datagaps

  ;cc=channel
  if  ncoinc ge 1 then begin
    negation_test  = where( channel[coinc] ge 0, nnegation_test )
    if nnegation_test ge 1 then $
      channel[coinc[negation_test]] = coinc_offset - channel[coinc[negation_test]] ;ras, 31-jan-2017
  endif
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
function hsi_dropout_block, seg_index, drop_out, ut_ref64, npack=npack
  ;if seg_index eq 0 then ptim, hsi_sctime2any(ut_ref64)
  ndrop = n_elements( drop_out[0,*] )
  checkvar, npack, 100
  nstr  = ceil( ndrop/float( npack) )
  structname = 'dropout'+strtrim(npack,2)
  dstruct = create_struct( name=structname, ['seg_index', 'npack','start','last','drop_out'], $
    byte(seg_index), long(npack),0LL,0LL,lonarr(2,npack) )

  dlist = replicate( dstruct, nstr)
  dropfield = dlist.drop_out
  dlist[nstr-1].npack = ndrop - (nstr-1)*npack
  dropfield[0] = drop_out[*]
  dlist.drop_out = dropfield
  ut_ref64 = hsi_sctime_convert( ut_ref64, /L64)
  dlist.start = ut_ref64 + dlist.drop_out[0]
  dlist.last  = ut_ref64 + dlist.drop_out[1, npack-1]
  dlist[nstr-1].last = ut_ref64 + dlist[nstr-1].drop_out[1, dlist[nstr-1].npack-1]
  dlist.seg_index = seg_index
  return, dlist
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Function HSI_Dropoutlist_noreset, ev, ut_ref, saved_reset,  obj=obj,$
  done=done, $
  livetimelist = livetimelist
  ;+
  ;
  ; 1. Prepare the deadacc_arr to hold the deadtime accumulation. The total dropout time in seconds.
  ;    A. Check the time binning.  If it is a pointer for multiple time bases (binned_eventlist), then
  ;       then the deadacc array must also be a pointer.  Otherwise a simple array.
  ; 2. Find the dropout starts and ends.  A dropout starts on a reset, and ends on the
  ;   first pha event (channel ge 0) more than 75 usec after the reset.  There may be several
  ;   resets before the first event.
  ; 3. Having found the dropouts segregate them by type and add the appropriate deadtime
  ;   A. Dropouts wholly contained within a timebin.  Typical for spectroscopy accumulations
  ;     with time_bins longer than 1 second.  All of the dropout time is accumulated in this bin
  ;   B. Dropouts straddling a time_bin boundary.  Divide the dropout time proportionately.
  ;   C. Timebins wholly contained within a dropout.  Set their acc time to the timebin duration
  ;     and proportionately assign the extra to the partial timebins.
  ; 4. Save any open active resets to allow for multiple entrances when accumulating over bunches.

  ; Restrictions: Returns empty arrays unless DP_ENABLE set and TIME_UNIT is 1 in object parameters
  ; History:
  ;   richard.schwartz@gsfc.nasa.gov 25-mar-02, based on algorithm developed with G Hurford and D Smith
  ;    3-may-2002, fixed crash from calling lindgen with single element vector, 5.3 needs a scalar.
  ;   7-may-2002, protect against no data for a2d, lines are transposed. ras
  ; 9-may-2002, ras, merge overlapping drop_outs which arise due to rejection
  ;   of photons within photon cutoff window
  ; 21-may-2002, ras, use spectrogram::accbin to set up accumulation bins
  ; 13-jun-2002, ras, added prebin to deadacc
  ; 21-jun-2002, ras, added EXTEND_TIME_RANGE to last_photon time
  ; 24-jun-2002, ras, fixed bug which caused crash with single time interval
  ; 1-jul-2002, ras, fixed bug due to reset index increasing beyond range of channel array.
  ; 17-jul-2002, ras, fixed bug from 13-jun fix introduced for intervals straddling
  ;   time boundaries
  ; 14-aug-2002, ras, fixed bug from 24-jun fix. This bug caused the dp_cutoff value to
  ;   be ignored resulting in anomalously high apparent dropout rates for low count rates
  ; 25-sep-02, ras, dropout split into two parts. The first is dropoutlist that is moved inside
  ; the eventlist object.  The second is dropout_build that remains in spectrogram to
  ; fill the accumulation array, deadacc_arr.
  ;
  ;New version required for data from 28-nov-02 through 10-apr-03 when csa resets were unknowingly disabled
  ; 10-apr-02 - remove test for reset in ha array.  Only need to look for
  ; valid events.
  ;  20-apr-2005, ras, added dp_extend to lengthen the datagaps and
  ;  set the coinc flag on all events within datagaps.
  ; 11-jan-2007, ras, allow for structure in dp_extend so different
  ;   a2d may be treated differently, added dp_extendi for individual
  ;   checks.
  ; 27-feb-2007, ras, dp_cutoff becomes a structure so cutoff duration is now rate dependent
  ;
  ; 12-aug-2011, ras, smooth(...,/edge)->smooth(...,/edge_truncate) to support idl version 8
  ; 2-feb-2017, ras, added ramp tests and removed saved_resets as we won't use csa resets
  ;  27-feb-2017, ras added min=0 to histogram, - terribly dangerous bug without it
  ;  2-mar-2017, ras, using info param, info_dp_extend to report dp_extend values used before and after gaps
  ;-

  If keyword_set( obj ) then self = obj
  coinc_offset = obj->get(/dp_coinc_offset)  ;check to see if it's defined, must be -3 or less
  coinc_offset = coinc_offset ge -2 ? -5 : coinc_offset

  nseg= 18 ;changing from 9 to 18 to set the livetime to zero for rears
  ;when the rears are off even though there are no traditional datagaps in the rear
  nadd  = 10000L



  two20 = 2.0^20
  ;dp_cutoff = Self->Get(/DP_CUTOFF), since dp_cutoff can't exist as a structure
  ;construct one from the elements in the control structure. hsi_f_dp_cutoff takes
  ;care of the details
  dp_cutoff = hsi_f_dp_cutoff(OBJ=obj)
  Self -> Set, info_dp_cutoff = dp_cutoff
  checkvar, valid_pht_time, 75 ;time difference to next valid photon to closeout reset
  lld = Self->Get(/DP_LLD)
  If lld[0] eq -1 then lld = [51,49,52,54,52,44,50,46,40 ]
  uld = Self->Get(/DP_ULD)
  obs_time = self->get(/obs_time_int)
  cf = hsi_get_e_edges( /coef, gain_time = obs_time[0])
  lld = (long( (-cf[*,0] - 5)/cf[*,1] ))[0:17] > 0


  ;  saved_reset = is_struct( saved_reset) ? $
  ;    (tag_names(/struc, saved_reset ) eq 'SAVED_DROPOUT'? saved_reset : 0 ) : 0
  ;  if not is_struct( saved_reset ) $
  ;    then saved_reset = replicate( {saved_dropout, ut_ref:-1.0d0, rst_time:0L}, 9)


  h = histogram( min=0, ev.a2d_index, rev=r )  ;added min=0, 27-feb-2017, ras - terribly dangerous bug without it

  ;ut_ref = obj->get(/ut_ref)
  lt_ut_ref_sec79 = hsi_sctime2any(obj->get(/lt_ut_ref))
  ir=0
  DP_ENABLE = Self->Get(/DP_ENABLE)
  dpx = reform_struct( Self->Get(/dp_extend, /dp_append, /dp_prepend ) ) ;gt_dp_extend(Self)

  TIME_UNIT = Self->Get(/TIME_UNIT)
  ut_ref64 = hsi_any2sctime( ut_ref, /L64)


  ;For each segment (front only for now, 28-may-02, ras), identify the valid datagaps
  ;and accumulate a deadtime.
  ;Valid datagaps start with a CSA reset (channel value of -2), and they extend until
  ;the first photon event more than 75 microseconds after the initiating reset.
  ;To be used to accumulate deadtime, the duration of the dropout must exceed the
  ;the DP_CUTOFF parameter value.
  ;
  nev = n_elements(ev)
  info_dp_extend = fltarr(2,18) ;save as info parameter
  for ir=0, nseg-1 do begin
    ;Establish a catch within the detector loop.  This will make this
    ;routine far more robust

    ;if ir eq 8 then stop

    ; only 1 time for each det now, if 0, then
    ;don't include this effect
    cfi = cf[ir,*]
    ch_bulk_lim = fcheck( dp_bulk_lim, 20.) / cfi[1] - cfi[0]

    dpxi = dpx[ir]


    dp_extendi  = [dpxi.dp_prepend_def, ( (anytim(ut_ref,/sec) ge anytim(dpxi.dp_extend_utlim,/sec) ? $
      ( dpxi.dp_extend_sec > 0.0 <0.04) :  0.0 ) > dpxi.dp_extend_def) > dpxi.dp_append_def]
    info_dp_extend[0,ir] = dp_extendi



    if hsi_get_debug() eq 0 then catch, error_catch
    if fcheck(error_catch,0) ne 0 then begin
      message, /continue, 'Warning: problem finding dropouts at '+$
        hsi_sctime2any( ut_ref64 + time[0], /vms)
      ;stop
      goto, catch_out
    endif

    if DP_ENABLE and (TIME_UNIT eq 1) then begin
      if h[ir] ge 2 then begin ;lines transposed before 7-may-2002, ras
        ;SEL holds the indices of the current segment.
        rsel = reverseindices( r, ir, count = nrsel ) ; r[ r[ir]:r[ir+1]-1 ]
        time = nev eq 1 ? ev.time[rsel] : ev[rsel].time


        channel = nev eq 1 ? ev.channel[rsel] : ev[rsel].channel
        if avg( obs_time ) gt anytim( '1-jan-2014') and ir le 8 then begin
          itramp = hsi_dp_ramp( channel, channel_max = channel_max, ntest=4, ch_bulk_lim = ch_bulk_lim ) <(nrsel-1)
          nvalid  = n_elements( itramp )/2



          ;if ir eq 5 then stop
          for jj = 0L, nvalid -1  do $
            channel[itramp[0,jj]:itramp[1,jj]] = -channel[itramp[0,jj]:itramp[1,jj]] + coinc_offset
        endif

        ;Make sure channels are greater than LLD
        ;
        lsel = where( channel gt lld[ir] and channel lt uld[ir], nsel)
        pretime = 0
        if nsel ge 2 then begin
          channel = channel[lsel]
          time    = time[lsel]

          ;          if saved_reset[ir].ut_ref gt 0.0 and $
          ;            ((ut_ref - saved_reset[ir].ut_ref) lt 2000. ) then begin
          ;            time = long( [saved_reset[ir].rst_time - $
          ;              (hsi_any2sctime(ut_ref,/l64) - $
          ;              hsi_any2sctime(saved_reset[ir].ut_ref,/l64)), time])
          ;            channel = [-2, channel];Look for any identical times
          ;            pretime = 1
          ;
        endif

        ord = sort(time)
        time = time[ord]
        channel = channel[ord]
        ; If we have requested the whole eventlist, done won't be defined
        ; so set it to 1. ras, 12-nov-02
        ;if not exist(done) then stop
        posttime = 0

        If fcheck( Done,1) then begin ;add one last photon to close any dropouts
          ;If Done then begin

          last_time =(( hsi_any2sctime(/L64, Self->get(/absolute_time_range) + $
            Self->get(/extend_time_range)) - $
            hsi_any2sctime(ut_ref,/L64) )[1] + valid_pht_time + 5 ) $
            > last_item(time) ;add the 5 as a fudge
          time = [time, last_time ]
          channel = [channel, 100]
          posttime = 1

        endif

        diff = time[1:*]-time
        npht = n_elements(time)
        ;Evaluate for cutoff that's a function of rate based
        ;on nearest 100 diffs with outliers removed

        srt= npht gt 20 ? 1./smooth(diff,npht<101,/edge_truncate) : 100.
        cutoff = hsi_f_dp_cutoff( srt, dp_cutoff )
        ;                 acutoff= avg(cutoff)
        ;                 if ir eq 7 then help,acutoff
        cutoff = ir ge 9 ? 2LL^20 * 60 : cutoff ;60 seconds to declare a gap on a rear seg
        tc  = time



        ; Trap time degeneracy and change the values so it will work
        ; with the remainder of the tests
        ;
        ; The first case has a reset and event with the same time, but
        ; the ordering has placed the reset first.  Here, make the
        ; event come and make the reset come after by a microsecond



        sel = where( diff ge cutoff, nsel)

        ;          saved_reset[ir].ut_ref = ut_ref
        ;          saved_reset[ir].rst_time = last_item( time)
        ;          saved = 1
        if nsel ge 1 then begin
          ;if ir ge 9 then stop ; and nsel gt 1 then stop
          drop_time_start = time[sel]-long(dp_extendi[0]*two20)
          drop_time_end   = time[sel+1]+long(dp_extendi[1]*two20)
          ;            if pretime then begin ;
          ;              ;Add a leading dropout
          ;              drop_time_start = [time[0], drop_time_start]
          ;              drop_time_end   = [time[1], drop_time_end]
          ;              ;clear the reset
          ;              channel[0] = 1
          ;            endif

          drop_out =  n_elements(drop_time_start) eq 1 ? $
            reform([drop_time_start, drop_time_end],2,1) : $
            [ transpose(drop_time_start), transpose(drop_time_end)]
          hsi_dropout_extend_4_valid, time, channel, drop_out, dpxi=dpxi ;also cleans overlaps
          ;            ;Look for any overlaps
          ;            if n_elements(drop_out) ge 4 then begin
          ;              overlap = where(drop_out[1,*] gt drop_out[0,1:*], nover)
          ;              if nover ge 1 then drop_out[1,overlap] = drop_out[0,overlap+1]
          ;            endif

          ;Now that we know where the dropouts are, we can go back
          ;to the original channels and set the drop_out interval channels to 0
          time =(ev.time)[rsel]
          channel = (ev.channel)[rsel]

          hsi_dropoutlist_livetimelist_zero, seg=ir,  lt_ut_ref_sec79, livetimelist, $
            drop_out, ut_ref, rev = segrev, hseg=hseg

          hsi_dropoutlist_remove, time, channel, drop_out, coinc_offset = coinc_offset
          ;if saved then begin
          if 0 then begin ;no longer using resets!
            sdrop_out = [saved_reset[ir].rst_time - long(dp_extendi[0]*two20), last_item( time )]
            hsi_dropout_extend_4_valid, time, channel, sdrop_out, dpxi=dpxi
            hsi_dropoutlist_livetimelist_zero, seg=ir,  lt_ut_ref_sec79, livetimelist, $
              sdrop_out, ut_ref, rev = segrev, hseg=hseg

            hsi_dropoutlist_remove, time, channel, sdrop_out

          endif

          if nev eq 1 then ev.channel[rsel] = channel else ev[rsel].channel = channel

          seg_index = ir


          if n_elements(dplist) eq 0 then begin
            npack = 100


            dplist1 = hsi_dropout_block( seg_index, drop_out, ut_ref64, npack=npack)
            ndplist = n_elements( dplist1 ) * 20
            dplist = replicate(dplist1[0], ndplist)
            dplist[0] = dplist1
            idplist = 0L + n_elements( dplist1 )

          endif else begin

            dplist1 = hsi_dropout_block(seg_index, drop_out, ut_ref64, npack=npack)
            n1 = n_elements(dplist1)

            if ( idplist + n1 ) gt ndplist then begin
              dplist = [dplist, replicate( dplist1[0], (2 * (nseg-ir-1) * n1 )> n1 )]
              ndplist = n_elements( ndplist )
            endif
            dplist[idplist] = dplist1
            idplist = idplist + n1
          endelse
          ;if saved eq 0 then saved_reset[ir].ut_ref = -1.0d0
          ;Save any open resets

        endif
      endif
    endif


    ;  endif
    ;Jump here for indexing errors
    Catch_out: catch, /cancel
  endfor
  Self->Set, INFO_DP_EXTEND = info_dp_extend



  return, keyword_set(idplist) ? dplist[0:idplist-1] : -1
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Function HSI_Dropoutlist, ev, ut_ref, saved_reset,  obj=obj,$
  livetimelist = livetimelist, $
  done=done
  ;+
  ;
  ; 1. Prepare the deadacc_arr to hold the deadtime accumulation. The total dropout time in seconds.
  ;    A. Check the time binning.  If it is a pointer for multiple time bases (binned_eventlist), then
  ;       then the deadacc array must also be a pointer.  Otherwise a simple array.
  ; 2. Find the dropout starts and ends.  A dropout starts on a reset, and ends on the
  ;   first pha event (channel ge 0) more than 75 usec after the reset.  There may be several
  ;   resets before the first event.
  ; 3. Having found the dropouts segregate them by type and add the appropriate deadtime
  ;   A. Dropouts wholly contained within a timebin.  Typical for spectroscopy accumulations
  ;     with time_bins longer than 1 second.  All of the dropout time is accumulated in this bin
  ;   B. Dropouts straddling a time_bin boundary.  Divide the dropout time proportionately.
  ;   C. Timebins wholly contained within a dropout.  Set their acc time to the timebin duration
  ;     and proportionately assign the extra to the partial timebins.
  ; 4. Save any open active resets to allow for multiple entrances when accumulating over bunches.

  ; Restrictions: Returns empty arrays unless DP_ENABLE set and TIME_UNIT is 1 in object parameters
  ; History:
  ;   richard.schwartz@gsfc.nasa.gov 25-mar-02, based on algorithm developed with G Hurford and D Smith
  ;    3-may-2002, fixed crash from calling lindgen with single element vector, 5.3 needs a scalar.
  ;   7-may-2002, protect against no data for a2d, lines are transposed. ras
  ; 9-may-2002, ras, merge overlapping drop_outs which arise due to rejection
  ;   of photons within photon cutoff window
  ; 21-may-2002, ras, use spectrogram::accbin to set up accumulation bins
  ; 13-jun-2002, ras, added prebin to deadacc
  ; 21-jun-2002, ras, added EXTEND_TIME_RANGE to last_photon time
  ; 24-jun-2002, ras, fixed bug which caused crash with single time interval
  ; 1-jul-2002, ras, fixed bug due to reset index increasing beyond range of channel array.
  ; 17-jul-2002, ras, fixed bug from 13-jun fix introduced for intervals straddling
  ;   time boundaries
  ; 14-aug-2002, ras, fixed bug from 24-jun fix. This bug caused the dp_cutoff value to
  ;   be ignored resulting in anomalously high apparent dropout rates for low count rates
  ; 25-sep-02, ras, dropout split into two parts. The first is dropoutlist that is moved inside
  ;  the eventlist object.  The second is dropout_build that remains in spectrogram to
  ;  fill the accumulation array, deadacc_arr.
  ; 18-apr-03 ras, put in correct control between csa dropout and non csa dropout
  ; 3-feb-2004, ras, protect histogram call against empty packets by setting max=0
  ;  20-apr-2005, ras, added dp_extend to lengthen the datagaps and
  ;  set the coinc flag on all events within datagaps.
  ;	11-jan-2007, ras, allow for structure in dp_extend so different
  ;		a2d may be treated differently, added dp_extendi for individual
  ;		checks.
  ;	27-feb-2007, ras, dp_cutoff becomes a structure so cutoff duration is now rate dependent
  ;	13-mar-2007, ras, need to merge dropoutlist and dropoutlist_noreset
  ;	20-mar-2007, ras, dp_extend and dp_cutoff can't be structures, so we instead build the structures
  ;		on the fly after grabbing their fields
  ;	12-aug-2011, ras, smooth(...,/edge)->smooth(...,/edge_truncate) to support idl version 8
  ;-
  If keyword_set( obj ) then self = obj

  NO_CSA_DROPOUT = Self->Get(/control, /NO_CSA_DROPOUT ) ;ras, 18-apr changed N0 to NO
  NO_CSA_DROPOUT = NO_CSA_DROPOUT ? 1 : $ ;Look for CSA below. If CSA and not NO_CSA then
    ;use the normal dropoutlist alg. Returns 0 for valid csa events in front segments.
    -((where( ev.channel eq -2 and ev.a2d_index le 8, ntest))[0] < 0)
  ;message,/cont,atime(ut_ref)
  no_csa_dropout = 1 ;csa algorithm is no longer supported as gaps are not always preceded by a recorded CSA
  if NO_CSA_DROPOUT  then $
    return, hsi_dropoutlist_noreset( ev, ut_ref, saved_reset,  obj=obj,$
    done=done, $
    livetimelist = livetimelist )

  nfront= 9
  nadd  = 10000L

  dp_cutoff = hsi_f_dp_cutoff(OBJ=obj)


  Self -> Set, info_dp_cutoff = dp_cutoff
  checkvar, valid_pht_time, 75 ;time difference to next valid photon to closeout reset
  lld = Self->Get(/DP_LLD)
  If lld[0] eq -1 then lld = [51,49,52,54,52,44,50,46,40 ]
  ;Check the dp_lld database, ras, 24-jan-2017
  lld = hsi_dp_lld_database( lld, ut_ref )
  uld = Self->Get(/DP_ULD)

  saved_reset = is_struct( saved_reset) ? $
    (tag_names(/struc, saved_reset ) eq 'SAVED_DROPOUT'? saved_reset : 0 ) : 0
  if not is_struct( saved_reset ) $
    then saved_reset = replicate( {saved_dropout, ut_ref:-1.0d0, rst_time:0L}, 9)


  h = histogram(min=0, ev.a2d_index, rev=r )
  two20 = 2L^20

  ;ut_ref = obj->get(/ut_ref)
  lt_ut_ref_sec79 = hsi_sctime2any(obj->get(/lt_ut_ref))
  ir=0
  DP_ENABLE = Self->Get(/DP_ENABLE)
  dpx = reform_struct( Self->Get(/dp_extend, /dp_append, /dp_prepend ) ) ;gt_dp_extend(Self)

  ;Force dp_extend to be 0 or positive and le 40 milliseconds.
  TIME_UNIT = Self->Get(/TIME_UNIT)
  ut_ref64 = hsi_any2sctime( ut_ref, /L64)


  ;For each segment (front only for now, 28-may-02, ras), identify the valid datagaps
  ;and accumulate a deadtime.
  ;Valid datagaps start with a CSA reset (channel value of -2), and they extend until
  ;the first photon event more than 75 microseconds after the initiating reset.
  ;To be used to accumulate deadtime, the duration of the dropout must exceed the
  ;the DP_CUTOFF parameter value.
  ;
  nev = n_elements(ev)
  for i=0, nfront-1 do begin
    ir = i ;([7,indgen(7),8])[i]
    dpxi = dpx[ir]
    ;Establish a catch within the detector loop.  This will make this
    ;routine far more robust

    ; only 1 time for each det now, if 0, then
    ;don't include this effect

    dp_extendi  = [dpxi.dp_prepend_def, ( (anytim(ut_ref,/sec) ge anytim(dpxi.dp_extend_utlim,/sec) ? $
      ( dpxi.dp_extend_sec > 0.0 <0.04) :  0.0 ) > dpxi.dp_extend_def) > dpxi.dp_append_def]


    if hsi_get_debug() eq 0 then catch, error_catch
    if fcheck(error_catch,0) ne 0 then begin
      message, /continue, 'Warning: problem finding dropouts at '+$
        hsi_sctime2any( ut_ref64 + time[0], /vms)
      goto, catch_out
    endif

    if DP_ENABLE and (TIME_UNIT eq 1) then begin
      if h[ir] ge 1 then begin ;lines transposed before 7-may-2002, ras
        ;RSEL holds the indices of the current segment.
        rsel = r[ r[ir]:r[ir+1]-1 ]
        time0 = (ev.time)[rsel]


        channel = (ev.channel)[rsel]
        ;Make sure channels are greater than LLD
        lsel = where( channel lt 0 or channel gt lld[ir], nsel)
        ;if   ir eq 5 then stop
        pretime = 0
        if nsel ge 1 then begin
          channel = channel[lsel]
          time    = time0[lsel]
          t0test = saved_reset[ir].ut_ref gt 0.0 and $
            ((ut_ref - saved_reset[ir].ut_ref) lt 2000. )
          ;this_saved_reset = 0

          if t0test then begin
            time = long( [saved_reset[ir].rst_time - $
              (hsi_any2sctime(ut_ref,/l64) - $
              hsi_any2sctime(saved_reset[ir].ut_ref,/l64)), time])
            ;this_saved_reset = saved_reset[ir]
            channel = [-2, channel];Look for any identical times
            pretime = 1
          endif

          ord = sort(time)
          time = time[ord]
          channel = channel[ord]
          tc    = 0
          tcsel = where(channel gt 0 and channel lt uld[ir], ntcs)
          if ntcs gt 50 then begin
            tc = time[tcsel]
            diff = tc[1:*]-tc
            srt = ntcs gt 20 ? 1./smooth(diff,ntcs<101,/edge_truncate) : 100.
            cutoff = hsi_f_dp_cutoff( srt, dp_cutoff)
          endif
          ; If we have requested the whole eventlist, done won't be defined
          ; so set it to 1. ras, 12-nov-02
          ;if not exist(done) then stop
          posttime = 0

          If fcheck( Done,1) then begin ;add one last photon to close any dropouts
            ;If Done then begin

            last_time =(( hsi_any2sctime(/L64, Self->get(/absolute_time_range) + $
              Self->get(/extend_time_range)) - $
              hsi_any2sctime(ut_ref,/L64) )[1] + valid_pht_time + 5 ) $
              > last_item(time) ;add the 5 as a fudge
            time = [time, last_time ]
            channel = [channel, 100]
            posttime = 1

          endif


          ;Evaluate for cutoff that's a function of rate based
          ;on nearest 100 diffs with outliers removed


          ; Trap time degeneracy and change the values so it will work
          ; with the remainder of the tests
          ;
          ; The first case has a reset and event with the same time, but
          ; the ordering has placed the reset first.  Here, make the
          ; event come and make the reset come after by a microsecond
          if nsel ge 2 then begin
            w = where( time eq time[1:*], nw)

            if nw ge 1 then begin
              wc = where( channel[w] eq -2 and channel[w+1] ge 0, nwc)
              if nwc ge 1 then begin
                channel[w[wc]] = channel[w[wc]+1]
                channel[w[wc]+1] = -2
                time[w[wc]+1] = time[w[wc]+1] + 1
              endif

            endif

            ;Trap the channel sequence -2 -1 >0 for identical times.
            if n_elements(time) ge 3 then begin
              w = where( time eq time[2:*], nw)
              if nw ge 1 then begin
                wc = where( channel[w] eq -2 and channel[w+2] ge 0, nwc)
                if nwc ge 1 then begin
                  channel[w[wc]] = channel[w[wc]+2]
                  channel[w[wc]+2] = -2
                  time[w[wc]+2] = time[w[wc]+2] + 1
                endif
              endif

            endif
            ;
            ;Set max for histogram to protect against empty packets, 3-feb-04

            ha = histogram( channel<0, rev=rc,min=-2 ,max=0)

            if ha[0] ge 1 and ha[2] ge 2 then begin
              rst = rc[rc[0]:rc[1]-1] ; Reset indices
              pht = rc[rc[2]:rc[3]-1] ; PHA event indices
              npht = ha[2] ;number of photon events
              pht_time = time[pht]
              rst_time = time[rst]


              sub1 = value_locate(pht_time, rst_time)
              sub1 = sub1[uniq( sub1, sort(sub1))]
              ;This is a unique set of indices into pht.time of events before resets
              ;The start of a dropout will be in the reset time after this time
              ;
              ; Save any open resets
              next_pht = sub1 + 1
              if last_item( next_pht) ge npht  then begin
                saved_reset[ir].ut_ref = ut_ref
                saved_reset[ir].rst_time = last_item( rst_time)
                saved = 1
              endif else begin
                saved_reset[ir].ut_ref = 0
                saved = 0
              endelse

              ;Resets are at pht[sub1]+1, possibly ULD, but soon after

              if n_elements( rst_time ) eq 1 then drop_time_start = rst_time[0] else begin
                reset = pht[sub1>0]+1
                if sub1[0] eq -1 then reset[0] = rst[0]

                ntest = 1
                lct =0
                max_channel = n_elements(channel) -1
                while ntest<1 do begin
                  lct = lct + 1
                  test = where( channel[reset] ne -2, ntest)
                  if ntest ge 1 then $
                    reset[test]=(reset[test]+1 < max_channel)

                  if lct ge 10 then begin
                    debug = Self->Get(/DEBUG)
                    if not DEBUG then ntest = 0 $
                    else begin
                      message,/continue,'Warning! Fails to identify reset after 10 tries.'
                      if DEBUG eq 10 then stop

                    endelse
                  endif
                endwhile

                drop_time_start = time[reset]
              endelse
              ;prevent crash here by brute force, ras, 12-aug-02
              drop_time_end   = pht_time[next_pht<(npht-1)]


              test = drop_time_end - drop_time_start

              wsec = where( test le valid_pht_time*time_unit, nsec)

              if nsec ge 1 then begin
                next_pht = next_pht[wsec] + 1
                if last_item( next_pht ge npht ) then begin
                  saved_reset[ir].ut_ref = ut_ref
                  saved_reset[ir].rst_time = last_item( rst_time)
                  saved = 1
                endif
                drop_time_end[wsec] = pht_time[next_pht<(npht-1)]
              endif
              if pretime then begin ;
                ;Add a leading dropout
                drop_time_start = [time[0], drop_time_start]
                drop_time_end   = [time[1], drop_time_end]
                ;clear the reset
                channel[0] = 1
              endif
              drop_out =  n_elements(drop_time_start) eq 1 ? $
                reform([drop_time_start, drop_time_end],2,1) : $
                [ transpose(drop_time_start), transpose(drop_time_end)]
              ;
              ;Now some of the drop_out ends will have moved to the next interval because
              ;their follow on photon was before valid_pht_time.  These intervals should be merged.
              ;Merge them by setting the end of one equal to the start of the next
              ndropout = (size( drop_out, /dim))[1]
              ;dd = drop_out
              If ndropout gt 1 then begin
                drop_out[1,*] = drop_out[1,*] < [ reform(drop_out[0, 1:*]), drop_out[1, ndropout-1]]
                ;Merge the overlaps
                start_merge = where( drop_out[1,*] ge drop_out[0, 1:*], nstart)
                if nstart ge 1 then for im=nstart-1,0,-1 do begin
                  drop_out[1, start_merge[im]] = drop_out[1, start_merge[im]+1]
                  ;Now after extending
                  drop_out[0, start_merge[im]+1] = drop_out[0, start_merge[im]]
                endfor
              endif
              ;Keep unique drop outs
              ;cutoffs for drop_out time using value_locate!
              i4co = n_elements(tc) gt 1 ? value_locate( tc, drop_out[0,*]) : 0
              cutoff = cutoff[ i4co]
              if ndropout eq 1 then $

                wg = where( (drop_out[1, *]-drop_out[0,*]) gt cutoff, ng) else $
                wg = where( (drop_out[0, 1:*] ge drop_out[1, *]) and $
                ((drop_out[1,*]-drop_out[0,*]) gt cutoff), ng)


              if ng ge 1 then begin
                drop_out = drop_out[*, wg]

                drop_out[1,*] += long(dp_extendi[1]*two20)
                drop_out[0,*] -= long(dp_extendi[0]*two20)
                drop_out_sec   = drop_out/2.0d0^20+ut_ref
                ;channel has csa, uld, and only valid channels above LLD but there may be zero channels so should
                ;


                hsi_dropout_extend_4_valid, time, channel, drop_out, dpxi=dpxi

                ;Look for any overlaps
                if ng ge 2 then begin
                  overlap = where(drop_out[1,*] gt drop_out[0,1:*], nover)
                  if nover ge 1 then $
                    drop_out[1,overlap] = drop_out[0,overlap+1]
                endif


                ;;Now that we know where the dropouts are, we can go back
                ;to the original channels and set the drop_out interval channels to 0
                time =(ev.time)[rsel]
                channel = (ev.channel)[rsel]

                if ng ge 1 then begin

                  hsi_dropoutlist_livetimelist_zero, seg=ir,  lt_ut_ref_sec79, livetimelist, $
                    drop_out, ut_ref, rev = segrev, hseg=hseg

                  hsi_dropoutlist_remove, time, channel, drop_out
                  if saved then begin
                    sdrop_out = [saved_reset[ir].rst_time - long(dp_extendi[0]*two20), last_item( time )]
                    hsi_dropout_extend_4_valid, time, channel, sdrop_out, dpxi=dpxi
                    hsi_dropoutlist_livetimelist_zero, seg=ir,  lt_ut_ref_sec79, livetimelist, $
                      sdrop_out, ut_ref, rev = segrev, hseg=hseg

                    hsi_dropoutlist_remove, time, channel, sdrop_out
                  endif
                endif

                if nev eq 1 then ev.channel[rsel] = channel else ev[rsel].channel = channel

                seg_index = ir
                if n_elements(dplist) eq 0 then begin
                  npack = 100


                  dplist1 = hsi_dropout_block( seg_index, drop_out, ut_ref64, npack=npack)
                  ndplist = n_elements( dplist1 ) * 20
                  dplist = replicate(dplist1[0], ndplist)
                  dplist[0] = dplist1
                  idplist = 0L + n_elements( dplist1 )

                endif else begin

                  dplist1 = hsi_dropout_block(seg_index, drop_out, ut_ref64, npack=npack)
                  n1 = n_elements(dplist1)

                  if ( idplist + n1 ) gt ndplist then begin
                    dplist = [dplist, replicate( dplist1[0], (2 * (nfront-ir-1) * n1 )> n1 )]
                    ndplist = n_elements( ndplist )
                  endif
                  dplist[idplist] = dplist1
                  idplist = idplist + n1
                endelse

                if saved eq 0 then saved_reset[ir].ut_ref = -1.0d0
                ;Save any open resets


              endif
            endif
          endif
        endif
      endif
    endif
    ;Jump here for indexing errors
    Catch_out: catch, /cancel
  endfor

  return, keyword_set(idplist) ? dplist[0:idplist-1] : -1
end


