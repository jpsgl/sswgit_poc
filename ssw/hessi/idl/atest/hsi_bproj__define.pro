;---------------------------------------------------------------------------
; Document name: hsi_bproj__define
; Created by:    Andre Csillaghy, May 1999
;
; Last Modified: Wed Sep  5 10:42:15 2001 (csillag@sunlight)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       BACK PROJECTION CLASS DEFINITION
;
; PURPOSE: 
;       Provides data structures and methods to generate and retrieve
;       back projection maps. The back projection class extends the
;       class strategy_holder_passive. The back projection can be
;       calculated using either of the annsec or the visibility
;       modulation pattern. The startegy holder is passive because it
;       just selects its strategy by listening to the modulation
;       pattern object, which really decides which strategy (commanded
;       by imaging_method) to use. 
;
;       HSI_Bproj is just a container. The real work is done by the
;       startegie, either hsi_bprj_annsec or hsi_ bproj_vismod, which
;       are two classes extending the basic class hsi_bproj_strategy.
;
; CATEGORY:
;       Imaging
; 
; CONSTRUCTION: 
;       o = HSI_BProj()
;
; INPUTS (CONTROL) PARAMETERS:
;       none in that class. But look at hsi_bproj_strategy
;      
; OUTPUTS (INFORMATION) PARAMETERS:
;       defined in {hsi_bproj_info}:
;       
;       bproj_alg_available: lists the back projection algorithms
;                            (i.e, objects) available.
;
; SOURCE OBJECT:
;       HSI_Modul_Pattern
; 
; DESTINATION OBJECTS:
;       HSI_Modpat_Products, HSI_Image
;
; SEE ALSO:
;       HESSI Utility Reference 
;          http://hessi.ssl.berkeley.edu/software/reference.html
;       hsi_modul_pattern__define
;       hsi_bproj_control__defin
;       hsi_bproj_control
;       hsi_bproj_info__define
;
; HISTORY:
;     
;       Based on the release 2 software of Richard Schwartz
;       Release 3 development, August / September 1999, 
;           A Csillaghy, csillag@ssl.berkeley.edu
;       Fundamental developments by Richard Schwartz, GSFC
;       Release 6 development: introduction of normal, uniform, taper, and
;                              spatial_frequency_weight keywords
;       19-Nov-2010, Kim.  Added setcaller and getcaller methods, but they
;         don't do anything here. Needed because this doesn't inherit hsi_image_alg,
;         which has those methods.  If we need to really store the caller,
;         just add a caller property.   
;        7-MAY-2017, RAS, added HSI_BPROJ_CARTESIAN
;        
;-

;----------------------------------------------------------

FUNCTION HSI_Bproj::INIT, SOURCE = source, _EXTRA=_extra

IF NOT Obj_Valid( source )  THEN source = HSI_Modul_Pattern( )

strategy_available =  ['HSI_BPROJ_ANNSEC', 'HSI_BPROJ_VISMOD','HSI_BPROJ_CARTESIAN']
ret=self->Strategy_Holder::INIT( strategy_available, $
                                 INFO={HSI_Bproj_Info}, $
                                 SOURCE=source, $
                                 _EXTRA=_extra )
self->Set, BPROJ_ALG_AVAILABLE = strategy_available
self->SetStrategy

RETURN, ret

END

;--------------------------------------------------------------------
; do nothing - just need a setcaller method
pro HSI_Bproj::setcaller, caller
;self.caller = caller
end

;--------------------------------------------------------------------
; do nothing, just need a getcaller function
function HSI_Bproj::getcaller
;return,self.caller
return, -1
end

;---------------------------------------------------------------------------

PRO HSI_Bproj__Define

bproj = {HSI_Bproj, $
         INHERITS Strategy_Holder_Passive}

END


;---------------------------------------------------------------------------
; End of 'hsi_bproj__define.pro'.
;---------------------------------------------------------------------------
