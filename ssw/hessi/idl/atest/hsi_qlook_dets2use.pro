;+
;NAME:
; hsi_qlook_dets2use
;PURPOSE:
; Given a time, this will return a set of front detectors that
; are reasonable to use for spectral fitting. Based on the plot:
; http://sprg.ssl.berkeley.edu/~jimm/hessi/hessi_dets/norm_alldets_vs_goes_b.png
;CALLING SEQUENCE:
; det_index = hsi_qlook_dets2use(time_in)
;INPUT:
; time_in = a time
;OUTPUT:
; det_index = an array of detector indices values 0 to 8
;HISTORY:
; 24-Jun-2015, jmm, jimm@ssl.berkeley.edu
; 6-jan-2016, jmm, Added an ELSE for case statement
;-
Function hsi_qlook_dets2use, time_in, qlook_image = qlook_image, $
                             fullsun_image = fullsun_image
  
  otp = -1
  t0 = anytim(time_in[0])
  
  ok_intv = hsi_anneal_dates(/inverse)
  nok_intv = n_elements(ok_intv[0, *])

  this_ok_period = -1
  For j = 0, nok_intv-1 Do Begin
     If(t0 Ge ok_intv[0, j] And t0 Le ok_intv[1, j]) Then this_ok_period = j
  Endfor

  If(keyword_set(fullsun_image)) Then Begin
     If(t0 Lt anytim('1-nov-2015 00:00')) Then Begin
        otp = [6, 7, 8]
     Endif Else If(t0 Lt anytim('10-may-2016 00:00')) Then Begin
        otp = [4, 6]
     Endif Else If(t0 Lt anytim('13-oct-2016 16:15')) Then Begin
        otp = [7]
     Endif Else If(t0 Lt anytim('26-jan-2017 15:35')) Then Begin
        otp = [7, 8] ;We seem to be switching, so use both
     Endif Else If(t0 Lt anytim('3-apr-2017 17:25')) Then Begin
        otp = [2, 5] ;We'll see
     Endif Else If(t0 Lt anytim('11-apr-2017 19:15')) Then Begin
        otp = [2, 5, 7]
     Endif Else If(t0 Lt anytim('18-apr-2017 00:00')) Then Begin
        otp = [2, 5]
     Endif Else Begin
        otp = [2, 5, 7]
     Endelse
  Endif Else If(keyword_set(qlook_image)) Then Begin
     Case this_ok_period Of
        -1: otp = -1
        0: otp = [2, 3, 4, 5, 6, 7]
        1: otp = [2, 3, 4, 5, 6, 7]
        2: otp = [2, 3, 4, 5, 6, 7]
        3: Begin
           If(t0 Lt anytim('1-dec-2012')) Then otp = [2, 4, 5, 6, 7] $
           Else otp = [2, 3, 4, 5, 6, 7]
        End
        4: Begin                ;2014, 2015, etc...
           If(t0 Lt anytim('5-dec-2014 01:24')) Then Begin
              otp = [2, 5, 8]
           Endif Else If(t0 Lt anytim('5-jan-2015 16:14:20')) Then Begin
              otp = [2, 5, 6, 8]
           Endif Else If(t0 Lt anytim('21-aug-2015 01:25')) Then Begin
              otp = [2, 4, 5, 6, 7, 8]
           Endif Else If(t0 Lt anytim('26-sep-2015 00:00')) Then Begin
              otp = [2, 4, 5, 6]
           Endif Else otp = [0, 4, 6]
        End
        5: Begin ;post 2016 anneal, can't just use 2,7,8 because locate_flare38 needs to know
           If(t0 Lt anytim('13-oct-2016 16:15')) Then Begin
              otp = [2, 7]
           Endif Else If(t0 Lt anytim('17-jan-2017 20:57')) Then Begin
              otp = [2, 8]
           Endif Else If(t0 Lt anytim('26-jan-2017 15:35')) Then Begin
              otp = [0, 2]
           Endif Else If(t0 Lt anytim('3-apr-2017 17:25')) Then Begin
              otp = [2, 5]
           Endif Else If(t0 Lt anytim('11-apr-2017 19:15')) Then Begin
              otp = [2, 5, 7]
           Endif Else If(t0 Lt anytim('18-apr-2017 00:00')) Then Begin
              otp = [2, 5]
           Endif Else Begin
              otp = [2, 5, 7]
           Endelse
        End
        Else: otp = [3, 4, 5, 6, 7, 8] ;default
     Endcase
  Endif Else Begin
     Case this_ok_period Of
        -1: otp = -1
        0: otp = [0, 2, 3, 4, 5, 8]
        1: otp = [0, 2, 3, 4, 5, 8]
        2: otp = [0, 2, 3, 4, 5, 8]
        3: Begin
           If(t0 Lt anytim('1-dec-2012')) Then otp = [0, 2, 4, 5, 8] $
           Else otp = [0, 2, 3, 4, 5, 8]
        End
        4: Begin                ;2014, 2015, etc...
           If(t0 Lt anytim('5-dec-2014 01:24')) Then Begin
              otp = [0, 2, 5, 8]
           Endif Else If(t0 Lt anytim('5-jan-2015 16:14:20')) Then Begin
              otp = [0, 2, 5, 6, 8]
           Endif Else If(t0 Lt anytim('21-aug-2015 01:25')) Then Begin
              otp = [0, 2, 4, 5, 6, 8]
           Endif Else If(t0 Lt anytim('26-sep-2015 00:00')) Then Begin
              otp = [0, 2, 4, 5, 6]
           Endif Else otp = [0, 4, 6]
        End
        5: Begin ;Post 2016 anneal
           If(t0 Lt anytim('13-oct-2016 16:15')) Then Begin
              otp = [0, 2]
           Endif Else If(t0 Lt anytim('19-jan-2017 20:06:00'))Then Begin
              otp = [2, 8]
           Endif Else If(t0 Lt anytim('26-jan-2017 15:35')) Then Begin
              otp = [0, 2]
           Endif Else If(t0 Lt anytim('3-apr-2017 17:25')) Then Begin
              otp = [2, 5]
           Endif Else If(t0 Lt anytim('11-apr-2017 19:15')) Then Begin
              otp = [0, 2, 5, 7]
           Endif Else If(t0 Lt anytim('18-apr-2017 00:00')) Then Begin
              otp = [2, 5]
           Endif Else Begin
              otp = [0, 2, 5, 7]
           Endelse
        End
        Else: otp = [0, 2, 3, 4, 5, 8] ;default
     Endcase
  Endelse

  Return, otp
End

  
