;---------------------------------------------------------------------------
;+
; NAME:
;     avsig10
; PURPOSE:
;     Average and dispersion of an array, zeros can be not included,
; CALLING SEQUENCE:
;     xbar=avsig10(x, sigma, no_zeros=no_zeros, sig_mean=sig_mean, rm_outliers=rm_outliers)
; INPUT:
;     x = an array
; OUTPUT:
;     xbar = mean, total(x)/n_elements(x)
;     sigma = standard deviation, sqrt(total((x-xbar)^2/(nx-1)))
; KEYWORDS:
;     no_zeros= if set, strip out zeros, note that this option does
;               not work if the dimension keyword is used. At least not as of
;               2-13-95...
;     sig_mean = sigma/sqrt(nx), the standard deviation of the mean of the array
;     rm_outliers = if set, will remove outlier points
; HISTORY:
;     2-18-2015, jmm
;-
FUNCTION avsig10, x0, sigma, no_zeros=no_zeros, sig_mean=sig_mean, $
                  rm_outliers = rm_outliers

   x = double(x0)
   IF(KEYWORD_SET(no_zeros)) THEN BEGIN
      ok = where(x GT 0.0, nx)
      nx = float(nx)
      IF(nx GT 0) THEN BEGIN
         xbar = total(x[ok])/nx
         IF(nx GT 1) THEN sigma = sqrt(total((x[ok]-xbar)^2)/(nx-1.0)) $
         ELSE sigma = 0.0
         sig_mean = sigma/sqrt(nx)
      ENDIF ELSE BEGIN
         xbar = 0.0
         sigma = 0.0
         sig_mean = 0.0
      ENDELSE
   ENDIF ELSE BEGIN
      nx = float(N_ELEMENTS(x))
      xbar = total(x)/nx
      IF(nx GT 1) THEN sigma = sqrt(total((x-xbar)^2)/(nx-1.0)) $
      ELSE sigma = 0.0
      sig_mean = sigma/sqrt(nx)
   ENDELSE

   IF(KEYWORD_SET(rm_outliers)) THEN BEGIN
      xxx = where(abs(x-xbar) Le 5.0*sigma, nxxx)
      IF(nxxx Gt 0) THEN BEGIN
         xbar = avsig10(x[xxx], sigma, no_zeros=no_zeros, sig_mean=sig_mean)
      ENDIF ELSE message, /info, 'No ok points'
   ENDIF

   RETURN, xbar
END
;+
; Averages 1 day of sohdata for the following data types:
;    tags = ['IUGT1T', 'ILGT1T', 'IRAS2T', $ ;imager temperatures
;           'ICP1T', 'ICP2T', 'ICT1T', 'ICT2T', 'ITST', $ ;Spectrometer T
;           'IRAD1T', 'IRAD2T', 'IACCEL', $ ;Cryocooler (average last
;           'IAVGLIVE', 'IATTLEVEL', 'ISC_SSR', 'CRYOPOWER'] ; misc.
;  Added 16-bit temps for , 14-dec-2002, 'ICP1T', 'ICP2T', 'ICT1T',
;  'ICT2T', 'ITST', jmm
; Added a whole bunch of tags, from the HESSI status page, see
; '~jimm/public_html/hessi/hsi_1day_sohdata/hsi_1day_sohdata_tbl.txt'
; 3-feb-2003, jmm, added Brian's plot limits
; Added a bunch of currents, 4-feb-2003, jmm, also does calculation
; over all of the orbits in a time period, weighting the first and
; last ones
;+
pro hsi_1day_sohdata, time_range, tags_for_output, pwpa_av, sigma, npak, $
                      names, plot_lo, plot_hi,  afile = afile, $
                      no_16bit_t = no_16bit_t, $
                      _extra = _extra

  common files_and_db, fb, file_table
  pwpa_av = -1 & sigma = -1 & npak = -1
  tr0 = anytim(time_range)
;Get the tags for the object
  if(not keyword_set(afile)) then $
    afile = '/disks/hessidata/metadata/hsi_1day_sohdata/hsi_1day_sohdata_tbl.txt'
  aaa = rd_tfile(afile, 13)
  no_zero_flag = fix(reform(aaa[0, *]))
  tags_for_output = strcompress(/remove_all, reform(aaa[1, *]))
  tags_for_object = strcompress(/remove_all, reform(aaa[2, *]))
  plot_lo = float(reform(aaa[3, *]))
  plot_hi = float(reform(aaa[4, *]))
  n = n_elements(tags_for_output)
  names = strarr(n)
  for j = 0,  n-1 do begin
    names[j] = strcompress(/remove_all, aaa[5, j])
    for k = 6, 12 do begin
      names[j] = names[j]+' '+strcompress(/remove_all, aaa[k, j])
    endfor
    names[j] = strtrim(names[j], 2)
  endfor
;you need all of the orbits for this time period
  if(datatype(fb) eq 'UND') then begin
    hsi_filedb_read, fb
    file_table = hsi_filedb_2_file_table(fb)
  endif
;this gives you the start and end times for each orbit
  orbs = where(file_table.start_time lt tr0[1] and $
               file_table.end_time gt tr0[0], norbs)
  print, 'N_orbits: ', norbs
  if(norbs Eq 0) then begin
    message, /info, 'No Orbits? This should never happen.'
    return
  endif
  file_table0 = file_table[orbs]
;ok, now set the time range to include all of the orbits
  oti = [file_table0[0].start_time, file_table0[norbs-1].end_time]
;Get the data
  pwpa = hsi_any_sohdata(oti, soh_label = tags_for_object, $
                         time_array = tim_arr, limits = lll, $
                         soh_name = soh_name)
  If(n_elements(pwpa) Eq 1 And pwpa[0] Eq -1) Then Begin
    message, /info, 'No good data for this day.'
    pwpa_av = -1 & sigma = -1 & npak = -1
    Return
  End
  pwpa = transpose(pwpa)        ;for concatenation purposes
  npak = n_elements(tim_arr)
  pak_today = tim_arr ge tr0[0] and tim_arr lt tr0[1]
  today = where(pak_today, npak_in_day) ;these are todays packets
;scale spin rate
  spinrt = where(tags_for_object eq 'TACESTSPINRT')
  if(spinrt[0] eq -1) then begin
    message, /info, 'No Spin Rate tag'
    goto, do_avg
  endif
  pwpa[spinrt[0], *] = pwpa[spinrt[0], *]*9.55
;scale pointing errors
  css_err = where(tags_for_object eq 'TACSCSSZ')
  if(css_err[0] eq -1) then begin
    message, /info, 'No CSS ERROR tag'
    goto, do_avg
  endif
  pwpa[css_err[0], *] = acos(pwpa[css_err[0], *])*180.0/!pi
  fss_err = where(tags_for_object eq 'TACSFSSZ')
  if(fss_err[0] eq -1) then begin
    message, /info, 'No FSS ERROR tag'
    goto, do_avg
  endif
  pwpa[fss_err[0], *] = acos(pwpa[fss_err[0], *])*180.0/!pi
  sas_err = where(tags_for_object eq 'TACSSASZ')
  if(sas_err[0] eq -1) then begin
    message, /info, 'No SAS ERROR tag'
    goto, do_avg
  endif
  pwpa[sas_err[0], *] = acos(pwpa[sas_err[0], *])*180.0/!pi
;cryopower
  bvolt = where(tags_for_object eq 'TPAGPACH25')
  bcurr = where(tags_for_object eq 'TPAGPACH35')
  if(bvolt[0] eq -1 or bcurr[0] eq -1) then begin
    message, /info, 'Can''t find battery voltage or current tags'
    goto, do_avg
  endif
  tags_for_output = [tags_for_output, 'CRYOPOWER']
  names = [names, 'Cryocooler Power [W]']
  cpower = pwpa[bvolt[0], *]*pwpa[bcurr[0], *]*0.8204 - 0.67
  pwpa = [pwpa, cpower]
  plot_lo = [plot_lo, 45.0]
  plot_hi = [plot_hi, 95.0]
;Battery State of charge
  battpress1 = where(tags_for_object eq 'TPAGPACH03')
  if(battpress1[0] eq -1) then begin
    message, /info, 'No Batt_press1 tag'
    goto, do_avg
  endif
  battpress2 = where(tags_for_object eq 'TPAGPACH04')
  if(battpress2[0] eq -1) then begin
    message, /info, 'No Batt_press2 tag'
    goto, do_avg
  endif
  battsoc1 = 0.01939 * pwpa[battpress1[0], *]- 24.0
  battsoc2 = 0.01984 * pwpa[battpress2[0], *]- 21.0
  tags_for_output = [tags_for_output, 'BATTSOC1', 'BATTSOC2']
  names = [names, 'Battery State of Charge 1 [%]', $
           ' Battery State of Charge 2 [%]']
  pwpa = [pwpa, battsoc1, battsoc2]
  plot_lo = [plot_lo, 60.0,  60.0]
  plot_hi = [plot_hi, 120.0, 120.0]
  names = [names, 'Battery Pressure Difference']
  tags_for_output = [tags_for_output, 'BATPRESSDIFF']
  pwpa = [pwpa, pwpa[battpress1[0], *]-pwpa[battpress2[0], *]]
  plot_lo = [plot_lo, -50.0]
  plot_hi = [plot_hi, 500.0]
;Long current:
;> >> >>GND_SUMCURR = + TPAGPACH21      "BAT CURRENT"
;> >> >>               - TPAGPACH27      "SOLAR ARRAY CURRENT"
;> >> >>               + TPAGPACH29      "ESSENTIAL BUS CURRENT"
;> >> >>               + TPAGPACH31      "NEB1 BUS CURRENT"
;> >> >>               + TPAGPACH32      "IDPU HEATER BUS CURRENT"
;> >> >>               + TPAGPACH33      "NEB2 BUS CURRENT"
;> >> >>               + TPAGPACH34      "IDPU CURRENT"
;> >> >>               + TPAGPACH35      "CRYO CURRENT"
;> >> >>               + TPAGPACH36      "IDPU SWITCHED LOADS CURRENT"
;>
  x21 = where(tags_for_object eq 'TPAGPACH21')
  x27 = where(tags_for_object eq 'TPAGPACH27')
  x29 = where(tags_for_object eq 'TPAGPACH29')
  x31 = where(tags_for_object eq 'TPAGPACH31')
  x32 = where(tags_for_object eq 'TPAGPACH32')
  x33 = where(tags_for_object eq 'TPAGPACH33')
  x34 = where(tags_for_object eq 'TPAGPACH34')
  x35 = where(tags_for_object eq 'TPAGPACH35')
  x36 = where(tags_for_object eq 'TPAGPACH36')
  xxxx = [x21, x27, x29, x31, x32, x33, x34, x35, x36]
  oops = where(xxxx eq -1)
  if(oops[0] ne -1) then begin
    message, /info,  'Missing a current'
    goto, do_avg
  endif
  gnd_sumcurr = pwpa[x21[0], *]-pwpa[x27[0], *]+pwpa[x29[0], *]+$
    pwpa[x31[0], *]+pwpa[x32[0], *]+pwpa[x33[0], *]+pwpa[x34[0], *]+$
    pwpa[x35[0], *]+pwpa[x36[0], *]
  pwpa = [pwpa, gnd_sumcurr]
  plot_lo = [plot_lo, -2.0]
  plot_hi = [plot_hi, 2.0]
  tags_for_output = [tags_for_output, 'GND_SUMMCURR']
  names = [names, 'SUMMED CURRENTS [A]']
;re-transpose
  do_avg: pwpa = transpose(pwpa)
;Do the averages
  pwpa_av = reform(pwpa[0, *]) & pwpa_av[*] = 0
  npwpa = n_elements(pwpa[*, 0])
  For j = 0, npwpa-1 Do pwpa_av[j] = avsig10(pwpa[j, *], sigma, sig_mean = sig_mean, /rm_outliers)
;replicate npak to an array, but only the packets for the day
  npak = replicate(npak_in_day, n_elements(tags_for_output))
;Ok, some of these need to be re-averaged, without zero values
  no_zeros = where(no_zero_flag eq 1, nno_zeros)
  if(nno_zeros gt 0) then begin
    for j = 0, nno_zeros-1 do begin
      temp = pwpa[*, no_zeros[j]] ;note it's retransposed
      xxx = where(temp gt 0, nxxx)
      yyy = where(temp gt 0 and pak_today, nyyy) ;for npak
      if(nxxx gt 0) then begin
        temp = temp[xxx]
        temp_av = avsig10(temp, stemp)
        pwpa_av[no_zeros[j]] = temp_av
        sigma[no_zeros[j]] = stemp
        npak[no_zeros[j]] = nyyy
      endif else npak[no_zeros[j]] = 0
    endfor
  endif
;Ok, now the 16-bit temperatures
  if(keyword_set(no_16bit_t)) then return
  tags1 = ['ICP1T', 'ICP2T', 'ICT1T', 'ICT2T', 'ITST']
  names1 = ['Cold Plate Temperature Monitor #1 (16 bit)', $
            'Cold Plate Temperature Monitor #2 (16 bit)', $
            'Cold Tip Temperature Monitor #1 (16 bit)', $
            'Cold Tip Temperature Monitor #2 (16 bit)', $
            'Thermal Shield Temperature Monitor (16 bit)']
  ntags1 = n_elements(tags1)
  pwpa_av1 = fltarr(ntags1)
  sigma1 = pwpa_av1
  hsi_16bit_temps, oti, ICP1T, ICP2T, ICT1T, ICT2T, ITST, tim_arr1
  if(tim_arr1[0] ne -1) then begin
;Need new weight factors
    npak1 = n_elements(tim_arr1)
    today = where(tim_arr1 ge tr0[0] and $
                  tim_arr1 lt tr0[1], npak1_in_day)
    npak1 = lonarr(ntags1)+npak1_in_day
    pwpa_av1[0] = avsig10(icp1t, sigma1sd)
    sigma1[0] = sigma1sd
    pwpa_av1[1] = avsig10(icp2t, sigma1sd)
    sigma1[1] = sigma1sd
    pwpa_av1[2] = avsig10(ict1t, sigma1sd)
    sigma1[2] = sigma1sd
    pwpa_av1[3] = avsig10(ict2t, sigma1sd)
    sigma1[3] = sigma1sd
    pwpa_av1[4] = avsig10(itst, sigma1sd)
    sigma1[4] = sigma1sd
  Endif Else Begin
    tags1 = ['ICP1T', 'ICP2T', 'ICT1T', 'ICT2T', 'ITST']
    pwpa_av1 = fltarr(5)
    sigma1 = pwpa_av1
    npak1 = lonarr(5)+8640
    names1 = ['Cold Plate Temperature Monitor #1 (16 bit)', $
              'Cold Plate Temperature Monitor #2 (16 bit)', $
              'Cold Tip Temperature Monitor #1 (16 bit)', $
              'Cold Tip Temperature Monitor #2 (16 bit)', $
              'Thermal Shield Temperature Monitor (16 bit)']
  Endelse
;Append these to output arrays
  tags_for_output = [tags_for_output, tags1]
  pwpa_av = [pwpa_av, pwpa_av1]
  sigma = [sigma, sigma1]
  npak = [npak, npak1]
  names = [names, names1]
;float plot limits for annealing, jmm, 4-nov-2007
;  plot_lo = [plot_lo, 0.0, 0.0, 0.0, 0.0, 0.0]
;  plot_hi = [plot_hi, 0.0, 0.0, 0.0, 0.0, 0.0]
;set upper limits to 110, jmm, 10-oct-2007, to 115, jmm, 13-feb-2013,
;to 120, jmm, 10-jul-2013, to 130, jmm, 22-jan-2014, to 140,jmm,
;5-jan-2015, to 180 and 140 , jmm, 2016-12-21
  plot_hi = [plot_hi, 180.0, 180.0, 140.0, 140.0, 200.0]
  plot_lo = [plot_lo, 60.0, 60.0, 60.0, 60.0, 100.0]
;App_id=102 data recovery
  f102 = hsi_data_recovery_fraction(oti, app_Id = 102, npakk = npakk)
  tags_for_output = [tags_for_output, 'data_frac102']
  pwpa_av = [pwpa_av, f102]
  sigma = [sigma, 1.0/8640.0]
  npak = [npak, npakk]
  names = [names, 'Fraction of recovered Science Data']
  plot_lo = [plot_lo, 0.00]
  plot_hi = [plot_hi, 1.20]

;PMTRAS OK count
  pmtras_ok = hsi_pmtras_ok(oti, sigma = sig_ok, npackets = npak_ok)
  tags_for_output = [tags_for_output, 'pmtras_count_ok']
  pwpa_av = [pwpa_av, pmtras_ok]
  sigma = [sigma, sig_ok]
  npak = [npak, npak_ok]
  names = [names, 'PMTRAS Count OK']
  plot_lo = [plot_lo, 50.0]
  plot_hi = [plot_hi, 150.0]

return
end

