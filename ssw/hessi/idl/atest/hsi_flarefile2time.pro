;+
; Name: hsi_flarefile2time
;
; Purpose: Return the time associated with a file(s) named with a RHESSI flare number.
;	Useful when trying to find the correct directory (yyyy/mm/dd) for the file.
;
; Calling sequence:  time = hsi_flarefile2time (file)
;
; Input arguments:
;	file - file name (or array of file names)
;
; Keywords:
;	_extra - in _extra pass in any keywords to select how the time is returned
;
; Example: print,hsi_flarefile2time('hsi_qlimg_2042101_000.fits', /date_only, /vms)
;          21-Apr-2002
;
; Output: time(s) of start of flare(s) contained in file(s) in requested format.
;
; Written:  Kim, 17-Oct-2003
; Modifications:
;   20-Apr-2011, Kim.  Allow 1 or 2 digit year
;   13-Aug-2013, Kim.  Allow 2 or 3 digit num (number of flare in day)
;-
;===========================================================================

function hsi_flarefile2time, file, _extra=_extra

; file name will have anything followed by ymmddxx or yymmddxx followed by anything
dyear = '([0-9]{1,2})'  ; 1 or 2 digits for year
dmon = '([0-9]{2})'
dday = '([0-9]{2})'
num = '([0-9]{2,3})'

sregex = dyear+dmon+dday+num

flare = stregex(file, sregex, /extr, /fold)

if n_elements(flare) eq 1 then flare = flare[0]

time = hsi_flare2time(flare, _extra=_extra, /start)
return, time

end