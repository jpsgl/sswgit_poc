;+
; NAME: hsi_get_extent_single
; PURPOSE:
;   Find minimum and maximum size of source within region(s) of a RHESSI image. Can use
;   the whole image, interactively define ROIs, or pass in a filename containing the ROIs. 
; METHOD:
;   PLOTMAN window allows users to interactively define regions containing sources(or can use
;   whole image, or pass in a filename containing the region(s).
;   Each source is rotated through 180 degrees in 1 degree increments, and the sigma
;   (standard deviation) at each angle is calculated.
;   Works for any rhessi image algorithm.
; CALLING SEQUENCE:
;   hsi_get_extent_single, o $
;     [, nobox=nobox, boxfile=boxfile, $
;     xs=xs, ys=ys, xm=xm, ym=ym, xk=xk, yk=yk, nbox=nbox,sources=sources,smapxy=smapxy]
; INPUT:
;   o - rhessi image object
;   nobox - if set, then use whole image, otherwise suppply boxfile or a widget will pop up to define boxes
;   boxfile - name of file to read containing boxes (written by plotman markbox widget)
; OUTPUT:
;   xs,ys - [180,nbox] array of sigmas in x,y direction at every angle in rotation (0-179, 1 degree increments)
;   xm,ym - [nbox] mean x,y value that source is rotated around
;   xk,yk -  - [180,nbox] array of kurtosis in x,y direction at every angle in rotation
;   nbox - number of boxes defined
;   sources - structure of x,y and image values used for each box
;   smapxy - image in x,y coordinates
; WRITTEN:
;   Rick Pernak, Kim Tolbert, Fall 2008
; MODIFIED:
;  Kim Tolbert - added header doc, and changed name from get_extent to put in SSW.
;  06-Jan-2017, Kim. Added boxfile option to pass in save file of box boundaries
;  08-Mar-2017, KIm. Renamed to hsi_get_extent_single from hsi_get_extent. Now hsi_get_extent, which can
;    handle image cubes, calls this for each image.
;  
;
;-
pro hsi_get_extent_single, o, $
  nobox=nobox, boxfile=boxfile, $
  xs=xs, ys=ys, xm=xm, ym=ym, xk=xk, yk=yk, nbox=nbox,sources=sources,smapxy

  xs = -1
  ys = -1

  alg = o -> get(/image_algorithm)

  if alg eq 'Clean' then begin
    ;get x,y coords of the annsec coords
    hsi_annsec_coord, o, x, y

    ; Save only the x,y, and flux values where the source map is non-zero
    ;uncorrected (for livetime) source map, also incorrect units
    smap_uc = o->get(/clean_source_map)
    smap = double(o->correct_source_map())
    cmap = o->get(/clean_component_map)
    cc = o->get(/clean_cleaned_map)
    map = o->getdata()
    factor = total(hsi_annsec2xy(cc,o))/total(map)
    smap = smap/factor
    cmap = cmap/factor

    smapxy = hsi_annsec2xy(smap,o,xg,yg,/noquin)
    xc = get_arr_center(xg,dx=dx)
    yc = get_arr_center(yg,dy=dy)
  endif else begin
    ;"smap" in MEM_NJIT and Pixon equivalent to "smapxy" with Clean since MEM
    ;and Pixon never converted to ANNSEC coordinates
    ;"smapxy" is used in other programs, but "smap" is used in this procedure

    pix = o -> get(/pixel_size)
    dx = pix[0] & dy = pix[1]
    dim = (o -> get(/image_dim))[0]
    xy = o -> get(/xyoffset)
    xc = xy[0] & yc = xy[1]
    half = dim / 2

    smap = o -> getdata()
    smapxy = smap
  endelse

  times = o->get(/absolute_time_range)
  map_str = make_map(smapxy, xc=xc,yc=yc,dx=dx,dy=dy,$
    time=anytim(times[0],/vms),dur=(times[1]-times[0]))  

  ;find non-zero data points in map
  q = where (smap ne 0., c)
  if c eq 0 then begin
    message,'Source map has no non-zero data.  Aborting', /cont
    return
  endif

  if alg eq 'Clean' then begin
    x_smap = x[q] ;corresponds to [1,*] (x-coordinates) in Clean component array
    y_smap = y[q] ;corresponds to [2,*] (y-coordinates) in Clean component array
    f_smap = smap[q] ;corresponds to [3,*] (fluxes) in Clean component array
  endif else begin
    x_smap = ((q mod dim) - half) * dx + xy[0]
    y_smap = ((q / dim) - half) * dy + xy[1]
    f_smap = smap[q]
  endelse

  ; if nobox is set, then use whole image, otherwise bring up widget to define boxes
  if keyword_set(nobox) then begin
    boxes = 0
    nperbox = 0
  endif else begin
    do_mark_box = 1
    if keyword_set(boxfile) then begin
      if file_exist(boxfile) then begin
        restore, boxfile
        boxes = *boxes_savefile.cw_list
        nperbox = *boxes_savefile.cw_nop
        do_mark_box = 0
      endif else message,"Box file supplied doesn't exist. Bringing up widget to mark box interactively.", /cont
    endif
    if do_mark_box then begin
      pp = plotman(input=map_str, plot_type='image', colortable=5)
      box_str = pp -> mark_box()
      boxes = box_str.list
      nperbox = box_str.nop
    endif
  endelse
  nbox = n_elements(nperbox)


  ; get indices of where the x_smap and y_smap values are in the image and
  ; convert them to 1d indices.
  ; set the image pixels to min of image so we can see them
  image_dim = o->get(/image_dim)
  image = o->getdata()
  xymap = fltarr(o->get(/image_dim))
  xaxis = o->getaxis(/xaxis)
  yaxis = o->getaxis(/yaxis)
  xind = value_locate(xaxis,x_smap)
  yind = value_locate(yaxis,y_smap)
  image[xind,yind] = min(image)
  xyind_1d = xind + yind*image_dim[0]

  ; arrays to accumulate the x and y stdev for each angle, for each box
  xs = fltarr(180,nbox)    &  ys = fltarr(180,nbox)
  xm = fltarr(nbox)  & ym = fltarr(nbox)
  xk = fltarr(180,nbox)  & yk = fltarr(180,nbox)

  ang = findgen(180) / !radeg

  ; loop through defined boxes

  i1 = 0

  image_temp = image

  srcflux = dblarr(nbox)
  for ibox=0,nbox-1 do begin
    comps = {xuse:ptr_new(/allocate_heap),yuse:ptr_new(/allocate_heap),$
      fuse:ptr_new(/allocate_heap)}
    if ibox eq 0 then sources = comps else sources = [sources,comps]

    if nperbox[ibox] eq 0 then begin
      ; if no boxes, use all data
      xuse = x_smap
      yuse = y_smap
      fuse = f_smap
    endif else begin
      ; for a box, find indices of original data that are in the box
      ; also, set pixels in image to max of image so we can see them and show image.
      i2 = i1 + nperbox[ibox] - 1
      box = boxes[*,i1:i2]
      ind = find_box_region_index(xaxis, yaxis, box)
      match, ind, xyind_1d, suba, subb
      if suba[0] eq -1 then begin
        message,'Box ' + trim(ibox) + ' has no source map points in it. Skipping.',/cont
        goto, nextbox
      endif
      ind = ind[suba]
      indx = ind mod image_dim[0]
      indy = ind / image_dim[0]
      image_temp[indx,indy]=.2 * (max(image) - min(image))
      xuse = x_smap[subb]
      yuse = y_smap[subb]
      fuse = f_smap[subb]
    endelse

    srcflux[ibox] = total(fuse)
    ;added by RLP 6/19/2008
    ;save the unrotated Clean components that are in defined region
    *sources[ibox].xuse = xuse
    *sources[ibox].yuse = yuse
    *sources[ibox].fuse = fuse

    ; get mean to rotate around
    mean_stdev, xuse, fuse, me=xmean, st=xst, kurt=xkurt
    mean_stdev, yuse, fuse, me=ymean, st=yst, kurt=ykurt

    xm[ibox] = xmean
    ym[ibox] = ymean

    xs[0,ibox] = xst  &  ys [0,ibox] = yst
    xk[0,ibox] = xkurt & yk[0,ibox] = ykurt

    range = minmax([xuse-xmean,yuse-ymean])+[-10,10]
    ;window,/free

    ; loop through 180 deg in 1 deg increments.  rotate data point to each angle, and find
    ; x and y stdev at each angle
    for iang=0,179 do begin
      theta = ang[iang]
      xr = (xuse - xmean)*cos(theta) + (yuse - ymean)*sin(theta)
      yr = -1.*(xuse - xmean)*sin(theta) + (yuse - ymean)*cos(theta)
      ;plot,xr,yr,xran=range,yran=range, psym=2 & wait,.001
      mean_stdev, xr, fuse, st=xst, kurt=xkurt
      mean_stdev, yr, fuse, st=yst, kurt=ykurt
      xs[iang,ibox] = xst   &  ys[iang,ibox] = yst
      xk[iang,ibox] = xkurt &  yk[iang,ibox] = ykurt
    endfor

    nextbox:
    i1 = i1 + nperbox[ibox]
  endfor

  fluxratio = total(srcflux)/total(smap)
  sfluxratio = string(fluxratio,format='(f5.2)')
  print,'Source Fluxes Total/Flux of Map = '+strcompress(sfluxratio,/rem)
  destroy,pp
end

