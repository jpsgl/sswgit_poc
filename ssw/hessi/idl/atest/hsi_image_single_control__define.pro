;---------------------------------------------------------------------------
; Document name: hsi_image_control__define.pro
; Created by:    Andre, April 27, 1999
;
; Last Modified: Sun May 27 22:13:38 2001 (csillag@MIMAS)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI IMAGE CONTROL STRUCTURE DEFINITION
;
; PURPOSE:
;       Defines the HESSI image control structure
;
; CATEGORY:
;       HESSI / Imaging
;
; CALLING SEQUENCE:
;       var = {hsi_image_control}
;
; EXAMPLES:
;       img_ctrl = {hsi_image_control}
;       help, img_ctrl, /structure
;
; SEE ALSO:
;       http://hessi.ssl.berkeley.edu/software/reference.html
;       HSI_Image_Control
;
; HISTORY:
;       Release 6: imaging_method introduced
;       Release 3 development, August 1999, ACs
;       Release 2 development, April 27, 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;       28-Apr-2010, Kim. Added profile parameters
;		17-May-2010, ras, Added NOQUINTIC_INTERP - for hsi_annsec2xy control
;		29-Oct-2010, Kim.  Added mc_ntrials, mc_show_plot
;		17-Feb-2017, Kim.  Added profile_ps_plot, profile_ps_dir, ,
;		  viscomp_show_plot, viscomp_window, viscomp_ps_plot, viscomp_ps_dir
;		09-Jul-2018, Kim. Added profile_jpeg_plot and viscomp_jpeg_plot params, and 
;		  changed profile_ps_dir and viscomp_ps_dir to profile_plot_dir and viscomp_plot_dir since for ps and jpeg. 
;
;-


PRO HSI_image_single_control__define

struct = { hsi_image_single_control, $
           flare_id_nr: 0L, $
           image_algorithm: '', $
           nvis_min: 0, $
           noquintic_interp: 0, $
           ;finescale: 0, $ ;maybe in the future, ras, 17-may-2010
           imaging_method: '', $    ; Not used
           profile_window: 0, $     ; window to draw profiles in
           profile_show_plot: 0, $  ; 0/1 disable/enable profile plots
           profile_ps_plot: 0, $    ; if set, draw profile plot in PS file (auto-generated name)
           profile_jpeg_plot: 0, $  ; if set, draw profile plot in jpeg file (auto-generated name)
           profile_plot_dir: '', $  ; directory to write profile plot file in
           profile_plot_rate: 0, $  ; 0 means plot counts/bin; 1 means plot count/sec
           profile_plot_resid: 0, $ ; If set plot profile residuals
           mc_ntrials: 0, $         ; Number of Monte Carlo trials to determine probability of C-statistic
           mc_show_plot: 0, $       ; If set, show Monte Carlo distribution of C-statistic for separate and combined detectors
           viscomp_show_plot: 0, $  ; 0/1 disable/enable visibility comparison plot
           viscomp_window: 0, $     ; window to draw vis comp plot in
           viscomp_ps_plot: 0, $    ; if set, draw vis comp plot in PS file (auto-generated name)
           viscomp_jpeg_plot: 0, $  ; if set, draw vis comp plot in jpeg file (auto-generated name)
           viscomp_plot_dir: '' }   ; directory to write viscomp plot file in

END


;---------------------------------------------------------------------------
; End of 'hsi_image_control__define.pro'.
;---------------------------------------------------------------------------
