;---------------------------------------------------------------------------
; Document name: hsi_clean__define.pro
; Created by:    Andre Csillaghy, March 4, 1999
; Time-stamp: <Tue Feb 01 2005 17:20:03 csillag auriga.ethz.ch>
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI CLEAN ALGORITHM CLASS DEFINITION
;
; PURPOSE:
;       Object wrapper for the clean algorithm
;
; CATEGORY:
;       Imaging (hessi/image)
;
; CONSTRUCTION:
;       image_obj = Obj_New( 'hsi_clean' )
;
; METHODS:
;      No public methods
;
; OUTPUT TYPE:
;      2D IMAGE ARRAY
;
; INPUT PARAMETERS:
;       Control (input) parameters are defined in the structure
;       {hsi_clean_parameters}.
;
; OUTPUT PARAMETERS:
;       Info (output) parameters are
;       CHI_SQ_TOT: the value of the chi2 at the exit of hsi_map_clean
;       RESID_MAP: the residual map
;
; SOURCE OBJECT:
;       HSI_Modpat_Products
;
; SEE ALSO:
;       hsi_map_clean
;       http://hessi.ssl.berkeley.edu/software/reference.html#hsi_clean
;
; HISTORY:
;   Development for Release 4, April 2000
;   24-Feb-2001, Kim - added mark_box block
;   9-Jan-2001, Kim - corrected mark_box option and call plotman mark_box method now
;     instead of hsi_clean_box_options
;   27-Feb-2002, Kim - remove clean boxes that are outside image window.  Make colortable 5
;     the default for image for marking clean boxes.
;  17-Mar-2003, RAS, if bproj_need_update is set, must start at iter 0.
;  30-Apr-2010, Kim. If clean_regress_combine is set, use regression to combine component map and residual map
;  20-aug-2010, ras, set output clean_map to 0 if there is no cleaning, do not add residual, no regression
;     04-apr-2013, ras, rerun hsi_map_clean only when necessary. If those params haven't changed then only
;  	the post processing algs need to be run, Convolve_With_Beam and Regress_Combine
;  	trap some control parameters to avoid reprocessing,
;  29-may-2013, ras, protect against bproj changing and returning to clean after
;  	changing algorithms.  if bproj has changed, and updated, clean still needs to update
;  	11-mar-2016, ras, allow input_dirty_map through control parameters
;  3-may-2017, RAS, changing the name of im_in to dirty_map and minor formatting changes. Removed /weight
;  in creating dirty_map as that wasn't passed through.  Spatial weighting used as set in the bproj control params
;  4-Aug-2017, Kim and RAS. Added image_done property.  This was needed because in redo_clean, checks whether control
;    parameters (excluding several getdata params) have changed, and if not, it returns a 0 meaning don't need to
;    remake image. Because of this, clean_obj->set,/need_update,/this_class did not trigger a redo. Now trap need_update
;    in set method, and if set, set image_done to 0.
;  15-nov-2017, RAS, new regress method and reporting regression coefficient in clean_coef_regress
;  29-nov-2017, RAS, added discussion about the use of the corrfactors to make the dirty map which is reduced by
;  the psfs
;  2-feb-2018, RAS, trap flatfield in Set. If it has changed from the last value used, reprocess
;  Also, check FLATFIELD in need_update() against the last used value. If it has changed, then set need_update
;  in this class as well as in hsi_psf class so that the psf cache can be cleared
;  6-feb-2018, Kim. Changed need_update to return a 1 as soon as it finds a need_update that's set. Added
;    setting clean_det_index_mask_used after image is made in image_alg_hook. And in need_update, added call to 
;    hsi_image_alg::need_update to check for change in det mask.
;   2-apr-2018, RAS - clarifying choices for regression.  See hsi_clean_regress_combine
;   for the full set of choices and their implementation, checking for changes in det_index_mask in REDO_CLEAN
;   4-apr-2018, RAS, updated redo_clean() and logic affecting redo_clean in alg_hook
;   change bproj_need_update to modpat_need_update - if the modpat class has changed that means something
;   about the fov has changed, so the dirty map and the psf have to change. So clean has to be performed.  If
;   it's only about regress or beam_width, those can be done without the need to call hsi_map_clean which is 
;   the core routine, the real process function
;-
;
FUNCTION HSI_Clean::INIT, _EXTRA=_extra

  ret=self->HSI_Image_Alg::INIT( 'clean', $
    CONTROL=hsi_clean_parameters(), $
    INFO={hsi_clean_info}, $
    _EXTRA=_extra)

  self -> set, clean_chi_window=-1, clean_map_window=-1  ; info parameters weren't getting set, kim
  self.image_done = 0

  RETURN, ret

END
;--------------------------------------------------------------------
;+
;  Name: HSI_CLEAN::REGRESS_COMBINE
;
;  pixscl - if set, units of output image will be pixon-like units (which is what modul_profile can handle)
;           if not set, leave in clean's units
;  18-may-2010, protected against all 0 resid_map in media mode
;  25-aug-2010, ras, break out function
;  15-nov-2017, ras, blocking change
;  04-apr-2018, ras, included REGRESS_COMBINE method within this file, Now checking to see if the
;  modpat class needed an update forcing a redo call to hsi_map_clean
;-
function hsi_clean::regress_combine, comp_map=comp_map, resid_map=resid_map, $
  c_coef = c_coef, r_coef = r_coef,  pixscl=pixscl, _extra=_extra

  return, $
    hsi_clean_regress_combine( Self,  comp_map=comp_map, resid_map=resid_map,$
    c_coef = c_coef, r_coef = r_coef,  pixscl=pixscl, _extra=_extra)
end
;--------------------------------------------------------------------
PRO hsi_clean::Set, $
  clean_progress_bar = clean_progress_bar, $
  clean_show_n_maps = clean_show_n_maps, $
  clean_show_n_chi = clean_show_n_chi, $
  need_update = need_update, $
  flatfield = flatfield, $
  _EXTRA=_extra

  If Keyword_set( flatfield ) then begin ;added 2-feb-2018, RAS
    old_flatfield = Self->Framework::Get(/flatfield)
    if flatfield ne old_flatfield then Self->Set, /need_update, flatfield = flatfield
  Endif
  If exist( clean_progress_bar ) then $
    Self->Framework::Set, clean_progress_bar = clean_progress_bar, /NO_UPDATE
  If exist( clean_show_n_maps )  then $
    Self->Framework::Set, clean_show_n_maps = clean_show_n_maps, /NO_UPDATE
  If exist( clean_show_n_chi )   then $
    Self->Framework::Set, clean_show_n_chi = clean_show_n_chi, /NO_UPDATE

  if keyword_set(need_update ) then self.image_done = 0

  ; for all other parameters (included in _extra), just pass them to the
  ; original Set procedure in Framework

  IF Keyword_Set( _EXTRA ) THEN BEGIN
    self->Framework::Set, need_update = need_update, _EXTRA = _extra
  ENDIF

END
;--------------------------------------------------------------------
function HSI_Clean::Need_update ;4-feb-2018, added by RAS
  
  flatfield_update = Self->Framework::Get(/clean_flatfield_used ) ne Self->Framework::Get( /flatfield )
  if flatfield_update then begin ;set need update on the psf as this will clear the psf cache
    opsf = Self->Get(/obj, class='hsi_psf')
    if is_class( opsf,'hsi_psf') then opsf->set, /need_update, /this_class
    return, 1
  endif
  
  return, Self->hsi_image_alg::need_update()
  
end

;--------------------------------------------------------------------
PRO HSI_Clean::Convolve_With_Beam

  ;+
  ;Name: HSI_CLEAN::Convolve_With_Beam
  ;
  ;Purpose: This procedure builds the component map by summing over the convolved source
  ;	components.  The size of the Gaussian comes from HSI_CLEAN_BEAM_POLAR which uses the
  ;	grid weighting function and the clean_beam_width_factor scaling
  ;
  ;
  ;Written: 4-apr-2013, ras. Based on methods used with HSI_Map_Clean()
  ;-


  params = tag_prefix( Self->Get(/clean_normalization, /clean_lt_correction, $
    /clean_resid_map, /clean_source_map, /clean_beam_width_factor), 'clean',/remove)
  xy = where( params.source_map, nzi)
  flx= params.source_map[xy]
  ;RAS 20-aug-2010-------------
  if nzi eq 0 then begin
    message,/continue,'No Clean Components Found: Setting output map to 0'
    null_image = 1
  endif  else null_image = 0
  ;RAS 20-aug-2010-------------
  comp_map = 0.0
  is_cart = stregex( /bool, /fold, hsi_get_modpat_strategy(self ), 'cart')
  map_dim = is_cart ? self->Get( /image_dim ) : self->Get( /rmap_dim )
  if nzi ge 1 then begin
    for inzi = 0, nzi-1 do comp_map += flx[inzi]*hsi_clean_beam_polar(xy[inzi], self, params)
    comp_map = reform( comp_map, map_dim,/over) / $
      (params.normalization * params.lt_correction)
  endif
  Self->set, CLEAN_COMPONENT_MAP = comp_map
  self->Set, CLEAN_CLEANED_MAP = (comp_map + params.resid_map) *(1-null_image)

end

;--------------------------------------------------------------------

PRO HSI_Clean::Image_Alg_Hook, param, image_out, param_out, _EXTRA = _extra

  ;The param are the control parameters for HSI_CLEAN with "CLEAN_" stripped
  source = self->Get( /SOURCE )
  bproj = self->get(/obj, class_name='hsi_bproj')

  ;  bproj_need_update = bproj->need_update( )
  ;  ;Whle bproj may be up to date if it last processed after Self
  ;  ;then we need to rerun clean because the clean object's fov params
  ;  ;may have changed
  ;  bproj_need_update = bproj_need_update eq 0 ? $
  ;    bproj->Get(/Last_Update) gt Self->Get(/Last_Update) : $
  ;    bproj_need_update
  ;
  modpat_need_update =  (self->get(/obj, class='hsi_modul_pattern'))->need_update()
  dirty_map = bproj->getdata()

  cbe    = bproj->getdata( class = 'hsi_calib_eventlist' )
  cbe_normalize   = self.cbe_normalize
  cbe_corrfactors = hsi_get_corrfactors( self, cbe_normalize = cbe_normalize ) ;   15-nov-2017, RAS
  ;default, cbe_corrfactors, bytarr( n_elements( prf9 )) + 1b
  ; Corrfactors are >1 for count rates lower than their expected values
  ; we multiply each detectors dirty map by the corrfactor and sum to get the final dirty map
  ; As the dirty map is obtained by the count rate multiplying the modulation patterns, the low values
  ; are amplified to match the dirty map expected from a set of perfectly balanced subcollimators. As this is
  ; what the PSF produces, the dirty map is then a better match to the PSF.
  dirty_maps = bproj->getdata( vrate=cbe, /no_sum ) ;/this_flatfield, /no_sum )
  dirty_dims = size( /dimension, dirty_maps )
  dirty_map  = reform( dirty_maps, product( dirty_dims[0:1] ),dirty_dims[2] ) # cbe_corrfactors
  dirty_map  = reform( dirty_map, /over, dirty_dims[0:1] )

  ;4-apr-2017, RAS, end of changes
  ;
  ; kim added following block
  ; acs changes mark_box to clean_mark_box

  ; remove any boxes that are no longer within image window.  kim 27-feb-2002
  cw_list = self-> get(/clean_cw_list)  &  cw_nop = self->get(/clean_cw_nop)
  if cw_nop[0] gt 0 then begin
    im_size = round(self->get(/pixel_size) * self->get(/image_dim))
    xyoffset = self->get(/xyoffset)
    im_area = round([xyoffset - im_size/2., xyoffset + im_size/2.])

    i2 = -1
    for i = 0,n_elements(cw_nop)-1 do begin
      i1 = i2+1 & i2 = i1 + cw_nop[i] - 1
      box = cw_list[*,i1:i2]
      xr = minmax(box[0,*]) & yr = minmax(box[1,*])
      if  (xr[0] lt im_area[2] and xr[1] gt im_area[0]) and $
        (yr[0] lt im_area[3] and yr[1] gt im_area[1]) then begin
        new_cw_nop = append_arr (new_cw_nop, cw_nop[i])
        if exist(new_cw_list) then new_cw_list = [[new_cw_list],[box]] else new_cw_list = box
      endif else begin
        print,'%HSI_CLEAN__DEFINE:  Clean box ' + trim(i) + ' is outside the image. It was removed.'
      endelse
    endfor
    if not exist(new_cw_nop) then new_cw_nop = 0
    if not exist(new_cw_list) then new_cw_list = 0
    self->set, _extra={clean_cw_list: new_cw_list, clean_cw_nop: new_cw_nop}
    param.cw_list = ptr_new(new_cw_list)
    param.cw_nop = ptr_new(new_cw_nop)
  endif

  if self -> get(/clean_mark_box) then begin
    save_window = !d.window
    plotman_obj = plotman (input=(self.source[0]->Get( CLASS_NAME='hsi_bproj', /obj)) , $
      class_name='hsi_bproj', plot_type='image', colortable=5)
    pboxes = plotman_obj -> mark_box(list=self->get(/clean_cw_list), nop=self->get(/clean_cw_nop), $
      cancel=cancel, type='Clean' )
    boxes = {clean_cw_list: pboxes.list, clean_cw_nop: pboxes.nop}
    self -> set,_extra=boxes
    print,'Setting ', strtrim(n_elements(pboxes.nop),2), ' clean boxes.'
    obj_destroy, plotman_obj
    ; need to do following because name changes with clean_ not in param struct. and
    ; param is not retrieved again before call to hsi_map_clean as it was previously, Kim
    ; don't use ptr_free here because may be pointing to same location as in control.
    *param.box = 0
    param.cw_list = ptr_new(boxes.clean_cw_list)
    param.cw_nop = ptr_new(boxes.clean_cw_nop)

    wset, save_window
  endif

  ;this one needs to be handled separately

  ; quiet kwd acs 2005-02-01
  param = Rep_Tag_Name( param, 'BOX', 'CLEAN_BOX', /quiet )
  param = Rep_Tag_Name( param, 'CHI_WINDOW', 'CLEAN_CHI_WINDOW', /quiet )
  param = Join_Struct( param, self->Get( /INFO, /THIS_CLASS_ONLY, /NO_DEREFERENCE ) )
  ;If the bproj_need_update is set, have to start at 0
  If modpat_need_update then param.more_iter = 0
  ;Here we are trapping for 3 parameters that don't need the full map_clean done
  ;if only they have changed, BEAM_WIDTH_FACTOR (used to convolve sources), and *REGRESS_COMBINE
  redo_clean =  Self->Redo_Clean() or modpat_need_update;

  null_image= 0 ;assume a good image will be made
  if redo_clean then begin
    dirty_map = keyword_set( *param.input_dirty_map ) ? *param.input_dirty_map : dirty_map
    clean_out = HSI_Map_Clean( source, dirty_map, CLEAN_PARAM=param );, cbe_normalize = self.cbe_normalize )
    Self->Set, clean_flatfield_used = Self->Get( /flatfield )
;    Self->Set, clean_det_index_mask_used = Self->Get( /det_index_mask)
    Self->Set, Clean_Info_Control = Self->Get(/control, /clean)
    self.image_done = 1

    if max(abs(clean_out.clean_map)) eq 0 then begin
      message,/continue,'No Clean Components Found: Setting output map to 0'
      null_image = 1
      null_map   = clean_out.clean_map
    endif  else null_image = 0
    ;Remove CLEAN_MAP from CLEAN_OUT to avoid a collision with CLEAN_MAP_WINDOW when the
    ;values in CLEAN_OUT are set through ->Set, _extra=CLEAN_OUT a few lines down.
    clean_out = rem_tag( clean_out, 'clean_map')

    ;Add the prefix 'CLEAN_'
    clean_out = tag_prefix( clean_out,'CLEAN')
    ;Since the tags in clean_out have the same names as those in the info parameter
    ;we can pass them in throug _extra saving  many ugly steps
    self->Set, _extra = clean_out
  endif
  ;CLEAN_COMPONENT_MAP and CLEAN_CLEANED_MAP are set within the next method
  Self->Convolve_with_Beam ;apply the clean beam with beam width factor

  ; added 30-apr-2010,Kim. If clean_regress_combine is set, use regression to combine component map and
  ; residual map.  Otherwise, do it the old way - just add comp and resid maps done in Convolve_With_Beam.
  ;RAS, 2-apr-2018 - clarifying choices for regression.  See hsi_clean_regress_combine
  ;for the full set of choices and their implementation
  use_regress = hsi_clean_regress_combine_list( self->Get(/CLEAN_REGRESS_COMBINE) ) ne 'disable'
   
  image_out = null_image ? null_map : $
    (use_regress ? self->regress_combine(comp_map=self->get(/clean_component_map), $
    resid_map=self->Get(/clean_resid_map), c_coef = c_coef, r_coef = r_coef ) : $
    self->Get(/CLEAN_CLEANED_MAP) )
  if exist( c_coef ) then begin
    ;help, c_coef[0]
    Self-> set, clean_coef_regress = c_coef
    components = Self->Get( /clean_components )
    ;components[ 3, *] *= c_coef[0]
    ;Self->Set, clean_components = components  ;keeping the components as is.
    if ~exist( r_coef ) then begin
      ;TBD RAS, 15-nov-2017
      ;We have a new c_coef so take the resultant clean_map and subtract for the dirty map
      ;to get the new residual
    endif
  endif
  ;if exist( r_coef ) then help, r_coef[0]

END
;--------------------------------------------------------------------
function HSI_CLEAN::Redo_Clean
  ;+
  ;Name: HSI_CLEAN::Redo_Clean
  ;
  ;Purpose: This function returns a 1 if we need to rerun hsi_map_clean() because one of
  ;	the inside CLEAN control paramters have changed or because need_update was set.
  ;   We do not if the only controls to change were the last 3,
  ;		old_regress_method, regress_combine, beam_width_factor
  ;Written: 4-apr-2013, ras
  ;-

  if ~self.image_done then return, 1  ; either haven't made an image yet, or need_update was set, so process

  Old_control = Self->Get(/clean_info_control)
  if size(/tname, old_control) ne 'STRUCT' then return, 1 ;First time in, we must process

  ;We're only here after the first pass so OLD_CONTROL is a structure from the first pass
  ;of the CLEAN control parameters

  ;And now the current CLEAN control parameters
  control =  Self->Get(/control, /clean)
  redo_clean0 = ~match_struct( old_control, control, $
    exclude='CLEAN_'+['REGRESS_COMBINE','BEAM_WIDTH_FACTOR','REGRESS_QUIET'])  
  ;Check to see if the det_index_mask has changed, 3-apr-2018, RAS
  redo_clean1 = ~same_data( self->get(/clean_det_index_mask_used), self->get(/det_index_mask) )
  return, redo_clean0 or redo_clean1
  
end
;--------------------------------------------------------------------

PRO HSI_Clean__Define

  self = {HSI_Clean, $
    image_done: 0, $
    INHERITS HSI_Image_Alg }

END


;---------------------------------------------------------------------------
; End of 'hsi_image__define.pro'.
;---------------------------------------------------------------------------
