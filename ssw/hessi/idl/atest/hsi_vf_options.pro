
;+
; Name: hsi_vf_options
;
; Purpose: Widget to set parameters specific to MEM NJIT image algorithm. Called from
;   hsi_ui_img.  Is a modal widget.
;
; Calling sequence:  new_vals = hsi_vf_options (struct, group=group)
;
; Input arguments:
;   struct - structure containing the values that this widget handles (normally
;     struct is the entire image control structure)
;   group - widget id of calling widget
;
; Output: structure containing new values of vf parameters
;
;
; Written:  Kim Tolbert 1-nov-2007
; Modifications:
;  06-Jul-2017, Kim. Made this a non-blocking widget. Now can set params here and use 'make image'
;    button from image widget to make an image. Values get set into object every time they're changed
;    (although for numeric values user has to hit enter (at least after last one typed). Added fitmask
;    red/green buttons. Also added buttons (at top of widget) to reset both sources to defaults, or
;    previous fwdfit results, or to select the source parameters for each enable source from an existing
;    image in plotman using image flux tool, or just set location by clicking in image.  Also added those
;    last two options for each source separately.  Added a Refresh button in case user changes values from
;    command line.  Decided not to automatically set previous fit results as input for next fit because then
;    we lose the starting values for that fit (need to add more params to keep track, like ospex does).
;    Added a help button.
;-
;===========================================================================


; hsi_vf_options_widget_update updates the widgets with the current values of parameters

pro hsi_vf_options_widget_update, state

  srcstr = state.srcstr

  for j=0,1 do begin
    widget_control, state.w_srcbase[j], sensitive=state.src_sel[j] ne 0
    shape_index = ( where (strlowcase(srcstr[j].srctype) eq strlowcase(state.shapes), n) )[0]
    if n eq 1 then begin
      widget_control, state.w_shape[j], set_value=shape_index
      for i=0,6 do begin
        widget_control, state.w_srcparm[i,j], set_value=trim(srcstr[j].(state.parm_strindex[i]))
        fit = srcstr[j].fitmask[state.parm_strindex[i]]
        widget_control, state.w_fitmask[i,j], set_value=fit ? state.green_bmp : state.red_bmp, set_uvalue=fit
      endfor
    endif

    for i=4,5 do begin
      widget_control, state.w_srclabel[i,j], sensitive=(shape_index gt 0)
      widget_control, state.w_srcparm[i,j], sensitive=(shape_index gt 0)
      widget_control, state.w_fitmask[i,j], sensitive=(shape_index gt 0)
    endfor
    widget_control, state.w_srclabel[6,j], sensitive=(shape_index eq 2)
    widget_control, state.w_srcparm[6,j],  sensitive=(shape_index eq 2)
    widget_control, state.w_fitmask[6,j],  sensitive=(shape_index eq 2)
  endfor

  widget_control, state.w_nvismin, set_value=trim(state.vals.nvis_min, '(i4)')
  widget_control, state.w_maxiter, set_value=trim(state.vals.vf_maxiter, '(i4)')
  widget_control, state.w_show, set_button = (state.vals.vf_noplotfit eq 0)
  widget_control, state.w_nophase, set_button = state.vals.vf_nophase
  widget_control, state.w_abs, set_button = state.vals.vf_absolute
  widget_control, state.w_noerr, set_button = state.vals.vf_noerr

end

;-----

; hsi_vf_options_event handles events from the widget

pro hsi_vf_options_event, event

  widget_control, event.top, get_uvalue=state

  widget_control, event.id, get_uvalue=uvalue

  cancel = 0
  exit = 0

  ; Currently the only numeric uvalue is the fitmask. If add more, have to differentiate here.
  if is_number(uvalue) then begin
    widget_control, event.id, get_uvalue=val
    val = val ? 0 : 1  ; whatever it was, set it to the other
    widget_control, event.id, set_value=val ? state.green_bmp : state.red_bmp, set_uvalue=val
    uvalue=''
  endif

  if strmid(uvalue,0,3) eq 'src' then begin
    srcid = fix(strmid(uvalue, 3, 1))
    uvalue = strmid(uvalue, 5, 99)
  endif else srcid = -1

  case uvalue of
    'enable': state.src_sel[srcid] = 1

    'disable': state.src_sel[srcid] = 0

    'autoset': begin
      if srcid eq -1 then begin
        srcid = 0	;if didn't select a src, just do 0th
        state.src_sel[1] = 0
      endif
      state.srcstr[srcid].srctype = 'ellipse'
      xyoffset = state.obj -> get(/xyoffset)
      state.srcstr[srcid].srcx = xyoffset[0]
      state.srcstr[srcid].srcy = xyoffset[1]
      state.srcstr[srcid].srcflux = 1
      state.srcstr[srcid].srcfwhm = 10.
      state.srcstr[srcid].fitmask = 1
      hsi_vf_options_widget_update, state
    end

    'setprev': begin
      vf_srcout = state.obj -> get(/vf_srcout)
      if is_struct(vf_srcout) then begin
        nsrc = n_elements(vf_srcout)
        if srcid eq -1 then begin
          state.srcstr[0]= vf_srcout[0]
          state.src_sel[0] = 1
          if nsrc eq 2 then begin
            state.srcstr[1] = vf_srcout[1]
            state.src_sel[1] = 1
          endif else state.src_sel[1] = 0
        endif else state.srcstr[srcid] = vf_srcout[srcid < (nsrc-1) ]
      endif else message, /info, 'No previous VIS_FWDFIT results to use.'
      hsi_vf_options_widget_update, state
    end

    'setmark': begin
      ;      hessi_data,plotman=p
      p = state.obj->get_plotman()
      if ~is_class(p, 'plotman') || ~p->valid_window(/image) then begin
        message, /info, 'No image plotted yet.'
        return
      endif
      p -> image_flux, struct=rstruct, /ellipse

      ; rstruct isn't a structure, user must have cancelled out of image_flux, so don't do anything
      if is_struct(rstruct) then begin
        ; If more than one structure, but just doing this for a particular source (srcid), just use first
        ; element of rstruct.
        ; ind will be index into rstruct to use. srcid will be index into state.srcstr array that we're setting.
        nr = n_elements(rstruct)
        if nr gt 1 then begin
          if srcid eq -1 then begin
            ind = [0,1]
            srcid = ind
          endif else begin
            message, /info, 'You specified more than one region for this single source. Using 0th region only.'
            ind = 0
          endelse
        endif else begin
          ind = 0
          if srcid eq -1 then srcid = ind
        endelse

        state.srcstr[srcid].srctype = 'ellipse'
        state.srcstr[srcid].srcflux = rstruct[ind].flux
        state.srcstr[srcid].srcx = rstruct[ind].centroid[0]
        state.srcstr[srcid].srcy = rstruct[ind].centroid[1]
        state.srcstr[srcid].srcfwhm = nr gt 1 ? max(rstruct[ind].stdev, dim=1) : max(rstruct[0].stdev)
        smaj = rstruct[ind].major / 2.
        smin = rstruct[ind].minor / 2.
        print,'semi-major, semi-minor axes = ', smaj,smin
        state.srcstr[srcid].eccen = sqrt(smaj^2 - smin^2) / smaj
        state.srcstr[srcid].srcpa = rstruct[ind].position_angle
        ;        state.src_sel[0:1] = 0
        state.src_sel[srcid] = 1
        hsi_vf_options_widget_update, state
      endif
    end

    'setpos': begin
      p = state.obj->get_plotman()
      if ~is_class(p, 'plotman') || ~p->valid_window(/image) then begin
        message, /info, 'No image plotted yet.'
        return
      endif

      if srcid eq -1 then srcid = [0,1]
      state.src_sel[srcid] = 1 ; enable this source in case it wasn't enabled

      print, '(Note: all widgets and command line are blocked until you click in image.)'
      nsrc = n_elements(srcid)
      for i=0,nsrc-1 do begin
        is = srcid[i]
        if state.src_sel[is] then begin
          print, 'Click (any button) in the image to set the location of source ' + trim(is)
          xy = p->point()
          if n_elements(xy) ne 1 then begin
            print,'Source position chosen for source ' + trim(is) + ': ', xy[0], xy[1]
            state.srcstr[is].srcx = xy[0]
            state.srcstr[is].srcy = xy[1]
          endif
        endif
      endfor
      print, 'Finished clicking for location.'
      hsi_vf_options_widget_update, state
    end

    'shape': state.srcstr[srcid].srctype = state.shapes[event.value]

    'help': begin
      check = concat_dir(local_name('$SSW/hessi/idl/image\visibility\gh\'), 'vis_fwdfit_options_help.txt')      
      file = file_search (check, count=count)
      if count gt 0 then begin
        msg = rd_ascii(file[0], error=error)
        if not error then begin
          xdisplayfile, file, text=wrap_txt(msg, length=100, delim=' ', /no_dollar), $
            done_button='Close', group=event.top, /grow_to_screen, $
            width=100, title='VIS FWDFIT Options Help', return_id=x_id
          winfo = widget_info(event.top, /geom)
          widget_control, x_id, xoffset=winfo.xoffset+50, yoffset=winfo.yoffset+50
        endif
      endif else error=1

      if error then a = dialog_message('Error finding or reading help file ' + check)
    end

    'refresh': begin
      hsi_vf_getparams, obj=state.obj, vals, srcstr, src_sel
      state = rep_tag_value (state, vals, 'vals')
      state = rep_tag_value (state, srcstr, 'srcstr')
      state = rep_tag_value (state, src_sel, 'src_sel')
      hsi_vf_options_widget_update, state
      widget_control, event.top, set_uvalue=state
      goto, getout  ; don't need to collect values from widget and set, since we just set everything
    end

    'reset': begin
      vf_vals = tag_prefix( hsi_fwdfit_parameters(), 'vf')
      vf_vals = rem_tag(vf_vals, 'vf_srcin')
      img_single_vals = hsi_image_single_control()
      vf_vals = add_tag (vf_vals, img_single_vals.nvis_min, 'nvis_min')
      temp = state.vals
      copy_tag_values, temp, vf_vals
      state.vals = temp
      state.srcstr = replicate(hsi_vis_src_structure(),2)
      state.srcstr[*].srctype = 'ellipse'
      state.src_sel = [1,0]
      hsi_vf_options_widget_update, state
    end

    'cancel': cancel = 1

    'accept_close': exit = 1

    'show':  state.vals.vf_noplotfit = (event.select eq 0)

    'nophase':  state.vals.vf_nophase = event.select

    'abs': state.vals.vf_absolute = event.select

    'noerr': state.vals.vf_noerr = event.select

    else:

  endcase

  if cancel then begin

    state.obj -> set, _extra = state.orig_vals
    state.obj -> set, vf_srcin = state.orig_vf_srcin
    widget_control, event.top, /destroy
    return

  endif else begin

    for is = 0,1 do begin
      if state.src_sel[is] ne 0 then begin
        for i=0,6 do begin
          widget_control, state.w_srcparm[i,is], get_value=val
          state.srcstr[is].(state.parm_strindex[i]) = val
          widget_control, state.w_fitmask[i,is], get_uvalue=val
          state.srcstr[is].fitmask[state.parm_strindex[i]] = val
        endfor
      endif
    endfor

    widget_control, state.w_nvismin, get_value=nvis_min
    widget_control, state.w_maxiter, get_value=maxiter
    state.vals.vf_maxiter = maxiter
    state.vals.nvis_min = nvis_min

    hsi_vf_options_widget_update, state

    sum = 0 & for i=1,7 do sum=sum+state.srcstr[0].(i)
    if sum eq 0. then begin
      new_vals = add_tag(state.vals, ptr_new(), 'vf_srcin')
      new_vals.vf_circle = strlowcase(state.srcstr[0].srctype) eq 'circle'
      new_vals.vf_loop = strlowcase(state.srcstr[0].srctype) eq 'loop'
      new_vals.vf_multi = strlowcase(state.srcstr[0].srctype) eq 'ellipse' and state.src_sel[1]
    endif else begin
      q = where (state.src_sel)
      new_vals = add_tag(state.vals, state.srcstr[q], 'vf_srcin')
      new_vals.vf_circle = 0
      new_vals.vf_loop = 0
      new_vals.vf_multi = 0
    endelse
    ;	; Store values in pointer so we can get them back to calling program after destroy widget
    ;	*state.ptr = new_vals
    ; set values into image object right here, since this isn't a blocking widget anymore
    state.obj -> set, _extra = new_vals
    if exit then widget_control, event.top, /destroy else widget_control, event.top, set_uvalue=state
  endelse

  getout:
end

;-----

; Get parameters from object. Must pass in either obj or control structure.
pro hsi_vf_getparams, obj=obj, control=control, vals, srcstr, src_sel

  if ~is_struct(control) then control = obj->get(/control)

  vals = str_subset (control, $
    ['vf_circle', $
    'vf_multi', $
    'vf_loop', $
    'nvis_min', $
    'vf_nophase', $
    'vf_noerr', $
    'vf_absolute', $
    'vf_maxiter', $
    'vf_noplotfit'])

  vf_srcin = control.vf_srcin

  srcstr = replicate(hsi_vis_src_structure(),2)
  if is_struct(vf_srcin) then begin
    nsrc = n_elements(vf_srcin)
    srcstr[0] = vf_srcin ; if > 1, fills both in starting with 0th
    src_sel = [1,0]
    if nsrc eq 2 then src_sel[1] = 1
  endif else begin
    case 1 of
      vals.vf_circle: srcstr[*].srctype = 'circle'
      vals.vf_loop: srcstr[*].srctype = 'loop'
      else: srcstr[*].srctype = 'ellipse'
    endcase
    src_sel = vals.vf_multi ? [1,1] : [1,0]
  endelse

end

;-----
; hsi_vf_options is the main routine that presents the vf options widget

function hsi_vf_options, control, group=group, obj=obj

  if xregistered('hsi_vf_options') then return, 1 ; if exists, will pop it to foreground and return

  red_bmp = bytarr(7,7,3)  &  red_bmp[1:*,1:*,0] = 240  ; red 7x7 bit map
  green_bmp = bytarr(7,7,3) & green_bmp[1:*,1:*,1] = 240 ; green 7x7 bitmap

  hsi_vf_getparams, control=control, vals, srcstr, src_sel

  vf_srcin = control.vf_srcin ; need this for storing original structure in state

  shapes = ['circle', 'ellipse', 'loop']

  parms = ['Flux, ph/cm2/s', $
    'X pos, arcsec', $
    'Y pos, arcsec', $
    'FWHM, arcsec', $
    'Eccentricity', $
    'Pos Angle, deg', $
    'Loop Angle, deg ']

  parm_strindex = [1,2,3,4,5,6,7]

  tlb = widget_mbase (group=group, $
    title='VIS FWDFIT algorithm options', $
    /base_align_center, $
    /column, $
    ypad=5, $
    space=5, $
    /frame);, $
  ;					/modal )

  w_main = widget_base (tlb, $
    /column, $
    /frame )

  tmp = widget_label(w_main, value='THIS IS NOW A NON-BLOCKING WIDGET. You may leave the widget open')
  tmp = widget_label(w_main, value='and click in the image widget to make the image, or use the command line, etc.')
  tmp = widget_label(w_main, value=' ')
  tmp = widget_label(w_main, value='Note that after changing a numeric value, you MUST hit ENTER')
  tmp = widget_label(w_main, value=' ')

  w_set = widget_base(w_main, /row, space=10)
  temp = widget_label(w_set, value='Set parameters to: ')
  temp = widget_button(w_set, value='Auto set', uvalue='autoset')
  temp = widget_button(w_set, value='Previous fit results', uvalue='setprev')
  w_gr = widget_button(w_set, value='Set graphically ->', /menu)
  tmp = widget_button(w_gr, value='using image flux tool', uvalue='setmark')
  tmp = widget_button(w_gr, value='clicking for X,Y position', uvalue='setpos')

  w_srcmain = widget_base (w_main, column=2, /frame)

  nparms = n_elements(parms)
  w_srcbase = lonarr(2)
  w_srcbut = lonarr(2)
  w_srclabel = lonarr(nparms,2)
  w_srcparm = lonarr(nparms,2)
  w_fitmask = lonarr(nparms,2)
  w_shape =lonarr(2)

  for is = 0,1 do begin
    prefix = 'src'+trim(is)+'_'
    w_srctop = widget_base (w_srcmain, /column, /frame)
    w_srcbut[is] = widget_button (w_srctop,value='Source '+trim(is+1)+' Options ->', /menu, /align_center)
    w_srcbase[is] = widget_base(w_srctop, /column)
    if is gt 0 then begin
      tmp = widget_button (w_srcbut[is], value='Enable', uvalue=prefix+'enable')
      tmp = widget_button (w_srcbut[is], value='Disable', uvalue=prefix+'disable')
    endif
    tmp = widget_button (w_srcbut[is], value='Auto set params', uvalue=prefix+'autoset')
    tmp = widget_button (w_srcbut[is], value='Set params to previous fit results', uvalue=prefix+'setprev')
    tmp = widget_button (w_srcbut[is], value='Set params graphically using image flux tool', uvalue=prefix+'setmark')
    tmp = widget_button (w_srcbut[is], value='Set X,Y position by clicking image', uvalue=prefix+'setpos')

    w_shape[is] = cw_bgroup (w_srcbase[is], shapes, /row, /exclusive, /return_index, uvalue=prefix+'shape', /no_release)
    base1 = widget_base(w_srcbase[is],column=3,/grid_layout)
    for i=0,nparms-1 do w_srclabel[i,is] = widget_label(base1, value=parms[i], /align_left)
    for i=0,nparms-1 do w_srcparm[i,is] = widget_text(base1, value='',uvalue='none', xsize=8, /editable)
    for i=0,nparms-1 do w_fitmask[i,is] = widget_button(base1, value=red_bmp, uvalue=0, /align_center, /frame)
  endfor


  w_optbase = widget_base (w_main, /row, space=10)

  w_optbase1 = widget_base (w_optbase, $
    /nonexclusive, $
    /column)

  w_show = widget_button (w_optbase1, $
    value='Show progress', uvalue='show' )

  w_nophase = widget_button (w_optbase1, $
    value='Force phases to zero', uvalue='nophase' )

  w_abs = widget_button (w_optbase1, $
    value='Use absolute values in fit', uvalue='abs' )

  w_noerr = widget_button (w_optbase1, $
    value='Ignore statistical errors', uvalue='noerr' )

  w_optbase2 = widget_base(w_optbase, /column)

  w_nvismin = cw_field (w_optbase2, $
    /string, $
    title='Min # visibilities required: ', $
    xsize=8, $
    value=' ', $
    uvalue='none', $
    /return_events )

  w_maxiter = cw_field (w_optbase2, $
    /string, $
    title='# Iterations: ', $
    xsize=8, $
    value=' ', $
    uvalue='none', $
    /return_events )

  w_button_base = widget_base (tlb, $
    /row, $
    space=20 )

  tmp = widget_button(w_button_base, value='Help', uvalue='help')

  tmp = widget_button(w_button_base, value='Refresh', uvalue='refresh')

  w_reset = widget_button(w_button_base, $
    value='Reset to Defaults', uvalue='reset' )

  w_cancel = widget_button(w_button_base, $
    value='Cancel', uvalue='cancel')

  tmp = widget_button(w_button_base, $
    value='Accept and Close', uvalue='accept_close')

  state = { $
    obj: obj, $
    vals: vals, $
    orig_vals: vals, $
    orig_vf_srcin: vf_srcin, $
    ptr: ptr_new(vals), $  ; just used to get values back here after destroy widget
    shapes: shapes, $
    ;	shape: shape, $
    parm_strindex: parm_strindex, $
    srcstr: srcstr, $
    src_sel: src_sel, $
    w_srcbase: w_srcbase, $
    w_shape: w_shape, $
    w_srclabel: w_srclabel, $
    w_srcparm: w_srcparm, $
    w_fitmask: w_fitmask, $
    red_bmp: red_bmp, $
    green_bmp: green_bmp, $
    w_nvismin:w_nvismin, $
    w_maxiter: w_maxiter, $
    w_show: w_show, $
    w_nophase: w_nophase, $
    w_abs: w_abs, $
    w_noerr: w_noerr }

  hsi_vf_options_widget_update, state

  if xalive(group) then begin
    widget_offset, group, xoffset, yoffset, newbase=tlb
    widget_control, tlb, xoffset=xoffset, yoffset=yoffset
  endif

  widget_control, tlb, /realize

  widget_control, tlb, set_uvalue=state

  xmanager, 'hsi_vf_options', tlb, /no_block

  ;vals = *state.ptr
  ;ptr_free, state.ptr
  ;
  ;return, vals
  return, 1
end