;+
; The Live Time field contains information about the current live time measurement. The live time
; measurement is made with a 9-bit counter counting at 1MHz (220 Hz), gated by the detector dead
; time (using the same scheme as the Monitor Rate live time measurement). A separate counter is
; maintained for each detector segment, and is read out with the associated segment’s events.
; Each counter is latched and reset every 1/2048th of a second, synchronous with the spacecraft
; 1Hz clock. If the previously latched value for a detector segment has not started to be read out
; before a new measurement is latched, the new measurement over-writes the previous
; measurement. Once a measurement has started being read out, it continues to completion. A
; new measurement may be latched while the previous is being read out without disturbing the
; readout (i.e. a measurement is either completely read out or not read out at all). The latched
; counter value is included in the Live Time field of the next 3 available detector events. The 9-bit
; counter value is broken into 3 3-bit pieces for transmission in the 3 LSB of the Live Time field of
; the 3 events. The 3 pieces are read out Most Significant Bits first. The MSB of each Live Time
; field contains a flag that is set to 1 for the first of the 3 readouts, and zero for the rest. If any
; additional events occur after all available live time measurements are read out and before the
; next time the counter is latched, the Live Time field shall contain the value 0000.;
; 23-sep-2015, ltlist is now in packet format, not streaming format, but the way the routine is
; written is indifferent to the format.  But anyway, now it is in the form of a replicated
; {hsi_livetime_word} structure.
; 1-feb-2017, removed constraint on n_elements of ut2_select which appeared to server no function any longer, ras
;-
pro hsi_mklivetime, $
  self, $
  ltlist, $
  time_binning, $
  time_offset, $
  livearr, $
  samples, $
  ltlist_last_seg ;last livetime time and counter value for each segment
  novalue = -1e-8
  cnovalue = -1.0
  ltlist_last_seg = is_struct( ltlist_last_seg ) ? ltlist_last_seg : replicate( { time: novalue, counter: cnovalue}, 18)

  is_binned_eventlist = 'POINTER' eq Size( livearr, /tname )


  a2d_index_mask = Self->Get(/A2D_INDEX_MASK)
  these_a2d = Where( A2D_INDEX_MASK , na2d )

  these_seg = hsi_a2d2seg( these_a2d )
  nseg = n_elements( these_live)

  lt_ut_ref = hsi_sctime2any( Self->Get(/lt_ut_ref))
  ;Compute the difference between the livetime reference time
  ;and the packet buffer reference time
  ;time_offset is computed wrt to the packet buffer reference time UT_REF
  lt_time_unit = 512 ;default for livetime time


  nkeep = 0

  constant = hessi_constant()

  ;Prevent empty ltlist crash
  if size(/tname, ltlist) eq 'STRUCT' then BEGIN

    ; acs changed the place of this it was too early
    nselect = n_elements(ltlist.time)

    if chktag(ltlist, 'seg_index') then begin

      seg_index = ltlist.seg_index
      utaxis = Self->GetAxis( /ut, /edges_1 ) ;it's a 9 element pointer for binned_eventlist
      ord    = sort( ltlist.time )
      ltlist = n_elements(ltlist) ne nselect ? reform_struct( ltlist ) : ltlist
      livestr = ltlist[ ord ]

      s = histogram( livestr.seg_index, min=0, max=17, rev=rev)
      seg_sel = where( s < 1 )
      w = where_arr( seg_sel, these_seg, nsel )
      seg_sel = seg_sel[w]
      for i=0, nsel-1 do begin

        this_seg = seg_sel[i]
        ;if this_seg eq 9 then stop
        sel      = reverseindices( rev, this_seg )
        livestr_i = livestr[sel]
        use_seg = 1
        ut2 = get_edges(/edges_2, ( is_binned_eventlist ? *utaxis[this_seg mod 9] : utaxis )  )
        If Is_binned_eventlist then use_seg = ptr_valid( livearr[this_seg] )
        If use_seg then begin
          livetime    = is_binned_eventlist ? *livearr[this_seg] : livearr[*, this_seg]
          ;sample_seg  = is_binned_eventlist ? *samples[this_seg] : samples[*, this_seg] ;ras, removed 10-feb-2017
          ;        last_sample = last_item( where( sample_seg ) ) > 0
          ;        last_sample = last_sample eq 0 ? -1 : last_sample
          nbin        = n_elements(livetime)

          this_time_spec = livestr_i.time / 2048d0 + (1.0/4096d0 + lt_ut_ref ) ;
          ntime_spec = n_elements( this_time_spec )
          if 1  && ntime_spec ge 2 && ( this_time_spec[ntime_spec-1] - this_time_spec[ntime_spec-2] ) gt 2. then begin  ;&& (last_sample + 1 le (nbin - 1) ) then begin
            ;Remove the last livetime if more than 2 sec delayed which is sometimes anomalous
            livestr_i = livestr_i[0:ntime_spec-2]
            this_time_spec = this_time_spec[0:ntime_spec-2]
            remove_i = append_arr(remove_i, sel[ ntime_spec-1])

          endif

          if n_elements( this_time_spec ) ge 2 then begin  ;&& (last_sample + 1 le (nbin - 1) ) then begin
            counter        = float( livestr_i.counter ) ;
            select_time = value_locate(  get_edges(ut2, /edges_1), this_time_spec[ [0, n_elements(this_time_spec) -1]] )>0
            select_time <= n_elements( ut2 )/2 -1
            ut2_select = ut2[*, select_time[0]: select_time[1]]
            if n_elements( ut2_select ) gt 0 then begin ;changed from gt 2, ras, 1-feb-2017

              ;
              ; Convert the counter value into actual livetime using David Smith's function
              ; Then, average over the livetime samples in each interval using the total(/cumul) function
              ;
              ;
              gcounter = where( counter lt 0, ngc) ;gap identified, counter is abs 0, gap gaps and ramp gaps both

              counter = hsi_corrected_livetime(  counter>0, this_seg,/raw)
              zerothresh = hsi_corrected_livetime(  0, this_seg,/raw)
              if ngc gt 0 then counter[gcounter] = 0.0
              ;livetime = interpol(  counter, this_time_spec, avg( ut2, 0) )
              ;ssw_rebinner, specin, edgesin, specout, edgesout, dbl=dbl
              ;time_spec2 = transpose([[this_time_spec - 1./4096.d0], [this_time_spec + 1./4096]])
              ;ssw_rebinner, counter, time_spec2, livetime, utmean_select
              select = lindgen( select_time[1]-select_time[0] +1) + select_time[0]
              csamples = is_binned_eventlist ? (*samples[this_seg])[select] : $
                samples[ select, this_seg ]
              clive    = is_binned_eventlist ? (*livearr[this_seg])[select] : $
                livearr[ select, this_seg ]
              use_these = where( csamples lt 1, nuse_these ) ;if there is a non-zero value that means utl up that point shouldn't be used
              csamples = csamples[use_these[0]]
              clive    = clive[use_these[0]]
              first = use_these[0]
              utc = this_time_spec - this_time_spec[0]
              mmutc = minmax( utc )
              utl = ut2_select[*,use_these] - this_time_spec[0]
              utl0 = utl[0,0] ;save this because it may change in the next line
              
              full_width = get_edges(/width, [[utl[*,0]],[utl[*, nuse_these-1]]] )
              utl[0, 0] += csamples * full_width[0]  ;new first bin leading edge
              utl[1, 0]  <= mmutc[1]
              utl[1, nuse_these-1] <= mmutc[1]
              fracs = get_edges(/width, [[utl[*,0]],[utl[*, nuse_these-1]]] ) / full_width
              
              
              
              ;can't just test for counter gt 0, sometimes that's real data
              gt0 = where( counter gt (zerothresh + .001), ngt0)
              ;livetime = ( ngt0 gt 2 ? interp2sum( utl, utc[gt0], counter[gt0] ): interp2sum( utl, utc, counter ) )< 1 >0
              ;now we include 0s from gaps in the counter values
              ;remove all values before ltlist_last_seg

              livetime = interp2sum( utl<last_item(utc), utc, counter )< 1 >0


              ;Care must be taken to only write into livearr the values of the interpolation
              ;covered by the current this_time_spect
              
              ;0's are where coverage isn't complete
              total_first   = csamples + fracs[0]
              live_first    = ((clive[0] * csamples[0]) + livetime[0] * fracs[0] ) / total_first
              livetime[0]   = live_first
              these_sample_fractions = 1.0 + livetime * 0.0
              these_sample_fractions[0] = total_first
              if nuse_these ge 2 then these_sample_fractions[nuse_these-1] = fracs[1]
              use_these = select[use_these]

              If is_binned_eventlist then (*livearr[this_seg])[use_these] = livetime <1 > 0 else $
                livearr[ use_these, this_seg ]= livetime<1 > 0
              If is_binned_eventlist then (*samples[this_seg])[use_these] = these_sample_fractions<1 > 0 else $
                samples[ use_these, this_seg ] = these_sample_fractions<1 > 0
              ;Samples is used to keep track of the fraction of the bin spanned by livetime samples
              ;samples = (samples<1)>0 
              ;livearr = (livearr<1)>0
              ;ltlist_last_seg[ this_seg ].time = last_item( this_time_spec )   ;abs midpoint of last datum for this_seg
              ;ltlist_last_seg[ this_seg ].counter = last_item( counter )
            endif
          endif
        endif
      endfor
      if keyword_set( remove_i ) then remove, ord[remove_i], ltlist

      ;Time_cumul_seg is an array which keeps the total cumulative number of time bins as a function
      ;of the seg_index.  This is needed for the final mapping between the tuples for
      ; (seg_index, time, channel) and the livearr  index.
    ENDIF
  ENDIF


end