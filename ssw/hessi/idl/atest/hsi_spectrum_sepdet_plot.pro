;+
; Name: hsi_spectrum_sepdet_plot
;
; Purpose: Plot the separate-detector spectrum plots shown in Browser, both the flare plots, and the minute plots
;
; Input Keywords:
;   struct - Structure containing data for plot
;     {time_range: dblarr(2), $
;     spec: fltarr(nen,9), $  ; flux spectrum for 8 dets
;     emax: emax, $ ; max energy for plot
;     atten: atten, $ ; value of attenuator at this time
;     decim: decim, $ ; value of decim weight at this time
;     enmid: enmid}   ; energy mid points in keV
;   out_dir - output directory for plots
;   screen - if set, plot on screen
;   ps - if set, plot in PS file
;   png - if set, plot in PNG file
;   gen_by - text containing name of generating program (to write at bottom of plot)
;
; Output Keywords:
;   out_file - name of plot file generated
;
; Written: Kim Tolbert, Aug 2016
; Modifications:
;-

pro hsi_spectrum_sepdet_plot, struct=s, out_dir=out_dir, $
  screen=screen, ps=ps, png=png, gen_by=gen_by, out_file=out_file

  checkvar, gen_by, ''
  checkvar, screen, 1
  checkvar, ps, 0

  if ps or png then screen = 0
  if screen+ps+png eq 0 then screen = 1

  photon_plot = tag_exist(s,'photon_spec')

  col = indgen(9)+1
  col[0]=0

  if ps or png then begin
    save_dev = !d.name
    file_base = s.file_base
  endif

  case 1 of
    screen: begin
      !p.background = 1
      hsi_linecolors, /pastel
      tvlct, rr, gg, bb, /get
      rr[255]=0  &  gg[255]=0  &  bb[255]=0
      tvlct, rr, gg, bb
      !p.thick = 3
      charsize=1.3
    end
    ps: begin
      out_file = concat_dir(out_dir, file_base + '.ps')
      ps, out_file, /land, /color
      hsi_linecolors, /pastel
      !p.thick = 8
      !x.thick=2
      !y.thick=2
      !p.font=0
      charsize=1.2
    end
    png: begin
      out_file = concat_dir(out_dir, file_base + '.png')
      set_plot,'z'
      device, set_resolution = [640,600]
      hsi_linecolors, /pastel
      tvlct, rr, gg, bb, /get
      rr[255]=0  &  gg[255]=0  &  bb[255]=0
      tvlct, rr, gg, bb
      !p.background = 1 ; white in hsi_linecolors
      !p.thick = 3
      charsize=.9
    end
  endcase

  title = 'Count Spectra for RHESSI Front Segments'
  if photon_plot then begin
    title = str_replace(title, 'Count', 'Count and Photon')
    !p.multi=[0,1,2]
  endif

  xrange = [1., s.emax]

  ; To determine yrange for count plot, look at values greater than 0. (for simplicity set 0s to NaNs), and
  ; look at data values within energy plot range.
  qnan = where(s.spec le 0., knan)
  if knan gt 0 then s.spec[qnan] = !values.f_nan
  qen = where(s.enmid gt 1. and s.enmid lt s.emax)
  yrange = minmax(s.spec[qen,*], /nan) > 1.e-3

  atten = fix(s.atten) ; was byte - makes unprintable chars
  atten_label = atten eq -99 ? 'Att CHANGING' : 'Att A' + trim(atten)
  atten_energy = atten eq 0 ? 3. : 6.

  decim_label = 'FDecim ' + strcompress(arr2str(s.decim,','), /remove_all)

  if photon_plot then begin
    xtickname=strarr(30)+' '
    ymargin=[0,2]
    xtitle=''
  endif else begin
    xtitle='Energy (keV)'
  endelse

  ytitle='counts cm!u-2!n s!u-1!n keV!u-1!n'

  plot, xrange, yrange, /nodata, /xlog, /ylog, /xst, charsize=charsize, ymargin=ymargin, $
    title=title, xtitle=xtitle, ytitle=ytitle, xtickname=xtickname, ytickformat='tick_label_exp'

  for idet=0,8 do begin
    oplot, s.enmid, s.spec[*,idet], psym=10, col=col[idet]
  endfor
  oplot, [atten_energy,atten_energy], crange('y'), thick=1
  color_box, x=[1.,atten_energy],/line_fill, orient=45, spacing=1.5, thick=1
  yr = !y.crange  ; returns log of plotted y axis limits
  ypos = 10.^(yr[0] + (yr[1]-yr[0])/13.)
  xpos = atten_energy + (atten_energy lt 10 ? .3 : 3)
  xyouts, xpos, ypos, 'Includes Background!C' + atten_label + ', ' + decim_label, align=0, /data, charsize=charsize
  if atten eq -99 then xyouts,1.1,.005,'ATTENUATOR CHANGING', /data, col=6, charsize=1.4*charsize, charthick=2.
  start_date = anytim(s.time_range[0], /date, /vms)
  times = strmid(format_intervals(s.time_range, /ut, /truncate), 12, 99)
  ssw_legend, [start_date, times, trim(indgen(9)+1)+'F'], linestyle=[-99,-99,intarr(9)], $
    color=[0,0,col], /right, box=0, charsize=.9*charsize, pspacing=1


  if photon_plot then begin
    qnan = where(s.photon_spec le 0., knan)
    if knan gt 0 then s.photon_spec[qnan] = !values.f_nan
    qen = where(s.enmid gt atten_energy and s.enmid lt 100.)
    yrange = minmax(s.photon_spec[qen,*], /nan) > 1.e-3
    yrange[1] = 10.*yrange[1]
    ytitle='photons cm!u-2!n s!u-1!n keV!u-1!n'
    xtitle='Energy (keV)'
    plot, xrange, yrange, /nodata, /xlog, /ylog, /xst, charsize=charsize, ymargin=[4,0], $
      title='', xtitle=xtitle, ytitle=ytitle, ytickformat='tick_label_exp'

    for idet=0,8 do begin
      oplot, s.enmid, s.photon_spec[*,idet], psym=10, col=col[idet]
    endfor
    oplot, [atten_energy,atten_energy], crange('y'), thick=1
    color_box, x=[1.,atten_energy],/line_fill, orient=45, spacing=1.5, thick=1
    xpos = atten_energy + (atten_energy lt 10 ? .3 : 3)
    yr = !y.crange  ; returns log of plotted y axis limits
    ypos = 10.^(yr[0] + (yr[1]-yr[0])/25.)
    xyouts, xpos, ypos, 'Semi-calibrated', align=0, /data, charsize=charsize
  endif

  if gen_by ne '' then xyouts, 0., .01, 'Generated by ' + gen_by, charsize=.9*charsize, /norm, align=0.
  timestamp, /bottom, charsize=.9*charsize

  case 1 of
    ps: psclose
    png: write_png, out_file, tvrd(), rr,gg,bb
    else:
  endcase

  if ps or png then message, /cont, 'Wrote plot file ' + out_file
  cleanplot, /silent
  if ps or png then set_plot, save_dev

end