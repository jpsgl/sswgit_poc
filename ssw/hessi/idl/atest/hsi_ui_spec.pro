;+
; Name: hsi_ui_spec
;
; Purpose: Widget interface to HESSI spectrum object
;
; Written: Kim Tolbert, 26-Mar-2001
; Modifications:
;   8-May-2001, Kim.  Use getaxis method instead of getdata for axis information
;   9-Sep-2001, make w_sum a keyword in call to hsi_coll_list_widget
;   24-Jul-2002, Kim.  Don't set time_range to absolute time interval anymore. (Needed to earlier.)
;      Also, use path='HSI_SPEC' when looking for energy_binning.txt file for speed.
;   30-Sep-2002, Kim.  In reset_times event, check if flarenumber changed, not whole structure
;      (match_struct returns 0 if there are Nan's in structure on Unix)
;   11-Oct-2002, Kim.  Added spectrogram plot option
;   15-Oct-2002, Kim.  Save energy and time intervals used and don't format them and set droplist
;     widget values unless they've changed.  Also use maxint=500 keyword on format_intervals in
;     case lists are really long.  Also realize widget before calling hsi_ui_spec_widget_update so
;     that list widgets will be wider (set by 'xxx...' string')
;   15-Oct-2002, Kim.  Added Change button for Obs Time Interval
;       7-Nov-2002, Kim,  Enabled energy binning codes 16 and 17
;   18-Nov-2002, Kim.  Use * to indicate parameters that require reprocessing.
;   5-Feb-2003, Kim.  Added options for plotting Counts, Rate or Flux.  When semical is
;     selected, changes widget labels to photons, ...
;   16-Feb-2003, Kim.  Allow detectors to be summed or not (previously desensitized sum button)
;   19-Feb-2003, Kim.  Added option to plot by channel instead of energy.
;   19-Feb-2003, Kim.  Use c_tags from xstruct, and only set params that changed into object.
;   25-Feb-2003, Kim.  Added option to plot in keV when using channels for binning
;   02-Mar-2003, Kim.  Added /hessi, ch_energy arguments to hsi_sel_intervals call
;   19-Mar-2003, Kim.  hsi_sel_intervals renamed to xsel_intervals. -1 returned, means cancelled.
;   12-Sep-2003, Kim.  Added decimation, pileup correction options, and use_flare_xyoffset
;   12-Jan-2004, Kim.  Added 'Write Script' button
;   16-Mar-2004, Kim.  xsel_intervals now returns -99 if cancelled.  So check for int[0] > -1.
;   25-May-2004, Kim,  Enabled energy binning code 18
;   09-Aug-2004, Sandhia,  Put the code for "manoptions" in a separate function xset_struct so that
;                              it can be used by other programs as well.
;   17-Jan-2005, Kim,  Enabled energy binning code 19,20
;	1-Mar-2005,  Kim.  get hessi_obs_obj from common now, not from state (uvalue) of hessi widget.
;	27-Apr-2005, Kim.  added buttons to plot livetime (used and ctr)
;   29-Apr-2005, Kim.  get(/info) in getparams isn't needed (I think) and was slow in some cases
;     so just set info to 0.
;	13-May-2005, Kim - Add rear_decimation_correct option
;	27-May-2005, Kim - Added buttons to plot monitor livetime (and corrected).  Also realized
;	  I need to clone flags_obj before sending it to plotman.
;	9-Jun-2005, Kim  - When plotting monitor rate livetimes, don't use segs from spectrum's
;	  seg_index_mask, plot segment 5 always, but have other segments available through
;	  plotman's xy plot options button (Richard's request)
;	12-Dec-2005, Kim - use full parameter names (for det_index_mask and seg_index_mask)
;	7-Mar-2006, Kim - enable energy binning code 21
;	28-Apr-2006, Kim - enable energy binning code 22
;	4-Oct-2007, Kim - call spectrum plot with legend_loc=2 (upper right) by default
; 04-Aug-2009, Kim. Added 'Plot Goes' button.
; 10-Aug-2009, Kim. Use {hsi_flarelistdata_ext} instead of hsi_flarelist_extend({hsi_flarelistdata}) -
;   another memory leak.  Added hsi_ui_spec_cleanup and set it in xmanager.
; 08-Sep-2009, Kim. Add sum_coincidence to list of params for script
; 15-Sep-2009, Kim Total # binning codes (for dropdown list ) is now 24
; 02-Nov-2010, Kim. Total # binning codes (for dropdown list ) is now 27
; 06-Dec-2010, Kim. Fixed bug - wasn't cloning flags_obj before plotting monitor livetimes
; 17-Jan-2011, Kim. Total # binning codes (for dropdown list ) is now 28 (added BC27 for Gerry)
; 18-Jan-2011, Kim. Added option under File write to write separate detector files with native bins.
;   Also call hsi_coll_list_widget_set (new) instead of setting coll list widget directly here.
;   And made widget scroll if on a small screen.
; 19-Jan-2011, Kim.  Undid scroll for small screen feature!  Doesn't work on UNIX!  (comes up really teeny)
; 21-Jan-2011, Kim. Redid scroll so it works on unix as well as windows.  Have to use size of child of tlb
;   which must contain full widget, since tlb with scroll is teeny on unix, to determine if ysize is too big.
;   Call widget_limit_ysize after populating widget to limit ysize.
; 12-Mar-2011, Kim. Added BC 28
; 18-Sep-2014, Kim. Added BC 29
; 10-Mar-2016, Kim. Added button for writing separate-detector spectrum files with non-native energy bins (previously was
;   only for native bins).  Also made setting times easier - previously required that spectrum time interval be
;   within obs time interval, but that isn't necessary. (removed valid_range keyword on call to hsi_time_widget, and removed
;   extra label that warned that sp time must be within obs time)
; 16-Mar-2017, Kim. Previously was setting time_range relative to GUI's obs time interval to set sp_time_interval.
;   Don't use time_range, just set sp_time_interval. 
;   Richard changed spectrum obj so that obs_time_int is set to minmax of sp_time_int, so
;   that obs_time is no longer same as what I was calling obs time in GUI - now calling that Working Time Interval.
;   If sp_time_interval is scalar, it divides obs_time into bins of that size. Now sp_time_interval can
;   be outside of the 'working' time. But it change 'working' time, then sp_time_int will be  to new time and binsize=4.
;   Also moved time binning widgets up to same area as spectrum time range.
; 17-Mar-2017, Kim. Temporary change in setting sp_time_int until Richard fixes something in 
;   the spectrum object - want to preserve bin size when change time - spectrum obj will do this soon.
; 05-Jul-2018, Kim. Call hsi_params2script without hard-coded list of params_used. Now it's
;   smarter and figures out which params to write.
;
;-

pro hsi_ui_spec_getparams, obj, params

  ;print,obj -> get(/det_index)
  ;print,obj -> get(/seg_index)

  ; if det_index_mask is set, then use it and set both segments for any selected colls on
  ; Otherwise use seg_index_mask, but if seg_index_mask is all zeros, then set all det/segs on.
  det = obj -> get(/det_index_mask)
  if total(det) ne 0 then obj -> set, seg_index_mask = det # [1,1] else begin
    seg = obj -> get(/seg_index_mask)
    if total(seg) eq 0 then obj -> set, seg_index_mask=intarr(18)+1
  endelse

  control = obj -> get (/control_only)
  ;info = obj -> get(/info)

  ;control.time_range = control.obs_time_interval

  ; kim, 12-sep-2003
  ; add xyoffset to params structure for now because getxyoffset() gets what will be used
  ; but get(/xyoffset) doesn't yet (in image object doing get(xyoffset) sets xyoffset params,
  ; so then xyoffset will be set in control struct)
  xyoffset = obj -> getxyoffset()
  params = { $
    xyoffset: xyoffset, $
    control: control, $
    info: 0}

end

;-----

pro hsi_ui_spec_widget_update, state

  params = state.params
  w = state.widgets

  widget_control, w.w_obs_time_label0, set_value='* ' + state.obs_time_label[0]
  widget_control, w.w_obs_time_label1, set_value=state.obs_time_label[1]

  stime = anytim(hsi_get_time_range (params.control, params.info), /vms)
  value = '* Spectrum Time Range: ' + stime[0] + '  -  ' + stime[1]
  widget_control, w.w_time, set_value=value

  chan = params.control.sp_chan_binning ne 0

  widget_control, w.w_energy_base, sensitive=(chan eq 0)
  widget_control, w.w_chan_base2, sensitive=chan
  widget_control, w.w_semical, sensitive=(chan eq 0)
  widget_control, w.w_flux, sensitive=(chan eq 0)
  widget_control, w.w_use_chan, set_button=chan
  widget_control, w.w_write, sensitive=(chan eq 0)
  if chan then begin
    widget_control, w.w_chan_bin, set_value=params.control.sp_chan_binning
    widget_control, w.w_chan_min, set_value=params.control.sp_chan_min
    widget_control, w.w_chan_max, set_value=params.control.sp_chan_max
  endif else begin
    energy_intervals_new = state.data_obj -> getaxis(/xaxis, /edges_2)
    if not same_data(energy_intervals_new, *state.energy_intervals) then begin
      widget_control,/hourglass
      *state.energy_intervals = energy_intervals_new
      value = format_intervals (energy_intervals_new,  format='(f9.1)', maxint=500)
      widget_control, w.w_energies, set_value=' ' + value + ' '
      widget_control, w.w_energies, set_droplist_select=0
      widget_control, w.w_nenergy, set_value='# Energy Bins: ' + strtrim(n_elements(energy_intervals_new[0,*]), 2)
    endif

    ecode = state.data_obj -> get(/sp_energy_binning)
    if n_elements(ecode) gt 1 then ecode = 0 else ecode = ecode + 1
    widget_control, w.w_ecode, set_droplist_select=ecode
  endelse

  time_intervals_new = state.data_obj -> getaxis(/yaxis, /edges_2)
  if not same_data(time_intervals_new, *state.time_intervals) then begin
    widget_control,/hourglass
    *state.time_intervals = time_intervals_new
    value = format_intervals (time_intervals_new,  /ut, /vms, maxint=500)
    widget_control, w.w_times, set_value=value
    widget_control, w.w_times, set_droplist_select=0
    widget_control, w.w_ntimebin, set_value='# Time Bins:' + strtrim(n_elements(time_intervals_new[0,*]), 2)

    value = state.data_obj -> get(/sp_time_interval)
    timebin = -1
    if n_elements(value) eq 1 then timebin = value else begin
      vals = time_intervals_new
      widths = reform(vals[1,*] - vals[0,*])
      q = where ((widths - widths[0]) gt .00001, count)
      if count eq 0 then timebin = widths[0]
    endelse
    stimebin = timebin eq -1 ? 'Various' : strtrim(string(timebin,format='(f12.3)'),2)
    widget_control, w.w_timebinw, set_value=stimebin
  endif

  hsi_coll_list_widget_set, w.w_colldisp, params.control
  ;segs = reform(params.control.seg_index_mask, 9, 2)
  ;colls = total (segs, 2) gt 0
  ;
  ;for ic = 0,8 do begin
  ;    widget_control, w.w_colldisp[ic,0], sensitive=colls[ic]
  ;    widget_control, w.w_colldisp[ic,1], sensitive=segs[ic,0]
  ;    widget_control, w.w_colldisp[ic,2], sensitive=segs[ic,1]
  ;endfor

  enab = ['Disabled (single selected time interval (spectrum plot) or energy band (time plot) will be shown.)', 'Enabled']
  widget_control, w.w_sum, set_value='Sum Detectors: ' + enab[params.control.sum_flag]

  widget_control, w.w_semical, set_button=params.control.sp_semi_calibrated

  widget_control, w.w_counts, get_value=c_or_p
  if params.control.sp_semi_calibrated then begin
    if c_or_p eq 'Counts' then begin
      widget_control, w.w_counts, set_value='Photons'
      widget_control, w.w_rate, set_value='Photon Rate'
      widget_control, w.w_flux, set_value='Photon Flux'
    endif
  endif else begin
    if c_or_p ne 'Counts' then begin
      widget_control, w.w_counts, set_value='Counts'
      widget_control, w.w_rate, set_value='Count Rate'
      widget_control, w.w_flux, set_value='Count Flux'
    endif
  endelse

  ; include this here even though normally not necessary for excl buttons, because
  ; user might change value from command line, and want to new true value here
  units = params.control.sp_data_unit
  widget_control, w.w_counts, set_button=(units eq 'Counts')
  widget_control, w.w_rate, set_button=(units eq 'Rate')
  widget_control, w.w_flux, set_button=(units eq 'Flux')

  sel = where (state.flags_obj -> get(/selected) eq 1, count)
  codes = state.flags_obj -> get(/codes)
  val = count gt 0 ? arr2str(codes[sel],', ') : 'None'
  widget_control, w.w_flags, set_value='Show Flags: ' + val

  decim = 'None'
  if params.control.decimation_correct then decim = 'Front'
  if params.control.rear_decimation_correct then decim = (decim eq 'None')  ? 'Rear' : decim + ', Rear'
  val = 'Pileup Correction: ' + (params.control.pileup_correct eq 1 ? 'Enabled' : 'Disabled') + $
    '              Decim Correction: ' + decim

  widget_control, w.w_corr1, set_value=val
  val = 'Use Flare XY Offset: ' + (params.control.use_flare_xyoffset eq 1 ? 'Enabled' : 'Disabled') + $
    '          XY Offset: ' + arr2str(trim(params.xyoffset, '(2f7.2)'), ', ')
  widget_control, w.w_corr2, set_value=val


end

;-----

pro hsi_ui_spec_event, event

  @hessi_obj_common

  widget_control, event.top, get_uvalue=state

  ; check if user clicked X in top right corner of window, and if so quit

  if tag_names(event,/struc) eq 'WIDGET_KILL_REQUEST' then goto, exit

  if tag_names(event,/struc) ne 'WIDGET_TIMER' then begin
    ;in case user changed any parameters from command line, make sure we're up to date
    hsi_ui_spec_getparams, state.data_obj, params
    state = rep_tag_value (state, params, 'params')
    ; clear any error messages in message widget
    if xalive(state.w_message) then widget_control, state.w_message, set_value=''
  endif

  widget_control, event.id, get_uvalue=uvalue

  _extra = -1
  chg_msg = ''

  ; for any of the plot commands figure out what we're plotting
  if strpos(uvalue, 'plot') ne -1 then begin
    what = state.params.control.sp_semi_calibrated ? 'Photon' : 'Count'
    case state.params.control.sp_data_unit of
      'Counts': what = what + 's'
      'Rate': what = what + ' Rate'
      'Flux': what = what + ' Flux'
    endcase

    ; temporarily set sp_data_struct - plot routine expects data in structure. For obj
    ; input, that's fine, because it sets sp_data_struct.  But in plotman, plotman does
    ; getdata before calling hsi_spectrum::plot and in plot, saved_data is used, so
    ; data must be a structure in saved_data.
    sp_data_struct_save = state.data_obj -> get(/sp_data_struct)
    state.data_obj -> set, /sp_data_struct

    ; get selected time and energy bin from widget in case we're doing a sep det plot
    ; and can only plot one time or energy bin.
    sepdet_tbin = widget_info(state.widgets.w_times, /droplist_select)
    sepdet_ebin = widget_info(state.widgets.w_energies, /droplist_select)
  endif

  case uvalue of

    'reset_times': begin
      widget_control,   state.widgets.w_obs_time_label0, timer = 3.
      if same_data(state.obs_time_interval, *state.ptr_obs_time_interval) and $
        same_data (state.flare_str.id_number, (*state.ptr_flare_str).id_number) then return
      obs = *state.ptr_obs_time_interval
      state.data_obj -> set, obs_time_interval = obs
      ;state.data_obj -> set, time_range = obs - obs[0]
      state.obs_time_interval = obs
      state.flare_str = *state.ptr_flare_str
      state.obs_time_label = hsi_ui_time_label(state.obs_time_interval, state.flare_str, /twolines)
      hsi_ui_spec_getparams, state.data_obj, params
      state = rep_tag_value (state, params, 'params')
      hsi_ui_spec_widget_update, state
    end

    'change_obs': begin
      top_base = state.plotman_obj->get(/plot_base)
      widget_control, top_base, get_uvalue=top_state
      widget_event = {id:top_state.w_labels, top: top_base, handler: 0L, $
        value:[0.d,0.d], flare_str:{hsi_flarelistdata_ext} }
      hsi_ui_obs, hessi_obs_obj, group=top_base, $
        obs_time_interval=top_state.obs_time_interval, widget_event=widget_event
    end

    'manoptions': begin
      ;answer = dialog_message (['Caution!  This option is for experienced users only.', $
      ;  'Setting parameters wrong could make the object unusable.', $
      ;  '', 'Do you want to continue?'], $
      ;  /question, title='Warning!')
      ;if strlowcase(answer) eq 'yes' then begin
      ;  widget_control, /hourglass
      ;  control = state.params.control
      ;  xstruct, control, $
      ;       /edit, $
      ;       group=event.top, $
      ;       title='Spectrum Control Parameters', $
      ;       nx=5, xsize=10, /center, $
      ;       /modal, $
      ;       status=status, c_tags=c_tags
      ;  if status and c_tags[0] ne -1 then _extra=str_subset(control, (tag_names(control))[c_tags])
      ;endif


      group = event.top
      control = state.params.control
      status = xset_struct("Spectrum", group, control, _extra)
    end

    'script': begin
      all = widget_info(event.id, /uname) eq 'all'
      out = hsi_params2script (state.data_obj, all=all)
    end

    'defstandard': begin
      widget_control, /hourglass
      temp_obj = obj_new('hsi_spectrum')
      _extra = temp_obj -> get(/control)
      ; don't set times to default. Set to times when we entered spectrum widget.
      if tag_exist(_extra,'obs_time_interval') then begin
        obs = state.params.control.obs_time_interval
        _extra.obs_time_interval = obs
        ;_extra.time_range = obs - obs[0]
      endif
      obj_destroy, temp_obj
    end

    'changetime': begin
      new_vals = hsi_time_widget (event.top, hsi_get_time_range(state.params.control, state.params.info), $
        chg_msg=chg_msg)
      ; Next 5 lines are temporary. Richard will change the obj to handle this, then
      ; will only need to set obs_time (remove these 5 lines, and uncomment _extra line.
      ; Need this now, because setting obs_time doesn't always work now (checks for overlaps, etc)
      ; but when set sp_time, obs_time gets set same, but bin widths are wiped out.
      old_sp_time_int = state.data_obj->get(/sp_time_interval)
      state.data_obj -> set, sp_time_interval=new_vals
      if n_elements(old_sp_time_int) eq 1 then state.data_obj -> set, sp_time_interval=old_sp_time_int
      hsi_ui_spec_getparams, state.data_obj, params
      state = rep_tag_value (state, params, 'params')
        
;      _extra = {obs_time_interval: new_vals}
    end
    
    'timebin_width':  _extra = {sp_time_interval: event.value}
    
    'changetbins': begin
      obs = state.params.control.obs_time_interval
      new_vals = xsel_intervals (group=event.top, $
        /hessi, $
        input_intervals=state.data_obj -> getaxis(/yaxis, /edges_2), $
        /time, $
        msg_label = 'Choose one or multiple time intervals to create spectrum for.', $
        chg_msg=chg_msg, $
        plotman_obj=state.plotman_obj, $
        stored_intervals_obj=state.intervals_obj)
      if new_vals[0] gt -1 and not same_data(new_vals, state.data_obj -> getaxis(/yaxis, /edges_2)) then $
        _extra = {sp_time_interval: new_vals}
      a=xregistered('hsi_ui_spec')
    end

    'drawtbins': begin
      if state.plotman_obj->valid_window(/utplot, /message) then begin
        state.plotman_obj -> select
        state.plotman_obj -> plot
        plotman_draw_int, 'all', state, $
          intervals=state.data_obj -> getaxis(/yaxis, /edges_2)
      endif
    end

    'ecode': if event.index ne 0 then _extra = {sp_energy_binning: event.index - 1}

    'showecodes': begin
      ebfile = hsi_loc_file('energy_binning.txt', path='$HSI_SPEC', count=count)
      if count gt 0 then more, rd_ascii (ebfile[0])
    end

    'changeenergy': begin
      obs = state.params.control.obs_time_interval
      new_vals = xsel_intervals (group=event.top, $
        /hessi, ch_energy=hessi_constant(/ql_energy_bins), $
        input_intervals=state.data_obj -> getaxis(/xaxis, /edges_2), $
        /energy, $
        msg_label = 'Choose energy bands to create spectrum for.', $
        valid_range=[1.,100000.], obs_time_interval=obs, chg_msg=chg_msg, $
        plotman_obj=state.plotman_obj, $
        stored_intervals_obj=state.intervals_obj)
      if new_vals[0] gt -1 and not same_data(new_vals, state.data_obj -> getaxis(/xaxis, /edges_2)) then $
        _extra = {sp_energy_binning: new_vals}
      a=xregistered('hsi_ui_spec')

    end

    'drawebins': begin
      if state.plotman_obj->valid_window(/xyplot, /message) then begin
        state.plotman_obj -> select
        state.plotman_obj -> plot
        plotman_draw_int, 'all', state, $
          intervals=state.data_obj -> getaxis(/xaxis, /edges_2)
      endif
    end

    'use_chan': begin
      if event.select then begin
        _extra={sp_chan_binning: 1, sp_semi_calibrated: 0}
        if state.params.control.sp_data_unit eq 'Flux' then $
          _extra = add_tag (_extra, 'Counts', 'sp_data_unit')
      endif else _extra={sp_energy_binning: 7}
    end

    'chan_bin': _extra = {sp_chan_binning: event.value}

    'chan_min': _extra = {sp_chan_min: event.value}

    'chan_max': _extra = {sp_chan_max: event.value}

    'use_kev': state.use_kev = event.select
    
    'changecoll': begin
      new_vals = hsi_coll_widget (state.params, group=event.top, /notime, /noharm, chg_msg=chg_msg)
      _extra = new_vals
    end

    'semical': _extra = {sp_semi_calibrated: event.select}

    'counts': _extra = {sp_data_unit: 'Counts'}

    'rate': _extra = {sp_data_unit: 'Rate'}

    'flux': _extra = {sp_data_unit: 'Flux'}

    'change_flags':  state.flags_obj -> select_widget, group=event.top

    'changecorr': _extra = hsi_corr_widget (state.params, group=event.top)

    'doenergyplot': begin
      start = (anytim(hsi_get_time_range (state.params.control, state.params.info), /vms, /trunc))[0]
      hsi_do_plotman, state, tagname='data_obj', group=event.top, $
        plot_type='xyplot', desc='HESSI ' + what + ' vs Energy ' + start, /xlog, /ylog, /dim1_sum, $
        chan_in_kev=state.use_kev, sepdet_bin=sepdet_tbin, legend_loc=2
    end

    'dotimeplot': begin
      start = (anytim(hsi_get_time_range (state.params.control, state.params.info), /vms, /trunc))[0]
      hsi_do_plotman, state, tagname='data_obj', group=event.top, $
        plot_type='utplot', desc='HESSI ' + what + ' vs Time ' + start, /pl_time, /ylog, /dim1_sum, $
        show_flags_obj=obj_clone(state.flags_obj), sepdet_bin=sepdet_ebin
    end

    'dospecplot': begin
      start = (anytim(hsi_get_time_range (state.params.control, state.params.info), /vms, /trunc))[0]
      hsi_do_plotman, state, tagname='data_obj', group=event.top, $
        plot_type='specplot', desc='HESSI ' + what + ' Spectrogram ' + start, /pl_spec, /ylog, /dim1_sum, $
        show_flags_obj=obj_clone(state.flags_obj), sepdet_bin=sepdet_ebin
    end

    'doltime_used_plot': begin
      start = (anytim(hsi_get_time_range (state.params.control, state.params.info), /vms, /trunc))[0]
      hsi_do_plotman, state, tagname='data_obj', group=event.top, $
        plot_type='utplot', desc='HESSI Livetime Used ' + start, /pl_ltime_used, $
        show_flags_obj=obj_clone(state.flags_obj), sepdet_bin=sepdet_ebin
    end

    'doltime_ctr_plot': begin
      start = (anytim(hsi_get_time_range (state.params.control, state.params.info), /vms, /trunc))[0]
      hsi_do_plotman, state, tagname='data_obj', group=event.top, $
        plot_type='utplot', desc='HESSI Livetime Ctr ' + start, /pl_ltime_ctr, $
        show_flags_obj=obj_clone(state.flags_obj), sepdet_bin=sepdet_ebin
    end

    'doltime_mon_plot': begin
      time = minmax(state.data_obj->getaxis(/ut,/edges_2))
      ;		seg = state.data_obj -> get(/seg_index_mask)
      seg = 4
      hsi_plot_monitor_ltime, time, seg, $
        show_flags_obj=obj_clone(state.flags_obj), plotman_obj=state.plotman_obj
    end

    'doltime_corr_mon_plot': begin
      time = minmax(state.data_obj->getaxis(/ut,/edges_2))
      ;		seg = state.data_obj -> get(/seg_index_mask)
      seg = 4
      hsi_plot_monitor_ltime, time, seg, /corrected, $
        show_flags_obj=obj_clone(state.flags_obj), plotman_obj=state.plotman_obj
    end

    'plot_goes': goes_plot, time_range=minmax(state.data_obj->getaxis(/ut,/edges_2)), plotman_obj=state.plotman_obj

    'writefile': begin
      hsi_spectrum_filewrite_widget, state.data_obj, event.top, err_msg=chg_msg
    end

    'write_sepdet': begin
      hsi_spectrum_filewrite_sepdet_widget, state.data_obj, /not_native, state.plotman_obj, event.top, err_msg=err_msg
    end

    'write_sepdet_native': begin
      hsi_spectrum_filewrite_sepdet_widget, state.data_obj, state.plotman_obj, event.top, err_msg=err_msg
    end

    'stop':  begin
      obj = state.data_obj
      stop
    end

    'help': gui_help, 'gui_spectrum_help.htm'

    'close':  goto, exit

    else:
  endcase

  ;if we did a plot, restore value of sp_data_struct
  if exist(sp_data_struct_save) then state.data_obj -> set, sp_data_struct=sp_data_struct_save

  if chg_msg[0] ne '' then ok = dialog_message(chg_msg, title='Warning message')

  if size(_extra, /tname) eq 'STRUCT' then begin
    widget_control, /hourglass
    state.data_obj -> set, _extra=_extra
    hsi_ui_spec_getparams, state.data_obj, params
    ;   if tag_exist(_extra, 'this_seg') then begin
    ;     params.this_seg = intarr(27)-1
    ;     params.this_seg = _extra.this_seg
    ;   endif
    state = rep_tag_value (state, params, 'params')
    ;   print,'this_seg = ', params.this_seg
  endif

  if xalive(event.top) then begin
    hsi_ui_spec_widget_update, state
    widget_control, event.top, set_uvalue=state
  endif

  return

  exit:

  widget_control, event.top, /destroy
  return

end

;-------------------

pro hsi_ui_spec_cleanup, id

  widget_control, id, get_uvalue=state
  free_var, state, exclude=['plotman_obj', 'data_obj','ptr_obs_time_interval','ptr_flare_str']

end

;-------------------

pro hsi_ui_spec, input_obj, $
  obs_time_interval=ptr_obs_time_interval, $
  flare_str=ptr_flare_str, $
  intervals_obj=intervals_obj, $
  group=group, $
  error=error

  error = 0

  if xregistered ('hsi_ui_spec') then begin
    xshow,'hsi_ui_spec', /name
    return
  endif

  if keyword_set(ptr_obs_time_interval) then obs_time_interval = *ptr_obs_time_interval
  if keyword_set(ptr_flare_str) then flare_str = *ptr_flare_str

  if not hsi_chk_obj ( input_obj, 'HSI_SPECTRUM', data_obj, obs_time_interval=obs_time_interval) then return

  hsi_ui_spec_getparams, data_obj, params

  obs_time_label = hsi_ui_time_label(obs_time_interval, flare_str, /twolines)

  flags_obj = hsi_show_flags()

  space = 1
  ypad = 1
  xpad = 1

  hsi_ui_getfont, font, big_font

  widget_control, default_font = font

  ; if standalone, position at 50,50.  If not, after create base, offset it from parent
  ; before realizing it.
  xoffset = 50  & yoffset = 50

  ; Add /scroll so if, once widget is populated, if we have to limit y size, it will scroll.
  ; For this to work on unix, child of this widget must contain full widget (to determine space needed)
  w_top_base = widget_base(group=group, $
    title=hsi_widget_title('Spectra'), $
    xoffset=xoffset, $
    yoffset=yoffset, $
    ;mbar=mbar, $
    /tlb_kill, /scroll )

  w_base = widget_base( w_top_base, $
    /column, $
    /frame, $
    space=space, $
    xpad=xpad, $
    ypad=ypad )

  w = widget_label( w_base, $
    value='SPECTRA and LIGHTCURVES', $
    /align_center, $
    font=big_font )

  w = widget_label( w_base, $
    value="(* - changing these parameters forces reprocessing and takes longer)", $
    /align_center)

  w_obs = widget_base(w_base, /row, /frame)
  w_obs_time = widget_base(w_obs, /column)
  w_obs_time_label0 = widget_label(w_obs_time, $
    value='* ' + obs_time_label[0], uvalue='reset_times', /align_center)
  w_obs_time_label1 = widget_label(w_obs_time, $
    value=obs_time_label[1], uvalue='reset_times', /align_center)
  w_obs_change = widget_base(w_obs, /column)
  tmp = widget_button(w_obs_change, value='Change...', uvalue='change_obs')

  ;w = widget_label(w_base, value='Note:  Spectrum time interval must be within Observation Time Interval (above) ')

  w_time_base = widget_base(w_base, /column, /frame)
  w_sptime_base = widget_base(w_time_base, /row, space=20)
  w_time = widget_label(w_sptime_base, value='', /dynamic_resize)
  tmp = widget_button(w_sptime_base, value='Change...', uvalue='changetime')

  w_timebin_base = widget_base(w_time_base, /row, space=5)

  w_col1 = widget_base(w_timebin_base, /column, space=5)
  w_ntimebin = widget_label(w_col1, value='', /dynamic_resize)
  w_times = widget_droplist(w_col1, title='* Time Bins:', $
    value='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', uvalue='nothing')

  w_col2 = widget_base(w_timebin_base, /column, space=5)
  tmp = widget_label(w_col2, value='Time Bin Width (s): ', /align_center)
  w_timebinw = cw_edroplist( w_col2, $
    value=0., $
    format='(f12.3)', $
    xsize=7, $
    label='', $
    drop_values=[.1, .5, 1., 2., 4., 5., 10., 20., 30., 60., 120., 240.], $
    uvalue='timebin_width' , /align_center)

  w_col3 = widget_base(w_timebin_base, /column, space=5)
  tmp = widget_button(w_col3, value='Define Bins Manually...', uvalue='changetbins')
  tmp = widget_button(w_col3, value='Draw Current Bins', uvalue='drawtbins')


  w_energy_base = widget_base(w_base, /row, /frame, space=15)

  w_col1 = widget_base(w_energy_base, /column, space=5)
  w_nenergy = widget_label(w_col1, value='',  /dynamic_resize)
  w_energies = widget_droplist(w_col1, title='* Energy Bins (keV): ', $
    value='xxxxxxxxxxxxxxxxxxxxx', uvalue='nothing')

  w_col2 = widget_base(w_energy_base, /column, space=5)
  w_ecode = widget_droplist( w_col2, value=['None',strtrim(indgen(30),2)], $
    title='Binning Code: ', uvalue='ecode' )
  w_showecodes = widget_button(w_col2, value='Show Binning Codes', uvalue='showecodes')

  w_col3 = widget_base(w_energy_base, /column, space=5)
  tmp = widget_button(w_col3, value='Define Bins Manually...', uvalue='changeenergy')
  tmp = widget_button(w_col3, value='Draw Current Bins', uvalue='drawebins')

  w_chan_base = widget_base(w_base, /row, /frame, space=2)
  w_use_chan_base = widget_base(w_chan_base, /row, /nonexclusive)
  w_use_chan = widget_button(w_use_chan_base, value='* Use Channels', uvalue='use_chan')
  w_chan_base2 = widget_base(w_chan_base, /row, space=2)
  w_chan_bin = cw_edroplist( w_chan_base2, $
    value=0., $
    format='(i5)', $
    xsize=5, $
    label='Bin Width: ', $
    drop_values=[1,2,4,8,10,100,1000], $
    uvalue='chan_bin')
  w_chan_min = cw_edroplist( w_chan_base2, $
    value=0., $
    format='(i5)', $
    xsize=5, $
    label='Channel Min: ', $
    drop_values=indgen(9)*1000, $
    uvalue='chan_min')
  w_chan_max = cw_edroplist( w_chan_base2, $
    value=0., $
    format='(i5)', $
    xsize=5, $
    label='Channel Max: ', $
    drop_values=[1000,2000,3000,4000,5000,6000,7000,8000,8192], $
    uvalue='chan_max')
  w_use_kev_base = widget_base(w_chan_base2, /row, /nonexclusive)
  w_use_kev = widget_button(w_use_kev_base, value='Plot keV', uvalue='use_kev')

  hsi_coll_list_widget, w_base, w_colldisp, w_sum=w_sum

  w_su_base = widget_base(w_base, /row, /frame, space=5)
  w_semical_base = widget_base(w_su_base, /row, /nonexclusive)
  w_semical = widget_button(w_semical_base, value='Semi-calibrated', uvalue='semical')
  tmp = widget_label(w_su_base, value='Units: ')
  w_units_base = widget_base(w_su_base, /row, /exclusive, space=0)
  w_counts = widget_button(w_units_base, value='Counts', uvalue='counts', /no_release)
  w_rate = widget_button(w_units_base, value='Count Rate', uvalue='rate', /no_release)
  w_flux = widget_button(w_units_base, value='Count Flux', uvalue='flux', /no_release)
  w_flag_base = widget_base(w_su_base, /row, /frame, space=5)
  w_flags = widget_label(w_flag_base, value='Show Flags:    None', /dynamic_resize)
  tmp = widget_button(w_flag_base, value='Change...', uvalue='change_flags')

  w_corrbase = widget_base(w_base, /row, /frame, space=40)
  w_corr1base = widget_base(w_corrbase, /column)
  w_corr1 = widget_label(w_corr1base, value='', /dynamic_resize, /align_left)
  w_corr2 = widget_label(w_corr1base, value='', /dynamic_resize, /align_left)
  tmp = widget_button(w_corrbase, value='Change...', uvalue='changecorr', /align_center)



  w_button_base1 = widget_base( w_base,  /row,   /align_center,   space=8 )

  tmp = widget_button(w_button_base1, value='Plot Spectrum', uvalue='doenergyplot' )
  tmp = widget_button(w_button_base1, value='Plot Time History', uvalue='dotimeplot' )
  tmp = widget_button(w_button_base1, value='Plot Spectrogram', uvalue='dospecplot' )
  w_ltime = widget_button(w_button_base1, value='Plot Livetime ->', /menu)
  tmp = widget_button(w_ltime, value='Used (includes decim, gaps)',uvalue='doltime_used_plot' )
  tmp = widget_button(w_ltime, value='Corrected Counter',uvalue='doltime_ctr_plot' )
  tmp = widget_button(w_ltime, value='From Monitor Rates',uvalue='doltime_mon_plot' )
  tmp = widget_button(w_ltime, value='From Monitor Rates - Corrected',uvalue='doltime_corr_mon_plot' )
  tmp = widget_button(w_button_base1, value='Plot GOES', uvalue='plot_goes')


  w_write = widget_button(w_button_base1, value='Write FITS, SRM files ->', /menu)
  tmp = widget_button(w_write, value='Combined detectors...', uvalue='writefile')
  tmp = widget_button(w_write, value='Separate detectors...', uvalue='write_sepdet')
  tmp = widget_button(w_write, value='Separate detectors, native energy bins...', uvalue='write_sepdet_native')

  w_button_base2 = widget_base( w_base,  /row,   /align_center,   space=15 )

  temp = widget_button(w_button_base2, value='Refresh', uvalue='refresh')
  ;temp = widget_button(w_button_base2, value='Parameter Summary', uvalue='params', sensitive=0)
  temp = widget_button(w_button_base2, value='Reset to defaults', uvalue='defstandard')
  temp = widget_button(w_button_base2, value='Set params manually', uvalue='manoptions')
  w_script = widget_button(w_button_base2, value='Write script ->', /menu)
  temp = widget_button(w_script, value='Non-default GUI params', uvalue='script', uname='non-default')
  temp = widget_button(w_script, value='All GUI params', uvalue='script', uname='all')
  tmp = widget_button(w_button_base2, value='Help', uvalue='help' )
  tmp = widget_button(w_button_base2, value='Close', uvalue='close' )

  valid_plotman = 0
  if xalive(group) then begin
    widget_control, group, get_uvalue=mw_state
    if size(mw_state,/tname) eq 'STRUCT' then begin
      if tag_exist(mw_state,'plotman_obj') then begin
        plotman_obj = mw_state.plotman_obj
        w_message = mw_state.widgets.w_message
        valid_plotman = 1
      endif
    endif
  endif
  if not valid_plotman then plotman_obj = obj_new()

  ; message widget id was defined if we already had a plotman object defined.  Otherwise,set to 0.
  ; Check if xalive before using it.
  checkvar, w_message, 0L

  widgets = {w_top_base: w_top_base, $
    w_obs_time_label0: w_obs_time_label0, $
    w_obs_time_label1: w_obs_time_label1, $
    w_time: w_time, $
    w_energy_base: w_energy_base, $
    w_energies: w_energies, $
    w_nenergy: w_nenergy, $
    w_ecode: w_ecode, $
    w_use_chan: w_use_chan, $
    w_chan_base2: w_chan_base2, $
    w_chan_bin: w_chan_bin, $
    w_chan_min: w_chan_min, $
    w_chan_max: w_chan_max, $
    w_ntimebin: w_ntimebin, $
    w_times: w_times, $
    w_timebinw: w_timebinw, $
    w_colldisp: w_colldisp, $
    w_sum: w_sum, $
    w_semical: w_semical, $
    w_counts: w_counts, $
    w_rate: w_rate, $
    w_flux: w_flux, $
    w_flags: w_flags, $
    w_corr1: w_corr1, $
    w_corr2: w_corr2, $
    w_write: w_write }

;  params_used = [ $
;    'decimation_correct', $
;    'rear_decimation_correct', $
;    'obs_time_interval', $
;    'pileup_correct', $
;    'seg_index_mask', $
;    'sp_chan_binning', $
;    'sp_chan_max', $
;    'sp_chan_min', $
;    'sp_data_unit', $
;    'sp_energy_binning', $
;    'sp_semi_calibrated', $
;    'sp_time_interval', $
;    'sum_flag', $
;    'time_range', $
;    'use_flare_xyoffset', $
;    'xyoffset', $
;    'sum_coincidence' ]

  state = {data_obj: data_obj, $
    obs_time_label: obs_time_label, $
    obs_time_interval: obs_time_interval, $
    flare_str: flare_str, $
    ptr_obs_time_interval: ptr_obs_time_interval, $
    ptr_flare_str: ptr_flare_str, $
    intervals_obj: intervals_obj, $
    energy_intervals: ptr_new(/alloc), $
    use_kev: 0, $
    time_intervals: ptr_new(/alloc), $
    ;       standalone: xalive(group) ne 1, $
    params: params, $
    widgets:widgets, $
    plotman_obj: plotman_obj, $
    flags_obj: flags_obj, $
    w_message: w_message}

  widget_limit_ysize, w_top_base, w_base

  if xalive(group) then begin
    widget_offset, group, xoffset, yoffset, newbase=w_top_base
    widget_control, w_top_base, xoffset=xoffset, yoffset=yoffset
  endif

  widget_control, w_top_base, /realize, /hourglass

  hsi_ui_spec_widget_update, state

  widget_control, w_obs_time_label0, timer=0.

  widget_control, w_top_base, set_uvalue=state

  xmanager, 'hsi_ui_spec', w_top_base, /no_block, cleanup='hsi_ui_spec_cleanup'

end
