;+
;Name: HSI_ALG_UNITS

;Purpose: Give the units for all the accepted imaging algorithms and classes
;
;a = {hsi_alg_units, $
;	class:'',$  ;alg obj class name, i.e. HSI_XXX, eg HSI_BPROJ
;	name:'',$	;Descriptive name, eg Back Projection
;	nknme1:'',$ ;Shorthand name, eg BPROJECTION
;	nknme2:'',$ ;2nd Shorthand name, eg. VF
;	units:''  ,$;eg. 'Counts sc!u-1!n' for CLEAN
;	prefix:''} $ ;string pre-pended to alg control parameters

;alg_list = [ 'CLEAN', 'SATO', 'VIS_FWDFIT','VF','MEM_NJIT',  'PIXON', 'FORWARD', 'BPROJECTION', 'VIS_CS' ]
;alg_name = ['Clean', 'MEM Sato', 'VIS FWDFIT'+STRARR(2), 'MEM NJIT', 'Pixon', 'Forward Fit', 'Back Projection', 'VIS_CS']
;obj_class = ['HSI_CLEAN', 'HSI_MEM_SATO', 'HSI_VIS_FWDFIT'+STRARR(2), 'HSI_MEM_NJIT', $
;             'HSI_PIXON', 'HSI_FORWARDFIT', 'HSI_BPROJ', 'HSI_VIS_CS' ]

; 20-aug-2007, andre.csillaghy@fhnw,ch vis_only kwd
; 6-jul-2007, richard.schwartz@gsfc.nasa.gov
; 14-jul-2009, add uv_smooth
; 22-mar-2011, ADDED PREFIX FIELD TO STRUCTURE
; 22-oct-2012, ras,  order, active fields
; 09-oct-2017, Roman Bolzern, added VIS_CS.
;-

pro hsi_alg_units__define


a = {hsi_alg_units, $
	class:'',$  ;alg obj class name, i.e. HSI_XXX, eg HSI_BPROJ
	name:'',$	;Descriptive name, eg Back Projection

	nknme1:'',$ ;Shorthand name, eg BPROJECTION
	nknme2:'',$ ;2nd Shorthand name, eg. VF

	units:'', $ ;eg. 'Counts sc!u-1!n' for CLEAN
    is_vis: 0B,$ ; acs: to check whether ths alg uses annsec or visibility based algorithm
	prefix:'', $

	order: 0, $ ;listing order
	active: 0 } ; 1 for active, 0 for inactive} ;prefix used in init; added March 2011, ras

end
