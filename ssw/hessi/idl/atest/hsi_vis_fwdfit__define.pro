;---------------------------------------------------------------------------
; Document name: hsi_vis_fwdfit__define.pro
; Created by:    Andre Csillaghy, March 4, 1999
; Time-stamp: <Mon Dec 31 2007 17:37:18 csillag tournesol2.local>
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI VISIBILITY_FWDFIT ALGORITHM CLASS DEFINITION
;
; PURPOSE:
;       Object wrapper for the vis_fwdfit algorithm
;
; CATEGORY:
;       Imaging (hessi/image)
;
; CONSTRUCTION:
;       image_obj = Obj_Name( 'hsi_vis_fwdfit' )
;
; OUTPUT TYPE:
;      2D IMAGE ARRAY
;
; INPUT PARAMETERS:
;       Control (input) parameters are defined in the structure
;       {hsi_vis_fwdfit_parameters}.
;
; OUTPUT PARAMETERS:
;       Info (output) parameters are
;       CHI_SQ_TOT: the value of the chi2 at the exit of hsi_map_vis_fwdfit
;       RESID_MAP: the residual map
;
; SOURCE OBJECT:
;       HSI_Modpat_Products
;
; SEE ALSO:
;       hsi_map_vis_fwdfit
;       http://hessi.ssl.berkeley.edu/software/reference.html#hsi_vis_fwdfit
;
; HISTORY:
;       Development for Release 4, April 2000
;     22-Nov-2007, Kim.   In image_alg_hook:  abort if vis is not a structure.  Also abort if
;       there are fewer than nvis_min non-zero visibilities.
;     19-Jun-2008, Kim. Added fitstddev, qflag, redchisq, nfree, niter to call to hsi_vis_fwdfit so we can add
;       them to saved info params
;     30-Jun-2008, Kim. In Image_Alg_Hook, pass fit_mask through to hsi_vis_fwdfit.  THIS IS TEMPORARY.
;      4-may-2009, Kim. Added vf_vis_window to keep track of window id used to plot observed/fitted visibilities
;      7-May-2009, Kim. Changed hsi_fwdfit_parameters to hsi_vis_fwdfit_parameters for consistency
;     15-aug-2012, ras, changed test for valid visibilities to use (abs(obsvis)) and not totflux
;     28-Dec-2016, Kim. Added derived_parms keyword to call to hsi_vis_fwdfit, and add loop parameters returned
;       in derived_parms to param_out structure.
;     11-Jan-2017, Kim. In Image_Alg_Hook, before calling hsi_vis_fwdfit, modify the flux value in the srcin structure
;       to be a better estimate of flux if necessary (estimated by abs(obsvis)) - fit will fail if it's too far off.
;     26-Jul-2017, Kim. Commented out help,srcin
;     01-Sep-2017, Kim. Modify fluxes of sources better than in change of 11-Jan-2017 (and that fix only worked 
;       for one source, now handles multiple sources)
;-
;

pro hsi_vis_fwdfit_test

  ; 17 Jan 2005
  DEFAULT, time_range,    '2005-jan-17 '+['09:43:23', '09:43:27']      ; JQ 4s at S10
  ;DEFAULT, time_range,   '2005-jan-17 '+['09:43:24', '09:43:25']       ; JQ 1s at S10
  DEFAULT, energy_band,   [25,100]
  DEFAULT, xyoffset ,     [434,296]

  o = obj_new( 'hsi_vis_fwdfit' )
  o->set, time_range = time_range
  o->set, energy_band = energy_band
  o->set, xyoffset = xyoffset
  o->set, image_dim = [128,128]
  d = o->getdata()

  o = hsi_image( )
  o->set, time_range = '2005-jan-17 '+['09:43:23', '09:43:27']
  o->set, energy_band = [25,100]
  o->set, xyoffset =  [434,296]
  o->set, image_dim = [128,128]
  o->set, image_alg = 'vis'
  o->plot


  endo=obj_new('hsi_image')
  o->set, obs_time='20-feb-02 11:'+['06','07'], energy_band=[35,50.], det_index_mask=[0,0,bytarr(7)+1b]
  o->set, time_range=[10., 23.33]
  o->set, image_alg='njit', image_dim=[64,64], pixel_size=[2.,2]
  o->set, image_alg='vis_fwdfit', image_dim=[64,64], pixel_size=[2.,2], vis_fwdfit_multi=0, /vis_fwdfit_circle


end

;--------------------------------------------------------------------

FUNCTION HSI_Vis_Fwdfit::INIT, _EXTRA=_extra, source = source

  ; we need to adapt the source: we dont need modpat products, but hsi_calib_eventlis
  if not obj_valid( source ) then begin
    source = hsi_visibility()
  endif
  ; acs 2007-12-31 took this away, seems to be dead wood
  ;else vis_source = hsi_visibility( source = source->get( class = 'hsi_calib_eventlist', /obj ))

  ret=self->HSI_Image_Alg::INIT( 'vf', $
    CONTROL=hsi_vis_fwdfit_parameters(), $
    INFO={hsi_vis_fwdfit_info}, $
    SOURCE = source, $
    _EXTRA=_extra)
  self -> set, vf_vis_window = -1
  RETURN, ret

END

;--------------------------------------------------------------------

PRO HSI_Vis_Fwdfit::Image_Alg_Hook, param, image_out, param_out, fit_mask=fit_mask, _EXTRA = _extra

  source = self->Get( /SOURCE )
  ;conjugate = param.conjugate
  ;rm_outlier   = param.rm_outlier
  ;oute = source->getdata(_extra = _extra, edit=rm_outlier, conjugate=conjugate)
  oute = source->getdata(_extra = _extra)

  if not is_struct(oute) then begin
    msg = 'Aborting.  No visibilities found.'
    @hsi_message
  endif

  ;ngood = where (oute.totflux ne 0., cgood)
  ngood = where (abs(oute.obsvis) ne 0., cgood) ;changed test from totflux to make it useful for
  ;regularization where totflux isn't set
  nvis_min = self.caller->get(/nvis_min)
  if cgood lt nvis_min then begin
    msg = 'Aborting.  Number of non-zero visibilities ( ' + trim(cgood) + $
      ' ) is less than minimum required (' + trim(nvis_min) + ').'
    @hsi_message
  endif

  ;If param contains the srcin structure, it will be a pointer, so dereference the pointer and
  ; put the structure back into param structure.

  if have_tag( param, 'srcin' ) then begin
    if ptr_valid( param.srcin ) then begin
      srcin = *param.srcin
      if have_tag(srcin, 'srcflux') then begin
        nsrc = n_elements(srcin)

        ; First, if necessary, modify the starting flux value(s) to make them reasonable as follows:
        ;   Only for non-albedo sources.
        ;   Get total estimated flux from coarsest collimators.
        ;   If user's values aren't too far off, preserve them.
        ;   If total flux is > 1.2*est. flux, adjust fluxes smaller in proportion to their original values.
        ;   Adjust each value to be within .05 to 1.2 times est. flux.
        ;   If new total flux is < .8*est.flux, adjust fluxes bigger in proportion to their original values.

        iprimary = where(srcin.srctype ne 'albedo', count)
        if count eq 0 then begin
          msg = 'Aborting. No primary sources. Must have at least one non-albedo source.'
          @hsi_message
        endif
        sflux = srcin[iprimary].srcflux
        sflux_orig = sflux
        flux_estimate = (self->get(/source))->flux_estimate(oute) ; estimate flux based on coarsest sub-collimators
        tot_srcflux   = total( sflux )
        rlimits       = [ 0.05, 1.2 ] ;limits on ratio of flux_estimate to srcflux for total and single sources
        tot_limits    = flux_estimate * rlimits
        if tot_srcflux gt tot_limits[1] then sflux =  rlimits[1] * sflux * (flux_estimate / tot_srcflux)
        sflux = sflux < tot_limits[1] > tot_limits[0] ;default bounds on the initial srcflx
        tot_srcflux   = total( sflux )
        if tot_srcflux lt (0.8 * flux_estimate) then sflux =  sflux * (flux_estimate /tot_srcflux)

        if ~same_data(sflux_orig, sflux) then begin
          srcin[iprimary].srcflux = sflux
          print, ' '
          message, /info, 'Modified initial source flux(es) to ensure that they are in line with total flux estimate from coarse collimators.'
          print, 'Original flux(es): ' + arr2str(trim(sflux_orig),' ') + ' New flux(es): ' + arr2str(trim(sflux), ' ') + $
            ' Total flux estimate: ' + trim(flux_estimate)
        endif

      endif
      param = rem_tag( param, 'srcin' )
      param = add_tag( param, srcin, 'srcin')
      ;      help,srcin,/st
    endif else param = rem_tag( param, 'srcin' )
  endif

  vf_vis_window = self->get(/vf_vis_window)
  ; call with /noedit because if user wanted edit/combine done, it's already done in vis obj
  hsi_vis_fwdfit, oute, _extra = param, fit_mask=fit_mask, /noedit, $
    srcout = srcout, fitstddev=fitstddev, qflag=qflag, redchisq=redchisq, nfree=nfree, niter=niter, vf_vis_window=vf_vis_window, $
    derived_parms=derived_parms

  param_out = { srcout: srcout, sigma: fitstddev, niter: niter, redchi2: redchisq, $
    nfree: nfree, qflag: qflag, vf_vis_window: vf_vis_window, $
    looparc: derived_parms.looparc, loopwidth: derived_parms.loopwidth, $
    siglooparc: derived_parms.siglooparc, sigloopwidth: derived_parms.sigloopwidth }

  hsi_vis_fwdfit_source2map, srcout, (oute[0]).xyoffset, image_out, $
    pixel = (self.caller->get( /pixel_size ))[0], $
    mapsize = (self.caller->get( /image_dim ))[0]

END

;--------------------------------------------------------------------

PRO HSI_Vis_Fwdfit__Define

  self = {HSI_Vis_Fwdfit, $
    INHERITS HSI_Image_Alg }

END


;---------------------------------------------------------------------------
; End of 'hsi_image__define.pro'.
;---------------------------------------------------------------------------
