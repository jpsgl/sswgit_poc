;+
;NAME:
; hsi_qlook_image
;PURPOSE:
; For an input time range, returns the qlook_images, objects,
; or map structures.
;CALLING SEQUENCE:
; data = hsi_qlook_image(time_range,object=object,map=map,energy_band=energy_band,_extra=_extra)
;INPUT:
; time_range = the input time range for the images
;OUTPUT:
; data = an hsi_qlook_image array, object (or array of objects), or map, or array of maps
;KEYWORDS:
; flare_id = return the images for the flare_ids passed in, ignores time_range
; object = return an hsi_image object (or an array)
; map = return a map structure (or an array)
; energy_band = only return images with low energy limits ge energy_band[0] and lt 
;               energy_band[1]
; full_sun = if set, the return the full-sun image
; header_out = a string array with all of the FITS headers
;HISTORY:
; 4-may-2001, jmm, jimm@ssl.berkeley.edu
; 8-oct-2003, jmm, Rewritten, to avoid using the hsi_qlook_image
; object altogether
; 20-mar-2005, jmm, added filename_out keyword, to facilitate
; replotting
; 1-aug-2013, Kim.  Fixed bug when looking for flare_id in qli_fb - assumed fid always 7 characters,
;  but in later years, fid was 8 characters 
;-
FUNCTION Hsi_qlook_image, time_range, object = object, map = map, $
                          energy_band = energy_band, full_sun = full_sun, $
                          flare_id = flare_id, filename_out = filename_out, $
                          header_out = header_out, _extra = _extra

  otp =  -1
;catch
  err_xxx = 0
  catch, err_xxx
  If(err_xxx Ne 0) Then Begin
    catch, /cancel
    help, /last_message, output = err_msg
    For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
    print, 'Returning'
    Return, -1
  Endif
; Use flare_ids if passed in
  If(keyword_set(flare_id)) Then Begin
    fid = flare_id
    fid_str = strcompress(string(flare_id), /remove_all)
;    qli_fb = hsi_mult_filedb_inp(file_type = 'QLOOK_IMAGE', $
;                                 /dont_copy, _extra = _extra)
    hsi_filedb_read, qli_fb, filename = 'hsi_qlimg_filedb.fits', $
      _extra = _extra
;Do you have a file for this flare id?
    If(is_struct(qli_fb)) Then Begin
      fb_fid = strmid(qli_fb.file_id, 10, 99)
      fb_fid = ssw_strsplit(fb_fid, '_', /head)
      hfile0 = where_arr(fb_fid, fid, nhfile0)
      If(nhfile0 Gt 0) Then Begin
        h0 = where(qli_fb[hfile0].status_flag Ge 0, nh0)
        If(nh0 Gt 0) Then hfile = hfile0[h0] Else hfile = -1
      Endif Else hfile = -1
      If(hfile[0] Ne -1) Then files = qli_fb[hfile].file_id $
      Else files = -1
    Endif Else files = -1
  Endif Else Begin
; Here check the time range, and find the files
    oti = anytim(time_range)
    files = hsi_filedb_filename(oti, file_type = 'QLOOK_IMAGE', /no_dialog, $
                                _extra = _extra)
;Occasionally, files may have duplicate elements, jmm, 2013-02-08
    files = files[uniq(files)]
;sometimes the file does not have a corresponding flare anymore?
    nfiles = n_elements(files)
    fileids = strarr(n_elements(files))
    For j = 0, nfiles-1 Do Begin
      tj = strsplit(files[j], '_', /extract)
      fileids[j] = tj[2]
    Endfor
    o = hsi_flare_list(obs_time_i = oti)
    d = o -> getdata()
    ss = hsi_where_arr(long(fileids), d.id_number)
    files = files[ss]
    print, files
  Endelse
  If(is_string(files) Eq 0) Then message, 'No files found in Filedb'
;full sun?
  If(keyword_set(full_sun)) Then Begin
    eindex = 0
;Also you need to change the filename
    files = strmid(files, 0, 4)+'fs'+strmid(files, 6)
  Endif

;Here you have files, but there may be dbase issues
  files = hsi_find_in_archive(files, data_dir = data_dir, count = n)
  filename_out = files
  If(n Eq 0) Then message, 'No files found in dbase'  
  n = n_elements(files)
  If(keyword_set(object) Or keyword_set(map)) Then need_obj = 1b $
  Else need_Obj = 0b
  count = 0l
  For j = 0, n-1 Do Begin
    If(keyword_set(full_sun)) Then Begin
      neindex = 1
      eindex = 0
    Endif Else Begin
; Get file info
      dummy = hsi_image_fitsread(fitsfile = files[j], ebands_arr = ebands)
      If(keyword_set(energy_band)) Then Begin
        eindex = where(ebands[0, *] Ge energy_band[0] And $
                       ebands[0, *] Lt energy_band[1], neindex)
      Endif Else Begin
        neindex = n_elements(ebands[0, *])
        eindex = indgen(neindex)
      Endelse
      If(neindex Eq 0) Then Begin
        message, /info, 'No images in energy_band'
        print, energy_band
        print, 'FILE: ', files[j]
      Endif
    Endelse
    If(need_obj) Then Begin
      For k = 0, neindex-1 Do Begin
        objj = hsi_image_fitsread(fitsfile = files[j], eindex = eindex[k], $
                                  /object, /quick_obj, header = hj)
        If(obj_valid(objj)) Then Begin
          If(keyword_set(map)) Then Begin
            mapj = objj -> make_map()
;Add an id tag, for the energy band
            e = objj -> get(/energy_band)
            If(n_elements(e) Eq 2) Then Begin
              e = strcompress(string(fix(e)), /remove_all)
              id = e[0]+' - '+e[1]+' keV'
              mapj.id = id
            Endif
            tx = objj -> get(/absolute_time_range)
            If(n_elements(tx) Eq 2) Then mapj.dur = tx[1]-tx[0]
            If(count Eq 0) Then otp = mapj $
            Else otp = [temporary(otp), temporary(mapj)]
          Endif Else Begin
            If(count Eq 0) Then otp = objj $
            Else otp = [otp, objj]
          Endelse
          If(count Eq 0) Then header_out = hj Else Begin
	    header_out = transpose([transpose(header_out), transpose(hj)])
          Endelse
          count = count+1
        Endif
      Endfor
    Endif Else Begin            ;a mess
      dataj = hsi_image_fitsread(fitsfile = files[j], header = hj)
      sz = size(dataj)
      nhj = n_elements(hj)
      If(sz[0] Gt 0) Then Begin
        dataj = dataj[*, *, eindex]
        If(count Eq 0) Then Begin
	  otp = dataj 
          header_out = hj
        Endif Else Begin
          c1 = n_elements(otp[0, 0, *])
          otpa = fltarr(sz[1], sz[2], c1+neindex)
          otpa[*, *, 0:c1-1] = temporary(otp)
          otpa[*, *, c1:*] = dataj
          otp = temporary(otpa)
          header_out = transpose([transpose(header_out), transpose(hj)])
        Endelse
        count = count+1
      Endif
    Endelse
  Endfor
  Return, otp
End

            

