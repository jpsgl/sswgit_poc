;+
; Name: hsi_get_orbit_times
; 
; Purpose: Get start/end times of RHESSI orbits within specified time intervals
; 
; Input Arg:
;  time_range - 2-element time range in anytim format (if sec, use sec since 79/1/1)
;  overlap - if set, find orbit times that overlap with time_range.  Otherwise find orbits that are 
;    totally contained in time_range.
;  
; Output Keywords:
;  orb_start - array of orbit start times (in sec since 79/1/1)
;  orb_end - array of orbit end times (in sec since 79/1/1)
;  count - number of orbits within requested times
; 
; Example:
;   hsi_get_orbit_times, ['1-jan-2012','2-jan'2012'], orb_start=os, orb_end=oe, count=c
;   IDL> help,os,oe,c
;     OS              DOUBLE    = Array[15]
;     OE              DOUBLE    = Array[15]
;     C               LONG      =           15
;   
;; Modifications:
; 21-Oct-2011, Kim.  Add check for existence of ostart before setting orb_start...
; 31-May-2012, Kim.  Call chklog to translate HSI_FILEDB_ARCHIVE before using (needed on 
;  Windows, and in message call, index ym by ifile
; 11-Jun-2012, Kim.  Change algorithm for finding start/end time of orbits.  Current alg - reverse
;   indices of histogram for start orbit # - failed for 10-jun-2012.  Now look at sum of start/end 
;   orbit #s. When same, times are part of same orbit.
; 13-Jun-2014, Kim. Added overlap option and fixed small bug where last end time was wrong.
;   
;-

pro hsi_get_orbit_times, time_range, orb_start=orb_start, orb_end=orb_end, overlap=overlap, count=count


tr = anytim(time_range)
day_grid = timegrid(tr[0], tr[1], /day, /quiet)
ym = get_uniq(strmid(time2file(day_grid), 0, 6))  ; yyyymm

nfile_tot = 0
for ifile=0,n_elements(ym)-1 do begin
  file = file_search(chklog('$HSI_FILEDB_ARCHIVE'), 'hsi_filedb_'+ym[ifile]+'.fits', count=nfile)
  if nfile eq 0 then begin
    message,'No filedb file for day ' + ym[ifile] + '.  Can not do orbit plot for that day.', /cont
    goto, next
  endif
  
  nfile_tot = nfile_tot + nfile
  
  db = mrdfits(file[0], 1)

;  old algorithm just used start orbit #, and assumed that any interval with that start was
;  part of same orbit.  
;  hist = histogram(db.start_hessi_orbit_number, binsize=1., reverse_indices=rind)                                                                                                                                                                                                                                                                            
;  
;  for i = 0,n_elements(hist)-1 do begin
;
;    tstart = db[rind[rind[i]]].DATA_START_TIME__ANYTIM_ 
;    tend = db[rind[rind[i+1]-1]].DATA_END_TIME__ANYTIM_ 
;    if tend gt tr[0] and tstart lt tr[1] then begin
;      ostart = append_arr(ostart, tstart)
;      oend = append_arr(oend, tend)
;    endif
;  endfor
  
; New 12-jun-2012 - now look at start and end orbit #s to find start /end time or orbits  
; First restrict db elements to +- ~orbit of requested times 
  q = where (db.data_start_time__anytim_ gt (tr[0]-5500.) and db.data_end_time__anytim_ lt (tr[1]+5500.),count)
  if count gt 0 then begin
    db = db[q]
    sum = db.start_hessi_orbit_number + db.end_hessi_orbit_number
    i = 0
    ndb = n_elements(db)
    while i lt ndb-1 do begin
      ts = db[i].data_start_time__anytim_
      te = db[i].data_end_time__anytim_
      ; changed lt to le in next statement, Kim, 13-jun-2014
      while i le ndb-2 && sum[i+1] eq sum[i] do begin
        te = db[i+1].data_end_time__anytim_
        i = i+1
      endwhile
      ostart = append_arr(ostart, ts)
      oend = append_arr(oend, te)
      i = i+1
    endwhile
  endif 
next:
endfor

none = 0
if nfile_tot eq 0 or ~exist(ostart) then none = 1 else begin
  
  if keyword_set(overlap) then $ 
    good = where(ostart lt tr[1] and oend gt tr[0], count) $
  else $
    good = where(ostart ge tr[0] and ostart lt tr[1], count)
    
  if count eq 0 then none = 1
endelse

if none then begin
  atr = anytim(tr,/vms,/date)
  message,'No RHESSI orbit times for days ' + atr[0] + ' to ' +atr[1], /cont
  orb_start = -1
  orb_end = -1
  count = 0
  return
endif

orb_start = ostart[good]
orb_end = oend[good]
count = n_elements(orb_start)

end