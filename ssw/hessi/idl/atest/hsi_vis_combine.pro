FUNCTION hsi_vis_combine, visin,  CONJUGATE=conjugate, ERRWEIGHT=errweight, IGNOREMAG=ignoremag, ADD=add, VISCOUNT=viscount, $
  QUIET=quiet
  ;+
  ; Procedure:
  ;	Given an array of RHESSI visibilities, hsi_vis_combine returns a new array with similar uv points
  ;     combined (regardless of energy, time, etc).
  ; 	Input array is not modified.
  ; Usage:
  ;	output = HSI_Vis_Combine( visin, $
  ;		[CONJUGATE=conjugate, ERRWEIGHT=errweight, IGNOREMAG=ignoremag, ADD=add, VISCOUNT=viscount])
  ; Inputs:
  ; visin is an array of visibility structures.
  ; Keyword Inputs
  ; If /CONJUGATE is set, visibilities with -ve v values will be converted to their conjugate values.
  ; If /ERRWEIGHT is set, then visibilities are combined with weights ~ (1/sigamp^2); Otherwise all contributing
  ;     visibilities are weighted equally (default).
  ; If /ADD is set, then visibilities are added (as opposed to averaged).
  ; IGNOREMAG is a switch, passed on the hsi_vis_sort, to sort visibilities solely on the basis of position angle.
  ; VISCOUNT, if specified, prescribes the number of input visibilities that must contribute to each output visibility.
  ; Output:
  ; xyoffset in returned output structure will be the average of contributing xyoffsets.
  ; time and energy ranges in returned output structure will represent the outer envelope of contributing times and energies.
  ; If there are multiple harmonics, the output harmonic will be set to 0.
  ; If there are multiple energies, times, harmonics and/or xyoffsets, a message will be issued to output log.
  ;
  ; Note that the weighting options are:
  ; /ADD does an unweighted addition of visibilities
  ; /ERRWEIGHT does a s/n-weighted averaging of visibilities
  ; Default is an unweighted average of visibilities
  ; /ADD /ERRWEIGHT is a disallowed combination.
  ; Future options will support energy and time-weighted splicing.
  ; Modification History:
  ;    Jul-05     Initial version.  (ghurford@ssl.berkeley.edu)
  ; 10-Aug-05 gh  Update tag names to reflect 8-09-05 version of vis structure
  ;  1-Sep-05 gh  Add IGNOREMAG keyword to pass on to hsi_vis_sort
  ; 14-Sep-05 gh  Add provision for multiple input visibility structures
  ; 18-Sep-05 gh  Adapt to simplified visibility format.
  ;               Add VISCOUNT keyword
  ; 19-Sep-05 gh  Add "ADD" keyword.
  ; 13-Sep-07 gh  Correct longstanding bug that overestimated errors by a factor of n when averaging n visibility values.
  ;               Correct a minor bug in assigning new .harm tag when harmonics>1 are involved.
  ;               Correct a minor bug that mishandled sparse data averaged with /ERR flag.
  ; 30-Apr-09 Kim Added quiet keyword
  ; 13-dec-11 RAS, replicates the incoming type.
  ; 14-jun-2017, ras, correct propagation of stacked counts in fit
  ; 16-Jun-2017, Kim. Added time and energy to message about number of vis being returned
  ;-
  ; Initializing...
  DEFAULT, conjugate, 0
  DEFAULT, ignoremag, 0
  DEFAULT, errweight, 0
  DEFAULT, add,       0
  DEFAULT, quiet,     0
  warning     = ['harmonics', 'time ranges', 'energy ranges', 'xyoffsets']
  parmflag    = INTARR(4)                              ; = max number of different (harmonics, trange, erange, xyoffset) values in 1 group
  IF add NE 0 AND errweight NE 0 THEN MESSAGE, '/ERRWEIGHT and /ADD are conflicting switches'
  ;
  nvis        = N_ELEMENTS(visin)
  wt          = FLTARR(nvis)                          ; will hold final weights
  uvindex     = hsi_vis_sort(visin, CONJUGATE=conjugate, IGNOREMAG=ignoremag)     ; Identifies which visibilities are to be combined.
  ngroup      = MAX(uvindex) + 1                    ; number of distinctive output uv points
  vis         = REPLICATE( visin[0], ngroup ) ; replicate whatever the incoming type is
  IF KEYWORD_SET (errweight) NE 0 THEN relwt = 1./(visin.sigamp)^2 ELSE relwt = FLTARR(nvis)+1      ; relative weighting of visibilities
  utemp       = visin.u
  vtemp       = visin.v
  obstemp     = visin.obsvis
  ;
  ; Optionally conjugate individual visibilities where necessary.
  IF conjugate NE 0 THEN BEGIN
    i  = hsi_vis_select(visin, PAOUT=pa)
    neg = WHERE (pa GE 180, nneg)
    IF nneg GT 0 THEN BEGIN
      vtemp[neg]    = -vtemp[neg]
      utemp[neg]    = -utemp[neg]
      obstemp[neg]  = CONJ(obstemp[neg])
    ENDIF
  ENDIF
  ;
  ; Loop over output groups
  FOR n=0, ngroup-1 DO BEGIN
    j = WHERE(uvindex EQ n, nj)
    IF nj EQ 0 THEN MESSAGE, 'empty visibility group'                   ; This should not happen
    dummy           = WHERE(visin[j].sigamp EQ 0, nbad)
    IF nbad NE 0 THEN MESSAGE, 'Zero sigamp detected'                   ; This should not happen if data have been quality-edited.
    IF N_ELEMENTS(viscount) NE 0 THEN IF nj NE viscount THEN CONTINUE   ; Skip this group if group does not have VISCOUNT entries.
    totrwt          = TOTAL(relwt[j])
    wt[j]           = relwt[j]
    IF KEYWORD_SET(add) EQ 0 THEN wt[j] = wt[j]/totrwt                  ; normalize weighting for this group so that sum=1 unless /ADD is set.
    totwt           = TOTAL(wt[j])                                      ; will be 1 unless /ADD is set
    nh              = N_ELEMENTS(UNIQ(visin[j].harm, SORT(visin[j].harm)))      ; Number of different harmonics at this uv point
    parmflag[0]     = parmflag[0] > nh
    parmflag[1]     = parmflag[1] > N_ELEMENTS(get_uniq_range(visin[j].trange))/2
    parmflag[2]     = parmflag[2] > N_ELEMENTS(get_uniq_range(visin[j].erange))/2
    parmflag[3]     = parmflag[3] > N_ELEMENTS(get_uniq_range(visin[j].xyoffset))/2
    IF nh EQ 1 THEN nhout = visin[j[0]].harm ELSE nh = 0
    vis[n].isc      = visin[j[0]].isc
    vis[n].harm     = nhout
    vis[n].erange   = [MIN(visin[j].erange), MAX(visin[j].erange)]
    vis[n].trange   = [MIN(visin[j].trange), MAX(visin[j].trange)]
    vis[n].u        = TOTAL(utemp[j]          * wt[j]) / totwt          ; Uses average u, even if /ADD is set.
    vis[n].v        = TOTAL(vtemp[j]          * wt[j]) / totwt          ; Uses average v, even if /ADD is set.
    vis[n].obsvis   = TOTAL(obstemp[j]        * wt[j])
    vis[n].totflux  = TOTAL(visin[j].totflux  * wt[j])
    vis[n].sigamp   = SQRT(TOTAL((wt[j]*visin[j].sigamp)^2))
    vis[n].chi2     = MAX(visin[j].chi2)                                ; new chi2 is maximum of contributing chi2
    vis[n].xyoffset = (visin[j[0]].xyoffset)                            ; interim kluge until phase center shifting is implemented
    ;14-jun-2017, ras, correct propagation of stacked counts in fit
    vis[n].atten_state = max( visin[j].atten_state )
    vis[n].count    = Total( visin[j].count )
  ENDFOR
  ;
  ; Truncate output structure if necessary.
  ok = WHERE(vis.erange[0] NE 0, nok)
  IF nok EQ 0 THEN MESSAGE, 'had no valid output visibilities.'
  vis = vis[ok]
  if ~quiet then message, '  ' + hsi_vis_time_en(vis) + ': Returned ' + trim(nok) + ' visibilities.', /info
  if ~quiet then FOR n=0, 3 DO IF parmflag[n] GT 1 THEN PRINT, 'HSI_VIS_COMBINE combined visibilities with multiple ', warning[n]
  RETURN, vis
END