;+
; Description:
;    hsi_visibility::flux_estimate() returns the approximate total solar flux in units of ph/cm2/s based
;    on the abs(obsvis) in the coarsest sub-collimators
;
; Input: 
;   vis - (optional) visibility bag sorted from finest to coarsest sub-collimator pitch, If not supplied,
;     then vis getdata is called
;   _extra - may contain eb_index, tb_index used in call to vis getdata 
;
; Method: Estimates flux by taking mean of last 5 visibility amplitudes (removing outliers). Since vis is sorted
;   from finest to coarsest sub-collimator pitch, these will be the coarsest sub-collimator available.
;
; :Author: richard.schwartz@nasa.gov
; :History: Version 1, 10-jan-2017, loosely based on technique in hsi_mem_njit::mem
; 06-Sep-2017, Kim, Only use flux_arr values that are > 0.
;-
function hsi_visibility::flux_estimate, vis, _extra = _extra

  if ~is_struct(vis) then vis = self->getdata(_extra = _extra)
  
  flux_arr =  abs( vis.obsvis )  ; get flux amplitudes
  q = where(flux_arr gt 0., nflux)
  if nflux gt 0 then flux_arr = flux_arr[q]
  
  ; If fewer than 5 visiblilities (rarely happens) return average of flux_arr
  if nflux lt 5 then return, avg( flux_arr )
  
  ; Total flux estimate is mean flux of last 5 points of flux_arr, with points > 2 st.dev. removed
  resistant_mean, flux_arr[-5:-1], 2, flux
  return, flux
end