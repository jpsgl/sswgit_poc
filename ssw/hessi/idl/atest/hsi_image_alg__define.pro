;---------------------------------------------------------------------------
; Document name: hsi_image_alg__define.pro
; Last Modified: Wed Aug  1 12:36:17 2001 (csillag@sunlight)
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI IMAGING ALGORITHM ABSTRACT CLASS DEFINITION
;
; PURPOSE:
;       This abstract class defines operations common to all imaging
;       algorithms used for HESSI image processing. It is inherited by
;       a concrete class that usually has the same name as the
;       algorithm it implements.
;
; CATEGORY:
;       Imaging (hessi/image)
;
; CONSTRUCTION:
;       Only through the conceret classes
;
; OUTPUT TYPE:
;       2D IMAGE ARRAY
;
; INPUT PARAMETERS:
;       Control (input) parameters are defined in the concrete class.
;
; OUTPUT PARAMETERS:
;       Info (output) parameters are defined in the concrete class.
;
; SOURCE OBJECT:
;       HSI_Modpat_Products
;
; KNOWN CONCRETE CLASSES:
;       hsi_clean__define
;       hsi_forwardfit__define
;       hsi_pixon__define
;
; SEE ALSO:
;       concrete classes above
;       http://hessi.ssl.berkeley.edu/software/reference.html#hsi_image_alg
;
; HISTORY:
;   23-Jul-2018, Kim. Modified to store new xx_n_event_used info parameter (xx is alg prefix)
;     For vis algs get from vis.count, for non vis algs get from binned_n_event in calib eventlist
;     (different because of vis_edit) (don't store in binned_n_event as in 11-jul change)
;   11-Jul-2018, Kim. Added check_snr method, and call to it in process method. Also, now
;     we're storing binned_n_event (get from vis.count) even for vis algs.
;   07-Feb-2018, Kim. In process, set xx_det_index_mask_used (xx=alg prefix) and added
;     a need_update method so we can check if dets have changed
;   29-nov-2017, RAS, added _extra = _extra to framework::getdata to pass in keywords eb_index and tb_index
;   15-nov-2017, RAS, added getdata with class_name exception
;   13-nov-2017, RAS, added properties, last_cbe_normalize, cbe_normalize
;   11-Sep-2012, Kim. Call HSI_IMAGE_PROFILE_CSTAT with _extra (to pass in tb_index and eb_index)
;		15-aug-2012, ras, in process, simplified checking for vis alg and finding erange from vis object
;		12-nov-2010, ras, use hsi_get_alg_name( obj_class, /prefix) to get prefix from hsi_alg_units()
;       26-Aug-2010, Kim. If self.caller available, use it in call to profile_cstat
;       30-Apr-2010, Kim.  Added getcaller method, and added call to hsi_image_profile_cstat to
;         compute statistics comparing image profile with obs. profile, and save as info params.
;       1-May-2009,  Kim. Added quiet in call to get vis in process so output isn't confusing.
;       10-Feb-2009, Kim. Get energy bin for vis images from vis structure, not get(/energy_band)
;       24-Sep-2008, Kim. Move setting is_vis_alg (correction to 3-mar correction)
;       03-mar-2008, Kim. For vis algs, explicitly set im_eband_used
;       Release 5.1: updated documentation, January 2001, ACS
;       Development for Release 4, April 2000
;            csillag@ssl.berkeley.edu
;--------------------------------------------------------------------

FUNCTION HSI_Image_Alg::INIT, $,
  param_prefix, $
  SOURCE=source, $
  CONTROL=control, $
  INFO=info, $
  IMAGE_ALG=image_alg, $
  _EXTRA=_extra


  IF NOT Obj_Valid( source ) THEN source =  Obj_New( 'HSI_Modpat_Products' )
  ;12-nov-2010, ras;;;;;;;;;
  image_class = obj_class(Self)
  default, param_prefix, hsi_get_alg_name(image_class,/prefix)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;

  param_prefix =  param_prefix
  self.param_prefix = param_prefix
  IF N_Elements( CONTROL ) NE 0 THEN BEGIN
    control = Tag_Prefix( control, param_prefix )
  ENDIF
  IF N_Elements( INFO ) NE 0 THEN BEGIN
    info = Tag_Prefix( info, param_prefix )
  ENDIF

  ; this is ugly to do this with add tag but i dont want to change the
  ; info structure of each algs.
  ;info = add_tag( info, [0.,0.], 'im_energy_band_used' )
  info = add_tag( info, ptr_new(), 'im_error' )
  control = add_tag( control, 0B, 'im_calc_error' )
  self.last_cbe_normalize = -1
  ret=self->Framework::INIT( CONTROL = control, $
    INFO=info, $
    SOURCE=source, $
    _EXTRA=_extra )

  RETURN, ret

END

;----------------------------------------------------------
; Kim Added need_update method, 6-Feb-2018 to check whether det_index_mask has changed
function hsi_image_alg::need_update

  if Self->framework::Get(/need_update, /this_class) then return, 1

  if Self->framework::Need_update() then return, 1

  _extra = create_struct(self.param_prefix + '_det_index_mask_used', 1)
  dets_used = self->framework::get(_extra=_extra)
  if ~same_data(dets_used, Self->framework::Get(/det_index_mask)) then return, 1

end

;----------------------------------------------------------
;RAS, method added 13-nov-2017, supporting cbe_normalize
FUNCTION Hsi_image_alg::GetData, $
  cbe_normalize = cbe_normalize, $
  class_name = class_name, $
  _ref_extra = _extra


  @framework_insert_catch
  IF Keyword_Set( CLASS_NAME ) THEN BEGIN
    this_object = framework_find_class( self, class_name )
    IF Obj_Valid( this_object ) THEN BEGIN
      RETURN, this_object->GetData( _EXTRA=_extra )
    ENDIF ELSE BEGIN
      Message, 'This class ' + class_name + ' has not yet been instantiated.';, /CONTINUE
      RETURN, -1
    ENDELSE
  ENDIF

  ; this function overwrites the GetData from Framework. One crucial thing here
  ; is that it does not call any Process procedure, but rather passes the
  ; control to one of the strategies.

  IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra
  class_name = obj_class( self )
  is_vis = (hsi_alg_units(class_name)).is_vis
  if exist( cbe_normalize ) then self.cbe_normalize = cbe_normalize
  if self.last_cbe_normalize ne self.cbe_normalize then begin
    if ~is_vis then begin
      Self->set, /need_update, /this_class_only
      opsf = Self->Get(/obj, class_name = 'hsi_psf')
      if is_class( opsf, 'hsi_psf') then opsf-> free_cache
    endif
  endif
  data = Self->Framework::GetData( _EXTRA=_extra )  ;29-nov-2017, RAS, added _extra = _extra
  if n_elements( data ) ne 1 and ~is_vis then begin
    self.last_cbe_normalize = self.cbe_normalize
    cbe_corr_factors = hsi_get_corrfactors( self, cbe_normalize = cbe_normalize )
  endif

  Return, data
END

;--------------------------------------------------------------------

pro hsi_image_alg::setcaller, caller

  self.caller = caller

end

;--------------------------------------------------------------------

function hsi_image_alg::getcaller
  return,self.caller
end

;--------------------------------------------------------------------

PRO Hsi_Image_Alg::Process, _EXTRA = _extra

  ; this procedure loads the control parameters and passes the control
  ; to the child class with image_alg_hook. Once it returns from
  ; image_alg_hook, it stores the information parameters that may come
  ; out of the concrete class into the object. Image_alg_hook MUST be
  ; defined in the concrete class.

  param = self->Get( /THIS_CLASS_ONLY, /NO_DEREFERENCE, /CONTROL )

  IF self.verbose GT 5 THEN Message, 'Processing ' + Obj_Class( self ), /INFO

  dets = self->get(/det_index_mask)
  dets_used = where(dets, ndet)

  valid_caller = obj_valid( self.caller )

  ; For visibility algorithms, images must be square; acs 2007-11-24
  ; Get vis for this time/energy for use further down
  is_vis_alg = (hsi_algorithm_units( Self,/class)).is_vis
  if is_vis_alg then begin
    if valid_caller then begin
      image_dim = self.caller->get( /image_dim )
      if image_dim[0] ne image_dim[1] then $
        message, 'image dimension must be square for visibility algorithms, not setting, aborting'
    endif
    vis = self->getdata(class='hsi_visibility', /quiet, _extra=_extra)
  endif  

  ; See if we should even try making an image for this time/energy by checking SNR, Kim 11-Jul-2018  
  snr_result = self->check_snr(vis=vis, _extra=_extra)
  if snr_result.proceed eq 0 then begin
    self.caller->set, snr_value=snr_result.value, snr_msg=snr_result.msg
    msg = snr_result.msg
    @hsi_message
  endif

  if is_struct(param) then param = Tag_Prefix( param, self.param_prefix, /REMOVE  )
  self->Image_Alg_Hook, param, image_out, param_out, cbe_normalize = cbe_normalize, _EXTRA = _extra

  self->SetData, image_out

  ; For vis algs, since vis for all energies and times were created before we make images, we have to
  ; set im_eband_used explicitly for this image, otherwise they're the last ones in the vis bag.
  ; Also, for vis algs, get n_event for each det from count tag in vis structure
  ;  If not vis alg, then get n_event from binned_n_event from calib_eventlist
  if is_vis_alg then begin
    eb_index = have_tag( _extra, 'eb_index') ? _extra.eb_index : 0
    ;We use the im_energy_binning here because it uses the edges used for the imaging which
    ;may be diff from the edges in the raw visibilities
    erange = ( hsi_energy_edge( /edges_2, self.caller->get(/im_energy_binning)) )[*,eb_index]
    self.caller -> set, im_eband_used=erange	; kim added 20-Jan-2008, changed ras 13-aug-2012   
    n_event = lonarr(9)
    for i=0,ndet-1 do n_event[dets_used[i]] = total(vis[where(vis.isc eq dets_used[i])].count)
  endif else n_event = self->get(/binned_n_event, class='hsi_calib_eventlist')

  IF N_Elements( param_out ) NE 0 THEN BEGIN
    param_out = Tag_Prefix( param_out, self.param_prefix )
    self->Set, _EXTRA = param_out, /THIS_CLASS_ONLY, /ONLY_INFO
  ENDIF

  self.caller->set, snr_value=snr_result.value, snr_msg=snr_result.msg

  ; Get structure with statistics from comparing profiles of observed and expected
  ; Self here is the alg obj (e.g. hsi_clean, hsi_vis_fwdfit).  For vis algs, don't have access
  ;  to hsi_modul_profile and some params needed in this call (like pixel_size), so use self.caller
  ;  if it's available (and it will be for vis algs).  If it's not available, use self.  26-aug-2010, Kim
  oo = obj_valid(self.caller) ? self.caller : self
  prof_struct = hsi_image_profile_cstat(oo, vimage=image_out, _extra=_extra)
  ; add alg's prefix as well as '_profile' to name of each tag, and store in info structure
  prof_struct = tag_prefix(prof_struct,self.param_prefix+'_profile')
  self -> set, _extra=prof_struct

  ; Set info param xx_det_index_mask_used (xx=alg prefix), 07-Feb-2018, Kim.
  det_used = tag_prefix({det_index_mask_used: self->get(/det_index_mask)}, self.param_prefix)
  self->set, _extra=det_used

  ; Set info param xx_n_event_used (xx=alg prefix), 20-Jul-2018, Kim.
  n_event_used = tag_prefix({n_event_used: n_event}, self.param_prefix)
  self->set, _extra=n_event_used

  ; this is very temporary for paolo, so dont use that as reference, we need to
  ; revise this in more details.
  im_calc_error = self->get( /im_calc_error )
  if im_calc_error then begin
    error = hsi_calc_image_error( image_out, self, ALG_USED = obj_class( self ) )
    self->set, im_error = error
  endif

  ;self->set, im_energy_band_used = self->get( /energy_band )

END
;--------------------------------------------------------------------
function hsi_image_alg::cbe_normalize
  return, self.cbe_normalize
end
;--------------------------------------------------------------------

; Function to check signal to noise ratio in the visibilities to see if an image should be made.
; Either pass in vis for current time/energy, or we'll try to get vis from file specified by
; snr_vis_file parameter. If using a vis alg, vis should be available already, if using a
; non-vis alg, user should set snr_vis_file to a vis file with matching time/energy to this image obj.
; snr_chk control parameter means:
;  0 - don't check SNR
;  1 - try to check SNR. If can't (no vis, or not enough vis), DON'T make image.  If can, make image if snr<thresh
;  2 - try to check SNR. If can't (no vis, or not enough vis), MAKE image anyway. If can, make image if snr<thresh
; Returns structure with:
;  proceed - 0/1 means don't / do make image
;  value - value of SNR
;  msg - any message saying why we couldn't compute SNR
; Added 11-Jul-2018, Kim Tolbert

function hsi_image_alg::check_snr, vis=vis, eb_index=eb_index, tb_index=tb_index

  ret_val = {proceed: 1, value: 0., msg: ''}

  valid_caller = obj_valid(self.caller)
  snr_chk = valid_caller ? self.caller->get(/snr_chk) : 0

  if snr_chk eq 0 then return, ret_val

  msg_tail = snr_chk eq 1 ? "Not making image" : "Making image anyway."

  ; If we didn't pass vis structure in, we'll try to get it from the vis FITS file set in
  ; snr_vis_file parameter, if any.

  if ~exist(vis) then begin
    snr_vis_file =  self.caller->get(/snr_vis_file)
    vinfo = hsi_vis_info(snr_vis_file, visobj, /quiet)
    if is_class(visobj,'hsi_visibility') then begin

      ; See if time/energy of this image match  a time/energy interval in vis file
      ; If not, we can't use vis file to check relative error, so return 1

      checkvar, eb_index, 0
      checkvar, tb_index, 0
      im_ens = self.caller->get(/im_energy_binning)
      im_times = self.caller->get(/im_time_interval)
      im_en = im_ens[*, eb_index]
      im_time = im_times[*, tb_index]

      match_intervals, vinfo.tbins, im_time, epsilon=.001, tind, count=count
      if count eq 1 then match_intervals, vinfo.ebins, im_en, epsilon=.1, eind, count=count
      if count eq 0 then begin
        ret_val.proceed = snr_chk eq 1 ? 0 : 1
        ret_val.msg = "No matching time/energy in vis file supplied, can't check SNR. " + msg_tail
        return,ret_val
      endif

      ; Get visibilities for this time/energy with same settings as are set in image object
      vis_edit = self.caller->get(/vis_edit)
      vis_normalize = self.caller->get(/vis_normalize)
      vis_conjugate = self.caller->get(/vis_conjugate)
      vis_type = self.caller->get(/vis_type)
      ; maybe not needed since already read file in hsi_vis_info??
      ;      visobj->read  ; have to read in photon vis first, then can change vis_type
      visobj->set, vis_type=vis_type
      visobj->set, vis_normalize=vis_normalize, vis_edit=vis_edt, vis_conjugate=vis_conjugate
      vis = visobj->getdata(tb_index=tind[0], eb_index=eind[0], det_index_mask=bytarr(9)+1b)

    endif else begin
      ret_val.proceed = snr_chk eq 1 ? 0 : 1
      ret_val.msg = "No visibilities or vis file supplied, can't check SNR. " + msg_tail
      return,ret_val
    endelse
  endif

  ; First try to use isc 6,7,8.  If not enough vis for them, then try 5,6,7,8. If still not enough vis
  ; then we can't do this test.  If have enough vis, then get mean of SNR for those vis.

  ibig = where(vis.isc gt 5, nbig)
  if nbig lt 2 then ibig = where(vis.isc gt 4, nbig)
  if nbig lt 2 then begin
    ret_val.proceed = snr_chk eq 1 ? 0 : 1
    ret_val.msg = "Not enough visibilities in ISC 5 and above. Can't check SNR. " + msg_tail
    return, ret_val
  endif else begin
    resistant_mean, f_div( abs(vis[ibig].obsvis), vis[ibig].sigamp ), 3., snr_value
  endelse
  ret_val.value = snr_value
  snr_thresh = self.caller->get(/snr_thresh)
  if snr_value lt snr_thresh then begin
    ret_val.proceed = 0
    ret_val.msg = "SNR in vis for coarse dets < threshold. Not making image. (SNR = " + trim(snr_value) + $
      ", SNR threshold = " + trim(snr_thresh) + ")"
  endif else ret_val.proceed = 1

  return, ret_val

end

;--------------------------------------------------------------------

PRO Hsi_Image_Alg__Define

  self = {HSI_Image_Alg, $
    param_prefix: '', $
    caller: obj_new(), $
    cbe_normalize: 0, $
    last_cbe_normalize: 0, $
    INHERITS Framework }

END

;---------------------------------------------------------------------------
; End of 'hsi_image__define.pro'.
;---------------------------------------------------------------------------
