;+
; PROJECT:
;   HESSI
; NAME:
;   HSI_XY2ANNSEC
;
; PURPOSE:
;   This function interpolates an image in the modpat Cartesian FOV to
;   the annular sector coordinate system.
;
; CATEGORY:
;   HESSI, IMAGE, UTIL, ANNSEC
;
; CALLING SEQUENCE:
;   annsecimage = hsi_xy2annsec(xyimage, modpat_obj, /cubic )
;
; CALLS:
;   HESSI OBJECTS, HSI_FOVBOX, INTERP2D, HSI_ANNSEC_COORD
;
; INPUTS:
;   xyimage - image on Cartesian coordiantes defined by modpat object's control parameters:
;     XYOFFSET, IMAGE_DIM, PIXEL_SIZE, PIXEL_SCALE
;   image_obj - image object containing hsi_modul_pattern object.
;   ;
;
; OPTIONAL KEYWORD INPUTS:
;   MODPAT_PTR - Pointer array which stores the universal modulation pattern map structure.
;     Used in for testing only.  Normally, the object carries the modulation pattern structure.
;   XGRID - optional 1-d coordinates of pixel along x axis.  In arcseconds.
;   YGRID - optional 1-d coordinates of pixel along y axis.  In arcseconds.
;   THIS_DET_INDEX and THIS_HARMONIC may be used to select a particular MODPAT_PTR from the
;   hsi_modul_pattern object. ( modpat_ptr_arr = object->getdata(class_name='hsi_modul_pattern') ).
;   EXTRAPOLATE, MISSING, QUINTIC and CUBIC are passed into the INTERP2D routine used
;   to map the image to the new coordinate system.  EXTRAPOLATE takes precedence over MISSING.
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   Uses INTERP2D to map from the Cartesian to polar coordinates of the annular sector.Mayfieldhigh

;
; MODIFICATION HISTORY:
;   Version 1, richard.schwartz@gsfc.nasa.gov, 8-jun-2000
;   5-sep-2003, ras, require img_obj input
;
;-

function hsi_xy2annsec, xyimage, image_obj,   xgrid=xgrid, ygrid=ygrid,$
this_harmonic=this_harmonic, this_det_index=this_det_index, $
quintic=quintic, cubic=cubic, extrapolate=extrapolate, $
modpat_ptr=modpat_ptr, missing=missing


fovbox = hsi_fovbox( img_obj, modpat_ptr=modpat_ptr )
sxy    = size( xyimage )
xgrid  = interpolate( fovbox[[0,2]], findgen(sxy[1])/(sxy[1]-1))
ygrid  = interpolate( fovbox[[0,2]+1], findgen(sxy[2])/(sxy[2]-1))

case  1 of
    size(/tname, modpat_obj) eq 'OBJREF' : begin
        valid= where(modpat_obj->get(/det_index_mask))
        this_modpat_ptr = (modpat_obj->getdata(class_name='hsi_modul_pattern'))[valid[0]]
        end
    else: begin
        valid = 0
        while size(/tname,*this_modpat_ptr[valid]) ne 'STRUCT' do valid = valid+1
        this_modpat_ptr = this_modpat_ptr[valid]
        end
    endcase

if exist(this_det_index) and n_elements(this_modpat_ptr) gt 1 then $
this_modpat_ptr = this_modpat_ptr[ this_det_index, this_harmonic ]

hsi_annsec_coord, dummy, x1, y1, rmap, thetamap, modpat_ptr = this_modpat_ptr

if not keyword_set(extrapolate) then $
if not keyword_set(missing) then missing = avg( xyimage)

return, interp2d( xyimage, xgrid, ygrid, x1, y1, $
extrapolate=extrapolate, missing=missing, cubic=cubic, quintic=quintic,/regular)


end


