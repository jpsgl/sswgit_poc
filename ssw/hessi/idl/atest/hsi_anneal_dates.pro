;+
;NAME:
; hsi_anneal_dates
;PURPOSE:
; Returns date ranges for RHESSI annealing, note that times are not
; exact, but estimated by looking at plots.
;CALLING SEQUENCE:
; dates = hsi_anneal_dates()
;INPUT:
; None
;OUTPUT:
; dates a 2XN array of start and end times for annealing, as seen from
; the RHESSI browser
;KEYWORDS:
; inverse = if set, return a 2Xn array of start and end times for
;           not annealing, starting with the start of the mission and
;           ending with now.
;HISTORY:
; 2014-11-30, jmm, jimm@ssl.berkeley.edu
; 2015-06-24, jmm, added inverse keyword
;-
Function hsi_anneal_dates, inverse = inverse, $
                           _extra = _extra

  dates = [['05-nov-2007 07:00:00', '29-nov-2007 10:56:40'], $
           ['16-mar-2010 00:52:20', '01-may-2010 08:31:40'], $
           ['17-jan-2012 03:12:42', '22-feb-2012 14:21:40'], $
           ['26-jun-2014 03:07:00', '12-aug-2014 04:27:00']]
  If(keyword_set(inverse)) Then Begin
     n_anneal = n_elements(dates[0, *])
     dates = anytim(dates)
     times0 = [anytim('12-feb-2002 00:00:00'), $
               reform(dates, 2*n_anneal), $
               anytim(!stime)+24.0*3600.0d0];add a day to be careful about time zones
     dates = reform(times0, 2, n_anneal+1)
  Endif

  Return, anytim(dates, _extra=_extra)

End

