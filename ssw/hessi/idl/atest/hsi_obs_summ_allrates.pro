;---------------------------------------------------------------------------
;+
; NAME:
;     avsig_1
; PURPOSE:
;     Average and dispersion of an array, zeros can be not included,
; CALLING SEQUENCE:
;     xbar=avsig_1(x, sigma, no_zeros=no_zeros, sig_mean=sig_mean, dimension=dimension, /fractional)
; INPUT:
;     x = an array
; OUTPUT:
;     xbar = mean, total(x)/n_elements(x)
;     sigma = standard deviation, sqrt(total((x-xbar)^2/(nx-1)))
; KEYWORDS:
;     no_zeros= if set, strip out zeros, note that this option does
;               not work if the dimension keyword is used. At least not as of
;               2-13-95...
;     sig_mean = sigma/sqrt(nx), the standard deviation of the mean of the array
;     dimension = the dimension of the array to find the mean in,
;                 passed into the total command, it must be a scalar.
;     fractional = the fractional error is passed out as sigma,
;                  don't use this if zero is a valid value of xbar...
; HISTORY:
;     12-9-94, jmm
;     2-13-95, jmm added dimension keyword, switched from ok_zeros to no_zeros
;     5-sep-1996, jmm, switched to double precision
;-
FUNCTION Avsig_1, x0, sigma, no_zeros=no_zeros, sig_mean=sig_mean, $
                  dimension=dimension, fractional=fractional

   x = double(x0)
   IF(KEYWORD_SET(dimension)) THEN BEGIN
;get the size of the array, if it's only 1d, goto the else part
      size_x = size(x)
      IF(size_x[0] LE 1) THEN GOTO, its_a_vector
;Ok, now be sure that the given dimension exists
      d0 = long(dimension[0])
      IF(d0 GT size_x[0]) THEN BEGIN
         message, 'NOT ENOUGH DIMENSIONS IN ARRAY, USING WHOLE ARRAY', /info
         GOTO, its_a_vector
      ENDIF
;N-elements
      nx = float(size_x(d0))
      xbar = total(x, d0)/nx ;the mean is easy
      sigma = total(x^2, d0) ;gotta use two sums

      IF(nx GT 1.0) THEN sigma = sqrt((sigma-nx*xbar^2)/(nx-1.0)) $
        ELSE sigma = make_array(size = size(xbar)) + 0.0
      sig_mean = sigma/sqrt(nx)
   ENDIF ELSE BEGIN
      its_a_vector: IF(KEYWORD_SET(no_zeros)) THEN BEGIN
         ok = where(x GT 0.0, nx)
         nx = float(nx)
         IF(nx GT 0) THEN BEGIN
            xbar = total(x[ok])/nx
            IF(nx GT 1) THEN sigma = sqrt(total((x[ok]-xbar)^2)/(nx-1.0)) $
              ELSE sigma = 0.0
            sig_mean = sigma/sqrt(nx)
         ENDIF ELSE BEGIN
            xbar = 0.0
            sigma = 0.0
            sig_mean = 0.0
         ENDELSE
      ENDIF ELSE BEGIN
         nx = float(N_ELEMENTS(x))
         xbar = total(x)/nx
         IF(nx GT 1) THEN sigma = sqrt(total((x-xbar)^2)/(nx-1.0)) $
           ELSE sigma = 0.0
         sig_mean = sigma/sqrt(nx)
      ENDELSE
   ENDELSE

   IF(KEYWORD_SET(fractional)) THEN BEGIN
      xxx = where(xbar NE 0.0)
      fraction = xbar & fraction[*] = 0.0
      fraction[xxx] = sigma[xxx]/xbar[xxx]
      sigma = fraction
   ENDIF

   RETURN, xbar
END

;+
;NAME:
; hsi_obs_summ_allrates
;PROJECT:
; HESSI
;CATEGORY:
; Hessi catalog generation
;PURPOSE:
; Calculates count rates from hessi eventlist data, to be
; used in the HESSI Observing Summary. The idea is that the
; eventlist should only have to be created once per file
;CALLING SEQUENCE:
; hsi_obs_summ_allrates, infile, time_intv = time_intv, $
;                        energy_edges = energy_edges, $
;                        Quiet=quiet, $
;                        simulated_data=simulated_data, $
;                        obs_time_interval=obs_time_interval
;INPUT:
; infile = A fits file to get data from
; data = a data structure of type  {Hsi_obssummratedata}
;OUTPUT:
; All in a common block hsi_obs_summ_rates,
; pkt = the packet array for the given eventlist
; pkttbl = the packet array lookup table
; pktt = the packet collect time in anytim format, sec from 1-jan-1979
; rates = the count rate, for each detector for each energy band
; rates_mvar = the modulation variance front detectors, colls 7 and 8
; ut_ref_rates = reference time for the rates
; dt_rates = the interval time
; ntimes_rates = the number of time intervals
; av_lvtim = av live time, for all detectors
;KEYWORDS:
; time_intv = the nominal duration, in seconds,
;             of a single time interval, note that
;             there is no info about the duration of
;             individual time intervals, the default is 4.0 seconds
; energy_edges = the energy bin edges
; quiet = if set, run quietly, this is the default
; obs_time_interval = if passed in, the obs_summary is calculated for this
;                  time_range, and the summary start and end times
;                  will be set to this range
; simulated_data = set to 1 for simulated data
;HISTORY:
; Hacked from hsi_obs_summ_fill, 21-nov-2000, jmm, jimm@ssl.berkeley.edu
; Added particle rate calculation, 30-nov-2000, jmm
; Combined rear segments, 1-dec-2000, jmm
; uses lightcurve object, 19-jan-2001, jmm
; Demands file input adds live time and fastrates, endfileinfo
; keyword, 23-may-2001, jmm
; changed common to an anonymous structure, added _extra,
; 6-jun-2001,jmm
; to avoid problems with lightcurve object, defines ut_ref_rates
; to be the start of the file, or obs_time_interval, not at the 
; start of the eventlist, jmm, 31-oct-2001
; Added det_index and seg_index_mask keywords, total_rates is now per
; detector jmm, 7-nov-2001
; Removed eventlist_obj from outputs, no longer use sources, uses the
; same light curve object for the rates and rates_mvar arrays, jmm,
; 13-nov-2001
; 2-mar-2002, jmm, added summ_page_data
; 11-mar-2002, jmm, added rates_per_det, 
; 10-apr-2002, jmm, insulated against problems caused by bad packets
; getting into hsi_sctime2any
; 5-may-2002, jmm, Returns arrays full of zeros for the no app_id=100
; case, this will make life easier for concatenation purposes
; 21-aug-2002, ref_time is the start of the day, otherwise the round
; function overflows, jmm
; 21-oct-2003, jmm, killed simulated_data keyword
; 13-nov-2003, jmm, Moved the light curve object inside the 2048
; second loop to avoid odd crashes.
; 5-aug-2005, jmm, set use_flare_xyoffset to 0
; 9-mar-2011, jmm, remove d9 from calculation of rates
; 8-nov-2011, jmm, Add option to change the actual time 
;                  range for the loop over light curve objects
; 21-jun-2012, jmm, Changed the default for the light curve to be 128 
;                   seconds, from 2048 seconds due to crashes in live 
;                   time calculations
; 4-jan-2016, jmm, manages detector use using hsi_qlook_dets2use
; 19-mar-2017, jmm, Using filename plus time interval no longer works
;                   in the light curve object.
;-
Pro Hsi_obs_summ_allrates, infile, $ 
                           time_intv = time_intv, $
                           Energy_edges = energy_edges, $
                           Quiet = quiet, Xxx = xxx, $
                           Var_nbin = var_nbin, $
                           Var_erange = var_erange, $
                           obs_time_interval = obs_time_interval, $
                           endfileinfo = endfileinfo, $
                           file_time_intv = file_time_intv, $
                           packet_time_intv = packet_time_intv, $
                           seg_index_mask = seg_index_mask, $
                           det_index = det_index, $
                           npdr = npdr, $
                           loop_time_intv = loop_time_intv, $
                           _extra = _extra

;Hsi_obs_summ_rates holds the count rates
  Common hsi_obs_summ_rates, hsi_rates_struct
   
  If(keyword_set(quiet)) Then notquiet = 0 Else notquiet = 1
  t00 = !Stime
  If(notquiet) Then print, 'Allrates start:  ', t00
  t00 = anytim(t00)
   
;Initialize
  hsi_rates_struct = -1
  pkt = -1 & pktt = -1 & pkttbl = -1 & rates = -1 & tot_rates = -1
  rates_mvar = -1 & mon_rates = -1
  segi = -1 & ftr = -1 &  ndet = -1
  ut_ref_rates = 0 & dt_rates = 0 & ntimes_rates = 0
  packet_rate = -1 & rates_per_det = -1 &  ut_ref_prt = 0
  max_det_vs_tot = -1
   
;put in a catch
  err_xxx = 0
  catch, err_xxx
  If(err_xxx Ne 0) Then Begin
    catch, /cancel
    Print, 'Hsi_obs_summ_allrates exiting...'
    help, /last_message, output = err_msg
    For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
    hsi_rates_struct = -1
    pkt = -1 & pktt = -1 & pkttbl = -1 & rates = -1 & tot_rates = -1
    rates_mvar = -1 & mon_rates = -1
    segi = -1 & ftr = -1 &  ndet = -1
    ut_ref_rates = 0 & dt_rates = 0 & ntimes_rates = 0
    packet_rate = -1 & rates_per_det = -1 &  ut_ref_prt = 0
    max_det_vs_tot = -1
    Return
  Endif

;Some keywords   
  If(Not keyword_set(npdr)) Then npdr = 5 ;# of rate bins per packet rate bin
  If(keyword_set(var_nbin)) Then vnbin = var_nbin Else vnbin = 128
  If(keyword_set(var_erange)) Then ch0v = var_erange $ 
  Else ch0v = 1
  If(keyword_set(energy_edges)) Then enbins = energy_edges $
  Else enbins = [3.0, 6.0, 12.0, 25.0, 50.0, 100.0, 300.0, 800.0, $
                 7000.0, 20000.0]
  nenbins = n_elements(enbins)-1
  rates_ebands = enbins

  If(keyword_set(time_intv)) Then dt_rates = time_intv[0] $
  Else dt_rates = 4.0d0
  If(keyword_set(file_time_intv)) Then dt_files = file_time_intv $
  Else dt_files = 5.0d0*dt_rates
  If(keyword_set(packet_time_intv)) Then dt_prt = packet_time_intv $
  Else dt_prt = 20.0d0
   
;Can you find the file?
  aa = loc_file(infile[0], count = bb)
  break_file, infile[0], a, b, c, d
  If(bb Eq 0) Then Begin        ;No file
    If(notquiet) Then Begin
      message, /info, 'File, '+infile[0]+' not found'
      message, /info, 'looking in Data Archive'
    Endif
    aa = hsi_find_in_archive(c+d, count = bb, /no_dialog)
    If(bb Eq 0) Then Begin
      message, /info, 'Telemetry File not found:'
      print, infile[0]
      Return
    Endif
  Endif
  infile = aa[0]
   
;Get the packet array,
  print, 'FILE: ', infile
  If(notquiet) Then message, /info, 'packet array'
  pkt_obj = obj_new('hsi_packet')
  pkt = pkt_obj -> getdata(/all, filename = infile[0])
  If(datatype(pkt) Ne 'STC') Then Begin
    If(notquiet) Then message, /info, 'No Packets'
    Return
  Endif
  pkttbl = pkt_obj -> getdata(/lookup_table)
  pktt = pkttbl.collect_time
  okpak = where(pkttbl.bad_pak Eq 0 And pktt Gt 0.0)
  If(okpak[0] Ne -1) Then Begin
    pktt[okpak] = hsi_sctime2any(pkt[okpak].collect_time)
  Endif Else Begin
    If(notquiet) Then message, /info, 'No Good Packets'
    Return
  Endelse
  npkt = n_elements(pkt)
;  ftr = pkt_obj -> get(/file_time_range), seems to be failing
  ftr = [min(pktt[okpak]), max(pktt[okpak])]
;Ok, now we have packets, Set up the time range,
  one_day = 24.0*3600.0
  t00x = anytim('4-jul-2000 0:00')
  If(keyword_set(obs_time_interval)) Then Begin
    print, 'OBS TIME INTERVAL IS SET'
    oti = anytim(obs_time_interval)
;Here, if there is something freaky about the ftr, reset it to oti
    ftr[0] = ftr[0] > oti[0]
    ftr[1] = ftr[1] < oti[1]
  Endif Else Begin
;All files start an integral number of dt from 4-jul-2000
    oti = ftr
    oti[0] =  hsi_set_file_time(ftr[0], time_intv = dt_files, $
                                ref_time = t00x)
  Endelse
;a reference time for rounding
  ref_time = t00x+one_day*long((oti[0]-t00x)/one_day)
  dt_total = oti[1]-oti[0]
;   tu = hsi_adjust_time_unit(dt_total, 1, quiet=quiet) ;10-nov-2001
  tu = 1                       ;this had better work, jmm, 12-apr-2002
  ntimes_rates = long(dt_total/dt_rates)
  If((dt_total Mod dt_rates) Ne 0.0) Then ntimes_rates = ntimes_rates+1
  oti[1] = oti[0]+ntimes_rates*dt_rates
  ut_ref_rates = oti[0]
;Here define the output arrays, no more -1's, jmm, 5-may-2002
  If(ntimes_rates Gt 0) Then Begin
    rates = dblarr(ntimes_rates, nenbins, 18)
    tot_rates = dblarr(ntimes_rates, nenbins)
    rates_mvar = bytarr(2, ntimes_rates)
  Endif
;Ok, now you may get the eventlist
  ok100 = where(pkttbl.app_id Eq 100 And pkttbl.bad_pak Eq 0)
  If(ok100[0] Ne -1) Then Begin
;Get the light curve, use all segments at vnbin time resolution
;Split into lt 2048 second groups to avoid time_unit problems, jmm, 10-jan-2003
;2048 is now the default for an option, the idea here is to allow shorter 
;intervals to avoid issues with missing bad packets, jmm, 8-nov-2011
;Changed to 128 seconds, 21-jun-2012, jmm
    dt1 = dt_rates/vnbin
    If(keyword_set(loop_time_intv)) Then t2048 = loop_time_intv $
    Else t2048 = 128.0
    n2048 = ceil((ftr[1]-ftr[0])/t2048)
    jcount = 0
    ltc1 = -1
    For j = 0, n2048-1 Do Begin
;skip out if there are no packets
      ltctr = t2048*j+[0.0, t2048]
      ltctr[1] = ltctr[1] < (ftr[1]-ftr[0])
      ltctr[1] = double(long(ltctr[1]+0.5))
      ntx = (ltctr[1]-ltctr[0])/dt1
      If(ntx Lt 1) Then Continue ;don't do this for partial time intervals
;drop if there are no packets      
      otij = oti[0]+ltctr
      okj = where(pktt[ok100] Ge otij[0] And pktt[ok100] Lt otij[1], nokj)
      If(nokj Eq 0) Then Begin
         message, /info, 'No app_id 100 packets in range:'
         ptim, otij
         ltc1j = -1
      Endif Else Begin
         message, /info, 'PROCESSING: '
         ptim, otij
         oltc1 = obj_new('hsi_spectrum')
         ltc1j = oltc1 -> getdata(obs_time_interval = otij, $
                                  sp_energy_binning = enbins, $
                                  seg_index_mask = bytarr(18)+1, $
                                  sp_time_interval = dt1, sum_flag = 0, $
                                  decimation_correct = 0, $
                                  use_flare_xyoffset = 0, $
                                  _extra=_extra)
         tr1j = oltc1 -> get(/absolute_time_range)
;Spectrum object returns nenergies, ntimes, so we transpose here
         If(size(ltc1j, /n_dimensions) Eq 3) Then ltc1j = transpose(ltc1j, [1, 0, 2])
      Endelse
;Here is what you need to do, you always have to have an array, even
;in the ltc1j=-1 case, also create the array if total(ltc1j)=0, to
;account for bizzare bug for file20050212_02340_002, which may be
;failing on a bad packet...
      If((ltc1j[0] Eq -1) Or (total(ltc1j) Le 0)) Then Begin
        ntx = (ltctr[1]-ltctr[0])/dt1
        ltc1j = fltarr(ntx, nenbins, 18)
        tr1j = ftr[0]+ltctr
        message, /info, 'Bad Light curve: '
        ptim, tr1j
      Endif
      If(jcount Eq 0) Then Begin 
        tr1 = tr1j
        ltc1 = ltc1j
      Endif Else Begin
        ltc1 = [temporary(ltc1), temporary(ltc1j)]
        tr1 = [tr1[0], tr1j[1]]
      Endelse
      jcount = jcount+1
      If(obj_valid(oltc1)) Then obj_destroy, oltc1
    Endfor
;synch the absolute time range up with the oti time range here
    If(ltc1[0] Ne -1) Then Begin
      nltc1 = n_elements(ltc1[*, 0, 0])
      If(notquiet) Then Begin
        print, 'LTC Absolute_time_range: ', $
          anytim(/yoh, tr1[0]), ' -- ', $
          anytim(/yoh, tr1[1])
        print, 'OTI Absolute_time_range: ', $
          anytim(/yoh, oti[0]), ' -- ', $
          anytim(/yoh, oti[1])
        print, 'REF_TIME: ', anytim(/yoh, ref_time)
      Endif
;Ahhhhhhhh!!!! round returns a long integer
      tr1[0] = ref_time+dt1*round((tr1[0]-ref_time)/dt1)
      tr1[1] = tr1[0]+dt1*nltc1
      nltc0 = ntimes_rates*vnbin
      If(total(abs(oti-tr1)) Eq 0.0 And nltc1 Eq nltc0) Then Begin 
        ltc0 = ltc1
      Endif Else Begin      
;           ltc0 = fltarr(nltc0, nenbins, 18)
;to do the synching correctly, embed ltc1, in a larger array, ltcx
        txmin = min([oti, tr1], max = txmax)
        nx = long((txmax-txmin)/dt1)
        ltcx = fltarr(nx, nenbins, 18)
;x1=1st ss of ltc0, in ltcx
;y1=1st ss of ltc1, in ltcx
        y1 = long((tr1[0]-txmin)/dt1)
        ltcx[y1:y1+nltc1-1, *, *] = ltc1
        x1 = long((oti[0]-txmin)/dt1)
        ltc0 = ltcx[x1:x1+nltc0-1, *, *]
        delvarx, ltcx
      Endelse
;now you need to rebin 2 ways, 1 for total rates, and 1 for
;mod_variance
      ltc0 = reform(ltc0, vnbin, ntimes_rates, nenbins, 18)
      rates = total(ltc0, 1)
;Normalize by time
      rates = rates/dt_rates
;The variances, colls 7 and 8, channels in ch0v
      If(n_elements(ch0v) Eq 1) Then lc0 = reform(ltc0[*, *, ch0v[0], *]) $
      Else lc0 = total(ltc0[*, *, ch0v, *], 3)
      rates_mvar = bytarr(2, ntimes_rates)
      s89 = [7, 8]
      For i = 0, 1 Do Begin
        avcts = avsig_1(lc0[*, *, s89[i]], dimension = 1, sigma, /no_zero)
        ok = where(avcts Gt 0 And sigma Gt 0)
        If(ok[0] Ne -1) Then Begin
          temp_rates_mvar = round(10.0*(sigma[ok]^2/avcts[ok]))
          temp_rates_mvar = temp_rates_mvar < 255.0
          rates_mvar[i, ok] = temp_rates_mvar
        Endif
      Endfor
    Endif Else Begin
      If(notquiet) Then message, /info, 'No Valid Light curve'
    Endelse
  Endif Else Begin
    If(notquiet) Then message, /info, 'No Event Packets (app_id=100)'
  Endelse
   
;Monitor rates, can be found in vc1's too, alway pass something out,
;even if there are no packets, jmm, 5-may-2002
  ntimes_npdr = ntimes_rates/npdr ;should always be an integer
  dt_npdr = dt_rates*npdr
  ok = where(pkttbl.app_id Eq 102)
  If(ok[0] Eq -1) Then Begin
    ok152 = where(pkttbl.app_id Eq 152)
    If(ok152[0] Eq -1) Then Begin
      message, /info, 'No Monitor rate packets'
    Endif Else Begin
      ok = ok152
      Goto, got_packets
    Endelse
;define 0 monitor rates
    mr = {particle_rate:fltarr(2), $
          preamp_reset:fltarr(18), $
          shaper_valid:fltarr(18), $
          shaper_over_uld:fltarr(18), $
          delay_line_valid:fltarr(18), $
          live_time:fltarr(18)}
    mon_rates = replicate(mr[0], ntimes_npdr)
  Endif Else Begin              ;got packets
    got_packets: mpak = pkt[ok]
    If(keyword_set(endfileinfo)) Then Begin
      If(datatype(endfileinfo) Eq 'STC') Then Begin
        If(notquiet) Then message, /info, 'Accessing Endfileinfo'
        If(datatype(endfileinfo.monitor_packet) Eq 'STC') Then $
          mpak = [endfileinfo.monitor_packet, mpak]
      Endif
    Endif
;Use hsi_monitor_rate_read directly
    mr_ut_ref = 0
    hsi_monitor_rate_read, packet_array = mpak, mr, nmr, dtmr, $
      time_array = mr_time_array, /alt_struct_output
;sum tag by tag
    mon_rates = replicate(mr[0], ntimes_npdr)
    tags = tag_names(mon_rates)
    ntags = n_elements(tags)
    For i = 0, ntags-1 Do mon_rates[*].(i) = 0
;use histogram with reverse indices.
    histo0 = histogram(mr_time_array, min = ut_ref_rates, $
                       max = ut_ref_rates+dt_npdr*ntimes_npdr, $ 
                       binsize = dt_npdr, reverse_indices = r)
    n_histo0 = n_elements(histo0)
    n_histo0 = n_histo0 < ntimes_npdr
    For j = 0l, n_histo0-1l Do Begin
      If(r[j] Ne r[j+1]) Then Begin
        ss = r[r[j]:r[j+1]-1]
        nnr = float(r[j+1]-r[j])
        If(nnr Gt 1) Then Begin
          For i = 0, ntags-1 Do mon_rates[j].(i) = total(mr[ss].(i), 2)/nnr
        Endif Else Begin
          For i = 0, ntags-1 Do mon_rates[j].(i) = mr[ss[0]].(i)
        Endelse
      Endif
    Endfor
  Endelse

;total rates, divide by the number of front segments used
;keywords
  If(keyword_set(det_index)) Then Begin
    segi = bytarr(18)
    segi[[det_index, det_index+9]] = 1
  Endif Else If(keyword_set(seg_index_mask)) Then Begin
    segi = seg_index_mask
  Endif Else Begin
;Use hsi_qlook_dets2use.pro
    det_index = hsi_qlook_dets2use(ut_ref_rates)
    segi = bytarr(18) & segi[*] = 0
    segi[[det_index, det_index+9]] = 1b
  Endelse
  used = where(segi Eq 1, nused)
  unused = where(segi Eq 0, nunused)
  det_index_l = where(segi[0:8] Eq 1, ndet_l)
  If(det_index_l[0] Ne -1) Then ndet = ndet_l Else ndet = nused
  If(nused Eq 0) Then Begin
    message, /info, 'No Detectors?'
    ndet = 0
    rates_per_det = dblarr(ntimes_npdr, 18)
  Endif Else Begin
;rates_per_det is the total over energy bands at lower cadence for
;each detector segment, keep all rates, 11-mar-2002, jmm
    ntimes_npdr = ntimes_rates/npdr ;should always be an integer
    rates_per_det = total(rates[0:npdr*ntimes_npdr-1, *, *], 2)
    rates_per_det = reform(rates_per_det, npdr, ntimes_npdr, 18)
;rates are normalized by time. so divide by npdr, and not dt_npdr
    rates_per_det = total(rates_per_det, 1)/npdr 
;zero rates for unused detectors, 27-feb-2002, jmm
;but hang on to a copy of the rates array with counts -- for ultimate
;output as rates in the output structure
    rates0 = rates
    rates[*, *, unused] = 0.0
    If(nused Gt 1) Then Begin
      tot_rates = total(rates, 3)/ndet
    Endif Else tot_rates = rates
  Endelse
;Max_det_vs_total flag, for flare_list_fill
  max_det_vs_tot = bytarr(ntimes_rates)
  If(total(tot_rates) Gt 0) Then Begin
    If(ndet Gt 4) Then Begin
      If(n_elements(ch0v) Eq 1) Then $
        rates_ch0v = reform(rates[*, ch0v[0], *]) $
      Else rates_ch0v = total(rates[*, ch0v, *], 2)
      trates_ch0v = total(rates_ch0v, 2)
      For j = 0, ntimes_rates-1 Do Begin
        If(trates_ch0v[j] Gt 0) Then $
          max_det_vs_tot[j] = 100.0*max(rates_ch0v[j, *])/trates_ch0v[j]
      Endfor
    Endif
  Endif
;Packet rate, 13-mar-2002, jmm
  app_ids = hsi_valid_app_ids()
  napp_ids = n_elements(app_ids)
;Set up time array
  prt_time0 = hsi_set_file_time(oti[0], time_intv = dt_prt)
  dt_total = oti[1]-prt_time0
  ntimes_prt = long(dt_total/dt_prt)
  If((dt_total Mod dt_prt) Ne 0.0) Then ntimes_prt = ntimes_prt+1
  prt_time1 = prt_time0+ntimes_prt*dt_prt-1.0d-8 ;to avoid extra bins
  packet_rate = fltarr(napp_ids, ntimes_prt)
  ut_ref_prt = prt_time0
  For j = 0, napp_ids-1 Do Begin
    ok = where(pkttbl.app_id Eq app_ids[j], nok)
    If(nok Gt 0) Then Begin
      If(nok Eq 1) Then Begin
        ttmp = [pktt[ok], pktt[ok]]
        xhisto = 0.5*histogram(ttmp, binsize = dt_prt, $
                               min = prt_time0, max = prt_time1)
      Endif Else Begin
        xhisto = histogram(pktt[ok], binsize = dt_prt, $
                           min = prt_time0, max = prt_time1)
      Endelse
      nxhisto = n_elements(xhisto)
      If(nxhisto Eq ntimes_prt) Then Begin
        packet_rate[j, *] = xhisto/dt_prt
      Endif Else If(nxhisto Lt ntimes_prt) Then Begin
        packet_rate[j, 0:nxhisto-1] = xhisto/dt_prt
      Endif Else Begin
        packet_rate[j, *] = xhisto[0:ntimes_prt-1]/dt_prt
      Endelse
    Endif
  Endfor
;add endfileinfo, if available, and if the start time is less than the
;packet rate end time for the previous file
  If(keyword_set(endfileinfo)) Then Begin
    If(datatype(endfileinfo) Eq 'STC') Then Begin
      If(endfileinfo.prt_endtime[0] Ne -1) Then Begin
        If(notquiet) Then message, /info, 'Accessing Endfileinfo'
        If(ut_ref_prt Lt endfileinfo.prt_endtime) Then Begin
          If(datatype(endfileinfo.packet_rate) Eq 'STC') Then $
            packet_rate[*, 0] = packet_rate[*, 0]+$
            endfileinfo.packet_rate.packet_rate
        Endif
      Endif
    Endif
  Endif
;timing info
  t01 = !Stime
  If(notquiet) Then print, 'Allrates End:  ', t01
  t01 = anytim(t01)
  Elapsed_time = t01-t00
  per_pkt = (elapsed_time/npkt)*1000.0
  If(notquiet) Then Begin
    print, 'Elapsed time: ', elapsed_time, '  s'
    print, 'Per 1000 Packets: ', per_pkt, '  s/1000pkt'
    help, /memory
  Endif
;If i am down here, i have some sort of time range
  hsi_rates_struct = {pkt:pkt, pkttbl:pkttbl, pktt:pktt, $ 
                      rates:rates0, tot_rates:tot_rates, $
                      rates_mvar:rates_mvar, ut_ref_rates:ut_ref_rates, $
                      dt_rates:dt_rates, ntimes_rates:ntimes_rates, $
                      mon_rates:mon_rates, rates_ebands:rates_ebands, $ 
                      seg_index_mask:segi, ndet:ndet, ftr:ftr, $
                      npdr:npdr, rates_per_det:rates_per_det, $
                      packet_rate:packet_rate, dt_prt:dt_prt, $
                      ut_ref_prt:ut_ref_prt, ntimes_prt:ntimes_prt, $
                      max_det_vs_tot:max_det_vs_tot}

End
