;+
; NAME:
;       temp_local_max
; CALLING SEQUENCE:
;       maxs = temp_local_max(array,max_pts,range=range)
; PURPOSE:
;       To find local maxima for a given array. For our purposes,
;       the range of a given maximum must be specified, i.e., if
;       there is a point within + or - range of greater than the
;       given point, then it isn't considered a local maximum.
; INPUT:
;       array = an array
; OUTPUT:
;       maxs = the values of array at the local maximum point.
;       max_pts = the subscripts of the maxima
; KEYWORDS:
;       range = the range of the maximum, the given point is a
;               maximum from max_pt-range to max_pt+range. Default
;                is 10 pts.
; HISTORY:
;       15-FEB-94, JMM
;-
FUNCTION Temp_local_max, array, max_pts, range=range

;Set range of maximum
   IF(KEYWORD_SET(range)) THEN r = range ELSE r = 10
   max_pts = -1
   n = N_ELEMENTS(array)
   n1 = n-1
;You must check each point.
   FOR j = 0, n1 DO BEGIN
      IF(j LT r) THEN ll = 0 ELSE ll = j-r
      IF(j GT n1-r) THEN lu = n1 ELSE lu = j+r
      tmp_max = max(array(ll:lu), jmax)
      jmax = ll+jmax
      IF(jmax EQ j) THEN max_pts = [max_pts, j]
   ENDFOR
   IF(N_ELEMENTS(max_pts) GT 1) THEN BEGIN
      max_pts = max_pts(1:*)
      maxs = array(max_pts)
   ENDIF ELSE BEGIN
      maxs = -1
   ENDELSE
   RETURN, maxs
END
;+
;NAME:
; hsi_flare_vfy
;PURPOSE:
; uses a power spectrum analysis to test to see if a flare is on the
; disk
;CALLING SEQUENCE:
; a = hsi_flare_vfy(tme_range, energy_band, det_index = det_index)
;INPUT:
; time_range =  a time range
; energy_band = an energy band
;OUTPUT:
; a = 0 or 1, 1 means that we have a source that modulates and gives a
;     peak near some multiple of the roll frequency between 0.1 and
;     1.0
;KEYWORDS:
; det_index = if set, use these detectors; it only takes one to give a
;             successful outcome, this may change, the default is to
;             use all detectors.
;HISTORY:
; 26-jan-2017, jmm, jimm@ssl.berkeley.edu
; 30-nov-2017, jmm, Checks for nsigma above local mean, not maximum;
;                   reset default nsigma from 10 to 5.
;-
Function hsi_flare_vfy, time_range, energy_band, det_index = det_index, $
                        df_close = df_close, nsigma = nsigma, _extra = _extra

  a = 0b
  oti0 = anytim(time_range)
  If(~keyword_set(energy_band)) Then energy_band = [6.0, 12.0]
  If(keyword_set(df_close)) Then dfc = df_close[0] Else dfc = 0.01
  If(keyword_set(nsigma)) Then ns = nsigma[0] Else ns = 10.0
;Get power spectra
  p = hsi_fft_pwrspc(oti0, t, f, s, ebin = energy_band, det_index = det_index, _extra = _extra)
  If(n_elements(p) Eq 1 And p[0] Eq -1) Then Return, a
;Also get the spin period
  o = hsi_qlook_roll_period(obs_time_interval = oti0)
  d = o -> getdata()
  If(is_struct(d)) Then Begin
     froll = 1.0/mean(d.roll_period)
  Endif Else Begin
     message, /info, 'No qlook roll period availaable'
     froll = 0.2465 ;Typically the spin is slower that 1/4 Hz
  Endelse
  If(obj_valid(o)) Then obj_destroy, o ;to avoid memory leaking
  ndet = n_elements(p[0,*])
  c = 0
  ftest = froll*[1.0, 2.0, 3.0, 4.0]
  For j = 0, ndet-1 Do Begin
     If(total(p[*,j]) Gt 0) Then Begin
        c = c+1
        ;lower limit for nf probably not needed, but you never know
        If(c eq 1) Then Begin
           nf = (long(0.2/(f[1]-f[0]))) > 10
           xxx = where(f Gt 0.10 ANd f Le 1.0)
        Endif
        ptest = temp_local_max(p[*, j], max_pts, range = nf)
        max_keep = where(max_pts Le max(xxx) And max_pts Gt 0, nkeep)
        If(nkeep Eq 0) Then continue
        max_pts = max_pts[max_keep]
        ptmax = ptest[max_keep]
        ;sort and start from the highest point
        If(nkeep Gt 1) Then Begin
           ssj = bsort(-ptmax) ;sort negative values to reverse index
           ptmax = ptmax[ssj]
           max_pts = max_pts[ssj]
        Endif
        ;Are these maxima near values of ftest (plus or minus df_close)
        ;if so try a median test
        For k = 0, nkeep-1 Do Begin
           fk = f[max_pts[k]]
           Okk= where(fk Gt ftest-dfc And fk Lt ftest+dfc, nokk)
           If(nokk Gt 0) Then Begin
              fk0 = fk - 0.10 & fk1 = fk + 0.10
              ssk = [where((f Gt fk-0.10 And f Lt fk-dfc) Or $
                    (f Gt fk+dfc And f Lt fk +0.10), nssk)]
              If(nssk Eq 0) Then Continue ;should never happen
              threshold = mean(p[ssk, j])+ns*stddev(p[ssk, j])
              If(ptmax[k] Gt threshold) Then Begin
;                 print, j, k, fk, ptmax[k], threshold
;                 hsi_linecolors
;                 plot, f, p[*,j], psym = -1, xrange = [0.1, 1.0]
;                 oplot, [fk-dfc, fk+dfc], [ptmax[k], ptmax[k]], psym = -2, color = 6 & stop
                 a = 1 ;success, you are done
                 Return, a
              Endif
           Endif
        Endfor
     Endif
  Endfor
  Return, a
End

        
