;---------------------------------------------------------------------------
; Document name: hsi_modul_profile__define
; Created by:    Andre Csillaghy, May 1999
;
; Last Modified: Mon Apr 02 13:44:04 2001 (Administrator@TOURNESOL)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI Mod Profile CLASS DEFINITION
;
; PURPOSE:
;       Provides data structures and methods to generate and retrieve
;       back projection maps.
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       o = Obj_New( 'hsi_modul_profile' ) ;
;       or
;       o = HSI_modul_profile()
;
;       The variable o is the object references used to
;       access back projection maps and associated methods.
;
; INPUTS (CONTROL) PARAMETERS:
;
; OUTPUTS (INFORMATION) PARAMETERS:
;
;
; SOURCE OBJECT:
;       HSI_Modul_Pattern
;
; DESTINATION OBJECTS:
;       HSI_Modpat_Products, HSI_Image
;
; EXAMPLE:
;
; SEE ALSO:
;       HESSI Utility Reference
;          http://hessi.ssl.berkeley.edu/software/reference.html
;       hsi_modul_pattern__define
;       hsi_bproj_control__defin
;       hsi_bproj_control
;       hsi_bproj_info__define
;
; HISTORY:
;       Based on the release 2 software of Richard Schwartz
;       Fundamental developments by Richard Schwartz, GSFC
;       Release 3 development, August / September 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;       Release 6 development: introduction of normal, uniform, taper, and
;                              spatial_frequency_weight keywords
;       7-MAY-2017, RAS, added HSI_MODUL_PROFILE_CARTESIAN strategy
;-

;----------------------------------------------------------

FUNCTION HSI_Modul_Profile::INIT, SOURCE = source, _EXTRA=_extra

  IF NOT Obj_Valid( source )  THEN source = HSI_Modul_Pattern()

  strategy_available = ['HSI_MODUL_PROFILE_ANNSEC', 'HSI_MODUL_PROFILE_VISMOD','HSI_MODUL_PROFILE_CARTESIAN']
  ret=self->Strategy_Holder::INIT( strategy_available, $
    SOURCE=source, $
    _EXTRA=_extra )

  self->SetStrategy

  RETURN, ret

END

;---------------------------------------------------------------------------

PRO HSI_Modul_Profile__Define

  bproj = {HSI_Modul_Profile, $
    INHERITS Strategy_Holder_Passive}

END


;---------------------------------------------------------------------------
; End of 'hsi_bproj__define.pro'.
;---------------------------------------------------------------------------
