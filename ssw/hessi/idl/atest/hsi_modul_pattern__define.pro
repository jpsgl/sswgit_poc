;---------------------------------------------------------------------------
; Document name: hsi_modul_pattern__define
; Created by:    Andre Csillaghy, May 1999
;
; Last Modified: Tue Jul 31 17:18:02 2001 (csillag@sunlight)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI MODULATION PATTERN CLASS DEFINITION
;
; PURPOSE:
;       Provides data structures and methods to work with modulation patterns
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       modul_pattern_obj = Obj_New( 'hsi_modul_pattern' ) ;
;
;       The variable modul_pattern_obj is the object references used to
;       access modulation pattern data and methods.
;
;
;
; SEE ALSO:
;      HESSI Utility Reference http://hessi.ssl.berkeley.edu/software/reference.html
;
; HISTORY:
;       Release 6: The hsi_modul_pattern class is the strategy holder
;                  of a couple of imaging methods. It basically just
;                  decides whether to use the annular sector or the
;                  visibilites in the image reconstruction process. ACs
;       Release 4: Really just a wrapper around HSI_Annsec_Map
;       Release 3 development, August / September 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;       Based on the release 2 software of Richard Schwartz
;       Fundamental developments by Richard Schwartz, GSFC
;       7-may-2017, added CARTESIAN imaging_strategy
;       3-aug-2017, AKT, setting need_update in the strategy classes, cart or annsec,
;       when the modul_pattern control param, mpat_control, is changed!! so the caller
;       of modul_pattern knows to update at the change in modul pattern coords
;
;----------------------------------------------------------

FUNCTION HSI_Modul_Pattern::INIT, SOURCE = source, _EXTRA=_extra

  IF NOT Obj_Valid( source ) THEN source = HSI_Calib_Eventlist()

  strategy_available =  ['HSI_ANNSEC_PATTERN', 'HSI_VISMOD_PATTERN', 'HSI_CART_PATTERN']
  ret=self->Strategy_Holder::INIT( strategy_available, $
    CONTROL=hsi_modul_pattern_control(), $
    INFO={hsi_modul_pattern_info}, $
    SOURCE=source, $
    _EXTRA=_extra )

  ;self->SetStrategy, 'HSI_ANNSEC_PATTERN'
  self->Set, mpat_coord = Self->framework::get( /mpat_coord )
  self->Set, IMG_STRATEGY_AVAILABLE = strategy_available, /THIS_CLASS_ONLY

  RETURN, ret

END

;----------------------------------------------------------

PRO HSI_Modul_Pattern::Set, IMAGING_STRATEGY = imaging_strategy, $
  MPAT_COORD = mpat_coord, $
  _EXTRA=_extra, DONE=done, NOT_FOUND=NOT_found
  
  old_mpat_coord = self->get(/mpat_coord)

  mpat_strat = replicate( {hsi_mpat_strat, coord: '', strategy: '' }, 3)
  mpat_strat.coord = ['ANNSEC','CART','VISMOD']
  mpat_strat.strategy = 'HSI_'+ mpat_strat.coord + '_PATTERN'
  
  If keyword_Set( MPAT_COORD ) then begin
    idx = where( stregex( /fold, /boo, mpat_strat.coord, mpat_coord), nidx )
    if nidx eq 1 then imaging_strategy = mpat_strat[ idx ].strategy else $
      print, mpat_coord + ' must be one of ' + mpat_strat.coord   
  Endif

  IF Keyword_Set( IMAGING_STRATEGY ) THEN BEGIN
    ; if we change the imaging strategy,
    ; we need to be sure that the common parameters XYOFFSET, PIXEL_SIZE,
    ; IMAGE_DIM are set in both classes
    control = self->Get( _extra =  {hsi_modul_pattern_strategy_control} ) ;generalize properly, RAS, AKT, 2-aug-2017
    ;control = self->get(/control, /this_class_only, class='hsi_annsec_pattern')
    self->SetStrategy, imaging_strategy
    strat = self->GetStrategy()
    strat->Set, _EXTRA = control
    idx = where( stregex( /fold, /boo, mpat_strat.strategy, imaging_strategy), nidx )
    if nidx eq 1 then begin
      new_mpat_coord = mpat_strat[ idx ].coord    
      if new_mpat_coord ne old_mpat_coord then begin
        Self->framework::set, mpat_coord = mpat_strat[ idx ].coord
        ;Set the need_update directly into the strategy classes because
        ;that's where the strategy_holder->need_update() is going to look!!
        strat->set, /need_update, /this_class_only
      endif      
    endif
    
  ENDIF

  self->Strategy_Holder::Set, IMAGING_STRATEGY = imaging_strategy, $
    _EXTRA=_extra, DONE=done, NOT_FOUND=NOT_found

END

;----------------------------------------------------------

PRO Hsi_Modul_Pattern__Define

  dummy = {Hsi_Modul_Pattern, $
    INHERITS Strategy_Holder }
END

;---------------------------------------------------------------------------
; End of 'hsi_modul_pattern__define.pro'.
;---------------------------------------------------------------------------
