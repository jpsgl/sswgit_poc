;---------------------------------------------------------------------------
; Document name: hsi_psf__define
; Created by:    Andre Csillaghy, May 1999
;
; Time-stamp: <Wed May 12 2004 11:59:39 Administrator CRAPPY3>
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
;
; NAME:
;       POINT SPREAD FUNCTION CLASS DEFINITION
;
; PURPOSE:
;       Provides the data management of the RHESSI point spread functions
;       for a given map coordinate. You give a map pixel location in
;       annsec coordinates and it returns the psf for that particular
;       pixel.
;       Since May 2004, the class returns by default the class summed
;       over detectors. There is a control parameter to get the psfs for
;       individual detectors, but using this control parameters might
;       disrupt the way the imaging algorithms work.
;
; CATEGORY:
;       Imaging (idl/image)
;
; CONSTRUCTION:
;       psf_obj = Obj_New( 'hsi_psf' )
;       psf_obj = HSI_PSF()
;
; (INPUT) CONTROL PARAMETERS DEFINED IN THIS CLASS:
;
;      xy_pixel - the xy position of the pixel for which the psf must
;                 be processed
;      psf_no_sum - if set, the psfs are returned by
;                   detectors. default is 0, i.e. the psfs are summed
;                   over the chosen detectors. This is not a true
;                   control parameter, it does not add to the general
;                   list and must be set specifically for this class,
;                   with for instance
;                   ooo->get( class_name = 'hsi_psf' )
;                   ooo->set, /psf_no_sum
;
; (OUTPUT) INFO PARAMETERS DEFINED IN THIS CLASS:
;
;      none yet
;
; KEYWORDS:
;       THIS_DET_INDEX: use with GetData; retrieves the psf only for
;                       this detector index. Note that the psf is
;                       weighted by the factor determined in the back
;                       projection calculation. Note in particular
;                       that the weighting depends on the choice of
;                       the detectors, with the control parameter
;                       det_index_mask. If you want to get the psf
;                       without weighting (i.e. weighting 1), for
;                       instance for the detector #4,  then it is
;                       advised to set
;                       det_index_mask=[0,0,0,1,0,0,0,0,0] and not set
;                       psf_no_sum at all.
;       THIS_HARMONIC_INDEX: use with GetData; retrieves the psf only
;                            for this harmonic index (fundamental is
;                            0)
;
; SEE ALSO:
;      hsi_psf_control__define
;      http://hessi.ssl.berkeley.edu/software/reference.html
;
; HISTORY:
;     Based on the release 2 software of Richard Schwartz
;     Fundamental developments by Richard Schwartz, GSFC
;     Release 3 development, August / September 1999,
;         A Csillaghy, csillag@ssl.berkeley.edu
;     Release 4: psf separated from modul_pattern object
;                uses annular sectors
;                March / April 2000
;     Release 5.1: fixes for the object updates ACs
;      Release 7: ras, 17-may-02, after the profile is used
;     7-Nov-2002, Kim.  Use self.debug instead of get(/debug) and don't init in init
;     12-May-2004 - acs. The linked list that was used was not
;       working at all, and the psf were summed every
;       time they were called. This has changed. A new
;       cache mechanism is in place and the sum is taken
;       care at the back projection level. The default
;       is to return the summed detectors.
;	     it is freed using ptr_free, sealing a memory leak.
;	17-feb-2010 - ras, removed EMPTY_FLAG
;	25-apr-2007 - ras, removed this_harmonic
;	4-may-2017, ras, bproj object returns a summed fltarr and not a pointer array
;	RAS, 15-jun-2017, allow the modul_profile object to be passed explicitly through _extra, otherwise it isn't available
; and then one or the other profile strategy function must be called directly
; 25-oct-2017, RAS, completed documenting support for MPAT_COORD of 'CART' for Cartesian modul patterns
; 15-nov-2017, RAS, use cbe_corrfactors

;-

PRO hsi_psf_test, xxx=xxx

  o =  HSI_PSF()

  o->set, time_range = ['2002/02/20 11:06', '2002/02/20 11:06:36']
  o->set, pixel_size=[1., 1.], image_dim=[64, 64], xyoffset=[905., 260.], det_index_mask=[0, 0, 1, 1, 1, 1, 1, 0, 0]

  psf =  o->getdata()
  plot_image, psf

  print, 'test the reusing cache: '
  psf =  o->getdata( xy_pixel = [32,32])
  plot_image, psf
  psf =  o->getdata( xy_pixel = [16,16])
  plot_image, psf
  psf =  o->getdata( xy_pixel = [23,33])
  plot_image, psf
  psf =  o->getdata( xy_pixel = [32,32])
  plot_image, psf
  psf =  o->getdata( xy_pixel = [16,16])

  obj_destroy, o

  o =  hsi_image()
  o->set, time_range = ['2002/02/20 11:06', '2002/02/20 11:06:36']
  o->set, pixel_size=[1., 1.], image_dim=[64, 64], xyoffset=[905., 260.], det_index_mask=[0, 0, 1, 1, 1, 1, 1, 0, 0]

  im =  o->getdata( image_alg = 'clean' )

  print, 'test the psf no sum'
  oo = o->get( class_name = 'hsi_psf', /object )

  print, 'this plots the last psf used'
  psf = oo->getdata()
  plot_image, psf

  oo->set, /psf_no_sum
  psff = oo->getdata()



  Print, '------'
  Print, 'Test of correctly upating the psf object'

  o =  HSI_Image( sim_a2d_index= [0,0,0,0,0,1,0,0,0], det_index= [0,0,0,0,0,1,0,0,0] )
  im =  o->getdata()
  psf1 =  o->getdata( class_name= 'hsi_psf', this_det= 5 )
  im =  o->getdata( pixel_size= [1,1] )
  psf2 =  o->getdata( class_name= 'hsi_psf', this_det= 5 )

  zero =   total( psf1-psf2 )
  IF zero ne 0 THEN BEGIN
    print, 'test passsed'
  ENDIF else BEGIN
    print, 'TEST FAILED: this sould NOT be zero: ', ZERO
  ENDELSE


  Obj_destroy, o

  Print, '------'
  Print, 'Tapering tests:'

  o = HSI_PSF()
  psf = o->GetData( det_index_mask=Bytarr(9)+1B )
  Plot, psf[*, 31], xs=3

  psf1 = o->GetData( /UNIFORM )
  OPlot, psf1[*, 31], color=200

  psf2 = o->GetData( TAPER=4 )
  OPlot, psf2[*,31], color=150

END

;----------------------------------------------------------

FUNCTION HSI_Psf::INIT, SOURCE = source

  IF NOT Obj_Valid( source ) THEN source = HSI_BProj( _EXTRA=_extra )

  ret = self->Framework::INIT( CONTROL = HSI_PSF_Control(), $
    SOURCE = source, _EXTRA =_extra )

  self.cache = ptr_new( /alloc )

  RETURN, ret

END

;----------------------------------------------------------

pro hsi_psf::set, psf_no_sum = psf_no_sum, _ref_extra = extra

  ; i dont want this to be a normal control param
  if is_number( psf_no_sum ) then begin
    self.psf_no_sum = psf_no_sum
    self->framework::set, /need_update, /this_class
  endif

  if exist( extra ) then self->framework::set, _extra = extra

end

;----------------------------------------------------------

function hsi_psf::get, psf_no_sum = psf_no_sum, _ref_extra = extra

  if is_number( psf_no_sum ) then begin
    return, self.psf_no_sum
  endif

  if exist( extra ) then return, self->framework::get( _extra = extra )

end

;----------------------------------------------------------

FUNCTION Hsi_Psf::GetData, $
  CLASS_NAME = class_name, $
  THIS_A2D_INDEX = this_a2d_index, $
  THIS_DET_INDEX = this_det_index, $


  _EXTRA = extra

  IF N_Elements( this_a2d_index ) NE 0  THEN this_det_index = this_a2d_index

  IF N_Elements( class_name ) NE 0 THEN $
    IF class_name NE Obj_Class( self ) THEN BEGIN
    RETURN, self->Framework::GetData( CLASS_NAME = class_name, $
      THIS_A2D_INDEX = this_a2d_index, $
      THIS_DET_INDEX = this_det_index, $

      _EXTRA=extra )
  ENDIF

  ; acs revised this part totally in May 2004
  psf=self->Framework::GetData( _EXTRA = extra )

  ; currently psf no sum does not work
  ;if self.psf_no_sum ne 0 then begin
  if 0 then begin
    CheckVar, this_det_index, 0

    RETURN, *psf[ this_det_index ]
  endif else begin
    RETURN, psf
  endelse


END

;----------------------------------------------------------

PRO Hsi_Psf::Process, $
  SUM_PSF = sum_psf, $
  NO_SUM  = NO_SUM, $
  CBE_CORRFACTORS = cbe_corrfactors, $
  _EXTRA =_extra

  xy_pixel = self->Get( /XY_PIXEL )

  PSF_NO_SUM = Self->Get( /PSF_NO_SUM )
  ;If PSF_NO_SUM or NO_SUM is set, then nothing is put into cache
  ;If SUM_PSF is set, it overrides PSF_NO_SUM and NO_SUM
  ;
  default, no_sum, 0
  no_sum = no_sum or psf_no_sum

  if keyword_set( sum_psf ) then no_sum = 0
  ;No_sum is the keyword used with USE_VRATE below

  IF self.debug gt 0 THEN BEGIN
    Message, 'Processing the PSF ' + $
      Arr2Str( String( xy_pixel ) ), /INFO
  ENDIF

  ; this is just to make sure that rmap dim is actual. this is also a potential
  ; source of time sparing
  modul_pattern_obj = self->Get( CLASS_NAME='hsi_modul_pattern', /OBJECT_REFERENCE )
  is_cart = self->Get(/mpat_coord) eq 'CART'
  cmpat = modul_pattern_obj->GetData()
  rmap_dim = modul_pattern_obj->Get( /rmap_dim )
  image_dim = modul_pattern_obj->Get( /image_dim )
  map_dim = is_cart ? image_dim: rmap_dim ;cart or annsec?

  source = self->Get( /SOURCE )

  IF source->need_update() or $
    self->Get( /LAST_UPDATE ) LE source->Get( /LAST_UPDATE ) THEN BEGIN

    ; if we are here we know that the stuff in the source has been reprocessed
    ; that means we need to reset the cache

    ; remember this is a double pointer for psf_no_sum = 1
    if total( ptr_valid( *self.cache ) ) gt 0  then ptr_free, *self.cache
    *self.cache = ptrarr( map_dim[0], map_dim[1] )

  endif

  if n_elements(xy_pixel) eq 1 then begin
    ipix = xy_pixel[0]
  endif else begin
    ipix = ulong(map_dim[0])*xy_pixel[1]+ xy_pixel[0]
  endelse

  ;print, ipix

  if ~no_sum && ptr_valid( (*self.cache)[ipix] ) then begin

    if self.debug gt 0 then begin
      print,'psf ' , xy_pixel, ' is already calculated, reusing it....'
    endif

    psf_ptr = (*self.cache)[ipix]

  endif else begin

    ; IF WE'RE HERE WE MUST CALCULATE THE PSF.

    if self.debug gt 0 then begin
      print,'psf ' , xy_pixel, ' is NOT calculated yet'
      print, '-------------------------------------------- processing psf ', xy_pixel
    endif

    det_index_mask = self->Get( /DET_INDEX_MASK )
    valid = where( det_index_mask, nvalid )
    bproj_obj = self->Get( CLASS_NAME='HSI_BPROJ', /OBJECT_REFERENCE )

    ; just to make sure we got the right map_dim

    inpsf = fltarr(map_dim[0], map_dim[1])

    inpsf[ipix]=1.0
    ;Self->Set, empty_flag=0 ;no longer used, count and livetime field both zero
    ;obviates the need, 17-feb-2010, ras
    ;RAS, 15-jun-2017, passing in the modul_profile object explicitly through _extra, otherwise it isn't available
    if have_tag( _extra, 'OBJ_MODUL_PROFILE' ) then prf9 = _extra.obj_modul_profile->getdata( vimage = inpsf ) else begin
      ;We maintain the direct profile calls in case the obj_modul_profile isn't provided on the getdata call
      if is_cart then begin
        prf9 = ptrarr(9)

        for i= 0, nvalid-1 do prf9[ valid[i] ] = ptr_new( hsi_cart_profile( inpsf, *cmpat[ valid[i] ] ))
      endif else begin
        prf9 = hsi_annsec_profile(inpsf, modul_pattern_obj, $
          det_index_mask=det_index_mask)
      endelse
    endelse
    default, cbe_corrfactors, bytarr( n_elements( prf9 )) + 1b
    for i= 0, nvalid-1 do if cbe_corrfactors[  valid[i] ] ne 1 then *prf9[ valid[i] ] *= cbe_corrfactors[  valid[i] ]
    ; currently pas no sum does not work
    ;	psf = bproj_obj->GetData( VRATE=prf9, NO_SUM=self.psf_no_sum, /POINTER )
    ;	------------- 4-apr-2017, replace call to getdata with call to use_vrate method
    ;	in the bproj strategy class. Calls to GETDATA may cause unwanted reprocessing issues
    ;	in other imaging classes within the current image object
    ;
    psf = bproj_obj->GetData( VRATE=prf9 ) ;4-may-2017, returns a summed map, not a pointer array as before
    ;if is_cart then psf = rot( psf, 180.)
    ; -------------

    ;Done with the profile, free it, ras, 17-may-02
    ptr_free, prf9

    ; the bproj getdata does not return a pointer if the no_sum is not set
    if size(psf, /type) ne 10 then begin
      psf_ptr =  ptr_new( psf)
    endif else begin
      psf_ptr = psf
    endelse
    if ~no_sum then (*self.cache)[ipix] = psf_ptr

  endelse

  self->SetData, psf_ptr

END
;----------------------------------------------------------
Pro Hsi_Psf::Free_Cache
  ptr_free, *self.cache
end
;----------------------------------------------------------

PRO Hsi_Psf__Define

  modul_profile = {HSI_Psf, $
    cache: Ptr_New(), $
    psf_no_sum: 0B, $
    INHERITS Framework}

END


;---------------------------------------------------------------------------
; End of 'hsi_psf__define.pro'.
;---------------------------------------------------------------------------
