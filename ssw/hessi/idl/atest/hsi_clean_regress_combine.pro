;+
;  Name: HSI_CLEAN_REGRESS_COMBINE
;
; :Description:
;    This routine uses a linear regression of the expected counts from the computed image against the observed counts
;    to give a correction coefficient, c_coef.  The addresses a well-known problem in the Clean algorithm that occurs because
;    the basic assumption is of point sources but for solar flares the image is actually not point-like so psf that is used
;    in the cleaning loop will be too intense and thus the normalization too low. This procedure should produce a coefficient
;    greater than 1.
;
; :Params:
;    Self
;
; :Keywords:
;   comp_map = cinfo.clean_component_map
;   resid_map = cinfo.clean_resid_map
;    c_coef - coefficient by linear fit of source map to data
;    r_coef - coefficient by linear fit of profile from resid map to counts - c_coef * profile(source component map)
;    _extra
;
; :Author: raschwar
;  pixscl - if set, units of output image will be pixon-like units (which is what modul_profile can handle)
;           if not set, leave in clean's units
;  18-may-2010, protected against all 0 resid_map in media mode
;  26-aug-2010, split function out from hsi_clean::regress_combine
;  1-apr-2013, ras, first regress the component map by itself. Then apply that coefficient
;   to the component map, compute the profile, subtract it from the counts, and then regress the residual
;   on that residual count rate.
;  15-nov-2017, RAS, added description
;-
function hsi_clean_regress_combine, Self, comp_map = comp_map, resid_map = resid_map, $
   pixscl = pixscl, c_coef = c_coef, r_coef = r_coef,  _extra = _extra

  Self->Set, _extra = _extra
  ;Has the previous method of REGRESS_COMBINE been requested.  Used prior to April 1, 2013
  Old_Regress_Method = Self->Get(/Clean_old_regress_method) >0 < 1
  Rsd_Regress_method = Self->Get(/Clean_rsd_regress_method) >0 < 1;added 6-nov-2017, RAS, regress on resid count rate
  if ~exist(comp_map) or ~exist(resid_map) then begin
    cinfo = self->get(/clean, /info)
    comp_map = cinfo.clean_component_map
    resid_map = cinfo.clean_resid_map
  endif

  ;cc will be all calib eventlist 'count' data from pointers concatenated into one array
  mprof_obj = self->get(/obj, class='hsi_modul_profile')
  cbe_obj = self->get(/obj, class='hsi_calib_eventlist')
  cc = ptr_concat( cbe_obj->getdata(), these_tag='count')

  ;x will be expected profile for component and residual maps [2xn]
  algscl = hsi_alg_scale( self)
  ;PPC is the expected mod profile from the component map
  ppc = mprof_obj->getdata(vimage=comp_map*algscl, /all)
  is_resid = total(abs(resid_map)) ne 0
  is_resid = rsd_regress_method eq 0 ? 0 : is_resid
  ;PPR is the expected mod profile from the residual map
  if is_resid then begin
    ppr = mprof_obj->getdata(vimage=resid_map*algscl, /all)
    x = transpose( [[ppc],[ppr]])
  endif else x = transpose(ppc)

  ; return image in pixon-like units?
  algscl = keyword_set(pixscl) ? algscl : 1.0

  if Old_Regress_Method then begin
    res=regress( x, cc.count, const=cnst, yfit=yfit, chisq=qc) * algscl
    ;b + a0*x0 + a1*x1 = y
    msg = 'OLD method:  Final image = ' + trim(res[0]) + ' * component map' + ' + ' + trim(res[1]) + ' * residual map'
    message, msg, /cont
    return, is_resid ? res[0] * comp_map + res[1] * resid_map : res[0] * comp_map
  endif else begin

    ;First, regress on the Component map profile vs the observation and get C_COEF, the scaling
    c_coef = regress( ppc, cc.count, const=cnst, yfit=yfit, chisq=qc) * algscl
    c_coef = c_coef[0]
    msg = 'Final image = ' + trim(c_coef[0]) + ' * component map'
    ;Finally, regress the PPR, residual profile, against the residual of the counts-C_coef*PPC
    if is_resid then begin
      r_coef = regress( ppr, cc.count-c_coef[0]*ppc, const=cnst, yfit=yfit, chisq=qc) * algscl
      msg = msg + ' + ' + trim(r_coef[0]) + ' * residual map'
    endif
    message, msg, /cont
    ;Send out the results, The regressed sum map or the regressed component map alone
    return, is_resid ? c_coef[0] * comp_map + r_coef[0] * resid_map : c_coef[0] * comp_map
  endelse
end


