;+
; Reads the flare list, does position and qlook images for the first
; flare that has image_status set to 0. Full database management,
; writes altered flare list to hsi_qflare_list.fits files, writes
; images in hsi_qlookimg.fits files, filedb to qlookimg_filedb files.
; And creates the plot files in the Qlook plots directory
; New version, uses hsi_multi_image object for output
; Designed to be called from am opsw script
; Added redo keyword, checks to see if position is screwed up, and
; then will redo the image if necessary, jmm, 17-nov-2003
; Changed grids, resolution (of full-sun) from advice by Ed
; Hacked out of hsi_do_qlook_image, now called from
; hsi_do_qlook_image, to make that program look a little cleaner, jmm,
; 5-nov-2004
; 21-nov-2005, jmm, added no_filedb_write keyword, for testing purposes
;-
Pro hsi_one_qlook_image, data, info, $
                         do_position = do_position, $
                         image_file_dir = image_file_dir, $
                         image_plot_dir = image_plot_dir, $
                         filedb_dir = filedb_dir, $
                         quiet = quiet, $
                         redo = redo, $
                         no_filedb_write = no_filedb_write, $
                         _extra = _extra

;catch
  err_xxx = 0
  catch, err_xxx
  If(err_xxx Ne 0) Then Begin
     catch, /cancel
     Print, 'Error'
     help, /last_message, output = err_msg
     For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
     print, 'Returning'
     Return
  Endif
;Set plot to zbuffer
  set_plot,  'Z'
  device, set_resolution = [500, 500]
  !p.multi = intarr(5)
  loadct, file = loc_file(path = '$SSWDB_HESSI', 'colors_hessi.tbl'), 41
  tvlct, r, g, b, /get
;Set up directories
  If(keyword_set(image_file_dir)) Then idir1 = image_file_dir[0] $
  Else idir1 = '$HSI_QLOOK_IMAGE_FILE_DIR'
  If(keyword_set(image_plot_dir)) Then plotdir = image_plot_dir[0] $
  Else plotdir = '$HSI_QLOOK_IMAGE_PLOT_DIR'
  If(is_struct(data) Eq 0) Then message, 'No data structure input'
  If(n_elements(data) Gt 1) Then message, /info, 'One flare at a time please'
  data0 = data[0]               ;don't change the data yet
  fid = data0.id_number         ;moved here, 2-jun-2010, jmm
  fid_str = strcompress(string(fid), /remove_all)
  date = strmid(time2file(data0.start_time), 0, 8)
;Only do images if the position is ok
  ss_quality = where(info.flag_ids Eq 'POSITION_QUALITY', ss_quality)
  If(ss_quality[0] Eq -1) Then message, 'Bad quality Flag?'
  p_quality0 = data[0].flags[ss_quality[0]] 
;Do you do flare position? If so, you need an obs_summary, so you need
;the file_table
  If(keyword_set(do_position) Or keyword_set(redo)) Then Begin
     fb = hsi_mult_filedb_inp(file_type = 'LEVEL0', $
                              /dont_copy_old_files, $
                              filedb_dir = filedb_dir, $
                              _extra = _extra)
     file_table = hsi_filedb_2_file_table(fb)
;Get the start and end times for this flare
     st_time = data0.start_time
     en_time = data0.end_time
     this_orbit = where(file_table.start_time Le st_time And $
                        file_table.end_time Ge en_time)
     If(this_orbit[0] Eq 0) Then message, 'No File_table times?'
;Now get the obs_summ_soc object
     oti = [file_table[this_orbit].start_time, file_table[this_orbit].end_time]
     oobs = hsi_obs_summ_soc()
     doobs = oobs -> getdata(obs_time_i = oti)
     If(is_struct(doobs) Eq 0) Then message, 'No Obs_Summary Data?'
     hsi_flare_position, data0, info, oobs, spin_axis = avp, $
                         /return_image_obj, image_obj = full_sun_obj, _extra = _extra
     If(keyword_set(redo)) Then Begin          ;Do some checking
        p_quality = data0.flags[ss_quality[0]] ;changed from before
        If(p_quality Gt 0) Then Begin
;If this had good position previously, far from the spin axis, do not redo
           If(p_quality0 Gt 0) Then Begin
              test_xy = total(abs(data0.position-data[0].position))
              If(test_xy Lt 40.0) Then Begin 
                 message, /info, 'Not Reprocessing: '+string(data0.id_number)
                 Return         ;no changes
              Endif
           Endif
        Endif Else Begin
           If(p_quality0 Gt 0) Then Begin
;there are images in existence, you don't want them, you must set the
;filedb.status_flag to -1 and remove the images
              message, /info, 'Good position turned bad, Will delete files'
;Full database management here
              qli_fb = hsi_mult_filedb_inp(filedb_dir = filedb_dir, $
                                           file_type = 'QLOOK_IMAGE', $
                                           quiet = quiet, $
                                           qfiledb_trange = qli_qfb_trange, $
                                           _extra = _extra)
;Do you have a file for this flare id?
              If(is_struct(qli_fb)) Then Begin
                 nqfb = n_elements(qli_fb)
                 fb_fid = strarr(nqfb)
                 For j = 0, nqfb-1 Do Begin
                    ppp = strsplit(qli_fb[j].file_id, '_', /extract)
                    fb_fid[j] = ppp[2]
                 Endfor
                 hfile = where(fb_fid Eq fid And $
                               qli_fb.status_flag Ge 0, nhfile)
                 If(nhfile Gt 0) Then Begin
;Set the status flag to -1, and delete files
                    qli_fb[hfile].status_flag = -1
                    del_file = qli_fb[hfile].file_id
;messy but effective, 
                    del_fsfile = strmid(del_file, 0, 4)+'fs'+strmid(del_file, 6)
                 Endif
              Endif
              file_dir = hsi_mk_dbase_dir(date, idir1)
              If(is_string(del_file)) Then begin
                 del_file = concat_dir(file_dir, del_file)
                 del_fsfile = concat_dir(file_dir, del_fsfile)
              Endif
;Must delete plot files too
              pdir = hsi_mk_dbase_dir(date, plotdir)
              del_pfile0 = 'hsi_*img*'+fid_str+'*'
              del_pfile = concat_dir(pdir, del_pfile0)
              cmd = '/bin/rm '+del_pfile
              message, /info, 'Spawning: '+cmd
              spawn, cmd
           Endif
        Endelse
     Endif
  Endif

  p_quality = data0.flags[ss_quality[0]]
  If(p_quality Gt 0) Then Begin
;Filenames and filedb stuff here, 
     qli_fb = hsi_mult_filedb_inp(filedb_dir = filedb_dir, $
                                  file_type = 'QLOOK_IMAGE', $
                                  quiet = quiet, $
                                  qfiledb_trange = qli_qfb_trange, $
                                  _extra = _extra)
;Do you have a file for this flare id?
     If(is_struct(qli_fb)) Then Begin
        nqfb = n_elements(qli_fb)
        fb_fid = strarr(nqfb)
        For j = 0, nqfb-1 Do Begin
           ppp = strsplit(qli_fb[j].file_id, '_', /extract)
           fb_fid[j] = ppp[2]
        Endfor
        hfile = where(fb_fid Eq fid And $
                      qli_fb.status_flag Ge 0, nhfile)
        If(nhfile Gt 0) Then Begin
           verno = hsi_get_file_verno(qli_fb[hfile].file_id)
           vvv = max(verno, max_verno)
           verno = vvv[0]
           new_verno = verno+1
           qli_fb[hfile].status_flag = -1
           del_file = qli_fb[hfile].file_id
;messy but effective, 
           del_fsfile = strmid(del_file, 0, 4)+'fs'+strmid(del_file, 6)
        Endif Else new_verno = 0
     Endif Else new_verno = 0
;New file, create a directory for the new file, if none exists
     filex0 = 'hsi_qlimg_'+fid_str+'_000.fits' ;image filename
     filex0 = hsi_set_file_verno(filex0, new_verno)
     filexf0 =  strmid(filex0, 0, 4)+'fs'+strmid(filex0, 6) ;full sun filename
     file_dir = hsi_mk_dbase_dir(date, idir1)
     filex = concat_dir(file_dir, filex0)
     filexf = concat_dir(file_dir, filexf0)
     If(is_string(del_file)) Then Begin
        del_file = concat_dir(file_dir, del_file)
        del_fsfile = concat_dir(file_dir, del_fsfile)
     Endif
;First do a position image, and write it out
;2-Nov-2015, jmm, we are running into memory allocation problems, for
;too long intervals - cut to 10 minutes
     otix = anytim(data0.image_time)
     otix = median(otix)+[-300.0d0, 300.0d0]
     message, /info, 'QLOOK IMAGE TIME RANGE'
     ptim, data0.image_time
;Manage detectors to use here, jmm, 2016-01-04
     det_index_fs = hsi_qlook_dets2use(otix[0], /fullsun_image)
     det_mask_in = bytarr(9) & det_mask_in[*] = 0b
     det_mask_in[det_index_fs] = 1b
;But flare position is different, 
     hsi_flare_position_image, otix, [6.0, 25.0], $
                               xy, image_obj = full_sun_obj, $
                               det_mask_in = det_mask_in, $
                               _extra = _extra
     If(obj_valid(full_sun_obj)) Then Begin
        full_sun_obj -> fitswrite, $
           fitsfile = filexf, extname = 'HESSI FULL SUN QLOOK IMAGE'
     Endif
;Now to the qlook_images
     ebands = [[3, 6], [6, 12], [12, 25], [25, 50], [50, 100], $
               [100, 300], [300, 800], [800, 7000], [7000, 20000]]
     nbands = N_elements(ebands[0, *])
     upto = where(ebands[0, *] Eq data0.energy_hi[0])
     upto = upto[0]
     upto = upto > 3
     upto = upto < 6
     im_energy_bin = ebands[*, 0:upto]
;Get ok detectors
     det_index_ql = hsi_qlook_dets2use(otix[0], /qlook_image)
     dmask = bytarr(9) & dmask[*] = 0
     dmask[det_index_ql] = 1b
;Now zoom in at the energy ranges
     data_obj = hsi_image()
     data_obj -> set, progress_bar = 0, $
                      image_alg = 'clean', $
                      im_time_interval = otix, $
                      im_energy_binning = im_energy_bin, $
                      pixel_size = [3., 3.], $
                      image_dim = [64, 64], $
                      xyoffset = data0.position, $
                      det_index_mask = dmask, $
                      im_out = filex
;tweaking
     hsi_qlook_image_twk, data_obj, /ql_image
     data_obj -> set_no_screen_output
     d = data_obj -> getdata()
;Ok, now in principle, you have the file(s)
     If(findfile(filexf) Eq '') Then Begin
        message, 'File: '+filexf+ ' not written'
     Endif
     If(findfile(filex) Eq '') Then Begin
        message, 'File: '+filex+ ' not written'
     Endif
;Get a new filedb structure
     qli_fb_new = hsi_filedb_version_control()
     qli_fb_new.file_id = filex0
;You need a time range, use the flare time range
     qli_fb_new.start_time = data0.start_time
     qli_fb_new.end_time = data0.end_time
;Next, append the filedb
     If(is_struct(qli_fb)) Then Begin
        qli_fb = [qli_fb, qli_fb_new]
     Endif Else qli_fb = qli_fb_new
;Only output for this time range and qtime_range
     If(qli_qfb_trange[0] ne -1 And total(qli_qfb_trange) Gt 0) Then Begin
        all_times = [qli_qfb_trange, qli_fb_new.start_time, $
                     qli_fb_new.end_time]
     Endif Else all_times = [qli_fb_new.start_time, qli_fb_new.end_time]
     all_time_range = [min(all_times), max(all_times)]
;Write the new filedb, if asked for
     If(Not keyword_set(no_filedb_write)) Then Begin
        hsi_filedb_sort, qli_fb
        ok = where(qli_fb.status_flag Ge 0, nok)
        If(nok Gt 0) Then Begin
           hsi_filedb_write, qli_fb[ok], filedb_dir = filedb_dir, $
                             filename = 'hsi_qlimg_filedb.fits', $
                             filedb_trange = all_time_range, /temp
           fdbdir_pub = '$HSI_FILEDB_PUB'
           hsi_filedb_write, qli_fb[ok], filedb_dir = fdbdir_pub, $
                             filename = 'hsi_qlimg_filedb.fits', $
                             filedb_trange = all_time_range, /temp
        Endif
     Endif
;Now the image plots, 1 file per image
     pdir = hsi_mk_dbase_dir(date, plotdir)
;First the full_sun_plot
     If(obj_valid(full_sun_obj)) Then Begin
        fs_pfile0 = 'hsi_fsimg_'+fid_str+'.png'
        fs_pfile = concat_dir(pdir, fs_pfile0)
        full_sun_obj -> plot, /show_atten, /limb_plot, /cbar      
        fsa = full_sun_obj -> get(class_name = 'hsi_aspect_solution', $
                                  /object_ref)
        avp = rhessi_get_spin_axis_position(otix, $
                                            aspect_obj = fsa)
        xyouts, avp[0], avp[1], '+', charsize = 1.5, alignment = 0.5
        label_size = ch_scale(.8, /xy)
        ll = strcompress(round(data0.position), /remove_all)
        tmpp = 'Flare Position = ['+ll[0]+','+ll[1]+']'
        ssw_legend, tmpp, /bottom, /right, box = 0, charsize = label_size
        ll = strcompress(round(avp), /remove_all)
        tmpp = 'Spin Axis = ['+ll[0]+','+ll[1]+']'
        ssw_legend, tmpp, /bottom, /left, box = 0, charsize = label_size
        write_png, fs_pfile, tvrd(), r, g, b
     Endif
;Next the multi_image plots
     If(obj_valid(data_obj)) Then Begin
        img_pfile0 = 'hsi_qlimg_'+fid_str
        img_pfile = concat_dir(pdir, img_pfile0)
        hsi_imagefile2png, fitsfile = filex, file_prefix = img_pfile, $
                           /limb_plot, /cbar, /show_atten, /find_peak, _extra = _extra
     Endif
  Endif Else message, /info, 'No good flare position'
;you're done, remember to reset the data structure, as the position
;may have changed
  data[0] = data0
;Delete files, but only if you aren't testing
  If(Not keyword_set(no_filedb_write)) Then Begin
     If(is_string(del_file)) Then Begin
        If(is_string(del_fsfile)) Then del_file = [del_file, del_fsfile]
        dir_old = '$HSI_OLD_LEVEL0_DIR'
        For j = 0, n_elements(del_file)-1 Do Begin
           cmd = '/bin/rm '+del_file[j]
           If(Not keyword_set(quiet)) Then $ 
              message, /info, 'Spawning: '+cmd
           spawn, cmd
        Endfor
     Endif
  Endif
;destroy objects
  If(obj_valid(full_sun_obj)) Then obj_destroy, full_sun_obj
  If(obj_valid(data_obj)) Then obj_destroy, data_obj
  If(obj_valid(oobs)) Then obj_destroy, oobs
  free_all_lun
  heap_gc
  Return
End
