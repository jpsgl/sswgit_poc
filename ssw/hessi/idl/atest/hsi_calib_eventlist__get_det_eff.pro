;+
;Used to manage the correction factors for each grid/detector combination
;in this energy range.  cbe_corr_factor is an info parameter, it
;should be a control parameter, get(/cbe_det_eff) should go through
;get_det_eff, ras, 29-apr-2010
;24-mar-2017, ras, now only use corr factors if cbe_normalize is set
;  make analogous to hsi_vis_normalize
; 9-nov-2017, ras, disabling this so it just returns normal cbe_det_eff
;-
function hsi_calib_eventlist::get_det_eff, $
  det_index_mask=det_index_mask, $
  _extra=_extra


  self->set, _extra=_extra
  if not exist(det_index_mask) then det_index_mask= Self->get(/det_index_mask)
  det_list = where( det_index_mask[*,0], n_det)
  cbe_det_eff = self->framework::get(/cbe_det_eff)
  cbe_normalize = Self->framework::Get(/cbe_normalize)
  Self->set, cbe_corr_factors = fltarr(9) + 1.0

  return, cbe_det_eff
end