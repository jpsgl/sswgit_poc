;+
;
; Name: hsi_read_soh_txt
; 
; Purpose: Find and read a text file of 1-day averages of SOH parameters covering the full RHESSI mission.
;   Looks for file first in keyword dir (if provided), then in proper directory under HSI_DATA_ARCHIVE, and if
;   not found, copies the file from the hesperia to your computer (to dir directory if keyword provided).
;   
; Input Keywords:
;  tag - tag name of SOH parameter (first column in file $SSW/hessi/idl/util/soh_tag_table.txt)
;  dir - if provided, looks here first for SOH file.  If copies file from archive, copies to dir.
;  time_range - time range of returned data in anytim format. If not provided, returns all data from file.
;  
; Output Keywords:
;  label - string containing label for data 
;  
; Output:
;  An array of structures containing items from the four columns in the SOH text files:
;  time - day of data point in sec since 1979
;  ave - average value of parameter for the day
;  sigma - sigma on ave value
;  npack - number of packets
;
; 
; Written: Kim Tolbert 14-May-2013
; Modifications:
;-

function hsi_read_soh_txt, tag=tag, dir=dir, time_range=time_range, label=label

checkvar, tag, 'icp2t'

checkvar, dir, curdir()
arch_dir = concat_dir(chklog('HSI_DATA_ARCHIVE'),'metadata/hsi_1day_sohdata/data')

arch_url = 'http://hesperia.gsfc.nasa.gov/hessidata/metadata/hsi_1day_sohdata/data/'

filename = 'hsi_1day_'+strlowcase(trim(tag))+'.txt'
file = concat_dir(dir, filename)
if ~file_test(file,/read) then begin
  file = concat_dir(arch_dir, filename)
  if ~file_test(file,/read) then begin
    if ~file_test(dir,/dir) then file_mkdir,dir
    sock_copy, arch_url+filename, out_dir=dir, local_file=local_file, /use_network, err=err
    if err ne '' then begin
      message, /cont, 'Error retrieving file ' + arch_url+filename
      return, -1
    endif
    message,/cont,'Copied file ' + local_file
    file = local_file
  endif
endif

a = rd_tfile (file, /hskip, /auto, header=header)

label = ssw_strsplit(header[1],':',/tail)

time = reform(anytim(a[0,*]))
ave = reform(float(a[1,*]))
sigma = reform(float(a[2,*]))
npack = reform(float(a[3,*])) * 8640.

if keyword_set(time_range) then begin
	time_in = anytim(time_range)
	q = where (time ge time_in[0] and time le time_in[1], count)
endif else q = indgen(n_elements(time))

z = {time: time[q], ave: ave[q], sigma: sigma[q], npack:npack[q]}
return, reform_struct(z)

end
