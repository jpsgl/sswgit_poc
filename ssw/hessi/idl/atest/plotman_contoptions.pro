; widget interface to set plotman contour options
; Written: Kim Tolbert, 21-Feb-2004

pro plotman_contoptions_widget_update, state

widget_control, state.w_color, set_droplist_select = *state.color - state.ncolors - 1
widget_control, state.w_thickness, set_value=*state.thickness
widget_control, state.w_style, set_droplist_select = *state.style
widget_control, state.w_label, set_value=*state.label
widget_control, state.w_translate, set_value=*state.translate
if xalive(state.w_rotate) then widget_control, state.w_rotate, set_value=*state.rotate
widget_control, state.w_percent, set_value=*state.percent

if *state.n_levels gt 0 then begin
	levels = trim (string ((*state.levels)[0:*state.n_levels-1], format='(f12.1)'))
	widget_control, state.w_level_text, set_value='Levels: ' + arr2str(levels,',')
endif else begin
	widget_control, state.w_level_text, set_value='Levels are autodefined'
endelse

end

;-----

pro plotman_contoptions_event, event

widget_control, event.top, get_uvalue=state
widget_control, event.id, get_uvalue=uvalue

exit = 0
case uvalue of

	'color': *state.color = event.index + state.ncolors + 1

	'style': *state.style = event.index

	'contour_auto': begin
		if event.select then begin
			*state.n_levels = 0
			*state. levels = *state.levels * 0
		endif
		end

	'contour_define': begin

		if event.select then begin
			image = state.pobj -> get(/saved_data_data)
			dtype = size(image, /type)
			if dtype gt  0 and dtype lt 6 and size(image,/n_dim) eq 2 then begin
				range = strtrim (string (minmax (image), format='(f12.2)'), 2)
				if *state.n_levels gt 0 then begin
					levels = trim (string ((*state.levels)[0:*state.n_levels-1], format='(f12.1)'))
					text = arr2str(levels, ',')
				endif else text = ''
				widget_offset, event.top, xoffset, yoffset
				if *state.percent then $
					instruct =['Enter contour level as percent of maximum separated by commas', $
						'  e.g. 10,20,50,80'] $
				else $
					instruct = ['Enter contour levels separated by commas', $
						'  e.g. 200,400,600']
				xinput, text, $
					[' ', $
					'Range of image data = ' + range[0] + ' - ' + range[1], $
					' ', $
					instruct[0], $
					instruct[1], $
					' '], $
					group=event.top, xoff=xoffset,yoff=yoffset, ysize=1, max_len=50, status=status, /modal

				; if clicked Accept, and text in box changed, then get new contour levels
				if status and text ne '' then begin
					arr = str2arr (text, ',')
					*state.n_levels = n_elements (arr)
					(*state.levels)[0:*state.n_levels-1] = float(arr)
				endif
			endif else begin
				err_msg =  "Can't compute range of data"
				print,err_msg
				a = dialog_message(err_msg)
			endelse

		endif
		end

	'cancel': begin
		*state.cancel = 1
		exit = 1
		end

	'accept': exit = 1

	else:

	endcase

widget_control, state.w_thickness, get_value = thickness & *state.thickness = thickness
widget_control, state.w_label, get_value = label & *state.label = label
widget_control, state.w_translate, get_value = translate & *state.translate = translate
if xalive(state.w_rotate) then begin
	widget_control, state.w_rotate, get_value = rotate & *state.rotate = rotate
endif
widget_control, state.w_percent, get_value = percent & *state.percent = percent

widget_control, state.w_base, set_uvalue=state

if exit then begin
	widget_control, event.top, /destroy
	return
endif

plotman_contoptions_widget_update, state

end

;-----

function plotman_contoptions, group=group, plotman_obj=pobj, overlay=overlay, ov_number=ov_number

overlay = keyword_set(overlay)
checkvar, ov_number, 0

if overlay then $
	struct = { $
		overlay_color: pobj -> get(/overlay_color), $
		overlay_thickness: pobj -> get(/overlay_thickness), $
		overlay_style: pobj -> get(/overlay_style), $
		overlay_label: pobj -> get(/overlay_label), $
		translate_overlay: pobj -> get(/translate_overlay), $
		drotate_image: pobj -> get(/drotate_image), $
		n_overlay_levels: pobj -> get(/n_overlay_levels), $
		overlay_levels: pobj -> get(/overlay_levels), $
		overlay_percent: pobj -> get(/overlay_percent) }

color = overlay ? struct.overlay_color[ov_number] : pobj->get(/contour_color)
thickness = overlay ? struct.overlay_thickness[ov_number] : pobj->get(/contour_thickness)
style = overlay ? struct.overlay_style[ov_number] : pobj -> get(/contour_style)
label = overlay ? struct.overlay_label[ov_number] : pobj -> get(/contour_label)
translate = overlay ? struct.translate_overlay[*,ov_number] : pobj -> get(/translate_image)
rotate = overlay ? struct.drotate_image[ov_number] : 0
n_levels = overlay ? struct.n_overlay_levels[ov_number] : pobj -> get(/n_contour_levels)
levels = overlay ? struct.overlay_levels[*,ov_number] : pobj -> get(/contour_levels)
percent = overlay ? struct.overlay_percent[ov_number] : pobj -> get(/contour_percent)

colorstr = pobj -> get(/color_names)
ncolors = pobj -> get(/ncolors)
linestyles = ['Solid', 'Dotted', 'Dashed', 'Dash Dot', 'Dash Dot Dot', 'Long Dashes']

what = overlay ? ' for Overlay ' + trim(ov_number+1) : ' for Primary Contour'
w_base = widget_base ( group_leader=group, $
					/column, $
					xpad=5, $
					ypad=0, $
					space=10, $
					title='Contour Options' + what, $
					/frame, $
					/modal )

temp = widget_label (w_base, value='Contours Options' + what, /align_center)

w_base1 = widget_base (w_base, /row, space=15)

w_color = widget_droplist (w_base1, $
					title='Color: ', $
					value=tag_names(colorstr), $
					uvalue='color')

w_thickness = cw_field (w_base1, $
					title='Thickness: ', $
					value='', $
					xsize=5, $
					/return_events, $
					uvalue='thickness')

w_style = widget_droplist (w_base1, $
					title='Line Style: ', $
					value=linestyles, $
					uvalue='style')

w_label = cw_bgroup (w_base1, 'Label', uvalue='label', /nonexclusive)


w_base2 = widget_base (w_base, /row, space=10)

w_translate = cw_range (w_base2, $
						uvalue='translate', $
						value=[0.,0.], $
						format='(g8.2)', $
						xsize=8, $
						label1='Translate X: ', $
						label2='  Y:', space=0 )

w_rotate = 0L
if keyword_set(overlay) then w_rotate = cw_bgroup(w_base2, 'Solar Rotate', uvalue='rotate', /nonexclusive)

w_base3 = widget_base (w_base, /column, /frame)
w_base3a = widget_base (w_base3, /row, space=10)

tmp = widget_button (w_base3a, $
					value='Autoselect levels', $
					uvalue='contour_auto' )

tmp = widget_button (w_base3a, $
					value='Define levels', $
					uvalue='contour_define' )

w_percent = cw_bgroup (w_base3a, 'As % of max', uvalue='contour_percent', /nonexclusive)

w_base3b = widget_base (w_base3, /row, space=10)

w_level_text = widget_label (w_base3b, $
						value='   ', $
						/align_left, $
						xsize=250 )

w_base4 = widget_base (w_base, /row, space=30, /align_center)

tmp = widget_button (w_base4, value='Accept', uvalue='accept')
tmp = widget_button (w_base4, value='Cancel', uvalue='cancel')

widget_offset, group, newbase=w_base, xoffset, yoffset

widget_control, w_base, xoffset=xoffset, yoffset=yoffset

widget_control, w_base, /realize

state = {w_base: w_base, $
	w_color: w_color, $
	w_thickness: w_thickness, $
	w_style: w_style, $
	w_label: w_label, $
	w_translate: w_translate, $
	w_rotate: w_rotate, $
	w_percent: w_percent, $
	w_level_text: w_level_text, $
	pobj: pobj, $
	colorstr: colorstr, $
	ncolors: ncolors, $
	linestyles: linestyles, $

	color: ptr_new(color), $
	thickness: ptr_new(thickness), $
	style: ptr_new(style), $
	label: ptr_new(label), $
	translate: ptr_new(translate), $
	rotate: ptr_new(rotate), $
	n_levels: ptr_new(n_levels), $
	levels: ptr_new(levels), $
	percent: ptr_new(percent), $

	cancel: ptr_new(0) }

widget_control, w_base, set_uvalue=state

plotman_contoptions_widget_update, state

xmanager, 'plotman_contoptions', w_base, group=group

if *state.cancel then return, -1

if overlay then begin
	struct.overlay_color[ov_number] = *state.color
	struct.overlay_thickness[ov_number] = *state.thickness
	struct.overlay_style[ov_number] = *state.style
	struct.overlay_label[ov_number] = *state.label
	struct.translate_overlay[*,ov_number] = *state.translate
	struct.drotate_image[ov_number] = *state.rotate
	struct.n_overlay_levels[ov_number] = *state.n_levels
	struct.overlay_levels[*,ov_number] = *state.levels
	struct.overlay_percent[ov_number] = *state.percent

endif else begin
	struct = { $
		contour_color: *state.color, $
		contour_thickness: *state.thickness, $
		contour_style: *state.style, $
		contour_label: *state.label, $
		translate_image: *state.translate, $
		n_contour_levels: *state.n_levels, $
		contour_levels: *state.levels, $
		contour_percent: *state.percent }
endelse

return, struct

end