;+
; Name: hsi_image_profile_plot
;
; Purpose: Plot profiles from generated image with observed profiles and
;   display goodness of fit and regression coefficients.
;
; Input:
; obj - hsi_image_single or hsi_image_alg (or an alg class, e.g. clean, pixon) object
;
; Input Keywords:
; force - if set, go ahead and reprocess if necessary without asking
; vimage - image to get profile for in annsec coords (unless mpat_coord is set to 'cart', then vimage should
;   be in cartesian coords, i.e. should match modpat coords).  If obj is an hsi_modul_profile object
;   then, vimage must be supplied.  If not supplied, vimage will be retrieved from object. Note that if
;   you're calling this WHILE image is being made (e.g. from hsi_image_single), then vimage must be supplied
;   since can't do a getdata here without getting in infinite loop.
; rate - if set, plot profiles in counts/sec
; resid - If set, plot resids of profiles  = (Obs - exp) / sqrt(exp)
; ps - If set, send plot to PS file
; jpeg - If set, send plot to jpeg file
; file_plot - plot file name (default is image_profile_xxx.yyy, where xxx is alg)
; dir_plot - directory to use for output plot file (default is profile_plot_dir parameter, or if that's blank, current dir)
; window - window number to use.  If not set, will create one of correct size.
;
; Output Keywords:
; prof_fit - structure containing fit c-statistic and coefficents (see
;   hsi_image_profile_cstat.pro header doc for details)
; xp - expected profiles for each selected detector (array of pointers)
; out_struct - ptr array to structure containing xval, xp, obs, ltim, and resid for each selected detector
; status - 0/1 means failed / succeeded to get image profiles
;
; Output: plot on screen or in PS file
;
; Written: Kim Tolbert April 2010
; Modifications:
; 18-aug-2010, Kim.  Make sure y dimension of plot isn't bigger than .95 time y dimension of screen
; 23-Aug-2010, Kim.  Use xp returned by hsi_image_profile_cstat (which is not modifying it to normalize
;   for vis algs) instead of getting it via getdata again here.
; 20-Sep-2010, Kim. Changed y label from 'Sigma' to 'Sigma (c/bin)' for residual plot
; 28-Feb-2011, Kim. Free xp at end if not passed out
; 26-Nov-2011, Kim. Call al_legend instead of legend (IDL V8 conflict)
; 7-Nov-2012, RAS. Changed Roll angle lable to Regularized Roll Angle
; 24-Jul-2015, Kim. Added force and out_struct keywords
; 17-Feb-2017, Kim. Added dir_ps keyword. Generate a better PS file name if one isn't passed in.
;   Previously was image_profile_xxx.ps, where xxx is alg,
;   Now profile_xxx_t0_e0-e1kev.ps where xxx is alg, t0 is start time, e0,e1 is energy bin
; 24-Jan-2018, Kim. Added F or R to x titles to indicate front /rear detector
; 14-Mar-2018, Kim. If plot dev is already PS, then don't open or close PS file, just adding to a PS file.
; 08-May-2018, Kim. Added jpeg option. Changed file_ps keyword to file_plot.  Previously setting file_ps enabled PS mode, now it
;   doesn't.  Changed dir_ps keyword to dir_plot. 
; 12-Jul-2018, Kim. Use ssw_legend instead of al_legend (sometimes has color issues)
; 31-Jul-2018, Kim. Call hsi_image_profile_cstat with resid_removed output keyword so we can add that to label. Also 
;   adjusted character sizes to improve appearance.
;-

pro hsi_image_profile_plot, obj, force=force, vimage=vimage, $
  rate=rate, resid=resid, $
  ps=ps, jpeg=jpeg, file_plot=file_plot, dir_plot=dir_plot, window=pwindow, $
  prof_fit=prof_fit, xp=xp, out_struct=out_struct, status=status, _extra=_extra

  if is_string(obj -> get(/im_input_fits)) then begin
    msg = 'Can not show profile plots when input is image FITS file.  Aborting.'
    answer = dialog_message(msg, /info)
    return
  endif

  if ~keyword_set(force) then begin
    if obj -> need_update() then begin
      msg = 'Image(s) and / or CBEs have not been generated yet.  Are you sure you want to do that now?'
      answer = dialog_message(msg, /question)
      if answer eq 'No' then return
    endif
  endif

  self = obj

  checkvar, rate, self->get(/profile_plot_rate)
  checkvar, resid, self->get(/profile_plot_resid)
  checkvar, pwindow, self->get(/profile_window)
  checkvar, ps, self->get(/profile_ps_plot)
  checkvar, jpeg, self->get(/profile_jpeg_plot)
  checkvar, dir_plot, self->get(/profile_plot_dir)

  if is_class(self, 'hsi_image') then begin
    save_use_single = self -> get(/use_single_return_mode)
    self -> set, /use_single_return_mode
  endif

  prof_fit = hsi_image_profile_cstat(self, vimage=vimage, scaled_vimage=scaled_vimage, xp=xp, $
    status=status, resid_removed=resid_removed, _extra=_extra)

  if status eq 0 then goto, getout

  ;if self is image_alg, and is vis, then mod_profile isn't available from it - have
  ; to go a level up to image_single.  hsi_image_get_vimage made sure a module_profile
  ; class is available at that level, even for vis algs.  However caller wasn't set
  ; if not vis, so only call getcaller for vis algs.
  alg_struct = hsi_algorithm_units(obj, /used, status=status)
  if alg_struct.is_vis and is_class(self, 'hsi_image_alg') then self = self->getcaller()

  cbe = self -> getdata(class='hsi_calib_eventlist')

  ind = where(ptr_valid(cbe), ndet)
  front = self->get(/front_segment)
  front_rear = front ? 'F' : 'R'
  ;ind=[4,5]&ndet=2

  gp=hsi_grid_parameters()
  alg_struct = hsi_algorithm_units(self, /used, status=status)
  if ~status then goto, getout
  alg = alg_struct.name
  alg_prefix = hsi_get_alg_name(alg_struct.class,/prefix)
  if alg_prefix eq '' then alg_prefix = 'bproj'

  add_to_ps = !d.name eq 'PS'
  ps = keyword_set(ps) or add_to_ps
  jpeg = ~ps and keyword_set(jpeg)

  tvlct,rr,gg,bb,/get
  thisdevice = !d.name
  loadct,0,/silent
  linecolors
  save_multi = !p.multi
  !p.multi = [0,1,ndet+1]
  charsz = ndet gt 1 ? 2 : 1 ; charsize shrinks with > 2 multi plots, so start bigger
  checkvar, leg_size, 1.2
  thick = 1.
  bw = 255

  if ps or jpeg then begin
    
    charsz = ndet gt 1 ? 1.5 : .8
    leg_size = .9  
    if ~is_string(file_plot) then begin
      t_int = self->get(/im_time_interval)
      e_int = self->get(/im_energy_binning)
      t_ind = self->get(/tb_index)
      e_ind = self->get(/eb_index)
      t0 = time2file(t_int[0,t_ind],/sec)
      e0=trim(e_int[0,e_ind])
      e1=trim(e_int[1,e_ind])
      file_plot = 'profile_'+alg_prefix+'_'+t0+'_'+e0+'-'+e1+'kev.'+ (ps ? 'ps' : 'jpeg')
    endif
    if is_string(dir_plot) then begin
      if ~is_dir(dir_plot) then file_mkdir, dir_plot
      full_file_plot = concat_dir(dir_plot, file_plot)
    endif else full_file_plot = file_plot    
    if ps then begin
      if ~add_to_ps then ps, full_file_plot, /port,/color
      thick = 3.
      bw = 0
    endif
    if jpeg then begin
      xsize=600
      ysize=800
      set_plot, 'z'
      device, set_resolution=[xsize, ysize]
    endif
    
  endif else begin
    
    save_window = !d.window
    if ~is_wopen(pwindow) then begin
      pwindow = next_window(/user)
      device, get_screen_size=sc
      window, pwindow, xsize=700, ysize=(900 < .95*sc[1])
    endif
    wset, pwindow
    self->set, profile_window=pwindow
    
  endelse

  abs_time_range = self -> get(/absolute_time_range)

  time_string = format_intervals(abs_time_range, /ut)
  en_string = format_intervals(self -> get(/im_eband_used), format='(f12.1)') + ' keV'
  cstat_string = 'C stat: ' + trim(prof_fit.tot_cstat, '(f12.3)')
  pixel_size = arr2str(trim(self -> get(/pixel_size)), 'x') + ' arcsec'
  xyoffset = arr2str(trim(self -> get(/xyoffset),'(f8.2)'), ', ') + ' arcsec'
  image_dim = arr2str(trim(self -> get(/image_dim)), 'x') + ' pixels'

  use_phz = self->get(/use_phz_stacker)
  nphz = self -> get(/phz_n_phase_bins)

  tdur = hsi_get_time_bins(self, /seconds)

  ;xr = [1.e20,1.e-20]
  ;if ~use_phz then begin
  ;  for i = 0,ndet-1 do begin
  ;    ii = ind[i]
  ;    rollang = ( (*cbe[ii]).roll_angle - gp[ii].orient ) * !radeg
  ;    print,'Det ii, minmax of rollang ', ii,minmax(rollang)
  ;    xr = [xr[0] < min(rollang), xr[1] > max(rollang)]
  ;  endfor
  ;  x0 = xr[0] - (xr[0] mod 360.)
  ;  xr = xr - x0
  ;  print,'x0 = ', x0, '  overall minmax = ', xr
  ;endif

  out_struct = ptrarr(9, /alloc)

  for i = -1,ndet-1 do begin

    case 1 of
      i eq -1: begin
        plot,[0,1],[0,1],/nodata,xstyle=4,ystyle=4
        text = [alg + ' ' + en_string + ' ' + cstat_string,  time_string, pixel_size + ' ' + image_dim]
        if resid_removed then text = [text,'Residual Removed']
        ssw_legend,text,/left,box=0,chars=leg_size
        text2 = keyword_set(resid) ? ['Residuals'] : ['Observed Profile', 'Image Profile']
        color2 = keyword_set(resid) ? bw : [2,bw]
        ssw_legend, text2, /right, /top, box=0, color=color2, lines=0, chars=leg_size
        goto, next
      end
      i lt (ndet-1): begin
        ymargin = [0.,2.]
        xtitle = ''
        xtickname = strarr(30)+' '
      end
      i eq (ndet-1): begin
        ymargin = [4.,2.]
        xtitle = use_phz ? 'Regularized Roll Angle (degrees)' : 'Time (sec)'
        delvarx, xtickname  ; get rid of variable, so will use default tick names
      end
    endcase

    ii = ind[i]

    ; If phase stacker used, the x axis will be roll angle corrected for the offset of
    ; each detector and modulo 360.  Otherwise, xaxis will be time
    ; (tdur is time duration of each interval, so just multiply by lindgen(npoints))
    if use_phz then begin
      w = (*cbe[ii])[nphz].roll_angle - (*cbe[ii])[0].roll_angle
      ncbe = n_elements(*cbe[ii])
      rollang =  ( (*cbe[ii]).roll_angle + rebin(findgen(nphz) * w/nphz, nphz, ncbe) )
      ; Align the profiles so that 0 will produce modulation for a source on the limb at an
      ; xyoffset of [960, 0.] arcseconds or [-960, 0.].  Detector 8's nominal orientation is
      ; 90., so use its real orientation.  720 is just to ensure positive.

      xval = ( (( rollang + gp[ii].orient - gp[8].orient) * !radeg ) + 720.) mod 360.
      xtickinterval = 90.
      if i eq 0 then xrange = minmax(xval)

    endif else begin
      ;    rollang = (*cbe[ii]).roll_angle
      ;    xval = (( rollang - gp[ii].orient ) * !radeg ) - x0
      ;    if i eq 0 then xrange = xr
      ;    print,'  minmax = ', minmax(xval)
      xval = tdur[ii] * lindgen(n_elements(*xp[ii])) + tdur[ii]/2.
      if i eq 0 then xrange = minmax(xval)
    endelse

    ;  xval = ( (( rollang - gp[ii].orient ) * !radeg ) + 720.)  mod 360.
    ;  if i eq 0 then xrange = minmax(xval)

    residuals = f_div ((*cbe[ii]).count- *xp[ii], sqrt( *xp[ii] > 0) )
    if keyword_set(resid) then begin
      ; for resid, will have just one plot, and want it white, so put it in oyval
      yval = -1.
      oyval = residuals
      ytitle = 'Sigma (c/bin)'
    endif else begin
      yval = (*cbe[ii]).count
      oyval = *xp[ii]
      ytitle = 'Counts / bin'
      if keyword_set(rate) then begin
        time_bins = hsi_get_time_bins(self, /second)
        ltime = ( (*cbe[ii]).livetime * time_bins[ii] )
        yval = f_div( yval, ltime )
        oyval = f_div( oyval, ltime )
        ytitle = 'Counts / Second'
      endif
    endelse

    text  = 'Detector = '+trim(ii+1) + front_rear + $
      '  C stat = ' + trim(prof_fit.cstat[ii], '(f12.3)') + $
      '  Const,Slope,Corr = ' + arr2str(trim(prof_fit.coeff[*,ii], '(f6.2)'), ', ')
    yrange = minmax([yval,oyval])
    sind = sort(xval)

    ; First plot with nodata to put axes up in white
    plot, xrange, yrange, charsize=charsz, $
      xtitle=xtitle, ytitle=ytitle, xtickname=xtickname, thick=thick, title=text, $
      ymargin=ymargin, xrange=xrange, /xst, /nodata, xtickinterval=xtickinterval, _extra=_extra

    ; Overlay observed counts in red
    if yval[0] ne -1 then oplot, xval[sind], yval[sind], color=2, thick=thick

    ; Overlay expected counts (or residuals) in white
    if oyval[0] ne -1 then oplot, xval[sind], oyval[sind], thick=thick

    if arg_present(out_struct) then begin

      *out_struct[ii] = replicate({xval: 0.d0, xp: 0., obs: 0., ltim: 0., resid: 0.}, n_elements(sind))

      (*out_struct[ii]).xval = xval[sind] + abs_time_range[0]
      (*out_struct[ii]).xp   = (*xp[ii])[sind]
      (*out_struct[ii]).obs  = (*cbe[ii])[sind].count
      (*out_struct[ii]).ltim = (*cbe[ii])[sind].livetime * tdur[ii]  ; in seconds
      (*out_struct[ii]).resid= residuals[sind]

    endif

    next:
  endfor

  timestamp, /bottom, charsize=.9 * leg_size
  
  case 1 of
    ps: if ~add_to_ps then psclose
    jpeg: begin
      thisimage = tvrd()
      tvlct, r, g, b, /get
      image24 = bytarr(3, xsize, ysize)
      image24(0,*,*) = r(thisimage)
      image24(1,*,*) = g(thisimage)
      image24(2,*,*) = b(thisimage)
      write_jpeg, full_file_plot, image24, true=1
      set_plot, thisdevice
    end
    else: wset,save_window
  endcase

  ; restore plot parameters we changed
  !p.multi = save_multi
  tvlct,rr,gg,bb

  getout:
  if ~arg_present(xp) && ptr_chk(xp) then ptr_free, xp

  if exist(save_use_single) then self -> set,use_single_return_mode=save_use_single

end
