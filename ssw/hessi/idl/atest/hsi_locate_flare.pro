;+
; function: in_set
; 
; purpose: simple boolean function to check whether an element is a member of 
;          a set(array).  It is mainly syntactic sugar for a frequently repeated
;          operation.
;          
; inputs: ele: The element to be searched for
;         set: The set to be searched
;         
; output: 1=yes, 0=no
; 
; $LastChangedBy: pcruce $
; $LastChangedDate: 2008-11-10 12:47:28 -0800 (Mon, 10 Nov 2008) $
; $LastChangedRevision: 3952 $
; $URL: svn+ssh://thmsvn@ambrosia.ssl.berkeley.edu/repos/spdsoft/trunk/general/misc/SSW/in_set.pro $
;- 


;checks if an item is inside a set, right now it only includes simple
;array based sets
function in_set,ele,set

  compile_opt idl2,hidden
  
  idx = where(ele eq set)
  
  if idx[0] eq -1 then begin
    return,0
  endif else begin
    return,1
  endelse

end
                             ;+
;	Pro twod_subs,ij,ipix,jpix,i,j
; Returns (i,j) the two-d subscripts of position ij in an (ipix,jpix) matrix
;-
Pro Twod_subs, ij, ipix, jpix, i, j

  j = ij/ipix
  i = ij-j*ipix

  Return
END
;---------------------------------------------------------------------------
; Document name: hsi_locate_flare.pro
; Created by:    Jim McTiernan, May 14, 2002
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
; NAME:
;       HSI_LOCATE_FLARE
; PURPOSE:
;       Given the time_range and energy_band, returns the flare
;       position
; CATEGORY:
;       flare_list
; CALLING SEQUENCE:
;       success = hsi_locate_flare(time_range, energy_band)
; INPUTS:
;       time_range= the time interval for position finding
;       energy_band= the energy band
; OUTPUTS:
;       success= 1 if it worked, 0 if not
;       xy= the flare location in arcsec from sun center
; KEYWORDS:
;       quiet = run quietly
;       spin_axis (output) = the spin axis for the interval
;       plot = if set, put up a plot
;       test_pmtras = If set, do a test for good PMTRAS solution
;       image_obj = the image_object used
;       return_image_obj = Only return the image_obj keyword, if this
;                          is set.
; PROCEDURE:
;  Here is a first cut at the promised outline of an hsi_flare_finder
;  algorithm.  It's purpose is to determine the approximate location of the
;  brightest real source on the full disk, hopefully distinguishing this
;  from any on-axis artifact or mirror source.
;
;  1. Choose an appropriate time/energy window.  Longer time windows (~1
;  minute) are better since it helps to smear out the mirror source.
;  2. Determine the center of rotation from the aspect solution.
;  3. Using 16 arcsecond pixels, make a 128x128 backprojection map using
;  subcollimator 9 only.
;  4. Excluding any pixel within FWHM of the rotation center, where
;  FWHM is the FWHM of the subcollimator, determine the location of
;  the brightest pixel in the remaining map.
;  5. Repeat steps 3 and 4 for subcollimators 8,7,6 and 5.
;  6. Determine XY0 = [MEDIAN(x), MEDIAN(y)], where x,y are each 5-element
;  vectors containing the map peak locations.  This will give a robust
;  centroid, provided at least 3 of the map peaks are correct.
;  7. Select those map peaks that are within a radial distance, R, from XY0
;  where R is a parameter (~1 arcminute).
;  8. If there are fewer than 3 maps satisfing this condition, algorithm
;  fails and a failure code can be returned.
;  9. Otherwise, determine the average, X,Y of the selected map peaks.
;  This is the result.
;
;  In the calling program, if the algorithm fails, you might just redo the
;  call to hsi_flare_finder using a longer integration time (MIN(entire
;  flare, 5 minutes).  If this case fails too, you should just set flare
;  location = 0,0 and label the flare catalog as 'no location available' or
;  somesuch.
;
;  The main differences between this and our current scheme is the use of 5
;  subcollimators, longer integration times, suppressing the on-axis
;  source, and testing for internal agreement.  There are variations on
;  this theme, (eg using subcollimators 7,8,9 and requiring 2/3 to be
;  consistent), comparing multiple times, etc, but this first cut seemed a
;  reasonable mix of complexity and promise.
;  gordon
; HISTORY:
;       Version 1, May 14, 2002,
;           jmm, jimm@ssl.berkeley.edu
;       Switched the order of images, put check for good position
;       inside the loop, if it succeeds early, it will return,
;       jmm,17-feb-2003
;       Added a catch procedure for bug handling, 6-mar-2003
;       Fixed bug that occasionally returns the spin axis as the
;       answer, jmm, 13-nov-2003
;       Added the spin_axis keyword, to return the spin axis
;       31-dec-2003, jmm, added image_obj input and output
;       2-feb-2005, jmm, if test_pmtras is set, will return the
;       position, but with suspect_position set to 1
;       Changed npix_test variable to use fwhm[coll0[2]]/2, or detector
;       7, instead of detector 0, which results in 360 arcseconds for
;       the test. Now sources must be within about 60
;       arcsec. jmm,23-dec-2006
;       Fixed case for no image -- catch statement was not picking this
;       up, jmm, 20-oct-2010
;       Guards against index overflow caused by spin axis calculations
;       too near to the edge of the image, jmm, 25-oct-2010
;       Different test criteria; do 5 images [9,8,7,6,5] for each
;       flare, and only check for 3 matching positions at the end,
;       post oct 1 2015, only two images need to match. Post
;       11-may-2016, calls hsi_locate_flare38, which only uses
;       collimators 3 and 8.
;       22-jun-2017, RAS, revised to use the bproj object to avoid 
;       reprocessing and setting USE_FLUX_VAR to 0
;       30-nov-2017, jmm, if available, tries hsi_locate_flare38,
;       using detectors 6 and 8 first.
;       21-feb-2018, jmm, scaled npix_out, the size of the box around
;       the spin axis that is not included in the position check, to
;       be 0.25*fwhm[coll0[0]]. ALso note that the hsi_locate_flare38
;       option is never reached from hsi_xy_test_new.pro -- it
;       didn't work all that well. The 0.25 scale_factor
;       default is now the same as in hsi_flare_position_image
;-
Function hsi_locate_flare, time_range, energy_band, xy, spin_axis = avp, $
                           quiet = quiet, plot = plot, $
                           test_pmtras = test_pmtras, $
                           image_obj = image_obj, $
                           suspect_solution = suspect_solution, $
                           front_segment = front_segment, $
                           rear_segment = rear_segment, $
                           scale_factor = scale_factor, $
                           _extra = _extra

  If(anytim(time_range[0]) Gt anytim('11-may-2016')) Then Begin
;Call hsi_locate_flare38 for times after 2016 anneal; this may change
     subcoll_in = hsi_qlook_dets2use(time_range[0], /qlook_image)
     If(n_elements(subcoll_in) Le 2) Then Begin
        If(subcoll_in[0] Eq 0) Then subcoll_in[0] = subcoll_in[1]
        do38: return, hsi_locate_flare38(time_range, energy_band, xy, $
                                         spin_axis = avp, $
                                         quiet = quiet, plot = plot, $
                                         test_pmtras = test_pmtras, $
                                         image_obj = image_obj, $
                                         suspect_solution = suspect_solution, $
                                         front_segment = front_segment, $
                                         rear_segment = rear_segment, $
                                         subcoll_in = subcoll_in, $
                                         scale_factor = scale_factor, $
                                         _extra = _extra)
     Endif Else Begin           ;if we have a 6 - 8 combination, use that, else use 3 - 8
        If(in_set(5, subcoll_in) && in_set(7, subcoll_in)) Then Begin
           guess0 = hsi_locate_flare38(time_range, energy_band, xy, $
                                       spin_axis = avp, $
                                       quiet = quiet, plot = plot, $
                                       test_pmtras = test_pmtras, $
                                       image_obj = image_obj, $
                                       suspect_solution = suspect_solution, $
                                       front_segment = front_segment, $
                                       rear_segment = rear_segment, $
                                       subcoll_in = [5, 7], $
                                       scale_factor = scale_factor, $
                                       _extra = _extra)
           If(~guess0) Then goto, do38 ;if 6, 8 didn't work, then try 3, 8
        Endif Else goto, do38
     Endelse
  Endif
  success = 0b
  jfile_open = 0b
  suspect_solution = 0b
  avp = -1
  ;Error handler, this should be able do deal with pmtras stops
  err = 0
  catch, err
  If(err Ne 0) Then Begin
    catch, /cancel
    Print, 'Error'
    help, /last_message, output = err_msg
    For j = 0, n_elements(err_msg)-1 Do print, err_msg[j]
    ;If the test_pmtras journal file is open, close it, and remove it
    If(jfile_open Eq 1) Then Begin
      journal
      jfile_open = 0b
    Endif
    If(is_string(jfile)) Then Begin
      cmd = '/bin/rm '+jfile
      message, /info, 'Spawning: '+cmd
      spawn, cmd
    Endif
    print, 'Returning 0'
    message, /info, 'Time Range:'
    For j = 0, 1 Do print, anytim(/ccsds, time_range[j])
    xy = [0, 0]
    Return, 0b
  Endif

  If(keyword_set(quiet)) Then notquiet = 0 Else notquiet = 1
  If(keyword_set(scale_factor)) Then kscl = scale_factor Else kscl = 0.25
  grid_pars = hsi_grid_parameters()
  fwhm = grid_pars.pitch/2.0
  If(notquiet) Then Begin
    message, /info, 'Time Range:'
    For j = 0, 1 Do print, anytim(/ccsds, time_range[j])
  Endif
  tr0 = anytim(time_range)
  ;Check for a suspect roll solution:
  suspect_solution = 0b
  If(keyword_set(test_pmtras)) Then Begin
     suspect_solution = hsi_pmtras_test(tr0, _extra=_extra)
  Endif Else suspect_solution = 0b
  ;jmm, 15-oct-2012, Inserted here, don't use the RAS, since it
  ;frequently hangs, maybe someday....
  use_ras = 0b
  If(use_ras) Then message, /info, 'USING RAS' $
  Else message, /info, 'NOT USING RAS'
  ;Ok, start with images
  npix = 128
  gs = 16. + fltarr(2)          ; pixel_size
  r0_offset = 10.*npix*gs[0]
  xyoffset = [0.0, 1.0]
  xy = [0.0, 0.0]
  ;Grid points
  xgrid = gs[0]*(findgen(npix)-npix/2)+gs[0]/2.0
  ygrid = gs[1]*(findgen(npix)-npix/2)+gs[1]/2.0
  ;Use 5 detectors
  coll0 = [8, 7, 6, 5, 4]
  ncoll0 = n_elements(coll0)
  ; create image object here
  If(obj_valid(image_obj)) Then fso = image_obj $
  Else fso = hsi_image()
  fso -> set, xyoffset = xyoffset, $
    ;    r0_offset = r0_offset, $
    pixel_size = gs, $
    image_dim = [npix, npix], $
    im_time_interval = tr0, $
    energy_band = energy_band, $
    use_auto_time_bin = 0b, $
    use_flux_var = 0
  ;tweaking, xy_replace and e0_replace may tell you to skip this one
  hsi_qlook_image_twk, fso, /locate_flare, xy_replace = xy_replace, $
    e0_replace = e0_replace
  If(total(abs(xy_replace)) Gt 0.0) Then Begin
    If(energy_band[0] Le e0_replace) Then Begin
      xy = xy_replace
      success = 1b
    Endif Else Begin
      xy = [0.0, 0.0]
      success = 0b
    Endelse
    Return, success
  Endif
  ;  fso -> set, obs_time_interval = tr0
  ;  fso -> set, time_range = [0, tr0[1]-tr0[0]]
  If(use_ras) Then fso -> set, as_roll_solution = 'RAS'
  If(Not keyword_set(plot)) Then fso -> set_no_screen_output
  maxqs = fltarr(2, ncoll0) & maxqs[*] = -9999
  If(notquiet) Then Begin
    message, /info, 'FLARE TIME RANGE:'
    print, anytim(tr0[0], /ccsds), ' -- ', anytim(tr0[1], /ccsds)
  Endif
  npix_test = fwhm[coll0[2]]/gs[0]
  npix_test = npix_test > 2
  dcount = 0                    ;need to have a counter, because 9 may be off
;Get all images here, from RAS 22-jun-2017
  axyfs = fso->getdata(class='hsi_bproj', /flat, /no_sum, $
                       front_segment = front_segment, $
                       rear_segment = rear_segment, $
                       _extra = _extra)
  If(~exist(axyfs) || total(abs(axyfs)) Le 0.0) Then Begin
     message, /info, 'No Images: '
     success = 0b
     Return, success
  Endif
  is_annsec = stregex(/fold, /bool, hsi_get_modpat_strategy( fso), 'annsec')
  For j = 0, ncoll0-1 Do Begin
    xyfs = axyfs[*,*,coll0[j]]
    xyfs = is_annsec ? hsi_annsec2xy( xyfs, fso ) : xyfs
    xyfs = xyfs > 0.0
    If(total(abs(xyfs)) Le 0.0) Then Begin
      message, /info, 'No Image for detector: '+string(coll0[j]+1)
      Continue
    Endif
    ;zero out the image_axis, this call to hsi_aspect_solution may change
    ;I already have the aspect solution only need to do this once:
    If(dcount eq 0) Then Begin
      fsa = fso -> get(class_name = 'hsi_aspect_solution', /object_ref)
      ppp = fsa -> getdata()
      If(is_struct(ppp)) Then Begin
        avp = rhessi_get_spin_axis_position(tr0, aspect_obj = fsa)
        If(n_elements(avp) Eq 1 And avp[0] Eq -1) Then Begin
          message, 'Bad Spin Axis --- No Position Found'
        Endif
      Endif Else Begin
        message, 'Bad Aspect Solution --- No Position Found'
      Endelse
      If(notquiet) Then print, 'SPIN AXIS:', avp
      dcount = dcount+1
    Endif
    xyaxis0 = max(where(find_ix(avp[0], xgrid) eq 1))
    xyaxis1 = max(where(find_ix(avp[1], ygrid) eq 1))
    xyaxis = [xyaxis0, xyaxis1] > 1 ;this is necessary for -1 cases
    xyaxis = xyaxis < (npix-2)
    ;Find the maximum, exclude points which are closer than fwhm[coll0[j]]
    ;to the spin axis
    npix_out = kscl*fwhm[coll0[j]]/gs[0]
    npix_out = npix_out > 1
    print, 'NPIX_OUT: ', npix_out
    xyfs0 = xyfs
    xx0 = (xyaxis[0]-npix_out) > 0 ;jmm, 25-oct-2010
    xx1 = (xyaxis[0]+npix_out) < (npix-1)
    yy0 = (xyaxis[1]-npix_out) > 0
    yy1 = (xyaxis[1]+npix_out) < (npix-1)
    xyfs[xx0:xx1, yy0:yy1] = 0
    ;New, 9-oct-2006, jmm, try hsi_map_evaluator here
    If(total(xyfs) Gt 0) Then Begin
      (fso -> getstrategy()) -> setdata, xyfs
      hsi_map_evaluator, fso, output = otpx
      If(is_struct(otpx)) Then maxqs[*, j] = otpx.xypeak
    Endif
    If(notquiet) Then print, maxqs
  Endfor
  ;If all of the images fail get out...
  If(dcount Eq 0) Then Begin
    message, 'No images --- No position found'
  Endif
  ;Compare maxima for position
  n = ncoll0
  x = string(indgen(10), format = '(i1.1)')
  tmp = ''
  If(tr0[0] Gt anytim('1-oct-2015')) Then Begin
    ;Get all of the possible combinations
    ;of 3 detectors, unless it's post 1-oct-2015, then use any 2 detectors
    nusecolls = 2
    For j = 0, n-1 Do Begin
      st1 = x[j]
      For k = j+1, n-1 Do Begin
        st2 = st1+x[k]
        tmp = [tmp, st2]
      Endfor
    Endfor
    tmp = tmp[1:*]
  Endif Else Begin
    nusecolls = 3
    For j = 0, n-1 Do Begin
      st1 = x[j]
      For k = j+1, n-1 Do Begin
        st2 = st1+x[k]
        For l = k+1, n-1 Do Begin
          st = st2+x[l]
          tmp = [tmp, st]
        Endfor
      Endfor
    Endfor
    tmp = tmp[1:*]
  Endelse
  ;Median test for the possibilities
  For j = 0, n_elements(tmp)-1 Do Begin
    If(nusecolls Eq 2) Then Begin
      ssj = [strmid(tmp[j], 0, 1), strmid(tmp[j], 1, 1)]
    Endif Else Begin
      ssj = [strmid(tmp[j], 0, 1), strmid(tmp[j], 1, 1), strmid(tmp[j], 2, 1)]
    Endelse
    ;     message, /info, 'Testing '+tmp[j]
    xy0 = [median(maxqs[0, ssj]), median(maxqs[1, ssj])]
    x0 = xy0[0]-npix_test*gs[0] & x1 = xy0[0]+npix_test*gs[0]
    y0 = xy0[1]-npix_test*gs[1] & y1 = xy0[1]+npix_test*gs[1]
    nssj = n_elements(ssj)
    okfit_xy = bytarr(nssj)+1
    For k = 0, nssj-1 Do Begin
      If(maxqs[0, ssj[k]] Eq -9999 Or maxqs[1, ssj[k]] Eq -9999) Then Begin
        okfit_xy[k] = 0
      Endif Else Begin
        If(maxqs[0, ssj[k]] Ge x0 And maxqs[0, ssj[k]] Le x1 And $
          maxqs[1, ssj[k]] Ge y0 And maxqs[1, ssj[k]] Le y1) Then Begin
          okfit_xy[k] = 1b
        Endif Else okfit_xy[k] = 0b
      Endelse
    Endfor
    ok = where(okfit_xy Eq 1, nok)
    If(nok Ge nssj) Then Begin
      success = 1b
      maxqs_out = total(maxqs[*, ssj[ok]], 2)/nok
      xy = [maxqs_out[0], maxqs_out[1]]
      message, /info, 'Successful'
      ptim, time_range
      print, xy
      goto, get_out
    Endif Else Begin
      success = 0b
      xy = [0.0, 0.0]
      ;        message, /info, 'Unsuccessful'
      ;        ptim, time_range
    Endelse
  Endfor

  get_out:
  return, success
END


;---------------------------------------------------------------------------
; End of 'hsi_locate_flare.pro'.
;---------------------------------------------------------------------------
