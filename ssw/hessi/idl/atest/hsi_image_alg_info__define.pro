; This is inherited by each image algorithm info structure.  The actual info parameters will have
; the alg prefix prepended.
; 
; Written: Kim Tolbert, April 2010
; Modifications:
; 17-Aug-2010, Kim.  Added mc parameters
; 06-Feb-2018, Kim.  Added det_index_mask_used

pro hsi_image_alg_info__define

struct = {hsi_image_alg_info, $
  profile_tot_cstat: 0., $     ; overall goodness of fit of image 
  profile_cstat: fltarr(9), $  ; goodness of fit of image for each detector
  profile_coeff: fltarr(3,9), $ ; const, slope, correlation of fit of exp. to obs. profile
  profile_mc_tot_cstat: 0., $   ; monte carlo cstat for all dets
  profile_mc_cstat: fltarr(9), $ ; monte carlo cstat for each det
  profile_mc_tot_fwhm: 0., $    ; monte carlo fwhm for all dets
  profile_mc_fwhm: fltarr(9), $ ; monte carlo fwhm for each det
  
  det_index_mask_used: bytarr(9) } ;detectors used in image

  
end