;+
; Method: Matrix_rep
;
; Purpose:  This method aggregates the total annsec matrix for
;	all the selected detectors.  Builds it to connect the pixels in
;	the image to the bins of the calib_eventlist.  Best used
;	with stacked eventlist to keep the size manageable
;	expected_count = annsec_matrix# image[*] * scale
;   Compare expected_count with calib_str.count
;	Image is in the units of clean image which is counts / sc
;	NB, image must be in annsec coordinate system
;	To extract clean image - obj->set,image_alg=clean
;							 clean_image = obj->getdata(class='hsi_clean')
; Example
;	Set up an arbitrary image object
;	obj = hsi_image()
;	obj-> set, det_index_mask= [0B, 0B, 1B, 1B, 1B, 1B, 1B, 1B, 0B]
;	obj-> set, im_energy_binning= [25.000000D, 50.000000D]
;	obj-> set, im_time_interval= ['20-Feb-2002 11:06:10.000', '20-Feb-2002 11:06:33.330']
;	obj-> set, image_algorithm= 'clean'
;	obj-> set, modpat_skip= 4
;	obj-> set, natural_weighting= 0
;	obj-> set, pixel_size= [2.00000, 2.00000]
;	obj-> set, taper= 2.00000
;	obj-> set, time_bin_def= [1.0, 2.0, 4.0, 8.0, 8.0, 16., 32.0, 64.0, 128.]
;	obj-> set, time_bin_min= 512L
;	obj-> set, uniform_weighting= 1
;	obj-> set, use_phz_stacker= 1L
;	obj-> set, phz_n_roll_bins_control= [0L, 64L, 32L, 32L, 16L, 12L, 8L, 6L, 4L]
;	obj-> set, phz_radius= 30.0
;	clean_image = obj->getdata(class='hsi_clean')
;	;Extract the modul_profile_annsec object
;	aobj = obj->get(/obj, class='hsi_modul_profile_annsec object')
;
;	Use this method
;	aobj->matrix_rep, amtrx, clb, scale
;
;	Test the method
;	plot, clb.count, psy=10
;	linecolors
;	oplot, amtrx#clean_image[*] * scale, psym=10, color=5
;
; History:
;	richard.schwartz@nasa.gov - first version 2007
;	22-aug-2014 - added keywords nbins, noduration, flat_livetime
;-
Pro Hsi_Modul_Profile_Annsec::matrix_rep, annsec_matrix, calib_str, scale,$
 nbins=nbins, noduration=noduration, flat_livetime=flat_livetime

clb_evntlst=self->getdata(class='hsi_calib_eventlist')
nbin = 0
s    = where(self->get(/det_index_mask),ns)
nbin = lonarr(ns)
for i=0,ns-1 do nbin[i] = n_elements(*clb_evntlst[s[i]])
ci   = *clb_evntlst[s[ns-1]]
npix = self->get(/rmap)
npix = product( npix )
annsec_matrix = fltarr(total(nbin), npix)
nbins = [0,total(nbin, /cum)]
for i=0,ns-1 do annsec_matrix[nbins[i],0] = $
	transpose( reform( hsi_annsec_mpat(0,nbin[i]-1,self, s[i]),npix,nbin[i]))
calib_str = replicate( ci[0], nbins[ns])
for i=0,ns-1 do calib_str[nbins[i]] = *clb_evntlst[s[i]]

duration = Self->Get(/absolute_time)
duration = noduration ? 1 : duration[1]-duration[0]
if keyword_set(flat_livetime) then calib_str.livetime = 1.0
scale     = calib_str.flux_var * calib_str.livetime / duration
;Use time_bin for time bin size in busec if available
;If time_bin undefined, we have to define it if time tag not available


time_bin = self->Get(/time_bin_min, /time_bin_def)
time_bin = long(time_bin.time_bin_min) * time_bin.time_bin_def
det_eff  = Self->Get(/cbe_det_eff)

for i=0,ns-1 do $

	scale[nbins[i]] = det_eff.rel[s[i]] * scale[nbins[i]:nbins[i+1]-1] * time_bin[s[i]]/2.0d0^20

end

