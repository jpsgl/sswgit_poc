;+
; helper function to fix solar eclipses, checks for short intervals,
; less than 5640 seconds, If the next interval plus this interval is
; GT 5640 seconds, then combine the two. Also, if there is an extra
; long eclipse, as happens for the 2017-08-21 eclipse, offset the
; start or end to adjust times.
;-
Function fix_eclipse_table, eclipse_table0

  test_dt = 5640.0
  dt = eclipse_table0[1:*].start_time- eclipse_table0.start_time
  x = where(dt Lt test_dt, nx)
  If(nx Eq 0) Then Return, eclipse_table0

  eclipse_table = eclipse_table0
  necl = n_elements(eclipse_table)
  keep = 1b+bytarr(necl)         ;default to keep
  For j = 0, nx-1 Do Begin
     xj = x[j]                  ;is the position of this eclipse
     xj1 = xj+1
     If(xj Lt necl-1 And keep[xj] Eq 1) Then Begin
        dtj1 = dt[xj]+dt[xj1]   ;add and test the next time interval
        If(dtj1 Gt test_dt And dtj1 Lt (test_dt+120.0)) Then Begin
;If the combined intervals are close to an orbit, then throw out the
;second one
           keep[xj1] = 0b
        Endif
     Endif
  Endfor
  
;There must be at least 1 ok
  ok = where(keep Eq 1)
  eclipse_table = eclipse_table[ok]
  eclipse_table.duration = eclipse_table.stop_time-eclipse_table.start_time

;What if there is too long an eclipse? (gt maybe
;5 minutes more than median duration), check
;either side and then restore some minutes to the shorter side
  test_dt = median(eclipse_table.duration)+300.0
  dt = eclipse_table.stop_time-eclipse_table.start_time
  x = where(dt Gt test_dt, nx)
  If(nx Eq 0) Then Return, eclipse_table
  necl = n_elements(eclipse_table)
;Note that we aren't deleting any eclipses here
  For j = 0, nx-1 Do Begin
;What are the sunlight periods before and after this interval (if any)
     If(x[j] Gt 0 && x[j] Lt (necl-1)) Then Begin
        dtj0 = eclipse_table[x[j]].start_time-eclipse_table[x[j]-1].stop_time
        dtj1 = eclipse_table[x[j]+1].start_time-eclipse_table[x[j]].stop_time
        offset = dt[x[j]]-(test_dt-300.0) ;use the difference for the median
        If(dtj0 Gt dtj1) Then Begin
           eclipse_table[x[j]].stop_time = eclipse_table[x[j]].stop_time-offset
        Endif Else If(dtj1 Gt dtj0) Then Begin
           eclipse_table[x[j]].start_time = eclipse_table[x[j]].start_time+offset
        Endif Else Begin
           eclipse_table[x[j]].stop_time = eclipse_table[x[j]].stop_time-offset/2.0
           eclipse_table[x[j]].start_time = eclipse_table[x[j]].start_time+offset/2.0
        Endelse
     Endif
  Endfor
;note that duration, and orbit numbers are not used after this,
;duration is easy to reset, orbit number is not
  eclipse_table.duration = eclipse_table.stop_time-eclipse_table.start_time
  Return, eclipse_table
End

;+
;NAME:
; hsi_eclipse2filetimes
;PROJECT:
; HESSI
;CATEGORY:
; Quicklook-Archive
;PURPOSE:
; Derives HESSI file start and end times from Orbit data
;CALLING SEQUENCE:
; hsi_eclipse2filetimes, orbit_table, eclipse_table, file_table
;INPUT:
; orbit_table = an anonymous structure for all of the days encompassing the
;              input time_range, tags:
;               .orbit_number = the orbit #
;               .start_time = the start time, anytim format
;               .end_time = the end time
; eclipse_table = a structure, one for each eclipse, with tags:
;                 .start_time = the start time, anytim format
;                 .orbit start = the orbit # on which it occurs
;                 .stop_time = the end time
;                 .orbit_stop = the orbit on which it stops
;                 .duration = eclipse duration in seconds
;OUTPUT:
; file_table = An anonymous structure contaning the data for files, Tags:
;                 .start_time = the start time, anytim format
;                 .orbit_start = the orbit # on which it occurs
;                 .end_time = the end time
;                 .orbit_end = the orbit on which it stops
;                 .duration = file duration in seconds
;METHOD:
; FIles will begin in the middle of eclipses, in general the
; Start and end Orbits will be different, but this is not necessary.
; File times are truncated to the previous minute
;HISTORY:
; jmm, 1-dec-1999, jimm@ssl.berkeley.edu
; jmm, 9-mar-2002, rewritten to use default file_table from the filedb
; structure .
; jmm, 2-jun-2006, only passes the filedb from the previous day on
;                  into the hsi_filedb_2_file_table routine
;-
PRO Hsi_eclipse2filetimes, orbit_table0, eclipse_table0, file_table, $
       time_intv=time_intv, ref_time=ref_time, filedb=filedb

;Here, attempt to fix the eclipse table to account for short obrits
;due to solar eclipses, jmm, 2014-10-24
;temporarily commented out, jmm, 2017-11-15
;  eclipse_table = fix_eclipse_table(eclipse_table0)

;ok,  for each element in the eclipse table, find the middle
   n = N_ELEMENTS(eclipse_table)
   midtime = 0.5*(eclipse_table.start_time+eclipse_table.stop_time)
   IF(keyword_set(time_intv)) THEN dt = time_intv[0] ELSE dt = 6.0d1
   midtime = hsi_set_file_time(midtime, ref_time=ref_time, $ 
                               time_intv=dt)
;There will be n-1 times passed out,
;from the middle of the 1st eclipse, to the middle of the last
   file_start_time = midtime[0:n-2]
   file_end_time = midtime[1:*]
   duration = file_end_time-file_start_time
;You need the start and end orbits for each, use a loop
   file_start_orbit = lonarr(n-1)
   file_end_orbit = file_start_orbit
;Sometimes the eclipse table will run to orbits not included in
;the orbit table, here, we simply add orbits onto each end of
;the orbit table
   orbit_table00 = orbit_table0[0]
   orbit_table00.end_time = orbit_table0[0].start_time
   orbit_table00.start_time = orbit_table0[0].start_time-120.0*60.0
   orbit_table00.orbit_number = orbit_table0[0].orbit_number-1
   n_orbits = N_ELEMENTS(orbit_table0)
   orbit_table11 = orbit_table0[n_orbits-1]
   orbit_table11.start_time = orbit_table0[n_orbits-1].end_time
   orbit_table11.end_time = orbit_table0[n_orbits-1].end_time+120.0*60.0
   orbit_table11.orbit_number = orbit_table0[n_orbits-1].orbit_number+1
   orbit_table = [orbit_table00, orbit_table0, orbit_table11]
   
   FOR j = 0, n-2 DO BEGIN
      ss_start = where(file_start_time[j] GE orbit_table.start_time AND $
                       file_start_time[j] LT orbit_table.end_time)
      IF(ss_start[0] EQ -1) THEN BEGIN
         message, /info, 'Mismatched orbit and eclipse tables'
         file_table = -1
         RETURN
      ENDIF
      file_start_orbit[j] = orbit_table[ss_start[0]].orbit_number
      ss_end = where(file_end_time[j] GE orbit_table.start_time AND $
                     file_end_time[j] LT orbit_table.end_time)
      IF(ss_end[0] EQ -1) THEN BEGIN
         message, /info, 'Mismatched orbit and eclipse tables'
         file_table = -1
         RETURN
      ENDIF
      file_end_orbit[j] = orbit_table[ss_end[0]].orbit_number
   ENDFOR
;Build the structure
   file_table0 = {start_time:0.0d0, orbit_start:0l, end_time:0.0d0, $
                   orbit_end:0l, duration:0.0d0}

   file_table = replicate(file_table0, n-1)
   file_table.start_time = file_start_time
   file_table.orbit_start = file_start_orbit
   file_table.end_time = file_end_time
   file_table.orbit_end = file_end_orbit
;You need to synch this file table with one obtained from the filedb
;structure
   n = n_elements(file_table)
   IF(keyword_set(filedb)) THEN BEGIN
      IF(datatype(filedb) EQ 'STC') THEN BEGIN
;don't pass the whole filedb into hsi_filedb_2_file_table, jmm, 2-jun-2006
         t0 = min(file_start_time)-24.0d0*3600.0d0
         ss_fb = where(filedb.start_time Gt t0)
         If(ss_fb[0] Ne -1) Then Begin
           file_table0 = hsi_filedb_2_file_table(filedb[ss_fb])
         Endif Else file_table0 = hsi_filedb_2_file_table(filedb)
         mdpt0 = 0.5*(file_table0.start_time+file_table0.end_time)
         mdpt = 0.5*(file_table.start_time+file_table.end_time)
         from_filedb = bytarr(n) ;flag for replaced file_table elements
         FOR j=0, n-1 DO BEGIN  ;replace file_table elements
;midpts should be close
            filedb_ft = where(abs(mdpt0-mdpt[j]) LT 60.0)
            ntags = n_elements(tag_names(file_table))
            IF(filedb_ft[0] NE -1) THEN BEGIN
               FOR i=0, ntags-1 DO $
                  file_table[j].(i) = file_table0[filedb_ft[0]].(i)
               from_filedb[j] = 1
            ENDIF
         ENDFOR
;synch start and end times, 
         oops = where(file_table[0:n-2].end_time NE $
                      file_table[1:*].start_time, noops)
         IF(oops[0] NE -1) THEN BEGIN
            message, /info, 'Synching orbit times'
            FOR j=0, noops-1 DO BEGIN
               oj = oops[j]
               oj1 = oj+1       ;there is always 1 more
               IF(from_filedb[oj] EQ 1) THEN BEGIN 
;always conform to the filedb
                  IF(from_filedb[oj1] EQ 1) THEN $ ;should never happen
                     file_table[oj].end_time=file_table[oj1].start_time $
                  ELSE file_table[oj1].start_time = file_table[oj].end_time
               ENDIF ELSE BEGIN
                  file_table[oj].end_time = file_table[oj1].start_time
               ENDELSE
            ENDFOR
         ENDIF
      ENDIF
   ENDIF
   file_table.duration = file_table.end_time-file_table.start_time
   RETURN
END
