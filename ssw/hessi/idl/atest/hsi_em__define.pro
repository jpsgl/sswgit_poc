;---------------------------------------------------------------------------
; Document name: hsi_em__define.pro
; 09-may-2013 Federico Benvenuto (benvenuto@dima.unige.it)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI EM ALGORITHM CLASS DEFINITION
;
; PURPOSE:
;       Object wrapper for the em algorithm
;
; CATEGORY:
;       Imaging (hessi/image)
;
; CONSTRUCTION:
;       image_obj = Obj_Name( 'hsi_em' )
;
; METHODS:
;      No public methods
;
; OUTPUT TYPE:
;      2D IMAGE ARRAY
;
; INPUT PARAMETERS:
;       Control (input) parameters are defined in the structure
;       {hsi_em_parameters}.
;
; OUTPUT PARAMETERS:
;       Info (output) parameters are;
;
; SOURCE OBJECT:
;       HSI_Modpat_Products
;
; SEE ALSO:
;       hsi_map_em
;       http://hessi.ssl.berkeley.edu/software/reference.html#hsi_em
;
; HISTORY:
;       09-may-2013 based on Richard Schwartz annsec code
;       01-aug-2017, RAS, change amtrx2 to mtrx2 as we also enable
;       Cartesian modulation pattern
;       08-nov-2017, Benvenuto, Duval-Poo, reimplementation of the EM algorithm.
;       14-nov-2017, RAS, H forced positive, added support for calib_eventlist normalization
;       15-nov-2017, RAS, Benv, Duval etc revised to use gridtran, modamp, cos matriz
;-
;--------------------------------------------------------------------


function hsi_em::init, _extra=_extra, source = source

  ret=self->hsi_image_alg::init( 'em', $
    control=hsi_em_parameters(), $
    info={hsi_em_info}, $
    source = source, $
    _extra=_extra)

  return, ret

end

;--------------------------------------------------------------------

pro hsi_em::image_alg_hook, param, image_out, param_out, _extra = _extra

  obj_modpat_products = self->get( /source )
  obj_bproj = obj_modpat_products->get(/obj, class = 'hsi_bproj')

  junk = obj_bproj->getdata()  ; do a back projection to make sure all is set up
  ;flatfield_in = obj_modpat_products->get(/flatfield) - we don't need to store this any more
  if hsi_get_modpat_strategy( self ) eq 'HSI_ANNSEC_PATTERN' then begin
    obj_modul_profile_annsec = self->get(/obj, class='hsi_modul_profile_annsec')
    ;renaming amtrx2 to mtrx2 since we can have cartesian strategy as well
    obj_modul_profile_annsec->matrix_rep, H
  endif else begin ;hsi_cart_pattern strategy!
    hsi_square_mpat, self, H
  endelse
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; expectation maximization algorithm
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;;                       y
  ;;;               h^t # -------
  ;;;                      h # x
  ;;;; x  =   x  *  -----------------
  ;;;                  h^t # l
  

  max_iter  = param.max_iterations
  tolerance = param.tolerance
  maxsize   = param.max_array_size
  show_maps = param.show_maps
  show_n_maps = param.show_n_maps
  progress_bar = param.progress_bar
  map_window = param.map_window
  
  ; initialization
  obj_calib_eventlist = obj_modpat_products->get(/obj, class='hsi_calib_eventlist')
  obj_modul_profile = obj_modpat_products->get(/obj, class='hsi_modul_profile')
 
  cbe_ptr           = obj_modpat_products->getdata(class='hsi_calib_eventlist')     
  bpimg             = obj_bproj->getdata( vrate = cbe_ptr, this_flatfield = 0)
  image_out         = bpimg * 0.0 + 1.
  xsava             = fltarr( [ size(/dimension, image_out), max_iter + 1 ] )
  cstata            = fltarr(max_iter+1)
    
  if show_maps then begin
    tvlct, /get, r, g, b
    map_window = self->get(/em_map_window)
    loadct, 5, /silent
    print, 'ITER:    ', 'STD INDEX:  ', 'C-STAT:  '
  endif
  ;;;;RAS
  cc = ptr_concat(cbe_ptr, cc_index, cc_valid, cc_nvalid ) 
  cbe_normalize = self.cbe_normalize
  corrfac = hsi_get_corrfactors( Self, cbe_normalize = cbe_normalize, $
     det_index_mask = self->Get(/det_index_mask), /noset )
  
  det_eff_rel = (self->get(/cbe_det_eff) ).rel
  eff_rel = fltarr( cc_index[ cc_nvalid ] )
  for ii = 0, cc_nvalid - 1 do eff_rel[ cc_index[ii] : cc_index[ii+1] - 1] = det_eff_rel[ cc_valid[ii] ]
  corrfacs = eff_rel * 0.0
  ;help, corrfacs & print, corrfac
  for ii = 0, cc_nvalid - 1 do corrfacs[ cc_index[ii] : cc_index[ii+1] - 1] = corrfac[ cc_valid[ii] ]
  ;  IDL> help, h
  ;  H               FLOAT     = Array[3192, 4225]  65x65 pix, just cos(phz), gridtran, modamp
  scale = cc.flux_var  * cc.livetime * eff_rel
  ;h = h * rebin( scale, size(/dim, h ) ) > 0
  h >=0.0
  ;;;;RAS
  y = cc.count
  y_index = where(y gt 0.)
  y_len = total(y gt -1.)
  x = fltarr((size(H,/dim))[1]) + 1.
  
  Ht1 = H ## (y*0.0+1.0)
  H2 = H^2
  
  ; begin loop
  Hx = H#x
  for iter = 1, max_iter do begin
    x_old = x
    
    z = f_div( y , Hx )
    Hz = H ## z
    x = x * transpose( f_div( Hz , Ht1 ) )
    Hx = H#x        
    image_out = reform(x, size(bpimg,/dim))
    xsava[0, 0, iter] = image_out
    
    expected_counts_ptr = OBJ_modul_profile->getdata(vimage=(image_out)) ;expected counts
    expected_counts_vec = ptr_concat(expected_counts_ptr)
    Hxc = Hx * corrfacs
    
    cstata[iter-1]      = 2. / y_len * total( y[y_index] * alog( f_div(y[y_index],Hxc[y_index]) ) + Hxc[y_index] - y[y_index]  )
    ; cstata[iter-1]      = c_statistic((ptr_concat(cbe_ptr)).count, expected_counts_vec)
    
    default, std_index, 1e4
    default, percent, 0.0
    if iter gt 10 and (iter mod 25) eq 0 then begin
      emp_back_res = total( ( x * (Ht1 - Hz) )^2 )
      std_back_res = total( x^2 * ( f_div( 1.0, Hx) # H2 ) )
      std_index    = f_div( emp_back_res , std_back_res )
      default, target, std_index
      percent = 100.0 * (1.- (alog10( std_index/ tolerance ) / alog10( target / tolerance)))
      print, iter, std_index, cstata[iter-1]
    endif
       
    if show_maps  and (iter mod show_n_maps eq 0) then begin       
      self->show_maps, image_out, iter = iter, save_cwindow=map_window
    endif
    
    if std_index lt tolerance then break   
    
    default, cancel, 0
    if progress_bar && (iter mod 10) eq 0 && ~cancel then begin
      if ~(size(/tname, progobj) eq 'objref') then progbar,progobj, /init       
      mtext='iteration '+strtrim(iter,2)+ ' percent done: ' + strtrim( long(percent), 2)      
      progbar, progobj, /update, message_text=mtext
      progbar, progobj, cancel=cancel
      if cancel then progbar, progobj, /destroy
    endif
    if cancel then break
        
  endfor
  
  progbar, progobj, /destroy
  if show_maps then tvlct, r, g, b

  self->set, em_map_window = map_window, /no_update
  max_maps = self->get(/em_max_maps_saved) ; save max_maps from xsava
  index_image_out = lindgen( iter )
  case max_maps of
    -1:
     0: index_image_out = -1
    else: index_image_out =  long( interpol( [1, iter-10.], max_maps ) )
  endcase
  if index_image_out[0] eq -1 then xsava = 0 else xsava = xsava[*,*, index_image_out]

  param_out = {flux: total(image_out), $
    iter: iter, $
    image_out_history: xsava, $
    index_image_out: index_image_out, $
    map_window: map_window, $
    cstat_history: cstata }
  
end

;--------------------------------------------------------------------
pro hsi_em::show_maps, em_map_data, iter=iter, save_cwindow=save_cwindow

  if save_cwindow eq -1 or (is_wopen(save_cwindow) eq 0) then begin
    save_cwindow = next_window(/user)
    dwsize_x= 512
    dwsize_y= 512
    window, save_cwindow, xsize=dwsize_x, ysize=dwsize_y,xpos=0,ypos=50,title='hsi em'
  endif

  wset,save_cwindow
  pos_save = !p.position
  xyoffset = self->get(/xyoffset)
  dxy = self->get(/pixel_size) * self->get(/pixel_scale)
  em_map = make_map( em_map_data, dx = dxy[0], dy = dxy[1], xc = xyoffset[0], yc = xyoffset[1], title='em iter: '+strtrim(iter,2),$
    time=anytim(/vms, avg(self->get(/absolute_time_range)) ))

  plot_map, em_map, /cbar
  ssw_legend, em_map.title

  !p.position = pos_save

end
;--------------------------------------------------------------------

pro hsi_em__define

  self = {hsi_em, $
    inherits hsi_image_alg }

end


;---------------------------------------------------------------------------
; end of 'hsi_image__define.pro'.
;---------------------------------------------------------------------------
