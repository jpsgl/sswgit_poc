
;+

function hsi_pixon_residuals, image, dobj, ceobj, corrfac, det_list, harmonics, $
  modprofile=modprofile, sspatt=sspatt, $
  noresidual=noresidual, setunits=setunits, $
  nounits=nounits, background=bobj, $
  cbe_ptr = cbe_ptr, $
  map_ptr = map_ptr, $
  time_unit = time_unit, $
  time_bin = time_bin, $
  det_eff = det_eff

  ;NAME:
  ;     hsi_pixon_residuals
  ;PURPOSE:
  ;     compute the data residuals for an image
  ;CATEGORY:
  ;CALLING SEQUENCE:
  ;     residuals = hsi_pixon_residuals(image, dobj, ceobj, det_list, harmonics)
  ;INPUTS:
  ;     First five parameters have been extracted from IOBJ to reduce object overhead
  ;      cbe_ptr = calib_eventlist pointer array
  ;      map_ptr = modpat pointer array
  ;      time_unit = multiplier on time_bin, number of binary microseconds
  ;      time_bin =  - number of time_units for each bin for each detector, 9 element vector
  ;      det_eff = cbe_det_eff extracted from object
  ;     image = the image. fltarr(nx*ny)
  ;     dobj = pixon data object, array of pointers, [27, 5]
  ;     ceobj = modulation pattern object
  ;     det_list = list of valid det's from hsi_pixon_get_data
  ;     harmonics = list of valid harmonics from hsi_pixon_get_data
  ;     corrfac - vector of cbe correction factors, 1 per detector
  ;OPTIONAL INPUT PARAMETERS:
  ;KEYWORD PARAMETERS
  ;     modprofile = returns with the reconstructed data.
  ;                  residuals = modprofile - data.
  ;     sspatt = index array giving the image pixels used if an image subset is
  ;              being passed in.
  ;     /noresidual = return the modulation profile rather than the residual.
  ;
  ;OUTPUTS:
  ;     residuals = vector of residuals, [nd]
  ;COMMON BLOCKS:
  ;SIDE EFFECTS:
  ;RESTRICTIONS:
  ;PROCEDURE:
  ;MODIFICATION HISTORY:
  ;     T. Metcalf 1999-Dec-09
  ;     19-jun-2002, ras, protect against division by 0 in computation of nunits
  ;     R Schwartz 2015-Jan-16- Extensively edited to pass cbe_ptr, map_ptr, det_list_eff, time_unit, time_bins
  ;       directly into hsi_annsec_bproj and not the object to bypass object overhead. Removed dead code bloat.
  ;       removed references to harmonics, they are used to compute profiles but all the info is in the mpat_ptr
  ;     R Schwartz 05-apr-2017- replaced all instances of "a2d" with "det", all commas followed by a space
  ;     R Schwartz  16-jun-2017, modify to allow Cartesian modulation pattern methods
  ;     R Schwartz  simplified, used append_arr and dispense with units as
  ;     profile is rendered in counts per bin so units/nunits is unnecessary
  ;     R Schwartz, 15-nov-2017, pass in corrfac
  ;+

  do_resid = ~keyword_set(noresidual)
  is_bkg = total( ptr_valid( bobj ), /int) <1
  for i=0, n_elements(det_list)-1 do begin

    id     = det_list[i]

    fobs = do_resid ? *dobj[ id ] : 0.0

    ;16-jun-2017, modify to allow Cartesian modulation pattern methods
    mpat      = *map_ptr[ id ]
    is_annsec = have_tag( mpat, 'annsec_center_ptr' )

    mtemp = is_annsec ? hsi_annsec_profile(image, $
      ceobj, $
      this_det= id, $
      map_ptr = map_ptr, $
      cbe_ptr = cbe_ptr, $
      time_unit = time_unit, $
      wnz = wnz, $
      det_eff = det_eff.rel, $
      time_bin = time_bin) : hsi_cart_profile( image,  mpat )
    mtemp *= corrfac[id]

    ;is there background, then add to expectation mtemp

    mtemp = is_bkg ? mtemp + *bobj[ id ] : mtemp
    ;if is_residual then subtract the observed, fobs
    profile = append_arr( profile, mtemp )
    residuals   = append_arr( residuals, mtemp - fobs  )
  endfor
  modprofile = profile

  if keyword_set(noresidual) then return, modprofile
  ;if ~same_data( [residuals, modprofile],[residuals1, modprofile1]) then stop
  return, residuals


end
