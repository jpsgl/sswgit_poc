;+
; NAME: HSI_MAP_CLEAN
;
; CALLING SEQUENCE:
;       clean_map =  hsi_map_clean( dirty_map, mod_pat, clean_params, $
;               count_rate = count_rate,  weights = weights, quiet = quiet, plot = plot)
;
; PURPOSE:
;       This function extracts a "clean" map from a "dirty" map.
;
; METHOD:
;       Iteratively decomposes the image into single bright pixels,
;       subtracting out the side lobes for each point.
;
;       Clean does not know about collimators, pitches or angles.  It takes
;       the modulation patterns as they are given.  It cannot tell which
;       mod_pats are from which collimator, so weighting (aka tapering)
;       must be done via a vector which "knows" which angles and
;       collimators are which.
;
; INPUTS:
;       Dirty_map = back-projection map created from binned or unbinned
;                   photon list--must be 1-dimensional of size N^2
;       mod_pat = Either a matrix (map_pixels x time_bins) or a file which
;                  can be used as an associated variable holding the matrix.
;        structure containing
;             mod_pat, the same array used to create dirty_map,
;              = N^2 x M matrix of modulation patterns, where
;                 N is size of map
;                 M is number of time bins or photons
;             ncoll = vector giving the collimators used in mod_pat
;             nbins = vector of sizes of each array within mod_pat
;                For example, if mod_pat includes collimators in the
;                order [3,8,5,2], that is vector ncoll, and if there
;                are 1000 bins for collimators 3 and 2, and 100 for 5 and 8,
;                nbins = [1000,100,100,1000]
;           mean_sq = total(mod_pat^2,1)/float(nx)^2, a value for each rotation angle bin
;       clean_params = structure created by hsi_clean_parameters.pro containing:
;
;       niter      ; max number of iterations
;       negative_max_test:0, $  ;  stop on highest absolute value negative.
;       frac       ; fraction to multiply the maximum by
;       gaussfwhm  ;arcsecond of convolving Gaussian
;       taperpsf: fltarr(9), $  ;   tapering weights for each collimator
;       chi_sq_crit:0.0,  $     ;   chi_sq_crit; minimum chi-squared to quit at
;                niter = number of iterations to perform
;                frac = amount of PSF to subtract in each iteration
;                chi_sq_crit = max allowed chi_sq
;
;
; OUTPUTS;
;        A structure containing:
;          clean_map
;          clean_components
;          clean_amplitudes
;          clean_counts
;          params
;          chi-squared (if count_rate is defined.)
; HISTORY:
;       richard.schwartz@gsfc.nasa.gov, adapted from EJ Schmahl clean.pro, 26-apr-1999.
;       krucker@ssl.berkeley.edu did the later changes
;		11-Apr-2003, Kim.  Added inverse clean box option
;		14-jul-2003, RAS, moved call to delta_t to top of code so
;			it can be used instead of dt in normalization
;       9-Jul-2004, Kim.  Ensure that clean_stop is defined so doesn't crash in create_struct
;       14-Mar-2007, Kim. Ensure that chi_iter and chi_value are defined so doesn't crash.
;       7-May-2007, Kim.  Don't call cleanplot. Changed plot routines so not necessary.
;       16-Sep-2008, Kim. Added show_n_maps, show_n_chi - to show every n'th plot. Cleaned up
;         plot window stuff - now handled in plot routines.  Added printing reason for stopping.
;       29-Jan-2009, Brian, Kim.  Call hsi_clean_beam_polar with iter keyword
;       9-jul-2009, ras.  Had to add clean_param argument in call to hsi_clean_norm because image_obj
;		is really modpat_products, aka, the source of the image_obj
;		20-aug-2010, ras, changed () notation to proper square bracket notation for array indexing
;		20-Sep-2010, Kim. Add image_obj to calling arguments to hsi_show_clean_chi2, so we can get correct
;		    time bins for x axis when phz stacker is off.
;    1-apr-2013, ras, removed ref to params not used, sigma, sigma_beam
;      sigma_factor, tau, lambda, mu, nu
;      added old_lt_correction to give correct map on restart from iter option, ras
;   05-Aug-2013, Kim. Previously, if any clean box had any part outside of image, we stopped and returned. Now
;      just let pollyfillv find pixels in clean boxes that overlap image.  Also, if rectangles are defined in
;      clean_box, previously had two sections - one to deal with rectangles, and another to deal with arbitrary
;      shapes. Now convert clean_box rectangle info to cw_list,cw_nop, and then just one section that finds pixels
;      in clean boxes.
;   4-may-2017, RAS, added some spaces for clarity
;   RAS, 15-jun-2017, allow the modul_profile object to be passed explicitly through _extra to hsi_psf, otherwise it isn't available
;     In that case the hsi_'strategy'_profile function must be called directly in psf::process
;   15-nov-2017, RAS, passing thru cbe_normalize to hsi_get_corrfactors and passing them thru to hsi_psf

;-

; ALGORITHM:
;      1. Copy dirty_map to map, set N = 0
;      2. Find the brightest point (x,y) in map, if negative, quit.
;      3. Look at component list to see if (x,y) was done before
;         a. If it was, extract the PSF from cache
;         b. If not, then use backprojection to create the PSF, add to cache
;      4. Subtract PSF*frac from map, set N = N+1, save (x,y), increm clean_map
;      5. compute modulation profile and chi_sq (if keyword no_chi2 is NOT set)
;      6. If chi_sq >  chi_sq_crit or N < Niter or interrupt not set, go to 2;
;      7. return maps and parameters
;
;

FUNCTION HSI_Map_Clean, image_obj, dirty_map, $
  cbe_normalize = cbe_normalize, $
  CLEAN_PARAM = clean_param, $
  DEBUG = debug, $
  PIXEL_SIZE = pixel_size, $
  IMAGE_DIM = image_dim, $
  PSFRESET = psfreset, $
  PLOT = plot, $
  SPLOT = splot, $
  VERBOSE = verbose, $
  QUIET = quiet, $
  RESID_MAP = resid_map


  IF NOT Keyword_Set( PLOT ) THEN plot = image_obj->get( /PLOT )
  IF NOT Keyword_Set( VERBOSE ) THEN verbose = image_obj->get( /VERBOSE )
  IF Keyword_Set( QUIET ) THEN verbose = 0
  IF NOT Keyword_Set( DEBUG ) THEN debug = image_obj->get( /DEBUG )


  IF debug THEN BEGIN
    verbose =  1
    plot = 1
  ENDIF

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; 1.  initialize stuff
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  IF NOT Keyword_Set( CLEAN_PARAM ) THEN  clean_param = image_obj->Get( /CLEAN_PARAM )

  cbe_corrfactors = hsi_get_corrfactors( image_obj, cbe_normalize = cbe_normalize )
  rmap_dim = image_obj->Get( /rmap_dim )
  image_dim = image_obj->Get( /image_dim )
  is_cart = stregex( /bool, /fold, hsi_get_modpat_strategy(image_obj ), 'cart')
  map_dim = is_cart ? image_dim: rmap_dim ;cart or annsec?
  pixel_size = image_obj->Get( /PIXEL_SIZE )
  niter = clean_param.niter
  frac = clean_param.frac
  chi_sq_crit = clean_param.chi_sq_crit
  frac = clean_param.frac
  negative_max = clean_param.negative_max_test
  chi_sq_min_test = clean_param.chi_sq_min_test
  c2_window = clean_param.clean_chi_window
  map_window = clean_param.clean_map_window
  ;size of display windows
  dwsize_x = clean_param.show_map_xdim
  dwsize_y = dwsize_x/2
  dwsize_y2 = dwsize_y*1.25
  ;ras, 14-jul-2003
  t_range = image_obj->get(/ABSOLUTE_TIME_RANGE)
  delta_t = float(t_range[1]-t_range[0])
  if delta_t eq 0 then delta_t = 4.


  ;sa"m new parameters
  if not ptr_valid( clean_param.clean_box ) then clean_param.clean_box = Ptr_new( 0 ) $
  else    checkvar,*clean_param.clean_box,0
  clean_box = *clean_param.clean_box
  no_chi2 = clean_param.no_chi2
  show_maps = clean_param.show_maps
  ;show_n_maps = clean_param.show_n_maps
  ttest = have_tag(clean_param, 'show_n_maps', ixttest)
  show_n_maps = clean_param.(ixttest)
  show_chi = clean_param.show_chi
  if no_chi2 then show_chi = 0  ; don't show chisq maps if not using chisq test
  ttest = have_tag(clean_param, 'show_n_chi', ixttest)
  show_n_chi = clean_param.(ixttest)
  ;show_n_chi = clean_param.show_n_chi
  nn = 0
  cw_list = *clean_param.cw_list
  cw_nop = *clean_param.cw_nop
  cw_inverse = clean_param.cw_inverse
  cancelled = 0.
  ttest = have_tag(clean_param, 'progress_bar', ixttest)
  do_progbar = clean_param.(ixttest)
  ;do_progbar = clean_param.progress_bar eq 1
  media_mode = clean_param.media_mode
  lt_correction = 0.


  clean_counts = 0.0
  iter = 0
  ncomp = 0
  chi_sq_tot = 1.e20
  map_max = 1.
  clean_flux = 0
  obs_flux = 1.e20
  label = '     ITER        XY[0]    MAP_MAX  TOTAL(CHI_SQ) MAX(CLEAN_MAP) TOTAL(CLEAN_COUNTS) TOTAL(RESID_MAP)'

  checkvar, psfreset, 1

  ;get calib_eventlist for chi2 test, if needed
  det_index_mask = image_obj->get(/det_index_mask)
  slist = where(det_index_mask eq 1)
  sdim = n_elements(slist)
  if no_chi2 eq 0 then begin
    fobs = image_obj->getdata(class_name = 'hsi_calib_eventlist')
    ;some used variables
    chi2 = fltarr(sdim)
    tot_cc = fltarr(sdim)
    tot_obs = fltarr(sdim)
  endif

  unif_weighting = image_obj -> get(/uniform_weighting)
  grids = hsi_grid_parameters()
  w_uni = 1./(grids.pitch/2.)
  w_uni = w_uni[slist]/total(w_uni[slist])

  ; If doing any plots, save window id and color table, and set color table 5
  if show_chi or show_maps gt 0 then begin
    tvlct, scr, scg, scb, /get
    save_window = !d.window
    loadct,5, /silent
  endif

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;1.1 CLEAN BOXES
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ; Find pixels we want to search for maximum during CLEAN operation - defined by rectangles (clean_box) or arbitrary shapes (cw_list, cw_nop)
  ; cw_list is [2,n] for x,y coordinates of boundary, cw_nop is array of number of points in cw_list for each boundary.
  ;
  ; If clean rectangles were defined, convert the corners stored in clean_box to arbitrary shapes stored in cw_list, cw_nop
  ; Each rectangle has 5 points for boundary, so if there were 2 rectangles, cw_list will be [2,10] array and cw_nop = [5,5]
  if ~keyword_set(cw_list) || cw_list[0] eq -1 then begin
    nbox = n_elements(clean_box)/4
    if nbox gt 0 then begin
      cw_list = fltarr(2,5*nbox)
      cw_nop = intarr(nbox)+5
      for i = 0,nbox-1 do begin
        b = reform(clean_box[*,i], 2,2)
        bb = [ [b[*,0]], [b[0,1],b[1,0]], [b[*,1]], [b[0,0],b[1,1]], [b[*,0]] ]
        i1 = i*5 & i2 = i1+4
        cw_list[*,i1:i2] = bb
      endfor
    endif
  endif else nbox = n_elements(cw_nop)

  ; Find the pixels inside all of the clean boxes.
  ; First make array of correct dimensions with all 0s. For each box, set pixels in that array that we want to use during
  ; CLEAN operation to 1.
  ; Final result is in clean_xy array - this is a 1-D array of pixels in image to search for max during CLEAN.
  clbox = fltarr(map_dim[0],map_dim[1])
  if nbox gt 0 then begin
    first_pixel = 0
    cw_pixels = cw_list*0.
    for i = 0,nbox-1 do begin
      xadd = cw_list[0,first_pixel:first_pixel+cw_nop[i]-1]
      yadd = cw_list[1,first_pixel:first_pixel+cw_nop[i]-1]
      if is_cart then cbp = hsi_xy2cart_index(xadd, yadd, image_obj) > 0 $
      else cbp = hsi_xy2annsec_index(xadd, yadd, image_obj) > 0

      ; round on or off so that size of clean box is maximized, i.e. if any part of pixel is included, include that pixel
      acbp = [avg(cbp[0,*]),avg(cbp[1,*])]
      for n = 0,1 do for j = 0,n_elements(cbp[n,*])-1 do if (cbp[n,j]-acbp[n]) gt 0 then cbp[n,j] = fix(cbp[n,j]+1) else cbp[n,j] = fix(cbp[n,j])
      clean_xy2 = polyfillv(cbp[0,*],cbp[1,*],map_dim[0],map_dim[1])

      ; Note: if we instead want to use a pixel only if more than half in x or y direction is inside region (instead of if any
      ; of pixel is in region), then uncomment the following line, and COMMENT OUT the 3 lines above (Must COMMENT OUT since they
      ; modify cbp!)
      ;        clean_xy2 = find_box_region_index(findgen(map_dim[0]), findgen(map_dim[1]), cbp)

      clbox[ clean_xy2 ] = 1
      cw_pixels[*,first_pixel:first_pixel+cw_nop[i]-1] = cbp
      first_pixel = first_pixel+cw_nop[i]
    endfor
  endif else clbox[1:map_dim[0]-2, 1:map_dim[1]-2] = 1  ; No boxes - set all pixels except edge pixels to 1.

  clean_xy = cw_inverse ? where(clbox eq 0) : where(clbox eq 1)    ;only these pixels have to be searched for maximum
  outside_xy = cw_inverse ? where(clbox eq 1) : where(clbox eq 0)  ;these pixels are outside the clean boxes


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; 1.2 Copy dirty_map to map, initialize maps
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  IF N_Elements( dirty_map ) EQ 0 THEN dirty_map = image_obj->GetData( CLASS_NAME = 'HSI_BPROJ' )
  resid_map = reform(dirty_map, float(map_dim[0])*map_dim[1])

  clean_map = resid_map*0
  clean_map_norm = resid_map*0
  clean_counts = 0.0
  cc = 0
  label = '     ITER        XY[0]    MAP_MAX  TOTAL(CHI_SQ) MAX(CLEAN_MAP) TOTAL(CLEAN_COUNTS) TOTAL(RESID_MAP)'

  checkvar, psfreset, 1

  ; sa"m point source model
  ps_model = fltarr(long(product(map_dim)))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; 1.3 restart clean for some more iteration
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if keyword_set(clean_param.more_iter) AND (clean_param.clean_last_iter gt 0) then begin
    print,'CLEAN resumes. starting cleaning at iteration ',fix(clean_param.clean_last_iter+1)
    nn = clean_param.clean_normalization
    ;added old_lt_correction, ras, 1-apr-2013
    old_lt_correction = clean_param.clean_lt_correction ;avg_lt_correction from previous
    iter = clean_param.clean_last_iter+1
    lt_correction = [0, fltarr(iter)+old_lt_correction]*delta_t ; so avg_lt_correction will be exactly the same
    hhh = *clean_param.clean_chi_sq_iter
    if hhh[0] ne 0 then begin                            ;only if chi2 was calculated in previous run (chi_sq_iter is 0 if not calculated in previous run)
      chi_value = *clean_param.clean_chi_sq_value
      chi_iter = *clean_param.clean_chi_sq_iter
    endif
    resid_map = *clean_param.clean_resid_map
    resid_map = resid_map[*] * nn * old_lt_correction ;added old_lt_correction, ras, 1-apr-2013
    ps_model = *clean_param.clean_source_map
    ps_model = ps_model[*]
    cc = *clean_param.clean_components
    clean_map = *clean_param.clean_component_map
    clean_map = clean_map[*] * nn * old_lt_correction ;added old_lt_correction, ras, 1-apr-2013
    hsi_coord, image_obj, xx, yy
  endif else begin
    print,'CLEAN starts'
  endelse



  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; MAIN LOOP STARTS
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


  WHILE ((chi_sq_tot GT chi_sq_crit) AND (iter LT niter) AND ( (map_max GT 0) or (negative_max le -1) ) $
    AND (clean_flux LT obs_flux)  AND (cancelled eq 0) )   do begin

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; 2. Find the brightest point (x,y) in map, if negative, then exit loop
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    clbox = fltarr(map_dim[0],map_dim[1])
    clbox[clean_xy] = resid_map[clean_xy]

    if negative_max then begin
      map_max = max(abs(clbox), xy)
      map_max = clbox[xy]
    endif else begin
      map_max = max( clbox, xy) ;map_max = max(abs(clbox), xy)
    endelse


    IF (resid_map[xy] GT 0) OR (negative_max eq -1) THEN BEGIN     ;only positive maximum is cleaned expect negative_max is set to -1

      ; point source model
      ps_model[xy] = ps_model[xy]+frac*map_max
      ; clean component
      if iter eq 0 then begin

        hsi_coord, image_obj, xx, yy
        cc = [xy, xx[xy], yy[xy], frac*map_max]
      endif else begin
        cc = [[cc], [xy, xx[xy], yy[xy], frac*map_max]]
      endelse

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; 3. Get the Point_spread_function
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ;original method: psf is calculated again from scratch
      ; RAS, 15-jun-2017, allow the modul_profile object to be passed explicitly through _extra, otherwise it isn't available

      psf = image_obj->GetData( CLASS_NAME = 'HSI_PSF', /SUM_psf, xy_pixel = xy, $
        obj_modul_profile = image_obj->get(/obj, class='hsi_modul_profile'), cbe_corrfactors = cbe_corrfactors )
      psf = reform(psf,float(map_dim[1])*map_dim[0])
      lt_correction = [lt_correction,max(psf)]

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; 4. Subtract PSF*frac*map_max from map, increment iteration count and map
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

      resid_map = resid_map-psf/max(psf)*(frac*map_max)
      clean_map = clean_map +frac*map_max * hsi_clean_beam_polar(xy, image_obj, clean_param, iter = iter)
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; 4.1 display of dirty, clean, res map after each iteration
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      if show_maps eq 1 and (iter mod show_n_maps eq 0) then $
        hsi_show_clean_maps, clean_map, resid_map, dirty_map, clbox, ps_model, $
        xy = xy, iter = iter, map_max = map_max, gain = frac, clean_param = clean_param, $
        psf = psf, clean_box_pixels = clean_box_pixels, cw_pixels = cw_pixels, save_cwindow = map_window, cw_inverse = cw_inverse

      iter = iter + 1
      ncomp = ncomp+1

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; 5. compute modulation profile and chi_sq
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

      if no_chi2 eq 0 then begin


        ;get modulation profile for clean component
        fclean = image_obj->GetData( CLASS_NAME = 'HSI_MODUL_PROFILE',  VIMAGE = ps_model/delta_t )

        ;calcualtion of CHI 2
        for i = 0,n_elements(slist)-1 do begin
          fcleani = *fclean[slist[i]]
          fobsi = (*fobs[slist[i]]).count
          fwd_statistic,fobsi,fcleani,chi_stat,c_stat
          chi2[i] = c_stat
          tot_cc[i] = total(fcleani)
          tot_obs[i] = total(fobsi)
        endfor
        chi_sq_tot = unif_weighting ? total(chi2 * w_uni[slist]) : avg(chi2)

        ;save CHI2 for evolution plot
        if (n_elements(chi_iter) eq 0) or (iter eq 1) then begin
          chi_iter = iter
          chi_value = chi2
        endif else begin
          chi_iter = [chi_iter,iter]
          chi_value = [[chi_value],[chi2]]
        endelse

        ;new display march 2001
        ;opens window or set window
        if show_chi ne 0 and (iter mod show_n_chi eq 0)then begin
          hsi_show_clean_chi2,image_obj,niter,iter-1,chi2,tot_obs,tot_cc,chi_iter,chi_value,slist,fobs,fclean,w_uni,c2_window,clean_param
        endif
      endif

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; 5.1 PROGRESS BAR
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

      if do_progbar then begin
        if (iter-1 le 0) OR $
          ( ((clean_param.more_iter ne 0) AND (clean_param.clean_last_iter+1 ge iter-1)) ) then progbar,progobj,/init
        ;tot_flux = total(image_obj->get(/BINNED_N_EVENT))
        ;t_range = image_obj->get(/ABSOLUTE_TIME_RANGE)
        ;delta_t = t_range[1]-t_range[0]
        ;cleaned_flux = (total(ps_model)/delta_t*sdim)/tot_flux*100.
        ;mtext = 'Processing iteration '+strtrim(iter,2)+': '+strtrim(cleaned_flux,2)+' % of observed flux cleaned'
        if no_chi2 eq 0 then begin
          cleaned_flux = total(tot_cc)/total(tot_obs)*100.
          mtext = ['Iteration '+strtrim(iter,2)+' done:',strmid(string(cleaned_flux),5,6)+' % of observed flux cleaned']
        endif else begin
          mtext = 'Iteration '+strtrim(iter,2)+' done'
        endelse
        progbar,progobj, /update, message_text = mtext,percent = cleaned_flux
        progbar, progobj, cancel = cancelled
      endif

    ENDIF ELSE BEGIN

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; NEGATIVE MAXIMUM
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

      IF verbose THEN Message, "Negative Maximum. Exiting Loop.", /INFORMATIONAL
      clean_stop = 'neg maximum'
    ENDELSE

  ENDWHILE

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; MAIN LOOP ENDS
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; 7. display final result, normalize the maps and return the result
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if (n_elements(count_rate) GT 0) then begin
    count_rate = (clean_counts)
    IF verbose THEN BEGIN
      ;print, 'Total chi square = ', chi_sq_tot
      ;print, 'Reduced chi square = ', chi_sq_tot/n_elements(clean_counts)
    endif
  endif

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; 7.1 display of dirty, clean, res map at the end of the last iteration
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  iter = iter-1
  if show_maps ne 0 then begin

    hsi_show_clean_maps,clean_map,resid_map,dirty_map,clbox,ps_model,iter = iter,map_max = map_max,gain = frac,clean_param = clean_param,$
      psf = psf,clean_box_pixels = clean_box_pixels,cw_pixels = cw_pixels,save_cwindow = map_window, cw_inverse = cw_inverse
  endif

  if show_chi ne 0 then begin
    hsi_show_clean_chi2,image_obj,niter,iter,chi2,tot_obs,tot_cc,chi_iter,chi_value,slist,fobs,fclean,w_uni,c2_window,clean_param
  endif


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; TO BE CHANGED:  normalization of output maps (january 29, 2001)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;clean component (max of dirty map) has same units as dirty map
  ;then the components are convolved with the cleaned beam
  ;to make the units so that we can sum over the source to get
  ;the total flux, the cleaned beam area should be 1
  ;the normalization factor is the following:

  nn = hsi_clean_norm(image_obj, clean_param) ;had to add clean_param because image_obj
  ; is really modpat_products, aka, the source of the image_obj, ras, 9 - jul - 2009

  ;this is done outside clean
  ;then we divide by the detector_area
  ;nn = nn/hessi_constant(/detector_area)
  ;and by the duration
  ;time_range = image_obj->Get( /time_range )
  ;dt = time_range[1]-time_range[0]
  ;RAS, 14-jul-2003, change dt to delta_t
  if n_elements(lt_correction) ge 2 then begin
    avg_lt_correction = avg(lt_correction[1:*])/delta_t
    print,'averaged correction: ',avg(lt_correction[1:*])/delta_t
    print,'standard deviation : ',sigma(lt_correction[1:*])/delta_t
    print,'sigma/average      : ',sigma(lt_correction[1:*])/avg(lt_correction[1:*])
  endif else begin
    ;no iteration done (absolute maximum is negative)
    avg_lt_correction = 1.
  endelse


  clean_map = clean_map/nn/avg_lt_correction
  resid_map = resid_map/nn/avg_lt_correction

  ;save why clean stopped
  if iter+1 ge niter then                                 clean_stop = 'max iter reached'
  if no_chi2 eq 0 then if chi_sq_tot lt chi_sq_crit then  clean_stop = 'chi sqr crit'
  if cancelled ne 0 then                                  clean_stop = 'cancelled'
  if map_max lt 0 then                                    clean_stop = 'neg maximum'

  checkvar, clean_stop, '?'
  if clean_stop ne '?' then print,'CLEAN stopped because: ' + clean_stop

  rr = map_dim

  checkvar, chi_iter, 0.
  checkvar, chi_value, 0.

  if keyword_set(media_mode) then begin
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'danger, danger, danger!'
    print,'media mode: residual map is set to ZERO!'
    print,'media mode: residual map is set to ZERO!'
    print,'media mode: residual map is set to ZERO!'
    print,'media mode: residual map is set to ZERO!'
    print,'media mode: residual map is set to ZERO!'
    print,'media mode: residual map is set to ZERO!'
    resid_map = resid_map*0.
  endif

  output = create_struct('clean_map', reform(clean_map,rr[0],rr[1]), 'chi_sq_tot', $
    chi_sq_tot, 'clean_param', clean_param,'resid_map', reform(resid_map,rr[0],rr[1]),$
    'source_map', reform(ps_model,rr[0],rr[1]),'clean_windows', clbox, $
    'last_iter', iter, 'chi_window', c2_window, 'map_window', map_window, $
    'clean_stop', clean_stop, 'components', cc, 'clean_normalization', nn, $
    'clean_lt_correction',avg_lt_correction, $
    'clean_chi_sq_value', chi_value, 'clean_chi_sq_iter', chi_iter  )

  ;destroy progress bar
  if do_progbar then progbar, progobj, /destroy

  if show_maps gt 0 or show_chi then begin
    wset,save_window
    tvlct, scr, scg, scb
  endif

  return, output


END
