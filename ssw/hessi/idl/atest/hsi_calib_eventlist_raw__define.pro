;---------------------------------------------------------------------------
; Document name: hsi_calib_eventlist_raw__define.pro
; Time-stamp: <Sat Jul 07 2007 16:12:37 csillag phoenix.ssl.berkeley.edu>
;+--------------------------------------------------------------------------
;
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI CALIBRATED EVENTLIST RAW CLASS DEFINITION
;
; PURPOSE:
;      Provides data structures and methods to generate and access
;      calibrated event lists. This is really part of the trio
;      hsi_calib_eventlist / hsi_calib_eventlist_raw /
;      hsi_calib_eventlist_file. In fact, it is the processor for
;      generating calib eventlists from raw (binned eventlist) data.
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       hsi_calbi_eventlist_raw is not called directly. Instead, use
;
;       cbe_obj = Hsi_Calib_Eventlis( )
;       cbe_obj = Obj_New('hsi_calib_eventlis')
;
;       "cbe_obj" may be any variable name
;
; CONTROL PARAMETERS: are defined via hsi_calib_eventlist__define.
;
;       USE_AUTO_TIME_BIN:  if set, the automatic time binning is implemented
;     based on the flare offset and the CBE_DIGITAL_QUALITY parameter.
;     0.99 is the highest value for CBE_DIGITAL_QUALITY
;       CBE_POWERS_OF_TWO:  forces the multipliers on TIME_BIN_MIN to be in powers of two.;
; USE_FLUX_VAR: if set, then that procedure attempts to construct a demodulated
;     profile of the true flux variation in the energy band,
;     unmarred by grid modulation. Used in hsi_calib_eventlist_raw::DEMODULATE
; SMOOTHING_TIME is in seconds, and is used with the IDL smooth function, as part
;     of this algorithm.  The SMOOTHING_TIME should not be longer than the accumulation
;     interval.  If you want to use your own smoothing profile, determine it on regular
;     bins, and load it into USER_FLUX_VAR. To subsequently disable that, and use
;     the algorithm's value, set USER_FLUX_VAR to a scalar value,
;     preferably either 1 or 0. Used in
;     hsi_calib_eventlist_raw::DEMODULATE
;       USER_FLUX_VAR: to load a custom-designed smoothing profile. See
;                      above
;       XYOFFSET: the position of the center of the map in arcescs
;       USE_AUTO_TIME_BIN: is set, the time bins are determined
;                          automatically.
;       CBE_DIGITAL_QUALITY:
;       SRT_FILENAME: to specify which SRT file to use.
;       USE_TIME_WINDOW: a 2 element vector, with the first element
;                        being the number of seconds of the data to
;                        use, and the second element the number of
;                        seconds of data to NOT use
;   IMAGING_POWER_LAW - POWER-LAW parameter to pass to hessi_grm, assumed for incident
;     photon spectrum
;
; KEYWORDS:
;
;       ALL: (GetData) returns the calibrated eventlist in a single big
;            structure. Use in clean (I think)
;       POINTER: (GetData) returns the calibrated eventlist as a pointer, does
;                not dereference.
;       THIS_DET_INDEX: (GetData) to specify the detector number

;                       (starting at 0) for which the calibrated
;                       eventlist is requested, can be a scalar or a
;                       vector in the range 0 to 9.
;       OBSOLETE - NO LONGER USED - not relevant
;			THIS_HARMONIC_INDEX: (GetData) to specify the harmonic number
;                       (starting at 0) for which the calibrated
;                       eventlist is requested, can be a scalar or a
;                       vector in the range 0 to 2.
;
; EXAMPLES:
;
;       Creation of a calibrated event list object:
;           cbe = Obj_New( 'hsi_calib_eventlist' )
;       or
;           cbe = hsi_calib_eventlist()
;
;       set the following for the other objects:
;           o->set,  FILENAME='test.fits', $
;                                          TIME_RANGE=[1., 200.], $
;                                          ENERGY_BAND=[6.,55.], $
;                                          TIME_BIN_MIN=512
;
;
;       Now:
;           data = cbe->GetData( FILENAME='test.fits', $
;                                          TIME_RANGE=[1., 200.], $
;                                          ENERGY_BAND=[6.,55.], $
;                                          TIME_BIN_MIN=512, $
;                                          DET_INDEX=4)
;
;       will return the calibrated eventlist for the data in filename
;       test.fits and over the time range [1,200] sec for an energy
;       band of [6,55] keV with a min time bin of 512 binary microsec,
;       for the detector #5. .
;
; SEE ALSO:
;       hsi_calib_eventlist_raw
;       hsi_eventlist
;       hsi_packet
;
; HISTORY:
;	23-may-2013, ras aspect_p_error_threshold reject data bins when p_error exceeds this value,
;		changed to use this control parameter instead of the default value of 0.4 arcseconds
;		;gpi =0.4* hessi_grid_params[this_det_index].pitch/hessi_grid_params[0].pitch
;		;RAS, from M Fivian and G Hurford, 23 may 2013
;		gpi = p_error_threshold * hessi_grid_params[this_det_index].pitch/hessi_grid_params[0].pitch

; 15-mar-2013, changed effb to eff so follow-on code for multi-atten doesn't break, eff was the original
;	output of hessi_drm4image which was replaced by new code that temporarily called it effb. However,
;	there was other code looking for eff in an path not normally taken that needed it to be eff
; 20-sep-2012, ras, replace hessi_drm_4image with object method that uses
; 	hessi_build_srm in exactly the same way but avoids recomputation for
;	energies and det_index, energy dependence is efficiently computed on the first
;	pass in det_index and eb_index as the full detector and energy binning is used
;	with the hsi_srm object and then the results reused as needed. Amazin.
; 10-jan-2012, ras, atten_state tagname added, needed for adv regularized vis
; 22-Sep-2011, Kim. if more than 80% points removed for a det by change of 28-mar-2011, print message
;	28-mar-2011, ras put in correction for attenuator state change into gridtran field
;		and remove bad p_error points by removing counts and setting livetime to 0
;		gap to 1
;   28-Feb-2011, kim, ras - in process, free existing cbe pointers.
;       01-may-2009, acs, add aspect solution recheck after failing
;                    the first time
;	11-jul-2007, ras, added r_threshold
;	3-jul-2007, ras, initialize cbe to ptrarr(9) not (9,3)
;	20-apr-2007, ras, changes to integrate harmonic data into calib_eventlist
;		separate pointer for harmonics removed, harmonics are now integrated
;		by expanding the modamp and phase_map_ctr fields of the calib_eventlist
;		structure
;   13-dec-2005, ras, integrated changes in calib_eventlist__define (old) into the
;     new calib_eventlist_raw__define
;   12-dec-2005, ras, removed some old commented code from process, added EMPTY_FLAG keyword to getdata.
;     8-jul-2005, ras, use hsi_radial_coord to compute coordinates in imaging axis
;     coordinate system
;     27-jun-2005, ras, change det_index_off to off_det_index
;     jun-2005, ras, avoid overflows in time passed to aspect object.  old formulation
;    could have suffered problems for intervals gt 2048 seconds
;     12-may-2005, ras, set det_index_off mask for detectors without valid time bins.
;     23-mar-2005, ras, traps bad aspect solutions and exits gracefully
;     29-sep-2004, ras, change to correction of 7-sep-2004, roll_angle is displaced
;     by minimum and fixed number of 2*!pi such that all values are gt 0.
;     7-sep-2004, ras, force roll_angle in process to be
;     positive. There is an unknown problem downstream for negative
;     roll_angles.
;

;     17-apr-2004, ras, added culling.  If the gap value (new 17-apr-2004) is 1, set the livetime
;     to 0.0.  0.0 livetime bins will be removed from the flatfielding, 0 livetime removes them from
;     profile, and a 0 count value removes them from backprojection
;     12-jan-2004, ras, changed call to get(/xyoffset) from getxyoffset()
;     Also changes in ::Get concerning USE_FLARE_XYOFFSET
;     at the start of process
;       29-Jan-03 Kim Prepended $ to HSI_GRID in hsi_loc_file call
;     25-Oct-2002, Kim.  Added @hsi_insert_catch in getdata, changed some message
;       calls to hsi_message
;     25-oct-02, ras, uses efficiencie for front or rear as needed nont
;      for total detector (both segments) as before
;     21-oct-02, ras, now it uses the correct atten_state to define image
;     efficiencies, before all efficiencies were for atten_state 1.
;     16-jul-02, ras, revised ::auto_time_bin to always use aspect solution
;      USE_AUTO_TIME_BIN restored as the default.
;     10-jun-02, ras, check for -1 on getdata for binned_eventlist
;       Release 6.1 2002-02-17 -> documentation updated, time_window
;       implemented, and firstimage for the support of the analysis of
;       the first flare recorded
;
;       16-aug-2001 richard schwartz, cbe_digital_quality constrained.
;       12-jun-2001 richard schwartz, added auto time bin, flux variation
;       17 April 2001, Release 5.1 - changed meaning of grid orientations
;       grid_angle = !pi/2 - (hessi_grid_params.orient)[coll]
;       was grid_angle = (hessi_grid_parms.orient)[coll]
;
;       Release 4: - dets instead of a2ds.
;                  -
;       Release 3 development, August, 1999,
;           initialization using Hsi_Calib_Eventlist_Raw
;       Release 2 development, April, 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;       Algorithm and fundamental development: April 1999,
;           G.Hurford, E.J.Schmahl and R.A.Schwartz, NASA/GSFC
;
;---------------------------------------------------------------------------

pro hsi_calib_eventlist_raw_test

o = obj_new( 'hsi_calib_eventlist' )
o->set, obs_time = '20-feb-02 11:' + ['06:00', '06:21']
o->set, sp_energy_binning=[6,12]
;cbe = o->getdata()
;o->set, use_phz = 1
o->write

obj_destroy, o
o = hsi_image()

o = hsi_image( cb_filename = 'gaga.fits' )

file = hsi_loc_file( 'hsi_20020218*fits', COUNT=c )

print, 'test of the use_window parameter for self-calibration'

o = hsi_calib_eventlist( filename=file[0] )
o->set, time_range = [0, 432.84]
o->set, as_spin_period = 4.3284
cbe = o->getdata()
plot, (*cbe[4,0]).time, (*cbe[4,0]).count

end

;----------------------------------------------------------------------------

FUNCTION Hsi_Calib_Eventlist_raw::Init,caller

self.caller = caller
RETURN, 1

END

;----------------------------------------------------------------------------

function hsi_calib_eventlist_raw::need_update

; kind of ugly. passes back the call to the caller. dont know yet how to avoid
; this level of call.

return, self.caller->need_update( /orig )

end
;----------------------------------------------------------------------------
;20-apr-2006 - this block was moved and expanded to
;support the use of cbe_time_bin_floor for both the case of
;the set and unset value of use_auto_time_bin

PRO HSI_CALIB_EVENTLIST_RAW::TIME_BIN, _EXTRA=_EXTRA
caller = self.caller

caller->Set, _extra=_extra
use_auto_time_bin = Caller->Get( /USE_AUTO_TIME_BIN )

if  use_auto_time_bin EQ 1 THEN begin
    Self->Auto_time_bin
endif else begin
	time_bin_floor = Caller->Get(/cbe_time_bin_floor)
	if max(time_bin_floor) gt 1 then begin
		time_bin_min = Caller->Get(/time_bin_min)
		time_bin_def = Caller->Get(/time_bin_def)
		time_bin_def = time_bin_def > (time_bin_floor /time_bin_min)
		Caller->Set, TIME_BIN_DEF= time_bin_def
		endif
endelse

;20-apr-2006 - this block was moved
end

;----------------------------------------------------------------------------

PRO HSI_CALIB_EVENTLIST_RAW::AUTO_TIME_BIN, _EXTRA=_EXTRA
;Totally revised, ras, 16-jul-02, use aspect solution object only

caller = self.caller

caller->Set, _extra=_extra

;xyoffset = caller->Get( /XYOFFSET )

time_unit = caller->Get(/time_unit)
a = caller->get(/obj, class='hsi_aspect_solution')
;build a time array in busec units covering the absolute time range
ut = caller->get(/absolute_time_range)
duration = (ut[1]-ut[0] ) < 6.0
time = 2.^20 * (lindgen(128) +.5)*( duration)/128

asp = a->getdata(this_time=time, /this_unit_time, this_ut_ref=ut[0])

;Get the spin period in seconds
spin_period = avg(   (time[1:*]-time) /(asp[1:*].phi-asp.phi))*2.*!pi / 2.^20

bb = Caller->Get(/obj, class='hsi_binned_eventlist') ; not known why
xyoffset = caller->getxyoffset() ;xyoffset = Caller->get(/xyoffset)
hsi_radial_coord, asp, xyoffset, r_map_sc


;enhance radius for coning, ras, 14-mar-2005
r_dev = stdev( r_map_sc, avg_r_map_sc)
r_map_sc_plus = avg_r_map_sc + r_dev ;use enhanced radius to account for coning

;Now we consider harmonics
max_harmonic = Caller->Framework::Get(/max_harmonic) > 1

hsi_time_bin_auto, $
    r_map_sc_plus, $
    lindgen(9), $
    max_harmonic, $
    spin_period, $
    time_bin_min, $
    time_bin_def, $
    /powers_of_two, $
    digitization_eff=((Caller->Get(/CBE_DIGITAL_QUALITY) < 0.99)>.1)

time_bin_min = long( time_bin_min / 64 * 64/time_unit)
time_bin_def = long( time_bin_def )
time_bin_floor = Caller->Get(/cbe_time_bin_floor)
time_bin_def   = time_bin_def > (time_bin_floor /time_bin_min)

Caller->Set, time_bin_min = time_bin_min, time_bin_def=time_bin_def

end

;---------------------------------------------------------------------------

PRO Hsi_Calib_Eventlist_Raw::Process,  _extra =  _extra

caller = self.caller

bb = Caller->Get(/obj, class='hsi_binned_eventlist')
;Changed to get(/xyoffset), 12-jan-04, ras
xyoffset = Caller->Get(/xyoffset) ;self->getxyoffset() ;(/xyoffset)

;Define twopi
twopi = !pi * 2.0

eb_index = caller->Get( /EB_INDEX )
energy_band = caller->Get( /energy_band)
x_map = xyoffset[0]
y_map = xyoffset[1]

hessi_grid_params = hsi_grid_parameters()
srt_file = caller->Get( /SRT_FILENAME )

srt_filename = HSI_Loc_file( path='$HSI_GRID', $
                             srt_file, /RECENT, COUNT=srt_file_exists )

IF srt_file_exists EQ 0 THEN BEGIN
    msg = srt_file + ' does not exist in HSI_GRID. Aborting.'
    @hsi_message
ENDIF

; acs 2009-04-29 moved this here from ~20 lines below to pass
; information about the time if no aspect solution is available
abs_time_range = caller->Get( /absolute_time_range )

det_eff = {avg:1.0, rel:fltarr(9)+1.}

; Framework::setdata doesn't free any pointers that are inside self.data.
; Here, we will replace entire contents of self.data, so get current pointers and free them. kim, ras, 28-feb-2011
cbe = caller -> framework::getdata(/noprocess)
if ptr_chk(cbe) then ptr_free, cbe

cbe = PtrArr(9) ;( 9, 3 ) -ras, 3-jul-2007
aspect_obj=caller->Get( /OBJ, CLASS_NAME = 'hsi_aspect_solution' )

test_aspect =  aspect_obj->getdata()
If Size(/tname, test_aspect) ne 'STRUCT' then begin ;bad aspect solution
; acs 2009-05-01 if this process method is executed the first time
; after reading a visibility fits file, the aspect solution is not
; (yet) calculated. Thus, before giving up, try to force the
; calculation.
   print, 'bad aspect solution. trying to reprocess....'
   test_aspect = aspect_obj->getdata( /need_update )
; and now, redo the test:
   If Size(/tname, test_aspect) ne 'STRUCT' then begin
      msg = 'No aspect solution available for time interval [' + $
            arr2str( anytim( abs_time_range, /vms ) ) + '], can not proceed...'
      @hsi_message
      caller->setdata,-1
      RETURN
   endif
ENDIF
; acs moved aut time binning before we get the first cbe
; 2002-12-20
Self->Time_bin ;Calls auto_time_bin if needed and checks for time_bin_floor

; CALIB EVENTLIST does not check anymore for det_index_mask. Instaed,
; we check for the validity of pointers in binned eventlist and use
; this information to determine the valid detectors. This allows, NOT
; to redo calib_evenbtlist when one detector is reset.
; det_index = Where( self->Get( /DET_INDEX_MASK ), n_det )
be_obj=caller->Get( /OBJ, CLASS_NAME = 'hsi_binned_eventlist' )
;be_need_update = be_obj->need_update()
be = be_obj->GetData()
;if be_need_update and ~be_obj->need_update() then begin
;	cbe_eff_eb = fltarr(9, n_elements((*be[0])[0].count))
;	endif
;Use new VALID_BINNED_INDEX info param, ras, 8-may-02
det_index = Caller->Get(/valid_binned_index) ;Where( Ptr_Valid( be ), ndet )
off_det_index = bytarr(9) ;ras, 12-may-2005
ndet = n_elements(det_index)

; protection against empty binned eventlists, 2002-02-19 acs
IF Size(/tname, be ) ne 'POINTER' THEN BEGIN
    msg = 'No binned eventlist available, can not proceed...'  ;kim 21-aug-02
    @hsi_message
    caller->setdata,-1    ;kim 02-apr-2002
    RETURN
ENDIF

all_a2d = where(caller->get(/a2d_index_mask))

; acs 2002-02-16 this is for caller calibration
;time_window = caller->Get( /USE_TIME_WINDOW )
binned_n_event = Caller->Get(/BINNED_N_EVENT)

;21-oct-2002 Determine the atten_state
;the atten_state must be fetched only after the call
;to the binned_eventlist or bunches fails.
;Atten_state can be found in the packet headers, read from the simulation params,
;or set by user input in DEFLT_ATTEN_STATE
evl = Caller->Get(/obj, class_name='hsi_eventlist')
multi_atten_threshold = caller->Get(/cbe_multi_atten_threshold)
use_multi_atten = (multi_atten_threshold ne -1) && (energy_band[0] ge multi_atten_threshold)

atten_str = evl->getdata(/atten_state)
atten_state = -1
natten_state = 0
if  tag_exist( atten_str, 'STATE') then begin
    atten_state = atten_str.state
    atten_time  = atten_str.time
    natten_state = n_elements(atten_str.state)
        if natten_state gt 1 then BEGIN
; this must be a mistake -- acs

       istate      = value_locate( atten_time, abs_time_range    )
       atten_state = atten_state[istate[0]]
       if istate[0] ne istate[1] then begin
       atten_state_msg =  use_multi_atten ? $
       '!!!Attention. Attenuator state correction during the observing interval, using gridtran fix 28-mar-2011 ////' + $
       'Attenuator State changes: '+arr2str( anytim( atten_time, /vms),' - ') $
       :    '!!!!!!!!Warning. Attenuator state changed during the observing interval' + $
       arr2str( anytim( atten_time, /vms),' - ')
       message, /continue, atten_state_msg
       endif
       endif

    endif else atten_state = Caller->Get(/sim_atten_state)
    atten_state = atten_state eq -1? Caller->Get(/deflt_atten_state) : atten_state
;21-oct-2002 Determine the atten_state
;Get the time binning
time_bin_min = Caller->Get(/time_bin_min) ;units of busec
time_bin_def = Caller->Get(/time_bin_def)


;stop
;Fill the hsi_calib_event structure for each time bin
;IDL> help,{hsi_calib_event},/st
;** Structure HSI_CALIB_EVENT, 11 tags, length=44, data length=44:
;   DX              FLOAT          0.000000   - arcseconds  pitch offset
;   DY              FLOAT          0.000000   - arcseconds  yaw offset
;   ROLL_ANGLE      FLOAT          0.000000   - radians  roll around imaging axis
;   MODAMP          FLOAT          0.000000   - number normally - 0-1.
;   PHASE_MAP_CTR   FLOAT          0.000000   - radians
;   GRIDTRAN        FLOAT          0.000000   - number normally - 0-1.
;   FLUX_VAR        FLOAT          0.000000   - number normally - 0-1.
;   BACKGROUND      FLOAT          0.000000   - counts, same as counts, number
;   TIME            LONG                 0    - in binary microseconds X time_unit
;   COUNT           FLOAT          0.000000   - counts, number,
;   LIVETIME        FLOAT          0.000000   - fraction 0.-1.

max_harmonic = Caller->Get(/max_harmonic) >1 <3
cbstr = max_harmonic eq 1 ? {hsi_calib_event} : {hsi_calib_eventx}
r_threshold = Caller->Get(/r_threshold)
p_error_threshold = Caller->Get(/aspect_p_error_threshold)
FOR i=0, ndet-1 DO BEGIN

    this_det_index = det_index[i] MOD 9
    ;this_harmonic = det_index[i] / 9

;-
;  STEP 1: SCORE CREATION
;
;  Select an energy range and a suba2d_indeximator.
;  Form a score, listing individual events satisfying these criteria.
;
;  Attribute of each event:        Time   4-bytes         (64 bus
;                           resolution) time
;  STEP 2: TIME-BINNING
;
;  Select time-binning parameterization.
;  Group events in the score into time-bins with predetermined widths.
;** Structure HSI_CALIB_EVENT, 11 tags, length=44, data length=44:
;   DX              FLOAT          0.000000   - arcseconds  pitch offset
;   DY              FLOAT          0.000000   - arcseconds  yaw offset
;   ROLL_ANGLE      FLOAT          0.000000   - radians  roll around imaging axis
;   MODAMP          FLOAT          0.000000   - number normally - 0-1.
;   PHASE_MAP_CTR   FLOAT          0.000000   - radians
;   GRIDTRAN        FLOAT          0.000000   - number normally - 0-1.
;   FLUX_VAR        FLOAT          0.000000   - number normally - 0-1.
;   BACKGROUND      FLOAT          0.000000   - counts, same as counts, number
;  Attribute of each time-bin:
;;;   TIME            LONG                 0    - in binary microseconds X time_unit
;;;   COUNT           FLOAT          0.000000     - counts, number,
;;;   LIVETIME        FLOAT          0.000000   - fraction 0.-1.

;
; these two first steps are done in the binned eventlist object
;

    be = be_obj->GetData( THIS_DET_INDEX=this_det_index) ;, $
                          ;THIS_HARMONIC=this_harmonic )

;    IF caller.debug GT 0 THEN help, be

    n_bin = N_Elements( be )

    IF n_bin GT 1 THEN BEGIN
        ;ras, 12-may-2005, check to see if detector has any livetime
        ;if not, mask it from analysis
        if total(be.livetime) eq 0 then begin

           message,/continue,'Detector '+strtrim(i+1,2)+' is off for this interval'
           message,/continue,'Please do not use this detector in imaging'
           off_det_index[i] = 1b
           endif
           ;
           ;ras, 12-may-2005


        this_cbe = REPLICATE( cbstr, n_bin ) ;23-apr-2007, allow for harmonics
        this_cbe.gap = be.gap ;Gap tag added 17-apr-2004, ras
        nogap = 1-be.gap
        this_cbe.time = be.time
        this_cbe.count = be.count[eb_index]*nogap
        ;implement culling via gap value, ras, 17-apr-2004
        this_cbe.livetime = be.livetime[eb_index]*nogap
;stop
;moved        binned_n_event[this_det_index, 0] = total(this_cbe.count*nogap)
        ;Struct_Assign, be, this_cbe
                                ;---
;  STEP 3: ASPECT ASSOCIATION
;
;
;** Structure HSI_CALIB_EVENT, 11 tags, length=44, data length=44:
;  Associate an aspect solution with each time-bin.
;;;   DX              FLOAT          0.000000   - arcseconds  pitch offset
;;;   DY              FLOAT          0.000000   - arcseconds  yaw offset
;;;   ROLL_ANGLE      FLOAT          0.000000   - radians  roll around imaging axis
;   MODAMP          FLOAT          0.000000   - number normally - 0-1.
;   PHASE_MAP_CTR   FLOAT          0.000000   - radians
;   GRIDTRAN        FLOAT          0.000000   - number normally - 0-1.
;   FLUX_VAR        FLOAT          0.000000   - number normally - 0-1.
;   BACKGROUND      FLOAT          0.000000   - counts, same as counts, number
;   TIME            LONG                 0    - in binary microseconds X time_unit
;   COUNT           FLOAT          0.000000   - counts, number,
;   LIVETIME        FLOAT          0.000000   - fraction 0.-1.
;
;
;
        abs_time_range = Caller->Get( /absolute_time_range )
        ;Avoid time overflows from longwords!!!!
        time = time_bin_def[this_det_index]*(dindgen(n_bin)+0.5d0)
        aspect = aspect_obj->GetData( THIS_TIME=time, $
                                      THIS_UNIT_TIME=time_bin_min, $
                                      THIS_UT_REF=abs_time_range[0] )

;       Somewhere there is a geometry problem with negative
;       roll_angle.  Force roll_angle to be positive, ras, 7-sep-2004
;
       roll_angle = aspect.phi
       min_roll   = min(roll_angle,imin)
       if min_roll lt 0 then begin
         delta_roll = ceil( abs(min_roll)/twopi) * twopi
         roll_angle = roll_angle + delta_roll
         endif

        ;roll_angle = (Float( aspect.phi ) mod (2.*!pi) ) + (2.*!pi)

        ;roll_angle = Float( aspect.phi ) ;removed 7-sep-2004
        this_cbe.roll_angle = roll_angle
        this_cbe.dx = Float( aspect.dx )
        this_cbe.dy = Float( aspect.dy )

;---
;  STEP 4: LIVE-TIME ASSOCIATION
;
;
;
;  Associate a live-time measure with each time-bin.
;  The actual time of the time-bin can be dropped.  It is no longer needed, and
;  in any case, can be reconstructed.
;
; THIS SHIFTED TO THE BINNED EVENTLIST

;            livetime = REPLICATE( 1.0, n_bin )
;            this_cbe.livetime = livetime
                                ;---

;  STEP 5: ASSOCIATE GRID CALIBRATION
;
;  Select a map center, relative to sun center.  Select desired harmonic
;  of grid response.  Determine the grid response matrix parameters
;  associated with each time bin.  (This is assumed to include
;  source-location independent factors such as detector efficiency.)
;  Combine the throughput and modulation efficiency into a
;  Modulation Sensitivity
;
;** Structure HSI_CALIB_EVENT, 11 tags, length=44, data length=44:
;   DX              FLOAT          0.000000   - arcseconds  pitch offset
;   DY              FLOAT          0.000000   - arcseconds  yaw offset
;   ROLL_ANGLE      FLOAT          0.000000   - radians  roll around imaging axis
;;;   MODAMP          FLOAT          0.000000   - number normally - 0-1.
;   PHASE_MAP_CTR   FLOAT          0.000000   - radians
;;;   GRIDTRAN        FLOAT          0.000000     - number normally - 0-1.
;   FLUX_VAR        FLOAT          0.000000   - number normally - 0-1.
;   BACKGROUND      FLOAT          0.000000   - counts, same as counts, number
;   TIME            LONG                 0    - in binary microseconds X time_unit
;   COUNT           FLOAT          0.000000   - counts, number,
;   LIVETIME        FLOAT          0.000000   - fraction 0.-1.
;


        hsi_radial_coord, aspect, xyoffset, r_map_sc, azimuth_map_sc, x_map_sc, y_map_sc

        Caller->Set, offaxis_disp = avg(r_map_sc) * 3600. ;measured in arcseconds
        azimuth_map_sc = ATAN( y_map_sc, x_map_sc )

                                ; stop

        Hessi_GRM, energy_band, $
            this_det_index, grm, 0, r_map_sc, azimuth_map_sc*!radeg, $
            power_law=Caller->Get(/imaging_power_law), $
            SRT_FILENAME=SRT_FILENAME
;For energy_band 10-400 keV, grm.modphz[0,*,0] is the same for all det_index, for all 0<r_map_sc<0.5
;Thus, with 1st harmonic the phase_map_ctr vector is the same for all energies
		;11-jul-2007, ras, for gh and sk
		;test for r_map_sc lt r_threshold,
		;Take advantage of p_error finally, ras, 1-apr-2011
		r_thresh = r_threshold[this_det_index] gt 0.0 ? r_threshold[this_det_index] : 2.0
		;gpi =0.4* hessi_grid_params[this_det_index].pitch/hessi_grid_params[0].pitch
		;RAS, from M Fivian and G Hurford, 23 may 2013
		gpi = p_error_threshold * hessi_grid_params[this_det_index].pitch/hessi_grid_params[0].pitch
		rtest = where( (r_map_sc lt r_thresh) and $
					    (aspect.p_error gt gpi), nrtest)
		if nrtest gt 0 then begin
			this_cbe[rtest].count = 0.0
			this_cbe[rtest].livetime = 0.0
			this_cbe[rtest].gap = 1
			bad_percent = 100 * nrtest / n_elements(this_cbe)
			if bad_percent gt 80 then message,/cont,'Warning. ' + trim(bad_percent) + $
			  '% of Detector ' + trim(this_det_index+1) + ' data rejected because of bad aspect data.'
			endif


        ;this_cbe.modamp = (grm.modampl[0, *, this_harmonic])[*]
        this_cbe.modamp = transpose(grm.modampl[0,*,0:max_harmonic-1])
        ;ras, 21-jan-2002
        this_cbe.gridtran = (max_harmonic eq 1) ? grm.gridtran[*] * $
        hsi_gridtran_correction(this_cbe.modamp) : grm.gridtran[*]


        ;peak_resp_offset = (grm.modphz[0, *, this_harmonic])[*]
        peak_resp_offset = transpose(grm.modphz)

;---
;  STEP 6: DETERMINE PHASE CALIBRATION
;
;** Structure HSI_CALIB_EVENT, 11 tags, length=44, data length=44:
;   DX              FLOAT          0.000000   - arcseconds  pitch offset
;   DY              FLOAT          0.000000   - arcseconds  yaw offset
;   ROLL_ANGLE      FLOAT          0.000000   - radians  roll around imaging axis
;   MODAMP          FLOAT          0.000000   - number normally - 0-1.
;  Combine map center, modulation phase, pitch and yaw offsets into a phaseoffset.
;;;   PHASE_MAP_CTR   FLOAT          0.000000   - radians
;   GRIDTRAN        FLOAT          0.000000   - number normally - 0-1.
;   FLUX_VAR        FLOAT          0.000000   - number normally - 0-1.
;   BACKGROUND      FLOAT          0.000000   - counts, same as counts, number
;   TIME            LONG                 0    - in binary microseconds X time_unit
;   COUNT           FLOAT          0.000000   - counts, number,
;   LIVETIME        FLOAT          0.000000   - fraction 0.-1.
;
;

;   Definition of Grid_angle changed on 17-april-2001.  RAS from GH
        grid_angle = !pi/2. - hessi_grid_params[this_det_index].orient

;   RAS, 1-oct-2001 - peak_resp_offset in radians, not arcseconds
;   calculation for phase_map_ctr changed accordingly.
;
;        phase_map_ctr = twopi *(this_harmonic+1.0)*$
;            ((x_map_sc*cos(grid_angle) + $
;             y_map_sc*sin(grid_angle) ) / $
;            ; peak_resp_offset ) / $
;            hessi_grid_params[this_det_index].pitch) + $
;            peak_resp_offset
        phase_map_ctr= twopi *(findgen(3)+1.0)#$
            ((x_map_sc*cos(grid_angle) + $
             y_map_sc*sin(grid_angle) ) / $
            ; peak_resp_offset ) / $
            hessi_grid_params[this_det_index].pitch) + $
            peak_resp_offset

        this_cbe.phase_map_ctr =reform( phase_map_ctr[0:max_harmonic-1,*])
        ;this_cbe.phase_map_ctr3= phase_map_ctr3

        indx = where_arr( all_a2d, this_det_index+indgen(3)*9, nindx )
        id = strtrim( this_det_index+1, 2) + (['r','f'])[caller->get(/front_segment)]
        ;case 1 of
        ;    nindx eq 1 and all_a2d[indx[0]] eq this_det_index: id=id+'f'
        ;    nindx eq 1: id = id + 'r'
        ;    else: id = id + 't'
        ;    endcase

        ; RS notes, 9-jun-2001
        ; Need to add the atten_state from the observing summary
        ; Need to properly determine the powerlaw index.
        ; Hessi_drm_4image needs to use the powerlaw index
        ; Why is offax_position a parameter of hessi_drm_4image?
        ;

;        hessi_drm_4image, eff, energy_band, id, $
;        fcheck(atten_state,1), powerlaw=fcheck(powerlaw,4), offax_position=r_map_sc
;        det_eff.rel[this_det_index] = eff[0]
		;replace hessi_drm_4image with object based method which
		;speeds up successive calls in energy and det_index as
		;they were already calculated with sp_energy_binning from binned_eventlist
		;15-mar-2013, changed effb to eff so follow-on code for multi-atten doesn't break
		;
        eff = be_obj->drm_4image( this_det_index, $
        	eb_index, atten_state,$
        	powerlaw= 4 );fcheck(powerlaw,4))
        det_eff.rel[this_det_index] = eff[0]
       	;help, eff[0], effb
        ;plot, phase_map_ctr
        ;28-mar-2011, put in correction for attenuator state change
        this_cbe.atten_state = fcheck(atten_state,1) ;10-jan-2012, ras, needed for adv regularized vis

        if natten_state gt 1 and use_multi_atten then begin
        	ichng = [value_locate(time * time_bin_min/2.d0^20 + abs_time_range[0], atten_str.time) > 0 < (n_bin-1), n_bin-1]
        	for ia=1,natten_state-1 do begin
        	if atten_str.state[ia] lt 4 then begin
        		hessi_drm_4image, effi, energy_band, id, $
        		atten_str.state[ia], powerlaw=fcheck(powerlaw,4) ;,offax_position=r_map_sc[0]
        		this_cbe[ichng[ia]:ichng[ia+1]].gridtran = this_cbe[ichng[ia]:ichng[ia+1]].gridtran * (effi/eff)[0]
        		endif else begin ;atten_str.state is 4, disable counts and livetime here
        		this_cbe[ichng[ia]:ichng[ia+1]].livetime = 0.0
        		this_cbe[ichng[ia]:ichng[ia+1]].gap = 1
        		this_cbe[ichng[ia]:ichng[ia+1]].count = 0.0
        		endelse

        	endfor

        	endif

        ;plot, phase_map_ctr
        cbe[this_det_index] = Ptr_New( this_cbe ) ;, this_harmonic] = Ptr_New( this_cbe )
		binned_n_event[this_det_index, 0] = total(this_cbe.count*nogap)
    ENDIF ELSE BEGIN
        cbe[this_det_index] = Ptr_New( {hsi_calib_event} );, this_harmonic] = Ptr_New( {hsi_calib_event} )
    ENDELSE
                                ;---
                                ; remains to be considered: background
                                ;---
ENDFOR

caller->set,off_det_index = off_det_index
;add the effect of a different atten state to the gridtran term

;   Add the modulation from the source flux
self->Demodulate, cbe

det_eff.avg = avg( det_eff.rel[det_index] )
det_eff.rel[det_index] = det_eff.rel[det_index]/det_eff.avg
caller->Set, cbe_det_eff = det_eff, binned_n_event=binned_n_event, $
    used_xyoffset=xyoffset, image_atten_state= atten_state

self->Process_user_hook, cbe
self->Process_post_hook, cbe

caller->SetData, cbe

END

;--------------------------------------------------------------------------

PRO Hsi_Calib_Eventlist_Raw__Define

self = { hsi_calib_eventlist_raw,  caller: obj_new() }

END


;---------------------------------------------------------------------------
; End of 'hsi_calib_eventlist_raw__define.pro'.
;---------------------------------------------------------------------------
