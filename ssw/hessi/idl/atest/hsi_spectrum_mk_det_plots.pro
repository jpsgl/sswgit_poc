;+
; Procedure to make separate detector spectrum (count and photon) for all flares in a requested time interval.
; This is normally run via a cron job on hesperia, with /archive set.  Plots are written on hesperia 
; yyyy/mm/dd directories under /data/rhessi/det_plots.  A catalog containing flare start times, flare numbers, 
; and plot file names is also created.
; Can also be called by users with archive keyword NOT set. Plot files are written in current dir or
; dir specified by out_dir keyword.
; 
; Input keywords:   
;  tr - time range of flares to make plots for. If scalar, do that day from 00:00 to 24:00.
;  archive - if set, then set out_dir to archive dir, and add lines to plot catalog
;  force - if set, do a flare even if already in plot catalog (not used if archive=0)
;  out_dir - directory to write plot files in (overridden if archive is set)
; 
; 18-Jun-2014, Kim Tolbert
; Modifications:
; 11-Dec-2014, Kim.  If force is set, then delete old entries in the detector plot catalog and 
;  delete the old plot files for anything between the times being redone (necessary because
;  flare catalog changes)
; 09-Feb-2015, Kim. Solved why we were getting plots from 235852to235952 - If obs summ file isn't available
;    yet for next day, last atten state ends at 23:59:56.000, not sometime in next day as it should.  So if 
;    last flare on last day peak time extended beyond end of day, it was forced to be less than last atten change.  
;    Fixed to now not do any flares that extend beyond last attend end time.
;  Also, previously didn't redo flare if it was in det_plot catalog. Now check that plot file exists too.
;-

pro hsi_spectrum_mk_det_plots, tr=tr, archive=archive, force=force, out_dir=out_dir

checkvar, tr, '11-jun-2014'
tr = anytim(tr)
if n_elements(tr) eq 1 then tr=anytim(tr, /date_only) + [0.,86400.]

checkvar, archive, 0
checkvar, force, 0

no_flares = 0
flare_list_obj = obj_new('hsi_flare_list')
flare_list = flare_list_obj -> getdata(obs_time_interval = tr)
; if found any flares, concatenate them and get rid of those with bad positions
if is_struct(flare_list) then begin
  fl = temp_concat_flares(flare_list_obj)
  if ~is_struct(fl) then no_flares = 1
endif else no_flares=1

if no_flares then begin
  message, /cont, 'No flares found for ' + format_intervals(tr,/ut,/end)
  return
endif

; If archiving, then 
;   1.  if force is set, remove catalog entries and plot files between time we're redoing
;   2.  if force is not set, remove flares that have already been done from flares found so we don't do them again

if archive then begin
  cat_file = '/data/rhessi/det_plots/rhessi_det_plots.txt'
  plt_cat = hsi_read_det_plots_cat(file=cat_file, list=list, status=status)
  if ~status then stop, 'Error reading det plots catalog ', cat_file
  if force then begin
    q1 = where(plt_cat.stime ge tr[0] and plt_cat.stime lt tr[1], k1)    
    q2 = where_arr(plt_cat.flare, fl.id_number, k2)
    if k1+k2 gt 0 then begin
      rem_elements = get_uniq([q1, q2])
      qrem = where(rem_elements gt -1, krem)
      if krem gt 0 then begin
        rem_elements = rem_elements[qrem]
        print,'Removing ' + trim(krem) + 'lines from catalog and plot files.'
        remove, rem_elements, list
        prstr, file=cat_file, list
        file_delete, plt_cat[rem_elements].file, /verbose, /allow_nonexistent
      endif
    endif        
  endif else begin
    ;First check whether the flares we want to do have already been done, and that their plot file exists.
    qmatch = where_arr(plt_cat.flare, fl.id_number, kmatch)
    if kmatch gt 0 then begin
      fexist = file_exist(plt_cat[qmatch].file)
      qnofile = where(fexist eq 0,knofile)
      ; if any plot files don't exist, remove that line from catalog, re-write it, and then re-read it.
      if knofile gt 0 then begin
        rem_elements = qmatch[qnofile]
        print,'Removing ' + trim(knofile) + ' lines from catalog because the listed plot file does not exist.'
        remove, rem_elements, list
        prstr, file=cat_file, list
        plt_cat = hsi_read_det_plots_cat(file=cat_file, list=list, status=status)
      endif
    endif
    ;Now find flares that have not already been done.  This will now include those we need to do again because
    ; the plot file didn't exist, since we re-read the cat file.
    q = where_arr(fl.id_number, plt_cat.flare, count, /notequal)
    if count gt 0 then fl = fl[q] else begin
      message, /cont, 'All requested flares have already been plotted for ' + format_intervals(tr,/ut,/end)
      return
    endelse
  endelse
endif

dur = 60.

obs = hsi_obs_summary(obs_time_interval=tr+[-3600.,3600.])
flags = obs->changes()
atten = flags.attenuator_state

; If times in atten struct are -1 means no change during requested time 
multi_atten = atten.start_times[0] ne -1

; if atten changes, don't do any flares that extend after last change (probably don't have actual last change if on last day)
if multi_atten then begin
  q = where(fl.peak_time + dur/2. lt max(atten.end_times), count)
  if count gt 0 then fl = fl[q] else begin
    message, /cont, 'No flares left to process after eliminating any too clase to last atten change. Finished.'
    return    
  endelse
endif

nfl = n_elements(fl)
peak_tr = dblarr(2,nfl)
peak_tr[0,*] = fl.peak_time - dur/2.
peak_tr[1,*] = fl.peak_time + dur/2.

; If multiple atten states in time intervals, then for each flare, find an interval of 
; duration dur around the peak that does not have a change in attenuator
if multi_atten then begin

  ; Keep only atten intervals that are wider than dur + 8 sec and are not state -1 or 4
  at_wid = atten.end_times - atten.start_times
  q = where(atten.state ge 0 and atten.state le 3 and at_wid gt (dur+8.), count)
  if count eq 0 then begin
    message, /cont, 'Your duration is larger than any atten state interval. Aborting.'
    return
  endif

  at_start = atten.start_times[q]
  at_end = atten.end_times[q]
  at_state = atten.state[q]

  for ii=0,nfl-1 do begin
    tpeak = peak_tr[*,ii]
    ; see if peak interval is contained within an atten interval.  If so, leave it alone.
    q1 = where(tpeak[0] gt (at_start + 4.) and tpeak[1] lt (at_end - 4.), count)
    if count eq 0 then begin
      print,'Adjust this plot time interval for atten states: ', anytim(/vms,peak_tr[*,ii])
      ; Otherwise, move dur to start or end of closest atten interval, since only kept atten intervals that
      ; are longer than dur, don't need to check if new interval is still in one atten interval
      dif = average(tpeak) - average([[at_start],[at_end]], 2)
      dif_min = min(abs(dif), use_ind)
      print,'Atten state times: ', anytim(/vms,[at_start[use_ind], at_end[use_ind]])
      ; if mid-peak-inteval is greater than mid-atten-interval, subtract dur from end of atten interval
      ; otherwise add dur to start of atten interval
      tpeak =  dif[use_ind] gt 0 ? at_end[use_ind] - 4 - [dur, 0.] : at_start[use_ind] + 4. + [0., dur]
      peak_tr[*,ii] = tpeak
      print,'New plot time interval: ', anytim(tpeak,/vms)
    endif
  endfor
endif

out = ''
flares_plotted = fl.id_number

for ii=0,nfl-1 do begin
  if archive then begin
    ymd_dir = str_replace(strmid(anytim(peak_tr[0,ii], /ccsds),0,10), '-', '/')
    out_dir = '/data/rhessi/det_plots/' + ymd_dir + '/'
    if ~is_dir(out_dir) then file_mkdir, out_dir   
  endif
  
  print, ' '
  print,'Running hsi_spectrum_check_dets for time ' + format_intervals(peak_tr[*,ii],/ut,/end_date)
  hsi_spectrum_check_dets,time=peak_tr[*,ii],  /png, /no_gui, out_dir=out_dir, out_file=out_file
  
  if strpos(out_file, '235852to235952') ne -1 then stop, 'out_file = ', out_file
  
  if archive then begin
    if out_file eq '' then begin
      failed_flare_ind = append_arr(failed_flare_ind, ii)
    endif else begin
      new_line = anytim(fl[ii].start_time,/vms,/trunc) + ', ' + trim(fl[ii].id_number) + ', ' + out_file
      out = [out, new_line]
      print, 'New line for text file: ' + new_line      
    endelse
  endif
endfor

if archive then begin

  nout = n_elements(out)
  if nout eq 1 then begin
    message, /cont, 'None of selected flares were plotted.'
    goto, getout
  endif else out = out[1:*] ; get rid of blank first line

  remove, failed_flare_ind, flares_plotted 
  plt_cat = hsi_read_det_plots_cat(file=cat_file, list=list, status=status)
  ind = where_arr(plt_cat.flare, flares_plotted, count, /notequal)
  new_list = count gt 0 ? [list[ind], out] : out
  a=str2cols(new_list,',', /unalign)
  stime = reform(anytim(a[0,*]))
  s = sort(stime)
  ; no invalid instances of weird times before Sept 2014
  z = where(strpos(new_list[s[66000:*]], '235852to235952') ne -1, nweird)
  if nweird gt 0 then stop, 'nweird = ', nweird 
  prstr, file=cat_file, new_list[s]
  message, /cont, 'Re-wrote ' + cat_file + ' with ' + trim(nout-1) + ' new plot files.'
endif

getout:
end
