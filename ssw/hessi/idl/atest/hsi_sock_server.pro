

; Project     : HESSI
;
; Name        : HSI_SOCK_SERVER
;
; Purpose     : return host name of nearest HESSI data server
;
; Category    : sockets
;
; Outputs     : SERVER = server name
;
; Keywords    : PATH = path to data directories
;               NETWORK = 1 if network is up
;               EXCLUDE = site to exclude
;
; History     : Written, 22-Mar-2002,  D.M. Zarro (L-3Com/GSFC)
;               Modified, 22-Oct-2002, Zarro (EER/GSFC) - added Berkeley
;                to remote site list
;               Modified, 12-Mar-2003, Zarro (EER/GSFC) - added
;               searching backup server sites
;               Modified, 12-Oct-2004, Zarro (L-3Com/GSFC) - added check
;               for $HSI_SOCK_SERVER
;               Modified 10-May-2010, Kim.  Set hedc time diff to 1000 so it won't 
;                 be used for now - it's broken.
;               Modified 10-Jan-2011, Kim. HEDC was moved from eth to i4ds.  Changed Swiss site from 
;                 www.hedc.ethz.ch/data to soleil, and set time diff back to 1 so it will be used.
;               Modified 04-Aug-2014. In call to have_network, add /use_network to use IDL network object
;               11-June-2018 - added scheme to returned URL.
;
; Contact     : DZARRO@SOLAR.STANFORD.EDU
;-

function hsi_sock_server,path=path,_ref_extra=extra,network=network,$
 exclude=exclude,no_log=no_log

servers={site:'',path:'',diff:0}
servers=replicate(servers,3)

;-- Greenbelt

servers[0].site='https://hesperia.gsfc.nasa.gov'
servers[0].path='/hessidata'
servers[0].diff=-5

;-- Berkeley

servers[1].site='http://hessi.ssl.berkeley.edu'
servers[1].path='/hessidata'
servers[1].diff=-8

;-- Zurich

servers[2].site='http://soleil.i4ds.ch'
servers[2].path='/hessidata'
servers[2].diff=1

;-- check if site set via HSI_SOCK_SERVER

count=0
if ~keyword_set(no_log) then begin
 hsi_site=strup(chklog('HSI_SOCK_SERVER'))
 if is_blank(hsi_site) then hsi_site=strup(chklog('hsi_sock_server'))
 chk=where(hsi_site eq strup(servers.site),count)
endif

check_next=1b
if count eq 0 then begin

;-- exclude unwanted servers

 if is_string(exclude) then begin
  chk=where_vector(strlowcase(exclude),servers.site,rcount=rcount,rest=rest)
  if rcount gt 0 then begin
   servers=servers[rest]
  endif else begin
   message,'No servers currently available. Defaulting to Hesperia.',/cont
   check_next=0b
  endelse
 endif

;-- find nearest server based on time difference wrt UT

 tdiff=ut_diff()
 ndiff= abs(tdiff-servers.diff)
 chk=where(ndiff eq min(ndiff))
endif

nserver=servers[chk[0]]
site=nserver.site
path=nserver.path
network=have_network(site,/use_network,_extra=extra)

;-- if down, check next nearest

if ~network && check_next then begin
 message,'Trying secondary server...',/cont
 if is_string(exclude) then nexclude=[exclude,site] else nexclude=site
 site=hsi_sock_server(path=path,_extra=extra,network=network,$
                      exclude=nexclude,no_log=no_log)
endif

return,site

end