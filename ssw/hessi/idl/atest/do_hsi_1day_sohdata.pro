Pro do_hsi_1day_sohdata, test_day = test_day, otp_dir = otp_dir, $
                         start_day = start_day, short = short, $
                         no_extensions = no_extensions, _extra = _extra
  If(keyword_set(test_day)) Then tday = anytim(test_day) $
  Else tday = anytim(!stime)
  If(keyword_set(otp_dir)) Then otpdir = otp_dir $
  Else otpdir = '/disks/hessidata/metadata/hsi_1day_sohdata'
  set_logenv, 'HSI_1DAY_SOHDATA', otpdir
  set_plot,  'Z'
  hsi_filedb_read, filedb_dir = '/disks/sunny/home/dbase', fb
  fb = fb[where(fb.status_flag Ne -1 And fb.start_time Lt tday[0])]
  fb = fb[where(fb.status_flag Ne -1)]
  nfb = n_elements(fb)
;You need the day of the last fb start_time
  tnow = anytim(/ccsds, fb[nfb-1].start_time)
  this_day = strmid(tnow, 0, 11)+'00:00:00.000Z'
  this_day = anytim(this_day)
  one_day = 24.0*3600.0
  one_week = 7.0*one_day
  If(keyword_set(start_day)) Then Begin
    start_day = anytim(start_day)
  Endif Else Begin
    If(keyword_set(short)) Then start_day = this_day-one_day $
    Else start_day = this_day-one_week
;This tells us the last files created:
;And you'll want to redo the last week
    tlast = rd_tfile('$HSI_1DAY_SOHDATA/last_update.txt')
    If(tlast Ne '') Then Begin
      tlast = anytim(tlast)
      If(tlast Lt start_day) Then start_day = tlast
    Endif Else Begin
      start_day = anytim('7-feb-2002 0:00')
    Endelse
  Endelse
  start_day = start_day > anytim('7-feb-2002 0:00')
;Now you have the start and end day that you want to use
  ndays = long(round((this_day-start_day)/one_day))
  print, 'START DAY: ', anytim(/yoh, start_day)
  print, 'END DAY: ', anytim(/yoh, this_day)
;SO create the averages
  all_days = start_day+one_day*dindgen(ndays+1)
  all_days_str = strmid(anytim(/ecs, all_days), 0, 10)
  c = 0
  For j = 0, ndays-1 Do Begin
    time_range = all_days[j:j+1]
    print, 'PROCESSING: ', anytim(/yoh, time_range[0]),  ' -- ',  $
      anytim(/yoh, time_range[1])
    hsi_1day_sohdata, time_range, tags, pwpa_av, sigma, npak, names, $
      plot_lo,  plot_hi, _extra = _extra
    heap_gc
    If(pwpa_av[0] Ne -1) Then Begin
      If(c Eq 0) Then Begin
        ntags = n_elements(tags)
        pwpa_day = fltarr(ntags, ndays)
        sigma_day = pwpa_day
        npak_day = pwpa_day
      Endif 
      c = c+1
      pwpa_day[*, j] = pwpa_av
      sigma_day[*, j] = sigma
      npak_day[*, j] = npak
    Endif
  Endfor
;Now you need to get the old files, read in, and rewrite
  ntags = n_elements(tags)
  c = 0
  For j = 0, ntags-1 Do Begin
    hsi_rd_1day_sohdata, tags[j], day, pwpa, sigma, npak
    If(pwpa[0] Ne -1) Then Begin
      nold = n_elements(pwpa)
      If(c Eq 0) Then Begin
        pwpa_old = fltarr(ntags, nold)
        sigma_old = pwpa_old
        npak_old = pwpa_old
        day_old = day
      Endif
;fix for when the days are not equal, use days for tags[0] (BATT_VOLT)
;for all, jmm, 2017-01-23, only took 15years
      c = c+1
      nday = n_elements(day)
      If(nday Eq nold) Then Begin
         pwpa_old[j, *] = pwpa
         sigma_old[j, *] = sigma
         npak_old[j, *] = npak
      Endif Else If(nday Gt nold) Then Begin
         pwpa_old[j, *] = pwpa[0:nold-1]
         sigma_old[j, *] = sigma[0:nold-1]
         npak_old[j, *] = npak[0:nold-1]
      Endif Else Begin ;nday Lt nold, as for 2017-01-21
         pwpa_old[j, 0:nday-1] = pwpa
         sigma_old[j, 0:nday-1] = sigma
         npak_old[j, 0:nday-1] = npak
      Endelse
    Endif
  Endfor
;Ok, now append the files
;But first deal with overlap
  If(c Gt 0) Then Begin
    upto_day = where(day Eq all_days_str[0])
    If(upto_day[0] Eq -1) Then upto_day = n_elements(pwpa_old[0, 0:*])
    If(upto_day[0] Gt 0) Then Begin
      nnew = n_elements(pwpa_day[0, *])
      nold = n_elements(pwpa_old[0, 0:upto_day-1])
      pwpa_all = fltarr(ntags, nnew+nold)
      sigma_all = pwpa_all
      npak_all = pwpa_all
      day_all = strarr(nnew+nold)
      pwpa_all[*, 0:upto_day-1] = pwpa_old[*, 0:upto_day-1]
      sigma_all[*, 0:upto_day-1] = sigma_old[*, 0:upto_day-1]
      npak_all[*, 0:upto_day-1] = npak_old[*, 0:upto_day-1]
      day_all[0:upto_day-1] = day_old[0:upto_day-1]
      pwpa_all[*, upto_day:*] = pwpa_day
      sigma_all[*, upto_day:*] = sigma_day
      npak_all[*, upto_day:*] = npak_day
      day_all[upto_day:*] = all_days_str[0:ndays-1]
    Endif Else Begin
      pwpa_all = pwpa_day
      sigma_all = sigma_day
      npak_all = npak_day
      day_all = all_days_str[0:ndays-1]
    Endelse
  Endif Else Begin
    pwpa_all = pwpa_day
    sigma_all = sigma_day
    npak_all = npak_day
    day_all = all_days_str[0:ndays-1]
  Endelse
  If(Not keyword_set(no_extensions)) Then Begin
;Add differences of temperatures here
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'IRAD1T', 'ICT1T', $
      'HRC to Cold Tip Temp #1 Diff. [C]', 120.0, 205.0, /add_273
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'IRAD2T', 'ICT1T', $
      'Radiator to Cold Tip Temp #1 Diff. [C]', 120.0, 185.0, /add_273
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'ITST', 'ICT1T', $
      'Thermal Shield to Cold Tip Temp #1 Diff. [C]', 70.0, 110.0
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'IRAD1T', 'ICT2T', $
      'HRC to Cold Tip Temp #2 Diff. [C]', 120.0, 205.0, /add_273
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'IRAD2T', 'ICT2T', $
      'Radiator to Cold Tip Temp #2 Diff. [C]', 120.0, 185.0, /add_273
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'ITST', 'ICT2T', $
      'Thermal Shield to Cold Tip Temp #2 Diff. [C]', 70.0, 110.0
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'ILGT1T', 'IUGT1T', $
      'Lower to Upper Grid Temp Diff. [C]', -1.0, 4.0
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'IRAD1T', 'IRAD2T', $
      'HRC to Radiator Temp Diff. [C]', 10.0, 40.0
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi,  'IRAD1T', 'SPEC_MID_TEMP', $
      'HRC to Mid Spectrometer Radiator Temp Diff. [C]', 5.0, 40.0
    hsi_1day_sohdata_diff, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi, 'SPEC_MID_TEMP', 'IRAD2T', $
      'Mid Spectrometer Radiator to Radiator Temp Diff. [C]', -10.0, 10.0
    hsi_1day_sohdata_mult, tags, names, pwpa_all, sigma_all, npak_all, $
      plot_lo, plot_hi, 'BATT_VOLT', 'SA_CURR', $
      'Battery Voltage times Solar Array Current [W]', 175.0, 225.0
;New for july 2003! cryocooler efficiency
;heat reject collar temp / (cold tip temp * cryocooler power)
    hrc = where(tags Eq 'IRAD1T')
    ctt1 = where(tags Eq 'ICT1T')
    ctt2 = where(tags Eq 'ICT2T')
    cpwr = where(tags Eq 'CRYOPOWER')
    If(hrc[0] Ne -1 And ctt1[0] Ne -1 $
       And ctt2[0] Ne -1 And cpwr[0] Ne -1) Then Begin
      temp1 = (273.0+pwpa_all[hrc[0], *])/(pwpa_all[ctt1[0], *]*pwpa_all[cpwr[0], *])
      sig1 = sqrt(sigma_all[hrc[0], *]^2+sigma_all[ctt1[0], *]^2+sigma_all[cpwr[0], *]^2)
      temp2 = (273.0+pwpa_all[hrc[0], *])/(pwpa_all[ctt2[0], *]*pwpa_all[cpwr[0], *])
      sig2 = sqrt(sigma_all[hrc[0], *]^2+sigma_all[ctt2[0], *]^2+sigma_all[cpwr[0], *]^2)
      pwpa_all = [pwpa_all, temp1, temp2]
      sigma_all = [sigma_all, sig1, sig2]
      tags = [tags, 'CRYO_EFF1', 'CRYO_EFF2']
      names = [names, 'Cryocooler Efficiency 1 (W^(-1))', 'Cryocooler Efficiency 2 (W^(-1))']
      npak_all = [npak_all, npak_all[ctt1[0], *], npak_all[ctt2[0], *]]
      plot_lo = [plot_lo, 0.0, 0.0]
      plot_hi = [plot_hi, 0.1, 0.1]
    Endif
;Add data for app_id=1 data recovery here
    tags = [tags, 'data_frac001']
    names = [names, 'Fraction of recovered SOH Data']
    plot_lo = [plot_lo, 0.0]
    plot_hi = [plot_hi, 1.2]
    pwpa_all = [pwpa_all, npak_all[0, *]/8640.0]
    sigma_all = [sigma_all, replicate(1.0/8640.0, 1, n_elements(day_all))]
    npak_all = [npak_all, npak_all[0, *]]
  Endif
;Output
  ntags = n_elements(tags)
  txtfiles = strarr(ntags)
  plotfiles = txtfiles
  For j = 0, ntags-1 Do Begin
    hsi_wrt_1day_sohdata, tags[j], day_all, reform(pwpa_all[j, *]), $
      reform(sigma_all[j, *]), reform(npak_all[j, *]), names[j], $
      filename = filex_j, pfilename = pfilex_j, /plot, $
      ylimits = [plot_lo[j], plot_hi[j]]
    txtfiles[j] = filex_j
    plotfiles[j] = pfilex_j
  Endfor

  openw, 1, concat_dir('$HSI_1DAY_SOHDATA', 'last_update.txt')
  printf, 1, anytim(/ccsds, this_day)
  close, 1
  message, /info, '****FINISHED SUCCESSFULLY****'
;Ok, you need an index.html file in the directory
;  html_file = 'index_sohdata.html'
;  predir = 'data'
;  openw, unit, html_file, /get_lun
;  printf, unit, '<HTML>'
;  printf, unit, '<HEAD>'
;  printf, unit, '<TITLE>index.html</TITLE>'
;  printf, unit, '</HEAD>'
;  printf, unit, '<BODY>'
;  printf, unit, '<H2> Index of RHESSI SOH data 1 day Averages: </H2>'
;  for j = 0,  ntags-1 do begin
;    printf, unit, '<p> '+names[j]
;    printf, unit, '<p> <A HREF='+concat_dir(predir, txtfiles[j])+'> '+$
;      txtfiles[j]+'</A> (TEXT)'
;    printf, unit, '<p> <A HREF='+concat_dir(predir, plotfiles[j])+'> '+$
;      plotfiles[j]+'</A> (GIF)'
;  endfor
;  printf, unit, '<p> '+!stime
;  printf, unit, '</BODY>'
;  printf, unit, '</HTML>'
; free_lun, unit

End

