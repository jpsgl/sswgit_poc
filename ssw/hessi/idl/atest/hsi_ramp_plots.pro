;+
; :Description:
;    This is used for plotting the eventlist over short time intervals with the detected ramps. Not meant for
;    general use. This is a tool for the data analysis team.
;
; :Examples:
; To make a PS file for a series of default times and all segments
; hsi_ramp_plots,   use_ps=01, interactive=0,yrang=[1,1000], xrang=[0,10.], oe=oe, ramp_ntest=6, ramp_epeak=25., seg=ss,  star=start_time_list
; 
; To use interactively
; hsi_ramp_plots,   use_ps=0, interactive=1 ,yrange = [1,1000], xrange = [0,10.], oe=oe, ramp_ntest=6, ramp_epeak=25., seg=5,  start= start_time
; here start_time should be a single start time for a 10 second interval, i.e. xrange
; 
; :Keywords:
;    oe              - hsi_eventlist object, will be created if not defined
;    start_time_list - array of start times for eventlist accumulation (relative to xrange)
;    seg_list        - integer array of segments (0-8) to include if segment is available
;    use_ps          - Send plot to postscript file
;    xrange          - default, [0,10], seconds, interval to use after start time
;    wait_time       - waiting time in seconds before next segment ramp plot if not postscript
;    yrange          - channel plotting range, 200 is default
;    ylog            - default 1, if set, use log scaling on y axis
;    psfile          - postscript filename, default 'Ramps_'+time2file( anytim( start_list[0] )) (.ps)
;    interactive     - generate plotman obj
;    The ramp control parameters are found in hsi_data_gap_control and may be entered into the eventlist object, oe, directly
;    or through _extra
;    ;Normally when lowering epeak then ntest should be increased to avoid wiping out good data
;      ;Can be evaluated using hsi_ramp_plots
;      var.ramp_epeak = 30. ; keV threshold for ntest of ramp, higher than bulk threshold
;      var.ramp_ntest = 6 ;number of consecutive values over bulk threshold required to be a ramp
;      var.ramp_emax = 300. ;cap on energy of events for ramp analysis.
;      var.ramp_emin = 20. ;once below emin, the ramp is over, emin used may be lower depending
;      ;on the attenuator state, emin may be modified when used
;      var.ramp_ebulk = 40. ;upper limit on photon energy when searching for the bulk median

; :Author: raschwartz, 22-mar-2017
;-
pro hsi_ramp_plots, $
  oe = oe, $
  start_time_list = start_time_list, $
  seg_list = seg_list, $

  use_ps = use_ps, $

  xrange = xrange, $
  wait_time = wait_time, $

  yrange = yrange, $
  ylog = ylog, $
  interactive = interactive, $
  psfile = psfile, $
  mark_intervals = mark_intervals, $
  _extra = extra

  he = 'hsi_eventlist'
  oe = is_class(oe,he)?oe :obj_new(he)
  if keyword_set( extra ) then oe->set, _extra = extra

  epeak = oe->get( /ramp_epeak )
  ntest = oe->get( /ramp_ntest )
  ebulk = oe->get( /ramp_ebulk )




  default, seg_list, indgen(9)
  default, xrange,[0,10]
  default, wait_time, 1

  default, yrange, [10, 200]
  default, ylog, 1
  default, use_ps, 1
  default, interactive, 0
  default, start_time_list, ['25-apr-2014 00:20:10', '25-apr-2014 08:44:20', '28-jan-2017 21:19:35', $
    '21-jan-2017 07:26:30', '10-jan-2017 16:45:00', '07-jan-2016 06:15:00', '08-jan-2015 04:17:00','08-jan-2015 04:40:00', '25-apr-2014 00:20:24',$
    '10-sep-2017 15:59:00', '10-sep-2017 17:30:00' ]


  if use_ps then interactive = 0
  if interactive then use_ps = 0

  ; obs_time=anytim('28-jan-2017 21:19:35')+ xrange+[0.,2]
  ; obs_time=anytim('25-apr-2014 00:20:10')+ xrange+[0.,2]
  ; obs_time=anytim('25-apr-2014 08:44:20')+ xrange+[0.,2]

  ord = sort( anytim( start_time_list) )
  start_list = start_time_list[ord]
  ;start_list = '07-jan-2016 06:15'
  dname = !d.name
  info  = '_epeak_'+strtrim( fix( epeak ), 2) + '_ntest_'+strtrim( ntest, 2) + '_bulk_'+strtrim( fix(ebulk), 2)
  if keyword_set(use_ps) then begin

    sps,/land, /color
    default, psfile, 'Ramps_'+time2file( anytim( start_list[0] ))
    psfile = concat_dir( file_dirname( psfile ), file_basename( psfile ) + info+ '_.ps' )
    if file_exist( psfile ) then file_delete, psfile
    device, filename=psfile
  endif
  linecolors
  two20 = 2d^20

  for jj = 0, n_elements(start_list) - 1 do begin
    start_time = start_list[jj]
    obs_time = anytim( start_time ) + xrange + [0,2]
    ;if is_object( oe ) then obj_destroy, oe


    oe->set, obs_time = obs_time
    lld = oe->Get(/DP_LLD)

    obs_time = oe->get(/obs_time_int)
    cf = hsi_get_e_edges( /coef, gain_time = obs_time[0])
    lld = (long( (-cf[*,0] - 5)/cf[*,1] ))[0:17] > 0


    oe->set, time_range=fltarr(2), dp_enable = 0
    ee = oe->getdata(/need, obs_time=obs_time, /attach_attenuator)
    ramp_parm = oe->get( /control, /ramp ) ;ramp_emin, ramp_ebulk, ramp_epeak, ramp_emax, ramp_ntest

    h = histogram( min=0,max=8, ee.a2d_index, rev=rad )
    ptim, obs_time
    print,h
    for iseg = 0, 8 do begin
      if is_member( iseg, seg_list) and h[iseg] gt 100 then begin
        seg = iseg
        cfi = cf[ seg, *]
        cfpoly = [ -cfi[0], 1.] / cfi[1] ;offset and gain, to convert energy to channel
        ch_peak = poly( epeak, cfpoly )

        sel = reverseindices( rad, seg )
        es  = ee[sel]
        t = es.time - es[0].time
        c = es.channel
        help, ch_peak, seg
        itramp = hsi_dp_ramp( c, t, cfi, seg= seg, _extra = ramp_parm)
        help, itramp
        plot, ylog = ylog, yrang=yrange, /ystyle, t/two20, c<yrange[1], psym=1, symsiz=.2, xran = xrange-xrange[0],$
          xcharsize=2, ymargin=[8,2], $
          xtitle=anytim(obs_time[0],/yoh)+ ' Seg: '+strtrim(seg,2)+' Ntest: '+strtrim(ntest,2) +' ch_peak: '+strtrim(ch_peak,2)
        q=(value_locate( itramp[*],lindgen(n_elements(t))) mod 2) eq 0
        z = where(c gt 80 and q, nz, comp=zc)
        q = where( q )
        cc = c
        cc[zc] = -100
        ut_ref = es[0].time/two20 + oe->get(/ut_ref)
        default, mark_intervals, 0
        if n_elements( itramp ) ge 2 then begin
          xramp_limits = (t[itramp]/two20)
          if mark_intervals then oplot_xmarkers, intervals= xramp_limits
          utramp = xramp_limits + ut_ref
          if nz ge 1 then oplot, t[q]/two20, c[q]<yrange[1], psym=1, symsize = 0.2, col=2
        endif

        if interactive then begin
          ut = t/two20 + ut_ref
          ut = ut - obs_time[0]
          ;move ut[0] to the nearest millsec to prevent overplotting offsets
          ut[0] = anytim( anytim( ut[0], /ext) ) ;ext format is only acc to millisec
          utobj=obj_new('utplot', ut, [[c<yrange[1]]], dim1_id=['channel'] , $
            utbase = obs_time[0], $
            addplot_name='hsi_oplot_ramps', addplot_arg = $
            {intervals: utramp, mark_intervals: mark_intervals, ut:1, xut: ut, chan: c<yrange[1]})
          utobj->plotman,desc='Ramp v Time', psy=3, yrange=yrange, /xstyle
        endif
      endif
      wait, wait_time
    endfor
  endfor
  if keyword_set( use_ps ) then begin
    device, /close

  endif
  set_plot, dname
  obj_destroy, oe
end