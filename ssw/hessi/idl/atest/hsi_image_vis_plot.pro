;+
; Name: hsi_image_vis_plot
;
; Purpose: Plot the observed visibilities and the visibilities computed from an image
;
; Method: If the observed visibilities are not available in the image object, then it creates them
;  just for the time and energy requested (or gets them from a visfile if provided). For photon vis, does this:
;    First gets visibilities for detectors used in image (normalized, if set, based on those dets).  Then gets
;    visibilities for all detectors (norm, if set, based on all detectors). Merges the two sets of vis, so
;    that the plot shows vis for the dets used in image based on those dets, and vis for the dets not used in image
;    based on all dets.
;  For regularized vis, doesn't try to show the detectors not used in image.
;  Calls vis_map2vis to create visibilities from the image. Calls hsi_vis_select to return the
;  position angles. Plots the amplitude of the
;  observed visibilities and their errors for each position angle for each detector (even those that weren't
;  used in image). Overplots the amplitude of the image visibilities. Plot is sent to screen or PS file. Chi-square
;  values for comparison of two sets of visibilities for all detectors, all detectors used in image, and each detector
;  separately, are displayed on plot and can be returned in keyword argument.
;
;
; Input Keywords:
;  imobj - image object
;
; Optional Input Keywords:
;  image - if set, use this image instead of calling getdata on image object (still need image object to correspond to
;    image, since will be getting other values like detectors, pixel_size, xyoffset, etc from object)
;  tb_index - Time index to plot (defaults to 0)
;  eb_index - Energy index to plot (defaults to 0)
;  last - if set, uses highest tb_index and eb_index available in image object
;  force - if set, and didn't pass in image, and image object needs update, will remake image without asking
;  visfile - if visibilities aren't available from image object without reprocessing, and visfile is supplied, get vis from file
;  ps - if set, send output to PS file
;  jpeg - If set, send plot to jpeg file
;  file_plot - name of plot file to use. Default is to autoname to
;    'viscomp_alg_prefix_t0_e0-e1kev.xx' where alg_prefix is short prefix name for image algorithm, t0 is start time of image,
;    and e0,e1 are start end of energy bin.
;  dir_plot - directory to use for output plot file (default is viscomp_plot_dir parameter, or if that's blank, current dir)
;  window - window number to use (default is whatever is in viscomp_window parameter)
;  yrange - yrange of plot, (defaults to range of last 20 elements of obsvis amplitudes to avoid bad values on finer collimators)
;  _extra - any keywords to pass to plot program
;  quiet - default is 1, set quiet=0 to see the vis combine/edit/normalize messages
;
; Output Keywords:
;  chivals - structure containing reduced chi-square for all vis and for each detector of comparison between observed and expected
;    visibilities.  Computed by total( (expx-obsx)^2 + (expy - obsy)^2) / obssig^2 ) / (np - 1), where expx, expy are the x and y
;    values of the expected vis (from image), obsx, obsy are the observed vis, obssig is sigma in amplitude of observed vis, and
;    np is the number of points used.
;
; Written: Kim Tolbert Jan. 2017
; Modifications:
; 3-Jul-2017, Kim. Even if vis are not already made, if we have image obj with a single time and energy, calib eventlist
;   is still available, so use that to make vis. (previously if vis not made, always made new obj and made vis from scratch).
;   Also, right side of red box showing which dets were used in image didn't show for Det 9, so make it 9.99 instead of 10.
;   Also, for reg vis, conjugates are always combined, so can't use value of vis_conj flag to control max angle to plot.
; 14-Sep-2017, Kim. Fixed red chi2 calc to divide by 2*nvis since using x and y components separately. Modified chi2 labels
;   on plot to make clearer and indicate that it's red chi2, not full. Added 'albedo removed' label if vf_fwdfit had albedo
;   component (and one of primary sources had albedo_apply on) - this plot is made from image so calculated vis don't include
;   albedo component (whereas similar plot from vis_fwdfit_plot includes albedo since has to compare to observed vis)
; 31-Oct-2017, Kim. Check that vf_srcin is a structure before using (for VIS_FWDFIT, when checking for albedo)
; 24-Jan-2018, Kim. Added Front or Rear to X title (and changed 'Subcollimator' to 'Detector')
;   Also, compute yrange from last 20 elements of vis from image (as well as last 20 elements of obs vis)
; 14-Mar-2018, Kim. If plot dev is already PS, then don't open or close PS file, just adding to a PS file.
; 19-Apr-2018, Kim. Fixed bug (thanks Jana K.) where dets_list index could be -1 (vis contains only the detectors that are ON)
; 08-May-2018, Kim. Added jpeg option. Changed file_ps keyword to file_plot.  Previously setting file_ps enabled PS mode, now it
;   doesn't.  Changed dir_ps keyword to dir_plot.
; 31-July-2018, Kim. For CLEAN images, remove residuals (only possible if clean_regress_combine = 'full_resid' or 'disable') 
;   (since vis_map2vis shouldn't ever deal with negative values in images). Changed input arg to image_in to protect it from changes.
;   Changed order of plotting so that symbols will be on top of blue error lines. Changed character sizing, and made jpeg plot lines
;   thicker.
; 25-sep-2018, Kim. Added quiet keyword. Made /quiet the default to vis getdata calls. Added this_det_index_mask to calls to getdata 
;   to get the right detectors (something else changed to make this necessary, not sure what). Set clean_component map to all zeroes
;   of correct dimension if image didn't succeed.
;-

pro hsi_image_vis_plot, imobj=imobj, image=image_in, tb_index = tb_index, eb_index = eb_index, last=last, force=force, $
  visfile=visfile, visobj=visobj, ps=ps, jpeg=jpeg, file_plot=file_plot, dir_plot=dir_plot, window=pwindow, $
  yrange=yrange, $
  chivals=chivals, $
  quiet=quiet, $
  _extra=_extra

  checkvar, tb_index, 0
  checkvar, eb_index, 0
  checkvar, quiet, 1

  checkvar, pwindow, imobj->get(/viscomp_window)
  checkvar, ps, imobj->get(/viscomp_ps_plot)
  checkvar, jpeg, imobj->get(/viscomp_jpeg_plot)
  checkvar, dir_plot, imobj->get(/viscomp_plot_dir)

  ; can't use checkvar for this because calls getdata even if image is passed in, causing an infinite loop

  det_index_mask = imobj->get(/det_index_mask)
  front = imobj->get(/front_segment)
  front_rear = front ? 'Front ' : 'Rear '
  pixel_size = imobj->get(/pixel_size)
  area = pixel_size[0] * pixel_size[1]
  xyoffset = imobj->get(/xyoffset)

  tbins = imobj->getaxis(/ut, /edges_2)
  nt = n_elements(tbins[0,*])
  ebins = imobj->getaxis(/energy, /edges_2)
  nen = n_elements(ebins[0,*])

  if keyword_set(last) then begin
    tb_index = nt - 1
    eb_index = nen - 1
  endif

  save_use_single = imobj -> get(/use_single_return_mode)
  imobj -> set, /use_single_return_mode

  if tb_index gt nt-1 or eb_index gt nen-1 then begin
    message, /cont, 'Invalid tb_index or eb_index. You have ' + trim(nt) + ' times and ' + trim(nen) + ' energies in your object. Aborting.'
    goto, getout
  endif

  if n_elements(image_in) ne 0 then begin
    image = image_in
  endif else begin
    if ~keyword_set(force) then begin
      if imobj -> need_update() then begin
        msg = 'Image(s) have not been generated yet.  Are you sure you want to do that now?'
        answer = dialog_message(msg, /question)
        if answer eq 'No' then goto, getout
      endif
    endif
    image = imobj->getdata(t_idx=tb_index, e_idx=eb_index)
  endelse

  alg = hsi_get_alg_name(imobj->get(/image_algorithm))
  more_label = ''

  if alg eq 'Clean' then begin
    regress_combine = imobj->get(/clean_regress_combine)
    if regress_combine eq 'full_resid' or regress_combine eq 'disable' then begin
      use_obj = get_caller() eq 'HSI_IMAGE_SINGLE::GETDATA' ? imobj->get(/obj, class='hsi_image_single') : imobj
      resid = use_obj->get(/clean_resid_map)
      if total(resid) ne 0. then begin
        unit_scale = use_obj->get(/alg_unit_scale)
        c_coef = use_obj->get(/clean_coef_regress)
        is_cart = use_obj->get(/mpat_coord) eq 'CART'
        comp = use_obj->get(/clean_component_map)
        if n_elements(comp) eq 1 then comp = image*0.  ; if image never got started comp will be scalar 0, so just make dimensions correct
        ; image without resid is component map times regression coefficient (converted to xy if in annsec) and divided by unit_scale
        image = ( is_cart ? comp * c_coef : hsi_annsec2xy(comp * c_coef, imobj, /finescale) ) / unit_scale
        more_label = ', Residual Removed'
      endif
    endif
  endif

  if alg eq 'VIS FWDFIT' then begin
    ; for vf_fwdfit, check if albedo component is enabled, AND one of the sources has albedo_apply on
    vf_srcin = imobj->get(/vf_srcin)
    if is_struct(vf_srcin) then begin
      alb_ind = where(vf_srcin.srctype eq 'albedo', compl=notalb_ind, count)
      if count gt 0 && total(vf_srcin[notalb_ind].albedo_apply) gt 0 then more_label = ', Albedo removed'
    endif
  endif
  vis_type = imobj->get(/vis_type)
  reg_electron = vis_type eq 'reg_electron'
  reg_vis = stregex(vis_type,/bool,/fold,'reg') ; regularized visibilities ?
  alg_prefix = (hsi_alg_units(alg)).prefix
  if alg_prefix eq '' then alg_prefix = 'bproj'
  vis_edit = imobj->get(/vis_edit)
  vis_normalize = imobj->get(/vis_normalize)
  vis_conjugate = imobj->get(/vis_conjugate)

  ; Can't use vis_conjugate for deciding whether to plot up to 180 or 360 degrees, because reg vis always combine conj
  ; regardless of how flag is set. Instead set maxpa by looking at range of paout that we get from hsi_vis_select call below.
  ;  vis_conj = imobj->get(/vis_conjugate)
  ;  maxpa = vis_conj ? 180. : 360.

  ; See if visibilities have already been generated.  If so, use them. If not, just generate them for the time and energy
  ; we want right now (or get them from visfile if provided).
  ; visin will be vis for dets used in image
  ; for non-regularized vis, visin_alldets will be vis for all dets
  if ~exist(visobj) then visobj = imobj->get(/obj,class='hsi_visibility')
  uptodate = ~visobj->need_update()

  ; Either the visibilities have already been made (uptodate = 1), or if there's only one image (nt=1 and nen=1), then
  ; we can use the calib eventlist that's already been accumulated and make the visibilities from that in the object chain.
  if uptodate or (nt+nen eq 2) then begin
    visin = visobj->getdata(tb_index = tb_index, eb_index = eb_index, quiet=quiet);, det_index_mask=bytarr(9)+1b)
    if ~reg_vis then visin_alldets = visobj->getdata(tb_index = tb_index, eb_index = eb_index, this_det_index_mask=bytarr(9)+1b, quiet=quiet)
  endif else begin
    ; Otherwise, either use a visibility file if provided, or make the visibilities for just the time and energy requested
    ; (don't want to make vis for all time and energy, which could be many, just to make this plot)
    visobj = hsi_visibility()
    if file_exist(visfile) then begin
      visobj->set, vis_input_fits=visfile
;      visobj->set, det_index_mask=det_index_mask
      visobj->read  ; have to read in photon vis first, then can change vis_type
      visobj->set, vis_type=vis_type
      visobj->set, vis_normalize=vis_normalize, vis_edit=vis_edit, vis_conjugate=vis_conjugate
      visin = visobj->getdata(tb_index=tb_index, eb_index=eb_index, quiet=quiet, this_det_index_mask=det_index_mask)
      if ~reg_vis then visin_alldets = visobj->getdata(tb_index=tb_index, eb_index=eb_index, this_det_index_mask=bytarr(9)+1b, quiet=quiet)
    endif else begin
      if reg_vis then begin
        message,'For regularized visibilities, because of the restrictions on energy binning, ', /cont
        print, ' you must pass in an image object with the visibilities already recreated, or a visibility file.'
        print, ' Can not make visibilities for just one time bin if regularizing.  Aborting.'
        goto, getout
      endif
      ; Use most of control parameters from image object, and make vis for just this time and energy bin.
      control = imobj->get(/control)
      control = rem_tag(control, ['im_input_fits', 'energy_band', 'time_range', 'filename'])
      visobj->set,_extra=control
      visobj->set, im_time_interval=tbins[*,tb_index], im_energy_binning=ebins[*,eb_index]
      visin = visobj->getdata(quiet=quiet)
      if ~reg_vis then visin_alldets = visobj->getdata(det_index_mask=bytarr(9)+1b, quiet=quiet)
    endelse
  endelse

  ; for non-regularized vis, merge vis for detectors not used in image (in visin_alldets) with visin (only has dets used
  ; in image, so now visin has vis for all dets. This is necessary if we want to show the vis that were actually used in
  ; image for the detectors that were used in image (normalization is different depending on which detectors used)
  if ~reg_vis then begin
    dets = where( det_index_mask eq 0, count )
    if count gt 0 then begin
      dets_list = where_arr( visin_alldets.isc, dets, kdets_list )
      if kdets_list gt 0 then visin = [visin, visin_alldets[dets_list]]
    endif
  endif

  ; Now we have visibilities from observed data for requested time and energy index in visin structure

  ampobs = abs(visin.obsvis)
  dummy = hsi_vis_select(visin, PAOUT=paout)       ; calculate paout
  maxpa = max(paout) gt 180. ? 360. : 180.
  scpa = visin.isc+1 + (paout/maxpa MOD 1)              ; (sc# + pa/180) is used as abscissa for plots
  visx = FLOAT(visin.obsvis)
  visy = IMAGINARY(visin.obsvis)

  ; Now create visibilities generated from image, will be in vismap structure.

  ; Make a map structure for image, multiply image by area to put in units of photons/cm2/s
  map = make_map( image * area, $
    xc=xyoffset[0], $
    yc=xyoffset[1], $
    dx=pixel_size[0], $
    id= 'RHESSI '+ trim( ebins[0,eb_index], '(f12.1)' ) + $
    '-' + trim( ebins[1,eb_index], '(f12.1)' ) + ' keV', $
    dur    = tbins[1,tb_index] - tbins[0,tb_index], $
    xunits = 'arcsec', $
    dy=pixel_size[1], $
    time=anytim(tbins[0,tb_index],/tai), $
    yunits = 'arcsec',$
    desc = desc )

  vismap = vis_map2vis(map, dummy, visin)
  if ~is_struct(vismap) then begin
    message,'Aborting plot for tb_index, eb_index = ' + trim(tb_index) + ', ' + trim(eb_index), /info
    goto, getout
  endif

  ampobsmap = abs(vismap.obsvis)
  dummy = hsi_vis_select(vismap, PAOUT=paoutmap)       ; calculate position angle
  scpamap = vismap.isc+1 + (paoutmap/maxpa MOD 1)              ; (sc# + pa/180) is used as abscissa for plots
  vismapx = FLOAT(vismap.obsvis)
  vismapy = IMAGINARY(vismap.obsvis)

  visdiff = SQRT((visx-vismapx)^2 + (visy-vismapy)^2)

  nvis = n_elements(vismapx)
  ; number of degrees of freedom is nvis*2 since we're using the x and y components separately
  chisq = total( f_div(((vismapx-visx)^2 + (vismapy-visy)^2), visin.sigamp^2 )) / (2*nvis-1)
  chisq_det = fltarr(9)
  for id=0,8 do begin
    ind = where(visin.isc eq id, nind)
    chisq_det[id] = 0.
    ; number of degrees of freedom is nind*2 since we're using the x and y components separately
    if nind gt 0 then chisq_det[id] =  total( f_diV(((vismapx[ind]-visx[ind])^2 + (vismapy[ind]-visy[ind])^2), visin[ind].sigamp^2 )) / (2*nind-1)
  endfor
  udet = where(det_index_mask)
  q = is_member(visin.isc, udet)
  used = where(q eq 1)
  nused = n_elements(used)
  ; number of degrees of freedom is nused*2 since we're using the x and y components separately
  chisq_detused = total( f_div(((vismapx[used]-visx[used])^2 + (vismapy[used]-visy[used])^2), visin[used].sigamp^2 )) / (2*nused-1)
  chivals = {chisq: chisq, chisq_det: chisq_det, chisq_detused: chisq_detused}

  add_to_ps = !d.name eq 'PS'
  ps = keyword_set(ps) or add_to_ps
  jpeg = ~ps and keyword_set(jpeg)

  tvlct, rr,gg,bb, /get
  thisdevice = !d.name
  linecolors, /quiet
  charsize = 1.
  leg_size=1.
  thick = 1
  bw = 255

  if ps or jpeg then begin

    charsize = .8
    leg_size = .8
    if ~is_string(file_plot) then begin
      t0 = time2file(tbins[0,tb_index],/sec)
      e0=trim(ebins[0,eb_index])
      e1=trim(ebins[1,eb_index])
      file_plot = 'viscomp_'+alg_prefix+'_'+t0+'_'+e0+'_'+e1+'kev.' + (ps ? 'ps' : 'jpeg')
    endif
    if is_string(dir_plot) then begin
      if ~is_dir(dir_plot) then file_mkdir, dir_plot
      full_file_plot = concat_dir(dir_plot, file_plot)
    endif else full_file_plot = file_plot
    if ps then begin
      if ~add_to_ps then ps, full_file_plot, /land,/color
      thick = 3.
      bw = 0
    endif
    if jpeg then begin
      xsize=700
      ysize=500
      thick = 1.6
      set_plot, 'z'
      device, set_resolution=[xsize, ysize]
    endif

  endif else begin

    save_window = !d.window
    if ~is_wopen(pwindow) then begin
      pwindow = next_window(/user)
      device, get_screen_size=sc
      window, pwindow, xsize=800<.9*sc[0], ysize=500<.9*sc[1]
    endif
    wset, pwindow
    imobj->set, viscomp_window=pwindow

  endelse

  title = 'Visibilities - Observed, From Image, and Differences'
  units = reg_electron ? 'electrons cm!U-2!N s!U-1!n kev!U-1!n' : 'photons cm!U-2!N s!U-1!n'
  xtitle = front_rear + 'Detector + Position Angle / 180.'

  ; If yrange not passed in, compute max of last 20 elements of obs. vis. amplitudes to avoid scaling to bad
  ; values on finer collimators
  if ~keyword_set(yrange) then begin
    s = sort(visin.isc)
    yrange = [ 0., max([ampobs[last_nelem(s,20)], ampobsmap[last_nelem(s,20)]]) ]
  endif

  plot,  scpa, ampobs, /nodata, xrange=[1,10], /xst, xtickinterval=1, xminor=-1, $
    title=title, xtitle=xtitle, ytitle=units, yrange=yrange, charsize=charsize, thick=thick, _extra=_extra
  ; draw vertical dotted lines at each detector boundary
  for i=2,9 do oplot, i+[0,0], !y.crange, linestyle=1
  ; draw red box around each detector used in image
  for i=0,n_elements(udet)-1 do oplot, udet[i]+1 + [0,1,1,0,0] < 9.99, [!y.crange[0]+[0,0],!y.crange[1]+[0,0],!y.crange[0]], col=2, thick=thick
  ; draw error bars on each observed vis amplitude
  errplot, scpa, (ampobs-visin.sigamp > !y.crange[0]), ampobs+visin.sigamp < !y.crange[1], width=0, COLOR=10, thick=thick
  oplot, scpa, ampobs, psym=7, thick=thick
  oplot, scpamap, ampobsmap, psym=4, col=2, thick=thick
  oplot, scpa, visdiff, psym=5, symsize=0.4, thick=thick*2, color=7

  atimes = format_intervals(minmax(visin.trange),/ut)
  aen = format_intervals(trim(minmax(visin.erange),'(f12.2)')) + ' keV'
  alg_label = alg + ' ' + vis_type + ' vis,   Normalize ' + (['off','on'])[vis_normalize] + more_label
  chia = 'Red chi2 all dets used in image = ' + trim(chisq_detused, '(f12.2)') + '  Dets = ' + arr2str(trim(udet+1),',')
  chib = 'Red chi2 all dets = ' + trim(chisq, '(f12.2)') + ',  Each det = ' + arr2str(trim(chisq_det,'(f12.2)'),', ')
  leg_text = [atimes, aen, alg_label, chia, chib, 'Observed', 'Error on Observed', 'From Image', 'Vector Difference']
  leg_color = [bw,bw,bw,bw,bw,bw, 10,2,7]
  leg_style = [-99, -99, -99, -99, -99, 0, 0, 0, 0]
  leg_sym = [0, 0, 0, 0, 0, 7, -3, 4, 5]
  ssw_legend, leg_text, psym=leg_sym, color=leg_color, linest=leg_style, box=0, charsize=leg_size, thick=thick

  timestamp, /bottom, charsize=charsize*.9

  case 1 of
    ps: if ~add_to_ps then psclose
    jpeg: begin
      thisimage = tvrd()
      tvlct, r, g, b, /get
      image24 = bytarr(3, xsize, ysize)
      image24(0,*,*) = r(thisimage)
      image24(1,*,*) = g(thisimage)
      image24(2,*,*) = b(thisimage)
      write_jpeg, full_file_plot, image24, true=1
      set_plot, thisdevice
    end
    else: wset,save_window
  endcase

  tvlct, rr,gg,bb

  getout:
  imobj -> set, use_single_return_mode=save_use_single

end
