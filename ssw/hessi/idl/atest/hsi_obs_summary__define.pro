;+
; PROJECT:
;       HESSI
;
; NAME: hsi_obs_summary__define
;
; PURPOSE: God object for obs summary objects.  This replace the old hsi_obs_summary object that used the
;	hsi_obs_summ_soc object.
;
; EXPLANATION:  The hsi_obs_summary object stores up to 20 separate obs summary object classes in parameter obj_arr[20].
;	By default the obs_summ_rate object is stored in obj_arr[0].  All of the hsi_obs_summary methods have an
;	optional class_name keyword.  If class_name is not set, the hsi_obs_summary methods use the obs_summ_rate object in
;	obj_arr[0].  If class_name is set, then hsi_obs_summary searches through obj_arr to see if that object class
;	has already been created.  If so, it reuses it.  If not, it creates a new object of that class and stores
;	it in the next available obj_arr element.
;	Each separate object has memory of whether a particular time interval has been accumulated yet, and will
;	only reaccumulate when necessary.
;	The previous incarnation of this routine was replaced because it was based on the
;	hsi_obs_summ_soc object which forced all obs summary data types to be read whenever data was requested for a
;	new time interval.  hsi_obs_summary only reads the data type requested.
;
;
; CATEGORY: HESSI
;
; CONSTRUCTION:  obs = hsi_obs_summary()
;
; INPUTS:
;
; OUTPUTS:
;
; GENERIC METHODS:
;
; OBJECT METHODS:
;	set
;	get
;	getdata
;	getaxis
; 	got_data
;	plot
; 	plotman
;	list
;	cleanup
;	help - just shows what objects are currently stored
;
; KEYWORDS:
;	class_name
;	obs_time_interval
;	filename
;	data_dir
;	plot and plotman commands can take additional keywords like plot options, or flags to show
;	list method can take additional keywords to pass to text_output routine (like show_text, print_text, file_text)
;	status - 0/1 means failure/success
;	err_msg - '' if successful, otherwise contains message
;
; EXAMPLES:
;	obs = hsi_obs_summary(obs_time_interval='20-feb-02 ' + ['10:40', '11:10'])
;	obs -> plot
;	obs -> plot, /saa, /flare
;	obs -> set, obs_time_interval='29-mar-02 21:' + ['25','29']
;	obs -> plotman, class_name='ephem', /night
;	list = obs -> list (class_name='mod_var', file_text='mod.txt')
;	ephem_obj = obs -> get(/object_ref, class='ephem')
;	mod_info = obs -> get(/info, class='mod_variance')
;
;	obs = hsi_obs_summary(filename='c:\hessidata_user\hsi_20020220_104040_006.fits')
;	obs -> plotman
;
; SUBCLASSES:
;
; IMPORTED OBJECTS:
;
; SEE ALSO:
;
; Written: 11-Aug-2002, Kim.  (Rewrite of hsi_obs_summary__define.pro)
;
; Modifications:
;	3-Sep-02, Kim.  Moved extracting the sub-object from hsi_obs_summary via
;	  /object_ref to the GET method from the GETDATA method to be consistent with
;	  other hessi objects.  Also added ut keyword to GETAXIS method, and check if
;	  data is already accumed (and if not accumulate it) in the get GETAXIS method.
;	24-Sep-02, Kim.  Was returning compressed values, not real values (Pascal noticed
;	  this).  Now in getdata, make sure to uncompress the data types that need
;	  need to be, but still return in a structure.
;-

;---------------------------------------------------------------------------

FUNCTION hsi_obs_summary::INIT, SOURCE = source, _EXTRA=_extra

ret=self->Framework::INIT( CONTROL = {hsi_obs_summary_control} )

;t='29-mar-02 21:' + ['25','29']

self->Framework::Set, OBS_TIME_INTERVAL = Anytim( ['2000/09/01 00:00:00', '2000/09/01 02:00:00'] )

self.obj_arr[0] = obj_new('HSI_OBS_SUMM_RATE' )

self.nobj_max = 20

IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

RETURN, ret

END

;---------------------------------------------------------------------------

pro hsi_obs_summary::cleanup
for i =0,self.nobj_max-1 do obj_destroy, self.obj_arr[i]
end

;---------------------------------------------------------------------------

pro hsi_obs_summary::help

print,'These are the objects currently stored in the hsi_obs_summary object: '
for i = 0,self.nobj_max-1 do if obj_valid(self.obj_arr[i]) then help,self.obj_arr[i]

end

;---------------------------------------------------------------------------

FUNCTION hsi_obs_summary::get, $
        control_only=control_only, $
        info_only=info_only, $
        object_reference=object_reference, $
        o_index=o_index, $
        class_name=class_name, $
        ut_ref=ut_ref, $
        time_range=time_range, $
        _extra=_extra

full_class_name = hsi_full_name(class_name)

if keyword_set(object_reference) then begin
	; find requested class in array of stored objects
	for i = 0,self.nobj_max-1 do begin
		if is_class(self.obj_arr[i], full_class_name, /quiet) then begin
			o_index = i
			return, self.obj_arr[i]
		endif
	endfor
	;if haven't returned already, then this class doesn't exist yet.  Create a new object
	; find first obj_arr element that doesn't have a valid object and put new object there.
	for i = 0,self.nobj_max-1 do begin
		if not obj_valid(self.obj_arr[i]) then begin
			ok = execute ('temp = obj_new(full_class_name)')
			if ok then begin
				self.obj_arr[i] = temp
				o_index = i
				return, self.obj_arr[i]
			endif
			return, -1
		endif
	endfor
	; shouldn't get here
	message,'Error. Already have max of 20 qlook objects stored.  Contact kim.tolbert@gsfc.nasa.gov'
endif

control = self->Framework::Get ( control_only=control_only, _EXTRA = _extra)

if keyword_set(ut_ref) then return, control.obs_time_interval[0]

if keyword_set(time_range) then return, control.obs_time_interval - control.obs_time_interval[0]

if keyword_set(class_name) and not keyword_set(control_only)then begin
	obj = self -> get(/object, class_name=class_name)
    if obj_valid(obj) then begin
        infostr = obj ->get(/info)
        if keyword_set(info) then return, infostr
        if keyword_set(_extra) then begin
        	tags_extra = tag_names(_extra)
        	tags_info = tag_names(infostr)
            for i = 0,n_elements(tags_extra)-1 do if tag_exist(infostr, tags_extra[i], index=index) then return, infostr.(index)
        endif else return, infostr
	endif else return, -1

endif

if keyword_set(info_only) then return, self.obj_arr[0] -> get(/info)

return, control

end

;---------------------------------------------------------------------------

PRO hsi_obs_summary::Set, $
       filename=filename, $
       OBS_TIME_INTERVAL=obs_time_interval, $
       data_dir=data_dir, $
       _EXTRA=_extra

IF Keyword_Set( OBS_TIME_INTERVAL ) THEN BEGIN
    self->Framework::Set, OBS_TIME_INTERVAL =  Anytim( obs_time_interval )
    self->Framework::Set, filename = ''
ENDIF

IF Keyword_Set( filename ) THEN BEGIN
    self->Framework::Set, filename =  filename
ENDIF

IF Keyword_Set( data_dir ) THEN BEGIN
    self->Framework::Set, data_dir =  data_dir
ENDIF

IF Keyword_Set( _EXTRA ) THEN BEGIN
    self->Framework::Set, _EXTRA = _extra
ENDIF

END

;---------------------------------------------------------------------------

FUNCTION hsi_obs_summary::GetData, $
        class_name=class_name, $
        xaxis=xaxis, $
        time_array=time_array, $
        _EXTRA=_extra

if keyword_set(_extra) then self -> set, _extra=_extra

full_class_name = hsi_full_name(class_name)

obj = self -> get(/object, class_name=full_class_name, o_index=o_index)

if not obj_valid(obj) then return, -1

control = self -> get(/control)
;time_range = self -> framework::get ( /obs_time_interval )
;filename = self -> framework::get (/filename)
;data_dir = self -> framework::get (/data_dir)

this_contr = self.contr_arr[o_index]

if control.filename eq '' then begin
	;told = obj -> get(/obs_time_interval)
	if not same_data(this_contr.obs_time_interval, control.obs_time_interval) then begin
    	data = obj -> getdata( obs_time_interval = control.obs_time_interval )
    	self.contr_arr[o_index] = control
    endif else data = obj -> getdata()
endif else begin
	;fold = obj -> get(/filename) & dold = obj->get(/data_dir)
	if not same_data(this_contr.filename, control.filename) or not same_data(this_contr.data_dir, control.data_dir) then begin
		filename = hsi_find_file(control.filename, path=control.data_dir)
		data = obj -> getdata(filename=filename)
		self.contr_arr[o_index] = control
	endif else data = obj -> getdata()
endelse

if size(data,/tname) ne 'STRUCT' then return, -1

if keyword_set(xaxis) or keyword_set(time_array) then return, obj -> get(/time_array)

; Take care of data types that are returned from their individual objects compressed.
; This routine should always returned uncompressed data in a structure.
; In order to not duplicate the calls to uncompress, we'll call the get(/xxx) for each
; object, which does the uncompression.  But that returns an array and we want to return
; a structure, so make a new structure.
case full_class_name of
	 'hsi_obs_summ_rate': begin
		d = obj -> get(/countrate)
		struct = replicate({countrate: lonarr(9)}, n_elements(d[0,*]))
		struct.countrate = d
		return, struct
		end
	'hsi_mod_variance': begin
		d = obj -> get(/mod_variance)
		struct = replicate({mod_variance: fltarr(2)}, n_elements(d[0,*]))
		struct.mod_variance = d
		return, struct
		end
	'hsi_qlook_roll_period': begin
		d = obj -> getdata(/de_bytescale)
		struct = replicate({roll_period: 0.}, n_elements(d))
		struct.roll_period=d
		return, struct
		end
	'hsi_qlook_roll_angle': begin
		d = obj -> getdata(/de_bytescale)
		struct = replicate({roll_angle: 0.}, n_elements(d))
		struct.roll_angle=d
		return, struct
		end
	'hsi_qlook_pointing': begin
		d = obj -> getdata(/de_bytescale)
		struct = replicate({pointing: fltarr(2), quality: 0B}, n_elements(d[0,*]))
		struct.pointing = d
		struct.quality = data.quality
		return, struct
		end
	'hsi_qlook_rate_per_det': begin
		d = obj -> get(/countrate)
		return, {countrate: d}
		end
	else: return, data
endcase

END
;---------------------------------------------------------------------------

function hsi_obs_summary::getaxis, class_name=class_name, ut=ut, xaxis=xaxis, yaxis=yaxis

full_class_name = hsi_full_name(class_name)

if self -> got_data(class_name=class_name, _extra=_extra) then begin
	obj = self -> get(/object, class_name=full_class_name)

	if keyword_set(xaxis) or keyword_set(ut) then return, obj -> get(/time_array)

	if keyword_set(yaxis) then return, -1

	;if no keywords set, return time axis
	return, obj -> get(/time_array)
endif else return, -1
end

;---------------------------------------------------------------------------
function hsi_obs_summary::got_data, class_name=class_name,_extra=_extra

full_class_name = hsi_full_name(class_name)

obj = self -> get(/object, class_name=full_class_name)
if size(obj, /tname) ne 'OBJREF' then return,0

data = self -> getdata(_extra = _extra, class_name=full_class_name )
if size(data,/tname) ne 'STRUCT' then return, 0

info = obj -> get(/info)
if size(info, /tname) eq 'STRUCT' then begin
	if tag_exist(info, 'n_time_intv') then begin
		if info.n_time_intv le 0 then return,0
	endif else return, 0
endif else return, 0

return, 1
end

;---------------------------------------------------------------------------

pro hsi_obs_summary::plot, status=status, err_msg=err_msg,  show_flags_obj=flags_obj, class_name=class_name, _extra=_extra

status=1
err_msg = ''

if keyword_set(_extra) then self -> set, _extra=_extra

full_class_name = hsi_full_name(class_name)

if self -> got_data(class_name=class_name, _extra=_extra) then begin
	obj = self -> get(/object, class_name=full_class_name)
	;if not keyword_set(flags_obj) then flags_obj = hsi_show_flags(_extra=_extra)
	obj -> plot, flags_obj=flags_obj, status=status, err_msg=err_msg, _extra=_extra
endif else begin
	status = 0
	err_msg = 'No data to plot'
	message, err_msg, /cont
endelse

end

;---------------------------------------------------------------------------

pro hsi_obs_summary::plotman, status=status, err_msg=err_msg, class_name=class_name, _extra=_extra

status = 1
err_msg = ''

if keyword_set(_extra) then self -> set, _extra=_extra

full_class_name = hsi_full_name(class_name)

if self -> got_data(class_name=class_name, _extra=_extra) then begin
	obj = self -> get(/object, class_name=full_class_name)
	plotman_obj = obj_new('plotman', $
		input=obj, $
		plot_type='utplot', $
		/single_panel, $
		error=error, $
		_extra=_extra)
		;class_name=class_name, $
		;/saa,/flare,/atte)
		;saa=saa, eclipse=eclipse, flare=flare)
endif else begin
	status = 0
	err_msg = 'No data to plot'
	message, err_msg, /cont
endelse

end
;---------------------------------------------------------------------------

function hsi_obs_summary::list, status=status, filename=filename, err_msg=err_msg, class_name=class_name, _extra=_extra

status = 1
err_msg = ''

if keyword_set(_extra) then self -> set, _extra=_extra

full_class_name = hsi_full_name(class_name)

if self -> got_data(class_name=class_name, _extra=_extra) then begin
	obj = self -> get(/object, class_name=full_class_name)
	list = obj -> list (filename=filename, err_msg=err_msg, _extra=_extra)
	return, list
endif else begin
	status = 0
	err_msg = 'No data to list'
	message, err_msg, /cont
endelse

end

;---------------------------------------------------------------------------

function hsi_obs_summary::changes, _extra=_extra

if keyword_set(_extra) then self -> set, _extra=_extra

if self -> got_data(class_name='hsi_obs_summ_flag', _extra=_extra) then begin
	obj = self -> get(/object, class_name='hsi_obs_summ_flag')
	return, obj -> changes()
endif else begin
	status = 0
	err_msg = 'No flag data retrieved'
	message, err_msg, /cont
endelse

end

;---------------------------------------------------------------------------

PRO hsi_obs_summary__Define


self = {hsi_obs_summary, $
        obj_arr: Objarr(20), $
        contr_arr: replicate({hsi_obs_summary_control}, 20), $
        nobj_max: 20, $
        INHERITS Framework }

END


;---------------------------------------------------------------------------
; End of 'hsi_profile__define.pro'.
;---------------------------------------------------------------------------

