;+
; Name:  hsi_aspect_solution::plot
;
; Purpose:  Plot method for hsi_aspect_solution object.
;
; Calling sequence:  o -> plot  where o is an hsi_aspect_solution object.
;    If o is an image object, call o -> plot_aspect, which will call this.
;	   _extra keywords are set in object and/or used in plot command.
; 
; 
; Keyword Inputs:
;	xrange, yrange - x,y range for aspect plot
;	xyoffset - if set, and drawing the basix image and spin axis plot on solar disk, show location of xyoffset on plot
;	plot_diam - if set, plot diameter of image axis circle vs time
;	plot_dist - if set, plot distance from Sun center to imaging axis (if user requested
;	  sas_only solution, then this is the only plot option
;	plot_triangle - if set, plot size of triangles (for measure of
;	  quality of aspect (< .5 is OK)
;	plot_p_error - if set, plot SAS error
;	triangle_yrange - yrange to use for triangle plot
;	aspect_p_error_threshold - cutoff for error, bins whose error is greater than this are rejected
;
; Outputs:  Creates new window for plot. Default is to plot the Sun disk and show image and 
; spin axis on it.  plot_dist, plot_triangle, and plot_p_error keywords select different plots.
;	
;	To make a PostScript plot, first set the device to PS, make this plot, and then close PS,e.g.
;	ps,'aspect.ps', /portr, /color
;	device,xsize=17.78,ysize=17.78  ; this makes it square
;	o -> plot
;	psclose
;	
;	To send all four plots to a PS file:
;	ps,'as4.ps',/portr,/color & o->plot & o->plot,/plot_dist & o->plot,/plot_trian & o->plot,/plot_p_err & psclose
;
; History:  Written Kim, 25-Apr-2007
; Modifications:
;  8-May-2007, Kim.  Some options are just keywords to getdata, and don't get set in
;    object, so don't use set for _extra, pass in getdata call, and also pass to get_spin_axis
;  21-Oct-2010, Kim. If PS device is set, don't call windows, and select different colors
;  17-Dec-2010, Kim. Added plot_dist keyword.  Check for whether sas_only was set (won't have RAS_SOL
;    tag in structure) - if so, or if plot_dist set, plot distance from center of Sun instead of x vs y.
;    Also cleaned up calculation of time axis
;  23-Sep-2011, Kim.  Added plot_p_error keyword.  And some other minor changes.
;  26-Nov-2011, Kim. Call al_legend instead of legend (IDL V8 conflict)
;  28-Apr-2015, Kim. Added aspect_p_error_threshold input keyword. Previously this value was hard-coded to .4. Now
;    it is a control param in calib_eventlist, which is not accessible from here. So pass it in if can, otherwise uses
;    the new default value of .8 for aspect_p_error_threshold. (just used to plot limits on error plot)
;  27-Jul-2015, Kim. Added xyoffset and plot_diam keywords.  Changed so that plots can be shown in plotman (called through 
;    hsi_aspect_solution::plotman.  Use ssw_legend instead of al_legend.
;  
;
;-
;============================================================================


pro hsi_aspect_solution::plot, xrange=xrange, yrange=yrange, $
  xyoffset=xyoffset, plot_diam=plot_diam, $
	plot_triangle=plot_triangle, triangle_yrange=triangle_yrange, $
	plot_dist=plot_dist, plot_p_error=plot_p_error, $
	aspect_p_error_threshold=aspect_p_error_threshold, $
	psym=pysm, no_timestamp=no_timestamp, $
	_extra=_extra

ps = !d.name eq 'PS'
checkvar, psym, 0
checkvar, no_timestamp, 0
pman = get_caller() eq 'PLOTMAN::PLOT'
wind = ~pman and ~ps

pl_diam = keyword_set(plot_diam)
pl_perror = keyword_set(plot_p_error)
pl_triangle = keyword_set(plot_triangle)
pl_dist = keyword_set(plot_dist)
;get aspect solution
as = self->getdata(_extra=_extra)

if size(as, /tname) eq 'STRUCT' then begin
  time = hsi_sctime2any(as.t0) + as.time*2.^(-7)
	time_range = minmax(time)
	atime_range = anytim(time_range, /vms)

	;RHESSI pointing
	x=as.pointing[*,0]
	y=as.pointing[*,1]

    ; if RAS_SOL tag isn't in structure, then sas_only was set.  Can only plot distance, not x vs y on Sun.
    no_ras = ~tag_exist(as, 'RAS_SOL')
    if no_ras and ~keyword_set(plot_dist) then message,/cont,'SAS_ONLY solution. Can only plot distance from Sun.'
    pl_dist = pl_dist or no_ras
  
	spin_axis = self->get_spin_axis(spin_period=spin_period, diameter=diameter, _extra=_extra)


  tvlct,rr,gg,bb,/get
	linecolors
  wb = ([255,0])[ps] ; select white/black for not PS / PS
  
  case 1 of
  
    pl_diam: begin
      if diameter[0] ne -1 then begin
        if wind then window,/free, xsize=800,ysize=600
        checkvar, yrange, minmax(diameter)
        utplot, anytim(time,/ext), diameter, $        
          ytitle='arcsec', $
          title='Diameter of Image Axis circle', _extra=_extra
      endif
      end
      
    pl_triangle: begin
      if wind then window,/free, xsize=800,ysize=600
      quality = self->get(/as_quality)
      ; SAS reduced triangle          
      if ~pman then yr = keyword_set(triangle_yrange) ? triangle_yrange : [-2.,2.]
      utplot, anytim(time,/ext), *quality.triangle, $
         xstyle=1, ystyle=1, yrange=yr,  $
         ytitle='arcsec', $
         title='SAS:  Size of Reduced Triangle from Mid-perpendiculars to Limb Crossings', _extra=_extra
       mom = moment(*quality.triangle)
       text = ['Mean: ' + trim(mom[0],'(f6.2)') + ' (<0.5 arcsec is reasonable)', $
               'Variance: ' + trim(mom[1], '(f6.3)') + '( ~0.5 arcsec^2 is reasonable)']
       ssw_legend, text, box=0
       if ~no_timestamp then timestamp, /bottom
      end 

    pl_dist: begin
      if wind then window,/free, xsize=800,ysize=600
      dist = sqrt(x^2 + y^2)    
      checkvar, yrange, [0,1200]
      utplot, anytim(time,/ext), dist, title='Distance from Sun Center to Imaging Axis', ytitle='arcsec', $
        yrange=yrange, /xstyle, _extra=_extra
      if ~no_timestamp then timestamp, /bottom
      end
      
    pl_perror: begin
      if wind then window,/free, xsize=800,ysize=600
      dur = time_range[1] - time_range[0]
      t = (findgen(128)+.5)* dur/128      
      z = self -> getdata(this_time=t, this_unit_time=2.^20, this_ut_ref=time_range[0])
      checkvar, yrange, [0., max(z.p_error)+.1]
      utplot, anytim(t+time_range[0], /ext), z.p_error, title='Aspect Solution P_ERROR', $
        /xstyle, ytitle='arcsec', yrange=yrange, _extra=_extra
      g=hsi_grid_parameters()
      if ~exist(aspect_p_error_threshold) then message,/info,'aspect_p_error_threshold not defined, using default value of .8'
      checkvar, aspect_p_error_threshold, .8
      thresh = aspect_p_error_threshold * g.pitch / g[0].pitch
      text = 'Detector Thresholds:'
      col = [2,7,9,12,13,8,10,4,1]
      for id = 0,8 do begin
        oplot, !x.crange, thresh[id]+[0.,0.], col=col[id], linest=2
        text=append_arr(text,['Det ' + trim(id+1) + ' ' +trim(thresh[id],'(f5.1)')])
      endfor
      ssw_legend, text, lines=[-99,intarr(9)+2], col=[wb,col], box=0
      if ~no_timestamp then timestamp, /bottom
      end
      
    else: begin
      if wind then window,/free, xsize=600,ysize=600
      limb = (get_rb0p(atime_range[0], /quiet))(0)
      ang = findgen(3601)/3600.*2.*!pi
      if ~exist(xrange) || total(xrange) eq 0. then xrange = [-1200,1200]
      if ~exist(yrange) || total(yrange) eq 0. then yrange = [-1200,1200]
      limb_col = ([5,10])[ps] ; select yellow/blue for not PS/PS      
      plot, x, y, title='Aspect Solution', $
          xtitle='Heliocentric X (arcsec)',ytitle='Heliocentric Y (arcsec)',$
          xrange=xrange, yrange=yrange, psym=0, _extra=_extra
      oplot, spin_axis[*,0], spin_axis[*,1], psym=-1, color=2
      oplot, limb*cos(ang), limb*sin(ang), psym=0, color=limb_col

      avsp = trim(average(spin_axis,1), '(2f8.2)')
      text = [atime_range[0]+' - '+atime_range[1], $
        'Average Spin Axis: ' + arr2str(avsp,', ') + ' arcsec', $
        'Spin Period: ' + trim(spin_period, '(f8.2)') + ' sec', $
        'Image Axis', 'Spin Axis', 'Solar Limb']
      linest = [-99,-99,-99,0,0,0]
      color=[wb,wb,wb,wb,2,limb_col]
      psym = [1,1,1, -3, -1, -3]
      
      if n_elements(xyoffset) eq 2 then begin
        oplot,xyoffset[0]+[0.,0.],xyoffset[1]+[0.,0.],psym=7,col=7,symsize=3.,thick=3
        text = [text, 'Source Position']
        linest = [linest, 0]
        color = [color, 7]
        psym = [psym, 7]
      endif
      
      ssw_legend, text, linestyle=linest, color=color, psym=psym, box=0
      if ~no_timestamp then timestamp, /bottom
      end
      
  endcase
   
  tvlct,rr,gg,bb

endif else begin

	message,'No aspect solution available.', /info

endelse

end