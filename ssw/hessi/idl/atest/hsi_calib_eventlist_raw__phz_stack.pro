FUNCTION hsi_vis_rollbin_nominal_sc, offset, resolution, NROLLBIN_LIMITS=nrollbin_limits
  ;;+
  ; Returns a nominal rollbin count for a given source-rotation axis offset and grid pitch.
  ; Calculation is based on maximizing the number of equal roll bins, each of which
  ;   would contain at least one modulation cycle, for the case of stable pointing.
  ;
  ; nrollbin_limits is a two-element vector with min and max allowable values of nrollbin.
  ;       defaults to [4,128]
  ;
  ; 9-May-2007    Initial version (ghurford@ssl.berkeley.edu)
  ; 7-jul-2009, richard.schwartz@nasa.gov, changed defaults on nrollbin_limits
  ;-
  DEFAULT, nrollbin_limits, [6,64]
  ;
  projmin  = 2 * resolution / offset                 ; change in projected offset corresponding to one modulation cycle (in roll angle)
  nmax    = FIX((offset/resolution) < (nrollbin_limits[1]/2))             ; upper limit to number of bins/half rotation
  nmin    = CEIL(nrollbin_limits[0] / 2.)
  nrollbin = nmin*2                                   ; fallback option
  IF nmax EQ 0    THEN RETURN, nrollbin
  IF nmin GT nmax THEN RETURN, nrollbin
  nbest   = 0
  FOR ntrial = nmin, nmax DO BEGIN
    phi     = FINDGEN(ntrial+1) /ntrial * !PI ; roll bin edges
    proj    = COS(phi)
    dproj   = SHIFT(proj,1) -  proj
    dummy   = WHERE(dproj GE projmin, nok)

    IF nok GE nbest THEN BEGIN
      nbest       = nok
      nrollbin    = ntrial*2
    ENDIF
  ENDFOR
  RETURN, nrollbin
END

function hsi_vis_rollbin_nominal, offset, res, roll_lims
  out =fltarr(9)

  for i=0,8 do begin
    ;if i eq 1 then stop
    out[i]= hsi_vis_rollbin_nominal_sc(offset, res[i], NROLLBIN_LIMITS= roll_lims[*,i])
  endfor
  return, out
end

function hsi_rollbins_calc, Radius, offaxis_disp, nroll_min, nroll_max
  fwhm = (hsi_grid_parameters()).pitch/2

  N_roll = ceil( 2.*!pi * Radius /FWHM) * 2

  n_roll = (n_roll > nroll_min) < nroll_max
  lims =transpose( reform([n_roll, n_roll*0+nroll_max], 9,2))

  rollbin_max = hsi_vis_rollbin_nominal( offaxis_disp, fwhm, $
    lims)

  return, n_roll>rollbin_max
end

;---------------------------------------------------------------------------
;ras, 21-dec-2001, put in this method to help Gordon test the phase
;combined calibrated eventlist
;
;7-jul-2009, modified to use hsi_rollbins_calc to optimize
; the number of rollbins for the source size (Radius) and the offset of the
;telescope axis
;RAS, 2-aug-2016 - fixed setting roll bins through phz_n_roll_bins_control
; 3-jul-2017, ras, added stagger fraction for first angle offset

PRO HSI_CALIB_EVENTLIST_raw::PHZ_STACK, cbe_unstacked, cbe_stacked

  caller = self.caller
  ;find max based on off axis displacement
  offaxis_disp= caller->get(/offaxis_disp)
  Radius = Caller->Get(/phz_radius)
  cbe_stacked = ptrarr(size(cbe_unstacked,/dim))
  valid = where( ptr_valid( cbe_unstacked ), nvalid)


  Nphase = Caller->Get(/phz_n_phase_bins)
  Nroll_min = Caller->Get(/phz_n_roll_bins_min)
  Nroll_max = Caller->Get(/phz_n_roll_bins_max) ;29-mar-2006, ras, added
  Nroll_control = Caller->Get(/phz_n_roll_bins_control)
  Nroll_computed = hsi_rollbins_calc( Radius, offaxis_disp, nroll_min, nroll_max)
  ;RAS, 2-aug-2016, next line is a horrific error, changed with the following 2 lines
  ;nroll_computed = total(abs(nroll_control)) gt 0 ? nroll_control : hsi_rollbins_calc( Radius, offaxis_disp, nroll_min, nroll_max)
  q  = where( nroll_control ge nroll_min and nroll_control le nroll_max, nq) ;have any nroll_control been set and are valid?
  ;if so then override the Nroll_computed
  if nq gt 0 then Nroll_computed[q] = nroll_control[q]
  ;;;;;;;;;;;;;
  caller->Set, PHZ_N_ROLL_BINS_INFO= Nroll_computed
  use_phz_conjugation = Caller->Get(/use_phz_conjugation) ;not active 6-mar-2012
  ;USE_REFERENCE_POSITION_ANGLE activated 6-mar-2012, ras, see hsi_calib_eventlist_raw__reorder_cbe
  use_reference_position_angle=caller->Get(/use_reference_position_angle)
  ;ras, 3-jul-2017
  use_stagger = caller->Get( /use_stagger_angle ) ;stagger the roll angles for roll bin clusters
  if use_stagger && ~use_reference_position_angle then begin ;comput scatter fractions
    stagger_frac = fltarr(9)
    hstg = histogram( min=0, nroll_computed, rev = revstg )
    zstg = where( hstg gt 1, nzstg)
    if nzstg ge 1 then begin
      for izs = 0, nzstg -1 do begin
        jj = reverseindices( revstg, zstg[ izs ] )
        stagger_frac[ jj ] = findgen( hstg[ zstg[izs] ]) / hstg [zstg[izs]]
      endfor
    endif
    
  endif

  if Caller->Get(/phz_report_roll_bins) then print,'Phz_N_Roll_Bins_info:',n_roll
  message,/info,'In Phz_Stack '

  for i=0,nvalid-1 do begin
    j = valid[i] ;det_index (coll_index)
    coll_index = j
    cbe_stacked[j] = ptr_new(hsi_phz_stacker(*cbe_unstacked[j], $
      coll_index, $
      stagger = stagger_frac[j], $ ;ras, 3-jul-2017
      n_roll=nroll_computed[j],  n_phase=nphase, $
      obj=caller))
    if  use_phz_conjugation eq 1 then $
      message,/info, 'phz_conjugation under development, not implemented'
    ;    	*cbe_stacked[j] = hsi_phz_conjugation( $
    ;    	*cbe_stacked[j], $
    ;        n_roll=nroll_computed[j],  n_phase=nphase, empty_flag=empty_flag,$
    ;        use_reference_position_angle=use_reference_position_angle)
    ;help, use_phz_conjugation, *cbe_stacked[j], j

  endfor

END
