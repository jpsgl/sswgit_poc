FUNCTION Hsi_full_rate::init, _extra = _extra
  
  ret = self -> hsi_qlook::init(_extra = _extra, $
                                class_name = 'HSI_FULL_RATE')
  RETURN, 1
END

PRO Hsi_full_rate::Fill, $
                 filename = filename, $
                 id_string = id_string, $
                 Time_Intv = time_Intv, $
                 energy_edges = energy_edges, $
                 Quiet = quiet, $
                 obs_time_interval = obs_time_interval, $
                 loop_time_intv = loop_time_intv, $ ;jmm, 2012-06-08
                 _extra = _extra
   
  If(keyword_set(quiet)) Then notquiet = 0b Else notquiet = 1b
;Create the structures
  cn = 'HSI_FULL_RATE'
  control = hsi_qlook_version_control('HSI_QLOOK', $
                                      out_version = version, /control)
  control.class_name = cn
  control.version = version
  info = hsi_qlook_version_control(cn, out_version = vers_info, /info)
  control.vers_info = vers_info
  
;time interval
  IF(KEYWORD_SET(time_Intv)) THEN dt0 = time_Intv(0) $
  ELSE dt0 = 4.0
;energy bins
  IF(KEYWORD_SET(energy_edges)) THEN enbins = energy_edges $
  ELSE enbins = [3.0, 6.0, 12.0, 25.0, 50.0, 100.0, 300.0, 800.0, $
                 7000.0, 20000.0]
  nfb = N_ELEMENTS(enbins)-1
  info.n_energy_bands = nfb
  info.energy_edges = enbins
  info.time_intv = dt0
  info.dim1_unit = 'Energy bands (keV)'
  enbins_str = strcompress(string(fix(enbins)), /remove_all)
  info.dim1_ids = enbins_str[0:nfb-1]+' - '+enbins_str[1:*]
  info.dim2_unit = 'Segment'
  info.dim2_ids = ['1f', '2f', '3f', '4f', '5f', '6f', '7f', '8f', '9f', $
                   '1r', '2r', '3r', '4r', '5r', '6r', '7r', '8r', '9r']

  data = hsi_qlook_version_control(cn, out_version = vers_data, /data)
  control.vers_data = vers_data
;Create the Summary
  IF(NOT KEYWORD_SET(quiet)) THEN message, 'Creating Full_Rate', /info
;Get the count rates, hacked from hsi_obs_summ_allrates, note that
;since I want decimation corrections this cannot be done at the same
;time as the obs_summ.
  nenbins = n_elements(enbins)-1
  rates_ebands = enbins
  dt_rates = dt0
  dt_files = 5.0d0*dt_rates
  infile = filename
;Can you find the file?
  aa = loc_file(infile[0], count = bb)
  break_file, infile[0], a, b, c, d
  If(bb Eq 0) Then Begin        ;No file
    If(notquiet) Then Begin
      message, /info, 'File, '+infile[0]+' not found'
      message, /info, 'looking in Data Archive'
    Endif
    aa = hsi_find_in_archive(c+d, count = bb, /no_dialog)
    If(bb Eq 0) Then Begin
      message, /info, 'Telemetry File not found:'
      print, infile[0]
      Return
    Endif
  Endif
  infile = aa[0]
;Get the packet array, for time interval selection
  print, 'FILE: ', infile
  If(notquiet) Then message, /info, 'packet array'
  pkt_obj = obj_new('hsi_packet')
  pkt = pkt_obj -> getdata(/all, filename = infile[0])
  If(datatype(pkt) Ne 'STC') Then Begin
    If(notquiet) Then message, /info, 'No Packets'
    Return
  Endif
  pkttbl = pkt_obj -> getdata(/lookup_table)
  pktt = pkttbl.collect_time
  okpak = where(pkttbl.bad_pak Eq 0 And pktt Gt 0.0)
  If(okpak[0] Ne -1) Then Begin
    pktt[okpak] = hsi_sctime2any(pkt[okpak].collect_time)
  Endif Else Begin
    If(notquiet) Then message, /info, 'No Good Packets'
    Return
  Endelse
  If(obj_valid(pkt_obj)) Then obj_destroy, pkt_obj
  npkt = n_elements(pkt)
  ftr = [min(pktt[okpak]), max(pktt[okpak])]
;Ok, now we have packets, Set up the time range,
  one_day = 24.0*3600.0
  t00x = anytim('4-jul-2000 0:00')
  If(keyword_set(obs_time_interval)) Then Begin
    print, 'OBS TIME INTERVAL IS SET'
    oti = anytim(obs_time_interval)
;Here, if there is something freaky about the ftr, reset it to oti
    ftr[0] = ftr[0] > oti[0]
    ftr[1] = ftr[1] < oti[1]
  Endif Else Begin
;All files start an integral number of dt from 4-jul-2000
    oti = ftr
    oti[0] =  hsi_set_file_time(ftr[0], time_intv = dt_files, $
                                ref_time = t00x)
  Endelse
;a reference time for rounding
  ref_time = t00x+one_day*long((oti[0]-t00x)/one_day)
  dt_total = oti[1]-oti[0]
  ntimes_rates = long(dt_total/dt_rates)
  If((dt_total Mod dt_rates) Ne 0.0) Then ntimes_rates = ntimes_rates+1
  oti[1] = oti[0]+ntimes_rates*dt_rates
  ut_ref_rates = oti[0]
;Here define the output arrays
  If(ntimes_rates Gt 0) Then Begin
    rates = dblarr(ntimes_rates, nenbins, 18)
  Endif
;Ok, now you may get the eventlist
  ok100 = where(pkttbl.app_id Eq 100 And pkttbl.bad_pak Eq 0)
  If(ok100[0] Ne -1) Then Begin
;Get the light curve, use all segments
;Split into lt 2048 second groups to avoid time_unit problems, jmm, 10-jan-2003
;2048 is now the default for an option, the idea here is to allow shorter 
;intervals to avoid issues with missing bad packets, jmm, 8-nov-2011
;Changed to 128 seconds, 21-jun-2012, jmm
    dt1 = dt_rates
    If(keyword_set(loop_time_intv)) Then t2048 = loop_time_intv $
    Else t2048 = 128.0
    n2048 = ceil((ftr[1]-ftr[0])/t2048)
    jcount = 0
    ltc1 = -1
    For j = 0, n2048-1 Do Begin
;fixed infinite loop, 20-apr-2012, jmm
      ltctr = t2048*j+[0.0, t2048]
      ltctr[1] = ltctr[1] < (ftr[1]-ftr[0])
      ltctr[1] = double(long(ltctr[1]+0.5))
      ntx = (ltctr[1]-ltctr[0])/dt1
      If(ntx Lt 1) Then Continue ;don't do this for partial time intervals
;drop if there are no packets      
      otij = oti[0]+ltctr
      okj = where(pktt[ok100] Ge otij[0] And pktt[ok100] Lt otij[1], nokj)
      If(nokj Eq 0) Then Begin
         message, /info, 'No app_id 100 packets in range:'
         goto, no_data
      Endif
;put a catch statement here
      errj = 0
      catch, errj
      If(errj Ne 0) Then Begin
        errj = 0 ;turn off the catch here to avoid infinite looping
        Print, 'Bad Light curve'
        help, /last_message, output = err_msg
        For kk = 0, n_elements(err_msg)-1 Do print, err_msg[kk]
        goto, no_data
      Endif
      message, /info, 'PROCESSING: '
      ptim, otij
      oltc1 = obj_new('hsi_spectrum')
      ltc1j = oltc1 -> getdata(obs_time_interval = otij, $
                               sp_energy_binning = enbins, $
                               seg_index_mask = bytarr(18)+1, $
                               sp_time_interval = dt1, sum_flag = 0, $
                               decimation_correct = 1, $
                               use_flare_xyoffset = 0)
      tr1j = oltc1 -> get(/absolute_time_range)
;Spectrum object returns nenergies, ntimes
      ltc1j = transpose(ltc1j, [1, 0, 2])
;in the ltc1j=-1 case, create an array of 0's, also create the array
;if total(ltc1j)=0, to account for bizzare bug for
;file20050212_02340_002, which may be failing on a bad packet...
      If((ltc1j[0] Eq -1) Or (total(ltc1j) Le 0)) Then Begin
no_data:
        ltc1j = fltarr(ntx, nenbins, 18)
        tr1j = ftr[0]+ltctr
      Endif
      If(jcount Eq 0) Then Begin 
        tr1 = tr1j
        ltc1 = ltc1j
      Endif Else Begin
        ltc1 = [temporary(ltc1), temporary(ltc1j)]
        tr1 = [tr1[0], tr1j[1]]
      Endelse
      jcount = jcount+1
      If(obj_valid(oltc1)) Then obj_destroy, oltc1
      errj = 0
    Endfor
;synch the absolute time range up with the oti time range here
    If(ltc1[0] Ne -1) Then Begin
      nltc1 = n_elements(ltc1[*, 0, 0])
      If(notquiet) Then Begin
        print, 'LTC Absolute_time_range: ', $
          anytim(/yoh, tr1[0]), ' -- ', $
          anytim(/yoh, tr1[1])
        print, 'OTI Absolute_time_range: ', $
          anytim(/yoh, oti[0]), ' -- ', $
          anytim(/yoh, oti[1])
        print, 'REF_TIME: ', anytim(/yoh, ref_time)
      Endif
;round returns a long integer
      tr1[0] = ref_time+dt1*round((tr1[0]-ref_time)/dt1)
      tr1[1] = tr1[0]+dt1*nltc1
      nltc0 = ntimes_rates
      If(total(abs(oti-tr1)) Eq 0.0 And nltc1 Eq nltc0) Then Begin 
        ltc0 = ltc1
      Endif Else Begin      
;to do the synching correctly, embed ltc1, in a larger array, ltcx
        txmin = min([oti, tr1], max = txmax)
        nx = long((txmax-txmin)/dt1)
        ltcx = fltarr(nx, nenbins, 18)
;x1=1st ss of ltc0, in ltcx
;y1=1st ss of ltc1, in ltcx
        y1 = long((tr1[0]-txmin)/dt1)
        ltcx[y1:y1+nltc1-1, *, *] = ltc1
        x1 = long((oti[0]-txmin)/dt1)
        ltc0 = ltcx[x1:x1+nltc0-1, *, *]
        delvarx, ltcx
      Endelse
;Normalize by time
      rates = ltc0/dt_rates
    Endif  
  Endif

;fill info and data structures
  info.ut_ref = ut_ref_rates
  info.time_intv = dt_rates
  Info.n_time_intv = ntimes_rates
  data = replicate(data, ntimes_rates)
  data.countrate = transpose(rates, [1, 2, 0])

  IF(datatype(data) EQ 'STC') THEN BEGIN
    oti = [info.ut_ref, $
           info.ut_ref+info.time_Intv*info.n_time_intv]
    IF(KEYWORD_SET(id_string)) THEN id_string = id_string $
    ELSE BEGIN
      id_string = cn+': '+anytim(oti[0], /ccsds)+ $
        ' TO '+anytim(oti[1], /ccsds)
    ENDELSE
    control.nodata = 0b
  ENDIF ELSE BEGIN
    id_string = 'No Data Found'
    IF(KEYWORD_SET(obs_time_interval)) THEN BEGIN
      oti = anytim(obs_time_interval)
    ENDIF ELSE oti = [0.0, 0.0]
    control.nodata = 1b
  ENDELSE

  control.id_string = id_string
  control.class_name = cn
  control.obs_time_interval = oti
  control.time_range = oti
  control.read_from_archive = 0b
  control.filename = ptr_new(filename)
  self -> set, info = info, data = data, control = control

END

FUNCTION Hsi_full_rate::get, Countrate = countrate, $
                      corrected_countrate = corrected_countrate, $
                      _extra = _extra
  otp = -1
  IF(KEYWORD_SET(countrate)) THEN BEGIN
    data = self -> hsi_qlook::get(/data)
    IF(datatype(data) EQ 'STC') THEN $
      otp = data.countrate
   ENDIF ELSE IF(KEYWORD_SET(corrected_countrate)) THEN BEGIN
      otp = self -> corrected_countrate(_extra = _extra)
  ENDIF ELSE IF(KEYWORD_SET(_extra)) THEN BEGIN
;Anything Else
    otp = self -> hsi_qlook::get(_extra = _extra)
  ENDIF ELSE BEGIN
    otp = self -> hsi_qlook::get()
  ENDELSE
  RETURN, otp
END

;Corrected_countrate correct for attenuation
FUNCTION hsi_full_rate::corrected_countrate, $
                      obs_summ_flag_obj = obs_summ_flag_obj, $
                      att0_not_1 = att0_not_1,  $
                      no_rear_dec = no_rear_dec, $
                      interp_gap = interp_gap, $
                      _extra = _extra
  rates = -1
  data = self -> get(/data)
  If(is_struct(data) Eq 0) Then Begin
    message, /info, 'No Data'
    return, rates
  Endif
  rates = data.countrate
  good0 = where(total(total(rates, 1), 1) Gt 0)
  If(good0[0] Eq -1) Then Begin
    message, /info, 'No Non-zero count rates'
    return, rates
  Endif
;Need obs_summ_flag_obj, if it's passed in use it, if not get it
  If(obj_valid(obs_summ_flag_obj)) Then fobj = obs_summ_flag_obj $
  Else Begin
    fobj = hsi_obs_summ_flag()
    fobj -> set, obs_time_interval = (self -> data_time_range())
    ddf = fobj -> getdata()
  Endelse
;      rates0 = rates
  ntimes = self -> get(/n_time_intv)
  tim_arr = self -> get(/time_array)
  atten_state = fobj -> get(/attenuator_state)

;Correct for attenuation here, only bother for the good data
  If(max(atten_state[good0]) Ne min(atten_state[good0])) Then Begin
;first correct for att_state = 4, to do this you locate the
;att_state=1 nearest to the offending 4 state, and reset the count
;rate and the att_state, jmm, 19-feb-2003
    att4 = where(atten_state Eq 4, natt4)
    If(natt4 Gt 0) Then temp_atten421, rates, atten_state
    att3 = where(atten_state Eq 3, natt3)
    att1 = where(atten_state Gt 0, natt1)
    att0 = where(atten_state Eq 0, natt0)
;three possibilities, 01, 13, 013, correct 3 then 1 if necessary
    If(natt3 Gt 0) Then Begin
      aflag = bytarr(ntimes)
      aflag[att3] = 1
      For k = 0, 17 Do For j = 0, 8 Do Begin ;correction here should be gt 1
        rates[j, k, *] = temp_atten_correct(reform(rates[j, k, *]), $
                                         aflag, tim_arr, _extra = _extra)
      Endfor
    Endif
    If(natt1 Gt 0 And natt0 Gt 0) Then Begin
      aflag = bytarr(ntimes)
      If(keyword_set(att0_not_1)) Then Begin
        aflag[att1] = 1 
        lt1 = 0                 ;Correction > 1
      Endif Else Begin
        aflag[att0] = 1
        lt1 = 1                 ;Correction < 1
      Endelse
      For k = 0, 17 Do For j = 0, 8 Do Begin
        rates[j, k, *] = temp_atten_correct(reform(rates[j, k, *]), $
                                            aflag, tim_arr, lt1 = lt1, $
                                            _extra = _extra)
      Endfor
    Endif
  Endif
  If(Not keyword_set(obs_summ_flag_obj)) And obj_valid(fobj) $
    Then obj_destroy, fobj
  Return, rates
End

;This will create a 3x3 plot of 9 detectors, using the plot method
Pro hsi_full_rate::plot9, front = front, rear = rear, _extra = _extra

  If(keyword_set(front)) Then Begin
    i0 = 0 & i1 = 8
  Endif Else If(keyword_set(rear)) Then Begin
    i0 = 9 & i1 = 17
  Endif Else Begin
    i0 = 0 & i1 = 8
  Endelse

  !p.multi = [0, 3, 3]
  For j = i0, i1 Do self -> plot, det_index_in = j, _extra = _extra

End




;+
;NAME:
; hsi_full_rate__define
;PROJECT:
; HESSI
;CATEGORY:
; Obeserving Summary
;PURPOSE:
; Defines the structure for the Hessi Observing Summary Countrate,
;CALLING SEQUENCE:
; hsi_full_rate__define
;INPUT:
; None
;OUTPUT:
; None
;HISTORY:
; 30-Jun-2009, jmm, jimm@ssl.berkeley.edu
;-
PRO Hsi_full_rate__define

  self = {hsi_full_rate, inherits hsi_qlook}

END
