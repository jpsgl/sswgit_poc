;+
; Name:  hsi_show_flags
;
; Purpose:  Object for plotting hessi flags on time plots.  Note:  the level-0 FITS files for
;   the time range requested must contain observing summary information for this to work.
;
; Calling sequence:
;	flags_obj = hsi_show_flags()
;	flags_obj -> select_widget
;	flags_obj -> display, timerange
;
;	To get the flag changes, and label and number of bars that will be written on plot
;	flags = flags_obj -> getdata(timerange)
;	label = flags_obj -> getlabel(timerange, n_bar=n_bar)
;
;   Examples when running standalone:
;      draw your utplot plot...
;      flags = hsi_show_flags(/night, /saa)
;      flags -> display,['20-feb-2002 00:00', '21-feb-2002 00:00'], /standalone
;
;      hsi_linecolors
;      flags = hsi_show_flags(/night, /saa, /atten, /bottom)
;      flags -> set, flag_colors=[8,7,255,255,5,255]
;      flags -> display,['20-feb-2002 00:00', '21-feb-2002 00:00'], /standalone
;
;   Parameters to set:
;      saa - 0/1 means don't / do show saa indicators (def=0)
;      night - 0/1 means don't / do show night indicators (def=0)
;      flare - 0/1 means don't / do show flare indicators (def=0)
;      front_decimation (or decimation) - 0/1 means don't / do show front decimation indicators (def=0)
;      front_energy - 0/1 means don't / do show front decimation energy indicators (def=0)
;      rear_decimation - 0/1 means don't / do show rear decimation indicators (def=0)
;      rear_energy - 0/1 means don't / do show rear decimation energy indicators (def=0)
;      attenuator - 0/1 means don't / do show attenuator indicators (def=0)
;      fronts_off - 0/1 means don't / do show fronts off indicators (def=0)
;      bottom - 0/1 means show indicators at top / bottom of plot (def=0)
;      flag_colors - intarr(9) giving color indices to use for the 9 flags in this order (same as listed above)
;         ['SAA', 'Night', 'Flare', 'FDecim', 'FDkeV', 'RDecim', 'RDkeV', 'Att', 'ROnly']
;         (def = [245,250,243,247,251,249,239,253,248] which are appropriate for plotman)
;         (if using hsi_linecolors, use [8,4,6,9,7,12,11,2,3] to match colors plotman uses)
;      flag_changes - structure returned by hsi_obs_summ_flags->changes() that contains
;         times for changes for every flag.  If this is passed in then this will always be used
;         instead of looking for and reading the data files. (done for jim for his quicklook plots)
;
;   Parameters to get:
;      nflags - number of flags
;      flag_names - flag names
;      flags - tag names for flags in obs summary extension
;      flag_colors - intarr(9) of colors to use for flags
;      codes - one or two letter codes to use for each flag when plotting
;      selected - intarr(9) showing which flags are selected for display (1=selected)
;      bottom - 0/1 means show indicators at top / bottom of plot
;
; Method:
;	hsi_show_flags object provides a widget (select_widget method) to select which flags to
;	display, and where on the plot to display them.  The display method
;	uses the timerange input argument to find all selected flag changes
;	within the selected time period, and for any selected flags that are set
;	but didn't change, adds the condition to the label, and for those that changed,
;	draws a color-coded bar either at the top or bottom of the plot window, and
;	labels the bar with a one or two letter label.
;	The flags that can be displayed are:
;	SAA, Night, Flare, Front Decimation Level, Front Decimation Energy, Rear Decimation Level,
;	Rear Decimation Energy, Attenuator State, and Fronts off.
;	For SAA and Night, vertical dashed lines are drawn in addition to the bar.
;	Some of the flags (att, decim level, decim energy)  have multiple values, not just on or off, so
;	in the text label, the value is also given, e.g. A0, A1 for attenuator states 0 and 1.
;
;	Flag information is retrieved from the observing summary via the hsi_obs_summ_flag object.
;	NOTE:  if the flag_changes structure is passed in then flag_changes_fixed is set to that, and
;	  it is returned by the getdata method, and the files are never read.  Means that for this
;     instance of the object, changing times will have no effect.  This was set up for jim so
;	  that he can make quicklook plots before files are in the right place.
;
;	These flags can be drawn on any time plot that is currently active.  The bars
;	may interfere with existing labels on the plots - hence the option to draw
;	the bars at the bottom (rescaling y can always clear enough space on the bottom).
;	Plot methods that know they will call the flags_obj->display can first
;	retrieve the flag label and number of bars that will be drawn, and then append
;	the flag label to their own label, and move the label down appropriately to
;	make room for the bars.
;
; Keyword Inputs:
;
; Outputs:
;
; Examples:
;
; History:  Written Kim, Feb 2002
; Modifications:
;   14-May-2002, Kim.  Made more general to be called without plotman
;	24-May-2002, Kim.  Changed Decimation flag used to decimation_weight
;	27-May-2002, Kim.  Display each state for a single flag on a single line
;		(instead of cascading down plot for A0,A1,A0,A1,... for example, now just show 2 lines)
;	09-Aug-2002, Kim.  Added flag_changes keyword in set method (therefore also init, through _extra)
;	18-Sep-2002, Kim.  Added obs_summ_flag_obj keyword.  If passed in use it, otherwise create a
;		new one, and in either case save it in the structure.  Got rid of time_range in structure - was
;		used to keep track of when to call getdata on object, but Jim changed obj so it should work
;		internally now.  Also added /object to get call, so can retrieve hsi_obs_summ_flag object
;	23-Sep-2002, Kim.  disabled selecting fast rate flag, since don't know how that works yet.
;	12-Dec-2002, Kim.  Don't show attenuator state 4 on plots (means attenuators are in transition)
;	11-Nov-2003, Kim.  Added rear decimation (RD), rear decim energy(RE), front decim energy(FE),
;	  and fronts_off (R).  Changed front decim to FD.  Got rid of fast rates flag option.
;	4-Oct-2004, Kim.  Previously only wrote non-zero state in label if same for entire plot, but
;	  for attenuator state, include in label even if it's zero for entire plot.
;	26-Nov-2011, Kim. Call al_legend instead of legend (IDL V8 conflict)
;	21-Mar-2012, Kim. Modified flag_colors explanation
;	10-Oct-2012, Kim. In getdata, if new time is within old, still need to set time (added else...)
;
;-
;============================================================================


function hsi_show_flags::init, _extra=_extra

nflags = 9

self.obs_summ_flag_obj = hsi_obs_summ_flag()
self.user_obs_summ = 0

self.flag_changes = ptr_new(/alloc)
self.flag_changes_fixed = ptr_new(/alloc)

self.nflags = nflags
self.flag_names = ['SAA', 'Night', 'Flare', 'Front Decimation', $
	'Front Decimation Energy', 'Rear Decimation', 'Rear Decimation Energy', $
	'Attenuator', 'Fronts_Off' ]
self.flags = ['saa_flag', 'eclipse_flag', 'flare_flag', 'decimation_weight', $
	'decimation_kev', 'rear_dec_weight', 'rear_dec_kev', $
	'attenuator_state',  'fronts_off']
self.codes = ['S', 'N', 'F', 'FDn', 'FEn', 'RDn', 'REn', 'An', 'R']
self.short_names = ['SAA', 'Night', 'Flare', 'FDecim', 'FDkeV', 'RDecim', 'RDkeV', 'Att', 'ROnly']

self.flag_colors = [245,250,243,247,251,249,239,253,248]
self.selected = intarr(nflags)
self.bottom = 0

if keyword_set(_extra) then self -> set,_extra=_extra, error=error
if keyword_set(error) then return,0



return,1
end

;-----

pro hsi_show_flags::cleanup
free_var, self.flag_changes
free_var, self.flag_changes_fixed
if not self.user_obs_summ  then obj_destroy, self.obs_summ_flag_obj
end
;-----

function hsi_show_flags::get, object_reference=object_reference, _extra=_extra

if keyword_set(_extra) then begin
	input_struct=_extra
	@unpack_struct
endif

case 1 of
	keyword_set(object_reference): return, self.obs_summ_flag_obj
	keyword_set(nflags): return, self.nflags
	keyword_set(flag_names): return, self.flag_names
	keyword_set(flags): return, self.flags
	keyword_set(codes): return, self.codes
	keyword_set(flag_colors): return, self.flag_colors
	keyword_set(selected): return, self.selected
	keyword_set(bottom): return, self.bottom
	keyword_set(w_flags): return, self.w_flags
	else: return, -1
endcase
end

;-----

pro hsi_show_flags::set, selected=selected, bottom=bottom, $
	saa=saa, night=night, eclipse=eclipse, flare=flare, $
	decimation=decimation, attenuator=attenuator, $
	front_decimation=front_decimation, rear_decimation=rear_decimation, $
	front_energy=front_energy, rear_energy=rear_energy, fronts_off=fronts_off, $
	flag_colors=flag_colors, $
	obs_summ_flag_obj=obs_summ_flag_obj, flag_changes=flag_changes, error=error

error = 0

if exist(selected) then self.selected=selected
if exist(bottom) then self.bottom=bottom

selected = self.selected
if exist(saa) then selected[0]=saa
if exist(night) then selected[1]=night
if exist(eclipse) then selected[1]=eclipse   ; allow 'eclipse' as well as 'night'
if exist(flare) then selected[2]=flare
if exist(decimation) then selected[3]=decimation
if exist(front_decimation) then selected[3]=front_decimation
if exist(front_energy) then selected[4]=front_energy
if exist(rear_decimation) then selected[5]=rear_decimation
if exist(rear_energy) then selected[6]=rear_energy
if exist(attenuator) then selected[7]=attenuator
if exist(fronts_off) then selected[8]=fronts_off
self.selected = selected

if exist(flag_colors) then begin
	ctype = size(flag_colors,/type)
	if ctype lt 1 or ctype gt 3 then begin
		message,'Error - flag_colors must be integer type.',/cont
		error = 1
		return
	endif
	nc = n_elements(flag_colors)
	nflags = self.nflags
	if nc ne nflags then begin
		if nc eq 1 then flag_colors = replicate(flag_colors,nflags)
		if nc gt 1 and nc lt nflags then flag_colors = [flag_colors, intarr(nflags-nc)+255]
		if nc gt nflags then flag_colors = flag_colors[0:nflags-1]
	endif
	self.flag_colors = flag_colors
endif

if exist(flag_changes) then *self.flag_changes_fixed = flag_changes

if exist (obs_summ_flag_obj) then begin
	if not self.user_obs_summ  then obj_destroy, self.obs_summ_flag_obj
	self.obs_summ_flag_obj = obs_summ_flag_obj
	self.user_obs_summ = 1
endif

end

;-----

pro hsi_show_flags_event, event
widget_control, event.top, get_uvalue=object
if obj_valid(object) then object -> select_flags_event, event
end

pro hsi_show_flags::select_flags_event, event

widget_control, event.id, get_uvalue=uvalue

exit=0
case uvalue of

	'flag': begin
		q = where (event.id eq self.w_flags)
		self.selected[q[0]] = event.select
		end

	'all_flags': begin
		self.selected = intarr(self.nflags)+1
		for i = 0,self.nflags-1 do widget_control, self.w_flags[i], set_button=1
		end

	'no_flags': begin
		self.selected = intarr(self.nflags)
		for i = 0,self.nflags-1 do widget_control, self.w_flags[i], set_button=0
		end

	'standard_flags': begin
		self.selected = [1,1,1,1,0,1,0,1,0]
		for i = 0,self.nflags-1 do widget_control, self.w_flags[i], set_button=self.selected[i]
		end

	'position': self.bottom = event.value

	'accept': exit=1
endcase

widget_control, event.top, set_uvalue=self
if exit then widget_control, event.top, /destroy
end

;-----

pro hsi_show_flags::select_widget, group=group

flag_base = widget_base (group=group, $
					title='Select Flags', $
					/column, $
					/frame, $
					modal = keyword_set(group), $
					space=10, xpad=20, ypad=20)

hsi_ui_getfont, font, big_font

tmp = widget_label (flag_base, value=' Select HESSI flags', font=big_font, /align_center)
tmp = widget_label (flag_base, value=' to display:  ', font=big_font, /align_center)

flag_base1 = widget_base (flag_base, /column, /frame)
flag_base2 = widget_base (flag_base1, /row)
flag_base3 = widget_base (flag_base2, /column)

for i = 0,self.nflags-1 do begin
	tmp = widget_base (flag_base3, 	/column, 	/nonexclusive, 	/align_left)
	self.w_flags[i] = widget_button (tmp, value=self.flag_names[i], uvalue='flag' )
	if self.selected[i] then widget_control, self.w_flags[i], /set_button
endfor

flag_base4 = widget_base (flag_base2, /column, /align_center, space=15, xpad=10)
tmp = widget_button (flag_base4, value='All', uvalue='all_flags')
tmp = widget_button (flag_base4, value='None', uvalue='no_flags')
tmp = widget_button (flag_base4, value='Standard', uvalue='standard_flags')

tmp = cw_bgroup (flag_base1, ['Top', 'Bottom'], /row, /no_release, $
	/exclusive, ids=ids, /return_index, label_left='Position: ', uvalue='position')
val = self.bottom ? 1 : 0
widget_control, tmp, set_value=val

but_base2 = widget_base (flag_base, /row, /align_center, space=15, xpad=10)

tmp = widget_button (but_base2, value='Accept', uvalue='accept')

widget_offset, group, newbase=flag_base, xoffset, yoffset

widget_control, flag_base, xoffset=xoffset, yoffset=yoffset

widget_control, flag_base, /realize

widget_control, flag_base, set_uvalue=self

xmanager, 'hsi_show_flags::select_widget', flag_base, event='hsi_show_flags_event'

end
;-----

function hsi_show_flags::getkeywords

struct = {dummy2:0}
for i = 0,self.nflags-1 do begin
	if self.selected[i] then struct = add_tag (struct, 1, self.flag_names[i])
endfor
if n_tags(struct) gt 1 then struct=rem_tag (struct, 'dummy2')
return, struct
end

;-----

function hsi_show_flags::getdata, time_range

if ptr_exist(self.flag_changes_fixed) then return, *self.flag_changes_fixed

if (where_within(anytim(time_range), self.obs_summ_flag_obj->get(/obs_time_interval)))[0] eq -1 then begin
;if not same_data(anytim(time_range), self.obs_summ_flag_obj->get(/obs_time_interval) ) then begin
	;obs_flag_obj = hsi_obs_summ_flag(obs_time_interval=time_range)
	data = self.obs_summ_flag_obj -> getdata(obs_time_interval=time_range)
	if not is_struct(data) then return, -1
endif else self.obs_summ_flag_obj -> set, obs_time_interval=time_range
;get times of flag changes of state into fc structure
*self.flag_changes = self.obs_summ_flag_obj -> changes()
return, *self.flag_changes
end

;-----

function hsi_show_flags::getlabel, time_range, n_bar=n_bar

n_bar = 0
fc = self->getdata(time_range)
if not is_struct(fc) then return, ''

fctags = strlowcase(tag_names(fc))

for i = 0,self.nflags-1 do begin
	if self.selected[i] then begin
		q = (where (self.flags[i] eq fctags))[0]

		if strpos(self.codes[i], 'n') eq -1 then begin
			if fc.(q).start_times[0] eq -1 then begin
				if fc.(q).state eq 1 then text = append_arr(text, self.short_names[i])
			endif else n_bar = n_bar + 1

		endif else begin
			if fc.(q).start_times[0] eq -1 then begin
				if fc.(q).state gt 0. or self.short_names[i] eq 'Att' then $
					text = append_arr(text, self.short_names[i] + ' ' + trim(fix(round(fc.(q).state))))
			endif else begin
				uniq_state = get_uniq(fix(round(fc.(q).state)))
				if strpos(self.flags[i], 'att') ne -1 then begin
					ind = where(uniq_state ne 4, count)
					if count gt 0 then uniq_state = uniq_state[ind] else goto, next
				endif
				if strpos(self.flags[i], 'kev') ne -1 then begin
					ind = where (uniq_state gt 0, count)
					if count gt 0 then uniq_state = uniq_state[ind] else goto, next
				endif
				n_bar = n_bar + n_elements(uniq_state)
			endelse
		endelse
	endif
	next:
endfor

if n_bar gt 0 then n_bar = fix(n_bar * .8) > 1
if exist(text) then return, arr2str(text, ', ') else return, ''
end

;-----

pro hsi_show_flags::display, time_range, standalone=standalone

fc = self->getdata(time_range)
if not is_struct(fc) then return

psymsave=!p.psym
!p.psym=0

charsize = ch_scale(.8, /xy)

fctags = strlowcase(tag_names(fc))
n_bar = 0

for i = 0,self.nflags-1 do begin

	if self.selected[i] then begin
		q = (where (self.flags[i] eq fctags))[0]

		if strpos(self.codes[i], 'n') eq -1 then begin
			if fc.(q).start_times[0] ne -1 then begin
				code = self.codes[i]
				w = where(fc.(q).state eq 1)
				longline = (self.codes[i] eq 'N' or self.codes[i] eq 'S') eq 1
				mark_intervals, fc.(q).start_times[w], fc.(q).end_times[w], $
					label=code, color=self.flag_colors[i], n_bar=n_bar, charsize=charsize, $
					longline=longline, bottom=self.bottom
			endif

		endif else begin
			if fc.(q).start_times[0] ne -1 then begin
				state = fix(round(fc.(q).state))
				letter = ssw_strsplit(self.codes[i], 'n', /head)
				; fix needed because string of byte values produces weird characters
				code = letter[0] + trim(state)
				uniq_state = get_uniq(state)
				; don't show attenuator state 4 (means attenuators are in transition)
				if letter eq 'A' then begin
					ind = where(uniq_state ne 4, count)
					if count gt 0 then uniq_state = uniq_state[ind] else goto, next
				endif
				if letter eq 'FE' or letter eq 'RE' then begin
					ind = where (uniq_state gt 0, count)
					if count gt 0 then uniq_state = uniq_state[ind] else goto, next
				endif
				for j = 0,n_elements(uniq_state)-1 do begin
					w = where(state eq uniq_state(j))
					mark_intervals, fc.(q).start_times[w], fc.(q).end_times[w], $
						label=(code[w])[0], color=self.flag_colors[i], n_bar=n_bar, charsize=charsize, $
						bottom=self.bottom
				endfor
			endif
		endelse
	endif
	next:
endfor

!p.psym = psymsave

if keyword_set(standalone) then begin
	label = self -> getlabel (time_range, n_bar=n_bar_temp)
	if label eq '' then return
	if n_bar gt 0 then label = [replicate('', n_bar), 'HESSI: ' + label]
	al_legend,  label, /right, /top, box=0
endif

end

;-----

pro hsi_show_flags__define

self = {hsi_show_flags, $
	obs_summ_flag_obj: obj_new(), $
	user_obs_summ: 0, $
	flag_changes: ptr_new(/alloc), $
	flag_changes_fixed: ptr_new(/alloc), $
	nflags: 0, $
	flag_names: strarr(9), $
	flags: strarr(9), $
	codes: strarr(9), $
	short_names: strarr(9), $
	flag_colors: intarr(9), $
	bottom: 0, $
	selected: intarr(9), $
	w_flags: lonarr(9)}
end
