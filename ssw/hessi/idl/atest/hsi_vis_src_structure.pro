;+
; Name: hsi_vis_src_structure
; 
; Purpose: Create a source structure used in vis_fwdfit and set default fit mask to all 1s.
; 
; Calling sequence:  vf_struct = hsi_vis_src_structure()
; 
; Written: 6-Jul-2017, Kim Tolbert
;-

function hsi_vis_src_structure

z = {hsi_vis_src_structure}

z.srctype = 'circle'
z.fitmask = 1

return, z
end