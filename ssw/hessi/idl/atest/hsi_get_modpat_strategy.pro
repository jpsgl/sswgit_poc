;+
; :Description:
;    Returns the object class name of the hsi_modul_pattern strategy
;    either ['HSI_ANNSEC_PATTERN', 'HSI_VISMOD_PATTERN', 'HSI_CART_PATTERN']
;
; :Params:
;    self - image object or any object with the modul_pattern class upstream
;
;
;
; :Author: raschwar, 5 Jun 2017
;-
function hsi_get_modpat_strategy, self

modpat_obj = self->Get(/object, class_name = 'hsi_modul_pattern' )
strategy =  modpat_obj->getstrategy()
return, obj_class( strategy )

end