;+
; Name:  hsi_image::plot_aspect
;
; Purpose:  plot_aspect method for image object.  Extracts aspect object
;	and calls its plot method.

;
; Calling sequence:  obj -> plot_aspect
;	_extra keywords are set in object, and get passed to hsi_aspect_solution::plot.
;	If keyword
;
; Keyword Inputs:
;	plot keywords and object keywords allowed in hsi_aapect_solution::plot
;
; Input/Output Keywords:
; plotman_obj - if refers to an exsiting plotman session, uses it.  If not, creates a new one and passes out the reference to it.
; 
; Outputs:  Opens new plot window for aspect plot or creates panel in existing or new plotman session.
;
; History:  Written Kim Tolbert, 25-Apr-2007
; 27-Jul-2015, Kim. Added plotman_obj keyword. If set, then call plotman method instead of plot method,
;
;-
;============================================================================

pro hsi_image::plot_aspect, plotman_obj=plotman_obj, _extra=_extra

if keyword_set(_extra) then self->set,_extra=_extra

asp_obj = self -> get(/obj, class='hsi_aspect_solution')

if arg_present(plotman_obj) then asp_obj -> plotman, plotman_obj=plotman_obj, _extra=_extra else asp_obj -> plot, _extra=_extra

end

