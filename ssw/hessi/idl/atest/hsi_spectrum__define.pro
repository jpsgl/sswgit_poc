;---------------------------------------------------------------------------
; Document name: hsi_spectrum__define.pro
; Created by:    Andre, June 1999
;
; Last Modified: Thu Jul 12 20:26:33 2001 (csillag@TOURNESOL)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       SPECTRUM CLASS DEFINITION
;
; PURPOSE:
;       Provides access to count rate and
;       semi-calibrated spectra.
;
;       The spectrum class extends the spectrogram class. It loads the
;       spectrogram and transforms it according to reqirements of the
;       spectral analysis.  It also adapts control and  info
;       parameters before passing them to hsi_spectrohgram. In
;       practis, what this really means is that it constructs a
;       variable called "binning" acoording the the spectrum control
;       parameters, in control2binning. binning is used by the spectrogram
;       class to build the spectrogram
;
; CATEGORY:
;       Spectrum (hessi/idl/spectra)
;
; CONSTRUCTION:
;       obj = Obj_New( 'hsi_spectrum' )
;       obj = HSI_Spectrum()
;
; (INPUT) CONTROL PARAMETERS DEFINED IN THIS CLASS:
;
;      sp_chan_binning: defines the channel binning. It can be a
;                       single value, which is a constant channel
;                       binning.  Sometimes called a grouping factor. If
;                       this is used (i.e. nonzero) it has precedence
;                       over the sp_energy_binning described below
;      sp_chan_min       minimum channel number used in sp_chan_binning
;      sp_chan_max       maximum_channel_number used in sp_chan_binning
;
;      sp_energy_binning: defines the energy binning of the
;                         spectra. If this is a single value (scalar), it reads
;                         the corresponding ct_edges table in the
;                         dbase. If it is a 1D array with M values, it
;                         considers each value as the edge of a
;                         (contiguous) energy channel (therefore there
;                         will be M-1 energy bins). If it is a 2 x N
;                         array, then each 2-element vector is a
;                         separate energy channel, and the resulting
;                         spectrum has N energy bins.
;
;      sp_semi_calib: calibrates the count rate spectrum using only
;                     the diagonal elements of the response matrix.
;                     Default: 0
;
;      sp_time_interval: defines the time interval in seconds for the
;                        accumulation
;             Single values are interpreted as the time bin duration in seconds.
;             Multiple values are first passed through anytim().  The result
;             is compared to the start of the ABSOLUTE_TIME_RANGE.  IF the values
;             are less, then they are interpreted as relative to the start of
;             ABSOLUTE_TIME_RANGE, otherwise they are interpreted as
;             times in seconds from 1-jan-1979.
;
;      All control parameters from hsi_spectrogram__control are also
;      inherited by this class.
;
;      ;For the spectrum class coincidence_flag and sum_coincidence have
;     this behavior
;      coincidence_flag (byte) : Default is 0.  Accumulate only
;                                anti-coincdent events. If set, then accumulate
;                     anti and coincident events into separate spectra.
;          These two spectra are held in memory and reported on getdata
;      and identified by the last index.
;
;
;      sum_coincidence: sum over coincidence condition on getdata.
;      When sum_coincidence is changed to 1, coincidence_flag is also set
;      Changing coincidence_flag to 0 from 1 will not trigger reprocessing
;     of the spectrogram unless forced.  With /COINCIDENCE_FLAG set
;     don't set SP_DATA_STR because it doesn't take care of all cases
;     in writing the structure.
;

;
; (OUTPUT) INFORMATIONAL PARAMETERS DEFINED IN THIS CLASS:
;
;      Info parameters from hsi_spectrogram_info__define are inherited
;      by this class.
;
; CLASS RELATIONSHIPS
;      HSI_Spectrogram, Framework: parent classes
;      HSI_Eventlist: source class
;
; KEYWORDS:
;      this_sum_flag: averages all spectra over detector elements chosen in the
;      det(seg)_index_mask
;
;
; EXAMPLES:
;
;       Start by creating an instance of the spectrum object:
;       obj = HSI_Spectrum()
;
;       obj->Plot
;       plots the integrated spectrum of the standard simulation
;
;       sp = obj->GetData( SUM_FLAG = 0 )
;       returns in sp a spectrum for each a2d and for each time
;       interval defined in sp_time_interval. Uses the default energy
;       binning. The energy values of the energy bins are retrieved by:
;       xaxis = obj->GetData( /XAXIS )
;
;       s = o->GetData( THIS_DET = [1,2] )
;       returns in s the spectra for detectors # 1 and 2 only.
;       o->Plot, THIS_DET = [1,2] will plot the sum of the spectra for
;       detectors 1 and 2.
;
;       s = o->GetData( THIS_SEG = [1,10] )
;       returns in s the spectra for segments # 1 and 10 only
;
;       s = o->GetData( THIS_A2D_INDEX = [1, 10, 13, 14, 26] )
;       returns the spectra for a2d indices 1,10,13,14,26
;
;       s = o->GetData( SP_CHAN_BINNING = 10 )
;       returns the spectra with a channel binning of 10 channels. In
;       this case, the axis is in channels, and not in energy, and the
;       values are counts and not counts / keV.
;
;       s = o->GetData( SP_TIME_INTERVAL = 1 )
;       returns a spectrum for each second of data during the defined time
;       range.
;
; s = o->GetData( SP_DATA_UNIT = 'Counts') [or 'C' or 'c' ]
; returns a spectrum with units of Counts per accumulation bin
;
; s = o->GetData( SP_DATA_UNIT = 'Rate') [or 'R' or 'r']
; returns a spectrum with units of Counts per Second
;
; s = o->GetData( SP_DATA_UNIT = 'Flux') [or 'F' or 'f']
; returns a spectrum with units of Counts per Second per CM^2 per keV
;
; s = o->GetData( /SP_DATA_STRUCTURE)
; returns a spectrum structure with tags for Count (Rate or Flux)
; and Ecount (Erate or Eflux)
;
;       s = o->GetData( SP_ENERGY_BINNING = 2 ) returns spectra that
;       use the energy binning defined in /ssw/hess/dbase/spec...
;
;       s = o->GetData( /COINCIDENCE_FLAG )
;       returns a 4-dim array where the 4th dimension contains the
;       anti-coincident spectrum (index #0) and the coincident
;       spectrum (index #1). Note that sum_flag is set to 0 in this
;       case. If sum_flag would be set to one, you would get all the
;       events, coincident or not (what you would not get if sum_flag
;       were 1 and coicidence_flag 0
;
;       s =o->GetData( /COINCID, OTHER_A2D_INDEX =
;       [0,0,0,1,1,1,1,bytarr(27-7) ] )
;       returns a 4-dim array where the 4th dim has 5 indices, the
;       first is the anti-coincident spectrum, the second is the
;       coincident spectrum with a2d # 3, the third with a2d #4, etc.
;
; sp_data_info = o->Get(/SP_DATA_INFO) returns
; an info parameter structure with pointers for the Eff (from SP_SEMI_CALIBRATED)
; Ltf (livetime fraction) and De (channel energy width in keV) and
; Dt (bin time width in seconds)
;
; RELATED DATA STRUCTURES:
;       {hsi_spectrum_control}
;       {hsi_spectrum_info}
;
; SEE ALSO:
;        http://hessi.ssl.berkeley.edu/software/reference.html#hsi_spectrum
;        framework__define
;        hsi_eventlist__define
;        hsi_spectrum_control__define
;        hsi_spectrum_info__define
;
; HISTORY:
; 17-apr-2013, ras. The block to check the coincidence conditions has been moved to spectrogram
; 2010-Nov-3, ras, in eff(), only selected segments appear in srmdiag and this
;	was previously unaccounted for, only affected semi_cal output and not spectral response fits files
;	, bug fix, ras, 3-nov-2010
; 2010-Aug-12, Kim. In GET, changed AND to && for IDL 8.0 (n_tags(null obj) fails in 8.0)
; 20-jan-2009, ras, added artifact masking, channels are turned off for specfic detectors
;	based on time through the mission.  the artifact_mask object is used to construct
;	this weighting function used with data,  and srm.  This object is included as a property
;	of this object so it can be referenced for both the data and srm task as needed
;	this feature is turned off if any front segments are used.  It is meant to be used in OSPEX
;	and is made available through hsi_spectrum__filewrite as a flag (keyword).  Should only be used
;	by analysts who know they want to use this feature.  USed here via getdata(/use_artifact_mask,...
; 28-sep-2009, ras, do a better job of figuring number of live detectors in each time interval
;		also, save sp_energy_binning at high energy above 20 MeV (channel limit) for photon edges with srm object
;		Changes are in init, control_2_binning and getdata
; 10-Aug-2009, Kim.  Added cleanup method to free self object and pointers.
; 26-sep-2007, ras, fixed bug for bins straddling 2750 keV
; 12-apr-2007, Kim.  Added call to hsi_old_defaults
; 10-apr-2007, ras, changed position of set, _extra in init
; and changed set for pileup to properly check for PILEUP prefix
; 2-apr-2007, ras, in getdata(), change the num of detector weighting
;	to be a function of time by turning the livetime into a 1/0 step function for each segment
; 29-mar-2007, ras, in ::Eff changed call to new srm method, get_diagonal()
; 17-oct-2006, ras, reproduced the a-coincidence livetime in the coincidence livetime
;	when they are requested.
; 15-mar-2006, ras, If new times outside of earlier obs_time, they become the new obs_time
; sp_time_interval and obs_time_interval now trapped in set
; 18-nov-2005, ras, modified control2binning, if sp_time_interval is set directly with multiple
;   bins, then time_range is set to fltarr(2) as the sp_time_interval is being controlled directly.
; 17-oct-2005, ras, changed test in getdata, instead of scalar test, 1 or fewer elements from sp
; 11-oct-2005, removed CONTROL from hsi_spectrum::get(), appears to be leftover from a development
;   phase associated with hsi_pileup as earlier versions did not have the pileup object and did
;   not have the get method, used the one inherited from hsi_spectrogram
; 5-oct-2005, ras, moved sp_data_ltf, sp_eff_info, and counts_2_unit to Self. instead of info params
;   This is to reduce the overhead in the FITS files.
; 5-jul-2005, ras, moving pileup functions to new pileup object contained in spectrum
;   object as a property (self.pileup)
; 29-sep-2004, ras, in process_post_hook revise test for uncertain atten_state
; 9-jun-2004, ras, revised documentation re sp_chan_binning
; 21-may-2004, ras, cleaned up behavior of COINCIDENCE_FLAG and SUM_COINCIDENCE
;     moved from spectrogram::set to spectrum::set because behavior is
;     different in binned_eventlist
; 13-aug-03, ras, big modifications to set and getdata to support pileup correction
;   implemented valid off_axis distance in degrees for srm
; 6-nov-02, ras, added control2binning_prehook needed by lightcurve
; 30-oct-02, ras, fix problem with one detector where errors reported
;   had sqrt applied twice to the counts
; 24-Oct-2002, Kim.  Added @hsi_insert_catch in getdata, and commented out some extra checks
;   for when no files are available - catch will handle it now.
; 1-oct-2002, ras, save num_rm
; 25-Jul-2002, Kim.  Call heap_gc (if >= 5.5) in getdata method to prevent memory leaks
; 19-jul-02, ras, fixed another bug in get_arf for a single time interval
; 10-jul-02, ras, fixed bugs in using sum_coincidence and multiple atten states
; 28-jun-02, ras, high energy a2d counts disabled below 2750 keV except when using
;     sp_chan_binning option.  all high energy a2d (a2d ge 18) are disabled for bin
;     widths less than 2.1 keV.  Rebinning algorithm fails for narrow bins.
; 21-jun-02, ras, I had oublied the arf case for 1 shutter state. fixed in get_arf()
; 10-Jun-02, Kim.  In Getdata, if not hsi_use_sim, using hsi_eventlist_packet, and no file, then error
; 17-may-02, ras, in Group, used long64 on time  bin
;   in control2binning
; 1-April-2002, Paul Bilodeau - added noupdate field and methods for
;   recreation of object without processing.
; 22-mar-2002, fixed bug in ::eff() after change in output of
;    /diagonal hessi_build_srm. Remove multiplication by channel energy widths.
; 19-mar-2002, ras, many fixes from 23-jul, most recent was to
;   use hsi_spectrogram::getaxis to consistently get energy using /xaxis
;   and time using /yaxis.
; 23-jul-2001, ras,
; 4-Jun-2001, richard schwartz, make sure args to rebin are vectors
; Release 5.1, richard.schwartz@gsfc.nasa.gov 14-feb-2001
;   fixed bug in sp_time_interval to binning choices
;       Release 5.1, inherits hsi_spectrogram, ACs, Dec 2000
;       Release 5, May-June 2000, ACS
;                  coincident event spectra +  packet bunches
;       Release 4, December 1999-February 2000
;           A Csillaghy, csillag@ssl.berkeley.edu (ACS)
;
;---------------------------------------------------------------------------

FUNCTION HSI_Spectrum::INIT, SOURCE = source, $
            INFO=info, $
            _EXTRA=_extra

CheckVar, info, {hsi_spectrum_info}
ret=self->HSI_Spectrogram::INIT( CONTROL = hsi_spectrum_control(), $
                                 SOURCE=source, $
                                 INFO=info) ;, $  ras, 10-apr-2007
                                 ;_EXTRA=_extra ) ras, 10-apr-2007
obj = obj_new( 'hsi_srm' )
self.artifact_mask = obj_new('hsi_artifact_mask')
self.pileup = obj_new('hsi_pileup')
self.sp_data_ltf = ptr_new(0)
self.sp_eff_info = ptr_new(0)
self.counts_2_unit = ptr_new(0)
self.last_sp_energy_binning = ptr_new(-1)
obj ->Set, NEED_UPDATE=0


self.noupdate = 0

self->set, source = obj, src_index=1
self->set, time_range =  fltarr(2)
self->set, _extra=_extra   	; ras, 10-apr-2007

hsi_old_defaults, self, _extra=_extra
self->set, energy_band = fltarr(2)

RETURN, ret

END

;----------------------------------------------------------;

PRO HSI_Spectrum::cleanup
self->framework::cleanup
destroy, self.pileup
ptr_free, self.sp_data_ltf
ptr_free, self.sp_eff_info
ptr_free, self.counts_2_unit
end

;----------------------------------------------------------;

PRO HSI_Spectrum::setnoupdate
self.noupdate = 1
END

FUNCTION HSI_Spectrum::getnoupdate
RETURN, self.noupdate
END

;--------------------------------------------------------------------
FUNCTION HSI_Spectrum::Need_Update,  NO_LAST_UPDATE = no_last_update


IF self->Get( /NEED_UPDATE ) NE 0 OR NOT Ptr_Valid( self.data ) THEN BEGIN
    need_update =  1
ENDIF ELSE BEGIN
    need_update = 0
    i = 0
    FOR i=0, N_Elements( self.source )-1 DO BEGIN
        IF Obj_Valid(  self.source[i] ) THEN $
       ;The spectrum class is independent of the state of the HSI_SRM class
         IF Obj_Class( self.source[i] ) ne 'HSI_SRM' THEN BEGIN
            need_update = need_update OR self.source[i]->Need_Update() OR $
                ( NOT Keyword_Set( NO_LAST_UPDATE ) AND  $
                  ( self->Get( /LAST_UPDATE ) LT self.source[i]->Get( /LAST_UPDATE ) ))
        ENDIF
    ENDFOR
ENDELSE

return, need_update

END
;---------------------------------------------------------------------------

FUNCTION HSI_Spectrum::Get, $
               _EXTRA=_extra, $
               NOSINGLE=NOSINGLE, $
               NOT_FOUND=NOT_found, $
                  FOUND=found, $
                  ;CONTROL=control, $
               xyoffset=xyoffset, $
               ;These values can only be retrieved individually by specific keyword
               sp_data_ltf=sp_data_ltf, $
               sp_eff_info=sp_eff_info, $
               counts_2_unit=counts_2_unit


@hsi_insert_catch

if keyword_set(sp_data_ltf) then return, *self.sp_data_ltf
if keyword_set(sp_eff_info) then return, *self.sp_eff_info

if keyword_set(counts_2_unit) then return, *self.counts_2_unit



out= Self->HSI_SPECTROGRAM::Get(  $
                     xyoffset=xyoffset, $
                     NOT_FOUND=NOT_found, $
                     FOUND=found, $

                     _EXTRA=_extra, /NOSINGLE )


if obj_valid(Self.Pileup) then pileup_param = Self.Pileup->Get(_extra=_extra,$
    xyoffset=xyoffset, $
     /nosingle)

p_struct = size(pileup_param,/tname) eq 'STRUCT'
o_struct = size(out,             /tname) eq 'STRUCT'
case 1 of
    o_struct and p_struct: out = join_struct(out, pileup_param)
    o_struct:
    p_struct: out = pileup_param

    1:
endcase

; added check for struct, and changed AND to && for Version 8.0, Aug 12,2010
out = (NOT keyword_set(NOSINGLE)) && is_struct(out) && (n_tags(out) eq 1) ? out.(0) : out



return, out

end
;----------------------------------------------------------
Function HSI_Spectrum::GetPileupObj ;For development only
return, Self.Pileup
end

;---------------------------------------------------------------------------

PRO HSI_Spectrum::Set, $
       SP_DATA_UNIT=sp_data_unit, $
       SP_DATA_STRUCTURE=sp_data_structure, $
       SP_CHAN_BINNING=sp_chan_binning, $
       SP_ENERGY_BINNING=sp_energy_binning, $
       SP_SEMI_CALIBRATED=sp_semi_calibrated, $
       SP_SQRT=sp_sqrt, $
       SP_TIME_INTERVAL = sp_time_interval, $
       OBS_TIME_INTERVAL= obs_time_interval, $

       ;NOT_FOUND=NOT_found, DONE=done, $
       XYOFFSET = XYOFFSET, $
       USE_FLARE_XYOFFSET=USE_FLARE_XYOFFSET, $
       COINCIDENCE_FLAG = coincidence_flag, $
       SUM_COINCIDENCE  = sum_coincidence, $

       _EXTRA=_extra

MASK = Self->Get(/seg_index_mask, /det_index_mask, /a2d_index_mask, /SUM_FLAG)
If keyword_set(OBS_TIME_INTERVAL) or keyword_set(SP_TIME_INTERVAL) then begin

	Self->Control2Binning, SP_TIME_INTERVAL = sp_time_interval, $
       OBS_TIME_INTERVAL= obs_time_interval
	endif


IF Keyword_Set( _extra ) THEN BEGIN

    self.noupdate = 0 ;needed for object creation from a FITS file
    taglist = [tag_names( {hsi_pileup_control}),tag_names({hsi_pileup_info})]
    pileup_param = str_subset(_extra, taglist,/quiet,/include, status=status)
    ;add wildcard to PILE, ras, 10-apr-2007
    if status eq 0 then $
    	pileup_param = str_subset(_extra, 'PILE*',/quiet,/include,/regex,status=status)


    If Status and obj_valid(self.pileup) $
        then self.pileup->Set, _EXTRA = pileup_param
    self->HSI_Spectrogram::Set, _EXTRA = _extra

ENDIF
;We need /NO_UPDATE until we make xyoffset a parameter of hsi_srm
If Keyword_Set( xyoffset ) or $
    N_elements( USE_FLARE_XYOFFSET) eq 1 THEN $
    Self->SETXYOFFSET, xyoffset=xyoffset, $
    use_flare_xyoffset=fcheck(use_flare_xyoffset,0), /no_update


IF Keyword_Set( sp_data_unit ) THEN BEGIN
    test = ['Flux', 'Rate', 'Counts']
    sp_data_unit = grep( strlowcase(sp_data_unit), test )
    if sp_data_unit eq '' then sp_data_unit = 'Counts'
    self->Framework::Set, SP_DATA_UNIT = sp_data_unit, /NO_UPDATE
ENDIF

;The block to check the coincidence conditions has been moved to spectrogram
;ras, 17-apr-2013
if 1 then begin
Self->HSI_Spectrogram::Set_Coincidence, $

       COINCIDENCE_FLAG = coincidence_flag, $
       SUM_COINCIDENCE  = sum_coincidence, $
        fw_set_id=fw_set_id
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Paul Bilodeau, 10-jul-2002
; Check the coincidence control parameters and insure that they are consistent
if 0 then begin
have_coincidence_flag = Exist( coincidence_flag )
have_sum_coincidence = Exist( sum_coincidence )
IF have_sum_coincidence OR have_coincidence_flag THEN BEGIN
    IF NOT( have_coincidence_flag ) THEN $
      coincidence_flag = Self->Get( /COINCIDENCE_FLAG )

    IF NOT( have_sum_coincidence ) THEN $
      sum_coincidence = Self->Get( /SUM_COINCIDENCE )

    dim_spectrogram = Self->Get(/dim_spectrogram)
    have_coincidence= 0
    if n_elements(dim_spectrogram) eq 4 then have_coincidence = dim_spectrogram[3] ge 2

    no_update = have_coincidence ;coincidence_flag EQ ( coincidence_flag OR sum_coincidence )

    coincidence_flag = coincidence_flag OR sum_coincidence

    Self->FRAMEWORK::Set, $
      SUM_COINCIDENCE=sum_coincidence, $
      NO_UPDATE=no_update, $
      COINCIDENCE_FLAG=coincidence_flag
ENDIF
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; moved this to spectrogram, but the way it's implemented is
; different. Need to chaeck with RAS if the functionality is still the same
;det_index_mask=KeyWord_Set( det_index_mask )?det_index_mask >0 < 1:bytarr(9)
;seg_index_mask=KeyWord_Set(SEG_INDEX_MASK)?seg_index_mask >0 < 1:bytarr(18)
;a2d_index_mask = self->Get( /A2D_INDEX_MASK )>0 < 1
;
; Order of precedence DET, SEG, A2D
;mask_flag=total(det_index_mask) ge 1? 'DET' : total(seg_index_mask) ge 1 ? 'SEG' : 'A2D'
;if mask_flag eq 'DET' then self->Framework::Set, /NO_UPDATE, DET_INDEX_MASK = byte(det_index_mask)
;if mask_flag eq 'SEG' then self->Framework::Set, /NO_UPDATE, SEG_INDEX_MASK = byte(seg_index_mask)

; moved this one to spectrogram too. We will need it in lightcurve.
; IF EXIST( SUM_FLAG ) then Self->Framework::Set, /NO_UPDATE, SUM_FLAG = byte(sum_flag>0 < 1)

IF N_Elements( SP_DATA_STRUCTURE ) NE 0 THEN $
  Self->Framework::Set, /NO_UPDATE, $
    SP_DATA_STRUCTURE=byte( sp_data_structure>0 < 1)

IF N_Elements( SP_SEMI_CALIBRATED ) NE 0 THEN $
  Self->Framework::Set, /NO_UPDATE, $
    SP_SEMI_CALIBRATED = byte( sp_semi_calibrated>0 < 1)

IF N_Elements( SP_SQRT ) NE 0 THEN $
    Self->Framework::Set, /NO_UPDATE, SP_SQRT = byte( sp_sqrt>0 < 1)

;IF Keyword_Set( SP_ENERGY_BINNING ) THEN BEGIN
;Change from Keyword_Set to Exist because 0 is meaningful value for sp_energy_binnng
;Make energy bands contiguous
IF Keyword_Set( sp_energy_binning ) AND $
  Self->Get(/CONTIG_ENERGY_EDGES) THEN BEGIN
    test = same_data( get_edges(/edges_1, sp_energy_binning), $
      get_edges(/contig, sp_energy_binning))
  IF NOT TEST THEN message,/continue, 'Forcing SP_ENERGY_BINNING contiguous'
  sp_energy_binning = get_edges(/contig, sp_energy_binning)
ENDIF

;Make energy bands contiguous
IF Keyword_Set( SP_CHAN_BINNING ) AND $
  Self->Get(/CONTIG_ENERGY_EDGES) THEN BEGIN
    test = same_data( get_edges(/edges_1, SP_CHAN_BINNING),  $
      get_edges(/contig, SP_CHAN_BINNING))
  IF NOT TEST then message,/continue, 'Forcing SP_CHAN_BINNING contiguous'
  SP_CHAN_BINNING = get_edges(/contig, SP_CHAN_BINNING)
ENDIF

IF EXIST( SP_ENERGY_BINNING ) THEN BEGIN
    self->Framework::Set, SP_CHAN_BINNING = 0
    self->Framework::Set, SP_ENERGY_BINNING = sp_energy_binning
ENDIF

IF Keyword_Set( SP_CHAN_BINNING ) THEN BEGIN
    self->Framework::Set, SP_ENERGY_BINNING = 0
    self->Framework::Set, SP_CHAN_BINNING = sp_chan_binning
    IF n_elements( sp_chan_binning ) GT 1 THEN BEGIN
      limits = minmax( sp_chan_binning )
      Self->Framework::Set, SP_CHAN_MIN = limits[0], SP_CHAN_MAX = LIMITS[1]
  ENDIF
ENDIF
;Be sure to set the srm use_segment variable if seg_index_mask or det_index_mask change
OSRM = Self->Get(/obj, class_name = 'HSI_SRM' )
if obj_valid(OSRM) then begin
    if not same_data(mask, $
       Self->Get(/seg_index_mask, /det_index_mask, /a2d_index_mask, /SUM_FLAG)) then begin
       OSRM->Set, spectrum_obj=self
       endif
endif
END

;---------------------------------------------------------------------------

FUNCTION HSI_Spectrum::GetData, CLASS_NAME=class_name, $
                               THIS_SP_DATA_STRUCTURE=this_sp_data_structure, $
                                this_sum_flag=this_sum_flag, $
                                SQRT_OUT=sqrt_out, $
                                NOPACKET=nopacket, $
                                XAXIS=xaxis, YAXIS=yaxis, $
                                KEEP_DROPOUT=keep_dropout, $
                                USE_ARTIFACT_MASK=use_artifact_mask, $
                                NOPILEUP_CORRECT=nopileup_correct,$ ;needed to extract rate w/o infinite loop
                                _EXTRA=_extra

@hsi_insert_catch

IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA =  _extra

; this code was removed when added catch, kim 21-aug-02
; if eventlist strategy is packet then check if the filename is valid.  If not,
; print message and return -1.  Otherwise it prints MANY message that it can't find file. kim
;ev = self->get(/obj,class='hsi_eventlist') ;kim 11-apr-2002 added this block
;this_strat = ev -> getstrategy()
;good = 1
;if not hsi_use_sim() then begin
;   IF Obj_Class( this_strat ) EQ 'HSI_EVENTLIST_PACKET' THEN BEGIN
;       filename = self->Get( /FILENAME )
;       IF Size( filename, /TYPE ) NE 7 THEN good=0 else $
;         if filename[0] eq '' then good=0
;       if not good then begin
;         message, 'No file available.  Not making spectrum.', /info
;         return, -1
;       endif
;   ENDIF
;endif

IF Keyword_Set( CLASS_NAME ) THEN $
  IF class_name NE Obj_Class( self ) THEN BEGIN
    source = self->Get( /SOURCE )
; si ce n'est toi, c'est donc ton frere
    RETURN, source->GetData( CLASS_NAME=class_name, _EXTRA=_extra )
ENDIF

IF self.noupdate THEN RETURN, *self.data

;If TIME_FIRST then we have LC dimensions
Time_first = Self->Get( /SP_TIME_FIRST )

IF Keyword_Set( XAXIS ) OR Keyword_Set( YAXIS ) THEN $
    RETURN, self->GetAxis( XAXIS=xaxis, YAXIS=yaxis, _EXTRA=_extra )

IF KeyWord_Set( THIS_SP_DATA_STRUCTURE ) OR $
    self->Get(/SP_DATA_STRUCTURE) AND $
    NOT KeyWord_Set( NOPACKET )  THEN BEGIN

; here we pass _extra
    self->SPECTRA4SPEX, packet, _EXTRA = _extra, keep_dropout=keep_dropout
    if since_version('5.5') then heap_gc     ;kim 25-jul-2002.  Try this!!!!!
    If Exist(packet) then RETURN, packet else return, -1

ENDIF


sp=self->HSI_Spectrogram::GetData( _EXTRA=_extra, keep_dropout=keep_dropout)
;Use artifactmask for rear segments


if since_version('5.5') then heap_gc       ;kim 25-jul-2002.  Try this!!!!!
;;;;;;;;;;;;Pileup Correction Code;;;;;;;;;;;;;;;;;;
default, nopileup_correct, 0
pileup_correct = Self.pileup->Get(/pileup_correct)
If (Not nopileup_correct and Pileup_Correct) then begin
    ;Pileup is corrected in correct_pileup only if it is required!!
    Self.pileup->correct_pileup, sp, spectrum_obj=Self
    endif
;;;    If size(/tname, pidx) eq 'STRUCT' then begin
;;;       nindex = n_elements(pidx)
;;;
;;;       if nindex ge 1 then for idx=0,nindex-1 do $
;;;         sp[0,pidx[idx].tbin, pidx[idx].seg_index]=$
;;;         sp[*,pidx[idx].tbin, pidx[idx].seg_index]*pileupratio[*,idx]
;;;       endif
;;;    endif
;;;;;;;;;;;;Pileup Correction Code;;;;;;;;;;;;;;;;;;




coincidence_flag = Self->Get(/coincidence_flag)
if not coincidence_flag then sp=sp[*,*,*,0]

sp_size = Size( sp, /STRUCT )
; get out of here if spectrogram is a scalar acs 2002-03-06
; changed test, ras, 17-oct-2005, used to look for a scalar
IF sp_size.n_elements LE 1 THEN return, -1

dim_start = long(self->Get( /INFO, /DIM_SPECTROGRAM )) ; Size( srm, /dim )

nx   = dim_start[0]>1
ny   = dim_start[1]>1
nz   = dim_start[2]>1
nc   = coincidence_flag ? dim_start[3]>1 : 1
ndim = sp_size.n_dimensions

mask = Self->Get(/det_index_mask, /a2d_index_mask, /seg_index_mask)
dmask = mask.det_index_mask
smask = mask.seg_index_mask
amask = mask.a2d_index_mask

front =  min(where(smask)) le 8 ;cannot use art_mask if any fronts are selected
if keyword_set(use_artifact_mask)  &&  ~front  then $
	sp = self.artifact_mask->getdata(sp, spec_obj=self,ntime=ny,ncoinc=nc)



its_a2d = total( dmask) + total(smask) eq 0 ? 1 : 0
;How many detectors have been summed

num_rm = its_a2d ? total( amask[0:8] or amask[9:17] or amask[18:26]) : $
        total( dmask ) >  total(  smask[0:8] or smask[9:17] )
Self->Set, num_rm= num_rm ;number of detectors used to normalize rate per detector


counts_2_unit = 1.0
sp_semi_calibrated = Self->Get( /SP_SEMI_CALIBRATED )
event_unit = sp_semi_calibrated ? 'photons' : 'counts'
;IF sp_semi_calibrated THEN BEGIN
    ;Make sure the Diagonal response has been processed

eff = rebin( Self-> Eff()+fltarr(nx,1,nz,nc), nx, ny, nz, nc)
Self->A2D_SELECT, eff, /SUMSTYLE
if sp_semi_calibrated then begin
    Arf =Self->Get_Arf()
    If n_elements(arf) gt 1 then Self->A2D_SELECT, arf, /ARFSTYLE

    eff = eff *arf
    endif


sp_data_unit = strlowcase( Self->Get( /SP_DATA_UNIT ) ); 'Counts' 'Flux' 'Rate'
units_spectrogram = event_unit
per_detector = 0 ; This means we're just looking at Counts or Photons, Otherwise normalize per detector or per cm2
area = 1.0
sp_data_ltf = *Self.sp_data_ltf
dim_ltf = size(/dim, sp_data_ltf)
sp_data_ltf = dim_ltf[0] eq nx ? sp_data_ltf : $
    rebin( sp_data_ltf +fltarr( 1, ny, nz, nc), nx, ny, nz, nc)
IF sp_data_unit NE 'counts' THEN BEGIN
    per_detector = 1            ;normalize per detector or unit area
    counts_2_unit = temporary( sp_data_ltf * $
      rebin( Self->Get(/sp_data_dt)+$
      fltarr(1, ny, 1, 1), nx, ny, nz, nc) )
    units_spectrogram = event_unit + ' '+'s!u-1!n detector!u-1!n'
    IF sp_data_unit EQ 'flux' THEN BEGIN

        units_spectrogram = event_unit + ' '+ 'cm!u-2!n s!u-1!n keV!u-1!n'
        area  = hessi_constant( /detector_area)
        counts_2_unit = counts_2_unit * $
          temporary( rebin( Self->Get(/sp_data_de)+fltarr(nx, 1, 1, 1), nx, ny, nz, nc) ) *area


    ENDIF



    if n_elements(counts_2_unit) gt 1 then self->A2D_SELECT, counts_2_unit, SUMSTYLE = 0

ENDIF
;Meaning of Sum_flag changed to "Sum over Detectors/Segments"
;To sum over coincidence states, set SUM_COINCIDENCE
sum_flag = self->Get( /SUM_FLAG ) OR Keyword_Set( this_sum_flag ) > 0 < 1
;To sum_coincidence coincidenc_flag and sum_coincidence must both be set
sum_coincidence = coincidence_flag and ( Self->Get(/SUM_COINCIDENCE) >0 <1)

if SP_SEMI_CALIBRATED then counts_2_unit = temporary( counts_2_unit * eff)

self->set, units_spectrogram = units_spectrogram
*Self.counts_2_unit=counts_2_unit

nwt = 1.0
;040307, ras
if (num_rm < n_elements(counts_2_unit)) gt 1 then begin
	cdim = size(/dim, counts_2_unit)
	cdim[0]=1
	;nwt = counts_2_unit[0,*,*,*]
    nwt = (reform(avg(counts_2_unit,0),cdim))[0,*,*,*]

	nwt = f_div(nwt,nwt)
	if nx ge 1 then nwt= rebin(total( nwt,3),nx,ny,nc)
	endif

;040307, ras

sp = f_div( sp, counts_2_unit )
sp_sqrt = (Self->Get( /this_class_only, /SP_SQRT)>0 < 1) or Arg_Present( sqrt_out)



; sum_flag means sum over detectors and NOT coincidence
If SUM_COINCIDENCE and size(/tname, sp) ne 'STRUCT' then sp = total( sp, 4 )

if SP_SQRT Then $
    sqrt_out=f_div( sqrt( abs(sp) ), sqrt(counts_2_unit) )

IF sum_flag and size(/tname, sp) ne 'STRUCT' THEN BEGIN

    ;if sp_size.dimensions[3] ge 2 then sp = total( sp, 4 )
    if sp_size.dimensions[2] ge 2 then sp = total( sp, 3 )
    ;sp = per_detector ? sp / num_rm : sp
     sp = per_detector ? f_div(sp , nwt) : sp  ;28-sep-09, ras


    If SP_SQRT THEN BEGIN
        ;if sp_size.dimensions[3] ge 2 then sqrt_out = total( sqrt_out^2, 4 )

        sqrt_out = (sp_size.dimensions[2] ge 2) ? total( sqrt_out^2, 3 ) : sqrt_out^2
        ;sqrt_out = per_detector ? sqrt( sqrt_out ) / num_rm :  sqrt( sqrt_out )
        sqrt_out = per_detector ? f_div(sqrt( sqrt_out ) , nwt) :  sqrt( sqrt_out )  ;28-sep-09, ras

    ENDIF

ENDIF

;If Units are flux, and sum_flag is set, average appropriately

RETURN, sp

END
;---------------------------------------------------------------------------
;
;Pro hsi_spectrum::control2binning_prehook, _extra=_extra

;end

;---------------------------------------------------------------------------

PRO HSI_Spectrum::Control2Binning, $
		SP_TIME_INTERVAL = sp_time_interval, $
		OBS_TIME_INTERVAL= obs_time_interval,$
       _EXTRA=_extra

If Keyword_set(_extra) then Self->Set, _extra=_extra
;Self->control2binning_prehook, _extra=_extra



; this procedure generates the control structure "binning" passed to
; hsi_eventlist_to_spectrogram. It is dependent on the sp_xxx
; parameters

chan_bin = self->Get( /SP_CHAN_BINNING )
IF chan_bin NE 0 THEN BEGIN
    binning = Add_Tag( binning, chan_bin, 'CHANNEL_BIN' )
    chan_min = self->Get( /SP_CHAN_MIN )
    IF chan_min GT 0 THEN BEGIN
        binning = Add_Tag( binning, chan_min, 'CHANNEL_MIN' )
    ENDIF
    chan_max = self->Get( /SP_CHAN_MAX )
    IF chan_max GT 0 THEN BEGIN
        binning = Add_Tag( binning, chan_max, 'CHANNEL_MAX' )
    ENDIF
ENDIF ELSE BEGIN

     sp_energy_binning = self->Get( /SP_ENERGY_BINNING )
     channel_cal_new = sp_energy_binning
    ;Revised by RAS, 23-sep-2009, to support high energy photon bins above 20 MeV
    ;Count bins are clipped there, but keep sp_energy_binning unchanged to pass that to SRM object in self->eff()
    IF N_Elements( channel_cal_new ) EQ 1 THEN $
        HSI_RD_CT_Edges, long(channel_cal_new), channel_cal_new
    ;terminate channel_cal_new bins at 20 MeV
    if last_item(channel_cal_new) gt 2e4 then begin
     smax = strtrim(last_item(channel_cal_new),2)

     if not same_data(sp_energy_binning, *self.last_sp_energy_binning) then $
     message,/info, 'Count bins terminated at 20 MeV. SRM photon bins will be extended to '+smax+' keV'
     endif
    channel_cal_new  =      get_uniq((get_edges(channel_cal_new,/edges_1)>1.0001)<2.e4)

    binning = Add_Tag( binning, channel_cal_new, 'CHANNEL_CAL_NEW' )
    *(self.last_sp_energy_binning) = sp_energy_binning
ENDELSE
obs_time_set = 0
if keyword_set(obs_time_interval) then begin
	Self->Framework::Set,OBS_TIME_INTERVAL= anytim(obs_time_interval);,time_range=fltarr(2)
	obs_time_set = 1
	endif
if exist(SP_TIME_INTERVAL) then begin
	sp_time_interval = n_elements(sp_time_interval) eq 1 ? sp_time_interval : $
		anytim(sp_time_interval)
	Self->Framework::Set, SP_TIME_INTERVAL = sp_time_interval
	endif else sp_time_interval = self->Get( /SP_TIME_INTERVAL )
;If SP_USE_TIME_UNIT  is set, then SP_TIME_INTERVAL is in TIME_UNIT units.
bin_time_unit    = self->Get( /SP_BIN_TIME_UNIT )
time_unit        = self->Get( /TIME_UNIT )
sec2time_unit = bin_time_unit ? time_unit : time_unit / 2d^20

IF N_Elements( sp_time_interval ) EQ 1 THEN BEGIN

    IF sp_time_interval EQ 0 THEN BEGIN
; if sp_time_interval is 0, we assume that the user wants a single
; time bin.
        absol = self->Get( /ABSOLUTE_TIME_RANGE )
        sp_time_interval =  absol[1] - absol[0]
        sec2time_unit = time_unit / 2d^20 ;since sp_time_interval is in sec, this must \
        ;be in these units for binning.time_bin to make sense.
    ENDIF
    ;Change from long to long64 to prevent overruns at 2049 seconds
    binning = Add_Tag( binning, Long64( sp_time_interval / sec2time_unit ), 'time_bin' )
    ;binning = Add_Tag( binning, Long( sp_time_interval / sec2time_unit ), 'time_bin' )

ENDIF ELSE BEGIN
                                ;   Release 5.1, richard.schwartz@gsfc.nasa.gov 14-feb-2001
                                ;           fixed bug in sp_time_interval to binning choices

    time_range = fltarr(2) ;added 18-nov-2005. if sp_time_interval has multiple times it supercedes time_range
    times = anytim( sp_time_interval )
    mmtimes = minmax( times )


    obs_time = self->Get(/obs_time_interval )
    ;Are the times outside of the obs_time interval, if so adjust obs_time
    if mmtimes[1] lt 7.2e8 then $;relative to obs_time
    mmtimes = mmtimes + obs_time[0]
    ;If new times outside of earlier obs_time, they become the new obs_time
    ;ras, 15-mar-2006
    test  = value_locate(obs_time, mmtimes)
    def_spt = (hsi_spectrum_control()).sp_time_interval
  case 1 of
		test[0] eq 0 and test[1] eq 0: ;do nothing, both mmtimes inside obs_time
		total(abs(test)) eq 1: begin ;mmtimes straddles obs_time so clip mmtimes
		;or extend obs_time
		if obs_time_set then begin
		;can't change obs_time_interval, must change sp_time_interval
			times = times > obs_time[0] < obs_time[1]
			gd    = where( times[1,*] - times[0,*] gt 1e-3, ngd)
			if ngd ge 1 then times = times[*,gd] else times = def_spt
			Self->Framework::Set,sp_time_interval = times
			endif else $
			Self->Framework::Set,obs_time_interval = minmax([obs_time,  mmtimes])
		end
		product(abs(test)) eq 1:begin
		if obs_time_set then $
		;can't change obs_time_interval, must change sp_time_interval

			Self->Framework::Set,sp_time_interval = def_spt $
			else $
			Self->Framework::Set,obs_time_interval= mmtimes
		end
		else: stop ;message,/continue,'Error, should not reach this branch'

		endcase



;    if (total(abs(obs_time_interval-obs_time)) gt .01) $
;         then self->Framework::Set, obs_time_interval=obs_time_interval
    abs_time = self->Get( /absolute_TIME_range )
    obs_time = self->Get(/obs_time_interval )

;   18-nov-2005, multiple time intervals define the time_range, time_range set to [0.,0.]
;   so it doesn't matter.
;
;    if mmtimes[0] ge obs_time[0] and mmtimes[1] le obs_time[1] and $
;         (total(abs(mmtimes-obs_time)) gt .01) then $
;        self->set, time_range= mmtimes - obs_time[0]

    binning = Add_Tag( binning, times, 'new_time_cal' )
    ;ENDIF ELSE BEGIN
    ;    binning = Add_Tag( binning, times, 'time_group')
    ;ENDELSE
ENDELSE

self->HSI_Spectrogram::Set, BINNING = binning, /NO_UPDATE

END

;---------------------------------------------------------------------------
;
PRO HSI_Spectrum::Process_Hook_Post, $
hsi_spectrogram, pha_total_vs_time, $
_extra=_extra ;ras - 7/23/2001

Self.Pileup->reset
;Self->Set, pileup_state = 0 ;Indicates no pileup correction done yet, 12-aug-03

Self->Hsi_Spectrogram::Process_Hook_post, $
hsi_spectrogram, pha_total_vs_time,$
_extra = _extra

dims = Self->Get(/dim_spectrogram,/info)
a2d_index_mask = Self->Get(/a2d_index_mask)
;High gain segments, 0-2.5 MeV
whigh= where( a2d_index_mask[0:17], nhigh)
;Low gain a2d's up to 15 MeV
wlow  = where( a2d_index_mask[18:*], nlow)

dim_spectrum = dims

its_sp_chan_binning = Self->Get(/SP_CHAN_BINNING)
;dims    = long(self->Get( /INFO, /DIM_SPECTROGRAM)) ; Size( srm, /dim )

; for now livetime is uniform across E
; Get and clear the saved livetime aray
  livetime = Self->Get(/livetime_arr) ;should be dimensioned dims[1]*nsegment

  Self->Set, livetime_arr=0
  If Not Self->Get(/use_total_count) then Self->Set, total_count=0
  sp_data_ltf = fltarr( 1, dims[1], dims[2], dims[3])



  if nhigh ge 1 then sp_data_ltf[0,*,0:nhigh-1] = $
   n_elements(livetime) eq 1 ? 1. : livetime[0:dims[1]-1, whigh]

  if nlow ge 1 then sp_data_ltf[0,*,nhigh:*] = $
   n_elements(livetime) eq 1 ? 1. :livetime[0:dims[1]-1, wlow+9]

  If Not Its_SP_Chan_binning  then $
    Self->Decim_Correct, sp_data_ltf, dim_spectrum
  ;17-oct-2006, need something for coincidence livetime so for now
  ;we'll use the livetime for the a-coindence counts
  if dim_spectrum[3] eq 2 then sp_data_ltf[*,*,*,1]=sp_data_ltf[*,*,*,0]
  *Self.SP_DATA_LTF = sp_data_ltf


  Self->Set,  $

  SP_DATA_DE = reform( (Self->HSI_Spectrogram::GetAxis( /XAXIS, /WIDTH, /NOPROCESS )), $
  dims[0],1, 1, 1 ), $

  SP_DATA_DT   = float( reform( (Self->HSI_Spectrogram::GetAxis(/YAXIS, /WIDTH,  /NOPROCESS)),$ ;ras - 7/23/2001
   1, dims[1], 1, 1) )

;Does the atten state change during the observation
sp_atten_state = Self->Get(/sp_atten_state) ;put into info during spectrogram process
atten_state0 = (sp_atten_state.state)[0]
energy_2 = Self->hsi_spectrogram::Getaxis( /xaxis, /edges_2 )
energy = get_edges( energy_2, /mean)




;If n_elements(sp_atten_state.state) gt 1 then begin
ut = Self->hsi_spectrogram::getaxis(/yaxis, /edges_2)

If its_sp_chan_binning then begin
;Whoa, we have channels, not energies, so convert to approximate energies.
   interpolates = hsi_get_e_edges()
   energy = interpol( interpolates, findgen(n_elements( interpolates)), energy )

   endif
energy = energy >1.000001 < 2.e4
;kim 4/22/05 - if just one state, move info into interval_atten_state structure
shutter_correction = 1.

if n_elements(sp_atten_state.state) eq 1 then begin
    interval_atten_state = {state:sp_atten_state.state, uncertain:0b}


    endif else begin
    interval_atten_state =  replicate( {state:0b, uncertain:0b}, dims[1])
    w0  = reform( value_locate(  sp_atten_state.time, ut[0,*] ) >0)
    w1  = reform(value_locate(  sp_atten_state.time, ut[1,*] ) >0 )
    interval_atten_state.state = (sp_atten_state.state)[w0] >0
    ;uncertain = where( ((sp_atten_state.state)[w0] ne (sp_atten_state.state)[w1])
    ;
    ;RAS, 29-sep-2004 (kudos to kim tolbert for analyzing this!)
    ;If the interval crosses an atten_state boundary it is UNCERTAIN, this covers
    ;the case of multiple changes going back to the original which was not covered
    ;by the original test above
    uncertain = where(w1 ne w0 or interval_atten_state.state gt 3, nuncertain)
    if nuncertain ge 1 then interval_atten_state[uncertain].uncertain =1

    all_state = interval_atten_state.state
    vstate= where( interval_atten_state.uncertain eq 0, nvstate)
    if nvstate ge 1 then begin
     all_state = all_state[vstate]

     if nvstate gt 1 then begin
        states = get_uniq( all_state)
        correct_this = bytarr(4)
        correct_this[states] = 1b
        ;For all a2d ge 9, shutter_transmission is 1

        shutter_correction = reform( (energy # fltarr(1,9))[*] # fltarr(1, 4), n_elements(energy), 9, 4) + 1.

        for idet=0,8 do for jstate=0, 3 do $
           if correct_this[jstate] then $
           shutter_correction[0, idet, jstate] = hsi_shutter_transmission( energy, idet, jstate, apt_filename)

        sc0 = shutter_correction[*,*, all_state[0]]

         for istate = 0, 3  do $
           if correct_this[istate] then $
            shutter_correction[0, 0, istate] = f_div( shutter_correction[*,*, istate],sc0 )
        endif
        endif
    endelse
    Self->Set, shutter_correction=shutter_correction, $
        interval_atten_state = interval_atten_state


    ;states[0] should be the first atten_state

;Protect against energy bins too narrow for high energy pha channels, a2d ge 18.
;28-aug-03 protection no longer needed.
If Not its_sp_chan_binning then begin
    select_2750 = where( energy_2[1,*] lt 2750., n2750)
    If N2750 ge 1 then hsi_spectrogram[select_2750,*,18:*] = 0.0
;    select_above= where( energy_2[0,*] ge 2750., nabove)
;    its_lt_minwidth = 0
;    if nabove ge 1 then begin
;    its_lt_minwidth = min( get_edges( energy_2[*,select_above], /width)) lt 2.0995
;    if its_lt_minwidth then begin
;       message,/continue, 'Energy bins too narrow (lt 2.1 keV) for high energy A2Ds. Setting counts to 0.'
;       hsi_spectrogram[*,*,18:*] = 0.0
;       endif
;       endif
    ;If (N2750 ge 1  ) or its_lt_minwidth then
    self->Framework::SetData, hsi_spectrogram
    endif


END

;---------------------------------------------------------------------------
;---------------------------------------------------------------------------

PRO Hsi_Spectrum::Process_Hook_Pre, $
       _EXTRA=_extra
Self->Set, energy_band = fltarr(2) ;always get all energies.
Self->hsi_spectrogram::process_hook_pre, _EXTRA=_EXTRA
END
;---------------------------------------------------------------------------
Function HSI_SPECTRUM::GET_ARF

;Build vector response for each detector and channel
;Include deviations from Spectroscopy Response Matrix which are diagonal
;and vary in time.

Shutter_correction = Self->Get(/SHUTTER_CORRECTION)
;If there aren't multiple shutter states then arf is 1.0
If N_elements( shutter_correction ) eq 1 then Return, 1.0
atten_state = Self->Get(/INTERVAL_ATTEN_STATE)
all_state = atten_state.state
vstate= where( atten_state.uncertain eq 0, nvstate)
atten_state0 = all_state[vstate[0]]
dim_start = long(self->Get( /INFO, /DIM_SPECTROGRAM )) ; Size( srm, /dim )

nx   = dim_start[0]>1
ny   = dim_start[1]>1
nz   = dim_start[2]>1
nc   = dim_start[3]>1

arf = 1.0
if n_elements(where(atten_state.state le 3)) gt 1 then begin
  hatten = histogram( atten_state.state, min=0, max=3, rev=rev)
  watten = where( hatten ge 1, natten)

  if natten ge 2 then begin
    arf = fltarr( nx, ny, 9 )+1.0
    for i=0, natten-1 do begin
    istate = watten[i]
    if istate  ne atten_state0   then begin
    select = rev[rev[istate]:rev[istate +1]-1]
    arf[*,   select, * ] = rebin( reform( shutter_correction[*, *, istate], nx, 1, 9), nx, hatten[istate], 9)
    endif
    endfor
    endif
  endif

return, arf
end


;PRO HSI_Spectrum::Process_Hook_Post, dims

; for now livetime is uniform across E

; why this?
;SP_DATA_LTF = fltarr( 1, dims[1], dims[2], dims[3]) +1.0
;sp_data_de = reform( Self->HSI_Spectrogram::GetAxis( /XAXIS, /WIDTH), dims[0], 1, 1, 1 )
;yaxis=Self->HSI_Spectrogram::GetAxis( /YAXIS, /WIDTH)
;SP_DATA_DT = reform( yaxis, 1, Size(yaxis ), 1, 1) ;;;;

;self->HSI_Spectrogram::Set, $
;    SP_DATA_INFO={sp_data_de: sp_data_de, $
;                  sp_data_dt: sp_data_dt, $
;                  sp_data_ltf: sp_data_ltf }

;END

;---------------------------------------------------------------------------
;This is a private method. Do not call from outside.
; 22-mar-02, ras, after change in hessi_build_srm,/diag
; on 14-mar-02, this was changed to remain consistent
; 29-mar-2007, changed call to new srm method, get_diagonal()
Function HSI_Spectrum::Eff, $
_EXTRA=_extra

Self->Set, _Extra=_extra



dims    = long(self->Get( /INFO, /DIM_SPECTROGRAM)) ; Size( srm, /dim )

a2d_index_mask = self->Get( /A2D_INDEX_MASK )
a2d_index_list = Where( a2d_index_mask, n_a2d )

use_segment = bytarr(18)

sel_a2d_lo = where( a2d_index_list le 17, nlo)

if nlo ge 1 then $
use_segment[sel_a2d_lo] = 1

sel_a2d_hi = where( a2d_index_list gt 17, nhi)

if nhi ge 1 then $
use_segment[sel_a2d_hi] = 1

nseg = long( total( use_segment))


;binning = Self->Get(/INFO,/THIS_CLASS_ONLY, /BINNING)
;Get the energy binning, resolve first channel issue, correct for
;channel overlap or missing.
channel_binning = Self->Chan_prep()
newcal = float(channel_binning.newcal)
regroup = channel_binning.regroup
remove_first_chan = keyword_set( channel_binning.addzero )
newcal = newcal[remove_first_chan:*]

;Retrieve the HSI_SRM object\
srm_xyoffset = Self->getxyoffset()
abs_time = Self->Get(/absolute_time)

Self->Set, used_xyoffset=srm_xyoffset, /no_update
OSRM = Self->Get(/obj, class_name = 'HSI_SRM' )
;Save the current control parameters
;simplify = OSRM->Get(/SIMPLIFY, /THIS_CLASS )

interval_atten_state = Self->Get(/interval_atten_state) ;put into info during spectrogram process
valid = where( interval_atten_state.uncertain eq 0, nvalid)
atten_state0 = nvalid ge 1 ? interval_atten_state[valid[0]].state :0

;If Self->Get(/SP_SEMI_CALIBRATED) EQ 0 then return, 1.0 else $
  OSRM->Set, CT_EDGES = Self->Get(/sp_energy_binning), $

  ;USE_SEGMENT=byte(use_segment),$
  spectrum_obj=self, $
  srm_gain_time = avg(Self->Get(/absolute_time)),$
  srm_xyoffset = srm_xyoffset, $
  ATTEN_STATE= atten_state0, $
  _EXTRA = _extra

;help, 'NEED_update FOR srm IN eff', OSRM->Need_Update()

if Self->Get(/SP_SEMI_CALIBRATED) EQ 0 then return, 1.0

keVs = Self->HSI_SPECTROGRAM::GetAxis(/XAXIS, /width)
keVs = keVs + fltarr(n_elements(keVs))
srmdiag = OSRM->Get_diagonal() ;Changed 29-mar-2007
;OSRM->Set,_extra = rem_tag(control,['ATTEN_STATE','OFF_AXIS']) ;reset the controls except for off_axis and atten_state
;srmdiag1= srmdiag
;OSRM->Set, SIMPLIFY=simplify ;restore simplify after creating diagonal response
;After 14-mar-02 change to hessi_build_srm, srmdiag should not be multiplied
;by the energy widths.
;srmdiag = srmdiag * rebin( keVs, n_elements(keVs), total( use_segment>0<1 ))
if keyword_set( regroup ) then begin
      nchan2 = n_elements( regroup ) / 2
      out = fltarr( nchan2, nseg )
      outkev = kevs
      one_chan_bin = regroup[0,*]-regroup[1,*]
      ungroup_kevs = (newcal[1:*]-newcal) # ( lonarr(nseg)+1)
      for i =0L,nchan2-1 do $
      if one_chan_bin[i] ne 0 then begin
       outkev[i] = total( ungroup_kevs[regroup[0,i]:regroup[1,i],0])
       out[i,*] = f_div( total( ungroup_kevs[regroup[0,i]:regroup[1,i],*]*$
        srmdiag[regroup[0,i]:regroup[1,i],*],1), $
        total( ungroup_kevs[regroup[0,i]:regroup[1,i],*], 1) )
       endif else begin
       out[i,*] = srmdiag[regroup[0,i]]
       endelse
       kevs = outkev
       srmdiag = out
       endif

list = Where( srmdiag LT 1E-10, ct )
IF ct GT 0 THEN  srmdiag[list] = 0
data_eff = fltarr(dims[0],1,dims[2])  ;no provision for coincidence yet
;only selected segments appear in srmdiag, bug fix, ras, 3-nov-2010
smsel = where(osrm->get(/use_segment))

if nlo ge 1 then $
data_eff[*, 0, (sel_a2d_lo)[smsel]] = srmdiag

;if nhi ge 1 then $
;data_eff[*, 0, sel_a2d_hi] = srmdiag[*, where_arr(a2d_index_list, sel_a2d_hi-9) ]

*SELF.SP_EFF_INFO = data_eff
return, data_eff

END

;---------------------------------------------------------------------------

;PRO HSI_Spectrum::Plot, XTITLE = xtitle, YTITLE=ytitle, $
;       NO_XLOG=no_xlog, NO_YLOG=no_ylog, _EXTRA=_extra

;sp = self->GetData( _EXTRA=_extra, /THIS_SUM )
;AXIS = self->getdata( /XAXIS )

;CHecKVar, xtitle, 'Energy [keV]'
;CheckVar, ytitle, 'Counts / keV'

; stop
;IF NOT Keyword_Set( NO_YLOG ) THEN BEGIN
;    sp =  sp  > 1
;    ylog =  1
;ENDIF ELSE ylog = 0
;IF NOT Keyword_Set( NO_XLOG ) THEN BEGIN
;    axis =  axis > 1
;    xlog =  1
;ENDIF ELSE xlog =  0
;
;PLOT, AXIS, sp, PSYM=10, XTITLE=xtitle, YTITLE=ytitle, $
;    YLOG=ylog , /YNO, XLOG=xlog, XS=3, YS=3

;END

;---------------------------------------------------------------------------

PRO HSI_Spectrum::Write, _EXTRA = _extra

flux = self->GetData( _EXTRA=_extra )

size_flux = Size( flux )
other_flux = Fltarr( size_flux[1], size_flux[3], size_flux[2] )

FOR i=0, size_flux[2]-1 DO BEGIN
    other_flux[ *, *, i ] = flux[*, i, *]
    ENDFOR

time_range =  self->Get( /TIME_RANGE )
time_duration = float(time_range[1] - time_range[0])

date_obs = self->Get( /UT_REF )

new_time_cal = self->Get( /NEW_TIME_CAL )
channel_cal_new = self->Get( /CHANNEL_CAL_NEW )

Spectra2FITS, new_time_cal, other_flux, date_obs, EDGES=channel_cal_new

END

;---------------------------------------------------------------------------

PRO HSI_Spectrum__Define

; this is the data structure residing in self.data:
self = { HSI_Spectrum, $
  noupdate: 0B, $
  artifact_mask: obj_new(), $
  pileup: obj_new(), $
  sp_data_ltf: ptr_new(),$
  sp_eff_info: ptr_new(),$
  counts_2_unit: ptr_new(),$
  last_sp_energy_binning:ptr_new(), $
  INHERITS HSI_XYOFFSET, $
  INHERITS HSI_Spectrogram }

END


;---------------------------------------------------------------------------
; End of 'hsi_spectrum__define.pro'.
;---------------------------------------------------------------------------
