;+
; Name: hsi_image_profile_cstat
;
; Purpose: Compare profiles from generated image with observed profiles and
;   return goodness of fit and regression coefficients.
;
; Input:
; obj - hsi_image, hsi_image_alg or hsi_modul_profile object.  Note:  When called internally while
;  making an image, for vis algs, input obj can't be the alg object (e.g. hsi_vis_fwdfit, which
;  inherits hsi_image_alg) because it doesn't have access to everything.  So for vis algs, pass
;  in its caller which is an hsi_image_single obj.  This is irrelevant when called directly by user,
;  who will pass in hsi_image obj.
;
; Input Keywords:
; vimage - image to get profile for in annsec coords (unless mpat_coord is set to 'cart', then vimage should 
;   be in cartesian coords, i.e. should match modpat coords).  If obj is an hsi_modul_profile object
;   then, vimage must be supplied
; n_mc - number of monte carlo iterations (NOTE:  Can be controlled by env. var. n_mc - temporary!)
; in_seed - seed to start random number sequence
;
; Output Keywords:
; scaled_vimage - output. Scaled image used to get profiles
; xp - array of pointers to image profiles
; status - 0/1 means failed/succeeded to get image profiles
; resid_removed - 0/1 means residuals were not / were removed (only for CLEAN images) 
;
; Output:
;  Structure containing:
;    tot_cstat - overall goodness of fit for image
;    cstat - goodness of fit for each detector used
;    coeff - linear regression constant and slope and correlation coefficient
;      (applied to expected to get observed)
;    mc_tot_cstat - c_stat at peak of monte carlo distr. for all detectors
;    mc_cstat - c-stat at peak of monte carlo distr. for each detector
;    mc_tot_fwhm - FWHM of monte carlo distr. for all detectors
;    mc_fwhm - FWHM of monte carlo distr. for each detector
;
; Written: Kim Tolbert April 2010
; Modifications:
;	30-jul-2010 RAS, change to counts/sec fit, makes more sense for the constant,
;	18-aug-2010, Kim. Added Monte Carlo runs to determine probability of c-stat values
;	  Temporary - control number of iterations and monte carlo plotting by env. var.
;	  setenv, 'n_mc=500'
;	  setenv, 'do_mc_plots=1'  ; set to 0 to disable plots
;	23-Aug-2010, Kim.  For vis algs, divide expected profile by vis_corr_factors for each det.
;	26-Aug-2010, Kim.  For change of 23-aug, should have added a check that vis_normalize was set.
;	  Also, now for vis algs, we pass in self.caller, so remove call to getcaller in 'if vis alg' test.
;	28-Feb-2011, Kim. Free xp at end if not passed out
; 15-Sep-2011, Kim,  correct typo: results=0 to result=0
; 11-Sep-2012, Kim. For image cubes with vis alg, only print error message once (added eb_index,
;   tb_index keywords so we can check if we're on first image)
; 27-sep-2012, ras, moved test block for image cube and vis_alg
; 07-Jun-2017, Kim. When input is Vis FITS file, can't compare obs and expected, abort (previously only
;  aborted if doing image cube with vis alg)
; 21-nov-2017, R Schwartz,  corrfac now divides the profile computed from the image
;       so that smaller relative count rates will be divided by corrfac gt 1 to better match!
; 25-Jul-2018, Kim. Call hsi_image_get_vimage with no_resid if requested, and get resid_removed returned

;-

function hsi_image_profile_cstat, obj, vimage=vimage_in, scaled_vimage=scaled_vimage, $
  xp=xp, status=status, quiet=quiet, _extra=_extra, eb_index=eb_index, tb_index=tb_index, $
  n_mc=n_mc, seed=in_seed, resid_removed=resid_removed

  checkvar, eb_index, 0
  checkvar, tb_index, 0

  ; seed is saved in common so that subsequent runs keep using random numbers along sequence.
  ; Otherwise it repeats.  But if user passes in seed, will use that, so sequence is
  ; repeatable.
  common save_seed, seed
  if keyword_set(in_seed) then seed = in_seed
  self = obj
  n_mc = self -> get(/mc_ntrials)
  mc_show_plot = self -> get(/mc_show_plot)
  ;default, n_mc, 0
  ;if chklog('n_mc') ne '' then n_mc = fix(chklog('n_mc'))
  mc = n_mc gt 0   ; mc=1 means do monte_carlo calculations
  status = 0
  ;help,n_mc
  ; bad_struct is struct to return if we fail.  All values are -1.
  bad_struct = {tot_cstat: -1., cstat: fltarr(9)-1., coeff: fltarr(3,9)-1.}
  ; If vis alg, and image cube, then calib eventlist is not current (makes all visibilities
  ; from all cbe's first, then makes images), so can't compare profiles, so return -1s.

  alg_struct = hsi_algorithm_units(obj, /used, status=status)
  ;if alg_struct.is_vis and is_class(self, 'hsi_image_alg') then self = self->getcaller()
  if alg_struct.is_vis then begin
    case 1 of
      self->get(/vis_input_fits) ne '': msg_out = 'Can not compare obs and expected profiles when input is visibility FITS file. '
      n_elements(self->get(/im_time_interval))/2. gt 1 || n_elements(self->get(/im_energy_binning))/2. gt 1: msg_out = $
        'Image cube with visibility algorithms can not compare obs and expected profiles. '
      else: msg_out = ''
    endcase

    if msg_out ne '' then begin
      ; just print this on first image (tb_index+eb_index eq 0) so don't get too many messages
      if ~keyword_set(quiet) && tb_index+eb_index eq 0 then $
        message, msg_out + 'Setting C-stat and fit parameters to -1', /cont
      status = 0
      return, bad_struct
    endif

    ;  if is_class(self, 'hsi_image_alg') then self = self->getcaller()
  endif

  ; If CLEAN image, and regress method is 'full_resid', and clean_profile_no_resid is set, then remove residuals from image
  ; RESID_REMOVED returned by hsi_image_get_vimage will be set if non-zero residuals were actually removed
  regress_combine = self->get(/clean_regress_combine)
  no_resid = alg_struct.name eq 'Clean' && $
    (regress_combine eq 'full_resid' or regress_combine eq 'disable') && $
    self->get(/clean_profile_no_resid) eq 1
  vimage = hsi_image_get_vimage(self, vimage=vimage_in, no_resid=no_resid, resid_removed=resid_removed)

  if vimage[0] eq -1 then return, bad_struct

  scale = hsi_alg_scale(self, status=status)
  scaled_vimage = vimage * scale


  xp = self -> getdata(class='hsi_modul_profile', vimage=scaled_vimage)
  corr = fltarr(9) + 1.0

  corr =  alg_struct.is_vis  ? self->get(/ vis_corr_factors) : self->get(/ cbe_corr_factors)
  for i=0,8 do $
    if ptr_valid(xp[i]) then *xp[i] = *xp[i] / corr[i]

  ;  if alg_struct.is_vis  && (self->get(/vis_normalize) eq 1) then begin
  ;    for i=0,8 do begin
  ;      corr = self->get(/ vis_corr_factors)
  ;      if ptr_valid(xp[i]) then *xp[i] = *xp[i] / corr[i]
  ;    endfor
  ;  endif

  cbe = self -> getdata(class='hsi_calib_eventlist')

  ind = where(ptr_valid(cbe), ndet)

  tot_cstat = 0.
  cstat = fltarr(9)
  coeff = fltarr(3,9)
  mc_det = fltarr(2,9)
  mc_tot = fltarr(2)
  if mc_show_plot and get_caller() eq 'HSI_IMAGE_PROFILE_PLOT' then p = plotman(/nopanel)

  ; Calculate c-statistic and coefficients of linear fit of expected to observed
  for i = 0,ndet-1 do begin
    ii = ind[i]
    ; only operate on non-zero livetime elements
    good = where( (*cbe[ii]).livetime gt 0., ngood)
    cstat[ii] = 0. & result = 0. & const = 0. &  corrii = 0.
    if ngood gt 2 then begin
      cg = (*cbe[ii])[good]
      cstat[ii] = hsi_pixon_gof_calc(cg.count, (*xp[ii])[good], /poisson) / ngood
      ; compute peak and fwhm of monte carlo distribution,
      ; first index is peak, fwhm, second index is detector
      if mc then $
        mc_det[*,ii] = hsi_image_mc_cstat( (*xp[ii])[good], seed=seed, n_mc=n_mc, plotman=p, det=ii+1 )

      ;result = regress( (*xp[ii])[good], cg.count, corr=corrii, yfit=yfit,const=const)
      ;change to counts/sec fit, makes more sense for the constant, ras, 30-jul-2010
      result = regress( (*xp[ii])[good]/cg.livetime, cg.count/cg.livetime, corr=corrii, yfit=yfit,const=const)
    endif
    coeff[*,ii] = [const, result[0], corrii]
  endfor

  cbeall = ptr_concat(cbe)
  xpall = ptr_concat(xp)
  good = where (cbeall.livetime gt 0., ngood)
  tot_cstat = 0.
  if ngood gt 2 then begin
    tot_cstat = hsi_pixon_gof_calc(cbeall[good].count, xpall[good], /poisson) / ngood
    if mc then mc_tot = hsi_image_mc_cstat( xpall[good], seed=seed, n_mc=n_mc, plotman=p, det='all' )
  endif

  status = 1
  if ~arg_present(xp) && ptr_chk(xp) then ptr_free, xp
  return, {tot_cstat: tot_cstat, cstat: cstat, coeff: coeff, $
    mc_tot_cstat: mc_tot[0], mc_cstat: reform(mc_det[0,*]), $
    mc_tot_fwhm: mc_tot[1], mc_fwhm: reform(mc_det[1,*]) }
end