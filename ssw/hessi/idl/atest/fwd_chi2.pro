pro fwd_chi2, o, coeff, nx, ny, pix, idet_sig, fobs_pp, mp_pp, chi_, chi, map_ptr = map_ptr, $
  background = backgr_opt, corrfac = corrfac
  ;+
  ; PROJECT:
  ;	HESSI
  ; NAME:
  ;	FWD_CHI2
  ; PURPOSE:
  ;	Calculates confidence criterion of fitting the modulation patterns MP_PP of a model
  ;	(specified by the Gaussian parameters COEFF) to the observed calibrated event
  ;	list FOBS_PP. Instead of using the chi-square criterion (which requires a large
  ;	number of photons per time bin) we use here the C-statistic (Cash 1979, ApJ 228, 939),
  ;	which is valid also for a small numbers of photons per time bin (including zeros).
  ;	http://hesperia.gsfc.nasa.gov/~schmahl/c-statistic/c-statistic.html
  ; CALLING SEQUENCE:
  ;	fwd_chi2, o, coeff, nx, ny, pix, idet_sig, fobs_pp, mp_pp, chi_, chi, map_ptr = map_ptr, background = backgr_opt
  ; INPUTS:
  ;       o	 =  object reference  o  =  hsi_image()
  ;	coeff(n_par, n_gaussians+1)  =  Gaussian coefficients of model, with background COEFF(0, NG)
  ;	nx, ny	 =  number of pixels of model map
  ;	pix	 =  pixel size [arcseconds]
  ;	idet_sig =  number of finest detector with significant modulation [0, ..., 8]
  ;	fobs_pp  =  observed calibrated event list
  ;	corrfac  - array of 9 normalization coefficients, see cbe_normalize for more info
  ; OUTPUTS:
  ;	mp_pp	 =  modulation pattern of 9 detectors
  ;	chi_(9)	 =  C-statistic (analogous to chi-square) of fits to 9 detectors
  ;	chi	 =  average C-statistic of all detectors (with significant modulation)
  ; MODIFICATION HISTORY:
  ;  	1999 Dec  1: Version 1, written
  ;  	2000 Mar 16: Version 2, new objects mp = o->GetData(CLASS_NAME = 'HSI_MODUL_PROFILE', ...)
  ;  	2000 Jun 15: update THIS_AD2 --> THIS_DET
  ;  	2000 Jun 24: R.Schwartz speeds up call for hsi_modul_pattern
  ;  	2003 Jan  9: implement automated fitting of absolute fluxes
  ;  	2003 Jan 23: add background modeling (keyword BACKGROUND)
  ;  	2003 Feb  7: define two background keywords BACKGROUND_MAP and BACKGROUND_DET
  ;   2003 Feb 17, correct sign error in residuals of (fobs-mp)
  ;  	2003 Mar  6, exclude bins with livetime = 0 in Cash statistic
  ;   2017 Nov  12, RAS, passing in corrfac to adjust profile for normalization, removing harm (harmonic)
  ;                 index which is not used

  ; AUTHOR:
  ;       Markus J. Aschwanden, LMSAL, phone = 650-424-4001, e-mail = aschwanden@lmsal.com
  ;-
  chi_	 = fltarr(9)
  fwd_modelmap, coeff, nx, ny, pix, model, background = backgr_opt
  if (min(model)   lt 0) then print, 'WARNING: negative map   : minmax(model) = ', minmax(model)
  if (total(model) lt 0) then print, 'WARNING: negative total : total(model)  = ', total(model)
  time_range       = o->get(/time_range)
  dtmin            = o->get(/time_bin_min)                  ;microseconds
  dt               = o->get(/time_bin_def)*dtmin/2.^20      ;seconds
  dur              = float(time_range(1)-time_range(0)) 	;duration [s]
  background_map	 = backgr_opt(0)
  background_det	 = backgr_opt(1)

  if size(/tname, map_ptr) ne 'POINTER' then map_ptr = o->getdata(class_name = 'hsi_modul_pattern')
  det_index_mask  =  o->get(/det_index_mask)
  mp_pp    =  o->getdata(class = 'hsi_modul_profile', vimage = model)                                  ;time_unit ok

  ndet_sig	 = n_elements(idet_sig)
  for i = 0, ndet_sig-1 do begin
    id	 = idet_sig(i)
    fobs	 = (*fobs_pp[id]).count		;calibrated event list
    live	 = (*fobs_pp[id]).livetime		;livetimes
    q_live	 = avg(live)				;average livetime
    mp	 = *mp_pp[id] * corrfac[id]
    mp_norm = mp
    if (background_det eq 1) then begin
      back_s    =  (total(fobs)-total(mp))/dur        ;difference (mp-obs)  =  nonflare residuals [cts/(s SC)]
      back_det  =  back_s * dt(id) 			;detected background counts per bin [cts/bin]
      back_det  =  back_det * live/q_live		;livetime correction
      mp_norm   =  mp + back_det 			;add background residuals to modulation profile
    endif
    ilive	 = where(live gt 0, nlive)
    if (nlive le 0) then ilive = [0]			;prevents from crashing if nlive = 0
    fwd_statistic, fobs(ilive), mp_norm(ilive), chi_stat, c_stat	;C-statistic (Cash 1979)
    chi_(id) = c_stat
  endfor
  chi	 = avg(chi_(idet_sig))			;average of detectors (with sign modulation)
end
