;+
;Name: hsi_adspubplot
;Purpose: Plot number of RHESSI publications and citations per year for RHESSI web site (refereed (all or metaonly) or 
; non-refereed separately). Makes two PNG files, large and thumbnail. (metaonly means pubs where HESSI was found in 
; title or abstract, not just anywhere in text).
;
;Method: Uses .html file containing list of publications to get number of pubs each year, then uses .csv file 
; for number of citations per year (citations to all previous articles in that year) (in html, the number of 
; citations is only for that year).  See adspublist2html.pro header for info on making the .html and .csv files.
; If png files already exist in final dir, first copy them to files named with currrent date and time. 
; 
;Keywords:
; ref = 0/1 for non-ref, ref pubs
; meta = 0/1 for metaonly files (only for ref)
; copy = 0/1 for don't /do copy plot file to /var/www/html/rhessi3/news-and-resources/publications/
; 
;Written: Kim Tolbert, Dec 2014
;
;-

pro hsi_adspubplot, ref=ref, meta=meta, copy=copy, getnums=getnums, year=year,npub_year=npub_year, tot_cit_yr=tot_cit_yr, tot_cit_num=tot_cit

checkvar, copy, 0
checkvar, getnums, 0
checkvar, ref, 0
checkvar, meta, 0

dir = '/var/www/html/rhessi3/news-and-resources/publications/
name = ref ? 'ref_pubs' : 'non_ref_pubs'
if meta then name = name + '_metaonly'
infile = dir + name + '_list.html'
metricsfile = dir + name + '_metrics.csv'
pngfile = name + '.png'
pngthumbfile = name + '_thumb.png'
type = ref ? 'Refereed' : 'Non-Refereed'

a = rd_ascii(infile)
na = n_elements(a)
qyear = where(stregex(a,'<h1.*-.*'+type+' Publications.*') ne -1, nyear)

yr_num = str2cols(stregex(a[qyear], '[0-9]+ - [0-9]+', /extract), ' ')
year = reform(fix(yr_num[0,*]))
npub_year = reform(fix(yr_num[2,*]))

qyear = [qyear, na-1]
ncit_year = intarr(nyear)
for ii=0,nyear-1 do begin
  ayear = a[qyear[ii]:qyear[ii+1]]
  cit_lines = stregex(ayear,'[0-9]+ citations', /extract)
  ncit = stregex(cit_lines, '[0-9]*', /extract)
  ncit_year[ii] = total(fix(ncit))  
endfor

metrics = rd_tfile(metricsfile, /autocol, delim=',', /hskip)
tot_cit_yr = fix(reform(metrics[0,*]))
index = ref ? 1 : 2  ; second column for ref pubs,, 3rd column for non-ref pubs
tot_cit = fix(reform(metrics[index,*]))

if getnums then return

s = sort(year)

xrange=ref ? [1998, max(year)+1] : [1990, max(year)+1]

b1 = barplot(year[s], npub_year[s], fill_color='tomato', xtitle='Year', index=0, nbars=2, name='Publications', $
  xrange=xrange, ystyle=0, title='RHESSI '+type+' Publications', yminor=1, yticklen=1., margin=[.08,.11,.1,.08])
b1.axes[0].tickdir=1
b1.axes[0].ticklen=.03
for ii=1,3,2 do b1.axes[ii].gridstyle=1
for ii=1,3,2 do b1.axes[ii].subgridstyle=-1

b2 = barplot(tot_cit_yr, tot_cit, fill_color='cornflower', index=1, nbars=2, /current, name='Citations', $
  xrange=xrange, ystyle=0, yminor=0, ymajor=0, xminor=0, xmajor=0, position=b1.position)

;b3 = barplot(year[s], ncit_year[s], fill_color='wheat', index=2, nbars=3, /current, /overplot, name='Citations this Year', $
;    xrange=xrange, ystyle=0, yminor=0, ymajor=0, xminor=0, xmajor=0, position=b1.position)
    
leg = legend(target=[b1,b2], position=[.2,.8], /normal, horizontal_alignment=-1, /auto_text_color, shadow=0, linestyle=6)

yaxis=axis('Y', location='right', axis_range=b2.yrange, text_color='cornflower', $
  minor=0, major=-1, ticklen=.02, tickdir=0, target=b2)
  
timestamp = text(.8,.01, strmid(!stime,0,17), /normal, /current, font_size=8, font_color='dim gray')

b1.save, pngfile, resolution=150

b1.save, pngthumbfile, resolution=20

if copy then begin
  outfile = dir+pngfile
  if file_exist(outfile) then file_copy, outfile, str_replace(outfile,'.png', '_' + time2file(!stime) + '.png'), /verbose
  file_copy, pngfile, outfile, /overwrite
  
  outfile = dir+pngthumbfile
  if file_exist(outfile) then file_copy, outfile, str_replace(outfile,'.png', '_' + time2file(!stime) + '.png'), /verbose
  file_copy, pngthumbfile, outfile, /overwrite
endif

end

