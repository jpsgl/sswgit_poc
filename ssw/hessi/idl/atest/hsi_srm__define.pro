;---------------------------------------------------------------------------
; Document name: HSI_SRM__DEFINE
; Created by:   Richard Schwartz 23 April 2001
; Based on hessi_build_srm by David Smith
; Last Modified: 23-april-2001, RAS
;   18-apr-2002 - for summed SRM use avg not total. ras.
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HSI_SRM__DEFINE
;
; PURPOSE:
;
;      This object class manages the HESSI spectrometer response matrix.
;
; CATEGORY:
;       HESSI, SPECTRA
;
; CONTRUCTION:
;       o = Obj_New( 'hsi_srm' )
;       or
;       o = hsi_srm( )
;
; INPUT (CONTROL) PARAMETERS:
;       Defined in {hsi_srm_control}
;
; SEE ALSO:
;       hsi_srm_control__define
;       hsi_srm_control
;       hsi_srm_info__define
;
; HISTORY:
;       23-apr-2001, RAS
;     12-jan-2005, ras, added protection for computation of offax_position
;     14-jul-2005, ras, mods to support changes to hsi_radial_offset
;     SRM_USED_ASPECT_TIME set to -1 for failure in hsi_radial_offset.pro
;     28-sep-2005, ras, error_offax set to default to prevent abort when
;     compute_offax set to 0.
;	  13-mar-2006, ras, patched lambda in from srm_lambda
;	   15-sep-2009, ras,
;		A series of changes to deal with the  new circumstances for the rear segments that support the new
;		responses w/o front segment AC and the photon inputs up to >100 MeV
;
;			1. edges can go to 100 MeV and higher.
;			2. ct_edges stopped at 20 MeV, higher energies used as ph_edges
;			3. Values used for srm in info_ct_edges, info_ph_edges
;			4. Edge parameters used to build edges are ct_edges, and srm_sp_binning/ph_edges
;			5. ph_edges if used would take precedence over srm_sp_binning when ph_edges go to higher
;				energies than ct_edges
;			6. srm_direct can be set and calls the new rear_segment and preferred responses.  These don't
;			have any modifications for anti-coincidence and treat all other segments as passive.
;			7. srm_direct can only be used with rear segments and cannot be used with diagonal responses
;				setting direct to 0 if diagonal is called, direct is only changed locally, srm_direct is unchanged
;				Also, srm_direct is ignored in the process method in the call to hessi_build_srm if any segment
;				is le 8, ie a front segment.
;			8. It is imperative to start to use srm_direct = 1 for the rear segments because with the REAR_NO_ANTI
;				parameter set for the hsi_spectrogram_control disables the front/rear anti-coincidence logic by
;				clearing the coincidence bit on rear segments
;	20-jan-2010, ras, changed getdata to support a new flag, ARTIFACT_MASK.  This is used at high energy
;		to remove small sections in channel space from specific detectors
;   27-dec-2011, ras (default 1) -added INSERT_KEDGE control parameter to add add k edge for Ge, Mo, Tu
;	to ph_edges which forces response to be calculated from the correct side of the edge and not interpolated
;		across the edge
;	ras, 5-jan-2012, add k-edges ('ge','mo','w') for low energy photon input.
;		hessi_build_srm still sees the same ct_edges as ph_edges below the high energy extension
;		but the columns of the srm are summed back to the original ct_edges while the
;		ph_edges keep their enhanced definition by splitting bins at the edge boundaries.
;-
;


;--------------------------------------------------------------------

FUNCTION hsi_srm::INIT, SOURCE = source, $
  INFO=info, $
  CONTROL=control, $
  _EXTRA=_extra



Checkvar, Info, {hsi_srm_info}
Checkvar, Control, hsi_srm_control()

RET=self->Framework::INIT( CONTROL = Control, $
                           INFO=Info, $
                           SOURCE=source, $
                           _EXTRA=_extra )

self->SetData, 1.0 ;Default SRM

RETURN, RET

END

;--------------------------------------------------------------------

PRO hsi_srm::Process, $
             _EXTRA=_extra


If Keyword_set(_Extra) then Self->Set, _Extra=_extra
Self->Validate_Energy_Range, group=group, error=error
if error then begin
	message, /info, 'SRM energy ranges incompatible'
	srm = -1
	goto, getout
	endif
info = Self->Get( /INFO, /this_class_only)
control = Self->Get( /CONTROL, /this_class_only)
ph_edges = info.info_ph_edges	;used as input to hessi_build_srm
ct_edges = info.info_ct_edges	;used with data_grouper_edg to make final srm
srm_ct_edges = info.info_srm_ct_edges ;used as input to hessi_build_srm
use_segment   = control.use_segment
aspect_time   = (anytim(control.srm_aspect_time))
gain_time     = (anytim(control.srm_gain_time))
srm_xyoffset  = control.srm_xyoffset
compute_offax_position = control.compute_offax_position
lambda        = control.srm_lambda
aspect_time = aspect_time[0] ne 0.0 ? aspect_time : gain_time
direct = max(where(use_segment)) gt 8 ? control.srm_direct :0 ;direct (no-ac) only applies to rear segments

if max(abs(avg(aspect_time)-avg(gain_time))) gt 12*3600. then begin
    message,/info,'Max deviation of aspect time from absolute time exceeds 12 hours. Using srm_gain_time.'
    aspect_time = gain_time
    Self->Set, srm_aspect_time = 0
    endif

error_offax = 0 ; ras, added 28-sep-2005, error_offax undefined if hsi_radial_offset not called
offax_position = compute_offax_position ? $
    float( hsi_radial_offset(xyoffset=srm_xyoffset, absolute_time = aspect_time, $
    last_aspect_time = last_aspect_time, $
    error=error_offax)) $
    : control.offax_position

self->Set, srm_used_aspect_time = last_aspect_time
;Check on ERROR_OFFAX added 12 jan 2005
If error_offax then begin
    print,'ERROR IN FINDING OFFAX_POSITION, USING DEFAULT VALUE'
    PRINT,'Try changing srm_aspect_time to a valid aspect interval.'
    offax_position = *(hsi_srm_control()).offax_position
    Self->Set, srm_used_aspect_time = -1
    endif
Self->Set,offax_position = offax_position
simplify = control.simplify

;message,/continue, string(/print,simplify)
diagonal = 0
If (total( (simplify<2) - 2 ) eq 0 ) then   diagonal=1


if keyword_set(diagonal) then direct=0
;help, diagonal, direct
;help, /trace
;Remove from var, those controls passed in directly
var = rem_tag(control,$
['ct_edges','ph_edges','use_segment','srm_aspect_time','offax_position','srm_lambda'])

;z = where(ph_edges le ct_edges )
;z = lindgen(n_elements(ph_edges))
;ct_edges_new = diagonal? ct_edges :get_uniq( [ct_edges, ph_edges[z]] )
;help, ct_edges_new
	hessi_build_srm, $
          srm_ct_edges, $
          offax_position = offax_position, $

          ph_edges =  ph_edges, $
          time_wanted = gain_time[0], $ ;make sure there's only one time
          ;this was a bug introduced on 31-jan-02 and fixed on 1-feb-02
          use_segment,$ ;to be change to use_virds in 7.0
          srm, $
          geom_area, $
          direct=direct, $
		  diagonal=diagonal, $
          lambda  = lambda, $
          _extra = var

;reassemble the srm back to the original ct_edges by summing over columns
;first we have to multiply by width and in the end divide by the width
;srm may have 2 or 3 dimensions so we must be careful using reproduce and reform
;to ensure the shape of ct_wid is correct
if ~diagonal && n_elements(group) gt 1 then begin
	dim = size( srm, /dimen)
	ct_wid = reform(reproduce( get_edges( srm_ct_edges, /width), product(dim[1:*])),/over,dim)
	srm = data_grouper( srm * ct_wid, group)
	dim = size( srm, /dimen)
	ct_wid = reform(reproduce( get_edges( ct_edges, /width), product(dim[1:*])),/over,dim)
	srm = f_div( srm, ct_wid)
	;data_grouper_edg, srm, srm_ct_edges, ct_edges, /perwidth, epsilon=1e-6, $
	;error=grouper_error, emsg=grouper_error_message
	endif
;if grouper_error then begin
;	Self->Setdata, 0
;	message,'Problem binning srm back to original channel edges after calling hessi_build_srm. '+grouper_error_message
;	endif

if Self->Get(/DEBUG) then help, srm
;23-aug-03 at this time we don't let the control parameters
;actually control the output. Perhaps in the future. Here
;we treat the control parameters like info parameters. ras.
srm_input_units = 'photons cm**(-2)'
srm_units = diagonal eq 1 ? 'counts cm**(-2)':'counts cm**(-2) keV**(-1)'


self->Set, geom_area = geom_area, $
  srm_input_units = srm_input_units, $
  srm_units = srm_units
getout:
self->SetData, srm

END

;--------------------------------------------------------------------



FUNCTION hsi_srm::GetData, $
                  ;THIS_SUBSET2=this_subset2, $
                  sum_srm=sum_srm, $
                  ARTIFACT_MASK=artifact_mask, $
                  _EXTRA=_extra

; first we call the predefined GetData in Framework:

self->Set, _extra=_extra

data=self->Framework::GetData(  )
dim = size(/dim, data)
use_segment = (Self->Get(/USE_SEGMENT))[0:17]
smask = where( USE_SEGMENT)
front =  min(smask) le 8 ;cannot use art_mask if any fronts are selected
if keyword_set(artifact_mask) && obj_isa(artifact_mask,'HSI_ARTIFACT_MASK')  &&  ~front  then begin

	ct_edges = self->get(/info_ct_edges)

	gain_time     = self->get(/srm_gain_time)

	data = artifact_mask->getdata(data, edges=ct_edges,$
		   time = gain_time, seg_index_mask=use_segment, ntime=dim[1],ncoinc=1)
	endif

geom_area = Self->get(/geom_area)
if Keyword_Set( Sum_SRM ) and n_elements(geom_area) gt 1 then begin
  data = avg( data, size( data, /n_dim)-1)

  endif

RETURN, data

END

;--------------------------------------------------------------------
FUNCTION hsi_srm::Get_Diagonal, sum_srm=sum_srm, $
	_EXTRA=_extra

;provides a method to get the diagonals apart from
;the normal setup, to facilitate semi_cal in spectrum
;and other such needs.



obj = Self->Get(/diagonal_obj,/info, /this_class)
if not obj_valid(obj) then obj=hsi_srm()

control=Self->Get(/control,/this_class)

control.simplify = control.simplify*0b+2b ;diagonal case
oc = obj->get(/control)
;put in kluge to control update because it doesn't work right
;just passing in control, always wants to update

control = rem_tag(control, 'SRM_UNITS')
oc      = rem_tag(control, 'SRM_UNITS')
obj->set,_extra=control

Self->Set, diagonal_obj = obj
Data = obj->getdata(sum_srm=sum_srm, $
	_EXTRA=_extra)
Self->Set,  offax_position = obj->get(/offax_position)

return, data
end
;--------------------------------------------------------------------


PRO hsi_srm::Set, $
       ALL_SIMPLIFY=ALL_SIMPLIFY, $
       SIMPLIFY = SIMPLIFY, $
       DIAGONAL=DIAGONAL, $
       CT_EDGES=CT_EDGES, $
        ;preserves upper limit of photon energy if ct_edges have been clipped, ras, 23-sep-2009
       PH_EDGES=PH_EDGES, $

       SRM_DIRECT = SRM_DIRECT, $
       SPECTRUM_OBJ= spectrum_obj,$
       DET_INDEX_MASK= det_index_mask, $
       SEG_INDEX_MASK= seg_index_mask, $
       A2D_INDEX_MASK= a2d_index_mask, $
       SEP_SEGS= sep_segs, $

       _EXTRA=_extra



; for all other parameters (included in _extra), just pass them to the
; original Set procedure in Framework


IF Keyword_Set( _EXTRA ) THEN BEGIN
    self->Framework::Set, _EXTRA = _extra
ENDIF

If Exist( SEP_SEGS ) then Self->Set, SEP_VIRDS = (SEP_SEGS>0<1)
If Keyword_set(spectrum_obj) or Keyword_Set( det_index_mask ) or $
Keyword_set( seg_index_mask ) or Keyword_Set( a2d_index_mask) then begin
  If Keyword_set( spectrum_obj ) then $
  if Obj_Class(spectrum_obj) eq 'HSI_SPECTRUM' then begin
    spectrum_obj->Set, DET_INDEX_MASK= det_index_mask, $
       SEG_INDEX_MASK= seg_index_mask, $
       A2D_INDEX_MASK= a2d_index_mask
       a2d = bytarr(27)
       a2d[0] =spectrum_obj->get(/a2d_index_mask)
       seg_a2d = a2d or [bytarr(9),a2d[18:*]]
       seg_det = bytarr(18)
       seg_det[0]= spectrum_obj->get(/det_index_mask) # (intarr(1,2)+1.)
       seg_index_mask = spectrum_obj->get(/seg_index_mask)
       use_segment = bytarr(18)
       use_segment[0] =  (seg_det or seg_index_mask)
       if total(use_segment) gt 0 then use_segment = use_segment and seg_a2d

      endif else begin
      a2d = bytarr(27)
       a2d[0] = fcheck(a2d_index_mask, bytarr(27))
       seg_a2d = a2d or [bytarr(9),a2d[18:*]]
       seg_det = bytarr(18)
       seg_det[0]= fcheck(det_index_mask, bytarr(9))
       seg_index_mask = fcheck( seg_index_mask, bytarr(18))
       use_segment = bytarr(18)
       use_segment[0] = seg_det or  seg_index_mask or seg_a2d

       if total(use_segment) eq 0 then use_segment = Self->get(/use_segment)
       endelse
       Self->Set, use_segment = use_segment
       endif


;ALL_SIMPLIFY has precedence over SIMPLIFY
IF EXIST( DIAGONAL ) THEN ALL_SIMPLIFY = 2
;if simplify is supplied with only 1 value, then it is being used
;like all_simplify
IF N_elements( SIMPLIFY ) EQ 1 THEN ALL_SIMPLIFY = SIMPLIFY
IF EXIST( ALL_SIMPLIFY) THEN $
  simplify= (Self->Get(/SIMPLIFY))*0 + all_simplify[0]


    ; first set the parameter using the original Set
 ;   self->Framework::Set, PARAMETER = parameter

; simplify = Self->Get(/SIMPLIFY, /THIS_CLASS_ONLY) * 0 + long(ALL_SIMPLIFY)>0<3
IF Keyword_Set( SIMPLIFY ) THEN self->Framework::Set, SIMPLIFY=simplify
   ; then take some action that depends on this parameter
  ;  Take_Some_Action, parameter

If KEYWORD_SET(ct_edges) or KEYWORD_SET(ph_edges) $
 or KEYWORD_SET(SRM_DIRECT) or KEYWORD_SET(SIMPLIFY) then begin

	Self->Framework::Set, ct_edges=ct_edges, ph_edges=ph_edges,  srm_direct=srm_direct

	Self->Validate_Energy_Range
	endif
END

pro hsi_srm::Validate_Energy_Range, group=group, error=error,_extra=_extra
;+-
; PROJECT:
;       HESSI
;
; NAME:
;       HSI_SRM__Validate_Energy_Range
;
; PURPOSE:
;
;      This procedure  extends count bins to high energy on the photon
;		side of the srm if necessary.  It also truncates any array at the upper
;		limits for the count spectrum or the photon spectrum.
;		 the maximum energy for
;		the ct_edges_used default to 20 MeV, the ph_edges_used must be less or equal to the
;		max_phedg whose default is also 20 MeV.  In the case of SRM_DIRECT=1, the photon edges
;		can be extended to 150 MeV
; CATEGORY:
;       HESSI, SPECTRA
;
; Keywords:
;		GROUP - the location of the ct_edges in srm_ct_edges.  Used in process with Data_Group to add the
;			separated columns together so they match ct_edges at the end of process
;
; HISTORY:
;		21-sep-2009, ras, added ph_edg, max_phedg, max_ctedg
;		20-JAM-2010, RAS, ADDED support for
;   27-dec-2011, ras (default 1) -added INSERT_KEDGE control parameter to add add k edge for Ge, Mo, W
;	to ph_edges which forces response to be calculated from the correct side of the edge and not interpolated
;		across the edge, using xray_kedge to obtain kedge values
;	15-nov-2012, ras, because we use data_grouper_edg we can now use finer ph_edges than ct_edges
;		hessi_build_srm forces that there only be a high energy extension and identical ct_edges and ph_edges
;		at all energies less than ct_max. So we create a set of ct_edges for hessi_build_srm from the
;		union of uniq [ct_edges_in, ph_edges_in, k_edge] and then using data_grouper_edg sum over the
;		output row groups so that we have ct_edges_in for the channels (bins) of the final srm
;	23-nov-2012, to avoid data_grouper_edg we find the edg boundary relationship between the srm_ct_edges
;		and the ct_edges and put them into the Keyword argument GROUP
;-


; for all other parameters (included in _extra), just pass them to the
; original Set procedure in Framework
error=0
simplify = Self->Get(/simplify)
diagonal = total(abs( (simplify-2) < 0)) eq 0
srm_direct = Self->Get(/srm_direct)
ct_edges = get_edges( Self->Get(/ct_edges), /edges_1)
if keyword_set(ct_edges) then hsi_rd_ct_edges, ct_edges, ct_edges

ph_edges = get_edges( Self->Get(/ph_edges), /edges_1)
input_ph_edges = is_number(ph_edges[0]) && n_elements(ph_edges) eq 1
if input_ph_edges then hsi_rd_ph_edges, ph_edges, ph_edges
;ras, 5-jan-2012, break input spectra at material k-edges



max_ctedg = 2e4
max_phedg = srm_direct eq 1 ? 150000. : 2e4
max_phedg = diagonal ? max_ctedg : max_phedg
;keep ph_edg below max_phedg
ph_edges = ~keyword_set(ph_edges) ? get_uniq(ct_edges<max_phedg) : ph_edges

if self->get(/insert_kedge) and ~diagonal then begin
	Kedge = xray_kedge(['ge','mo','w']) > ph_edges[0] ;only use k-edges above lower photon energy limit
	;	IDL> print, kedge
	;      11.1040      19.9990      69.5240
	;
		;kedge = [11.1, 20.0, 69.53] ;k-edge for Ge, Mo, w
	ph_edges = get_uniq([ph_edges, kedge], epsilon=1e-6)
	endif


;
;Make sure we have valid edges for the pha channels and photon
;energies.  Build a high energy extension if the user hasn't
;specified photon energies.
;
;keep ct_edg below max_ctedg

ct_edges = get_edges(/edges_1,  get_uniq( (ct_edges>1.001)< max_ctedg))
nct = n_elements(ct_edges)
if ~diagonal && ( last_item(ct_edges) eq last_item(ph_edges) ) then begin

	energy_m = last_item( ph_edges )
	;expand the photon energies if they aren't defined on input


		ph_energy_m = (2 * energy_m )< max_phedg
		nchan_p  = (nct / 10 ) > 10
		ph_edges =  [ph_edges, energy_m + $
		              (findgen(nchan_p+1))[1:*] * (ph_energy_m-energy_m)/nchan_p ]
		ph_edges = get_uniq( ph_edges<ph_energy_m)
	endif

sel = where(ph_edges lt last_item(ct_edges),  nsel)

;any 'identical' ph_edges before combining
;
ph_edges_2_merge = ph_edges[sel] ;ph_edges lt highest valid ct_edges
z = value_locate( ct_edges, ph_edges_2_merge)
q = where( abs(ph_edges_2_merge-ct_edges[z]) ge (2e-6*ct_edges[z]) and $
	abs(ph_edges_2_merge-ct_edges[z+1]) ge (2e-6*ct_edges[z+1]), nq)

;ph_edges_2_merge = nq ge 1 ? ph_edges_2_merge[q] : -1
;We've removed the ph_edges that are too close, all we have to do is sort and
;find the old ct_edges
if nq ge 1 then begin
	srm_ct_edges = [ct_edges, ph_edges_2_merge[q]]
	srm_ct_edges = srm_ct_edges[ sort( srm_ct_edges) ]
	endif else srm_ct_edges = ct_edges
group = value_locate( srm_ct_edges,  ct_edges)
if diagonal then ph_edges=ct_edges
;Save these as they the actual edges used in hessi_build_srm
Self->Set, info_ct_edges=ct_edges, info_ph_edges=ph_edges, info_srm_ct_edges = srm_ct_edges


end

;---------------------------------------------------------------------------


FUNCTION hsi_srm::Get, $
                  NOT_FOUND=NOT_found, $
                  FOUND=found, $
                  _EXTRA=_extra


RETURN, self->Framework::Get( $   ;PARAMETER = parameter, $
                              NOT_FOUND=not_found, $
                              FOUND=found, _EXTRA=_extra )
END

;---------------------------------------------------------------------------

PRO hsi_srm__Define

self = {hsi_srm, $
        INHERITS Framework }

END


;---------------------------------------------------------------------------
; End of 'hsi_srm__define.pro'.
;---------------------------------------------------------------------------
