;+
; :Description:
;    Selects the state times relevant for the abs_time interval
;    This is the initial state and any included states
;
; :Params:
;    att_ssr - atten or ssr state structure from the eventlist
;    abs_time - time interval of interest
;
;
;
; :Author: raschwar
; 20-nov-2018, Initial. Based on time selections that were embedded in hsi_spectrogram__define
; RAS, 14-dec-2018 older versions of IDL are fussy and require a 1 element vector at least and not a scalar

;-
pro hsi_evl_state_filter_time, att_ssr, abs_time
  z = where( att_ssr.time gt abs_time[0] and att_ssr.time lt abs_time[1], nz)
  arg = att_ssr.time + dblarr( n_elements( att_ssr.time ) )
  ;older versions of IDL are fussy and require a 1 element vector at least and not a scalar
  ;RAS, 14-dec-2018
  iv = value_locate( arg, abs_time )
  
  nssr = n_elements( att_ssr)
  case 1 of
    nz eq 0: begin

      att_ssr = att_ssr[iv[0]>0]
    end
    nz gt 1: begin
      if z[0] gt 1 then begin
        att_ssr = att_ssr[ z[0]-1:last_item(z) ]

      endif
    end
    else: att_ssr = att_ssr ;should never be here
  endcase

  att_ssr[0].time = abs_time[0] ;change the initial state time to the interval's first time
  ;may be unnecessary

end
