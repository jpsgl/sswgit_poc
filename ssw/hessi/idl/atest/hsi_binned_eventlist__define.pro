;---------------------------------------------------------------------------
; Document name: hsi_binned_eventlist__define.pro
; Created by:    Andre, April 29, 1999
; Last Modified: Mon Aug 13 17:13:05 2001 (csillag@sunlight)
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI BINNED EVENTLIST CLASS DEFINITION;
;
; PURPOSE:
;       Provides the algorithme to generate binned eventlists and to
;       access them. Using the control parameters one can select
;       different time binnings for each detector.
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       obj = Obj_New('hsi_binned_eventlist')
;
;       "obj" may be any variable name
;
; (INPUT) CONTROL PARAMETERS:
;       from {hsi_binned_eventlist_control}:
;
;       front_segment: set this to 1 if you want to use the front segments
;       rear_segment: set this to 1 if you want to use the rear segments
;       det_index_mask: BYTARR( 9 ) set the corresponding byte to 1
;                       for those detectors you want to get the
;                       binned eventlist. Note that what this parameter
;                       really do is set the parameter a2d_index_mask
;                       for those a2ds associated with the choice. In
;                       this way, if
;                       the a2ds are already set, it will not redo the
;                       binned eventlist each time the det_index_mask
;                       is changed.
;       time_bin_def: FltArr(9): values that multiplies time_bin_min
;                     to determine the time binnning for each
;               values must be ge 1.
;                     detector. Default: 1,1,2,4,8,8,16,32,64
;       time_bin_MIN: 0L: minimum value of the time binning (which
;                     is then multiplied by time_bin_def). Default is
;                     1024 binary microseconds. Must be ge 1.
;      ;For the binned_eventlist class coincidence_flag and sum_coincidence have
;     this behavior different from the spectrum class.
;      coincidence_flag (byte) : Default is 0.  Accumulate only
;                                anti-coincdent events. If set, then accumulate
;                     anti and coincident events into summed spectra.
;     if coincidence_flag is set, sum_coincidence is forced to be set.
;     if coincidence_flag is changed, then the object will reprocess
;
;
;      sum_coincidence: sum over coincidence condition on getdata.
;      When sum_coincidence is changed to 1, coincidence_flag is also set
;      Changing coincidence_flag to 0 from 1 will not trigger reprocessing
;     of the spectrogram unless forced.  With /COINCIDENCE_FLAG set
;     don't set SP_DATA_STR because it doesn't take care of all cases
;     in writing the structure.

;
; OUTPUTS:
;       n_bin: Lonarr( 9): number of bins for each detector and
;
;       binned_n_event: lonarr(9): Records the total number of
;                       events in each binned eventlist. Dimensioned SC(sub-collimator)
;
; PARENT CLASS:
;       HSI_SPECTROGRAM
;
; METHODS:
;       INIT: Just passes stuff to Framework
;       Set: Deals with the setting of det_index_mask. It checks the
;            values, sets the corresponding a2d_index_mask, using the
;            conversion with hsi_detseg2a2d
;       GetData: does subset selection using then keywords
;                THIS_DET_INDEX
;       Process
;       GETAXIS: Returns pointer array for time axes (/yaxis) or axis for THIS_DET_INDEX
;         Returns energy edges for /XAXIS
;
; KEYWORDS:
;       THIS_DET_INDEX: (GetData) returns the binned
;                       eventlist only for that specific detector
;
; SEE ALSO:
;       hsi_binned_eventlist_control
;       hsi_binned_eventlist_info
;       hsi_eventlist__define
;       http://hessi.ssl.berkeley.edu/software/reference.html#hsi_binned_eventlist
;
; HISTORY:
; 21-apr-2011, added seg_index_mask to set where does nothing but prevent this from getting
;	into spectrogram::set where it would cause problems, ras, 21-apr-2011;
;   28-Feb-2011, kim, ras - in process, free existing cbe pointers.
;	20-apr-2007, harmonics removed. they are integrated into the binned_eventlist
;		by binning at higher cadence, same with calib_eventlist and
;		a similar spirit is in the modulation patterns.
; Release 5.1, richard.schwartz@gsfc, added binned_n_event info param.
;       Release 3 development, August, 1999,
;           initialization using HSI_Binned_Eventlist
;       Release 2 development, April, 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;       Algorithm and fundamental development: April 1999,
;           G.Hurford, E.J.Schmahl and R.A.Schwartz, NASA/GSFC
;   14-dec-2001, energy_band is now a control param of this
;   class which holds the energy bin(s) limits, ras
;   25-mar-02 RAS
;     Added UT_BINNED_EVENTLIST info parameter to hold absolute time axes
;     and GETAXIS method.
; 1-apr-02, ras, added UT and ENERGY to keywords for getaxis
; 3-jun-02, ras, integrate full livetime/dropout merged in livetime_arr
; 25-Oct-2002, Kim.  Added @hsi_insert_catch in getdata
; 10-oct-02, ras, patch around problem in setting energies before
;   obs_time is set.
; 7-nov-2002, ras, force TIME_BIN_DEF ge 1
; 26-nov-2002, ras, turn on rear detector livetime if enabled
; Feb-2004, ras, culling introduced, small datagap fraction bins removed.
;RAS, 17-may-2004
 ;Forced to sum over coincidence if coincidence_flag and sum_coincidence are set
 ;Therefore the independent states are not preserved and
 ;it must be forced to reprocess to regain this information
 ;4-mar-2005, ras, removed unneeded info arrays, livetime_ctr, dropout, livetime_arr
 ;  and ut_binned_eventlist.  These were sometimes quite large.
 ;  The call to getaxis(/ut) will still work even after ut_binned_eventlist is cleared
 ;  because the get(/ut_binned_eventlist) will trigger control2binning where these
 ;  are calculated.  Using /keep_dropout in the call to getdata() will inhibit the destruction
 ;  of those pointers.
 ;6-jun-2005, ras, eliminate memory leak in compute_time_axes and only recompute when necessary
 ; 26-jul-2012, ras, changed all ref to im_energy_binning to sp_energy_binning.  We keep im_energy_binning
 ;	as a control parameter (essentially an info parameter) so higher level objects can still see it
 ;	but when it is changed it is via /no_update. the real binning for this class is thru sp_energy_binning
 ;  added srm object so we can drm4image method
;-
;---------------------------------------------------------------------------

FUNCTION HSI_Binned_Eventlist::Init, $
            SOURCE=source, $
            Info = info, $
            _EXTRA=_extra

CheckVar, info, {hsi_binned_eventlist_info}

ret=self->HSI_Spectrogram::INIT(CONTROL = hsi_binned_eventlist_control(), $
                          INFO= Info, $
                          SOURCE=source,$
                          _EXTRA=_extra )
;added to make a more efficient hessi_drm4image, ras, 18-sep-2012
;obj = obj_new( 'hsi_srm' )
;self->set, source = obj, src_index=1
IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

RETURN, ret

END

;---------------------------------------------------------------------------

PRO HSI_Binned_Eventlist::Set, DET_INDEX_MASK = det_index_mask, $
    A2d_index_mask = a2d_index_mask, $
;    energy_band=energy_band, $
;    eb_index=eb_index, $
    sp_energy_binning=sp_energy_binning, $
    im_energy_binning=im_energy_binning, $

    seg_index_mask = seg_index_mask, $ ;does nothing but prevent this from getting
    ;into spectrogram::set where it would cause problems, ras, 21-apr-2011
        time_bin_def = time_bin_def, time_bin_min=time_bin_min, $
        COINCIDENCE_FLAG = coincidence_flag, $
        SUM_COINCIDENCE  = sum_coincidence, $
        _EXTRA=_extra, NOT_FOUND=NOT_found, DONE=done, fw_set_id=fw_set_id

;Make sure a2d_index_mask doesn't cause an update without a real change

; acs 2007-11-22 fw_set_ids is there to make it faster (doesnt go
; several times in the same class)

IF Keyword_Set( A2D_INDEX_MASK ) then Self->Framework::Set, $
  A2D_INDEX_MASK=Self->Loadvar( a2d_index_mask, /A2D_INDEX_MASK), fw_set_id = fw_set_id


IF Keyword_Set( DET_INDEX_MASK ) THEN BEGIN
	det_index_mask = bytarr(9) + det_index_mask ;in case reading in a file with the old bytarr(9,3)
    a2d_index_mask = HSI_DetSeg2A2d( DET_INDEX_MASK=det_index_mask )
    old_a2d_index_mask = self->Get( /A2D_INDEX_MASK )
    self->Framework::Set, A2D_INDEX_MASK = Byte( a2d_index_mask OR old_a2d_index_mask ) < 1B > 0B, $
        fw_set_id = fw_set_id
    self->Framework::Set, DET_INDEX_MASK = Byte( det_index_mask ) < 1B > 0B, /NO_UPDATE, fw_set_id = fw_set_id
ENDIF

;Kluge to fix energy binning problem.
;Because we haven't separated making the energy bins from the detector calibration  that
;is only needed to accumulate, we have to put in a dummy time if one isn't found
;There won't be an absolute time if obs_time_interval or filename aren't set
;If exist(energy_band) or exist(eb_index) or exist(sp_energy_binning) then begin
;   need_dummy_time = (SIZE(Self->Get(/filename),/tname) ne 'STRING') and $
;   (total(abs(self->Get(/obs_time_interval))) eq 0)
;
;   ;If NEED_DUMMY_TIME then $
;   ; Self->Framework::Set, obs_time_interval = '20-feb-2002 11:'+['06','07']
;   If exist(eb_index ) then self->framework::set, eb_index=eb_index>0, /no_update
;   If exist(energy_band) and not exist( sp_energy_binning) then $
;     sp_energy_binning = energy_band
;
; 8-aug-03, ras - reconciling eb_index, sp_energy_binning, energy_band
; Since sp_energy_binning completely controls energy binning,
; just as sp_energy_binning does for spectrum.  This is all we need to set
; energy_band and eb_index are control parameters of the calib_eventlist
; and are only used to select the desired energy_band
;

if exist( im_energy_binning) then begin
	Self->Framework::Set, im_energy_binning= im_energy_binning, /no_update
	sp_energy_binning = im_energy_binning
	endif
;if n_elements(sp_energy_binning) gt 2 then help, /tra
If exist( sp_energy_binning) then begin
   ;Self->Framework::Set, im_energy_binning = sp_energy_binning, /no_update
   Self->Framework::Set, sp_energy_binning= hsi_energy_edge(sp_energy_binning), fw_set_id = fw_set_id
   Self->Control2binning, /ENERGY_ONLY, fw_set_id = fw_set_id

endif
; 8-aug-03, ras - reconciling eb_index, sp_energy_binning, energy_band

;   If exist( eb_index ) or exist( sp_energy_binning ) then begin
;     if not exist( eb_index ) then eb_index = self->get(/eb_index )
;     energy = Self->GetAxis( /energy, /edges_2)
;     nchan = n_elements( energy ) / 2
;     eb_index = eb_index < (nchan-1)  ;ras, 28-oct-2002, change to (nchan-1) from nchan
;     Self->Framework::Set, eb_index = eb_index, energy_band=energy[*,eb_index], /no_update
;     endif
;
;   endif
;endif
;Prevent the memvis folks from setting time_bin_def to 0, ras, 7-nov-2002
If Keyword_set( TIME_BIN_DEF) then begin
    old_time_bin_def = Self->Get(/time_bin_def)
    ndef = (n_elements(old_time_bin_def)-1)< (n_elements(time_bin_def)-1)
    old_time_bin_def = time_bin_def[0:ndef] > 1
    Self->Framework::Set, TIME_BIN_DEF = OLD_TIME_BIN_DEF, fw_set_id = fw_set_id
    ENDIF

If Keyword_set( TIME_BIN_MIN) then $
    Self->Framework::Set, TIME_BIN_MIN = long( TIME_BIN_MIN ) >1, fw_set_id = fw_set_id



IF exist( sum_coincidence) OR exist( coincidence_flag) THEN BEGIN

    IF NOT exist(coincidence_flag)  THEN $
      coincidence_flag = sum_coincidence
    IF NOT exist(sum_coincidence)  THEN $
      sum_coincidence = coincidence_flag

;The value of coincidence_flag has precedence
    sum_coincidence = coincidence_flag



    Self->FRAMEWORK::Set, $
      sum_coincidence=sum_coincidence, coincidence_flag=coincidence_flag, fw_set_id = fw_set_id
ENDIF

If Keyword_set(_extra) then Self->hsi_spectrogram::set, _extra=_extra,$
 NOT_FOUND=NOT_found, DONE=done, fw_set_id = fw_set_id




END
;---------------------------------------------------------------------------
;
;Newly added 4-mar-2005 in case the Ut_binned_eventlist is requested.
;After process it's been deleted and will only be created on request.
Function HSI_Binned_Eventlist::Get, $
         ut_binned_eventlist=ut_binned_eventlist, $
         FOUND=found, NOT_FOUND=NOT_found, DONE=done, $
         _EXTRA=_extra

;We need to run control2binning for ut_binned_eventlist to
;be certain ut_binned_eventlist computed with the correct parameters
;control2binning functions like a little object that doesn't store it's data products

if keyword_set(ut_binned_eventlist) then Self->control2binning,/time_only

return, self->hsi_spectrogram::get( FOUND=found, NOT_FOUND=NOT_found, DONE=done, $
         _extra=_extra,ut_binned_eventlist=ut_binned_eventlist )
end
;---------------------------------------------------------------------------
pro hsi_binned_eventlist::Compute_time_axes,$
    ;OUTPUTS
    ut_binned_eventlist, $
    absolute_time_range, $
    ntime_bins, $
    time_bin_busec_a2d, $
    this_det_index=this_det_index

;6-jun-2005 - if ut_binned_eventlist pointers exist then don't redo them
;this was a memory leak at about 25 Mbyte/minute of accumulation
u=self->framework::get(/ut_binned_eventlist)
input = Self->Get( /time_bin_min, /time_bin_def,/absolute_time_range)
absolute_time_range = input.absolute_time_range
time_bin_busec_a2d = ( input.time_bin_min * $
    input.time_bin_def) # (lonarr(3)+1L) ; for expanding in a2d, outside routines handle harmonics.

ntime_bins = ceil( (absolute_time_range[1]-absolute_time_range[0])*2.0d0^20 / time_bin_busec_a2d )
be_time_info = self->get(/be_time_info)
if not same_data( be_time_info, input ) or not keyword_set(u) then begin

    self->set, be_time_info = input
    ;if Self->Get( /align512 ) then hsi_aligntime512, absolute_time_range, sc_abs_time,  absolute_time_range
    det_index_mask = Self->Get(/det_index_mask)
    u = total(ptr_valid(u)) ne 9 ? ptrarr(9,/allocate_heap) : u

    for i=0, 8 do $
            *u[i] = absolute_time_range[0] + $
            dindgen(ntime_bins[i]+1)*time_bin_busec_a2d[i] / 2.0d0^20
        ut_binned_eventlist=u
        Self->Framework::Set, ut_binned_eventlist = ut_binned_eventlist

    endif
if keyword_set( this_det_index) then ut_binned_eventlist = *u[this_det_index]

end

;---------------------------------------------------------------------------

PRO HSI_Binned_Eventlist::Control2Binning, $
         ENERGY_ONLY=ENERGY_ONLY, $
         TIME_ONLY=TIME_ONLY, $
       _EXTRA=_extra, fw_set_id = fw_set_id

Self->Set, _Extra=_extra
; this procedure generates the control structure "binning" passed to
; hsi_eventlist_to_spectrogram. It is dependent on the sp_xxx
; parameters

If not keyword_set(TIME_ONLY) then begin
;When we have multiple energy bands for binned eventlists, we'll turn this on.
    ;
    sp_energy_binning = self->Get( /sp_energy_BINNING )
    ; 8-aug-03, ras - reconciling eb_index, sp_energy_binning, energy_band

    ;if not keyword_set( sp_energy_binning ) then sp_energy_binning = Self->Get(/energy_band)
    ; 8-aug-03, ras - reconciling eb_index, sp_energy_binning, energy_band

    IF N_Elements( sp_energy_binning ) EQ 1 THEN BEGIN
      HSI_RD_CT_Edges, sp_energy_binning, channel_cal_new
        binning = Add_Tag( binning, channel_cal_new, 'CHANNEL_CAL_NEW' )
        ENDIF ELSE $
        binning = Add_Tag( binning, sp_energy_binning, 'CHANNEL_CAL_NEW' )

    If Keyword_Set( ENERGY_ONLY ) then BEGIN
        self->HSI_Spectrogram::Set, BINNING = binning, /NO_UPDATE, fw_set_id = fw_set_id
        return
        endif
    endif
;5-mar-05 - move some of control2binning computations to this procedure
;for organization
Self->Compute_time_axes,$
    ut_binned_eventlist, $
    abs_time_range, $
    ntime_bins, $
    time_bin_busec_a2d

a2d_index_mask = Self->Get(  /a2d_index_mask)


ntime_bins = ceil( (abs_time_range[1]-abs_time_range[0])*2.0d0^20 / time_bin_busec_a2d )
;28-nov-2001, ras, make output of total(/cum) 1 dimensional in all IDL versions.
time_cumul_a2d = long([0L, (total( /cumulative,/double, ntime_bins * a2d_index_mask))[*]])
binning = Add_Tag(binning, {time_cumul_a2d:time_cumul_a2d, abs_time_range:abs_time_range, $
      time_bin_busec_a2d:time_bin_busec_a2d, regroup:0 }, 'BINNED_EVENTLIST')
;Every time binning changes, then ut_binned_eventlist and binning must be changed.

self->HSI_Spectrogram::Set, BINNING = binning, /NO_UPDATE

END

;----------------------------------------------
FUNCTION Hsi_Binned_Eventlist::GetAxis, XAXIS = xaxis, YAXIS=yaxis, $
                             UT= ut, ENERGY= energy, $ ras, 1-apr-02
                             MEAN=mean, GMEAN=gmean, $
                             WIDTH=width, EDGES_1=edges_1, EDGES_2=edges_2, $
                             EDGE_AXIS=edge_axis, $
                             This_Det_Index=this_det_index, $
                                ;This_Harmonic = this_harmonic, $ all harmonics for a grid have the same binning
                             _EXTRA=_extra

Self->Set, _EXTRA=_extra

binning = self->Get( /BINNING, ENERGY_ONLY=ENERGY )
IF Keyword_Set( UT ) or Keyword_Set( ENERGY )then begin
  yaxis = keyword_set( ut)
  xaxis = keyword_set( energy)
  endif

IF Keyword_Set( YAXIS ) THEN BEGIN

    If Keyword_Set( this_det_index )  then begin
      this_det_index = fcheck(this_det_index, 8)
      self->compute_time_axes, axis, this_det_index=this_det_index
      endif else axis = self->get(/ut_binned_eventlist)

ENDIF ELSE $

  Return, Self->HSI_SPECTROGRAM::GetAxis(MEAN=mean, GMEAN=gmean, $
            WIDTH=width, EDGES_1=edges_1, EDGES_2=edges_2, $
            EDGE_AXIS=edge_axis, /ENERGY)


nels = N_Elements( axis )
IF nels GT 1 and size(/tname, axis) ne 'POINTER' THEN BEGIN
    Edge_Products, axis, MEAN=this_mean, GMEAN=this_gmean, $
        WIDTH=this_width, EDGES_1=this_edges_1, EDGES_2=this_edges_2
    this_width = float( this_width )
    CASE 1 OF
        Keyword_Set( EDGES_1 ): RETURN, this_edges_1
        Keyword_Set( FULL ): RETURN, {MEAN:this_mean, GMEAN:this_gmean, $
                                      WIDTH:this_width, $
                                      EDGES_1:this_edges_1, EDGES_2:this_edges_2}
        Keyword_Set( EDGES_2 ): RETURN, this_edges_2
        Keyword_Set( GMEAN ): RETURN, this_gmean
        Keyword_Set( WIDTH ): RETURN, this_width
        ELSE: RETURN, this_mean
    ENDCASE
ENDIF ELSE RETURN, axis

END

;---------------------------------------------------------------------------

FUNCTION HSI_Binned_Eventlist::GetData, $
            THIS_DET_INDEX=this_det_index, $
            ;this_harmonic_index removed, ras, 20-apr-2007
            _EXTRA=_extra

@hsi_insert_catch

If keyword_set( _EXTRA) then Self->Set, _Extra = _extra
data=self->Framework::GetData( _Extra = _extra )

IF N_Elements( this_det_index ) NE 0 THEN BEGIN



    IF Ptr_Valid( data[ this_det_index ] ) THEN BEGIN
        RETURN, *data[ this_det_index ]
    ENDIF ELSE BEGIN
        RETURN, -1
    ENDELSE

ENDIF ELSE BEGIN

    RETURN, data

ENDELSE

END




;---------------------------------------------------------------------------

 Pro HSI_Binned_Eventlist::Process_Hook_post, $
 hsi_spectrogram, pha_total_vs_time,$
 ut_ref0 = ut_ref0, $
 nchan_out=nchan_out, $
 dim_spectrogram=dim_spectrogram, $
 channel_binning = channel_binning, $
 time_binning=time_binning, $
 time_cumul_a2d = time_cumul_a2d, $
 keep_dropout=keep_dropout



 remove_first_chan =  0 ;channel_binning.addzero
 a2d_index_mask = self->Get( /A2D_INDEX_MASK )

 these_a2d = Where( a2d_index_mask, na2d )
 ;RAS, 17-may-2004
 ;Forced to sum over coincidence if coincidence_flag and sum_coincidence are set
 ;Therefore the independent states are not preserved and
 ;it must be forced to reprocess to regain this information
 coincidence = self->Get( /COINCIDENCE_FLAG ) and self->Get(/sum_coincidence)
 if coincidence then hsi_spectrogram = total( hsi_spectrogram, 3)
 ;
 det_index = Get_Uniq( These_a2d MOD 9 )
 n_det = N_Elements( det_index )
 Self->Set, valid_binned_index = det_index

 front_segment = self->Get( /FRONT_SEGMENT )
 rear_segment = self->Get( /REAR_SEGMENT )

 time_unit = Self->Get( /TIME_UNIT )

 binned_eventlist_a2d = PtrArr( 27, /allocate_heap )

 binned_eventlist     = PtrArr( 9,  /allocate_heap)
 n_bin = Lonarr( 27 )

 use_total_count = Self->Get( /use_total_count)
 ;For now we don't need this
 IF USE_TOTAL_COUNT then begin
 total_count = Self->Get(/TOTAL_COUNT)
  If n_elements( total_count ) ne 27 then begin
   ptr_free, total_count
   total_count = PtrArr( 27, /allocate_heap )
   endif
   endif
 n_bin =  (time_binning.time_cumul_a2d[1:*]-time_binning.time_cumul_a2d)
 binned_n_event = self->get( /binned_n_event )

 for i=0,na2d-1 do begin
     a2d = these_a2d[i]
     n_bini = n_bin[a2d]
     if n_bini ge 1 then begin
         index = lindgen(n_bini) + long( time_binning.time_cumul_a2d[a2d])
         If USE_TOTAL_COUNT then $
         if ptr_valid( total_count[a2d] ) then $
            *total_count[a2d] = pha_total_vs_time[index] else $
            total_count[a2d] = ptr_new(pha_total_vs_time[index])

     if not exist( csel ) then  csel = lindgen( n_elements(hsi_spectrogram) / $
      last_item( time_binning.time_cumul_a2d) )

         *binned_eventlist_a2d[a2d] = reform( (hsi_spectrogram[ *, index,* ])[csel,*,*] )
         endif
     endfor
 If USE_TOTAL_COUNT then self->Framework::Set, total_count = total_count
 utp = Self->getaxis(/yaxis )
 use_cull = Self->Get(/use_cull)  ;set gap flag on all bins where dropout fraction gt cull_frac
 cull_frac = Self->Get(/cull_frac)
 If use_cull  then begin
    dropout = Self->get(/dropout)
    time_bin_def = Self->Get(/time_bin_def)
    time_bin_min= Self->Get(/time_bin_min)
    time_bin_frac = long(time_bin_min) * time_bin_def / 2.^20 * cull_frac
    endif

 livetime_arr = Self->Get(/livetime_arr)
 ;this_harmonic = 0 ;For now, only first harmonic, ras, 20-oct-2001
 Front_Segment = Self->Get(/FRONT_SEGMENT)
 for i=0, n_det-1 do begin

     this_det_index = det_index[i]
     n = n_bin[this_det_index]
     time_bin = time_binning.time_bin_busec_a2d[this_det_index]
     this_time_bin = time_bin / time_unit
   timewidth= get_edges( *utp[this_det_index], /width)
     if n ge 1 then begin

         ;hsi_time_bin will require modification for multiple energy channels
         ;in the binned eventlist
         this_be = nchan_out eq 1 ? REPLICATE( {hsi_time_bin}, n ) : $;If more than one energy
         REPLICATE( {TIME:0L, COUNT:fltarr( nchan_out ), $
         LIVETIME:fltarr(nchan_out), gap:0b}, n)
         if front_segment then begin
            if a2d_index_mask[this_det_index] then begin
                 if use_cull then $
                 this_be.gap    = (fix( *dropout[this_det_index]/time_bin_frac[this_det_index])<1>0)
                 this_be.count = *binned_eventlist_a2d[this_det_index]

                 endif else this_be.count = 0

         endif

         if rear_segment then this_be.count = $
         this_be.count + $
         (a2d_index_mask[this_det_index+9]  ? *binned_eventlist_a2d[this_det_index+9 ] :0 ) + $
         (a2d_index_mask[this_det_index+18] ? *binned_eventlist_a2d[this_det_index+18] :0 )

         binned_n_event[ this_det_index ] = total( this_be.count )

         ;this_be.livetime = FRONT_SEGMENT ? $ ;non-negative, kudos to EJS, 13-may-02
         ; (1.0 - f_div(*dropout[this_det_index ],timewidth))>0.0 : 1.0

        this_be.livetime[0] =  Self->get(/livetime_enable) ? $ ;and FRONT_SEGMENT ? $
        (*livetime_arr[this_det_index +(1-Front_segment)*9])[0:n-1] : 1.0
        If nchan_out gt 1 then for ichan=1,nchan_out-1 do this_be.livetime[ichan]= this_be.livetime[0]

         this_be.time = LINDGEN( n ) * (this_time_bin) + this_time_bin / 2
         *binned_eventlist[this_det_index ] = this_be

         endif

     endfor

 ;Kill large info arrays, 4-mar-2005

 if not keyword_set(keep_dropout) then $
    Self->set,Dropout = 0,Livetime_arr = 0, ut_binned_eventlist=0, livetime_ctr=0
 ptr_free, binned_eventlist_a2d
 self->set, binned_n_event = binned_n_event,$
 N_BIN = n_bin, $
 LIVETIME_ARR=0 ;Clear the livetime_arr info parameter after it's used, ras, 14-jun-02
 Self->decim_correct, binned_eventlist
 ; Framework::setdata doesn't free any pointers that are inside self.data.
; Here, we will replace entire contents of self.data, so get current pointers and free them. kim, ras, 28-feb-2011

 old_binned_eventlist = Self->framework::Getdata(/noprocess)
 if ptr_chk(old_binned_eventlist) then ptr_free, old_binned_eventlist
 self->Framework::SetData, binned_eventlist


 end

;----------------------------------------------------------------

;+
; PROJECT:
;   HESSI
; NAME:
;   HSI_SPECTROGRAM::ACCBIN
;
; PURPOSE:
;   Returns arrays or pointers of accumulation bins
;   dimensioned by energy/time/segment as needed
; CATEGORY:
;   SPECTRA
;
; CALLING SEQUENCE:
;
;
; CALLS:
;   none
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   none
;
; MODIFICATION HISTORY:
;   Version 1, richard.schwartz@gsfc.nasa.gov
;   May 21, 2002
;   25-jul-02, accumulate dropouts for front and rear
;-
pro HSI_binned_eventlist::accbin, time_binning, $
    livetime_arr, $
    samples, $
    deadacc_arr

If keyword_set(_extra) then Self->Set, _extra=_extra


If (not exist( livetime_arr)) and (not exist( samples )) and (not exist( deadacc_arr)) then begin


    TIME_CUMUL_A2D = time_binning.TIME_CUMUL_A2D
    nbin = time_cumul_a2d[1:*]-time_cumul_a2d ;
    deadacc_arr = ptrarr(9,3)
    livetime_arr = ptrarr(18,3)
    samples = ptrarr(18,3)

      ;index0 = 0 ; 9*(1-front) ;start at 0 or 9
    for i= 0, 8  do $
        deadacc_arr[i]= ptr_new( fltarr(nbin[i]))
    for i= 0, 17 do begin
        livetime_arr[i] = ptr_new( fltarr(nbin[i]))
        samples[i] = ptr_new( fltarr(nbin[i]))
       endfor
      endif
      end
;---------------------------------------------------------------------------

PRO HSI_Binned_Eventlist__Define

; this is the data structure residing in self.data:
self = {HSI_Binned_Eventlist, $
srm: obj_new('hsi_srm'), $
INHERITS HSI_Spectrogram }

END

;---------------------------------------------------------------------------
; End of 'hsi_binned_eventlist__define.pro'.
;---------------------------------------------------------------------------
;---------------------------------------------------------------------------

