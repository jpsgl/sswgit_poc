;---------------------------------------------------------------------------
; Document name: hsi_visibility_raw__define.pro
; Time-stamp: <Thu Nov 27 2008 09:39:13 csillag soleil.i4ds.ch>
;+--------------------------------------------------------------------------
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI VISIBILITY RAW CLASS DEFINITION
;
; PURPOSE:
;       This class is really just a wrapper around hsi_vis_gen. It manages
;       the visibilities generated and stores them temporarily for later use.
;
;       The class really belongs to a whole pattern. It is best used from
;       hsi_visibility(), which provides the possibility to get visibilities
;       either from a file or from this class.
;
;       For more infromation please check hsi_visibility__define
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       Not directly, only via hsi_visibility()
;
; EXAMPLES: see tests below
;
; HISTORY:
;   24-May-2017, Kim. Now process all vis for all energy and time at once and save in obj. Previously did all energies for
;     a time band, What is in process_ras is what used to be the process method. And what was in process_dev is now
;     the process method with some changes. Changed need_update to use a no_new_tb flag (in addition to the
;     no_new_eb flag already being used).
;
;		20-nov-2014, ras, revert the process method to prior to 25-jun-2013 revision but
;			also integrated the change of 28-aug-2014 into the reverted process
;			The 25-jun-2013 change was very problematic causing the full reprocessing of all intervals
;			until the final time bin.  Therefore, I changed the process method back to the previous code
;
;		28-aug-2014, ras, passing VIS_USE_FLUX_VAR from the Caller to hsi_vis_gen through process::
;		25-jun-2013, ras, simplified the processing.  If there is a new set of times or energies
;			the whole series is processed and put into vismap.  No picking and choosing times, all at once
;			Process_old was made from Process on 25-jun-2013.  It's there in case we need to revert but
;			it should not be called,
;
;		Changes made to support the different energy binning for the normal photon
;			visibilities and the derived regularized visibilities (electron and photon)
;			The raw photon visibilities energies are determined from the input electron energies
;			to simplify the image cube processing, also, automatic write if filename defined is disabled
;		    , use obj->write instead, RAS, Oct 2012
;       Changes to make /all and tb_index work correctly, 23-Dec-2009, ACS
;       In need_update, add check for noreload, 19-Jul-2009, ACS
;       Print time and energy when making new visibilities, 24-Sep-2008, Kim
;       Merging w/ harmonics s/w, July 2007 acs
;       Support for image cubes Jan-Jun 2007 acs
;       Modifications and testing Dec 2005 acs
;       Created sept 2005 acs.
;
;---------------------------------------------------------------------------

pro hsi_visibility_raw_test

  o = obj_new( 'hsi_visibility' )

  energy_band = [6, 12,25]
  time_range  = ['2002-aug-28 18:57:50', '2002-aug-28 18:58:50']
  nroll    = 32 ; select number of roll bins
  det_index_mask  = [1,0,1, 1,1,1, 1,1,1] ; suppresses grid 2
  xyoffset    = [-939, 166]; map center good to < 1 arcsec

  ;o->set, phz_n_roll_bins_control     = 1; 1==>User specifies number of roll bins; otherwise 0
  ;o->set, phz_n_roll_bins_min        = INTARR(9)+nroll
  ;o->set, phz_report_roll_bins      = 1; 1==> display number of roll bins in output log
  ;o->set, cbe_digital_quality        = 0.99
  o->set, xyoffset       = xyoffset
  o->set, obs_time   = time_range
  o->set, energy_band     = energy_band
  ;o->set, det_index_mask      = det_index_mask

  vis = o->getdata()
  nvis = N_ELEMENTS(vis)
  SAVE, vis, FILENAME=savefile
  PRINT, nvis,  '-element visibility structure array, "vis", saved in  ', savefile
  IF plotfit NE 0 THEN PRINT, 'Fits are plotted in hsi_vis_gen.ps'

end

;----------------------------------------------------------------------------

FUNCTION Hsi_Visibility_raw::Init,caller

  self.caller = caller

  RETURN, 1

END

;----------------------------------------------------------------------------

function hsi_visibility_raw::need_update, no_new_eb = no_new_eb, no_new_tb=no_new_tb, noreload = noreload, all = all

  ; kind of ugly. passes back the call to the caller. dont know yet how to avoid
  ; this level of call.

  ; first check the calib eventlist, if this is enabled or it has been
  ; updated after the visibility we need to recompute.

  ; first, the decisions that can be made quickly:
  if not keyword_set( noreload ) and self.caller->getreload() then return, 1b
  if not keyword_set( no_new_eb ) and self.caller->yes_new_eb() then return, 1b
  if not keyword_set( no_new_tb ) and self.caller->yes_new_tb() then return, 1b
  if not keyword_set( noreload ) then if self.caller->get( /need_update, /this ) then return, 1b

  cbe_obj = self.caller->get( class = 'hsi_calib_eventlist', /obj )

  if cbe_obj->need_update() or $
    self.caller->get( /last_update, /this ) lt cbe_obj->get( /last_update, /this ) then return, 1b
  ;      self.caller->get( /need_update, /this ) or $

  data = self.caller->getdata( / noprocess )
  tb_index = self.caller->get_tb_index()
  ;source = self.caller->get( /source )

  if self.caller->process_all() then $
    for i=0, n_elements(data)-1 do $
    if (ptr_valid(data[i]) ? n_elements( *data[i] ) eq 0 : 1 ) then return, 1b

  ; need only to test first element in energy dimension
  ;need_update = (not vis_map_valid[tb_index,0] ) or $
  ;    (keyword_set(all) and total( vis_map_valid[*,0] ) ne n_elements( vis_map_valid[*,0] ) ) or $
  ;    source->need_update() ? 1:0

  need_update = N_elements( *data[tb_index,0] ) eq 0 ? 1b:0b

  return,need_update

end

;---------------------------------------------------------------------------
; 24-May-2017, Kim. Now I've renamed this from process to process_ras, so it is not called any more.
; This version only kept vis from one time band.  Now I've renamed the old process_old to process because I've
; made changes so that vis for all times and energies are done at once and stored.
;			Process_old was made from Process on 25-jun-2013 by RAS.  It's there in case we need to revert but
;			it should not be called,
;           Restored by RAS, 20-nov-2014.
;
PRO Hsi_Visibility_Raw::Process_ras, tb_index = tb_index, vis_get_mode = vis_get_mode


  caller = self.caller

  ; that means it's just a change of detector mask
  ; !!!!!!!!!!!!!!!! kim commented out next line 5-nov-2008   !!!!!!!!!!!!!!!!!!!!!!!
  ;if caller->get( /need_update ) eq 0 then return

  ; this means only the eb index changed, so we dont have to reprocess
  ; anything.
  ; acs 2009-05-06 added noreload, this will replace no_new_eb and is
  ; there to differentiate the cases where just a reload of the already
  ; calculated visibilities is needed instead of a full recalculation

  if not self->need_update(/no_new_eb, /noreload)  then return

  source = caller->get( /source )

  sp_energy_binning= get_edges( /edges_2, source->get( /sp_energy_binning ))
  n_en = (size(/dim, sp_energy_binning))[1]
  times = get_edge_products( source->get( /im_time_interval ), /edges_2 )
  n_time = (n_elements( times[0, *]))

  vismap = caller->getdata( /noprocess )

  vismap_size = size( vismap )
  vismap_dim  = ([size(/dim, vismap),1])[0:1] ;we need to get 2dim even if vismap is 1 dim, ras oct 2012
  ; if vismap is -1 then it has not been defined yet.
  ; also if the dimensions have changed, we need to reinitialize, as
  ; well as if the map is undefined
  ; kim changed second line to use [1] and [2] from [0] and [1].  21-apr-2009
  print, 'Self->need_update()', self->need_update()
  if self->need_update() or is_number( vismap[0] ) or $
    ;vismap_size[1] ne n_time or vismap_size[2] ne n_en or $ that blows up if vismap is ptrarr(1) and n_en is 10 and n_time is 1
    vismap_dim[0] ne n_time or vismap_dim[1] ne n_en or $ ;, ras oct 2012
    (vismap_size[0] eq 1 and not ptr_valid( vismap[0] )) then begin
    if ptr_valid(vismap[0]) then ptr_free, vismap
    vismap = ptrarr( n_time, n_en, /alloc )
  endif

  if tb_index ne -1 and not caller->process_all() then begin
    niter = 1
    iter_list = [tb_index]
    ;    start_iter=tb_index
    ;    end_iter =tb_index
  endif else if caller->process_all() then begin
    niter = 0
    for i = 0, n_elements( vismap[*,0] )-1 do begin
      nels = n_elements( *vismap[i,0] )
      if nels eq 0 or self->need_update() then begin ;added source->need_update(), ras, 25-jun-2013, if that changes, this must too
        niter++
        iter_list = append_arr( iter_list, i )
      endif
    endfor
  endif else begin
    niter = n_time
    iter_list = indgen( n_time )
  endelse

  ;times = get_edge_products( times, /edges_2 )
  vis_plotfit = caller->get( /vis_plotfit )

  ;acs we compute always for all detectors
  old_det = source->get( /det_index_mask )
  source->set, det_index_mask=bytarr(9)+1
  for j = 0, niter-1 do begin
    idx = iter_list[j]
    for i=0, n_en-1 do begin
      print, '****** making vis for ', format_intervals(times[*,idx],/ut), '  ', format_intervals(sp_energy_binning[*,i]), ' keV'
      source->set, eb_index = i
      source->set, tb_index = idx
      *vismap[idx,i] = hsi_vis_gen( source, plotfit = vis_plotfit, vis_use_flux_var = caller->Get(/vis_use_flux_var) )
    endfor
  endfor
  source->set, det_index_mask = old_det, /no_update

  ;source->Set, used_xyoffset = (*vismap[0]).xyoffset
  caller->setdata, vismap
  caller->set, need_update = 0
  caller->unsetreload

  if is_string( caller->get( /vis_out_filename ) ) then begin
    print,"Automatic WRITE after hsi_visibility_raw::process disabled. "
    print,"Use hsi_visibility object's explicit write method"; then caller->write
  endif

END

;--------------------------------------------------------------------------
; 24-May-2017, Kim. Richard had called this process_dev and made what's in process_ras the real process (in that
;  version, all energies for a time interval were saved, but lost when move to a new time interval).  I've
;  gone back to this process method (which Richard had called process_dev) and made it work.  Now save vis for
;  all times and energies and save in obj.  Changed need_update to use a no_new_tb flag (in addition to
;  the no_new_eb flag already being used)
;
;  Richard's old comments:
;
;     25-jun-2013, simplified the processing.  If there is a new set of times or energies
;     the whole series is processed and put into vismap.  No picking and choosing times, all at once
;     This is a simplifcation of the overly complicated and buggy old process, now called process_old
;
;     20-nov-2014, this was not working properly, getting to the vismap regeneration too often (almost every time) and so have
;	    disabled this block and returned to the previous block.
;	    added  vis_use_flux_var = caller->Get(/vis_use_flux_var) in the call to hsi_vis_gen
;	    which came between the code fork and reversion to the earlier version of process

;
PRO Hsi_Visibility_Raw::Process, tb_index = tb_index, vis_get_mode = vis_get_mode

  message, /cont, 'IN vis raw process'

  caller = self.caller

  ; that means it's just a change of detector mask
  ; !!!!!!!!!!!!!!!! kim commented out next line 5-nov-2008   !!!!!!!!!!!!!!!!!!!!!!!
  ;if caller->get( /need_update ) eq 0 then return

  ; this means only the eb index changed, so we dont have to reprocess
  ; anything.
  ; acs 2009-05-06 added noreload, this will replace no_new_eb and is
  ; there to differentiate the cases where just a reload of the already
  ; calculated visibilities is needed instead of a full recalculation

  if not self->need_update(/no_new_eb, /no_new_tb, /noreload)  then return

  message, /cont, 'Continuing in vis raw process'

  source = caller->get( /source )

  sp_energy_binning= get_edges( /edges_2, source->get( /sp_energy_binning ))
  n_en = (size(/dim, sp_energy_binning))[1]
  times = get_edge_products( source->get( /im_time_interval ), /edges_2 )
  n_time = (n_elements( times[0, *]))

  vismap = caller->getdata( /noprocess )
  if total(ptr_valid( vismap )) gt 1 then ptr_free, vismap
  vismap = ptrarr( n_time, n_en, /alloc )



  ;Don't screw around, if we're here fill the vismap over all the n_en and n_time
  iter_list = indgen( n_time )
  niter     = n_time
  ;times = get_edge_products( times, /edges_2 )
  vis_plotfit = caller->get( /vis_plotfit )

  ;acs we compute always for all detectors
  old_det = source->get( /det_index_mask )
  source->set, det_index_mask=bytarr(9)+1
  for j = 0, niter-1 do begin
    idx = iter_list[j]
    for i=0, n_en-1 do begin
      print, '****** making vis for ', format_intervals(times[*,idx],/ut), '  ', format_intervals(sp_energy_binning[*,i]), ' keV'
      source->set, eb_index = i
      source->set, tb_index = idx
      *vismap[idx,i] = hsi_vis_gen( source, plotfit = vis_plotfit, vis_use_flux_var = caller->Get(/vis_use_flux_var) )
    endfor
  endfor
  source->set, det_index_mask = old_det, /no_update

  ;source->Set, used_xyoffset = (*vismap[0]).xyoffset
  caller->setdata, vismap
  caller->set, need_update = 0
  caller->unsetreload

  if is_string( caller->get( /vis_out_filename ) ) then begin
    print,"Automatic WRITE after hsi_visibility_raw::process disabled. "
    print,"Use hsi_visibility object's explicit write method"; then caller->write
  endif

END

;--------------------------------------------------------------------------

PRO Hsi_Visibility_Raw__Define

  self = { hsi_visibility_raw,  caller: obj_new() }

END


;---------------------------------------------------------------------------
; End of 'hsi_visibility_raw__define.pro'.
;---------------------------------------------------------------------------
