;---------------------------------------------------------------------------
; Document name: Hsi_Bproj_Cart__define
; Created by:    Andre Csillaghy, May 1999
;
; Last Modified: Sat Jun 02 08:47:27 2001 (csillag@TOURNESOL)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI BACK PROJECTION CLASS DEFINITION
;
; PURPOSE:
;       Provides data structures and methods to generate and retrieve
;       back projection maps.
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       o = Obj_New( 'hsi_bproj' ) ;
;       or
;       o = HSI_BProj()
;
;       The variable o is the object references used to
;       access back projection maps and associated methods.
;
; INPUTS (CONTROL) PARAMETERS:
;
; OUTPUTS (INFORMATION) PARAMETERS:
;
;
; SOURCE OBJECT:
;       HSI_Modul_Pattern
;
; DESTINATION OBJECTS:
;       HSI_Modpat_Products, HSI_Image
;
; EXAMPLE:
;
; SEE ALSO:
;       HESSI Utility Reference
;          http://hessi.ssl.berkeley.edu/software/reference.html
;       hsi_modul_pattern__define
;       hsi_bproj_control__defin
;       hsi_bproj_control
;       hsi_bproj_info__define
;
; HISTORY:
;    15-Feb-2018, RAS, added taking care of flatfield
;    26-apr-2016, ras, extract cbe_ptr in the alg_hook to be sure off_det_index info parameter is consistent
;    with the just processed cbe_ptr
;    28-jun-2005, ras, changed DET_INDEX_OFF to OFF_DET_INDEX
;
;    12-may-2005, ras, added DETECTOR_INDEX_OFF check for times
;     with no livetime for a particular detector such as 8
;     Won't be set on the first pass so must check inside
;     hsi_annsec_bproj.pro as well.
;
;       29-apr-2005, ras, by setting NORATE_CORRECT to 1, the USE_RATE
;     correction can be inhibited on the call to getdata.  This is a true
;     keyword not a control parameter and so does not persist.
;     19-apr-2004, ras, pass USE_RATE control parameter through to annsec_bproj
;       Release 6 development: introduction of normal, uniform, taper, and
;                              spatial_frequency_weight keywords
;       Release 3 development, August / September 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;       Based on the release 2 software of Richard Schwartz
;       Fundamental developments by Richard Schwartz, GSFC
;
;-----------------------------------------------------------------

FUNCTION Hsi_Bproj_Cart::HSI_Bproj_Alg_Hook, $

  NORATE_CORRECT = norate_correct,$
  NO_DET_MASK = no_det_mask, $
  _EXTRA=_extra


  ;use_rate = Self->Get(/USE_RATE)
  use_rate = keyword_set( NORATE_CORRECT)  ?  0 :  Self->Get( /USE_RATE )
  ocbe = self->Get( /obj,  class='hsi_calib_eventlist' )

  use_local_average = self->get(/use_local_average)
  smoothing_bins     = use_local_average ? ocbe->Get_smoothing_bins() : 0
  ;This check would need to be in bproj_vismod as well
  default, no_det_mask, 0
  cbeptr = no_det_mask ? ocbe->framework::getdata() : ocbe->getdata() ; We're getting this now to make sure off_det_index_mask is consistent

  ;There will be 0's for any detector segment that must be excluded
  ;because of no livetime, ras, 12-may-2005
  ;

  ocmp = self->get(/obj, class='hsi_modul_pattern')
  ;cpix = obj->get(/pixel)
  ;ocmp->set, _extra = cpix
  dcmp = ocmp->getdata()
  extra = fix_extra( _extra, 'this_flatfield')
  this_flatfield = get_tag_value( extra, /this_flatfield, /quiet)

  if (self->Get(/flatfield ) || this_flatfield) then begin
    ;make the flatfield arrays if needed
    pvalid = where( ptr_valid( dcmp ), npvalid )
   for ipv = 0, npvalid - 1 do begin 
    mpati = *dcmp[ pvalid[ipv]]
    if ~ptr_exist( (mpati).weight_map_ptr ) then begin
      message, /inf, 'Weight map for CART Detector: '+strtrim( pvalid[ipv], 2)
      mpati = *dcmp[ pvalid[ ipv ] ]
      hsi_cart_bproj_weight_map, self, mpati
      (*dcmp[pvalid[ ipv ] ]).weight_map_ptr = mpati.weight_map_ptr     
    endif
    endfor
  endif
  out = hsi_cart_bproj_arr( cbeptr, dcmp, use_rate=use_rate, _extra = _extra )

  return, out
END

;----------------------------------------------------------

PRO HSI_Bproj_Cart__Define

  bproj = {HSI_Bproj_Cart, $
    INHERITS HSI_Bproj_Strategy}

END


;---------------------------------------------------------------------------
; End of 'hsi_bproj__define.pro'.
;---------------------------------------------------------------------------
