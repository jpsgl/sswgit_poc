;+
; Name:  hsi_aspect_solution::plotman
;
; Purpose:  Plotman method for hsi_aspect_solution object.
;
; Calling sequence:  o -> plotman  where o is an hsi_aspect_solution object.
;    If o is an image object, call o -> plot_aspect, plotman_obj=plotman_obj, which will call this.
;    _extra keywords are passed on to aspect plot method.
;
;
; Keyword Inputs:
; plotman_obj - plotman obj to use, or new plotman obj will be returned 
; plot_diam - if set, plot diameter of image axis circle vs time
; plot_dist - if set, plot distance from Sun center to imaging axis (if user requested
;   sas_only solution, then this is the only plot option
; plot_triangle - if set, plot size of triangles (for measure of
;   quality of aspect (< .5 is OK)
; plot_p_error - if set, plot SAS error
;
; Outputs:  Creates new panel in plotman with requested plot.
;
; History:  Written Kim, 27-Jul-2015
; Modifications:
;-
;============================================================================


pro hsi_aspect_solution::plotman, plotman_obj=plotman_obj, $ 
  plot_diam=plot_diam, $
  plot_triangle=plot_triangle, $
  plot_dist=plot_dist, plot_p_error=plot_p_error, _extra=_extra
  
valid_plotman = is_class(plotman_obj,'plotman', /quiet) ? plotman_obj->valid() : 0
if not valid_plotman then begin
  plotman_obj = obj_new('plotman', error=error)
  if error then return
endif
    
plot_type = 'xyplot'
desc = 'Image, Spin Axis'
if keyword_set(plot_diam) or keyword_set(plot_triangle) or keyword_set(plot_dist) or keyword_set(plot_p_error) then plot_type = 'utplot'
case 1 of
  keyword_set(plot_diam): desc = 'Diam Image Axis'
  keyword_set(plot_dist): desc = 'Distance to Image Axis'
  keyword_set(plot_triangle): desc = 'SAS Triangle'
  keyword_set(plot_p_error): desc = 'SAS Error'
  else:
endcase

tr = self->get(/absolute_time_range)
desc = 'RHESSI ' + desc + ' ' + format_intervals(tr, /ut, /trunc)

plotman_obj->new_panel, input=self, plot_type=plot_type, desc=desc, $
  plot_diam=plot_diam, plot_triangle=plot_triangle, plot_dist=plot_dist, plot_p_error=plot_p_error, _extra=_extra

end