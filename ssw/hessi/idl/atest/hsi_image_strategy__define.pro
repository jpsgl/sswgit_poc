;---------------------------------------------------------------------------
; Document name: hsi_image_strategy__define.pro
; Created by:    Kim Tolbert & Andre Csillaghy, October 2002
; Time-stamp: <Wed Jan 28 2009 17:52:27 csillag soleil.i4ds.ch>
;---------------------------------------------------------------------------
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI IMAGE STRATEGY CLASS DEFINITION
;
; PURPOSE:
;       This class provides the generic prcedures and attributes needed by
;       both hsi_image_raw and hsi_image_file. This class is abstract in the
;       sense that it works only together with the classes just mentioned.
;
; CATEGORY:
;       HESSI Imaging
;
; CONSTRUCTION:
;       Only with the concrete classes hsi_image_raw or hsi_image_file
;
; CONTROL PARAMETERS DEFINED IN THIS CLASS:
;       See hsi_image__define
;
; INFO PARAMETERS DEFINED IN THIS CLASS:
;       See hsi_image__define
;
; CLASS RELATIONSHIPS:
;       Inherits framework
;
; SEE ALSO:
;       hsi_image
;       hsi_image_strategy
;       hsi_image_raw
;       hsi_image_file
;
; Modifications:
;           1-jun-2017, RAS removed kluge in getaxis for mem_njit offset problem with even pixels, fixed in hsi_mem_njit::mem
;           2017-May-18, Kim. Added namedialog and path keywords to fits_create and fitswrite methods. If namedialog=0, then
;             output file is autonamed (need for running autonomously). In movie method, make sure time array is [2,n].
;           2013-aug-08, Kim. Fixed bug in get - returned structure contained itself (see comments in get)
;           2011-Jun-27, Kim. Remove AS_QUALITY from subset_info_tags in INIT, and therefore from the
;            summary_info structure (extension 2) for image cubes.  Didn't handle the nested structure
;            correctly, so ended up with too many triangle arrays which can be huge.  Can't find anywhere
;            that it's used later anyway. (thanks to Qingrong Chen for pointing out the large fits files)
;           2010-08-12 - Kim. In GET, add check for struct for IDL 8.0 (n_tags(null obj) fails in 8.0)
;           2010-01-04- Kim. In fitswrite, set silent in writer, and then write one message
;            for updating each image.  Previously got a msg per extension for each image, which
;            grows since it writes all extensions again for each new image.
;           2009-08-18- Kim. In cleanup, free self's pointers and call framework::cleanup
;           2008-12-17 - acs, removed time and energy axis objects
;           2007-07-18 = ras, correction to coordinates for mem_njit for even pixels
;           2005-07-27 - see change below with this date; minor change
;           2005-06-16 - changes to control reprocessing when dealing with
;                        different image algorithms. acs
;           2005-05-11 - changes to make it work with o->get( /time )
;           2005-01-07 - first beta version csillag@fh-aargau.ch
;           2004-09-01 - first alpha version, csillag@fh-aargau.ch
;           2004-01-15 - merge hsi_image and hsi_multi_image, acs
;           2003-12-13: Andre Csillaghy - many changes to make a
;                                         smoother input to
;                                         spex_hessi_multi_image
;           2003-10-01 - hsi_image is split into multiple
;                        strategies, csillag@ssl.berkeley.edu
;
; Written:  Kim Tolbert / Andre Csillaghy
;--------------------------------------------------------------------

FUNCTION hsi_image_strategy::INIT, SOURCE = source, $
  control = control, info=info, $
  caller = caller, _EXTRA=_extra

  self.use_single_return_mode = 1
  self.info_summary = ptr_new(/alloc)
  self.param_arr = ptr_new( /alloc )

  ; acs 2004-08-31
  if not keyword_set( source ) then source = hsi_image_single(caller=caller)

  if not keyword_set( control ) then control = hsi_image_strategy_control()
  if not keyword_set( info ) then info = {hsi_image_strategy_info}
  ;info.time_axis = obj_new('time_axis' )
  ;info.energy_axis = obj_new( 'spectrum_axis' )

  ret = self->framework::init(control=control, $
    info=info, $
    source=source, _extra=_extra)

  IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

  self.subset_info_tags = ptr_new( ['BINNED_N_EVENT', $
    'IM_EBAND_USED', $
    'USED_XYOFFSET', $
    'ABSOLUTE_TIME_RANGE', $
    'CBE_DET_EFF', $
    ; acs 2005-02-19                                  'AS_QUALITY', $
    ;                                  'AS_QUALITY', $
    'BINNED_N_EVENT', $
    'IMAGE_ATTEN_STATE'] )

  RETURN, ret

END

;----------------------------------------------------------
; Kim added namedialog and path keywords, 18-may-2017
; namedialog - if set, does what it used to do - opens widget for user to set name. Call with namaedialog=0 to
;   NOT have a dialog, and have it autoname the file
; path - Can pass in the path for the output file

pro hsi_image_strategy::fits_create,  err_msg =  err_msg, $
  n_time = n_time, $
  n_energy = n_energy, $
  this_out_filename = this_out_filename, $
  namedialog=namedialog, $
  path=path

  err_msg =  ''

  checkvar, namedialog, 1

  if is_string( this_out_filename ) then begin
    im_out = this_out_filename
    if keyword_set(path) then im_out = concat_dir(path, im_out)
  endif else begin
    im_out = self -> get( /im_out )
    if not is_string( im_out ) then begin
      prefix = n_time gt 1 or n_energy gt 1 ? $
        'hsi_imagecube_'+trim(n_time)+'tx'+trim(n_energy)+'e_': $
        'hsi_image_'
      default_file = prefix + time2file( (self->getaxis( /ut, /edges_1 ))[0], /sec ) + '.fits'
    endif
  endelse

  obj_destroy, self.writer
  self.writer = obj_new( 'hsi_image_fitswrite',  $
    self,  $
    filename =  im_out,  $
    default = default_file, $
    title='Select the output FITS file name', $
    /create )

  if not is_string( im_out ) then begin

    if namedialog then im_out = self.writer->get( /filename ) else begin
      im_out = default_file
      ; note - path should only be used if namedialog wasn't used since namedialog returns full path
      if keyword_set(path) then im_out = concat_dir(path, im_out)
      self.writer->set,filename=im_out
    endelse
    if ~is_string( im_out ) then begin
      err_msg = 'No filename given. Aborting.'
      message, err_msg, /cont
      return
    endif

    self->framework::set, im_out = im_out

  endif

  if self.verbose gt 0 then message, '(re)creating file with name ' + im_out, /info

end

;----------------------------------------------------------

pro hsi_image_strategy::cleanup_axis, _ref_extra = extra

  axis = self->framework::get( _extra = extra )
  obj_destroy, axis

end

;----------------------------------------------------------

pro hsi_image_strategy::cleanup

  ;self->cleanup_axis, /time_axis
  ;self->cleanup_axis, /energy_axis
  obj_destroy, self.writer
  free_var, self.param_arr
  free_var, self.info_summary
  free_var, self.subset_info_tags

  self->framework::cleanup

end

;----------------------------------------------------------

pro hsi_image_strategy::save, filename
  ; this writes a VOtable xml file with all params

  ;  !!!!!!!!!!!!kim commented these 4 and inserted next 2 - axis change- 3-nov-2008 !!!!!!!!!!!!!!!!!!!!!!
  ;energy_axis = self->get( /energy_axis )
  ;time_axis   = self->get( /time_axis )
  ;ebands = energy_axis->get( /edges_2 )
  ;times  =   time_axis->get( /edges_2 )
  times = self -> getaxis (ut, /edges_2)
  ebands = self -> getaxis (/energy, /edges_2)

  control_params = self->get(/control)

  control_params = add_tag( control_params, ebands, 'EBANDS' )
  control_params = add_tag( control_params, anytim(times, /ccsds), 'TIMES' )

  ; add info about the user
  control_params = add_tag( control_params, 'Andre Csillaghy', 'PERSONAL_NAME' )
  control_params = add_tag( control_params, 'csillag@fh-aargau.ch', 'PERSONAL_EMAIL' )

  ; add drig mode (edit by reto leuenberger)
  ; values: singleband multiband
  control_params = add_tag( control_params, 'singleband', 'DRIG_MODE' )

  ; remove the time_range and energy_band params
  ; these are old params that should not go into the xml file

  control_params = rem_tag( control_params, 'time_range' )
  control_params = rem_tag( control_params, 'energy_band' )
  control_params = rem_tag( control_params, 'obs_time_interval' )

  xmldoc = struct2votable( control_params )

  ; acs adds this to write the xml data in a file 2004-6-3
  checkvar, file, 'hsi_gaga.xml'
  ;if keyword_set( save ) then begin
  openw, 1, 'gaga.xml'
  printf, 1, xmldoc, format = '(a)'
  close, 1
  ;    return
  ;endif

end

;-----------------------------------------------------------------------------------

pro hsi_image_strategy::fitswrite,  image_arr,  $
  n_time =  n_time,  $
  n_energy =  n_energy,  $
  err_msg =  err_msg,  $
  nocreate = nocreate,  $
  this_out_filename = this_out_filename, $
  full_info = full_info, $
  namedialog=namedialog, $
  path=path, $
  _ref_extra =  extra

  err_msg = ''

  if not exist( image_arr ) then begin
    ; if the image arr is not passed, that means fitswrite is called directly
    ; from the user. Thus we check if the image is up to date, and if not
    ; we call getdata to redo the cube. the mess with the negations is historical
    if keyword_set( extra ) then self->set, _extra =  extra
    need_update = self->need_update( )
    checkvar, full_info, self->get( /full_info )

    image_arr =  self->getdata( this_out_filename = this_out_filename, $
      _extra = extra, err_msg = err_msg, /yes_file, /all_images )

    ; if updating was required, then this getdata call updated the cube and wrote the FITS file.
    ; Otherwise, the cube was already generated, and we fall through here to write the FITS file.
    if need_update then return

  endif

  if image_arr[0] eq -1 then begin
    err_msg = 'No data accumulated.'
    return
  endif

  old_use_single = self->get( /use_single_return )
  self->set, use_single = 0

  if not obj_valid( self.writer ) or not keyword_set( nocreate ) then begin

    ;  obj_destroy,  self.writer

    checkvar,  n_time, self->getaxis( /ut, /n_els )
    checkvar,  n_energy, self->getaxis( /energy, /n_els )

    self -> fits_create,  err_msg =  err_msg, $
      n_time = n_time, n_energy = n_energy, $
      this_out_filename = this_out_filename, namedialog=namedialog, path=path
    if err_msg ne '' then begin
      self->set, use_single = old_use_single
      return
    endif
  endif

  this_out_filename = self.writer -> get(/filename)

  ; here we know we got a file to write in, and we add data as it gets produced
  self.writer -> write_primary,  image_arr, status = status

  if status eq 0 then begin
    self->set, use_single = old_use_single
    err_msg = 'Aborting file write.'
    message, err_msg, /cont
    return
  endif

  self.writer -> set, /silent
  self.writer -> write_control_extension

  self.writer -> write_info_summary, *self.info_summary

  ;print, energy_index, time_index

  if exist( *self.param_arr ) then begin

    ; full info must be passed from strategy because otherwise it gets into a loop
    ;    full_info = self->get( /full_info, /this_class_only )
    self.writer -> write_info_extension,  *self.param_arr, full_info = full_info

  endif

  if not keyword_set( nocreate ) then obj_destroy, self.writer

  self->set, use_single = old_use_single

  if self->need_update() then $
    message, 'Updated file ' + trim(this_out_filename) + ' with new image.', /info else $
    message, 'Completed file ' + trim(this_out_filename), /info

end

;--------------------------------------------------------------------

function hsi_image_strategy::getdata, _ref_extra = extra, all_images = all_images

  @hsi_insert_catch

  data = self->framework::getdata( _extra = extra )
  if self.use_single_return_mode and not keyword_set( all_images ) then begin
    dim = size( data, /dimension )
    dim2 = n_elements( dim ) ge 3 ? dim[2] : 0
    dim3 = n_elements( dim ) ge 4 ? dim[3] : 0
    return, reform( data[*,*, (self.e_idx)<(dim2-1)>0, (self.t_idx)<(dim3-1)>0 ] )
  endif else return, data

end

;--------------------------------------------------------------------

function hsi_image_strategy::get_sub_struct, struct, tags, ok

  ; this private method returns a subset of the structure
  ; tags = array of tags to return

  ok = 0
  if exist(tags) then begin
    sub_struct = str_subset( struct, $
      tags + '*', $
      status = ok, /regex, /quiet )
  endif else begin
    sub_struct = struct
    ok=1
  endelse

  return, sub_struct

end

;--------------------------------------------------------------------


function hsi_image_strategy::get_info_params, tags

  ; acs 2005-07-06 fix the issue with colliding ok's values. the "ok" in the
  ; loop should not change the value of the global_ok. (global_ok was
  ; earlier just "ok")
  global_ok = 0
  if exist( *self.param_arr ) then begin
    ; needs three-level tests! this will have to change
    ; do nothing if n_elements is 1 because then we
    ; get the info params directly from the framework::get call above
    if is_struct( *self.param_arr ) then begin
      ret_struct = self->get_sub_struct( (*self.param_arr), tags, global_ok  )
    endif else begin
      if n_elements(*((*self.param_arr)[0])) ge 1 then begin
        ; check the local vars
        dims = size( *self.param_arr, /dimension )
        for i=0, dims[0]-1 do begin
          if n_elements( dims ) gt 1 then dims1 = dims[1] else dims1=1
          for j=0, dims1-1 do begin
            ; search for tag names with regexp
            if n_elements( *(*self.param_arr)[i,j] ) ne 0 then begin
              sub_struct = self->get_sub_struct( *(*self.param_arr)[i,j], tags, ok  )
              if ok then begin
                ; we use reproduce to fill the array w/ the reference values
                if i eq 0 and j eq 0 then begin
                  ret_struct = reform( reproduce( sub_struct, dims[0]*dims1 ), $
                    dims[0], dims1 )
                  global_ok = 1
                  ; now we check those that changed
                  ; needs the exist function because sometimes it can be very confused
                endif else if exist( ret_struct ) then begin
                  temp=ret_struct[i,j]
                  struct_assign, sub_struct, temp, /nozero
                  ret_struct[i,j] = temp
                endif
              endif
            endif
            ; here we know that all params are requested
          endfor
        endfor
      endif
    endelse
  endif

  if not exist( ret_struct ) or not global_ok then ret_struct = -1

  return, ret_struct

end

;--------------------------------------------------------------------

function hsi_image_strategy::get, $
  ;                           absolute_time_range= absolute_time_range,  $
  control_only = control_ONLY, $
  e_idx=e_idx, $
  ;                          energy_band = energy_band, $
  found = found, $
  fw_get_id = fw_get_id, $
  info_only = info_only, $
  no_dereference = no_dereference, $
  nosingle = nosingle, $
  not_found = not_found, $
  progress_bar = progress_bar, $
  summary_info=summary_info, $
  ;                          time_range = time_range, $
  t_idx=t_idx, $
  use_single_return_mode=use_single_return_mode, $
  _ref_extra = extra;, $
  ;  !!!!!!!!!!!!kim commented this - axis change- 3-nov-2008 !!!!!!!!!!!!!!!!!!!!!!
  ;                               time_axis = time_axis, energy_axis = energy_axis

  @hsi_insert_catch

  if keyword_set( progress_bar ) then begin
    ; need this to fool the framework
    found = 'PROGRESS_BAR'
    return, self.progress_bar
  endif

  if keyword_set( use_single_return_mode ) then begin
    ; need this to fool the framework
    found = 'USE_SINGLE_RETURN_MODE'
    return, self.use_single_return_mode
  endif

  if keyword_set( summary_info ) then begin
    ; need this to fool the framework
    found = 'SUMMARY_INFO'
    return, ptr_exist(self.info_summary) ? *self.info_summary : -1
  endif

  ; if only t_idx or e_idx set then return their values
  ; need to check the n_elements of extra because extra gets at least
  ; four elements, control_only, info_only, nosingle, n_dereference
  if keyword_set( t_idx ) then begin
    found = 'T_IDX'
    return, self.t_idx
  endif
  if keyword_set( e_idx ) then begin
    found = 'E_IDX'
    return, self.e_idx
  endif

  for i=0, n_elements( extra )-1 do begin
    if stregex( 'time_range', '^' + extra[i], /fold ) ne -1 then yes_time_range = 1
    if stregex( 'energy_band', '^' +extra[i], /fold ) ne -1 then yes_energy_band = 1
    if stregex( 'absolute_time_range', '^' +extra[i], /fold ) ne -1 then yes_abs_tr = 1
  endfor

  ; get the "normal" control parameters
  ; time_range and energy band are a bit special
  ; Actually this gets all params, not just control, but even though we'll get info params via get_info_params below, we can't set /control_only
  ; here, because need to go through framework so found, not_found, etc. will be set properly. kim 7-aug-2013
  ctl_params =  self->framework::get( _extra = extra, $
    found = found, $
    not_found = not_found, $
    time_range = yes_time_range, $
    energy_band = yes_energy_band, $
    /nosingle, $
    info_only = info_only, $
    control_only = control_only, $
    no_dereference = no_dereference, $
    fw_get_id = fw_get_id,  $
    absolute_time_range=yes_abs_tr);, $
  ; acs 2006-12-20 need to specify these following two specially, as they are
  ; now special cases of hsi_image, due to the possibility that these get set from
  ; files. See hsi_image_raw::get for more info.
  ;  !!!!!!!!!!!!kim commented these two  - axis change- 3-nov-2008 !!!!!!!!!!!!!!!!!!!!!!
  ;                                    time_axis = time_axis, $
  ;                                    energy_axis = energy_axis )

  ; dont believe the not_found value here.
  ; this is ok for controls but not for info params
  ; thus we go into param_arr to check out what;s available there

  ; acs 2005-07-27
  ; this has been changed because it was not working for o->get( /info ), which
  ; was returning only the info of the last processed image.
  ;if keyword_set( info_only ) or ( keyword_set( extra ) and not keyword_set( control_only ) ) then begin
  ; if keyword_set( extra )  and ( not keyword_set( control_only ) or keyword_set( info_only ) ) then begin
  if not keyword_set( control_only) then begin  ; kim changed above to this and added next 2 lines, 25-Jan-09
    ;    if keyword_set(energy_axis) then extra = append_arr(extra, 'energy_axis')
    ;    if keyword_set(time_axis)   then extra = append_arr(extra, 'time_axis')
    ; if keyword_set( extra )  and ( not keyword_set( control_only ) or keyword_set( info_only ) ) then begin
    ret_struct=self->get_info_params( extra )
  endif


  if self.use_single_return_mode then begin
    checkvar, t_idx, self.t_idx
    checkvar, e_idx, self.e_idx
    dim = size(ret_struct, /dim )
    dim1 = n_elements( dim ) ge 2 ? dim[1] : 0
    dim0 = n_elements( dim ) ge 1 ? dim[0] : 0
    if dim[0] ne 0 then begin
      t_idx = t_idx < (dim1-1) > 0
      e_idx = e_idx < (dim0-1) > 0
    endif
  endif

  if is_struct( ret_struct ) then begin
    ; we use t_idx to check whether to return all vars or not
    if is_number( t_idx ) then begin
      ret_struct = reform( ret_struct[*,t_idx] )
    endif
    if is_number( e_idx ) then begin
      ret_struct = reform( ret_struct[e_idx<(dim[0]-1),*] )
    endif
    ;    ret_struct = str_top2sub( ret_struct )

    if not exist( ret_strr ) then begin
      ; this can happen if we deal with old files
      ret_strr = ret_struct
    endif else begin
      tagnames = tag_names( ret_struct )
      for i=0, n_elements( tagnames )-1 do begin
        ret_strr = rep_tag_value( ret_strr, ret_struct.(i), tagnames[i] )
      endfor
    endelse

    ret_strr = str_top2sub( ret_strr )

    ; If ctl_params (from framework::get of all params requested, not just control - bad name) is a structure, and
    ; ret_strr (info params requested) has only one element, then merge them (the tags in ret_strr that are
    ; identical to those we already got in ctl_params will be set to the values in ret_strr).  If ret_strr has
    ; more than one element, then we're returning info params for multiple images - don't merge with ctl_params
    ; (means if user asked for a control and an info param, e.g. image_dim and vf_niter), the control param won't
    ; be returned, only vf_niter for all images requested)

    if is_struct(ctl_params) and n_elements( ret_strr ) eq 1 then begin
      ; One special case we need to handle. If getting just one parameter and the parameter is a structure,
      ; then ctl_params is a structure with the fields of the param structure, but ret_strr is a structure
      ; that contains a structure containing those fields (i.e. one level down), so the end result of the
      ; joining was a structure containing both, e.g. get(/decim_table) returned a structure with these
      ; 3 tags: decim_table (which is a structure containing front and rear), front, and rear.  Kim corrected
      ; this via the test below to see if the first tag in ret_strr is a structure matching ctl_params. 8-aug-2013
      ;
      if is_struct(ret_strr.(0)) && match_struct(ctl_params, ret_strr.(0), /tags_only) then $
        ret_strr = join_struct( ret_strr.(0), str_top2sub( ctl_params) ) $
      else $
        ret_strr = join_struct( ret_strr, str_top2sub( ctl_params) )
    endif

  endif else ret_strr = ctl_params

  ; go there only if we have more than one image. otherwise use the
  ; value from hsi_image_single.
  if is_struct( ret_strr ) then begin

    if tag_exist( ret_strr, 'energy_band' ) then begin
      ; first get the real values
      ; get the energy band from the energy array
      ; acs 2008-7-9 "axis" is not used any more, now we use im_energy_binning
      ;        ebin = self->getaxis( /energy, /edges_2 )
      ebin = get_edge_products( self->framework::get( /im_energy_binning ), /edges_2 )
      ; check if we want just one single range
      if is_number( e_idx ) then begin
        ebin = ebin[*,e_idx < (n_elements(ebin[0,*])-1)]
      endif
      ret_strr = rep_tag_value( ret_strr, ebin, 'ENERGY_BAND' )
    endif

    ; same for time as for energy, filter out the range to pass
    if tag_exist( ret_strr, 'time_range' ) or $
      tag_exist( ret_strr,  'absolute_time_range' ) or $
      tag_exist( ret_strr, 'im_time_interval' ) then begin

      ; acs 2008-08-02 this disappears now as the time binning is not in an
      ; axis object any more.
      ;      tbin = self -> getaxis( /ut, /edges_2 )
      tbin = GET_EDGE_PRODUCTS( self->framework::get( /im_time_interval ), /edges_2 )

      ;      if not valid_range( tbin[*,0] ) then begin
      ;           ;;; seems the time axis is not set correctly, probably
      ;           ;;; because of eventlist files. try to fix that...
      ;           obs_time=self->framework::get( /obs_time )
      ;           if valid_range( obs_time ) then begin
      ;               self->set, im_time_int = obs_time
      ;               ret_strr = rep_tag_value( ret_strr, obs_time, 'IM_TIME_INTERVAL', /no_add, /quiet )
      ;           endif
      ;           ;;; ok now can continue
      ;       endif
      if is_number( t_idx ) then begin
        tbin = tbin[*, t_idx < (n_elements(tbin[0, *])-1)]
      endif
      if tag_exist( ret_strr, 'time_range' ) then begin
        if ret_strr.time_range[0] gt 1e6 or not valid_range( ret_strr.time_range ) then begin
          ret_strr = rep_tag_value( ret_strr, tbin, 'TIME_RANGE' )
        endif
      endif
      ; in some case tbin is undef but absolute time range is defined
      ; (e.g. visibility files) -- just fort he first call
      if tag_exist( ret_strr, 'absolute_time_range' ) and valid_range( tbin[*,0] ) then begin
        ret_strr = rep_tag_value( ret_strr, tbin, 'ABSOLUTE_TIME_RANGE' )
      endif
    endif

  endif

  ; at some point needs to install nosingle kwd he-re too ... mmm not sure
  ; Add check for struct for Version 8.0, Aug 12, 2010
  if is_struct(ret_strr) && n_tags( ret_strr ) eq 1 then ret_strr = ret_strr.(0)
  return, ret_strr


end

;--------------------------------------------------------------------

;pro hsi_image_strategy::getdata, map = map, energy_index = energy_index, $
;	time_index = time_index

;checkvar, energy_index, 0
;checkvar, time_index, 0

;end

;--------------------------------------------------------------------

pro hsi_image_strategy::set, $
  done = done, $
  e_idx = e_idx, $
  full_info=full_info,  $
  image_algorithm=image_algorithm,  $
  im_time_interval = im_time_interval, $
  im_time_bin = im_time_bin, $
  not_found = not_found, $
  obs_time_interval = obs_time_interval, $
  progress_bar = progress_bar, $
  t_idx = t_idx, $
  eb_index = eb_index, $
  use_single_return_mode=use_single_return_mode, $
  _extra = _extra


  ; now the real cases
  ; !!!!!!!!!!!!kim commented axis stuff in this block - no time_axis - 3-nov-2008 !!!!!!!!!!!!!!!!!!!!!!
  if exist( im_time_interval ) then begin
    ;
    if is_string( im_time_interval ) then im_time_interval = anytim( im_time_interval )
    ; lets do some input test, but set im time interval only if we are not
    ; doing this the old way, otherwise we're all set anyway at this point
    if n_elements( im_time_interval ) gt 1 then begin
      ; 1 element is invalid
      ;        o = self -> framework::get( /time_axis, /this_class_only )
      ;        o ->set, time_range = im_time_interval
      ; we do a get to take advantage of the anytim operation in
      ; time_axis__define and we set obs_time_interval such that we can
      ; prepare the files to read already.
      if not self.yes_old_way then begin
        ;            obs_time_interval = minmax( o->get( /time_edges ) )
        obs_time_interval = minmax(im_time_interval )
      endif

    endif else begin
      message, 'im time interval does not contain a valid range ', /info
      return
    endelse

  endif
  ;
  ;if is_number( eb_index ) then begin
  ;    ;stop
  ;endif
  ;
  ;if is_number( im_time_bin ) then begin
  ;    time_axis = self -> framework::get( /time_axis, /this_class )
  ;    time_axis -> set, time_resolution = im_time_bin
  ;endif

  if is_number( progress_bar ) then self.progress_bar = progress_bar

  if is_number( t_idx ) then self.t_idx = t_idx
  if is_number( e_idx ) then self.e_idx = e_idx

  if is_number( use_single_return_mode ) then $
    self.use_single_return_mode = use_single_return_mode

  ; full_info should not generate reprocessing
  if exist( full_info ) then self->framework::set,  full_info =  full_info,  /no_update

  ; unlike in hsi_image_single, here if the imaging alg changes we need
  ; to reprocess -- as we may have lost intermediate data products.
  if keyword_set( image_algorithm ) then begin ;;;;;;;;;;check this
    imalg =  self->framework::get( /image_alg )
    ;  !!!!!!!!!!!!kim commented these two and inserted next two - axis change- 3-nov-2008 !!!!!!!!!!!!!!!!!!!!!!
    ;  n_t = N_elements( (self->framework::get( /time_axis, /this_class ))->get( /edges_2 ) )
    ;  n_e = N_Elements( (self->framework::get( /energy_axis, /this_class ))->get( /edges_2 ) )
    n_t = self -> getaxis(/ut, /n_els)
    n_e = self -> getaxis(/energy, /n_els)

    if not same_data( imalg,  image_algorithm ) then begin
      if n_t gt 2 then self->set,/need $
        ;;; need to reprocess down to the calib eventlist, but not to the
        ;;; binned eventlist, if we have only one time int and several energy bands
        ;;; If it's a visibility alg and the time and energy bins haven't changed then
        ;;; we don't want to reprocess the calib_eventlist.
      else if n_e gt 2 then begin
        calib= self->get(/obj,class='hsi_calib_eventlist')
        calib->set, /need, class = 'hsi_calib_eventlist', /this_class
      endif
      ;;; ... and if n_t = 1 and n_e = 1 then do nothing.
    endif
  endif

  ;ptim, obs_time_interval, im_time_interval
  self->framework::set, $
    eb_index = eb_index, $
    image_algorithm=image_algorithm,  $
    im_time_interval=im_time_interval, $
    im_time_bin=im_time_bin, $
    obs_time_interval=obs_time_interval, $
    _extra=_extra, done=done, not_found=not_found

end

;--------------------------------------------------------------------
;--------------------------------------------------------------------

pro hsi_image_strategy::plot_old, p_e_idx, p_t_idx, $
  energy_index = energy_index, $
  time_index = time_index, $
  saved_data = saved_data, $
  _ref_extra = extra

  ; only set extra if we're not using saved_data
  ;if keyword_set( extra ) then self->set, _extra = extra

  if not keyword_set(saved_data) then begin

    data = self->getdata(_extra = extra)
    dims = size( data, /struct )

    if dims.n_dimensions gt 2 then begin

      if is_number( p_e_idx ) then energy_index = p_e_idx < (dims.dimensions[2]-1)
      if is_number( p_t_idx ) then time_index = p_t_idx < (dims.dimensions[3]-1)

      checkvar, time_index, dims.dimensions[3]/2
      checkvar, energy_index, dims.dimensions[2]/2

      data = data[*,*,energy_index,time_index]

    endif

    obj = self

  endif

  ; we will work on that later
  hsi_image_plot, data, obj = obj, saved_data = saved_data, $
    _extra = extra,  energy_idx = energy_index, time_idx = time_index

end

;----------------------------------------------------------------------------------

pro hsi_image_strategy::plot, p_e_idx, p_t_idx, $
  energy_index = energy_index, $
  time_index = time_index, $
  saved_data = saved_data, $
  _ref_extra = extra

  ; only set extra if we're not using saved_data
  ;if keyword_set( extra ) then self->set, _extra = extra

  if not keyword_set(saved_data) then begin

    old_use_single = self->get( /use_single )
    self->set, use_single = 1

    if is_number( p_e_idx ) or is_number( energy_index ) then begin
      if is_number(p_e_idx) then energy_index = p_e_idx
      old_e = self->get( /e_idx )
    endif

    if is_number( p_t_idx ) or is_number( time_index ) then begin
      if is_number( p_t_idx ) then time_index = p_t_idx
      old_t = self->get( /t_idx )
    endif

    self->set, t_idx = time_index
    self->set, e_idx = energy_index

    data = self->getdata()

    ; this is to avoid to pass self if saved data is used
    obj=self

  endif

  ; we will work on that later
  hsi_image_plot, data, obj = obj, saved_data = saved_data, $
    _extra = extra, energy_idx = energy_index, time_idx = time_index

  if not keyword_set( saved_data ) then begin
    if is_number( energy_index ) then self->set, e_idx = old_e
    if is_number( time_index ) then self->set, t_idx = old_t
    self->set, use_single = old_use_single
  endif


end

;--------------------------------------------------------------------

pro hsi_image_strategy::slicer, time = time, energy = energy

  old_use_single =  self -> get( /use_single )
  self -> set,  use_single =  0
  data = self->getdata()
  self -> set, use_single=old_use_single

  if keyword_set( time ) then begin
    slicer3, ptr_new( reform( (*self.data)[*,*,0,*] ) )
  endif else begin
    slicer3, ptr_new( reform( (*self.data)[*,*,*,0] ) )
  endelse

end
;--------------------------------------------------------------------

; Method to make xinteranimate, mpeg, or javascript movie from hessi image cube.
;
; Keywords:
; time - If set, make movie in time dimension (default)
; energy - If set, make movie in energy dimension
; integrate - If set, sum all images in non-movie dimension (note exception below)
; t_idx - time indices to use
; e_idx - energy indices to use
;   Note: if multiple indices are specified in non-movie dimension, they are summed
;   even if integrate isn't set.
; mpeg - If set, make mpeg movie.  (used with file_mpeg keyword)
; file_mpeg - name of output mpeg file if mpeg is set (def = 'hsi_imagemovie.mpeg')
; java - If set, make a javascript movie. (used with dir_java keyword)
; java_dir - Directory to make jpegs and java movie in (def is 'java_dir')
;    Name of jpeg files is hsi_imagexx.jpeg, and movie is hsi_image_moive.html
; size - [x,y] size of movie in screen pixels
; rgb - [256,256,256] red,green,blue color
; table - number of color table to use
; status - Output status.  0 = failure, 1 = success
; _extra - keyword to pass through to movie-making routines, e.g.
;    noscale - If set, don't scale to min/max of all images
;    log - If set, show images on log scale
;    any other plot_map keyword
;
; Example: o->movie
;               make xinteranimate movie of first energy band through all time intervals
;          o->movie, /energy, t_idx=[3,4,5]
;               make xinteranimate movie of sum of time intervals 3,4,5 through all energy bands
;          o->movie, /time, t_idx=[3,4,5], e_idx=2, /mpeg
;               make mpeg movie of energy band #2 through times intervals 3,4,5
;          o->movie, /energy, /integrate, /java, dir_java='12feb2005', table=4, size=[300,300]
;               make java movie of the sum of all time intervals through all energy bands
;               in directory '12feb2005' under current directory using color table 4
;

pro hsi_image_strategy::movie, time = time, energy = energy,  $
  integrate =  integrate,  t_idx =  t_idx,  e_idx =  e_idx, $
  mpeg=mpeg, file_mpeg=file_mpeg, $
  java=java, dir_java=dir_java, $
  size=size, rgb=rgb, table=table, status=status, _extra=_extra

  status = 1

  old_use_single =  self -> get( /use_single )
  self -> set,  use_single =  0

  data = self->getdata()
  ; want to use the dimension tag even if

  pixel_size = self -> get( /pixel_size )
  dx = pixel_size[0] &  dy =  pixel_size[1]

  xyoffset =  self -> get( /xyoffset )
  xc =  xyoffset[0] &  yc =  xyoffset[1]

  ;  time_axis = self->get( /time_axis )
  ;  dur =  time_axis->get( /width )
  ;  tt =  time_axis->get( /edges_2 )
  ;  nt = time_axis->get( /n_els )
  ;
  ;  energy_axis = self -> get( /energy_axis )
  ;; eb =  energy_axis -> get( /edges_2 )
  ;;
  ;  n_e = energy_axis->get( /n_els )

  tt = self->get(/im_time_int)
  tt = get_edges(tt,/edges_2)

  dur = get_edge_products(tt, /width)
  nt = n_elements(dur)

  eb = self->get(/im_energy)
  n_e = n_elements(eb[0,*])

  if keyword_set( energy ) then begin
    ; movie is in energy dimension.  e_idx are indices to loop through.
    if n_elements(e_idx) eq 0 then e_idx = indgen(n_e)
    if n_elements(e_idx) gt 1 then begin
      eb = eb[*,e_idx]
      if keyword_set( integrate ) or n_elements(t_idx) gt 1 then begin
        if n_elements(t_idx) eq 0 then t_idx = indgen(nt)
        data =  total( data[*,*,e_idx,t_idx],  4 )
        dur = total( dur[t_idx] )
        tt =  tt[0, t_idx[0]]
      endif else begin
        checkvar,  t_idx,  self.t_idx
        data =  reform( data[*, *, e_idx,  t_idx] )
        dur =  dur[t_idx]
        tt =  tt[0, t_idx]
      endelse
    endif else begin
      message, 'Cannot make a movie in energy with a single image', /cont
      self->set, use_single = old_use_single
      status = 0
      return
    endelse
  endif else begin
    ; movie is in time dimension.  t_idx are indices to loop through
    if n_elements(t_idx) eq 0 then t_idx = indgen(nt)
    if n_elements(t_idx) gt 1 then begin
      tt = tt[*,t_idx]
      if keyword_set( integrate ) or n_elements(e_idx) gt 1 then begin
        if n_elements(e_idx) eq 0 then e_idx = indgen(n_e)
        data =  total( data[*,*,e_idx,t_idx],  3 )
        eb =  minmax(eb[*,e_idx])
      endif else begin
        checkvar,  e_idx, self.e_idx
        data =  reform( data[*, *,  e_idx,  t_idx] )
        eb =  eb[*, e_idx]
      endelse
    endif else begin
      message, 'Cannot make a movie in time with a single image', /cont
      self->set, use_single = old_use_single
      status = 0
      return
    endelse
  endelse

  n_map =  n_elements( data[0, 0, *] )
  ;  mmax =   max( max( data,  dimension =  1 ),  dim =  1)
  ;  for i = 0, n_map-1 do data[*, *, i ] =  data[*, *, i]/ mmax[i]

  ; because we've specified t_max and e_max above, we're either looping
  ; through the first or the second loop
  for i = 0, n_map -1 do begin
    ; generate the first map

    if keyword_set( energy ) then begin
      time_idx =  0
      energy_idx = i
    endif else begin
      time_idx =  i
      energy_idx = 0
    endelse

    this_map = make_map( reform( data[*, *, i]), $
      dx = dx, dy = dy, xc = xc, yc = yc, roll_center = [0, 0], $
      time = anytim( tt[0,time_idx], /tai), dur = dur[time_idx])
    add_prop, this_map, e_low = eb[0, energy_idx]
    add_prop, this_map, e_high = eb[1, energy_idx]

    f1 = (fix(this_map.e_low) eq this_map.e_low) ? '(f12.0)' : '(f12.1)'
    f2 = (fix(this_map.e_high) eq this_map.e_high) ? '(f12.0)' : '(f12.1)'

    this_map.id = trim(this_map.e_low,f1) + ' - ' + trim(this_map.e_high,f2) + ' keV,'

    if i eq 0 then h =  replicate( this_map,  n_map )

    h[i] =  this_map

  endfor

  tvlct, rsave, gsave, bsave, /get
  case 1 of
    keyword_set(rgb): tvlct, rgb[*,0], rgb[*,1], rgb[*,2]
    keyword_set(table): loadct, table, /silent
    else: loadct, 5, /silent
  endcase

  case 1 of
    keyword_set(mpeg): begin
      checkvar, file_mpeg, 'hsi_imagemovie.mpeg'
      map2mpeg, h, file_mpeg, /durtitle, size=size, _extra=_extra, status=status
    end

    keyword_set(java): begin
      checkvar, dir_java, 'java_dir'
      if not file_test(dir_java, /directory) then file_mkdir, dir_java
      map2jpeg, h, prefix=dir_java+'/hsi_image', /durtitle, $
        size=size, _extra=_extra, status=status
      if not status then goto, cleanup
      jsmovie, concat_dir(dir_java,'hsi_image_movie.html'), $
        concat_dir(dir_java,'jpeg_files.txt'), /range, status=status
    end

    else: begin
      if keyword_set(size) then begin
        xsize=size[0]
        ysize=size[1]
      endif
      movie_map, h, rate=10, /durtitle, xsize=xsize, ysize=ysize,abort=abort,  _extra=_extra
      if abort then status=0
    end

  endcase

  cleanup:
  tvlct, rsave, gsave, bsave
  self -> set,  use_single =  old_use_single

end

;----------------------------------------------------------

;pro hsi_image_strategy::panel_display,  _ref_extra =  extra, $
;                      t_idx = t_idx, e_idx = e_idx, done = done, $
;                      zoom = zoom, selected_images = selected_images
;
;hsi_panel_display,  self, t_idx = t_idx, e_idx = e_idx,  $
;                    _extra= extra, done = done, $
;                    selected_images = selected_images
;
;if done then return
;
;if keyword_set( zoom ) then begin
;    repeat begin
;        self->set, t_idx = t_idx, e_idx = e_idx
;        self->plot
;        hsi_panel_display,  self, t_idx = t_idx, e_idx = e_idx,  _extra= extra, done = done
;    end until done
;endif
;
;end

;============================================================================
;+
; PROJECT:  HESSI
;
; NAME:  hsi_image__getaxis
;
; PURPOSE:  function method for hsi_image object to return x, y axes
;
; CATEGORY: HESSI
;
; CALLING SEQUENCE:  obj -> getaxis()
;
; INPUTS:  none
;
; OPTIONAL INPUTS (KEYWORDS):
;	any keywords to set in the hsi_image object
;	xaxis - if set, return x axis values   (default)
;	yaxis - if set, return y axis values
;	mean - if set, return arithmetic mean of boundaries  (default)
;	gmean - if set, return geometric mean
;	width - if set, return absolute difference between upper and lower edges
;	edges_2 - if set, return 2xn array of edges [lo(i), hi(i)], etc.
;	edges_1 - if set, return array of n+1 edges of n contiguous channels
;
;
; OUTPUTS:  array of axis values in form specified by user.  Default is array of
;	centers of each pixel.
;
; OPTIONAL OUTPUTS:  None
;
; Calls:
;
; COMMON BLOCKS: None
;
; PROCEDURE:
;
; RESTRICTIONS: None
;
; SIDE EFFECTS: None.
;
; EXAMPLES:
;
; HISTORY:
;	Written Kim, Andre May 8, 2001
;	Modifications:
;
;-
;============================================================================

function hsi_image_strategy::axis_kwds, data, _extra = _extra, n_els = n_els

  ; private helper function to simulate the old object axis' keywords

  ; For n_els, first convert to /edges_1 format
  if keyword_set( n_els ) then return, n_elements( get_edge_products(data,/edges_1) ) -1
  if keyword_set( _extra ) then return, get_edge_products( data, _extra = _extra )
  ; if nothing specified, return mean
  return, get_edge_products( data, /mean )

end

;----

FUNCTION HSI_Image_strategy::GetAxis, $
  XAXIS=xaxis, YAXIS=yaxis, $
  ut = ut, energy=energy, $
  used_position=used_position, $
  _EXTRA=extra

  ; acs 2008-08-02 this should not be needed now that we have elimintaed
  ; the axis objects. Thus we fake it such that we dont need to change
  ; the code everywhere. but we need to simulate the keywords such as
  ; n_els in axis_kwds


  ;self->Set,_EXTRA = _extra

  ; times arr and energy_array are shortcuts to the binnings
  if keyword_set( UT ) then begin
    ; acs 2008-08-02 this is what has been turned off
    ;    tbin = self -> framework::get( /time_axis, /this_class, /edges_2
    ;    )

    tbin = self->framework::get( /im_time_interval )

    if not valid_range( tbin[0:1] ) then begin
      ;;; seems the time axis is not set correctly, probably
      ;;; because of eventlist files. try to fix that...
      obs_time=self->framework::get( /obs_time )
      if valid_range( obs_time ) then begin
        self->set, im_time_int = obs_time
        tbin = obs_time
      endif
    endif

    return, self->axis_kwds( tbin, _extra = extra )

    ;    time_axis = self -> framework::get( /time_axis, /this_class )
    ;    IF obj_valid( time_axis ) then begin
    ; make sure it is defined:
    ;        return,  time_axis -> get( _extra = extra  )
    ;    endif else return, -1
  endif

  if keyword_set( energy ) then begin

    ebin =  self->framework::get( /im_energy_bin )
    return, self->axis_kwds( ebin, _extra = extra )


    ; acs 2008-08-02
    ;    energy_axis = self -> framework::get( /energy_axis )
    ;    return,  energy_axis -> get( _extra = extra )
  endif

  xaxis = Keyword_Set( XAXIS )
  yaxis = Keyword_Set( YAXIS )
  if xaxis + yaxis eq 0 then xaxis = 1

  image_dim = self->Get( /IMAGE_DIM )
  pixel_SIZE = self->Get( /PIXEL_SIZE )
  pixel_scale = self->Get( /PIXEL_SCALE )
  xyoffset = keyword_set(used_position) ? self->get(/used_xyoffset) : self->Get( /XYOFFSET )
  ;RAS, 1-jun-2017, removed kluge for mem_njit offset problem with even pixels, fixed in hsi_mem_njit::mem
  ;    class = hsi_get_alg_name( self->Get(/image_algorithm), /object_class)
  ;    offset = (class eq 'HSI_MEM_NJIT' ) and ((image_dim[0] mod 2) eq 0)? 0.5 : 0.0
  ;    xyoffset = xyoffset- pixel_scale[0] * pixel_size[0] * offset

  ; return centers of pixels unless another option was selected in _extra
  return, hsi_compute_image_axis (xyoffset, image_dim, pixel_size, pixel_scale, $
    xaxis=xaxis, yaxis=yaxis, _extra=extra)


END

;----------------------------------------------------------

PRO hsi_image_strategy__define

  self = {hsi_image_strategy, $
    writer: obj_new(), $
    param_arr: ptr_new(),  $
    info_summary: ptr_new(), $
    subset_info_tags: ptr_new(), $
    yes_old_way: 0B, $
    use_single_return_mode: 0B, $
    t_idx:0, e_idx:0, $
    INHERITS framework }

END


;---------------------------------------------------------------------------
; End of 'hsi_image_strategy__define.pro'.
;---------------------------------------------------------------------------
