;============================================================================
; Time-stamp: <Fri Dec 17 2004 10:42:17 csillag sundry>
;+
; PROJECT:  HESSI
;
; NAME:  hsi_image_plot
;
; PURPOSE:  procedure to plot and label HESSI image,  Called from hsi_image__plot.
;
; CATEGORY: HESSI
;
; CALLING SEQUENCE:  hsi_image_plot, image, control, info, alg, _extra=_extra
;
; INPUTS:
;   image - 2d image array
;   control - control structure from image object
;   info - info structure from image object
;   alg - string, image algorithm name
;
; INPUT KEYWORDS:
;	drange - data range to plot (only upper value is used if using a hessi color)
;	rescale_image - 0/1 means don't / do rescale color range for zoomed-in plot (i.e. if
;	  0, use entire plot for color scaling, if 1 only use zoomed-in portion for color scaling)
;	show_atten - 0/1 means don't / do show atten state in legend of plot (on detector line)
;       _extra - any other parameters to be passed into plot_map
;
; OUTPUTS:  Plots image in currently selected window
;
; OPTIONAL OUTPUTS:  None
;
; Calls:
;
; COMMON BLOCKS: None
;
; PROCEDURE:
;
; RESTRICTIONS: None
;
; SIDE EFFECTS: None.
;
; EXAMPLES:
;
; HISTORY:
;	Written Kim,  Feb 2001.  This used to be part of hsi_image__plot, but was extracted into a
;      separate routine so that hsi_bproj__plot could use it too.
;
; MODIFICATIONS:
;   Mar-15-2001, Kim.  For total counts label, only total binned_n_event for collimators used (some
;      algorithms - mem sato and mem vis - set values for binned_n_event for collimators not used)
;	Apr-12-2001, Kim.  Added status and err_msg keywords
;   3-Aug-2001, Kim.  Added passing image_units via cb_title keyword to plot_map.
;   Dec-13-2001, Kim.  Previously made label size .8 of charsize. Now = charsize.
;   20-Feb-2002, Kim.  Increased time label size.
;	10-Jul-2002, Kim.  Use hsi_get_time_range function instead of info.absolute_time_range
;	1-Aug-2002, Kim.  Added a check for _extra.label_size.  If it's there, will override charsize
;	22-Jan-2003, Kim.  Changed HESSI to RHESSI in plot title
;	3-Oct-3002, Kim.  Added show_atten keyword
;	6-Nov-2003, Kim.  Use drange, rescale_image keywords explicitly, instead of in _extra.
;	  If drange keyword is set, then when figuring out min,max for HESSI colors (which
;	  is CENTERED on zero), don't use max of data, use drange[1] to define [-max, max]
;	  range for image
;   10-jun-2004 andre, changes to make it work with multiple images
;   9-Dec-2005 Kim.  Use used_xyoffset, not xyoffset.  For cubes, xyoffset used for
;     each image can change if use_flare_xyoffset is set and times span multiple flares.
; 15-Jan-2008, Kim. Don't print total counts for vis algs - it's wrong for multiple images
; 18-Jun-2008, Kim.  Use map object instead of making map structure and calling plot_map.
; 05-Aug-2009, Kim.  Destroy map_obj after plotting.  Memory leak.
; 08-Nov-2009, Kim. Changed map_obj->setmap to map_obj->set because of a change in the map object.
; 10-Oct-2011, Kim. Call al_legend instead of legend (IDL 8. name conflict) and ensure textcolor is byte
; 08-Nov-2011, Kim. Fix bug in 10-Oct change (make textcolor byte when first set in case doesn't exist later)
; 07-Aug-2012, Kim. Use ssw_legend (same as original legend) instead of al_legend - too many problems with al_legend
; 09-Apr-2013, Kim. When obj passed in, use im_eband_used for energy for label if available. Otherwise energy_band.
; 29-Jul-2015, Kim. map obj now takes care of timestamp, so take no_timestamp out of explicit keyword list, it's in _extra
; 09-Feb-2016, Kim. When doing overlay ('ov' is in extra, and is set to 1), don't put legend on plot
; 17-Jul-2018, Kim. Previously couldn't write total_counts for vis algs, but now binned_n_event is saved, so can
;
;-
;============================================================================

pro hsi_image_plot, image, control, info, alg, $
                    drange=drange, $
;                    energy_idx = energy_idx, time_idx = time_idx, $
                    rescale_image=rescale_image, $
                    show_atten=show_atten, $
                    status=status, $
                    xtitle = xtitle, $
                    ytitle = ytitle, $
                    err_msg=err_msg, $
                    obj=obj, $
                    saved_data = saved_data, $
;                    no_timestamp = no_timestamp, $
                    _extra=_extra

;checkvar, energy_idx, 0
;checkvar, time_idx, 0

;protect keywords from returning changed
if exist(drange) then data_range = drange
if exist(rescale_image) then rescale = rescale_image

if keyword_set( obj ) then begin

    xyoffset = obj->get( /used_xyoffset )
    pixel_size = obj->get( /pixel_size )
    time_interval = obj->get( /absolute_time_range )
    atten_state = obj->get( /image_atten_state )
    if atten_state ne -1 then atten_state = trim(atten_state)
    cb_title = obj->get( /image_units )
    if not is_string( cb_title ) then cb_title = ''

endif else begin
    if not exist( control ) then begin
        image = *saved_data.data
        control = *saved_data.control
        info = *saved_data.info
    endif
    xyoffset = info.used_xyoffset
    pixel_size = control.pixel_size
    time_interval = info.absolute_time_range
    atten_state = tag_exist( info, 'image_atten_state') ? trim(info.image_atten_state) : -1
    cb_title = tag_exist(info,'image_units',/quiet) ? info.image_units : ''
endelse


; for color bar units, use image_units if exists - older quicklook images don't have it.
if is_string(cb_title) then cb_title = '(' + cb_title + ')'

; create map structure that plot_map expects to see

;map =  make_map( image, $
;                 xc=xyoffset[0], $
;                 yc=xyoffset[1], $
;                 dx=pixel_size[0], $
;                 dy=pixel_size[1], $
;                 time=anytim(time_interval[0],/tai) )

; changed 18-jun-08 to make map object instead - more flexible if map params change
map_obj = obj_new('map')
map_obj -> set, data=image, $
                 xc=xyoffset[0], $
                 yc=xyoffset[1], $
                 dx=pixel_size[0], $
                 dy=pixel_size[1], $
                 time=anytim(time_interval[0],/tai)

checkvar, xtitle, 'Heliocentric X (arcsec)'
checkvar, ytitle, 'Heliocentric Y (arcsec)'
checkvar, title, 'Reconstructed RHESSI Image'

if is_hessi_color (_extra=_extra) then begin
	;print,'Using a hessi color, so scaling is symmetric around 0.0'
	maxval = max(abs(image))
	if exist(data_range) then if data_range[1] gt 0. then maxval = maxval < data_range[1]
	data_range = [-maxval, maxval]
	; make sure plot_map doesn't ever rescale the image when using hessi colors.
	; This seems backward, but if rescale_image = 0 then plot_map will use full range of image to scale instead
	; of dmin and dmax, so set rescale_image to 1, but pass in dmin,dmax to force it to use dmin,dmax in zoom
	rescale = 1
endif

if tag_exist(_extra, 'mark_point', /quiet) then if _extra.mark_point eq 1 then begin
	p = (hsi_qlook_pointing(obs_time_interval=time_interval)) -> getdata(/de_bytescale)
	axis = [ mean(p[0,*]), mean(p[1,*]) ]
	_extra = rep_tag_value (_extra, axis, 'mark_point')
	message, 'Pointing axis=' + arr2str(trim(axis)) + ' arcsec', /cont
endif

;Plot_Map, map, $
map_obj -> plot, $
	_EXTRA=_extra, drange=data_range,  rescale_image=rescale, $
	xtitle=xtitle, ytitle=ytitle, title=title, cb_title=cb_title, $
	status=status, err_msg=err_msg

obj_destroy, map_obj

if not status then return

; put labels on image

label_size = ch_scale(.8, /xy)
color = !p.color
time_size = ch_scale(.8, /xy)
legend_loc = 1
if keyword_set(_extra) then begin
  tags = tag_names(_extra)
  ovtag = grep('ov', tags, index=index)
  if ovtag  eq '' || _extra.(index[0]) eq 0 then begin  ; if doing an overlay, don't add legend
    if tag_exist (_extra, 'charsize') then  label_size = _extra.charsize
    if tag_exist (_extra, 'label_size') then  label_size = _extra.label_size
    if tag_exist (_extra, 'charsize') then time_size = _extra.charsize * .8
    if tag_exist (_extra, 'legend_loc') then legend_loc = _extra.legend_loc
    if tag_exist (_extra, 'legend_color') then textcolor = byte(_extra.legend_color)
  endif else legend_loc = 0
endif

if legend_loc ne 0 then begin
   top = legend_loc lt 3
   bottom = legend_loc ge 3
   right = (legend_loc mod 2) eq 0
   left = legend_loc mod 2

   if keyword_set( obj ) then begin
       det = obj->get( /det_index_mask )
; not active yet
;       a2d = reform (obj->get( /a2d_index_mask) , 9, 3)
       a2d = bytarr(9,3) + 1B  ; ?
       front = obj->get( /front_segment )
       rear = obj->get( /rear_segment )
       enb = obj->get( /im_eband_used )
       if enb[0] eq -1 or total(enb) eq 0. then enb = obj->get(/energy_band)
       n_event = obj->get( /binned_n_event )
       alg = obj->get( /image_algorithm )
       vis_type = obj->get( /vis_type )
   endif else begin
       det = control.det_index_mask
       a2d = reform (control.a2d_index_mask, 9, 3)
       front = control.front_segment
       rear = control.rear_segment
       enb = -1
       if tag_exist(info, 'im_eband_used') then $
          if total(info.im_eband_used) gt 0 then enb = info.im_eband_used
       if enb[0] eq -1 then enb = control.energy_band
       n_event = info.binned_n_event
       ; image alg not in control if we're coming from hsi_bproj::plot. get alg in args.
       if tag_exist(control, 'image_algorithm') then alg = control.image_algorithm
       if tag_exist(control, 'vis_type') then vis_type = control.vis_type else vis_type = 'photon'
   endelse

	alg = hsi_get_alg_name (alg) ; convert to proper full name for legend
	yes_vis = (hsi_alg_units(alg)).is_vis
;	yes_vis = is_member (alg, (hsi_alg_units( /vis_only )).name )
	detector_list = hsi_coll_segment_list(det, a2d, front, rear, colls_used=colls_used)

	fm = '(f12.1)'
        en_string = strtrim ( string(enb[0],format=fm), 2) + $
                    ' - ' + strtrim ( string(enb[1],format=fm), 2)

	fm = '(g12.3)'
	total_events = strupcase (strtrim (string(total(n_event[where(colls_used),0]), format=fm),2))
	total_str = total_events gt 0.? 'Total counts: ' + str_replace (total_events, 'E+00', 'E+0') : ''
	if yes_vis then begin
	   ;previously replace total_str with '', since vis algs didn't have, but now they do
	  vis_type = hsi_vis_type(vis_type, /full)
	  vis_str = ' ('+vis_type+' vis)'
	endif else vis_str = ''

  if keyword_set( show_atten ) and is_string( atten_state ) then $
	st_atten_state = ', Att ' + atten_state else st_atten_state = ''

	text = [anytim(time_interval[0], /vms) + ' to ' +  anytim(time_interval[1], /vms,/time), $
		'Detectors: ' + detector_list + st_atten_state, $
		'Energy Range: ' + en_string + ' keV', $
		alg + '    ' + total_str + '  ' + vis_str]

	ssw_legend, text, box=0, charsize=label_size,$
		top_legend=top, bottom_legend=bottom, right_legend=right, left_legend=left, $
		textcolor=textcolor

endif

;if not keyword_set( no_timestamp ) then timestamp, /bottom, charsize=time_size

END
