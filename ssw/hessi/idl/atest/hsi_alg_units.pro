
;+
;Name: HSI_ALG_UNITS

;Purpose: Give the units for all the accepted imaging algorithms and classes
;
;a = {hsi_alg_units, $
;	class:'',$  ;alg obj class name, i.e. HSI_XXX, eg HSI_BPROJ
;	name:'',$	;Descriptive name, eg Back Projection
;
;	nknme1:'',$ ;Display Name, "Back Projection"
;	nknme2:'',$ ;2nd Shorthand name, eg. VF
;
;	units:'', $ ;eg. 'Counts sc!u-1!n' for CLEAN
;    is_vis: 0B,$ ; acs: to check whether ths alg uses annsec or visibility based algorithm
;	prefix:'', $

;	order: 0, $ ;listing order
;	active: 0 }, $ 1 for active, 0 for inactive} ;prefix used in init; added March 2011, ras
;string pre-pended to alg control parameters

;alg_list = [ 'CLEAN', 'SATO', 'VIS_FWDFIT','VF','MEM_NJIT',  'PIXON', 'FORWARD', 'BPROJECTION' ]
;alg_name = ['Clean', 'MEM Sato', 'VIS FWDFIT'+STRARR(2), 'MEM NJIT', 'Pixon', 'Forward Fit', 'Back Projection']
;obj_class = ['HSI_CLEAN', 'HSI_MEM_SATO', 'HSI_VIS_FWDFIT'+STRARR(2), 'HSI_MEM_NJIT', $
;             'HSI_PIXON', 'HSI_FORWARDFIT', 'HSI_BPROJ' ]

; 20-aug-2007, andre.csillaghy@fhnw,ch vis_only kwd
; 6-jul-2007, richard.schwartz@gsfc.nasa.gov
; 14-jul-2009, add uv_smooth
; 22-mar-2011, ADDED PREFIX FIELD TO STRUCTURE
; 22-oct-2012, ras, added  order, active fields, modified nknme1 'UV_Smooth' from 'UV_SMOOTH'
; 09-may-2013 Federico Benvenuto added EM (benvenuto@dima.unige.it)
; 29-Mar-2017, Kim. Commented out MEM_SATO line, and renumbered subsequent elements in str
; 09-Oct-2017, Roman Bolzern. Added VIS_CS.
;-

function hsi_alg_units, name, vis_only = vis_only, status=status

  status = 1
  nalg   = 10
  ;str = replicate( {hsi_alg_units}, 9) when adding SPACED
  str = replicate( {hsi_alg_units}, nalg)


  str[0] = {hsi_alg_units,'HSI_CLEAN','Clean','CLEAN','','Counts sc!u-1!n', 0,'clean',1,1}
  ;str[1] = {hsi_alg_units,'HSI_MEM_SATO','MEM Sato','SATO','','Counts s!u-1!n sc!u-1!n', 0,'sato',20,0}
  str[1] = {hsi_alg_units,'HSI_VIS_FWDFIT','VIS FWDFIT','VIS_FWDFIT','VF',$
    'Photons cm!u-2!n s!u-1!n asec!u-2!n',1,'vf',5,1}
  str[2] = {hsi_alg_units,'HSI_MEM_NJIT','MEM NJIT','MEM_NJIT','',$
    'Photons cm!u-2!n s!u-1!n', 1,'nj',4,1}
  str[3] = {hsi_alg_units,'HSI_PIXON','Pixon','PIXON','',$
    'Counts s!u-1!n sc!u-1!n', 0,'pixon',2,1}

  str[4] = {hsi_alg_units,'HSI_FORWARDFIT','Forward Fit','FORWARD','',$
    'Counts s!u-1!n cm!u-2!n', 0,'ff',3,1}
  str[5] = {hsi_alg_units,'HSI_BPROJ','Back Projection','BPROJECTION','',$
    'No Units', 0,'',0,1}
  str[6] = {hsi_alg_units,'HSI_UV_SMOOTH','UV_Smooth','UV','UVS',$
    'Photons cm!u-2!n s!u-1!n asec!u-2!n',1,'uv',6,1}
  str[7] = {hsi_alg_units,'HSI_VIS_CS','VIS_CS','VCS','VCS',$
    'Photons cm!u-2!n s!u-1!n asec!u-2!n',1,'vis_cs',7,1}
  str[8] = {hsi_alg_units,'HSI_EM','EM','EM','EM',$
    'Counts s!u-1!n sc!u-1!n', 0,'EM',2,1}

  ;str[9] =  {hsi_alg_units,'HSI_SPACE_D','SPACE_D','sd','SPACE_D','SD',$
  ;		'Photons cm!u-2!n s!u-1!n asec!u-2!n',1}
  str = str[sort(str.order)]
  str.order = indgen(nalg)
  if keyword_set(name) then begin
    class = hsi_get_alg_name( name, /object_class)
    status = class ne ''
    if status eq 0 then message,/info, name + ' : not a valid image class name' else $
      str = str[where( str.class eq class, count)]
  endif
  ; acs

  if keyword_set( vis_only ) then return, str[ where( str.is_vis ) ]

  return, str

end
