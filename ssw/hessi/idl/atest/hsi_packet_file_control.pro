;---------------------------------------------------------------------------
; Document name: hsi_packet_control.pro
; Created by:    Andre Csillaghy, December 13, 1999
;
; Last Modified: Thu Aug 15 14:20:03 2002 (csillag@soleil.cs.fh-aargau.ch)
;---------------------------------------------------------------------------
;
;+
; PROJECT:
;       HESSI
;
; NAME:
;       HSI_PACKET_CONTROL
;
; PURPOSE: 
;       Initializes the control parameters of the hsi_packet_file class
;
; CATEGORY:
;       Utilities
; 
; CALLING SEQUENCE: 
;       var = hsi_packet_control() 
;
; OUTPUTS:
;       var: a structure of type {hsi_packet_file_control}
;
; SEE ALSO:
;       hsi_packet_file__define
;       hsi_packet_file_control__define
;
; HISTORY:
;       2002-08-15: changed test and assumes that there is a lot of
;                   memory whenever there are more than 1 cpu. ACS
;       2002-02-24: introduces a test to reduce the packet per bunch
;                   value for machines with smaller memory
;       Release 6: Init of the adp and 272 tests included ACs, Oct 2001
;       Version 1, December 13, 1999, 
;           A Csillaghy, csillag@ssl.berkeley.edu
;
;-
;

FUNCTION HSI_Packet_File_Control

var =  {hsi_packet_file_control}

var.packet_per_bunch = 1000

IF !version.release GE '5.4' THEN BEGIN  
    IF !version.memory_bits GE 64 OR !cpu.hw_ncpu GT 1 THEN BEGIN 
        var.packet_per_bunch = 5000
    ENDIF 
ENDIF 

var.adp_test =  bytarr(10)

RETURN, var

END


;---------------------------------------------------------------------------
; End of 'hsi_packet_control.pro'.
;---------------------------------------------------------------------------
