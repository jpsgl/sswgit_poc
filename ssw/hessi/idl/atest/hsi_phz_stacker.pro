FUNCTION HSI_PHZ_STACKER, cbe,$
    coll_index, $ ;unnecessary
    n_roll_bins = n_roll_bins, n_phase_bins=n_phase_bins, $
    harm_enable=harm_enable, $
         reform2d=reform2d,$
         _extra=_extra
;+
; PURPOSE:
; Converts calibrated event list structure into populated 2-D arrays, whose
; indices correspond to digitized steps in roll angle and phase_map_ctr. The
; new structure is a stacked calibrated  eventlist {hsi_calib_event_stack}
; where the roll and phase indices are merged  into a single index that can
; easily be reformatted as needed to n_phase_bins x n_roll_bins
;
; INPUTS:
;   CBE = array of calibrated eventlist structures for one detector
;   coll_index = integer from 0 to 8 (SCALAR) -Unused, kept for compatiblity
;
; OPTIONAL INPUTS:
;   n_roll_bins = LONG scalar giving # of roll bins (defaults to 180)
;   n_phase_bins = LONG scalar  giving # of phase  bins (defaults to 8)
;	 counts are 0, when livetime is 0, livetime forced >0
;   /reform2d   If set, the stack is returned as a n_roll_bins x n_phase_bins
;
;
; OUTPUTS:
; Returns structure, cbestack, defined below.
;
; Define output structure.
; Initial version uses predetermined array sizes and does not split time bins among phase or roll bins.
; History:
;   21-may-2003 RAS, adapted from g hurford populate_phase_bins
;       2004 Jan 20 EJS, added warnings about phase and roll spreading (don't
;       conserve flux)
;       2004 aug 05, EJS added options to include empty phasebins and reform
;       to a 2-D array.
; 15-mar-2005, removed extraneous code that was going to phz and roll spread the values. GH
;   determined unnecessary.  Have added phase weighting to the stacked list output
; May, 2007, modified for harmonics
; Jul, 2007, ras, found bug in implementation in copying phase_map_ctr w/o change to 0-twopi
; Jan, 2009, ras, change empty_flag to 0 counts, 0 livetime condition.  No data bins are removed
;		empty_flag can be passed but it will have no effect
; 10-jan-2012, ras, atten_state added, needed for adv regularized vis
;-


default, usedphase, 0
default, n_roll_bins, 180L
default, n_phase_bins, 8L

default, xyoffset, fltarr(2)


n_phase_bins = long(n_phase_bins)

n_roll_phase_bins = long( n_roll_bins ) * n_phase_bins
harm_enable  = fcheck( harm_enable, 1)
harm_input   = tag_names(/str, cbe) eq 'HSI_CALIB_EVENTX'
use_harm = harm_input and harm_enable
if not harm_enable and harm_input then begin
	temp = replicate( {hsi_calib_event}, n_elements(cbe))
	struct_assign, cbe, temp
	cbe = temp
	endif

stk_struct = use_harm ? {hsi_calib_eventx_stack} : {hsi_calib_event_stack}
cbestack =   replicate( stk_struct, n_roll_phase_bins)
cbestack.atten_state = cbe[0].atten_state ;cannot combine atten_states in stacker, meaningless

;     IDL> help,{hsi_calib_event_stack},/st
;     ** Structure HSI_CALIB_EVENT_STACK, 8 tags, length=32, data length=32:
;        ROLL_ANGLE      FLOAT          0.000000
;        MODAMP          FLOAT          0.000000
;        PHASE_MAP_CTR   FLOAT          0.000000
;        GRIDTRAN        FLOAT          0.000000
;        FLUX_VAR        FLOAT          0.000000
;        BACKGROUND      FLOAT          0.000000
;        COUNT           FLOAT          0.000000
;        LIVETIME        FLOAT          0.000000

;
; Calculate grid angle (radians) and wavenumber = 2pi / angular pitch in arcseconds.
twopi = !pi* 2.0
dphase_bin = twopi / n_phase_bins
;
; Calculate digitized roll and phase bin values.
cbestack.roll_angle     = ((fltarr(n_phase_bins)+1.) # $
    (FINDGEN(1,n_roll_bins)+0.5)  * twopi / n_roll_bins)[*]    ; [0,,,2pi]

;cbestack.phase_map_ctr = ((FINDGEN(n_phase_bins)+0.5)# $
;    (fltarr(1,n_roll_bins)+1.) * dphase_bin )[*]   ; [0,,,2pi]
;
; Calculate roll bin index for each time bin.
roll_angle01 = (((cbe.roll_angle/twopi MOD 1.) +1. ) MOD 1.)
rbi =long( NINT ((roll_angle01 * n_roll_bins) -0.5)) >0

nharm = use_harm ? 3 : 1

;
;
; Calculate phase bin index for each time bin.
;

;corrphase = reform( (((cbe.phase_map_ctr / twopi MOD 1.) +1. ) MOD 1.),$
;                    nharm, n_elements(cbe))
;
;pbi = reform(transpose(long( NINT (( corrphase * n_phase_bins) -0.5)) >0))
corrphase = reform( (((cbe.phase_map_ctr / twopi MOD 1.) +1. ) MOD 1.),$
                    nharm, n_elements(cbe))

pbi = reform(transpose(long( NINT (( corrphase[0,*] * n_phase_bins) -0.5)) >0))


;
; Populate appropriate bins.
;ntimebin = N_ELEMENTS(cbe)


;for ih = 0, nharm-1 do begin

    ;npop = histogram( n_phase_bins * rbi + pbi[*,ih], min=0,max=n_roll_phase_bins-1, rev=rev)
    npop = histogram( n_phase_bins * rbi + pbi, min=0,max=n_roll_phase_bins-1, rev=rev)

    wbin = where( npop ge 1, nbin )

       if nbin ge 1 then FOR i = 0l, nbin-1 DO BEGIN
           j = wbin[i]
           ix = rev[ rev[j]:rev[j+1]-1] ;These are the indices for this roll/phase bin
           nix= n_elements(ix)
           if nix eq 1 then begin
           	temp = cbestack[j]
           	ra   = cbestack[j].roll_angle
           	struct_assign, cbe[ix], temp
           	cbestack[j] = temp
           	;ras, added 16-jul-2007
           	cbestack[j].phase_map_ctr = corrphase[ix] * twopi ;oops, forgot about this
           	cbestack[j].roll_angle    = ra

           	endif else begin

           live = cbe[ix].livetime

           wt = total( live  )
           cbestack[j].count  = total( cbe[ix].count  )
           cbestack[j].livetime  = wt
           cbestack[j].background = total( cbe[ix].background )
           cbestack[j].flux_var   = f_div(total( cbe[ix].flux_var * live ) , wt)
           cbestack[j].gridtran   = f_div(total( cbe[ix].gridtran * live ) , wt)
           ;

;         cbestack[j].modamp[ih]     = f_div(total( cbe[ix].modamp[ih]   * live ) , wt)
;         cbestack[j].phase_map_ctr[ih] = f_div(total( corrphase[ih, ix] * live), wt) *twopi



          live = transpose(rebin(live, nix, nharm))
          dim  = size(/N_dim, cbe[ix].modamp)

          cbestack[j].modamp     = f_div(total( cbe[ix].modamp   * live,dim), wt)

          cbestack[j].phase_map_ctr = f_div(total( (nharm eq 1 ? corrphase[ix] : corrphase[*,ix]) $
          	* live,dim), wt) *twopi


          endelse
           ENDFOR
      ; endfor
;
;
cbestack.livetime = cbestack.livetime > 0.0
empty = where( cbestack.livetime le 0.0, nempty)
;zero livetime should have 0 counts but to prevent nasty outliers be certain
if nempty GT 0 then cbestack[empty].count=0.0
if keyword_set(reform2d) then cbestack=reform(cbestack,n_phase_bins,n_roll_bins)

RETURN, cbestack
END

