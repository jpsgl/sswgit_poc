;Return the first time in each packet
function hsi_first_time, collect_time, first_time
return, ishft(mask(collect_time, 10, 42),10) or first_time
end



;+
; PROJECT:
;   HESSI
; NAME:
;   HSI_APP100_UNPACK_TIME
;
; PURPOSE:
;   This procedure decodes the full time from the science data packets.
;
; CATEGORY:
;
;
; CALLING SEQUENCE:
;   hsi_app100_unpack_time( data, collect_time, source, $
;   eventlist.time =  is_event, n100, ut_ref, $
;   time_unit=time_unit, simulated_data=simulated_data, max_pack=max_pack, $
;   interpolate = interpolate,  min_diff=min_diff, collect_time_only = collect_time_only,  $
;    error=error, errmsg=errmsg, errcond=errcond )
;
; CALLS:
;   HSI_UNPK_TIMESTAMP, HSI_FILL_FORWARD, MASK, HSI_FIRST_TIME
;   HSI_TIME_RESET, FCHECK, MINMAX
;
; INPUTS:
;       none explicit, only through commons;
;
; OPTIONAL INPUTS:
;   none
;
; OUTPUTS:
;       none explicit, only through commons;
;
; OPTIONAL OUTPUTS:
;   none
;
; KEYWORDS:
;   none
; COMMON BLOCKS:
;   none
;
; SIDE EFFECTS:
;   none
;
; RESTRICTIONS:
;   none
;
; PROCEDURE:
;   none
;
; MODIFICATION HISTORY:
;   May 30, 2001 Version 1, richard.schwartz@gsfc.nasa.gov
;   10-jun, 2002, ras, ensure that time_unit must be a power of two
;   in agreement with hsi_sctime_add.  I'm not sure where that
;   requirement comes from.
;   2-feb-2006, ras, look for anomalous timestamps.
;     Reject them if they are larger than the next collect_time
;     or more than 5 seconds before or after their packet's collect_time
;     Set time to the last time, a2d_index to 0, livetime to 0.
;	28-aug-2006, ras, made tstamp checking more robust
;
;-


function hsi_app100_unpack_time, data, collect_time, source, is_event, n100, ut_ref, $
    time_unit=time_unit, max_pack=max_pack,$
    interpolate = interpolate,  min_diff=min_diff, collect_time_only = collect_time_only,  $
    simulated_data = simulated_data, $
    nbad=nbad, $; 1 or more if there are bad timestamps
    error=error, errmsg=errmsg, errcond=errcond

maxevent = n100 * 270L

if not keyword_set( min_diff ) then $
    min_diff =  fcheck(min_diff,3) * ( 1- keyword_set(simulated_data) )
;
;
index_tstamp = where( source eq 31, ntstamp)
all_index    = lonarr( n100 + ntstamp)
first_index  = lindgen(n100)*270
all_index[0] = first_index
time_out     = lon64arr(maxevent)
time         = fix(mask( data[0:maxevent-1], 4, 10))
first_time   = time[ first_index ]




; Resolve the timestamps and timewords to make full 52 bit times.
; Replace the timestamp timeword with the same value as for the science word.

if ntstamp ge 1 then begin
    tstamp = mask( data[index_tstamp], 0, 27 )
    timestamp64 = hsi_unpk_timestamp( index_tstamp, tstamp, time, collect_time, $
    simulated_data = simulated_data, error=error, errmsg=errmsg, errcond=errcond)
    time[index_tstamp] = time[index_tstamp+1]

    sel_tstamp_first = where( (index_tstamp mod 270) eq 0, ntstamp_first)
    if ntstamp_first ge 1 then begin
       collect_time[ index_tstamp[sel_tstamp_first] / 270] = timestamp64[sel_tstamp_first]
       first_time[   index_tstamp[sel_tstamp_first] / 270] = fix(mask( timestamp64[sel_tstamp_first], 0, 10))
       endif
;stop
    endif



first_time_64 = hsi_first_time( collect_time, first_time  )

;Look for rollovers between the first time and the collect_time.
;Their signature is a diff of gt 1024-min_diff
chk_diff = where( (first_time_64 - collect_time) ge (1023-min_diff), nchk_diff)
if nchk_diff ge 1 then begin
    first_time_64[chk_diff] = first_time_64[chk_diff] - 1024

    endif

if ntstamp ge 1 then begin
;Look for out of sequence timestamp64, probably due to telem errors
;
    ;Bad if > next collect_time or last collect_time + 5.0 seconds
    ulimit = ([first_time_64, first_time_64[n100-1]+ 5242880L ])[(index_tstamp/270L + 1L)]
    bad = where( timestamp64 gt (ulimit+100000L), nbad)

    if nbad ge 1 then begin
    ;stop
       ;for i=0,nbad -1 do begin ;remove from is_event all from bad stamp to packet end.
       ;better to remove than to have poison of bad time.
         next_pack = ceil( index_tstamp[bad[0]] / 270.) * 270
         wrem = where( is_event gt index_tstamp[bad[0]] and is_event lt next_pack , nrem)

         if nrem ge 1 and nrem lt n_elements(is_event) then begin
          message, /continue,$
              '!!!!Removing '+strtrim(nrem,2)+' words from telemetry due to bad timestamp'

          remove, wrem, is_event
          ntstamp = ntstamp - nbad       ;28-aug-2006
          if ntstamp gt 0 then begin	;28-aug-2006
          	bb= bad
          	remove, bb, index_tstamp
          	remove, bb, timestamp64
          ;bad = where( timestamp64 gt (ulimit+100000L), nbad)
          	endif 	;28-aug-2006
          endif
          endif



	all_index    = lonarr( n100 + ntstamp)

	if ntstamp ge 1 then begin	;28-aug-2006
		all_index[0] = first_index	;28-aug-2006
    	all_index[n100] = index_tstamp	;28-aug-2006
    	endif	;28-aug-2006
    fill_times = ntstamp ge 1 ? [first_time_64, timestamp64] : [first_time_64] 	;28-aug-2006
    ord = sort( all_index )
    all_index = all_index[ord]

    time_out =  hsi_fill_forward( (fill_times)[ord], all_index, lindgen(maxevent))


    endif else begin

    ;dlast = 2*first_time_64[n100-1]-first_time_64[n100-2]
    first = first_time_64[0]


    time_out[0] = rebin( first_time_64, maxevent, /sample )

    time_out[270L*(n100-1L):*] = first_time_64[n100-1]

    endelse

time_out = ishft(ishft(time_out, -10), 10) + time

;
; HSI_TIME_RESET corrects for the 10 bit counter overflow.
; For simulated data, all overflow, time[i+1] lt time[i], should
; be corrected by adding 1024 to time[i+1]. In the real event stream,
; time[i+1] may in fact trail time[i] by a few (min_diff) microseconds.
;



time_out = (hsi_time_reset(time_out, all_index, min_diff = min_diff))[is_event]

ut_internal = time_out[0]
ut_ref = {hessi_sctime_full}
ut_ref.seconds = long(ishft( ut_internal, -20))
ut_ref.bmicro    = long(mask( ut_internal, 0, 20))

;Check and adjust (if necessary) TIME_UNIT so the range fits as an unsigned longword.
;
collect_time_lim = minmax(collect_time)
collect_time_lim = collect_time_lim[1]-collect_time_lim[0]
unit_scale = 2ll^32
test = collect_time_lim  / unit_scale + 1

time_unit =long( fcheck(time_unit, 1) >  test  )
;Ensure that test is a power of two, ras, 10-jun-02
time_unit = 2l^ceil(alog(time_unit)/alog(2))

return, ulong((time_out - ut_internal)/time_unit)
end

