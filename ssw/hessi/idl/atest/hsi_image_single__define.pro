;---------------------------------------------------------------------------
; Document name: hsi_image_single__define.pro
; Time-stamp: <Thu Oct 23 2008 18:20:11 csillag tournesol2.local>
;---------------------------------------------------------------------------
;+
; PROJECT:
;       RHESSI
;
; NAME:
;       RHESSI IMAGE SINGLE CLASS DEFINITION
;
; PURPOSE:
;       Generates and provides access to single images. This is the
;       former hsi_image interface. It generates single images,
;       and passes the generated images to the concrete classes of
;       hsi_image_strategy, in fact always hsi_image_raw__define.
;
;       Ususally you dont want to call hsi_image_single directly
;       -- although it should work
;
; CATEGORY:
;       RHESSI Imaging
;
; CONSTRUCTION:
;       image_obj = Obj_Name( 'hsi_image_single' )
;       image_obj = Hsi_Image_Single()
;
; (INPUT) CONTROL PARAMETERS DEFINED IN THIS CLASS:
;
;       image_alg: a string determining which algorithm to
;                  use. Currently, the following algorithms can
;                  be called:  'CLEAN'
;                              'MEM_SATO',
;                              'VIS_FWDFIT',
;                              'MEM_NJIT',
;                              'PIXON',
;                              'FORWARDFIT',
;                              'BACK PROJECTION'
;                              'UV_SMOOTH'
;                              'VIS_CS'
;                              'EM'
;                              'VIS_WV'
;
;       see hsi_image_single_control__define
;
; (OUTPUT) INFO PARAMETERS DEFINED IN THIS CLASS:
;
;      image_alg_used: a string giving the name of the imaging
;                       algorithm used to create the image.
;
;      see hsi_image_single_info__define
;
; CLASS RELATIONSHIPS:
;      HSI_Strategy_Holder: parent class
;      HSI_Modpat_Products: source class
;      HSI_Bproj, HSI_Clean, HSI_MEM_Sato, HSI_Pixon, HSI_Forwardfit: strategies
;
; SEE ALSO:
;       hsi_image_single, hsi_image__define, hsi_image_strategy__define
;
; HISTORY:
; 11-Jul-2018, Kim. Added snr params to those that need to be set explicitly for all algs in SET method, 
;   added checking profile_jpeg_plot and viscomp_jpeg_plot when deciding whether to make plots 
; 09-Feb-2018, RAS, Added scale_image method and call this instead of alg_unit method (which was
;   in a standalone file) to scale the image to the standard units of photons/cm2/s/asec^2
; 07-Feb-2018, Kim. In INIT method, set default alg to new BPROJ_IMAGE not BPROJ
; 02-Feb-2018, Kim. Ensure that all vis algs reprocess when image_dim, pixel_scale, or pixel_size
;   are changed. Previously, current alg reprocessed, but other vis algs didn't know of change.
;   Also for nvis_min and mc_ntrials changes, make sure all algs reprocess.
; 15-nov-2017, RAS, passing cbe_normalize through getdata to the image als from image_raw the caller
; 01-Nov-2017, Kim. Changed FIVE_CS to VIS_WV
; 27-Oct-2017, M.A. Duval-Poo, added FIVE_CS and EM
; 09-oct-2017, Roman Bolzern, added VIS_CS
; 18-jun-2017, RAS, added cartesion modpat exception to getdata
; 24-May-2017, Kim. Now that we're calculating all visibilities at once (all eb and tb), in Set, trap tb_index_p
;   (in a similar way to what was already being done for eb_index_p), and if doing a vis alg, only set tb_index
;   in the vis object, not the calib eventlist obj. This is so cbe won't think it needs to reprocess (which would
;   force the vis to reprocess).
;   Also, check for profile_ps_plot set, and handle profile plot window management in hsi_image_profile_plot.
;   Also, added check for viscomp_show_plot or viscomp_ps_plot set, and call to hsi_image_vis_plot.
; 31-May-2013, Kim. In GetData, set catch only if framework_get_debug() is 0 (like hsi_insert_catch),
;   Previously no check - always called catch.
; 19-oct-2012, ras, if image_alg isn't vis, and vis_input_fits is set to a file then
; vis_input_fits is disabled by setting it to a null string
; 11-oct-2012, ras, if not is_vis then vis_type is photon
; 14-aug-2012, ras, used hsi_alg_units to determine whether vis or annsec
; also see note about why we reprocess for a new image_alg, even if the vis bag is correct
; for the time and energy bins.
; 08-Apr-2011, Kim. In ::set, at end in call to self->Strategy_Holder::Set, spelled mc_ntrials without s
;   by mistake.  Corrected.
; 19-Nov-2010, Kim. In setstrategy, call setcaller for all algs, not just vis als. Also
;   in set, added branch to set need_update if mc_ntrials is changed
; 17-may-2010, ras, NOQUINTIC_INTERP added to hsi_annsec2xy in getdata
; 30-Apr-2010, Kim. If profile_show_plot is set, call hsi_image_profile_plot to plot profiles
; 13-Dec-2007, Kim. Added GetCaller method.
; 26-Mar-2007, Kim. In set, if image_algorithm is memvis, print msg and return.
; 15-Mar-2007, acs and kim.  In getdata catch, for current strategy call setdata,-1
; 21-dec-2005, acs new algorithms defined
; 13-Mar-2005, acs, change cacth handling in getdata such that wehne called
;              from hsi_image_raw it does not bubble up but go on to the next
;              image
; March 2005, Kim + acs, imagecube release
; 24-Oct-2002, Kim.  Added @hsi_insert_catch in getdata, and commented out some extra checks
;   for when no files are available - catch will handle it now.
; 19-Jul-2002, Kim.  Call heap_gc (if >= 5.5) in getdata method to prevent memory leaks
; 10-Jun-02, Kim.  In Getdata, if not hsi_use_sim, using hsi_eventlist_packet, and no file, then error
;       Development for Release 3, July-October 1999
;       Development for Release 2, March-July 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;--------------------------------------------------------------------

pro hsi_image_single_test

  ; problem of checking the wrong need update when switching back and forth between
  ; vis algs and modpat algs:
  o = hsi_image_single()
  o->set, time_range = ['2002/02/20 11:06', '2002/02/20 11:06:36']
  im = o->getdata( pixel_size=[1., 1.], image_dim=[64, 64], $
    xyoffset=[905., 260.], det_index_mask=[0, 0, 1, 1, 1, 1, 1, 1, 1] )
  o->plot, /limb
  ; these two are 0
  print, o->get( /need_update ), o->need_update()
  o->plot, image_alg = 'vis_fwdfit'
  ; these are also 0:
  print, o->get( /need_update ), o->need_update()
  ; now plot with clean:
  o->plot, image_alg = 'clean'
  print, o->get( /need_update ), o->need_update()
  ; now get back to fwdfit:
  o->plot, image_alg = 'vis_fwdfit'
  ; and now need_update() is never again 0
  print, o->get( /need_update ), o->need_update()
  ; now same with njit
  o->plot, image_alg = 'njit'
  print, o->get( /need_update ), o->need_update()

  oo = o->get( class = 'hsi_visibility', /obj )
  oo->write, 'aa.fits'

  obj_destroy, o

  o = hsi_image_single()
  o->set, image_alg = 'vis_fwdfit'
  o->set, vis_filename = 'aa.fits'
  o->Plot, /limb

  obj_destroy, o

  o = hsi_image_single()
  o->set, vis_filename = 'aa.fits'
  o->plot

  window, 0
  o = hsi_image_single()
  o->set, time_range = ['2002/02/20 11:06', '2002/02/20 11:06:36']
  im = o->getdata( pixel_size=[1., 1.], image_dim=[64, 64], $
    xyoffset=[905., 260.], det_index_mask=[0, 0, 1, 1, 1, 1, 1, 1, 1] )
  o->plot, /limb
  window, 1
  o->set, time_range = ['2002/02/20 11:06', '2002/02/20 11:06:35']
  o->plot, /limb

  obj_destroy, o
  o = hsi_image_single()
  o->set, time_range = ['2002/02/20 11:06', '2002/02/20 11:06:35']
  im = o->getdata( pixel_size=[1., 1.], image_dim=[64, 64], $
    xyoffset=[905., 260.], det_index_mask=[0, 0, 1, 1, 1, 1, 1, 1, 1] )
  o->plot, /limb

  o->plot, image_alg = 'clean'

  ;check for images that do not finish correctly
  ;o=hsi_image()
  ;o->set,im_time_int='2002/02/20 '+[['09:46:44','09:47:44'],['09:47:44','09:48:44'],['09:48:44','09:49:44']]
  ;o->set,im_energy_bin=[[12,25],[25,50]]
  ;o->set,image_dim=128,pixel_size=1,xyoffset=[911,335]
  ;o->fitswrite, this_out_file='test.fits'

end

;----------------------------------------------------------------

FUNCTION Hsi_Image_Single::INIT, SOURCE = source, caller = caller, _EXTRA=_extra

  if obj_valid( caller ) then self.caller = caller

  IF NOT Obj_Valid( source ) THEN BEGIN
    source = Obj_New( 'HSI_Modpat_Products', _EXTRA=_extra )
  ENDIF

  alg = hsi_alg_units()

  ;acs needs to define the alternate sources for visibility stuff
  alt_src = strarr( n_elements( alg ) )
  vis_algs = where( alg.is_vis )
  alt_src[vis_algs] = 'HSI_VISIBILITY'

  ;HSI_CLEAN HSI_MEM_SATO HSI_VIS_FWDFIT HSI_MEM_NJIT HSI_PIXON HSI_FORWARDFIT HSI_BPROJ

  ret=self->Strategy_Holder::INIT( alg.class, alt_src,  $
    CONTROL=hsi_image_single_control(), $
    INFO={hsi_image_single_info}, $
    SOURCE=source)

  self->Set, ALGORITHM_AVAILABLE = alg_list, /THIS_CLASS_ONLY

  ; initialize alg_iunits to a blank string otherwise it creates
  ; problems when writing files.
  self->set, image_units=' '

  ; we need to pass the bproj reference to the strategy in order to
  ; avoid re-creating another object
  bproj = source->Get( CLASS_NAME='HSI_BPROJ', /OBJ )
  class_index = (where(alg.class eq 'HSI_BPROJ'))[0]

  self->Set, SOURCE = bproj, SRC_INDEX=class_index+1

  alg_index = (where(alg.class eq 'HSI_BPROJ_IMAGE'))[0]

  self->SetStrategy, alg_index

  ; create hsi_visibility, otherwise we have problems

  vis_src = hsi_visibility( source = source->get( class = 'hsi_calib_eventlist', /obj ), $
    image_obj = self.caller )
  ;vis_src = hsi_visibility( source = source->get( class = 'hsi_modul_pattern', /obj ) )
  self->set, source = vis_src, src_index = n_elements(alg)+1

  ; this is to tell that a get should return parameters from any of the
  ; strategies, not only the current one
  self.get_all_strategies =  1B

  self.noupdate =  0

  IF Keyword_Set( _EXTRA ) THEN self->Set, _EXTRA = _extra

  RETURN, ret

END

;----------------------------------------------------------

PRO hsi_image_single::SetStrategy, strategy

  ; rework of the setstrategy method to be able to track down the update time needed
  ; then in hsi_image-single to see if the image strategy has changed.

  self->strategy_holder::setstrategy, strategy
  ;stop

  ; needs this to make sure we're not using the phase stacker for annec-based
  ; images, this does not work yet.
  yes_vis = is_member( strategy, (hsi_alg_units( /vis_only )).class )
  if not yes_vis and self.old_strat_is_vis then begin
    ;    self->set, /need_update, class = 'hsi_calib_eventlist'
    ; acs see remark below
    self->set, use_phz= self.old_use_phz
    self.old_strat_is_vis = 0
  endif else if yes_vis then begin
    if obj_valid( self.caller ) then begin
      ; 14-jan-2008, kim changed to framework::get- if reading an image file, and call plain get here it
      ; starts an infinite loop.
      ; I have NO idea why, but if I say /im_time_int there's a problem, so have to spell it out!?
      times = self.caller->framework::get( /im_time_interval )
      strat = self->getstrategy()
      strat->set, vis_time_intervals = times
      ;        strat->setcaller, self  ; moved this to below, outside of this if test
    endif
    ; acs 2006-12-23 vis algs always use the phase stacker, so we have to keep track
    ; of its value to be able to reset it when we switch back to annsec algorithms.
    self.old_use_phz = self->get( /use_phz )
    self.old_strat_is_vis = 1
  endif

  ;Added 19-nov-2010, Kim. Now set caller for all strats, not just vis, because some params
  ; (e.g. mc_ntrials) are not available in strat.
  strat = self->getstrategy()
  strat->setcaller, self

  ; try this for hsi_image_single which needs to track the strategy update. otherwise it
  ; keeps the old image and does not change the image
  self.admin->set, last_update = !hsi_last_update_counter
  !hsi_last_update_counter = !hsi_last_update_counter + 1

END

;----------------------------------------------------------

function hsi_image_single::is_vis_alg, strat, vis_algs

  ; helper function used in the ::set method
  ; strat and vis_algs are  secondary output parameters

  strat = obj_class( self->getstrategy() )
  return, (hsi_alg_units(strat)).is_vis

  ;  vis_algs = hsi_alg_units( /vis_only )
  ;  return,  is_member( strat, vis_algs.class )

end

;---------------------------------------------------------

PRO Hsi_Image_Single::Set, $
  IMAGE_ALGORITHM=image_algorithm, $
  vis_input_fits=vis_input_fits, $
  DONE=done, NOT_FOUND=NOT_found, $
  PIXEL_SIZE = pixel_size, $
  IMAGE_DIM = image_dim, PIXEL_SCALE =pixel_scale, $
  NVIS_MIN = nvis_min, $
  mc_ntrials = mc_ntrials, $
  eb_index = eb_index_p, $
  tb_index = tb_index_p, $
  snr_chk=snr_chk, $
  snr_thresh=snr_thresh, $
  snr_vis_file=snr_vis_file, $
  _EXTRA=_extra

  ; NOTE: if not doing a vis alg, then eb_index / tb_index will be set to eb_index_p / tb_index_p and
  ; set in the normal way via Strategy_Holder::Set below.
  if is_number( eb_index_p ) then begin
    ; If doing a vis alg, then only set eb_index in the vis obj. Don't want it to
    ; get through to the calib eventlist obj. Vis are calculated for all energies
    ; and times and stored, so don't need any reprocessing for new eb or tb index.
    ; If we don't catch it here, goes through annsec obj chain to calib eventlist and
    ; thinks it needs to reprocess.
    strat = obj_class( self->getstrategy() )
    if self->is_vis_alg() then begin
      vis_obj = self->get( class = 'hsi_visibility', /obj )
      vis_obj->set, eb_index = eb_index_p
    endif else eb_index = eb_index_p
  endif

  if is_number( tb_index_p ) then begin
    ; Same as above comment for eb_index - for vis algs, only set tb_index in vis obj
    strat = obj_class( self->getstrategy() )
    if self->is_vis_alg() then begin
      vis_obj = self->get( class = 'hsi_visibility', /obj )
      vis_obj->set, tb_index = tb_index_p
    endif else tb_index = tb_index_p
  endif

  if Keyword_Set( _EXTRA ) OR Keyword_Set( IMAGE_ALGORITHM ) then self.noupdate = 0

  ; vis filename is an exception that we must deal with here. if vis_filename
  ; is set, then we assume the use of a visibiliy algorithm. this restriction
  ; will be dropped eventually
  if keyword_set( vis_input_fits ) then begin
    ;    is_vis_alg = self->is_vis_alg( strat, vis_algs )
    ;    if not is_vis_alg then begin
    ;        self->setstrategy, vis_algs[0].class
    ;    endif
    ;    self->strategy_holder::set, vis_input_fits = vis_input_fits
    ;    if not is_vis_alg then self->setstrategy, strat
    image_alg = hsi_get_alg_name( self->get( /image_alg ), /object )
    vis_algs = hsi_alg_units( /vis_only )
    if not is_member( image_alg, vis_algs.class ) then image_algorithm = vis_algs[0].name
  endif

  ; When image_dim, pixel_scale, or pixel_size are changed, we need to make sure all the vis algs
  ; reprocess their image.  They wouldn't normally because those params are in hsi_modul_pattern_strategy
  ; which is not in the chain for the vis algs after the visibility bag is made. Note that for non-vis algs, they
  ; will be told to reprocess when Strategy_Holder::Set for image_dim is called at the end of this Set method.
  ; Previously just set /need_update on current strategy. Changed to all vis algs 02-feb-2018, Kim.
  ; Also for a few other parameters, need to make sure all algs (or all vis algs) will reprocess when they're changed.

  set_need_update_vis = 0
  set_need_update_all = 0

  IF Keyword_set( PIXEL_SIZE ) THEN BEGIN
    if n_elements( pixel_size ) eq 1 then pixel_size = replicate( pixel_size, 2)
    oldval = self->get( /pixel_size )
    if ~same_data( oldval, pixel_size ) then set_need_update_vis = 1
  endif

  IF Keyword_set( IMAGE_DIM ) THEN BEGIN
    if n_elements( image_dim ) eq 1 then image_dim = replicate( image_dim, 2)
    oldval = self->get( /image_dim )
    if ~same_data( oldval, image_dim ) then set_need_update_vis = 1
  endif

  IF Keyword_set( PIXEL_SCALE ) THEN BEGIN
    oldval = self->get( /pixel_scale )
    if ~same_data( oldval, pixel_scale ) then set_need_update_vis = 1
  endif

  IF Keyword_set( NVIS_MIN ) THEN BEGIN
    oldval = self->get( /nvis_min )
    if ~same_data(nvis_min, oldval) then set_need_update_vis = 1
  endif

  if exist(mc_ntrials) then begin
    oldval = self->get( /mc_ntrials )
    if ~same_data(mc_ntrials, oldval) then set_need_update_all = 1
  endif
  
  if exist(snr_chk) then begin
    oldval = self->get( /snr_chk )
    if ~same_data(snr_chk, oldval) then set_need_update_all = 1
  endif
  
  if exist(snr_thresh) then begin
    oldval = self->get( /snr_thresh )
    if ~same_data(snr_thresh, oldval) then set_need_update_all = 1
  endif
  
  if exist(snr_vis_file) then begin
    oldval = self->get( /snr_vis_file )
    if ~same_data(snr_vis_file, oldval) then set_need_update_all = 1
  endif

  ; If either flag is set, set need_update in either all strats, or in vis strats
  if set_need_update_vis or set_need_update_all then begin
    strat = self->getstrategy()
    strat->set, /need_update, /this
    ; Get info struct for all algs or all vis algs. Order field gives strategy number.
    alg = set_need_update_all ? hsi_alg_units() : hsi_alg_units(/vis_only)
    n_alg = n_elements(alg.class)
    for i=0,n_alg-1 do begin
      strat = self->getstrategy(alg[i].order)
      if is_object(strat) then strat->set, /need_update, /this
    endfor
  endif

  IF Keyword_Set( IMAGE_ALGORITHM ) THEN BEGIN
    alg_used = HSI_Get_Alg_Name( image_algorithm, /OBJECT_CLASS, INDEX=idx )
    IF alg_used NE '' THEN BEGIN
      if alg_used eq 'HSI_MEMVIS' then begin
        message, 'MEM VIS Image Algorithm is no longer valid. Aborting.', /cont
        return
      endif
      ;        self->SetStrategy, idx[0]
      self->setstrategy, alg_used
      ;RAS, 11-oct-2012, add the following line
      if ~self->is_vis_alg() then begin
        Self->Set, vis_type='photon';only possibility
        if is_string(Self->Get(/vis_input_fits)) then begin
          Self->set,vis_input_fits=''
          message, /continue, 'Disabling VIS_INPUT_FITS selection'
        endif
      endif
    ENDIF ELSE BEGIN
      Message, 'No algorithm found that matches ' + image_algorithm, /INFO, /CONTINUE
      RETURN
    ENDELSE
  ENDIF

  self->Strategy_Holder::Set, IMAGE_ALGORITHM = image_algorithm, $
    _EXTRA=_extra, DONE=done, NOT_FOUND=NOT_found, $
    vis_input_fits = vis_input_fits, $
    PIXEL_SIZE = pixel_size, IMAGE_DIM = image_dim, PIXEL_SCALE =pixel_scale, $
    NVIS_MIN = nvis_min, mc_ntrials=mc_ntrials, eb_index = eb_index, tb_index=tb_index, $
    snr_chk=snr_chk, snr_thresh=snr_thresh, snr_vis_file=snr_vis_file

END

;----------------------------------------------------------

;Note that this need_update calls the strategy's need_update, so any hsi_image_single control parameters that are not handled
; explicitly in the set method (like image_alg) DO NOT cause reprocessing!  This is why the profile and viscomp plot options don't cause
; reprocessing. The strategy here is the image alg.

FUNCTION hsi_image_single::Need_Update, _EXTRA = _extra

  ; we need to redefine need_update, because it needs to be passed to the
  ; strategy class

  ;... except when hsi_modul_pattern_strategy_control is to 1, this need for all strategies.
  ;if (self->get( class = 'hsi_modul_pattern_strategy', /obj ))->get( /need_update, /this ) then return, 1

  this_strat = self->GetStrategy( _EXTRA=_extra )

  RETURN, this_strat->Need_Update()

END

;----------------------------------------------------------

FUNCTION Hsi_Image_Single::Get, _EXTRA=_extra, $
  FOUND=found, NOT_FOUND=NOT_found, $
  last_update = last_update

  @hsi_insert_catch

  if keyword_set( last_update ) then return, self.admin->get( /last_update )

  return, self->Strategy_Holder::Get(_EXTRA = _extra, found=found, not_found = not_found )

end

;----------------------------------------------------------

FUNCTION Hsi_Image_Single::GetCaller
  return,self.caller
end
;----------------------------------------------------------
PRO Hsi_Image_Single::Scale_Image, Image, OLD=old, ALG_USED=alg_used ; input for testing
  ;+
  ; Purpose: Divides the image by the UNIT_SCALE, which is the conversion between
  ; the units of the different imaging strategy outputs, and the official
  ; Image Units (photons/cm2/s/asec^2) for standard photon algs
  ; Algorithm_Units - units on result of image_alg_class->getdata()
  ; Updated, 2-aug-2001. ras. Change asec2 to pixel_area. Change unit format.
  ; added image_units.
  ; RAS. 9-oct-2001, changed back projection to not divide by pixel area
  ; RAS, 30-nov-2003, use absolute_time_range not time_range for duration
  ; RAS, 15-dec-2005, added hsi_mem_njit and hsi_vis_fwdfit which are
  ;   already in the final units.
  ; 8-feb-2006, RAS, fixed unit bug introduced on 15-dec-2005
  ; 6-jul-2007, ras, using hsi_get_alg_name and hsi_algorithm_units
  ; These functions gather the information about image_algs and\
  ; units more compactly and in a way that can be used
  ; more easily by other routines
  ; 28-sep-2012, RAS, added check for electron flux images
  ; set broj image_units to ''
  ; 9-feb-2018, RAS, inserted this method (which replaces the alg_unit method in 
  ;   hsi_image_single__alg_unit.pro) in this file
  ;-

  image_units = 'photons cm!u-2!n s!u-1!n asec!u-2!n'
  default, old, 0 ;if set to 1 use old method in next block
  default, alg_used, self->get(/image_alg)
  class= hsi_get_alg_name(alg_used, /object_class, index = index)
  unit_string = (hsi_alg_units())[index].units
  scales = hsi_algorithm_units( self )
  unit_scale = 1./scales[index].scale
  if class eq 'HSI_BPROJ_IMAGE' then begin
    unit_scale =1.0
    image_units = ''
  endif

  ;Check for regularized image, reg_photon or reg_electron
  if self->Get(/VIS_TYPE)  eq 'reg_electron' and self->Is_Vis_Alg() then $
    image_units = 'Electrons cm!u-2!n s!u-1!n keV!u-1!n asec!u-2!n'
    
  self->set, pixel_area = float( product(self->Get(/pixel_size ) $
    * self->Get(/pixel_scale)))
  self->set, alg_unit_scale = unit_scale, $
    algorithm_units = unit_string , $
    image_units = image_units
  Image /= unit_scale
end

;----------------------------------------------------------

FUNCTION Hsi_Image_Single::GetData, $
  CLASS_NAME=class_name, $
  PLOT=plot, $
  yes_fromraw = yes_fromraw, $
  _EXTRA=_extra

  ;@hsi_insert_catch
  ; 2005-03-13 acs
  ; the hsi_insert catch needs to be extended a little bit here as we
  ; want the getdata to just return -1 but then, if we'r in the middle
  ; of an image cube, just go on to the next image.
  error_catch = 0
  ;if self.debug eq 0 then catch, error_catch
  if framework_get_debug() eq 0 then catch, error_catch

  checkvar, yes_imagecube, 0
  if error_catch ne 0 then begin

    catch, /cancel

    ; was exception generated by a MESSAGE call?
    if !error_state.name eq 'IDL_M_USER_ERR' then print,!error_state.msg else begin
      help, /last_message, out=out
      prstr,out, /nomore
    endelse

    ; calldetails.module will contain the name of the current procedure
    ;calldetails = get_calldetails(1)
    msg = 'Aborting ' ; + calldetails.module

    ; if we're nested then generate an exception via MESSAGE to jump to previous handler
    if hsi_is_obj_nested() and not yes_fromraw then message, msg

    ; otherwise, use MESSAGE to show that we're aborting, but then return a -1
    message, msg + '.  Returning -1.', /cont

    ; acs / kim 2007-03-15 needs to make
    ; sure the need_update() method
    ; returns 0 after an error occurs in
    ; an image algorithm.
    (self->getstrategy())->setdata, -1
    return, -1

  endif

  do_profile_plot = self->get(/profile_show_plot) or self->get(/profile_ps_plot) or self->get(/profile_jpeg_plot)
  do_viscomp_plot = self->get(/viscomp_show_plot) or self->get(/viscomp_ps_plot) or self->get(/viscomp_jpeg_plot)

  IF Keyword_Set( CLASS_NAME ) THEN BEGIN
    RETURN, self->Strategy_Holder::GetData( _EXTRA = _extra, $
      CLASS_NAME=class_name )
  ENDIF

  ; RAS comment doesn't apply anymore. Kim 24-May-2017
  ;RAS, 14-aug-2012, when we arrive here with eb_index = 0 and tb_index = 0 in _extra
  ;this will force reprocessing of the visibility bag even if the visibilities are
  ;consistent with the current im_energy_binning and im_time_binning
  self->Set, _EXTRA = _extra

  IF self.noupdate THEN BEGIN  ; I think self.noupdate is obsolete? It's never set to 1. Kim 24-May-2017
    Message, /reset
    RETURN, *self.data
  ENDIF ELSE BEGIN
    ; here we get the data from the image alg class.


    strategy = self->GetStrategy()
    alg_used = Obj_Class( strategy )

    ; hardcoded not good need to be changed eventually acs
    ;if alg_used ne 'HSI_MEM_NJIT' and alg_used ne 'HSI_VIS_FWDFIT' then begin
    if ~self->is_vis_alg() then begin
      ; if eventlist strategy is packet then check if the filename is valid.  If not,
      ; print message and return -1.  Otherwise it prints MANY message that
      ; it can't find file. kim
      ; If using simulations will just revert to default simulation if no file

      ev = self->get(/obj,class='hsi_eventlist') ;kim 8-apr-2002 added this block
      this_strat = ev -> getstrategy()
      doimage=1

      if not hsi_use_sim() then begin
        IF Obj_Class( this_strat ) EQ 'HSI_EVENTLIST_PACKET' THEN BEGIN

          filename = self->Get( /FILENAME )
          IF Size( filename, /TYPE ) NE 7 THEN doimage=0 else $
            if filename[0] eq '' then doimage=0
        ENDIF
      endif

    endif else doimage = 1

    ; we need to check explicitely for the control parameters in modpat
    ; strat, where image_dim, pixel_scale, amd pixel_size are define,
    ; because in this case we will have to reprocess too

    ;     oo = self->get( class='hsi_modul_pattern_strategy__define', /obj )
    ;     if oo->get( /need_update ) then begin
    ;         self->set, /need_update
    ;         doimage = 1
    ;     endif else doimage = 1

    ; endelse

    if doimage then begin
      cbe_normalize = ((self->getcaller())->get(/obj, class='hsi_image_raw'))->get(/cbe_normalize)
      image=self->Strategy_Holder::GetData( _EXTRA = _extra, $
        ; these three need to go through getdata, they cannot go through the set method
        eb_index = eb_index, tb_index = tb_index, $
        time_range = time_range, $
        cbe_normalize = cbe_normalize )


      ; this might be a problem. check this out later acs
      ;if since_version('5.5') then heap_gc   ;kim 19-jul-2002.  Try this!!!!!
    endif else $
      message, 'No file available.  Not making image.', /info
    IF N_Elements( image ) EQ 0 THEN image = -1
    self->SetData, image

  ENDELSE

  if image[0] eq -1 then BEGIN
    message, 'No image generated', /noprint, /CONTINUE
    return, -1                  ;kim 02-apr-2002
  ENDIF
  strategy = self->GetStrategy()
  alg_used = Obj_Class( strategy )
  self->Set, ALGORITHM_USED = alg_used,  /no_update

  IF Keyword_Set( PLOT ) THEN self->Plot

  ; If profile plots selected, plot observed profiles overlaid by expected profile from image.
  ; Added 30-Apr-10, Kim.
;  ; For clean, use just the component map (final map minus residuals) for the profiles.
  image_use = image
;  if alg_used eq 'HSI_CLEAN' then begin
;    resid_map = strategy->get(/clean_resid_map)
;    image_use = image - resid_map
;  endif
  if do_profile_plot then hsi_image_profile_plot, self, vimage=image_use, /quiet


  ; this is dirty but it prevents me to do a fake class for the
  ; bproj/clean/sato strategy, but we dont care once all algs return
  ; their image in annsec coords, we'll take the if away.
  ;annsecalg=['HSI_BPROJ','HSI_CLEAN','HSI_PIXON','HSI_MEM_SATO','HSI_FORWARDFIT']
  ;use_annsec2xy = grep(alg_used, annsecalg) eq alg_used
  use_annsec2xy = ~(hsi_alg_units(alg_used)).is_vis
  strategy = (self->get(/obj, class='hsi_modul_pattern'))->getstrategy()
  is_cart = is_class( strategy,'hsi_cart_pattern')


  IF use_annsec2xy and ~is_cart then BEGIN
    ;NOQUINTIC_INTERP added, ras, 17-may-2010
    noquintic = Self->Get(/NOQUINTIC_INTERP)
    finescale = Self->Get(/FINESCALE)
    ;    image = HSI_Annsec2XY( image, self, NOQUINTIC=noquintic, FINESCALE=finescale )/ ( self->alg_unit() )
    ;ENDIF ELSE BEGIN
    ;   image = image / ( self->alg_unit() ) ;ras, 8-apr-2006, removed ;
    ;ENDELSE
    image = HSI_Annsec2XY( image, self, NOQUINTIC=noquintic, FINESCALE=finescale )
  endif
  ;image = image / ( self->alg_unit() )
  ;9-feb-2018, RAS, replace the line above with the renamed method
  ;Divides the image by the scaling factor and sets several control params
  ;Also, integrate the method into the main file
  Self->Scale_Image, image

  if do_viscomp_plot then $
    hsi_image_vis_plot, imobj=self.caller, image=image, _extra=_extra ;tb_index = tb_index, eb_index = eb_index

  Message, /reset

  return, image

  ;IF alg_used EQ 'HSI_BPROJ' OR alg_used EQ 'HSI_CLEAN' OR alg_used EQ 'HSI_MEM_SATO' THEN BEGIN
  ;    RETURN, HSI_Annsec2XY( image, self )
  ;ENDIF ELSE BEGIN
  ;    RETURN, image
  ;ENDELSE

END
;=================== =========================================================
;
;=================== =========================================================
;+
; PROJECT:  HESSI
;
; NAME:  hsi_image__plot
;
; PURPOSE:  plot method for hsi_image object
;
; CATEGORY:
;
; CALLING SEQUENCE:  obj -> plot
;
; INPUTS:  none
;
; OPTIONAL INPUTS (KEYWORDS):  any keywords to set in the hsi_image object
;
; OUTPUTS:  None
;
; OPTIONAL OUTPUTS:  None
;
; Calls:
;
; COMMON BLOCKS: None
;
; PROCEDURE:
;
; RESTRICTIONS: None
;
; SIDE EFFECTS: None.
;
; EXAMPLES:
;
; HISTORY:
; Written Kim, Andre Feb 2000
; Modifications:
;   Kim, June 1, 2000.  New det_index_mask, front_segment,rear_segment in control structure.
;     time_range and energy_band are now in info structure, not control
;   8/8/00, Kim, Use legend routine for legend, can pass in right_legend, bottom_legend  etc.
;     to control placement of legend in plot
;   Kim, Aug16, 2000, time_range and energy_band moved back to control (from info)
;       Kim, Oct 19, 2000.  Call hsi_get_image_timeused to get time interval of image
;       Kim, Oct 26, 2000.  Use absolute_time_range in info parameters to get time range of image
;   Kim, Jan 8, 2001, Call is_hessi_color.  If yes, then set scaling min and max to scale symmetrically around 0.0
;   Kim, Jan14, 2001.  Changes time format in legend to /vms.
;   Kim, Feb 17,2001.  plot_map needs time in tai format for correct solar-rotating
;       Kim, Feb 20, 2001. Add total # events (from info params) to plot label.  Also changed format of label.
;   Kim, Apr-12-2001, Added status and err_msg keywords
;   Kim, Aug-21-2002, Added test for image=-1 - just abort right away with message
;   3-Oct-3002, Kim.  Added show_atten keyword
;
;-
;============================================================================

PRO HSI_Image_single::PLOT, status=status, show_atten=show_atten, err_msg=err_msg,  _EXTRA = _extra

  status = 1
  err_msg = ''

  ; if user passed in an existing image array and control parameters in _extra keyword, use them

  if keyword_set(_extra) then begin
    if tag_exist(_extra, 'saved_data') then begin

      if ptr_valid (_extra.saved_data.data) then begin
        image = *_extra.saved_data.data
        control = *_extra.saved_data.control
        info = *_extra.saved_data.info
      endif
    endif
  endif

  ; otherwise get image array and control parameters from object.
  if not exist(image)  then begin
    ;if keyword_set( _extra ) then self->set, _extra = _extra
    image = self -> GetData(_extra=_extra )
    control = self -> get(/control)
    info = self -> get(/info)
  endif

  if image[0] eq -1 then begin
    status = 0
    err_msg = 'ERROR - No image data to plot.'
    message, err_msg, /cont
    return
  endif

  alg = hsi_get_alg_name (control.image_algorithm)
  hsi_image_plot, image, control, info, alg, $
    show_atten=show_atten, status=status, err_msg=err_msg, _extra=_extra

end
;--------------------------------------------------------------------

PRO Hsi_Image_Single__Define

  self = {Hsi_Image_Single, $
    noupdate: 0B, $
    caller: obj_new(), $
    old_use_phz: 0B, $
    old_strat_is_vis: 0B, $
    INHERITS Strategy_Holder }

END


;---------------------------------------------------------------------------
; End of 'hsi_image_single__define.pro'.
;---------------------------------------------------------------------------
