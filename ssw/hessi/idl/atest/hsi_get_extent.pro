;+
; NAME: hsi_get_extent
;
; PURPOSE:
;   Find minimum and maximum size of source within region(s) of RHESSI images. Can use
;   the whole image, interactively define ROIs, or pass in a filename containing the ROIs.
;   Can handle single images or image cubes. This is a wrapper around hsi_get_extent_single.
;
; METHOD:
;   PLOTMAN window allows users to interactively define regions containing sources (or can use
;   whole image, or pass in a filename containing the region(s).
;   Each source is rotated through 180 degrees in 1 degree increments, and the sigma
;   (standard deviation) at each angle is calculated.
;   Works for any rhessi image algorithm.
;   If working on image cube, output arrays have one or two extra dimensions for energy and/or time.
;
; CALLING SEQUENCE:
;   hsi_get_extent, o [, fitsfile=fitsfile,  $
;     , nobox=nobox, boxfile=boxfile, mark_each=mark_each, $
;     xs=xs, ys=ys, xm=xm, ym=ym, xk=xk, yk=yk, nbox=nbox,sources=sources,smapxy=smapxy]
;
; EXAMPLES:
;    hsi_get_extent, fitsfile='hsi_imagecube_...fits', boxfile='myboxfile.sav', xs=xs
;    hsi_get_extent, o, ys=ys
;
; INPUT:
;   o - rhessi image object
;   fitsfile - name of RHESSI image FITS file (single or cube)
;   nobox - if set, then use whole image, otherwise suppply boxfile or a widget will pop up to define boxes
;   boxfile - name of file to read containing boxes (written by plotman markbox widget)
;   mark_each - if set (and nobox is not set, and boxfile is not provided) then allow user to
;     interactively define ROI in each image. Otherwise, after marking first image, same region is
;     used for the rest of the images.
;
; OUTPUT:
;   xs,ys - [180,nbox,nen,ntime] array of sigmas in x,y direction at every angle in rotation (0-179, 1 degree increments)
;   xm,ym - [nbox,nen,ntime] mean x,y value that source is rotated around
;   xk,yk -  - [180,nbox,nen,ntime] array of kurtosis in x,y direction at every angle in rotation
;   nbox - number of boxes defined
;   sources - [nbox,nen,ntime] structure of x,y and image values used for each box
;   smapxy - [nx,ny,nen,ntime] image(s) in x,y coordinates
;
; WRITTEN:
;   Kim Tolbert, 7-Mar-2017
; MODIFIED:
;
;
;-
pro hsi_get_extent, o, fitsfile=fitsfile, $
  nobox=nobox, boxfile=boxfile, mark_each=mark_each, $
  xs=xs, ys=ys, xm=xm, ym=ym, xk=xk, yk=yk, nbox=nbox,sources=sources,smapxy

  common plotman_mark_box_common, boxes_save

  if ~is_class(o, 'HSI_IMAGE') then begin
    if ~keyword_set(fitsfile) then begin
      message,'You must provide an image FITS file, or an image object that has a time set. Aborting.', /cont
      return
    endif
    o = hsi_image()
  endif

  if keyword_set(fitsfile) then begin
    if file_exist(fitsfile) then o->set,im_input_fits=fitsfile else begin
      message,'FITS file does not exist. ' + fitsfile, /cont
      return
    endelse
  endif

  times = o->getaxis(/ut, /mean)
  ebands = o->getaxis(/energy, /mean)
  nt = n_elements(times)
  nen = n_elements(ebands)

  is_cube = (nt + nen) gt 2

  save_e_idx = o->get(/e_idx)
  save_t_idx = o->get(/t_idx)
  save_use_all = o->get(/use_all_info_params)
  save_use_single = o->get(/use_single_return_mode)

  o->set, /use_single_return_mode, /use_all_info_params

  for it = 0,nt-1 do begin
    for ie = 0,nen-1 do begin
      o->set, e_idx=ie, t_idx=it
      hsi_get_extent_single, o, nobox=nobox, boxfile=boxfile, xs=xsi, ys=ysi, xm=xmi, ym=ymi, xk=xki, yk=yki, $
        nbox=nbox,sources=sourcesi,smapxyi

      ;If input is a cube, now that we have dimensions for a single image for
      ; the output arrays, re-dimension that for number of energy and time bins.
      ; Also, if nobox or boxfile wasn't set, and user didn't set mark_each, then user will define box(es)
      ; on first image, and we'll save the box(es) in a file for use with subsequent images.
      if is_cube and ie eq 0 and it eq 0 then begin
        nang = (size(xsi,/dim))[0]
        xs = rebin(xsi, [nang, nbox, nen, nt])
        ys = rebin(ysi, [nang, nbox, nen, nt])
        xm = rebin(xmi, [nbox, nen, nt])
        ym = rebin(ymi, [nbox, nen, nt])
        xk = rebin(xki, [nang, nbox, nen, nt])
        yk = rebin(yki, [nang, nbox, nen, nt])
        sources = reproduce(reproduce(sourcesi, nen), nt)
        ; Leading dimension of 1 is lost by reproduce, so for 1 box make sure it has that dimension
        if nbox eq 1 then sources = reform(sources, 1, nen, nt)
        smapxy = rebin(smapxyi, [size(smapxyi,/dim), nen, nt])

        if ~(keyword_set(nobox) or keyword_set(boxfile) or keyword_set(mark_each)) then begin
          boxes_savefile = boxes_save
          boxfile = 'boxes_' + time2file(!stime) + '.sav'
          save, file=boxfile, boxes_savefile
          message, 'Your boxes were saved in ' + boxfile, /cont
        endif
      endif else begin
        xs[*,*,ie,it] = xsi
        ys[*,*,ie,it] = ysi
        xm[*,ie,it] = xmi
        ym[*,ie,it] = ymi        
        xk[*,*,ie,it] = xki
        yk[*,*,ie,it] = yki
        sources[*,ie,it] = sourcesi
        smapxy[*,*,ie,it] = smapxyi
      endelse
    endfor
  endfor

  o->set, use_single_return_mode=save_use_single, use_all_info_params=save_use_all
  o->set, e_idx=save_e_idx, t_idx=save_t_idx

  ; Reform in case only one time or energy
  xs = reform(xs)
  ys = reform(ys)
  xm = reform(xm)
  ym = reform(ym)
  xk = reform(xk)
  yk = reform(yk)
  sources = reform(sources)
  smapxy = reform(smapxy)

end