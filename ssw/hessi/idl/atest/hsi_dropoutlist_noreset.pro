;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;+
; :Description:
;
; 1. Prepare the deadacc_arr to hold the deadtime accumulation. The total dropout time in seconds.
;    A. Check the time binning.  If it is a pointer for multiple time bases (binned_eventlist), then
;       then the deadacc array must also be a pointer.  Otherwise a simple array.
; 2. Find the dropout starts and ends.  A dropout starts on a reset, and ends on the
;   first pha event (channel ge 0) more than 75 usec after the reset.  There may be several
;   resets before the first event.
; 3. Having found the dropouts segregate them by type and add the appropriate deadtime
;   A. Dropouts wholly contained within a timebin.  Typical for spectroscopy accumulations
;     with time_bins longer than 1 second.  All of the dropout time is accumulated in this bin
;   B. Dropouts straddling a time_bin boundary.  Divide the dropout time proportionately.
;   C. Timebins wholly contained within a dropout.  Set their acc time to the timebin duration
;     and proportionately assign the extra to the partial timebins.
; 4. Save any open active resets to allow for multiple entrances when accumulating over bunches.
;    Describe the procedure.
;
; :Params:
;    ev  - eventlist structure
;    ut_ref
;    saved_reset - no longer used
;
; :Keywords:
;    obj - eventlist_packet object
;    done
;    livetimelist - livetime structure array
;
; Restrictions: Returns empty arrays unless DP_ENABLE set and TIME_UNIT is 1 in object parameters
; History:
;   richard.schwartz@gsfc.nasa.gov 25-mar-02, based on algorithm developed with G Hurford and D Smith
;    3-may-2002, fixed crash from calling lindgen with single element vector, 5.3 needs a scalar.
;   7-may-2002, protect against no data for a2d, lines are transposed. ras
; 9-may-2002, ras, merge overlapping drop_outs which arise due to rejection
;   of photons within photon cutoff window
;   21-may-2002, ras, use spectrogram::accbin to set up accumulation bins
;   13-jun-2002, ras, added prebin to deadacc
;   21-jun-2002, ras, added EXTEND_TIME_RANGE to last_photon time
;   24-jun-2002, ras, fixed bug which caused crash with single time interval
;   1-jul-2002, ras, fixed bug due to reset index increasing beyond range of channel array.
;   17-jul-2002, ras, fixed bug from 13-jun fix introduced for intervals straddling
;     time boundaries
;   14-aug-2002, ras, fixed bug from 24-jun fix. This bug caused the dp_cutoff value to
;     be ignored resulting in anomalously high apparent dropout rates for low count rates
;   25-sep-02, ras, dropout split into two parts. The first is dropoutlist that is moved inside
;   the eventlist object.  The second is dropout_build that remains in spectrogram to
;   fill the accumulation array, deadacc_arr.
;
;   New version required for data from 28-nov-02 through 10-apr-03 when csa resets were unknowingly disabled
;   10-apr-02 - remove test for reset in ha array.  Only need to look for
;   valid events.
;    20-apr-2005, ras, added dp_extend to lengthen the datagaps and
;    set the coinc flag on all events within datagaps.
;   11-jan-2007, ras, allow for structure in dp_extend so different
;     a2d may be treated differently, added dp_extendi for individual
;     checks.
;   27-feb-2007, ras, dp_cutoff becomes a structure so cutoff duration is now rate dependent
;
;   12-aug-2011, ras, smooth(...,/edge)->smooth(...,/edge_truncate) to support idl version 8
;   2-feb-2017, ras, added ramp tests and removed saved_resets as we won't use csa resets
;  27-feb-2017, ras added min=0 to histogram, - terribly dangerous bug without it
;  2-mar-2017, ras, using info param, info_dp_extend to report dp_extend values used before and after gaps
;  14-mar-2017, ras, convert a2d_index to seg_index before histogram, suppress dp_cutoff calc for rears
;  5-may-2017, ras, separated from hsi_dropoutlist file, no-csa method is now the only acceptable
;   version for the entire mission, remove high deadtime att_state 0 data after 2014 as it cannot
;   be corrected for ramps and is unsuitable in any event
;   fixed ngt0 test bug, ras, 11-may-2017
;   ;ras, 13-may-2017, bugfix!!! reference itramp back to time array

;-
Function HSI_Dropoutlist_noreset, ev, ut_ref, saved_reset,  obj=obj,$
  done=done, $
  livetimelist = livetimelist





  If keyword_set( obj ) then self = obj
  coinc_offset = obj->get(/dp_coinc_offset)  ;check to see if it's defined, must be -3 or less
  coinc_offset = coinc_offset ge -2 ? -5 : coinc_offset

  nseg= 18 ;changing from 9 to 18 to set the livetime to zero for rears
  ;when the rears are off even though there are no traditional datagaps in the rear
  nadd  = 10000L

  two20 = 2.0^20
  ;dp_cutoff = Self->Get(/DP_CUTOFF), since dp_cutoff can't exist as a structure
  ;construct one from the elements in the control structure. hsi_f_dp_cutoff takes
  ;care of the details
  dp_cutoff = hsi_f_dp_cutoff(OBJ=obj)
  Self -> Set, info_dp_cutoff = dp_cutoff
  checkvar, valid_pht_time, 75 ;time difference to next valid photon to closeout reset
  lld = Self->Get(/DP_LLD)
  If lld[0] eq -1 then lld = [51,49,52,54,52,44,50,46,40 ]
  uld = Self->Get(/DP_ULD)
  obs_time = self->get(/obs_time_int)
  cf = hsi_get_e_edges( /coef, gain_time = obs_time[0])
  lld = (long( (-cf[*,0] - 5)/cf[*,1] ))[0:17] > 0


  ;  saved_reset = is_struct( saved_reset) ? $
  ;    (tag_names(/struc, saved_reset ) eq 'SAVED_DROPOUT'? saved_reset : 0 ) : 0
  ;  if not is_struct( saved_reset ) $
  ;    then saved_reset = replicate( {saved_dropout, ut_ref:-1.0d0, rst_time:0L}, 9)

  seg = (ev.a2d_index mod 9) + 9 * ( (ev.a2d_index < 17 ) /9) ;convert to seg index
  h = histogram( min=0, seg, max=17, rev=r )  ;added min=0, 27-feb-2017, ras - terribly dangerous bug without it

  ;ut_ref = obj->get(/ut_ref)
  lt_ut_ref_sec79 = hsi_sctime2any(obj->get(/lt_ut_ref))
  ir=0
  DP_ENABLE = Self->Get(/DP_ENABLE)
  dpx = reform_struct( Self->Get(/dp_extend, /dp_append, /dp_prepend ) ) ;gt_dp_extend(Self)

  TIME_UNIT = Self->Get(/TIME_UNIT)
  ut_ref64 = hsi_any2sctime( ut_ref, /L64)


  ;For each segment (front only for now, 28-may-02, ras), identify the valid datagaps
  ;and accumulate a deadtime.
  ;Valid datagaps start with a CSA reset (channel value of -2), and they extend until
  ;the first photon event more than 75 microseconds after the initiating reset.
  ;To be used to accumulate deadtime, the duration of the dropout must exceed the
  ;the DP_CUTOFF parameter value.
  ;
  nev = n_elements(ev)
  info_dp_extend = fltarr(2,18) ;save as info parameter
  ramp_ctrl = obj->get(/ramp,/control)
  for ir=0, nseg-1 do begin
    ;Establish a catch within the detector loop.  This will make this
    ;routine far more robust
    ; only 1 time for each det now, if 0, then
    ;don't include this effect
    cfi = cf[ir,*]
    ch_bulk_lim = fcheck( dp_bulk_lim, 20.) / cfi[1] - cfi[0]
    dpxi = dpx[ir]
    dp_extendi  = [dpxi.dp_prepend_def, ( (anytim(ut_ref,/sec) ge anytim(dpxi.dp_extend_utlim,/sec) ? $
      ( dpxi.dp_extend_sec > 0.0 <0.04) :  0.0 ) > dpxi.dp_extend_def) > dpxi.dp_append_def]
    info_dp_extend[0,ir] = dp_extendi
    if hsi_get_debug() eq 0 then catch, error_catch
    if fcheck(error_catch,0) ne 0 then begin
      message, /continue, 'Warning: problem finding dropouts at '+$
        hsi_sctime2any( ut_ref64 + time[0], /vms)
      ;stop
      goto, catch_out
    endif

    if DP_ENABLE and (TIME_UNIT eq 1) then begin
      if h[ir] ge 2 then begin ;lines transposed before 7-may-2002, ras
        ;SEL holds the indices of the current segment.
        rsel = reverseindices( r, ir, count = nrsel ) ; r[ r[ir]:r[ir+1]-1 ]
        time = ( ev.time )[rsel]
        channel = ( ev.channel )[rsel]
        att_state = have_tag( ev, 'att_state' ) ? (ev.att_state)[rsel] : 4 ;use uncertain att_state when not passed


        if avg( obs_time ) gt anytim( '1-jan-2014') and ir le 8 then begin

          ;look for anomalous ev.channel in front segs when in att_state 0
          if (where_arr( get_uniq( att_state ), 0))[0] eq 0 then begin
            find_changes, att_state, ix_att, ix_att_state, index2d=index2d, count=acount
            time2d = time[ index2d ]/two20
            wtime2d = time2d[1,*] - time2d[0,*]
            atten0_test = Self->Get(/atten0_test)
            ltcounter_threshold = hsi_corrected_livetime( atten0_test, ir, /rev)*511 ;nominally 250.

            for iac = 0, acount-1 do begin

              lsel = where( livetimelist.seg_index eq ir, nlsel)
              if nlsel gt 0 then livesel = livetimelist[lsel]
              ;is the interval too short or is the ix_att_state 4? If so, set the channels to their neg -4
             case 1 of
                wtime2d[ iac ] lt 1.0: begin
                  q2d = value_locate( time, time2d[*, iac]*two20 ) > 0 < (n_elements( time ) -1)
                  channel[ q2d[0]:q2d[1] ] = -channel[ q2d[0]:q2d[1] ] + coinc_offset
                end
                ix_att_state[ iac  ] eq 0: begin ;check avg livetimelist.counter for indication of pileup
                  q2d = value_locate( livesel.time / 2048.0, time2d[*, iac] ) > 0 < (n_elements( livesel)-1 );time in sec
                  i2d = index2d[*,iac]
                  ;Compute the average counter
                  avcounter = avg( livesel[q2d[0]:q2d[1]].counter )
                  avchannel = avg( channel[ i2d[0]:i2d[1] ] < (5.0*ch_bulk_lim ) )
                  thirtykev = 30.0 / cfi[1] - cfi[0]
                  if avcounter lt ltcounter_threshold  && $
                    avchannel gt thirtykev then begin
                    
                    channel[ i2d[0]:i2d[1] ] = -channel[ i2d[0]:i2d[1] ] + coinc_offset
                  endif
                end
                else: iac = iac
              endcase
            endfor
            ;look for att_state 0 and a low livetimelist counter lt 250, remove those data
            ;because of pileup
          endif
          qgt0 = where( channel gt 0, ngt0 )
          if ngt0 gt 100 then begin  ;fixed ngt0 test bug, ras, 11-may-2017
            changt0 = channel[ qgt0 ]
            timegt0 = time[ qgt0 ]
            itramp = hsi_dp_ramp( changt0, timegt0, cfi, seg= ir, _extra = ramp_ctrl) < (nrsel-1)
            ;hsi_dp_ramp( changt0, timegt0, cfi, seg=ir, $
            ;  channel_max = channel_max, ntest = ramp_ctrl.ramp_ntest, $
            ;  peak = ramp_ctrl.ramp_peak , ch_bulk_lim = ch_bulk_lim ) <(nrsel-1)
            itramp = qgt0[itramp]  ;ras, 13-may-2017, bugfix!!! reference itramp back to time array
            channel[ qgt0 ] = changt0
            time[ qgt0 ] = timegt0
            nvalid  = n_elements( itramp )/2
            for jj = 0L, nvalid -1  do $
              channel[itramp[0,jj]:itramp[1,jj]] = (-channel[itramp[0,jj]:itramp[1,jj]] + coinc_offset) < coinc_offset
          endif
        endif

        ;Make sure channels are greater than LLD
        ;
        lsel = where( channel gt lld[ir] and channel lt uld[ir], nsel)
        pretime = 0
        if nsel ge 2 then begin
          channel = channel[lsel]
          time    = time[lsel]
          ;
        endif

        ord = sort(time)
        time = time[ord]
        channel = channel[ord]
        ; If we have requested the whole eventlist, done won't be defined
        ; so set it to 1. ras, 12-nov-02
        ;if not exist(done) then stop
        posttime = 0

        If fcheck( Done,1) then begin ;add one last photon to close any dropouts
          ;If Done then begin

          last_time =(( hsi_any2sctime(/L64, Self->get(/absolute_time_range) + $
            Self->get(/extend_time_range)) - $
            hsi_any2sctime(ut_ref,/L64) )[1] + valid_pht_time + 5 ) $
            > last_item(time) ;add the 5 as a fudge
          time = [time, last_time ]
          channel = [channel, 100]
          posttime = 1

        endif

        diff = time[1:*]-time
        npht = n_elements(time)
        ;Evaluate for cutoff that's a function of rate based
        ;on nearest 100 diffs with outliers removed
        if ir ge 9 then cutoff =  2LL^20 * 60 else begin ;60 seconds to declare a gap on a rear seg

          srt= npht gt 20 ? 1./smooth(diff,(npht-2)<101,/edge_truncate) : 100.
          cutoff = hsi_f_dp_cutoff( srt, dp_cutoff )
          ;                 acutoff= avg(cutoff)
          ;                 if ir eq 7 then help,acutoff
        endelse
        tc  = time
        ; Trap time degeneracy and change the values so it will work
        ; with the remainder of the tests
        ;
        ; The first case has a reset and event with the same time, but
        ; the ordering has placed the reset first.  Here, make the
        ; event come and make the reset come after by a microsecond
        sel = where( diff ge cutoff, nsel)
        if nsel ge 1 then begin
          ;if ir ge 9 then stop ; and nsel gt 1 then stop
          drop_time_start = time[sel]-long(dp_extendi[0]*two20)
          drop_time_end   = time[sel+1]+long(dp_extendi[1]*two20)

          drop_out =  n_elements(drop_time_start) eq 1 ? $
            reform([drop_time_start, drop_time_end],2,1) : $
            [ transpose(drop_time_start), transpose(drop_time_end)]
          hsi_dropout_extend_4_valid, time, channel, drop_out, dpxi=dpxi ;also cleans overlaps
          ;            ;Look for any overlaps
          ;            if n_elements(drop_out) ge 4 then begin
          ;              overlap = where(drop_out[1,*] gt drop_out[0,1:*], nover)
          ;              if nover ge 1 then drop_out[1,overlap] = drop_out[0,overlap+1]
          ;            endif

          ;Now that we know where the dropouts are, we can go back
          ;to the original channels and set the drop_out interval channels to 0
          time =(ev.time)[rsel]
          channel = (ev.channel)[rsel]

          hsi_dropoutlist_livetimelist_zero, seg=ir,  lt_ut_ref_sec79, livetimelist, $
            drop_out, ut_ref, rev = segrev, hseg=hseg

          hsi_dropoutlist_remove, time, channel, drop_out, coinc_offset = coinc_offset
          ;if saved then begin
          if 0 then begin ;no longer using resets!
            sdrop_out = [saved_reset[ir].rst_time - long(dp_extendi[0]*two20), last_item( time )]
            hsi_dropout_extend_4_valid, time, channel, sdrop_out, dpxi=dpxi
            hsi_dropoutlist_livetimelist_zero, seg=ir,  lt_ut_ref_sec79, livetimelist, $
              sdrop_out, ut_ref, rev = segrev, hseg=hseg

            hsi_dropoutlist_remove, time, channel, sdrop_out

          endif

          if nev eq 1 then ev.channel[rsel] = channel else ev[rsel].channel = channel

          seg_index = ir

          if n_elements(dplist) eq 0 then begin
            npack = 100

            dplist1 = hsi_dropout_block( seg_index, drop_out, ut_ref64, npack=npack)
            ndplist = n_elements( dplist1 ) * 20
            dplist = replicate(dplist1[0], ndplist)
            dplist[0] = dplist1
            idplist = 0L + n_elements( dplist1 )

          endif else begin

            dplist1 = hsi_dropout_block(seg_index, drop_out, ut_ref64, npack=npack)
            n1 = n_elements(dplist1)

            if ( idplist + n1 ) gt ndplist then begin
              dplist = [dplist, replicate( dplist1[0], (2 * (nseg-ir-1) * n1 )> n1 )]
              ndplist = n_elements( ndplist )
            endif
            dplist[idplist] = dplist1
            idplist = idplist + n1
          endelse
          ;if saved eq 0 then saved_reset[ir].ut_ref = -1.0d0
          ;Save any open resets

        endif
      endif
    endif


    ;  endif
    ;Jump here for indexing errors
    Catch_out: catch, /cancel
  endfor
  Self->Set, INFO_DP_EXTEND = info_dp_extend


  return, keyword_set(idplist) ? dplist[0:idplist-1] : -1
end
