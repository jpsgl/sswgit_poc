;+
; Name: hsi_ui_img_doimage
; Purpose: Called by hsi_ui_img to either RHESSI images for single or multiple time and energy interval
;   The images are shown as panels in the GUI or written to a FITS file, or both
;
; Written: Kim Tolbert, 2001
; Modifications:
;   5-Jul-2002, Kim.  Major rewrite.  Allow writing in FITS file while also sending to GUI.  Added
;	   progress bar.
;	20-Aug-2002, Kim.  Changed order of commands when writing a FITS file so that control
;		structure that gets written is correct for the first image in cube, and so that
;		separate info structures get passed to fitswrite for each image in cube.
;	08-Oct-2002, Kim.  When sending images to GUI, set noplot=1 for all but last image.
;	14-Jul-2003, Kim.  Pass state.clean_full_info to fits_write
;	16-Oct-2003, Kim.  Speed cubes up a little by switching to im_energy_binning, eb_index instead
;	   of energy_band
;	? Feb-2005, Kim.  Lots of changes for new image cube object
;	28-Apr-2006, Kim.  Improved error messages
;	16-Nov-2006, Kim.  Removed delay_set_pulldown stuff. Not needed anymore.  Also removed
;		don't unmap main widget while importing images.
;	1-Feb-2007, Kim.  If total(data) is 0 don't go any further.  Previously if error in image(s)
;		kept going and in plot loop,hit next getdata and tried making image(s) again.
;	15-Mar-2007, Kim.  Now we've changed hsi_image_single (now if an image fails, it
;		does a setdata,-1 for the image alg strategy, so doesn't redo on next getdata), so
;		removed 1-feb change.  When last image was all zeros, wouldn't show any images.
;	9-May-2007, Kim. In dogui section, take most of the code and put it into hsi_image__plotman, so
;		that we have the same options there (choosing which images to plot, naming the panel
;		desc something meaningful etc). Previously had some duplicate code, and not as many
;		options in plotman method.
;	29-Nov-2017, Kim. Change the message asking if really want to make cube to one that can be
;	 suppressed (by using xanswer routine instead of dialog_message). Also, don't pop up window
;	 to say wrote FITS file, just print in IDL log.
;-

pro hsi_ui_img_doimage, state, gui=gui, file=file, status=status

  ;Here we are checking im_energy_binning for self-consistency. sp_energy_binning will be set accordingly
  status = hsi_vis_check_ebins(state.data_obj, msg=msg)
  if ~status then begin
    msg = [msg, $
      '', $
      'Aborting making images.', $
      '', $
      'Adjust your energy edges and try again.']
    a=dialog_message(msg, /error)
    status = 0
    return
  endif

  imagefile_input = is_string(state.data_obj -> get(/im_input_fits))
  dogui = keyword_set(gui)
  dofile = keyword_set(file) and not imagefile_input

  times = state.data_obj -> getaxis (/ut, /edges_2)
  ntimes = n_elements(times)/2
  ebands = state.data_obj -> getaxis (/energy, /edges_2)
  nebands = n_elements(ebands)/2
  ntotal = ntimes * nebands

  n_images_done= state.data_obj -> get(/n_images_done)

  status = 0	;assume failure, and if we get to end, set to success

  if ntimes eq 1 and nebands eq 1 then docube = 0 else begin
    if not imagefile_input then begin
      if state.data_obj -> need_update() then begin
        msg = ['Are you sure you want to make ' + strtrim(ntimes*nebands,2) + ' images?', $
          '', $
          'Remember:  If you are making a large number of images, you may run out of memory', $
          '  if you send them to the GUI.  Better to send them to a FITS file only,', $
          '  or make them in memory (unselect GUI and FITS).', $
          '  You can import selected images into the GUI later from memory or the FITS file.']
        instruct = 'Do you want to make the image cube?'
        answer = xanswer(msg, instruct=instruct, /suppress, /check_instruct)
        if answer eq 0 then return
      endif else begin
        if n_images_done lt ntotal then begin
          msg = ['This image cube is not complete.', $
            'It contains ' + trim(n_images_done) + ' images of the originally requested ' + trim(ntotal)+'.', $
            ' ', $
            'Do you want to regenerate the cube?', $
            ' ', $
            'If you answer No, you can still extract existing images for display from the incomplete cube.']
          answer = dialog_message(msg, /question)
          ; If Yes, then set need_update so image cube will process again.
          ; If No, then don't write file, but do let user choose existing images to plot
          if answer eq 'Yes' then state.data_obj -> set, /need_update else dofile = 0
        endif
      endelse
    endif
    docube = 1
  endelse

  if dofile then begin
    ; set output FITS filename to blank, so user will be prompted for name.  fitswrite will save im_out
    ; selection, so set it to blank again afterwards so that next getdata won't write overwrite that file.
    state.data_obj -> set, im_out_fits_filename=''
    state.data_obj -> fitswrite, err_msg=err_msg
    im_out = state.data_obj -> get(/im_out_fits_filename)
    state.data_obj -> set, im_out_fits_filename=''
    ; if aborted and was handled by catch, err_msg won't be set, so check if data is -1
    data = state.data_obj -> getdata()
    if err_msg ne '' or data[0] eq -1 then begin
      a = dialog_message ('Error.  FITS file not written.  Check IDL log for error messages.', /error)
      return
    endif
  endif

  ; If we read in an imagecube this will just get the images already created corresponding to t_idx,e_idx.
  ; Otherwise it will create them.
  if dogui then begin
    state.data_obj->plotman, plotman_obj=state.plotman_obj, /choose, error=error, group = state.widgets.w_top_base
    status = error ne 1
    if status eq 0 then return
  endif

  if not (dofile or dogui) then data = state.data_obj -> getdata()

  status = 1

end