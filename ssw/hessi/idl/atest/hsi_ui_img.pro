;+
; Name: hsi_ui_img
;
; Purpose: Widget interface to HESSI image object
;
; Written: Kim Tolbert, 1999
; Modifications:
;  21-Jan-2001, Kim - call hsi_do_plotman to draw plot (extracted code to hsi_do_plotman)
;  22-Jan-2001, Kim - changed weight param to flatfield
;  4-Feb-2001,  Kim - Made hsi_ui_img aware of changes in obs time, flare, and analysis
;    interval selection in Main GUI.  Call hsi_ui_img with pointers to
;    obs_time_interval and flare_str, instead of values.  Save
;    pointers as well as values.  Set timer on obs time label widget so that
;    every 3 seconds, use pointers to check whether obs_time_interval or flare number have
;    changed in main GUI, and if so, modify labels and image object time parameters
;    appropriately.  Intervals was already passed in as a pointer, but do same thing with it
;    (i.e. when timer event happens, check if intervals have changed, and modify accordingly.
;  11-Mar-2001, Kim.  Enabled pixon image alg.
;  25-Apr-2001, Kim. Added mark clean box option.  Also use hsi_get_alg_name
;    in hsi_ui_img_widget_update
;  22-May-2001, Kim.  Made pixel size float, and round image size calc.
;  30-May-2001, Kim.  4-second interval at peak is peak_time + [0,4], not [-2,2],
;     and use hsi_ui_time_label to write obs time label and flare on widget
;  11-Jun-2001, Kim.  Took care of modpat_skip not always in control params (e.g. memvis)
;  29-Jun-2001, Kim.  Added aspect_sim
;  9-Sep-2001,  Kim.  Added use_auto_time_bin, cbe_powers_of_two, cbe_digital_quality,
;       use_flux_var, smoothing_time
;  13-Oct-2001, Kim.  Got rid of menu bar.  Added reset, set manually, and help buttons.
;  5-Jul-2002, Kim.  Changed buttons to make Images or Movie File to one button with
;    choice of output - GUI, FITS, or both.  Added More Info button to explain.
;  30-Sep-2002, Kim.  In reset_times event, check if flarenumber changed, not whole structure
;    (match_struct returns 0 if there are Nan's in structure on Unix)
;  15-Oct-2002, Kim.  Added Change button for Obs Time Interval
;   18-Nov-2002, Kim.  Use * to indicate parameters that require reprocessing.
;   11-Dec-2002, Kim.  Added init keyword.  If set, initializes time_range and xyoffset
;     to peak 4 sec, and position of flare.
;   15-Dec-2002, Kim.  Added init keyword to call to hsi_chk_obj.
;   15-Jan-2003, Kim  Previously when you selected a new obs time interval or flare, the
;     image time was automatically set to be 4s at the peak of the flare, and the xyoffset was
;     automatically set to the position of the flare in the flare catalog.
;     Now, if the image time(s) selected still fall within the new observation time
;     interval, then they are not changed.  If they don't, then the image time is set to 4s
;     at the peak of the flare.  If there is no flare in new obs time interval, then the
;     image time is set to the first 4s of the obs time interval.  And if the flare number
;     doesn't change when you select a new obs time interval, then the program will not
;     attempt to change the xyoffset.  If the flare number does change, then the xyoffset
;     will be set to the flare position in the catalog only if the value is not [0.,0.].
;     Also added new '4s at peak' button to set image time quickly.
;   10-Feb-2003, Kim.  Fix an error in hsi_ui_img_newtime where relative time_range parameter
;     wasn't set properly if obs time changed, but still contained image time interval.
;   21-Feb-2003, Kim.  Use c_tags from xstruct, and only set params that changed into object.
;   02-Mar-2003, Kim.  Added /hessi, ch_energy arguments to hsi_sel_intervals call
;   19-Mar-2003, Kim.  hsi_sel_intervals renamed to xsel_intervals.  -1 returned, means cancelled.
;   17-June-2003, Xiaolin Li. added intermediate diagnostics button to gui to plot each selected detector separately
;   14-Jul-2003, Kim.  Added clean_full_info in state.  Controls whether for CLEAN, the intermediate maps
;     will get stored in the FITS file.
;   11-Sep-2003, Kim.  Added decimation_correction and use new use_flare_xyoffset flag to set xyoffset now,
;     and added Refresh button, just to update widget with any params that might have changed from command line.
;   3-Oct-2003, Kim.  Small correction to when manual parameter setting widget (xstruct) is cancelled.
;   12-Jan-2004, Kim.  Added 'Write Script' button
;   16-Mar-2004, Kim.  xsel_intervals now returns -99 if cancelled.  So check for int[0] > -1.
;   1-Apr-2004, Kim.  Show taper value for either natural or uniform weighting
;   9-Aug-2004, Sandhia,  Put the code for "manoptions" in a separate function xset_struct so that it can
;                             be used by other programs as well.
; ? Feb-2005, Kim.  Lots of changes for new image cube object
; 1-Mar-2005,  Kim.  get hessi_obs_obj from common now, not from state (uvalue) of hessi widget,
;         and added two write script options
; 13-May-2005, Kim - Add rear_decimation_correct option
; 29-Jun-2005, Kim. Fixed full_info option (hadn't changed for new image object), and pop
;   up a 'Reading' message while reading a fits file.
; 3-Nov-2005, Kim. Changed buttons so that Panel Display is now just Display-> and has
;   pulldown for the 3 sizes of image panel display, the separate detector bproj display,
;   and the show parameters display.
; 30-Mar-2006, Kim.  Added phz_n_roll_bins_max to list of script params
; 19-May-2006, Kim.  Added cbe_time_bin_floor to list of script params.  Also don't put
;   time_bin_def_vals and harm_vals in params struct anymore.  Let hsi_coll_widget set them.
; 23-May-2007, Kim.  Removed mem sato and mem vis from list of algs.
; 27-Apr-2007, Kim.  Added option to plot aspect under Display pulldown
; 27-Apr-2007, Kim.  If image or eventlist FITS file can't be read, display error and reset to old file.
; 24-Aug-2007, Kim.  Don't get and save info params in hsi_ui_img_getparams - I don't think we need them,
;   and it slows things down.
; 7-Mar-2008, Kim.  Call getdata for vis with eb_index,tb_index = 0,0
; 28-Mar-2008, Kim. Make clean uppercase in check for sensitizing widgets
; 25-Aug-2008, Kim. Call store_intervals instead of hsi_store_intervals
; 16-Sep-2008, Kim.  Added show_n_maps and show_n_chi to params for script
; 19-Dec-2008, Kim.  Added vis, vis_fwdfit, mem_njit params to script params, and added message
;   warning that the vf_srcin structure is not included in the script.
; 1-May-2009, Kim. Add vis_normalize to script params, make vis options insensitive for image fits input
; 16-Jul-2009, Kim. Call hsi_uv_options for uv_smooth options.
; 04-Aug-2009, Kim. Added 'Plot Goes' button. Plots image time +-10 minutes
; 30-Apr-2010, Kim. Added buttons to display obs and expected image profiles.  'Show' button for profile
;   will show profiles while making images, button under 'Display' will show after images are made.
; 20-Sep-2010, Kim. For Display pulldown, change text for profiles (vs time or angle) depending on use_phz_stacker.
; 19-Nov-2010, Kim. Added mc_ntrials, mc_show_plot to params to save in script
; 18-Jan-2011, Kim. Call hsi_coll_list_widget_set (new) instead of setting coll list widget directly here.
; 23-Sep-2011, Kim. Added menu options from Display -> Aspect Solution button to plot p_error, triangles, and dist
; 01-Nov-2011, Kim. Added 'Click Image to Set Map Center' button.  Changed 'arcsec' to 'asec' in widget to save space.
; 30-May-2012, Kim. Added clean_beam_width_factor, pixon_sensitivity to params_used
; 11-Sep-2012, Kim. Added vis_type pull-down. For vis file input, make 'click image to set center' not sensitive, and
;   make Change size button sensitive, but call hsi_imagesize_widget with input_sel so only size and dim can be set for
;   vis file input.
; 13-Nov-2012, Kim. In hsi_ui_img_widget_update, use strlowcase when checking for is_vis_alg. Also, make
;   vistype and vis params sensitive even for vis file input (but not for image file input)
; 18-Jan-2013, Kim. Added vis_type and vis_photon2el to params for script
; 09-may-2013 Federico Benvenuto added EM in 'algparm' uvalue
; 28-Apr-2015, Kim. Added aspect_p_error_threshold keyword to call to aspect p_error plot (previously was hard-coded to .4)
; 01-May-2015, Kim. Added pixon_full_pm_calc, pixon_background_model, pixon_variable_metric to pixon params to add to
;   script (previously just added pixon_snr because other variables were coded into it, but RAS changed hsi_pix_options to not)
; 27-Jul-2015, Kim. Added buttons to show clean boxes, show time intervals, and show energy intervals.
;   Also changed calls for aspect plots to send them to plotman interface instead of simple windows.
; 29-Jul_2015, Kim. Added option to plot distance from source to image axis from aspect solution.
; 29-Mar-2017, Kim. Commented out sato and memvis parameters from params_used list, and from event handler
; 24-May-2017, Kim. Added options for viscomp plot (screen or PS during generation or from display pulldown),
;   option to send profile plot to PS, removed show images and verbose options, and added a Force Redo button.
;   Also, made 'Write FITS file' a pulldown menu button, with two choices - image or vis file.  Previously by setting
;   vis_out_filename vis file was automatically written, but no more. Removed that option from hsi_vis_options.
; 16-Jun-2017, Kim. In hsi_ui_img_event, for 'show_clean_box' check cw_nop[0] instead of cw_nop in case there are multiple boxes
; 30-Jun-2017, Kim. Added /scroll to top widget and call widget_limit_ysize after all widgets are set up to control y size.
; 28-Jul-2017, Kim. Removed warning about vf_srcin not being written in script, because now it is.
; 09-Oct-2017, Roman Bolzern. Added VIS_CS.
; 27-Oct02017, M.A. Duval-Poo. Added FIVE_CS
; 01-Nov-2017, Kim. Changed FIVE_CS to VIS_WV
; 07-Feb-2018, Kim. Flatfield should be sensitive if alg is clean or bproj (previously only for bproj)
; 05-Jul-2018, Kim. Call hsi_params2script without hard-coded list of params_used. Now it's
;   smarter and figures out which params to write.
; 17-Jul-2018, Kim. Added jpeg option for profile plot and viscomp plot
;
;-

pro hsi_ui_img_getparams, obj, params

  alg = hsi_alg_units()
  alg = alg[where(alg.active)]
  image_algs = alg.name
  vis_types = hsi_vis_type(/all,/full)

  weight_vals = ['Natural', 'Uniform']

  control = obj -> get (/control_only)

  if tag_exist(control, 'sim_ut_ref') then $
    control = add_tag (control, hsi_get_total_time(control), 'obs_time_interval') ;try
  params = {image_algs: image_algs, $
    vis_types: vis_types, $
    weight_vals: weight_vals, $
    control: control}

end

;-----
pro hsi_ui_img_newobs, state

  temp = hsi_whichflare (state.obs_time_interval, err_msg=err_msg, /only_one)
  if temp ne -1 then begin
    flarenum = temp
    state.flare_str = hsi_getflare(flarenum, desc=desc, err_msg=err_msg)
  endif else begin
    flarenum = -1L
    state.flare_str = hsi_flarelist_extend({hsi_flarelistdata})
  endelse
  top_base = state.plotman_obj->get(/plot_base)
  widget_control, top_base, get_uvalue=top_state
  widget_event = {id:top_state.w_labels, top: top_base, handler: 0L, $
    value:state.obs_time_interval, flare_str:state.flare_str }
  widget_control, widget_event.id, send_event=widget_event

end

;-----

pro hsi_ui_img_newtime, state, init=init, new_flare=new_flare

  checkvar, init, 0

  if init then begin
    ;   checkvar, new_flare, 1  ;removed this new_flare stuff once use_flare_xyoffset stuff was added

    sim = state.data_obj ->get(/sim_ut_ref) ne  -1

    ;if data_obj is not a simulation, then set xyoffset from flare if there is one
    ;   if new_flare and not sim then begin
    ;
    ;     if is_struct(state.flare_str) then begin
    ;      if state.flare_str.id_number ne 0 then begin
    ;          if total(state.flare_str.position) ne 0. then begin
    ;             state.params.control.xyoffset = state.flare_str.position
    ;             state.data_obj -> set, xyoffset=state.params.control.xyoffset
    ;          endif else xtext,'No flare position in catalog yet.  Not setting xyoffset parameter. ', $
    ;             /just_reg, wait=2, /center
    ;      endif
    ;     endif
    ;
    ;   endif

    ;    state.params.control.obs_time_interval = state.obs_time_interval
    ;    if not tag_exist(state.params.control, 'sim_ut_ref') then $
    ;       state.data_obj -> set, obs_time_interval=state.obs_time_interval

    ; Set default time range if necessary.  In following order of precedence:
    ;   1. check if selected image times are still within obs time interval, if so, keep them
    ;   2. 4s at peak of flare if there is one
    ;   3. [0,4]

    times_ok = 0

    times = state.data_obj -> getaxis(/ut, /edges_2)
    mm = minmax(times)
    times_ok = ( mm[0] gt (state.obs_time_interval[0] - .001)  and $
      mm[1] lt (state.obs_time_interval[1] + .001) )

    if times_ok then begin
      ;       state.params.control.time_range = (*state.multi_times)[*,0] - state.obs_time_interval[0]
      ;       state.data_obj -> set, time_range=state.params.control.time_range
    endif else begin
      ; otherwise set to 4s at peak of flare, if there is a flare
      if is_struct(state.flare_str) then if state.flare_str.id_number ne 0 then $
        set_times = state.flare_str.peak_time + [0., 4.]
      if not exist(set_times) then set_times = [0.,4.] + state.obs_time_interval[0]
      ;ptr_free, state.params.control.im_time_interval
      state.params.control.im_time_interval = (set_times)

      state.data_obj -> set, im_time_interval = set_times

      ;       *state.multi_times = hsi_get_time_range (state.params.control, state.params.info)
    endelse

    state.params.control.xyoffset = state.data_obj -> get(/xyoffset)  ; make sure xyoffset is set for new times
  endif

  state.obs_time_label = hsi_ui_time_label (state.obs_time_interval, state.flare_str, /twolines)

end

;-----

function hsi_ui_img_newfile, state, file

  change = 0

  if file eq '' or not file_test(file,/read) then begin
    message,/info, 'No file selected or file does not exist: ' + file
    return, 0
  endif

  ; in each case, if the file hasn't changed, just return.
  ; If the input file isn't readable, a message will already have been printed
  ; about the error, so just set the file back to the old file.
  case state.input_sel of
    1: begin
      old_file = state.data_obj -> get(/im_input_fits)
      if same_data(file, old_file) then return,0
      state.data_obj -> set, _extra={im_input_fits: file}
      xmessage,['', '      Reading...       ', ''], wbase=wxmessage
      obst = state.data_obj -> get(/obs_time_interval)
      if obst[0] ne -1 then change = 1 else $
        state.data_obj -> set, _extra={im_input_fits: old_file}
    end
    2: begin
      old_file = state.data_obj->get(/ev_filename)
      if same_data(file, old_file) then return,0
      state.data_obj -> set, _extra={ev_filename: file}
      xmessage,['', '      Reading...       ', ''], wbase=wxmessage
      obst = state.data_obj -> get(/obs_time_interval)
      if obst[0] ne -1 then change = 1 else $
        state.data_obj -> set, _extra={ev_filename: old_file}
    end
    3: begin
      old_file = state.data_obj->get(/vis_input_fits)
      if same_data(file, old_file) then return,0
      state.data_obj -> set, _extra={vis_input_fits: file}
      ;vis = state.data_obj -> getdata(class='hsi_visibility', tb_index=0,eb_index=0)
      ;We insert the following lines to prevent regularization before it is needed
      obj_vis = state.data_obj->Get(/obj,class='hsi_visibility')
      obj_vis->Read
      vis_arr = obj_vis->framework::getdata()
      vis = vis_arr[0]
      vis = (ptr_valid(vis))[0] ? *vis[0] : vis
      if is_struct(vis) then begin
        change = 1
        obst = state.data_obj -> get(/obs_time_interval)
      endif else state.data_obj -> set, _extra={vis_input_fits: old_file}
    end
  endcase

  if xalive(wxmessage) then widget_control, wxmessage, /destroy

  if change then begin
    state.obs_time_interval = obst
    hsi_ui_img_newobs, state
    if state.input_sel eq 1 then hsi_ui_img_doimage, state, /gui
    ; if state.input_sel eq 2 then $
    ;   state.data_obj->set,im_energy_binning=state.data_obj->get(/energy_band)
  endif else a = dialog_message('Bad input file.  See IDL log for more information.', /error)

  return, change
end

;-----

pro hsi_ui_img_widget_update, state

  params = state.params
  w = state.widgets

  ; check that any of widgets are still alive (image widget might have been killed)
  if not xalive(w.w_obs_time_label0) then return

  ;label = hsi_ui_time_label (params.control.obs_time_interval, state.flare_str, /twolines)
  obs_label = hsi_ui_time_label (state.obs_time_interval, state.flare_str, /twolines)
  obs_label[0] = str_replace(obs_label[0], 'Observation Time Interval', 'Selected Time Range')
  widget_control, w.w_obs_time_label0, set_value=obs_label[0]
  widget_control, w.w_obs_time_label1, set_value=obs_label[1]

  ;index = widget_info (w.w_intlist, /droplist_select)
  ;widget_control, w.w_intlist, set_value=*state.pint_descs, set_droplist_select=index

  ;if ptr_exist(state.multi_times) then times=*state.multi_times else $
  ;   times = anytim (hsi_get_time_range (state.params.control, state.params.info))
  ;stime = anytim (*state.multi_times, /vms)
  times = state.data_obj -> getaxis(/ut, /edges_2)
  value = format_intervals(times,  /ut)
  ;value = stime[0,*] + '  to  ' + strmid(stime[1,*], 12, 99)
  ntime = strtrim(n_elements(value),2)
  s = ntime gt 1 ? 's' : ''
  widget_control, w.w_num_times, set_value='* ' + ntime + ' Image Time Interval' + s + ':'
  widget_control, w.w_time, set_value=value


  ;senergy = strtrim (string (*state.multi_ebands, format='(f9.1)'), 2)
  ;senergy = reform(senergy, size(*state.multi_ebands, /dim))
  ;value = senergy[0,*] + '  -  ' + senergy[1,*]
  value = format_intervals(state.data_obj -> getaxis(/energy, /edges_2), format='(f9.1)')
  nenergy = strtrim(n_elements(value),2)
  s = nenergy gt 1 ? 's' : ''
  widget_control, w.w_num_energy, set_value='* ' + nenergy + ' Energy Band' + s + ' (keV):'
  widget_control, w.w_energy, set_value=value

  ecode = state.data_obj -> get(/im_energy_binning)
  if n_elements(ecode) gt 1 then ecode = 0 else ecode = ecode + 1
  widget_control, w.wc_ecode, set_droplist_select=ecode

  spixel = strtrim (string (params.control.pixel_size, format='(f7.1)'), 2)
  value = 'Pixel Size (asec): ' + spixel[0] + '  x  '  + spixel[1]
  widget_control, w.w_pixel, set_value=value

  simagedim = strtrim (string (params.control.image_dim, format='(i5)'), 2)
  value = 'Image Dimensions (pixels): ' + simagedim[0] + '  x  ' + simagedim[1]
  widget_control, w.w_imagedim, set_value=value

  sxyoffset = strtrim (string (params.control.xyoffset, format='(f9.2)'), 2)
  value = 'Offset of Map Center from Sun Center (asec):  X: ' + $
    sxyoffset[0] + '    Y: ' + sxyoffset[1]
  widget_control, w.w_xyoffset, set_value=value

  im_size = round(params.control.pixel_size * params.control.image_dim)
  im_size_str = strtrim( string( im_size, format='(i10)' ), 2)
  im_area = round([params.control.xyoffset - im_size/2., params.control.xyoffset + im_size/2.])
  im_area_str = strtrim( string(im_area, format='(i10)' ), 2)
  value = 'Image Size = ' + im_size_str[0] + ' x ' + im_size_str[1] + ' asec' + $
    '    Range: X: ' + im_area_str[0] + ' to ' + im_area_str[2] + ' asec' +$
    '   Y: ' + im_area_str[1] + ' to ' + im_area_str[3] + ' asec'
  widget_control, w.w_sizeinfo, set_value=value

  hsi_coll_list_widget_set, w.w_colldisp, params.control, /image
  ;  colls = size(/n_dim,params.control.det_index_mask) ? params.control.det_index_mask gt 0 : $
  ;    total (params.control.det_index_mask, 2) gt 0
  ;  for ic = 0,8 do begin
  ;    widget_control, w.w_colldisp[ic,0], sensitive=colls[ic]
  ;    widget_control, w.w_colldisp[ic,1], sensitive=params.control.front_segment and colls[ic]
  ;    widget_control, w.w_colldisp[ic,2], sensitive=params.control.rear_segment and colls[ic]
  ;  endfor

  label = 'Automatic Time Bin Calculation: '
  value = params.control.use_auto_time_bin eq 1 ? 'Enabled' : 'Disabled'
  widget_control, w.w_auto_time_bin, set_value=label + value

  qual = string(params.control.cbe_digital_quality, format='(f4.2)')
  widget_control, w.w_digital_qual, set_value='Digital Quality: '+qual

  alg = strupcase(hsi_get_alg_name(params.control.image_algorithm))
  alg_index = (where (alg eq strupcase(params.image_algs)))[0]
  widget_control, w.wc_alg, set_droplist_select = alg_index
  image_plotted = is_class(state.plotman_obj, 'PLOTMAN') ? $
    state.plotman_obj->valid_window(/image) : 0
  is_vis_alg = (hsi_alg_units(alg)).is_vis

  vis_type = params.control.vis_type
  vis_index = (where(hsi_vis_type(vis_type,/full) eq params.vis_types))[0]
  widget_control, w.w_vis, set_droplist_select = vis_index

  val =  ''
  if tag_exist(params.control,'FLATFIELD') then $
    val = 'Flatfield:  ' + (params.control.flatfield eq 1 ? 'Enabled' : 'Disabled')
  if tag_exist(params.control,'MODPAT_SKIP') then $
    val = val + '     Modpat_skip: ' + strtrim (string (params.control.modpat_skip, format='(i6)'), 2)
  if tag_exist(params.control,'USE_PHZ_STACKER') then $
    val = val + '     Phase Stacker: ' + (params.control.use_phz_stacker ? 'Enabled' : 'Disabled')
  if tag_exist(params.control, 'use_cull') then begin
    val = val + '     Cull: ' + (params.control.use_cull ? 'Enabled' : 'Disabled')
    if params.control.use_cull then val = val + '  (Fraction: ' + trim(params.control.cull_frac,'(f4.2)') + ')'
  endif
  widget_control, w.w_flat, set_value=val

  label = 'Weighting:  '
  val = 'Natural'
  if tag_exist (params.control, 'uniform_weighting') then  begin
    if params.control.uniform_weighting then val = 'Uniform'
  endif
  if tag_exist (params.control, 'taper') then taper =  params.control.taper else taper = 0.
  val = val + '      Tapering Width (asec): ' + trim(taper, '(f7.2)')
  if tag_exist(params.control, 'use_local_average') then $
    val = val + '      Local Average: ' + (params.control.use_local_average ? 'Enabled' : 'Disabled')
  widget_control, w.w_weight, set_value = label + val

  val = 'Variable Flux Correction: ' + (params.control.use_flux_var ? 'Enabled' : 'Disabled')
  if tag_exist(params.control, 'DECIMATION_CORRECT') THEN begin
    decim = 'None'
    if params.control.decimation_correct then decim = 'Front'
    if tag_exist(params.control, 'REAR_DECIMATION_CORRECT') THEN $
      if params.control.rear_decimation_correct then decim = (decim eq 'None')  ? 'Rear' : decim + ', Rear'
    val = val + '      Decimation Correction: ' + decim
  endif
  if tag_exist(params.control, 'use_rate') then $
    val = val + '      Rate-based BProj: ' + (params.control.use_rate ? 'Enabled' : 'Disabled')
  widget_control, w.w_flux_var, set_value = val

  ;widget_control, w.w_send, set_value = [state.img_to_gui, state.img_to_file]

  widget_control, w.w_progbar, set_button=(state.data_obj->get(/progress_bar) eq 1)

  do_ps = state.data_obj->get(/profile_ps_plot) eq 1
  do_screen = (state.data_obj->get(/profile_show_plot) eq 1) and (do_ps eq 0)
  widget_control, w.w_prof_plot, set_value=[do_screen,do_ps]

  do_ps = state.data_obj->get(/viscomp_ps_plot) eq 1
  do_screen = (state.data_obj->get(/viscomp_show_plot) eq 1) and (do_ps eq 0)
  widget_control, w.w_viscomp_plot, set_value=[do_screen,do_ps]

  ; use 'eq 1' since they might be -1 and that's treated as 1.
  ;  widget_control, w.w_show, set_value= [state.data_obj -> get(/progress_bar) eq 1, $
  ;    state.data_obj -> get(/verbose) eq 1, $
  ;    state.data_obj -> get(/show_images) eq 1, $
  ;    state.data_obj -> get(/profile_show_plot)]

  ; All of the Change buttons have widget ids that start with 'wc' so we can find all of them
  ; and change to sensitive or insensitive.  Then, depending on the input selection, we can explicitly
  ; change the ones we want.
  wc = where (strmid(tag_names(w),0,2) eq 'WC', nc)
  widget_control, w.w_inputsel, set_droplist_select=state.input_sel
  case state.input_sel of
    0: begin  ;Raw input
      widget_control, w.w_inputtext, set_value=format_intervals(state.obs_time_interval, /ut)
      widget_control, w.w_inputchange, set_value='Change...'
      widget_control, w.w_doimage, set_value='Make/Plot Image(s)'
      for i=0,nc-1 do widget_control, w.(wc[i]), sensitive=1
      widget_control, w.wc_foursatpeak, sensitive = (trim(obs_label[1]) ne '')
      widget_control, w.wc_mark_clean, sensitive=(alg eq 'CLEAN') and image_plotted
      widget_control, w.wc_show_clean, sensitive=(alg eq 'CLEAN') and image_plotted
      widget_control, w.wc_visfitswrite, sensitive=is_vis_alg
    end
    1: begin  ;Image FITS input
      file = state.data_obj -> get(/im_input_fits)
      file = is_string(file) ? file : ''
      widget_control, w.w_inputtext, set_value=file
      widget_control, w.w_inputchange, set_value='Browse...'
      widget_control, w.w_doimage, set_value='     Plot Image(s)     '
      for i=0,nc-1 do widget_control, w.(wc[i]), sensitive=0
    end
    2: begin  ;Eventlist FITS file input
      file = state.data_obj -> get(/ev_filename)
      file = is_string(file) ? file : ''
      widget_control, w.w_inputtext, set_value=file
      widget_control, w.w_inputchange, set_value='Browse...'
      widget_control, w.w_doimage, set_value='Make/Plot Image(s)'
      for i=0,nc-1 do widget_control, w.(wc[i]), sensitive=1
      widget_control, w.wc_foursatpeak, sensitive = (trim(obs_label[1]) ne '')
      widget_control, w.wc_mark_clean, sensitive=(alg eq 'CLEAN') and image_plotted
      widget_control, w.wc_visfitswrite, sensitive=is_vis_alg
    end
    3: begin  ; Visibility FITS file input
      file = state.data_obj -> get(/vis_input_fits)
      file = is_string(file) ? file : ''
      widget_control, w.w_inputtext, set_value=file
      widget_control, w.w_inputchange, set_value='Browse...'
      widget_control, w.w_doimage, set_value='Make/Plot Image(s)'
      for i=0,nc-1 do widget_control, w.(wc[i]), sensitive=0
      widget_control, w.wc_changesize, sensitive=1
      widget_control, w.wc_changecoll, sensitive=1
      widget_control, w.wc_send_base1, sensitive=1
      widget_control, w.wc_alg, sensitive=1
      widget_control, w.wc_algparm, sensitive=1
      widget_control, w.wc_fitswrite, sensitive=1
      widget_control, w.wc_imgfitswrite, sensitive=1
      ;      widget_control, w.wc_visfitswrite, sensitive=0
    end
  endcase

  ; make vis options button insensitive if not a vis alg, or if image file is input

  widget_control, w.wc_visbase, sensitive = (is_vis_alg and state.input_sel ne 1)

  ; x axis of profile plots is time bins when phase stacker is off, roll angle when it's on
  disp_label = ['Count Profile vs Time', 'Count Profile vs Roll Angle']
  use_phz = state.data_obj->get(/use_phz_stacker) > 0
  widget_control, w.w_prof_display, set_value=disp_label[use_phz]

  ;widget_control, w.w_movie, sensitive=(ntime gt 1 or nenergy gt 1)

end

;-----

pro hsi_ui_img_event, event

  @hessi_obj_common

  ;help,event,/st

  widget_control, event.top, get_uvalue=state

  ; check if user clicked X in top right corner of window, and if so quit

  struct_name = tag_names (event, /struct)

  if struct_name eq 'WIDGET_KILL_REQUEST' then goto, exit

  if struct_name eq 'WIDGET_TRACKING' then if event.enter eq 1 then return

  if struct_name ne 'WIDGET_TIMER' then begin
    ;in case user changed any parameters from command line, make sure we're up to date
    hsi_ui_img_getparams, state.data_obj, params
    state = rep_tag_value (state, params, 'params')
    ; clear any error messages in message widget
    if xalive(state.w_message) then widget_control, state.w_message, set_value=''
  endif

  widget_control, event.id, get_uvalue=uvalue

  _extra = -1
  chg_msg = ''
  update_params = 0
  plotman_obj=state.plotman_obj

  case uvalue of

    'input': begin
      state.input_sel = event.index
      case state.input_sel of
        0: _extra = {im_input_fits: '', ev_filename: '', vis_input_fits: ''}
        1: _extra = {ev_filename: '', vis_input_fits: ''}
        2: _extra = {im_input_fits: '', vis_input_fits: ''}
        3: _extra = {im_input_fits: '', ev_filename: ''}
        else:
      endcase
    end

    'inputtext': begin
      widget_control, event.id, get_value=value
      value = value[0]
      if value eq '' then return

      ; this widget generates events when the user exits the box (WIDGET_TRACKING event)
      ; as well as when the user hits return.  In either case, take action only if the
      ; value changed.

      if state.input_sel eq 0 then begin  ; raw data is input, changing time interval
        tmp = unformat_intervals(value, /ut, err_msg=err_msg)
        if err_msg ne '' then begin
          message, err_msg, /cont
          return
        endif
        if same_data(tmp, state.obs_time_interval) then return
        state.obs_time_interval = tmp
        hsi_ui_img_newobs, state
        hsi_ui_img_newtime, state, /init

      endif else begin  ; some kind of file is input, change file if it's valid
        change = hsi_ui_img_newfile (state, value)
        if change then update_params = 1

      endelse

    end

    'reset_times': begin
      widget_control, state.widgets.w_obs_time_label0, timer = 3.
      new_time = same_data(state.obs_time_interval, *state.ptr_obs_time_interval) eq 0
      new_flare = same_data (state.flare_str.id_number, (*state.ptr_flare_str).id_number) eq 0
      if not (new_time or new_flare) then return
      state.obs_time_interval = *state.ptr_obs_time_interval
      state.flare_str = *state.ptr_flare_str
      ;state.data_obj -> set, obs_time_interval = *state.ptr_obs_time_interval
      hsi_ui_img_newtime, state, /init;, new_flare=new_flare
      hsi_ui_img_widget_update, state
    end

    'inputchange': begin
      if state.input_sel eq 0 then begin
        top_base = state.plotman_obj->get(/plot_base)
        widget_control, top_base, get_uvalue=top_state
        widget_event = {id:top_state.w_labels, top: top_base, handler: 0L, $
          value:[0.d,0.d], flare_str:hsi_flarelist_extend({hsi_flarelistdata}) }
        hsi_ui_obs, hessi_obs_obj, group=top_base, $
          obs_time_interval=top_state.obs_time_interval, widget_event=widget_event
      endif else begin
        title = 'Select ' + state.input_opts[state.input_sel]
        file = ssw_pickfile( file=file, title=title, filter='*.fits' )
        change = hsi_ui_img_newfile (state, file)
        if change then update_params = 1
      endelse
    end

    'manoptions': begin
      control = state.params.control
      status = xset_struct("Image", event.top, control, _extra)
    end

    'script': begin
      all = widget_info(event.id, /uname) eq 'all'
      out = hsi_params2script (state.data_obj, all=all)
    end

    'force': begin
      msg = ['This option will set the need_update flag on the image object,', $
        'forcing it to completely reprocess next time you click the Make/Plot image(s) button.', $
        '']
      answer = xanswer(msg, /suppress, instruct='Do you want to continue?')
      if answer then state.data_obj->set, /need_update
    end

    'refresh': update_params = 1

    'defstandard': begin
      ;       widget_control, /hourglass
      ;       temp_obj = obj_new('hsi_image')
      ;       control = temp_obj -> get(/control)
      ;       ; can't just use all of control - resets xypos, and other things we might not want to set
      ;       ; so explicitly add the parameters we want to change.
      ;       ;_extra = control
      ;       _extra = {energy_band: control.energy_band, $
      ;         det_index_mask: control.det_index_mask, $
      ;         a2d_index_mask: control.det_index_mask, $
      ;         front_segment:  control.front_segment, $
      ;         rear_segment:  control.rear_segment, $
      ;         time_bin_min:  control.time_bin_min, $
      ;         time_bin_def:  control.time_bin_def, $
      ;         pixel_size: control.pixel_size, $
      ;         image_dim: control.image_dim, $
      ;         image_algorithm: control.image_algorithm, $
      ;         flatfield: control.flatfield, $
      ;         modpat_skip: control.modpat_skip, $
      ;         use_auto_time_bin: control.use_auto_time_bin, $
      ;         cbe_digital_quality: control.cbe_digital_quality, $
      ;         aspect_sim: control.aspect_sim, $
      ;         uniform_weighting: control.uniform_weighting, $
      ;         taper: control.taper, $
      ;         use_flux_var: control.use_flux_var }
      ;       obj_destroy, temp_obj
      ;;       *state.multi_ebands = control.energy_band
      msg = ['Are you sure you want to reset the image object?', $
        '', $
        'When you reset the parameters to default values you will lose the image(s) ', $
        'you have generated.']
      answer = dialog_message(msg, /question)
      if answer eq 'No' then return
      obj_destroy, state.data_obj
      ; Removed time_range=[0.,4.] from next statement - im_time_int and obs_time_int now do it all. 24-may-2017, Kim
      state.data_obj = hsi_image(obs_time_interval = state.obs_time_interval, /progress_bar)

      hessi_img_obj = state.data_obj
      state.input_sel = 0
      hsi_ui_img_newobs, state
      hsi_ui_img_newtime, state, /init
      update_params = 1
    end

    'help': gui_help, 'gui_image_help.htm'

    'changetime': begin
      ;obs = state.params.control.obs_time_interval
      new_vals = xsel_intervals (group=event.top, $
        /hessi, $
        input_intervals=state.data_obj->getaxis(/ut,/edges_2), $
        /time, $
        msg_label = 'Choose one or multiple time intervals to create image for.', $
        ;valid_range=obs, $
        ;obs_time_interval=obs, $
        chg_msg=chg_msg, $
        plotman_obj=state.plotman_obj, $
        stored_intervals_obj=state.intervals_obj)
      ;print,'new times returned are:'
      ;ptim,new_vals
      if new_vals[0] gt -1 then begin
        ;         *state.multi_times = new_vals
        ;         _extra = {time_range: new_vals[*,0] - obs[0]}
        _extra = {im_time_interval: new_vals}
      endif
      a=xregistered('hsi_ui_img')
    end

    '4satpeak': begin
      new_vals = state.flare_str.peak_time + [0., 4.]
      ;       *state.multi_times = new_vals
      ;       _extra = {time_range: new_vals - state.obs_time_interval[0]}
      _extra = {im_time_interval: new_vals}
    end

    'showtime': begin
      if state.plotman_obj->valid_window(/utplot) then success=1 else state.plotman_obj->show_recent_panel_type, 'utplot', success
      if success then plotman_draw_int,'all', {plotman_obj:state.plotman_obj}, intervals=state.data_obj->get(/im_time_interval)
    end

    'changeenergy': begin
      obs = state.params.control.obs_time_interval
      new_vals = xsel_intervals (group=event.top, $
        /hessi, ch_energy=hessi_constant(/ql_energy_bins), $
        ;input_intervals=*state.multi_ebands, $
        input_intervals=state.data_obj->getaxis(/energy, /edges_2), $
        /energy, $
        msg_label = 'Choose one or multiple energy bands to create image for.', $
        valid_range=[1.,100000.], $
        obs_time_interval=obs, $
        chg_msg=chg_msg, $
        plotman_obj=state.plotman_obj, $
        stored_intervals_obj=state.intervals_obj)
      if new_vals[0] gt -1 then begin
        ;         *state.multi_ebands = new_vals
        ;         _extra = {energy_band: new_vals[*,0]}
        _extra = {im_energy_binning: new_vals}
      endif
      a=xregistered('hsi_ui_img')
    end

    'ecode': if event.index ne 0 then _extra = {im_energy_binning: event.index - 1}

    'showecodes': begin
      ebfile = hsi_loc_file('energy_binning.txt', path='$HSI_SPEC', count=count)
      if count gt 0 then more, rd_ascii (ebfile[0])
    end

    'showenergy': begin
      if state.plotman_obj->valid_window(/xyplot) then success=1 else state.plotman_obj->show_recent_panel_type, 'xyplot', success
      if success then plotman_draw_int,'all', {plotman_obj:state.plotman_obj}, intervals=state.data_obj->get(/im_energy_bin)
    end

    'changecoll': _extra = hsi_coll_widget (state.params, group=event.top, /image, chg_msg=chg_msg)

    'clickcenter': begin
      if state.plotman_obj->valid_window(/image, /message) then begin
        print, 'Click (any button) in the image to define the center of the map.'
        print, '(Note: all widgets and command line are blocked until you click in image.)'
        xy = state.plotman_obj->point()
        if n_elements(xy) ne 1 then begin
          print,'Map center location chosen: ', xy[0], xy[1]
          _extra = {xyoffset: xy[0:1]}
        endif
      endif
    end

    'changesize': _extra = hsi_imagesize_widget (state.params.control, group=event.top, input_sel=state.input_sel)

    'alg': _extra = {image_algorithm: state.params.image_algs( event.index )}

    'algparm': begin
      widget_control, /hourglass
      case strlowcase(hsi_get_alg_name( state.params.control.image_algorithm )) of

        'back projection': chg_msg = '   No back projection parameters to set.   '

        'clean':  _extra = hsi_clean_options (state.params.control, group=event.top)

        ;        'mem sato':  _extra = hsi_memsato_options (state.params.control, group=event.top)

        ;        'mem vis':  _extra = hsi_memvis_options (state.params.control, group=event.top)

        'pixon': _extra = hsi_pix_options (state.params.control, state.data_obj ->get(/verbose), group=event.top)

        'forward fit':  _extra = hsi_forwardfit_options (state.params.control, group=event.top)

        'mem njit': _extra = hsi_nj_options (state.params.control, group=event.top)

        'vis fwdfit': _extra = hsi_vf_options (state.params.control, group=event.top, obj=state.data_obj)

        'uv_smooth': _extra = hsi_uv_options (state.params.control, group=event.top)

        'vis_cs': _extra = hsi_vis_cs_options (state.params.control, group=event.top)

        'vis_wv': _extra = hsi_vis_wv_options (state.params.control, group=event.top)

        'em': _extra = hsi_em_options (state.params.control, group=event.top)

        else:
      endcase
    end

    'vis_type': _extra = {vis_type: state.params.vis_types(event.index)}

    'visparm': _extra = hsi_vis_options (state.params.control, group=event.top)

    'mark_clean_box': begin
      if state.plotman_obj->valid_window(/image, /message) then begin
        state.data_obj -> mark_clean_box, plotman_obj=state.plotman_obj, /noplot
        update_params = 1
      endif
    end

    'show_clean_box': begin
      if state.plotman_obj->valid_window(/image, /message) then begin
        cw_nop = state.data_obj->get(/clean_cw_nop)
        if cw_nop[0] gt 0 then begin
          state.plotman_obj->select
          cyan = state.plotman_obj->get(/cyan)
          cw_list = state.data_obj->get(/clean_cw_list)
          plotman_oplot_boxes,color=cyan,cw_pixels=cw_list,cw_nop=cw_nop,thick=2, /labelwindow
          state.plotman_obj->unselect
        endif else message,/info,'No clean boxes have been defined.'
      endif
    end

    'changemisc': begin
      ; only want flatfield widget to be sensitive if alg is back projection or clean
      alg = hsi_get_alg_name(state.params.control.image_algorithm)
      doflatfield =  alg eq 'Back Projection' or alg eq 'Clean'
      _extra = hsi_imagemisc_widget (state.params, doflatfield=doflatfield, group=event.top)
    end

    'params': state.data_obj -> params

    'doimage': begin
      gui = state.img_to_gui or is_string(state.data_obj -> get(/im_input_fits))
      hsi_ui_img_doimage, state, gui=gui, file=state.img_to_file
      ;clean box control params (clean_box, cw_list, cw_nop) can change while
      ; doing image, so get updated params and store in state structure.
      update_params = 1
    end

    'plot_goes': begin
      time_range = minmax(state.data_obj -> getaxis(/ut, /edges_2))
      ; if interval is < 5 min, expand by 10 min on either side
      if time_range[1]-time_range[0] lt 300. then time_range = time_range + [-600.,600.]
      goes_plot, time_range=time_range, plotman_obj=state.plotman_obj
    end

    'imgfitswrite': begin
      ; call the doimage routine so it will ask the 'are you sure' question if necessary.
      hsi_ui_img_doimage, state, gui=0, /file
    end

    'visfitswrite': begin
      visobj = state.data_obj->get(/obj, class='hsi_visibility')
      visobj->write, /namedialog
    end

    'panel_display': begin
      ; call the doimage routine so it will ask the 'are you sure' question if necessary.
      hsi_ui_img_doimage, state, gui=0, file=0, status=status
      uname = widget_info(event.id, /uname)
      size = uname eq 'BIGGER' ? 200 : (uname eq 'bigger' ? 100 : 50)
      if status then state.data_obj -> panel_display, single_im=size, /zoom, $
        /plotman, group=event.top
    end

    'movie': begin
      ; call the doimage routine so it will ask the 'are you sure' question if necessary.
      hsi_ui_img_doimage, state, gui=0, file=0, status=status
      if status then state.data_obj -> movie_gui
    end

    ; Li, call gui object to plot each selected detector separately
    'det_sep': begin
      state.data_obj->show_det_sep, err_msg=err_msg
      if err_msg ne '' then a = dialog_message(err_msg, /error)
    end

    'aspect': state.data_obj->plot_aspect, plotman_obj=plotman_obj,xyoffset=state.params.control.xyoffset

    'asp_diameter': state.data_obj->plot_aspect, /plot_diam, plotman_obj=plotman_obj

    'asp_distance': state.data_obj->plot_aspect, /plot_dist, plotman_obj=plotman_obj

    'asp_src_distance': state.data_obj->plot_aspect, /plot_source_dist, plotman_obj=plotman_obj,xyoffset=state.params.control.xyoffset

    'asp_triangle': state.data_obj->plot_aspect, /plot_triangle, plotman_obj=plotman_obj

    'asp_p_error': state.data_obj->plot_aspect, /plot_p_error, plotman_obj=plotman_obj, aspect_p_error_threshold=state.data_obj->get(/aspect_p_error_threshold)

    'cntprofile': begin
      uname = widget_info(event.id, /uname)
      rate = 0  &  sigma = 0
      if uname eq 'prof_rate' then rate = 1
      if uname eq 'prof_resid' then resid = 1
      ; set ps to 0 explicitly since want it to be on screen and profile_ps_plot parameter might be set
      hsi_image_profile_plot, state.data_obj, rate=rate, resid=resid, ps=0
    end

    'vis_comp': hsi_image_vis_plot, imobj=state.data_obj, ps=0, /last

    'image_dest': begin
      if event.value eq 0 then state.img_to_gui = event.select
      if event.value eq 1 then state.img_to_file = event.select
    end

    'progbar': _extra = {progress_bar: event.select}

    'profplot': begin
      do_screen = event.value eq 0 and event.select eq 1
      do_ps = event.value eq 1 and event.select eq 1
      do_jpeg = event.value eq 2 and event.select eq 1
      _extra = {profile_show_plot: do_screen, profile_ps_plot: do_ps, profile_jpeg_plot: do_jpeg}
      widget_control, event.id, set_value=[do_screen, do_ps, do_jpeg]
    end

    'viscompplot': begin
      do_screen = event.value eq 0 and event.select eq 1
      do_ps = event.value eq 1 and event.select eq 1
      do_jpeg = event.value eq 2 and event.select eq 1
      _extra = {viscomp_show_plot: do_screen, viscomp_ps_plot: do_ps, viscomp_jpeg_plot: do_jpeg}
      widget_control, event.id, set_value=[do_screen, do_ps, do_jpeg]
    end

    ;    'show': begin
    ;      if event.value eq 0 then _extra = {progress_bar: event.select}
    ;      if event.value eq 1 then _extra = {verbose: event.select}
    ;      if event.value eq 2 then _extra = {show_images: event.select}
    ;      if event.value eq 3 then _extra = {profile_show_plot: event.select}
    ;    end

    'image_info': begin
      msg = ['If you choose not to make a FITS file while generating images,', $
        '   you can always do it later.', $
        '', $
        '   For single images, use the File / Export Data button.', $
        '   For multiple images, use the Window_Control / Multi-Panel Options button.', $
        '', $
        '   Once you have made a FITS file with multiple images you can:', $
        '      1. View the images as a movie (use File / View Image Movie button)', $
        '      2. Import the images back into the GUI (use File / Import FITS file button)', $
        '      3. Use the IMSPEC program to do imaging spectroscopy (exit GUI, type imspec in IDL)', $
        '', $
        'If you choose not to send Images to the GUI while generating them,', $
        '   you can import them later (use File / Import Fits File button)']
      a = dialog_message(msg, /info)
    end

    'stop':  begin
      obj = state.data_obj
      stop
    end

    'close':  goto, exit

    else:
  endcase

  if chg_msg[0] ne '' then ok = dialog_message (chg_msg, title='Warning message')

  if size(_extra, /tname) eq 'STRUCT' then begin
    ; if changing image algorithm, do that first, so the rest of the changed parameters
    ; will be set in the right group of params
    if tag_exist(_extra, 'image_algorithm') then state.data_obj -> set, image_alg=_extra.image_algorithm
    state.data_obj -> set, _extra=_extra
    update_params = 1
  endif

  if update_params then begin
    hsi_ui_img_getparams, state.data_obj, params
    state = rep_tag_value (state, params, 'params')
  endif

  if xalive(event.top) then begin
    hsi_ui_img_widget_update, state
    widget_control, event.top, set_uvalue=state
  endif

  return

  exit:

  widget_control, event.top, /destroy
  if state.standalone then free_var, state, exclude=['data_obj','data']
  return

end

;-------------------


pro hsi_ui_img, input_obj, $
  init=init, $
  obs_time_interval=ptr_obs_time_interval, $
  flare_str=ptr_flare_str, $
  intervals_obj=intervals_obj, $
  group=group, $
  error=error

  error = 0
  checkvar, init, 1

  if xregistered ('hsi_ui_img') then begin
    xshow,'hsi_ui_img', /name
    return
  endif

  img_to_file = 0
  img_to_gui = 1

  if not keyword_set(intervals_obj) then intervals_obj = obj_new('store_intervals')

  ;obs_time_intervaland flare_str keywords may be pointers or values (pointers if called from hessi,
  ;may be values if hsi_ui_img is called directly by user.  So need to handle both cases.

  if keyword_set(ptr_obs_time_interval) then begin
    if ptr_chk(ptr_obs_time_interval) then obs_time_interval = *ptr_obs_time_interval else begin
      obs_time_interval =  anytim(ptr_obs_time_interval)
      ptr_obs_time_interval = ptr_new(obs_time_interval)
    endelse
  endif

  ;if keyword_set(ptr_intervals) then intervals = *ptr_intervals
  if keyword_set(ptr_flare_str) then begin
    if ptr_chk(ptr_flare_str) then flare_str = *ptr_flare_str else begin
      flare_str = ptr_flare_str
      ptr_flare_str = ptr_new(flare_str)
    endelse
  endif

  if not hsi_chk_obj ( input_obj, 'HSI_IMAGE', data_obj, $
    obs_time_interval=obs_time_interval, init=init) then return

  input_opts = ['Raw Data', 'Image FITS File', 'Eventlist FITS File', 'Visibility FITS File']
  input_sel = 0
  if is_string(data_obj->get(/ev_filename)) then input_sel = 2
  if is_string(data_obj->get(/im_input_fits)) then input_sel = 1
  if is_string(data_obj->get(/vis_input_fits)) then input_sel = 3

  ; set default for progress_bar to enabled since this is GUI
  data_obj -> set, /progress_bar

  ;if total(data_obj -> get(/energy_band)) eq 0. then data_obj -> set, energy_band = [12., 25.]   ;  temporary workaround!!!

  hsi_ui_img_getparams, data_obj, params

  if not exist(flare_str) then flare_str = {hsi_flarelistdata_ext}

  ; if wasn't called from the GUI, won't have these pointers, so define them just so
  ; we won't crash, but don't need them.
  if not exist(ptr_obs_time_interval) then ptr_obs_time_interval = ptr_new(obs_time_interval)
  ;if not exist(ptr_intervals) then ptr_intervals = ptr_new(intervals)
  if not exist(ptr_flare_str) then ptr_flare_str = ptr_new(flare_str)

  space = 5
  ypad = 5
  xpad = 5

  hsi_ui_getfont, font, big_font

  widget_control, default_font = font


  ; if standalone, position at 50,50.  If not, after create base, offset it from parent
  ; before realizing it.
  xoffset = 50  & yoffset = 50

  w_top_base = widget_base(group=group, title=hsi_widget_title('Imaging'), $
    xoffset=xoffset, yoffset=yoffset, /tlb_kill, /scroll)

  w_base = widget_base( w_top_base, $
    /column, $
    /frame, $
    space=space, $
    xpad=xpad, $
    ypad=ypad )

  w_title = widget_base (w_base, /row, /align_center)
  w = widget_label( w_title, $
    value='IMAGING   ', $
    ;/align_center, $
    font=big_font )

  w = widget_label( w_title, $
    value="(* - changing these parameters forces reprocessing and takes longer)")
  ;/align_center)

  ;w_inputbase = widget_base (w_base, /column, /frame)
  ;w_input = widget_base (w_inputbase, /row)
  ;tmp = widget_label (w_input, value='Select Input: ', /align_center)
  ;w_input2 = widget_base (w_input, /column)

  w_inputbase = widget_base (w_base, /column, /frame)
  w_input = widget_base (w_inputbase, /row, space=10)
  w_inputsel = widget_droplist ( w_input, $
    title='Select Input: ', $
    value=input_opts, $
    uvalue='input' )
  w_inputtext = widget_text (w_input, /edit, xsize=50, value='', uvalue='inputtext', /tracking_events)
  w_inputchange = widget_button (w_input, value='Change...', uvalue='inputchange')

  ;w_imfile = widget_base (w_input2, /row)
  ;tmp = widget_label (w_imfile, value='Image FITS File: ')
  ;wspecfile = widget_text (w_imfile, /edit, xsize=50, value='', uvalue='imfile')
  ;tmp = widget_button (w_imfile, value='Browse...', uvalue='imbrowse')
  ;
  ;w_evfile = widget_base (w_input2, /row)
  ;tmp = widget_label (w_evfile, value='Eventlist FITS File: ')
  ;wspecfile = widget_text (w_evfile, /edit, xsize=50, value='', uvalue='evfile')
  ;tmp = widget_button (w_evfile, value='Browse...', uvalue='evbrowse')

  ;w_obs = widget_base (w_base, /row, /frame)
  ;w_obs_time = widget_base (w_obs, /column)
  w_obs_time_label0 = widget_label (w_inputbase, $
    value='', uvalue='reset_times', /align_center)
  w_obs_time_label1 = widget_label (w_inputbase, $
    value='', uvalue='reset_times', /align_center)
  w_obs_change = widget_base (w_inputbase, /column)
  ;tmp = widget_button (w_obs_change, value='Change...', uvalue='change_obs')

  ;w = widget_label (w_base, value='Note:  Imaging time interval(s) does NOT have to be within Observation Time Interval (above) ')

  w_time_base = widget_base (w_base, /row, space=10, /frame)
  w_num_times = widget_label (w_time_base, value='', /dynamic_resize)
  w_time = widget_droplist (w_time_base, value='', uvalue='times', /dynamic_resize)
  wc_changetime = widget_button (w_time_base, value='Change...', uvalue='changetime')
  wc_foursatpeak = widget_button (w_time_base, value='4s at peak', uvalue='4satpeak')
  tmp = widget_button (w_time_base, value='Show', uvalue='showtime')

  w_energy_base = widget_base (w_base, /row, /frame, space=10)
  w_num_energy = widget_label (w_energy_base, value='', /dynamic_resize)
  w_energy = widget_droplist (w_energy_base, value='', uvalue='energies', /dynamic_resize)
  wc_changeenergy = widget_button (w_energy_base, value='Change...', uvalue='changeenergy')
  wc_ecode = widget_droplist ( w_energy_base, value=['None',strtrim(indgen(23),2)], $
    title='Binning Code: ', uvalue='ecode' )
  w_showecodes = widget_button (w_energy_base, value='Show Binning Codes', uvalue='showecodes')
  tmp = widget_button (w_energy_base, value='Show', uvalue='showenergy')

  hsi_coll_list_widget, w_base, w_colldisp, $
    w_auto=w_auto_time_bin, w_qual=w_digital_qual, w_change=wc_changecoll

  w_size0 = widget_base (w_base, /row, /frame, space=6)
  w_sizeleft = widget_base (w_size0, /column)
  w_pixdim = widget_base (w_sizeleft, /row, ypad=0, space=15)
  w_pixel = widget_label (w_pixdim, value='', /dynamic_resize, /align_left)
  w_imagedim = widget_label (w_pixdim, value='', /dynamic_resize, /align_left)
  w_xyoffset = widget_label (w_sizeleft, value='', /dynamic_resize, /align_left)
  w_sizeinfo = widget_label (w_sizeleft, value='', /dynamic_resize)
  w_sizeright = widget_base (w_size0, /column)
  wc_point = widget_button (w_sizeright, /align_center, value='Click Image to Set Map Center', uvalue='clickcenter')
  wc_changesize = widget_button (w_sizeright, /align_center, value='Change...', uvalue='changesize')

  w_alg_vis_base = widget_base(w_base, /column, /frame)
  w_algbase = widget_base ( w_alg_vis_base, /row, space=10);, /frame)
  wc_alg = widget_droplist ( w_algbase, $
    title='* Image Algorithm: ', $
    value=params.image_algs, $
    uvalue='alg' )
  wc_algparm = widget_button (w_algbase, value='Set params...', uvalue='algparm')
  wc_mark_clean = widget_button (w_algbase, value='Mark clean boxes...', uvalue='mark_clean_box')
  wc_show_clean = widget_button (w_algbase, value='Show clean boxes', uvalue='show_clean_box')

  wc_visbase = widget_base ( w_alg_vis_base, /row, space=10);, /frame)
  w_vis = widget_droplist ( wc_visbase, $
    title='Visibility Type: ', $
    value=params.vis_types, $
    uvalue='vis_type' )
  w_visparm = widget_button (wc_visbase, value='Set visibility params...', uvalue='visparm')

  w_miscbase = widget_base (w_base, /row, /frame, space=20)
  w_miscleft = widget_base (w_miscbase, /column)
  w_flat = widget_label (w_miscleft, value='', /dynamic_resize, /align_left)
  w_weight = widget_label  (w_miscleft, value='', /dynamic_resize, /align_left)
  w_flux_var = widget_label (w_miscleft, value='', /dynamic_resize, /align_left)
  wc_changemisc = widget_button (w_miscbase, /align_center, value='Change...', uvalue='changemisc')

  wc_send_base1 = widget_base ( w_base,  /row, space=10, /frame )

  w_send = cw_bgroup (wc_send_base1, ['GUI', 'FITS File'], $
    label_top='Send Image(s) to: ', $
    set_value=[img_to_gui,img_to_file], $
    /nonexclusive, $
    /row, $
    uvalue='image_dest', space=0, /frame, ypad=0)
  ;/no_release )

  ;  w_show = cw_bgroup (wc_send_base1, ['Progress Bar', 'Verbose', 'Images', 'Profiles'], $
  ;    label_left='Show: ', $
  ;    /nonexclusive, $
  ;    /row, $
  ;    uvalue='show', space=0, ypad=0)


  w_progbar = widget_base(wc_send_base1, /row, /nonexclusive)
  tmp = widget_button (w_progbar, value='Progress Bar', uvalue='progbar')

  w_prof_plot = cw_bgroup (wc_send_base1, ['Screen', 'PS', 'JPEG'], $
    label_top='Profile Plots: ', $
    set_value=[0,0,0], $
    /nonexclusive, $
    /row, $
    uvalue='profplot', space=0, /frame, ypad=0)

  w_viscomp_plot = cw_bgroup (wc_send_base1, ['Screen', 'PS', 'JPEG'], $
    label_top='Vis Comparison Plots: ', $
    set_value=[0,0,0], $
    /nonexclusive, $
    /row, $
    uvalue='viscompplot', space=0, /frame, ypad=0)

  ;tmp = widget_button (w_button_base1, value='More info...', uvalue='image_info', /align_center )
  ; Li, adds intermediate diagnostics dropdown button to gui
  ;pdm = widget_button (w_button_base1, /menu, value='Diagnostics', uvalue='inter_plot', /align_center )
  ; Li, adds widget to plot each detector separately
  ;tmp = widget_button (pdm, value='Show Selected Detectors Separately', uvalue='det_sep')
  ;tmp = widget_button (pdm, value='Show All Detectors Separately', uvalue='det_sep_all')

  w_button_base1 = widget_base ( w_base, /row, /align_center, space=10)

  w_doimage = widget_button ( w_button_base1, value='Make/Plot Image(s)', uvalue='doimage')
  tmp = widget_button (w_button_base1, value='Plot GOES', uvalue='plot_goes')
  wc_fitswrite = widget_button (w_button_base1, value='Write FITS File ->', /menu)
  wc_imgfitswrite = widget_button(wc_fitswrite, value='Image FITS File', uvalue='imgfitswrite')
  wc_visfitswrite = widget_button(wc_fitswrite, value='Visibilities FITS File', uvalue='visfitswrite')
  w_panel = widget_button ( w_button_base1, value='Display ->', /menu)
  temp = widget_button (w_panel, value='All images in cube, small panels', uvalue='panel_display', uname='small')
  temp = widget_button (w_panel, value='All images in cube, bigger panels', uvalue='panel_display', uname='bigger')
  temp = widget_button (w_panel, value='All images in cube, BIGGER panels', uvalue='panel_display', uname='BIGGER')
  temp = widget_button (w_panel, value='Bproj map for sep. detectors', uvalue='det_sep')
  w_asp = widget_button (w_panel, value='Aspect Solution', /menu)
  temp = widget_button (w_asp, value='Image and Spin Axis on Solar Disk', uvalue='aspect')
  temp = widget_button (w_asp, value='Distance from Sun Center to Image Axis', uvalue='asp_distance')
  temp = widget_button (w_asp, value='Distance from Source to Image Axis', uvalue='asp_src_distance')
  temp = widget_button (w_asp, value='Diameter of Image Axis Circle', uvalue='asp_diameter')
  temp = widget_button (w_asp, value='SAS Reduced Triangle Size', uvalue='asp_triangle')
  temp = widget_button (w_asp, value='SAS Error', uvalue='asp_p_error')
  temp = widget_button (w_panel, value='Parameter summary', uvalue='params')
  w_prof_display = widget_button (w_panel, value='Count Profile vs Roll Angle', /menu)
  temp = widget_button (w_prof_display, value='Counts / bin', uvalue='cntprofile', uname='prof_cts')
  temp = widget_button (w_prof_display, value='Counts / sec', uvalue='cntprofile', uname='prof_rate')
  temp = widget_button (w_prof_display, value='Residuals', uvalue='cntprofile', uname='prof_resid')
  temp = widget_button (w_panel, value='Visibility Comparison', uvalue='vis_comp')

  w_movie = widget_button ( w_button_base1, value='Movie', uvalue='movie')
  w_script = widget_button ( w_button_base1, value='Write Script ->', /menu)
  temp = widget_button (w_script, value='Non-default GUI params', uvalue='script', uname='non-default')
  temp = widget_button (w_script, value='All GUI params', uvalue='script', uname='all')

  w_button_base2 = widget_base ( w_base,  /row,   /align_center,   space=10 )
  wc_force = widget_button (w_button_base2, value='Force Redo', uvalue='force')
  temp = widget_button (w_button_base2, value='Refresh', uvalue='refresh')
  ;temp = widget_button (w_button_base2, value='Parameter Summary', uvalue='params')
  temp = widget_button (w_button_base2, value='Reset to Defaults', uvalue='defstandard')
  wc_manoptions = widget_button (w_button_base2, value='Set Params Manually', uvalue='manoptions')
  temp = widget_button (w_button_base2, value='Help', uvalue='help')
  temp = widget_button (w_button_base2, value='Close', uvalue='close' )

  valid_plotman = 0
  if xalive(group) then begin
    widget_control, group, get_uvalue=mw_state
    if size(mw_state,/tname) eq 'STRUCT' then begin
      if tag_exist(mw_state,'plotman_obj') then begin
        plotman_obj = mw_state.plotman_obj
        w_message = mw_state.widgets.w_message
        valid_plotman = 1
      endif
    endif
  endif
  if not valid_plotman then plotman_obj = plotman(/multi)

  ; message widget id was defined if we already had a plotman object defined.  Otherwise,set to 0.
  ; Check if xalive before using it.
  checkvar, w_message, 0L

  widgets = {w_top_base: w_top_base, $
    w_inputsel: w_inputsel, $
    w_inputtext: w_inputtext, $
    w_inputchange: w_inputchange, $
    w_obs_time_label0: w_obs_time_label0, $
    w_obs_time_label1: w_obs_time_label1, $
    ;   w_intlist: w_intlist, $
    w_time: w_time, $
    w_num_times: w_num_times, $
    wc_changetime: wc_changetime, $
    wc_foursatpeak: wc_foursatpeak, $
    w_energy: w_energy, $
    wc_changeenergy: wc_changeenergy, $
    wc_ecode: wc_ecode, $
    w_num_energy: w_num_energy, $
    w_colldisp: w_colldisp, $
    wc_changecoll: wc_changecoll, $
    w_auto_time_bin: w_auto_time_bin, $
    w_digital_qual: w_digital_qual, $
    w_pixel: w_pixel, $
    w_imagedim: w_imagedim, $
    w_xyoffset:w_xyoffset, $
    w_sizeinfo: w_sizeinfo, $
    wc_point: wc_point, $
    wc_changesize: wc_changesize, $
    wc_alg: wc_alg, $
    wc_algparm: wc_algparm, $
    wc_visbase: wc_visbase, $
    w_vis: w_vis, $
    w_visparm: w_visparm, $
    wc_mark_clean: wc_mark_clean, $
    wc_show_clean: wc_show_clean, $
    w_flat: w_flat, $
    w_weight: w_weight, $
    w_flux_var: w_flux_var, $
    wc_changemisc: wc_changemisc, $
    wc_send_base1: wc_send_base1, $
    w_send: w_send, $
    w_progbar: w_progbar, $
    w_prof_plot: w_prof_plot, $
    w_viscomp_plot: w_viscomp_plot, $
    ;    w_show: w_show, $
    w_button_base1: w_button_base1, $
    w_button_base2: w_button_base2, $
    w_doimage: w_doimage, $
    wc_fitswrite: wc_fitswrite, $
    wc_imgfitswrite: wc_imgfitswrite, $
    wc_visfitswrite: wc_visfitswrite, $
    wc_force: wc_force, $
    wc_manoptions: wc_manoptions, $
    w_prof_display: w_prof_display }
  ;w_movie: w_movie }

;  params_used = [ $
;    'cbe_digital_quality', $
;    'cbe_powers_of_two', $
;    'cull_frac', $
;    'decimation_correct', $
;    'rear_decimation_correct', $
;    'det_index_mask', $
;    'flatfield', $
;    'front_segment', $
;    'im_energy_binning', $
;    'im_input_fits', $
;    'im_out_fits_filename', $
;    'im_time_interval', $
;    'im_time_binning', $
;    'image_algorithm', $
;    'image_dim', $
;    'modpat_skip', $
;    'natural_weighting', $
;    ;       'obs_time_interval', $
;    'pixel_size', $
;    'progress_bar', $
;    'rear_segment', $
;    'show_images', $
;    'smoothing_time', $
;    'taper', $
;    'time_bin_def', $
;    'time_bin_min', $
;    'cbe_time_bin_floor', $
;    ;       'time_range', $
;    'uniform_weighting', $
;    'use_auto_time_bin', $
;    'use_cull', $
;    'use_flare_xyoffset', $
;    'use_flux_var', $
;    'use_local_average', $
;    'use_rate', $
;    'verbose', $
;    'xyoffset', $
;    'use_phz_stacker', $
;    'phz_n_phase_bins', $
;    'phz_n_roll_bins_min', $
;    'phz_n_roll_bins_max', $
;    'phz_n_roll_bins_control', $
;    'phz_radius', $
;    'phz_report_roll_bins', $
;
;    'profile_show_plot', $
;    'profile_ps_plot', $
;    'profile_ps_dir', $
;    'profile_plot_rate', $
;    'profile_plot_resid', $
;
;    'mc_ntrials', $
;    'mc_show_plot', $
;
;    'viscomp_show_plot', $
;    'viscomp_ps_plot', $
;    'viscomp_ps_dir', $
;
;    'clean_niter', $
;    'clean_negative_max_test', $
;    'clean_no_chi2', $
;    'clean_show_maps', $
;    'clean_show_n_maps', $
;    'clean_show_chi', $
;    'clean_show_n_chi', $
;    'clean_chi_sq_crit', $
;    'clean_frac', $
;    'clean_more_iter', $
;    'clean_mark_box', $
;    'clean_progress_bar', $
;    'full_info', $
;    'clean_box', $
;    'clean_cw_nop', $
;    'clean_cw_list', $
;    'clean_regress_combine', $
;    'clean_beam_width_factor', $
;
;    ;    'sato_iter_max', $
;    ;    'sato_show_image', $
;    ;    'sato_no_chi2', $
;    ;    'sato_chi_limit', $
;    ;    'sato_lnorm', $
;    ;    'sato_lambda_max', $
;    ;    'sato_init_btot', $
;    ;    'sato_progress_bar', $
;
;    ;    'vis_iter_max', $
;    ;    'vis_show_image', $
;    ;    'vis_no_chi2', $
;    ;    'vis_chi_limit', $
;    ;    'vis_lnorm', $
;    ;    'vis_lambda_max', $
;    ;    'vis_init_btot', $
;    ;    'vis_progress_bar', $
;
;    'pixon_noplot', $
;    'verbose', $
;    'pixon_progress_bar', $
;    'pixon_snr', $
;    'pixon_full_pm_calc', $
;    'pixon_background_model', $
;    'pixon_variable_metric', $
;    'pixon_sensitivity', $
;
;    'ff_n_gaussians', $
;    'ff_n_par', $
;    'ff_nitmax', $
;    'ff_min_sigma', $
;    'ff_minsep', $
;    'ff_ftol', $
;    'ff_testplot', $
;    'ff_savefile', $
;    'ff_progress_bar', $
;
;    'vf_srcin', $
;    'vf_circle', $
;    'vf_multi', $
;    'vf_loop', $
;    'nvis_min', $
;    'vf_nophase', $
;    'vf_noerr', $
;    'vf_absolute', $
;    'vf_maxiter', $
;    'vf_noplotfit', $
;
;    'nj_ferr', $    ; mem_njit
;    'nj_tol', $
;    'nvis_min', $
;    'nj_show_maps', $
;    'nj_progress_bar', $
;
;    'em_max_iterations', $    ; em
;    'em_max_array_size', $
;    'em_tolerance', $
;    'em_show_maps', $
;    'em_show_n_maps', $
;    'em_progress_bar', $
;
;    'vis_chi2lim', $   ; visibilities
;    'vis_edit', $
;    'vis_conjugate', $
;    'vis_normalize', $
;    'vis_input_fits', $
;    'vis_out_filename', $
;    'vis_plotfit', $
;    'vis_type', $
;    'vis_photon2el' $
;    ]

  state = {data_obj: data_obj, $
    input_opts: input_opts, $
    input_sel: input_sel, $
    obs_time_label: strarr(2), $
    obs_time_interval: obs_time_interval, $
    flare_str: flare_str, $
    ptr_obs_time_interval: ptr_obs_time_interval, $
    ptr_flare_str: ptr_flare_str, $
    intervals_obj: intervals_obj, $
    standalone: xalive(group) ne 1, $
    params: params, $
    ;multi_times: ptr_new(/alloc), $
    ;multi_ebands: ptr_new(params.control.energy_band), $
    widgets:widgets, $
    plotman_obj: plotman_obj, $
    img_to_gui: img_to_gui, $
    img_to_file: img_to_file, $
    w_message: w_message}

  hsi_ui_img_newtime, state, init=init
  ;if not ptr_exist(state.multi_times) then $
  ;    *state.multi_times = hsi_get_time_range (state.params.control, state.params.info)

  hsi_ui_img_widget_update, state

  widget_limit_ysize, w_top_base, w_base

  if xalive(group) then begin
    widget_offset, group, xoffset, yoffset, newbase=w_top_base
    widget_control, w_top_base, xoffset=xoffset, yoffset=yoffset
  endif

  widget_control, w_top_base, /realize, /hourglass

  widget_control, w_obs_time_label0, timer=0.

  widget_control, w_top_base, set_uvalue=state

  xmanager, 'hsi_ui_img', w_top_base, /no_block

end