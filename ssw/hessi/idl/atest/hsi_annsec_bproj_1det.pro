;+
; PROJECT:
;  HESSI
; NAME:
;  HSI_ANNSEC_bproj_1det
;
; PURPOSE:
;  Returns the back-projection of the hessi calibrated eventlist.
;
; CATEGORY:
;  HESSI, UTIL
;
; CALLING SEQUENCE:
;  map = hsi_annsec_bproj_1det( cbe, map_ptr = map_ptr, /sum, this_det_index = this_det_index)
;
;;
;
; CALLS:
;  none
;
; INPUTS:
;    CBE_OBJ - Object containing the calibrated eventlist or a pointer array to same. If computing this for
;   a set of detectors without the THIS_DET_INDEX keywords, then either
;   the cbe containing object or the cbe pointer + DET_INDEX_MASK + rmap_dim must be defined.
;
; OPTIONAL INPUTS:
;
;
; OUTPUTS:
;    Returns the map as a pointer ( if SUM eq 0 ) or as a 2-d image in polar coordinates.
;
; OPTIONAL OUTPUTS:
;  none
;
; KEYWORDS:
;  MAP_PTR - map_ptr is an HSI_ANNSEC_MODUL_PATTERN_STR from modul_pattern object getdata
;  SUM - If set, sum the map over the detectors.
;  THIS_DET_INDEX- detector index for single collimator/harmonic pair.
;;;  DATA_PTR - Alternate count rate input.
;   E.G. Used to create point spread functions and intermediate MEM images.
;   Counts per time_bin corrected for livetime. May be a vector if not for
;   an array of detectors. If not corrected for livetime, then you can set USE_RATE
;  COUNTS_SUMMED - sum of livetime corrected counts. Returned as a 9 elem array
;  unles this_det_index is used, when it comes back as a single number.
;  USE_RATE- use count rate not count to for back-projection weighting.
; MAX_HARMONIC - normally harmonics are detected from modulation pattern structure
;  but for flatfield correction need to force use only 1st harmonic for simplicity
;
;
; COMMON BLOCKS:
;  none
;
; SIDE EFFECTS:
;  none
;
; RESTRICTIONS:
;  should only be called from hsi_annsec_bproj!!!
;
; PROCEDURE:
;  none
;
; MODIFICATION HISTORY:
;
; 3-apr-2017, ras, tidied up and broken out from hsi_annsec_bproj
; 4-apr-2017, ras, added test for data_ptr incase *data_ptr[i] is a structure
;   and then we're expecting to find the data in (*data_ptr[i]).count as in the cbe
;-
function hsi_annsec_bproj_1det, cbe, $ ;cbe is a pointer or pointer array
  this_det_index = this_det_index, $
  map_ptr        = map_ptr, $
  data_ptr       = data_ptr, $
  flatfield      = flatfield, $
  det_eff        = det_eff, $
  use_rate       = use_rate, $
  smoothing_bins = smoothing_bins, $
  max_harmonic   = max_harmonic, $
  counts_summed  = counts_summed, $
  nmap = nmap
  default, use_rate, 0

  det_index = fcheck( this_det_index, 0 )
  twopi = 2.0 * !pi
  default, flatfield, 0

  checkvar, det_eff, fltarr( 9 ) + 1.
  this_det_eff = det_eff[ det_index ]

  
  dcbe =  n_elements( cbe ) eq 1 ? *cbe :*cbe[ det_index ] 
  if is_struct( map_ptr ) && $
    tag_names(/str, map_ptr ) eq 'HSI_ANNSEC_MODUL_PATTERN_STR' then $
    mpat = map_ptr else begin

    if n_elements( map_ptr ) gt 1 then mpat = *map_ptr[ det_index ] $
    else if map_type eq 'POINTER' then mpat = *map_ptr else mpat = map_ptr
  endelse

  default, max_harmonic, 3
  phase_ptr = mpat.phase_ptr
  rmap_dim = mpat.rmap_dim
  harmonic = mpat.harmonic < max_harmonic
  out = fltarr( long( rmap_dim[ 0 ] ) * rmap_dim[ 1 ] )

  counts_summed = 0.0

  ;livetime = ( * cbe ).livetime ; * ( cbe[ 1 ].time-cbe[ 0 ].time ) / 2.0^20

  dtype = size( /tname, data_ptr )
  case 1 of
    dtype eq 'POINTER': begin
      data = *data_ptr[ det_index ]
      ;In case we are using the cbe pointer array for the data array to bypass the normal object call
      data = is_struct( data ) ? data.count : data
      end

    dtype eq 'UNDEFINED': data = dcbe.count

    else: data = data_ptr
  endcase
  ;bin culling included 18-apr-2004, ras

  zro = where( dcbe.gap eq 1, invalid )
  ;vrate   = data * ( * cbe ).gridtran
  vrate   = data  ;19-dec-00 new formulation by gh implem. by ras.
  if invalid ge 1 then vrate[ zro ] = 0.0
  tot_vrate = total( vrate )
  if use_rate then begin
    ;message, 'use_rate is set', /continue
    vrate = f_div( vrate, dcbe.livetime )
    norm  = f_div( tot_vrate, total( vrate ) )
    vrate = vrate * norm

  endif
  out       = out + tot_vrate
  phase_ptr = ( *phase_ptr )
  vrate_in  = vrate
  for hh    = 0, harmonic-1 do begin
    vrate   = ( dcbe.modamp[ hh ] * vrate_in )[ * ] ## ( 1.+fltarr( 2 ) )
    counts_summed = tot_vrate

    cos_factor = reform( phase_ptr.phz_coef[ * , 0, hh ] ) * vrate
    sin_factor = reform( phase_ptr.phz_coef[ * , 1, hh ] ) * vrate
    ;
    ; the phz_coef includes the trig function and wcoef
    ;
    max_sum = max( phase_ptr.isum )
    ;
    ; Sum the phases for intervals which share modulation pattern maps.
    ; I.E. we're adding phasors here.
    ;
    select0 = where( phase_ptr.isum eq 0, nmap )
    ibin0  = hsi_iphase( select0, /ibin )   ;chooses the bin phase structure array
    icoef0 = hsi_iphase( select0, /icoef )   ;given ibin0, finds the right coefficient index.
    map_index0 = ( phase_ptr[ ibin0 ].map_index )[ icoef0 ]

    if idl_release( lower = 5.3, /inclusive ) then find_ix = 'value_locate' else find_ix = 'find_ix'

    ord0 = sort( map_index0 )  ;;;;;;;;;;;

    ss = histogram( phase_ptr.isum, min = 0, max = max_sum, rev = rev )

    if max_sum ge 1 then for this_sum = 1L, max_sum do begin

      nselect = ss[ this_sum ]
      if nselect ge 1 then begin
        select = rev[ rev[ this_sum ]:rev[ this_sum+1 ]-1 ]

        ;select = where( ( * phase_ptr ).isum eq this_sum, nselect )
        ;if not same_data( select, select1 ) then stop
        ibin  = hsi_iphase( select, /ibin )   ;chooses the bin phase structure array
        icoef = hsi_iphase( select, /icoef )   ;given ibin0, finds the right coefficient index.
        map_index = ( phase_ptr[ ibin ].map_index )[ icoef ]

        ;;;sindex0 = call_function( find_ix, map_index0, map_index )
        sindex0 = call_function( find_ix, map_index0[ ord0 ], map_index )

        ;;;add_to_these = select0[ sindex0 ]
        add_to_these = select0[ ord0[ sindex0 ] ]

        cos_factor[ add_to_these ] = cos_factor[ add_to_these ]+cos_factor[ select ]
        sin_factor[ add_to_these ] = sin_factor[ add_to_these ]+sin_factor[ select ]
      endif
    endfor

    npixel = long( rmap_dim[ 0 ] * rmap_dim[ 1 ] )-1
    cmp    = ( *mpat.cmap_ptr )[ * , * , hh ]
    smp    = ( *mpat.smap_ptr )[ * , * , hh ]
    ;
    ; Build the back projection by multiplying summing the weighted maps one at a time. No explicit
    ; matrix multiplication ( hashing ) involved.
    ;
    for ii = 0L, nmap-1 do begin
      i = select0[ ii ]
      index1 = ( phase_ptr[ i/2 ].map_index )[ i mod 2 ] * rmap_dim[ 0 ]
      index2 = index1 + npixel

      out[ 0 ] = temporary( out + cmp[ index1:index2 ] * cos_factor[ i ] - $
        smp[ index1:index2 ] * sin_factor[ i ] )
    endfor
  endfor

  out = reform( /over, out, rmap_dim[ 0 ], rmap_dim[ 1 ] )
  if keyword_set( flatfield ) then begin
    wmap_ptr = mpat.weight_map_ptr
    if not ptr_valid( wmap_ptr ) then begin

      ;if size( /tname, cbe_obj ) ne 'OBJREF' then return, 0

      hsi_annsec_bproj_weight_map, cbe, mpat, $
        smoothing_bins = smoothing_bins, $
        use_rate    = use_rate, $
        this_det_index = det_index

      ( *map_ptr[ det_index ] ).weight_map_ptr = mpat.weight_map_ptr
    endif

    out = hsi_annsec_bproj_weight( out, counts_summed, mpat.weight_map_ptr, cbe[det_index], $
      use_rate = use_rate, det_eff = this_det_eff )
  endif

  if fcheck( error_weight, 0 ) then message, /info, 'Flatfield map not computed. Cbe_obj must be object.'

  return, out
end

