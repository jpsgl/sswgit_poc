;+
;Name: HSI_VIS_PREP
;
;Purpose: This fuction returns the visibility bag after the
;	steps of select, edit, combine(/conjugate), and normalize
;
;Method: Some of these methods require grouping the visibilities by
;	trange and erange.  So these are identified and processed sequentially
;	as needed for combine(/conjugate) and normalize
;
;Input:
;	INVIS - Visibility bag, random visibilities, normally one time and energy
;		or the whole set for regularization
;	OBJ_VIS  - Visibility object that produced invis
;	NO_E_T_SEGRATE - if set then use hsi_vis_combine(/conjugate) on the entire data set
;	QUIET - passes to vis_edit and vis_combine
;	this_det_index_mask - if set, use this selection instead of what's in object. Also, if set, don't
;	  set vis_corr_factors and vis_det_index_mask_used back into the object.
;History:
;	12-13-2011, ras, taken from hsi_visibility::getdata()
;	10-aug-2012, ras, added quiet
;	02-Jun-2017, Kim. Added this_det_index_mask keyword
;-
function hsi_vis_prep, invis, OBJ_VIS, paout=paout, SORTPA = sortpa, $
  no_e_t_segregate=no_e_t_segregate, quiet=quiet, this_det_index_mask=this_det_index_mask

  default, no_e_t_segregate, 0
  if ~NO_E_T_SEGREGATE then begin
    trange0 = invis.trange[0]
    erange0  = invis.erange[0]
    time = get_uniq( trange0[sort(trange0)])
    energy = get_uniq( erange0[sort(erange0)])
    kti = n_elements(time) eq 1 ? intarr(n_elements(trange0)) : value_locate( time, trange0)
    kei = n_elements(energy) eq 1 ? intarr(n_elements(erange0)) : value_locate( energy, erange0)
    ntime = n_elements(time)
    nenergy = n_elements(energy)
  endif else begin
    kt = 0
    ke =0
    ntime = 1
    nenergy = 1
  endelse
  for kt = 0, ntime-1 do for ke=0,nenergy-1 do begin
    zvis = where(kti eq kt and kei eq ke, nvis)
    if nvis eq 0 then break
    vis = invis[zvis]
    det_index_mask = keyword_set(this_det_index_mask) ? this_det_index_mask : OBJ_VIS->get( /det_index_mask )
    dets = where( det_index_mask, count )
    if count gt 0 then begin
      dets_list = where_arr( vis.isc, dets, cc )
      if cc gt 0 then vis = vis[dets_list]
    endif

    ;Remove outliers
    vis_edit = OBJ_VIS->get( /vis_edit )
    If vis_edit ne 0 and not keyword_set( no_filter ) then begin

      chi2lim=OBJ_VIS->Get(/vis_chi2lim )
      vis = chi2lim gt 0 ? hsi_vis_edit( vis, chi2lim=chi2lim, quiet=quiet  ):hsi_vis_edit(vis, quiet=quiet )
      if not is_struct( vis ) then return, -1

      ; acs 2008-12-18 NEW: vis_combine can only be called when vsi_edit is
      ; set. Only the conjugate option is used for now.

      vis_conjugate= OBJ_VIS->get( /vis_conjugate )
      If vis_conjugate ne 0 or keyword_set( vis_conjugate ) then vis = hsi_vis_combine( vis, /conj, quiet=quiet )

    endif

    ; normalise visibilities JK
    ; acs 2009-04-29 added stuff to pass control params in and store info
    ; params out
    vis_normalize = OBJ_VIS -> get(/vis_normalize, /this)
    if vis_normalize ne 0 then begin
      max_corr = OBJ_VIS->get( /vis_max_corr, /this )
      vis = hsi_vis_normalize(vis, max_corr = max_corr, corr_factors=corr, valid_det_index_mask=valid_det_index_mask, quiet=quiet )
      if not keyword_set(this_det_index_mask) then OBJ_VIS->set, vis_corr_factors = corr, vis_det_index_mask_used=valid_det_index_mask
      if not keyword_set(quiet) then message,'Correction factor for each RMC: ' + arr2str(trim(corr, '(f7.2)')), /cont
    endif
    outvis = append_arr( outvis, vis, /NO_COPY)
  endfor
  return, outvis
end