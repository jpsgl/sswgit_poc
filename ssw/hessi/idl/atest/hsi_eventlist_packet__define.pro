;---------------------------------------------------------------------------
; Document name: hsi_eventlist_packet__define.pro
; Created by:    Andre Csillaghy, February 17, 1999
;
; Last Modified: Sat May 24 18:31:04 2003 (csillag@sunstroke)
;---------------------------------------------------------------------------
;
; NAME:
;       EVENTLIST PACKET CLASS DEFINITION
;
; PURPOSE:
;       This class extends the event list strategy class to use it with
;       telemetry and level 0 packet files. Its main purpose is the
;       implementation of tasks associated with unpacking events
;       from telemetry packets. Thus the core of this class is the call to
;       the routine hsi_packet2eventlist in the process_hook. It also
;       stores the livetime events and dropouts. Note
;       that thsi class is usually not instantiated alone, but
;       is automatically instantiated by hsi_eventlist
;       whenever a filename, a file type, or an obs_time_interval are
;       set. .
;
; CATEGORY:
;       Utilities
;
; CONSTRUCTION:
;       obj = Obj_New( 'hsi_eventlist_packet )
;       (but this is not recommended)
;
; PARENT CLASS:
;       hsi_eventlist_strategy
;
; METHODS DEFINED OR OVERRIDDEN IN THIS CLASS:
;       INIT: usual stuff
;       GetData: some additional tests for eventlist bunch processing
;       Adjust_Absolute_time_range: some checks needed to see whether
;                                   we use time_range,
;                                   obs_time_interval or directly a
;                                   file
;       select_by_time: implements the time selection for the
;                       livetime
;       process_hook: gets packets and unpacks the events and
;                     livetime.
;
; EXAMPLES:
;
;       This will select all events between 2002/04/21 10:00:10 and 2002/04/21 10:00:20
;       o = HSI_Eventlist( filename = 'abc.fits' )
;       ev = o->getdata( obs_time = '2002/04/21 ' + ['10:00', '10:30']
;       o->set, time_range = [10,20]
;       ev = o->getdata()
;
; DESTINATION CLASS:
;       HSI_Eventlist
;
; SEE ALSO:
;       hsi_eventlist_packet_info__define
;       hsi_eventlist_strategy__define
;       hsi_eventlist_strategy_control__define
;       hsi_eventlist_strategy_control
;       hsi_eventlist_strategy_info__define
;       hsi_packet2eventlist
;
; HISTORY:
;		07-may-2017, ras, packet_per_bunch changed from 5000 to 20000
;		14-apr-2007, ras, fcheck added to FIRST_BUNCH to prevent eventlist crash
;		March 2007, ras, added saved_reset pointer to object define
;       May 2003, acs, version 2 of event list files, works also for simulations
;       Dec 2002, acs, adaptation for event list files
;   Jul 2002, ras, added protection against null livetime pointer
;      in self in getdata()
;       Feb 2002: Livetime counters integrated.
;       Oct 2001: Better handling of livetime and separation of
;                 simulation / packet objects ACs
;       Release 6 Apr 2001 acs:
;           hsi_eventlist_packet created from hsi_eventlist
;           Despagetthizing
;           Strategy_holder stuff, i.e. now you can have both
;           simulation and packets coexisting
;       Release 5 June - August 2000
;           Major changes in the coincidence checking
;       Release 4 May 2000 ACS
;           Code to read eventlists in bunches
;           Coincidence stuff
;       jimm's adaptations,
;       Dev. for Release 3, July 1999
;       Release 2, June 1999
;       Version 1, February 17, 1999,
;           A Csillaghy, csillag@ssl.berkeley.edu
;  ;  22-sep-2015, ras, richard.schwartz@nasa.gov, added rollover_test control param, default is 5,
; made it a keyword to expose it to the packet object. Previous default was 3.
; ras, 10-feb-2017 support L64 time so disable 2048 sec protection if using l64 time
; ras, 2-mar-2017, packet bunches in packet2eventlist are now normally very large (suggested 5000 packets)
; and they are converted in one pass and not in a loop of 100 packets. 
;
; 
; 1. Packet_bunches -  we cannot expect to accumulate data requests by analyzing a single set (bunch) of packets.
;The boundary management was always a bit unreliable.  Datagaps and livetime procedures mean that you always have needed to
;look forward to complete any accumulation.  It's managed much more cleanly using a 2 second overlap for every bunch as we've
;been using a 2 second extension for many years.  Working with that has made the packet-eventlist software much cleaner. Also, there is a 1-1
;relationship between the eventlist elements and the data longwords in the packets until just before the eventlist
;is returned to eventlist_strategy::process from the return in ::process_hook

;2. Livetime - There are a number of issues.  Livetime isn't reported in the datagaps.
;In the original telem schema that would have meant nearly 100% livetime.
;The maximum time resolution for livetime reporting is 512 binary microsec (busec).
;High resolution imaging may require 256 busec or finer time bins. Livetime is only reported in 3 events following an interval or
;not at all so there has always been the issue of interpolating/integrating the livetime at both short and long time intervals
;(128 busec to thousands of seconds).
;
;The old software mapped the livetime poorly for time bins lt 512 busec.  The new software is robust at all timescales although there is still
;potential for modest improvement at sub-512 busec. Finally, the datagaps have to be integrated with the reported livetime.
;In the new software, the datagaps force the livetime during the gaps to be zero and it's a value that tells the follow-on software that
;it is a gap zero so different from a livetimecounter value of 0 that would mean 100% livetime.
;
;3. Datagaps - The CSA reset method used in the first few years was unreliable and so now datagaps are always
;detected by the absence of events.  This new software doesn't support the old CSA event method at all.  The gap detection size limit varies with the rate.
;There are more possibilities for extending and prepending added time on the gap intervals. Also, the post-gap ramps are detected and
;removed from the spectrogram data although they can be recovered from the eventlist. Although there are no datagaps (maybe) in the
;rear segments this code is used to detect detector off intervals as well (SAA for example) and thus to control the livetime
;integration over large time intervals better. Also, event detection for datagaps starts at 0 keV by default
; 24-apr-2017, ras, adding attenuator state to eventlist to better support ramp detection
;-


;--------------------------------------------------------------------

FUNCTION HSI_EventList_Packet::INIT,  _EXTRA=_extra

  ret=self->HSI_Eventlist_Strategy::INIT( _EXTRA = _extra )

  self->set, ev_sim_data =  0

  RETURN, ret

END

;--------------------------------------------------------------------

PRO HSI_Eventlist_Packet::Write_Hook, o, all_ut_ref, _EXTRA = _extra

  self->Set, ev_sim_data = 0

  packet_obj = self->get( /obj, class='hsi_packet' )
  packet_file_obj = self->get( /source )
  packet_file_obj->set, packet_per_bunch = 20000 ;ras, 7-may-2017

  first = 1 & next=0

  CheckVar, this_front_segment, 1
  CheckVar, this_rear_segment, 1

  REPEAT BEGIN

    ev = self->GetData( FIRST=first, $
      NEXT=next, $
      THIS_TIME_RANGE_EXTENSION=self->Get( /EXTEND_TIME_RANGE ), $
      DONE=done, /STREAM, _EXTRA=_extra )

    ev_ut_ref = self->Get( /EV_UT_REF )
    ev_time_unit = self->get( /EV_TIME_UNIT )
    livetime = self->GetData( /LIVETIME )
    dropout_list = self->GetData( /DROPOUT_LIST )
    LT_ut_ref = self->Get( /LT_UT_REF )
    next = 1 & first=0
    ev = n_elements( ev ) eq 1 ? ev : hsi_ev2ev( ev )
    if n_elements( livetime ) ne 1 then livetime = {time:livetime.time, counter:livetime.counter, seg_index:livetime.seg_index }

    self->WriteBtable, o, ev, ev_ut_ref, 'EVENTLIST'
    self->WriteBtable, o, livetime, lt_ut_ref, 'LIVETIME'
    self->Writebtable, o, dropout_list, ev_ut_ref, 'DROPOUT_LIST'

    IF N_Elements( all_ut_ref ) EQ 0 THEN BEGIN
      all_ut_ref = ev_ut_ref
    ENDIF ELSE BEGIN
      all_ut_ref = [ all_ut_ref, ev_ut_ref ]
    ENDELSE

  END UNTIL done

  packet_obj->Set, packet_per_bunch = self->Get( /PACKET_PER_BUNCH )

END

;--------------------------------------------------------------------

FUNCTION HSI_Eventlist_Packet::GetData, $
  ATTEN_STATE=atten_state, $
  SSR_STATE=ssr_state, $
  INSUN_STATE=insun_state, $
  FIRST_BUNCH=first_bunch, $
  NEXT_BUNCH=next_bunch, $
  LIVETIME=livetime, $
  DROPOUT_LIST=dropout_list, $
  DONE=done, $
  _EXTRA=_extra

  IF Keyword_Set( FIRST_BUNCH ) THEN BEGIN
    self->Framework::Set, /NEED_UPDATE,  /THIS_CLASS_ONLY
    self.bunch_nr = 0
  ENDIF ELSE IF Keyword_Set( NEXT_BUNCH ) THEN BEGIN
    self->Framework::Set, /NEED_UPDATE,  /THIS_CLASS_ONLY
    self.bunch_nr =  self.bunch_nr + 1
  ENDIF

  IF Keyword_Set( ATTEN_STATE ) OR Keyword_Set( SSR_STATE ) OR $
    Keyword_Set( INSUN_STATE ) THEN BEGIN

    pack_obj = self->Get( CLASS='HSI_PACKET', /OBJECT_REFERENCE )
    ;    lookup_table = pack_obj->GetData( /LOOKUP_TABLE )
    ;    selection = pack_obj->GetData( /PACKET_SELECTION )
    ;    this_LT = lookup_table[selection]

    ; acs 2005-03-16 make sure we use the app id 100 for this
    this_lt = pack_obj->GetData( /LOOKUP_TABLE, /packet_selection, app_id = 100 )

    out_state = exist( atten_state ) + exist( ssr_state) + exist( insun_state)
    IF Keyword_Set( ATTEN_STATE ) then begin
      find_changes, this_lt.atten_state, index, state
      atten_state = {time: this_LT[index].collect_time, state: state}
      ;        print, 'atten_state: ========================= ', atten_state
      if out_state eq 1 then Return, atten_state
    endif

    IF Keyword_Set( SSR_STATE ) then begin
      find_changes, this_lt.ssr_state, index, state
      ssr_state = {time: this_LT[index].collect_time, state: state}
      if out_state eq 1 then Return, ssr_state
    endif

    IF Keyword_Set( INSUN_STATE ) then BEGIN
      ; changed insun to the correct in_sun, acs 2002-09-19
      find_changes, this_lt.in_sun, index, state
      insun_state = {time: this_LT[index].collect_time, state: state}
      if out_state eq 1 then Return, insun_state
    endif

    If exist( insun_state ) and exist( atten_state ) then BEGIN
      IF EXIST( ssr_state ) THEN BEGIN
        return, {insun_state: insun_state, atten_state: atten_state, ssr_state: ssr_state}
      ENDIF ELSE BEGIN
        return, {insun_state: insun_state, atten_state: atten_state}
      ENDELSE
    ENDIF

  ENDIF

  data=self->HSI_Eventlist_Strategy::GetData( _EXTRA = _extra, $
    DONE=done, $
    FIRST_BUNCH=first_bunch, $
    NEXT_BUNCH=next_bunch )

  if size(/tname, data) ne 'STRUCT' then return, -1     ;kim 02-apr-2002
  ; here we get the atten state or the decim state from the packet
  ; lookupo table and pass back only the changes

  IF Keyword_Set( LIVETIME ) THEN BEGIN
    RETURN, ptr_valid( self.livetime ) ? *self.livetime : 0
  ENDIF

  IF Keyword_Set( DROPOUT_LIST ) THEN BEGIN
    RETURN, Ptr_valid( self.dropout_list ) ? *self.dropout_list: 0
  endif

  RETURN, data

END

;--------------------------------------------------------------------

PRO HSI_Eventlist_Packet::Adjust_Absolute_Time_Range, absolute_time_range

  ; absolute time range contains the UT time of the analysis time
  ; interval.

  time_range = self->Get( /TIME_RANGE )

  ; when time range is in anytim, then it *is* the absolute time range
  IF time_range[0] GT 1e6 THEN BEGIN
    absolute_time_range = time_range
  ENDIF ELSE BEGIN
    time_interval = self->Get( /OBS_TIME_INTERVAL )
    IF time_interval[0] EQ 0 THEN BEGIN
      time_interval = self->Get( /FILE_TIME_RANGE )
    ENDIF
    IF NOT Valid_Range( time_range ) THEN BEGIN
      absolute_time_range = time_interval
    ENDIF ELSE BEGIN
      absolute_time_range = time_interval[0] + time_range
    ENDELSE
  ENDELSE

  self->HSI_Eventlist_Strategy::Adjust_Absolute_Time_Range, absolute_time_range

END

;--------------------------------------------------------------------

PRO HSI_Eventlist_Packet::Select_By_Time, $
  absolute_time_range, time_unit, ut_ref, n_event, $
  ev

  ; adds the livetime handling to the generic select_by_time prcedure

  ut_ref = self->Get( /EV_UT_REF )

  self->HSI_Eventlist_Strategy::Select_by_time, $
    absolute_time_range, time_unit, ut_ref, n_event, ev, $
    time_range_bmicro

  IF Ptr_Valid( self.livetime ) THEN BEGIN
    IF Size( *self.livetime, /TYPE ) EQ 8 THEN BEGIN
      minmax_idx = Value_Locate( (*self.livetime).time, time_range_bmicro )
      minmax_idx[0] = minmax_idx[0] > 0
      IF minmax_idx[1] GT minmax_idx[0] THEN BEGIN
        *self.livetime = (*self.livetime)[minmax_idx[0]:minmax_idx[1]]
      ENDIF
    ENDIF
  ENDIF

END

;--------------------------------------------------------------------

function hsi_eventlist_packet::need_update

  return, self->framework::need_update( /no_last_update )

end

;--------------------------------------------------------------------

PRO HSI_Eventlist_Packet::Process_Hook, $
  source, absolute_time_range, time_unit, $
  eventlist, ut_ref, ev_time_unit, $
  ATTACH_ATTENUATOR = attach_attenuator, $
  FIRST_BUNCH=first_bunch, $
  NEXT_BUNCH=next_bunch, $
  ALL=all, $
  DONE=done,$
  THIS_TIME_RANGE_EXTENSION=this_time_range_extension,$
  _EXTRA=_extra


  ; absolute_time_range, gives the UT time by which
  ; the packets will be selected (via collect time)
  ;absolute_time_range = self->Get( /ABSOLUTE_TIME_RANGE )
  ;14-jun-02 - may need an extended time range to account for datagaps
  If Exist( this_time_range_extension ) then $
    absolute_time_range = absolute_time_range + this_time_range_extension * [-1.,1.]
  packet = source->GetData( DONE=done, $
    APP_ID=100, $
    FIRST_BUNCH=first_bunch, $
    NEXT_BUNCH=next_bunch, $
    PACKET_TIME_RANGE=absolute_time_range, $
    _EXTRA=_extra )
  
 
  
  ;  ENDELSE
  If keyword_set( all ) || keyword_set( first_bunch ) then begin
    ptr_free, self.overlap_packets
    self.overlap_packets = ptr_new(0)

  Endif else packet = ptr_valid( self.overlap_packets ) ? [ *self.overlap_packets, packet ] : packet




  IF self.debug GT 5 THEN BEGIN
    Message, 'Packets = ', /INFO
    help, packet
  ENDIF

  Ptr_Free, self.livetime

  ; this needs to be set up before the following if
  ev_time_unit = 1

  IF Size( packet, /TYPE ) NE 2 THEN BEGIN

    simulated_data = self->Get( /SIMULATED_DATA )
    max_pak = self->Get( /PACKET_PER_BUNCH )
    IF Keyword_Set( FIRST_BUNCH ) THEN BEGIN
      last2 = 0
    ENDIF ELSE BEGIN
      last2 = self->Get( /LAST2 )
    ENDELSE
    ; ras, 10-feb-2017 support L64 time
    ev_time_unit = 1

    no_livetime = self->Get( /NO_LIVETIME )
    rollover_test = Self->Get(/EVL_ROLLOVER_TEST) ;ADDED 22-SEP-2015, RAS

    HSI_Packet2Eventlist, packet, collect_time, $
      eventlist, ut_ref_full, $
      LAST2=last2, $
      LIVETIME=livetime, $
      ROLLOVER_TEST = rollover_test, $
      SIMULATED_DATA=simulated_data, $
      TIME_UNIT=ev_time_unit, $
      NO_LIVETIME = no_livetime, $
      UT_REF_LIVETIME=ut_ref_livetime, $
      IS_EVENT = is_event
    self->Set, LT_UT_REF = hsi_sctime_convert(ut_ref_livetime,/full)
    self->Set, EV_UT_REF = ut_ref_full
    self->Set, LAST2 = last2 ;20-jul-2005, ras, actually save LAST2!!!!!
    ut_ref = HSI_Sctime2Any( ut_ref_full )
    
    ;RAS, 17-apr-2017, add the attenuator state to each event by first extracting it from each packet header
    ;Cannot use the lookup table because we are prepending packets to the current bunch. We could save the 
    ;lookup table info, but we can read the attenuator state directly from the packet header here
    hsi_data_header_unpack, packet, byte_out, col_ids, front_dec, rear_dec
    ; now store the livetime
    IF N_Elements( livetime ) NE 0 THEN BEGIN
      Ptr_Free, self.livetime
      self.livetime = Ptr_New( livetime )
    END

    ; now do the droupout list
    if Self->Get(/dp_enable) then begin ;RAS, 17-apr-2017, add the attenuator state to each event
      eventlist = add_tag( eventlist, ((bytarr(270)+1b)#byte_out[11,*])[*], 'att_state')
    
      dropout_list = hsi_dropoutlist( eventlist, ut_ref,  OBJ=self, done=done, livetimelist=livetime )
    endif
    case 1 of
      Keyword_Set( attach_attenuator ) and have_tag( eventlist,'att_state'): 
      Keyword_Set( attach_attenuator ) and ~have_tag( eventlist,'att_state'): $
        eventlist = add_tag( eventlist, ((bytarr(270)+1b)#byte_out[11,*])[*], 'att_state')  
      ~Keyword_Set( attach_attenuator ) and have_tag( eventlist,'att_state'): $
        eventlist = rem_tag( eventlist,'att_state')
      else: 
    endcase
    ;put the last two seconds into overlap packets
    ;In Manage_Overlap - packets less than 2 seconds from the end are saved to prepend to the next bunch
    ;Any events in the saved packets are not returned from manage_overlap until the next iteration

    if ~keyword_set(all) then $
      self->manage_overlap, $
      packet, $
      collect_time, $
      eventlist, $
      is_event
    IF N_Elements( livetime ) NE 0 THEN BEGIN

      *self.livetime = livetime
    END

    if Done then ptr_free, self.overlap_packets
    Ptr_Free, self.dropout_list
    self.dropout_list = Ptr_New( dropout_list )

  ENDIF

END

;---------------------------------------------------------------------------
PRO HSI_Eventlist_Packet::manage_overlap, $
  packet, $
  collect_time, $
  eventlist, $
  is_event

  lt_ut_ref   = self->get(/LT_UT_REF)
  ut_ref_full = self->get(/EV_UT_REF)
  ;find the packet two seconds before the end of the eventlist's last packet collect_time
  ;all of the packets after this one will be appended to the next iteration. We remove
  ;events located those now prepended packets so they won't be double counted. They are counted in the spectrogram
  ;on the next iteration
  iover = value_locate( collect_time, (last_item( collect_time ) - 2L^21)) - 1
  if iover le 0 then return
  if ~ptr_valid( self.overlap_packets ) then self.overlap_packets = ptr_new( packet[iover:*]) else $
  *self.overlap_packets = packet[iover:*]
  q = where( is_event lt 270LL * iover, nq )
  eventlist = Self->select_eventlist( eventlist, range=is_event[q] )


end
;
function HSI_EventList_Packet::select_eventlist, evl, range = range
;Don't be fooled, changed to evl from eventlist to keep the lines from getting too long
  nel = n_elements( evl.time )
  default, range, l64indgen( nel )

  
  out = have_tag( evl, 'att_state' ) ? $ 
      {time: evl.time[range], a2d_index:evl.a2d_index[range], channel:evl.channel[range], att_state: evl.att_state[range] } $
    : {time: evl.time[range], a2d_index:evl.a2d_index[range], channel:evl.channel[range] }

  return, out
end

;---------------------------------------------------------------------------
PRO HSI_EventList_Packet__Define

  self = { HSI_Eventlist_Packet, $
    bunch_nr: 0L, $
    livetime: Ptr_New(), $
    dropout_list: Ptr_new(), $
    overlap_packets: ptr_new(), $

    INHERITS HSI_Eventlist_Strategy }

END

;---------------------------------------------------------------------------
; End of 'hsi_eventlist_packet__define.pro'.
;---------------------------------------------------------------------------
