;---------------------------------------------------------------------------
; Document name: hsi_visibility__define.pro
; Time-stamp: <Thu Nov 27 2008 11:20:28 csillag soleil.i4ds.ch>
;+--------------------------------------------------------------------------
;
; PROJECT:
;       HESSI
;
; NAME:
;       HESSI VISIBILITY CLASS DEFINITION
;
; PURPOSE:
;      Provides data structures and methods to generate and access visibilites. This
;      is the "user" interface to hsi_visibility_raw and hsi_visibility_file, which
;      can either generate the vis' from scratch or read them from a file
;
; CATEGORY:
;       HESSI Utilities
;
; CONSTRUCTION:
;       vis = Hsi_Visibility( )
;       vis = Obj_New('hsi_visibility')
;
; CONTROL PARAMETERS
;
; KEYWORDS:
;
; EXAMPLES:
;
; SEE ALSO:
;       hsi_visibility_raw__define
;       hsi_visibility_file__define
;
; HISTORY:
;   14-Jun-2017, Kim. Removed no_vis_conjugate and no_filter keywords from getdata call (not used), passed quiet on to prep routine
;     (not used before), and added a lot of explanation for getdata and get_vis_reg
;   23-Mar-2017, Kim. In write, added namedialog, path, and force keywords. Added this_det_index_mask keyword to getdata
;		24-jun-2013, we must actually set im_energy_binning. It wasn't
;         before from the vis object, only from image after we added these checks last January, now if it
;		  passes via set it will be set and then reconciled
;		22-jan-2013 -- richard.schwartz@nasa.gov, better check for required visibilities in ::regularized_inversion
;		17-oct-2012 -- richard.schwartz@nasa.gov
;			Added a read method for vis_input_fits files,
;			This way the user can evaluate what is in the files (sp_energy_binning and
;			time_binning) so they can see if that's what they want. Better for interactive usage
;		10-jan-2012 -- richard.schwartz@nasa.gov
;			adding support for visibility regularization,
;			for this we need to work with an energy spectrum for each visibility

;       27-dec-2009 -- Andre, corrected broken management of tb_index
;                      and eb_index -- see also _raw__define and calib_eventlist
;       22-dec-2009 -- Andre added /dump and changed the meaning of
;                      /all. Now, /all means all visibilities but
;                      taking into account the settings of vis_edit
;                      vis_conjugate etc. /dump just passes all
;                      visibilities available.
;       18-Aug-2009, Kim. Added cleanup method
;       26-May-2009 Kim In getdata::, added quiet keyword and call to vis_normalize
;         now returns vis_det_index_mask_used
;       6-May-2009 ACS Added self.reload flag, removed yes_new_det
;       Jan-2009.  ACS Lots of changes, including:
;                  vis_time_interval is gone.  Now just use im_time_interval.
;                  vis_combine is now called vis_conjugate
;                  Added vis_normalize call in getdata
;                  vis_get_mode is gone.  Now use tb_index, eb_index to get
;                    specific visibilities
;       17-dec-2008 -- acs, after discussions with gordon at SSL,
;                      hsi_vis_combined is called only when
;                      hsi_vis_adit has been called. Also,
;                      the call to hsi_vis_combine is really a call to
;                      hsi_vis_combine, /conj. The parameter
;                      /conjugate can be overpassed by the keyword to
;                      ::getdata /no_vis_conjugate. That is why also
;                      the parameter name has changed to vis_conjugate.
;       01-Nov-2008 -- acs, this new verison has removed the original
;                      vis_map, which was causing more troubles than
;                      anything else. Now, it uses the same system as
;                      we used for det_index_mask to keep track of
;                      which visibility interval has been already
;                      computed.
;       03-mar-2008, Kim. Commented out vis_allow_reprocess stuff
;       31-dec-2007 -- acs, now we keep track of the image object of
;                      present to set parameters correctly in case of
;                      vis files.
;       21-Nov-2007, Kim. In getdata, if value returned by hsi_vis_edit isn't
;         a structure, return -1
;       Version w/ multiple energy bands and times aug - oct 2006 acs
;       Many tests and extensions Dec 2005 acs / ras
;       First object version acs sept 2005
;       Algorithm and fundamental development: 2005,
;           G.Hurford, E.J.Schmahl and R.A.Schwartz
;
;---------------------------------------------------------------------------

pro hsi_visibility_test

  o = hsi_visibility()
  o = hsi_image()
  o->set, det_index_mask=[0,0,1,1,1,1,1,1,0]
  o->set, xyoffset = [88,-330]
  ;o->set, im_time= '29-oct-2003 ' + ['20:41', '20:42:12']
  ;o->set, im_time= '29-oct-2003 ' + ['20:41', '20:41:30']
  ;o->set, sp_energy_binning=[25,50]
  ;d = o->getdata()
  o->set,  im_time_int= '29-oct-2003 ' + ['20:41', '20:42', '20:43']
  o->set, sp_energy_binning=[25,40,150]
  d = o->getdata()

  o = hsi_visibility( )
  o->set, im_time_int = '20-feb-02 11:' + ['06:20', '06:21', '06:22']
  o->set, sp_energy_binning=[6,12, 25]

  ; this should generate vis for eb index 1 and 2
  vis1 = o->getdata( tb_index = 0, eb_index = 0 )
  plot, vis1.u, vis1.v, psym = 2

  ; test the /all and /dump keywords:
  vis_all = o->getdata( /all )
  vis_dump = o->getdata( /dump )

  ; this should be 0
  print, o->need_update()

  ; now we change tb_index from 0 to 1, then need_update should be 1
  o->set, tb_index = 1
  print, o->get( /need )
  vis2 = o->getdata( eb_index = 0 )

  o->write, 'aa.fits'

  obj_destroy, o

  o = hsi_visibility( )
  o->set, vis_out_filename = 'aa.fits'
  data = o->getdata()

  ; should take the data for eb1 and 2

  vis2= o->getdata( tb_index = 0, eb_index = 1 )
  oplot, vis2.u, vis2.v, psym=3

  vis3= o->getdata( tb_index = 1, eb_index = 0 )
  oplot, vis.u, vis.v, psym = 4
  vis4 = o->getdata( tb_index = 1, eb_index = 1 )
  oplot, vis.u, vis.v, psym=5

  print, o->get( /sp_energy_binning )
  vis = o->getdata()
  urf =  (*cbe[0]).count
  plot, urf
  o->set, use_phz = 1
  o->write
  print, o->get( /sp_energy_bin )

  obj_destroy, o
  o = hsi_visibility()
  o->set, cbe_filename = 'gaga.fits'
  print, o->get( /cbe_filename )
  cbe2 = o->getdata()

  obj_destroy, o
  o = hsi_visibility()
  o->set, vis_filename = 'gaga.fits'
  v = o->getdata()


  o = hsi_image()
  o = hsi_image( cbe_filename = 'gaga.fits' )
  o->set, obs_time = '20-feb-02 11:' + ['06:00', '06:21']
  o->set, sp_energy_binning=[6,12]
  o->plot

  file = hsi_loc_file( 'hsi_20020218*fits', COUNT=c )

  print, 'test of the  use_window parameter for self-calibration'

  o = hsi_visibility( filename=file[0] )
  o->set, time_range = [0, 432.84]
  o->set, as_spin_period = 4.3284
  cbe = o->getdata()
  plot, (*cbe[4,0]).time, (*cbe[4,0]).count

end

;----------------------------------------------------------------------------

FUNCTION Hsi_Visibility::Init, $
  SOURCE=source, $
  image_obj = image_obj, $
  _EXTRA=_extra

  IF NOT Keyword_Set( SOURCE ) THEN begin
    src = Obj_New( 'HSI_calib_Eventlist' )
  endif else begin
    ;    src = source->get( class = 'hsi_calib_eventlist', /obj )
    src = source
  endelse

  if obj_valid( image_obj ) then self.image_obj = image_obj

  processor_name = ['hsi_visibility_raw', 'hsi_visibility_file']

  ret=self->hsi_fileandraw::INIT( processor_name, $
    CONTROL = Hsi_Visibility_Control(), $
    INFO={hsi_visibility_info}, $
    SOURCE=src )

  if exist( _extra ) then self->set, _extra = _extra

  self.data = ptr_new(/alloc)
  RETURN, ret

END

;--------------------------------------------------------------------------

pro hsi_visibility::cleanup
  self->hsi_fileandraw::cleanup
end
;--------------------------------------------------------------------------

;----------------------------------------------------------

pro hsi_visibility::setreload
  self.reload = 1
end

;----------------------------------------------------------

pro hsi_visibility::unsetreload
  self.reload = 0B
end

;----------------------------------------------------------

function hsi_visibility::getreload
  return, self.reload
end
;----------------------------------------------------------
pro hsi_visibility::reconcile_energy_binning, force=force, $
  sp_energy_binning=sp_energy_binning, im_energy_binning=im_energy_binning
  ;Make sure sp_energy_binning, im_energy_binning, img_alg, and vis_type are consistent
  ;
  ;Unless we have a regularization to do, im_energy_binning has already set
  ;sp_energy_binning to be the same so we are done

  vis_type= hsi_vis_type( self->get(/vis_type))
  vis_input_fits = Self->Get(/vis_input_fits)
  if ~stregex(vis_type,/bool,/fold,'reg') and ~file_exist(vis_input_fits)  then return

  test= hsi_vis_check_ebins( self, /set_binning,	 $
    sp_energy_binning=sp_energy_binning, im_energy_binning=im_energy_binning, msg=msg)

  if ~test then begin

    print,'Set im_energy_binning to a minimum of 5 (reg_photon) or 10 (reg_electron) bins of equal width (2-6 keV) starting at >=10 keV'
  endif
  ;		endif else begin ;so it must be regularized photon
  ;		endelse

  ;	scaling = cntrl.vis_photon2el
  ;	sp_energy_binning = noreg ? im_energy_binning : hsi_vis_ph_energy(im_energy_binning, scaling)
  ;;	help, im_energy_binning, noreg, sp_energy_binning
  ;;	wait, .1
  ;	if ~same_data( get_edges(/edges_1, sp_energy_binning), get_edges(/edges_1, cntrl.sp_energy_binning)) then begin
  ;		be_obj = Self->Get(/obj, class='hsi_binned_eventlist')
  ;		;be_obj->framework::set, im_energy_binning = im_energy_binning, /no_update
  ;		;help, im_energy_binning, noreg, sp_energy_binning
  ;		be_obj->Set, sp_energy_binning = sp_energy_binning
  ;		;if be_obj->need_update() then stop
  ;		endif
  ;	endif


end
;----------------------------------------------------------


;----------------------------------------------------------

pro hsi_visibility::set, _Extra=_extra, $
  vis_input_fits = vis_input_fits, $
  vis_filename = vis_filename, $
  vis_edit = vis_edit, vis_chi2lim = vis_chi2lim, $
  vis_conjugate = vis_conjugate, vis_plotfit = vis_plotfit, $
  vis_normalize = vis_normalize, $
  not_found = not_found, done = done, $
  tb_index = tb_index, eb_index = eb_index, $
  det_index_mask = det_index_mask, $
  vis_type = vis_type, $
  sp_energy_binning=sp_energy_binning,$
  im_energy_binning = im_energy_binning, $
  force = force


  ; This is an unfortunate kludge to make sure that the parameters
  ; associated with the visibility class do get set even when there is a
  ; file set in vis_in.
  ; Setting these should trigger reprocessing image, but not vis (since they operate in getdata).
  if exist( vis_edit ) or exist( vis_chi2lim ) or exist( vis_conjugate ) or exist( vis_plotfit ) or $
    exist( vis_normalize )  then begin
    self->framework::set, vis_edit = vis_edit, vis_chi2lim = vis_chi2lim, $
      vis_conjugate = vis_conjugate, vis_plotfit = vis_plotfit, $
      vis_normalize = vis_normalize

  endif
  if exist(vis_type) or exist(im_energy_binning)  then begin
    ;Self->framework::Set, vis_type=vis_type ;24-jun-2013, we must actually set im_energy_binning. It wasn't
    ;before from the vis object, only from image after we added these checks last November!!
    Self->framework::Set, vis_type=vis_type
    vis_type = hsi_vis_type( Self->framework::Get(/vis_type))
    ;This sets im_energy_binning and sp_energy_binning in the binned eventlist, this must be tested for conflict first
    ;This needs to be changed
    Self->framework::Set, vis_type=vis_type, im_energy_binning = im_energy_binning, /force

    ;This will change sp_energy_binning if it should be different from im_energy_binning as
    ;with regularized_electron imaging for data input.  For vis_input_fits it will change
    ;im_energy_binning to be consistent
    Self->reconcile_energy_binning, force=force, $
      sp_energy_binning=sp_energy_binning, im_energy_binning=im_energy_binning
  endif


  if is_number( eb_index ) then begin
    ; for eb_index change, we need to make sure we select the correct
    ; ones, but we should not propagate to calib_eventlist
    if not same_data( self.eb_index, eb_index ) then begin
      self.eb_index = eb_index
      self.yes_new_eb = 1b
    endif
  endif

  if is_number( tb_index ) then begin
    ; for tb_index change, we need to make sure we select the correct
    ; ones, but we should not propagate to calib_eventlist
    if not same_data( self.tb_index, tb_index ) then begin
      self.tb_index = tb_index
      self.yes_new_tb = 1b
    endif
  endif

  ; convert to bytarr(9) in case reading old file with bytarr(9,3), kim 18-dec-2008
  if keyword_set( det_index_mask ) then begin
    if not same_data( self.det_index_mask, bytarr(9) + det_index_mask, /NOTYPE_CHECK ) then begin
      self.det_index_mask = bytarr(9) + det_index_mask
      self->setreload
    endif
  endif




  ; for compatibility reasons
  ; kim changed is_string to exist in next 2 lines 21-apr-2009
  if exist( vis_filename ) then vis_input_fits = vis_filename
  if exist( vis_input_fits ) then filename = vis_input_fits


  ; now we set filename here in addition to vis filename as filename will
  ; control the choice of processor -- either generate from raw or get
  ;                                    from file.

  is_file = is_string( self->get( /vis_in, /this ) ) or is_string( vis_input_fits )
  cbe = self->get( class = 'hsi_calib_eventlist', /obj )
  if is_file then cbe->disableset else cbe->enableset


  if keyword_set( _extra ) then begin

    if  is_file and not keyword_set( force ) then begin

      ;        if self.debug gt 0 then begin
      ;            message, "Parameter not set, in file input mode, set vis_input to '' to enable " + $
      ;                     "setting and reprocessing", /info, /cont
      ;        endif
    endif else really_extra = _extra

  endif

  self->hsi_fileandraw::set, _extra = really_extra, $
    not_found = not_found, done = done, vis_input_fits = vis_input_fits, $
    ;vis_time_intervals = vis_time_intervals, $
    no_update = no_update,$ ; vis_process_all = vis_process_all, $
    filename=filename, time_range=time_range, force = force , $
    det_index_mask = det_index_mask




end
;----------------------------------------------------------
pro hsi_visibility::set_2, _Extra=_extra, $
  vis_input_fits = vis_input_fits, $
  vis_filename = vis_filename, $
  vis_edit = vis_edit, vis_chi2lim = vis_chi2lim, $
  vis_conjugate = vis_conjugate, vis_plotfit = vis_plotfit, $
  vis_normalize = vis_normalize, $
  ;;                          vis_time_intervals=vis_time_intervals, $
  ;;                          time_range=time_range,  $
  ;;                          this_class_only=this_class_only,  $
  not_found = not_found, done = done, $
  tb_index = tb_index, eb_index = eb_index, $
  det_index_mask = det_index_mask, $
  vis_type = vis_type, $
  sp_energy_binning=sp_energy_binning,$
  im_energy_binning = im_energy_binning, $
  force = force
  ;;                          no_update = no_update, $
  ;;                          energy_band = energy_band


  ; this is an unfortunate kludge to make sure that the parameters
  ; associated with the visibility class do get set even when there is a
  ; file set in vis_in.
  if exist( vis_edit ) or exist( vis_chi2lim ) or exist( vis_conjugate ) or exist( vis_plotfit ) or $
    exist( vis_normalize )  then begin
    self->framework::set, vis_edit = vis_edit, vis_chi2lim = vis_chi2lim, $
      vis_conjugate = vis_conjugate, vis_plotfit = vis_plotfit, $
      vis_normalize = vis_normalize

  endif
  if exist(vis_type) or exist(im_energy_binning) or exist(im_energy_binning) then begin
    Self->framework::Set, vis_type=vis_type
    vis_type = hsi_vis_type( Self->framework::Get(/vis_type))
    ;This sets im_energy_binning and sp_energy_binning in the binned eventlist, this must be tested for conflict first
    ;This needs to be changed
    Self->framework::Set, vis_type=vis_type ;, im_energy_binning = im_energy_binning, /force

    ;This will change sp_energy_binning if it should be different from im_energy_binning as
    ;with regularized_electron imaging for data input.  For vis_input_fits it will change
    ;im_energy_binning to be consistent
    Self->reconcile_energy_binning, force=force, $
      sp_energy_binning=sp_energy_binning, im_energy_binning=im_energy_binning
  endif


  if is_number( eb_index ) then begin
    ; for eb_index change, we need to make sure we select the correct
    ; ones, but we should not propagate to calib_eventlist
    if not same_data( self.eb_index, eb_index ) then begin
      self.eb_index = eb_index
      self.yes_new_eb = 1b
    endif
  endif

  if is_number( tb_index ) then begin
    ; for eb_index change, we need to make sure we select the correct
    ; ones, but we should not propagate to calib_eventlist
    if not same_data( self.tb_index, tb_index ) then begin
      self.tb_index = tb_index
      self.yes_new_tb = 1b
    endif
  endif

  ; convert to bytarr(9) in case reading old file with bytarr(9,3), kim 18-dec-2008
  if keyword_set( det_index_mask ) then begin
    if not same_data( self.det_index_mask, bytarr(9) + det_index_mask, /NOTYPE_CHECK ) then begin
      self.det_index_mask = bytarr(9) + det_index_mask
      self->setreload
    endif
  endif


  ; this is needed in case the obs tim interval remains the same but the
  ; number of time ranges change.
  ;if exist( vis_time_intervals ) then begin
  ;    vis_time_intervals = anytim( vis_time_intervals )
  ;    oldvistimeint =  self->get( /vis_time_int,  /this)
  ;    if not same_data2( vis_time_intervals,  oldvistimeint ) then begin
  ;      self -> hsi_fileandraw::set, obs_time_int=minmax( vis_time_intervals )
  ;      destroy_vis_map =  1
  ;    endif
  ;endif

  ;; acs 2007-11-21 if you change the im energy binning, then you should
  ;; reprocess.this is just for the files
  ;; if exist( sp_energy_binning ) then begin
  ;;    allow_update = self->Get( /vis_allow_reprocess )
  ;;    if allow_update then begin
  ;;       old_im  = self->get( /sp_energy_binning )
  ;;       if not same_data2( old_im, sp_energy_binning ) then begin
  ;; putting the filename to null will switch to the raw telemetry processing
  ;;          filename = ''
  ;;       endif
  ;; this because otherwise we dont want to set it
  ;;       imenbintoset = sp_energy_binning
  ;;    endif else begin
  ;;       Message, 'Cannot change sp_energy_binning with visibility files', /info, /cont
  ;;       Message, 'To allow reprocessing, set vis_allow_reprocess to 1', /info, /cont
  ;;    endelse
  ;; endif

  ;if is_number( tb_index ) or is_number( eb_index ) then self->set, vis_process = 0, /no_update

  ; for compatibility reasons
  ; kim changed is_string to exist in next 2 lines 21-apr-2009
  if exist( vis_filename ) then vis_input_fits = vis_filename
  if exist( vis_input_fits ) then filename = vis_input_fits


  ; now we set filename here in addition to vis filename as filename will
  ; control the choice of processor -- either generate from raw or get
  ;                                    from file.

  is_file = is_string( self->get( /vis_in, /this ) ) or is_string( vis_input_fits )
  cbe = self->get( class = 'hsi_calib_eventlist', /obj )
  if is_file then cbe->disableset else cbe->enableset


  if keyword_set( _extra ) then begin

    if  is_file and not keyword_set( force ) then begin

      ;        if self.debug gt 0 then begin
      ;            message, "Parameter not set, in file input mode, set vis_input to '' to enable " + $
      ;                     "setting and reprocessing", /info, /cont
      ;        endif
    endif else really_extra = _extra

  endif

  self->hsi_fileandraw::set, _extra = really_extra, $
    not_found = not_found, done = done, vis_input_fits = vis_input_fits, $
    ;vis_time_intervals = vis_time_intervals, $
    no_update = no_update,$ ; vis_process_all = vis_process_all, $
    filename=filename, time_range=time_range, force = force, $
    det_index_mask = det_index_mask


  ;;  vismap = self->get( /vis_map, /this )
  ;;  if obj_valid( vismap ) then begin
  ;;    if exist( destroy_vis_map ) then begin
  ;;       vismap->invalidate
  ;; ;      if obj_valid( self.image_obj ) then begin
  ;; ;         energy_axis = self.imge_obj->get( /energy_axis )
  ;; ;         obj_destroy, energy_axis
  ;; ;      endif
  ;; ;      dummy = self->getdata() ; need to actualize all parameters again.
  ;;    endif else begin
  ;;        if not is_string( self->get( /vis_filename, /this ) ) then begin ;or $
  ;; ;           self->framework::get( /vis_allow_reprocess, /this) then begin
  ;;            cbe_obj = self -> get( class='hsi_calib_eventlist', /obj )
  ;;            if obj_valid( cbe_obj ) then begin
  ;;                if cbe_obj -> need_update() and keyword_set(_extra) then begin
  ;;                                 ;help,  _extra,  /str
  ;;                    vismap -> invalidate
  ;; ;                   if self->framework::get( /vis_allow_reprocess ) then self->hsi_fileandraw::set, filename = '', /this
  ;;                endif
  ;;            endif
  ;;        endif
  ;;    endelse
  ;;  endif
  ;; self->hsi_fileandraw::set, vis_get_mode = vis_get_mode, $
  ;;                       tb_index=tb_index, $
  ;;                       eb_index=eb_index, /this_class
  ;; ;stop

end

;----------------------------------------------------------

;----------------------------------------------------------

function hsi_visibility::unpack, vis, all =all, dump = dump, tb_index = tb_index, eb_index = eb_index, $
  spectrum_mode=spectrum_mode

  n = n_elements( vis )

  If keyword_set( dump ) or keyword_set( all ) then begin
    for i = 0, n-1 do begin
      retvis = append_arr( retvis, *vis[i] )
    endfor
    return, retvis
  endif

  if keyword_set( spectrum_mode ) then begin ;append all vis for tb_index
    dim = size(/dim, vis)
    for i = 0, dim[1]-1 do retvis = append_arr( retvis, *vis[tb_index, i])

    return, retvis
  endif
  ; this might get more complicated later

  return, *(vis[tb_index, eb_index])

end

;----------------------------------------------------------

function hsi_visibility::process_all

  return, self.process_all

end

;----------------------------------------------------------

function hsi_visibility::yes_new_tb

  return, self.yes_new_tb

end
;----------------------------------------------------------

function hsi_visibility::yes_new_eb

  return, self.yes_new_eb

end

;----------------------------------------------------------

function hsi_visibility::get_tb_index

  return, self.tb_index

end
;----------------------------------------------------------
function hsi_visibility::regularized_inversion, vis, vis_type=vis_type,$
  eb_index=eb_index, all=all, el_energy_max_factor=el_energy_max_factor,$
  _extra=_extra

  if ~stregex(vis_type,/boolean, /fold,'reg')  then return, vis ;do nothing

  ;Have we already regularized these vis?
  if ~total(ptr_valid(self.vis_reg)) then self.vis_reg =ptrarr(4,/alloc)
  vis_reg_store = self.vis_reg ;this is a pointer
  if  size(/tname, *vis_reg_store[0]) NE 'STRUCT' Then begin
    *vis_reg_store[0] = vis
    mk_invrsn  = 1
  endif else begin
    ;only check finite obsvis
    mk_invrsn = ~ptr_valid(vis_reg_store[1])
    mk_invrsn = mk_invrsn eq 0 ? size( *vis_reg_store[1],/tname) ne 'STRUCT' : mk_invrsn
    z = where( finite(vis.obsvis))
    zz= where( finite((*vis_reg_store[3]).obsvis)) ;Need this selection as well, ras, 22-jan-2013
    ;mk_invrsn = mk_invrsn eq 0 ? ~same_data2( (*vis_reg_store[3])[z], vis[z]) : mk_invrsn ;old version
    mk_invrsn = mk_invrsn eq 0 ? ~same_data2( (*vis_reg_store[3])[zz], vis[z]) : mk_invrsn ;new 22-jan-2013

  endelse



  if mk_invrsn eq 1 then begin
    message,/info,'ENTERING REGULARIZATION
    vis_orig_noprep = vis
    vis = hsi_vis_prep( vis, self,/quiet )
    print, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ptim, vis[0].trange
    print, get_uniq(vis.erange)
    help, vis[0].type
    print, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Self->reconcile_energy_binning
    ;all the regularized edges

    reg_edg = get_edges(Self->Get(/im_energy_binning),/edges_1)
    photon2el = Self->Get(/vis_photon2el) ; electron/photon energy scaling factor

    hsi_visibilities_inversion, self, vis, $
      reg_el_vis, orig_ph_vis, reg_ph_vis, /EDITED, $
      el_energy_max_factor=photon2el, /INTEGRATE_ENERGY
    ;Note, these vis are all based on the det_index_mask
    ;The original vis stored in self.data are for all det
    ;So if you extract vis_reg_store you'll get that subset
    *vis_reg_store[0] = vis  ;prepped
    *vis_reg_store[1] = reg_el_vis
    *vis_reg_store[2] = reg_ph_vis
    *vis_reg_store[3] = vis_orig_noprep
  endif else begin
    reg_edg = get_edges(Self->Get(/im_energy_binning),/edges_1)
    ;	vis_ph_prep= vis_reg_store[0]
    ;	reg_el_vis = vis_reg_store[1]
    ;	reg_ph_vis = vis_reg_store[2]
  endelse
  default, vis_type, hsi_vis_type('reg_electron') ;we use hsi_vis_type to manage the allowed strings
  vis_type = strlowcase(vis_type)
  ovis = vis_reg_store[0] ;ptr to normal photons/prepped
  if ~keyword_set(all) and exist(eb_index) then begin
    case 1 of
      vis_type eq hsi_vis_type('reg_electron') : ovis = vis_reg_store[1]
      vis_type eq hsi_vis_type('reg_photon')   : ovis = vis_reg_store[2]
      else: ovis = orig_ph_vis
    endcase   
    z = where( abs( (*ovis).erange[0] -(reg_edg[eb_index+[0]])[0] ) + $
      abs( (*ovis).erange[1] -(reg_edg[eb_index+[1]])[0] ) eq 0, nz)
  endif else z = indgen(n_elements((*ovis)))
  return, (*ovis)[z]

  return, vis ; should never get here
end
;----------------------------------------------------------
; conjugate (same as combine), edit, and normalize are all done in the getdata call - the saved visibilities are unfiltered.
; noprocess = return whatever's in *self.data (will be unfiltered photon vis for all dets)
; all = get PHOTON vis for all times and energies for current det selection, filtered (edit,combine, normalize applied)
; dump = get PHOTON vis for all times, energies, dets unfiltered (NO edit, combine, or normalize)
;   NOTE: To get all regularized vis, use get_vis_reg method.
; eb_index, tb_index - time and energy band indicese to return filtered vis for current det selection for
; paout - returns the position angles in this named variable
; sortpa - if set, sorts the vis by position angle
; this_det_index_mask - if set, use these detectors just for this call - normalization will be done using just these dets, and
;   the normalization factors (vis_corr_factors) won't be stored in obj.
; 
; spectrum_mode=1 means get all energies for requested time, for regularizing

Function hsi_visibility::GetData, _Extra=_extra,$
  conjugate=conjugate, $
  time_range = time_range, $
  edit=edit, noprocess=noprocess, $
  all = all, $
  dump = dump, $
  PAOUT = paout, $
  SORTPA = sortpa, $
  tb_index = tb_index, eb_index = eb_index, $
  this_det_index_mask=this_det_index_mask, $
  quiet=quiet

  ; this is needed by the process method of visibility_raw
  if keyword_set( noprocess ) then begin
    if not exist( *self.data ) then return, -1 else $
      return, *self.data
  endif

  if keyword_set( _extra ) then Self->Set, _extra=_extra

  ; tb_index and eb_index are variables local to getdata.
  checkvar, tb_index, 0
  checkvar, eb_index, 0
  
  vis_type = hsi_vis_type( Self->Get(/vis_type))
  ; If we're doing regularized vis, then we need spectrum (i.e. return vis for all energies)
  spectrum_mode = stregex(vis_type,/fold,/bool,'reg')
  
  if keyword_set( all ) or keyword_set( DUMP ) then self.process_all = 1

  ; now get them
  vis_input_fits=Self->get(/vis_input_fits)
  @hsi_insert_catch
  ebin_test = 1

  if ~file_exist(vis_input_fits) then begin ;
    ;trap error in energies with hsi_vis_check_ebins for vis_type if we aren't reading the file
    ;if we are reading the files we have to let the read set the im and sp energy_binning
    ebin_test = hsi_vis_check_ebins(self, /set_binning, msg=msg)
    if ~ebin_test then begin
      print, msg
      return, -1
      ;print, msg & message, 'Cannot regularize on these energy bins' ;This will go to catch
    endif
  endif
  vis = Self->hsi_fileandraw::GetData( tb_index = tb_index )

  ;trap error in energies with hsi_vis_check_ebins for reg vis_type
  ebin_test = hsi_vis_check_ebins(self,  msg=msg)
  if ~ebin_test then begin
    print, msg
    return, -1
    ;print, msg & message, 'Cannot regularize on these energy bins' ;This will go to catch
  endif

  ;If this is spectrum_mode we need to get everything
  if spectrum_mode then begin
    visall = Self->framework::getdata()
;    help, visall
  endif
  
  ; When we're done, we'll have a complete set of vis for all energies and times in the pointer
  self.yes_new_eb = 0
  self.yes_new_tb = 0
  self.process_all = 0

  ; here we need to take away the pointers

  vis = self->unpack(vis, all = all, dump = dump, tb_index = tb_index, eb_index = eb_index, $
    spectrum_mode = spectrum_mode)
  ; From now on, we have a local copy of the data for the selected time / energy index. Except if spectrum_mode was set then 
  ; vis contains all energies for time index tb_index, so we can do regularization.

  if keyword_set( dump ) then return, vis

  ; Now limit vis to requested detectors
  det_index_mask = keyword_set(this_det_index_mask) ? this_det_index_mask : self->get( /det_index_mask )
  dets = where( det_index_mask, count )
  if count gt 0 then begin
    dets_list = where_arr( vis.isc, dets, cc )
    if cc gt 0 then vis = vis[dets_list]
  endif

  ; get position angles, and sort vis by position angles if requested
  if exist( paout ) or exist( sortpa ) then begin
    list = hsi_vis_select( vis, PAOUT = PAOUT, SORTPA = SORTPA, det_index = dets )
    if list[0] eq -1 then return, -1
    print, n_elements( list ), ' visibilities selected'
    vis = vis[list]
  endif
  
;  message,/info,'In vis::getdata()'
  
  ; Either regularize the vis (filtering is done inside reg routines) or do filtering (edit, combine, normalize).
  if stregex(vis_type,/boolean, /fold,'reg') then begin
    ;This step will do the regularization across all energies for this time interval, and then select the vis
    ; for the eb_index requested from that.
    vis = self->regularized_inversion( vis, vis_type=vis_type, eb_index=eb_index, all=all,_extra=_extra) ;
  endif else begin
    vis = hsi_vis_prep( vis, self, quiet=quiet, this_det_index_mask=this_det_index_mask ); edit, combine, normalize
  endelse

  self->unsetreload

  return, vis

end

;---------------------------------------------------------------------------

pro hsi_visibility::plot, _extra = _extra, SUBCOLL = subcoll, PS = ps

  vis = self->getdata( _extra = _extra )
  hsi_vis_display, vis, SUBCOLL=subcoll, PS = ps

end

;---------------------------------------------------------------------------

PRO hsi_visibility::read,  _EXTRA = _extra
  ;Much more prudent to read in a vis_input_fits file
  ;Let the parameters get set, and let the user see what the hell they've just
  ;read


  IF keyword_set( _extra ) THEN self->Set, _EXTRA = _extra
  if file_exist(Self->Get(/vis_input_fits)) then $
    Self->hsi_fileandraw::process, /read ;


END

;--------------------------------------------------------------------------
; If filename is not provided either in filename argument or in vis_out_filename, then the default name is used (generated by
; hsi_fitswrite::defaultname, or if namedialog is set, then user is asked for output file name.
; path option is not used if dialog to choose name is used (since that lets you navigate dirs and returns the full path)
; If force is set, always reprocesses vis if necessary.  Otherwise, asks if you want to process now.
; Added namedialog, path, and force keywords, 24-May-2017, Kim.

PRO hsi_visibility::write, filename, namedialog=namedialog, path=path, force=force, _EXTRA = _extra, vis_out_filename=vis_out_filename

  checkvar, namedialog, 0

  IF n_params() EQ 0 then begin
    if is_string( vis_out_filename ) THEN filename =  vis_out_filename $
    else filename =  self -> get( /vis_out )
  endif

  ask_filename = ~is_string(filename) and namedialog

  IF keyword_set( _extra ) THEN self->Set, _EXTRA = _extra

  if self->need_update() then begin
    if ~keyword_set(force) then begin
      msg = ['Visibilities are not up-to-date.', $
        '', $
        'Are you sure you want to process them now?' ]
      answer = dialog_message(msg, /question)
      if answer eq 'No' then return
    endif
    dummy = self->getdata()
  endif

  vis = self->getdata( /noprocess )
  ; this is still in pointer format
  vis = self->unpack( vis, /all )

  writer = obj_new( 'hsi_fitswrite', self, filename = filename )

  filename = writer->get(/filename)

  if ask_filename then begin
    filename = writer->select_file(default=filename, title='Choose an output visibility file name.')
    if ~is_string(filename) then begin
      err_msg = 'No filename given. Aborting.'
      message, err_msg, /cont
      return
    endif
  endif else if keyword_set(path) then filename = concat_dir(path, filename)

  writer->set, filename=filename

  extname = 'VISIBILITY'

  ; WRITE CODE FOR THE VISIBILITY EXTENSION HERE
  message, 'Writing visibility file now', /cont

  writer->AddPar, 'EXTNAME', extname

  writer->SetData, vis
  writer->write

  ; acs 2008-12-23 this is taken away
  ; vis_map takes care itself of writing the correct data into the file
  ;vis_map = self->get( /vis_map, /this )
  ;vis_map->write, writer

  obj_destroy, writer

END

;--------------------------------------------------------------------------
; Returns all photon and regularized visibilities (FOR THE LAST SELECTED TIME INTERVAL):
; vis = visobj->get_vis_reg() returns a POINTER = Array[4]
;  *vis[0] contains the filtered (edited, combined, and normalized ) normal photon visibilities
;  *vis[1] contains the regularized electron visibilities
;  *vis[2] contains the regularized photon visibilities
;  *vis[3] contains the normal unfiltered photon visibilities

function hsi_visibility::get_vis_reg
  return, self.vis_reg
end

;--------------------------------------------------------------------------


function hsi_visibility::get_image_obj

  return, self.image_obj

end

;--------------------------------------------------------------------------

PRO Hsi_Visibility__Define

  self = {Hsi_Visibility, $
    image_obj: obj_new(), $
    eb_index: 0, $
    tb_index: 0, $
    det_index_MASK: bytarr( 9 ), $
    reload: 0B, $
    process_all: 0b, $
    yes_new_eb: 0b, $
    yes_new_tb: 0b, $
    vis_reg: ptrarr(4,/alloc), $
    inherits hsi_fileandraw }

END


;---------------------------------------------------------------------------
; End of 'hsi_visibility_raw__define.pro'.
;---------------------------------------------------------------------------
