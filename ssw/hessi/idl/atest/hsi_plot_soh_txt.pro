;+
; Name: hsi_plot_soh_txt
;
; Purpose: Plot daily average of RHESSI State of Health parameters in an interactive PLOTMAN window.
;   The data are read from the text files of 1-day averages of SOH parameters available here:
;   http://hesperia.gsfc.nasa.gov/hessidata/metadata/hsi_1day_sohdata/.
;   Files will be located automatically (under HSI_DATA_ARCHIVE, or if not available, copied from
;   archive at hesperia), so you don't need to worry about their location.
;  
; Input keywords:
;  tag - scalar or vector name(s) of SOH item(s) to plot
;   If you do not enter any tag names, a list widget will popup allowing you to choose one or
;   multiple tags.
;   The tag names can be found in the file
;    $SSW/hessi/idl/util/soh_tag_table.txt
;    Here are some of the more commonly used tags:
;    icp2t - Cold Plate Temp #2
;    css_insun - Sun Fraction
;    cryopower - Cryocooler Power
;    irad2t - Radiator Temp.
;    iaccel - Accelerometer Amplitude
;    cryo_eff2 - Cryocooler Efficiency 2
;    ipd_ctrb - Particle Detector B
;  sigma - if set, plot the sigma instead of the average value for the day
;  npacket - if set, plot the number of packets
;  show_sigma - if set, show the error on the average values (not used if sigma or npacket is set)
;  plotman_obj - INPUT/OUTPUT - plotman object reference.  If doesn't exists, creates it and
;    passes it out.  If exists, adds a panel to it.
;  dir - output directory to use if text files need to be copied to your computer
;  time_range - time range to plot in anytim format
;  stack - if set, after the panels for each soh item are created in plotman, they will be stacked in plotman
;  
; Calling sequence: hsi_plot_soh_txt
; 
; Examples:
;   hsi_plot_soh_txt, plotman_obj=p
;   hsi_plot_soh_txt, tag='icp2t', plotman_obj=p
;   hsi_plot_soh_txt, tag=['icp2t','irad2t'], plotman_obj=p
;   hsi_plot_soh_txt, tag=['icp2t','irad2t'], plotman_obj=p, /sigma
;   
;  
; Written: Kim Tolbert 14-May-2013
; Modifications:
;  23-Jan-2015, Kim.  Added stack option
; 
;-
pro hsi_plot_soh_txt, tag=tag, sigma=sigma, npacket=npacket, show_sigma=show_sigma, plotman_obj=plotman_obj, stack=stack, _extra=_extra

checkvar, sigma, 0
checkvar, npacket, 0
checkvar, show_sigma, 0
if sigma + npacket gt 1 then npacket = 0

if ~keyword_set(tag) then begin
  file = concat_dir('HESSI_PATH','idl/util/soh_tag_table.txt')
  ind = -1
  if file_test(file,/read) then begin
    list = rd_ascii(file)
    ind = xsel_list_multi(list, /index, cancel=cancel)
  endif
  if ind[0] eq -1 or cancel then begin
    message, /cont, 'No selections made or file not found: ' + file
    return
  endif
    
  cols = trim(str2cols(list,';'))
  tag = cols[0,ind] 
endif

ntag = n_elements(tag)

igood = 0

for i=0,ntag-1 do begin

  z = hsi_read_soh_txt(tag=tag[i], label=label, _extra=_extra)
  if ~is_struct(z) then continue ; skip to next plot

  y = z.ave
  desc = tag[i]
  if sigma then begin
    y = z.sigma
    show_sigma = 0
    label = label + ' Sigma'
    desc = desc + ' Sigma'
  endif else if npacket then begin
    y = z.npack
    show_sigma = 0
    label = label + ' Npacket'
    desc = desc + ' Npacket'
  endif

  ut_obj = obj_new('utplot', anytim(z.time,/ext), y, id=desc, data_unit=label)

  if show_sigma then begin
    desc = desc + ' with sigmas'
    ut_obj->set,edata=z.sigma
  endif

  if ~is_class(plotman_obj,'plotman', /quiet) then begin
      plotman_obj = obj_new('plotman', error=error, _extra = _extra )
      if error then return
  endif
  
  plotman_obj->new_panel, desc, input=ut_obj
  igood = igood + 1
  panel_desc = append_arr(panel_desc, desc)
endfor

if keyword_set(stack) then begin
  if igood gt 1 then begin 
    plotman_obj->show_panel, panel=plotman_obj->desc2panel(panel_desc[0], /number, /latest)
    plotman_obj->select
    overlay_panel = strarr(12)
    overlay_panel[1] = panel_desc[1:*] ;0th is reserved for self (for images) - so use 1,2,3,4,5
    plotman_obj->set,overlay_panel=overlay_panel
    plotman_obj->plot
    plotman_obj->set, last_window_choice = panel_desc[0]  ; so if resize plotman window, it redraws the correct plot
  endif
endif
  
end
