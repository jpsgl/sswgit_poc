;+
; Name: hsi_get_alg_name
; Purpose:
; Function to return the full name of the image algorithm or the object class name.
; User can input many forms of the algorithm names, but if it contains the key letters
; listed below, this function will return the correct name.  If it doesn't contain these letters,
; will return back projection.
;
; Calling sequence:  full_name = hsi_get_alg_name (image_algorithm)
;
; Keywords:
;   object_class - If set, then returns the object class name instead of the algorithm name
;   index - returns the index of the selected algorithm
;	prefix - if set, return the prefix field, prefix is the short form name for the algorithm
;
; Kim, 3/8/00  (like what Andre does in hsi_image__define, set method)
;      1/8/2001 - added vis/memvis
;       3/9/2001 - mem and memvis will both return mem, so do a second check for vis
;       3/9/2001 - added object_class keyword
;       3/21/2001 - added index keyword
;     16-DEC-2005, RAS, ADDED MEM_NJIT AND VIS_FORWARDFIT, changed list entry from MEM to SATO
;      to differentiate from the other MEMs, ADDED OPTIONAL STRINGS TO IDENTIFY THESE
;		ALGORITHMS
; 	15-may-2006, ras change from array to scalar
;	19-nov-2006, ras, revised to use stregex to find substrings in alg_list for
;		added flexibility.  Also, reports chosen algorithm unless QUIET is set
;	22-mar-2011, ras, added PREFIX
;	15-aug-2012, ras, corrected typo in PREFIX and added information about the meaning of "PREFIX"
; 01-Nov-2017, Kim. Removed comments showing current definition of alg structure so we don't have to keep them up-to-date
; 09-Mar-2018, Kim. Only look at algs that are active (needed when bproj became hsi_bproj_image)
;-

function hsi_get_alg_name, image_algorithm, object_class=object_class, prefix=prefix, index=index, quiet=quiet

  alg = hsi_alg_units()
  active = where(alg.active)
  alg = alg[active]

  alg_list = [alg.nknme1,alg.nknme2,alg.class]
  class_list = [alg.class,alg.class, alg.class]
  nalg   = n_elements(alg)
  index = 0
  test = stregex(alg_list, image_algorithm,/boolean,/fold_case)
  index = where( test, nindex)
  if nindex eq 0 then begin
    test = stregex(alg.name, image_algorithm,/boolean, /fold_case)
    index = where( test, nindex)
  endif
  if nindex ge 1 then begin
    index = index[0] mod nalg
    this_alg = alg[index]

    name   = this_alg.name
    oclass = this_alg.class
    ;	index  = idx[index]
    ;	if not keyword_set(quiet) then message,/continue, 'Algorithm Selected: '+name
    if keyword_set(object_class) then name= oclass
    if keyword_set(prefix) then name= this_alg.prefix

    return, name
  endif
  if not keyword_set(quiet) then  $
    message,/continue,image_algorithm +' does not match any of '+ $
    arr2str(alg_list,' ')+' for '+arr2str(alg.class,' ')
  return,''

end
