;+
;helper function, returns median, unless there are only two non-zero 
;values, then average, don't use on negative numbers
Function median_value_tested, ayy
  ayy = ayy > 0
  ok = where(ayy Gt 0, nok)
  If(nok Eq 0) Then Return, ayy[0] $ ;will be zero, but correct type
  Else If(nok Eq 1) Then Return, ayy[ok[0]] $
  Else If(nok Eq 2) Then Return, 0.5*total(ayy[ok]) $
  Else Return, median(ayy)
End
; This is different than temp_rear_decimation_correct it has the
; offset set to zero on the front end, 1 on the back..., and npts=1
; This is now used for temp_rear_decimation, for post FSW 2.4, since
; there are now rear decimation flags, jmm, 2003-03-26
; Added check and correction for isolated 0's which should never
; happen, but there is one early on 24-mar-2003
; Added check for data gaps, to correct 21-apr-2002 problem
; changed to avoid interpolating over datagaps, 29-nov-2003, jmm
; Added lt1 keyword, 7-sep-2004, jmm
; Changed to use median values for points -1 to -3 and 2 to 4 from
; flag change interval, to handle en_pt's where the count rate
; doesn't change until 12 seconds after the flag change,
; 14-dec-2017, jmm 
; Does not interpolate over st_pt, en_pt if no correction is made,
; 15-dec-2017, jmm
;-
Function temp_atten_correct, rate0, aflag0, tim_arr0, lt1 = lt1, _extra = _extra

  If(keyword_set(lt1)) Then gt1_or_lt1 = 0b Else gt1_or_lt1 = 1b
  rate = rate0
  If(max(aflag0) EQ 0) Then return, rate
  okx = where(rate GT 0.0, nokx)
  If(okx[0] EQ -1) Then return, rate

;throw out points with zero values
  rate = rate[okx]
  aflag = aflag0[okx]
  tim_arr = tim_arr0[okx]

  nrate = n_elements(rate)
  If(nrate LT 15) Then return, rate

;get start and end intervals
  temp_st_en, aflag, st_pt, en_pt
;for each start-end pair, get a correction factor, interpolate and
;multiply
  n = n_elements(st_pt)
;first check for isolated 0's, should never happen, but it does...
  If(n Gt 1) Then Begin
    ok_intv = bytarr(n)+1
    For i1 = 1,  n-1 Do Begin
      If(st_pt[i1]-en_pt[i1-1] Lt 3) Then Begin
        en_pt[i1-1] = en_pt[i1]
        ok_intv[i1] = 0
      Endif
    Endfor
    keep = where(ok_intv eq 1, n)  ;has to be at least 1
    st_pt = st_pt[keep]
    en_pt = en_pt[keep]
  Endif
;Check for no changes for the whole non-zero interval, as in
;4-may-2002 flare front decimation...
  If(n_elements(st_pt) Eq 1) Then Begin
    If(st_pt[0] Le okx[0] And en_pt[0] Ge okx[nokx-1]) Then Return, rate
  Endif
;Now you can try to correct
  subs = lindgen(nrate)
  c = 0                         ;counter for points to interpolate
  For j = 0, n-1 Do Begin
     If(en_pt[j] Gt st_pt[j]) Then Begin
        st_cfactor = 0.0
;use the 3 points before point, if they are there
        If(st_pt[j] Eq 1) Then st_arr0 = [0] $
        Else If(st_pt[j] Eq 2) Then st_arr0 = [0, 1] $
        Else If(st_pt[j] Gt 2) Then st_arr0 = st_pt[j]-[3, 2, 1] $
        Else st_arr0 = -1
;use the 3 points after if they are there, the point after st_pt[j] is ignored
        If(st_pt[j] Eq nrate-3) Then st_arr1 = [nrate-1] $
        Else If(st_pt[j] Eq nrate-4) Then st_arr1 = [nrate-2, nrate-1] $
        Else If(st_pt[j] Eq nrate-5) Then st_arr1 = [nrate-3, nrate-2, nrate-1] $
        Else If(st_pt[j] Lt nrate-5) Then st_arr1 = st_pt[j]+[2, 3, 4] $
        Else st_arr1 = -1
;Medians, if available
        If(st_arr0[0] Ne -1 And st_arr1[0] Ne -1) Then Begin
           st_top = median_value_tested(rate[st_arr0])
           st_btm = median_value_tested(rate[st_arr1])
           If(st_top Gt 0 And st_btm Gt 0) Then Begin
              st_cfactor = st_top/st_btm
           Endif 
        Endif
        en_cfactor = 0.0
;use the 3 points before point, if they are there
        If(en_pt[j] Eq 1) Then en_arr0 = [0] $
        Else If(en_pt[j] Eq 2) Then en_arr0 = [0, 1] $
        Else If(en_pt[j] Gt 2) Then en_arr0 = en_pt[j]-[3, 2, 1] $
        Else en_arr0 = -1
;use the 3 points after if they are there, the point after en_pt[j] is ignored
        If(en_pt[j] Eq nrate-3) Then en_arr1 = [nrate-1] $
        Else If(en_pt[j] Eq nrate-4) Then en_arr1 = [nrate-2, nrate-1] $
        Else If(en_pt[j] Eq nrate-5) Then en_arr1 = [nrate-3, nrate-2, nrate-1] $
        Else If(en_pt[j] Lt nrate-5) Then en_arr1 = en_pt[j]+[2, 3, 4] $
        Else en_arr1 = -1
;Medians, if available
        If(en_arr0[0] Ne -1 And en_arr1[0] Ne -1) Then Begin
           en_top = median_value_tested(rate[en_arr1]) ;opposite of st_top, st_btm 
           en_btm = median_value_tested(rate[en_arr0])
           If(en_top Gt 0 And en_btm Gt 0) Then Begin
              en_cfactor = en_top/en_btm
           Endif 
        Endif
;So cfactor is unattenuated/attenuated
        If(en_cfactor Eq 0.0 And st_cfactor Eq 0.0) Then Begin ;no correction
           factorj = 1.0
        Endif Else If(en_cfactor Gt 0.0 And st_cfactor Gt 0.0) Then Begin
           factorj = interpol([st_cfactor, en_cfactor], $
                              [tim_arr[st_pt[j]], tim_arr[en_pt[j]]], $
                              tim_arr)
        Endif Else If(st_cfactor Gt 0.0 And en_cfactor Eq 0.0) Then Begin
           factorj = st_cfactor
        Endif Else Begin
           factorj = en_cfactor
        Endelse
;Sanity check, the factor must be gt1 for a rate, lt1 for a ratio
        If(gt1_or_lt1) Then factorj = factorj > 1.0 $
        Else factorj = factorj < 1.0
;Only correct and interpolate if factorj is not equal to 1
        If(min(factorj) Ne 1 Or max(factorj) Ne 1) Then Begin
           ss_correct = subs[st_pt[j]:en_pt[j]]
           rate[ss_correct] = rate[ss_correct]*factorj[ss_correct]
           ipts = [st_arr0, st_pt[j], st_pt[j]+1, st_arr1, $
                   en_arr0, en_pt[j], en_pt[j]+1, en_arr1]
;           ipts = [st_pt[j], st_pt[j]+1, $
;                   en_pt[j], en_pt[j]+1]
           ipts = (ipts > 0) < (nrate-1)
           If(c Eq 0) Then interp_pts = ipts $
           Else interp_pts = [interp_pts, ipts]
           c = c+1
        Endif
     Endif                      ;only one point, do nothing
  Endfor
;interpolate over all st_j, en_j points, which had good corrections
  If(c Gt 0) Then Begin
    rest = where_arr(subs, interp_pts, /notequal)
    oky = where(rate[rest] Gt 0, noky)
    If(noky Gt 0) Then Begin
      interp_pts = interp_pts[bsort(interp_pts)]
      interp_pts = interp_pts[uniq(interp_pts)]
      rate[interp_pts] = interpol(rate[rest[oky]], tim_arr[rest[oky]], $
                                       tim_arr[interp_pts])
      rate1 = smooth(rate, 5)
      rate[interp_pts] = rate1[interp_pts]
    Endif Else message, /info, 'No non-zero points to interpolate with'
;stop
  Endif
  rate_out = rate0 & rate_out[*] = 0.0
  rate_out[okx] = rate
  Return, rate_out
END

