#!/bin/csh -f
# Sets up symbolic links in ops/tables/defaults
#
# $Log: linkgen.sh,v $
# Revision 1.5  2007/02/16 20:47:32  nathan
# add testing directory
#
# Revision 1.4  2006/10/12 21:39:04  nathan
# utilize defaultsource directory
#
# Revision 1.3  2006/08/30 20:08:47  nathan
# add iandt/tables/default link
#
# Revision 1.2  2006/08/03 17:52:54  nathan
# add gtcf*.img (GT coeff tables, lo gain only)
#
# Revision 1.1  2006/07/17 20:38:23  nathan
# use linkgen.sh to generate symbolic links
#
# Log: secchisetup.scr,v 
# Revision 1.28  2006/05/16 15:33:51  nathan
# implement ops/tables/default
#

ln -s ../../../defaultsource/is/src/expostba.img 		
ln -s ../../../defaultsource/is/src/expostbb.img 		
ln -s ../../../defaultsource/is/src/imagetbl.img 		
ln -s imagetbl.img imagetba.img
ln -s imagetbl.img imagetbb.img
ln -s ../../../defaultsource/is/src/setuptba.img 		
ln -s ../../../defaultsource/is/src/setuptbb.img 		
ln -s ../../../defaultsource/ip/src/occulta.img 		
ln -s ../../../defaultsource/ip/src/occultb.img 		
ln -s ../../../defaultsource/ip/src/thresha.img 		
ln -s ../../../defaultsource/ip/src/threshb.img 		
ln -s ../../../defaultsource/ip/src/wavelet.img 		
ln -s ../../../defaultsource/ip/inc/ipcodes.h 		
ln -s ../../../defaultsource/scipctrl/src/motortba.img 	
ln -s ../../../defaultsource/scipctrl/src/motortbb.img 	
ln -s ../../../defaultsource/swire/src/rotbtb1a.img 		
ln -s ../../../defaultsource/swire/src/rotbtb1b.img 		
ln -s ../../../defaultsource/swire/src/wavetb1a.img 		
ln -s ../../../defaultsource/swire/src/wavetb1b.img 		
ln -s ../../../defaultsource/gt/src/gtcfplda.img
ln -s ../../../defaultsource/gt/src/gtcfrlda.img
ln -s ../../../defaultsource/gt/src/gtcfpldb.img
ln -s ../../../defaultsource/gt/src/gtcfrldb.img


cd ../current
ln -s ../../../source/is/src/expostba.img 		
ln -s ../../../source/is/src/expostbb.img 		
ln -s ../../../source/is/src/imagetbl.img 		
ln -s imagetbl.img imagetba.img
ln -s imagetbl.img imagetbb.img
ln -s ../../../source/is/src/setuptba.img 		
ln -s ../../../source/is/src/setuptbb.img 		
ln -s ../../../source/ip/src/occulta.img 		
ln -s ../../../source/ip/src/occultb.img 		
ln -s ../../../source/ip/src/thresha.img 		
ln -s ../../../source/ip/src/threshb.img 		
ln -s ../../../source/ip/src/wavelet.img 		
ln -s ../../../source/ip/inc/ipcodes.h 		
ln -s ../../../source/scipctrl/src/motortba.img 	
ln -s ../../../source/scipctrl/src/motortbb.img 	
ln -s ../../../source/swire/src/rotbtb1a.img 		
ln -s ../../../source/swire/src/rotbtb1b.img 		
ln -s ../../../source/swire/src/wavetb1a.img 		
ln -s ../../../source/swire/src/wavetb1b.img 		
ln -s ../../../source/gt/src/gtcfplda.img
ln -s ../../../source/gt/src/gtcfrlda.img
ln -s ../../../source/gt/src/gtcfpldb.img
ln -s ../../../source/gt/src/gtcfrldb.img

cd ../testing
ln -s ../../../sourcedev/is/src/expostba.img 		
ln -s ../../../sourcedev/is/src/expostbb.img 		
ln -s ../../../sourcedev/is/src/imagetbl.img 		
ln -s ../../../sourcedev/is/src/setuptba.img 		
ln -s ../../../sourcedev/is/src/setuptbb.img 		
ln -s ../../../sourcedev/ip/src/occulta.img 		
ln -s ../../../sourcedev/ip/src/occultb.img 		
ln -s ../../../sourcedev/ip/src/thresha.img 		
ln -s ../../../sourcedev/ip/src/threshb.img 		
ln -s ../../../sourcedev/ip/src/wavelet.img 		
ln -s ../../../sourcedev/ip/inc/ipcodes.h 		
ln -s ../../../sourcedev/scipctrl/src/motortba.img 	
ln -s ../../../sourcedev/scipctrl/src/motortbb.img 	
ln -s ../../../sourcedev/swire/src/rotbtb1a.img 		
ln -s ../../../sourcedev/swire/src/rotbtb1b.img 		
ln -s ../../../sourcedev/swire/src/wavetb1a.img 		
ln -s ../../../sourcedev/swire/src/wavetb1b.img 		
ln -s ../../../sourcedev/gt/src/gtcfplda.img
ln -s ../../../sourcedev/gt/src/gtcfrlda.img
ln -s ../../../sourcedev/gt/src/gtcfpldb.img
ln -s ../../../sourcedev/gt/src/gtcfrldb.img
