;+
;$Id: blk_seq_os.pro,v 1.1.1.2 2004/07/01 21:18:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : BLK_SEQ_OS
;               
; Purpose     : Checks to see if any of the requested OS(es) belong to a Block Sequence.
;               If so, it returns the os_num of first such OS so that the calling program
;               does not delete the requested OS(es).
;               
; Use         : stat= BLK_SEQ_OS(os_arr,sched_cmdseq,os_num,start_time,stop_time) 
;    
; Inputs      : os_arr        All scheudled observations. 
;               sched_cmdseq  Block sequence currently used in the schedule.
;               os_num        Observation IDs to be checked.
;               start_time    time to start the check.
;               stop_time     time to end the ckeck. 
;
; Opt. Inputs : None
;               
; Outputs     : stat          Either an os_num (to keep) or 0 (to delete). 
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: blk_seq_os.pro,v $
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION BLK_SEQ_OS, os_arr, sched_cmdseq, os_num, start_time, stop_time

  IF (DATATYPE(sched_cmdseq) NE 'STC') THEN RETURN,0

  blk_seq_os= 0L
  blk_seq_os_start_time= 0D

  n= N_ELEMENTS(sched_cmdseq)
  FOR i= 0, n-1 DO BEGIN
    blk_seq_os=[blk_seq_os , LONG(STR_SEP(sched_cmdseq(i).os_num,','))]
    blk_seq_os_start_time=[blk_seq_os_start_time , UTC2TAI(STR2UTC(STR_SEP(sched_cmdseq(i).os_time,',')))]
  ENDFOR
  blk_seq_os= blk_seq_os[1:*]
  blk_seq_os_start_time= blk_seq_os_start_time[1:*]

  FOR i= 0, N_ELEMENTS(os_num)-1 DO BEGIN
    delete_os = WHERE(os_arr.os_num EQ os_num(i) AND os_arr.os_start GE start_time AND $
                      os_arr.os_stop LE stop_time,dcnt)
    FOR j= 0, dcnt-1 DO BEGIN
      os_match= WHERE(blk_seq_os EQ os_arr(delete_os(j)).os_num, ocnt)
      IF (ocnt GT 0) THEN BEGIN
        time_match= WHERE(blk_seq_os_start_time(os_match) EQ os_arr(delete_os(j)).os_start, tcnt)  
        IF(tcnt GT 0) THEN RETURN, os_num(i) ; Atleast one of the OSes to be deleted is in block seq.
      ENDIF
    ENDFOR
  ENDFOR 

  RETURN, 0 
END
