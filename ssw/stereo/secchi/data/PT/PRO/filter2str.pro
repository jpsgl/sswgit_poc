;+
;$Id: filter2str.pro,v 1.5 2009/09/11 20:28:09 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : FILTER2STR
;               
; Purpose     : Convert a filter number to its actual text representaion.
;               
; Use         : filter= FILTER2STR(fw_types,table, fw, tele) 
;    
; Inputs      : fw_types  String array of filter names. 
;               table     exposure table number
;               fw        filter wheel number selected
;               tele      telescope number
;
; Opt. Inputs : None
;               
; Outputs     : filter    Text string for the selected filter wheel. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; Ed Esfandiari 04/03/06 - Remove filter info in "()".
;
;
; $Log: filter2str.pro,v $
; Revision 1.5  2009/09/11 20:28:09  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;
;-


FUNCTION FILTER2STR,fw_types,table,fw,tele

   filter= 'None'
   IF (tele EQ 0) THEN filter=  fw_types(table,fw,tele)
   IF (STRPOS(filter,'(') GT 0) THEN filter= STRTRIM(STRMID(filter,0,STRPOS(filter,'(')),2)

   RETURN, filter 

END

