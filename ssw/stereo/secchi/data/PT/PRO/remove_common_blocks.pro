;+
;$Id: remove_common_blocks.pro,v 1.9 2009/09/11 20:28:21 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : REMOVE_COMMON_BLOCKS
;               
; Purpose     : This batch file deletes all of the blocks variables used in the
;               planning/scheduling tool. It should be called between calls to 
;               the schedule or define_os so that data from previous runs do not
;               end up in the next run's results. (The only other way to accomplish 
;               this is to terminate the current IDL session and start a new one).
;               To execute this file, issuse the following command from the IDL
;               prompt: @remove_common_blocks
;
;               
; Use         : @remove_common_blocks
;    
; Inputs      : None
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/27/04 - Added OS_ALL_AB common block.
;              Ed Esfandiari 11/04/04 - Added "Full" and "Other" display options.
;              Ed Esfandiari 11/05/05 - Added I and A commands (ic and rsc).
;              Ed Esfandiari 06/20/07 - Added TM_DATA
;
; $Log: remove_common_blocks.pro,v $
; Revision 1.9  2009/09/11 20:28:21  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.4  2005/03/10 16:50:13  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


retall
COMMON TM_DATA, sb_usage, sb_total_bits_avail, s1_usage, s1_total_bits_avail, $
                s2_usage, s2_total_bits_avail, rt_usage, rt_total_bits_avail, rt_usage_avail, $
                sw_usage, sw_total_bits_avail, sbuf_use, ssr1_use, ssr2_use, rt_use, sw_use
 delvar, sb_usage, sb_total_bits_avail, s1_usage, s1_total_bits_avail, $
                s2_usage, s2_total_bits_avail, rt_usage, rt_total_bits_avail, rt_usage_avail, $
                sw_usage, sw_total_bits_avail, sbuf_use, ssr1_use, ssr2_use, rt_use, sw_use
COMMON FULL_OS, tosarr
 delvar, tosarr
COMMON SET_DEFINED, set_instance, defined_set_arr ; AEE 2/6/04
 delvar, set_instance, defined_set_arr
COMMON SETS, select_set ; AEE 2/6/04
 delvar, select_set
COMMON APIDS, multi_apid ; AEE 2/13/04
 delvar, multi_apid
COMMON EXPANDED_OS,  expanded ; AEE 1/15/04
 delvar, expanded
COMMON HDR_ONLY_REMOVED, no_ho_expanded  ; AEE 5/11/04
 delvar, no_ho_expanded
COMMON SCHED_SHARE, schedv
 delvar, schedv
COMMON SCHED_CONF, all_conflicts
 delvar, all_conflicts
COMMON SHIFT_OS_SHARE, return_dte, beg_dte
 delvar, return_dte, beg_dte
COMMON APIDS, multi_apid
 delvar, multi_apid
COMMON LP_GT_DUMPS_SHARE, gtdump
 delvar, gtdump
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt
 delvar, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt
COMMON PREV_GT_DUMPS_SHARE, pstime_gt, petime_gt, papid_gt, psc_gt, prate_gt, pbsf_gt
 delvar, pstime_gt, petime_gt, papid_gt, psc_gt, prate_gt, pbsf_gt
COMMON LP_SCIP_DOORS_SHARE, lpdoor
 delvar, lpdoor
COMMON SCIP_DOORS_SHARE, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
                         sc_maneuvers, start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2
 delvar, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
         sc_maneuvers, start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2
COMMON PREV_SCIP_DOORS_SHARE, pdoors, pcdoor_euvi, podoor_euvi, pcdoor_cor1, podoor_cor1, pcdoor_cor2,podoor_cor2
 delvar, pdoors, pcdoor_euvi, podoor_euvi, pcdoor_cor1, podoor_cor1, pcdoor_cor2,podoor_cor2
COMMON BLOCK_TABLE_SHARE, beg_ind, end_ind, blk_start_time,scheduled
 delvar, beg_ind, end_ind, blk_start_time,scheduled
COMMON CMD_SEQ_SCHEDULED, sched_cmdseq
 delvar, sched_cmdseq
COMMON CMD_TABLE_ENTRY, first, cmd_table_entry
 delvar, first, cmd_table_entry
COMMON CMD_BITPACKED, bp_ind, bp_data
 delvar, bp_ind, bp_data
COMMON DIALOG, mdiag,font
 delvar, mdiag,font
COMMON OS_SHARE, osv
 delvar, osv
COMMON RUN_SCRIPT, rsc
 delvar, rsc
COMMON INIT_TL, ic
 delvar, ic
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
ip_arr, plan_lp, lprow, cor1pw, cor2pw
 delvar, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, ip_arr, plan_lp, lprow,cor1pw, cor2pw
COMMON OP_DEFINED, op_instance, defined_op_arr
 delvar, op_instance, defined_op_arr
COMMON OP_SCHEDULED, op_struct, op_arr
 delvar, op_struct, op_arr
COMMON OBS_PROG_DEF, op_id_struct, op_id_arr
 delvar, op_id_struct, op_id_arr
COMMON OS_DEFINED, defined_os_arr, os_instance
 delvar, defined_os_arr, os_instance
COMMON OS_SCHEDULED, os_struct, os_arr, tm_arr, load_camtable, load_wlexptable, do_candc, move_fp
 delvar, os_struct, os_arr, tm_arr, load_camtable, load_wlexptable, do_candc, move_fp
COMMON OS_ALL_AB, remain_os_arr, all_defined_os_arr, all_defined_set_arr, all_sched_cmdseq
 delvar,remain_os_arr, all_defined_os_arr, all_defined_set_arr, all_sched_cmdseq
COMMON SCHEDULE_BUFFER, save_buffer
 delvar, save_buffer
COMMON DBMODES_COMMON, dbmodes
 delvar, dbmodes
COMMON INPUT_WL_ORDER_COMMON, wl_s, order_s
 delvar, wl_s, order_s
COMMON MASK_SHARE, maskv
 delvar, maskv
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
 delvar, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON SCHEDULE_BASE, sched_base
 delvar, sched_base
COMMON LOCATION_SHARE, x, y, xx, yy, last_base
 delvar, x, y, xx, yy, last_base
COMMON LP_NORMAL_IMAGE_SHARE, lpnormv
 delvar, lpnormv
COMMON LP_DOUBLE_IMAGE_SHARE, lpdoubv
 delvar, lpdoubv
COMMON LP_DARK_IMAGE_SHARE, lpdarkv
 delvar, lpdarkv
COMMON LP_CAL_LAMP_SHARE, lpcalv
 delvar, lpcalv
COMMON LP_CONT_IMAGE_SHARE, lpcont
 delvar, lpcont
COMMON LP_BLOCK_SEQ_SHARE, beg_ind, end_ind, blk_start_time,scheduled
 delvar, beg_ind, end_ind, blk_start_time,scheduled
COMMON LP_HI_SEQ_SHARE, lpseqhi
 delvar, lpseqhi
COMMON LP_SEQ_PW_FW_SHARE, lpseqpwv
 delvar, lpseqpwv
COMMON LP_TAKE_SEQ_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_CONCURRENT_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_SUM_IMG_SHARE, lpsumv
 delvar, lpsumv
COMMON LP_MOVE_M1_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_M1_MEASURE_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_WOBBLE_IN_SHARE, lpwobinv
 delvar, lpwobinv
COMMON LP_WOBBLE_OUT_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_CHK_CORR_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_FP_SCAN_LINE_SHARE, lpscanv
 delvar, base, lpscanv
COMMON LP_FP_CAMCOORD_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_GRND_PERIPH_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_PERIPH_LOAD_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_UP_DFLT_PRM_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_GRND_MECH_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_TRANS_DET_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_DUMP_MEMORY_SHARE, base, dismiss
 delvar, base, dismiss
COMMON LP_STATUS_REQ_SHARE, base, dismiss
 delvar, base, dismiss
COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt
 delvar, a_bsf_cnt, b_bsf_cnt
COMMON LP_BSF_SHARE, lpcmdv
 delvar, lpcmdv
COMMON LP_CMD_TABLE_SHARE, lpcmdv
 delvar, lpcmdv
COMMON LP_NULL_SHARE, base, dismiss
 delvar, base, dismiss
COMMON OS_SUMMARY_SHARE, os_sumv
 delvar, os_sumv
COMMON OS_TABLE_CAMERA_SHARE, ccdv
 delvar, ccdv
COMMON OS_TABLE_PROCESSING_SHARE, ipv
 delvar, ipv
COMMON OS_TABLE_EXPOSURE_SHARE, exv
 delvar, exv
COMMON OS_TABLE_OCCULTER_SHARE, base, dismiss
 delvar, base, dismiss
COMMON OS_TABLE_MASK_SHARE, base, dismiss
 delvar, base, dismiss
COMMON OS_TABLE_BADCOL_SHARE, base, dismiss
 delvar, base, dismiss
COMMON OS_TABLE_TRANSIENT_SHARE, base, dismiss
 delvar, base, dismiss
COMMON OS_TABLE_FP_WL_EXP_SHARE, fpwlv
 delvar, fpwlv
COMMON PICKFILES2_COMMON, base, all_files, selected, files_base
 delvar, base, all_files, selected, files_base
COMMON PICKFILES2_COMMON, all_files, selected, files_base
delvar, all_files, selected, files_base
COMMON PLAN_OP_SHARE, opv
 delvar, opv
COMMON KAP_INIT, kap_nrt_reserved, kap_resource, kap_resource_names, kap_dsn_names, kap_descriptions, $
kap_tlm_mode_names, kap_iie
 delvar, kap_nrt_reserved, kap_resource, kap_resource_names, kap_dsn_names, kap_descriptions, $
         kap_tlm_mode_names, kap_iie
COMMON KAP_INPUT, kap_nrt_reserved_arr, kap_resource_arr, kap_iie_arr
 delvar, kap_nrt_reserved_arr, kap_resource_arr, kap_iie_arr
COMMON SCA_NAMES, sca_fname, scb_fname
 delvar, sca_fname, scb_fname
COMMON RWMASK_SHARE, rwmaskv
 delvar, RWMASK_SHARE, rwmaskv
COMMON OS_PLOT_SHARE, os_yarr
 delvar, os_yarr
COMMON SUBMODE_SHARE, return_value
 delvar, return_value
COMMON SHIFT_IPT_SHARE, return_value
 delvar, return_value
COMMON SHIFT_IPT1_SHARE, ret_val
 delvar, ret_val
;COMMON MANAGED, ids, names, nummanaged, inuseflag, backroutines, backids, backnumber, nbacks, validbacks, $
;blocksize,cleanups,outermodal
; delvar, ids, names, nummanaged, inuseflag, backroutines, backids, backnumber, nbacks, validbacks, $
;         blocksize,cleanups,outermodal
COMMON INPUTS,val,success
 delvar, val,success
COMMON INPUTS_NOSCAN,val,success
 delvar, val,success

;return
;end
