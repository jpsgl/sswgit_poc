;+
;$Id: read_exp_tables.pro,v 1.8 2009/09/11 20:28:19 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : READ_EXP_TABLES
;               
; Purpose     : Read FSW exposure tables.
;
; Explanation : The flight exposure table contains exposure time information
;               for each of 5 telescopes in Hex format (see comments in the file).
;               This routine reads in the data, coverts them to base10 and stores 
;               them in PT exposure structure, expt(5,7,4,24), for 5 cameras, 
;               7 tables, 4 fw, 24 pw.
;               The fw and pw of 4 and 20 represent the maximum possible (those not 
;               used are set to 0).
;               
; Use         : READ_EXP_TABLES, fn, expt, fw_pos, pw_pos 
;    
; Inputs      : fn:  In flight exposure tables filename (i.e. '../TABLES/exp_tables.dat')
;
; Opt. Inputs : None
;               
; Outputs     : expt   FLTARR(5,7,4,24) for 5 cameras; 7 tables; 4 fw; 24 pw.
;               fw_pos INTARR(7,4)      7 tables, 4 fw, 1 camera (EUVI).
;               pw_pos INTARR(7,24,3)   7 tables, 24 pw (4 for EUVI, 24 for COR1 & COR2), 
;                                       3 cameras (EUVI, COR1, COR2).
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 07/09/04 - Use 24 polar position for COR1 & COR2 instead of 20.
;              Ed Esfandiari 08/11/04 - Changed returned exp. times to physical milli-seconds
;                                       (from SCIP and HI units of 1.024 and 2.0 milli-seconds)  
;                                       and changed expt type to float.
;              Ed Esfandiari 09/16/04 - Corrected EUVI quad position comments.
;              Ed Esfandiari 03/31/06 - Changed comment for new euvi filter and quad pos comments.
;              Ed Esfandiari 12/04/07 - Changed EUVI filter/sector combination order to match new
;                                       order of exposure table values: 
;                                       195-Open, 195-S1, 195-Dbl, 195-S2, 284-Open, 284_S1, ....
;
; $Log: read_exp_tables.pro,v $
; Revision 1.8  2009/09/11 20:28:19  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2004/09/20 16:10:44  esfand
; Corrected EUVI quad position comments
;
; Revision 1.3  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.2  2004/07/12 13:10:59  esfand
; now using 24 COR1 and COR2 polar positions
;
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO READ_EXP_TABLES,fn, expt, fw_pos, pw_pos
  
  expt= FLTARR(5,7,4,24)   ;  5 cameras; 7 tables; 4 fw; 24 pw; - to match exd in os_init.pro
  fw_pos= INTARR(7,4*1)    ;  7 tables, 4 fw, 1 camera (EUVI) 
  pw_pos= INTARR(7,24,3)   ;  7 tables, 24 pw (4 for EUVI, 24 for COR1 & 2), 3 cameras (EUVI, COR1, COR2)

  OPENR,lun, fn, /GET_LUN 
  line= ''
  table= 0
  cnt= 1 
  WHILE NOT EOF(lun) DO BEGIN
    READF, lun, line
    line= STRTRIM(line,2)
    IF (STRMID(line,0,1) EQ 'X') THEN BEGIN
      ;print,line
      line= STRCOMPRESS(STRMID(line,1,500),/REMOVE_ALL) ; remove X and all blanks.

      CASE 1 OF
        cnt EQ 1 : BEGIN ; start of EUVI section (FW and QS positions)
             ; Line 1 is FW POS (max of 4 values) and QS POS (max of 4 values).
             ;  Note: these are not used in PT; PT assumes exptimes are always in the following order:
             ;        FW: Clear, Al+1a, Al+1b, Al+2
             ;        QS: 304, 195, 284, 171 
             ;        March 31, 2006 - use new order from exptables:
             ;        FW: Al+2 (OPEN), Clear (S1), Al+1a (DBL), Al+1b (S2)
             ;        QS: 195A, 284A, 175A, 304A

             fwqs= INTARR(8) 
             READS, line, fwqs, format='(8z4)'
             ; Same FW and SQ are used for all 7 tables:
             FOR table=0, 6 DO BEGIN
               fw_pos(table,0:3)= fwqs[0:3]
               pw_pos(table,0:3,0)= fwqs[4:7]
             ENDFOR
             cnt= cnt+1
             table= 0
           END
        cnt GE 2 AND cnt LE 8 : BEGIN ; EUVI exptimes
             ; Lines 2 to 8 are exptimes for EUVI 7 tables (16 exptimes per lines for filter,quad combinations:
             ex= LONARR(16)
             READS, line, ex, format='(16z4)'
             ;expt(0,table,0,0:3) = ex[0:3]   ; EUVI exptimes for (Open,S1, Dbl, S2), 195 
             ;expt(0,table,1,0:3) = ex[4:7]   ; EUVI exptimes for (Open,S1, Dbl, S2), 284 
             ;expt(0,table,2,0:3) = ex[8:11]  ; EUVI exptimes for (Open,S1, Dbl, S2), 171 
             ;expt(0,table,3,0:3) = ex[12:15] ; EUVI exptimes for (Open,S1, Dbl, S2), 304 
             expt(0,table,0:3,0)= ex[0:3]     ; EUVI exptimes for 195-Open, 195-S1, 195-Dbl, 195-S2
             expt(0,table,0:3,1)= ex[4:7]     ; EUVI exptimes for 284-Open, 284-S1, 284-Dbl, 284-S2
             expt(0,table,0:3,2)= ex[8:11]    ; EUVI exptimes for 171-Open, 171-S1, 171-Dbl, 171-S2
             expt(0,table,0:3,3)= ex[12:15]   ; EUVI exptimes for 304-Open, 304-S1, 304-Dbl, 304-S2
              
             cnt= cnt+1
             table= table+1
           END
        cnt EQ 9 : BEGIN ;  start of COR1 section
             ; Line 9 contains 24 PW positions. Same PW positions are used for all 7 tables:
             pw= INTARR(24)
             READS, line, pw, format='(24z4)'
             FOR table=0, 6 DO BEGIN
               pw_pos(table,0:23,1)= pw 
             ENDFOR
             cnt= cnt+1
             table= 0
           END
        cnt GE 10 AND cnt LE 16 : BEGIN ;  COR1 exptimes 
             ; Lines 10 to 16 are exptimes for COR1 7 tables (24 exptimes per line):
             ex= LONARR(24)
             pw_pos(table,0:23,1)= pw 
             READS, line, ex, format='(24z4)'
             expt(1,table,0,0:23)= ex 
             cnt= cnt+1
             table= table+1
           END
        cnt EQ 17 : BEGIN ;  start of COR2 section
             ; Line 17 contains 24 PW positions. Same PW positions are used for all 7 tables:
             pw= INTARR(24)
             READS, line, pw, format='(24z4)'
             FOR table=0, 6 DO BEGIN
               pw_pos(table,0:23,2)= pw 
             ENDFOR
             cnt= cnt+1
             table= 0
           END
        cnt GE 18 AND cnt LE 24 : BEGIN ;  COR2 exptimes 
             ; Lines 18 to 24 are exptimes for COR2 7 tables (24 exptimes per line):
             ex= LONARR(24)
             pw_pos(table,0:23,1)= pw
             READS, line, ex, format='(24z4)'
             expt(2,table,0,0:23)= ex
             cnt= cnt+1
             table= table+1
           END
        cnt GE 25 AND cnt LE 31 : BEGIN ;  HI1 and Hi2 section
             ; Only 7 lines of exptimes with 2 exptimes per line. First exptime is for HI1 and second for HI2.
             IF cnt EQ 25 THEN table= 0
             ex= LONARR(2)
             READS, line, ex, format='(2z4)' 
             expt(3,table,0,0)= ex(0) * 2.0  ; Note: HI exp.times inputs are in units of 2ms. Change them
             expt(4,table,0,0)= ex(1) * 2.0  ;       to physical ms.
             cnt= cnt+1
             table= table+1
           END

       cnt EQ 32: BEGIN ; too many valid lines. Generate error
             PRINT,'STOP - ERROR: Expoure table '+fn+' contains extra input.'
             STOP
           END 
      ENDCASE
      ;print, cnt
    ENDIF
  END

  expt(0:2,*,*,*)= expt(0:2,*,*,*) * 1.024 ; SCIP exptime units of 1.024ms to physical ms.

  CLOSE,lun
  FREE_LUN,lun

  RETURN
END
