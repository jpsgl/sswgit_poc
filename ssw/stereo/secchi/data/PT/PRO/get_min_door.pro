;+
;$Id: get_min_door.pro,v 1.1.1.2 2004/07/01 21:19:02 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GET_MIN_DOOR
;               
; Purpose     : Return earliest valid dates of all  SCIP telescopes existing close and open doors.
;               
; Use         : dtime= GET_MIN_DOOR(cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, 
;                                   cdoor_cor2, odoor_cor2)
;    
; Inputs      : cdoor_euvi   Scheduled EUVI door closures
;               odoor_euvi   Scheduled EUVI door opens
;               cdoor_cor1   Scheduled COR1 door closures 
;               odoor_cor1   Scheduled COR1 door opens
;               cdoor_cor2   Scheduled COR2 door closures
;               odoor_cor2   Scheduled COR2 door opens
;
; Opt. Inputs : None
;               
; Outputs     : dtime        The earliest scheduled SCIP door closures and opens. 
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: get_min_door.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

FUNCTION GET_MIN_DOOR, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2 

   min_dt= 0.0D

   ; close doors (may have '2000/01/01' dates):

   cdt = UTC2TAI(STR2UTC('2000/01/01'))
   IF (DATATYPE(cdoor_euvi) EQ 'DOU') THEN BEGIN
     ok= WHERE(cdoor_euvi GT cdt, cnt)
     IF (cnt GT 0) THEN min_dt= [min_dt, MIN(cdoor_euvi(ok))]
   ENDIF
   IF (DATATYPE(cdoor_cor1) EQ 'DOU') THEN BEGIN
     ok= WHERE(cdoor_cor1 GT cdt, cnt)
     IF (cnt GT 0) THEN min_dt= [min_dt, MIN(cdoor_cor1(ok))]
   ENDIF
   IF (DATATYPE(cdoor_cor2) EQ 'DOU') THEN BEGIN
     ok= WHERE(cdoor_cor2 GT cdt, cnt)
     IF (cnt GT 0) THEN min_dt= [min_dt, MIN(cdoor_cor2(ok))]
   ENDIF

   ; open doors (can't have '2000/01/01' dates):

   IF (DATATYPE(odoor_euvi) EQ 'DOU') THEN min_dt= [min_dt, MIN(odoor_euvi)] 
   IF (DATATYPE(odoor_cor1) EQ 'DOU') THEN min_dt= [min_dt, MIN(odoor_cor1)]
   IF (DATATYPE(odoor_cor2) EQ 'DOU') THEN min_dt= [min_dt, MIN(odoor_cor2)]

   IF (N_ELEMENTS(min_dt) GT 1) THEN  min_dt= min_dt(1:*)

   RETURN, MIN(min_dt)

END

