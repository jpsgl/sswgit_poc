;+
;$Id: plot_ssr1_channel.pro,v 1.9 2009/09/11 20:28:18 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : PLOT_SSR1_CHANNEL
;
; Purpose     : Plots one-second snapshot of SSR1 channel for the displayed schedule.
;
; Use         : PLOT_SSR1_CHANNEL, schedv, expanded, prev_hr, rate, truesize, size, tdump,
;                                  ybot, ytop, ylimit, xarr, secs, startdis, enddis,
;                                  pct_full, sb_rate, full_dumps, gt_data
;
; Inputs      : schedv      Structure containing widgets information.
;               expanded    Structure containing info for each image destained for SSR1-Channel.
;               prev_hr     Hours prior to the start of schedule (one) to calculate statistics.
;               rate        SSR1 channel telemetry rate.
;               truesize    True capacity of SSR1.
;               size        Downloadable size of SSR1 channel (depends on mission high, med, low dates).
;               tdump       Schedule of telemetry Playbacks (from .SCT file) for current display.
;               ybot, ytop  Scale to plot window, ytop-ybot is the SSR1-window range.
;               ylimit      Scale to plot window, ylimit-ybot is the max. SSR1-window range.
;               xarr        Plot-range in seconds (including the prev_hr).
;               secs        Number of seconds in the plot-range (including the prev_hr).
;               startdis    Start time of the schedule.
;               enddis      End time of the schedule.
;               pct_full    Percentage of SSR1-capacity in use at the start of the schedule.
;               sb_rate     Secchi-Buffer (RAM-disk) rate.
;               full_dumps  Full schedule of telemetry Playbacks (from .SCT file). 
;               gt_data     Structure containg GT data-dumps info.
;
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 10/25/04 - Re-arranged Tlm load section.
;              Ed Esfandiari 10/25/04 - Use adjusted rate depending on playback length.
;              Ed Esfandiari 06/23/06 - use exact s1 and s2.
;              Ed Esfandiari 08/22/06 - new capacity and rate usage.
;              Ed Esfandiari 09/02/06 - Added nominal 3.26 kbps HK rate to SSR1 and RT.
;              Ed Esfandiari 09/06/06 - Added GT dumps.
;              Ed Esfandiari 06/20/07 - Added toggle option to display data volume/downloadable ratio or volume in MB.
;              Ed Esfandiari 01/22/09 - Changed HK rate from maximum 3.26 to reduced 2.00 pbks (~21 MB/day) - NR suggested.
;              Ed Esfandiari 09/02/09 - changed data volume display from .1 to .3 decimal places.
;
; $Log: plot_ssr1_channel.pro,v $
; Revision 1.9  2009/09/11 20:28:18  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO PLOT_SSR1_CHANNEL, schedv, expanded, prev_hr, rate, truesize, size, tdump, ybot, ytop, ylimit, $
                   xarr, secs, startdis, enddis, pct_full, sb_rate, full_dumps, gt_data

COMMON TM_DATA

        truesize= truesize*8000000.0    ; convert true physical SSR1 size from MB to bits

        bufsize= size*8000000.0           ; convert from MB to bits

        ;arry= FLTARR(5,secs)           ;** only contains os_size at os_stop for every scheduled image
        arry= FLTARR(7,secs)           ;** to keep track of HK and GT data. 

        ihk= 5
        igt= 6

    ; ADD GT-dumps data, if any:
    FOR g= 1, N_ELEMENTS(gt_data)-1 DO BEGIN ; first element is always empty
      IF (STRPOS(gt_data(g).apid,'SSR1') GE 0) THEN BEGIN
        g1= (gt_data(g).x1- (!X.CRANGE(0)-prev_hr))*3600D
        g2= (gt_data(g).x2- (!X.CRANGE(0)-prev_hr))*3600D
        arry(igt,g1:g2)= gt_data(g).rate
      ENDIF
    ENDFOR

        ;arry(ihk,*) = 3.26*1000 ; nominal HK data rate to SSR1 (and also RT)
        arry(ihk,*) = 2.00*1000 ; reduced HK data rate to SSR1 (and also RT)

        IF (DATATYPE(expanded) EQ 'STC') THEN BEGIN
         FOR i = 0, N_ELEMENTS(expanded)-1 DO BEGIN ; AEE - 01/16/03

           tstop = expanded(i).stop
 
           tsecs = 1 ; AEE - Dec 20, 02

           ; Note:
           ; X.crange(0) contains start time of the plot and X.crange(1) contains the endtime in hours.
           ; So, for a 5min plot range X.crange(1)-X.crange(0) = 0.0833333 * 3600 = 300 seconds.
           ; Also, 24 hrs (86400 seconds) is added to the start of the arry (secs = 86400+300) so
           ; the first image in the displayed schedule starts at the 86400+xxx seconds where xxx is
           ; 0 or more seconds.

           ; Note: expanded.stop for the images that are beyond the dispay end time (crange(1)), are set
           ;       to crange(1) so do not include those images:

           ; use all expanded images within the prev_hr before the start of plot display to the end of the
           ; plot display:

           IF ((tstop GE (!X.CRANGE(0)-prev_hr)) AND (tstop LT !X.CRANGE(1))) THEN BEGIN 
              tstopind = (tstop - (!X.CRANGE(0)-prev_hr))

              tele = expanded(i).tele

              ; AEE 3/3/04 - use the secchi_buffer rate (sb_rate) and the image size to spread the
              ; image being placed on SSR1:

              ;s1 = tstopind*3600D - expanded(i).proc_time
              ;s2= s1 + expanded(i).size / sb_rate
              s1= LONG(tstopind*3600D - expanded(i).proc_time)
              s2= s1 + LONG(expanded(i).size / sb_rate)
              tsize = expanded(i).size / DOUBLE(s2-s1+1)
            
              ; Note: tsize should be calculated before s1 and/or s2 or possibly changed since
              ;       we want to keep the true one second value for it.

              s1= s1 > 0
              s2= s2 < LONG(secs-1) 

              IF (s2 LT s1) THEN s2= s1

              IF (s2 LT N_ELEMENTS(arry(tele,*))) THEN BEGIN
                arry(tele,s1:s2) = arry(tele,s1:s2) + tsize
                ;** Assume 1st os starts with buffer at a certain percentage:
                ;IF (i EQ 0) THEN arry(tele,s1) = arry(tele,s1) + bufsize*(FLOAT(pct_full)/100)
                IF (i EQ 0) THEN arry(tele,s1) = arry(tele,s1) + truesize*(FLOAT(pct_full)/100)
              ENDIF ELSE BEGIN
                PRINT,'%SCHEDULE_PLOT: telemetry buffer index where image is to be placed is out of current plot-range.'
              ENDELSE

           ENDIF
        ENDFOR
       ENDIF

;******

        ; AEE 03/17/04 - At this point, arry contains the distributed data for each second NOT the
        ;                accumulated data.


        buffer = FLTARR(secs)           ;** contains amt of data in buffer 1 entry every second
        tarry = TOTAL(arry,1)  ; totats values of all cameras (0 to 4) for each entry (each second) and collapses
                               ; arry(5,secs) to tarry(secs)
        buffer(*,0) = tarry(0) ; copy first value from tarry to all of the buffer

        ; AEE - March 5, 04:
        ; Since SSR1 is only downlinked during certain contact times, create an array of downlinks for
        ; each second (set to zero) and only set the valid downlink periods to the actual downlink rate:

        dlarry= fltarr(secs)

        ; AEE 3/16/04:
        ; use full dumps to account for playbacks scheduled before the start of plot-display, if any:
        ; first full_dumps(0) is extra and should be ignored:

        ; first element of full_dumps is dummy and should be ignored.

        end_pb= -9L ; in case there is no playback within the schedule

        IF (N_ELEMENTS(full_dumps) GT 1) THEN $ 
          end_pb= LONARR(N_ELEMENTS(full_dumps)-1)

        FOR k = 1, N_ELEMENTS(full_dumps) - 1 DO BEGIN  ; account for downlinks in the tape-dump period(s):
          x1 = LONG(full_dumps(k).x1) > 0
          ;x2 = LONG(full_dumps(k).x2) < (secs-1)
          x2 = (LONG(full_dumps(k).x2) < (secs-1)) > 0
          ;dlarry(x1:x2)= rate(0)
          dlarry(x1:x2)= rate(k)
          end_pb(k-1)= x2  ; keep ends of each playback
        ENDFOR

        ; AEE - March 5, 04:
        ; Now build-up (accumulate) the data and adjust for downlinks for SSR array:

        red= BYTARR(secs) 
        accum= buffer(0)

        FOR i = 1L, N_ELEMENTS(tarry)-1 DO BEGIN        ;** for every second in plot range
           ;** amt in buffer the previous second + amt this second - amt downlinked this second
           ;
           ; 3/18/04 -  Since write-limit pointer is set to read-pointer only once at the end of each 
           ;            playback, if SSR becomes full, the overflow data is lost (even during the playback
           ;            period which more space may become available). So, from start of day (or end
           ;            of previous playback) to the end of the next playback, make sure overflow, if
           ;            any, is marked as red before downlink amounts are taken out.
           ;            Note: Any data remained in SSR (not fully played-back) will be saved for the
           ;                  next playback (so no special handling is required).

           tmp= WHERE(i EQ end_pb+1L, cnt)      ; Mark end of each playback as not red (at this point 
           IF (cnt GT 0) THEN BEGIN
             accum= buffer(i-1)
           ENDIF

           accum= accum + tarry(i)

           ; For overflow data, mark this second as red only if data is added this second:

           ;IF (accum GE bufsize AND tarry(i) GT 0) THEN red(i)= 1B 
           ;IF (accum GE truesize AND tarry(i) GT 0) THEN red(i)= 1B ; this does not account for downlinks

           buffer(i)= buffer(i-1)

           ; Buildup (accumulate) buffer only if not already full:

           ;IF (accum LT bufsize) THEN $
           ;  buffer(i)= (buffer(i) + tarry(i)) < bufsize 
           ;IF (accum LT truesize) THEN $
           ;IF (buffer(i) LT truesize) THEN $
           ;  buffer(i)= (buffer(i) + tarry(i)) < truesize 

           buffer(i)= (buffer(i) + tarry(i)) < truesize

           ; Account for downlinks, if any:

           buffer(i)= (buffer(i) - dlarry(i)) > 0

           IF (buffer(i) GE truesize) THEN red(i)= 1B ; this accounts for downlinks

        ENDFOR

        ; 3/18/04 - Now buffer has the correct data and red array where values are one corresponds
        ;           to the overflow data being lost and will be plotted as red. To correct the schedule
        ;           for overflows, the plotted RED line(s), if any, have to be removed.


        ; AEE 3/9/04 - only keep the data within the current displayed plot-range:
        ; Note: xarr, as it comes in, contains the plot_range in the display appended to the previous
        ;       24 hrs. It has 24*3600 + display-range in seconds + 1 elements. Element values are
        ;       in hrs and the first 2 values, xarr(0:1), are -24.0, -23.999722 and gets down to 0.0 and
        ;       (start of the plot display-range) and then up to the end of the plot display-range.

        plot_range= WHERE(xarr GE !x.crange(0) AND xarr LT !x.crange(1))
        buffer= buffer(plot_range)
        arrx= xarr(plot_range)
        td= dlarry(plot_range)
        red= red(plot_range)

        save_buffer = buffer
        ;buffer = buffer / bufsize       ;** normalize to 1.0 = 100% full (buffer range = 0.0-1.0)
        buffer = buffer / truesize       ;** normalize to 1.0 = 100% full (buffer range = 0.0-1.0)
        buffer = buffer * (ytop-ybot) ;** scale to plot window, ytop-ybot is the window range-AEE 6/17/03
        buffer = (buffer < (ylimit-ybot))  ;** ylimit-ybot is the max. window range ; AEE 6/17/03
         
        ; Flag places where the playback is scheduled with green and polyfill data with white:

        nelem= N_ELEMENTS(arrx)
        pxval = [arrx(0), arrx, arrx(nelem-1)]

        POLYFILL, pxval, [ybot,ybot+buffer-0.2, ybot] ; AEE - 3/17/04

        FOR i = 0, N_ELEMENTS(tdump)-1 DO BEGIN
          x1= ((tdump(i).x1 - prev_hr*3600.0) -1) > 0
          x2= ((tdump(i).x2 - prev_hr*3600.0) -1) < (nelem-1)
          dur= x2 - x1 +1
          OPLOT, arrx(x1:x2), (ybot + FLTARR(dur)) , PSYM=3, COLOR=2 ; use Green. AEE - 3/16/04
        ENDFOR


        ; draw brown (5) line to bufsize limit for entire plot range:
        OPLOT, arrx, FINDGEN(N_ELEMENTS(arrx))*0.0+ $
          ;(ytop-ybot)*(bufsize/truesize)+ybot-0.2, PSYM=3, COLOR=5
          (ytop-ybot)*(bufsize/truesize)+ybot, PSYM=3, COLOR=5

        ; Display lost data (over the limit) after downlinks, if any, in red:

        r= WHERE(red EQ 1, rcnt)
        IF (rcnt GT 0) THEN OPLOT, arrx(r), FINDGEN(rcnt)*0.0+ytop-0.2, PSYM=3, COLOR=1
        ;IF (rcnt GT 0) THEN OPLOT, arrx(r), FINDGEN(rcnt)*0.0+ $
        ;  (ytop-ybot)*(bufsize/truesize)+ybot-0.2, PSYM=3, COLOR=1

        ;** Update buffer statistics in schedule widget
        ;**
        date_str = UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' + $
                   UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
        ;WIDGET_CONTROL, schedv.usage_dates, SET_VALUE="Data/Telemetry Loads For:  "+date_str ; AEE 10/10/03

        WIDGET_CONTROL, schedv.usage_dates, SET_VALUE= date_str

        ;AEE - 3/9/04:
        ;total_bits_avail is total number of telemetry bits that can be downloaded during SSR1 playback
        ;contacts within the displayed plot-range. So, if in a 24-hr plot-range there is a one-hour
        ;SSR1 playback, number of bits that can be trasferred over that 1-hr period is the total_bits_avail.
        ;So, For SSR1, total_bits_avail is the tape-dump period(s) within the displayed plot-range NOT
        ;all of the plot-range.
        ;

        usage= arry(*,plot_range)
        ;** usage is in bits scheduled per telescope over entire plot range

        pct_total= 0D
        ;pct_each= DBLARR(5)
        pct_each= DBLARR(7)
        total_bits_avail= TOTAL(td)

        s1_usage= usage
        s1_total_bits_avail= total_bits_avail
        IF (schedv.pct_stats EQ 1) THEN BEGIN
         
          pct_total = DOUBLE(TOTAL(usage))/8000000.0 ; bits to MB
          pct_each =  DOUBLE(TOTAL(usage,2))/8000000.0 ; bits to MB
          WIDGET_CONTROL, schedv.s1_tot, SET_VALUE= STRING(pct_total,'(f12.3)') +' '
          WIDGET_CONTROL, schedv.s1_eu, SET_VALUE= STRING(pct_each(0),'(f13.3)')+' '
          WIDGET_CONTROL, schedv.s1_c1, SET_VALUE= STRING(pct_each(1),'(f13.3)')+' '
          WIDGET_CONTROL, schedv.s1_c2, SET_VALUE= STRING(pct_each(2),'(f11.3)')+' '
          WIDGET_CONTROL, schedv.s1_h1, SET_VALUE= STRING(pct_each(3),'(f12.3)')+' '
          WIDGET_CONTROL, schedv.s1_h2, SET_VALUE= STRING(pct_each(4),'(f11.3)')+' '
          WIDGET_CONTROL, schedv.s1_hk, SET_VALUE= STRING(pct_each(5),'(f11.3)')+' '
          WIDGET_CONTROL, schedv.s1_gt, SET_VALUE= STRING(pct_each(6),'(f11.3)')+' '
          ;goto, skipdata
       ENDIF ELSE BEGIN  
        IF (total_bits_avail GT 0) THEN BEGIN
          pct_total = (DOUBLE(TOTAL(usage)) / total_bits_avail) * 100
          pct_each = (DOUBLE(TOTAL(usage,2)) / total_bits_avail) * 100
        ENDIF 

        IF (total_bits_avail EQ 0) THEN BEGIN
          ;stats_str = "        No Playback Present Within The Selected Range"
          WIDGET_CONTROL, schedv.s1_tot, SET_VALUE="     (No "
          WIDGET_CONTROL, schedv.s1_eu, SET_VALUE= "Playback"
          WIDGET_CONTROL, schedv.s1_c1, SET_VALUE= "Present  "
          WIDGET_CONTROL, schedv.s1_c2, SET_VALUE= "Within   "
          WIDGET_CONTROL, schedv.s1_h1, SET_VALUE= "Selected "
          WIDGET_CONTROL, schedv.s1_h2, SET_VALUE= "Range)   "
        ENDIF ELSE BEGIN 
;          stats_str = STRING(pct_total,'(f12.3)')  +'%'+ $
;                      STRING(pct_each(0),'(f13.3)')+'%'+ $
;                      STRING(pct_each(1),'(f9.3)') +'%'+ $
;                      STRING(pct_each(2),'(f11.3)')+'%'+ $
;                      STRING(pct_each(3),'(f12.3)')+'%'+ $
;                      ;STRING(pct_each(4),'(f11.3)')+'%'
;                      STRING(pct_each(4),'(f11.3)')+'%'+ $
;                      STRING(pct_each(ihk),'(f11.3)')+'%'+ $
;                      STRING(pct_each(igt),'(f11.3)')+'%'
;
          ;WIDGET_CONTROL, schedv.ssr1_stats, SET_VALUE='SSR1 '+stats_str

          WIDGET_CONTROL, schedv.s1_tot, SET_VALUE= STRING(pct_total,'(f12.3)') +'%'
          WIDGET_CONTROL, schedv.s1_eu, SET_VALUE= STRING(pct_each(0),'(f13.3)')+'%'
          WIDGET_CONTROL, schedv.s1_c1, SET_VALUE= STRING(pct_each(1),'(f13.3)')+'%'
          WIDGET_CONTROL, schedv.s1_c2, SET_VALUE= STRING(pct_each(2),'(f11.3)')+'%'
          WIDGET_CONTROL, schedv.s1_h1, SET_VALUE= STRING(pct_each(3),'(f12.3)')+'%'
          WIDGET_CONTROL, schedv.s1_h2, SET_VALUE= STRING(pct_each(4),'(f11.3)')+'%'
          WIDGET_CONTROL, schedv.s1_hk, SET_VALUE= STRING(pct_each(5),'(f11.3)')+'%'
          WIDGET_CONTROL, schedv.s1_gt, SET_VALUE= STRING(pct_each(6),'(f11.3)')+'%'
        ENDELSE
      ENDELSE
;skipdata:

  RETURN
END
