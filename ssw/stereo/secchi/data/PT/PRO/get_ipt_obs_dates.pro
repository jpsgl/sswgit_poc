;+
;$Id: get_ipt_obs_dates.pro,v 1.1.1.2 2004/07/01 21:19:02 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GET_IPT_OBS_DATES
;
; Purpose     : Returns observation dates for images in a .IPT file.
;
; Use         : dates= GET_IPT_OBS_DATES(ipt_file_name)
;
; Inputs      : ipt_file_name   An .IPT file for which observation dates are required.
;
; Opt. Inputs : None
;
; Outputs     : dates           An string array of observation date/times.            
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;
; $Log: get_ipt_obs_dates.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

FUNCTION GET_IPT_OBS_DATES,ipt_file_name

;OPENR, lun, GETENV('PT')+'/IO/IPT/'+ipt_file_name, /GET_LUN
OPENR, lun,ipt_file_name, /GET_LUN

str= ''
dates=''
READF,lun,str
WHILE ( NOT(EOF(lun))) DO BEGIN
 IF (STRPOS(str,'PT_LP_START') GE 0 OR STRPOS(str,'PT_LP_END') GE 0) THEN BEGIN
   toks= STR_SEP(str,'(')
   toks= STR_SEP(toks(1),')')
   dates= dates+','+toks(0)
 ENDIF
 READF,lun,str
END

CLOSE, lun 
FREE_LUN, lun 

dates= STR_SEP(dates,',')
dates= dates(1:*)

RETURN, dates

END
