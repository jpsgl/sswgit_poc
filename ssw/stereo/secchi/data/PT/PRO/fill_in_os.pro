;+
;$Id: fill_in_os.pro,v 1.3 2005/01/24 17:56:32 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : FILL_IN_OS
;
; Purpose     : Populate a OS structure from input .IPT file for a single OS.
;
; Use         : os = FILL_IN_LP(key_val_arr, used)
;
; Inputs      : key_val_arr  parameters pickedup from the .IPT file. 
;               used         Mark parameters that were actually used.
;
; Opt. Inputs : None
;
; Outputs     : os           IDL structure containing OS parameters.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/15/04 - Added processing time (os_proct). 
;              Ed Esfandiari 06/16/04 - Added setup time (os_setup).
;              Ed Esfandiari 09/22/04 - Added spacecraft (sc) tag.
;              Ed Esfandiari 10/14/03 - Modified SET_ID code to handle S/C A & B.
;              Ed Esfandiari 10/15/03 - Added SC A/B bsf field.
;
; $Log: fill_in_os.pro,v $
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION FILL_IN_OS, key_val_arr, used
COMMON DIALOG, mdiag,font

   nsched = KEYWORD_VALUE(key_val_arr, 'PT_LP_NSCHED', index)

   IF (nsched EQ '') THEN BEGIN
      PRINT, '%%%FILL_IN_OS: PT_LP_NSCHED undefined, assuming 1.'
      nsched = 1
   ENDIF ELSE BEGIN
      nsched = FIX(nsched)
      used(index) = 1
   ENDELSE

   utc = {cds_int_time, mjd:0L, time:0L}

   os = {os_num:	-1L,	$
         ;set_id:         0L,    $  ; AEE - 2/11/04 - added.
         op_num:	-1L,	$
         op_id_name:	'',	$
         date_gen:	'',	$
         os_obj:	'',	$
         lp_num:	-1,	$
         lp_exit:	-1,	$
         lp_iter:	-1,	$
         lp_dur:	-1,	$
         os_size:	 0L,	$
         os_dur:	0.0,	$
         os_rot:        0.0,    $  ; AEE - 01/27/03 - added readout (rot) and pre-processing (ppt) times
         os_ppt:        0.0,    $
         os_setup:      0.0,    $
         os_cad:        0.0,    $  ; AEE - 01/27/03 - added cadence (cad)
         os_proct:      0.0,    $
         lp_nsched:	nsched,	$
         lp_start:	REPLICATE(utc,nsched),	$
         lp_sc:         REPLICATE('',nsched),   $
         bsf_id:        '',     $
         set_id:        INTARR(nsched), $
         lp_load_cam:	BYTARR(nsched),	$		;** 1 means do it
         lp_load_wlexp:	BYTARR(nsched),	$		;** 1 means do it
         lp_do_candc:	BYTARR(nsched),	$		;** 1 means do it
         lp_move_fp_wl:	DBLARR(nsched),	$		;** wavelength
         lp_move_fp_order:INTARR(nsched),	$	;** order
         lp_end:	REPLICATE(utc,nsched) 	$
        }

   os_tags = TAG_NAMES(os)
   get_num = 15 ;** only want the first n (os_num thru os_proct)
   fatal = [0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0] ;  (1=true, 0=false)
   get_vars = 'PT_' + os_tags(0:get_num)

   FOR i=0, get_num DO BEGIN
      val = KEYWORD_VALUE(key_val_arr, get_vars(i), index)
      IF (val EQ '') THEN BEGIN	;** undefined
         IF (fatal(i)) THEN BEGIN
            PRINT, '%%%FILL_IN_OS: '+get_vars(i)+' must be defined.  Fatal Error'
            RETURN, -1
         ENDIF
      ENDIF ELSE BEGIN
         IF ( (get_vars(i) EQ 'PT_OS_NUM') AND (STRMID(val,0,1) EQ 'T') ) THEN $
            val = 0
         t = DATATYPE(os.(i))
         CASE (t) OF
            'INT' : val = FIX(val)
            'LON' : val = LONG(val)
            'STR' : val = val
            'FLO' : val = FLOAT(val)
            ELSE : BEGIN
                  PRINT, '%%%FILL_IN_OS: Undefined datatype: '+t
                  RETURN, -1
               END
         ENDCASE
         os.(i) = val
         used(index) = 1
      ENDELSE

   ENDFOR

   ;** parse through start times
   start = KEYWORD_VALUE(key_val_arr, 'PT_LP_START', index, /GET_ALL)
   IF (start EQ '') THEN BEGIN
      PRINT, '%%%FILL_IN_OS: PT_LP_START undefined.  Fatal Error.'
      RETURN, -1
   ENDIF ELSE BEGIN
      start_utc = PTDATE2UTC(start)
      IF (os.lp_nsched NE N_ELEMENTS(start_utc)) THEN BEGIN
         PRINT, '%%%FILL_IN_OS: PT_LP_NSCHED: '+STRN(os.lp_nsched)+' not equal N_ELEMENTS(PT_LP_START)'+ $
            STRN(N_ELEMENTS(start_utc))
         IF (os.lp_nsched GT N_ELEMENTS(start_utc)) THEN BEGIN
            PRINT, '%%%FILL_IN_OS: Fatal Error.'
            RETURN, -1
         ENDIF
         PRINT, '%%%FILL_IN_OS: Only using first '+STRN(os.lp_nsched)+' elements of PT_LP_START.'
         start_utc = start_utc(0:os.lp_nsched-1)
      ENDIF
      os.lp_start = start_utc
      used(index) = 1
   ENDELSE

   sc= KEYWORD_VALUE(key_val_arr, 'PT_SC_ID', index, /GET_ALL)
   sc= STR_SEP(STRMID(sc,1,STRLEN(sc)-2),',') ; remove ( and ) and separate tokens between comma's.
   IF (N_ELEMENTS(sc) NE nsched) THEN BEGIN
     PRINT,' ERROR: PT_SC_ID does not contain correct number of parameters.'
     STOP
   ENDIF
   os.lp_sc= sc
   used(index) = 1

   bsf_id= KEYWORD_VALUE(key_val_arr, 'PT_BSF_ID', index, /GET_ALL)
   os.bsf_id= bsf_id
   IF (index GE 0) THEN used(index) = 1

   set_id= KEYWORD_VALUE(key_val_arr, 'PT_SET_ID', index, /GET_ALL)
   IF (STRPOS(set_id,'(') GE 0) THEN $
     set_id= STR_SEP(STRMID(set_id,1,STRLEN(set_id)-2),',') $ ;remove (&) and separate tokens between comma's.
   ELSE $
     set_id= REPLICATE(FIX(set_id),nsched)
   os.set_id= set_id
   used(index) = 1

   ;** parse through to see if any need cam table updates
   start = KEYWORD_VALUE(key_val_arr, 'PT_LOAD_CAM', index, /GET_ALL)
   IF (start NE '') THEN BEGIN
      used(index) = 1
      start_cam = UTC2TAI(PTDATE2UTC(start))
      start_tai = UTC2TAI(start_utc)
      FOR s=0, N_ELEMENTS(start_cam)-1 DO BEGIN
         ind = WHERE(start_cam(s) EQ start_tai)
         IF (ind(0) EQ -1) THEN BEGIN 
            PRINT, 'ERROR camera table load time does not correspond to any OS start time'
         ENDIF ELSE $ 
            os.lp_load_cam(ind) = 1
      ENDFOR
   ENDIF

   ;** parse through to see if any need wl/exp table updates
   start = KEYWORD_VALUE(key_val_arr, 'PT_LOAD_WLEXP', index, /GET_ALL)
   IF (start NE '') THEN BEGIN
      used(index) = 1
      start_cam = UTC2TAI(PTDATE2UTC(start))
      start_tai = UTC2TAI(start_utc)
      FOR s=0, N_ELEMENTS(start_cam)-1 DO BEGIN
         ind = WHERE(start_cam(s) EQ start_tai)
         IF (ind(0) EQ -1) THEN BEGIN 
            PRINT, 'ERROR wavelength/exp table load time does not correspond to any OS start time' 
         ENDIF ELSE $
            os.lp_load_wlexp(ind) = 1
      ENDFOR
   ENDIF

   ;** parse through to see if any check and corrects needed
   start = KEYWORD_VALUE(key_val_arr, 'PT_DO_CANDC', index, /GET_ALL)
   IF (start NE '') THEN BEGIN
      used(index) = 1
      start_cc = UTC2TAI(PTDATE2UTC(start))
      start_tai = UTC2TAI(start_utc)
      FOR s=0, N_ELEMENTS(start_cc)-1 DO BEGIN
         ind = WHERE(start_cc(s) EQ start_tai)
         IF (ind(0) EQ -1) THEN BEGIN 
            PRINT, 'ERROR check and correct time does not correspond to any OS start time' 
         ENDIF ELSE $
            os.lp_do_candc(ind) = 1
      ENDFOR
   ENDIF


   ;** fill in rest of structure according to exit criteria
   CASE (os.lp_exit) OF

      ;** Iteration
      0 : IF (os.lp_iter LE 0) THEN BEGIN
             PRINT, '%%%FILL_IN_OS: Undefined iteration count.  Assuming 1.'
             os.lp_iter = 1
          ENDIF		

      ;** Time Duration
      1 : BEGIN
             dur = KEYWORD_VALUE(key_val_arr, 'PT_LP_DUR', index)
             IF (dur EQ '') THEN BEGIN
                PRINT, '%%%FILL_IN_OS: PT_LP_DUR undefined.  Fatal Error.'
                RETURN, -1
             ENDIF ELSE BEGIN
                os.lp_dur = FIX(dur)
                used(index) = 1
             ENDELSE
          END

      ;** Absolute Time
      2 : BEGIN
             ;** parse through end times
             endt = KEYWORD_VALUE(key_val_arr, 'PT_LP_END', index, /GET_ALL)
             IF (endt EQ '') THEN BEGIN
                PRINT, '%%%FILL_IN_OS: PT_LP_END undefined.  Fatal Error.'
                RETURN, -1
             ENDIF ELSE BEGIN
                end_utc = PTDATE2UTC(endt)
                IF (os.lp_nsched NE N_ELEMENTS(end_utc)) THEN BEGIN
                   PRINT, '%%%FILL_IN_OS: PT_LP_NSCHED: '+STRN(os.lp_nsched)+$
                      ' not equal N_ELEMENTS(PT_LP_END)'+ STRN(N_ELEMENTS(end_utc))
                   IF (os.lp_nsched GT N_ELEMENTS(end_utc)) THEN BEGIN
                      PRINT, '%%%FILL_IN_OS: Fatal Error.'
                      RETURN, -1
                   ENDIF
                   PRINT, '%%%FILL_IN_OS: Only using first '+STRN(os.lp_nsched)+' elements of PT_LP_END.'
                   end_utc = end_utc(0:os.lp_nsched-1)
                ENDIF
                os.lp_end = end_utc
                used(index) = 1
             ENDELSE
          END

      ;** Unknown
      DEFAULT : BEGIN
             PRINT, '%%%FILL_IN_OS: PT_LP_EXIT Illegal Value: '+STRN(os.lp_exit)+'  Fatal Error.'
             RETURN, -1
          END

   ENDCASE

   RETURN, os
END
