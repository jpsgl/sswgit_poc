;+
;$Id: get_pre_exp_times.pro,v 1.2 2004/09/08 17:01:26 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GET_PRE_EXP_TIMES 
;
; Purpose     : Generate commanded exposure times of one or more images.
;
; Explanation : This function generates and returns commanded exposure times of images
;               by calculating pre-exposure setup time of images and adding them to
;               the images start-times.
;
; Use         : pexpt= GET_PRE_EXP_TIMES(sdt,lp,tel,led) 
;
; Inputs      : sdt   - Array of image start-times of form "yyyymmdd-hhmmss"
;               lp    - Array of obsevation programs.
;               tel   - Array of telescopes.
;               led   - Array of LED IDs.
;
; Opt. Inputs : None
;
; Outputs     : pexpt - Array of image exposure-times. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, Aug 2004 - First Version.
;
; Modification History:
;
; $Log: get_pre_exp_times.pro,v $
; Revision 1.2  2004/09/08 17:01:26  esfand
; removed debug stuff
;
; Revision 1.1  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
;
;-
;

FUNCTION GET_PRE_EXP_TIMES,start_times,lp_nums,telescopes,lamps

 n = N_ELEMENTS(start_times)
 pre_exp_times= start_times

 RESTORE, FILENAME= GETENV('PT')+'/IN/OTHER/'+'mech_times.sav'
 ; => filter_time,polar_time,shutter_time (all units of sec).

 FOR i= 0, n-1 DO BEGIN
   tele= telescopes(i)-1  ; telescopes are 1-5 (not 0-4) at this point. use 0-4 for tele.
   lp= lp_nums(i)
   lamp= lamps(i)

   IF (lp LE 6) THEN BEGIN 
     ; for Non block-seq images (lp=0-6), set the obs-time to the start of exposure time
     ; by adding the pre-exposure setup time (filter, polar, shutter times) to the
     ; obs_time:

     ; shutter_time is the time to open or close the shutter. So, for images that require
     ; shutter movement use it as the shutter open/close time.
     ; Also, HI1 and HI2 are shutterless (and their doors open only once and remain open), so set
     ; the shutter_time for HI1 and 2 to zero to use 0 for shutter open/close times. For SCIP
     ; telescopes shutters are also not opened/closed for LED images that are FPA mounted.
     ; For Dark images we also don't have any shutter movement for any instrument:

     pexp = shutter_time 

     IF (tele GT 2) THEN pexp = 0    ; No shutter open/close for any type of HI image.
     IF (lp EQ 2) THEN pexp = 0      ; No shutter open/close for Darks
     IF (lp EQ 3) THEN BEGIN        ; no shutter open/close for SCIP FPA mounted LED images.
       IF (tele EQ 1) THEN pexp = 0  ; No shutter open/close for COR1 LED images.
       IF (tele EQ 2 AND lamp GT 0) THEN $
          pexp= 0                    ; No shutter open/close for COR2 Blu&Pur LED images
                                    ; because they are FPA mounted).
       IF (tele EQ 0 AND lamp EQ 0) THEN $
         pexp = 0                    ; No shutter open/close for EUVI first Blue LED image.
     ENDIF

     ; Now pexp contains a valid shutter time to be used for each shutter open.
     ; For images that no shutter movement is needed, pexp is now zero.
 
     ;** Move a mech

     ; Note: There is no Filter Wheel movement for CO1, COR2, HI1, and HI2. There is also no
     ; Polarizer wheel for HI1 and HI2.  In addition, there is no filter/polar movement for any
     ; dark image. So, for these don't add filter and polar movement times:

     ; Add sector/polar movement time if needed:
     IF (lp NE 2 AND tele LE 2) THEN BEGIN ; None dark SCIP images
       pexp = pexp + polar_time ; only SCIP has sector/polar wheel
     ENDIF

     ; Add filter movement time if needed:

     IF (lp NE 2 AND tele EQ 0) THEN pexp = pexp + filter_time  ; only none DARK EUVI images use filters.

     ; *** should ccd CLEAR time also be added to the pre_exposure_time? ***

     ; Now add pexp (pre-exposure setup time to to the start time. Since dates in upload file 
     ; are only accurate to seconds, round pexp to get rid of milliseconds:

     IF (pexp GT 0.0) THEN $ 
       pre_exp_times(i)= ECS_DATES2IPT(TAI2UTC(UTC2TAI(PTDATE2UTC(pre_exp_times(i)))+ROUND(pexp),/ECS)) 

   ENDIF
 ENDFOR
 
 RETURN, pre_exp_times

END

