;+
;$Id: get_comp_str.pro,v 1.3 2005/01/24 17:56:33 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GET_COMP_STR
;
; Purpose     : Returns compresstion string for compression step used by an OS.
;
; Use         : comp= GET_COMP_STR(os)
;
; Inputs      : OS       Obervation for which compression string is to be produced.
;
; Opt. Inputs : None
;
; Outputs     : comp     Compression string - 1 char per compression step.
;
; Opt. Outputs: None
;
; Keywords    :
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;
; $Log: get_comp_str.pro,v $
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:01  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION GET_COMP_STR, os
COMMON DIALOG, mdiag,font


   comp_types = READ_IP_DAT()
   comp_types_cnt= N_ELEMENTS(comp_types)-1
   comp_str = ''

   iptable = os.iptable
   ip = os.ip(iptable)
   good = WHERE(ip.steps GE 0, nip)
   FOR n=0, nip-1 DO BEGIN
    IF (ip.steps(good(n)) LE comp_types_cnt) THEN BEGIN  ; ignore HI summing 2nd and 3rd realated ip_nums.
      obecode = ip.steps(good(n))
      cind = WHERE(obecode EQ comp_types.ip_num)
      IF (cind(0) NE -1) THEN BEGIN
         comp_str = comp_str + comp_types(cind(0)).ip_char
      ENDIF ELSE BEGIN
        IF(DATATYPE(scip_os) NE 'UND') THEN $
          WIDGET_CONTROL,mdiag,SET_VALUE='%%WARNING unknown compression type '+STRN(obecode)
        PRINT, '%%WARNING unknown compression type', obecode
      ENDELSE
     ENDIF
   ENDFOR

   RETURN, comp_str

END
