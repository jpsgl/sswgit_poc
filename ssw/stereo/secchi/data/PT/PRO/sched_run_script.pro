;+
;$Id: sched_run_script.pro,v 1.5 2009/09/11 20:28:22 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SCHED_RUN_SCRIPT
;               
; Purpose     : An interactive widget to select a run script file.
;               
; Use         : SCHED_RUN_SCRIPT, rsfile, start_time, sc 
;    
; Inputs      : start_time   Default time (start of schedule) to schedule the selected file  
;             : sc         0=SC-AB, 1=SC-A, 2=SC-B
;
;
; Opt. Inputs : None
;               
; Outputs     : rsfile      Name of the selected run script file. 
;             : start_time  String - date/time: Selected time to schedule the run script file.
;                                    remove:    Remove existing run script file from schedule.
;                                    blank:     Cancel the command.
;                           
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  :  Ed Esfandiari, NRL, May 2005 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 05/09/06 - Allow 8.* filenames.
;
;
; $Log: sched_run_script.pro,v $
; Revision 1.5  2009/09/11 20:28:22  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.1  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
;
;
;-


PRO SCHED_RUN_SCRIPT_EVENT, event

COMMON LP_SCHED_RUN_SCRIPT_SHARE, rscmdv
COMMON TMPRS, spcraft
COMMON RUN_SCRIPT, rsc

CASE (event.id) OF

   rscmdv.dismiss : BEGIN       ;** exit program
      rscmdv.scheduled= 0 ; false 
      rscmdv.rsfile= '' 
      rscmdv.start_time= 'cancel' 
      WIDGET_CONTROL, /DESTROY, rscmdv.base1
   END

   rscmdv.remove: BEGIN
          rscmdv.start_time = 'remove'
          IF (spcraft EQ 0) THEN $
            list= WHERE((rsc.sc EQ 'A' OR rsc.sc EQ 'B') AND rsc.bsf EQ 0, cnt)
          IF (spcraft EQ 1) THEN $
            list= WHERE(rsc.sc EQ 'A' AND rsc.bsf EQ 0, cnt)
          IF (spcraft EQ 2) THEN $
            list= WHERE(rsc.sc EQ 'B' AND rsc.bsf EQ 0, cnt)

          IF (cnt GT 0) THEN BEGIN
            fn= rsc(list(SORT(rsc(list).dt))).fn
            dt= rsc(list(SORT(rsc(list).dt))).dt
            ;fn= STRMID(fn,STRLEN(fn)-12,12)
            ;dates= TAI2UTC(rsc(list(SORT(rsc(list).dt))).dt,/ECS)+' '+ fn 
            fc= N_ELEMENTS(fn)
            dates= STRARR(fc)
            FOR fcnt= 0, fc-1 DO dates(fcnt)= TAI2UTC(dt(fcnt),/ECS)+' '+ $
                                 STRMID(fn(fcnt),RSTRPOS(fn(fcnt),'.')-8,20)
            IF (spcraft EQ 0) THEN dates= dates+' S/C-'+rsc(list(SORT(rsc(list).dt))).sc
            result= PICKFILES2(FILES=dates, XSIZE=300, TITLE="Choose RunScript(s) to Remove")

         
            IF ( result(0) NE '' ) THEN BEGIN
              fn= rsc.fn
              dt= rsc.dt
              nf= N_ELEMENTS(fn)
              sc= ' S/C-'+rsc.sc 
              IF (spcraft EQ 1) THEN result= result+' S/C-A'
              IF (spcraft EQ 2) THEN result= result+' S/C-B'
              dtfn= STRARR(nf)
              FOR i=1, nf-1 DO BEGIN ; first one is the dummy entry.
                dtfn(i)= TAI2UTC(dt(i),/ECS)+' '+STRMID(fn(i),RSTRPOS(fn(i),'.')-8,20)+sc(i) 
              END
               
              FOR i=0, N_ELEMENTS(result)-1 DO BEGIN
                rmv= WHERE(dtfn EQ result(i))
                rsc(rmv).sc='R'
              ENDFOR
              keep= WHERE(rsc.sc NE 'R')
              ;help,rsc
              ;print,tai2utc(rsc.dt,/ecs)

              rsc= rsc(keep)

              ;help,rsc
              ;print,tai2utc(rsc.dt,/ecs)
              ;stop
            ENDIF
          ENDIF ELSE BEGIN
            rscmdv.start_time = 'not_remove'
          ENDELSE
          WIDGET_CONTROL, /DESTROY, rscmdv.base1 

   END

   rscmdv.schedule : BEGIN

      WIDGET_CONTROL, rscmdv.time_text, GET_VALUE= stime
      rscmdv.start_time= STRTRIM(stime(0),2) 
      rscmdv.scheduled= 1 ; true 

      WIDGET_CONTROL, /DESTROY, rscmdv.base1
    END

   rscmdv.rsfind : BEGIN
     rscmdv.rsfile= rscmdv.files(event.index)
   END

   rscmdv.time_text : BEGIN
     WIDGET_CONTROL, rscmdv.time_text, GET_VALUE= stime & stime = stime(0)
     rscmdv.start_time= stime
   END

   ELSE : BEGIN
   END

ENDCASE

END

 

PRO SCHED_RUN_SCRIPT,rsfile,start_time, sc

COMMON LP_SCHED_RUN_SCRIPT_SHARE, rscmdv
COMMON TMPRS, spcraft

    scheduled= 0 ; false 

    IF XRegistered("SCHED_RUN_SCRIPT") THEN RETURN

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    spcraft= sc

    CASE sc OF
      0: tsc= 'SC-AB'
      1: tsc= 'SC-A'
      2: tsc= 'SC-B'
    ENDCASE


      base1 = WIDGET_BASE(/COLUMN, TITLE='Schedule/Remove Run Script for '+tsc, /FRAME)

      row= WIDGET_BASE(base1, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='       ')
      schedule = WIDGET_BUTTON(row, VALUE=' Schedule Selected File at Displayed Time',/FRAME)
      tmp = WIDGET_LABEL(row, VALUE='         ')
      remove = WIDGET_BUTTON(row, VALUE=' Remove Currently Scheduled command(s) ',/FRAME)

      row0 = WIDGET_BASE(base1, /ROW)

      col = WIDGET_BASE(row0, /COLUMN, /FRAME)

      ; AEE 12/29/03 - select a Run Script File from IN/RUN_SCRIPT directory:

      row = WIDGET_BASE(col, /ROW)
        temp = WIDGET_LABEL(row, VALUE='Select a Run Script File:')
        ;files= FINDFILE(GETENV('PT')+'/IN/RUN_SCRIPT/????????.???') ; allow all 8.3 filenames
        files= FINDFILE(GETENV('PT')+'/IN/RUN_SCRIPT/????????.*') ; allow all 8.* filenames
        
        IF (STRLEN(rsfile) EQ 0) THEN BEGIN
          ind= 0 
          rsfile= files(0)
        ENDIF ELSE BEGIN
          ind= WHERE(files EQ rsfile,cnt)
          IF (cnt EQ 0) THEN BEGIN
            PRINT,''
            PRINT, rsfile + ' does NOT exist.'
            PRINT,''
            ind= 0
          ENDIF
        ENDELSE

        rsfind =  CW_BSELECTOR2(row, files, SET_VALUE=ind(0))

      row = WIDGET_BASE(col, /ROW)
      row = WIDGET_BASE(col, /ROW)
        temp = WIDGET_LABEL(row, VALUE='Schedule Run Script File at yyyy/mm/dd hh:mm:ss ')
        time_text= WIDGET_TEXT(row, /EDITABLE, YSIZE=1, XSIZE=24,VALUE=STRMID(start_time,0,19))
      row = WIDGET_BASE(col, /ROW)
      msg=''
      msg='     RunScript Files Can Not Contain Observations And Should Not Interfere With Scheduled OSes.'
      font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
      temp = WIDGET_LABEL(row, VALUE= msg, FONT=font)

     dismiss = WIDGET_BUTTON(base1, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base1
    WIDGET_CONTROL, time_text, /INPUT_FOCUS

    rscmdv = CREATE_STRUCT( 'base1', base1,                     $
                             'files',files,                     $
                             'rsfind', rsfind,                  $
                             'rsfile', rsfile,                  $ 
                             'start_time', start_time,          $
                             'time_text', time_text,            $
                             'dismiss', dismiss,                $
                             'remove', remove,                  $
                             'scheduled', scheduled,            $
                             'schedule', schedule)

    XMANAGER, "SCHED_RUN_SCRIPT", base1, /MODAL

  rsfile= rscmdv.rsfile
  start_time= rscmdv.start_time 

  RETURN

END


