;+
;$Id: generate_itos_schedule.pro,v 1.20 2014/06/06 19:42:03 mcnutt Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GENERATE_ITOS_SCHEDULE
;
; Purpose     : Generate a command schedule.
;
; Explanation : This pro takes as input an IPT file and generates either a
;               *.IPT.RTS (RelativeTimeSchedule) if /rts keyword is set or a
;               *.IPT.ATS (AbsoluteTimeSchedule) schedule file.
;
; Use         : GENERATE_ITOS_SCHEDULE, ipt_name, launch_date, /RTS
;
; Inputs      : ipt_name     The .IPT scheule file to be converted to the command file.
;             : launch_date  S/C launch date used in .ATS command files. 
;             : readtime     FSW time (sec) required to read in an schedule (used for RTS and BSF) 
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : RTS           If present, generate a .RTS file, otherwise, generate a .ATS file.
;               BSF           IF present, generate out put in ..../BSF directory. 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/07/04 - Corrected set ID code and made os_num string 4-digits.
;              Ed Esfandiari 08/05/04 - Added call to get_pre_exp_times to use the start of
;                                       exp.times as the date-obs of images.
;              Ed Esfandiari 08/31/04 - Changed LED pulses from 5 to 7 bytes.
;              Ed Esfandiari 09/08/04 - Added dashes for scip sequences of less that 4 images
;                                       at the end of command line.
;                                       Also sort the output by time+img_cntr and re-assign 
;                                       img_cntr if time changes changes order of images taken.
;              Ed Esfandiari 09/30/04 - Use SC_ID (A, B, or AB) to set the sync byte (AB=1). 
;              Ed Esfandiari 09/30/04 - assigned set_id from IPT file.
;              Ed Esfandiari 10/04/04 - direct output to SC_A or SC_B.
;              Ed Esfandiari 10/22/04 - IDLs STRING function can only handle data array of up
;                                       to 1024 elements when using explicit formatting. Replaced
;                                       some STRING calls with my INT2STR function, instead. 
;              Ed Esfandiari 11/08/04 - Added /BSF keyword and, if set, to generate .bsf file.
;              Ed Esfandiari 01/21/05 - Removed call to get_pre_exp_times as agreed upon with Dennis.
;              Ed Esfandiari 02/04/05 - Added new method of setting B and T commands and also added I
;                                       command. Also, now, T command has B command's time. 
;              Ed Esfandiari 03/10/05 - Moved delay checking to schedule_event.pro.
;              Ed Esfandiari 04/27/05 - Removed img_cntr form image taking command lines.
;              Ed Esfandiari 05/02/05 - Added FWS readtime as beg_cmd and end_cmd times and shifted
;                                       all other commands in schedule so that first command starts
;                                       at zero offset for only .RTS and .BSF files.
;              Ed Esfandiari 05/03/05 - Added Run Script (A) command.
;              Ed Esfandiari 05/18/05 - Made LEDs start from 1 instead of 0.
;              Ed Esfandiari 03/16/06 - Corrected GT dump duration for RTS files and RT-SSR1 value set to 1.
;              Ed Esfandiari 03/20/06 - For RTS and BSF files, use smallest date from images, GTdumps, InitTL,
;                                       etc..
;              Ed Esfandiari 04/21/06 - Added table usage info as comments to upload schedules.
;              Ed Esfandiari 05/09/06 - Allow 8.* filenames for Run Scripts.
;              Ed Esfandiari 05/23/06 - Added PT filename info and removed $ from table info.
;              Ed Esfandiari 12/06/06 - Fixed floating overflow problem with adjsuting times for RTS files.
;              Ed Esfandiari 12/13/06 - Adde GTIPT common block.
;              Ed Esfandiari 01/12/06  For a .ATS file, set B and T commands to dummy RTS format of '---00000000'. 
;                                      B is not used by flight software and if .ATS file contains a .bsf file,
;                                      the .ATS style B line causes problems processing the .bsf file (requested
;                                      by Dennis Wang). 
;              Ed Esfandiari 02/27/07  Made changes to handle very long .IPT lines.
;              Ed Esfandiari 08/14/09  Changed SPAWN Id to Id to avoid cvs cobfusion.
;
;
; $Log: generate_itos_schedule.pro,v $
; Revision 1.20  2014/06/06 19:42:03  mcnutt
; checking for modification
;
; Revision 1.19  2013/01/09 13:33:23  mcnutt
; changed made when generating new block sched 20130108
;
; Revision 1.18  2011/10/31 19:18:46  nathan
; do NOT eliminate duplicate time rows
;
; Revision 1.17  2011/08/19 20:43:55  nathan
; ensure there are no duplicate rows by checking time
;
; Revision 1.16  2009/09/11 20:28:11  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.10  2005/10/17 19:02:09  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.9  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.8  2005/04/27 20:38:38  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.7  2005/03/10 16:42:16  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.6  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.4  2004/09/08 20:17:04  esfand
; use time+img_cntr to sort output commands and re-assign img_cntr if time order changes.
;
; Revision 1.3  2004/09/08 14:29:46  esfand
; dash fill scip seq commands of less that 4 images
;
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:01  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

pro GENERATE_ITOS_SCHEDULE, ipt_fname, launch_date, readtime, rts=rts, bsf=bsf

COMMON OS_SCHEDULED 
COMMON SCHED_SHARE, schedv
COMMON DIALOG, mdiag,font
COMMON GTIPT, gt4ipt_names
COMMON TABLES_IN_USE, tables_used, tbAout, tbBout 

 IF (schedv.sc EQ 0) THEN sc= 'SC_AB'
 IF (schedv.sc EQ 1) THEN sc= 'SC_A'
 IF (schedv.sc EQ 2) THEN sc= 'SC_B'

  ;f= FINDFILE(GETENV('PT')+'/IO/IPT/'+ipt_fname)
  f= FINDFILE(GETENV('PT')+'/IO/IPT/'+sc+'/'+ipt_fname)
  f= f(0)
  IF (STRLEN(STRMID(f,STRPOS(f,'SEC'),200)) NE 18) THEN BEGIN ; Not a valid SEC*.IPT file
    PRINT,''
    PRINT,'Invalid Input IPT filename '+ipt_fname+' - Did not Create '+ipt_fname+'.rel'
    WIDGET_CONTROL, mdiag, SET_VALUE= 'Invalid Input IPT '+ipt_fname+'. Didn''t create upload file.'
    PRINT,''
  ENDIF ELSE BEGIN
    OPENR,in,f,/GET_LUN
    n_sched= 0
    line=''
    WHILE NOT(EOF(in)) DO BEGIN
      READF,in,line
      pt_what= STRTRIM(STRMID(line,STRPOS(line,'PT_'),STRPOS(line,'=')),2)

      IF (pt_what EQ 'PT_OS_NUM') THEN BEGIN
        IF (n_sched EQ 0) THEN BEGIN ; start of first os - nothing read yet.
          ; init stuff:
          n_sched= 1 
          os_set='' ; AEE 2/12/04
          os_num='' & os_lp='' & os_tel='' & os_expt='' & os_camt='' & os_fw=''
          os_pw='' & os_exptime='' & os_led='' & os_ipt='' & os_fps='' & os_sync='' & img_cntr=''
          os_sdt= '' & os_edt= '' & os_apid= '' & os_bsf= '' & os_cad= '' & os_seq_cntr= '' & os_rsf=''
          new_set='' ; AEE 2/12/04
          new_os= '' & new_lp= '' & new_tel= '' & new_expt= '' & new_camt= '' & new_fw= ''
          new_pw= '' & new_exptime= '' & new_led= '' & new_ipt= '' & new_fps= '' & new_sync= ''
          new_img_cntr= '' & new_sdt= '' & new_edt= '' & new_apid= '' & new_bsf= '' & new_cad= ''
          new_seq_cntr= '' & new_rsf= ''
        ENDIF ELSE BEGIN ; end of one os-section and start of another
          ; for each OS, expand the once-only parameters to the number of images scheduled:

          ; For Door sections (OS_0001), throw away door schedules that are before or beyond
          ; current schedule range (already closed or open doors):

          IF (new_os EQ '0001') THEN BEGIN
            ok= WHERE(new_sdt GT '2004/01/01' AND new_sdt LT '2500/01/01', n_sched)
            IF (n_sched GT 0) THEN new_sdt= new_sdt(ok) 
          ENDIF
          IF (n_sched GT 0) THEN BEGIN
            ;os_set= [os_set,REPLICATE(new_set,n_sched)] ; AEE 2/12/04
            os_set= [os_set,new_set] ; AEE 2/12/04
            os_num= [os_num,REPLICATE(new_os,n_sched)]
            os_lp = [os_lp,REPLICATE(new_lp,n_sched)]
            os_tel= [os_tel,REPLICATE(new_tel,n_sched)]
            os_expt= [os_expt,REPLICATE(new_expt,n_sched)]
            os_camt= [os_camt,REPLICATE(new_camt,n_sched)]
            os_fw= [os_fw,REPLICATE(new_fw,n_sched)]
            os_pw= [os_pw,REPLICATE(new_pw,n_sched)] 
            os_exptime= [os_exptime,REPLICATE(new_exptime,n_sched)]
            os_led= [os_led,REPLICATE(new_led,n_sched)]
            os_ipt= [os_ipt,REPLICATE(new_ipt,n_sched)]
            os_fps= [os_fps,REPLICATE(new_fps,n_sched)]
            os_cad= [os_cad,REPLICATE(new_cad,n_sched)]
            os_seq_cntr= [os_seq_cntr,REPLICATE(new_seq_cntr,n_sched)]
            os_sdt= [os_sdt,new_sdt]
            os_bsf= [os_bsf,REPLICATE(new_bsf,n_sched)]
            os_rsf= [os_rsf,REPLICATE(new_rsf,n_sched)]

            IF (FIX(new_lp(0)) LT 7) THEN BEGIN 
              img_cntr= [img_cntr,new_img_cntr]  
              os_sync= [os_sync,new_sync]
            ENDIF ELSE BEGIN 
              img_cntr= [img_cntr,REPLICATE(new_img_cntr,n_sched)]
              os_sync= [os_sync,REPLICATE(new_sync,n_sched)]
            ENDELSE

            IF (FIX(new_lp(0)) EQ 10) THEN BEGIN ; GT dump
              os_edt= [os_edt,new_edt]
              os_apid= [os_apid,new_apid]
            ENDIF ELSE BEGIN
              os_edt= [os_edt,REPLICATE(new_edt,n_sched)]
              os_apid= [os_apid,REPLICATE(new_apid,n_sched)]
            ENDELSE
          ENDIF

          n_sched= 1 
          new_os= '' & new_lp= '' & new_tel= '' & new_expt= '' & new_camt= '' & new_fw= ''
          new_pw= '' & new_exptime= '' & new_led= '' & new_ipt= '' & new_fps= '' & new_sync= ''
          new_img_cntr= '' & new_sdt= '' & new_edt= '' & new_apid= '' & new_bsf= '' & new_cad= ''
          new_seq_cntr= '' & new_set= '' & new_rsf= ''
        ENDELSE        
      ENDIF

      ;val= STRTRIM(STRMID(line,STRPOS(line,'=')+1,2000),2)
      val= STRTRIM(STRMID(line,STRPOS(line,'=')+1,8000),2)

      CASE (pt_what) OF
        'PT_OS_NUM': BEGIN
          new_os= val
        END
        'PT_LP_NUM': BEGIN
          new_lp= val
        END
        'PT_LP_NSCHED': BEGIN
          ;n_sched= FIX(STRTRIM(STRMID(line,STRPOS(line,'=')+1,10),2))
          n_sched= FIX(val)
        END
        'PT_IMG_CNTR': BEGIN
          new_img_cntr= STR_SEP(STRMID(val,1,STRLEN(val)-2),',')
        END
        'PT_LP_START': BEGIN
           IF (new_sdt(0) EQ '') THEN $
             new_sdt= STR_SEP(STRMID(val,1,STRLEN(val)-2),',') $
           ELSE $   ; if more than one PT_LP_START lines for this os:
             new_sdt= [new_sdt,STR_SEP(STRMID(val,1,STRLEN(val)-2),',')]
        END
       'PT_LP_END': BEGIN ; only used for GT dumps (lp=10)
          IF (new_edt(0) EQ '') THEN $
            new_edt= STR_SEP(STRMID(val,1,STRLEN(val)-2),',') $
          ELSE $   ; if more than one PT_LP_START lines for this os:
            new_edt= [new_edt,STR_SEP(STRMID(val,1,STRLEN(val)-2),',')]
        END
       'PT_LP_APID': BEGIN ; only used for GT dumps (lp=10)
          new_apid= STR_SEP(STRMID(val,1,STRLEN(val)-2),',')
        END
        'PT_TELE': BEGIN
          new_tel= STRTRIM(val+1,2) ; cameras in .IPT start with 0 (FSW wants 1).
        END
        'PT_EXPT': BEGIN
          new_expt= STRTRIM(val-1,2)  ; exp tables in .IPT start with one (FSW wants 0).
        END
        'PT_CAMT': BEGIN
          new_camt= STRTRIM(val-1,2)  ; cam tables in .IPT start with one (FSW wants 0).
        END
        'PT_IP_TAB_NUM': BEGIN
          new_ipt= val               ; Image Processing table in .IPT start with 0 (FSW wants 0).
        END
        'PT_FW': BEGIN
          new_fw= val
        END
        'PT_PW': BEGIN
          ; For seqeunces, keep pw positions  between ( and ) as one entry since only one command will
          ; be generated per seq no matter how many images in each sequence:
          ; Also for double images, there will be 2 pw positions that must be kept as well.
          new_pw= STRMID(val,1,STRLEN(val)-2)
        END
        'PT_EXPTIME': BEGIN
          ; For seqeunces, keep exptimes between ( and ) as one entry since only one command will
          ; be generated per seq no matter how many images in each sequence:
          new_exptime= STRMID(val,1,STRLEN(val)-2)  ; remove '(' and ')'
        END
        'PT_CAL_LAMP': BEGIN
           ;new_led= val
           new_led= val +1  ; leds start with 1 not 0.
        END
        'PT_OS_CAD': BEGIN
          ;new_cad= val
          new_cad= STRING(ROUND(val+0.49),'(I3.3)') ; round up to next integer if decimal exists.
        END
        'PT_SEQ_CNTR': BEGIN
          new_seq_cntr= STRMID(val,1,STRLEN(val)-2)  ; remove '(' and ')'
        END
        'PT_FPS': BEGIN
          new_fps= val
        END
        'PT_SC_ID': BEGIN
          new_sync= STR_SEP(STRMID(val,1,STRLEN(val)-2),',')
        END
        'PT_BSFILE': BEGIN
          new_bsf= val
        END
        'PT_RSF': BEGIN
          new_rsf= val
        END
        'PT_SET_ID': BEGIN
          ;new_set= val
          new_set= STR_SEP(STRMID(val,1,STRLEN(val)-2),',') 
        END
        ELSE : BEGIN
         ;PRINT,'Following .IPT entry is not used:'
         ;PRINT,line
        END
      ENDCASE

    ENDWHILE

    ; Add the last OS section of the .IPT file:

    ;os_set= [os_set,REPLICATE(new_set,n_sched)] ; AEE 2/12/04
    os_set= [os_set,new_set] ; AEE 2/12/04
    os_num= [os_num,REPLICATE(new_os,n_sched)]
    os_lp = [os_lp,REPLICATE(new_lp,n_sched)]
    os_tel= [os_tel,REPLICATE(new_tel,n_sched)]
    os_expt= [os_expt,REPLICATE(new_expt,n_sched)]
    os_camt= [os_camt,REPLICATE(new_camt,n_sched)]
    os_fw= [os_fw,REPLICATE(new_fw,n_sched)]
    os_pw= [os_pw,REPLICATE(new_pw,n_sched)] 
    os_exptime= [os_exptime,REPLICATE(new_exptime,n_sched)]
    os_led= [os_led,REPLICATE(new_led,n_sched)]
    os_ipt= [os_ipt,REPLICATE(new_ipt,n_sched)]
    os_cad= [os_cad,REPLICATE(new_cad,n_sched)]
    os_seq_cntr= [os_seq_cntr,REPLICATE(new_seq_cntr,n_sched)]
    os_fps= [os_fps,REPLICATE(new_fps,n_sched)]
    IF (N_ELEMENTS(new_sync) NE n_sched) THEN $
      os_sync= [os_sync,REPLICATE(new_sync,n_sched)] $
    ELSE $
      os_sync= [os_sync,new_sync] 
    IF (N_ELEMENTS(new_img_cntr) NE n_sched) THEN $
      img_cntr= [img_cntr,REPLICATE(new_img_cntr,n_sched)] $
    ELSE $
      img_cntr= [img_cntr,new_img_cntr]

    os_sdt= [os_sdt,new_sdt]

    IF (N_ELEMENTS(new_edt) NE n_sched) THEN $
      os_edt= [os_edt,REPLICATE(new_edt,n_sched)] $
    ELSE $
      os_edt= [os_edt,new_edt]

    IF (N_ELEMENTS(new_apid) NE n_sched) THEN $
      os_apid= [os_apid,REPLICATE(new_apid,n_sched)] $
    ELSE $
      os_apid= [os_apid,new_apid]

    IF (N_ELEMENTS(new_bsf) NE n_sched) THEN $
      os_bsf= [os_bsf,REPLICATE(new_bsf,n_sched)] $
    ELSE $
      os_bsf= [os_bsf,new_bsf]

    IF (N_ELEMENTS(new_rsf) NE n_sched) THEN $
      os_rsf= [os_rsf,REPLICATE(new_rsf,n_sched)] $
    ELSE $
      os_rsf= [os_rsf,new_rsf]

;help,os_sdt,os_set,os_num,os_lp,os_tel,os_expt,os_camt,os_fw,os_pw,os_exptime,os_led,os_ipt,os_cad,os_seq_cntr,os_fps,os_sync,img_cntr,os_edt,os_apid,os_bsf,os_rsf
;stop

    ; remove the first (blank) elements:

    os_set= os_set(1:*) ; AEE 2/12/04
    os_num= STRING(os_num(1:*), FORMAT='(I4.4)')
    ;os_num= INT2STR(os_num(1:*),4)
    os_lp= os_lp(1:*)
    os_tel= os_tel(1:*)
    os_expt= os_expt(1:*)
    os_camt= os_camt(1:*)
    os_fw= os_fw(1:*)
    os_pw= os_pw(1:*)
    os_exptime= os_exptime(1:*)
    os_led= os_led(1:*)
    os_ipt= os_ipt(1:*)
    os_cad= os_cad(1:*)
    os_seq_cntr= os_seq_cntr(1:*)
    os_fps= os_fps(1:*)
    os_sync= os_sync(1:*)
    img_cntr= img_cntr(1:*)
    os_sdt= os_sdt(1:*)
    os_edt= os_edt(1:*)
    os_apid= os_apid(1:*)
    os_bsf= os_bsf(1:*)
    os_rsf= os_rsf(1:*)

    CLOSE,in
    FREE_LUN,in

    ; Use start of commanded exp.time as start of image (instead of start of setup time):

;    pexpt= GET_PRE_EXP_TIMES(os_sdt,os_lp,os_tel,os_led)
;    os_sdt= pexpt

    ; 02/04/05 - discussed delay times with Dennis and decided on the following.
    ; Figure out if delay time to read in the schedule (for FSW) and see if it interfears with first
    ; scheduled image. Since scheudles are read in at midnight, the time of first image should be
    ; atleast midnight+delaytime. If it is not, push back all image to allow for the delay. This
    ; applies to both .rts and .ats files.
    ; Dennis also asked the T line have the same time as the B line. 

; NOTE: Moved delay checking to schedule_event.pro
;
;    tot_img= 0L
;    FOR scntr= 0, N_ELEMENTS(os_seq_cntr)-1 DO BEGIN
;      toks= STR_SEP(os_seq_cntr(scntr),',')
;      tot_img= tot_img + (LONG(toks(N_ELEMENTS(toks)-1)) > 1L) ; add one if seq_cntr='' (for lp7, lp11)
;    ENDFOR
;
;    tot_img= N_ELEMENTS(EXPAND_SEQ(os_arr))+2 ; 2 for B and T lines.
;
;    ; get the delay time (FSW schedule read), nimage_per_sec from save file, give number of images for which
;    ; 1 second delay is required. Use it to figure out total delay for all images in current
;    ; BSF file that is being generated:
;    delay= 5  ; default (so start of first image in schedule should be midnight+5 or greater).
;    RESTORE,GETENV('PT')+'/IN/OTHER/images_delay_per_sec.sav' ; => nimage_per_sec
;    IF (nimage_per_sec GT 0) THEN delay= FIX(tot_img/nimage_per_sec + 0.99)
;    ;first_time= UTC2TAI(IPT_DATES2ECS(MIN(os_sdt)))
;    first_time= STR2UTC(IPT_DATES2ECS(MIN(os_sdt)))
;    IF (first_time.time/1000 LT delay) THEN BEGIN
;     ; if first command starts before delay, shift everything by delay-first_time:
;      tais= UTC2TAI(IPT_DATES2ECS(os_sdt))+(delay-first_time.time/1000)
;      os_sdt= ECS_DATES2IPT(TAI2UTC(tais,/ECS))
;    ENDIF


    ; May 2005 - Dennis asked B and T times of .RTS (and .BSF) files bet set to the
    ; time it takes to read in the schedule (readtime) but to shift the whole schedule
    ; so that time of first observation starts at zero offset:

    IF (KEYWORD_SET(rts)) THEN BEGIN
      first_time= STR2UTC(IPT_DATES2ECS(MIN(os_sdt)))
      IF (first_time.time GT 0) THEN BEGIN
        tais= UTC2TAI(IPT_DATES2ECS(os_sdt))-(first_time.time/1000)
        os_sdt= ECS_DATES2IPT(TAI2UTC(tais,/ECS))
      ENDIF
    ENDIF

    ; Now sort by time:
    srt0=sort(os_sdt)
    nlines=n_elements(srt0)
    unq =uniq(os_sdt[srt0])
    IF n_elements(srt0) NE n_elements(unq) THEN BEGIN
    	message,'Non-unique time(s) found in schedule:',/info
	arr=intarr(nlines)+1
	arr[unq]=0
	print,os_sdt[srt0[where(arr)]]
stop
    	WIDGET_CONTROL, mdiag, SET_VALUE= 'Non-unique time found... waiting 10sec'
	print,string(7b)
    	wait,10
    ENDIF
    ;srt=srt0[unq]  	; this was a mistake - NR, 10/31/11
    ;srt= SORT(os_sdt+img_cntr)
    srt= SORT(os_sdt+string(img_cntr,'(i5.5)'))
    os_sdt=   os_sdt(srt)
    os_set= os_set(srt) ; AEE 2/12/04
    os_num= os_num(srt)
    os_lp= os_lp(srt)
    os_tel= os_tel(srt)
    os_expt= os_expt(srt)
    os_camt= os_camt(srt)
    os_fw= os_fw(srt)
    os_pw= os_pw(srt)
    os_exptime= os_exptime(srt)
    os_led= os_led(srt)
    os_ipt= os_ipt(srt)
    os_cad= os_cad(srt)
    os_seq_cntr= os_seq_cntr(srt)
    os_fps= os_fps(srt)
    os_sync= os_sync(srt)
    img_cntr= img_cntr(srt)
    os_edt= os_edt(srt)
    os_apid= os_apid(srt)
    os_bsf= os_bsf(srt)
    os_rsf= os_rsf(srt)

    ; Use S/C ID (AB) to set the synched:

    synched= WHERE(os_sync EQ 'AB', syncnt)
    os_sync= STRARR(N_ELEMENTS(os_sync))+'0'
    IF (syncnt GT 0) THEN os_sync(synched)= '1'
    
    ; At this point, all of the OSes to be scheduled, are in arrays and sorted by time.

    ; Now, re-assign img_cntr in case some image orders where switched using the commanded
    ; exp.time. This will make the img_cntr match the image taking times but it will no
    ; longer match the img_cntr in the .IPT file:
    ;
;
;    valid= WHERE(img_cntr NE '', gcnt)
;    IF (gcnt GT 0) THEN BEGIN
;      vcntr= img_cntr(valid)
;      img_cntr(valid)= vcntr(SORT(vcntr))    
;    ENDIF

    dates= STRMID(os_sdt,0,8) ; separate the date, yyyymmdd, part of the date-time.
    times= STRMID(os_sdt,9,6) ; separate the time, hhmmss, part of the date-time.

    sched_bytes= STRARR(N_ELEMENTS(os_sdt))

    ; Set bytes 0 - 4:

    IF (KEYWORD_SET(rts)) THEN BEGIN
      sched_bytes(0:*)= '---'
      udts= dates(UNIQ(dates))  ; only keep the unique dates (yyyymmdd) of the IPT file.
      utc= STR2UTC(STRMID(udts,0,4)+'-'+STRMID(udts,4,2)+'-'+STRMID(udts,6,2))  
      dayoffsets= (utc.mjd - utc(0).mjd) ; = 0, 1, 2, etc for each day. 
      FOR i= 0, N_ELEMENTS(udts)-1 DO BEGIN
       ind= WHERE(dates eq udts(i))
       sched_bytes(ind)= sched_bytes(ind) + STRING(dayoffsets(i),'(I2.2)')
      ENDFOR
    ENDIF ELSE BEGIN
      yrs= INDGEN(20)+launch_date
      syrs= [STRTRIM(yrs(0:9) MOD 2000 MOD 10, 2) , ['A','B','C','D','E','F','G','H','I','J']]
      year= FIX(STRMID(dates,0,4))
      ind=WHERE(yrs eq year(0))
      sched_bytes= sched_bytes + syrs((year-year(0))+ind(0)) ; add Y(ear) byte
      sched_bytes= sched_bytes + STRMID(dates,4,4)           ; add mmdd bytes
    ENDELSE   

    ; Set bytes 5 - 10:

    sched_bytes= sched_bytes + times  ; add hhmmss bytes

    ; Set byte 11 (Function Code):

    os_lp= FIX(os_lp)
    w= WHERE(os_lp EQ 0, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'E' ; Double Sky Image 
    w= WHERE(os_lp EQ 1, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'N' ; Normal Sky Image
    w= WHERE(os_lp EQ 2, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'D' ; Dark Image
    w= WHERE(os_lp EQ 3, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'L' ; LED Image
    w= WHERE(os_lp EQ 4, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'C' ; Continuous Image
    w= WHERE(os_lp EQ 5, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'S' ; SCIP Seq
    w= WHERE(os_lp EQ 6, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'H' ; HI Seq
    w= WHERE(os_lp EQ 7, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'F' ; Block Seq File
    tel= FIX(os_tel)
    w= WHERE(os_lp EQ 8, cnt) ; Door Close
    IF (cnt GT 0) THEN BEGIN
      x= WHERE(tel(w) EQ 1,cnt) & IF (cnt GT 0) THEN sched_bytes(w(x))= sched_bytes(w(x)) + '2' ; EUVI Door Close
      x= WHERE(tel(w) EQ 2,cnt) & IF (cnt GT 0) THEN sched_bytes(w(x))= sched_bytes(w(x)) + '4' ; COR1 Door Close
      x= WHERE(tel(w) EQ 3,cnt) & IF (cnt GT 0) THEN sched_bytes(w(x))= sched_bytes(w(x)) + '6' ; COR2 Door Close
    ENDIF 
    w= WHERE(os_lp EQ 9, cnt) ; Door Open
    IF (cnt GT 0) THEN BEGIN
      x= WHERE(tel(w) EQ 1,cnt) & IF (cnt GT 0) THEN sched_bytes(w(x))= sched_bytes(w(x)) + '1' ; EUVI Door Open 
      x= WHERE(tel(w) EQ 2,cnt) & IF (cnt GT 0) THEN sched_bytes(w(x))= sched_bytes(w(x)) + '3' ; COR1 Door Open 
      x= WHERE(tel(w) EQ 3,cnt) & IF (cnt GT 0) THEN sched_bytes(w(x))= sched_bytes(w(x)) + '5' ; COR2 Door Open 
    ENDIF
    w= WHERE(os_lp EQ 10, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'G' ; GT dumps

    w= WHERE(os_lp EQ 11, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'I' ;Init Timeline

    w= WHERE(os_lp EQ 12, cnt) & IF (cnt GT 0) THEN sched_bytes(w)= sched_bytes(w) + 'A' ;Run Script

    ; Bytes 0 - 11 are now set. Need the info for the rest of the bytes (for setting the "Byte count"
    ; and "Checksum" Bytes) first:

    sched_bytes_b4_byte_count= sched_bytes

    sched_bytes= STRARR(N_ELEMENTS(os_sdt))

    w= WHERE(os_lp GE 0 AND os_lp LE 6, wcnt)  ; Image taking OSes
    IF (wcnt GT 0) THEN BEGIN                                            ; (Bytes starting with 0)
      ;sched_bytes(w)= sched_bytes(w) + INT2STR(FIX(img_cntr(w)),4)        ; bytes 16-19 (Image Counter)
      sched_bytes(w)= sched_bytes(w) + os_tel(w)                         ; byte  20    (Instrument 1= EUVI) 
      sched_bytes(w)= sched_bytes(w) + os_num(w)                         ; bytes 21-24 (Observation ID)
      sched_bytes(w)= sched_bytes(w) + os_camt(w)                        ; byte  25    (Camera Setup Table)
      ;sched_bytes(w)= sched_bytes(w) + os_expt(w)                        ; byte  26    (Exposure Table) 
      ; AEE 4/26/04 - Set exp table for Dark and LED images to '-':
      exptables= os_expt(w)
      dl_ind= WHERE(os_lp(w) EQ 2 OR os_lp(w) EQ 3, dlcnt)
      IF (dlcnt GT 0) THEN exptables(dl_ind)='-'
      sched_bytes(w)= sched_bytes(w) +exptables                          ; byte  26    (Exposure Table)
      
      sched_bytes(w)= sched_bytes(w) + STRING(FIX(os_ipt(w)),'(i2.2)')   ; bytes 27-28 (Image Processing Table)
      ;sched_bytes(w)= sched_bytes(w) + INT2STR(FIX(os_ipt(w)),2)         ; bytes 27-28 (Image Processing
      sched_bytes(w)= sched_bytes(w) + os_fps(w)                         ; byte  29    (FPS 0=Off 1=On)
      sched_bytes(w)= sched_bytes(w) + os_sync(w)                        ; byte  30    (Sync 0=No 1=Yes)

      ; AEE 2/12/04 - Add the 4 digint set_id (os_set) to the end of the image taking command strings:
      ; AEE 4/26/04 - We decided to add the 4 digint set_id as bytes 31-34 of image taking commands:

      ;sched_bytes(w)= sched_bytes(w)+ STRING(os_set(w),'(I4.4)') ; always zeros (from .IPT).
      sched_bytes(w)= sched_bytes(w)+ STRING(os_set(w),'(I4.4)') ; Not any more.
      ;sched_bytes(w)= sched_bytes(w)+ INT2STR(os_set(w),4)        ; Not any more.

;      vind= WHERE(os_arr.set_id GT 0, vcnt)
;      IF (vcnt EQ 0) THEN BEGIN
;        sched_bytes(w)= sched_bytes(w) + '0000' ; set all image taking set_id's to zero
;      ENDIF ELSE BEGIN
;        os_dates= STRMID(os_sdt,0,4)+'/'+STRMID(os_sdt,4,2)+'/'+STRMID(os_sdt,6,2)+' '+$
;                  STRMID(os_sdt,9,2)+':'+STRMID(os_sdt,11,2)+':'+STRMID(os_sdt,13,2)
;        os_tais= UTC2TAI(STR2UTC(os_dates))
;        FOR j= 0, vcnt-1 DO BEGIN
;          sind= WHERE(os_num(w) EQ os_arr(vind(j)).os_num AND $
;                      os_tais(w) EQ os_arr(vind(j)).os_start, scnt)
;;          IF (scnt GT 0) THEN $
;;            sched_bytes(w(sind))= sched_bytes(w(sind)) + STRING(os_arr(vind(j)).set_id,'(i4.4)') $
;;          ELSE $
;;            sched_bytes(w(sind))= sched_bytes(w(sind)) + '0000'
;;          w(sind)= -1
;          IF (scnt GT 0) THEN BEGIN ; images belonging to lp=7 (block seq) with set_id > 0, will have scnt=0
;            sched_bytes(w(sind))= sched_bytes(w(sind)) + STRING(os_arr(vind(j)).set_id,'(i4.4)')
;            w(sind)= -1
;          ENDIF
;help,os_arr.set_id,vind,vcnt,sched_bytes(w)
;        ENDFOR
;        zind= WHERE(w NE -1, zcnt) ; get images for which set_id is zero.
;        IF (zcnt GT 0) THEN sched_bytes(w(zind))= sched_bytes(w(zind)) + '0000'
;      ENDELSE
;help,zcnt
;stop

    ENDIF

    ; Block Seq Files (Bytes 16-27):
    w= WHERE(os_lp EQ 7, wcnt) 
    FOR i= 0, wcnt-1 DO sched_bytes(w(i))= sched_bytes(w(i)) + STRMID(os_bsf(w(i)),STRLEN(os_bsf(w(i)))-12,12)

    ; Run Script Files (Bytes 16-27):
    w= WHERE(os_lp EQ 12, wcnt)
    ;FOR i= 0, wcnt-1 DO sched_bytes(w(i))= sched_bytes(w(i)) + STRMID(os_rsf(w(i)),STRLEN(os_rsf(w(i)))-12,12)
    FOR i= 0, wcnt-1 DO sched_bytes(w(i))= sched_bytes(w(i)) + STRMID(os_rsf(w(i)),RSTRPOS(os_rsf(w(i)),'.')-8,20)

    ; GT dumps (Bytes 16-20):

    w= WHERE(os_lp EQ 10, wcnt)
    gt_ind= w
    gt_cnt= wcnt
    IF (wcnt GT 0) THEN BEGIN
     ; calculate number of seconds between start and stop times
     st= os_sdt(w)
     st= STRMID(st,0,4)+'/'+STRMID(st,4,2)+'/'+STRMID(st,6,2)+' '+ $
         STRMID(st,9,2)+':'+STRMID(st,11,2)+':'+STRMID(st,13,2)
     et= os_edt(w)
     et= STRMID(et,0,4)+'/'+STRMID(et,4,2)+'/'+STRMID(et,6,2)+' '+ $
         STRMID(et,9,2)+':'+STRMID(et,11,2)+':'+STRMID(et,13,2)
     ;secs= FIX(UTC2TAI(STR2UTC(et)) - UTC2TAI(STR2UTC(st)))
     secs= LONG(UTC2TAI(STR2UTC(et)) - UTC2TAI(STR2UTC(st)))

     IF (KEYWORD_SET(rts)) THEN BEGIN
       IF (first_time.time GT 0) THEN secs= secs - (first_time.time/1000) 
     ENDIF

     sched_bytes(w)= sched_bytes(w) + STRING(secs,'(i4.4)') ; Bytes 16-19
     ;sched_bytes(w)= sched_bytes(w) + INT2STR(secs,4)        ; Bytes 16-19

     apid= os_apid(w)
     ; AEE 4/30/04 - Added all 5 APIDs:
     ;w1= WHERE(apid EQ 'SSR1', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'1'     ; SSR1
     ;w1= WHERE(apid EQ 'SSR2', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'2'     ; SSR2
     ;w1= WHERE(apid EQ 'RealTime', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'3' ; RT

     w1= WHERE(apid EQ 'RT-SSR1', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'1'  ; RT-SSR1
     w1= WHERE(apid EQ 'SSR1', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'2'     ; SSR1
     w1= WHERE(apid EQ 'SSR2', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'3'     ; SSR2
     w1= WHERE(apid EQ 'SpaceWthr', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'4' ; SW
     w1= WHERE(apid EQ 'GndTest', cnt) & IF (cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'5' ; GndTest
    ENDIF

    ; Dark Images (Bytes 35-39):
    ; Note: only for dark images exposure time is passed as part of upload command. But, since
    ;       for dark images there are no MEB (mech. electronic box) involved (not even shutter
    ;       is moved) the units of exposure times from .IPT file are the physical units and do not 
    ;       need to divide them to 1.024 units for SCIP. For HI Darks, don't divide exp. time by 
    ;       2.0 either since, again, the physical units of milliseconds should be passed along:

    w= WHERE(os_lp EQ 2, wcnt)
    IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+STRING(LONG(os_exptime(w)),'(i5.5)')
    ;IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+INT2STR(LONG(os_exptime(w)),5)

    ; HI Seq (Bytes 3-39):
    w= WHERE(os_lp EQ 6, wcnt)
    FOR i=0, wcnt-1 DO BEGIN
      toks= STRTRIM(STR_SEP(os_seq_cntr(w(i)),','),2)
      seqcnt= toks(N_ELEMENTS(toks)-1)
      sched_bytes(w(i))= sched_bytes(w(i))+ STRING(seqcnt,'(I2.2)')
    ENDFOR
    IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+STRING(os_cad(w),'(i3.3)') ; AEE 4/5/04
    ;IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+INT2STR(os_cad(w),3)

    ; Continuous Images (Bytes 35-36) ; EUVI=1, COR1=2, COR2=3, HI1=4, HI2=5
    w= WHERE(os_lp EQ 4 AND FIX(os_tel) LE 3, wcnt)
    IF (wcnt GT 0) THEN BEGIN
      w1= WHERE(FIX(os_tel(w)) EQ 1, w1cnt)  ; EUVI
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+os_fw(w(w1))+os_pw(w(w1))
      w1= WHERE(FIX(os_tel(w)) GT 1, w1cnt)  ; COR1 and COR2
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+STRING(FIX(os_pw(w(w1))),'(i2.2)')
      ;IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+INT2STR(FIX(os_pw(w(w1))),2)
    ENDIF
    w= WHERE(os_lp EQ 4 AND FIX(os_tel) GE 4, wcnt) ; HI 1 and 2
    IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+'--'

    ; Normal Images (Bytes 35-36):  (same as Continuous Images.)
    w= WHERE(os_lp EQ 1 AND FIX(os_tel) LE 3, wcnt)
    IF (wcnt GT 0) THEN BEGIN
      w1= WHERE(FIX(os_tel(w)) EQ 1, w1cnt)  ; EUVI
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+os_fw(w(w1))+os_pw(w(w1))
      w1= WHERE(FIX(os_tel(w)) GT 1, w1cnt)  ; COR1 and COR2
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+STRING(FIX(os_pw(w(w1))),'(i2.2)')
      ;IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+INT2STR(FIX(os_pw(w(w1))),2)
    ENDIF
    w= WHERE(os_lp EQ 1 AND FIX(os_tel) GE 4, wcnt) ; HI 1 and 2
    IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+'--'

    ; Double Exposure Images (Bytes 35-38):
    w= WHERE(os_lp EQ 0 AND FIX(os_tel) LE 3, wcnt)
    IF (wcnt GT 0) THEN BEGIN
      fw= STRTRIM(os_fw(w),2)
      pw1= STRARR(wcnt)
      pw2= STRARR(wcnt)
      FOR i=0, wcnt-1 DO BEGIN
        pw12= STRTRIM(STR_SEP(os_pw(w(i)),','),2)
        pw1(i)= pw12(0)
        pw2(i)= pw12(1)
      ENDFOR
      w1= WHERE(FIX(os_tel(w)) EQ 1, w1cnt)  ; EUVI
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+fw(w1)+pw1(w1)+fw(w1)+pw2(w1)
      w1= WHERE(FIX(os_tel(w)) GT 1, w1cnt)  ; COR1 and COR2
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+STRING(FIX(pw1(w1)),'(i2.2)')+ $
                                                                  STRING(FIX(pw2(w1)),'(i2.2)')
      ;IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+INT2STR(FIX(pw1(w1)),2)+ $
      ;                                                            INT2STR(FIX(pw2(w1)),2)
    ENDIF
    w= WHERE(os_lp EQ 0 AND FIX(os_tel) GE 4, wcnt) ; HI 1 and 2
    IF (wcnt GT 0) THEN sched_bytes(w)= sched_bytes(w)+'----'

;LED Images (Bytes 35-47):
    w= WHERE(os_lp EQ 3, wcnt)
    IF (wcnt GT 0) THEN BEGIN
      pulses= os_exptime(w)
      w1= WHERE(FIX(os_tel(w)) EQ 1, w1cnt) ; EUVI
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+os_fw(w(w1))+os_pw(w(w1))
      w1= WHERE(FIX(os_tel(w)) GT 1 AND FIX(os_tel(w)) LT 4, w1cnt) ; COR1 and COR2
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+STRING(FIX(os_pw(w(w1))),'(i2.2)')
      ;IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+INT2STR(FIX(os_pw(w(w1))),2)
      w1= WHERE(FIX(os_tel(w)) GE 4, w1cnt) ; HI 1 and 2
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'--'

      leds= STRTRIM(os_led(w),2)
      sched_bytes(w)= sched_bytes(w)+leds
      sched_bytes(w)= sched_bytes(w)+STRING(LONG(pulses),'(i7.7)')
      ;sched_bytes(w)= sched_bytes(w)+INT2STR(LONG(pulses),7)
    ENDIF 


    ; SCIP Seq (Bytes 35-47):

    w= WHERE(os_lp EQ 5, wcnt)
    IF (wcnt GT 0) THEN BEGIN
      w1= WHERE(FIX(os_tel(w)) EQ 1, w1cnt) ; EUVI
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+os_fw(w(w1))
      w1= WHERE(FIX(os_tel(w)) GT 1, w1cnt) ; COR1 and COR2
      IF (w1cnt GT 0) THEN sched_bytes(w(w1))= sched_bytes(w(w1))+'-' 
      FOR i=0, wcnt-1 DO BEGIN
        toks= STRTRIM(STR_SEP(os_seq_cntr(w(i)),','),2)
        seqcnt= toks(N_ELEMENTS(toks)-1)
        sched_bytes(w(i))= sched_bytes(w(i))+ STRING(seqcnt,'(I1.1)')
        sched_bytes(w(i))= sched_bytes(w(i))+ STRING(os_cad(w(i)),'(I3.3)') ; AEE 4/23/04 added.
        pw= STR_SEP(os_pw(w(i)),',')
        FOR j=0, N_ELEMENTS(pw)-1 DO sched_bytes(w(i))= sched_bytes(w(i))+STRING(pw(j),'(I2.2)')
        ; IF less than 4 images in seq, add dashes for the rest:
        FOR k=j, 3 DO sched_bytes(w(i))= sched_bytes(w(i))+'--' 
      ENDFOR
    ENDIF   

    byte_cnt= strlen(sched_bytes_b4_byte_count)+strlen(sched_bytes)+4  ; +4 => 2 for byte-count & 2 for check-sum)

    ; Add byte count after the Function code (2 Bytes):
    sched_bytes_b4_byte_count= sched_bytes_b4_byte_count+STRING(byte_cnt,'(I2.2)')
    ;sched_bytes_b4_byte_count= sched_bytes_b4_byte_count+INT2STR(byte_cnt,2)

    ; Figure out the chksum for each command and add it after the byte count (2 Bytes):
    FOR i=0, N_ELEMENTS(sched_bytes_b4_byte_count)-1 DO BEGIN
      cmdstr= sched_bytes_b4_byte_count(i)+sched_bytes(i)
      n= STRLEN(cmdstr)
      chksum= BYTARR(n) 
      FOR j=0, n-1 DO chksum(j)= BYTE(STRMID(cmdstr,j,1))
      sched_bytes_b4_byte_count(i)= sched_bytes_b4_byte_count(i)+STRUPCASE(STRING(TOTAL(chksum) MOD 256,'(z2.2)'))
    ENDFOR

    ; Now add the 2 sections of sched_bytes together to form the final commands:

    itos_commands= sched_bytes_b4_byte_count+sched_bytes

;************************************************

    IF (KEYWORD_SET(rts)) THEN $
      itos_file= ipt_fname+'.RTS' $
    ELSE $
      itos_file= ipt_fname+'.ATS'


    IF (KEYWORD_SET(bsf)) THEN BEGIN
      GET_UTC, utime 
      dte= STR_SEP(UTC2STR(utime,/ECS,/DATE_ONLY),'/')
      ecnt= 1 
      bsfile= GETENV('PT')+'/IN/BSF/'+STRMID(dte(0),2,2)+dte(1)+dte(2)+'_'+STRTRIM(ecnt,2)+'.bsf'
      ff= FINDFILE(bsfile)
      WHILE (ff(0) NE '') DO BEGIN
        ecnt= ecnt+1
        bsfile= GETENV('PT')+'/IN/BSF/'+STRMID(dte(0),2,2)+dte(1)+dte(2)+'_'+STRTRIM(ecnt,2)+'.bsf'  
        ff= FINDFILE(bsfile)
      ENDWHILE
      OPENW,out,bsfile,/GET_LUN
    ENDIF ELSE BEGIN
      ;OPENW,out,GETENV('PT')+'/OUT/UPLOAD/'+itos_file,/GET_LUN
      OPENW,out,GETENV('PT')+'/OUT/'+sc+'/UPLOAD/'+itos_file,/GET_LUN
    ENDELSE

tbldir=getenv('TABLES')
    IF (sc EQ 'SC_A') THEN BEGIN
    ;  SPAWN,"grep -h 'Id:' "+tbldir+"/*a.img",sca_tables
    ;  SPAWN,"grep -h 'Id:' "+tbldir+"/imagetbl.img",ip_tables
    ;  tables_used= [ip_tables,sca_tables]
    ;  tables_used= ';'+strmid(tables_used,2,100) ; remove first $ (second char) from each string.
    ;  tables_used= [';PT Schedule = '+sc+'/UPLOAD/'+itos_file,tables_used]
    ;  FOR tb= 0, N_ELEMENTS(tables_used)-1 DO PRINTF, out, '#'+tables_used(tb)
      tables= tbAout
    ENDIF
    IF (sc EQ 'SC_B') THEN BEGIN
      SPAWN,"grep -h 'Id:' "+tbldir+"/*b.img",scb_tables
    ;  SPAWN,"grep -h 'Id:' "+tbldir+"/imagetbl.img",ip_tables
    ;  tables_used= [ip_tables,scb_tables]
    ;  tables_used= ';'+strmid(tables_used,2,100) ; remove first $ (second char) from each string.
    ;  tables_used= [';PT Schedule = '+sc+'/UPLOAD/'+itos_file,tables_used]
    ;  FOR tb= 0, N_ELEMENTS(tables_used)-1 DO PRINTF, out, '#'+tables_used(tb)
      tables= tbBout
    ENDIF

    PRINTF, out, '#'+';PT Schedule = '+sc+'/UPLOAD/'+itos_file
    FOR tb= 0, N_ELEMENTS(tables)-1 DO PRINTF, out, '#'+tables(tb)

    beg_cmd= STRMID(itos_commands(0),0,11)+'B16'
    ; Set RTS (and BSF) beg command to FSW schedule read time:
    IF (KEYWORD_SET(rts)) THEN BEGIN 
      ;rts_hr= STRING(readtime/3600,'(I2.2)')
      ;rts_mn= STRING((readtime MOD 3600)/60,'(I2.2)')
      ;rts_sc= STRING((readtime MOD 3600) MOD 60,'(I2.2)')
      ;beg_cmd= STRMID(itos_commands(0),0,5)+rts_hr+rts_mn+rts_sc+'B16'

      ; use first_time (since a none image such as GT, initTL, etc. may be the first in schedule)
      ; instead of readtime:
      fstime= first_time.time/1000
      rts_hr= STRING(fstime/3600,'(I2.2)')
      rts_mn= STRING((fstime MOD 3600)/60,'(I2.2)')
      rts_sc= STRING((fstime MOD 3600) MOD 60,'(I2.2)')
      beg_cmd= STRMID(itos_commands(0),0,5)+rts_hr+rts_mn+rts_sc+'B16'
    ENDIF ELSE BEGIN 
      ; For a .ATS file, set B and T commands to dummy RTS format of '---00000000'. 
      ; B is not used by flight software and if .ATS file contains a .bsf file,
      ; the .ATS style B line causes problems processing the .bsf file. 
      beg_cmd= '---00000000B16'
    ENDELSE

    n= STRLEN(beg_cmd)
    chksum= BYTARR(n)
    FOR j=0, n-1 DO chksum(j)= BYTE(STRMID(beg_cmd,j,1))
    
    PRINTF,out,beg_cmd+STRUPCASE(STRING(TOTAL(chksum) MOD 256,'(z2.2)'))

    FOR i=0, N_ELEMENTS(itos_commands)-1 DO PRINTF,out,itos_commands(i) 

    ; keep GT-dump commands to generate single ipt entries of .bsf files, if any:
    IF (gt_cnt GT 0) THEN gt4ipt_names= itos_commands(gt_ind) 

    ;end_cmd= STRMID(itos_commands(N_ELEMENTS(itos_commands)-1),0,11)+'T16'
    ; Dennis asked end_cmd have the same time as the beg_cmd:
    end_cmd= STRMID(itos_commands(0),0,11)+'T16' 

    ; Set RTS (and BSF) end command to FSW schedule read time:
    ;IF (KEYWORD_SET(rts)) THEN end_cmd= STRMID(itos_commands(0),0,5)+rts_hr+rts_mn+rts_sc+'T16'

    ; set end_cmd to beg_cmd:
    end_cmd= STRMID(beg_cmd,0,11)+'T16'

    n= STRLEN(end_cmd)
    chksum= BYTARR(n)
    FOR j=0, n-1 DO chksum(j)= BYTE(STRMID(end_cmd,j,1))
    PRINTF,out,end_cmd+STRUPCASE(STRING(TOTAL(chksum) MOD 256,'(z2.2)'))

    CLOSE,out
    FREE_LUN,out

    PRINT,''
    IF (KEYWORD_SET(bsf)) THEN BEGIN
      PRINT,'Created ../IN/'+STRMID(bsfile,STRPOS(bsfile,'BSF'),20)
      WIDGET_CONTROL, mdiag, SET_VALUE= 'Created ../IN/'+STRMID(bsfile,STRPOS(bsfile,'BSF'),20)
    ENDIF ELSE BEGIN
      PRINT,'Created ../OUT/'+sc+'/UPLOAD/'+itos_file
      WIDGET_CONTROL, mdiag, SET_VALUE= 'Created ../OUT/'+sc+'/UPLOAD/'+itos_file

    ENDELSE
    PRINT,''

  ENDELSE 

  RETURN
END
