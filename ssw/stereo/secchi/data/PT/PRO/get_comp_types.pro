;+
;$Id: get_comp_types.pro,v 1.10 2009/09/11 20:28:11 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GET_COMP_TYPES
;
; Purpose     : Returns descriptions of compression steps used by an OS.
;
; Use         : comp_type= GET_COMP_TYPES(os)
;
; Inputs      : os         Observation for which compression descriptions are needed,
;
; Opt. Inputs : None
;
; Outputs     : comp_type  Compression string - 1 description  per compression (header only,
;                          no compression, rice, etc.).
;
; Opt. Outputs: None
;
; Keywords    :
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 07/13/04 Added new IP function "Send Raw Image" (function/step #44).
;              Ed Esfandiari 10/06/05 Added ICER IP functions (steps #82 to 93).
;              Ed Esfandiari 11/09/05 icer functions changed to steps #90 to 101.
;              Ed Esfandiari 08/14/09  Changed H-compress range from 5-17 to 5-14.
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15. Treat HISAM as no-compression.
;
;
; $Log: get_comp_types.pro,v $
; Revision 1.10  2009/09/11 20:28:11  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.7  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.6  2005/10/17 19:02:09  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.5  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION GET_COMP_TYPES, os
COMMON DIALOG, mdiag,font

   all_comp_types = READ_IP_DAT()

   comp_type = ''

   iptable = os.iptable

   ip = os.ip(iptable)

   ;comps= all_comp_types(ip.steps(where(ip.steps GE 5 and ip.steps LE 17))).ip_description
   ;comps= all_comp_types(ip.steps(where(ip.steps GE 5 and ip.steps LE 17 OR $
   ;comps= all_comp_types(ip.steps(where(ip.steps GE 5 and ip.steps LE 14 OR $
   comps= all_comp_types(ip.steps(where(ip.steps GE 5 and ip.steps LE 15 OR $
                                        ip.steps GE 90 AND ip.steps LE 101 OR $
                                        ip.steps EQ 44))).ip_description
   apids= all_comp_types(ip.steps(where(ip.steps GE 39 and ip.steps LE 43))).ip_description ; AEE 5/3/04
   FOR i= 0, N_ELEMENTS(comps)-1 DO BEGIN
     apid= STRMID(apids(i),4,STRLEN(apids(i))-8) 
     IF (STRPOS(apid,'Space') GE 0) THEN apid= 'SW'

     comp_type= comp_type+', '+comps(i)+' ('+STRTRIM(apid,2)+')'
   ENDFOR 

   IF (STRLEN(comp_type) GT 0) THEN comp_type= STRMID(comp_type,2,200)

   RETURN, comp_type

END
