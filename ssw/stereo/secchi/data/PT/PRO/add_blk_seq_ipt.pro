;+
;$Id: add_blk_seq_ipt.pro,v 1.9 2009/09/11 20:28:06 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ADD_BLK_SEQ_IPT
;               
; Purpose     : Reads a block sequence file and add its entries to a expanded .IPT file.
;               
; Use         : ADD_BLK_SEQ_IPT, lun, bsfile, start_time, sc
;    
; Inputs      : lun          Logical unit number of the .IPT file to be expanded with bsfile.
;               bsfile       Block sequence file to use.
;               start_time   Start time of first entry in bsf file.
;               sc           SpaceCraft ID for which this .BSF file applies.
;
; Opt. Inputs : None
;               
; Outputs     : None 
;
; Opt. Outputs: None
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/06/04 - Added SC dependency.
;              Ed Esfandiari 02/03/05 - Added code to handle Init Timeline commands read 
;                                       from bsf file. 
;              Ed Esfandiari 05/03/05 - Added code to handle Run Script commands read from
;                                       bsf file.
;              Ed Esfandiari 05/10/05 - Allow multiple Init timeline entries.
;              Ed Esfandiari 05/11/05 - Allow multiple Run Script entries.
;              Ed Esfandiari 04/21/06 - Read-off comment lines (start with #) at top of the file.
;              Ed Esfandiari 05/09/06 - Allow 8.* filenames for Run Scripts.
;              Ed Esfandiari 12/29/06 - Add GT-dumps (G command) code to match schedule_event.
;              Ed Esfandiari 06/20/07 - Changes to handle bsf files thtat don't have any images.
;
; $Log: add_blk_seq_ipt.pro,v $
; Revision 1.9  2009/09/11 20:28:06  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/05/26 20:00:57  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.4  2005/03/10 16:36:45  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:57  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;
PRO ADD_BLK_SEQ_IPT, in_lun, bsfile, start_time, sc
COMMON CMD_SEQ_SCHEDULED   ; AEE - 5/22/03
COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt
COMMON SCHED_SHARE, schedv
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt,bsf_gt
COMMON OS_ALL_AB, remain_os_arr, all_defined_os_arr, all_defined_set_arr, all_sched_cmdseq

; This pro added os_num sections for each lp=7 index.
; Note: the code in this routine should be the same as those in schedule_event.pro

   delay= 0.0D
   tai_stime= UTC2TAI(STR2UTC(start_time))  ; change to seconds

   ; Keep track of the block seq commands as they are expanded from the input IPT file

   cmdseq= {cmdseq,bsfile:'',start_time:'',os_num:'',os_time:'', sc:'', bsf:''}
   cmdseq.bsfile= bsfile
   cmdseq.start_time= start_time
   cmdseq.sc= sc  ; sc is from PT_LP_SC line of block seq section (lp=7) of .IPT file.
   IF (sc EQ 'A') THEN cmdseq.bsf= 'A'+STRTRIM(a_bsf_cnt,2) ; bsf_cnt incremented in expand_ipt
   IF (sc EQ 'B') THEN cmdseq.bsf= 'B'+STRTRIM(b_bsf_cnt,2)

   ;OPENR, bsf_lun, GETENV('PT')+'/IN/BSF/'+bsfile, /GET_LUN
   OPENR, bsf_lun, bsfile, /GET_LUN

    entry=''

    ; read-off comment lines, if any, and begin line:
    READF,bsf_lun, entry
    WHILE (STRMID(entry,0,1) EQ '#') DO READF,bsf_lun, entry 

    ;READF,bsf_lun, entry ; read off the "Start" line (start of file must be an entry with a B function code) .
    IF (STRMID(entry,11,1) NE 'B') THEN BEGIN
      PRINT,''
      PRINT,'ERROR: '+bsfile+' Does NOT start with a "Start Timeline" entry.'
      PRINT,''
      STOP
    ENDIF

    ; Grab BSF offset time from 'B' command line, if any, to add to schedule offset:
    GET_BSF_ENTRY_INFO,entry,osnum,delay,fname
    bsf_offset= delay

    READF,bsf_lun, entry 
    ; bsfile must end with a End (function code T) line.
    WHILE (NOT(EOF(bsf_lun)) AND STRMID(entry,11,1) NE 'T') DO BEGIN  ; bsfile should end with a "End" line.
      GET_BSF_ENTRY_INFO,entry,osnum,delay,fname
      delay= bsf_offset+delay

      IF (STRMID(entry,11,1) EQ 'G') THEN BEGIN
         ; A GT-dump (G) command (handle differetly than an OS command):
         ; Note: For each S/C, check input .bsf file for Run Script (lp=10)
         ; commands. We keep all G commands. If an G command belongs to a bsf file,
         ; mark it as such so that it is not written separetly in the output files.

        gtfn= GETENV('PT')+'/IO/OSID/'+STRMID(entry,3,20)+'.gt'
        str_arr= ''
        OPENR, gtlun, gtfn, /GET_LUN
        str= ''
        WHILE ( NOT(EOF(gtlun))) DO BEGIN
          READF, gtlun, str
          str = STRTRIM(str, 2)
          ;** throw out comments and blank lines now
          IF ( (STRMID(str,0,1) NE ';') AND (str NE '') ) THEN str_arr = [str_arr, str]
        ENDWHILE
        CLOSE, gtlun
        FREE_LUN, gtlun
        str_arr= str_arr(1:*)
        key_val_arr = STR_ARR2KEY_VAL(str_arr)
        IF (DATATYPE(stime_gt) EQ 'UND') THEN $
          gt_ind= 0 $
        ELSE        $
          gt_ind= N_ELEMENTS(stime_gt)
        ADD_IPT_GT,key_val_arr
        ; Adjust GT-dump times:
        gt_dur= etime_gt(gt_ind) - stime_gt(gt_ind)
        stime_gt(gt_ind)= tai_stime+delay
        etime_gt(gt_ind)= tai_stime+delay+gt_dur

        IF (schedv.sc EQ 1) THEN BEGIN 
          bsf_gt(gt_ind)= 'A'+STRTRIM(a_bsf_cnt,2) 
        ENDIF
        IF (schedv.sc EQ 2) THEN BEGIN
          bsf_gt(gt_ind)= 'B'+STRTRIM(b_bsf_cnt,2)
        ENDIF
        IF (schedv.sc EQ 0) THEN BEGIN
          IF (sc_gt(gt_ind) EQ 'A') THEN $
            bsf_gt(gt_ind)= 'A'+STRTRIM(a_bsf_cnt,2) $
          ELSE $
            bsf_gt(gt_ind)= 'B'+STRTRIM(b_bsf_cnt,2)
        ENDIF

     ENDIF ELSE BEGIN
      IF (STRMID(entry,11,1) EQ 'A') THEN BEGIN
        ; A Run Script command (handle differetly than an OS command):

        ; Must add the A command (from .bsf file) to the expanded ipt file so that if shift is 
        ; applied, the A command is shifted, too: 

        new_tl= tai_stime + delay
        stm= UTC2STR(TAI2UTC(new_tl),/ECS)
        PRINTF, in_lun, 'PT_OS_NUM = 0004'
        PRINTF, in_lun, 'PT_LP_NUM = 12'
        PRINTF, in_lun, 'PT_LP_NSCHED = 1'
        gstart= 'PT_LP_START = ('+STRMID(stm,0,4)+STRMID(stm,5,2)+STRMID(stm,8,2)+'_'+ $
                                  STRMID(stm,11,2)+STRMID(stm,14,2)+STRMID(stm,17,2)+')'
        gsc   = 'PT_LP_SC    = '+sc
        PRINTF, in_lun, gstart
        PRINTF, in_lun, gsc
        ;PRINTF, in_lun, 'PT_RSF = '+STRMID(entry,STRLEN(entry)-12,12)
        PRINTF, in_lun, 'PT_RSF = '+STRMID(entry,RSTRPOS(entry,'.')-8,20)
        IF (sc EQ 'A') THEN $
          PRINTF, in_lun, 'PT_BSF_CNT = '+STRTRIM(a_bsf_cnt,2) $
        ELSE $
          PRINTF, in_lun, 'PT_BSF_CNT = '+STRTRIM(b_bsf_cnt,2)
        PRINTF, in_lun, ';'
        PRINTF, in_lun, ';'
      ENDIF ELSE BEGIN
       IF (STRMID(entry,11,1) EQ 'I') THEN BEGIN
        ; An Init Timeline command (handle differetly than an OS command):

        ; Must add the I command (from .bsf file) to the expanded ipt file so that if shift is applied, the
        ; I command is shifted, too: 

        new_tl= tai_stime + delay
        stm= UTC2STR(TAI2UTC(new_tl),/ECS)
        PRINTF, in_lun, 'PT_OS_NUM = 0003'
        PRINTF, in_lun, 'PT_LP_NUM = 11'
        PRINTF, in_lun, 'PT_LP_NSCHED = 1'
        gstart= 'PT_LP_START = ('+STRMID(stm,0,4)+STRMID(stm,5,2)+STRMID(stm,8,2)+'_'+ $
                                  STRMID(stm,11,2)+STRMID(stm,14,2)+STRMID(stm,17,2)+')'
        gsc   = 'PT_LP_SC    = '+sc
        PRINTF, in_lun, gstart
        PRINTF, in_lun, gsc
        IF (sc EQ 'A') THEN $ 
          PRINTF, in_lun, 'PT_BSF_CNT = '+STRTRIM(a_bsf_cnt,2) $
        ELSE $
          PRINTF, in_lun, 'PT_BSF_CNT = '+STRTRIM(b_bsf_cnt,2) 
        PRINTF, in_lun, ';'
        PRINTF, in_lun, ';'
       ENDIF ELSE BEGIN
        IF (cmdseq.os_num EQ '') THEN BEGIN
          cmdseq.os_num= osnum 
          cmdseq.os_time= UTC2STR(TAI2UTC(tai_stime + delay),/ECS)
        ENDIF ELSE BEGIN
          cmdseq.os_num= cmdseq.os_num+','+osnum
          cmdseq.os_time= cmdseq.os_time+','+UTC2STR(TAI2UTC(tai_stime + delay),/ECS)
        ENDELSE
 
        str= ''
        OPENR, os_ipt, GETENV('PT')+'/IO/OSID/'+fname, /GET_LUN
 
        WHILE (NOT(EOF(os_ipt))) DO BEGIN
          READF, os_ipt, str
          IF (STRPOS(str,'PT_LP_START') GE 0) THEN BEGIN
            dt= UTC2STR(TAI2UTC(tai_stime + delay),/ECS) ;=> dt="yyyy/mm/dd hh:mm:ss.mmm"
            dt= STR_SEP(dt,' ')    ; => dt=["yyyy/mm/dd","hh:mm:ss.mmm"]
            dd= STR_SEP(dt(0),'/') ; => dd= [yyyy,mm,dd]
            dd= dd(0)+dd(1)+dd(2)  ; => dd= "yyyymmdd"
            tt= STR_SEP(dt(1),'.') ; => tt= [hh:mm:ss,mmm]
            tt= STR_SEP(tt(0),':') ; => tt= [hh,mm,ss]
            tt= tt(0)+tt(1)+tt(2)  ; => tt= "hhmmss" 
            PRINTF, in_lun, 'PT_LP_START = (' + dd + '_'+ tt + ')'
          ENDIF ELSE BEGIN
            ; Pickup number of scheduled Oses from PT_LP_NSCHED line:
            IF (STRPOS(str,'PT_LP_NSCHED') GE 0) THEN ns= FIX((STR_SEP(str,'='))(1))
            ; Set the SpaceCraft for this OS (ignore PT_SC_ID of ../IO/OSID/OS_xxxx.IPT file):
            IF (STRPOS(str,'PT_SC_ID') GE 0) THEN $
            str= 'PT_SC_ID = ('+ARR2STR(REPLICATE(sc,ns))+')'
            PRINTF, in_lun, str 
          ENDELSE
        
        ENDWHILE
        PRINTF, in_lun,'PT_BSF_ID= '+cmdseq.bsf  ; so that schedule_read_ipt[_fst].pro pick them up.
   
        CLOSE, os_ipt
        FREE_LUN, os_ipt 
       ENDELSE
      ENDELSE
     ENDELSE
      READF,bsf_lun, entry
   ENDWHILE ; bsfile

   IF (EOF(bsf_lun) AND STRMID(entry,11,1) NE 'T') THEN BEGIN
     CLOSE, bsf_lun
     FREE_LUN, bsf_lun
     WIDGET_CONTROL,mdiag,SET_VALUE= $
       'Error: '+bsfile+' file does NOT end with a "End Timeline" entry - Block Sequence Canceled.'
     PRINT,''
     PRINT,'Error: '+bsfile+' file does NOT end with a "End Timeline" entry - Block Sequence Canceled.'
     PRINT,''
     STOP 
   ENDIF

   CLOSE, bsf_lun
   FREE_LUN, bsf_lun

    ; AEE - 6/17/2008 commented out to handle bsf files that don't have any images:
;   IF (STRLEN(cmdseq.os_num) EQ 0) THEN BEGIN
;     WIDGET_CONTROL,mdiag,SET_VALUE= 'Error: '+bsfile+' file is EMPTY - Block Sequence Canceled.'
;     PRINT,''
;     PRINT,'Error: '+bsfile+' file is EMPTY - Block Sequence Canceled.'
;     PRINT,''
;     STOP 
;   ENDIF


   IF (DATATYPE(sched_cmdseq) NE 'STC') THEN $
     sched_cmdseq= cmdseq $
   ELSE $
     sched_cmdseq= [sched_cmdseq,cmdseq]

   IF (DATATYPE(all_sched_cmdseq) NE 'STC') THEN $
     all_sched_cmdseq= cmdseq $
   ELSE $
     all_sched_cmdseq= [all_sched_cmdseq,cmdseq]

   RETURN

END
