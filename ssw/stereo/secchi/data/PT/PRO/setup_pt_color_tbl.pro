;+
;$Id: setup_pt_color_tbl.pro,v 1.3 2009/09/11 20:28:24 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : SETUP_PT_COLOR_TBL
;
; Purpose     : This pro sets up color table needed for Planning Tool displays.
;
; Use         : SETUP_PT_COLOR_TBL
;
; Inputs      : None

; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. :
; Written by  : Ed Esfandiari, NRL, Dec 2007 - First Version. Based on color setting of PT.
;
; Modification History:
;
; $Log: setup_pt_color_tbl.pro,v $
; Revision 1.3  2009/09/11 20:28:24  esfand
; use 3-digit decimals to display volumes
;
;
;-


PRO SETUP_PT_COLOR_TBL

 LOADCT, 0, /SILENT
 TVLCT, 255,0,0, 1      ;** load red into entry 1
 TVLCT, 0,255,0, 2      ;** load green into entry 2
 TVLCT, 142,229,238, 3  ;** load blue into entry 3
 TVLCT, 255,255,0,4     ;** load yellow into entry 4
 TVLCT, 204,117,89, 5   ;** load brown into entry 5    ;AEE changed here and in mask.pro and mask_old.pro
 TVLCT, 200,0,0, 6      ;** load dull red into entry 6
 TVLCT, 0,200,0, 7      ;** load dull green into entry 7
 TVLCT, 0,206,237, 8    ;** load dull blue into entry 8
 TVLCT, 200,200,0, 9    ;** load dull yellow into entry 9
 TVLCT, 180,95,85, 10   ;** load dull dark brown into entry 10
 TVLCT, 142,229,238, 11  ;** load light blue into entry 11 - used by mask.pro

 RETURN
END
