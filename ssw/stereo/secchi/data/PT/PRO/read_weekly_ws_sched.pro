;+
;$Id: read_weekly_ws_sched.pro,v 1.5 2009/09/11 20:28:21 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : READ_WEEKLY_WS_SCHED
;
; Purpose     : Read a moc_sds ahead or behind weekly schedule file (such as
;               ahead_2006_310_04.ws) and generates a .SCA activity file 
;               (such as ahead_2006_310_1106_1112.SCA).
;               Weekly *.ws schedules can be downloaded from 
;               http://stereo-ssc.nascom.nasa.gov/data/moc_sds/ahead/data_products/weekly_schedule/
;               or
;               http://stereo-ssc.nascom.nasa.gov/data/moc_sds/behind/data_products/weekly_schedule/
;               and also in
;               /net/venus/secchi3/sds/ahead/data_products/weekly_schedule/
;               or
;               /net/venus/secchi3/sds/behind/data_products/weekly_schedule/
;
;
; Use         : READ_WEEKLY_WS_SCHED, ws 
;
; Inputs      : ws   A .ws schedule filename including full path (string).
;
; Opt. Inputs : None
;
; Outputs     : *.SCA file in the same directory as input path.
;
; Opt. Outputs: None
;
; Side Effects: Creates a .SCA output file.
;
; Exampe: 
;         READ_WEEKLY_WS_SCHED, '/home/esfand/secchi/PT1/IN/SCA/SC_A/ahead_2006_310_04.ws'
;         =>
;         /home/esfand/secchi/PT1/IN/SCA/SC_A/ahead_2006_310_1106_1112.SCA
;
; Prev. Hist. : None 
;
; Written by  : Ed Esfandiari, NRL, May 2006 - First Version.
;
; Modification History:
;               Ed Esfandiari  10/02/06 - changed wfn check of GE 0 (instead of GT 0).
;               Ed Esfandiari  01/23/07 - Added " RT Stop SSR Playback  " check.
;               Ed Esfandiari  05/24/07 - Check dur(ation) for varios Momentum Dump descriptions.
;               Ed Esfandiari  01/08/08 - Added "SECCHI (and S/C) Partitions" check.
;
; $Log: read_weekly_ws_sched.pro,v $
; Revision 1.5  2009/09/11 20:28:21  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.1  2006/05/30 18:26:57  esfand
; Reads weekly SC activity plans
;
;-
;

PRO READ_WEEKLY_WS_SCHED, ws 

; Examples of ws (weekly schedule):
;  ws = '/home/esfand/secchi/PT1/IN/SCA/SC_A/ahead_2006_310_04.ws'
;  ws = '/home/esfand/secchi/PT1/IN/SCA/SC_B/behind_2006_310_08.ws'

  dsn_start= ''
  dsn_stop= ''
  dsn_bval= ''
  dsn_eval= ''
  pb_start= ''
  pb_stop= ''
  pb_bval= ''
  pb_eval= ''
  maneuver_start= ''
  maneuver_stop= ''

  OPENR, inlun, ws, /GET_LUN
  event=''
  WHILE NOT(EOF(inlun)) DO BEGIN
    READF,inlun, event
    event= STRTRIM(event,2)
    IF (STRPOS(event,'Time Period:') EQ 0) THEN BEGIN
      toks= STRTRIM(STR_SEP(event,':'),2)
      sdoy= STRMID(toks(2),0,3)
    ENDIF
    IF (STRPOS(event,' RT BOT ') GT 0) THEN BEGIN
      toks= STR_SEP(event,' ')
      toks= toks(WHERE(toks NE ''))
      dt= STR_SEP(toks(0),':')
      sdt= UTC2STR(DOY2UTC(dt(1),dt(0)),/ECS,/DATE_ONLY)+' '+dt(2)+':'+dt(3)+':'+dt(4)
      dsn_start= [dsn_start,sdt]
      dsn_bval= [dsn_bval,toks(3)]
    ENDIF
    IF (STRPOS(event,' RT EOT ') GT 0) THEN BEGIN
      toks= STR_SEP(event,' ')
      toks= toks(WHERE(toks NE ''))
      dt= STR_SEP(toks(0),':')
      edt= UTC2STR(DOY2UTC(dt(1),dt(0)),/ECS,/DATE_ONLY)+' '+dt(2)+':'+dt(3)+':'+dt(4)
      dsn_stop= [dsn_stop,edt]
      dsn_eval= [dsn_eval,toks(3)]
    ENDIF
    ;IF (STRPOS(event,' UT Start of Playback of    ') GT 0) THEN BEGIN
    IF (STRPOS(event,' UT Start of Playback of    ') GT 0 OR $
        STRPOS(event,' RT Switch SSR playback to ') GT 0) THEN BEGIN
      toks= STR_SEP(event,' ')
      toks= toks(WHERE(toks NE ''))
      bval= toks(6)
      READF,inlun, event
      ;IF (STRPOS(event,' Science (and S/C) Partitions ') GT 0) THEN BEGIN
      IF (STRPOS(event,' Science (and S/C) Partitions ') GT 0 OR $
          STRPOS(event,' Science (SECCHI and S/C') GT 0 OR $
          STRPOS(event,' SECCHI (and S/C) Partitions') GT 0 OR $
          STRPOS(event,' Science & S/C partitions') GT 0) THEN BEGIN
        toks= toks(WHERE(toks NE ' ')) 
        dt= STR_SEP(toks(0),':')
        sdt= UTC2STR(DOY2UTC(dt(1),dt(0)),/ECS,/DATE_ONLY)+' '+dt(2)+':'+dt(3)+':'+dt(4)
        pb_start= [pb_start,sdt]
        pb_bval= [pb_bval,bval]
        ;pb_start_dnlnk_rate= [pb_start_dnlnk_rate,
        ;pb_start_rt_rate= [pb_start_rt_rate,
      ENDIF
    ENDIF
    ;IF (STRPOS(event,' UT Stop of Playback   ') GT 0) THEN BEGIN
    IF (STRPOS(event,' UT Stop of Playback   ') GT 0 OR $
        STRPOS(event,' RT Stop SSR Playback   ') GT 0) THEN BEGIN
      toks= STR_SEP(event,' ')
      toks= toks(WHERE(toks NE ''))
      eval= toks(5)
      dt= STR_SEP(toks(0),':')
      edt= UTC2STR(DOY2UTC(dt(1),dt(0)),/ECS,/DATE_ONLY)+' '+dt(2)+':'+dt(3)+':'+dt(4)
      pb_stop= [pb_stop,edt]
      pb_eval= [pb_eval,eval]
    ENDIF
    IF (STRPOS(event,' RT Momentum Dump ') GT 0) THEN BEGIN
     dur= FIX(STRMID(event,56,10))
     IF (dur GT 0) THEN BEGIN  
      toks= STR_SEP(event,' ')
      toks= toks(WHERE(toks NE ''))
      dt= STR_SEP(toks(0),':')
      sdt= UTC2STR(DOY2UTC(dt(1),dt(0)),/ECS,/DATE_ONLY)+' '+dt(2)+':'+dt(3)+':'+dt(4)
      maneuver_start= [maneuver_start,sdt]
      ;edt= TAI2UTC(UTC2TAI(STR2UTC(sdt))+toks(5)*60,/ECS,/TRUNCATE) ; toks(5) = duration minutes
      edt= TAI2UTC(UTC2TAI(STR2UTC(sdt))+dur*60,/ECS,/TRUNCATE) ; toks(5) = duration minutes
      maneuver_stop= [maneuver_stop,edt]
     ENDIF
    ENDIF
    IF (STRPOS(event,' SECCHI Stepped ') GT 0) THEN BEGIN
      toks= STR_SEP(event,' ')
      READF,inlun, event
      IF (STRPOS(event,' Calibration    ') GT 0) THEN BEGIN
        toks= toks(WHERE(toks NE ''))
        dt= STR_SEP(toks(0),':')
        sdt= UTC2STR(DOY2UTC(dt(1),dt(0)),/ECS,/DATE_ONLY)+' '+dt(2)+':'+dt(3)+':'+dt(4)
        maneuver_start= [maneuver_start,sdt]
        edt= TAI2UTC(UTC2TAI(STR2UTC(sdt))+toks(5)*60,/ECS,/TRUNCATE) ; toks(5) = duration minutes
        maneuver_stop= [maneuver_stop,edt]
      ENDIF
    ENDIF
  ENDWHILE

  FREE_LUN, inlun 

  IF (N_ELEMENTS(dsn_start) GT 1) THEN dsn_start= dsn_start(1:*)
  IF (N_ELEMENTS(dsn_bval) GT 1) THEN dsn_bval= dsn_bval(1:*)
  IF (N_ELEMENTS(dsn_stop) GT 1) THEN dsn_stop= dsn_stop(1:*)
  IF (N_ELEMENTS(dsn_eval) GT 1) THEN dsn_eval= dsn_eval(1:*)
  IF (N_ELEMENTS(pb_start) GT 1) THEN pb_start= pb_start(1:*)
  IF (N_ELEMENTS(pb_bval) GT 1) THEN pb_bval= pb_bval(1:*)
  IF (N_ELEMENTS(pb_stop) GT 1) THEN pb_stop= pb_stop(1:*)
  IF (N_ELEMENTS(pb_eval) GT 1) THEN pb_eval= pb_eval(1:*)
  IF (N_ELEMENTS(maneuver_start) GT 1) THEN maneuver_start= maneuver_start(1:*)
  IF (N_ELEMENTS(maneuver_stop) GT 1) THEN maneuver_stop= maneuver_stop(1:*)

  HELP,dsn_start, dsn_stop,pb_start, pb_stop,maneuver_start, maneuver_stop
  ;stop

  scnt= N_ELEMENTS(dsn_start)
  ecnt= N_ELEMENTS(dsn_stop)
  IF(scnt LT ecnt) THEN BEGIN
    ;dsn start  was in previous schedule
    dsn_start= ['yyyy/mm/dd hh:mm:ss',dsn_start]
    dsn_bval= [dsn_eval(0),dsn_bval] 
    scnt= scnt+1
  ENDIF ELSE BEGIN
    IF(ecnt LT scnt) THEN BEGIN
      dsn_stop= [dsn_stop,'yyyy/mm/dd hh:mm:ss']
      dsn_eval= [dsn_eval,dsn_bval(scnt-1)]
      ecnt= ecnt+1
    ENDIF ELSE BEGIN  ; scnt EQ ecnt
      IF (dsn_bval(0) NE dsn_eval(0)) THEN BEGIN  
        ; file starts with a dsn_stop time (started in previous schedule)
        ; and ends with a dsn_start time (ends in the next schedule).
        dsn_stop= [dsn_stop(1:*),'yyyy/mm/dd hh:mm:ss']  
        dsn_eval= [dsn_eval(1:*),dsn_bval(scnt-1)]
      ENDIF
    ENDELSE
  ENDELSE

  PRINT,''
  PRINT,"DSN Start and Stop Times:'
  FOR m= 0, N_ELEMENTS(dsn_start)-1 do PRINT,dsn_start(m),'  '+dsn_bval(m)+'  and  '+dsn_stop(m)+'  '+dsn_eval(m)

;  FOR m= 0, N_ELEMENTS(dsn_bval)-1 DO BEGIN
;   ind= WHERE(dsn_eval EQ dsn_bval(m),cnt)
;   dsn_start(m)= dsn_start(m)+' '+dsn_bval(m)
;   IF (cnt GT 0) THEN BEGIN 
;     ;dsn_stop(m)= dsn_stop(ind(0)) 
;     dsn_stop(m)= dsn_stop(ind(0))+' '+dsn_eval(ind(0))
;   ENDIF ELSE BEGIN 
;     PRINT,'' 
;     PRINT,'Warning:'
;     PRINT,'   No DSN start or stop for '+dsn_start(m)+' and '+dsn_stop(m)
;     PRINT,'' 
;     dsn_stop(m)= 'yyyy/mm/dd hh:mm:ss'
;   ENDELSE
;   dsn_eval(m)= ''
;  ENDFOR

  IF (N_ELEMENTS(dsn_start) NE N_ELEMENTS(dsn_stop)) THEN BEGIN
    HELP, dsn_start, dsn_stop
    STOP,' Warning: dsn_starts <> dsn_stops'
  ENDIF


  IF (N_ELEMENTS(pb_start) NE N_ELEMENTS(pb_stop)) THEN BEGIN
    PRINT,''
    HELP, pb_start, pb_stop
    HELP, pb_bval, pb_eval
    b= N_ELEMENTS(pb_bval)
    e= N_ELEMENTS(pb_eval)
    j= b < e
    k= b > e
    PRINT,''
    PRINT,'    PB UT Start                  PB UT Stop'
    FOR l= 0,j-1 DO $
      PRINT,pb_start(l)+'  '+pb_bval(l)+'    '+pb_stop(l)+'  '+pb_eval(l)
    IF (b GT e) THEN BEGIN
      PRINT,'  Extra UT Start'
      FOR l= j, k-1 DO $
        PRINT,pb_start(l)+'  '+pb_bval(l)
    ENDIF
    IF (e GT b) THEN BEGIN
      PRINT,'                               Extra UT Stop'
      FOR l= j, k-1 DO $
        PRINT,'                           '+pb_stop(l)+'  '+pb_eval(l)
    ENDIF
    PRINT,''
    STOP,' Warning: pb_starts <> pb_stops'
    PRINT,''
  ENDIF

  PRINT,''
  IF (N_ELEMENTS(pb_start) EQ N_ELEMENTS(pb_stop)) THEN BEGIN
    FOR l=0, N_ELEMENTS(pb_start)-1 DO BEGIN
      IF (pb_start(l) GE pb_stop(l)) THEN BEGIN
        PRINT,'PB Start LE PB Stop: '+pb_start(l)+'  '+pb_bval(l)+'   '+pb_stop(l)+'  '+pb_eval(l)
        STOP
      ENDIF
    ENDFOR 
  ENDIF

  IF (N_ELEMENTS(maneuver_start) NE N_ELEMENTS(maneuver_stop)) THEN BEGIN
    HELP, maneuver_start, maneuver_stop
    STOP,' Warning: maneuver_starts <> maneuver_stops'
  ENDIF

help,dsn_start,dsn_stop,dsn_bval,dsn_eval
stop

  ;IF (KEYWORD_SET(mkfile)) THEN BEGIN
    IF (pb_start(0) NE '') THEN BEGIN
      n= N_ELEMENTS(pb_start) 
      dts= STRMID(pb_start(0),0,4)+'_'+sdoy+'_'+STRMID(pb_start(0),5,2)+ $
           STRMID(pb_start(0),8,2)+'_'+ $
           STRMID(pb_start(n-1),5,2)+STRMID(pb_start(n-1),8,2)
      IF (STRPOS(ws,'ahead') GE 0) THEN wfn= STRMID(ws,0,STRPOS(ws,'ahead'))+'ahead_'
      IF (STRPOS(ws,'behind') GE 0) THEN wfn= STRMID(ws,0,STRPOS(ws,'behind'))+'behind_'
      wfn= wfn+dts+'.SCA'
      PRINT,'Generating '+wfn

      OPENW, wlun, wfn, /GET_LUN
      PRINTF,wlun, 'DATATYPE=  SECCHI ACTIVITY PLAN'
      GET_UTC, ctime
      ctime= UTC2STR(ctime,/ECS,/TRUNCATE)
      PRINTF,wlun, 'DATE_CRE=  '+ctime
      PRINTF,wlun, 'OUT_FORM=  KEYWORD'
      PRINTF,wlun, 'COMMENT='
      PRINTF,wlun, 'END'
      PRINTF,wlun, ''
      IF (dsn_start(0) NE '') THEN BEGIN
        FOR i= 0, N_ELEMENTS(dsn_start)-1 DO BEGIN
          PRINTF,wlun, 'DSN_Contact'
          PRINTF,wlun, '   STARTIME = '+dsn_start(i)
          PRINTF,wlun, '   ENDTIME  = '+dsn_stop(i)
          PRINTF,wlun, ''
        ENDFOR
      ENDIF
      IF (pb_start(0) NE '') THEN BEGIN
        FOR i= 0, N_ELEMENTS(pb_start)-1 DO BEGIN
          PRINTF,wlun, 'TLM_Tape_Dump'
          PRINTF,wlun, '   STARTIME = '+pb_start(i)
          PRINTF,wlun, '   ENDTIME  = '+pb_stop(i)
          PRINTF,wlun, ''
        ENDFOR
      ENDIF
      IF (maneuver_start(0) NE '') THEN BEGIN
        FOR i= 0, N_ELEMENTS(maneuver_start)-1 DO BEGIN
          PRINTF,wlun, 'SPACECRAFT_MANEUVER'
          PRINTF,wlun, '   STARTIME = '+maneuver_start(i)
          PRINTF,wlun, '   ENDTIME  = '+maneuver_stop(i)
          PRINTF,wlun, ''
        ENDFOR
      ENDIF
      FREE_LUN, wlun
    ENDIF ELSE BEGIN
      STOP, ws+" does not have playback times. Didn't Generate .SCA file."
    ENDELSE
  ;ENDIF
  
  RETURN

END
