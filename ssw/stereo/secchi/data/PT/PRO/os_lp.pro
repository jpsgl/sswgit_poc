;+
;$Id: os_lp.pro,v 1.11 2009/09/11 20:28:15 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_LP
;             : LP_DOUBLE_IMAGE()       Implemented.
;	      :	LP_NORMAL_IMAGE()	Implemented.
;	      :	LP_DARK_IMAGE()		Implemented.
;	      :	LP_CAL_LAMP()		Implemented.
;	      :	LP_SCIP_SEQ()		Implemented.
;             : LP_HI_SEQ()             Implemented.
;             : LP_BLOCK_SEQ()          Implemented.
;	      :	LP_CONT_IMAGE()         Implemented.
;               
; Purpose     : Widgets to define a SEB Program.
;               
; Explanation : These routines create a widget interface for a specific
;		SECCHI SEB Program.  
;               
; Use         :  result = LP_NORMAL_IMAGE( caller )
;    
; Inputs      : caller	structure containing id of caller.
;               
; Opt. Inputs : None.
;               
; Outputs     : Always returns 1.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/04/04 - Limited number of camera tables to 5.
;              Ed Esfandiari 06/22/04 - Use lamp descriptions from a save set.
;              Ed Esfandiari 07/09/04 - Use 24 polar pos. for COR1 & COR2 instead of 20.
;              Ed Esfandiari 08/16/04 - removed commented out code using 1.024 msec scip exptime units.
;              Ed Esfandiari 08/31/04 - Added new LED constraint to limit Telescope mounted LED images
;                                       to 3126098 pulses (67.1 secs) and FPA mounted LED images 
;                                       to 9999999 pulses (214.652 Secs).
;              Ed Esfandiari 09/24/04 - changed FPS labels to EUVI FPS and made active only for EUVI. 
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 10/12/04 - Swithced BLU and PRP LED assignments from 1 and 2 to 2 and 1
;                                       to match hard ware assignment.
;              Ed Esfandiari 11/15/04 - Used WIDGET_DROPLIST to display all of longer Image-Proc lists. 
;              Ed Esfandiari 12/06/04 - changed HI2 seq max images from 25 to 70 (same as Hi1)
;              Ed Esfandiari 12/14/04 - Added exptime and CCD summing info for each type of selected image.
;              Ed Esfandiari 01/11/05 - Added low gain exptime correction for each type of selected image.
;              Ed Esfandiari 03/09/05 - Added calib led gain scheduled return call and added exp.time
;                                       info display to HI sequence selection window.
;              Ed Esfandiari 03/11/05 - For IDL v5.6+ use WIDGET_COMBOBOX instead of WIDGET_DROPLIST
;                                       since droplist can't show all of 80 IP table names and added 
;                                       0-70 with increments of 1 for HI images.
;              Ed Esfandiari 03/14/05 - Set Default FPS to ON for EUVI.
;              Ed Esfandiari 04/18/05 - Changed from 5 camera tables per camera to 7.
;              Ed Esfandiari 10/30/06 - Allow up to 99 images per HI sequence.
;              Ed Esfandiari 11/21/06 - Ignore ccd_sum and low gain exptime correction (FSW does not do it).
;              Ed Esfandiari 01/09/07 - Added dark 0-99998 ms range and changed exp.time format to 6.2.
;         
;
; $Log: os_lp.pro,v $
; Revision 1.11  2009/09/11 20:28:15  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.8  2006/10/31 18:51:31  esfand
; allowed up to 99 images per HI sequence
;
; Revision 1.7  2005/04/27 20:38:38  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.6  2005/03/10 16:47:37  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.5  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.3  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.2  2004/07/12 13:17:08  esfand
; now using 24 COR1 and COR2 polar positions
;
; Revision 1.1.1.2  2004/07/01 21:19:05  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO LP_NORMAL_IMAGE_EVENT, event

COMMON OS_INIT_SHARE
COMMON LP_NORMAL_IMAGE_SHARE, lpnormv
COMMON OS_SHARE
COMMON OS_ALL_SHARE  ; has ex (exposure times)

CASE (event.id) OF

   lpnormv.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpnormv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END

   lpnormv.schedule : BEGIN
      WIDGET_CONTROL,osv.info_text,SET_VALUE='Normal Image Exposure'
      IF (lpnormv.tele NE 0) THEN lpnormv.fps= 0 ; FPS is only valid for EUVI
      OS_CALL_SCHEDULE
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpnormv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END
 
   lpnormv.telepd : BEGIN
     IF (lpnormv.tele NE event.index) THEN BEGIN
      lpnormv.tele = event.index
      WIDGET_CONTROL, lpnormv.fwpd, SET_VALUE=fw_types(lpnormv.exptable,*,lpnormv.tele)
      WIDGET_CONTROL, lpnormv.fwpd, SET_VALUE=0
      lpnormv.fw= 0 ; AEE - Nov 19, 02 - added so that fw from other cameras are not used as previous
                    ; fw for this camera.
      WIDGET_CONTROL, lpnormv.pwpd, SET_VALUE=pw_types(lpnormv.exptable,*,lpnormv.tele)
      WIDGET_CONTROL, lpnormv.pwpd, SET_VALUE=0
      lpnormv.pw= 0 ; AEE - Nov 19, 02 - added so that pw from other cameras are not used as previous
                    ; pw for this camera.
      IF (lpnormv.tele EQ 0) THEN $
        WIDGET_CONTROL, lpnormv.fps_pd, SENSITIVE=1 $
      ELSE $
        WIDGET_CONTROL, lpnormv.fps_pd, SENSITIVE=0
     ENDIF
   END
 
   lpnormv.iptable_pd : BEGIN
      lpnormv.iptable = event.index
   END

   lpnormv.exptable_pd : BEGIN  ; AEE 1/13/04
      lpnormv.exptable = event.index
   END

   lpnormv.camtable_pd : BEGIN ; AEE 1/13/04
      lpnormv.camtable = event.index
   END

   lpnormv.fps_pd : BEGIN ; AEE 1/13/04
      lpnormv.fps = event.index
   END

   lpnormv.fwpd : BEGIN

      ;IF selected filter is blank, ignore it and use the previously selected/displayed filter, instead:
      IF (STRTRIM(fw_types(lpnormv.exptable,event.index,lpnormv.tele),2) EQ '') THEN $  
        WIDGET_CONTROL, lpnormv.fwpd, SET_VALUE=lpnormv.fw          $
      ELSE                                                          $ 
        lpnormv.fw = event.index
   END

   lpnormv.pwpd : BEGIN
      IF (STRTRIM(pw_types(lpnormv.exptable,event.index,lpnormv.tele),2) EQ '') THEN $ 
        WIDGET_CONTROL, lpnormv.pwpd, SET_VALUE=lpnormv.pw          $
      ELSE                                                          $
        lpnormv.pw = event.index
   END

   ELSE : BEGIN
   END

ENDCASE
; show the exposure times that will be used:

msec=1000.0 ; don't need to show real scip exptime (units of 1.024).
ccdsum= (ccd(lpnormv.tele,lpnormv.camtable).xsum > 1) * (ccd(lpnormv.tele,lpnormv.camtable).ysum > 1)
gmode= 'High'
IF (ccd(lpnormv.tele,lpnormv.camtable).gmode EQ 1) THEN BEGIN
  gmode='Low'
  ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
ENDIF

ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

tsumexp= 'ExpTime (CCDsum '+STRTRIM(ccd(lpnormv.tele,lpnormv.camtable).xsum > 1,2)+'x'+ $
         STRTRIM(ccd(lpnormv.tele,lpnormv.camtable).ysum > 1,2)+' & gain='+gmode+') = '
WIDGET_CONTROL, lpnormv.exid,  SET_VALUE= $
   tsumexp+STRING(ex(lpnormv.tele,lpnormv.exptable,lpnormv.fw,lpnormv.pw)/msec/ccdsum,'(F6.2)')+' Sec'

END

;________________________________________________________________________________________________________
;

FUNCTION LP_NORMAL_IMAGE, caller

COMMON LP_NORMAL_IMAGE_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE  ; has ex (exposure times)

    IF XRegistered("LP_NORMAL_IMAGE") THEN RETURN, 1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 1
    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpnormv))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       tele = 0
       exptable = 0  ; AEE 1/13/04
       camtable = 0 
       iptable = 0
       ;fps = 0 ; off
       fps = 1 ; on
       fw = 0
       pw = 0
    ENDIF ELSE BEGIN
       tele = lpnormv.tele
       exptable = lpnormv.exptable
       camtable = lpnormv.camtable
       iptable = lpnormv.iptable
       fps = lpnormv.fps
       fw = lpnormv.fw
       pw = lpnormv.pw
    ENDELSE


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/COLUMN, TITLE='LP NORMAL_IMAGE', /FRAME, $
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW, /FRAME)

      col = WIDGET_BASE(row0, /COLUMN)
        telepd =  CW_BSELECTOR2(col, tele_types, SET_VALUE=tele)
        ;iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=iptable)
        ;iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable

        IF (!version.release LT '5.6') THEN BEGIN
          iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE


        exptable_pd = CW_BSELECTOR2(col, "Exposure "+table_types, SET_VALUE=exptable) ; AEE 1/13/04
        ;camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=camtable) ; AEE 6/4/04
        camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=camtable)
      row = WIDGET_BASE(col, /ROW)
        tmp= WIDGET_LABEL(row,VALUE= 'EUVI FPS') ; AEE 1/13/04
        fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

      row = WIDGET_BASE(col, /ROW)
        msec=1000.0 ; don't need to show real scip exptime (units of 1.024).
        ccdsum= (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
        gmode= 'High '
        IF (ccd(tele,camtable).gmode EQ 1) THEN BEGIN
          gmode='Low'
          ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
        ENDIF

        ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

        tsumexp= 'ExpTime (CCDsum '+STRTRIM(ccd(tele,camtable).xsum > 1,2)+'x'+ $
                 STRTRIM(ccd(tele,camtable).ysum > 1,2)+' & gain='+gmode+') = '

        font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
        exid=  WIDGET_LABEL(row, FONT= font, $
                VALUE= tsumexp+STRING(ex(tele,exptable,fw,pw)/msec/ccdsum,'(F6.2)')  + ' Sec')

      col = WIDGET_BASE(row0, /COLUMN)
        fwpd = CW_BSELECTOR2(col, fw_types(exptable,*,tele), SET_VALUE=fw)
        pwpd = CW_BSELECTOR2(col, pw_types(exptable,*,tele), SET_VALUE=pw)
      
 
     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    lpnormv = CREATE_STRUCT( 'base', base,			$
                             'telepd', telepd,			$
                             ;'table_pd', table_pd,		$
                             'exptable_pd', exptable_pd,        $ ; AEE 1/13/04
                             'camtable_pd', camtable_pd,        $ ; AEE 1/13/04
                             'iptable_pd', iptable_pd,		$
                             'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             'fwpd', fwpd,			$
                             'pwpd', pwpd,			$
                             'fw', fw,				$
                             'pw', pw,				$
                             'exid', exid,                      $
                             'fps', fps,                        $ ; AEE 1/13/04
                             'tele', tele,			$
                             ;'table', table,			$
                             'exptable', exptable,              $ ; AEE 1/13/04
                             'camtable', camtable,              $ ; AEE 1/13/04
                             'iptable', iptable )

    IF (tele NE 0) THEN WIDGET_CONTROL, lpnormv.fps_pd, SENSITIVE=0
    XMANAGER, "LP_NORMAL_IMAGE", base

    RETURN, 1

END

;________________________________________________________________________________________________________
;
 
PRO LP_DOUBLE_IMAGE_EVENT, event

COMMON OS_INIT_SHARE
COMMON LP_DOUBLE_IMAGE_SHARE, lpdoubv
COMMON OS_SHARE
COMMON OS_ALL_SHARE  ; has ex (exposure times)

CASE (event.id) OF

   lpdoubv.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpdoubv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN ; AEE 9/23/03 - required since added more stuff after the end of case statement.
   END

   lpdoubv.schedule : BEGIN
      WIDGET_CONTROL,osv.info_text,SET_VALUE='Normal Image Exposure'
      IF (lpdoubv.tele NE 0) THEN lpdoubv.fps= 0 ; FPS is only valid for EUVI
      OS_CALL_SCHEDULE
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpdoubv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN ; AEE 9/23/03 - required since added more stuff after the end of case statement.
   END
 
   lpdoubv.telepd : BEGIN
     IF (lpdoubv.tele NE event.index) THEN BEGIN
      lpdoubv.tele = event.index
      WIDGET_CONTROL, lpdoubv.fwpd, SET_VALUE=fw_types(lpdoubv.exptable,*,lpdoubv.tele)
      WIDGET_CONTROL, lpdoubv.fwpd, SET_VALUE=0
      lpdoubv.fw= 0 ; AEE - Nov 19, 02 - added so that fw from other cameras are not used as previous
                    ; fw for this camera.
      WIDGET_CONTROL, lpdoubv.pwpd, SET_VALUE=pw_types(lpdoubv.exptable,*,lpdoubv.tele)
      WIDGET_CONTROL, lpdoubv.pwpd, SET_VALUE=0
      lpdoubv.pw= 0 ; AEE - Nov 19, 02 - added so that pw from other cameras are not used as previous
                    ; pw for this camera.

      WIDGET_CONTROL, lpdoubv.pwpd2, SET_VALUE=pw_types(lpdoubv.exptable,*,lpdoubv.tele)
      WIDGET_CONTROL, lpdoubv.pwpd2, SET_VALUE=0
      lpdoubv.pw2= 0 
      IF (lpdoubv.tele EQ 0) THEN $
        WIDGET_CONTROL, lpdoubv.fps_pd, SENSITIVE=1 $
      ELSE $
        WIDGET_CONTROL, lpdoubv.fps_pd, SENSITIVE=0

     ENDIF
   END
 
   lpdoubv.iptable_pd : BEGIN
      lpdoubv.iptable = event.index
   END

   lpdoubv.exptable_pd : BEGIN  ; AEE 1/13/04
      lpdoubv.exptable = event.index
   END

   lpdoubv.camtable_pd : BEGIN ; AEE 1/13/04
      lpdoubv.camtable = event.index
   END

   lpdoubv.fps_pd : BEGIN ; AEE 1/13/04
      lpdoubv.fps = event.index
   END

   lpdoubv.fwpd : BEGIN

      ;IF selected filter is blank, ignore it and use the previously selected/displayed filter, instead:
      IF (STRTRIM(fw_types(lpdoubv.exptable,event.index,lpdoubv.tele),2) EQ '') THEN $  
        WIDGET_CONTROL, lpdoubv.fwpd, SET_VALUE=lpdoubv.fw          $
      ELSE                                                          $ 
        lpdoubv.fw = event.index
   END

   lpdoubv.pwpd : BEGIN
      IF (STRTRIM(pw_types(lpdoubv.exptable,event.index,lpdoubv.tele),2) EQ '') THEN $ 
        WIDGET_CONTROL, lpdoubv.pwpd, SET_VALUE=lpdoubv.pw          $
      ELSE                                                          $
        lpdoubv.pw = event.index
   END

   lpdoubv.pwpd2 : BEGIN
      IF (STRTRIM(pw_types(lpdoubv.exptable,event.index,lpdoubv.tele),2) EQ '') THEN $
        WIDGET_CONTROL, lpdoubv.pwpd2, SET_VALUE=lpdoubv.pw2        $
      ELSE                                                          $
        lpdoubv.pw2 = event.index
   END

   ELSE : BEGIN
   END

ENDCASE

; show the exposure times that will be used:

msec=1000.0 ; don't need to show real scip exptime (units of 1.024). 
ccdsum= (ccd(lpdoubv.tele,lpdoubv.camtable).xsum > 1) * (ccd(lpdoubv.tele,lpdoubv.camtable).ysum > 1)
gmode= 'High'
IF (ccd(lpdoubv.tele,lpdoubv.camtable).gmode EQ 1) THEN BEGIN
  gmode='Low'
  ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
ENDIF

ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

tsumexp= 'ExpTime (CCDsum '+STRTRIM(ccd(lpdoubv.tele,lpdoubv.camtable).xsum > 1,2)+'x'+ $
         STRTRIM(ccd(lpdoubv.tele,lpdoubv.camtable).ysum > 1,2)+' & gain='+gmode+') = '

WIDGET_CONTROL, lpdoubv.exid,  SET_VALUE= $
   '1st '+tsumexp+STRING(ex(lpdoubv.tele,lpdoubv.exptable,lpdoubv.fw,lpdoubv.pw)/msec/ccdsum,'(F6.2)')+' Sec'
WIDGET_CONTROL, lpdoubv.ex2id, SET_VALUE= $
  '2nd '+tsumexp+STRING(ex(lpdoubv.tele,lpdoubv.exptable,lpdoubv.fw,lpdoubv.pw2)/msec/ccdsum,'(F6.2)')+' Sec'

END

;________________________________________________________________________________________________________
;

FUNCTION LP_DOUBLE_IMAGE, caller

COMMON LP_DOUBLE_IMAGE_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE  ; has ex (exposure times)

    IF XRegistered("LP_DOUBLE_IMAGE") THEN RETURN, 1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 0 
    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpdoubv))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       tele = 0
       exptable = 0
       camtable = 0
       iptable = 0
       ;fps = 0 ; off
       fps = 1 ; on
       fw = 0
       pw = 0
       pw2= 0  ; second pw position
    ENDIF ELSE BEGIN
       tele = lpdoubv.tele
       exptable = lpdoubv.exptable
       camtable = lpdoubv.camtable
       iptable = lpdoubv.iptable
       fps = lpdoubv.fps
       fw = lpdoubv.fw
       pw = lpdoubv.pw
       pw2= lpdoubv.pw2
    ENDELSE


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/COLUMN, TITLE='LP DOUBLE_IMAGE', /FRAME, $
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW, /FRAME)

      col = WIDGET_BASE(row0, /COLUMN)
        telepd =  CW_BSELECTOR2(col, tele_types, SET_VALUE=tele)
        ;table_pd = CW_BSELECTOR2(col, table_types, SET_VALUE=table)
        ;iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=iptable)
        ;iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable

        IF (!version.release LT '5.6') THEN BEGIN
          iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE

        exptable_pd = CW_BSELECTOR2(col,"Exposure "+table_types, SET_VALUE=exptable) ; AEE 1/13/04
        ;camtable_pd = CW_BSELECTOR2(col,"Camera "+table_types(0:4), SET_VALUE=camtable) ; AEE 6/4/04
        camtable_pd = CW_BSELECTOR2(col,"Camera "+table_types, SET_VALUE=camtable) 

      col1 = WIDGET_BASE(row0, /COLUMN)
        fwpd = CW_BSELECTOR2(col1, fw_types(exptable,*,tele), SET_VALUE=fw)
        pwpd = CW_BSELECTOR2(col1, pw_types(exptable,*,tele), SET_VALUE=pw)
        pwpd2= CW_BSELECTOR2(col1, pw_types(exptable,*,tele), SET_VALUE=pw2)

;      col1 = WIDGET_BASE(row0, /COLUMN)
;        temp = WIDGET_LABEL(col1, VALUE='')
;        temp = WIDGET_LABEL(col1, VALUE='Filter Position For Both Exposures') 
;        temp = WIDGET_LABEL(col1, VALUE='')

        row = WIDGET_BASE(col, /ROW)
        tmp= WIDGET_LABEL(row,VALUE= 'EUVI FPS') ; AEE 1/13/04
        fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

        ; show the exposure times that will be used:
        font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
        msec=1000.0 ; don't need to show real scip exptime (units of 1.024). 
        ccdsum= (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
        gmode= 'High '
        IF (ccd(tele,camtable).gmode EQ 1) THEN BEGIN
          gmode='Low'
          ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
        ENDIF

        ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

        tsumexp= 'ExpTime (CCDsum '+STRTRIM(ccd(tele,camtable).xsum > 1,2)+'x'+ $
            STRTRIM(ccd(tele,camtable).ysum > 1,2)+' & gain='+gmode+') = '
        
        row = WIDGET_BASE(col, /ROW)
        exid=  WIDGET_LABEL(row, FONT=font, $
               VALUE='1st '+tsumexp+STRING(ex(tele,exptable,fw,pw)/msec/ccdsum,'(F6.2)')  + ' Sec')
        row = WIDGET_BASE(col, /ROW)
        ex2id= WIDGET_LABEL(row, FONT=font, $
               VALUE='2nd '+tsumexp+STRING(ex(tele,exptable,fw,pw2)/msec/ccdsum,'(F6.2)') + ' Sec')


     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    lpdoubv = CREATE_STRUCT( 'base', base,			$
                             'telepd', telepd,			$
                             'exptable_pd', exptable_pd,        $ ; AEE 1/13/04
                             'camtable_pd', camtable_pd,        $ ; AEE 1/13/04
                             'iptable_pd', iptable_pd,		$
                             'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             'fwpd', fwpd,			$
                             'pwpd', pwpd,			$
                             'pwpd2', pwpd2,                    $
                             'fw', fw,				$
                             'pw', pw,				$
                             'pw2', pw2,                        $
                             'exid', exid,                      $
                             'ex2id', ex2id,                    $
                             'fps', fps,                        $ ; AEE 1/13/04
                             'tele', tele,			$
                             'exptable', exptable,              $ ; AEE 1/13/04
                             'camtable', camtable,              $ ; AEE 1/13/04
                             'iptable', iptable )

    IF (tele NE 0) THEN WIDGET_CONTROL, lpdoubv.fps_pd, SENSITIVE=0
    XMANAGER, "LP_DOUBLE_IMAGE", base

    RETURN, 1

END

;________________________________________________________________________________________________________
;
 
PRO LP_DARK_IMAGE_EVENT, event

COMMON OS_INIT_SHARE
COMMON LP_DARK_IMAGE_SHARE, lpdarkv
COMMON OS_SHARE
COMMON OS_ALL_SHARE

CASE (event.id) OF

   lpdarkv.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpdarkv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END

   lpdarkv.schedule : BEGIN
      WIDGET_CONTROL,osv.info_text,SET_VALUE='Dark Image Exposure'
      WIDGET_CONTROL, lpdarkv.exp_text, GET_VALUE=expt & expt = expt(0)
      lpdarkv.expt = LONG(expt) 
      IF (lpdarkv.tele NE 0) THEN lpdarkv.fps= 0 ; FPS is only valid for EUVI
      OS_CALL_SCHEDULE
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpdarkv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END
 
   lpdarkv.telepd : BEGIN
      lpdarkv.tele = event.index
      IF (lpdarkv.tele EQ 0) THEN $
        WIDGET_CONTROL, lpdarkv.fps_pd, SENSITIVE=1 $
      ELSE $
        WIDGET_CONTROL, lpdarkv.fps_pd, SENSITIVE=0
   END
 
   lpdarkv.iptable_pd : BEGIN
      lpdarkv.iptable = event.index
   END

   lpdarkv.camtable_pd : BEGIN ; AEE 1/13/04
      lpdarkv.camtable = event.index
   END

   lpdarkv.fps_pd : BEGIN ; AEE 1/13/04
      lpdarkv.fps = event.index
   END

   lpdarkv.exp_text : BEGIN
      WIDGET_CONTROL, lpdarkv.exp_text, GET_VALUE=expt & expt = expt(0)
      temp = P_CHECK(expt,'expt',new) & WIDGET_CONTROL, lpdarkv.exp_text, SET_VALUE=new
      WIDGET_CONTROL, lpdarkv.exp_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(new),2))]
      lpdarkv.expt = LONG(new)
   END

   ELSE : BEGIN
   END

ENDCASE

ccdsum= (ccd(lpdarkv.tele,lpdarkv.camtable).xsum > 1) * (ccd(lpdarkv.tele,lpdarkv.camtable).ysum > 1)
gmode= ccd(lpdarkv.tele,lpdarkv.camtable).gmode
IF (ccdsum EQ 1 AND gmode EQ 0) THEN BEGIN 
  ; no ccd summing and gain is high ( no need to adjust exptime):
  tsum='                                                                                                                                    '
ENDIF ELSE BEGIN
  tsum=''
  IF (ccdsum GT 1) THEN BEGIN 
    ; ccd summing:
    tsum= 'Please adjust exposure for '+$
          STRTRIM(ccd(lpdarkv.tele,lpdarkv.camtable).xsum > 1,2)+'x'+ $
          STRTRIM(ccd(lpdarkv.tele,lpdarkv.camtable).ysum > 1,2)+' CCD summing (divide by '+ $
          STRTRIM(ccdsum,2)+')'
  ENDIF
  IF (gmode EQ 1) THEN BEGIN 
    ; low gain (user has to amplify exptime by 4):
    IF (STRLEN(tsum) GT 0) THEN $ 
      tsum= tsum+' & low gain (amplify by 4)' $
    ELSE $
      tsum= 'Please adjust exposure for low gain (amplify by 4)'
  ENDIF
ENDELSE
tsum='  ' ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

WIDGET_CONTROL, lpdarkv.msid, SET_VALUE= tsum      

END

;________________________________________________________________________________________________________
;

FUNCTION LP_DARK_IMAGE, caller

COMMON LP_DARK_IMAGE_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE

    IF XRegistered("LP_DARK_IMAGE") THEN RETURN,1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 2
    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpdarkv))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       tele = 0
       camtable = 0
       iptable = 0
       ;fps = 0 ; off
       fps = 1 ; on
       expt = 0L
    ENDIF ELSE BEGIN
       tele = lpdarkv.tele
       camtable = lpdarkv.camtable
       iptable = lpdarkv.iptable
       fps = lpdarkv.fps
       expt = lpdarkv.expt
    ENDELSE

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/COLUMN, TITLE='LP DARK_IMAGE', /FRAME, $
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW)

      col = WIDGET_BASE(row0, /COLUMN, /FRAME)
        telepd =  CW_BSELECTOR2(col, tele_types, SET_VALUE=tele)
        ;iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=iptable)
        ;iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable

        IF (!version.release LT '5.6') THEN BEGIN
          iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE

        ;camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=camtable) ; AEE 6/4/04
        camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=camtable) 
      row = WIDGET_BASE(col, /ROW)
        tmp= WIDGET_LABEL(row,VALUE= 'EUVI FPS') ; AEE 1/13/04
        fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

      row = WIDGET_BASE(col, /ROW)
        temp = WIDGET_LABEL(row, VALUE='Exposure Time (ms):')
        exp_text = WIDGET_TEXT(row, /EDITABLE, YSIZE=1, XSIZE=8, VALUE=STRTRIM(STRING(expt),2))
      font= '-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
      row = WIDGET_BASE(col, /ROW)
      ccdsum= (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
      gmode= ccd(tele,camtable).gmode
      IF (ccdsum EQ 1 AND gmode EQ 0) THEN BEGIN 
        ; no ccd summing and gain is high ( no need to adjust exptime):
        tsum='                                                                                                                                    '
      ENDIF ELSE BEGIN
        tsum=''
        IF (ccdsum GT 1) THEN BEGIN 
          ; ccd summing:
          tsum= 'Please adjust exposure for '+$
                STRTRIM(ccd(tele,camtable).xsum > 1,2)+'x'+ $
                STRTRIM(ccd(tele,camtable).ysum > 1,2)+' CCD summing (divide by '+ $
                STRTRIM(ccdsum,2)+')'
        ENDIF
        IF (gmode EQ 1) THEN BEGIN 
          ; low gain (user has to amplify exptime by 4):
          IF (STRLEN(tsum) GT 0) THEN $ 
            tsum= tsum+' & low gain (amplify by 4)' $
          ELSE $
            tsum= 'Please adjust exposure for low gain (amplify by 4)'
        ENDIF
      ENDELSE

      tsum= '   ' ; Ignore ccd_sum and low gain exptime correction (FSW does not do it).

      tsum= 'Exp Range = 0 - 99998 ms'
      msid = WIDGET_LABEL(row, FONT= font, VALUE=tsum)

     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
    WIDGET_CONTROL, exp_text, /INPUT_FOCUS
    WIDGET_CONTROL, exp_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(expt),2))]

    lpdarkv = CREATE_STRUCT( 'base', base,			$
                             'telepd', telepd,			$
                             'camtable_pd', camtable_pd,	$ ; AEE 1/13/04
                             'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'iptable_pd', iptable_pd,	        $
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             'exp_text', exp_text,		$
                             'expt', expt,			$
                             'tele', tele,			$
                             'camtable', camtable,		$ ; AEE 1/13/04
                             'fps', fps,                        $ ; AEE 1/13/04
                             'msid',msid,                       $
                             'iptable', iptable )

    IF (tele NE 0) THEN WIDGET_CONTROL, lpdarkv.fps_pd, SENSITIVE=0
    XMANAGER, "LP_DARK_IMAGE", base
    RETURN, 1

END

;________________________________________________________________________________________________________
;
 
PRO LP_CAL_LAMP_EVENT, event

COMMON LP_CAL_LAMP_SHARE, lpcalv
COMMON OS_INIT_SHARE
COMMON OS_SHARE
COMMON OS_ALL_SHARE

CASE (event.id) OF

   lpcalv.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpcalv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END

   lpcalv.schedule : BEGIN
      WIDGET_CONTROL, lpcalv.pulse_pd, GET_VALUE=pulse & pulse = LONG(pulse(0)) ; AEE - 11/18/03
      lpcalv.pulse= pulse
      IF (lpcalv.pulse LT lpcalv.prange(0) OR lpcalv.pulse GT lpcalv.prange(1)) THEN BEGIN
        WIDGET_CONTROL, lpcalv.diag, SET_VALUE= '** Error: Invalid Pulse **'
        PRINT,'Pulse = '+STRTRIM(pulse,2)+'** Error: Invalid Pulse **'
        RETURN
      ENDIF ELSE BEGIN
        WIDGET_CONTROL, lpcalv.diag, SET_VALUE= ''
        WIDGET_CONTROL,osv.info_text,SET_VALUE='Calibration Lamp Exposure'
        IF (lpcalv.tele NE 0) THEN lpcalv.fps= 0 ; FPS is only valid for EUVI
        OS_CALL_SCHEDULE
        plan_lp = -2 ; AEE - 9/30/03 
        WIDGET_CONTROL, /DESTROY, lpcalv.base
        WIDGET_CONTROL, lprow, SENSITIVE=1
        RETURN
      ENDELSE
   END
 
   lpcalv.telepd : BEGIN
      ; AEE - 8/20/03
      IF (lpcalv.tele NE event.index) THEN BEGIN 	;** only change lamp label(s) if tele changed
        lpcalv.tele = event.index
        WIDGET_CONTROL, lpcalv.fwpd, SET_VALUE=fw_types(lpcalv.exptable,*,lpcalv.tele)
        WIDGET_CONTROL, lpcalv.fwpd, SET_VALUE=0
        lpcalv.fw=0
        WIDGET_CONTROL, lpcalv.pwpd, SET_VALUE=pw_types(lpcalv.exptable,*,lpcalv.tele)
        WIDGET_CONTROL, lpcalv.pwpd, SET_VALUE=0
        lpcalv.pw=0
        lpcalv.lamp= 0
        WIDGET_CONTROL, lpcalv.pulse_pd, SET_VALUE= STRTRIM(lpcalv.prange(0),2) 
        lpcalv.pulse= lpcalv.prange(0) 
        WIDGET_CONTROL, lpcalv.diag, SET_VALUE= ''
        p = WIDGET_INFO(lpcalv.lamppd, /PARENT)
        WIDGET_CONTROL, /DESTROY, lpcalv.lamppd 
        CASE lpcalv.tele of
           0: lpcalv.lamppd = CW_BSELECTOR2(p, lpcalv.euvi_lamps, SET_VALUE=lpcalv.lamp)
           1: lpcalv.lamppd = CW_BSELECTOR2(p, lpcalv.cor1_lamps, SET_VALUE=lpcalv.lamp)
           2: lpcalv.lamppd = CW_BSELECTOR2(p, lpcalv.cor2_lamps, SET_VALUE=lpcalv.lamp)
           3: lpcalv.lamppd = CW_BSELECTOR2(p, lpcalv.hi1_lamps, SET_VALUE=lpcalv.lamp) 
           4: lpcalv.lamppd = CW_BSELECTOR2(p, lpcalv.hi2_lamps, SET_VALUE=lpcalv.lamp)
        ENDCASE
      IF (lpcalv.tele EQ 0) THEN $
        WIDGET_CONTROL, lpcalv.fps_pd, SENSITIVE=1 $
      ELSE $
        WIDGET_CONTROL, lpcalv.fps_pd, SENSITIVE=0
      ENDIF

      IF ((lpcalv.tele EQ 0 AND lpcalv.lamp GT 0) OR (lpcalv.tele EQ 2 AND lpcalv.lamp EQ 0)) THEN $ 
        ; telescope mounted LED:
        lpcalv.prange= lpcalv.ptrange $
      ELSE $ ; FPA mounted LED
        lpcalv.prange= lpcalv.pfrange

      WIDGET_CONTROL,lpcalv.plabel,SET_VALUE= $
       'Enter # of Pulses ('+STRTRIM(lpcalv.prange(0),2)+'-'+STRTRIM(lpcalv.prange(1),2)+')'

   END
 
   lpcalv.lamppd : BEGIN
      lpcalv.lamp = event.index

      IF ((lpcalv.tele EQ 0 AND lpcalv.lamp GT 0) OR (lpcalv.tele EQ 2 AND lpcalv.lamp EQ 0)) THEN $
        ; telescope mounted LED:
        lpcalv.prange= lpcalv.ptrange $
      ELSE $ ; FPA mounted LED
        lpcalv.prange= lpcalv.pfrange

      WIDGET_CONTROL,lpcalv.plabel,SET_VALUE= $
       'Enter # of Pulses ('+STRTRIM(lpcalv.prange(0),2)+'-'+STRTRIM(lpcalv.prange(1),2)+')'

   END

   lpcalv.iptable_pd : BEGIN
      lpcalv.iptable = event.index
   END

   lpcalv.camtable_pd : BEGIN ; AEE 1/13/04
      lpcalv.camtable = event.index
   END

   lpcalv.fps_pd : BEGIN ; AEE 1/13/04
      lpcalv.fps = event.index
   END

   lpcalv.fwpd : BEGIN
      ;IF selected filter is blank, ignore it and use the previously selected/displayed filter, instead:
      IF (STRTRIM(fw_types(lpcalv.exptable,event.index,lpcalv.tele),2) EQ '') THEN $  
        WIDGET_CONTROL, lpcalv.fwpd, SET_VALUE=lpcalv.fw          $
      ELSE                                                          $ 
        lpcalv.fw = event.index
   END

   lpcalv.pwpd : BEGIN
      ;IF selected polar is blank, ignore it and use the previously selected/displayed polar, instead:
      IF (STRTRIM(pw_types(lpcalv.exptable,event.index,lpcalv.tele),2) EQ '') THEN $ 
        WIDGET_CONTROL, lpcalv.pwpd, SET_VALUE=lpcalv.pw          $
      ELSE                                                          $
        lpcalv.pw = event.index
   END

   lpcalv.pulse_pd : BEGIN
      WIDGET_CONTROL, lpcalv.pulse_pd, GET_VALUE=pulse & pulse = LONG(pulse(0)) ; AEE - 11/18/03

      IF (pulse LT lpcalv.prange(0) OR pulse GT lpcalv.prange(1)) THEN BEGIN
        WIDGET_CONTROL, lpcalv.diag, SET_VALUE= 'Error: Invalid Pulse'
        PRINT,'Pulse = '+STRTRIM(pulse,2)+'Error: Invalid Pulse'
      ENDIF ELSE BEGIN
         WIDGET_CONTROL, lpcalv.diag, SET_VALUE= ''
      ENDELSE
      lpcalv.pulse = pulse

   END

   ELSE : BEGIN
   END

ENDCASE

ccdsum= (ccd(lpcalv.tele,lpcalv.camtable).xsum > 1) * (ccd(lpcalv.tele,lpcalv.camtable).ysum > 1)
gmode= ccd(lpcalv.tele,lpcalv.camtable).gmode
IF (ccdsum EQ 1 AND gmode EQ 0) THEN BEGIN
  ; no ccd summing and gain is high ( no need to adjust pulse):
  tsum='                                                                                                                                 ' 
ENDIF ELSE BEGIN
  tsum=''
  IF (ccdsum GT 1) THEN BEGIN
    ; ccd summing:
    tsum= 'Please adjust pulse for '+$
          STRTRIM(ccd(lpcalv.tele,lpcalv.camtable).xsum > 1,2)+'x'+ $
          STRTRIM(ccd(lpcalv.tele,lpcalv.camtable).ysum > 1,2)+' CCD summing (divide by '+ $
          STRTRIM(ccdsum,2)+')'
  ENDIF
  IF (gmode EQ 1) THEN BEGIN
    ; low gain (user has to amplify pulse by 4):
    IF (STRLEN(tsum) GT 0) THEN $
      tsum= tsum+' & low gain (amplify by 4)' $
    ELSE $
      tsum= 'Please adjust pulse for low gain (amplify by 4)'
  ENDIF
ENDELSE

tsum= '   ' ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

WIDGET_CONTROL, lpcalv.msid, SET_VALUE= tsum

END

;________________________________________________________________________________________________________
;

FUNCTION LP_CAL_LAMP, caller

COMMON LP_CAL_LAMP_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE

    IF XRegistered("LP_CAL_LAMP") THEN RETURN,1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 3
    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpcalv))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       tele = 0
       camtable = 0
       iptable = 0
       ;fps = 0 ; off
       fps = 1 ; on
       fw = 0
       pw = 0
       lamp = 0
       pulse= 1L 
    ENDIF ELSE BEGIN
       tele = lpcalv.tele
       camtable = lpcalv.camtable
       iptable = lpcalv.iptable
       fps = lpcalv.fps
       fw = lpcalv.fw
       pw = lpcalv.pw
       lamp = lpcalv.lamp
       pulse = lpcalv.pulse
    ENDELSE

    exptable= 0 ; AEE - 3/29/04 - use 1st exptable for now (also for continious images).

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    ;base = WIDGET_BASE(/COLUMN, TITLE='LP CAL_LAMP', /FRAME, $
    base = WIDGET_BASE(/COLUMN, TITLE='LP CAL_LED', /FRAME, $
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW, /FRAME)

      col = WIDGET_BASE(row0, /COLUMN)
        telepd =  CW_BSELECTOR2(col, tele_types, SET_VALUE=tele)
        ;iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=iptable)
        ;iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable

        IF (!version.release LT '5.6') THEN BEGIN
          iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE

        ;camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=camtable) ; AEE 6/4/04
        camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=camtable) 
      row = WIDGET_BASE(col, /ROW)
        tmp= WIDGET_LABEL(row,VALUE= 'EUVI FPS') ; AEE 1/13/04
        fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

        pfrange= [1L,9999999L] ; LED function code L allows 7 bytes (9999999) pulses for FPA mounted LEDs
                              ; and 3126098 pulses for telescope mounted (require shutter monvement) LEDs.
        ptrange=[1L,3126098L]

        IF (tele EQ 0 AND lamp GT 0 OR tele EQ 2 AND lamp EQ 0) THEN $ ; telescope mounted LED
          prange= ptrange $
        ELSE $ ; FPA mounted LED
          prange= pfrange

        ptxt='Enter # of Pulses ('+STRTRIM(prange(0),2)+'-'+STRTRIM(prange(1),2)+')'
        plabel = WIDGET_LABEL(col, VALUE=ptxt)
        pulse_pd = WIDGET_TEXT(col, /EDITABLE, YSIZE=1, XSIZE=4, VALUE= STRTRIM(pulse,2))

        font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

      ccdsum= (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
      gmode= ccd(tele,camtable).gmode
      IF (ccdsum EQ 1 AND gmode EQ 0) THEN BEGIN
        ; no ccd summing and gain is high ( no need to adjust pulse):
        tsum='                                                                                                                                 ' 
      ENDIF ELSE BEGIN
        tsum=''
        IF (ccdsum GT 1) THEN BEGIN
          ; ccd summing:
          tsum= 'Please adjust pulse for '+$
                STRTRIM(ccd(tele,camtable).xsum > 1,2)+'x'+ $
                STRTRIM(ccd(tele,camtable).ysum > 1,2)+' CCD summing (divide by '+ $
                STRTRIM(ccdsum,2)+')'
        ENDIF
        IF (gmode EQ 1) THEN BEGIN
          ; low gain (user has to amplify pulse by 4):
          IF (STRLEN(tsum) GT 0) THEN $
            tsum= tsum+' & low gain (amplify by 4)' $
          ELSE $
            tsum= 'Please adjust pulse for low gain (amplify by 4)'
        ENDIF
      ENDELSE

      tsum='   ' ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

      ;msid= WIDGET_LABEL(col, FONT= font, VALUE=tsum)
      msid= WIDGET_LABEL(base, FONT= font, VALUE=tsum)

        diag = WIDGET_LABEL(col,FONT=font,VALUE='                                          ')
      col = WIDGET_BASE(row0, /COLUMN)
        fwpd = CW_BSELECTOR2(col, fw_types(exptable,*,tele), SET_VALUE=fw)
        pwpd = CW_BSELECTOR2(col, pw_types(exptable,*,tele), SET_VALUE=pw)


        ; AEE - 8/20/03:

        ;euvi_lamps= ['Blu FPA','Blu Tel','Vio TBD']
        ;cor1_lamps= ['Red FPA','Blue FPA','Vio FPA']
        ;cor2_lamps= ['Red Tel (REQUIRES DOOR-CLOSURE)','Blu FPA','Vio FPA']
        ;hi1_lamps=  ['Red FPA','Blu FPA','Vio FPA']
        ;hi2_lamps=  ['Red FPA','Blu FPA','Vio FPA']

        ;euvi_lamps= ['RED FPA (actual-color=Blue)',   $
        ;             'PRP Tel (actual-color=Purple)', $
        ;             'BLU Tel (actual-color=Blue)']
        ;cor1_lamps= ['Red FPA (actual-color=Red)',    $
        ;             'PRP FPA (actual-color=Violet)', $
        ;             'Blue FPA (actual-color=Blue)'] 
        ;cor2_lamps= ['Red Tel Door Closed Only (actual-color=Red)', $
        ;             'PRP FPA (actual-color=Red)',                  $
        ;             'BLU FPA (actual-color=Blue)']
        ;hi1_lamps=  ['RED FPA (actual-color=Red)',    $
        ;             'PRP FPA (actual-color=Violet)', $ 
        ;             'Blue FPA (actual-color=Blue)']
        ;hi2_lamps=  ['RED FPA (actual-color=Red)',    $
        ;             'PRP FPA (actual-color=Violet)', $ 
        ;             'Blue FPA (actual-color=Blue)']
       
        ;Above lamps are now in  secchi_cal_leds.sav file. Now PRP_LED = 1
        ;and BLU_LED = 2:

        RESTORE, FILENAME= GETENV('PT')+'/IN/OTHER/secchi_cal_leds.sav' 
        ; => euvi_lamps, cor1_lamps, cor2_lamps, hi1_lamps, hi2_lamps

        CASE tele of
           0: lamppd = CW_BSELECTOR2(col, euvi_lamps, SET_VALUE=lamp)
           1: lamppd = CW_BSELECTOR2(col, cor1_lamps, SET_VALUE=lamp)
           2: lamppd = CW_BSELECTOR2(col, cor2_lamps, SET_VALUE=lamp)
           3: lamppd = CW_BSELECTOR2(col, hi1_lamps, SET_VALUE=lamp) 
           4: lamppd = CW_BSELECTOR2(col, hi2_lamps, SET_VALUE=lamp)
        ENDCASE



     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    IF (pulse LT prange(0)-1 OR pulse GT prange(1)) THEN BEGIN 
      WIDGET_CONTROL, diag, SET_VALUE= 'Error: Invalid Pulse' 
      PRINT,'Pulse = '+STRTRIM(pulse,2)+'Error: Invalid Pulse'
    ENDIF ELSE BEGIN 
      WIDGET_CONTROL, diag, SET_VALUE= ''
    ENDELSE

    lpcalv = CREATE_STRUCT( 'base', base,			$
                             'telepd', telepd,			$
                             'camtable_pd', camtable_pd,	$ ; AEE 1/13/04
                             'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'iptable_pd', iptable_pd,		$
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             'fwpd', fwpd,			$
                             'pwpd', pwpd,			$
                             'fw', fw,				$
                             'pw', pw,				$
                             ;'lamp_types', lamp_types,		$
                             'lamp', lamp,			$
                             'euvi_lamps', euvi_lamps,          $
                             'cor1_lamps', cor1_lamps,          $
                             'cor2_lamps', cor2_lamps,          $
                             'hi1_lamps', hi1_lamps,            $
                             'hi2_lamps', hi2_lamps,            $
                             'lamppd', lamppd,			$
                             'pulse_pd', pulse_pd,              $
                             'pulse', pulse,                    $
                             'prange',prange,                   $
                             'ptrange',ptrange,                 $
                             'pfrange',pfrange,                 $
                             'plabel',plabel,                   $
                             'diag', diag,                      $
                             'tele', tele,			$
                             'camtable', camtable,              $ ; AEE 1/13/04
                             'fps', fps,                        $ ; AEE 1/13/04
                             'exptable', exptable,              $ ; AEE 3/29/04
                             'msid', msid,                      $
                             'iptable', iptable )

    IF (tele NE 0) THEN WIDGET_CONTROL, lpcalv.fps_pd, SENSITIVE=0
    XMANAGER, "LP_CAL_LAMP", base

    RETURN, 1

END

;________________________________________________________________________________________________________
;

PRO LP_HI_SEQ_EVENT, event

COMMON LP_HI_SEQ_SHARE, lpseqhi
COMMON OS_INIT_SHARE
COMMON OS_SHARE
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld

CASE (event.id) OF

   lpseqhi.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpseqhi.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END

   lpseqhi.schedule : BEGIN
      WIDGET_CONTROL,osv.info_text,SET_VALUE='HI Sequence'

      IF (lpseqhi.hi1_images EQ 0 AND lpseqhi.hi2_images EQ 0) THEN BEGIN
        WIDGET_CONTROL,lpseqhi.info,SET_VALUE='No HI1 or HI2 images were selected.'
        PRINT,'No HI1 or HI2 images were selected.'
        RETURN
      ENDIF

      IF (lpseqhi.hi1_images GT 0 AND lpseqhi.hi2_images GT 0) THEN BEGIN
        WIDGET_CONTROL,lpseqhi.info,SET_VALUE='Single Tel Sequences Only (Select Images From Either HI1 or HI2)'
        PRINT,'Single Tel Sequences Only (Select Images From Either HI1 or HI2'
        RETURN
      ENDIF

      WIDGET_CONTROL, lpseqhi.hi1_cad, GET_VALUE= cadence
      lpseqhi.hi1_cadence= FIX(cadence(0))
      WIDGET_CONTROL, lpseqhi.hi2_cad, GET_VALUE= cadence
      lpseqhi.hi2_cadence= FIX(cadence(0))

      IF (lpseqhi.hi1_images GT 0) THEN BEGIN 
        iptab= lpseqhi.hi1_iptable 
        lpseqhi.hi2_iptable= 0
      ENDIF ELSE BEGIN
        iptab= lpseqhi.hi2_iptable
        lpseqhi.hi1_iptable= 0
      ENDELSE

      lpseqhi.fps= 0 ; FPS is only valid for EUVI
      OS_CALL_SCHEDULE
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpseqhi.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END
 
   lpseqhi.hi1_numpd : BEGIN
      lpseqhi.hi1_images = lpseqhi.image_arr(event.index) ; AEE - 01/08/03
   END

   lpseqhi.hi1_cad : BEGIN
      WIDGET_CONTROL, lpseqhi.hi1_cad, GET_VALUE= cadence
      lpseqhi.hi1_cadence= FIX(cadence(0))
   END

   lpseqhi.hi1_iptable_pd : BEGIN
      lpseqhi.hi1_iptable = event.index
   END

   lpseqhi.hi1_exptable_pd : BEGIN  ; AEE 1/13/04
      lpseqhi.hi1_exptable = event.index
   END

   lpseqhi.hi1_camtable_pd : BEGIN ; AEE 1/13/04
      lpseqhi.hi1_camtable = event.index
   END

   lpseqhi.hi2_exptable_pd : BEGIN  ; AEE 1/13/04
      lpseqhi.hi2_exptable = event.index
   END

   lpseqhi.hi2_camtable_pd : BEGIN ; AEE 1/13/04
      lpseqhi.hi2_camtable = event.index
   END

;   lpseqhi.fps_pd : BEGIN ; AEE 1/13/04
;      lpseqhi.fps = event.index
;   END

   lpseqhi.hi2_numpd : BEGIN
      lpseqhi.hi2_images = lpseqhi.image_arr(event.index) ; AEE - 01/08/03
   END

   lpseqhi.hi2_cad : BEGIN
      WIDGET_CONTROL, lpseqhi.hi2_cad, GET_VALUE= cadence
      lpseqhi.hi2_cadence= FIX(cadence(0))
   END

   lpseqhi.hi2_iptable_pd : BEGIN
      lpseqhi.hi2_iptable = event.index
   END

ENDCASE

IF (lpseqhi.hi1_images EQ 0 AND lpseqhi.hi2_images EQ 0) THEN BEGIN
 WIDGET_CONTROL, lpseqhi.exsumid, SET_VALUE= '                                                                       '
ENDIF ELSE BEGIN
 IF (lpseqhi.hi1_images GT 0) THEN BEGIN
  t= 3 
  ct= lpseqhi.hi1_camtable
  e= lpseqhi.hi1_exptable
  cm= ' HI1 '
 ENDIF
 IF (lpseqhi.hi2_images GT 0) THEN BEGIN
  t=4
  ct= lpseqhi.hi2_camtable
  e= lpseqhi.hi2_exptable
  cm= ' HI2 '
 ENDIF
 ccdsum= (ccd(t,ct).xsum > 1) * (ccd(t,ct).ysum > 1)
gmode= 'High'
IF (ccd(t,ct).gmode EQ 1) THEN BEGIN
  gmode='Low'
  ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
ENDIF

ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

 tsumexp= 'ExpTime (CCDsum '+STRTRIM(ccd(t,ct).xsum > 1,2)+'x'+ $
          STRTRIM(ccd(t,ct).ysum > 1,2)+' & gain='+gmode+') for all'+cm+'images = '
 WIDGET_CONTROL, lpseqhi.exsumid, SET_VALUE= tsumexp+STRING(ex(t,e,0,0)/1000.0/ccdsum,'(F6.2)')  + ' Sec'
ENDELSE

IF (lpseqhi.hi1_images GT 0 AND lpseqhi.hi2_images GT 0) THEN BEGIN
  WIDGET_CONTROL, lpseqhi.exsumid, SET_VALUE= '                                                                       '
  WIDGET_CONTROL, lpseqhi.info, SET_VALUE= 'Single Tel Sequences Only (Select Images From Either HI1 or HI2)' 
ENDIF

END

;________________________________________________________________________________________________________
;

FUNCTION LP_HI_SEQ, caller

COMMON LP_HI_SEQ_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE

    IF XRegistered("LP_HI_SEQ") THEN RETURN,1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 6 
    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpseqhi))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       hi1_exptable = 0 ; AEE 1/13/04
       hi1_camtable = 0 ; AEE 1/13/04
       hi1_iptable = 0
       fps = 0 ; off (only EUVI uses fps but we need to carry fps byte for all upload commands
               ;      so just use 0 for default).
       hi1_images= 0 ; AEE - 01/08/03 
       hi1_cadence= 0 ; default = 0 = take images as close together as possible.
       hi2_exptable = 0 ; AEE 1/13/04
       hi2_camtable = 0 ; AEE 1/13/04
       hi2_iptable = 0
       hi2_images= 0  ; AEE - 01/08/03
       hi2_cadence= 0 ; default = 0 = take images as close together as possible.
    ENDIF ELSE BEGIN
       fps= lpseqhi.fps
       hi1_exptable = lpseqhi.hi1_exptable
       hi1_camtable = lpseqhi.hi1_camtable
       hi1_iptable = lpseqhi.hi1_iptable
       hi1_images = lpseqhi.hi1_images
       hi1_cadence= lpseqhi.hi1_cadence  ; cadence (for between images in the HI1 sequence)
       hi2_exptable = lpseqhi.hi2_exptable
       hi2_camtable = lpseqhi.hi2_camtable
       hi2_iptable = lpseqhi.hi2_iptable
       hi2_images = lpseqhi.hi2_images
       hi2_cadence= lpseqhi.hi2_cadence  ; cadence (for between images in the HI2 sequence)
    ENDELSE

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

   image_arr =   ['1 image  ', $
                  '2 images ', $
                  '3 images ', $
                  '4 images ', $
                  '5 images ', $
                  '6 images ', $
                  '7 images ', $
                  '8 images ', $
                  '9 images ', $
                  '10 images ', $
                  '11 images ', $
                  '12 images ', $
                  '13 images ', $
                  '14 images ', $
                  '15 images ', $
                  '16 images ', $
                  '17 images ', $
                  '18 images ', $
                  '19 images ', $
                  '20 images ', $
                  '21 images ', $
                  '22 images ', $
                  '23 images ', $
                  '24 images ', $
                  '25 images ', $
                  '26 images ', $
                  '27 images ', $
                  '28 images ', $
                  '29 images ', $
                  '30 images ', $
;                  '40 images ', $
;                  '50 images ', $
;                  '60 images ', $
;                  '70 images ']
                  '31 images ', $
                  '32 images ', $
                  '33 images ', $
                  '34 images ', $
                  '35 images ', $
                  '36 images ', $
                  '37 images ', $
                  '38 images ', $
                  '39 images ', $
                  '40 images ', $
                  '41 images ', $
                  '42 images ', $
                  '43 images ', $
                  '44 images ', $
                  '45 images ', $
                  '46 images ', $
                  '47 images ', $
                  '48 images ', $
                  '49 images ', $
                  '50 images ', $
                  '51 images ', $
                  '52 images ', $
                  '53 images ', $
                  '54 images ', $
                  '55 images ', $
                  '56 images ', $
                  '57 images ', $
                  '58 images ', $
                  '59 images ', $
                  '60 images ', $
                  '61 images ', $
                  '62 images ', $
                  '63 images ', $
                  '64 images ', $
                  '65 images ', $
                  '66 images ', $
                  '67 images ', $
                  '68 images ', $
                  '69 images ', $
   ;               '70 images ']
                  '70 images ', $
                  '71 images ', $
                  '72 images ', $
                  '73 images ', $
                  '74 images ', $
                  '75 images ', $
                  '76 images ', $
                  '77 images ', $
                  '78 images ', $
                  '79 images ', $
                  '80 images ', $
                  '81 images ', $
                  '82 images ', $
                  '83 images ', $
                  '84 images ', $
                  '85 images ', $
                  '86 images ', $
                  '87 images ', $
                  '88 images ', $
                  '89 images ', $
                  '90 images ', $
                  '91 images ', $
                  '92 images ', $
                  '93 images ', $
                  '94 images ', $
                  '95 images ', $
                  '96 images ', $
                  '97 images ', $
                  '98 images ', $
                  '99 images ']



   image_arr =   ['0 image  ',image_arr] ; AEE - 01/08/03

   intcnt= FIX(STRMID(STRTRIM(image_arr,2),0,2))  ; AEE - 01/23/03

    base = WIDGET_BASE(/COLUMN, TITLE='LP HI_SEQ', /FRAME, $
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW)

      col = WIDGET_BASE(row0, /COLUMN, /FRAME)
        tmp =  WIDGET_LABEL(col, VALUE= "HI 1")
        ;hi1_iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=hi1_iptable)
        ;hi1_iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, hi1_iptable_pd, SET_DROPLIST_SELECT= hi1_iptable

        IF (!version.release LT '5.6') THEN BEGIN
          hi1_iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, hi1_iptable_pd, SET_DROPLIST_SELECT= hi1_iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          hi1_iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,hi1_iptable) ; so idl5.3 does not complain.
        ENDELSE

        hi1_exptable_pd = CW_BSELECTOR2(col, "Exposure "+table_types, SET_VALUE=hi1_exptable)
        ;hi1_camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=hi1_camtable) ; AEE 6/4/04
        hi1_camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=hi1_camtable)
        ;hi1_numpd = CW_BSELECTOR2(col, image_arr, SET_VALUE= (WHERE(intcnt EQ hi1_images))(0)) ; AEE - 01/23/03 

        ;hi1_numpd = WIDGET_DROPLIST(col, VALUE=image_arr)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, hi1_numpd, SET_DROPLIST_SELECT= (WHERE(intcnt EQ hi1_images))(0)

        IF (!version.release LT '5.6') THEN BEGIN
          hi1_numpd = WIDGET_DROPLIST(col, VALUE=image_arr)
          WIDGET_CONTROL, hi1_numpd, SET_DROPLIST_SELECT= (WHERE(intcnt EQ hi1_images))(0) 
        ENDIF ELSE BEGIN
          hi1_numpd = WIDGET_COMBOBOX_SHELL(col,image_arr,(WHERE(intcnt EQ hi1_images))(0)) ; so idl5.3 does not complain.
        ENDELSE

        lab = WIDGET_LABEL(col,VALUE='Cadence (Sec)')
        hi1_cad = WIDGET_TEXT(col, XSIZE=15, /EDITABLE, VALUE= STRTRIM(hi1_cadence,2))

      col = WIDGET_BASE(row0, /COLUMN, /FRAME)
        tmp =  WIDGET_LABEL(col, VALUE= "HI 2")
        ;hi2_iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=hi2_iptable)
        ;hi2_iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, hi2_iptable_pd, SET_DROPLIST_SELECT= hi2_iptable

        IF (!version.release LT '5.6') THEN BEGIN
          hi2_iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, hi2_iptable_pd, SET_DROPLIST_SELECT= hi2_iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          hi2_iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,hi2_iptable) ; so idl5.3 does not complain.
        ENDELSE

        hi2_exptable_pd = CW_BSELECTOR2(col, "Exposure "+table_types, SET_VALUE=hi2_exptable)
        ;hi2_camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=hi2_camtable) ; AEE 6/4/04
        hi2_camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=hi2_camtable)
        ;hi2_numpd = CW_BSELECTOR2(col, image_arr(0:25), SET_VALUE= (WHERE(intcnt EQ hi2_images))(0));AEE-01/23/03
        ;hi2_numpd = WIDGET_DROPLIST(col, VALUE=image_arr(0:25))
        ;hi2_numpd = WIDGET_DROPLIST(col, VALUE=image_arr) ; AEE 12/06/04
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, hi2_numpd, SET_DROPLIST_SELECT= (WHERE(intcnt EQ hi2_images))(0)

        IF (!version.release LT '5.6') THEN BEGIN
          hi2_numpd = WIDGET_DROPLIST(col, VALUE=image_arr)
          WIDGET_CONTROL, hi2_numpd, SET_DROPLIST_SELECT= (WHERE(intcnt EQ hi2_images))(0) 
        ENDIF ELSE BEGIN
          hi2_numpd = WIDGET_COMBOBOX_SHELL(col,image_arr,(WHERE(intcnt EQ hi2_images))(0)) ; so idl5.3 does not complain.
        ENDELSE

        lab = WIDGET_LABEL(col,VALUE='Cadence (Sec)')
        hi2_cad = WIDGET_TEXT(col, XSIZE=15, /EDITABLE, VALUE= STRTRIM(hi2_cadence,2))

        ;lab = WIDGET_LABEL(base,VALUE='0 = Default Cadence')
        lab = WIDGET_LABEL(base,VALUE='0 = Default Cadence (take images as close together as possible)')

        font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

        ;row= WIDGET_BASE(base,/ROW)
        IF (hi1_images EQ 0 AND hi2_images EQ 0) THEN BEGIN
          expmsg= '                                                                                                            '
          ;exsumid= WIDGET_LABEL(base, FONT= font, VALUE= '                                                                                                            ')
        ENDIF ELSE BEGIN
         IF (hi1_images GT 0) THEN BEGIN
           cm= ' HI1 '
           t= 3 
           ct= hi1_camtable
           e= hi1_exptable
         ENDIF
         IF (hi2_images GT 0) THEN BEGIN
           cm= ' HI2 '
           t= 4 
           ct= hi2_camtable
           e= hi2_exptable
         ENDIF
         ccdsum= (ccd(t,ct).xsum > 1) * (ccd(t,ct).ysum > 1)
         gmode= 'High '
         IF (ccd(t,ct).gmode EQ 1) THEN BEGIN
           gmode='Low'
           ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
         ENDIF

         ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

         tsumexp= 'ExpTime (CCDsum '+STRTRIM(ccd(t,ct).xsum > 1,2)+'x'+ $
                  STRTRIM(ccd(t,ct).ysum > 1,2)+' & gain='+gmode+') for all'+cm+'images = '
         expmsg= tsumexp+STRING(ex(t,e,0,0)/1000.0/ccdsum,'(F6.2)')  + ' Sec' 
         ;exsumid= WIDGET_LABEL(base, FONT= font, VALUE= tsumexp+ $
         ;         STRING(ex(t,e,0,0)/1000.0/ccdsum,'(F6.2)')  + ' Sec')
        ENDELSE

        exsumid= WIDGET_LABEL(base, FONT= font, VALUE= expmsg) 

        ;tmp= WIDGET_LABEL(row,VALUE= '                  EUVI FPS') ; AEE 1/13/04
        ;fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')
     tmp= WIDGET_LABEL(base, VALUE='')

     info= WIDGET_LABEL(base, FONT=font, VALUE='                                Single Telescope Sequences Only                                ')


    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    IF (hi1_images GT 0 AND hi2_images GT 0) THEN BEGIN
      WIDGET_CONTROL, exsumid, SET_VALUE= '                                                                       '
      WIDGET_CONTROL, info, SET_VALUE= 'Single Tel Sequences Only (Select Images From Either HI1 or HI2)' 
    ENDIF

    lpseqhi = CREATE_STRUCT('base', base,			$
                             'image_arr', image_arr,            $
                             'hi1_exptable_pd', hi1_exptable_pd,$ ; AEE 1/13/04
                             'hi1_camtable_pd', hi1_camtable_pd,$ ; AEE 1/13/04
                             'hi1_iptable_pd', hi1_iptable_pd,	$
                             'hi1_images', hi1_images,		$
                             'hi1_numpd', hi1_numpd,		$
                             'hi1_cad', hi1_cad,                $
                             'hi1_cadence', hi1_cadence,        $
                             'hi1_exptable', hi1_exptable,	$ ; AEE 1/13/04
                             'hi1_camtable', hi1_camtable,      $ ; AEE 1/13/04
                             'hi1_iptable', hi1_iptable,        $
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             ;'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'fps', fps,                        $ ; AEE 1/13/04
                             'hi2_exptable_pd', hi2_exptable_pd,$ ; AEE 1/13/04
                             'hi2_camtable_pd', hi2_camtable_pd,$ ; AEE 1/13/04
                             'hi2_iptable_pd', hi2_iptable_pd,  $
                             'hi2_images', hi2_images,          $
                             'hi2_numpd', hi2_numpd,            $
                             'hi2_cad', hi2_cad,                $
                             'hi2_cadence', hi2_cadence,        $
                             'hi2_exptable', hi2_exptable,      $ ; AEE 1/13/04
                             'hi2_camtable', hi2_camtable,      $ ; AEE 1/13/04
                             'info', info,                      $
                             'exsumid', exsumid,                $
                             'hi2_iptable', hi2_iptable )


    XMANAGER, "LP_HI_SEQ", base

    RETURN, 1

END


;-------------------------------------------------------------------------------------

 
PRO LP_SCIP_SEQ_EVENT, event 

COMMON LP_SEQ_PW_FW_SHARE, lpseqpwv
COMMON OS_INIT_SHARE
COMMON OS_SHARE
COMMON OS_ALL_SHARE

CASE (event.id) OF

   lpseqpwv.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpseqpwv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END

   lpseqpwv.schedule : BEGIN
      WIDGET_CONTROL,osv.info_text,SET_VALUE='Sequence Polarizer at Filter'
      WIDGET_CONTROL, lpseqpwv.img_cad, GET_VALUE= cadence
      lpseqpwv.cadence= FIX(cadence(0))

      IF (lpseqpwv.tele NE 0) THEN lpseqpwv.fps= 0 ; FPS is only valid for EUVI
      OS_CALL_SCHEDULE
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpseqpwv.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END
 
   lpseqpwv.telepd : BEGIN
     IF (lpseqpwv.tele NE event.index) THEN BEGIN
      lpseqpwv.tele = event.index
      ;** set the fw for the given telescope
      WIDGET_CONTROL, lpseqpwv.fwpd, SET_VALUE=fw_types(lpseqpwv.exptable,*,lpseqpwv.tele)
      WIDGET_CONTROL, lpseqpwv.fwpd, SET_VALUE=0
      lpseqpwv.fw= 0 ; AEE - Nov 25, 02 - added so that fw from other cameras are not used as previous
                     ; fw for this camera.
      ;** set the pw number for the given telescope
      WIDGET_CONTROL, lpseqpwv.numpd, SET_VALUE=lpseqpwv.num_types ; AEE 12/19/03

      IF (lpseqpwv.tele EQ 0) THEN $
        WIDGET_CONTROL, lpseqpwv.fps_pd, SENSITIVE=1 $
      ELSE $
        WIDGET_CONTROL, lpseqpwv.fps_pd, SENSITIVE=0
      ; AEE 12/19/03:
      maxnum = 4

      lpseqpwv.num =  maxnum ; AEE Dec 06, 02
      WIDGET_CONTROL, lpseqpwv.numpd, SET_VALUE=lpseqpwv.num-1 ; AEE Dec 06, 02

      ;** set the pw labels for the given telescope
      FOR i=0, 3 DO BEGIN   ; AEE 12/19/03 - max of 4 images per scip seq (see Dennis Doc) 
         WIDGET_CONTROL, lpseqpwv.pwpd(i), SET_VALUE=pw_types(lpseqpwv.exptable,*,lpseqpwv.tele)
         WIDGET_CONTROL, lpseqpwv.pwpd(i), SET_VALUE= i
         lpseqpwv.pw(i)= i ; AEE - Nov 25, 02 - added so that pw from other cameras are not used as previous
                           ; pw for this camera.
         IF (i LT lpseqpwv.num) THEN WIDGET_CONTROL, lpseqpwv.pwpd(i), SENSITIVE=1 $ ; AEE Dec 06, 02
         ELSE WIDGET_CONTROL, lpseqpwv.pwpd(i), SENSITIVE=0
      ENDFOR

     ENDIF
   END
 
   lpseqpwv.numpd : BEGIN

      ; Nov 25, 02 - AEE ; Added these 3 statements instead of above statement.
      IF (lpseqpwv.tele EQ 0) THEN maxnum =4 
      ;IF (lpseqpwv.tele EQ 1 OR lpseqpwv.tele EQ 2) THEN maxnum = 20
      IF (lpseqpwv.tele EQ 1 OR lpseqpwv.tele EQ 2) THEN maxnum = 24

      ;IF selected polar is blank, ignore it and use the previously selected/displayed polar, instead:
      IF (event.index GE maxnum) THEN $
        ;WIDGET_CONTROL, lpseqpwv.numpd, SET_VALUE=lpseqpwv.num $
        WIDGET_CONTROL, lpseqpwv.numpd, SET_VALUE=lpseqpwv.num $ ; AEE Dec 06, 02
      ELSE                                                     $
        lpseqpwv.num = event.index + 1
      
      FOR i=0, 3 DO BEGIN ; AEE 12/19/03 - max of 4 images per scip seq (see Dennis Doc) 
         WIDGET_CONTROL, lpseqpwv.pwpd(i), SET_VALUE=pw_types(lpseqpwv.exptable,*,lpseqpwv.tele)
         WIDGET_CONTROL, lpseqpwv.pwpd(i), SET_VALUE=lpseqpwv.pw(i)
         IF (i LT lpseqpwv.num) THEN WIDGET_CONTROL, lpseqpwv.pwpd(i), SENSITIVE=1 $ ; AEE Dec 06, 02
         ELSE WIDGET_CONTROL, lpseqpwv.pwpd(i), SENSITIVE=0
      ENDFOR
   END

   lpseqpwv.img_cad: BEGIN
      WIDGET_CONTROL, lpseqpwv.img_cad, GET_VALUE= cadence
      lpseqpwv.cadence= FIX(cadence(0))
   END

   lpseqpwv.iptable_pd : BEGIN
      lpseqpwv.iptable = event.index
   END

   lpseqpwv.exptable_pd : BEGIN  ; AEE 1/13/04
      lpseqpwv.exptable = event.index
   END

   lpseqpwv.camtable_pd : BEGIN ; AEE 1/13/04
      lpseqpwv.camtable = event.index
   END

   lpseqpwv.fps_pd : BEGIN ; AEE 1/13/04
      lpseqpwv.fps = event.index
   END

   lpseqpwv.fwpd : BEGIN
      ;IF selected filter is blank, ignore it and use the previously selected/displayed filter, instead:
      IF (STRTRIM(fw_types(lpseqpwv.exptable,event.index,lpseqpwv.tele),2) EQ '') THEN $  
        WIDGET_CONTROL, lpseqpwv.fwpd, SET_VALUE=lpseqpwv.fw          $
      ELSE                                                          $ 
        lpseqpwv.fw = event.index
   END

   ELSE : BEGIN		;** must be changing one of the PW positions
      i = WHERE(lpseqpwv.pwpd EQ event.id)
      IF (STRTRIM(pw_types(lpseqpwv.exptable,event.index,lpseqpwv.tele),2) EQ '') THEN $
        WIDGET_CONTROL, lpseqpwv.pwpd(i(0)), SET_VALUE=lpseqpwv.pw(i(0))   $
      ELSE                                                           $
        lpseqpwv.pw(i(0)) = event.index
   END

ENDCASE

ccdsum= (ccd(lpseqpwv.tele,lpseqpwv.camtable).xsum > 1) * (ccd(lpseqpwv.tele,lpseqpwv.camtable).ysum > 1)
gmode= 'High'
IF (ccd(lpseqpwv.tele,lpseqpwv.camtable).gmode EQ 1) THEN BEGIN
  gmode='Low'
  ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
ENDIF

ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

tsum= 'ExpTimes for '+STRTRIM(ccd(lpseqpwv.tele,lpseqpwv.camtable).xsum > 1,2)+'x'+ $
      STRTRIM(ccd(lpseqpwv.tele,lpseqpwv.camtable).ysum > 1,2)+' CCD summing & gain='+gmode

msec=1000.0 ; don't need to show real scip exptime (units of 1.024).
WIDGET_CONTROL, lpseqpwv.sumid, SET_VALUE= tsum
FOR i= 0, 3 do BEGIN
 WIDGET_CONTROL, lpseqpwv.exid(i), $
  SET_VALUE= 'Img'+STRTRIM(i+1,2)+': '+STRING(ex(lpseqpwv.tele,lpseqpwv.exptable,lpseqpwv.fw,lpseqpwv.pw(i))/msec/ccdsum,'(F6.2)')+$
             ' Sec'
 IF (i LT lpseqpwv.num) THEN $
   WIDGET_CONTROL, lpseqpwv.exid(i), SENSITIVE=1 $
 ELSE $
   WIDGET_CONTROL, lpseqpwv.exid(i), SENSITIVE=0 
ENDFOR

END

;________________________________________________________________________________________________________
;

FUNCTION LP_SCIP_SEQ, caller

COMMON LP_SEQ_PW_FW_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE

    IF XRegistered("LP_SCIP_SEQ") THEN RETURN,1   ; AEE - Mar 19, 03
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 5 
    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpseqpwv))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       tele = 0
       exptable = 0 ; AEE 1/13/04
       camtable = 0 ; AEE 1/13/04
       iptable = 0
       ;fps = 0 ; off
       fps = 1 ; on
       fw = 0
       ;pw = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19] ; AEE Nov 25, 02 - first 4 are vaild for EUVI
       pw = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23] ; AEE 07/09/04
       num = 3 ; Default is EUVI with 4 sectors (0 to 3)
       cadence= 0  ; default = 0 = take images as close together as possible.
    ENDIF ELSE BEGIN
       tele = lpseqpwv.tele
       exptable = lpseqpwv.exptable
       camtable = lpseqpwv.camtable
       iptable = lpseqpwv.iptable
       fps = lpseqpwv.fps
       fw = lpseqpwv.fw
       num = lpseqpwv.num - 1 ; AEE - Dec 06, 02
       pw = lpseqpwv.pw
       cadence= lpseqpwv.cadence  ; cadence (for between images in the sequence).
    ENDELSE

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/COLUMN, TITLE='LP SCIP_SEQ', /FRAME, $ ; AEE 5/29/03
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW)

      col = WIDGET_BASE(row0, /COLUMN, /FRAME)

        telepd =  CW_BSELECTOR2(col, tele_types(0:2), SET_VALUE=tele) ; AEE - Dec 05, 02 - removed HI 1&2
        ;iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=iptable)
        ;iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable

        IF (!version.release LT '5.6') THEN BEGIN
          iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE

        exptable_pd = CW_BSELECTOR2(col, "Exposure "+table_types, SET_VALUE=exptable)
        ;camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=camtable) ; 6/4/04
        camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=camtable)
        fwpd = CW_BSELECTOR2(col, fw_types(exptable,*,tele), SET_VALUE=fw)

        ; AEE 12/19/03 - Max of 4 images per scip seq even for COR1 and COR2 (see Dennis's Doc):

        num_types = [ ['1 Image  Per Sequence', $
                       '2 Images Per Sequence', $
                       '3 Images Per Sequence', $
                       '4 Images Per Sequence']]

        numpd = CW_BSELECTOR2(col, num_types, SET_VALUE=num) ; AEE - 12/19/03

        row = WIDGET_BASE(col, /ROW)
        tmp= WIDGET_LABEL(row,VALUE= 'EUVI FPS') ; AEE 1/13/04
        fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

        row = WIDGET_BASE(col, /ROW)
        tmp = WIDGET_LABEL(row,VALUE='')

        col = WIDGET_BASE(col, /ROW, /FRAME)
        row = WIDGET_BASE(col, /COL)
        lab = WIDGET_LABEL(row,VALUE='Image Cadence (Sec)')
        lab = WIDGET_LABEL(row,VALUE='0 = default - Take images')
        lab = WIDGET_LABEL(row,VALUE='as close together as possible')
        img_cad= WIDGET_TEXT(row, XSIZE=15, /EDITABLE, VALUE= STRTRIM(cadence,2))



       col = WIDGET_BASE(row0, /COLUMN, /FRAME)

        pwpd = LONARR(4) ; AEE - 12/19/03 - commented out above and set max. images to 4 (See Dennis doc): 
        FOR scipseq= 0, 3 DO pwpd(scipseq) = CW_BSELECTOR2(col, pw_types(exptable,*,tele), SET_VALUE=pw(scipseq))

        font= '-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
        ccdsum= (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
        gmode= 'High '
        IF (ccd(tele,camtable).gmode EQ 1) THEN BEGIN
          gmode='Low'
          ccdsum= ccdsum/4.0 ; for lowgain amplify exptime by 4.
        ENDIF

        ccdsum= 1.0 ;Ignore ccd_sum and low gain exptime correction (FSW does not do it).

        tsum= 'ExpTimes for '+STRTRIM(ccd(tele,camtable).xsum > 1,2)+'x'+ $
              STRTRIM(ccd(tele,camtable).ysum > 1,2)+' CCD summing & gain='+gmode

        msec=1000.0 ; don't need to show real scip exptime (units of 1.024).
        sumid= WIDGET_LABEL(col, FONT= font, VALUE= tsum)
        exid = LONARR(4)
        FOR scipseq= 0, 3 DO exid(scipseq)= WIDGET_LABEL(col, FONT= font, $
          VALUE= 'Img'+STRTRIM(scipseq+1,2)+': '+STRING(ex(tele,exptable,fw,pw(scipseq))/msec/ccdsum,'(F6.2)')  + ' Sec')

     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
    FOR i=0, 3 DO BEGIN ; AEE - 12/19/03 - max of 4 images per scip seq (see Dennis Doc)
       IF (i LE num) THEN BEGIN
         WIDGET_CONTROL, pwpd(i), SENSITIVE=1
         WIDGET_CONTROL, exid(i), SENSITIVE=1
       ENDIF ELSE BEGIN 
         WIDGET_CONTROL, pwpd(i), SENSITIVE=0
         WIDGET_CONTROL, exid(i), SENSITIVE=0
       ENDELSE
    ENDFOR

    lpseqpwv = CREATE_STRUCT('base', base,			$
                             'telepd', telepd,			$
                             'exptable_pd', exptable_pd,        $ ; AEE 1/13/04
                             'camtable_pd', camtable_pd,        $ ; AEE 1/13/04
                             'iptable_pd', iptable_pd,		$
                             'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             'pwpd', pwpd,			$
                             'pw', pw,				$
                             'fwpd', fwpd,			$
                             'fw', fw,				$
                             'fps', fps,                        $ ; AEE 1/13/04
                             ;'num', num,			$
                             'num', num+1,			$  ; AEE - 01/15/03
                             'numpd', numpd,			$
                             'img_cad', img_cad,                $
                             'cadence', cadence,                $
                             'num_types', num_types,		$
                             'tele', tele,			$
                             'exptable', exptable,              $ ; AEE 1/13/04
                             'camtable', camtable,              $ ; AEE 1/13/04
                             'sumid', sumid,                    $
                             'exid', exid,                      $
                             'iptable', iptable )

    IF (tele NE 0) THEN WIDGET_CONTROL, lpseqpwv.fps_pd, SENSITIVE=0
    XMANAGER, "LP_SCIP_SEQ", base  ; AEE - Mar 19, 03


    RETURN, 1

END

;________________________________________________________________________________________________________
;
PRO LP_CONT_IMAGE_EVENT, event

COMMON OS_INIT_SHARE
COMMON LP_CONT_IMAGE_SHARE, lpcont
COMMON OS_SHARE

CASE (event.id) OF

   lpcont.dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpcont.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END

   lpcont.schedule : BEGIN
      WIDGET_CONTROL,osv.info_text,SET_VALUE='Continues Image Exposure'
      IF (lpcont.tele NE 0) THEN lpcont.fps= 0 ; FPS is only valid for EUVI
      OS_CALL_SCHEDULE
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, lpcont.base
      WIDGET_CONTROL, lprow, SENSITIVE=1
      RETURN
   END
 
   lpcont.telepd : BEGIN
     IF (lpcont.tele NE event.index) THEN BEGIN
      lpcont.tele = event.index
      WIDGET_CONTROL, lpcont.fwpd, SET_VALUE=fw_types(lpcont.exptable,*,lpcont.tele)
      WIDGET_CONTROL, lpcont.fwpd, SET_VALUE=0
      lpcont.fw= 0 ; AEE - Nov 19, 02 - added so that fw from other cameras are not used as previous
                    ; fw for this camera.
      WIDGET_CONTROL, lpcont.pwpd, SET_VALUE=pw_types(lpcont.exptable,*,lpcont.tele)
      WIDGET_CONTROL, lpcont.pwpd, SET_VALUE=0
      lpcont.pw= 0 ; AEE - Nov 19, 02 - added so that pw from other cameras are not used as previous
                    ; pw for this camera.
      IF (lpcont.tele EQ 0) THEN $
        WIDGET_CONTROL, lpcont.fps_pd, SENSITIVE=1 $
      ELSE $
        WIDGET_CONTROL, lpcont.fps_pd, SENSITIVE=0
     ENDIF
   END
 
   lpcont.iptable_pd : BEGIN
      lpcont.iptable = event.index
   END

   lpcont.camtable_pd : BEGIN ; AEE 1/13/04
      lpcont.camtable = event.index
   END

   lpcont.fps_pd : BEGIN ; AEE 1/13/04
      lpcont.fps = event.index
   END

   lpcont.fwpd : BEGIN
      ;IF selected filter is blank, ignore it and use the previously selected/displayed filter, instead:
      IF (STRTRIM(fw_types(lpcont.exptable,event.index,lpcont.tele),2) EQ '') THEN $  
        WIDGET_CONTROL, lpcont.fwpd, SET_VALUE=lpcont.fw          $
      ELSE                                                          $ 
        lpcont.fw = event.index
   END

   lpcont.pwpd : BEGIN
      IF (STRTRIM(pw_types(lpcont.exptable,event.index,lpcont.tele),2) EQ '') THEN $ 
        WIDGET_CONTROL, lpcont.pwpd, SET_VALUE=lpcont.pw          $
      ELSE                                                          $
        lpcont.pw = event.index
   END

   ELSE : BEGIN
   END


ENDCASE

END

;________________________________________________________________________________________________________
;

FUNCTION LP_CONT_IMAGE, caller

COMMON LP_CONT_IMAGE_SHARE
COMMON OS_INIT_SHARE

    IF XRegistered("LP_CONT_IMAGE") THEN RETURN, 1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 4 ; AEE - May 19, 03

    WIDGET_CONTROL, lprow, SENSITIVE=0

    IF ((SIZE(lpcont))(1) EQ 0) THEN BEGIN          ;** not defined yet use default
       tele = 0
       camtable = 0
       iptable = 0
       ;fps = 0 ; off
       fps = 1 ; on
       fw = 0
       pw = 0
    ENDIF ELSE BEGIN
       tele = lpcont.tele
       camtable = lpcont.camtable
       iptable = lpcont.iptable
       fps = lpcont.fps
       fw = lpcont.fw
       pw = lpcont.pw
    ENDELSE

    exptable= 0 ; use only 1st exptable for now (also for CAL LED).

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/COLUMN, TITLE='LP CONT_IMAGE', /FRAME, $
             GROUP_LEADER=caller.id)
      schedule = WIDGET_BUTTON(base, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base, /ROW, /FRAME)

      col = WIDGET_BASE(row0, /COLUMN)
        telepd =  CW_BSELECTOR2(col, tele_types, SET_VALUE=tele)
        ;iptable_pd = CW_BSELECTOR2(col, proc_tab_types, SET_VALUE=iptable)
        ;iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
        ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
        ; SET_DROPLIST_SELECT instead:
        ;WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable

        IF (!version.release LT '5.6') THEN BEGIN
          iptable_pd = WIDGET_DROPLIST(col, VALUE=proc_tab_types)
          WIDGET_CONTROL, iptable_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN
          comb1= WIDGET_BASE(col, /COLUMN)
          iptable_pd = WIDGET_COMBOBOX_SHELL(comb1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE

        ;camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types(0:4), SET_VALUE=camtable) ; AEE 6/4/04
        camtable_pd = CW_BSELECTOR2(col, "Camera "+table_types, SET_VALUE=camtable)
      row = WIDGET_BASE(col, /ROW)
        tmp= WIDGET_LABEL(row,VALUE= 'EUVI FPS') ; AEE 1/13/04
        fps_pd  = CW_BSELECTOR2(row, ['Off','On'], SET_VALUE=fps) ; AEE 1/13/04

      col = WIDGET_BASE(row0, /COLUMN)
        fwpd = CW_BSELECTOR2(col, fw_types(exptable,*,tele), SET_VALUE=fw)
        pwpd = CW_BSELECTOR2(col, pw_types(exptable,*,tele), SET_VALUE=pw)

     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    lpcont = CREATE_STRUCT( 'base', base,			$
                             'telepd', telepd,			$
                             'camtable_pd', camtable_pd,	$ ; AEE 1/13/04
                             'fps_pd', fps_pd,                  $ ; AEE 1/13/04
                             'iptable_pd', iptable_pd,		$
                             'dismiss', dismiss,		$
                             'schedule', schedule,		$
                             'fwpd', fwpd,			$
                             'pwpd', pwpd,			$
                             'fw', fw,				$
                             'pw', pw,				$
                             'tele', tele,			$
                             'camtable', camtable,		$ ; AEE 1/13/04
                             'fps', fps,                        $ ; AEE 1/13/04
                             'exptable', exptable,              $ ; AEE 3/29/04
                             'iptable', iptable )

    IF (tele NE 0) THEN WIDGET_CONTROL, lpcont.fps_pd, SENSITIVE=0
    XMANAGER, "LP_CONT_IMAGE", base

    RETURN, 1

END

;________________________________________________________________________________________________________
;
 
PRO LP_NULL_EVENT, event

COMMON LP_NULL_SHARE, base, dismiss
COMMON OS_INIT_SHARE

CASE (event.id) OF

   dismiss : BEGIN	;** exit program
      plan_lp = -2 ; AEE - 9/30/03 
      WIDGET_CONTROL, /DESTROY, base
      RETURN
   END

   ELSE : BEGIN
   END

ENDCASE

END

;________________________________________________________________________________________________________
;

FUNCTION LP_NULL, caller

COMMON LP_NULL_SHARE
COMMON OS_INIT_SHARE

    IF XRegistered("LP_NULL") THEN RETURN,1
    IF (plan_lp) THEN RETURN,1 ELSE plan_lp = 24

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    base = WIDGET_BASE(/COLUMN, TITLE='LP NULL', /FRAME, $
             GROUP_LEADER=caller.id)

     dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    XMANAGER, "LP_NULL", base

    RETURN, 1

END

;________________________________________________________________________________________________________
;

PRO OS_LP
END
