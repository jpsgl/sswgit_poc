;+
;$Id: os_init.pro,v 1.17 2011/06/15 22:47:44 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_INIT
;               
; Purpose     : Initialize vars used by the SECCHI Observation Definition tools.
;               
; Explanation : This tool initializes the variables defined in the Commons below.
;               
; Use         : OS_INIT
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written By  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/04/04 - Corrected fw and pw width and sizes. 
;              Ed Esfandiari 07/09/04 - Use 24 polar positions for COR1 & COR2 instead of 20.
;              Ed Esfandiari 07/14/04 - added cl_time and ro_time to CCD setup structure. 
;              Ed Esfandiari 07/21/04 - use tables pointed by an env-variable and fsw table names.
;              Ed Esfandiari 07/14/04 - changed IP table from 20 to 40.
;              Ed Esfandiari 09/16/04 - Corrected order of EUVI filters.
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 12/15/04 - corrected HI-summing code.
;              Ed Esfandiari 03/10/05 - added datatype check for plan_lp. 
;              Ed Esfandiari 03/11/05 - changed IP tables from 40 to 80.
;              Ed Esfandiari 04/18/05 - changed # of IP tables from 80 to 100.
;              Ed Esfandiari 05/23/05 - Added ccd depenedent orientaions for masks.
;              Ed Esfandiari 09/11/05 - Added new IP steps 114 and 115 to figure out 1st
;                                       table number of HI summing (114 and 115 can be used
;                                       in place of 34 and 36).
;              Ed Esfandiari 11/21/05 - For HI summed images, only keep and use steps of 3rd table.
;              Ed Esfandiari 03/31/06 - Changed order of euvi_filt and euvi_sector to match those in
;                                       latest exptables.
;              Ed Esfandiari 11/27/06 - Added code to switch to SC-B tables when B is selected. 
;                                       Note: using different SC-A and SC-B table values for same 
;                                       image (same OS_NUM), may result in images that are different
;                                       but are assumed to be the same (not ideal if synched?). It
;                                       is best that tables on both spacecrafts are the same.
;                                        
;              Ed Esfandiari 11/29/06 - Switched EUVI filter wheel names around and changed 175 sector
;                                       to 171.
;              Ed Esfandiari 02/21/07 - Added code to handle HI summing HI/LO SSR1 images.
;              Ed Esfandiari 08/14/09  Changed SPAWN Id to Id to avoid cvs cobfusion.
;              Ed Esfandiari 08/14/09  Changed H-compress range from 5-17 to 5-14.
;              Ed Esfandiari 08/18/09  Use only 1st Id of spawn commands (some header have multiple Id).
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15. Treat HISAM as no-compression.
;
; $Log: os_init.pro,v $
; Revision 1.17  2011/06/15 22:47:44  nathan
; use getenv instead of $TABLES
;
; Revision 1.16  2009/09/11 20:28:14  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.10  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.9  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.8  2005/04/27 20:38:38  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.7  2005/03/10 16:46:59  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.6  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.4  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.3  2004/07/12 13:15:14  esfand
; now using 24 COR1 and COR2 polar positions
;
; Revision 1.2  2004/07/12 13:09:07  esfand
; now using 24 COR1 and COR2 polar positions
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-            

;__________________________________________________________________________________________________________
;

PRO OS_INIT

COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
   ip_arr, plan_lp, lprow, cor1pw, cor2pw
COMMON SCHED_SHARE, schedv
COMMON TABLES_IN_USE, tables_used, tbAout,tbBout

tbldir=getenv('TABLES')

  SPAWN,"grep -h 'Id:' "+tbldir+"/imagetbl.img",sca_tables
  sca_tables= sca_tables(0)
  scb_tables= sca_tables

  SPAWN,"grep -h 'Id:' "+tbldir+"/expostba.img",tbl
  sca_tables= [sca_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/occulta.img",tbl
  sca_tables= [sca_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/rotbtb1a.img",tbl
  sca_tables= [sca_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/setuptba.img",tbl
  sca_tables= [sca_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/wavetb1a.img",tbl
  sca_tables= [sca_tables,tbl(0)]

  SPAWN,"grep -h 'Id:' "+tbldir+"/expostbb.img",tbl
  scb_tables= [scb_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/occultb.img",tbl
  scb_tables= [scb_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/rotbtb1b.img",tbl
  scb_tables= [scb_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/setuptbb.img",tbl
  scb_tables= [scb_tables,tbl(0)]
  SPAWN,"grep -h 'Id:' "+tbldir+"/wavetb1b.img",tbl
  scb_tables= [scb_tables,tbl(0)]

    IF(DATATYPE(schedv) EQ 'UND') THEN BEGIN
      sc= 'A' ; for AB
    ENDIF ELSE BEGIN
      IF (schedv.sc EQ 0) THEN sc= 'A' ; for AB
      IF (schedv.sc EQ 1) THEN sc= 'A'
      IF (schedv.sc EQ 2) THEN sc= 'B'
    ENDELSE
    PRINT,''
    PRINT,'Preparing to use Tables for SC-'+sc+':'
    PRINT,'================================='
    PRINT,''

  ; get tables_used info and keep around (also done in os_tables.pro):
  ;SPAWN,"grep -h 'Id:' "+tbldir+"/imagetbl.img",tables_used
  CASE (sc) OF
    'A': BEGIN
      ;SPAWN,"grep -h 'Id:' "+tbldir+"/*a.img",sca_tables
      ;tables_used= [tables_used,sca_tables]
      tables_used= sca_tables
    END
    'B': BEGIN
      ;SPAWN,"grep -h 'Id:' "+tbldir+"/*b.img",scb_tables
      ;tables_used= [tables_used,scb_tables]
      tables_used= scb_tables
    END
    ELSE : BEGIN
    END
  ENDCASE

  tbAout= ';'+strmid(sca_tables,2,100) ; remove first $ (second char) from each string.
  tbBout= ';'+strmid(scb_tables,2,100) ; remove first $ (second char) from each string.

  FOR i=0, N_ELEMENTS(tables_used)-1 DO BEGIN
    toks= STR_SEP(tables_used(i),' ')
    toks= (toks(WHERE(toks NE ' ')))(1:2) ; Keep tablename_version
    tables_used(i)= toks(0)+toks(1)
  ENDFOR
  ; so now:
  ; tables_used(0)= IP table
  ; tables_used(1)= expcal table
  ; tables_used(2)= occult table
  ; tables_used(3)= rotbtb1 table
  ; tables_used(4)= setuptb table
  ; tables_used(5)= wavetb1 table


    ;** camera table contents
    ;IF ((SIZE(ccd))(1) EQ 0) THEN BEGIN
       ccdvars = {ccdvars, pform:1, camera:0, clr_id:0, rdout_id:0, x1:0, y1:0, x2:0, y2:0, xsum:0, $
                  ysum:0, offset:0, gain:0, gmode:0, cl_time:0.0, ro_time:0.0} 

       ; note cl_time and ro_time are not in camera setup table. Must get them from readout/clear table.

       ;ccd = REPLICATE(ccdvars,5,5)    	;** 5 cameras; 5 tables  ; AEE Nov 25 02
       ccd = REPLICATE(ccdvars,5,7)    	;** 5 cameras; 7 tables  ; AEE Nov 25 02

       ; AEE 3/30/04:

       ;READ_CAMERA_TABLES,GETENV('PT')+'/TABLES/'+'camera_tables.dat', ccd
       ;READ_CAMERA_TABLES,GETENV_SLASH('TABLES')+'setuptba.img', ccd
       IF (DATATYPE(sc) EQ 'UND') THEN BEGIN
         setup_tbl= GETENV_SLASH('TABLES')+'setuptba.img'
       ENDIF ELSE BEGIN
         IF (sc EQ 'A') THEN $
           setup_tbl= GETENV_SLASH('TABLES')+'setuptba.img' $
         ELSE $
           setup_tbl= GETENV_SLASH('TABLES')+'setuptbb.img'
       ENDELSE
       PRINT,'Reading '+setup_tbl
       READ_CAMERA_TABLES,setup_tbl, ccd

    ;ENDIF

    ;** image processing table contents
    ;num_ip_tables= 40

    ; Note: when anything in os_instance is changed, like num_ip_tables, must do:
    ;   1. make changes in os_init and run it by itself with an stop at the end 
    ;      from a fresh idl session.
    ;   2. set defined_os_arr= os_instance
    ;   3. rename ../IN/OTHER/os_arr.sav to something else in case it is needed later.
    ;   4. save,filename= '../IN/OTHER/os_arr.sav',defined_os_arr
    ;   5. remove stop from os_init and start the schedule.
    ;   6. add observations and do a store current schedule to extend the os_arr.sav
    ;      with other data as well.

    num_ip_tables= 100 

    IF ((SIZE(ip))(1) EQ 0) THEN BEGIN
       steps = INTARR(26)-1  ;** 26 steps per table
       ipvars = {ipvars, steps:steps, mask:0, comp:0}
       ;ip = REPLICATE(ipvars,20)    	;** 20 IP tables
       ip = REPLICATE(ipvars,num_ip_tables)
       IF (DATATYPE(ip_arr) EQ 'UND') THEN ip_arr = READ_IP_DAT()

       ; AEE 4/2/04:

       ;READ_IP_TABLES,GETENV('PT')+'/TABLES/'+'ip_tables.dat', ip
       PRINT,'Reading '+GETENV_SLASH('TABLES')+'imagetbl.img'
       READ_IP_TABLES,GETENV_SLASH('TABLES')+'imagetbl.img', ip

       ipdef= ip  ; keep a default copy with orignal data as was read in.
       ipd= ip    ; keep a default copy with HI summed-Seq tables & descriptions changed 
                  ; for display purposes. 

       ; AEE - 4/2/04 - Note to self:
       ; Note: since the 20 IP tables can be used by any of the 5 instruments, 
       ; I may want to check the steps in each table, at this point, and if any 
       ; are telescope specific, don't let that table be used by other telescopes.

       ; AEE 5/6/04 - HI1 and HI2 Summing sequences use 3 tables each. The reason for
       ;              3 tables is that it is how FSW handles the summing and because
       ;              the first image maybe processed differently than the rest of
       ;              summed images. For instance, exp.time adjustment step may only be
       ;              applied to first image but not to the rest (to save time) as
       ;              long as that step does not effect the CCD readout time and size of
       ;              the image. The first 2 steps of table 1 names the 2nd and 3rd tables
       ;              that also has to be used.
       ;
       ;              In PT, though, to avoid making many changes in different routines,
       ;              I check to find out the first table for HI1 and/or HI2 summing (if
       ;              used) and then add the steps from the following two tables to it
       ;              just as if all steps were already in 1 table:
       ;
       ;              Note: the 2nd and 3rd table of HI1 and HI2 summimg SHOULD not be used
       ;              for taking any kind of images by themselves (as if they don't even exist).

       
       ; AEE 5/21/04:
       ; Pick up first table of the 3 used for HI1 or HI2 Summed-Sequences (it should 
       ; contain Clear SB, Add to HI SB, and Header-Only functions). It's first and 
       ; second elements specifies 2nd and 3rd IP tables to be used: 
       ; (note: use ipd (ip default) to display the Image Processing Tables.)
    
       new_ip_arr_elem= ip_arr(0)
 
       FOR i=0, N_ELEMENTS(ip)-1 DO BEGIN

         hi1_ind= WHERE(ip(i).steps(2:*) EQ 33 OR ip(i).steps(2:*) EQ 34 OR ip(i).steps(2:*) EQ 5 OR ip(i).steps(2:*) EQ 114 , hi1cnt)
         hi2_ind= WHERE(ip(i).steps(2:*) EQ 35 OR ip(i).steps(2:*) EQ 36 OR ip(i).steps(2:*) EQ 5 OR ip(i).steps(2:*) EQ 115, hi2cnt)

         IF (hi1cnt EQ 3 OR hi2cnt EQ 3) THEN BEGIN

           ; Now that 1st table for a HI1 or HI2 summed-sequence is located, pickup 2nd and 3rd
           ; table numbers and add their steps to the 1st table. Also, remove duplicate steps,
           ; and header-only step: 

           tab1_steps= ip(i).steps(WHERE(ip(i).steps NE -1))
           tab1_steps= tab1_steps(2:*)  ; remove first 2 entries (table2 and table3 numbers).
           tab2= ip(i).steps(0)
           tab2_steps= ip(tab2).steps(WHERE(ip(tab2).steps NE -1))
           tab3= ip(i).steps(1)
           tab3_steps= ip(tab3).steps(WHERE(ip(tab3).steps NE -1))
           combined_steps= [tab1_steps,tab2_steps,tab3_steps]
           combined_steps= combined_steps(UNIQ_NOSORT(combined_steps)) ; remove duplicate steps - keep order
           combined_steps= combined_steps(WHERE(combined_steps NE 5))  ; remove Header-Only steps (so we have
;help,combined_steps
;stop
           ; Now overwrite the steps in 1st table with combined steps from all 3 tables:

           ip(i).steps= combined_steps
           ip(i).steps(N_ELEMENTS(combined_steps):*)= -1

           ; No - just keep 3rd table (for last image that is downlinked), Ignore 1st and 2nd tables:
           ;ip(i).steps= ip(tab3).steps 

           ; IF HI/LO is used for SSR1 summed images, remove duplicate SSR1 and Comp. (usually Rice) combinations:
           ;t3uniq= tab3_steps(UNIQ_NOSORT(tab3_steps))
           ;ip(i).steps(N_ELEMENTS(t3uniq):*)= -1
           ;ip(i).steps= t3uniq

           ; If Rice, then keep the 2nd one (so there are no steps without dest-comp around them:
           rice= WHERE(tab3_steps EQ 7, rice_cnt)
           ;IF (rice_cnt GT 0) THEN tab3_steps= [tab3_steps(0:rice(0)-1),tab3_steps(rice(0)+1:*)]
           IF (rice_cnt GT 1) THEN tab3_steps= [tab3_steps(0:rice(0)-1),tab3_steps(rice(0)+1:*)]
           t3uniq= tab3_steps(UNIQ_NOSORT(tab3_steps))

           ; If HI summed images goes to a second destination (like SW), add a summing step to it so that
           ; it also uses implicit summing:

           ;comp_ind = WHERE(t3uniq GE 5 AND t3uniq LE 17 OR t3uniq GE 90 AND t3uniq LE 101 OR $ 
           ;comp_ind = WHERE(t3uniq GE 5 AND t3uniq LE 14 OR t3uniq GE 90 AND t3uniq LE 101 OR $ 
           comp_ind = WHERE(t3uniq GE 5 AND t3uniq LE 15 OR t3uniq GE 90 AND t3uniq LE 101 OR $ 
                     t3uniq EQ 44, comp_cnt)
           IF (comp_cnt GT 1) THEN BEGIN
             usteps= t3uniq(0:comp_ind(0))
             ind = WHERE(usteps EQ 34 OR usteps EQ 36 OR usteps EQ 37 OR steps EQ 38 OR $
                     usteps EQ 114 OR usteps EQ 115, hscnt)
             t3uniq= [t3uniq(0:N_ELEMENTS(t3uniq)-2),usteps(ind(0)),t3uniq(N_ELEMENTS(t3uniq)-1)]
           ENDIF

           ip(i).steps(N_ELEMENTS(t3uniq):*)= -1
           ip(i).steps= t3uniq
;help,i,ip(i).steps
;print,ip(i).steps
;stop

           ; Also, use ipd for displaying contents of each IP table. But, for HI summed-images,
           ; the first 2 steps of 1st table represent table numbers not the steps numbers. For
           ; these, extend the description and change ipd steps to display "Use Table # ...." instead
           ; of two IP-functions that correspond to the step numbers:

           new_ip_arr_elem.ip_description= 'Use IP Table '+STRTRIM(ipd(i).steps(0),2) + $
                                           ' (for middle summed images)' 
           new_ip_arr_elem.ip_num= N_ELEMENTS(ip_arr)
           ipd(i).steps(0)= N_ELEMENTS(ip_arr)
           ip_arr= [ip_arr,new_ip_arr_elem]
           new_ip_arr_elem.ip_description= 'Use IP Table '+STRTRIM(ipd(i).steps(1),2) + $
                                           ' (for last summed image)'
           new_ip_arr_elem.ip_num= N_ELEMENTS(ip_arr)
           ipd(i).steps(1)= N_ELEMENTS(ip_arr)
           ip_arr= [ip_arr,new_ip_arr_elem]

         ENDIF 

       ENDFOR

       ; Note: the ip.mask and ip.comp don't seem to be used anywhere - just ip.steps are used.

    ENDIF

    ;** exposure durations default table contents (values are for no summing)

    ;READ_EXP_TABLES,GETENV('PT')+'/TABLES/'+'exp_tables.dat',expt,fw_pos, pw_pos
    ;READ_EXP_TABLES,GETENV_SLASH('TABLES')+'expostba.img',expt,fw_pos, pw_pos
    IF (DATATYPE(sc) EQ 'UND') THEN BEGIN
      expos_tbl= GETENV_SLASH('TABLES')+'expostba.img'
    ENDIF ELSE BEGIN
      IF (sc EQ 'A') THEN $
        expos_tbl= GETENV_SLASH('TABLES')+'expostba.img' $
      ELSE $
        expos_tbl= GETENV_SLASH('TABLES')+'expostbb.img'
    ENDELSE
    PRINT,'Reading '+expos_tbl
    READ_EXP_TABLES,expos_tbl,expt,fw_pos, pw_pos

    ;** exposure durations table contents
    ;IF ((SIZE(ex))(1) EQ 0) THEN BEGIN
       ex= expt                                 ;** 5 cameras; 7 tables; 4 fw; 24 pw
       exd= expt
    ;ENDIF

    ;** define mask tables
    ;IF ((SIZE(occ_blocks))(1) EQ 0) THEN BEGIN
       ; AEE 7/25/04:
       ;READ_MASK_TABLES,GETENV('PT')+'/TABLES/mask_tables.dat',roi_blocks,occ_blocks

       ; Note: sc is always undefined at this point (default schedule is for 'AB') so
       ;       occulta.img is always read in. Stop and ask operator for input?

       IF (DATATYPE(sc) EQ 'UND') THEN BEGIN 
         READ_MASK_TABLES,GETENV_SLASH('TABLES')+'occulta.img',roi_blocks,occ_blocks,'A' 
         PRINT,'Reading '+GETENV_SLASH('TABLES')+'occulta.img'
       ENDIF ELSE BEGIN 
         IF (sc EQ 'A') THEN BEGIN
           READ_MASK_TABLES,GETENV_SLASH('TABLES')+'occulta.img',roi_blocks,occ_blocks,'A' 
           PRINT,'Reading '+GETENV_SLASH('TABLES')+'occulta.img'
         ENDIF ELSE BEGIN 
           READ_MASK_TABLES,GETENV_SLASH('TABLES')+'occultb.img',roi_blocks,occ_blocks,'B'
           PRINT,'Reading '+GETENV_SLASH('TABLES')+'occultb.img'
         ENDELSE
       ENDELSE
    ;ENDIF

COMMON LOCATION_SHARE, x, y, xx, yy, last_base

    ;** initialize location for first pop up window
    last_base = -1
    x = 500
    y = 0
    DEVICE, GET_SCREEN_SIZE=ss
    xx = ss(0)
    yy = ss(1)


   ; Note: Order of op_types should match those of op_pro in define_os.pro:

   filename= GETENV('PT')+'/IN/OTHER/'+'op_types.sav'
   RESTORE,FILENAME= filename ; => op_types, above


    IF (DATATYPE(plan_lp) EQ 'UND') THEN BEGIN 
      plan_lp = -2 ; AEE 9/30/03 - used plan_lp=0 for Sky Double Images. (-2 is the first negative
                 ;               false value before zero.
    ENDIF

    tele_types = [ "Telescope: EUVI      ", $
                   "Telescope: COR1      ", $
                   "Telescope: COR2      ", $
                   "Telescope: HI1      ", $
                   "Telescope: HI2     "]

    table_types =[ "Table 1", $
                   "Table 2", $
                   "Table 3", $
                   "Table 4", $
                   "Table 5", $
                   "Table 6", $
                   "Table 7"]

; AEE - Aug 30, 2002 Since c0fw (EUVI) has maximum number of filters (5 for now) must set rest of them
; also to the same size (5) but set the extra ones to blanks.

; AEE - Nov 25, 02 - c0fw (EUVI) has 4 filters not 5.

   sze= SIZE(fw_pos)
   fw_types = STRARR(sze(1),sze(2),5)  ; strarr(7,4,5) ; 7 tables, 4 filter(positins), 5 telescopes
   ; AEE 6/4/04 - follwing statement is  needed for non euvi telescopes (they don't have filters):
   fw_types = fw_types+'                 '
   ;euvi_filt= ['Clear','Al+1a','Al+1b','A1+2 ']
   ;euvi_filt= ['Al+2 (OPEN)','Clear (S1)','Al+1a (DBL)','Al+1b (S2)']
   euvi_filt= ['Open (Al+2)','S1 (Clear)','Dbl (Al+1a)','S2 (Al+1b)']
   FOR i= 0, sze(1)-1 DO fw_types(i,*,0)= euvi_filt

   sze= SIZE(pw_pos)
   pw_types = STRARR(sze(1),sze(2),5) ; strarr(7,24,5) ; 7 tables, 24 positions, 5 telescopes
   ; AEE 6/4/04 - follwing statement is needed for telescopes that don't have pw:
   pw_types = pw_types+'                     '
   pw_types(*,0:3,0)= STRING(pw_pos(*,0:3,0),'(i3)')
   pw_types(*,*,1:2)= STRING(pw_pos(*,*,1:2),'(i3)')
   ; Add euvi sectors:
   ;euvi_sectors= ['171','195','284','304']
   ;euvi_sectors= ['304','195','284','171']
   ;euvi_sectors= ['195A','284A','175A','304A']
   euvi_sectors= ['195A','284A','171A','304A']
   FOR i= 0, sze(1)-1 DO pw_types(i,0:3,0)= pw_types(i,0:3,0) + ': ' + euvi_sectors
   all_pos= STRING(FINDGEN(144)*2.5,'(f5.1)') ; AEE 3/26/04 - max of 144 pw names (2.5 degrees apart).
   ; Now add COR1 and COR2 angles for the selected postions:
   FOR j= 1,2 DO BEGIN
    FOR i= 0, sze(1)-1 DO pw_types(i,*,j)= pw_types(i,*,j) + ': ' + all_pos(pw_pos(i,*,j))
   ENDFOR 
   
    exit_types = ['Iteration   ', 	$
                  'End at Time ', 	$
                  'Duration    ']
    ;proc_tab_types = "Img Proc Table  " + STRMID(STRING(INDGEN(20)), 6, 2) + ' '
    proc_tab_types = "Img Proc Table  " + STRMID(STRING(INDGEN(num_ip_tables)), 6, 2) + ' '

COMMON OS_DEFINED, defined_os_arr, os_instance

  IF ((SIZE(os_instance))(1) EQ 0) THEN BEGIN

   RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ; => xyblks (34 blocks for a ccd size of 34*34=1156)
   os_instance = {os_instance, os_num:0L, lp:0, tele:0, exptable:0, camtable:0, iptable:0, fw:0, $ ; AEE 1/13/04
                  pw:INTARR(24),                $;** up to 24 pos for SEQ PW LP
                  lamp:0, num_images:0,        	$
                  fps: 0,                       $ ; AEE 1/13/04
                  pre_proc_time:0.0,            $ ; AEE 6/14/04
                  cadence:0.0,                  $ ;** AEE - 01/15/03 - for SEQ. default=0=take images in 
                                                  ;                    a SEQ as close together as possible.
                  sub:0, 		        $ ;** use subregions for line scan
                  start:0,			$;** start index for line scan
                  ;ccd:REPLICATE(ccdvars,5,5),  	$;** 5 cameras; 5 tables  ;AEE Nov 25, 02
                  ccd:REPLICATE(ccdvars,5,7),  	$;** 5 cameras; 7 tables  ;AEE Nov 25, 02
                  ;ip:REPLICATE(ipvars,20),   	$;** 20 IP tables
                  ip:REPLICATE(ipvars,num_ip_tables),   	$
                  ;ex:LONARR(5,7,4,24),          $;AEE 2/6/04 - changed to 7 tables. 
                  ex:FLTARR(5,7,4,24),          $ ; AEE 08/16/04 - changed to flt. 
                  occ_blocks:BYTARR(5,xyblks*xyblks),   $;** 5 cameras; 1156 blocks            ;AEE
                  roi_blocks:BYTARR(5,xyblks*xyblks)}   ;** 5 cameras; 1156 blocks            ;AEE
  ENDIF

END

;__________________________________________________________________________________________________________
;
