;+
;$Id: plot_ccd.pro,v 1.8 2011/01/21 18:48:17 mcnutt Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : PLOT_CCD
;               
; Purpose     : This routine defines the area of the CCD readout
;               for an image using a ccd setup table. 
;               
; Use         : PLOT_CCD, win, tele, x1, x2, y1, y2, /BLOCKS
;    
; Inputs      : win     Window number to plot the image layout on CCD. 
;               tele    Telescope number.
;               x1, x2  X-coordinates of the image layout on CCD.
;               y1, y2  Y-coordinates of the image layout on CCD.
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : blocks - if set, shade the masked blocks, if any. 
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 01/11/05 Corrected image coordinates in 272x272 ccd plot window.
;              Ed Esfandiari 05/25/05 Adjusted center of FOV (xc,yc) using ccd coordinates and
;                                     plot shaded area using idl coordinates. 
;              Ed Esfandiari 12/17/07 Corrected shading the mask blocks.
;              Ed Esfandiari 01/08/08 Added images in CCD area display for all telescopes.
;
; $Log: plot_ccd.pro,v $
; Revision 1.8  2011/01/21 18:48:17  mcnutt
; read in fits file from the secchi directory tree
;
; Revision 1.7  2009/09/11 20:28:17  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.3  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:06  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO PLOT_CCD, win, tele, x1, x2, y1, y2, BLOCKS=blocks
   WSET, win
   ERASE

   uscan= 0

   x1 = ((x1) / 8.0) ; devide by 8 to reduce 1-2176 to 1-272 (ccd draw window is 272x272).
   x2 = ((x2) / 8.0)
   y1 = ((y1) / 8.0)
   y2 = ((y2) / 8.0)

   RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ; => xyblks (34 blocks for a ccd size of 34*34=1156)
   IF (xyblks EQ 34) THEN BEGIN
     xs = 272
     ys = 272
     xc = 136
     yc = 136
   ENDIF

   ;** SHADE OUT ENTIRE CCD
   x = [0,1,1,0]
   y = [0,0,1,1]
   pat = BYTARR(2,2)
   pat(1,1) = 9
   POLYFILL, x, y, PATTERN=pat, /NORMAL

   ; adjust center of field of view circles using x1,y1,x2,y2 of ccd area used? - AEE 05/24/05
   xc= x1+(x2-x1)/2.0
   yc= ys-(y1+(y2-y1)/2.0)
   ;help,xc,yc

   IF tele EQ 0 THEN BEGIN ; EUVI
     img= SCCREADFITS(GETENV('secchi')+'/lz/L0/a/img/euvi/20071127/20071127_000600_n4euA.fts',h)
     ;mg= 2048x2048
     LOADCT, 0, /SILENT
     ;TVSCL,REBIN(img,256,256),8.5,30
     TVSCL,REBIN(img,256,256),8.5,16
     SETUP_PT_COLOR_TBL
   ENDIF 
   IF tele EQ 1 THEN BEGIN ; COR1 
     img= SCCREADFITS(GETENV('secchi')+'/lz/L0/a/seq/cor1/20071127/20071127_000500_s4c1A.fts',h)
     ; img= 1024x1024
     LOADCT, 0, /SILENT
     TVSCL,REBIN(img,256,256),8.5,16
     SETUP_PT_COLOR_TBL
   ENDIF
   IF tele EQ 2 THEN BEGIN ; COR2
     img= SCCREADFITS(GETENV('secchi')+'/lz/L0/a/img/cor2/20071127/20071127_000754_d4c2A.fts',h)
     ;mg= 2048x2048
     LOADCT, 0, /SILENT
     TVSCL,REBIN(img,256,256),8.5,16
     SETUP_PT_COLOR_TBL
   ENDIF
   IF tele EQ 3 THEN BEGIN ; HI-1
     img= SCCREADFITS(GETENV('secchi')+'/lz/L0/a/img/hi_1/20071127/20071127_000901_s4h1A.fts',h)
     ; img= 1024x1024
     LOADCT, 0, /SILENT
     TVSCL,REBIN(img,256,256),8.5,16
     SETUP_PT_COLOR_TBL
   ENDIF
   IF tele EQ 4 THEN BEGIN ; HI-2
     img= SCCREADFITS(GETENV('secchi')+'/lz/L0/a/img/hi_2/20071127/20071127_000921_s4h2A.fts',h)
     ; img= 1024x1024
     LOADCT, 0, /SILENT
     TVSCL,REBIN(img,256,256),8.5,16
     SETUP_PT_COLOR_TBL
   ENDIF

;   ;** UN-SHADE READ OUT COORDS
;   x = [x1,x2,x2,x1]
;   y = [y1,y1,y2,y2]
;   x = 272.0-x  ; change to IDL coordinate 0= lower left corner
;   y = 272.0-y  ; change to IDL coordinate 0= loweR left corner
;   POLYFILL, x, y, /DEVICE, COLOR=0

   ;** SHADE outside of READ OUT COORDS:
   x = [x1,x2,x2,x1]
   y = [y1,y1,y2,y2]
   x = 272.0-x  ; change to IDL coordinate 0= lower left corner
   y = 272.0-y  ; change to IDL coordinate 0= loweR left corner
   POLYFILL, [x(0:1),x(1),x(0)],[y(0:1),272,272],PATTERN=pat,/DEVICE
   POLYFILL, [x(2:3),x(3),x(2)],[y(2:3),0,0],PATTERN=pat,/DEVICE

   ;** SHADE OUT MASKED BLOCKS IF PASSED IN
   bs = 8	;** block size in pixels
   pat(1,1) = 4
   IF KEYWORD_SET(BLOCKS) THEN BEGIN
   IF (blocks(0) NE -1) THEN BEGIN
      FOR i=0, N_ELEMENTS(blocks)-1 DO BEGIN
         x1 = (blocks(i) - blocks(i)/xyblks*xyblks)*bs
         x1 = x1 + uscan
      ;x1 = 264 - x1
      IF tele LT 3 THEN x1 = 264 - x1
         y1 = blocks(i)/xyblks*bs
         x2 = x1+bs-1
         y2 = y1+bs-1

;         x1 = blocks(i)/xyblks*bs
;         x1 = x1 + uscan
;         x1 = 264 - x1
;         y1 = (blocks(i) - blocks(i)/xyblks*xyblks)*bs
;         y1= 264 - y1
;         x2 = x1+bs-1
;         y2 = y1+bs-1

         x = [x1,x2,x2,x1]
         y = [y1,y1,y2,y2]
         ;;POLYFILL, x, y, PATTERN=pat, /DEVICE
         ;POLYFILL, y, x, PATTERN=pat, /DEVICE
         IF tele LT 3 THEN $
           POLYFILL, y, x, PATTERN=pat, /DEVICE $
         ELSE $
           POLYFILL, x, y, PATTERN=pat, /DEVICE
      ENDFOR
   ENDIF
   ENDIF

   ;** PLOT LINE SHOWING UNDERSCAN REGION
   IF (uscan GT 0) THEN PLOTS, [uscan-1,uscan-1], [0,ys-1], /DEVICE

;print,"Note:"
;print,"plot_ccd.pro is using underscan pixels of "+strtrim(uscan)+' from LASCO which may need to be changed.'
;print,"also make sure block size used to shade the ROI and OCC are correct.'
;print,"Remember to fix it for SECCHI"
;print,""

   ;** PLOT FIELD OF VIEW AND SOLAR DISK
   IF (tele LE 4) THEN BEGIN ; All cameras ; AEE 12/16/03
      fov = [ [0.0, 1.7], [1.25, 4.0], [2.0, 15.0], $   ;** FOV (in Rsun) for EUVI, COR1, COR2 - AEE 12/16/03
              [0.0,75.0], [0.0,259.2]]     ; HI FOV (HI1 actual center= 49.8 and HI2 actual center=200 not 0).

      sec_r = (16. * 60.)  				;** arc sec per solar radius

      ; *** What are pixel sizes in arc seconds for EUVI, COR1, COR2? and for HI1 and HI2?

      pixel_size = [1.6,3.75,14.0,35.15,123.0] 	;** pixel size in arc seconds for EUVI, COR1, COR2, HI1&2
      pixel_size = pixel_size *  8.0			;** we've shrunk from 2048 to 256
      pix_Rsun = sec_r / pixel_size
      fov(0,*) = fov(0,*) * pix_Rsun			;** convert Rsun to pixels
      fov(1,*) = fov(1,*) * pix_Rsun			;** convert Rsun to pixels

      IF (tele LE 2) THEN BEGIN ; SCIP
        TVCIRCLE, fov(0,tele), xc+uscan, yc			;** plot inner fov
        TVCIRCLE, fov(1,tele), xc+uscan, yc,$		;** plot outer fov
           CLIP=[uscan-1,0,xs-1,ys-1], NOCLIP=0
        sp = [0.1,0.1,0.]
        IF (tele GT 0) THEN $
            TVCIRCLE, 1.0*pix_Rsun(tele), xc+uscan, yc, /FILL,$	;** plot Sun
           /LINE_FILL, SPACING=sp(tele)
      ENDIF ELSE BEGIN  ; for HI divide fov by 2 so it fits in the draw window and no sun to plot.
        TVCIRCLE, fov(0,tele)/2.0, xc+uscan, yc			;** plot inner fov
        TVCIRCLE, fov(1,tele)/2.0, xc+uscan, yc,$		;** plot outer fov
           CLIP=[uscan-1,0,xs-1,ys-1], NOCLIP=0
      ENDELSE
   ENDIF

   xyouts,136,5,'S',/DEVICE,COLOR=2
   xyouts,136,265,'N',/DEVICE,COLOR=2
   xyouts,3,136,'E',/DEVICE,COLOR=2
   xyouts,262,136,'W',/DEVICE,COLOR=2

END
