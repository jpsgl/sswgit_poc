;+
;$Id: sexist.pro,v 1.1.1.2 2004/07/01 21:19:09 esfand Exp $
;
; Project     : STEREO - SECCHI	
;
; Name        :	SEXIST
;
; Purpose     :	To See if variable Exists
;
; Explanation :	So obvious, that explaining it will take more
;               lines than the code.
;
; Use         :	A=SEXIST(VAR)
;
; Inputs      :	VAR = variable name
;
; Opt. Inputs : None.
;
; Outputs     :	1/0 if variable exists or not
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. : Based on SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;
; $Log: sexist.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:09  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


function sexist,var

return,(n_elements(var) ne 0)

end

