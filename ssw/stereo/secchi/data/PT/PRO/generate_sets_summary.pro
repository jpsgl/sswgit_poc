;+
;$Id: generate_sets_summary.pro,v 1.6 2009/09/11 20:28:11 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GENERATE_SETS_SUMMARY
;               
; Purpose     : Routine to generate s summary file for newly created SET(s), if any.
;               
; Explanation : This routine prints out the sets, if any, in current schedule to *.IPT.SETS file.
;               For each set, the set information (such as ID, creator, and the requester) followed
;               by summary of each image in the set are printed.
;               
; Use         : GENERATE_SETS_SUMMARY, ipt_name
;    
; Inputs      : ipt_name  used to name the sets filename by appending .SETS to it. 
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/04/04 - Changes filename form .IPT.SETS to .summary.SETS.
;              Ed Esfandiari 10/04/04 - direct output to SC_A or SC_B.
;
; $Log: generate_sets_summary.pro,v $
; Revision 1.6  2009/09/11 20:28:11  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:01  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO GENERATE_SETS_SUMMARY, ipt_name

COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON DIALOG, mdiag,font
COMMON SET_DEFINED, set_instance, defined_set_arr
COMMON SCHED_SHARE, schedv


  info = VIEW_SETS_SUMMARY(os_arr,defined_set_arr)

  lines= N_ELEMENTS(info)
  IF (lines GT 1) THEN BEGIN
    ;set_file_name= ipt_name+'.SETS'
    set_file_name= STRMID(ipt_name,0,STRPOS(ipt_name,'.IPT'))+'.summary.SETS'

    ;OPENW, OUT, GETENV('PT')+'/OUT/SUMMARY/'+set_file_name, /GET_LUN

    IF (schedv.sc EQ 1) THEN sc= 'SC_A'
    IF (schedv.sc EQ 2) THEN sc= 'SC_B'
    OPENW, OUT, GETENV('PT')+'/OUT/'+sc+'/SUMMARY/'+set_file_name, /GET_LUN

    WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_SETS_SUMMARY: Creating file: '+set_file_name
    PRINT,'%%%GENERATE_SETS_SUMMARY: Creating file: '+set_file_name
    WIDGET_CONTROL, /HOUR

    FOR i=0, lines-1 DO PRINTF,OUT, info(i) 
    CLOSE, OUT
    FREE_LUN, OUT
    WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_SETS_SUMMARY: (../OUT/'+sc+'/SUMMARY/'+set_file_name+') Done.'
    PRINT,'%%%GENERATE_SETS_SUMMARY: (../OUT/'+sc+'/SUMMARY/'+set_file_name+') Done.'
  ENDIF

  RETURN
END

