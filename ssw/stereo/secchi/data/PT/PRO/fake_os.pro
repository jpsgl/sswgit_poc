;+
;$Id: fake_os.pro,v 1.9 2009/09/11 20:28:09 esfand Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : FAKE_OS
;
; Purpose     : Define some sample Observation Sequences for SECCHI Schedule.
;
; Use         : FAKE_OS, startdis
;
; Inputs      : startdis        input start of display time in TAI.
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/09/04 - Added proc_time.
;              Ed Esfandiari 06/15/04 - Moved defining os_struct to define_os_struct function.
;              Ed Esfandiari 10/06/04 - Added sc_gt to common block
;              Ed Esfandiari 11/12/04 - Added BSF_CNT common block
;              Ed Esfandiari 02/07/05 - Added OS_ALL_SHARE and INIT_TIMELINE common blocks
;              Ed Esfandiari 05/11/05 - Added INIT_TL and RUN_SCRIPT common blocks
;              Ed Esfandiari 12/29/06 - Added bsf_gt to common block.
;
; $Log: fake_os.pro,v $
; Revision 1.9  2009/09/11 20:28:09  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.4  2005/03/10 16:41:56  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-
 
;__________________________________________________________________________________________________________
;

PRO FAKE_OS, startdis

COMMON OS_SCHEDULED, os_struct, os_arr, tm_arr, load_camtable, load_wlexptable, do_candc, move_fp
COMMON OS_DEFINED, defined_os_arr, os_instance
COMMON SET_DEFINED, set_instance, defined_set_arr
COMMON APIDS, multi_apid ; 2/23/04 AEE - added.
COMMON SCIP_DOORS_SHARE, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
                         sc_maneuvers,start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2 
;COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt  ; 2/23/04 AEE - added.
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt  ; 09/01/06 added rate_gt
COMMON CMD_SEQ_SCHEDULED, sched_cmdseq ; 2/23/04 AEE - added
COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON INIT_TL, ic
COMMON RUN_SCRIPT, rsc

COMMON DIALOG,mdiag,font

   ;AEE 2/6/04 - Added set_id.
   ;AEE 2/18/04 - Added set_cnt.

   OS_INIT

   os_struct= DEFINE_OS_STRUCT()

   os_arr = -1 
   quit = RESPOND_WIDG( /COLUMN, $
          mess= ['Do you want to read in a pre-defined schedule from "os_arr.sav", if one exists?'],$
          butt=['Yes','No (display an empty shedule)'],$
          group= base) 
         ;group=schedv.base)
   IF(quit EQ 0) THEN BEGIN 
     filename= GETENV('PT')+'/IN/OTHER/'+'os_arr.sav'
     f_exist = FINDFILE(filename)
     IF (f_exist(0) EQ '') THEN BEGIN
       WIDGET_CONTROL,mdiag,SET_VALUE='Could not find "os_arr.sav" in current directory.' 
       PRINT,'Could not find "os_arr.sav" in current directory.'
     ENDIF ELSE BEGIN
       filename= GETENV('PT')+'/IN/OTHER/'+'os_arr.sav'
       RESTORE, filename
       filename= GETENV('PT')+'/IN/OTHER/'+'set_arr.sav'
       RESTORE, filename
     ENDELSE
   ENDIF

   RETURN

END
