;+
;$Id: reset_stats.pro,v 1.7 2009/09/11 20:28:21 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : RESET_STATS
;               
; Purpose     : This pro initializes the Telemetry Statitstics for
;               Secchi-Buffer, RT, SSR1, SSR2, and SW channeles to
;               zeros.
;               
; Use         : RESET_STATS, schedv, startdis, enddis
;    
; Inputs      : schedv      Structure containing widgets information.
;               startdis    Start time of the schedule.
;               enddis      End time of the schedule.
; 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/25/04 - Re-arranged Tlm load section.
;              Ed Esfandiari 09/02/06 - Added nominal 3.26 kbps HK rate to SSR1 and RT.
;              Ed Esfandiari 06/20/07 - Added toggle option to display data volume/downloadable ratio or volume in MB.
;              Ed Esfandiari 09/02/09 - changed data volume display from .1 to .3 decimal places.
;
; $Log: reset_stats.pro,v $
; Revision 1.7  2009/09/11 20:28:21  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

PRO RESET_STATS, schedv, startdis, enddis

     ; Update buffer statistics date:

     date_str = UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' + $
                UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
     ;WIDGET_CONTROL, schedv.usage_dates, SET_VALUE="Data/Telemetry Loads For:  "+date_str
     WIDGET_CONTROL, schedv.usage_dates, SET_VALUE= date_str

     IF (schedv.pct_stats EQ 0) THEN $
       use= '%' $
     ELSE $
       use= ' '

     ; Set all percentages to zeros:

     tuse= 0.0 & teuvi= 0.0 & tcor1= 0.0 & tcor2= 0.0 & thi1= 0.0 & thi2= 0.0

     ;use_str= STRING(tuse,'(f12.3)')+use+STRING(teuvi,'(f13.3)')+use+STRING(tcor1,'(f9.3)')+use+ $
     ;         STRING(tcor2,'(f11.3)')+use+STRING(thi1,'(f12.3)')+use+STRING(thi2,'(f11.3)')+use
     ;WIDGET_CONTROL, schedv.sbuf_stats, SET_VALUE='SBUF '+use_str
     ;WIDGET_CONTROL, schedv.ssr1_stats, SET_VALUE='SSR1 '+use_str
     ;WIDGET_CONTROL, schedv.ssr2_stats, SET_VALUE='SSR2 '+use_str
     ;WIDGET_CONTROL, schedv.rt_stats, SET_VALUE='RTch '+use_str
     ;WIDGET_CONTROL, schedv.sw_stats, SET_VALUE='SWch '+use_str

     WIDGET_CONTROL, schedv.sb_tot, SET_VALUE= STRING(tuse,'(f12.3)')+use
     WIDGET_CONTROL, schedv.sb_eu, SET_VALUE= STRING(teuvi,'(f13.3)')+use
     WIDGET_CONTROL, schedv.sb_c1, SET_VALUE= STRING(tcor1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.sb_c2, SET_VALUE= STRING(tcor2,'(f11.3)')+use
     WIDGET_CONTROL, schedv.sb_h1, SET_VALUE= STRING(thi1,'(f12.3)')+use
     WIDGET_CONTROL, schedv.sb_h2, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.s1_tot, SET_VALUE= STRING(tuse,'(f12.3)')+use
     WIDGET_CONTROL, schedv.s1_eu, SET_VALUE= STRING(teuvi,'(f13.3)')+use
     WIDGET_CONTROL, schedv.s1_c1, SET_VALUE= STRING(tcor1,'(f9.3)')+use 
     WIDGET_CONTROL, schedv.s1_c2, SET_VALUE= STRING(tcor2,'(f9.3)')+use
     WIDGET_CONTROL, schedv.s1_h1, SET_VALUE= STRING(thi1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.s1_h2, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.s1_hk, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.s1_gt, SET_VALUE= STRING(thi2,'(f12.3)')+use

     WIDGET_CONTROL, schedv.s2_tot, SET_VALUE= STRING(tuse,'(f12.3)')+use
     WIDGET_CONTROL, schedv.s2_eu, SET_VALUE= STRING(teuvi,'(f13.3)')+use
     WIDGET_CONTROL, schedv.s2_c1, SET_VALUE= STRING(tcor1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.s2_c2, SET_VALUE= STRING(tcor2,'(f9.3)')+use
     WIDGET_CONTROL, schedv.s2_h1, SET_VALUE= STRING(thi1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.s2_h2, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.rt_tot, SET_VALUE= STRING(tuse,'(f12.3)')+use
     WIDGET_CONTROL, schedv.rt_eu, SET_VALUE= STRING(teuvi,'(f13.3)')+use
     WIDGET_CONTROL, schedv.rt_c1, SET_VALUE= STRING(tcor1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.rt_c2, SET_VALUE= STRING(tcor2,'(f9.3)')+use
     WIDGET_CONTROL, schedv.rt_h1, SET_VALUE= STRING(thi1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.rt_h2, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.rt_hk, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.rt_gt, SET_VALUE= STRING(thi2,'(f12.3)')+use
     WIDGET_CONTROL, schedv.sw_tot, SET_VALUE= STRING(tuse,'(f13.3)')+use
     WIDGET_CONTROL, schedv.sw_eu, SET_VALUE= STRING(teuvi,'(f13.3)')+use
     WIDGET_CONTROL, schedv.sw_c1, SET_VALUE= STRING(tcor1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.sw_c2, SET_VALUE= STRING(tcor2,'(f9.3)')+use
     WIDGET_CONTROL, schedv.sw_h1, SET_VALUE= STRING(thi1,'(f9.3)')+use
     WIDGET_CONTROL, schedv.sw_h2, SET_VALUE= STRING(thi2,'(f12.3)')+use

  RETURN
END
