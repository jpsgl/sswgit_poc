;+
;$Id: pulse2ms.pro,v 1.1.1.2 2004/07/01 21:19:07 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : PULSE2MS
;               
; Purpose     : This function converts number of pulses for Cal. LED images
;               to exposure time in milliseconds.
;
; Use         : msec= PULSE2MS(pulse)
;    
; Inputs      : Pulse  Number of pulses to be converted to milliseconds.
;
; Opt. Inputs : None
;               
; Outputs     : msec   Exposure time (milliseconds). 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: pulse2ms.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

FUNCTION PULSE2MS,pulses
 
  ; This function converts number of pulses to milliseconds (as
  ; required for Cal LED images). 
  ; Pulse counts > 4095 require multiple LED pulse commands with
  ; 6 ms intervals between them. There are 50K pulses per second 
  ;(50 pulses per milliseconds).


  intervals_ms= (((pulses+4094) / 4095) - 1) * 6.0 ; number of ms required between each 4095 pulses.
  exptime = pulses/50.0 + intervals_ms  ; exptime is now in units of ms

  RETURN, exptime
END
