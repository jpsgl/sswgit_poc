;+
;$Id: moveschedule.pro,v 1.1.1.2 2004/07/01 21:19:04 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : MOVESCHEDULE
;               
; Purpose     : Modify plot window of SECCHI Scheduling Tool.
;               
; Explanation : This routine is called by the Scheduling tool every time
;		the user modifies the plot start time, time span, or 
;		schedules a new OP or OS.
;               
; Use         : MOVESCHEDULE, SCHEDV=schedv, SEC=sec 
;    
; Inputs      : None.
;               
; Opt. Inputs : schedv	structure containing start and time span of current plot.
;		sec	optional secs to move plot.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: moveschedule.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;
PRO MOVESCHEDULE, SCHEDV=schedv, SEC=sec 

IF (N_ELEMENTS(sec) EQ 0) THEN sec = 0D	; assign a default that does nothing

;
; Add (or subtract) the reqested number of seconds to the input STARTDIS
; to get the new start time.  The end time is then calculated using the 
; duration that was passed into the routine.
;

 schedv.startdis = schedv.startdis + sec			; form is seconds (TAI)
 schedv.enddis   = schedv.startdis + schedv.duration 		; form is seconds (TAI)

;
; Rewrite the time text windows, only if in display mode.
; (If in edit mode, don't overwrite time that are used for studies.)
;

 IF (schedv.mode EQ 0) THEN BEGIN 

    WIDGET_CONTROL, schedv.start_text, SET_VALUE=TAI2UTC(schedv.startdis, /ECS)
    WIDGET_CONTROL, schedv.stop_text,  SET_VALUE=TAI2UTC(schedv.enddis,   /ECS)

 ENDIF

;
; Redraw the instrument plans, to the DRAW widget
;

 SCHEDULE_PLOT, schedv
                                         

RETURN
END
