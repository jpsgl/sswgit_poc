;+
;$Id: read_camera_tables.pro,v 1.9 2009/09/11 20:28:19 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : READ_CAMERA_TABLES
;               
; Purpose     : Read FSW camera setup tables.
;
; Explanation : The flight camera table contains camera setup information for each of 5 telescopes
;               in Hex format (see comments in the file). It contains 35 lines (tables) of data
;               or 7 table per telescope. Every 5 lines has a COR2, COR1, EUVI, HI2, and HI1
;               info so the first 5 lines represent the 1st table for each telescope, the
;               2nd set of 5 lines represent the 2nd table for each telescope, etc..
;               Telescopes are numbered in this file using 1=EUVI, 2=COR1, 3=COR2, 4=HI1, 5=HI2
;               instead of 0-4 used in PT.
;               The header for this file also contains 5 comment lines, one per camera, listing the 
;               camera readout and clear *.img files containing, among other things, the camera readout 
;               and clear times. These .img files are used to set the cl_time and ro_time of each
;               of 35 CCD tables. The clr_id and rdout_id from each camera setup table is used as 
;               an index to one of eight sections of a *.img file to get the clear and readout times. 
;               This routine reads in each line/table, coverts the data to base10 and stores them
;               in PT CCD structure, ccd(5,7) for 5 cameras, and 7 tables.
;               Each ccd structure/table contains the following 15 fields:
;               pform, camera, clr_id, rdout_id, x1, y1, x2, y2, xsum,ysum, offset, gain, and gmode.
;               cl_time, and ro_time.
;               
; Use         : READ_CAMERA_TABLES,fn, ccd 
;    
; Inputs      : fn      In flight camera tables filename (i.e. '../TABLES/camera_tables.dat').
;
; Opt. Inputs : None
;               
; Outputs     : ccd     A (5,7) CCD structure for 5 cameras and 7 tables provided as a parameter
;                       on input and populated on ouput. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari - 07/15/04 - Read in appropriate *.img file for each telescope,
;                                         pickup CCD readout and clear times, and add to CCD.
;              Ed Esfandiari - 07/19/04 - Read in using new data format of 2z2 and 11z4.
;              Ed Esfandiari - 07/21/04 - use tables pointed by an env-variable and fsw table names.
;              Ed Esfandiari - 12/17/04 - added rotbtb*.img table checking.
;              Ed Esfandiari - 04/18/05 - Changed from 5 tables per camera to 7 tables pre camera
;                                         in the comment lines.
;              Ed Esfandiari - 04/12/06 - Added code to use default rotbtb1a.img table if those 
;                                         mentioned in setup table hdr are not found.
;
; $Log: read_camera_tables.pro,v $
; Revision 1.9  2009/09/11 20:28:19  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/04/27 20:38:39  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.4  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION PICK_RO_CL_TIMES, str

  times_arr= FLTARR(8) ; 8 sections/times per file.

  ;f= FINDFILE(GETENV('PT')+'/TABLES/'+STRMID(str,STRPOS(str,'.img')-8,12))
  f= FINDFILE(GETENV_SLASH('TABLES')+STRMID(str,STRPOS(str,'.img')-8,12))
  IF (f(0) EQ '') THEN BEGIN
    PRINT,"WARNING: Can't loacte "+GETENV_SLASH('TABLES')+STRMID(str,STRPOS(str,'.img')-8,12)
    PRINT,"Will use deafult rotbtb1a.img table."
    f= FINDFILE(GETENV_SLASH('TABLES')+'rotbtb1a.img')
    IF (f(0) EQ '') THEN BEGIN
      PRINT,""
      PRINT,"Didn't find '+GETENV_SLASH('TABLES')+'rotbtb1a.img, either.'
      PRINT,"         IF .c is enterd, 0.0 seconds will be used for CCD clear and readout times." 
      STOP
      RETURN, times_arr
    ENDIF
  ENDIF

  OPENR, lun1, f(0), /GET_LUN 
  tmp= ''
  scnt= -1
  val= FLTARR(1)
  WHILE NOT EOF(lun1) DO BEGIN
    READF, lun1, tmp 
    tmp= STRCOMPRESS(tmp,/REMOVE_ALL) ; remove all blanks.
    IF (STRMID(tmp,0,1) EQ 'X' AND STRLEN(tmp) EQ 9) THEN BEGIN
      scnt= scnt+1
      READS, tmp, val, FORMAT='(1x,1z4)' ; pickup 2nd,3rd,4th,5th bytes. it is time in msec
      times_arr(scnt)= val/1000.0 ; change to seconds. 
    ENDIF
  ENDWHILE 

  CLOSE,lun1
  FREE_LUN,lun1 

  RETURN, times_arr

END



PRO READ_CAMERA_TABLES,fn, ccd

  ro_cl_times= FLTARR(5,8) ; 5 telescope and 8 readout/clear times per telescope.

  OPENR,lun, fn, /GET_LUN 
  line= ''
  cnt= 0
  table= 0
  WHILE NOT EOF(lun) DO BEGIN
    READF, lun, line
    line= STRTRIM(line,2)
    IF (STRMID(line,0,1) EQ 'X') THEN BEGIN
      ;print,line
      line= STRCOMPRESS(STRMID(line,1,500),/REMOVE_ALL) ; remove X and all blanks.

      data= FLTARR(13) ; each table has 13 set of data.
      ;READS, line, data, format='(13z4)'
      READS, line, data, format='(2z2,11z4)'
      cam= data(1)-1
      ccd(cam,table).pform= data(0)
      ccd(cam,table).camera= data(1)
      ccd(cam,table).clr_id= data(2)
      ccd(cam,table).rdout_id= data(3)
      ccd(cam,table).x1= data(4)
      ccd(cam,table).y1= data(5)
      ccd(cam,table).x2= data(6)
      ccd(cam,table).y2= data(7)
      ccd(cam,table).xsum= data(8)
      ccd(cam,table).ysum= data(9)
      ccd(cam,table).offset= data(10)
      ccd(cam,table).gain= data(11)
      ccd(cam,table).gmode= data(12)
      ccd(cam,table).cl_time= ro_cl_times(cam,ccd(cam,table).clr_id)
      ccd(cam,table).ro_time= ro_cl_times(cam,ccd(cam,table).rdout_id)

      cnt= cnt+1

      IF (cnt EQ 5) THEN BEGIN
        table= table+1
        cnt= 0
        ;PRINT,''
        ;PRINT,'End of 5 camera tables '+STRTRIM(table,2)
        ;PRINT,''
      ENDIF

    ENDIF ELSE BEGIN

      ; Pickup CCD readout and clear times for each telescope and table
      ; using *.img file names specified at the top of this (camera setup)
      ; file. This appear before the acutal CCD info starting with X's:

      IF (STRPOS(line,'EUVI') GT 0 AND STRPOS(line,'.img') GT 0) THEN $
         ro_cl_times(0,*) = PICK_RO_CL_TIMES(line)
      IF (STRPOS(line,'COR1') GT 0 AND STRPOS(line,'.img') GT 0) THEN $
         ro_cl_times(1,*) = PICK_RO_CL_TIMES(line)
      IF (STRPOS(line,'COR2') GT 0 AND STRPOS(line,'.img') GT 0) THEN $
         ro_cl_times(2,*) = PICK_RO_CL_TIMES(line)
      IF (STRPOS(line,'HI1') GT 0 AND STRPOS(line,'.img') GT 0) THEN $
         ro_cl_times(3,*) = PICK_RO_CL_TIMES(line)
      IF (STRPOS(line,'HI2') GT 0 AND STRPOS(line,'.img') GT 0) THEN $
         ro_cl_times(4,*) = PICK_RO_CL_TIMES(line) 
    ENDELSE
  ENDWHILE

  CLOSE,lun
  FREE_LUN,lun

  bad= WHERE(ro_cl_times EQ 0.0,bcnt)
  IF (bcnt GT 0) THEN BEGIN
    PRINT,'**** WARNING ****'
    PRINT,'Camera setup table ('+  fn + ')' 
    PRINT,'does not have rottb*.img comment lines for each telescope.'
    PRINT,'Fix and run again.' 
    STOP
  ENDIF

  RETURN
END
