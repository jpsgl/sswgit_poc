;+
;$Id: os_call_schedule.pro,v 1.10 2011/06/30 20:45:33 nathan Exp $
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_CALL_SCHEDULE
;               
; Purpose     : Routine to interface SECCHI Planning and Scheduling Tools.
;               
; Explanation : This routine is the interface between the Planning tool
;		(DEFINE_OS) and the Scheduling Tool (SCHEDULE).  It 
;		adds the current OS to the defined_os_arr.  Then calculates
;		the stats for this OS, starts SCHEDULE if needed, and 
;		updates the OS_NUM, and stats in SCHEDULE.
;               
; Use         : OS_CALL_SCHEDULE
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Restrictions: None.
;               
; Side effects: Starts SCHEDULE if needed.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;       Ed Easfandiari 06/04/04 - Set start and stop times to display-range and clear delta 
;                                 and count for a newly defined OS:
;       Ed Esfandiari 06/16/04 - Added setup and exp. times.
;       Ed Esfandiari 07/14/04 - Using 4 ROI tables instead of 1.
;       Ed Esfandiari 09/20/04 - Also used fps and sync as part of assinging OS numbers.
;       Ed Esfandiari 09/30/04 - Removed sync.
;       Ed Esfandiari 03/09/05 - Added checks for CCD and IP to chk_osnum_db and add_osnum2db.
;       Ed Esfandiari 03/03/09 - Added check fow double PW positions.
;
; $Log: os_call_schedule.pro,v $
; Revision 1.10  2011/06/30 20:45:33  nathan
; num_text no longer has OS_NUM in it
;
; Revision 1.9  2009/09/11 20:28:13  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/03/10 16:45:58  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:44  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO OS_CALL_SCHEDULE

COMMON SCHEDULE_BASE, sched_base
COMMON OS_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE
COMMON OS_DEFINED
COMMON OBS_PROG_DEF, op_id_struct, op_id_arr
COMMON DIALOG, mdiag,font
COMMON APIDS, multi_apid


   OS_STATS, dur_time, psize, pre_proc_time, ro_time, proc_time, setup_time, os_cadence, obs_mode_id 

   IF XREGISTERED("SCHEDULE") THEN BEGIN 
      WIDGET_CONTROL,mdiag,SET_VALUE='SCHEDULE is up'
      PRINT, 'SCHEDULE is up'
   ENDIF ELSE SCHEDULE

   WIDGET_CONTROL, sched_base, GET_UVALUE=schedv

;   os_num = GET_OS_NUM()
;   WIDGET_CONTROL, schedv.num_text, SET_VALUE='OS_NUM: '+STRING(os_num,'(I4.4)')
;
;   IF (DATATYPE(osv) NE 'UND') THEN $  ; AEE 10/15/03 - added
;     WIDGET_CONTROL, osv.os_text , SET_VALUE=' OS_NUM: '+STRING(os_num,'(I4.4)')

   ; AEE 06/04/04 - set start and stop times to display range for a newly defined OS:
   WIDGET_CONTROL, schedv.start_text, SET_VALUE= UTC2STR(TAI2UTC(schedv.startdis),/ECS)  
   WIDGET_CONTROL, schedv.stop_text, SET_VALUE= UTC2STR(TAI2UTC(schedv.enddis),/ECS)
   WIDGET_CONTROL, schedv.count_text, SET_VALUE= '' 
   WIDGET_CONTROL, schedv.delta_text, SET_VALUE= '' 

   ; AEE 7/7/03:
   ;zind=WHERE(multi_apid.os_num EQ 0, zcnt)
   ;IF(zcnt GT 0) THEN multi_apid(zind).os_num= os_num

   WIDGET_CONTROL, schedv.os_label, /SENSITIVE
   WIDGET_CONTROL, schedv.delta_text, /SENSITIVE
   WIDGET_CONTROL, schedv.count_text, /SENSITIVE

   WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=to	;** convert size to correct units
   new_value = SCHEDULE_CONVERT_UNITS(psize, 0, to)
   WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.size_fmt),2)+' '

   new_value = FLOAT(dur_time * schedv.os_dur_factor)         ;** units secs or mins
   WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '

   ; AEE - Dec 19, 02 - Added the proc and rot statements:
   ;new_value = FLOAT((dur_time - pre_proc_time - ro_time) * schedv.os_dur_factor)
   new_value = FLOAT(proc_time * schedv.os_dur_factor)

   WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
   new_value = FLOAT(ro_time * schedv.os_dur_factor)
   WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '

   new_value = FLOAT(setup_time * schedv.os_dur_factor)
   WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '

   new_value = FLOAT((pre_proc_time - setup_time) * schedv.os_dur_factor)
   WIDGET_CONTROL, schedv.os_exp_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '

   WIDGET_CONTROL, schedv.base, SHOW=1

   ;** save the os to the defined_os_arr
   new_entry = os_instance			;** os_instance defined in OS_DEFINED COMMON
   ;new_entry.os_num = os_num
   
   new_entry.lp = plan_lp
   ;** GET VARS FOR GIVEN LP BEING SCHEDULED

   result = GET_PLAN_LP_INFO( tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, ex_table, $
                              sub, start, cadence, fps) ; AEE 1/14/04

   i= 0 ; FOR LP=0-5 and HI1-Seq 
   l= 0
   IF (plan_lp eq 0) THEN l= 1 ; Double PW pos.
   j= 0 ; FOR os_cadence 
   IF (N_ELEMENTS(lp_num_images) EQ 2)  THEN BEGIN ;  a HI Seq
     IF (lp_num_images(1) GT 0) THEN i= 1  ; for HI2-Seq
     IF (N_ELEMENTS(os_cadence) EQ 2)  THEN j = 1 ; IF both HI1 and HI2 in same HI seq
   ENDIF

   ;CHK_OSNUM_DB, tele(i), exptable(i), ex_table, fw(i), pw(i), lamp(i), cadence(i), lp_num_images(i), $
; use os_cadence returned by os_stats.pro which may be different than cadence returned by get_plan_lp_info:
   CHK_OSNUM_DB, tele(i), exptable(i), ex_table, fw(i), pw(0:l), lamp(i), os_cadence(j), lp_num_images(i), $
                 plan_lp, psize, os_num, fps, iptable(i), camtable(i), ccd(tele(i), camtable(i)), $
                 ip(iptable(i)).steps

   IF (os_num EQ -1) THEN BEGIN
     os_num = GET_OS_NUM()
     ;ADD_OSNUM2DB, tele(i), exptable(i), ex_table, fw(i), pw(i), lamp(i), cadence(i), lp_num_images(i), $
     ADD_OSNUM2DB, tele(i), exptable(i), ex_table, fw(i), pw(0:l), lamp(i), os_cadence(j), lp_num_images(i), $
                   plan_lp, psize, os_num, fps, iptable(i), camtable(i), ccd(tele(i), camtable(i)), $
                   ip(iptable(i)).steps
   ENDIF

   WIDGET_CONTROL, schedv.num_text, SET_VALUE=STRING(os_num,'(I4.4)')

   IF (DATATYPE(osv) NE 'UND') THEN $  ; AEE 10/15/03 - added
     WIDGET_CONTROL, osv.os_text , SET_VALUE=' OS_NUM: '+STRING(os_num,'(I4.4)')

   IF (DATATYPE(defined_os_arr) NE 'UND') THEN BEGIN
     w= WHERE(defined_os_arr.os_num EQ os_num, cnt)
     IF (cnt GT 0) THEN BEGIN
       WIDGET_CONTROL, mdiag,SET_VALUE='This image setup (OS_'+STRING(os_num,'(I4.4)')+') is already defined. Use "ADD" to schedule.'
       PRINT,'This image setup (OS_'+STRING(os_num,'(I4.4)')+') Is Already defined. Use "ADD" to schedule.'
       RETURN ; This os_num is already defined and is in the defined_os_arr.
     ENDIF
   ENDIF

   ; AEE 7/7/03:
   zind=WHERE(multi_apid.os_num EQ 0, zcnt)
   IF(zcnt GT 0) THEN multi_apid(zind).os_num= os_num

   new_entry.os_num = os_num
   new_entry.tele = tele(0)   ; AEE Dec 03, 2002 - added (0)s.
   new_entry.exptable = exptable(0) ; AEE 1/14/04
   new_entry.camtable = camtable(0) ; AEE 1/14/04
   new_entry.fps = fps ; AEE 1/14/04
   new_entry.iptable = iptable(0)
   ;new_entry.cadence= cadence(0)
   new_entry.cadence= os_cadence(j)
   new_entry.pre_proc_time= pre_proc_time(j)
   new_entry.fw = fw(0)
   new_entry.pw = pw  ; AEE - Dec 06, 02
   new_entry.lamp = lamp(0)
   new_entry.sub = sub
   new_entry.start = start
   new_entry.num_images = lp_num_images(0)
   new_entry.ex = ex_table
   new_entry.ccd = ccd
   new_entry.ip = ip
   new_entry.occ_blocks = occ_blocks
   ;new_entry.roi_blocks = roi_blocks

   ; We need to keep one of the 4 input ROI tables (from OS_ALL_SHARE) as defined_os_arr.roi_blocks so 
   ; check the IP steps and find out which one of four is to be used:

   roi_table= WHICH_ROI_TABLE(ip,iptable(0))
   IF (roi_table EQ -1) THEN roi_table= 0 ; use the first ROI table as defualt when none present.
   new_entry.roi_blocks = roi_blocks(*,*,roi_table)

   IF (DATATYPE(defined_os_arr) NE 'STC') THEN $	;** first os scheduled  ; AEE 2/17/04
      defined_os_arr = new_entry		$
   ELSE $
      defined_os_arr = [defined_os_arr, new_entry]	;** append to end of scheduled array

   IF (plan_lp EQ 6) THEN BEGIN ; HI_Seq - AEE - Dec 03, 2002 - Add HI2 info
     new_entry.tele = tele(1)
     new_entry.exptable = exptable(1) ; AEE 1/14/04
     new_entry.camtable = camtable(1) ; AEE 1/14/04
     new_entry.fps = fps ; AEE 1/14/04
     new_entry.iptable = iptable(1)
     ;new_entry.cadence = cadence(1)
     new_entry.cadence = os_cadence(j)
     new_entry.pre_proc_time= pre_proc_time(j)
     new_entry.fw = fw(1)
     new_entry.pw = new_entry.pw ; same as pw for Hi1 (0-19). Only 0=clear is used for both HI1 and HI2.
     new_entry.lamp = lamp(1)
     new_entry.num_images = lp_num_images(1)
     defined_os_arr = [defined_os_arr, new_entry]
   END

   RETURN

END
