;+
;$Id: ecs_dates2ipt.pro,v 1.1.1.2 2004/07/01 21:18:59 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ECS_DATES2IPT
;               
; Purpose     : Changes the format of input ECS date to ones used in .IPT files 
;               (yyyymmdd_hhmmss).
;               
; Use         : ipt_dates= ECS_DATES2IPT(dates) 
;    
; Inputs      : dates      array of dates of format 'yyyy/mm/dd hh:mm:ss' 
;
; Opt. Inputs : None
;               
; Outputs     : ipt_dates  array of dates of format 'yyyymmdd_hhmmss'
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: ecs_dates2ipt.pro,v $
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

FUNCTION ECS_DATES2IPT, dates


   ipt_dates= STRMID(dates,0,4)+STRMID(dates,5,2)+STRMID(dates,8,2)+'_'+ $
              STRMID(dates,11,2)+STRMID(dates,14,2)+STRMID(dates,17,2)

   RETURN, ipt_dates 
END

