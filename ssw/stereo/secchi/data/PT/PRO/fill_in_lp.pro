;+
;$Id: fill_in_lp.pro,v 1.6 2005/05/26 20:00:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : FILL_IN_LP
;               
; Purpose     : Populate a SEB program from input .IPT file for a single OS.
;               
; Use         : sb = FILL_IN_LP(sb_num, key_val_arr, used) 
;    
; Inputs      : sb_num       SEB program number indicating type of image (NORMAL, DOUBLE, DARK, etc.).
;               key_val_arr  parameters pickedup from the .IPT file. 
;               used         Mark parameters that were actually used.
;              
; Opt. Inputs : None
;               
; Outputs     : sb           IDL structure containing SEB parameters.
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 05/24/05 - Added ccd orientation dependancy for masks.
;
; $Log: fill_in_lp.pro,v $
; Revision 1.6  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.5  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:42  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION FILL_IN_LP, lp_num, key_val_arr, used

       ; AEE 4/5/04:
       ccdvars = {ccdvars, pform:1, camera:0, clr_id:0, rdout_id:0, x1:1, y1:1, x2:1, y2:1, xsum:1, $
                  ysum:1, offset:0, gain:0, gmode:0, cl_time:0.0, ro_time:0.0}  

       ; note cl_time and ro_time are not in camera setup table. Must get them from readout/clear table.

   RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ;=> xyblks,xyblkpix,xypix,xstart,ystart

   CASE (lp_num) OF

      ;** Take Sky Double Image ; AEE 9/25/03 
      0 : BEGIN
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   fw:		-1,		$
                   pw:		INTARR(2),	$
                   exptime:     LONARR(2),      $
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }

             key = 'PT_FW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.fw = FIX(val)
             used(index) = 1

             key = 'PT_PW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             pw = FIX(STR2ARR(val,DELIM=','))
             num_pw = N_ELEMENTS(pw)
             IF (num_pw NE 2) THEN BEGIN
                PRINT, '%%%FILL_IN_LP: N_ELEMENTS(pw) '+STRN(num_pw)+' NE 2.  Fatal Error.'
                RETURN, -1
             ENDIF
             lp.pw = pw 
             used(index) = 1

             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             exptime = LONG(STR2ARR(val,DELIM=',')) 
             num_exp = N_ELEMENTS(exptime)
             IF (num_exp NE 2) THEN BEGIN
                PRINT, '%%%FILL_IN_LP: N_ELEMENTS(exptime) '+STRN(num_exp)+' NE 2  Fatal Error.'
                RETURN, -1
             ENDIF
             lp.exptime = exptime
             used(index) = 1

          END	;** Take Sky Double 

      ;** Take Sky Normal
      1 : BEGIN
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   fw:		-1,		$
                   pw:		-1,		$
                   ;exptime:     -1,             $
                   exptime:     -1L,            $  ; AEE - 01/27/03
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_FW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.fw = FIX(val)
             used(index) = 1
             key = 'PT_PW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             lp.pw = FIX(val)
             used(index) = 1
             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             lp.exptime = LONG(val) ; AEE - 01/27/03
             used(index) = 1

          END	;** Take Normal

      ;** Take Dark
      2 : BEGIN
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   ;exptime:	-1,		$
                   exptime:	-1L,		$  ; AEE - 01/27/03
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             ;lp.exptime = FIX(val)
             lp.exptime = LONG(val) ; AEE - 01/27/03
             used(index) = 1

          END	;** Take Dark

      ;** Take Cal Lamp
      3 : BEGIN
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   fw:		-1,		$
                   pw:		-1,		$
                   ;exptime:	-1,		$
                   exptime:	-1L,		$  ; AEE - 01/27/03
                   lamp:	-1,		$
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_FW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.fw = FIX(val)
             used(index) = 1
             key = 'PT_PW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             lp.pw = FIX(val)
             used(index) = 1
             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             lp.exptime = LONG(val)   ; AEE - 01/27/03
             used(index) = 1
             key = 'PT_CAL_LAMP' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.lamp = FIX(val)
             used(index) = 1

          END	;** Take Cal Lamp


      ;** Take Continous Image 
      4 : BEGIN
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   fw:		-1,		$
                   pw:		-1,		$
                   exptime:     -1L,            $  ; AEE - 01/27/03
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_FW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.fw = FIX(val)
             used(index) = 1
             key = 'PT_PW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             lp.pw = FIX(val)
             used(index) = 1
             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             lp.exptime = LONG(val) ; AEE - 01/27/03
             lp.exptime = 0L ; AEE - 05/20/03
             used(index) = 1

          END	;** Take Continous Image 


      ;** Take Polarization Sequence
      5 : BEGIN
             key = 'PT_PW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             pw = FIX(STR2ARR(val,DELIM=','))
             num_pw = N_ELEMENTS(pw)
             used(index) = 1
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   fw:		-1,		$
                   pw:		INTARR(num_pw)-1,		$
                   exptime:	LONARR(num_pw)-1,		$ ; AEE - 01/27/03
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_FW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.fw = FIX(val)
             used(index) = 1
             lp.pw = pw
             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             exptime = LONG(STR2ARR(val,DELIM=','))  ; AEE - 01/27/03
             num_exp = N_ELEMENTS(exptime)
             IF (num_exp NE num_pw) THEN BEGIN
                PRINT, '%%%FILL_IN_LP: N_ELEMENTS(pw) '+STRN(num_pw)+' NE N_ELEMENTS(exptime) '+ $
		   STRN(num_exp)+'.  Fatal Error.'
                RETURN, -1
             ENDIF
             lp.exptime = exptime
             used(index) = 1

          END	;** Take Polarization Sequence

      ;** Take HI Sequence
      6 : BEGIN  ; AEE - Dec 06, 02
             key = 'PT_PW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             pw = FIX(STR2ARR(val,DELIM=','))
             num_pw = N_ELEMENTS(pw)
             used(index) = 1
             lp = {tele:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   fw:		-1,		$
                   pw:		INTARR(num_pw)-1,		$
                   exptime:	LONARR(num_pw)-1,		$  ; AEE - 01/27/03
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_FW' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.fw = FIX(val)
             used(index) = 1
             lp.pw = pw
             key = 'PT_EXPTIME' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
             exptime = LONG(STR2ARR(val,DELIM=',')) ; AEE - 01/27/03
             num_exp = N_ELEMENTS(exptime)
             IF (num_exp NE num_pw) THEN BEGIN
                PRINT, '%%%FILL_IN_LP: N_ELEMENTS(pw) '+STRN(num_pw)+' NE N_ELEMENTS(exptime) '+ $
		   STRN(num_exp)+'.  Fatal Error.'
                RETURN, -1
             ENDIF
             lp.exptime = exptime
             used(index) = 1

          END	;** Take HI Sequence


      ; BLock Sequence
      7 : BEGIN
             lp = {tele:	-1,		$
                   start:	-1,		$
                   num_images:	-1,		$
                   ;dpt:		-1,	 $ ; AEE 1/19/04
                   expt:		-1,	$ ; AEE 1/19/04
                   camt:		-1,	$ ; AEE 1/19/04
                   fps:                 -1,     $ ; AEE 1/19/04
                   exptime:	-1L,		$  ; AEE - 01/27/03
                   ccd:		ccdvars,	$
                   ip_tab_num:	-1,		$
                   ip:		INTARR(26)-1,	$
                   occ:		BYTARR(xyblks*xyblks),	$
                   roi:		BYTARR(xyblks*xyblks)	$
                  }
             key = 'PT_START' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.start = FIX(val)
             used(index) = 1
             key = 'PT_STOP' & val = KEYWORD_VALUE(key_val_arr, key, index)
             IF (val EQ '') THEN GOTO, FATAL_ERROR
             lp.num_images = FIX(val)
             used(index) = 1
             GOTO, CONT
          END	;** BLock Sequence 

      ELSE : BEGIN
             PRINT, '%%%FILL_IN_LP: Unknown LP: '+STRN(lp_num)
             RETURN, -1
          END

   ENDCASE

   ;** these are common for all LPs except TAKE_SEQ and TAKE_CONCURRENT and CMD_TABLE
   key = 'PT_TELE' & val = KEYWORD_VALUE(key_val_arr, key, index)
   IF (val EQ '') THEN GOTO, FATAL_ERROR
   lp.tele = FIX(val)
   used(index) = 1

   ; AEE 1/19/04 - added these:
   key = 'PT_EXPT' & val = KEYWORD_VALUE(key_val_arr, key, index)
   IF (val EQ '') THEN GOTO, FATAL_ERROR
   lp.expt = FIX(val)
   used(index) = 1
   key = 'PT_CAMT' & val = KEYWORD_VALUE(key_val_arr, key, index)
   IF (val EQ '') THEN GOTO, FATAL_ERROR   
   lp.camt = FIX(val)   
   used(index) = 1
   key = 'PT_FPS' & val = KEYWORD_VALUE(key_val_arr, key, index)
   IF (val EQ '') THEN GOTO, FATAL_ERROR   
   lp.fps = FIX(val)   
   used(index) = 1

   key = 'PT_CAM_RO' & ccd1 = KEYWORD_VALUE(key_val_arr, key, index)
   IF (ccd1 EQ '') THEN GOTO, FATAL_ERROR
   used(index) = 1
   key = 'PT_CAM_OTH' & ccd2 = KEYWORD_VALUE(key_val_arr, key, index)
   IF (ccd2 EQ '') THEN GOTO, FATAL_ERROR
   used(index) = 1
   lp.ccd = FILL_IN_CCD(ccdvars, ccd1, ccd2)

   key = 'PT_IP_TAB_NUM' & val = KEYWORD_VALUE(key_val_arr, key, index)
   IF (val EQ '') THEN GOTO, FATAL_ERROR
   IF (lp_num EQ 19) THEN BEGIN	;** TAKE SUM requires 3 ip tables
      IF (STRMID(val,0,1) EQ '(') THEN val = STRMID(val,1,STRLEN(val)-2)
      val = FIX(STR2ARR(val,DELIM=','))
   ENDIF
   lp.ip_tab_num = FIX(val)
   used(index) = 1

   key = 'PT_IP' & ip = KEYWORD_VALUE(key_val_arr, key, index)
   IF (ip EQ '') THEN GOTO, FATAL_ERROR
   used(index) = 1
   ip = FIX(STR2ARR(STRMID(ip,1,STRLEN(ip)-2),DELIM=','))
   lp.ip(0:N_ELEMENTS(ip)-1) = ip

   key = 'PT_SC_ID' & sc = KEYWORD_VALUE(key_val_arr, key, index)
   IF (sc EQ '') THEN $
     sc= 'A' $
   ELSE $
     sc= (STRING(STR2ARR(STRMID(sc,1,STRLEN(sc)-2),DELIM=',')))(0)
   IF (sc EQ 'AB') THEN sc= 'A'
   
   telid= ['EUVI','COR1','COR2','HI1','HI2']
   telstr= telid(lp.tele) 

   key = 'PT_OCC_MASK' & val = KEYWORD_VALUE(key_val_arr, key, index, /GET_ALL)
   IF (val NE '') THEN BEGIN
      used(index) = 1
      ;lp.occ = MASK_RUNS2ARR(val)
      lp.occ = MASK_RUNS2ARR(val,sc,telstr)
   ENDIF
   key = 'PT_ROI_MASK' & val = KEYWORD_VALUE(key_val_arr, key, index, /GET_ALL)
   IF (val NE '') THEN BEGIN
      used(index) = 1
      ;lp.roi = MASK_RUNS2ARR(val)
      lp.roi = MASK_RUNS2ARR(val,sc,telstr)
   ENDIF

   CONT:

   RETURN, lp

   FATAL_ERROR:
      PRINT, '%%%FILL_IN_LP: '+key+' undefined.  Fatal Error.'
      RETURN, -1
END
