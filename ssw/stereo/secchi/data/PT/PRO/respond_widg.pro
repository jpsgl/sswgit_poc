;+
;$Id: respond_widg.pro,v 1.1.1.2 2004/07/01 21:19:08 esfand Exp $
;
; Project     :	STEREO - SECCHI 
;
; Name        :	RESPOND_WIDG
;
; Purpose     :	Widget to prompt user for (e.g.) a "YES" or "NO" response.
;
; Explanation :	
;	Creates a widget to prompt user for a "YES" or "NO" response as a
;	default.  But you may pass your own button names with the BUTTONS
;	keyword.
;
; Use         :	
;   var = respond_widg, [message=message, buttons=buttons, /column,
;			/row, group_leader=group]
;
; Inputs      :	
;	MESSAGE : 	String or string vector with message to be 
;		    	displayed in widget
;			The default message is 'Respond YES or NO '	
;	BUTTONS :	String vector where each element will be a button
;			You may have as many elements as will fit.
;	TITLE   :	string to appear as widget's banner
;	COLUMN  :	if set then the buttons will be verically placed
;	ROW	:	if set then the buttons will be horizontally placed
;				(/ROW is the default and it overrides /COLUMN)
;	GROUP_LEADER : 	Causes the widgets destruction if parent is killed
;	XOFFSET :       X-POSITION OF BASE IN PIXEL UNITS
;	YOFFSET :       Y-POSITION OF BASE IN PIXEL UNITS
;       X_SCROLL_SIZE:  If set, x-axis scroll is enabled using the provided size for message.
;       Y_SCROLL_SIZE:  If set, y-axis scroll is enabled using the provided size for message.
;
; Opt. Inputs :	None.
;
; Outputs     :	
;	Returns a -1 if program fails else it returns the index number of 
;	the button that was selected, as determined by the array BUTTONS.
;       Indexes start at 0.  
;	If you passed this array then you already know your values.
;	If you use the default BUTTONS array then 
;		   0 if "YES" was selected
;	           1 if "NO" was selected
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	None.
;
; Restrictions:	
;	Must have X windows device.
;
; Side effects:	None.
;
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: respond_widg.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;

pro respond_event, event             	; the event handler for RESPOND_WIDG
common respond_widget, result

;
; Only two things could have happen: the user clicked on the YES or the NO
;
 widget_control, event.id, get_uvalue=input     ; which was clicked

 result = long(input)				; this button was clicked

 widget_control, event.top, /destroy            ; get rid of widget

return
end



function respond_widg, message=message, buttons=buttons, title=title, $
		       xoffset=xoffset, yoffset=yoffset, $
                       x_scroll_size=x_scroll_size, $
                       y_scroll_size=y_scroll_size, $
		       column=column, row=row, group_leader=group

common respond_widget, result

result = -1
if n_elements(title) eq 0 then title='Use mouse to choose'	

if keyword_set(column) then rows = 0 else rows=1
if keyword_set(row) then rows = 1
if keyword_set(xoffset) then xoffset=xoffset(0) else xoffset=0
if keyword_set(yoffset) then yoffset=yoffset(0) else yoffset=0

mess_loop = n_elements(message)-1   &  if mess_loop eq -1 then begin
					  message = 'Respond YES or NO '	
					  mess_loop = 0
				       endif

if not keyword_set(buttons) then but_names = [' YES ', '  NO '] $ ; defaults
			    else but_names = buttons	          ; user's
but_loop = n_elements(but_names)   &  but_junk = lonarr(but_loop)

;
; Create widget with a message and as many buttons as in the array BUTTONS,
; which will require one of the buttons to be selected
;

  if rows then base = widget_base(title=title, /row, $
		    			xoffset=xoffset, $
					yoffset=yoffset, $
					xpad=25, ypad=25, space=20) $
	  else base = widget_base(title=title, /column, $
		    			xoffset=xoffset, $
					yoffset=yoffset, $
					xpad=25, ypad=25, space=20)

       if not keyword_set(x_scroll_size) then x_scroll_size=0
       if not keyword_set(y_scroll_size) then y_scroll_size=0

       ; write the message in a column, regardless.

        label = widget_base(base, /column, x_scroll_size=x_scroll_size, y_scroll_size= y_scroll_size)

	for i = 0, mess_loop do lab = widget_label(label, value=message(i))

	for i = 0, but_loop-1 do  $
		but_junk(i) = widget_button(base, value=but_names(i), $
						  uvalue=strtrim(i))

	widget_control, base, /realize
	xmanager, 'respond', base, group_leader=group, /modal

return, result
end

