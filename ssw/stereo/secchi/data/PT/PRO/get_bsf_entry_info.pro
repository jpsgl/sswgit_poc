;+
;$Id: get_bsf_entry_info.pro,v 1.4 2005/05/26 20:00:58 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GET_BSF_ENTRY_INFO
;
; Purpose     : This pro uses an entry from a command seq file (*.bsf), parses it, and return
;               the os_num, the relative start time, and the corresponding IPT filename.
;
; Use         : GET_BSF_ENTRY_INFO, entry, osnum, delay, iptname
;
; Inputs      : entry  A command line from .bsf to be scheduled.      
;
; Opt. Inputs : None
;
; Outputs     : osnum    OS number of the command
;               delay    delay time (seconds) relative to the start_time of .bsf file.
;               iptname  Corresponding .IPT name for the entry to be scheduled.
;
; Opt. Outputs: None
;
; Keywords    :
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;               Ed Esfandiari 05/03/05 - pickup os_num from new location (img_cntr removed).
;
; Modification History:
;
; $Log: get_bsf_entry_info.pro,v $
; Revision 1.4  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:01  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


PRO GET_BSF_ENTRY_INFO,entry,osnum,delay,iptname

  ; pickup os_num and delay time (seconds) for each os_num (delays are relative to the start_time):
  ; Bytes 3-10 (in IDL) are the RTS time:

  delay= FIX(STRMID(entry,3,2))*86400.0 + FIX(STRMID(entry,5,2))*3600.0 + $
         FIX(STRMID(entry,7,2))*60.0    + FIX(STRMID(entry,9,2))

  ;osnum= STRMID(entry,21,4) ; bytes 22-25 are the os_num
  osnum= STRMID(entry,17,4) ; bytes 18-21 are the os_num since img_cntr is removed.

  iptname= 'OS_'+osnum+'.IPT'

  RETURN
END
