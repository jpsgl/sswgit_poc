;+
;$Id: bsf.pro,v 1.8 2011/06/30 20:37:15 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : BSF
;               
; Purpose     : An interactive widget to select a Block Sequence File (.bsf).
;               
; Use         : status= BSF(bsfile,start_time, n_sched, bsf_cad) 
;    
; Inputs      : start_time   Default time (start of schedule) to schedule the selected .bsf file  
;
; Opt. Inputs : None
;               
; Outputs     : bsfile      Name of the selected bsfile. 
;             : start_time  Selected time (default or modified) to schedule the .bsf file.
;             : n_sched     Number of time to schedule a .bsf file (default=1)
;             : bsf_cad     Cadence for multiple .bsf schedule in minutes (default = 120 min)
;             : status      Success or failuer to select a .bsf file.
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/08/04  Allow all 8.3 block seq filenames.
;              Ed Esfandiari 11/04/04  Added conflict warning.
;              Ed Esfandiari 04/-2/07  Added capability to schedule a BSF file multiple times at a given cadence.
;              Ed Esfandiari 01/30/09  Used WIDGET_COMBOBOX for idl 5.6+ (uses scroll bar to see all files).
;
; $Log: bsf.pro,v $
; Revision 1.8  2011/06/30 20:37:15  nathan
; use pickfile to select bsf
;
; Revision 1.7  2009/09/11 20:28:07  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/08 15:19:34  esfand
; allow all 8.3 block seq filenames
;
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


PRO BSF_EVENT, event

COMMON LP_BSF_SHARE, lpcmdv

help,event.id
CASE (event.id) OF

   lpcmdv.dismiss : BEGIN       ;** exit program
      lpcmdv.scheduled= 0 ; false 
      lpcmdv.bsfile= '' 
      lpcmdv.start_time= ''
      WIDGET_CONTROL, /DESTROY, lpcmdv.base1
   END

   lpcmdv.schedule : BEGIN

      WIDGET_CONTROL, lpcmdv.time_text, GET_VALUE= stime
      lpcmdv.start_time= STRTRIM(stime(0),2) 
      WIDGET_CONTROL, lpcmdv.num_sched, GET_VALUE= nsched
      lpcmdv.n_sched= STRTRIM(nsched(0),2)
      WIDGET_CONTROL, lpcmdv.cadence, GET_VALUE= cad
      lpcmdv.cad= STRTRIM(cad(0),2)
      lpcmdv.scheduled= 1 ; true 

      WIDGET_CONTROL, /DESTROY, lpcmdv.base1
    END

   lpcmdv.bsfind : BEGIN
     lpcmdv.bsfile= lpcmdv.files(event.index)
   END

   lpcmdv.time_text : BEGIN
     WIDGET_CONTROL, lpcmdv.time_text, GET_VALUE= stime & stime = stime(0)
     lpcmdv.start_time= stime
   END

   lpcmdv.num_sched : BEGIN
     WIDGET_CONTROL, lpcmdv.num_sched, GET_VALUE= nsched & nsched = nsched(0)
     lpcmdv.n_sched= nsched
   END

   lpcmdv.cadence : BEGIN
     WIDGET_CONTROL, lpcmdv.cadence, GET_VALUE= cad & cad = cad(0)
     lpcmdv.cad= cad
   END

   ELSE : BEGIN
   END

ENDCASE

END

 

FUNCTION BSF,bsfile,start_time, n_sched, bsf_cad

COMMON LP_BSF_SHARE, lpcmdv

    scheduled= 0 ; false 

    IF XRegistered("BSF") THEN RETURN, scheduled   ; AEE - Mar 20, 03

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************


      base1 = WIDGET_BASE(/COLUMN, TITLE='Block Sequence File (BSF)', /FRAME)

      schedule = WIDGET_BUTTON(base1, VALUE=' Insert into Plan ')

      row0 = WIDGET_BASE(base1, /ROW)

      col = WIDGET_BASE(row0, /COLUMN, /FRAME)

      ; AEE 12/29/03 - select a Block Sequence File (*.bsf) from ./BSF directory:

      row = WIDGET_BASE(col, /ROW)
      
        temp = WIDGET_LABEL(row, VALUE='Selected Block Seq File:')
	
        ;files= FINDFILE(GETENV('PT')+'/IN/BSF/*.bsf')
        ;files= FINDFILE(GETENV('PT')+'/IN/BSF/????????.???') ; allow all 8.3 filenames
        
	refind:
        IF (STRLEN(bsfile) EQ 0) THEN BEGIN
    	    files = PICKFILE(PATH= GETENV('PT')+'/IN/BSF', FILTER='*.bsf')
    	    ind= 0 
    	    bsfile= files(0)
        ENDIF ELSE BEGIN
            ;ind= WHERE(files EQ bsfile,cnt)
            IF ~file_exist(bsfile) THEN BEGIN
            	PRINT,''
            	PRINT, bsfile + ' does NOT exist.'
            	PRINT,''
            	ind= 0
		bsfile=''
		goto, refind
            ENDIF
        ENDELSE

    	bsfwi=widget_text(row,value=bsfile)

      row = WIDGET_BASE(col, /ROW)
        temp = WIDGET_LABEL(row, VALUE='                                           yyyy/mm/dd hh:mm:ss')
      row = WIDGET_BASE(col, /ROW)
        temp = WIDGET_LABEL(row, VALUE='Start Block Seq File at:')
        time_text= WIDGET_TEXT(row, /EDITABLE, YSIZE=1, XSIZE=24,VALUE=STRMID(start_time,0,19))
        temp = WIDGET_LABEL(row, VALUE=' Schedule ')
        n_sched= '1'
        num_sched= WIDGET_TEXT(row, /EDITABLE, YSIZE=1, XSIZE=3,VALUE=n_sched)
        temp = WIDGET_LABEL(row, VALUE=' at Cadence of ')
        cad= '120'
        cadence= WIDGET_TEXT(row, /EDITABLE, YSIZE=1, XSIZE=5,VALUE=cad)
        temp = WIDGET_LABEL(row, VALUE=' minutes')

      row = WIDGET_BASE(col, /ROW)
      
      msg= '          Schedule is NOT checked for possible BSF insert conflicts. Insert only in an empty section of schedule.'
      font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
      ;temp = WIDGET_LABEL(row, VALUE= msg, /ALIGN_CENTER, FONT=font)
      temp = WIDGET_LABEL(row, VALUE= msg, FONT=font)

     dismiss = WIDGET_BUTTON(base1, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base1
    WIDGET_CONTROL, time_text, /INPUT_FOCUS

    lpcmdv = CREATE_STRUCT( 'base1', base1,                     $
                             'files',files,                     $
                             'bsfind', 0,                  $
                             'bsfile', bsfile,                  $ 
                             'start_time', start_time,          $
                             'time_text', time_text,            $
                             'num_sched',num_sched,             $
                             'n_sched', n_sched,                $
                             'cadence',cadence,                 $
                             'cad',cad,                         $
                             'dismiss', dismiss,                $
                             'scheduled', scheduled,            $
                             'schedule', schedule)

    XMANAGER, "BSF", base1, /MODAL

bsfile= lpcmdv.bsfile
start_time= lpcmdv.start_time 
n_sched= FIX(lpcmdv.n_sched)
bsf_cad= DOUBLE(lpcmdv.cad)

; help,start_time,n_sched,bsf_cad

  RETURN,lpcmdv.scheduled

END


