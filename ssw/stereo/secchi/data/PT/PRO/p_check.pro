;+
;$Id: p_check.pro,v 1.2 2004/09/01 15:40:46 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : P_CHECK()
;               
; Purpose     : Function to do parameter checking for SECCHI DEFINE_OS Tool.
;               
; Explanation : This tool allows the user to specify a specific SECCHI 
;               Observing Sequence (OS) consisting of a LEB Program (LP),
;		telescope setup, camera setup, image processing steps, etc.
; 		The tool then calculates the estimated time to process the
;		image(s) and the processed image size.  The SECCHI 
;		Scheduling tool can then be called from this tool.
;               
; Use         : result = P_CHECK( value, name, new )
;    
; Inputs      : value	The value that you are checking.
;		name  	The name of the variable you are checking.
;			['x1','x2','y1','y2','xsum','ysum','exp','expt','wl','wlexp', 'order', $
;                        'nclrs',  'lowhtr','num', 'start', 'cmd']
;               
; Opt. Inputs : None.
;               
; Outputs     : result	Returns 0 for an invalid param, otherwise 1.
;               new	A string containing the checked (valid) parameter.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 07/14/04 - Removed htimer.
;
; $Log: p_check.pro,v $
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:06  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;________________________________________________________________________________________________________
;
 
FUNCTION P_CHECK, value, name, new

COMMON OS_ALL_SHARE
COMMON OS_TABLE_CAMERA_SHARE
COMMON OS_TABLE_EXPOSURE_SHARE

   ;;** 'x1','x2','y1','y2','xsum','ysum','leb_sum','nclrs','lpulse' 
   ;** 'x1','x2','y1','y2','xsum','ysum','leb_sum','nclrs'
   ;**  are entered from the Camera Parameters table
   ;** 'exp' is entered from the Exposure Durations table
   ;** 'expt' is entered from the Dark Image LP
   ;** 'cmd' is start or stop index for cmd table

   vars = ['x1','x2','y1','y2','xsum','ysum','exp','expt','wl','wlexp', 'order', $
           'nclrs', 'lowhtr', 'num', 'start', 'cmd']

   value = STRTRIM(value, 2)

   index = WHERE(vars EQ name)
   index = index(0)
   IF (index(0) LT 0) THEN BEGIN	;** not a valid named variable
      new = '?'
      RETURN, 0
   ENDIF

   ON_IOERROR, L1

   ;** summing can't be 1 has to be [0,2,3,4....]
   IF ((name EQ 'xsum') OR (name EQ 'ysum') AND (FIX(value) LE 1)) THEN BEGIN
      new = '0'
      RETURN, 0
   ENDIF

   IF ((name EQ 'x2') AND (STRMID(value,0,1) EQ '+')) THEN BEGIN
      WIDGET_CONTROL, ccdv.x1_text, GET_VALUE=x1 & x1 = FIX(x1(0))
      x2 = STRMID(value,1,STRLEN(value)-1)
      good = P_CHECK(x2,'x2',new2)
      IF (good) THEN BEGIN
         new = STRTRIM(STRING(x1 + FIX(new2)-1),2)
         RETURN, 1
      ENDIF
   ENDIF

   IF ((name EQ 'y2') AND (STRMID(value,0,1) EQ '+')) THEN BEGIN
      WIDGET_CONTROL, ccdv.y1_text, GET_VALUE=y1 & y1 = FIX(y1(0))
      y2 = STRMID(value,1,STRLEN(value)-1)
      good = P_CHECK(y2,'y2',new2)
      IF (good) THEN BEGIN
         new = STRTRIM(STRING(y1 + FIX(new2)-1),2)
         RETURN, 1
      ENDIF
   ENDIF

   IF ((name EQ 'exp') AND (STRMID(value,0,1) EQ '*')) THEN BEGIN
      ;** account for any summing for this tele,table configuration
      sumfactor = ccd(exv.tele,exv.table).xsum * ccd(exv.tele,exv.table).ysum
      default = ROUND(exd(exv.tele,exv.table,exv.fw,exv.pw) / FLOAT(sumfactor))
      factor = STRMID(value,1,STRLEN(value)-1)
      IF (STRMID(factor,0,1) EQ '.') THEN BEGIN
         factor = STRMID(factor,1,STRLEN(factor)-1)
         good = P_CHECK(factor,'exp',new2)
         new2 = '.'+new2
      ENDIF ELSE BEGIN
         good = P_CHECK(factor,'exp',new2)
      ENDELSE
      IF (good) THEN BEGIN
         new = STRTRIM(STRING(ROUND(default * FLOAT(new2))),2)
         RETURN, 1
      ENDIF
   ENDIF

   IF (name EQ 'nclrs') THEN BEGIN
      IF (FIX(value) LT 0) THEN value = '0'
      IF (FIX(value) GT 15) THEN value = '15'
   ENDIF

   IF (name EQ 'lowhtr') THEN BEGIN
      IF (FIX(value) LT 0) THEN value = '0'
      IF (FIX(value) GT 255) THEN value = '255'
   ENDIF

   IF (name EQ 'num') THEN BEGIN
      IF (FIX(value) LE 0) THEN value = '1'
      IF (FIX(value) GT 60) THEN value = '60'
   ENDIF

   IF (name EQ 'start') THEN BEGIN
      IF (FIX(value) LT 0) THEN value = '0'
      IF (FIX(value) GT 59) THEN value = '59'
   ENDIF

   IF (name EQ 'cmd') THEN BEGIN
      IF (FIX(value) LT 0) THEN value = '0'
      IF (FIX(value) GT 1000) THEN value = '999'
   ENDIF

   IF ( (name EQ 'expt') ) THEN valuei = LONG(value) $  : HI expt (ms) may be more than a fix.
   ELSE valuei = FIX(value)

   IF (valuei LE 0) THEN GOTO, L1	;** LE zero

   new = STRTRIM(STRING(valuei),2)	;** VALID
   RETURN, 1

   L1: 
   IF (name EQ 'exp') THEN $
      new = STRTRIM(STRING(exd(exv.tele,exv.table,exv.fw,exv.pw)),2) $
   ELSE $
   IF (name EQ 'expt') THEN $
      new = '0' $
   ELSE $
   IF (name EQ 'wl') THEN $
      new = '0' $
   ELSE $
   IF (name EQ 'wlexp') THEN $
      new = '0' $
   ELSE $
   IF (name EQ 'order') THEN $
      new = '0' $
   ELSE $
   IF (name EQ 'nclrs') THEN $
      new = '0' $
   ELSE $
   IF (name EQ 'lowhtr') THEN $
      new = '0' $
   ELSE $
   IF (name EQ 'start') THEN $
      new = '0' $
   ELSE $
      new = STRTRIM(STRING(ccd(ccdv.tele,ccdv.table).(index)),2)

   RETURN, 0

END

;________________________________________________________________________________________________________
;
