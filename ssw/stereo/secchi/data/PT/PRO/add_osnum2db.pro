;+
;$Id: add_osnum2db.pro,v 1.7 2009/09/11 20:28:07 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ADD_OSNUM2DB
;               
; Purpose     : Add a new observation Id (OS_NUM) to the previously accumulated Ids.
;               
; Explanation :
;
; Use         : ADD_OSNUM2DB, tele, exptable, extimes, fw, pw, led, cadence, n_images,
;                             lp_num, size, os_num, fps
;    
; Inputs      : tele       - telescope number
;             : exptable   - exposure table entry number 
;             : exptimes   - table of exposure times
;             : fw         - Filter Wheel positions
;             : pw         - Polarizer/Sector Wherel positions
;             : led        - calibration LED ID
;             : cadence    - tadence between images (for Sequences)
;             : n_images   - number of images (for Sequences)
;             : lp_num     - type of image taken
;             : size       - size of image taken
;             : os_num     - Observation ID
;             : fps        - FPS ON/OFF indicator 
;             : iptable    - Image processing table number
;             : camtable   - Camera processing table number
;             : ccd        - CCD setup
;             : ip_steps   - Image Processing steps
;
; Opt. Inputs : None
;
; Outputs     : None
;               
; Keywords    : None
;
; Restrictions: None
;
; Side effects: It updates defined_osnum.sav save set.               
;
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : None
;
; Written By  : Ed Esfandiari, NRL - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 03/09/05 - Added CCD and IP info.
;              Ed Esfandiari 05/22/07 - Added exptable # to save structure. 
;
; $Log: add_osnum2db.pro,v $
; Revision 1.7  2009/09/11 20:28:07  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2005/03/10 16:41:14  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
;
;-


PRO ADD_OSNUM2DB, tele, exptable, extimes, fw, pw, led, cadence, n_images, lp_num, size, os_num, $
                  fps, iptable, camtable, ccd, ip_steps

COMMON OS_INIT_SHARE

  ; Change filter position to actual description:
  fws= FILTER2STR(fw_types,EXPTABLE,fw(0),tele)

  ; Change polar/sector wheel positions (maximum of 5) to actual descriptions.
  ; If less than 5 pw, then set the rest to ''.
  ; Also do the same for exp. times:

  pws= POLAR2STR(pw_types,exptable,pw(0),tele)
  exptm= extimes(tele,exptable,fw(0),pw(0))
  FOR i= 1, N_ELEMENTS(pw)-1 DO BEGIN
    pws= [pws,POLAR2STR(pw_types,exptable,pw(i),tele)]
    exptm= [exptm,extimes(tele,exptable,fw(0),pw(i))]
  ENDFOR
  k= N_ELEMENTS(pws)
  FOR j= k, 4 DO BEGIN
    pws= [pws,'']
    exptm= [exptm,0.0]
  ENDFOR

  os_info1 = {os_info1,            $
             tele: tele,         $
             size: size,         $
             fw: fws,            $
             pw: pws(0:4),       $
             exptable: exptable, $
             exp: exptm(0:4),    $
             led: led,           $
             cad: cadence,       $
             n_images: n_images, $
             lp_num: lp_num,     $
             fps: fps,           $
             iptable: iptable,   $
             camtable: camtable, $
             ccd: ccd,           $
             ip_steps: ip_steps, $
             os_num: os_num      }

  f= FINDFILE(GETENV('PT')+'/IN/OTHER/defined_osnum.sav')
  IF (STRLEN(f(0)) NE 0) THEN BEGIN
    RESTORE, f(0) ; => os_db structure of os_info1
    os_db= [os_db,os_info1]
  ENDIF ELSE BEGIN
    os_db= os_info1
  ENDELSE

  SAVE, FILENAME= GETENV('PT')+'/IN/OTHER/defined_osnum.sav', os_db
   
  RETURN
END

