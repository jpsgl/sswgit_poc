;+
;$Id: toggle_stats.pro,v 1.3 2009/09/11 20:28:24 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : TOGGLE_STATS
;               
; Purpose     : This pro toggles the Telemetry Statitstics for
;               Secchi-Buffer, RT, SSR1, SSR2, and SW channeles 
;               between volumes and percent of volume/downloadable. 
;               
; Use         : TOGGLE_STATS, schedv, startdis, enddis
;    
; Inputs      : schedv      Structure containing widgets information.
;               startdis    Start time of the schedule.
;               enddis      End time of the schedule.
; 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : NA 
;
; Written by  : Ed Esfandiari, NRL, June 2006 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/02/09 - changed data volume display from .1 to .3 decimal places.
;
; $Log: toggle_stats.pro,v $
; Revision 1.3  2009/09/11 20:28:24  esfand
; use 3-digit decimals to display volumes
;
;
;-

PRO TOGGLE_STATS, schedv, startdis, enddis
COMMON TM_DATA

     ; Update buffer statistics date:

     date_str = UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' + $
                UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
     ;WIDGET_CONTROL, schedv.usage_dates, SET_VALUE="Data/Telemetry Loads For:  "+date_str
     WIDGET_CONTROL, schedv.usage_dates, SET_VALUE= date_str

;help,schedv.pct_stats

     IF (schedv.pct_stats EQ 0) THEN BEGIN 
       use= '%' 
       ;help,sb_usage,sb_total_bits_avail
       IF (sb_total_bits_avail EQ 0) THEN BEGIN
         pct_total = 0.0
         pct_each = FLTARR(5)
       ENDIF ELSE BEGIN
         pct_total = (DOUBLE(TOTAL(sb_usage)) / sb_total_bits_avail) * 100
         pct_each = (DOUBLE(TOTAL(sb_usage,2)) / sb_total_bits_avail) * 100
       ENDELSE
       sb_tot= STRING(pct_total,'(f12.3)')  +use
       sb_eu=  STRING(pct_each(0),'(f13.3)')+use
       sb_c1=  STRING(pct_each(1),'(f13.3)')+use
       sb_c2=  STRING(pct_each(2),'(f11.3)')+use
       sb_h1=  STRING(pct_each(3),'(f12.3)')+use
       sb_h2=  STRING(pct_each(4),'(f11.3)')+use
       
       s1_tot= "     (No "
       s1_eu=  "Playback"
       s1_c1=  "Present  "
       s1_c2=  "Within   "
       s1_h1=  "Selected "
       s1_h2=  "Range)   "
       s1_hk= ""
       s1_gt= ""
       IF (s1_total_bits_avail GT 0) THEN BEGIN
         pct_total = (DOUBLE(TOTAL(s1_usage)) / s1_total_bits_avail) * 100
         pct_each = (DOUBLE(TOTAL(s1_usage,2)) / s1_total_bits_avail) * 100
         s1_tot= STRING(pct_total,'(f12.3)')  +use
         s1_eu=  STRING(pct_each(0),'(f13.3)')+use
         s1_c1=  STRING(pct_each(1),'(f13.3)')+use
         s1_c2=  STRING(pct_each(2),'(f11.3)')+use
         s1_h1=  STRING(pct_each(3),'(f12.3)')+use
         s1_h2=  STRING(pct_each(4),'(f11.3)')+use
         s1_hk=  STRING(pct_each(5),'(f11.3)')+use
         s1_gt=  STRING(pct_each(6),'(f11.3)')+use 
;help,s1_total_bits_avail,pct_total,pct_each,s1_usage,s1_h1,s1_h2
;stop

       ENDIF

       s2_tot= "     (No "
       s2_eu=  "Playback"
       s2_c1=  "Present  "
       s2_c2=  "Within   "
       s2_h1=  "Selected "
       s2_h2=  "Range)   "
       IF (s2_total_bits_avail GT 0) THEN BEGIN
         pct_total = (DOUBLE(TOTAL(s2_usage)) / s2_total_bits_avail) * 100
         pct_each = (DOUBLE(TOTAL(s2_usage,2)) / s2_total_bits_avail) * 100
         s2_tot= STRING(pct_total,'(f12.3)')  +use
         s2_eu=  STRING(pct_each(0),'(f13.3)')+use
         s2_c1=  STRING(pct_each(1),'(f13.3)')+use
         s2_c2=  STRING(pct_each(2),'(f11.3)')+use
         s2_h1=  STRING(pct_each(3),'(f12.3)')+use
         s2_h2=  STRING(pct_each(4),'(f11.3)')+use
       ENDIF

       ;help,rt_usage_avail,rt_total_bits_avail
       IF (rt_total_bits_avail EQ 0) THEN BEGIN
         pct_total = 0.0
         pct_each = FLTARR(7)
       ENDIF ELSE BEGIN
         pct_total = (DOUBLE(TOTAL(rt_usage_avail)) / rt_total_bits_avail) * 100
         pct_each = (DOUBLE(TOTAL(rt_usage_avail,2)) / rt_total_bits_avail) * 100
       ENDELSE
       rt_tot= STRING(pct_total,'(f12.3)')  +use
       rt_eu=  STRING(pct_each(0),'(f13.3)')+use
       rt_c1=  STRING(pct_each(1),'(f13.3)')+use
       rt_c2=  STRING(pct_each(2),'(f11.3)')+use
       rt_h1=  STRING(pct_each(3),'(f12.3)')+use
       rt_h2=  STRING(pct_each(4),'(f11.3)')+use
       rt_hk=  STRING(pct_each(5),'(f11.3)')+use
       rt_gt=  STRING(pct_each(6),'(f11.3)')+use

       ;help,sw_total_bits_avail,sw_total_bits_avail
       IF (sw_total_bits_avail EQ 0) THEN BEGIN
         pct_total = 0.0
         pct_each = FLTARR(5)
       ENDIF ELSE BEGIN
         pct_total = (DOUBLE(TOTAL(sw_usage)) / sw_total_bits_avail) * 100
         pct_each = (DOUBLE(TOTAL(sw_usage,2)) / sw_total_bits_avail) * 100
       ENDELSE
       sw_tot= STRING(pct_total,'(f12.3)')  +use
       sw_eu=  STRING(pct_each(0),'(f13.3)')+use
       sw_c1=  STRING(pct_each(1),'(f13.3)')+use
       sw_c2=  STRING(pct_each(2),'(f11.3)')+use
       sw_h1=  STRING(pct_each(3),'(f12.3)')+use
       sw_h2=  STRING(pct_each(4),'(f11.3)')+use

     ENDIF ELSE BEGIN 
       use= ' '
       pct_total = DOUBLE(TOTAL(sb_usage))/8000000.0 ; bits to MB
       pct_each =  DOUBLE(TOTAL(sb_usage,2))/8000000.0 ; bits to MB
       sb_tot= STRING(pct_total,'(f12.3)')  +use
       sb_eu=  STRING(pct_each(0),'(f13.3)')+use
       sb_c1=  STRING(pct_each(1),'(f13.3)')+use
       sb_c2=  STRING(pct_each(2),'(f11.3)')+use
       sb_h1=  STRING(pct_each(3),'(f12.3)')+use
       sb_h2=  STRING(pct_each(4),'(f11.3)')+use
       sbuf_use= sb_tot+sb_eu+sb_c1+sb_c2+sb_h1+sb_h2

       pct_total = DOUBLE(TOTAL(s1_usage))/8000000.0 ; bits to MB
       pct_each =  DOUBLE(TOTAL(s1_usage,2))/8000000.0 ; bits to MB
         s1_tot= STRING(pct_total,'(f12.3)')  +use
         s1_eu=  STRING(pct_each(0),'(f13.3)')+use
         s1_c1=  STRING(pct_each(1),'(f13.3)')+use
         s1_c2=  STRING(pct_each(2),'(f11.3)')+use
         s1_h1=  STRING(pct_each(3),'(f12.3)')+use
         s1_h2=  STRING(pct_each(4),'(f11.3)')+use
         s1_hk=  STRING(pct_each(5),'(f11.3)')+use
         s1_gt=  STRING(pct_each(6),'(f11.3)')+use
      ssr1_use= s1_tot+s1_eu+s1_c1+s1_c2+s1_h1+s1_h2+s1_hk+s1_gt

       pct_total = DOUBLE(TOTAL(s2_usage))/8000000.0 ; bits to MB
       pct_each =  DOUBLE(TOTAL(s2_usage,2))/8000000.0 ; bits to MB
         s2_tot= STRING(pct_total,'(f12.3)')  +use
         s2_eu=  STRING(pct_each(0),'(f13.3)')+use
         s2_c1=  STRING(pct_each(1),'(f13.3)')+use
         s2_c2=  STRING(pct_each(2),'(f11.3)')+use
         s2_h1=  STRING(pct_each(3),'(f12.3)')+use
         s2_h2=  STRING(pct_each(4),'(f11.3)')+use
       ssr2_use= s2_tot+s2_eu+s2_c1+s2_c2+s2_h1+s2_h2

       pct_total = DOUBLE(TOTAL(rt_usage))/8000000.0 ; bits to MB
       pct_each =  DOUBLE(TOTAL(rt_usage,2))/8000000.0 ; bits to MB
         rt_tot= STRING(pct_total,'(f12.3)')  +use
         rt_eu=  STRING(pct_each(0),'(f13.3)')+use
         rt_c1=  STRING(pct_each(1),'(f13.3)')+use
         rt_c2=  STRING(pct_each(2),'(f11.3)')+use
         rt_h1=  STRING(pct_each(3),'(f12.3)')+use
         rt_h2=  STRING(pct_each(4),'(f11.3)')+use
         rt_hk=  STRING(pct_each(5),'(f11.3)')+use
         rt_gt=  STRING(pct_each(6),'(f11.3)')+use
       rt_use= rt_tot+rt_eu+rt_c1+rt_c2+rt_h1+rt_h2+rt_hk+rt_gt

       pct_total = DOUBLE(TOTAL(sw_usage))/8000000.0 ; bits to MB
       pct_each =  DOUBLE(TOTAL(sw_usage,2))/8000000.0 ; bits to MB
         sw_tot= STRING(pct_total,'(f12.3)')  +use
         sw_eu=  STRING(pct_each(0),'(f13.3)')+use
         sw_c1=  STRING(pct_each(1),'(f13.3)')+use
         sw_c2=  STRING(pct_each(2),'(f11.3)')+use
         sw_h1=  STRING(pct_each(3),'(f12.3)')+use
         sw_h2=  STRING(pct_each(4),'(f11.3)')+use
       sw_use= sw_tot+sw_eu+sw_c1+sw_c2+sw_h1+sw_h2

     ENDELSE
     
     WIDGET_CONTROL, schedv.sb_tot, SET_VALUE= sb_tot
     WIDGET_CONTROL, schedv.sb_eu, SET_VALUE= sb_eu
     WIDGET_CONTROL, schedv.sb_c1, SET_VALUE= sb_c1
     WIDGET_CONTROL, schedv.sb_c2, SET_VALUE= sb_c2
     WIDGET_CONTROL, schedv.sb_h1, SET_VALUE= sb_h1
     WIDGET_CONTROL, schedv.sb_h2, SET_VALUE= sb_h2
     WIDGET_CONTROL, schedv.s1_tot, SET_VALUE= s1_tot
     WIDGET_CONTROL, schedv.s1_eu, SET_VALUE= s1_eu
     WIDGET_CONTROL, schedv.s1_c1, SET_VALUE= s1_c1
     WIDGET_CONTROL, schedv.s1_c2, SET_VALUE= s1_c2
     WIDGET_CONTROL, schedv.s1_h1, SET_VALUE= s1_h1
     WIDGET_CONTROL, schedv.s1_h2, SET_VALUE= s1_h2
     WIDGET_CONTROL, schedv.s1_hk, SET_VALUE= s1_hk
     WIDGET_CONTROL, schedv.s1_gt, SET_VALUE= s1_gt
     WIDGET_CONTROL, schedv.s2_tot, SET_VALUE= s2_tot
     WIDGET_CONTROL, schedv.s2_eu, SET_VALUE= s2_eu
     WIDGET_CONTROL, schedv.s2_c1, SET_VALUE= s2_c1
     WIDGET_CONTROL, schedv.s2_c2, SET_VALUE= s2_c2
     WIDGET_CONTROL, schedv.s2_h1, SET_VALUE= s2_h1
     WIDGET_CONTROL, schedv.s2_h2, SET_VALUE= s2_h2
     WIDGET_CONTROL, schedv.rt_tot, SET_VALUE= rt_tot
     WIDGET_CONTROL, schedv.rt_eu, SET_VALUE= rt_eu
     WIDGET_CONTROL, schedv.rt_c1, SET_VALUE= rt_c1
     WIDGET_CONTROL, schedv.rt_c2, SET_VALUE= rt_c2
     WIDGET_CONTROL, schedv.rt_h1, SET_VALUE= rt_h1
     WIDGET_CONTROL, schedv.rt_h2, SET_VALUE= rt_h2
     WIDGET_CONTROL, schedv.rt_hk, SET_VALUE= rt_hk
     WIDGET_CONTROL, schedv.rt_gt, SET_VALUE= rt_gt
     WIDGET_CONTROL, schedv.sw_tot, SET_VALUE= sw_tot
     WIDGET_CONTROL, schedv.sw_eu, SET_VALUE= sw_eu
     WIDGET_CONTROL, schedv.sw_c1, SET_VALUE= sw_c1
     WIDGET_CONTROL, schedv.sw_c2, SET_VALUE= sw_c2
     WIDGET_CONTROL, schedv.sw_h1, SET_VALUE= sw_h1
     WIDGET_CONTROL, schedv.sw_h2, SET_VALUE= sw_h2





;IF (schedv.pct_stats EQ 1) THEN BEGIN 
;print,'           Telemetry Volume (MB) for The Period Of '+UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' +UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
;print,'            Total         EUVI          COR1        COR2          HI1         HI2          HK          GT'
;print,'SBUF'+sbuf_use
;print,'SSR1'+ssr1_use
;print,'SSR2'+ssr2_use
;print,'RTch'+rt_use
;print,'SWch'+sw_use
;stop
;ENDIF

  RETURN
END
