;+
;$Id: schedule_convert_units.pro,v 1.7 2009/09/11 20:28:22 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SCHEDULE_CONVERT_UNITS()
;               
; Purpose     : This function converts an image size from one unit to another.
;               Units can be any of bits, bytes, % Secchi-Buffer, % SSR1-Buffer,
;               % SSR2-Buffer, % RT-Buffer, % SW-Buffer.
;               
; Use         : new = SCHEDULE_CONVERT_UNITS(value, from, to) 
;    
; Inputs      :  value    Size to be converted.
;                from     Convert from.
;                to       Convert to.
;
; Opt. Inputs : None
;               
; Outputs     : new       Converted unit. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari  08/22/06 - New capacity and rate info.
;              Ed Esfandiari  08/30/06 - use rtrate from common block.
;
; $Log: schedule_convert_units.pro,v $
; Revision 1.7  2009/09/11 20:28:22  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:09  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


;__________________________________________________________________________________________________________
;

FUNCTION SCHEDULE_CONVERT_UNITS, value, from, to
COMMON RT_CHANNEL, rt_rates, rtrate

   ;** 0:bits, 1:bytes, 2:%buffer

   ;RESTORE,'secchi_downlink_channels.sav' ; AEE 10/7/03
   ;filename=GETENV('PT')+'/IN/OTHER/'+'secchi_downlink_channels.sav'
   ;RESTORE, filename 
           ;=> CHANNELS.
              ;SB_CAPACITY     FLOAT           32.0        ; Mega Bytes  (Secchi-Buffer RAM disk)
              ;SB_RATE         FLOAT           1000000.0   ; bits/sec   ???
              ;SSR1_CAPACITY   FLOAT           645.000     ; Mega Bytes
              ;SSR1_RATE       FLOAT           42000.0     ; bits/sec
              ;SSR2_CAPACITY   FLOAT           161.000     ; Mega Bytes
              ;SSR2_RATE       FLOAT           42000.0     ; bits/sec
              ;RT_CAPACITY     FLOAT           250.000     ; Mega Bytes (page 29)
              ;RT_RATE         FLOAT           12000.0     ; bits/sec  (page32 MissionOps. of GS CDR PR)
              ;SW_CAPACITY     FLOAT           12.5000     ; Mega Bytes (page 29)
              ;SW_RATE         FLOAT           500.000     ; bits/sec  (page32 MissionOps. of GS CDR PR

     channels= READ_SSR_INFO()

        ; AEE 4/8/04: RT-channel can only hold 1 packet (272 bytes) at a time:

       ; RESTORE,GETENV('PT')+'/IN/OTHER/'+'rt_channel.sav'
         ; =>
           ; rt_rate= 50        Note: this value, (packets/sec), can change daily and should be read in
                        ;       from a file.
                        ;       This is also true for schedule_plot.pro and other routines that may
                        ;       that may use secchi_downlink_channels.sav file.
           ; rt_capacity= 1     1 packet


;   AEE 4/8/04:
;   Note: rt_rate at this point is the number of packets/second (not bits/sec) which should be changed
;         to bits/sec. Also rt_capacity on input is number of packets that RT-channel can hold at a time
;         (normally 1-packet). But since the resolution of display plot is in seconds, I use
;         the rt_rate (i.e. 50 packets/sec) as the rt_capacity instead.
;  channels.

       ; rtrate    = rt_rate*272.0*8.0         ; bits/sec
       ; rtcapacity= (rt_rate*272.0)/1000000.0 ; MB/sec 

       rtcapacity= rtrate/8000000.0 ; MB/sec


   ; from/to of 0 = bits
   ;            1 = bytes
   ;            2 = % Secchi Buffer (RAM disk)
   ;            3 = % SSR1 Channel
   ;            4 = % SSR2 Channel
   ;            5 = % RT   Channel
   ;            6 = % SW   Channel


   ;bufsize = channels.sb_capacity * 1000000.0 / 2.0  ; change from MBytes to Words

   CASE (1) OF 

      ((from EQ 0) AND (to EQ 1)) : BEGIN	;** bits to bytes
            new = value / 8.0D
         END
      ((from EQ 0) AND (to EQ 2)) : BEGIN	;** bits to % buf
            ;new = (value / 8.0D / 2.0D) / bufsize * 100D
            new = (value / 8.0D) / (channels.sb_capacity * 1000000.0D) * 100D
         END
      ((from EQ 0) AND (to EQ 3)) : BEGIN       ;** bits to % SSR1
            new = (value / 8.0D) / (channels.ssr1_capacity * 1000000.0D) * 100D
         END
      ((from EQ 0) AND (to EQ 4)) : BEGIN       ;** bits to % SSR2
            new = (value / 8.0D) / (channels.ssr2_capacity * 1000000.0D) * 100D
         END
      ((from EQ 0) AND (to EQ 5)) : BEGIN       ;** bits to % RT
            ;new = (value / 8.0D) / (channels.rt_capacity * 1000000.0) * 100D
            new = (value / 8.0D) / (rtcapacity * 1000000.0) * 100D
         END
      ((from EQ 0) AND (to EQ 6)) : BEGIN       ;** bits to % SW
            new = (value / 8.0D) / (channels.sw_capacity * 1000000.0D) * 100D
         END

      ((from EQ 1) AND (to EQ 0)) : BEGIN	;** bytes to bits
            new = value * 8.0D
         END
      ((from EQ 1) AND (to EQ 2)) : BEGIN	;** bytes to % buf
            new = (value) / (channels.sb_capacity * 1000000.0D ) * 100D
         END
      ((from EQ 1) AND (to EQ 3)) : BEGIN       ;** bytes to % SSR1
            new = (value) / (channels.ssr1_capacity * 1000000.0D) * 100D
         END
      ((from EQ 1) AND (to EQ 4)) : BEGIN       ;** bytes to % SSR2
            new = (value) / (channels.ssr2_capacity * 1000000.0D) * 100D
         END
      ((from EQ 1) AND (to EQ 5)) : BEGIN       ;** bytes to % RT
            ;new = (value) / (channels.rt_capacity * 1000000.0D) * 100D
            new = (value) / (rtcapacity * 1000000.0D) * 100D
         END
      ((from EQ 1) AND (to EQ 6)) : BEGIN       ;** bytes to % SW
            new = (value) / (channels.sw_capacity * 1000000.0D) * 100D
         END

      ((from EQ 2) AND (to EQ 0)) : BEGIN	;** % buf to bits
            new = (value / 100D * channels.sb_capacity) * 1000000.0D * 8.0D
         END
      ((from EQ 2) AND (to EQ 1)) : BEGIN	;** % buf to bytes
            new = (value / 100D * channels.sb_capacity) * 1000000.0D
         END
      ((from EQ 2) AND (to EQ 3)) : BEGIN       ;** % buf to % SSR1
            new = (value / 100D * channels.sb_capacity)
            new = (new) / (channels.ssr1_capacity) * 100D
         END
      ((from EQ 2) AND (to EQ 4)) : BEGIN       ;** % buf to % SSR2
            new = (value / 100D * channels.sb_capacity)
            new = (new) / (channels.ssr2_capacity) * 100D
         END
      ((from EQ 2) AND (to EQ 5)) : BEGIN       ;** % buf to % RT
            new = (value / 100D * channels.sb_capacity)
            ;new = (new) / (channels.rt_capacity) * 100D
            new = (new) / (rtcapacity) * 100D
         END
      ((from EQ 2) AND (to EQ 6)) : BEGIN       ;** % buf to % SW
            new = (value / 100D * channels.sb_capacity)
            new = (new) / (channels.sw_capacity) * 100D
         END

      ((from EQ 3) AND (to EQ 0)) : BEGIN       ;** % SSR1 to bits
            new = (value / 100D * channels.ssr1_capacity) * 1000000.0D * 8.0D
         END
      ((from EQ 3) AND (to EQ 1)) : BEGIN       ;** % SSR1 to bytes
            new = (value / 100D * channels.ssr1_capacity) * 1000000.0D
         END
      ((from EQ 3) AND (to EQ 2)) : BEGIN       ;** % SSR1 to % buf 
            new = (value / 100D * channels.ssr1_capacity)
            new = (new) / (channels.sb_capacity) * 100D
         END
      ((from EQ 3) AND (to EQ 4)) : BEGIN       ;** % SSR1 to % SSR2
            new = (value / 100D * channels.ssr1_capacity)
            new = (new) / (channels.ssr2_capacity) * 100D
         END
      ((from EQ 3) AND (to EQ 5)) : BEGIN       ;** % SSR1 to % RT
            new = (value / 100D * channels.ssr1_capacity)
            ;new = (new) / (channels.rt_capacity) * 100D
            new = (new) / (rtcapacity) * 100D
         END
      ((from EQ 3) AND (to EQ 6)) : BEGIN       ;** % SSR1 to % SW
            new = (value / 100D * channels.ssr1_capacity)
            new = (new) / (channels.sw_capacity) * 100D
         END

      ((from EQ 4) AND (to EQ 0)) : BEGIN       ;** % SSR2 to bits
            new = (value / 100D * channels.ssr2_capacity) * 1000000.0D * 8.0D
         END
      ((from EQ 4) AND (to EQ 1)) : BEGIN       ;** % SSR2 to bytes
            new = (value / 100D * channels.ssr2_capacity) * 1000000.0D
         END
      ((from EQ 4) AND (to EQ 2)) : BEGIN       ;** % SSR2 to % buf 
            new = (value / 100D * channels.ssr2_capacity)
            new = (new) / (channels.sb_capacity) * 100D
         END
      ((from EQ 4) AND (to EQ 3)) : BEGIN       ;** % SSR2 to % SSR1
            new = (value / 100D * channels.ssr2_capacity)
            new = (new) / (channels.ssr1_capacity) * 100D
         END
      ((from EQ 4) AND (to EQ 5)) : BEGIN       ;** % SSR2 to % RT
            new = (value / 100D * channels.ssr2_capacity)
            ;new = (new) / (channels.rt_capacity) * 100D
            new = (new) / (rtcapacity) * 100D
         END
      ((from EQ 4) AND (to EQ 6)) : BEGIN       ;** % SSR2 to % SW
            new = (value / 100D * channels.ssr2_capacity)
            new = (new) / (channels.sw_capacity) * 100D
         END

      ((from EQ 5) AND (to EQ 0)) : BEGIN       ;** % RT to bits
            ;new = (value / 100D * channels.rt_capacity) * 1000000.0D * 8.0D
            new = (value / 100D * rtcapacity) * 1000000.0D * 8.0D
         END
      ((from EQ 5) AND (to EQ 1)) : BEGIN       ;** % RT to bytes
            ;new = (value / 100D * channels.rt_capacity) * 1000000.0D
            new = (value / 100D * rtcapacity) * 1000000.0D
         END
      ((from EQ 5) AND (to EQ 2)) : BEGIN       ;** % RT to % buf 
            ;new = (value / 100D * channels.rt_capacity)
            new = (value / 100D * rtcapacity)
            new = (new) / (channels.sb_capacity) * 100D
         END
      ((from EQ 5) AND (to EQ 3)) : BEGIN       ;** % RT to % SSR1
            ;new = (value / 100D * channels.rt_capacity)
            new = (value / 100D * rtcapacity)
            new = (new) / (channels.ssr1_capacity) * 100D
         END
      ((from EQ 5) AND (to EQ 4)) : BEGIN       ;** % RT to % SSR2 
            ;new = (value / 100D * channels.rt_capacity)
            new = (value / 100D * rtcapacity)
            new = (new) / (channels.ssr2_capacity) * 100D
         END
      ((from EQ 5) AND (to EQ 6)) : BEGIN       ;** % RT to % SW
            ;new = (value / 100D * channels.rt_capacity)
            new = (value / 100D * rtcapacity)
            new = (new) / (channels.sw_capacity) * 100D
         END

      ((from EQ 6) AND (to EQ 0)) : BEGIN       ;** % SW to bits
            new = (value / 100D * channels.sw_capacity) * 1000000.0D * 8.0D
         END
      ((from EQ 6) AND (to EQ 1)) : BEGIN       ;** % SW to bytes
            new = (value / 100D * channels.sw_capacity) * 1000000.0D
         END
      ((from EQ 6) AND (to EQ 2)) : BEGIN       ;** % SW to % buf 
            new = (value / 100D * channels.sw_capacity)
            new = (new) / (channels.sb_capacity) * 100D
         END
      ((from EQ 6) AND (to EQ 3)) : BEGIN       ;** % SW to % SSR1
            new = (value / 100D * channels.sw_capacity)
            new = (new) / (channels.ssr1_capacity) * 100D
         END
      ((from EQ 6) AND (to EQ 4)) : BEGIN       ;** % SW to % SSR2 
            new = (value / 100D * channels.sw_capacity)
            new = (new) / (channels.ssr2_capacity) * 100D
         END
      ((from EQ 6) AND (to EQ 5)) : BEGIN       ;** % SW to % RT 
            new = (value / 100D * channels.sw_capacity)
            ;new = (new) / (channels.rt_capacity) * 100D
            new = (new) / (rtcapacity) * 100D
         END

      ELSE : new = value			;** no change of units
   ENDCASE

   RETURN, new
END
