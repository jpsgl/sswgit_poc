;+
;$Id: shift_doors.pro,v 1.1.1.2 2004/07/01 21:19:11 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SHIFT_DOORS
;               
; Purpose     : This pro shift all of the door open and closes by delta time.
;               
; Use         : SHIFT_DOORS, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1,
;                            cdoor_cor2, odoor_cor2, delta
;    
; Inputs      : cdoor_euvi  Double  EUVI door closures (TAI).
;               odoor_euvi  Double  EUVI door opens (TAI).
;               cdoor_cor1  Double  COR1 door closures (TAI).
;               odoor_cor1  Double  COR1 door opens (TAI).
;               cdoor_cor2  Double  COR2 door closures (TAI).
;               odoor_cor2  Double  COR2 door opens (TAI).
;               delta       Double  Time by which to shift the door commands (TAI).
;
; Opt. Inputs : None
;               
; Outputs     : All door closure/open commands will be shifted on output. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: shift_doors.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO SHIFT_DOORS, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2, delta

  ; Shift all of the door open and closes that are not '3000/01/01' and
  ; '2000/01/01' by delta:

  cdte= UTC2TAI(STR2UTC('2000/01/01'))
  odte= UTC2TAI(STR2UTC('3000/01/01'))

  IF (DATATYPE(cdoor_euvi) EQ 'DOU') THEN BEGIN
    okdates= WHERE(cdoor_euvi GT cdte, cnt)
    IF (cnt GT 0) THEN cdoor_euvi(okdates) = cdoor_euvi(okdates) + delta
  ENDIF
  IF (DATATYPE(odoor_euvi) EQ 'DOU') THEN BEGIN
    okdates= WHERE(odoor_euvi LT odte, cnt)
    IF (cnt GT 0) THEN odoor_euvi(okdates) = odoor_euvi(okdates) + delta
  ENDIF

  IF (DATATYPE(cdoor_cor1) EQ 'DOU') THEN BEGIN
    okdates= WHERE(cdoor_cor1 GT cdte, cnt)
    IF (cnt GT 0) THEN cdoor_cor1(okdates) = cdoor_cor1(okdates) + delta
  ENDIF
  IF (DATATYPE(odoor_cor1) EQ 'DOU') THEN BEGIN
    okdates= WHERE(odoor_cor1 LT odte, cnt)
    IF (cnt GT 0) THEN odoor_cor1(okdates) = odoor_cor1(okdates) + delta
  ENDIF

  IF (DATATYPE(cdoor_cor2) EQ 'DOU') THEN BEGIN
    okdates= WHERE(cdoor_cor2 GT cdte, cnt)
    IF (cnt GT 0) THEN cdoor_cor2(okdates) = cdoor_cor2(okdates) + delta
  ENDIF
  IF (DATATYPE(odoor_cor2) EQ 'DOU') THEN BEGIN
    okdates= WHERE(odoor_cor2 LT odte, cnt)
    IF (cnt GT 0) THEN odoor_cor2(okdates) = odoor_cor2(okdates) + delta
  ENDIF


  RETURN
END
