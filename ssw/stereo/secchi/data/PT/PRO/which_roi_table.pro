;+
;$Id: which_roi_table.pro,v 1.4 2009/09/11 20:28:25 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : WHICH_ROI_TABLE
;               
; Purpose     : This function checks image processing steps used to see which
;               of 4 ROI tables is used and returns it. If more than one ROI
;               table use is present, by mistake, the last one listed is used
;               and a warning message indicating that is generated.
;               
; Use         : roi_tab = WHICH_ROI_TABLE(ip,iptable) 
;    
; Inputs      : ip        Image Processing structure. 
;               iptable   Image Processing table used.
;
; Opt. Inputs : None
;               
; Outputs     : roi_tab   Function returns:
;                         0 to indicate step #25 = "Use ROI1 table"
;                         1 to indicate step #45 = "Use ROI2 table"
;                         2 to indicate step #46 = "Use ROI3 table"
;                         3 to indicate step #47 = "Use ROI4 table"
;                        -1 to indicate NO ROI table.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : None 
;
; Written by  : Ed Esfandiari, NRL, July 2004 - First Version. 
;               
; Modification History:
;
; $Log: which_roi_table.pro,v $
; Revision 1.4  2009/09/11 20:28:25  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.1  2004/09/01 15:40:48  esfand
; commit new version for Nathan.
;
;
;-

FUNCTION WHICH_ROI_TABLE, ip, iptable

   ; See which of 4 ROI tables, if any, is used:

   ind = WHERE(ip(iptable).steps EQ 25 OR $   ; step #25 = "Use ROI1 table"
               ip(iptable).steps EQ 45 OR $   ; step #45 = "Use ROI2 table"
               ip(iptable).steps EQ 46 OR $   ; step #46 = "Use ROI3 table"
               ip(iptable).steps EQ 47, icnt) ; step #47 = "Use ROI4 table"

   IF (icnt EQ 0) THEN BEGIN
     roi_tab= -1
   ENDIF ELSE BEGIN
     ind= ind(icnt-1)
     IF (icnt GT 1) THEN $
       PRINT,'WARNING - Multiple "Use ROI.." functions selected. Using last (function #'+strtrim(ip(iptable).steps(ind),2)+').'
     IF (ip(iptable).steps(ind) EQ 25) THEN roi_tab= 0
     IF (ip(iptable).steps(ind) EQ 45) THEN roi_tab= 1
     IF (ip(iptable).steps(ind) EQ 46) THEN roi_tab= 2
     IF (ip(iptable).steps(ind) EQ 47) THEN roi_tab= 3
   ENDELSE

   RETURN, roi_tab

END
