;+
;$Id: schedule.pro,v 1.15 2013/01/09 13:33:23 mcnutt Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SCHEDULE
;               
; Purpose     : Widget tool to schedule a SECCHI Observing Sequence.
;               
; Explanation : This is the top level tool for defining the SECCHI 
;		observing schedule.  From this tool you can define and
;		schedule OP's and OS's.  A graphical representation of
;		the schedule is displayed including the expected
;		LEB telemetry buffer usage.
;               
; Use         : SCHEDULE, ops
;    
; Inputs      : None
;
; Opt. Inputs : ops - A *.IPT (*.InstrumentPlanningTool) schedule.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written By  : Ed Esfandiari, NRL, May 2004 - First Version.
;               
; Modification History:
;              Ed Esfandiari 06/07/04 - Label changes.
;              Ed Esfandiari 06/16/04 - Added setup and exp. times to the display.
;              Ed Esfandiari 06/17/04 - Added Image size and time formats.
;              Ed Esfandiari 09/22/04 - Added Select SpaceCraft icon.
;              Ed Esfandiari 09/30/04 - Added SpaceCraft A and B scheduling capabilities.
;              Ed Esfandiari 10/15/04 - Added SC A/B blockseqeunce counters.
;              Ed Esfandiari 10/25/04 - Re-arranged Tlm load section.
;              Ed Esfandiari 11/02/04 - Added "Display Mode" options.
;                            11/05/04 - Added code to generate BSF files. 
;              Ed Esfandiari 11/30/04 - Added device, decomposed=0 statement.
;              Ed Esfandiari 02/02/05 - Added Add/Remove Initialize timeline icon.
;              Ed Esfandiari 03/10/05 - Added FSW schedule reading time delay message.
;              Ed Esfandiari 05/03/05 - Added schedule run script (A) command.
;              Ed Esfandiari 05/11/05 - Allow multiple I and A commands.
;              Ed Esfandiari 05/26/05 - Added code to allow actual or a 24hr playback.
;              Ed Esfandiari 04/20/06 - Added SSR2 percentage playback.
;              Ed Esfandiari 05/19/06 - Added S/C A and B starting perecntage and SSR2 playback independency.
;              Ed Esfandiari 06/22/06 - Added data_rate selection.
;              Ed Esfandiari 09/01/06 - Added rate_gt to GT_DUMPS_SHARE.
;              Ed Esfandiari 09/02/06 - Added nominal 3.26 kbps HK rate (secchi flight software, etc. - NOT Spacecraft) 
;                                       to SSR1 and RT.
;              Ed Esfandiari 11/27/06 - Added Phasing Orbit rates.
;              Ed Esfandiari 12/28/06 - Added bsf_gt to common block.
;              Ed Esfandiari 03/13/07 - Added 1 and 2 min schedule span.
;              Ed Esfandiari 06/20/07 - Added toggle option to display data volume/downloadable ratio or volume in MB.
;              Ed Esfandiari 07/08/08 - Added .reset_session option (for idl 5.3+) to remove_common_block section.
;              Ed Esfandiari 09/02/09 - changed data volume display from .1 to .3 decimal places.
;              Ed Esfandiari 03/06/10 - Changed default data rate (drate) from 720 to 240 kbps.
;              Ed Esfandiari 03/06/10 - Changed drate seletion automatic according to the today's date.
;
;
; $Log: schedule.pro,v $
; Revision 1.15  2013/01/09 13:33:23  mcnutt
; changed made when generating new block sched 20130108
;
; Revision 1.14  2013/01/07 19:37:07  nathan
;  have one button to save all files
;
; Revision 1.13  2011/06/30 20:38:44  nathan
; bunch of widget adjustments
;
; Revision 1.12  2011/06/15 22:55:20  nathan
; Allow input of OSID; note that changed num_text from string with prefix to
; just a 4-digit number. Possibly output is affected.
; Also changed default state to ST-A instead of both, and automatically
; computes tlm volumes.
;
; Revision 1.11  2010/06/25 18:15:36  esfand
; automatic telemetry rate selection
;
; Revision 1.10  2009/09/11 20:28:22  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.6  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.5  2005/04/27 20:38:39  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.4  2005/03/10 16:52:17  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:09  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;               
;-            

;__________________________________________________________________________________________________________
;
PRO SCHEDULE, ops


COMMON SCHEDULE_BASE, sched_base
COMMON DIALOG, mdiag,font
COMMON CMD_SEQ_SCHEDULED, sched_cmdseq   ; AEE - 5/20/03 
COMMON SCIP_DOORS_SHARE, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
                         sc_maneuvers, start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2 
;COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt ; AEE - 7/24/03
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt ; AEE - 09/01/06 
COMMON SCHED_SHARE, schedv
COMMON SCHED_CONF, all_conflicts ; AEE - 10/21/03
COMMON SCA_NAMES, sca_fname, scb_fname
COMMON KAP_INPUT, kap_nrt_reserved_arr, kap_resource_arr, kap_iie_arr
COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt
COMMON INIT_TL, ic
COMMON RUN_SCRIPT, rsc
COMMON TM_DATA, sb_usage, sb_total_bits_avail, s1_usage, s1_total_bits_avail, $
                s2_usage, s2_total_bits_avail, rt_usage, rt_total_bits_avail, rt_usage_avail, $
                sw_usage, sw_total_bits_avail, sbuf_use, ssr1_use, ssr2_use, rt_use, sw_use

device, decomposed=0 ; to use 8 bits for color (for 24 bit systems) - IDL limitation.

s1_usage= FLTARR(7,2) & s1_total_bits_avail= 0 & s2_usage= FLTARR(5,2) & s2_total_bits_avail= 0.0
rt_usage= FLTARR(7,2) & rt_total_bits_avail= 0 & sw_usage= FLTARR(5,2) & sw_total_bits_avail= 0.0
sb_usage= FLTARR(5,2) & sb_total_bits_avail= 0 & rt_usage_avail= FLTARR(7,2) 
sbuf_use='' & ssr1_use='' & ssr2_use='' & rt_use='' & sw_use='' 

a_bsf_cnt= 0
b_bsf_cnt= 0

; set bsf=-1 for first init command structure to indicate it is a dummy one
; and should not be used. Vaild bsf values are:
;  0: stand alone init command
;  1,2,3,..: init command belongs to a .bsf file read 

ic= {sc:'-',dt:0.0D,bsf:-1}
rsc= {sc:'-',dt:0.0D,fn:'',bsf:-1}

all_conflicts= ''

 ssr_rt_mode= READ_SSR_RT_MODE('SSR_realtime.dat')  ; AEE - 6/13/03
 IF(ssr_rt_mode EQ '') THEN BEGIN
   PRINT
   PRINT,'==========================================================='
   PRINT,'ERROR: SSR_realtime.dat file does not indicate which one of'
   PRINT,'            BOTH, SPACEWEATHER, REALTIME, or NONE'
   PRINT,'       modes to be used. Edit the file and try again.'
   PRINT,'==========================================================='
   PRINT
   RETURN
 END
  
 IF ((SIZE(sched_base))(1) NE 0) THEN BEGIN ; defined, remove common blocks before runing this routine.

    PRINT,''
    PRINT,'*********************************'
    PRINT,'Old Common Block Data Is Detected. 
    PRINT,'Must issue a'
    PRINT,'   @remove_common_blocks'
    PRINT,' Or starting with IDL v5.3'
    PRINT,'   .reset_session'
    PRINT,'command, first.'
    PRINT,'*********************************'
    PRINT,''
    RETURN

 END

 ;
 ; The variable STARTDIS is used throughout the code.  It is the first time point 
 ; displayed in the draw window and is in TAI form.  Likewise, ENDDIS
 ; is the last displayed time point.
 ;

 read_ipt_flag = ''
 IF N_ELEMENTS(ops) EQ 0 THEN BEGIN	; no time entered, use default
      GET_UTC, stime			; get system time
      stime.mjd = stime.mjd + 1		; add a day
      stime.time = 0			; only want date
      startdis = UTC2TAI(stime)		; seconds since 1 jan 1958 (TAI)
 ENDIF ELSE BEGIN
   pos = STRPOS(ops, '.IPT')
   IF (pos GT 0) THEN BEGIN	;** read in input IPT
    ; AEE - 6/11/03 - Make sure .IPT file exist before trying to open it.
    IF (STRPOS(ops,'/IO/IPT/') LT 0) THEN $ 
      fipt= FINDFILE(GETENV('PT')+'/IO/IPT/'+ops) $
    ELSE $
      fipt= FINDFILE(ops)
    IF (STRLEN(fipt(0)) EQ 0) THEN BEGIN
      PRINT & PRINT
      PRINT,'Sorry, '+ops+' file does not exist.'
      PRINT
      RETURN
    ENDIF
    read_ipt_flag = ops
    SCHEDULE_READ_IPT_FST, fipt(0), /SILENT, startdis=startdis, /PROMPT_SHIFT
   ENDIF ELSE $
      startdis = UTC2TAI(ops)     ; Hopefully, the user entered a valid time string so get in TAI 
 ENDELSE

 baseop = startdis 			; Remember input operational day (TAI),
				        ;    STARTDIS will be changing.

 duration = 86400d   			; Default display span in seconds
 enddis   = startdis + duration 	; The last displayed time point (TAI)

 ;
 ; Initialize some VALUEs
 ;


 study_start = startdis		; STUDY_START: VALUE to go in start text widget
 study_stop  = enddis		; STUDY_STOP : VALUE to go in stop text widget

 mode = 0 			; Initialize to display mode
				;    mode = 0 is display mode
				;    mode = 1 is edit mode

 ;
 ;
 ; Create the widget interface
 ;
 ;

 DEVICE, GET_SCREEN_SIZE = scr

 ; Create base and divide as columns. Use 1st column for buttons + draw window (cols) and 2nd column for (mdiag):
 ; ----------------------------------
 ; |           1st (cols)           |               
 ; ----------------------------------
 ; |           2nd (mdiag)          |
 ; ---------------------------------

wsizes=define_widget_sizes()
yscroffsets=wsizes.dsktop+wsizes.winhdr+wsizes.border+wsizes.scrbar
xscroffsets=wsizes.scrbar
 base = WIDGET_BASE(TITLE='SECCHI Scheduling Tool   /   ' + $
        'Operational Day ' + TAI2UTC(baseop, /ECS), $
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-95,Y_SCROLL_SIZE=scr(1)-80)
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-60,Y_SCROLL_SIZE=scr(1)-70)
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-165,Y_SCROLL_SIZE=scr(1)-130)
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-165,Y_SCROLL_SIZE=scr(1)-80) ;mahanadi
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-120,Y_SCROLL_SIZE=scr(1)-80) ;solar6
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-300,Y_SCROLL_SIZE=scr(1)-100)
        ;SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-390,Y_SCROLL_SIZE=scr(1)-100) ;secchig
        SPACE=5, XPAD=3, YPAD=-5, /COL,/SCROLL,X_SCROLL_SIZE=scr(0)-xscroffsets,Y_SCROLL_SIZE=scr(1)-yscroffsets-200) ; lasco7


  ; 
  ; The COLUMN of button, text, and list widgets to the left of the DRAW window
  ;

; Take first column of base and divide into rows (call it cols) one for leftside buttons (A) and one for drawing plan (B):
; ----------------------------------
; |      A        |       B        |               
; ----------------------------------
; |            mdiag               |
; ---------------------------------

  cols = WIDGET_BASE(base, SPACE=10,/ROW)
  col1 = WIDGET_BASE(cols, SPACE=10,/COLUMN)

  ; 
  ; The row of buttons at the top of the left side COLUMN
  ;

  controls =  WIDGET_BASE(col1, /ROW)
     ;quit = WIDGET_BUTTON(controls, VALUE='QUIT', UVALUE='QUIT')
     help = WIDGET_BUTTON(controls, VALUE='Help', UVALUE='HELP')
     lab = WIDGET_LABEL(controls, VALUE='')
     gen  = WIDGET_BUTTON(controls, VALUE='Verify Schedule', UVALUE='VERIFY')

     ; 
     ; To add more time spans, add the VALUEs below and provide an appropiate
     ; case statement in the event handler (PLAN_EV)
     ;

;span_list= ['5 min span',           $
span_list= ['1 min span',           $
            '2 min span',           $
            '5 min span',           $
            '10 min span',          $
            '30 min span',          $
            '1 hour span',          $
            '2 hour span',          $
            '3 hour span',          $
            '4 hour span',          $
            '6 hour span',          $
            '9 hour span',          $
            '12 hour span',         $
            '18 hour span',         $
            '1 day span',           $
            '1.25 day span',        $
            '1.5 day span',         $
            '2 day span',           $
            '3 day span',           $
            '7 day span']
 
drop_span = widget_droplist(controls, uvalue='TIMESPAN', value= span_list)

     ; NOTE: SPAN_SEC must coorrespond to text in DROP_SPAN in second

     ;span_sec = [ [300d, 600d, 1800d, 3600d, 7200d], $           ; minutes
     span_sec = [ [60d,120d,300d, 600d, 1800d, 3600d, 7200d], $   ; minutes
                     [.125d, (4d/24d), .25d, .375d, .5d, .75d,  $ ; hours
                        1d, 1.25d, 1.5d, 2d, 3d, 7d] * 86400d]   ; days

  size_fmt  = '(d18.3)'
  time_fmt1 = '(d12.1)'
  time_fmt2 = '(d8.3)'

  ; 
  ; Below the control button row, these are buttons to call outside programs.
  ;

  refer = WIDGET_BASE(col1, SPACE=2, /FRAME, /COLUMN, YPAD=-1)
   prog = WIDGET_BASE(refer, SPACE=5, /COLUMN, XPAD=10, YPAD=5)
;   prog = WIDGET_BASE(refer, SPACE=2, /COLUMN, XPAD=-1, YPAD=-1)
     sched_set = WIDGET_BUTTON(prog, VALUE='Plan & Describe An Observing Set', EVENT_PRO='PLAN_SET')
     def_os = WIDGET_BUTTON(prog, VALUE='Define Observing Sequence (OS)', UVALUE='DEFINE_OS')
     ;sched_bs = WIDGET_BUTTON(prog, VALUE='Schedule Block Sequence (BS)', UVALUE='SCHEDULE_BS')
     sched_bs = WIDGET_BUTTON(prog, VALUE='Load BSF', UVALUE='SCHEDULE_BS')
     sdoor = WIDGET_BUTTON(prog, VALUE='Schedule SCIP Door Commands',UVALUE='SCHEDULE_DOORS')
     sgt = WIDGET_BUTTON(prog, VALUE='Schedule GT Dumps',UVALUE='GT_DUMPS')
     sca = WIDGET_BUTTON(prog, VALUE='          Read SCA File          ', UVALUE='INPUT_KAP') ;AEE 4/6/04 
     ript = WIDGET_BUTTON(prog, VALUE="          Read IPT File          ", UVALUE='INPUT_IPT')

  refer = WIDGET_BASE(col1, SPACE=2, /FRAME, /COLUMN, YPAD=-1)
   prog = WIDGET_BASE(refer, SPACE=5, /COLUMN, XPAD=10, YPAD=5)
   ;prog = WIDGET_BASE(refer, SPACE=2, /COLUMN, XPAD=-1, YPAD=-1)
     store = WIDGET_BUTTON(prog, VALUE='     Store Current Schedule      ', UVALUE='STORE')
     ;gipt = WIDGET_BUTTON(prog, VALUE='        Generate IPT File        ', UVALUE='OUTPUT_IPT') ; AEE 1/15/04
     ;conf = WIDGET_BUTTON(prog, VALUE='     Generate Conflict File      ', UVALUE='conf_out')   ; AEE 11/17/03
     ;csf= WIDGET_BUTTON(prog, VALUE='Generate BSF File', UVALUE='OUT_BSF') 
     gall = WIDGET_BUTTON(prog, VALUE='Save as Files', UVALUE='OUTPUT_PLAN') ; ; AEE 11/17/03
     ;sca = WIDGET_BUTTON(prog, VALUE='          Read SCA File          ', UVALUE='INPUT_KAP') ;AEE 4/6/04 
     ;ript = WIDGET_BUTTON(prog, VALUE="          Read IPT File          ", UVALUE='INPUT_IPT')

  intime= WIDGET_BASE(col1, SPACE=1, /COLUMN, /FRAME, XPAD=-1, YPAD=-1)
  ;intl= WIDGET_BUTTON(intime, VALUE="           Initialize Timeline            ", UVALUE='INIT_TL') 
  intl= WIDGET_BUTTON(intime, VALUE="      Add/Remove ISTIMELINE INIT      ", UVALUE='INIT_TL') 

  rstime= WIDGET_BASE(col1, SPACE=1, /COLUMN, /FRAME, XPAD=-1, YPAD=-1)
  sched_rs = WIDGET_BUTTON(rstime, VALUE='      Schedule/Remove Run Script File     ', UVALUE='SCHEDULE_RS')

  ;
  ; Below the outside call buttons, there are 2 text window for entering
  ; start and stop times and a list widget that displays study names. 
  ;

  edit = WIDGET_BASE(col1, SPACE=2, /COLUMN, /FRAME, XPAD=-1, YPAD=-1)

     ; In order to line up text widgets, pair the label in a COLUMN
     ; and the text windows in a COLUMN.

     font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

     row = WIDGET_BASE(edit, /ROW,XPAD=-1, YPAD=-1)
       get_os_op = CW_PDMENU(row, [ '1\Select OS', '0\From Currently Loaded'], UVALUE="GET_OS_OP")
       num_text =  CW_FIELD(row, VALUE='0000', title='  OBS_ID:', FONT=font, uvalue='GET_OS_OP', /return_events, xsize=4)
       ;lab = WIDGET_LABEL(row, VALUE='        ')
       ;help = WIDGET_BUTTON(row, VALUE='Help', UVALUE='HELP')

     times_input = WIDGET_BASE(edit, /ROW)
      ;lab_times = WIDGET_BASE(times_input, /COLUMN, SPACE=12,XPAD=-1, YPAD=-1)
      ;lab_times = WIDGET_BASE(times_input, /COLUMN, SPACE=10,XPAD=-1, YPAD=-1)
       ;lab_times = WIDGET_BASE(times_input, SPACE=12, YPAD=8, /COLUMN)
       ;lab_times = WIDGET_BASE(times_input, SPACE=12, YPAD=8, /COLUMN)
       lab_times = WIDGET_BASE(times_input, SPACE=18, YPAD=8, /COLUMN)
       lab = WIDGET_LABEL(lab_times, VALUE='START')
       lab = WIDGET_LABEL(lab_times, VALUE='Count')
;       lab = WIDGET_LABEL(lab_times, VALUE='      ')
;       lab = WIDGET_LABEL(lab_times, VALUE='DELTA')
       lab = WIDGET_LABEL(lab_times, VALUE='Delta (hh:mm:ss)')
       lab = WIDGET_LABEL(lab_times, VALUE='STOP')

      tex_times = WIDGET_BASE(times_input,/COLUMN,XPAD=-1, YPAD=-1)
       ;start_text = WIDGET_TEXT(tex_times, XSIZE=22, /EDITABLE, $
       start_text = WIDGET_TEXT(tex_times, XSIZE=19, /EDITABLE, $
				 UVALUE='TEXTSTART')
       count_text = WIDGET_TEXT(tex_times, XSIZE=5, /EDITABLE, UVALUE='COUNT')

       ;delta format is: hh:mm:ss.ms
       ;AEE - May 2002: allowed +hh:mm:ss.ms (forward and same as hh:mm:ss.ms) and
       ;                        -hh:mm:ss.ms (backward) changes in time.
;       lab= WIDGET_LABEL(tex_times, VALUE='delta (hh:mm:ss)')

       delta_text = WIDGET_TEXT(tex_times, XSIZE=11, /EDITABLE, UVALUE='DELTA')
;      trow = WIDGET_BASE(times_input,/COLUMN)
;       lab= WIDGET_LABEL(trow, VALUE='')
;       lab= WIDGET_LABEL(trow, VALUE='')
;       lab = WIDGET_LABEL(trow, VALUE='hh:mm:ss')
       ;stop_text = WIDGET_TEXT(tex_times, XSIZE=22, /EDITABLE, $
       stop_text = WIDGET_TEXT(tex_times, XSIZE=19, /EDITABLE, $
				 UVALUE='TEXTSTOP')

;      lab = WIDGET_LABEL(times_input, VALUE='')

      os_label = WIDGET_BASE(edit, /ROW)
       ;col = WIDGET_BASE(os_label, /COLUMN, SPACE=12)
       ;col = WIDGET_BASE(os_label, /COLUMN, SPACE=14, YPAD=14)
       col = WIDGET_BASE(os_label, /COLUMN, SPACE=17, YPAD=10)
         lab = WIDGET_LABEL(col, VALUE='SIZE')
         lab = WIDGET_LABEL(col, VALUE='DURATION')
         lab = WIDGET_LABEL(col, VALUE='PROC_TIME')
         lab = WIDGET_LABEL(col, VALUE='CCD_TIME')
         lab = WIDGET_LABEL(col, VALUE='EXP_TIME')
         lab = WIDGET_LABEL(col, VALUE='SETUP_TIME')

       col = WIDGET_BASE(os_label, /COLUMN, SPACE=4, YPAD=4)
         new_value = 0D
         os_size_text = WIDGET_TEXT(col, XSIZE=11, /EDITABLE, VALUE=STRTRIM(STRING(new_value,size_fmt),2)+' ', $
                        UVALUE='OS_SIZE')
         os_dur_text = WIDGET_TEXT(col, XSIZE=11, /EDITABLE, VALUE=STRTRIM(STRING(new_value,time_fmt1),2)+' ', $
                        UVALUE='OS_DUR')
         os_proc_text = WIDGET_TEXT(col, XSIZE=11, /EDITABLE, VALUE=STRTRIM(STRING(new_value,time_fmt1),2)+' ', $
                        UVALUE='OS_PROC')
;         lab = WIDGET_LABEL(col, VALUE='')
         os_rot_text = WIDGET_TEXT(col, XSIZE=11, /EDITABLE, VALUE=STRTRIM(STRING(new_value,time_fmt2),2)+' ', $
                        UVALUE='OS_ROT')

        os_exp_text= WIDGET_TEXT(col, XSIZE=11, VALUE=STRTRIM(STRING(new_value,time_fmt2),2), UVALUE= "OS_EXP")

        os_setup_text = WIDGET_TEXT(col, XSIZE=11, /EDITABLE, VALUE=STRTRIM(STRING(new_value,time_fmt2),2)+' ', $
                       UVALUE='OS_SETUP')

;         lab = WIDGET_LABEL(col, VALUE='')

       col = WIDGET_BASE(os_label, /COLUMN,SPACE=5, YPAD=8)
         bits_bytes = CW_BSELECTOR2(col, ["bits ","bytes","% SBUF Cap","% SSR1 Cap","% SSR2 Cap","% RT Capac","% SW Capac"], SET_VALUE=0, UVALUE="BITS_BYTES")
         secs_mins  = CW_BSELECTOR2(col, ["secs ","mins "], SET_VALUE=0, UVALUE="SECS_MINS")

;     lab = WIDGET_LABEL(col, VALUE=' ')
;     lab = WIDGET_LABEL(col, VALUE=' ')
;     lab = WIDGET_LABEL(col, VALUE='Exp.Time')
;     os_exp_text= WIDGET_TEXT(col, XSIZE=7, VALUE=STRTRIM(STRING(new_value,time_fmt2),2), UVALUE= "OS_EXP") 

     ;
     ; Near the bottom of the left side are some editing control buttons.
     ;

     edit_study  = WIDGET_BASE(edit, /COL); XPAD=10, SPACE=10)
     ;edit_study  = WIDGET_BASE(edit, /COL, XPAD=-2, SPACE=5, YPAD= -3)
      row  = WIDGET_BASE(edit_study, /ROW); XPAD=10, SPACE=10)
      ;row  = WIDGET_BASE(edit_study, /ROW); XPAD=5, YPAD= 1, SPACE=5)
        ;tmp = WIDGET_BUTTON(row, VALUE='ADD',  UVALUE='ADD')
        tmp = WIDGET_BUTTON(row, VALUE='ADD',  UVALUE='ADD')
        tmp= WIDGET_LABEL(row, VALUE='  ')
        tmp = WIDGET_BUTTON(row, VALUE='MOVE',  UVALUE='MOVE_OS') ; AEE - 7/30/03
        tmp= WIDGET_LABEL(row, VALUE='  ')
	tmp = WIDGET_BUTTON(row, VALUE='DELETE', UVALUE='DELETE')
        tmp= WIDGET_LABEL(row, VALUE='  ')
	tmp = WIDGET_BUTTON(row, VALUE="DEL MULT OS's", UVALUE='DELETE_OSs')
      ;row  = WIDGET_BASE(edit_study, /ROW); XPAD=10, SPACE=10)
      row  = WIDGET_BASE(edit_study, /ROW); XPAD=10, SPACE=10)
        tmp= WIDGET_LABEL(row, VALUE='')
	tmp = WIDGET_BUTTON(row, VALUE='QUICK SUMMARY', UVALUE='SUMMARY')
        tmp= WIDGET_LABEL(row, VALUE='  ')
	tmp = WIDGET_BUTTON(row, VALUE='DETAILS', UVALUE='DETAILS')
        tmp= WIDGET_LABEL(row, VALUE='  ')
        sconf = WIDGET_BUTTON(row, VALUE='CONFLICTS', UVALUE='CONFLICTS')

  ;
  ; On the right-hand side of the interface is the display window.
  ;


   plan = WIDGET_BASE(cols, /COLUMN, XPAD= -1)

     sc_display= 1 ; at start, default display to spacecraft A 

     p1= WIDGET_BASE(plan,/ROW)     

     ;sc_arr= ['SpaceCraft - A and B','SpaceCraft - A','SpaceCraft - B']
     sc_arr= ['SpaceCrafts A and B Schedule','SpaceCraft A Schedule','SpaceCraft B Schedule']
     ;label= '                                                                 '
     ;sc_sel= CW_BSELECTOR2(plan, sc_arr, UVALUE='SpaceCraft', LABEL_LEFT= label)
     label= '                            '
     sc_sel= CW_BSELECTOR2(p1, sc_arr, UVALUE='SpaceCraft', LABEL_LEFT= ' ', set_value=1) ; default 1 = ST-A

     modes= ['Full','Partial']
     ;lab= WIDGET_LABEL(p1, VALUE='Display Mode: ') 
     ;lab =WIDGET_LABEL(p1, VALUE='   ')
     dmodes= CW_BSELECTOR2(p1, modes, UVALUE='DISP_MODE',LABEL_LEFT= 'Display Mode:') 
     smode= 0 ; 'Full' 

     ;lab =WIDGET_LABEL(p1, VALUE='   ')
     ;data_rates= ['High (0-14)','Med (14-18)','Low (18-24)']
     ;data_rates= ['High (0-14)','Med (14-18)','Low (18-24)','Phasing Orbit']
     data_rates= ['720 kbps (Jan 2007 A and B)','480 kbps (Oct 2008 A, Sep 2008 B)','360 kbps (May 2009 A, Jun 2009 B)',$
                  '240 kbps (Apr 2010 A, Dec 2009 B)','160 kbps (Sep 2010 A, Sep 2010 B)','120 kbps (Apr 2011 A, Nov 2010 B)',$
                  '96 kbps (Sep 2011 A, Sep 2011 B)','30 kbps (Aug 2012 A, Aug 2012 B)']
     ;drates= CW_BSELECTOR2(p1, data_rates, UVALUE='DATA_RATES',LABEL_LEFT= 'SSR Data Rate:')
     ;drate= 0 ; 'High' 
     ;drate= 0 ; '720' 

     ;drate= 3 ; '240' 
     ;drates= CW_BSELECTOR2(p1, data_rates, UVALUE='DATA_RATES', SET_VALUE= drate, LABEL_LEFT= 'SSR Data Rate:')
   
     mjd_switch_dates= [54710, $ ; Sep 1 2008, 480 kbps (used earliest of A or B switch dates)
                                54952, $ ; May 1 2009, 360 kbps
                                55166, $ ; Dec 1 2009, 240 kbps
                                55440, $ ; Sep 1 2010, 160 kbps
                        55501, $ ; Nov 1 2010, 120 kbps
                                55805, $ ; Sep 1 2011,  96 kbps
                                56140]   ; Aug 1 2012,  30 kbps

     GET_UTC, utc_today
     mjd_today= utc_today.mjd

     case 1 of
      mjd_today lt mjd_switch_dates(0): drate= 0 ; 720 kbps
      mjd_today lt mjd_switch_dates(1): drate= 1 ; 480 kbps
      mjd_today lt mjd_switch_dates(2): drate= 2 ; 360 kbps
      mjd_today lt mjd_switch_dates(3): drate= 3 ; 240 kbps
      mjd_today lt mjd_switch_dates(4): drate= 4 ; 160 kbps
      mjd_today lt mjd_switch_dates(5): drate= 5 ; 120 kbps
      mjd_today lt mjd_switch_dates(6): drate= 6 ;  96 kbps
      else:                            drate= 7 ;  30 kbps
     endcase 

     drates= CW_BSELECTOR2(p1, data_rates, UVALUE='DATA_RATES', SET_VALUE= drate, LABEL_LEFT= 'SSR Data Rate:')
 
     ;lab =WIDGET_LABEL(p1, VALUE='                                   ')
     ;lab = WIDGET_LABEL(p1, VALUE='                 ')
     ;lab = WIDGET_LABEL(p1, VALUE='           ')
     quit = WIDGET_BUTTON(p1, VALUE='QUIT', UVALUE='QUIT')

     ;mrow= WIDGET_BASE(plan, /ROW)
     ;blanks='                                                                         '
     blanks='                                                                                       '
     ;blanks= blanks+blanks+blanks
     blanks= blanks+blanks+blanks+blanks
     mdiag= WIDGET_LABEL(plan,FONT=font,VALUE='Starting up...',/frame,xsize=1150)
  
     ;
     ; The draw widget shows the studies from STARTDIS to ENDDIS
     ;

     ; Note; If drawable size is changed, also change the TVRD parameters in schedule_event for
     ;       creating the .gif file.

     ;draw = WIDGET_DRAW(plan, XSIZE=700, YSIZE=512*2, /BUTTON_EVENT, /SCROLL, Y_SCROLL_SIZE=512, $
     ;draw = WIDGET_DRAW(plan, XSIZE=700, YSIZE=512*2, /BUTTON_EVENT, /SCROLL, Y_SCROLL_SIZE=535, $
;			      X_SCROLL_SIZE=700, RETAIN=2, /FRAME, UVALUE='DRAW') 
     draw = WIDGET_DRAW(plan, XSIZE=1200, YSIZE=705*2, /BUTTON_EVENT, /SCROLL, Y_SCROLL_SIZE=735, $
                              x_scroll_size=1202, RETAIN=2, /FRAME, UVALUE='DRAW')


     ;
     ; Below the draw window are the time buttons.
     ;   In display mode, they modify the STARTDIS VALUE by the specified time
     ;   In edit mode, they modity the times in the start and stop text window.
     ;

     ;jump = WIDGET_BASE(plan, /ROW, /FRAME)
     ;jump = WIDGET_BASE(plan, /ROW, XPAD= 85)
     jump = WIDGET_BASE(plan, /ROW, XPAD= 190)
     ;jump = WIDGET_BASE(jump,/ROW, /FRAME)
     jump = WIDGET_BASE(jump,/ROW, XPAD=55, /FRAME)

      sarr= ['sec','2s','5s','10s','20s','30s']
      secarr= [1,2,5,10,20,30]
      marr= ['min','2m','5m','10m','20m','30m']
      minarr= [1,2,5,10,20,30]
      harr = ['hr','2h','3h','4h','5h','6h','12h']
      hrarr= [1,2,3,4,5,6,12]
      darr= ['day','2d','3d','4d','5d','10d','15d']
      dayarr= [1,2,3,4,5,10,15]

      ;jump1 = WIDGET_BASE(jump, /ROW,YPAD=-2,XPAD=-2)
      ;jj1 = WIDGET_BASE(jump, /ROW,YPAD=-2,XPAD=-2)
      jj1 = WIDGET_BASE(jump, /ROW)
      jump1 = WIDGET_BASE(jj1, /ROW)
   	;bck5 = WIDGET_BUTTON(jump1, VALUE='<mo',   UVALUE='BACKMON')
   	bck5 = WIDGET_BUTTON(jump1, VALUE='<month',   UVALUE='BACKMON')
        bck4 = CW_BSELECTOR2(jump1, '<'+darr, SET_VALUE=0, UVALUE='BACKDAY')
        bck3 = CW_BSELECTOR2(jump1, '<'+harr, SET_VALUE=0, UVALUE='BACKHR') 
        bck2 = CW_BSELECTOR2(jump1, '<'+marr, SET_VALUE=0, UVALUE='BACKMIN')
   	;bck1 = WIDGET_BUTTON(jump1, VALUE='<sec',  UVALUE='BACKSEC')
        bck1 = CW_BSELECTOR2(jump1, '<'+sarr, SET_VALUE=0, UVALUE='BACKSEC') 

      movetype = WIDGET_BASE(jump, /FRAME, /COLUMN, YPAD=-2, XPAD=-2)
	;
	; These buttons allow the user to select the current mode,
	; either display mode or edit mode.  The resulting action 
	; of many widgets is determined by this current mode.
	;

        arrow = WIDGET_BASE(movetype, /EXCLUSIVE, /COLUMN)
	    ; DO NOT use the /NO_RELEASE keyword, button setting control
	    ; is performed in the event handler.
            arr_span = WIDGET_BUTTON(arrow, VALUE='Display', UVALUE='ARROW_DIS')
            arr_edit = WIDGET_BUTTON(arrow, VALUE='Edit',  UVALUE='ARROW_EDIT')

      bufstats = WIDGET_BASE(jump, /FRAME, /COLUMN, YPAD= -2, XPAD= -1)
        txt= WIDGET_LABEL(bufstats, VALUE='TlmStats',FONT=font)
        bs1= WIDGET_BASE(bufstats, /COLUMN)
        uparr= ['Update','Skip']
        up_stats= 0 ; corresponding to 'Update' 
        onoff= CW_BSELECTOR2(bs1, uparr, UVALUE='UPSTATS') 

      ;jump2 = WIDGET_BASE(jump, /ROW,YPAD=-2,XPAD=-2)
      ;jj2 = WIDGET_BASE(jump, /ROW,YPAD=-2,XPAD=-2)
      jj2 = WIDGET_BASE(jump, /ROW)
      jump2 = WIDGET_BASE(jj2, /ROW)
   	;for1 = WIDGET_BUTTON(jump2, VALUE='sec> ',  UVALUE='FORSEC')
   	for1 = CW_BSELECTOR2(jump2, sarr+'>', SET_VALUE=0, UVALUE='FORSEC')
   	for2 = CW_BSELECTOR2(jump2, marr+'>', SET_VALUE=0, UVALUE='FORMIN')
        for3 = CW_BSELECTOR2(jump2, harr+'>', SET_VALUE=0, UVALUE='FORHR')
        for4 = CW_BSELECTOR2(jump2, darr+'>', SET_VALUE=0, UVALUE='FORDAY') 
   	;for5 = WIDGET_BUTTON(jump2, VALUE='mo>',   UVALUE='FORMON')
   	for5 = WIDGET_BUTTON(jump2, VALUE='month>',   UVALUE='FORMON')

     ;rrow = WIDGET_BASE(plan, /ROW)
     ;rrow = WIDGET_BASE(plan, /ROW, XPAD= 90, ypad= 10)
     ;rrow = WIDGET_BASE(plan, /ROW, XPAD= 90, ypad= 5)
     rrow = WIDGET_BASE(plan, /ROW, XPAD= 0, ypad= 5)

  ; below this is buffer statistics
  ;buffstats = WIDGET_BASE(rrow, SPACE=5, /COLUMN, /FRAME, YPAD= -3)
  ;buffstats = WIDGET_BASE(rrow, SPACE=0, /COLUMN, /FRAME, YPAD= -2)
  buffstats = WIDGET_BASE(rrow, SPACE=0, /COLUMN, /FRAME, YPAD= 0, /align_left)
     date_str = UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' + $
                UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
     ;pctlab= "Telemetry (To Be Downlinked / Downlinkable) Percentages For The Period Of"

     pctlab= "Telemetry Volume (MB) for The Period Of"
     pctlab= ["Telemetry (To Be Downlinked / Downlinkable) Percentages For The Period Of", $
              "Telemetry Volume (MB) for The Period Of"]
     pct_stats= 1 ; corresponding to pctlab[pct_stats] 
     ;pctid= CW_BSELECTOR2(buffstats, pctlab, UVALUE='PCTSTATS') 
     pct= WIDGET_BASE(buffstats,  /ROW)
     ;lab = WIDGET_LABEL(pct,VALUE='                                  ')
     pctid= CW_BSELECTOR2(pct,  pctlab, UVALUE='PCTSTATS', set_value=1)

     fakepb= 0 ; False 
     ;fpb = WIDGET_BASE(plan, SPACE=0, /ROW, YPAD= 0)
     txt= WIDGET_LABEL(pct, VALUE= '        ')
     fake= CW_BSELECTOR2(pct, ['Use Actual PlayBacks','Fake a 24hr PlayBack'], UVALUE= 'FAKE_PB') 


     ;plab = WIDGET_LABEL(buffstats, VALUE= pctlab, FONT=font)
     usage_dates = WIDGET_LABEL(buffstats, VALUE= date_str, FONT=font)

     ;lab = WIDGET_LABEL(buffstats, VALUE='Total           EUVI         COR1         COR2          HI1           HI2  ',/ALIGN_RIGHT)

     labuf= WIDGET_BASE(buffstats,  /ROW)
     ;spaces='                  '
     spaces='                          '
     lab= WIDGET_LABEL(labuf, VALUE=spaces,/ALIGN_RIGHT)
     ;lab= WIDGET_LABEL(labuf, VALUE='     Total',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='  Total',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='            EUVI',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='            COR1',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='             COR2',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='            HI1',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='            HI2',/ALIGN_RIGHT,FONT=font) 
     lab= WIDGET_LABEL(labuf, VALUE='             HK',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='              GT',/ALIGN_RIGHT,FONT=font)
     lab= WIDGET_LABEL(labuf, VALUE='        ')

     tuse= 0.0 & teuvi= 0.0 & tcor1= 0.0 & tcor2= 0.0 & thi1= 0.0 & thi2= 0.0

     ;use_str= STRING(tuse,'(f12.1)')+'%'+STRING(teuvi,'(f13.1)')+'%'+STRING(tcor1,'(f9.1)')+'%'+ $
     ;         STRING(tcor2,'(f11.1)')+'%'+STRING(thi1,'(f12.1)')+'%'+STRING(thi2,'(f11.1)')+'%'+'   '

     ;use_str= STRING(tuse,'(f12.1)')+' '+STRING(teuvi,'(f13.1)')+' '+STRING(tcor1,'(f9.1)')+' '+ $
     ;         STRING(tcor2,'(f11.1)')+' '+STRING(thi1,'(f12.1)')+' '+STRING(thi2,'(f11.1)')+' '+'   '

     use_str= STRING(tuse,'(f12.3)')+' '+STRING(teuvi,'(f13.3)')+' '+STRING(tcor1,'(f9.3)')+' '+ $
              STRING(tcor2,'(f11.3)')+' '+STRING(thi1,'(f12.3)')+' '+STRING(thi2,'(f11.3)')+' '+'   '
     

     ;buffstats0= WIDGET_BASE(buffstats, /COLUMN, YPAD= -2)
     buffstats0= WIDGET_BASE(buffstats, /COLUMN, YPAD= -1)

     sec_buf= WIDGET_BASE(buffstats0,  /ROW, YPAD= -2)
     ;spaces='                  '
     sbuf_stats = WIDGET_LABEL(sec_buf, VALUE= spaces,/ALIGN_RIGHT, FONT=font)
     sb_tot= WIDGET_LABEL(sec_buf, VALUE= STRING(0,'(f16.1)'),/ALIGN_RIGHT) 
     sb_eu= WIDGET_LABEL(sec_buf, VALUE= STRING(0,'(f14.1)'),/ALIGN_RIGHT) 
     sb_c1= WIDGET_LABEL(sec_buf, VALUE= STRING(0,'(f14.1)'),/ALIGN_RIGHT) 
     sb_c2= WIDGET_LABEL(sec_buf, VALUE= STRING(0,'(f15.1)'),/ALIGN_RIGHT) 
     sb_h1= WIDGET_LABEL(sec_buf, VALUE= STRING(0,'(f13.1)'),/ALIGN_RIGHT) 
     sb_h2= WIDGET_LABEL(sec_buf, VALUE= STRING(0,'(f13.1)'),/ALIGN_RIGHT)
     
     ;buffstats1 = WIDGET_BASE(buffstats, /COLUMN, YPAD= -2)
     buffstats1 = WIDGET_BASE(buffstats, /COLUMN, YPAD= 0)
     ssr1_buf= WIDGET_BASE(buffstats1,  /ROW, YPAD= -2)
     ssr1_stats= WIDGET_LABEL(ssr1_buf, VALUE= spaces,/ALIGN_RIGHT, FONT=font)
     s1_tot= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f16.3)'),/ALIGN_RIGHT)
     s1_eu= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     s1_c1= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     s1_c2= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f15.3)'),/ALIGN_RIGHT)
     s1_h1= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     s1_h2= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     s1_hk= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     s1_gt= WIDGET_LABEL(ssr1_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)

     ;buffstats2 = WIDGET_BASE(buffstats, /COLUMN, YPAD= -2)
     buffstats2 = WIDGET_BASE(buffstats, /COLUMN, YPAD= 0)
     ssr2_buf= WIDGET_BASE(buffstats2,  /ROW, YPAD= -2)
     ssr2_stats= WIDGET_LABEL(ssr2_buf, VALUE= spaces,/ALIGN_RIGHT, FONT=font)
     s2_tot= WIDGET_LABEL(ssr2_buf, VALUE= STRING(0,'(f16.3)'),/ALIGN_RIGHT)
     s2_eu= WIDGET_LABEL(ssr2_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     s2_c1= WIDGET_LABEL(ssr2_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     s2_c2= WIDGET_LABEL(ssr2_buf, VALUE= STRING(0,'(f15.3)'),/ALIGN_RIGHT)
     s2_h1= WIDGET_LABEL(ssr2_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     s2_h2= WIDGET_LABEL(ssr2_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)

     ;buffstats3 = WIDGET_BASE(buffstats, /COLUMN, YPAD= -2)
     buffstats3 = WIDGET_BASE(buffstats, /COLUMN, YPAD= 0)
     rt_buf= WIDGET_BASE(buffstats3,  /ROW, YPAD= -2)
     rt_stats= WIDGET_LABEL(rt_buf, VALUE= spaces,/ALIGN_RIGHT, FONT=font)
     rt_tot= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f16.3)'),/ALIGN_RIGHT)
     rt_eu= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     rt_c1= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     rt_c2= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f15.3)'),/ALIGN_RIGHT)
     rt_h1= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     rt_h2= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     rt_hk= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     rt_gt= WIDGET_LABEL(rt_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
 

     ;buffstats4 = WIDGET_BASE(buffstats, /COLUMN, YPAD= -2)
     buffstats4 = WIDGET_BASE(buffstats, /COLUMN, YPAD= 0)
     sw_buf = WIDGET_BASE(buffstats4, /ROW, YPAD= -2)
     ;sw_stats  = WIDGET_LABEL(sw_buf, VALUE= '    SWch ',/ALIGN_RIGHT, FONT=font)
     sw_stats= WIDGET_LABEL(sw_buf, VALUE= spaces,/ALIGN_RIGHT, FONT=font)
     sw_tot= WIDGET_LABEL(sw_buf, VALUE= STRING(0,'(f16.3)'),/ALIGN_RIGHT)
     sw_eu= WIDGET_LABEL(sw_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     sw_c1= WIDGET_LABEL(sw_buf, VALUE= STRING(0,'(f14.3)'),/ALIGN_RIGHT)
     sw_c2= WIDGET_LABEL(sw_buf, VALUE= STRING(0,'(f15.3)'),/ALIGN_RIGHT)
     sw_h1= WIDGET_LABEL(sw_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
     sw_h2= WIDGET_LABEL(sw_buf, VALUE= STRING(0,'(f13.3)'),/ALIGN_RIGHT)
  
     

 WIDGET_CONTROL, base, /REALIZE		; bring widget to life
 WIDGET_CONTROL, /HOUR

 ;WIDGET_CONTROL,mdiag,SET_VALUE='Tool Start Up.....'
 ;
 ; Set some initial widget conditions
 ;

 WIDGET_CONTROL, sbuf_stats, SET_VALUE= 'SBUF  '
 WIDGET_CONTROL, ssr1_stats, SET_VALUE= 'SSR1  '
 WIDGET_CONTROL, ssr2_stats, SET_VALUE= 'SSR2  '
 WIDGET_CONTROL, rt_stats, SET_VALUE= 'RTch  '
 WIDGET_CONTROL, sw_stats, SET_VALUE= 'SWch  '

 WIDGET_CONTROL, start_text, SET_VALUE=TAI2UTC(startdis, /ECS)	; write current
 WIDGET_CONTROL, stop_text,  SET_VALUE=TAI2UTC(enddis,   /ECS);    time range
 ;widget_control, drop_span, set_droplist_select = 11  ; "1 day span"
 widget_control, drop_span, set_droplist_select = 13  ; "1 day span"

 WIDGET_CONTROL, start_text, /INPUT_FOCUS	; input focus at start time

 WIDGET_CONTROL, arr_span, /SET_BUTTON		; initially in display mode
 WIDGET_CONTROL, edit_study, SENSITIVE=0	; display mode has no editting
 WIDGET_CONTROL, os_label, SENSITIVE=0		; start out in OP mode

; WIDGET_CONTROL, mlab, SENSITIVE=0 ; AEE 4/6/04
; WIDGET_CONTROL, ctmp, SENSITIVE=0 ; AEE 4/6/04
; WIDGET_CONTROL, etmp, SENSITIVE=0 ; AEE 4/6/04
; WIDGET_CONTROL, ltmp, SENSITIVE=0 ; AEE 4/6/04
; WIDGET_CONTROL, dtmp, SENSITIVE=0 ; AEE 4/6/04

 WIDGET_CONTROL, sc_sel
 WIDGET_CONTROL, dmodes, SENSITIVE=0

 WIDGET_CONTROL, draw, GET_VALUE=win_index	; find out window's index no.
 WIDGET_CONTROL, draw, SET_DRAW_VIEW=[0,0]	; set viewport to bottom left
 WSET, win_index                                ; set out to display window

 XYOUTS,50,400,'The FSW normally reads in a schedule at midnight. Depending on the length of schedule,',/dev,charsize=1.5
 xyouts,50,360,'allow an appropriate delay time for reading the schedule before start of first observation.',/dev,charsize=1.5 
 xyouts,50,320,'For example, assuming a delay time of 35 seconds, start the first observation from at least',/dev,charsize=1.5
 xyouts,50,280,'00:00:35, to avoid losing one or more of starting observations.',/dev,charsize=1.5
 ;wait,2

 ;** set up color table
 LOADCT, 0
 TVLCT, 255,0,0, 1	;** load red into entry 1
 TVLCT, 0,255,0, 2	;** load green into entry 2
 TVLCT, 142,229,238, 3	;** load blue into entry 3
 TVLCT, 255,255,0,4	;** load yellow into entry 4
 TVLCT, 204,117,89, 5   ;** load brown into entry 5    ;AEE changed here and in mask.pro and mask_old.pro
 TVLCT, 200,0,0, 6 	;** load dull red into entry 6 
 TVLCT, 0,200,0, 7 	;** load dull green into entry 7 
 TVLCT, 0,206,237, 8 	;** load dull blue into entry 8 
 TVLCT, 200,200,0, 9	;** load dull yellow into entry 9  
 TVLCT, 180,95,85, 10   ;** load dull dark brown into entry 10
 TVLCT, 142,229,238, 11  ;** load light blue into entry 11 - used by mask.pro

 ;
 ; Draw all instruments' plans for the input operational day
 ;

 FAKE_OP, startdis ; AEE 2/13/04 - either keep this or remove common blocks defined in it from schedule_plot, etc.
 FAKE_SET
 IF (read_ipt_flag EQ '') THEN FAKE_OS, startdis

 ; Note: don't read in SCA file(s) for AB scheule. Read them later when only S/C A or
 ;       SC/ B is selected.
 ;READ_SCA, 1, startdis, enddis, /DEFAULT  ; read SpaceCraft-A default activity file. 
 ;READ_SCA, 2, startdis, enddis, /DEFAULT  ; read SpaceCraft-B default activity file.

 SCA_INIT
 sca_fname= GETENV('PT')+'/IN/SCA/SC_A/DEFAULT.SCA'
 scb_fname= GETENV('PT')+'/IN/SCA/SC_B/DEFAULT.SCA'

 charsize=1.2
 ;pct_start = 10  ; AEE - Dec 20, 02

 filename= GETENV('PT')+'/IN/OTHER/'+'pct_full_starts.sav'
 RESTORE, filename ; => pct_full struture {sbuf,ssr1,ssr2,rt,sw}  ; AEE 12/05/03

 sbuf_pct_start_a = pct_full.sbuf_a  ; AEE - 12/05/03 
 ssr1_pct_start_a = pct_full.ssr1_a  ; AEE - 12/05/03 
 ssr2_pct_start_a = pct_full.ssr2_a  ; AEE - 12/05/03 
 rt_pct_start_a =   pct_full.rt_a    ; AEE - 12/05/03 
 sw_pct_start_a =   pct_full.sw_a    ; AEE - 12/05/03
 ssr2_pct_pb_a  =   pct_full.ssr2_pb_a  ; AEE - 04/20/06
 sbuf_pct_start_b = pct_full.sbuf_b
 ssr1_pct_start_b = pct_full.ssr1_b
 ssr2_pct_start_b = pct_full.ssr2_b
 rt_pct_start_b =   pct_full.rt_b
 sw_pct_start_b =   pct_full.sw_b
 ssr2_pct_pb_b  =   pct_full.ssr2_pb_b

 ;
 ; Create a structure that will be carried around as the UVALUE of the main base (BASE)
 ; 

 schedv = { $
	base:base,  	    	    $	; widget ID for the base widget
	arr_span:arr_span,  	    $	; widget ID for the DISPLAY MODE button
	arr_edit:arr_edit,  	    $	; widget ID for the PLAN MODE button
	win_index:win_index,  	    $	; window number for the draw window
	x:!x,  	    		    $	; !X system var for schedule plot window
	y:!y,  	    		    $	; !Y system var for schedule plot window
	p:!p,  	    		    $	; !P system var for schedule plot window
	baseop:baseop,		    $   ; the user entered ops day (TAI)
	startdis:startdis, 	    $	; Time of first display data pt. (TAI)
	enddis:enddis,   	    $	; Last display point time (TAI)
        span_list:span_list,        $   ; AEE - 3/24/04 - added.
        span_sec:span_sec,          $   ; seconds represented by span droplist
	duration:duration,	    $	; How many seconds to display 
        proc_time:0D,               $   ; Pre processing time (including CCD readout but not image processing)
        ro_time:4.0D,               $   ; CCD readout time in seconds
	charsize:charsize,	    $	; charsize
	num_text:num_text,     	    $	; widget ID for the OS_NUM or OP_NUM label
	start_text:start_text,      $	; widget ID for the start time text
	stop_text:stop_text,  	    $	; widget ID for the stop time text
	last_text:stop_text,  	    $	; widget ID for last time
	count_text:count_text, 	    $	; widget ID for the count text
	delta_text:delta_text, 	    $	; widget ID for the delta time text
	edit_study:edit_study,      $	; widget ID for the parent of the SCHEDULE & READY buttons
	mode:mode,  		    $	; the current mode (display or edit)
	study_start:study_start,    $	; start time of a study in plan mode
	study_stop:study_stop,      $	; end time of a study in plan mode
	os_label:os_label,          $   ; widget ID for the parent of the OS_DURATION & OS_SIZE labels
	os_size_text:os_size_text,  $   ; widget ID for the OS_SIZE label
	os_dur_text:os_dur_text,    $   ; widget ID for the OS_DURATION label
        os_proc_text:os_proc_text,  $   ; widget ID for the OS_PROC (processing time) label
        os_rot_text:os_rot_text,    $   ; widget ID for the OS_ROT (CCD read out time) label
        os_setup_text:os_setup_text,$
        os_exp_text:os_exp_text,$
	os_size_units:0,            $   ; for converting os_size bits to desired units (default bits)
	os_dur_factor:1.0D,         $   ; factor for converting os_dur secs to desired units (default secs)
	bits_bytes:bits_bytes,      $   ; widget ID for the bits_bytes button
	secs_mins:secs_mins,        $   ; widget ID for the secs_mins button
        size_fmt:size_fmt,          $
        time_fmt1:time_fmt1,        $
        time_fmt2:time_fmt2,        $
	;pct_start:pct_start,        $   ; percentage of buffer at start of schedule
        sbuf_pct_start_a:sbuf_pct_start_a, $   ; percentage of buffer at start of schedule FOR A
        ssr1_pct_start_a:ssr1_pct_start_a, $   ; percentage of ssr1 at start of schedule
        ssr2_pct_start_a:ssr2_pct_start_a, $   ; percentage of ssr2 at start of schedule
        ssr2_pct_pb_a:ssr2_pct_pb_a,       $   ; percentage of ssr2 to be played back
        rt_pct_start_a:rt_pct_start_a, $   ; percentage of RT at start of schedule
        sw_pct_start_a:sw_pct_start_a, $   ; percentage of SW at start of schedule
        sbuf_pct_start_b:sbuf_pct_start_b, $   ; percentage of buffer at start of schedule for B
        ssr1_pct_start_b:ssr1_pct_start_b, $   ; percentage of ssr1 at start of schedule
        ssr2_pct_start_b:ssr2_pct_start_b, $   ; percentage of ssr2 at start of schedule
        ssr2_pct_pb_b:ssr2_pct_pb_b,       $   ; percentage of ssr2 to be played back
        rt_pct_start_b:rt_pct_start_b, $   ; percentage of RT at start of schedule
        sw_pct_start_b:sw_pct_start_b, $   ; percentage of SW at start of schedule
	usage_dates:usage_dates,    $   ; widget ID for the buffer usage date label
	;usage_stats:usage_stats,     $   ; widget ID for the buffer usage stats label
	;lusage_stats:lusage_stats   $   ; widget ID for the LEB usage stats label
        sbuf_stats:sbuf_stats,       $
        sb_tot: sb_tot,              $
        sb_eu:sb_eu,                 $
        sb_c1:sb_c1,                 $
        sb_c2:sb_c2,                 $
        sb_h1:sb_h1,                 $
        sb_h2:sb_h2,                 $
        ssr1_stats:ssr1_stats,       $
        s1_tot: s1_tot,              $
        s1_eu: s1_eu,                $
        s1_c1: s1_c1,                $
        s1_c2: s1_c2,                $ 
        s1_h1: s1_h1,                $ 
        s1_h2: s1_h2,                $ 
        s1_hk: s1_hk,                $
        s1_gt: s1_gt,                $
        ssr2_stats:ssr2_stats,       $
        s2_tot: s2_tot,              $
        s2_eu: s2_eu,                $
        s2_c1: s2_c1,                $ 
        s2_c2: s2_c2,                $ 
        s2_h1: s2_h1,                $  
        s2_h2: s2_h2,                $
        rt_stats:rt_stats,           $
        rt_tot: rt_tot,              $
        rt_eu: rt_eu,                $
        rt_c1: rt_c1,                $ 
        rt_c2: rt_c2,                $ 
        rt_h1: rt_h1,                $ 
        rt_h2: rt_h2,                $
        rt_hk: rt_hk,                $
        rt_gt: rt_gt,                $
        sw_stats:sw_stats,           $
        sw_tot: sw_tot,              $
        sw_eu: sw_eu,                $
        sw_c1: sw_c1,                $
        sw_c2: sw_c2,                $
        sw_h1: sw_h1,                $
        sw_h2: sw_h2,                $
;        gipt:gipt,                   $ ; AEE 1/15/04
        gall:gall,                   $ ; AEE 1/15/04
;        conf:conf,                   $
        sconf:sconf,                 $
        sca:sca,                     $
        sched_set:sched_set,         $
        sdoor:sdoor,                 $
        sched_bs:sched_bs,           $
        secarr: secarr,              $
        minarr: minarr,              $ ; AEE 7/29/03
        hrarr: hrarr,                $ ; AEE 7/29/03
        dayarr: dayarr,              $ ; AEE 7/29/03
        up_stats: up_stats,          $ ; AEE 7/30/02
        pct_stats: pct_stats,        $ 
        pctid: pctid,                $
        quit: quit,                  $
        def_os: def_os,              $
        sgt: sgt,                    $
        store: store,                $
        ript: ript,                  $
        get_os_op: get_os_op,        $
        help: help,                  $
        prog: prog,                  $
        col1: col1,                  $
        sc_sel: sc_sel,              $
        gen: gen,                    $
        drop_span: drop_span,        $
        jump: jump,                  $
        jump1: jump1,                $
        arrow: arrow,                $
        jump2: jump2,                $
        dmodes: dmodes,              $
        smode: smode,                $
        drate: drate,                $
;        csf: csf,                    $
        fakepb: fakepb,              $
        sc: sc_display               $
       }


 SCHEDULE_PLOT, schedv

 ;
 ; Load the structure into the main base's UVALUE for one way trip to handler
 ;

; WIDGET_CONTROL,mdiag,SET_VALUE='Tool Startup Done.'
 WIDGET_CONTROL,mdiag,SET_VALUE='Tool Startup Done'

 WIDGET_CONTROL, base, SET_UVALUE=schedv
 sched_base = base

 ;De-activate Sets and BlockSeq and Doors for AB schedule:
 IF (schedv.sc EQ 0) THEN BEGIN
   WIDGET_CONTROL, schedv.sched_set, SENSITIVE=0
   WIDGET_CONTROL, schedv.sched_bs, SENSITIVE=0
   WIDGET_CONTROL, schedv.sca, SENSITIVE=0
   WIDGET_CONTROL, schedv.sdoor, SENSITIVE=0
 ENDIF

 ;
 ; Call the XMANAGER to take control until there is an event
 ;

 XMANAGER, 'SCHEDULE', base, EVENT_HANDLER='SCHEDULE_EVENT'


RETURN
END
