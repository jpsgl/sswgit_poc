;+
;$Id: view_sets_summary.pro,v 1.1.1.2 2004/07/01 21:19:12 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : VIEW_SETS_SUMMARY
;               
; Purpose     : This function generate a string array of information for
;               all OSes in a selected set and returns them to the
;               calling program to be displayed.
;               
; Use         : set_info = VIEW_SETS_SUMMARY(os_arr,defined_set_arr) 
;    
; Inputs      : os_arr           All of currently scheduled OSes.
;               defined_set_arr  All of currently scheduled OSes that
;                                belong to the set. 
;
; Opt. Inputs : None
;               
; Outputs     : set_info  A string array of OS info - one line per os_num. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: view_sets_summary.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:12  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:37  esfand
; first checkin
;
;
;-


FUNCTION VIEW_SETS_SUMMARY, os_arr, defined_set_arr

  IF (DATATYPE(defined_set_arr) NE 'STC') THEN BEGIN
    msg= '"defined_set_arr" is empty."'
    PRINT, msg
    RETURN, msg 
  ENDIF

  set_arr= defined_set_arr
  set_arr=set_arr(SORT(set_arr.set_id))
  w= WHERE(STRTRIM(set_arr.imp_by,2) eq '', wcnt)
  IF (wcnt GT 0) THEN set_arr(w).imp_by= 'the same person'

  info= ''
  FOR i=0, N_ELEMENTS(set_arr)-1 DO BEGIN
    w= WHERE(os_arr.set_id EQ set_arr(i).set_id AND os_arr.set_cnt EQ set_arr(i).set_cnt, wcnt)
    IF (wcnt GT 0) THEN BEGIN
      info= [info,'SET_ID: '+STRING(set_arr(i).set_id,'(i4.4)')+' ('+set_arr(i).fname+') '+ $
                  'created for '+set_arr(i).req_by+' and scheduled for '+set_arr(i).imp_by+':'] 
      info= [info,OS_SUMMARY_SET(os_arr(w)),'','']
    ENDIF
  ENDFOR

  RETURN,info
END
