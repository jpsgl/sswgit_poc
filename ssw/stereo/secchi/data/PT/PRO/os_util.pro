;+
;$Id: os_util.pro,v 1.1.1.2 2004/07/01 21:19:06 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_UTIL
;		X0()
;		Y0()
;               
; Purpose     : Utility routines used by SECCHI DEFINE_OS tool.
;               
; Explanation : X0() and Y(0) return the x and y coordinates of where
;		to place the next widget window on the screen.
;               
; Use         : xloc = X0(base)
;               yloc = Y0(base)
;    
; Inputs      : base 	Widget ID of the base you are wanting to place.
;               
; Opt. Inputs : None.
;               
; Outputs     : xloc	X position.
;               yloc	Y position.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Calls       : None.
;
; Common      : LOCATION_SHARE, x, y, xx, yy, last_base  Defined in this routine.
;               
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;
; $Log: os_util.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:06  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

FUNCTION X0, base
   COMMON LOCATION_SHARE, x, y, xx, yy, last_base
   WIDGET_CONTROL, last_base, BAD_ID=bad
   IF (bad EQ 0) THEN BEGIN
      WIDGET_CONTROL, last_base, TLB_GET_OFFSET=xy
      x = xy(0) & y = xy(1)
      WIDGET_CONTROL, base, TLB_GET_SIZE=bs
      x = (x+40)
      IF ((x + bs(0)) GT xx) THEN x=0
   ENDIF
   RETURN, x
END
FUNCTION Y0, base
   COMMON LOCATION_SHARE, x, y, xx, yy, last_base
   WIDGET_CONTROL, last_base, BAD_ID=bad
   IF (bad EQ 0) THEN BEGIN
      WIDGET_CONTROL, last_base, TLB_GET_OFFSET=xy
      x = xy(0) & y = xy(1)
      WIDGET_CONTROL, base, TLB_GET_SIZE=bs
      y = (y+40)
      IF ((y + bs(1)) GT yy) THEN y=0
   ENDIF
   last_base = base
   RETURN, y
END

;__________________________________________________________________________________________________________
;

PRO OS_UTIL
END
