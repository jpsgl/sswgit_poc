;+
;$Id: schedule_modtime.pro,v 1.1.1.2 2004/07/01 21:19:10 esfand Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : SCHEDULE_MODTIME
;
; Purpose     : Modify the time shown in the selected text widgets.
;
; Explanation :
;       Modify the time shown in the selected text widgets, either the
;       start time widget or the stop time widget. The selected window
;       is the one with the highlight in it.
;       Note: Highlight means highlighted characters, not which base has
;             the input focus.
;
;       If one of the text widgets used for start and stop time has a
;       highlight in it (either user generated or program generated) then
;       the arrow time button selection will modify the times displayed
;       by the appropriate amount.  If there is no highlight in either
;       text window, then perform the appropriate time modification to
;       the last highlighted window.
;
; Use         :
;       SCHEDULE_MODTIME, SEC=SEC, SCHEDV=SCHEDV, LOCATE=LOCATE
;
; Inputs      :
;       SEC             The number of seconds by which the currently
;                          display time is to be modified.  Either
;                          positive or negative.
;       SCHEDV             A structure that contains the widget ID of the
;                          start time text widget and the end time text widget.
;                          (There are additional unused tag in the structure.)
;       LOCATE          A two element integer vector containing the text
;                          widget location to highlight and number of
;                          characters to highlight
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : All parameters are keywords.
;
; Restrictions: None
;
; Side effects: None.
;
; Category    : Planning, Scheduling
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: schedule_modtime.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:10  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

PRO SCHEDULE_MODTIME, SEC=sec, SCHEDV=schedv, LOCATE=locate

;
; Find out which time text widget has the highlight.
; NOTE:  This mean wht window that has some characters highlighted,
;        not which window shows the input focus.   
;        I know how to set the input focus but not how to determiNE
;	 which window has the focus.  Sensing the input focus is a
;	 much better option.
; If NEither window has a highlight THEN use last highlighted (CDS.LAST_TEXT).
;

  lit_1 = widget_info(schedv.start_text, /text_select) ; check for highlight start 
  lit_2 = widget_info(schedv.stop_text, /text_select)  ; check for highlight stop 


  IF lit_1(1) NE 0 THEN type=0 		$	; highlight is in start window
  ELSE IF lit_2(1) NE 0 THEN type=1 	$	; highlight is in stop window
  ELSE IF schedv.last_text eq schedv.start_text THEN type=0 $	; no highlights but 
							; start text was last 
							; set as highlight
  ELSE type=1 	; no highlights but stop text was last set as highlight


  IF type eq 0 THEN BEGIN	
	;
	; Modify the start time by SEC and write to start text widget
	;

	     ; Need to read what is currently in the window.
	     ; (If the user modIFied the text widget without  
	     ;  pressing RETURN THEN the time in the window 
 	     ;  is not what is stored in CDS.STUDY_START)

	     WIDGET_CONTROL, schedv.start_text, GET_VALUE=get_start
	     schedv.study_start = UTC2TAI(get_start(0))	; ASCII to TAI

	     ; Modify start time by the input amount

	     schedv.study_start = schedv.study_start + sec

	     ; Rewrite text widget to reflect new time

	     WIDGET_CONTROL, schedv.start_text, $
			     set_value= TAI2UTC(schedv.study_start, /ECS)

	     ; Highlight the part of start time that has just been changed

	     WIDGET_CONTROL, schedv.start_text, SET_TEXT_SELECT=locate	

  ENDIF ELSE BEGIN 	
	;
	; Modify the stop time by SEC and write to stop text widget
	;

	     ; Need to read what is currently in the window
	     ; (If the user modIFied the text widget without  
	     ;  pressing RETURN THEN the time in the window 
 	     ;  is not what is stored in CDS.STUDY_START)


	     WIDGET_CONTROL, schedv.stop_text, GET_VALUE=get_stop
	     schedv.study_stop = UTC2TAI(get_stop(0))	; ASCII to TAI

	     ; Modify stop time by the input amount

	     schedv.study_stop = schedv.study_stop + sec

	     ; Rewrite text widget to reflect new time

	     WIDGET_CONTROL, schedv.stop_text, $		; TAI to ASCII
	   		     set_value= TAI2UTC(schedv.study_stop, /ECS) 

	     ; Highlight the part of stop time that has just been changed

	     WIDGET_CONTROL, schedv.stop_text, SET_TEXT_SELECT=locate	

  ENDELSE

RETURN
END


PRO SCHEDULE_MODTIME, SEC=sec, SCHEDV=schedv, LOCATE=locate

;
; Find out which time text widget has the highlight.
; NOTE:  This mean wht window that has some characters highlighted,
;        not which window shows the input focus.   
;        I know how to set the input focus but not how to determiNE
;	 which window has the focus.  Sensing the input focus is a
;	 much better option.
; If NEither window has a highlight THEN use last highlighted (CDS.LAST_TEXT).
;

  lit_1 = widget_info(schedv.start_text, /text_select) ; check for highlight start 
  lit_2 = widget_info(schedv.stop_text, /text_select)  ; check for highlight stop 


  IF lit_1(1) NE 0 THEN type=0 		$	; highlight is in start window
  ELSE IF lit_2(1) NE 0 THEN type=1 	$	; highlight is in stop window
  ELSE IF schedv.last_text eq schedv.start_text THEN type=0 $	; no highlights but 
							; start text was last 
							; set as highlight
  ELSE type=1 	; no highlights but stop text was last set as highlight


  IF type eq 0 THEN BEGIN	
	;
	; Modify the start time by SEC and write to start text widget
	;

	     ; Need to read what is currently in the window.
	     ; (If the user modIFied the text widget without  
	     ;  pressing RETURN THEN the time in the window 
 	     ;  is not what is stored in CDS.STUDY_START)

	     WIDGET_CONTROL, schedv.start_text, GET_VALUE=get_start
	     schedv.study_start = UTC2TAI(get_start(0))	; ASCII to TAI

	     ; Modify start time by the input amount

	     schedv.study_start = schedv.study_start + sec

	     ; Rewrite text widget to reflect new time

	     WIDGET_CONTROL, schedv.start_text, $
			     set_value= TAI2UTC(schedv.study_start, /ECS)

	     ; Highlight the part of start time that has just been changed

	     WIDGET_CONTROL, schedv.start_text, SET_TEXT_SELECT=locate	

  ENDIF ELSE BEGIN 	
	;
	; Modify the stop time by SEC and write to stop text widget
	;

	     ; Need to read what is currently in the window
	     ; (If the user modIFied the text widget without  
	     ;  pressing RETURN THEN the time in the window 
 	     ;  is not what is stored in CDS.STUDY_START)


	     WIDGET_CONTROL, schedv.stop_text, GET_VALUE=get_stop
	     schedv.study_stop = UTC2TAI(get_stop(0))	; ASCII to TAI

	     ; Modify stop time by the input amount

	     schedv.study_stop = schedv.study_stop + sec

	     ; Rewrite text widget to reflect new time

	     WIDGET_CONTROL, schedv.stop_text, $		; TAI to ASCII
	   		     set_value= TAI2UTC(schedv.study_stop, /ECS) 

	     ; Highlight the part of stop time that has just been changed

	     WIDGET_CONTROL, schedv.stop_text, SET_TEXT_SELECT=locate	

  ENDELSE

RETURN
END
