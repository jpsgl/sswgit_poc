;+
;$Id: chk_osnum_db.pro,v 1.7 2009/09/11 20:28:07 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : CHK_OSNUM_DB
;
; Purpose     : Checks a new image setup against an existing Observation setup database
;               to see if it exsits.
;
; Explanation :
;
; Use         : CHK_OSNUM_DB, tele, exptable, extimes, fw, pw, led, cadence, n_images,
;                             lp_num, size, os_num, fps
;   
; Inputs      : tele       - telescope number
;             : exptable   - exposure table entry number 
;             : extimes    - table of exposure times
;             : fw         - Filter Wheel positions
;             : pw         - Polarizer/Sector Wherel positions
;             : led        - calibration LED ID
;             : cadence    - tadence between images (for Sequences)
;             : n_images   - number of images (for Sequences)
;             : lp_num     - type of image taken
;             : size       - size of image taken
;             : fps        - FPS ON/OFF indicator
;             : iptable    - Image processing table number
;             : camtable   - Camera processing table number
;             : ccd        - CCD setup
;             : ip_steps   - Image Processing steps
;
; Opt. Inputs : None
;
; Outputs     : os_num     - Observation ID of existing image setup. It is set
;                            to -1 if it does not exist.
;
; Keywords    : None
;
; Restrictions: None
;
; Side effects: None
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : None
;
; Written By  : Ed Esfandiari, NRL - First Version.
;               
; Modification History:
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 03/09/05 - Added checks for CCD and IP info.
;              Ed Esfandiari 05/22/07 - Added exptable # to save structure.
;
; $Log: chk_osnum_db.pro,v $
; Revision 1.7  2009/09/11 20:28:07  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2005/03/10 16:41:29  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
;
;- 

 
PRO CHK_OSNUM_DB, tele, exptable, extimes, fw, pw, led, cadence, n_images, lp_num, size, os_num, $
                  fps, iptable, camtable, ccd, ip_steps

COMMON OS_INIT_SHARE

  ; Change filter position to actual description:
  fws= FILTER2STR(fw_types,exptable,fw(0),tele)

  ; Change polar/sector wheel positions (maximum of 5) to actual descriptions.
  ; If less than 5 pw, then set the rest to ''. 
  ; Also do the same for exp. times:

  pws= POLAR2STR(pw_types,exptable,pw(0),tele)
  exptm= extimes(tele,exptable,fw(0),pw(0))
  FOR i= 1, N_ELEMENTS(pw)-1 DO BEGIN
    pws= [pws,POLAR2STR(pw_types,exptable,pw(i),tele)]
    exptm= [exptm,extimes(tele,exptable,fw(0),pw(i))]
  ENDFOR
  k= N_ELEMENTS(pws)
  FOR j= k, 4 DO BEGIN 
    pws= [pws,'']
    exptm= [exptm,0.0]
  ENDFOR

  os_num= -1
  f= FINDFILE(GETENV('PT')+'/IN/OTHER/defined_osnum.sav')
  IF (STRLEN(f(0)) NE 0) THEN BEGIN
    RESTORE, f(0)  ; => os_db structure of os_info1  
    IF (DATATYPE(os_db) NE 'UND') THEN BEGIN
      w= WHERE(os_db.tele         EQ tele         AND $
               os_db.size         EQ size         AND $
               os_db.fw           EQ  fws         AND $
               os_db.pw(0)        EQ  pws(0)      AND $
               os_db.pw(1)        EQ  pws(1)      AND $
               os_db.pw(2)        EQ  pws(2)      AND $  
               os_db.pw(3)        EQ  pws(3)      AND $  
               os_db.pw(4)        EQ  pws(4)      AND $  
               os_db.exptable     EQ  exptable    AND $
               os_db.exp(0)       EQ  exptm(0)    AND $
               os_db.exp(1)       EQ  exptm(1)    AND $
               os_db.exp(2)       EQ  exptm(2)    AND $
               os_db.exp(3)       EQ  exptm(3)    AND $
               os_db.exp(4)       EQ  exptm(4)    AND $
               os_db.led          EQ  led         AND $
               os_db.cad          EQ  cadence     AND $
               os_db.n_images     EQ  n_images    AND $
               os_db.fps          EQ  fps         AND $
               os_db.lp_num       EQ  lp_num      AND $
               os_db.iptable      EQ  iptable     AND $
               os_db.camtable     EQ  camtable    AND $
               os_db.ccd.clr_id   EQ ccd.clr_id   AND $
               os_db.ccd.rdout_id EQ ccd.rdout_id AND $
               os_db.ccd.x1       EQ ccd.x1       AND $
               os_db.ccd.y1       EQ ccd.y1       AND $
               os_db.ccd.x2       EQ ccd.x2       AND $
               os_db.ccd.y2       EQ ccd.y2       AND $
               os_db.ccd.xsum     EQ ccd.xsum     AND $
               os_db.ccd.ysum     EQ ccd.ysum     AND $
               os_db.ccd.offset   EQ ccd.offset   AND $
               os_db.ccd.gain     EQ ccd.gain     AND $
               os_db.ccd.gmode    EQ ccd.gmode    AND $
               os_db.ccd.cl_time  EQ ccd.cl_time  AND $
               os_db.ccd.ro_time  EQ ccd.ro_time, cnt)
               ;os_db.lp_num   EQ lp_num, cnt)
       IF (cnt GT 0) THEN BEGIN
         ;os_num= os_db(w(0)).os_num
         os_db= os_db(w)
         FOR i=0, cnt-1 DO BEGIN
           w1= WHERE(os_db(i).ip_steps NE ip_steps, cnt1)
           IF (cnt1 EQ 0) THEN os_num= os_db(i).os_num
         ENDFOR
       ENDIF
    ENDIF 
  ENDIF

  RETURN

END

