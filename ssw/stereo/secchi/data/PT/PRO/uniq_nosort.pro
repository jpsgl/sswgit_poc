;+
;$Id: uniq_nosort.pro,v 1.1.1.2 2004/07/01 21:19:11 esfand Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : UNIQ_NOSORT
;
; Purpose     : Return the subscripts of the unique elements in an array.
;               Does not require array to be sorted (as in UNIQ).
;
; Use         : UNIQ_SORT(Array)
;
; Inputs      : Array  The array to be scanned.  
;
; Opt. Inputs : None
;
; Outputs     : An array of indicies into ARRAY is returned.  The expression:
;               ARRAY(UNIQ_NOSORT(ARRAY))
;               will be a copy of the sorted Array with duplicate elements removed.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: uniq_nosort.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:37  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;
FUNCTION UNIQ_NOSORT, a

   len = N_ELEMENTS(a)
   used = BYTARR(len)
   new = 0
   ind = WHERE(a EQ a(0))
   used(ind) = 1
   FOR i=1, len-1 DO BEGIN
      IF (used(i) EQ 0) THEN BEGIN
         new = [new, i]
         ind = WHERE(a EQ a(i))
         used(ind) = 1
      ENDIF
   ENDFOR

   RETURN, new

END
