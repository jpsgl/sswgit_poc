;+
;$Id: read_ip_tables.pro,v 1.10 2009/09/11 20:28:20 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : READ_IP_TABLES
;               
; Purpose     : Read FSW Image Processing (IP) tables.
;               
; Explanation : The Hex format flight Image Processing (IP) table contains IP steps 
;               used to process an image. There are total of 20 tables that can used
;               by any of 5 telescopes. Note: Great care should be taken not to use
;               a table with a instrument specific step(s) for another telescope
;               (i.e. if a table contains add to HI2 summing buffer, then that table
;               should NOT be used by other telescopes). Each table can have maximum 
;               of 20 steps but planning tool allows a maximum of 26 steps with those
;               not used set to -1. Each line/table of the flight IP file starts with
;               the number of steps for that table followed by that many step numbers.
;               This routine reads in each line/table, coverts the data to base10 and
;               stores them in the plannning tool ip structure.
;
; Use         : READ_IP_TABLES, fn, ip 
;    
; Inputs      : fn       In flight IP tables filename (i.e. '../TABLES/ip_tables.dat'). 
;
; Opt. Inputs : None
;               
; Outputs     : ip       Structure containing 20 tables populated on output. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari - 07/21/04 Use z2 format instead of z4 to read in data.
;              Ed Esfandiari - 12/16/04 Added "raw image" compression as the last step
;                                       if a table for GroundTest APID does not end
;                                       with a compression string as required by PT.
;                                       This is only for PT purposes - FSW assumes 
;                                       "raw image".
;              Ed Esfandiari - 03/09/05 Use elements of ip to check for # of allowed tables.
;              Ed Esfandiari - 04/18/05 Changed 10 steps comment to 20 steps.
;              Ed Esfandiari - 05/25/06 Added HC1 after CADJ (77) and ICER1 after IADJ (102)
;                              so these tables (i.e. table 59 an d60) also end with compressions.
;                                         
;
; $Log: read_ip_tables.pro,v $
; Revision 1.10  2009/09/11 20:28:20  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.6  2005/04/27 20:38:39  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.5  2005/03/10 16:49:16  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO READ_IP_TABLES, fn, ip   

  OPENR,lun, fn, /GET_LUN 
  line= ''
  table= 0
  allowed_tables= N_ELEMENTS(ip)
  WHILE NOT EOF(lun) DO BEGIN
    READF, lun, line
    line= STRTRIM(line,2)
    IF (STRMID(line,0,1) EQ 'X') THEN BEGIN

      ;IF (table GE 20) THEN BEGIN
      ;IF (table GE 40) THEN BEGIN
      IF (table GE allowed_tables) THEN BEGIN
        PRINT,''
        ;PRINT,'ERROR - More than 20 Tables in IP file.'
        ;PRINT,'ERROR - More than 40 Tables in IP file.'
        PRINT,'ERROR - More than '+STRTRIM(allowed_tables,2)+' Tables in IP file.'
        PRINT,''
        STOP 
      ENDIF

      ;print,line
      
      line= STRCOMPRESS(STRMID(line,1,500),/REMOVE_ALL) ; remove X and all blanks.
      nsteps= 0
      ;READS, STRMID(line,0,4), nsteps, format='(z4)'
      READS, STRMID(line,0,4), nsteps, format='(z2)'
      IF (nsteps EQ 0) THEN BEGIN
        PRINT,''
        PRINT,'WARNING - IP table #'+STRTRIM(table+1,2)+' is empty (nsteps= 0000).'
        PRINT,''
      ENDIF ELSE BEGIN
        data= INTARR(nsteps+1) ; each TABLE can have up to 20 steps.
        ;READS, line, data, format= '(' + STRTRIM(nsteps+1,2) + 'z4)'
        READS, line, data, format= '(' + STRTRIM(nsteps+1,2) + 'z2)'
        ip(table).steps(0:nsteps-1)= data(1:*)

        ; If a table does not end with a compression (step# 5 to 17 or 44) and it has a ground
        ; test apid, then add a 44 (raw) to the end of the table. This is only a special case
        ; and all other tables should end with a compression step:

        IF ((data(nsteps) LT 5 OR data(nsteps) GT 17) AND (data(nsteps) NE 44) AND $
            (data(nsteps) LT 90  OR data(nsteps) GT 101)) THEN BEGIN 
          gndtst= WHERE(data EQ 43, gtind)
          IF (gtind GT 0) THEN ip(table).steps(nsteps)= 44
          IF (data(nsteps) EQ 77) THEN ip(table).steps(nsteps)= 9 ;replace CADJ with HC1
          IF (data(nsteps) EQ 102) THEN ip(table).steps(nsteps)= 91 ; replace IADJ with ICER1
        ENDIF

      ENDELSE

      table= table + 1

    ENDIF
  ENDWHILE

  ;PRINT,''
  ;PRINT,'Finished reading '+STRTRIM(table,2)+' IP TABLES.'
  ;PRINT,''

  CLOSE,lun
  FREE_LUN,lun

  RETURN
END
