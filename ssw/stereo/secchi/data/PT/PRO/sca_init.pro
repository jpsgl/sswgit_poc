;+
;$Id: sca_init.pro,v 1.3 2005/01/24 17:56:35 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SCA_INIT
;               
; Purpose     : Define structures used when reading in SAT file (ECS Keyword
;               input to the Activity Plan) for SECCHI Planning Purposes. 
;               
; Use         : SCA_INIT
;    
; Inputs      : None 
;
; Opt. Inputs : None
;               
; Outputs     : None 
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Prev. Hist. : Based on SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: sca_init.pro,v $
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:09  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


PRO SCA_INIT

;COMMON KAP_INIT
COMMON KAP_INIT, kap_nrt_reserved, kap_resource, kap_resource_names, kap_dsn_names, kap_descriptions, $
                 kap_tlm_mode_names, kap_iie

;**
;** kap_nrt_reserved
;** kap_resource
;**
;** the following are not implemented
;**
;** kap_sci_plan
;** kap_program 
;** kap_activity
;** kap_other_obs
;**
;** kap_iie
;** kap_nrt_session 
;** kap_delayed_cmd 
;** kap_tstol_execution 
kap_file_name = ''

kap_nrt_reserved = {kap_nrt_reserved,	$
                    startime:	0D,	$	;** TAI
                    endtime:	0D,	$	;** TAI
                    instrume:	'',	$	;** L, E, C, S, M, ...
                    cmd_rate:	0,	$	;** cmds/minute
                    status:	''      }	;** acceptance status from ECS: Requested,Confirmed,Denied

kap_resource = {kap_resource,		$
                id:		0B,	$
		startime:	0D, 	$	;** TAI
		endtime:	0D, 	$	;** TAI
		type:		0	}
kap_resource_names = [  		$
                        'TLM_MODE',	$;	  N/A		0:IDLE, 1:LR, 2:MR, 3:HR
                        'TLM_SUBMODE',	$;	  N/A		0:?   , 1:MODE 1, 2:MODE 2, 3:MODE 3, 4:MODE 4
                        'TLM_MDI',	$;			0:Magnetogram, 1:H
                        'TLM_HR_IDLE',	$;			N/A
                        'TLM_TAPE_DUMP',$;			N/A
                        'CLOCK_ADJUST',	$;	  N/A		index in description table
                        'SPACECRAFT_MANEUVER',$;		index in description table
                        'THROUGHPUT',	$;			0:No RCR, 1:RCR Allowed
                        'RESERVED',	$;			0:SVM Reserved, 1:Payload Reserved
                        'DSN_CONTACT'	$;			0:?, 1:CAN, 2:MAD, 3:GDS 
                     ]
kap_dsn_names = ['?','CAN','MAD','GDS']
kap_tlm_mode_names = ['IDLE','LR','MR','HR']

kap_iie = {iie,	$
           type:	0,	$	;** 1: Master
           startime:	0D,	$	;** TAI
           endtime:	0D,	$	;** TAI
           instrume:	'',	$	;** L, E, C, S, M, ...
           status:	''      }	;** acceptance status from ECS: Requested,Confirmed,Denied


END
