;+
;$Id: chk_num_doors.pro,v 1.3 2005/01/24 17:56:32 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : CHK_NUM_DOORS
;               
; Purpose     : Checks to see if number of close and open doors scheduled for a telescope
;               are the same. IF not, make them even by adding '3000/01/01' at the end
;               for the missing open dates and '2000/01/01' to the front for missing 
;               close dates.  
;               
; Use         : CHK_NUM_DOORS, tel_close, tel_open 
;    
; Inputs      : tel_close   scheduled door closures for a SCIP telescope.
;               tel_open    scheduled door openings for a SCIP telescope. 
;
; Opt. Inputs : None
;               
; Outputs     : tel_close   scheduled door closures for a SCIP telescope (same numbers as opens)
;               tel_open    scheduled door openings for a SCIP telescope (same numbers as closes) 
;
; Opt. Outputs: None
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/13/2004 Removed sort statements (can't be used for 2 S/C PT).
;
; $Log: chk_num_doors.pro,v $
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

PRO CHK_NUM_DOORS, closes, opens

   IF (DATATYPE(closes) EQ 'DOU' OR DATATYPE(opens) EQ 'DOU') THEN BEGIN
     dif = N_ELEMENTS(closes) - N_ELEMENTS(opens)
     IF (dif GT 0) THEN BEGIN ; more door close than opens, so make them the same by adding '3000' dates
       odt= UTC2TAI(STR2UTC('3000/01/01'))
       IF (DATATYPE(opens) NE 'DOU') THEN $
          opens= odt $
       ELSE $
          opens= [opens,odt]
       FOR i= 1, dif-1 DO opens= [opens,odt]
       ;opens= opens(SORT(opens)) ; SORT just incase
     ENDIF ELSE BEGIN
       IF (dif LT 0) THEN BEGIN ;more door opens than close, so make them the same by adding '2000' dates
         cdt= UTC2TAI(STR2UTC('2000/01/01'))
         IF (DATATYPE(closes) NE 'DOU') THEN $
           closes= cdt $
         ELSE $
           closes= [cdt,closes]
         FOR i= 1, ABS(dif)-1 DO closes= [cdt,closes]
         ;closes= closes(SORT(closes)) ; SORT just incase
       ENDIF

     ENDELSE
   ENDIF

  RETURN
END

