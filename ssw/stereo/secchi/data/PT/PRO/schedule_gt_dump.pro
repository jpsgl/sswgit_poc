;+
;$Id: schedule_gt_dump.pro,v 1.7 2009/09/11 20:28:23 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SCHEDULE_GT_DUMP
;               
; Purpose     : This functions schedules Guide-Telescope Data Dumps. 
;               
; Use         : SCHEDULE_GT_DUMP() 
;    
; Inputs      : None 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 01/21/05 - changed "Add to Schedule" to "Add Checked Entries...".
;              Ed Esfandiari 03/16/06 - Changed apids to RT+SSR1, SSR1, SSR2, etc.
;              Ed Esfandiari 08/22/06 - Added RETAIN=2 to draw widget.
;              Ed Esfandiari 12/28/06 - Added bsf_gt code.
;
; $Log: schedule_gt_dump.pro,v $
; Revision 1.7  2009/09/11 20:28:23  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:10  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;
FUNCTION GT_REFRESH
COMMON LP_GT_DUMPS_SHARE, gtdump
COMMON GT_DUMPS_SHARE

     removed = 0 ; false
     IF (gtdump.gt_cnt GT 0) THEN BEGIN
       keep= WHERE(gtdump.gt_select EQ 1, cnt)
       IF (cnt LT gtdump.gt_cnt) THEN BEGIN
         gtdump.gt_cnt = cnt
         removed= 1 ; true
         IF (cnt GT 0) THEN BEGIN
           stime_gt= stime_gt(keep) 
           etime_gt= etime_gt(keep)
           apid_gt= apid_gt(keep)
           rate_gt= rate_gt(keep) 
           sc_gt= sc_gt(keep)
           bsf_gt= bsf_gt(keep)
         ENDIF ELSE BEGIN
           stime_gt= 0
           etime_gt= 0
           apid_gt= ''
           rate_gt= 0
           sc_gt= ''
           bsf_gt= '' 
         ENDELSE
       END
     END


     IF (DATATYPE(stime_gt) NE 'UND' AND gtdump.gt_cnt GT 0) THEN BEGIN
       srt= SORT(stime_gt)
       stime_gt= stime_gt(srt)
       ;etime_gt= etime_gt(SORT(etime_gt))
       etime_gt= etime_gt(srt)
       apid_gt= apid_gt(srt)
       rate_gt= rate_gt(srt)
       sc_gt= sc_gt(srt)
       bsf_gt= bsf_gt(srt)
     END
     removed= 1 ; true  ; to force a refresh incase there are sorts but not removes.

  RETURN, removed
END




PRO ADD2GT,sc
COMMON LP_GT_DUMPS_SHARE, gtdump
COMMON GT_DUMPS_SHARE

     WIDGET_CONTROL, gtdump.bdate, GET_VALUE= stime & stime = STRTRIM(stime(0),2)
     WIDGET_CONTROL, gtdump.edate, GET_VALUE= etime & etime = STRTRIM(etime(0),2)
     WIDGET_CONTROL, gtdump.rate, GET_VALUE= rate & rate = LONG(rate(0))

     IF (STRLEN(stime) GT 0 AND NOT CHECK_DATE(stime)) THEN BEGIN
       ERASE
       XYOUTS, 255, 5, '** Error in Start Time -  "Add" Is Ignored **', $
               CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1 
       RETURN
     ENDIF
     IF (STRLEN(etime) GT 0 AND NOT CHECK_DATE(etime)) THEN BEGIN
       ERASE
       XYOUTS, 255, 5, '** Error in Stop Time - "Add" Is Ignored **', $
               CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1
       RETURN
     ENDIF
 

     IF (STRLEN(stime) EQ 0 OR STRLEN(etime) EQ 0) THEN BEGIN
       ERASE
       XYOUTS, 255, 5,'** Must provide Start and Stop Times  - "Add" Is Ignored **', $
               CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1
       RETURN
     ENDIF

     IF (STRLEN(stime) EQ 0) THEN stime= '2000/01/01'
     IF (STRLEN(etime) EQ 0) THEN etime= '3000/01/01'

     stime = UTC2TAI(STR2UTC(stime))
     etime = UTC2TAI(STR2UTC(etime))

     IF (etime LE stime) THEN BEGIN
       ERASE
       XYOUTS, 255, 5, 'Stop Time MUST be greater than the Start time - "Add" Is Ignored', $
               CHARSIZE=1.3, ALIGNMENT= 0.5, /DEVICE, COLOR= 1

       RETURN
     ENDIF


     IF (DATATYPE(stime_gt) EQ 'UND' OR gtdump.gt_cnt EQ 0) THEN BEGIN
       stime_gt= stime
       etime_gt= etime
       apid_gt= gtdump.apids[gtdump.apid_select]
       rate_gt= rate
       sc_gt= sc 
       bsf_gt= '' 
     ENDIF ELSE BEGIN
       stime_gt= [stime_gt,stime]
       etime_gt= [etime_gt,etime]
       sc_gt= [sc_gt,sc]
       apid_gt= [apid_gt,gtdump.apids[gtdump.apid_select]]
       rate_gt= [rate_gt,rate]
       bsf_gt= [bsf_gt,'']
     ENDELSE
  
     WIDGET_CONTROL, /DESTROY, gtdump.base1
     stat = SCHEDULE_GT_DUMP() ; call itself again (recursively) to reflect the change.

  RETURN
END




PRO SCHEDULE_GT_DUMP_EVENT, event

COMMON LP_GT_DUMPS_SHARE, gtdump
COMMON GT_DUMPS_SHARE

igt= WHERE(gtdump.gt_rows EQ event.id, cnt) 
IF (cnt GT 0) THEN igt= igt(0) & igt= 0 > igt

CASE (event.id) OF

   gtdump.dismiss : BEGIN       ;** exit program
      gtdump.scheduled= 0 ; false 
      WIDGET_CONTROL, /DESTROY, gtdump.base1
   END

   gtdump.refresh : BEGIN
     IF (GT_REFRESH()) THEN BEGIN
       WIDGET_CONTROL, /DESTROY, gtdump.base1
       stat = SCHEDULE_GT_DUMP() ; call itself again (recursively) to reflect the change.
     END
   END

;   gtdump.bdate : BEGIN
;
;   END

;   gtdump.edate : BEGIN
;
;   END

   gtdump.apid   : BEGIN
     ;help,event.index
     gtdump.apid_select= event.index
   END

   gtdump.gt_rows(igt) : BEGIN
     gtdump.gt_select(igt)= event.select
   END


   gtdump.add_plan_ab : BEGIN
     ADD2GT,'AB'
   END

   gtdump.add_plan_a : BEGIN
     ADD2GT,'A'
   END

   gtdump.add_plan_b : BEGIN
     ADD2GT,'B'
   END

   gtdump.schedule : BEGIN

     gtdump.scheduled= 1 ; true 
    
     stat= GT_REFRESH() 

     WIDGET_CONTROL, /DESTROY, gtdump.base1
   END

   ELSE : BEGIN
   END

ENDCASE

END

 

FUNCTION SCHEDULE_GT_DUMP 

COMMON LP_GT_DUMPS_SHARE, gtdump
COMMON GT_DUMPS_SHARE

    IF XRegistered("SCHEDULE_GT_DUMP") THEN RETURN, 0   ; return false, AEE - 5/30/03 


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

      font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

      base1 = WIDGET_BASE(/COLUMN, TITLE=' Schedule Guide Telescope Data Dumps', /FRAME)

      base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)
      row0 = WIDGET_BASE(base1, /ROW)

        col = WIDGET_BASE(row0, /COLUMN, /FRAME)
     
        row = WIDGET_BASE(col, /COLUMN, /FRAME)

        row1= WIDGET_BASE(row, /ROW, /FRAME)
        temp= WIDGET_LABEL(row1, VALUE='  Current Schedule Range:            '+ $
                           beg_sched+' - '+end_sched, FONT=font) 

        spc= '                   '
        col = WIDGET_BASE(col, /COLUMN, /FRAME)
        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc+'Enter New GT Data Dump Start Time, Stop Time, APID, and Rate', FONT=font)

        spc=''
        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc+'             Start Time                                 Stop Time', $
                           FONT=font)
        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc+'   yyyy/mm/dd hh:mm:ss          yyyy/mm/dd hh:mm:ss')

        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc)
        rr= WIDGET_BASE(row, /ROW, /FRAME)
        r3= WIDGET_BASE(rr, /ROW)
        ;start='yyyy/mm/dd mm:ss:ss'
        start=''
        bdate= WIDGET_TEXT(r3, /EDITABLE, YSIZE=1, XSIZE=20, VALUE=STRTRIM(STRING(start),2))
        temp= WIDGET_LABEL(row, VALUE='   ')
        rr= WIDGET_BASE(row, /ROW, /FRAME)
        r5= WIDGET_BASE(rr, /ROW)
        edate= WIDGET_TEXT(r5, /EDITABLE, YSIZE=1, XSIZE=20, VALUE=STRTRIM(STRING(start),2))

        rr= WIDGET_BASE(row, /ROW, /FRAME)
        r6= WIDGET_BASE(rr, /ROW)
        ;apids= ['SSR1','GndTest']
        ; AEE 4/30/04 - added all 5 APIDs for GT:
        ;apids= ['SSR1','SSR2','RealTime','SpaceWthr','GndTest']
        ;apids= ['RT-SSR1','SSR1','SSR2','SpaceWthr','GndTest']
        apids= ['RT-SSR1']
        apid_select= 0
        temp= WIDGET_LABEL(r6, VALUE='APID: ', FONT=font)
        apid= CW_BSELECTOR2(r6,apids, SET_VALUE=0, UVALUE="GET_APID")

        rr= WIDGET_BASE(row, /ROW, /FRAME)
        r8= WIDGET_BASE(rr, /ROW)
        temp= WIDGET_LABEL(r8, VALUE='Rate (bps):', FONT=font)
        gtrate= 39100  ;default bps         
        rate= WIDGET_TEXT(r8, /EDITABLE, YSIZE=1, XSIZE=6, VALUE=STRTRIM(STRING(gtrate),2))

        spc= '         '
        row = WIDGET_BASE(col, /ROW)
        rr= WIDGET_BASE(row, /ROW, /FRAME)
         spc= '                                                  '
        ;add_gt = WIDGET_BUTTON(rr, VALUE= spc+'Add To The Plan'+spc, FONT=font)
        r7= WIDGET_BASE(rr, /ROW)
        temp= WIDGET_LABEL(r7,VALUE=' ')
        add_plan_ab= WIDGET_BUTTON(r7, VALUE= 'Add to Plan for S/C A+B', FONT=font)
        temp= WIDGET_LABEL(r7,VALUE='  ')
        add_plan_a = WIDGET_BUTTON(r7, VALUE= 'Add to Plan for S/C A', FONT=font)
        temp= WIDGET_LABEL(r7,VALUE='  ')
        add_plan_b = WIDGET_BUTTON(r7, VALUE= 'Add to Plan for S/C B', FONT=font)
        temp= WIDGET_LABEL(r7,VALUE=' ')

        row = WIDGET_BASE(col, /ROW)
        rr= WIDGET_BASE(row, /ROW) 
        blanks= '                                                               '
        !p.background= 170 ; gray,  originally= 0 (black)
        msg1= WIDGET_DRAW(rr, XSIZE=510, YSIZE=20, RETAIN=2)



        col = WIDGET_BASE(row0, /COLUMN, /FRAME)
        row = WIDGET_BASE(col, /COLUMN)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='Guide Telescope Data Dump Scheduling Notes:', FONT=font)
        row = WIDGET_BASE(col, /COLUMN)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='1. Use the menu on the left to plan new GT data dumps:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * Enter Start Time, End Time, and select an APID.', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * Press "Add to Plan for S/C A+B, A, or B".',FONT=font) 
        row = WIDGET_BASE(col, /COLUMN)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='2. Use the menu below to view, verify, delete, or schedule planned GT dumps:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * Press on the "Refresh" Button to:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       A.  Remove any toggled-off times.', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       B.  SORT the remaining Start and Stop times (separately).', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * Press on the "Add to Schedule" Button to:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       A.  Perform a "Refresh".', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       B.  Schedule sorted GT dumps that remain toggled-on.', FONT=font)


        ; *****

      temp= WIDGET_LABEL(base1, VALUE='  ')

      base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)

      schedule = WIDGET_BUTTON(base2, VALUE='Add Checked Entries, Below, to Schedule', FONT=font)

      col = WIDGET_BASE(base2, /ROW, X_SCROLL_SIZE= 1020, Y_SCROLL_SIZE= 150, /FRAME)
 
      row = WIDGET_BASE(col, /COLUMN, /FRAME) 
      temp= WIDGET_LABEL(row, VALUE='Planned GT Data Dumps', FONT=font)

      gt_cnt= 0 
      gt_rows= 0L
      gt_select= 0B
      IF (DATATYPE(stime_gt) NE 'UND') THEN BEGIN
       IF (stime_gt(0) NE 0) THEN BEGIN
        gt_cnt= N_ELEMENTS(stime_gt)
        gt_rows= LONARR(gt_cnt)
        gt_select= BYTARR(gt_cnt)
        gt_btimes= LONARR(gt_cnt)
        gt_etimes= LONARR(gt_cnt)
        gt_apids= STRARR(gt_cnt)
        gt_rates= LONARR(gt_cnt)
        gt_scs = STRARR(gt_cnt)
        FOR i=0, gt_cnt-1 DO BEGIN 
          row1 = WIDGET_BASE(row, /ROW)
          col1 = WIDGET_BASE(row1, /COLUMN)
          row2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE)
          gt_rows(i)= WIDGET_BUTTON(row2, VALUE=' ')
         
          time= UTC2STR(TAI2UTC(stime_gt(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '2000') THEN time = '        <--'
          gt_btimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=20, VALUE=time)

          time= UTC2STR(TAI2UTC(etime_gt(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '3000') THEN time = '        -->'
          gt_etimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=20, VALUE=time)

          ;gt_apids(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=10, VALUE=apid_gt(i))
          gt_apids(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=12, VALUE=apid_gt(i)+' '+sc_gt(i))
          ;gt_scs(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=4, VALUE=sc_gt(i))

          srate= STRTRIM(rate_gt(i),2)
          gt_rates(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=6, VALUE=srate)

          IF (bsf_gt(i) NE '') THEN BEGIN
            WIDGET_CONTROL,row1, SENSITIVE=0 
          ENDIF
        END
       END
      END

      blanks='        Check/Correct/Remove Door CLOSE/OPEN times. "Refresh" updates the display with the latest info. "Add to Schedule" schedules the displayed door closures and opens.        ' 
      col = WIDGET_BASE(base2, /ROW)

     refresh = WIDGET_BUTTON(base2, VALUE=' Refresh ', FONT=font)
     dismiss = WIDGET_BUTTON(base2, VALUE=' Dismiss ', FONT=font)

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base1
    WIDGET_CONTROL, bdate, /INPUT_FOCUS

    FOR i= 0, gt_cnt-1 DO BEGIN
      WIDGET_CONTROL, gt_rows(i), SET_BUTTON = 1 
      gt_select(i)= 1
    END

    gtdump = CREATE_STRUCT( 'base1', base1,                $
                            'gt_rows', gt_rows,        $
                            'gt_select', gt_select,    $
                            'gt_cnt', gt_cnt,          $
                            'msg1', msg1              ,    $
                            'dismiss', dismiss        ,    $
                            'scheduled', 0            ,    $
                            'refresh', refresh        ,    $
                            'bdate', bdate            ,    $
                            'edate', edate            ,    $
                            'apid', apid              ,    $
                            'rate', rate              ,    $
                            'apids', apids            ,    $
                            'apid_select', apid_select,    $
                            'add_plan_ab', add_plan_ab,    $
                            'add_plan_a', add_plan_a,      $
                            'add_plan_b', add_plan_b,      $
                            'schedule', schedule)

    WIDGET_CONTROL, base1, SET_UVALUE= gtdump

    txt= 'Enter Start/Stop Times, Select APID, and Add to the Plan'  

    ERASE

    XYOUTS, 255, 5, txt, CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 250

    XMANAGER, "SCHEDULE_GT_DUMP", base1, /MODAL  ; AEE - Mar 20, 03

  !p.background= 0 ; originally= 0

  RETURN,gtdump.scheduled

END

