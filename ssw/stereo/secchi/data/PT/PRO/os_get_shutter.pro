;+
;$Id: os_get_shutter.pro,v 1.3 2005/01/24 17:56:34 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_GET_SHUTTER
;               
; Purpose     : Returns 0 if shutter is open and 1 if closed.
;               
; Use         : status = OS_GET_SHUTTER(os)
;    
; Inputs      : os       Observing sequence. 
;
; Opt. Inputs : None
;               
; Outputs     : status   Shutter - 0:Open, 1:Closed 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 11/17/04 updated LEDs and also comments.
;
; $Log: os_get_shutter.pro,v $
; Revision 1.3  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;

FUNCTION OS_GET_SHUTTER, os

   shutter = 0 ; 0=open, 1=closed

   ; AEE 1/30/04 - This routine returns whether shutter is left closed while taking an image and
   ; is only used as part of the tel_state_str outputed to .IPT file and not used by SECCHI .RTS or .ATS
   ; files. It is not used anywhere else and not needed but I updated it anyway.
   ; Also note that other characters in tel_state_str, cam_state_str, etc. of the .IPT file may also 
   ; need to be updated for secchi if they are used some day.

   ; Note: HI is shutterless, so leave shutter at 0

   IF (os.lp EQ 2 AND os.tele LE 2) THEN $
     shutter = 1 ;closed for DARK SCIP images (no shutter for HI) - AEE 1/30/04

   IF (os.lp EQ 3) THEN BEGIN
     ; For cal LED images, shutter is also left closed for the FPA mounted LEDs: 

     ; COR2 SCIP_BLU_LED and SCIP_PRP_LED (lamp=1,2) are FPA mounted:
     IF (os.tele EQ 2 AND os.lamp GT 0) THEN shutter = 1 

     ; all COR1 LEDS (lamp=0,1,2) are FPA mounted so shutter remains closed:
     IF (os.tele EQ 1) THEN shutter = 1

     ; EUVI SCIP_RED_LED (lamp=0) is also FPA mounted and remain closed:
     IF (os.tele EQ 0 AND os.lamp EQ 0) THEN shutter = 1
   ENDIF

   RETURN, shutter

END
