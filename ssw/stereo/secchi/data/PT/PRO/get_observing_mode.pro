;+
;$Id: get_observing_mode.pro,v 1.10 2011/06/30 20:40:08 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GET_OBSERVING_MODE
;               
; Purpose     : Generates an obs_mode structure containing various image parameters
;               listed below. 
;               
; Use         : mode = GET_OBSERVING_MODE(os, index, /DOORCLOSED) 
;    
; Inputs      : os          Observating sequence structure. 
;               index       Index into the polarizer wheel/sector for the image.  
;          
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    :  DOORCLOSED IF set, set door in tel_state_str to 2, otherwise set to 0. 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/01/04 Used local table definintion.
;              Ed Esfandiari 01/28/05 Added 32 bits/pix summing and new method to calculate
;                                     IP time for the image.
;              Ed Esfandiari 10/06/05 Added code to handle IP functions 72 and 73.
;              Ed Esfandiari 11/21/05 Added code for new steps ?T (114) and ?U (115) and for 
;                                     modified step orders (?l and ?m).
;              Ed Esfandiari 08/16/06 Use step numbers instead of step compr_str to pickup
;                                     bits_per_pixel. Also use Simon's 14 and 21 bits_per_pix
;                                     instead of 16 and 32 for non-summed and summed images.
;              Ed Esfandiari 02/06/07 Used 16 and 32 bits/pixel since comp_factor is based on 16 bits/pix.
;
;
; $Log: get_observing_mode.pro,v $
; Revision 1.10  2011/06/30 20:40:08  nathan
; fix hdr-only case
;
; Revision 1.9  2009/09/11 20:28:12  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.4  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.3  2005/03/10 16:44:44  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.2  2004/09/01 19:10:42  esfand
; Removed init_db and get_db struct pros.
;
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION GET_OBSERVING_MODE, os, index, DOORCLOSED=doorclosed

COMMON DBMODES_COMMON, dbmodes
COMMON OS_SHARE

;input: index, which pw for SEQ PW
;input: OS_INSTANCE structure (ex: from defined_os_arr)
;** Structure OS_INSTANCE, 15 tags, length=12100:
;   OS_NUM          LONG              1695
;   LP              INT              4
;   TELE            INT              1
;   TABLE           INT              2
;   IPTABLE         INT             16
;   FW              INT              3
;   PW              INT       Array(5)
;   LAMP            INT              0
;   NUM_IMAGES      INT              4
;   CCD             STRUCT    -> CCDVARS Array(4, 3)
;   IP              STRUCT    -> IPVARS Array(20)
;   EX              INT       Array(4, 3, 5, 5)
;   OCC_BLOCKS      BYTE      Array(4, 1024)
;   ROI_BLOCKS      BYTE      Array(4, 1024)
;   FPWL            STRUCT    -> FPWL1 Array(3, 5, 10)
;
;output: obs_mode - structure containing  observing mode
;** Structure OBSERVING_MODES, 11 tags, length=68:
;   DB_NAME         STRING    'lasco'
;   TABLE_NAME      STRING    'observing_modes'
;   DATE_MOD        STRING    ''
;   TELESCOPE       INT              0
;   TEL_STATE_STR   STRING    ''
;   CAM_STATE_STR   STRING    ''
;   COMPRESSION_STR STRING    ''
;   AVG_PROC_TIME   FLOAT           0.00000
;   AVG_COMP_FACTOR FLOAT           0.00000
;   AVG_BITS_PIXEL  FLOAT           0.00000
;   NUM_IMAGES      LONG                 0
;
;

   obs_mode =  $
   {obs_modes, $
   DB_NAME         :    'secchi', $
   TABLE_NAME      :    'observing_modes',$
   DATE_MOD        :    '',$
   TELESCOPE       :     0,$
   TEL_STATE_STR   :    '',$
   CAM_STATE_STR   :    '',$
   COMPRESSION_STR :    '',$
   AVG_PROC_TIME   :   0.0,$
   AVG_COMP_FACTOR :   0.0,$
   AVG_BITS_PIXEL  :   0.0,$
   NUM_IMAGES      :   0}

   ;**
   ;** FILL IN OBSERVING_MODE STRUCTURE
   ;**

   obs_mode.telescope = os.tele

   ;**
   ;** build up telescope state string
   ;**
   shutter = OS_GET_SHUTTER(os)

   ; AEE 8/22/03 - commented above stuff and added these (lamp,fw,pw):
   lamp= os.lamp
   fw = os.fw
   pw = os.pw

   IF (KEYWORD_SET(DOORCLOSED)) THEN door = 2 ELSE door = 0

   obs_mode.tel_state_str = STRTRIM(shutter,2) + $
                            STRTRIM(fw,2) + $
                            STRTRIM(pw(index),2) + $
                            STRTRIM(door,2) + $
                            STRTRIM(lamp,2)

;** AEE 9/23/03 - appended the second pw for double-images lp=0) - make sure it is needed and is in right
; place. The tel_state_str, among other things, is used below to find matching statistics from a database.
; It is NOT used outside of this routine. **

   IF (os.lp EQ 0) THEN obs_mode.tel_state_str = obs_mode.tel_state_str +STRTRIM(pw(1),2) 

   ;**
   ;** build up camera state string
   ;**

   ; 4/5/04 - AEE the cam_state_str is not used anywhere bt software and may need to be removed.

   ;obs_mode.cam_state_str = STRTRIM(STRING(os.ccd(os.tele,os.camtable).ysum,FORMAT='(I4.4)'),2) + $
   ;                         STRTRIM(STRING(os.ccd(os.tele,os.camtable).xsum,FORMAT='(I4.4)'),2) + $
   ;                         STRTRIM(STRING(os.ccd(os.tele,os.camtable).clr_id,FORMAT='(I1.1)'),2) + $
   ;                         STRTRIM(STRING(os.ccd(os.tele,os.camtable).rdout_id,FORMAT='(I2.2)'),2) + $
   ;                         '1' + $    ;** Dump Speed
   ;                         STRTRIM(STRING(os.ccd(os.tele,os.camtable).pform,FORMAT='(I1.1)'),2)


   ;**
   ;** build up compression string
   ;**
   comp_str = GET_COMP_STR(os)
   obs_mode.compression_str = comp_str

;   obs_mode.avg_bits_pixel = 16 ; AEE - Dec 19, 02 - 16 bits/pixel for all SECCHI CCDs (according to D.Wang)
;   obs_mode.avg_proc_time =  5.5 ;AEE - Dec 19, 02 - use 5.5 sec/Mpixel for SECCHI (it is for lossless comp
;                                 ; resion but use it for all compressions for now.
;   obs_mode.avg_proc_time =  obs_mode.avg_proc_time / 1000.0 ; change to milliseconds per pixel

   ; For any image involving  a summing buffer (add to or use as output), there are 32 bits/pix (also
   ;  (an implicit pixel summing is assumed):

;   IF (STRPOS(comp_str,'I') GE 0 OR STRPOS(comp_str,'K') GE 0 OR $
;       STRPOS(comp_str,'L') GE 0 OR STRPOS(comp_str,'M') GE 0) THEN BEGIN

;   IF (STRPOS(comp_str,'I') GE 0 OR STRPOS(comp_str,'K') GE 0 OR $
;       STRPOS(comp_str,'L') GE 0 OR STRPOS(comp_str,'M') GE 0 OR $
;       STRPOS(comp_str,'?T') GE 0 OR STRPOS(comp_str,'?U') GE 0) THEN BEGIN
;     obs_mode.avg_bits_pixel = 32
;   ENDIF ELSE BEGIN
;     obs_mode.avg_bits_pixel = 16
;   ENDELSE


   steps= os.ip(os.iptable).steps
   sumind= WHERE(steps EQ 34 OR steps EQ 36 OR steps EQ 37 OR steps EQ 38 OR $
                 steps EQ 114 OR steps EQ 115, sumcnt)
    
    hdrip= WHERE(steps EQ 5,hdronly)
    
    IF (hdronly) THEN obs_mode.avg_bits_pixel = 0 ELSE $
    IF (sumcnt GT 0) THEN obs_mode.avg_bits_pixel = 32 ELSE $
     ;obs_mode.avg_bits_pixel = 21 
    obs_mode.avg_bits_pixel = 16
     ;obs_mode.avg_bits_pixel = 14

   ; IF an HI summing buffer to 16 bit (from 32), functions 72 (?l) or 73 (?m), is used,
   ; it only reduces image size by 2 since 32 bits/pixel image becomes 16 bits/pix so
   ; reset avg_bits_pixel to 16 because it is used elsewhere to generate image size:

   ;IF (STRPOS(comp_str,'?d') GE 0 OR STRPOS(comp_str,'?e') GE 0) THEN obs_mode.avg_bits_pixel = 16
;   IF (STRPOS(comp_str,'?l') GE 0 OR STRPOS(comp_str,'?m') GE 0) THEN obs_mode.avg_bits_pixel = 16

    to16= WHERE(steps EQ 80 OR steps EQ 81, to16_ind)
    IF (to16_ind GT 0) THEN obs_mode.avg_bits_pixel = 16
    ;IF (to16_ind GT 0) THEN obs_mode.avg_bits_pixel = 14

   obs_mode.avg_proc_time = GET_IP_TIME(os)

   RETURN, obs_mode

END
