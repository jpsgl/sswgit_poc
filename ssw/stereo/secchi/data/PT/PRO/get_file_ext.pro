;+
;$Id: get_file_ext.pro,v 1.2 2004/09/01 15:40:43 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : GET_FILE_EXT
;
; Purpose     : This function returns a SECCHI science filename extension. 
;
; Use         : ext= GET_FILE_EXT(sc, apid, tel)
;
; Inputs      : sc    'A' or 'B'.
;               apid  Destination of the image.
;               tel   Telescope used to take the image.
;
; Opt. Inputs : None
;
; Outputs     : ext   A 3 charecter string based on sc, apid, and telescope. 
;
; Opt. Outputs: None
;
; Keywords    :
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;               Ed Esfandiari 8/16/04 - used new .APT per FSW operations overview doc.
;
; $Log: get_file_ext.pro,v $
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION GET_FILE_EXT, sc, chan, tel
;
; INPUTS:
;  sc   = 'A' or 'B'
;  chan = 'SSR1', 'SSR2', 'RT', 'SW', or 'GT' or 'S1RT'   (note GT is for GroundTest.)
;  tel  = 0, 1, 2, 3, or 4 (for EUVI, COR1, COR2, HI1, and HI2)

; 3/24/04 - New APID ranges are (no longer S/C related):
;
;   0x400 - 0x43F  Real-Time
;   0x400 - 0x44F  SSR1        => (0x440-0x44F for SSR1 only)
;   0x450 - 0x46F  SSR2
;   0x470 - 0x47F  Space Weather
;
; For extension, use the last two digits of APIDs. For example, use '3F' for '43F'.
;
; Notice that there is no Real-Time only APID range anymore (Real-Time and SSR1 share
; 0400 - 043F range). This means that we cannot send data real-time only. Everything 
; we send down real-time will also go to SSR1. So, everyyhing in the 0400-043F goes
; to both RT and SSR1. We no longer have RT only, "Use Real-Time APID", image-processing step.
; To send data only to SSR1, we must use the 0440-044F APID range.
; The S/C is also no longer used but for PT purposes I use the first APID address to denote S/C A 
; and the second APID address to denote S/C B. For exampe, For SSR1 only data, use '40' for S/C A
; and '41' for S/C B.

  IF (STRUPCASE(chan) EQ 'RT') THEN BEGIN
    PRINT,''
    PRINT,'"Use Real-Time APID" is no longer a valid IP step/function. It should not be used.'
    PRINT,'"using the extension for SSR1RT instead.'
    PRINT,''
    chan= 'S1RT' 
    STOP
  ENDIF

  CASE STRUPCASE(chan) of
    'S1RT': ext= '00'
    'SSR1': ext= '40'
    'SSR2': ext= '50'
    'SW'  : ext= '70'
    'GT'  : ext= 'GT' ; for GroundTest (not part of APID range).
  ENDCASE

; Use the following to identify the telescopes by Spacecraft (T in .APT):
;
;         EUVI   COR1   COR2    HI1    HI2
; S/C A:   0      1      2       3      4
; S/C B:   5      6      7       8      9
;
;  t= tel
;  IF (STRUPCASE(sc) EQ 'B') THEN  t= tel+5

; use new T (in .APT):
;
;         EUVI   COR1   COR2    HI1    HI2
; S/C A:   3      2      1       4      5 
; S/C B:   8      7      6       9      0 

  a_tel= [3,2,1,4,5]
  b_tel= [8,7,6,9,0]
  IF (STRUPCASE(sc) EQ 'A') THEN $
    t= a_tel(tel) $
  ELSE $ 
    t= b_tel(tel) 
  
  RETURN, ext+STRTRIM(t,2)

END

