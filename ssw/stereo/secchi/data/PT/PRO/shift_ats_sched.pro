FUNCTION SHIFT_ATS_SCHED, in_ats,new_date

  ; in_ats= '/home/esfand/secchi/PT1/OUT/SC_B/UPLOAD/SEC20070328004.IPT.ATS'
  ; new_date= '20070402'
  
next: 
  v= 0 
  out_ats= STRMID(in_ats,0,STRPOS(in_ats,'SEC200'))+'shifted_ats/SEC'+new_date+STRING(v,'(I3.3)')+'.IPT.ATS'
  f= FINDFILE(out_ats)
  IF (f(0) NE '') THEN BEGIN
    v= v+1
    GOTO, next
  ENDIF
  PRINT,'Generating '+out_ats+' file.......'

  yr= STRMID(new_date,0,4)
  y10up= ['A','B','C','D','E','F','G','H','I','J', 'K','L','M','N','O']
  launch_date= 2006
  IF (FIX(yr) - launch_date LT 10) THEN $
    yr= STRMID(yr,3,1) $   ; '0' to '9'
  ELSE $
    yr= y10up(FIX(yr) - launch_date - 10)  ; 'A' to 'O'

  nd= yr+STRMID(new_date,4,4)

  OPENR,in,in_ats,/GET_LUN
  OPENW,out,out_ats,/GET_LUN
  cmd=''
  WHILE (NOT EOF(in)) DO BEGIN
    READF,in,cmd
    IF (STRPOS(cmd,'#') EQ 0 OR STRPOS(cmd,'---') EQ 0) THEN BEGIN
      ; copy comments (starting with #) and Begin and End commands (starting with ---) to output file.
      PRINTF,out,cmd
    ENDIF ELSE BEGIN
      ncmd= nd+STRMID(cmd,5,100)
      b4_byte_count= STRMID(ncmd,0,12)
      af_byte_count= STRMID(ncmd,16,100)
      byte_cnt= strlen(b4_byte_count)+strlen(af_byte_count)+4
      b4_byte_count= b4_byte_count+INT2STR(byte_cnt,2)
      cmdstr= b4_byte_count+af_byte_count
      n= STRLEN(cmdstr)
      chksum= BYTARR(n)
      FOR j=0, n-1 DO chksum(j)= BYTE(STRMID(cmdstr,j,1))
      b4_byte_count= b4_byte_count+STRUPCASE(STRING(TOTAL(chksum) MOD 256,'(z2.2)'))
      PRINTF,out,b4_byte_count+af_byte_count
    ENDELSE
  ENDWHILE

  CLOSE,in
  CLOSE,out
  FREE_LUN,in
  FREE_LUN,out

  RETURN, 'Generated '+out_ats
END

