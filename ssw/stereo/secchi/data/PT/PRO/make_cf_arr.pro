;+
;$Id: make_cf_arr.pro,v 1.3 2009/09/11 20:28:13 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : MAKE_CF_ARR 
;
; Purpose     : Reads comp_factors.dat and geneates cf_full and cf_ipsum arrays. 
;
; Use         : MAKE_CF_ARR, filename, cf_full, cf_ipsum 
;
; Inputs      : filename  STRING - comp_factors.dat 
;
; Opt. Inputs : None
;
; Outputs     : cf_full   FLOAT(5,27)  comp factor array used for image that are not IP-summed.
;             : cf_ipsum  FLOAT(5,27)  comp. factor array used for IP-summed images.
;
; Opt. Outputs: None
;
; Keywords    :
;
; Written by  : Ed Esfandiari, NRL, Feb 2007 - First Version.
;
; Modification History:
;
; $Log: make_cf_arr.pro,v $
; Revision 1.3  2009/09/11 20:28:13  esfand
; use 3-digit decimals to display volumes
;
;
;-
;

PRO MAKE_CF_ARR, filename, cf_full, cf_ipsum

 ;The comp_factors.dat should contain 27 lines:
 ;  line   0    HdrOnly
 ;  line   1    NoComp 
 ;  line   2    Rice
 ;  lines  3-14 HC00 - HC11
 ;  lines 15-26 IC00 - IC11
 ;And each line must contains 10 factors for:
 ;        -------not ipsummed---------   -----------ip summed--------
 ;        EUVI, COR1, COR2, HI-1, HI-2,  EUVI, COR1, COR2, HI-1, HI-2

 OPENR, lu, filename, /GET_LUN
  str = ''
  ;cf_full=  FLTARR(5,27) 
  ;cf_ipsum= FLTARR(5,27)  
  cf_full=  FLTARR(5,25) ; ignore HC10 and HC11 (not IP functions)
  cf_ipsum= FLTARR(5,25) ; ignore HC10 and HC11 (not IP functions)
  maxline= N_ELEMENTS(cf_full)
 
  line= -1
  WHILE NOT(EOF(lu)) DO BEGIN
    READF, lu, str
    ; throw away comment (;) and blank lines.
    IF (STRMID(STRTRIM(str,2),0,1) NE ';' AND STRLEN(STRTRIM(str,2)) NE 0) THEN BEGIN
      line= line+1
      cf= (STRTRIM(STR_SEP(str,'='),2))(1)
      cf= FLOAT(STR_SEP(cf,','))
      IF (line GT maxline OR N_ELEMENTS(cf) NE 10) THEN BEGIN
        PRINT,''
        PRINT,'ERROR:'
        PRINT,filename+' does not contain '+STRTRIM(maxline,2)+' lines  or line does not have exactly 10 compression factors.'
        PRINT,str
        PRINT,''
        STOP 
      ENDIF
      cf_full(*,line)= cf(0:4)
      cf_ipsum(*,line)= cf(5:9)
    ENDIF
  ENDWHILE
  CLOSE, lu
  FREE_LUN, lu

  ;help,cf_full,cf_ipsum

END
