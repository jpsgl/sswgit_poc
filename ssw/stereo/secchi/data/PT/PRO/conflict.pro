;+
;$Id: conflict.pro,v 1.4 2013/01/09 13:33:23 mcnutt Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : CONFLICT 
;
; Purpose     : Check to see if the OS instance to be scheduled causes any conflicts with 
;               other OS times already scheduled.
;
; Use         : result = CONFLICT(os_arr,os,camera,conf_info) 
;
; Inputs      : os_arr  An array of all of already scheduled OSs (assumed to be conflict free). 
;                       If not defined (i.e. nothing is currently scheduled), does not do anything.
;               os      The new OS to be added to os_arr is checked for conflicts.
;               camera  camera number of new OS.
;
; Opt. Inputs : None
;
; Outputs     : conf_info  A string array of found conflicts. For each conflict, the os_num 
;                          causing the conflict plus its start and end dates is added to the array. 
;               result     This function returns 0 (for no conflicts) or 1 (for one or more conflicts).
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Calls       : None
;
; Common      : None
;
; Restrictions: This routine assumes all of the existing OSs in os_arr are conflict free. It
;               only checks for possible conflicts of the new os and those in the os_arr.
;
; Category    : Planning, Scheduling.
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/24/04 - distinguish setup and ccd conflicts for the same telescope.
;              Ed Esfandiari 10/20/04 - Added S/C A B dependency.
;              Ed Esfandiari 11/30/04 - IDL's STRING function can only handle data array of up
;                                       to 1024 elements when using explicit formatting. Replaced
;                                       some STRING calls with my INT2STR function, instead.
;
;
; $Log: conflict.pro,v $
; Revision 1.4  2013/01/09 13:33:23  mcnutt
; changed made when generating new block sched 20130108
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

FUNCTION CONFLICT,tos_arr,tos_arr_cameras,os,os_camera,conf_info


; Each element of os_arr is a struture of this form:
;   OS_NUM          LONG
;   OS_START        DOUBLE 
;   OS_STOP         DOUBLE
;   OS_SIZE         LONG
;   OS_DURATION     DOUBLE

    conf= 0       ; => no conflict
    conf_info=''

    os_arr= -1
    os_arr_cameras= -1
    IF (DATATYPE(tos_arr) EQ 'STC') THEN BEGIN
      IF (os.sc EQ 'AB') THEN BEGIN
        os_arr= tos_arr
        os_arr_cameras= tos_arr_cameras 
      ENDIF ELSE BEGIN
        ind= WHERE(tos_arr.sc EQ os.sc OR tos_arr.sc EQ 'AB', icnt) 
        IF (icnt GT 0) THEN BEGIN
          os_arr= tos_arr(ind)
          os_arr_cameras= tos_arr_cameras(ind)
        ENDIF
      ENDELSE
    ENDIF

    IF (DATATYPE(os_arr) NE 'STC') THEN RETURN,conf

    starts=os_arr.os_start
    stops=os_arr.os_stop
    ccd_readouts= os_arr.os_ro_time   ; AEE 8/11/03
    pre_procs= os_arr.os_pre_proc_time ; AEE 8/11/03
    os_nums=os_arr.os_num 
    sc= os_arr.sc
    os_cnt= n_elements(starts)
    srt_ind=SORT(starts)
    os_nums= os_nums(srt_ind)
    sc= sc(srt_ind)
    starts= starts(srt_ind)
    stops= stops(srt_ind)
    ccd_readouts= ccd_readouts(srt_ind)  ; AEE 8/11/03
    pre_procs= pre_procs(srt_ind)  ; AEE 8/11/03
    os_arr_cameras= os_arr_cameras(srt_ind)
    
    ; For all cameras (within the same package), use he ccd_readout times for CCD conflict 
    ; checking. Also for the same camera (within the same package) use the pre_processing times
    ; (setup+exptime) to check for setup conflicts.

    snum_conf= 0
    same= WHERE(os_arr_cameras EQ os_camera,scnt)

    IF(scnt GT 0) THEN BEGIN
      sstops= starts(same)+pre_procs(same)
      sstarts= starts(same) 
      rstops= stops(same) ; use the real starts and stops for error display - AEE 8/12/03
      sos_nums= "OS_"+STRING(os_nums(same),'(I4.4)')
      ;sos_nums= "OS_"+INT2STR(os_nums(same),4)
      sos_nums= sc(same)+" "+sos_nums
     

      loc= WHERE(sstops GE os.os_start AND $  ; AEE 8/11/03 ; don't include processing time
                 sstarts LE (os.os_start+os.os_pre_proc_time+os.os_ro_time), snum_conf)

      IF (snum_conf GT 0) THEN BEGIN
        conf= 1
        FOR i= 0, snum_conf - 1 DO conf_info=[conf_info,sos_nums(loc(i))+" scheduled between "+ $
                                    TAI2UTC(sstarts(loc(i)),/ECS)+" and "+TAI2UTC(rstops(loc(i)),/ECS)+ $
                                    ' (Setup)'] 
      END 
    END

    onum_conf= 0
    bstarts= starts ; keep starts ans stops of each entire image for error display. AEE 8/12/03
    bstops= stops
    ;RESTORE,'ccd_readout_time.dat'  ;=> ccd_readout_time
    ccd_readout_time= ccd_readouts
    ostops= starts+pre_procs+ccd_readout_time ; AEE - 8/11/03 - only use ccd readout times
    ostarts= starts+pre_procs ; AEE - 8/11/03 - only use ccd readout times
    oos_nums= "OS_"+STRING(os_nums,'(I4.4)')
    ;oos_nums= "OS_"+INT2STR(os_nums,4)
    oos_nums= sc+" "+oos_nums

    loc= WHERE(ostops GE (os.os_start+os.os_pre_proc_time) AND $ ; AEE - 8/11/03 - only use ccd times
               ostarts LE (os.os_start+os.os_pre_proc_time+os.os_ro_time), onum_conf)

    IF (onum_conf GT 0) THEN BEGIN
      conf= 1
      FOR i= 0, onum_conf - 1 DO conf_info=[conf_info,oos_nums(loc(i))+" scheduled between "+ $
                                 TAI2UTC(bstarts(loc(i)),/ECS)+" and "+TAI2UTC(bstops(loc(i)),/ECS)+ $
                                 ' (CCD)']
     END

    num_conf= snum_conf + onum_conf

    IF (num_conf GT 0) THEN conf_info= conf_info(1:*)

    RETURN,conf  ; 1 = conflict is true.  0 = conflict is false.

END
