;+
;$Id: get_sca_description.pro,v 1.3 2005/01/24 17:56:33 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GET_SCA_DESCRIPTION
;               
; Purpose     : Returns type of an entry (val) in the .SCT (SpaceCraftActivity) file. 
;
; Explanation : If val is a number then return the descriptive string associated
;               with that index from the SCT Descriptions table ( kap_descriptions).
;               If val is a description string then return the index associated
;               with that string from the SCT Descriptions table.  If the string
;               does not exist in the table then create a new entry and return
;               the new index.
;               
; Use         : type = GET_SCA_DESCRIPTION(val)
;    
; Inputs      : val    Either a number or a description string. 
;
; Opt. Inputs : None
;               
; Outputs     : type   Either a descriptive string or an index. 
;
; Opt. Outputs: None
;
; Keywords    : 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: get_sca_description.pro,v $
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:03  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

FUNCTION GET_SCA_DESCRIPTION, val

COMMON KAP_INIT

   IF NOT(SEXIST(kap_descriptions)) THEN kap_descriptions = ''

   val_type = DATATYPE(val)

   IF (val_type EQ 'STR') THEN BEGIN
      ind = WHERE(kap_descriptions EQ val)
      IF (ind(0) GE 0) THEN BEGIN
         RETURN, ind(0)
      ENDIF ELSE BEGIN
         kap_descriptions = [kap_descriptions, val]
         RETURN, N_ELEMENTS(kap_descriptions)-1
      ENDELSE
   ENDIF ELSE BEGIN
      ind = FIX(val)
      IF ( (ind GE 0) AND (ind LT N_ELEMENTS(kap_descriptions)) ) THEN $
         RETURN, kap_descriptions(ind) $
      ELSE RETURN, ''
   ENDELSE

END
