;+
;$Id: find_os_match.pro,v 1.3 2005/01/24 17:56:33 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : FIND_OS_MATCH
;
; Purpose     : This function determines which of the selected Oses to be shifted
;               do not belong to a command sequence and if they do, all of the OSes
;               in the command sequence are being shifted since we can't shift only
;               a subset of a command sequence.
;
; Use         : shft_ind= FIND_OS_MATCH(os_arr,shft_oses,tos,sched_cmdseq,cmd_seq)
;
; Inputs      : os_arr         All of currently scheduled OSes.
;               shft_oses      OSes to be shifted.
;               tos            String array of all schedule OS numbers including those in a
;                              block command sequence, if any. 
;               sched_cmdseq   Block command sequences in the current schedule, if any.
;               cmd_seq        String containing '(cmd seq)' to identify command sequences.
;
; Opt. Inputs : None
;
; Outputs     : shft_ind       Index of all Oses that can be shifted.
;
; Opt. Outputs: None
;
; Keywords    :
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 10/26/04 - Added S/C A & B capability.
;
; $Log: find_os_match.pro,v $
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION GET_SCHED_CMD_SEQ_INDEX,str,sched_cmdseq 

  toks= STR_SEP(str,'@')  ; => break up "xxxx,yyyy @ yyyy/mm/dd hh:mm:ss.mss  (cmd seq)"
  os_str= STRTRIM(toks(0),2)    ; => "xxxx,yyyy"
  os_tme= STR_SEP(toks(1),',')  ; => " yyyy/mm/dd hh:mm:ss.mss  (cmd seq)"
  cseq=os_tme(1)
  os_tme= STRTRIM(os_tme(0),2)  ; => "yyyy/mm/dd hh:mm:ss.mss"
  bsid= STR_SEP(STRTRIM(cseq,2),' ') ; => "cmd","seq","A#" (or "B#")
  bsid= STR_SEP(bsid(2),')')
  bsid=bsid(0)
  cs_ind= WHERE(sched_cmdseq.os_num EQ os_str AND STRMID(sched_cmdseq.os_time,0,23) EQ os_tme AND $
                sched_cmdseq.bsf EQ bsid, wcnt)

  IF (wcnt GT 0) THEN $
    cs_ind= cs_ind(0) $
  ELSE                $
    cs_ind= -1

  RETURN, cs_ind

END



PRO CHECK_CMDSEQ,os_arr,os_ind,tres,toss,sched_cmdseq, cmd_seq

  toss1= toss

  ; if any seq_cmd lines remain, see if they contain os_id:

  FOR i= 0, N_ELEMENTS(toss1)-1 DO BEGIN
    cs_ind= GET_SCHED_CMD_SEQ_INDEX(toss1(i),sched_cmdseq)
    ; Now we have index into sched_cmdseq to check for input os_ind:
    oses= LONG(STR_SEP(sched_cmdseq(cs_ind).os_num,','))
    times= UTC2TAI(STR2UTC(STR_SEP(sched_cmdseq(cs_ind).os_time,',')))
    f= WHERE(oses EQ os_arr(os_ind(0)).os_num, fcnt)
    FOR j= 0, fcnt-1 DO BEGIN ; if cmdseq contains the OS we are looking for:
      ok= WHERE(os_arr(os_ind).os_start NE times(f(j)))
      os_ind= os_ind(ok)
    ENDFOR
  ENDFOR

END



FUNCTION FIND_OS_MATCH,os_arr,tres,toss,sched_cmdseq,cmd_seq

  shift_ind= -1

  csind= WHERE(STRPOS(toss,cmd_seq) GE 0, cnt) ; AEE 2/18/04

  FOR i= 0, N_ELEMENTS(tres)-1 DO BEGIN
    IF (STRPOS(tres(i),cmd_seq) GE 0) THEN BEGIN ; a cmdseq line
      w= WHERE(toss EQ tres(i))    
      cs_ind= GET_SCHED_CMD_SEQ_INDEX(toss(w(0)),sched_cmdseq)

      oses=  LONG(STR_SEP(sched_cmdseq(cs_ind).os_num,','))
      times= UTC2TAI(STR2UTC(STR_SEP(sched_cmdseq(cs_ind).os_time,',')))
      ind= WHERE(os_arr.os_num EQ oses(0) AND os_arr.os_start EQ times(0) AND $
                 os_arr.bsf EQ sched_cmdseq(cs_ind).bsf)
      FOR j= 1, N_ELEMENTS(oses)-1 DO $ 
        ind= [ind,WHERE(os_arr.os_num EQ oses(j) AND os_arr.os_start EQ times(j) AND $
                        os_arr.bsf EQ sched_cmdseq(cs_ind).bsf)]
    ENDIF ELSE BEGIN ; just an os_num line 
      ; Find os_num indexes for this (single) os_num minus those that may be part of non-selected
      ; cmdseq lines, if any:
      ind= WHERE(os_arr.os_num EQ LONG(tres(i)))
      IF (cnt GT 0) THEN CHECK_CMDSEQ,os_arr,ind,tres,toss(csind),sched_cmdseq,cmd_seq  ; AEE 2/18/04
      help,ind
    ENDELSE
    shift_ind= [shift_ind,ind]
  ENDFOR  
 
  shift_ind= shift_ind(1:*)
 
  RETURN, shift_ind

END 
