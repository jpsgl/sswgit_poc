;+
;$Id: expand_oses.pro,v 1.6 2009/09/11 20:28:08 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : EXPAND_OSES
;               
; Purpose     : This pro is used to expand the scheduled OSes by addind a new
;               entry for those going to multiple APIDs in order to correctly
;               maintain the dowlink loads for all channels.
;
; Explanation:  Images scheduled for multiple APIDs destinations are selected
;               and for each APID a new entry is added. The image counters,
;               sequence counters, and S/C ID are used to generate a science
;               file name for each image. For the HI summed-sequence images 
;               where only one image (at the time of last image) is downloaded,
;               only the last image of the sequence is used.
;               
; Use         : EXPAND_OSES, expanded, os_dest, sc 
;    
; Inputs      : expanded     contains all instances of all images scheduled.
;               os_dest      contain APIDs for all scheduled OSes.
;               sc           'A' or 'B' SpaceCraft used for generating image
;                            header science file names. 
;
; Opt. Inputs : None 
;               
; Outputs     : expanded     Images with multiple APIDs now have an entry for
;                            each of the APID with the correct image counter. 
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/30/04 - used same_seq_start_time to isolate sequences.
;                                       start with img_cntr=1 with 1st image in a new day. 
;                                       Also use orig_start to set img_cnt since start for
;                                       images before start of display, if any, are set to
;                                       start of display time. 
;                            07/02/04 - used os_start to set img_cnt.
;              Ed Esfandiari 05/08/06 - Added separate proc_time for each apid.
;              Ed Esfandiari 08/26/09 - Added new HI sample, HI1SPW, HI2PW, HI word, and
;                                       LO word (hilo checks) IP function effects on resources.
;              Ed Esfandiari 09/04/09 - Changed code so that HI summed SW images also go to SSR1.
;
;
; $Log: expand_oses.pro,v $
; Revision 1.6  2009/09/11 20:28:08  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.2  2004/07/02 15:40:26  esfand
; use os_start to set img_cnt
;
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


PRO EXPAND_OSES, expanded, os_dest, sc 
COMMON APIDS, multi_apid

;On output, expanded may be modified.

; In this routine:
; 1 - If an image has more than one APID (selected from PT), let's say SSR1 and SW, then expand
;     the input to have an entry for each of the SSR1 and SW APIDs.  
;
; 2 - If a SW or RT image goes to SSR1 by default, then add 'SSR1' to the APID string for
;     that image.
;
; 3 - If a summed HI seq, only keep and use the last image of the sequence.
;
; 4 - Create the science filenames for the files that will be downlinked. 
;

; First, if summing is used (in a HI1 or HI2 sequence), only keep the last image and remove the
; rest from the expanded list since only one image at the time of the last image is downlinked
; and that is all we need for displaying all of the APID buffers in the rest of the schedule_plot:


  ;RESTORE,'./launched_date.sav' ;=> launch_date  (2004)  ; AEE 2/13/04
  filename= GETENV('PT')+'/IN/OTHER/'+'launched_date.sav'
  RESTORE,filename ;=> launch_date  (2004 for now)

  y10up= ['A','B','C','D','E','F','G','H','I','J', 'K','L','M','N','O']
  mts=['1','2','3','4','5','6','7','8','9','A','B','C']

  summed= WHERE(os_dest.summed EQ 1, scnt)
;help,summed,scnt
;help,os_dest

  IF (scnt GT 0) THEN BEGIN
    FOR i=0, scnt-1 DO BEGIN
      ind= WHERE(expanded.os_num EQ os_dest(summed(i)).os_num AND $
                 expanded.tele EQ os_dest(summed(i)).tele)
      ;last= WHERE(expanded(ind).seq_cnt EQ os_dest(summed(i)).num_images, lcnt)
      ;keep_os= expanded(ind(last))
      ;keep_os= expanded(ind) ; keep all (first,middle,last) HI summed images
      ;keep_os= expanded(ind(1:*)) ; keep middle + last HI summed images
      ; Only keep middle + last HI summed images:
      no_fst= WHERE(expanded(ind).seq_cnt NE 1, no_fst_cnt)
      keep_os= expanded(ind(no_fst)) ; keep middle + last HI summed images
      IF (STRPOS(keep_os(0).apid,'Drop') GE 0) THEN BEGIN ; no HISUM for middle, keep only last image
        last= WHERE(expanded(ind).seq_cnt EQ os_dest(summed(i)).num_images, lcnt)
        keep_os= expanded(ind(last))
      ENDIF
;help,keep_os
;stop
      ind= WHERE(expanded.os_num NE os_dest(summed(i)).os_num OR $
                 expanded.tele NE os_dest(summed(i)).tele, icnt)
      IF (icnt EQ 0) THEN BEGIN
        expanded= keep_os
      ENDIF ELSE BEGIN
        expanded= [expanded(ind),keep_os]
        expanded= expanded(SORT(expanded.orig_ccd_stop)) ;since expanded.ccd_stop is changed if display
                                                         ;is moved; OSes not in display get the same time
      ENDELSE 
    ENDFOR
  ENDIF

;help,expanded
;stop

  ; AEE 1/21/04 - For each SCIP sequence and non-summed HI sequence, image counter should be incremented
  ;               only once for the time of 1st image taken. The combination of this + seq. counter will
  ;               still produce unique filenames for each image in the sequence since seq. counter is
  ;               incremented for each image:

  ; need to keep all non-summed seq images around to correctly track the secchi-buffer and
  ; SSR, etc. so only keep the img_cnt for all images in the seq to the first counter instead
  ; of removing images 1 to last. 


  new_expanded= expanded

  nosum_seq= WHERE(os_dest.lp GE 5 AND os_dest.lp LE 6 AND os_dest.summed EQ 0, ns_cnt)

;help,ns_cnt

  IF (ns_cnt GT 0) THEN BEGIN
    FOR i=0, ns_cnt-1 DO BEGIN
      ; keep only the first image in the seq (non-summed for HI seq) where seq_cnt=1:
      fst= WHERE(new_expanded.os_num EQ os_dest(nosum_seq(i)).os_num AND new_expanded.seq_cnt EQ 1 AND $
                 new_expanded.tele EQ os_dest(nosum_seq(i)).tele, fst_cnt)
      IF (fst_cnt GT 0) THEN BEGIN 
        keep_os= new_expanded(fst)
        ind= WHERE(new_expanded.os_num NE os_dest(nosum_seq(i)).os_num OR $
                   new_expanded.tele NE os_dest(nosum_seq(i)).tele, icnt)
        IF (icnt EQ 0) THEN BEGIN
          new_expanded= keep_os
        ENDIF ELSE BEGIN
          new_expanded= [new_expanded(ind),keep_os]
          new_expanded= new_expanded(SORT(new_expanded.orig_ccd_stop)) ;since expanded.ccd_stop is changed if 
                                                           ;display is moved; OSes not in display get the same time

        ENDELSE
      ENDIF
    ENDFOR

  ENDIF

;help,expanded,new_expanded  

  ; Now assign image_counter (for science filenames) to each unique OS:

  ; Note: All of the scheduled oses (not only the ones that are displyed) will go to .IPT, etc.
  ;       outputfile even if a smaller portion of the schedule is displayed. So, will need to
  ;       start the image_counter from 1 for the first image at or after the 00:00:00 hour on
  ;       each day.


  ; AEE 1/22/04:
  ; Assign img_cnt to the fst image of each OS_NUM (for non-summed sequences, use the 1st image in each
  ; seq) and for seqeunces give rest of the images in the seq. the same img_cnt. Start each day with
  ; img_cnt=1:

  ;prev_start= 0.0
  days = TAI2UTC(new_expanded.os_start)
  days= days.mjd
  ndays= UNIQ(days)
  bind= 0L
  FOR dcnt= 0L, N_ELEMENTS(ndays)-1 DO BEGIN
    cntr= 0L
    FOR i= bind, ndays(dcnt) DO BEGIN
      cntr= cntr+1L
      w= WHERE(expanded.os_num EQ new_expanded(i).os_num AND $
               expanded.orig_ccd_stop EQ new_expanded(i).orig_ccd_stop) 
      expanded(w).img_cnt= cntr
      ; Find all images within the same seq and assign the same img_cnt to them:
      rest= WHERE(expanded.os_num EQ expanded(w).os_num AND $
                  expanded.same_seq_start_time EQ expanded(w).same_seq_start_time, rcnt)
      IF (rcnt GT 0) THEN expanded(rest).img_cnt= cntr
    ENDFOR
    bind= ndays(dcnT)+1L 
  ENDFOR

;for p=0,N_ELEMENTS(expanded)-1 do $ 
; print,expanded(p).os_num,expanded(p).orig_start,expanded(p).same_seq_start_time,expanded(p).seq_cnt,expanded(p).img_cnt
;stop


  ; Now figure out the size of each image per APIDs used. Need to further expand the "expanded"
  ; so that there is en entry for each APID used (using correct order/time and size for going
  ; into the telemetry buffer (ccd_rot will be the same but add processing time for each.


  texpand= expanded

  new_expanded= expanded(0)  ; remove this, 1st element, at the end 
 
  FOR i=0L, N_ELEMENTS(expanded)-1 DO BEGIN
    w= WHERE(os_dest.os_num EQ expanded(i).os_num AND os_dest.tele EQ expanded(i).tele)
    dest= os_dest(w).dest
    ssr1= dest(0)
    ssr1_rt= 0
    ssr1_sw= 0
;help, dest
;stop
    IF (ssr1 GE 7 AND ssr1 LE 13) THEN BEGIN  ; SSR1 + RT
      dest(0)= ssr1 - 7 
      ssr1_rt= 1
    ENDIF
    IF (ssr1 GE 14 AND ssr1 LE 20) THEN BEGIN ; SSR1 + SW
      dest(0)= ssr1 - 14
      ssr1_sw= 1
    ENDIF
    IF (ssr1 GE 21 AND ssr1 LE 26) THEN BEGIN ; SSR1 + RT + SW
      dest(0)= ssr1 - 21 
      ssr1_rt= 1
      ssr1_sw= 1
    ENDIF

    ids= WHERE(dest GT 0, cnt)
    dest= dest(ids)
    srt= SORT(dest)
    ids= ids(srt)
    sizes= DOUBLE(STR_SEP(expanded(i).sizes,' '))  ; sizes correspond to the order of selected APIDs
    proc_times= DOUBLE(STR_SEP(expanded(i).proc_times,' '))

    expand= expanded(i)
    hilo_apids= STR_SEP(expanded(i).apid,',') ; hilo apids assigned in schedule_plot.pro
    ; To avoid ids(j) out-of-range. note that ids not used in this case:
    while (n_elements(ids) lt n_elements(hilo_apids)) do ids= [ids,ids[0]]
    ; To avoid ids(j) out-of-range. if ids > hilo_apids:
    while (n_elements(ids) gt n_elements(hilo_apids)) do hilo_apids= [hilo_apids,'']


    FOR j= 0, N_ELEMENTS(sizes)-1 DO BEGIN ; AEE 1/19/04 For images that go to SSR1 by default, there is no size 
                                           ;             (sizes is less than cnt).
      size= sizes(j)
      proc_time= proc_times(j)
      expand.apid= hilo_apids(j)

;help,ids(j)
;stop
      CASE ids(j) OF
        0: apid= 'SSR1'
        1: apid= 'SSR2'
        2: BEGIN  
             apid= 'RT' 
             IF (ssr1_rt) THEN apid= apid + ',SSR1' 
           END
        3: BEGIN 
             apid= 'SW' 
             IF (ssr1_sw) THEN apid= apid + ',SSR1' 
           END
        4: BEGIN   ; GroundTest
             apid= 'GT'
           END
        5: BEGIN   ; SSR1RT  (new - Added 3/3/04)
             apid= 'S1RT'
           END
      ENDCASE

      ; For each PT scheduled APID, add a new element to the expanded. If a RT or SW APID
      ; also go to SSR1, the 'SSR1' is added to the RT or SW element's APID (from case 2 or 3 above).
      ; An image that goes to more than one APID by default, is only processed and placed on the telem. 
      ; buffer once; In that case, the APID string should be used later for populating various display 
      ; channels: 


      expand.size= sizes(j)
      ;expand.apid= apid
      ;if (expand.lp ne 6) then begin 
      if (expand.apid eq '') then begin 
         expand.apid= apid
      endif else begin
        case expand.apid of  ; apid 40-44 are assigned to HI summed seq in schedule_plot.pro
         ;'40': expand.apid= 'SW'
         '40': expand.apid= 'SW,SSR1' ; SW images also go to SSR1
         '41': expand.apid= 'SSR1'
         '42': expand.apid= 'SSR2'
         '43': expand.apid= 'GND'
         '44': expand.apid= 'RAW'
        endcase 
      endelse
      ;expand.proc_time= expanded(i).proc_time*(j+1)
      ;expand.proc_time= expanded(i).proc_time ; later move all images apart so that ccd_rot of next
      ;                                        ; image is GE stop time of the image before.
      expand.proc_time= proc_times(j)

;help,/st,expand,j
;stop

      ; Add science filename for the image:

      sdt= UTC2STR(TAI2UTC(expand.orig_ccd_stop))
      yr= STRMID(sdt,0,4)
      mo= STRMID(sdt,5,2)
      dy= STRMID(sdt,8,2)

      IF (FIX(yr) - launch_date LT 10) THEN $ 
        yr= STRMID(yr,3,1) $   ; '0' to '9'
      ELSE $ 
        yr= y10up(FIX(yr) - launch_date - 10)  ; 'A' to 'O'
      mo= mts(FIX(mo)-1)     

      expand.sfn= yr+mo+dy+BASE10TO36(expand.img_cnt*100+expand.seq_cnt)

      channels= STR_SEP(expand.apid,',')
      ;FOR k= 0, n_ELEMENTS(channels)-1 DO BEGIN 
      ;  expand.sfn= expand.sfn + '.'+GET_FILE_EXT(sc, channels(k), expand.tele)
      ;  IF (k LT n_ELEMENTS(channels)-1) THEN expand.sfn= expand.sfn + ', '
      ;ENDFOR

      ; Note: The RT and SW images that go to SSR1 by default don't get assigned a
      ;       filename based on the SSR1 APID. They go to SSR1 on the spacecraft
      ;       side (and have the same science filename with the RT or SW extension)
      ;       and we don't even need to know about it except for the fact that the
      ;       planning tool needs to account for them for managing the SSR1 buffer:

      expand.sfn= expand.sfn + '.'+GET_FILE_EXT(sc, channels(0), expand.tele)

      new_expanded= [new_expanded,expand] 

    ENDFOR

  ENDFOR

  new_expanded= new_expanded(1:*)


   ; March 02, 04:
   ;Images from all telescopes go into a single "image processing buffer" and only
   ;one at a time. They are put there according to the ccd readout time order (the
   ;image with an earlier ccd readout goes to the "image processing buffer" first).
   ;If an SCIP and HI image have the same ccd readout times, assume SCIP goes in 
   ;first:

   expanded= new_expanded

;  3/3/04 - AEE:
;  Note: removed the above SORTs since it can messup order of APIDs for the same images with
;        multiple destinations. These images should be already in right ccd stop time order
;        and if anything, they should be sorted using the img_cnt.

   ; Finally, separate images that may be overlapping going to the "image processing buffer"
   ; by changing the ccd stop and image stop times. Make sure that the ccd_stop time of an
   ; image is not less than the end of the image processing time of the previous image. If it
   ; is, then move the ccd_stop time of the current image to the stop time of the previous image
   ; and also adjust the stop time of the current image:
   ;
   ; Note: this does not impact the actual time of image taking, start of ccd readout, or 
   ;       or ccd duration time. These are temporary changes for the 
   ;       purpose of displaying the telemetry buffer and downlink channels.

   scnt= N_ELEMENTS(expanded)
   FOR i= 1, scnt-1 DO BEGIN
     IF (expanded(i).ccd_stop LT expanded(i-1).stop) THEN BEGIN
       ;print,'Image overlap in "image processing buffer" detected. Seprating the 2 images.'
       ;print,'i, (i-1).stop, (i).ccd_stop= ',i, expanded(i-1).stop, expanded(i).ccd_stop
       expanded(i).ccd_stop= expanded(i-1).stop
       expanded(i).stop= expanded(i).ccd_stop + expanded(i).proc_time/3600.0
       ;print,'  (i).ccd_stop= ', expanded(i).ccd_stop
     ENDIF
   ENDFOR

  RETURN

END
