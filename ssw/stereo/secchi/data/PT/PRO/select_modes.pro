;+
;$Id: select_modes.pro,v 1.2 2005/01/24 17:56:36 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : SELECT_MODES
;
; Purpose     : This function displays a list of various scheduled image info such as telescopes,
;               type of images, blocked sequence files, sets, etc. and user chooses one or more
;               items from the list to be plotted.
;
; Use         : selected= SELECT_MODES(select_list, func)
;
; Inputs      : select_list  List of currently scheduled image types, telescopes, etc. 
;               func         List of functions (os_tele, os_lp, bsf, set, or sc tags from os_arr).
;
; Opt. Inputs : None
;
; Outputs     : selected     Index of selected items form input list.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, November 2004 - First Version.
;
; Modification History:
;
; $Log: select_modes.pro,v $
; Revision 1.2  2005/01/24 17:56:36  esfand
; checkin changes since SCIP_A_TVAC
;
;
;-
;

PRO SELECT_MODES_EVENT, event
COMMON MODE_SELECTION, selected
 
  WIDGET_CONTROL, event.top, GET_UVALUE= modes
  WIDGET_CONTROL, event.id, GET_UVALUE=uval

  CASE (uval) OF
     'OK': BEGIN
            WIDGET_CONTROL, /DESTROY, modes.base
          END
     ELSE : BEGIN ; uval= an item in the input list
              ind = WHERE(modes.ids EQ event.id)
              IF (event.select) THEN $
                selected(ind)= 1 $
              ELSE $
                selected(ind)= 0
         END
  ENDCASE

END


FUNCTION SELECT_MODES,list,func
COMMON MODE_SELECTION, selected

    items= N_ELEMENTS(list)
    selected= LONARR(items)

    IF XRegistered("SELECT_MODES") THEN RETURN, 0   ; return false

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

      font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
      ids= LONARR(N_ELEMENTS(list))
      base = WIDGET_BASE(/COLUMN, TITLE=' Select One or More Item(s) and Press OK', $
                         /FRAME, X_SCROLL_SIZE=300, Y_SCROLL_SIZE=600)
      base2= WIDGET_BASE(base,/COLUMN)
      done= WIDGET_BUTTON(base2, VALUE='   OK   ', UVALUE= 'OK', /FRAME)
      lab= WIDGET_LABEL(base2,VALUE='')

      base1= WIDGET_BASE(base,/COLUMN, /NONEXCLUSIVE, /FRAME)
      cam= WHERE(STRPOS(func,"os_tele") GT 0, cind)
      tot= cind
      FOR i= 0, cind-2 DO BEGIN
        ids(cam(i))= WIDGET_BUTTON(base1, VALUE=list(cam(i)), UVALUE= list(cam(i)))
      ENDFOR
      ids(cam(cind-1))= WIDGET_BUTTON(base1, VALUE=list(cam(cind-1)), UVALUE= list(cam(cind-1)))
   
      IF (tot LT items) THEN BEGIN
        b= WIDGET_BASE(base,/COLUMN)
        lab= WIDGET_LABEL(b, VALUE='     AND')
        base1= WIDGET_BASE(base,/COLUMN, /NONEXCLUSIVE, /FRAME)
      ENDIF
   
      lp=  WHERE(STRPOS(func,"os_lp") GT 0, lind)
      tot= tot+lind
      FOR i= 0, lind-2 DO BEGIN
        ids(lp(i))= WIDGET_BUTTON(base1, VALUE=list(lp(i)), UVALUE= list(lp(i)))
      ENDFOR
      ids(lp(lind-1))= WIDGET_BUTTON(base1, VALUE=list(lp(lind-1)), UVALUE= list(lp(lind-1)))
   
      bs=  WHERE(STRPOS(func,"bsf") GT 0, bind)
      IF (tot LT items AND bind GT 0) THEN BEGIN 
        b= WIDGET_BASE(base,/COLUMN)
        lab= WIDGET_LABEL(b, VALUE='     AND')
        base1= WIDGET_BASE(base,/COLUMN, /NONEXCLUSIVE, /FRAME)
      ENDIF

      tot = (tot+bind) < items
      FOR i= 0, bind-2 DO BEGIN
        ids(bs(i))= WIDGET_BUTTON(base1, VALUE=list(bs(i)), UVALUE= list(bs(i)))
      ENDFOR 
      IF (bind GT 0) THEN ids(bs(bind-1))= $
        WIDGET_BUTTON(base1, VALUE=list(bs(bind-1)), UVALUE= list(bs(bind-1)))
   
      sd= WHERE(STRPOS(func,"set_id") GT 0, sind) 
      IF (tot LT items AND sind GT 0) THEN BEGIN
        b= WIDGET_BASE(base,/COLUMN)
        lab= WIDGET_LABEL(b, VALUE='     AND')
        base1= WIDGET_BASE(base,/COLUMN, /NONEXCLUSIVE, /FRAME)
      ENDIF

      tot = (tot+sind) < items
      FOR i= 0, sind-2 DO BEGIN
        ids(sd(i))= WIDGET_BUTTON(base1, VALUE=list(sd(i)), UVALUE= list(sd(i)))
      ENDFOR 
      IF (sind GT 0) THEN ids(sd(sind-1))=  $
        WIDGET_BUTTON(base1, VALUE=list(sd(sind-1)), UVALUE= list(sd(sind-1)))


      sy= WHERE(STRPOS(func,"AB") GT 0, syind)
      IF (tot LT items AND syind GT 0) THEN BEGIN
        b= WIDGET_BASE(base,/COLUMN)
        lab= WIDGET_LABEL(b, VALUE='     AND')
        base1= WIDGET_BASE(base,/COLUMN, /NONEXCLUSIVE, /FRAME)
      ENDIF
      tot = (tot+syind) < items
      FOR i= 0, syind-2 DO BEGIN
        ids(sy(i))= WIDGET_BUTTON(base1, VALUE=list(sy(i)), UVALUE= list(sy(i)))
      ENDFOR
      IF (syind GT 0) THEN ids(sy(syind-1))=  $
        WIDGET_BUTTON(base1, VALUE=list(sy(syind-1)), UVALUE= list(sy(syind-1)))

      ;base1= WIDGET_BASE(base,/COLUMN, /NONEXCLUSIVE, /FRAME)
      ;FOR i= 0, N_ELEMENTS(list)-1 DO BEGIN
      ;  ids(i)= WIDGET_BUTTON(base1, VALUE=list(i), UVALUE= list(i))
      ;ENDFOR

      WIDGET_CONTROL, /REAL, base

      modes = CREATE_STRUCT( 'base', base, $
                             'ids', ids)
 
      WIDGET_CONTROL,base, SET_UVALUE= modes
      XMANAGER, "SELECT_MODES",base, /MODAL

  RETURN, selected 
END

