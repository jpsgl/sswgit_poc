;+
;$Id: check_date.pro,v 1.1.1.2 2004/07/01 21:18:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : CHECK_DATE
;               
; Purpose     : Check a date/time string for validity.
;               
; Use         : stat= CHECK_DATE(dt) 
;    
; Inputs      : dt     Datetime string to be checked. 
;
; Opt. Inputs : None
;               
; Outputs     :   stat True (valid dt) / False (invalid dt).
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: check_date.pro,v $
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;

FUNCTION CHECK_DATE,date

   ; date can only be 'yyyy/mm/dd hh:mm:ss' or 'yyyy/mm/dd hh:mm:ss.sss'
   ; return 0 (false) if not the correct format and return 1 (true) if it is.

   IF (STRLEN(date) EQ 19) THEN BEGIN
     toks= STR_SEP(date,' ')
     IF (N_ELEMENTS(toks) NE 2) THEN RETURN, 0
     ltok= STR_SEP(toks(0),'/')
     IF (N_ELEMENTS(ltok) NE 3) THEN RETURN, 0
     rtok= STR_SEP(toks(1),':')
     IF (N_ELEMENTS(rtok) NE 3) THEN RETURN, 0
     RETURN,1
   ENDIF ELSE BEGIN
     IF (STRLEN(date) EQ 23) THEN BEGIN
       toks= STR_SEP(date,' ')
       IF (N_ELEMENTS(toks) NE 2) THEN RETURN, 0
       ltok= STR_SEP(toks(0),'/')
       IF (N_ELEMENTS(ltok) NE 3) THEN RETURN, 0
       rtok= STR_SEP(toks(1),':')
       IF (N_ELEMENTS(rtok) NE 3) THEN RETURN, 0
       dot= STR_SEP(rtok(2),'.')
       IF (N_ELEMENTS(dot) NE 2) THEN RETURN, 0
       RETURN,1
     ENDIF ELSE RETURN,0 ; false
   ENDELSE
END

