;
; AEE - 07/25/03 - add the GT-dump section of .IPT file to the variables in GT_DUMPS_SHARE common block.
;
;+
;$Id: add_ipt_gt.pro,v 1.7 2009/09/11 20:28:06 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ADD_IPT_GT
;               
; Purpose     : Adds the GT-dump section of .IPT file to the variables in
;               the GT_DUMPS_SHARE common block.
;               
; Use         : ADD_IPT_GT, key_val_arr 
;    
; Inputs      : key_val_arr  Contains GT information picked up from .IPT file. 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None 
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/06/04 - Added SpaceCraft dependency.
;              Ed Esfandiari 09/01/06 - Added GT rates.
;              Ed Esfandiari 12/28/06 - Added bsfs and bsf_gt.
;
; $Log: add_ipt_gt.pro,v $
; Revision 1.7  2009/09/11 20:28:06  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

PRO ADD_IPT_GT,str_arr
COMMON GT_DUMPS_SHARE   ; contains stime_gt, etime_gt, apid_gt, sc_gt, rate_gt, bsf_gt

  nsched = FIX(STRTRIM(str_arr(2).val,2))
  sdates = STRTRIM(str_arr(3).val,2)
  edates = STRTRIM(str_arr(4).val,2)
  apids  = STRTRIM(str_arr(5).val,2)
  apids  = STRMID(apids,1,STRLEN(apids)-2) ; remove '(' and ')' from the ends.
  apid= STR_SEP(apids,',')
  sc = STRTRIM(str_arr(6).val,2)
  srates= STRTRIM(str_arr(7).val,2)

  sdates = STRMID(sdates,1,STRLEN(sdates)-2) ; remove '(' and ')' from the ends.
  sdates = STR_SEP(sdates,',')
  edates = STRMID(edates,1,STRLEN(edates)-2) ; remove '(' and ')' from the ends.
  edates = STR_SEP(edates,',')
  sc = STRMID(sc,1,STRLEN(sc)-2) ; remove '(' and ')' from the ends.
  sc = STR_SEP(sc,',')
  srates= STRMID(srates,1,STRLEN(srates)-2) ; remove '(' and ')' from the ends.
  srates= STR_SEP(srates,',')

  stime= DBLARR(nsched)
  etime= DBLARR(nsched)
  rates= LONARR(nsched)
  bsfs= STRARR(nsched) ; not part of IPT file but keep as place holder.

  FOR i= 0, nsched-1 DO BEGIN
    sdate= sdates(i)
    edate= edates(i)
    ; form a date format of 'yyyy/mm/dd hh:mm:dd':
    sdate= STRMID(sdate,0,4)+'/'+STRMID(sdate,4,2)+'/'+STRMID(sdate,6,2)+' '+ $
           STRMID(sdate,9,2)+':'+STRMID(sdate,11,2)+':'+STRMID(sdate,13,2)
    edate= STRMID(edate,0,4)+'/'+STRMID(edate,4,2)+'/'+STRMID(edate,6,2)+' '+ $
           STRMID(edate,9,2)+':'+STRMID(edate,11,2)+':'+STRMID(edate,13,2)
    stime(i)= UTC2TAI(STR2UTC(sdate)) 
    etime(i)= UTC2TAI(STR2UTC(edate)) 
    rates(i)= LONG(srates(i))
    bsfs(i)= '' ; will be set in schedule_event if it G-command belongs to a .bsf file.
  ENDFOR

  IF (DATATYPE(stime_gt) NE 'UND') THEN BEGIN
    stime_gt= [stime_gt,stime]
    etime_gt= [etime_gt,etime]
    apid_gt = [apid_gt,apid]
    sc_gt = [sc_gt,sc]
    rate_gt = [rate_gt,rates]
    bsf_gt= [bsf_gt,bsfs]
  ENDIF ELSE BEGIN
    stime_gt= stime
    etime_gt= etime
    apid_gt = apid
    sc_gt= sc
    rate_gt= rates
    bsf_gt= bsfs
  ENDELSE 

  RETURN
END

