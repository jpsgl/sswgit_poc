;+
;$Id: find_conflicts.pro,v 1.3 2005/01/24 17:56:33 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : FIND_CONFLICTS 
;
; Purpose     : Check to see if one or more OS [instances] to be scheduled (added) or shifted causes any 
;               conflicts with other OS times already scheduled.
;
; Use         : result = FIND_CONFLICT(os_arr,os,defined_os,sched_type,conflicts_info) 
;
; Inputs      : os_arr     - An array of all of already scheduled OSs (assumed to be conflict free). 
;                            If not defined (i.e. nothing is currently scheduled), does not do anything.
;               os         - An array of one or more OS to be added to os_arr or shifted is checked for 
;                            conflicts. OS instances of the same os_num should be adjacent to each
;                            other (i.e. can't have os_num=[4500,4501,4500]).
;               defined_os - An array of all defined OS(s) that also include the os_num and telescope
;                            associated with it. The os_num and telescope pair will be used to separate 
;                            the SCIP and HI packages since they have separate time-lines (can overlap). 
;               sched_type - "os_shift" to shift the OS(s) or "os_add" to add OS(s).
;  
;
; Opt. Inputs : None.
;
; Outputs     : conflicts_info - A string array of found conflicts. For each conflict, the os_num 
;                                causing the conflict plus its start and end dates is added to the array. 
;               return         - This function returns 0 (for no conflicts) or 1 (for one or more conflicts).
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Calls       : None.
;
; Common      : None.
;
; Restrictions: This routine assumes all of the existing OSs in os_arr are conflict free. It
;               only checks for possible conflicts of the new os and those in the os_arr.
;
; Side effects: None.
;
; Category    : Planning, Scheduling.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/07/03 - Print os_num using 4-digits.
;              Ed Esfandiari 10/20/04 - Added S/C A B dependency.
;
; $Log: find_conflicts.pro,v $
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

FUNCTION FIND_CONFLICTS,os_arr,os,defined_os,sched_type,conflicts_info

; Each element of os_arr and os is a struture of this form:
;   OS_NUM          LONG
;   OS_START        DOUBLE 
;   OS_STOP         DOUBLE
;   OS_SIZE         LONG
;   OS_DURATION     DOUBLE

    os_conflict= 0       ; => no os_conflict
    conflicts_info=''

    exp_os_arr= EXPAND_SEQ(os_arr) ;expand SCIP & HI seq images, if any, before checking for conflicts.
    exp_os= EXPAND_SEQ(os) ; also exand the OS that is being checked for conflicts if it is a Sequence

;*** Must use the exp_os_arr,exp_os in the following code instead of os_arr and os. Also, all of
;    (or most) of the info needed now exists in the os structure so I don't think there is a need
;    for defined_os, etc..

    os_num= exp_os.os_num
                      ;                                              index= 0    1    2    3    4
    uos= UNIQ(os_num) ; uos contains last index of unique OS numbers (ie. [4500,4500,4500,4501,4501]
                      ; => uos=[2,4]). This means that OS instances of the same os_num should be
                      ; adjacent to each other (i.e. can't have os_num=[4500,4501,4500]).

    IF(STRLOWCASE(sched_type) EQ 'shift_os') THEN $
       os_msg= 'shifted to' $
    ELSE $
       os_msg= 'scheduled between'

;   Separate OSs in the OS_ARR into SCIP (tele=0,1,2) and HI (tele=3,4) arrays to chech
;   separately for SCIP and HI time-lines OS conflicts:

    scips=WHERE(defined_os.tele LE 2,scip_cnt)
    IF(scip_cnt GT 0) THEN BEGIN
      scip_os= defined_os(scips).os_num 
      scip_tel= defined_os(scips).tele
    END
    his=WHERE(defined_os.tele GE 3,hi_cnt)
    IF(hi_cnt GT 0) THEN  BEGIN
      hi_os= defined_os(his).os_num
      hi_tel= defined_os(his).tele
    END
    FOR i=0,N_ELEMENTS(exp_os_arr)-1 DO BEGIN
      IF(DATATYPE(scip_os) NE 'UND') THEN BEGIN
        scip_ind= WHERE(scip_os EQ exp_os_arr(i).os_num,cnt)
        IF(cnt GT 0) THEN BEGIN
          IF(DATATYPE(scip_os_arr_ind) EQ 'UND') THEN BEGIN 
            scip_os_arr_ind= i 
            scip_tel= defined_os(where(exp_os_arr(i).os_num eq defined_os.os_num)).tele
          ENDIF ELSE BEGIN 
            scip_os_arr_ind= [scip_os_arr_ind,i]
            scip_tel= [scip_tel,defined_os(where(exp_os_arr(i).os_num eq defined_os.os_num)).tele]
          ENDELSE
        ENDIF
      ENDIF
      IF(DATATYPE(hi_os) NE 'UND') THEN BEGIN
        hi_ind= WHERE(hi_os EQ exp_os_arr(i).os_num,cnt)
        IF(cnt GT 0) THEN BEGIN
          ttel= defined_os(where(exp_os_arr(i).os_num EQ defined_os.os_num AND $
                                 exp_os_arr(i).os_tele EQ defined_os.tele)).tele  ; AEE 8/12/03
          IF(DATATYPE(hi_os_arr_ind) EQ 'UND') THEN BEGIN 
            hi_os_arr_ind= i 
            ;hi_tel= defined_os(where(exp_os_arr(i).os_num eq defined_os.os_num)).tele
            hi_tel= ttel ; AEE 8/12/03
          ENDIF ELSE BEGIN 
            hi_os_arr_ind= [hi_os_arr_ind,i]
            ;hi_tel= [hi_tel,defined_os(where(exp_os_arr(i).os_num eq defined_os.os_num)).tele]
            hi_tel= [hi_tel,ttel]
          ENDELSE
        ENDIF
      ENDIF
    ENDFOR

    k= 0
    FOR i=0, N_ELEMENTS(uos) - 1 DO BEGIN
      cnt= 1
      FOR j=k, uos(i) DO BEGIN
        c_info=''
        IF(DATATYPE(scip_os_arr_ind) NE 'UND') THEN BEGIN
          pind= WHERE(scip_os EQ exp_os(j).os_num,pcnt)
          IF (pcnt GT 0) THEN BEGIN 
            package_ind= scip_os_arr_ind
            os_arr_cameras= scip_tel
            package_msg= "*** SCIP CEB (Setup/CCD) Constraint Conflict ***" 
          ENDIF
        ENDIF
        IF(DATATYPE(hi_os_arr_ind) NE 'UND') THEN BEGIN
          pind= WHERE(hi_os EQ exp_os(j).os_num,pcnt)
          IF (pcnt GT 0) THEN BEGIN
            package_ind= hi_os_arr_ind
            os_arr_cameras= hi_tel
            package_msg= "*** HI CEB (Setup/CCD) Constraint Conflict ***"
          ENDIF
        ENDIF
 
        IF (DATATYPE(package_ind) NE 'UND') THEN BEGIN 

          os_camera= defined_os(WHERE(defined_os.os_num EQ exp_os(j).os_num AND $
                                      defined_os.tele EQ exp_os(j).os_tele)).tele ; AEE 8/12/03

         ; AEE - 8/13/03
         ; IF a HI1&2 Sequence is checked for conflicts, identify the conflicts with tel and os_instance:

          os_cnt= uos(i)-k+1
          os_inst= cnt

          cam=''
          to= uos(i)
          frm= 0 & IF (i GT 0) THEN frm= uos(i-1)+1
          IF (N_ELEMENTS(UNIQ(exp_os(frm:to).os_tele)) EQ 2) THEN BEGIN
            tmp= WHERE(exp_os(frm:to).os_tele EQ os_camera,os_cnt)
            IF (cnt EQ 1) THEN p_cnt= os_cnt
            IF (cnt GT p_cnt) THEN os_inst= os_inst - p_cnt
            IF (os_camera EQ 3) THEN BEGIN 
              cam= ' of HI1' 
            ENDIF ELSE BEGIN 
              cam= ' of HI2'
            ENDELSE
          ENDIF 

          IF (CONFLICT(exp_os_arr(package_ind),os_arr_cameras,exp_os(j),os_camera,c_info)) THEN BEGIN
            os_conflict= 1

            conflicts_info= [conflicts_info,package_msg,$  ; AEE 8/13/03
                             exp_os(j).sc+" "+ $
                             "OS_"+STRING(os_num(j),'(I4.4)')+" (#"+STRTRIM(os_inst,2)+" of "+ $
                             STRTRIM(os_cnt,2)+cam+") to be "+os_msg,$
                             TAI2UTC(exp_os(j).os_start,/ECS)+" and "+TAI2UTC(exp_os(j).os_stop,/ECS), $
                             "conflicts with the following OS(s) already in the schedule:", $
                             c_info,$
                             ""]
          ENDIF
        ENDIF
        cnt=cnt+1
      ENDFOR 
      k= uos(i)+1 
    ENDFOR

    RETURN,os_conflict  ; 1 = os_conflict is true.  0 = os_conflict is false.

END
