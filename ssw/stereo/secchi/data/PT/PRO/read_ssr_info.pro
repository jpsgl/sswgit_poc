;+
;$Id: read_ssr_info.pro,v 1.4 2009/09/11 20:28:20 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : READ_SSR_INFO
;
; Purpose     : Uses secchi_ssr_info.sav set and generates a new ssr info structure depending
;               on mission data rate (high, mid, low).
;
; Use         : ssr_info= READ_SSR_INFO() 
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : Returns a structure containing secchi-buufer rate plus 
;               ssr1, ssr2, sw capacity, volume, and rate. 
;
; Opt. Outputs: None
;
; Prev. Hist. : N/A 
;
; Written by  : Ed Esfandiari, NRL, Sep 2006 - First Version.
;
; Modification History:
;              Ed Esfandiari 11/27/06 - Added Phasing Orbit rates.
;              Ed Esfandiari 01/09/07 - Changed SW volume to reflect 2-days (not 1).
;              Ed Esfandiari 01/30/09 - used new secchi_ssr_rate_info.sav instead of
;                                       secchi_ssr_info.sav with volumes and rates
;                                       of all future mission rate changes.
;
; $Log: read_ssr_info.pro,v $
; Revision 1.4  2009/09/11 20:28:20  esfand
; use 3-digit decimals to display volumes
;
;
;-

FUNCTION READ_SSR_INFO
COMMON SCHED_SHARE, schedv

        ;filename= GETENV('PT')+'/IN/OTHER/'+'secchi_ssr_info.sav'
        ;RESTORE, filename
           ;=> ssr_info:
              ;SB_CAPACITY     FLOAT           50.0000
              ;SB_RATE         FLOAT           150000.
              ;SSR1_CAPACITY   FLOAT           655.300    ; max cap.
              ;SSR2_CAPACITY   FLOAT           163.825    ; max cap.
              ;SW_CAPACITY     FLOAT           12.5000    ; max cap.
              ;PHASING_ORBIT_VOLUME FLOAT      135.000    ; downloadable SSR cap. for Phasing Orbit
              ;HIGH_VOLUME     FLOAT           583.570    ; downloadable SSR cap. for months  0-14
              ;MID_VOLUME      FLOAT           538.625    ; downloadable SSR cap. for months 14-18
              ;LOW_VOLUME      FLOAT           490.157    ; downloadable SSR cap. for months 18-24
              ;SSR1PCT         FLOAT           92.3000    ; % of SSR cap. allocated to SSR1
              ;SSR2PCT         FLOAT           6.00000    ; % of SSR cap. allocated to SSR2
              ;SWPCT           FLOAT           1.80000    ; % of SSR cap. allocated to SW (for 2-days).

   ;CASE schedv.drate OF
   ;  0: volume= ssr_info.high_volume
   ;  1: volume= ssr_info.mid_volume
   ;  2: volume= ssr_info.low_volume
   ;  3: volume= ssr_info.phasing_orbit_volume
   ;ENDCASE

;   ;volume= volume * 8.0 ; change from MB/day to Mb/day
;   ssr1_vol= volume * ssr_info.ssr1pct/100.0 ; SSR1 MB/day 
;   ; get rates for 7200 seconds (2 hours) of playback:
;   ssr1_rate= (ssr1_vol * 8000000) / 7200 ; bps SSR1 playback rate (24hr worth of data is played back in 1st 2 hrs)
;   ssr2_vol= volume * ssr_info.ssr2pct/100.0 ; SSR2  MB/day
;   ssr2_rate= (ssr2_vol * 8000000) / 7200    ; bps SSR2 playback rate (for the 1st 2 hrs of playback)
;   ssr2_rate2= ((ssr_info.ssr2_capacity-ssr2_vol)*8000000)/(0.92 * 3600) ; (for remaining .92 hrs of playback, if any)
;
;   ;sw_vol= volume * ssr_info.swpct/100.0 ; SW  MB/day (no - this is for 2 days)
;   sw_vol= (volume * ssr_info.swpct/100.0) / 2.0 ; SW  MB/day (can put 2-days worth of data but bring down 1/2 of it per day)
;   sw_rate= (sw_vol * 8000000) / 86400.0 ; bps SW playback rate (24hr playback)
;
;   ssr_data= {sb_capacity:ssr_info.sb_capacity, $
;              sb_rate:ssr_info.sb_rate,$
;              ssr1_capacity:ssr_info.ssr1_capacity, $  ; max SSR1 capacity
;              ssr1_vol:ssr1_vol, $                     ; max SSR1 data that can be played back in 24 hrs
;              ssr1_rate:ssr1_rate,$                    ; SSR1 PB rate (to PB 24hr of data in 1st 2 hrs)
;              ssr2_capacity:ssr_info.ssr2_capacity, $  ; max SSR2 capacity
;              ssr2_vol:ssr2_vol,$                      ; max SSR2 data that can be played back in 2 hrs
;              ssr2_rate:ssr2_rate, $                 ; SSR2 PB rate (in 1st 2 hrs of PB)
;              ssr2_rate2:ssr2_rate2,$                  ; SSR2 PB rate (remaining .92 hrs of SSR2 PB, if possible)
;              sw_capacity:ssr_info.sw_capacity, $      ; max SW capacity
;              sw_vol:sw_vol, $                         ; max SW data played back in 24 hrs
;              sw_rate:sw_rate $                        ; SW PB rate (to PB 24 hr of data in 24 hrs)
;             }


; Jan 30, 2009:
; The following ssr1, ssr2, and sw vol and rate for 480 to 30 kbps telemerty rates were calculated from 
; Nathan's worksheet (see solar6 ~/secchi_data_volume_rates.txt):

filename= GETENV('PT')+'/IN/OTHER/'+'secchi_ssr_rate_info.sav'
RESTORE, filename
;=> ssr_rate_info
;IDL[secchig]>help,/st,ssr_rate_info
; Structure PBRATES, 53 tags, length=212, data length=212:
;   SB_CAPACITY     FLOAT           50.0000
;   SB_RATE         FLOAT           150000.
;   SSR1_CAPACITY   FLOAT           706.000
;   SSR2_CAPACITY   FLOAT           100.000
;   SW_CAPACITY     FLOAT           12.5000
;   SSR1_720_VOL    FLOAT           700.000
;   SSR1_720_RATE   FLOAT           506.950
;   SSR2_720_VOL    FLOAT           55.0000
;   SSR2_720_RATE   FLOAT           38.0200
;   SW_720_VOL      FLOAT           6.00000
;   SW_720_RATE     FLOAT           4.22000
;   SSR1_480_VOL    FLOAT           608.000
;   SSR1_480_RATE   FLOAT           334.150
;   SSR2_480_VOL    FLOAT           45.0000
;   SSR2_480_RATE   FLOAT           25.0600
;   SW_480_VOL      FLOAT           5.00000
;   SW_480_RATE     FLOAT           2.78000
;   SSR1_360_VOL    FLOAT           541.000
;   SSR1_360_RATE   FLOAT           247.750
;   SSR2_360_VOL    FLOAT           40.0000
;   SSR2_360_RATE   FLOAT           18.5800
;   SW_360_VOL      FLOAT           4.50000
;   SW_360_RATE     FLOAT           2.06000
;   SSR1_240_VOL    FLOAT           411.000
;   SSR1_240_RATE   FLOAT           161.350
;   SSR2_240_VOL    FLOAT           30.0000
;   SSR2_240_RATE   FLOAT           12.1000
;   SW_240_VOL      FLOAT           3.40000
;   SW_240_RATE     FLOAT           1.34000
;   SSR1_160_VOL    FLOAT           302.000
;   SSR1_160_RATE   FLOAT           103.750
;   SSR2_160_VOL    FLOAT           22.0000
;   SSR2_160_RATE   FLOAT           7.78000
;   SW_160_VOL      FLOAT           2.50000
;   SW_160_RATE     FLOAT          0.860000
;   SSR1_120_VOL    FLOAT           218.000
;   SSR1_120_RATE   FLOAT           74.9500
;   SSR2_120_VOL    FLOAT           16.0000
;   SSR2_120_RATE   FLOAT           5.62000
;   SW_120_VOL      FLOAT           1.80000
;   SW_120_RATE     FLOAT          0.620000
;   SSR1_96_VOL     FLOAT           168.000
;   SSR1_96_RATE    FLOAT           57.6700
;   SSR2_96_VOL     FLOAT           12.5000
;   SSR2_96_RATE    FLOAT           4.33000
;   SW_96_VOL       FLOAT           1.40000
;   SW_96_RATE      FLOAT          0.480000
;   SSR1_30_VOL     FLOAT           37.0000
;   SSR1_30_RATE    FLOAT           10.1500
;   SSR2_30_VOL     FLOAT           2.70000
;   SSR2_30_RATE    FLOAT          0.760000
;   SW_30_VOL       FLOAT          0.300000
;   SW_30_RATE      FLOAT         0.0800000


   CASE schedv.drate OF
     0: begin ; 720 kbps
          ssr1_vol=  ssr_rate_info.ssr1_720_vol
          ssr1_rate= ssr_rate_info.ssr1_720_rate
          ssr2_vol=  ssr_rate_info.ssr2_720_vol
          ssr2_rate= ssr_rate_info.ssr2_720_rate
          sw_vol=    ssr_rate_info.sw_720_vol
          sw_rate=  ssr_rate_info.sw_720_rate
       end
     1: begin ; 480 kbps
          ssr1_vol=  ssr_rate_info.ssr1_480_vol
          ssr1_rate= ssr_rate_info.ssr1_480_rate
          ssr2_vol=  ssr_rate_info.ssr2_480_vol
          ssr2_rate= ssr_rate_info.ssr2_480_rate
          sw_vol=    ssr_rate_info.sw_480_vol
          sw_rate=  ssr_rate_info.sw_480_rate
       end
     2: begin ; 360 kbps
          ssr1_vol=  ssr_rate_info.ssr1_360_vol
          ssr1_rate= ssr_rate_info.ssr1_360_rate
          ssr2_vol=  ssr_rate_info.ssr2_360_vol
          ssr2_rate= ssr_rate_info.ssr2_360_rate
          sw_vol=    ssr_rate_info.sw_360_vol
          sw_rate=  ssr_rate_info.sw_360_rate
       end
     3: begin ; 240 kbps
          ssr1_vol=  ssr_rate_info.ssr1_240_vol
          ssr1_rate= ssr_rate_info.ssr1_240_rate
          ssr2_vol=  ssr_rate_info.ssr2_240_vol
          ssr2_rate= ssr_rate_info.ssr2_240_rate
          sw_vol=    ssr_rate_info.sw_240_vol
          sw_rate=  ssr_rate_info.sw_240_rate
       end
     4: begin ; 160 kbps
          ssr1_vol=  ssr_rate_info.ssr1_160_vol
          ssr1_rate= ssr_rate_info.ssr1_160_rate
          ssr2_vol=  ssr_rate_info.ssr2_160_vol
          ssr2_rate= ssr_rate_info.ssr2_160_rate
          sw_vol=    ssr_rate_info.sw_160_vol
          sw_rate=  ssr_rate_info.sw_160_rate
       end
     5: begin ; 120 kbps
          ssr1_vol=  ssr_rate_info.ssr1_120_vol
          ssr1_rate= ssr_rate_info.ssr1_120_rate
          ssr2_vol=  ssr_rate_info.ssr2_120_vol
          ssr2_rate= ssr_rate_info.ssr2_120_rate
          sw_vol=    ssr_rate_info.sw_120_vol
          sw_rate=  ssr_rate_info.sw_120_rate
       end
     6: begin ; 96 kbps
          ssr1_vol=  ssr_rate_info.ssr1_96_vol
          ssr1_rate= ssr_rate_info.ssr1_96_rate
          ssr2_vol=  ssr_rate_info.ssr2_96_vol
          ssr2_rate= ssr_rate_info.ssr2_96_rate
          sw_vol=    ssr_rate_info.sw_96_vol
          sw_rate=  ssr_rate_info.sw_96_rate
       end
     7: begin ; 30 kbps
          ssr1_vol=  ssr_rate_info.ssr1_30_vol
          ssr1_rate= ssr_rate_info.ssr1_30_rate
          ssr2_vol=  ssr_rate_info.ssr2_30_vol
          ssr2_rate= ssr_rate_info.ssr2_30_rate
          sw_vol=    ssr_rate_info.sw_30_vol
          sw_rate=  ssr_rate_info.sw_30_rate
       end
   ENDCASE

  ;Note:  ssr1, ssr2, and sw rates in new ssr_rate_info are in kbps - must change them 
  ;       to bps (expected by schedule_plot), done below:
   
   ssr_data= {sb_capacity:ssr_rate_info.sb_capacity, $
              sb_rate:ssr_rate_info.sb_rate,$
              ssr1_capacity:ssr_rate_info.ssr1_capacity, $  ; max SSR1 capacity
              ssr1_vol:ssr1_vol, $                          ; max SSR1 data that can be played back in 24 hrs
              ssr1_rate:ssr1_rate * 1000, $                 ; SSR1 PB rate
              ssr2_capacity:ssr_rate_info.ssr2_capacity, $  ; max SSR2 capacity
              ssr2_vol:ssr2_vol,$                           ; max SSR2 data that can be played back
              ssr2_rate:ssr2_rate * 1000 , $                ; SSR2 PB rate (in 1st 2 hrs of PB)
              ssr2_rate2:ssr2_rate * 1000 ,$                ; SSR2 PB rate (was used in last version - here set it to smae as ssr2_rate)
              sw_capacity:ssr_rate_info.sw_capacity, $      ; max SW capacity
              sw_vol:sw_vol, $                              ; max SW data played back in 24 hrs
              sw_rate:sw_rate * 1000 $                      ; SW PB rate (to PB 24 hr of data in 24 hrs)
             }

  RETURN, ssr_data

END

