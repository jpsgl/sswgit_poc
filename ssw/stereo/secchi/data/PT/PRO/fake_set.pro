;+
;$Id: fake_set.pro,v 1.1.1.2 2004/07/01 21:19:00 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : FAKE_SET
;               
; Purpose     : Define some sample Observation Programs for SECCHI Schedule.
;               
; Explanation : This routine defines the defined_set_arr.
;               
; Use         : FAKE_SET
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : None.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: fake_set.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO FAKE_SET

COMMON SET_DEFINED, set_instance, defined_set_arr

   set_instance = {set_instance, set_id:0L, set_cnt:0, fname:'' ,instr:'', sci_obj:'', comment:'', req_by:'', $
                  imp_by:'', cr_date:''} ; AEE 2/18/04

END
