;$Id: os_summary_text.pro,v 1.9 2009/09/11 20:28:16 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : OS_SUMMARY_TEXT
;
; Purpose     : This routine looks in the defined_os_arr and outputs
;               an ascii summary of all OS's to the screen using 
;               keyword = value format.
;
; Use         : OS_SUMMARY_TEXT
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;               
; Modification History:
;              Ed Esfandiari 6/7/04 - print os_num using 4-digits.
;              Ed Esfandiari 06/22/04 - Use lamp descriptions from a save set.
;              Ed Esfandiari 07/14/04 - Use 4 ROI tables instead of 1.
;              Ed Esfandiari 08/31/04 - Changed format of exptime reports.
;              Ed Esfandiari 09/30/04 - Removed sync and added S/C.
;              Ed Esfandiari 10/07/04 - Direct output to SC_A or SC_B.
;              Ed Esfandiari 12/13/04 - Report non cal image exptimes corrected for CCD summing.
;              Ed Esfandiari 01/10/05 - Added lowgain exptime correction.
;              Ed Esfandiari 08/22/06 - Added new capacity and rates (channels).
;              Ed Esfandiari 08/30/06 - use rtrate from common block.
;              Ed Esfandiari 09/01/06 - Added GT rates.
;              Ed Esfandiari 11/21/06 - Ignore ccd_sum and low gain exptime correction (FSW does not do it).
;
; $Log: os_summary_text.pro,v $
; Revision 1.9  2009/09/11 20:28:16  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.4  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:05  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO ADD_DOOR_TEXT,times,tel,dlp,spacecraft,schedv_sc

   IF (DATATYPE(times) NE 'DOU') THEN RETURN

   dtimes= times
   sc= spacecraft

   ocnt= N_ELEMENTS(times)
   ok= INDGEN(ocnt)
   IF (schedv_sc EQ 1) THEN ok= WHERE(sc EQ 'A', ocnt)
   IF (schedv_sc EQ 2) THEN ok= WHERE(sc EQ 'B', ocnt)
   IF (ocnt GT 0) THEN BEGIN
       dtimes= dtimes(ok)
       sc = sc(ok)
   ENDIF ELSE dtimes= -1

   IF (DATATYPE(dtimes) EQ 'DOU') THEN BEGIN
     ct= UTC2STR(TAI2UTC(dtimes),/ECS)

     IF (dlp EQ 8) THEN BEGIN ; closed doors
       kcd= WHERE(ct GT '2003/12/31', kcnt) 
       slp='  LP = Close Door'
     ENDIF ELSE BEGIN ; open doors (dlp=9)
       kcd= WHERE(ct GT '2003/12/31' AND ct LT '2500/01/01', kcnt)
       slp='  LP = Open Door'
     ENDELSE

     IF(kcnt GT 0) THEN BEGIN
       PRINT & PRINT
       PRINT, 'OS_NUM = 0001'
       PRINT, slp 
       PRINT, '  Telescope = '+STRTRIM(tel,2)
       PRINT, '  # Scheduled= '+STRTRIM(kcnt,2)
       cstart= '  Times = ('
       scid= 'SpaceCraft = ('
       FOR i= 0, kcnt-1 DO BEGIN
         cstart= cstart + STRMID(ct(kcd(i)),0,4)+STRMID(ct(kcd(i)),5,2)+STRMID(ct(kcd(i)),8,2)+'_'+ $
                          STRMID(ct(kcd(i)),11,2)+STRMID(ct(kcd(i)),14,2)+STRMID(ct(kcd(i)),17,2)
         scid= scid+sc(i)
         IF (i LT kcnt-1) THEN BEGIN 
           cstart= cstart + ',' 
           scid= scid + ',' 
         ENDIF ELSE BEGIN 
           cstart= cstart + ')'
          scid= scid + ')'
         ENDELSE
       END
       PRINT, cstart
       PRINT,scid
     ENDIF
   ENDIF
   RETURN
END



PRO ADD_GT_TEXT,stime,etime,apid,lp,gt_sc,sc,rate

 IF (DATATYPE(stime) EQ 'DOU') THEN BEGIN
   wcnt= N_ELEMENTS(gt_sc)
   w= INDGEN(wcnt)
   IF (sc EQ 1) THEN w= WHERE(gt_sc EQ 'A' OR gt_sc EQ 'AB', wcnt)
   IF (sc EQ 2) THEN w= WHERE(gt_sc EQ 'B' OR gt_sc EQ 'AB', wcnt)

   IF (wcnt GT 0) THEN BEGIN
     stm= UTC2STR(TAI2UTC(stime(w)),/ECS)
     etm= UTC2STR(TAI2UTC(etime(w)),/ECS)

     glp='  LP = Guide Telescope Data Dump'

     PRINT & PRINT
     PRINT, 'OS_NUM = 0002'
     PRINT, glp 
     gcnt= N_ELEMENTS(stm)
     PRINT, '  # Scheduled= '+STRTRIM(gcnt,2)
     gstart= '  Start Times = ('
     gend=   '  Stop  Times = ('
     gapid=  '  APIDs       = ('
     grate=  '  GT Rates    = ('

     FOR i= 0, gcnt-1 DO BEGIN
       gstart= gstart + STRMID(stm(i),0,4)+STRMID(stm(i),5,2)+STRMID(stm(i),8,2)+'_'+ $
                        STRMID(stm(i),11,2)+STRMID(stm(i),14,2)+STRMID(stm(i),17,2)
       gend= gend     + STRMID(etm(i),0,4)+STRMID(etm(i),5,2)+STRMID(etm(i),8,2)+'_'+ $
                        STRMID(etm(i),11,2)+STRMID(etm(i),14,2)+STRMID(etm(i),17,2)
       gapid= gapid   + STRTRIM(apid(w(i)),2)
       grate= grate   + STRTRIM(rate(w(i)),2)

       IF (i LT gcnt-1) THEN BEGIN 
         gstart= gstart + ',' 
         gend= gend + ',' 
         gapid= gapid + ','  
         grate= grate + ','
       ENDIF ELSE BEGIN 
         gstart= gstart + ')'
         gend= gend + ')'
         gapid= gapid +')'
         grate= grate +')'
       ENDELSE
     END
     PRINT, gstart
     PRINT, gend
     PRINT, gapid
     PRINT,'  S/C         = ('+ARR2STR(gt_sc(w))+')'
     PRINT, grate
   ENDIF
 ENDIF

 RETURN
END



PRO OS_SUMMARY_TEXT

COMMON OP_SCHEDULED
COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
   ip_arr, plan_lp, lprow, cor1pw, cor2pw  ;AEE - 12/29/03
COMMON DIALOG, mdiag,font

COMMON SCIP_DOORS_SHARE    ; AEE - 06/10/03 - add door close/open commands.
COMMON GT_DUMPS_SHARE      ; AEE - 07/25/03
COMMON LP_CAL_LAMP_SHARE, lpcalv ; AEE - 08/20/03
COMMON SCHED_SHARE, schedv
COMMON RT_CHANNEL, rt_rates, rtrate

   ADD_DOOR_TEXT,cdoor_euvi,'EUVI',8,sc_euvi,schedv.sc   ; 8=close-door
   ADD_DOOR_TEXT,odoor_euvi,'EUVI',9,sc_euvi,schedv.sc   ; 9=open-door
   ADD_DOOR_TEXT,cdoor_cor1,'COR1',8,sc_cor1,schedv.sc
   ADD_DOOR_TEXT,odoor_cor1,'COR1',9,sc_cor1,schedv.sc
   ADD_DOOR_TEXT,cdoor_cor2,'COR2',8,sc_cor2,schedv.sc
   ADD_DOOR_TEXT,odoor_cor2,'COR2',9,sc_cor2,schedv.sc

   ;ADD_GT_TEXT,stime_gt,etime_gt,apid_gt,10,sc_gt,schedv.sc ; LP=10 is for GT-dumps
   ADD_GT_TEXT,stime_gt,etime_gt,apid_gt,10,sc_gt,schedv.sc,rate_gt ; LP=10 is for GT-dumps

   IF (DATATYPE(os_arr(0)) EQ 'INT') THEN BEGIN
      WIDGET_CONTROL,mdiag,SET_VALUE="%%OS_SUMMARY_TEXT: No OS's scheduled"
      PRINT, "%%OS_SUMMARY_TEXT: No OS's scheduled"
      RETURN
   ENDIF
   os_arr2 = os_arr(SORT([os_arr.os_num]))   ;** sort on os_num
   uniq_os_arr2 = os_arr2(UNIQ([os_arr2.os_num]))
   num_os = N_ELEMENTS(uniq_os_arr2)
 
   IF (SEXIST(tele_types) EQ 0) THEN OS_INIT	;** define some variables

   FOR i = 0, num_os-1 DO BEGIN    ;** for every unique os_num

    os_num = uniq_os_arr2(i).os_num

    ind1 = WHERE(defined_os_arr.os_num EQ os_num,ind1_cnt) ; AEE - 1/8/03

    IF (ind1_cnt LT 0) THEN GOTO, CONT

    FOR ind1_loop= 0, ind1_cnt -1 DO BEGIN
      ind= ind1(ind1_loop)
      os = defined_os_arr(ind)
      lp = defined_os_arr(ind).lp
      tele = defined_os_arr(ind).tele
      fw = defined_os_arr(ind).fw
      pw = defined_os_arr(ind).pw
      exptable = defined_os_arr(ind).exptable ; AEE 1/14/04
      camtable = defined_os_arr(ind).camtable ; AEE 1/14/04
      fps = defined_os_arr(ind).fps ; AEE 1/14/04
      iptable = defined_os_arr(ind).iptable
      lamp = defined_os_arr(ind).lamp
      start = defined_os_arr(ind).start
      num_images = defined_os_arr(ind).num_images
      ccd = defined_os_arr(ind).ccd
      ip = defined_os_arr(ind).ip
      ex = defined_os_arr(ind).ex
      occ_blks = REFORM(defined_os_arr(ind).occ_blocks(tele,*))
      roi_blks = REFORM(defined_os_arr(ind).roi_blocks(tele,*))

    indm = WHERE(ip(iptable).steps EQ 28) ; 28 = don't use occ mask table. 
    IF (indm(0) GE 0) THEN useocc = 0 ELSE useocc = 1  ; Also see os_get_num_pixels.pro
                                                      ;          generate_ipt.pro
                                                      ;          generate_ipt_set.pro
                                                      ;          os_summary.pro
                                                      ;          os_summary_1line.pro
                                                      ;          os_summary_set.pro
                                                      ;          os_summary_text.pro


   ;indm = WHERE(ip(iptable).steps EQ 25)
   ;IF (indm(0) LT 0) THEN useroi = 0 ELSE useroi = 1

   roi_table= WHICH_ROI_TABLE(ip,iptable)
   IF (roi_table EQ -1) THEN useroi = 0 ELSE useroi = 1

   CASE (1) OF
      ((useocc EQ 1) AND (useroi EQ 0)) : BEGIN
         blocks = WHERE(occ_blks GT 0,num)
         blockstr = 'Occulter (' + STRTRIM(num,2) + ' blocks)'
      END
      ((useroi EQ 1) AND (useocc EQ 0)) : BEGIN
         blocks = WHERE(roi_blks GT 0,num)
         blockstr = 'ROI (' + STRTRIM(num,2) + ' blocks)'
      END
      ((useroi EQ 1) AND (useocc EQ 1)) : BEGIN
         blocks = WHERE((occ_blks+roi_blks) GT 0,num)
         blockstr = 'Occulter & ROI (' + STRTRIM(num,2) + ' blocks)'
      END
      ELSE : BEGIN
         blocks = -1
         blockstr = 'None'
      END
   ENDCASE


      ind = 0
      add = 92	

      ;str = STRTRIM(STRING(os_num),2)
      str = STRING(os_num,FORMAT='(I4.4)')
      PRINT & PRINT
      WIDGET_CONTROL,mdiag,SET_VALUE='OS_NUM = ' + str
      PRINT, 'OS_NUM = ' + str
      str = STRTRIM(op_types(lp),2)
      IF (num_images GT 1) THEN BEGIN
        IF (LP eq 6) THEN $ ; HI Seq
          str = str + ' ('+STRTRIM(num_images,2)+' Images)' $
        ELSE $  ; SEQ PW
          str = str + ' ('+STRTRIM(num_images,2)+' Steps)' 
      ENDIF 

      lamp_used=''
      IF (lp EQ 3) THEN BEGIN ; AEE 8/20/03
        RESTORE, FILENAME= GETENV('PT')+'/IN/OTHER/secchi_cal_leds.sav'
        ; => euvi_lamps, cor1_lamps, cor2_lamps, hi1_lamps, hi2_lamps

        CASE tele of
           0: lamp_used= ' (' +euvi_lamps(lamp)+')'
           1: lamp_used= ' (' +cor1_lamps(lamp)+')'
           2: lamp_used= ' (' +cor2_lamps(lamp)+')'
           3: lamp_used= ' (' +hi1_lamps(lamp)+')'
           4: lamp_used= ' (' +hi2_lamps(lamp)+')'
        ENDCASE
      ENDIF
      IF (num_images EQ 1 AND LP EQ 6) THEN str = str + ' ('+STRTRIM(num_images,2)+' Image)'
      PRINT, '         LP = ' + str + lamp_used ; AEE 8/20/03

      IF (lp EQ 7) THEN BEGIN ;** Block Seq ; AEE - Dec 10, 02
         PRINT, 'START_INDEX = '+STRTRIM(STRING(start),2)
         PRINT, 'STOP_INDEX = '+STRTRIM(STRING(num_images),2)
         GOTO, CONT1
      ENDIF

      str = STRMID(tele_types(tele),11,4)
      PRINT, '  Telescope = ' + str
      str = STRTRIM(exptable+1,2)   ; AEE 1/14/04
      PRINT, 'Exposur DPT = ' + str
      str = STRTRIM(camtable+1,2)   ; AEE 1/14/04
      PRINT, ' Camera DPT = ' + str
      str = STRTRIM(fps,2)   ; AEE 1/14/04
      PRINT, '        FPS = ' + str
      str = STRTRIM(iptable,2)
      PRINT, '   IP TABLE = ' + str
      ;str = STRTRIM(STRMID(fw_types(exptable,fw,tele),0,13), 2) ; AEE 4/5/04
      str = STRING(FILTER2STR(fw_types,exptable,fw,tele), FORMAT='(a7)')
      PRINT, '     Filter = ' + str

      IF (tele EQ 0) THEN str0 = '     Sector = ' ELSE str0 = '  Polarizer = ' ; AEE - Dec 10, 02
      str1 = ''
      str2 = ''

         ; For non cal images, report exptime that is corrected for CCD summing:

         ccdsum= 1
         IF (lp NE 2 AND lp NE 3) THEN BEGIN
           xs = ccd(tele,camtable).xsum
           ys = ccd(tele,camtable).ysum
           ccdsum= ((xs > 1) * (ys > 1))
           IF (ccd(tele,camtable).gmode EQ 1) THEN ccdsum= ccdsum/4.0 ; for low gain, amplify exptime by 4
         ENDIF

         ccdsum=1 ; Ignore ccd_sum and low gain exptime correction (FSW does not do it).

      FOR n=0, num_images -1 DO BEGIN        ;** for each pw position (for no seq-images, n=0 always)
         IF (tele LE 2) THEN BEGIN
           ;str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(n),tele),0,10),2) + '  ' ; AEE 4/5/04
           str1 = str1 + POLAR2STR(pw_types,exptable,pw(n),tele) + '  '
           IF (lp EQ 4) THEN BEGIN 
             str2 = str2 + STRTRIM(0,2) + ','   ; AEE 5/22/03
           ENDIF ELSE BEGIN
             str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(n))/ccdsum,FORMAT='(I7)'),2) + '  '
           ENDELSE
           IF (lp EQ 0) THEN BEGIN  ; AEE - 9/24/03
             ; For double sky images, add the second pw and exp.time (for these images n is always 0) 
             ; and pw(1) indicates the second pw positions and exptime to be used.
             ;str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(1),tele),0,10),2) + '  ' ; AEE 4/5/04
             str1 = str1 + POLAR2STR(pw_types,exptable,pw(1),tele) + '  ' ; AEE 4/5/04
             str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(1))/ccdsum,FORMAT='(I7)'),2) + '  '
           ENDIF
         ENDIF ELSE BEGIN ; AEE, 01/23/03 - HI does not have polar (and pw is only a 20 element array)
          ;Note: HI telescpes do not have a shtter, polarizer, or filter wheels so pw(0) is only used.
          ;str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(0),tele),0,10),2) + '  '  ; AEE 4/5/04
          str1 = str1 + POLAR2STR(pw_types,exptable,pw(0),tele) + '  '  ; AEE 4/5/04
           IF (lp EQ 4) THEN BEGIN 
             str2 = str2 + STRTRIM(0,2) + ','   ; AEE 5/22/03
           ENDIF ELSE BEGIN
             str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2) + '  ' 
           ENDELSE
           IF (lp EQ 0) THEN BEGIN
             ; For double sky images, add the second pw and exp.time (for these images n is always 0)
             ; and since it is a HI image, the second pw and, therefore, exp.time are the same as the 
             ; first.
             ;str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(0),tele),0,10),2) + '  ' ; AEE 4/5/04
             str1 = str1 + POLAR2STR(pw_types,exptable,pw(0),tele) + '  ' ; AEE 4/5/04
             str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2) + '  ' 
           ENDIF
         ENDELSE
      ENDFOR

      ; For cal lamps, assign the pulse to the exptime:  ; AEE 8/22/03

      PRINT, str0+str1

      CASE (lp) OF  ; AEE 9/23/03
        3: PRINT, '      Pulse = ' + str2
        4: PRINT, '    Exptime = ' +'0 (N/A)'
        ELSE: PRINT, '    Exptime = ' + str2
      ENDCASE 

      ccd1 = ccd(tele,camtable)
      str1 = 'x1='+STRING(ccd1.x1, FORMAT='(i4)') + $
              ' x2='+STRING(ccd1.x2, FORMAT='(i4)') + $
              ' xsum='+STRING(ccd1.xsum, FORMAT='(i2)')
      str2 = 'y1='+STRING(ccd1.y1, FORMAT='(i4)') + $
              ' y2='+STRING(ccd1.y2, FORMAT='(i4)') + $
              ' ysum='+STRING(ccd1.ysum, FORMAT='(i2)')
      PRINT, '        CCD = ' + str1
      PRINT, '              ' + str2

      good = WHERE(ip(iptable).steps GT 0, nip) ; AEE - 02/04/03 - don't pickup anything if selected 
                                                ; IP table is empty (which should not be).
      FOR n=0, nip-1 DO BEGIN
         IF (n EQ 0) THEN str0 = ' Image Proc = ' ELSE str0 = '              '
         str = STRING(ip_arr(ip(iptable).steps(good(n))).ip_description,FORMAT='(a30)') ; AEE 5/5/04
         PRINT, str0 + str
      ENDFOR

      ; AEE - 02/04/03 - Added compression string:

      str = GET_COMP_STR(os)
      PRINT,' Compression String = ' + str

      PRINT, '    Masking = ' + blockstr

CONT1:

      ind2 = WHERE(os_arr.os_num EQ os_num AND os_arr.os_tele EQ tele)

      IF (ind2(0) GE 0) THEN BEGIN

         str = STRING(SCHEDULE_CONVERT_UNITS(os_arr(ind2(0)).os_size, 0, 2), FORMAT='(f5.1)') + ' % buffer'
         PRINT, '    OS_SIZE = ' + str
         num = N_ELEMENTS(ind2)
         str = STRTRIM(num, 2)
         PRINT, '# Scheduled = ' + str

         str= ARR2STR(os_arr(ind2).sc)
         PRINT, '        S/C = '+ str

         dest= WHERE(ip(iptable).steps(good) GE 39 AND ip(iptable).steps(good) LE 42, dcnt)
         ;RESTORE, GETENV('PT')+'/IN/OTHER/'+'secchi_downlink_channels.sav'
         channels=  READ_SSR_INFO()
;         RESTORE, GETENV('PT')+'/IN/OTHER/'+'rt_channel.sav'
         tsize= TOTAL(os_arr(ind2).os_size)

         FOR id=0, dcnt-1 DO BEGIN
           CASE ip(iptable).steps(good(dest(id))) OF
             39: BEGIN ; SSR1RT
               str= [STRING(tsize / channels.ssr1_rate, FORMAT='(I7)')+' Secs over SSR1', $
                     ;STRING(tsize / (rt_rate*272.0*8.0), FORMAT='(I7)')+' Secs over RT']
                     STRING(tsize / rtrate, FORMAT='(I7)')+' Secs over RT']
                 END
             40: str= STRING(tsize / channels.sw_rate, FORMAT='(I7)')+' Secs over SW'
             41: str= STRING(tsize / channels.ssr1_rate, FORMAT='(I7)')+' Secs over SSR1'
             42: str= STRING(tsize / channels.ssr2_rate, FORMAT='(I7)')+' Secs over SSR2'
           ENDCASE
           PRINT,'  Download Time = ' + STRTRIM(str(0), 2)
           IF (ip(iptable).steps(good(dest(id))) EQ 39) THEN BEGIN
             PRINT,'  Download Time = ' + STRTRIM(str(1), 2)
           ENDIF
      ENDFOR

      ENDIF 

    ENDFOR ;ind1loop 
    CONT:
   ENDFOR

END
 

;________________________________________________________________________________________________________
;
