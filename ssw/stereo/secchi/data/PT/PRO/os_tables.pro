;+
;$Id: os_tables.pro,v 1.17 2011/06/14 15:27:57 mcnutt Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : OS_TABLES
;	      : TABLE_CAMERA		Implemented.
;	      : TABLE_PROCESSING	Implemented.
;	      : TABLE_EXPOSURE		Implemented.
;	      : TABLE_OCCULTER		Implemented.
;	      : TABLE_MASK		Implemented.
;
; Purpose     : Widgets to define SEB Tables.
;
; Explanation : These routines create a widget interface for a specific
;               SECCHI SEB Table definition.
;
; Use         : TABLE_CAMERA, caller
;
; Inputs      : caller  structure containing id of caller.
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/07/04 - modified exp-tables Help menu and deactivated "Load Defaults".
;              Ed Esfandiari 07/09/04 - Use 24 polar pos. for COR1 & COR2 instead of 20. Also use
;                                       exd instead os ex to display default exposure times.
;              Ed Esfandiari 07/13/04 - Use new IP function "Send Raw Image" and treat it as
;                                       "No Compression" for PT purposes.
;              Ed Esfandiari 11/15/04 - Used WIDGET_DROPLIST for the longer Image-Proc list.
;              Ed Esfandiari 03/09/05 - Changed help comments of exposure units.
;              Ed Esfandiari 03/11/05 - For IDL v5.6+ use WIDGET_COMBOBOX instead of WIDGET_DROPLIST
;                                       since droplist can't show all of 80 IP table names. 
;              Ed Esfandiari 04/18/05 - Changed from 5 camera tables per camera to 7.
;              Ed Esfandiari 08/17/05 - Added CCD readout times to camera tool display.
;              Ed Esfandiari 10/06/05 - Added ICER compressions (IP steps #82 to 93).
;              Ed Esfandiari 11/09/05 - icer functions changed to steps #90 to 101.
;              Ed Esfandiari 08/22/06 - Added RETAIN=2 to draw widget.
;              Ed Esfandiari 11/21/06 - Removed ccd_sum and low gain exptime correction Help comments (FSW does not do it).
;              Ed Esfandiari 12/05/06 - Added table info (from tables_used) to table widgets. Also, added IP table search.
;              Ed Esfandiari 08/14/09  Changed SPAWN Id to Id to avoid cvs confusion.
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15.
;
; $Log: os_tables.pro,v $
; Revision 1.17  2011/06/14 15:27:57  mcnutt
; use getenv instead of $TABLES
;
; Revision 1.16  2009/09/11 20:28:16  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.9  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.8  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.7  2005/04/27 20:38:38  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.6  2005/03/10 16:48:13  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.5  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.3  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.2  2004/07/12 13:17:53  esfand
; now using 24 COR1 and COR2 polar positions
;
; Revision 1.1.1.2  2004/07/01 21:19:05  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-
 
;__________________________________________________________________________________________________________
;

;**
;** Routine to initialize table upload routines used by schedule.pro
;**
;** PRO TABLE_CAMERA
;** PRO TABLE_PROCESSING
;** PRO TABLE_EXPOSURE
;** PRO TABLE_OCCULTER
;** PRO TABLE_MASK
;**
;**

;________________________________________________________________________________________________________
;

PRO TABLE_CAMERA_EVENT, event

COMMON OS_ALL_SHARE
COMMON OS_INIT_SHARE
COMMON OS_TABLE_CAMERA_SHARE, ccdv

CASE (event.id) OF

   ccdv.dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, ccdv.base
      GOTO, done
   END

   ccdv.helpb : BEGIN	;** display help
      help_str = STRARR(10)
      help_str(0) = 'These tables define the camera state for each telescope.'
   ;   help_str(1) = 'The first 20 columns on the CCD are underscan.'
   ;   help_str(2) = 'In the x2: field you can enter +value where value is the number'
   ;   help_str(3) = 'of columns you wish to have starting with x1. Ex. +512'
      POPUP_HELP, help_str, TITLE="CAMERA TABLE HELP"
   END

   ccdv.telepd : BEGIN
      ccdv.tele = event.index
      WIDGET_CONTROL, ccdv.x1_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).x1,2)
      WIDGET_CONTROL, ccdv.x2_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).x2,2)
      WIDGET_CONTROL, ccdv.y1_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).y1,2)
      WIDGET_CONTROL, ccdv.y2_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).y2,2)
      WIDGET_CONTROL, ccdv.xsum_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).xsum,2)
      WIDGET_CONTROL, ccdv.ysum_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).ysum,2)
      WIDGET_CONTROL, ccdv.clearpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).clr_id,2)
      WIDGET_CONTROL, ccdv.rdoutpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).rdout_id,2)
      WIDGET_CONTROL, ccdv.rotime, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).ro_time,2)
      WIDGET_CONTROL, ccdv.offsetpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).offset,2)
      WIDGET_CONTROL, ccdv.gainpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).gain,2)
      WIDGET_CONTROL, ccdv.gmodepd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).gmode,2)
      plat_forms= ['A','B']
      WIDGET_CONTROL, ccdv.pformpd, SET_VALUE= plat_forms(ccd(ccdv.tele,ccdv.table).pform)

      PLOT_CCD, ccdv.draw_win, ccdv.tele, $
         ccd(ccdv.tele,ccdv.table).x1, ccd(ccdv.tele,ccdv.table).x2, $
         ccd(ccdv.tele,ccdv.table).y1, ccd(ccdv.tele,ccdv.table).y2
   END

   ccdv.tablepd : BEGIN
      ccdv.table = event.index
      WIDGET_CONTROL, ccdv.x1_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).x1,2)
      WIDGET_CONTROL, ccdv.x2_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).x2,2)
      WIDGET_CONTROL, ccdv.y1_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).y1,2)
      WIDGET_CONTROL, ccdv.y2_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).y2,2)
      WIDGET_CONTROL, ccdv.xsum_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).xsum,2)
      WIDGET_CONTROL, ccdv.ysum_text, SET_VALUE=STRTRIM(ccd(ccdv.tele,ccdv.table).ysum,2)
      WIDGET_CONTROL, ccdv.clearpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).clr_id,2)
      WIDGET_CONTROL, ccdv.rdoutpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).rdout_id,2)
      WIDGET_CONTROL, ccdv.rotime, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).ro_time,2)
      WIDGET_CONTROL, ccdv.offsetpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).offset,2)
      WIDGET_CONTROL, ccdv.gainpd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).gain,2)
      WIDGET_CONTROL, ccdv.gmodepd, SET_VALUE= STRTRIM(ccd(ccdv.tele,ccdv.table).gmode,2)
      plat_forms= ['A','B']
      WIDGET_CONTROL, ccdv.pformpd, SET_VALUE= plat_forms(ccd(ccdv.tele,ccdv.table).pform)

      PLOT_CCD, ccdv.draw_win, ccdv.tele, $
         ccd(ccdv.tele,ccdv.table).x1, ccd(ccdv.tele,ccdv.table).x2, $
         ccd(ccdv.tele,ccdv.table).y1, ccd(ccdv.tele,ccdv.table).y2
   END

   ccdv.x1_text : BEGIN
      WIDGET_CONTROL, ccdv.x1_text, GET_VALUE=x1 & x1 = x1(0)
      temp = P_CHECK(x1,'x1',new) & WIDGET_CONTROL, ccdv.x1_text, SET_VALUE=new
      WIDGET_CONTROL, ccdv.x2_text, /INPUT_FOCUS
      WIDGET_CONTROL, ccdv.x2_text, GET_VALUE=x2
      WIDGET_CONTROL, ccdv.x2_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(x2),2))]
      PLOT_CCD, ccdv.draw_win, ccdv.tele, $
         x1, ccd(ccdv.tele,ccdv.table).x2, $
         ccd(ccdv.tele,ccdv.table).y1, ccd(ccdv.tele,ccdv.table).y2
   END

   ccdv.x2_text : BEGIN
      WIDGET_CONTROL, ccdv.x2_text, GET_VALUE=x2 & x2 = x2(0)
      temp = P_CHECK(x2,'x2',new) & WIDGET_CONTROL, ccdv.x2_text, SET_VALUE=new
      WIDGET_CONTROL, ccdv.y1_text, /INPUT_FOCUS
      WIDGET_CONTROL, ccdv.y1_text, GET_VALUE=y1
      WIDGET_CONTROL, ccdv.y1_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(y1),2))]
      PLOT_CCD, ccdv.draw_win, ccdv.tele, $
         ccd(ccdv.tele,ccdv.table).x1, x2, $
         ccd(ccdv.tele,ccdv.table).y1, ccd(ccdv.tele,ccdv.table).y2
   END

   ccdv.y1_text : BEGIN
      WIDGET_CONTROL, ccdv.y1_text, GET_VALUE=y1 & y1 = y1(0)
      temp = P_CHECK(y1,'y1',new) & WIDGET_CONTROL, ccdv.y1_text, SET_VALUE=new
      WIDGET_CONTROL, ccdv.y2_text, /INPUT_FOCUS
      WIDGET_CONTROL, ccdv.y2_text, GET_VALUE=y2
      WIDGET_CONTROL, ccdv.y2_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(y2),2))]
      PLOT_CCD, ccdv.draw_win, ccdv.tele, $
         ccd(ccdv.tele,ccdv.table).x1, ccd(ccdv.tele,ccdv.table).x2, $
         y1, ccd(ccdv.tele,ccdv.table).y2
   END

   ccdv.y2_text : BEGIN
      WIDGET_CONTROL, ccdv.y2_text, GET_VALUE=y2 & y2 = y2(0)
      temp = P_CHECK(y2,'y2',new) & WIDGET_CONTROL, ccdv.y2_text, SET_VALUE=new
      WIDGET_CONTROL, ccdv.xsum_text, /INPUT_FOCUS
      WIDGET_CONTROL, ccdv.xsum_text, GET_VALUE=xsum
      WIDGET_CONTROL, ccdv.xsum_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(xsum),2))]
      PLOT_CCD, ccdv.draw_win, ccdv.tele, $
         ccd(ccdv.tele,ccdv.table).x1, ccd(ccdv.tele,ccdv.table).x2, $
         ccd(ccdv.tele,ccdv.table).y1, y2
   END

   ccdv.xsum_text : BEGIN
      WIDGET_CONTROL, ccdv.xsum_text, GET_VALUE=xsum & xsum = xsum(0)
      temp = P_CHECK(xsum,'xsum',new) & WIDGET_CONTROL, ccdv.xsum_text, SET_VALUE=new
      WIDGET_CONTROL, ccdv.ysum_text, /INPUT_FOCUS
      WIDGET_CONTROL, ccdv.ysum_text, GET_VALUE=ysum
      WIDGET_CONTROL, ccdv.ysum_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(ysum),2))]
   END

   ccdv.ysum_text : BEGIN
      WIDGET_CONTROL, ccdv.ysum_text, GET_VALUE=ysum & ysum = ysum(0)
      temp = P_CHECK(ysum,'ysum',new) & WIDGET_CONTROL, ccdv.ysum_text, SET_VALUE=new
      WIDGET_CONTROL, ccdv.x1_text, /INPUT_FOCUS
      WIDGET_CONTROL, ccdv.x1_text, GET_VALUE=x1
      WIDGET_CONTROL, ccdv.x1_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(x1),2))]
   END

   ccdv.clear_text : BEGIN
      WIDGET_CONTROL, ccdv.clear_text, GET_VALUE=nclrs & nclrs = nclrs(0)
      temp = P_CHECK(nclrs,'nclrs',new) & WIDGET_CONTROL, ccdv.clear_text, SET_VALUE=new
   END

   ccdv.lowhtr_text : BEGIN
      WIDGET_CONTROL, ccdv.lowhtr_text, GET_VALUE=lowhtr & lowhtr = lowhtr(0)
      temp = P_CHECK(lowhtr,'lowhtr',new) & WIDGET_CONTROL, ccdv.lowhtr_text, SET_VALUE=new
   END

   ELSE : BEGIN
   END

ENDCASE

WIDGET_CONTROL, ccdv.x1_text, GET_VALUE=x1
WIDGET_CONTROL, ccdv.x2_text, GET_VALUE=x2
WIDGET_CONTROL, ccdv.y1_text, GET_VALUE=y1
WIDGET_CONTROL, ccdv.y2_text, GET_VALUE=y2
WIDGET_CONTROL, ccdv.xsum_text, GET_VALUE=xsum
WIDGET_CONTROL, ccdv.ysum_text, GET_VALUE=ysum
WIDGET_CONTROL, ccdv.clearpd, GET_VALUE=clr_id
WIDGET_CONTROL, ccdv.rdoutpd, GET_VALUE=rdout_id
WIDGET_CONTROL, ccdv.rotime, GET_VALUE=ro_time
WIDGET_CONTROL, ccdv.offsetpd, GET_VALUE=offset
WIDGET_CONTROL, ccdv.gainpd, GET_VALUE=gain
WIDGET_CONTROL, ccdv.gmodepd, GET_VALUE=gmode
WIDGET_CONTROL, ccdv.pformpd, GET_VALUE= pform

ccd(ccdv.tele,ccdv.table).x1 = FIX(x1(0))
ccd(ccdv.tele,ccdv.table).x2 = FIX(x2(0))
ccd(ccdv.tele,ccdv.table).y1 = FIX(y1(0))
ccd(ccdv.tele,ccdv.table).y2 = FIX(y2(0))
ccd(ccdv.tele,ccdv.table).xsum = FIX(xsum(0))
ccd(ccdv.tele,ccdv.table).ysum = FIX(ysum(0))

done:
END

;________________________________________________________________________________________________________
;

PRO TABLE_CAMERA, caller

COMMON OS_ALL_SHARE
COMMON OS_TABLE_CAMERA_SHARE
COMMON OS_INIT_SHARE
COMMON TABLES_IN_USE, tables_used, tbAout, tbBout

    IF XRegistered("TABLE_CAMERA") THEN RETURN

   IF ((SIZE(ccdv))(1) EQ 0) THEN BEGIN		 ;** not defined yet use default
      tele = 0		
      table = 0
   ENDIF ELSE BEGIN
      tele = ccdv.tele		
      table = ccdv.table
   ENDELSE

   cc_opts = ['Long  - Begin OP', $
              'Short - After Image', $
              'Long  - After Image', $
              'Status + Mux', $
              'Short - Wobble', $
              'Short - Wobble']

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

; AEE 2/2/04 - changed the camera table to just display the information and not let the
;              user to change any of the parameters.

    ;base = WIDGET_BASE(/ROW, TITLE='SECCHI TABLE_CAMERA TOOL', /FRAME, GROUP_LEADER=caller.id)
    base = WIDGET_BASE(/ROW, TITLE='SECCHI TABLE_CAMERA TOOL  ('+tables_used(4)+')', /FRAME, GROUP_LEADER=caller.id)

      base1 = WIDGET_BASE(base, /COLUMN)

      row0 = WIDGET_BASE(base1, /ROW)
        telepd =  CW_BSELECTOR2(row0, tele_types, SET_VALUE=tele)
        ;tablepd = CW_BSELECTOR2(row0, table_types(0:4), SET_VALUE=table) ; AEE 4/2/04 - only 5 camera tables
        tablepd = CW_BSELECTOR2(row0, table_types, SET_VALUE=table) ; 7 camera tables

        platforms=['A','B']        ; AEE 3/30/04 - added
        temp=    WIDGET_LABEL(row0, VALUE='  SpaceCraft: ')
        pformpd= WIDGET_TEXT(row0, YSIZE=1, XSIZE=2, VALUE= platforms(ccd(tele,table).pform))

        temp=    WIDGET_LABEL(row0, VALUE='  ')
        helpb = WIDGET_BUTTON(row0, VALUE=' Help ')

      row = WIDGET_BASE(base1, /ROW)

       ccdcol = WIDGET_BASE(row, /COLUMN, /FRAME)

         camrow = WIDGET_BASE(ccdcol, /ROW, /FRAME)
           temp = WIDGET_LABEL(camrow, VALUE='          CCD Setup')

         camrow0 = WIDGET_BASE(ccdcol, /ROW)
           temp = WIDGET_LABEL(camrow0, VALUE='  x1:')
           ;x1_text = WIDGET_TEXT(camrow0, /EDITABLE, YSIZE=1, XSIZE=5, $
           x1_text = WIDGET_TEXT(camrow0, YSIZE=1, XSIZE=5, $  ; AEE 2/2/04
              VALUE=STRTRIM(STRING(ccd(tele,table).x1),2))
           temp = WIDGET_LABEL(camrow0, VALUE='  x2:')
           ;x2_text = WIDGET_TEXT(camrow0, /EDITABLE, YSIZE=1, XSIZE=5, $
           x2_text = WIDGET_TEXT(camrow0, YSIZE=1, XSIZE=5, $ ; AEE 2/2/04
              VALUE=STRTRIM(STRING(ccd(tele,table).x2),2))

         camrow1 = WIDGET_BASE(ccdcol, /ROW)
           temp = WIDGET_LABEL(camrow1, VALUE='  y1:')
           ;y1_text = WIDGET_TEXT(camrow1, /EDITABLE, YSIZE=1, XSIZE=5, $
           y1_text = WIDGET_TEXT(camrow1, YSIZE=1, XSIZE=5, $
              VALUE=STRTRIM(STRING(ccd(tele,table).y1),2)) ; AEE 2/2/04
           temp = WIDGET_LABEL(camrow1, VALUE='  y2:')
           ;y2_text = WIDGET_TEXT(camrow1, /EDITABLE, YSIZE=1, XSIZE=5, $
           y2_text = WIDGET_TEXT(camrow1, YSIZE=1, XSIZE=5, $ ; AEE 2/2/04
              VALUE=STRTRIM(STRING(ccd(tele,table).y2),2))

         camrow2 = WIDGET_BASE(ccdcol, /ROW)
           temp = WIDGET_LABEL(camrow2, VALUE='xsum:')
           ;xsum_text = WIDGET_TEXT(camrow2, /EDITABLE, YSIZE=1, XSIZE=5, $
           xsum_text = WIDGET_TEXT(camrow2, YSIZE=1, XSIZE=5, $ ; AEE 2/2/04
              VALUE=STRTRIM(STRING(ccd(tele,table).xsum),2))
           temp = WIDGET_LABEL(camrow2, VALUE='ysum:')
           ;ysum_text = WIDGET_TEXT(camrow2, /EDITABLE, YSIZE=1, XSIZE=5, $
           ysum_text = WIDGET_TEXT(camrow2, YSIZE=1, XSIZE=5, $ ; AEE 2/2/04
              VALUE=STRTRIM(STRING(ccd(tele,table).ysum),2))

         col = WIDGET_BASE(row, /COLUMN, /FRAME)
           row2 = WIDGET_BASE(col, /ROW) 
           temp = WIDGET_LABEL(row2, VALUE='CCD Clear Table:            ') 
           clearpd = WIDGET_TEXT(row2, YSIZE=1, XSIZE=2,VALUE=STRTRIM(ccd(tele,table).clr_id,2))

           row2 = WIDGET_BASE(col, /ROW)
           temp = WIDGET_LABEL(row2, VALUE='CCD Readout Table: ')
           rdoutpd = WIDGET_TEXT(row2, YSIZE=1, XSIZE=2,VALUE=STRTRIM(ccd(tele,table).rdout_id,2))
           temp = WIDGET_LABEL(row2, VALUE='ROT: ')
           rotime = WIDGET_TEXT(row2, YSIZE=1, XSIZE=3,VALUE=STRTRIM(ccd(tele,table).ro_time,2))

           row2 = WIDGET_BASE(col, /ROW)
           temp = WIDGET_LABEL(row2, VALUE='Offset (0-1023):            ')
           offsetpd = WIDGET_TEXT(row2, YSIZE=1, XSIZE=5,VALUE=STRTRIM(ccd(tele,table).offset,2))

           row2 = WIDGET_BASE(col, /ROW)
           temp = WIDGET_LABEL(row2, VALUE='Gain (0-127):          ')
           gainpd = WIDGET_TEXT(row2, YSIZE=1, XSIZE=4,VALUE=STRTRIM(ccd(tele,table).gain,2))

           row2 = WIDGET_BASE(col, /ROW)
           temp = WIDGET_LABEL(row2, VALUE='Gain Mode (0:hi,1=low): ')
           gmodepd = WIDGET_TEXT(row2, YSIZE=1, XSIZE=4,VALUE=STRTRIM(ccd(tele,table).gmode,2))

     dismiss = WIDGET_BUTTON(base1, VALUE=" Dismiss ")

     ;draw_w = WIDGET_DRAW(base, /FRAME, XSIZE=256+5, YSIZE=256)
     ;draw_w = WIDGET_DRAW(base, /FRAME, XSIZE=256, YSIZE=256)

     RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav'

     IF (xyblks EQ 34) THEN $
       draw_w = WIDGET_DRAW(base, /FRAME, XSIZE=272, YSIZE=272, RETAIN=2)

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
    WIDGET_CONTROL, x1_text, /INPUT_FOCUS
    WIDGET_CONTROL, x1_text, GET_VALUE=x1
    WIDGET_CONTROL, x1_text, SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(x1),2))]
    WIDGET_CONTROL, draw_w, GET_VALUE=draw_win
;    IF (tele NE 0) THEN WIDGET_CONTROL, cc_opts_id, SENSITIVE=0

    PLOT_CCD, draw_win, tele, $
       ccd(tele,table).x1, ccd(tele,table).x2, ccd(tele,table).y1, ccd(tele,table).y2

    ccdv = CREATE_STRUCT('base', base,			$
                         'dismiss', dismiss,		$
                         'draw_win', draw_win,		$
                         'x1_text', x1_text,		$
                         'x2_text', x2_text,		$
                         'y1_text', y1_text,		$
                         'y2_text', y2_text,		$
                         'xsum_text', xsum_text,	$
                         'ysum_text', ysum_text,	$
                         'telepd', telepd,		$
                         'tablepd', tablepd,		$
                         'helpb', helpb,		$
                         'pformpd', pformpd,            $
                         'clearpd', clearpd,		$
                         'rdoutpd', rdoutpd,            $
                         'rotime', rotime,              $
                         'offsetpd', offsetpd,          $
                         'gainpd', gainpd,              $
                         'gmodepd', gmodepd,            $
                         'table', table,                $
                         'tele', tele)

    XMANAGER, 'TABLE_CAMERA', base

END

;________________________________________________________________________________________________________
;

PRO TABLE_PROCESSING_EVENT, event

COMMON OS_ALL_SHARE
COMMON OS_INIT_SHARE
COMMON OS_TABLE_PROCESSING_SHARE, ipv
COMMON SRCH_LIST, srch_items

    CASE (event.id) OF
	ipv.dismiss : BEGIN	;** exit program

           ; AEE - 7/3/03 - make sure if any IP function is selected, there is the same # of
           ; compression-strings selected as apids and there is atleast one of each.
           ;
           ; Must use the "No Compression" and/or "NO APID" (should be added at the end of IP
           ; functions) if we intentionally don't want to compress an image or not to send
           ; it to the ground:

           selected = WHERE(ip(ipv.iptable).steps GT 0, sel_cnt)
           IF(sel_cnt GT 0) THEN BEGIN
             ; See if same number of compression and APID steps and, atleast, one of each:

             ;"No Compression" is the 6th function (step):
             ;comp_ind = WHERE(ip(ipv.iptable).steps GE 6 AND ip(ipv.iptable).steps LE 17, comp_cnt)
             ; Header Only", the 5th function, can also be used as a transmition (compression) method: 
             ;comp_ind = WHERE(ip(ipv.iptable).steps GE 5 AND ip(ipv.iptable).steps LE 17, comp_cnt)

             ; "Send Raw Image", step #44, can also be treated as the "No Compression", step #6 transmition method:
             ;comp_ind = WHERE(ip(ipv.iptable).steps GE 5 AND ip(ipv.iptable).steps LE 17 OR $
             ;comp_ind = WHERE(ip(ipv.iptable).steps GE 5 AND ip(ipv.iptable).steps LE 14 OR $
             comp_ind = WHERE(ip(ipv.iptable).steps GE 5 AND ip(ipv.iptable).steps LE 15 OR $
                              ip(ipv.iptable).steps GE 90 AND ip(ipv.iptable).steps LE 101 OR $
                              ip(ipv.iptable).steps EQ 44, comp_cnt)

             ;;"No APID" is the 59th (last) function (step):
             ; "Ground Test APID" is the 59th (last) function (step):
             ;apid_ind = WHERE(ip(ipv.iptable).steps GE 55 AND ip(ipv.iptable).steps LE 59, apid_cnt)

             ; "SSR1RT APID" is the 60th (last) function (step):
             ;apid_ind = WHERE(ip(ipv.iptable).steps GE 55 AND ip(ipv.iptable).steps LE 60, apid_cnt)
             apid_ind = WHERE(ip(ipv.iptable).steps GE 39 AND ip(ipv.iptable).steps LE 43, apid_cnt) ; AEE 5/3/04

             IF (comp_cnt NE apid_cnt) THEN BEGIN 
               ;str1= 'Warning: Number of Selected Compression-Strings <> Selected APIDs.'
               ;str2= ' Atleast One of Each Is Required And Compression Must Be The Last Step.' 
               ;WIDGET_CONTROL,ipv.msg,SET_VALUE=str1+str2
               PRINT,''
               PRINT,'Warning: Number of Selected Compression-Strings <> Selected APIDs.'
               PRINT,'         Atleast One of Each Is Required And Compression Must Be The Last Step.'
               PRINT,'' 
               RETURN
             ENDIF 

;             ; Now see if each compression step is preceded by an APID step:
;
;             FOR i= 0, comp_cnt-1 DO BEGIN
;               IF (apid_ind(i) NE comp_ind(i)-1) THEN BEGIN
;                 str1= 'Warning: An APID MUST Be Immediately Followed By a Compression Step. '
;                 str2= 'Correct And "Update" Again.'
;                 WIDGET_CONTROL,ipv.msg,SET_VALUE=str1+str2
;                 PRINT,''
;                 PRINT,str1+str2
;                 PRINT,''
;                 RETURN
;               ENDIF
;             ENDFOR


;             ; Now see if last step is "No Operation":
;
;             IF (ip(ipv.iptable).steps(selected(sel_cnt-1)) NE 54) THEN BEGIN
;               str1= 'Warning: Last Step MUST be "No Operation". Correct And "Update" Again.'
;               WIDGET_CONTROL,ipv.msg,SET_VALUE=str1
;               PRINT,''
;               PRINT,str1
;               PRINT,''
;               RETURN
;             ENDIF

           ENDIF

	   WIDGET_CONTROL, /DESTROY, ipv.base
	END

;	ipv.defaultb : BEGIN	;** load defaults
;           ip = ipd
;           good = WHERE(ip(ipv.iptable).steps GE 0)
;           IF (good(0) LT 0) THEN WIDGET_CONTROL, ipv.proclist2, SET_VALUE = "" $
;           ; AEE 5/5/04:
;           ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)).ip_description
;           ;ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)-1).ip_description
;           len = N_ELEMENTS(good) & IF (len GT 5) THEN WIDGET_CONTROL, ipv.proclist2, SET_LIST_TOP = len-5
;	END


	ipv.proclist : BEGIN
           good = WHERE(ip(ipv.iptable).steps GE 0)
           IF (good(0) LT 0) THEN $
              ip(ipv.iptable).steps(0) = event.index+1 $;** first selection
           ELSE BEGIN
              steps = ip(ipv.iptable).steps(WHERE(ip(ipv.iptable).steps GE 0))
              ip(ipv.iptable).steps = [steps, event.index+1]  ;** additional selection
           ENDELSE
           good = WHERE(ip(ipv.iptable).steps GE 0)
           IF (good(0) LT 0) THEN WIDGET_CONTROL, ipv.proclist2, SET_VALUE = "" $
           ; AEE 5/5/04:
           ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)).ip_description
           ;ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)-1).ip_description
           len = N_ELEMENTS(good) & IF (len GT 5) THEN WIDGET_CONTROL, ipv.proclist2, SET_LIST_TOP = len-5
	END

	ipv.proclist2 : BEGIN
           ; AEE 5/13/04 -added these 2 statements:
           ip_tmp= ip
           ip= ipd

           ip(ipv.iptable).steps(event.index) = -1
           good = WHERE(ip(ipv.iptable).steps GE 0)
           steps = ip(ipv.iptable).steps
           IF (good(0) GE 0) THEN BEGIN
              ip(ipv.iptable).steps = -1
              ip(ipv.iptable).steps(0:N_ELEMENTS(good)-1) = steps(good)
           ENDIF
           good = WHERE(ip(ipv.iptable).steps GE 0)
           IF (good(0) LT 0) THEN WIDGET_CONTROL, ipv.proclist2, SET_VALUE = "" $
           ; AEE 5/5/04:
           ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)).ip_description
           ;ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)-1).ip_description
           len = N_ELEMENTS(good) & IF (len GT 5) THEN WIDGET_CONTROL, ipv.proclist2, SET_LIST_TOP = len-5

           ip= ip_tmp ; AEE 5/13/04 -added this.
	END

        ipv.ip_table_pd : BEGIN		;** new table number

           ; AEE 5/13/04 -added these 2 statements:
           ip_tmp= ip
           ip= ipd

           ipv.iptable = event.index

           ;** list current contents for this table
           good = WHERE(ip(ipv.iptable).steps GE 0)
           IF (good(0) LT 0) THEN WIDGET_CONTROL, ipv.proclist2, SET_VALUE = "" $
           ; AEE 5/5/04:
           ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)).ip_description
           ;ELSE WIDGET_CONTROL, ipv.proclist2, SET_VALUE = ip_arr(ip(ipv.iptable).steps(good)-1).ip_description
           len = N_ELEMENTS(good) & IF (len GT 5) THEN WIDGET_CONTROL, ipv.proclist2, SET_LIST_TOP = len-5

           ip= ip_tmp ; AEE 5/13/04 -added this.
        END

        ipv.srch_for: BEGIN
           SPAWN, 'grep -h '+srch_items(event.index)+' '+GETENV('TABLES')+'/imagetbl.img',srch_result
           WIDGET_CONTROL, ipv.srch_list,SET_VALUE = srch_result
        END

        ELSE : BEGIN
        END

   ENDCASE

END

;________________________________________________________________________________________________________
;

PRO TABLE_PROCESSING, caller

COMMON OS_TABLE_PROCESSING_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE
COMMON TABLES_IN_USE, tables_used
COMMON SRCH_LIST, srch_items

   IF XRegistered("TABLE_PROCESSING") THEN RETURN

   IF ((SIZE(ipv))(1) EQ 0) THEN BEGIN		 ;** not defined yet use default
      iptable = 0
   ENDIF ELSE BEGIN
      iptable = ipv.iptable
   ENDELSE

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    ;base = WIDGET_BASE(/COLUMN, TITLE='SECCHI TABLE_IMAGE_PROCESSING TOOL', /FRAME, $
    base = WIDGET_BASE(/COLUMN, TITLE='SECCHI TABLE_IMAGE_PROCESSING TOOL ('+tables_used(0)+')', /FRAME, $
             GROUP_LEADER=caller.id)

      row1 = WIDGET_BASE(base, /ROW)
        blanks='                                                                               '
        txt= WIDGET_LABEL(row1,VALUE= blanks) ; AEE 5/5/04
        ;ip_table_pd = CW_BSELECTOR2(row1, proc_tab_types, SET_VALUE=iptable)
        IF (!version.release LT '5.6') THEN BEGIN 
          ip_table_pd = WIDGET_DROPLIST(row1, VALUE=proc_tab_types) 
          ; Since WIDGET_DROPLIST does not have a SET_VALUE, use WIDGET_CONTROL's
          ; SET_DROPLIST_SELECT instead:
          WIDGET_CONTROL, ip_table_pd, SET_DROPLIST_SELECT= iptable
        ENDIF ELSE BEGIN  
          ;ip_table_pd = WIDGET_COMBOBOX(row1, VALUE=proc_tab_types)
          ;WIDGET_CONTROL, ip_table_pd, SET_COMBOBOX_SELECT= iptable
          ip_table_pd = WIDGET_COMBOBOX_SHELL(row1,proc_tab_types,iptable) ; so idl5.3 does not complain.
        ENDELSE

;        defaultb = WIDGET_BUTTON(row1, VALUE='Load Defaults')

      row0 = WIDGET_BASE(base, /ROW, /FRAME)

        col0 = WIDGET_BASE(row0, /COLUMN)

          lbl =  WIDGET_LABEL(col0, VALUE = "Possible Image Processing Steps")
          ;proclist = WIDGET_LIST(col0, VALUE = ip_arr.ip_description, YSIZE = 9, XSIZE=30)

          ; AEE 5/5/04 - add step number (0-43) before description:

          ipsteps= STRTRIM(INDGEN(N_ELEMENTS(ip_arr.ip_description)),2)+': '
          ipsteps(0:9)= ' '+ipsteps(0:9)
          ;proclist = WIDGET_LIST(col0, VALUE = ipsteps+ip_arr.ip_description, YSIZE = 9, XSIZE=30)

          ;proclist = WIDGET_LIST(col0, VALUE = ipsteps+ip_arr.ip_description, YSIZE = 9, XSIZE=21) ;AEE 5/5/04

          ; AEE 5/5/04 - Make the steps non-selectable:
          ;proclist = WIDGET_TEXT(col0, VALUE = ipsteps+ip_arr.ip_description, YSIZE = 20, XSIZE=40, /SCROLL)
          proclist = WIDGET_TEXT(col0, VALUE = ipsteps+ip_arr.ip_description, YSIZE = 30, XSIZE=40, /SCROLL)

        col1 = WIDGET_BASE(row0, /COLUMN)

          ;lbl =  WIDGET_LABEL(col1, VALUE = "Selected Processing Steps")
          lbl =  WIDGET_LABEL(col1, VALUE = "Processing Steps Used in Selected Table") ; AEE 5/5/04
          ;proclist2 = WIDGET_LIST(col1, YSIZE = 9, XSIZE=30)
          ;proclist2 = WIDGET_LIST(col1, YSIZE = 9, XSIZE=21) ; AEE 5/5/04

          ; AEE 5/5/04 - Make the table steps non-selectable:
          proclist2 = WIDGET_TEXT(col1, YSIZE = 10, XSIZE=30, /SCROLL)

          ; Provide some IP table search:
          lbl =  WIDGET_LABEL(col1,VALUE='  ')
          col2= WIDGET_BASE(col1, /COLUMN, /FRAME)
          lbl =  WIDGET_LABEL(col2, VALUE = "Search IP Tables for ") ; AEE 5/5/04
          ;srch_items= ['IP ','EUVI','COR1','COR2','SCIP','HI1','HI2','HI','SSR','RT','SWB','RICE','HC','IC0','IC1']
          srch_items= ['IP ','EUVI','COR1','COR2','SCIP','HI1','HI2','HI','SSR','RT','SWB','RICE','HC', $
                       'HC0','HC1','HC2','HC3','HC4','HC5','HC6','HC7','HC8','HC9','IC0','IC1','IC00', $
                       'IC01','IC02','IC03','IC04','IC05','IC06','IC07','IC08','IC09','IC10','IC11','PSUM']
          srch_for= WIDGET_COMBOBOX_SHELL(col2,srch_items,0) ; so idl5.3 does not complain.
          
          srch_list = WIDGET_TEXT(col2, YSIZE = 10, XSIZE=50, /SCROLL)


;      ;AEE - 7/10/03
;      font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
;      blanks='                                                                         '
;      blanks= blanks+blanks+blanks
;      msg= WIDGET_LABEL(base,FONT=font,VALUE=blanks,/frame)

      ;dismiss = WIDGET_BUTTON(base, VALUE=" Dismiss ")
      ;dismiss = WIDGET_BUTTON(base, VALUE="Update")
      dismiss = WIDGET_BUTTON(base, VALUE="Dismiss")

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)

    good = WHERE(ip(iptable).steps GE 0)
    IF (good(0) LT 0) THEN WIDGET_CONTROL, proclist2, SET_VALUE = "" $
    ELSE WIDGET_CONTROL, proclist2, SET_VALUE = ip_arr(ipd(iptable).steps(good)).ip_description ; AEE 5/5/04

    ; AEE 7/7/03:
    len = N_ELEMENTS(good) & IF (len GT 5) THEN WIDGET_CONTROL, proclist2, SET_LIST_TOP = len-5

    SPAWN, 'grep -h '+srch_items(0)+' '+GETENV('TABLES')+'/imagetbl.img',srch_result    
    WIDGET_CONTROL, srch_list, SET_VALUE = srch_result

    ipv = CREATE_STRUCT( 'base', base,			$
                         'dismiss', dismiss,		$
                         'proclist', proclist,		$
                         'proclist2', proclist2,	$
                         'srch_for', srch_for,          $
                         'srch_list', srch_list,        $
                         'iptable', iptable,		$
                         'ip_table_pd', ip_table_pd)

    XMANAGER, 'TABLE_PROCESSING', base

END

PRO DISPLAY_EXP,exv,fw_types,pw_types,ex

      tel= exv.tele
      IF (tel EQ 2) THEN tel=1
      IF (tel EQ 4) THEN tel=3

      FOR fw = 0, 3 DO BEGIN  ; First, clear everything
        ;FOR pw= 0, 19 DO BEGIN
        FOR pw= 0, 23 DO BEGIN
          WIDGET_CONTROL, exv.labelw(fw,pw), SET_VALUE= ''
          WIDGET_CONTROL, exv.textw(fw,pw), SET_VALUE= ''
         ENDFOR
      ENDFOR

      CASE tel OF
        0: FOR fw = 0, 3 DO BEGIN
            FOR pw = 0, 3 DO BEGIN
              WIDGET_CONTROL, exv.labelw(fw,pw), SET_VALUE=fw_types(exv.table,fw,exv.tele)+','+ $
                              pw_types(exv.table,pw,exv.tele)
              WIDGET_CONTROL, exv.textw(fw,pw), SET_VALUE=STRTRIM(STRING(ex(exv.tele,exv.table,fw,pw)),2) 
            ENDFOR
           ENDFOR
        ;1: FOR pw = 0, 19 DO BEGIN 
        1: FOR pw = 0, 23 DO BEGIN 
             WIDGET_CONTROL, exv.labelw(0,pw), SET_VALUE=pw_types(exv.table,pw,exv.tele) 
             WIDGET_CONTROL, exv.textw(0,pw), SET_VALUE=STRTRIM(STRING(ex(exv.tele,exv.table,0,pw)),2)
           ENDFOR
        3: BEGIN
             WIDGET_CONTROL, exv.textw(0,0), SET_VALUE=STRTRIM(STRING(ex(exv.tele,exv.table,0,0)),2) 
           END
      ENDCASE

  RETURN
END


;________________________________________________________________________________________________________
;

PRO TABLE_EXPOSURE_EVENT, event
   COMMON OS_TABLE_EXPOSURE_SHARE, exv
   COMMON OS_ALL_SHARE
   COMMON OS_INIT_SHARE

CASE (event.id) OF

   exv.dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, exv.base
   END

   exv.helpb : BEGIN	;** display help
      help_str = STRARR(10)

      ;help_str(0) = 'These tables define exposure times in units of milliseconds.'
      ;help_str(1) = 'Exposure times are converted EUVI, COR1, and COR2 units of 1.024'
      ;help_str(2) = 'and HI1 and HI2 units of 2.0 milliseconds read from exposure tables.'
      ;help_str(3) = 'If CCD pixel summing has been selected from the camera parameters table'
      ;help_str(4) = 'then the default exposure times are adjusted automatically in the SEB.'
      ;help_str(5) = ''

      help_str(0) = 'These tables define exposure times in units of physical milliseconds.'
      help_str(1) = 'The EUVI, COR1, and COR2 exposure times were multiplied by 1.024, on '
      help_str(2) = 'input, to achive physical units; The HI1 and HI2 exposure times were '
      help_str(3) = 'multiplied by 2.0.                                                   '
;      help_str(4) = 'Exposure times are adjusted automatically for CCD pixel summing and  '
;      help_str(5) = 'low gain, in the SEB, as specified in camera parameters tables for   '
;      help_str(6) = 'non-calibration images. For Dark and LED images where exposure times '
;      help_str(7) = 'and pulses are user provided, a message is displayed reminding user  '
;      help_str(8) = 'to adjust inputs accordingly.                                        '
      help_str(9) = ''
      POPUP_HELP, help_str, TITLE="EXPOSURE TABLE HELP"
   END

   exv.reset : BEGIN	;** load defaults
      ;** account for any summing for this tele,table configuration

      DISPLAY_EXP,exv,fw_types,pw_types,exd
   END

   exv.telepd : BEGIN
      exv.tele = event.index

      DISPLAY_EXP,exv,fw_types,pw_types,exd
   END

   exv.tablepd : BEGIN
      exv.table = event.index

      DISPLAY_EXP, exv,fw_types,pw_types,exd
   END

   ELSE : BEGIN 	;** must be changing an exposure value
      WIDGET_CONTROL, event.id, GET_UVALUE=uval
      fw = uval(0)
      pw = uval(1)
      exv.fw = fw
      exv.pw = pw
      WIDGET_CONTROL, exv.textw(fw,pw), GET_VALUE=val & val = val(0)
      temp = P_CHECK(val,'exp',new) & WIDGET_CONTROL, exv.textw(fw,pw), SET_VALUE=new
      ex(exv.tele,exv.table,fw,pw) = LONG(new) ;aee
      ;pw = (pw + 1) MOD 20                    ; AEE - Nov 25, 02 
      pw = (pw + 1) MOD 24                    ; AEE - Nov 25, 02 
      IF (pw EQ 0) THEN fw = (fw + 1) MOD 4   ; AEE - Nov 25, 02 
      WIDGET_CONTROL, exv.textw(fw,pw), /INPUT_FOCUS
      WIDGET_CONTROL, exv.textw(fw,pw), GET_VALUE=val & val = val(0)
      WIDGET_CONTROL, exv.textw(fw,pw), SET_TEXT_SELECT=[STRLEN(STRTRIM(STRING(val),2))]
   END

ENDCASE

END

;________________________________________________________________________________________________________
;

PRO TABLE_EXPOSURE, caller
   COMMON OS_TABLE_EXPOSURE_SHARE
   COMMON OS_ALL_SHARE
   COMMON OS_INIT_SHARE
   COMMON TABLES_IN_USE, tables_used

   IF XRegistered("TABLE_EXPOSURE") THEN RETURN

   IF ((SIZE(exv))(1) EQ 0) THEN BEGIN		 ;** not defined yet use default
      tele = 0		
      table = 0
   ENDIF ELSE BEGIN
      tele = exv.tele		
      table = exv.table
   ENDELSE

   note= ' (Press the Return Key After Each Change to Make It Effective)'
   ;base = WIDGET_BASE(/COLUMN, TITLE='SECCHI EXPOSURE TABLE TOOL'+note, /FRAME, GROUP_LEADER=caller.id)
   ;base = WIDGET_BASE(/COLUMN, TITLE='SECCHI EXPOSURE TABLE TOOL', /FRAME, GROUP_LEADER=caller.id)
   base = WIDGET_BASE(/COLUMN, TITLE='SECCHI EXPOSURE TABLE TOOL ('+tables_used(1)+')', /FRAME, GROUP_LEADER=caller.id)

   row = WIDGET_BASE(base, /ROW)
      telepd =  CW_BSELECTOR2(row, tele_types, SET_VALUE=tele)
      tablepd = CW_BSELECTOR2(row, table_types, SET_VALUE=table)
      reset = WIDGET_BUTTON(row, VALUE='    Load Defaults    ')
      helpb = WIDGET_BUTTON(row, VALUE='        Help         ')

   row = WIDGET_BASE(base, /ROW)
   col = LONARR(5)
   col(0) = WIDGET_BASE(row, /COLUMN, /FRAME, SPACE=0)
   col(1) = WIDGET_BASE(row, /COLUMN, /FRAME, SPACE=0)
   col(2) = WIDGET_BASE(row, /COLUMN, /FRAME, SPACE=0)
   col(3) = WIDGET_BASE(row, /COLUMN, /FRAME, SPACE=0)
   col(4) = WIDGET_BASE(row, /COLUMN, /FRAME, SPACE=0)
   ;labelw = LONARR(4,20)  ; AEE - Nov 25, 02
   labelw = LONARR(4,24)  ; AEE - Nov 25, 02
   ;textw = LONARR(4,20)   ; AEE - Nov 25, 02
   textw = LONARR(4,24)   ; AEE - Nov 25, 02

   ; AEE 4/2/04 - define and init an empty table for now (and later fill in exp info for the input data):

   FOR fw = 0, 3 DO BEGIN
      ;FOR pw = 0, 19 DO BEGIN 
      FOR pw = 0, 23 DO BEGIN 
         row = WIDGET_BASE(col(fw), /ROW, YPAD=0)
         label = WIDGET_LABEL(row, VALUE= '                            ')
         text = WIDGET_TEXT(row, YSIZE=1, XSIZE=5, VALUE='', UVALUE=[fw,pw])
         labelw(fw,pw) = label
         textw(fw,pw) = text
      ENDFOR
   ENDFOR

   dismiss = WIDGET_BUTTON(base, VALUE=" Dismiss ")

   WIDGET_CONTROL, /REAL, base

   exv = CREATE_STRUCT( 'base', base,			$
                        'dismiss', dismiss,		$
                        'labelw', labelw,		$
                        'textw', textw,			$
                        'tablepd', tablepd,		$
                        'telepd', telepd,		$
                        'reset', reset,			$
                        'helpb', helpb,			$
 			'fw', fw,			$
 			'pw', pw,			$
 			'tele', tele,			$
			'table', table )

   ; AEE 4/2/04 - now fill in exp info for current telescope and table
  
   WIDGET_CONTROL, exv.reset, SENSITIVE= 0

   DISPLAY_EXP,exv,fw_types,pw_types,exd

   XMANAGER, 'TABLE_EXPOSURE', base
END

;________________________________________________________________________________________________________
;

PRO TABLE_OCCULTER_EVENT, event
   COMMON OS_TABLE_OCCULTER_SHARE, base, dismiss
CASE (event.id) OF
   dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, base
   END
   ELSE : BEGIN
   END
ENDCASE
END

PRO TABLE_OCCULTER, caller
   COMMON OS_TABLE_OCCULTER_SHARE
   COMMON TABLES_IN_USE, tables_used
   IF XRegistered("TABLE_OCCULTER") THEN RETURN
   ;base = WIDGET_BASE(/COLUMN, TITLE='SECCHI OCCULTER TABLE TOOL', /FRAME, $
   base = WIDGET_BASE(/COLUMN, TITLE='SECCHI OCCULTER TABLE TOOL ('+tables_used(2)+')', /FRAME, $
             GROUP_LEADER=caller.id)
      temp = WIDGET_LABEL(base, VALUE='          Not Implemented Yet          ')
      dismiss = WIDGET_BUTTON(base, VALUE=" Dismiss ")
   WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
   XMANAGER, 'TABLE_OCCULTER', base
END

;________________________________________________________________________________________________________
;

PRO TABLE_MASK_EVENT, event
   COMMON OS_TABLE_MASK_SHARE, base, dismiss
CASE (event.id) OF
   dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, base
   END
   ELSE : BEGIN
   END
ENDCASE
END

PRO TABLE_MASK, caller
   COMMON OS_TABLE_MASK_SHARE
   IF XRegistered("TABLE_MASK") THEN RETURN
   base = WIDGET_BASE(/COLUMN, TITLE='SECCHI MASK TABLE TOOL', /FRAME, $
             GROUP_LEADER=caller.id)
      temp = WIDGET_LABEL(base, VALUE='          Not Implemented Yet          ')
      dismiss = WIDGET_BUTTON(base, VALUE=" Dismiss ")
   WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
   XMANAGER, 'TABLE_MASK', base
END

;________________________________________________________________________________________________________
;

PRO TABLE_BADCOL_EVENT, event
   COMMON OS_TABLE_BADCOL_SHARE, base, dismiss
CASE (event.id) OF
   dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, base
   END
   ELSE : BEGIN
   END
ENDCASE
END

PRO TABLE_BADCOL, caller
   COMMON OS_TABLE_BADCOL_SHARE
   IF XRegistered("TABLE_BADCOL") THEN RETURN
   base = WIDGET_BASE(/COLUMN, TITLE='SECCHI BADCOL TABLE TOOL', /FRAME, $
             GROUP_LEADER=caller.id)
      temp = WIDGET_LABEL(base, VALUE='          Not Implemented Yet          ')
      dismiss = WIDGET_BUTTON(base, VALUE=" Dismiss ")
   WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
   XMANAGER, 'TABLE_BADCOL', base
END

;________________________________________________________________________________________________________
;

PRO TABLE_TRANSIENT_EVENT, event
   COMMON OS_TABLE_TRANSIENT_SHARE, base, dismiss
CASE (event.id) OF
   dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, base
   END
   ELSE : BEGIN
   END
ENDCASE
END

PRO TABLE_TRANSIENT, caller
   COMMON OS_TABLE_TRANSIENT_SHARE
   IF XRegistered("TABLE_TRANSIENT") THEN RETURN
   base = WIDGET_BASE(/COLUMN, TITLE='SECCHI TRANSIENT TABLE TOOL', /FRAME, $
             GROUP_LEADER=caller.id)
      temp = WIDGET_LABEL(base, VALUE='          Not Implemented Yet          ')
      dismiss = WIDGET_BUTTON(base, VALUE=" Dismiss ")
   WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=X0(base), TLB_SET_YOFFSET=Y0(base)
   XMANAGER, 'TABLE_TRANSIENT', base
END

;________________________________________________________________________________________________________
;


PRO OS_TABLES
COMMON SCHED_SHARE, schedv
COMMON TABLES_IN_USE, tables_used

tbls=getenv('TABLES')

  SPAWN,"grep -h 'Id:' "+tbls+"/imagetbl.img",sca_tables
  scb_tables= sca_tables

  SPAWN,"grep -h 'Id:' "+tbls+"/expostba.img",tbl
  sca_tables= [sca_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/occulta.img",tbl
  sca_tables= [sca_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/rotbtb1a.img",tbl
  sca_tables= [sca_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/setuptba.img",tbl
  sca_tables= [sca_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/wavetb1a.img",tbl
  sca_tables= [sca_tables,tbl]

  SPAWN,"grep -h 'Id:' "+tbls+"/expostbb.img",tbl
  scb_tables= [scb_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/occultb.img",tbl
  scb_tables= [scb_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/rotbtb1b.img",tbl
  scb_tables= [scb_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/setuptbb.img",tbl
  scb_tables= [scb_tables,tbl]
  SPAWN,"grep -h 'Id:' "+tbls+"/wavetb1b.img",tbl
  scb_tables= [scb_tables,tbl]

  scid= 0 ; 0=SC-AB
  IF (DATATYPE(schedv) EQ 'STC') THEN $
    scid= schedv.sc ; 1=SC-A, 2=SC-B
  IF (scid EQ 0) THEN scid= 1 ; Use SC-A for A+B

  ; get tables_used info and keep around (also done in os_init.pro):  
  SPAWN,"grep -h 'Id:' "+tbls+"/imagetbl.img",tables_used
  CASE (scid) OF
    1: BEGIN
      ;SPAWN,"grep -h 'Id:' "+tbls+"/*a.img",sca_tables
      ;tables_used= [tables_used,sca_tables] 
      tables_used= sca_tables 
    END
    2: BEGIN
      ;SPAWN,"grep -h 'Id:' "+tbls+"/*b.img",scb_tables
      ;tables_used= [tables_used,scb_tables]
      tables_used= scb_tables 
    END
    ELSE : BEGIN
    END
  ENDCASE

  tbAout= ';'+strmid(sca_tables,2,100) ; remove first $ (second char) from each string.
  tbBout= ';'+strmid(scb_tables,2,100) ; remove first $ (second char) from each string. 

  FOR i=0, N_ELEMENTS(tables_used)-1 DO BEGIN
    toks= STR_SEP(tables_used(i),' ')
    toks= (toks(WHERE(toks NE ' ')))(1:2) ; Keep tablename_version  
    tables_used(i)= toks(0)+toks(1)
  ENDFOR
  ; so now:
  ; tables_used(0)= IP table
  ; tables_used(1)= expcal table
  ; tables_used(2)= occult table
  ; tables_used(3)= rotbtb1 table
  ; tables_used(4)= setuptb table
  ; tables_used(5)= wavetb1 table

END
