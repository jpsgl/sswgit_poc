;+
;$Id: fill_in_ccd.pro,v 1.2 2004/09/01 15:40:42 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : FILL_IN_CCD
;               
; Purpose     : This function fills in a ccd structure for an OS from information from CCD section of
;               .IPT file.
;               
; Use         : ccd= FILL_IN_CCD(ccdvars, ro, oth) 
;    
; Inputs      : ccdvars    Structure containing parametes of camera table.
;               ro         Camera Readout info from .IPT file.
;               oth        Other Camera Readout info from .IPT file.
;
; Opt. Inputs : None
;               
; Outputs     :  ccd       ccd structure for the current OS.
;
; Opt. Outputs: None
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 07/14/04 - x and y now start with 1 instead of 0.
;                                       Also added cl_time and ro_time and removed htimer.
;
; $Log: fill_in_ccd.pro,v $
; Revision 1.2  2004/09/01 15:40:42  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:00  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION FILL_IN_CCD, ccd, ro, oth

   ro_arr = FIX(STR2ARR(STRMID(ro,1,STRLEN(ro)-2),DELIM=','))
   ccd.x1 = 1 > ro_arr(1)
   ccd.y1 = 1 > ro_arr(0)
   ccd.x2 = 1 > ro_arr(3) > ccd.x1
   ccd.y2 = 1 > ro_arr(2) > ccd.y1
   ccd.xsum = ro_arr(5) & IF (ccd.xsum LT 1) THEN ccd.xsum = 1   ; AEE 4/5/04
   ccd.ysum = ro_arr(4) & IF (ccd.ysum LT 1) THEN ccd.ysum = 1   ; AEE 4/5/04

   oth_arr = FIX(STR2ARR(STRMID(oth,1,STRLEN(oth)-2),DELIM=','))

;   AEE 4/5/04 - changed to these:

   ccd.pform    = oth_arr(0)  ; valid values:  1='A' 2='B' 
   ccd.clr_id   = oth_arr(1)  
   ccd.rdout_id = oth_arr(2)
   ccd.cl_time  = oth_arr(3)
   ccd.ro_time  = oth_arr(4)

   RETURN, ccd

END
