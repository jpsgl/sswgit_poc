;+
;$Id: base10to36.pro,v 1.1.1.2 2004/07/01 21:18:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : BASE10TO36
;               
; Purpose     : Converts a positive base 10 number to its base 36 equivalent.
;               
; Use         : b36= BASE10TO36(b10) 
;    
; Inputs      : b10    A base 10 number. 
;
; Opt. Inputs : None 
;               
; Outputs     : b36    Base 36 equivalent of b10. 
;
; Opt. Outputs: None
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: base10to36.pro,v $
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

FUNCTION BASE10TO36, b10

 ;INPUT: 
 ;        b10: A base 10 number (int or long)
 ;
 ;OUTPUT:
 ;        s36: A string representing the base36 equivalent of base10 input.
 ;             On error, an empty string, '', is returned.
 ;

  IF(b10 LT 0 OR (SIZE(b10))(1) LT 2 OR (SIZE(b10))(1) GT 3) THEN BEGIN
    PRINT,''
    IF ((SIZE(b10))(1) NE 0) THEN PRINT,'INPUT  (base10) = ',b10
    PRINT,'WARNING: Invalid Input. Input must be a positive base10 INT or LONG.'
    PRINT,''
    RETURN,''
  END

  L10= LONG(b10)

  ;PRINT,''
  ;PRINT,'INPUT  (base10) = ', L10 

 ;b10:  1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18
 ;=============================================================================
  b36=['1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I',  $
       'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','10']
 ;=============================================================================
 ;b10:  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36

  bind= INDGEN(36)+1
  b36= ['0',b36]
  bind=[0,bind]

  ; See how many digits of base36 is required for converting the base10 input:
  len= 0 
  tmp= L10 / 36
  WHILE (tmp GT 0) DO BEGIN
   len= len+1
   tmp= tmp / 36
  END

  ;PRINT,'len=',len + 1

  s36= '0000'
  FOR i= len, 0, -1 DO BEGIN
    n= L10 / (36.0^i)
    L10= ROUND((n MOD 1) * (36.0^i))
    w= WHERE(bind EQ BYTE(n))
    s36= s36+b36(w(0))
  ENDFOR 

  s36= STRMID(s36,STRLEN(s36)-4,4)

  ;PRINT,"OUTPUT (base36) = '"+ s36 +"'"
  ;PRINT,''

  RETURN, s36
END
