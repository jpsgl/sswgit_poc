;+
;$Id: fake_op.pro,v 1.1.1.2 2004/07/01 21:18:59 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : FAKE_OP
;               
; Purpose     : Define some sample Observation Programs for SECCHI Schedule.
;               
; Use         : FAKE_OP, startdis
;    
; Inputs      : startdis	input start of display time in TAI.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool. 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: fake_op.pro,v $
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;
PRO FAKE_OP, startdis

COMMON OP_DEFINED, op_instance, defined_op_arr
COMMON OP_SCHEDULED, op_struct, op_arr
COMMON OBS_PROG_DEF, op_id_struct, op_id_arr

   op_id_struct = {op_id_struct, op_id:0L, op_id_name:'', op_id_desc:''}

   op_instance = {op_instance, num:0L, id:0L, camp_num:0L, instrument:'', sci_obj:'', object:'', $
                  comment:'', date_modified:0.0D}
   op_struct = {op_struct, num:0L, op_start:0.0D, op_stop:0.0D}

   ;RESTORE, 'op_id_arr.sav'
   filename= GETENV('PT')+'/IN/OTHER/'+'op_id_arr.sav'
   RESTORE, filename

   ;RESTORE, 'op_arr.sav'
   filename= GETENV('PT')+'/IN/OTHER/'+'op_arr.sav'
   RESTORE, filename

goto, cont2
   ;** change day to startdis
   datastart = TAI2UTC(startdis)             ; form .DAY and .TIME
   thisstart = TAI2UTC(op_struct.op_start)      ; form .DAY and .TIME
   thisstop = TAI2UTC(op_struct.op_stop)      ; form .DAY and .TIME

   daydiff = datastart.mjd - thisstart.mjd
   thisstart.mjd = thisstart.mjd + daydiff
   thisstop.mjd = thisstop.mjd + daydiff
;*** AEE - changed these two statements *****
   op_struct.op_start = UTC2TAI(thisstart)                ; start of STARTDIS in TAI
   op_struct.op_stop = UTC2TAI(thisstop)

   GOTO, CONT2

CONT:

   datastart = TAI2UTC(startdis)                ; form .DAY and .TIME
   datastart.time = 0                           ; only want day part
   daystart = UTC2TAI(datastart)                ; start of STARTDIS in TAI

  start_arr = [0,3,6, 7,10,15,20]       ; study start hrs from start of STARTDIS day
  stop_arr  = [3,6,7,10,15,20,25]      	; study stop hrs from start of STARTDIS day
  op_nums   = [100,101,102, 103, 104,105, 106]	;** this is unique
  op_ids    = [0,1,2, 0, 1,3,  4]	;** this is index into op_id_arr 
  camp_nums = [0,9,9, 0, 9,15, 0]
  instrument = 'SECCHI'
  sci_obj = 'The scientific objective would go here.'
  comment = 'Any comments would go here.'

op_arr = REPLICATE(op_struct, n_elements(start_arr))
defined_op_arr = REPLICATE(op_instance, n_elements(start_arr))

  defined_op_arr.num=op_nums
  defined_op_arr.id=op_ids 
  defined_op_arr.camp_num=camp_nums 
  defined_op_arr.instrument=instrument
  defined_op_arr.sci_obj=sci_obj
  defined_op_arr.comment=comment
  get_utc, stime                    ; get system time
  defined_op_arr.date_modified=utc2tai(stime)         ; seconds since 1 jan 1958 (TAI)

  op_arr.num=op_nums 
  op_arr.op_start=(start_arr*3600D) +daystart
  op_arr.op_stop=(stop_arr*3600D)+daystart

CONT2:

END
