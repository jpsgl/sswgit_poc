;+
;$Id: schedule_read_ipt_fst.pro,v 1.8 2005/10/17 19:02:10 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : SCHEDULE_READ_IPT_FST
;
; Purpose     : SCHEDULE_READ_IPT_FST, ipt_name, startdis, /SILENT, /PROMPT_SHIFT
;
; Use         : This routine reads in a InstrumentPlanningTool (.IPT) file and adds its
;               content to the schedule. The .IPT file is provided as a prameter to schedule.
;
; Inputs      : ipt_name      String containing the .IPT filename.
;               startdist     Start time of schedule (used to shift .IPT scheule times)
;                             and can be changed on output if .IPT file is shifted.
;
; Opt. Inputs : None
;
; Outputs     : startdist     Start time of schedule is changed on output if .IPT file is 
;                              shifted. 
;
; Opt. Outputs: None
;
; Keywords    : SILENT        If set, prints out diagnostic messages.
;               PROMPT_SHIFT  If set, prompt user if .IPT schedule should be shifted.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/09/04 - Added cadence to duplicate os check list.
;              Ed Esfandiari 06/15/04 - Moved defining os_struct to define_os_struct function.
;              Ed Esfandiari 06/16/04 - Added setup time.
;              Ed Esfandiari 07/02/04 - recalculate sizes in case new ip table use other apids.
;              Ed Esfandiari 07/14/04 - Use one of 4 ROI tables (used to have only 1 ROI table).
;              Ed Esfandiari 09/22/04 - Added spacecraft (sc) assignment.
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 10/04/04 - Set set_id=0.
;              Ed Esfandiari 10/15/04 - Added SC A/B bsf_id field.
;              Ed Esfandiari 02/04/05 - Added call to ADD_IPT_INIT_TL to read Init Timelines.
;              Ed Esfandiari 05/03/05 - Added call to ADD_IPT_RUN_SCRIPT to read Run Script sections.
;              Ed Esfandiari 05/10/05  - Allow multiple Init timeline commands.
;              Ed Esfandiari 05/11/05  - Allow multiple Run Script commands.
;              Ed Esfandiari 08/19/05  - Fixed HI seq update checks.
;
; $Log: schedule_read_ipt_fst.pro,v $
; Revision 1.8  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.7  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.6  2005/03/10 16:52:33  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.5  2005/01/24 17:56:36  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.3  2004/09/01 15:40:47  esfand
; commit new version for Nathan.
;
; Revision 1.2  2004/07/02 15:47:10  esfand
; calculate sizes in case new ip table contain new apids
;
; Revision 1.1.1.2  2004/07/01 21:19:10  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;

PRO SCHEDULE_READ_IPT_FST, ipt_file_name, SILENT=silent, STARTDIS=startdis, PROMPT_SHIFT=prompt_shift

COMMON OS_SCHEDULED, os_struct, os_arr, tm_arr, load_camtable, load_wlexptable, do_candc, move_fp
COMMON OS_DEFINED, defined_os_arr, os_instance
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON CMD_SEQ_SCHEDULED   ; AEE - 5/23/03
COMMON SCIP_DOORS_SHARE    ; AEE - 6/10/03
COMMON GT_DUMPS_SHARE      ; AEE - 7/25/03
COMMON APIDS, multi_apid   ; AEE - 7/25/03
COMMON INIT_TL, ic
COMMON RUN_SCRIPT, rsc


  ;USEDEFAULTS = 0 ; => Use table parameters in .IPT (maybe different if tables have been changed).

   USEDEFAULTS = 1 ;=> use table numbers in .IPT to get parameters from current tables read by os_init
                   ;   and also recalculate image size and durations.

   new_os_num = -1

   os_struct = DEFINE_OS_STRUCT()

   IF (DATATYPE(os_instance) EQ 'UND') THEN OS_INIT
   IF (DATATYPE(os_arr) EQ 'UND') THEN os_arr = -1

   ipt_file_name = STRTRIM(ipt_file_name,2)

   ; AEE - 5/21/03 See if ipt file has lp = 7 (block seq) section(s) and if so expand the secion(s):

   expanded_ipt_name= EXPAND_IPT(ipt_file_name)  ; AEE 5/21/03

   ;iptdir= GETENV('PT')+'/IO/IPT/'
   iptdir= ''

   IF (expanded_ipt_name NE '') THEN $
     OPENR, ipt_file, iptdir+expanded_ipt_name, /GET_LUN, /DELETE $  ; remove the expanded ipt file when done
   ELSE $
     OPENR, ipt_file, iptdir+ipt_file_name, /GET_LUN


;   WIDGET_CONTROL,mdiag,SET_VALUE='READING IPT FILE: '+ipt_file_name
   PRINT, '**************************************'
   PRINT, 'READING IPT FILE: ', ipt_file_name
   PRINT, '**************************************'

   str = ''

   all_commands = ''

   ;** find first occurance of PT_OS_NUM
   WHILE ( NOT(EOF(ipt_file)) AND (STRMID(str,0,9) NE "PT_OS_NUM") ) DO BEGIN
      READF, ipt_file, str
      str = STRTRIM(str, 2)
   ENDWHILE

   ;*************************************************
   ;** loop over each OS
   ;** 

next_sec:

   WHILE (NOT(EOF(ipt_file))) DO BEGIN
      curr_str_arr = str

      str = ''
      ;** find everything up to the next occurance of PT_OS_NUM
      WHILE ( NOT(EOF(ipt_file)) AND (STRMID(str,0,9) NE "PT_OS_NUM") ) DO BEGIN
         READF, ipt_file, str
         str = STRTRIM(str, 2)
         ;** throw out comments and blank lines now
         IF ( (STRMID(str,0,1) NE ';') AND (str NE '') ) THEN curr_str_arr = [curr_str_arr, str]
      ENDWHILE

      ;** if str is "PT_OS_NUM" it is for the next OS loop
      IF NOT(EOF(ipt_file)) THEN curr_str_arr = curr_str_arr(0:N_ELEMENTS(curr_str_arr)-2)

      ;** curr_str_arr contains all parameters for this OS
      ;** convert this to an array of keyword/value structures
      key_val_arr = STR_ARR2KEY_VAL(curr_str_arr)

      ; AEE 06/10/03 - check IPT file for door close (lp=8) and open (lp=9) commands:

      IF (key_val_arr(0).val EQ '0001') THEN BEGIN ; door section
        ADD_IPT_DOORS,key_val_arr
        GOTO, next_sec 
      ENDIF

      ; AEE 07/25/03 - check IPT file for GT-dump (lp=10) commands:

      IF (key_val_arr(0).val EQ '0002') THEN BEGIN ; GT-dump section
        ADD_IPT_GT,key_val_arr
        GOTO, next_sec
      ENDIF

      ; Check IPT file for Initialize Timeline (lp=11) commands:
      IF (key_val_arr(0).val EQ '0003') THEN BEGIN ; Init Timeline section
        ADD_IPT_INIT_TL,key_val_arr
        GOTO, next_sec
      ENDIF

      ; Check IPT file for Run Script (lp=12) command:
      IF (key_val_arr(0).val EQ '0004') THEN BEGIN ; Run Script section
        ADD_IPT_RUN_SCRIPT,key_val_arr
        GOTO, next_sec
      ENDIF
     
      ;** keep track of which parameters we have used
      used = BYTARR(N_ELEMENTS(curr_str_arr))

      ;** now initialize an OS structure and fill it in
      os = FILL_IN_OS(key_val_arr, used)
      ;** now initialize an LP structure for this particular LP and fill it in
      lp = FILL_IN_LP(os.lp_num, key_val_arr, used)

      IF (USEDEFAULTS EQ 1) THEN BEGIN
        os.os_size= 0 ; To force calculating the statistics.
        lp.ip = ip(lp.ip_tab_num).steps
        lp.occ = occ_blocks(lp.tele,*) 
        ;lp.roi = roi_blocks(lp.tele,*)
        roi_table= WHICH_ROI_TABLE(ip,lp.ip_tab_num)
        IF (roi_table EQ -1) THEN roi_table= 0 ; use the first ROI table as defualt when none present.
        lp.roi = roi_blocks(lp.tele,*,roi_table)
        lp.ccd = ccd(lp.tele,lp.camt-1)
        IF (os.lp_num LT 2 OR os.lp_num GT 3) THEN lp.exptime(0)= exd(lp.tele,lp.expt-1,lp.fw,lp.pw(0))
        IF (os.lp_num EQ 0) THEN lp.exptime(1)= exd(lp.tele,lp.expt-1,lp.fw,lp.pw(1)) 
        IF (os.lp_num EQ 5 OR os.lp_num EQ 6) THEN BEGIN
          n = N_ELEMENTS(lp.pw)
          FOR i=0,n-1 DO lp.exptime(i)= exd(lp.tele,lp.expt-1,lp.fw,lp.pw(i))
        ENDIF
      ENDIF

      IF NOT(KEYWORD_SET(SILENT)) THEN BEGIN
         print & print
         ;print, key_val_arr(where(used EQ 1)), format='(a)'
         ;print & print
         print, 'Unused parameter/values:'
         print, key_val_arr(where(used NE 1)), format='(a)'
         print & print
      ENDIF

      ;** FILL IN AN OS_INSTANCE
 
   new_entry = os_instance                      ;** os_instance defined in OS_DEFINED COMMON
   new_entry.ex = exd
   new_entry.os_num = os.os_num

   IF (os.lp_num LT 0) THEN BEGIN ; AEE 9/25/03
     PRINT, '%%SCHEDULE_READ_IPT_FST  Error: Unknown LP', os.lp_num
   END
   new_entry.lp = os.lp_num 
   new_entry.cadence= os.os_cad
   new_entry.pre_proc_time= os.os_ppt

   CASE (new_entry.lp) OF

   0 : BEGIN                 ;** Sky Double Image LP - AEE 9/25/03
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0:1) = lp.pw(0:1)
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0))= lp.exptime(0)
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(1))= lp.exptime(1)
   END

   1 : BEGIN                 ;** Sky Normal Image LP
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0) = lp.pw
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 
   2 : BEGIN                 ;** Dark Image LP
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, 0, 0) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 
   3 : BEGIN                 ;** Cal Lamp LP
      new_entry.tele = lp.tele
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0) = lp.pw
      new_entry.num_images = 1
      new_entry.lamp = lp.lamp
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END

   4 : BEGIN                 ;** Cont. Image LP
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0) = lp.pw
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END

 
   5 : BEGIN                 ;** Seq PW FW LP
      new_entry.tele = lp.tele
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      n = N_ELEMENTS(lp.pw)
      new_entry.num_images = n       ; AEE Dec 10, 02.
      new_entry.pw(0:n-1) = lp.pw
      new_entry.cadence= os.os_cad  ; AEE 01/27/03
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      FOR i=0,n-1 DO $
        new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(i)) = lp.exptime(i)
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 

    6 : BEGIN  ; Hi Seq
      new_entry.tele = lp.tele
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      n = N_ELEMENTS(lp.pw)
      new_entry.num_images = n       ; AEE Dec 10, 02.
      new_entry.pw(0) = lp.pw(0)
      new_entry.cadence= os.os_cad  ; AEE 01/27/03
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      FOR i=0,n-1 DO $
        new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime(i)
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END

   7 : BEGIN                 ;** Block Seq 
     stop,'SCHEDULE_READ_IPT_FST: Should not gotten here; LP = 7 should have been expanded to other LPs.'
   END
 
   ELSE : BEGIN
     print,"****"
     print,'LP= ', new_entry.lp
     print,"In schedule_read_ipt_fst.pro, LP= ", new_entry.lp," not found."
     print,"****"
   END
 
ENDCASE


   IF (DATATYPE(defined_os_arr) NE 'STC') THEN BEGIN   ;** first os scheduled ; AEE 2/17/04
      IF (new_entry.os_num EQ 0) THEN new_entry.os_num = GET_OS_NUM()
      defined_os_arr_save = new_entry
      defined_os_arr = new_entry 
   ENDIF ELSE BEGIN		;** only add if new
      ind = WHERE(defined_os_arr.os_num EQ new_entry.os_num)
      IF (ind(0) EQ -1) THEN BEGIN			;** then it is new

         ;** save os definitions from this IPT so that we only allocate new os_num 
         ;** for truly new definitions
         IF (DATATYPE(defined_os_arr_save) NE 'STC') THEN BEGIN   ; AEE - 02/17/04
            IF (new_entry.os_num EQ 0) THEN new_entry.os_num = GET_OS_NUM()
            defined_os_arr_save = new_entry
            defined_os_arr = [defined_os_arr, new_entry]	;** append to end of defined array
         ENDIF ELSE BEGIN
          found = 0
          n = 0
          len = N_ELEMENTS(defined_os_arr_save)
          WHILE ( (found NE 1) AND (n LT len) ) DO BEGIN
            t_os =  defined_os_arr_save(n)
            IF ( (new_entry.lp EQ t_os.lp) AND $
                          (new_entry.tele EQ t_os.tele) AND $
                          ;(new_entry.table EQ t_os.table) AND $  ; AEE 1/19/04
                          (new_entry.camtable EQ t_os.camtable) AND $ ; AEE 1/19/04
                          (new_entry.exptable EQ t_os.exptable) AND $ ; AEE 1/19/04
                          (new_entry.fps EQ t_os.fps) AND $ ; AEE 1/19/04
                          (new_entry.cadence EQ t_os.cadence) AND $
                          (new_entry.iptable EQ t_os.iptable) AND $
                          (new_entry.fw EQ t_os.fw) AND $
                          MIN(new_entry.pw EQ t_os.pw) AND $
                          (new_entry.lamp EQ t_os.lamp) AND $
                          (new_entry.num_images EQ t_os.num_images) AND $
                          (new_entry.start EQ t_os.start) AND $
                          MIN(new_entry.ex EQ t_os.ex) AND $
                          MIN(new_entry.occ_blocks EQ t_os.occ_blocks) AND $
                          MIN(new_entry.roi_blocks EQ t_os.roi_blocks) ) THEN found = 1
            IF (found EQ 1) THEN BEGIN	;** compare all tags in structure
               st = new_entry.ccd(new_entry.tele,new_entry.camtable)
               st2 = t_os.ccd(new_entry.tele,new_entry.camtable)
               nt = N_TAGS(st)
               res = BYTARR(nt)
               FOR t=0, nt-1 DO $
                  res(t) = MIN(st.(t) EQ st2.(t))
               st = new_entry.ip(new_entry.iptable)
               st2 = t_os.ip(new_entry.iptable)
               nt = N_TAGS(st)
               res2 = BYTARR(nt)
               FOR t=0, nt-1 DO $
                  res2(t) = MIN(st.(t) EQ st2.(t))
               IF ((MIN(res) < MIN(res2)) EQ 1) THEN found = 1 ELSE found = 0
            ENDIF
            n = n + 1
          ENDWHILE
            IF (found EQ 0) THEN BEGIN			;** this is new
               IF (new_entry.os_num EQ 0) THEN new_entry.os_num = GET_OS_NUM()
               defined_os_arr_save = [defined_os_arr_save, new_entry]
               defined_os_arr = [defined_os_arr, new_entry]	;** append to end of defined array
            ENDIF ELSE BEGIN 						;** this was already defined
               new_entry.os_num = t_os.os_num
            ENDELSE
         ENDELSE

      ENDIF
   ENDELSE


   ; AEE - 7/25/03

   IF (USEDEFAULTS NE 1) THEN BEGIN
     aind= WHERE( key_val_arr.key EQ 'PT_OS_SIZES', acnt)
     IF (acnt GT 0) THEN BEGIN
       IF (DATATYPE(multi_apid) EQ 'UND') THEN BEGIN 
         multi_apid= {multi_apid,os_num:os.os_num, sizes: key_val_arr(aind).val}
       ENDIF ELSE BEGIN 
         mind= WHERE(multi_apid.os_num EQ os.os_num, scnt)
         IF (scnt EQ 0) THEN $
           multi_apid= [multi_apid,{multi_apid,os_num:os.os_num, sizes: key_val_arr(aind).val}]
       ENDELSE
     ENDIF ELSE BEGIN
       IF (os.os_size GT 0) THEN BEGIN
         PRINT,''
         PRINT,'WARNING: Each OS in .IPT file should have either a valid PT_OS_SIZES or PT_OS_SIZE = 0'
         PRINT,''
         STOP
       ENDIF
     ENDELSE
   ENDIF
 
         ;** 
         ;** IF stats are not in IPT file the calculate them
         IF ( (os.os_size EQ 0) ) THEN BEGIN
            GET_OS_DB_STATS, new_entry, os_size, os_duration, os_pre_proc_time, os_ro_time, $
                             os_proc_time, os_setup_time,  os_cadence 
            os.os_ppt= os_pre_proc_time
            os.os_rot= os_ro_time 
            os.os_size = os_size
            os.os_dur = os_duration
            os.os_proct= os_proc_time
            os.os_setup = os_setup_time
            os.os_cad= os_cadence
            ;ndef= N_ELEMENTS(defined_os_arr)-1
            ;last= WHERE(defined_os_arr.os_num EQ new_entry.os_num, lcnt)
            last= WHERE(defined_os_arr.os_num EQ new_entry.os_num AND $
                        defined_os_arr.tele EQ new_entry.tele, lcnt)
            IF (lcnt GT 0) THEN BEGIN
              ;IF (last(0) NE ndef) THEN BEGIN
                IF (defined_os_arr(last).cadence NE os_cadence) THEN $
                  defined_os_arr(last).cadence= os_cadence
                IF (defined_os_arr(last).pre_proc_time NE os_pre_proc_time) THEN $
                  defined_os_arr(last).pre_proc_time= os_pre_proc_time
              ;ENDIF
            ENDIF
         ENDIF

      ;** loop over each scheduled start time and add to os_arr
      FOR s_index = 0, N_ELEMENTS(os.lp_start)-1 DO BEGIN

         tai = UTC2TAI(os.lp_start(s_index))
         add_os = os_struct
         add_os.os_num = new_entry.os_num
         ;add_os.set_id = os.set_id ; AEE 2/11/04 - Added
         add_os.set_id = 0
         add_os.os_duration = os.os_dur
         add_os.os_size = os.os_size
         add_os.os_pre_proc_time=  os.os_ppt  ; AEE 01/27/03 
         add_os.os_ro_time= os.os_rot            ; AEE 01/27/03
         add_os.os_setup_time= os.os_setup
         add_os.os_tele= new_entry.tele      ; AEE 01/27/03
         add_os.os_lp= new_entry.lp          ; AEE 01/27/03
         add_os.os_images= new_entry.num_images  ; AEE 01/27/03
         add_os.os_cadence= os.os_cad  ; AEE 01/27/03
         add_os.os_proc_time=  os.os_proct
         add_os.os_start = tai
         add_os.os_stop = add_os.os_start + add_os.os_duration      ;** calc stop time
         ;add_os.sc= os.lp_sc(s_index)
         IF (STRPOS(ipt_file_name,'SC_A') GT 0) THEN add_os.sc= 'A'
         IF (STRPOS(ipt_file_name,'SC_AB') GT 0) THEN add_os.sc= os.lp_sc(s_index) 
         IF (STRPOS(ipt_file_name,'SC_B') GT 0) THEN add_os.sc= 'B'

         add_os.bsf= os.bsf_id

         IF (DATATYPE(os_arr) EQ 'INT') THEN os_arr = add_os ELSE $
            os_arr = [os_arr, add_os]                    ;** add to current schedule

         ;***************************
         ;** ADD CAMERA TABLE LOADS
         IF (os.lp_load_cam(s_index) EQ 1) THEN BEGIN
            cam_stc = {cam_stc, os_num:0L, os_start:0.0D}
            cam_stc.os_num = add_os.os_num
            cam_stc.os_start = add_os.os_start
            IF (DATATYPE(load_camtable) NE 'STC') THEN BEGIN  ;** first entry
               load_camtable = cam_stc
            ENDIF ELSE BEGIN
               ind = WHERE((load_camtable.os_num EQ add_os.os_num) AND (load_camtable.os_start EQ add_os.os_start))
               IF (ind(0) EQ -1) THEN BEGIN
                  load_camtable = [load_camtable,cam_stc]
               ENDIF
            ENDELSE
         ENDIF

         ;***************************
         ;** ADD WAVELENGTH/EXP TABLE LOADS
         IF (os.lp_load_wlexp(s_index) EQ 1) THEN BEGIN
            cam_stc = {cam_stc, os_num:0L, os_start:0.0D}
            cam_stc.os_num = add_os.os_num
            cam_stc.os_start = add_os.os_start
            IF (DATATYPE(load_wlexptable) NE 'STC') THEN BEGIN  ;** first entry
               load_wlexptable = cam_stc
            ENDIF ELSE BEGIN
               ind = WHERE((load_wlexptable.os_num EQ add_os.os_num) AND (load_wlexptable.os_start EQ add_os.os_start))
               IF (ind(0) EQ -1) THEN BEGIN
                  load_wlexptable = [load_wlexptable,cam_stc]
               ENDIF
            ENDELSE
         ENDIF

      ENDFOR	;** end looping on start times for this lp
      IF (new_os_num(0) EQ -1) THEN $
         new_os_num = new_entry.os_num $
      ELSE $
         new_os_num = [new_os_num,new_entry.os_num]

   ENDWHILE

   CLOSE, ipt_file
   FREE_LUN, ipt_file

   PRINT, '**************************************'
   PRINT, 'DONE READING IPT FILE '
   PRINT, '**************************************'

   ; AEE - 6/11/03 - to make sure we have same # of open and close doors for each SCIP telescope

   CHK_NUM_DOORS, cdoor_euvi, odoor_euvi
   CHK_NUM_DOORS, cdoor_cor1, odoor_cor1
   CHK_NUM_DOORS, cdoor_cor2, odoor_cor2

   ; Get the minumum none '2000/01/01' door close or open time:

   min_dtime= GET_MIN_DOOR(cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2)

   ; See if a GuideTelescope dump has a smaller value than the door:
   IF (DATATYPE(stime_gt) NE 'UND') THEN BEGIN
     IF (min_dtime EQ 0.0D) THEN $
       min_dtime = stime_gt(0)  $
     ELSE                       $
       min_dtime = min_dtime < stime_gt(0)
   ENDIF

   ; Also see if there are any I commands:

   amin_dt= 0.0D
   asc= WHERE(ic.sc EQ 'A',acnt)
   IF (acnt GT 0) THEN amin_dt= MIN(ic(asc).dt)
   bmin_dt= 0.0D
   bsc= WHERE(ic.sc EQ 'B',bcnt)
   IF (bcnt GT 0) THEN bmin_dt= MIN(ic(bsc).dt)
   IF (min_dtime EQ 0.0D) THEN BEGIN 
     IF (amin_dt GT 0.0) THEN min_dtime= amin_dt
     IF (bmin_dt GT 0.0) THEN min_dtime= bmin_dt
   ENDIF ELSE BEGIN
     IF (amin_dt GT 0.0) THEN min_dtime= amin_dt < min_dtime
     IF (bmin_dt GT 0.0) THEN min_dtime= bmin_dt < min_dtime 
   ENDELSE

   ; Also see if there are any A commands:
   amin_dt= 0.0D
   asc= WHERE(rsc.sc EQ 'A',acnt)
   IF (acnt GT 0) THEN amin_dt= MIN(rsc(asc).dt)
   bmin_dt= 0.0D
   bsc= WHERE(rsc.sc EQ 'B',bcnt)
   IF (bcnt GT 0) THEN bmin_dt= MIN(rsc(bsc).dt)
   IF (min_dtime EQ 0.0D) THEN BEGIN 
     IF (amin_dt GT 0.0) THEN min_dtime= amin_dt
     IF (bmin_dt GT 0.0) THEN min_dtime= bmin_dt
   ENDIF ELSE BEGIN
     IF (amin_dt GT 0.0) THEN min_dtime= amin_dt < min_dtime
     IF (bmin_dt GT 0.0) THEN min_dtime= bmin_dt < min_dtime 
   ENDELSE

   ; IF .IPT file has no OS section, then see if .IPT needs to be shifted using the minimum 
   ; door close/open or GT-dump times , if any:

   delta= 0.0D
   IF (min_dtime GT 0.0D) THEN BEGIN ; atleast one door or GT command was present in .IPT file.
     ; See if .IPT file contained any OS sections. If not, ignore the OS code.
     IF (DATATYPE(os_arr) EQ 'INT') THEN BEGIN ; no OS in .IPT file.
       IF ( KEYWORD_SET(PROMPT_SHIFT)) THEN BEGIN ; See if user wants to shift using door times.  
         shift_tai = SHIFT_IPT(min_dtime)
         IF (shift_tai GT 0) AND (shift_tai NE min_dtime) THEN BEGIN
           delta = shift_tai - min_dtime 
           SHIFT_DOORS, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2, delta

           ; Also shift GT-dumps, if any:  AEE - 7/25/03
           IF (DATATYPE(stime_gt) NE 'UND') THEN BEGIN
             stime_gt= stime_gt + delta
             etime_gt= etime_gt + delta
           ENDIF

           ; Also shift I commands, if any:

           IF (N_ELEMENTS(ic) GT 1) THEN ic(1:*).dt= ic(1:*).dt + delta

           ; Also shift A commands, if any:

           IF (N_ELEMENTS(rsc) GT 1) THEN rsc(1:*).dt= rsc(1:*).dt + delta

         ENDIF 
       ENDIF
       startdis = min_dtime + delta 
       RETURN
     ENDIF
   ENDIF

   BREAK_FILE, ipt_file_name, a, dir, name, ext
   ;**  Prompt user to shift entire IPT file:
   IF ( KEYWORD_SET(PROMPT_SHIFT)) THEN BEGIN
      first=1
      new_os_num= new_os_num(SORT(new_os_num)) ; AEE 1/9/04 - must be sorted for uniq to work.
      new_os_num = new_os_num(UNIQ(new_os_num)) ; AEE 5/23/03 incase same os_num is used in two 
                                                ; or more block seq lps

      IF (new_os_num(0) NE -1) THEN BEGIN
        FOR i=0, N_ELEMENTS(new_os_num)-1 DO BEGIN
           ind = WHERE(os_arr.os_num EQ new_os_num(i))
           IF (i EQ 0) THEN new_os_arr = os_arr(ind) ELSE new_os_arr = [new_os_arr, os_arr(ind)]
        ENDFOR
        ;ind = SORT(new_os_arr.os_start)
        ;first_tai = new_os_arr(ind(0)).os_start
      
        dates= new_os_arr.os_start
      ENDIF

      IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN BEGIN 
        IF (DATATYPE(dates) EQ 'UND') THEN $
          dates= UTC2TAI(STR2UTC(sched_cmdseq.start_time)) $
        ELSE $
          dates= [dates,UTC2TAI(STR2UTC(sched_cmdseq.start_time))]
      ENDIF

      ;dates= dates(SORT(dates))
      ;first_tai =dates(0)

      ; AEE - 6/11/03 - IF .IPT also has door commands, see if one of them is the first_tai:

      IF (DATATYPE(dates) NE 'UND') THEN BEGIN
        dates= dates(SORT(dates))
        first_tai =dates(0)
        IF (min_dtime GT 0.0D) THEN first_tai= MIN([first_tai,min_dtime])
      ENDIF ELSE first_tai= min_dtime

      shift_tai = SHIFT_IPT(first_tai)
      IF (shift_tai GT 0) AND (shift_tai NE first_tai) THEN BEGIN
         delta = shift_tai - first_tai
         ; AEE - 5/23/03 - shift the block seq (lp=7) dates, if any:

         ; Shift I and A commands:
         IF (N_ELEMENTS(ic) GT 1) THEN ic(1:*).dt= ic(1:*).dt + delta
         IF (N_ELEMENTS(rsc) GT 1) THEN rsc(1:*).dt= rsc(1:*).dt + delta

         IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN BEGIN
           sched_cmdseq.start_time=UTC2STR(TAI2UTC(UTC2TAI(STR2UTC(sched_cmdseq.start_time))+delta),/ECS)
           FOR i=0, N_ELEMENTS(sched_cmdseq)-1 DO BEGIN
             ; since there may be more than one time to be modified:
             toks= STR_SEP(sched_cmdseq(i).os_time,',')
             toks= UTC2STR(TAI2UTC(UTC2TAI(STR2UTC(toks))+delta),/ECS)
             sched_cmdseq(i).os_time= STRJOIN(toks,',')
           ENDFOR
         ENDIF

         ; AEE - 6/10/03 - shift the door close/open (lp=8/9) dates, if any:

         SHIFT_DOORS, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2, delta

         ; Also shift GT-dumps, if any:  AEE - 7/25/03
         IF (DATATYPE(stime_gt) NE 'UND') THEN BEGIN
           stime_gt= stime_gt + delta
           etime_gt= etime_gt + delta
         ENDIF

         FOR i=0, N_ELEMENTS(new_os_num)-1 DO BEGIN	;** for each new os added
            selected_os = new_os_num(i)
            inds = WHERE(os_arr.os_num EQ selected_os)
            os_arr(inds).os_start = os_arr(inds).os_start+delta
            os_arr(inds).os_stop = os_arr(inds).os_start + os_arr(inds).os_duration
            ;** apply delta to all camera table loads
            IF (DATATYPE(load_camtable) EQ 'STC') THEN BEGIN
               inds = WHERE(load_camtable.os_num EQ selected_os)
               IF (inds(0) NE -1) THEN $
                  load_camtable(inds).os_start = load_camtable(inds).os_start + delta
            ENDIF
            IF (DATATYPE(load_wlexptable) EQ 'STC') THEN BEGIN
               inds = WHERE(load_wlexptable.os_num EQ selected_os)
               IF (inds(0) NE -1) THEN $
                  load_wlexptable(inds).os_start = load_wlexptable(inds).os_start + delta
            ENDIF
            IF (DATATYPE(do_candc) EQ 'STC') THEN BEGIN
               inds = WHERE(do_candc.os_num EQ selected_os)
               IF (inds(0) NE -1) THEN $
                  do_candc(inds).os_start = do_candc(inds).os_start + delta
            ENDIF
            IF (DATATYPE(move_fp) EQ 'STC') THEN BEGIN
               inds = WHERE(move_fp.os_num EQ selected_os)
               IF (inds(0) NE -1) THEN $
                  move_fp(inds).os_start = move_fp(inds).os_start + delta
            ENDIF
         ENDFOR
      ENDIF
   ENDIF

   ;ind = SORT(os_arr.os_start)
   ;startdis = os_arr(ind(0)).os_start

   ; AEE - 06/11/03 - incase min_dtime (plus delta, if any) is the min. time:
   ;IF (min_dtime GT 0.0D) THEN startdis = MIN([startdis, min_dtime+delta])

   startdis = first_tai+delta

   ; AEE - 02/05/03 - for LP=6 (HI-seq) if both HI1 and HI2 are used in the same sequence, the HI1 and
   ;                  HI2 images are identified by os_num and -os_num in the IPT file. Once the IPT is
   ;                  read and finished (at this point), change the minus os_num(s) to os_num using
   ;                  ABS, if any exists:

   os_arr.os_num= ABS(os_arr.os_num)
   defined_os_arr.os_num= ABS(defined_os_arr.os_num)

   RETURN
   
END
