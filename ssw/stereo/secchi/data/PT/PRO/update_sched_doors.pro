;+
;$Id: update_sched_doors.pro,v 1.7 2009/09/11 20:28:25 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : UPDATE_SCHED_DOORS
;               
; Purpose     : This function Schedules and updates SCIP instruments 
;               door open/close commands. It adds or removes door 
;               commands using the door variables in common blocks.  
;               
; Use         : stat = UPDATE_SCHED_DOORS()
;    
; Inputs      : None 
;
; Opt. Inputs : None
;               
; Outputs     : None 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;               Ed Esfandiari 10/08/04 - Added S/C A and B capabilities.
;               Ed Esfandiari 01/21/05 - changed "Add to Schedule" to "Add Checked Entries...".
;               Ed Esfandiari 08/22/06 - Added RETAIN=2 to draw widget.
;
; $Log: update_sched_doors.pro,v $
; Revision 1.7  2009/09/11 20:28:25  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:36  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:37  esfand
; first checkin
;
;
;-

;
;

FUNCTION DO_REFRESH
COMMON LP_SCIP_DOORS_SHARE, lpdoor
COMMON SCIP_DOORS_SHARE

     removed = 0 ; false

     IF (lpdoor.euvi_cnt GT 0) THEN BEGIN
       keep= WHERE(lpdoor.euvi_select EQ 1, cnt)
       IF (cnt LT lpdoor.euvi_cnt) THEN BEGIN
         lpdoor.euvi_cnt = cnt
         removed= 1 ; true
         IF (cnt GT 0) THEN BEGIN
           cdoor_euvi= cdoor_euvi(keep) 
           odoor_euvi= odoor_euvi(keep)
           sc_euvi = sc_euvi(keep)
         ENDIF ELSE BEGIN
           cdoor_euvi= 0
           odoor_euvi= 0
           sc_euvi= '' 
         ENDELSE
       END
     END

     IF (lpdoor.cor1_cnt GT 0) THEN BEGIN
       keep= WHERE(lpdoor.cor1_select EQ 1, cnt)
       IF (cnt LT lpdoor.cor1_cnt) THEN BEGIN
         lpdoor.cor1_cnt = cnt
         removed= 1 ; true
         IF (cnt GT 0) THEN BEGIN
           cdoor_cor1= cdoor_cor1(keep) 
           odoor_cor1= odoor_cor1(keep)
           sc_cor1= sc_cor1(keep)
         ENDIF ELSE BEGIN
           cdoor_cor1= 0
           odoor_cor1= 0
           sc_cor1= ''
         ENDELSE
       END
     END

     IF (lpdoor.cor2_cnt GT 0) THEN BEGIN
       keep= WHERE(lpdoor.cor2_select EQ 1, cnt)
       IF (cnt LT lpdoor.cor2_cnt) THEN BEGIN
         lpdoor.cor2_cnt = cnt
         removed= 1 ; true
         IF (cnt GT 0) THEN BEGIN
           cdoor_cor2= cdoor_cor2(keep) 
           odoor_cor2= odoor_cor2(keep)
           sc_cor2= sc_cor2(keep)
         ENDIF ELSE BEGIN
           cdoor_cor2= 0
           odoor_cor2= 0
           sc_cor2= ''
         ENDELSE
       END
     END

     IF (DATATYPE(cdoor_euvi) NE 'UND' AND lpdoor.euvi_cnt GT 0) THEN BEGIN
       ;cdoor_euvi= cdoor_euvi(SORT(cdoor_euvi))
       ;odoor_euvi= odoor_euvi(SORT(odoor_euvi))
       srt= SORT(cdoor_euvi)
       cdoor_euvi= cdoor_euvi(srt)
       odoor_euvi= odoor_euvi(srt)
       sc_euvi= sc_euvi(srt)
     END
     IF (DATATYPE(cdoor_cor1) NE 'UND' AND lpdoor.cor1_cnt GT 0) THEN BEGIN
       ;cdoor_cor1= cdoor_cor1(SORT(cdoor_cor1))
       ;odoor_cor1= odoor_cor1(SORT(odoor_cor1))
       srt= SORT(cdoor_cor1)
       cdoor_cor1= cdoor_cor1(srt)
       odoor_cor1= odoor_cor1(srt)
       sc_cor1= sc_cor1(srt)
     END
     IF (DATATYPE(cdoor_cor2) NE 'UND' AND lpdoor.cor2_cnt GT 0) THEN BEGIN
       ;cdoor_cor2= cdoor_cor2(SORT(cdoor_cor2))
       ;odoor_cor2= odoor_cor2(SORT(odoor_cor2))
       srt= SORT(cdoor_cor2)
       cdoor_cor2= cdoor_cor2(srt)
       odoor_cor2= odoor_cor2(srt)
       sc_cor2= sc_cor2(srt)
     END
     removed= 1 ; true  ; to force a refresh incase there are sorts but not removes.

  RETURN, removed
END

PRO ADD2PLAN, tel
COMMON LP_SCIP_DOORS_SHARE, lpdoor
COMMON SCIP_DOORS_SHARE
COMMON SCHED_SHARE, schedv

     WIDGET_CONTROL, lpdoor.bdate, GET_VALUE= stime & stime = STRTRIM(stime(0),2)
     WIDGET_CONTROL, lpdoor.edate, GET_VALUE= etime & etime = STRTRIM(etime(0),2)

     IF (STRLEN(stime) GT 0 AND NOT CHECK_DATE(stime)) THEN BEGIN
       ERASE
       XYOUTS, 255, 5, '** Error in Close-Door Date -  "Add to '+tel+'" Ignored **', $
               CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1 
       RETURN
     ENDIF
     IF (STRLEN(etime) GT 0 AND NOT CHECK_DATE(etime)) THEN BEGIN
       ERASE
       XYOUTS, 255, 5, '** Error in Open-Door Date - "Add to '+tel+'" Ignored **', $
               CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1
       RETURN
     ENDIF
 
     IF (STRLEN(stime) EQ 0 AND STRLEN(etime) EQ 0) THEN BEGIN
       ERASE
       XYOUTS, 255, 5,'** No Close/Open Dates - "Add to '+tel+'" Ignored **', $
               CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1
       RETURN
     ENDIF

     IF (STRLEN(stime) EQ 0) THEN stime= '2000/01/01'
     IF (STRLEN(etime) EQ 0) THEN etime= '3000/01/01'

     stime = UTC2TAI(STR2UTC(stime))
     etime = UTC2TAI(STR2UTC(etime))

     IF (etime LE stime) THEN BEGIN
       ERASE
       XYOUTS, 255, 5, 'Open time MUST be greater than the Close time - "Add to '+tel+'" ignored', $
               CHARSIZE=1.3, ALIGNMENT= 0.5, /DEVICE, COLOR= 1

       RETURN
     ENDIF
     
     IF (schedv.sc EQ 1) THEN sc= 'A'
     IF (schedv.sc EQ 2) THEN sc= 'B'

     CASE (tel) OF
       'EUVI' : BEGIN
                  IF (DATATYPE(cdoor_euvi) EQ 'UND' OR lpdoor.euvi_cnt EQ 0) THEN BEGIN 
                    cdoor_euvi= stime
                    odoor_euvi= etime
                    sc_euvi= sc 
                  ENDIF ELSE BEGIN
                    cdoor_euvi= [cdoor_euvi,stime]
                    odoor_euvi= [odoor_euvi,etime]
                    sc_euvi=  [sc_euvi,sc]
                  ENDELSE
                END
       'COR1' : BEGIN
                  IF (DATATYPE(cdoor_cor1) EQ 'UND' OR lpdoor.cor1_cnt EQ 0) THEN BEGIN
                    cdoor_cor1= stime
                    odoor_cor1= etime
                    sc_cor1= sc
                  ENDIF ELSE BEGIN
                    cdoor_cor1= [cdoor_cor1,stime]
                    odoor_cor1= [odoor_cor1,etime]
                    sc_cor1= [sc_cor1,sc]
                  ENDELSE
                END
       'COR2' : BEGIN
                  IF (DATATYPE(cdoor_cor2) EQ 'UND' OR lpdoor.cor2_cnt EQ 0) THEN BEGIN
                    cdoor_cor2= stime
                    odoor_cor2= etime
                    sc_cor2= sc
                  ENDIF ELSE BEGIN
                    cdoor_cor2= [cdoor_cor2,stime]
                    odoor_cor2= [odoor_cor2,etime]
                    sc_cor2= [sc_cor2,sc]
                  ENDELSE
                END
       'SCIP' : BEGIN
                  IF (DATATYPE(cdoor_euvi) EQ 'UND' OR lpdoor.euvi_cnt EQ 0) THEN BEGIN
                    cdoor_euvi= stime
                    odoor_euvi= etime
                    sc_euvi= sc
                  ENDIF ELSE BEGIN
                    cdoor_euvi= [cdoor_euvi,stime]
                    odoor_euvi= [odoor_euvi,etime]
                    sc_euvi=  [sc_euvi,sc]
                  ENDELSE
                  IF (DATATYPE(cdoor_cor1) EQ 'UND' OR lpdoor.cor1_cnt EQ 0) THEN BEGIN
                    cdoor_cor1= stime
                    odoor_cor1= etime
                    sc_cor1= sc
                  ENDIF ELSE BEGIN
                    cdoor_cor1= [cdoor_cor1,stime]
                    odoor_cor1= [odoor_cor1,etime]
                    sc_cor1= [sc_cor1,sc]
                  ENDELSE
                   IF (DATATYPE(cdoor_cor2) EQ 'UND' OR lpdoor.cor2_cnt EQ 0) THEN BEGIN
                    cdoor_cor2= stime
                    odoor_cor2= etime
                    sc_cor2= sc
                  ENDIF ELSE BEGIN
                    cdoor_cor2= [cdoor_cor2,stime]
                    odoor_cor2= [odoor_cor2,etime]
                    sc_cor2= [sc_cor2,sc]
                  ENDELSE
                END
        ELSE :  BEGIN
                  ;WIDGET_CONTROL,lpdoor.msg1,SET_VALUE='*** Unknown SCIP Telescope '+tel+' ***'
                  ERASE
                  XYOUTS, 255, 5, '** Unknown SCIP Telescope '+tel+' **', $
                  CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 1
                  PRINT,'Unknown SCIP Telescope '+tel
                END
     ENDCASE
   
     WIDGET_CONTROL, /DESTROY, lpdoor.base1
     stat = UPDATE_SCHED_DOORS() ; call UPDATE_SCHED_DOORS again (recursively) to reflect the change.

  RETURN
END




PRO UPDATE_SCHED_DOORS_EVENT, event

COMMON LP_SCIP_DOORS_SHARE, lpdoor
COMMON SCIP_DOORS_SHARE

ieuvi= WHERE(lpdoor.euvi_rows EQ event.id, cnt) 
IF (cnt GT 0) THEN ieuvi= ieuvi(0) & ieuvi= 0 > ieuvi
icor1= WHERE(lpdoor.cor1_rows EQ event.id, cnt) 
IF (cnt GT 0) THEN icor1= icor1(0) & icor1= 0 > icor1
icor2= WHERE(lpdoor.cor2_rows EQ event.id, cnt)
IF (cnt GT 0) THEN icor2= icor2(0) & icor2= 0 > icor2

CASE (event.id) OF

   lpdoor.dismiss : BEGIN       ;** exit program
      lpdoor.scheduled= 0 ; false 
      WIDGET_CONTROL, /DESTROY, lpdoor.base1
   END

   lpdoor.refresh : BEGIN
     IF (DO_REFRESH()) THEN BEGIN
       WIDGET_CONTROL, /DESTROY, lpdoor.base1
       stat = UPDATE_SCHED_DOORS() ; call UPDATE_SCHED_DOORS again (recursively) to reflect the change.
     END
   END

;   lpdoor.bdate : BEGIN
;
;   END

;   lpdoor.edate : BEGIN
;
;   END

   lpdoor.euvi_rows(ieuvi) : BEGIN
     lpdoor.euvi_select(ieuvi)= event.select
   END

   lpdoor.cor1_rows(icor1) : BEGIN
     lpdoor.cor1_select(icor1)= event.select
   END

   lpdoor.cor2_rows(icor2) : BEGIN
     lpdoor.cor2_select(icor2)= event.select
   END

   lpdoor.add_euvi : BEGIN
     ADD2PLAN, "EUVI"
   END

   lpdoor.add_cor1 : BEGIN
     ADD2PLAN, "COR1"
   END

   lpdoor.add_cor2 : BEGIN
     ADD2PLAN, "COR2"
   END

   lpdoor.add_all : BEGIN
     ADD2PLAN, "SCIP"
   END

   lpdoor.schedule : BEGIN
     lpdoor.scheduled= 1 ; true
     stat= DO_REFRESH() 
     WIDGET_CONTROL, /DESTROY, lpdoor.base1
   END

   ELSE : BEGIN
   END

ENDCASE

END

 

FUNCTION UPDATE_SCHED_DOORS

COMMON LP_SCIP_DOORS_SHARE, lpdoor
COMMON SCIP_DOORS_SHARE
COMMON SCHED_SHARE, schedv

    IF XRegistered("UPDATE_SCHED_DOORS") THEN RETURN, 0   ; return false, AEE - 5/30/03 


    sc=''
    IF (schedv.sc EQ 1) THEN sc='A'
    IF (schedv.sc EQ 2) THEN sc='B'

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

      font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

     ;base1 = WIDGET_BASE(/COLUMN, TITLE=' Schedule SCIP Close/Open Doors', /FRAME)
      base1 = WIDGET_BASE(/COLUMN, TITLE=' Schedule SCIP Close/Open Doors (SpaceCraft '+sc+')', /FRAME)

      base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)
      row0 = WIDGET_BASE(base1, /ROW)

        col = WIDGET_BASE(row0, /COLUMN, /FRAME)
     
        row = WIDGET_BASE(col, /COLUMN, /FRAME)

        row1= WIDGET_BASE(row, /ROW, /FRAME)
        temp= WIDGET_LABEL(row1, VALUE='  Current Schedule Range:            '+ $
                           start_sched+' - '+stop_sched, FONT=font) 

        row2 = WIDGET_BASE(row, /ROW, /FRAME)
        temp= WIDGET_LABEL(row2, VALUE='Scheduled S/C Maneuvers: ', FONT=font)
        sc_man= CW_BSELECTOR2(row2,sc_maneuvers, SET_VALUE=0, UVALUE="SC_MAN")
 
        spc= '                   '
        col = WIDGET_BASE(col, /COLUMN, /FRAME)
        row = WIDGET_BASE(col, /ROW)
        ;temp= WIDGET_LABEL(row, VALUE=spc+'              Enter New Door Closure and Open Times', FONT=font)
        temp= WIDGET_LABEL(row, VALUE=spc+'Enter New Door Closure and Open Times for SpaceCraft '+sc, FONT=font)

        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc+'           Close Door at                           Open Door at', $
                           FONT=font)
        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc+'   yyyy/mm/dd hh:mm:ss          yyyy/mm/dd hh:mm:ss')

        row = WIDGET_BASE(col, /ROW)
        temp= WIDGET_LABEL(row, VALUE=spc)
        rr= WIDGET_BASE(row, /ROW, /FRAME)
        r3= WIDGET_BASE(rr, /ROW)
        ;start='yyyy/mm/dd mm:ss:ss'
        start=''
        bdate= WIDGET_TEXT(r3, /EDITABLE, YSIZE=1, XSIZE=20, VALUE=STRTRIM(STRING(start),2))
        temp= WIDGET_LABEL(row, VALUE='   ')
        rr= WIDGET_BASE(row, /ROW, /FRAME)
        r5= WIDGET_BASE(rr, /ROW)
        edate= WIDGET_TEXT(r5, /EDITABLE, YSIZE=1, XSIZE=20, VALUE=STRTRIM(STRING(start),2))

        spc= '         '
        row = WIDGET_BASE(col, /ROW)
        rr= WIDGET_BASE(row, /ROW, /FRAME)
        add_euvi = WIDGET_BUTTON(rr, VALUE=' Add to EUVI', FONT=font)
        temp= WIDGET_LABEL(rr, VALUE=spc)
        add_cor1 = WIDGET_BUTTON(rr, VALUE=' Add to COR1', FONT=font)
        temp= WIDGET_LABEL(rr, VALUE=spc)
        add_cor2 = WIDGET_BUTTON(rr, VALUE=' Add to COR2', FONT=font)
        temp= WIDGET_LABEL(rr, VALUE=spc)
        add_all  = WIDGET_BUTTON(rr, VALUE=' Add to SCIP', FONT=font)

        row = WIDGET_BASE(col, /ROW)
        rr= WIDGET_BASE(row, /ROW) 
        blanks= '                                                               '
        !p.background= 170 ; gray,  originally= 0 (black)
        msg1= WIDGET_DRAW(rr, XSIZE=510, YSIZE=20, RETAIN=2)



        col = WIDGET_BASE(row0, /COLUMN, /FRAME)
        row = WIDGET_BASE(col, /COLUMN)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='SCIP Door Commands Planning/Scheduling Notes:', FONT=font)
        row = WIDGET_BASE(col, /COLUMN)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='1. Use the menu on the left to plan and ADD new door closures and opens:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * If only a close-date is provided, the door is assumed to remain closed during the rest', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='     of the schedule (indicated by -->).', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * If only a open-date is provided, the door is assumed to have been closed before the', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='     start of the schedule. (indicated by <--).', FONT=font)
        row = WIDGET_BASE(col, /COLUMN)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='2. Use the menu below to view, verify, delete, or schedule the planned door commands:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * Press on the "Refresh" Button to:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       A.  Remove any Toggled-Off date-pairs.', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       B.  SORT remaining close and open dates by close dates.', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * Press on the "Add to Schedule" Button to:', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       A.  Perform a "Refresh".', FONT=font)
        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='       B.  Schedule close and open doors that remain Toggled-On.', FONT=font)

;        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * To SORT close and open door times (separately), press the "Refresh" button.', FONT=font)
;        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * To REMOVE one or more door close/open pair(s) from the lists below, toggle them', FONT=font)
;        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='      OFF and press on "Refresh".', FONT=font)

;        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='  * To SCHEDULE the toggled-ON door close/open commands from the lists below, press ', FONT=font)
;        temp= WIDGET_LABEL(row, /ALIGN_LEFT, VALUE='      on "Add to Schedule". It performs a "Refresh", first.', FONT=font)
     

        ; *****

      temp= WIDGET_LABEL(base1, VALUE='  ')

      base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)

      ;schedule = WIDGET_BUTTON(base2, VALUE='Add to Schedule', FONT=font)
      schedule = WIDGET_BUTTON(base2, VALUE='Add Checked Entries, Below, to Schedule', FONT=font)

      col = WIDGET_BASE(base2, /ROW, X_SCROLL_SIZE= 1080, Y_SCROLL_SIZE= 150, /FRAME)
 
      row = WIDGET_BASE(col, /COLUMN, /FRAME) 
      temp= WIDGET_LABEL(row, VALUE='Planned Close/Open Doors for EUVI', FONT=font)

      euvi_cnt= 0 
      euvi_rows= 0L
      euvi_select= 0B
      IF (DATATYPE(cdoor_euvi) NE 'UND') THEN BEGIN
       IF (cdoor_euvi(0) NE 0) THEN BEGIN
        euvi_cnt= N_ELEMENTS(cdoor_euvi)
        euvi_rows= LONARR(euvi_cnt)
        euvi_select= BYTARR(euvi_cnt)
        euvi_ctimes= LONARR(euvi_cnt)
        euvi_otimes= LONARR(euvi_cnt)
        FOR i=0, euvi_cnt-1 DO BEGIN 
          row1 = WIDGET_BASE(row, /ROW)
          col1 = WIDGET_BASE(row1, /COLUMN)
          row2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE)
          euvi_rows(i)= WIDGET_BUTTON(row2, VALUE=' ')
         
          time= UTC2STR(TAI2UTC(cdoor_euvi(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '2000') THEN time = '        <--'
          euvi_ctimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=19, VALUE=time)

          time= UTC2STR(TAI2UTC(odoor_euvi(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '3000') THEN time = '        -->'
          euvi_otimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=19, VALUE=time)
          temp= WIDGET_LABEL(row1,VALUE= sc_euvi(i), FONT=font)
        END
       END
      END

      row = WIDGET_BASE(col, /COLUMN, /FRAME)
      temp= WIDGET_LABEL(row, VALUE='Planned Close/Open Doors for COR1', FONT=font)

      cor1_cnt= 0
      cor1_rows= 0L
      cor1_select= 0B
      IF (DATATYPE(cdoor_cor1) NE 'UND') THEN BEGIN
       IF (cdoor_cor1(0) NE 0) THEN BEGIN
        cor1_cnt= N_ELEMENTS(cdoor_cor1)
        cor1_rows= LONARR(cor1_cnt)
        cor1_select= BYTARR(cor1_cnt)
        cor1_ctimes= LONARR(cor1_cnt)
        cor1_otimes= LONARR(cor1_cnt)
        FOR i=0, cor1_cnt-1 DO BEGIN
          row1 = WIDGET_BASE(row, /ROW)
          col1 = WIDGET_BASE(row1, /COLUMN)
          row2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE)
          cor1_rows(i)= WIDGET_BUTTON(row2, VALUE=' ')
          
          time= UTC2STR(TAI2UTC(cdoor_cor1(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '2000') THEN time = '        <--'
          cor1_ctimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=19, VALUE=time)

          time= UTC2STR(TAI2UTC(odoor_cor1(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '3000') THEN time = '        -->'
          cor1_otimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=19, VALUE=time)
          temp= WIDGET_LABEL(row1,VALUE= sc_cor1(i), FONT=font)
        END
       END
      END 


      row = WIDGET_BASE(col, /COLUMN, /FRAME)
      temp= WIDGET_LABEL(row, VALUE='Planned Close/Open Doors for COR2', FONT=font)

      cor2_cnt= 0
      cor2_rows= 0L
      cor2_select= 0B
      IF (DATATYPE(cdoor_cor2) NE 'UND') THEN BEGIN
       IF (cdoor_cor2(0) NE 0) THEN BEGIN
        cor2_cnt= N_ELEMENTS(cdoor_cor2)
        cor2_rows= LONARR(cor2_cnt)
        cor2_select= BYTARR(cor2_cnt)
        cor2_ctimes= LONARR(cor2_cnt)
        cor2_otimes= LONARR(cor2_cnt)
        FOR i=0, cor2_cnt-1 DO BEGIN
          row1 = WIDGET_BASE(row, /ROW)
          col1 = WIDGET_BASE(row1, /COLUMN)
          row2 = WIDGET_BASE(col1, /COLUMN, /NONEXCLUSIVE)
          cor2_rows(i)= WIDGET_BUTTON(row2, VALUE=' ')

          time= UTC2STR(TAI2UTC(cdoor_cor2(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '2000') THEN time = '        <--'
          cor2_ctimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=19, VALUE=time)

          time= UTC2STR(TAI2UTC(odoor_cor2(i)),/ECS)
          time= STRMID(time,0,19)
          IF (STRMID(time,0,4) EQ '3000') THEN time = '        -->'
          cor2_otimes(i)= WIDGET_TEXT(row1, YSIZE=1, XSIZE=19, VALUE=time)
          temp= WIDGET_LABEL(row1,VALUE= sc_cor2(i), FONT=font)
        END
       END
      END
        
      blanks='        Check/Correct/Remove Door CLOSE/OPEN times. "Refresh" updates the display with the latest info. "Add to Schedule" schedules the displayed door closures and opens.        ' 
      col = WIDGET_BASE(base2, /ROW)

     refresh = WIDGET_BUTTON(base2, VALUE=' Refresh ', FONT=font)
     dismiss = WIDGET_BUTTON(base2, VALUE=' Dismiss ', FONT=font)

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base1
    WIDGET_CONTROL, bdate, /INPUT_FOCUS

    FOR i= 0, euvi_cnt-1 DO BEGIN
      WIDGET_CONTROL, euvi_rows(i), SET_BUTTON = 1 
      euvi_select(i)= 1
    END
    FOR i= 0, cor1_cnt-1 DO BEGIN
      WIDGET_CONTROL, cor1_rows(i), SET_BUTTON = 1 
      cor1_select(i)= 1
    END
    FOR i= 0, cor2_cnt-1 DO BEGIN 
      WIDGET_CONTROL, cor2_rows(i), SET_BUTTON = 1 
      cor2_select(i)= 1
    END


    lpdoor = CREATE_STRUCT( 'base1', base1,                $
                            'euvi_rows', euvi_rows,        $
                            'euvi_select', euvi_select,    $
                            'euvi_cnt', euvi_cnt,          $
                            'cor1_rows', cor1_rows,        $
                            'cor1_select', cor1_select,    $
                            'cor1_cnt', cor1_cnt,          $
                            'cor2_rows', cor2_rows,        $
                            'cor2_select', cor2_select,    $
                            'cor2_cnt', cor2_cnt,          $
                            ;'msg', msg                ,    $
                            'msg1', msg1              ,    $
                            'dismiss', dismiss        ,    $
                            'scheduled', 0            ,    $
                            'refresh', refresh        ,    $
                            'bdate', bdate            ,    $
                            'edate', edate            ,    $
                            'add_euvi', add_euvi  ,        $
                            'add_cor1', add_cor1  ,        $
                            'add_cor2', add_cor2  ,        $
                            'add_all', add_all    ,        $
                            'schedule', schedule)

    WIDGET_CONTROL, base1, SET_UVALUE= lpdoor

    txt= 'Enter Close/Open Dates and Add to the Plan'  

    ERASE
    XYOUTS, 255, 5, txt, CHARSIZE=1.5, ALIGNMENT= 0.5, /DEVICE, COLOR= 250

    XMANAGER, "UPDATE_SCHED_DOORS", base1, /MODAL  ; AEE - Mar 20, 03

 !p.background= 0 ; originally= 0

  RETURN,lpdoor.scheduled

END


