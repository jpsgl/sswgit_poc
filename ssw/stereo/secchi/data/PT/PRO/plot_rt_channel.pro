;+
;$Id: plot_rt_channel.pro,v 1.8 2009/09/11 20:28:17 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : PLOT_RT_CHANNEL
;               
; Purpose     : Plots one-second snapshot of RT channel for the displayed schedule.
;               
; Use         : PLOT_RT_CHANNEL, schedv, expanded, prev_hr, rt_rate, size, tdump,
;                                ybot, ytop, ylimit, xarr, secs, startdis, enddis, gt_data
;    
; Inputs      : schedv      Structure containing widgets information. 
;               expanded    Structure containing info for each image destained for RT-Channel.
;               prev_hr     Hours prior to the start of schedule (one) to calculate statistics. 
;               rt_rate     RT channel telemetry rate(s) during playback periods(s)
;               size        Capacity of RT channel (1 packet but not used).
;               tdump       Schedule of telemetry Playbacks (from .SCT file).
;               ybot, ytop  Scale to plot window, ytop-ybot is the RT-window range.
;               ylimit      Scale to plot window, ylimit-ybot is the max. RT-window range.
;               xarr        Plot-range in seconds (including the prev_hr). 
;               secs        Number of seconds in the plot-range (including the prev_hr).
;               startdis    Start time of the schedule.
;               enddis      End time of the schedule.
;               gt_data     Structure containg GT data-dumps info.
;    
;
; Opt. Inputs : None
;               
; Outputs     : None 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/25/04 - Re-arranged Tlm load section.
;              Ed Esfandiari 05/26/06 - modified warning test.
;              Ed Esfandiari 06/23/06 - use exact s1 and s2.
;              Ed Esfandiari 08/30/06 - Added rates from SCA and allow multiple playbacks/rates. 
;              Ed Esfandiari 09/02/06 - Added nominal 3.26 kbps HK rate to SSR1 and RT.
;              Ed Esfandiari 09/06/06 - Added GT dumps.
;              Ed Esfandiari 06/20/07 - Added toggle option to display data volume/downloadable ratio or volume in MB.
;              Ed Esfandiari 01/22/09 - Changed HK rate from maximum 3.26 to reduced 2.00 pbks (~21 MB/day) - NR suggested.
;              Ed Esfandiari 09/02/09 - changed data volume display from .1 to .3 decimal places.
;
; $Log: plot_rt_channel.pro,v $
; Revision 1.8  2009/09/11 20:28:17  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:06  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;

PRO PLOT_RT_CHANNEL, schedv, expanded, prev_hr, rt_rate, size, tdump, ybot, ytop, ylimit, $
                     xarr, secs, startdis, enddis, gt_data

COMMON RT_CHANNEL, rt_rates
COMMON TM_DATA

;   AEE 4/8/04:
;   Note: rt_rate at this point is the number of packets/second (not bits/sec) which should be changed
;         to bits/sec. Also size on input is number of packets that RT-channel can hold at a time
;         (normally 1-packet). But since the resolution of display plot is in seconds, I use
;         the rt_rate (i.e. 50 packets/sec) as the size instead.

;   March 05, 04 - AEE
;   Notes: 
;   For the RT-channel, we can assume a varabile packets/sec rate that can be changed on a daily
;   basis. So, I can assume a rate of X (# of packets per second) somehow will be provided to PT 
;   once per day to be used here (X is an integer, such as 50, and can change only once per day). 
; 
;   There are 272 bytes per packet. We can figure out how many packets per image (use the image 
;   size after compression, etc.) and since RT-buffer can only hold one packet at a time, we 
;   should use the telemetry rate, X, to find out how many seconds it takes for an image to be
;   transferred over RT-channel.
;
;   The flight software does not know when a contact is made and keeps spitting out X number
;   of packets (it knows X) per second. If some of these packets are before or after the
;   contact times, they will be lost so I should hi-lite those packets so that the image's 
;   scheudle time can be modified or image is removed.
;   NOTE:
;   Also, images going to SSR1, SSR2, and SW at the same time or after the RT image, may need to
;   be delayed because a RT image at rt_rate takes longer to be moved out of the secchi telemtry
;   buffer but I use the much faster sb_rate as the time it takes for an image to go to SSR1, 
;   SSR2, and SW. 


    IF MAX(rt_rate) EQ 0 THEN rt_rate= 3600 ; bps

    bufsize= MAX(rt_rate) ; use playback rate for highest of  playback rates (bits).

    ; Since RT is only downlinked during certain contact times, create an array of downlinks for
    ; each second (set to zero) and only set the valid downlink periods to the actual downlink rate:

    ;arry= FLTARR(5,secs)           ;** only contains os_size at os_stop for every scheduled image
    arry= FLTARR(7,secs)            ;** to keep track of HK and GT data.

    ihk= 5
    igt= 6
    ;arry(ihk,*) = 3.26*1000 ; nominal HK data rate to RT 
    ;arry(ihk,*) = 2.00*1000 ; reduced HK data rate to RT 

    ; ADD GT-dumps data, if any:
    FOR g= 1, N_ELEMENTS(gt_data)-1 DO BEGIN ; first element is always empty
      IF (STRPOS(gt_data(g).apid,'RT') GE 0) THEN BEGIN
        g1= (gt_data(g).x1- (!X.CRANGE(0)-prev_hr))*3600D
        g2= (gt_data(g).x2- (!X.CRANGE(0)-prev_hr))*3600D
        arry(igt,g1:g2)= gt_data(g).rate
      ENDIF 
    ENDFOR

    dlarry= fltarr(secs)
    FOR k = 0, N_ELEMENTS(tdump) - 1 DO BEGIN  ; account for downlinks in the tape-dump period(s):
      x1 = LONG(tdump(k).x1) > 0
      ;x2 = LONG(tdump(k).x2) < (secs-1)
      x2 = (LONG(tdump(k).x2) < (secs-1)) > 0
      ;dlarry(x1:x2)= tsize ; only tsize bits can be downloaded per second on RT.
      dlarry(x1:x2)= tdump(k).rate(3) ; # of bits can be downloaded per second on RT during this playback.
      ;arry(ihk,x1:x2) = 3.26*1000 ; nominal HK data rate to RT (populate only for playbcak periods)
      arry(ihk,x1:x2) = 2.0*1000 ; reduced HK data rate to RT (populate only for playbcak periods)
    ENDFOR


   IF (DATATYPE(expanded) EQ 'STC') THEN BEGIN
    FOR i = 0, N_ELEMENTS(expanded)-1 DO BEGIN 
      tstop = expanded(i).stop

      ; use all expanded images within the prev_hr before the start of plot display to the end of the
      ; plot display:

      IF ((tstop GE (!X.CRANGE(0)-prev_hr)) AND (tstop LT !X.CRANGE(1))) THEN BEGIN
        tstopind = (tstop - (!X.CRANGE(0)-prev_hr))
 
        tele = expanded(i).tele

        ; Find start and stop seconds between which the image can be downloded at the provided rt_rate:

        s1 = LONG(tstopind*3600D - expanded(i).proc_time)
        this_rate= dlarry(s1)
        IF (this_rate EQ 0) THEN this_rate= MAX(rt_rate) 
        nsec= LONG( expanded(i).size / this_rate + 0.99)
        IF (nsec LT 1.0) THEN nsec= 1.0
        s2 = LONG(s1 + nsec -1)

        s1= s1 > 0
        s2= s2 < LONG(secs-1)
        arry(tele,s1:s2) = arry(tele,s1:s2) + this_rate 
      ENDIF
    ENDFOR
  ENDIF


    buffer = FLTARR(secs)           ;** contains amt of data in buffer 1 entry every second
    tarry = TOTAL(arry,1)  ; totats values of all cameras (0 to 4) for each entry (each second) and collapses
                           ; arry(5,secs) to tarry(secs)

    buffer =  (tarry - dlarry) > 0

    ; At this point if any data is left in buffer will be lost so mark them as so:

        ; AEE 3/9/04 - only keep the data within the current displayed plot-range:
        ; Note: xarr, as it comes in, contains the plot_range in the display appended to the previous
        ;       24 hrs. It has 24*3600 + display-range in seconds + 1 elements. Element values are
        ;       in hrs and the first 2 values, xarr(0:1), are -24.0, -23.999722 and gets down to 0.0 and
        ;       (start of the plot display-range) and then up to the end of the plot display-range.

        plot_range= WHERE(xarr GE !x.crange(0) AND xarr LE !x.crange(1))
        buffer= buffer(plot_range)
        arrx= xarr(plot_range)
        td= dlarry(plot_range)

        save_buffer = buffer
        buffer = buffer / bufsize       ;** normalize to 1.0 = 100% full (buffer range = 0.0-1.0)
        buffer = buffer * (ytop-ybot) ;** scale to plot window, ytop-ybot is the window range-AEE 6/17/03
        buffer = (buffer < (ylimit-ybot))  ;** ylimit-ybot is the max. window range ; AEE 6/17/03
    
        lost= WHERE(buffer GT 0, lcnt)

        IF (lcnt GT 0) THEN BEGIN

          ; If lost data indexes are not continous, separate them before ployfilling so that
          ; each continous block of indexes are plotted separetey:

          slost= SHIFT(lost,1)
          nlost= WHERE(lost-slost GT 1, ncnt)
          IF (ncnt EQ 0) THEN $
             nlost= [0,lcnt] $
          ELSE $
             nlost= [0,nlost,lcnt]

          FOR n= 0, N_ELEMENTS(nlost)-2 DO BEGIN 
            l1= lost(nlost(n))
            l2= lost(nlost(n+1)-1)
            pxval = arrx(l1:l2)
            lcnt= l2-l1+1
            pxval = [pxval(0), pxval, pxval(lcnt-1)]
            ;POLYFILL, pxval, [ybot,ybot+buffer(l1:l2)-0.1, ybot], color=1 ;  polyfill using red
            ;POLYFILL, pxval, [ybot+0.1,ybot+buffer(l1:l2)-0.1, ybot+0.1] < ytop-0.2, color=1 ;  polyfill using red
            tmpb= buffer(l1:l2)
            IF (MAX(tmpb) LT ybot+0.5) THEN tmpb= tmpb+0.5 ; so we can see better when red line is hard to see.
            POLYFILL, pxval, [ybot+0.1,ybot+tmpb-0.1, ybot+0.1] < ytop-0.2, color=1 ;  polyfill using red
            ;Display lost data, outside of tape-dump period(s) or too much in the dump area, in red:
            OPLOT, arrx(l1:l2), FINDGEN(lcnt)*0.0+ybot+0.1, PSYM=3, COLOR=1
            ;OPLOT, arrx(l1:l1+1),[ybot+0.1,ytop-0.8], COLOR= 1 ;Also display a red bar because for a wide plot-display
                                                           ;polyfill can't be seen. 
          ENDFOR
        ENDIF

        ; Flag places within the tape-dump range(s) where the buffer is empty with green:

        tdind= WHERE(td GT 0, tdicnt)
        IF (tdicnt GT 0) THEN BEGIN
          zeros= WHERE(buffer(tdind) LE 0, zcnt)
          IF(zcnt GT 0) THEN $
            OPLOT, arrx(tdind(zeros)), (ybot + FLTARR(zcnt)) , PSYM=3, COLOR=2 ; AEE - 6/17/03 - use Green
        ENDIF

        ;** Update buffer statistics in schedule widget
        ;**
        date_str = UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' + $
                   UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
        ;WIDGET_CONTROL, schedv.usage_dates, SET_VALUE="Data/Telemetry Loads For:  "+date_str
        WIDGET_CONTROL, schedv.usage_dates, SET_VALUE=date_str


        ;** calculate total bits available for SCI downlink over plotting range

        ;AEE - 3/9/04:
        ;total_bits_avail is total number of telemetry bits that can be downloaded during RT playback
        ;contacts within the displayed plot-range. So, if in a 24-hr plot-range there is a one-hour
        ;RT playback, number of bits that can be trasferred over that 1-hr period is the total_bits_avail.
        ;So, For RT, total_bits_avail is the tape-dump period(s) within the displayed plot-range NOT
        ;all of the plot-range.
        ;
        ; For RT, usage is the scheduled data bits only within the RT tape-dump period(s), if any,
        ; converted to % of buffer used. 

        arry= arry(*,plot_range)

        pct_total= 0D
        ;pct_each= DBLARR(5)
        pct_each= DBLARR(7)

        rt_usage= arry(*,plot_range) 
        rt_total_bits_avail= 0.0D
        rt_usage_avail= FLTARR(7,2) 
        IF (tdicnt GT 0) THEN BEGIN
          rt_usage_avail= arry(*,tdind)  ; only for images during playback period(s)
          rt_total_bits_avail= TOTAL(DOUBLE(td(tdind)))
        ENDIF

      IF (schedv.pct_stats EQ 1) THEN BEGIN
        pct_total = DOUBLE(TOTAL(rt_usage))/8000000.0 ; bits to MB
        pct_each =  DOUBLE(TOTAL(rt_usage,2))/8000000.0 ; bits to MB
        WIDGET_CONTROL, schedv.rt_tot, SET_VALUE= STRING(pct_total,'(f12.3)') +' '
        WIDGET_CONTROL, schedv.rt_eu, SET_VALUE= STRING(pct_each(0),'(f13.3)')+' '
        WIDGET_CONTROL, schedv.rt_c1, SET_VALUE= STRING(pct_each(1),'(f13.3)')+' '
        WIDGET_CONTROL, schedv.rt_c2, SET_VALUE= STRING(pct_each(2),'(f11.3)')+' '
        WIDGET_CONTROL, schedv.rt_h1, SET_VALUE= STRING(pct_each(3),'(f12.3)')+' '
        WIDGET_CONTROL, schedv.rt_h2, SET_VALUE= STRING(pct_each(4),'(f11.3)')+' '
        WIDGET_CONTROL, schedv.rt_hk, SET_VALUE= STRING(pct_each(ihk),'(f11.3)')+' '
        WIDGET_CONTROL, schedv.rt_gt, SET_VALUE= STRING(pct_each(igt),'(f11.3)')+' '
        ;goto, skipdata
      ENDIF ELSE BEGIN
        IF (tdicnt GT 0) THEN BEGIN
          bits_avail = DOUBLE(td(tdind)) 
          usage = arry(*,tdind)  ; only for images during playback period(s)
          total_bits_avail= TOTAL(bits_avail)
          pct_total = (DOUBLE(TOTAL(usage)) / total_bits_avail) * 100
          pct_each = (DOUBLE(TOTAL(usage,2)) / total_bits_avail) * 100
        ENDIF

        ;stats_str = STRING(pct_total,'(f12.3)')  +'%'+ $
        ;            STRING(pct_each(0),'(f13.3)')+'%'+ $
        ;            STRING(pct_each(1),'(f9.3)') +'%'+ $
        ;            STRING(pct_each(2),'(f11.3)')+'%'+ $
        ;            STRING(pct_each(3),'(f12.3)')+'%'+ $
        ;            ;STRING(pct_each(4),'(f11.3)')+'%'
        ;            STRING(pct_each(4),'(f11.3)')+'%'+ $
        ;            STRING(pct_each(ihk),'(f11.3)')+'%'+ $
        ;            STRING(pct_each(igt),'(f11.3)')+'%'
        ;WIDGET_CONTROL, schedv.rt_stats, SET_VALUE='RTch '+stats_str
        WIDGET_CONTROL, schedv.rt_tot, SET_VALUE= STRING(pct_total,'(f12.3)') +'%'
        WIDGET_CONTROL, schedv.rt_eu, SET_VALUE= STRING(pct_each(0),'(f13.3)')+'%'
        WIDGET_CONTROL, schedv.rt_c1, SET_VALUE= STRING(pct_each(1),'(f13.3)')+'%'
        WIDGET_CONTROL, schedv.rt_c2, SET_VALUE= STRING(pct_each(2),'(f11.3)')+'%'
        WIDGET_CONTROL, schedv.rt_h1, SET_VALUE= STRING(pct_each(3),'(f12.3)')+'%'
        WIDGET_CONTROL, schedv.rt_h2, SET_VALUE= STRING(pct_each(4),'(f11.3)')+'%'
        WIDGET_CONTROL, schedv.rt_hk, SET_VALUE= STRING(pct_each(ihk),'(f11.3)')+'%'
        WIDGET_CONTROL, schedv.rt_gt, SET_VALUE= STRING(pct_each(igt),'(f11.3)')+'%'
     ENDELSE
;skipdata:

  RETURN
END
