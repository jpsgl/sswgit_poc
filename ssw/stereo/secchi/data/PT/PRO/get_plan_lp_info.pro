;+
;$Id: get_plan_lp_info.pro,v 1.4 2005/01/24 17:56:33 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GET_PLAN_LP_INFO
;               
; Purpose     : Return details of the given SEB Program.
;               
; Explanation : This routine will read the details (tele,exptable,camtable, etc)
;		for the current SEB Program being scheduled (plan_lp)
;		from the appropriate LP_..._SHARE common block.
;               
; Use         : result = GET_PLAN_LP_INFO( tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, 
;                                          ex_table, sub, start, cadence, fps)
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : 0 if LP was found, otherwise -1.
;               tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, ex_table, sub, start, 
;               cadence, fps. 
;		These are variables that will be filled.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Based on SOHO/LASCO planning tool. 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/30/04 - Removed sync.
;
; $Log: get_plan_lp_info.pro,v $
; Revision 1.4  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:03  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

FUNCTION GET_PLAN_LP_INFO, tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, ex_table, sub, $ 
                           start, cadence, fps

COMMON OS_INIT_SHARE
COMMON OS_SHARE
COMMON OS_ALL_SHARE
COMMON LP_NORMAL_IMAGE_SHARE
COMMON LP_DARK_IMAGE_SHARE
COMMON LP_SEQ_PW_FW_SHARE
COMMON LP_HI_SEQ_SHARE
COMMON LP_CAL_LAMP_SHARE
COMMON LP_CONT_IMAGE_SHARE ; AEE May 19,03
COMMON LP_DOUBLE_IMAGE_SHARE ; AEE 9/23/03

;** depending on plan_lp (defined in OS_INIT_SHARE) this routine returns the above
;** variables from the correct COMMON LP_...

sub = 0
start = 0 

cadence= 0  ; AEE - 01/15/03 - dummy default for non sequence LPs.

CASE (plan_lp) OF

   0 : BEGIN                 ;** Double Sky Image LP  - AEE 9/23/03
      tele = lpdoubv.tele
      exptable = lpdoubv.exptable ; AEE 1/13/04
      camtable = lpdoubv.camtable ; AEE 1/13/04
      fps = lpdoubv.fps ; AEE 1/13/04
      iptable = lpdoubv.iptable
      fw = lpdoubv.fw
      pw = [lpdoubv.pw,lpdoubv.pw2] 
      lamp = 0
      lp_num_images = 1
      ex_table = ex
   END

   1 : BEGIN                 ;** Normal Sky Image LP
      tele = lpnormv.tele
      exptable = lpnormv.exptable ; AEE 1/13/04
      camtable = lpnormv.camtable ; AEE 1/13/04
      fps = lpnormv.fps ; AEE 1/13/04
      iptable = lpnormv.iptable
      fw = lpnormv.fw
      pw = INTARR(5)+lpnormv.pw
      lamp = 0
      lp_num_images = 1
      ex_table = ex
   END

   2 : BEGIN                 ;** Dark Image LP
      tele = lpdarkv.tele
      exptable= 0 ; AEE 1/13/04
      camtable = lpdarkv.camtable ; AEE 1/13/04
      fps = lpdarkv.fps ; AEE 4/6/04 
      iptable = lpdarkv.iptable
      fw = 0
      pw = INTARR(5)
      lamp = 0
      lp_num_images = 1

      ; AEE 9/26/03 - changed the above to the following (assgin exp. value read to the OS but not to the 
      ;               default (ex) tables) - Just like Cal lamps.

      ex_table = ex
      ex_table(tele,exptable,0,0) = lpdarkv.expt

   END

   3 : BEGIN                 ;** Cal Lamp LP
      tele = lpcalv.tele
      exptable = 0 ; AEE 1/13/04
      camtable = lpcalv.camtable ; AEE 1/13/04
      fps = lpcalv.fps ; AEE 1/13/04
      iptable = lpcalv.iptable
      fw = lpcalv.fw
      pw = INTARR(5)+lpcalv.pw
      lamp = lpcalv.lamp
      lp_num_images = 1
      ex_table = ex
      ex_table(tele,exptable,fw,pw) = lpcalv.pulse  ; AEE - 1/13/04
   END

   4 : BEGIN                 ;** Continous Image LP ; AEE - 05/19/03
      tele = lpcont.tele
      exptable = 0 ; AEE 1/13/04
      camtable = lpcont.camtable ; AEE 1/13/04
      fps = lpcont.fps ; AEE 1/13/04
      iptable = lpcont.iptable
      fw = lpcont.fw
      pw = INTARR(5)+lpcont.pw
      lamp = 0
      lp_num_images = 1
      ex_table = ex
   END

   5 : BEGIN                 ;** SCIP Seq LP
      tele = lpseqpwv.tele
      exptable = lpseqpwv.exptable ; AEE 1/13/04
      camtable = lpseqpwv.camtable ; AEE 1/13/04
      fps = lpseqpwv.fps ; AEE 1/13/04
      iptable = lpseqpwv.iptable
      cadence= lpseqpwv.cadence    ; AEE - 01/15/03  - cadence between images in the sequence
      fw = lpseqpwv.fw
      pw = lpseqpwv.pw
      lamp = 0
      lp_num_images = lpseqpwv.num  ; AEE Nov 27, 02
      ex_table = ex
   END

   6 : BEGIN                 ;** HI Seq
      tele = [3,4] ; Hi1 and HI2 
      exptable = [lpseqhi.hi1_exptable,lpseqhi.hi2_exptable] ; AEE 1/13/04
      camtable = [lpseqhi.hi1_camtable,lpseqhi.hi2_camtable] ; AEE 1/13/04
      fps = lpseqhi.fps ; AEE 1/13/04
      iptable = [lpseqhi.hi1_iptable,lpseqhi.hi2_iptable]
      cadence= [lpseqhi.hi1_cadence,lpseqhi.hi2_cadence]  ; AEE - 01/15/03
      fw = [0,0] ; clear for both HI1 and HI2 
      pw = [0,0] ; clear for both HI1 and HI2 
      lamp = [0,0] ; for both HI1 and HI2
      lp_num_images = [lpseqhi.hi1_images,lpseqhi.hi2_images]
      ex_table = ex  ; ex has info for all 5 tele, 5 tables, filters, and polars.
   END

   7 : BEGIN                 ;** Block Seq  ; AEE - 02/10/03
      tele = -1 
      table = 0
      iptable = 0
      fw = 0
      pw = 0
      lamp = 0
      ex_table = 0
      start = lpcmdv.start
      lp_num_images = lpcmdv.stop	;** this is the stop index
   END

   ELSE : BEGIN 
      WIDGET_CONTROL, osv.info_text, SET_VALUE='No LP currently being scheduled.'
      PRINT, 'No LP currently being scheduled.'
      RETURN, -1
   END

ENDCASE

RETURN, 0

END
