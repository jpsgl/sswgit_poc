;+
;$Id: plot_sw_channel.pro,v 1.8 2009/09/11 20:28:19 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : PLOT_SW_CHANNEL
;
; Purpose     : Plots one-second snapshot of SW channel for the displayed schedule.
;
; Use         : PLOT_SW_CHANNEL, schedv, expanded, prev_hr, rate, turesize, size, tdump,
;                                ybot, ytop, ylimit, xarr, secs, startdis, enddis,
;                                pct_full, sb_rate
;
; Inputs      : schedv      Structure containing widgets information.
;               expanded    Structure containing info for each image destained for SW-Channel.
;               prev_hr     Hours prior to the start of schedule (one) to calculate statistics.
;               rate        SW channel telemetry rate.
;               truesize    True capacity of SW
;               size        Downloadable size of SSR1 channel (depends on mission high, med, low dates). 
;               tdump       Schedule of telemetry Playbacks (from .SCT file) for current display.
;               ybot, ytop  Scale to plot window, ytop-ybot is the SW-window range.
;               ylimit      Scale to plot window, ylimit-ybot is the max. SW-window range.
;               xarr        Plot-range in seconds (including the prev_hr).
;               secs        Number of seconds in the plot-range (including the prev_hr).
;               startdis    Start time of the schedule.
;               enddis      End time of the schedule.
;               pct_full    Percentage of SW-capacity in use at the start of the schedule.
;               sb_rate     Secchi-Buffer (RAM-disk) rate.
;
;
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 10/25/04 - Re-arranged Tlm load section.
;              Ed Esfandiari 06/23/06 - use exact s1 and s2.
;              Ed Esfandiari 08/25/06 - new capacity and rate usage.
;              Ed Esfandiari 06/20/07 - Added toggle option to display data volume/downloadable ratio or volume in MB.
;              Ed Esfandiari 09/02/09 - changed data volume display from .1 to .3 decimal places.
;
; $Log: plot_sw_channel.pro,v $
; Revision 1.8  2009/09/11 20:28:19  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


PRO PLOT_SW_CHANNEL, schedv, expanded, prev_hr, rate, truesize, bsize, ybot, ytop, ylimit, xarr, secs, $
                   startdis, enddis, pct_full, sb_rate
COMMON TM_DATA

; NOTE: secchi buffer empties at a rate of 153 kbs (~2min for a full size image to travel through).

        truesize= truesize*8000000.0    ; convert true physical SW size from MB to bits
        bufsize= bsize*8000000.0           ; convert from MB to bits

        arry= FLTARR(5,secs)           ;** only contains os_size at os_stop for every scheduled image

        FOR i = 0L, N_ELEMENTS(expanded)-1 DO BEGIN ; AEE - 01/16/03

           tstop = expanded(i).stop
 
           tsecs = 1 ; AEE - Dec 20, 02

           IF ((tstop GE (!X.CRANGE(0)-prev_hr)) AND (tstop LT !X.CRANGE(1))) THEN BEGIN ;should always be true
              tstopind = (tstop - (!X.CRANGE(0)-prev_hr))

              tele = expanded(i).tele

              ; AEE 3/3/04 - use the secchi_buffer rate (sb_rate) and the image size to spread the
              ; image being placed on SW:

              ;s1 = tstopind*3600D - expanded(i).proc_time
              ;s2= s1 + expanded(i).size / sb_rate
              s1 = LONG(tstopind*3600D - expanded(i).proc_time)
              s2= s1 + LONG(expanded(i).size / sb_rate)

              tsize = expanded(i).size / DOUBLE(s2-s1+1)

              ; Note: tsize should be calculated before s1 and/or s2 or possibly changed since
              ;       we want to keep the true one second value for it.

              s1= s1 > 0
              s2= s2 < LONG(secs-1)

              IF (s2 LT s1) THEN s2= s1

              IF (s2 LT N_ELEMENTS(arry(tele,*))) THEN BEGIN
                arry(tele,s1:s2) = arry(tele,s1:s2) + tsize
                ;** Assume 1st os starts with buffer at a certain percentage:
                ;IF (i EQ 0) THEN arry(tele,s1) = arry(tele,s1) + bufsize*(FLOAT(pct_full)/100)
                IF (i EQ 0) THEN arry(tele,s1) = arry(tele,s1) + truesize*(FLOAT(pct_full)/100)
              ENDIF ELSE BEGIN
                PRINT,'%SCHEDULE_PLOT: telemetry buffer index where image is to be placed is out of current plot-range.'
              ENDELSE

           ENDIF
        ENDFOR

;******


        buffer = FLTARR(secs)           ;** contains amt of data in buffer 1 entry every second
        tarry = TOTAL(arry,1)  ; totats values of all cameras (0 to 4) for each entry (each second) and collapses
                               ; arry(5,secs) to tarry(secs)
        buffer(*,0) = tarry(0) ; copy first value from tarry to all of the buffer
        FOR i = 1L, N_ELEMENTS(tarry)-1 DO BEGIN        ;** for every second in plot range
           ;** amt in buffer the previous minute - amt downlinked last minute + any new data
           buffer(i) = (buffer(i-1) + tarry(i) - rate) > 0   ; AEE - March 4, 04
        ENDFOR

        b = WHERE(buffer NE 0)
        IF (b(0) GT 0) THEN $    ;** show buffer as being % full before 1st os starts
          ;buffer(0:b(0)-1) = bufsize*(FLOAT(pct_full)/100)
          buffer(0:b(0)-1) = truesize*(FLOAT(pct_full)/100)

        ; AEE 3/9/04 - only keep the data within the current displayed plot-range:
        ; Note: xarr, as it comes in, contains the plot_range in the display appended to the previous
        ;       24 hrs. It has 24*3600 + display-range in seconds + 1 elements. Element values are
        ;       in hrs and the first 2 values, xarr(0:1), are -24.0, -23.999722 and gets down to 0.0 and
        ;       (start of the plot display-range) and then up to the end of the plot display-range.

        plot_range= WHERE(xarr GE !x.crange(0) AND xarr LT !x.crange(1))
        buffer= buffer(plot_range)
        arrx= xarr(plot_range)

        save_buffer = buffer
        ;buffer = buffer / bufsize       ;** normalize to 1.0 = 100% full (buffer range = 0.0-1.0)
        buffer = buffer / truesize       ;** normalize to 1.0 = 100% full (buffer range = 0.0-1.0)
        buffer = buffer * (ytop-ybot) ;** scale to plot window, ytop-ybot is the window range-AEE 6/17/03
        buffer = (buffer < (ylimit-ybot))  ;** ylimit-ybot is the max. window range ; AEE 6/17/03

        nelem= N_ELEMENTS(arrx)

        pxval = [arrx(0), arrx, arrx(nelem-1)]
        POLYFILL, pxval, [ybot+0.1,ybot+buffer-0.2, ybot+0.1] ; AEE - 3/17/04

        ; draw brown (5) line to bufsize limit for entire plot range:
        OPLOT, arrx, FINDGEN(N_ELEMENTS(arrx))*0.0+ $
               ;(ytop-ybot)*(bufsize/truesize)+ybot-0.2, PSYM=3, COLOR=5
               (ytop-ybot)*(bufsize/truesize)+ybot, PSYM=3, COLOR=5

        OPLOT, arrx, (ybot + FLTARR(nelem)) , COLOR=2 ; AEE - 3/17/04 plot all in Green (show overflow in red)
        ;ofw= WHERE(save_buffer GE bufsize, ofcnt)
        ofw= WHERE(save_buffer GE truesize, ofcnt)
        IF (ofcnt GT 0) THEN OPLOT, arrx(ofw),FLTARR(ofcnt)+ytop-0.2,color=1 ;mark overflow area, if any, with red

        ;** Update buffer statistics in schedule widget
        ;**
        date_str = UTC2STR(TAI2UTC(startdis), /ECS, /TRUNCATE) + ' to ' + $
                   UTC2STR(TAI2UTC(enddis), /ECS, /TRUNCATE)
        ;WIDGET_CONTROL, schedv.usage_dates, SET_VALUE="Data/Telemetry Loads For:  "+date_str
        WIDGET_CONTROL, schedv.usage_dates, SET_VALUE= date_str

        ;** calculate total bits available for SCI downlink over plotting range

        bits_avail = rate * N_ELEMENTS(plot_range)

        total_bits_avail = TOTAL(bits_avail)


;       Note: AEE - Dec 23, 02:
;       total_bits_avail is total number of telemetry bits that can be downloaded within the displayed
;       plot-range. So, for a one-hour plot, if telemetry rate is at 42000 bits per seconds, then the
;       total_bits_avail = 3601 * 42000 = 1.5124200e+08 bits (3601 seconds within the hour).
;       Also, usage is the total size of image(s) scheduled within the displayed plot-ragne. So, if
;       there is one image and its size is 8.58931e+07 bits (after all processing, etc), then the
;       usage = 8.58931e+07 bits.

        usage = arry(*,plot_range) ; AEE - Dec 16, 02, use seconds for Secchi.

        sw_usage= usage
        sw_total_bits_avail= total_bits_avail
        IF (schedv.pct_stats EQ 1) THEN BEGIN 
          pct_total = DOUBLE(TOTAL(usage))/8000000.0 ; bits to MB
          pct_each =  DOUBLE(TOTAL(usage,2))/8000000.0 ; bits to MB
          WIDGET_CONTROL, schedv.sw_tot, SET_VALUE= STRING(pct_total,'(f12.3)') +' '
          WIDGET_CONTROL, schedv.sw_eu, SET_VALUE= STRING(pct_each(0),'(f13.3)')+' '
          WIDGET_CONTROL, schedv.sw_c1, SET_VALUE= STRING(pct_each(1),'(f13.3)')+' '
          WIDGET_CONTROL, schedv.sw_c2, SET_VALUE= STRING(pct_each(2),'(f11.3)')+' '
          WIDGET_CONTROL, schedv.sw_h1, SET_VALUE= STRING(pct_each(3),'(f12.3)')+' '
          WIDGET_CONTROL, schedv.sw_h2, SET_VALUE= STRING(pct_each(4),'(f11.3)')+' '
          ;goto, skipdata
       ENDIF ELSE BEGIN
        ;** usage is in bits used per telescope over entire plot range - convert to % of buffer used

        pct_total = (DOUBLE(TOTAL(usage)) / total_bits_avail) * 100
        pct_each = (DOUBLE(TOTAL(usage,2)) / total_bits_avail) * 100
        ;stats_str = STRING(pct_total,'(f12.3)')  +'%'+ $
        ;            STRING(pct_each(0),'(f13.3)')+'%'+ $
        ;            STRING(pct_each(1),'(f9.3)') +'%'+ $
        ;            STRING(pct_each(2),'(f11.3)')+'%'+ $
        ;            STRING(pct_each(3),'(f12.3)')+'%'+ $
        ;            STRING(pct_each(4),'(f11.3)')+'%'
        ;WIDGET_CONTROL, schedv.sw_stats, SET_VALUE='SWch '+stats_str
        WIDGET_CONTROL, schedv.sw_tot, SET_VALUE= STRING(pct_total,'(f12.3)') +'%'
        WIDGET_CONTROL, schedv.sw_eu, SET_VALUE= STRING(pct_each(0),'(f13.3)')+'%'
        WIDGET_CONTROL, schedv.sw_c1, SET_VALUE= STRING(pct_each(1),'(f13.3)')+'%'
        WIDGET_CONTROL, schedv.sw_c2, SET_VALUE= STRING(pct_each(2),'(f11.3)')+'%'
        WIDGET_CONTROL, schedv.sw_h1, SET_VALUE= STRING(pct_each(3),'(f12.3)')+'%'
        WIDGET_CONTROL, schedv.sw_h2, SET_VALUE= STRING(pct_each(4),'(f11.3)')+'%'
     ENDELSE
;skipdata:

  RETURN
END
