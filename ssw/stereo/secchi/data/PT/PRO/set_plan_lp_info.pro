;+
;$Id: set_plan_lp_info.pro,v 1.4 2005/01/24 17:56:36 esfand Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : SET_PLAN_LP_INFO
;
; Purpose     : Set details for the given SEB Program.
;
; Explanation : This routine will set the details (tele,table, etc)
;               for the current SEB Program being scheduled (plan_lp)
;               in the LP_..._SHARE common block from the input variables.
;
; Use         : SET_PLAN_LP_INFO, lp, tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images,
;                     ex_table, inccd, inip, inocc_blocks,inroi_blocks, sub, start, cadence, fps
;
; Inputs      : lp, tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, ex_table,
;               inccd, inip, inocc_blocks,inroi_blocks, sub, start, cadence, fps
;       	These parameters are used to set the details of the SEB program.
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Restrictions: None.
;
; Side effects: None.
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : Based on SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 07/13/04 - Using 4 ROI tables (they can't be editted so no
;                                       need to re-assign them. Also correct header comments. 
;              Ed Esfandiari 09/30/04 - Removed sync.
;
; $Log: set_plan_lp_info.pro,v $
; Revision 1.4  2005/01/24 17:56:36  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:48  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO SET_PLAN_LP_INFO, lp, tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, ex_table, $
                      inccd, inip, inocc_blocks,inroi_blocks, sub, start, cadence, fps

COMMON OS_INIT_SHARE
COMMON OS_SHARE
COMMON OS_ALL_SHARE
COMMON LP_NORMAL_IMAGE_SHARE
COMMON LP_DARK_IMAGE_SHARE
COMMON LP_SEQ_PW_FW_SHARE
COMMON LP_HI_SEQ_SHARE
COMMON LP_CAL_LAMP_SHARE
COMMON LP_CONT_IMAGE_SHARE ; AEE - 05/19/03
COMMON LP_DOUBLE_IMAGE_SHARE, lpdoubv ; AEE - 10/01/03

;** depending on lp this routine sets the rest of the above
;** variables in the correct COMMON LP_...

caller = {caller, id:0L}

ccd(tele,camtable) = inccd(tele,camtable)
ip(iptable) = inip(iptable)

;indm = WHERE(ip(iptable).steps EQ 27)
;IF (indm(0) LT 0) THEN occ_blocks(tele) = inocc_blocks(tele)
;indm = WHERE(ip(iptable).steps EQ 25)
;IF (indm(0) LT 0) THEN roi_blocks(tele) = inroi_blocks(tele)

;occ_blocks = inocc_blocks
;roi_blocks = inroi_blocks

CASE (lp(0)) OF ; AEE 10/6/03  added to handle HI-Seq when both HI1 and HI2 are selected.

   0 : BEGIN                 ;** Double Sky Image LP  - AEE 9/30/03
      IF ((SIZE(lpdoubv))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller)
         WIDGET_CONTROL, lpdoubv.base, /DESTROY
         plan_lp = -2  ; AEE 9/30/03
      ENDIF
      lpdoubv.tele = tele
      lpdoubv.exptable = exptable
      lpdoubv.camtable = camtable
      lpdoubv.fps = fps
      lpdoubv.iptable = iptable
      lpdoubv.fw = fw
      lpdoubv.pw = pw(0) 
      lpdoubv.pw2= pw(1)
      ex(tele,exptable,fw,pw(0)) = ex_table(tele,exptable,fw,pw(0))
      ex(tele,exptable,fw,pw(1)) = ex_table(tele,exptable,fw,pw(1))
   END

   1 : BEGIN                 ;** Normal Image LP
      IF ((SIZE(lpnormv))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         ;tmp = CALL_FUNCTION( osv.op_pro(lp-1), caller)
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpnormv.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03 
      ENDIF
      lpnormv.tele = tele
      lpnormv.exptable = exptable
      lpnormv.camtable = camtable
      lpnormv.fps = fps
      lpnormv.iptable = iptable
      lpnormv.fw = fw
      lpnormv.pw = pw(0)
      ex(tele,exptable,fw,pw(0)) = ex_table(tele,exptable,fw,pw(0))
   END

   2 : BEGIN                 ;** Dark Image LP
      IF ((SIZE(lpdarkv))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         ;tmp = CALL_FUNCTION( osv.op_pro(lp-1), caller)
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpdarkv.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03
      ENDIF
      lpdarkv.tele = tele
      lpdarkv.camtable = camtable
      lpdarkv.fps = fps
      lpdarkv.iptable = iptable
      lpdarkv.expt = ex_table(tele,exptable,0,0)
   END

   3 : BEGIN                 ;** Cal Lamp LP
      IF ((SIZE(lpcalv))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpcalv.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03
      ENDIF
      lpcalv.tele = tele
      lpcalv.camtable = camtable
      lpcalv.fps = fps
      lpcalv.iptable = iptable
      lpcalv.fw = fw
      lpcalv.pw = pw(0)
      lpcalv.lamp = lamp
      lpcalv.pulse= ex_table(tele,exptable,fw,pw(0)) ;AEE 1/14/04 - exptable for dark, led, and cont. is always 0. 
   END

   4 : BEGIN                 ;** Continous Image LP
      IF ((SIZE(lpcont))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpcont.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03
      ENDIF
      lpcont.tele = tele
      lpcont.camtable = camtable
      lpcont.fps = fps
      lpcont.iptable = iptable
      lpcont.fw = fw
      lpcont.pw = pw(0)
      ex(tele,exptable,fw,pw(0)) = ex_table(tele,exptable,fw,pw(0)) ; AEE 1/14/04 - exptable is always 0 and ex is
                                                                    ;               never used.
   END

   5 : BEGIN                 ;** Seq PW FW LP
      IF ((SIZE(lpseqpwv))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpseqpwv.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03
      ENDIF
      lpseqpwv.tele = tele
      lpseqpwv.exptable = exptable
      lpseqpwv.camtable = camtable
      lpseqpwv.fps = fps
      lpseqpwv.iptable = iptable
      lpseqpwv.fw = fw
      lpseqpwv.pw = pw
      lpseqpwv.cadence= cadence
      lpseqpwv.num = lp_num_images
      ex(tele,exptable,fw,*) = ex_table(tele,exptable,fw,*)
   END

   6 : BEGIN                 ;** HI Seq
      IF ((SIZE(lpseqhi))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         tmp = CALL_FUNCTION( osv.op_pro(lp(0)), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpseqhi.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03
      ENDIF

      lpseqhi.fps = fps(0)
      FOR t= 0, N_ELEMENTS(tele)-1 DO BEGIN
        CASE (tele(t)) OF
          3: BEGIN
             lpseqhi.hi1_exptable= exptable(t)
             lpseqhi.hi1_camtable= camtable(t)
             lpseqhi.hi1_iptable = iptable(t)
             lpseqhi.hi1_images= lp_num_images(t)
             lpseqhi.hi1_cadence= cadence(t)
           END
          4: BEGIN
             lpseqhi.hi2_exptable= exptable(t)
             lpseqhi.hi2_camtable= camtable(t)
             lpseqhi.hi2_iptable = iptable(t)
             lpseqhi.hi2_images= lp_num_images(t)
             lpseqhi.hi2_cadence= cadence(t)
           END
        ENDCASE 
      ENDFOR

      ex= ex_table

      sz= SIZE(ex_table)

      IF (sz(0) EQ 5) THEN BEGIN ; both HI1 and HI2 in the same seq (otherwise sz(0) = 4):
        ex(3,*,*,*,0)= ex_table(3,*,*,*,0)   ; HI1 exp. values
        ex(4,*,*,*,0)= ex_table(4,*,*,*,1)   ; Hi2 exp. values
        ex= ex(*,*,*,*,0)                    ; change from sz(0) = 5 to sz(0) = 4
        ;occ_blocks(3,*,0)= occ_blocks(3,*,0) ; HI1 occ. blocks
        ;occ_blocks(4,*,0)= occ_blocks(4,*,1) ; HI2 occ. blocks
        ;occ_blocks= occ_blocks(*,*,0)
        ;roi_blocks(3,*,0)= roi_blocks(3,*,0) ; HI1 roi. blocks
        ;roi_blocks(4,*,0)= roi_blocks(4,*,1) ; HI2 roi. blocks
        ;roi_blocks= roi_blocks(*,*,0)
      ENDIF

   END

   7 : BEGIN                 ;** Block Seq
      IF ((SIZE(lpcmdv))(1) EQ 0) THEN BEGIN             ;** not defined yet, call routine
         tmp = CALL_FUNCTION( osv.op_pro(lp), caller) ; AEE 9/30/03
         WIDGET_CONTROL, lpcmdv.base, /DESTROY
         plan_lp = -2 ; AEE 9/30/03
      ENDIF
      lpcmdv.start = start
      lpcmdv.stop = lp_num_images
   END

   ELSE : BEGIN 
      WIDGET_CONTROL, osv.info_text, SET_VALUE='No LP currently being scheduled.'
      RETURN
   END

ENDCASE

RETURN

END
