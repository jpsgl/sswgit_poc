;+
;$Id: cal_conflict.pro,v 1.3 2005/01/24 17:56:32 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : CAL_CONFLICT 
;
; Purpose     : Check to see if a cal LED OS instance to be scheduled causes any conflicts with 
;               other OS times already scheduled.
;
; Use         : result = CAL_CONFLICT(os_arr,os,camera,conf_info) 
;
; Inputs      : os_arr  An array of all of already scheduled OSs (assumed to be conflict free). 
;                       If not defined (i.e. nothing is currently scheduled), does not do anything.
;               os      The new os to be added to os_arr is checked for conflicts.
;               camera  camera number of new OS.
;
; Opt. Inputs : None.
;
; Outputs     : conf_info  A string array of found conflicts. For each conflict, the os_num 
;                          causing the conflict plus its start and end dates is added to the array. 
;               result     This function returns 0 (for no conflicts) or 1 (for one or more conflicts).
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Calls       : None
;
; Restrictions: This routine assumes all of the existing OSs in os_arr are conflict free. It
;               only checks for possible conflicts of the new os and those in the os_arr.
;
;
; Category    : Planning, Scheduling.
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/22/04 - Used only pre_proc_times to check all OSes (not only LEDs)
;                                       for conflicts.
;              Ed Esfandiari 10/20/04 - Added S/C A B dependency.
;              Ed Esfandiari 12/06/04 - IDL's STRING function can only handle data array of up
;                                       to 1024 elements when using explicit formatting. Replaced
;                                       some STRING calls with my INT2STR function, instead.
;
; $Log: cal_conflict.pro,v $
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:58  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

FUNCTION CAL_CONFLICT,tos_arr,tos_arr_cameras,os,os_camera,conf_info


; Each element of os_arr is a struture of this form:
;   OS_NUM          LONG
;   OS_START        DOUBLE 
;   OS_STOP         DOUBLE
;   OS_SIZE         LONG
;   OS_DURATION     DOUBLE

    conf= 0       ; => no conflict
    conf_info=''

    os_arr= -1
    os_arr_cameras= -1
    IF (DATATYPE(tos_arr) EQ 'STC') THEN BEGIN
      IF (os.sc EQ 'AB') THEN BEGIN
        os_arr= tos_arr
        os_arr_cameras= tos_arr_cameras 
      ENDIF ELSE BEGIN
        ind= WHERE(tos_arr.sc EQ os.sc OR tos_arr.sc EQ 'AB', icnt) 
        IF (icnt GT 0) THEN BEGIN
          os_arr= tos_arr(ind)
          os_arr_cameras= tos_arr_cameras(ind)
        ENDIF
      ENDELSE
    ENDIF

    IF (DATATYPE(os_arr) NE 'STC') THEN RETURN,conf

    starts=os_arr.os_start
    stops=os_arr.os_stop
    ccd_readouts= os_arr.os_ro_time   ; AEE 8/11/03
    pre_procs= os_arr.os_pre_proc_time ; AEE 8/11/03
    os_nums=os_arr.os_num 
    sc= os_arr.sc
    os_cnt= n_elements(starts)
    srt_ind=SORT(starts)
    os_nums= os_nums(srt_ind)
    sc= sc(srt_ind)
    starts= starts(srt_ind)
    stops= stops(srt_ind)
    ccd_readouts= ccd_readouts(srt_ind)  ; AEE 8/11/03
    pre_procs= pre_procs(srt_ind)  ; AEE 8/11/03
    os_arr_cameras= os_arr_cameras(srt_ind)
   
    ; Note: I'm using the pre_proc_time plus the ccd readout time for cal. lamp conflict overlaps. 
    ;       This may need to be changed. (removed processing time from it).
    ;
    ; No - only use exptime periods but since setup time is very small, just use the pre_proc_time
    ;      (setup+exptime).

    snum_conf= 0

    ;sstops= starts+pre_procs+ccd_readouts ; AEE - 8/11/03 - don't include processing
    sstops= starts+pre_procs 
    sstarts= starts 
    rstops= stops ; use the real starts and stops for error display - AEE 8/12/03
    ;sos_nums= "OS_"+STRING(os_nums,'(I4.4)')
    sos_nums= "OS_"+INT2STR(os_nums,4)
    sos_nums= sc+" "+sos_nums

    loc= WHERE(sstops GE os.os_start AND $  ; AEE 8/11/03 ; don't include processing time
               sstarts LE (os.os_start+os.os_pre_proc_time), snum_conf)
               ;sstarts LE (os.os_start+os.os_pre_proc_time+os.os_ro_time), snum_conf)
    ;PRINT,'Number of OS conflicts= ', snum_conf

    IF (snum_conf GT 0) THEN BEGIN
      conf= 1
      FOR i= 0, snum_conf - 1 DO conf_info=[conf_info,sos_nums(loc(i))+" scheduled between "+ $
                                  TAI2UTC(sstarts(loc(i)),/ECS)+" and "+TAI2UTC(rstops(loc(i)),/ECS)] 
    END 

    num_conf= snum_conf

  IF (num_conf GT 0) THEN conf_info= conf_info(1:*)

  RETURN,conf  ; 1 = conflict is true.  0 = conflict is false.

END
