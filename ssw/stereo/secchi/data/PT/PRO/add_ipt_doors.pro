;+
;$Id: add_ipt_doors.pro,v 1.3 2005/01/24 17:56:32 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ADD_IPT_DOORS
;               
; Purpose     : Adds a door section in .IPT file to the EUVI, COR1, or COR2 door variables 
;               in common block.
;               
; Use         : ADD_IPT_DOORS, key_val_arr 
;    
; Inputs      : key_val_arr contains the telescope number for which to schedule a door command. 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/12/04 - Added S/C IDs for door commmands.
;
; $Log: add_ipt_doors.pro,v $
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:57  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION GET_SDATES, str_arr

  nsched = STRTRIM(str_arr(2).val,2)
  sdates = STRTRIM(str_arr(3).val,2)
  sdates = STRMID(sdates,1,STRLEN(sdates)-2) ; remove '(' and ')' from the ends.
  sdates = STR_SEP(sdates,',')
  ddates= DBLARR(nsched)
  FOR i= 0, nsched-1 DO BEGIN
    sdate= sdates(i)
    ; form a date format of 'yyyy/mm/dd hh:mm:dd':
    sdate= STRMID(sdate,0,4)+'/'+STRMID(sdate,4,2)+'/'+STRMID(sdate,6,2)+' '+ $
           STRMID(sdate,9,2)+':'+STRMID(sdate,11,2)+':'+STRMID(sdate,13,2)
    ddates(i)= UTC2TAI(STR2UTC(sdate)) 
  ENDFOR

  RETURN, ddates
END

 

PRO ADD_IPT_DOORS,str_arr

COMMON SCIP_DOORS_SHARE 

   dates= GET_SDATES(str_arr)

   ; Get S/C IDs and apply them to door_close only (since door_close and door_open
   ; have exact same S/C Ids, repeated):

   sc= STRTRIM(str_arr(5).val,2)
   sc= STRMID(sc,1,STRLEN(sc)-2) ; remove '(' and ')' from the ends
   sc= STR2ARR(sc)

   IF (str_arr(1).val EQ '8') THEN BEGIN  ; door close (lp = 8)

     CASE (str_arr(4).val) OF
       
       '0' : BEGIN ; EUVI
               IF (DATATYPE(cdoor_euvi) NE 'DOU') THEN BEGIN 
                 cdoor_euvi= dates                     
                 sc_euvi= sc
               ENDIF ELSE BEGIN 
                 cdoor_euvi= [cdoor_euvi,dates]
                 sc_euvi= [sc_euvi,sc]
               ENDELSE
           END

       '1' : BEGIN ; COR1
              IF (DATATYPE(cdoor_cor1) NE 'DOU') THEN BEGIN 
                 cdoor_cor1= dates                  
                 sc_cor1= sc
               ENDIF ELSE BEGIN
                 cdoor_cor1= [cdoor_cor1,dates]
                 sc_cor1= [sc_cor1,sc]
               ENDELSE
           END

       '2' : BEGIN ; COR2
              IF (DATATYPE(cdoor_cor2) NE 'DOU') THEN BEGIN
                 cdoor_cor2= dates                   
                 sc_cor2= sc
              ENDIF ELSE BEGIN 
                 cdoor_cor2= [cdoor_cor2,dates]
                 sc_cor2= [sc_cor2,sc]
              ENDELSE
           END

     ENDCASE

   ENDIF ELSE BEGIN ; door open (lp = 9)

     CASE (str_arr(4).val) OF
       
       '0' : BEGIN ; EUVI
               IF (DATATYPE(odoor_euvi) NE 'DOU') THEN $
                 odoor_euvi= dates                     $
               ELSE                                    $
                 odoor_euvi= [odoor_euvi,dates]
           END

       '1' : BEGIN ; COR1
              IF (DATATYPE(odoor_cor1) NE 'DOU') THEN $
                 odoor_cor1= dates                    $
               ELSE                                   $
                 odoor_cor1= [odoor_cor1,dates]
           END

       '2' : BEGIN ; COR2
              IF (DATATYPE(odoor_cor2) NE 'DOU') THEN $
                 odoor_cor2= dates                    $
               ELSE                                   $
                 odoor_cor2= [odoor_cor2,dates]
           END

     ENDCASE

   ENDELSE

  RETURN
END
