;+
;$Id: ipt_dates2ecs.pro,v 1.1.1.2 2004/07/01 21:19:03 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : IPT_DATES2ECS
;               
; Purpose     : This function coverts .IPT dates formats to ECS format.
;               
; Use         : ecs = IPT_DATES2ECS(dates) 
;    
; Inputs      : dates   String array of 'yyyymmdd hhmmss' dates. 
;
; Opt. Inputs : None
;               
; Outputs     : ecs     String array of ECS dates ('yyyy/mm/dd hh:mm:ss').
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: ipt_dates2ecs.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:03  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

FUNCTION IPT_DATES2ECS, dates

   ; dates:     array of dates of format 'yyyymmdd hhmmss'
   ; ecs_dates: array of dates of format 'yyyy/mm/dd hh:mm:ss'

   ecs_dates= STRMID(dates,0,4)+'/'+STRMID(dates,4,2)+'/'+STRMID(dates,6,2)+' '+ $
              STRMID(dates,9,2)+':'+STRMID(dates,11,2)+':'+STRMID(dates,13,2)

   RETURN, ecs_dates 
END

