;+
;$Id: schedule_call_os.pro,v 1.4 2005/01/24 17:56:35 esfand Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : SCHEDULE_CALL_OS
;
; Purpose     : Routine to interface SECCHI Planning and Scheduling Tools.
;
; Explanation : This routine is the interface between the Scheduling Tool 
;		(SCHEDULE) and the Planning tool (DEFINE_OS).  It brings
;		up the details for a particular OS.
;		It looks up os_num in the defined_os_arr and sets the appropriate 
;		variables for the os definition tool.
;
; Use         : SCHEDULE_CALL_OS, os_num
;
; Inputs      : os_num		The unique number (LONG) that identifies an OS.
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Restrictions: None.
;
; Side effects: Starts DEFINE_OS if needed.
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/30/04 - Removed sync. 
;
; $Log: schedule_call_os.pro,v $
; Revision 1.4  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:09  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

 
;__________________________________________________________________________________________________________
;

;** THIS ROUTINE LOOKS UP os_num IN THE defined_os_arr
;** AND SETS THE APPROPRIATE VARIABLES FOR THE OS DEFINITION TOOL.

PRO SCHEDULE_CALL_OS, os_num

COMMON OS_SHARE, osv   ; AEE 2/17/04
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE
COMMON OS_DEFINED

   ;** IF ANY LP DEFINITION TOOLS ARE UP GET RID OF THEM
   plan_lp = -2 ; AEE 9/30/03 - used plan_lp=0 for Double Normal Images. 
 
   IF (DATATYPE(defined_os_arr) NE 'STC') THEN RETURN   ; AEE 2/17/04

   ind = WHERE(defined_os_arr.os_num EQ os_num)
   IF (ind(0) EQ -1) THEN RETURN
   lp = defined_os_arr(ind).lp
   dos= defined_os_arr(ind)

   ;** SET VARS FOR GIVEN LP TYPE

   SET_PLAN_LP_INFO, lp, dos.tele, dos.exptable, dos.camtable, dos.iptable, dos.fw, dos.pw,             $
                     dos.lamp, dos.num_images, dos.ex, dos.ccd, dos.ip, dos.occ_blocks, dos.roi_blocks, $
                     dos.sub, dos.start, dos.cadence, dos.fps

   lp = lp(0) ; HI seq is an array of two (but same lp).

   ;** GENERATE AN EVENT FOR THE GIVEN LP TO USE VARS SET IN SET_PLAN_LP_INFO.PRO
   event = {event, id:0L, top:0L, handler:0L}
   event.id = osv.op_buttons(lp) ; AEE 10/01/03
   event.top = osv.base
   event.handler = osv.op_buttons(lp) ; AEE 10/01/03
   WIDGET_CONTROL, event.id, SEND_EVENT=event, /NO_COPY, SHOW=1

   RETURN

END
