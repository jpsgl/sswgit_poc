;+
;$Id: init_timeline.pro,v 1.2 2005/05/26 20:00:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : INIT_TIMELINE
;               
; Purpose     : This function uses widgets to prompt the user for entering
;               a date for adding or removing initialize timeline commands.
;
; Explanation : The start and stop times  of the scheduled are displayed
;               The user entered date/response is then returned.
;               
; Use         : init_date= INIT_TIMELINE(start_dte, stop_dte) 
;    
; Inputs      : 
;               start_dte  Start time (UTC). 
;               stop_dte   Stop time (UTC).
;               sc         0=SC-AB, 1=SC-A, 2=SC-B
;
; Opt. Inputs : caller     Structure containing id of caller. 
;               
; Outputs     : init_date    String - date/time :  to add the init timeline command.
;                                     remove:      to remove init timeline command. 
;                                     blank:       to cancel the command.
;
; Opt. Outputs: None 
;
; Keywords    : None
;
; Prev. Hist. : None 
;
; Written by  : Ed Esfandiari, NRL, Feb 2005 - First Version. 
;               Ed Esfandiari 05/06/05 Modified to have multiple init commands either from bsf
;                                      file or as stand-alone.
;               
; Modification History:
;
; $Log: init_timeline.pro,v $
; Revision 1.2  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.1  2005/03/10 16:55:49  esfand
; first version
;
;
;-


PRO INIT_TIMELINE_EVENT, event

COMMON INIT_TIMELINE_SHARE, return_dte, start_dte
COMMON TMPINIT, spcraft
COMMON INIT_TL, ic

  WIDGET_CONTROL, event.top, GET_UVALUE=itl   ; get structure from UVALUE

    CASE (event.id) OF

	itl.textd : BEGIN	;** exit program
	   WIDGET_CONTROL, itl.textd, GET_VALUE=val
           ;val = UTC2STR(STR2UTC(val), /ECS)
	   WIDGET_CONTROL, itl.textd, SET_VALUE=val
	END

	itl.cancel : BEGIN	;** cancel and exit program
           return_dte = ''
	   WIDGET_CONTROL, /DESTROY, itl.base
	END

	itl.add : BEGIN	;** return date to shift to exit program
	   WIDGET_CONTROL, itl.textd, GET_VALUE=val
           ;val = UTC2TAI(STR2UTC(val))
           return_dte = val(0)
	   WIDGET_CONTROL, /DESTROY, itl.base
	END
  
;        itl.remove: BEGIN
;          return_dte = 'remove'
;          WIDGET_CONTROL, /DESTROY, itl.base
;        END
        itl.remove: BEGIN
          return_dte = 'remove'
          IF (spcraft EQ 0) THEN $
            list= WHERE((ic.sc EQ 'A' OR ic.sc EQ 'B') AND ic.bsf EQ 0, cnt) 
          IF (spcraft EQ 1) THEN $
            list= WHERE(ic.sc EQ 'A' AND ic.bsf EQ 0, cnt)
          IF (spcraft EQ 2) THEN $
            list= WHERE(ic.sc EQ 'B' AND ic.bsf EQ 0, cnt)
  
          IF (cnt GT 0) THEN BEGIN
            dates= TAI2UTC(ic(list(SORT(ic(list).dt))).dt,/ECS)
            IF (spcraft EQ 0) THEN dates= dates+' S/C-'+ic(list(SORT(ic(list).dt))).sc
            result= PICKFILES2(FILES=dates, XSIZE=300, TITLE="Choose Init Timelines to Remove")
            
            IF ( result(0) NE '' ) THEN BEGIN
              FOR i=0, N_ELEMENTS(result)-1 DO BEGIN
                IF (spcraft EQ 0) THEN $
                  rmv= WHERE(TAI2UTC(ic(list).dt,/ECS)+' S/C-'+ic(list).sc EQ result(i)) $
                ELSE $
                  rmv= WHERE(TAI2UTC(ic(list).dt,/ECS) EQ result(i))
                ic(list(rmv(0))).sc='R'
              ENDFOR 
              keep= WHERE(ic.sc NE 'R')
              ;help,ic
              ;print,tai2utc(ic.dt,/ecs)

              ic= ic(keep)

              ;help,ic
              ;print,tai2utc(ic.dt,/ecs)
              ;stop 
            ENDIF
          ENDIF ELSE BEGIN
            return_dte = 'not_removed'
          ENDELSE
          WIDGET_CONTROL, /DESTROY, itl.base
        END

        ELSE : BEGIN
        END

   ENDCASE

END

;__________________________________________________________________________________________________________
;

FUNCTION INIT_TIMELINE, CALLER=caller, beg_dte, end_dte, sc

COMMON INIT_TIMELINE_SHARE, return_dte, start_dte
COMMON TMPINIT, spcraft

start_dte= beg_dte

    IF XRegistered("INIT_TIMELINE") THEN RETURN, -1


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    spcraft= sc

    CASE sc OF
      0: tsc= 'SC-AB'
      1: tsc= 'SC-A'
      2: tsc= 'SC-B'
    ENDCASE

    IF (KEYWORD_SET(caller)) THEN $
       base = WIDGET_BASE(/COLUMN, TITLE='Add/Remove Init Timeline for '+tsc, /FRAME, GROUP_LEADER=caller.id) $
    ELSE $
       base = WIDGET_BASE(/COLUMN, TITLE='Add/Remove Init Timeline for '+tsc, /FRAME)

    row = WIDGET_BASE(base, /COL)
     tmp=  WIDGET_LABEL(row, VALUE='Schedule Start and End Date/Times are:') 
     tmp = WIDGET_LABEL(row, VALUE='Start Time: '+beg_dte)
     tmp = WIDGET_LABEL(row, VALUE='Stop  Time: '+end_dte)
    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='Add Initialize Timeline Command at: ')
     textd = WIDGET_TEXT(row, /EDITABLE, XSIZE=25, VALUE= beg_dte)
    row = WIDGET_BASE(base, /COL)
     tmp= WIDGET_LABEL(row, VALUE='Allow 2 seconds between "Initialize Timeline" command and next command.')
    row = WIDGET_BASE(base, /ROW)
     add = WIDGET_BUTTON(row, VALUE=" Add InitTimeline")
     tmp = WIDGET_LABEL(row, VALUE=' ')
     cancel = WIDGET_BUTTON(row, VALUE=" Cancel InitTimeline")
     tmp = WIDGET_LABEL(row, VALUE=' ')
     remove = WIDGET_BUTTON(row, VALUE="Remove InitTimeline")

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, base, /REALIZE

    itl = CREATE_STRUCT( 'base', base, 		$
                             'add', add, 		$
                             'cancel', cancel, 		$
                             'remove', remove,          $
                             'textd', textd)

   WIDGET_CONTROL, base, SET_UVALUE=itl

   XMANAGER, 'INIT_TIMELINE', base, /MODAL

   RETURN, return_dte

END
