;+
;$Id: image_destination.pro,v 1.2 2006/04/06 15:27:19 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : IMAGE_DESTINATION
;               
; Purpose     : This function figures out the downlink destination(s) for image(s) for each OS.
;               
; Use         : dest= IMAGE_DESTINATION(defined_os_arr) 
;    
; Inputs      : defined_os_arr   All scheduled OSes 
;
; Opt. Inputs : None
;               
; Outputs     : dest             Structure for destination(s) for each OS.  
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;               Ed Esfandiari 01/13/06  - Added steps 37 and 38 checks.
;
; $Log: image_destination.pro,v $
; Revision 1.2  2006/04/06 15:27:19  esfand
; Added steps 37 and 38 checks
;
; Revision 1.1.1.2  2004/07/01 21:19:03  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION IMAGE_DESTINATION, defined_os_arr
 
  ; for each OS, indicate if the image is either droped to the floor (set to 0), or placed in SSR1, 
  ; SSR2, RealTime, SpaceWeather, GroundTest, SSR1RT by setting the index to 1 to 6.
  ;
  ; Examples:
  ;
  ; dest(img_number,[SST1,SSR2,RT,SW,GT,S1RT])
  ;
  ; dest(0,[2,0,0,1,0,0]) means that the images for first OS in the dest will go to the SpaceWeather
  ;                       and SSR1 channels, in that order, for downlink. Both of SW and SSR1 APIDs were 
  ;                       specified via the IP functions in planning Tool and value of 1 assigned to
  ;                       SW means SW APID was choosen first and value of 2 for SSR1 means that SSR
  ;                       APID was choosen second. Also, it means the same image is processed twice
  ;                       and sequentially:
  ;                       1. First, using SW APID a file is created and sent through the RAM-disk buffer
  ;                          to the 1553 bus and SW partition.  
  ;                       2. Second, using SSR1 APID a file is created and sent through the RAM-disk
  ;                          buffer to the 1553 bus and SSR1 partition.
  ;
  ; Or
  ;
  ; dest(1,[14,0,0,1,0,0]) means that the images for second OS in the dest will go to the SpaceWeather
  ;                       and SSR1 channels for downlink. Value of 1 for SW means SW APID was selected
  ;                       from the Planning Tool. Vaule of 14 (0+14) for SSR1 means that SSR1 APID was not
  ;                       selected from the planning tool but via the default mode the same SW image
  ;                       will go to the SSR1 by default (at the S/C side). So, the image is only 
  ;                       processed once (but goes to two different place for downlink) 
  ;                       and a filename using SW APID is created and sent through the RAM-disk buffer
  ;                       to the 1553 bus and SW partition. S/C will also place the same file on SSR1
  ;                       but since PT knows about this, it should generate the right filename using
  ;                       SSR1 APID for the header.
  ;
  ; Or
  ;
  ; dest(2,[16,0,0,1,0,0]) means that the images for third OS in the dest will go to the SpaceWeather
  ;                       and SSR1 channels for downlink. Value of 1 for SW means SW APID was selected
  ;                       from the Planning Tool. Vaule of 16 (2+14) for SSR1 means that SSR1 APID was 
  ;                       also selected from the Planning Tool (its APID appeared after SW APID) and
  ;                       the SW image will also go to SSR1 by default.
  ; Or
  ;
  ; dest(3,[0,0,0,0,0,0]) means that the images for the fourth OS in the os_des will be dropped to
  ;                       the ground (will not be downlinked). 
  ; Or
  ;
  ; dest(4,[0,0,0,0,1,0]) means that the images for the fifth OS in the os_des will go to the Ground
  ;                       Test APID only.
  ; Or
  ;
  ; dest(4,[0,0,0,0,0,1]) means that the images for the fifth OS in the os_des will go to the S1RT APID
  ;                       only.


  ; note: currently, ip functions 1 to 58 for APIDs in cnvrt_ip.dat are (they may be changed?):
  ; note: currently, ip functions 1 to 59 for APIDs in cnvrt_ip.dat are (they may be changed?):
  ; note: currently, ip functions 1 to 60 for APIDs in cnvrt_ip.dat are (they may be changed?):
  ;
  ;       55  Use Real-Time APID
  ;       56  Use Space Weather APID
  ;       57  Use SSR1 APID
  ;       58  Use SSR2 APID 
  ;       59  Use Ground Test APID 
  ;       60  Use SSR1RT APID

  ; 5/3/04 - Note new APID step numbers:

  ;       39  Use SSR1RT APID
  ;       40  Use Space Weather APID
  ;       41  Use SSR1 APID
  ;       42  Use SSR2 APID
  ;       43  Use Ground Test APID


  ; mode can be either BOTH, SPACEWEATHER, REALTIME, or NONE.

  mode= READ_SSR_RT_MODE('SSR_realtime.dat') ; this is also called from schedule.pro and define_os.pro
                                             ; to make sure mode is defined.
  IF(mode EQ '') THEN BEGIN
   PRINT
   PRINT,'==========================================================='
   PRINT,'ERROR: SSR_realtime.dat file does not indicate which one of'
   PRINT,'            BOTH, SPACEWEATHER, REALTIME, or NONE'
   PRINT,'       modes to be used. Edit the file and try again.'
   PRINT,'==========================================================='
   PRINT
   STOP ,'Error in image_destination.pro'
  END

  ; A HI1 or HI2 seq. has two entries but only one is used; Throw away the one not used:
  uniques= WHERE(defined_os_arr.num_images GT 0, nos)

  os_dest= REPLICATE({os_num:0L, tele:0, lp:0, num_images:0, summed:0B, dest:INTARR(6)},nos)

  ; possible dest values are
  ;   0=no-downlink, or 1,6 to downlink SSR1,SSR2,RealTime,SpaceWeather,GroundTest,SSR1RT.
  ; But, for SSR1, possible values are 0 to 27. 

  iptab= defined_os_arr(uniques).iptable

  ; keep order of APIDs for an image marked for more than one downlink. So, the first APID
  ; will get value of k=1, second APID gets k=2, third APID gets k=3, forth APID gets k=4,
  ; fifth APID gets k=5, and last APID gets k=6. If no APID is specified (image is dropped 
  ; to the floor) the place-holders for the all possible 6 APIDs will contain zeros:

  FOR i= 0, nos-1 DO BEGIN
    ; Set summing for LP=6 (HI1 or HI2 sequences) OSes that have summing enabled (we may
    ; also include SCIP seqeunces, LP=5, in the future):

    os_dest(i).os_num= defined_os_arr(uniques(i)).os_num
    os_dest(i).tele = defined_os_arr(uniques(i)).tele
    os_dest(i).lp= defined_os_arr(uniques(i)).lp
    os_dest(i).num_images= defined_os_arr(uniques(i)).num_images
    IF (os_dest(i).lp EQ 6) THEN BEGIN
      summ= WHERE(defined_os_arr(uniques(i)).ip(iptab(i)).steps EQ 34 OR $   ; 34 = Add to HI1 Summing Buffer
                  defined_os_arr(uniques(i)).ip(iptab(i)).steps EQ 37 OR $
                  defined_os_arr(uniques(i)).ip(iptab(i)).steps EQ 38 OR $
                  defined_os_arr(uniques(i)).ip(iptab(i)).steps EQ 36, scnt) ; 36 = Add to HI2 Summing Buffer 

      IF (scnt GT 0) THEN os_dest(i).summed= 1B 
    ENDIF

    ; Also, get the destination APIDs:

    ip_func= defined_os_arr(uniques(i)).ip.steps 
    good= WHERE(ip_func(*,iptab(i)) GE 39 AND ip_func(*,iptab(i)) LE 43, cnt) ; AEE 5/3/04
    IF(cnt GT 6) THEN BEGIN
      PRINT,''
      PRINT,'Warning - More than 6 APIDs (SSR1,SSR2,RT,SW,GT,S1RT) are selected to process this image.'
      PRINT,'          OS_NUM = ',STRING(defined_os_arr(uniques(i)).os_num)
      PRINT,'          TELE   = ',STRING(defined_os_arr(uniques(i)).tele)
      PRINT,''
      STOP ,'Stopped in image_destination.pro' 
    ENDIF

    k=0
    FOR j= 0, cnt-1 DO BEGIN ; keep order of APIDs
      k= k+1
      CASE ip_func(good(j),iptab(i)) OF
         39: os_dest(i).dest(5)= k   ; S1RT APID
         40: os_dest(i).dest(3)= k   ; SW APID
         41: os_dest(i).dest(0)= k   ; SSR1 APID
         42: os_dest(i).dest(1)= k   ; SSR2 APID
         43: os_dest(i).dest(4)= k   ; Ground Test APID
      ENDCASE
    ENDFOR

    ; Now see if images scheduled for downlink on Real-Time and/or Space Weather channels
    ; should also be added for downlink via the SSR1 by default:

      ssr1v= os_dest(i).dest(0)
      CASE (mode) OF
        'BOTH' : BEGIN 
             IF (os_dest(i).dest(2) GT 0 AND os_dest(i).dest(3) GT 0) THEN BEGIN
               ssr1v=  ssr1v + 21  ;=> 21-27 (both RT & SW channels were selected)
               os_dest(i).dest(0)= ssr1v
             ENDIF ELSE BEGIN
               IF (os_dest(i).dest(2) GT 0) THEN ssr1v= ssr1v + 7   ;=>7-13 (only RT channel was selected)
               IF (os_dest(i).dest(3) GT 0) THEN ssr1v= ssr1v +14 ;=> 14-20 (only SW channel was selected)
               os_dest(i).dest(0)= ssr1v
             ENDELSE
           END
        'REALTIME' : BEGIN 
             IF (os_dest(i).dest(2) GT 0) THEN os_dest(i).dest(0)= ssr1v + 7  ;=> 7-13 (RT was selected)
           END
        'SPACEWEATHER' : BEGIN 
             IF (os_dest(i).dest(3) GT 0) THEN os_dest(i).dest(0)= ssr1v + 14 ;=>14-20 (SW was selected)
           END
      ENDCASE

  ENDFOR

  RETURN, os_dest

END
