;+
;$Id: widget_combobox_shell.pro,v 1.1 2005/05/26 20:00:59 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : WIDGET_COMBOBOX_SHELL
;
; Purpose     : widget_combobox was introduced with idl5.6 and is used in PT.
;               Since idl versions before 5.6 can't compile the widget_combobox,
;               I placed it in this shell to it is not complied if PT is used
;               with idl versions older than 5.6.
;
; Explanation : Calls widget_combobox.pro
;
; Use         : id= WIDGET_COMBOBOX_SHELL(base,list,val) 
;
; Inputs      : base     widget base to be used with widget_combobox.
;               list     a list (string array) to be used with widget_combobox VALUE. 
;               val      value to be assigned to SET_COMBOBOX_SELECT.
;
; Opt. Inputs : None
;
; Outputs     : id       widget id of widget_combobox.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, March 2005 - First Version.
;
; Modification History:
;
; $Log: widget_combobox_shell.pro,v $
; Revision 1.1  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
;
;-

FUNCTION WIDGET_COMBOBOX_SHELL, base, list, val 

  id= WIDGET_COMBOBOX(base, VALUE=list)
  WIDGET_CONTROL, id, SET_COMBOBOX_SELECT= val 
 
  RETURN,id

END

