;+
;$Id: mask_runs2arr.pro,v 1.4 2005/05/26 20:00:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : MASK_RUNS2ARR
;               
; Purpose     : Converts CCD block numbers (i.e. 0-10,35,40-45,60-70)
;               to a bytarr(34*34) with input indices set to 1.
;               
; Use         : arr = MASK_RUNS2ARR(data_str, sc, tel) 
;    
; Inputs      : data_str  String of CCD blocks used.
;               sc        String for SpaceCraft: A or B
;               tel       String for telescope: EUVI, COR1, COR2, HI1, or HI2  
;
; Opt. Inputs : None
;               
; Outputs     : arr       Byte arr of full CCD (34*34) with the blocks
;                         used set to 1. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               Ed Esfandiari 05/24/05 - Added ccd orientaion dependancy for masks
;               
; Modification History:
;
; $Log: mask_runs2arr.pro,v $
; Revision 1.4  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.3  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

FUNCTION MASK_RUNS2ARR, data_str, sc, tel

   RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav'

   mask = BYTARR(xyblks*xyblks)

   ;** clip off "(" and ")" and convert to string array
   arr = STR2ARR(STRMID(data_str,1,STRLEN(data_str)-2),DELIM=',')

   IF (arr(0) EQ '-1') THEN BEGIN
      PRINT, '%%MASK_RUNS2ARR: Warning. Empty mask table'
      RETURN, mask
   ENDIF

   FOR i=0, N_ELEMENTS(arr)-1 DO BEGIN
      loc = STRPOS(arr(i), '-')
      IF (loc GE 0) THEN BEGIN		;** entry is 'x1-x2'
         x1 = FIX(STRMID(arr(i),0,loc))
         x2 = FIX(STRMID(arr(i),loc+1,STRLEN(arr(i))-loc)) > x1
         IF ( (x1 GE 0) AND (x1 LE xyblks*xyblks-1) AND $
              (x2 GE 0) AND (x2 LE xyblks*xyblks-1) ) THEN mask(x1:x2) = 1
      ENDIF ELSE BEGIN			;** entry is 'x1'
         x1 = FIX(arr(i))
         IF ( (x1 GE 0) AND (x1 LE xyblks*xyblks-1) ) THEN mask(x1) = 1
      ENDELSE
   ENDFOR

   ; AEE 5/27/04 -  Modify mask from mask_table coordinates to idl:

   ;mask= MODIFY_MASK_ORDER(mask,/TABLE2IDL)
   mask= MODIFY_MASK_ORDER(sc,tel,mask,/TABLE2IDL) 
   RETURN, mask
END
