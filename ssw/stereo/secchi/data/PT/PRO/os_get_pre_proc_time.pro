;+
;$Id: os_get_pre_proc_time.pro,v 1.8 2009/09/11 20:28:14 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_GET_PRE_PROC_TIME
;               
; Purpose     : Calculates pre=processing and CCD readout time of an image.
;               NOTE: these values are hardcoded estimates for now. Will
;                     change in the future.
;               
; Use         : pp_time = OS_GET_PRE_PROC_TIME(os, index, ro_time, setup_time, filter_time, polar_time) 
;    
; Inputs      : os       Observing sequence.
;               index    Index or polarizer-wheel/sector used.
;
; Opt. Inputs : None
;               
; Outputs     : ro_time     CCD readout time for the image (secs).
;               pp_time     Pre processing time of the image (secs). It includes
;                           Setup times for mech. movements and exptime.
;               setup_time  Image setup time (pp_time - exptime) (secs).
;               filter_time Time to change EUVI filter wheel position (secs).
;               polar_time  TIme to change quad-selector/polarization wheel position (secs).
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;               
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/16/04 - Also return image setup time.
;              Ed Esfandiari 07/14/04 - Since EUVI purple LED is now known to 
;                                       be Telescope mounted, use the shutter_time
;                                       (time to open/close shutter) for it. 
;              Ed Esfandiari 07/14/04 - Read mech movement times from a save set.
;              Ed Esfandiari 07/16/04 - Return filter/polar times to the caller for
;                                       use in filter/polar dependent image setup times
;                                       in SCIP sequences. 
;              Ed Esfandiari 08/16/04 - Add 4.8 msec of setup time to CCD readout. 
;                                       Also added 100 msec exposure setup time to each SCIP
;                                       exptime not involving MEB (non-dark or LED SCIP images).
;              Ed Esfandiari 12/13/04 - Added CCD summimg adjustment to exptime for non cal images.
;              Ed Esfandiari 01/10/05 - Added lowgain exptime correction.
;              Ed Esfandiari 02/08/05 - Correct filter time (only used for EUVI, set to 0.0 for others).
;              Ed Esfandiari 11/21/06 - Ignore ccd_sum and low gain exptime correction (FSW does not do it).
;
;
; $Log: os_get_pre_proc_time.pro,v $
; Revision 1.8  2009/09/11 20:28:14  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2005/03/10 16:46:48  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:44  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION OS_GET_PRE_PROC_TIME, os, index, ro_time, setup_time, filter_time, polar_time

   ccd = os.ccd
   tele = os.tele
   exptable = os.exptable ; AEE 1/14/04
   camtable = os.camtable ; AEE 1/14/04
   iptable = os.iptable
   ip = os.ip
   occ_blocks = os.occ_blocks
   roi_blocks = os.roi_blocks

   RESTORE, FILENAME= GETENV('PT')+'/IN/OTHER/'+'mech_times.sav' 
   ; => filter_time,polar_time,shutter_time (all units of sec).

   IF (tele NE 0) THEN filter_time= 0.0  ; Only EUVI has filter.

   ; shutter_time is the time to open or close the shutter. So, for images that require
   ; shutter movement use it as the shutter open/close time.
   ; Also, HI1 and HI2 are shutterless (and their doors open only once and remain open), so set
   ; the shutter_time for HI1 and 2 to zero to use 0 for shutter open/close times. For SCIP 
   ; telescopes shutters are also not opened/closed for LED images that are FPA mounted.
   ; For Dark images we also don't have any shutter movement for any instrument:

   IF (tele GT 2) THEN shutter_time = 0  ; No shutter open/close for any type of HI image. 
   IF (os.lp EQ 2) THEN shutter_time = 0  ; No shutter open/close for Darks
   IF (os.lp EQ 3) THEN BEGIN ; no shutter open/close for SCIP FPA mounted LED images.
     IF (tele EQ 1) THEN shutter_time = 0  ; No shutter open/close for COR1 LED images.
     IF (tele EQ 2 AND os.lamp GT 0) THEN shutter_time = 0  ; No shutter open/close for COR2 Blu&Pur LED images
                                                          ; because they are FPA mounted).
     IF (tele EQ 0 AND os.lamp EQ 0) THEN shutter_time = 0  ; No shutter open/close for EUVI first Blue LED image.
   ENDIF
 
   ; Now shutter_time contains a valid number to be used for each shutter open or
   ; close. For images that no shutter movement is needed, shutter_time is now zero. 
    

   ;** get exptime

   ; For non cal images, correct exptime for CCD summing:

   ccdsum= 1
   IF( os.lp NE 2 AND os.lp NE 3) THEN BEGIN 
     ccdsum= ((ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1))
     ; for low gain (gain mode = 1) magnify exptime 4 times):
     IF (ccd(tele,camtable).gmode EQ 1) THEN ccdsum= ccdsum/4.0 ; for low gain, amplify exptime by 4
   ENDIF

   ccdsum=1 ; Ignore ccd_sum and low gain exptime correction (FSW does not do it).

   exptime= os.ex(tele, exptable, os.fw, os.pw(index)) / ccdsum 

   IF (os.lp EQ 3) THEN BEGIN ; AEE - 11/18/03 (from Dennis's Image-Taking Operations Discussion Notes)

     ; For cal LED images, exptime at this point is in units of pulse. Change
     ; them to ms. Pulse counts > 4095 require multiple LED pulse commands with
     ; 6 ms intervals between them. There are 50K pulses per second (50 pulses per milliseconds).

     exptime = PULSE2MS(LONG(exptime)) ; AEE 1/27/04 moved the code above to a function.

   ENDIF

   IF (os.lp EQ 0) THEN BEGIN
     ; Double Images:
     exptime= exptime + os.ex(tele, exptable, os.fw, os.pw(1)) / ccdsum
   ENDIF

   ; SCIP images not invloving MEB (non-dark images), require an additional of 100 msec of wait for
   ; commanding and status in addition to the commanded exposure time that needs to be added to the
   ; exptime. Note that 100 should be added only to display the image bars on the plot and duration
   ; and its components display in schedule_event and NOT in .IPT file (and hence upload files) or
   ; summary files because this is a wait that occurs for each exposure in addition to the commanded
   ; exp. time:
   ; LED images also don't require the 100 msec.

   ;IF (tele LT 3 AND os.lp NE 2) THEN BEGIN ; SCIP tele, non-dark image
   IF (tele LT 3 AND os.lp NE 2 AND os.lp NE 3) THEN BEGIN ; SCIP tele, non-dark, non-LED image
       exptime= exptime + 100.0
       IF (os.lp EQ 0) THEN exptime= exptime + 100.0 ; add another 100 msec for double images. 
   ENDIF

   exptime = exptime/1000.0

   IF (os.lp EQ 4) THEN exptime = 0.0   ; Assume cont. image exptime is zero. Note: this means
                                        ; the 100 msec exp.time wait from above is not applied
                                        ; to cont. SCIP images.
   
; ****
; AEE 9/23/03
; the move mech needs to be corrected. For now, we use just 1 sec anyway (see the end of this routine).
; ****

   ;** Move a mech

   tme = 0.0D

   ; Note: There is no Filter Wheel movement for CO1, COR2, HI1, and HI2. There is also no
   ; Polarizer wheel for HI1 and HI2.  In addition, there is no filter/polar movement for any
   ; dark image. So, for these don't add filter and polar movement times:

   ; Add sector/polar movement time if needed:

   ;polar_time= 1.87 ; ?? Also, same value if originally at the requested position or moved many positions?

   IF (os.lp NE 2 AND tele LE 2) THEN BEGIN ; None dark SCIP images
     tme = tme + polar_time ; only SCIP has sector/polar wheel
     If (os.lp EQ 0 AND os.pw(0) NE os.pw(1)) THEN $
       tme = tme + polar_time ; AEE 9/23/03 - for Double exposure SCIP Images, polar may move twice
   ENDIF

   ; Add filter movement time if needed:

   ;filter_time= 1.5 ; ?? Also, same value if originally at the requested position or moved many positions?

   IF (os.lp NE 2 AND tele EQ 0) THEN tme = tme + filter_time  ; only none DARK EUVI images use filters.

   ;** set up complete

   ;** Clear CCD:

   ;clear_time= 1.0

   clear_time = ccd(tele,camtable).cl_time
   tme = tme + clear_time

   ;** Open Shutter if needed (shutter_time=0 if it is not needed):

   tme = tme + shutter_time 

   IF (os.lp EQ 0) THEN tme = tme + shutter_time ; AEE 9/23/03 - for Double Images, shutter is opened twice.

   ;** expose

   tme = tme + exptime

   ;** Close Shutter if needed (shutter_time=0 if it is not needed):

   ; Note: for cont readout, shutter is closed after CCD readout so it is not part of the setup time, AEE-5/19/03:

   IF (os.lp NE 4) then tme = tme + shutter_time

   IF (os.lp EQ 0) THEN $
     tme = tme + shutter_time ; AEE 9/23/03 - for Double Images, shutter is closed twice b4 CCD readout).

   ;** read out ccd

   ;o_time= ccd(tele,camtable).ro_time
  
   ; All ccd readouts involve an additinal 4.8 msec to setup:

   ro_time= ccd(tele,camtable).ro_time + (4.8/1000.0)


   setup_time= tme - exptime

   RETURN, tme

END
