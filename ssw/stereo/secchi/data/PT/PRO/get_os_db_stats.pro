;+
;$Id: get_os_db_stats.pro,v 1.13 2011/06/30 20:41:39 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GET_OS_DB_STATS
;               
; Purpose     : Get the size, and duration info for an image. 
;               
; Use         : GET_OS_DB_STATS, os, os_size, os_duration, pre_proc_time,ro_time, proc_time, 
;                                setup_time, cadence, DOORCLOSED=doorclosed
;    
; Inputs      : os       Observation structure for the image.
;
; Opt. Inputs : None
;               
; Outputs     : os_size        Size of the image (bits). 
;               os_duration    Duration to take the image (secs).
;               pre_proc_time  Pre processing time (secs).
;               ro_time        CCD readout time (secs).
;               proc_time      Processing time (secs).
;               cadence        cadences (secs) used between images in a sequence.
;                              0.0 = as close together as possible.
;               setup_time     Image setup time (pre_proc_time - exptime) (secs).
;
; Opt. Outputs: None
;
; Keywords    : DOORCLOSED     Set if door is closed for this image. 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/09/04 - Added proc_time for output.
;              Ed Esfandiari 06/14/04 - added cadence parameter use it for duration.
;              Ed Esfandiari 06/16/04 - added setup_time for output.
;              Ed Esfandiari 07/17/04 - use filter/polar dependent setup times for
;                                       SCIP seq images.
;              Ed Esfandiari 01/28/05 - Added summing buffer comments and the way the
;                                       proc_time is calculated. 
;                                       also Added code to calculate image processing times for HI summed 
;                                       seq images using real table steps for first, middle, and last images.
;              Ed Esfandiari 10/24/05 - Added code to use lesser of calculated image
;                                       size and icer_size for images that use an
;                                       icer compression.

;              Ed Esfandiari 11/07/05 - Added code to report the actual cadences used for SCIP and
;                                       HI seqeueces in PT, instead of 0.0, in PT outputs including
;                                       the upload files.
;              Ed Esfandiari 11/09/05 - icer functions changed to steps #90 to 101 (from 82-93).
;              Ed Esfandiari 06/22/06 - use greater size of calculated image size vs icer_size if difference less 2KB.
;              Ed Esfandiari 11/29/06 - Corrected candence error messages.
;              Ed Esfandiari 02/08/07 - Added 500 bytes for hdr&trailer and 12 bytes per packet to each image size.
;                                       Also use 2% more for each ICER byte quota. 
;              Ed Esfandiari 02/09/07 - Don't use exposure time of last image in a SCIP sequence when figuring out
;                                       how close images can be taken (it may have the longest exposure time).
;              Ed Esfandiari 02/22/07 - Handle HI summed HI/LO images going to SSR1 and SW using new IP tables.
;              Ed Esfandiari 02/23/07 - Talked to Dennis and decided to use pre-determined ICER sizes anythime
;                                       ICER compression is used regardless of image size (don't use calcaulated
;                                       image size using compression factors).
;              Ed Esfandiari 08/14/09  Changed H-compress range from 5-17 to 5-14.
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15. Treat HISAM as no-compression.
;                                      Also checked for HI LO word, HI word, HIsample, HI1SPW, and HI2SPW IP
;                                      functions.
;              Ed Esfandiari 09/03/09  HI1SPW and HI2SPW are now calculated (removed hard coded size).
;              Ed Esfandiari 09/03/09  Use new values for icer9 and 11 (in icer_size_bytes.sav).
;
; $Log: get_os_db_stats.pro,v $
; Revision 1.13  2011/06/30 20:41:39  nathan
; do not add 2% to icer size
;
; Revision 1.12  2011/06/15 22:49:10  nathan
; fix max_expt for undefined
;
; Revision 1.11  2009/09/11 20:28:12  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.5  2006/04/06 15:26:23  esfand
; removed debug stuff
;
; Revision 1.4  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.3  2005/03/10 16:44:56  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

PRO GET_ICER_SIZE, os, ipd, icer_size, comp_ind, comp_cnt

  RESTORE,GETENV('PT')+'/IN/OTHER/'+'icer_size_bytes.sav'
  ;=>ICER_SIZE_BYTES LONG      = Array[12] containing 
  ; final size of images (bytes) using icer compressions 0-11.
  ; change from bytes to bits:

  icer_size_bits= icer_size_bytes * 8.0

  ;help,icer_size_bits

  ;ipsteps= os.ip
  ipsteps= ipd
  iptable= os.iptable
  ;comp_ind = WHERE(ipsteps(iptable).steps GE 5 AND ipsteps(iptable).steps LE 17 OR $
  ;comp_ind = WHERE(ipsteps(iptable).steps GE 5 AND ipsteps(iptable).steps LE 14 OR $
  comp_ind = WHERE(ipsteps(iptable).steps GE 5 AND ipsteps(iptable).steps LE 15 OR $
                     ipsteps(iptable).steps GE 90 AND ipsteps(iptable).steps LE 101 OR $
                     ipsteps(iptable).steps EQ 44, comp_cnt)
  ;print,ipsteps(os.iptable).steps
  ;print,comp_ind
  ;help,comp_cnt

  icer_size= 80000000.0 ; for none icers 

  IF (comp_cnt GT 0) THEN BEGIN
    icer_size= FLTARR(comp_cnt)+80000000.0 ; very large size for none icers

    FOR cc= 0, comp_cnt-1 DO BEGIN
      CASE ipsteps(iptable).steps(comp_ind(cc)) OF 
        90: icer_size(cc)= icer_size_bits(0)  ; icer0 used
        91: icer_size(cc)= icer_size_bits(1)  ; icer1 used
        92: icer_size(cc)= icer_size_bits(2)
        93: icer_size(cc)= icer_size_bits(3)
        94: icer_size(cc)= icer_size_bits(4)
        95: icer_size(cc)= icer_size_bits(5)
        96: icer_size(cc)= icer_size_bits(6)
        97: icer_size(cc)= icer_size_bits(7)
        98: icer_size(cc)= icer_size_bits(8)
        99: icer_size(cc)= icer_size_bits(9)
       100: icer_size(cc)= icer_size_bits(10)
       101: icer_size(cc)= icer_size_bits(11) ; icer11 used
       ELSE: icer_size(cc)= 80000000.0       ; icer not used
      ENDCASE
    ENDFOR
  ENDIF

  ;icer_size= icer_size * 1.02  ; add 2% to ICER size byte quota

;print,'************'
;print,'steps= ',ipsteps(iptable).steps
;print,'osnum,icer_size,comp_ind,comp_cnt= ', os.os_num, icer_size, comp_ind, comp_cnt
;print,'************'

  RETURN
END

PRO GET_OS_DB_STATS, orig_os, os_size, os_duration, pre_proc_time,ro_time, proc_time, setup_time, $
                     cadence, DOORCLOSED=doorclosed
COMMON OS_SHARE, osv
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
                      ip_arr, plan_lp, lprow, cor1pw, cor2pw
COMMON APIDS, multi_apid

;print,'*** in GET_OS_DB_STATS ****'

   ;** GET OBSERVING_MODE FOR EACH IMAGE IN THIS OS
   ;** MULTIPLE IMAGES FOR SEQ PW, ..

   os= orig_os
   os_size = 0L
   os_duration = 0.0D
   pre_proc_time= 0.0D  ; AEE - 02/10/03
   ro_time= 0.0D        ; AEE - 02/10/03
   proc_time= 0.0D
   setup_time= 0.0D
   cadence= 0.0D
  
ipsteps= ipd

lp = os.lp
;help,os


GET_ICER_SIZE, os, ipd, icer_size, comp_ind, comp_cnt ; use ipd since for HI-seq, first os.ip is modified

CASE (1) OF

   ; AEE 0/23/03
   ( (lp EQ 0) OR (lp EQ 1) OR (lp EQ 2) OR (lp EQ 3) OR (lp EQ 4) ) : BEGIN    ;** Double, Normal, Dark, Cal Lamp, Cont Image
      index = 0
      obs_mode = GET_OBSERVING_MODE(os, index, DOORCLOSED=doorclosed)
      numpixels = OS_GET_NUM_PIXELS(os)
      os_size = numpixels * obs_mode.avg_bits_pixel
      FOR ni=0, N_ELEMENTS(os_size)-1 DO BEGIN
        ;IF (ABS(icer_size(ni) - os_size(ni)) LT 16000) THEN $
        ;  os_size(ni)= icer_size(ni) $
        ;ELSE $
        ;  os_size(ni)= icer_size(ni) < os_size(ni)

        IF (icer_size(ni) LT 80000000.0) THEN os_size(ni)= icer_size(ni) 
          
        ;os_size(ni)= os_size(ni)+500.0+(os_size(ni)/272)*12.0  ; add 500 bytes for hdr+trailer and 12 bytes per packet
        os_size(ni)= os_size(ni)+500.0*8+(os_size(ni)/272)*12.0  ; add 500 bytes for hdr+trailer and 12 bytes per packet
      ENDFOR

;print,'os_size, numpixels= ',os_size, numpixels
;help,numpixels,obs_mode.avg_bits_pixel

      numpixels = numpixels(0) ; AEE 7/7/03

      ;AEE 7/7/03:
      ; save size for each apid using a space between them:
      ssize= STRTRIM(LONG(os_size(0)),2)
      FOR i= 1, N_ELEMENTS(os_size)-1 DO ssize= ssize+' '+STRTRIM(LONG(os_size(i)),2)

      iptimes= GET_IP_TIME(os,/SEPARATE)
      proc_times= STRTRIM(STRING(iptimes(0),'(F6.1)'),2)
      FOR i= 1, N_ELEMENTS(iptimes)-1 DO $
        proc_times= proc_times+' '+STRTRIM(STRING(iptimes(i),'(F6.1)'),2)
      
      IF (DATATYPE(multi_apid) EQ 'UND') THEN BEGIN
        ;multi_apid= {multi_apid, os_num: os.os_num, sizes: ssize}
        multi_apid= {multi_apid, os_num: os.os_num, sizes: ssize, proc_times: proc_times}
      ENDIF ELSE BEGIN
        mind= WHERE(multi_apid.os_num EQ os.os_num, mcnt)
        IF (mcnt EQ 0) THEN $
          ;multi_apid= [multi_apid, {multi_apid, os_num: os.os_num, sizes: ssize}]
          multi_apid= [multi_apid, {multi_apid, os_num: os.os_num, sizes: ssize, proc_times: proc_times}]
      ENDELSE
      os_size = os_size(0)

      pre_proc_time =  OS_GET_PRE_PROC_TIME(os, index, ro_time, setup_time)
      ;proc_time = numpixels * obs_mode.avg_proc_time / 1000	;** convert msec to sec
      proc_time= obs_mode.avg_proc_time
      os_duration = pre_proc_time + proc_time + ro_time ; AEE - Dec 19, 02
      ;cadence = os_duration ; cadence is not used for non-seq images but set it correctly, anyway.
   END

   (lp EQ 6) : BEGIN                 ;** HI Seq
      first = 1
      all_numpixels = OS_GET_NUM_PIXELS(os) ; AEE 7/9/03
      numpixels = all_numpixels(0)

      ; See if seq is a summed seq:
      tmp= WHERE(ip(os.iptable).steps NE ipd(os.iptable).steps, summed_images)

      FOR index=0, os.num_images-1 DO BEGIN
         IF (summed_images GT 0) THEN BEGIN
;print,'img '+strtrim(index,2)+': '
;print,os.ip(os.iptable).steps
           IF (index EQ 0) THEN BEGIN
             ; First image's table
             os.ip(os.iptable).steps= ipd(os.iptable).steps(2:*)      
             comp_ind= comp_ind-2
;print,'img '+strtrim(index,2)+': '
;print,os.ip(os.iptable).steps
;stop
           ENDIF
           IF (index GT 0 AND index LT os.num_images-1) THEN BEGIN
             ; Middle image's table
             desc= ip_arr(ipd(os.iptable).steps(0)).ip_description
             table_num= FIX(STRMID(desc,STRPOS(desc,'Table ')+6,2))
             os.ip(os.iptable).steps= ipd(table_num).steps
             ;comp_ind = WHERE(os.ip(os.iptable).steps GE 5 AND os.ip(os.iptable).steps LE 15 OR $
             ;                 os.ip(os.iptable).steps GE 90 AND os.ip(os.iptable).steps LE 101 OR $
             ;                 os.ip(os.iptable).steps EQ 44, comp_cnt)
             all_numpixels = OS_GET_NUM_PIXELS(os) ; AEE 7/9/03
             ;If (n_elements(icer_size) ne comp_cnt) then $
             ;  icer_size= fltarr(comp_cnt)+icer_size(0) 
 
             ; incase middle images have different comperssion
             ;IF (index EQ 1) THEN GET_ICER_SIZE, os, ipd, icer_size, comp_ind, comp_cnt
             IF (index EQ 1) THEN GET_ICER_SIZE, os, os.ip, icer_size, comp_ind, comp_cnt


;print,'img '+strtrim(index,2)+': '
;print,os.ip(os.iptable).steps
           ENDIF
           IF (index EQ os.num_images-1) THEN BEGIN
             ; Last image's table
;print,os.ip(os.iptable).steps
             desc= ip_arr(ipd(os.iptable).steps(1)).ip_description
             table_num= FIX(STRMID(desc,STRPOS(desc,'Table ')+6,2))
             os.ip(os.iptable).steps= ipd(table_num).steps
             ;comp_ind = WHERE(os.ip(os.iptable).steps GE 5 AND os.ip(os.iptable).steps LE 14 OR $
             ;comp_ind = WHERE(os.ip(os.iptable).steps GE 5 AND os.ip(os.iptable).steps LE 15 OR $
             ;                 os.ip(os.iptable).steps GE 90 AND os.ip(os.iptable).steps LE 101 OR $
             ;                 os.ip(os.iptable).steps EQ 44, comp_cnt)
             all_numpixels = OS_GET_NUM_PIXELS(os) ; AEE 7/9/03
;print,all_numpixels
;stop
             ;If (n_elements(icer_size) ne comp_cnt) then $
             ;  icer_size= fltarr(comp_cnt)+icer_size(0) 

             ;GET_ICER_SIZE, os, ipd, icer_size, comp_ind, comp_cnt ; incase last image has different compr
             GET_ICER_SIZE, os, os.ip, icer_size, comp_ind, comp_cnt ; incase last image has different compr

;print,'img '+strtrim(index,2)+': '
;print,os.ip(os.iptable).steps
;help,os,os.iptable
;stop
           ENDIF
         ENDIF
         ;obs_mode = GET_OBSERVING_MODE(os, index, DOORCLOSED=doorclosed)
         ;obs_mode = GET_OBSERVING_MODE(os, 0, DOORCLOSED=doorclosed) ; same PW (0) for HI_Seq
         ;an_os_size = all_numpixels * obs_mode.avg_bits_pixel

         ; IF summed image goes to two APIDs, break IP processing apart so that correct
         ; avg_bits_pixel is used for each:

         tos= os
         an_os_size = FLTARR(comp_cnt)
         IF comp_cnt GT 1 THEN BEGIN
        ;   tos.ip(os.iptable).steps=-1
        ;   ;tos.ip(os.iptable).steps= ipsteps(iptable).steps(0:comp_ind(0))
        ;   tos.ip(os.iptable).steps= ipsteps(os.iptable).steps(0:comp_ind(0))
        ;   if (index eq os.num_images-1) then $
        ;     tos.ip(os.iptable).steps= os.ip(os.iptable).steps(0:comp_ind(0)) 

;help,an_os_size,tos,comp_ind
;if index eq 0 then stop
;if (index eq os.num_images-1) THEN stop

           ;obs_mode = GET_OBSERVING_MODE(tos, 0, DOORCLOSED=doorclosed) ; same PW (0) for HI_Seq
;help,/st,obs_mode
           ;an_os_size(0)= all_numpixels(0) * obs_mode.avg_bits_pixel

           ;tos.ip(os.iptable).steps=-1
           ;tos.ip(os.iptable).steps= ipsteps(iptable).steps(comp_ind(0)+1:comp_ind(1)) 
           ;if (index eq os.num_images-1) then begin
             ;tos.ip(os.iptable).steps= os.ip(os.iptable).steps(comp_ind(0)+1:comp_ind(1)) ; middle (last if 2 comp_ind) 
             ;tos.ip(os.iptable).steps= os.ip(os.iptable).steps(comp_ind(comp_cnt-2)+1:comp_ind(comp_cnt-1)) ;last
           ;endif

           tcomp_ind= comp_ind
           comp_ind= [-1,comp_ind]
           hilo_size= 0L
           for ik= 0, comp_cnt-1 do begin 
             tos.ip(os.iptable).steps=-1
             tos.ip(os.iptable).steps= os.ip(os.iptable).steps(comp_ind(ik)+1:comp_ind(ik+1))
          ;print,tos.ip(os.iptable).steps
             obs_mode = GET_OBSERVING_MODE(tos, 0, DOORCLOSED=doorclosed) ; same PW (0) for HI_Seq
             ;an_os_size(1)= all_numpixels(1) * obs_mode1.avg_bits_pixel
             an_os_size(ik)= all_numpixels(ik) * obs_mode.avg_bits_pixel
             ; Note: if hi/lo is used, will have 2 imags each one-half image size?
             lohi= where(tos.ip(os.iptable).steps eq 120 or tos.ip(os.iptable).steps eq 121,hilo_cnt)
             if (hilo_cnt gt 0) then begin 
;help,ik,an_os_size,an_os_size(ik)
               an_os_size(ik)= an_os_size(ik)/2 ; assume each hi/lo is 1/2 image size.
               if (hilo_cnt gt 0) then hilo_size= an_os_size(0)*2 ; does not have hdr++trailer bytes
             endif
;help,hilo_size,an_os_size(ik)
;stop
             ;hi12spw= where(tos.ip(os.iptable).steps eq 16 or tos.ip(os.iptable).steps eq 17, hi12spw_cnt)
             ;if (hi12spw_cnt gt 0) then an_os_size(ik)= 55340L * 8
             ;print,an_os_size
             ;help,hilo_size
             ;if (index eq os.num_images-1) THEN stop
           endfor
           comp_ind= tcomp_ind
         ENDIF ELSE BEGIN
           tos.ip(os.iptable).steps=-1
           ;tos.ip(os.iptable).steps= ipsteps(iptable).steps
           tos.ip(os.iptable).steps= ipsteps(os.iptable).steps
           obs_mode = GET_OBSERVING_MODE(tos, 0, DOORCLOSED=doorclosed) ; same PW (0) for HI_Seq
           an_os_size(0)= all_numpixels(0) * obs_mode.avg_bits_pixel
         ENDELSE

;print,an_os_size
;if (index eq os.num_images-1) THEN stop

hisam_ind= WHERE(os.ip(os.iptable).steps EQ 15, hisam_cnt)
IF (hisam_cnt GT 0) THEN begin
  an_os_size = 4546L * 8
  ;help,hisam_cnt,an_os_size
  ;stop
end

         ;an_os_size = icer_size < an_os_size
         FOR ni=0, N_ELEMENTS(an_os_size)-1 DO BEGIN
           ;IF (ABS(icer_size(ni) - an_os_size(ni)) LT 16000) THEN $
           ;  an_os_size(ni)= icer_size(ni) $
           ;ELSE $
           ;  an_os_size(ni)= icer_size(ni) < an_os_size(ni)

           IF (icer_size(ni) LT 80000000.0) THEN an_os_size(ni)= icer_size(ni) 

           ;an_os_size(ni)= an_os_size(ni)+500.0+(an_os_size(ni)/272)*12.0  ; add 500 bytes for hdr+trailer and 12 bytes per packet
           an_os_size(ni)= an_os_size(ni)+500.0*8+(an_os_size(ni)/272)*12.0  ; add 500 bytes for hdr+trailer and 12 bytes per packet
         ENDFOR

;print,'os_size, numpixels= ',an_os_size, all_numpixels
;print,an_os_size
;if (index eq os.num_images-1) THEN stop

         iptimes= GET_IP_TIME(os, /separate)
        
         ;AEE 7/7/03:
         IF(first EQ 1) THEN BEGIN
           ssize= STRTRIM(LONG(an_os_size(0)),2) 
           proc_times= STRTRIM(STRING(iptimes(0),'(F6.1)'),2)
         ENDIF ELSE BEGIN 
           ssize= ssize + STRTRIM(LONG(an_os_size(0)),2)
           proc_times= proc_times+STRTRIM(STRING(iptimes(0),'(F6.1)'),2)
         ENDELSE
         ; save size for each apid using a space between them:
         FOR i= 1, N_ELEMENTS(an_os_size)-1 DO ssize= ssize+' '+STRTRIM(LONG(an_os_size(i)),2)
         FOR i= 1, N_ELEMENTS(iptimes)-1 DO proc_times= proc_times+' '+STRTRIM(STRING(iptimes(i),'(F6.1)'),2)
         ; Separate sizes for 1 to num_images in the sequence with a comma:
         IF (index LT os.num_images-1) THEN BEGIN
           ssize= ssize+','
           proc_times= proc_times+','
         ENDIF
         an_os_size = an_os_size(0)

         an_pre_proc_time =  OS_GET_PRE_PROC_TIME(os, 0, an_ro_time, an_setup_time) ;same PW (0) for HI_Seq
         ;an_proc_time = numpixels * obs_mode.avg_proc_time / 1000	;** convert msec to sec

         an_proc_time= obs_mode.avg_proc_time
         an_os_duration = an_pre_proc_time + an_proc_time + an_ro_time ; AEE - Dec 19, 02

         IF (first EQ 1) THEN BEGIN
            os_size = an_os_size
            pre_proc_time = an_pre_proc_time
            proc_time =  an_proc_time
            ro_time = an_ro_time
            setup_time = an_setup_time
            first = 0

            ; Set HI seq cadence to calculated minimum instead of 0.0 (will be reported in uploads)
            IF (os.cadence EQ 0.0) THEN os.cadence= FIX(an_pre_proc_time+an_ro_time+0.999)

            ;If user entered 0 or invalid cadence, set it to take images as close together:
            cadence= os.cadence

            ;IF (cadence LT (an_pre_proc_time+an_ro_time)) THEN cadence= an_pre_proc_time+an_ro_time 
            IF (cadence LT (an_pre_proc_time+an_ro_time)) THEN BEGIN
              oldc= cadence
;help,an_pre_proc_time,an_ro_time,cadence
;stop
              cadence= FIX(an_pre_proc_time+an_ro_time+0.999) 
              os.cadence= cadence 
              os_duration = an_os_duration
              PRINT,'Invalid Cadence ('+STRTRIM(oldc,2)+'). Using '+STRTRIM(cadence,2)+' Instead.' 
              IF (DATATYPE(osv) EQ 'STC') THEN $ 
                WIDGET_CONTROL,osv.info_text,SET_VALUE='Invalid Cadence ('+STRTRIM(oldc,2)+'). Using '+STRTRIM(cadence,2)+' Instead.'
            ENDIF ELSE os_duration = cadence
         ENDIF ELSE BEGIN
            os_size = os_size + an_os_size

            IF (cadence EQ 0.0) THEN $
              os_duration = os_duration + an_pre_proc_time+an_ro_time $
            ELSE $
              os_duration = os_duration + cadence 

            pre_proc_time = pre_proc_time + an_pre_proc_time
            proc_time =  proc_time + an_proc_time
            ro_time = ro_time + an_ro_time
            setup_time = setup_time + an_setup_time
         ENDELSE
         IF (an_os_size EQ 0) THEN BEGIN
            index = os.num_images	;** if any stats come up zero -> quit
            os_size = 0
            os_duration = 0
            pre_proc_time = 0
            proc_time = 0
            ro_time = 0
            setup_time = 0
            cadence = 0
         ENDIF
      ENDFOR
      ;AEE 7/7/03:
      IF (os.num_images GT 0) THEN BEGIN ; if only HI1 OR HI2 in sequence, don't try to add the other
                                         ; one that does not exist.
  
        IF (DATATYPE(multi_apid) EQ 'UND') THEN BEGIN
          multi_apid= {multi_apid, os_num: os.os_num, sizes: ssize, proc_times: proc_times}
        ENDIF ELSE BEGIN
          ; For HI seq, This routine is called twice and os_num is still 0 after first call,
          ; so go ahead and add the new data and separating the HI1 and HI2 with a &. Also,
          ; if this os already exists, os_num is no longer zero, make sure not to add to the
          ; sizes, when this routine is called repeatedly, by checking for a & in the sizes:
        
            mind= WHERE(multi_apid.os_num EQ os.os_num, mcnt)
            IF (mcnt EQ 0) THEN BEGIN
              multi_apid= [multi_apid, {multi_apid, os_num: os.os_num, sizes: ssize, proc_times: proc_times}]
            ENDIF ELSE BEGIN
              IF (multi_apid(mind).os_num EQ 0 AND STRPOS(multi_apid(mind).sizes,'&') LT 0) THEN BEGIN 
                ; don't add if os_num is not zero (sizes already set) or if there is already a & (both
                ; HI1 and HI2 in sequence and therefore an & is already present):
                multi_apid(mind).sizes= multi_apid(mind).sizes +'&'+ssize
                ;multi_apid(mind).proc_times= multi_apid(mind).proc_times +'&'+proc_times
              END
            ENDELSE
        ENDELSE
      ENDIF
;help,multi_apid
;help,/st,multi_apid
;if (index eq os.num_images-1) THEN stop

      ; AEE 5/14/04 - For HI summed-sequences, only the last image is downlinked (only hdr for the others
      ;               are downlinked) so set the size to just size of a single image in the sequence:
      ; Should not include step 37 or 38 (output summing buffers) since these can be used for single
      ; images. Single images won't have steps 34 or 36 as part of the processing functions but summed
      ; sequences do.

      ;tmp= WHERE(os.ip(os.iptable).steps EQ 34 OR os.ip(os.iptable).steps EQ 36, summ_cnt) 
      ;tmp= WHERE(os.ip(os.iptable).steps EQ 34 OR os.ip(os.iptable).steps EQ 36 OR $
      ;           os.ip(os.iptable).steps EQ 114 OR os.ip(os.iptable).steps EQ 115, summ_cnt) 
      ;IF (summ_cnt GT 0) THEN os_size= an_os_size


      IF (summed_images GT 0) THEN BEGIN 
        os_size= an_os_size
        ;proc_time= an_proc_time
      END

      lohi= where(os.ip(os.iptable).steps eq 120 or os.ip(os.iptable).steps eq 121,hilo_cnt)
      if (hilo_cnt gt 0) then os_size= an_os_size * 2 ; since an_os_size is 1/2 of image 

;help,os_size/8
;stop

   END

   (lp EQ 5) : BEGIN                 ;** SCIP Seq LP
      first = 1
      all_numpixels = OS_GET_NUM_PIXELS(os) ; AEE 7/9/03
      numpixels = all_numpixels(0)
      FOR index=0, os.num_images-1 DO BEGIN
         obs_mode = GET_OBSERVING_MODE(os, index, DOORCLOSED=doorclosed)
         an_os_size = all_numpixels * obs_mode.avg_bits_pixel
         ;an_os_size = icer_size < an_os_size
         FOR ni=0, N_ELEMENTS(an_os_size)-1 DO BEGIN
           ;IF (ABS(icer_size(ni) - an_os_size(ni)) LT 16000) THEN $
           ;  an_os_size(ni)= icer_size(ni) $
           ;ELSE $
           ;  an_os_size(ni)= icer_size(ni) < an_os_size(ni)

           IF (icer_size(ni) LT 80000000.0) THEN an_os_size(ni)= icer_size(ni) 

           ;an_os_size(ni)= an_os_size(ni)+500.0+(an_os_size(ni)/272)*12.0  ; add 500 bytes for hdr+trailer and 12 bytes per packet
           an_os_size(ni)= an_os_size(ni)+500.0*8+(an_os_size(ni)/272)*12.0  ; add 500 bytes for hdr+trailer and 12 bytes per packet
         ENDFOR

;print,'os_size, numpixels= ',an_os_size, all_numpixels
;stop

         iptimes= GET_IP_TIME(os, /separate)
        
         ;AEE 7/7/03:
         IF(first EQ 1) THEN BEGIN
           ssize= STRTRIM(LONG(an_os_size(0)),2) 
           proc_times= STRTRIM(STRING(iptimes(0),'(F6.1)'),2)
         ENDIF ELSE BEGIN 
           ssize= ssize + STRTRIM(LONG(an_os_size(0)),2)
           proc_times= proc_times+STRTRIM(STRING(iptimes(0),'(F6.1)'),2)
         ENDELSE
         ; save size for each apid using a space between them:
         FOR i= 1, N_ELEMENTS(an_os_size)-1 DO ssize= ssize+' '+STRTRIM(LONG(an_os_size(i)),2)
         FOR i= 1, N_ELEMENTS(iptimes)-1 DO proc_times= proc_times+' '+STRTRIM(STRING(iptimes(i),'(F6.1)'),2)
         ; Separate sizes for 1 to num_images in the sequence with a comma:
         IF (index LT os.num_images-1) THEN BEGIN
           ssize= ssize+','
           proc_times= proc_times+','
         ENDIF

         an_os_size = an_os_size(0)

         an_pre_proc_time =  OS_GET_PRE_PROC_TIME(os, index, an_ro_time, an_setup_time, filter_time, polar_time)
         IF (index GT 0) THEN BEGIN
           ; Remove filter time (for EUVI) for all but first image of the sequence:
           an_pre_proc_time= an_pre_proc_time - filter_time ; filter_time for non EUVI telescopes contains zero.
           an_setup_time= an_setup_time -  filter_time      ; filter_time for non EUVI telescopes contains zero
           ; IF polar/quad-sector is not changed, remove its time for this image: 
           IF (os.pw(index) EQ os.pw(index-1)) THEN BEGIN
             an_pre_proc_time= an_pre_proc_time - polar_time
             an_setup_time= an_setup_time - polar_time
           ENDIF
         ENDIF

         ;an_proc_time = numpixels * obs_mode.avg_proc_time / 1000	;** convert msec to sec
         an_proc_time = obs_mode.avg_proc_time
         an_os_duration = an_pre_proc_time + an_proc_time + an_ro_time ; AEE - Dec 19, 02.

         IF (first EQ 1) THEN BEGIN
            os_size = an_os_size
            pre_proc_time = an_pre_proc_time
            proc_time =  an_proc_time
            ro_time = an_ro_time
            setup_time = an_setup_time
            first = 0
            ;If user entered 0 or invalid cadence, set it to take images as close together:
            ;cadence= os.cadence
            ;max_expt= MAX(os.ex(os.tele,os.exptable,os.fw,os.pw(0:os.num_images-1))) / 1000.0
            ; Don't include exp.time of last image to figure out cadence since it may be the longest:
            max_expt= MAX(os.ex(os.tele,os.exptable,os.fw,os.pw(0:os.num_images-1))) / 1000.0
            max_pptime= an_setup_time + max_expt*os.num_images
           ;help,cadence,max_pptime

            ; Set SCIP seq cadence to calculated minimum instead of 0.0 (will be reported in uploads)
            IF (os.cadence EQ 0.0) THEN os.cadence= FIX(max_pptime+an_ro_time+0.999)

            cadence= os.cadence

            IF (cadence LT (max_pptime+an_ro_time)) THEN BEGIN
              oldc= cadence
              cadence= FIX(max_pptime+an_ro_time+0.999) 
              os.cadence= cadence 
              os_duration = an_os_duration
              PRINT,'Invalid Cadence ('+STRTRIM(oldc,2)+'). Using '+STRTRIM(cadence,2)+' Instead.'
              IF (DATATYPE(osv) EQ 'STC') THEN $ 
                WIDGET_CONTROL,osv.info_text,SET_VALUE='Invalid Cadence ('+STRTRIM(oldc,2)+'). Using '+STRTRIM(cadence,2)+' Instead.'
            ENDIF ELSE os_duration = cadence
         ENDIF ELSE BEGIN
            os_size = os_size + an_os_size
            IF (cadence EQ 0.0) THEN $
              os_duration = os_duration + (an_pre_proc_time+an_ro_time) $
            ELSE $
              os_duration = os_duration + cadence 
            pre_proc_time = pre_proc_time + an_pre_proc_time
            proc_time =  proc_time + an_proc_time
            ro_time = ro_time + an_ro_time
            setup_time = setup_time + an_setup_time
         ENDELSE
         IF (an_os_size EQ 0) THEN BEGIN
            index = os.num_images	;** if any stats come up zero -> quit
            os_size = 0
            os_duration = 0
            pre_proc_time = 0
            proc_time = 0
            ro_time = 0
            setup_time = 0
            cadence = 0
         ENDIF
      ENDFOR
      ;AEE 7/7/03:

      IF (DATATYPE(multi_apid) EQ 'UND') THEN BEGIN
        multi_apid= {multi_apid, os_num: os.os_num, sizes: ssize, proc_times: proc_times}
      ENDIF ELSE BEGIN
        mind= WHERE(multi_apid.os_num EQ os.os_num, mcnt)
        IF (mcnt EQ 0) THEN $
          multi_apid= [multi_apid, {multi_apid, os_num: os.os_num, sizes: ssize, proc_times: proc_times}]
      ENDELSE
   END

   ELSE : BEGIN 
      WIDGET_CONTROL,osv.info_text,SET_VALUE='%%GET_OS_DB_STATS: Unknown LP: '+STRN(os.lp)
      PRINT, '%%GET_OS_DB_STATS: Unknown LP: ', os.lp
      RETURN
   END

ENDCASE

os_size = os_size > 1
proc_time = proc_time > 1
os_duration = os_duration > 1

; Note: the os.os_num in this routine is not defined (zero) so the 
;       os_num in multi_apid will be set, later, in os_call_schedule.pro

;help,os.os_num,os.cadence,os_duration
;stop

; Note: Now only first image of a SCIP seq and all of SCIP images that are not sequences have
;       polar/quad_selector and filter (EUVI) times added to the setup time. In the other words,
;       We assume a filter/plor change for each non-sequence scheduled image and the first image
;       of SCIP seq. Note that HI telescopes don't use filter/polar so they are ok. Also, Images
;       2-N of a SCIP seq only have polar/quad time added if there was a movement.
;       Done in get_os_db_stats.pro, schedule_plot.pro, expand_seq.pro.

;help,multi_apid
;stop

RETURN

END
