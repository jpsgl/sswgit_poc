;+
;$Id: get_os_num.pro,v 1.1.1.2 2004/07/01 21:19:02 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GET_OS_NUM()
;               
; Purpose     : Get the next unique Observation Sequence number.
;               
; Explanation : The OS_NUM counter is stored in the saved dataset
;		'os_num.sav'.  This routine simply reads in the last
;		allocated os_num and adds one to it.
;               
; Use         : os_num = GET_OS_NUM()
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : os_num	The next unique Observation Sequence number.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Calls       : None.
;
; Restrictions: The saveset file 'os_num.sav' must exist in the current
;		directory and have write permission by the user.
;               
; Side effects: The saveset file 'os_num.sav' will be written over with
;		the allocated number.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Based on SOHO/LASCO planning tool. 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: get_os_num.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:02  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


;__________________________________________________________________________________________________________
;
FUNCTION GET_OS_NUM

   filename = GETENV('PT')+'/IN/OTHER/'+'os_num.sav'
   RESTORE, filename

   os_num = os_num + 1

   SAVE, os_num, FILENAME= filename 
  
   ; AEE - moved this SAVE to "ADD" section of scheudle_event to eliminate incrementing os_num even when they are 
   ;       not scheduled but it causes lots of problems becasue GET_OS_NUM is called from different places. So, I
   ;       left it as is, for now.

   RETURN, os_num

END
