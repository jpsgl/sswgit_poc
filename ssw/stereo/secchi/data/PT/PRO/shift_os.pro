;+
;$Id: shift_os.pro,v 1.1.1.2 2004/07/01 21:19:11 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SHIFT_OS
;               
; Purpose     : This function uses widgets to prompt the user for entering
;               a date to shift selected OS(s) to.
;
; Explanation : The start and stop times defining a subset of the schedule
;               plus the Observation number and its instances within that
;               period are displayed. The user entered date is then returned.
;               
; Use         : move_to= SHIFT_OS(os_num,start_dte, stop_dte, os_cnt) 
;    
; Inputs      : os_num     The unique number (LONG) that identifies an OS. 
;               start_dte  Start time (UTC). 
;               stop_dte   Stop time (UTC).
;               os_cnt     Instances of os_num (Integer).
;
; Opt. Inputs : caller     Structure containing id of caller. 
;               
; Outputs     : move_to    String - Time to move the selected os_num(s) to. 
;
; Opt. Outputs: None 
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: shift_os.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:37  esfand
; first checkin
;
;
;-


PRO SHIFT_OS_EVENT, event

COMMON SHIFT_OS_SHARE, return_dte, start_dte

  WIDGET_CONTROL, event.top, GET_UVALUE=shiftos   ; get structure from UVALUE

    CASE (event.id) OF

	shiftos.textd : BEGIN	;** exit program
	   WIDGET_CONTROL, shiftos.textd, GET_VALUE=val
           ;val = UTC2STR(STR2UTC(val), /ECS)
	   WIDGET_CONTROL, shiftos.textd, SET_VALUE=val
	END

	shiftos.cancel : BEGIN	;** cancel and exit program
           ;return_dte = 0
           return_dte = start_dte 
	   WIDGET_CONTROL, /DESTROY, shiftos.base
	END

	shiftos.apply : BEGIN	;** return date to shift to exit program
	   WIDGET_CONTROL, shiftos.textd, GET_VALUE=val
           ;val = UTC2TAI(STR2UTC(val))
           return_dte = val(0)
	   WIDGET_CONTROL, /DESTROY, shiftos.base
	END

        ELSE : BEGIN
        END

   ENDCASE

END

;__________________________________________________________________________________________________________
;

FUNCTION SHIFT_OS, CALLER=caller, os_num, beg_dte, end_dte, os_cnt

COMMON SHIFT_OS_SHARE, return_dte, start_dte

start_dte= beg_dte

    IF XRegistered("SHIFT_OS") THEN RETURN, -1


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    IF (KEYWORD_SET(caller)) THEN $
       base = WIDGET_BASE(/COLUMN, TITLE='SHIFT OS', /FRAME, GROUP_LEADER=caller.id) $
    ELSE $
       base = WIDGET_BASE(/COLUMN, TITLE='SHIFT OS', /FRAME)

    row = WIDGET_BASE(base, /COL)
     tmp = WIDGET_LABEL(row, VALUE='Shift scheduled instance(s) of OS_NUM '+os_num+' between:')
     tmp = WIDGET_LABEL(row, VALUE='Start Time: '+beg_dte)
     tmp = WIDGET_LABEL(row, VALUE='Stop  Time: '+end_dte)
     tmp = WIDGET_LABEL(row, VALUE='Scheduled:  '+os_cnt)
    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='Shift OS Start Time to: ')
     textd = WIDGET_TEXT(row, /EDITABLE, XSIZE=25)
    row = WIDGET_BASE(base, /ROW)
     apply = WIDGET_BUTTON(row, VALUE=" Apply ")
     cancel = WIDGET_BUTTON(row, VALUE=" Cancel ")

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, base, /REALIZE

    shiftos = CREATE_STRUCT( 'base', base, 		$
                             'apply', apply, 		$
                             'cancel', cancel, 		$
                             'textd', textd)

   WIDGET_CONTROL, base, SET_UVALUE=shiftos

   XMANAGER, 'SHIFT_OS', base, /MODAL

   RETURN, return_dte

END
