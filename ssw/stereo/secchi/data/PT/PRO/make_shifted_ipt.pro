;+
;$Id: make_shifted_ipt.pro,v 1.1.1.2 2004/07/01 21:19:03 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : MAKE_SHIFTED_IPT
;               
; Purpose     : Shift the observations in an .IPT file by a delta-time and place
;               the results in a temporary .IPT file.
;               
; Use         : MAKE_SHIFTED_IPT, ipt_file_name, tmp_ipt_name, delta_tai ,/DELETE
;    
; Inputs      : ipt_file_name   .IPT filenames to be shifted. 
;               tmp_ipt_name    Temporary .IPT file with observations shifted.
;               delta_tai       Delta time (TAI) by which to shift the observations.
;
; Opt. Inputs : None
;               
; Outputs     : None 
;
; Opt. Outputs: None
;
; Keywords    : DELETE           If set, delete the temporary .IPT file when finished.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: make_shifted_ipt.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:03  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

PRO MAKE_SHIFTED_IPT,ipt_file_name,tmp_ipt_name, shift_tai, delete=delete

IF(KEYWORD_SET(delete)) THEN $
  ;OPENR, lun, GETENV('PT')+'/IO/IPT/'+ipt_file_name, /GET_LUN, /DELETE $
  OPENR, lun, ipt_file_name, /GET_LUN, /DELETE $
ELSE $
  ;OPENR, lun, GETENV('PT')+'/IO/IPT/'+ipt_file_name, /GET_LUN
  OPENR, lun, ipt_file_name, /GET_LUN

;OPENW, tmp_lun, GETENV('PT')+'/IO/IPT/'+tmp_ipt_name, /GET_LUN
OPENW, tmp_lun, tmp_ipt_name, /GET_LUN

str= ''
READF,lun,str
WHILE ( NOT(EOF(lun))) DO BEGIN
 IF (STRPOS(str,'PT_LP_START') GE 0 OR STRPOS(str,'PT_LP_END') GE 0) THEN BEGIN
   toks= STR_SEP(str,'(')
   str= toks(0)+'('
   toks= STR_SEP(toks(1),')')
   dates= STR_SEP(toks(0),',')
   dates= IPT_DATES2ECS(dates)
   dates= UTC2TAI(STR2UTC(dates)) + shift_tai
   dates= UTC2STR(TAI2UTC(dates),/ECS)
   dates= ECS_DATES2IPT(dates)
   str= str+dates(0)
   FOR i= 1, N_ELEMENTS(dates)-1 DO str= str+','+dates(i)
   str= str+')'
 ENDIF
 PRINTF,tmp_lun,str
 READF,lun,str
END

CLOSE, lun 
FREE_LUN, lun 
CLOSE, tmp_lun
FREE_LUN, tmp_lun

END
