;+
;$Id: check_delay.pro,v 1.2 2005/12/16 14:58:51 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : CHECK_DELAY
;
; Purpose     : Check to see if adequate delay time is provided at the
;               start of schedule for FSW to read in the schedule.
;
; Explanation : Given total of images in the schedule, it calculates
;               estimated delay time (from 00:00:00) required by FSW
;               to read in the schedule.
;
; Use         : delay= CHECK_DELAY(os_arr, readtime)
;
; Inputs      : os_arr     Structure of scheduled obsevations.
;
; Opt. Inputs : None
;
; Outputs     : delay      If 0, current delay is adequate.
;                          >  0, shift of delay (seconds) is needed. 
;             : readtime   number of seconds required for FSW to read in the schedule.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, March 2005 - First Version.
;
; Modification History:
;               Ed Esfandiari 05/02/05 Added readtime outout parameter.
;               Ed Esfandiari 11/17/05 set readtime to first os start time in it is less.
;
; $Log: check_delay.pro,v $
; Revision 1.2  2005/12/16 14:58:51  esfand
; Commit as of 12/16/05
;
; Revision 1.1  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
;
;-


FUNCTION CHECK_DELAY, os_arr, readtime

  ; get the delay time (FSW schedule read), nimage_per_sec from save file,
  ; give number of images for which 1 second delay is required. Use it to
  ; figure out total delay for all images in current schedule. Return 0 if
  ; delay is OK, otherwise return delay (in seconds) that is required to shift
  ; the entire schedule with.

  tot_img= N_ELEMENTS(EXPAND_SEQ(os_arr))+2 ; 2 for B and T lines.

  readtime= 5  ; default (so start of first image in schedule should be midnight+5 or greater).
  delay= readtime 
  RESTORE,GETENV('PT')+'/IN/OTHER/images_delay_per_sec.sav' ; => nimage_per_sec
  IF (nimage_per_sec GT 0) THEN BEGIN
    readtime= FIX(tot_img/nimage_per_sec + 0.99)
    delay= readtime
  ENDIF

  first_time= TAI2UTC(MIN(os_arr.os_start))
  IF (first_time.time/1000 LT delay) THEN BEGIN 
    delay= delay - (first_time.time/1000) 
  ENDIF ELSE BEGIN 
    delay= 0
    readtime= first_time.time/1000
  ENDELSE

  RETURN,delay

END
