;+
;$Id: generate_ipt_singles.pro,v 1.7 2011/07/19 16:10:43 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GENERATE_IPT_SIGNLES
;               
; Purpose     : Generates single IPT files, for each OS in schedule, used
;               by PT BSF scheduling.
;               
; Explanation : This routine creates a single IPT format file, in form of
;               keyword/value pairs, for each OS in the current schedule
;               using an IPT file that has all of OSes in the schedule.
;               
;               
; Use         : GENERATE_IPT_SINGLES, ipt_name
;    
; Inputs      : ipt_name  IPT filename of full-schedule to be separated. This IPT file
;                         is deleted after individual IPT files are created.
;               
; Opt. Inputs : None.
;               
; Outputs     : None. (It generates an IPT file for each OS in the schedule in BSF directory.)
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : None
;
; Written by  : Ed Esfandiari, NRL, Nov 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 12/13/04 Only write 1st os_start line if many are present.
;              Ed Esfandiari 11/08/06 Added code to read-off starting comment lines.
;              Ed Esfandiari 12/13/06 Added code to generate single IPT files for GT-dumps.
;
; $Log: generate_ipt_singles.pro,v $
; Revision 1.7  2011/07/19 16:10:43  nathan
; Do not delete input IPT file; add info messages.
;
; Revision 1.6  2009/09/11 20:28:10  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.2  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
;
;-
;

;__________________________________________________________________________________________________________
;


PRO GENERATE_IPT_SINGLES, ipt_name
COMMON GTIPT, gt4ipt_names

   OPENR, IN, ipt_name, /GET_LUN
   WIDGET_CONTROL, /HOUR

   os_dir= GETENV('PT')+'/IO/OSID/'
   nonew=1

   str= ''
   READF, IN, str
   WHILE (STRPOS(str,'PT_OS_NUM') LT 0) DO READF, IN, str  ; read-off starting comment lines
   WHILE (NOT EOF(IN)) DO BEGIN
     IF (STRPOS(str,'PT_OS_NUM') GE 0) THEN BEGIN
       fipt= 'dir'
       osnum= FIX(STRMID(str,strpos(str,'=')+1,10))

       IF (osnum EQ  2) THEN BEGIN
         gt_os= str ; "PT_OS_NUM = 0002"
         READF, IN, str ; "PT_LP_NUM = 10"
         gt_lp= str
         READF, IN, str ; "PT_LP_NSCHED = .."
         gt_ns= "PT_LP_NSCHED = 1"
         READF, IN, str ; "PT_LP_START = (..."
         gt_st= str_sep((str_sep((str_sep(str,'('))(1),')'))(0),',') ; separate start_times
         READF, IN, str ; "PT_LP_END   = (..." 
         gt_ed= str_sep((str_sep((str_sep(str,'('))(1),')'))(0),',') ; separate end_times
         READF, IN, str ; "PT_LP_APID  = (..."
         gt_ap= str_sep((str_sep((str_sep(str,'('))(1),')'))(0),',') ; separate APIDs
         READF, IN, str ; "PT_LP_SC    = (..."
         gt_sc= str_sep((str_sep((str_sep(str,'('))(1),')'))(0),',') ; separate SpaceCraft IDs
         READF, IN, str ; "PT_GT_RATE  = (..."
         gt_rt= str_sep((str_sep((str_sep(str,'('))(1),')'))(0),',') ; separate GT-rates
         READF, IN, str ; ";"
         READF, IN, str ; ";"

         ; note: gt_ipt_names should have same number of elements as gt_ns (PT_LP_NSCHED) for GT-dumps
         FOR ngt= 0, N_ELEMENTS(gt4ipt_names)-1 DO BEGIN
           fn= os_dir+STRMID(gt4ipt_names(ngt),3,20)+'.gt'  ; GT-dump (1st)
           fipt= FINDFILE(fn)
           IF (fipt(0) EQ '') THEN BEGIN
             ;GT IPT single does not exist, create it:
             PRINT,'Generating single GT-dump IPT file '+fn
             OPENW, out, fn, /GET_LUN
             PRINTF, out, gt_os 
             PRINTF, out, gt_lp
             PRINTF, out, gt_ns
             PRINTF, out, "PT_LP_START = ("+gt_st(ngt)+")"
             PRINTF, out, "PT_LP_END   = ("+gt_ed(ngt)+")"
             PRINTF, out, "PT_LP_APID  = ("+gt_ap(ngt)+")"
             PRINTF, out, "PT_LP_SC    = ("+gt_sc(ngt)+")"
             PRINTF, out, "PT_GT_RATE  = ("+gt_rt(ngt)+")"
             PRINTF, out, ";"
             PRINTF, out, ";"
             CLOSE, out 
             FREE_LUN, out
	     nonew=0
           ENDIF
         ENDFOR
         IF EOF(IN) THEN BEGIN 
           CLOSE,in
           FREE_LUN,in
           RETURN
         ENDIF
       ENDIF

       IF (osnum GT 2) THEN BEGIN
         fn= os_dir+'OS_'+STRTRIM(STRMID(str,strpos(str,'=')+1,10),2)+'.IPT' 
         fipt= FINDFILE(fn)
         IF (fipt(0) EQ '') THEN BEGIN
            ;IPT does not exist, create it:
	    print,'Creating ',fn
	    nonew=0
            OPENW, out, fn, /GET_LUN 
            PRINTF, out, str
         ENDIF
       ENDIF
       os_start= 0
       READF, IN, str
       WHILE (NOT EOF(IN) AND STRPOS(str,'PT_OS_NUM') EQ -1) DO BEGIN
         IF (fipt(0) EQ '') THEN BEGIN
           ;IF (FIX(STRMID(str,strpos(str,'=')+1,10)) GT 2) THEN BEGIN
           IF (osnum GT 2) THEN BEGIN
             ; only image taking images:
             IF (STRPOS(str,'PT_LP_NSCHED') GE 0) THEN str= 'PT_LP_NSCHED = 1'
             IF (STRPOS(str,'PT_SET_ID') GE 0) THEN str= 'PT_SET_ID = (0)'
             IF (STRPOS(str,'PT_IMG_CNTR') GE 0) THEN str= 'PT_IMG_CNTR= (1)'
             IF (STRPOS(str,'PT_SC_ID') GE 0 OR STRPOS(str,'PT_SFN') GE 0 OR $
                 STRPOS(str,'PT_LP_START') GE 0) THEN BEGIN
               IF (STRPOS(str,',') GT 0) THEN str= STRMID(str,0,STRPOS(str,','))+')'
               IF (STRPOS(str,'PT_LP_START') GE 0) THEN os_start= os_start+1
             ENDIF 
             ;PRINTF, out, str
             ; Only write 1st os_start line if many are present:
             IF (os_start LE 1 OR STRPOS(str,'PT_LP_START') LT 0) THEN PRINTF, out, str
           ENDIF
         ENDIF
         READF, IN, str
       ENDWHILE
       IF (EOF(IN) OR STRPOS(str,'PT_OS_NUM') GE 0) THEN BEGIN
         IF (fipt(0) EQ '') THEN BEGIN
           CLOSE, out 
           FREE_LUN, out
         ENDIF
       ENDIF
     ENDIF
   ENDWHILE
   CLOSE,in
   FREE_LUN,in
   IF (nonew) THEN print,'No new OS_xxxx.IPT files created.'
END
