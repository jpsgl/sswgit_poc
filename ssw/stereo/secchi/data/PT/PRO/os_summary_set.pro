;+
;$Id: os_summary_set.pro,v 1.10 2009/09/11 20:28:16 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : OS_SUMMARY_SET
;
; Purpose     : This routine looks os_nums for a selected set in the 
;                definded_os_arr and displays a summary information
;                in the set window.
;
; Use         : OS_SUMMARY_SET, os_set
;
; Inputs      : os_set   Observations making up the set. 
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;               
; Modification History:
;              Ed Esfandiari 06/04/04 - Added Hdr-Only label for header-Only images.
;              Ed Esfandiari 06/07/04 - Display SCIP exptimes without the 1024 factor.
;              Ed Esfandiari 06/22/04 - Use lamp descriptions from a save set.
;              Ed Esfandiari 06/29/04 - Display all images in a seq using the actual start times.
;              Ed Esfandiari 07/14/04 - Use 4 ROI tables instead of 1.
;              Ed Esfandiari 07/19/04 - display COR image b4 HI image if scheduled at same time.
;              Ed Esfandiari 08/05/04 - Added date-obs and commanded exp.time comment.
;              Ed Esfandiari 09/20/04 - Added fps and sync to output.
;              Ed Esfandiari 09/30/04 - Changed sync to sc selected.
;              Ed Esfandiari 12/06/04 - Set HI summed seqeunces Pixel Summing since they
;                                       imply an implicit summing.
;              Ed Esfandiari 12/13/04 - Report non cal image exptimes corrected for CCD summing.
;              Ed Esfandiari 01/10/05 - Added lowgain exptime correction.
;              Ed Esfandiari 01/21/05 - Removed date-obs and commanded exp.time comment.
;              Ed Esfandiari 01/27/05 - Added implicit pixel summing for non seq images going
;              Ed Esfandiari 04/18/05 - Added functions/steps 52 and 53 which contain pixel summing
;                                       to the pixel summing checks.
;              Ed Esfandiari 01/13/06 - changed checks for steps 34 and 36 to 37 and 38.
;              Ed Esfandiari 11/21/06 - Ignore ccd_sum and low gain exptime correction (FSW does not do it).
;
;
; $Log: os_summary_set.pro,v $
; Revision 1.10  2009/09/11 20:28:16  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.7  2006/04/06 15:28:50  esfand
; changed checks for steps 34 and 36 to 37 and 38
;
; Revision 1.6  2005/04/27 20:38:38  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.5  2005/03/10 16:48:02  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:05  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;
FUNCTION OS_SUMMARY_SET, os_set

COMMON OP_SCHEDULED
COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
ip_arr, plan_lp, lprow, cor1pw, cor2pw

COMMON DIALOG, mdiag,font
COMMON CMD_SEQ_SCHEDULED   ; AEE - 02/22/03

COMMON LP_CAL_LAMP_SHARE, lpcalv ; AEE - 08/20/03
COMMON SCHED_SHARE, schedv

     
   os_arr2 = os_set(SORT([os_set.os_num]))   ;** sort on os_num
   uniq_os_arr2 = os_arr2(UNIQ([os_arr2.os_num]))
   num_os = N_ELEMENTS(uniq_os_arr2)

   title2=''
   cntr= 0


   IF (SEXIST(tele_types) EQ 0) THEN OS_INIT	;** define some variables

   str_arr_all = ''

   counter = 0
   FOR i = num_os-1, 0, -1 DO BEGIN    ;** for every unique os_num

      os_num = uniq_os_arr2(i).os_num

      osind = WHERE(defined_os_arr.os_num EQ os_num AND defined_os_arr.num_images GT 0,cnt) ; AEE - Mar 19, 03 

      IF (cnt EQ 0) THEN GOTO, CONT

     IF(defined_os_arr(osind(0)).lp EQ 6) THEN BEGIN ; AEE - 01/28/03 
       hi_seq_images= 0
       FOR hsi= 0, cnt-1 DO hi_seq_images= hi_seq_images + defined_os_arr(osind(hsi)).num_images 
       hi_seq_images= STRTRIM(FIX(STRING(hi_seq_images)),2)
     ENDIF

     FOR ind1= 0, cnt-1 DO BEGIN ; since HI seq has a two entry os_num (one for HI1 and one for HI2).
      ind= osind(ind1)
      os = defined_os_arr(ind)
      lp = defined_os_arr(ind).lp
      tele = defined_os_arr(ind).tele
      fw = defined_os_arr(ind).fw
      pw = defined_os_arr(ind).pw
      exptable = defined_os_arr(ind).exptable ; AEE 1/14/04
      camtable = defined_os_arr(ind).camtable ; AEE 1/14/04
      fps = defined_os_arr(ind).fps ; AEE 1/14/04
      iptable = defined_os_arr(ind).iptable
      lamp = defined_os_arr(ind).lamp
      start = defined_os_arr(ind).start
      num_images = defined_os_arr(ind).num_images
      ccd = defined_os_arr(ind).ccd
      ip = defined_os_arr(ind).ip
      ex = defined_os_arr(ind).ex
      occ_blks = REFORM(defined_os_arr(ind).occ_blocks(tele,*))
      roi_blks = REFORM(defined_os_arr(ind).roi_blocks(tele,*))

    ; AEE 5/5/04:
    indm = WHERE(ip(iptable).steps EQ 28) ; 28 = don't use occ mask table. 
    IF (indm(0) GE 0) THEN useocc = 0 ELSE useocc = 1  ; Also see os_get_num_pixels.pro
                                                      ;          generate_ipt.pro
                                                      ;          generate_ipt_set.pro
                                                      ;          os_summary.pro
                                                      ;          os_summary_1line.pro
                                                      ;          os_summary_set.pro
                                                      ;          os_summary_text.pro

   ;indm = WHERE(ip(iptable).steps EQ 25)
   ;IF (indm(0) LT 0) THEN useroi = 0 ELSE useroi = 1

   roi_table= WHICH_ROI_TABLE(ip,iptable)
   IF (roi_table EQ -1) THEN useroi = 0 ELSE useroi = 1

   CASE (1) OF
      ((useocc EQ 1) AND (useroi EQ 0)) : BEGIN
         blocks = WHERE(occ_blks GT 0,num)
         blockstr = 'Occulter (' + STRTRIM(num,2) + ' blocks)'
      END
      ((useroi EQ 1) AND (useocc EQ 0)) : BEGIN
         blocks = WHERE(roi_blks GT 0,num)
         blockstr = 'ROI (' + STRTRIM(num,2) + ' blocks)'
      END
      ((useroi EQ 1) AND (useocc EQ 1)) : BEGIN
         blocks = WHERE((occ_blks+roi_blks) GT 0,num)
         blockstr = 'Occulter & ROI (' + STRTRIM(num,2) + ' blocks)'
      END
      ELSE : BEGIN
         blocks = -1
         blockstr = 'None'
      END
   ENDCASE

   
      ;tos_num = '  '+STRTRIM(STRING(os_num),2)
      ;IF (ind1 EQ 1) THEN tos_num = ' -'+STRTRIM(STRING(os_num),2) ; AEE - 02/05/03 for 2nd HI of LP=6 use -os_num
      tos_num = '  '+STRING(os_num,FORMAT='(I4.4)')
      IF (ind1 EQ 1) THEN tos_num = ' -'+STRING(os_num,FORMAT='(I4.4)') ; for 2nd HI of LP=6 use -os_num
      ttele = '  '+STRMID(tele_types(tele),11,4)
      tlp = '  '+STRMID(STRING(op_types(lp))+'                     ',0,12)+' ' ; AEE - 9/25/03 - left justify
      tfw = ' '+STRING(FILTER2STR(fw_types,exptable,fw,tele), FORMAT='(a7)')+' ' ; AEE 4/5/04

      ccd1 = ccd(tele,camtable) ; AEE 1/14/04
      tnx = STRING((ccd1.x2 - ccd1.x1+1), FORMAT='(i5)')
      tny = STRING((ccd1.y2 - ccd1.y1+1), FORMAT='(i5)')
      xs = ccd1.xsum > 1
      ys = ccd1.ysum > 1
      tsum=' '+STRING(xs, FORMAT='(i1)')+'x'+STRING(ys, FORMAT='(i1)')


      ;** check for LEB summing

      ;** check for SEB (Pixel) summing:
      ;good = WHERE(ip(iptable).steps EQ 3, nip) ; AEE - 01/13/03


      IF (lp EQ 6) THEN BEGIN
        ; HI summed Sequences (have step 34 or 36) are implcitly summed to
        ; 1024x1024 (Pixel Summing), so set it in the summary file:
        good = WHERE(ip(iptable).steps EQ 3  OR $
                     ip(iptable).steps EQ 52 OR $
                     ip(iptable).steps EQ 53 OR $
;                     ip(iptable).steps EQ 34 OR $
;                     ip(iptable).steps EQ 36, nip) ; AEE - 01/13/03
                     ip(iptable).steps EQ 37 OR $
                     ip(iptable).steps EQ 38, nip) 
      ENDIF ELSE BEGIN
        ; Also can have non seq images going to a summing buffer (steps 37 or 38)
        ; which are also implicitly summed:
        good = WHERE(ip(iptable).steps EQ 3  OR $
                     ip(iptable).steps EQ 52 OR $
                     ip(iptable).steps EQ 53 OR $
                     ip(iptable).steps EQ 37 OR $
                     ip(iptable).steps EQ 38, nip)
      ENDELSE

      nip = 2^nip > 1
      tlsum=' '+STRING(nip, FORMAT='(i2)')+'x'+STRTRIM(STRING(nip, FORMAT='(i2)'),2)
      IF(nip LE 8) THEN tlsum= tlsum+' '
   
      ind2 = WHERE(os_set.os_num EQ os_num AND os_set.os_tele EQ tele) ; FOR HI Seq pickup only HI1 or HI2
      num_scheduled = N_ELEMENTS(ind2)

      FOR s=0, num_scheduled-1 DO BEGIN	;** loop over num_scheduled
         os_start = os_set(ind2(s)).os_start

         ; For non cal images, report exptime that is corrected for CCD summing:
         ccdsum= 1
         IF (lp NE 2 AND lp NE 3) THEN BEGIN
           ccdsum= ((xs > 1) * (ys > 1))
           IF (ccd1.gmode EQ 1) THEN ccdsum= ccdsum/4.0 ; for low gain, amplify exptime by 4
         ENDIF

         ccdsum=1 ; Ignore ccd_sum and low gain exptime correction (FSW does not do it).

         sc= os_arr(ind2(s)).sc
         IF (sc EQ 'AB') THEN sc= sc+'-Synced'

         IF (lp EQ 5 or lp EQ 6) THEN BEGIN
           expand_os= EXPAND_SEQ(os_set(ind2(s)))
           os_start= expand_os.os_start
         ENDIF

         ;tdate_obs = STRMID(UTC2STR(TAI2UTC(os_start), /ECS),0,19) ; also need ":ss" for secchi
         date_str = STRMID(UTC2STR(TAI2UTC(os_start), /ECS),0,19) ; also need ":ss" for secchi
         tdate_obs= date_str(0)


               ;filename= GETENV('PT')+'/IN/OTHER/'+'exposure_cnvt_factors.sav'
               ;RESTORE,filename ;=> HI_EXP_FACTOR=1000 and SCIP_EXP_FACTOR=1024.0
               ;scip_exp_factor= 1000.0  ; show scip exptimes without applying the 1024 ; 6/18/04
               ; For cal lamps, expsures are actually pulses so don't convert them:
               ;IF (lp EQ 3) THEN BEGIN
               ;  scip_exp_factor= 1.0
               ;  hi_exp_factor= 1.0
               ;ENDIF

               tpw_exp2= ''  ; AEE 9/25/03
               FOR n=0, num_images -1 DO BEGIN ; ** for each pw position in the seq (for
                                               ;    non-seq num_images=1 always)

                  tdate_obs = date_str(n)
                  IF tele LE 2 THEN BEGIN ; change SCIP exp from ms to sec (for SCIP 1sec=1024ms)
                    bk= ' ' 
                    tpw = bk+STRING(POLAR2STR(pw_types,exptable,pw(n),tele), FORMAT='(a7)')
                    IF (lp EQ 5 AND num_images GT 1) THEN BEGIN
                      tlp= STRMID(tlp,0,10)+' '+STRTRIM(n+1,2)+'/'+STRTRIM(num_images,2)+' '
                    ENDIF
                    IF (lp EQ 3) THEN $ ; AEE - 11/18/03 - report cal lamps in pulses (maybe 6 digits).
                      texp=' '+STRING(ex(tele,exptable,fw,pw(n))/ccdsum,FORMAT='(I7)') $ ; AEE 1/14/04
                    ELSE $
                      texp=' '+STRING(ex(tele,exptable,fw,pw(n))/ccdsum,FORMAT='(I7)') ; AEE 1/14/04

                    IF (lp EQ 0) THEN BEGIN  ; AEE - 9/24/03
                      ; For double sky images, add the second pw and exp.time (for these images n is 
                      ; always 0) and pw(1) indicates the second pw positions and exptime to be used.

                      tpw_exp2= '   ('+ POLAR2STR(pw_types,exptable,pw(1),tele)+' , '+ $ ; AEE 4/5/04
                              STRTRIM(STRING(ex(tele,exptable,fw,pw(1))/ccdsum,FORMAT='(I7)'),2)+')' 
                    ENDIF
                  ENDIF ELSE BEGIN              ; change HI exp from ms to sec (for HI 1sec= 1000ms)
                    ; AEE - 01/23/03 - HI has no polar (and pw is only a 20 element array):
                    tpw = ' '+STRING(POLAR2STR(pw_types,exptable,pw(0),tele), FORMAT='(a7)')  ; AEE 4/5/04
                    IF (lp EQ 3) THEN $ ; AEE - 11/18/03 - report cal lamps in pulses (maybe 6 digits).
                      texp=' '+STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)') $
                    ELSE $
                      texp=' '+STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)')

                    IF (lp EQ 0) THEN BEGIN  ; AEE - 9/24/03
                      ;Note: HI telescpes do not have a shutter, polarizer, or filter wheels so pw(0) 
                      ;is only used.
 
                      tpw_exp2= '   ('+POLAR2STR(pw_types,exptable,pw(0),tele)+' , '+ $ 
                                STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2)+')' 

                    ENDIF
                  ENDELSE
                  IF (lp EQ 4) THEN texp = ' '+STRING(0.0,FORMAT='(I7)')  ; AEE 10/7/03
                  ;counter = counter+1
                  ;tcntr =''
                  cntr= cntr+1
                  tcntr = STRING(cntr,FORMAT='(i5)')

                  ; AEE - 7/29/03 - only show the last image of a HI-seq:

                  IF (lp EQ 6) THEN BEGIN
                    this_image= n+1
                    IF (ind1 EQ 1) THEN this_image= this_image + defined_os_arr(osind(0)).num_images 
                    hi_seq_img= STRTRIM(FIX(STRING(this_image)),2)+'/'+hi_seq_images
                    tlp = STRMID('  '+STRTRIM(STRING(op_types(lp)),2)+' '+hi_seq_img+'     ',0,15)
                  ENDIF

                  IF (lp EQ 3) THEN BEGIN ; AEE 8/20/03
                    RESTORE, FILENAME= GETENV('PT')+'/IN/OTHER/secchi_cal_leds.sav'
                    ; => euvi_lamps, cor1_lamps, cor2_lamps, hi1_lamps, hi2_lamps
                    CASE tele of
                       0: lamp_used= ' (' +euvi_lamps(lamp)+')'
                       1: lamp_used= ' (' +cor1_lamps(lamp)+')'
                       2: lamp_used= ' (' +cor2_lamps(lamp)+')'
                       3: lamp_used= ' (' +hi1_lamps(lamp)+')'
                       4: lamp_used= ' (' +hi2_lamps(lamp)+')'
                    ENDCASE
                    ;tlp = STRMID('  Cal'+lamp_used+'       ',0,15) 
                    tlp = STRMID('  LED'+lamp_used+'       ',0,15) 
                    ; For cal lamps, assign the pulse to the exptime:
                  ENDIF


                  str= tdate_obs+tcntr+ ttele+ texp+ tnx+ tny+ tsum+tlsum+ tfw+ tpw+ tlp+ tos_num+ tpw_exp2

                  IF (schedv.sc EQ 0) THEN BEGIN
                    str= str+' '+sc
                  ENDIF ELSE BEGIN
                    IF (sc EQ 'AB-Synced') THEN str= str+' '+sc
                  ENDELSE

                  IF (fps EQ 1) THEN str= str+ ' FPS'

                  ; AEE 6/6/04 - added hdr-only label:
                  hdr_only= WHERE(ip(iptable).steps EQ 5, hcnt)
                  IF (hcnt GT 0) THEN str= str+' Hdr-Only'

                  IF (STRLEN(tpw_exp2) GT 0) THEN title2= '  (Polar2,Exp2)'
                  IF (lp NE 6) THEN BEGIN 
                    str_arr_all = [str_arr_all, str]
                  ENDIF ELSE BEGIN
                    ;summed= WHERE(ip(iptable).steps EQ 34 OR ip(iptable).steps EQ 36,scnt)
                    summed= WHERE(ip(iptable).steps EQ 37 OR ip(iptable).steps EQ 38,scnt)
                    IF (scnt GT 0) THEN str= str+' Summed'
                    ; only print last line (i.e. 25/25 line) of a HI seq since everything else
                    ; on all lines including the times are the same:
                    ;IF (n EQ num_images-1) THEN str_arr_all = [str_arr_all, str]

                    ; No, print all HI seq lines (each line/image now has its acutual time).
                    str_arr_all = [str_arr_all, str]
                  ENDELSE

               ENDFOR
         END
      ENDFOR ; ind1 for HI Seq
      CONT:
   ENDFOR   ; uniqe os

   str_arr_all = str_arr_all(1:*)

   inds = SORT(str_arr_all)     
   ;str_arr_all(inds(0)) = ' '+str_arr_all(inds(0)) 
   str_arr_all = STRMID(str_arr_all,0,20) + STRMID(str_arr_all,25,STRLEN(str_arr_all(0))-1)
   str_arr_all =    str_arr_all(inds)
   ; do a second sort to move COR images before HI images, if at same time:
   str_arr_all = str_arr_all(SORT(str_arr_all))
   str_arr_all(0) = ' '+str_arr_all(0) ;line up first row with the rest.

  ;ttime='Unlike upload times, following "Date Obs" are start of image setup times NOT commanded exposure times.'
  ttime=''
  tit=   'Exposures are in milliseconds but For Cal. LEDs Exp represents number of pulses.'
  title= 'Date-Obs             Tele    Exp   Nx   Ny  CCD  SEB   Filter    Polar  SEB-Program   OS_NUM'
  title= title+title2 ; AEE 9/25/03

   ;RETURN, [tit,title,STRTRIM(str_arr_all(inds),2)]
   ;RETURN, [tit,title,STRTRIM(str_arr_all,2)]
   RETURN, [ttime,tit,title,STRTRIM(str_arr_all,2)]

END
