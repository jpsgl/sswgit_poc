;+
;$Id: os_stats.pro,v 1.4 2005/01/24 17:56:34 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_STATS
;               
; Purpose     : Calculates time to process and the processed size for a SECCHI image.
;               
; Use         : OS_STATS, os_duration, os_size, os_pre_proc_time, os_ro_time, os_proc_time, 
;                         os_setup_time, os_cadence, obs_mode_id
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : os_duration	  time in secs to process image(s) plus
;                                 delay times , if any, between images in a seq (DOUBLE).
;		os_size		  size in bits of processed image(s) (LONG).
;               os_pre_proc_time  time is secs to pre-process image(s).
;               os_ro_time        time is secs to read-out CCD.
;               os_proc_time      time is secs to process image(s) (DOUBLE).
;               os_setup_time     time is secs for mech. setup (DOUBLE).
;               os_cadence        time is secs between images in a sequence.
;		obs_mode_id	  The unique ID of this observing mode.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO. 
;
; Written By  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/09/04 - Added os_proc_time as output.
;              Ed Esfandiari 06/14/04 - Added os_cadence as output.
;              Ed Esfandiari 06/16/04 - Added os_setup time (os_pre_proc_time - exptime) as output.
;              Ed Esfandiari 07/14/04 - Using 4 ROI tables instead of 1.
;              Ed Esfandiari 09/30/04 - Removed sync.
;
; $Log: os_stats.pro,v $
; Revision 1.4  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:05  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-            

;__________________________________________________________________________________________________________
;
PRO OS_STATS, os_duration, os_size, os_pre_proc_time, os_ro_time, os_proc_time, os_setup_time, $
              os_cadence, obs_mode_id

COMMON OS_SHARE
COMMON OS_INIT_SHARE
COMMON OS_ALL_SHARE
COMMON OS_DEFINED

COMMON OS_TABLE_CAMERA_SHARE

   info_text = osv.info_text
   ;** first erase current values
   new_value = 0D
   WIDGET_CONTROL, osv.time_text, SET_VALUE=STRING(new_value,FORMAT='(d7.1)')+' Secs'
   WIDGET_CONTROL, osv.size_text, SET_VALUE=STRTRIM(STRING(new_value,FORMAT='(d16.1)'),2)+' '
   WIDGET_CONTROL, osv.comp_text, SET_VALUE=STRING(new_value,FORMAT='(d5.1)')+' %'

   WIDGET_CONTROL, /HOUR

   WIDGET_CONTROL, info_text, SET_VALUE='Calculating Statistics ....'

   ;** GET VARS FOR GIVEN LP BEING SCHEDULED
   result = GET_PLAN_LP_INFO( tele, exptable, camtable, iptable, fw, pw, lamp, lp_num_images, ex_table, sub, $
                              start, cadence, fps) ; AEE - 1/13/04
   IF (result LT 0) THEN BEGIN ;** Error. or LP_CMD_TABLE
      os_duration=1
      os_size=1
      os_pre_proc_time= 0.0  ; AEE - 02/07/03 ; To keep OS_CALL_SCHEDULE from complaining.
      os_ro_time= 0.0        ; AEE - 02/07/03
      os_proc_time= 0.0
      os_setup_time = 0.0
      RETURN		
   ENDIF

   an_os = os_instance                      ;** os_instance defined in OS_DEFINED COMMON

   ; Note: os_instance (an element for defined_os_arr) in only used in this routine to pass info
   ;       using a structure (an_os) to get_os_db_stats for calculating size and duration info.
   ;       It is not saved as an element in the defined_os_arr (saving is done in os_call_schedule.pro).
  
   an_os.lp = plan_lp
   an_os.tele = tele(0)
   an_os.exptable = exptable(0) ; AEE - 1/13/04
   an_os.camtable = camtable(0) ; AEE - 1/13/04
   an_os.fps = fps ; AEE - 1/13/04
   an_os.iptable = iptable(0)
   an_os.cadence = cadence(0)  ; AEE - 01/15/03
   an_os.fw = fw(0)
   ;an_os.pw = pw(0)
   an_os.pw = pw
   IF (plan_lp EQ 0) THEN an_os.pw(1)= pw(1) ; AEE 9/23/03  Double Sky images (lp=0) have two valid pw positions.
   an_os.lamp = lamp(0)
   an_os.num_images = lp_num_images(0)
   an_os.ex = ex_table
   an_os.ccd = ccd
   an_os.ip = ip
   an_os.occ_blocks = occ_blocks
   ;an_os.roi_blocks = roi_blocks

   ; See which of 4 ROI tables, if any, goes with this image:

;   iptable= iptable(0)
;   ind = WHERE(ip(iptable).steps EQ 25 OR $   ; step #25 = "Use ROI1 table"
;               ip(iptable).steps EQ 45 OR $   ; step #45 = "Use ROI2 table"
;               ip(iptable).steps EQ 46 OR $   ; step #46 = "Use ROI3 table"
;               ip(iptable).steps EQ 47, icnt) ; step #47 = "Use ROI4 table"
;
;   IF (icnt EQ 0) THEN BEGIN
;     an_os.roi_blocks = roi_blocks(*,*,0) ; keep "Use ROI1 table" as default. 
;   ENDIF ELSE BEGIN
;     ind= ind(icnt-1)
;     IF (icnt GT 1) THEN $
;       PRINT,'WARNING - Multiple "Use ROI.." functions selected. Using last (function #'+ip(iptable).steps(ind)+').'
;     IF (ip(iptable).steps(ind) EQ 25) THEN an_os.roi_blocks = roi_blocks(*,*,0) ; Use ROI1 Mask Table 
;     IF (ip(iptable).steps(ind) EQ 45) THEN an_os.roi_blocks = roi_blocks(*,*,1) ; Use ROI2 Mask Table 
;     IF (ip(iptable).steps(ind) EQ 46) THEN an_os.roi_blocks = roi_blocks(*,*,2) ; Use ROI3 Mask Table 
;     IF (ip(iptable).steps(ind) EQ 47) THEN an_os.roi_blocks = roi_blocks(*,*,3) ; Use ROI4 Mask Table
;   ENDELSE

   ; We need to keep one of the 4 input ROI tables (from OS_ALL_SHARE) as defined_os_arr.roi_blocks so 
   ; check the IP steps and find out which one of four is to be used:

   roi_table= WHICH_ROI_TABLE(ip,iptable(0))
   IF (roi_table EQ -1) THEN roi_table= 0 ; use the first ROI table as defualt when none present.
   an_os.roi_blocks = roi_blocks(*,*,roi_table)

   GET_OS_DB_STATS, an_os, os_size, os_duration, os_pre_proc_time, os_ro_time, os_proc_time, $
                    os_setup_time, os_cadence, DOORCLOSED=osv.door

   
   IF (os_size LE 1) THEN BEGIN 
      WIDGET_CONTROL, info_text, SET_VALUE='No Statistics Found for Given Observing Mode'
      os_size= 0.0
      os_duration= 0.0
      os_pre_proc_time= 0.0
      os_ro_time= 0.0
      os_proc_time= 0.0
      os_setup_time= 0.0
   ENDIF

   IF (plan_lp EQ 6) THEN BEGIN  ; HI_SEQ uses two telescopes HI1 and HI2. Add HI2 info to HI1.
     an_os.lp = plan_lp
     an_os.tele = tele(1)
     an_os.exptable = exptable(1) ; AEE - 1/13/04
     an_os.camtable = camtable(1) ; AEE - 1/13/04
     an_os.fps = fps ; AEE - 01/13/03
     an_os.iptable = iptable(1)
     an_os.cadence = cadence(1)  ; AEE - 01/15/03
     an_os.fw = fw(1)
     an_os.pw = pw(1)
     an_os.lamp = lamp(1)
     an_os.num_images = lp_num_images(1)
     an_os.ex = ex_table  ; *** see the ex1 note in set_plan_lp_info.pro
     an_os.ccd = ccd
     an_os.ip = ip
     an_os.occ_blocks = occ_blocks
     ;an_os.roi_blocks = roi_blocks

     ; See which of 4 ROI tables, if any, goes with this image:

;     iptable= iptable(1)
;     ind = WHERE(ip(iptable).steps EQ 25 OR $   ; step #25 = "Use ROI1 table"
;                 ip(iptable).steps EQ 45 OR $   ; step #45 = "Use ROI2 table"
;                 ip(iptable).steps EQ 46 OR $   ; step #46 = "Use ROI3 table"
;                 ip(iptable).steps EQ 47, icnt) ; step #47 = "Use ROI4 table"
;
;     IF (icnt EQ 0) THEN BEGIN
;       an_os.roi_blocks = roi_blocks(*,*,0) ; keep "Use ROI1 table" as default. 
;     ENDIF ELSE BEGIN
;       ind= ind(icnt-1)
;       IF (icnt GT 1) THEN $
;         PRINT,'WARNING - Multiple "Use ROI.." functions selected. Using last (function #'+ip(iptable).steps(ind)+').'
;       IF (ip(iptable).steps(ind) EQ 25) THEN an_os.roi_blocks = roi_blocks(*,*,0) ; Use ROI1 Mask Table 
;       IF (ip(iptable).steps(ind) EQ 45) THEN an_os.roi_blocks = roi_blocks(*,*,1) ; Use ROI2 Mask Table 
;       IF (ip(iptable).steps(ind) EQ 46) THEN an_os.roi_blocks = roi_blocks(*,*,2) ; Use ROI3 Mask Table 
;       IF (ip(iptable).steps(ind) EQ 47) THEN an_os.roi_blocks = roi_blocks(*,*,3) ; Use ROI4 Mask Table
;     ENDELSE

     roi_table= WHICH_ROI_TABLE(ip,iptable(1))
     IF (roi_table EQ -1) THEN roi_table= 0 ; use the first ROI table as defualt when none present.
     an_os.roi_blocks(tele(1),*) = roi_blocks(tele(1),*,roi_table) ; don't overwrite HI1 roi_blocks from above.

     GET_OS_DB_STATS, an_os, os_size1, os_duration1, os_pre_proc_time1, os_ro_time1, os_proc_time1, $
                      os_setup_time1, os_cadence1
     IF (os_size1 LE 1) THEN BEGIN 
        WIDGET_CONTROL, info_text, SET_VALUE='No Statistics Found for Given Observing Mode'
        os_size1= 0.0
        os_duration1= 0.0
        os_pre_proc_time1= 0.0
        os_ro_time1= 0.0
        os_proc_time1= 0.0
        os_setup_time1= 0.0
     ENDIF

     IF(lp_num_images(0) GT 0 AND lp_num_images(1) GT 0) THEN BEGIN ; AEE - 01/28/03
       ; Both HI1 and HI2 images, keep all of them:
       os_size= [os_size,os_size1]
       os_duration= [os_duration,os_duration1]
       os_pre_proc_time= [os_pre_proc_time,os_pre_proc_time1]
       os_ro_time= [os_ro_time,os_ro_time1]
       os_proc_time= [os_proc_time,os_proc_time1]
       os_setup_time= [os_setup_time,os_setup_time1]
       os_cadence= [os_cadence,os_cadence1]
     ENDIF ELSE BEGIN
       IF(lp_num_images(1) GT 0) THEN BEGIN  ; Only HI2 images, keep only HI2 data
         os_size= os_size1
         os_duration= os_duration1
         os_pre_proc_time= os_pre_proc_time1
         os_ro_time= os_ro_time1
         os_proc_time= os_proc_time1
         os_setup_time= os_setup_time1
         os_cadence= os_cadence1
       ENDIF ; No else needed since HI1 data is already picked up from first call to GET_OS_DB_STATS, above.
     ENDELSE
   ENDIF

   ;** UPDATE STATS IN BASE SCHED WINDOW
   WIDGET_CONTROL, info_text, GET_VALUE=tmp
   IF (tmp EQ 'Calculating Statistics ....') THEN $
      WIDGET_CONTROL, info_text, SET_VALUE='   '

   WIDGET_CONTROL, osv.time_text, SET_VALUE=STRING(TOTAL(os_duration),FORMAT='(d7.1)')+' Secs'


   ;** convert units
   WIDGET_CONTROL, osv.bits_bytes, GET_VALUE=to
   new_value = SCHEDULE_CONVERT_UNITS(os_size, 0, to)
   WIDGET_CONTROL, osv.size_text, SET_VALUE=STRTRIM(STRING(TOTAL(new_value),FORMAT='(d16.1)'),2)+' '

   comp_type= GET_COMP_TYPES(an_os)
   WIDGET_CONTROL, osv.comp_text , SET_VALUE= comp_type

END
