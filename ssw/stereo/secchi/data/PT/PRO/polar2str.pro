;+
;$Id: polar2str.pro,v 1.1.1.2 2004/07/01 21:19:07 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : POLAR2STR
;               
; Purpose     : Convert a polarizer/sector number to its actual text representaion.
;               
; Use         : polar= POLAR2STR(pw_types,table, pw, tele) 
;    
; Inputs      : pw_types  String array of polarizer/sector names. 
;               table     exposure table number
;               pw        Polarizer/sector wheel number selected
;               tele      telescope number
;
; Opt. Inputs : None
;               
; Outputs     : polar    Text string for the selected polarizer/sector. 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: polar2str.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION POLAR2STR,pw_types,table,pw,tele

   IF (tele LE 2) THEN $
     RETURN, STRTRIM((STR_SEP(pw_types(table,pw,tele),':'))(1),2) $
   ELSE $
     RETURN, 'None'

END

