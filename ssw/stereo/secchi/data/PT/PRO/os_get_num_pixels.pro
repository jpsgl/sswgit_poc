;+
;$Id: os_get_num_pixels.pro,v 1.15 2013/01/09 13:33:23 mcnutt Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_GET_NUM_PIXELS
;               
; Purpose     : Return number of pixels in an image taking into accunt
;               masked blocks and summings.
;               
; Use         : pixels = OS_GET_NUM_PIXELS(os) 
;    
; Inputs      : os       Observing sequence structure.
;
; Opt. Inputs : None 
;               
; Outputs     : pixels   Numer of pixels contained in the image. 
;
; Opt. Outputs: None
;
; Keywords    : ccd_ro_pixels, no_compress 
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 07/12/04 - changed x,y range from 0-2175 to 1-2176 and also
;                                       made changes to use 4 ROI tables instead of 1.
;                            07/14/04 - Added new IP step, "Send Raw Image", and treat it
;                                        as "No Ccompression" for PT purposes.
;              Ed Esfandiari 12/06/04 - for HI 1 & 2 summed sequences, apply an implicit
;                                       "Pixel Summing" (make it 1/4 or 1024x1024).
;              Ed Esfandiari 01/27/05 - For any image going to a summing buffer, assume
;                                       an implicit pixel summing.
;              Ed Esfandiari 01/28/05 - Added ccd_ro_pixels keyword to return number of
;                                       pixels when ccd is readout for calculating the 
;                                       Image Processing, IP, time.
;              Ed Esfandiari 02/04/05 - Read compression factors from a file.
;              Ed Esfandiari 04/18/05 - Added functions/steps 52 and 53 which contain pixel
;                                       summing to pixel summing check.
;              Ed Esfandiari 05/13/05 - Trim x-axis by 128 pixels if IP-Trim function present.
;              Ed Esfandiari 05/19/05 - Modified code to apply only IP functions for summing
;                                       and masking found before each compresstion function to
;                                       image going to corresponding destination (comp_cnt).
;              Ed Esfandiari 10/06/05   Added ICER compressions (IP functions 82 to 93).
;              Ed Esfandiari 10/06/05   Added code so that for square root functions (step #2, 70, 
;                                       71, or 96), a compression factor of 3.0 is applied.
;              Ed Esfandiari 11/09/05   Square root functions (step #70,71,96) changed to
;                                       78,79,104. Also icer steps changed to 90 to 101.
;              Ed Esfandiari 11/21/05   added checks for steps 114 and 115.
;              Ed Esfandiari 05/04/06   Added no_compress keyword. If set pixels returned do not 
;                                       acount for comperssion.
;              Ed Esfandiari 02/08/07   Use new comp_factor file with telescope specific comp. factors.
;              Ed Esfandiari 02/16/07   Trimmed HI-summed images to 2048x2048, if greater, before summing.
;              Ed Esfandiari 07/17/07   Added summing buffer, sb_cnt, to use summed cmpr. factor for HI 
;                                       summed images.
;              Ed Esfandiari 08/14/09   Changed H-compress range from 5-17 to 5-14.
;              Ed Esfandiari 08/14/09   Added checks for HI1SPW and HI2SPW (functions 16-17). Each counts
;                                       as 2 pixel-sum.  
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15. Treat HISAM as no-compression.
;              Ed Esfandiari 09/02/09  Treat steps 16 and 17 as explict summing for HI-summed images. 
;              Ed Esfandiari 09/03/09  Corrected ROI use for mutil-apid image processing ertires (HI-summed).
;                                      If COMMON OS_ALL_SHARE common block is added in the future, rename the
;                                      ip to something else.
;
;
;
; $Log: os_get_num_pixels.pro,v $
; Revision 1.15  2013/01/09 13:33:23  mcnutt
; changed made when generating new block sched 20130108
;
; Revision 1.14  2009/09/11 20:28:14  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.9  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.8  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.7  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.6  2005/04/27 20:38:38  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.5  2005/03/10 16:46:29  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:44  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


;FUNCTION GET_CCDSUM_MASK_PIXELS, os, ccd_ro_pixels= ccd_ro_pixels
FUNCTION GET_CCDSUM_MASK_PIXELS, os, steps, ccd_ro_pixels= ccd_ro_pixels

COMMON OS_SHARE, osv


   ccd = os.ccd
   tele = os.tele
   camtable = os.camtable ; AEE 1/14/04
   iptable = os.iptable
   ip = os.ip
   ip(iptable).steps= -1
   ip(iptable).steps= steps ; to take care of multi-apid steps
   occ_blocks = os.occ_blocks
   roi_blocks = os.roi_blocks

   RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ; => xyblks  = 34 (blocks for a ccd size of 34*34=1156)
                                                 ; => xyblkpix= 64 (a block is 64*64 pixels)
                                                 ; => xypix   = 2176 (num pixels in x or y direction)
                                                 ; => xstart  = 1 (in cam tables x-ragne = 1-2176)
                                                 ; => ystart  = 1 (in cam tables y-ragne = 1-2176)
;   Jan 14, 2003:
;   Note: Header-Only can be ignored for the purposes of planning tool telemetry buffer usage, etc..
;         It is only 20 bytes and image is taken anyway (usually used in combination with other IP 
;         functions such as sum-buffer, etc.).

      ;** GET SIZE OF IMAGE ON CHI
      ;** we shouldn't take into account summing until we see which blocks are masked ??
      ;                         ((x2  -                      x1) +1) /                    xsum

      ; x and y range in camera table is 1-2176. Pick it up, get it within this range if not, and
      ; subtract 1 to make them 0-2175 needed by idl:

      IF (ccd(tele,camtable).x2 GE xstart) THEN BEGIN
         x1 = ( (ccd(tele,camtable).x1) > xstart < (xypix) ) -1 
         x2 = ( (ccd(tele,camtable).x2) > xstart < (xypix) ) -1 
         y1 = ( (ccd(tele,camtable).y1) > ystart  < (xypix) ) -1   
         y2 = ( (ccd(tele,camtable).y2) > ystart < (xypix) ) -1 

         ; Now x1,x2,y1,y2 are within 0-2175 range.
         
         xs = x2 - x1 + 1
         ys = y2 - y1  +1

         ;** CALCULATE TOTAL PIXELS IN IMAGE BEFORE PROCESSING AND BEFORE SUMMING


         IF (KEYWORD_SET(ccd_ro_pixels)) THEN BEGIN
           ;** ACCOUNT FOR SUMMING (CCD summing):
           sumfactor = (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
           numpixels_beforemask = LONG(xs)*ys 
           numpixels_beforemask = numpixels_beforemask / sumfactor
           RETURN, numpixels_beforemask 
         ENDIF

         ; See if IP-Trim function is present. IP-Trim removes 128 pixels from
         ; x-axis (50 underscan and 78 overscan pixels):
         ; Note: For SECCHI, number of columns, x, read from ccd camera table is
         ; always 1-2176 (0-2175) so xs is always 2176 and we trim it to 2048.
         ; For number of rows, y, trim it to 2048 only if orginal y from
         ; camera table > 2048:

         ;trim = WHERE(ip(iptable).steps EQ 68, trim_cnt) ; See if "IP-Trim" is selected
         ;trim = WHERE(steps EQ 68, trim_cnt) ; See if "IP-Trim" is selected
         trim = WHERE(steps EQ 76, trim_cnt) ; See if "IP-Trim" is selected

         IF (trim_cnt GT 0) THEN BEGIN
           xs= 2048 
           IF (ys GT 2048) THEN ys= 2048
         END
 
         ; Also is image is a HI summed image, make sure it is not more than 2048x2048
         ; since anything going to summing buffer is trimmed before being IPsummed to 1024x1024): 
         ind = WHERE(steps EQ 34 OR steps EQ 36 OR steps EQ 37 OR steps EQ 38 OR $
                     ;steps EQ 114 OR steps EQ 115, hscnt)
                     steps EQ 114 OR steps EQ 115 OR steps EQ 16 OR steps EQ 17, hscnt)

         IF (hscnt GT 0) THEN BEGIN
           xs= xs < 2048
           ys= ys < 2048 
         ENDIF

         ; Note: is it required that final image has x and y sizes each be
         ; multiples of 64 pixels. Check them and pad to be multiples of 64
         ; if needs be:

         IF ((xs MOD 64) GT 0) THEN xs= xs -(xs MOD 64) + 64
         IF ((ys MOD 64) GT 0) THEN ys= ys -(ys MOD 64) + 64
 
         ;** CALCULATE TOTAL PIXELS IN IMAGE BEFORE PROCESSING AND BEFORE SUMMING

         numpixels = LONG(xs)*ys

      ENDIF ELSE BEGIN
        PRINT,'ERROR: X2 < X_START ('+x2+' < '+x_start+').'
        STOP
      ENDELSE
      
      ;** ACCOUNT FOR BLOCKS MASKED OUT IN OCCULTER TABLE AND MASKING TABLE
      ;** If only underscan region or No Masking selected then skip this part


      ; AEE 5/5/04 - defuault for occ mask is to apply it:
      ;ind = WHERE(ip(iptable).steps EQ 28) ; 28 = don't use occ mask table. 
      ind = WHERE(steps EQ 28) ; 28 = don't use occ mask table. 
      IF (ind(0) GE 0) THEN useocc = 0 ELSE useocc = 1  ; Also see generate_ipt.pro
                                                        ;          generate_ipt.pro
                                                        ;          generate_ipt_set.pro
                                                        ;          os_summary.pro
                                                        ;          os_summary_1line.pro
                                                        ;          os_summary_set.pro
                                                        ;          os_summary_text.pro

      roi_table= WHICH_ROI_TABLE(ip,iptable)
      IF (roi_table EQ -1) THEN useroi = 0 ELSE useroi = 1
;help,roi_table

      IF ((useocc EQ 1) OR (useroi EQ 1)) THEN BEGIN

         ; Note: for secchi, each block (super pixel) is 64x64 pixels. So, there
         ;       are total of 1156 blocks per CCD for secchi:

         block1 = FIX(x1/xyblkpix)+FIX(y1/xyblkpix)*xyblks  ;block for lower left corner of selected pixels 
         block2 = FIX(x2/xyblkpix)+FIX(y2/xyblkpix)*xyblks  ;block for upper right corner of selected pixels 
         xs2 = x2 - x1 + 1  ; selected pixels in x-axis
         ys2 = y2 - y1 + 1  ; selected pixels in y-axis
      
         blocksx = ((xs2 + xyblkpix-1) / xyblkpix) < xyblks 
         blocksy = ((ys2 + xyblkpix-1) / xyblkpix) < xyblks 
         goodblocks = INDGEN(blocksx)+block1
         FOR row=block1/xyblks+1, (block1/xyblks)+blocksy-1 DO $
            goodblocks = [goodblocks,INDGEN(blocksx)+((row*xyblks)+(x1/xyblkpix))]

         ; AEE - 01/13/03: GOODBLOCKS      INT       = Array[1024] (0,1,2,.....,1022,1023)
         ; AEE - 05/24/04: GOODBLOCKS      INT       = Array[1156] (0,1,2,.....,1022,1155)
 
         allmasked_blocks = -1

         CASE (1) OF
            ((useocc EQ 1) AND (useroi EQ 0)) : allmasked_blocks = WHERE(occ_blocks(tele,*) GT 0)
            ((useroi EQ 1) AND (useocc EQ 0)) : allmasked_blocks = WHERE(roi_blocks(tele,*) GT 0)
            ((useroi EQ 1) AND (useocc EQ 1)) : allmasked_blocks = WHERE((occ_blocks(tele,*)+ $
                                                                         (roi_blocks(tele,*))) GT 0)
            ELSE : BEGIN
               WIDGET_CONTROL, info_text, SET_VALUE='Unknown Masking Type.'
               RETURN, -1
            ENDELSE
         ENDCASE
         loc = WHERE(allmasked_blocks GE 0)
         IF (loc(0) GE 0) THEN BEGIN
            allmasked_blocks = allmasked_blocks(loc)
            ;** can't mask a block twice
            allmasked_blocks = allmasked_blocks(SORT([allmasked_blocks]))
            allmasked_blocks = allmasked_blocks(UNIQ([allmasked_blocks]))

            ; AEE - 01/13/03 - If "Invert Mask Tables" is selected, use it to determine actual numpixels
            ; used for determining image size:
            ; *** NOTE: in .IPT file, occ_blocks and/or roi_blocks are used (using FIND_DATA_RUNS) to
            ;           get and print to the .IPT files the masked blocks used, if any. It also returns
            ;           selected image_processing functions including a 29 for "Invert Mask Tables" in the
            ;           PT_IP = line. If we want the .IPT file include the inverted-mask blocks instead of
            ;           the masked blocks, then we should find the imask (as done below) in the generate_ipt.pro,
            ;           as well. 

            
            ;ind = WHERE(ip(iptable).steps EQ 29)  ; See if "Invert Mask Tables" is selected 
            ind = WHERE(steps EQ 29)  ; See if "Invert Mask Tables" is selected 
            IF (ind(0) GE 0) THEN BEGIN
              imask= INTARR(N_ELEMENTS(xyblks*xyblks))  ; 34*34 = 1156 = max # of blocks 
              imask(allmasked_blocks)= 1
              imask= WHERE(imask EQ 0, imcnt)
              IF (imcnt GT 0) THEN BEGIN
                imask=goodblocks(imask)
                no_invert_allmasked_blocks= allmasked_blocks 
                allmasked_blocks= imask
              ENDIF ELSE allmasked_blocks= [-1]

            ENDIF 

            collisions = 0      ;** number of valid blocks masked out
            FOR i=0, N_ELEMENTS(allmasked_blocks)-1 DO BEGIN
               tmp = WHERE(allmasked_blocks(i) EQ goodblocks)
               IF (tmp(0) GE 0) THEN collisions = collisions + N_ELEMENTS(tmp)
            ENDFOR

            IF (DATATYPE(osv) NE 'UND' AND DATATYPE(osv) NE 'STR') THEN $ ; AEE 10/03/03 - added to handle osv='' 
                                                                          ; (set in define_os) when menus is closed.
              WIDGET_CONTROL,osv.info_text,SET_VALUE='Num Pixels Before Mask= '+STRN(numpixels)+ $
                                                      ', Masked Blocks (64x64 Pixels) = '+STRN(collisions)
;help,numpixels
            numpixels = numpixels - LONG(collisions)*xyblkpix*xyblkpix
;help,numpixels
;if roi_table gt 0 then stop
         ENDIF
      ENDIF

      ;** NOW ACCOUNT FOR SUMMING (CCD summing):
      sumfactor = (ccd(tele,camtable).xsum > 1) * (ccd(tele,camtable).ysum > 1)
      numpixels = numpixels / sumfactor

   RETURN, numpixels
END



FUNCTION GET_PIXSUM_COMP_PIXELS, pixels,steps, telescope, no_compress=no_compress

  numpixels = pixels  ; input pixels is what we have got after the occulter/roi masking and ccd summimg.

  ; See if "Pixel Sum" (same as LEB Sum) is selected (can be requested multiple times). For each 
  ; selection, apply a 2x2 summing:
 
;  ind = WHERE(steps EQ 3, ps_cnt)  ; See if "Pixel Sum" is selected.  

  ind = WHERE(steps EQ 3 OR steps EQ 52 OR steps EQ 53, ps_cnt)  ; See if "Pixel Sum" is selected.
;help,ps_cnt
  ind = WHERE(steps EQ 16 OR steps EQ 17, hisw_cnt) ; Each HI1SPW and HI2SPW = 2 pixelsum
;help,hisw_cnt
  ps_cnt= ps_cnt + 2*hisw_cnt
;help,ps_cnt
;stop

  FOR i= 0, ps_cnt -1 DO numpixels = numpixels / (2*2) 

  ; Anytime a HI1 or HI2 summing buffer is used, we must assume an implicit "2x2 Pixel Summing"
  ; So, for HI 1 or 2 summed sequences or an image going to a summing buffer, must assume an 
  ; implicit "Pixel Summing" (a full size image is summed to 1/4 (1024x1024):

  ;ind = WHERE(steps EQ 34 OR steps EQ 36 OR steps EQ 37 OR steps EQ 38, hscnt)
  ind = WHERE(steps EQ 34 OR steps EQ 36 OR steps EQ 37 OR steps EQ 38 OR $
              ;steps EQ 114 OR steps EQ 115, hscnt)
              steps EQ 114 OR steps EQ 115 OR steps EQ 16 OR steps EQ 17, hscnt) 

  IF (hscnt GT 0) THEN numpixels = numpixels / (2*2)  


 IF NOT (KEYWORD_SET(no_compress)) THEN BEGIN

  ; Now account for compression:
  ; steps 7-17 are Rice plus 10 H-compress:

  ; AEE - 01/13/03: Following comp_fac values are subject to change.
;  comp_fac= [2.3,     $ ; Rice                           
;             3.32,    $ ; H-compress - scale factor[0]
;             4.0,     $ ; H-compress - scale factor[1]
;             4.17,    $ ; H-compress - scale factor[2]
;             5.67,    $ ; H-compress - scale factor[3]
;             6.0,     $ ; H-compress - scale factor[4]
;             7.0,     $ ; H-compress - scale factor[5]
;             8.0,     $ ; H-compress - scale factor[6]
;             8.46,    $ ; H-compress - scale factor[7]
;             9.59,    $ ; H-compress - scale factor[8]
;             10.90]     ; H-compress - scale factor[9]


;  filename= GETENV('PT')+'/IN/OTHER/comp_factors.dat'   ; => comp_fac with for rice and h-compresses
;  OPENR, lu, filename, /GET_LUN
;  str = ''
;  comp_fac= [0.0]
;  WHILE NOT(EOF(lu)) DO BEGIN
;    READF, lu, str
;    ; throw away comment (;) and blank lines.
;    IF (STRMID(STRTRIM(str,2),0,1) NE ';' AND STRLEN(STRTRIM(str,2)) NE 0) THEN BEGIN
;      toks= STRTRIM(STR_SEP(str,'='),2)
;      comp_fac= [comp_fac,toks(1)]
;    ENDIF
;  ENDWHILE            
;  CLOSE, lu
;  FREE_LUN, lu
;  comp_fac= comp_fac(1:*)
;  ;IF (N_ELEMENTS(comp_fac) LT 11) THEN BEGIN
;  IF (N_ELEMENTS(comp_fac) LT 23) THEN BEGIN
;    PRINT,'ERROR - Missing compression factor(s) in '+filename
;    ;PRINT,'        11 (Rice+10Hcompress) factors are required.' 
;    PRINT,'        23 (Rice+10Hcompress+12Icer) factors are required.' 
;    STOP
;  ENDIF
;
;  comp_fac= [1.0,1.0,comp_fac] ; add "Header Only" and "NO Compression" value of 1.0 to the array. ; AEE 5/11/04 
;

  filename= GETENV('PT')+'/IN/OTHER/comp_factors.dat'   ; => comp_fac with for rice and h-compresses
  make_cf_arr, filename, cf_full,cf_ipsum

  n= N_ELEMENTS(steps)-1 ; last step is the compression used.

  IF (steps(n) EQ 44) THEN steps(n)= 6 ; treat "Raw Image" (step #44), if any, as "No Compression" (step #6): 

  ; Adjust Icer steps to correspond to elements of comp_fac array:

;  IF (steps(n) GE 82 AND steps(n) LE 93) THEN steps(n)= steps(n) - 69 + 5 ; Icers are elements 13 to 24 of comp_fac
                                                                          ; add 5 since it is taken out next.

  ; Icers are elements13 to 24 of comp_fac. Add 5 since it is taken out next:

  IF (steps(n) GE 90 AND steps(n) LE 101) THEN steps(n)= steps(n) - 77 + 5 

  ;numpixels= numpixels / comp_fac(steps(n)-5) ; "Header Only" (first of all compressions) is step # 5 

  sb= WHERE(steps EQ 37 OR steps EQ 38, sb_cnt)  ; See if summing-buffer is selected.
  
  ;IF (ps_cnt GT 0) THEN $
  IF (ps_cnt GT 0 OR sb_cnt GT 0) THEN $
    comp_fac= cf_ipsum(telescope,steps(n)-5) $
  ELSE $
    comp_fac= cf_full(telescope,steps(n)-5)
  numpixels= numpixels / comp_fac

 ENDIF ; no_compress keyword

  ; AEE 5/11/04:
  ; Note: For "Header Only" images, the image is taken and readout so we need to use the actual image
  ;       size to display it and account for conflicts. But, the image is NOT downliked (only the header
  ;       is) so we need to identify the Header Only images, later in schedule_plot, and do not add them to 
  ;       the specified channels (only header goes there and is very minumal and can be ignored).

   ; Assume a square root function (IP step # 2, 70, 71, or 96), if used, applies a compression factor of
   ; 3 to the image (says Dennis Wang - 10/05/05). it reduces numpixels which is used later to generate image
   ; size:

   ;sqind= WHERE(steps EQ 2 OR steps EQ 70 OR steps EQ 71 OR steps EQ 96, sqcnt)

   sqind= WHERE(steps EQ 2 OR steps EQ 78 OR steps EQ 79 OR steps EQ 104, sqcnt)
   IF (sqcnt GT 0) THEN numpixels= numpixels / (3.0^sqcnt)

   RETURN, numpixels

END



FUNCTION OS_GET_NUM_PIXELS, os, ccd_ro_pixels=ccd_ro_pixels, no_compress=no_compress
COMMON APIDS, multi_apid

  ; AEE 7/11/03:
  ; Get the pixels after occulter/roi masking and ccd summing, if any. Assume if any of
  ; these are found, they are applied only once to all of images for more than one APID
  ; using all of the processing steps that were selected: (We may have to change this
  ; to use portions of steps for each APID - sort of like the compressions/APID pairs). 

  iptable = os.iptable
  ip = os.ip

  IF (KEYWORD_SET(ccd_ro_pixels)) THEN BEGIN 
    ;pixels= GET_CCDSUM_MASK_PIXELS(os,/ccd_ro_pixels) 
    pixels= GET_CCDSUM_MASK_PIXELS(os,os.ip(iptable).steps,/ccd_ro_pixels) 
    RETURN, pixels
  ENDIF ELSE BEGIN
    ;pixels= GET_CCDSUM_MASK_PIXELS(os)
    ;comp_ind = WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 17 OR $
    ;comp_ind = WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 14 OR $
    comp_ind = WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 15 OR $
                     ip(iptable).steps GE 90 AND ip(iptable).steps LE 101 OR $
                     ip(iptable).steps EQ 44, comp_cnt) 
;help,comp_ind
;stop
;print,'****'
;print,ip(iptable).steps
;print,os.ip(iptable).steps

    IF ( comp_cnt EQ 0) THEN BEGIN  ; AEE 9/30/03
      PRINT,''
      PRINT,'ERROR: No one-to-one correspondance between APID and compression and/or APID'
      PRINT,'       does NOT appear somewhere before the compression for each pair.'
      PRINT,'       Also, compression must be the last step of the processing.
      PRINT,''
      STOP
    ENDIF
    all_numpixels= LONARR(comp_cnt)
    j=0
    FOR i= 0, comp_cnt-1 DO BEGIN
      steps= os.ip(iptable).steps(j:comp_ind(i))
      j= comp_ind(i)+1
      all_numpixels(i)= GET_CCDSUM_MASK_PIXELS(os,steps)
;help,comp_cnt,i
;print,os.os_num, os.iptable, steps
;print,all_numpixels
;print,'****'
;stop
    ENDFOR
  ENDELSE

  ; Now get the pixels after SEB (pixel) summing and the compressions BUT do it for
  ; each APID (and its corresponding compression) separately: 


;  Note: In os_tables.pro, I added code to make sure that for each APID there is a compression
;        (atleast one pair).

  ;iptable = os.iptable
  ;ip = os.ip

  ; AEE 5/11/04 - Also treat "Header Only" as compression (actually no compression):
  ;comp_ind = WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 17, comp_cnt) ; also treat "Header Only" as compression.

  ; AEE 7/13/04 - treat the new function "Send RAW Image" also as a compression (actually no compression):

;  comp_ind = WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 17 OR $
;                   ip(iptable).steps EQ 44, comp_cnt) 
;
;  IF ( comp_cnt EQ 0) THEN BEGIN  ; AEE 9/30/03
;    PRINT,''
;    PRINT,'ERROR: No one-to-one correspondance between APID and compression and/or APID'
;    PRINT,'       does NOT appear somewhere before the compression for each pair.'
;    PRINT,'       Also, compression must be the last step of the processing.
;    PRINT,''
;    STOP
;  ENDIF
;
;  all_numpixels= LONARR(comp_cnt)

  j=0
  FOR i= 0, comp_cnt-1 DO BEGIN
    steps= os.ip(iptable).steps(j:comp_ind(i))
    j= comp_ind(i)+1
    ;all_numpixels(i)= GET_PIXSUM_COMP_PIXELS(pixels,steps)
    ;all_numpixels(i)= GET_PIXSUM_COMP_PIXELS(all_numpixels(i),steps)
    IF (KEYWORD_SET(no_compress)) THEN BEGIN
      all_numpixels(i)= GET_PIXSUM_COMP_PIXELS(all_numpixels(i),steps,os.tele,/no_compress)
    ENDIF ELSE BEGIN
      all_numpixels(i)= GET_PIXSUM_COMP_PIXELS(all_numpixels(i),steps,os.tele)
    ENDELSE
  ENDFOR

 RETURN, all_numpixels

END
