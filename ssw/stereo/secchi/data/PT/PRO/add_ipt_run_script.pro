;
;+
;$Id: add_ipt_run_script.pro,v 1.1 2005/05/26 20:00:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ADD_IPT_RUN_SCRIPT
;               
; Purpose     : Adds the "Run Script" section of .IPT file to the variables in
;               the RUN_SCRIPT common block.
;               
; Use         : ADD_IPT_RUN_SCRIPT, key_val_arr 
;    
; Inputs      : key_val_arr  Contains run script information picked up from .IPT file. 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None 
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, May 2005 - First Version. 
;               
; Modification History:
;
; $Log: add_ipt_run_script.pro,v $
; Revision 1.1  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
;
;-

PRO ADD_IPT_RUN_SCRIPT,str_arr
COMMON RUN_SCRIPT, rsc

  sdate = STRTRIM(str_arr(3).val,2)
  sdate = STRMID(sdate,1,STRLEN(sdate)-2) ; remove '(' and ')' from the ends.
  ssc = STRTRIM(str_arr(4).val,2)
  rsfn= STRTRIM(str_arr(5).val,2)
  bsf_cnt= FIX(str_arr(6).val)

  sdate= STRMID(sdate,0,4)+'/'+STRMID(sdate,4,2)+'/'+STRMID(sdate,6,2)+' '+ $
           STRMID(sdate,9,2)+':'+STRMID(sdate,11,2)+':'+STRMID(sdate,13,2)

  rsc= [rsc,{sc:ssc,dt:UTC2TAI(STR2UTC(sdate)),fn:rsfn,bsf:bsf_cnt}]

  RETURN
END

