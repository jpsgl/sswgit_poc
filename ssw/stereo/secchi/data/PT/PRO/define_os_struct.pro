;+
;$Id: define_os_struct.pro,v 1.3 2005/01/24 17:56:32 esfand Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : DEFINE_OS_STRUCT 
;
; Purpose     : Defines the Observation Sequence structure for SECCHI Schedule
;               and returns it.
;
; Use         : os_struct = DEFINE_OS_STRUCT() 
;
; Inputs      : None. 
;
; Opt. Inputs : None.
;
; Outputs     : os_struct   Structure to hold an instance of a Observing Sequence. 
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : None. 
;
; Written by  :  Ed Esfandiari, NRL, June 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/22/04 - Added SpaceCraft (sc) tag to os_struct.
;              Ed Esfandiari 10/15/04 - Added bsf field.
;
;$Log: define_os_struct.pro,v $
;Revision 1.3  2005/01/24 17:56:32  esfand
;checkin changes since SCIP_A_TVAC
;
;Revision 1.1.1.1  2004/07/01 21:19:12  esfand
;first checkin
;
;
;-
;

FUNCTION DEFINE_OS_STRUCT

   os_struct= {os_struct,os_num:0L,sc:'',set_id:0L,set_cnt:0,bsf:'',os_start:0.0D,os_stop:0.0D, $
               os_size:0L, os_duration:0.0D, os_pre_proc_time:0.0D, os_ro_time:0.0D, os_setup_time:0.0D,  $
               os_tele:0, os_lp:0, os_images:0, os_cadence: 0.0, os_proc_time:0.0D }

  RETURN, os_struct
 
END
