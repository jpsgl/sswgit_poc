;
;+
;$Id: add_ipt_init_tl.pro,v 1.2 2005/05/26 20:00:58 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : ADD_IPT_INIT_TL
;               
; Purpose     : Adds the "Initialize Timeline" section of .IPT file to the variables in
;               the TIMELINE common block.
;               
; Use         : ADD_IPT_INIT_TL, key_val_arr 
;    
; Inputs      : key_val_arr  Contains Init Timeline information picked up from .IPT file. 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None 
;
; Keywords    : 
;
; Written by  :  Ed Esfandiari, NRL, Feb 2005 - First Version. 
;               
; Modification History:
;                 Ed Esfandiari 05/10/05 - allow multiple Init Timeline commands.
;
; $Log: add_ipt_init_tl.pro,v $
; Revision 1.2  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.1  2005/03/10 16:40:48  esfand
; first version
;
;
;-

PRO ADD_IPT_INIT_TL,str_arr
COMMON INIT_TL, ic

  ;nsched = FIX(STRTRIM(str_arr(2).val,2)) ; nsched is always one
  sdate = STRTRIM(str_arr(3).val,2)
  sdate = STRMID(sdate,1,STRLEN(sdate)-2) ; remove '(' and ')' from the ends.
  ssc = STRTRIM(str_arr(4).val,2)

  sdate= STRMID(sdate,0,4)+'/'+STRMID(sdate,4,2)+'/'+STRMID(sdate,6,2)+' '+ $
           STRMID(sdate,9,2)+':'+STRMID(sdate,11,2)+':'+STRMID(sdate,13,2)

  bsf_cnt= FIX(str_arr(5).val)
  ic= [ic,{sc:ssc,dt:UTC2TAI(STR2UTC(sdate)),bsf:bsf_cnt}]

  RETURN
END

