;+
;$Id: expand_seq.pro,v 1.5 2005/03/10 16:41:44 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : EXPAND_SEQ 
;
; Use         : expanded_seq= EXPAND_SEQ(os_arr)
;               
; Purpose     : expand the HI and SCIP sequences into individual instances.
;               
; Explanation : This function expands each HI and SCIP sequence of images into 
;               individual instances. The expanded array will be returned.
;               
; Inputs      : os_arr         Contains OSes already scheduled.
;
; Opt. Inputs : None
;               
; Outputs     : expanded_seq   Expanded array of individual instances of the sequence. 
;               
; Opt. Outputs: None
;               
; Keywords    : None
;
; Called From : SCHEDULE_EVENT
;
; Category    : Planning, Scheduling.
;               
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/14/04 - use proc_time directly.
;              Ed Esfandiari 06/18/04 - For a SCIP seq, expand the images pre_proc_times using
;                                       actual exptimes of each image.
;              Ed Esfandiari 07/16/04 - Added filter/polar dependent setup time for SCIP seq. images.
;              Ed Esfandiari 08/16/04 - Use physical exptime units of sec (from read_exp_tables).
;              Ed Esfandiari 12/13/04 - use pre_proc_times from os_get_pre_proc_time that has CCD summing
;                                       corrected exptimes instead of using ex. 
;              Ed Esfandiari 01/28/05 - Added code to calculate image processing times for HI summed seq
;                                       images using real table steps for first, middle, and last images.
;
; $Log: expand_seq.pro,v $
; Revision 1.5  2005/03/10 16:41:44  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:42  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

FUNCTION EXPAND_SEQ, os_arr 
COMMON OS_DEFINED
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
                      ip_arr, plan_lp, lprow, cor1pw, cor2pw

   expand_os_arr= os_arr(0)
   FOR i= 0, N_ELEMENTS(os_arr)-1 DO BEGIN
     IF (os_arr(i).os_images EQ 1) THEN BEGIN   ; a single image
       expand_os_arr= [expand_os_arr, os_arr(i)]
     ENDIF ELSE BEGIN ; a Seq of images
       os= os_arr(i)
       nimages= os_arr(i).os_images
       os.os_images= 1
       pre_proc_times= os_arr(i).os_pre_proc_time/nimages

       IF (os.os_lp EQ 5) THEN BEGIN ; SCIP seq
         ; For a SCIP seq, since polar positions may be different, the exptimes 
         ; may also be different resulting in different pre_proc_time for different
         ; images in the seq (also done in schedule_plot.pro):
         tel= os.os_tele
         ind= WHERE(defined_os_arr.os_num EQ os.os_num AND defined_os_arr.num_images EQ nimages)
         exptab= defined_os_arr(ind(0)).exptable
         fw= defined_os_arr(ind(0)).fw(0)
         pw= defined_os_arr(ind(0)).pw(0:nimages-1)
         ;;exptimes= defined_os_arr(ind(0)).ex(tel,exptab,fw,pw)/1024.0 ; change units to SCIP seconds.
         ;exptimes= defined_os_arr(ind(0)).ex(tel,exptab,fw,pw)/1000.0  ; units are physical seconds.
         ;a_pre_proc_time= os.os_setup_time/nimages ;pre_proc_ime per image minus exptime. 
         ;pre_proc_times= exptimes + a_pre_proc_time
         ; for a SCIP seq, since setup_time for each image may be different, generate them separately:
         setup_times= FLTARR(nimages) 
         pre_proc_times= FLTARR(nimages)
         ;pre_time= OS_GET_PRE_PROC_TIME(defined_os_arr(ind(0)),0,ro_time,setup_time,fil_time,pol_time)
         pre_proc_times(0)= OS_GET_PRE_PROC_TIME(defined_os_arr(ind(0)),0,ro_time,setup_time,fil_time,pol_time)
         setup_times(0)= setup_time
         FOR j= 1, nimages-1 DO BEGIN
           ;pre_time= OS_GET_PRE_PROC_TIME(defined_os_arr(ind(0)),j,ro_time,setup_time,fil_time,pol_time)
           pre_proc_times(j)= OS_GET_PRE_PROC_TIME(defined_os_arr(ind(0)),j,ro_time,setup_time,fil_time,pol_time)
           setup_times(j)= setup_time - fil_time 
           IF (pw(j) EQ pw(j-1)) THEN setup_times(j)= setup_time - pol_time 
         ENDFOR
         ;pre_proc_times= exptimes + setup_times
       ENDIF ELSE BEGIN ;  A HI Seq
         ; All exptimes (hence pre_proc_times) are the same:
         pre_proc_times= REPLICATE(pre_proc_times,nimages)
       ENDELSE

; Note: Now only first image of a SCIP seq and all of SCIP images that are not sequences have
;       polar/quad_selector and filter (EUVI) times added to the setup time. In the other words,
;       We assume a filter/plor change for each non-sequence scheduled image and the first image
;       of SCIP seq. Note that HI telescopes don't use filter/polar so they are ok. Also, Images
;       2-N of a SCIP seq only have polar/quad time added if there was a movement.
;       Done in get_os_db_stats.pro, schedule_plot.pro, expand_seq.pro.

  
       ;os.os_ro_time= os_arr(i).os_ro_time/nimages
       img_ro_time1= os_arr(i).os_ro_time/nimages 
       os.os_ro_time= img_ro_time1
       img_proc1= os_arr(i).os_proc_time/nimages
       os.os_size= os_arr(i).os_size/nimages
       ;os.os_duration= os_arr(i).os_duration/nimages
       os.os_cadence= os_arr(i).os_cadence
       start= os.os_start

       ; See if HI seq is a summed seq (only an HI summed seq will have different steps in ip and ipd):
       tind= WHERE(os.os_num EQ defined_os_arr.os_num AND os.os_tele EQ defined_os_arr.tele)
       def_os= defined_os_arr(tind)
       iptable= def_os.iptable
       tmp= WHERE(ip(iptable).steps NE ipd(iptable).steps, nind)

       FOR j= 0, nimages-1 DO BEGIN

         IF (nind GT 0) THEN BEGIN
           IF (j EQ 0) THEN BEGIN
             ; First image's table
             def_os.ip(iptable).steps= ipd(iptable).steps(2:*)
           ENDIF
           IF (j GT 0 AND j LT def_os.num_images-1) THEN BEGIN
             ; Middle image's table
             desc= ip_arr(ipd(iptable).steps(0)).ip_description
             table_num= FIX(STRMID(desc,STRPOS(desc,'Table ')+6,2))
             def_os.ip(iptable).steps= ipd(table_num).steps
           ENDIF
           IF (j EQ def_os.num_images-1) THEN BEGIN
             ; Last image's table
             desc= ip_arr(ipd(iptable).steps(1)).ip_description
             table_num= FIX(STRMID(desc,STRPOS(desc,'Table ')+6,2))
             def_os.ip(iptable).steps= ipd(table_num).steps
           ENDIF
           img_proc1= GET_IP_TIME(def_os)
         ENDIF

         os.os_pre_proc_time= pre_proc_times(j)
         os.os_duration= pre_proc_times(j)+img_ro_time1+img_proc1
;         os.os_start= start + os.os_cadence * j
         IF (j EQ 0) THEN BEGIN 
           os.os_start= start 
         ENDIF ELSE BEGIN
           IF (os.os_cadence EQ 0.0) THEN $
             os.os_start= start + TOTAL(pre_proc_times(0:j-1)) + os.os_ro_time * j $
           ELSE $
             os.os_start=  start + os.os_cadence * j
         ENDELSE
         os.os_stop= os.os_start+os.os_pre_proc_time+os.os_ro_time+img_proc1
         expand_os_arr= [expand_os_arr, os] 

;print,'x1,x2,ccdx1,ccdx2=',os.os_start-start,os.os_stop-start,os.os_start-start+os.os_pre_proc_time,$
;      os.os_start-start+os.os_pre_proc_time+os.os_ro_time

       ENDFOR
     ENDELSE
   ENDFOR

   expand_os_arr= expand_os_arr(1:*)

   RETURN,expand_os_arr
END
