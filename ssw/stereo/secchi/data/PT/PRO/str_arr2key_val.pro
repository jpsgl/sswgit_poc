;+
;$Id: str_arr2key_val.pro,v 1.1.1.2 2004/07/01 21:19:11 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : STR_ARR2KEY_VAL
;               
; Purpose     : This function converts an array of strings (i.e 'keyword = value')
;               to an array of structures of strings ({key,val}).
;               
; Use         : key_val = STR_ARR2KEY_VAL(str_arr)
;    
; Inputs      : str_arr    An array of strings of type 'keyword = value'. 
;
; Opt. Inputs : None
;               
; Outputs     : key_val   An array of structures of strings ({key,val}). 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: str_arr2key_val.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:37  esfand
; first checkin
;
;
;-

FUNCTION STR_ARR2KEY_VAL, str_arr
  COMMON DIALOG, mdiag,font

   key_val_struct = {key_val_struct, key:'', val:''}

   len = N_ELEMENTS(str_arr)
   key_val_arr = REPLICATE(key_val_struct, len)

   j=0
   FOR i=0, len-1 DO BEGIN
      line = str_arr(i)
      pos = STRPOS(line, '=')
      IF (pos LE 0) THEN BEGIN 
         WIDGET_CONTROL,mdiag,SET_VALUE='%%STR_ARR2KEY_VAL: Illegal element of str_arr: no = in line:'+STRN(line)
         PRINT, '%%STR_ARR2KEY_VAL: Illegal element of str_arr: no = in line:'+line 
      ENDIF ELSE BEGIN
         key = STRUPCASE(STRTRIM(STRMID(line, 0, pos), 2))
         val = STRTRIM(STRMID(line, pos+1, STRLEN(line)-pos+1), 2)
         IF (key EQ '') THEN BEGIN 
            WIDGET_CONTROL,mdiag,SET_VALUE='%%STR_ARR2KEY_VAL: Illegal element of str_arr: no = in line:'+STRN(line)
            PRINT, '%%STR_ARR2KEY_VAL: Illegal element of str_arr: no keyword in line:'+line 
         ENDIF ELSE IF (val EQ '') THEN BEGIN 
            WIDGET_CONTROL,mdiag,SET_VALUE='%%STR_ARR2KEY_VAL: Illegal element of str_arr: no = in line:'+STRN(line)
            PRINT, '%%STR_ARR2KEY_VAL: Illegal element of str_arr: no value in line:'+line 
         ENDIF ELSE BEGIN
            key_val_arr(j).key = key
            key_val_arr(j).val = val
            j = j+1
         ENDELSE
      ENDELSE
   ENDFOR

   RETURN, key_val_arr(0:j-1)

END
