;+
;$Id: os_summary.pro,v 1.14 2009/09/11 20:28:15 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : OS_SUMMARY
;               
; Purpose     : This routine looks up os_num in the definded_os_arr and displays
;               a widget of summary information. 
;               
; Use         : OS_SUMMARY, os_num, caller
;    
; Inputs      : os_num   Observation sequence number. 
;               caller   Structure containing id of caller. 
;
; Opt. Inputs : None
;               
; Outputs     : None 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/02/04 - Corrected LP display.
;              Ed Esfandiari 07/09/04 - Keep original ex and ccd values around.
;              Ed Esfandiari 07/13/04 - Use 4 ROI tables instead of 1.
;              Ed Esfandiari 08/31/04 - Changed format of exptime reports.
;              Ed Esfandiari 09/20/04 - Added fps and sync to output.
;              Ed Esfandiari 09/30/04 - Removed sync and added S/C.
;              Ed Esfandiari 12/13/04 - Report non cal image exptimes corrected for CCD summing.
;              Ed Esfandiari 01/10/05 - Added lowgain exptime correction.
;              Ed Esfandiari 05/19/05 - Display occ and roi of first image for multiple
;                                       destinations.
;              Ed Esfandiari 05/24/05 - Added ccd orientation dependancy for masks.
;              Ed Esfandiari 07/26/05 - Checked os_arr datatype before use.
;              Ed Esfandiari 10/06/05 - Added ICER compressions (IP steps #82 to 93).
;              Ed Esfandiari 11/09/05 - icer functions changed to steps #90 to 101.
;              Ed Esfandiari 11/21/05 - Added checks for steps 114 and 115.
;              Ed Esfandiari 05/10/06 - Use vaild HI1 or HI2 sequence info
;              Ed Esfandiari 08/22/06 - Added new capacity and rates (channels)
;              Ed Esfandiari 08/22/06 - Added RETAIN=2 to draw widget.
;              Ed Esfandiari 08/30/06 - use rtrate from common block.
;              Ed Esfandiari 11/21/06 - Ignore ccd_sum and low gain exptime correction (FSW does not do it).
;              Ed Esfandiari 12/14/07 - Allow occuter block_str assignement for SC-AB quick-summary displays.
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15.
;
;
;
; $Log: os_summary.pro,v $
; Revision 1.14  2009/09/11 20:28:15  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.9  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.8  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.7  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.6  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.4  2004/09/01 15:40:45  esfand
; commit new version for Nathan.
;
; Revision 1.3  2004/07/12 13:18:47  esfand
; Keep original ex and ccd values around
;
; Revision 1.2  2004/06/03 12:28:22  esfand
; Corrected LP Display
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;
PRO OS_SUMMARY_FILL, os_num, ind 

COMMON OS_SUMMARY_SHARE, os_sumv
COMMON OP_SCHEDULED
COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
   ;ip_arr, plan_lp, lprow
   ip_arr, plan_lp, lprow, cor1pw, cor2pw ; AEE - 12/29/03
COMMON LP_CAL_LAMP_SHARE, lpcalv ; AEE - 08/20/03
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld ; AEE 5/21/04 - need ipd
COMMON RT_CHANNEL, rt_rates, rtrate

ip_orig= ip ;ip is overwriteen by defined_os_arr.ip in these routine. keep it and set it back at the end.
ccd_orig= ccd
ex_orig= ex
   

   IF (SEXIST(tele_types) EQ 0) THEN OS_INIT	;** define some variables

   os = defined_os_arr(ind)
   lp = defined_os_arr(ind).lp
   tele = defined_os_arr(ind).tele
   fw = defined_os_arr(ind).fw
   pw = defined_os_arr(ind).pw
   exptable = defined_os_arr(ind).exptable ; AEE 1/14/04
   camtable = defined_os_arr(ind).camtable ; AEE 1/14/04
   fps = defined_os_arr(ind).fps ; AEE 1/14/04
   iptable = defined_os_arr(ind).iptable
   lamp = defined_os_arr(ind).lamp
   sub = defined_os_arr(ind).sub
   start = defined_os_arr(ind).start
   num_images = defined_os_arr(ind).num_images 
   ccd = defined_os_arr(ind).ccd
   ip = defined_os_arr(ind).ip
   ex = defined_os_arr(ind).ex
   occ_blks = REFORM(defined_os_arr(ind).occ_blocks(tele,*))
   roi_blks = REFORM(defined_os_arr(ind).roi_blocks(tele,*))

   ;fst_comp= WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 17 OR $
   ;fst_comp= WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 14 OR $
   fst_comp= WHERE(ip(iptable).steps GE 5 AND ip(iptable).steps LE 15 OR $
                   ip(iptable).steps GE 90 AND ip(iptable).steps LE 101 OR $
                   ip(iptable).steps EQ 44, comp_cnt)
   fst_comp= fst_comp(0)

    ; AEE 5/5/04:
    ;indm = WHERE(ip(iptable).steps EQ 28) ; 28 = don't use occ mask table. 
    indm = WHERE(ip(iptable).steps(0:fst_comp) EQ 28) ; 28 = don't use occ mask table. 
    IF (indm(0) GE 0) THEN useocc = 0 ELSE useocc = 1  ; Also see os_get_num_pixels.pro
                                                      ;          generate_ipt.pro
                                                      ;          generate_ipt_set.pro
                                                      ;          os_summary.pro
                                                      ;          os_summary_1line.pro
                                                      ;          os_summary_set.pro
                                                      ;          os_summary_text.pro

   ;indm = WHERE(ip(iptable).steps EQ 25)
   ;IF (indm(0) LT 0) THEN useroi = 0 ELSE useroi = 1

   
   roi_table= WHICH_ROI_TABLE(ip,iptable)

   rnd = WHERE(ip(iptable).steps(0:fst_comp) EQ 25 OR $   ; step #25 = "Use ROI1 table"
               ip(iptable).steps(0:fst_comp) EQ 45 OR $   ; step #45 = "Use ROI2 table"
               ip(iptable).steps(0:fst_comp) EQ 46 OR $   ; step #46 = "Use ROI3 table"
               ip(iptable).steps(0:fst_comp) EQ 47, rcnt) ; step #47 = "Use ROI4 table"
   IF (rcnt EQ 0) THEN roi_table= -1

   IF (roi_table EQ -1) THEN useroi = 0 ELSE useroi = 1

   sc= '?'
   IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
     osind= WHERE(os_arr.os_num EQ os_num, ocnt)
     IF (ocnt GT 0) THEN sc= os_arr(osind(0)).sc
     IF (sc EQ 'AB') THEN sc= 'A'
   ENDIF

   telid= ['EUVI','COR1','COR2','HI1','HI2']
   telstr= telid(tele) 

   tsc= sc
   IF (sc EQ '?') THEN sc= 'A' 

   CASE (1) OF
      ((useocc EQ 1) AND (useroi EQ 0)) : BEGIN
         blocks = WHERE(occ_blks GT 0,num)
         blockstr = 'Occulter (' + STRTRIM(num,2) + ' blocks)'
         ;blocks= MODIFY_MASK_ORDER(sc,telstr,occ_blks,/TABLE2IDL)
         ;blocks= WHERE(blocks GT 0,num)
      END
      ((useroi EQ 1) AND (useocc EQ 0)) : BEGIN
         blocks = WHERE(roi_blks GT 0,num)
         blockstr = 'ROI (' + STRTRIM(num,2) + ' blocks)'
         ;blocks= MODIFY_MASK_ORDER(sc,telstr,roi_blks,/TABLE2IDL)
         ;blocks= WHERE(blocks GT 0,num)
      END
      ((useroi EQ 1) AND (useocc EQ 1)) : BEGIN
         blocks = WHERE((occ_blks+roi_blks) GT 0,num)
         blockstr = 'Occulter & ROI (' + STRTRIM(num,2) + ' blocks)'
         all_blks= (occ_blks+roi_blks) < 1
         
         blocks= MODIFY_MASK_ORDER(sc,telstr,all_blks,/TABLE2IDL)
         blocks= WHERE(blocks GT 0,num)
;help,blocks,num,all_blks
;stop
      END
      ELSE : BEGIN
         blocks = -1
         blockstr = 'None'
      END
   ENDCASE
sc= tsc

   tind = 0
   add = 92	

   str = STRTRIM(STRING(os_num),2)
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='                    OS_NUM = ' + str

   tind = tind+1
   str = STRTRIM(STRING(lp),2)+', '+STRTRIM(op_types(lp),2)

   IF (num_images GT 1) THEN BEGIN
     IF (LP EQ 6) THEN $ ; HI Seq
       str = str + ' ('+STRTRIM(num_images,2)+' Images)' $
     ELSE $ ; SEQ PW
       str = str + ' ('+STRTRIM(num_images,2)+' Steps)' 
   ENDIF 
   IF (num_images EQ 1 AND LP EQ 6) THEN str = str + ' ('+STRTRIM(num_images,2)+' Image)'

   lamp_used=''
   IF (lp eq 3 AND DATATYPE(lpcalv) NE 'UND') THEN BEGIN ; AEE 8/20/03
     CASE tele of
        0: lamp_used= ' (' +lpcalv.euvi_lamps(lamp)+')'
        1: lamp_used= ' (' +lpcalv.cor1_lamps(lamp)+')'
        2: lamp_used= ' (' +lpcalv.cor2_lamps(lamp)+')'
        3: lamp_used= ' (' +lpcalv.hi1_lamps(lamp)+')'
        4: lamp_used= ' (' +lpcalv.hi2_lamps(lamp)+')'
     ENDCASE
   ENDIF
   
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='LP = ' + str + lamp_used
   tind = tind+1
 
   IF (lp EQ 7) THEN BEGIN ;** Block Seq ; AEE - Dec 03, 2002
      str = STRTRIM(STRING(start),2)
      WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Start Index = ' + str
      tind = tind+1
      str = STRTRIM(STRING(num_images),2)
      WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Stop Index = ' + str
      tind = tind+1
      GOTO, CONT
   ENDIF

   str = STRMID(tele_types(tele),11,4)
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Telescope = ' + str
   tind = tind+1
   str = STRTRIM(exptable+1,2) ; AEE 1/14/04
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Exposure DPT = ' + str
   tind = tind+1
   str = STRTRIM(camtable+1,2) ; AEE 1/14/04
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Camera DPT = ' + str
   tind = tind+1
   ;str = STRTRIM(STRMID(fw_types(exptable,fw,tele),0,13), 2) ; AEE 4/5/04
   str = FILTER2STR(fw_types,exptable,fw,tele)
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Filter = ' + str
   tind = tind+1

      IF (lp EQ 14) OR (lp EQ 11) THEN BEGIN          ;** FP WL Scan, output wl/exp table instead of pw/exp pairs

      ENDIF ELSE BEGIN                  ;** output pw/exp pairs

         IF (tele EQ 0) THEN str0 = 'Sector = ' ELSE str0 = 'Polarizer = '  ; AEE - Nov 26, 02
         str1 = ''
         str2 = ''

         ; For non cal images, report exptime that is corrected for CCD summing:

         ccdsum= 1
         IF (lp NE 2 AND lp NE 3) THEN BEGIN
           xs = ccd(tele,camtable).xsum
           ys = ccd(tele,camtable).ysum
           ccdsum= ((xs > 1) * (ys > 1))
           IF (ccd(tele,camtable).gmode EQ 1) THEN ccdsum= ccdsum/4.0 ; for low gain, amplify exptime by 4
         ENDIF

         ccdsum=1 ; Ignore ccd_sum and low gain exptime correction (FSW does not do it).

         FOR n=0, num_images -1 DO BEGIN ;** for each pw position (for no seq-images, n=0 always)
            IF(tele LE 2) THEN BEGIN
              str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(n),tele),0,10),2) + '  ' ; AEE 4/5/04
              IF (lp EQ 4) THEN $  ; AEE - 5/21/03
                str2 = '0 ' $
              ELSE $
                str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(n))/ccdsum,FORMAT='(I7)'),2) + '  '
              IF (lp EQ 0) THEN BEGIN  ; AEE - 9/24/03
                ; For double sky images, add the second pw and exp.time (for these images n is always 0)
                ; and pw(1) indicates the second pw positions and exptime to be used.
                str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(1),tele),0,10),2) + '  ' ; AEE 4/5/04
                str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(1))/ccdsum,FORMAT='(I7)'),2) + '  ' 
              ENDIF
            ENDIF ELSE BEGIN  ; HI has no polar wheel (and pw= array of 20 for now)  ; AEE - 01/23/03
              str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(0),tele),0,10),2) + '  ' ; AEE 4/5/04
              IF (lp EQ 4) THEN $  ; AEE - 5/21/03
                str2 = str2 + '0 ' $
              ELSE $
                str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2) + '  '
              IF (lp EQ 0) THEN BEGIN
                ; For double sky images, add the second pw and exp.time (for these images n is always 0)
                ; and since it is a HI image, the second pw and, therefore, exp.time are the same as the
                ; first.
                str1 = str1 + STRTRIM(STRMID(pw_types(exptable,pw(0),tele),0,10),2) + '  ' ; 4/5/04
                str2 = str2 + STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2) + '  ' 
              ENDIF
            ENDELSE
         ENDFOR

         IF (STRTRIM(str1,2) EQ '') THEN str1= 'None'

         WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE=str0+str1
         tind = tind+1
         IF (lp eq 3) THEN $
           WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Pulse = ' + str2 $
         ELSE $
           WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Exptime = ' + str2
         tind = tind+1

      ENDELSE

   ccd1 = ccd(tele,camtable)
   str1 = 'x1='+STRING(ccd1.x1, FORMAT='(i4)') + $
              ', x2='+STRING(ccd1.x2, FORMAT='(i4)') + $
              ', xsum='+STRING(ccd1.xsum, FORMAT='(i2)')
   str2 = 'y1='+STRING(ccd1.y1, FORMAT='(i4)') + $
              ', y2='+STRING(ccd1.y2, FORMAT='(i4)') + $
              ', ysum='+STRING(ccd1.ysum, FORMAT='(i2)')
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='CCD: ' + str1
   tind = tind+1
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='     ' + str2
   tind = tind+1
   gmode= 'High'
   IF (ccd1.gmode EQ 1) THEN gmode='Low'
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='     gain= '+gmode
   tind = tind+1

   good = WHERE(ip(iptable).steps GT 0, nip)

   iptab_str= STRTRIM(STRING(iptable),2)

   ; AEE 5/21/04:
   ;hi1_ind= WHERE(ipd(iptable).steps EQ 33 OR ipd(iptable).steps EQ 34 OR ipd(iptable).steps EQ 5 , hi1cnt)
   ;hi2_ind= WHERE(ipd(iptable).steps EQ 35 OR ipd(iptable).steps EQ 36 OR ipd(iptable).steps EQ 5 , hi2cnt)
   hi1_ind= WHERE(ipd(iptable).steps EQ 33 OR ipd(iptable).steps EQ 34 OR ipd(iptable).steps EQ 5 OR $
                  ipd(iptable).steps EQ 114, hi1cnt)
   hi2_ind= WHERE(ipd(iptable).steps EQ 35 OR ipd(iptable).steps EQ 36 OR ipd(iptable).steps EQ 5 OR $
                  ipd(iptable).steps EQ 115, hi2cnt)
   IF (hi1cnt EQ 3 OR hi2cnt EQ 3) THEN BEGIN 
     toks= STR_SEP(ip_arr(ipd(iptable).steps(0)).ip_description,' ') ; brakeup 'Use IP Table ?? (for ....)'
     t2= STRTRIM(toks(3),2) ; pickup 2nd table #
     toks= STR_SEP(ip_arr(ipd(iptable).steps(1)).ip_description,' ') ; brakeup 'Use IP Table ?? (for ....)'
     t3= STRTRIM(toks(3),2) ; pickup 3rd table #
     iptab_str= iptab_str + ',' + t2 + ',' + t3 
   END

   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Image Proc Table = '+ iptab_str

   tind = tind+1
   FOR n=0, nip-1 DO BEGIN
      str0= '   '
      str = STRTRIM(ip_arr(ip(iptable).steps(good(n))).ip_description,2) ; AEE 5/5/04
      WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE= str0+str
      tind = tind+1
   ENDFOR

   str = GET_COMP_STR(os)
   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Compression String = ' + str
   tind = tind+1

   WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Masking = ' + blockstr
   tind = tind+1

   IF (tele EQ 0) THEN BEGIN
     str= 'Off'
     IF (fps EQ 1) THEN str= 'On'
     WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='FPS = '+ str
     tind = tind+1
   ENDIF

CONT:

   IF (DATATYPE(os_arr(0)) NE 'INT') THEN BEGIN
   ind2 = WHERE(os_arr.os_num EQ os_num AND os_arr.os_tele EQ tele)
   IF (ind2(0) GE 0) THEN BEGIN
      ;str = STRTRIM(os_arr(ind2(0)).os_size,2) ; size of a single schedule os
      str = STRTRIM(os_arr(ind2(0)).os_size/8,2) + ' Bytes' ; size of a single schedule os
      WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='OS_SIZE = ' + str
      tind = tind+1
      num = N_ELEMENTS(ind2)
      str = STRTRIM(num, 2)
      WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='# Scheduled = ' + str
      tind = tind+1


      str= ARR2STR(os_arr(ind2).sc)
        
      WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='S/C = '+ str
      tind = tind+1

      
      

      hdr_only= WHERE(ip(iptable).steps(good) EQ 5, hcnt)
      IF (hcnt GT 0) THEN BEGIN
        WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Download Time = ' + 'N/A (Header-Only)'
        tind = tind+1
      ENDIF ELSE BEGIN
        dest= WHERE(ip(iptable).steps(good) GE 39 AND ip(iptable).steps(good) LE 42, dcnt)
        ;RESTORE, GETENV('PT')+'/IN/OTHER/'+'secchi_downlink_channels.sav'
        channels= READ_SSR_INFO() 
        ;RESTORE, GETENV('PT')+'/IN/OTHER/'+'rt_channel.sav'
        tsize= TOTAL(os_arr(ind2).os_size)
        daily=    24.0 * 3600.0  ; 86400.0 seconds
        second= 1.0              ; 1 second -  Use to report seconds it takes to downlink the image(s). 

        FOR id=0, dcnt-1 DO BEGIN
          CASE ip(iptable).steps(good(dest(id))) OF 
            39: BEGIN ; SSR1RT 
              str= [STRING(tsize / (second * channels.ssr1_rate)+1,FORMAT='(I7)')+' Secs (SSR1)', $
                    ;STRING(tsize / (second * (rt_rate*272.0*8.0))+1,FORMAT='(I7)')+' Secs (RT)'] 
                    STRING(tsize / (second * rtrate)+1,FORMAT='(I7)')+' Secs (RT)'] 
                END
            40: str= STRING(tsize / (second * channels.sw_rate)+1,FORMAT='(I7)')+' Secs (SW)'
            41: str= STRING(tsize / (second * channels.ssr1_rate)+1,FORMAT='(I7)')+' Secs (SSR1)'
            42: str= STRING(tsize / (second * channels.ssr2_rate)+1,FORMAT='(I7)')+' Secs (SSR2)'
          ENDCASE 
          WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Download Time = ' + str(0)
          tind = tind+1
          IF (ip(iptable).steps(good(dest(id))) EQ 39) THEN BEGIN 
            WIDGET_CONTROL, os_sumv.lab(tind), SET_VALUE='Download Time = ' + str(1)
            tind = tind+1
          ENDIF
        ENDFOR
      ENDELSE

   ENDIF 
   ENDIF 


   FOR i=tind, N_ELEMENTS(os_sumv.lab)-1 DO WIDGET_CONTROL, os_sumv.lab(i), SET_VALUE='   '

ip= ip_orig
ccd= ccd_orig
ex= ex_orig

   PLOT_CCD, os_sumv.draw_win, tele, $
      ccd1.x1, ccd1.x2, ccd1.y1, ccd1.y2, $
      BLOCKS=blocks
 

END
 

;________________________________________________________________________________________________________
;


PRO OS_SUMMARY_EVENT, event
 
COMMON OS_SUMMARY_SHARE, os_sumv

CASE (event.id) OF
 
   os_sumv.dismiss : BEGIN      ;** exit program
      WIDGET_CONTROL, /DESTROY, os_sumv.base
   END
 
   ELSE : BEGIN
   END
 
ENDCASE
 
END
 
;________________________________________________________________________________________________________
;


PRO OS_SUMMARY, os_num, caller

COMMON OS_SUMMARY_SHARE, os_sumv
COMMON OS_DEFINED
COMMON DIALOG, mdiag,font

   IF (DATATYPE(defined_os_arr) EQ 'UND') THEN BEGIN
     WIDGET_CONTROL,mdiag,SET_VALUE="%%OS_SUMMARY: No OS Selected - Quick Summary Ignored."
     PRINT,'%%OS_SUMMARY: No OS Selected - Quick Summary Ignored.'
     RETURN 
   ENDIF

   ind = WHERE(defined_os_arr.os_num EQ os_num, cnt)
   IF(cnt EQ 0) THEN RETURN

   ;  Use vaild HI1 or HI2 sequence info:
   IF(cnt EQ 2) THEN $
     ind = WHERE(defined_os_arr.os_num EQ os_num AND defined_os_arr.num_images GT 0)
  
   IF XRegistered("OS_SUMMARY") AND NOT(KEYWORD_SET(HI)) THEN BEGIN  ; AEE Dec 04, 2002
      OS_SUMMARY_FILL, os_num,ind(0)
      RETURN
   ENDIF

   base = WIDGET_BASE(/COLUMN, TITLE='SECCHI OBSERVING SEQUENCE SUMMARY', /FRAME, $
          GROUP_LEADER=caller)

     row1 = WIDGET_BASE(base, /ROW)

       col = WIDGET_BASE(row1, /COLUMN)
         xsize = 650
         col1 = WIDGET_BASE(col, /COLUMN, /FRAME, SPACE=2, XSIZE=xsize, /SCROLL, $
                X_SCROLL_SIZE=270, Y_SCROLL_SIZE=234)
           lab = LONARR(40) ; AEE - 7/11/03
           str = STRING(' ', FORMAT='(a165)')
           lab(0) = WIDGET_LABEL(col1, VALUE=str,/ALIGN_LEFT,FONT=font)
           FOR i=1, N_ELEMENTS(lab)-1 DO BEGIN
              lab(i) = WIDGET_LABEL(col1, VALUE=str,/ALIGN_LEFT)
           ENDFOR

       col2 = WIDGET_BASE(row1, /COLUMN, /FRAME)

         RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav'
         IF (xyblks EQ 32) THEN draw_w = WIDGET_DRAW(col2, /FRAME, XSIZE=256, YSIZE=256, RETAIN=2)
         IF (xyblks EQ 34) THEN draw_w = WIDGET_DRAW(col2, /FRAME, XSIZE=272, YSIZE=272, RETAIN=2)

      row2 = WIDGET_BASE(base, /ROW)
      cola = WIDGET_BASE(row2, /COLUMN)

      dismiss = WIDGET_BUTTON(cola, VALUE=' Dismiss ')
 
    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************
 
    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, base, TLB_SET_XOFFSET=400, TLB_SET_YOFFSET=400
    WIDGET_CONTROL, draw_w, GET_VALUE=draw_win

    os_sumv = CREATE_STRUCT( 'base', base,                  $
                             'draw_win', draw_win,          $
                             'lab', lab,                    $
                             'dismiss', dismiss )
 
     ;; AEE 5/12/04 - display only the valid (HI1 or HI2 sequence) summary:
     ;IF (cnt EQ 2) THEN BEGIN
     ;  hi1or2= WHERE(defined_os_arr(ind).num_images GT 0) ; => either 0 or 1
     ;  ind= ind(hi1or2(0))
     ;ENDIF

     OS_SUMMARY_FILL, os_num, ind(0)

    XMANAGER, "OS_SUMMARY", base

   RETURN

END
