;+
;$Id: shift_ipt.pro,v 1.3 2005/01/24 17:56:36 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : SHIFT_IPT
;               
; Purpose     : This function uses widgets to prompt the user for entering
;               a date to shift the observations in a .IPT file to. 
;
; Explanation:  The earliest obsevation date from .IPT file is presented
;               to the user. The user entered date is then returned. This
;               function is used when the .IPT file is used as a parameter
;               when schedule starts.
;               starts. 
;               
; Use         : shift_tai = SHIFT_IPT(first_dte)
;    
; Inputs      : first_dte  Double  Earliest observation date from .IPT file (TAI).
;
; Opt. Inputs : caller     Structure containing id of caller. 
;               
; Outputs     : shift_tai  Double  Difference between entered date and first_dte (TAI). 
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: shift_ipt.pro,v $
; Revision 1.3  2005/01/24 17:56:36  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:11  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

PRO SHIFT_IPT_EVENT, event

COMMON SHIFT_IPT_SHARE, return_value

  WIDGET_CONTROL, event.top, GET_UVALUE=shiftv   ; get structure from UVALUE

    CASE (event.id) OF

	shiftv.textd : BEGIN	;** exit program
	   WIDGET_CONTROL, shiftv.textd, GET_VALUE=val
           val = UTC2STR(STR2UTC(val), /ECS)
	   WIDGET_CONTROL, shiftv.textd, SET_VALUE=val
	END

	shiftv.cancel : BEGIN	;** cancel and exit program
           return_value = 0
	   WIDGET_CONTROL, /DESTROY, shiftv.base
	END

	shiftv.apply : BEGIN	;** return date to shift to exit program
	   WIDGET_CONTROL, shiftv.textd, GET_VALUE=val
           val = UTC2TAI(STR2UTC(val))
           return_value = val
	   WIDGET_CONTROL, /DESTROY, shiftv.base
	END

        ELSE : BEGIN
        END

   ENDCASE

END

;__________________________________________________________________________________________________________
;

FUNCTION SHIFT_IPT, CALLER=caller, first_dte

COMMON SHIFT_IPT_SHARE, return_value

    IF XRegistered("SHIFT_IPT") THEN RETURN, -1


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    IF (KEYWORD_SET(caller)) THEN $
       base = WIDGET_BASE(/COLUMN, TITLE='SHIFT IPT', /FRAME, GROUP_LEADER=caller.id) $
    ELSE $
       base = WIDGET_BASE(/COLUMN, TITLE='SHIFT IPT', /FRAME)


    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='Earliest Obs date in IPT file: '+UTC2STR(TAI2UTC(first_dte), /ECS))
    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='Shift to: ')
     GET_UTC, utc
     textd = WIDGET_TEXT(row, VALUE=STRMID(UTC2STR(utc, /ECS),0,19), /EDITABLE, XSIZE=25)
    row = WIDGET_BASE(base, /ROW)
     apply = WIDGET_BUTTON(row, VALUE=" Apply ")
     cancel = WIDGET_BUTTON(row, VALUE=" Cancel ")

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, base, /REALIZE

    shiftv = CREATE_STRUCT( 'base', base, 		$
                             'apply', apply, 		$
                             'cancel', cancel, 		$
                             'textd', textd)

   WIDGET_CONTROL, base, SET_UVALUE=shiftv

   XMANAGER, 'SHIFT_IPT', base, /MODAL

   RETURN, return_value

END
