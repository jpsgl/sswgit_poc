;+
;$Id: read_ssr_rt_mode.pro,v 1.1.1.2 2004/07/01 21:19:08 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : READ_SSR_RT_MODE 
;               
; Purpose     : This function reads the SSR_realtime.dat file and returns wether both 
;               SPACEWEATHER, and REATIME channel data, either one, or none will be 
;               added to the SSR1.
;               
; Use         : mode = READ_SSR_RT_MODE(fname)
;    
; Inputs      : fname   File 'SSR_realtime.dat' should exist in '../IN/OTHER/' directory.
;
; Opt. Inputs : None
;               
; Outputs     : mode    String of one of "BOTH", "SPACEWEATHER", "REATIME", "NONE", or "".
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: read_ssr_rt_mode.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION READ_SSR_RT_MODE,fname

  OPENR, lu, GETENV('PT')+'/IN/OTHER/'+fname, /GET_LUN

  found= 0 ;false
  str=''
  WHILE NOT(EOF(lu)) AND NOT found DO BEGIN
    READF, lu, str
    ;print,str
    str= STRUPCASE(STRTRIM(str,2))
    IF (STRLEN(str) GT 0 AND STRPOS(str,';') NE 0) THEN BEGIN
      IF (str EQ "BOTH" OR str EQ "SPACEWEATHER" or str EQ "REALTIME" OR str EQ "NONE") THEN found=1
    ENDIF
  ENDWHILE

  CLOSE, lu
  FREE_LUN, lu

  IF (NOT found) THEN str= ''

  RETURN, str

END
