;+
;$Id: schedule_read_os_ipt.pro,v 1.10 2011/06/30 20:41:02 nathan Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : SCHEDULE_READ_OS_IPT
;
; Purpose     : SCHEDULE_READ_OS_IPT, ipt_name, start_time, /SILENT, /PROMPT_SHIFT
;
; Use         : This routine reads in a InstrumentPlanningTool (.IPT) file and adds its
;               content to the schedule while schedule is up and running. The .IPT
;               file is selected from a widget. The .IPT file contains only one section
;               (only one OS_NUM) used for expanding a block sequence command (lp=7).
;
; Inputs      : ipt_name      String containing the .IPT filename.
;               sched_start   Start time of schedule (used to shift .IPT scheule times).
;
; Opt. Inputs : None
;
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : SILENT        If set, prints out diagnostic messages.
;               PROMPT_SHIFT  If set, prompt user if .IPT schedule should be shifted.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/09/04 - Added cadence to duplicate os check list.
;              Ed Esfandiari 06/15/04 - Moved defining os_struct to define_os_struct function.
;              Ed Esfandiari 06/16/04 - Added setup time.
;              Ed Esfandiari 07/02/04 - recalculate sizes in case new ip table use other apids.
;              Ed Esfandiari 07/14/04 - Use one of 4 ROI tables (used to have only 1 ROI table).
;              Ed Esfandiari 09/30/04 - Removed sync.
;              Ed Esfandiari 10/05/04 - Added spacecraft (sc) assignment.
;                            10/15/04 - Added SC A/B blockseqeunce counters.
;              Ed Esfandiari 05/18/04 - deduct 1 from lamp number since in BSF files,
;                                       lamps start from 1 now instead of 0.
;              Ed Esfandiari 08/19/05 - Fixed HI seq update checks.
;              Ed Esfandiari 12/07/05 - lamp values now starts from 0 (read from IPT files).
;
; $Log: schedule_read_os_ipt.pro,v $
; Revision 1.10  2011/06/30 20:41:02  nathan
; adjust how define_os_struct, os_init called
;
; Revision 1.9  2011/06/15 22:55:20  nathan
; Allow input of OSID; note that changed num_text from string with prefix to
; just a 4-digit number. Possibly output is affected.
; Also changed default state to ST-A instead of both, and automatically
; computes tlm volumes.
;
; Revision 1.8  2005/12/16 14:58:53  esfand
; Commit as of 12/16/05
;
; Revision 1.7  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.6  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.5  2005/01/24 17:56:36  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.3  2004/09/01 15:40:48  esfand
; commit new version for Nathan.
;
; Revision 1.2  2004/07/02 15:47:11  esfand
; calculate sizes in case new ip table contain new apids
;
; Revision 1.1.1.2  2004/07/01 21:19:10  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;

PRO SCHEDULE_READ_OS_IPT, ipt_file_name, start_time, SILENT=silent, STARTDIS=startdis

COMMON OS_SCHEDULED, os_struct, os_arr, tm_arr, load_camtable, load_wlexptable, do_candc, move_fp
COMMON OS_DEFINED, defined_os_arr, os_instance
COMMON OS_ALL_SHARE, ccd, ip, ipd, ex, exd, occ_blocks, roi_blocks, fpwl, fpwld
COMMON DIALOG, mdiag,font
COMMON APIDS, multi_apid   ; AEE - 7/25/03
COMMON SCHED_SHARE, schedv
COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt

IF n_params() LT 2 THEN start_time=0
;
; AEE - May 2003
; This routine only reads the ./OS_IPT/OS_nnnn.IPT files containing only one OS_NUM
; used to expand a block seq aommand (lp=7).
;
; NOTE: Door Open/Close (LP=8 and 9, OS_NUM=0001) and GuideTelescope Dumps (LP=10, OS_NUM=0002)
;       are not image taking OSes and are NOT allowed to appear as block commands.

  ;USEDEFAULTS = 0 ; => Use table parameters in .IPT (maybe different if tables have been changed).

   USEDEFAULTS = 1 ;=> use table numbers in .IPT to get parameters from current tables read by os_init
                   ;   and also recalculate image size and durations.

   new_os_num = -1

   IF (DATATYPE(os_instance) EQ 'UND') THEN BEGIN
    	OS_INIT
    	os_arr = -1
    ENDIF
   ;OPENR, ipt_file, GETENV('PT')+'/IO/IPT/'+ipt_file_name, /GET_LUN
   OPENR, ipt_file, ipt_file_name, /GET_LUN

   WIDGET_CONTROL,mdiag,SET_VALUE='READING IPT FILE: '+ipt_file_name
   PRINT, '**************************************'
   PRINT, 'READING IPT FILE: ', ipt_file_name

   str = ''

   all_commands = ''

   ;** find first occurance of PT_OS_NUM
   WHILE ( NOT(EOF(ipt_file)) AND (STRMID(str,0,9) NE "PT_OS_NUM") ) DO BEGIN
      READF, ipt_file, str
      str = STRTRIM(str, 2)
   ENDWHILE

   os_struct = DEFINE_OS_STRUCT()

   ;*************************************************
   ;** loop over each OS
   ;** 
   WHILE (NOT(EOF(ipt_file))) DO BEGIN
      curr_str_arr = str

      str = ''
      ;** find everything up to the next occurance of PT_OS_NUM
      WHILE ( NOT(EOF(ipt_file)) AND (STRMID(str,0,9) NE "PT_OS_NUM") ) DO BEGIN
         READF, ipt_file, str
         str = STRTRIM(str, 2)
         ;** throw out comments and blank lines now
         IF ( (STRMID(str,0,1) NE ';') AND (str NE '') ) THEN curr_str_arr = [curr_str_arr, str]
      ENDWHILE

      ;** if str is "PT_OS_NUM" it is for the next OS loop
      IF NOT(EOF(ipt_file)) THEN curr_str_arr = curr_str_arr(0:N_ELEMENTS(curr_str_arr)-2)

      ;** curr_str_arr contains all parameters for this OS
      ;** convert this to an array of keyword/value structures
      key_val_arr = STR_ARR2KEY_VAL(curr_str_arr)

      ;** keep track of which parameters we have used
      used = BYTARR(N_ELEMENTS(curr_str_arr))

      ;** now initialize an OS structure and fill it in
      os = FILL_IN_OS(key_val_arr, used)
 
    ; If os is not defined, then return
    	IF datatype(os) NE 'STC' THEN BEGIN
	    message,'Warning: '+ipt_file_name+' is not valid; Skipping',/info
	    wait,1
	    free_lun,ipt_file
	    return
	ENDIF
      ;** now initialize an LP structure for this particular LP and fill it in
      lp = FILL_IN_LP(os.lp_num, key_val_arr, used)

      IF (USEDEFAULTS EQ 1) THEN BEGIN
        os.os_size= 0 ; To force calculating the statistics.
        lp.ip = ip(lp.ip_tab_num).steps
        lp.occ = occ_blocks(lp.tele,*) 
        ;lp.roi = roi_blocks(lp.tele,*)
        roi_table= WHICH_ROI_TABLE(ip,lp.ip_tab_num)
        IF (roi_table EQ -1) THEN roi_table= 0 ; use the first ROI table as defualt when none present.
        lp.roi = roi_blocks(lp.tele,*,roi_table)
        lp.ccd = ccd(lp.tele,lp.camt-1)
        IF (os.lp_num LT 2 OR os.lp_num GT 3) THEN lp.exptime(0)= exd(lp.tele,lp.expt-1,lp.fw,lp.pw(0))
        IF (os.lp_num EQ 0) THEN lp.exptime(1)= exd(lp.tele,lp.expt-1,lp.fw,lp.pw(1)) 
        IF (os.lp_num EQ 5 OR os.lp_num EQ 6) THEN BEGIN
          n = N_ELEMENTS(lp.pw)
          FOR i=0,n-1 DO lp.exptime(i)= exd(lp.tele,lp.expt-1,lp.fw,lp.pw(i))
        ENDIF
      ENDIF

      IF NOT(KEYWORD_SET(SILENT)) THEN BEGIN
         print & print
         print, 'Unused parameter/values:'
         print, key_val_arr(where(used NE 1)), format='(a)'
         print & print
      ENDIF

      ;** FILL IN AN OS_INSTANCE
 
   new_entry = os_instance                      ;** os_instance defined in OS_DEFINED COMMON
   new_entry.ex = exd
   new_entry.os_num = os.os_num
   new_entry.cadence= os.os_cad
   new_entry.pre_proc_time= os.os_ppt

   IF (os.lp_num LT 0) THEN BEGIN ; AEE 9/25/03
     WIDGET_CONTROL,mdiag,SET_VALUE='%%SCHEDULE_READ_OS_IPT  Error: Unknown LP '+STRN(os.lp_num)
     PRINT, '%%SCHEDULE_READ_OS_IPT  Error: Unknown LP', os.lp_num
   END
   new_entry.lp = os.lp_num 

   CASE (new_entry.lp) OF

   0 : BEGIN                 ;** Sky Double Image LP - AEE 9/25/03
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0:1) = lp.pw(0:1)
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0))= lp.exptime(0)
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(1))= lp.exptime(1)
   END

   1 : BEGIN                 ;** Normal Image LP
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0) = lp.pw
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 
   2 : BEGIN                 ;** Dark Image LP
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, 0, 0) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 
   3 : BEGIN                 ;** Cal Lamp LP
      new_entry.tele = lp.tele
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0) = lp.pw
      new_entry.num_images = 1
      ;new_entry.lamp = lp.lamp - 1 ; since in BSF file, lamp was changed to start from 1
      new_entry.lamp = lp.lamp ; lamp is read from ipt file which starts with 0.
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END

   4 : BEGIN                 ;** Cont. Image LP
      new_entry.tele = lp.tele
      new_entry.num_images = 1
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      new_entry.pw(0) = lp.pw
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 
   5 : BEGIN                 ;** Seq PW FW LP
      new_entry.tele = lp.tele
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      n = N_ELEMENTS(lp.pw)
      new_entry.num_images = n      ; AEE - Dec 10, 02 
      new_entry.pw(0:n-1) = lp.pw
      new_entry.cadence= os.os_cad  ; AEE 01/27/03
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      FOR i=0,n-1 DO $
        new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(i)) = lp.exptime(i)
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
   END
 
   6 : BEGIN ; HI Seq
      new_entry.tele = lp.tele
      new_entry.exptable = lp.expt - 1 ; AEE 1/19/04
      new_entry.camtable = lp.camt - 1 ; AEE 1/19/04
      new_entry.fps = lp.fps ; AEE 1/19/04
      new_entry.iptable = lp.ip_tab_num
      new_entry.fw = lp.fw
      n = N_ELEMENTS(lp.pw)
      new_entry.num_images = n      ; AEE - Dec 10, 02 
      new_entry.pw(0) = lp.pw(0)
      new_entry.cadence= os.os_cad  ; AEE 01/27/03
      new_entry.ccd(new_entry.tele,new_entry.camtable) = lp.ccd
      new_entry.ip(new_entry.iptable).steps = lp.ip
      new_entry.occ_blocks(new_entry.tele, *) = lp.occ
      FOR i=0,n-1 DO $
        new_entry.ex(new_entry.tele, new_entry.exptable, new_entry.fw, new_entry.pw(0)) = lp.exptime(i)
      new_entry.roi_blocks(new_entry.tele, *) = lp.roi
  END

  7 : BEGIN                 ;** Block Seq 
    stop,'SCHEDULE_READ_OS_IPT: Should not gotten here; LP = 7 should have been expanded to other LPs.'
   END
 
   ELSE : BEGIN
     print,"****"
     print,"LP= ",new_entry.lp
     print,"In schedule_read_os_ipt.pro, LP= ",new_entry.lp," no found."
     print,"****"
   END
 
ENDCASE

   IF (DATATYPE(defined_os_arr) NE 'STC') THEN BEGIN   ;** first os scheduled  ; AEE 2/17/04
      IF (new_entry.os_num EQ 0) THEN new_entry.os_num = GET_OS_NUM()
      defined_os_arr_save = new_entry
      defined_os_arr = new_entry 
   ENDIF ELSE BEGIN		;** only add if new
      ind = WHERE(defined_os_arr.os_num EQ new_entry.os_num)
      IF (ind(0) EQ -1) THEN BEGIN			;** then it is new

         ;** save os definitions from this IPT so that we only allocate new os_num 
         ;** for truly new definitions
         IF (DATATYPE(defined_os_arr_save) NE 'STC') THEN BEGIN   ;** AEE - 02/17/04
            IF (new_entry.os_num EQ 0) THEN new_entry.os_num = GET_OS_NUM()
            defined_os_arr_save = new_entry
            defined_os_arr = [defined_os_arr, new_entry]	;** append to end of defined array
         ENDIF ELSE BEGIN
          found = 0
          n = 0
          len = N_ELEMENTS(defined_os_arr_save)
          WHILE ( (found NE 1) AND (n LT len) ) DO BEGIN
            t_os =  defined_os_arr_save(n)
            IF ( (new_entry.lp EQ t_os.lp) AND $
                          (new_entry.tele EQ t_os.tele) AND $
                          ;(new_entry.table EQ t_os.table) AND $  ; AEE 1/19/04
                          (new_entry.camtable EQ t_os.camtable) AND $ ; AEE 1/19/04
                          (new_entry.exptable EQ t_os.exptable) AND $ ; AEE 1/19/04
                          (new_entry.fps EQ t_os.fps) AND $ ; AEE 1/19/04
                          (new_entry.cadence EQ t_os.cadence) AND $
                          (new_entry.iptable EQ t_os.iptable) AND $
                          (new_entry.fw EQ t_os.fw) AND $
                          MIN(new_entry.pw EQ t_os.pw) AND $
                          (new_entry.lamp EQ t_os.lamp) AND $
                          (new_entry.num_images EQ t_os.num_images) AND $
                          (new_entry.start EQ t_os.start) AND $
                          MIN(new_entry.ex EQ t_os.ex) AND $
                          MIN(new_entry.occ_blocks EQ t_os.occ_blocks) AND $
                          MIN(new_entry.roi_blocks EQ t_os.roi_blocks) ) THEN found = 1
            IF (found EQ 1) THEN BEGIN	;** compare all tags in structure
               st = new_entry.ccd(new_entry.tele,new_entry.camtable)
               st2 = t_os.ccd(new_entry.tele,new_entry.camtable)
               nt = N_TAGS(st)
               res = BYTARR(nt)
               FOR t=0, nt-1 DO $
                  res(t) = MIN(st.(t) EQ st2.(t))
               st = new_entry.ip(new_entry.iptable)
               st2 = t_os.ip(new_entry.iptable)
               nt = N_TAGS(st)
               res2 = BYTARR(nt)
               FOR t=0, nt-1 DO $
                  res2(t) = MIN(st.(t) EQ st2.(t))
               IF ((MIN(res) < MIN(res2)) EQ 1) THEN found = 1 ELSE found = 0
            ENDIF
            n = n + 1
          ENDWHILE
            IF (found EQ 0) THEN BEGIN			;** this is new
               IF (new_entry.os_num EQ 0) THEN new_entry.os_num = GET_OS_NUM()
               defined_os_arr_save = [defined_os_arr_save, new_entry]
               defined_os_arr = [defined_os_arr, new_entry]	;** append to end of defined array
            ENDIF ELSE BEGIN 						;** this was already defined
               new_entry.os_num = t_os.os_num
            ENDELSE
         ENDELSE

      ENDIF
   ENDELSE

   ; AEE - 7/28/03

   IF (USEDEFAULTS NE 1) THEN BEGIN
     aind= WHERE( key_val_arr.key EQ 'PT_OS_SIZES', acnt)
     IF (acnt GT 0) THEN BEGIN
       IF (DATATYPE(multi_apid) EQ 'UND') THEN BEGIN 
         multi_apid= {multi_apid,os_num:os.os_num, sizes: key_val_arr(aind).val}  
       ENDIF ELSE BEGIN 
         mind= WHERE(multi_apid.os_num EQ os.os_num, scnt)
         IF (scnt EQ 0) THEN $
           multi_apid= [multi_apid,{multi_apid,os_num:os.os_num, sizes: key_val_arr(aind).val}]
       ENDELSE
     ENDIF ELSE BEGIN
       IF (os.os_size GT 0) THEN BEGIN
         PRINT,''
         PRINT,'WARNING: Each OS in .IPT file should have either a valid PT_OS_SIZES or PT_OS_SIZE = 0'
         PRINT,''
         STOP
       ENDIF
     ENDELSE
   ENDIF


         ;** 
         ;** IF stats are not in IPT file the calculate them
         ;IF ( (os.os_dur EQ 0) OR (os.os_size EQ 0) ) THEN BEGIN
         IF ( (os.os_size EQ 0) ) THEN BEGIN
            GET_OS_DB_STATS, new_entry, os_size, os_duration, os_pre_proc_time, os_ro_time, $
                             os_proc_time, os_setup_time, os_cadence 
            os.os_ppt= os_pre_proc_time
            os.os_rot= os_ro_time
            os.os_size = os_size
            os.os_dur = os_duration
            os.os_proct= os_proc_time
            os.os_setup= os_setup_time
            os.os_cad= os_cadence
            ;ndef= N_ELEMENTS(defined_os_arr)-1
            ;last= WHERE(defined_os_arr.os_num EQ new_entry.os_num, lcnt)
            last= WHERE(defined_os_arr.os_num EQ new_entry.os_num AND $
                        defined_os_arr.tele EQ new_entry.tele, lcnt)
            IF (lcnt GT 0) THEN BEGIN
              ;IF (last(0) NE ndef) THEN BEGIN
                IF (defined_os_arr(last).cadence NE os_cadence) THEN $
                  defined_os_arr(last).cadence= os_cadence
                IF (defined_os_arr(last).pre_proc_time NE os_pre_proc_time) THEN $
                  defined_os_arr(last).pre_proc_time= os_pre_proc_time
              ;ENDIF
            ENDIF
 
         ENDIF

      ;** loop over each scheduled start time and add to os_arr

      IF (N_ELEMENTS(os.lp_start) EQ 1) THEN BEGIN
        diff_time= [0.0D]
      ENDIF ELSE BEGIN
        diff_time= UTC2TAI(os.lp_start)-SHIFT(UTC2TAI(os.lp_start),1)
        diff_time(0)= 0.0D
      ENDELSE

      FOR s_index = 0, N_ELEMENTS(os.lp_start)-1 DO BEGIN

         tai = UTC2TAI(os.lp_start(s_index))

         add_os = os_struct
         add_os.os_num = new_entry.os_num
         ;add_os.set_id = os.set_id ; AEE 2/11/04 - Added
         add_os.os_duration = os.os_dur
         add_os.os_size = os.os_size
         add_os.os_pre_proc_time=  os.os_ppt  ; AEE 01/27/03
         add_os.os_ro_time= os.os_rot  ; AEE 01/27/03
         add_os.os_setup_time= os.os_setup
         add_os.os_tele= new_entry.tele 
         add_os.os_lp= new_entry.lp
         add_os.os_images= new_entry.num_images
         add_os.os_cadence= os.os_cad  ; AEE 01/27/03
         add_os.os_proc_time=  os.os_proct
         add_os.os_start = start_time + TOTAL(diff_time(0:s_index)) 
         add_os.os_stop = add_os.os_start + add_os.os_duration      ;** calc stop time
         add_os.sc= os.lp_sc(s_index)
         IF (schedv.sc EQ 1) THEN BEGIN
           add_os.sc= 'A'
           add_os.bsf= add_os.sc+STRTRIM(a_bsf_cnt,2)
         ENDIF
         IF (schedv.sc EQ 2) THEN BEGIN
           add_os.sc= 'B'
           add_os.bsf= add_os.sc+STRTRIM(b_bsf_cnt,2)
         ENDIF

         IF (DATATYPE(os_arr) EQ 'INT') and start_time GT 0 THEN os_arr = add_os ELSE $
            os_arr = [os_arr, add_os]                    ;** add to current schedule

         ;***************************
         ;** ADD CAMERA TABLE LOADS
         IF (os.lp_load_cam(s_index) EQ 1) THEN BEGIN
            cam_stc = {cam_stc, os_num:0L, os_start:0.0D}
            cam_stc.os_num = add_os.os_num
            cam_stc.os_start = add_os.os_start
            IF (DATATYPE(load_camtable) NE 'STC') THEN BEGIN  ;** first entry
               load_camtable = cam_stc
            ENDIF ELSE BEGIN
               ind = WHERE((load_camtable.os_num EQ add_os.os_num) AND (load_camtable.os_start EQ add_os.os_start))
               IF (ind(0) EQ -1) THEN BEGIN
                  load_camtable = [load_camtable,cam_stc]
               ENDIF
            ENDELSE
         ENDIF

         ;***************************
         ;** ADD WAVELENGTH/EXP TABLE LOADS
         IF (os.lp_load_wlexp(s_index) EQ 1) THEN BEGIN
            cam_stc = {cam_stc, os_num:0L, os_start:0.0D}
            cam_stc.os_num = add_os.os_num
            cam_stc.os_start = add_os.os_start
            IF (DATATYPE(load_wlexptable) NE 'STC') THEN BEGIN  ;** first entry
               load_wlexptable = cam_stc
            ENDIF ELSE BEGIN
               ind = WHERE((load_wlexptable.os_num EQ add_os.os_num) AND (load_wlexptable.os_start EQ add_os.os_start))
               IF (ind(0) EQ -1) THEN BEGIN
                  load_wlexptable = [load_wlexptable,cam_stc]
               ENDIF
            ENDELSE
         ENDIF

      ENDFOR	;** end looping on start times for this lp
      IF (new_os_num(0) EQ -1) THEN $
         new_os_num = new_entry.os_num $
      ELSE $
         new_os_num = [new_os_num,new_entry.os_num]

   ENDWHILE

   CLOSE, ipt_file
   FREE_LUN, ipt_file

   WIDGET_CONTROL,mdiag,SET_VALUE='DONE READING IPT FILE '
   PRINT, 'DONE READING IPT FILE '


   ind = SORT(os_arr.os_start)
   startdis = os_arr(ind(0)).os_start

   ; AEE - 02/05/03 - for LP=6 (HI-seq) if both HI1 and HI2 are used in the same sequence, the HI1 and
   ;                  HI2 images are identified by os_num and -os_num in the IPT file. Once the IPT is
   ;                  read and finished (at this point), change the minus os_num(s) to os_num using
   ;                  ABS, if any exists:

   os_arr.os_num= ABS(os_arr.os_num)
   defined_os_arr.os_num= ABS(defined_os_arr.os_num)

   RETURN
   
END
