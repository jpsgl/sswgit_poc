;+
;$Id: popup_help.pro,v 1.4 2009/09/11 20:28:19 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : POPUP_HELP
;               
; Purpose     : Creates a widget that displays a pop up message.
;               
; Use         : POPUP_HELP, message, title=title, align=['left','right','center']
;    
; Inputs      : message  String or string vector containing a message
;                        that will be displayed on the screen for the
;                        user to read.
;                        (multi-line messages are aesthetically better)
;
;     title:    Optional title of the message window
; 
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : align     align='left' or align='right' or align='center' (center is also default) 
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;               Ed Esfandiari 12/12/06 - Added align keyword.
;               Ed Esfandiari 02/08/07 - Added xsize keyword.
;
; $Log: popup_help.pro,v $
; Revision 1.4  2009/09/11 20:28:19  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Document name: popup_help.pro
; Created by:    Liyun Wang, GSFC/ARC, August 19, 1994
;
; Last Modified: Thu Sep  8 10:05:26 1994 (lwang@orpheus.gsfc.nasa.gov)
; Last Modified: Thu Oct 13 14:51:10 1994 (scott@argus.nrl.navy.mil)
;    took out restriction of only one popup at a time.
;    renamed from popup_msg.pro to popup_help.pro.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;+
;
; file POPUP_HELP.PRO - Creates a widget that displays a pop up message,
;                      modifed from DMZ's acknowledge.pro -- LYW
;
;-	

PRO popup_help_event, event
;+
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; pro popup_help_event, event
;
; The event handler for PRO POP_HELP
;  calls to  :  none
;  common    :  none
;
;  The only purpose of the routine is to kill the message window, 
;     created by PRO POP_HELP, AFTER the user reads the message.
;     The user clicks the "Dismiss" button to get here.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;-
   WIDGET_CONTROL, event.top, /destroy ; This widget is modaled so its the 
                                ;  only widget the user is able to kill 
   RETURN 
END

;===========================================================================

PRO popup_help, text, title=title, group=group, align=align, xsize=xsize 
;+
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; pro popup_help, message [, title=title, group=group]
;
;  Creates a message window that informs the user with a message and
;     requires the user to read the message and to dismiss the window   
;     before control will return to the calling procedure.
;
;  calls to  :  xregistered('pop_help'), xmanager,'popup_help'
;  common    :  none
;
;
; INPUT
;     message:  string or string vector containing a message
;		that will be displayed on the screen for the 
;		user to read.
;		(multi-line messages are aesthetically better)
;     title:    Optional title of the message window
; OUTPUT
;     none
;
; MODIFICATION HISTORY
;	JAN 1993        -- Elaine Einfalt (HSTX)
;       August 19, 1994 -- Liyun Wang (ARC)
;       August 31, 1994 -- Liyun Wang (ARC), added GROUP keyword
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;-

   
   IF (N_ELEMENTS(title) EQ 0) THEN title = 'Message'
   IF N_ELEMENTS(text) EQ 0 THEN BEGIN
      text = ['Usage:','    popup_help, text [,title=title]']
      title = 'POPUP_HELP'
   ENDIF 
   
   button_name = ' Dismiss '   ; word appearing in button
   mess_siz = N_ELEMENTS(text) - 1 ; lines in message, -1 for loop
   IF mess_siz LT 0 THEN BEGIN  ; no user provided message
      mess_siz = N_ELEMENTS(text) - 1 ; # times to loop below
      RETURN
   ENDIF

   ;base = WIDGET_BASE(title=title, /column, space=20, xpad=10, ypad = 10,X_SCROLL_SIZE=450,Y_SCROLL_SIZE=800)

   IF (KEYWORD_SET(xsize)) THEN BEGIN
     base = WIDGET_BASE(title=title, /column, space=20, xpad=10, ypad = 10,X_SCROLL_SIZE=xsize,Y_SCROLL_SIZE=800)
   ENDIF ELSE BEGIN
     base = WIDGET_BASE(title=title, /column, space=20, xpad=10, ypad = 10,X_SCROLL_SIZE=450,Y_SCROLL_SIZE=800)
   ENDELSE
   
   tmp_bs = WIDGET_BASE(base, /column, xpad = 50)
   respond = WIDGET_BUTTON(tmp_bs, value= button_name, /frame)
   
   text_part = WIDGET_BASE(base, /column) ; write message 
   do_align=''
   IF (KEYWORD_SET(align)) THEN do_align= STRLOWCASE(STRTRIM(align,2)) 
   CASE do_align OF
     'center' : FOR i = 0, mess_siz DO info = WIDGET_LABEL(text_part, value=text(i))
     'right'  : FOR i = 0, mess_siz DO info = WIDGET_LABEL(text_part, value=text(i),/ALIGN_RIGHT)
     'left'   : FOR i = 0, mess_siz DO info = WIDGET_LABEL(text_part, value=text(i),/ALIGN_LEFT)
     else     : FOR i = 0, mess_siz DO info = WIDGET_LABEL(text_part, value=text(i))
   ENDCASE
 
   WIDGET_CONTROL, base, /realize ; Realize the widget
   XMANAGER,'popup_help', base, modal = KEYWORD_SET(group)

   RETURN 
END

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of 'popup_help.pro'.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
