;+
;$Id: schedule_event.pro,v 1.19 2013/01/09 13:33:23 mcnutt Exp $
;
; Project     : STEREO - SECCHI 
;
; Name        : SCHEDULE_EVENT
;
; Purpose     : The event handler for the SECCHI SCHEDULE routine.
;
; Use         : SCHEDULE_EVENT, event
;
; Inputs      : event	The event from the SCHEDULE tool.
;
; Opt. Inputs : None.
;
; Outputs     : None.
;
; Opt. Outputs: None.
;
; Keywords    : None.
;
; Restrictions: The saveset files 'os_arr.sav' must exist in '../IN/OTHER/'
;		directory and have write permission by the user if
;		the user wishes to save changes to the schedule.
;
; Side effects: None.
;
; Category    : Planning, Scheduling.
;
; Prev. Hist. : Adapted from SOHO/LASCO Planning Tool.
;
; Written by  :  Ed Esfandiari, NRL, May 2004 - First Version. 
;
; Modification History:
;              Ed Esfandiari 06/07/04 - Added diagnostic info if saving an empty schedule. Also
;                                       print os_num strings using 4-digits.
;                            06/09/04 - Use proc_time for OS structure instead of calculating it. 
;                            06/17/04 - directed Verify-Schedule to a popup menu instead of terminal.
;                            06/22/04 - Implemented LED constraint due to LED controller leaks.
;                            08/05/04 - Use os_num strings of 4 char.
;                            09/22/04 - Added SpaceCraft section.
;                            09/27/04 - Added OS_ALL_AB common block
;                            09/30/04 - Added SpaceCraft A and B scheduling capabilities.
;              Ed Esfandiari 10/04/04 - direct output to SC_A or SC_B. Added conf and removed gipt
;                            10/15/04 - Added SC A/B blockseqeunce counters.
;                            11/05/04 - Added code to generate BSF files. 
;                            12/01/04 - Added door closure overlap detection.
;                            12/03/04 - Capture schedule in .PNG instead of .GIF
;                            01/12/05 - Ask for confirmation when quitting a PT session.
;                            02/01/05 - Added Initialize timeline command.
;                            02/03/05 - Added code to handle Init Timeline command when reading a .BSF file.
;                            03/10/05 - Added FSW schedule-read delay check before allowing to generate BSF
;                                       and command files.
;                            05/02/05 - Added code to return total delay FSW delay, readtime, required
;                                       to read in a schedule and use it with .RTS and .BSF file. The B 
;                                       and T commands in these 2 file now contain the readtime (sec)
;                                       but actual observation commands are shifted so that first 
;                                       command starts as zero offset. 
;                            05/06/05   Modified Initialize timeline code so that multiple stand-alone
;                                       or from .bsf files are allowed. 
;                            05/11/05   Modified Run Script code so that multiple stand-alone 
;                                       or from .bsf files are allowed.
;              Ed Esfandiari 05/26/05 - Added code to allow actual or a 24hr playback.
;              Ed Esfandiari 03/07/06 - Don't replot using new pct_full changes when in Skip mode.
;              Ed Esfandiari 03/13/06 - Commented out OS_SUMMARY_TEXT statement when schedule is verified.
;              Ed Esfandiari 04/20/06 - Added code to read and use SSR2 playback percentage during a playback.
;                                       Initial default is 100% of SSR2 channel.
;              Ed Esfandiari 04/21/06 - Read-off comment lines (start with #) at top of the file.
;              Ed Esfandiari 05/09/06 - Keep only valid OS of HI1 or HI2 sequence instead of both. 
;              Ed Esfandiari 05/19/06 - Added SC A and B % full and SSR2 playback independency.
;              Ed Esfandiari 08/17/06 - Changed draw x,y values for png file for secchig display.
;              Ed Esfandiari 08/22/06 - Added data_rate (drate) selection.
;              Ed Esfandiari 09/01/06 - Added rate_gt code.
;              Ed Esfandiari 09/06/06 - Since 20% of SSR2 playback is now volume (ie. 35MB for months 0-14)
;                                       and is read in via "SSR Data Rate" dropdown button, I commented out
;                                       SSR2 playback percentage prompt. It now used 100% as default for
;                                       ssr2_pct_pb which does not do anything (see plot_ssr2_channel.pro).
;              Ed Esfandiari 09/06/06 - Also commented out rt_pct_start (not needed).
;              Ed Esfandiari 11/27/06 - Added code to switch to SC-B tables when B is selected (calls to OS_INIT).
;                                       Note: using different SC-A and SC-B table values for same
;                                       image (same OS_NUM), may result in images that are different
;                                       but are assumed to be the same (not ideal if synched?). It
;                                       is best that tables on both spacecrafts are the same.
;              Ed Esfandiari 11/30/06 - print .png filename to the screen.
;              Ed Esfandiari 01/11/06 - Expanded .png file so that BSF section is also saved.
;              Ed Esfandiari 04/-2/07 - Added capability to schedule a BSF file multiple times at a given cadence.
;              Ed Esfandiari 06/20/07 - Added toggle option to display data volume/downloadable ratio or volume in MB.
;              Ed Esfandiari 06/20/07 - Changes to handle bsf files thtat don't have any images.
;              Ed Esfandiari 08/28/09 - Changed display width for summary files to 170.
;
;
; $Log: schedule_event.pro,v $
; Revision 1.19  2013/01/09 13:33:23  mcnutt
; changed made when generating new block sched 20130108
;
; Revision 1.18  2013/01/07 19:35:50  nathan
; call os_proc_time with /loud
;
; Revision 1.17  2011/08/19 20:40:43  nathan
; fix error in output_plan
;
; Revision 1.16  2011/07/19 16:11:29  nathan
; always generate_ipt_singles
;
; Revision 1.15  2011/06/30 20:44:05  nathan
; Change all cases of num_text to omit OS_NUM prefix to label
;
; Revision 1.14  2011/06/15 22:55:19  nathan
; Allow input of OSID; note that changed num_text from string with prefix to
; just a 4-digit number. Possibly output is affected.
; Also changed default state to ST-A instead of both, and automatically
; computes tlm volumes.
;
; Revision 1.13  2009/09/11 20:28:23  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.8  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.7  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.6  2005/04/27 20:38:39  esfand
; these were used for 4/21/05 synoptics
;
; Revision 1.5  2005/03/10 16:50:30  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.4  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:09  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-
 
;__________________________________________________________________________________________________________
;

PRO CHECK_CONFLICTS,conf_arr,os,times
COMMON SCHED_SHARE, schedv 

  sc= 'AB'
  IF (schedv.sc EQ 1) THEN sc= 'A'
  IF (schedv.sc EQ 2) THEN sc= 'B'

  confs= WHERE(conf_arr NE '', n_cnf) ; remove the blank line(s) separators (each conflict consists of 5 text lines)
  IF (n_cnf EQ 0) THEN $
    RETURN $
  ELSE $
    confs= conf_arr(confs)

  new_conf_arr= ['','']

  os_str= STRTRIM(os,2)         ; os_num to be deleted, or moved.
  bet= STR_SEP(times,' and ')   ; begin and end times for deleting or moving of the os_num.


  ;conf_start_ind= WHERE(STRPOS(confs," Within ") GT 0, cfcnt)
  conf_start_ind= WHERE(STRPOS(confs,"Constraint Conflict") GT 0, cfcnt)
  conf_start_ind= [conf_start_ind,N_ELEMENTS(confs)]
  FOR i= 0, cfcnt-1 DO BEGIN ; check each conflict section to see it can be removed.
    j= conf_start_ind(i)      ; start of this conflict section
    k= conf_start_ind(i+1)-1  ; end   of this conflict section
    be= STR_SEP(confs(j+2),' and ') 

    keep= INDGEN(4)+j 
    FOR l= j+4, k DO BEGIN
      be1= STR_SEP(confs(l),' between ')
      ; be1(1) now contains 'date and date (....)' 
      be1= STR_SEP(be1(1),' (')
      ; be1(0) now contains 'date and date' 
      be1= STR_SEP(be1(0),' and ')

      IF ((STRPOS(confs(j+1),os_str) GE 0 AND be(0) GE bet(0) AND be(1) LE bet(1)) OR $
        (STRPOS(confs(l),os_str) GE 0 AND be1(0) GE bet(0) AND be1(1) LE bet(1))) THEN BEGIN
        ; Found a os_num and time match - remove this conflict entry from the array by not adding
        ; its text line to keep:

        IF (SC NE 'AB') THEN BEGIN
          osc= 'A'
          IF (sc EQ 'A') THEN osc= 'B'
          ; if current schedule is not AB and entry to be removed is the other sc, keep it:
          IF (STRPOS(confs(l),osc+' ') EQ 0) THEN keep= [keep,l]
          IF (STRPOS(confs(l),'AB') EQ 0) THEN BEGIN
            ; Also, if current schedule is not AB and entry to be removed is 'AB', keep the entry
            ; but change it to be only for the other sc:
            confs(l)= osc+STRMID(confs(l),2,200)
            keep= [keep,l] 
          ENDIF
        ENDIF
      ENDIF ELSE BEGIN
        keep= [keep,l]
      ENDELSE
    ENDFOR

    ; Only remove this section if all of conflicts were removed. IF keep > 4 => all or some remain):
    IF (N_ELEMENTS(keep) GT 4) THEN new_conf_arr= [new_conf_arr,confs(keep),'','']

;help,confs,new_conf_arr,j,k,keep
;stop

  ENDFOR

  conf_arr= new_conf_arr(0:N_ELEMENTS(new_conf_arr)-2) ; remove last blank (file ends with just one blank line)

  RETURN
END



PRO CHECK_CONFLICTS_DELTA,conf_arr,delta_oses,delta_times
COMMON SCHED_SHARE, schedv 

sc= 'AB'
IF (schedv.sc EQ 1) THEN sc= 'A'
IF (schedv.sc EQ 2) THEN sc= 'B'

FOR n= 0, N_ELEMENTS(delta_oses)-1 DO BEGIN
  os= delta_oses(n)
  times= delta_times(n)

  confs= WHERE(conf_arr NE '', n_cnf) ; remove the blank line(s) separators (each conflict consists of 5 text lines)
  IF (n_cnf EQ 0) THEN $
    RETURN $
  ELSE $
    confs= conf_arr(confs)

  new_conf_arr= ['','']

  os_str= STRTRIM(os,2)         ; os_num to be deleted, or moved.
  bet= STR_SEP(times,' and ')   ; begin and end times for deleting or moving of the os_num.

  ;conf_start_ind= WHERE(STRPOS(confs," Within ") GT 0, cfcnt)
  conf_start_ind= WHERE(STRPOS(confs,"Constraint Conflict") GT 0, cfcnt)
  conf_start_ind= [conf_start_ind,N_ELEMENTS(confs)]
  FOR i= 0, cfcnt-1 DO BEGIN ; check each conflict section to see it can be removed.
    j= conf_start_ind(i)      ; start of this conflict section
    k= conf_start_ind(i+1)-1  ; end   of this conflict section

    be= STR_SEP(confs(j+2),' and ')

    keep= INDGEN(4)+j
    FOR l= j+4, k DO BEGIN
      be1= STR_SEP(confs(l),' between ')
      be1= STR_SEP(be1(1),' and ')
      be1(1)= STRMID(be1(1),0,STRLEN(be(0)))  ; remove extra stuff like '  (CCD)' from be1(1), if any.
      IF ((STRPOS(confs(j+1),os_str) GE 0 AND be(0) GE bet(0) AND be(1) LE bet(1)) OR $
        (STRPOS(confs(l),os_str) GE 0 AND be1(0) GE bet(0) AND be1(1) LE bet(1))) THEN BEGIN
        ; Found a os_num and time match - remove this conflict entry from the array by not adding
        ; its text line to keep:

        IF (SC NE 'AB') THEN BEGIN
          osc= 'A'
          IF (sc EQ 'A') THEN osc= 'B'
          ; if current schedule is not AB and entry to be removed is the other sc, keep it:
          IF (STRPOS(confs(l),osc+' ') EQ 0) THEN keep= [keep,l]
          IF (STRPOS(confs(l),'AB') EQ 0) THEN BEGIN
            ; Also, if current schedule is not AB and entry to be removed is 'AB', keep the entry
            ; but change it to be only for the other sc:
            confs(l)= osc+STRMID(confs(l),2,200)
            keep= [keep,l]
          ENDIF
        ENDIF
      ENDIF ELSE BEGIN
        keep= [keep,l]
      ENDELSE
    ENDFOR
    ; Only remove this section if all of conflicts were removed. IF keep > 4 => all or some remain):
    IF (N_ELEMENTS(keep) GT 4) THEN new_conf_arr= [new_conf_arr,confs(keep),'','']

;help,confs,new_conf_arr,j,k,keep
;stop

  ENDFOR

  conf_arr= new_conf_arr(0:N_ELEMENTS(new_conf_arr)-2) ; remove last blank (file ends with just one blank line

ENDFOR

RETURN
END


FUNCTION GET_SC_CONF, tmpconf,sc
  confs=''
  sections= WHERE(STRPOS(tmpconf,"Constraint Conflict") GT 0, sec_cnt)
  sections(0)= 0
  FOR i= 0, sec_cnt -1 DO BEGIN
    IF (i LT sec_cnt-1) THEN $
      sec= tmpconf(sections(i):sections(i+1)-1) $ 
    ELSE $
      sec= tmpconf(sections(i):N_ELEMENTS(tmpconf)-1) 
    oses= WHERE(STRPOS(STRMID(sec,0,2),sc) GE 0, oscnt)
    IF (oscnt GT 1 ) THEN BEGIN
      IF (STRPOS(sec(oses(0))," of ") GT 0) THEN BEGIN
        othr= WHERE(STRPOS(sec,"OS_") LT 0)
        keep= [othr,oses]
        keep= keep(SORT(keep))
        confs= [confs,sec(keep)]
      ENDIF
    ENDIF
  ENDFOR

  IF (N_ELEMENTS(confs) GT 1) THEN confs= confs(1:*)

;help,confs
;stop

  RETURN,confs
END


FUNCTION ADD_OS, schedv

COMMON OP_SCHEDULED
COMMON OP_DEFINED
COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON OS_PLOT_SHARE
COMMON KAP_INIT
COMMON KAP_INPUT
COMMON DIALOG,mdiag,font
COMMON CMD_SEQ_SCHEDULED, sched_cmdseq
COMMON SCIP_DOORS_SHARE, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
                         sc_maneuvers,start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2
;COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt ; AEE - 7/24/03
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt ; AEE - 09/01/06 

COMMON SCHED_CONF, all_conflicts ; AEE - 10/21/03

                   WIDGET_CONTROL,/HOUR
                   WIDGET_CONTROL,mdiag,SET_VALUE=''
                   WIDGET_CONTROL, schedv.num_text,     GET_VALUE=os_num
		   WIDGET_CONTROL, schedv.start_text,   GET_VALUE=start_value
		   WIDGET_CONTROL, schedv.stop_text,    GET_VALUE=stop_value

                   IF (STRTRIM(start_value,2) GE STRTRIM(stop_value,2)) THEN BEGIN
                     WIDGET_CONTROL,mdiag,SET_VALUE='Invalid Start/Stop Times - Ignored ADD Command.'
                     PRINT,'Invalid Start/Stop Times - Ignored ADD Command.'
                     RETURN,0
                   ENDIF

		   WIDGET_CONTROL, schedv.delta_text,   GET_VALUE=delta
                   delta= strtrim(delta(0),2)
                   IF(STRLEN(delta) GT 0 AND STRPOS(delta(0),':') LE 0) THEN BEGIN
                     WIDGET_CONTROL,mdiag,SET_VALUE='Please re-enter DELTA using a time (hh:mm:ss) fromat.'
                     PRINT,'Please re-enter DELTA using a time (hh:mm:ss) fromat.'
                     RETURN,0
                   ENDIF
		   WIDGET_CONTROL, schedv.count_text,   GET_VALUE=count
                   WIDGET_CONTROL, schedv.os_size_text, GET_VALUE=os_size
                   WIDGET_CONTROL, schedv.os_dur_text,  GET_VALUE=os_duration
                   WIDGET_CONTROL, schedv.os_proc_text,  GET_VALUE=os_proc     ; AEE - Dec 19, 02
                   WIDGET_CONTROL, schedv.os_rot_text,  GET_VALUE=os_rot       ; AEE - Dec 19, 02
                   WIDGET_CONTROL, schedv.os_setup_text, GET_VALUE=os_setup

                   dos_size= DOUBLE(STR_SEP(STRCOMPRESS(STRTRIM(os_size,2)),' '))
                   ;** convert displayed units to bits
                   WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=from
                   dos_size = SCHEDULE_CONVERT_UNITS(dos_size, from, 0)

                   dos_duration= FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_duration,2)),' '))
                   dos_proc= FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_proc,2)),' '))
                   dos_rot= FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_rot,2)),' '))
                   dos_setup= FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_setup,2)),' '))

                   ;** convert displayed units [secs or mins] to secs
                   dos_duration = FLOAT(dos_duration / schedv.os_dur_factor)
                   tot_os_dur= TOTAL(dos_duration)
                   dos_proc= FLOAT(dos_proc / schedv.os_dur_factor)
                   dos_rot= FLOAT(dos_rot / schedv.os_dur_factor)
                   dos_setup= FLOAT(dos_setup / schedv.os_dur_factor)

                   ;os_num = STRTRIM(os_num, 2)
                   ;os_num = LONG(STRMID(os_num, 7, STRLEN(os_num)-7))
		   os_num=long(os_num[0])

                   ;ind = WHERE(defined_os_arr.os_num EQ os_num)
                   ;indx = WHERE(defined_os_arr.os_num EQ os_num, ind_cnt)

                   indx = WHERE(defined_os_arr.os_num EQ os_num AND $ ; AEE - 01/28/03 - check for 0 images (HI)
                                defined_os_arr.num_images NE 0, ind_cnt)

                   ;IF (ind(0) EQ -1) THEN BEGIN
                   IF (ind_cnt EQ 0) THEN BEGIN
                      PRINT, '%SCHEDULE_EVENT: OS_NUM: ', os_num, ' not found in defined_os_arr.'
                      WIDGET_CONTROL,mdiag,SET_VALUE='OS_NUM: '+ STRING(os_num,'(I4.4)')+ ' not found in defined_os_arr.'
                      RETURN,0
                   ENDIF

	   	   orig_start_value = UTC2TAI(start_value(0))	; ASCII to TAI
	   	   stop_value  = UTC2TAI(stop_value(0))		; ASCII to TAI

                   ;ind = ind(0)
                  FOR ind = 0, ind_cnt -1 DO BEGIN ; AEE - Dec 31, 02 . Added to handle HI Seq. 

                   start_value = orig_start_value
                   IF (ind EQ 1) THEN BEGIN ; mark start and stop of HI2 of a HI_Seq
                      start_value = start_value + dos_duration(0)
                      ;stop_value  = stop_value + dos_duration(0) 
                      GOTO, skip2hi2   ;***** HI2 conflicts are NOT checked this way ****

                      ;**** AEE - 02/05/03 - NOTE: if above GOTO is commented out, MUST make
                      ; sure that for ind=1, delta is not re-calculated and also sheck for other
                      ; possible problems. *****

                   ENDIF

	   	   count  = FIX(count(0))
                   ;dte = STR2UTC(STRTRIM(delta,2))

                   ;AEE - May 2002, allowed + and - delta shift in times (in 'DELTA' and 'ADD' sections):
                   ;                plus=  hh:mm:ss.ms 
                   ;                         or 
                   ;                       +hh:mm:ss.ms
                   ;                minus= -hh:mm:ss.ms

                   delta= STRTRIM(delta,2)
                   sign= STRMID(delta,0,1)
                   sign= sign(0)
                   IF (sign EQ '-' OR sign EQ '+') THEN    $
                     delta= STRMID(delta,1,STRLEN(delta))  $
                   ELSE                                    $
                     sign= '+'
                   dte = STR2UTC(delta)
                   delta = dte.time / 1000			;** convert msec to sec
                   IF (sign EQ '-') THEN delta= -delta


                   ;os_duration= tot_os_dur(0)
                   os_duration= DOUBLE(TOTAL(tot_os_dur)) ; AEE - 02/06/03 - For LP=6 and both tele used, use the
                                                        ; total duration to compare with (or get) delta and count.

                   sduration= strtrim(string(os_duration,'(f15.2)'),2)
                   sduration= sduration(0)+' sec'
                  
                   ;AEE - Make sure we can fit the requested number of observations within
                   ; the provided start and stop times:
                   IF (count LE 0) THEN BEGIN
                     scount= strtrim(string(count),2)
                     scount= scount(0)
                     sdelta= strtrim(string(delta,'(f13.2)'),2)
                     sdelta= sdelta(0)+' sec'
                     IF (delta LE 0) THEN BEGIN 
                       mess1=["COUNT="+scount+", DELTA="+sdelta+", and DURATION= "+sduration+".",$
                              "Use DELTA = DURATION + 1 sec to schedule"]
                       ;mess1="COUNT="+scount+" and DELTA="+sdelta+". Add 1 sec to DURATION="+sduration+ $
                       ;       " and use it as DELTA to schedule"
                       separation= 1.0
                      ;mess1="COUNT="+scount+" and DELTA="+sdelta+". Use DURATION="+sduration+" and schedule"$
                     ENDIF ELSE BEGIN 
                       mess1= "COUNT is "+scount+". Use DELTA="+sdelta+" and schedule"
                     ENDELSE
                     quit = RESPOND_WIDG( /COLUMN, $
                             mess= [mess1, $
                                    "as many instances of OS_NUM="+STRING(os_num,'(I4.4)')+" as possible",$
                                    "between START ("+TAI2UTC(start_value,/ECS)+ $
                                    ") and STOP ("+TAI2UTC(stop_value,/ECS)+") times?"],$
                                    butt=['Yes','No (Cancel)'],$
                                    group=schedv.base)
                     IF (quit NE 0) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='Canceled the ADD command.'
                        PRINT,'Canceled the ADD command.'
                        RETURN,0
                     ENDIF ELSE BEGIN ; user wants to continue. figure out undefined values.
                        IF (delta LE 0) THEN delta= os_duration + separation
                        ;count= FIX( (stop_value - start_value) / delta ) 
                        ; remove floating point error effects by adding 0.00001:
                        IF (stop_value - start_value LT delta) THEN delta= os_duration ;See if can fit one OS
                        count= FIX( (stop_value - start_value)/delta + 0.00001)
                        IF (count LE 0) THEN BEGIN
                          WIDGET_CONTROL,mdiag,SET_VALUE="Sorry - Can't schedule any OS_"+ $
                             STRING(os_num,'(I4.4)')+" in the provided time-range."
                          RETURN,0
                        ENDIF
                     ENDELSE
                   ENDIF

                   ; We get to this point only if COUNT > 0 but delta  may not be (if provided count
                   ; was greater than 1):

                   IF (delta LE 0) THEN delta= (stop_value - start_value) / count 
                   IF (delta LT 0) THEN BEGIN
                      WIDGET_CONTROL,mdiag,SET_VALUE='STOP time LE START time. Ignored the ADD command.'
                      RETURN,0
                   END
                    
                   sdelta= strtrim(string(delta,'(f13.2)'),2)
                   sdelta= sdelta(0)+' sec'

                   IF (count GT 1 AND delta + 0.00001 LT os_duration) THEN BEGIN  ; AEE - 7/31/03
                      WIDGET_CONTROL,mdiag,SET_VALUE="WARNING: DELTA="+sdelta+ $
                                           ", is less than DURATION="+sduration
                      PRINT,"WARNING: DELTA="+sdelta+", is less than DURATION="+sduration 
                      quit = RESPOND_WIDG( /COLUMN, $
                            mess= ["WARNING:", $
                            "DELTA ("+sdelta+") < DURATION ("+sduration+"); Adjust DELTA and/or STOP-time and try again."],$
                            butt=['OK'],$
                            group=schedv.base)
                     WIDGET_CONTROL,mdiag,SET_VALUE='Canceled the ADD command.'
                     PRINT,'Canceled the ADD command.'
                     RETURN,0
                   ENDIF

                   ;IF ((count * delta) GT (stop_value - start_value)) THEN BEGIN

                   IF ( ((count-1) * delta + os_duration) GT (stop_value - start_value) + 0.00001) THEN BEGIN
                     quit = RESPOND_WIDG( /COLUMN, $
                       mess= ["WARNING:", $
                       "Can't fit "+strtrim(string(count),2)+" OS_NUM="+STRING(os_num,'(I4.4)')+" within", $
                      "START ("+TAI2UTC(start_value,/ECS)+") and STOP ("+TAI2UTC(stop_value,/ECS)+") times.",$
                        "Either change the STOP time to "+$
                         ;TAI2UTC(start_value + double(count * delta),/ECS)+$
                         TAI2UTC(start_value + double((count-1) * delta + os_duration),/ECS)+$
                        " or adjust COUNT and/or DELTA.",$
                        "(To ignore this warning and schedule beyond the STOP time using current values "+$
                        "press the CONTINUE button.)"],$ 
                        butt=['CONTINUE (ignore this warning)', 'CANCEL ADD (provide new input)'],$
                        group=schedv.base)
                     IF (quit NE 0) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='Canceled the ADD command.'
                        PRINT,'Canceled the ADD command.'
                        RETURN,0
                     ENDIF
                   ENDIF

skip2hi2:
                   
                   os = os_struct
                   os.os_num = os_num
                   os.os_duration = dos_duration(ind)
                   os.os_size = dos_size(ind)
                 
                   os.os_ro_time= dos_rot(ind)
                   os.os_tele= defined_os_arr(indx(ind)).tele
                   os.os_lp= defined_os_arr(indx(ind)).lp ; AEE - Dec 23, 02, added
                   os.os_images= defined_os_arr(indx(ind)).num_images ;EE - Dec 23, 02, added
                   os.os_cadence= defined_os_arr(indx(ind)).cadence   ;AEE - 01/28/03

                   os.os_proc_time= dos_proc(ind)

                   os.os_setup_time= dos_setup(ind)

                   os.os_pre_proc_time = defined_os_arr(indx(ind)).pre_proc_time ; AEE 6/14/04

		   ;** Calculate start and stop times

                   ; At this point both count and delta are greater than zero. Go ahead and schedule:
                   ; count times, delta apart:


                   orig_start= start_value
                   FOR i=0, count-1 DO BEGIN
                     os.os_start = start_value
                     ;os.os_stop = start_value + os_duration	;** calc stop time
                     os.os_stop = start_value + dos_duration(ind) ;** calc stop time
                     IF (i EQ 0) THEN BEGIN 
                       temp_os= os     
                       temp_start= start_value
                     ENDIF ELSE BEGIN
                       temp_os= [temp_os,os]
                       temp_start= [temp_start,start_value]
                     ENDELSE
                     start_value = start_value + delta
                   ENDFOR

                   IF (schedv.sc EQ 0) THEN temp_os.sc= 'AB'
                   IF (schedv.sc EQ 1) THEN temp_os.sc= 'A' 
                   IF (schedv.sc EQ 2) THEN temp_os.sc= 'B'

                   tmp_conf= [''] ; AEE 10/23/03

                   IF (DATATYPE(os_arr) NE 'INT') THEN BEGIN

                     ; AEE - 8/14/03 - check for calib lamp conflicts:

                     IF (temp_os(0).os_lp EQ 3) THEN BEGIN  ; cal. lamp
                       ; Because of LED controller leak to all instruments, make sure no other exposure
                       ; of any kind in any telescope is in progress (not only other LED images):

                       IF (FIND_CAL_CONFLICTS(os_arr,temp_os,defined_os_arr,"add_os",conf_info)) THEN BEGIN
                         tmp_conf= [tmp_conf,conf_info]
                       ENDIF
                     ENDIF

                    ; Now see any of existing OSes are Calib lamp images and if so check for
                    ; exposure time conflicts with the OS that is being added:

                    cosind= WHERE(os_arr.os_lp EQ 3, coscnt)
                    IF (coscnt GT 0) THEN BEGIN ; one or more of OSes that is not shifted is cal.lamp
                      IF (FIND_CAL_CONFLICTS(os_arr(cosind),temp_os, $
                                             defined_os_arr,"add_os",conf_info)) THEN BEGIN
                        tmp_conf= [tmp_conf,conf_info]
                      ENDIF
                    ENDIF


                    ; Finally, check all of existing OSes for possible CCD confilcts
                    ; with one being added:

                    IF (FIND_CONFLICTS(os_arr,temp_os,defined_os_arr,"add_os",conf_info)) THEN BEGIN
                        tmp_conf= [tmp_conf,conf_info]
                    ENDIF

;                     oind= WHERE(os_arr.os_lp NE 3, ocnt)
;                     IF (ocnt GT 0) THEN BEGIN ; check the non-calib oses for CCD readout conflicts
;                       IF (FIND_CONFLICTS(os_arr(oind),temp_os,defined_os_arr,"add_os",conf_info)) THEN BEGIN
;                         tmp_conf= [tmp_conf,conf_info]
;                       ENDIF
;                     ENDIF

                     IF (N_ELEMENTS(tmp_conf) GT 1) THEN BEGIN
                       tmp_conf= tmp_conf(1:*)
                       WIDGET_CONTROL,mdiag,SET_VALUE="Warning - Scheduling conflict(s)"
                          quit = RESPOND_WIDG( /COLUMN,X_SCROLL_SIZE=630, Y_SCROLL_SIZE=600, $
                          mess= ["WARNING - SCHEDULING CONFLICT","",tmp_conf], $
                          butt=['Add TO Schedule (ignore this warning)', 'Cancel ADD (provide new input)'],$
                          group=schedv.base)
                       IF (quit NE 0) THEN BEGIN
                         WIDGET_CONTROL,mdiag,SET_VALUE='Canceled ADD.'
                         PRINT,'Canceled ADD.'
                         RETURN,0
                       ENDIF ELSE BEGIN
                         all_conflicts= [all_conflicts,tmp_conf]
                       ENDELSE
                     ENDIF

                   ENDIF


                   IF (DATATYPE(os_arr) EQ 'INT') THEN os_arr = temp_os ELSE $
                     os_arr = [os_arr, temp_os]			;** add to current schedule
                   IF (DATATYPE(save_start) EQ 'UND') THEN save_start = temp_start ELSE $
                     save_start = [save_start, temp_start]

                   WIDGET_CONTROL,mdiag,SET_VALUE="Added/Scheduled "+strtrim(string(count),2)+" os_"+$
                      strtrim(string(os_num),2)+" at "+sdelta+" apart between "+$
                      TAI2UTC(orig_start,/ECS)+" and "+TAI2UTC(stop_value,/ECS)+"."

                  ENDFOR ; ind_cnt (HI Seq)

  RETURN,1
END


FUNCTION REMOVE_OS, schedv, no_update=no_update

COMMON OP_SCHEDULED
COMMON OP_DEFINED

COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON OS_PLOT_SHARE

COMMON KAP_INIT
COMMON KAP_INPUT
COMMON DIALOG,mdiag,font

COMMON CMD_SEQ_SCHEDULED, sched_cmdseq

COMMON SCIP_DOORS_SHARE, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
                         sc_maneuvers,start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2 

;COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt ; AEE - 7/24/03
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt ; AEE - 0/01/06 
COMMON SCHED_CONF, all_conflicts ; AEE - 10/21/03
COMMON OS_ALL_AB, remain_os_arr, all_defined_os_arr, all_defined_set_arr, all_sched_cmdseq

 	                IF (DATATYPE(os_arr) EQ 'INT') THEN RETURN,0

                        WIDGET_CONTROL, /HOUR
 	                WIDGET_CONTROL, schedv.start_text, GET_VALUE=start_value
	   	        WIDGET_CONTROL, schedv.stop_text,  GET_VALUE=stop_value	
                        WIDGET_CONTROL, schedv.num_text,   GET_VALUE=num_text
                        os_num = long(STRTRIM(num_text[0], 2))
;                        os_num = LONG(STRMID(num_text, 7, STRLEN(num_text)-7))

	   	        start_value = UTC2TAI(start_value(0))	; ASCII to TAI
	   	        stop_value  = UTC2TAI(stop_value(0))	; ASCII to TAI

                        be_val= TAI2UTC(start_value,/ECS)+' and '+TAI2UTC(stop_value,/ECS) 

		        ;quit = VERIFY_TIMES(baseop=schedv.baseop, $
			;		  study_start=start_value, $
			;		  startonly=1)
			quit = 0

 		        IF NOT(quit) THEN BEGIN	;** delete all studies with this OS_NUM between start & stop 
                           keep_os = WHERE( (os_arr.os_num NE os_num) OR $
                                            (os_arr.os_start LT start_value) OR $
                                            (os_arr.os_stop  GT stop_value+0.0005) )
                                            ;(os_arr.os_start GT (stop_value+1)) )

;help,os_arr
;print,os_num
;print,TAI2UTC(os_arr.os_start,/ECS)
;print,TAI2UTC(start_value,/ECS)
;print,TAI2UTC(os_arr.os_stop,/ECS)
;print,TAI2UTC(stop_value+0.0005,/ECS)
;help,keep_os
;stop

                           ; AEE 5/23/03 - don't delete an OS if one of them is part of lp=7:

                           osr= BLK_SEQ_OS(os_arr,sched_cmdseq,os_num,start_value,stop_value+0.0005)
			   help,os_num,keep_os,osr
			   
			   
                           IF (osr NE 0) THEN BEGIN
                             WIDGET_CONTROL,mdiag,SET_VALUE='os_num = '+ STRN(osr,FORMAT='(I4.4)')+ $
                           ' belongs to a Block Sequence and can NOT be removed.'+ $
                           ' Delete Command ignored. Use "DEL MULT OS''s".'
                             PRINT,'os_num = '+STRN(osr,FORMAT='(I4.4)')+ $
                           ' belongs to a Block Sequence and can NOT be removed.'+ $ 
                           ' Delete Command ignored. Use "DEL MULT OS''s".''
                              
                             RETURN,0
                           ENDIF

                           ; When deleting a S/C A or B OSnum and the OSnum is synced (has .sc='AB'), 
                           ; then keep the OSnum for the other S/C (instead of removing it from both):

                           del_os= WHERE((os_arr.os_num EQ os_num) AND $
                                        ((os_arr.os_start GE start_value) AND $
                                        (os_arr.os_start LE (stop_value+0.0005))), dcnt) 
                           IF (schedv.sc NE 0 ) THEN BEGIN ; not AB 
                             FOR dadd= 0, dcnt-1 DO BEGIN 
                               IF (os_arr(del_os(dadd)).sc EQ 'AB') THEN BEGIN
                                 IF (schedv.sc EQ 1) THEN sc= 'B'
                                 IF (schedv.sc EQ 2) THEN sc= 'A'
                                 os2keep= os_arr(del_os(dadd))
                                 os2keep.sc= sc
                                 IF (DATATYPE(remain_os_arr) EQ 'STC') THEN $
                                   remain_os_arr= [remain_os_arr,os2keep] $
                                 ELSE $
                                   remain_os_arr= os2keep 
                               ENDIF 
                             ENDFOR
                           ENDIF

                           IF (keep_os(0) LT 0) THEN BEGIN
                              os_arr = -1
                              WIDGET_CONTROL, /HOUR
                              ;SCHEDULE_PLOT, schedv
                              IF (NOT KEYWORD_SET(no_update)) THEN SCHEDULE_PLOT, schedv ; AEE - 7/31/03
                              WIDGET_CONTROL,mdiag,SET_VALUE='Deleted all OS''s between '+be_val+'.'
                              PRINT, 'Deleted all OS''s between '+be_val+'.'
tmp_conf=all_conflicts
                              ;CHECK_CONFLICTS,all_conflicts,os_num,be_val   ; AEE 10/22/03
                              all_conflicts=['']  ; all oses are removed so there are no conflicts left.

                           ENDIF ELSE IF (N_ELEMENTS(keep_os) EQ N_ELEMENTS(os_arr)) THEN BEGIN
                              ;WIDGET_CONTROL,mdiag,SET_VALUE='No matching OS to delete between '+be_val+'.'
                              ;PRINT, 'No matching OS to delete between '+be_val+'.'
                              WIDGET_CONTROL,mdiag,SET_VALUE='No matching OS_'+STRTRIM(os_num,2)+' to delete between '+be_val+'.'
                               PRINT, 'No matching OS_'+STRTRIM(os_num,2)+' to delete between '+be_val+'.'
                           ENDIF ELSE BEGIN
                              ;** remove the matching OS's from the os_arr and re-plot
                              os_arr = os_arr(keep_os)
                              WIDGET_CONTROL, /HOUR
                              ;SCHEDULE_PLOT, schedv
                              IF (NOT KEYWORD_SET(no_update)) THEN SCHEDULE_PLOT, schedv ; AEE - 7/31/03
                              ;WIDGET_CONTROL,mdiag,SET_VALUE='Removed the matching OS''s between '+be_val+'.'
                              ;PRINT, 'Removed the matching OS''s between '+be_val+'.'
                              WIDGET_CONTROL,mdiag,SET_VALUE='Removed the matching OS_'+STRTRIM(os_num,2)+' between '+be_val+'.'
                              PRINT, 'Removed the matching OS_'+STRTRIM(os_num,2)+' between '+be_val+'.'
tmp_conf=all_conflicts
                              CHECK_CONFLICTS,all_conflicts,os_num,be_val  ; AEE 10/22/03
                           ENDELSE
 		        ENDIF
    RETURN,1
END


;--------------------------------------------------------------------------------------------------

PRO SCHEDULE_EVENT, event

COMMON OP_SCHEDULED
COMMON OP_DEFINED

COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON OS_PLOT_SHARE

COMMON OS_ALL_AB, remain_os_arr, all_defined_os_arr, all_defined_set_arr, all_sched_cmdseq

COMMON KAP_INIT
COMMON KAP_INPUT
COMMON SCA_NAMES, sca_fname, scb_fname
COMMON DIALOG,mdiag,font

COMMON CMD_SEQ_SCHEDULED, sched_cmdseq

COMMON SCIP_DOORS_SHARE, doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, cdoor_cor2, odoor_cor2,$
                         sc_maneuvers,start_sched, stop_sched, sc_euvi, sc_cor1, sc_cor2 
COMMON PREV_SCIP_DOORS_SHARE, pdoors, pcdoor_euvi, podoor_euvi, pcdoor_cor1, podoor_cor1, pcdoor_cor2, $
                              podoor_cor2
 

;COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt ; AEE - 7/24/03
COMMON GT_DUMPS_SHARE, stime_gt, etime_gt, apid_gt, beg_sched, end_sched, sc_gt, rate_gt, bsf_gt ; AEE - 09/01/06 
;COMMON PREV_GT_DUMPS_SHARE, pstime_gt, petime_gt, papid_gt, psc_gt
COMMON PREV_GT_DUMPS_SHARE, pstime_gt, petime_gt, papid_gt, psc_gt, prate_gt, pbsf_gt

COMMON SCHED_CONF, all_conflicts ; AEE - 10/21/03
COMMON SCHED_SHARE, schedv ; 12/08/03 - AEE added and commented out the ....., GET_UVALUE=schedv below.
COMMON APIDS, multi_apid 
COMMON SET_DEFINED, set_instance, defined_set_arr
COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt
COMMON FULL_OS, tosarr
COMMON INIT_TL, ic
COMMON RUN_SCRIPT, rsc

 WIDGET_CONTROL, event.id,  GET_UVALUE=input	; get the UVALUE of the event 
						;     generated by the user
;   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input

help,input
 CASE (input) OF 

   'QUIT'   : BEGIN
		;
		; Destroy the planning tool interface and all grouped widgets
  		; 
                resp = RESPOND_WIDG( /COLUMN, $
                       TITLE='Quit PT?', $
                mess= ["Really quit the PT session?","(An unsaved schedule will be lost.)"], $
                butt= ['Yes','No'],group=schedv.base)
                IF (resp EQ 0) THEN WIDGET_CONTROL, event.top, /DESTROY
		RETURN
	      END

   'HELP'   : BEGIN
                filename= GETENV('PT')+'/IN/OTHER/'+'help_menu.dat'
                XDISPLAYFILE,filename,TITLE='Help Menu',$
                GROUP=event.top, HEIGHT=30, WIDTH=110
              END

   'VERIFY' : BEGIN
                ; NOTE: VERIFY only outputs couple of formats of the scheduled OSes to the screen. It does
                ;       not check for conflicts, etc - just a visual verification.
		WIDGET_CONTROL, /HOUR
                ;OS_SUMMARY_TEXT
                ;OS_SUMMARY_1LINE   ; since no output filename is provided, output will be writeen to screen.
                GET_UTC,sdate
                sfname= 'summary.'+STRTRIM(sdate.time,2)
                OS_SUMMARY_1LINE,sfname
                ;f= FINDFILE(GETENV('PT')+'/OUT/SUMMARY/'+sfname)
                IF (schedv.sc EQ 0) THEN sc= 'SC_A' ; these will be deleted anyway.
                IF (schedv.sc EQ 1) THEN sc= 'SC_A'
                IF (schedv.sc EQ 2) THEN sc= 'SC_B'
                f= FINDFILE(GETENV('PT')+'/OUT/'+sc+'/SUMMARY/'+sfname)
                IF (f(0) NE '') THEN BEGIN
                  IF (schedv.sc EQ 0) THEN sc= ' (S/C AB)'
                  IF (schedv.sc EQ 1) THEN sc= ' (S/C A)'
                  IF (schedv.sc EQ 2) THEN sc= ' (S/C B)'
                  ;XDISPLAYFILE,f(0),TITLE='Summary of Current Schedule',$
                  XDISPLAYFILE,f(0),TITLE='Summary of Current Schedule'+ sc,$
                  ;GROUP=event.top, HEIGHT=30, WIDTH=120
                  ;GROUP=event.top, HEIGHT=30, WIDTH=130
                  ;GROUP=event.top, HEIGHT=30, WIDTH=160
                  GROUP=event.top, HEIGHT=30, WIDTH=170
                  OPENR,slun,f(0),/GET_LUN, /DELETE 
                  CLOSE, slun
                  FREE_LUN, slun
                ENDIF
	      END

   'STORE' : BEGIN
		WIDGET_CONTROL, /HOUR
 
                filename= GETENV('PT')+'/IN/OTHER/'+'os_arr.sav'
                SAVE, FILENAME= filename, os_arr, defined_os_arr, multi_apid, sched_cmdseq, $
                                             a_bsf_cnt, b_bsf_cnt, $
                                             doors, cdoor_euvi, odoor_euvi, cdoor_cor1, odoor_cor1, $
                                             cdoor_cor2, odoor_cor2,sc_maneuvers,start_sched, stop_sched,$
                                             sc_euvi,sc_cor1,sc_cor2, $
                                             stime_gt, etime_gt, apid_gt, rate_gt, sc_gt, beg_sched, end_sched,$
                                              ic, rsc, bsf_gt 

                ;SAVE, FILENAME='set_arr.sav', defined_set_arr
                filename= GETENV('PT')+'/IN/OTHER/'+'set_arr.sav'
                SAVE, FILENAME= filename, defined_set_arr

                WIDGET_CONTROL,mdiag,SET_VALUE='Saved current schedule in os_arr.sav set_arr.sav.'
                PRINT,'Saved current schedule in os_arr.sav and set_arr.sav.'
	     END


   'OUT_BSF': BEGIN
                ; Uses current day to create individual IPT files from last IPT file created.
		; OS_*.IPT files are created in $PT/IO/OSID/.

                ; Note: expand_bsf keyword in call to generate_ipt prevents a cmdseq file, 
                ;       if any exists in the schedule, from collapsing into a single BSF
                ;       entry because I don't allow nested BSF files.

                WIDGET_CONTROL, /HOUR

                WIDGET_CONTROL,mdiag,SET_VALUE='Generating BSF file......'
                readtime=0
                delay=0
                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                  delay= CHECK_DELAY(os_arr, readtime)
                  IF (delay GT 0) THEN BEGIN
                    mess1= 'FSW needs '+STRTRIM(readtime,2)+' seconds to'+$
                            ' read in schedule; Schedule must be shifted by at least +'+STRTRIM(delay,2)
                    quit = RESPOND_WIDG( /COLUMN, $
                             mess= mess1, $
                                   butt=['OK (user will shift schedule)', 'Ignore Shift (make schedule anyway)'],$
                                   group=schedv.base)
                    IF (quit EQ 0) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='OK - Will Shift Schedule'
                        PRINT,'OK - User Will Shift Schedule'
                        RETURN
                    ENDIF ELSE BEGIN ; user wants to continue.
                      WIDGET_CONTROL,mdiag,SET_VALUE='Shift was Ignored - Make Schedule'
                      PRINT,'Shift was Ignored - Make Schedule'
                    ENDELSE
                  ENDIF
                ENDIF

                ;IF (STRPOS(ipt_name,"FYI: Detected") GE 0) THEN RETURN
                IF (STRPOS(ipt_name,"Door Schedule Overlap") GE 0) THEN RETURN
                IF (STRPOS(ipt_name,"Empty Schedule") GE 0) THEN RETURN

		;*************************************************
		;** form IPT filename root SECYYYYMMDD
		;**
		utc = TAI2UTC(schedv.startdis, /EXTERNAL)
		ipt_file_name = 'SEC'+STRN(utc.year)+STRN(utc.month,FORMAT='(I2.2)')+STRN(utc.day,FORMAT='(I2.2)')

		;*************************************************
		;** check for previous versions
   
                IF (schedv.sc EQ 0) THEN sc='SC_AB/'
                IF (schedv.sc EQ 1) THEN sc='SC_A/'
                IF (schedv.sc EQ 2) THEN sc='SC_B/'
                f= FINDFILE(GETENV('PT')+'/IO/IPT/'+sc+ipt_file_name+'*.IPT')
		nf=n_elements(f)
                f= f(nf-1)
    	    	IF f EQ '' THEN $
                    GENERATE_IPT, schedv.startdis, schedv.enddis, ipt_name, /expand_bsf ELSE $
		    ipt_name=f
    	    	help,ipt_name
    	    	wait,3
                IF (STRLEN(STRMID(f,STRPOS(f,'SEC'),200)) NE 18) THEN BEGIN ; Not a valid SEC*.IPT file
                  WIDGET_CONTROL,mdiag,SET_VALUE="Empty Schedule - Did not generate BSF file."
                  PRINT, "Empty Schedule - Did not generate BSF file."
                ENDIF ELSE BEGIN
                  filename= GETENV('PT')+'/IN/OTHER/'+'launched_date.sav'
                  RESTORE,filename ; => launch_date (2006 for now)
                  GENERATE_ITOS_SCHEDULE,ipt_name,launch_date,readtime,/RTS,/BSF
                ENDELSE

            END

   'INIT_TL': BEGIN
                ;IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                ;  begt= UTC2STR(TAI2UTC(MIN(os_arr.os_start)),/ECS,/TRUNCATE) ; truncate removes .msec
                ;  endt= UTC2STR(TAI2UTC(MAX(os_arr.os_stop)),/ECS,/TRUNCATE)
                ;ENDIF ELSE BEGIN
                  begt= UTC2STR(TAI2UTC(schedv.startdis),/ECS,/TRUNCATE)
                  endt= UTC2STR(TAI2UTC(schedv.enddis),/ECS,/TRUNCATE)
                  ;WIDGET_CONTROL,mdiag,SET_VALUE="Empty Schedule - Using Display Time Range."
                ;ENDELSE
                itl_date= INIT_TIMELINE(begt,endt, schedv.sc)
                ;help,itl_date
                IF (itl_date NE '') THEN BEGIN
                  CASE schedv.sc OF
                    0: tsc='SC-AB'
                    1: tsc='SC-A'
                    2: tsc='SC-B'
                  ENDCASE
                  ;IF (itl_date NE 'remove') THEN BEGIN
                  IF (STRPOS(itl_date,'remove') LT 0) THEN BEGIN
                    WIDGET_CONTROL,mdiag,SET_VALUE="Added InitTimeline Command @ "+itl_date+" to "+tsc
                    CASE schedv.sc OF
                      0: BEGIN
                           ic= [ic,{sc:'A',dt:UTC2TAI(STR2UTC(itl_date)),bsf:0}] 
                           ic= [ic,{sc:'B',dt:UTC2TAI(STR2UTC(itl_date)),bsf:0}]
                         END
                      1: BEGIN
                           ic= [ic,{sc:'A',dt:UTC2TAI(STR2UTC(itl_date)),bsf:0}]
                         END
                      2: BEGIN
                           ic= [ic,{sc:'B',dt:UTC2TAI(STR2UTC(itl_date)),bsf:0}]
                         END
                    ENDCASE
                    ;help,ic
                    ;stop
                  ENDIF ELSE BEGIN 
                    IF (itl_date EQ 'remove') THEN $ 
                      WIDGET_CONTROL,mdiag,SET_VALUE="Removed InitTimeline Command(s) from "+tsc $
                    ELSE $
                      WIDGET_CONTROL,mdiag,SET_VALUE="Did not find a InitTimeline Command to Remove." 
                  ENDELSE
                  WIDGET_CONTROL, /HOUR
                  SCHEDULE_PLOT, schedv
                ENDIF ELSE BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE="Cancelled InitTimeline Command."
                ENDELSE
            END

   'OUTPUT_PLAN' : BEGIN
		WIDGET_CONTROL, /HOUR
                WIDGET_CONTROL,mdiag,SET_VALUE='Generating command files......'
                readtime=0
                ;delay=0
                ;IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                ;  delay= CHECK_DELAY(os_arr, readtime)
                ;  IF (delay GT 0) THEN BEGIN
                ;    WIDGET_CONTROL,mdiag,SET_VALUE='FSW needs '+STRTRIM(readtime,2)+' seconds to'+ $
                ;     ' read in schedule; Schedule must be shifted by at least +'+STRTRIM(delay,2)+ $
                ;     ' seconds - Command was Ignored.'
                ;    RETURN
                ;  ENDIF
                ;ENDIF
                delay=0
                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                  delay= CHECK_DELAY(os_arr, readtime)
                  IF (delay GT 0) THEN BEGIN
                    mess1= 'FSW needs '+STRTRIM(readtime,2)+' seconds to'+$
                            ' read in schedule; Schedule must be shifted by at least +'+STRTRIM(delay,2)
                    quit = RESPOND_WIDG( /COLUMN, $
                             mess= mess1, $
                                   butt=['OK (user will shift schedule)', 'Ignore Shift (make schedule anyway)'],$
                                   group=schedv.base)
                    IF (quit EQ 0) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='OK - Will Shift Schedule'
                        PRINT,'OK - User Will Shift Schedule'
                        RETURN
                    ENDIF ELSE BEGIN ; user wants to continue.
                      WIDGET_CONTROL,mdiag,SET_VALUE='Shift was Ignored - Make Schedule'
                      PRINT,'Shift was Ignored - Make Schedule'
                    ENDELSE
                  ENDIF
                ENDIF

                GENERATE_IPT, schedv.startdis, schedv.enddis, ipt_name
		;IF (STRPOS(ipt_name,"FYI: Detected") GE 0) THEN RETURN IF (STRPOS(ipt_name,"Door Schedule Overlap") GE 0) THEN RETURN
                IF (STRPOS(ipt_name,"Empty Schedule") GE 0) THEN RETURN
                IF (schedv.sc EQ 0) THEN sc='SC_AB/'
                IF (schedv.sc EQ 1) THEN sc='SC_A/'
                IF (schedv.sc EQ 2) THEN sc='SC_B/'
                f= FINDFILE(GETENV('PT')+'/IO/IPT/'+sc+ipt_name)
                f= f(0)
                IF (STRLEN(STRMID(f,STRPOS(f,'SEC'),200)) NE 18) THEN BEGIN ; Not a valid SEC*.IPT file
                  WIDGET_CONTROL,mdiag,SET_VALUE="Empty Schedule - Did not generate command files."
                  PRINT, "Empty Schedule - Did not generate command files."   
                ENDIF ELSE BEGIN
                  filename= GETENV('PT')+'/IN/OTHER/'+'launched_date.sav'
                  RESTORE,filename ; => launch_date (2006)

                  GENERATE_CONF, schedv.startdis, schedv.enddis, ipt_name+'.conflicts',/all_files ; AEE 11/17/03

                  GENERATE_ITOS_SCHEDULE,ipt_name,launch_date      ; AEE 1/27/04 
                  GENERATE_ITOS_SCHEDULE,ipt_name,launch_date,readtime,/RTS ; AEE 1/27/04
                  GENERATE_SETS_SUMMARY,ipt_name ; AEE 2/20/04  generate a .summary.SETS file, if sets exist.
                  GENERATE_IPT_SET,os_arr ; AEE 2/12/04
    	    	  GENERATE_IPT_SINGLES,f
                  pos = STRPOS(ipt_name, 'IPT')
                  fname = STRMID(ipt_name, 0, pos) + 'summary'
                  OS_SUMMARY_1LINE, fname
                  ;fname = STRMID(ipt_name, 0, pos) + 'gif'
                  ;WSET, schedv.win_index
                  ;img = TVRD(0,0,700,512+20)
                  ;img = TVRD(0,0,700,512+50)  ; AEE - 7/28/03
                  ;TVLCT, r, g, b, /GET
                  ;IF (schedv.sc EQ 1) THEN sc= 'SC_A'
                  ;IF (schedv.sc EQ 2) THEN sc= 'SC_B'
                  ;WRITE_GIF, GETENV('PT')+'/OUT/'+sc+'/WEB/'+fname, img, r, g, b

                  ; Generate a .png file:

                  pngname= GETENV('PT')+'/OUT/'+sc+'/WEB/'+STRMID(ipt_name, 0, pos-1)
                  ;img = EDFTVREAD(0,0,1200,735+40,/PNG,FILENAME=pngname)
                  img = EDFTVREAD(0,0,1200,735+110,/PNG,FILENAME=pngname) ; so BSF section is also saved.
                  ; Now, image is captured in pngname+.png (.png is added to pngname)
                  ; and img is -1. 
                  PRINT,'Generated  ../OUT/'+sc+'/WEB/'+STRMID(ipt_name, 0, pos-1)+'.png'

                  PRINT,'"Generate Command Files" Done.'
                  WIDGET_CONTROL, mdiag, SET_VALUE= '"Generate Command Files" Done.'
                ENDELSE
	      END

   'OUTPUT_IPT' : BEGIN
		WIDGET_CONTROL, /HOUR
                GENERATE_IPT, schedv.startdis, schedv.enddis, ipt_name
                ;IF (STRPOS(ipt_name,"FYI: Detected") GE 0) THEN RETURN
                IF (STRPOS(ipt_name,"Door Schedule Overlap") GE 0) THEN RETURN
                IF (STRPOS(ipt_name,"Empty Schedule") GE 0) THEN RETURN
                IF (schedv.sc EQ 0) THEN sc='SC_AB/'
                IF (schedv.sc EQ 1) THEN sc='SC_A/'
                IF (schedv.sc EQ 2) THEN sc='SC_B/'
                f= FINDFILE(GETENV('PT')+'/IO/IPT/'+sc+ipt_name)
                f= f(0)
                IF (STRLEN(STRMID(f,STRPOS(f,'SEC'),200)) NE 18) THEN BEGIN ; Not a valid SEC*.IPT file
                  WIDGET_CONTROL,mdiag,SET_VALUE="Empty Schedule - Did not generate IPT file."
                  PRINT, "Empty Schedule - Did not generate IPT file."   
                ENDIF ELSE BEGIN
                    GENERATE_IPT_SINGLES,f
	      	ENDELSE
	      END

   'conf_out'   : BEGIN ; AEE 11/17/03
                WIDGET_CONTROL, /HOUR
                GENERATE_CONF, schedv.startdis, schedv.enddis, ipt_name
              END


   'OUTPUT_IAP' : BEGIN
		WIDGET_CONTROL, /HOUR
                GENERATE_IAP, schedv.startdis, schedv.enddis
	      END

   'INPUT_IPT' : BEGIN
                IF (schedv.sc EQ 0) THEN sc='SC_AB/'
                IF (schedv.sc EQ 1) THEN sc='SC_A/'
                IF (schedv.sc EQ 2) THEN sc='SC_B/'
                fname = PICKFILE(PATH= GETENV('PT')+'/IO/IPT/'+sc, FILTER='*.IPT')
                IF (STRPOS(fname,'SEC2') GE 0) THEN BEGIN
		   WIDGET_CONTROL, /HOUR
                   ;fname= STRMID(fname,STRPOS(fname,'SEC2'),30) ; drop the path
		   WIDGET_CONTROL, /HOUR
                   SCHEDULE_READ_IPT, fname, schedv.startdis, /SILENT, /PROMPT_SHIFT ; AEE 1/8/04
                   SCHEDULE_PLOT, schedv
                ENDIF
	      END

   'SCHEDULE_DOORS' : BEGIN

                ; following sc_maneuvers is a dummy array. I should get the real
                ; dates from the S/C maneuvers file(s). *** See read_kap.pro and
                ; schedule_plot.pro which use DEFAULT.KAP to read all of the anci. info and plot.

                ;sc_maneuvers=['2003/05/31 12:00:00 - 2003/05/31 22:00:00', $
                ;              '2003/06/02 00:00:00 - 2003/06/05 00:00:00']

                sc_maneuvers=['None Scheduled']

                id = WHERE(kap_resource_names EQ "SPACECRAFT_MANEUVER") & id = id(0)
                mind = WHERE(kap_resource_arr.id EQ id, mcnt)
                IF (mcnt GT 0) THEN BEGIN
                  mind = REVERSE(SORT(kap_resource_arr(mind).startime))
                  FOR i= 0, mcnt-1 DO BEGIN
                    mbeg= STRMID(UTC2STR(TAI2UTC(kap_resource_arr(mind(i)).startime),/ECS),0,19) ;rm msec
                    mend= STRMID(UTC2STR(TAI2UTC(kap_resource_arr(mind(i)).endtime),/ECS),0,19) ;rm msec 
                    sc_maneuvers=[sc_maneuvers,mbeg+' - '+mend]
                  ENDFOR
                  sc_maneuvers= sc_maneuvers(1:*)
                ENDIF 

                ;WIDGET_CONTROL, schedv.start_text, GET_VALUE=start_sched
                ;start_sched= STRMID(start_sched(0),0,19) ; remove msec
                start_sched= UTC2STR(TAI2UTC(schedv.startdis), /ECS, /TRUNCATE) ; TRUNCATE removes msec.

                ;WIDGET_CONTROL, schedv.stop_text, GET_VALUE=stop_sched
                ;stop_sched= STRMID(stop_sched(0),0,19) ; remove msec
                stop_sched= UTC2STR(TAI2UTC(schedv.enddis), /ECS, /TRUNCATE)

                ;doors= {scip_doors,euvi_cl:0D,euvi_op:0D,cor1_cl:0D,cor1_op:0D,cor2_cl:0D,cor2_op:0D}
                doors= {scip_doors,euvi_cl:0D,euvi_op:0D,cor1_cl:0D,cor1_op:0D,cor2_cl:0D,cor2_op:0D}

                ; Keep current door stuff to be used again (incase cancel in update_sched_doors was
                ; issued after changes to doors stuff were made):

                pdoors= doors
                IF (DATATYPE(cdoor_euvi) NE 'UND') THEN pcdoor_euvi= cdoor_euvi
                IF (DATATYPE(odoor_euvi) NE 'UND') THEN podoor_euvi= odoor_euvi
                IF (DATATYPE(sc_euvi) NE 'UND') THEN psc_euvi= sc_euvi
                IF (DATATYPE(cdoor_cor1) NE 'UND') THEN pcdoor_cor1= cdoor_cor1
                IF (DATATYPE(odoor_cor1) NE 'UND') THEN podoor_cor1= odoor_cor1
                IF (DATATYPE(sc_cor1) NE 'UND') THEN psc_cor1= sc_cor1
                IF (DATATYPE(cdoor_cor2) NE 'UND') THEN pcdoor_cor2= cdoor_cor2
                IF (DATATYPE(cdoor_cor2) NE 'UND') THEN podoor_cor2= odoor_cor2
                IF (DATATYPE(sc_cor2) NE 'UND') THEN psc_cor2= sc_cor2

                status= UPDATE_SCHED_DOORS()

                IF (status) THEN BEGIN

                  WIDGET_CONTROL, /HOUR
                  SCHEDULE_PLOT, schedv

                  ; NOTES:
                  ; 1. Must also Add door close/open to .IPT, .GIF, etc. and MAKE SURE 
                  ;    that for door dates that are before and after the current schedule  
                  ;    which are valid dates (but indicated as <--  and  --> in the display) 
                  ;    nothing is scheduled in .IPT file that is beyond the schedule range.
                  ; 2. Also MUST allow reading the door close/open stuff from an .IPT (if any exists). 


                ENDIF ELSE BEGIN ; status
                  ; update_sched_doors was canceled - reuse previous door values in case some where changed:
                  doors= pdoors

                  cdoor_euvi= 0 ; set to 0 (INT) instead of 0D (double) since 'DUB' datatype is used 
                                ; in os_summary_1line, os_summary_text, and generate_ipt to
                                ; check these variable and 0 (int datatype) means invalid (don't schedule).

                  IF (DATATYPE(pcdoor_euvi) NE 'UND') THEN cdoor_euvi= pcdoor_euvi
                  odoor_euvi= 0 ; (don't use 0D) 
                  IF (DATATYPE(podoor_euvi) NE 'UND') THEN odoor_euvi= podoor_euvi
                  sc_euvi= '' ; (don't use 0D)
                  IF (DATATYPE(psc_euvi) NE 'UND') THEN sc_euvi= psc_euvi
                  cdoor_cor1= 0 ; (don't use 0D) 
                  IF (DATATYPE(pcdoor_cor1) NE 'UND') THEN cdoor_cor1= pcdoor_cor1
                  odoor_cor1= 0 ; (don't use 0D) 
                  IF (DATATYPE(podoor_cor1) NE 'UND') THEN odoor_cor1= podoor_cor1
                  sc_cor1= '' ; (don't use 0D)
                  IF (DATATYPE(psc_cor1) NE 'UND') THEN sc_cor1= psc_cor1
                  cdoor_cor2= 0 ; (don't use 0D) 
                  IF (DATATYPE(pcdoor_cor2) NE 'UND') THEN cdoor_cor2= pcdoor_cor2
                  odoor_cor2= 0 ; (don't use 0D) 
                  IF (DATATYPE(podoor_cor2) NE 'UND') THEN odoor_cor2= podoor_cor2
                  sc_cor2= '' ; (don't use 0D)
                  IF (DATATYPE(psc_cor2) NE 'UND') THEN sc_cor2= psc_cor2
                ENDELSE ; status

              END

   'GT_DUMPS' : BEGIN


                ;WIDGET_CONTROL, schedv.start_text, GET_VALUE=beg_sched
                ;beg_sched= STRMID(beg_sched(0),0,19) ; remove msec
                beg_sched= UTC2STR(TAI2UTC(schedv.startdis), /ECS, /TRUNCATE) ; TRUNCATE removes msec.

                ;WIDGET_CONTROL, schedv.stop_text, GET_VALUE=end_sched
                ;end_sched= STRMID(end_sched(0),0,19) ; remove msec
                end_sched= UTC2STR(TAI2UTC(schedv.enddis), /ECS, /TRUNCATE)

                ; Keep current GT dump stuff to be used again (incase cancel in schedule_gt_dump was
                ; issued after changes to existing values were made):

                IF (DATATYPE(stime_gt) NE 'UND') THEN pstime_gt= stime_gt
                IF (DATATYPE(etime_gt) NE 'UND') THEN petime_gt= etime_gt
                IF (DATATYPE(apid_gt) NE 'UND') THEN papid_gt= apid_gt
                IF (DATATYPE(rate_gt) NE 'UND') THEN prate_gt= rate_gt
                IF (DATATYPE(sc_gt) NE 'UND') THEN psc_gt= sc_gt
                IF (DATATYPE(bsf_gt) NE 'UND') THEN pbsf_gt= bsf_gt

                status= SCHEDULE_GT_DUMP()

                IF (status) THEN BEGIN
                  WIDGET_CONTROL, /HOUR
                  SCHEDULE_PLOT, schedv

                  ; NOTES:
                  ; 1. Must also Add GT dumps in .IPT, .GIF, etc. 
                  ; 2. Also MUST allow reading the GT dumpd stuff from an .IPT (if any exists). 

                ENDIF ELSE BEGIN ; status
                  ; schedule_gt_dump was canceled - reuse previous values in case some where changed:
                  stime_gt= 0  ; (don't use 0D)'
                  IF (DATATYPE(pstime_gt) NE 'UND') THEN stime_gt= pstime_gt
                  etime_gt= 0  ; (don't use 0D)'
                  IF (DATATYPE(petime_gt) NE 'UND') THEN etime_gt= petime_gt
                  apid_gt= ''
                  IF (DATATYPE(papid_gt) NE 'UND') THEN apid_gt= papid_gt
                  IF (DATATYPE(psc_gt) NE 'UND') THEN sc_gt= psc_gt
                  rate_gt= 0
                  IF (DATATYPE(prate_gt) NE 'UND') THEN rate_gt= prate_gt
                  bsf_gt= ''
                  IF (DATATYPE(pbsf_gt) NE 'UND') THEN bsf_gt= pbsf_gt
                ENDELSE ; status
;help,sc_gt,stime_gt,etime_gt,apid_gt,rate_gt,bsf_gt
;stop

              END


   'SCHEDULE_RS' : BEGIN ; Schedule a Run Script file

                rsfile= '' 

                ;IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                ;  rs_date= UTC2STR(TAI2UTC(MIN(os_arr.os_start)),/ECS,/TRUNCATE) ; truncate removes .msec
                ;ENDIF ELSE BEGIN
                  rs_date= UTC2STR(TAI2UTC(schedv.startdis),/ECS,/TRUNCATE)
                ;ENDELSE
                SCHED_RUN_SCRIPT, rsfile, rs_date, schedv.sc 
                IF (rs_date NE 'cancel') THEN BEGIN
                  CASE schedv.sc OF
                    0: tsc='SC-AB'
                    1: tsc='SC-A'
                    2: tsc='SC-B'
                  ENDCASE
                  ;IF (rs_date NE 'remove') THEN BEGIN
                  IF (STRPOS(rs_date,'remove') LT 0) THEN BEGIN
                    WIDGET_CONTROL,mdiag,SET_VALUE="Added "+rsfile+" @ "+rs_date+" to "+tsc
                    CASE schedv.sc OF
                      0: BEGIN
                           rsc= [rsc,{sc:'A',dt:UTC2TAI(STR2UTC(rs_date)),fn:rsfile,bsf:0}]
                           rsc= [rsc,{sc:'B',dt:UTC2TAI(STR2UTC(rs_date)),fn:rsfile,bsf:0}]
                         END
                      1: BEGIN
                           rsc= [rsc,{sc:'A',dt:UTC2TAI(STR2UTC(rs_date)),fn:rsfile,bsf:0}]
                         END
                      2: BEGIN
                           rsc= [rsc,{sc:'B',dt:UTC2TAI(STR2UTC(rs_date)),fn:rsfile,bsf:0}]
                         END
                    ENDCASE
                    ;stop
                  ENDIF ELSE BEGIN  ; remove
                    IF (rs_date EQ 'remove') THEN $
                      WIDGET_CONTROL,mdiag,SET_VALUE="Removed Run Script File(s) from "+tsc $
                    ELSE $
                      WIDGET_CONTROL,mdiag,SET_VALUE="Did not find a Run Script File to Remove."
                  ENDELSE
                  WIDGET_CONTROL, /HOUR
                  SCHEDULE_PLOT, schedv
                ENDIF ELSE BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE="Cancelled Schedule/Remove Run Script Command."
                ENDELSE

	      END


   'SCHEDULE_BS' : BEGIN

                bsfile= '' 
                ;WIDGET_CONTROL, schedv.start_text, GET_VALUE=start_time
                ;start_time= start_time(0)
                ;ostart= start_time

                ostart= UTC2STR(TAI2UTC(schedv.startdis),/ECS)
                start_time= ostart
              
                ;status= BSF(bsfile,start_time) ; AEE - Dec 29, 03
                status= BSF(bsfile,start_time,n_sched,bsf_cad) ; AEE - Dec 29, 03
                orig_start_time= start_time
                ;IF (status AND start_time GE ostart ) THEN BEGIN
                IF (status AND start_time GE STRMID(ostart,0,19)) THEN BEGIN
                 FOR bsfcnt= 0, n_sched-1 DO BEGIN
                  start_time= TAI2UTC(UTC2TAI(STR2UTC(orig_start_time))+bsfcnt*(bsf_cad*60.0D),/ECS) 
                  WIDGET_CONTROL,mdiag,SET_VALUE='Scheduling '+bsfile+' at '+start_time
                  PRINT,'Scheduling '+bsfile+' at '+start_time 

                  ; Note: make sure to also update add_blk_seq_ipt.pro if following code changes:

                  delay= 0.0D
                  tai_stime= UTC2TAI(STR2UTC(start_time))  ; change to seconds

                  ;cmdseq= {cmdseq,bsfile:'',start_time:'',os_num:'',os_time:'',sc:''}
                  cmdseq= {cmdseq,bsfile:'',start_time:'',os_num:'',os_time:'',sc:'',bsf:''}
                  cmdseq.bsfile= bsfile 
                  cmdseq.start_time=start_time
                  IF (schedv.sc EQ 1) THEN cmdseq.sc= 'A' 
                  IF (schedv.sc EQ 2) THEN cmdseq.sc= 'B' 

                  ; Note: the code in this section should be the same as in add_blk_seq_ipt.pro

                  OPENR, bsf_lun, bsfile, /GET_LUN

                  entry=''

                  ; read-off comment lines, if any, and begin line:
                  READF,bsf_lun, entry
                  WHILE (STRMID(entry,0,1) EQ '#') DO READF,bsf_lun, entry 

                  ;READF,bsf_lun, entry ; read off the "Start" line (start of file).
                  IF (STRMID(entry,11,1) NE 'B') THEN BEGIN
                    WIDGET_CONTROL,mdiag, $
                      SET_VALUE= 'Error: '+bsfile+' Does NOT start with a "Start Timeline" entry - Block Sequence Canceled.'
                    PRINT,''
                    PRINT,'Error: '+bsfile+' Does NOT start with a "Start Timeline" entry - Block Sequence Canceled.'
                    PRINT,''
                    RETURN 
                  ENDIF
         
                  ; Grab BSF offset time from 'B' command line, if any, to add to schedule offset:
                  GET_BSF_ENTRY_INFO,entry,osnum,delay,fname
                  bsf_offset= delay 

                  IF (schedv.sc EQ 1) THEN BEGIN 
                    a_bsf_cnt= a_bsf_cnt + 1
                    cmdseq.bsf= 'A'+STRTRIM(a_bsf_cnt,2)
                  ENDIF
                  IF (schedv.sc EQ 2) THEN BEGIN
                    b_bsf_cnt= b_bsf_cnt + 1
                    cmdseq.bsf= 'B'+STRTRIM(b_bsf_cnt,2)
                  ENDIF

                  READF,bsf_lun, entry
                  ; bsfile must end with a End (function code T) line.
                  WHILE (NOT(EOF(bsf_lun)) AND STRMID(entry,11,1) NE 'T') DO BEGIN
       		    WIDGET_CONTROL, /HOUR
                    GET_BSF_ENTRY_INFO,entry,osnum,delay,fname
                    delay= bsf_offset+delay

                   IF (STRMID(entry,11,1) EQ 'G') THEN BEGIN
                      ; A GT-dump (G) command (handle differetly than an OS command):
                      ; Note: For each S/C, check input .bsf file for Run Script (lp=10)
                      ; commands. We keep all G commands. If an G command belongs to a bsf file,
                      ; mark it as such so that it is not written separetly in the output files.

                      gtfn= GETENV('PT')+'/IO/OSID/'+STRMID(entry,3,20)+'.gt'
                      str_arr= ''
                      OPENR, gtlun, gtfn, /GET_LUN
                      str= ''
                      WHILE ( NOT(EOF(gtlun))) DO BEGIN
                        READF, gtlun, str
                        str = STRTRIM(str, 2)
                        ;** throw out comments and blank lines now
                        IF ( (STRMID(str,0,1) NE ';') AND (str NE '') ) THEN str_arr = [str_arr, str]
                      ENDWHILE
                      CLOSE, gtlun
                      FREE_LUN, gtlun 
                      str_arr= str_arr(1:*)
                      key_val_arr = STR_ARR2KEY_VAL(str_arr)
                      IF (DATATYPE(stime_gt) EQ 'UND') THEN $
                        gt_ind= 0 $
                      ELSE        $
                        gt_ind= N_ELEMENTS(stime_gt)
                      ADD_IPT_GT,key_val_arr
                      ; Adjust GT-dump times:
                      gt_dur= etime_gt(gt_ind) - stime_gt(gt_ind)
                      stime_gt(gt_ind)= tai_stime+delay
                      etime_gt(gt_ind)= tai_stime+delay+gt_dur 

                      IF (schedv.sc EQ 1) THEN $
                        bsf_gt(gt_ind)= 'A'+STRTRIM(a_bsf_cnt,2) $
                      ELSE $
                        bsf_gt(gt_ind)= 'B'+STRTRIM(b_bsf_cnt,2)

                   ENDIF ELSE BEGIN
                    IF (STRMID(entry,11,1) EQ 'A') THEN BEGIN
                       ; A Run Script command (handle differetly than an OS command):
                       ; Note: For each S/C, check input .bsf file for Run Script (lp=12)
                       ; commands. We keep all A commands. If an A command belongs to a bsf file,
                       ; mark it as such so that it is not written separetly in the output files.

                       rsfn= STRMID(entry,STRLEN(entry)-12,12)
                       IF (schedv.sc EQ 1) THEN $
                         rsc= [rsc,{sc:'A',dt:tai_stime+delay,fn:rsfn,bsf:a_bsf_cnt}] $
                       ELSE $
                         ;ic= [ic,{sc:'B',dt:tai_stime+delay,fn:rsfn,bsf:b_bsf_cnt}]
                         rsc= [rsc,{sc:'B',dt:tai_stime+delay,fn:rsfn,bsf:b_bsf_cnt}]
                    ENDIF ELSE BEGIN  
                     IF (STRMID(entry,11,1) EQ 'I') THEN BEGIN
                       ; An Init Timeline command (handle differetly than an OS command):
                       ; Note: For each S/C, check input .bsf file for Initialize Timeline (lp=11) 
                       ; commands. We keep all I commands. If an I command belongs to a bsf file,
                       ; mark it as such so that it is not written separetly in the output files.

                       IF (schedv.sc EQ 1) THEN $
                         ic= [ic,{sc:'A',dt:tai_stime+delay,bsf:a_bsf_cnt}] $
                       ELSE $
                         ic= [ic,{sc:'B',dt:tai_stime+delay,bsf:b_bsf_cnt}]
                     ENDIF ELSE BEGIN  
                      IF (cmdseq.os_num EQ '') THEN BEGIN
                        cmdseq.os_num= osnum
                        cmdseq.os_time= UTC2STR(TAI2UTC(tai_stime + delay),/ECS)
                      ENDIF ELSE BEGIN
                        cmdseq.os_num= cmdseq.os_num+','+osnum
                        cmdseq.os_time= cmdseq.os_time+','+UTC2STR(TAI2UTC(tai_stime + delay),/ECS)
                      ENDELSE
                      SCHEDULE_READ_OS_IPT, GETENV('PT')+'/IO/OSID/'+fname, tai_stime + delay, /SILENT
                     ENDELSE
                    ENDELSE   
                   ENDELSE
                    READF,bsf_lun, entry
                  ENDWHILE ; bsfile
;help,entry,cmdseq,bsf_gt,rate_gt,sc_gt,stime_gt,ic,rsc
;stop

                  IF (EOF(bsf_lun) AND STRMID(entry,11,1) NE 'T') THEN BEGIN
                    CLOSE, bsf_lun
                    FREE_LUN, bsf_lun
                    WIDGET_CONTROL,mdiag,SET_VALUE= $
                     'Error: '+bsfile+' file does NOT end with a "End Timeline" entry - Block Sequence Canceled.'
                    PRINT,''
                    PRINT,'Error: '+bsfile+' file does NOT end with a "End Timeline" entry - Block Sequence Canceled.'
                    PRINT,''
                    RETURN
                  ENDIF 

                  CLOSE, bsf_lun
                  FREE_LUN, bsf_lun

                  ; AEE - 6/17/2008 commented out to handle bsf files that don't have any images:
;                  IF (STRLEN(cmdseq.os_num) EQ 0) THEN BEGIN
;                    WIDGET_CONTROL,mdiag,SET_VALUE= 'Error: '+bsfile+' file is EMPTY - Block Sequence Canceled.' 
;                    PRINT,''
;                    PRINT,'Error: '+bsfile+' file is EMPTY - Block Sequence Canceled.'
;                    PRINT,''
;                    RETURN 
;                  ENDIF

                  IF (DATATYPE(sched_cmdseq) NE 'STC') THEN $
                    sched_cmdseq= cmdseq $
                  ELSE $
                    sched_cmdseq= [sched_cmdseq,cmdseq]

                  IF (DATATYPE(all_sched_cmdseq) NE 'STC') THEN $
                    all_sched_cmdseq= cmdseq $
                  ELSE $
                    all_sched_cmdseq= [all_sched_cmdseq,cmdseq]



                  ;WIDGET_CONTROL, /HOUR
                  ;SCHEDULE_PLOT, schedv
;help,sched_cmdseq,all_sched_cmdseq
;stop

                  WIDGET_CONTROL,mdiag,SET_VALUE='Scheduling '+bsfile+' at '+start_time+' - Done.'
                  PRINT,'Scheduling '+bsfile+' at '+start_time+' - Done.' 
                 ENDFOR ; bsf_cnt
                 WIDGET_CONTROL, /HOUR
                 SCHEDULE_PLOT, schedv
                ENDIF ELSE BEGIN
                  IF (NOT status) THEN BEGIN
                    WIDGET_CONTROL,mdiag,SET_VALUE='Block Sequence Canceled.'
                    PRINT,'Block Sequence Canceled.'
                  ENDIF ELSE BEGIN
                   WIDGET_CONTROL,mdiag,SET_VALUE='Invalid Input StartTime of '+start_time+ $
                     ' < Display StartTime of '+ostart+'. Block Sequence Canceled.'
                   PRINT,'Invalid Input StartTime of '+start_time+ $
                     ' < Display StartTime of '+ostart+'. Block Sequence Canceled.'
                  ENDELSE
                ENDELSE

	      END

   'INPUT_KAP' : BEGIN
                READ_SCA, schedv.sc, schedv.startdis, schedv.enddis
                WIDGET_CONTROL, /HOUR
                SCHEDULE_PLOT, schedv
	      END

   'OUTPUT_BUF' : BEGIN
                GENERATE_BUF, schedv.startdis, schedv.enddis
	      END

   'GET_OS_OP' : BEGIN		;** allow user to select previously defined OS or OP
                 ; event from cw_pdmenu has value not index
		 
    	    	    osid=event.value
    	    	    type=0
		    print,'eventvalue=',trim(osid[0])
		    
    	    	    IF osid[0] EQ 2 THEN BEGIN	
		    ; read in all OSIDs from IO/OSID
		    	qury=getenv('PT')+'/IO/OSID/OS*.IPT'
			list=findfile(qury)
			nf=n_elements(list)
			IF nf GT 1 THEN BEGIN
			    FOR i=0,nf-1 DO schedule_read_os_ipt,list[i],0
			ENDIF ELSE $
			    WIDGET_CONTROL,mdiag,SET_VALUE=qury+' not found.'
			
		    ENDIF ELSE IF osid[0] GT 3 THEN BEGIN
    	    	    ; read in OSID that was input into field
		    	qury=getenv('PT')+'/IO/OSID/OS_'+string(osid,'(i4.4)')+'.IPT'
		    	IF file_exist(qury) THEN BEGIN
    	    	    	    WIDGET_CONTROL,mdiag,SET_VALUE='Reading '+qury
    	    	    	    schedule_read_os_ipt,qury,0 
		    	ENDIF ELSE BEGIN
			    WIDGET_CONTROL,mdiag,SET_VALUE=qury+' not found.'
			    print,qury+' not found.'
    	    	    	    return
			ENDELSE
    	    	    ENDIF ELSE type = event.value-1	;** 0:OS, 2:OP

                CASE (type) OF
                    0 : BEGIN		;** select OS
                          WIDGET_CONTROL,mdiag,SET_VALUE=''
                          ;os_num = SCHEDULE_GET_OS()
                          ;READ, 'Enter os: ', os_num
                          ;os_num = 1705
                          ;os_num = FIX(os_num)

                          ;success= GET_INPUT(tmp_num,/integer,title="Enter OS Num") ;AEE - June 11, 01
                          ;IF(not success) THEN RETURN

                          ;IF DATATYPE(defined_os_arr) EQ 'UND' THEN BEGIN   ; AEE - 02/14/03
                          IF DATATYPE(defined_os_arr) NE 'STC' THEN BEGIN   ; AEE - 02/17/04
                             WIDGET_CONTROL,mdiag,SET_VALUE='No OS is currently scheduled.'
                             PRINT,'No OS is currently scheduled.'
                             RETURN
                          ENDIF

                          ;sos= STRTRIM(STRING(defined_os_arr.os_num),2)
                          sos= STRING(defined_os_arr.os_num,'(I4.4)')
                          sos= sos(UNIQ_NOSORT(sos)) ; AEE - 02/06/03; if LP=6 & HI1&2 used, show only one os_num

                          result = PICKFILES2(FILES=sos, XSIZE=100, TITLE="Choose an OS",/exclusive)
                          If (result(0) EQ '') THEN BEGIN
                             WIDGET_CONTROL,mdiag,SET_VALUE='No OS was selected.'
                             PRINT,'No OS was selected.'
                             RETURN
                          ENDIF

                          os_num = FIX(result(0))

                          WIDGET_CONTROL,mdiag,SET_VALUE='Selected OS_NUM: '+result(0)
                          ;PRINT,'Selected OS_NUM: '+result(0)


                          ;ind = WHERE(defined_os_arr.os_num EQ os_num)
                          ;IF (ind(0) EQ -1) THEN BEGIN
                          ;   PRINT, '%SCHEDULE_EVENT: OS_NUM: ', os_num, ' not found in defined_os_arr.'
                          ;   WIDGET_CONTROL,mdiag,SET_VALUE='OS_NUM: '+ STRN(os_num)+ ' not found in defined_os_arr.'
                          ;   RETURN
                          ;ENDIF
                          ;os = defined_os_arr(ind(0))


                          os= defined_os_arr(WHERE(defined_os_arr.os_num EQ os_num))

                          IF (N_ELEMENTS(os) EQ 2) THEN BEGIN 
                            ; Keep only vaild HI1 or HI2 from a sequence (a better way of doing it
                            ; is not to generate the invalid os entry in defined_os_arr to begin with).
                            os= os(WHERE(os.num_images GT 0))
                          ENDIF

                          GET_OS_DB_STATS, os, os_size, os_duration, os_pre_proc_time, os_ro_time, $
                                           os_proc_time, os_setup_time, os_cadence;, /loud

                          ;GET_OS_DB_STATS, os(0), os_size, os_duration, os_pre_proc_time, os_ro_time, $
                          ;                 os_proc_time, os_setup_time, os_cadence 
                          ;; AEE 2/13/04 - changed:
                          ;IF (N_ELEMENTS(os) EQ 2) THEN BEGIN  ; Get info for HI2 of HI-Seq
                          ;  GET_OS_DB_STATS, os(1), os_size1, os_duration1, os_pre_proc_time1, os_ro_time1, $
                          ;                   os_proc_time1, os_setup_time1, os_cadence1
                          ;  os_size= [os_size,os_size1]
                          ;  os_duration= [os_duration,os_duration1]
                          ;  os_pre_proc_time= [os_pre_proc_time,os_pre_proc_time1]
                          ;  os_ro_time= [os_ro_time,os_ro_time1]
                          ;  os_proc_time= [os_proc_time,os_proc_time1]
                          ;  os_setup_time= [os_setup_time,os_setup_time1]
                          ;  os_cadence= [os_cadence,os_cadence1]
                          ;ENDIF
                          
                          WIDGET_CONTROL, schedv.os_label, /SENSITIVE
                          WIDGET_CONTROL, schedv.delta_text, /SENSITIVE
                          WIDGET_CONTROL, schedv.count_text, /SENSITIVE
                          WIDGET_CONTROL, schedv.num_text, SET_VALUE=STRING(os(0).os_num,'(I4.4)')
                          WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=to

                          new_value = SCHEDULE_CONVERT_UNITS(os_size, 0, to)
                          WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.size_fmt),2)+' '
                          new_value = FLOAT(os_duration * schedv.os_dur_factor)	;** units secs of mins
                          WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' ' 
	                  WIDGET_CONTROL, schedv.delta_text, SET_VALUE=''
	                  WIDGET_CONTROL, schedv.count_text, SET_VALUE=''
                          ; AEE - Dec 19, 02: Added following:
                          new_value = FLOAT(os_proc_time * schedv.os_dur_factor)
                          WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
                          new_value = FLOAT(os_ro_time * schedv.os_dur_factor)
                          WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                          new_value = FLOAT(os_setup_time * schedv.os_dur_factor)
                          WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                          new_value = FLOAT((os_pre_proc_time - os_setup_time) * schedv.os_dur_factor)
                          WIDGET_CONTROL, schedv.os_exp_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                       END

				
                    2 : BEGIN		;** select OP
                          ;op_num = SCHEDULE_GET_OP()

                          ; AEE jun 02, No code exists for this from LASCO. I added the following nut i'm not sure 
                          ; what it should do.

                          IF DATATYPE(defined_op_arr) EQ 'UND' THEN BEGIN   ; AEE - 02/14/03
                             WIDGET_CONTROL,mdiag,SET_VALUE='No OP is currently scheduled.'
                             PRINT,'No OP is currently scheduled.'
                             RETURN
                          END

                          sop= STRTRIM(STRING(defined_op_arr.num),2)

                          result = PICKFILES2(FILES=sop, XSIZE=100, TITLE="Choose an OP",/exclusive)
                          If (result(0) ne '') THEN BEGIN
                            op_num= FIX(result(0)) 
                            op= defined_op_arr(WHERE(defined_op_arr.num EQ op_num)) 
                            WIDGET_CONTROL, schedv.num_text, SET_VALUE='OP_NUM: '+STRTRIM(STRING(op.num),2)
                            WIDGET_CONTROL,mdiag,SET_VALUE='Selected OP '+result(0)
                            PRINT,'Selected OP '+result(0)
                          ENDIF ELSE BEGIN
                            WIDGET_CONTROL,mdiag,SET_VALUE='No OP was selected.'
                            PRINT,'No OP was selected.' 
                          ENDELSE
                       END

                ENDCASE
; somewhere os_arr is getting zero values in it. I think this has something to do with inputting OBS_ID (osid GT 3)
		nzs=where(os_arr.os_start NE 0, NCOMPLEMENT=nz)
		IF nz GT 0 THEN BEGIN
		    message,'Removing '+trim(nz)+' invalid OS instances.',/info
		    os_arr=os_arr[nzs]
		ENDIF

	      END
   'DELETE_OSs' : BEGIN		;** delete selected OS's between two times
                IF (DATATYPE(os_arr) EQ 'INT') THEN RETURN

                WIDGET_CONTROL, schedv.start_text, GET_VALUE=start_value
  	        WIDGET_CONTROL, schedv.stop_text,  GET_VALUE=stop_value	
   	        start_value = UTC2TAI(start_value(0))	; ASCII to TAI
   	        stop_value  = UTC2TAI(stop_value(0))	; ASCII to TAI

                be_val= TAI2UTC(start_value,/ECS)+' and '+TAI2UTC(stop_value,/ECS) 

                ;os_tme = REVERSE(os_tme)
                ;selected = BYTARR(N_ELEMENTS(oss))


                ; AEE 8/5/03:
                ; For OSes than belong to a cmd_seq, display the OS numbers on the
                ; same line and add '(cmd seq)' at the end so that all of the OSes
                ; belonging to a cmd_seq are shifted, if selected.
                ; Also, if a cmd_seq is moved, reflect the date changes
                ; in sched_cmdseq.start_time and .os_time:

                ;cmd_seq= '(cmd seq)'
                IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN BEGIN
                  ;oss = os_arr.os_num
                  bsf_ind = WHERE(os_arr.bsf NE '')
                  oss = os_arr(bsf_ind).os_num
                  oss = oss(SORT([oss]))
                  oss = oss(UNIQ([oss]))
                  oss = STRING(REVERSE(oss),'(I4.4)')
                  toss = oss

                  FOR j= 0, N_ELEMENTS(sched_cmdseq)-1 DO BEGIN
                    cmd_seq= ' (cmd seq '+sched_cmdseq(j).bsf+')'
                    scs= STR_SEP(sched_cmdseq(j).os_num,',')
                    ;cs_tme= UTC2TAI(STR2UTC(STR_SEP(sched_cmdseq(j).os_time,',')))
                    FOR i= 0, N_ELEMENTS(scs)-1 DO BEGIN
                      ;ind= WHERE(toss NE scs(i) OR os_tme NE cs_tme(i), cnt)
                      ind= WHERE(toss NE scs(i), cnt)
                      IF (cnt GT 0) THEN BEGIN 
                        toss= toss(ind)    
                        ;os_tme = os_tme(ind)
                      ENDIF ELSE BEGIN     ;no other OS remains except for the last one of the cmdseq 
                        toss= '' 
                        ;os_tme= 0.0
                      ENDELSE
                    ENDFOR
                    toss= [toss,sched_cmdseq(j).os_num+cmd_seq]
                    ; AEE -  1/5/04:
                    ;toss= [toss,sched_cmdseq(j).os_num+' @ '+STRMID(sched_cmdseq(j).os_time,0,23)+', '+cmd_seq]

                  ENDFOR 
                  ind= WHERE(toss NE '')
                  toss= toss(ind)  
                  toss= REVERSE(toss(SORT(toss)))
                ENDIF

                no_bsf= WHERE(os_arr.bsf EQ '', no_cnt)
                IF (no_cnt GT 0) THEN BEGIN
                  no_bsf= os_arr(no_bsf).os_num
                  no_bsf= no_bsf(SORT(no_bsf))
                  no_bsf= no_bsf(UNIQ(no_bsf))
                  no_bsf= STRING(REVERSE(no_bsf),'(I4.4)')
                  IF (DATATYPE(toss) NE 'UND') THEN $
                    toss= [no_bsf,toss] $
                  ELSE $
                    toss= no_bsf 
                ENDIF
 
                result = PICKFILES2(FILES=toss, XSIZE=300, TITLE="Choose OS's to Delete")

                cmd_seq= ' (cmd seq '

                IF ( result(0) NE '' ) THEN BEGIN

                   ; see if result needs to be expanded for the cmd_seq entries, if any: - AEE 8/6/03
                   tres= result
                   ind= WHERE(STRPOS(result,cmd_seq) LT 0, cnt) ;pickup non-cmd_seq OSes, if any selecetd.
                   IF (cnt GT 0) THEN $
                     result= result(ind) $
                   ELSE $
                     result= ''
                   ind= WHERE(STRPOS(tres,cmd_seq) GE 0, cnt) ;pickup cmd_seq OSes, if any selected.
     
                   FOR i= 0, cnt-1 DO BEGIN
                     tmp= STR_SEP(tres(ind(i)),'(')
                     bsf_id= STRMID(tmp(1),8,STRLEN(tmp(1))-9) ; pickup bsf_id from the end of string.
                     tmp= STRTRIM(STR_SEP(tmp(0),','),2) ; pickup OS nums in the string.
                     result= [result,tmp+' '+bsf_id] ; remove last element the "(cmd seq)".
                   ENDFOR
                   result= result(WHERE(result NE ''))

                   ; Remove from the sched_cmdseq elements, if any:  ; AEE - 8/6/03

                   indx= WHERE(STRPOS(tres,cmd_seq) GE 0, cntx)
                   FOR i=0, cntx-1 DO BEGIN
                     tmp= tres(indx(i))
                     nid= WHERE(sched_cmdseq.os_num+cmd_seq+sched_cmdseq.bsf+')' EQ tmp)
                     nid= nid(0)
                     sdte= UTC2TAI(STR2UTC(sched_cmdseq(nid).start_time))
                     edte= STR_SEP(sched_cmdseq(nid).os_time,',')
                     edte= UTC2TAI(STR2UTC(edte(N_ELEMENTS(edte)-1)))
                   
                     ; IF some of the OSes to be deleted is a sched_cmdseq, then remove 
                     ; that entry from the sched_cmdseq (the corresponding OSes between
                     ; the applicable times will be also deleted from os_arr):

                     IF (sdte GE start_value AND edte LE stop_value) THEN BEGIN 
                       ;ind= WHERE(sched_cmdseq.os_num NE tmp, cnt)
                       ind= WHERE(sched_cmdseq.os_num+cmd_seq+sched_cmdseq.bsf+')' NE tmp, cnt)
                       IF (cnt GT 0) THEN BEGIN
                         sched_cmdseq= sched_cmdseq(ind)
                       ENDIF ELSE BEGIN
                         sched_cmdseq=''
                       ENDELSE 
                       IF (DATATYPE(all_sched_cmdseq) EQ 'STC') THEN BEGIN
                         ind= WHERE(all_sched_cmdseq.os_num+cmd_seq+all_sched_cmdseq.bsf+')' NE tmp, cnt)
                         IF (cnt GT 0) THEN BEGIN
                           all_sched_cmdseq= all_sched_cmdseq(ind)
                         ENDIF ELSE BEGIN
                           all_sched_cmdseq=''
                         ENDELSE
                       ENDIF

                       ; Also remove any GT (Guide Telescope) commands that is part of the bsf file:

;help,stime_gt,etime_gt,apid_gt,rate_gt,sc_gt,bsf_gt
;stop

                       gtc_scid= STR_SEP(tmp,' ')
                       gtc_scid= gtc_scid(N_ELEMENTS(gtc_scid)-1)  ; it has ')' at the end
                       fgtc_cnt= 0
                       IF (DATATYPE(bsf_gt) NE 'UND') THEN $
                         fgtc= WHERE(bsf_gt+')' EQ gtc_scid, fgtc_cnt)
                       IF (fgtc_cnt GT 0) THEN BEGIN
                         ;found corresponding GT command(s) for this BSF file.
                         bsf_gt(fgtc)= 'R'
                         keep= WHERE(bsf_gt NE 'R', k_cnt)
                         IF (k_cnt GT 0) THEN BEGIN
                           bsf_gt= bsf_gt(keep)  
                           stime_gt= stime_gt(keep)
                           etime_gt= etime_gt(keep)
                           apid_gt= apid_gt(keep)
                           sc_gt= sc_gt(keep)
                           rate_gt= rate_gt(keep)
                         ENDIF ELSE BEGIN
                           bsf_gt= '' 
                           stime_gt= 0 ; (don't use 0D)' 
                           etime_gt= 0 ; (don't use 0D)' 
                           apid_gt= '' 
                           sc_gt= ''
                           rate_gt= 0 
                         ENDELSE
                         IF(DATATYPE(pstime_gt) NE 'UND') THEN BEGIN
                           pstime_gt= stime_gt
                           petime_gt= etime_gt
                           papid_gt= apid_gt
                           prate_gt= rate_gt
                           psc_gt= sc_gt
                           pbsf_gt= bsf_gt
                         ENDIF
;help,stime_gt,etime_gt,apid_gt,rate_gt,sc_gt,bsf_gt
;stop

                       ENDIF
                        
                       ; Also remove any Init Timeline commands that is part of the bsf file:
                       ;help,tmp, ic
                       ic_scid= STR_SEP(tmp,' ')
                       ic_scid= ic_scid(N_ELEMENTS(ic_scid)-1)  ; it has ')' at the end
                       fic= WHERE(ic.sc+STRTRIM(ic.bsf,2)+')' EQ ic_scid, fic_cnt)
                       IF (fic_cnt GT 0) THEN BEGIN
                         ;found corresponding Init Timeline command(s) for this BSF file.
                         ic(fic).sc= 'R'
                         keep= WHERE(ic.sc NE 'R')
                         ic= ic(keep)  
                         ;help,ic
                         ;stop
                       ENDIF

                       ; Also remove any RunScript commands that is part of the bsf file:
                       ;help, rsc
                       fic= WHERE(rsc.sc+STRTRIM(rsc.bsf,2)+')' EQ ic_scid, fic_cnt)
                       IF (fic_cnt GT 0) THEN BEGIN
                         ;found corresponding RunScript command(s) for this BSF file.
                         rsc(fic).sc= 'R'
                         keep= WHERE(rsc.sc NE 'R')
                         rsc= rsc(keep)
                         ;help,rsc
                         ;stop
                       ENDIF

                     ENDIF ELSE BEGIN  
                       ; entry is not removed from sched_cmdseq. Make sure is not removed from os_arr
                       ; either by removing the OSes from result:

                       kp_ind= WHERE(STRPOS(result,sched_cmdseq(nid).bsf) LT 0, kp_cnt)
                       IF (kp_cnt GT 0) THEN $
                         result= result(kp_ind) $
                       ELSE $
                         result= ''
                       msg= 'Can''t delete parts of OSes in a Block Sequence (BS). '+ $
                           ' Delete Command ignored. Select all OSes of BS and/or correct time-range.'
                       PRINT,msg
                       WIDGET_CONTROL,mdiag,SET_VALUE= msg
                       RETURN


                       ;tos= STR_SEP(tmp,',')
                       ;FOR j= 0, N_ELEMENTS(tos)-1 DO BEGIN
                       ;  ind= WHERE(result NE tos(j), cnt)
                       ;  IF (cnt GT 0) THEN BEGIN 
                       ;    result= result(ind) 
                       ;  ENDIF ELSE BEGIN
                       ;    msg= 'Can''t delete parts of OSes in a Block Sequence (BS). '+ $
                       ;    ' Delete Command ignored. Select all OSes of BS and/or correct time-range.' 
                       ;    PRINT,msg
                       ;    WIDGET_CONTROL,mdiag,SET_VALUE= msg 
                       ;    RETURN
                       ;  ENDELSE
                       ;ENDFOR 

                     ENDELSE 
                   ENDFOR

                   ; Make sure result has only unique OSes at this point:

                   result= result(UNIQ_NOSORT(result))
 
                   FOR i=0, N_ELEMENTS(result) - 1 DO BEGIN
                     ;selected_os = LONG(result(i))
                     selected_os = result(i)

                     ;keep_os=WHERE( (os_arr.os_num NE selected_os) OR ((os_arr.os_start LT start_value) $
                     ;              OR (os_arr.os_start GT (stop_value+1))) )

                     os_num_bsf= STRTRIM(STRING(os_arr.os_num,'(I4.4)')+' '+os_arr.bsf,2)

                     ; When deleting a S/C A or B OSnum and the OSnum is synced (has .sc='AB'), then
                     ; keep the OSnum for the other S/C (instead of removing it from both):

                     del_os= WHERE( (os_num_bsf EQ selected_os) AND ((os_arr.os_start GE start_value) $
                                   AND (os_arr.os_start LE (stop_value+1))), dcnt) 
                     IF (schedv.sc NE 0 ) THEN BEGIN ; not AB 
                       FOR dadd= 0, dcnt-1 DO BEGIN 
                         IF (os_arr(del_os(dadd)).sc EQ 'AB') THEN BEGIN
                           IF (schedv.sc EQ 1) THEN sc= 'B'
                           IF (schedv.sc EQ 2) THEN sc= 'A'
                           os2keep= os_arr(del_os(dadd))
                           os2keep.sc= sc
                           IF (DATATYPE(remain_os_arr) EQ 'STC') THEN $
                             remain_os_arr= [remain_os_arr,os2keep] $
                           ELSE $
                             remain_os_arr= os2keep 
                         ENDIF
                       ENDFOR 
                     ENDIF

                     keep_os= WHERE( (os_num_bsf NE selected_os) OR ((os_arr.os_start LT start_value) $
                                    OR (os_arr.os_start GT (stop_value+1))) )
                     IF (keep_os(0) LT 0) THEN BEGIN ; only for last element of result this becomes true.
                        WIDGET_CONTROL,mdiag,SET_VALUE='Deleted all OS_'+selected_os+' between '+be_val+'.' 
                        PRINT, 'Deleted all OS_'+selected_os+' between '+be_val+'.' 
                        os_arr = -1
                        CHECK_CONFLICTS,all_conflicts,selected_os,be_val ; AEE 10/22/03
                     ENDIF ELSE IF (N_ELEMENTS(keep_os) EQ N_ELEMENTS(os_arr)) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='No matching OS_'+selected_os+' to delete between '+be_val+'.' 
                        PRINT, 'No matching OS_'+selected_os+' to delete between '+be_val+'.'
                     ENDIF ELSE BEGIN
                        ;** remove the matching OS's from the os_arr and re-plot
                        WIDGET_CONTROL,mdiag,SET_VALUE='Removing OS_'+selected_os+' between '+be_val+'.'
                        PRINT,'Removing OS_'+selected_os+' between '+be_val+'.'
			stop
                        os_arr = os_arr(keep_os)
                        CHECK_CONFLICTS,all_conflicts,selected_os,be_val ; AEE 10/22/03
                     ENDELSE
                   ENDFOR
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv
                ENDIF

	      END

   'BITS_BYTES' : BEGIN		;** change units on OS_SIZE label
                     WIDGET_CONTROL, schedv.os_size_text, GET_VALUE=os_size
                     from = schedv.os_size_units
                     to = event.index	;** 0:bits, 1:bytes, 2:% buffer
                     schedv.os_size_units = to
                     IF (to EQ 0) THEN $
                       schedv.size_fmt= '(d18.3)' $
                     ELSE $
                       schedv.size_fmt= ''

                     os_size= DOUBLE(STR_SEP(STRCOMPRESS(STRTRIM(os_size,2)),' ')) ; AEE - Jan 06, 03
                     new_value = SCHEDULE_CONVERT_UNITS(os_size, from, to)
;                     WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(new_value,FORMAT='(d18.3)'),2)+' '
                     WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.size_fmt),2)+' '
                  END

   'SECS_MINS' : BEGIN		;** change units on OS_DUR label, and OS_PROC and OS_ROT
                    
		    units = event.index	;** 0:secs, 1:mins
                    IF (units EQ 0) THEN new_os_dur_factor = 1.0D		;** secs
                    IF (units EQ 1) THEN new_os_dur_factor = 1.0D/60.0D		;** mins
                    IF (new_os_dur_factor NE schedv.os_dur_factor) THEN BEGIN
                       WIDGET_CONTROL, schedv.os_dur_text,   GET_VALUE=os_dur
                       os_dur= FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_dur,2)),' ')) ; AEE - Jan 07, 03

                       IF (schedv.os_dur_factor EQ 1.0D) THEN BEGIN
                         factor = 1.D/60.0D   ;** convert secs to minss
                         schedv.time_fmt1=  ''  ; show all digits when min (so when pickedup to change to sec, accuracy is not lost)
                         schedv.time_fmt2= ''
                       ENDIF ELSE BEGIN
                         factor = 60.0D 				     	    ;** convert mins to secs
                         schedv.time_fmt1=  '(d12.1)' ; show normal format when displayed in seconds (as original data).
                         schedv.time_fmt2= '(d8.3)'
                       ENDELSE

                       new_value = FLOAT(os_dur * factor)
                       WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
                       schedv.os_dur_factor = new_os_dur_factor

                       ; Also change units of PROC_TIME - AEE Dec 12, 02:
                       WIDGET_CONTROL, schedv.os_proc_text,   GET_VALUE=os_proc
                       os_proc = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_proc,2)),' ')) ; AEE - Jan 07, 03
                       new_value = FLOAT(os_proc * factor)
                       WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' ' 
                       ; Also change units of SETUP TIME 
                       WIDGET_CONTROL, schedv.os_setup_text,   GET_VALUE=os_setup
                       os_setup = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_setup,2)),' '))
                       new_value = FLOAT(os_setup * factor)
                       WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '

                      ; Also change units of Exp TIME
                       WIDGET_CONTROL, schedv.os_exp_text,   GET_VALUE=os_exp
                       os_exp = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_exp,2)),' '))
                       new_value = FLOAT(os_exp * factor)
                       WIDGET_CONTROL, schedv.os_exp_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '

                       ; Also change units of ROT_TIME - AEE Dec 12, 02:
                       WIDGET_CONTROL, schedv.os_rot_text,   GET_VALUE=os_rot
                       os_rot= FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_rot,2)),' ')) ; AEE - Jan 07, 03
                       new_value = FLOAT(os_rot * factor)
                       WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                    ENDIF
                 END

   'DEFINE_SET' : PLAN_SET

   'DEFINE_OS' : BEGIN
                    WIDGET_CONTROL, /HOUR
                    DEFINE_OS,/keep_common   ; AEE - added keep_common key - March 2002
                 END

   ;
   ;
   ; These buttons determine the plan's time span (shown in the DRAW window).
   ; By adding additional spans to the XPDMENU in widget base identified by 
   ; CONTROLS (in PLAN), and by adding an appropriate CASE statement below,
   ; more time spans can be added
   ;
   ;

   'TIMESPAN' : begin
                   WIDGET_CONTROL, /HOUR
                   WIDGET_CONTROL, mdiag, SET_VALUE= "Changed to "+schedv.span_list(event.index)
		   schedv.duration = schedv.span_sec(event.index)
		   MOVESCHEDULE, schedv=schedv
		end


   ;
   ;
   ; Set the mode condition:
   ;	ARROW_DIS  = 0 =  Display mode: Allows modIFication of the plan's 
   ;					start time and END time (via DURATION)
   ;	ARROW_EDIT = 1 =  Edit mode:    Allows editting of the plan at the
   ;					individual study level. 
   ;
   ;

   'ARROW_DIS'  : BEGIN			; pressed the "DISPLAY" button

	schedv.mode = 0					; in display mode now

	;
	; Because turning this button off may be confusing, because
	; neither display nor edit would be turned on, make all events
	; to this button appear to be turning on.
	;

	WIDGET_CONTROL, schedv.edit_study,  SENSITIVE=0

	WIDGET_CONTROL, schedv.arr_span, SET_BUTTON = 1	; make sure buttons 
	WIDGET_CONTROL, schedv.arr_edit, SET_BUTTON = 0	;      look correct

	; 
	; Ignore button turn offs, now that button look correct.
        ; 

	IF event.select EQ 0 THEN RETURN	; Allowed to return without
						; saving CDS to UVALUE.

        ; 
        ; Put the input focus in the start time text widget window
        ;

        WIDGET_CONTROL, schedv.start_text, /INPUT_FOCUS

	END

   'ARROW_EDIT' : BEGIN			; pressed the "EDIT" button

        ; For SpaceCraft AB display de-activate CONFLICTS icon even in edit mode
        ; bit for A or B, activate it:

        IF schedv.sc EQ 0 THEN WIDGET_CONTROL, schedv.sconf, SENSITIVE=0 
        IF schedv.sc GT 0 THEN WIDGET_CONTROL, schedv.sconf, SENSITIVE=1

	IF schedv.mode ne 1 THEN BEGIN			; was NOT in edit mode

	   schedv.mode = 1					; in edit mode now

	ENDIF ELSE BEGIN
			
	   ; Was already in edit mode (user must be switching input focus 
	   ;    between the start and the stop widgets).
	   ; Make sure that the time in the text widgets are saved, 
	   ;    This saving means that the user need NOT press RETURN
	   ;	while in the time text windows.
	   ;   

 	   WIDGET_CONTROL, schedv.start_text, GET_VALUE=start_value	
	   WIDGET_CONTROL, schedv.stop_text,  GET_VALUE=stop_value	


	   schedv.study_start = UTC2TAI(start_value(0))	; ASCII to TAI
	   schedv.study_stop  = UTC2TAI(stop_value(0))	; ASCII to TAI

	ENDELSE

	;
	; Because turning this button off may be confusing, because
	; neither display nor edit would be turned on, make all events
	; to this button appear to be turning on.
	;

	WIDGET_CONTROL, schedv.arr_edit, SET_BUTTON = 1	; make sure buttons
	WIDGET_CONTROL, schedv.arr_span, SET_BUTTON = 0	;      look correct


	; 
	; Put the input focus in the text widget window NOT used last time.
	; Highlight the hours part of the time.
	;
	
	IF schedv.last_text EQ schedv.stop_text THEN BEGIN	   ; use start text
   	   WIDGET_CONTROL, schedv.start_text, /INPUT_FOCUS, SET_TEXT_SELECT=[11,2]
	   schedv.last_text = schedv.start_text
	ENDIF ELSE BEGIN				   ; use stop text
   	   WIDGET_CONTROL, schedv.stop_text, /INPUT_FOCUS, SET_TEXT_SELECT=[11,2]
	   schedv.last_text = schedv.stop_text 
	ENDELSE

	; sensitize the action widgets
	WIDGET_CONTROL, schedv.edit_study,  SENSITIVE=1

      END                                


   ;
   ;
   ; The following CASE statements handle the events from the time unit
   ; button, below the DRAW window.
   ;
   ;
   ; When in DISPLAY mode,  THEN the following buttons move the 
   ;    displayed plan the chosen distance in the selected direction.
   ;    Note: month is 30 days, year is 365 days.
   ;
   ; When in EDIT MODE THEN the following buttons change either the 
   ;	start time or the stop time, whichever text widget has any 
   ;	highlights in it.
   ; There is no reason to allow the user, in edit mode, to use
   ;    months or years.
   ;
   ;    

   ;'UPSTATS' :  schedv.up_stats= event.index ; 0=update telem. stats/buffers, 1=skip them ;AEE 7/30/03

   ; AEE 1/15/04 - allow output generation only when telem stats update is on so that img_cntr is set:
   'UPSTATS' : BEGIN
               schedv.up_stats= event.index
               IF (schedv.up_stats EQ 1) THEN BEGIN 
                 ; Skip is used, disable generate IPT and plans for commanding.
                 ;WIDGET_CONTROL,schedv.gipt, SENSITIVE=0
                 WIDGET_CONTROL,schedv.csf, SENSITIVE=0
                 WIDGET_CONTROL,schedv.gall, SENSITIVE=0
                 WIDGET_CONTROL,schedv.conf, SENSITIVE=0
               ENDIF ELSE BEGIN
                 ; Update is used, enable generate IPT and plans for commanding. 
                 ;WIDGET_CONTROL,schedv.gipt, SENSITIVE=1
                 WIDGET_CONTROL,schedv.csf, SENSITIVE=1
                 WIDGET_CONTROL,schedv.gall, SENSITIVE=1
                 WIDGET_CONTROL,schedv.conf, SENSITIVE=1
                 WIDGET_CONTROL, /HOUR
                 SCHEDULE_PLOT,schedv ;also redo the plot so that stats are updated and used for making IPT 
               ENDELSE
             END 

   'PCTSTATS' : BEGIN
                IF (schedv.pct_stats  NE event.index) THEN BEGIN
                  schedv.pct_stats= event.index
                  TOGGLE_STATS,  schedv, schedv.startdis, schedv.enddis
                ENDIF
              END 

   'FAKE_PB': BEGIN
                IF (event.index NE schedv.fakepb) THEN BEGIN
                  schedv.fakepb= event.index
                  SCHEDULE_PLOT,schedv
                ENDIF
             END

   'BACKYR'  : IF schedv.mode EQ 0 THEN BEGIN		; display mode

		  ; go backwards 365 days * 86400 secs

                  WIDGET_CONTROL, /HOUR
		  MOVESCHEDULE, sec=-31536000d, schedv=schedv

	       ENDIF 

   'BACKMON' : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; go backwards 30 days * 86400 secs
	
                  WIDGET_CONTROL, /HOUR
		  MOVESCHEDULE, sec=-2592000d, schedv=schedv

	       ENDIF

   'BACKDAY' : IF schedv.mode EQ 0 THEN BEGIN		; display mode

		  ; go backwards 1 day * 86400 secs

                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=-86400d, schedv=schedv
                  mult= schedv.dayarr(event.index) ; AEE - 7/29/03
                  MOVESCHEDULE, sec=-86400d * mult, schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window backward 1 day

 	     	  ;SCHEDULE_MODTIME, sec=-86400d, schedv=schedv, locate=[8,2]
                  mult= schedv.dayarr(event.index) ; AEE - 7/29/03
 	     	  SCHEDULE_MODTIME, sec=-86400d * mult, schedv=schedv, locate=[8,2]

	       ENDELSE

   'BACKHR'  : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; move display backwards 1 hour (in secs)

                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=-3600d, schedv=schedv
                  mult= schedv.hrarr(event.index) ; AEE - 7/29/03
		  MOVESCHEDULE, sec=-3600d * mult , schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window backward 1 hour

 	     	  ;SCHEDULE_MODTIME, sec=-3600d, schedv=schedv, locate=[11,2]
                  mult= schedv.hrarr(event.index) ; ; AEE - 7/29/03
 	     	  SCHEDULE_MODTIME, sec=-3600d * mult, schedv=schedv, locate=[11,2]

	       ENDELSE

   'BACKMIN' : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; go backwards 1 min (in secs)  
   
                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=-60d, schedv=schedv
                  mult= schedv.minarr(event.index) ; AEE - 7/29/03
                  MOVESCHEDULE, sec=-60d * mult , schedv=schedv
 		
	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window backward 1 minute

 	     	  ;SCHEDULE_MODTIME, sec=-60d, schedv=schedv, locate=[14,2]
                  mult= schedv.minarr(event.index) ; AEE - 7/29/03
                  SCHEDULE_MODTIME, sec=-60d * mult , schedv=schedv, locate=[14,2]

	       ENDELSE

   'BACKSEC' : IF schedv.mode EQ 0 THEN BEGIN		; display mode

		  ; go backwards 1 second

                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=-1d, schedv=schedv
                  mult= schedv.secarr(event.index)
                  MOVESCHEDULE, sec=-1d * mult , schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window backward 1 second

 	     	  ;SCHEDULE_MODTIME, sec=-1d, schedv=schedv, locate=[17,2]
                  mult= schedv.secarr(event.index)
                  SCHEDULE_MODTIME, sec=-1d * mult , schedv=schedv, locate=[17,2]

	       ENDELSE
                                                                       
   'FORSEC' : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; go forwards 1 second

                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=1d, schedv=schedv
                  mult= schedv.secarr(event.index)
                  MOVESCHEDULE, sec=1d * mult, schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window forward 1 second

 	     	  ;SCHEDULE_MODTIME, sec=1d, schedv=schedv, locate=[17,2]
                  mult= schedv.secarr(event.index)
                  SCHEDULE_MODTIME, sec=1d * mult, schedv=schedv, locate=[17,2]

	       ENDELSE

   'FORMIN' : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; go forwards 1 min (in secs)

                  WIDGET_CONTROL, /HOUR
	  	  ;MOVESCHEDULE, sec=60d, schedv=schedv
                  mult= schedv.minarr(event.index) ; AEE - 7/29/03
                  MOVESCHEDULE, sec=60d * mult, schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window forward 1 minute

 	     	  ;SCHEDULE_MODTIME, sec=60d, schedv=schedv, locate=[14,2]
                  mult= schedv.minarr(event.index) ; AEE - 7/29/03
 	     	  SCHEDULE_MODTIME, sec=60d * mult, schedv=schedv, locate=[14,2]

	       ENDELSE

   'FORHR'  : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; go forwards 1 hour (in secs)

                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=3600d, schedv=schedv
                  mult= schedv.hrarr(event.index) ; AEE - 7/29/03
		  MOVESCHEDULE, sec=3600d * mult, schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window forward 1 hour

 	     	  ;SCHEDULE_MODTIME, sec=3600d, schedv=schedv, locate=[11,2]
                  mult= schedv.hrarr(event.index) ; AEE - 7/29/03
 	     	  SCHEDULE_MODTIME, sec=3600d * mult, schedv=schedv, locate=[11,2]

	       ENDELSE

   'FORDAY' : IF schedv.mode EQ 0 THEN BEGIN		; display mode	

		  ; go forwards 1 day * 86400 secs

                  WIDGET_CONTROL, /HOUR
		  ;MOVESCHEDULE, sec=86400d, schedv=schedv
                  mult= schedv.dayarr(event.index) ; AEE - 7/29/03
		  MOVESCHEDULE, sec=86400d * mult, schedv=schedv

	       ENDIF ELSE BEGIN				; edit mode

                  ; Change start time and text window forward 1 day

 	     	  ;SCHEDULE_MODTIME, sec=86400d, schedv=schedv, locate=[8,2]
                  mult= schedv.dayarr(event.index) ; AEE - 7/29/03
 	     	  SCHEDULE_MODTIME, sec=86400d * mult, schedv=schedv, locate=[8,2]

	       ENDELSE

   'FORMON' : IF schedv.mode EQ 0 THEN BEGIN	

		  ; go forwards 30 days * 86400 secs

                  WIDGET_CONTROL, /HOUR
	          MOVESCHEDULE, sec=2592000d, schedv=schedv

	      ENDIF

   'FORYR'  : IF schedv.mode EQ 0 THEN BEGIN	

		  ; go forwards 365 days * 86400
 
                  WIDGET_CONTROL, /HOUR
		  MOVESCHEDULE, sec=31536000d, schedv=schedv

	      ENDIF


   ;
   ;
   ; Draw events are generated by a cursor click in the window where the 
   ; plan has been plotted.
   ;
   ; In display mode :  Describe what is happening for the selected
   ;			instrument at the selected time.
   ;
   ; In edit mode    :  For a click in the CDS-details line, load the 
   ;			   study name and the times for that study, IF any.
   ;			   Also switches editting control to the CDS-details
   ;			   line.
   ;			For a click in the CDS-plan line,  load the 
   ;			   study name and the times for that study, IF any.
   ;			   Also switches editting control to the CDS-plan
   ;			   line.
   ;


   'DRAW'   : BEGIN

              IF (DATATYPE(os_yarr) EQ 'UND') THEN RETURN ; AEE 6/18/03 - no OSes displayed, yet.
   
	      IF event.press EQ 1 THEN RETURN	; ignore mouse down events
              WSET, schedv.win_index
              !x = schedv.x
              !y = schedv.y
              !p = schedv.p

	      CURSOR, x, y, /NOWAIT		; where did the user click
						;    X hours since start of day
						;    Y op or os_num
	      x = x < !X.CRANGE(1) 	; force click in bounds

   	      datastart = TAI2UTC(schedv.startdis)	; form .DAY and .TIME
   	      datastart.time = 0		; only want day part
   	      daystart = UTC2TAI(datastart)	; start of STARTDIS in TAI

              ; AEE - 7/31/03 - Do a linear interp. since display time span is not always 24hrs:
              time= ((schedv.enddis-schedv.startdis)/(!X.CRANGE(1)-!X.CRANGE(0))) * $
                    (x-!X.CRANGE(0)) + schedv.startdis


; AEE - 6/18/03 - added the following cursor setection for the DRAW area for OS-section
;                 and the 5 Secchi buffer/channels. The y-values for each section are
;                 the same as those in schedule_plot.pro

IF (y GE 40.0 AND y LT 100.0) THEN BEGIN ; OS section

                IF (DATATYPE(os_arr) EQ 'INT') THEN RETURN
		; Y location determines OS_NUM
                ind = WHERE(os_yarr.pos LE y)
                os_num = os_yarr(ind(N_ELEMENTS(ind)-1)).os_num
                WIDGET_CONTROL,mdiag,SET_VALUE='OBS_ID = '+STRN(os_num)

		;
		; If the user clicks to the left of the yaxis then display
		; information for all instances of the os that matches.
		;
		; Otherwise only display info for the specific instance clicked on.
		;
                WIDGET_CONTROL, schedv.os_label, /SENSITIVE
                WIDGET_CONTROL, schedv.delta_text, /SENSITIVE
                WIDGET_CONTROL, schedv.count_text, /SENSITIVE
                os_ind = -1
                os_arr2 = os_arr(SORT([os_arr.os_start]))  ;** sort on start time
                good_os = WHERE(os_arr2.os_num EQ os_num)
	        IF (x LE !X.CRANGE(0)) THEN BEGIN	; display all instances of os
                   os0 = os_arr2(good_os(0))
                   os1 = os_arr2(good_os(N_ELEMENTS(good_os)-1))
                   WIDGET_CONTROL, schedv.num_text, SET_VALUE=STRING(os0.os_num,'(I4.4)')

	           WIDGET_CONTROL, schedv.start_text, SET_VALUE=TAI2UTC(os0.os_start, /ECS)
	           WIDGET_CONTROL, schedv.stop_text, SET_VALUE=TAI2UTC(os1.os_stop, /ECS)
	           WIDGET_CONTROL, schedv.delta_text, SET_VALUE=''

;help,os_arr2,good_os,TAI2UTC(os0.os_start, /ECS),TAI2UTC(os1.os_stop, /ECS)
;stop

                   ; AEE - Dec 19, 02: Added following:

                   ; AEE 02/05/03 - for LP=6 and both HI cameras used, show double size, dur, etc.:

                   WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=to

                   IF (os0.os_tele EQ os1.os_tele) THEN BEGIN ; Not LP=6 and if so, only one of HI camera used.
                     WIDGET_CONTROL, schedv.count_text, SET_VALUE=STRTRIM(STRING(N_ELEMENTS(good_os)),2) 
                     size_val = SCHEDULE_CONVERT_UNITS(os0.os_size, 0, to) 
                     dur_val = FLOAT(os0.os_duration * schedv.os_dur_factor)	;** units secs of mins

                     proc_val = FLOAT(os0.os_proc_time * schedv.os_dur_factor)
                     rot_val = FLOAT(os0.os_ro_time * schedv.os_dur_factor)
                     setup_val = FLOAT(os0.os_setup_time * schedv.os_dur_factor)
                     exp_val = FLOAT((os0.os_pre_proc_time - os0.os_setup_time) * schedv.os_dur_factor)
                   ENDIF ELSE BEGIN   ;LP=6 and both HI cameras used
                     WIDGET_CONTROL, schedv.count_text, SET_VALUE=STRTRIM(STRING(N_ELEMENTS(good_os)/2),2)
                     size_val = SCHEDULE_CONVERT_UNITS([os0.os_size,os1.os_size], 0, to)
                     dur_val = FLOAT([os0.os_duration,os1.os_duration] * schedv.os_dur_factor) ;units secs of mins
                     proc_val = FLOAT([os0.os_proc_time,os1.os_proc_time] * schedv.os_dur_factor)
                     rot_val = FLOAT([os0.os_ro_time,os1.os_ro_time] * schedv.os_dur_factor)
                     setup_val = FLOAT([os0.os_setup_time,os1.os_setup_time] * schedv.os_dur_factor)
                     exp_val = FLOAT([os0.os_pre_proc_time - os0.os_setup_time, $
                                      os1.os_pre_proc_time - os1.os_setup_time] * schedv.os_dur_factor)
                   ENDELSE
                   WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(size_val,schedv.size_fmt),2)+' '
                   WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(dur_val,schedv.time_fmt1),2)+' '
                   WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(proc_val,schedv.time_fmt1),2)+' '
                   WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(rot_val,schedv.time_fmt2),2)+' '
                   WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(setup_val,schedv.time_fmt2),2)+' '
                   WIDGET_CONTROL, schedv.os_exp_text, SET_VALUE=STRTRIM(STRING(exp_val,schedv.time_fmt2),2)+' '
                ENDIF ELSE BEGIN			; display single instance of os
                   FOR i=0, N_ELEMENTS(good_os)-1 DO BEGIN	;** for every matching os_num
                      IF ( (time GE os_arr2(good_os(i)).os_start) AND $;** look for correct time range
                           (time LT os_arr2(good_os(i)).os_stop) ) THEN os_ind = good_os(i)
                   ENDFOR
                   IF (os_ind GE 0) THEN BEGIN
                      os = os_arr2(os_ind)
                      WIDGET_CONTROL, schedv.num_text, SET_VALUE=STRING(os.os_num,'(I4.4)')

                      WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=to
                      new_value = SCHEDULE_CONVERT_UNITS(os.os_size, 0, to)
                     WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.size_fmt),2)+' '

                      new_value = FLOAT(os.os_duration * schedv.os_dur_factor)	;** units secs of mins
                      WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
	              WIDGET_CONTROL, schedv.start_text, SET_VALUE=TAI2UTC(os.os_start, /ECS)
	              WIDGET_CONTROL, schedv.stop_text, SET_VALUE=TAI2UTC(os.os_stop, /ECS)
	              WIDGET_CONTROL, schedv.delta_text, SET_VALUE=''
	              WIDGET_CONTROL, schedv.count_text, SET_VALUE=''
                      WIDGET_CONTROL, schedv.count_text, SET_VALUE= '1' ; AEE - 7/30/03 

                      ; AEE - Dec 19, 02: Added following:
                      new_value = FLOAT(os.os_proc_time * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
                      new_value = FLOAT(os.os_ro_time * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                      new_value = FLOAT(os.os_setup_time * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                      new_value = FLOAT((os.os_pre_proc_time - os.os_setup_time) * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_exp_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                   ENDIF ELSE BEGIN
                      WIDGET_CONTROL,mdiag,SET_VALUE='no OS for time'
                      print, 'no OS for time'
                   ENDELSE
                ENDELSE

ENDIF

; Note: the pct_start info for SSR1 and SSR2 should originally be picked up
;       from the S/C input. But, I declared it using some dummy defaults for now in here.
;       This structure must then be passed to schedule_plot.pro and then to plot_channels.pro
;       AEE 6/18/03.
;       Note: the SSR1 and SSR2 pct_start data can't go in the *.KAP file since the *.KAP files
;             only contain STARTIME and ENDTIME for various events. The pct info should then
;             either be read from a different file (like a save file) or be hardcoded here as
;             it is now and be changed by the operator by clicking on the SSR1 and SSR2 plot
;             regions.  AEE 12/05/03.

IF (schedv.sc EQ 0) THEN RETURN

IF (y GE 30.0 AND y LT 37.5) THEN BEGIN ; Secchi-Buffer
   ;tmp = schedv.sbuf_pct_start  ; AEE 12/05/03
   IF (schedv.sc EQ 1) then tmp = schedv.sbuf_pct_start_a
   IF (schedv.sc EQ 2) then tmp = schedv.sbuf_pct_start_b
   XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set Secchi-Buffer Percentage at Start', $
      GROUP=schedv.base, status=status
   IF (status) THEN BEGIN
     ;schedv.pct_start = tmp
     ;schedv.sbuf_pct_start = tmp  ; AEE 12/05/03
     IF (schedv.sc EQ 1) then schedv.sbuf_pct_start_a = tmp 
     IF (schedv.sc EQ 2) then schedv.sbuf_pct_start_b = tmp 
     
     IF (schedv.up_stats EQ 0) THEN BEGIN  ; Update is used (not Skip), just change pct_full
       WIDGET_CONTROL, /HOUR
       SCHEDULE_PLOT, schedv
     ENDIF
   END
ENDIF

IF (y GE 22.5 AND y LT 30.0) THEN BEGIN ; SSR1-Channel
   ;tmp = schedv.ssr1_pct_start
   IF (schedv.sc EQ 1) then tmp = schedv.ssr1_pct_start_a
   IF (schedv.sc EQ 2) then tmp = schedv.ssr1_pct_start_b
   XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set SSR1-Channel Percentage at Start', $
      GROUP=schedv.base, status=status
   IF (status) THEN BEGIN
     ;schedv.ssr1_pct_start = tmp
     IF (schedv.sc EQ 1) then schedv.ssr1_pct_start_a = tmp 
     IF (schedv.sc EQ 2) then schedv.ssr1_pct_start_b = tmp 
     IF (schedv.up_stats EQ 0) THEN BEGIN  ; Update is used (not Skip), just change pct_full
       WIDGET_CONTROL, /HOUR
       SCHEDULE_PLOT, schedv
     ENDIF
   END
ENDIF

; default for schedv.ssr2_pct_pb is 100

IF (y GE 15.0 AND y LT 22.5) THEN BEGIN ; SSR2-Channel
;   IF (x GE 0.0) THEN BEGIN
     ;tmp = schedv.ssr2_pct_start
     IF (schedv.sc EQ 1) then tmp = schedv.ssr2_pct_start_a
     IF (schedv.sc EQ 2) then tmp = schedv.ssr2_pct_start_b
     XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set SSR2-Channel Percentage at Start', $
        GROUP=schedv.base, status=status
     IF (status) THEN BEGIN
       ;schedv.ssr2_pct_start = tmp
       IF (schedv.sc EQ 1) then schedv.ssr2_pct_start_a = tmp 
       IF (schedv.sc EQ 2) then schedv.ssr2_pct_start_b = tmp 
       IF (schedv.up_stats EQ 0) THEN BEGIN  ; Update is used (not Skip), just change pct_full
         WIDGET_CONTROL, /HOUR
         SCHEDULE_PLOT, schedv
       ENDIF
     END
;   ENDIF ELSE BEGIN
;     ;tmp = schedv.ssr2_pct_pb
;     IF (schedv.sc EQ 1) then tmp = schedv.ssr2_pct_pb_a
;     IF (schedv.sc EQ 2) then tmp = schedv.ssr2_pct_pb_b
;     XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set SSR2 Playback Percentage', $
;        GROUP=schedv.base, status=status
;     IF (status) THEN BEGIN
;       ;schedv.ssr2_pct_pb = tmp
;       IF (schedv.sc EQ 1) then schedv.ssr2_pct_pb_a = tmp 
;       IF (schedv.sc EQ 2) then schedv.ssr2_pct_pb_b = tmp 
;       IF (schedv.up_stats EQ 0) THEN BEGIN  ; Update is used (not Skip), just change pct_pb
;         WIDGET_CONTROL, /HOUR
;         SCHEDULE_PLOT, schedv
;       ENDIF
;     END
;   ENDELSE
ENDIF

;IF (y GE 7.5 AND y LT 15.0) THEN BEGIN  ; RT-Channel
;   ;tmp = schedv.rt_pct_start
;   IF (schedv.sc EQ 1) then tmp = schedv.rt_pct_start_a
;   IF (schedv.sc EQ 2) then tmp = schedv.rt_pct_start_b
;   XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set RT-Channel Percentage at Start', $
;      GROUP=schedv.base, status=status
;   IF (status) THEN BEGIN
;     ;schedv.rt_pct_start = tmp
;     IF (schedv.sc EQ 1) then schedv.rt_pct_start_a = tmp 
;     IF (schedv.sc EQ 2) then schedv.rt_pct_start_b = tmp 
;     IF (schedv.up_stats EQ 0) THEN BEGIN  ; Update is used (not Skip), just change pct_full
;       WIDGET_CONTROL, /HOUR
;       SCHEDULE_PLOT, schedv
;     ENDIF
;   END
;ENDIF

IF (y GE 0.0 AND y LT 7.5) THEN BEGIN   ; SW-Channel
   ;tmp = schedv.sw_pct_start
   IF (schedv.sc EQ 1) then tmp = schedv.sw_pct_start_a
   IF (schedv.sc EQ 2) then tmp = schedv.sw_pct_start_b
   XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set SW-Channel Percentage at Start', $
      GROUP=schedv.base, status=status
   IF (status) THEN BEGIN
     ;schedv.sw_pct_start = tmp
     IF (schedv.sc EQ 1) then schedv.sw_pct_start_a = tmp 
     IF (schedv.sc EQ 2) then schedv.sw_pct_start_b = tmp 
     IF (schedv.up_stats EQ 0) THEN BEGIN  ; Update is used (not Skip), just change pct_full
       WIDGET_CONTROL, /HOUR
       SCHEDULE_PLOT, schedv
     ENDIF
   END
ENDIF

RETURN  ; ignore the following (from lasco).

;***********

ymax= 100.0 ; see schedule_plot.pro - AEE 6/18/03

IF (y LT os_yarr(0).pos) THEN BEGIN   ;os_yarr(0).pos = 40 (see schedule_plot.pro) - AEE 6/18/03
   tmp = schedv.pct_start
   XSET_VALUE, tmp, MIN=0, MAX=100, TITLE='Set Buffer Percentage at Start', $
      GROUP=schedv.base
   schedv.pct_start = tmp
   WIDGET_CONTROL, /HOUR
   SCHEDULE_PLOT, schedv
; AEE 6/18/03
ENDIF ELSE IF (y GE ymax) THEN BEGIN ;PRINT, 'above os & op - do nothing for now' $ 
   id = WHERE(kap_resource_names EQ "TLM_SUBMODE") & id = id(0)
   ind = WHERE(kap_resource_arr.id EQ id)
   IF (ind(0) GE 0) THEN $
      kap_resource_arr(ind).type = val+1

   WIDGET_CONTROL, /HOUR
   SCHEDULE_PLOT, schedv
ENDIF $

ELSE IF (y GE ymax) THEN BEGIN	;** selecting an op (AEE 6/18/03 - No OP for now)

	      IF (x GT !X.CRANGE(0)) THEN BEGIN	; ignore clicks left of the y-axis
                 ;** x location determines which op
                 loc = WHERE( (op_arr.op_start LE time) AND (op_arr.op_stop GE time) )
                 IF (loc(0) GE 0) THEN BEGIN
                    op = op_arr(loc(0))
                    WIDGET_CONTROL, schedv.os_label, SENSITIVE=0
                    WIDGET_CONTROL, schedv.num_text, SET_VALUE='OP_NUM: '+STRTRIM(op.num,2)
                    WIDGET_CONTROL, schedv.start_text, SET_VALUE=TAI2UTC(op.op_start, /ECS)
	            WIDGET_CONTROL, schedv.stop_text, SET_VALUE=TAI2UTC(op.op_stop, /ECS)
	            WIDGET_CONTROL, schedv.delta_text, SET_VALUE=''
	            WIDGET_CONTROL, schedv.count_text, SET_VALUE=''
                 ENDIF ELSE BEGIN
                    WIDGET_CONTROL,mdiag,SET_VALUE='no OP for time'
                    print, 'no OP for time'
                 ENDELSE
	      ENDIF

ENDIF ELSE BEGIN		;** selecting an os

                IF (DATATYPE(os_arr) EQ 'INT') THEN RETURN
		; Y location determines OS_NUM
                ind = WHERE(os_yarr.pos LE y)
                os_num = os_yarr(ind(N_ELEMENTS(ind)-1)).os_num
                WIDGET_CONTROL,mdiag,SET_VALUE='os_num = '+STRN(os_num)
                print, 'os_num = ', os_num

		;
		; If the user clicks to the left of the yaxis then display
		; information for all instances of the os that matches.
		;
		; Otherwise only display info for the specific instance clicked on.
		;
                WIDGET_CONTROL, schedv.os_label, /SENSITIVE
                WIDGET_CONTROL, schedv.delta_text, /SENSITIVE
                WIDGET_CONTROL, schedv.count_text, /SENSITIVE
                os_ind = -1
                os_arr2 = os_arr(SORT([os_arr.os_start]))  ;** sort on start time
                good_os = WHERE(os_arr2.os_num EQ os_num)
	        IF (x LE !X.CRANGE(0)) THEN BEGIN	; display all instances of os
                   os0 = os_arr2(good_os(0))
                   os1 = os_arr2(good_os(N_ELEMENTS(good_os)-1))
                   WIDGET_CONTROL, schedv.num_text, SET_VALUE=STRING(os0.os_num,'(I4.4)')

	           WIDGET_CONTROL, schedv.start_text, SET_VALUE=TAI2UTC(os0.os_start, /ECS)
	           WIDGET_CONTROL, schedv.stop_text, SET_VALUE=TAI2UTC(os1.os_stop, /ECS)
	           WIDGET_CONTROL, schedv.delta_text, SET_VALUE=''

                   ; AEE - Dec 19, 02: Added following:

                   ; AEE 02/05/03 - for LP=6 and both HI cameras used, show double size, dur, etc.:

                   WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=to

                   IF (os0.os_tele EQ os1.os_tele) THEN BEGIN ; Not LP=6 and if so, only one of HI camera used.
                     WIDGET_CONTROL, schedv.count_text, SET_VALUE=STRTRIM(STRING(N_ELEMENTS(good_os)),2) 
                     size_val = SCHEDULE_CONVERT_UNITS(os0.os_size, 0, to) 
                     dur_val = FLOAT(os0.os_duration * schedv.os_dur_factor)	;** units secs of mins

                     proc_val = FLOAT(os0.os_proc_time * schedv.os_dur_factor)
                     rot_val = FLOAT(os0.os_ro_time * schedv.os_dur_factor)
                     setup_val = FLOAT(os0.os_setup_time * schedv.os_dur_factor)
                     exp_val = FLOAT((os0.os_pre_proc_time - os0.os_setup_time) * schedv.os_dur_factor)
                   ENDIF ELSE BEGIN   ;LP=6 and both HI cameras used
                     WIDGET_CONTROL, schedv.count_text, SET_VALUE=STRTRIM(STRING(N_ELEMENTS(good_os)/2),2)
                     size_val = SCHEDULE_CONVERT_UNITS([os0.os_size,os1.os_size], 0, to)
                     dur_val = FLOAT([os0.os_duration,os1.os_duration] * schedv.os_dur_factor) ;units secs of mins
                     proc_val = FLOAT([os0.os_proc_time,os1.os_proc_time] * schedv.os_dur_factor)
                     rot_val = FLOAT([os0.os_ro_time,os1.os_ro_time] * schedv.os_dur_factor)
                     setup_val = FLOAT([os0.os_setup_time,os1.os_setup_time] * schedv.os_dur_factor)
                     exp_val = FLOAT([os0.os_pre_proc_time - os0.os_setup_time, $
                                     os1.os_pre_proc_time - os1.os_setup_time] * schedv.os_dur_factor)
                   ENDELSE
                   WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(size_val,schedv.size_fmt),2)+' '
                   WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(dur_val,schedv.time_fmt1),2)+' '
                   WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(proc_val,schedv.time_fmt1),2)+' '
                   WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(rot_val,schedv.time_fmt2),2)+' '
                   WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(setup_val,schedv.time_fmt2),2)+' '
                   WIDGET_CONTROL, schedv.os_exp_text, SET_VALUE=STRTRIM(STRING(exp_val,schedv.time_fmt2),2)+' '
                ENDIF ELSE BEGIN			; display single instance of os
                   FOR i=0, N_ELEMENTS(good_os)-1 DO BEGIN	;** for every matching os_num
                      IF ( (time GE os_arr2(good_os(i)).os_start) AND $;** look for correct time range
                           (time LT os_arr2(good_os(i)).os_stop) ) THEN os_ind = good_os(i)
                   ENDFOR
                   IF (os_ind GE 0) THEN BEGIN
                      os = os_arr2(os_ind)
                      WIDGET_CONTROL, schedv.num_text, SET_VALUE=STRING(os.os_num,'(I4.4)')

                      WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=to
                      new_value = SCHEDULE_CONVERT_UNITS(os.os_size, 0, to)
                     WIDGET_CONTROL, schedv.os_size_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.size_fmt),2)+' '

                      new_value = FLOAT(os.os_duration * schedv.os_dur_factor)	;** units secs of mins
                      WIDGET_CONTROL, schedv.os_dur_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
	              WIDGET_CONTROL, schedv.start_text, SET_VALUE=TAI2UTC(os.os_start, /ECS)
	              WIDGET_CONTROL, schedv.stop_text, SET_VALUE=TAI2UTC(os.os_stop, /ECS)
	              WIDGET_CONTROL, schedv.delta_text, SET_VALUE=''
	              WIDGET_CONTROL, schedv.count_text, SET_VALUE=''

                      ; AEE - Dec 19, 02: Added following:
                      new_value = FLOAT(os_proc_time * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_proc_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt1),2)+' '
                      new_value = FLOAT(os.os_ro_time * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_rot_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                      new_value = FLOAT(os.os_setup_time * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                      new_value = FLOAT((os.os_pre_proc_time - os.os_setup_time) * schedv.os_dur_factor)
                      WIDGET_CONTROL, schedv.os_setup_text, SET_VALUE=STRTRIM(STRING(new_value,schedv.time_fmt2),2)+' '
                   ENDIF ELSE BEGIN
                      WIDGET_CONTROL,mdiag,SET_VALUE='no OS for time'
                      print, 'no OS for time'
                   ENDELSE
                ENDELSE

ENDELSE

	      END

   'TEXTSTART' : IF schedv.mode EQ 0 THEN BEGIN	; display mode

		   ; 
		   ; The user pressed RETURN in the start text window,
		   ; while in display mode.
		   ; Read in the start value in the text window and
		   ; redisplay the plan starting at that time for the current span.
		   ;

		   WIDGET_CONTROL, event.id, GET_VALUE=value, SET_VALUE=value

		   ; input was in English but TAI is easier for the program
		   schedv.startdis = UTC2TAI(value(0)) 	; ASCII to TAI

                   ;
		   ; Using the new start time, re-draw the display.
                   ;

                   WIDGET_CONTROL, /HOUR
		   MOVESCHEDULE, schedv=schedv
                   WIDGET_CONTROL,mdiag, $
                     SET_VALUE='Display mode - Redisplaying plan using new start time for the selected span.'
                   PRINT,'Display mode - Redisplaying plan using new start time for the selected span.'

                 ENDIF ELSE BEGIN		; edit mode

		   ; 
		   ; The user pressed RETURN in the start text window,
		   ; while in edit mode.
		   ; Read in the start value in the text window and
		   ; save for use later as a study start time.
		   ;

		   WIDGET_CONTROL, event.id, GET_VALUE=value, SET_VALUE=value

		   ; Re-Write start time with all the zeroes
		   WIDGET_CONTROL, schedv.start_text, $	; ASCII to ASCII
				SET_VALUE=UTC2STR(STR2UTC(value(0)),/ECS)

                   WIDGET_CONTROL,mdiag,SET_VALUE='Edit mode - Save start time '+value(0)+' for later use.'
                   PRINT,'Edit mode - Save start time '+value(0)+' for later use.'

		   ; Input is in English, get time in an easily testable unit
		   schedv.study_start = UTC2TAI(value(0))		; ASCII to TAI
              
		   ; 
		   ; Since user pressed RETURN, they must be done with start.
	  	   ; Put the input focus in the stop time text widget window
		   ; Highlight the hours part of the time.
		   ;
	
		   WIDGET_CONTROL, schedv.stop_text, /INPUT_FOCUS
		   WIDGET_CONTROL, schedv.stop_text, SET_TEXT_SELECT=[11,2]
	           schedv.last_text = schedv.stop_text	; just a flag

		 ENDELSE


   'TEXTSTOP'  : IF schedv.mode EQ 0 THEN BEGIN	; display mode

		   ; 
		   ; The user pressed RETURN in the stop text window,
		   ; while in display mode.
		   ; Read in the stop value in the text window and
		   ; redisplay the plan starting at stop time minus
		   ; the current duration (span) value.
		   ;

		   WIDGET_CONTROL, event.id, GET_VALUE=value, SET_VALUE=value
			
		   ;
		   ; Passing the routine MOVESCHEDULE a STARTDIS that is 
		   ; really END time that was just input.
		   ; The seconds subtraction will correct in MOVESCHEDULE.
		   ;

                   schedv.startdis = UTC2TAI(value(0))	; ASCII to TAI

                   WIDGET_CONTROL, /HOUR
		   MOVESCHEDULE, sec=-(schedv.duration), schedv=schedv

                   WIDGET_CONTROL,mdiag, $
                   SET_VALUE='Display mode - Redisplaying plan ending with new stop time for the selected span.' 
                   PRINT,'Display mode - Redisplaying plan ending with new stop time for the selected span.'

		 ENDIF ELSE BEGIN		; edit mode

		   ; 
		   ; The user pressed RETURN in the stop text window,
		   ; while in edit mode.
		   ; Read in the stop value in the text window and
		   ; save for use later as a study stop time.
		   ;

		   WIDGET_CONTROL, event.id, GET_VALUE=value, SET_VALUE=value

		   ; Re-Write stop time with all the zeroes

		   WIDGET_CONTROL, schedv.stop_text, $	; ASCII to ASCII
				SET_VALUE=UTC2STR(STR2UTC(value(0)),/ECS)

                   WIDGET_CONTROL,mdiag,SET_VALUE='Edit mode - Save stop time '+value(0)+' for later use.'
                   PRINT,'Edit mode - Save stop time '+value(0)+' for later use.'

		   ; Input is in English, get time in an easily testable unit
		   schedv.study_stop = UTC2TAI(value(0))		; ASCII to TAI

		   ; 
	  	   ; Put the input focus in the start time text widget window
		   ; Highlight the hour part of the time.
		   ;
	
		   WIDGET_CONTROL, schedv.start_text, /INPUT_FOCUS
		   WIDGET_CONTROL, schedv.start_text, SET_TEXT_SELECT=[11,2]
	           schedv.last_text = schedv.start_text

		 ENDELSE

   'COUNT' : BEGIN
                WIDGET_CONTROL, event.id, GET_VALUE=value
                WIDGET_CONTROL, event.id, SET_VALUE=STRTRIM(STRING(FIX(value)), 2)
             END

   'DELTA' : BEGIN
                IF (DATATYPE(os_arr) EQ 'INT') THEN RETURN
                IF (schedv.mode EQ 0) THEN BEGIN

	           WIDGET_CONTROL, /HOUR
	           WIDGET_CONTROL, schedv.delta_text, GET_VALUE=delta
                   ;dte = STR2UTC(STRTRIM(delta,2))
                   delta= strtrim(delta(0),2)
                   IF(STRPOS(delta,':') LE 0) THEN BEGIN
                     WIDGET_CONTROL,mdiag,SET_VALUE='Please re-enter DELTA using a time (hh:mm:ss) fromat.'
                     PRINT,'Please re-enter DELTA using a time (hh:mm:ss) fromat.'
                     RETURN
                   ENDIF

                   ;AEE - May 2002, allowed + and - delta shift in times (in 'DELTA' and 'ADD' sections):
                   ;                plus=  hh:mm:ss.ms 
                   ;                         or 
                   ;                       +hh:mm:ss.ms
                   ;                minus= -hh:mm:ss.ms

                   delta= STRTRIM(delta,2)
                   sign= STRMID(delta,0,1)
                   sign= sign(0)
                   IF (sign EQ '-' OR sign EQ '+') THEN    $
                     delta= STRMID(delta,1,STRLEN(delta))  $
                   ELSE                                    $
                     sign= '+'
                   dte = STR2UTC(delta)
                   delta = dte.time / 1000			;** convert msec to sec
                   IF (delta EQ 0) THEN RETURN
                   IF (sign EQ '-') THEN delta= -delta
                   ;** have user pick which OS's to apply this delta to
                   oss = os_arr.os_num
                   oss = oss(SORT([oss]))

                   ; AEE 1/8/04 - if an os apears in cmd_seq and also by itself, keep it so it is not lost.
                   add_oses= [-1L]
                   IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN BEGIN
;help,sched_cmdseq
;stop
                     cs_oses= sched_cmdseq.os_num
                     cmdseq_oses= STR_SEP(cs_oses(0),',') 
                     FOR j= 1, N_ELEMENTS(sched_cmdseq)-1 DO cmdseq_oses= [cmdseq_oses, STR_SEP(cs_oses(j),',')]
                     cmdseq_oses= LONG(cmdseq_oses(SORT(LONG(cmdseq_oses))))
                     uniq_cs_oses= cmdseq_oses(UNIQ(cmdseq_oses))
                     FOR j= 0, N_ELEMENTS(uniq_cs_oses)-1 DO BEGIN
                       tmp= WHERE(cmdseq_oses EQ uniq_cs_oses(j),csoscnt)
                       tmp= WHERE(os_arr.os_num EQ uniq_cs_oses(j),alloscnt)
                       IF (alloscnt GT csoscnt) THEN add_oses= [add_oses,uniq_cs_oses(j)]
                     ENDFOR
                   ENDIF  

                   oss = oss(UNIQ([oss]))
                   ;oss = STRTRIM(REVERSE(oss), 2)
                   oss = STRING(REVERSE(oss),'(I4.4)')
                   selected = BYTARR(N_ELEMENTS(oss))

                   toss = oss

                   ; AEE 8/5/03:
                   ; For OSes than belong to a cmd_seq, display the OS numbers on the
                   ; same line and add '(cmd seq)' at the end so that all of the OSes
                   ; belonging to a cmd_seq are shifted, if selected.
                   ; Also, if a cmd_seq is moved, reflect the date changes
                   ; in sched_cmdseq.start_time and .os_time:

                   ;cmd_seq= '(cmd seq)'
                   IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN BEGIN
                     FOR j= 0, N_ELEMENTS(sched_cmdseq)-1 DO BEGIN
                       cmd_seq= ' (cmd seq '+sched_cmdseq(j).bsf+')'
                       scs= STR_SEP(sched_cmdseq(j).os_num,',')
                       FOR i= 0, N_ELEMENTS(scs)-1 DO BEGIN
                         ind= WHERE(toss NE scs(i), cnt)
                         IF (cnt GT 0) THEN $
                           toss= toss(ind)    $
                         ELSE               $ ;no other OS remains except for the last one of the cmdseq 
                           toss= '' 
                       ENDFOR
                       toss= [toss,sched_cmdseq(j).os_num+' @ '+STRMID(sched_cmdseq(j).os_time,0,23)+', '+cmd_seq]
                     ENDFOR 
                     ind= WHERE(toss NE '')
                     toss= toss(ind)  
                   ENDIF

                   IF (N_ELEMENTS(add_oses) GT 1) THEN toss= [toss,STRTRIM(add_oses(1:*),2)] ; AEE 1/8/04

                   result = PICKFILES2(FILES=toss, XSIZE=550, $
                    TITLE="Choose OS's to Shift (All OS Instancesin the entire schedule will be shifted)")
                   IF ( result(0) NE '' ) THEN BEGIN

                      ; Don't allow shift if any of Oses are synched but shifting is
                      ; done for S/C A or B:

                      IF (schedv.sc NE 0) THEN BEGIN ; S/C A or B 
                        nonseq= WHERE(STRPOS(result,'(cmd seq') LT 0,nseqcnt)
                        FOR nsi = 0, nseqcnt-1 DO BEGIN
                          fnd= WHERE(os_arr.os_num EQ result(nonseq(nsi)))
                          mtch= WHERE(os_arr(fnd).sc EQ 'AB', mtchcnt)
                          IF (mtchcnt GT 0) THEN BEGIN
                            WIDGET_CONTROL,mdiag,SET_VALUE= 'One or more OS to be shifted is synced. '+ $
                             'Synced images can only be shifted from "SpaceCrafts A and B Schedule".'+ $
                             ' Command canceled.'
                            RETURN
                          ENDIF 
                        ENDFOR
                      ENDIF

                      cmd_seq= ' (cmd seq '

                      ; see if result needs to be expanded for the cmd_seq entries, if any: - AEE 8/6/03
                      tres= result
                      ind= WHERE(STRPOS(result,cmd_seq) LT 0, cnt)
                      IF (cnt GT 0) THEN $
                        result= result(ind) $
                      ELSE $
                        result= ''

                      ind= WHERE(STRPOS(tres,cmd_seq) GE 0, cnt)
                      FOR i= 0, cnt-1 DO BEGIN  
                        ;tmp= STR_SEP(tres(ind(i)),',')   
                        tmp= STR_SEP(tres(ind(i)),'(')
                        bsf_id= STRMID(tmp(1),8,STRLEN(tmp(1))-9) ; pickup bsf_id from the end of string.
                        tmp= STRTRIM(STR_SEP(tmp(0),','),2) ; pickup OS nums in the string.
                        ;result= [result,tmp(0:N_ELEMENTS(tmp)-2)] ; remove last element the "(cmd seq)".
                        ;result= [result,tmp+' '+bsf_id] ; remove last element the "(cmd seq)".
                      ENDFOR
                      ;result= result(WHERE(result NE ''))

                      tmp_conf= [''] ; AEE 10/23/03

                      ; Check for conflicts only if some of the uniqe OS's (NOT ALL) are to be shifted. 
                      ;IF (N_ELEMENTS(result) LT N_ELEMENTS(oss)) THEN BEGIN  

                      shft_ind= INDGEN(N_ELEMENTS(os_arr.os_num)) ; AEE 1/8/04

                      IF (N_ELEMENTS(tres) LT N_ELEMENTS(toss)) THEN BEGIN   ; AEE 1/6/04

                        shft_ind= FIND_OS_MATCH(os_arr,tres,toss,sched_cmdseq,cmd_seq) ; AEE 1/8/04
                        othr_ind= INDGEN(N_ELEMENTS(os_arr.os_num))
                        FOR i= 0, N_ELEMENTS(shft_ind)-1 DO BEGIN
                          othr= WHERE(othr_ind EQ shft_ind(i))
                          othr_ind(othr)= -1
                        ENDFOR
                        othr_ind= WHERE(othr_ind NE -1) 

                       ; **** AEE 1/8/04 - NOTE: result (or lresult) is used for conflict checking too. Must
                       ; check to see if it needs to be modified, too (i.e. use shft_ind, etc). **** 

                        ;lresult=LONG(result)

                        lresult= os_arr(shft_ind).os_num
                        i= 0

                        temp_os= os_arr(shft_ind)

                        ;be_val=  UTC2STR(TAI2UTC(schedv.study_start),/ECS) +' and '+ $ ; AEE 10/23/03
                        ;         UTC2STR(TAI2UTC(schedv.study_stop),/ECS)

                        ; Delta shift will shift selected Os(es) for the entire scheduled os_num(s)
                        ; So use the whole data time-range for be_val to be used for removing old
                        ; conflicts, if any:

                        be_val=  UTC2STR(TAI2UTC(MIN(os_arr.os_start)),/ECS) +' and '+ $
                                 UTC2STR(TAI2UTC(MAX(os_arr.os_stop)),/ECS)

                        be_val= REPLICATE(be_val,N_ELEMENTS(lresult)) ; AEE 10/23/03

                        temp_os.os_start = temp_os.os_start + delta
                        temp_os.os_stop = temp_os.os_start + temp_os.os_duration

                        ; AEE - 8/14/03 - check for calib lamp conflicts:

                        cosind= WHERE(temp_os.os_lp EQ 3, coscnt)
                        IF (coscnt GT 0) THEN BEGIN ; one or more of OSes to be shifted is cal.lamp
                          ; Because of LED controller leak to all instruments, make sure no other exposure
                          ; of any kind in any telescope is in progress (not only other LED images):
                          IF (FIND_CAL_CONFLICTS(os_arr(othr_ind),temp_os(cosind), $
                                                 defined_os_arr,"shift_os",conf_info)) THEN BEGIN
                            tmp_conf= [tmp_conf,conf_info]
                          ENDIF
                        ENDIF

                        ; Now see any of existing OSes are Calib lamp images and if so check for
                        ; exposure time conflicts with Oses that are being moved:

                        cosind= WHERE(os_arr(othr_ind).os_lp EQ 3, coscnt)
                        IF (coscnt GT 0) THEN BEGIN ; one or more of OSes that is not shifted is cal.lamp
                          IF (FIND_CAL_CONFLICTS(os_arr(othr_ind(cosind)),temp_os, $
                                                 defined_os_arr,"shift_os",conf_info)) THEN BEGIN
                            tmp_conf= [tmp_conf,conf_info]
                          ENDIF
                        ENDIF

                        ; Finally, check all of OSes that are not being shifted for possible CCD confilcts
                        ; with those being shifted:

                        IF (FIND_CONFLICTS(os_arr(othr_ind),temp_os,defined_os_arr,"shift_os",conf_info)) THEN BEGIN
                            tmp_conf= [tmp_conf,conf_info]
                        ENDIF


;                        oind= WHERE(os_arr(othr_ind).os_lp NE 3, ocnt)
;                        IF (ocnt GT 0) THEN BEGIN ; check remaining non-calib oses for conflicts
;                          IF (FIND_CONFLICTS(os_arr(othr_ind(oind)),temp_os,defined_os_arr, $
;                                             "shift_os",conf_info)) THEN BEGIN
;                            tmp_conf= [tmp_conf,conf_info]
;                          ENDIF
;                        ENDIF

                        IF (N_ELEMENTS(tmp_conf) GT 1) THEN BEGIN
                          WIDGET_CONTROL,mdiag,SET_VALUE="Warning - Scheduling conflict(s)"
                            quit = RESPOND_WIDG( /COLUMN,X_SCROLL_SIZE=630, Y_SCROLL_SIZE=600, $
                            mess= ["WARNING - SCHEDULING CONFLICT","",tmp_conf(1:*)], $
                            butt=['Shift (ignore this warning)', 'Cancel Shift'],$
                            group=schedv.base)
                          IF (quit NE 0) THEN BEGIN
                            WIDGET_CONTROL,mdiag,SET_VALUE='Canceled Shift.'
                            PRINT,'Canceled Shift.'
                            RETURN
                          ENDIF ELSE BEGIN
                            ;all_conflicts= [all_conflicts,tmp_conf(1:*)]
                          ENDELSE
                        ENDIF
                      ENDIF

                      CHECK_CONFLICTS_DELTA,all_conflicts,lresult,be_val ;before shifting, remove old conflicts, if any.

                      IF (N_ELEMENTS(tmp_conf) GT 1) THEN all_conflicts= [all_conflicts,tmp_conf(1:*)]

                      ; Shift the sched_cmdseq elements, also ; AEE - 8/6/03

                      ind= WHERE(STRPOS(tres,cmd_seq) GE 0, cnt)
                      FOR i=0, cnt-1 DO BEGIN
                        tmp= tres(ind(i))
                        ;tmp= STRMID(tmp,0,STRPOS(tmp,cmd_seq)-2) ; remove ", (cmd seq)" from the end
                        ;nid= WHERE(sched_cmdseq.os_num+' @ '+STRMID(sched_cmdseq.os_time,0,23) EQ tmp)
                        nid= WHERE(sched_cmdseq.os_num+' @ '+STRMID(sched_cmdseq.os_time,0,23)+ $
                                   ', '+cmd_seq+sched_cmdseq.bsf+')' EQ tmp)
                        IF (DATATYPE(all_sched_cmdseq) EQ 'STC') THEN $
                          anid= WHERE(all_sched_cmdseq.os_num+' @ '+ $
                                      STRMID(all_sched_cmdseq.os_time,0,23)+ $
                                      ', '+cmd_seq+all_sched_cmdseq.bsf+')' EQ tmp)

                        os_nums= LONG(STR_SEP(sched_cmdseq(nid).os_num,','))
                        sched_cmdseq(nid).start_time= $
                          TAI2UTC(UTC2TAI(STR2UTC(sched_cmdseq(nid).start_time))+delta,/ECS)

                        ; Also shift I and A commands that belong to a bsf file:

                        ic_ind= WHERE(ic.sc+STRTRIM(ic.bsf,2) EQ sched_cmdseq(nid).bsf, ic_cnt)
                        IF (ic_cnt GT 0) THEN ic(ic_ind).dt= ic(ic_ind).dt+delta
                        ic_ind= WHERE(rsc.sc+STRTRIM(rsc.bsf,2) EQ sched_cmdseq(nid).bsf, ic_cnt)
                        IF (ic_cnt GT 0) THEN rsc(ic_ind).dt= rsc(ic_ind).dt+delta

                        ; Also shift GT commands that belong to a bsf file:
                        IF (DATATYPE(bsf_gt) NE 'UND') THEN BEGIN 
                          gtc_ind= WHERE(bsf_gt EQ sched_cmdseq(nid).bsf, gtc_cnt)
                          IF (gtc_cnt GT 0) THEN BEGIN
                            stime_gt(gtc_ind)= stime_gt(gtc_ind)+delta
                            etime_gt(gtc_ind)= etime_gt(gtc_ind)+delta
                            IF(DATATYPE(pstime_gt) NE 'UND') THEN BEGIN
                              pstime_gt(gtc_ind)= pstime_gt(gtc_ind)+delta
                              petime_gt(gtc_ind)= petime_gt(gtc_ind)+delta
                            ENDIF
                          ENDIF
                        ENDIF

;help,anid
;stop
;help,all_sched_cmdseq

                        IF (DATATYPE(all_sched_cmdseq) EQ 'STC') THEN $
                          all_sched_cmdseq(anid).start_time= $
                          TAI2UTC(UTC2TAI(STR2UTC(all_sched_cmdseq(anid).start_time))+delta,/ECS)
                        os_times= STR_SEP(sched_cmdseq(nid).os_time,',')
                        tai_os_times= UTC2TAI(STR2UTC(os_times))
                        os_times= TAI2UTC(UTC2TAI(STR2UTC(os_times))+delta,/ECS)
                        sched_cmdseq(nid).os_time= os_times(0)
                        IF (DATATYPE(all_sched_cmdseq) EQ 'STC') THEN $
                          all_sched_cmdseq(anid).os_time= os_times(0)
                        FOR j=1, N_ELEMENTS(os_times)-1 DO BEGIN 
                          sched_cmdseq(nid).os_time = sched_cmdseq(nid).os_time + ','+ os_times(j)
                          IF (DATATYPE(all_sched_cmdseq) EQ 'STC') THEN $
                            all_sched_cmdseq(anid).os_time = all_sched_cmdseq(anid).os_time + ','+ $
                                                             os_times(j)
                        ENDFOR
                      ENDFOR

                      ; AEE 1/9/04 - shift all os_arr times in the shft_ind:

                      os_arr(shft_ind).os_start = os_arr(shft_ind).os_start + delta
                      os_arr(shft_ind).os_stop = os_arr(shft_ind).os_start + os_arr(shft_ind).os_duration
 

                      ; Note: for SECCHI since we don't have contiuous contact, unlike LASCO, the
                      ;       table uploads have to take place during the contact before a scheudle
                      ;       is started. So, we can't/don't need to tag an OS with a table upload.
                      ;       Therefore, the code to do that was removed.
                      ;       We must still, however, provide some sort of code to generate table upload
                      ;       commands to be uploaded as needed.
                      
                      WIDGET_CONTROL, /HOUR
                      SCHEDULE_PLOT, schedv
                      IF (N_ELEMENTS(tres) EQ N_ELEMENTS(toss)) THEN $
                        ;sftstr= "everything" $
                        sftstr= "all observations" $
                      ELSE BEGIN 
                        sftstr= "OS_"+tres(0)
                        FOR k=1, N_ELEMENTS(tres)-1 DO sftstr= sftstr+','+tres(k)
                      ENDELSE
                      WIDGET_CONTROL,mdiag,SET_VALUE='Shifted '+sftstr+' by DELTA = '+ $
                                     STRTRIM(STRING(delta),2)+' Sec.' 
                      PRINT,'Shifted '+sftstr+' by DELTA = '+STRTRIM(STRING(delta),2)+' Sec.'
                   ENDIF ELSE BEGIN
                      WIDGET_CONTROL,mdiag,SET_VALUE='Nothing selected - Canceled Shift.'
                      PRINT,'Nothing selected - Shift Canceled.'
                   ENDELSE
                ENDIF ELSE BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE='Must be in DISPLAY mode to shift scheduled OS''s by DELTA.'
                  PRINT,'Must be in DISPLAY mode to shift scheduled OS''s by DELTA.'
                ENDELSE
             END

   'OS_SIZE' : BEGIN
                WIDGET_CONTROL, schedv.num_text, GET_VALUE=os_num
                sos=os_num
                os_num = LONG(STRTRIM(os_num, 2))
                ;os_num = LONG(STRMID(os_num, 7, STRLEN(os_num)-7))

                WIDGET_CONTROL, event.id, GET_VALUE=os_size
                ;** convert displayed units to bits
                os_size = STRTRIM(os_size, 2)
                WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE=from
                os_size= DOUBLE(STR_SEP(STRCOMPRESS(STRTRIM(os_size,2)),' ')) ; AEE - Jan 06, 03
                os_size = SCHEDULE_CONVERT_UNITS(os_size, from, 0)

                ;** change size of every scheduled instance of this OS
                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                   ind = WHERE(os_arr.os_num EQ os_num)
                   IF (ind(0) NE -1) THEN BEGIN
                      sched_os_size= os_arr(ind(0)).os_size
                      IF(os_arr(ind(0)).os_lp EQ 6) THEN BEGIN
                        ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)
                        sched_os_size= sched_os_size + os_arr(ind(ind1(0))).os_size
                      ENDIF
                      new_os_size= TOTAL(os_size)
                      IF (sched_os_size EQ new_os_size) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='No change in SIZE;  '+sos+' not changed.'
                        PRINT,'No change in SIZE;  '+sos+' not changed.'
                        RETURN
                      END
                      os_arr(ind).os_size = os_size(0)

                      IF (os_arr(ind(0)).os_lp EQ 6) THEN BEGIN
                        lp6_cnts= N_ELEMENTS(ind)
                        IF (lp6_cnts MOD 2 EQ 1) THEN BEGIN
                          onum= STRTRIM(STRING(lp6_cnts),2)
                          WIDGET_CONTROL,mdiag,SET_VALUE='ERROR: Odd number ('+onum+') of HI-Seq scheduled OS '+sos
                          PRINT,'ERROR: Odd number ('+onum+') of HI-Seq scheduled OS '+sos
                          STOP
                        ENDIF ELSE BEGIN
                          ind1= WHERE(os_arr(ind).os_tele EQ os_arr(ind(0)).os_tele) 
                          os_arr[ind(ind1)].os_size = os_size(0)    ; pickup and use HI1 (first os_size) 
                          ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)
                          os_arr[ind(ind1)].os_size = os_size(1)    ; pickup and use HI2 (second os_size)
                        ENDELSE
                      ENDIF

                      WIDGET_CONTROL, /HOUR
                      SCHEDULE_PLOT, schedv
                      ssize= STRTRIM(STRING(LONG(os_size+0.5)),2)

                      szmsg= 'Changed size of every scheduled instance of '+sos+' to '+ssize(0)+ ' bits.'
                      IF (os_arr(ind(0)).os_lp EQ 6) THEN $
                        szmsg= 'Changed size of every scheduled instance of '+sos+' to '+ssize(0)+' and '+ $
                                ssize(1)+ ' bits.'
 
                      PRINT, szmsg
                      WIDGET_CONTROL,mdiag, SET_VALUE= szmsg

                   ENDIF
                ENDIF
             END


   'OS_DUR' : BEGIN

                ; AEE 8/14/03 - NOTE: it is very dangerous to change the OS_DUR specially when
                ; more than one instance is involved (they may overlap), sequences are invloved,
                ; etc.. Instead of changing DURATION, PROC_TIME, or CCD_TIME, they should be
                ; adjusted more carefully when first planned. These are also NOT checked for
                ; conflicts. So, use these ONLY for testing purposes.


                WIDGET_CONTROL, schedv.num_text, GET_VALUE=os_num
                ssn= os_num
                os_num = LONG(STRTRIM(os_num, 2))
                ;os_num = LONG(STRMID(os_num, 7, STRLEN(os_num)-7))

                WIDGET_CONTROL, event.id, GET_VALUE=os_duration
                os_duration = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_duration,2)),' ')) ; AEE 01/07/03

                ;** convert displayed units [secs or mins] to secs
                os_duration = FLOAT(os_duration / schedv.os_dur_factor)

                ;** adjust start time, stop time, and duration of every scheduled instance of this OS
                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                   ind = WHERE(os_arr.os_num EQ os_num, ocnt)
                   IF (ocnt GT 0) THEN BEGIN
                     sched_os_dur= os_arr(ind(0)).os_duration
                     IF(os_arr(ind(0)).os_lp EQ 6) THEN BEGIN
                       ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)
                       sched_os_dur= sched_os_dur + os_arr(ind(ind1(0))).os_duration
                     ENDIF
                     new_os_dur= TOTAL(os_duration)
                     IF (new_os_dur EQ sched_os_dur) THEN BEGIN
                       WIDGET_CONTROL,mdiag,SET_VALUE='No change in DURATION;  '+ssn+' not changed.' 
                       PRINT,'No change in DURATION;  '+ssn+' not changed.'
                       RETURN
                     ENDIF
                    IF (os_arr(ind(0)).os_lp EQ 6 AND N_ELEMENTS(os_duration) NE 2) THEN BEGIN
                      WIDGET_CONTROL,mdiag,SET_VALUE='Warning: Need TWO space separated DURATIONs for HI-Seq.'
                      PRINT,'Warning: Need TWO space separated DURATIONs for HI-Seq.'
                      RETURN 
                    ENDIF
                    FOR k= 0, N_ELEMENTS(os_duration)-1 DO BEGIN
                       IF (k EQ 0) THEN $
                         ind1= WHERE(os_arr(ind).os_tele EQ os_arr(ind(0)).os_tele) $
                       ELSE $ ; k=1
                         ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)

                       IF (os_duration(k) LT $
                           os_arr(ind(ind1(0))).os_pre_proc_time+os_arr(ind(ind1(0))).os_ro_time+ $
                           os_arr(ind(ind1(0))).os_proc_time) THEN BEGIN
                         WIDGET_CONTROL, schedv.os_dur_text,  $
                            SET_VALUE=STRTRIM(STRING(os_arr(ind(ind1(0))).os_duration,schedv.time_fmt1),2)+' ' 
                         WIDGET_CONTROL,mdiag,SET_VALUE='New duration, '+ $
                            STRTRIM(STRING(os_duration(k),schedv.time_fmt1),2)+', is too small - Try again.'
                          PRINT,'New duration, '+ $
                            STRTRIM(STRING(os_duration(k),schedv.time_fmt1),2)+', is too small - Try again.'
                         RETURN
                       ENDIF

                       FOR j= 1, ocnt-1 DO $ 
                          os_arr(ind(ind1(j))).os_start = os_arr(ind(ind1(j-1))).os_start+os_duration(k)
                       os_arr(ind(ind1)).os_stop = os_arr(ind(ind1)).os_start + os_duration(k) 
                       os_arr(ind(ind1)).os_duration = os_duration(k)
                      ENDFOR 
                      WIDGET_CONTROL,mdiag,SET_VALUE='Applied new duration to all instances of '+ssn
                      PRINT,'Applied new duration to all instances of '+ssn
                   ENDIF
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv
                ENDIF
             END


   'OS_PROC' : BEGIN
                WIDGET_CONTROL, schedv.num_text, GET_VALUE=os_num
                os_num = long(STRTRIM(os_num, 2))
                ;os_num = LONG(STRMID(os_num, 7, STRLEN(os_num)-7))

                WIDGET_CONTROL, event.id, GET_VALUE=os_proc_time
                os_proc_time = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_proc_time,2)),' ')) ; AEE 01/07/03
                os_proc_time = FLOAT(os_proc_time / schedv.os_dur_factor)

                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                   ind = WHERE(os_arr.os_num EQ os_num)
                   IF (ind(0) NE -1) THEN BEGIN
                      sched_os_proc= os_arr(ind(0)).os_proc_time
                      IF(os_arr(ind(0)).os_lp EQ 6) THEN BEGIN
                        ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)
                         sched_os_proc= sched_os_proc + os_arr(ind(ind1(0))).os_proc_time
                      ENDIF
                      new_os_proc= TOTAL(os_proc_time)
                      IF (new_os_proc EQ sched_os_proc) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='No change in PROC_TIME'
                        PRINT,'No change in PROC_TIME'
                        RETURN
                      ENDIF
                      IF (os_arr(ind(0)).os_lp EQ 6 AND N_ELEMENTS(os_proc_time) NE 2) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='Warning: HI-Seq requires TWO space separated PROC_TIMEs' 
                        PRINT,'Warning: HI-Seq requires TWO space separated PROC_TIMEs'
                        RETURN
                      END
                      FOR k= 0, N_ELEMENTS(os_proc_time)-1 DO BEGIN
                        IF (k EQ 0) THEN $
                          ind1= WHERE(os_arr(ind).os_tele EQ os_arr(ind(0)).os_tele) $
                        ELSE $ ; k=1
                          ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele) 

                        os_arr(ind(ind1)).os_proc_time = os_proc_time(k)
                      ENDFOR
                   ENDIF
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv
                ENDIF
             END

   'OS_SETUP' : BEGIN 
                WIDGET_CONTROL, schedv.num_text, GET_VALUE=os_num
                os_num = long(STRTRIM(os_num, 2))
;                os_num = LONG(STRMID(os_num, 7, STRLEN(os_num)-7))

                WIDGET_CONTROL, event.id, GET_VALUE=os_setup_time
                os_setup_time = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_setup_time,2)),' '))
                os_setup_time = FLOAT(os_setup_time / schedv.os_dur_factor)
                
                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                   ind = WHERE(os_arr.os_num EQ os_num)
                   IF (ind(0) NE -1) THEN BEGIN
                      sched_os_setup= os_arr(ind(0)).os_setup_time
                      IF(os_arr(ind(0)).os_lp EQ 6) THEN BEGIN
                        ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)
                         sched_os_setup= sched_os_setup + os_arr(ind(ind1(0))).os_setup_time
                      ENDIF
                      new_os_setup= TOTAL(os_setup_time)
                      IF (new_os_setup EQ sched_os_setup) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='No change in SETUP_TIME'
                        PRINT,'No change in SETUP_TIME'
                        RETURN
                      ENDIF
                      IF (os_arr(ind(0)).os_lp EQ 6 AND N_ELEMENTS(os_setup_time) NE 2) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='Warning: HI-Seq requires TWO space separated SETUP_TIMEs' 
                        PRINT,'Warning: HI-Seq requires TWO space separated SETUP_TIMEs'
                        RETURN
                      END
                      WIDGET_CONTROL, schedv.os_exp_text, GET_VALUE=os_exp_time
                      os_exp_time = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_exp_time,2)),' '))
                      os_exp_time = FLOAT(os_exp_time / schedv.os_dur_factor)

                      FOR k= 0, N_ELEMENTS(os_setup_time)-1 DO BEGIN
                        IF (k EQ 0) THEN $
                          ind1= WHERE(os_arr(ind).os_tele EQ os_arr(ind(0)).os_tele) $
                        ELSE $ ; k=1
                          ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele) 
                        os_arr(ind(ind1)).os_setup_time = os_setup_time(k)
                        ; pre_proc_time is always equal to setup_time + exp_time, so set it correctly:
                        os_arr(ind(ind1)).os_pre_proc_time = os_setup_time(k) + os_exp_time(k)
                      ENDFOR
                   ENDIF
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv
                ENDIF

             END

   'OS_EXP' : BEGIN

             END

   'OS_ROT' : BEGIN
                WIDGET_CONTROL, schedv.num_text, GET_VALUE=os_num
                os_num = long(STRTRIM(os_num, 2))
;                os_num = LONG(STRMID(os_num, 7, STRLEN(os_num)-7))

                WIDGET_CONTROL, event.id, GET_VALUE=os_ro_time
                os_ro_time = FLOAT(STR_SEP(STRCOMPRESS(STRTRIM(os_ro_time,2)),' ')) 

                os_ro_time = FLOAT(os_ro_time / schedv.os_dur_factor)

                IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                   ind = WHERE(os_arr.os_num EQ os_num)
                   IF (ind(0) NE -1) THEN BEGIN
                      sched_os_rot= os_arr(ind(0)).os_ro_time
                      IF(os_arr(ind(0)).os_lp EQ 6) THEN BEGIN
                        ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele)
                        sched_os_rot= sched_os_rot + os_arr(ind(ind1(0))).os_ro_time
                      ENDIF
                      new_os_rot= TOTAL(os_ro_time)
                      IF (new_os_rot EQ sched_os_rot) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='No change in CCD_TIME'
                        PRINT,'No change in CCD_TIME'
                        RETURN
                      ENDIF
                      IF (os_arr(ind(0)).os_lp EQ 6 AND N_ELEMENTS(os_ro_time) NE 2) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='Warning: HI-Seq requires TWO space separated CCD_TIMEs' 
                        PRINT,'Warning: HI-Seq requires TWO space separated CCD_TIMEs'
                        RETURN
                      END
                      FOR k= 0, N_ELEMENTS(os_ro_time)-1 DO BEGIN
                        IF (k EQ 0) THEN $
                          ind1= WHERE(os_arr(ind).os_tele EQ os_arr(ind(0)).os_tele) $
                        ELSE $ ; k=1
                          ind1= WHERE(os_arr(ind).os_tele NE os_arr(ind(0)).os_tele) 

                        os_arr(ind(ind1)).os_ro_time = os_ro_time(k)
                      ENDFOR
                   ENDIF
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv
                ENDIF
             END


   'MOVE_OS': IF schedv.mode EQ 1 THEN BEGIN     ; only for edit mode
                ; AEE - 7/30/03 - Move all or an instance of a OS to a new location 
                ; ask user for a start-date for the selected OS_NUM (displayed using the 
                ; start/stop times, count, etc.) to be moved to:

                ; Get current partamerets for the selected OS:
                WIDGET_CONTROL, schedv.num_text,   GET_VALUE=num_text
                os_num = STRTRIM(num_text, 2)

                IF (STRMID(num_text,0,2) EQ 'OP') THEN RETURN

                ;os_num=str_sep(num_text,':')
                os_num= STRTRIM(os_num(1),2)  ; pickup os_num

                WIDGET_CONTROL, schedv.count_text,   GET_VALUE=oscount    & oscount= oscount(0)
                WIDGET_CONTROL, schedv.start_text,   GET_VALUE=start_dte  & start_dte= start_dte(0)
                WIDGET_CONTROL, schedv.stop_text,    GET_VALUE=stop_dte   & stop_dte= stop_dte(0)
                WIDGET_CONTROL, schedv.os_size_text, GET_VALUE=ossize     & ossize= ossize(0)
                WIDGET_CONTROL, schedv.delta_text,   GET_VALUE=osdelta    & osdelta= osdelta(0)
                WIDGET_CONTROL, schedv.os_dur_text,  GET_VALUE=osduration & osduration= osduration(0)
                WIDGET_CONTROL, schedv.os_proc_text,  GET_VALUE=osproc    & osproc= osproc(0)
                WIDGET_CONTROL, schedv.os_rot_text,  GET_VALUE=osrot      & osrot= osrot(0)

                sdtai= UTC2TAI(STR2UTC(start_dte))
                edtai= UTC2TAI(STR2UTC(stop_dte))
                be_val= TAI2UTC(sdtai,/ECS)+' and '+TAI2UTC(edtai,/ECS) 

                dt= 0.01  ; AEE 4/8/03

                osr= BLK_SEQ_OS(os_arr,sched_cmdseq,os_num,sdtai-dt,edtai+dt)
                IF (osr NE 0) THEN BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE='os_num = '+ STRN(osr)+ $
                  ' belongs to a Block Sequence and can NOT be removed. MOVE Command ignored.'+ $
                  ' Use DELTA to move entire Block Sequence.'
                  PRINT,'os_num = '+STRN(osr)+ $
                  ' belongs to a Block Sequence and can NOT be removed. MOVE Command ignored.'+ $
                  ' Use DELTA to move entire Block Sequence.'
                  RETURN
                ENDIF

                ; Don't allow shift if any of Oses that are synched but shifting is 
                ; done from S/C A or B:

                IF (schedv.sc NE 0) THEN BEGIN ; S/C A or B
                  rmind= WHERE(os_arr.os_num EQ os_num AND os_arr.os_start+0.001 GE SDTAI AND $
                               os_arr.os_stop LE edtai+0.001, rmcnt)
                  IF (rmcnt GT 0) THEN BEGIN
                    ab= WHERE(os_arr(rmind).sc EQ 'AB', abcnt)
                    IF (abcnt GT 0) THEN BEGIN
                      IF (abcnt EQ 1) THEN $ 
                        msg='1 instance of OS_'+os_num+' is AB synced.' $
                      ELSE $
                        msg=STRTRIM(abcnt,2)+' instances of OS_'+os_num+' are Synced.'
                      WIDGET_CONTROL,mdiag,SET_VALUE= msg+' These can only be shifted from '+ $
                      '"SpaceCrafts A and B Schedule". MOVE command canceled.'
                      RETURN
                    ENDIF
                  ENDIF
                ENDIF

                move_to= SHIFT_OS(os_num,start_dte, stop_dte, oscount) 
                mvtai= UTC2TAI(STR2UTC(move_to))

                IF (mvtai NE sdtai) THEN BEGIN
                  IF (mvtai LT schedv.startdis OR mvtai GE schedv.enddis) THEN BEGIN
                    WIDGET_CONTROL,mdiag,SET_VALUE='Move OS '+os_num+' Canceled. Move Date Out of Plot Range.'
                    PRINT,'Move OS  '+os_num+' Canceled. Move Date Out of Plot Range.'
                    RETURN
                  END

                  list= WHERE(os_arr.os_num EQ os_num AND  $
                        os_arr.os_start GE sdtai-dt AND    $
                        os_arr.os_stop LE edtai+dt, lcnt) 

                  IF (lcnt EQ 0) THEN BEGIN  ; AEE 10/22/03
                    WIDGET_CONTROL,mdiag, $
                      SET_VALUE='MOVE ignored - No instacnes of OS '+os_num+' within the displayed times.'
                    PRINT,'MOVE ignored - No instacnes of OS '+os_num+' within the displayed times.'
                    RETURN
                  END

                  ; AEE 8/13/03 - Added confict checking:

                  temp_os= os_arr(list)
                  temp_os.os_start= temp_os.os_start + (mvtai-sdtai)
                  temp_os.os_stop = temp_os.os_stop  + (mvtai-sdtai)

                  rind= WHERE(os_arr.os_num NE os_num OR  $
                        (os_arr.os_num EQ os_num AND $
                         (os_arr.os_start LT sdtai-dt OR $
                         os_arr.os_stop GT edtai+dt)), rcnt)

                  tmp_conf= [''] ; AEE 10/22/03

                  IF (rcnt GT 0) THEN BEGIN 
                    ; AEE - 8/14/03 - check for calib lamp conflicts:

                    cosind= WHERE(temp_os.os_lp EQ 3, coscnt)
                    IF (coscnt GT 0) THEN BEGIN ; one or more of OSes to be shifted is cal.lamp
                      ; Because of LED controller leak to all instruments, make sure no other exposure
                      ; of any kind in any telescope is in progress (not only other LED images):
                      IF (FIND_CAL_CONFLICTS(os_arr(rind),temp_os(cosind), $
                                             defined_os_arr,"shift_os",conf_info)) THEN BEGIN
                        tmp_conf= [tmp_conf,conf_info]
                      ENDIF
                    ENDIF

                    ; Now see any of existing OSes are Calib lamp images and if so check for 
                    ; exposure time conflicts with Oses that are being moved:

                    cosind= WHERE(os_arr(rind).os_lp EQ 3, coscnt)
                    IF (coscnt GT 0) THEN BEGIN ; one or more of OSes that is not shifted is cal.lamp
                      IF (FIND_CAL_CONFLICTS(os_arr(rind(cosind)),temp_os, $
                                             defined_os_arr,"shift_os",conf_info)) THEN BEGIN
                        tmp_conf= [tmp_conf,conf_info]
                      ENDIF
                    ENDIF


                    ; Finally, check all of OSes that are not being shifted for possible CCD confilcts
                    ; with those being shifted:

                    IF (FIND_CONFLICTS(os_arr(rind),temp_os,defined_os_arr,"shift_os",conf_info)) THEN BEGIN
                        tmp_conf= [tmp_conf,conf_info]
                    ENDIF

                    oind= WHERE(os_arr(rind).os_lp NE 3, ocnt)
;                    IF (ocnt GT 0) THEN BEGIN ; check the remaining non-calib oses for conflicts
;                      IF (FIND_CONFLICTS(os_arr(rind(oind)),temp_os,defined_os_arr,"shift_os",conf_info)) THEN BEGIN 
;                        tmp_conf= [tmp_conf,conf_info]
;                      ENDIF
;                    ENDIF 
                    
                    IF (N_ELEMENTS(tmp_conf) GT 1) THEN BEGIN
                      WIDGET_CONTROL,mdiag,SET_VALUE="Warning - Scheduling conflict(s)"
                         quit = RESPOND_WIDG( /COLUMN,X_SCROLL_SIZE=630, Y_SCROLL_SIZE=600, $
                         mess= ["WARNING - SCHEDULING CONFLICT","",tmp_conf(1:*)], $
                         butt=['Move (ignore this warning)', 'Cancel Move'],$
                         group=schedv.base)
                      IF (quit NE 0) THEN BEGIN
                        WIDGET_CONTROL,mdiag,SET_VALUE='Canceled Move.'
                        PRINT,'Canceled Move.'
                        RETURN
                      ENDIF ELSE BEGIN ; AEE 10/21/03
                        ;all_conflicts= [all_conflicts,tmp_conf(1:*)]
                      ENDELSE
                    ENDIF

                  ENDIF

                  CHECK_CONFLICTS,all_conflicts,os_num,be_val ; before moving, remove old conflicts, if any.

                  IF (N_ELEMENTS(tmp_conf) GT 1) THEN all_conflicts= [all_conflicts,tmp_conf(1:*)]

                  os_arr(list).os_start= os_arr(list).os_start + (mvtai-sdtai)
                  os_arr(list).os_stop = os_arr(list).os_stop  + (mvtai-sdtai)

                  new_start= UTC2STR(TAI2UTC(os_arr(list(0)).os_start),/ECS)
                  new_stop = UTC2STR(TAI2UTC(os_arr(list(lcnt-1)).os_stop),/ECS)
                  WIDGET_CONTROL, schedv.start_text, SET_VALUE= new_start
                  WIDGET_CONTROL, schedv.stop_text, SET_VALUE= new_stop

                  ; 
                  ; Put the input focus in the start time text widget window
                  ; Highlight the hour part of the time.
                  ;

                  WIDGET_CONTROL, schedv.start_text, /INPUT_FOCUS
                  WIDGET_CONTROL, schedv.start_text, SET_TEXT_SELECT=[11,2]
                  schedv.last_text = schedv.start_text
                  WIDGET_CONTROL, /HOUR
                  SCHEDULE_PLOT, schedv
                  WIDGET_CONTROL,mdiag,SET_VALUE='Move OS '+os_num+' Successful.'
                  PRINT,'Move OS '+os_num+' Successful.'  
                ENDIF ELSE BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE='Move OS '+os_num+' Canceled.'
                  PRINT,'Move OS  '+os_num+' Canceled.'
                ENDELSE

              ENDIF


   'ADD' : IF schedv.mode EQ 1 THEN BEGIN	; only for edit mode	

		   ;
		   ; Using the current start and stop time and the current
		   ; study name, insert this new study into the plan,
		   ; IF the inputs are valid.
		   ;

		   ;
		   ; Read and save the current values from the text windows,
		   ; incase the user has modified the times without a RETURN.
		   ;

                   ;** see if user is scheduling an OP or an OS
                   WIDGET_CONTROL, schedv.num_text,   GET_VALUE=num_text
                   num_text = STRTRIM(num_text, 2)

                IF (STRMID(num_text,0,2) EQ 'OP') THEN BEGIN		;** Scheduling an OP

                        op_num = LONG(STRMID(num_text, 7, STRLEN(num_text)-7))
 	                WIDGET_CONTROL, schedv.start_text, GET_VALUE=start_value
		        WIDGET_CONTROL, schedv.stop_text,    GET_VALUE=stop_value
	   	        start_value = UTC2TAI(start_value(0))	; ASCII to TAI
	   	        stop_value  = UTC2TAI(stop_value(0))	; ASCII to TAI
                        loc = WHERE(op_arr.num EQ op_num)
                        IF (loc(0) GE 0) THEN BEGIN		;** OP already scheduled - change it
                           WIDGET_CONTROL,mdiag,SET_VALUE='OP already scheduled - change it'
                           print, 'OP already scheduled - change it'
                           op_arr(loc(0)).op_start = start_value
                           op_arr(loc(0)).op_stop = stop_value
                        ENDIF ELSE BEGIN 			;** scheduling new OP
                           WIDGET_CONTROL,mdiag,SET_VALUE='scheduling new OP = '+strtrim(string(op_num),2)
                           print, 'scheduling new OP = '+strtrim(string(op_num),2)
                           op = op_struct
                           op.num = op_num
                           op.op_start = start_value
                           op.op_stop = stop_value
                           op_arr = [op_arr,op]			;** append to scheduled op_arr
                        ENDELSE

                ENDIF ELSE BEGIN					;** Scheduling an OS
                  IF (DATATYPE(defined_os_arr) EQ 'UND') THEN BEGIN
                    WIDGET_CONTROL,mdiag,SET_VALUE='No OS Selected - Add Ignored.'
                    PRINT,'No OS Selected - Add Ignored.'
                    RETURN
                  ENDIF
                  ret_status= ADD_OS(schedv)
                  IF (ret_status EQ 0) THEN RETURN
                ENDELSE						;** Scheduling an OS

		   ;
	   	   ; Verify times
		   ; 
	   	   ; Put new stud[y|ies] into plan
		   ; 
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv

		   ; 
		   ; Put the input focus in the start time text widget window
		   ; Highlight the hour part of the time.
		   ;
		
		   WIDGET_CONTROL, schedv.start_text, /INPUT_FOCUS
		   WIDGET_CONTROL, schedv.start_text, SET_TEXT_SELECT=[11,2]
	           schedv.last_text = schedv.start_text


		  ENDIF		


   'DELETE' : IF (schedv.mode EQ 1) THEN BEGIN	; NOT available in display mode
                ret_status= REMOVE_OS(schedv)
       	      ENDIF


   'SUMMARY' : BEGIN
                   WIDGET_CONTROL, /HOUR
                   WIDGET_CONTROL, schedv.num_text,   GET_VALUE=num_text
                   num_text = STRTRIM(num_text[0], 2)
    	    	   os_num=long(num_text)
                   ;IF (STRMID(num_text,0,2) EQ 'OS') THEN BEGIN		;** OS Summary
                   ;   os_num = LONG(STRMID(num_text, 7, STRLEN(num_text)-7))
                      OS_SUMMARY, os_num, schedv.base

                   ;ENDIF ELSE BEGIN					;** OP Details
                   ;ENDELSE
                   WSET, schedv.win_index
                   !x = schedv.x & !y = schedv.y & !p = schedv.p
               END

   'DETAILS' : BEGIN
                   WIDGET_CONTROL, schedv.num_text,   GET_VALUE=num_text
                   os_num = long(STRTRIM(num_text, 2))

                   ;IF (STRMID(num_text,0,2) EQ 'OS') THEN BEGIN		;** OS Details
                   ;   os_num = LONG(STRMID(num_text, 7, STRLEN(num_text)-7))
                      DEFINE_OS,/keep_common ;AEE - added keep_common key, May 2002
                      SCHEDULE_CALL_OS, os_num
                   ;ENDIF ELSE BEGIN					;** OP Details
                   ;   ev = {ev, id:0L, top:0L, handler:0L}
                   ;   ev.id = schedv.base
                   ;   op_num = LONG(STRMID(num_text, 7, STRLEN(num_text)-7))
                   ;   PLAN_SET, ev, op_num
                   ;ENDELSE
                   WSET, schedv.win_index
                   !x = schedv.x & !y = schedv.y & !p = schedv.p
               END

   'CONFLICTS' : BEGIN

;help,all_conflicts
;stop

          
                  tmpconf= all_conflicts 
                  IF (schedv.sc EQ 1) THEN tmpconf= GET_SC_CONF(tmpconf,'A')
                  IF (schedv.sc EQ 2) THEN tmpconf= GET_SC_CONF(tmpconf,'B')
                  quit = RESPOND_WIDG( /COLUMN,X_SCROLL_SIZE=640, Y_SCROLL_SIZE=600, $
                  ;mess= ["Current Scheduling Conflicts",all_conflicts], $
                  mess= ["Current Scheduling Conflicts",tmpconf], $
                  butt= ['Quit'],group=schedv.base)
               END

   'DATA_RATES' : BEGIN
                   IF (event.index EQ schedv.drate ) THEN RETURN
                   schedv.drate= event.index
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv
               END

   'DISP_MODE' : BEGIN
                 ; Only active for S/C A or B:

                 ; If Full selected again, don't do anything (return): 
                 IF (event.index EQ 0 AND schedv.smode EQ 0) THEN RETURN

                 schedv.smode= event.index

                 IF (DATATYPE(os_arr) NE 'STC') THEN BEGIN
                   WIDGET_CONTROL,mdiag,SET_VALUE=' Empty Schedule' 
                   IF (schedv.smode EQ 1) THEN BEGIN ; 'Partial' was selected.
                     WIDGET_CONTROL,schedv.col1, SENSITIVE= 0
                     WIDGET_CONTROL,schedv.sc_sel, SENSITIVE= 0
                     WIDGET_CONTROL,schedv.jump, SENSITIVE= 0
                   ENDIF ELSE BEGIN ; 'Full' was selected.
                     WIDGET_CONTROL,schedv.col1, SENSITIVE= 1
                     WIDGET_CONTROL,schedv.sc_sel, SENSITIVE= 1
                     WIDGET_CONTROL,schedv.jump, SENSITIVE= 1
                   ENDELSE
                   RETURN
                 ENDIF

                 IF (schedv.smode EQ 1) THEN BEGIN ; 'Partial' was selected.
                
                   WIDGET_CONTROL,mdiag,SET_VALUE='Switched to "Partial" schedule'
 
                   WIDGET_CONTROL,schedv.col1, SENSITIVE= 0
                   WIDGET_CONTROL,schedv.sc_sel, SENSITIVE= 0
                   WIDGET_CONTROL,schedv.jump, SENSITIVE= 0
                   ;WIDGET_CONTROL,schedv.gen, SENSITIVE= 1 ;does not work since parent, col1, is inactive
                   ;WIDGET_CONTROL,schedv.drop_span, SENSITIVE= 1 ; does not work since col1 is inactive

                   n_os= N_ELEMENTS(os_arr)

                   list=['']
                   func=['']
                   eu= WHERE(os_arr.os_tele EQ 0, eucnt)
                   IF (eucnt GT 0) THEN BEGIN 
                     list= [list,'EUVI']
                     func= [func,'os_arr.os_tele EQ 0']
                   ENDIF
                   c1= WHERE(os_arr.os_tele EQ 1, c1cnt)
                   IF (c1cnt GT 0) THEN BEGIN
                     list= [list,'COR1']
                     func= [func,'os_arr.os_tele EQ 1']
                   ENDIF
                   c2= WHERE(os_arr.os_tele EQ 2, c2cnt)
                   IF (c2cnt GT 0) THEN BEGIN
                     list= [list,'COR2']
                     func= [func,'os_arr.os_tele EQ 2']
                   ENDIF
                   h1= WHERE(os_arr.os_tele EQ 3, h1cnt)
                   IF (h1cnt GT 0) THEN BEGIN
                     list= [list,'HI1']
                     func= [func,'os_arr.os_tele EQ 3']
                   ENDIF
                   h2= WHERE(os_arr.os_tele EQ 4, h2cnt)
                   IF (h2cnt GT 0) THEN BEGIN
                     list= [list,'HI2']
                     func= [func,'os_arr.os_tele EQ 4']
                   ENDIF
                   n= WHERE(os_arr.os_lp EQ 1, ncnt)
                   IF (ncnt GT 0) THEN BEGIN
                     list= [list,'Normal']
                     func= [func,'os_arr.os_lp EQ 1']
                   ENDIF
                   d= WHERE(os_arr.os_lp EQ 0, dcnt)
                   IF (dcnt GT 0) THEN BEGIN
                     list= [list,'Double']
                     func= [func,'os_arr.os_lp EQ 0']
                   ENDIF
                   k= WHERE(os_arr.os_lp EQ 2, kcnt)
                   IF (kcnt GT 0) THEN BEGIN
                     list= [list,'Dark']
                     func= [func,'os_arr.os_lp EQ 2']
                   ENDIF
                   l= WHERE(os_arr.os_lp EQ 3, lcnt)
                   IF (lcnt GT 0) THEN BEGIN
                     list= [list,'Led']
                     func= [func,'os_arr.os_lp EQ 3']
                   ENDIF
                   c= WHERE(os_arr.os_lp EQ 4, ccnt)
                   IF (ccnt GT 0) THEN BEGIN
                     list= [list,'Continuous']
                     func= [func,'os_arr.os_lp EQ 4']
                   ENDIF
                   s= WHERE(os_arr.os_lp EQ 5 OR os_arr.os_lp EQ 6, scnt)
                   IF (scnt GT 0) THEN BEGIN
                     list= [list,'Sequence']
                     func= [func,'os_arr.os_lp EQ 5 OR os_arr.os_lp EQ 6']
                   ENDIF

                   nb= WHERE(os_arr.bsf EQ "", nbcnt)
                   IF (nbcnt GT 0 AND nbcnt LT n_os) THEN BEGIN
                     list= [list,'Non BSF']
                     func= [func,'os_arr.bsf EQ ""']
                   ENDIF 
                   bsfs= WHERE(os_arr.bsf NE "", abscnt)
                   IF (abscnt GT 0) THEN BEGIN
                     list= [list,'All BSF']
                     func= [func,'os_arr.bsf NE ""']
                     absf= sched_cmdseq.bsf
                     FOR i= 0, N_ELEMENTS(absf)-1 DO BEGIN
                       list= [list,'    "'+absf(i)+'" bsf']
                       func= [func,'os_arr.bsf EQ "'+absf(i)+'"'] 
                     ENDFOR 
                   ENDIF

                   ns= WHERE(os_arr.set_id EQ 0, nscnt)
                   IF (nscnt GT 0 AND nscnt LT n_os) THEN BEGIN
                     list= [list,'Non Sets']
                     func= [func,'os_arr.set_id EQ 0']
                   ENDIF
                   sets= WHERE(os_arr.set_id GT 0, setcnt)
                   IF (setcnt GT 0) THEN BEGIN
                     list= [list,'All Sets']
                     func= [func,'os_arr.set_id GT 0']
                     asets= defined_set_arr.set_id
                     FOR i= 0, N_ELEMENTS(asets)-1 DO BEGIN
                       list= [list,'    Set '+STRTRIM(asets(i),2)]
                       func= [func,'os_arr.set_id EQ '+STRTRIM(asets(i),2)]
                     ENDFOR
                   ENDIF 

                   ns= WHERE(os_arr.sc NE 'AB', nscnt)
                   IF (nscnt GT 0 AND nscnt LT n_os) THEN BEGIN
                     list= [list,'Non Synced']
                     func= [func,'os_arr.sc NE "AB"']
                   ENDIF
                   synced= WHERE(os_arr.sc EQ 'AB', syncnt)
                   IF (syncnt GT 0) THEN BEGIN
                     list= [list,'Synced']
                     func= [func,'os_arr.sc EQ "AB"']
                   ENDIF 

                   list= list(1:*)
                   func= func(1:*)
                   ;picked= SELECT_MODES(list) 
                   picked= SELECT_MODES(list,func) 
                   pk= WHERE(picked EQ 1, pkcnt)

                   IF (pkcnt GT 0) THEN BEGIN
                     tot= 0
                     qry= 'query= WHERE('
                     msg=''
                     tel= WHERE(STRPOS(func(pk),'os_tele') GT 0,telcnt)
                     IF (telcnt GT 0) THEN BEGIN
                       qry= qry+'('
                       msg= msg+'('
                       FOR p= 0, telcnt-2 DO BEGIN
                         tot= tot+1
                         qry= qry+func(pk(tel(p)))+' OR ' 
                         msg= msg+list(pk(tel(p)))+' or '
                       ENDFOR
                       tot= tot+1
                       oper= ') '
                       IF (tot LT pkcnt) THEN oper= ') AND '
                       qry= qry+func(pk(tel(telcnt-1)))+oper                    
                       msg= msg+list(pk(tel(telcnt-1)))+oper
                     ENDIF

                     lp = WHERE(STRPOS(func(pk),'os_lp') GT 0,lpcnt)
                     IF (lpcnt GT 0) THEN BEGIN
                       qry= qry+'('
                       msg= msg+'('
                       FOR p= 0, lpcnt-2 DO BEGIN
                         tot= tot+1
                         qry= qry+func(pk(lp(p)))+' OR '
                         msg= msg+list(pk(lp(p)))+' or '
                       ENDFOR
                       tot= tot+1
                       oper= ') '
                       IF (tot LT pkcnt) THEN oper= ') AND '
                       qry= qry+func(pk(lp(lpcnt-1)))+oper                    
                       msg= msg+list(pk(lp(lpcnt-1)))+oper
                     ENDIF

                     cf= WHERE(STRPOS(func(pk),'bsf') GT 0,cfcnt)
                     IF (cfcnt GT 0) THEN BEGIN
                       qry= qry+'('
                       msg= msg+'('
                       FOR p= 0, cfcnt-2 DO BEGIN
                         tot= tot+1
                         qry= qry+func(pk(cf(p)))+' OR '
                         msg= msg+STRTRIM(list(pk(cf(p))),2)+' or '
                       ENDFOR
                       tot= tot+1
                       oper= ') '
                       IF (tot LT pkcnt) THEN oper= ') AND '
                       qry= qry+func(pk(cf(cfcnt-1)))+oper
                       msg= msg+STRTRIM(list(pk(cf(cfcnt-1))),2)+oper
                     ENDIF

                     si= WHERE(STRPOS(func(pk),'set_id') GT 0,sicnt)
                     IF (sicnt GT 0) THEN BEGIN
                       qry= qry+'('
                       msg= msg+'('
                       FOR p= 0, sicnt-2 DO BEGIN
                         tot= tot+1
                         qry= qry+func(pk(si(p)))+' OR '
                         msg= msg+STRTRIM(list(pk(si(p))),2)+' or '
                       ENDFOR
                       tot= tot+1
                       oper= ') '
                       IF (tot LT pkcnt) THEN oper= ') AND '
                       qry= qry+func(pk(si(sicnt-1)))+oper
                       msg= msg+STRTRIM(list(pk(si(sicnt-1))),2)+oper
                     ENDIF

                     sy = WHERE(STRPOS(func(pk),'os_arr.sc') GE 0,sycnt)
                     IF (sycnt GT 0) THEN BEGIN
                       qry= qry+'('
                       msg= msg+'('
                       FOR p= 0, sycnt-2 DO BEGIN
                         tot= tot+1
                         qry= qry+func(pk(sy(p)))+' OR '
                         msg= msg+list(pk(sy(p)))+' or '
                       ENDFOR
                       tot= tot+1
                       oper= ') '
                       IF (tot LT pkcnt) THEN oper= ') AND '
                       qry= qry+func(pk(sy(sycnt-1)))+oper                    
                       msg= msg+list(pk(sy(sycnt-1)))+oper
                     ENDIF
                     

                     qry= qry+',fcnt)'
                     s=EXECUTE(qry)

                     IF (s NE 1) THEN BEGIN
                       WIDGET_CONTROL,mdiag,SET_VALUE= 'Invalid Selection: '+msg
                       RETURN
                     ENDIF 
                      
;help,os_arr,tos_arr,sche_cmdseq
;stop 
                     IF (fcnt GT 0) THEN BEGIN
                       tosarr= os_arr
                       os_arr= os_arr(query)
                       WIDGET_CONTROL, /HOUR
                       SCHEDULE_PLOT, schedv
                       WIDGET_CONTROL,mdiag,SET_VALUE='Results for: '+msg 
                       os_arr= tosarr  ; store orignal os_arr after each "Partial" plot is done.
                     ENDIF ELSE BEGIN
                       tosarr= os_arr
                       os_arr= 0
                       WIDGET_CONTROL, /HOUR
                       SCHEDULE_PLOT, schedv
                       WIDGET_CONTROL,mdiag,SET_VALUE=' No images for: '+msg
                       os_arr= tosarr  ; store orignal os_arr after each "Partial" plot is done.
                     ENDELSE
                   ENDIF
                 ENDIF ELSE BEGIN ; Full selected.
                   WIDGET_CONTROL,mdiag,SET_VALUE='Back to "Full" schedule'
                   IF schedv.mode EQ 0 THEN WIDGET_CONTROL,schedv.edit_study, SENSITIVE=0
                   WIDGET_CONTROL,schedv.col1, SENSITIVE= 1 
                   WIDGET_CONTROL,schedv.sc_sel, SENSITIVE= 1 
                   WIDGET_CONTROL,schedv.jump, SENSITIVE= 1
                   WIDGET_CONTROL, /HOUR
                   SCHEDULE_PLOT, schedv

;help,schedv.smode
;stop
                  
                 ENDELSE
               END


   'SpaceCraft' : BEGIN
               IF (schedv.sc EQ event.index) THEN RETURN
               schedv.sc= event.index

               CASE schedv.sc OF
                 0: BEGIN  ; SpaceCraft - A and B selected
                     msg= 'Changed to SpaceCrafts A and B Schedule'
                     ; De-activate Sets and BlockSeq for AB schedule:
                     WIDGET_CONTROL,schedv.sched_set, SENSITIVE=0
                     WIDGET_CONTROL,schedv.sched_bs, SENSITIVE=0
                     WIDGET_CONTROL,schedv.sconf, SENSITIVE=0 
                     WIDGET_CONTROL,schedv.sca, SENSITIVE=0 
                     WIDGET_CONTROL,schedv.sdoor, SENSITIVE=0
                     kap_resource_arr = 0
                     KAP_nrt_reserved_arr = 0
                     kap_iie_arr = 0
                     WIDGET_CONTROL,schedv.dmodes, SENSITIVE=0 ; inactive for S/C "A and B"
                     OS_INIT
                    END
                 1: BEGIN  ; SpaceCraft - A selected
                     msg= 'Changed to SpaceCraft A Schedule'
                     sc_id= 'A'
                     ; Activate Sets and BlockSeq for A or B schedule:
                     WIDGET_CONTROL,schedv.sched_set, SENSITIVE=1
                     WIDGET_CONTROL,schedv.sched_bs, SENSITIVE=1
                     WIDGET_CONTROL,schedv.sca, SENSITIVE=1
                     WIDGET_CONTROL,schedv.sdoor, SENSITIVE=1
                     IF schedv.mode EQ 1 THEN WIDGET_CONTROL,schedv.sconf, SENSITIVE=1
                     READ_SCA, 1, schedv.startdis, schedv.enddis, /DEFAULT
                     WIDGET_CONTROL,schedv.dmodes, SENSITIVE=1
                     OS_INIT
                    END
                 2: BEGIN  ; SpaceCraft - B selected
                     msg= 'Changed to SpaceCraft B Schedule'
                     sc_id= 'B'
                     ; Activate Sets and BlockSeq for A or B schedule:
                     WIDGET_CONTROL,schedv.sched_set, SENSITIVE=1
                     WIDGET_CONTROL,schedv.sched_bs, SENSITIVE=1
                     WIDGET_CONTROL,schedv.sca, SENSITIVE=1
                     WIDGET_CONTROL,schedv.sdoor, SENSITIVE=1
                     IF schedv.mode EQ 1 THEN WIDGET_CONTROL,schedv.sconf, SENSITIVE=1
                     READ_SCA, 2, schedv.startdis, schedv.enddis, /DEFAULT
                     WIDGET_CONTROL,schedv.dmodes, SENSITIVE=1
                     OS_INIT
                    END
               ENDCASE

;help,doors,pdoors,sc_euvi,sc_cor1,sc_cor2,cdoor_euvi,odoor_euvi,cdoor_cor1,odoor_cor1,cdoor_cor2,odoor_cor2
;stop

               WIDGET_CONTROL,mdiag,SET_VALUE= msg
               PRINT,msg

               ; Assign images for specific spacecraft selection (including those
               ; belonging to both spacecrafts), to os_arr and the remaining images,
               ; if any, to remain_os_arr so what ever is done after this code uses
               ; os_arr containing selected spacecraft schedule. Once spacecraft is
               ; changed again, the os_arr and remaing_os_arr are merged and re-asigned:

               IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                 IF (DATATYPE(remain_os_arr) NE 'STC') THEN BEGIN
                   remain_os_arr= os_arr
                 ENDIF ELSE BEGIN
                   os_arr= [remain_os_arr,os_arr]
                 ENDELSE

                 IF (schedv.sc GT 0) THEN BEGIN  ;S/C A or S/C B
                   ind1= WHERE(os_arr.sc NE sc_id AND os_arr.sc NE 'AB', cnt1)
                   ind= WHERE(os_arr.sc EQ sc_id OR os_arr.sc EQ 'AB', cnt)
                   IF (cnt1 GT 0) THEN BEGIN
                     remain_os_arr= os_arr(ind1) 
                   ENDIF ELSE BEGIN
                     remain_os_arr= 0
                   ENDELSE
                   IF (cnt GT 0) THEN BEGIN 
                     os_arr= os_arr(ind) 
                   ENDIF ELSE BEGIN
                     os_arr= 0
                   ENDELSE
                 ENDIF ELSE BEGIN ; S/C AB
                   remain_os_arr= 0
                 ENDELSE
               ENDIF ELSE BEGIN ; os_arr NE 'STC'
                 IF (DATATYPE(remain_os_arr) EQ 'STC') THEN BEGIN
                   os_arr= remain_os_arr
                   remain_os_arr= 0
                 ENDIF 
               ENDELSE

               IF (DATATYPE(all_defined_os_arr) NE 'STC') THEN BEGIN
                 IF (DATATYPE(defined_os_arr) EQ 'STC') THEN all_defined_os_arr= defined_os_arr 
               ENDIF ELSE BEGIN
                 IF (DATATYPE(defined_os_arr) EQ 'STC') THEN BEGIN
                   all_defined_os_arr= [all_defined_os_arr,defined_os_arr]
                   all_defined_os_arr= all_defined_os_arr(UNIQ_NOSORT(all_defined_os_arr.os_num))
                 ENDIF
               ENDELSE

; Note: following code removes defintion of deleted OSes from defined_os_arr. Commented them out 
; to keep the defintions so that they can be scheduled again if needed (instead of starting from
; scratch by going back to define_os menu to try to recreate them). If it causes problems somewhere
; else, then should put them back in:
;
;               IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
;                 uos= os_arr(UNIQ_NOSORT(os_arr.os_num)).os_num      
;                 dos= WHERE(all_defined_os_arr.os_num EQ uos(0)) 
;                 FOR i=1, N_ELEMENTS(uos)-1 DO dos= [dos,WHERE(all_defined_os_arr.os_num EQ uos(i))] 
;                 defined_os_arr= all_defined_os_arr(dos)
;               ENDIF ELSE defined_os_arr= 0


               IF (DATATYPE(all_defined_set_arr) NE 'STC') THEN BEGIN
                 IF (DATATYPE(defined_set_arr) EQ 'STC') THEN all_defined_set_arr= defined_set_arr
               ENDIF ELSE BEGIN
                 IF (DATATYPE(defined_set_arr) EQ 'STC') THEN BEGIN
                   all_defined_set_arr= [all_defined_set_arr,defined_set_arr]
                   all_defined_set_arr= all_defined_set_arr(UNIQ_NOSORT(all_defined_set_arr.set_id))
                 ENDIF
               ENDELSE
               IF (DATATYPE(os_arr) EQ 'STC') THEN BEGIN
                 setind= WHERE(os_arr.set_id GT 0, scnt)
                 IF (scnt GT 0) THEN BEGIN
                   uset= os_arr(setind).set_id
                   uset= uset(UNIQ_NOSORT(uset))
                   dos= WHERE(all_defined_set_arr.set_id EQ uset(0))
                   FOR i=1, N_ELEMENTS(uset)-1 DO dos= [dos,WHERE(all_defined_set_arr.set_id EQ uset(i))]
                   defined_set_arr= all_defined_set_arr(dos)
                 ENDIF ELSE defined_set_arr= 0
               ENDIF ELSE defined_set_arr= 0

               IF (DATATYPE(all_sched_cmdseq) NE 'STC') THEN BEGIN
                 IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN all_sched_cmdseq= sched_cmdseq
               ENDIF ELSE BEGIN
                 IF (DATATYPE(sched_cmdseq) EQ 'STC') THEN BEGIN
                   all_sched_cmdseq= [all_sched_cmdseq,sched_cmdseq]
                   all_sched_cmdseq= all_sched_cmdseq(UNIQ_NOSORT(all_sched_cmdseq.sc+ $
                                                                  all_sched_cmdseq.bsf+ $
                                                                  all_sched_cmdseq.start_time))
                 ENDIF
               ENDELSE
               IF (DATATYPE(all_sched_cmdseq) EQ 'STC') THEN BEGIN
                 sched_cmdseq= all_sched_cmdseq
                 IF (schedv.sc EQ 1) THEN BEGIN
                   sched_cmdseq= 0
                   aind= WHERE(all_sched_cmdseq.sc EQ 'A', acnt)
                   IF (acnt GT 0) THEN sched_cmdseq= all_sched_cmdseq(aind)
                 ENDIF
                 IF (schedv.sc EQ 2) THEN BEGIN
                   sched_cmdseq= 0
                   bind= WHERE(all_sched_cmdseq.sc EQ 'B', bcnt)
                   IF (bcnt GT 0) THEN sched_cmdseq= all_sched_cmdseq(bind)
                 ENDIF
               ENDIF ELSE sched_cmdseq= 0

  
;help,os_arr,remain_os_arr,defined_os_arr,all_defined_os_arr, defined_set_arr,all_defined_set_arr
;help,all_sched_cmdseq,sched_cmdseq
;stop

               WIDGET_CONTROL, /HOUR
               SCHEDULE_PLOT, schedv


            END

    ELSE : BEGIN
             WIDGET_CONTROL,mdiag,SET_VALUE='Invalid widget event in SCHEDULE_EVENT, '+STRN(input)
             PRINT,'Invalid widget event in SCHEDULE_EVENT, ', input
           END
 ENDCASE


; 
; Any changes to the CDS structure in this pass must be resaved into
; the UVALUE, or ELSE they will be lost at the following program RETURN
; to the XMANAGER.
;

  WIDGET_CONTROL, event.top, SET_UVALUE=schedv	


RETURN
END

