;+
;$Id: ptdate2utc.pro,v 1.1.1.2 2004/07/01 21:19:07 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : PTDATE2UTC
;               
; Purpose     : Takes string of dates in the format required for the
;               planning tool output and returns an array of UTC date structures.
;               
; Use         : utc_dt = PTDATE2UTC(date_str)
;    
; Inputs      : date_str  String of dates (i.e. '19951020_000000,19951020_003000'). 
;
; Opt. Inputs : None
;               
; Outputs     : utc_dt   date_str converted to UTC format.
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: ptdate2utc.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:07  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION PTDATE2UTC, date_str

   str = STRTRIM(date_str, 2)
   IF ( STRMID(str,0,1) EQ '(' ) THEN $
      str = STRMID(date_str, 1, STRLEN(date_str)-2)	;** clip off "(" and ")"
   d_arr = STR2ARR(str, DELIM=",")
   d_arr = STRTRIM(d_arr, 2)

   d = STRMID(d_arr,0,4)+'/'+STRMID(d_arr,4,2)+'/'+STRMID(d_arr,6,2)+' '+STRMID(d_arr,9,2)+ $
       ':'+STRMID(d_arr,11,2)+':'+STRMID(d_arr,13,2)

   utc = STR2UTC(d)
   RETURN, utc
END
