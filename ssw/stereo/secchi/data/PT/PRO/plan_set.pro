;+
;$Id: plan_set.pro,v 1.4 2005/01/24 17:56:34 esfand Exp $
; Project     : STEREO - SECCHI 
;                   
; Name        : PLAN_SET
;               
; Purpose     : Widget tool to define a SECCHI Observing Program.
;               
; Explanation : This tool allows the user to specify a specific SECCHI 
;               Observing SET .  This tool is also used to display
;		the details of a previously defined set.
;               
; Use         : PLAN_SET, caller, default_op
;    
; Inputs      : caller		Structure containing id of caller.
;               
; Opt. Inputs : default_op	(LONG) bring up details for this unique op_num
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/29/04 - allow keeping/removing imported sets. 
;              Ed Esfandiari 08/05/04 - skips 3 summary file label lines now.
;              Ed Esfandiari 10/19/04 - Added /SET keyword to call to schedule_read_ipt.
;                                       Also added S/C id to the page title. 
;
; $Log: plan_set.pro,v $
; Revision 1.4  2005/01/24 17:56:34  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:06  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


;________________________________________________________________________________________________________
;


PRO CLEAR_ALL,opv

  WIDGET_CONTROL, opv.curset, SET_VALUE=0
  WIDGET_CONTROL, opv.set_id, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.fname, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.instr, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.disp,SENSITIVE=1
  WIDGET_CONTROL, opv.sci_obj, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.req_by, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.imp_by, SET_VALUE='',SENSITIVE=0
  WIDGET_CONTROL, opv.comment, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.cr_date, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.sel_obs, SET_VALUE='',SENSITIVE=1
  WIDGET_CONTROL, opv.newset,SENSITIVE=1
  WIDGET_CONTROL, opv.oldset,SENSITIVE=1
  WIDGET_CONTROL, opv.curset,SENSITIVE=1
  WIDGET_CONTROL, opv.update,SENSITIVE=1
  WIDGET_CONTROL, opv.instr,/INPUT_FOCUS

  RETURN
END


FUNCTION MAKE_SET

COMMON OS_DEFINED   ; AEE 2/3/04
COMMON OS_SCHEDULED ; AEE 2/3/04
COMMON SCHED_SHARE, schedv 
COMMON DIALOG,mdiag,font

    IF (DATATYPE(os_arr) NE 'STC') THEN BEGIN
      PRINT,''
      PRINT,'Schedule Contains No Images.'
      PRINT,''
      RETURN,'Schedule Contains No Images.'
    END

    tos_arr= os_arr
    ;w= WHERE(tos_arr.set_id EQ 0, wcnt)
    w= WHERE(tos_arr.set_id EQ 0 AND tos_arr.bsf EQ '', wcnt)
    IF (wcnt EQ 0) THEN BEGIN
            PRINT,''
      PRINT,'All Scheduled OSes Are Used By Other Sets Or Belong To BSF.'
      PRINT,''
      RETURN,'***All Scheduled OSes Are Used By Other Sets Or Belong To BSF***'
    ENDIF ELSE BEGIN
      tos_arr= tos_arr(w) 
    ENDELSE

    oses= tos_arr.os_num
    oses= oses(SORT([oses]))
    ;oses= STRTRIM(oses(UNIQ(oses)),2)
    oses= STRING(oses(UNIQ(oses)),'(I4.4)')
    oses= REVERSE(oses)
    result = PICKFILES2(FILES=oses, XSIZE=320, TITLE="Select OS Numbers For Making a Set") 
    ;op_sel= {os_num:0L, instances:0, picked:''}

    IF (result(0) NE '') THEN BEGIN
      n_os= N_ELEMENTS(result)
      op_sel= {os_num:0L, date:''}
      op_sel= REPLICATE(op_sel,n_os)
      os_sel= INTARR(n_os)
      ;help,result
      FOR i=0, n_os-1 DO BEGIN
        op_sel(i).os_num= LONG(result(i))
        w= WHERE(tos_arr.os_num EQ result(i), wcnt)
 
        IF (i EQ 0) THEN $
          instances= 'OS_'+result(i)+' @ '+STRMID(UTC2STR(TAI2UTC(tos_arr(w).os_start)),0,19) $
        ELSE $
          instances= [instances,'OS_'+result(i)+' @ '+STRMID(UTC2STR(TAI2UTC(tos_arr(w).os_start)),0,19)]
      ENDFOR
      os_title="Make a Set by Selecting From The Listed Images"
      res= PICKFILES2(FILES=instances, XSIZE=335, TITLE=os_title)

      IF (res(0) NE '') THEN BEGIN 
        osids= LONG(STRMID(res,3,4)) ; pickup OS-num part of the selected images.
        ostms= STRMID(res,10,19) ; pickup date (yyyy-mm-ddThh:mm:ss) parts of selected images.
        FOR i= 0, n_os-1 DO BEGIN
          w= WHERE(osids EQ op_sel(i).os_num, wcnt)
          FOR j=0, wcnt-1 DO op_sel(i).date= op_sel(i).date+' '+ostms(w(j))
        END
        op_sel.date= STRMID(op_sel.date,1,20000)  ; remove the first character (a blank)
        op_sel= op_sel(WHERE(op_sel.date NE '')) ; remove OSes not selected from
      ENDIF ELSE op_sel= 'Did Not Create a Set.' 
    ENDIF
 
    IF (DATATYPE(op_sel) EQ 'STC') THEN BEGIN
      PRINT,''
      PRINT,'Selected Set='
      FOR i=0, N_ELEMENTS(op_sel)-1 DO PRINT,'   '+STRTRIM(op_sel(i).os_num,2)+' @ '+op_sel(i).date
      PRINT,''
    ENDIF ELSE BEGIN
      PRINT,''
      PRINT,'Did Not Create a Set.'
      PRINT,''
      RETURN,'***Did Not Create a Set***'
    ENDELSE

  RETURN,op_sel

END

 
PRO PLAN_SET_EVENT, event

COMMON SCHEDULE_BASE, sched_base

COMMON PLAN_OP_SHARE, opv

COMMON OBS_PROG_DEF, op_id_struct, op_id_arr
COMMON OP_DEFINED, op_instance, defined_op_arr

COMMON OS_DEFINED   ; AEE 2/3/04
COMMON OS_SCHEDULED ; AEE 2/3/04
COMMON SCHED_SHARE, schedv ; AEE 2/5/04
COMMON SET_DEFINED, set_instance, defined_set_arr ; AEE 2/6/04
COMMON SETS, select_set ; AEE 2/6/04
COMMON DIALOG,mdiag,font

CASE (event.id) OF


   opv.dismiss : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, opv.base
   END

   opv.help: BEGIN
         filename= GETENV('PT')+'/IN/OTHER/'+'help_set.dat'
         XDISPLAYFILE,filename,TITLE='Help Menu',$
                GROUP=event.top, HEIGHT=20, WIDTH=100
   END

   opv.set_id : BEGIN
;      WIDGET_CONTROL, opv.set_id, GET_VALUE=set_id
;      set_id = FIX(set_id)
;      WIDGET_CONTROL, opv.set_id, SET_VALUE=STRTRIM(set_id,2)
;      WIDGET_CONTROL, opv.fname, SET_VALUE= "SET_"+STRTRIM(set_id,2)+".IPT"
   END

   opv.fname : BEGIN
   END

   opv.disp : BEGIN
         IF (DATATYPE(defined_set_arr) EQ 'STC') THEN BEGIN
           info= VIEW_SETS_SUMMARY(os_arr,defined_set_arr)
           XDISPLAYFILE,TEXT=info,TITLE='Currently Scheduled Sets', $
                  GROUP=event.top, HEIGHT=20, WIDTH=105
         ENDIF
   END


   opv.instr : BEGIN
      WIDGET_CONTROL, opv.instr, GET_VALUE=inst
      WIDGET_CONTROL, opv.instr, SET_VALUE= STRTRIM(inst(0),2)
      WIDGET_CONTROL, opv.sci_obj, /INPUT_FOCUS
      WIDGET_CONTROL, opv.sci_obj, GET_VALUE=tmp
      WIDGET_CONTROL, opv.sci_obj, SET_TEXT_SELECT=[STRLEN(STRTRIM(tmp,2))]
   END

   opv.sci_obj : BEGIN
      WIDGET_CONTROL, opv.sci_obj, GET_VALUE=sci_obj
      WIDGET_CONTROL, opv.sci_obj, SET_VALUE= STRTRIM(sci_obj(0),2)
      WIDGET_CONTROL, opv.comment, /INPUT_FOCUS
      WIDGET_CONTROL, opv.comment, GET_VALUE=tmp
      WIDGET_CONTROL, opv.comment, SET_TEXT_SELECT=[STRLEN(STRTRIM(tmp,2))]
   END

   opv.comment : BEGIN
      WIDGET_CONTROL, opv.comment, GET_VALUE=comment
      WIDGET_CONTROL, opv.comment, SET_VALUE= STRTRIM(comment(0),2)
      WIDGET_CONTROL, opv.req_by, /INPUT_FOCUS
      WIDGET_CONTROL, opv.req_by, GET_VALUE=tmp
      WIDGET_CONTROL, opv.req_by, SET_TEXT_SELECT=[STRLEN(STRTRIM(tmp,2))]
   END

   opv.req_by : BEGIN
      WIDGET_CONTROL, opv.req_by, GET_VALUE=requester
      WIDGET_CONTROL, opv.req_by, SET_VALUE= STRTRIM(requester(0),2)
      WIDGET_CONTROL, opv.imp_by, /INPUT_FOCUS
      WIDGET_CONTROL, opv.imp_by, GET_VALUE=tmp
      WIDGET_CONTROL, opv.imp_by, SET_TEXT_SELECT=[STRLEN(STRTRIM(tmp,2))]
   END

   opv.imp_by : BEGIN
      WIDGET_CONTROL, opv.imp_by, GET_VALUE=importer
      WIDGET_CONTROL, opv.imp_by, SET_VALUE= STRTRIM(importer(0),2)
   END


   opv.clear : BEGIN	;** clear all inputs
      CLEAR_ALL,opv
      WIDGET_CONTROL, opv.msg, SET_VALUE= opv.init_msg 
   END

   opv.sel_obs : BEGIN ; AEE 2/9/04
   END


   opv.curset : BEGIN ; AEE 2/9/04
     opv.last_flag = 'NA' 

     WIDGET_CONTROL, opv.curset, GET_VALUE= index & index= index(0)

     IF (index EQ 0 OR opv.cur_sets(index) EQ 'Removed') THEN BEGIN
       WIDGET_CONTROL, opv.set_id, SET_VALUE=''
       WIDGET_CONTROL, opv.fname, SET_VALUE=''
       WIDGET_CONTROL, opv.instr, SET_VALUE=''
       WIDGET_CONTROL, opv.sci_obj, SET_VALUE=''
       WIDGET_CONTROL, opv.req_by, SET_VALUE=''
       WIDGET_CONTROL, opv.imp_by, SET_VALUE='',SENSITIVE=0
       WIDGET_CONTROL, opv.comment, SET_VALUE=''
       WIDGET_CONTROL, opv.cr_date, SET_VALUE=''
       WIDGET_CONTROL, opv.sel_obs, SET_VALUE=''
       RETURN
     ENDIF
     
     opv.last_flag = 'SCH' 

     index= index-1 ; since index=0 (for Pick a Set) does not have a entry in defined_os_arr.
     WIDGET_CONTROL, opv.set_id, SET_VALUE= STRING(defined_set_arr(index).set_id,'(i4.4)'),SENSITIVE=0
     WIDGET_CONTROL, opv.instr, SET_VALUE= defined_set_arr(index).instr,SENSITIVE=0
     WIDGET_CONTROL, opv.sci_obj, SET_VALUE= defined_set_arr(index).sci_obj,SENSITIVE=0
     WIDGET_CONTROL, opv.req_by, SET_VALUE= defined_set_arr(index).req_by,SENSITIVE=0
     WIDGET_CONTROL, opv.imp_by, SET_VALUE= defined_set_arr(index).imp_by,SENSITIVE=0
     WIDGET_CONTROL, opv.comment, SET_VALUE= defined_set_arr(index).comment,SENSITIVE=0
     WIDGET_CONTROL, opv.cr_date, SET_VALUE= defined_set_arr(index).cr_date,SENSITIVE=0
     WIDGET_CONTROL, opv.fname, SET_VALUE= defined_set_arr(index).fname,SENSITIVE=0

     setid= defined_set_arr(index).set_id
     setcnt= defined_set_arr(index).set_cnt
     w= WHERE(os_arr.set_id EQ setid AND os_arr.set_cnt EQ setcnt, wcnt)
     IF (wcnt GT 0) THEN BEGIN
       this_set= os_arr(w)
       info= OS_SUMMARY_SET(this_set) 
       ;info= info(2:*) ; remove labels
       info= info(3:*) ; remove labels (3 lines)
       WIDGET_CONTROL, opv.sel_obs, SET_VALUE= info,SENSITIVE=0
       inst_used= STRTRIM(STRMID(info,19,7),2)
       WIDGET_CONTROL, opv.instr, SET_VALUE= inst_used(UNIQ_NOSORT(inst_used))+' ',SENSITIVE=0
     ENDIF
     WIDGET_CONTROL, opv.update, SENSITIVE=0
     WIDGET_CONTROL, opv.delete, SENSITIVE=1
     WIDGET_CONTROL, opv.clear , SENSITIVE=1

   END

   opv.newset : BEGIN ; AEE 2/5/04
      WIDGET_CONTROL, opv.update, SENSITIVE=1
      WIDGET_CONTROL, opv.imp_by, SENSITIVE=0
      select_set= MAKE_SET()
      IF (DATATYPE(select_set) NE 'STC') THEN BEGIN
        WIDGET_CONTROL, opv.msg, SET_VALUE= select_set
      ENDIF ELSE BEGIN
        CLEAR_ALL,opv
        WIDGET_CONTROL, opv.msg, SET_VALUE= '***Provide All Needed Info Before Inserting Into Plan***' 
        RESTORE,GETENV('PT')+'/IO/SETS/'+'set_last_id.sav'   ; -> last_id
        IF (DATATYPE(defined_set_arr) EQ 'STC') THEN BEGIN
          IF (MAX(defined_set_arr.set_id) GT last_id) THEN last_id= MAX(defined_set_arr.set_id)
        ENDIF
        last_id= last_id+1

        WIDGET_CONTROL, opv.set_id, SET_VALUE= STRING(last_id,'(i4.4)')
        WIDGET_CONTROL, opv.fname, SET_VALUE= "SET_"+STRING(last_id,'(i4.4)')+".IPT"
        GET_UTC, stime, /ECS                    ; get system time
        WIDGET_CONTROL, opv.cr_date, SET_VALUE=STRMID(stime,0,19) ; remove milliseconds

        ; Mark instances of selected OS_NUMs in a temp os_arr with this set_id:

        tmp_os_arr= os_arr 
        FOR i=0, N_ELEMENTS(select_set)-1 DO BEGIN
          osnum= select_set(i).os_num
          stimes= STR_SEP(select_set(i).date,' ')
          ;dtimes= UTC2TAI(STR2UTC(stimes)) 
          os_starts= STRMID(UTC2STR(TAI2UTC(tmp_os_arr.os_start)),0,19) ; remove .sss
          FOR j=0, N_ELEMENTS(stimes)-1 DO BEGIN
            ;w= WHERE(tmp_os_arr.os_num EQ osnum AND tmp_os_arr.os_start EQ dtimes(j), wcnt)
            w= WHERE(tmp_os_arr.os_num EQ osnum AND os_starts EQ stimes(j), wcnt)
            IF (wcnt GT 0) THEN BEGIN
              IF (tmp_os_arr(w(0)).set_id EQ 0) THEN BEGIN
                tmp_os_arr(w(0)).set_id= last_id 
              ENDIF ELSE BEGIN
                PRINT,''
                PRINT,'WARNING: OS_'+STRING(osnum,'(i4.4)')+' @ '+stimes(j)+' is already used by another set.'
                PRINT,''
                WIDGET_CONTROL, opv.msg, SET_VALUE=  $
                  '***WARNING: OS_'+STRING(osnum,'(i4.4)')+' @ '+stimes(j)+' is already used by another set***'
                CLEAR_ALL,opv
                RETURN
              ENDELSE
            ENDIF ELSE BEGIN
              PRINT,''
              PRINT,'ERROR: No matching image for set image (OS_'+STRING(osnum,'(i4.4)')+' @ '+ $
                     stimes(j)+') in TMP_OS_ARR.'
              PRINT,''
                WIDGET_CONTROL, opv.msg, SET_VALUE= $
                   '***ERROR: No matching image for set image (OS_'+STRING(osnum,'(i4.4)')+' @ '+stimes(j)+') in TMP_OS_ARR***' 
 
              STOP
            ENDELSE
          ENDFOR
        ENDFOR

        w= WHERE(tmp_os_arr.set_id EQ last_id AND tmp_os_arr.set_cnt EQ 0, wcnt)
        IF (wcnt GT 0) THEN BEGIN
          info= OS_SUMMARY_SET(tmp_os_arr(w)) 
          ;info= info(2:*) ; remove labels
          info= info(3:*) ; remove labels (3 lines)
          WIDGET_CONTROL, opv.sel_obs, SET_VALUE= info,SENSITIVE=0
          inst_used= STRTRIM(STRMID(info,19,7),2)
          WIDGET_CONTROL, opv.instr, SET_VALUE= inst_used(UNIQ_NOSORT(inst_used))+' ',SENSITIVE=1
        ENDIF

        WIDGET_CONTROL, opv.newset,SENSITIVE=0
        WIDGET_CONTROL, opv.oldset,SENSITIVE=0
        WIDGET_CONTROL, opv.curset,SENSITIVE=0
        WIDGET_CONTROL, opv.clear, SENSITIVE=1
        WIDGET_CONTROL, opv.update,SENSITIVE=1
        WIDGET_CONTROL, opv.delete,SENSITIVE=0
        WIDGET_CONTROL, opv.imp_by, SET_VALUE= 'not needed.',SENSITIVE=0
        WIDGET_CONTROL, opv.sci_obj, SET_VALUE= '',SENSITIVE=1,/INPUT_FOCUS
        opv.last_flag = 'NEW' 
      ENDELSE
   END

   opv.oldset : BEGIN ; AEE 2/5/04
     sets= FINDFILE(GETENV('PT')+"/IO/SETS/SET*.IPT")
     sets= STRMID(sets,STRPOS(sets(0),'SET_'),50) ; remove path from all filenames
     IF (sets(0) NE '') THEN BEGIN 
       result = PICKFILES2(FILES=sets, XSIZE=170, TITLE="Select a Set", /EXCLUSIVE)
       result = result(0)
       IF (result NE '') THEN BEGIN
         ;help,result
         info= FINDFILE(GETENV('PT')+"/IO/SETS/"+STRMID(result,0,8)+".INFO")
         IF (info(0) EQ '') THEN BEGIN
           PRINT,"Warning: Could not find "+info(0)+" file."
           STOP
         ENDIF ELSE BEGIN
           OPENR, inlun, info(0), /GET_LUN
           str=''
          str_arr= ''
          WHILE (NOT(EOF(inlun))) DO BEGIN
            READF, inlun, str
            str_arr= [str_arr,STRTRIM(str,2)]
          ENDWHILE
          CLOSE, inlun
          FREE_LUN, inlun
          str_arr= str_arr(1:*)
          key_val_arr = STR_ARR2KEY_VAL(str_arr)
          WIDGET_CONTROL, opv.instr,   SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'INSTR')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.sci_obj, SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'SCI_OBJ')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.comment, SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'COMMENT')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.req_by,  SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'REQ_BY')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.imp_by,  SET_VALUE= '',SENSITIVE=1,/INPUT_FOCUS
          WIDGET_CONTROL, opv.cr_date, SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'CREATED')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.set_id,  SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'SET_ID')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.fname,   SET_VALUE= key_val_arr(WHERE(key_val_arr.key EQ 'FNAME')).val,SENSITIVE=0
          WIDGET_CONTROL, opv.newset, SENSITIVE=0
          imgs= WHERE(key_val_arr.key EQ 'IMAGE', ncnt)
          IF (ncnt GT 0) THEN WIDGET_CONTROL, opv.sel_obs, SET_VALUE= key_val_arr(imgs).val,SENSITIVE=0
          opv.last_flag= 'OLD'

          lab='            Date Obs             Tele     Exp   Nx     Ny   CCD  SEB    Filter    Polar  SEB Program   OS_NUM  (Polar2,Exp2)'
          WIDGET_CONTROL, opv.newset,SENSITIVE=0
          WIDGET_CONTROL, opv.oldset,SENSITIVE=0
          WIDGET_CONTROL, opv.curset,SENSITIVE=0
          WIDGET_CONTROL, opv.update,SENSITIVE=1
          WIDGET_CONTROL, opv.delete,SENSITIVE=0
          WIDGET_CONTROL, opv.clear, SENSITIVE=1
          WIDGET_CONTROL, opv.imp_by,/INPUT_FOCUS
          PRINT,'Provide "Imported For" Info Before Inserting Into Plan.'
          WIDGET_CONTROL, opv.msg, SET_VALUE= '***Provide "Imported For" Info Before Inserting Into Plan***'

         ENDELSE
       ENDIF
     ENDIF 
   END

   opv.delete : BEGIN 
       WIDGET_CONTROL, opv.set_id,  GET_VALUE= setid   & setid= LONG(setid(0))
       WIDGET_CONTROL, opv.fname, GET_VALUE= fn & fn= fn(0)

       WIDGET_CONTROL, opv.curset, GET_VALUE= index & index= index(0)
       IF (index EQ 0) THEN RETURN 

       index= index-1 ; since index=0 (Pick a Set) does not have a entry in defined_os_arr.

       setcnt= defined_set_arr(index).set_cnt

       old_dsa= defined_set_arr
       w= WHERE(defined_set_arr.set_id NE setid OR defined_set_arr.set_cnt NE setcnt, wcnt)
       IF (wcnt GT 0) THEN $
         defined_set_arr= defined_set_arr(w) $
       ELSE $ 
         defined_set_arr= ''

       WIDGET_CONTROL, /DESTROY, opv.base
      
       RESTORE,GETENV('PT')+'/IO/SETS/'+'set_last_id.sav'   ; -> last_id

       IF (setid GT last_id) THEN BEGIN ; removing a new set (no imported .IPT)
         w = WHERE(os_arr.set_id EQ setid AND os_arr.set_cnt EQ setcnt, wcnt)
         IF (wcnt GT 0) THEN BEGIN
           os_arr(w).set_id= 0 
           os_arr(w).set_cnt= 0
         ENDIF
       ENDIF ELSE BEGIN                 ; removing an imported set
         quit = RESPOND_WIDG(/COLUMN, $
                mess= ['Remove all images with set_id='+STRTRIM(setid,2)+' from schedule??'], $
                butt=['Yes (remove images)','No (keep images but not as a set)','Cancel Command'],$
                group=schedv.base)
         IF (quit EQ 2) THEN BEGIN
            defined_set_arr= old_dsa
            WIDGET_CONTROL,mdiag,SET_VALUE='Set Commnad Canceled.'
            RETURN
         ENDIF
         IF (quit EQ 1) THEN BEGIN
           w = WHERE(os_arr.set_id EQ setid AND os_arr.set_cnt EQ setcnt, wcnt)
           IF (wcnt GT 0) THEN BEGIN
             os_arr(w).set_id= 0
             os_arr(w).set_cnt= 0
           ENDIF
           WIDGET_CONTROL,mdiag,SET_VALUE='De-set images with set_id='+STRTRIM(setid,2)
           RETURN
         ENDIF
         ; So, quit=0, Go ahead and remove the images.
         WIDGET_CONTROL,mdiag,SET_VALUE='Removed all set_id='+STRTRIM(setid,2)+' images.'
         osnum= [0]
         w= WHERE(os_arr.set_id EQ setid AND os_arr.set_cnt EQ setcnt, wcnt)
         IF (wcnt GT 0) THEN osnum= os_arr(w).os_num
         osnum= osnum(UNIQ_NOSORT(osnum))

         w= WHERE(os_arr.set_id NE setid OR os_arr.set_cnt NE setcnt, wcnt)
         IF (wcnt GT 0) THEN BEGIN 
           os_arr= os_arr(w) 
           FOR i=0, N_ELEMENTS(osnum)-1 DO BEGIN
             w= WHERE(os_arr.os_num EQ osnum(i), wcnt)
             IF (wcnt EQ 0) THEN BEGIN ;remove defined_os_arr entry only if all instances of a os_num are removed.
               w= WHERE(defined_os_arr.os_num NE osnum(i), wcnt)
               IF (wcnt GT 0) THEN defined_os_arr= defined_os_arr(w)
             ENDIF
           ENDFOR
         ENDIF ELSE BEGIN               
           os_arr= 0 
           w= WHERE(defined_os_arr.os_num LE 2, wcnt)
           IF (wcnt GT 0) THEN $
             defined_os_arr= defined_os_arr(w) $
           ELSE $
             defined_os_arr= 0 
         ENDELSE

         SCHEDULE_PLOT, schedv ; update the plot

       ENDELSE

  END

   opv.update : BEGIN

      IF (opv.last_flag EQ '') THEN BEGIN
        PRINT,'No Set Created or Imported.'
        WIDGET_CONTROL, opv.msg, SET_VALUE= '***No Set Created or Imported***'
        RETURN 
      ENDIF

      IF (opv.last_flag EQ 'NA') THEN RETURN

      IF (opv.last_flag EQ 'SCH') THEN BEGIN
        PRINT,'This Set Is Already Scheduled.'
        WIDGET_CONTROL, opv.msg, SET_VALUE= '***This Set Is Already Scheduled***'
        RETURN
      END

      WIDGET_CONTROL, opv.instr,   GET_VALUE= instr & instr= STRTRIM(instr(0),2)
      WIDGET_CONTROL, opv.sci_obj, GET_VALUE= sci_obj & sci_obj= STRTRIM(sci_obj(0),2)
      WIDGET_CONTROL, opv.comment, GET_VALUE= comment & comment= STRTRIM(comment(0),2)
      WIDGET_CONTROL, opv.req_by,  GET_VALUE= reqby   & reqby= STRTRIM(reqby(0),2)
      WIDGET_CONTROL, opv.imp_by,  GET_VALUE= impby   & impby= STRTRIM(impby(0),2)

      IF (opv.last_flag EQ 'OLD' AND impby EQ '') THEN BEGIN
        WIDGET_CONTROL, opv.imp_by, /INPUT_FOCUS
        PRINT,''
        PRINT,'Please Provide Import (Imported For) Info.'
        PRINT,''
        WIDGET_CONTROL, opv.msg, SET_VALUE= '***Please Provide Import (Imported For) Info***'
        RETURN
      ENDIF

      IF (opv.last_flag EQ 'NEW') THEN BEGIN
        IF (instr EQ '') THEN BEGIN
          WIDGET_CONTROL, opv.instr, /INPUT_FOCUS
          PRINT,''
          PRINT,'Please Provide Instrument Info.'
          PRINT,''
          WIDGET_CONTROL, opv.msg, SET_VALUE= '***Please Provide Instrument (EUVI, COR1, COR2, HI1, HI2) Info***'
          RETURN
        ENDIF
        IF (sci_obj EQ '') THEN BEGIN
          WIDGET_CONTROL, opv.sci_obj, /INPUT_FOCUS
          PRINT,''
          PRINT,'Please Provide Science Objective Info.'
          PRINT,''
          WIDGET_CONTROL, opv.msg, SET_VALUE= '***Please Provide Science Objective Info***'
          RETURN
        ENDIF
        IF (reqby EQ '') THEN BEGIN
          WIDGET_CONTROL, opv.req_by, /INPUT_FOCUS
          PRINT,''
          PRINT,'Please Provide Requester (Created For) Info.'
          PRINT,''
          WIDGET_CONTROL, opv.msg, SET_VALUE= '***Please Provide Requester (Created For) Info***'
          RETURN
        ENDIF
      ENDIF

      WIDGET_CONTROL, opv.cr_date, GET_VALUE= created & created= created(0)
      WIDGET_CONTROL, opv.set_id,  GET_VALUE= setid   & setid= setid(0)
      WIDGET_CONTROL, opv.fname,   GET_VALUE= fname   & fname= fname(0)

      ; Add the new or imported set to the defined_set_arr:

        new_set = {set_instance,set_id:0L,set_cnt:0,fname:'',instr:'',sci_obj:'',comment:'',req_by:'', $
                   imp_by:'', cr_date:''}
        new_set.set_id= setid
        new_set.fname= fname
        new_set.instr= instr
        new_set.sci_obj= sci_obj
        new_set.comment= comment
        new_set.req_by= reqby
        new_set.imp_by= impby
        new_set.cr_date= created

        WIDGET_CONTROL, /DESTROY, opv.base

        IF (DATATYPE(defined_set_arr) NE 'STC') THEN BEGIN 
          new_set.set_cnt= 1
          defined_set_arr= new_set 
        ENDIF ELSE BEGIN 
          new_set.set_cnt= MAX(defined_set_arr.set_cnt)+1
          defined_set_arr= [defined_set_arr, new_set]
        ENDELSE

      IF (opv.last_flag EQ 'OLD') THEN BEGIN
        WIDGET_CONTROL, /HOUR
        SCHEDULE_READ_IPT, GETENV('PT')+'/IO/SETS/'+fname, schedv.startdis, /SILENT, /PROMPT_SHIFT,/SET
        SCHEDULE_PLOT, schedv
        WIDGET_CONTROL,mdiag,SET_VALUE='Imported Set '+STRTRIM(setid,2)
      ENDIF 
   
 
      IF (opv.last_flag EQ 'NEW') THEN BEGIN
     

        ; Mark instances of selected OS_NUMs in os_arr with this set_id:
 
        FOR i=0, N_ELEMENTS(select_set)-1 DO BEGIN
          osnum= select_set(i).os_num
          stimes= STR_SEP(select_set(i).date,' ')
          ;dtimes= UTC2TAI(STR2UTC(stimes)) 
          os_starts= STRMID(UTC2STR(TAI2UTC(os_arr.os_start)),0,19) ; remove .sss
          FOR j=0, N_ELEMENTS(stimes)-1 DO BEGIN
            ;w= WHERE(os_arr.os_num EQ osnum AND os_arr.os_start EQ dtimes(j), wcnt)
            w= WHERE(os_arr.os_num EQ osnum AND os_starts EQ stimes(j), wcnt)
            IF (wcnt GT 0) THEN BEGIN
              IF (os_arr(w(0)).set_id EQ 0) THEN BEGIN
                os_arr(w(0)).set_id= LONG(setid) 
              ENDIF ELSE BEGIN
                PRINT,''
                PRINT,'WARNING: OS_'+STRING(osnum,'(i4.4)')+' @ '+stimes(j)+' already belongs to set_id '+setid+'.'
                PRINT,''
                STOP      
              ENDELSE
            ENDIF ELSE BEGIN
              PRINT,''
              PRINT,'ERROR: No matching image for set image (OS_'+STRING(osnum,'(i4.4)')+' @ '+ $
                     stimes(j)+') in OS_ARR.'
              PRINT,''
              STOP
            ENDELSE
          ENDFOR
        ENDFOR
      
        WIDGET_CONTROL,mdiag,SET_VALUE='Generated Set '+STRTRIM(setid,2)

      ENDIF

      ; Now add the set_cnt to the instances of os_arr with the newly created or imported setid:

      w= WHERE(os_arr.set_id EQ setid AND os_arr.set_cnt EQ 0, wcnt)
      IF (wcnt GT 0) THEN os_arr(w).set_cnt= new_set.set_cnt

   END

   ELSE : BEGIN
   END

ENDCASE

END

;________________________________________________________________________________________________________
;


PRO PLAN_SET, caller, default_op

COMMON PLAN_OP_SHARE
COMMON OBS_PROG_DEF, op_id_struct, op_id_arr
COMMON OS_DEFINED   ; AEE 2/3/04
COMMON OS_SCHEDULED ; AEE 2/3/04
COMMON SCHED_SHARE, schedv ; AEE 2/3/04
COMMON SET_DEFINED, set_instance, defined_set_arr ; AEE 2/6/04
COMMON SETS, select_set ; AEE 2/6/04
COMMON DIALOG,mdiag,font


select_set=''

    IF XRegistered("PLAN_SET") THEN GOTO, CONT

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

    IF (schedv.sc EQ 1) THEN sc= ' (SpaceCraft A)'
    IF (schedv.sc EQ 2) THEN sc= ' (SpaceCraft B)'
    base = WIDGET_BASE(/COLUMN, TITLE='DEFINE OBSERVING SET'+sc, /FRAME, $
             GROUP_LEADER=caller.id,XSIZE=650)

    row = WIDGET_BASE(base, /ROW)
    tmp = WIDGET_LABEL(row, VALUE='                                                                               ')
    help= WIDGET_BUTTON(row, VALUE='Help', UVALUE='HELP')

    row = WIDGET_BASE(base, /ROW)
      newset = WIDGET_BUTTON(row, VALUE='Create a New Set')
      tmp = WIDGET_LABEL(row, VALUE='            ')
      oldset = WIDGET_BUTTON(row, VALUE='Import a Set')

      IF (DATATYPE(defined_set_arr) EQ 'STC') THEN BEGIN 
        ; Check to see if all images in a set are deleted from a schedule outside of the plan_set via 
        ; the "DELETE" or "DEL MULT OS's" buttons. If all of the images of a set are deleted, then
        ; remove that set from the plan_set to avoid problems. This section of code, however, is
        ; executed only if a new session of plan_set is started so if some of the images were deleted
        ; this way and the plan_set window is still open, dismiss and start it again to clean up the
        ; defined_set_arr.
        ; Note: If an existing set was imported and some of the images were deleted, then the saved
        ;       ITOS and summary.SETS file would only include the remaining images of the set.

        n= N_ELEMENTS(defined_set_arr)
        keep=INDGEN(n) 
        FOR i=0, n-1 DO BEGIN
         w= WHERE(os_arr.set_id EQ defined_set_arr(i).set_id AND os_arr.set_cnt EQ defined_set_arr(i).set_cnt,cnt)
         IF (cnt EQ 0) THEN keep(i)= -1 
        ENDFOR
        ok= WHERE(keep GE 0, ok_cnt)
        IF (ok_cnt GT 0) THEN BEGIN
          defined_set_arr= defined_set_arr(ok)
          cur_sets= ['Pick a Set',defined_set_arr.fname] 
        ENDIF ELSE BEGIN
          defined_set_arr= ''
          cur_sets= ['None']
        ENDELSE
      ENDIF ELSE BEGIN
        cur_sets= ['None']
      ENDELSE
      tmp = WIDGET_LABEL(row, VALUE='            Currently Scheduled:')
      curset = CW_BSELECTOR2(row,cur_sets, SET_VALUE=0)

    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='IMAGES FROM ')
      instr = WIDGET_TEXT(row, VALUE='', XSIZE=22, YSIZE=1, /EDITABLE, FONT=font)
      tmp = WIDGET_LABEL(row, VALUE='                       ')
      disp= WIDGET_BUTTON(row, VALUE='Display Scheduled Sets') 
    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='SCIENCE OBJECTIVE ')
      sci_obj = WIDGET_TEXT(row, VALUE='', XSIZE=50, YSIZE=1, /EDITABLE, FONT=font)
    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='COMMENT ')
      comment = WIDGET_TEXT(row, VALUE='', XSIZE=50, YSIZE=1, /EDITABLE, FONT=font)
    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='CREATED FOR   ')
      req_by = WIDGET_TEXT(row, VALUE='', XSIZE=15, YSIZE=1, /EDITABLE, FONT=font)
      tmp = WIDGET_LABEL(row, VALUE=' (1 word, letters only)')
    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='IMPORTED FOR ')
      imp_by = WIDGET_TEXT(row, VALUE='', XSIZE=15, YSIZE=1, /EDITABLE, FONT=font)
      tmp = WIDGET_LABEL(row, VALUE=' (1 word, letters only)')
    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='CREATION DATE ')
      cr_date = WIDGET_TEXT(row, VALUE='', XSIZE=20, YSIZE=1, FONT=font)
    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='SET ID ')
      set_id = WIDGET_TEXT(row, VALUE='', XSIZE=5, YSIZE=1, FONT=font) 
      tmp = WIDGET_LABEL(row, VALUE='           FILENAME ')
      fname = WIDGET_TEXT(row, VALUE='', XSIZE=13, YSIZE=1, FONT=font)

    row = WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(row, VALUE='                                                      Selected Observations')

    row = WIDGET_BASE(base, /ROW)
      lab='Date Obs                Tele     Exp   Nx  Ny   CCD SEB   Filter   Pol  SEB-Prog  OSnum  (Pol2,Exp2)'
      label = WIDGET_LABEL(row, VALUE=lab)
   
    col = WIDGET_BASE(base, /COL, /SCROLL, X_SCROLL_SIZE=80, Y_SCROLL_SIZE=100)
      bk='                                                                                                       '
      sel_obs= WIDGET_LIST(col, VALUE = [bk,bk,bk,bk,bk], XSIZE=60, YSIZE=100, FONT=font)

    init_msg= '***Select OSes To Be Marked As a Set (Use Create Or Import) Or Removed (Currently Scheduled)***'

    tmp     = WIDGET_LABEL(base,VALUE='')
    msg     = WIDGET_LABEL(base,FONT=font,VALUE=init_msg)
    tmp     = WIDGET_LABEL(base,VALUE='')
    update  = WIDGET_BUTTON(base, VALUE=' Insert Into Plan  ')
    delete  = WIDGET_BUTTON(base, VALUE=' Remove From Plan  ')
    clear   = WIDGET_BUTTON(base, VALUE='  Clear  ')
    dismiss = WIDGET_BUTTON(base, VALUE=' Dismiss ')

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, /REAL, base

    WIDGET_CONTROL, update, SENSITIVE=0
    WIDGET_CONTROL, delete, SENSITIVE=0
    WIDGET_CONTROL, clear , SENSITIVE=0
    WIDGET_CONTROL, imp_by, SENSITIVE=0

last_flag= ''

op_num=0
op_id= 0

    opv = CREATE_STRUCT( "base", base,			$
                         "help", help,              $
                         "set_id", set_id, 		$
                         "fname", fname,                $
                         "op_num",  op_num,  $
                         "op_id", op_id, 		$
                         "sci_obj", sci_obj, 		$
                         ;"name", name, 		$
                         "comment", comment, 		$
                         "cr_date", cr_date, 		$
                         "update", update, 		$
                         "delete", delete,              $  ; AEE 2/9/04
                         "sel_obs", sel_obs,            $  ; AEE 2/9/04
                         "curset", curset,              $  ; AEE 2/9/04
                         "cur_sets", cur_sets,          $  ; AEE 2/9/04
                         "newset",newset,               $  ; AEE 2/5/04
                         "oldset",oldset,               $  ; AEE 2/5/04
                         "req_by", req_by,              $  ; AEE 2/5/04
                         "imp_by", imp_by,              $  ; AEE 2/17/04
                         "instr", instr,                $  ; AEE 2/5/04
                         "last_flag", last_flag,        $  ; AEE 2/5/04
                         "msg", msg,                    $  ; AEE 2/18/04
                         "init_msg", init_msg,          $  ; AEE 2/18/04
                         "disp", disp,                  $  ; AEE 2/20/04
                         "label", label,                $
                         "clear", clear, 		$
                         "dismiss", dismiss    		$
                       )

    XMANAGER, "PLAN_SET", base

    WIDGET_CONTROL, opv.msg, SET_VALUE= init_msg

CONT:
    IF KEYWORD_SET(default_op) THEN BEGIN	;** bring up details for this default_op
       WIDGET_CONTROL, opv.op_num, SET_VALUE=STRTRIM(default_op, 2)
       ;** GENERATE AN EVENT FOR THE OP_NUM TEXT WIDGET
       event = {event, id:0L, top:0L, handler:0L}
       event.id = opv.op_num
       event.top = opv.base
       event.handler = opv.op_num
       WIDGET_CONTROL, opv.op_num, SEND_EVENT=event, /NO_COPY, SHOW=1
    ENDIF

    RETURN

END

;________________________________________________________________________________________________________
;
