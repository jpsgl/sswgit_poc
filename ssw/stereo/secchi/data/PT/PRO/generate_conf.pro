;+
;$Id: generate_conf.pro,v 1.7 2009/09/11 20:28:09 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GENERATE_CONF
;               
; Purpose     : Routine to output the current conflicts in the schedule, if any.
;               
; Use         : GENERATE_CONF, startime, endtime, conf_name, /all_files
;    
; Inputs      : startime     Start time of schedule.
;               endtime      End time of schedule.
;               conf_name    Output conflict filename  (.IPT.conflicts).
;               
; Opt. Inputs : None
;               
; Outputs     : None
;               
; Opt. Outputs: None
;               
; Keywords    : all_files   Indicates to use conf_name as is, otherwise, generate 
;                           the conflict filename.
;               
; Category    : Planning, Scheduling.
;               
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 08/17/04 - remove path from reported filename.
;              Ed Esfandiari 10/04/04 - direct output to SC_A or SC_B.
;              Ed Esfandiari 10/20/04 - Added S/C A and B dependency.
;
; $Log: generate_conf.pro,v $
; Revision 1.7  2009/09/11 20:28:09  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.4  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:01  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;               
;_____________________________________________________________________________________________________
;

PRO GENERATE_CONF, startdis, enddis, conf_name, all_files=all_files

COMMON DIALOG,mdiag,font
COMMON SCHED_CONF, all_conflicts
COMMON SCHED_SHARE, schedv

   ; Note:
   ; conflict file names generated using "Generate Conflicts File" end in .conflicts
   ;   but
   ; conflict file names generated using "Generate Plans File for Commnading" end in .IPT.conflicts

   ; conf_name when /all_files is set, is already set to SECYYYYMMDDvvv.IPT.conflicts on input.

  
   IF (schedv.sc EQ 1) THEN sc= 'SC_A'
   IF (schedv.sc EQ 2) THEN sc= 'SC_B'

   IF (NOT KEYWORD_SET(all_files)) THEN BEGIN
     ;** form conflicts filename SECYYYYMMDDvvv.conflicts
     utc = TAI2UTC(startdis, /EXTERNAL)
     conf_file_name = 'SEC'+STRN(utc.year)+STRN(utc.month,FORMAT='(I2.2)')+STRN(utc.day,FORMAT='(I2.2)')
     ;conf_file_name = GETENV('PT')+'/OUT/CONFLICTS/'+conf_file_name
     conf_file_name = GETENV('PT')+'/OUT/'+sc+'/CONFLICTS/'+conf_file_name

     ;** check for previous versions
     f_exist = FINDFILE(conf_file_name + '???.conflicts', count=count)
     IF (f_exist(0) EQ '') THEN $                                   ;** first version
        conf_file_name = conf_file_name + '000.conflicts' $
     ELSE BEGIN
        f_exist = STRUPCASE(f_exist(count-1))                         ;** make next version
        version = FIX(STRMID(f_exist,STRLEN(f_exist)-13,3))+1 
        conf_file_name = conf_file_name + STRN(version,FORMAT='(I3.3)') + '.conflicts'
     ENDELSE
     conf_name = conf_file_name
   ENDIF ELSE $
     ;conf_name= GETENV('PT')+'/OUT/CONFLICTS/'+conf_name
     conf_name= GETENV('PT')+'/OUT/'+sc+'/CONFLICTS/'+conf_name

   cfn= STR_SEP(conf_name,'/')
   cfn= cfn(N_ELEMENTS(cfn)-1)

   OPENW, OUT, conf_name, /GET_LUN
   ;WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_CONF: Creating file: '+conf_name
   ;PRINT,'%%%GENERATE_CONF: Creating file: '+conf_name
   WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_CONF: Creating file: '+cfn
   PRINT,'%%%GENERATE_CONF: Creating file: '+cfn
   WIDGET_CONTROL, /HOUR

   tmpconf= all_conflicts
   IF (schedv.sc EQ 1) THEN tmpconf= GET_SC_CONF(tmpconf,'A')
   IF (schedv.sc EQ 2) THEN tmpconf= GET_SC_CONF(tmpconf,'B')

   PRINTF,OUT,'Current Scheduling Conflicts:'
   PRINTF,OUT,''
   ;IF N_ELEMENTS(all_conflicts) EQ 1 THEN PRINTF,OUT,'None Exists.'
   ;FOR i= 1,N_ELEMENTS(all_conflicts)-1 DO PRINTF,OUT,all_conflicts(i)
   IF N_ELEMENTS(tmpconf) EQ 1 THEN PRINTF,OUT,'None Exists.'
   FOR i= 1,N_ELEMENTS(tmpconf)-1 DO PRINTF,OUT,tmpconf(i)

   CLOSE, OUT
   FREE_LUN, OUT
   cn= STRMID(conf_name,STRPOS(conf_name,'SEC2'),50)
   WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_CONF: ../OUT/'+sc+'/CONFLICTS/'+cn+' Done.'
   PRINT,'%%%GENERATE_CONF: ../OUT/'+sc+'/CONFLICTS/'+cn+' Done.'
   RETURN 
END

