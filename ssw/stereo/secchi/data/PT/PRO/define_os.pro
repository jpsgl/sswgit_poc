;+
;$Id: define_os.pro,v 1.7 2011/06/30 20:45:13 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : DEFINE_OS
;               
; Purpose     : Widget tool to define a SECCHI Observing Sequence.
;               
; Explanation : This tool allows the user to specify a specific SECCHI 
;               Observing Sequence (OS) consisting of a LEB Program (LP),
;		telescope setup, camera setup, image processing steps, etc.
; 		The tool then calculates the estimated time to process the
;		image(s) and the processed image size.  The SECCHI 
;		Scheduling tool can then be called from this tool.
;               
; Use         : Generally this tool will be called from the SECCHI 
;		Scheduling tool.  But it can be called from the command
;		line without any params:	IDL> define_os
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Restrictions: None.
;               
; Side effects: None.
;               
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Widget layout adapted from SOHO/LASCO.
;
; Written By  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 06/04/04 - Cleaned-Up.
;              Ed Esfandiari 11/30/04 - Added device, decomposed=0 statement.
;              Ed Esfandiari 12/12/06 - Added Compression Factors popup help.
;              Ed Esfandiari 02/08/07 - Read new comp_factors file.
;
; $Log: define_os.pro,v $
; Revision 1.7  2011/06/30 20:45:13  nathan
; num_text no longer has OS_NUM in it
;
; Revision 1.6  2009/09/11 20:28:08  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-            

;__________________________________________________________________________________________________________
;

PRO DEFINE_OS_EVENT, event

COMMON OS_SHARE, osv
COMMON OS_DEFINED, defined_os_arr, os_instance
COMMON SCHED_SHARE, schedv

CASE (event.id) OF

   osv.quit : BEGIN	;** exit program
      WIDGET_CONTROL, /DESTROY, osv.base
      osv=''       ; AEE 10/03/03 - added
   END

   osv.shcomp : BEGIN
     filename= GETENV('PT')+'/IN/OTHER/comp_factors.dat' 
     OPENR, lu, filename, /GET_LUN
     str = ''
     cfac= ['']
     WHILE NOT(EOF(lu)) DO BEGIN
       READF, lu, str
       ; throw away comment (;) and blank lines.
       IF (STRPOS(str,'IP Binned') GE 0) THEN BEGIN
         cfac= [cfac,STRTRIM(str,2)]
         READF, lu, str
         cfac= [cfac,STRTRIM(str,2)]
         READF, lu, str
         cfac= [cfac,STRTRIM(str,2)]
       ENDIF
       IF (STRMID(STRTRIM(str,2),0,1) NE ';' AND STRLEN(STRTRIM(str,2)) NE 0) THEN $ 
         cfac= [cfac,STRTRIM(str,2)]
     ENDWHILE
     CLOSE, lu
     FREE_LUN, lu
     cfac= cfac(1:*)

     RESTORE, GETENV('PT')+'/IN/OTHER/icer_size_bytes.sav'
     ic= WHERE(STRPOS(cfac,'IC') GE 0, icnt)
     ;FOR i=0, icnt-1 DO cfac(ic(i))= cfac(ic(i))+'  or '+STRING(icer_size_bytes(i),'(I8)')+'  Bytes'
     cfac(ic)= cfac(ic)+'  (or '+STRTRIM(icer_size_bytes,2)+'  Bytes)'
     OPENR, lu, GETENV('PT')+'/IN/OTHER/cheat_sheet.txt'
     WHILE NOT(EOF(lu)) DO BEGIN
       READF, lu, str
       cfac= [cfac,str]
     ENDWHILE
     CLOSE, lu
     FREE_LUN, lu

     POPUP_HELP,cfac, TITLE='Current IP Compression Factors + Image Setups',align='left', xsize=750
   END

   osv.statsb : BEGIN	;** calculate statistics
      ; AEE 10/14/03 - added:
      IF (DATATYPE(schedv) NE 'UND') THEN BEGIN
        WIDGET_CONTROL, schedv.num_text,     GET_VALUE=os_num
        osval= LONG(strmid(os_num(0),strpos(os_num,':')+1,10))
        ;IF (STRPOS(os_num,'OS_NUM') GE 0 AND osval GT 0) THEN BEGIN ; ignore OP(es) and "OS_NUM:  0"
        IF (osval GT 0) THEN BEGIN ; ignore OP(es) and "OS_NUM:  0"

          WIDGET_CONTROL, schedv.os_size_text, GET_VALUE=os_size
          WIDGET_CONTROL, schedv.os_dur_text,  GET_VALUE=os_duration
          WIDGET_CONTROL, osv.os_text , SET_VALUE=os_num(0)
          WIDGET_CONTROL, schedv.bits_bytes, GET_VALUE= from
          os_size = SCHEDULE_CONVERT_UNITS(os_size, from, osv.size_units)

          IF (schedv.time_fmt1 EQ '') THEN os_duration= os_duration * 60.D ; change from min to secs.
 
          WIDGET_CONTROL, osv.time_text , SET_VALUE=STRING(os_duration(0),FORMAT='(d7.1)')+' Secs '
          WIDGET_CONTROL, osv.size_text , SET_VALUE=STRING(DOUBLE(os_size(0)),FORMAT='(d16.1)')+'          '
          comp_type= ''
          ind= WHERE(defined_os_arr.os_num eq osval,cnt)
          IF (cnt GT 0) THEN comp_type= GET_COMP_TYPES(defined_os_arr(ind(0)))
          WIDGET_CONTROL, osv.comp_text , SET_VALUE= comp_type
        ENDIF ELSE BEGIN
          WIDGET_CONTROL, osv.info_text, SET_VALUE= 'Invalid '+os_num
        ENDELSE
      ENDIF ELSE WIDGET_CONTROL, osv.info_text, SET_VALUE= 'No OS is planned or scheduled. '
   END

   osv.bits_bytes : BEGIN	;** convert size units
      WIDGET_CONTROL, osv.size_text, GET_VALUE=os_size
      from = osv.size_units
      to = event.index   ;** 0:bits, 1:bytes, 2:% buffer
      osv.size_units = to
      new_value = SCHEDULE_CONVERT_UNITS(DOUBLE(os_size), from, to)
      IF (to EQ 0) THEN $
        WIDGET_CONTROL, osv.size_text, SET_VALUE=STRTRIM(STRING(new_value,FORMAT='(d16.1)'),2)+'          ' $
      ELSE $
        WIDGET_CONTROL, osv.size_text, SET_VALUE=STRTRIM(STRING(new_value,FORMAT=''),2)+'          '
   END

   ELSE : BEGIN
   END

ENDCASE

END

;__________________________________________________________________________________________________________
;

PRO DEFINE_OS,sched_start_time,keep_common=keep_common  ; AEE - 02/11/03

COMMON OS_SHARE
COMMON OS_INIT_SHARE, op_types, tele_types, table_types, fw_types, pw_types, exit_types, proc_tab_types, $
   ip_arr, plan_lp, lprow, cor1pw, cor2pw  ; AEE 12/29/03

 device, decomposed=0 ; to use 8 bits for color (for 24 bit systems) - IDL limitation.


 ssr_rt_mode= READ_SSR_RT_MODE('SSR_realtime.dat')  ; AEE - 6/13/03
 IF(ssr_rt_mode EQ '') THEN BEGIN
   PRINT
   PRINT,'==========================================================='
   PRINT,'ERROR: SSR_realtime.dat file does not indicate which one of'
   PRINT,'            BOTH, SPACEWEATHER, REALTIME, or NONE'
   PRINT,'       modes to be used. Correct the file and try again.'
   PRINT,'==========================================================='
   PRINT
   RETURN
 END

 IF(NOT KEYWORD_SET(keep_common)) THEN BEGIN
   IF ((SIZE(op_types))(1) NE 0) THEN BEGIN ; defined, remove common blocks before runing this routine.

    PRINT,''
    PRINT,'*********************************'
    PRINT,'Old Common Block Data Is Detected. 
    PRINT,'Must issue a'
    PRINT,'   @remove_common_blocks'
    PRINT,'command, first.'
    PRINT,'*********************************'
    PRINT,''
    RETURN

   END
 END


    IF XRegistered("DEFINE_OS") THEN RETURN

    OS_INIT		        ;** initialize some variables
    OS_UTIL			;** compile utilities ahead of time
    OS_TABLES			;** compile table upload procedures ahead of time
    OS_LP			;** compile observation procedures ahead of time

    IF ((SIZE(sched_list))(1) EQ 0) THEN sched_list = ''	;** not defined yet use default
    utable_types =["  Image Processing Table  ", $
                  " Camera Parameters Table  ", $
                  " Exposure Durations Table ", $
                  "   Occulter Mask Table    ", $
                  "      ROI Mask Table      "]
    table_pro = ["TABLE_PROCESSING", $
                 "TABLE_CAMERA", $
                 "TABLE_EXPOSURE", $
                 "MASK", $
                 "MASK", $
                 "TABLE_BADCOL", $
                 "TABLE_TRANSIENT"]


;   Note: order of op_pro should match those of op_types in init_os.pro

    ; AEE 9/23/03 - use lp=0 for double-images:
    op_pro = ["LP_DOUBLE_IMAGE",        $   ; lp = 0
              "LP_NORMAL_IMAGE",	$   ; lp = 1 
              "LP_DARK_IMAGE",		$   ; lp = 2 
              "LP_CAL_LAMP",		$   ; lp = 3 
              "LP_CONT_IMAGE",          $   ; lp = 4 
              "LP_SCIP_SEQ",		$   ; lp = 5 
              "LP_HI_SEQ",              $   ; lp = 6 
              "LP_BLOCK_SEQ"]               ; lp = 7 

    nop = N_ELEMENTS(op_types)
    op_buttons = LONARR(nop)

    op_sens = BYTARR(N_ELEMENTS(op_pro))

    good = [0,1,2,3,4,5,6,7] ; AEE 5/20/03 - make everything sensitive
    op_sens(good) = 1
    door = 0	;** closed

    door = 1    ; open  - AEE 10/14/03 - may (or not) use this but don't set it by the user in this
                ;                        routine (set it using the separately scheduled door close/opens.


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1' ; AEE May 06, 03

    sched_base = WIDGET_BASE(/COLUMN, TITLE='SECCHI Observation Planning Tool', /FRAME)

      col0 = WIDGET_BASE(sched_base, /COLUMN, /FRAME)
      arow = WIDGET_BASE(col0, /ROW)
      bk= '                                                                    '
      statsb = WIDGET_BUTTON(arow, VALUE=bk+'CALCULATE STATISTICS'+bk)

      ; 10/14/03 - AEE, Note: the door close/open
      ;                 for an scheduled images should be set using the "Schedule SCIP Door Commands"
      ;                 data before an .IPT file is generated (by setting the door bit of obs_mode_tel_state_str
      ;                 accordingly):

      toprow = WIDGET_BASE(col0, /ROW)

        os_text = WIDGET_LABEL(toprow, VALUE=' OS_NUM:             ') ; AEE 10/14/03 added

        ;temp = WIDGET_LABEL(toprow, VALUE=' Time to Process:')
        temp = WIDGET_LABEL(toprow, VALUE=' Image Duration:')
        new_value = 0D
        time_text = WIDGET_LABEL(toprow, VALUE=STRING(new_value,FORMAT='(d7.1)')+' Secs   ',/FRAME)

        temp = WIDGET_LABEL(toprow, VALUE='    Processed Size:')
        size_text = WIDGET_LABEL(toprow, VALUE=STRING(new_value,'(d16.1)')+'          ' ,/FRAME)
        bits_bytes = CW_BSELECTOR2(toprow, ["bits ","bytes","% SBUF Capacity","% SSR1 Capacity","% SSR2 Capacity", $
                                            "% RT Capacity","% SW Capacity"], SET_VALUE=0)

      midrow= WIDGET_BASE(col0, /ROW)
        temp = WIDGET_LABEL(midrow, VALUE=' Compression: ')
        comp_text = WIDGET_LABEL(midrow, VALUE=bk+bk)

      info_text = WIDGET_LABEL(col0, VALUE='                                                                                                                      ',FONT=font)

      nextrow = WIDGET_BASE(sched_base, /COL, /FRAME)
      temp = WIDGET_LABEL(nextrow, FONT=font, VALUE='     TABLE DEFINITIONS')
      nextrow = WIDGET_BASE(nextrow, /ROW, /FRAME)
        nc = 3
        colm = LONARR(nc)
        FOR i=0, nc-1 DO begin
          colm(i) = WIDGET_BASE(nextrow, /COLUMN)
          temp= WIDGET_LABEL(nextrow,VALUE='         ')
        end
        nt = N_ELEMENTS(utable_types)
        FOR i=0, nt-1 DO BEGIN
          temp = WIDGET_BUTTON(colm(i / CEIL(FLOAT(nt)/nc)), VALUE=utable_types(i), EVENT_PRO=table_pro(i))
          IF (STRTRIM(utable_types(i),2) EQ "Occulter Mask Table") THEN occ_id = temp
          IF (STRTRIM(utable_types(i),2) EQ "ROI Mask Table") THEN roi_id = temp
        ENDFOR

      ;shcomp = WIDGET_BUTTON(nextrow, VALUE='Compression Info')
      ;shcomp = WIDGET_BUTTON(nextrow, VALUE='Compression Factors')
      shcomp = WIDGET_BUTTON(nextrow, VALUE='The Cheat Sheet')

      lprow = WIDGET_BASE(sched_base, /COL, /FRAME)
      temp = WIDGET_LABEL(lprow, FONT=font, VALUE='COMPLEX IMAGE COMMANDS')  ; AEE - Mar 19, 03
      lprow = WIDGET_BASE(lprow, /ROW, /FRAME)
        nc = 6
        colm = LONARR(N_ELEMENTS(op_pro) -1) ; AEE -  9/23/03 - use Null (lp=0) for double-images

        nt = N_ELEMENTS(op_types) - 1 ; AEE - 9/23/03 - use lp=0 for double-images

        mid= 5 ; AEE - 9/23/03 - use lp=0 for double-images

        FOR i=0, mid-1 DO colm(i) = WIDGET_BASE(lprow, /ROW)
    
        FOR i=0, mid-1 DO BEGIN 
          tmp= WIDGET_LABEL(colm(i), VALUE='   ') ; AEE 9/23/03
          op_buttons(i) = WIDGET_BUTTON(colm(i), VALUE=op_types(i), EVENT_FUNC=op_pro(i)) ; AEE - 9/23/03
          tmp= WIDGET_LABEL(colm(i), VALUE='        ')
        ENDFOR

        lprow1 = WIDGET_BASE(sched_base, /COL, /FRAME)
        temp = WIDGET_LABEL(lprow1, FONT=font, VALUE='SEQUENCE IMAGE COMMANDS') 
        lprow1 = WIDGET_BASE(lprow1, /ROW, /FRAME)
        nc = 2 
        FOR i=0, nc-1 DO colm(mid+i) = WIDGET_BASE(lprow1, /ROW)
        FOR i=mid, nt-1 DO BEGIN 
          tmp= WIDGET_LABEL(colm(i), VALUE='                              ')
          op_buttons(i) = WIDGET_BUTTON(colm(i), VALUE=op_types(i), EVENT_FUNC=op_pro(i)) ; AEE - 9/23/03
          tmp= WIDGET_LABEL(colm(i), VALUE='                                   ')
        ENDFOR

      quit = WIDGET_BUTTON(sched_base, VALUE=" Quit ")

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, sched_base, /REALIZE
    WIDGET_CONTROL, occ_id, SET_UVALUE='OCCULTER'
    WIDGET_CONTROL, roi_id, SET_UVALUE='ROI'
    FOR i=0, nt-1 DO $
      WIDGET_CONTROL, op_buttons(i), SENSITIVE=op_sens(i) ; AEE 9/23/03 - use lp=0 for double-images

    osv = CREATE_STRUCT( 'base', sched_base, 	$
                            'op_buttons', op_buttons,	$
                            'op_pro', op_pro,		$
                            'quit', quit,		$
                            'shcomp', shcomp,           $
                            'door', door,		$
                            'os_text', os_text,         $ ; AEE 10/14/03 added
                            'statsb', statsb,		$
                            'time_text', time_text,	$
                            'size_text', size_text,	$
                            'size_units', 0,		$
                            'bits_bytes', bits_bytes,	$
                            'comp_text', comp_text,	$
                            'info_text', info_text )

    XMANAGER, 'DEFINE_OS', sched_base

END
