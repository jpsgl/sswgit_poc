;+
;$Id: generate_ipt_set.pro,v 1.12 2009/09/11 20:28:10 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : GENERATE_IPT_SET
;               
; Purpose     : Routine to output OSes in any newly created campaign set in form of a .IPT file.
;               
; Explanation : This routine prints out the current schedule SETs in IPT format. It also
;               generates a .info file for the set.
;               
; Use         : GENERATE_IPT_SET, set_os
;    
; Inputs      : set_os     OSes used in the SET(s). 
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version.
;
; Modification History:
;              Ed Esfandiari 06/04/04 - Made more accurate time matching when selecting OSes for a set
;                                       and also print os_num as 4-digits.
;                            06/15/04 - Added PT_OS_PROCT (processing time) parameter. 
;                            06/16/04 - Added PT_OS_SETUP (setup time) parameter.
;                            07/01/04 - Used expanded os_start to pickup img_cntr, seq_cntr, and sfn. 
;              Ed Esfandiari 07/14/04 - Use 4 ROI tables instead of 1. Also added cl_time and ro_time.
;                            08/31/04 - Changed format of exptime reports.
;                            09/27/04 - Added PT_SC_ID.
;                            09/30/04 - Removed sync.
;              Ed Esfandiari 12/13/04 - Report non cal image exptimes corrected for CCD summing.
;              Ed Esfandiari 01/10/05 - Added low gain exptime correction for non-cal images.
;              Ed Esfandiari 05/24/05 - Use ccd orientation dependant masks.
;              Ed Esfandiari 07/21/05 - Added sched_share (SCHEdv) common block.
;              Ed Esfandiari 03/24/06 - Break schedule into daily periods so that img_cntr
;                                       info which is reset to 1 at 00:00:00 hour on each day
;                                       for subsequent days are not lost if schedule expands
;                                       into next day(s).
;              Ed Esfandiari 05/10/06 - Added proc_times for multi_apids. Also keep first size and
;                                       unique proc_times of OS in a sequence and changes size units from
;                                       from bits to bytes. In schedule_read*.pro since USEDEFAULTS = 1,
;                                       size, etc. are recalculated so things are fine.
;              Ed Esfandiari 05/11/06 - Added table usage information to the IPT file.
;              Ed Esfandiari 05/23/06 - Added PT filename info and removed $ from table info.
;              Ed Esfandiari 10/21/06 - Ignore ccd_sum and low gain exptime correction (FSW does not do it).
;
;
; $Log: generate_ipt_set.pro,v $
; Revision 1.12  2009/09/11 20:28:10  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.7  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.6  2005/10/17 19:02:09  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.5  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.4  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 15:40:43  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:01  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO GENERATE_IPT_SET, os_arr_info

COMMON OS_DEFINED
COMMON OS_SCHEDULED
COMMON DIALOG, mdiag,font
COMMON APIDS, multi_apid   ; AEE - 07/25/03
COMMON LP_CAL_LAMP_SHARE, lpcalv ; AEE - 08/22/03
COMMON EXPANDED_OS,  expanded ; AEE 1/15/04
COMMON SET_DEFINED, set_instance, defined_set_arr ; AEE 2/12/04
COMMON SETS, select_set ; AEE 2/12/04
COMMON SCHED_SHARE, schedv
COMMON TABLES_IN_USE, tables_used, tbAout,tbBout

IF (DATATYPE(os_arr_info) NE 'STC') THEN RETURN  ; AEE 4/5/04

w= WHERE(os_arr_info.set_id GT 0, wcnt)
IF (wcnt EQ 0) THEN RETURN
os_tmp_set= os_arr_info(w)

RESTORE,GETENV('PT')+'/IO/SETS/'+'set_last_id.sav' ; => last_id

IF (MAX(os_tmp_set.set_id) LE last_id) THEN BEGIN
  PRINT,''
  PRINT,'Schedule contains only OLD sets. Nothing was added.'
  PRINT,''
  RETURN
ENDIF ELSE BEGIN
  os_tmp_set= os_tmp_set(WHERE(os_tmp_set.set_id GT last_id))
ENDELSE

setids= os_tmp_set.set_id
usets= setids(UNIQ_NOSORT(setids))

FOR u= 0, N_ELEMENTS(usets)-1 DO BEGIN
   last_id= usets(u)

   setdir= GETENV('PT')+'/IO/SETS/'
   ipt_set_name = setdir+'SET_'+STRING(last_id,'(i4.4)')+'.IPT' 
   info_set_name = setdir+'SET_'+STRING(last_id,'(i4.4)')+'.INFO'

   os_set= os_tmp_set(WHERE(os_tmp_set.set_id EQ usets(u)))

   OPENW, OUT, ipt_set_name, /GET_LUN
   OPENW, OUT1, info_set_name, /GET_LUN
   WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_SET_IPT: Creating files: '+ipt_set_name+' and '+info_set_name
   PRINT,'%%%GENERATE_IPT: Creating files: '+ipt_set_name+' and '+info_set_name

   WIDGET_CONTROL, /HOUR

   tables= [tbAout,tbBout]
   tables= [';SETS/PT Schedule = '+ipt_set_name,tables]

   FOR tb= 0, N_ELEMENTS(tables)-1 DO PRINTF, OUT, tables(tb)

   os_arr2 = os_set(SORT([os_set.os_num]))   ;** sort on os_num
   uniq_os_arr2 = os_arr2(UNIQ([os_arr2.os_num]))
   num_os = N_ELEMENTS(uniq_os_arr2)

   FOR i = num_os-1, 0, -1 DO BEGIN    ;** for every unique os_num

      os_num = uniq_os_arr2(i).os_num
      PRINTF, OUT, 'PT_OS_NUM = '+STRING(os_num,'(i4.4)')
      PRINTF, OUT, 'PT_SET_ID = '+STRING(usets(u),'(i4.4)')

      index = WHERE(defined_os_arr.os_num EQ os_num AND $
                    defined_os_arr.num_images GT 0, oscnt)    ; AEE - 01/10/03 

    FOR cnt= 0, oscnt-1 DO BEGIN  ; AEE - Dec 05, 02 ; if os_cnt=2 => lp=6 (HI Seq)
      ind= index(cnt)
      lp = defined_os_arr(ind).lp

      ; Identify second OS of LP=6 (when both HI1 and 2 are used) with minus os_num:
      IF (cnt EQ 1) THEN PRINTF, OUT, 'PT_OS_NUM = -'+STRING(os_num,'(i4.4)')

      PRINTF, OUT, 'PT_LP_NUM = ' + STRTRIM(lp,2)

      PRINTF, OUT, 'PT_LP_EXIT = '+STRTRIM(0,2)	;** for now always use exit by iteration
      iter_cnt = 1				;** for now always schedule just 1
      PRINTF, OUT, 'PT_LP_ITER = '+STRTRIM(iter_cnt,2)

      good = WHERE(os_set.os_num EQ uniq_os_arr2(i).os_num AND os_set.os_tele EQ defined_os_arr(ind).tele)

      ;** sort on start time
      nsched = N_ELEMENTS(good)
      IF (nsched GT 1) THEN good = good(SORT([os_set(good).os_start]))
      first_os = os_set(good(0))
      PRINTF, OUT, 'PT_LP_NSCHED = '+STRTRIM(nsched,2)

      ; pickup SpaceCraft ID(s):
      sc_id= '('
      FOR sc_cnt=0, nsched -2 DO BEGIN
        sc_id= sc_id + os_set(good(sc_cnt)).sc + ','
      ENDFOR
      IF (nsched GT 1) THEN $
        sc_id= sc_id + os_set(good(sc_cnt)).sc $
      ELSE $
        sc_id= sc_id + os_set(good(0)).sc
      sc_id= sc_id + ')'
      PRINTF, OUT, 'PT_SC_ID = '+sc_id

      ; pick up img_cntr for images. For a seq of N images, all N images have the same img_cntr and
      ; seq_cntr of 1-N but for a summed-seq, only the Nth imags exists with seq_cntr set to N:

      eind= WHERE(expanded.os_num EQ os_num AND expanded.os_start EQ os_set(good(0)).os_start)
      FOR osind= 1, nsched-1 DO BEGIN
        eind= [eind,WHERE(expanded.os_num EQ os_num AND expanded.os_start EQ os_set(good(osind)).os_start)]
      ENDFOR

      expand= expanded(eind)

      img_cnt= expand.img_cnt ;two non-seq images may have the same img_cnt when schedule expands beyond 1 day

      ;img_cnt= expand(UNIQ_NOSORT(expand.img_cnt)).img_cnt

      ; since at start of each days, img_cnt is set to 1, breakup schedule to daily pieces (if expands
      ; into next day(s)) so that same cntrs (starting with 1 again) are not lost when using uniq:

        sdays= long(expand.start / 24.0)
        unq_sdays= UNIQ_NOSORT(sdays)
        unq_sdays= [unq_sdays,N_ELEMENTS(sdays)-1] ; now have index range for each day
        img_cnt= -1
        FOR ud= 0, N_ELEMENTS(unq_sdays)-2 DO BEGIN
          ;daily_cnt= expand(unq_sdays(ud):unq_sdays(ud+1)-1).img_cnt
          daily_cnt= expand(unq_sdays(ud):unq_sdays(ud+1)).img_cnt
         img_cnt= [img_cnt,daily_cnt(UNIQ_NOSORT(daily_cnt))]
        ENDFOR
        img_cnt= img_cnt(1:*)

      img_cntr= '('+STRTRIM(img_cnt(0),2)
      FOR ucnt= 1, N_ELEMENTS(img_cnt)-1 DO $
        img_cntr= img_cntr + ','+STRTRIM(img_cnt(ucnt),2)
      img_cntr= img_cntr + ')'
      PRINTF, OUT, 'PT_IMG_CNTR= '+STRTRIM(img_cntr,2) 

      seq_cnt=expand(UNIQ_NOSORT(expand.seq_cnt)).seq_cnt

      seq_cntr= '('+STRTRIM(seq_cnt(0),2)
      FOR ucnt= 1, N_ELEMENTS(seq_cnt)-1 DO $
        seq_cntr= seq_cntr + ','+STRTRIM(seq_cnt(ucnt),2)
      seq_cntr=  seq_cntr + ')'
      PRINTF, OUT, 'PT_SEQ_CNTR= '+STRTRIM(seq_cntr,2)

;      sfn= expand.sfn
;      sfns= '('+STRTRIM(sfn(0),2)
;      FOR ucnt= 1, N_ELEMENTS(sfn)-1  DO sfns= sfns+ ','+STRTRIM(sfn(ucnt),2)
;      sfns=  sfns + ')'
;      PRINTF, OUT, 'PT_SFN= '+STRTRIM(sfns,2)

      ;** output start times
      c = 0 					;** counter, only 5 start times per line
      cc = 0 					;** counter, only 5 start times per line
      ccc = 0 					;** counter, only 5 start times per line
      c4 = 0 					;** counter, only 5 start times per line
      FOR j=0, nsched-1 DO BEGIN 		;** for every occurance of this os_num
         IF (c EQ 0) THEN str = '('		;** new line
         IF (cc EQ 0) THEN cam_str = '('		;** new line
         os_start = os_set(good(j)).os_start
         utc = TAI2UTC(os_start, /EXTERNAL)
         yy = STRTRIM(utc.year,2) 
         mm = STRTRIM(utc.month,2) & IF (STRLEN(mm) EQ 1) THEN mm = '0'+mm
         dd = STRTRIM(utc.day,2)   & IF (STRLEN(dd) EQ 1) THEN dd = '0'+dd
         hh = STRTRIM(utc.hour,2)  & IF (STRLEN(hh) EQ 1) THEN hh = '0'+hh
         mi = STRTRIM(utc.minute,2)& IF (STRLEN(mi) EQ 1) THEN mi = '0'+mi
         ss = STRTRIM(utc.second,2)& IF (STRLEN(ss) EQ 1) THEN ss = '0'+ss
         tt = yy+mm+dd+'_'+hh+mi+ss
         str = str + tt + ','
         IF (DATATYPE(load_camtable) EQ 'STC') THEN BEGIN
            cind = WHERE((os_num EQ load_camtable.os_num) AND (os_start EQ load_camtable.os_start))
            IF (cind(0) NE -1) THEN BEGIN
               cam_str = cam_str + tt + ','
               cc = cc + 1
            ENDIF
         ENDIF
         c = c + 1
         IF ((c EQ 5) OR (j EQ (nsched-1))) THEN BEGIN	;** only 5 entries per line
            str = STRMID(str,0,STRLEN(str)-1) + ')'
            PRINTF, OUT, 'PT_LP_START = '+str
            c = 0
         ENDIF
      ENDFOR					;** for every start time for this os_num

      cam_str = STRMID(cam_str,0,STRLEN(cam_str)-1) + ')'
      IF (cam_str NE ')') THEN PRINTF, OUT, 'PT_LOAD_CAM = '+cam_str


      tele = defined_os_arr(ind).tele
      fw = defined_os_arr(ind).fw
      pw = defined_os_arr(ind).pw
      sub = defined_os_arr(ind).sub
      start = defined_os_arr(ind).start
      ;table = defined_os_arr(ind).table  ; AEE 1/14/04
      exptable = defined_os_arr(ind).exptable  ; AEE 1/14/04
      camtable = defined_os_arr(ind).camtable  ; AEE 1/14/04
      fps = defined_os_arr(ind).fps ; AEE 1/14/04
      iptable = defined_os_arr(ind).iptable
      lamp = defined_os_arr(ind).lamp
      num_images = defined_os_arr(ind).num_images
      ccd = defined_os_arr(ind).ccd
      ip = defined_os_arr(ind).ip
      ex = defined_os_arr(ind).ex
      occ_blks = REFORM(defined_os_arr(ind).occ_blocks(tele,*))
      roi_blks = REFORM(defined_os_arr(ind).roi_blocks(tele,*))
  

      PRINTF, OUT, 'PT_TELE = '+STRTRIM(tele,2)
      PRINTF, OUT, 'PT_EXPT = '+STRTRIM(exptable+1,2)  ; AEE 1/14/04
      PRINTF, OUT, 'PT_CAMT = '+STRTRIM(camtable+1,2)  ; AEE 1/14/04
      PRINTF, OUT, 'PT_FW = '+STRTRIM(fw,2)

      IF (lp EQ 200) THEN BEGIN	; AEE -8/23/03
      ENDIF ELSE BEGIN			;** output pw/exp pairs
         pwstr = '('
         expstr = '('

         ; For non cal images, report exptime that is corrected for CCD summing:

         ccdsum= 1
         IF (lp NE 2 AND lp NE 3) THEN BEGIN
           xs = ccd(tele,camtable).xsum
           ys = ccd(tele,camtable).ysum
           ccdsum= ((xs > 1) * (ys > 1))
           IF (ccd(tele,camtable).gmode EQ 1) THEN ccdsum= ccdsum/4.0 ; for low gain, amplify exptime by 4
         ENDIF

         ccdsum=1 ; Ignore ccd_sum and low gain exptime correction (FSW does not do it).

         FOR n=0, num_images -1 DO BEGIN  ;** for each pw position in the seq (for non-seq num_images=1 always)
            IF (tele GE 3) THEN BEGIN ; AEE March 19, 2003
              ; Note: HI telescpes do not have a shtter, polarizer, or filter wheels so pw(0) is only used.
              pwstr = pwstr + STRTRIM(pw(0),2) + ','
              IF (lp EQ 4) THEN BEGIN 
                expstr = expstr + STRTRIM(0,2) + ','   ; AEE 5/22/03
              ENDIF ELSE BEGIN 
                expstr = expstr + STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2) + ','
              ENDELSE
              IF (lp EQ 0) THEN BEGIN
                ; For double sky images, add the second pw and exp.time (for these images n is always 0)
                ; and since it is a HI image, the second pw and, therefore, exp.time are the same as the first.
                pwstr = pwstr + STRTRIM(pw(0),2) + ','
                expstr = expstr + STRTRIM(STRING(ex(tele,exptable,fw,pw(0))/ccdsum,FORMAT='(I7)'),2) + ','
              ENDIF
            ENDIF ELSE BEGIN
              pwstr = pwstr + STRTRIM(pw(n),2) + ','
              IF (lp EQ 4) THEN BEGIN 
                expstr = expstr + STRTRIM(0,2) + ','   ; AEE 5/22/03
              ENDIF ELSE BEGIN
                expstr = expstr + STRTRIM(STRING(ex(tele,exptable,fw,pw(n))/ccdsum,FORMAT='(I7)'),2) + ','
              ENDELSE
              IF (lp EQ 0) THEN BEGIN
                ; For double sky images, add the second pw and exp.time (for these images n is always 0)
                ; and pw(1) indicates the second pw positions and exptime to be used. 
                pwstr = pwstr + STRTRIM(pw(1),2) + ','
                expstr = expstr + STRTRIM(STRING(ex(tele,exptable,fw,pw(1))/ccdsum,FORMAT='(I7)'),2) + ','
              ENDIF
            ENDELSE

         ENDFOR


         pwstr = STRMID(pwstr,0,STRLEN(pwstr)-1) + ')'
         PRINTF, OUT, 'PT_PW = '+pwstr
         expstr = STRMID(expstr,0,STRLEN(expstr)-1) + ')'
         PRINTF, OUT, 'PT_EXPTIME = '+expstr
      ENDELSE
      IF (lamp GT 2) then BEGIN ; AEE - 8/22/03
        WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_IPT:  STOP -  PT_CAL_LAMP = '+STRING(lamp,'(I3)')+ $
                                       ', from input, is out of range.' 
        PRINT,''
        PRINT,'STOP -  PT_CAL_LAMP = '+STRING(lamp,'(I3)')+', from input, is out of range.'
        PRINT,'        Exit the program, fix the problem, and start again.'
        PRINT,''
        STOP 
      ENDIF


      PRINTF, OUT, 'PT_CAL_LAMP = '+STRTRIM(lamp,2)
      str = '('+STRTRIM(ccd(tele,camtable).y1,2)+','
      str = str+STRTRIM(ccd(tele,camtable).x1,2)+','
      str = str+STRTRIM(ccd(tele,camtable).y2,2)+','
      str = str+STRTRIM(ccd(tele,camtable).x2,2)+','
      str = str+STRTRIM(ccd(tele,camtable).ysum,2)+','
      str = str+STRTRIM(ccd(tele,camtable).xsum,2)+')'
      PRINTF, OUT, 'PT_CAM_RO = '+str
      str = '('+STRTRIM(ccd(tele,camtable).pform,2)+',' ; AEE 4/5/04
      str = str+STRTRIM(FIX(ccd(tele,camtable).clr_id),2)+','
      str = str+STRTRIM(ccd(tele,camtable).rdout_id,2)+','
      str = str+STRTRIM(ccd(tele,camtable).cl_time,2)+','
      str = str+STRTRIM(ccd(tele,camtable).ro_time,2)+')'

      PRINTF, OUT, 'PT_CAM_OTH = '+str

      ;** print out the image processing table number 
      PRINTF, OUT, 'PT_IP_TAB_NUM = '+STRTRIM(iptable,2)

      good = WHERE(ip(iptable).steps GE 0, nip)
      str = '('
      FOR n=0, nip-1 DO BEGIN
         str = str+STRTRIM(ip(iptable).steps(good(n)),2)+','
      ENDFOR
      PRINTF, OUT, 'PT_NIP = '+STRTRIM(nip,2)
      str = STRMID(str,0,STRLEN(str)-1) + ')'
      PRINTF, OUT, 'PT_IP = '+str

      ; AEE 5/5/04:
      ind = WHERE(ip(iptable).steps EQ 28) ; 28 = don't use occ mask table. 
      IF (ind(0) GE 0) THEN useocc = 0 ELSE useocc = 1  ; Also see os_get_num_pixels.pro
                                                        ;          generate_ipt.pro
                                                        ;          generate_ipt_set.pro
                                                        ;          os_summary.pro
                                                        ;          os_summary_1line.pro
                                                        ;          os_summary_set.pro
                                                        ;          os_summary_text.pro

      ;ind = WHERE(ip(iptable).steps EQ 25)
      ;IF (ind(0) LT 0) THEN useroi = 0 ELSE useroi = 1

      roi_table= WHICH_ROI_TABLE(ip,iptable)
      IF (roi_table EQ -1) THEN useroi = 0 ELSE useroi = 1


         RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav'

         IF (useocc EQ 1) THEN BEGIN		;** Occulter Mask
            blocks = occ_blks
            p1 = 'PT_NOCC = '
            p2 = 'PT_OCC_MASK = '
            good = WHERE(blocks GE 1, nb)
            PRINTF, OUT, p1+STRTRIM(nb,2)
            chng_arr= INTARR(xyblks*xyblks)
            IF (nb GT 0) THEN BEGIN 
              ;change= MODIFY_MASK_ORDER(good) ; AEE 5/26/04 - change order from idl to mask_table.
              telid= ['EUVI','COR1','COR2','HI1','HI2']
              telstr= telid(tele)
              scid=['A','A','B'] ; first 'A' is really 'AB' but can't have it for mask.
              scstr= scid(schedv.sc)
              change= MODIFY_MASK_ORDER(scstr,telstr,good) ; change order from idl to mask_table.
              chng_arr(change)= 1
            ENDIF
            runs = FIND_DATA_RUNS(INDGEN(xyblks*xyblks),chng_arr,0,0,1)

            c = 0 	;** counter, only 5 entries per line
            pairs = (N_ELEMENTS(runs)/2)
            FOR p=0, pairs-1 DO BEGIN
               IF (c EQ 0) THEN str = '('				;** new line
               IF (runs(p,0) EQ runs(p,1)) THEN $			;** ex. use 39, instead of 39-39,
                  str = str + STRTRIM(runs(p,0),2) + ',' $
               ELSE $
                  str = str + STRTRIM(runs(p,0),2) + '-' + STRTRIM(runs(p,1),2) + ','
               c = c + 1
               IF ((c EQ 5) OR (p EQ (pairs-1))) THEN BEGIN	;** only 5 entries per line
                  str = STRMID(str,0,STRLEN(str)-1) + ')'
                  PRINTF, OUT, p2+str
                  c = 0
               ENDIF
            ENDFOR		;** for each pair
         ENDIF
         IF (useroi EQ 1) THEN BEGIN		;** ROI Mask
            blocks = roi_blks
            p1 = 'PT_NROI = '
            p2 = 'PT_ROI_MASK = '
            good = WHERE(blocks GE 1, nb)
            PRINTF, OUT, p1+STRTRIM(nb,2)
            chng_arr= INTARR(xyblks*xyblks)
            IF (nb GT 0) THEN BEGIN 
              ;change= MODIFY_MASK_ORDER(good) ; AEE 5/26/04 - change order from idl to mask_table.
              telid= ['EUVI','COR1','COR2','HI1','HI2']
              telstr= telid(tele)
              scid=['A','A','B'] ; first 'A' is really 'AB' but can't have it for mask.
              scstr= scid(schedv.sc)
              change= MODIFY_MASK_ORDER(scstr,telstr,good) ; change order from idl to mask_table.
              chng_arr(change)= 1
            ENDIF
            runs = FIND_DATA_RUNS(INDGEN(xyblks*xyblks),chng_arr,0,0,1)

            c = 0 	;** counter, only 5 entries per line
            pairs = (N_ELEMENTS(runs)/2)
            FOR p=0, pairs-1 DO BEGIN
               IF (c EQ 0) THEN str = '('				;** new line
               IF (runs(p,0) EQ runs(p,1)) THEN $			;** ex. use 39, instead of 39-39,
                  str = str + STRTRIM(runs(p,0),2) + ',' $
               ELSE $
                  str = str + STRTRIM(runs(p,0),2) + '-' + STRTRIM(runs(p,1),2) + ','
               c = c + 1
               IF ((c EQ 5) OR (p EQ (pairs-1))) THEN BEGIN	;** only 5 entries per line
                  str = STRMID(str,0,STRLEN(str)-1) + ')'
                  PRINTF, OUT, p2+str
                  c = 0
               ENDIF
            ENDFOR		;** for each pair
         ENDIF

CONT:

      os_size = first_os.os_size
      os_size = LONG(first_os.os_size / 8.0) ; change to bytes
      os_duration = first_os.os_duration
      aind= WHERE(multi_apid.os_num EQ os_num)  ; AEE - 7/25/03
      PRINTF, OUT, 'PT_OS_SIZE = '+STRTRIM(os_size,2)
      PRINTF, OUT, 'PT_OS_DUR = '+STRTRIM(STRING(os_duration,'(f20.2)'),2)
      PRINTF, OUT, 'PT_OS_PROCT = '+STRTRIM(STRING(first_os.os_proc_time,'(f20.2)'),2)
      PRINTF, OUT, 'PT_OS_ROT = '+STRTRIM(STRING(first_os.os_ro_time,'(f20.2)'),2)  ; AEE - 01/28/03
      PRINTF, OUT, 'PT_OS_PPT = '+STRTRIM(STRING(first_os.os_pre_proc_time,'(f20.2)'),2)  ; AEE - 01/28/03
      PRINTF, OUT, 'PT_OS_SETUP = '+STRTRIM(STRING(first_os.os_setup_time,'(f20.2)'),2)
      PRINTF, OUT, 'PT_OS_CAD = '+STRTRIM(STRING(first_os.os_cadence,'(f20.2)'),2)  ; AEE - 01/28/03

      ;PRINTF, OUT, 'PT_OS_SIZES= '+multi_apid(aind).sizes  ; AEE 7/25/03
      sizes= STRTRIM((STR_SEP(multi_apid(aind).sizes,','))(0),2)
      sizes= LONG(str_sep(sizes,' ')/8.0)
      sstr= 'PT_OS_SIZES ='
      FOR sz= 0, N_ELEMENTS(sizes)-1 DO sstr= sstr+' '+STRTRIM(sizes(sz),2)
      PRINTF, OUT, sstr

      ;PRINTF, OUT, 'PT_OS_PROC_TIMES = '+multi_apid(aind).proc_times
      ;PRINTF, OUT, 'PT_OS_PROC_TIMES = '+(STR_SEP(multi_apid(aind).proc_times,','))(0)
      ; Keep unique proc_times (for HI summed sequences that use 3 different ip tables):
      toks= STR_SEP(multi_apid(aind).proc_times,',')
      stoks= SHIFT(toks,1)
      dind= WHERE(toks NE stoks, dcnt)
      ptm= toks(0)
      FOR di= 1, dcnt-1 DO ptm= ptm+','+toks(dind(di))
      PRINTF, OUT, 'PT_OS_PROC_TIMES = '+ptm

      PRINTF, OUT, 'PT_FPS = '+STRTRIM(fps,2)   ; AEE - 1/14/04
      PRINTF, OUT, ';' & PRINTF, OUT, ';'

   ENDFOR ; AEE - Dec 05, 02
   ENDFOR		;** for every unique os_num

   CLOSE, OUT
   FREE_LUN, OUT

   ; Now create the corrsponding info file:

   sind= WHERE(defined_set_arr.set_id EQ last_id) & sind= sind(0)
   PRINTF, OUT1, 'SET_ID  = '+STRING(defined_set_arr(sind).set_id,'(i4.4)')
   PRINTF, OUT1, 'FNAME   = '+defined_set_arr(sind).fname
   PRINTF, OUT1, 'INSTR   = '+defined_set_arr(sind).instr
   PRINTF, OUT1, 'SCI_OBJ = '+defined_set_arr(sind).sci_obj
   PRINTF, OUT1, 'COMMENT = '+defined_set_arr(sind).comment
   PRINTF, OUT1, 'REQ_BY  = '+defined_set_arr(sind).req_by
   PRINTF, OUT1, 'CREATED = '+defined_set_arr(sind).cr_date

   img_info= OS_SUMMARY_SET(os_set)

   rows= N_ELEMENTS(img_info)
   IF (rows GE 2) THEN BEGIN 
    FOR p= 0, 1 DO PRINTF, OUT1, 'LABEL   = '+img_info(p) 
    FOR p=2, rows-1 DO PRINTF, OUT1, 'IMAGE   = '+img_info(p)
   ENDIF

   CLOSE, OUT1
   FREE_LUN, OUT1

   WIDGET_CONTROL,mdiag,SET_VALUE='%%%GENERATE_IPT_SET: ('+ipt_set_name+' and '+info_set_name+') Done.'
   PRINT,'%%%GENERATE_IPT: ('+ipt_set_name+' and '+info_set_name+') Done.'

ENDFOR ; usets

SAVE,filename= GETENV('PT')+'/IO/SETS/'+'set_last_id.sav',last_id

END
