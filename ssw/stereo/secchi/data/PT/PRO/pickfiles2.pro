;+
;$Id: pickfiles2.pro,v 1.4 2009/09/11 20:28:17 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : PICKFILES2
;               
; Purpose     : Widget tool to allow user to pick multiple files.
;               
; Use         : files = PICKFILES2(FILES=files, FILTER=filter, PATH=path, EXCLUSIVE=exclusive)
;    
; Inputs      : None.
;               
; Outputs     : STRARR containing names of files selected, or '' (empty string)
;		if none selected.
;               
; Keywords    : 
;       FILES:  A string array containing file choices to diplay.
;
;       PATH:   The initial path to select files from.  If this keyword is
;               not set, the current directory is used.
;
;       FILTER: A string value for filtering the files in the file list.  This
;               keyword is used to reduce the number of files to choose from.
;               Example filter values might be "*.fits" or "*.pro".
;
;    EXCLUSIVE: If set, only one file can be selected. Default is multiple (nonexclusive)
;               selection. 
;
; Category    : Widgets.
;               
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
; Written by  : Ed Esfandiari 11/22/06 - Increased ysize.
;               
; Modification History:
;
; $Log: pickfiles2.pro,v $
; Revision 1.4  2009/09/11 20:28:17  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.1.1.2  2004/07/01 21:19:06  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

;__________________________________________________________________________________________________________
;

PRO PICKFILES2_EVENT, ev

COMMON PICKFILES2_COMMON, base, all_files, selected, files_base

WIDGET_CONTROL, ev.id, GET_UVALUE=uval

   xs = 100
   ys = 100
   colxmax = 5
    
   CASE (uval) OF

      0 : BEGIN	;** exit program
	     WIDGET_CONTROL, /DESTROY, base
	     GOTO, done
          END

      1 : BEGIN	;** select ALL files
             WIDGET_CONTROL, files_base, SET_BUTTON=1
             selected(*) = 1B
          END

      2 : BEGIN	;** CLEAR ALL files
             WIDGET_CONTROL, files_base, SET_BUTTON=0
             selected(*) = 0B
          END

      ELSE : BEGIN	;** file selection
             ind = WHERE(all_files EQ uval)
             IF (ev.select EQ 1) THEN selected(ind) = 1B ELSE selected(ind) = 0B
          END

   ENDCASE

   done:

END

;-----------------------------------------------------------------------

FUNCTION PICKFILES2, FILES=files, XSIZE=xsize, FILTER=filter, PATH=path, TITLE=title,EXCLUSIVE=exclusive

COMMON PICKFILES2_COMMON, base, all_files, selected, files_base

   IF NOT(KEYWORD_SET(FILES)) THEN BEGIN
      IF (KEYWORD_SET(FILTER)) THEN filt = FILTER ELSE filt = "*.*"
      IF (KEYWORD_SET(PATH)) THEN filt = path + '/' + filt
      files = FINDFILE(filt)
   ENDIF
   IF KEYWORD_SET(xsize) THEN xs = xsize ELSE xs = 210

   all_files = files
   selected = BYTARR(N_ELEMENTS(files))

   ;********************************************************************
   ;** SET UP WIDGETS **************************************************

   IF (KEYWORD_SET(TITLE)) THEN atitle = title ELSE atitle = 'SELECT FILES'
   base = WIDGET_BASE(/COLUMN, TITLE=atitle, X_SCROLL_SIZE=xs+50, Y_SCROLL_SIZE= 610, /FRAME)

   done_button = WIDGET_BUTTON(base, VALUE=" DONE ", UVALUE=0)
   none_button = WIDGET_BUTTON(base, VALUE=" NONE ", UVALUE=2)

   IF (KEYWORD_SET(EXCLUSIVE)) THEN BEGIN 
     XMENU, all_files, base, /EXCLUSIVE, /COLUMN, UVALUE=all_files, $
     /FRAME, BASE=files_base, /SCROLL, X_SCROLL_SIZE=xs, Y_SCROLL_SIZE=2000
   ENDIF ELSE BEGIN 
     all_button = WIDGET_BUTTON(base, VALUE=" ALL ", UVALUE=1)
     XMENU, all_files, base, /NONEXCLUSIVE, /COLUMN, UVALUE=all_files, $
     /FRAME, BASE=files_base, /SCROLL, X_SCROLL_SIZE=xs, Y_SCROLL_SIZE=2000
;     all_button = WIDGET_BUTTON(base, VALUE=" ALL ", UVALUE=1)
   ENDELSE

;   none_button = WIDGET_BUTTON(base, VALUE=" NONE ", UVALUE=2)
;   done_button = WIDGET_BUTTON(base, VALUE=" DONE ", UVALUE=0)

   ;********************************************************************
   ;** REALIZE THE WIDGETS *********************************************

   WIDGET_CONTROL, /REAL, base

   XMANAGER, 'PICKFILES2', base, /MODAL

   inds =WHERE(selected EQ 1B)
   IF (inds(0) LT 0) THEN RETURN, ''
   RETURN, files(WHERE(selected EQ 1B))

END
