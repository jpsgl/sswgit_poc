;+
;$Id: keyword_value.pro,v 1.1.1.2 2004/07/01 21:19:03 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : KEYWORD_VALUE
;               
; Purpose     : Get the value for the keyword in a keyword=value array.
;               
; Use         : val= KEYWORD_VALUE(key_val_arr, keyword, ind, offset, /GET_ALL)
;    
; Inputs      : key_val_arr, keyword, ind, offset 
;
; Opt. Inputs : None
;               
; Outputs     : val 
;
; Opt. Outputs: None
;
; Keywords    : GET_ALL 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;
; $Log: keyword_value.pro,v $
; Revision 1.1.1.2  2004/07/01 21:19:03  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

FUNCTION KEYWORD_VALUE, key_val_arr, keyword, ind, offset, GET_ALL=GET_ALL

   IF NOT(KEYWORD_SET(offset)) THEN  offset = 0

   key = STRTRIM(STRUPCASE(keyword), 2)

   ind = WHERE(key_val_arr(offset:N_ELEMENTS(key_val_arr)-1).key EQ key)

   IF ( NOT(KEYWORD_SET(GET_ALL)) OR (N_ELEMENTS(ind) EQ 1) ) THEN BEGIN
      ind = ind(0)
      IF (ind GE 0) THEN $
         RETURN, key_val_arr(ind).val $
      ELSE $
         RETURN, ''
   ENDIF ELSE BEGIN	;** return values (reformatted) for all instances of keyword 
      num_matches = N_ELEMENTS(ind)
      result = key_val_arr(ind(0)).val
      result = STRMID(result,0,STRLEN(result)-1)	;** clip off the ")"
      result = result+','				;** add a ","
      FOR i=1, num_matches-1 DO BEGIN
         val = key_val_arr(ind(i)).val
         val = STRMID(val,1,STRLEN(val)-1)		;** clip off the "("
         IF (i LT (num_matches-1)) THEN BEGIN
            val = STRMID(val,0,STRLEN(val)-1)		;** clip off the ")"
            val = val+','				;** add a ","
         ENDIF
         result = result + val
      ENDFOR
      RETURN, result
   ENDELSE

END
