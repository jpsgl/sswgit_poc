;+
;$Id: int2str.pro,v 1.2 2005/01/24 17:56:33 esfand Exp $
;
; Project     : STEREO - SECCHI
;
; Name        : INT2STR
;
; Purpose     : This function changes an array of intergers to an array of strings using a 
;               specified width. This is useful for arrays of longer 1024 elements since
;               IDLs STRING function can only handle data arrays of upo 1024 elements when
;               using explicit formatting.
;
; Use         : strarr= INT2STR(intarr, width)
;
; Inputs      : select_list  Array of integers.
;               width        Width of converted integer to string.
;
; Opt. Inputs : None
;
; Outputs     : strarr       String array of converted integers (each string is width elements). 
;
; Opt. Outputs: None
;
; Keywords    : None
;
; Written by  : Ed Esfandiari, NRL, November 2004 - First Version.
;
; Modification History:
;
; $Log: int2str.pro,v $
; Revision 1.2  2005/01/24 17:56:33  esfand
; checkin changes since SCIP_A_TVAC
;
;
;-
;

FUNCTION INT2STR, data,width

  ; IDL limit to convert a data array (i.e integers) using STRING function and
  ; explicit FORMAT (i.e. FORMAT= '(I4.4)', etc) is 1024. For longer arrays must
  ; use this function instead.

  lval= 1000000000L
  len= STRLEN(STRTRIM(lval,2))
  RETURN, STRMID(STRTRIM(data+lval,2),len-width,width)
END


