;+
;$Id: read_sca.pro,v 1.7 2009/09/11 20:28:20 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : READ_SCA
;               
; Purpose     : Read the SpaceCraft Activity (.SCA) file for SpaceCraft A or B.
;               
; Use         : READ_SCA, sc, startdis, enddis, /DEFAULT
;    
; Inputs      : sc        Integer; 1= SpaceCraft A  2= SpaceCraft B.
;             : startdis  Start time of the schedule.
;               enddis    End time of the schedule. 
;
; Opt. Inputs : None
;               
; Outputs     : None
;
; Opt. Outputs: None
;
; Keywords    : DEFAULT   If set, use saved sca_fname or scb_fname (usually 
;                         DEFAULT.SCA) file. 
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/05/04 - Added SpaceCraft A or B dependency.
;              Ed Esfandiari 08/30/06 - Read RT rates from SCA and generate rt_rates structure.
;
; $Log: read_sca.pro,v $
; Revision 1.7  2009/09/11 20:28:20  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-


FUNCTION SCA_GET_TIMES, key_val_arr, kap_entry, NO_ENDTIME=NO_ENDTIME

COMMON KAP_INIT, kap_nrt_reserved, kap_resource, kap_resource_names, kap_dsn_names, kap_descriptions, $
                 kap_tlm_mode_names, kap_iie
COMMON DIALOG, mdiag,font

   key = 'STARTIME' & val = KEYWORD_VALUE(key_val_arr, key)
   IF (val EQ '') THEN BEGIN
      IF (TAG_NAMES(kap_entry, /STRUCTURE_NAME) EQ 'SCA_RESOURCE') THEN BEGIN
         WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(kap_resource_names(kap_entry.id))+'.  Ignoring Entry.' 
         PRINT,'%%%READ_SCA: '+key+' undefined for '+kap_resource_names(kap_entry.id)+'.  Ignoring Entry.' 
      ENDIF ELSE BEGIN 
         WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(TAG_NAMES(kap_entry,/STRUCTURE_NAME))+'.  Ignoring Entry.'
         PRINT,'%%%READ_SCA: '+key+' undefined for '+TAG_NAMES(kap_entry,/STRUCTURE_NAME)+'.  Ignoring Entry.'
      ENDELSE
      RETURN, -1
   ENDIF ELSE kap_entry.startime = UTC2TAI(STR2UTC(val))

   IF NOT(KEYWORD_SET(NO_ENDTIME)) THEN BEGIN
      key = 'ENDTIME' & val = KEYWORD_VALUE(key_val_arr, key)
      IF (val EQ '') THEN BEGIN
        IF (TAG_NAMES(kap_entry, /STRUCTURE_NAME) EQ 'SCA_RESOURCE') THEN BEGIN
         WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(kap_resource_names(kap_entry.id))+'.  Ignoring Entry.' 
         PRINT,'%%%READ_SCA: '+key+' undefined for '+kap_resource_names(kap_entry.id)+'.  Ignoring Entry.' 
        ENDIF ELSE BEGIN 
         WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(TAG_NAMES(kap_entry,/STRUCTURE_NAME))+'.  Ignoring Entry.'
         PRINT,'%%%READ_SCA: '+key+' undefined for '+TAG_NAMES(kap_entry,/STRUCTURE_NAME)+'.  Ignoring Entry.'
        ENDELSE
        RETURN, -1
      ENDIF ELSE kap_entry.endtime = UTC2TAI(STR2UTC(val))
   ENDIF

   RETURN, 1

END

;-------------------------------------------------------------------------------------

PRO ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource

COMMON KAP_INPUT, kap_nrt_reserved_arr, kap_resource_arr, kap_iie_arr

   IF (DATATYPE(kap_resource_arr) EQ 'INT') THEN $
      kap_resource_arr = tmp_kap_resource $
   ELSE $
      kap_resource_arr = [kap_resource_arr, tmp_kap_resource]
END

;-------------------------------------------------------------------------------------


PRO READ_SCA, sc, startdis, enddis, DEFAULT=default

COMMON KAP_INPUT, kap_nrt_reserved_arr, kap_resource_arr, kap_iie_arr
COMMON KAP_INIT

COMMON DIALOG, mdiag,font
COMMON SCA_NAMES, sca_fname, scb_fname
COMMON RT_CHANNEL, rt_rates, rtrate


   WIDGET_CONTROL, /HOUR

   IF NOT(SEXIST(kap_resource)) THEN SCA_INIT	;** define kap structures in common

   dir = GETENV('PT')+'/IN/SCA/'
   IF (sc EQ 1) THEN BEGIN
     dir = GETENV('PT')+'/IN/SCA/SC_A/'
     fn= sca_fname
   ENDIF
   IF (sc EQ 2) THEN BEGIN
     dir = GETENV('PT')+'/IN/SCA/SC_B/'
     fn= scb_fname
   ENDIF

   ;*************************************************
   ;** Try to find default file for startdis
   ;**
   utc = TAI2UTC(startdis, /EXTERNAL)
   dname = 'ECS'+STRN(utc.year)+STRN(utc.month,FORMAT='(I2.2)')+STRN(utc.day,FORMAT='(I2.2)')
 
   f_exist = FINDFILE(dir + dname + '???.SCA', count=count)
   IF (f_exist(0) EQ '') THEN $
      ;IF KEYWORD_SET(default) THEN fname =dir+'DEFAULT.SCA'  $ ;AEE 4/6/04 - SCA for SpaceCraft Anci./Activity file.
      IF KEYWORD_SET(default) THEN fname = fn  $ ;AEE 4/6/04 - SCA for SpaceCraft Anci./Activity file.
      ELSE fname = PICKFILE(PATH=dir, FILTER='*.SCA', /NOCONFIRM) $  ;AEE 4/6/04
   ELSE BEGIN 
      IF KEYWORD_SET(default) THEN fname = f_exist(count-1)  $;** don't even verify
      ELSE fname = PICKFILE(PATH=dir, FILTER='*.SCA', /NOCONFIRM, FILE=f_exist(count-1))  ; AEE 4/6/04
   ENDELSE

   IF (FNAME EQ '') THEN RETURN

   rt_rates=0 ; init to non-structure to remove existing rates, if any.
   rtrate= 0 ; will be set in schedule_plot and used by other routines instead of 
          ; using rt_channel.sav.

   IF (sc EQ 1) THEN sca_fname= fname
   IF (sc EQ 2) THEN scb_fname= fname 

   WIDGET_CONTROL,mdiag,SET_VALUE='Reading SCA file '+ fname  ; AEE 4/6/04
   PRINT, '%%%READ_SCA: Reading SCA file ', fname
   OPENR, kap_file, fname, /GET_LUN

   str = ''
   kap_resource_arr = 0
   kap_nrt_reserved_arr = 0
   kap_iie_arr = 0

   WIDGET_CONTROL, /HOUR
 
   ;** find first entry
   ;** an entry is a non-empty line that doesn't contain the "=" character and is not "END"
   WHILE ( NOT(EOF(kap_file)) AND ( (str EQ "END") OR (STRPOS(str,'=') GE 0) OR (str EQ '') ) ) DO BEGIN
      READF, kap_file, str
      str = STRTRIM(str, 2)
   ENDWHILE

   ;*************************************************
   ;** loop over each entry
   ;**
   WHILE (NOT(EOF(kap_file))) DO BEGIN

      ;** zero out this structure
      tmp_kap_resource = kap_resource
      tmp_kap_nrt_reserved = kap_nrt_reserved
      tmp_kap_iie = kap_iie

      ;** curr_str_arr is all the keyword/value pairs for this entry
      ;** the first element of curr_str_arr is the entry name
      curr_str_arr = str
 
      str = '='
      ;** find everything up to the next entry
      ;** an entry is a non-empty line that doesn't contain the "=" character
      WHILE ( NOT(EOF(kap_file)) AND ( (STRPOS(str,'=') GE 0) OR (str EQ '') ) ) DO BEGIN
         READF, kap_file, str
         str = STRTRIM(str, 2)
         ;** throw out comments and blank lines
         IF ( (STRMID(str,0,7) NE 'COMMENT') AND (str NE '') ) THEN curr_str_arr = [curr_str_arr, str]
      ENDWHILE
 
      ;** str is the next entry name
      IF NOT(EOF(kap_file)) THEN curr_str_arr = curr_str_arr(0:N_ELEMENTS(curr_str_arr)-2)

      entry = STRUPCASE(curr_str_arr(0))
      key_val_arr = STR_ARR2KEY_VAL(curr_str_arr(1:*))
      CASE (1) OF

   ;---------------------------------------------------------------------------------
   ;-- The following are resources from the SCA file
   ;-- They are stored in kap_resource_arr
   ;--
         ;** DSN_CONTACT
         (STRMID(entry,0,3) EQ 'DSN') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'DSN_CONTACT')))(0)
               tmp_kap_resource.type = (WHERE(kap_dsn_names EQ STRMID(entry,12,3)))(0)
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** SVM_RESERVED or PAYLOAD_RESERVED
         ( (STRMID(entry,0,3) EQ 'SVM') OR (STRMID(entry,0,7) EQ 'PAYLOAD') ) : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'RESERVED')))(0)
               IF (STRMID(entry,0,3) EQ 'SVM') THEN tmp_kap_resource.type = 0 ELSE tmp_kap_resource.type = 1
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** THROUGHPUT_RCR or THROUGHPUT_NORCR
         (STRMID(entry,0,10) EQ 'THROUGHPUT') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'THROUGHPUT')))(0)
               IF (STRMID(entry,11,3) EQ 'RCR') THEN tmp_kap_resource.type = 1 ELSE tmp_kap_resource.type = 0
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** SPACECRAFT_MANEUVER
         (entry EQ 'SPACECRAFT_MANEUVER') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'SPACECRAFT_MANEUVER')))(0)
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               key = 'NOTES' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(kap_resource_names(tmp_kap_resource.id)) 
                  PRINT,'%%%READ_SCA: '+key+' undefined for '+kap_resource_names(tmp_kap_resource.id) 
               ENDIF ELSE $ 
                  tmp_kap_resource.type = GET_SCA_DESCRIPTION(val)
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** CLOCK_ADJUST
         (entry EQ 'CLOCK_ADJUST') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'CLOCK_ADJUST')))(0)
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource, /NO_ENDTIME) LT 0) THEN GOTO, SKIP
               key = 'TYPE' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN 
                  WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(kap_resource_names(tmp_kap_resource.id))
                  PRINT,'%%%READ_SCA: '+key+' undefined for '+kap_resource_names(tmp_kap_resource.id) 
               ENDIF ELSE $
                  tmp_kap_resource.type = GET_SCA_DESCRIPTION(val)
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** TLM_TAPE_DUMP
         (entry EQ 'TLM_TAPE_DUMP') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'TLM_TAPE_DUMP')))(0)
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource

                rtind= WHERE(key_val_arr.key EQ 'RT_RATE', rtcnt)
                IF (rtcnt GT 0) THEN $
                  rt_rate= FLOAT(key_val_arr(rtind(0)).val) $
                ELSE $
                  rt_rate= 3.6 ; kbps (default)
                rt_st= {startime: tmp_kap_resource.startime, $
                           endtime: tmp_kap_resource.endtime,    $
                           rt_rate: rt_rate}
		IF (datatype(rt_rates) NE 'STC') THEN $ 
                  rt_rates= rt_st $
                ELSE $
                  rt_rates= [rt_rates,rt_st] 
            END

         ;** TLM_MDI
         (STRMID(entry,0,7) EQ 'TLM_MDI') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'TLM_MDI')))(0)
               IF (STRMID(entry,8,1) EQ 'M') THEN tmp_kap_resource.type = 0 ELSE tmp_kap_resource.type = 1
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** TLM_HR_IDLE
         (entry EQ 'TLM_HR_IDLE') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'TLM_HR_IDLE')))(0)
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** TLM_MODE
         (entry EQ 'TLM_MODE') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'TLM_MODE')))(0)
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource, /NO_ENDTIME) LT 0) THEN GOTO, SKIP
               key = 'MODE' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(kap_resource_names(tmp_kap_resource.id))
                  PRINT,'%%%READ_SCA: '+key+' undefined for '+kap_resource_names(tmp_kap_resource.id)
                  GOTO, SKIP
               ENDIF
               ind = WHERE(kap_tlm_mode_names EQ STRUPCASE(val))
               IF (ind(0) LT 0) THEN BEGIN
                  WIDGET_CONTROL,mdiag,SET_VALUE='Unknown TLM_MODE: '+STRN(val)+' Ignoring entry.'
                  PRINT,'%%%READ_SCA: Unknown TLM_MODE: '+val+' Ignoring entry.'
                  GOTO, SKIP
               ENDIF
               tmp_kap_resource.type = ind(0)
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

         ;** TLM_SUBMODE
         (STRMID(entry,0,11) EQ 'TLM_SUBMODE') : BEGIN
               tmp_kap_resource.id = (BYTE(WHERE(kap_resource_names EQ 'TLM_SUBMODE')))(0)
               tmp_kap_resource.type = FIX(STRMID(entry,12,1))
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_resource, /NO_ENDTIME) LT 0) THEN GOTO, SKIP
               ADD_TO_SCA_RESOURCE_ARR, tmp_kap_resource
            END

   ;---------------------------------------------------------------------------------
   ;-- Store instrument NRT reserved commanding in kap_nrt_reserved_arr
   ;--
         ;** INST_NRT_RESERVED
         (entry EQ 'INST_NRT_RESERVED') : BEGIN
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_nrt_reserved) LT 0) THEN GOTO, SKIP
               key = 'INSTRUME' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                 WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(entry)
                 PRINT,'%%%READ_SCA: '+key+' undefined for '+entry 
               ENDIF ELSE tmp_kap_nrt_reserved.instrume = val
               key = 'CMD_RATE' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                 WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(entry)
                 PRINT,'%%%READ_SCA: '+key+' undefined for '+entry 
               ENDIF ELSE tmp_kap_nrt_reserved.cmd_rate = FIX(val)
               key = 'STATUS' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                 WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(entry)
                 PRINT,'%%%READ_SCA: '+key+' undefined for '+entry 
               ENDIF ELSE tmp_kap_nrt_reserved.status = val
               IF (DATATYPE(kap_nrt_reserved_arr) EQ 'INT') THEN $
                  kap_nrt_reserved_arr = tmp_kap_nrt_reserved $
               ELSE $
                  kap_nrt_reserved_arr = [kap_nrt_reserved_arr, tmp_kap_nrt_reserved]
            END

   ;---------------------------------------------------------------------------------
   ;-- Store IIE Master in kap_iie_arr  (ignore receiver for now)
   ;--
         ;** INST_IIE_MASTER
         (entry EQ 'INST_IIE_MASTER') : BEGIN
               IF (SCA_GET_TIMES(key_val_arr, tmp_kap_iie) LT 0) THEN GOTO, SKIP
               key = 'INSTRUME' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                 WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(entry)
                 PRINT,'%%%READ_SCA: '+key+' undefined for '+entry 
               ENDIF ELSE tmp_kap_iie.instrume = val
               key = 'MSTR_TYPE' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                 WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(entry)
                 PRINT,'%%%READ_SCA: '+key+' undefined for '+entry 
               ENDIF ELSE tmp_kap_iie.type = FIX(val)
               key = 'STATUS' & val = KEYWORD_VALUE(key_val_arr, key)
               IF (val EQ '') THEN BEGIN
                 WIDGET_CONTROL,mdiag,SET_VALUE=STRN(key)+' undefined for '+STRN(entry)
                 PRINT,'%%%READ_SCA: '+key+' undefined for '+entry 
               ENDIF ELSE tmp_kap_iie.status = val
               IF (DATATYPE(kap_iie_arr) EQ 'INT') THEN $
                  kap_iie_arr = tmp_kap_iie $
               ELSE $
                  kap_iie_arr = [kap_iie_arr, tmp_kap_iie]
            END

         ELSE : BEGIN
               IDGET_CONTROL,mdiag,SET_VALUE='Ignoring Unknown entry: '+STRN(entry)
               PRINT, "%%%READ_SCA: Ignoring Unknown entry: ", entry
               GOTO, SKIP
            END
      ENDCASE

      SKIP:

   ENDWHILE
 



   CLOSE, kap_file
   FREE_LUN, kap_file
   ;WIDGET_CONTROL,mdiag,SET_VALUE='READ_SCA: Done.'
   WIDGET_CONTROL,mdiag,SET_VALUE='READ_SCA: Done.' ; AEE 4/6/04
   ;PRINT, '%%%READ_SCA: Done.'
   PRINT, '%%%READ_SCA: Done.' ;  AEE 4/6/04

   ;**********************
   ;** set day to startdis
   ;today = TAI2UTC(startdis)
   ;utc = TAI2UTC(kap_resource_arr.startime)
   ;utc.mjd = today.mjd
   ;kap_resource_arr.startime = UTC2TAI(utc)
   ;utc = TAI2UTC(kap_resource_arr.endtime)
   ;utc.mjd = today.mjd
   ;kap_resource_arr.endtime = UTC2TAI(utc)

;  help,/st,tmp_kap_resource, rt_rates

   IF (DATATYPE(kap_nrt_reserved_arr) EQ 'STC') THEN BEGIN
      utc = TAI2UTC(kap_nrt_reserved_arr.startime)
      utc.mjd = today.mjd
      kap_nrt_reserved_arr.startime = UTC2TAI(utc)
      utc = TAI2UTC(kap_nrt_reserved_arr.endtime)
      utc.mjd = today.mjd
      kap_nrt_reserved_arr.endtime = UTC2TAI(utc)
   ENDIF
   IF (DATATYPE(kap_iie_arr) EQ 'STC') THEN BEGIN
      utc = TAI2UTC(kap_iie_arr.startime)
      utc.mjd = today.mjd
      kap_iie_arr.startime = UTC2TAI(utc)
      utc = TAI2UTC(kap_iie_arr.endtime)
      utc.mjd = today.mjd
      kap_iie_arr.endtime = UTC2TAI(utc)
   ENDIF


END
