;+
;$Id: get_ip_time.pro,v 1.9 2013/01/07 19:36:18 nathan Exp $
; Project     : STEREO - SECCHI
;
; Name        : GET_IP_TIME 
;
; Purpose     : Returns the image processing time for a given image.
;
; Use         : ip_time = READ_IP_TIME()
;
; Inputs      : os      Observation structure for image.
;
; Opt. Inputs : None
;
; Outputs     : ip_time Time in seconds to process the image.
;
; Opt. Outputs: None
;
; Keywords    : Separate - IF not set, return max of proc times for multi apids. If set, 
;                          returns array of proc times with a separate time for each apid.
;
; Prev. Hist. : None
;
; Written by  : Ed Esfandiari, NRL, Jan 2005 - First Version.
;
; Modification History:
;              Ed Esfandiari 10/06/05 Added code to handle IP functions 72 and 73.
;              Ed Esfandiari 11/21/05 Added checks for steps 114 and 115.
;              Ed Esfandiari 05/08/06 Modified to better estimate single and multi-apid 
;                                     proc times.
;              Ed Esfandiari 08/14/09  Changed H-compress range from 5-17 to 5-14.
;              Ed Esfandiari 08/26/09  Changed H-compress range from 5-17 to 5-15. Treat HISAM as no-compression.
;
; $Log: get_ip_time.pro,v $
; Revision 1.9  2013/01/07 19:36:18  nathan
; print ip_time of OS
;
; Revision 1.8  2009/09/11 20:28:12  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/12/16 14:58:52  esfand
; Commit as of 12/16/05
;
; Revision 1.2  2005/10/17 19:02:10  esfand
; Added Icer+Mission Sim call. roll Synoptic 20060724000
;
; Revision 1.1  2005/03/10 16:44:17  esfand
; first version
;
;
;-
;
FUNCTION GET_IP_TIME, os, separate=separate

  ;ip_time= 0.0

  ip = os.ip(os.iptable)
  good = WHERE(ip.steps GE 0, nip)
  IF (nip GT 0) THEN BEGIN
    pixels= OS_GET_NUM_PIXELS(os, /ccd_ro_pixels) 
    ipfuncs=   ip.steps(good) 
    ip_info= READ_IP_DAT()
    ohtimes= ip_info(ipfuncs).ip_oht ; overhead times for each funtion
    extimes= ip_info(ipfuncs).ip_ext ; execution times for each function
    ;fz_pix= 2048.0^2.0   ; number of pixels in a full size image
    fz_pix= 2176.0 * 2050.0   ; number of pixels in a full size image
    sz_ratio= pixels(0)/fz_pix

    ;; add function times for all but the last (compression used):
    ;ip_time= TOTAL(ohtimes(0:nip-2)*sz_ratio + extimes(0:nip-2)*sz_ratio)

   ; add function times for all but the compression(s) used:

   ;comp_ind = WHERE(ipfuncs GE 5  AND ipfuncs LE 17 OR $
   ;comp_ind = WHERE(ipfuncs GE 5  AND ipfuncs LE 14 OR $
   comp_ind = WHERE(ipfuncs GE 5  AND ipfuncs LE 15 OR $
                    ipfuncs GE 90 AND ipfuncs LE 101 OR $
                    ipfuncs EQ 44, comp_cnt)

;print,ipfuncs
;help,comp_cnt

   ip_time= FLTARR(comp_cnt)

   ;oht= ohtimes
   ;oht(comp_ind)= 0.0
   ;ext= extimes
   ;ext(comp_ind)= 0.0
   ;ip_time= TOTAL(oht(0:nip-1)*sz_ratio + ext(0:nip-1)*sz_ratio)


   j= 0 
   FOR ci= 0, comp_cnt-1 DO BEGIN
    ip_time(ci)= TOTAL(ohtimes(j:comp_ind(ci)-1)*sz_ratio + extimes(j:comp_ind(ci)-1)*sz_ratio)
    j= comp_ind(ci)+1
   ENDFOR

   ; Now add compression(s) time separately since pixel summings, masking, 
   ; ect. will affect the compression time:

   ;pixels= OS_GET_NUM_PIXELS(os)
   pixels= OS_GET_NUM_PIXELS(os, /no_compress)

   comp_ind= [-1,comp_ind]
   
   FOR i= 1, comp_cnt DO BEGIN  ; comp_cnt should be same as N_ELEMENTS(pixels).

    ; IF summing buffer (32 bits/pix) is used, it increases compression time
    ; so check and adjust for it:
   
    funcs= ipfuncs(comp_ind(i-1)+1:comp_ind(i)) 
    ctime= extimes(comp_ind(i)) 
    coht=  ohtimes(comp_ind(i)) 

    ;tmp= WHERE(funcs EQ 34 OR funcs EQ 36 OR funcs EQ 37 OR funcs EQ 38, sum_buf) 
    tmp= WHERE(funcs EQ 34 OR funcs EQ 36 OR funcs EQ 37 OR funcs EQ 38 OR $ 
               funcs EQ 114 OR funcs EQ  115, sum_buf) 
    IF (sum_buf GT 0) THEN ctime= ctime*2  ; 32bit/pixel so multiply comp.time by 2

    ; But, if also an HI summing buffer to 16 bit (from 32), functions 72 (?d) or 73 (?e),
    ; is used, it reduces image size by 2 since 32 bits/pixel image becomes 16 bits/pixel
    ; so compression time is cut in half:

    tmp= WHERE(funcs EQ 72 OR funcs EQ 73, sum_buf)
    IF (sum_buf GT 0) THEN ctime= ctime/2.0 

    sz_ratio= pixels(i-1)/fz_pix
    ;ip_time= ip_time + coht*sz_ratio + ctime*sz_ratio
    ip_time(i-1)= ip_time(i-1) + coht*sz_ratio + ctime*sz_ratio

   ENDFOR
  ENDIF ELSE BEGIN
    PRINT,'ERROR: No functions/steps in selected IP table.'
    STOP
  ENDELSE

  IF NOT (KEYWORD_SET(separate)) THEN ip_time= MAX(ip_time)

print,'OS_'+trim(os.os_num)+' ip_time= '+trim(ip_time)
  RETURN, ip_time
END

