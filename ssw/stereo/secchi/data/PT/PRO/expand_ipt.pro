
; Ed Esfandiari Dec 2003  
; This function expands an input ipt file if it includes lp=7 (block sequence) entries.
; The expanded file (ipt_file_name.EXPAND.todays_elapsed_seconds) is the same as the
; input IPT file execpt that lp=7 sections are replaced with the real os_num sections
; that lp=7 corresponds to.
;+
;$Id: expand_ipt.pro,v 1.6 2009/09/11 20:28:08 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : EXPAND_IPT
;               
; Purpose     : This function expands an input ipt file if it includes lp=7 (block sequence)
;               entries. The expanded file (ipt_file_name.EXPAND.todays_elapsed_seconds) is 
;               the same as the input IPT file execpt that lp=7 sections are replaced with 
;               the actual os_num sections that lp=7 corresponds to.
;               
; Use         : expanded_ipt_file= EXPAND_IPT(ipt_file_name) 
;    
; Inputs      : ipt_file_name      .IPT file used by the schedule.
;
; Opt. Inputs : None
;               
; Outputs     : expanded_ipt_file  An expanded .IPT file that will be used by the schedule
;                                  and removed when no longer needed. 
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 10/06/04 - Added SpaceCraft, sc, dependency.
;              Ed Esfandiari 10/15/04 - Added SC A/B bsf counters.
;
; $Log: expand_ipt.pro,v $
; Revision 1.6  2009/09/11 20:28:08  esfand
; use 3-digit decimals to display volumes
;
; Revision 1.3  2005/01/24 17:56:32  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.1.1.2  2004/07/01 21:18:59  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-
 
FUNCTION EXPAND_IPT,ipt_file_name

COMMON BSF_CNT, a_bsf_cnt, b_bsf_cnt

   ;OPENR, ipt_file, GETENV('PT')+'/IO/IPT/'+ipt_file_name, /GET_LUN
   OPENR, ipt_file, ipt_file_name, /GET_LUN

   ; Check to see if IPT file contains any Block Seq entreis (LP=7):

   bseq= 0
   str = ''
   WHILE ( NOT(EOF(ipt_file)) AND NOT bseq) DO BEGIN
     READF, ipt_file, str
     IF (STRCOMPRESS(str, /REMOVE_ALL) EQ 'PT_LP_NUM=7') THEN bseq= 1
   ENDWHILE


   IF (bseq) THEN BEGIN
     GET_UTC, utc
     expand_file= ipt_file_name+'.EXPAND.'+STRTRIM(STRING(utc.time),2)
     ;OPENW, expand_ipt, GETENV('PT')+'/IO/IPT/'+expand_file , /GET_LUN
     OPENW, expand_ipt,  expand_file , /GET_LUN
     PRINT,'EXPAND_IPT: Creating '+expand_file

     POINT_LUN,ipt_file,0  ; reset the pointer to the begining of the file.
     WHILE (NOT(EOF(ipt_file))) DO BEGIN
       READF, ipt_file, str
       another_lp7:  ; expand another lp=7 section if it comes right after another lp=7 section.
       IF (STRCOMPRESS(str, /REMOVE_ALL) EQ 'PT_OS_NUM=0000') THEN BEGIN
         READF, ipt_file, str ; str should contain 'PT_LP_NUM = 7'
         curr_str_arr = ''  
         WHILE ( NOT(EOF(ipt_file)) AND (STRMID(str,0,9) NE "PT_OS_NUM") ) DO BEGIN
           str = STRTRIM(str, 2)
           ;** throw out comments and blank lines now
           IF ( (STRMID(str,0,1) NE ';') AND (str NE '') ) THEN curr_str_arr = [curr_str_arr, str]
           READF, ipt_file, str
         ENDWHILE
         ;** Throw away 1st string ('') and 2nd string ('PT_LP_NUM = 7') and keep the rest (3 strings
         ;   which are PT_BSFILE and PT_LP_START and PT_LP_SC):
         curr_str_arr = curr_str_arr(2:N_ELEMENTS(curr_str_arr)-1)
         ;** convert this to an array of keyword/value structures
         key_val_arr = STR_ARR2KEY_VAL(curr_str_arr)
         dt= key_val_arr(1).val ; the 'PT_LP_START = (yyyymmdd_hhmmss)' line.
         dt= STRMID(dt,1,4)+'/'+STRMID(dt,5,2)+'/'+STRMID(dt,7,2)+' '+ $
             STRMID(dt,10,2)+':'+STRMID(dt,12,2)+':'+STRMID(dt,14,2)

         sc_id= key_val_arr(2).val
         IF (sc_id EQ 'A') THEN a_bsf_cnt= a_bsf_cnt + 1 
         IF (sc_id EQ 'B') THEN b_bsf_cnt= b_bsf_cnt + 1 
         
         ADD_BLK_SEQ_IPT, expand_ipt, key_val_arr(0).val, dt, sc_id 

       ENDIF

       IF (STRCOMPRESS(str, /REMOVE_ALL) NE 'PT_OS_NUM=0000') THEN $
         PRINTF, expand_ipt, str $
       ELSE $
         GOTO, another_lp7  ; another lp=7 section is found right after the just expanded one.

     ENDWHILE 
     CLOSE, expand_ipt 
     FREE_LUN, expand_ipt 
   END

   CLOSE, ipt_file
   FREE_LUN, ipt_file

   IF(bseq) THEN $
     RETURN,expand_file $
   ELSE $
     RETURN,''  

END

