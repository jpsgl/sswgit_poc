pro set_inputs
;+
;$Id: set_inputs.pro,v 1.1 2005/01/18 17:59:02 nathan Exp $
;
; Project     : STEREO - SECCHI PT
;                   
; Name        : set_inputs.inc
;               
; Purpose     : Initialize quantitities formerly held in save sets. Takes the place of the 
;		following savesets: 
;***exposure_cnvt_factor.sav***
;***mech_times.sav***
;***launched_date.sav***
;***ccd_size.sav***
;***op_types.sav***
;***secchi_downlink_channels.sav***
;***rt_channel.sav***
;
; FUTURE: dbmodes.sav
;
; Use         : set_inputs
;    
; Inputs      : None
;
; Opt. Inputs : None
;               
; Outputs     : structure 'inputs' in COMMON os_defined
;
; Written by  : Nathan Rich, NRL/I2, 2004.09.03
;
; $Log: set_inputs.pro,v $
; Revision 1.1  2005/01/18 17:59:02  nathan
; takes the place of set_inputs.inc
;
;-
COMMON OS_DEFINED, defined_os_arr, os_instance, inputs

inputs = { $
;***exposure_cnvt_factor.sav***
;(no longer used)
;
;os_get_pre_proc_time.pro:
;   filename= GETENV('PT')+'/IN/OTHER/'+'exposure_cnvt_factors.sav'
;   RESTORE,filename ;=> HI_EXP_FACTOR=1000.0 and SCIP_EXP_FACTOR=1024.0
;   IF (tele LE 2) THEN $ ; SCIP
;     exptime = exptime/SCIP_EXP_FACTOR $
;   ELSE $ ; HI
;     exptime = exptime/HI_EXP_FACTOR

hi_exp_factor:	1000.	,$
scip_exp_factor: 1024.	,$

;..........................................
;***mech_times.sav***
;get_pre_exp_times.pro, os_get_pre_proc_time.pro
;
;   RESTORE, FILENAME= GETENV('PT')+'/IN/OTHER/'+'mech_times.sav' 
   ; => filter_time,polar_time,shutter_time (all units of sec).
;filter_time:	1.5	; 1 revolution
;polar_time:	0.75	; 1/2 revolution
shutter_time:	0.05 ,$	; 1/3 revolution
;qs_time:	0.15	; 1 revolution
; times in seconds

; **** NOTE: I recommend using motortba.img for these values instead. ****
filter_time:	0   ,$
polar_time: 	2   ,$	
qs_time:    	1.5 ,$

; COR1/2 mechanism revolution (spin) time values from acceptance testing in 
; July,August,November 2003
;c1shspintim=0.152
;c2shspintim=0.153
;c1polspintim=1.5
;c2polspintim=1.11
;
; EUVI mech revolution (spin) times are from EUVI A functional test on 
; March 19, 2004
;eshutspintim=0.11
;equadspintim=0.15
;efwspintim=1.01

; Use max value for all?

; Recommend using full revolution time for all FW, POL, and QS motions as a
; reasonable approximation. I don't know quite how to charactarize acceleration 
; and deceleration time for different distances moved, though I could try from 
; TDS logs.

;..........................................
;***launched_date.sav***
;expand_oses.pro, schedule_event.pro
;
;schedule_event.pro:
;                filename= GETENV('PT')+'/IN/OTHER/'+'launched_date.sav'
;                RESTORE,filename ; => launch_date = 2004 (int) (for now)
;                GENERATE_ITOS_SCHEDULE,ipt_name,launch_date      ; AEE 1/27/04 

launch_date:	2004	,$
;..........................................
;***ccd_size.sav***
;fill_in_lp.pro, generate_ipt.pro, generate_ipt_set.pro, mask.pro, mask_runs2arr.pro, mask_tool.pro, ;os_get_num_pixels.pro, os_init.pro, os_summary.pro, os_tables.pro, plot_ccd.pro, read_mask_tables.pro
;
;os_get_num_pixels.pro:
;   RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ; => xyblks  = 34 (blocks for a ccd size of 34*34=1156)
                                                 ; => xyblkpix= 64 (a block is 64*64 pixels)
                                                 ; => xypix   = 2176 (num pixels in x or y direction)
                                                 ; => xstart  = 1 (in cam tables x-ragne = 1-2176)
                                                 ; => ystart  = 1 (in cam tables y-ragne = 1-2176)
xyblks:	34	,$
xyblkpix:	64	,$
xypix:	2176	,$
xstart:	1	,$
ystart:	1	,$

;............................................
;***op_types.sav***
;os_init.pro
;
;os_init.pro:
;   ; Note: Order of op_types should match those of op_pro in define_os.pro:
;   filename= GETENV('PT')+'/IN/OTHER/'+'op_types.sav'
;   RESTORE,FILENAME= filename ; => op_types, above
;OP_TYPES        STRING    = Array[8]
;IDL[calliope]>for i=0,7 do print,op_types[i]
;Sky Double
;Sky Normal
;Dark
;Cal LED
;Continuous
;SCIP Seq
;HI Seq
;Block Seq

op_types:	['Sky Double', 'Sky Normal', 'Dark', 'Cal LED', 'Continuous', 'SCIP Seq', 'HI Seq', 'Block Seq']	,$

;............................................
;***secchi_downlink_channels.sav***
;schedule_plot.pro, os_summary.pro, os_summary_text.pro, schedule_convert_units.pro
;
;schedule_plot.pro:
;        ;RESTORE,'secchi_downlink_channels.sav' ; AEE 6/16/03
;        filename= GETENV('PT')+'/IN/OTHER/'+'secchi_downlink_channels.sav'
;        RESTORE, filename
           ;=> CHANNELS.
              ;SB_CAPACITY     FLOAT           32.0        ; Mega Bytes  (Secchi-Buffer RAM disk)
              ;SB_RATE         FLOAT           150000.0    ; bits/sec  (it is really 153 kbs but used 150 kbs)
              ;SSR1_CAPACITY   FLOAT           645.000     ; Mega Bytes 
              ;SSR1_RATE       FLOAT           42000.0     ; bits/sec
              ;SSR2_CAPACITY   FLOAT           161.000     ; Mega Bytes
              ;SSR2_RATE       FLOAT           42000.0     ; bits/sec
              ;RT_CAPACITY     FLOAT           250.000     ; Mega Bytes (page 29)
              ;RT_RATE         FLOAT           12000.0     ; bits/sec  (page32 MissionOps. of GS CDR PR)
              ;SW_CAPACITY     FLOAT           12.5000     ; Mega Bytes (page 29)
              ;SW_RATE         FLOAT           500.000     ; bits/sec  (page32 MissionOps. of GS CDR PR
;IDL[calliope]>help,/str,channels
;** Structure CHANNELS, 8 tags, length=32:
;   SB_CAPACITY     FLOAT           64.0000
;   SB_RATE         FLOAT           150000.
;   SSR1_CAPACITY   FLOAT           400.000
;   SSR1_RATE       FLOAT           320000.
;   SSR2_CAPACITY   FLOAT           100.000
;   SSR2_RATE       FLOAT           320000.
;   SW_CAPACITY     FLOAT           12.5000
;   SW_RATE         FLOAT           500.000
channels:	{ dwnl_channels, $ 
SB_CAPACITY:	64.0000	,$
SB_RATE:	150000.	,$
SSR1_CAPACITY:	400.000	,$
SSR1_RATE:	320000.	,$
SSR2_CAPACITY:	100.000	,$
SSR2_RATE:	320000.	,$
SW_CAPACITY:	12.5000	,$
SW_RATE:	500.000	},$

;............................................
;***rt_channel.sav***
;os_summary.pro, os_summary_text.pro, schedule_convert_units.pro, schedule_plot.pro
;
;schedule_plot.pro:
        ; AEE 4/7/04: RT-channel can only hold 1 packet (272 bytes or ~0.3KB) at a time:
;        RESTORE,GETENV('PT')+'/IN/OTHER/'+'rt_channel.sav'
         ; =>
           ; rt_rate= 50        Note: this value, (packets/sec), can change daily and should be read in
                        ;       from a file.
                        ;       This is also true for schedule_convert_units.pro and other routines 
                        ;       that may use secchi_downlink_channels.sav file.
           ; rt_capacity= 1     1 packet

rt_rate:	50	,$
rt_capacity:	1	}

; TBD:
;............................................
;***dbmodes.sav*** 
;get_observing_mode.pro
;............................................
; this structure contains processing time
;
;   IF (DATATYPE(dbmodes) NE 'STC') THEN BEGIN
;      filename= GETENV('PT')+'/IN/OTHER/'+'dbmodes.sav'
;      RESTORE, filename 
;   ENDIF
;
;IDL[calliope]>help,dbmodes
;DBMODES         STRUCT    = -> OBSERVING_MODES Array[697]
;IDL[calliope]>help,/str,dbmodes
;** Structure OBSERVING_MODES, 11 tags, length=68:
;   DB_NAME         STRING    'lasco'
;   TABLE_NAME      STRING    'observing_modes'
;   DATE_MOD        STRING    ''
;   TELESCOPE       INT              1
;   TEL_STATE_STR   STRING    '10000'
;   CAM_STATE_STR   STRING    '0002000211010'
;   COMPRESSION_STR STRING    'XZR'
;   AVG_PROC_TIME   FLOAT           0.00000
;   AVG_COMP_FACTOR FLOAT           4.28905
;   AVG_BITS_PIXEL  FLOAT           3.73065
;   NUM_IMAGES      LONG                 2;
;
;   obs_mode.avg_bits_pixel = 16 
;	; AEE - Dec 19, 02 - 16 bits/pixel for all SECCHI CCDs (according to D.Wang)
;   obs_mode.avg_proc_time =  5.5 
;	;AEE - Dec 19, 02 - use 5.5 sec/Mpixel for SECCHI (it is for lossless comp
;                                ; ression but use it for all compressions for now.
;   obs_mode.avg_proc_time =  obs_mode.avg_proc_time / 1000.0 
;	; change to milliseconds per pixel
;
; See get_observing_modes.pro for explanations.

end

