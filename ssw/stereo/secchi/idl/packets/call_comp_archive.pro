;                   
; Name      :call_comp_archive.pro
;               
; Purpose   : calls comp_archive.pro in pipeline processing
;
; Explanation:
;               
; Use       : IDL> 
;    
; Inputs    : 	sc = spacecraft A or B, yyyymmdd from pipeline processing 
;
; Outputs   :  calls comp_archive.pro for days current day processing -3, -9, and -39
;
; Optional Outputs: 
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database sci apid's
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Aug 07
;               
; $Log: call_comp_archive.pro,v $
; Revision 1.3  2012/12/10 15:54:13  nathan
; check 1 and 2 days before
;
; Revision 1.2  2008/06/06 14:33:23  mcnutt
; added 9 to minus day list now 3,6,9, and 30
;
; Revision 1.1  2007/08/24 18:08:02  mcnutt
; New
;
;
pro call_comp_archive,sc,yyyymmdd

yr=strmid(yyyymmdd,0,4) & mm=strmid(yyyymmdd,4,2) & dd=strmid(yyyymmdd,6,2)
doday=utc2doy(str2utc(yr+'-'+mm+'-'+dd))

tmp=doy2utc(doday,yr)
;check days n [-3,-9,-39]
dnimus=[1,2,6,9,30]
for n=0,n_elements(dnimus)-1 do begin
   tmp.mjd=tmp.mjd-dnimus(n)
   doy=mjd2doy(tmp)
   if(doy gt doday) then yr2=fix(yr-1) else yr2=fix(yr)
   print,'Comparing Archive files for '+string(doy,'(i3.3)'),' ',string(yr2,'(i4.4)')
   comp_archive,sc,yr2,doy
endfor
end
