
;+
; $Id: monthly_apid_report.pro,v 1.15 2012/12/27 18:02:13 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : monthly_apid_report.pro
;               
; Purpose   : creates a monthly text report and monthly PNG plot of missing SCI packets .
;               
; Explanation: Automatically finds files in $sccb or $scca directory based on lzpb input
;               
; Use       : IDL> 
;    
; Inputs    : 	sc [spacecraft A or B], yyyy [four digit year] , mm [2 digit month]  , lzpb ['lz' , 'pb'  or 'mon']
;
; Outputs   :  monthly text report and monthly pdf plot of missing SCI packets.
;
; Optional Outputs:	
;
; Keywords  : 
;   /PIPELINE	Save PNG files in $SSW_SECCHI/data, else use outpdir
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database sci apid's
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Apr 06
;               
; $Log: monthly_apid_report.pro,v $
; Revision 1.15  2012/12/27 18:02:13  mcnutt
; changed output and input directory
;
; Revision 1.14  2012/05/22 22:09:30  secchia
; nr - by default do IO in $HOME not $sccx
;
; Revision 1.13  2011/06/07 15:45:14  secchib
; nr - fix case where gap crosses month boundary
;
; Revision 1.12  2011/04/19 19:22:15  nathan
; determine where PNGs saved with /pipeline
;
; Revision 1.11  2010/02/01 21:11:00  secchib
; nr - increase init size of strarrs
;
; Revision 1.10  2009/09/22 18:00:27  secchia
; delete previous allseqerror report files, creates png not pdf output and hardcode outpdir
;
; Revision 1.9  2009/09/04 11:31:38  secchib
; added _extra keyword
;
; Revision 1.8  2008/03/11 16:51:29  mcnutt
; sets days and hours to -1 if no gaps are found for the month
;
; Revision 1.7  2008/01/29 15:46:15  mcnutt
; increased the size of xxxa variables
;
; Revision 1.6  2008/01/10 11:50:47  secchib
; removed stops
;
; Revision 1.5  2008/01/09 16:58:35  mcnutt
; corrected to file last gap which has been missing from the get go
;
; Revision 1.4  2008/01/08 14:00:18  secchib
; new year correction
;
; Revision 1.3  2007/12/07 13:24:16  secchia
; does not create a report until first missing packet is found
;
; Revision 1.2  2007/08/08 16:53:14  mcnutt
; commented out all apid report
;
; Revision 1.1  2007/08/08 16:40:35  mcnutt
; moved from dev/packets
;


pro monthly_apid_report,sc,yyyy,mm,lzpb,outpdir,PIPELINE=pipeline, USEDIR=usedir, _extra=_extra

;device,decomposed=0
tek_color
device,retain=2
IF keyword_set(USEDIR) THEN inpdir=usedir ELSE $
;inpdir=getenv('scc'+strlowcase(sc))+'/nrl/'+lzpb+'/outputs/prints/' 
inpdir='/net/calliope/data/secchi/'+strlowcase(sc)+'/lzsci/pcktcnt/daily/'
;outpdir=inpdir

;--get day numbers of input month
yyyy=string(yyyy,'(i4.4)') & mm=string(mm,'(i2.2)')
doy0=utc2doy(str2utc(yyyy+'-'+mm+'-01'))
month=fix(mm) & yr=fix(yyyy)
IF month EQ 12 THEN BEGIN
        doy1=utc2doy(str2utc(yyyy+'-'+mm+'-31'))
ENDIF ELSE BEGIN
	mm1s=string(month+1,'(i2.2)')
        doy1=utc2doy(str2utc(yyyy+'-'+mm1s+'-01'))-1
ENDELSE

print
print,'Doing report for ',yyyy,'-',mm
help,doy0,doy1
print

apids=strarr(200000)
dofgap=strarr(200000)
sofgap=strarr(200000)
logap=strarr(200000)
npckts=strarr(200000)
x1=' ' & x2=' ' & x3=' ' & x4=' ' & line=' ' & d=0l
x5=' ' & x6=' ' & x6=' ' & x8=' ' & x9=' '
close,1
for nd=doy0-1,doy1 do begin
   nd1=nd & yr1=yr
   if doy0 eq 1 and mm eq 1 and nd eq 0 then begin
      yr1 = yr-1
      nd1 = 365+leap_year(yr1)
   endif  
   afile=findfile(inpdir+'allsequenceerrors_'+strupcase(sc)+'_'+string(yr1,'(i4.4)')+'_'+string(nd1,'(i3.3)')+'*.rpt')
   afile=afile(0)
   help,d
   print,'Reading ',afile
   if (afile ne '') then begin
     openr,1,afile
     readf,1,line  ;get past header
     readf,1,line
     readf,1,line
     readf,1,line
     while (not eof(1)) do begin
	readf,1,line
        tmp=strsplit(line)
        tmp=[tmp,strlen(line)]
           apids(d)=strmid(line,tmp(0),tmp(1)-tmp(0))
    	   dofgap(d)=strmid(line,tmp(1),tmp(2)-tmp(1))
           sofgap(d)=strmid(line,tmp(2),tmp(3)-tmp(2))
           logap(d)=strmid(line,tmp(8),tmp(9)-tmp(8))
           npckts(d)=strmid(line,tmp(7),tmp(8)-tmp(7))
	d=d+1   
    endwhile
  close,1
  endif
endfor

d=d-1
if(d ne -1) then begin
apids=apids(0:d)
dofgap=dofgap(0:d)
sofgap=sofgap(0:d)
logap=logap(0:d)
npckts=npckts(0:d)

days=strmid(dofgap,8,2)
mons=strmid(dofgap,5,2)
hour1=strmid(sofgap,0,2)+strmid(sofgap,3,2)/60.+strmid(sofgap,6,2)/(60.*60.)
hour2=hour1+logap/60.

za=where(hour2 gt 24)

daysa=intarr(n_elements(days)+n_Elements(za))
monsa=intarr(n_elements(days)+n_Elements(za))
apida=intarr(n_elements(days)+n_Elements(za))
hour1a=fltarr(n_elements(days)+n_Elements(za))
hour2a=fltarr(n_elements(days)+n_Elements(za))
npckta=fltarr(n_elements(days)+n_Elements(za))

nz2=-1l
td=n_elements(days)-1
td1=[0l]
td2=[td]
if (td gt 30000) then begin
  td1=[0l,30000l] 
  td2=[29999l,td-1l] 
endif 

for nz1=0,n_Elements(td1)-1 do begin
  for nz=td1(nz1)-td1(nz1),td2(nz1)-td1(nz1) do begin
    nz2=nz2+1
    ;help,nz2,nz,nz1
    daysa(nz2)=days(nz+td1(nz1))
    monsa(nz2)=mons(nz+td1(nz1))
    apida(nz2)=apids(nz+td1(nz1))
    hour1a(nz2)=hour1(nz+td1(nz1))
    hour2a(nz2)=hour2(nz+td1(nz1))
    npckta(nz2)=npckts(nz+td1(nz1))
    if (hour2(nz+td1(nz1)) gt 24)then begin
    ; split gap between two days
       npckta(nz2)=round(npckts(nz+td1(nz1))*((24-hour1a(nz2))/(hour2a(nz2)-hour1a(nz2))))
       hour2a(nz2)=24
       nz2=nz2+1
       daysa(nz2)=days(nz+td1(nz1))+1
    	if (daysa(nz2) gt (doy1-doy0)+1) then BEGIN
    	    monsa(nz2)=mons(nz+td1(nz1))+1 
	    daysa[nz2]=1
    	ENDIF else monsa(nz2)=mons(nz+td1(nz1))
       apida(nz2)=apids(nz+td1(nz1))
       hour1a(nz2)=0
       hour2a(nz2)=hour2(nz+td1(nz1))-24
       npckta(nz2)=round(npckts(nz+td1(nz1))*((hour2a(nz2)-hour1a(nz2))/((hour2a(nz2)+24)-hour1a(nz2-1))))
    endif 
  endfor       
endfor       

nz3=where(monsa eq month)  ;last day of previous month and first day of next month

if nz3(0) gt -1 then begin
    daysa=daysa(nz3)
    monsa=monsa(nz3)
    apida=apida(nz3)
    hour1a=hour1a(nz3)
    hour2a=hour2a(nz3)
    npckta=npckta(nz3)
endif else begin
    daysa=-1
    monsa=-1
    apida=-1
    hour1a=-1
    hour2a=-1
    npckta=-1
endelse
    
    
clr=intarr(500)
clr(440)=0 & clr(441)=2  & clr(442)=4  & clr(443)=6  & clr(444)=3 
clr(450)=0 & clr(451)=2  & clr(452)=4  & clr(453)=6  & clr(454)=3 
clr(470)=0 & clr(471)=2  & clr(472)=4  & clr(473)=6  & clr(474)=3 

offsets=fltarr(500)
offsets(440)=0 & offsets(441)=.1  & offsets(442)=.3  & offsets(443)=.4  & offsets(444)=.2
offsets(450)=0 & offsets(451)=.1  & offsets(452)=.3  & offsets(453)=.4  & offsets(454)=.2
offsets(470)=0 & offsets(471)=.1  & offsets(472)=.3  & offsets(473)=.4  & offsets(474)=.2
offsets=offsets*2

td=n_elements(daysa)-1
td1=[0l]
td2=[td-1l]
if (td gt 30000) then begin
  td1=[0l,30000l] 
  td2=[29999l,td-1l] 
endif 

;!p.multi=[0,0,3,0,0]
 ytcks=(doy1-doy0+1) - indgen(doy1-doy0+1) & ytcks=[' ',string(ytcks,'(i2)')]
 xtcks=indgen (24) +1

;set_plot,'ps'
;device,/land ,/color
window,11,xsize=1000,ysize=800

plot,[0,0],[0,0],xrange=[0,24],yrange=[doy1-doy0+2,1],xstyle=1,ystyle=1,background=1,color=0, $
    title=strupcase(sc)+'  '+yyyy+'-'+mm+' SSR1 DATA GAPS', ytitle='day of month',$
    xtitle='hours',xticks=24,yticks=doy1-doy0+1,ytickname=ytcks,yticklen=1.0,ygridstyle=1;,font=0
if nz3(0) gt -1 then begin
  for nz1=0,n_Elements(td1)-1 do begin
    for n=td1(nz1)-td1(nz1),td2(nz1)-td1(nz1) do $
      if(apida(n+td1(nz1)) lt 450)then $
      oplot,[hour1a(n+td1(nz1)),hour2a(n+td1(nz1))],[daysa(n+td1(nz1))+offsets(apida(n+td1(nz1))), $
      	    daysa(n+td1(nz1))+offsets(apida(n+td1(nz1)))], thick=2,color=clr(apida(n+td1(nz1)))
  endfor
endif
xyouts,1,doy1-doy0+3.5,'COR1',color=clr(440);,font=0
xyouts,3,doy1-doy0+3.5,'COR2',color=clr(441);,font=0
xyouts,5,doy1-doy0+3.5,'EUVI',color=clr(444);,font=0
xyouts,7,doy1-doy0+3.5,'HI1',color=clr(442);,font=0
xyouts,9,doy1-doy0+3.5,'HI2',color=clr(443);,font=0

;device,/close
;set_plot,'x'
IF keyword_set(PIPELINE) THEN pngdir='/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SSR1/' ELSE pngdir=outpdir
;spawn,'ps2pdf idl.ps '+'/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SSR1/'+strupcase(sc)+'_SSR1_'+yyyy+'_'+mm+'.pdf'
im=ftvread(filename=concat_dir(pngdir,strupcase(sc)+'_SSR1_'+yyyy+'_'+mm),/png,/noprompt)

wdel,11

close,10


minsinmonth=intarr((doy1-doy0+1)*24l*60l)
nmiss=lonarr(n_Elements(minsinmonth))

min1=(hour1a-fix(hour1a))*60 & min2=(hour2a-fix(hour2a))*60

;
; compute SSR1 missing packets
;

help,td1,td2,apida,hour2a,hour1a,daysa,hour1a,min1,hour2a,min2,minsinmonth,nmiss,npckta
for nz1=0,n_Elements(td1)-1 do begin
  for n=td1(nz1)-td1(nz1),td2(nz1)-td1(nz1) do $
    if(apida(n+td1(nz1)) lt 450 and hour2a(n+td1(nz1))-hour1a(n+td1(nz1)) gt 5/60.)then begin 
      p1=((fix(daysa(n+td1(nz1))-1)*24l)+fix(hour1a(n+td1(nz1))))*60l+fix(min1(n+td1(nz1)))
      p2=((fix(daysa(n+td1(nz1))-1)*24l)+fix(hour2a(n+td1(nz1))))*60l+fix(min2(n+td1(nz1)))
      help,nz1,n,p1,p2
      minsinmonth(p1:p2)=1
      nmiss(p1:p2)=nmiss(p1:p2)+npckta(n+td1(nz1))
    endif
;    printf,10,apida(n+td1(nz1)),yyyy,'-',mm,'-',fix(daysa(n+td1(nz1))),hour1a(n+td1(nz1)),':',min1(n+td1(nz1)), '- ',hour2a(n+td1(nz1)),':',min2(n+td1(nz1)),npckta(n+td1(nz1)),format=fmt
endfor

tmp=minsinmonth-shift(minsinmonth,1)
tmp2=where(tmp eq 1) & tmp3=where(tmp eq -1)

dayz=tmp2/(24*60l)+1
hrz=fix((tmp2/(24*60.)-(fix(dayz)-1))*24)
minz=(((tmp2/(24*60.)-(fix(dayz)-1))*24)-fix((tmp2/(24*60.)-(fix(dayz)-1))*24))*60
dayz3=tmp3/(24*60l)+1
hrz3=fix((tmp3/(24*60.)-(fix(dayz3)-1))*24)
minz3=(((tmp3/(24*60.)-(fix(dayz3)-1))*24)-fix((tmp3/(24*60.)-(fix(dayz3)-1))*24))*60

openw,11,concat_dir(outpdir,strupcase(sc)+'_SSR1_'+yyyy+'_'+mm+'.txt')
    fmt='(i4.4,a1,i2.2,a1,i2.2,a1,i2.2,a1,i2.2,a3,i4.4,a1,i2.2,a1,i2.2,a1,i2.2,a1,i2.2,5x,f5.1," hours",5x,i15," Missing Packets")'
printf,11,strupcase(sc)+'_SSR1_'+yyyy+'_'+mm+'.txt'
if(tmp2(0) gt -1) then begin
for nt=0,n_elements(tmp2)-1 do $
   printf,11,yyyy,'-',mm,'-',dayz(nt),'-',hrz(nt),':',minz(nt),' - ',yyyy,'-',mm,'-',dayz3(nt),'-',hrz3(nt),':',minz3(nt),(tmp3(nt)-tmp2(nt))/60.,max(nmiss(tmp2(nt):tmp3(nt))),format=fmt
endif
close,11



 ytcks=(doy1-doy0+1) - indgen(doy1-doy0+1) & ytcks=[' ',string(ytcks,'(i2)')]
 xtcks=indgen (24) +1

;set_plot,'z'
;device,/land ,/color

window,11,xsize=1000,ysize=800
plot,[0,0],[0,0],xrange=[0,24],yrange=[doy1-doy0+2,1],xstyle=1,ystyle=1,background=1,color=0,title=strupcase(sc)+'  '+yyyy+'-'+mm+' SPACE WEATHER DATA GAPS',ytitle='day of month',$
    xtitle='hours',xticks=24,yticks=doy1-doy0+1,ytickname=ytcks,yticklen=1.0,ygridstyle=1;,font=0
if nz3(0) gt -1 then begin
  for nz1=0,n_Elements(td1)-1 do begin
    for n=td1(nz1)-td1(nz1),td2(nz1)-td1(nz1) do $
      if(apida(n+td1(nz1)) ge 470)then $
      oplot,[hour1a(n+td1(nz1)),hour2a(n+td1(nz1))],[daysa(n+td1(nz1))+offsets(apida(n+td1(nz1))),daysa(n+td1(nz1))+offsets(apida(n+td1(nz1)))],$
      thick=3,color=clr(apida(n+td1(nz1)))
  endfor
endif
xyouts,1,doy1-doy0+3.5,'COR1',color=clr(470);,font=0
xyouts,3,doy1-doy0+3.5,'COR2',color=clr(471);,font=0
xyouts,5,doy1-doy0+3.5,'EUVI',color=clr(474);,font=0
xyouts,7,doy1-doy0+3.5,'HI1',color=clr(472);,font=0
xyouts,9,doy1-doy0+3.5,'HI2',color=clr(473);,font=0

;device,/close

;set_plot,'x'
;spawn,'ps2pdf idl.ps '+'/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SPW/'+strupcase(sc)+'_SPW_'+yyyy+'_'+mm+'.pdf'

IF keyword_set(PIPELINE) THEN pngdir='/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SPW/' ELSE pngdir=outpdir
im=ftvread(filename=concat_dir(pngdir,strupcase(sc)+'_SPW_'+yyyy+'_'+mm),/png,/noprompt)

wdel,11

close,10


minsinmonth=intarr((doy1-doy0+1)*24l*60l)
nmiss=lonarr(n_Elements(minsinmonth))

min1=(hour1a-fix(hour1a))*60 & min2=(hour2a-fix(hour2a))*60

for nz1=0,n_Elements(td1)-1 do begin
  for n=td1(nz1)-td1(nz1),td2(nz1)-td1(nz1) do $
    if(apida(n+td1(nz1)) ge 470 and hour2a(n+td1(nz1))-hour1a(n+td1(nz1)) gt 5/60.)then begin 
      p1=((fix(daysa(n+td1(nz1))-1)*24l)+fix(hour1a(n+td1(nz1))))*60l+fix(min1(n+td1(nz1)))
      p2=((fix(daysa(n+td1(nz1))-1)*24l)+fix(hour2a(n+td1(nz1))))*60l+fix(min2(n+td1(nz1)))
      minsinmonth(p1:p2)=1
      nmiss(p1:p2)=nmiss(p1:p2)+npckta(n+td1(nz1))
    endif
;    printf,10,apida(n+td1(nz1)),yyyy,'-',mm,'-',fix(daysa(n+td1(nz1))),hour1a(n+td1(nz1)),':',min1(n+td1(nz1)), '- ',hour2a(n+td1(nz1)),':',min2(n+td1(nz1)),npckta(n+td1(nz1)),format=fmt
endfor

tmp=minsinmonth-shift(minsinmonth,1)
tmp2=where(tmp eq 1) & tmp3=where(tmp eq -1)

dayz=tmp2/(24*60l)+1
hrz=fix((tmp2/(24*60.)-(fix(dayz)-1))*24)
minz=(((tmp2/(24*60.)-(fix(dayz)-1))*24)-fix((tmp2/(24*60.)-(fix(dayz)-1))*24))*60
dayz3=tmp3/(24*60l)+1
hrz3=fix((tmp3/(24*60.)-(fix(dayz3)-1))*24)
minz3=(((tmp3/(24*60.)-(fix(dayz3)-1))*24)-fix((tmp3/(24*60.)-(fix(dayz3)-1))*24))*60

openw,11,concat_dir(outpdir,strupcase(sc)+'_SPW_'+yyyy+'_'+mm+'.txt')
    fmt='(i4.4,a1,i2.2,a1,i2.2,a1,i2.2,a1,i2.2,a3,i4.4,a1,i2.2,a1,i2.2,a1,i2.2,a1,i2.2,5x,f5.1," hours",5x,i15," Missing Packets")'
printf,11,strupcase(sc)+'_SPW_'+yyyy+'_'+mm+'.txt'
if(tmp2(0) gt -1) then begin
for nt=0,n_elements(tmp2)-1 do $
   printf,11,yyyy,'-',mm,'-',dayz(nt),'-',hrz(nt),':',minz(nt),' - ',yyyy,'-',mm,'-',dayz3(nt),'-',hrz3(nt),':',minz3(nt),(tmp3(nt)-tmp2(nt))/60.,max(nmiss(tmp2(nt):tmp3(nt))),format=fmt
endif
close,11

endif ; d>0

;openw,10,outpdir+strupcase(sc)+'_SSR1_'+yyyy+'_'+mm+'_all_apids.txt'
;    fmt='(i3.3,2x,i4.4,a1,i2.2,a1,i2.2,1x,i2.2,a1,i2.2,a3,i2.2,a1,i2.2,3x,i)'
;printf,10,strupcase(sc)+'_SSR1_'+yyyy+'_'+mm+'.txt'
;for nz1=0,n_Elements(td1)-1 do begin
;  for n=td1(nz1)-td1(nz1),td2(nz1)-td1(nz1) do $
;    if(apida(n+td1(nz1)) lt 450 and hour2a(n+td1(nz1))-hour1a(n+td1(nz1)) gt 5/60.)then $
;    printf,10,apida(n+td1(nz1)),yyyy,'-',mm,'-',fix(daysa(n+td1(nz1))),hour1a(n+td1(nz1)),':',min1(n+td1(nz1)), '- ',hour2a(n+td1(nz1)),':',min2(n+td1(nz1)),npckta(n+td1(nz1)),format=fmt
;endfor
;close,10


end
