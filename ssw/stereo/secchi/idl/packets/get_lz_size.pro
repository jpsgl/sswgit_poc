pro get_lz_size,sc,d1,d2,ut,sizes,ver=ver
;+
; $Id: get_lz_size.pro,v 1.2 2009/01/21 23:01:14 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_lz_size
;               
; Purpose   : gets total size of daily lz files  
;               
; Explanation: 
;               
; Use       : IDL> get_lz_size,'a','2008-10-01','2009-01-31',dates,sizes,/fin
;    
; Inputs    :sc = 'a' or 'b' d1=first day d2 = last day
;               
; Outputs   : ut = UTC array of dates
;   	      sizes = daily total of the 6 lz files (Bytes)
;
; Keywords  : fin to read in the final version size
;             ver to set version number of ptp files default is 2 
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write temporary file.
;               
; Side effects: writes a temporary file  (~/lzsizetmp.txt) to the home directory.
;               
; Category    : data anal
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2008
;               
; $Log: get_lz_size.pro,v $
; Revision 1.2  2009/01/21 23:01:14  nathan
; Remove /FIN option and always retrieve .fin data as well as current data
; and then sort; return time in UTC format
;
; Revision 1.1  2009/01/16 18:54:46  mcnutt
; gets daily lz file size totals
;

ut0=anytim2utc(d1)
ut1=anytim2utc(d2)

yr1=fix(strmid(d1,0,4)) & yr2=fix(strmid(d2,0,4))

sc=strlowcase(sc)
if keyword_set(fin) then ext='.fin' else ext='.ptp'
if keyword_set(ver) then ver=ver else ver=2
if sc eq 'b' then sc1='behind'
if sc eq 'a' then sc1='ahead' 

tmp=findfile('~/lzsizetmp.txt')
if tmp(0) ne '' then begin
  cmd='rm ~/lzsizetmp.txt'
  spawn,cmd,/sh
endif
for iy=yr1,yr2 do begin
       yr=string(iy,'(i4)')
       if iy eq yr1 then m1=fix(strmid(d1,5,2)) else m1=1
       if iy eq yr2 then m2=fix(strmid(d2,5,2)) else m2=12      
       for im=m1,m2 do begin
         mo=string(im,'(i2.2)')
         cmd='/bin/ls -ltr '+getenv('SDS_FIN_DIR')+'/'+sc+'/'+yr+'/'+mo+'/*.fin | awk '+"'"+'{print $9,$5}'+"'"+' >> ~/lzsizetmp.txt'
         spawn,cmd,/sh
       endfor
endfor
cmd='/bin/ls -ltr '+getenv('SDS_DIR')+'/'+sc1+'/data_products/level_0_telemetry/secchi/*'+string(ver,'(i2.2)')+'.ptp | awk '+"'"+'{print $9,$5}'+"'"+' >> ~/lzsizetmp.txt'
spawn,cmd,/sh

openr,szlun,'~/lzsizetmp.txt',/get_lun
line=''
line2=''
readf,szlun,line
parts=strsplit(line,/extract)
ids=rstrmid(parts[0],4,13)
sz=long(parts[1])

while ~eof(szlun) do begin
    readf,szlun,line
    parts=strsplit(line,/extract)
    ids=[ids,rstrmid(parts[0],4,13)]
    sz=[sz,long(parts[1])]
endwhile
close,szlun
FREE_LUN,szlun

cmd='rm ~/lzsizetmp.txt'
;spawn,cmd
srt=sort(ids)
ids=ids[srt]
ids2=strmid(ids,0,10)
unq=uniq(ids2)
days=strmid(ids[unq],0,8)
sz=sz[srt[unq]]

utdays=doy2utc(fix(strmid(days,5,3)),fix(strmid(days,0,4)))

ut=replicate(ut0,ut1.mjd-ut0.mjd+1)
dates=strarr(ut1.mjd-ut0.mjd+1)
sizes=lonarr(ut1.mjd-ut0.mjd+1)

FOR i=ut0.mjd,ut1.mjd DO BEGIN
    z=where(utdays.mjd eq i, nz)
    ut[i-ut0.mjd].mjd=i
    if nz EQ 6 then sizes[i-ut0.mjd]=total(sz[z]) ELSE message,'not 6 files for '+utc2str(ut[i-ut0.mjd],/date),/info
ENDFOR

end
