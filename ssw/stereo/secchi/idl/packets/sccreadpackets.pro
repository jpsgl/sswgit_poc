
function sccreadpackets,ts,te,s_c0,mnemonic,file=file,dir=dir,no_grh=no_grh, CHANGED=changed, $
    	    	SAVEDIR=savedir

;+
; $Id: sccreadpackets.pro,v 1.5 2009/12/10 21:10:05 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : sccreadpackets
;
; Purpose : reads raw telemetry file(s) (.ptp or .fin) and extracts
;           desired mnemonics
;
; Use     : IDL> d = sccreadpackets(ts,te,'a',dir='/User/wuelser/telem_a')
;
; Inputs  :
;    ts = start time (anytim format). yyyymmdd hh:mm:ss
;    te = end time.
;    s_c = 'a' or 'b'
;   all 3 inputs required unless FILE keyword supplied
;
;    mnemonic = STRARR of mnemonic names, all must be from the same APID
;
; Outputs :
;    d = structure:
;   	MNEMONIC        STRING    Array[nmnm]
;   	TIME            STRUCT    -> CDS_INT_TIME Array[npkts]
;   	VAL             ULONG     Array[nmnm, npkts]
;   	FRMT            STRING    Array[nmnm]
;
; Keywords:
;   file = file list  (input, optional)
;   dir = directory for automatic file search (defaults to default dir path at NRL)
;   /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;   /CHANGED   Only includes element in output for packet where value changes from previous packet
;   SAVEDIR=	Specify directory to save output (by default no save file)
;
; Common  : None
;
; Restrictions: Requires merged_tlm.sdb file in $ITOS_GROUPDIR/sdb/
;
; Side effects:  mnemonics which are byte format may not have correct endian order.
;
; Category    : packets database telemetry housekeeping trending
;
; Prev. Hist. :
;
; Written     : 
;
; $Log: sccreadpackets.pro,v $
; Revision 1.5  2009/12/10 21:10:05  nathan
; added SAVEDIR keyword
;
; Revision 1.4  2009/12/09 17:15:26  nathan
; update file header doc
;
; Revision 1.3  2009/12/09 17:00:11  nathan
; Use $ITOS_GROUPDIR/sdb/merged_tlm.sdb; parse_stereo_name; fix call to
; scc_time2tlmfile; convert time in output to cds utc structure
;
; Revision 1.2  2009/12/07 19:17:08  mcnutt
; added program first, try based on scc_gtslew
;
; Revision 1.1  2009/12/01 20:33:09  nathan
; outline only
;
;-
nm=n_elements(mnemonic)
apid=intarr(nm)
mnemlen=intarr(nm)
mnempos=intarr(nm)
mnembpos=intarr(nm); used for UB format only
mnemfrmt=strarr(nm)
for n=0,nm-1 do begin
  cmd='grep '+mnemonic(n)+' '+getenv('ITOS_GROUPDIR')+'/sdb/merged_tlm.sdb |grep PKT'
  print,cmd
  spawn,cmd,result
  if strpos(result,mnemonic(n)) gt -1 then begin
    tmp=strsplit(result,'|')
    hapid=strmid(result,tmp(1),(tmp(2)-tmp(1)-1))  
    if strpos(hapid,'x') gt -1 then begin
       hapid=strmid(hapid,strpos(hapid,'x')+1,3)
       reads,hapid,apidt,format='(z)' 
    endif else apidt=STRCOMPRESS(hapid, /REMOVE_ALL)
    apid(n)=fix(apidt)
    nfmt=STRCOMPRESS(strmid(result,tmp(6),(tmp(7)-tmp(6)-1)), /REMOVE_ALL)  
    if strpos(strupcase(nfmt),'TIME') gt -1 then mnemlen(n)=fix(strmid(nfmt,strpos(strupcase(nfmt),'TIME')+1,2)) else mnemlen(n)=strlen(nfmt)-1
    if strpos(strupcase(nfmt),'UI') gt -1 then mnemlen(n)=2
    mnempos(n)=fix(strmid(result,tmp(7),(tmp(8)-tmp(7)-1)))
    mnemfrmt(n)=strmid(nfmt,0,1)
    if strpos(strupcase(nfmt),'UB') gt -1 then begin   ;change pos info for Byte format
          mnempos(n)=fix(strmid(result,tmp(7),(tmp(8)-tmp(7)-1)))
	  mnembpos(n)=fix(strmid(result,tmp(8),(tmp(9)-tmp(8)-1)))
          mnemlen(n)=fix(strmid(result,tmp(9),(tmp(10)-tmp(9)-1)))
          mnemfrmt(n)='B'
    endif
  endif else begin
    print,'********* mnemonic ',mnemonic(n),'  not found **************'
    return,0
  endelse
endfor
apidtest=where(apid ne apid(0))
if apidtest(0) gt -1 then begin
    for n=0,n_elements(mnemonic)-1 do print,mnemonic(n),'  ',apid(n)
    print,'********* mnemonics must have the same APID **************'
    return,0
endif

s_c=parse_stereo_name(s_c0,['a','b'])
n = n_elements(file)
if n eq 0 then begin
   file = scc_time2tlmfile(ts,te,s_c,/NRL,dir=dir)
   n = n_elements(file)
endif

if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words
it1  =  (6+ghb);/2
rec = intarr(136+ghi)              ; 1 packet
dsize=136/(mnemlen/2.0)
mnemnum=mnempos/mnemlen


on_ioerror,endfil
icount=0
apid0=0

for i=0,n-1 do begin
  print,'Reading ',file[i]
  openr,lun,file[i],/get_lun
  while 1 do begin
    readu,lun,rec
    apid0 = rec[ghi]
    ieee_to_host, apid0
    if (apid0 and 2047) eq apid(0) then begin
      trn0 = long(rec,it1)
      ieee_to_host, trn0
      trn = double(trn0)
      for ni=0,n_elements(mnemonic)-1 do begin
        if strupcase(mnemfrmt(ni)) ne 'B' then begin
          if strupcase(mnemfrmt(ni)) eq 'I' then vl=long(rec,ghb,dsize(ni))   
          if strupcase(mnemfrmt(ni)) eq 'U' and mnemlen(ni) le 2 then vl=uint(rec,ghb,dsize(ni))   
          if strupcase(mnemfrmt(ni)) eq 'U' and mnemlen(ni) gt 2 then vl=ulong(rec,ghb,dsize(ni))   
          if strupcase(mnemfrmt(ni)) eq 'F' then vl=float(rec,ghb,dsize(ni))   
          ieee_to_host,vl
          vl=vl(mnemnum(ni))
	endif; else begin
        if strupcase(mnemfrmt(ni)) eq 'B' then begin
          recb=rec
          ieee_to_host,recb
          vl=byte(recb,ghb,272)
          vl=strmid(string(vl(mnempos(ni)),'(b8.8)'),mnembpos(ni),mnemlen(ni))
;          reads,vl2,vl,format='(b)'
        endif
        if ni eq 0 then vls=vl else vls=[vls,vl]
      endfor 
      if icount gt 0 then begin
        add=1
	unchanged=(total(vls) eq total(val(*,icount-1)))
        if (keyword_set(changed) and unchanged) then add=0
        if add eq 1 then begin
	    IF keyword_set(changed) THEN print,'Change found at ',utc2str(tai2utc(trn),/ecs)
	    tunc=[tunc,anytim2utc(trn)]
            val=[[val],[vls]]
            icount=icount+1 ;count apid packets
        endif
      endif else begin
         tunc=anytim2utc(trn)
         val=[vls]
         icount=icount+1 ;count apid packets
      endelse
    endif

  endwhile
  endfil: free_lun,lun

endfor

if datatype(tunc) ne 'UND' then d={mnemonic:mnemonic,time:tunc,val:val,frmt:mnemfrmt} else d=-1

IF keyword_set(SAVEDIR) and icount GT 1 THEN BEGIN
    dec2hex,apid[0],hx
    ymd=utc2yymmdd(tunc[1],/yyyy)
    fn=concat_dir(savedir,'scc'+strupcase(s_c)+ymd+'x'+hx+'.csv')
    colw=max(strlen(mnemonic))+1
    ;      2006-10-31T23:59:34.000
    line0='Packet Time            '
    FOR i=0,nm-1 DO line0=line0+string(mnemonic[i],'(a'+trim(colw)+')')
    len0=strlen(line0)
    line1='='
    FOR i=0,len0-2 DO line1=line1+'='

    openw,1,fn
    printf,1,line0
    printf,1,line1
    FOR i=0,icount-1 DO BEGIN
	line=utc2str(tunc[i])
	FOR j=0,nm-1 DO line=line+string(val[j,i],format='(i'+trim(colw)+')')
	printf,1,line
    ENDFOR
    close,1
ENDIF

return,d

end

   
