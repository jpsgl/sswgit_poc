pro plot_apids, filename, uts, apids, HK=hk
;+
; Project     :	STEREO 
;
; Name        :	
;
; Purpose     :	Plot ApIDs vs. time from a ptp file
;
; Category    :	STEREO, Telemetry
;
; Explanation :	
;
; Syntax      :	
;
; Examples    :	
;
; Inputs      :	filename    PTP or STP or FIN filename
;
; Opt. Inputs :	None.
;
; Outputs     :	INT
;
; Opt. Outputs:	uts 	Array of UT times
;   	    	apids	Matching array of ApIDs
;
; Keywords    : /HK 	Plot/return only HK ApIDs
;
; Calls       :	parse_pkts.pro
;
; Common      :	None.
;
; Restrictions:	
;
; Side effects:	None.
;
; Prev. Hist. :	count_packets.pro
;-
; $Log: plot_apids.pro,v $
; Revision 1.2  2015/09/01 21:15:18  nathan
; bugs
;
; Revision 1.1  2015/09/01 21:10:44  nathan
; for counting packets
;

;

parse_pkts, filename, pkts, grt, seqcnts, grh, HK=hk, /combine

apids=pkts.pkt.hdr-2048
uts=tai2utc(double(pkts.pkt.seconds)) 
n=n_elements(uts)
print,'Plotting ', utc2str(uts[0]),' to ',utc2str(uts[n-1]) 

plotprep

utplot,uts,apids, psym=1 ,ytitle='APID',charsize=1.5


end
