; $Id: getscifileinfo.pro,v 1.2 2007/09/17 16:19:50 nathan Exp $
;
; Name:     getscifileinfo
;
; Purpose:  Generate stats from raw image science files (FILEORIG)
;
; Inputs: 	See keywords
;
; Keywords:	Following are required:
;			/VOLUME (diskspace) or /NUMBER (number of images)
;
;		Following are optional:
;			/AHEAD or /BEHIND (default is to do both side-by-side)
;			TELESCOPE = tel where tel = cor1 or cor2 or euvi or hi1 or hi2
;			One or more of /SSR1, /SSR2, /SSRT, /SPWX (downlink channel)
;			/NOPLOT	  Do not open plot window
;			/OVERPLOT	Plot on top of previous plot instead of redrawing
;			STA_DATE=date to start stats from
;			PREDICT_DATE=date for which to print estimated total volume (from sta_date);
;				defaults to 30 days from last day
;			NDAYS=number of days from sta_date to get stats for (default=to last day)
;			LOGFILE=name of file to append summary info to 
;			
; Outputs:	
;   	    
; Side effects:	
;
; Restrictions:  Uses NRL MySQL database via sqdb.pro.
;
; Category:	archive, utility, pipeline, data management, statistics
;
; $Log: getscifileinfo.pro,v $
; Revision 1.2  2007/09/17 16:19:50  nathan
; comments; note this pro is not polished--feel free to upgrade
;
; Revision 1.1  2007/09/17 16:17:32  nathan
; do stats for raw image files
;
; Log: getsccarchiveinfo.pro,v 
; Revision 1.13  2007/08/31 21:23:55  nathan
; fix /noplot keyword
;

pro readvolfile, filenames, firoot, fisfx, fisz

; one array for each file
; dates is a list of UTC dates since 2006/11/01 (inclusive)

nf=n_elements(filenames)
firoot=strarr(nf)
fisfx=intarr(nf)
fisz=lonarr(nf)
FOR j=0,nf-1 DO BEGIN
    fsci=filenames[j]
    openr,11,fsci
    break_file,fsci,g,p,r,s
    firoot[j]=r
    fisfx[j]=fix(strmid(s,1,3))
    st=fstat(11)
    fisz[j]=st.size
    close,11
    ;stop
ENDFOR
    
    
end

pro getscifileinfo, crnttotal, crntdate, bm, STA_DATE=sta_date, NDAYS=ndays, $
			AHEAD=ahead, BEHIND=behind, TELESCOPE=telescope, PREDICT_DATE=predict_date, $
			VOLUME=volume, NUMBER=number, SSR1=ssr1, SSR2=ssr2, SSRT=ssrt, SPWX=spwx, $
    	    FITS=fits, SCI=sci, IMG=img, SEQ=seq, NOPLOT=noplot, OVERPLOT=overplot, $
			LOGFILE=logfile
			
common sccdata, opcolor, opsym
pause=2

obs=''
IF keyword_set(AHEAD) THEN BEGIN
    isahead=1
    sdir=getenv_slash('scia')
    obs=' and secchi_flight.img_seb_hdr.obsrvtry =1'
ENDIF ELSE isahead=0
IF keyword_set(BEHIND) THEN BEGIN
    isbehind=1
    sdir=getenv_slash('scib') 
    obs=' and secchi_flight.img_seb_hdr.obsrvtry =2'
ENDIF ELSE isbehind=0

IF ~isahead and ~isbehind THEN BEGIN
	npltcol=2
	pltxsz=756
	isahead=1
ENDIF ELSE BEGIN
	npltcol=1
	pltxsz=512
ENDELSE
IF keyword_set(OVERPLOT) THEN BEGIN
	opcolor=opcolor+1
	opsym=opsym+1
ENDIF ELSE BEGIN
	opcolor=1
	opsym=4
		; will add 1 before using
ENDELSE

;cd,concat_dir(getenv('SCC_DATA'),'statistics'),cur=cdir
spawn,'pwd'

src=['lz','pb','rt']
sc=['a','b']
sdr=['img','seq','cal']

;++++ Loop for two s/c
FOR j=1,npltcol DO BEGIN
	if j GT 1 THEN BEGIN
		isahead=0
		isbehind=1
	endif
;      SSRT   SSR1   SSR2   SPWX

; suffixes for raw image files
IF isahead THEN BEGIN
    message,'Processing data for SECCHI-A...',/info
    wait,2
    aorb='A'
    ab='a'
    sfxa=['302', '402', '502', '702', $ ; COR1-A
	  '311', '411', '511', '711', $ ; COR2-A
	  '343', '443', '543', '743', $ ; EUVI-A
	  '325', '425', '525', '725', $ ; HI1-A
	  '334', '434', '534', '734']   ; HI2-A

    ; equivalent ATTS for FITS filenames

    atsa=['s3c1A','s4c1A','s5c1A','s7c1A', $ 
	  'd3c1A','d4c1A','d5c1A','d7c1A', $
	  's3c2A','s4c2A','s5c2A','s7c2A', $
	  'd3c2A','d4c2A','d5c2A','d7c2A', $
	  '3euA','4euA','5euA','7euA', $
	  '3h1A','4h1A','5h1A','7h1A', $
	  '3h2A','4h2A','5h2A','7h2A']

ENDIF ELSE BEGIN
    message,'Processing data for SECCHI-B...',/info
    wait,2
    aorb='B'
    ab='b'
    sfxa=['307', '407', '507', '707', $ ; COR1-B
	  '316', '416', '516', '716', $ ; COR2-B
	  '348', '448', '548', '748', $ ; EUVI-B
	  '320', '420', '520', '720', $ ; HI1-B
	  '339', '439', '539', '739']   ; HI2-B

    atsa=['s3c1B','s4c1B','s5c1B','s7c1B', $ 
	  'd3c1B','d4c1B','d5c1B','d7c1B', $
	  's3c2B','s4c2B','s5c2B','s7c2B', $
	  'd3c2B','d4c2B','d5c2B','d7c2B', $
	  '3euB','4euB','5euB','7euB', $
	  '3h1B','4h1B','5h1B','7h1B', $
	  '3h2B','4h2B','5h2B','7h2B'] 
ENDELSE
sfxa=fix(sfxa)

t='?'
d='?'
det=''
IF keyword_set(TELESCOPE) THEN BEGIN
    tel=strupcase(telescope)
    case tel of
	'COR1': BEGIN
		    tssubs=[0,1,2,3]
		    tfsubs=[0,1,2,3,4,5,6,7]
		    t='[27]'
		    det=' and secchi_flight.img_seb_hdr.detector =2'
		END
	'COR2': BEGIN
    		    tssubs=[4,5,6,7]
		    tfsubs=[8,9,10,11,12,13,14,15]
		    t='[16]'
		    det=' and secchi_flight.img_seb_hdr.detector =1'
		END
	'EUVI': BEGIN
    		    tssubs=[8,9,10,11]
		    tfsubs=[16,17,18,19]
		    t='[38]'
		    det=' and secchi_flight.img_seb_hdr.detector =3'
		END
	'HI1':  BEGIN
    		    tssubs=[12,13,14,15]
		    tfsubs=[20,21,22,23]
		    t='[50]'
		    det=' and secchi_flight.img_seb_hdr.detector =5'
		END
	'HI2':  BEGIN
    		    tssubs=[16,17,18,19]
		    tfsubs=[24,25,26,27]
		    det=' and secchi_flight.img_seb_hdr.detector =4'
		    t='[49]'
		END
	ELSE:
    ENDCASE
ENDIF  ELSE  BEGIN
    tssubs=indgen(20)
	tfsubs=indgen(28)
	tel='SECCHI'
ENDELSE

dsubs=indgen(28)
dest='ALL'
d=''
des=''
IF keyword_set(SSRT) THEN BEGIN
	dsubs=[dsubs,0,4,8,12,16,20,24]
	dest='SSRT'
	d='3'
	des=' and secchi_flight.img_seb_hdr.downlink =3'
ENDIF
IF keyword_set(SSR1) THEN BEGIN
	dsubs=[dsubs,1,5,9,13,17,21,25]
	if dest NE 'ALL' then dest=dest+'+SSR1' else dest='SSR1'
	d='4'
	des=' and secchi_flight.img_seb_hdr.downlink =4'
ENDIF
IF keyword_set(SSR2) THEN BEGIN
	dsubs=[dsubs,2,6,10,14,18,22,26]
	if dest NE 'ALL' then dest=dest+'+SSR2' else dest='SSR2'
	d='5'
	des=' and secchi_flight.img_seb_hdr.downlink =5'
ENDIF
IF keyword_set(SPWX) THEN BEGIN
	dsubs=[dsubs,3,7,11,15,19,23,27]
	if dest NE 'ALL' then dest=dest+'+SPWX' else dest='SPWX'
	d='7'
	des=' and secchi_flight.img_seb_hdr.downlink =7'
ENDIF

ndsubs=n_elements(dsubs)
IF ndsubs GT 28 THEN dsubs=dsubs[28:ndsubs-1]

isubs=indgen(28)
IF keyword_set(IMG) THEN isubs=[4,5,6,7,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
IF keyword_set(SEQ) THEN isubs=[0,1,2,3,8,9,10,11]

IF keyword_set(FITS) THEN tsubs=tfsubs ELSE tsubs=tssubs
subs=intersection(intersection(tsubs,dsubs),isubs)
;subs is column subscripts to be added together

utc0=anytim2utc(sta_date)
utc1=utc0
IF keyword_set(NDAYS) THEN utc1.mjd=utc0.mjd+ndays ELSE utc1.mjd=utc0.mjd+1
utc1s=utc1
utc1s.mjd=utc1.mjd-1
ndays=utc1.mjd-utc0.mjd
yymmdd=utc2yymmdd(utc0)
;files=file_search(sdir+yymmdd+'/*.'+d+'?'+t)

qu='select secchi_flight.img_seb_hdr.obs_id, secchi_flight.img_seb_hdr.fileorig, secchi_flight.img_seb_hdr.filename, secchi_flight.img_seb_hdr.date_obs, secchi_flight.img_seb_hdr.sebxsum, secchi_flight.img_seb_hdr.naxis1, secchi_flight.img_seb_hdr.naxis2, secchi_flight.img_seb_hdr.diskpath, secchi_flight.img_seb_hdr_ext.comprssn, secchi_flight.img_seb_hdr_ext.compfact from secchi_flight.img_seb_hdr,secchi_flight.img_seb_hdr_ext where secchi_flight.img_seb_hdr.date_obs between "'+utc2str(utc0)+'" and "'+utc2str(utc1) +'"'+obs +det +des +' and secchi_flight.img_seb_hdr.fileorig = secchi_flight.img_seb_hdr_ext.fileorig'

s=sqdb('secchi_flight',qu)

files=sdir+strmid(s.filename,2,6)+'/'+s.fileorig
help,files

common sccarchive, rootsa, sufxsa, sizesa, rootsb, sufxsb, sizesb

IF keyword_set(IMG) or keyword_set(SEQ) THEN fits=1

IF isahead   THEN BEGIN
    ;IF datatype(rootsa) EQ 'UND' THEN  
    readvolfile,files,rootsa, sufxsa, sizesa
    numbers=sizesa
    subject="-A Downlinked Image Volume"
    yax='KB'
ENDIF
IF isbehind  THEN BEGIN
    ;IF datatype(rootsb) EQ 'UND' THEN 
    readvolfile,files,rootsb, sufxsb, sizesb
    numbers=sizesb
    subject="-B Downlinked Image Volume"
    yax='KB'
ENDIF

; change size to kbytes
IF isahead THEN BEGIN
    roots=rootsa
    sufxs=sufxsa
    sizes=sizesa/1024.
ENDIF
IF isbehind THEN BEGIN
    roots=rootsb
    sufxs=sufxsb
    sizes=sizesb/1024.
ENDIF

;fitsizes=    float(s.naxis1)*s.naxis2*2/1024.    ; 2 byte/pixel -> KB
;vec=sizes - (fitsizes/s.compfact)

gd=where(sizes GT 0.4)	; header only is .22 KB except euvi is .36 KB)
vec=sizes[gd]
;stop

mngtz=min(vec,max=mxgtz)
sdev=stdev(vec,vavg)
;crnttotal=total(vec)
;help,crnttotal
nselm=n_elements(vec)
print,''
msg=tel+aorb+' '+dest+' '+trim(nselm-1)+' Files: Max= '+trim(mxgtz)+' KB, Avg= '+trim(vavg)+' KB, Min= '+trim(mngtz)+' KB'
print,msg


IF ~keyword_set(NOPLOT) THEN BEGIN
	;--Display vector and running total of vector

	IF j LT 2 and ~keyword_set(OVERPLOT) THEN BEGIN
		!y.style=3
		!x.style=3
		device,decomposed=0
		tek_color
		!p.background=255
		!p.color=0
		;IF keyword_set(PREDICTDATE) THEN BEGIN
		window
		dmin=min(vec,max=dmax)
		dminmax=[dmin,dmax]
	ENDIF 
	IF npltcol EQ 1 THEN dminmax=0
	;--plot last 90 days + predict date
	;print,!p.multi
	IF keyword_set(OVERPLOT) THEN BEGIN
		xyouts,105,512-15*opcolor,tel+' '+dest,/device,size=1.5,color=opcolor
		!p.multi[0]=!p.multi[0]-1
		outplot,s.date_obs,vec,color=opcolor,psym=-opsym
	ENDIF ELSE BEGIN
		;ENDIF
		utplot,s.date_obs,vec,title=msg,ytitle=yax,psym=-5
		;xyouts,105,512-15,'Starting '+yymmdd,/device,size=1.5
		;xyouts,105,512-40,dest,/device,size=1.5
	
	ENDELSE
	;print,!p.multi
ENDIF
 print,'Waiting 2...'
 wait,2
IF keyword_set(LOGFILE) THEN BEGIN
	openw,2,logfile,/append
	printf,2,utc2str(utc0,/date)+' to '+utc2str(utc1s,/date)+'  '+msg+' Sdev= '+trim(sdev)
	close,2
ENDIF

ENDFOR
;!p.multi=0
;cd,cdir

end
