pro pktlossreport,yr

;+
; $Id: pktlossreport.pro,v 1.3 2008/07/14 18:39:36 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : pktlossreport
;               
; Purpose   : generate packet loss statistics (SSR1 image packets only)
;               
; Explanation: Reads pkt loss files from $scc[ab]/nrl/lz/outputs/prints and tlm volume with getsccarchiveinfo.pro and computes stats
;               
; Use       : 
;    
; Inputs    : 	INT year for which to generate stats
;
; Outputs   : '~/attic/missingtlmreport.txt',/append - does A and B one line per month and summary for year
;
; Optional Outputs:	
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : sci apids ssr telemetry statistics
;               
; Prev. Hist. : None.
;-
; $Log: pktlossreport.pro,v $
; Revision 1.3  2008/07/14 18:39:36  nathan
; updated comments
;
; Revision 1.2  2008/04/29 17:02:00  nathan
; implement first check
;
; Revision 1.1  2008/04/18 20:42:47  nathan
; for packet loss reporting
;

aandb=['A','B']
IF yr EQ 2006 THEN BEGIN
    d0='2006-11-01' 
    d1='2007-01-01'
    ud0=anytim2utc(d0)
    ud1=anytim2utc(d1)
    ndays=ud1.mjd-ud0.mjd
ENDIF ELSE BEGIN
    d0=trim(yr)+'-01-01'
    ndays=365
ENDELSE

FOR ab=0,1 DO BEGIN
    label='SSR1-'
    partdig='4'     ; for now SSR1 only
    aorb=aandb[ab]
    ablc=strlowcase(aorb)
    lossfs=file_search(concat_dir(getenv('scc'+ablc),'nrl/lz/outputs/prints/allsequenceerrors_'+aorb+'_'+trim(yr)+'*lz.rpt'))
    nf=n_elements(lossfs)
    first=1
    FOR i=0,nf-1 DO BEGIN
	fn=lossfs[i]
	rows=readlist(fn)
	g=where(strmid(rows,1,1) EQ partdig,ng)
	IF ng GT 0 THEN $
	IF first THEN BEGIN
	    rowsa=rows[g] 
	    first=0 
	ENDIF ELSE rowsa=[rowsa,rows[g]]
    ENDFOR
    help,rowsa
    cols=str2cols(rowsa)
    gapt=trim(reform(cols[1,*]))+'T'+trim(reform(cols[2,*]))
    gaput=anytim2utc(gapt)
    gapd=float(reform(cols[8,*]))	; minutes
    gapmp=long(reform(cols[7,*]))

    getsccarchiveinfo,tot,dtx,vec=tlmvol,udates=tlmut,/ssr1,/sci,/volume,spacecraft=aorb,/noplot, sta_date=d0,ndays=ndays
    tlmt=utc2str(tlmut)
    openw,1,'~/attic/missingtlmreport.txt',/append
    printf,1,'Date	    	Mbit Rcvd   	Percent Missed'
    printf,1,'=============================================='
    ;2008-01         123456789.0       123.567890%

    rformat='(a5,a-2,a-7,f11.1,f17.6,a1)'

    FOR i=1,12 do begin
	mm=string(i,format='(i02)')
	tlmmm=where(strmid(tlmt,5,2) EQ mm,ntlmmm)
	gapmm=where(strmid(gapt,5,2) EQ mm,ngapmm)
	IF ntlmmm GT 0 and ngapmm GT 0 THEN BEGIN
    	    tlm=total(tlmvol[tlmmm])*8     ;MB to Mb
	    gap=total(gapmp[gapmm])*2176/(1024.^2)	; packets to Mb
	    printf,1,'SSR1-',aorb,strmid(tlmt[tlmmm[0]],0,7),tlm,100.*gap/(gap+tlm),'%',format=rformat
	ENDIF
    ENDFOR

    ttlm=total(tlmvol)*8
    tgap=total(gapmp)*2176/(1024.^2)
    printf,1,'SSR1-',aorb,trim(yr),ttlm,100*tgap/(tgap+ttlm),'%',format=rformat

    close,1
endfor

end

    
