
;+
; $Id: comp_archive.pro,v 1.7 2012/04/25 19:13:10 secchia Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      :comp_archive.pro
;               
; Purpose   : compare size of played back level 0 archive files with most recent version
;               
; Explanation: Automatically finds files in $SDS_DIR directory based on lzpb input
;               
; Use       : IDL> 
;    
; Inputs    : 	sc = spacecraft A or B, yr = four digit year , day = DOY 
;
; Outputs   : appends sc(AorB)_arch.rpt 

; Optional Outputs: 
;
; Keywords  : 

; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database sci apid's
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Apr 06
;               
; $Log: comp_archive.pro,v $
; Revision 1.7  2012/04/25 19:13:10  secchia
; changed prep log file name
;
; Revision 1.6  2011/06/29 19:30:23  nathan
; changed maillist to secchipipeline@cronus
;
; Revision 1.5  2010/07/19 16:57:35  secchib
; changed ptp and fin log dirs added lzarchive
;
; Revision 1.4  2010/07/19 16:35:32  secchib
; changed ptp and fin log dirs
;
; Revision 1.3  2008/02/11 18:46:26  mcnutt
; only checks current year logs
;
; Revision 1.2  2007/09/25 18:23:34  mcnutt
; compares lzfinprep log along with the ptpprep log
;
; Revision 1.1  2007/08/24 18:07:09  mcnutt
; moved over from dev/packets for pipeline processing
;
; Revision 1.3  2007/06/18 19:48:03  nathan
; add year to output filename
;
; Revision 1.2  2007/05/18 16:06:50  nathan
; only report files to update; check latest version, ptp or fin; if difference found
; check lzptpprep.log for version actually used
;
; Revision 1.1  2007/04/18 16:59:08  mcnutt
; compares level_0_telemtry files ptp vs fin
;
;
;
pro comp_archive,sc0,yr,day,nomail=nomail

sc=strlowcase(sc0)
outpdir=getenv('SCC_DATA')+'/statistics/' 

if(strlowcase(sc) eq 'a')then sc1='ahead' else sc1='behind'
inpdir=getenv_slash('SDS_DIR')+sc1+'/data_products/archived_product_list/'
;ptppreplog=getenv('stereo')+'/itos/lzarchive/'+sc+'lzptpprep.log'
;finpreplog=getenv('stereo')+'/itos/lzarchive/'+sc+'lzfinprep.log'
ptppreplog='/net/calliope/data/secchi/sds/lzarchive/'+sc+'lzprep.log'
finpreplog='/net/calliope/data/secchi/sds/lzarchive/'+sc+'lzfinprep.log'

print,inpdir
yeardoy=string(yr,'(i4.4)')+'_'+string(day,'(i3.3)')

file1=getenv_slash('HOME')+'sc'+sc+'_arch_'+yeardoy+'.txt'

spawn, 'grep -h secchi_'+sc1+'_'+yeardoy+' '+inpdir+'st'+strlowcase(sc)+'*'+strmid(yeardoy,0,4)+'*.apl > '+file1
wait, 2

close,/all
file4=outpdir+string(yr,'(i4.4)')+'sc'+sc+'_arch.rpt'

openw,4,file4,width=120,/append

openr,1,file1
fs=fstat(1)
IF fs.size EQ 0 THEN BEGIN
	message,yeardoy+' not found, exitting',/info
	close,1
	return
ENDIF
line='' & lines=' '
while (not eof(1)) do begin
    readf,1,line
    lines=[lines,line]
endwhile
close,1
ptp=fltarr(6) & fin=fltarr(6)
ptpt=strarr(6) & fint=strarr(6) & stat=strarr(6)+' OK '
for n=0,5 do begin
    ; find all versions of file
    daypart='secchi_'+sc1+'_'+yeardoy+'_'+string(n+1,'(i1.1)')
    allver=where(strpos(lines,daypart) gt -1, nv)
	allvert=lines[allver]
    ; get version 02
	sv02=where(strpos(allvert,'02.ptp') GT -1,nv02)
    if (nv02 GT 0) then begin
    	ptpt(n)=allvert[sv02[nv02-1]]
    	ptp(n)=strmid(ptpt(n),strlen(ptpt(n))-10,10)
    endif else goto,nov02
	
    ; if no .fin file then use last ptp file
    fint(n)=allvert[nv-1]
    fin(n)=strmid(fint(n),strlen(fint(n))-10,10)
	
    print,fint[n]
    IF ptp[n] NE fin[n] THEN BEGIN
		; last .fin file NE 02.ptp. Now check ptpprep.log to see what version was actually used.
		print,'Checking ',daypart
		spawn,'grep '+daypart+' '+ptppreplog,res,/sh
		nres=n_elements(res)
		verused=strmid(res[nres-1],24,27)
		help,verused
		IF verused NE strmid(ptpt[n],0,27) and verused NE '' THEN BEGIN
	    	u=where(strpos(lines,verused) GE 0,nvu)
	    	IF nvu GT 0 THEN BEGIN
    			print,'Updating result.'
				ptp(n)=strmid(lines[u[nvu-1]],strlen(lines[u[nvu-1]])-10,10)
    			ptpt(n)=lines[u[nvu-1]]
    		endif
		ENDIF
   		wait,2

		print,'Checking for fin playback for ',daypart
		spawn,'grep '+daypart+' '+finpreplog,res,/sh
		nres=n_elements(res)
		verused=strmid(res[nres-1],24,27)
		help,verused
		IF verused NE strmid(ptpt[n],0,27) and verused NE '' THEN BEGIN
	    	u=where(strpos(lines,verused) GE 0,nvu)
	    	IF nvu GT 0 THEN BEGIN
    			print,'Updating result.'
				ptp(n)=strmid(lines[u[nvu-1]],strlen(lines[u[nvu-1]])-10,10)
    			ptpt(n)=lines[u[nvu-1]]
    		endif
		ENDIF
   		wait,2

    ENDIF


    nov02:
    if (nv02 EQ 0) then print,'APL file not up todate does not enclude V02 for '+yeardoy

endfor

pdbf=ptp/fin
; fraction of final in ptp v.02
n2=where(pdbf LT 1,nn2) 

; only print cases where .fin is bigger than v.02

if(nn2 GT 0)then begin 
    mailfile=getenv_slash('HOME')+'sc'+sc+'_arch_'+yeardoy+'.tmp'
    openw,5,mailfile
    for n=0,nn2-1 do BEGIN
       print,   'REDO '+fint[n2[n]]+' (+'+string(1.-pdbf[n2[n]],'(f4.2)')+')'
       printf,4,'REDO '+fint[n2[n]]+' (+'+string(1.-pdbf[n2[n]],'(f4.2)')+')'
       printf,5,'REDO '+fint[n2[n]]+' (+'+string(1.-pdbf[n2[n]],'(f4.2)')+')'
      wait,2
    endfor
    close,5
    repro_subj='REDO_'+sc1+'_'+yeardoy
    repro_mail='secchipipeline@cronus.nrl.navy.mil'
    if not(keyword_set(nomail)) THEN spawn,'mailx -s '+repro_subj+' '+repro_mail+' <'+mailfile
    wait,10
    spawn, '/bin/rm -f '+mailfile
ENDIF

spawn, '/bin/rm -f '+file1
close,4
end
