;+
; $Id: scc_tm_size.pro,v 1.2 2010/05/19 17:39:41 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_tm_size
;               
; Purpose   : This function returns original telemetry size of level-0.5 FITS image
;             computed from header information in bytes or bits.
;               
; Explanation: The function calls scc_get_missing to find image index 
;              all superpixels that are missing and subtracts the missing
;              pixels from calculated image size and applies compfact to
;              get actual downloaded image size. 
;               
; Use       : IDL> bytes = scc_tm_size(hdr)
;    
; Inputs    : hdr -image header, either fits or SECCHI structure
;
; Outputs   : bytes - number of actual bytes or bits downloaded
;
; Keywords  : bits - return number of bits (instead of bytes)
;   	    /ACTUAL - Get actual raw image/sci file size
;
; Restictions: 
;
; Calls     : SCC_GET_MISSING 
;            
;               
; Category    : Telemetry 
;               
; Prev. Hist. : None.
;
; Written     : Ed Esfandiari NRL/Interferometrics, Inc. August 2008
;
; $Log: scc_tm_size.pro,v $
; Revision 1.2  2010/05/19 17:39:41  nathan
; added /ACTUAL
;
; Revision 1.1  2008/09/23 20:23:07  esfand
; secchi TM statistics routines
;
; Revision 1.1  2008/08/13 19:33:05  esfand
; return telemetry size of an image
;
;-


FUNCTION scc_tm_size,hdr,bits=bits, ACTUAL=actual

  IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF keyword_set(ACTUAL) THEN BEGIN
    scidir=parse_stereo_name(hdr.obsrvtry,['scia','scib'])
    yymmdd=utc2yymmdd(anytim2utc(hdr.date_cmd))
    bytes=file_stat(concat_dir(getenv_slash(scidir)+yymmdd,hdr.fileorig),/size)
ENDIF ELSE BEGIN
  missing_pixels= 0L
  IF (hdr.nmissing gt 0) THEN $ 
    missing_pixels= N_ELEMENTS(SCC_GET_MISSING(hdr,/silent))
  bytes= LONG((LONG(hdr.naxis1)*hdr.naxis2 - missing_pixels)/(1 > hdr.compfact) * hdr.bitpix/8) 
ENDELSE

  IF KEYWORD_SET(bits) THEN $
    RETURN, bytes*8         $
  ELSE                      $
    RETURN, bytes 

END
