pro plotencoders, times, tel, sc, mech, hdrs, NOPLOT=noplot, psym=psym, png=png, jpg=jpg
; $Id: plotencoders.pro,v 1.3 2012/11/06 17:26:54 mcnutt Exp $
;
; Project:  STEREO SECCHI
;
; Purpose: Plot encoder positions from header for input mechanism and time interval
;   	    
; Inputs:
;	times	STRING single date to get info for (nrlanytim2utc format)
;   	    -or-
;   	    	STRARR[2]   Start and end of interval
;   	tel 	STRING cor1, cor2, euvi
;   	sc  	STRING a, b
;   	mech	STRING	shtr, pol, wave
;
; optional Outputs:
;   	hdrs	array of header structures with one element for each exposure in interval
; 
; Keyword Inputs:
;   	/NOPLOT     Do not draw plot, read in headers only.
;   	psym = plot symbol
;   	png   write plot to a png file
;   	jpg   write plot to a jpg file
;
;
; Procedure:
;   	Plots encoder position for each commanded position separately on same plot.
;   	Includes extended header info, where applicable.
;
; Restrictions: 
;
; $Log: plotencoders.pro,v $
; Revision 1.3  2012/11/06 17:26:54  mcnutt
; corrected png/jpg name added keyword definitions
;
; Revision 1.2  2012/11/06 16:32:20  mcnutt
; New program
;
; Revision 1.1  2012/11/05 18:47:00  nathan
; stub
;
if keyword_set(psym) then psym=psym else psym=0
minihdr={ POLAR:0.0,$
   ENCODERP:0 ,$
   WAVELNTH :0,$
   ENCODERQ :0 ,$
   FILTER :'',$
   ENCODERF :0  ,$
   SHUTTDIR  :'' ,$
   SHUTTER  :'' ,$
   EXPCMD :0.0 ,$
   EXPTIME :0.0 ,$
   DATE_CLR :'' ,$
   DATE_RO :'' } 
 
s=scc_read_summary(date=times,tel=strlowcase(tel),spacec=strlowcase(sc),/nobeacon)
for n=0,n_elements(s)-1 do begin
  fname=sccfindfits(s(n).filename)
  im=sccreadfits(fname,hdr,/nodata)
  if hdr.extend eq 'T' then begin
    exthdr=1
    im=sccreadfits(fname,hdr,/nodata,exthdr=exthdr)
    mhdr=replicate(minihdr,n_elements(exthdr))
    mhdr(0).POLAR=hdr.POLAR
    mhdr(0).ENCODERP=hdr.ENCODERP
    mhdr(0).WAVELNTH=hdr.WAVELNTH
    mhdr(0).ENCODERQ=hdr.ENCODERQ
    mhdr(0).FILTER=hdr.FILTER 
    mhdr(0).ENCODERF=hdr.ENCODERF
    mhdr(0).SHUTTDIR=hdr.SHUTTDIR
    mhdr(0).EXPCMD =hdr.EXPCMD
    mhdr(0).EXPTIME=hdr.EXPTIME
    mhdr(0).DATE_CLR=hdr.DATE_CLR
    mhdr(0).DATE_RO=hdr.DATE_RO
    mhdr.EXPTIME =exthdr.EXPTIME
    mhdr.DATE_CLR =exthdr.DATE_CLR
    mhdr.DATE_RO =exthdr.DATE_RO
    mhdr.POLAR =exthdr.POLAR
    mhdr.ENCODERP =exthdr.ENCODER
    mhdr.SHUTTER =exthdr.SHUTTER
  endif else begin
    mhdr=minihdr
    mhdr.POLAR=hdr.POLAR
    mhdr.ENCODERP=hdr.ENCODERP
    mhdr.WAVELNTH=hdr.WAVELNTH
    mhdr.ENCODERQ=hdr.ENCODERQ
    mhdr.FILTER=hdr.FILTER 
    mhdr.ENCODERF=hdr.ENCODERF
    mhdr.SHUTTDIR=hdr.SHUTTDIR
    mhdr.EXPCMD =hdr.EXPCMD
    mhdr.EXPTIME=hdr.EXPTIME
    mhdr.DATE_CLR=hdr.DATE_CLR
    mhdr.DATE_RO=hdr.DATE_RO
  endelse
  if n eq 0 then hdrs=mhdr else hdrs=[hdrs,mhdr]
endfor
if ~keyword_set(noplot) then begin
  if strlowcase(mech) eq 'shtr' then $
    utplot,hdrs.date_ro,hdrs.EXPTIME,yrange=[min(hdrs.exptime)-0.5,max(hdrs.exptime)+0.5],$
    ystyle=1,title=strupcase(tel)+'-'+strupcase(sc)+' Exposure Time',xstyle=1,background=255,color=0,psym=psym
  if strlowcase(mech) eq 'pol' then $
    utplot,hdrs.date_ro,hdrs.encoderp,yrange=[min(hdrs.encoderp)-5,max(hdrs.encoderp)+5],$
    ystyle=1,title=strupcase(tel)+'-'+strupcase(sc)+' Polarizer Encoder Position',xstyle=1,background=255,color=0,psym=psym
  if strlowcase(mech) eq 'wave' then $
    utplot,hdrs.date_ro,hdrs.encoderq,yrange=[min(hdrs.encoderq)-5,max(hdrs.encoderq)+5],$
    ystyle=1,title=strupcase(tel)+'-'+strupcase(sc)+' Quadrant Encoder Position',xstyle=1,background=255,color=0,psym=psym
  if keyword_set(png) or keyword_set(jpg) then begin
    fname=strupcase(tel)+strupcase(sc)+'_'+mech+'_'+strmid(hdrs(0).date_ro,0,4)+strmid(hdrs(0).date_ro,5,2)+strmid(hdrs(0).date_ro,8,2)
    print,'Writting plot file - '+fname
    img=fTVREAD(png=png,jpg=jpg, filename=fname,/noprompt)
  endif
endif

end
