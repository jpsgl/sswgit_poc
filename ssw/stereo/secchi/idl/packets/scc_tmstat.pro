;+
; $Id: scc_tmstat.pro,v 1.2 2010/02/24 16:00:35 esfand Exp $
;
; Project   : STEREO SECCHI
;
; NAME:
;       SCC_TMSTAT 
;
; PURPOSE:
;        This subroutine plots percentages of EUVI, COR1, COR2, HI1, and HI2 
;        telemetry bytes for all downlinks (SSR1, SW, RT) data for a given 
;        date-range as a percentage of total telemetery bytes on a daily, or
;        monthly basis. SECCHI-A and SECCHI-B TM info are displayed side-by-side.
;
; CATEGORY:
;       UTIL
;
; CALLING SEQUENCE:
;        SCC_TMSTAT 
;
; INPUTS:
;        None
;
; KEYWORD PARAMETERS:
;       None
;
; OUTPUTS:
;        None
;
; SIDE EFFECTS:
;       Plot to a printer or save to a file.
;
; RESTRICTIONS:
;
; CALLS: GETSCCARCHIVEINFO
;
; EXAMPLE:
;       scc_tmstat
;
; MODIFICATION HISTORY:
;       Written by:     Ed Esfandiari, NRL/Interferometrics, August 2008
;           Sep 13, 08  Ed Esfandiari Added code to handle 8 and 24-bit machines
;                                     so that plots on the screen as well as
;                                     saved gif files have color.
;           Feb 24, 10  Ed Esfandiari Changed p.multi to froce column oriented plots.
;
;
; $Log: scc_tmstat.pro,v $
; Revision 1.2  2010/02/24 16:00:35  esfand
; Changed p.multi
;
; Revision 1.1  2008/09/23 20:23:07  esfand
; secchi TM statistics routines
;
; Revision 1.1  2008/08/15 18:40:49  esfand
; Plot TM statistics per instrument - aee
;
;-
;

pro box,x0,y0,x1,y1,color
  polyfill,[x0,x0,x1,x1],[y0,y1,y1,y0],col=color
  return
end




pro box_plot,ps_plt=ps_plt,multi=multi
  common scctm_share,selected_img,fname,save,save_req,img,prof_win,$
              res_types,old_query,old_s,rtype,query,polar,ptype,$
              x_arr,y_arr,plot_cnt,plot_info,last_press,beg_date,end_date, $
              cnts,img_cnt,image_dates,pdates,flt_str,pol_str,img_type,ored,ogreen,oblue,$
              stypes,stype,snames,sname,tot_bytes_a,eu_bytes_a,c1_bytes_a,c2_bytes_a,$
              h1_bytes_a,h2_bytes_a,tot_bytes_b,eu_bytes_b,c1_bytes_b,c2_bytes_b,h1_bytes_b,h2_bytes_b

  if(cnts eq -1) then begin
    print,'*** NO data for either or both SC-A SC-B instruments ***'
    return
  endif

  z= where(tot_bytes_a eq 0.0,zcnt)
  if(zcnt gt 0) then begin
    eu_bytes_a(z)= 0.0
    c1_bytes_a(z)= 0.0
    c2_bytes_a(z)= 0.0
    h1_bytes_a(z)= 0.0
    h2_bytes_a(z)= 0.0
    tot_bytes_a(z)= 0.00001 
  end
  z= where(tot_bytes_b eq 0.0,zcnt)
  if(zcnt gt 0) then begin
    eu_bytes_b(z)= 0.0
    c1_bytes_b(z)= 0.0
    c2_bytes_b(z)= 0.0
    h1_bytes_b(z)= 0.0
    h2_bytes_b(z)= 0.0
    tot_bytes_b(z)= 0.00001 
  end
  eupa= eu_bytes_a/tot_bytes_a*100.0
  c1pa= c1_bytes_a/tot_bytes_a*100.0
  c2pa= c2_bytes_a/tot_bytes_a*100.0
  h1pa= h1_bytes_a/tot_bytes_a*100.0
  h2pa= h2_bytes_a/tot_bytes_a*100.0
  eupb= eu_bytes_b/tot_bytes_b*100.0
  c1pb= c1_bytes_b/tot_bytes_b*100.0
  c2pb= c2_bytes_b/tot_bytes_b*100.0
  h1pb= h1_bytes_b/tot_bytes_b*100.0
  h2pb= h2_bytes_b/tot_bytes_b*100.0
 
  totbytes_a= total(tot_bytes_a)
  eubytes_a=  total(eu_bytes_a)
  c1bytes_a=  total(c1_bytes_a)
  c2bytes_a=  total(c2_bytes_a)
  h1bytes_a=  total(h1_bytes_a)
  h2bytes_a=  total(h2_bytes_a)
  totbytes_b= total(tot_bytes_b)
  eubytes_b=  total(eu_bytes_b)
  c1bytes_b=  total(c1_bytes_b)
  c2bytes_b=  total(c2_bytes_b)
  h1bytes_b=  total(h1_bytes_b)
  h2bytes_b=  total(h2_bytes_b)
  
  if(img_type eq 0) then $ 
    src= ' (src=QL)' $
  else $
    src= ' (src=LZ)'
  src=''

if(keyword_set(multi)) then begin
  !x.charsize=1.4
  !y.charsize=1.4
  !p.charsize=1.4
endif else begin
  !x.charsize=1.0
  !y.charsize=1.0
  !p.charsize=1.0
endelse

  dates= pdates.mjd - pdates(0).mjd + 1
  if datatype(dates) eq 'LON' then dates= [dates]
  img_dates= strmid(image_dates,2,8) ; remove 19 from 199x/xx/xx 
  ;img_dates= strmid(img_dates,3,5)+'/'+strmid(img_dates,0,2)
  img_dates= strmid(img_dates,3,5)+'-'+strmid(img_dates,0,2)
  cnt= cnts
!y.style=1
  if(cnt eq 0) then begin ; only one day of data
    cnt= 1   
    !x.range= [dates(0)-0.5,dates(0)+0.5] 
;    !y.range= [0,100]
    !y.range= [-1,101]
;    !x.ticks= cnt
    !x.tickname= ' ' ;only 1 date->ticks =0 (IDL default for many)
;    !x.tickv= dates  ;ticks) so use xyouts instead of tickname
    !x.ticks= 2
    !x.tickv= 0.5
    !xtitle= img_dates(0)
;    xyouts,295,23,img_dates,/dev,color=1
   if(keyword_set(multi)) then begin
    ;!p.multi=[0,2,2]
    ; !p.multi=[0,2,5]
    !p.multi=[0,2,5,0,1] ; last 1 is to force plots from top to bottom and from left to right.
    xoff= 0.2
    !mtitle='EUVI-A TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,eupa(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(eubytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(eubytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR1-A TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c1pa(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(c1bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(c1bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR2-A TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c2pa(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(c2bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(c2bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2
    !mtitle='HI1-A TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h1pa(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(h1bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(h1bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI2-A TM Percentage'+src
    ;plot,dates,[0,100.0,0],/nodata,color=1,subtitle="Total TM-A: "+string(totbytes_a,'(f11.2)')+" MB"
    plot,dates,[0,100.0,0],/nodata,color=1,xtitle="Total TM-A: "+string(totbytes_a,'(f11.2)')+" MB"
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h2pa(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(h2bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(h2bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='EUVI-B TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,eupb(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(eubytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(eubytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR1-B TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c1pb(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(c1bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(c1bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR2-B TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c2pb(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(c2bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(c2bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI1-B TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h1pb(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(h1bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(h1bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI2-B TM Percentage'+src
    ;plot,dates,[0,100.0,0],/nodata,color=1,subtitle="Total TM-B: "+string(totbytes_B,'(f11.2)')+" MB"
    plot,dates,[0,100.0,0],/nodata,color=1,xtitle="Total TM-B: "+string(totbytes_B,'(f11.2)')+" MB"
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h2pb(i),1
    xyouts,1.0,85,'Tot: '+strtrim(string(h2bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1.0,70,'Avg: '+string(h2bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
   endif else begin
    !mtitle='SC-A TM downlink usage'
    ;!p.multi=[0,1,1]
    ;!p.multi=[0,2,1]
    !p.multi=[0,2,1,0,1] ; last 1 is to force plots from top to bottom and from left to right.
    plot,dates,c1_bytes_a,/nodata,color=1
    oplot,dates,eupa,psym=1,color=6
    oplot,dates,c1pa,psym=2,color=2
    oplot,dates,c2pa,psym=4,color=3
    oplot,dates,h1pa,psym=5,color=5
    oplot,dates,h2pa,psym=6,color=7
    !mtitle='SC-B TM downlink usage'
    plot,dates,c1_bytes_b,/nodata,color=1
    oplot,dates,eupb,psym=1,color=6
    oplot,dates,c1pb,psym=2,color=2
    oplot,dates,c2pb,psym=4,color=3
    oplot,dates,h1pb,psym=5,color=5
    oplot,dates,h2pb,psym=6,color=7
   endelse
  endif else begin 
    !x.range= [dates(0)-0.5,dates(cnt-1)+0.5]
;    !y.range=[0,100]
    !y.range= [-1,101]
    if(dates(cnt-1)-dates(0) gt 8 and res_types(rtype) eq 'Daily') then begin 
      ; for more than 9 days, use only 3 x-axis dates (first, mid, last)
      d= [dates(0),(dates(cnt-1)+1)/2,dates(cnt-1)]
      !x.tickv= d 
      !x.ticks= 2 
      d= fix(d)-1
      date1= {mjd:0L,time:0l}
      date3= replicate(date1,3) 
      date3.mjd= d+pdates(0).mjd
      yymmdd= utc2yymmdd(date3)
      if(res_types(rtype) eq 'Daily') then begin
        !x.tickname= strmid(yymmdd,2,2)+'/'+strmid(yymmdd,4,2)+'/'+ $
                     strmid(yymmdd,0,2)  ; change to mm/dd/yy format
      endif else begin ; 'Monthly' resolution
        !x.tickname= strmid(yymmdd,4,2)+'/'+strmid(yymmdd,0,2) ; change to mm/yy
      endelse
    endif else begin
      !x.ticks= cnt-1
      !x.tickv= dates 
      !x.tickname= img_dates 
    endelse
   if(keyword_set(multi)) then begin
    ;!p.multi=[0,2,2]
    ;!p.multi=[0,2,5]
    !p.multi=[0,2,5,0,1] ; last 1 is to force plots from top to bottom and from left to right.
    xoff= 0.45
    !mtitle='EUVI-A TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,eupa(i),1
    xyouts,1,85,'Tot: '+strtrim(string(eubytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(eubytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR1-A TM Percentage'+src 
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c1pa(i),1 
    xyouts,1,85,'Tot: '+strtrim(string(c1bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(c1bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR2-A TM Percentage'+src 
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c2pa(i),1
    xyouts,1,85,'Tot: '+strtrim(string(c2bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(c2bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI1-A TM Percentage'+src 
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h1pa(i),1
    xyouts,1,85,'Tot: '+strtrim(string(h1bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(h1bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI2-A TM Percentage'+src 
    ;plot,dates,[0,100.0,0],/nodata,color=1,subtitle="Total TM-A: "+string(totbytes_a,'(f11.2)')+" MB"
    plot,dates,[0,100.0,0],/nodata,color=1,xtitle="Total TM-A: "+string(totbytes_a,'(f11.2)')+" MB"
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h2pa(i),1
    xyouts,1,85,'Tot: '+strtrim(string(h2bytes_a,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(h2bytes_a/totbytes_a*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0

    !mtitle='EUVI-B TM Percentage'+src
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,eupb(i),1
    xyouts,1,85,'Tot: '+strtrim(string(eubytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(eubytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR1-B TM Percentage'+src 
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c1pb(i),1 
    xyouts,1,85,'Tot: '+strtrim(string(c1bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(c1bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='COR2-B TM Percentage'+src 
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,c2pb(i),1
    xyouts,1,85,'Tot: '+strtrim(string(c2bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(c2bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI1-B TM Percentage'+src 
    plot,dates,[0,100.0,0],/nodata,color=1
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h1pb(i),1
    xyouts,1,85,'Tot: '+strtrim(string(h1bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(h1bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0
    !mtitle='HI2-B TM Percentage'+src 
    ;plot,dates,[0,100.0,0],/nodata,color=1,subtitle="Total TM-B: "+string(totbytes_B,'(f11.2)')+" MB"
    plot,dates,[0,100.0,0],/nodata,color=1,xtitle="Total TM-B: "+string(totbytes_B,'(f11.2)')+" MB"
    for i=0,cnt-1 do box,dates(i)-xoff,0, dates(i)+xoff,h2pb(i),1
    xyouts,1,85,'Tot: '+strtrim(string(h2bytes_b,'(f10.3)'),2)+" MB",color=2,charsize=1.0
    xyouts,1,70,'Avg: '+string(h2bytes_b/totbytes_b*100.0,'(f5.1)')+'% of total TM',color=2,charsize=1.0

   endif else begin
    !mtitle='SC-A TM downlink usage'
    ;!p.multi=[0,1,1]
    ;!p.multi=[0,2,1]
    !p.multi=[0,2,1,0,1] ; last 1 is to force plots from top to bottom and from left to right.
    plot,dates,c1_bytes_a,/nodata,color=1
    oplot,dates,eupa,psym=1,color=6
    oplot,dates,c1pa,psym=2,color=2
    oplot,dates,c2pa,psym=4,color=3
    oplot,dates,h1pa,psym=5,color=5
    oplot,dates,h2pa,psym=6,color=7
    !mtitle='SC-B TM downlink usage'
    plot,dates,c1_bytes_b,/nodata,color=1
    oplot,dates,eupb,psym=1,color=6
    oplot,dates,c1pb,psym=2,color=2
    oplot,dates,c2pb,psym=4,color=3
    oplot,dates,h1pb,psym=5,color=5
    oplot,dates,h2pb,psym=6,color=7
   endelse
  endelse

  if(keyword_set(ps_plt)) then begin
   if(not keyword_set(multi)) then begin
    xyouts,0,0,"Total TM-A,B: "+strtrim(string(totbytes_a,'(f11.2)'),2)+','+ $
                                strtrim(string(totbytes_b,'(f11.2)'),2)+"MB",/dev,color=1,charsize=1.0
    xyouts, ", EUVI: "+strtrim(string(eubytes_a,'(f10.1)'),2)+','+ $
                       strtrim(string(eubytes_b,'(f10.1)'),2)+"MB ("+ $
                       strtrim(string(eubytes_a/totbytes_a*100.0,'(f5.1)'),2)+"%,"+ $
                       strtrim(string(eubytes_b/totbytes_b*100.0,'(f5.1)'),2)+"%)",/dev,color=1,charsize=1.0
    xyouts, ", COR1: "+strtrim(string(c1bytes_a,'(f10.1)'),2)+','+ $
                       strtrim(string(c1bytes_b,'(f10.1)'),2)+"MB ("+ $
                       strtrim(string(c1bytes_a/totbytes_a*100.0,'(f5.1)'),2)+"%,"+ $ 
                       strtrim(string(c1bytes_b/totbytes_b*100.0,'(f5.1)'),2)+"%)",/dev,color=1,charsize=1.0
    xyouts, ", COR2: "+strtrim(string(c2bytes_a,'(f10.1)'),2)+','+ $
                       strtrim(string(c2bytes_b,'(f10.1)'),2)+"MB ("+ $
                       strtrim(string(c2bytes_a/totbytes_a*100.0,'(f5.1)'),2)+"%,"+ $
                       strtrim(string(c2bytes_b/totbytes_b*100.0,'(f5.1)'),2)+"%)",/dev,color=1,charsize=1.0
    xyouts, ", HI1: "+ strtrim(string(h1bytes_a,'(f10.1)'),2)+','+ $
                       strtrim(string(h1bytes_b,'(f10.1)'),2)+"MB ("+ $
                       strtrim(string(h1bytes_a/totbytes_a*100.0,'(f5.1)'),2)+"%,"+ $
                       strtrim(string(h1bytes_b/totbytes_b*100.0,'(f5.1)'),2)+"%)",/dev,color=1,charsize=1.0
    xyouts, ", HI2: "+strtrim(string(h2bytes_a,'(f10.1)'),2)+','+ $
                       strtrim(string(h2bytes_b,'(f10.1)'),2)+"MB ("+ $
                       strtrim(string(h2bytes_a/totbytes_a*100.0,'(f5.1)'),2)+"%,"+ $
                       strtrim(string(h2bytes_b/totbytes_b*100.0,'(f5.1)'),2)+"%)",/dev,color=1,charsize=1.0

   end
  endif else begin
   if(not keyword_set(multi)) then begin
    ad=320
    xyouts,90,280+ad,"Total TM:  "+string(totbytes_a,'(f11.2)')+" MB",/dev,color=1
    xyouts,90,260+ad,"  EUVI TM:   "+string(eubytes_a,'(f10.2)')+" MB  ("+string(eubytes_a/totbytes_a*100.0,'(f5.1)')+"%)",/dev,color=6,charsize=1.0
    xyouts,90,240+ad,"  COR1 TM:   "+string(c1bytes_a,'(f10.2)')+" MB  ("+string(c1bytes_a/totbytes_a*100.0,'(f5.1)')+"%)",/dev,color=2,charsize=1.0
    xyouts,90,220+ad,"  COR2 TM:   "+string(c2bytes_a,'(f10.2)')+" MB  ("+string(c2bytes_a/totbytes_a*100.0,'(f5.1)')+"%)",/dev,color=3,charsize=1.0
    xyouts,90,200+ad,"  HI1 TM:   "+string(h1bytes_a,'(f10.2)')+" MB  ("+string(h1bytes_a/totbytes_a*100.0,'(f5.1)')+"%)",/dev,color=5,charsize=1.0
    xyouts,90,180+ad,"  HI2 TM:   "+string(h2bytes_a,'(f10.2)')+" MB  ("+string(h2bytes_a/totbytes_a*100.0,'(f5.1)')+"%)",/dev,color=7,charsize=1.0


    xyouts,ad+100,280+ad,"Total TM:  "+string(totbytes_b,'(f11.2)')+" MB",/dev,color=1,charsize=1.0
    xyouts,ad+100,260+ad,"  EUVI TM:   "+string(eubytes_b,'(f10.2)')+" MB  ("+string(eubytes_b/totbytes_b*100.0,'(f5.1)')+"%)",/dev,color=6,charsize=1.0
    xyouts,ad+100,240+ad,"  COR1 TM:   "+string(c1bytes_b,'(f10.2)')+" MB  ("+string(c1bytes_b/totbytes_b*100.0,'(f5.1)')+"%)",/dev,color=2,charsize=1.0
    xyouts,ad+100,220+ad,"  COR2 TM:   "+string(c2bytes_b,'(f10.2)')+" MB  ("+string(c2bytes_b/totbytes_b*100.0,'(f5.1)')+"%)",/dev,color=3,charsize=1.0
    xyouts,ad+100,200+ad,"  HI1 TM:   "+string(h1bytes_b,'(f10.2)')+" MB  ("+string(h1bytes_b/totbytes_b*100.0,'(f5.1)')+"%)",/dev,color=5,charsize=1.0
    xyouts,ad+100,180+ad,"  HI2 TM:   "+string(h2bytes_b,'(f10.2)')+" MB  ("+string(h2bytes_b/totbytes_b*100.0,'(f5.1)')+"%)",/dev,color=7,charsize=1.0

   end
  endelse

  minval= !y.crange(0)

  return
end




PRO IMG_SELECT_EVENT, event
common scctm_share,selected_img,fname,save,save_req,img,prof_win,$
              res_types,old_query,old_s,rtype,query,polar,ptype,$
              x_arr,y_arr,plot_cnt,plot_info,last_press,beg_date,end_date, $
              cnts,img_cnt,image_dates,pdates,flt_str,pol_str,img_type,ored,ogreen,oblue,$
              stypes,stype,snames,sname,tot_bytes_a,eu_bytes_a,c1_bytes_a,c2_bytes_a,$
              h1_bytes_a,h2_bytes_a,tot_bytes_b,eu_bytes_b,c1_bytes_b,c2_bytes_b,h1_bytes_b,h2_bytes_b
 
WIDGET_CONTROL, event.id, GET_UVALUE=uval
WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE


help_info=[$
   'This program plots all SECCHI-A&B raw telemetry for all ten instruments',$
   'and all downlinks (SSR, SW, RT) in a daily or monthly basis for the provided',$
   'date-range. It also displays the total telemetry bytes and its distribution',$
   'amongst the five instruments of SECCHI-A&B for the entire period.',$
   '',$
   'To display the data on a daily basis, select the "Daily" resolution" (default).',$
   'To display the data on a monthly (calendar) basis, use the "Monthly" resolution',$
   'option.',$
   '',$
   'The "Histogram" plot option (default) divides the screen in two columns of',$
   'five rows. The EUVI, COR1, COR2, HI1, and HI2 percenatges in histogram mode',$
   'appear in left column for SECCHI-A and right column for SECCHI-B.',$
   '',$
   'The "Symbol" option plots all the percentages for SECCHI-A instruments in the',$
   'same plot in left column and for SECCHI-B instruments in right column. Different',$
   'symbols and colors are used to distinguish each instrument.',$
   '',$ 
   'The currently displayed plot can be printed, or saved as a postscript or',$
   'gif file. Simply select a printer or a postscript or gif filename before',$
   'clicking on the "Print Plot" or "Save Plot" buttons.']

select_info=[$
   ' No selection.']

;print,'uval= ',uval
 case (uval) of
  'Done': begin ; exit the program but delete the plot file first, if any.
                  if(last_press eq 'prn') then wait,3
                  openw,unit,'ff_img_files.ps',/get_lun,/delete
                  close,unit
                  free_lun,unit
                  TVLCT,255*ored,255*ogreen,255*oblue
                  ;if 24-bit machine, reset decomposed to 1 (from 0)
                  DEVICE,get_visual_depth=bits
                  IF (bits gt 8) THEN device,decomposed=1
             WIDGET_CONTROL, /DESTROY, struct.base
          end
  'Help': begin
             last_press= 'help'
             save_req= 0 ;if this event selected, ignore the savescreen req.
             xdisplayfile,TITLE='Help',$
             GROUP=event.top, HEIGHT=15, WIDTH=85, $
             TEXT= help_info
          end 

  'proc': begin ; proceed with retrieving data and plotting
            last_press= 'proc'
            WIDGET_CONTROL,/hour
            old_query= query

; Note: for 1-day of data, getsccarchiveinfo generates "Floating divide by 0" and
;       "Floating illegal operand" errors but result volumes returned are OK:

            euvi_vol_a= 0
            cor1_vol_a= 0
            cor2_vol_a= 0
            hi1_vol_a= 0
            hi2_vol_a= 0
            euvi_vol_b= 0
            cor1_vol_b= 0
            cor2_vol_b= 0
            hi1_vol_b= 0
            hi2_vol_b= 0
            euvi_date_a= ''
            euvi_date_b= ''

            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='euvi',/volume,/sci,/noplot,vec=euvi_vol_a,udate=euvi_date_a,spacecraft='A'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='cor1',/volume,/sci,/noplot,vec=cor1_vol_a,udate=cor1_date_a,spacecraft='A'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='cor2',/volume,/sci,/noplot,vec=cor2_vol_a,udate=cor2_date_a,spacecraft='A'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='hi1',/volume,/sci,/noplot,vec=hi1_vol_a,udate=hi1_date_a,spacecraft='A'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='hi2',/volume,/sci,/noplot,vec=hi2_vol_a,udate=hi2_date_a,spacecraft='A'

            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='euvi',/volume,/sci,/noplot,vec=euvi_vol_b,udate=euvi_date_b,spacecraft='B'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='cor1',/volume,/sci,/noplot,vec=cor1_vol_b,udate=cor1_date_b,spacecraft='B'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='cor2',/volume,/sci,/noplot,vec=cor2_vol_b,udate=cor2_date_b,spacecraft='B'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='hi1',/volume,/sci,/noplot,vec=hi1_vol_b,udate=hi1_date_b,spacecraft='B'
            getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=beg_date,ndays=end_date,telescope='hi2',/volume,/sci,/noplot,vec=hi2_vol_b,udate=hi2_date_b,spacecraft='B'

            if(datatype(euvi_date_a) eq 'STR' or datatype(euvi_date_b) eq 'STR') then begin
              print,'*** NO data for either or both SC-A SC-B instruments ***'
              return
            end

           if(old_query ne query) then begin
              !p.multi=[0,1,1]  ; single plot
            endif else begin 
            endelse

            tot_vol_a=euvi_vol_a+cor1_vol_a+cor2_vol_a+hi1_vol_a+hi2_vol_a
            tot_vol_b=euvi_vol_b+cor1_vol_b+cor2_vol_b+hi1_vol_b+hi2_vol_b

            ;Figure out how many images per day:
            ;s= strmid(s.date_obs,0,10)
            !x.tickname= ' ' 
            !x.tickv= 0  
            !x.style=1

            if(img_type eq 0) then it= 'QL'
            if(img_type eq 1) then it= 'LZ'
            mt= res_types(rtype)+' telemetry percentages for each camera (using '+it+' files)'
            !mtitle= mt 
            !ytitle= '% of total TM'
;            !xtitle= 'Observation Dates'
            !ymin=0
            !ymax= 100

            s_a= utc2str(euvi_date_a)
            s_a= strmid(s_a,0,10)
            if(s_a(0) ne '') then begin ; qdb returned data.
              cnts= 0
              if(res_types(rtype) eq 'Monthly') then s_a= strmid(s_a,0,7)  ; only keep yyyy/mm of date_obs
              if(n_elements(s_a) gt 1) then pos= where(s_a ne shift(s_a,1),cnts)
              if(cnts gt 0) then begin ; multiple days images
               img_cnt= shift(pos,-1)-pos
               img_cnt(cnts-1)= n_elements(s_a)+img_cnt(cnts-1) 
               image_dates= s_a(pos)
               tot_bytes_a= fltarr(cnts)
               eu_bytes_a= fltarr(cnts)
               c1_bytes_a= fltarr(cnts)
               c2_bytes_a= fltarr(cnts)
               h1_bytes_a= fltarr(cnts)
               h2_bytes_a= fltarr(cnts)
               for i=0,cnts-1 do begin
                 bytes= tot_vol_a(pos(i):pos(i)+img_cnt(i)-1)
                 tot_bytes_a(i)= total(bytes)
                 eu_bytes_a(i)= 0.0
                 bytes= euvi_vol_a(pos(i):pos(i)+img_cnt(i)-1)
                 eu_bytes_a(i)= total(bytes)
                 c1_bytes_a(i)= 0.0
                 bytes= cor1_vol_a(pos(i):pos(i)+img_cnt(i)-1)
                 c1_bytes_a(i)= total(bytes)
                 c2_bytes_a(i)= 0.0
                 bytes= cor2_vol_a(pos(i):pos(i)+img_cnt(i)-1)
                 c2_bytes_a(i)= total(bytes)
                 h1_bytes_a(i)= 0.0
                 bytes= hi1_vol_a(pos(i):pos(i)+img_cnt(i)-1)
                 h1_bytes_a(i)= total(bytes)
                 h2_bytes_a(i)= 0.0
                 bytes= hi2_vol_a(pos(i):pos(i)+img_cnt(i)-1)
                 h2_bytes_a(i)= total(bytes)
               end
              endif else begin ; only one day of images
               img_cnt= [n_elements(s_a)]
               tot_bytes_a= tot_vol_a
               eu_bytes_a= euvi_vol_a
               c1_bytes_a= cor1_vol_a
               c2_bytes_a= cor2_vol_a
               h1_bytes_a= hi1_vol_a
               h2_bytes_a= hi2_vol_a
               image_dates= s_a(0)
              endelse
              image_dates_a= image_dates

;help,eu_bytes_a,c1_bytes_a,c2_bytes_a,h1_bytes_a,h2_bytes_a,image_dates
;stop

              s_b= utc2str(euvi_date_b)
              s_b= strmid(s_b,0,10)
              cnts= 0
              if(res_types(rtype) eq 'Monthly') then s_b= strmid(s_b,0,7)  ; only keep yyyy/mm of date_obs
              if(n_elements(s_b) gt 1) then pos= where(s_b ne shift(s_b,1),cnts)
              if(cnts gt 0) then begin ; multiple days images
               img_cnt= shift(pos,-1)-pos
               img_cnt(cnts-1)= n_elements(s_b)+img_cnt(cnts-1) 
               image_dates= s_b(pos)
               tot_bytes_b= fltarr(cnts)
               eu_bytes_b= fltarr(cnts)
               c1_bytes_b= fltarr(cnts)
               c2_bytes_b= fltarr(cnts)
               h1_bytes_b= fltarr(cnts)
               h2_bytes_b= fltarr(cnts)
               for i=0,cnts-1 do begin
                 bytes= tot_vol_b(pos(i):pos(i)+img_cnt(i)-1)
                 tot_bytes_b(i)= total(bytes)
                 eu_bytes_b(i)= 0.0
                 bytes= euvi_vol_b(pos(i):pos(i)+img_cnt(i)-1)
                 eu_bytes_b(i)= total(bytes)
                 c1_bytes_b(i)= 0.0
                 bytes= cor1_vol_b(pos(i):pos(i)+img_cnt(i)-1)
                 c1_bytes_b(i)= total(bytes)
                 c2_bytes_b(i)= 0.0
                 bytes= cor2_vol_b(pos(i):pos(i)+img_cnt(i)-1)
                 c2_bytes_b(i)= total(bytes)
                 h1_bytes_b(i)= 0.0
                 bytes= hi1_vol_b(pos(i):pos(i)+img_cnt(i)-1)
                 h1_bytes_b(i)= total(bytes)
                 h2_bytes_b(i)= 0.0
                 bytes= hi2_vol_b(pos(i):pos(i)+img_cnt(i)-1)
                 h2_bytes_b(i)= total(bytes)
               end
              endif else begin ; only one day of images
               img_cnt= [n_elements(s_b)]
               tot_bytes_b= tot_vol_b
               eu_bytes_b= euvi_vol_b
               c1_bytes_b= cor1_vol_b
               c2_bytes_b= cor2_vol_b
               h1_bytes_b= hi1_vol_b
               h2_bytes_b= hi2_vol_b
               image_dates_b= s_b(0)
              endelse

;help,eu_bytes_a,c1_bytes_a,c2_bytes_a,h1_bytes_a,h2_bytes_a,image_dates
;help,eu_bytes_b,c1_bytes_b,c2_bytes_b,h1_bytes_b,h2_bytes_b
;stop

            DEVICE,get_visual_depth=bits
            IF (bits gt 8) THEN device,decomposed=0 

              wset,prof_win
              if(res_types(rtype) eq 'Monthly') then begin
                pdates= yymmdd2utc(strmid(image_dates,2,2)+strmid(image_dates,5,2)+'01') 
              endif else begin
                pdates= yymmdd2utc(strmid(image_dates,2,2)+ $
                      strmid(image_dates,5,2)+strmid(image_dates,8,2)) 
              endelse
              if(ptype eq 0) then begin ; Box type plot
                box_plot,/multi 
              endif else begin ; outline type plot
                box_plot
              endelse
            endif else begin ; s= '' -> No data was returned
              cnts= -1
              !x.range= [0,1]
              !y.range=[0,1]
              !x.ticks= 0
              !x.tickname= ' '
              !x.tickv= 0.5
              plot,[0,0],[0,0],/nodata,color=1
              xyouts,.3,.5,'Sorry - No images in the selected range.',color=1 ,charsize=1.0
            endelse
          end
  'pid':  begin
           ptype= event.index
           if(cnts gt -1) then begin
             if(ptype eq 0) then $
               box_plot,/multi $
             else $
               box_plot
           end
         end
 'r_type' : begin 
           last_press= 'r_type'
           rtype= event.index
  end 
  'imgtyp' : begin
           last_press= 'imgtyp'
           img_type= event.index
  end
  'bdte':  begin
             last_press= 'bdte'
             WIDGET_CONTROL, event.id ,get_value=beg_date
             beg_date= beg_date(0)
           end
  'edte':   begin
             last_press= 'edte'
             WIDGET_CONTROL, event.id ,get_value=end_date
             end_date= end_date(0)
           end
  'pwin':  begin
;             print,'Plot: event type, x, y= ',event.type,event.x,event.y
;             if(event.type eq 2) then print,'x, y motion= ',event.type,event.x,event.y
           end
  'sid':   begin
             last_press= 'sid'
             sname= snames(event.index) 
             stype= stypes(event.index)
             WIDGET_CONTROL, save ,set_value=sname
;              print,'save name= ',sname
           end
  'svn':   begin
             last_press= 'svn'
             WIDGET_CONTROL, save ,get_value=sname
              sname= sname(0)
;              print,'save name= ',sname
           end
  'sav':   begin
             last_press= 'sav'
;             msg='Save plot as '+stype+' in '+sname+'?'
             if(stype eq 'PS') then begin 
               msg=['Note: This file will be saved in the current working',$
                    'directory unless a path is provided with the filename.',$
                    '',$
                    'Save plot as a PostScript file in '+sname+'?']
             endif else begin 
               msg=['Note: This file will be saved in the current working',$
                    'directory unless a path is provided with the filename.',$
                    '',$
                    'Save plot as a GIF file in '+sname+'?']
             endelse
             res= WIDGET_MESSAGE(msg,/question)
             if(res ne "Yes") then return

             ; for 24-bit machines, reset decomposed to 1 to use 24-bits and update
             ; screen (to blank) so that tvrd for .gif and .ps have color:

             DEVICE,get_visual_depth=bits
             IF (bits gt 8) THEN device,decomposed=1 

             if(stype eq 'PS') then begin
               WIDGET_CONTROL, /HOUR
               print,'Creating PS: '+sname
               set_plot,'ps'
               device,file= sname
               device,/landscape
               if(cnts ge 0) then begin ; data was selected
                 if(ptype eq 0) then begin ; Box type plot
                   box_plot,/ps_plt,/multi 
                 endif else begin ; outline type plot
                   box_plot,/ps_plt
                 endelse
               endif else begin ; no data was selected.
                 plot,[0,0],[0,0],/nodata
                 xyouts,.3,.5,'Sorry - No images in the selected range.' ,charsize=1.0
               endelse
               device,/close_file
               set_plot,'x'
             endif else begin ; 'GIF'
               WIDGET_CONTROL, /HOUR
               if(ptype eq 0) then $
                 box_plot,/multi $
               else $
                 box_plot
               print,'Creating GIF: '+sname
               img = TVRD()
               TVLCT, r, g, b, /GET
               WRITE_GIF, sname, img, r, g, b ;;=> blank plot with decompsed=0
             endelse
             ; for 24-bit machines, reset decomposed to 0 to use 8-bits and update
             ; screen with colors:
             DEVICE,get_visual_depth=bits
             IF (bits gt 8) THEN BEGIN 
               device,decomposed=0
               if(ptype eq 0) then $
                 box_plot,/multi $
               else $
                 box_plot
             ENDIF
           end
  'pnm':   begin
             last_press='pnm'
             WIDGET_CONTROL, event.id ,GET_VALUE=fname
             fname= fname(0)
           end
  'prn': begin
              save_req= event.select
              if(event.select eq 1) then begin
                res=widget_message('Print to "'+fname+'"?',/question, $
                                   title="Printer")
                if(res eq "Yes") then begin
                  ; for 24-bit machines, reset decomposed to 1 to use 24-bits and update
                  ; screen (to blank) so that printed plot has color:
                  DEVICE,get_visual_depth=bits
                  IF (bits gt 8) then  device,decomposed=1
                  last_press= 'prn'
                  print,'printing to '+fname
                  set_plot,'ps'
                  device,file='ff_img_files.ps'
                  device,/landscape
                  if(cnts ge 0) then begin ; data was selected
                    if(ptype eq 0) then begin ; Box type plot
                      box_plot,/ps_plt,/multi 
                    endif else begin ; outline type plot
                      box_plot,/ps_plt
                    endelse
                  endif else begin ; no data was selected.
                    plot,[0,0],[0,0],/nodata,color=1
                    xyouts,.3,.5,'Sorry - No images in the selected range.' ,charsize=1.0
                  endelse
                  device,/close_file
                  set_plot,'x'
                  spawn,'lp -o nobanner -d '+fname+' ff_img_files.ps'
                  ; for 24-bit machines, reset decomposed to 0 to use 8-bits and update
                  ; screen with colors:
                  DEVICE,get_visual_depth=bits
                  IF (bits gt 8) THEN BEGIN 
                    device,decomposed=0
                    if(ptype eq 0) then $ 
                      box_plot,/ps_plt,/multi $
                    else $
                      box_plot,/ps_plt                     
                  ENDIF
                end
              end
           end
 endcase

end


pro scc_tmstat 
  common scctm_share,selected_img,fname,save,save_req,img,prof_win,$
              res_types,old_query,old_s,rtype,query,polar,ptype,$
              x_arr,y_arr,plot_cnt,plot_info,last_press,beg_date,end_date, $
              cnts,img_cnt,image_dates,pdates,flt_str,pol_str,img_type,ored,ogreen,oblue,$
              stypes,stype,snames,sname,tot_bytes_a,eu_bytes_a,c1_bytes_a,c2_bytes_a,$
              h1_bytes_a,h2_bytes_a,tot_bytes_b,eu_bytes_b,c1_bytes_b,c2_bytes_b,h1_bytes_b,h2_bytes_b


  query= ''
  erase_plot= 'true'
  plot_cnt= 0
  ptype= 0
  last_press= ''
  plot_info= {plt:'',beg:0l,fin:0l,xout:0.0D,yout:0.0D,msg:'',keys:''}
  plot_info= replicate(plot_info,51)
  save_req= 0
  cnts= -1
  flt_str= 'All'
  pol_str= 'All'

;  Get the current color table and save it:
   TVLCT,/GET,ored,ogreen,oblue
;  Create a color table of Black, White, Red, Green, Blue, Yellow, Cyan, and
;  Magenta and load it into the display device:
;         B,W,R,G,B,Y,C,M
       r=[0,1,1,0,0,1,0,1]
       g=[0,1,0,1,0,1,1,0]
       b=[0,1,0,0,1,0,1,1]
       TVLCT,255*r,255*g,255*b


  ;base=widget_base(title='scc_tmstat - Telemetry Statistics',xsize=636,ysize=600,$
  base=widget_base(title='scc_tmstat - Telemetry Statistics',xsize=736,ysize=950,$
                    xoffset=2,/frame) 

  b1= widget_base(base,/ROW)
  b2= widget_base(b1,/COLUMN,/frame)     ; ,/frame frames entire window
  b3= widget_base(b2,/ROW)               ; ,/frame frames quit & help
  colb1= widget_base(b2,/ROW,/frame)     ; ,/frame frames plot window
  colb2= widget_base(b2,/ROW) 
  b4= widget_base(colb2,/COLUMN,/frame)  ; ,/frame frames selection icons
  colb0= widget_base(b2,/ROW,/FRAME)
  b5= widget_base(b2,/COLUMN,/frame)  ; ,/frame frames print section
  ;colb3= widget_base(b4,/ROW)
  colb4= widget_base(b4,/ROW)
  colb5= widget_base(b4,/ROW)
  ;colb6= widget_base(b4,/ROW)
  cimg= widget_base(b4,/ROW)
  ;colb7= widget_base(b4,/ROW)
  colb8= widget_base(b5,/ROW)
  rowb1= widget_base(colb1,/ROW)

  txt= widget_label(b3,value='                  ')
  quit = WIDGET_BUTTON(b3, VALUE='Quit', UVALUE='Done',/frame)
  ptypes= ['Histogram','Symbol']
  txt= widget_label(b3,value='           ')
  pltid= cw_bselector(b3, UVALUE='pid',ptypes,label_left="Plot Type:",/frame)
  txt= widget_label(b3,value='           ')
  help= WIDGET_BUTTON(b3, VALUE='Help',UVALUE='Help',/frame)

  ;lineprofile=  widget_draw(rowb1,xsize=600,ysize=300,retain=2,/button_events, $
  lineprofile=  widget_draw(rowb1,xsize=700,ysize=700,retain=2,/button_events, $
                    /viewport_events,UVALUE='pwin',/frame)

  txt= widget_label(colb4,value='                                        ')
  proceed = WIDGET_BUTTON(colb4, VALUE='    Submit    ', UVALUE='proc',/frame)

  beg_date= '2008/01/01'
  end_date= 31

  beg_dte= cw_field(colb5,UVALUE='bdte', VALUE= beg_date, xsize= 12,$
                    title=' TM StartDate:',/row,/ALL_EVENTS)
  end_dte= cw_field(colb5,UVALUE='edte', VALUE= end_date, xsize= 4, $
                    title='Days',/row,/ALL_EVENTS)
  txt= widget_label(colb5,value='  ')


  imtype=['Raw Telemetry']
  img_type= 0
  imtp_id= cw_bselector(colb5, UVALUE='imgtyp',imtype,label_left="TM Type: ",/frame)
  txt= widget_label(colb5,value='  ')

  res_types=['Daily','Monthly']
  rtype= 0 ; initialize it to 'Daily' 
  res_id= cw_bselector(colb5, UVALUE='r_type',res_types, label_left="Resolution: ",/frame)
 
  txt= widget_label(colb8,value='          ')
  ;cb77= widget_base(colb8,/ROW)
  cb8= widget_base(colb8,/ROW,/FRAME)
  b_id= widget_button(cb8, VALUE="Print Plot", UVALUE="prn")
  fname="hp4-247"
  val= cw_field(cb8,UVALUE='pnm', VALUE= fname,title="Printer:",xsize=10, $
                       /ALL_EVENTS) ; can add /frame 
  
  txt= widget_label(colb8,value='            ')
  cb9= widget_base(colb8,/ROW,/FRAME)
  s_id= widget_button(cb9, VALUE="Save Plot", UVALUE="sav")
  txt= widget_label(cb9,value='as')
  snames=['scc_tm_plot.ps','scc_tm_plot.gif']
  sname= snames(0)
  stypes=['PS','GIF']
  stype= stypes(0)
  svid= cw_bselector(cb9, UVALUE='sid',stypes,/frame)
  save= cw_field(cb9,UVALUE='svn', VALUE= sname,title="in",xsize=12,/ALL_EVENTS)
 
  widget_control,/realize,base
  widget_control,lineprofile,get_value= prof_win 

  struct = { $
    base:base $
  }
  
  widget_control,base, SET_UVALUE=struct

  XMANAGER, 'IMG_SELECT', base, EVENT_HANDLER='IMG_SELECT_EVENT'

  return

end
