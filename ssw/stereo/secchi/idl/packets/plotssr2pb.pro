pro plotssr2pb,stad, pointer=pointer,percent=percent,nfiles=nfiles, NDAYS=ndays, SIDE=side
;+
; $Id: plotssr2pb.pro,v 1.11 2012/05/14 19:28:14 secchia Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      :
;               
; Purpose   : 
;               
; Use       : IDL> 
;    
; Inputs    : 	defaults to last track
;
; Optional Inputs:
;           stad    start date (anytim2utc)
;   
; Outputs   : plot
;
; Optional Outputs:	
;
; Keywords  : 
;       /POINTER    Plot ssr pointer position, not percent.
;       NFILES=     Number of tracks to plot, either from now backwards, or if stad
;                   number of tracks from the start of stad  
;       NDAYS=  Number of days to plot	    	
;   	SIDE= 	'a' or 'b', look in $scc[ab]/nrl/outputs/prints instead of $ITOS_GROUPDIR/outputs/prints
;               
; Category    : telemetry planning operations ssr2 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL
;               
; $Log: plotssr2pb.pro,v $
; Revision 1.11  2012/05/14 19:28:14  secchia
; nr - fix NFILES= and no arg
;
; Revision 1.10  2012/03/23 21:55:35  nathan
; fix file read loop
;
; Revision 1.9  2012/03/15 21:46:42  secchia
; nr - use while instead of for to limit number of files read in; compute
; and print amount played back for each track
;
; Revision 1.8  2011/11/28 19:34:39  nathan
; added SIDE= option
;
; Revision 1.7  2011/11/09 18:44:17  secchia
; nr - implement NDAYS=
;
; Revision 1.6  2011/10/25 21:37:46  secchia
; nr - added stad input, /pointer
;

IF keyword_set(SIDE) THEN $
cfile=file_search(getenv('scc'+strlowcase(side))+'/nrl/outputs/prints/stereo21_E*.txt') ELSE $
cfile=file_search(getenv('ITOS_GROUPDIR')+'/outputs/prints/stereo21_E*.txt')
;cfile=cfile(n_Elements(cfile)-1)
ncf=n_elements(cfile)
dayrange=0
IF keyword_set(nfiles) THEN nf=nfiles ELSE nf=1
starts=yymmdd2utc('20'+rstrmid(cfile,8,6)+'_'+rstrmid(cfile,4,4))

IF n_params() GT 0 THEN BEGIN
    stadu =anytim2utc(stad)
    w=where(starts.mjd EQ stadu.mjd,nw)
    IF nw GT 0 THEN n0=w[0] ELSE BEGIN
        w=where(starts.mjd GE stadu.mjd,nw)
        IF nw GT 0 THEN BEGIN
            n0=w[0]
            message,'First day available is '+utc2str(starts[n0],/date)+'; starting there.',/info
        ENDIF ELSE BEGIN
            message,'Last day available is '+cfile[n_elements(cfile-1)],/info
            return
        ENDELSE
    ENDELSE
    nf=ncf-n0
    stadu.time=0
    enddu=stadu
    IF ~keyword_set(NDAYS) THEN ndays=1
    enddu.mjd=enddu.mjd+ndays
    dayrange=[stadu,enddu]
ENDIF ELSE BEGIN
    n0=ncf-nf
    enddu=starts[ncf-1]
ENDELSE

; first file
print,'Reading ',cfile[n0]
tmp=readlist(cfile[n0])

; remaining files
n=n0+1
if nf GT 1 then repeat begin 
    print,'Reading ',cfile[n]
    tmp=[tmp,readlist(cfile[n])]
    n=n+1
endrep until (n GE ncf) or (starts[n<(ncf-1)].mjd GT enddu.mjd+1)   

nf=n-n0

tmp=tmp(where(strlen(tmp) ge 93))
title=tmp(0)
ctitles=strsplit(title,'|',/extract)
vals=tmp(where(tmp ne title))
vals=vals(0:n_elements(vals)-2)
ssr2vals=strarr(n_Elements(vals),n_Elements(ctitles))
nvals=n_elements(vals)
for n=0,nvals-1 do ssr2vals(n,*)=strsplit(vals(n),'|',/extract)

ctitles=ctitles(1:n_elements(ctitles)-1)
times=ssr2vals(*,0)
times='20'+strmid(times,6,2)+'/'+strmid(times,0,5)+' '+strmid(times,9,8) 
ssr2vals=float(ssr2vals(*,1:n_Elements(ctitles)))
window,xsize=800,ysize=450
rd=ssr2vals(*,where(ctitles eq 'CDSSRPTN20RDPTR021'))
if ~keyword_set(pointer) then begin
  rd= rd/ ssr2vals(*,where(ctitles eq 'CDSSRPTN20SIZE021'))*100
  yr=[0,100]
endif else yr=[0,ssr2vals(0,where(ctitles eq 'CDSSRPTN20SIZE021'))]
IF keyword_set(POINTER) THEN ti='SSR2 Pointer Positions' ELSE ti='SSR2 Pointer Percents'
plotvtime,times,rd,'read',title=ti,yrange=yr,ystyle=1,color=0,background=1,psym=-1, timerange=dayrange

wrt=ssr2vals(*,where(ctitles eq 'CDSSRPTN20WRTPTR021'))
if ~keyword_set(pointer) then wrt=wrt/ ssr2vals(*,where(ctitles eq 'CDSSRPTN20SIZE021'))*100
oplotvtime,times,wrt,'write',color=2,psym=-1

mval=fltarr(n_Elements(ssr2vals(*,0)))
tmp=where(ssr2vals(*,where(ctitles eq 'CDSSRPTN20RDPTR021')) gt ssr2vals(*,where(ctitles eq 'CDSSRPTN20WRTPTR021')),cnt)
if cnt ge 1 then mval(where(ssr2vals(*,where(ctitles eq 'CDSSRPTN20RDPTR021')) gt ssr2vals(*,where(ctitles eq 'CDSSRPTN20WRTPTR021'))))=1
ntrd=(ssr2vals(*,where(ctitles eq 'CDSSRPTN20SIZE021'))*mval-ssr2vals(*,where(ctitles eq 'CDSSRPTN20RDPTR021'))+ssr2vals(*,where(ctitles eq 'CDSSRPTN20WRTPTR021'))) 
if ~keyword_set(pointer) then ntrd=ntrd/ ssr2vals(*,where(ctitles eq 'CDSSRPTN20SIZE021'))*100
oplotvtime,times,ntrd,'not read',color=3,psym=-1

tais=anytim2tai(times)
timdif=abs(tais-shift(tais,1))
ntrdif=(ntrd-shift(ntrd,-1))>0
wd=where(timdif gt 1000,nwd)
wd=[wd,nvals-1]
print,ntrd[wd]
totpb=0
print,'MB played back for BOT of'
for i=0,nwd-1 do BEGIN
    pdif=ntrd[wd[i]]-ntrd[wd[i+1]]
    IF pdif LT 0 THEN BEGIN
        IF wrt[wd[i]+1] EQ wrt[wd[i]] THEN pdif=pdif+100 ELSE pdif=0
    ENDIF
    print,times[wd[i]+1],' was ',string(pdif,'(i3.3)')
    totpb=totpb+pdif
endfor
;      2012/03/15 13:38:42 was 008
print,'TOTAL                   ',string(totpb,'(f5.1)'),' MB'

oplotvtime,times[wd],ntrd[wd],'not read',color=4,psym=2

end
