function parsefcmetafile, file0
;+
; $Id: parsefcmetafile.pro,v 1.4 2007/08/31 15:38:36 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : parsefcmetafile
;               
; Purpose     : parse the counts in the image meta file generated by file_capture
;               
; Explanation : 
;               
; Use         : IDL> fcnts=parsefcmetafile('76010012.748')
;    
; Inputs      : fileorig    Image file with path
;               
; Outputs     :
;** Structure FILE_CAPTURE_META, 4 tags, length=8, data length=8:
;   PACKETS_FOUND   INT             
;   NBLOCKS         INT              
;   NERRORS         INT              
;   PACKETS_EXPECTED
;                   INT             
;
; Keywords :	
;               
; Calls from LASCO : 
;
; Common      : 
;               
; Restrictions: Input requires path
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Aug 2007
;               
; $Log: parsefcmetafile.pro,v $
; Revision 1.4  2007/08/31 15:38:36  nathan
; nr - allow incomplete metafile
;
; Revision 1.3  2007/08/30 20:20:29  nathan
; fixed check for .0 on input
;
; Revision 1.2  2007/08/30 18:34:43  secchia
; nr - fix typo
;
; Revision 1.1  2007/08/30 18:05:28  nathan
; used with secchi_reduce.pro,v 1.240
;

len=strlen(file0)
; assuming fileorig has path on it
IF strmid(file0,len-2,2) EQ '.0' THEN metafile=strmid(file0,0,len-1)+'meta.0' $
ELSE metafile=file0+'.meta'

cnts={ file_capture_meta, PACKETS_FOUND:0, NBLOCKS:0, NERRORS:0, PACKETS_EXPECTED:0 }

rows=readlist(metafile)
n=n_elements(rows)
c=where(rows EQ 'COUNTS')
c=c[0]
IF c LT 0 THEN BEGIN
	message,metafile+' is incomplete!!',/info
	wait,10
	return,cnts
ENDIF

parts=strsplit(rows[c+1],'=',/extract)
cnts.packets_found=fix(parts[1])
parts=strsplit(rows[c+2],'=',/extract)
cnts.nblocks=fix(parts[1])
parts=strsplit(rows[c+3],'=',/extract)
cnts.nerrors=fix(parts[1])
parts=strsplit(rows[c+4],'=',/extract)
first=fix(parts[1])
parts=strsplit(rows[c+5],'=',/extract)
last=fix(parts[1])

IF last LT first THEN last=last+16384
cnts.packets_expected=last-first+1

return,cnts

end
