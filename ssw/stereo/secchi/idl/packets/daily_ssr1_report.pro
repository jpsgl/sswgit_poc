pro daily_ssr1_report,sc,yyyymmdd,outpdir,reprocess=reprocess, _EXTRA=_extra

;+
; $Id: daily_ssr1_report.pro,v 1.9 2012/12/27 18:03:19 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      :daily_ssr1_seqprt.pro
;               
; Purpose   : to run monthly and daily SCI apid reports at the end of each days pipeline processing .
;               
; Explanation: Automatically finds files in $sccb or $scca directory based on lzpb input
;               
; Use       : IDL> 
;    
; Inputs    : 	sc = spacecraft A or B, yr = four digit year , mm = 2 digit month  , lzpb = 'lz' , 'pb'  or 'mon'
;
; Outputs   :  PNG plots of the months SSR2 packets received and SSR1 DATA gaps along with the yearly gap report files.
;
; Optional Outputs:	
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database sci apid's
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Apr 06
;               
; $Log: daily_ssr1_report.pro,v $
; Revision 1.9  2012/12/27 18:03:19  mcnutt
; changed output and input directory
;
; Revision 1.8  2012/12/07 11:27:53  secchib
; added reprocess keyword
;
; Revision 1.7  2012/05/22 22:10:25  secchia
; nr - by default do IO in $HOME not $sccx
;
; Revision 1.6  2012/05/22 22:09:30  secchia
; nr - by default do IO in $HOME not $sccx
;
; Revision 1.5  2011/04/19 19:20:00  nathan
; update notes
;
; Revision 1.4  2009/09/22 18:00:26  secchia
; delete previous allseqerror report files, creates png not pdf output and hardcode outpdir
;
; Revision 1.3  2009/09/02 22:41:09  nathan
; add _extra
;
; Revision 1.2  2007/08/09 15:25:39  mcnutt
; made yr a string for catline
;
; Revision 1.1  2007/08/08 16:39:17  mcnutt
; moved from dev/packets to be run daily in the EUVI processing
;

outpdir2='/net/calliope/data/secchi/'+strlowcase(sc)+'/lzsci/pcktcnt/'  ;always use EUVI seb_img

yr=strmid(yyyymmdd,0,4) & mm=strmid(yyyymmdd,4,2) & dd=strmid(yyyymmdd,6,2)
doday=utc2doy(str2utc(yr+'-'+mm+'-'+dd))

dodaym1=doday-1 & yrm1=yr
if(dodaym1 le 0)then begin
  yrm1=string(yr-1,'(i4.4)')
  dodaym1=utc2doy(str2utc(yrm1+'-12-31'))
endif
print,doday

;inpdir=getenv('HOME')+'/lz/outputs/prints/'
;IF not file_exist(inpdir) THEN inpdir=0
inpdir=0  ;never set the inpdir always use $scc(ab)

daily_apid_seqprt,sc,yrm1,dodaym1,'lz',/overwrite, USEDIR=inpdir, _EXTRA=_extra
daily_apid_seqprt,sc,yr,doday,'lz',/overwrite,USEDIR=inpdir, _EXTRA=_extra

monthly_apid_report,sc,yr,mm,'lz',outpdir2,USEDIR=inpdir, _EXTRA=_extra

monthly_ssr2_seqprt, sc,yr,mm,'lz',USEDIR=inpdir, _EXTRA=_extra

yrt=string(yr,'(i4.4)')
mms=indgen(mm) +1
catline='cat'
for nm=0,mm-1 do catline=catline+' '+outpdir2+sc+'_SSR1_'+yrt+'_'+string(nm+1,'(i2.2)')+'.txt'
spawn,catline+' >/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SSR1/'+strupcase(sc)+'_SSR1_'+yrt+'.txt'

catline='cat'
for nm=0,mm-1 do catline=catline+' '+outpdir2+sc+'_SPW_'+yrt+'_'+string(nm+1,'(i2.2)')+'.txt'
spawn,catline+' >/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SPW/'+strupcase(sc)+'_SPW_'+yrt+'.txt'


end
