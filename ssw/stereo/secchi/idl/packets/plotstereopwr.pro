;+
pro plotstereopwr,fn, _EXTRA=_extra
; $Id: plotstereopwr.pro,v 1.1 2009/04/13 20:32:46 nathan Exp $
; Name:     plotstereopwr
;
; Purpose:  plot values from file created by stereopwr.sprt
;
; Inputs:   fn	STRING	File to plot values from
;
; Optional input keywords:
;   	    Any plot keywords
;
; Outputs:  none - plots values in window
;
; Common blocks:    	
;
; WRitten by N.Rich, NRL/I2
;-
; $Log: plotstereopwr.pro,v $
; Revision 1.1  2009/04/13 20:32:46  nathan
; checkin
;

common sccplot, currentstmpl, voltagestmpl, aorb, xfscitemplate, stereopwrfmt, sctempsfmt

IF datatype(stereopwrfmt) EQ 'UND' THEN restore,getenv('SCC_DATA')+'/templates/stereotlmtemplates.sav'

print,'Reading ',fn
s=read_ascii(fn,template=stereopwrfmt)

plotvtime,s.x00btime,s.decvoltage,'decontam',title='SECCHI Voltages from S/C', _Extra=_extra
oplotvtime,s.x00btime,s.opvoltage,'operational', _Extra=_extra
oplotvtime,s.x00btime,s.survvoltage,'survival', _Extra=_extra

end
