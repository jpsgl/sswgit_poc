pro plotpbrates,rptfiles, MULT=mult, TIMERANGE=timerange, YMAX=ymax
;sc,time1,time2,mbpb,hrs,mins
; $Id: plotpbrates.pro,v 1.9 2012/11/29 22:05:38 nathan Exp $
;
; Purpose:  Plot playback rates from ground receipt times for EUVI packets.
;   	    Default is to draw a single plot with each track overplotted on it.
;
; Input:    rptfiles	STRARR of scheduling report files, such as 
;   	    	    	li=findfile('$sched/A/dailyats/STA2009*rpt')
;   	    	    	There are used to determine track times.
;
;   	    Uses $sccx/ssrpbrates.sav to get actual playback rates. This file contains structure of format:
;** Structure SSRPBSTRUCT, 2 tags, length=12, data length=12:
;   RATE            FLOAT           150.201  	(packets/sec)
;   GRT             STRUCT    -> CDS_INT_TIME Array[1]
;
; Keywords:
;   /MULT   Put each track in a separate plot
;   TIMERANGE=[t1,t2]	Range for which to plot points, or if scalar, start date: any input to anytim2utc.pro
;   YMAX=   max value of yrange
;
; $Log: plotpbrates.pro,v $
; Revision 1.9  2012/11/29 22:05:38  nathan
; finish track length plot
;
; Revision 1.8  2011/06/21 18:36:11  nathan
; plot different rates in different colors
;
; Revision 1.7  2010/06/16 21:59:09  nathan
; fix so duplicate rows are not included in plot; added TIMERANGE keyword
;
; Revision 1.6  2010/05/19 17:42:51  nathan
; improved labeling; plot available times
;
; Revision 1.5  2009/09/03 18:39:13  mcnutt
; change plot range to y2=pbrate from report file and added stop after 10 plots if mult
;
; Revision 1.4  2009/09/02 22:37:19  nathan
; change how sc1 is derived; accomodate most recent report file format
;
; Revision 1.3  2009/03/23 19:49:28  nathan
; update notes
;
; Revision 1.2  2009/03/23 19:47:52  nathan
; changed default behavior to plot values on a single plot
;

domult=0
plotprep
IF keyword_set(MULT) THEN BEGIN
    !p.multi=[0,2,5,0,0]
    domult=1
    window,0,xsi=800,ysi=1000
ENDIF ELSE $
    window,0

nf=n_elements(rptfiles)
; Loop through each track in input, can be multiple files

isa=strpos(rptfiles[0],'STA')
IF isa GT 0 THEN SC1='A' ELSE sc1='B'
psm1=2	; *
psm2=1	; +
psmx=7	; x

pbfile=getenv('scc'+strlowcase(SC1))+'/ssrpbrates.sav'
restore,pbfile,/verb

ssr1t=utc2tai(SSR1PB.GRT)
ssr2t=utc2tai(ssr2PB.GRT)
spwxt=utc2tai(spwxPB.GRT)
d=0
nssr1t=n_elements(ssr1t)
nssr2t=n_elements(ssr2t)
nspwxt=n_elements(spwxt)

vals1=make_array(nssr1t,val=3)
vals2=make_array(nssr2t,val=2)
valsx=make_array(nspwxt,val=1)
curw=!d.window
window,1,xsize=800,ysize=200
utplot, SSR1PB.GRT,vals1,yrange=[0,4],psym=psm1,title='Times in .sav file'
outplot,ssr2PB.GRT,vals2,psym=psm2
outplot,spwxPB.GRT,valsx,psym=psmx
xyouts,10,190,'SSR1= *',/device
xyouts,70,190,'SSR2= +',/device
xyouts,130,190,'SPWX= x',/device
wset,curw
wait,5
; First read in rows and eliminate duplicate rows
nrt=0
FOR i=0,nf-1 DO BEGIN
    print,'Reading in ',rptfiles[i]
    rows=readlist(rptfiles[i])
    nr=n_elements(rows)
    prt=strpos(rows[1],'rate')
    FOR j=2,nr -1 DO BEGIN
    	line=rows[j]
    	if strmid(line,0,1) ne '2' then continue
    	tmp=strsplit(line)
    	time1=strmid(line,tmp(0),10)+'T'+strmid(line,tmp(2),2)+':'+strmid(line,tmp(2)+2,2)+':00'
    	hrs= fix(strmid(line,tmp(5),2))
    	mins=fix(strmid(line,tmp(5)+3,2))
	dura=(60.*60.*hrs)+(mins+1)*60.
    	IF prt GT 0 THEN BEGIN
    	    mbpb=fix(strmid(line,tmp[7],3))
	    pbrate=fix(strmid(line,tmp[6],3)) 
	ENDIF ELSE BEGIN
	    pbrate=720
	    mbpb=fix(strmid(line,tmp[6],3))
	ENDELSE
	print,time1,pbrate
	
	IF nrt EQ 0 THEN BEGIN
	    time1s=time1
	    mbpbs=mbpb
	    duras=dura
	    pbrates=pbrate
	ENDIF ELSE BEGIN
    	    time1s=[time1s,time1]
	    mbpbs =[mbpbs,mbpb]
	    duras =[duras,dura]
	    pbrates=[pbrates,pbrate]
	ENDELSE
	nrt=nrt+1
    ENDFOR
ENDFOR		
st=sort (time1s)
u=uniq(time1s[st])
time1s=time1s[st[u]]
mbpbs = mbpbs[st[u]]
duras = duras[st[u]]
pbrates=pbrates[st[u]]
nt=n_elements(u)

first=1
d=0
tr1=anytim2tai(time1s[0])
tr2=anytim2tai('2049/01/01')
IF keyword_set(TIMERANGE) THEN BEGIN
    tr1=anytim2tai(timerange[0])
    IF n_elements(timerange) GT 1 THEN tr2=anytim2tai(timerange[1])  
ENDIF
window,0
pbkbps=    [720,   480,    360,    240,    160,    120]    ; kbps
pbcol =     [4,     3,	    7,	    2,	    5,	    6]

FOR i=0,nt-1 DO BEGIN
; Loop over tracks.
    time1=time1s[i]
    help,time1
    t1=anytim2tai(time1)
    IF t1 LT tr1 or t1 GT tr2 THEN continue
    IF (first) THEN BEGIN
    	ind=where(pbrates eq max(pbrates))
	duram=duras[ind]
	bps=((mbpbs[ind]*1e6)*8)/duram
    	xout=(indgen(4)*duram/10.0)+500
    	y1=0 
	y2=max(pbrates);bps*1.5/1000
	IF keyword_set(YMAX) THEN y2=ymax
    	yout=10
	IF ~(domult) THEN BEGIN
    	    xout=xout/60
	    plot,[0,max(duras)]/60.,[bps,bps],yrange=[y1,y2], $
    	    	title= 'SECCHI-'+SC1+' playback rates starting '+strmid(time1,0,16), $
		xstyle=1,ystyle=1,ytitle='KbPS',xtitle='Minutes into pass', $
		yticklen=1.,yticks=20
    	    xyouts,10,yout,'SSR1= *',/device
    	    xyouts,70,yout,'SSR2= +',/device
    	    xyouts,130,yout,'SPWX= X',/device
	    FOR j=0,5 DO xyouts,200+j*40,yout,trim(pbkbps[j]),color=pbcol[j],/device
	ENDIF
	first=0
	
    ENDIF
    
   t2=t1+duras[i]
   time2=utc2str(tai2utc(t2))
   ud0=anytim2utc(time1)
   ud1=anytim2utc(time2)
   x1=utc2tai(ud0)
   x2=utc2tai(ud1)
 
    y2=pbrates[i]
    IF domult THEN BEGIN
	utplot,[time1,time2],[bps,bps],timerange=[ud0,ud1],yrange=[y1,y2],background=1,color=0, $
    	    	title= 'SECCHI-'+SC1+' playback rates starting '+strmid(time1,0,16), $
		xstyle=1,ystyle=1,ytitle='KbPS',xtitle='Minutes into pass'
;    	    title= 'SECCHI-'+SC1+' EOT = '+strmid(time2,0,16),xstyle=1,ystyle=1,ytitle='kbps'
    	xyouts,xout(0),yout,'SSR1',color=0,charsize=.75
    	xyouts,xout(1),yout,'SSR2',color=2,charsize=.75
    	xyouts,xout(2),yout,'SPWX',color=3,charsize=.75
    ENDIF

    pcol=pbcol[where(pbkbps EQ y2)]
   z1=where(ssr1t ge t1 and ssr1t le t2, nz1)
  if nz1 GT 1 then begin
   ssr1rate=SSR1PB(z1).rate*2.176
   stimes=SSR1PB[z1].grt
   taistimes=utc2tai(stimes)
   ;zs=sort(stimes)
    IF (domult) THEN outplot,stimes,SSR1rate,psym=psm1,color=pcol ELSE $
    	oplot,(taistimes-x1)/60.,ssr1rate,psym=psm1,color=pcol
  endif
   z2=where(ssr2t ge t1 and ssr2t le t2,nz2)
  if nz2 GT 1 then begin
   ssr2rate=ssr2PB(z2).rate*2.176
   stimes=ssr2PB[z2].grt
   taistimes=utc2tai(stimes)
    IF (domult) THEN outplot,stimes,ssr2rate,psym=psm2,color=pcol ELSE $
    	oplot,(taistimes-x1)/60.,ssr2rate,psym=psm2,color=pcol
  endif
   zx=where(spwxt ge t1 and spwxt le t2,nzx)
  if nzx GT 1 then begin
   spwxrate=spwxPB(zx).rate*2.176
   stimes=spwxPB[zx].grt
   taistimes=utc2tai(stimes)
    IF (domult) THEN outplot,stimes,spwxrate,psym=psmx,color=pcol ELSE $
    	oplot,(taistimes-x1)/60.,spwxrate,psym=psmx,color=pcol
  endif
  print,'NZ1=',nz1,'  NZ2=',nz2,'  NZX=',nzx
  ;if (d mod 10) eq 0 then stop
  inp=''
  ;read,'Enter to cont:',inp
  
  ;+++ Build statistics over all tracks
  avghrsperday=(total(duras[0:i])/((t2-tr1)/86400))/3600.
  help,avghrsperday
  
  IF nz1 EQ 0 THEN continue
  d=d+1
  lastdate=strmid(time2,0,10)
  if domult and (d mod 10) eq 0 then stop
endfor	; Loop through rows/tracks

!p.multi=0

IF ~domult THEN BEGIN
    xyouts,0.25,0.9,trim(d)+' Tracks',charsize=2,/normal,color=0
    xyouts,0.6,0.9,'through '+lastdate,charsize=2,/normal,color=0
    window,2
    ti='SECCHI-'+SC1+' playback durations starting '+strmid(time1s[0],0,10)
    w5=where(pbrates eq 160)
    w2=where(pbrates eq 240)
    w3=where(pbrates eq 480)
    w4=where(pbrates eq 720)
    w6=where(pbrates eq 120)
    utplot,[time1s[0],time1s[i-1]], [10,10],psym=1,title=ti,ytit='hours',yran=[0,10]
    outplot,time1s[w2],duras[w2]/3600,psym=2,color=2
    outplot,time1s[w5],duras[w5]/3600,psym=2,color=5
    outplot,time1s[w3],duras[w3]/3600,psym=2,color=3
    outplot,time1s[w4],duras[w4]/3600,psym=2,color=4
    outplot,time1s[w6],duras[w6]/3600,psym=2,color=6

ENDIF

stop
end


