pro redo_ssr1_reports,sc,dtst,dtend
;+
; $Id: redo_ssr1_reports.pro,v 1.2 2012/12/28 11:49:33 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : redo_ssr1_reports
;               
; Purpose   : to rerun daily_ssr1_report for given days and sc
;               
; Explanation: 
;               
; Use       : IDL> redo_ssr1_reports,'YYYY-MM-DD','YYYY-MM-DD','CAM'
;    
; Inputs    :	dtst= starting day yyyy-mm-dd (or yyyymmdd)
;		dtend= ending day yyyy-mm-dd 
;		sc=a or b
;               
; Outputs   : allsequenceerrors_A_YYYY_DOY_lz.rpt monthly reports and plots
;
; Keywords  :
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to disk, environment variable sc must be defined.
;               
; Side effects: 
;               
; Category    : packets
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2012
;               
; $Log: redo_ssr1_reports.pro,v $
; Revision 1.2  2012/12/28 11:49:33  mcnutt
; added dates to for loop
;
ut0=nrlanytim2utc(dtst)
IF n_params() LT 3 THEN ut1=ut0 ELSE ut1=nrlanytim2utc(dtend)

mjd=ut0
FOR i=ut0.mjd,ut1.mjd DO BEGIN
    mjd.mjd=i
    yyyymmdd=utc2yymmdd(mjd,/yyyy)
    daily_ssr1_report,sc,yyyymmdd,/pipeline
ENDFOR 
end
