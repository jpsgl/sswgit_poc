function countstpackets, filename, header_only=header_only
;+
; Project     :	STEREO 
;
; Name        :	COUNT_PACKETS()
;
; Purpose     :	Quickly find number of packets in a STEREO packet file
;
; Category    :	STEREO, Telemetry
;
; Explanation :	use readu to determine size
;
; Syntax      :	OUT = scc_count_packets( filename )
;
; Examples    :	print,scc_count_packets('secchi_ahead_2015_204_6_02.ptp')
;
; Inputs      :	filename    PTP or STP filename
;
; Opt. Inputs :	None.
;
; Outputs     :	INT
;
; Opt. Outputs:	None.
;
; Keywords    : HEADER_ONLY = If set, then only read in the packet header
;                             information.  Only used for the header-only
;                             beacon ".ptp.hdr" or ".stp.hdr" files.
;
; Calls       :	ieee_to_host
;
; Common      :	None.
;
; Restrictions:	
;
; Side effects:	None.
;
; Prev. Hist. :	count_packets.pro
;-
; $Log: countstpackets.pro,v $
; Revision 1.2  2015/07/29 17:29:49  nathan
; updated version
;

n_packets = 0L
;
if file_exist(filename) then name = filename else begin
    spawn, 'find . -follow -name ' + filename, output
    name = output[0]
endelse
openr, unit, name, /get_lun
on_ioerror, error
;
while not eof(unit) do begin
    size = 0u
    readu, unit, size
    ieee_to_host, size
    if keyword_set(header_only) then size = size - 261
    b = bytarr(size-2)
    readu, unit, b
    n_packets = n_packets + 1
endwhile
goto, done
;
error:
message, /continue, 'An error occured reading the last packet'
n_packets = 0
;
done:
free_lun, unit
return, n_packets
;
end
