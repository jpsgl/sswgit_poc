
;+
; $Id: monthly_ssr2_seqprt.pro,v 1.5 2012/05/22 22:09:30 secchia Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : monthly_ssr2_seqprt.pro
;               
; Purpose   : checks sci SSR2 apid's for packets received
;               
; Explanation: Automatically finds files in $sccb or $scca directory based on lzpb input
;               
; Use       : IDL> 
;    
; Inputs    : 	sc = spacecraft A or B, yr = four digit year , mm = 2 digit month  , lzpb = 'lz' , 'pb'  or 'mon'
;
; Outputs   :  PNG plots of the months SSR2 packets received.
;
; Optional Outputs:	
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database sci apid's
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Apr 06
;               
; $Log: monthly_ssr2_seqprt.pro,v $
; Revision 1.5  2012/05/22 22:09:30  secchia
; nr - by default do IO in $HOME not $sccx
;
; Revision 1.4  2011/04/19 19:19:40  nathan
; update notes
;
; Revision 1.3  2009/09/22 18:00:27  secchia
; delete previous allseqerror report files, creates png not pdf output and hardcode outpdir
;
; Revision 1.2  2009/09/04 11:31:38  secchib
; added _extra keyword
;
; Revision 1.1  2007/08/08 16:41:33  mcnutt
; moved from dev/packets creates a PDF plot showing the SSR2 packets received
;
;
pro monthly_ssr2_seqprt, sc,yr,mm,lzpb,USEDIR=usedir,_extra=_extra
print,sc,yr,mm,lzpb
;!p.multi=[0,0,2,0,0]
;clr=['FFFFFF'x,'000000'x,'0000FF'x,'00FF00'x,'FF0000'x,'8888FF'x]
device,decomposed=0
tek_color
clr=[0,1,2,3,4,5]
help,yr


if (n_params() LT 4) THEN BEGIN
	sc=''
	yr=''
	day=''
	lzpb=''
ENDIF


if(sc EQ '' OR yr EQ '' OR mm EQ '') then begin
  read,'Enter spacecraft A or B:',sc
  read,'Enter ground receipt year: yyyy ',yr
  read,'Enter ground receipt doy: ddd ',day
endif
IF not (lzpb EQ 'lz' OR lzpb EQ 'pb' OR lzpb EQ 'mon') THEN BEGIN
	read,'Fourth argument must be "lz","pb", or "mon":[mon]',lzpb
	IF lzpb EQ '' THEN lzpb='mon'
ENDIF

IF keyword_set(USEDIR) THEN inpdir=usedir ELSE $
inpdir=getenv('scc'+strlowcase(sc))+'/nrl/'+lzpb+'/outputs/prints/' 
close,4
close,5
close,1
err=0
nferrs=0
zferrs=0

sc=strupcase(sc)

apids=['450','451','454'];,'452','453']
apidt=['COR1','COR2','EUVI'];,'HI1','HI2']
;apids=['450','451','454']
;apidt=['COR1','COR2','EUVI']


yyyy=string(yr,'(i4.4)') & mm=string(mm,'(i2.2)')
doy0=utc2doy(str2utc(yyyy+'-'+mm+'-01'))
month=fix(mm) & yr=fix(yyyy)
IF month EQ 12 THEN BEGIN
        doy1=utc2doy(str2utc(yyyy+'-'+mm+'-31'))
ENDIF ELSE BEGIN
	mm1s=string(month+1,'(i2.2)')
        doy1=utc2doy(str2utc(yyyy+'-'+mm1s+'-01'))-1
ENDELSE


day=indgen((doy1-doy0)+1) +doy0
ndays=n_elements(day)
utc=doy2utc(day[0],yr)
t0=utc2unixtai(utc)

utc=doy2utc(day[0],yr)
t1=utc2unixtai(utc)
napids=n_Elements(apids)

 ytcks=(doy1-doy0+1) - indgen(doy1-doy0+1) & ytcks=[' ',string(ytcks,'(i2)')]
 xtcks=indgen (24) +1
;set_plot,'ps'
;device,/land ,/color
window,11,xsize=1000,ysize=800
plot,[0,0],[0,0],xrange=[0,24],yrange=[doy1-doy0+2,1],xstyle=1,ystyle=1,background=1,color=0,$
      title=strupcase(sc)+'  '+yyyy+'-'+mm+' SSR2 PACKETS RECEIVED',ytitle='day of month',$
      xtitle='hours',xticks=24,yticks=doy1-doy0+1,ytickname=ytcks,yticklen=1.0,ygridstyle=1;,font=0

clr=[0,2,3,4,6,7,8]
for na=0,napids-1 do begin
  	apid=apids[na]
	FOR fi=0,ndays-1 DO BEGIN
	       d=0L
               utc=doy2utc(day[fi],yr)
               t0=utc2unixtai(utc)
		;file1=findfile(inpdir+'/h'+apid+'*'+strmid(string(yr,'(i4.4)'),2,2)+string(day,'(i3.3)')+'*'+sc+'*.txt')
		help,yr
		targfile=inpdir+'h'+apid+'*'+string(yr,'(i4.4)')+'*'+string(day[fi],'(i3.3)')+'*.txt'
		print,'Reading ',targfile
		file1=findfile(targfile)
		file1=file1(0)
		IF file1 EQ '' THEN BEGIN
			print,'File not found !!! ... skipping.'
			print
			wait,1
			nferrs=nferrs+1
			goto, skipit
		ENDIF
		openr,1,file1
		line=''
		fst=fstat(1)
                s0=-1
		IF fst.size GT 0 THEN BEGIN
			nrows=(fst.size-208)/100
			help,nrows
				grtime= dblarr(nrows)
				ptim=	dblarr(nrows)
				strtime=strarr(nrows)
				pcnt=	lonarr(nrows)
				ptot=	lonarr(nrows)
				seqerr= intarr(nrows)
				s0=0d
				n2h=4*3600d
                                ptotl=0l
  			while (not eof(1)) do begin
	    		readf,1,line
	    		if(strmid(line,0,3) eq 'Grd') then begin
	    		  readf,1,line
	    		  readf,1,line
	    		endif
	    		;IF strlen(strcompress(line)) GT 70 THEN BEGIN
				; weed out bad rows
				pcktime=double(strmid(line,48,15))
				IF pcktime GT 0 THEN BEGIN
                                      if pcktime-t0 le 86400 or fi gt 0 THEN begin
					grtime(d)	=strmid(line,16,15)
					ptim(d)		=pcktime-t1
;					IF ptim[d] GT ndays*86400 THEN message,'ptim is too big.'
					; start at time zero
                	    		strtime(d)	=strmid(line,32,15)
					pcnt(d)		=strmid(line,66,5)
					ptot(d)		=long(strmid(line,80,7)) +ptotl
					seqerr(d)	=strmid(line,90,5)
				      d=d+1
    				    ENDIF
     	    		       ENDIF else print,'Bad row after',d
  			endwhile
    	    	    	d2=where(strmid(strtime,0,6) eq strmid(strtime(0),0,6))	
		    	grtime= grtime(d2)
			ptim=	ptim(d2)
			strtime=strtime(d2)
			pcnt=	pcnt(d2)
			ptot=	ptot(d2)
			seqerr= seqerr(d2)

    	    	        packettim=ptim-ptim(0)
                        

;			stop
    	    if apid eq '451' then gapsize=60*60*2. else gapsize=900 ;2 hours
            zt1=[0,where(shift(packettim,-1)-packettim gt gapsize)+1]
            zt2=[where(shift(packettim,-1)-packettim gt gapsize)-1,d2(n_elements(d2)-1)]

            dom=strmid(strtime(zt1),3,3)-doy0+1
            hrs1=strmid(strtime(zt1),7,2)+strmid(strtime(zt1),10,2)/60.0+strmid(strtime(zt1),13,2)/(60.0*60.0)
            hrs2=strmid(strtime(zt2),7,2)+strmid(strtime(zt2),10,2)/60.0+strmid(strtime(zt2),13,2)/(60.0*60.0)
            for nz=0,n_Elements(dom)-1 do oplot,[hrs1(nz),hrs2(nz)],[dom(nz)+((na*.2)+.2),dom(nz)+((na*.2)+.2)],thick=3,color=clr(na)

    	endif

	skipit:
    	close,1
	ENDFOR 	

  xyouts,1+(na*2),doy1-doy0+3.5,apidt(na),color=clr(na);,font=0
;stop
endfor

im=ftvread(filename='/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SSR2/'+strupcase(sc)+'_SSR2_'+yyyy+'_'+mm,/png,/noprompt)

wdel,11


;device,/close
;set_plot,'x'

;spawn,'ps2pdf idl.ps '+'/net/cronus/opt/local/idl_nrl_lib/secchi/data/statistics/SSR2/'+strupcase(sc)+'_SSR2_'+yyyy+'_'+mm+'.pdf'


 end
