pro parse_pkts, filename, pkts, grt, seqcnts, grh, n_packets=k_n_packets, $
                  COMBINE_OUTPUT=combine_output, errpause=errpause, LOUD=loud, nodata=k_nodata, $
                  header_only=header_only, APID=apid, HK=hk
;+
; Project     :	STEREO 
;
; Name        :	READ_SCC_ALL()
;
; Purpose     :	Wrapper for read_stereo_pkt.pro from SSC
;
; Category    :	STEREO, Telemetry
;
; Explanation :	utilize read_stereo_pkt.pro for various outputs
;
; Syntax      :	
;
; Examples    :	
;
; Inputs      :	filename    PTP or STP filename
;
; Opt. Inputs :	None.
;
; Outputs     :	INT
;
; Opt. Outputs:	None.
;
; Keywords    : N_PACKETS = number of packets into file to go (default is all)
;
;   	    	APID = var;  return apids INTARR in returned packets.
;   
;   	    	/HK 	Return all non-science packets
;
;   	    	/COMBINE_OUTPUT     Return same structure format as read_stereo_pkt.pro
;   	    	
;               /ERRPAUSE = If set, then the normal error handling is bypassed.
;                          Only used for debugging.
;
;   	    	/NODATA = If set, then only the packet header information is
;                        returned.
;
;   	    	/LOUD	Print status of reading file
;
;   	    	/HEADER_ONLY = Only read in the packet header
;                             information.  Only used for the header-only
;                             beacon ".ptp.hdr" or ".stp.hdr" files.
;
; Calls       :	ieee_to_host
;
; Common      :	None.
;
; Restrictions:	
;
; Side effects:	None.
;
; Prev. Hist. :	read_stp_all.pro
;-
; $Log: parse_pkts.pro,v $
; Revision 1.2  2015/07/29 17:29:49  nathan
; updated version
;
;
;on_error, 2
if n_elements(k_nodata) eq 1 then nodata=k_nodata else nodata=1
;
if n_params() LT 2 then message, $
  'Syntax: PARSE_PKTS, FILENAME, PACKETS'
;
IF n_elements(filename) gt 1 then begin
    delvarx, packets, /old
    nfiles = n_elements(filename)
    if keyword_set(LOUD) then countdown, nfiles, /open
    for i=0L,nfiles-1 do begin
        if keyword_set(LOUD) then countdown, i
        PARSE_PKTS, filename[i], p, n_packets=k_n_packets, /combine_out, $
          errpause=errpause, nodata=k_nodata, header_only=header_only
        if n_elements(p) gt 0 then begin
            if n_elements(packets) eq 0 then packets = p else $
              packets = [packets, p]
        endif
    endfor
    if keyword_set(LOUD) then countdown, /close
    
ENDIF ELSE BEGIN
;
if file_exist(filename) then name = filename else begin
    spawn, 'find . -follow -name ' + filename, output
    name = output[0]
endelse
if n_elements(k_n_packets) eq 1 then n_packets = k_n_packets else $
  n_packets = countstpackets(name, header_only=header_only)
if n_packets eq 0 then begin
    delvarx, packets, /old
    return
endif
openr, unit, name, /get_lun
;
read_stereo_pkt, unit, packet, nodata=nodata, errpause=errpause, $
  header_only=header_only
if n_packets gt 1 then packets = replicate(packet, n_packets) else $
  packets = packet
i_packet = 0L
if keyword_set(LOUD) then countdown, n_packets, /open
;;while n_elements(packet) ne 0 do begin
for ii=1L,n_packets-1 do begin
    read_stereo_pkt, unit, packet, nodata=nodata, errpause=errpause, $
      header_only=header_only
    if n_elements(packet) ne 0 then begin
        i_packet = i_packet + 1
        if keyword_set(LOUD) and ((i_packet mod 1000) eq 0) then countdown,i_packet
        if packet.grh.datatype ne 0 then begin
            if n_packets gt 1 then begin
                if i_packet ge n_packets then goto, done
                if packet.grh.datatype eq packets[0].grh.datatype then $
                  packets[i_packet] = packet else packets[i_packet].grh.size=0
            end else packets = [packets, packet]
        end else packets[i_packet].grh.size=0
    endif
endfor
;;endwhile
;
help,packets
done:
if keyword_set(LOUD) then countdown, /close
free_lun, unit

ENDELSE ; 1 file

get_utc,u
np=n_elements(packets)
grt=replicate(u,np)
for i=0,np-1 do grt[i].mjd=packets[i].grh.grt_epoch_day+36204
for i=0,np-1 do grt[i].time=packets[i].grh.grt_millisec
apids=packets.pkt.hdr-2048
seqcnts=packets.pkt.grp-49152
IF keyword_set(COMBINE_OUTPUT) THEN pkts=packets ELSE pkts=packets.pkt
grh=packets.grh
IF keyword_set(HK) THEN BEGIN

    w=where(apids LT 1072)
    ;na=n_elements(apid)
    ;w=where(apids eq apid[0],nw)    
    ;IF na GT 1 THEN BEGIN
    ;	FOR i=1,na-1 DO BEGIN
	;    wi=where(apids eq apid[i],nwi)
	;    w=[w,wi]
;	ENDFOR
;    ENDIF
;    wgd=where(w GE 0,ngd)
;    IF ngd GT 0 THEN w=w[ngd] ELSE BEGIN
;    	message,'Input APID not found.',/info
;    ENDELSE
    
    srt=sort(w)
    w=w[srt]
    grt=grt[w]
    seqcnts=seqcnts[w]
    pkts=pkts[w]
    grh=grh[w]
    apid=apids[w]
    
ENDIF ELSE apid=apids

end
