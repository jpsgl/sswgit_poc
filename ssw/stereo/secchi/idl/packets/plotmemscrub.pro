pro plotmemscrub,d1,d2,sc, UPDATE=update, SAVEDIR=savedir
; $Id: plotmemscrub.pro,v 1.1 2011/02/11 16:49:44 nathan Exp $
;
; Project:  STEREO SECCHI
;
; Purpose: Plot EDC errors for SECCHI-A or SECCHI-B
;   	    
; Inputs:
;	d1,d2	start and end dates for updating values in sdir
;   	    	d2 is also last day of plot
;   	sc  	'a' or 'b' 
;
; Keyword Inputs:
;   	/UPDATE Run sccreadpackets.pro on input dates
;   	SAVEDIR=    Directory to save in, default is /net/earth/data4/secchi/operations/hk/memscrub.
;
; Outputs:
;	Generates plot in ./. If /update then generates files in savedir.
;
; Keyword output:
;
; Restrictions: 
;   	see sccreadpackets.pro
;
; Author: N.Rich, NRL/I2, 
;
; $Log: plotmemscrub.pro,v $
; Revision 1.1  2011/02/11 16:49:44  nathan
; make plots on wiki page/ops/trends
;

sc=strupcase(sc)
mnm=['MSRADCUMEDCERRS','MSRADCUMUNCERRS','MSSWICCUMEDCERRS','MSSWICCUMUNCERRS']
IF keyword_set(SAVEDIR) THEN sdir=savedir ELSE sdir='/net/earth/data4/secchi/operations/hk/memscrub'
IF keyword_set(UPDATE) THEN BEGIN
    s=sccreadpackets(d1,d2,sc,mnm,saved=sdir,/changed)
    help,/str,s
    maxmin,s.val
ENDIF
li=findfile(sdir+'/scc'+sc+'*x402.csv')
nli=n_elements(li)
help,li

;mstemplate=ascii_template(li[0])
;save,filen=concat_dir(sdir,'memscrubtemplate.sav'),mstemplate

restore,concat_dir(sdir,'memscrubtemplate.sav')

s=read_ascii(li[0],template=mstemplate)
ti=s.pktime
da1=s.(1)
da2=s.(2)
da3=s.(3)
da4=s.(4)

n=n_elements(s.pktime)

help,/str,s

FOR i=1,nli-1 DO BEGIN
    sn=read_ascii(li[i],template=mstemplate)
    help,/str,sn
    nsn=n_elements(sn.pktime)
    j=0
    while sn.(1)[j] LE s.(1)[n-1] do j=j+1
    help,j
    ti =[ti, sn.pktime[j:nsn-1]]
    da1=[da1,sn.(1)[j:nsn-1]]
    da2=[da2,sn.(2)[j:nsn-1]]
    da3=[da3,sn.(3)[j:nsn-1]]
    da4=[da4,sn.(4)[j:nsn-1]]
ENDFOR

help,ti,da1,da2,da3,da4

tt='SECCHI-'+strupcase(sc)+' MemScrub EDC Errors in one day'
d1=da1-shift(da1,1)
wd1=where(d1 gt 0,nd1)
help,wd1

; put counts into day bins
bin_data, ti[wd1],d1[wd1], tid1b, d1b, days=1, /fill
ndy=float(n_elements(d1b))
rnz=where(d1b gt 0,nrnz)

ut2=anytim2utc(d2)
tran=['2006/10/20',utc2str(ut2)]
ylim=30

plotvtime,tid1b[rnz], d1b[rnz],'rad750 ('+string(100*nrnz/ndy,'(f4.1)')+'% of days)',title=tt,yran=[0,ylim],psym=-2,timerange=tran

d3=da3-shift(da3,1)
wd3=where(d3 gt 0,nd3)

; put counts into day bins
bin_data, ti[wd3],d3[wd3], tid3b, d3b, days=1, /fill
snz=where(d3b gt 0,nsnz)

oplotvtime,tid3b[snz], d3b[snz],'SWIC ('+string(100*nsnz/ndy,'(f4.1)')+'% of days)',color=4,psym=-2

plotsecchievents,'watchdog',atim=wtra,btim=wtrb,/noplot
help,wtra,wtrb
IF sc EQ 'A' THEN BEGIN
    wtlabti='2008/09/11' 
    wtr=wtra
ENDIF ELSE BEGIN
    wtlabti='2009-03-06'
    wtr=wtrb
ENDELSE

print,wtr

evt_grid,wtr,color=2,linesty=2
ut_label,wtlabti,29,'WTResets',align=1,labcolor=2

wbigr=where(d1b gt ylim,nbigr)
help,wbigr
IF nbigr GT 0 THEN BEGIN
    print,utc2str(tid1b[wbigr]),d1b[wbigr]
    FOR i=0,nbigr-1 DO $
    ut_label,tid1b[wbigr[i]],ylim-1-i,trim(d1b[wbigr[i]])+'@'+utc2str(tid1b[wbigr[i]],/ecs,/trunc),align=0
ENDIF

wbigs=where(d3b gt ylim,nbigs)
help,wbigs
IF nbigs GT 0 THEN BEGIN
    print,utc2str(tid3b[wbigr]),d3b[wbigr]
    FOR i=0,nbigs-1 DO $
    ut_label,tid3b[wbigs[i]],ylim-1-i,trim(d3b[wbigs[i]])+'@'+utc2str(tid3b[wbigs[i]],/ecs,/trunc),align=0,labcolor=4
ENDIF

x=ftvread(/png,filename='./msedcerrs'+sc+utc2yymmdd(ut2))

print,''
print,'Total EDC errors'
print,'RAD750:',total(d1b)
print,'SWIC:  ',total(d3b)
print,''

w2=where(da2 gt 0,nw2)
;w3=where(da3 gt 0)
w4=where(da4 gt 0,nw4)
help,w2,w4
IF nw2+nw4 GT 0 THEN BEGIN
    print,''
    message,'Uncorrected error detected!',/info
    IF nw2 GT 0 THEN print,'RAD750: ',ti[w2],da1[w2],da2[w2],da3[w2],da4[w2]
    print,''
    IF nw4 GT 0 THEN print,'SWIC:   ',ti[w4],da1[w4],da2[w4],da3[w4],da4[w4]
    print,''
    
ENDIF
stop
end
