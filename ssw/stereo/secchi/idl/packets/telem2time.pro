
function telem2time,ts,te,s_c,apid0,file=file,dir=dir,no_grh=no_grh

;+
; $Id: telem2time.pro,v 1.3 2007/08/28 21:07:37 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : telem2time
;
; Purpose : reads a LZ telemetry file (from SSC website) and extracts
;           the packet system time from the SECCHI HK packets (Apid 1025 = x401)
;
; Use     : IDL> d = telem2time(file=file_search('secchi_ahead*ptp'))
;           IDL> d = telem2time(ts,te,'a')
;
; Inputs  :
;    ts = start time (anytim format).
;    te = end time.
;    s_c = 'a' or 'b'
; all 3 inputs required unless file keyword supplied
;
; Outputs :
;    d = structure vector containing gt housekeeping data.
;        not all mnemonics are included yet.
;
; Keywords:
;    file = file list  (input, optional)
;    dir = directory for automatic file search (used only if file not supplied)
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jeff Newmark 12-dec-2006
;
; $Log: telem2time.pro,v $
; Revision 1.3  2007/08/28 21:07:37  nathan
; updates to make more general (incomplete)
;
; Revision 1.2  2007/06/22 18:35:47  mcnutt
; corrected SDS directory
;
; Revision 1.1  2007/01/30 16:00:26  nathan
; unchanged from Jeff
;
;-

n = n_elements(file)
if n eq 0 then begin
   if n_elements(dir) ne 1 then begin
      if strlowcase(strmid(s_c,0,1)) eq 'a' then obs='ahead' else obs='behind'
      dir=getenv_slash('SDS_DIR') $
         +obs+'/data_products/level_0_telemetry/secchi'
   endif
   file = time2telemfile(ts,te,dir)
   n = n_elements(file)
endif
print,file
print

IF datatype(apid0) EQ 'STR' THEN apid=hex2decf(apid0) ELSE apid=apid0
;apid = 1025                  ; apid for hk data


;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;+  Generate missing packet report
;+
;-- report header
dec2hex,apid,apidhex
apidhex=strupcase(apidhex)
outpfileroot='H'+apidhex+'_'+utc2yymmdd(str2utc(ts)),/hhmmss)
openw,lun1,outpfileroot+'LZ_gaps.txt',/get_lun

printf,lun1, ';+++++++++++++++++++++++++++++++++++++++++++++++++++++++'
printf,lun1, ';+ LZ file Gaps in Packet Sequence for ApID 0x'+apidhex
printf,lun1, ';+ Before and after gap boundary'
              ;0        1         2         3         4         5         6         7
              ;12345678901234567890123456789012345678901234567890123456789012345678901234567890
printf,lun1, ';++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
printf,lun1, ';+    Tot  Last pkt             First pkt            Skipped  Seconds PktCnt PktCnt'
printf,lun1, ';+ Pktcnt  Before gap           After gap               Pkts   of Gap Before  After'
printf,lun1, ';++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
              ;12345678  yyyy-mm-dd hh:mm:ss  yyyy-mm-dd hh:mm:ss  1234567  1234567  12345  12345 
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words

d0 = {t:0d0, n:0u}	; data structure: { TAI packet time, UNSIGNED INT packet counter }

pktcnt   =  2+ghb	; byte offset
pkttime  =  6+ghb
subsec   = 10+ghb

rec = intarr(136+ghi)        ; 1 packet = 272 Bytes
;dt = 7670L*24L*3600L         ; diff. betw. SC and anytim time def.
dt=0

; intermediate size data vector to improve speed (d=[d,d0] is slow!)
n10 = 1440L
temparr = replicate(d0,n10)
i10 = 0L
totpkts=0L

on_ioerror,endfil
for i=0,n-1 do begin

    openr,lun,file[i],/get_lun
	print,'reading ',file[i]
    while 1 do begin
		readu,lun,rec
		; so it works on linux PCs, need to swap words AFTER being retrieved
		; from array		
		rapid0=rec[ghi]
		byteorder,rapid0,/sswap,/swap_if_little_endian
		rapid=(rapid0 and 2047)
		
		;help,rapid
		if rapid eq apid then begin
			;--parse packet
			;--first account for swapping
			sec0=long(rec,pkttime)
			byteorder,sec0,/lswap,/swap_if_little_endian
			d0.t = double(sec0-dt)+double(byte(rec,subsec))/256d0
			; don't need to swap 1 byte
			cnt0=fix(rec,pktcnt)
			byteorder,cnt0,/sswap,/swap_if_little_endian
			d0.n = (cnt0 and 16383)	
			; counter is the last 14 bits of 2-bytes
			;print,ulong(rec,pkttime),byte(rec,subsec)
			;wait,.5
			temparr[i10] = d0	; build intermediate size vector of structures
			i10 = i10 + 1L
			if i10 eq n10 then begin
			; append intermediate size data vector to output result
				if n_elements(outp) gt 0L then outp=[outp,temparr] else outp=temparr
					
				i10 = 0L
			endif
	
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;+  Generate missing packet report (cont.)
;--Check for gap and if gap then print line
			
			skippedpkts=d0.n-nlast-1
			IF skippedpkts LT 0 THEN skippedpkts=skippedpkts+16384
			; does not work if more than one wraparound
			totpkts=totpkts+skippedpkts+1
			IF skippedpkts GT 0 and totpkts GT 1 THEN BEGIN
			
				lineout= 	string(totpkts,format='(I8)')+'  ' + $
							utc2str(anytim2utc(tlast),/ECS,/TRUNCATE)+'  ' + $
							utc2str(anytim2utc(d0.t) ,/ECS,/TRUNCATE)+'  ' + $
							string(skippedpkts,format='(I7)')+'  '+ $
							string(long(d0.t-tlast),format='(I7)')+'  '+ $
							string(nlast,format='(I5)')+'  '+ $
							string(d0.n,format='(I5)')
				print,lineout
				printf,lun1,lineout
				totskippedpkts=totskippedpkts+skippedpkts
			ENDIF
			tlast=d0.t
			nlast=d0.n
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		endif
		;if rapid eq apid then stop
   endwhile
    endfil: free_lun,lun

endfor

if i10 gt 0L then begin
	pktsmryarr = pktsmryarr[0L:i10-1L]
	if n_elements(outp) gt 0L then outp=[outp,pktsmryarr] else outp=pktsmryarr
endif

if datatype(outp) EQ 'UND' THEN BEGIN
	print,'Apid ',trim(string(apid)) , ' not found for ',ts,' to ',te
	return,0
endif

if n_params() ge 2 then begin
   ww = where(outp.t ge anytim(ts) and outp.t le anytim(te),nww)
   if nww gt 0 then d=outp[ww] else outp=0
endif

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;+  Generate missing packet report (cont.)
;--print summary line
printf,lun1, ';+++++++++++++++++++++++++++++++++++++TOTALS+++++++++++++++++++++++++++++++++++++++'
				lineout= 	string(totpkts,format='(I8)')+'  ' + $
							utc2str(anytim2utc(tlast),/ECS,/TRUNCATE)+'  ' + $
							utc2str(anytim2utc(d0.t) ,/ECS,/TRUNCATE)+'  ' + $
							string(skippedpkts,format='(I7)')+'  '+ $
							string(long(d0.t-tlast),format='(I7)')+'  '+ $
							string(nlast,format='(I5)')+'  '+ $
							string(d0.n,format='(I5)')

stop
return,d

end

