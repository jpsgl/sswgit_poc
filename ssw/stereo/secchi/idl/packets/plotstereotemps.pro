;+
pro plotstereotemps,fn, _EXTRA=_extra
; $Id: plotstereotemps.pro,v 1.1 2009/04/13 20:32:47 nathan Exp $
; Name:     plotstereopwr
;
; Purpose:  plot values from file created by stereopwr.sprt
;
; Inputs:   fn	STRING	File to plot values from
;
; Optional input keywords:
;   	    Any plot keywords
;
; Outputs:  none - plots values in window
;
; Common blocks:    	
;
; WRitten by N.Rich, NRL/I2
;-
; $Log: plotstereotemps.pro,v $
; Revision 1.1  2009/04/13 20:32:47  nathan
; checkin
;

common sccplot, currentstmpl, voltagestmpl, aorb, xfscitemplate, stereopwrfmt, sctempsfmt

IF datatype(sctempsfmt) EQ 'UND' THEN restore,getenv('SCC_DATA')+'/templates/stereotlmtemplates.sav'

print,'Reading ',fn
s=read_ascii(fn,template=sctempsfmt)

ymn=0.
ymx=0.
ymn=min([s.sccor1t,ymn,ymx],max=ymx)
ymn=min([s.sccor2t,ymn,ymx],max=ymx)
ymn=min([s.sceuvit,ymn,ymx],max=ymx)
ymn=min([s.scgtt,ymn,ymx],max=ymx)
ymn=min([s.scscipift,ymn,ymx],max=ymx)
ymn=min([s.scsebt,ymn,ymx],max=ymx)
ymn=min([s.sccor1doort,ymn,ymx],max=ymx)
ymn=min([s.sccor2doort,ymn,ymx],max=ymx)
ymn=min([s.sceuvidoort,ymn,ymx],max=ymx)

ymn=floor(ymn)
ymx=ceil(ymx)

plotvtime,s.x00btime,s.sccor1t,'cor1',title='SCIP Temperatures from S/C', ytitle='Deg C', yrang=[ymn,ymx], _Extra=_extra
oplotvtime,s.x00btime,s.sccor2t,'cor2', _Extra=_extra
oplotvtime,s.x00btime,s.sceuvit,'euvi', _Extra=_extra
oplotvtime,s.x00btime,s.scgtt,'gt', _Extra=_extra
oplotvtime,s.x00btime,s.scscipift,'scipif', _Extra=_extra
oplotvtime,s.x00btime,s.scsebt,'sebif', _Extra=_extra
oplotvtime,s.x00btime,s.sccor1doort,'c1door', _Extra=_extra
oplotvtime,s.x00btime,s.sccor2doort,'c2door', _Extra=_extra
oplotvtime,s.x00btime,s.sceuvidoort,'eudoor', _Extra=_extra

ymn=0.
ymx=0.
ymn=min([s.schi1t,ymn,ymx],max=ymx)
ymn=min([s.schi2t,ymn,ymx],max=ymx)
ymn=min([s.schiift,ymn,ymx],max=ymx)
ymn=min([s.schibafcvrt,ymn,ymx],max=ymx)

ymn=floor(ymn)
ymx=ceil(ymx)

window,2,xsiz=800,ysiz=600
plotvtime,s.x00btime,s.schi1t,'hi1',title='HI Temperatures from S/C', ytitle='Deg C', yrang=[ymn,ymx], _Extra=_extra,/NOWIN
oplotvtime,s.x00btime,s.schi2t,'hi2', _Extra=_extra
oplotvtime,s.x00btime,s.schiift,'hiintf', _Extra=_extra
oplotvtime,s.x00btime,s.schibafcvrt,'hibafcvr', _Extra=_extra

end
