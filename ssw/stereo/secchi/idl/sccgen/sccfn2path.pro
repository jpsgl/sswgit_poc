function sccfn2path,list
;
; $Id: sccfn2path.pro,v 1.1 2005/07/29 20:59:53 nathan Exp $
;
; Purpose: convert list of filenames into filenames with path
;
; Inputs:	list	STRARR	list of fits files
;
; Outputs:	STRARR	path + list of fits files
;
; $Log: sccfn2path.pro,v $
; Revision 1.1  2005/07/29 20:59:53  nathan
; moved from nrlgen
;
; Revision 1.1  2005/07/29 19:40:05  nathan
; get path from filename
;

n=n_elements(list)
topdir=getenv_slash('secchi')
outp=strarr(n)

for i=0,n-1 do begin
	; build root directory
        len=strlen(list[i])
        tt=strmid(list[i],len-7,2)
        IF tt EQ 'eu' THEN tel='euvi'
        IF tt EQ 'c1' THEN tel='cor1'
        IF tt EQ 'c2' THEN tel='cor2'
        s =strlowcase(strmid(list[i],len-5,1))
        dir=topdir+s+'/'+tel+'/'+strmid(list[i],0,8)
	    outp[i]=dir+'/'+list[i]
endfor

return,outp

end
