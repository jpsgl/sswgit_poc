function scc_fitshdr2struct,fits_hdr
;+
; $Id: scc_fitshdr2struct.pro,v 1.4 2007/11/20 22:01:41 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_fitshdr2struct
;               
; Purpose   : Converts SECCHI FITS header into SECCHI STRUCTURE header
;               
; Explanation: This routine defines the SECCHI STRUCTURE then reads
;              the fits header into the structure
;               
; Use       : IDL> struct = scc_fitshdr2struct(fits_hdr)
;    
; Inputs    : hdr - string type fits header from SECCHI image 
;               
; Outputs   : struct - header information in SECCHI STRUCTURE
;
; Keywords  :               
;
; Common    : secchi_header              
;               
; Calls     : DEF_SECCHI_HDR
;
; Restrictions: Only works with one string	              
;
; Category  : Administration
;              
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU July 2006
;               
; $Log: scc_fitshdr2struct.pro,v $
; Revision 1.4  2007/11/20 22:01:41  nathan
; fixed another syntax error
;
; Revision 1.3  2007/11/20 19:18:12  nathan
; syntax error
;
; Revision 1.2  2007/11/20 19:08:30  nathan
; Defined new variable for output so that sechdrstruct, which is used in
; sccreadfits.pro, is not changed.
;
; Revision 1.1  2007/08/08 20:09:54  colaninn
; moved from prep
;
; Revision 1.6  2007/05/21 17:40:02  nathan
; fix type conversion error reported by Karl
;
; Revision 1.5  2007/04/11 21:02:54  thompson
; Check for additional boolean parameters.
;
; Revision 1.4  2007/02/13 15:22:31  colaninn
; removed check for level2 header
;
; Revision 1.3  2007/01/31 18:47:30  colaninn
; verified last update
;
; Revision 1.2  2007/01/22 23:28:01  thompson
;
; Catch DATE_* vs. DATE-*, boolean values
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.2  2006/08/03 18:22:49  colaninn
; added INSTRUME check
;
; Revision 1.1  2006/07/25 21:13:19  colaninn
; first draft
;
;-            
COMMON secchi_header, sechdrstruct, sechdrtags

;--Check if secchi header structruct is defined in COMMON BLOCK 
IF DATATYPE(sechdrstruct) EQ 'UND' THEN sechdrstruct=DEF_SECCHI_HDR(sechdrtags)
scch=sechdrstruct

;--Check that header is from secchi instrument
IF STRTRIM(FXPAR(fits_hdr,'INSTRUME'),2) NE 'SECCHI' THEN $
MESSAGE, '%%SCC_ FITSHDR2STRUCT: NOT FOR NON-SECCHI INSTRUMENTS'

;--Adds F or T instead of 0 and 1
secchi_boolean = ['EVENT', 'EXTEND', 'RECTIFY', 'SIMPLE', 'SPWX', 'SYNC', $
                 'FPS_ON', 'DIV2CORR', 'SCANT_ON', 'SCFP_ON', 'FPS_CMD']
boolean = ['F','T']

numtag = n_elements(sechdrtags)
FOR t=0,numtag-1 DO BEGIN  
   ;--Get keyword for sechdrtags(secchi tag string)
    keyword = sechdrtags[t]     

    ;--Fix any "DATE_*" keywords to read "DATE-*".
    IF STRMID(keyword,0,5) EQ 'DATE_' THEN strput, keyword, '-', 4

    var = FXPAR(fits_hdr, keyword, COUNT=cnt) ;gets value with FXPAR
    IF ((keyword EQ 'COMMENT') OR (keyword EQ 'HISTORY')) THEN BEGIN 
    ;--Only these keywords may have multiple entries 
       FOR c=0,cnt-1 DO BEGIN
            val = STRTRIM(var[c], 2)
            scch.(t)(c) = val
        ENDFOR
    ENDIF ELSE BEGIN

      ;--Fix any boolean values to read as 'T' or 'F'
      wb = where(keyword EQ secchi_boolean, count)

      IF (DATATYPE(var[0]) EQ 'STR') THEN var = STRTRIM(var[0], 2) ELSE $
      	    IF count GT 0 THEN var = boolean[var[0]]

      scch.(t) = var[0]
    ENDELSE

ENDFOR

RETURN,scch
END
