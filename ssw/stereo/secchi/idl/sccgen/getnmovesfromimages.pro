pro getnmovesfromimages, indx
;+	
;$Id: getnmovesfromimages.pro,v 1.2 2005/10/17 19:05:03 nathan Exp $
;
; NAME:				
; PURPOSE: generates a summary file of types of images where mechs move
; CATEGORY:			
; CALLING SEQUENCE:		
; INPUTS:			array of secchi header structures to search
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS: appends ./nmovesumm.txt
;  	
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	
; PROCEDURE:
;
; MODIFICATION HISTORY:		
; $Log: getnmovesfromimages.pro,v $
; Revision 1.2  2005/10/17 19:05:03  nathan
; fixed 050802
;
; Revision 1.1  2005/06/14 18:54:31  nathan
; update after SCIP-B TVAC
;
;
;-
;
nim=n_elements(indx)
sorted=sort(indx.date_obs)

euvinorm=where(indx.detector EQ 'EUVI' and indx.seb_prog EQ 'NORMAL',neuvinorm)
help,euvinorm
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuvinorm-1 do begin
    if indx[euvinorm[i]].encoderq NE indx[0>(euvinorm[i]-1)].encoderq THEN nne=nne+1
    ;prevpos=indx[euvinorm[i]].encoderq
endfor
nneunq=nne
help,nne
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuvinorm-1 do begin
    if indx[euvinorm[i]].encoderf NE indx[0>(euvinorm[i]-1)].encoderf THEN nne=nne+1
    ;prevpos=indx[euvinorm[i]].encoderf
endfor
nneunf=nne
help,nne
;++++++++++++++++++++++++++++++++++
euviseq =where(indx.detector EQ 'EUVI' and indx.seb_prog EQ 'SEQ',neuviseq)
help,euviseq
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuviseq-1 do begin
    if indx[euviseq[i]].encoderq NE indx[0>(euviseq[i]-1)].encoderq THEN nne=nne+1
    ;prevpos=indx[euviseq[i]].encoderq
endfor
nneusq=nne
help,nne
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuviseq-1 do begin
    if indx[euviseq[i]].encoderf NE indx[0>(euviseq[i]-1)].encoderf THEN nne=nne+1
    ;prevpos=indx[euviseq[i]].encoderf
endfor
nneusf=nne
help,nne
;++++++++++++++++++++++++++++++++++
euviblue=where(indx.detector EQ 'EUVI' and indx.led EQ 'BLUE',neuviblue)
help,euviblue
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuviblue-1 do begin
    if indx[euviblue[i]].encoderq NE indx[0>(euviblue[i]-1)].encoderq THEN nne=nne+1
    prevpos=indx[euviblue[i]].encoderq
endfor
nneubq=nne
help,nne
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuviblue-1 do begin
    if indx[euviblue[i]].encoderf NE indx[0>(euviblue[i]-1)].encoderf THEN nne=nne+1
    prevpos=indx[euviblue[i]].encoderf
endfor
nneubf=nne
help,nne
;++++++++++++++++++++++++++++++++++
euvipurp=where(indx.detector EQ 'EUVI' and indx.led EQ 'PURPLE',neuvipurp)
help,euvipurp
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuvipurp-1 do begin
    if indx[euvipurp[i]].encoderq NE indx[0>(euvipurp[i]-1)].encoderq THEN nne=nne+1
    prevpos=indx[euvipurp[i]].encoderq
endfor
nneupq=nne
help,nne
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,neuvipurp-1 do begin
    if indx[euvipurp[i]].encoderf NE indx[0>(euvipurp[i]-1)].encoderf THEN nne=nne+1
    prevpos=indx[euvipurp[i]].encoderf
endfor
nneupf=nne
help,nne
;++++++++++++++++++++++++++++++++++
cor1norm=where(indx.detector EQ 'COR1' and indx.seb_prog EQ 'NORMAL',ncor1norm)
help,cor1norm
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,ncor1norm-1 do begin
    if indx[cor1norm[i]].encoderp NE indx[0>(cor1norm[i]-1)].encoderp THEN nne=nne+1
    prevpos=indx[cor1norm[i]].encoderp
endfor
nnc1np=nne
help,nne
;++++++++++++++++++++++++++++++++++
cor1seq =where(indx.detector EQ 'COR1' and indx.seb_prog EQ 'SEQ',ncor1seq)
help,cor1seq
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,ncor1seq-1 do begin
    if indx[cor1seq[i]].encoderp NE indx[0>(cor1seq[i]-1)].encoderp THEN nne=nne+1
    prevpos=indx[cor1seq[i]].encoderp
endfor
nnc1sp=nne
help,nne
;++++++++++++++++++++++++++++++++++
cor2norm=where(indx.detector EQ 'COR2' and indx.seb_prog EQ 'NORMAL',ncor2norm)
help,cor2norm
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,ncor2norm-1 do begin
    if indx[cor2norm[i]].encoderp NE indx[0>(cor2norm[i]-1)].encoderp THEN nne=nne+1
    prevpos=indx[cor2norm[i]].encoderp
endfor
nnc2np=nne
help,nne
;++++++++++++++++++++++++++++++++++
cor2seq =where(indx.detector EQ 'COR2' and indx.seb_prog EQ 'SEQ',ncor2seq)
help,cor2seq
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,ncor2seq-1 do begin
    if indx[cor2seq[i]].encoderp NE indx[0>(cor2seq[i]-1)].encoderp THEN nne=nne+1
    prevpos=indx[cor2seq[i]].encoderp
endfor
nnc2sp=nne
help,nne
;++++++++++++++++++++++++++++++++++
cor2red =where(indx.detector EQ 'COR2' and indx.led EQ 'RED',ncor2red)
help,cor2red
;++++++++++++++++++++++++++++++++++
nne=0
prevpos=0
for i=0,ncor2red-1 do begin
    if indx[cor2red[i]].encoderp NE indx[0>(cor2red[i]-1)].encoderp THEN nne=nne+1
    prevpos=indx[cor2red[i]].encoderp
endfor
nnc2rp=nne
help,nne
;++++++++++++++++++++++++++++++++++
stop
yymmdd=utc2yymmdd(str2utc(indx[sorted[nim-2]].date_obs))
openw,1,'nmovesumm'+yymmdd+'.txt',/append
printf,1,' '
printf,1,'Summary of image information for '+indx[sorted[1]].date_obs+' to '+indx[sorted[nim-2]].date_obs
printf,1,' '

printf,1,'EUVI'+string(neuvinorm,format='(i6)')+' NORMAL +'+string(neuviseq,format='(i6)')+' SEQ +'+string(neuviblue,format='(i6)')+' BLUE +'+string(neuvipurp,format='(i6)')+' PURPLE ='+string(neuvinorm+neuviseq+neuviblue+neuvipurp,format='(i6)') $
	+ ' SHTR, '+string(nneunq+nneusq+nneubq+nneupq,format='(i6)')+' QS, '+string(nneunf+nneusf+nneubf+nneupf,format='(i6)')+' FW'
printf,1,'COR1'+string(ncor1norm,format='(i6)')+' NORMAL +'+string(ncor1seq,format='(i6)')+' SEQ                             ='+string(ncor1norm+ncor1seq,format='(i6)') $
	+ ' SHTR, '+string(nnc1np+nnc1sp,format='(i6)')+' POL'
printf,1,'COR2'+string(ncor2norm,format='(i6)')+' NORMAL +'+string(ncor2seq,format='(i6)')+' SEQ +'+string(ncor2red,format='(i6)') +' RED                 ='+string(ncor2norm+ncor2seq+ncor2red,format='(i6)') $
	+ ' SHTR, '+string(nnc2np+nnc2sp,format='(i6)')+' POL'

close,1

END
