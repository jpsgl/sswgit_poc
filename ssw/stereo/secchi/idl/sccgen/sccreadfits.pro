FUNCTION SccREADFITS, filename0, headers, NODATA=nodata,HEADER=str_header, EXTHDR=exthdr, $
    	_extra=_extra, LASCO=lasco, DEBUG=debug
;+
; $Id: sccreadfits.pro,v 1.12 2012/04/05 19:53:46 nathan Exp $
;
; Project     : STEREO - SECCHI
;                   
; Name        : SccREADFITS
;               
; Purpose     : Read a SECCHI FITS file to obtain an image and header array, or
;               read a list of SECCHI FITS files to obtain a cube of images
;               and an array of header structures.
;               
; Explanation : This routine calls the SSW routine mreadfits. It uses a 
;		SECCHI-specific header structure
;               
; Use         : IDL> image = SccREADFITS('filename' [,header])
;		IDL> images = SccREADFITS(filenamelist [,headers, /NODATA])
;    
;      A useful sequence is:
;      --------------------------------------------------------------------
;      IDL> junk=sccreadfits( files, hdrs, /nodata)       ; headers only->struct 
;      IDL> ss=where(hdrs.naxis2 eq 512)   		; vector filter
;      IDL> images=sccreadfits(files[ss],hdrs) 		; read desired->3D 
;      --------------------------------------------------------------------
;
; Inputs      : filename	Name of the SECCHI FITS file or array of filenames.
;               
; Outputs     : image		The image array or array of images.
;               
; Opt. Outputs: headers		The image header(s) returned as a(n array of)
;				SECCHI header structure(s)
;               
; Keywords    : /NODATA  	Set this keyword if you only want the header returned.
;           	OUTSIZE = [nx,ny] - 1 or 2 element array specifying
;                                   the output size of data array 
;               HEADER=     	Set this keyword to a varible which will
;                               return the FITS string header. 
;   	    	EXTHDR=	    	Set equal to a variable which will contain an array
;   	    	    	    	of structures, 1 element for each exposure of summed image(s)
;   	    	    	    	Set variable=1 before calling sccreadfits.
;   	    	/LASCO	    	Use lasco header structure
;   	    	/DEBUG	    Print how long it takes.
;
;
; Calls       : MREADFITS, MRDFITS
;
; Category    : Data_Handling, I_O
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL, 2004.04.26
;               
; Modified    :$Log: sccreadfits.pro,v $
; Modified    :Revision 1.12  2012/04/05 19:53:46  nathan
; Modified    :call mreadfits with /QUIET by default
; Modified    :
; Modified    :Revision 1.11  2012/03/23 15:41:08  nathan
; Modified    :remembered to use /nodata for 2nd mreadfits call
; Modified    :
; Modified    :Revision 1.10  2012/03/21 22:02:57  nathan
; Modified    :Add /debug timing info; re-read with convert2secchi if not secchi and
; Modified    :not /lasco
; Modified    :
; Modified    :Revision 1.9  2011/10/18 20:10:51  nathan
; Modified    :auto check for .gz if first in input list is not found, and vice versa
; Modified    :
; Modified    :Revision 1.8  2011/07/28 21:44:32  nathan
; Modified    :use def_lasco_hdr() instead of .sav file if /lasco
; Modified    :
; Modified    :Revision 1.7  2010/02/01 23:17:21  nathan
; Modified    :added /LASCO option
; Modified    :
; Modified    :Revision 1.6  2007/05/21 18:44:25  colaninn
; Modified    :added silent keyword to def_secchi_hr call
; Modified    :
; Modified    :Revision 1.5  2007/04/03 21:05:33  nathan
; Modified    :add EXTHDR keyword for extended headers
; Modified    :
; Modified    :Revision 1.4  2007/02/06 17:50:53  colaninn
; Modified    :added header keyword for string header
; Modified    :
; Modified    :Revision 1.3  2006/09/08 16:45:39  nathan
; Modified    :use /DASH2UNDERSCORE
; Modified    :
; Modified    :Revision 1.2  2006/02/14 23:30:10  nathan
; Modified    :update backwards compatibility for def_secchi_hdr v1.18
; Modified    :
; Modified    :Revision 1.1  2005/02/02 16:14:06  nathan
; Modified    :move to new directory
; Modified    : 
;
;-            

COMMON secchi_header, sechdrstruct, sechdrtags

debugon=keyword_set(DEBUG)

IF datatype(sechdrstruct) EQ 'UND' THEN sechdrstruct=def_secchi_hdr(sechdrtags, silent=silent)
IF keyword_set(LASCO) THEN BEGIN
    ;rootdir=getenv('SSW_LASCO')
    ;IF rootdir EQ '' THEN rootdir=getenv('NRL_LIB')
    ;RESTORE, FILEPATH('example_hdr.sav',ROOT_DIR=rootdir,SUBDIRECTORY=['idl','inout'])
    ;hdrstruct = lasco_hdr_struct
    hdrstruct=def_lasco_hdr()
ENDIF ELSE hdrstruct=sechdrstruct
ims=-1

IF not file_exist(filename0[0]) THEN BEGIN
    gzp=strpos(filename0,'.gz')
    wgz=where(gzp GT 0,ngz,complement=wnogz)
    IF gzp[0] GT 0 THEN $
    ; try without .gz
	filename0[wgz]=strmid(filename0[wgz],0,gzp[0]) $
    ELSE $
    ; try with .gz
    	filename0[wnogz]=filename0[wnogz]+'.gz'
    IF not file_exist(filename0[0]) THEN BEGIN
    	message,filename0[0]+' not found; exiting',/info
	return,0
    ENDIF
ENDIF
    	
t0=systime(1)
mreadfits, filename0, headers0, ims, STRTEMPLATE=hdrstruct, NODATA=nodata, /DASH2UNDERSCORE, header=str_header, QUIET=~debugon, _extra=_extra 
;wb=where(headers.blank eq -32768,nwb)
;IF nwb GT 0 THEN headers[wb].blank=0     
; if im is UINT, blank might be -32768 in the FITS file. in this case we always want blank=0 in software

; Fill in DATE_OBS in headers
n=n_elements(filename0)
IF ~keyword_set(LASCO) THEN BEGIN
    IF headers0[n-1].date_obs LT '2006-02-14' THEN isold=1 ELSE isold=0
    IF headers0[0].instrume NE 'SECCHI' THEN BEGIN
    	message,'Non-SECCHI header detected; re-reading with CONVERT2SECCHI.',/info
	mreadfits, filename0, headers0, /NODATA, /DASH2UNDERSCORE, header=str_header,_extra=_extra
	headers=convert2secchi(headers0[0],filename=filename0[0])
	for i=1,n-1 do headers=[headers,convert2secchi(headers0[i],filename=filename0[i])]
    ENDIF ELSE BEGIN
    	headers=headers0
	FOR i=0,n-1 DO BEGIN
    	    IF isold THEN BEGIN
		h=headfits(filename0[i])
		headers[i].date_obs=fxpar(h,'DATE-OBS')
		; Backwards compatability
		IF (headers[i].date LT '2006-02-08') THEN headers[i].ledcolor=fxpar(h,'LED')
		IF (headers[i].date LT '2006-02-14') THEN BEGIN
		    headers[i].biasmean=fxpar(h,'BIAS')
		    headers[i].gaincmd=fxpar(h,'GAIN')
		    headers[i].date_avg=fxpar(h,'DATE-MID')
		ENDIF
	    ENDIF
	    ;--populate extended header
	    IF keyword_set(EXTHDR) THEN IF headers[i].extend EQ 'T' and headers[i].n_images GT 1 THEN BEGIN
		xh=mrdfits(filename0[i],1)
		IF i EQ 0 THEN exthdr=xh ELSE exthdr=[exthdr,xh]
	    ENDIF
	ENDFOR
    ENDELSE
ENDIF ELSE headers=headers0

IF debugon THEN message,'took '+trim(systime(1)-t0)+' seconds.',/info
return, ims

END
