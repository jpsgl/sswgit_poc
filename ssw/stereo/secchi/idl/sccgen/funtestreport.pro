pro funtestreport, funtests, ALIVE=alive
;+
; $Id: funtestreport.pro,v 1.3 2005/06/14 18:54:31 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : funtestreport
;               
; Purpose   : generate plots (3.9 hours) and compute values for trending for functional tests (last 2 hours)
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : funtests  STRARR  list of funtest snap files

; Optional Inputs: 
;               
; Outputs   : generate plots and 1 report for all inputs

; Optional Outputs: 
;
; Keywords  : /ALIVE
;	/ALIVE	for aliveness test, plot 10 minutes, statistucs for 5 minutes 

; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database hk utility plotting reports
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Mar 05
;               
; $Log: funtestreport.pro,v $
; Revision 1.3  2005/06/14 18:54:31  nathan
; update after SCIP-B TVAC
;
; Revision 1.2  2005/05/20 17:59:12  nathan
; misc changes
;
; Revision 1.1  2005/04/21 19:25:12  nathan
; pros for generating statistics
;
;-            
common sccplot, currentstmpl, voltagestmpl, aorb

aorb='b'
upaorb=strupcase(aorb)
hblist=findfile('$scc'+aorb+'/*/outputs/prints/hbvolts*txt')
hbtvac=findfile('$scc'+aorb+'/*/outputs/prints/scip'+upaorb+'tvac/hbvolts*txt')
hblist=[hblist,hbtvac]

crntlist=findfile('$scc'+aorb+'/*/outputs/prints/currents*txt')
crnttvac=findfile('$scc'+aorb+'/*/outputs/prints/scip'+upaorb+'tvac/currents*txt')
crntlist=[crntlist,crnttvac]

nhb=n_elements(hblist)
IF nhb NE n_elements(crntlist) THEN message, 'HBVOLTlist has different number than CURRENTSlist'

hbtimes=strarr(nhb)

; get actual times from files
for i=0,nhb-1 do begin
        spawn,'head -4 '+hblist[i],outp
        IF (outp[0] NE '') THEN BEGIN
                parts=str_sep(outp[3],'|')
                sttime=parts[0]
                strput,sttime,'T',6
                sttime='20'+sttime
                ; sttime is format YYYY-doyThh:mm:ss.sssss
                hbtimes[i]=utc2str(str2utc(sttime),/noz,/trunc)
                IF (hblist[i] EQ 'solar5/outputs/prints/scipBtvac/hbvolts20050605_161334.txt') OR $
                   (hblist[i] EQ 'solar5/outputs/prints/scipBtvac/hbvolts20050605_180407.txt') THEN $
                        hblist[i]=addecstimes(hblist[i],'19:50:00')
	ENDIF
endfor

srtdhb=sort(hbtimes)
hbtimes=hbtimes(srtdhb)
hblist=hblist(srtdhb)
crntlist=crntlist(srtdhb)
; ++++++ start looking at funtest files ++++++++++
n=n_elements(funtests)

;cd,'$stereo/itos/testing/tlmanalysis',current=lastdir
IF keyword_set(ALIVE) THEN BEGIN
	tsttyp='alive' 
        plotdur='00:15:00'
        statdur='00:05:00'
ENDIF ELSE BEGIN
	tsttyp='fun'
        plotdur='03:55:00'
        statdur='02:00:00'
ENDELSE
openw,vlun,'$stereo/itos/testing/tlmanalysis/'+aorb+'/'+tsttyp+'testvreport'+upaorb+'.txt',/append,/get_lun
printf,vlun,'Test EndTime (local)| SEBBUSV | SEBBUSV | SEB+15V | SEB+15V | SEB-15V | SEB-15V | SEB+05V | SEB+05V | MTR+15V | MTR+15V '
printf,vlun,'                    |   AVG   |  STDEV  |   AVG   |  STDEV  |   AVG   |  STDEV  |   AVG   |  STDEV  |   AVG   |  STDEV  '
printf,vlun,'====================|=========|=========|=========|=========|=========|=========|=========|=========|=========|========='
;            2005-01-05T13:10:00 |  34.319 |   0.085 |  14.504 |   0.019 | -14.514 |   0.031 |   4.992 |   0.002 |  14.504 |   0.019

openw,ilun,'$stereo/itos/testing/tlmanalysis/'+aorb+'/'+tsttyp+'testireport'+upaorb+'.txt',/append,/get_lun
printf,ilun,'Test EndTime (local)| SEBBUSI | SEBBUSI | SCIPBUSI| SCIPBUSI| MEBMTRI | MEBMTRI |  HIBUSI |  HIBUSI | DOORMTRI| DOORMTRI'
printf,ilun,'                    |   AVG   |  STDEV  |   AVG   |  STDEV  |   AVG   |  STDEV  |   AVG   |  STDEV  |   AVG   |  STDEV  '
printf,ilun,'====================|=========|=========|=========|=========|=========|=========|=========|=========|=========|========='
;            2005-01-05T13:10:00 |  34.319 |   0.085 |  14.504 |   0.019 | -14.514 |   0.031 |   4.992 |   0.002 |  14.504 |   0.019

FOR i=0,n-1 DO BEGIN
    ; assume times in hbvolts files is UT    
    funendutc=tai2utc(file_date_mod(funtests[i],/ut))
    funendfstr=utc2yymmdd(funendutc,/hhmmss,/yyyy)
    time1str=utc2str(funendutc,/trunc)
    ;
    ; ++++++++++ compute input times ++++++++++
    ;
    ; ++++++++ subtract estimated duration of functional test ++++++
    print,'Start time:'
    funstartutc=addecstimes(time1str,plotdur,/diff,/utc)
    funstartfstr=utc2yymmdd(funstartutc,/hhmmss,/yyyy)
    time0str=utc2str(funstartutc,/trunc)

    ;hbtimes has the START time of the seqprt, so want time BEFORE funendtime
    ; assume times in hbvolts files is UT    
    ind =find_closest(time0str,hbtimes,/less)
    ind2=find_closest(time1str,hbtimes,/less)
    
    IF ind LT 0 THEN BEGIN
        print,' '
        print,'No seqprt file found for ',funtests[i],'; Continuing.'
        print,' '
        wait,5
        goto, next
    ENDIF
    
    ;ind = [ind,ind+1]	; include next file, if available
    voltfile=hblist[ind:ind2]
    crntfile=crntlist[ind:ind2]
    ;
    ; ++++++++++ generate plots +++++++++++++++
    ;
    help,time0str,time1str,ind,voltfile,crntfile
    print,'Waiting 10...'
    wait,10
    parsecurrents,crntfile,time0str,time1str, crnttimes, sebbusi, mebbus, scipbus, hibus, doormtr
    print,'Done with parse currents...waiting 10 ...'
    wait,10
    parsehbvolts, voltfile,time0str,time1str, volttimes, sebbusv, sebp15, sebm15, sebp15mtr, seb5v
    print,'Done with parse volts waiting 10 ...'
    wait,10
    
    nc = n_elements(crnttimes)
    ;IF (nc NE n_elements(volttimes)) THEN message, 'Number of current rows NE number of voltage rows'
    ;
    ; +++++ look at last 2 hours +++++
    ;
    time0astr=addecstimes(time1str,statdur,/diff,/ccsds)
    goodi=where(crnttimes gt time0astr)
    goodv=where(volttimes gt time0astr)
    help,goodi,goodv
    ;
    sccwritestats,vlun,time1str,sebbusv[goodv],sebp15[goodv],sebm15[goodv],seb5v[goodv],sebp15mtr[goodv]
    sccwritestats,ilun,time1str,sebbusi[goodi],scipbus[goodi],mebbus[goodi],hibus[goodi],doormtr[goodi]
   
	print,'Waiting...'
        wait, 10

    next:
ENDFOR

close,vlun
close,ilun
free_lun,vlun
free_lun,ilun
;break_file, filein, dlg, dirpath,fnbase,filext

end
