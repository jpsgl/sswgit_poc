;$Id: convert2secchi.pro,v 1.51 2016/03/16 21:09:11 nathan Exp $
;
; Project     : SECCHI + SOHO - LASCO/(EIT  C2 C3) + ...
;                   
; Name        : convert2secchi
;               
; Purpose     : create a SECCHI FITS file header structure 
;   	    	- from mvi frame header without reading in fits file -or-
;               - from non-SECCHI FITS header
;
; INPUTS :  ahdr0   STRUCT or FITs hdr STRARR
;
; Optional Input :  mvi_file_hdr    STRUCT  MVI file header
;
; KEYWORDS:
;   	/NOCORRECTION  Do not do roll, center, or other correction to input values
;   	    	    Equivalent to fix(strmid(ahdr.filename,1,1)) GT 3 for LASCO/EIT files
;   	XY= 	[width,height] of image frame. Required if MVI frame header and no mvi_file_hdr input.
;   	/DEBUG	Print more info
;   	FILENAME=   In case ahdr0 does not have filename in it.
;
; Use         : IDL>fhdr=convert2secchi(ahdr)

; Calls       : GET_SUN_CENTER, EIT_POINT, GET_SEC_PIXEL, GET_CROTA, get_roll_or_xy.pro
;
; Comments    : 
;               
; Side effects: assumes some values:
;   	For MVI hdr case, if fhdr.NAXIS1 + fhdr.P1row *2 gt 1044 then sumval =2 else 1 (will not work for all subfields)
;               
; Category    : 
;               
; Written     : N.Rich NRL/I2 Aug 2010
;               
; $Log: convert2secchi.pro,v $
; Revision 1.51  2016/03/16 21:09:11  nathan
; fix missing detector; allow Solar Orbiter binning keywords
;
; Revision 1.50  2016/02/22 15:29:47  nathan
; syntax error
;
; Revision 1.49  2014/11/03 18:57:37  hutting
; defines wlstr for hmi to create fhdr.filename
;
; Revision 1.48  2014/10/21 20:38:54  hutting
; removed extra line
;
; Revision 1.47  2014/10/21 20:13:21  hutting
; addeds filename to AIA level 1 data
;
; Revision 1.46  2013/07/23 16:12:23  mcnutt
; added keywords for MK4 headers
;
; Revision 1.45  2013/07/05 18:12:45  mcnutt
; added hmi to match aia
;
; Revision 1.44  2013/07/03 11:52:17  mcnutt
; set AIA rectify to 1 not T
;
; Revision 1.43  2012/08/13 17:09:55  avourlid
; Prevented overwrite of RSUN when keyword was defined previously (affects AIA headers).
;
; Revision 1.42  2012/07/17 22:11:34  nathan
; fix setting pcol/row before calling sccgetcrpix, and set rectify
;
; Revision 1.41  2012/06/28 21:32:51  nathan
; workaround for bad date_obs value appearing in lasco hdrs
;
; Revision 1.40  2012/04/05 19:54:31  nathan
; check for LEBXSUM in lasco hdr and broaden level-1 definition
;
; Revision 1.39  2012/03/28 22:44:59  nathan
; fix previous mod (fhdr.instrume eq SECCHI by default)
;
; Revision 1.38  2012/03/21 22:01:33  nathan
; add FILENAME=; if input not SECCHI struct but is SECCHI, return
;
; Revision 1.37  2012/01/25 17:14:10  nathan
; add MDI and MK3 stuff from generic_movie; use cam in getsccsecpix; fix cfile gotit for LASCO
;
; Revision 1.36  2011/11/09 20:57:44  nathan
; some more robustness for AIA
;
; Revision 1.35  2011/11/09 20:06:54  nathan
; fix case where AIA image has SECCHI-type FITS header
;
; Revision 1.34  2011/10/12 21:12:39  nathan
; fix case where mvi hdr filename is undefined
;
; Revision 1.33  2011/09/29 21:55:09  nathan
; AIA fits OBSRVTRY=SDO
;
; Revision 1.32  2011/09/26 18:02:32  nathan
; init ishi
;
; Revision 1.31  2011/09/23 16:51:24  nathan
; fill in rsun and dsun_obs and other kwords in scc_wcs_keywords
;
; Revision 1.30  2011/09/21 21:18:00  nathan
; do sun center estimate for all secchi
;
; Revision 1.29  2011/09/20 15:10:51  nathan
; fix fhdr.rectify; call getscccrpix if blank mvi header
;
; Revision 1.28  2011/09/19 21:29:44  nathan
; should be improved for /nofits option--but not done yet
;
; Revision 1.27  2011/05/18 19:42:32  nathan
; do not require filename to be defined, but print warning message
;
; Revision 1.26  2011/05/13 20:13:42  nathan
; do not call get_sun_center if xcen and ycen defined
;
; Revision 1.25  2011/05/13 19:47:58  nathan
; check for unit of cdelt1 in HI mvi header
;
; Revision 1.24  2011/05/04 20:23:58  nathan
; fix lasco crpix bug
;
; Revision 1.23  2011/05/04 20:05:27  nathan
; added XY= ; remove mvi_file_hdr dependencies
;
; Revision 1.22  2011/04/29 19:29:16  nathan
; assumve ffv for undefined LASCO MVI hdr
;
; Revision 1.21  2011/04/28 17:24:53  nathan
; remove /noexternal from get_solar_radius calls
;
; Revision 1.20  2011/04/25 16:47:10  nathan
; use parse_secchi_filename for MVI-frame headers so scc_playmoviem works without needing FITS files
;
; Revision 1.19  2011/02/14 19:32:56  nathan
; set time_obs=blank for lasco AFTER get_sun_center
;
; Revision 1.18  2010/12/08 14:06:54  mcnutt
; change fdhr fhdr in Hi section
;
; Revision 1.17  2010/11/30 20:53:24  nathan
; call get_solar_radius with /noexternal
;
; Revision 1.16  2010/11/24 17:21:26  nathan
; added /NOCORRECTIONS and /SILENT to fitshead2struct call
;
; Revision 1.15  2010/11/23 20:08:48  mcnutt
; change scch to fdhr in hi section
;
; Revision 1.14  2010/11/17 23:25:41  nathan
; do reset crota if not 0
;
; Revision 1.13  2010/11/17 23:05:23  nathan
; do not reset crota if mvi
;
; Revision 1.12  2010/11/17 22:49:12  nathan
; Redo how sun center and roll derived for SOHO.
;
; Revision 1.11  2010/09/28 19:40:20  nathan
; fix case where input is secchi
;
; Revision 1.10  2010/09/13 20:11:18  nathan
; set crota=crota1 for lasco
;
; Revision 1.9  2010/09/13 18:49:43  nathan
; make sure get_roll_or_xy returns degrees; do not do attitude calc if LASCO level-1
;
; Revision 1.8  2010/09/08 16:56:21  mcnutt
;  added sun center keyhole correction for lasco headers
;
; Revision 1.7  2010/08/27 19:40:02  nathan
; only reset fhdr.r*col/row if zero; reset time_obs=blank
;
; Revision 1.6  2010/08/26 13:30:47  mcnutt
; sets cunit1 and cunit2 when not using a mvi hdr
;
; Revision 1.5  2010/08/19 15:35:17  nathan
; use sccrorigin for aia
;
; Revision 1.4  2010/08/18 20:35:00  nathan
; attempt to compute subfield values
;
; Revision 1.3  2010/08/18 17:17:15  nathan
; fix lasco file search logic; sector for eit only
;
; Revision 1.2  2010/08/17 22:58:53  nathan
; fix bugs, such as EIT center
;
; Revision 1.1  2010/08/17 17:58:49  nathan
; called by wscc_mkmovie, scc_movie_win
;
; Log: mk_lasco_hdr.pro,v 
; Revision 1.11  2010/07/26 14:27:04  battams
; convert ra/dec to degrees
;
; Revision 1.10  2010-07-26 12:41:14  mcnutt
; corrected GEI crvals to sun position
;
; Revision 1.9  2010/07/22 18:34:45  mcnutt
; corrected GEI crvals
;
; Revision 1.8  2010/07/16 16:52:52  mcnutt
; Set crval1a and crval2a
;
; Revision 1.7  2010/07/16 16:20:15  mcnutt
; Sets RA and Dec coords
;
; Revision 1.6  2010/07/12 13:21:00  mcnutt
; will now work with a lasco fits header input
;
; Revision 1.5  2010/05/14 17:55:08  mcnutt
; added common block for lasco header
;
; Revision 1.4  2009/09/24 20:22:13  nathan
; added info messages
;
; Revision 1.3  2009/09/24 20:16:58  nathan
; added dummy values for EIT and C2 if original file is not available
;
; Revision 1.2  2009/01/29 20:47:24  nathan
; check input to see if it is a complete header structure, and if so use it as base for output
;
; Revision 1.1  2008/12/19 13:12:58  mcnutt
; creats minimal fits hdr with enough info to create wsc hdr with out reading in fits file
;

function convert2secchi,ahdr0,mvi_file_hdr, DEBUG=debug, NOCORRECTION=nocorrection, XY=xy, $
    FILENAME=filename

COMMON secchi_header, sechdrstruct, sechdrtags

IF keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0
debug_on=debugon

IF datatype(sechdrstruct) EQ 'UND' THEN BEGIN
    sechdrstruct=def_secchi_hdr(sechdrtags, silent=silent)
ENDIF
if datatype(ahdr0) eq 'STR' then ahdr=fitshead2struct(ahdr0,/dash2underscore, /silent) ELSE ahdr=ahdr0

IF strmatch(TAG_NAMES(ahdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN BEGIN
    message,'Input is already SECCHI_HDR_STRUCT; not changing',/info
    return,ahdr
ENDIF
IF tag_exist(ahdr,'INSTRUME') THEN instrume=ahdr.instrume ELSE instrume='UND' 
IF tag_exist(ahdr,'POLAR') THEN IF datatype(ahdr.polar) EQ 'STR' THEN IF ahdr.polar EQ 'Clear' THEN ahdr.polar='1001'
IF tag_exist(ahdr,'COMPRSSN') THEN IF datatype(ahdr.comprssn) EQ 'STR' THEN ahdr.COMPRSSN='0'	; leave undefined

; This command takes care of all identities
fhdr=str_copy_tags(sechdrstruct,ahdr)

IF ~tag_exist(ahdr,'filename') and tag_exist(ahdr,'lvl_num')  THEN BEGIN
   if strpos(strupcase(ahdr.instrume),'HMI') gt -1 then wlstr=strmid(ahdr.content,0,3) else wlstr=strmid(ahdr.wave_str,0,3)
   fhdr.filename=ahdr.instrume+'_'+wlstr+'_'+ahdr.date_obs+'_lvl'+string(ahdr.lvl_num,'(i1.1)')
ENDIF

IF instrume EQ 'SECCHI' THEN BEGIN
    ; Backwards compatability
    IF (fhdr.date LT '2006-02-08') and tag_exist(ahdr,'DATE') THEN fhdr.ledcolor=ahdr.LED
    IF (fhdr.date LT '2006-02-14') and tag_exist(ahdr,'BIAS') THEN BEGIN
	fhdr.biasmean=ahdr.BIAS
	fhdr.gaincmd=ahdr.GAIN
	fhdr.date_avg=ahdr.DATE_MID
    ENDIF
    IF debugon THEN message,'Input has hdr.INSTRUME=SECCHI so returning.',/info
    return,fhdr
ENDIF
IF keyword_set(FILENAME) THEN BEGIN
    break_file,filename,di,pa,ro,sf
    fhdr.filename=ro+sf
ENDIF
ismvi=1
ishi=0
IF tag_exist(ahdr,'NAXIS1') THEN ismvi=0
;
; Are attitude values already corrected?
;
level=0.5
IF keyword_set(NOCORRECTION) THEN level=1

if (ismvi) then begin

    fhdr.XCEN=0.    ; IF MVI, then this value has different meaning in FITS header
    fhdr.YCEN=0.

; Basic header based on MVI frame values and MVI file values 
    fhdr.naxis=2
    fhdr.bitpix=8
    fhdr.ccdsum=1
    fhdr.summed=1   ; corrected later
    cam=strlowcase(ahdr.detector)
    datetime=fhdr.date_obs+' '+fhdr.time_obs
    fhdr.date_obs=datetime
    fhdr.date_avg=datetime
    fhdr.time_obs=''
    fhdr.CUNIT1= 'arcsec'
    fhdr.CUNIT2= 'arcsec'
    fhdr.CTYPE1= 'HPLN-TAN'
    fhdr.CTYPE2= 'HPLT-TAN'
    fhdr.CRPIX1=ahdr.XCEN+1
    fhdr.CRPIX2=ahdr.YCEN+1
    fhdr.CROTA =ahdr.ROLL
    rectified=0
    sunxcen=ahdr.XCEN
    sunycen=ahdr.YCEN
    file_sec_pix=0
    IF n_params() GT 1 THEN BEGIN
    	IF fhdr.CDELT1 EQ 0 THEN fhdr.CDELT1=mvi_file_hdr.sec_pix
	rectified=mvi_file_hdr.rectified
	sunxcen=mvi_file_hdr.sunxcen
	sunycen=mvi_file_hdr.sunycen
	file_sec_pix=mvi_file_hdr.sec_pix
    	IF ahdr.XCEN EQ 0 THEN fhdr.CRPIX1 = sunxcen+1
    	IF ahdr.yCEN EQ 0 THEN fhdr.CRPIX2 = sunycen+1
    	fhdr.NAXIS1 = mvi_file_hdr.nx
	fhdr.NAXIS2 = mvi_file_hdr.ny
	fhdr.R1COL  = mvi_file_hdr.ccd_pos[0]
    	fhdr.R2COL  = mvi_file_hdr.ccd_pos[1]
	fhdr.R1ROW  = mvi_file_hdr.ccd_pos[2]
	fhdr.R2ROW  = mvi_file_hdr.ccd_pos[3]
    ENDIF ELSE BEGIN
    	IF keyword_set(XY) THEN IF n_elements(xy) EQ 2 THEN BEGIN
	    fhdr.NAXIS1=xy[0]
	    fhdr.NAXIS2=xy[1]
	ENDIF ELSE message,'IF mvi_file_hdr not passed, XY=[naxis1,naxis2] is required.'
    ENDELSE
    fhdr.CDELT2=fhdr.CDELT1
    
; Special case for (old) SOHO MVI header
    IF (cam eq 'c1' or cam eq 'c2' or cam eq 'c3' or cam eq 'eit') THEN BEGIN
    ; input is LASCO or EIT MVI frame header
    	gotit=0
    	
	cfileq=getenv('QL_IMG')+'/catalogs/daily_combined/'+ $
	    	strmid(ahdr.date_obs,2,2)+strmid(ahdr.date_obs,5,2)+strmid(ahdr.date_obs,8,2)+'_??_05.txt'
    	cfile=file_search( cfileq)
    	if cfile(0) ne '' then begin
    	    openr,catlun,cfile,/get_lun
    	    line=''
    	    repeat begin
    	    	readf,catlun,line
    	    	if strmid(line,2,6) eq strmid(ahdr.filename,2,6) then gotit=1
    	    endrep until (gotit eq 1 or eof(catlun))
    	    close,catlun
    	    free_lun,catlun
    	endif

    	if gotit ne 1  then begin
    	    cfile=file_search(getenv('LZ_IMG')+'/catalogs/daily_combined/'+ $
	    	strmid(ahdr.date_obs,2,2)+strmid(ahdr.date_obs,5,2)+strmid(ahdr.date_obs,8,2)+'_??_05.txt')
    	    if cfile(0) ne '' then begin
    	    	openr,catlun,cfile,/get_lun
    	    	line=''
            	repeat begin
            	    readf,catlun,line
    	    	    if strmid(line,2,6) eq strmid(ahdr.filename,2,6) then gotit=1
            	endrep until (gotit eq 1 or eof(catlun))
            	close,catlun
            	free_lun,catlun
	    ENDIF
    	endif 
    	if gotit ne 1  then begin
    	;print,''
	
    	message,'Original file not found; using default values for '+ahdr.detector,/info
    	IF ahdr.DETECTOR EQ 'EIT' THEN $
    	line='41486953.fts    2008/11/22  00:00:10  EIT    12.9   1024   1024     20      1  Al +1   195A    Normal' $ 
	ELSE IF ahdr.DETECTOR EQ 'C2' THEN $
	line='21303104.fts    2009/09/09  03:30:03   C2    25.1   1024   1024     20      1  Orange  Clear   Normal       0.0000  3389' $
	ELSE IF ahdr.DETECTOR EQ 'C3' THEN $
    	line='32228757.fts    2010/07/12  15:18:15   C3    19.1   1024   1024     20      1  Clear   Clear   Normal       0.0000  3390' $
 	ELSE IF ahdr.DETECTOR EQ 'C1' THEN $
    	line='12012057.fts    1996/08/05  06:59:24   C1    25.1    832    672    115    193  Fe XIV  Clear   Line Sca  5302.7273  1697'
   	print,line

    	ENDIF
  

;0         1         2         3         4         5         6         7         8         9         10        11  
;0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
;41486953.fts    2008/11/22  00:00:10  EIT    12.9   1024   1024     20      1  Al +1   195A    Normal       0.0000  3406


	fhdr.OBSRVTRY  ='SOHO'
	fhdr.TELESCOP  ='SOHO'
	fhdr.INSTRUME  ='LASCO'
	if fhdr.DETECTOR eq 'EIT' then BEGIN
     	    fhdr.WAVELNTH 	=fix(strmid(line,87,4))
    	    fhdr.INSTRUME	='EIT'
	ENDIF
    	;if ((fhdr.r1col+fhdr.NAXIS2)*2 gt 1044 or (fhdr.r1row+fhdr.NAXIS1)*2 gt 1044) then sumvalx=1 else sumval=2
	IF fhdr.CDELT1 GT 0 THEN sumval=fhdr.CDELT1/subtense(ahdr.detector) $
	ELSE BEGIN
	; This is not tested for subfields:
	    sumval=1024./fhdr.naxis1
    	    fhdr.CDELT1=SUBTENSE(ahdr.detector)*sumval
	ENDELSE

    	;fhdr.NAXIS1 =fix(strmid(line,52,4))/sumval
	;fhdr.NAXIS2 =fix(strmid(line,59,4))/sumval
	 fhdr.SUMMED = alog(sumval)/alog(2)
	 fhdr.IPSUM = fhdr.SUMMED

	 fhdr.cdelt2=fhdr.CDELT1
	 ;keyhole=get_crota(datetime)
	 fhdr.crval1=0.
	 fhdr.crval2=0.

    	; we trust MVI sun center more than occulter sun center
	IF level LT 1. THEN BEGIN
	    IF fhdr.crpix1 + fhdr.crpix2 EQ 2 THEN BEGIN
    	    ; only change this if not defined in MVI (file) header
    	    	asunc1 = GET_SUN_CENTER(fhdr,FULL=1024,DOCHECK=gotit, ROLL=sroll,/DEGREES)
    	    	newcen=[asunc1.xcen,asunc1.ycen]
	    ; in this case we have to assume roll-corrected FFV
		message,'Assuming solar north is up.',/info
    		fhdr.crpix1=1+(newcen[0])/sumval
    		fhdr.crpix2=1+(newcen[1])/sumval
		fhdr.rectrota=-sroll	
		fhdr.rectify='T'
	    ENDIF ELSE BEGIN
	    	sroll=0
		newcen=[sunxcen,sunycen]*sumval
    	    	IF fhdr.crota EQ 0 THEN fhdr.crota=sroll-rectified 	; limited to 1 deg resolution
    	    ENDELSE
    	    lir1col=fix(strmid(line,66,4))
    	    lir1row=fix(strmid(line,73,4))
	    IF fhdr.r1col EQ 0 THEN fhdr.r1col=lir1col+newcen[0]-sunxcen*sumval
	    IF fhdr.r1row EQ 0 THEN fhdr.r1row=lir1row+newcen[1]-sunycen*sumval
    	    IF fhdr.r2col EQ 0 THEN fhdr.r2col=(fhdr.r1col-1)+fhdr.NAXIS1*sumval
    	    IF fhdr.r2row EQ 0 THEN fhdr.r2row=(fhdr.r1row-1)+fhdr.NAXIS2*sumval


	    ;    if keyhole eq 180 and newcen[0] GT 1 then begin
            ;	fhdr.crpix1=fhdr.naxis1-fhdr.crpix1
            ;	fhdr.crpix2=fhdr.naxis2-fhdr.crpix2
    	    ;    endif
	    ;--seems to me, if mvi_file_hdr.rectified is defined, crpix is already correct from above - nbr

	    ;IF fhdr.rsun LT 100 THEN fhdr.rsun=GET_SOLAR_RADIUS(fhdr)
	ENDIF
     ; end lasco or eit mvi hdr
     ENDIF  ELSE BEGIN
     
	    ; derive assumed values from filename if not already set. 
	    ; Normally these are set in generic_movie.pro, but let's not assume this.
	    ;
	    name=fhdr.filename
	    namesubs=strsplit(name,'_') ; gives locations of subparts
	    IF name EQ '' THEN message,'WARNING: filename is undefined.',/info
	    dobs=datetime
	    det=''
	    sc=''
	    wvl=0
	;stop
	    IF n_elements(namesubs) GT 2 THEN $
	    IF namesubs[1] EQ 9 THEN BEGIN
    		; assume name is of format 20100101_000921_tbh2B
		; currently works for SECCHI and AIA - nbr, 3/18/11
    		parse_secchi_filename, name, dobs, det, sc, wvl
	    ENDIF 
	    IF det eq '' THEN det=cam
	    ;
	    ; Now generate d
	    ;
	; use this to determine if values have already been populated in frame header or not
	; set values in ahdr and fhdr
	IF file_sec_pix EQ 0 THEN BEGIN
	; need to set all values from filename
	    IF det NE '' THEN BEGIN
		ahdr.detector=det
		ahdr.date_obs=strmid(dobs,0,10)
		ahdr.time_obs=strmid(dobs,11,8)
		fhdr.wavelnth=wvl
		fhdr.date_obs=dobs
		fhdr.detector=det
		cam=strlowcase(det)
	    ENDIF
	    IF strlen(dobs) LT 8 or det EQ '' THEN BEGIN
	    	help,dobs,det
		message,'Insufficient information in input header.',/info
		if (debug_on) then wait,1
		return,fhdr
	    ENDIF
	    message,'Assuming input is square FFV and roll=0. Sun center is estimated.',/info
	    fhdr.crota = 0.
	    fhdr.rectify='T'
	    IF strmid(cam,0,3) EQ 'aia' or strmid(cam,0,3) EQ 'hmi' THEN BEGIN
	    	fhdr.CRPIX1=fhdr.naxis1/2 + 0.5
	    	fhdr.CRPIX2=fhdr.naxis2/2 + 0.5
	    	fhdr.summed=(alog(4096./ fhdr.naxis1)/alog(2))+1
		arcsec=(4096./ fhdr.naxis1)*0.5999 
	    	;fhdr.RSUN=get_solar_radius(dobs,TELESCOPE='aia', DISTANCE=dkm)	; +/- 0.4 arcsec
	    ENDIF


	    IF sc EQ 'A' or sc EQ 'B' THEN  BEGIN
		;(2048./fhdr.naxis1)*
		arcsec=(2048./fhdr.naxis1)*getsccsecpix(det,sc,/fast,silent=~debugon,/arcsec) 
		fhdr.summed=(alog(2048./ fhdr.naxis1)/alog(2))+1
		fhdr.ipsum=fhdr.summed
    		fhdr.OBSRVTRY='STEREO_'+sc
		fhdr.p1col=51
		fhdr.p1row=1
		fhdr.p2col=2098
		fhdr.p2row=2048
		IF fhdr.crpix1 LT 2 THEN BEGIN
	    	    crpx=getscccrpix(fhdr,sc,/updateh,loud=debugon,fast=~debugon)
		    ; If cor1 or cor2 then we need to fill in CRVAL
		    IF cam NE 'euvi' THEN BEGIN
		    	ypr=getsccpointing(fhdr,fast=~debugon,/nospice)
			fhdr.crval1=ypr[0]
			fhdr.crval2=ypr[1]
		    ENDIF
		ENDIF
	    ENDIF
	    
	    ; assuming square FFV
	    help,arcsec
	    
	    fhdr.cdelt1=arcsec
	    fhdr.cdelt2=arcsec
	ENDIF $	; values not already in MVI header
	ELSE BEGIN
	    cam = strlowcase(fhdr.detector)
	ENDELSE

	IF strmid(cam,0,3) EQ 'aia' or strmid(cam,0,3) EQ 'hmi' THEN BEGIN
    	    cdelt0=0.5999 ; arcsec/pixel
	    fhdr.OBSRVTRY='SDO'
	    fhdr.INSTRUME=strupcase(strmid(cam,0,3));'AIA'
	    ;fhdr.DSUN_OBS=dkm*1000.d ; +/- 5e7 m
	ENDIF	; is AIA

	IF strlowcase(cam) EQ 'dalsa' THEN BEGIN
    	    cdelt0=ahdr.cdelt1 ;0.5999 ; arcsec/pixel
	    fhdr.OBSRVTRY='MK4'
	    fhdr.INSTRUME='MK4'
	ENDIF	; is MK4

	isse=0
	ishi=0  
	IF sc EQ 'A' or sc EQ 'B'  THEN  BEGIN
	    fhdr.INSTRUME='SECCHI'
	    isse=1
    	    fhdr.OBSRVTRY='STEREO_'+sc
	    IF fhdr.crpix1 LT 2 THEN BEGIN
	    	;stop
	    ENDIF
    	    IF strmid(cam,0,2) EQ 'hi' THEN BEGIN
		ishi=1
		fhdr.CTYPE1A='RA---AZP'
		fhdr.CTYPE2A='DEC--AZP'
		fhdr.CTYPE1='HPLN-AZP'
		fhdr.CTYPE2='HPLT-AZP'
		cdelt0=getsccsecpix(cam,sc,/fast,silent=~debugon)
		; cdelt0 is in deg
		IF fhdr.cdelt1 GT 1 THEN fhdr.cdelt1=fhdr.cdelt1/3600.
		; if mvi header value is in arcsec convert to degrees
		fhdr.cdelt2=fhdr.cdelt1
    	    	fhdr.CUNIT1= 'deg'
    	    	fhdr.CUNIT2= 'deg'

	    ENDIF $    ; HI mvi
    	    ELSE cdelt0=getsccsecpix(cam,sc,/fast,silent=~debugon,/arcsec)
	ENDIF	; is SECCHI

	; derive summed from cdelt1
	summed=round(alog(fhdr.cdelt1/cdelt0)/alog(2))+1
	fhdr.summed=summed
	fhdr.ipsum=summed
	
	; update values in hdr
	IF isse THEN arcsec=getsccsecpix(fhdr,/fast,silent=~debugon,/updateheader)
	arcsec=fhdr.cdelt1
	IF debug_on THEN help,dobs,cam,sc,wvl,arcsec,summed
	fhdr.rectify='T'
    ENDELSE
    
endif else begin    
; end if mvi frame header
;
; now ahdr is assumed to be a structure from a FITS header that is NOT SECCHI

    IF ~tag_exist(ahdr,'OBSRVTRY') THEN IF tag_exist(ahdr,'TELESCOP') THEN fhdr.OBSRVTRY =ahdr.TELESCOP
    
    IF fhdr.cdelt1 EQ 0 THEN if tag_exist(ahdr,'PLATESCL') THEN BEGIN
    	fhdr.cdelt1=ahdr.PLATESCL
    	fhdr.cdelt2=ahdr.PLATESCL
    ENDIF
    IF fhdr.detector EQ '' THEN fhdr.detector=fhdr.instrume
    
    IF fhdr.OBSRVTRY EQ 'Solar Orbiter' THEN BEGIN
    	fhdr.ccdsum = 1
	fhdr.ipsum = 1
	IF tag_exist(fhdr,'NBIN') THEN fhdr.summed = alog(ahdr.nbin>2)/alog(2)
    ENDIF
    
    IF (strmid(fhdr.INSTRUME,0,3) EQ 'AIA' or strmid(fhdr.INSTRUME,0,3) EQ 'HMI' )and tag_exist(ahdr,'CROTA2') THEN BEGIN
    	fhdr.CUNIT1= 'arcsec'
    	fhdr.CUNIT2= 'arcsec'

     	fhdr.DETECTOR	=strmid(fhdr.INSTRUME,0,3);'AIA'
	fhdr.INSTRUME	=strmid(fhdr.INSTRUME,0,3);'AIA'
	fhdr.OBSRVTRY	='SDO'
	fhdr.TELESCOP	='SDO'
	fhdr.WAVELNTH	=ahdr.WAVELNTH
	fhdr.DATAAVG	=ahdr.DATAMEAN
	fhdr.CROTA  	=ahdr.CROTA2
	fhdr.RSUN   	=ahdr.RSUN_OBS
	fhdr.SUMMED 	=(alog(fhdr.CDELT1/0.6)/alog(2))>1
	fhdr.IPSUM  	=fhdr.SUMMED
	fhdr.ccdsum 	=1.
	fhdr.RECTIFY	=1. ;'T'
	fhdr.DATE_CMD	=fhdr.DATE_OBS
	xy0=sccrorigin(fhdr)

	IF (2*ahdr.CRPIX1-1 NE ahdr.NAXIS1) or (2*ahdr.CRPIX2-1 NE ahdr.NAXIS2) THEN BEGIN
	    message,'WARNING: AIA Unknown subfield detected.',/info 
	    IF fhdr.r1col EQ 0 THEN fhdr.R1COL=xy0[0]
	    IF fhdr.r2col EQ 0 THEN fhdr.R2COL=xy0[0]+fhdr.naxis1*2^(fhdr.summed-1)-1
	    IF fhdr.r1row EQ 0 THEN fhdr.R1ROW=xy0[1]
	    IF fhdr.r2row EQ 0 THEN fhdr.R2ROW=xy0[1]+fhdr.naxis2*2^(fhdr.summed-1)-1
	ENDIF ELSE BEGIN
	    ; EIT default 
	    IF fhdr.r1col EQ 0 THEN fhdr.R1COL=xy0[0]
	    IF fhdr.r2col EQ 0 THEN fhdr.R2COL=xy0[0]+4095
	    IF fhdr.r1row EQ 0 THEN fhdr.R1ROW=xy0[1]
	    IF fhdr.r2row EQ 0 THEN fhdr.R2ROW=xy0[1]+4095
         ENDELSE

    ENDIF ;  AIA
    
    IF fhdr.INSTRUME EQ 'EIT' or fhdr.INSTRUME EQ 'LASCO' THEN BEGIN

    	
	char2=strmid(ahdr.filename,1,1)
    	IF fix(char2) GT 3 or strlen(ahdr.filename) GT 13 THEN level=1

    	fhdr.CUNIT1= 'arcsec'
    	fhdr.CUNIT2= 'arcsec'

	;
	; SECCHI or LASCO unique values
	;
	IF tag_exist(ahdr,'CROTA1') THEN fhdr.CROTA=ahdr.CROTA1
	if fhdr.DETECTOR eq 'EIT' then fhdr.WAVELNTH = fix(ahdr.SECTOR)
	IF tag_exist(ahdr,'LEBXSUM') THEN fhdr.IPSUM  =(ahdr.LEBXSUM+ahdr.LEBYSUM)/2.
    	fhdr.SUMMED =fhdr.IPSUM
	IF tag_exist(ahdr,'SUMCOL') THEN fhdr.SUMMED=fhdr.SUMMED+ahdr.SUMCOL ; 1 + 0 typically
	sumval=2^(fhdr.summed-1)
	
	IF level LT 1 THEN BEGIN
	; here we are assuming existing values are always incorrect
    	    ;fhdr.RSUN=GET_SOLAR_RADIUS(ahdr,DISTANCE=dsunkm)	
	    
    	    sunc = GET_SUN_CENTER(fhdr,FULL=(1024), ROLL=sroll, /DEGREES)
    	    fhdr.crpix1=1+(sunc.xcen+fhdr.r1col-20)/sumval
    	    fhdr.crpix2=1+(sunc.ycen+fhdr.r1row- 1)/sumval
    	    fhdr.crota=sroll
	    ;fhdr.DSUN_OBS=dsunkm*1000.
    	   
    	ENDIF ELSE message,'Not doing corrections.',/info
	
    	IF strlen(fhdr.TIME_OBS) GT 4 THEN BEGIN
	    fhdr.DATE_OBS =strmid(fhdr.DATE_OBS,0,10)+' '+fhdr.TIME_OBS
	    fhdr.TIME_OBS =''
            datetime=fhdr.date_obs
	ENDIF
	
    ENDIF ; EIT or LASCO

    IF tag_exist(ahdr,'r_sun') and ~fhdr.rsun THEN fhdr.rsun=ahdr.r_sun ;Check if RSUN has been set already -AV
    IF tag_exist(ahdr,'p_angle') THEN fhdr.crota = ahdr.p_angle  ; MDI
    IF fhdr.instrume EQ 'MDI' THEN fhdr.rsun=fhdr.rsun*2
    print,fhdr.rsun
    ; BBSO images have cunit in ctype?

    ; MK3/4 has wrong units in cdelt1
    IF (fhdr.OBSRVTRY EQ 'MK4' or fhdr.OBSRVTRY EQ 'MK3') THEN fhdr.cdelt1 = ahdr.cdelt1 * fhdr.rsun
    ; Separate case for Yohkoh/SXT data
    IF fhdr.instrume EQ 'SXT' or fhdr.detector EQ 'SXT' THEN BEGIN
    	fhdr.CUNIT1= 'arcsec'
    	fhdr.CUNIT2= 'arcsec'

                    ;Fill in generic structure
        ;all_hdr(i).detector = 'SXT'
	tel='SXT'
	fhdr.obsrvtry='YOHKOH'
        tt = ahdr.DATE_OBS
                    ;From SXT --> Lasco date format
        IF (STRMID(tt,6,7) LT '90') THEN century = '19' ELSE century = '20'
        fhdr.date_obs = century + STRMID(tt, 6, 7) + STRMID(tt, 2, 4) + STRMID(tt, 0, 2)
        fhdr.filter = ahdr.FILTER_B
        ;sunxcen = hdr.CRPIX1
        ;sunycen = hdr.CRPIX2
        resolut = ahdr.RESOLUT
        CASE resolut OF
            'Full    ': fhdr.cdelt1 = 2.46
            'Half    ': fhdr.cdelt1 = 4.92
            'Qrtr    ': fhdr.cdelt1 = 9.84
        ENDCASE 
	
    ENDIF 
    
    fhdr.cdelt2=fhdr.cdelt1
		    
endelse ; is FITS file header

gamma = fhdr.CROTA*!pi/180
cgamma = cos(gamma)
sgamma = sin(gamma)
fhdr.pc1_1 =  cgamma
fhdr.pc1_2 = -sgamma
fhdr.pc2_1 =  sgamma
fhdr.pc2_2 =  cgamma

;
; Keywords for RA-DEC header
;
IF ishi THEN degfac=1 ELSE degfac=3600
fhdr.CDELT1A=fhdr.cdelt1/degfac
fhdr.CDELT2A=fhdr.cdelt2/degfac
fhdr.CRPIX1A=fhdr.crpix1
fhdr.CRPIX2A=fhdr.crpix2

state=get_stereo_coord(fhdr.date_obs,'SUN',system='gei',/novel)
cspice_reclat, state, radius, ra, dec

fhdr.CRVAL1A=ra* !radeg
fhdr.CRVAL2A=dec* !radeg

; Determine angular offset of GEI system from HPC system ???

IF (0) THEN BEGIN
     dr=!DPI/180.d
     if fhdr.DETECTOR eq 'EIT' then offset=0.	; deg from SOHO roll
     if fhdr.DETECTOR eq 'C2' then offset=0.5 
     if fhdr.DETECTOR eq 'C3' then  offset=0.24
     orbit = get_orbit_fits(fhdr.date_obs,/retain)
     state = [orbit.hec_x, orbit.hec_y, orbit.hec_z]
     cspice_reclat, state, radius, longitude, latitude
     GEI_crota = (-1*cos(longitude-(11.2606*!dtor))*23.45-offset)
     print,GEI_crota
     cgeiroll= cos(GEI_crota*dr)
     sgeiroll= sin(GEI_crota*dr)
    fhdr.pc1_1a =  cgeiroll
    fhdr.pc1_2a = -sgeiroll
    fhdr.pc2_1a =  sgeiroll
    fhdr.pc2_2a =  cgeiroll
ENDIF


IF debugon then begin
    print,fhdr.crpix1,fhdr.crpix2
    wait,1
ENDIF

; ** Implemented conversions:
wcsok=['SOHO','STEREO','MK3','MK4','SDO','YOHKOH']
x=where(wcsok EQ fhdr.obsrvtry,nwcsok)
IF nwcsok GE 1 THEN scc_wcs_keywords,fhdr

return,fhdr

end
