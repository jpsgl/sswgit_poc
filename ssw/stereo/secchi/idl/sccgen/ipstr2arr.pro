function ipstr2arr, inp, SILENT=silent, TRIMOUT=trimout
;+
; $Id: ipstr2arr.pro,v 1.3 2008/11/10 18:14:07 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_sebip
;               
; Purpose   : Translate IP_00_19 from string to intarr
;               
; Explanation:
;               
; Use       : IDL> ipvals=ipstr2arr(hdr.ip_00_19 [,/silent])
;    
; Inputs    : SECCHI header structure -or- IP_00_19 string -or- 
;   	      Row from imagetbl.img starting with X
;               
; Outputs   : int array of IP functions in IP_00_19 
;              
; Optional Output : prints string equivalents of IP functions by default
;
; Keywords: /SILENT   Do not print string equivalents
;   	    /TRIMOUT	Remove trailing zeros from output
;
; Common    :                
;               
; Calls     : scc_read_ip_dat.pro, which reads $PT/IN/OTHER/cnvrt_ip.dat
;               
; Category    : header utility ip image processing
;               
; Prev. Hist. : None.
;
; Written     : N.Rich
;               
; $Log: ipstr2arr.pro,v $
; Revision 1.3  2008/11/10 18:14:07  nathan
; add as possible input Row from imagetbl.img starting with X
;
; Revision 1.2  2008/07/14 18:25:41  nathan
; added /TRIMOUT
;
; Revision 1.1  2007/09/24 18:05:51  nathan
; ip code translation utility
;
;- 

IF(DATATYPE(inp) EQ 'STC') THEN ip = inp.ip_00_19 ELSE ip=inp

IF strmid(ip,0,1) EQ 'X' THEN BEGIN
    ; is row from imagetbl.img
    parts=strsplit(ip,' ',/extract)
    np=hex2decf(strmid(parts[0],1,2))
    seb_ip=intarr(np)
    FOR i=0,np-1 DO seb_ip[i]=hex2decf(parts[i+1])
ENDIF ELSE BEGIN

    ;--Fix length of ip string
    IF strlen(ip) LT 60 THEN ip=' '+ip      ; ip should be 3x20 char long, but
    IF strlen(ip) LT 60 THEN ip=' '+ip      ; could be as short as 58 if trimmed

    seb_ip0 = fix(string(reform(byte(ip),3,20)))
    w=where(seb_ip0,np)
    seb_ip=seb_ip0[w]
ENDELSE 

IF ~keyword_set(SILENT) THEN BEGIN
    s=scc_read_ip_dat()
    ipdat=s.ip_description
    FOR i=0,np-1 do print,string(seb_ip[i],'(i3)'),' ',ipdat[seb_ip[i]]
ENDIF
IF keyword_set(TRIMOUT) THEN return,seb_ip(where(seb_ip)) ELSE $
return,seb_ip
END
