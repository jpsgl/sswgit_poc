pro getsebontimes, files
;+	
;$Id: getsebontimes.pro,v 1.1 2005/06/14 18:54:31 nathan Exp $
;
; NAME:				
; PURPOSE:			
; CATEGORY:			
; CALLING SEQUENCE:		
; INPUTS:			File list of seqprt files to use
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS:	csv file with two columns: start time, cumulative duration (hours)
;  	
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	
; PROCEDURE:
;
; MODIFICATION HISTORY:		
; $Log: getsebontimes.pro,v $
; Revision 1.1  2005/06/14 18:54:31  nathan
; update after SCIP-B TVAC
;
;
;-
;

nfiles=n_elements(files)
sttimes=dblarr(nfiles)
entimes=dblarr(nfiles)

for i=0,nfiles-1 do begin
	pos=strpos(files[i],'200')
        ststr=strmid(files[i],pos,15)
        sttimes[i]=utc2tai(yymmdd2utc(ststr))
	entimes[i]=file_date_mod(files[i])        
endfor
order=sort(sttimes)

sttimes=sttimes[order]
entimes=entimes[order]

outpf='sebontimes'+utc2yymmdd(tai2utc(sttimes[0]))+'-'+utc2yymmdd(tai2utc(entimes[nfiles-1]))+'.csv'
print,'Starting ',outpf
wait, 3
;stop
openw,1,outpf
printf,1,'Session end time   , Tot Hrs'
;	  2005/03/09 17:36:13,     1.9
tothrs=0
for i=0,nfiles-1 do begin
	
        spawn,'head -4 '+files[i],outp
        IF (outp[0] NE '') THEN BEGIN
                parts=str_sep(outp[3],'|')
                sttime=parts[0]
                strput,sttime,'T',6
                sttime='20'+sttime

                spawn,'tail -6 '+files[i],outp
                ok=where(strmid(outp,0,2) EQ '05')
                parts=str_sep(outp[ok[0]],'|')
                entime=parts[0]
                strput,entime,'T',6
                entime='20'+entime

                hrs=(utc2tai(str2utc(entime)) - utc2tai(str2utc(sttime)))/3600
	        tothrs=tothrs+hrs
	        printf,1,utc2str(tai2utc(entimes[i]),/ecs,/trunc)+','+string(tothrs,format='(F8.1)')
	ENDIF
endfor

close,1

END
