pro parsemechlogs, type, START_DATE=start_date
;+	
;$Id: parsemechlogs.pro,v 1.4 2006/04/07 16:04:38 nathan Exp $
;
; NAME:				
; PURPOSE: generates a summary file of nmoves for mechanism type
; CATEGORY:			
; CALLING SEQUENCE:		
; INPUTS:			string: cor1shtr, cor2shtr, cor1pol, cor2pol, euvifw, euviqs, euvishtr
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		
;   START = 'yyyy-mm-dd'    ; time to start from 
;
; OUTPUTS: writes output file in .
;  	
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	must be run FROM $scca or $sccb
; PROCEDURE:
;
; MODIFICATION HISTORY:		
; $Log: parsemechlogs.pro,v $
; Revision 1.4  2006/04/07 16:04:38  nathan
; update input path
;
; Revision 1.3  2005/10/17 19:05:03  nathan
; fixed 050802
;
; Revision 1.2  2005/06/14 18:54:32  nathan
; update after SCIP-B TVAC
;
; Revision 1.1  2005/04/21 19:25:13  nathan
; pros for generating statistics
;
;-
;

spawn,"grep 'total moves' ln*/outputs/logs/*fun/"+type+"*eval", /sh, lines
print,lines
wait,5
;lines=readlist(fname,nlines)
nlines=n_elements(lines)
nmoves=fltarr(nlines)
times=strarr(nlines)

for i=0,nlines-1 do begin
    parts=str_sep(lines[i],'=')
    nmoves[i]=float(parts[1])
    pos=strpos(lines[i],'200')
    utc=yymmdd2utc(strmid(lines[i],pos,15))
    times[i]=utc2str(utc,/ecs,/trunc)
  
endfor
order=sort(times)

timesr=times[order]
nmovesr=nmoves[order]
linesr=lines[order]
;for i=1,nlines-1 do nmovesr[i]=nmovesr[i-1]+nmovesr[i]
IF keyword_set(START_DATE) THEN BEGIN
    startdt=utc2str(anytim2utc(start_date),/ecs,/trunc)
    good=where(timesr GT startdt)
    subst=good[0]
ENDIF ELSE subst=0

outpf='mechs/nmoves'+type+utc2yymmdd(str2utc(timesr[subst]))+'-'+utc2yymmdd(str2utc(timesr[nlines-1]))+'.csv'
print,'Starting ',outpf
parts=str_sep(linesr[nlines-2],'=')
parts1=str_sep(parts[0],':')
print,parts1[1]
wait, 3
openw,1,outpf
printf,1,parts1[1]
printf,1,'Session start time ,  RunTot,  Nmoves'
;	  2005/03/09 17:36:13,     1.9
totmoves=0
for i=subst,nlines-1 do begin
    totmoves=totmoves+nmoves[order[i]]
    printf,1,timesr[i]+','+string(totmoves,format='(F8.1)')+','+string(nmovesr[i],format='(F8.1)')
endfor
close,1

END
