;+
;$Id: scc_read_ip_dat.pro,v 1.2 2007/09/24 18:05:25 nathan Exp $
; Project     : STEREO - SECCHI 
;                
; Name        : READ_IP_DAT
;
; Purpose     : Returns an array of structures containing defined
;	        image processing id codes and descriptions.
;
; Use         : result = READ_IP_DAT () 
;
; Inputs      : 
;
; Opt. Inputs : None
;               
; Outputs     : result   Array of structures containing:
;                     result.ip_num           int: compression type 
;                     result.ip_char          string: 1 character compression code
;                     result.ip_description   string: String describing compression step
;                     result.ip_oht           floar: overhead (sec) to execute a function
;                     result.ip_ext           float: execution time (sec) for a function (for
;                                             full size 2048x2048 images).
;
; Opt. Outputs: None
;
; Keywords    : IPver	    set Flight software IP version for images v46 = V4.6
;
; RESTRICTIONS:
;	The file cnvrt_ip.dat must exist in '../IN/OTHER/' directory.
;
; Prev. Hist. : Adapted from SOHO/LASCO planning tool.
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 09/01/04 Used local table definintion.
;              Ed Esfandiari 01/24/05 Added ip_oht and ip_ext times (for each function) now included  
;                                     in cnvrt_ip.dat. Also use str_sep to brakeup each input str
;                                     separated by commas and ignore blank lines and comments (lines
;                                     starting with ;) in cnvrt_ip.dat.
;
; $Log: scc_read_ip_dat.pro,v $
; Revision 1.2  2007/09/24 18:05:25  nathan
; modified output to always have 256 elements
;
; Revision 1.1  2007/04/12 19:31:31  nathan
; renamed from PT/PRO/read_ip_dat.pro
;
; Revision 1.10  2006/09/20 17:54:01  nathan
; re-do IPVER keyword
;
; Revision 1.8  2005/12/21 19:34:45  mcnutt
; added version keyword for V4.6
;
; Revision 1.7  2005/03/10 16:49:03  esfand
; changes since Jan24-05 to Mar10-05
;
; Revision 1.6  2005/01/24 17:56:35  esfand
; checkin changes since SCIP_A_TVAC
;
; Revision 1.2  2004/09/01 19:10:42  esfand
; Removed init_db and get_db struct pros.
;
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-

FUNCTION scc_READ_IP_DAT, IPver=IPver

   a= {ip_types,DB_NAME:'secchi',TABLE_NAM:'img_ip_types',IP_NUM:0,IP_CHAR:'',IP_DESCRIPTION:'ERROR',$
       ip_oht:0.0, ip_ext:0.0}

   filename= 'cnvrt_ip.dat'  ; Aug 14 02, AEE - copied over from NRL_LIB and modified
   if(keyword_set(IPver))then filename='cnvrt_ip_'+IPver+'.dat'
   filename= GETENV('PT')+'/IN/OTHER/'+filename
   OPENR, lu, filename, /GET_LUN
   str = ''
   first = 1
   IP_ARR=replicate(a,256)
   WHILE NOT(EOF(lu)) DO BEGIN
     READF, lu, str
     IF (STRMID(STRTRIM(str,2),0,1) NE ';' AND STRLEN(STRTRIM(str,2)) NE 0) THEN BEGIN
       toks= STRTRIM(STR_SEP(str,','),2)
       indx=FIX(toks[0])
       ip_arr[indx].ip_num = indx 
       ip_arr[indx].ip_char = toks(1) 
       ip_arr[indx].ip_description = toks(2) 
       ip_arr[indx].ip_oht= FLOAT(toks(3))
       ip_arr[indx].ip_ext= FLOAT(toks(4))
     ENDIF
   ENDWHILE

   CLOSE, lu
   FREE_LUN, lu

   RETURN, ip_arr

END
