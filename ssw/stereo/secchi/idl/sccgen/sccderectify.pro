function sccderectify, xy, h
;+
; NAME:			secchiderectify
; PURPOSE:
;	function convert rectified image coordinates to unrectified CCD coordinates
; 
; CATEGORY: convert coordinate ccd astrometry
; CALLING SEQUENCE:	
;
; INPUTS:   	xy  INTARR  x,y pair of FITS image coordinates (IMAGE_X,Y)
;   	    	h   STC  SECCHI header structure 
;
; KEYWORDs:		
;
; OUTPUTS:	INTARR x,y pair of CCD coordinates (PCOL,PROW)	
;
; OPTIONAL OUTPUT PARAMETERS:	
; COMMON BLOCKS:    
; SIDE EFFECTS:		
; RESTRICTIONS:
; PROCEDURE:	Applies R1COL,R1ROW to input and does reverse of secchi_rectify.pro
;
; Written     : N.Rich, NRL/Interferometrics, 2/2009
;               
; Modified    :
; $Log: sccderectify.pro,v $
; Revision 1.2  2009/02/13 18:30:56  nathan
; made corrections
;
; Revision 1.1  2009/02/13 17:19:47  nathan
; EUVI only until someone finishes it
;

rx=h.r1col-1 + xy[0]
ry=h.r1row-1 + xy[1]

IF (h.OBSRVTRY eq 'STEREO_A') then begin
    case h.detector of
    	'EUVI': begin
    	    	px = 2176 - ry + 1
    	    	py = 2176 - rx + 1
		end
	ELSE: 	begin
	    	message,'Invalid detector for SECCHI.',/info
		return,-1
	    	end	
    endcase
ENDIF ELSE BEGIN
    case h.detector of
    	'EUVI': begin
    	    	px = 2176 - ry + 1
    	    	py = rx
		end
	ELSE: 	begin
	    	message,'Invalid detector for SECCHI.',/info
		return,-1
	    	end	
    endcase
ENDELSE

return,[px,py]

end
