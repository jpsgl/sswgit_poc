pro parse_secchi_filename, fn, dobs, det, sc, wvl, cfn
;
; Project     : SECCHI 
;                   
; Name        : parse_secchi_filename
;               
; Purpose     : get expanded info from secchi filename or AIA filename downloaded by scc_get_sdo_browse.pro
;
; INPUTS :  fn	STRING	SECCHI filename of format YYYYMMDD_HHMMSS_XXXTTS.ext
;
; Optional Input :  
;
; OUTPUTS:  dobs    STRING  date and time from filename
;   	    det     STRING  3 or 4-char SECCHI telescope
;   	    sc	    STRING  A or B
;   	    wvl     FIX     Wavelength of EUVI or AIA (Angstroms)
;   	    cfn     STRING  corrected filename (changes wavelet file names to secchi format) else its the same as input fn
;
; Use         : IDL> parse_secchi_filename,'20100101_000921_tbh2B.png',dobs,det,sc
;
; Calls       : 
;
; Comments    : 
;               
; Side effects: 
;               
; Category    : 
;               
; Written     : N.Rich NRL/I2 Aug 2010
;               
; $Log: parse_secchi_filename.pro,v $
; Revision 1.11  2015/03/17 15:45:20  hutting
; correct wvl string for sdo images
;
; Revision 1.10  2015/03/16 20:26:40  hutting
; Added a new sdo naming convention
;
; Revision 1.9  2013/07/05 18:13:12  mcnutt
; added hmi matches aia
;
; Revision 1.8  2012/08/14 17:58:43  mcnutt
; added check for lasco running difference file names
;
; Revision 1.7  2012/07/10 16:04:17  mcnutt
; now works for SPW daily browse euvi jpgs
;
; Revision 1.6  2011/11/03 17:49:26  mcnutt
; added new Lasco secchi suffix
;
; Revision 1.5  2011/08/04 16:37:34  mcnutt
; will now work for euvi wavelets new output for coorected wavelet filename
;
; Revision 1.4  2011/04/29 19:27:31  nathan
; works for input with or without suffix; works for $SECCHI_PNG/soho (LASCO) frames
;
; Revision 1.3  2011/03/31 15:42:02  nathan
; add AIA and define wvl
;
; Revision 1.2  2011/03/22 20:15:15  mcnutt
; replaces seconds with 00 if equal to ss (movie routine issue)
;
; Revision 1.1  2010/11/10 15:42:26  nathan
; called from generic_movie.pro
;

if strpos(fn,'ss') ge 13 then strput,fn,'01',13
dobs=utc2str(yymmdd2utc(strmid(fn,0,15)),/ecs)

; check if input has suffix or not
dotp=strpos(fn,'.')
IF dotp GT 0 THEN sfp=strlen(fn)-dotp ELSE sfp=0
;check for wavelet filename
if strpos(fn,'eu_R') gt -1 or strpos(fn,'eu_L') gt -1 then wavelet=1 else wavelet=0 
;check for SPW daily browse filename
if strpos(fn,'A_195') gt -1 or strpos(fn,'B_195') gt -1 then spwb=1 else spwb=0 
;LASCO daily pretties suffix
IF rstrmid(fn,4+sfp,1) EQ 'l'  or strpos(fn,'Lasc') gt 0 or strpos(fn,'_rdlc') gt 0 or strpos(fn,'_rc2c3') gt 0 or strpos(fn,'_Lc2c3') gt 0 THEN BEGIN
    sc='SOHO'
    det=strupcase(rstrmid(fn,0+sfp,2))
ENDIF ELSE BEGIN
    IF ~wavelet THEN sc = rstrmid(fn,0+sfp,1)
    IF spwb THEN sc = rstrmid(fn,4+sfp,1)
    if strpos(fn,'eu_R') gt -1 then sc='A'
    if strpos(fn,'eu_L') gt -1 then sc='B'
    IF ~wavelet and ~spwb THEN tt = rstrmid(fn,1+sfp,2) else tt='eu'
    case tt of
	'h1': det='HI1'
	'h2': det='HI2'
	'c1': det='COR1'
	'c2': det='COR2'
	'eu': det='EUVI'
	ELSE: det=''
    endcase
ENDELSE
IF ~wavelet THEN ww=rstrmid(fn,3+sfp,2) else ww=rstrmid(fn,5+sfp,2)
IF spwb THEN ww='19'
wvl=0
IF det EQ 'EUVI' THEN CASE ww of
    '19': wvl=195
    '30': wvl=304
    '28': wvl=284
    '17': wvl=171
    ELSE:
ENDCASE
IF sc EQ 'i' THEN BEGIN
    if strpos(fn,'HMI') lt 0 then begin
      det='AIA'
      wvl=fix(rstrmid(fn,1+sfp,4))
    endif else det='HMI'
    sc='SDO'
ENDIF
IF strpos(fn,'sdo_') gt 0 THEN BEGIN
    if strpos(fn,'sdo_a') gt 0 then begin
      det='AIA'
       if rstrmid(fn,0,3) eq 'jpg' then wvl=fix(rstrmid(fn,4,3)) else wvl=fix(rstrmid(fn,0,3))
    endif else det='HMI'
    sc='SDO'
ENDIF

cfn=fn
IF wavelet THEN cfn= strmid(fn,0,15)+'_'+ww+tt+sc

end
