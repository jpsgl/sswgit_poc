pro tuneallmechs_results,evalfile
;+	
;$Id: tuneallmechs_results.pro,v 1.1 2006/10/04 17:03:36 mcnutt Exp $
;
; NAME:				
; PURPOSE: create mech tunning eval files from fits headers			
; CATEGORY: SECCHI calibrations			
; CALLING SEQUENCE:		
; INPUTS:   	ITOS tune mech eval file name
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS: SCC_LOG/*tunemechs*.csv		
;  	
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	
; PROCEDURE:
;
; MODIFICATION HISTORY:		
; $Log: tuneallmechs_results.pro,v $
; Revision 1.1  2006/10/04 17:03:36  mcnutt
; logs delay setting and mech positions
;
;

enums = def_scc_enums()

tmp=strpos(evalfile,'_')
tday=strmid(evalfile,tmp-8,8)
sc=strmid(evalfile,tmp-9,1)
openr,2,evalfile
tel=['euvi','cor1','cor2']
imnum=intarr(4,13)
poldel=intarr(3,13)
shtrdel=intarr(3,13)
runtime=strarr(2)
line='' & d=-1 & dt=-1
while (not EOF(2)) do begin
  readf,2,line
  if (strpos(line,'DELAY SETTINGS') gt -1) then begin
    d=d+1
    tmp=strsplit(line,':')
    POLdel(0,d)=strmid(line,tmp(1)+1,3) 
    POLdel(1,d)=strmid(line,tmp(2)+1,3) 
    POLdel(2,d)=strmid(line,tmp(3)+1,3)
    SHTRdel(0,d)=strmid(line,tmp(4)+1,3) 
    SHTRdel(1,d)=strmid(line,tmp(5)+1,3) 
    SHTRdel(2,d)=strmid(line,tmp(6)+1,3)
  endif
  if (strpos(line,'STARTING IMAGE') gt -1) then begin
    tmp=strsplit(line,':')
    IMnum(0,d)=strmid(line,tmp(1)+1,4) 
    IMnum(1,d)=strmid(line,tmp(2)+1,4) 
    IMnum(2,d)=strmid(line,tmp(3)+1,4) 
    IMnum(3,d)=strmid(line,tmp(4)+1,strlen(line)-tmp(4)+1) 
  endif
  if (strpos(line,'Onb time:') gt -1) then begin
    dt=dt+1
    tmp=strsplit(line,':')
    runtime(dt)=strmid(line,tmp(5)-3,2)+strmid(line,tmp(5),2)+strmid(line,tmp(6),2)
  endif


  print,line
endwhile
close,2

logdir=getenv_slash('SCC_LOG')
hdrdir=getenv_slash('SECCHI')

if(sc eq 'A')then qsoff=2 else qsoff=1
sectors=[304,195,284,175] & cmdqs=[2,8,14,20]
filters=['S1','DBL','S2','OPEN']

for nt=0,n_Elements(tel)-1 do begin
  ftsfiles=findfile(hdrdir+'*/'+tel(nt)+'/'+string(tday,'(I8.8)')+'/*.fts')
  tmp=strmid(ftsfiles,strpos(ftsfiles(0),'.fts')-12,6)
  zf=where((tmp ge runtime(0)) and (tmp le runtime(1)))
  ftsfiles=ftsfiles(zf)

;stop
  exppf=strarr(n_Elements(ftsfiles))
  qspf=strarr(n_Elements(ftsfiles))
  polpf=strarr(n_Elements(ftsfiles))
  hdrlog=logdir+sc+"_"+tel(nt)+'_tunemechs_'+tday+'.csv'
  openw,1,hdrlog,width=160

  if(sc eq 'A' and tel(nt) eq 'cor1')then poloffset= 10. else poloffset=0.

  if(tel(nt) eq 'euvi')then printf,1,'IMGnum,QS delay,CMD QS,Actual QS,p/f,SHTR delay,CMD expo,actual expo,p/f' else $
    	    	printf,1,'IMGnum,POL delay,CMD POL,Actual POL,p/f,SHTR delay,CMD expo,actual expo,p/f'


  exppf=strarr(n_Elements(ftsfiles))
  qspf=strarr(n_Elements(ftsfiles))
  polpf=strarr(n_Elements(ftsfiles))

  for n=0,n_Elements(ftsfiles)-1 do begin
     tmp=sccreadfits(ftsfiles(n),headers,/nodata)
    z=where(imnum(nt,*) lt headers(0).timgctr) & pdel=poldel(nt,z(n_elements(z)-1)) & sdel=shtrdel(nt,z(n_elements(z)-1))
    if(headers(0).expcmd ge headers(0).exptime-(headers(0).expcmd*.05) and headers(0).expcmd le headers(0).exptime+(headers(0).expcmd*.05)) then exppf(n)='PASS' else exppf(n)='FAIL'
    if(tel(nt) eq 'EUVI')then begin
      if(cmdqs(where(sectors eq (headers(0).sector))) eq headers(0).encoderq-qsoff)then qspf(n)='PASS' else qspf(n)='FAIL'
      printf,1,headers(0).TIMGCTR,',',pdel,',',headers(0).wavelnth,',',headers(0).encoderq,',',qspf(n),',',$
				sdel,',',headers(0).expcmd,',',headers(0).exptime,',',exppf(n)
    endif else begin
      cmdpol=headers(0).polar & recpol=(headers(0).encoderp*2.5)-poloffset
      if(cmdpol eq 0.0 and recpol gt 350)then cmdpol=360.
      if(cmdpol ge recpol-2.5 and cmdpol le recpol+2.5)then polpf(n)='PASS' else polpf(n)='FAIL'       
      printf,1,headers(0).TIMGCTR,',',pdel,',',headers(0).polar,',',headers(0).encoderp,',',polpf(n),',',$
				sdel,',',headers(0).expcmd,',',headers(0).exptime,',',exppf(n)   
    endelse
  endfor
  printf,1,'Total fits files= ',n_elements(ftsfiles)
  if(tel(nt) eq 'euvi')then begin
    z=where(QSPF eq 'FAIL')
    if(z(0) ne -1)then printf,1,'Total QS fails= ',n_elements(where(QSPF eq 'FAIL')) else printf,1,'Total QS fails=   0'
  endif else begin
    z=where(POLPF eq 'FAIL')
    if(z(0) ne -1)then printf,1,'Total POL fails= ',n_elements(where(POLPF eq 'FAIL')) else printf,1,'Total POL fails=   0'
  endelse
  z=where(EXPPF eq 'FAIL')
  if(z(0) ne -1)then printf,1,'Total exposure fails= ',n_elements(where(EXPPF eq 'FAIL')) else printf,1,'Total exposure fails=   0'
close,1				

endfor				

end				
