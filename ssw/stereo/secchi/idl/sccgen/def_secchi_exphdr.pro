FUNCTION DEF_SECCHI_expHDR, secchi_hdr_tags
;+	
;$Id: def_secchi_exphdr.pro,v 1.7 2011/09/06 19:34:02 nathan Exp $
;
; NAME:				DEF_SECCHI_EXPHDR
; PURPOSE:			Define a Header Structure for a Level 0.5 SECCHI Image
; CATEGORY:			Administration, Pipeline
; CALLING SEQUENCE:		structure=DEF_SECCHI_HDR(hdr_tags)
; INPUTS:			None
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS:		a structure array containing an initialized header
;  secchi_hdr_tags	Returns strarr of tags in structure
;
; OPTIONAL OUTPUT PARAMETERS:
;  hdr_tags	STRARR of tags in header
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	
;	Must be called before DEF_SECCHI_HDR is called, defines the same structure.	 
; PROCEDURE:
;
; MODIFICATION HISTORY:		
;  2004.04.14, nbr - create from def_lasco_hdr.pro
;
; $Log: def_secchi_exphdr.pro,v $
; Revision 1.7  2011/09/06 19:34:02  nathan
; rename structure so does not conflict with def_secchi_hdr (?)
;
; Revision 1.6  2005/06/16 17:20:51  nathan
; add doorstat,ceb_stat,cam_stat
;
; Revision 1.5  2005/05/20 22:35:56  secchib
; nr - add stat_ceb and stat_cam
;
; Revision 1.4  2005/05/20 17:59:12  nathan
; misc changes
;
; Revision 1.3  2005/04/21 19:23:58  nathan
; update comments
;
; Revision 1.2  2005/03/02 16:10:51  nathan
; add NMISSING
;
; Revision 1.1  2005/02/28 23:01:04  nathan
; from def_secchi_hdr.pro
;
; Revision 1.4  2005/02/02 16:10:25  nathan
; moved to new directory
;
; Revision 1.4  2005/01/24 23:45:51  nathan
; remove encoderh keyword
;
; Revision 1.3  2004/09/20 21:07:13  nathan
; fixed SIMPLE, LEDPULSE
;
; Revision 1.2  2004/09/12 03:38:43  nathan
; added fsw keywords
;
;
;-
;
   version=strmid('$Id: def_secchi_exphdr.pro,v 1.7 2011/09/06 19:34:02 nathan Exp $',5,25)
   int = 0
   lon = 0L
   flt = 0d
   ; *** Those keywords where zero is a valid value are given initial values
   ;     of -1.
   ;
   str = ''
   sta = REPLICATE(str, 20)
   secchi_hdr={secchi_exphdr_struct,	$
	;SIMPLE	:'T',	$
;
; ++++ Most-used keywords ++++
;
	BITPIX	:int,	$	;Number of bits per pixel (multiple of 8)
	NAXIS	:int,	$	;Number of axes in array
	NAXIS1	:int,	$	;Number of pixels in fastest ranging axis
	NAXIS2	:int,	$	;Number of pixels in second fastest ranging axis
    
        DATE_OBS:str,	$	;UTC Date and time of observation
	FILEORIG:str,$	    ; name of original downlinked science file
	SEB_PROG:str,$	    ; type of image
        EXPCMD	:-1.,	$	;Exposure duration sent (sec)
        EXPTIME	:flt,	$	;Duration of shutter exposure (sec)
        P1COL	:int,	$	;Position of 1st column of image on CCD
        P2COL	:int,	$	;Position of last column of image on CCD
        P1ROW	:int,	$	;Position of 1st row of image on CCD
        P2ROW	:int,	$	;Position of last row of image on CCD
        LED	:str,	$	;LED or laser:'N','B','P','R' ?
	LEDPULSE:lon,$	    ; number of led pulses
        OFFSET	:int,	$	;Offset value used in camera
        BIAS    :flt,   $	; mean of column 15 of readout
        BIASSDEV:flt,	$	; standard deviation of column 15 of readout
        GAIN	:int,	$	;Gain value used in camera
        GAINMODE:str,$	    ;FPGA Mode (1=LOW or 0=HIGH)
        SUMMED	:flt,	$	;Summing factor
        CEB_T   :int,   $
        TEMP_CCD:flt,   $
        POLAR	:-1.,	$	;Polarization angle (degrees) of polarizer, if any
                                ;from commanded position
	ENCODERP:-1,$	    ; actual encoder value of polarizer 
        ;ENCODERH:-1,	$	;FIlter wheel or Polarizer encoder value
        SECTOR	:str,	$	;Wavelength of commanded Quadrant Selector position
        ENCODERQ:-1,	$	;Quadrant selector encoder value
        FILTER	:str,	$	;EUVI Filter Wheel commanded position
	ENCODERF:-1,$	    ; actual encoder value of filter wheel
	FPS_ON	:str,$	    ; T/F: from actualFPSmode
        OBS_PROG:str,	$	;Calling procedure or purpose of image
        ;DOORCLOS:str,	$	;Door is closed, T or F
        DOORSTAT:int,	$	; door encoder value from enum eDoorStatus in cImageHdr.h
        SHUTTDIR:str,	$	;CCW, CW, or NONE means no shutter used
        READ_TBL:-1,	$	;[0-7] which table in WGA_FILE used for readout
	DATE_CLR:str,	$	; Date/time of start of clear table
        DATE_RO :str,	$	; Date/time of start of image retrieval
        READTIME:flt,	$	;Duration of readout (sec)
    	IP_TIME :int,$	    ; duraction of onboard processing time (sec)
	COMPRSSN:int,$	    ; code indicating algorithm used for compression
	COMPFACT:flt,$	    ; compression factor achieved
        NMISSING:-1,$		; number of blocks missing
	READFILE:str,$	    ; name of readout table file used by fsw
	SETUPTBL:str,$
	EXPOSTBL:str,$
	MASK_TBL:str,$
	IP_TBL:str,$
	COMMENT :sta,	$	; Comment
	HISTORY :sta,   $	; Comments of processing history
;
; ++++ Less-used keywords ++++
;
	TEMPAFT1:flt,   $
	TEMPAFT2:flt,   $
	TEMPMID1:flt,   $
	TEMPMID2:flt,   $
	TEMPFWD1:flt,   $
	TEMPFWD2:flt,   $
	TEMP_CEB:flt,   $
        ORIGIN	:str,	$	;Where image taken/processed
        DETECTOR:str,	$	;Which telescope
        CAMERA	:str,	$	;Identifier of camera electronics
        IMGCTR	:int,	$	;Number of images since swire/ops mode started
	    	    	    	; -or- added by planning tool: 0-9999
	OBJECT	:str,	$	;What is supposed to be in image
        FILENAME:str,	$	;Name of FITS file
        CLR_TBL	:-1,	$	;[0-7] which table in WGA_FILE used for clear
    	DATE:str,$  	    ; time FITS header was created
    	INSTRUME:'SECCHI',$
	OBSRVTRY:str,$
    	WAVEFILE:str,$	    ; name of waveform table file used by fsw
	CCDSUM	:flt,$	    ; (sumrow + sumcol) / 2.0
	IPSUM	:flt,$	    ; (sebxsum + sebysum) / 2.0
	DATE_CMD:str,$	    ; originally scheduled observation time
	DATE_MID:str,$	    ; date of midpoint of the exposure(s) (UTC standard)
	OBT_TIME:flt,$	    ; value of STEREO on-board-time since epoch ???
	APID	:int,$	    ; application identifier / how downlinked
	OBS_ID	:int,$	    ; observing sequence ID from planniing tool
	OBSSETID:int,$	    ; observing set id from planning tool
	IP_PROG0:int,$	    ; description of onboard image processing sequence used
	IP_PROG1:int,$	    ; description of onboard image processing sequence used
	IP_PROG2:int,$	    ; description of onboard image processing sequence used
	IP_PROG3:int,$	    ; description of onboard image processing sequence used
	IP_PROG4:int,$	    ; description of onboard image processing sequence used
	IP_PROG5:int,$	    ; description of onboard image processing sequence used
	IP_PROG6:int,$	    ; description of onboard image processing sequence used
	IP_PROG7:int,$	    ; description of onboard image processing sequence used
	IP_PROG8:int,$	    ; description of onboard image processing sequence used
	IP_PROG9:int,$	    ; description of onboard image processing sequence used
	IP_10_19:str,$		;string representation of values 10 - 19 in ip.Cmds
	IMGSEQ	:int,$	    ; number of image in current sequence (usually 1)
 	OBSERVER:str,	$	;Name of operator
        STG1POS	:flt,	$	;Position of StimTel stage 1
        STG2POS	:flt,	$	;Postion of StimTel stage 2
;
; ++++ Keywords not yet in use ++++
;
        VERSION	:str,	$	;Identifier of version of psessioninit.proc
        CEB_STAT:int,	$	; CEB-Link-status (enum CAMERA_INTERFACE_STATUS)
        CAM_STAT:int,	$	; CCD-Interface-status (enum CAMERA_PROGRAM_STATE)
;	    	    	    	; -or- seb hdr version
	BUNIT:str,$ 	    ; unit of values in array
	BSCALE:flt,$	    ; scale factor for FITS
	BZERO:lon,$ 	    ; value corresponding to zero in array for FITS
	BLANK:int,$ 	    ; value in array which means no data
	SCSTATUS:int,$	    ; spacecraft status message before exposure
	SCANT_ON:str,$	    ; T/F: derived from s/c status before and after
	FPS_CMD :str,$	    ; T/F: from useFPS
	SCFP_ON :str,$	    ; T/F: from actualSCFinePointMode
; ++ WCS - related ++
	SYNC	:str,$	    ; whether image is synchronized with other S/C
	CRPIX1	:flt,$	
        CRPIX2	:flt,$
        PC1_1	:flt,$
        PC1_2	:flt,$
        PC2_1	:flt,$
        PC2_2	:flt,$
        CRVAL1	:flt,$
        CRVAL2	:flt,$
        CTYPE1	:'HPLN-TAN',$
        CTYPE2	:'HPLT-TAN',$
        CUNIT1	:'ARCSEC',$
        CUNIT2	:'ARCSEC',$
        CDELT1	:flt,$
        CDELT2	:flt,$
        XCEN	:flt,$
        YCEN	:flt,$
        SPICEFIL:str,$
        HCIX_OBS:lon,$
        HCIY_OBS:lon,$
        HCIZ_OBS:lon,$
        LONPOLE	:180 $
;
; ++++ Preflight only ++++
;
;        EXPCLR	:flt,	$	;Time from start clear to start readout (sec)
;        CLEARTIM:flt,	$	;Duration of clear (sec)
;        BOARD	:int,	$	; cam 1, 2, or 3
;        CCD_ID	:str,	$	;identifier of CCD or FPA
;        SWIRE	:str,	$	;Version info of do_swire.cpp
;        WGA_FILE:str,	$	;Version info or name of WGA file, which lists waveforms and tables
;        TDSPC	:str,	$	;PC used to control camera/mechs
;    	READSTIM:flt,	$   	; duration of readout from do_swire.cpp
;        KEITHAVG:flt,	$	;Average current (Amps) during exposure from Keithley electrometer
;        KEITH_N :int,	$	;Number of values used to compute KEITHAVG
;        KEITH_HI:flt,	$	;High value in KEITH_N readings
;        KEITH_LO:flt,	$	;Low value in KEITH_N readings
;        TEMP1	:flt,	$	;Deg C from Lakeshore 330A
;        TEMP2	:flt,	$	;Deg C from Lakeshore 330B
;        TEMP3	:flt,	$	;Deg C from Lakeshore 211
;        LAMP	:str,	$	; = LED
;        MODE	:-1	$	; = GAINMODE

   }


   secchi_hdr_nt = N_TAGS(secchi_hdr)
   secchi_hdr_tags = TAG_NAMES(secchi_hdr)

  ; IF (N_PARAMS() EQ 1) THEN BEGIN	;** fill in the structure with the contents of the FITS header
 ;     FOR t=0, lasco_hdr_nt -1 DO BEGIN	;** for each tag in the lasco hdr structure
 ;        var = FXPAR(fits_hdr, lasco_hdr_tags(t))
 ;        lasco_hdr.(t) = var
 ;     ENDFOR
 ;  ENDIF

   RETURN, secchi_hdr

END
