FUNCTION wcs_coord_system, index, system
;+
;$Id: wcs_coord_system.pro,v 1.1 2008/01/08 17:31:21 colaninn Exp $
;
; Project     :	STEREO
;
; Name        :	WCS_COORD_SYSTEM
;
; Purpose     :	Extract WCS data from FITS header and converts
;               to input coordinate system
;
; Category    :	FITS, Coordinates, WCS
;
; Explanation : This procdure calls FITSHEAD2WCS to convert the fits
;               header to WCS. Then the WCS values are changed to the 
;               input coordinate system.
;
; Syntax      :	WCS = WCS_COORD_SYSTEM( HEADER )
;
; Inputs      :	HEADER = Either a FITS header, or an index structure from
;                        FITSHEAD2STRUCT.
;               SYSTEM = Currently only works for :
;                        'CARRINGTON' or 'CR'
;                        'HAE'
;                        'HERTN'
;
; Opt. Inputs :	None.
;
; Outputs     :	WCS = Structure containing World Coordinate System
;          
; Calls       : FITSHEAD2WCS, CONVERT_STEREO_LONLAT, HI_HAE_POINT
;
; Written     : Robin C Colaninno NRL/GMU Jan 2008
;               robin.colaninno@nrl.navy.mil
; $Log: wcs_coord_system.pro,v $
; Revision 1.1  2008/01/08 17:31:21  colaninn
; very limited application
;		
;-

;--Create wcs header
wcs = fitshead2wcs(index)

system = strupcase(system)
IF system EQ 'CR' THEN system ='CARRINGTON'

;--Convert CRVAL from HPC to input system
dr = !dpi/180.d0 ;degrees to radians

IF wcs.cunit[0] EQ 'arcsec' THEN BEGIN
  c=(wcs.crval/3600.0)*dr 
  wcs.cdelt = wcs.cdelt/3600.
  wcs.cunit =['deg','deg']
ENDIF ELSE c=wcs.crval*dr 

convert_stereo_lonlat,index.date_avg,c,'HPC',system,space=index.obsrvtry

;--Convert Roll Angle
IF strmid(index.detector,0,2) NE 'HI' THEN BEGIN
  ;--Get roll angle for SCIP
  delta_roll = get_stereo_roll(index.date_obs,index.obsrvtry,system=system)-$ 
  get_stereo_roll(index.date_obs,index.obsrvtry ,system='RTN')
  roll =  delta_roll-wcs.roll_angle   
ENDIF ELSE roll = hi_hae_point(index)

;--Add values for input system
CASE system OF 
  'CARRINGTON': BEGIN
     wcs.crval[0] = index.crln_obs              ;x-coordinate
     wcs.crval[1] = index.crlt_obs              ;y-coordinate
     wcs.coord_type = 'Carrington-Heliographic' ;coordinate type
     wcs.wcsname =  'Carrington-Heliographic'   ;coordinate type
     wcs.projection = 'AZP'                     ;projection type
     wcs.ctype = ['HGLN-AZP','HGLT-AZP']
     wcs.cdelt = wcs.cdelt*(index.dsun_obs/6.96e8-1) ;degress on the sun
   END
  'HAE': BEGIN
     wcs.crval[0] = c[0]/dr                 ;x-coordinate
     wcs.crval[1] = c[1]/dr                 ;y-coordinate
     wcs.coord_type =  'Celestial-Ecliptic' ;coordinate type
     wcs.wcsname = 'Celestial-Ecliptic'     ;coordinate type
     wcs.cdelt[0] = -wcs.cdelt[0]           ;handedness 
     wcs.roll_angle = roll                  ;roll angle
     wcs.pc[0,0] = cos(roll*dr)
     wcs.pc[0,1] = sin(roll*dr)
     wcs.pc[1,0] = -sin(roll*dr)
     wcs.pc[1,1] = cos(roll*dr)
   END
  'HERTN': BEGIN
     wcs.crval[0] = c[0]/dr                 ;x-coordinate
     wcs.crval[1] = c[1]/dr                 ;y-coordinate
     wcs.coord_type =  'Celestial-Ecliptic' ;coordinate type
     wcs.roll_angle = roll                  ;roll angle
     wcs.pc[0,0] = cos(roll*dr)
     wcs.pc[0,1] = sin(roll*dr)
     wcs.pc[1,0] = -sin(roll*dr)
     wcs.pc[1,1] = cos(roll*dr)
   END
ENDCASE

RETURN, wcs
END
