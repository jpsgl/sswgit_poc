function scc_update_history,hdr0,str,value=value

;+
; $Id: scc_update_history.pro,v 1.3 2018/12/26 16:19:04 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_update_history
;               
; Purpose   : This function place history comments in the structure
;               
; Explanation: The history value for SECCHI structure is a 20 string
;              array. If there is no room in the string array then the
;              header is return unchanged. 
;              
;               
; Use       : IDL> hdr = scc_update_history(hdr,str,value=flt)
;    
; Inputs    : hdr - image header,FITS or SECCHI structure
;             str - string of text to be added to the header
;             
;
; Outputs   : hdr - updated header
;
; Keywords  : value - a non string varible to be added to the history
;
; Calls     : SCC_IMG_STATS
;
; Category    : Administration, Calibration
;
; Written     : Robin C Colaninno NRL/GMU Jan 2007
;
; $Log: scc_update_history.pro,v $
; Revision 1.3  2018/12/26 16:19:04  mcnutt
; use where instead of strmatch to work with gdl
;
; Revision 1.2  2011/12/21 23:08:01  thompson
; Removed wait -- it tremendously slows down processing!!!
;
; Revision 1.1  2007/08/08 20:10:02  colaninn
; moved from prep
;
; Revision 1.5  2007/08/07 17:55:21  colaninn
; corrected handling of full history
;
; Revision 1.4  2007/05/17 19:18:19  nathan
; fix Bug 172
;
; Revision 1.3  2007/03/13 17:37:47  nathan
; wait after message
;
; Revision 1.2  2007/02/06 21:46:32  colaninn
; changed varible name
;
; Revision 1.1  2007/01/31 18:23:49  colaninn
; moved code out of program
;
;-



hdr = hdr0

;--Check Header-------------------------------------------------------
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF tag_exist(hdr,'HISTORY') THEN BEGIN
    zb=where(hdr.history eq '',zblank)
    h_dex=n_elements(hdr[0].history)-zblank
;    h_dex=n_elements(hdr[0].history)-total(strmatch(hdr.HISTORY,''),1)
ENDIF ELSE BEGIN
    message, 'NO HISTORY ARRAY IN HEADER STRUCTURE; hdr unchanged',/inform
    RETURN,hdr
ENDELSE
  
;--Check if history is full
IF h_dex GE 20 THEN BEGIN 
  message, 'HEADER STRUCTURE HISTORY ARRAY FULL; hdr unchanged',/inform
;;  wait,2
  RETURN,hdr
ENDIF

;--Change value type
IF keyword_set(value) THEN str_value=strtrim(string(value),2) ELSE str_value=''

hdr.history[h_dex]=str + ' '+str_value

RETURN,hdr
END
