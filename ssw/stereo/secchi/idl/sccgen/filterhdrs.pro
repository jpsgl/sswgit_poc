function filterhdrs, hdrs
;+	
;$Id: filterhdrs.pro,v 1.1 2005/04/21 19:25:12 nathan Exp $
;
; NAME:				
; PURPOSE:			
; CATEGORY:			
; CALLING SEQUENCE:		
; INPUTS:	hdrs: array of header structures
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS:	lonarr: subscripts of hdrs	
;  	
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	
; PROCEDURE:
;
; MODIFICATION HISTORY:		
; $Log: filterhdrs.pro,v $
; Revision 1.1  2005/04/21 19:25:12  nathan
; pros for generating statistics
;
;-
;
nh=n_elements(hdrs)
gd=[-1]
chk1=0
chk2=0
chk3=0
chk4=0
chk5=0
chk6=0
chk7=0
chk8=0
chk9=0
for i=1,nh-1 do begin

    pe0=hdrs[i-1].encoderp
    pe1=hdrs[i].polar/2.5
    IF (hdrs[i].detector EQ 'COR2') THEN BEGIN
        chk1=chk1+1
      IF (hdrs[i].date_obs GT '2004-12-22T00:00:00') THEN BEGIN
          chk2=chk2+1
        IF (hdrs[i].seb_prog EQ 'NORMAL' or hdrs[i].seb_prog EQ 'SEQ') THEN BEGIN
            chk3=chk3+1
          IF (pe1 EQ 0 and pe0 NE 143 and pe0 NE 0 and pe0 NE 1) THEN BEGIN
            chk4=chk4+1
            gd=[gd,i]
          ENDIF ELSE $
          IF (pe1 EQ 143 and pe0 NE 142 and pe0 NE 143 and pe0 NE 0) THEN BEGIN
            chk5=chk5+1
            gd=[gd,i]
          ENDIF ELSE $
          IF (pe0 LT pe1-1 or pe0 GT pe1+1) THEN BEGIN
            chk6=chk6+1
            gd=[gd,i]
          ENDIF
        ENDIF
      ENDIF
    ENDIF
    
endfor

help, chk1, chk2, chk3, chk4, chk5, chk6, chk7, chk8, chk9
ngd=n_elements(gd)
IF ngd GT 1 THEN return, gd[1:ngd-1] ELSE return,-1
END
