function get_fitscomments,str_hdr,struct_hdr
;+
; $Id: get_fitscomments.pro,v 1.1 2007/08/08 20:10:17 colaninn Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_fitscomments.pro
;               
; Purpose   : This function creates an array of the FITS string header
;             comments. (/ comments not the COMMENT keyword) Suitable for
;             use in struct2fitshead
;               
; Use       : IDL> comments = get_fitscomments(string_header)
;    
; Inputs    : hdr - FITS string header
;
; Outputs   : hdr - string array of comments
;
; Calls     : 
;
; Category    : Administration, Calibration
;
; Written     : Robin C Colaninno NRL/GMU  Feb 2007
;
; $Log: get_fitscomments.pro,v $
; Revision 1.1  2007/08/08 20:10:17  colaninn
; moved from prep
;
; Revision 1.3  2007/06/28 20:45:30  colaninn
; added on_error,2
;
; Revision 1.2  2007/02/13 15:11:40  colaninn
; now uses input structure
;
; Revision 1.1  2007/02/06 19:11:21  colaninn
; added to secchi_prep
;
;-
ON_ERROR,2

IF datatype(str_hdr) NE 'STR' THEN BEGIN
  message,'Need FITS header as first input'
END  

IF datatype(struct_hdr) NE 'STC' THEN BEGIN
  message,'Need header structure as second input'
END  

tags = tag_names(struct_hdr)
numtag = n_elements(tags)
comments = strarr(numtag)

n = n_elements(str_hdr)

FOR i=0, n-1 DO BEGIN
  keyword = strtrim(strmid(str_hdr[i],0,8),2)    
 
  ;--Fix any "DATE_*" keywords to read "DATE-*".
  IF STRMID(keyword,0,5) EQ 'DATE-' THEN strput, keyword, '_', 4

  IF (keyword EQ 'COMMENT') OR (keyword EQ 'HISTORY') THEN BEGIN
    i=i+1   
  ENDIF ELSE BEGIN
    cdex = where(keyword EQ tags,cnt)  
    
    IF cnt GT 0 THEN BEGIN
      IF strmid(str_hdr[i],31,1) EQ '/' THEN BEGIN
        comments[cdex] = strmid(str_hdr[i],32,strlen(str_hdr[i])-1)
      ENDIF ELSE BEGIN
        str = strsplit(str_hdr[i],"'",count=np,/extract)
        str = str[np-1]
        str = strsplit(str,'/',count=ns,/extract)
        comments[cdex] = str[ns-1]   
      ENDELSE
    ENDIF
  ENDELSE
ENDFOR

RETURN,comments
END  



