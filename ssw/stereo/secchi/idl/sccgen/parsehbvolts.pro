pro parsehbvolts,filein,time0,time1, times, sebbus, sebp15, sebm15, $
            seb15mtr, seb5v, seb3_3v, pib5v, sebhkgnd, scip, hi

;+
; $Id: parsehbvolts.pro,v 1.3 2005/06/14 18:54:32 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : parsehbvolts
;               
; Purpose   : convert list from seqprt file for HBVOLTS to 1-D arrays for plotting
;               
; Explanation: input and output in ECS (hh:mm:ss or hhmmss) format
;               
; Use       : IDL> 
;    
; Inputs    : filein    STRING name of hbvolts*txt seqprt file

; Optional Inputs: time0, time1 STRING  start and end time of output
;       must be in same format as in input file
;               
; Outputs   : generate plots

; Optional Outputs: times, hbvstc: array with 9 mnmemonci structure
;
; Keywords  : 

; Calls from LASCO : 
;
; Common    : sccplot
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database hk utility plotting
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Mar 05
;               
; $Log: parsehbvolts.pro,v $
; Revision 1.3  2005/06/14 18:54:32  nathan
; update after SCIP-B TVAC
;
; Revision 1.2  2005/05/20 17:59:12  nathan
; misc changes
;
; Revision 1.1  2005/04/21 19:25:13  nathan
; pros for generating statistics
;
;-            
common sccplot, currentstmpl, voltagestmpl, aorb

IF datatype(voltagestmpl) NE 'STC' THEN restore,getenv_slash('SCC_DATA')+'voltagetmpl.sav'

nfiles=n_elements(filein)
help,filein
wait,5
print,'Reading '+filein[0],'...'
voltages=read_ascii(filein[0],template=voltagestmpl)
print,'Done'

good=where(strmid(voltages.time,0,2) NE 'HB' and (strpos(voltages.time,'ime') LT 0),nnewpg)

    times=voltages.time[good]
    sebp15=voltages.sebp15[good]
    sebbus=voltages.sebbus[good]
    sebm15=voltages.sebm15[good]
    seb15mtr=voltages.seb15mtr[good]
    seb5v=voltages.seb5v[good]
    seb3_3v=voltages.seb3_3v[good]
    pib5v=voltages.pib5v[good]
    sebhkgnd=voltages.sebhkgnd[good]
    scip=voltages.scip[good]
    hi=voltages.hi[good]

ii=1
IF nfiles GT 1 THEN REPEAT BEGIN
    print,'Reading '+filein[ii],'...'
    voltages=read_ascii(filein[ii],template=voltagestmpl)
    print,'Done'
    good=where(strmid(voltages.time,0,2) NE 'HB' and (strpos(voltages.time,'ime') LT 0),nnewpg)

    times=      [times,voltages.time[good]]
    sebp15=     [sebp15,voltages.sebp15[good]]
    sebbus=     [sebbus,voltages.sebbus[good]]
    sebm15=     [sebm15,voltages.sebm15[good]]
    seb15mtr=   [seb15mtr,voltages.seb15mtr[good]]
    seb5v=      [seb5v,voltages.seb5v[good]]
    seb3_3v=    [seb3_3v,voltages.seb3_3v[good]]
    pib5v=      [pib5v,voltages.pib5v[good]]
    sebhkgnd=   [sebhkgnd,voltages.sebhkgnd[good]]
    scip=       [scip,voltages.scip[good]]
    hi=         [hi,voltages.hi[good]]

    ii=ii+1
ENDREP UNTIL ii GE nfiles

!p.background=255
wait,1
window,0
!p.color=1
!x.style=1
!y.style=3

print,'converting dates to utc...'
strput,times,'T',6
times='20'+times
utimes=str2utc(times)
pos=strpos(filein[0],'200')
utc0=yymmdd2utc(strmid(filein[0],pos,15))

; check if times in file are UT or Local; assume Local is EDT
IF (abs(utimes[0].time - utc0.time) LT 1800000) THEN BEGIN
    print,'Adding 04:00:00 to data times...'
    utimes=addutctimes(utimes,'04:00:00')
ENDIF ELSE $
    IF  (filein[0] EQ 'solar5/outputs/prints/scipBtvac/hbvolts20050605_161334.txt') OR $
        (filein[0] EQ 'solar5/outputs/prints/scipBtvac/hbvolts20050605_180407.txt') $
    THEN BEGIN         
        print,'Adding 19:50:00 to data times...'
        utimes=addutctimes(utimes,'19:50:00')
    ENDIF
times=utc2str(utimes,/trunc)

n=n_elements(times)
IF n_params() GT 1 THEN BEGIN
    goodtimes=where(times GE time0 and times LE time1,ngt)
    IF ngt LT 10 THEN BEGIN
        print,'Not enough good times in '+filein+'. Waiting 10 before returning...'
        help,goodtimes
        wait, 10
        return
    ENDIF
    starttoolate=addecstimes(time0,'00:15:00',/ccsds)
    IF (times[0] GT starttoolate) or (goodtimes[ngt-1] GT n-3) THEN BEGIN
        print,'Input file times donot match input times. Waiting 10 before continuing...'
        help,goodtimes,times,time0,time1
        print,filein+' duration: ',times[0],' to ',times[n-1]
        wait, 10
    ENDIF
ENDIF

times   =times[goodtimes]
utimes  =utimes[goodtimes]
    sebp15=sebp15[goodtimes]
    sebbus=sebbus[goodtimes]
    sebm15=sebm15[goodtimes]
    seb15mtr=seb15mtr[goodtimes]
    seb5v=seb5v[goodtimes]
    seb3_3v=seb3_3v[goodtimes]
    pib5v=pib5v[goodtimes]
    sebhkgnd=sebhkgnd[goodtimes]
    scip=scip[goodtimes]
    hi=hi[goodtimes]

; plot result

cd,'$stereo/itos/testing/tlmanalysis',current=lastdir

;break_file, filein, dlg, dirpath,fnbase0,filext
fnbase=utc2yymmdd(utimes[ngt-1],/hhmmss,/yyyy)
daydir=aorb+'/'+utc2yymmdd(utimes[0],/yyyy)+'/'
spawn,'mkdir '+daydir
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB BUS Voltage for Functional Test (UT)'
utplot,utimes,sebbus, title=ptitle,psym=1,yticklen=1.
outplot,utimes,sebbus

outp=tvrd()
write_png,daydir+'vsebbus'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB+15 Voltage for Functional Test (UT)'
utplot,utimes,sebp15, title=ptitle,psym=1,yticklen=1.
outplot,utimes,sebp15

outp=tvrd()
write_png,daydir+'vsebp15v'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB-15 Voltage for Functional Test (UT)'
utplot,utimes,sebm15, title=ptitle,psym=1,yticklen=1.
outplot,utimes,sebm15

outp=tvrd()
write_png,daydir+'vsebm15v'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB+15 Door Motor Voltage for Functional Test (UT)'
utplot,utimes,seb15mtr, title=ptitle,psym=1,yticklen=1.
outplot,utimes,seb15mtr

outp=tvrd()
write_png,daydir+'vdoormtr'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB+5 Voltage for Functional Test (UT)'
utplot,utimes,seb5v, title=ptitle,psym=1,yticklen=1.
outplot,utimes,seb5v

outp=tvrd()
write_png,daydir+'vseb5v'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB+3.3 Voltage for Functional Test (UT)'
utplot,utimes,seb3_3v, title=ptitle,psym=1,yticklen=1.
outplot,utimes,seb3_3v

outp=tvrd()
write_png,daydir+'vseb3_3v'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI PIB+5 Voltage for Functional Test (UT)'
utplot,utimes,pib5v, title=ptitle,psym=1,yticklen=1.
outplot,utimes,pib5v

outp=tvrd()
write_png,daydir+'vpib'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB HK GND Voltage for Functional Test (UT)'
utplot,utimes,sebhkgnd, title=ptitle,psym=1,yticklen=1.
outplot,utimes,sebhkgnd

outp=tvrd()
write_png,daydir+'vhkgnd'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SCIP Bus Voltage for Functional Test (UT)'
utplot,utimes,scip, title=ptitle,psym=1,yticklen=1.
outplot,utimes,scip

outp=tvrd()
write_png,daydir+'vscipbus'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI HI Bus Voltage for Functional Test (UT)'
utplot,utimes,hi, title=ptitle,psym=1,yticklen=1.
outplot,utimes,hi

outp=tvrd()
write_png,daydir+'vhibus'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

cd,lastdir


end
