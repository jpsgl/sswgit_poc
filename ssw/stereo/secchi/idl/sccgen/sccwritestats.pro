pro sccwritestats, alun, timestr, ar1, ar2, ar3, ar4, ar5
;+
; $Id: sccwritestats.pro,v 1.2 2005/05/20 17:59:12 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : sccwritestats
;               
; Purpose   : compute, format, and output mean and stdev of input
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : alun  INT     logical unit number of file open for writing
;       timestr     STRING  time; first column of output
;       ar1...ar5   FLTARR  arrays of numbers to compute statistics for
;
; Optional Inputs: 
;               
; Outputs   : prints to file and to screen

; Optional Outputs: 
;
; Keywords  : 

; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database hk utility reports statistics
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Mar 05
;               
; $Log: sccwritestats.pro,v $
; Revision 1.2  2005/05/20 17:59:12  nathan
; misc changes
;
; Revision 1.1  2005/04/21 19:25:13  nathan
; pros for generating statistics
;
;-            
    line =  timestr +				' |' $
            + string(  avg(ar1),format='(F8.3)') +	' |' $
            + string(stdev(ar1),format='(F8.3)') +	' |' $
            + string(  avg(ar2),format='(F8.3)') +	' |' $
            + string(stdev(ar2),format='(F8.3)') +	' |' $
            + string(  avg(ar3),format='(F8.3)') +	' |' $
            + string(stdev(ar3),format='(F8.3)') +	' |' $
            + string(  avg(ar4),format='(F8.3)') +	' |' $
            + string(stdev(ar4),format='(F8.3)') +	' |' $
            + string(  avg(ar5),format='(F8.3)') +	' |' $
            + string(stdev(ar5),format='(F8.3)')
    print,line
    printf,alun,line

end
