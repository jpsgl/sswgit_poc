pro parsecurrents,filein,time0,time1, times, sebbus, meb, scipbus, hibus, doormtr

;+
; $Id: parsecurrents.pro,v 1.3 2006/04/14 18:21:24 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : parsecurrents
;               
; Purpose   : convert seqprt file for CURRENTS to 1-D arrays and plot
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : filein    STRING name of currents*txt seqprt file

; Optional Inputs: time0, time1 STRING  start and end time (UT) of output, CCSDS /trunc format
;       
;               
; Outputs   : generate plots (scale is UT)

; Optional Outputs: times (UT), sebbus, mebbus, scipbus, hibus, doormtr
;
; Keywords  : 

; Calls from LASCO : 
;
; Common    : sccplot
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database hk utility plotting
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Mar 05
;               
; $Log: parsecurrents.pro,v $
; Revision 1.3  2006/04/14 18:21:24  nathan
; update sccplot common block
;
; Revision 1.2  2005/06/14 18:54:31  nathan
; update after SCIP-B TVAC
;
; Revision 1.1  2005/04/21 19:25:12  nathan
; pros for generating statistics
;
;-            

common sccplot, currentstmpl, voltagestmpl, aorb, xfscitemplate

IF datatype(currentstmpl) NE 'STC' THEN restore,getenv_slash('SCC_DATA')+'currentstmpl.sav'

nfiles=n_elements(filein)
help,filein
wait,2
print,'Reading '+filein[0],'...'
currents=read_ascii(filein[0],template=currentstmpl)
print,'Done'

good=where(currents.time NE 'Currents' and (strpos(currents.time,'ime') LT 0),nnewpg)

times=currents.time[good]
meb  =currents.meb[good]
sebbus=currents.sebbus[good]
scipbus=currents.scipbus[good]
doormtr=currents.doormtr[good]
hibus=currents.hibus[good]

i=1
IF nfiles GT 1 THEN REPEAT BEGIN
    print,'Reading '+filein[i],'...'
    currents=read_ascii(filein[i],template=currentstmpl)
    print,'Done'
    good=where(currents.time NE 'Currents' and (strpos(currents.time,'ime') LT 0),nnewpg)

    times=[times,currents.time[good]]
    meb  =[meb,currents.meb[good]]
    sebbus=[sebbus,currents.sebbus[good]]
    scipbus=[scipbus,currents.scipbus[good]]
    doormtr=[doormtr,currents.doormtr[good]]
    hibus=[hibus,currents.hibus[good]]

    i=i+1
ENDREP UNTIL i GE nfiles

!p.background=255
wait,1
window,0
!p.color=1
!x.style=1
!y.style=3
print,'waiting 10 sec...'
wait,10
print,'converting dates to utc...'
; convert times from file to correct format    
;times = strmid(times,0,15)
strput,times,'T',6
times='20'+times
utimes=str2utc(times)

pos=strpos(filein[0],'200')
utc0=yymmdd2utc(strmid(filein[0],pos,15))

; check if times in file are UT or Local; assume Local is EDT
IF (abs(utimes[0].time - utc0.time) LT 1800000) THEN BEGIN
    print,'Adding 04:00:00 to data times...'
    utimes=addutctimes(utimes,'04:00:00')
ENDIF

times=utc2str(utimes,/trunc)

n=n_elements(times)
IF n_params() GT 1 THEN BEGIN
    goodtimes=where(times GE time0 and times LE time1,ngt)
    IF ngt LT 10 THEN BEGIN
        print,'Not enough good times in '+filein+'. Waiting 10 before returning...'
        help,goodtimes
        wait, 10
        return
    ENDIF
    starttoolate=addecstimes(time0,'00:15:00',/ccsds)
    IF (times[0] GT starttoolate) or (goodtimes[ngt-1] GT n-3) THEN BEGIN
        print,'Input file times donot match input times. Waiting 10 before continuing...'
        help,goodtimes,times,time0,time1
        print,filein+' duration: ',times[0],' to ',times[n-1]
        wait, 10
    ENDIF
ENDIF
        
times   =times[goodtimes]
utimes  =utimes[goodtimes]
meb     =meb[goodtimes]
sebbus  =sebbus[goodtimes]
scipbus =scipbus[goodtimes]
doormtr =doormtr[goodtimes]
hibus   =hibus[goodtimes]

; plot result

cd,'$stereo/itos/testing/tlmanalysis',current=lastdir

;break_file, filein, dlg, dirpath,fnbase0,filext
fnbase=utc2yymmdd(utimes[ngt-1],/hhmmss,/yyyy)
daydir=aorb+'/'+utc2yymmdd(utimes[0],/yyyy)+'/'
spawn,'mkdir '+daydir
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SEB BUS Current for Functional Test (UT)'
utplot,utimes,sebbus, title=ptitle,psym=1,yticklen=1.
outplot,utimes,sebbus

outp=tvrd()
write_png,daydir+'isebbus'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI MEB Current for Functional Test (UT)'
utplot,utimes,meb, title=ptitle,psym=1,yticklen=1.
outplot,utimes,meb

outp=tvrd()
write_png,daydir+'imeb'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SCIP BUS Current for Functional Test (UT)'
utplot,utimes,scipbus, title=ptitle,psym=1,yticklen=1.
outplot,utimes,scipbus

outp=tvrd()
write_png,daydir+'iscipbus'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI HI BUS Current for Functional Test (UT)'
utplot,utimes,hibus, title=ptitle,psym=1,yticklen=1.
outplot,utimes,hibus

outp=tvrd()
write_png,daydir+'ihibus'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

ptitle='SECCHI SCIP Door Motor Current for Functional Test (UT)'
utplot,utimes,doormtr, title=ptitle,psym=1,yticklen=1.
outplot,utimes,doormtr

outp=tvrd()
write_png,daydir+'idoormtr'+fnbase+'.png',outp

print,'Waiting 10 before continuing ...'
wait,10
;++++++++++++++++++++++++++++++++++++++++++++++++++++

cd,lastdir
end
