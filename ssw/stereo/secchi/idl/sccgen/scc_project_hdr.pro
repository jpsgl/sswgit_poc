function scc_project_hdr, refimg, time, roll, phdr, system=system
;+
; $Id: scc_project_hdr.pro,v 1.3 2011/03/10 19:23:13 mcnutt Exp $
;
; NAME:
;	scc_project_hdr
;
; PURPOSE:
;	This procedure produces a projected header with time and roll input to create wcs header in the future.
;
; CATEGORY:
;
; CALLING SEQUENCE:
;	pwcs=scc_project_hdr(refimg, '2011-07-01T12:00:00.00', roll, phdr)
;
; INPUTS:
;	refimg:	name of reference fits file 
;
; KEYWORD PARAMETERS:
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
; $Log: scc_project_hdr.pro,v $
; Revision 1.3  2011/03/10 19:23:13  mcnutt
; added system keyword
;
; Revision 1.2  2011/03/10 14:31:09  mcnutt
; set crota to roll
;
; Revision 1.1  2011/03/10 14:23:39  mcnutt
; to create a project wcs hdr and adjust roll
;
;-
;

image=sccreadfits(refimg,fhdr,/nodata)

phdr=fhdr
tdiff=(anytim2tai(time)-anytim2tai(fhdr.date_obs))
phdr.date_avg=utc2str(tai2utc(anytim2tai(fhdr.date_avg)+tdiff))
phdr.date_end=utc2str(tai2utc(anytim2tai(fhdr.date_end)+tdiff))
phdr.date_obs=utc2str(tai2utc(anytim2tai(fhdr.date_obs)+tdiff))
phdr.crota=double(roll)
gamma = ROLL*!pi/180
cgamma = cos(gamma)
sgamma = sin(gamma)
phdr.pc1_1 =  cgamma
phdr.pc1_2 = -sgamma
phdr.pc2_1 =  sgamma
phdr.pc2_2 =  cgamma

;
; Keywords for RA-DEC header
;
phdr.CDELT1A=phdr.cdelt1/3600
phdr.CDELT2A=phdr.cdelt2/3600
phdr.CRPIX1A=phdr.crpix1
phdr.CRPIX2A=phdr.crpix2

state=get_stereo_coord(phdr.date_obs,'SUN',system='gei',/novel)
cspice_reclat, state, radius, ra, dec

phdr.CRVAL1A=ra* !radeg
phdr.CRVAL2A=dec* !radeg

pwcs=  fitshead2wcs(phdr, system=system)

return,pwcs

end

