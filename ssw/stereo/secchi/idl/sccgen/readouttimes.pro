pro readouttimes, hdrs

read2048=where(hdrs.p2col eq 2097 and hdrs.p2row eq 2050 and hdrs.ccdsum eq 1)
read1088=where(hdrs.p2col eq 2176 and hdrs.p2row eq 2050 and hdrs.ccdsum eq 2)
read1024=where(hdrs.p2col eq 2097 and hdrs.p2row eq 2050 and hdrs.ccdsum eq 2)
readctr =where(hdrs.p2col eq 1329 and hdrs.p2row eq 1331 and hdrs.ccdsum eq 1)
read2176=where(hdrs.p2col eq 2176 and hdrs.p2row eq 2114 and hdrs.ccdsum eq 1)
readequ=where(hdrs.p2col gt 2000 and hdrs.p1row eq 820 and hdrs.ccdsum eq 1)

help,read2048,read1088,read1024, readctr, read2176,  readequ

n=n_elements(hdrs)
gd=where(hdrs[read2048].readtime gt 0 and hdrs[read2048].readtime lt 100)
read2048=read2048[gd]
gd=where(hdrs[read1088].readtime gt 0 and hdrs[read1088].readtime lt 100)
read1088=read1088[gd]
gd=where(hdrs[read1024].readtime gt 0 and hdrs[read1024].readtime lt 100)
read1024=read1024[gd]
gd=where(hdrs[readctr].readtime gt 0 and hdrs[readctr].readtime lt 100)
readctr=readctr[gd]
gd=where(hdrs[read2176].readtime gt 0 and hdrs[read2176].readtime lt 100)
read2176=read2176[gd]
gd=where(hdrs[readequ].readtime gt 0 and hdrs[readequ].readtime lt 100)
readequ=readequ[gd]

srtdt=sort(hdrs.date_obs)
lastdate=strmid(hdrs[srtdt[n-1]].date_obs,0,10)

strng='readouttimes'+lastdate+'.txt'
openw,1,strng
print,' '
print,strng,':'

strng='RO Table          Avg             Min             Max          StdDev              N'
;      read2048       4.8797652       4.8585649       4.8804479    0.0020241568         224
print,strng
printf,1,strng
strng='======================================================================================'
print,strng
printf,1,strng

readtimes=hdrs[read2048].readtime
strng = 'read2048' + $
        string(avg(readtimes)) + $
        string(min(readtimes)) + $
        string(max(readtimes)) + $
        string(stdev(readtimes))+$
        string(n_elements(readtimes))
print,strng
printf,1,strng

readtimes=hdrs[read1088].readtime
strng = 'read1088' + $
        string(avg(readtimes)) + $
        string(min(readtimes)) + $
        string(max(readtimes)) + $
        string(stdev(readtimes))+$
        string(n_elements(readtimes))
print,strng
printf,1,strng

readtimes=hdrs[read1024].readtime
strng = 'read1024' + $
        string(avg(readtimes)) + $
        string(min(readtimes)) + $
        string(max(readtimes)) + $
        string(stdev(readtimes))+$
        string(n_elements(readtimes))
print,strng
printf,1,strng
readtimes=hdrs[readctr].readtime
strng = 'readctr' + $
        string(avg(readtimes)) + $
        string(min(readtimes)) + $
        string(max(readtimes)) + $
        string(stdev(readtimes))+$
        string(n_elements(readtimes))
print,strng
printf,1,strng
readtimes=hdrs[read2176].readtime
strng = 'read2176' + $
        string(avg(readtimes)) + $
        string(min(readtimes)) + $
        string(max(readtimes)) + $
        string(stdev(readtimes))+$
        string(n_elements(readtimes))
print,strng
printf,1,strng
readtimes=hdrs[readequ].readtime
strng = 'readequ' + $
        string(avg(readtimes)) + $
        string(min(readtimes)) + $
        string(max(readtimes)) + $
        string(stdev(readtimes))+$
        string(n_elements(readtimes))
print,strng
printf,1,strng

close,1

end
        
        
