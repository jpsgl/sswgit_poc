function match_coord_system, image,wcs_image0,wcs_base0,fits=fits

;+
;$Id: match_coord_system.pro,v 1.1 2008/01/08 17:33:17 colaninn Exp $
;
; Project     :	STEREO
;
; Name        :	MATCH_COORD_SYSTEM
;
; Purpose     :	Extract WCS data from FITS header and converts
;               to input coordinate system
;
; Category    :	interpolation, backgrounds, stars, WCS
;
; Explanation : This procdure uses WCS_GET_COORD and WCS_GET_PIXEL to
;               warps an image to the coordinates of the base wcs struc.
;
; Syntax      :	WCS = MATCH_COORD_SYSTEM( IMAGE, IMAGE_WCS, BASE_WCS) 
;    
; Inputs      :	IMAGE = input image to be warped
;               IMAGE_WCS = WCS structure of the image to be warped   
;               BASE_WCS = WCS structure of the base or target coordinates
;               *WCS sturctures must be of the same coordinate system
;
; Keyword     : Converts the inputs from FITS headers to WCS
;               structures of the input coordinates system.
;               FITS = Currently only works for :
;                        'HPC'
;                        'GEI'
;                        'CARRINGTON' or 'CR' (source regions)
;                        'HAE'  (star field)                 
;                        'HERTN' (F-corona)
;
; Opt. Inputs :	None.
;
; Outputs     :	WARP_IMAGE = warped input image
;          
; Calls       : FITSHEAD2WCS, WCS_COORD_SYSTEM, WCS_GET_COORD, WCS_GET_PIXEL
;
; History     : expansion of sccshiftstarfield.pro

; Written     : Robin C Colaninno NRL/GMU Jan 2008
;               robin.colaninno@nrl.navy.mil
; $Log: match_coord_system.pro,v $
; Revision 1.1  2008/01/08 17:33:17  colaninn
; very limited application
;		
;-

; ---- extract wcs structure from fits header
IF keyword_set(fits) THEN BEGIN
  system = strupcase(fits)
  CASE system OF
    'HPC': BEGIN
       wcs_base = fitshead2wcs(wcs_base0)
       wcs_image = fitshead2wcs(wcs_image0)
    END
   'GEI': BEGIN
      wcs_base = fitshead2wcs(wcs_base0,system='A')
      wcs_image = fitshead2wcs(wcs_image0,system='A')
   END        
   ELSE: BEGIN
     wcs_base=wcs_coord_system(wcs_base0,system)
     wcs_image=wcs_coord_system(wcs_image0,system)
    END
  ENDCASE
ENDIF ELSE BEGIN
    wcs_image=wcs_image0
    wcs_base=wcs_base0
ENDELSE

; ---- compute position of input image pixels into the target image
coord = wcs_get_coord(wcs_base)

pix = wcs_get_pixel(wcs_image,coord)

; ---- interpolate input image into target field
fieldout=interpolate(image, reform(pix[0,*,*]), reform(pix[1,*,*]),cubic=-0.5,missing=0);!values.f_nan)

RETURN,fieldout
END

