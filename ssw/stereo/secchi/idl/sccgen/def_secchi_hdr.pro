FUNCTION DEF_SECCHI_HDR, secchi_hdr_tags,silent = silent
;+	
;$Id: def_secchi_hdr.pro,v 1.65 2012/06/26 21:51:36 nathan Exp $
;
; NAME:				DEF_SECCHI_HDR
; PURPOSE:			Define a Header Structure for a Level 0.5 SECCHI Image
; CATEGORY:			Administration, Pipeline
; CALLING SEQUENCE:		structure=DEF_SECCHI_HDR(hdr_tags)
; INPUTS:			None
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS:		a structure array containing an initialized header
;  secchi_hdr_tags	Returns strarr of tags in structure
;
; OPTIONAL OUTPUT PARAMETERS:
;  hdr_tags	STRARR of tags in header
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:			 
; PROCEDURE:
;
; MODIFICATION HISTORY:		
;  2004.04.14, nbr - create from def_lasco_hdr.pro
;
; $Log: def_secchi_hdr.pro,v $
; Revision 1.65  2012/06/26 21:51:36  nathan
; Add keyword CALFAC for use in SECCHI_PREP
;
; Revision 1.64  2012/04/05 21:27:37  nathan
; change VERSION to type string
;
; Revision 1.63  2011/08/26 16:10:29  nathan
; add datap50
;
; Revision 1.62  2011/01/07 16:39:36  nathan
; add CRITEVT and change EV* field definitions
;
; Revision 1.61  2010/08/18 17:16:26  nathan
; add telescop for intermission compatability
;
; Revision 1.60  2010/08/06 15:43:22  nathan
; fix ledpulse defn
;
; Revision 1.59  2008/07/02 17:39:45  nathan
; added RAVG for HI Level1
;
; Revision 1.58  2008/04/30 15:57:16  nathan
; added DISTCORR
;
; Revision 1.57  2008/03/14 19:14:27  nathan
; make EXPCMD, EXPTIME, BIASMEAN type float not double
;
; Revision 1.56  2007/11/13 18:35:21  mcnutt
; added CMDOFFSE Bug 256
;
; Revision 1.55  2007/08/28 21:02:48  nathan
; added INS_[ryx]0 (Bug 91)
;
; Revision 1.54  2007/08/14 14:52:13  nathan
; add TIME_OBS for lasco-heritage pros
;
; Revision 1.53  2007/07/18 13:05:49  nathan
; add SC_[YPR]*A keywords (Bug 177)
;
; Revision 1.52  2007/06/08 18:22:54  colaninn
; added misslist
;
; Revision 1.51  2007/05/21 18:43:56  colaninn
; added silent keyword
;
; Revision 1.50  2007/04/24 18:18:17  secchib
; nr - default OBS_PROG=schedule (Bug 151)
;
; Revision 1.49  2007/02/28 16:02:18  nathan
; add SC_ROLL,PITCH,YAW; remove CAMERA,STG1POS,STG2POS
;
; Revision 1.48  2007/02/09 23:59:19  nathan
; Make LEDPULSE uint32 (Bug 73); Add DSTART, DSTOP, RECTROTA (Bug 93)
;
; Revision 1.47  2007/02/08 18:59:13  nathan
; make OFFSETCR type Float
;
; Revision 1.46  2007/01/25 21:36:30  colaninn
; changed datazer and datasat to long
;
; Revision 1.45  2007/01/24 21:46:31  nathan
; rename ICERDIV2 to DIV2CORR
;
; Revision 1.44  2007/01/23 18:36:35  mcnutt
; added keyword icerdiv2
;
; Revision 1.43  2007/01/22 23:07:54  secchia
; added CRLN_OBS and CRLT_OBS
;
; Revision 1.42  2006/12/05 16:59:57  nathan
; fix BLANK
;
; Revision 1.41  2006/10/06 18:02:00  mcnutt
; added telescope image counter
;
; Revision 1.40  2006/09/21 17:34:11  mcnutt
; corrected Syntax error
;
; Revision 1.39  2006/09/21 15:16:40  nathan
; mv CAMERA,STGiPOS to pre-flight section; update PCi_j; PV2_1A
;
; Revision 1.38  2006/09/20 21:01:59  nathan
; Add PV2_1; change many defaults to 0. or 0.d
;
; Revision 1.37  2006/09/15 16:42:11  nathan
; make row/col vals signed
;
; Revision 1.36  2006/09/15 16:40:16  secchia
; nr - remove byte types
;
; Revision 1.35  2006/09/13 21:25:27  nathan
; init sumrow,sumcol with 1
;
; Revision 1.34  2006/09/12 19:07:56  colaninn
; changed DATAPii and BLANK to floats
;
; Revision 1.33  2006/09/08 16:46:06  nathan
; fix DATA comments
;
; Revision 1.32  2006/09/06 19:33:07  mcnutt
; added statistics keywords
;
; Revision 1.31  2006/09/06 14:01:03  colaninn
; fixed typo flts to flt
;
; Revision 1.30  2006/09/05 20:46:47  nathan
; update some default values and change flt to float not double
;
; Revision 1.29  2006/08/10 16:48:27  nathan
; EPHEMFIL, ATT_FILE, FPS defaults, JITRSDEV
;
; Revision 1.28  2006/07/26 15:58:14  mcnutt
; changed SOURCE to DOWNLINK
;
; Revision 1.27  2006/07/25 16:43:37  mcnutt
; added rectify and r row and col
;
; Revision 1.26  2006/07/20 20:48:38  mcnutt
; added more keywords for r1.7
;
; Revision 1.25  2006/07/20 20:37:23  nathan
; GAINMODE=str; CUNIT[12]A=deg; typo
;
; Revision 1.24  2006/07/19 19:37:11  nathan
; use 254 for encoder default
;
; Revision 1.23  2006/07/19 17:06:59  nathan
; add RO_DELAY, LINE_CLR, LINE_RO for HI
;
; Revision 1.22  2006/07/19 16:27:56  nathan
; updated initial values
;
; Revision 1.21  2006/07/18 16:35:43  mcnutt
; corrected errors
;
; Revision 1.20  2006/07/17 20:40:04  mcnutt
; added Keywords from FITS keywords r1.7
;
; Revision 1.19  2006/03/24 17:39:56  secchia
; nr - uncomment scstatus, scant_on, scfp_on
;
; Revision 1.18  2006/02/14 23:29:08  nathan
; change BIAS-> BIASMEAN, GAIN-> GAINCMD, DATE_MID -> DATE_AVG
;
; Revision 1.17  2006/02/03 10:46:04  nathan
; rename LED to LEDCOLOR
;
; Revision 1.16  2006/01/11 18:37:26  nathan
; uncomment SYNC,CRPIX,CRVAL,CDELT,[XY]CEN,SPICEFIL
;
; Revision 1.15  2005/12/12 20:57:42  mcnutt
; added SPICE
;
; Revision 1.14  2005/10/26 15:32:18  secchib
; fix type of BZERO
;
; Revision 1.13  2005/10/07 16:21:42  nathan
; keyword ip_00_19 replaces ip_10_19
;
; Revision 1.12  2005/08/01 14:08:46  nathan
; add uint16 type; add tabs
;
; Revision 1.11  2005/06/16 17:20:51  nathan
; add doorstat,ceb_stat,cam_stat
;
; Revision 1.10  2005/05/20 22:37:22  nathan
; add stat_ceb and stat_cam
;
; Revision 1.9  2005/05/20 17:59:12  nathan
; misc changes
;
; Revision 1.8  2005/04/21 19:23:58  nathan
; update comments
;
; Revision 1.7  2005/03/11 18:54:55  nathan
; uncomment bscale and bzero
;
; Revision 1.6  2005/03/02 16:10:51  nathan
; add NMISSING
;
; Revision 1.5  2005/02/28 23:00:45  nathan
; reorder keywords and remove obsolete and unused ones
;
; Revision 1.4  2005/02/02 16:10:25  nathan
; moved to new directory
;
; Revision 1.4  2005/01/24 23:45:51  nathan
; remove encoderh keyword
;
; Revision 1.3  2004/09/20 21:07:13  nathan
; fixed SIMPLE, LEDPULSE
;
; Revision 1.2  2004/09/12 03:38:43  nathan
; added fsw keywords
;-

   version=strmid('$Id: def_secchi_hdr.pro,v 1.65 2012/06/26 21:51:36 nathan Exp $',5,25)
IF ~keyword_set(silent) THEN print,version
   int = 0
   lon = 0L
   flt = 0.
   fltd= 0.0D
uint8 = 0UB
uint16 = 0US
uint32 = 0UL
int8 = 0B
int16 = 0S
int32 = 0L

   ; *** Those keywords where zero is a valid value are given initial values
   ;     of -1.
   ;
   str = ''
   sta = REPLICATE(str, 20)
   secchi_hdr={secchi_hdr_struct,	$
	;SIMPLE	:'T',	$
        EXTEND  :'F',	$	;T/F Indicates that there is (not) an extension
;
; ++++ Most-used keywords ++++
;
	BITPIX	:int,	$	;Number of bits per pixel (multiple of 8)
	NAXIS	:int,	$	;Number of axes in array
	NAXIS1	:int,	$	;Number of pixels in fastest ranging axis
	NAXIS2	:int,	$	;Number of pixels in second fastest ranging axis
    
        DATE_OBS:str,	$	;UTC Date and time of observation
	TIME_OBS:str,	$   	; for lasco compatability
	FILEORIG:str,	$	; name of original downlinked science file
	SEB_PROG:str,	$	; type of image
	SYNC	:str,	$   	; T/F: whether image is synchronized with other S/C
	SPWX	:'F',	$   	; T/F: whether image was also sent down SPWX channel
        EXPCMD	:-1.,	$	;Exposure duration sent (sec)
        EXPTIME	:-1.,	$	;Duration of shutter exposure (sec)
	DSTART1 :int,	$   	;First column of image area on data array
	DSTOP1	:int,	$   	;Last column of image area
	DSTART2	:int,	$   	;First row of image area
	DSTOP2	:int,	$   	;Last row of image area
        P1COL	:int16,$	;Position of 1st column of image on CCD (starting at 1)
        P2COL	:int16,$	;Position of last column of image on CCD
        P1ROW	:int16,$	;Position of 1st row of image on CCD
        P2ROW	:int16,$	;Position of last row of image on CCD
        R1COL	:int16,$	;rectified Position of 1st column of image on CCD (starting at 1)
        R2COL	:int16,$	;rectified Position of last column of image on CCD
        R1ROW	:int16,$	;rectified Position of 1st row of image on CCD
        R2ROW	:int16,$	;rectified Position of last row of image on CCD
        RECTIFY	:'F'   ,$	;Put ecliptic north to the top of the image
	RECTROTA:int,	$   	;Argument for rotate.pro to put ecliptic north at top of image
        LEDCOLOR:str,	$	;LED or laser:'NONE','BLUE','PURPLE','RED' ?
	LEDPULSE:0UL,	$	; number of led pulses
        OFFSET	:9999,	$	;Commanded offset value used in camera
        BIASMEAN:flt,   $	; mean of column 15 of readout
        BIASSDEV:-1.,	$	; standard deviation of column 15 of readout
        GAINCMD :-1,	$	;Commanded Gain value used in camera
        GAINMODE:str,	$	;FPGA Mode (1=LOW or 0=HIGH)
        SUMMED	:flt,	$	;Summing factor
        SUMROW	:1,	$	;Number rows being summed on CCD
        SUMCOL	:1,	$	;Number colums being summed on CCD
        CEB_T   :999,   $
        TEMP_CCD:9999., $
        POLAR	:-1.,	$	;Commanded Polarization angle (degrees) of polarizer, if any
	ENCODERP:-1,	$	; actual encoder value of polarizer 
        ;ENCODERH:-1,	$def_secchi_hdr.pro	;FIlter wheel or Polarizer encoder value
        WAVELNTH:int,	$	;Wavelength (angstroms) of selected Quadrant Selector position
        ENCODERQ:-1,	$	;Quadrant selector encoder value
        FILTER	:str,	$	;EUVI Filter Wheel position
	ENCODERF:-1,	$	; actual encoder value of filter wheel
	FPS_ON	:str,	$	; T/F: from actualFPSmode
        OBS_PROG:'schedule',	$	;Calling procedure or purpose of image
        ;DOORCLOS:str,	$	;Door is closed, T or F
        DOORSTAT:-1,	$	; door encoder value from enum eDoorStatus in cImageHdr.h
        SHUTTDIR:str,	$	;CCW, CW, or NONE means no shutter used
        READ_TBL:-1,	$	;[0-7] which table in READFILE/WGA_FILE used for readout
        CLR_TBL	:-1,	$	;[0-7] which table in READFILE/WGA file used for clear
	READFILE:str,	$	; name of readout table file used by fsw
	DATE_CLR:str,	$	; Date/time of start of clear table
        DATE_RO :str,	$	; Date/time of start of image retrieval
        READTIME:-1.d,	$	;Duration of readout (sec)
        CLEARTIM:fltd,	$	;Duration of clear (sec)
    	IP_TIME :-1,	$	; duration of onboard processing time (sec)
	COMPRSSN:int,	$	; code indicating algorithm used for compression
	COMPFACT:flt,	$	; compression factor achieved
        NMISSING:-1.,	$	; number of blocks missing
        MISSLIST:str,   $
	SETUPTBL:str,	$
	EXPOSTBL:str,	$
	MASK_TBL:str,	$
	IP_TBL:str,	$
	COMMENT :sta,	$	; Comment
	HISTORY :sta,   $	; Comments of processing history
	DIV2CORR:'F',   $	; corrected for IP divide by 2
	DISTCORR:'F',   $	; a platescale distortion correction has been applied to the data; details in comment or history

;
; ++++ Less-used keywords ++++
;
	TEMPAFT1:9999.,   $
	TEMPAFT2:9999.,   $
	TEMPMID1:9999.,   $
	TEMPMID2:9999.,   $
	TEMPFWD1:9999.,   $
	TEMPFWD2:9999.,   $
	TEMPTHRM:9999.,   $
	TEMP_CEB:9999.,   $
        ORIGIN	:str,	$	;Where image taken/processed
        DETECTOR:str,	$	;Which telescope
        IMGCTR	:uint16,$	;Number of images since swire/ops mode started (?)
        TIMGCTR :uint16,$	;Number of images since swire/ops mode started for telescope
	OBJECT	:str,	$	;What is supposed to be in image
        FILENAME:str,	$	;Name of FITS file
    	DATE:str,	$	; time FITS header was created
    	INSTRUME:'SECCHI',$
	OBSRVTRY:str,	$
	TELESCOP:'STEREO',$ 	; for inter-mission structure compatability
    	WAVEFILE:str,	$	; name of waveform table file used by fsw
	CCDSUM	:flt,	$	; (sumrow + sumcol) / 2.0
	IPSUM	:flt,	$	; (sebxsum + sebysum) / 2.0
	DATE_CMD:str,	$	; originally scheduled observation time
	DATE_AVG:str,	$	; date of midpoint of the exposure(s) (UTC standard)
	DATE_END:str,	$	; Date/time of end of (last) exposure
	OBT_TIME:fltd,	$	; value of STEREO on-board-time since epoch ???
	APID	:int,	$	; application identifier / how downlinked
	OBS_ID	:uint16,$	; observing sequence ID from planniing tool
	OBSSETID:uint16,$	; observing set (=campaign) ID from planning tool
	IP_PROG0:int,	$	; description of onboard image processing sequence used
	IP_PROG1:int,	$	; description of onboard image processing sequence used
	IP_PROG2:int,	$	; description of onboard image processing sequence used
	IP_PROG3:int,	$	; description of onboard image processing sequence used
	IP_PROG4:int,	$	; description of onboard image processing sequence used
	IP_PROG5:int,	$	; description of onboard image processing sequence used
	IP_PROG6:int,	$	; description of onboard image processing sequence used
	IP_PROG7:int,	$	; description of onboard image processing sequence used
	IP_PROG8:int,	$	; description of onboard image processing sequence used
	IP_PROG9:int,	$	; description of onboard image processing sequence used
	IP_00_19:str,	$	; numeral char representation of values 0 - 19 in ip.Cmds
	IMGSEQ	:-1,	$	; number of image in current sequence (usually 0)
 	OBSERVER:str,	$	;Name of operator
        BUNIT:str,	$	; unit of values in array
	BLANK:int, 	$	; value in array which means no data
	FPS_CMD :str, 	$	; T/F: from useFPS
        VERSION	:str,	$	; Identifier of FSW header version plus (EUVI only) pointing version
        CEB_STAT:-1,	$	; CEB-Link-status (enum CAMERA_INTERFACE_STATUS)
        CAM_STAT:-1,	$	; CCD-Interface-status (enum CAMERA_PROGRAM_STATE)
    	READPORT:str,	$   	; CCD readout port
        CMDOFFSE:flt,	$	; lightTravelOffsetTime/1000.
	RO_DELAY:-1.d,	$   	; time (sec) between issuing ro command to the CEB and the start of the ro operation
    	LINE_CLR:-1.d,	$   	; time (sec) per line for clear operation
	LINE_RO:-1.d,	$   	; time (sec) per line for readout operation
	RAVG:-999.,   	$   	; average error in star position (pixels)


        BSCALE:1.0, 	$	; scale factor for FITS
	BZERO:fltd, 	$	; value corresponding to zero in array for FITS
	SCSTATUS:-1,$	    ; spacecraft status message before exposure
	SCANT_ON:str,$	    ; T/F: derived from s/c status before and after
	SCFP_ON :str,$	    ; T/F: from actualSCFinePointMode
    	CADENCE:int,	$   	; Number of seconds between exposures/sequences for the current observing program

    	CRITEVT:str,	$   ; 0xHHHH (uppercase hex word)
    	EVENT:'F',  	$   ;A flare IP event has (not) been triggered
	EVCOUNT:str,    $   ;count of number of times evtDetect has run ('0'..'127') ... remains a string
	
	EVROW:int16,	$   ;X-coordinate of centroid of triggered event
	EVCOL:int16,	$   ;Y-coordinate of centroid of triggered event
;    	S1COL:int16,	$   ;Start X-coordinates of sub-image extracted by the FSW
;    	S2COL:int16,	$   ;End X-coordinates of sub-image extracted by the FSW
;    	S1ROW:int16,	$   ;Start Y-coordinates of sub-image extracted by the FSW
;    	S2ROW:int16,	$   ;End Y-coordinates of sub-image extracted by the FSW
    	COSMICS:int32,	$   ;Number of pixels removed from image by cosmic ray removal algorithm in FSW
    	N_IMAGES:int16, $   ;Number of CCD readouts used to compute the image
    	VCHANNEL:int,	$   ;Virtual channel of telemetry downlink  

; ++ Computed from information external to the image, on the ground ++
    	OFFSETCR:flt, 	$   ;Offset bias subtracted from image.
	DOWNLINK:str, 	$   ;How the image came down
    	DATAMIN:-1.0, 	$   ; Minimum value of the image, including the bias derived 
	DATAMAX :-1.0, 	$   ; Maximum value of the image derived 
	DATAZER :-1L, 	$   ; Number of zero pixels in the image derived 
	DATASAT :-1L, 	$   ; Number of saturated values in the image derived 
	DSATVAL :-1.0, 	$   ; Value used as saturated constant 
	DATAAVG :-1.0, 	$   ; Average value of the image derived 
	DATASIG :-1.0, 	$   ; Standard deviation in computing the average derived 
	DATAP01 :-1.0, 	$   ; Intensity of 1st percentile of image derived 
	DATAP10 :-1.0, 	$   ; Intensity of 10th percentile image derived 
	DATAP25 :-1.0, 	$   ; Intensity of 25th percentile of image derived 
	DATAP50 :-1.0, 	$   ; Intensity of 50th percentile of image derived (median)
	DATAP75 :-1.0, 	$   ; Intensity of 75th percentile of image derived 
	DATAP90 :-1.0, 	$   ; Intensity of 90th percentile of image derived 
	DATAP95 :-1.0, 	$   ; Intensity of 95th percentile of image derived 
	DATAP98 :-1.0, 	$   ; Intensity of 98th percentile of image derived 
	DATAP99 :-1.0, 	$   ; Intensity of 99th percentile of image derived
	CALFAC  : 0.0,	$   ; Calibration factor applied, NOT including binning correction 

;; ++ WCS - related ++

; ++ from pre flight cal++
    	CRPIX1	:flt,$	
        CRPIX2	:flt,$
    	CRPIX1A	:flt,$	
        CRPIX2A	:flt,$

;;++ From SPICE
        RSUN	:fltd,	$
        CTYPE1	:'HPLN-TAN',$
        CTYPE2	:'HPLT-TAN',$
        CRVAL1	:fltd,$
        CRVAL2	:fltd,$
        CROTA	:fltd,	$
        PC1_1	:1d,	$
        PC1_2	:fltd,	$
        PC2_1	:fltd,	$
        PC2_2	:1d,	$
        CUNIT1	:str,	$ ;ARCSEC or DEG for HI
        CUNIT2	:str,	$ ;ARCSEC or DEG for HI
        CDELT1	:fltd,$
        CDELT2	:fltd,$
	PV2_1	:fltd,$	    ; parameter for AZP projection (HI only)
	PV2_1A	:fltd,$	    ; parameter for AZP projection (HI only)
	SC_ROLL :9999d,$    ; values from get_stereo_hpc_point: (deg) - HI from scc_sunvec (GT)
	SC_PITCH:9999d,$    ; arcsec, HI deg
	SC_YAW  :9999d,$    ; arcsec, HI deg
	SC_ROLLA:9999d,$    ; RA/Dec values: (deg)
	SC_PITA :9999d,$    ; degrees
	SC_YAWA :9999d,$    ; degrees
	INS_R0	:0d,$	    ; applied instrument offset in roll
	INS_Y0	:0d,$	    ; applied instrument offset in pitch (Y-axis)
	INS_X0	:0d,$	    ; applied instrument offset in yaw (X-axis) from  

        CTYPE1A	:'RA---TAN',$
        CTYPE2A	:'DEC--TAN',$
        CUNIT1A	:'deg',	$ ;DEG
        CUNIT2A	:'deg',	$ ;DEG
        CRVAL1A	:fltd,$
        CRVAL2A	:fltd,$
        PC1_1A	:1d,	$
        PC1_2A	:fltd,	$
        PC2_1A	:fltd,	$
        PC2_2A	:1d,	$
        CDELT1A	:fltd,$
        CDELT2A	:fltd,$
        CRLN_OBS:fltd,$
        CRLT_OBS:fltd,$


        XCEN	:9999d,$
        YCEN	:9999d,$
        EPHEMFIL:str,	$   ; ephemeris SPICE kernel
        ATT_FILE:str,	$   ; attitude SPICE kernel 
        DSUN_OBS:fltd,	$
        HCIX_OBS:fltd,	$
        HCIY_OBS:fltd,	$
        HCIZ_OBS:fltd,	$
    	HAEX_OBS:fltd,	$
    	HAEY_OBS:fltd,	$
    	HAEZ_OBS:fltd,	$
    	HEEX_OBS:fltd,	$
    	HEEY_OBS:fltd,	$
    	HEEZ_OBS:fltd,	$
    	HEQX_OBS:fltd,	$
    	HEQY_OBS:fltd,	$
    	HEQZ_OBS:fltd,	$
        LONPOLE	:180, 	$
        HGLN_OBS:fltd,	$
        HGLT_OBS:fltd,	$
        EAR_TIME:fltd,	$
        SUN_TIME:fltd,	$
	
;
; EUVI only keywords
    	JITRSDEV:fltd,	$   	; std deviation of jitter from FPS or GT values
    	FPSNUMS:99999L,	$	; Number of FPS samples
    	FPSOFFY:0L,	$	; Y offset
    	FPSOFFZ:0L,	$	; Z offset
    	FPSGTSY:0L,	$	; FPS Y sum
    	FPSGTSZ:0L,	$	; FPS Z sum
    	FPSGTQY:0L,	$	; FPS Y square
    	FPSGTQZ:0L,	$	; FPS Z square
    	FPSERS1:0L,	$	; PZT Error sum [0]
    	FPSERS2:0L,	$	; PZT Error sum [1]
    	FPSERS3:0L,	$	; PZT Error sum [2]
    	FPSERQ1:0L,	$	; PZT Error square [0]
    	FPSERQ2:0L,	$	; PZT Error square [1]
    	FPSERQ3:0L,	$	; PZT Error square [2]
    	FPSDAS1:0L,	$	; PZT DAC sum [0]
    	FPSDAS2:0L,	$	; PZT DAC sum [1]
    	FPSDAS3:0L,	$	; PZT DAC sum [2]
    	FPSDAQ1:0L,	$	; PZT DAC square [0]
    	FPSDAQ2:0L,	$	; PZT DAC square [1]
    	FPSDAQ3:0L	$	; PZT DAC square [2]
;
; ++++ Preflight only ++++
;
;        CAMERA	:str,	$	;Identifier of camera electronics
;        STG1POS	:9999.,	$	;Position of StimTel stage 1
;        STG2POS	:9999.	$	;Postion of StimTel stage 2
;        EXPCLR	:flt,	$	;Time from start clear to start readout (sec)
;        BOARD	:int,	$	; cam 1, 2, or 3
;        CCD_ID	:str,	$	;identifier of CCD or FPA
;        SWIRE	:str,	$	;Version info of do_swire.cpp
;        WGA_FILE:str,	$	;Version info or name of WGA file, which lists waveforms and tables
;        TDSPC	:str,	$	;PC used to control camera/mechs
;    	READSTIM:flt,	$   	; duration of readout from do_swire.cpp
;        KEITHAVG:flt,	$	;Average current (Amps) during exposure from Keithley electrometer
;        KEITH_N :int,	$	;Number of values used to compute KEITHAVG
;        KEITH_HI:flt,	$	;High value in KEITH_N readings
;        KEITH_LO:flt,	$	;Low value in KEITH_N readings
;        TEMP1	:flt,	$	;Deg C from Lakeshore 330A
;        TEMP2	:flt,	$	;Deg C from Lakeshore 330B
;        TEMP3	:flt,	$	;Deg C from Lakeshore 211
;        LAMP	:str,	$	; = LED
;        MODE	:-1	$	; = GAINMODE
   }


   secchi_hdr_nt = N_TAGS(secchi_hdr)
   secchi_hdr_tags = TAG_NAMES(secchi_hdr)

  ; IF (N_PARAMS() EQ 1) THEN BEGIN	;** fill in the structure with the contents of the FITS header
 ;     FOR t=0, lasco_hdr_nt -1 DO BEGIN	;** for each tag in the lasco hdr structure
 ;        var = FXPAR(fits_hdr, lasco_hdr_tags(t))
 ;        lasco_hdr.(t) = var
 ;     ENDFOR
 ;  ENDIF

   RETURN, secchi_hdr

END
