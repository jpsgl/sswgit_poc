function scc_suffix, hdr
;
; Project     : SECCHI 
;                   
; Name        : scc_suffix
;               
; Purpose     : Get a standardized portion of filename describing the detector and type of image
;		which will be correctly interpreted by parse_secchi_filename.pro.
;
; INPUTS :  	frame or FITS header structure of supported instruments
;
; Optional Input :  
;
; OUTPUTS:  	"_wwttS" string which is part of filename like YYYYMMDD_HHMMSS_wwttS.png
;
; Use         : 
;
; Calls       : 
;
; Comments    : 
;               
; Side effects: 
;               
; Category    : 
;               
; Written     : N.Rich, NRL
;               
; $Log: scc_suffix.pro,v $
; Revision 1.1  2011/10/13 19:14:56  nathan
; called by scc_daily_pretties and make_lasco_pretties
;

cam=strlowcase(hdr.detector)
IF tag_exist(hdr,'obsrvtry') THEN sc =rstrmid(hdr.obsrvtry,0,1) ELSE $
    	    	    	    	  sc =rstrmid(hdr.filename,4,1)
wave='tb'
CASE cam OF
'cor2': BEGIN
    	IF tag_exist(hdr,'seb_prog') THEN IF hdr.seb_prog EQ 'DOUBLE' THEN wave='db'
	wwtts=wave+'c2'+sc
	END
'cor1': wwtts='tbc1'+sc
'euvi': BEGIN
    	wave=strmid(trim(hdr.wavelnth),0,2)
	wwtts=wave+'eu'+sc
	END
'hi1':  wwtts='tbh1'+sc
'hi2':  wwtts='tbh2'+sc
'c3':   wwtts='Lasc3'
'c2':   wwtts='Lasc2'
'aia':  BEGIN
    	wave=strmid(trim(hdr.wavelnth),0,2)
	wwtts=string(hdr.wavelnth,'(i4.4)')+'i'
	END
ELSE:	wwtts=cam
ENDCASE

return,'_'+wwtts

end

