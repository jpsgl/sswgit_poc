pro parseallmechs, START_DATE=start_date
;pro parsemechlogs, type, START_DATE=start_date
;+	
;$Id: parseallmechs.pro,v 1.1 2005/10/17 19:05:03 nathan Exp $
;
; NAME:				
; PURPOSE: generates a summary file of nmoves for all mechanisms
; CATEGORY:			
; CALLING SEQUENCE:		
; INPUTS:			none
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		
;   START_DATE = 'yyyy-mm-dd'    ; time to start from 
;
; OUTPUTS: writes output file in ./mechs
;  	
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	must be run FROM $stereo/itos/secchia or $sterei/itos/secchib
; PROCEDURE:
;
; MODIFICATION HISTORY:		
; $Log: parseallmechs.pro,v $
; Revision 1.1  2005/10/17 19:05:03  nathan
; fixed 050802
;
;-
;
;cor1shtr, cor2shtr, cor1pol, cor2pol, euvifw, euviqs, euvishtr

parsemechlogs, 'cor1shtr', START_DATE=start_date
parsemechlogs, 'cor2shtr', START_DATE=start_date
parsemechlogs, 'euvishtr', START_DATE=start_date
parsemechlogs, 'cor1pol', START_DATE=start_date
parsemechlogs, 'cor2pol', START_DATE=start_date
parsemechlogs, 'euvifw', START_DATE=start_date
parsemechlogs, 'euviqs', START_DATE=start_date
END
