pro plotsccpointing, timesin, cam0, sc0, dx, dy, tout, SUNCEN=suncen, $
    	PIXELS=pixels, _EXTRA=_extra
;+
; $Id: plotsccpointing.pro,v 1.3 2011/12/23 17:47:22 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : plotsccpointing
;
; Purpose : Plot variation in location of sun center relative to CCD 
;
; Use     : IDL> plotsccpointing,'2008-01-02','hi1','a'
;
; Required Input:
;   	timesin     STRING or UTC time; if scalar than data for day is returned;
;    	    	    if 2-element array, then this is a time range
;	cam 	    detector (hi1, hi2, cor1, cor2, euvi)
;	sc  	    spacecraft (input to parse_stereo_name.pro)
;
; Optional Keyword Input:
;   	SUNCEN= FLTARR values of CRVAL1,2 to use as baseline instead of first value 
;   	    	(for EUVI, values of CRPIX1,2)
;   	/PIXELS Plot output as pixel units for full=res
;
; Optional Output:   
;    	dx,dy,tout	change in x and change in y (arcsec) and times
;   	SUNCEN= FLTARR values of CRVAL1,2 used as baseline
;
; Common  :
;
; Restrictions: 
;
; Side effects: 
;
; Category   : attitude, analysis, calibration, pointing
;
; Prev. Hist.:

; Written    : N.Rich, NRL/I2, 10/15/08
; $Log: plotsccpointing.pro,v $
; Revision 1.3  2011/12/23 17:47:22  nathan
; optional output SUNCEN=
;
;-

d1=anytim2utc(timesin[0])
IF n_elements(timesin) LT 2 THEN BEGIN
	d1.time=0
	d2=d1
	d2.mjd=d2.mjd+1
ENDIF ELSE d2=anytim2utc(timesin[1])

cam=strupcase(cam0)
tel=cam
sc=parse_stereo_name(sc0,['A','B'])
dlm=get_delim()

IF cam NE 'HI1' and cam ne 'HI2' THEN BEGIN
    print,'For now, this program only does HI1 and HI2. Sorry.'
    return
ENDIF

;-First get list of hi pointing files
dstr=utc2str(d1,/date)
dd=d1
filedir=getenv('SCC_DATA')+dlm+'hi'+dlm
hifn=file_search(filedir+'pnt_'+tel+sc+'_'+dstr+'*fts')
IF hifn[0] EQ '' THEN BEGIN
    message,'HI pointing file for '+dstr+' not found.',/info
    hifns=file_search(filedir+'pnt_'+tel+sc+'_*fts')
    IF hifns[0] EQ '' THEN BEGIN
    	print,'$SCC_DATA is read as <',getenv('SCC_DATA'),'> Is this correct?'
    ENDIF ELSE BEGIN
    	nf=n_elements(hifns)
    	print,'Sorry. Last file available is ',hifns[nf-1]
    ENDELSE
    return
ENDIF
IF d2.mjd-d1.mjd GT 0 THEN FOR i=d1.mjd+1,d2.mjd DO BEGIN
    dd.mjd=i
    dstr=utc2str(dd,/date)
    hifn1=file_search(getenv('SCC_DATA')+dlm+'hi'+dlm+'pnt_'+tel+sc+'_'+dstr+'*fts')
    IF hifn1[0] EQ '' THEN BEGIN
    	nf=n_elements(hifn)
	print,'NOTE: Last file available is ',hifn[nf-1]
    ENDIF ELSE $
    hifn=[hifn,hifn1]
ENDFOR

;-Now read in hi pointing files
hdrs=hi_mread_pointing(hifn)

; Filter out bad roll angles
help,hdrs

wg=where(abs(hdrs.crota) LT 10,nwg,complement=wb, ncompl=nwb)
IF nwb GT 1 THEN print,'Omitting following out-of-range angles:',hdrs[wb].filename
wait,2
IF nwg GT 2 THEN hdrs=hdrs[wg]

;-Now compute differences
IF keyword_set(SUNCEN) THEN crval0=suncen ELSE crval0=[hdrs[0].crval1,hdrs[0].crval2]
suncen=crval0

dx = hdrs.crval1-crval0[0]
dy = hdrs.crval2-crval0[1]
dxy=sqrt(dx^2 + dy^2)    

;-Get time range
tout=anytim2utc(hdrs.date_avg)
ind1=near_time(tout,d1)
ind2=near_time(tout,d2)

;-Do plot
avdxy=avg(dxy)
;-for HI values are in degrees
vunit='Degrees'
help,vunit
print,'CDELT=',hdrs[0].cdelt1
;IF avdxy LT 1 THEN BEGIN
;    dxy=dxy*3600.
;    vunit='Arcsec'
;ENDIF

fac=1.
IF keyword_set(PIXELS) THEN BEGIN
    dxy=dxy/(hdrs.cdelt1*hdrs.ny/1024)
    vunit='Pixels (1024x1024)'
    fac=hdrs.cdelt1
ENDIF
tit='Change in Sun Location for '+tel+'-'+sc
miny=min([dx,dy]/fac,max=maxy)
plotvtime,tout[ind1:ind2],dx[ind1:ind2]/fac,'x',title=tit,ytitle=vunit,yrange=[miny,maxy],_EXTRA=_extra
oplotvtime,tout[ind1:ind2],dy[ind1:ind2]/fac,'y'
stop
end
