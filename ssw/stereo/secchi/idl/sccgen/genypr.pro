pro genypr, times, hypr, gtypr, STARTTIME=starttime, NDAYS=ndays, RES=res, SPACECRAFT=spacecraft, $
    	    RADEC=radec
;+
; $Id: genypr.pro,v 1.2 2007/08/28 21:01:40 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : genypr
;
; Purpose : Generate yaw,pitch,roll over a period of time 
;
; Use     : IDL> 
;
; Required Keyword Input:
;	STARTTIME= 	'yyyy-mm-dd' start date
;	NDAYS=		number of days
;	RES=		number of seconds between data points
;
; Optional Keyword Input:
;   	/RADEC	Use get_stereo_roll and return GEI coordinates instead of 
;   	    	get_stereo_hpc_point and HPC coordinates
;
; Output:   
;	times	Array(n) of UTC time structures
;	hypr	3xn array of yaw (arcsec), pitch(arcsec), roll(degrees)
;		from get_stereo_hpc_point
;	gypr	3xn array of yaw,pitch,roll from scc_sunvec
;
; Common  :
;
; Restrictions: 
;
; Side effects: 
;
; Category   : attitude, analysis, calibration, pointing
;
; Prev. Hist.:

; Written    : N.Rich, NRL/I2, 6/5/07
;
; $Log: genypr.pro,v $
; Revision 1.2  2007/08/28 21:01:40  nathan
; add /RADEC
;
; Revision 1.1  2007/07/11 14:54:39  nathan
; Generates yaw,pitch,roll arrays for SPICE and GT
;
; Revision 1.1  2007/06/20 21:12:19  nathan
; draft
;
;-

t0=utc2tai(str2utc(starttime))
n=ndays*24*3600d/res
help,n
tais=(dindgen(n)*res)+t0

times=tai2utc(tais)
IF keyword_set(RADEC) THEN BEGIN
    print,'Computing RA-DEC pointing'
    r0 = GET_STEREO_ROLL(times,spacecraft, y0, p0, SYSTEM='GEI')
    hypr=dblarr(3,n)
    hypr[0,*]=y0
    hypr[1,*]=p0
    hypr[2,*]=r0
ENDIF ELSE BEGIN
    print,'Computing hpc pointing'
    hypr=get_stereo_hpc_point(times,spacecraft)
ENDELSE
print,'Getting sun distance'
xyz= get_stereo_lonlat(times,spacecraft,/au)
gpy=dblarr(2,n)
print,'Getting GT-sun vector'
for i=0,n-1 do gpy[*,i]=scc_sunvec(times[i],xyz[0,i],obs=spacecraft,/quiet)
gtypr=dblarr(3,n)
gtypr[0,*]=gpy[1,*]
gtypr[1,*]=gpy[0,*]
gtypr[2,*]=hypr[2,*]

end
