function ymdd2utc, ymdd
; $Id: ymdd2utc.pro,v 1.2 2010/01/04 22:39:19 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : 
;               
; Purpose     : converts first four chars of SECCHI raw image filename to UTC date
;               
; Explanation : 
;               
; Use         : 
;    
; Inputs      : ymdd	STRING
;               
; Outputs     : UTC structure
;
; Keywords :	
;               
; Calls from LASCO : 
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : time, telemetry, file
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Apr 2007
;               
; $Log: ymdd2utc.pro,v $
; Revision 1.2  2010/01/04 22:39:19  secchia
; nr - fix for base-36 year digit
;
; Revision 1.1  2007/04/10 22:42:07  nathan
; used in secchi_reduce.pro
;

y=strmid(ymdd,0,1)
hex2dec,strmid(ymdd,1,1),m
mm=string(m,'(i2.2)')
dd=strmid(ymdd,2,2)
yyyy=string(2000+bntoint(y,36),'(i4)')

return, str2utc(yyyy+'-'+mm+'-'+dd)

end
