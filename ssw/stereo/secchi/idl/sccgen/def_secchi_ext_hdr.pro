FUNCTION DEF_SECCHI_EXT_HDR, secchi_hdr_tags
;+	
;$Id: def_secchi_ext_hdr.pro,v 1.13 2010/07/06 22:49:42 nathan Exp $
;
; NAME:				DEF_SECCHI_HDR
; PURPOSE:			Define an extended Header Structure for a Level 0.5 SECCHI Seq
; CATEGORY:			Administration, Pipeline, FITS, header, 
; CALLING SEQUENCE:		structure=DEF_SECCHI_EXT_HDR(hdr_tags)
; INPUTS:			None
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
;
; OUTPUTS:		
;  secchi_hdr_tags	Returns FITS header strarr 
;
; OPTIONAL OUTPUT PARAMETERS:
;  hdr_tags	STRARR of tags in header
; COMMON BLOCKS:
; SIDE EFFECTS:
; RESTRICTIONS:	Values must match string defined in make_scc_ext_tbl.pro		 
; PROCEDURE:
;
; MODIFICATION HISTORY:		
;
; $Log: def_secchi_ext_hdr.pro,v $
; Revision 1.13  2010/07/06 22:49:42  nathan
; added col 25 fileorig; corrections in col 7,13
;
; Revision 1.12  2009/06/02 18:52:18  mcnutt
; changed deltatime to f8.2 for hisam
;
; Revision 1.11  2007/12/13 16:40:31  nathan
; updated column nums for POLAR change, add units for crval
;
; Revision 1.10  2007/12/12 20:48:53  nathan
; changed exptime format and updated column numbers to match
;
; Revision 1.9  2007/06/01 20:49:55  mcnutt
; corrected ncolums
;
; Revision 1.8  2007/04/05 14:55:30  mcnutt
; change RVAL format to allow for negative
;
; Revision 1.7  2007/04/03 18:26:27  mcnutt
; corrected table colums
;
; Revision 1.6  2007/01/26 17:11:46  mcnutt
; added spice to extended header
;
; Revision 1.5  2007/01/24 19:47:18  mcnutt
; change sumcol and sumrow to ccdsum and ipsum
;
; Revision 1.4  2006/12/20 19:41:19  mcnutt
; now uses fxaddpar instead of creating structure
;
; Revision 1.3  2006/07/25 16:42:05  mcnutt
; change cosmic format to i7
;
; Revision 1.2  2006/07/21 18:34:13  mcnutt
; modified for R1.7
;
; Revision 1.1  2006/03/28 19:56:41  mcnutt
; SECCHI extended table header
;
;  2004.04.14, nbr - create from def_lasco_hdr.pro

   version=strmid('$Id: def_secchi_ext_hdr.pro,v 1.13 2010/07/06 22:49:42 nathan Exp $',5,25)
print,version
   int = 0
   lon = 0L
   flt = 0d
uint8 = 0UB
uint16 = 0US
uint32 = 0UL
int8 = 0B
int16 = 0S
int32 = 0L



 	FXADDPAR,secchi_ext_hdr,'XTENSION','TABLE','Written by IDL:  '+ SYSTIME()
	FXADDPAR,secchi_ext_hdr,'BITPIX',8
	FXADDPAR,secchi_ext_hdr,'NAXIS',2,'ASCII table'
	FXADDPAR,secchi_ext_hdr,'NAXIS1',202,'Number of bytes per row'
	FXADDPAR,secchi_ext_hdr,'NAXIS2',0,'Number of exposures in the sequence'
	FXADDPAR,secchi_ext_hdr,'PCOUNT',0,'Random parameter count'
	FXADDPAR,secchi_ext_hdr,'GCOUNT',1,'Group count'
	FXADDPAR,secchi_ext_hdr,'TFIELDS',25,'Number of columns'
;   
        FXADDPAR,secchi_ext_hdr,'TBCOL1',1        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM1','F8.2'       
        FXADDPAR,secchi_ext_hdr,'TTYPE1','DELTATIME' 
        FXADDPAR,secchi_ext_hdr,'TUNIT1','seconds'    

        FXADDPAR,secchi_ext_hdr,'TBCOL2',10        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM2','F10.6'       
        FXADDPAR,secchi_ext_hdr,'TTYPE2','EXPTIME'  
        FXADDPAR,secchi_ext_hdr,'TUNIT2','seconds'    

        FXADDPAR,secchi_ext_hdr,'TBCOL3',21       		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM3','I2'      
        FXADDPAR,secchi_ext_hdr,'TTYPE3','CCDSUM'  
        FXADDPAR,secchi_ext_hdr,'TUNIT3','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL4',24       		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM4','I2'      
        FXADDPAR,secchi_ext_hdr,'TTYPE4','IPSUM' 
        FXADDPAR,secchi_ext_hdr,'TUNIT4','NA'   

        FXADDPAR,secchi_ext_hdr,'TBCOL5',27       		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM5','F6.1'       
        FXADDPAR,secchi_ext_hdr,'TTYPE5','POLAR'  
        FXADDPAR,secchi_ext_hdr,'TUNIT5','Degrees'    

        FXADDPAR,secchi_ext_hdr,'TBCOL6',34        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM6','A1'       
        FXADDPAR,secchi_ext_hdr,'TTYPE6','SHUTTER'  
        FXADDPAR,secchi_ext_hdr,'TUNIT6','Logical'    

        FXADDPAR,secchi_ext_hdr,'TBCOL7',36        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM7','I3'       
        FXADDPAR,secchi_ext_hdr,'TTYPE7','ENCODER' 
        FXADDPAR,secchi_ext_hdr,'TUNIT7','NA'    
	
        FXADDPAR,secchi_ext_hdr,'TBCOL8',40        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM8','A1'       
        FXADDPAR,secchi_ext_hdr,'TTYPE8','LEDCOLOR' 
        FXADDPAR,secchi_ext_hdr,'TUNIT8','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL9',42        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM9','A1'      
        FXADDPAR,secchi_ext_hdr,'TTYPE9','DOORSTAT' 
        FXADDPAR,secchi_ext_hdr,'TUNIT9','NA'   

        FXADDPAR,secchi_ext_hdr,'TBCOL10',44       		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM10','I5'       
        FXADDPAR,secchi_ext_hdr,'TTYPE10','IMGCTR'  
        FXADDPAR,secchi_ext_hdr,'TUNIT10','None'    

        FXADDPAR,secchi_ext_hdr,'TBCOL11',50       		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM11','I4'       
        FXADDPAR,secchi_ext_hdr,'TTYPE11','IMGSEQ'
        FXADDPAR,secchi_ext_hdr,'TUNIT11','None'    
	
        FXADDPAR,secchi_ext_hdr,'TBCOL12',55        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM12','A1'       
        FXADDPAR,secchi_ext_hdr,'TTYPE12','EVENT'  
        FXADDPAR,secchi_ext_hdr,'TUNIT12','Logical'    

        FXADDPAR,secchi_ext_hdr,'TBCOL13',57       		    ;Column Number 
        FXADDPAR,secchi_ext_hdr,'TFORM13','A6'      
        FXADDPAR,secchi_ext_hdr,'TTYPE13','EVCOUNT' 
        FXADDPAR,secchi_ext_hdr,'TUNIT13','None'  

        FXADDPAR,secchi_ext_hdr,'TBCOL14',64       		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM14','I4'      
        FXADDPAR,secchi_ext_hdr,'TTYPE14','EVROW'  
        FXADDPAR,secchi_ext_hdr,'TUNIT14','Row'    

        FXADDPAR,secchi_ext_hdr,'TBCOL15',69        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM15','I4'       
        FXADDPAR,secchi_ext_hdr,'TTYPE15','EVCOL'  
        FXADDPAR,secchi_ext_hdr,'TUNIT15','Column'    

        FXADDPAR,secchi_ext_hdr,'TBCOL16',74        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM16','A23'       
        FXADDPAR,secchi_ext_hdr,'TTYPE16','DATE_CLR' 
        FXADDPAR,secchi_ext_hdr,'TUNIT16','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL17',98        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM17','A23'       
        FXADDPAR,secchi_ext_hdr,'TTYPE17','DATE_RO'  
        FXADDPAR,secchi_ext_hdr,'TUNIT17','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL18',122        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM18','F9.6'       
        FXADDPAR,secchi_ext_hdr,'TTYPE18','PC1_1' 
        FXADDPAR,secchi_ext_hdr,'TUNIT18','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL19',132        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM19','F9.6'       
        FXADDPAR,secchi_ext_hdr,'TTYPE19','PC1_2' 
        FXADDPAR,secchi_ext_hdr,'TUNIT19','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL20',142        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM20','F9.6'       
        FXADDPAR,secchi_ext_hdr,'TTYPE20','PC2_1' 
        FXADDPAR,secchi_ext_hdr,'TUNIT20','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL21',152        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM21','F9.6'       
        FXADDPAR,secchi_ext_hdr,'TTYPE21','PC2_2' 
        FXADDPAR,secchi_ext_hdr,'TUNIT21','NA'    

        FXADDPAR,secchi_ext_hdr,'TBCOL22',162        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM22','F9.5'       
        FXADDPAR,secchi_ext_hdr,'TTYPE22','CRVAL1' 
        FXADDPAR,secchi_ext_hdr,'TUNIT22','deg'    

        FXADDPAR,secchi_ext_hdr,'TBCOL23',172        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM23','F9.5'       
        FXADDPAR,secchi_ext_hdr,'TTYPE23','CRVAL2' 
        FXADDPAR,secchi_ext_hdr,'TUNIT23','deg'    

        FXADDPAR,secchi_ext_hdr,'TBCOL24',182        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM24','I7'       
        FXADDPAR,secchi_ext_hdr,'TTYPE24','COSMIC'  
        FXADDPAR,secchi_ext_hdr,'TUNIT24','Pixels'    

        FXADDPAR,secchi_ext_hdr,'TBCOL25',190        		    ;Column Number
        FXADDPAR,secchi_ext_hdr,'TFORM25','A12'       
        FXADDPAR,secchi_ext_hdr,'TTYPE25','FILEORIG'  
        FXADDPAR,secchi_ext_hdr,'TUNIT25','NA'    


   RETURN, secchi_ext_hdr

END
