FUNCTION scc_mk_image,infile,yloc,startind,ftimes,outsize=outsize,domask=domask,nologo=nologo, $ 
    	nodatetime=nodatetime,outhdr=outhdr,nopop=nopop, NOROTATE=norotate, MINMAX=mm, FITS=fits, $
	TIMES=times, NOSCALE=noscale, PNG=png, histars=histars, AUTOMAX=automax, SKIPROLLED=skiprolled, $
	MASK_OCC=mask_occ, LG_MASK_OCC=lg_mask_occ, _EXTRA=_extra
;+
; $Id: scc_mk_image.pro,v 1.40 2017/08/10 21:43:53 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : SCC_MK_IMAGE
;               
; Purpose   : This routine will return a pretty, background subtracted image for a given SECCHI file
;
;               
; Explanation: 
;               
; Use       : IDL> pretty_image=scc_mk_image('full_path_to_secchi_fits_image',[opts])
;    
; Inputs    : infile = full path to a SECCHI FITS image. !!!SEE RESTRICTIONS!!!!
;            OUTSIZE = output image size (default = 1024)
;   	    MINMAX= [bmin,bmax] Min and max values (FLOAT) to scale ratio image.
;            
;               
; Outputs   : 'pretty_image' = Bytescaled data array of the specified output size (default 1024x1024)
;             OUTHDR (optional) = returns the header structure from the FITS file.
;   	      Bad image returns -1
;
; Other Keywords  : 
;   	     /NOSCALE	Return float image array
;            /DOMASK = Mask off some dark lines in COR2-A -- Don't use this option for now, please!
;            /NOLOGO = Don't put SECCHI logo on output image
;            /NODATETIME = Don't put date/time on output image
;            /NOPOP = Don't pop up a window while making the image
;            /NOROTATE = Do not apply solar-north-up correction
;   	     TIMES= size of time-stamp; 1-2 is charsize for regular Hershey vector font; 3+ uses helvetica bold truetype
;   	     /HISTARS	Omit median filter if HI image
;   	    /RDIFF= 	Returns running diff version of output (bytescaled)
;   	    /SKIPROLLED Return -1 if abs(crota) GT 10
;   	    /PNG    Save png file from output
;   	    /FITS   Save FITS file from output (implies /NOSCALE)
;   	    /MASK_OCC	Use small occulter mask
;
; Common    : 
;               
; Restrictions: 
;           Does not combine polarized images.
;               
; Side effects: 
;               
; Category    : DISPLAY
;               
; Prev. Hist. : None.
;
; Written     : Karl Battams, NRL/I2, JUNE 2007
;               
; $Log: scc_mk_image.pro,v $
; Revision 1.40  2017/08/10 21:43:53  nathan
; mask keywords
;
; Revision 1.39  2014/04/25 17:41:13  nathan
; added /FITS option
;
; Revision 1.38  2012/04/11 16:54:42  secchib
; add /skiprolled
;
; Revision 1.37  2011/12/22 15:35:53  nathan
; add /debug for wait
;
; Revision 1.36  2011/09/01 12:59:50  secchia
; updated common block scc_movie to match scc_mkframe
;
; Revision 1.35  2011/08/10 16:31:36  nathan
; adjustments for cor2 rolled mask
;
; Revision 1.34  2011/08/09 21:09:27  nathan
; return -1 if error in scc_mkframe
;
; Revision 1.33  2011/08/09 19:01:50  nathan
; implement /AUTOMAX option
;
; Revision 1.32  2011/08/08 22:30:27  nathan
; different bmin for rolled HI1 image
;
; Revision 1.31  2011/08/08 17:42:26  nathan
; adjust bmin/bmax for rolled HI images
;
; Revision 1.30  2011/02/04 19:42:50  nathan
; adjust cor2b min/max
;
; Revision 1.29  2011/01/14 16:59:35  secchib
; automax not working for hi1
;
; Revision 1.28  2010/09/30 22:01:42  nathan
; implement MINMAX= keyword
;
; Revision 1.27  2010/08/26 17:40:36  mcnutt
; default for EUVI mask to off
;
; Revision 1.26  2010/02/17 18:29:20  nathan
; update documentation
;
; Revision 1.25  2010/02/08 22:07:08  secchia
; init running_diff
;
; Revision 1.24  2008/11/03 16:26:47  mcnutt
; change keyword hienhance to histars to default to enhanced images
;
; Revision 1.23  2008/11/03 15:34:55  mcnutt
; added histars keyword, will create newer hi images if set else default back to v1.17
;
; Revision 1.22  2008/10/08 15:16:31  secchib
; nr - changed median box to 5 for hi1
;
; Revision 1.21  2008/09/26 19:30:52  nathan
; added /PNG option; tweaked HI1 parameters
;
; Revision 1.20  2008/09/25 15:43:08  nathan
; Added /NOSCALE; fill_col=-1; COR2: use ratio, no log_scl; hi2: use monthly model,
; ratio not diff, no log_scl; hi1: ratio, no log_scl; removed waits
;
; Revision 1.19  2008/09/17 14:03:06  mcnutt
; changed HI to use_model 2 daily_med
;
; Revision 1.18  2008/07/09 18:03:29  nathan
; allow setting of Times size with keyword
;
; Revision 1.17  2008/06/18 18:20:35  nathan
; added /NOROTATE and auto set times size
;
; Revision 1.16  2008/03/31 20:05:39  nathan
; fix /nologo keyword
;
; Revision 1.15  2008/02/27 16:42:10  mcnutt
; gets ftimes from scc_daily_Pretties or defines ftime if not set
;
; Revision 1.14  2008/02/22 15:36:53  nathan
; remove unused arrays; define startind if not input
;
; Revision 1.13  2008/02/14 16:59:09  mcnutt
; change fill_col to 01 for cor2
;
; Revision 1.12  2008/02/13 17:35:58  mcnutt
; calls scc_mkframe which calls sechi_prep
;
; Revision 1.11  2008/02/12 14:50:51  nathan
; default dorotate=0; add /rotcubic; fix outsizex(y) computation
;
; Revision 1.10  2008/02/01 15:02:10  nathan
; misc changes
;
; Revision 1.9  2007/11/14 14:40:07  reduce
; Add nopop k/w to stop popup windows, if desired. Karl.
;
; Revision 1.8  2007/11/13 21:59:47  nathan
; tweaked bmin/max for HI1; exptime on for EUVI; display EUVI in scc_bytscl
;
; Revision 1.7  2007/11/13 16:03:30  nathan
; more changes so it works right for cor2; still not working for HI
;
; Revision 1.6  2007/10/26 22:39:08  nathan
; Made a number of changes to go with new backgrounds: uses secchi_prep instead
; of sccreadfits; uses fixed bmin,bmax instead of depending on median; removed
; /silent from prep calls (at least temporarily); do not add min(im)
;
; Revision 1.5  2007/10/04 15:25:14  reduce
; Use /interpolate in getbkgimg. Karl.
;
; Revision 1.4  2007/09/24 18:08:57  reduce
; Added totalb kw call to getbkgimg for COR2. Karl
;
; Revision 1.3  2007/06/28 19:59:02  reduce
; Had the scaling wrong on COR2-B. Argh! KB.
;
; Revision 1.2  2007/06/28 18:17:35  reduce
; Fixed it so EUVI definitely works now! KB.
;
; Revision 1.1  2007/06/27 20:06:04  reduce
; Initial release! Karl B.
;
;
;-

;common mvmask
common scc_movie, prevfimg, prevhdr, prevbimg, modtai, hdrm, refcmn ,ref_exp,  fillcol, datecolor, firstpol, fflag, normvals
; common scc_movie is used in scc_mkframe.pro

;print,infile
IF NOT file_exist(infile) THEN BEGIN
    PRINT,'File ',strcompress(infile,/remove_all),' does not appear to exist.'
    PRINT,'Please make sure you have specified the correct FULL PATH to the file.'
    return,-1
ENDIF
x=sccreadfits(infile,header,/nodata)
IF keyword_set(SKIPROLLED) THEN IF abs(header.crota) GT 10 THEN BEGIN
    message,'Skipping '+infile+' because CROTA='+trim(header.crota),/info
    wait,5
    return,-1
ENDIF
if ~keyword_set(nopop) then nopop=0
cam=strlowcase(header.DETECTOR)
obs=strlowcase(header.OBSRVTRY)
diff=0
ratio=1
logo=1
euvi_flag=0
hi_flag=0
dorotate=0
;lg_mask_occ=0
outermask=0
secchiprepmask=0
filemask=0
docolor=0
noexpcorr=1
log_scl=0
use_model=0
limb=0
;VERBOSE=0
doAUTOMAX=0
nocal=1
fill_col=-1 	; use median
domedian=0
lee_filt=0
running_diff=0
;defined in scc_daily_pretties
IF datatype(startind) EQ 'UND' THEN startind=0

IF datatype(ftimes) EQ 'UND' THEN ftimes= STRARR(N_ELEMENTS(infile))

IF ~keyword_set(outsize) then outsize=1024 
IF ~keyword_set(TIMES) THEN times=outsize/512 ; 2048=4, 512=1
IF keyword_set(NODATETIME) THEN times=0


;pan=float(outsize)/2048
pan=outsize
IF keyword_set(NOLOGO) THEN logo=0

CASE cam of
    'cor1':  BEGIN
         PRINT,'Pretty picture processing is not yet implemented for COR1. Sorry...'
         RETURN,-1
    END
    'cor2':  BEGIN
        LOADCT,1 
        filemask=1
        use_model=1
        ;log_scl=1
        ;diff=1
        limb=1
        ;fill_col=-1 
        IF ~keyword_set(NOROTATE) THEN dorotate=1
        IF obs EQ 'stereo_a' THEN BEGIN
    	    bmax=1.12
    	    bmin=0.93
	ENDIF ELSE BEGIN
	    bmax=1.1	;1.05
	    bmin=0.93	;0.95
	ENDELSE
	IF header.seb_prog EQ 'DOUBLE' or header.polar LT 1000 THEN BEGIN
	    bmax=1.1
	    bmin=0.95
	    ; these values worked for A; maybe try bminscalfac=0.25, bmaxscalefac=2.6 for median(im)
	ENDIF
        ;IF expt EQ 2 THEN BEGIN
        ;    bmin_scalefac = 0.997
        ;    bmax_scalefac = 1.0023
        ;ENDIF ELSE BEGIN
        ;    bmin_scalefac = 0.8     ;0.991
        ;    bmax_scalefac = 1.2     ;1.0055
        ;ENDELSE
     	IF abs(header.sc_roll) GT 15 THEN BEGIN
	    filemask=0
	    outermask=1
	ENDIF
	    
   END
    'hi1':  BEGIN
        hi_flag=1
        noexpcorr=0
        LOADCT,3
            if keyword_set (histars) then begin 
               log_scl=1
               diff=1        
               use_model=3
               ratio=0
	       domedian=0
     	           ;IDL[iapetus]>maxmin,im
    	           ;       2.5943793      0.49137331      0.42772352
	       bmax=0.85
	       bmin=0.38
	       IF obs EQ 'stereo_b' THEN BEGIN
	          ;IDL[iapetus]>maxmin,im
    	           ;       2.5869070      0.48516662      0.43978811
    	              bmax=0.88
	              bmin=0.33
               ENDIF	    
            ENDIF else begin
               log_scl=0
               diff=0       
               use_model=2
	       domedian=5
     	;IDL[iapetus]>maxmin,im
    	;       2.5943793      0.49137331      0.42772352
	;bmax=0.85
	;bmin=0.38
    	;Image before bytscl: (after ratio)
    	;      3.26877      1.00144     0.999334
	      ;doautomax=1
	      bmax=1.01
	      bmin=0.997
	      	IF obs EQ 'stereo_b' THEN BEGIN
	      ;IDL[iapetus]>maxmin,im
    	      ;       2.5869070      0.48516662      0.43978811
    	            bmax=1.02
	        ;bmin=0.97
              	ENDIF
    	    	IF abs(header.sc_roll) GT 15 THEN BEGIN
	     	;bmax=bmax+0.2
		;bmin=0.8
		    doautomax=1
    	    	ENDIF
            ENDELSE
       ;IF obs EQ 'stereo_b' THEN BEGIN
        ;    bmin_scalefac = 0.2
        ;    bmax_scalefac = 7
        ;ENDIF ELSE BEGIN
        ;    bmin_scalefac = 0.1
        ;    bmax_scalefac = 6
        ;ENDELSE
    END
    'hi2':  BEGIN
        hi_flag=1
        noexpcorr=0
        LOADCT,1
            if keyword_set (histars) then begin 
                  use_model=3
                  log_scl=1
                  diff=1        
   	          domedian=0
                  ratio=0
 	          bmax=1.0
	          bmin=0.3
             ENDIF ELSE begin
                  use_model=1
                  log_scl=0
                  diff=0        
	          domedian=3
 	          bmax=1.5
	          bmin=0.93
	          IF obs EQ 'stereo_b' THEN bmax=1.4
             ENDELSE
    	    IF abs(header.sc_roll) GT 15 THEN BEGIN
	     	bmax=bmax+0.2
		bmin=bmin-.03
	    ENDIF

    END
    'euvi':  BEGIN
        euvi_flag=1
	IF ~keyword_set(NOROTATE) THEN dorotate=1
	;domask=1
	docolor=1
	ratio=0
	noexpcorr=0
	doautomax=1
	; scaling done in scc_bytscl.pro below
    END
    ELSE:  BEGIN
        PRINT,'Unrecognized telescope code: '+cam
        RETURN,-1
    END
ENDCASE
IF n_elements(mm) GT 1 THEN BEGIN
    bmin=mm[0]
    bmax=mm[1]
ENDIF
IF keyword_set(FITS) THEN noscale=1
IF keyword_set(NOSCALE) THEN bmin=bmax
IF keyword_set(AUTOMAX) THEN doautomax=1
i=0
fname=infile(i)
box=0
im=SCC_MKFRAME(fname,tai_all,exp_all,bias_all,rolld,fpwvl_all,filter_all, $
                      all_hdr,i,startind,img_xy,dofull,bmin,bmax,cam, $
                      box_ref,r_occ,r_occ_out,sunc,r_sun, $
                      linethickness, pan, hsize, vsize, coords, $
                      yloc,win_index,camt,xouts,youts,mask_occ, sunxcen, sunycen, $
                      lg_mask_occ,diff,hide_pylon,use_model,box,no_normal,noexpcorr, $
                      ratio,use_sharp,sharp_fact,sqrt_scl,log_scl,running_diff,unsharp,lee_filt, $
                      fill_col,fixgaps,outermask,limb,tiff,png,pict,times,ftimes,dbiasx, $
                      bad_skip,logo,sdir,save,tlen,nocal,skipped, $
                      docolor,secchiprepmask,dorotate, DOMEDIAN=domedian, $
		      AUTOMAX=doautomax, FMASK=filemask, /nopop , _EXTRA=_extra)




outhdr=all_hdr

outsizex=pan*all_hdr.naxis1*(2^(all_hdr.summed-1))
outsizey=pan*all_hdr.naxis2*(2^(all_hdr.summed-1))

maxmin,im
if keyword_set(DEBUG) THEN wait,3
IF max(im) EQ 0 THEN return,-1

IF ~nopop THEN BEGIN
    qysiz=512*float(all_hdr.naxis2)/all_hdr.naxis1
    window,1,xsize=512,ysize=qysiz
    ;maxmin,im
    help,bmax,bmin
    tv,rebin(im,512,qysiz)
    ;wait,2
ENDIF

IF keyword_set(outsize) then im=rebin(im,hsize,vsize)

IF keyword_set(PNG) THEN BEGIN
    break_file,fname,a, dir, root, ext
    pngfname=root+'.png'
    IF keyword_set(SAVEDIR) THEN outfile=concat_dir(savedir,pngfname) ELSE outfile=concat_dir('.',pngfname)

    tvlct,r,g,b,/get
    print,'Writing ',outfile
    write_png,outfile,im,r,g,b
ENDIF

IF keyword_set(FITS) THEN BEGIN
    break_file,fname,a, dir, root, ext
    ffname=root+'.fts'
    strput,ffname,'2',16
    
    IF keyword_set(SAVEDIR) THEN outfile=concat_dir(savedir,ffname) ELSE outfile=concat_dir('.',ffname)
    sccwritefits, outfile, im, all_hdr
ENDIF


return,im

END
