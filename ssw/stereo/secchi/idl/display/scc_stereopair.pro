;+
; $Id: scc_stereopair.pro,v 1.7 2010/03/09 22:52:54 thompson Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	SCC_STEREOPAIR
;
; Purpose     :	Create stereo image pair from two SECCHI files
;
; Category    :	STEREO, SECCHI, Registration
;
; Explanation :	This procedure creates coaligned image pairs from the two
;               STEREO spacecraft.  Each image is read in, processed, and
;               rescaled to a common center and image size.  The orientation of
;               the images are aligned for proper stereo viewing.  The output
;               can be returned either as left-right pairs, or as anaglyph
;               images.
;
; Syntax      :	Output = SCC_STEREOPAIR( FILES_AHEAD, FILES_BEHIND )
;
;               or
;
;               Output = SCC_STEREOPAIR( IMAGE_AHEAD, IMAGE_BEHIND, $
;                               INDEX_AHEAD, INDEX_BEHIND )
;
; Examples    :	FILES_AHEAD  = '20070321_122811_s4euA.fts'
;               FILES_BEHIND = '20070321_122811_s4euB.fts'
;               Output = SCC_STEREOPAIR(FILES_AHEAD, FILES_BEHIND, /ANAGLYPH)
;               EXPTV, Output, TRUE=1
;
;               SECCHI_PREP, FILES_AHEAD,  INDEXA, IMAGEA
;               SECCHI_PREP, FILES_BEHIND, INDEXB, IMAGEB
;               Output = SCC_STEREOPAIR( IMAGEA, IMAGEB, INDEXA, INDEXB )
;
; Inputs      :	FILES_AHEAD  = List of filenames for the Ahead observatory
;               FILES_BEHIND = List of filenames for the Behind observatory
;
;               The filenames must be coordinated--i.e. a Behind image for
;               every Ahead image, and vice versa.
;
; Opt. Inputs :	Instead of passing the filenames, one can pass in images and
;               indices which have already been read in.  In that case, the
;               calling parameters are:
;
;               IMAGE_AHEAD  = Image(s) for the Ahead observatory
;               IMAGE_BEHIND = Image(s) for the Behind observatory
;               INDEX_AHEAD  = FITS index structure(s) for Ahead
;               INDEX_BEHIND = FITS index structure(s) for Behind
;
; Outputs     :	The result of the function is an array of images with
;               dimensions [2,Nx,Ny,Ni] where Output[0,*,*,*] are the left-eye
;               (Behind) images, and Output[1,*,*,*] are the right-eye (Ahead)
;               images.  Nx, Ny are the dimensions of each image, and Ni is the
;               total number of image pairs.
;
;               When used with the /ANAGLYPH keyword, the output has the
;               dimensions [3,Nx,Ny,Ni] where the first dimension represents
;               the colors red, green, and blue.
;
;               For historical reasons, the order of the output is reversed
;               from the order of the input.  To make the order match, use the
;               /REVERSE keyword.
;
; Opt. Outputs:	INDEX_AHEAD  = Array of FITS index structures for the Ahead
;                              images.
;               INDEX_BEHIND = Same for the Behind images.
;
;               When images are passed instead of filenames, these are input
;               parameters rather than output parameters.
;
; Keywords    :	ANAGLYPH = If set, then return an anaglyph image rather than
;                          left-right pairs.
;
;               REVERSE   = If set, reverse the order of the output to be
;                           right-left instead of left-right.  Ignored if
;                           /ANALGLYPH is set.
;
;               INTERP   = Interpolation type, 0=nearest neighbor, 1=bilinear
;                          interpolation (default), 2=cubic interpolation.
;
;               CUBIC    = Interpolation parameter for cubic interpolation.
;                          See the IDL documentation for POLY_2D for more
;                          information.
;
;               NOMISSING = If set, then don't filter out missing pixels around
;                           the edge of the CCD.
;
;               SECCHIPREP= If set, call SECCHI_PREP.  This is the default,
;                           unless SECCHIPREP=0 is passed.
;
; Calls       :	SCCREADFITS, FITSHEAD2WCS, WCS_GET_PIXEL, GET_STEREO_ROLL,
;               WCS2FITSHEAD, SECCHI_PREP, FITSHEAD2STRUCT, DEF_SECCHI_HDR
;
; Common      :	None.
;
; Restrictions:	Currently only works for STEREO Ahead and Behind images.  Does
;               not (yet) work for combinations of STEREO and other
;               observatories (e.g. SOHO).
;
; Side effects:	The returned index structures are updated to reflect the
;               image manipulations.
;
; Prev. Hist. :	None.
;
; Written     :	29-Aug-2006, William Thompson, GSFC
;
; $Log: scc_stereopair.pro,v $
; Revision 1.7  2010/03/09 22:52:54  thompson
; Added calls to FITSHEAD2STRUCT, DEF_SECCHI_HDR.
; Made /SECCHIPREP the default.
;
; Revision 1.6  2007/09/04 19:19:37  thompson
; Fixed bug
;
; Revision 1.5  2007/08/03 22:25:54  thompson
; Added REVERSE keyword.  Treat secondary coordinate system.
;
; Revision 1.4  2007/08/03 19:55:36  thompson
; Corrected bug with wrong roll angle being returned in header structures.
; Take telescope roll relative to spacecraft frame into account
;
; Revision 1.3  2007/07/02 22:32:51  thompson
; Add SECCHIPREP keyword, option of passing in images and headers
;
; Revision 1.2  2007/06/28 22:09:03  thompson
; *** empty log message ***
;
; Revision 1.1  2006/09/27 17:04:06  nathan
; moved from dev/display
;
; Revision 1.5  2006/09/26 21:45:18  nathan
; add CVS keywords, Written label
;
;               Version 3, 25-Sep-2006, William Thompson, GSFC
;                       Split INDEX into INDEX_BEHIND and INDEX_AHEAD
;               Version 2, 19-Sep-2006, William Thompson, GSFC
;                       Added output parameter INDEX
;
; Contact     :	WTHOMPSON
;-
;
function scc_stereopair, files_ahead, files_behind, index_ahead, index_behind,$
                         anaglyph=anaglyph, interp=interp, reverse=reverse, $
                         secchiprep=secchiprep, _extra=_extra
;
if n_elements(secchiprep) eq 0 then secchiprep = 1
;
;  Check whether the filenames or image arrays were passed.  Check on the total
;  number of parameters. passed.  Also check on the number of filenames or
;  images passed.
;
names_passed = datatype(files_ahead,1) eq 'String'
if names_passed then begin
    if n_params() lt 2 then message, $
      'Syntax: Result = SCC_STEREOPAIR(FILES_AHEAD, FILES_BEHIND)
    n_files = n_elements(files_ahead)
    if n_files ne n_elements(files_behind) then message, $
      'Arrays FILES_AHEAD and FILES_BEHIND do not agree'
end else begin
    if n_params() lt 4 then message, $
      'Syntax: Result = SCC_STEREOPAIR(IMAGE_A, IMAGE_B, INDEX_A, INDEX_B)
    sza = size(files_ahead)
    case sza[0] of
        2:  n_files = 1
        3:  n_files = sza[3]
        else: message, 'IMAGE_AHEAD must have 2 or 3 dimensions'
    endcase
    szb = size(files_behind)
    case szb[0] of
        2:  n_fil_b = 1
        3:  n_fil_b = sza[3]
        else: message, 'IMAGE_BEHIND must have 2 or 3 dimensions'
    endcase
    if n_files ne n_fil_b then message, $
      'Arrays IMAGE_AHEAD and IMAGE_BEHIND do not agree'
endelse
if n_files eq 0 then message, 'No filenames or images passed'
;
;  Get the INTERP value to pass to POLY_2D.
;
if n_elements(interp) eq 0 then interp = 1
;
;  Read the first Ahead image header, or copy the original headers into new
;  arrays.
;
if names_passed then begin
    if keyword_set(secchiprep) then $
      secchi_prep, files_ahead[0], header, dummy, _extra=_extra else $
      dummy = sccreadfits(files_ahead[0], header, /nodata)
end else begin
    header_ahead  = index_ahead
    header_behind = index_behind
    header = header_ahead[0]
endelse
;
;  Get the solar distance, the pixel spacing, and the pixel location of Sun
;  center.  All the other images will be matched to this.
;
wcs = fitshead2wcs(header)
dsun_obs0 = wcs.position.dsun_obs
cdelt0 = wcs.cdelt
pixel0 = wcs_get_pixel(wcs, [0,0])
;
;  Create the arrays of index structures to return.
;
if n_params() gt 2 then begin
    iindex = wcs2fitshead(wcs,old=header,/add_xcen,/add_roll)
    iindex = fitshead2struct(iindex, def_secchi_hdr(), /dash2underscore)
    index_ahead  = replicate(iindex, n_files)
    index_behind = index_ahead
endif
;
;  Read the images.
;
if names_passed then begin
    if keyword_set(secchiprep) then begin
        secchi_prep, files_ahead[*],  header_ahead,  image_ahead,  $
          _extra=_extra
        secchi_prep, files_behind[*], header_behind, image_behind, $
          _extra=_extra
    end else begin
        image_ahead  = sccreadfits(files_ahead[*],  header_ahead)
        image_behind = sccreadfits(files_behind[*], header_behind)
    endelse
endif
;
;  Create the output array.
;
if names_passed then sz = size(image_ahead) else sz = size(files_ahead)
type = sz[sz[0]+1]
if keyword_set(anaglyph) then dim = [3,sz[1:2]] else dim = [2,sz[1:2]]
if n_files gt 0 then dim = [dim, n_files]
output = make_array(type=type, dimension=dim)
;
;  Select a missing value, based on the datatype.
;
case type of
    4: missing = !values.f_nan
    5: missing = !values.d_nan
    else: missing = 0
endcase
;
;  Resample each image so that each has the same Sun center and scale.  Rotate
;  the image by the roll angle to the STEREO Mission Plane coordinate system.
;
p = dblarr(2,2)
q = dblarr(2,2)
obs = ['Behind', 'Ahead']
for j=0,1 do begin
    if keyword_set(reverse) and (not keyword_set(anaglyph)) $
      then jj = 1-j else jj = j
    for i=0,n_files-1 do begin
        if j eq 0 then begin
            header = header_behind[i]
            if names_passed then image = image_behind[*,*,i] else $
              image = files_behind[*,*,i]
        end else begin
            header = header_ahead[i]
            if names_passed then image = image_ahead[*,*,i] else $
              image = files_ahead[*,*,i]
        endelse
        wcs = fitshead2wcs(header)
        pixel = wcs_get_pixel(wcs, [0,0])
        scale = (cdelt0 / wcs.cdelt) * (dsun_obs0 / wcs.position.dsun_obs)
        roll = get_stereo_roll(wcs.time.observ_date, obs[j], system='STPLN') $
             - get_stereo_roll(wcs.time.observ_date, obs[j], system='RTN') $
             - wcs.roll_angle
        cosr = cos(roll * !dpi / 180.d0)
        sinr = sin(roll * !dpi / 180.d0)
;
;  Call POLY_2D to resample the image.
;
        p[0,0] = pixel[0] - scale[0]*pixel0[0]*cosr + scale[1]*pixel0[1]*sinr
        q[0,0] = pixel[1] - scale[0]*pixel0[0]*sinr - scale[1]*pixel0[1]*cosr
        p[0,1] =  scale[0]*cosr
        p[1,0] = -scale[1]*sinr
        q[0,1] =  scale[1]*sinr
        q[1,0] =  scale[0]*cosr
        output[jj,*,*,i] = poly_2d(image, p, q, interp, missing=missing, $
                                   _extra=_extra)
;
;  Modify the WCS structure to reflect the new image scale.
;
        if n_params() gt 2 then begin
            wcs.crpix = pixel0
            wcs.crval[*] = 0
            wcs.cdelt = wcs.cdelt * scale
            wcs.roll_angle = wcs.roll_angle + roll
            cos_a = cos(wcs.roll_angle * !dpi / 180.d0)
            sin_a = sin(wcs.roll_angle * !dpi / 180.d0)
            lambda = wcs.cdelt[1] / wcs.cdelt[0]
            wcs.pc[0,0] = cos_a
            wcs.pc[0,1] = -lambda * sin_a
            wcs.pc[1,0] = sin_a / lambda
            wcs.pc[1,1] = cos_a
            iindex = wcs2fitshead(wcs, old=header, /add_xcen, /add_roll, $
                                  /structure)
            if j eq 0 then temp = index_behind[i] else temp = index_ahead[i]
            struct_assign, iindex, temp
;
;  Update the secondary coordinate system.
;
            wcsa = fitshead2wcs(header, system='a')
            origina = wcs_get_coord(wcsa, pixel)
            temp.crpix1a = temp.crpix1
            temp.crpix2a = temp.crpix2
            temp.crval1a = origina[0]
            temp.crval2a = origina[1]
            temp.cdelt1a = temp.cdelt1a * scale[0]
            temp.cdelt2a = temp.cdelt2a * scale[1]
            rolla = wcsa.roll_angle - roll
            cos_a = cos(rolla * !dpi / 180.d0)
            sin_a = sin(rolla * !dpi / 180.d0)
            temp.pc1_1a = cos_a
            temp.pc1_2a = -lambda * sin_a
            temp.pc2_1a = sin_a / lambda
            temp.pc2_2a = cos_a
;
;  Store the modified header.
;       
            if j eq 0 then index_behind[i] = temporary(temp) else $
              index_ahead[i] = temporary(temp)
        endif
    endfor
endfor
;
;  If the /ANAGLYPH keyword was set, then set the blue and green images equal
;  to each other.
;
if keyword_set(anaglyph) then output[2,*,*,*] = output[1,*,*,*]
;
return, output
end
