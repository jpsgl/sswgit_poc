;
; Project   : developer area for STEREO SECCHI
;                   
; Name      : scc_crop
;               
; Purpose   : To crop a STEREO Cor FITS File and save as SCC FITS
;               
; Explanation: This routine takes a STEREO FITS file plus the desired
;              new center location, and crops it around that center.
;              It saves the result as a FITS file.
;                
; Use       : IDL> scc_crop,filename,newcenter[,factor=factor]
;    
; Inputs    : FILENAME: The STEREO FITS file to be cropped
;
;             NEWCENTER: A 2-element array giving the pixel
;             coordinates of the new cropped image center
;
; Outputs   : a FITS file
;
; Keywords  : FACTOR: Optional input on how small to crop down to,
;             defaults to a factor of , i.e. the output image will be
;             N/2 x N/2 the size of the N x N input image.
;               
; Calls from LASCO : none 
;
; Common    : none (some in scc_measure)
;               
; Restrictions: none
;               
; Side effects: 
;               
; Category    : display, ssc
;               
; Prev. Hist. : None.
;
; Written     : Sandy Antunes, NRL, May 2007


PRO scc_crop_example

    ; Example of how to recenter and crop a STEREO pair
  
  fa='20070519_235256_s4c2A.fts'
  fb='20070519_235256_s4c2B.fts'
  cachefile='cubebuffer'
  
    ;ia=sccreadfits(fa,hdra)
    ;ib=sccreadfits(fb,hdrb)
  
    ; CHOOSE NEW CENTER
    ; remember to hit 'store' with this one
  scc_measure,fa,fb,outfile=cachefile,/forcesave,/crop
    ; stores the lat, lon and Rsun out, and pixel positions clicked in the image
    ; now read our clicked result
  data=read_ascii(cachefile)
    ; grab values from last row
  if (size(data.field1,/n_dimensions) eq 1) then myrow=data.field1 else $
      myrow=(data.field1)[*,(size(data.field1))[2]-1]
  newcenters=myrow[3:6]
  
  scc_crop,fa,newcenters[0:1]; ,factor=2
  scc_crop,fb,newcenters[2:3]; ,factor=2

  fnewa=fa
  strput, fnewa,'R',17
  imagea=sccreadfits(fnewa,hdra)
  fnewb=fb
  strput, fnewb,'R',17
  imageb=sccreadfits(fnewb,hdrb)

  tv_multi,[[[imagea]],[[imageb]]],res=0.2,labelset=['A','B']

END

PRO scc_crop,fname,newcenter,factor=factor,image

  if (n_elements(fname) eq 0 or n_elements(newcenter) lt 2) then begin
      print,'Usage is: scc_crop,fname,newcenter[,factor=factor'
      return
  end

  if (n_elements(factor) eq 0) then factor=[2,2]; default is half size it
  if (n_elements(factor) eq 1) then factor=[factor,factor]

  ; get our original headers and info
  image=sccreadfits(fname,hdr)

  origcenter=[hdr.NAXIS1/2,hdr.NAXIS2/2]

  ; find delta then shift center (note sun location is just an offset
  ; and need not be changed)
  shift=origcenter-newcenter

  ; ADJUST HEADER
  ; New size
  origNAXIS=[hdr.NAXIS1,hdr.NAXIS2]
  hdr.NAXIS1=hdr.NAXIS1/factor[0]
  hdr.NAXIS2=hdr.NAXIS2/factor[1]
  print,hdr.NAXIS1
  ; Relocate our optical center
  hdr.CRPIX1 = hdr.CRPIX1 - shift[0]
  hdr.CRPIX2 = hdr.CRPIX2 - shift[1]
  hdr.CRPIX1A = hdr.CRPIX1A - shift[0]
  hdr.CRPIX2A = hdr.CRPIX2A - shift[1]
  ; (sun center is offset from optical center so no change needed)

  ; Change A processing field (e.g. C/3/4/5/7) to 'R' for 'reduced/cropped'
  pname=hdr.FILENAME
  strput, pname,'R',17
  hdr.FILENAME=pname

  ; now crop
  dx=origcenter[0]/factor[0]
  dy=origcenter[1]/factor[1]

  ; image area
  xmin=newcenter[0]-dx
  xmax=newcenter[0]+dx-1
  ymin=newcenter[1]-dy
  ymax=newcenter[1]+dy-1
  image=image[xmin:xmax,ymin:ymax]

;DSTART1, DSTOP1, DSTART2, DSTOP2
;  Indicates the first (last) column (row) of the iimage area on the data array
  hdr.DSTART1 = 1
  hdr.DSTOP1 = hdr.NAXIS1
  hdr.DSTART2 = 1
  hdr.DSTOP2 = hdr.NAXIS2
;R1COL, R2COL, R1ROW, R2ROW
;  The rectified begin (end) X-coordinate, as though rectification had
;      been unnecessary.  If RECTIFY if F, equals P1(2)COL(ROW)
  hdr.R1COL = hdr.R1COL + xmin*2^(hdr.summed-1)
  hdr.R2COL = hdr.R1COL+(2*dx*2^(hdr.summed-1))-1
  hdr.R1ROW = hdr.R1ROW + ymin*2^(hdr.summed-1)
  hdr.R2ROW = hdr.R1ROW + (2*dy*2^(hdr.summed-1))-1
;P1COL, P2COL, P1ROW, P2ROW
;  CCD col(row) number of start (end) of image
  pid=hdr.obsrvtry + hdr.detector
  if (pid eq 'STEREO_ACOR2' or pid eq 'STEREO_BCOR1') then begin
      hdr.P1COL = hdr.R1ROW 
      hdr.P2COL = hdr.R2ROW 
      hdr.P1ROW = 2176-hdr.R2COL+1
      hdr.P2ROW = 2176-hdr.R1COL+1

  end else if (pid eq 'STEREO_BCOR2' or pid eq 'STEREO_ACOR1') then begin
      hdr.P1COL = 2176-hdr.R2ROW+1
      hdr.P2COL = 2176-hdr.R1ROW+1
      hdr.P1ROW = hdr.R1COL 
      hdr.P2ROW = hdr.R2COl
Print, hdr.P1COL,hdr.P2COL,hdr.P1ROW,hdr.P2ROW
  end else begin
      ; not sure, no rectify
      hdr.P1COL=hdr.R1COL
      hdr.P2COL=hdr.R2COL
      hdr.P1ROW=hdr.R1ROW
      hdr.P2ROW=hdr.R2ROW
  end

  ; save
  sccwritefits,hdr.filename,image,hdr,comments='Cropped image'

END

; now convert to cube coords:

;L0= avg sun-satellite distance (e.g. 1 AU converted to Solar Radii)
;L0tau = distance subtended by a pixel at L0 in solar radii
;lla0 = existing sun center in image coordinates
;
;d = length of voxel edge/(L0*tau) (so is this voxel edge in Solar Radii?)
;    in which case d/N is length of a single voxel in Solar Radii?
;
;Convert HAE km to units of AU and apply to voxel space:
; lla0 = HAE + delta * d/N
