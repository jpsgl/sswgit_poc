pro secchi_image, filenames,headers,images,savepath=savepath,outsize=outsize,$
polariz_on=polariz_on,logo_off=logo_off,date_off=date_off,$
write_png=write_png,write_jpg=write_jpg,silent=silent,$
whisper=whisper,color_off=color_off,_extra=ex
;+
; $Id: secchi_image.pro,v 1.7 2008/01/28 21:07:33 colaninn Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : secchi_image
;               
; Purpose   : Creates display quality images 
;               
; Explanation: Reads in the filename of a secchi pre-flight level 0.5
;              data and process data for dispaly
;               
; Use       : IDL> secchi_image,filename,headers,images 
;    
; Inputs    : filenames - of SECCHI data
;
; Output    :headers - array of header sturctures
;
;            images - array of processed image
;
; Keywords  :savepath - if any write_* is set and  you would like to
;                        save the image to a directory other then the
;                        current, set equal to the full path.
;            outsize - size of output array, if not set output array
;                       is the mode of the image sizes
;            polariz_on - COR2 ONLY! Set this keyword if the 
;                       input image are not Total brightness images
;            logo_off - set to return image with no logo stamp
;            date_off - set to return image with no date-time stamp
;            write_png - if set the program writes image to png file
;            write_jpg - if set the program writes image to jpg file
;            silent - set to turn off command line messages
;            whisper - only reports image processing progress
;            color_off - return images in grayscale
;
; Restrictions : does not work for COR1 images
;                does not work for double total brightness images
;
; Calls     : SCCREADFITS, SCC_IMG_TRIM, COR_PREP, EUVI_PREP, SCC_PUTIN_ARRAY
;             COR_POLARIZ, SCC_BYTSCL, SCC_GETBKGIMG, SCC_ADD_LOGO, 
;             SCC_ADD_DATETIME
;               
; Category    : Display
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU July 2007
;
; $Log: secchi_image.pro,v $
; Revision 1.7  2008/01/28 21:07:33  colaninn
; added sub-routine scc_make_array.pro
;
; Revision 1.6  2007/08/21 19:30:29  reduce
; Added color_off kw. Karl B
;
; Revision 1.5  2007/07/11 21:29:30  colaninn
; added color keyword to scc_add_*
;
; Revision 1.4  2007/07/11 17:21:48  colaninn
; change default outsize to mode of input
;
; Revision 1.3  2007/07/05 19:24:03  colaninn
; added whisper keyword and changed cor2 bin
;
; Revision 1.2  2007/07/03 20:28:37  colaninn
; added silent keyword to scc_getbkgimg
;
; Revision 1.1  2007/07/03 20:15:26  colaninn
; BETA Release
;
;-

;----------------------------------------------------------------------
;----------------------------------------------------------------------
;--Check validity of input parameters and keywords---------------------
;----------------------------------------------------------------------
  IF datatype(ex) EQ 'UND' THEN ex = create_struct('blank',-1)
  little_endian = (BYTE(1, 0, 1))[0]
  circle=0

  ;--Check for WRITE_ON keyword
  write_flag = 0
  IF keyword_set(write_png) THEN write_flag = 1
  IF keyword_set(write_jpg) THEN write_flag = 1 
  IF NOT write_flag AND n_params() EQ 1 THEN $
   message, 'Keyword WRITE_* must be set when no parameters are entered'

  ;--Pass SILENT keyword to subroutines
  IF keyword_set(silent) THEN report = 0 ELSE report =1
  IF keyword_set(whisper) THEN silent = 1
  IF keyword_set(silent) THEN ex = CREATE_STRUCT('silent',1,ex)
 
  ;--Check for SAVEPATH keyword
  IF keyword_set(savepath) THEN BEGIN
    savepath = file_search(savepath,/test_directory,/test_write,count=pflag)
    DEFSYSV, '!DELIMITER', EXISTS = df  
    IF df NE 1 THEN sl=get_delim() ELSE sl=!delimiter
    IF pflag EQ 1 THEN savepath=savepath+sl ELSE $
     message,'Error finding entered SAVEPATH'
  ENDIF
  ;--For Polariz_on
  ;polariz_on = 0
  pn=3
;----------------------------------------------------------------------
;----------------------------------------------------------------------
;--Create array to return images to memory-----------------------------
;----------------------------------------------------------------------
  IF keyword_set(OUTSIZE) THEN outsize0=outsize
  
  images = scc_make_array(filenames,headers,outout,outsize=outsize0,_extra=ex)
  ex = create_struct(ex,'outsize',outout)

  ;--Check for POLARIZ_ON keyword
  IF keyword_set(polariz_on) THEN BEGIN
    ss = size(images,/dim)
    seq_hdr = replicate(def_secchi_hdr(silent=silent),pn)
    ex = create_struct(ex,'polariz_on',1)
  ENDIF ELSE seq=0 
  polariz_count=0
;----------------------------------------------------------------------
;----------------------------------------------------------------------
;--Loop to process each image------------------------------------------ 
;----------------------------------------------------------------------
  num = n_elements(filenames)
  str_num = strtrim(string(num),2)

  FOR i=0, num-1 DO BEGIN 
    ;--Allows SECCHI_IMAGE to continue on errors
     CATCH, error_status
     IF error_status NE 0 THEN BEGIN
        print, 'ERROR WHILE PROCESSING '+hdr.filename+' IMAGE SKIPPED'
        help, /Last_Message, Output=traceback  
        Print, 'Traceback Report from SECCHI_IMAGE :'
        FOR j=0,N_Elements(traceback)-2 DO Print, "     " + traceback[j]
        !ERROR_STATE.CODE =0
        IF keyword_set(polariz_on) THEN BEGIN
          i = i+(pn-polariz_count)
          polariz_count = 0
        ENDIF ELSE i=i+1
       IF i GT num-1 THEN RETURN
     ENDIF
 
    IF report THEN message,'Processing image '+strtrim(string(i+1),2)+$
           ' out of '+str_num,/inform
    IF ~keyword_set(silent) THEN message,filenames[i],/inform

    ;--Open image 
    im = SCCREADFITS(filenames[i],hdr,/silent,header=str_hdr)
    spcomments = GET_FITSCOMMENTS(str_hdr,hdr)
    
    ;--Trim image under/over scan
    im = SCC_IMG_TRIM(im,hdr,_extra=ex)
 
    ;--Rescale image to fit in output array
    im = SCC_PUTIN_ARRAY(im,hdr,outsize,_extra=ex)
    
  ;--BEGIN BEAUTIFICATION--------------------------------       

  IF ~KEYWORD_SET(color_off) then loadct,0,silent=silent ; load grayscale color table
  
  ;--Load Telescope Color Table (if desired)
  path = concat_dir(getenv('SSW'),'stereo')
  IF ~KEYWORD_SET(color_off) then begin
        IF hdr.DETECTOR EQ 'EUVI' THEN BEGIN
        color_table = strtrim(string(hdr.WAVELNTH),2)+'_EUVI_color.dat'
        RESTORE, FILEPATH(color_table ,ROOT_DIR=path,$
            SUBDIRECTORY=['secchi','data','color'])
        tvlct, r,g,b
        ENDIF ;ELSE BEGIN 
  ENDIF
;    color_table = strtrim(string(hdr.DETECTOR),2)+'_color.dat'
;  ENDELSE

   CASE hdr.DETECTOR OF
      'EUVI': BEGIN
        EUVI_PREP,hdr,im,/smask_on,/calimg_off,/rotate_on,$
          /update_hdr_off,_extra=ex
        im = scc_bytscl(im,hdr)
       END
      'COR1':BEGIN
        message,/inform,$
        'Pretty picture processing not yet implemented for COR1'
       END
      'COR2': BEGIN     
        IF keyword_set(polariz_on) THEN BEGIN
          cor_prep,hdr,im,/smask_on,/calimg_off,/calfac_off,/update_hdr_off,$
            _extra=ex
          polariz_count = polariz_count+1
          seq = shift(seq,0,0,1)
          seq[*,*,0] = im
           seq_hdr = shift(seq_hdr,1)
           seq_hdr[0]=hdr
           IF  polariz_count EQ pn THEN BEGIN
             im = COR_POLARIZ(seq,seq_hdr,hdr, _extra=ex)
           ENDIF
         ENDIF
         IF ~keyword_set(polariz_on) OR (polariz_count EQ pn) THEN BEGIN
           IF ~KEYWORD_SET(color_off) then loadct,3,silent=silent
           gamma_ct,1.0
           bkg=scc_getbkgimg(hdr,outhdr=bkhdr,_extra=ex)   
           mask = get_smask(hdr)
           im = im*mask    
           im = ratio(im,bkg)
           nz = where(im GT 0)
           m = median(im(nz))
           bmin = m - 0.1;0.2
           bmax = m + 0.1;0.4
           im =bytscl(im,bmin,bmax)
           circle=1
         ENDIF
       END   
      'HI1': BEGIN
         IF ~KEYWORD_SET(color_off) then loadct,1,silent=silent
         GAMMA_CT, 0.6
         HI_PREP,hdr,im,outsize=outout,/update_hdr_off,silent=silent
         bkg=scc_getbkgimg(hdr,outhdr=bkhdr,_extra=ex)
         ;bkg = readfits('~/idl/memin_perez/secchi/20070709/newbkg.fts')
         im = ratio(im,bkg)
         nz = where((im GT 0) AND (im LT 2),nnn)
         m = median(im(nz))
         bmin = m - 0.01;0.05
         bmax = m + 0.01;0.1
         im =bytscl(im,bmin,bmax)
       END
      'HI2': BEGIN
         IF ~KEYWORD_SET(color_off) then loadct,7,silent=silent
         HI_PREP,hdr,im,outsize=outout,/update_hdr_off,silent=silent
         bkg=scc_getbkgimg(hdr,outhdr=bkhdr,_extra=ex)
         im = ratio(im,bkg)
         nz = where((im GT 0) AND (im LT 2))
         m = median(im(nz))
         bmin = m - 0.3
         bmax = m + 1.0
         im =bytscl(im,bmin,bmax)
       END
       ELSE: message,'STEREO SECCHI DETECTOR could not be found'
    ENDCASE
  IF ~keyword_set(logo_off) THEN im=scc_add_logo(im,hdr,color=255)
  IF ~keyword_set(date_off) THEN im=scc_add_datetime(im,hdr,circle=circle,color=255)
  ;----------------------------------------------------
  ;--Fill output array     
  IF n_params() GT 1 THEN BEGIN
    IF keyword_set(polariz_on) THEN BEGIN
      IF (polariz_count EQ pn) THEN BEGIN 
       	p = (i-2)/3.0 ;p is 1 image for every 3 of i
       	headers[p] = hdr 
       	images[*,*,p]=im
       	IF ~write_flag THEN polariz_count=0
       ENDIF
    ENDIF ELSE BEGIN
      headers[i] = hdr 
      images[*,*,i]=im
    ENDELSE
  ENDIF
  ;--Print file to disk
  IF write_flag THEN BEGIN
    pfilename = hdr.filename
    tvlct, r,g,b,/get
    IF keyword_set(savepath) THEN pfilename = savepath+pfilename
    skip_flag = 0
    IF keyword_set(polariz_on) AND (polariz_count NE pn) THEN skip_flag =1
 
    IF NOT skip_flag THEN BEGIN
      CASE 1 OF
        keyword_set(write_png): BEGIN
          pfilename = strmid(pfilename,0,strlen(pfilename)-3)+'png'
          IF ~keyword_set(silent)THEN message,'WRITING '+pfilename,/inform
          IF little_endian THEN BEGIN
            write_png,pfilename,im,r,g,b
          ENDIF ELSE write_png,pfilename,rotate(im,7),r,g,b
        END
        keyword_set(write_jpg): BEGIN
          pfilename = strmid(pfilename,0,strlen(pfilename)-3)+'jpg'
          IF ~keyword_set(silent) THEN message,'WRITING '+pfilename,/inform
          write_jpeg,pfilename,bytscl(im,0,hdr.datap95)
        END
       ENDCASE   
     polariz_count =0
    ENDIF
  ENDIF      
ENDFOR   
IF ~keyword_set(silent) THEN message,'FINISHED!',/inform

;----------------------------------------------------------------------
  
RETURN
END 
