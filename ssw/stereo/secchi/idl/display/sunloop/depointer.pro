;+
; $Id: depointer.pro,v 1.2 2010/01/08 15:37:00 nathan Exp $
;
; depointer.pro
;
; This fuction evaluates the reference given, and 
; frees ALL pointers and destroys ALL objects thoughout 
; any nested arrangement of structures and pointers 
; that are referred to by the reference.
;
; (Objects are responsible for cleaning themselves up, 
; they may call this routine in their own cleanup routine.)
;
; Returns:
;	0 if zero valid pointers or objects were found.
;	1 if one or more pointers were freed or objects destroyed.
;
; Written for the AIRS project, Jan 1999 by:
;	Jeffrey.R.Hall@jpl.nasa.gov
;	Charles.K.Thompson@jpl.nasa.gov
;
; "Copyright 2009, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."
;
; $Log: depointer.pro,v $
; Revision 1.2  2010/01/08 15:37:00  nathan
; committed sunloop_20091117
;
;-

function depointer, reference, VERBOSE = verbose

IF KEYWORD_SET( verbose ) THEN verbose = 1 ELSE verbose = 0

;; size(reference) returns:
;; Type Code	Data Type	
;; 0		Undefined	
;; 1		Byte	
;; 2		Integer	
;; 3		Longword integer	
;; 4		Floating point	
;; 5		Double-precision floating	
;; 6		Complex floating	
;; 7		String	
;; 8		Structure	
;; 9		Double-precision complex	
;; 10		Pointer	
;; 11		Object reference	

success = 0

case size( reference, /type ) of

	;UNDEFINED
	0 : begin
	end

	;STRUCTURE
	8 : begin
		ntags = n_tags( reference )
		tnames = tag_names( reference )
		IF verbose THEN print, 'DEPOINTER:  Recursing on a STRUCTURE with ntags = ', ntags
		for tag = 0, ntags - 1 do begin
			IF verbose THEN print, 'DEPOINTER:     Tag Name = ', tnames[tag]
			success = depointer( reference.( tag ) )
			IF verbose THEN print, 'DEPOINTER:     (STRUCTURE)   Result = ', success
		endfor
	end

	;POINTER
	10 : begin

		; Account for pointer arrays.
		valid_ptrarr_indicies = where( ptr_valid( reference[*] ) )
		IF verbose THEN print,'DEPOINTER:  n_elements( valid_ptrarr_indicies ) = ', n_elements( valid_ptrarr_indicies )
		IF verbose THEN print,'DEPOINTER:  list of valid_ptrarr_indicies = ', valid_ptrarr_indicies
		if valid_ptrarr_indicies[0] ne -1 then begin
			for i = 0, n_elements( valid_ptrarr_indicies ) - 1 do begin
				if (where( size( *(reference[valid_ptrarr_indicies[i]]), /type ) eq [8,10,11] ))[0] ne -1 then begin
					IF verbose THEN print, 'DEPOINTER:  Recursing on a POINTER'
					success = depointer( *(reference[valid_ptrarr_indicies[i]]) )
					IF verbose THEN print, 'DEPOINTER:     (POINTER)   Result = ', success
				endif
				ptr_free, reference[valid_ptrarr_indicies[i]]
				success = 1
				IF verbose THEN print, 'DEPOINTER:  A POINTER WAS FREED'
			endfor
		endif
	end

	;OBJECT
	11 : begin

		; Account for object arrays.
		valid_objarr_indicies = where( obj_valid( reference[*] ) )
		IF verbose THEN print,'DEPOINTER:  n_elements( valid_objarr_indicies ) = ', n_elements( valid_objarr_indicies )
		IF verbose THEN print,'DEPOINTER:  list of valid_objarr_indicies = ', valid_objarr_indicies
		if valid_objarr_indicies[0] ne -1 then begin
			for i = 0, n_elements( valid_objarr_indicies ) - 1 do begin
				obj_destroy, reference[valid_objarr_indicies[i]]
				IF verbose THEN print, 'DEPOINTER:  AN OBJECT WAS DESTROYED'
			endfor
		endif
	end

	;OTHER
	else : begin
	end

endcase

return, success

end
