;+
; $Id: get_closest_tiff_filename.pro,v 1.3 2012/02/22 16:51:06 nathan Exp $
;
; written by:   Jeffrey.R.Hall@jpl.nasa.gov
; written for:  Paulett.C.Liewer@jpl.nasa.gov
;               STEREO/SECCHI Project
;
; "Copyright 2012, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."
;-

FUNCTION get_closest_tiff_filename, date, time, craft, detector, wavelnth

	; Perform a check to see if 'secchi' environment variable is set.
	dir_components=STRSPLIT(SCC_DATA_PATH('a'),PATH_SEP(),/EXTRACT)
	secchi_components_ndx=WHERE(dir_components EQ 'secchi',count)
	IF (count EQ 0) THEN BEGIN
		ddddd=DIALOG_MESSAGE(['Cannot locate TIF, GIF or JPG images.','secchi environment variable directory path does not contain a path element named:  secchi'])
		RETURN,''
	ENDIF
	secchi_tiff_location=GETENV('secchi_tiff_location')
	IF (secchi_tiff_location EQ '') THEN BEGIN
		ddddd=DIALOG_MESSAGE(['Cannot locate TIFF images.','Secchi TIFF location not found.'])
		RETURN,''
	ENDIF

	sc = (['STEREO_A','STEREO_B'])[craft]
	dir = 'tiff'
	detector = STRLOWCASE( detector )

	CASE detector OF
		'euvi' : detector = STRTRIM(wavelnth,2)
		'cor1' : dir = 'tiff_' + detector
		'cor2' : dir = 'tiff_' + detector
		'hi1' : BEGIN
			detector = 'hi_1'
			dir = 'tiff_' + detector
			END
		'hi_1' : BEGIN
			detector = 'hi_1'
			dir = 'tiff_' + detector
			END
		'hi2' : BEGIN
			detector = 'hi_2'
			dir = 'tiff_' + detector
			END
		'hi_2' : BEGIN
			detector = 'hi_2'
			dir = 'tiff_' + detector
			END
	ENDCASE

	find_command='find '+secchi_tiff_location+'/'+dir+'/'+date+'/'+detector+'/'+sc+' -name ''*.tiff'' | sort'
print,find_command
	SPAWN,find_command,list

	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	julian_anchor = (JULIANDAY( date + time ))[0]
	all_diff = ABS( all_juliandays - julian_anchor )
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN ndx = ndx[0] ELSE ndx = 0
	;----------------------------------------------------------------------------------------------
	; Check if ndx is at beginning or end of list, in which case it's necessary rebuild list and 
	; include the previous or next day.  Then the list can be searched for the best matching time.
	;----------------------------------------------------------------------------------------------
	IF (ndx EQ 0) OR (ndx EQ count-1) THEN BEGIN
		julian_check = julian_anchor + ([-1,1])[ndx NE 0]
		CALDAT, julian_check, month, day, year, hour, minute, second
		IF (  year LT 10) THEN   year = '0' + STRTRIM(   year, 2 ) ELSE   year = STRTRIM(   year, 2 )
		IF ( month LT 10) THEN  month = '0' + STRTRIM(  month, 2 ) ELSE  month = STRTRIM(  month, 2 )
		IF (   day LT 10) THEN    day = '0' + STRTRIM(    day, 2 ) ELSE    day = STRTRIM(    day, 2 )
		IF (  hour LT 10) THEN   hour = '0' + STRTRIM(   hour, 2 ) ELSE   hour = STRTRIM(   hour, 2 )
		IF (minute LT 10) THEN minute = '0' + STRTRIM( minute, 2 ) ELSE minute = STRTRIM( minute, 2 )
		IF (second LT 10) THEN second = '0' + STRTRIM( second, 2 ) ELSE second = STRTRIM( second, 2 )
		date_new = year + month + day
		; Rebuild list only if directory for date_new exists.
		IF ( (FILE_SEARCH(secchi_tiff_location+'/'+dir+'/'+date_new+'/'+detector))[0] NE '' ) THEN BEGIN
			find_command='find '+secchi_tiff_location+'/'+dir+'/'+date_new+'/'+detector+'/'+sc+' -name ''*.tiff'' | sort'
print,'recheck'
print,find_command
			SPAWN,find_command,list_new
			list = [ list, list_new ]
		ENDIF
	ENDIF
	;----------------------------------------------------------------------------------------------
	; Now check for the best matching time.
	;----------------------------------------------------------------------------------------------
	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	julian_anchor = JULIANDAY( date + time )
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN ndx = ndx[0] ELSE ndx = 0
	; Use FILE_SEARCH to get the actual correct directory (and get rid of all the '../')
	tiff_filename = (FILE_SEARCH(list[ndx]))[0]

	RETURN, tiff_filename
END
