;+
; $Id: get_closest_fits_filename.pro,v 1.3 2012/02/22 16:51:06 nathan Exp $
;
; written by:   Jeffrey.R.Hall@jpl.nasa.gov
; written for:  Paulett.C.Liewer@jpl.nasa.gov
;               STEREO/SECCHI Project
;
; "Copyright 2012, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."
;-

FUNCTION get_closest_fits_filename, date, time, craft, detector, NO_WARNING_DIALOGS = no_warning_dialogs

	; When used in a batch program, the warning dialogs are unwanted.
	IF KEYWORD_SET( no_warning_dialogs ) THEN no_warning_dialogs = 1 ELSE no_warning_dialogs = 0

	sc = (['a','b'])[craft]
	detector = STRLOWCASE( detector )

	number	= '4'
	CASE detector OF
		'304' : BEGIN
			image_type	= 'img'
			detector	= 'euvi'
			number		= '[4-5]'
			END
		'euvi' : BEGIN
			image_type	= 'img'
			number		= '[4-5]'
			END
		'cor1' : image_type = 'seq'
		'cor2' : image_type = 'img'
		'hi_1' : image_type = 'img'
		'hi_2' : image_type = 'img'
	ENDCASE

	dir=SCC_DATA_PATH(sc, TYPE=image_type, TELESCOPE=detector, DATE=date)
;	dir=GETENV('secchi')+PATH_SEP()+'lz'+PATH_SEP()+'L0'+PATH_SEP()+sc+PATH_SEP()+image_type+PATH_SEP()+detector+PATH_SEP()+date
	list=FILE_SEARCH(dir,'*.fts',/FULLY_QUALIFY_PATH,COUNT=file_count)
	; Weed out any occurances of '_s7h'
	ndx=WHERE(STRPOS(list,'_s7h') EQ -1,ndx_count)
	IF (ndx_count GT 0) THEN list=list[ndx]
	; Weed out any occurances of '_n4h'
	ndx=WHERE(STRPOS(list,'_n4h') EQ -1,ndx_count)
	IF (ndx_count GT 0) THEN list=list[ndx]
	; Weed out any occurances of '_k4h'
	ndx=WHERE(STRPOS(list,'_k4h') EQ -1,ndx_count)
	IF (ndx_count GT 0) THEN list=list[ndx]
	; Sort the list.
	list=list[SORT(list)]

	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	julian_anchor = JULIANDAY( date + time )
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN ndx = ndx[0] ELSE ndx = 0
	fits_filename = list[ndx]

	IF (ndx EQ 0) OR (ndx EQ count-1) THEN BEGIN
		julian_check = julian_anchor + ([-1,1])[ndx NE 0]
		CALDAT, julian_check, month, day, year, hour, minute, second
		IF (  year LT 10) THEN   year = '0' + STRTRIM(   year, 2 ) ELSE   year = STRTRIM(   year, 2 )
		IF ( month LT 10) THEN  month = '0' + STRTRIM(  month, 2 ) ELSE  month = STRTRIM(  month, 2 )
		IF (   day LT 10) THEN    day = '0' + STRTRIM(    day, 2 ) ELSE    day = STRTRIM(    day, 2 )
		IF (  hour LT 10) THEN   hour = '0' + STRTRIM(   hour, 2 ) ELSE   hour = STRTRIM(   hour, 2 )
		IF (minute LT 10) THEN minute = '0' + STRTRIM( minute, 2 ) ELSE minute = STRTRIM( minute, 2 )
		IF (second LT 10) THEN second = '0' + STRTRIM( second, 2 ) ELSE second = STRTRIM( second, 2 )
		date_new = year + month + day
		dir_new=SCC_DATA_PATH(sc, TYPE=image_type, TELESCOPE=detector, DATE=date_new)
;		dir_new=GETENV('secchi')+PATH_SEP()+'lz'+PATH_SEP()+'L0'+PATH_SEP()+sc+PATH_SEP()+image_type+PATH_SEP()+detector+PATH_SEP()+date_new
		list_new=FILE_SEARCH(dir_new,'*.fts',/FULLY_QUALIFY_PATH,COUNT=file_count)
		; Weed out any occurances of '_s7h'
		ndx=WHERE(STRPOS(list_new,'_s7h') EQ -1,ndx_count)
		IF (ndx_count GT 0) THEN list_new=list_new[ndx]
		; Weed out any occurances of '_n4h'
		ndx=WHERE(STRPOS(list_new,'_n4h') EQ -1,ndx_count)
		IF (ndx_count GT 0) THEN list_new=list_new[ndx]
		; Weed out any occurances of '_k4h'
		ndx=WHERE(STRPOS(list_new,'_k4h') EQ -1,ndx_count)
		IF (ndx_count GT 0) THEN list_new=list_new[ndx]
		; Sort the list_new.
		list_new=list_new[SORT(list_new)]
		list = [ list , list_new ]
	ENDIF
	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN ndx = ndx[0] ELSE ndx = 0
	fits_filename = list[ndx]

	IF (no_warning_dialogs EQ 0) THEN BEGIN
		diff_hours = (min_diff*24.0)
		thresh_hours = 6.0
		IF (diff_hours GT thresh_hours) THEN BEGIN
			diff_string = STRTRIM(diff_hours,2)
			WHILE (STRMID( diff_string, 0, 1, /REVERSE ) EQ '0') DO diff_string = STRMID( diff_string, 0, STRLEN( diff_string ) - 1 )
			thresh_string = STRTRIM(thresh_hours,2)
			WHILE (STRMID( thresh_string, 0, 1, /REVERSE ) EQ '0') DO thresh_string = STRMID( thresh_string, 0, STRLEN( thresh_string ) - 1 )
			IF (STRMID( thresh_string, 0, 1, /REVERSE ) EQ '.') THEN thresh_string = thresh_string + '0'
			ddddd=DIALOG_MESSAGE([	'Frustrum for this image may appear to wander from main set of frustrums.', $
									'FITS file is '+diff_string+' hours different from the master time.', $
									'(Threshold for this warning is '+STRTRIM(thresh_string,2)+' hours.)', $
									'', $
									'Master time = '+date+'_'+time, $
									'FITS file = '+FILE_SEARCH(fits_filename)	])
		ENDIF
	ENDIF

print,'RETURN, fits_filename = ',fits_filename
	RETURN, fits_filename
END
