;+
; $Id: get_closest_fits_filename_2.pro,v 1.1 2012/02/22 16:51:06 nathan Exp $
;
; written by:   Jeffrey.R.Hall@jpl.nasa.gov
; written for:  Paulett.C.Liewer@jpl.nasa.gov
;               STEREO/SECCHI Project
;
; "Copyright 2012, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."
;-

FUNCTION get_closest_fits_filename_2, date, time, craft, detector, NO_WARNING_DIALOGS = no_warning_dialogs

	; When used in a batch program, the warning dialogs are unwanted.
	IF KEYWORD_SET( no_warning_dialogs ) THEN BEGIN
		no_warning_dialogs = 1
	ENDIF ELSE BEGIN
		no_warning_dialogs = 0
	ENDELSE

	; Translate spacecraft from 0|1 to a|b.
	sc = (['a','b'])[craft]

	; Ensure detector is lower case.
	detector = STRLOWCASE( detector )

	CASE detector OF
		'171' : BEGIN
			extra		= {QUADRANT:detector}
			detector	= 'euvi'
			image_type	= 'img'
			END
		'195' : BEGIN
			extra		= {QUADRANT:detector}
			detector	= 'euvi'
			image_type	= 'img'
			END
		'284' : BEGIN
			extra		= {QUADRANT:detector}
			detector	= 'euvi'
			image_type	= 'img'
			END
		'304' : BEGIN
			extra		= {QUADRANT:detector}
			detector	= 'euvi'
			image_type	= 'img'
			END
		'euvi' : BEGIN
			image_type	= 'img'
			END
		'cor1' : image_type = 'seq'
		'cor2' : image_type = 'img'
		'hi_1' : image_type = 'img'
		'hi_2' : image_type = 'img'
	ENDCASE

	dir=SCC_DATA_PATH(sc, TYPE=image_type, TELESCOPE=detector, DATE=date)
;	dir=GETENV('secchi')+PATH_SEP()+'lz'+PATH_SEP()+'L0'+PATH_SEP()+sc+PATH_SEP()+image_type+PATH_SEP()+detector+PATH_SEP()+date

	list=SCC_READ_SUMMARY(DATE=date,SOURCE='lz',TYPE=image_type,/NOBEACON, $
		SPACECRAFT=STRUPCASE(sc),TELESCOPE=STRUPCASE(detector), $
		/CHECK,/DEBRIS,/QUIET,ERRMSG=errmsg,_EXTRA=extra)
	IF (detector EQ 'cor2') THEN BEGIN
		wg= where((list.OSNUM NE 1921) and (list.OSNUM NE 1935))
		list=list[wg]
	ENDIF
	list=dir+PATH_SEP()+list.FILENAME
	list = list[SORT(list)]

;CONTENTS OF LIST STRUCTURE:
;                       FILENAME   = Name of the file
;                       DATE_OBS   = Observation date
;                       TELESCOPE  = Telescope (EUVI,COR1,COR2
;                       EXPTIME    = Exposure time, in seconds
;                       XSIZE      = Array size along X, in pixels
;                       YSIZE      = Array size along Y, in pixels
;                       FILTER     = Stray light filter
;                       VALUE      = Polarization angle or EUVI wavelength
;                       PROG       = Observation program (Dark/Led/Norm/Sequ)
;                       OSNUM      = Observation number
;                       DEST       = Destination (RT/SSR1/SSR2/SW)
;                       FPS        = Fine Pointing System (ON/OFF)
;                       LED        = LED (e.g. RedFPA, BluTel, None)
;                        COMPR      = Type of compression (ICER0..ICER11, RICE, HC0..HC9)
;                       NMISS      = Number of missing blocks or Icer segments 
;                        SPACECRAFT = Either A or B
;                       TYPE       = Type (cal/img/seq)


	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	julian_anchor = (JULIANDAY( date + time ))[0]
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN BEGIN
		ndx = ndx[0]
	ENDIF ELSE BEGIN
		ndx = 0
	ENDELSE

	IF (ndx EQ 0) OR (ndx EQ N_ELEMENTS(list)-1) THEN BEGIN
		julian_check = julian_anchor + ([-1,1])[ndx NE 0]
		CALDAT, julian_check, month, day, year, hour, minute, second
		IF (  year LT 10) THEN   year = '0' + STRTRIM(   year, 2 ) ELSE   year = STRTRIM(   year, 2 )
		IF ( month LT 10) THEN  month = '0' + STRTRIM(  month, 2 ) ELSE  month = STRTRIM(  month, 2 )
		IF (   day LT 10) THEN    day = '0' + STRTRIM(    day, 2 ) ELSE    day = STRTRIM(    day, 2 )
		IF (  hour LT 10) THEN   hour = '0' + STRTRIM(   hour, 2 ) ELSE   hour = STRTRIM(   hour, 2 )
		IF (minute LT 10) THEN minute = '0' + STRTRIM( minute, 2 ) ELSE minute = STRTRIM( minute, 2 )
		IF (second LT 10) THEN second = '0' + STRTRIM( second, 2 ) ELSE second = STRTRIM( second, 2 )
		date_new = year + month + day
		dir_new=SCC_DATA_PATH(sc, TYPE=image_type, TELESCOPE=detector, DATE=date_new)
;		dir_new=GETENV('secchi')+PATH_SEP()+'lz'+PATH_SEP()+'L0'+PATH_SEP()+sc+PATH_SEP()+image_type+PATH_SEP()+detector+PATH_SEP()+date_new

		list_new=SCC_READ_SUMMARY(DATE=date_new,SOURCE='lz',TYPE=image_type,/NOBEACON, $
			SPACECRAFT=STRUPCASE(sc),TELESCOPE=STRUPCASE(detector), $
			/CHECK,/DEBRIS,/QUIET,ERRMSG=errmsg,_EXTRA=extra)
		; Make sure list_new is a structure.  Otherwise, this implies the "next" day directory does not exist.
		IF (SIZE(list_new,/TYPE) EQ 8) THEN BEGIN
			IF (detector EQ 'cor2') THEN BEGIN
				wg= where((list_new.OSNUM NE 1921) and (list_new.OSNUM NE 1935))
				list_new=list_new[wg]
			ENDIF
			list_new = dir_new+PATH_SEP()+list_new.FILENAME
			list = [ list , list_new ]
			list = list[SORT(list)]
		ENDIF
	ENDIF
	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN BEGIN
		ndx = ndx[0]
	ENDIF ELSE BEGIN
		ndx = 0
	ENDELSE
	fits_filename = list[ndx]

	IF (no_warning_dialogs EQ 0) THEN BEGIN
		diff_hours = (min_diff*24.0)
		thresh_hours = 6.0
		IF (diff_hours GT thresh_hours) THEN BEGIN
			diff_string = STRTRIM(diff_hours,2)
			WHILE (STRMID( diff_string, 0, 1, /REVERSE ) EQ '0') DO BEGIN
				diff_string = STRMID( diff_string, 0, STRLEN( diff_string ) - 1 )
			ENDWHILE
			thresh_string = STRTRIM(thresh_hours,2)
			WHILE (STRMID( thresh_string, 0, 1, /REVERSE ) EQ '0') DO BEGIN
				thresh_string = STRMID( thresh_string, 0, STRLEN( thresh_string ) - 1 )
			ENDWHILE
			IF (STRMID( thresh_string, 0, 1, /REVERSE ) EQ '.') THEN BEGIN
				thresh_string = thresh_string + '0'
			ENDIF
			ddddd=DIALOG_MESSAGE([	'Frustrum for this image may appear to wander from main set of frustrums.', $
						'FITS file is '+diff_string+' hours different from the master time.', $
						'(Threshold for this warning is '+STRTRIM(thresh_string,2)+' hours.)', $
						'', $
						'Master time = '+date+'_'+time, $
						'FITS file = '+FILE_SEARCH(fits_filename) ])
		ENDIF
	ENDIF

	RETURN, fits_filename
END
