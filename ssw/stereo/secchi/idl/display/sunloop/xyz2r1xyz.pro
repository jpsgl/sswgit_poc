;+
; $Id: xyz2r1xyz.pro,v 1.1 2012/02/22 16:51:07 nathan Exp $
;
; Read one .xyz file.
; Flatten xyz points to surface of sun.
; Write new .xyz_r1 file.
;-
pro xyz2r1xyz,xyz_filename

; Read one .xyz file.
data_start=12
ascii=READ_ASCII(xyz_filename,COUNT=ascii_count)
ascii=strarr(ascii_count)
openr,lun,xyz_filename,/get_lun
readf,lun,ascii
free_lun,lun
meta_start=(WHERE(strmid(ascii,0,5) EQ '-9999'))[0]
all_points=ascii[data_start:meta_start-1]
each_point=dblarr(11,n_elements(all_points))
for i=0,n_elements(all_points)-1 do begin
 each_point[*,i]=double(strsplit(all_points[i],' ',/extract))
endfor

; Flatten xyz points to suface of sun.
solar_radius_km = 6.96e05
for i=0,n_elements(each_point[0,*])-1 do begin
 x=each_point[4,i]
 y=each_point[5,i]
 z=each_point[6,i]
 if (x ne 0) and (y ne 0) and (z ne 0) then begin
  flattening_factor=solar_radius_km/sqrt(x^2+y^2+z^2)
  x_r1=x*flattening_factor
  y_r1=y*flattening_factor
  z_r1=z*flattening_factor
  each_point[4:6,i]=[x_r1,y_r1,z_r1]
 endif
endfor

; Write new .xyz_r1 file.
xyz_filename_r1=strmid(xyz_filename,0,strlen(xyz_filename)-4)+'_r1.xyz'
OPENW,lun,xyz_filename_r1,/GET_LUN
for i=0,data_start-1 do begin
 PRINTF,lun,ascii[i]
endfor
for i=0,n_elements(each_point[0,*])-1 do begin
 PRINTF,lun,each_point[0:3,i],each_point[4:6,i],each_point[7:9,i],each_point[10,i],FORMAT='(4(F11.2),3(D13.0),3(D11.4),1(D11.0))'
endfor
for i=meta_start,ascii_count-1 do begin
 PRINTF,lun,ascii[i]
endfor
FREE_LUN,lun
print,'New .xyz_r1 filename with points at R1 = ',xyz_filename_r1

end
