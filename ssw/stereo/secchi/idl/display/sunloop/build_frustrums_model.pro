;+
; $Id: build_frustrums_model.pro,v 1.3 2012/02/22 16:51:06 nathan Exp $
;
; written by:   Jeffrey.R.Hall@jpl.nasa.gov
; written for:  Paulett.C.Liewer@jpl.nasa.gov
;               STEREO/SECCHI Project
;
; "Copyright 2012, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."
;-

@get_closest_fits_filename_2
@get_closest_tiff_filename

FUNCTION LAW_OF_COSINES, xyz, vec, axis
	a_ndx = WHERE([0,1,2] NE axis)
	axis1 = ([0,1,2])[a_ndx[0]]
	axis2 = ([0,1,2])[a_ndx[1]]
	sidea = SQRT( TOTAL( xyz[axis1] ^ 2 + xyz[axis2] ^ 2, /DOUBLE ) )
	sideb = SQRT( TOTAL( (vec[axis1] - xyz[axis1]) ^ 2 + (vec[axis2] - xyz[axis2]) ^ 2, /DOUBLE ) )
	hypotenuse = SQRT( TOTAL( vec[axis1] ^ 2 + vec[axis2] ^ 2, /DOUBLE ) )
	radians = ACOS( (sidea^2 + sideb^2 - hypotenuse^2) / (2 * sidea * sideb) )
	RETURN, radians
END

FUNCTION ROTATE_VECTOR,x,y,z,a,b,c,u,v,w,angle
	;----------------------------------------------------------------------------------------------------------
	; Vector rotation formula from:
	; http://www.mines.edu/~gmurray/ArbitraryAxisRotation/ArbitraryAxisRotation.html
	;----------------------------------------------------------------------------------------------------------
	u2 = u^2
	v2 = v^2
	w2 = w^2
	uvw2 = u2 + v2 + w2
	sqrtuvw2 = SQRT( uvw2 )
	cosa = COS( angle )
	sina = SIN( angle )
	RETURN, [ ( a*(v2+w2) + u*(-b*v-c*w+u*x+v*y+w*z) + ((x-a)*(v2+w2)+u*(b*v+c*w-v*y-w*z))*cosa + sqrtuvw2*( b*w-c*v-w*y+v*z)*sina ) / uvw2, $
              ( b*(u2+w2) + v*(-a*u-c*w+u*x+v*y+w*z) + ((y-b)*(u2+w2)+v*(a*u+c*w-u*x-w*z))*cosa + sqrtuvw2*(-a*w+c*u+w*x-u*z)*sina ) / uvw2, $
              ( c*(u2+v2) + w*(-a*u-b*v+u*x+v*y+w*z) + ((z-c)*(u2+v2)+w*(a*u+b*v-u*x-v*y))*cosa + sqrtuvw2*( a*v-b*u-v*x+u*y)*sina ) / uvw2 ]
END

FUNCTION BUILD_FRUSTRUMS_MODEL, $
	date, $
	time, $
	stereoa_xyz, $
	stereob_xyz, $
	fits_data_access, $
	tiff_data_access, $
	step, $
	nsteps, $
	tlb, $
	EXCLUDE_EUVI_TIFF=exclude_euvi_tiff, $
	SAVE_VRML=save_vrml, $
	ONLY_SHOW_ONE_INST_IMAGE=only_show_one_inst_image, $
	ONLY_SHOW_TWO_INST_IMAGE=only_show_two_inst_image, $
	SYSTEM=system

	IF KEYWORD_SET(exclude_euvi_tiff) THEN exclude_euvi_tiff = 1 ELSE exclude_euvi_tiff = 0
	IF KEYWORD_SET(save_vrml) THEN save_vrml = 1 ELSE save_vrml = 0

	frustrumsModel		=	OBJ_NEW( 'IDLgrModel', HIDE = 1 )
	imagesModel			=	OBJ_NEW( 'IDLgrModel', HIDE = 1 )
	imageObjArr			=	OBJARR( 2, 5 )
	fits_filenames		=	STRARR( 2, 6 )
	hdr_detector		=	STRARR( 2, 5 )
	hdr_wavelnth		=	STRARR( 2, 5 )

	;-------------------------------------------------------------------
	; Build camera view frustrums.
	; Frustrums must be built from a synchronous stereo pair of FITS.
	;-------------------------------------------------------------------
	IF ( fits_data_access EQ 1 ) THEN BEGIN

		; 4 for the lines plus 1 for the view box at the end.  This gets re-used for each telescope (instrument).
		frustrumLineObjArr	=	OBJARR(5)

		spacecraft_struct = REPLICATE( { letter : '', xyz : dblarr(3) }, 2 )
		spacecraft_struct[0].letter	= 'a'
		spacecraft_struct[0].xyz	= stereoa_xyz
		spacecraft_struct[1].letter	= 'b'
		spacecraft_struct[1].xyz	= stereob_xyz
		instruments_struct = REPLICATE( { color : lonarr(3) }, 5 )
		instruments_struct[0].color	= [255,0,127]
		instruments_struct[1].color	= [191,255,127]
		instruments_struct[2].color	= [255,191,127]
		instruments_struct[3].color	= [127,191,255]
		instruments_struct[4].color	= [63,63,255]
		instruments = ['euvi','cor1','cor2','hi_1','hi_2']
		max_heights = DBLARR(2,5)

		;==================================================================
		; Progress bar set-up vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		;==================================================================
		; Set up progress bar.
		therm				=	WIDGET_BASE( TITLE = 'Reading FITS headers...', /COLUMN, GROUP_LEADER = tlb )
		progress			=	WIDGET_DRAW( therm, XSIZE = 700, YSIZE = 25, RETAIN = 2 )
		label				=	WIDGET_LABEL( therm, VALUE = '0%', /DYNAMIC_RESIZE )
		cancelB				=	WIDGET_BUTTON( therm, VALUE = 'Cancel' )
		WIDGET_CONTROL, therm, /REALIZE
		WIDGET_CONTROL, progress, GET_VALUE = win
		count				=	0
		per				=	0
		perPrevious			=	0
		cancel_pressed			=	0
		stop_showing			=	0
		;==================================================================
		; Progress bar set-up ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		;==================================================================

		FOR craft = 0, 1 DO BEGIN

			xyz = spacecraft_struct[craft].xyz
			spacecraft_distance = (SQRT( REFORM(xyz[0,*]) ^ 2 + REFORM(xyz[1,*]) ^ 2 + REFORM(xyz[2,*]) ^ 2 ))[0]

			FOR inst = 0, 4 DO BEGIN


				;==================================================================
				; Progress bar update vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
				;==================================================================
				IF cancel_pressed THEN BEGIN
					; Destroy progress bar.
					IF WIDGET_INFO( therm, /VALID_ID ) THEN WIDGET_CONTROL, therm, /DESTROY
					RETURN, { success : 0 }
				ENDIF
				IF WIDGET_INFO( therm, /VALID_ID ) THEN BEGIN
					; Update progress bar.
					per				=	( FLOAT(step) / nsteps ) + ( FLOAT(craft) / 2 / nsteps ) + ( FLOAT(inst) / 5 / 2 / nsteps )
					perCheck			=	per * 100
					IF (perCheck GT perPrevious) THEN BEGIN
						curwin				=	!d.window
						WSET, win
						LOADCT,3,/SILENT
						; Vary the color index from 0-255 over the life of the progress bar.
                                        	POLYFILL, [0,per,per,0], [0,0,1,1], /NORMAL, COLOR = FIX(SIN(per)*255)
						a=BYTARR(CEIL(!D.X_SIZE*per),!D.Y_SIZE)
						FOR p=0,N_ELEMENTS(a[*,0])-1 DO a[p,*]=FIX(255/(N_ELEMENTS(a[*,0])/(p+1.)))
						TV,a
						LOADCT,0,/SILENT
						WSET, curwin
						WIDGET_CONTROL, label, SET_VALUE = STRCOMPRESS( CEIL( per * 100 ) )+'% ('+STRTRIM(step+1,2)+' of '+STRTRIM(nsteps,2)+' sets)'
						perPrevious			=	CEIL( per * 100 )
					ENDIF
					quit = WIDGET_EVENT( cancelB, /NOWAIT )
					IF (quit.ID EQ cancelB) THEN cancel_pressed=1
					WIDGET_CONTROL,/HOURGLASS
				ENDIF
				;==================================================================
				; Progress bar update ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				;==================================================================

				detector = instruments[inst]
				fits_filename = GET_CLOSEST_FITS_FILENAME_2( date, time, craft, detector, NO_WARNING_DIALOGS=save_vrml )
				fits_filenames[craft,inst] = fits_filename

				IF (only_show_one_inst_image NE '') THEN IF (instruments[inst] NE only_show_one_inst_image) THEN BEGIN
					fits_filenames[craft,inst] = instruments[inst] + ' not loaded'
					IF (inst NE 0) THEN CONTINUE
				ENDIF

				IF (only_show_two_inst_image[0] NE '') THEN IF ((WHERE(only_show_two_inst_image EQ instruments[inst]))[0] EQ -1) THEN BEGIN
					fits_filenames[craft,inst] = instruments[inst] + ' not loaded'
					IF (inst NE 0) THEN CONTINUE
				ENDIF

				junk=SCCREADFITS(fits_filename,hdr,/NODATA)
				wcs=FITSHEAD2WCS(hdr,SYSTEM='A')
				ixy=LONARR(2,4)
				ixy[0,*] = [ 0, 0,            hdr.naxis1-1, hdr.naxis1-1 ]
				ixy[1,*] = [ 0, hdr.naxis2-1, hdr.naxis2-1, 0            ]
				coord=WCS_GET_COORD(wcs,ixy, /FORCE_PROJ )
				CONVERT_STEREO_LONLAT, hdr.date_obs, coord, 'GEI', system, /DEGREES, /IGNORE_ORIGIN
				corners = DBLARR(3,4)
				FOR ixy_coord = 0, 3 DO BEGIN
					lon = coord[0,ixy_coord]
					lat = coord[1,ixy_coord]
					simulated_infinity = 1.0e06
					; "vec" is camera view point projected onto sphere centered at [0,0,0].
					CSPICE_RADREC, simulated_infinity, lon * CSPICE_RPD(), lat * CSPICE_RPD(), vec
					vec = vec * [1,-1,-1]	; Flip upside down.

					; Adjust image planes to similar distances from spacecraft.
					IF (ixy_coord EQ 0) THEN BEGIN
						other_lon = coord[0,1]
						other_lat = coord[1,1]
						CSPICE_RADREC, simulated_infinity, other_lon * CSPICE_RPD(), other_lat * CSPICE_RPD(), other
						other = other * [1,-1,-1]	; Flip upside down.
						sidea = SQRT( TOTAL( vec ^ 2, /DOUBLE ) )
						sideb = SQRT( TOTAL( other ^ 2, /DOUBLE ) )
						hypotenuse = SQRT( TOTAL( (vec - other) ^ 2, /DOUBLE ) )
						ang = ACOS( (sidea^2 + sideb^2 - hypotenuse^2) / (2 * sidea * sideb) )
						ang = ang / 2
						adj_factor = 1 / COS( ang )
					ENDIF

					; Normalize vec to spacecraft_distance.
					vec = vec * ( spacecraft_distance / simulated_infinity ) * adj_factor
					; Offset to spacecraft location.
					vec = vec + xyz

					; ROTATE FRUSTRUMS TO POINT AT SUN.
					; USE EUVI SUN CENTER AS POINTING GUIDE.

					IF ( (inst EQ 0) AND (ixy_coord EQ 0) ) THEN BEGIN
						euvi_wcs=FITSHEAD2WCS(hdr)
						euvi_suncenter_xy = WCS_GET_PIXEL( euvi_wcs, [0,0], /FORCE_PROJ ) + 1
						euvi_suncenter_coord = WCS_GET_COORD( wcs, euvi_suncenter_xy, /FORCE_PROJ )
						CONVERT_STEREO_LONLAT, hdr.date_obs, euvi_suncenter_coord, 'GEI', system, /DEGREES, /IGNORE_ORIGIN
						euvi_suncenter_lon = euvi_suncenter_coord[0]
						euvi_suncenter_lat = euvi_suncenter_coord[1]
						CSPICE_RADREC, simulated_infinity, euvi_suncenter_lon * CSPICE_RPD(), euvi_suncenter_lat * CSPICE_RPD(), euvi_vec
						euvi_vec = euvi_vec * [1,-1,-1]	; Flip upside down.
						; Normalize euvi_vec to spacecraft distance.
						euvi_vec = euvi_vec * ( spacecraft_distance / simulated_infinity )
						; Offset to spacecraft location.
						euvi_vec = euvi_vec + xyz
						euvi_distance = SQRT( TOTAL( (euvi_vec - xyz) ^ 2, /DOUBLE ) )
						; Get swing angle.
						angle_z = LAW_OF_COSINES( xyz, euvi_vec, 2 )
					ENDIF

					IF (only_show_one_inst_image NE '') THEN IF (instruments[inst] NE only_show_one_inst_image) THEN CONTINUE
					IF (only_show_two_inst_image[0] NE '') THEN IF (instruments[inst] NE only_show_two_inst_image[0]) AND (instruments[inst] NE only_show_two_inst_image[1]) THEN CONTINUE

					; Swing rotation.
					vec = (vec - xyz) / euvi_distance
					vec = ROTATE_VECTOR(vec[0],vec[1],vec[2], 0,0,1, 0,0,1, angle_z)
					vec = vec * euvi_distance + xyz
					IF ( (inst EQ 0) AND (ixy_coord EQ 0) ) THEN BEGIN
						euvi_vec = (euvi_vec - xyz) / euvi_distance
						euvi_vec = ROTATE_VECTOR(euvi_vec[0],euvi_vec[1],euvi_vec[2], 0,0,1, 0,0,1, angle_z)
						euvi_vec = euvi_vec * euvi_distance + xyz
					ENDIF

					; Create one frustrum line.
					frustrumLineObjArr[ixy_coord]=	OBJ_NEW( 'IDLgrPolyline', $
																[ xyz[0], vec[0] ], $
																[ xyz[1], vec[1] ], $
																[ xyz[2], vec[2] ], $
																COLOR=instruments_struct[inst].color )
					; Save end point as one corner of the view box.
					corners[*,ixy_coord] = vec
					max_heights[craft,inst]		=	MAX( [ max_heights[craft,inst], SQRT( REFORM(corners[0,ixy_coord]) ^ 2 + REFORM(corners[1,ixy_coord]) ^ 2 + REFORM(corners[2,ixy_coord]) ^ 2 ) ] )
				ENDFOR

				IF ((only_show_one_inst_image NE '') AND (instruments[inst] NE only_show_one_inst_image)) THEN CONTINUE
				IF (only_show_two_inst_image[0] NE '') THEN IF ((WHERE(only_show_two_inst_image EQ instruments[inst]))[0] LT 0) THEN CONTINUE

				; Create view box using corner points.
				frustrumLineObjArr[ixy_coord] = OBJ_NEW( 'IDLgrPolyline', $
															[	[REFORM(corners[*,0])], $
																[REFORM(corners[*,1])], $
																[REFORM(corners[*,2])], $
																[REFORM(corners[*,3])], $
																[REFORM(corners[*,0])] ], $
															COLOR=instruments_struct[inst].color )
				frustrumsModel		->	Add, frustrumLineObjArr
				IF (save_vrml EQ 1) THEN BEGIN
					WRITE_WAVE, date+'_'+time+'_'+instruments[inst]+'_'+spacecraft_struct[craft].letter+'.obj', corners, /VECTOR, /NOMESHDEF
				ENDIF

				IF ( tiff_data_access EQ 1 ) THEN BEGIN
	
					; Skip EUVI images, if requested to do so.
					IF (inst GT 0) OR ((inst EQ 0) AND (exclude_euvi_tiff EQ 0)) THEN BEGIN

						;-------------------------------------------------------------------
						; If correspoinding TIFF is available, then create a polygon at 
						; end of the view frustrum where the image can be put.
						;-------------------------------------------------------------------
						hdr_detector[craft,inst] = hdr.DETECTOR
						hdr_wavelnth[craft,inst] = hdr.WAVELNTH
						imageObjArr[craft,inst] = OBJ_NEW( 'IDLgrImage', INTARR(10,10), /NO_COPY )
						imagePolygonObj	= OBJ_NEW( 'IDLgrPolygon', $
														corners, $
														TEXTURE_MAP = imageObjArr[craft,inst], $
														TEXTURE_COORD = [[0,0],[1,0],[1,1],[0,1]], $
														COLOR = [127,127,255], $
														ALPHA_CHANNEL = 0.6, $
														STYLE = 2 )
						imagesModel		->	Add, imagePolygonObj
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR

		CSPICE_KTOTAL,'all',kcount & PRINT,'build_frustrums_model.pro:  CSPICE_KTOTAL count = ',kcount
		FOR i = 0, (kcount) DO BEGIN
			CSPICE_KDATA, i, 'ALL', file, type, source, handle, found
			IF ( found ) THEN print, type, file
		ENDFOR

	ENDIF

	;==================================================================
	; Progress bar destroy vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	;==================================================================
	; Destroy progress bar.
	IF WIDGET_INFO( therm, /VALID_ID ) THEN WIDGET_CONTROL, therm, /DESTROY
	;==================================================================
	; Progress bar destroy ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	;==================================================================

	RETURN, {	success:1, $
				frustrumsModel:frustrumsModel, $
				imagesModel:imagesModel, $
				imageObjArr:imageObjArr, $
				fits_filenames:fits_filenames, $
				hdr_detector:hdr_detector, $
				hdr_wavelnth:hdr_wavelnth, $
				max_heights_of_frustrums:max_heights }
END

FUNCTION SUPPLY_IMAGES_TO_FRUSTRUMS_MODEL, $
	date, $
	time, $
	imageObjArr, $
	hdr_detector, $
	hdr_wavelnth, $
	step_state, $
	step, $
	nsteps, $
	tlb, $
	EXCLUDE_EUVI_TIFF=exclude_euvi_tiff, $
	SAVE_VRML=save_vrml, $
	ONLY_SHOW_ONE_INST_IMAGE=only_show_one_inst_image, $
	ONLY_SHOW_TWO_INST_IMAGE=only_show_two_inst_image

	IF KEYWORD_SET(exclude_euvi_tiff) THEN exclude_euvi_tiff = 1 ELSE exclude_euvi_tiff = 0
	IF KEYWORD_SET(save_vrml) THEN save_vrml = 1 ELSE save_vrml = 0

	success = 0
	instruments = ['euvi','cor1','cor2','hi_1','hi_2']
	tiff_filenames = STRARR( 2, 6 )

	;==================================================================
	; Progress bar set-up vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	;==================================================================
	; Set up progress bar.
	therm				=	WIDGET_BASE( TITLE = 'Reading TIFF images...', /COLUMN, GROUP_LEADER = tlb )
	progress			=	WIDGET_DRAW( therm, XSIZE = 700, YSIZE = 25, RETAIN = 2 )
	label				=	WIDGET_LABEL( therm, VALUE = '0%', /DYNAMIC_RESIZE )
	cancelB				=	WIDGET_BUTTON( therm, VALUE = 'Cancel' )
	WIDGET_CONTROL, therm, /REALIZE
	WIDGET_CONTROL, progress, GET_VALUE = win
	count				=	0
	per				=	0
	perPrevious			=	0
	cancel_pressed			=	0
	stop_showing			=	0
	;==================================================================
	; Progress bar set-up ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	;==================================================================


	FOR craft = 0, 1 DO BEGIN

		FOR inst = 0, 4 DO BEGIN

			IF (only_show_one_inst_image NE '') THEN IF (instruments[inst] NE only_show_one_inst_image) THEN BEGIN
				tiff_filenames[craft,inst] = instruments[inst] + ' not loaded'
				IF (inst NE 0) THEN CONTINUE
			ENDIF

			IF (only_show_two_inst_image[0] NE '') THEN IF (instruments[inst] NE only_show_two_inst_image[0]) AND (instruments[inst] NE only_show_two_inst_image[1]) THEN BEGIN
				tiff_filenames[craft,inst] = instruments[inst] + ' not loaded'
				IF (inst NE 0) THEN CONTINUE
			ENDIF

			; Skip EUVI images, if requested to do so.
			IF (inst GT 0) OR ((inst EQ 0) AND (exclude_euvi_tiff EQ 0)) THEN BEGIN

				;==================================================================
				; Progress bar update vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
				;==================================================================
				IF cancel_pressed THEN BEGIN
					; Destroy progress bar.
					IF WIDGET_INFO( therm, /VALID_ID ) THEN WIDGET_CONTROL, therm, /DESTROY
					RETURN, { success : 0, tiff_filenames : '' }
				ENDIF
				IF WIDGET_INFO( therm, /VALID_ID ) THEN BEGIN
					; Update progress bar.
					per				=	( FLOAT(step) / nsteps ) + ( FLOAT(craft) / 2 / nsteps ) + ( FLOAT(inst) / 5 / 2 / nsteps )
					perCheck			=	per * 100
					IF (perCheck GT perPrevious) THEN BEGIN
						curwin				=	!d.window
						WSET, win
						LOADCT,3,/SILENT
						; Vary the color index from 0-255 over the life of the progress bar.
						POLYFILL, [0,per,per,0], [0,0,1,1], /NORMAL, COLOR = FIX(SIN(per)*255)
						a=BYTARR(CEIL(!D.X_SIZE*per),!D.Y_SIZE)
						FOR p=0,N_ELEMENTS(a[*,0])-1 DO a[p,*]=FIX(255/(N_ELEMENTS(a[*,0])/(p+1.)))
						TV,a
						LOADCT,0,/SILENT
						WSET, curwin
						WIDGET_CONTROL, label, SET_VALUE = STRCOMPRESS( CEIL( per * 100 ) )+'% ('+STRTRIM(step+1,2)+' of '+STRTRIM(nsteps,2)+' sets)'
						perPrevious			=	CEIL( per * 100 )
					ENDIF
					quit = WIDGET_EVENT( cancelB, /NOWAIT )
					IF (quit.ID EQ cancelB) THEN cancel_pressed=1
					WIDGET_CONTROL,/HOURGLASS
				ENDIF
				;==================================================================
				; Progress bar update ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				;==================================================================

				;-------------------------------------------------------------------
				; If correspoinding TIFF is available, then map it to a polygon at 
				; end of the view frustrum.
				;-------------------------------------------------------------------
				tiff_filename = GET_CLOSEST_TIFF_FILENAME( date, time, craft, hdr_detector[craft,inst], hdr_wavelnth[craft,inst] )
				tiff_filenames[craft,inst] = tiff_filename
				IF ( tiff_filename NE '' ) THEN BEGIN
					img = READ_TIFF( tiff_filename )
					img = ROTATE( TEMPORARY(img), 1 )
					img = BYTSCL( TEMPORARY(img), MIN = 0, MAX = 2L^16-1 )
					; Reduce weight of image objects by reducing image size in some cases.
					; The thinking is, if it's only_show_one_inst_image and it's sun-pointed 
					; then maximize the 1024 space by making the image size = 1024.  If it's 
					; only_show_one_inst_image and it's HI then make it 512.  If it's the 
					; first of only_show_two_inst_image including an HI then make it 256, 
					; and if it's the second then make it 512.  If it's only_show_two_inst_image 
					; and does NOT include HI then make both 1024.
					; If it's a full set of 5 instruments per telescope, then make them 
					; EUVI=512, COR1=512, COR2=1024, HI_1=1024 and HI_2=1024.
					CASE instruments[inst] OF
						'euvi' : BEGIN
							CASE 1 OF
								(save_vrml EQ 1) : new_size = 64
								(only_show_one_inst_image EQ 'euvi') : new_size = 1024
								ELSE : new_size = 512
							ENDCASE
							END
						'cor1' : BEGIN
							CASE 1 OF
								(save_vrml EQ 1) : new_size = 64
								(only_show_one_inst_image EQ 'cor1') OR ((WHERE(only_show_two_inst_image EQ 'cor1'))[0] GE 0) : new_size = 1024
								ELSE : new_size = 512
							ENDCASE
							END
						'cor2' : BEGIN
							CASE 1 OF
								(save_vrml EQ 1) : new_size = 64
								(only_show_two_inst_image[0] EQ 'cor2') : new_size = 256
								ELSE : new_size = 1024
							ENDCASE
							END
						'hi_1' : BEGIN
							CASE 1 OF
								(save_vrml EQ 1) : new_size = 64
								((WHERE(only_show_two_inst_image EQ 'hi_1'))[0] GE 0) : new_size = 512
								(only_show_two_inst_image[0] EQ 'hi_1') : new_size = 256
								ELSE : new_size = 1024
							ENDCASE
							END
						'hi_2' :BEGIN
							CASE 1 OF
								(save_vrml EQ 1) : new_size = 64
								((WHERE(only_show_two_inst_image EQ 'hi_2'))[0] GE 0) : new_size = 512
								ELSE : new_size = 1024
							ENDCASE
							END
					ENDCASE
					img = CONGRID( TEMPORARY(img), new_size, new_size, CUBIC=-0.5 )
					imageObjArr[craft,inst] -> SetProperty, DATA = img
				ENDIF
			ENDIF ELSE BEGIN
				tiff_filenames[craft,inst] = 'EUVI not loaded'
			ENDELSE
		ENDFOR
	ENDFOR

	;==================================================================
	; Progress bar destroy vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
	;==================================================================
	; Destroy progress bar.
	IF WIDGET_INFO( therm, /VALID_ID ) THEN WIDGET_CONTROL, therm, /DESTROY
	;==================================================================
	; Progress bar destroy ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	;==================================================================

	RETURN, { success : 1, tiff_filenames : tiff_filenames }
END

;;-------------------------------------------------------------------
;; Project images onto curved surface of Thomson Sphere.  
;; (INITIAL DEVELOPMENT VERSION)
;;-------------------------------------------------------------------
;IF ( fits_data_access EQ 1 ) THEN BEGIN
;
;	dir = SCC_DATA_PATH('a', TYPE='img', TELESCOPE='hi_2', DATE=date) + '/'
;	spawn,'find '+dir,list
;	filename=list[1]
;	filename=date+'_'+time+'_s4h2A.fts'
;	img=SCCREADFITS(filename,hdr)
;	img=BYTSCL(img,MIN=0,MAX=1000000L)
;	wcs=FITSHEAD2WCS(hdr)
;	coord=WCS_GET_COORD(wcs)
;	MAP_SET, /CYLINDRICAL, 0, 0
;	map=MAP_PATCH(img,coord[0,*,*],coord[1,*,*],XSTART=sx,YSTART=sy)
;	map=REVERSE(REVERSE(TEMPORARY(map),1),2)	; Flip upside down.  (As is done with the XYZ data.)
;	;TV,map,sx,sy
;	;TV,CONGRID(img,512,512)
;	
;		skyaImage			=	OBJ_NEW('IDLgrImage', map, ALPHA_CHANNEL = 0.0 )
;		skyaModel			=	OBJ_NEW( 'orb', POS = stereoa_xyz/2, RADIUS = SQRT( TOTAL( stereoa_xyz^2, /DOUBLE ) ) / 2, $
;									TEXTURE_MAP = skyaImage, /NOCOPY, $
;									COLOR = [255,255,255], STYLE = 2, /ZERO, /TEX_COORDS, /BOTTOM )
;
;ENDIF

