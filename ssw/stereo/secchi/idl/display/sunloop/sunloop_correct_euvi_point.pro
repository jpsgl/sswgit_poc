pro sunloop_correct_euvi_point, header
;
; $Id: sunloop_correct_euvi_point.pro,v 1.1 2012/02/22 16:51:07 nathan Exp $
;
case header.obsrvtry of
    'STEREO_A': begin
        xparam = [0.21584997, 0.15774528]
        yparam = [0.85618944, -1.3752966, 0.19157372]
    end
    'STEREO_B': begin
        xparam = [0.26168900, 0.32874260]
        yparam = [-0.16206742, 1.1228401, -0.10127881]
    end
endcase
;
year = (utc2tai(header.date_obs) - utc2tai('26-Oct-2006')) / 86400 / 365.25d0
header.crpix1 = header.crpix1 + poly(year, xparam)
header.crpix2 = header.crpix2 + poly(year, yparam)
;
end
