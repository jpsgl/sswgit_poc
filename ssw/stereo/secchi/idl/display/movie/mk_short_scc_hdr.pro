function mk_short_scc_hdr,mvi_frame_hdr,mvi_file_hdr
;+
; $Id: mk_short_scc_hdr.pro,v 1.10 2011/05/13 17:57:54 mcnutt Exp $
;
; NAME:	mk_short_scc_hdr
;
; PURPOSE:
;	make short scch from mvi_frame_hdr, mvi_file_hdr and spice if original fits file is missing
;
; CATEGORY: movie, header, fits, wcs
;
; CALLING SEQUENCE:
;
; INPUTS:
;	mvi_file_hdr	MVI header structure as defined in sccread_mvi.pro
;	mvi_frame_hdr	MVI frame header structure as defined in def_mvi_strct.pro
;
; OPTIONAL INPUTS:
;	
; KEYWORD PARAMETERS: 
;
; OUTPUTS:
;	scch	
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;-
; MODIFICATION HISTORY:
;
; $Log: mk_short_scc_hdr.pro,v $
; Revision 1.10  2011/05/13 17:57:54  mcnutt
; sets sun center frame_hdr
;
; Revision 1.9  2011/05/12 20:39:01  nathan
; fix shortscch persistence
;
; Revision 1.8  2010/07/13 18:23:13  mcnutt
; corrected spacecraft selection if image is a total brightness
;
; Revision 1.7  2009/11/13 19:57:22  mcnutt
; commented out HI scch.date_avg hi_fix_pointing changed to use filename and date_avg
;
; Revision 1.6  2009/11/13 18:42:44  mcnutt
; creates header for 2048X2048 image, HI 1024X1024 and sets date_avg based on hi pointing files reuses most previous header values if same tel and sc
;
; Revision 1.5  2009/10/29 16:06:56  nathan
; added date_end and date keywords
;
; Revision 1.4  2009/10/27 19:45:00  nathan
; add date_avg and filename to header (so hi_pointing works)
;
; Revision 1.3  2009/09/09 17:47:11  mcnutt
; changed OBSRVTRY to STEREO not SECCHI
;
; Revision 1.2  2009/02/20 15:59:01  mcnutt
; removed database call
;
; Revision 1.1  2009/02/19 20:03:02  mcnutt
; creates a short version of scc hdr for missing fits files
;

common short_scchdr, shortscch

detector = mvi_frame_hdr.detector
aorb=strmid(mvi_frame_hdr.filename,strpos(mvi_frame_hdr.filename,'.ft')-1,1)
OBSRVTRY='STEREO_'+strupcase(aorb)

if datatype(shortscch) eq "UND" then shortscch=def_secchi_hdr() ELSE $
if shortscch.OBSRVTRY NE obsrvtry or shortscch.detector NE detector then shortscch=def_secchi_hdr()
scch=shortscch

scch.date_obs=mvi_frame_hdr.date_obs+'T'+mvi_frame_hdr.time_obs
scch.date_avg=scch.date_obs
scch.date_end=scch.date_obs
scch.date    =scch.date_obs
scch.filename=mvi_frame_hdr.filename
scch.detector=detector

scch.OBSRVTRY=obsrvtry
scch.rsun=mvi_frame_hdr.RSUN
scch.crota=mvi_frame_hdr.ROLL
     
; Only re-set these values if a new telescope

if scch.OBSRVTRY ne shortscch.OBSRVTRY or scch.detector ne shortscch.detector then begin
  scch.CUNIT1='arcsec'
  scch.CUNIT2='arcsec'


  scch.naxis=2
  scch.naxis1=2048;mvi_file_hdr.nx
  scch.naxis2=2048;mvi_file_hdr.ny

  scch.p1row=1
  scch.p2row=2048
  scch.p1col=51
  scch.p2col=2098

  if (scch.OBSRVTRY eq 'STEREO_A') then begin
    case scch.detector of
      'EUVI':   begin
                scch.r1row=2176-scch.p2col+1
                scch.r2row=2176-scch.p1col+1
                scch.r1col=2176-scch.p2row+1
                scch.r2col=2176-scch.p1row+1
             end
      'COR1':   begin
                scch.r1row=2176-scch.p2col+1
                scch.r2row=2176-scch.p1col+1
                scch.r1col=scch.p1row
                scch.r2col=scch.p2row
             end
      'COR2':   begin
                scch.r1row=scch.p1col
                scch.r2row=scch.p2col
                scch.r1col=2176-scch.p2row+1
                scch.r2col=2176-scch.p1row+1
             end
      'HI1':   begin
                scch.r1row=scch.p1row
                scch.r2row=scch.p2row
                scch.r1col=scch.p1col
                scch.r2col=scch.p2col
             end
      'HI2':   begin
                scch.r1row=scch.p1row
                scch.r2row=scch.p2row
                scch.r1col=scch.p1col
                scch.r2col=scch.p2col
             end
     endcase
  endif

  if (scch.OBSRVTRY eq 'STEREO_B') then begin
   case scch.detector of
      'EUVI':   begin
                scch.r1row=2176-scch.p2col+1
                scch.r2row=2176-scch.p1col+1
                scch.r1col=scch.p1row
                scch.r2col=scch.p2row
           end
      'COR1':   begin
                scch.r1row=scch.p1col
                scch.r2row=scch.p2col
                scch.r1col=2176-scch.p2row+1
                scch.r2col=2176-scch.p1row+1
             end
      'COR2':   begin
                scch.r1row=2176-scch.p2col+1
                scch.r2row=2176-scch.p1col+1
                scch.r1col=scch.p1row
                scch.r2col=scch.p2row
             end
      'HI1':   begin
                scch.r1row=2176-scch.p2row+1
                scch.r2row=2176-scch.p1row+1
                scch.r1col=2176-scch.p2col+1
                scch.r2col=2176-scch.p1col+1
             end
      'HI2':   begin
                scch.r1row=2176-scch.p2row+1
                scch.r2row=2176-scch.p1row+1
                scch.r1col=2176-scch.p2col+1
                scch.r2col=2176-scch.p1col+1
             end
    endcase
  endif

  scch.RECTIFY  =  'T'

  scch.ipsum=1.0
  scch.summed=1.0
  scch.ccdsum=1.0
  scch.sumcol=1
  scch.sumrow=1

  if strpos(scch.detector,'HI') gt -1 then begin
     scch.CTYPE1A='RA---AZP'
     scch.CTYPE2A='DEC--AZP'
     scch.CTYPE1='HPLN-AZP'
     scch.CTYPE2='HPLT-AZP'
     scch.CUNIT1='deg'
     scch.CUNIT2='deg'
     scch.naxis1=1024
     scch.naxis2=1024
     scch.ipsum=2.0
     scch.summed=2.0
  endif


  platescl=getsccsecpix(scch,/UPDATE, /FAST)
  crpix=getscccrpix(scch,/UPDATE, /FAST)
  

  if mvi_frame_hdr.XCEN gt 1 then begin
     scl=scch.naxis1/mvi_file_hdr.nx
     scch.crpix1=(mvi_frame_hdr.XCEN*scl)+1.
     scch.crpix2=(mvi_frame_hdr.YCEN*scl)+1.
  endif

endif

if scch.detector eq 'EUVI' then begin
    carlonlat=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1),/au, /degrees, system='Carrington')
    scch.CRLN_OBS=carlonlat(1)
    scch.CRLT_OBS=carlonlat(2)
    heeq=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1), /degrees, system='HEEQ',_extra=_extra)
    scch.HGLN_OBS=heeq(1)
    scch.HGLT_OBS=heeq(2)
    hee_d=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1), /meters, system='HEE',_extra=_extra)
    scch.DSUN_OBS=hee_d(0)
endif

;if strpos(scch.detector,'HI') gt -1 then begin
;     dir = getenv('SCC_DATA')+path_sep()+'hi'+path_sep()
;     IF (n_elements(path) eq 1) THEN dir = path + '/'
;     fle = dir + 'pnt_' + scch.detector + strmid(scch.obsrvtry,7,1) + '_' + strmid(scch.date_avg,0,10) + '_fix_mu_fov.fts'
;     file = file_search(fle,count=exists)
;     IF (exists eq 1) THEN BEGIN
;        hd = hi_mread_pointing(file(0))
;        scch.date_avg=hd(where(strmid(hd.filename,0,16) eq strmid(scch.filename,0,16))).date_avg
;     ENDIF
;endif

shortscch=scch
return, scch
END
