;+
; $Id: scc_pngplay.pro,v 1.94 2016/02/17 18:54:11 mcnutt Exp $
PRO scc_pngplay,stdate0,endate0,cam0,sc0,imgsize=imgsize,length=length,rdiff=rdiff,skip=skip, half=half, AIA=aia, EUVI_PLUS=euvi_plus, $
    anaglyph=anaglyph,bmin=bmin,bmax=bmax,jpg=jpg,mpegname=mpegname,pan=pan,doxcolors=doxcolors, wavelets=wavelets,events=events,$
    hdrs=hdrs,nohdrs=nohdrs,all=all,euvi_all=euvi_all,cor=cor,hi=hi,euvia=euvia,euvib=euvib,stars=stars,cadence=cadence, $
    _EXTRA=_extra, USE_SIZE=use_size, SYNOPTIC=synoptic,STONYHURST=stonyhurst,srem=srem,wcscombine=wcscombine,pnglist=pnglist,pngtimes=pngtimes, spwx=spwx
;
; Project   : STEREO SECCHI
;                   
; Name      : SCC_PNGPLAY
;               
; Purpose   : This procedure runs a movie of a given days worth of PNG data in the "standard"
;             SolarSoft movie widget
;               
; Explanation: 
;               
; Use       : IDL> scc_pngplay,'YYYYMMDD-ST','YYYYMMDD-END','CAM','SC',imgsize=imgsize,length=length,rdiff=rdiff,skip=skip
;
; Examples:   IDL> scc_pngplay,'20070501_120000','20070502_120000','cor2','a',imgsize=1024,skip=2
;             IDL> scc_pngplay,'2007-04-12','2007-04-12','euvi_304',/anaglyph
;    
; Inputs    :CAM = cor1, cor2, hi1, hi2, euvi_171, etc --or--
;            CAM array option to view more then one cam at a time
;                  create one row: 
;                      cam=['hi2a','hi1a','cor2a','cor2b','hi1b','hi2b']              
;                  to create two rows: 
;                      Row 2  'euvi_284a','euvi_195a','euvi_284b','euvi_195b'
;                      Row 1  'euvi_171a','euvi_304a','euvi_171b','euvi_304b'
;                      cam=[['euvi_171a','euvi_304a','euvi_171b','euvi_304b'],['euvi_284a','euvi_195a','euvi_284b','euvi_195b']]
;   	    	    Note that with this option the cadence is determined by the cam with the highest frequency, 
;   	    	    so using SKIP=10 is recommended if EUVI is one  of the cams.
;
;            SC = 'A', 'B' (optionally can be set by adding sc (a or b) to the end of cam)
;            YYYYMMDD-ST = start date/time of images to be displayed - any CDS format or YYMMDD
;            YYYYMMDD-END = optional end date of images to be displayed  
;            
; Outputs   : None
;
; Keywords  : IMGSIZE=	: size of movie frames; Default is 512, options are 256 or 1024 -- eg: imgsize=1024
;             LENGTH=	: length of movie; Default is all images for the day (~140) -- eg: length=60 will only play first 60 images
;             /RDIFF	: Use running-diff (EUVI, COR2, COR1) or star_rem (HI) images from $SECCHI_PNG
;             SKIP= 	: Skip every n-frames; Default is 0 -- eg: skip=2
;             /ANAGLYPH : displays EUVI anaglyph movies (Only Mar01 - Jun31, 2007). Note that ONLY 1024x1024 images 
;                       are available
;             /JPG  	: displays the archived jpegs instead of pngs. (COR1 is forced to jpg)
;   	      /STARS	: use hi_stars HI images instead of edge-enhanced HI images
;   	      /srem	: use hi_srem HI2  can also be set by using tel='h2sra'
;
;             /ALL  	:   displays 8 SECCHI coronagraphs and 8 EUVI wavelengths: imgsize=256, output is 1024x1024
;             /EUVI_ALL :   displays all 4 euvi wavelengths for both a and b: imgsize=256, output is 1024x512 
;             /EUVIA	:   displays all 4 euvi wavelengths for a: imgsize=512, output is 1024x1024
;             /EUVIB	:   displays all 4 euvi wavelengths for b: imgsize=512, output is 1024x1024
;             /COR  	:   displays both COR1 and COR2 for a and b: imgsize=512, output is 1024x1024
;             /HI   	:   displays both HI1 and HI2 for a and b: imgsize=512, output is 1024x1024
;             /DOXCOLORS :  OBSOLETE - always set to 1
;             /HDRS 	:   OBSOLETE - functionality moved to generic_movie and define_wcshdrs
;             /NOHDRS 	:   Turns off automatic header creation in generic_movie
;   	      CADENCE=  :   minimum time between images in minutes
;   	      PAN= 	:   Fraction by which to resize input 
;             /WAVELETS	:  will read in wavelets pngs (hdrs should be defined using hdr keyword due to changes in the file name formats)
;   	    	    	First checks full_cadence; then events; then 01_per_hour
;             HALF=  	: a string array of r or l to indicate which side of image to use; must be same dimension as cam string array
;   	    	    	    (optionally can be set by adding (l or r) to the end of cam)
;   	    	/AIA	:   Retrieve 4 wavelengths of AIA: 304, 211, 193, 171 (2x2)
;   	    	/EUVI_PLUS: Displays EUVI-A, AIA, EUVI-B (4x3)
;   	    (removed)	/COMBINE:  saves movies of individual telescopes and calls WSCC_COMBINE_MVI
;   	    wcscombine  : creates combined movie frames using wcs_combine for more control create movie using the wscc_pngplay widget.
;   	/SYNOPTIC   : 	Display Carrington maps; use with EUVI cam
;   	/STONYHURST :	Display Stonyhurst maps; use with EUVI cam
;
; Calls from LASCO : RTMVIPLAYPNG
;
; Common    : 
;               
; Restrictions: Requires definition of $SECCHI_PNG or $SECCHI_JPG
;               
; Side effects: 
;               
; Category    : DISPLAY
;               
; Prev. Hist. : None.
;
; Written     : Karl Battams, NRL/I2, Mar 2007
;               
; $Log: scc_pngplay.pro,v $
; Revision 1.94  2016/02/17 18:54:11  mcnutt
; do not remove b from the end of camu hmib
;
; Revision 1.93  2015/09/03 18:56:04  hutting
; correct soho directory name
;
; Revision 1.92  2015/07/30 16:51:33  hutting
; added yearly directory to SECCHI daily browse filenames
;
; Revision 1.91  2013/08/26 21:49:57  nathan
; fix useweb for lasco and reorder stmts
;
; Revision 1.90  2013/07/16 11:13:41  mcnutt
; added / to end of web directory string to work with new find_sock
;
; Revision 1.89  2013/07/05 18:11:52  mcnutt
; corrected SECCHI web dirs and added HMI option
;
; Revision 1.88  2013/05/24 15:45:17  nathan
; Fix paths for useweb; shorten print messages'
;
; Revision 1.87  2012/12/21 15:27:39  mcnutt
; change synoptic maps directorys
;
; Revision 1.86  2012/08/28 21:57:14  secchib
; nr - change ftype and use to determine useweb
;
; Revision 1.85  2012/08/10 20:52:15  avourlid
; Fixed calls to wavelet dirs
;
; Revision 1.84  2012/08/03 12:24:00  mcnutt
; removed combine keyword
;
; Revision 1.83  2012/07/26 19:35:22  nathan
; do not call scc_sun_center, it appears unnecessary
;
; Revision 1.82  2012/07/26 12:28:11  mcnutt
; check for jpgs if pngs are not found
;
; Revision 1.81  2012/07/17 19:04:03  mcnutt
; checks for secchi-a image dir if not found uses web images
;
; Revision 1.80  2012/07/17 18:09:03  mcnutt
; added lasco to web search options
;
; Revision 1.79  2012/07/17 16:42:47  mcnutt
; added AIA from web image selection
;
; Revision 1.78  2012/07/16 21:59:10  nathan
; minor change to del for url
;
; Revision 1.77  2012/07/12 17:43:58  mcnutt
; correct cor1 becon dir
;
; Revision 1.76  2012/07/12 16:43:28  mcnutt
; added rdiff option for spwx becon images
;
; Revision 1.75  2012/07/12 15:29:30  mcnutt
; added web search for outside NRL
;
; Revision 1.74  2012/07/12 13:34:27  mcnutt
; corrected sc for more then one day if spwx
;
; Revision 1.73  2012/07/12 13:15:56  mcnutt
; added spwx keyword to search daily browse from the STEREO Science Center web site
;
; Revision 1.72  2012/05/02 19:42:39  mcnutt
; last change was commited unintentionally
;
; Revision 1.71  2012/05/02 19:36:23  mcnutt
; sets sc for wavelet filenames based on _L and _R define_wcshdrs.pro
;
; Revision 1.70  2012/04/30 17:28:26  mcnutt
; defaults to cor2 doubles t= total brightness and * = both
;
; Revision 1.69  2012/04/13 17:42:40  secchib
; sets plstd to ff if not previously defined do to missing days of pretties
;
; Revision 1.68  2012/04/13 16:56:00  mcnutt
; change  (bang)delimiter to get_delim
;
; Revision 1.67  2012/04/03 17:56:31  mcnutt
; added lasco rdiff options
;
; Revision 1.66  2012/02/08 22:34:18  secchib
; nr - stop if ndays gt 3 and no files found
;
; Revision 1.65  2012/01/23 20:48:48  mcnutt
; returns noimage for pnglst if no images are found
;
; Revision 1.64  2012/01/20 14:54:58  mcnutt
; undefines plstd for first day to avoid previous telescope filenames being used
;
; Revision 1.63  2012/01/20 14:26:39  mcnutt
; changed list ot plst to be compatable with IDL8
;
; Revision 1.62  2011/11/02 17:11:11  battams
; Bug fix line 865: please use [ and ] for subscripts, not parentheses!
;
; Revision 1.61  2011/10/17 22:37:10  nathan
; do not pass rdiff to generic_movie; uncomment if /pngtimes
;
; Revision 1.60  2011/10/17 18:34:15  mcnutt
; corrected error
;
; Revision 1.59  2011/10/07 19:18:14  nathan
; make adjustments for new wavelet directories; /EVENTS is now obsolete
;
; Revision 1.58  2011/10/03 14:42:44  mcnutt
; added pngtimes keyword to return frame times
;
; Revision 1.57  2011/09/22 13:13:29  mcnutt
; added file_hdr keyword to generic_movie call
;
; Revision 1.56  2011/09/20 19:54:11  nathan
; default imgsize=512 where appropriate
;
; Revision 1.55  2011/09/16 20:13:18  nathan
; Clean up keyword definitions; change char for difference or srem from r to d
; because r means right
;
; Revision 1.54  2011/09/14 19:17:13  mcnutt
; return file plst only if keyword pnglist is set
;
; Revision 1.53  2011/08/19 20:18:42  mcnutt
; get wavlet headers when wavelets are set from tel name
;
; Revision 1.52  2011/08/04 16:40:54  mcnutt
; added w to euvi options to select wavelets in a combined movie ie euvi_195wa
;
; Revision 1.51  2011/08/02 18:47:09  avourlid
; Changed file filter for COR2 backgrounds in include bkgs made of DOUBLE imgs
;
; Revision 1.50  2011/06/14 17:52:09  nathan
; do not change inputs; print camu before help dplst
;
; Revision 1.49  2011/05/20 16:53:56  mcnutt
; added rdiff png option
;
; Revision 1.48  2011/05/12 17:25:28  mcnutt
; added wsccombine keyword
;
; Revision 1.47  2011/05/03 19:12:41  mcnutt
; add combine keyword creates individual moives and call wscc_combine_mvi
;
; Revision 1.46  2011/05/02 18:01:20  mcnutt
; added hi2 srem png movies
;
; Revision 1.45  2011/04/27 20:58:29  nathan
; defn for /euvi_plus
;
; Revision 1.44  2011/04/27 20:52:49  nathan
; fix aia filename query; rename plst dplst for clarity
;
; Revision 1.43  2011/03/31 15:43:29  nathan
; update print comment
;
; Revision 1.42  2011/03/24 16:56:42  mcnutt
; added stonyhurst maps
;
; Revision 1.41  2011/03/21 18:30:02  mcnutt
; added synoptic movie options
;
; Revision 1.40  2011/02/18 22:32:53  nathan
; use break_file to determine each filename datetime independent of path or suffix
;
; Revision 1.39  2011/02/11 18:58:49  nathan
; added c3, c2 values to camu plst; added /EUVI_PLUS
;
; Revision 1.38  2011/02/08 17:56:20  mcnutt
; added less keyword to find_closest call
;
; Revision 1.37  2011/02/07 19:25:54  mcnutt
; added lasco combined image tel=lasco
;
; Revision 1.36  2011/01/26 16:43:50  mcnutt
; corrected COR2 tb image names for sccfindfits
;
; Revision 1.35  2011/01/20 15:32:16  nathan
; add /AIA
;
; Revision 1.34  2010/12/02 18:58:33  mcnutt
; will look for SSR2 fits files if wavlets keyword_set
;
; Revision 1.33  2010/12/02 18:46:02  nathan
; add help,plst
;
; Revision 1.32  2010/11/30 14:32:03  mcnutt
; added half keyword passed to generic_movie
;
; Revision 1.31  2010/11/29 19:14:26  mcnutt
; added wavelet and event keywords to display wavelet png movies
;
; Revision 1.30  2010/11/18 22:18:09  nathan
; put hdr functionality back in; call find_closest
;
; Revision 1.29  2010/11/10 16:31:38  mcnutt
; added nohdrs keyword to turn off automatic headers in generic movie
;
; Revision 1.28  2010/11/10 14:45:05  mcnutt
; commetted out find_closest
;
; Revision 1.27  2010/10/09 00:18:02  nathan
; always /doxcolors; obsolete /hdrs
;
; Revision 1.26  2010/08/25 15:58:50  mcnutt
; removed stop
;
; Revision 1.25  2010/08/25 15:46:59  mcnutt
; uses find closest instead of gt the selectect closest image
;
; Revision 1.24  2010/02/22 18:50:32  mcnutt
; correct sun center in headers
;
; Revision 1.23  2010/01/11 15:49:04  secchib
; changed how kyword jpg works set onlyftype get changed if cor1
;
; Revision 1.22  2009/12/02 18:02:47  mcnutt
; added keyword _extra
;
; Revision 1.21  2009/11/30 19:16:34  nathan
; Remove requirement for $SECCHI_JPG to be defined; make endate completely optional
;
; Revision 1.20  2009/07/29 21:42:57  nathan
; allow any time format as input
;
; Revision 1.19  2009/07/10 18:38:54  mcnutt
; added candance keyword and allows and times hhmmss to input dates(optional)
;
; Revision 1.18  2009/04/22 17:31:38  mcnutt
; added keywords hdrs and doxcolors
;
; Revision 1.17  2009/03/31 14:52:00  mcnutt
; added pan keyword to resize movies in generic_movie
;
; Revision 1.16  2009/03/30 11:53:34  mcnutt
; added keyword mpegname to passed to generic_movie to create mpeg movie without calling scc_wrunmovie
;
; Revision 1.15  2009/03/25 19:17:53  nathan
; added doc
;
; Revision 1.14  2009/03/25 18:59:47  nathan
; added doc
;
; Revision 1.13  2009/03/25 18:57:23  nathan
; Updated keyword documentation; allow single input date if using keywords to
; specify telescope(s)
;
; Revision 1.12  2009/03/25 18:37:45  mcnutt
; changed euvi keyword to euvi_all to keep idl happy
;
; Revision 1.11  2009/03/19 14:54:37  mcnutt
; added stars keyword to display hi_stars
;
; Revision 1.10  2009/03/19 13:54:18  mcnutt
; will now play combination png or jpg movies, new quick select keywords added
;
; Revision 1.9  2008/05/28 15:41:04  reduce
; Now uses generic_movie and does COR1 data. Karl B.
;
; Revision 1.8  2007/10/18 14:58:09  reduce
; More tweaks and bug fixes. Karl B
;
; Revision 1.7  2007/10/04 13:41:25  reduce
; Little bug fix, and add ugly hack (and offend the IDL Gods). Karl B.
;
; Revision 1.6  2007/10/03 19:07:24  reduce
; Now does anaglyph movies! (complete with bugs!) Karl B.
;
; Revision 1.5  2007/07/03 17:10:37  reduce
; Bug fix for when a day is chosen that doesn't have data. Karl B.
;
; Revision 1.4  2007/06/27 18:57:42  reduce
; Now runs movie between start and end dates. Karl B.
;
; Revision 1.3  2007/06/26 19:01:12  reduce
; Release HI1/2 movies. Karl B.
;
; Revision 1.2  2007/04/27 20:22:59  reduce
; Now works with COR2 pngs! -- KarlB
;
; Revision 1.1  2007/03/28 19:14:03  reduce
; Initial Release -- Karl B
;
;
;-


; ********************************************************************
; ********************************************************************
; *********** START WITH A BUNCH OF VARIABLE/SETUP CHECKS...**********
; ********************************************************************
; ********************************************************************
del=get_delim()
camsrch='JunkString'
noloadct=0
if ~keyword_set(bmin) then bmin=0
if ~keyword_set(bmax) then bmax=255

IF not keyword_set(imgsize) and (keyword_set(all) or keyword_set(euvi_all) or (keyword_set(cor) and keyword_set(hi))) then imgsize=256
IF not keyword_set(imgsize) then imgsize=512
np=n_params()
stdate=stdate0
IF np GT 1 THEN endate=endate0
IF np GT 2 THEN cam=cam0
IF np GT 3 THEN sc=sc0

; set up cam array if quick call keywords are set
if keyword_set(all) then cam=[['hi2a','hi1a','cor2a','cor1a'],$
                              ['cor1b','cor2b','hi1b','hi2b'],$
	                      ['euvi_171a','euvi_304a','euvi_284a','euvi_195a'],$
	                      ['euvi_171b','euvi_304b','euvi_284b','euvi_195b']]

if keyword_set(euvi_all) then cam=[ ['euvi_304b','euvi_284b','euvi_195b','euvi_171b'], $
    	    	    	    	    ['euvi_304a','euvi_284a','euvi_195a','euvi_171a']]
if keyword_set(EUVI_PLUS) then cam=[['euvi_304b','euvi_284b','euvi_195b','euvi_171b'], $
    	    	    	    	    ['aia_304',  'aia_211',  'aia_193',  'aia_171'], $
				    ['euvi_304a','euvi_284a','euvi_195a','euvi_171a']]
    
if keyword_set(euvia) then cam=[['euvi_171a','euvi_304a'],['euvi_284a','euvi_195a']]
if keyword_set(euvib) then cam=[['euvi_171b','euvi_304b'],['euvi_284b','euvi_195b']]
if keyword_set(cor) and imgsize eq 256 then cam=['cor2b','cor1b','cor1a','cor2a']
if keyword_set(cor) and imgsize gt 256 then cam=[['cor2b','cor1b'],['cor2a','cor1a']]
if keyword_set(hi) and imgsize eq 256 then cam=['hi2a','hi1a','hi1b','hi2b']
if keyword_set(hi) and imgsize gt 256 then cam=[['hi2a','hi1a'],['hi1b','hi2b']]
if keyword_set(hi) and keyword_set(cor) then cam=[['hi2a','hi1a','cor2a','cor1a'],['cor1b','cor2b','hi1b','hi2b']]
IF keyword_set(AIA) THEN cam = [['aia_171',  'aia_304'],  ['aia_211',  'aia_193']]

IF np LT 2 and datatype(cam) eq 'UND' THEN BEGIN
    PRINT,'********************************
    PRINT,'INPUT VARIABLES MISSING!'
    PRINT,'MINIMUM CALLING SEQUENCE: '
    PRINT,'E.G.  IDL> scc_pngplay,stdate,endate,cam,sc
    PRINT,' -- OR (for anaglyph) --
    PRINT,'      IDL> scc_pngplay,stdate,endate,cam,/anaglyph
    PRINT,'Formats: (all are strings)'
    PRINT,'     Start and End Dates: YYYYMMDD'
    PRINT,'     Cam: hi1,hi2,cor1,cor2,euvi_171 thru euvi_304'
    PRINT,'     SC: A or B'
    PRINT,'********************************
    return
ENDIF

; ********************************************************************
; ********************************************************************
; ********************* NOW WE'LL SET SOME STUFF...*******************
; ********************************************************************
; ********************************************************************
; Now we need to over-ride some stuff for the anaglyph images:

IF keyword_set(anaglyph) THEN BEGIN
  sc='a'
;  cam='euvi' ; no choice!
  running_diff=0  ; still no other choice!
  noloadct=1
  if keyword_set(imgsize) THEN BEGIN
      IF imgsize NE 1024 THEN BEGIN
        PRINT,''
        PRINT,''
        PRINT,'%%%%%%%%%%%%%%%%%%    WARNING    %%%%%%%%%%%%%%%%%%
        PRINT,'Anaglyphs are only available in 1024x1024...so that
        PRINT,' is what you are getting. If you do not think you
        PRINT,' have enough memory to handle this, you should
        PRINT,' probably kill this process.
        PRINT,' You have been warned.... :)
        PRINT,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        PRINT,''
        PRINT,''
        wait,4
    ENDIF
  ENDIF
  imgsize=1024 ; no choice again!
ENDIF

imgsizedir=strcompress(string(imgsize),/remove_all)

IF not keyword_set(skip) then skip=0




    
;stop
; ********************************************************************
; ********************************************************************
; ************* SEE IF WE'RE COVERING MULTIPLE DAYS...****************
; ********************************************************************
; ********************************************************************
IF n_params() EQ 1 THEN endate=stdate
IF datatype(stdate) NE datatype(endate) THEN BEGIN
    sc=cam
    cam=endate
    endate=stdate
ENDIF
IF strmid(stdate,0,2) NE strmid(endate[0],0,2) THEN BEGIN
    IF n_params() GT 2 THEN sc=cam
    cam=endate
    endate=stdate
ENDIF
cam=strlowcase(cam)
w=where(strmid(cam[0],0,3) EQ ['c2','c3','c23','c2c','eit','las'], issoho)
IF issoho THEN sc='soho'

;if strlen(endate) lt 9 then endate=endate+'_235959'    
    st = nrlanytim2utc(stdate)
    en = nrlanytim2utc(endate)
    IF en.time EQ 0 THEN en.time=86399000
    ndays=en.mjd-st.mjd + 1
    dates=strarr(ndays)
    
    ; Here we just generate a string array of dates (YYMMDD)
    FOR i=0,ndays-1 DO BEGIN ; only do it if there's more than one day chosen, otherwise...
        mjdval=st.mjd+i
        mjd2date,mjdval,yy,mm,dd
        mm=strcompress(mm,/remove_all)
        dd=strcompress(dd,/remove_all)
        IF strlen(mm) EQ 1 THEN mm='0'+mm
        IF strlen(dd) EQ 1 THEN dd='0'+dd
        ;stop
        nextdt=strcompress((string(yy)+mm+dd),/remove_all)
        dates[i]=nextdt
    ENDFOR

nd=n_elements(dates) 
ncr=[1,1]
ns=size(cam,/dim)
if ns(0) eq 0 then ns(0)=1
for ns1=0,n_Elements(ns)-1 do ncr(ns1)=ns(ns1)

plstout=strarr(1500*nd,ncr(0),ncr(1))
allfntimes=plstout
cdmin=8000
cdm=intarr(2)
imgsize0=imgsize
if ~keyword_set(half) then half=strarr(ncr(0),ncr(1))
for sk=0,ncr(1)-1 do begin
; rows
  for ck=0,ncr(0)-1 do begin
  ; columns
    imgsize=imgsize0
    plst=''
    camu =strlowcase(cam(ck,sk))
    tchar=rstrmid(camu,0,1)
    if tchar eq 'r' or tchar eq 'l' then begin
    	half(ck,sk)=strmid(camu,0,1,/reverse)
        camu=strmid(camu,0,strlen(camu)-1)
    endif
    if tchar EQ 'a' then sc ='a'
    if tchar EQ 'b' then sc ='b'
    if camu ne 'hmib' and (tchar EQ 'a' or tchar EQ 'b') then camu=strmid(camu,0,strlen(camu)-1)
    if keyword_set(stars) and strpos(camu,'hi') gt -1 then camu=camu+'s'
    if keyword_set(srem) and strpos(camu,'hi') gt -1 then camu=camu+'d'
    if keyword_set(rdiff) then camu=camu+'d'
    IF strmid(camu,0,3) EQ 'aia' THEN sc='sdo'
    IF strmid(camu,0,3) EQ 'hmi' THEN sc='sdo'
    IF keyword_set(synoptic) or keyword_set(stonyhurst)  THEN sc='synoptic'
    w=where(strmid(camu,0,3) EQ ['c2','c3','c23','c2c','eit','las','c2d','c3d'], issoho)
    IF issoho THEN sc='soho'
    IF (sc NE 'a') and (sc NE 'b') and (sc NE 'sdo')  and (sc NE 'soho') and (sc NE 'synoptic')THEN BEGIN
        PRINT,'Bad spacecraft identifier...'
        return
    ENDIF 
    isscip=1
    uwavelets=0
    if camu eq 'euvi' and keyword_set(spwx) then camu='euvi_195'
    CASE camu of
      'cor1':       BEGIN
                    camsrch='_s*c1'
                    camdir='cor1'
                  END
      'cor1d':       BEGIN
                    camsrch='_rdBc1'
                    camdir='cor1_rdif'
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'cor2':       BEGIN
                    camsrch='_dbc2'
                    camdir='cor'
                  END
      'cor2t':       BEGIN
                    camsrch='_tbc2'
                    camdir='cor'
                  END
      'cor2*':       BEGIN
                    camsrch='_*bc2'
                    camdir='cor'
                  END
      'cor2d':       BEGIN
                    camsrch='_rdDc2'
                    camdir='cor2_rdif'
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'hi1':        BEGIN
                    camsrch='_tbh1'
                    camdir='hi'
		    isscip=0
                  END
      'hi2':        BEGIN
                    camsrch='_tbh2'
                    camdir='hi'
		    isscip=0
                  END
      'hi1s':        BEGIN
                    camsrch='_tbh1'
                    camdir='hi_stars'
		    isscip=0
                  END
      'hi1d':        BEGIN
                    camsrch='_SRh1'
                    camdir='hi_srem'
		    isscip=0
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'hi2s':        BEGIN
                    camsrch='_tbh2'
                    camdir='hi_stars'
		    isscip=0
                  END
      'hi2d':        BEGIN
                    camsrch='_SRh2'
                    camdir='hi_srem'
		    isscip=0
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'euvi_171':   BEGIN
                    IF ~keyword_set(anaglyph) THEN camsrch='_17eu' ELSE camsrch='17AB'
                    IF ~keyword_set(synoptic)  and ~keyword_set(stonyhurst) THEN camdir='euvi' ELSE camdir='171'  
                  END
      'euvi_195':   BEGIN
                    IF ~keyword_set(anaglyph) THEN camsrch='_19eu' ELSE camsrch='19AB'
                    camdir='euvi'  
                    IF ~keyword_set(synoptic) and ~keyword_set(stonyhurst) THEN camdir='euvi' ELSE  camdir='195'  
                  END
      'euvi_284':   BEGIN
                    IF ~keyword_set(anaglyph) THEN camsrch='_28eu' ELSE camsrch='28AB'
                    IF keyword_set(synoptic) or keyword_set(stonyhurst) THEN BEGIN
                      PRINT,'synoptic not available for 284...'
                        return
                    ENDIF ELSE camdir='euvi'  
                  END
      'euvi_304':   BEGIN
                    IF ~keyword_set(anaglyph) THEN camsrch='_30eu' ELSE camsrch='30AB'
                    IF ~keyword_set(synoptic) and ~keyword_set(stonyhurst) THEN camdir='euvi' ELSE  camdir='304'  
                  END
      'euvi_171d':   BEGIN
                    camsrch='_17rdeu'
                    camdir='euvi_rdif'
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'euvi_195d':   BEGIN
                    camsrch='_19rdeu'
                    camdir='euvi_rdif'
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'euvi_284d':   BEGIN
                    camsrch='_28rdeu'
                    camdir='euvi_rdif'
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'euvi_304d':   BEGIN
                    camsrch='_30rdeu'
                    camdir='euvi_rdif'
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
                    imgsize=512
                  END
      'euvi_171w':   BEGIN
                    camdir=strmid(camu,5,3)+'_'+strupcase(sc)
                    camsrch=strmid(camu,5,3)+'eu'
		    uwavelets=1
                  END
      'euvi_195w':   BEGIN
                    camdir=strmid(camu,5,3)+'_'+strupcase(sc)
                    camsrch=strmid(camu,5,3)+'eu'
		    uwavelets=1
                  END
      'euvi_284w':   BEGIN
                    camdir=strmid(camu,5,3)+'_'+strupcase(sc)
                    camsrch=strmid(camu,5,3)+'eu'
		    uwavelets=1
                  END
      'euvi_304w':   BEGIN
                    camdir=strmid(camu,5,3)+'_'+strupcase(sc)
                    camsrch=strmid(camu,5,3)+'eu'
		    uwavelets=1
                  END
      'aia_171':   BEGIN
                    camsrch='_0171i'
                    camdir='aia'    
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available locally for '+camu+'; set image size to 512 to use local files',/info
                   ;IF ~keyword_set(spwx) then imgsize=512
                  END
      'aia_193':   BEGIN
                    camsrch='_0193i'
                    camdir='aia'  
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available locally for '+camu+'; set image size to 512 to use local files',/info
                   ;IF ~keyword_set(spwx) then imgsize=512
                  END
      'aia_211':   BEGIN
                    camsrch='_0211i'
                    camdir='aia'  
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available locally for '+camu+'; set image size to 512 to use local files',/info
                    ;IF ~keyword_set(spwx) then imgsize=512
                  END
      'aia_304':   BEGIN
                    camsrch='_0304i'
                    camdir='aia'  
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available locally for '+camu+'; set image size to 512 to use local files',/info
                    ;IF ~keyword_set(spwx) then imgsize=512
                  END
      'hmib':   BEGIN
                    camsrch='_HMIB'
                    camdir='aia'  
                  END
      'hmii':   BEGIN
                    camsrch='_HMII'
                    camdir='aia'  
                  END
      'hmid':   BEGIN
                    camsrch='_HMID'
                    camdir='aia'  
                  END
      'lasco':   BEGIN
                    camsrch='c2c3'
                    camdir='lasco'  
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
 		    imgsize=512 
                 END  
      'c2c3':   BEGIN
                    camsrch='c2c3'
                    camdir='lasco' 
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
		    imgsize=512 
                  END  
      'c23':	BEGIN
                    camsrch='c2c3'
                    camdir='lasco'  
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
		    imgsize=512 
                  END  
      'c2':	BEGIN
                    camsrch='asc2'
                    camdir='c2'  
                    imgsize=1024
                  END  
      'c3':	BEGIN
                    camsrch='asc3'
                    camdir='c3'  
                    imgsize=1024
                  END  
      'c2c3d':   BEGIN
                    camsrch='rc2c3'
                    camdir='lasco_rdiff' 
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
		    imgsize=512 
                  END  
      'c23d':	BEGIN
                    camsrch='rc2c3'
                    camdir='lasco_rdiff'  
		    IF imgsize NE 512 THEN message,'IMGSIZE='+trim(imgsize)+' not available for '+camu+'; using 512',/info
		    imgsize=512 
                  END  
      'c2d':	BEGIN
                    camsrch='rdlc2'
                    camdir='c2_rdiff'  
                    imgsize=1024
                  END  
      'c3d':	BEGIN
                    camsrch='rdlc3'
                    camdir='c3_rdiff'  
                    imgsize=1024
                  END  


      ELSE:       BEGIN
                    PRINT,'Unrecognised telescope: ',camu
                    PRINT,'Options are cor1, cor2, hi1, hi2, euvi_171, etc'
		    print,'scc_pngplay, date, tel, sc'
                    return
                END
    ENDCASE
    IF KEYWORD_SET(synoptic) then begin
      imgsizedir='carrington'
      camsrch='SYeu*'
    ENDIF
    IF KEYWORD_SET(stonyhurst) then  begin
      imgsizedir='stonyhurst'
      camsrch='SHeu*'
    ENDIF
    IF KEYWORD_SET(wavelets) then  BEGIN
       if strpos(camu,'euvi') gt -1 then begin
          camdir=strmid(camu,5,3)+'_'+strupcase(sc)
          camsrch=strmid(camu,5,3)+'eu'
          uwavelets=1
       endif else begin
          PRINT,'wavelets keyword can only be use for EUVI'
          return
       endelse
    ENDIF


img_root_dir=getenv('SECCHI_PNG') 

ftype='png'

if camu EQ 'cor1' or camdir EQ 'aia' or camdir EQ 'hmi' or keyword_set(JPG) THEN BEGIN
    IF camu EQ 'hi1s' or camu EQ 'hi2s' THEN BEGIN
    	help,camu
	message,'For this Telescope PNG only.',/info
        ftype='png'
	wait,2
    ENDIF ELSE BEGIN
    	ftype='jpg'
	img_root_dir=getenv('SECCHI_JPG')
    ENDELSE
ENDIF

useweb=1
if ftype EQ 'png' THEN IF is_dir(getenv_slash('SECCHI_PNG')+sc) then useweb=0 
if ftype EQ 'jpg' THEN IF is_dir(getenv_slash('SECCHI_JPG')+sc) then useweb=0 
if keyword_set(spwx) then useweb=1
if strpos(strlowcase(camu),'hmi') gt -1 then useweb=1
if strpos(strlowcase(camu),'aia') gt -1 and imgsize ne 512 then useweb=1
if useweb then begin
   print,'*********************************************************************************'
   print,'*********************************************************************************'
   print,'**                                                                             **'
   if keyword_set(spwx) and camdir ne 'aia' and ~issoho then $
   print,'**   Downloading Image files from http://stereo-ssc.nascom.nasa.gov/browse     **' else $
   if camdir eq 'aia' then $
   print,'**   Downloading Image files from http://sdo.gsfc.nasa.gov/assets/img/browse/  **' else $
   if issoho then $
   print,'**   Downloading Image files from http://lasco-www.nrl.navy.mil/pngs/          **' else $
   print,'**   Downloading Image files from http://secchi.nrl.navy.mil/sccimages         **'
   print,'**                                                                             **'
   print,'*********************************************************************************'
   print,'*********************************************************************************'
endif   

;if img_root_dir EQ '' THEN BEGIN
;    IF ftype eq '*.jpg' THEN var='JPG' ELSE var='PNG'
;    PRINT,''
;    PRINT,'ERROR!! $SECCHI_'+var+' environment variable is not set!
;    PRINT,''
;    return
;endif

IF ~(uwavelets) THEN imgsize=imgsize<1024
IF ~KEYWORD_SET(synoptic) and ~KEYWORD_SET(stonyhurst) then imgsizedir=strcompress(string(imgsize),/remove_all)


; This for loop is where the pro determines the directory to search and then does the actual file search.  
; If it doesn't find any files, it quits somewhat gracefully. 
    FOR k=0,nd-1 DO BEGIN
        IF k EQ 0 then undefine,plstd
	retrydir:
    	if (uwavelets) then BEGIN
    	    ftype='png'
    	    if keyword_set(imgsize) then pan=imgsize/2048. 
    	    img_root_dir='/net/corona/secchi/wavelets/'
	    yyyy=strmid(dates[k],0,4)
	    yyyymm=strmid(dates[k],0,6)
	    dd=strmid(dates[k],6,2)
	    ; first check full_cadence
;	    srcdir=img_root_dir+'EUVI_full_mission'+del+yyyy+del+'pngs'+del+yyyymm+del+camdir+del
    	    srcdir=img_root_dir+'EUVI_full_cadence'+del+yyyy+del+'pngs'+del+yyyymm+del+dd+del+camdir+del
	    IF not file_exist(srcdir) THEN $
	    ; then check events
	    srcdir=img_root_dir+'EUVI_events'+del+'pngs'+del+dates[k]+del+camdir+del
	    IF not file_exist(srcdir) THEN $
	    ; finally go to 01_per_hour
;    	    srcdir=img_root_dir+'EUVI_full_cadence'+del+yyyy+del+'pngs'+del+yyyymm+del+dd+del+camdir+del
	        srcdir=img_root_dir+'EUVI_full_mission'+del+yyyy+del+'pngs'+del+yyyymm+del+camdir+del
    	ENDIF ELSE BEGIN
          if sc eq 'soho' then datestr=dates[k] else datestr=strmid(dates[k],0,4)+del+dates[k]
	      srcdir=img_root_dir+del+sc+del+camdir+del+datestr+del+imgsizedir+del
          Result = DIR_EXIST( srcdir )
	      if result eq 0 then srcdir=img_root_dir+del+sc+del+camdir+del+datestr+del+imgsizedir+del
        ENDELSE
    	IF KEYWORD_SET(anaglyph) THEN $
      	    srcdir=img_root_dir+'/anaglyph/euvi/1024/'+dates[k]+del

        IF useweb THEN BEGIN
    	    IF camdir EQ 'aia' THEN BEGIN
	    	   srcdir='http://sdo.gsfc.nasa.gov/assets/img/browse/'+strmid(dates(k),0,4)+'/'+strmid(dates(k),4,2)+'/'+strmid(dates(k),6,2)+'/' 
            	camsrchweb='*'+imgsizedir+strmid(camsrch,0,5)+'.jpg'
    	    endif ELSE BEGIN
	          camsrchweb='*'+camsrch+'*g'
              ;if camdir eq 'COR' then camdir='Cor2' else camdir=strupcase(camdir)
	    	  IF issoho THEN $
		        srcdir='http://lasco-www.nrl.navy.mil/pngs/'+strlowcase(camdir)+'/'+dates[k]+'/'+imgsizedir+'/' $
		      ELSE BEGIN
	    	    camdir=strupcase(camdir)
		        srcdir='http://secchi.nrl.navy.mil/sccimages/'+camdir+'_'+strupcase(sc)+'/'+strmid(dates[k],0,4)+'/'+dates[k]+'/'+imgsizedir+'/'
		     ENDELSE
    	ENDELSE
	ENDIF

      	IF KEYWORD_SET(spwx) and camdir ne 'aia' THEN begin
            if strpos(camdir,'_rdif') gt 0 then camdir='_rdiff' else camdir=''
	    if strpos(camu,'hi2') gt -1 then camdir='hi2'+camdir
	    if strpos(camu,'cor2') gt -1 then camdir='cor2'+camdir
	    if strpos(camu,'cor1') gt -1 then camdir='cor1'+camdir
	    if strpos(camu,'hi1') gt -1 then camdir='hi1'+camdir
      	    if strpos(camu,'euvi') gt -1 then camdir='euvi'+camdir+'/195'
	    if sc eq 'a' or sc eq 'ahead' then sc='ahead' else sc='behind'
	    srcdir='http://stereo-ssc.nascom.nasa.gov/browse/'+strmid(dates(k),0,4)+'/'+strmid(dates(k),4,2)+'/'+strmid(dates(k),6,2) $
	    	    +'/'+sc+'/'+camdir+'/'+imgsizedir+'/'
            camsrchweb='*.jpg'
        ENDIF
	
	print,'Searching ',srcdir
	wait,1
        uftype='*.'+ftype
       IF not file_exist(srcdir) and not useweb and img_root_dir eq getenv('SECCHI_PNG') then BEGIN
	  PRINT,'Checking for JPGs'
	  srcdir=getenv('SECCHI_JPG')+del+sc+del+camdir+del+strmid(dates[k],0,4)+del+dates[k]+del+imgsizedir+del
          Result = DIR_EXIST( srcdir )
	  if result eq 0 then srcdir=getenv('SECCHI_JPG')+del+sc+del+camdir+del+dates[k]+del+imgsizedir+del
	  uftype='*.jpg'
       ENDIF
	IF not file_exist(srcdir) and not useweb THEN BEGIN
            PRINT,''
            PRINT,'Could not find the above directory.'
            PRINT,'Do you have a typo in your dates?'
            PRINT,'Does your machine recognise the above directory?' 
            PRINT,'--
            PRINT,'Contact Lynn Hutting or Nathan Rich if you have issues with this routine or the data.'
            PRINT,''
            IF keyword_set(anaglyph) THEN BEGIN
        	PRINT,''
        	PRINT,'**********************************************************************************'
        	PRINT,'NOTE:  For anaglyph images, we only have an archive that spans the first few months
        	PRINT,'       of 2007 (roughly late Feb to early June).  Spacecraft separation outside of 
        	PRINT,'       those times was too small or big for anaglyphs.'
        	PRINT,'**********************************************************************************'
        	PRINT,''
            ENDIF
	    IF camdir EQ 'aia' and imgsize EQ 512 THEN BEGIN
		print,'Trying imgsize=1024...
		imgsize=1024
		imgsizedir='1024'
		goto, retrydir
	    ENDIF
	    wait,5
    ;        return
	ENDIF ELSE BEGIN
        	if uwavelets eq 1 then srcdir=srcdir+del+strmid(dates[k],0,8)
        	;stop
        	;if cam EQ 'cor1' then ftype='*.jpg' else ftype='*.png' ; cor1 files are jpgs only
        	if not useweb then ff=file_search(srcdir+'*'+camsrch+uftype) ELSE ff=sock_find(srcdir,camsrchweb)
        	IF k EQ 0 or datatype(plstd) eq 'UND' then plstd=ff ELSE $
        	    IF (ff[0] NE '') THEN plstd=[plstd,ff] ELSE $
                	 PRINT,'No data found for ',strcompress(dates[k],/remove_all); check that some data was found and then add it to the array
        	;stop
        	IF (n_elements(plstd) GT 1) THEN plstd=plstd(where(plstd NE '')) ; another bug fixed! Go me!
        	nf=n_elements(plstd)
        	IF nf LT 2 and k EQ nd-1 THEN BEGIN
        	    PRINT,'Found no specified pngs using the following search term:'
        	    if not useweb then PRINT,srcdir+'*'+camsrch+'*.png' else print,srcdir+camsrchweb
		    IF k GT 3 THEN stop
        	    ;return
        	ENDIF
	ENDELSE
    ENDFOR
   print,'camu: ',camu
    help,plstd
    if datatype(plstd) eq 'UND' then plstd=''
    if plstd(0) ne '' then begin
    	plstout(0,ck,sk)=plstd
    	break_file,plstd,di,pa,ro,sf
    	lenr=strlen(ro+sf)
    	allfntimes[0,ck,sk]=strmid(ro,0,8)+strmid(ro,9,4)
      ;toff=12 & toff2=21
      ;if keyword_set(anaglyph) then begin
      ;	  toff=11 & toff2=20
      ;endif
      ;if keyword_set(wavelets) then begin
      ;    toff=14 & toff2=23
      ;endif
      c1s=strmid(ro[1],9,6) ; hhmmss
      c0s=strmid(ro[0],9,6)
      c1=fix(strmid(c1s,0,2))*60*60+fix(strmid(c1s,2,2))*60+fix(strmid(c1s,4,2))
      c0=fix(strmid(c0s,0,2))*60*60+fix(strmid(c0s,2,2))*60+fix(strmid(c0s,4,2))
      if c1-c0 lt cdmin then begin
       cdmin=c1-c0 & cdm=[ck,sk]
      endif
    endif
  endfor
endfor

;check to see if any files were found
tmp=where(plstout ne '',cnt)
if cnt eq 0 then begin
  print,'No images found for the requested date.'
   pnglist='NoImages'
   return
endif
 
     time1a=utc2tai(st)
     time1b=utc2tai(en)
     endtime=utc2yymmdd(tai2utc(time1b),/yyyy,/hhmmss)
     endtime=strmid(endtime,0,8)+strmid(endtime,9,4)
     starttime=utc2yymmdd(tai2utc(time1a),/yyyy,/hhmmss)
     starttime=strmid(starttime,0,8)+strmid(starttime,9,4)
     
     help,starttime,endtime
     time1=reform(allfntimes(where(allfntimes(*,cdm(0),cdm(1)) ne ''),cdm(0),cdm(1)))
     time1=time1(where(time1 ge starttime and time1 le endtime))
     
     IF keyword_set(skip) then time1=time1[indgen(floor(n_elements(time1)/(skip*1.0)))*skip]
     IF keyword_set(length) then time1=time1[0:length-1] ; default length is all images for the day

     if keyword_set(cadence) then begin 
        time1=utc2yymmdd(tai2utc(findgen((((time1b-time1a)/60) /cadence)+1) *(cadence*60.) +time1a),/yyyy,/hhmmss)
        time1=double(strmid(time1,0,8)+strmid(time1,9,4))
     endif
     plsts=strarr(n_Elements(time1),ncr(0),ncr(1))

  for sk=0,ncr(1)-1 do begin
     for ck=0,ncr(0)-1 do begin
    	time2=double(allfntimes(*,ck,sk))
        tz=where(time2 gt 0,cnt)
        if cnt gt 1 then time2=time2(tz)
        if time2(0) gt 0 then begin
          for ln=0,n_Elements(time1)-1 do begin
            z=find_closest(double(time1(ln)),time2,/less)
	    ;z=where(time2 le time1(ln))
	    ;if z(0) ne -1 then z=z(n_Elements(z)-1) else z=n_Elements(time2(where(time2 ne '')))-1 
	    plsts(ln,ck,sk)= plstout(z,ck,sk)
          endfor
        endif
     endfor
   endfor
   
   plst=plsts

;get headers from fits files
if ~keyword_set(anaglyph) and keyword_set(hdrs) then begin
    message,'Loading headers...',/info
    hdrplst=plst[*,cdm[0],cdm[1]]
    break_file,hdrplst,di,pa,roots,sf
    if uwavelets eq 1 then begin
       if strpos(pa(0),'_A/') gt -1 then sc='A' else sc='B'
       roots=strmid(roots,0,16)+'??eu'+sc
    endif else strput,roots,'?4',16 
    ftf=sccfindfits(roots)
    if ftf[0] ne '' then begin
    	help,ftf
    	dummy = sccreadfits(ftf, outhdr, /nodata)
    	if uwavelets eq 1 then scl=1.0 else scl=1.0*imgsize/outhdr[0].naxis1
    	outhdr.crpix1=0.5+(outhdr.crpix1-0.5)*scl
    	outhdr.crpix2=0.5+(outhdr.crpix2-0.5)*scl
    	outhdr.cdelt1=outhdr.cdelt1/scl
    	outhdr.cdelt2=outhdr.cdelt2/scl
    	;sunc = SCC_SUN_CENTER(outhdr,FULL=0)
      ; IDL coords!!
      	outhdr.crota=0.
     endif
     if datatype(outhdr) ne 'UND' then hdrs=outhdr else begin
        hdrs=0
        print,'!!!!!!!!!!!!!!!!!Unable to find fits - files Headers not defined!!!!!!!!!!!!!!!!!!' 
     endelse
endif

if keyword_set(synoptic) or keyword_set(stonyhurst) then begin
      BREAK_FILE, plst(0), a, dir, name, ext
      if keyword_set(synoptic) then pngmvi=file_search(dir+'*SYeuA.hdr') else pngmvi=file_search(dir+'*SHeuA.hdr')
      OPENR,lu,pngmvi,/GET_LUN
      SCCREAD_MVI, lu, file_hdr, ihdrs, imgsin, swapflag , pngmvi=pngmvi
      CLOSE,lu
      Free_lun,lu
endif else file_hdr=0
; ********************************************************************
; ********************************************************************
; ************************ NOW WE'LL RUN THE MOVIE! ******************
; ********************************************************************
; ********************************************************************
zh=where(half ne '')
if zh(0) eq -1 then half=0 

doxcolors=1

if keyword_set(pngtimes) then begin
   time1=string(time1,'(i12)')
   pngtimes=strmid(time1,0,4)+'-'+strmid(time1,4,2)+'-'+strmid(time1,6,2)+' '+strmid(time1,8,2)+':'+strmid(time1,10,2)+':00'
endif

if ~keyword_set(pnglist) then begin
    IF keyword_set(PAN) THEN help,pan
    help,plst
    if n_elements(plst) GT 200 THEN wait,5
;  if ~keyword_set(combine) then begin
    if plst(0) ne '' then generic_movie,plst,bmin,bmax,doxcolors=doxcolors,mpegname=mpegname,pan=pan, hdrs=hdrs, nohdrs=nohdrs,half=half, $
    wcscombine=wcscombine, vsize=imgsize, file_hdr=file_hdr,labels=pngtimes,_EXTRA=_extra
;  endif else begin
;     stop
;     for mkmv=0,n_Elements(cam)-1 do generic_movie,plst[*,mkmv],bmin,bmax,doxcolors=doxcolors,mpegname=mpegname,pan=pan, hdrs=hdrs, $
;     nohdrs=nohdrs,half=half, file_hdr=file_hdr, _EXTRA=_extra,save=cam(mkmv)+'.mvi'
;     mvis=cam+'.mvi'
;     wscc_combine_mvi,mvis,/auto
;  endelse
endif else pnglist=plst 
;rtmviplaypng,camsrch,fileplst=plst,length=length,running_diff=running_diff,skip=skip,noloadct=noloadct

END
