PRO SCCWRITE_DISK_MOVIE, Fname, Img, Hdr0, new=new, maxnum=maxnum, $
                SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, $
                R_SUN= r_sun, ROLL=roll, REPLACE=replace, INDEX=replace_index, $
		RTHETA=rtheta, RTCOORDS=rtcoords, RECTIFIED=rectified, $
		TRUECOLOR=truecolor, CCD_POS=ccd_pos, DEBUG=debug, nx=nx,ny=ny,_EXTRA=_extra
;+
; $Id: sccwrite_disk_movie.pro,v 1.25 2013/07/23 16:14:19 mcnutt Exp $
;
; NAME:
;	SCCWRITE_DISK_MOVIE
;
; PURPOSE:
;	This procedure adds an image to a file in the disk movie (MVI) format.
;
; CATEGORY:
;	LASCO/SECCHI MOVIE
;
; CALLING SEQUENCE:
;	WRITE_DISK_MOVIE, Fname, Img, Hdr
;
; INPUTS:
;	Fname:	File name of the movie disk (MVI) file, with full path
;
;	Img:	2D image array to be added to the file
;
;	Hdr:	FITS header (string or structure) for image
;
; KEYWORD PARAMETERS:
;	/NEW:	If set this keyword indicates that a new file is to be created.
;		The default is to write to an existing file
;
;	MAXNUM:	If set, this keyword specifies the maximum number of files to
;		be loaded into the file.  The default value is 128
;
;	/REPLACE:  	Flag to replace an image rather than append
;	REPLACE_INDEX:	Set to index of frame to replace in .mvi file
;	/RTHETA		flag indicating r-theta movie
;	RTCOORDS	[radius@bottom, radius@top, theta@left, theta@right] 
;			coords for making RTHETA movies
;	RECTIFIED=	Last image has been rotated RECTIFIED degrees CW 
;   	    	    	(same as input to rot() )
;   	    	    	(=1 indicates 180 deg)
;	TRUECOLOR	Set to indicate that the images are truecolor
;   	SUNXCEN=, SUNYCEN=  Set to col, row of center of sun in image (IDL coords!!)
;   	R_SUN=	    	Solar radius in arcsec
;
; PROCEDURE:
;	The disk movie format is:
;
;	file header (each value is 2-byte integer):
;			# images in the file
;			# columns in each image
;			# rows in each image
;			# maximum number of images in file
;			# bytes in image header
;			# version number
;
;	for version 1:	# bytes in file header
;	for version 1:	# sunxcen * 10
;	for version 3:	# R-theta flag
;	for version 3:	# radius at bottom of image (Rsun)
;	for version 3:	# radius at top of image (Rsun)
;	for version 3:	# theta at left of image (deg, 0@N, CCW+)
;	for version 3:	# theta at right of image (deg, 0@N, CCW+)
;	for version 4:	# rectified flag (degrees images rotated by)
;	for version 5:	# indicates true color
;	for version 1:	# red color vector BYTARR(256)
;	for version 1:	# green color vector BYTARR(256)
;	for version 1:	# blue color vector BYTARR(256)
;
;	img hdr #1:	date of image, string (15)
;			time of image, string (15)
;			file name, string (25)
;			filter wheel, string (10)
;			polarizer wheel, string (10)
;			detector, string (10)
;	img hdr #2
;		...
;	img #1
;	img #2
;		...
;
; EXAMPLE:
;
;	Create a new movie file with img being the first image in the file:
;
;		DISK_MOVIE_FILE, '~/mymovie.mvi', img , hdr, /new, maxnum=20
;
;	Add to an existing movie file:
;
;		DISK_MOVIE_FILE, '~/mymovie.mvi', img , hdr
;
;       @(#)write_disk_movie.pro	1.7, 09/23/04 : NRL LASCO LIBRARY
;
; MODIFICATION HISTORY:
; $Log: sccwrite_disk_movie.pro,v $
; Revision 1.25  2013/07/23 16:14:19  mcnutt
; sets rectify to 0 if value is string
;
; Revision 1.24  2011/11/02 14:53:47  mcnutt
; sets rtheta only if keyword_set
;
; Revision 1.23  2011/09/26 18:30:14  mcnutt
; added latplane movie grrid and measuring options
;
; Revision 1.22  2011/05/16 20:46:21  secchib
; nr - fix info messages
;
; Revision 1.21  2011/03/21 18:29:37  mcnutt
; corrected rtheta movie sun center
;
; Revision 1.20  2011/03/18 16:51:01  mcnutt
; set sun(xy)cen from rtcoords if rtheta movie
;
; Revision 1.19  2010/11/17 23:27:38  nathan
; update rectified defn to be roll correction applied
;
; Revision 1.18  2010/10/27 17:04:05  mcnutt
; does not use sec_pix with rtheta movies allows rtheta = 2 for ylog
;
; Revision 1.17  2010/10/20 16:48:20  mcnutt
; sets sec_pix for rtheta movies
;
; Revision 1.16  2010/10/08 18:25:39  mcnutt
; commented out EIT sector
;
; Revision 1.15  2010/09/30 22:03:56  nathan
; Fix bug for HI2 file_hdr.sec_pix; now save value is UINT and when read in
; if neg assume it was saved as UINT
;
; Revision 1.14  2010/08/17 15:53:26  nathan
; set secpol=hdr.wavelnth if wavelnth gt 0
;
; Revision 1.13  2010/02/04 22:29:29  nathan
; always use existing xcen/ycen for sun center
;
; Revision 1.12  2009/08/03 14:46:54  mcnutt
; sets arcs to hdr.cdelt1 id sec_pix lt 0
;
; Revision 1.11  2009/04/06 18:46:01  mcnutt
; change to png and gif header movies
;
; Revision 1.10  2009/03/12 17:13:49  mcnutt
; corrected error datatype img
;
; Revision 1.9  2008/11/03 12:25:10  mcnutt
; added ccd_position key word to include movie ccd_postion in header
;
; Revision 1.8  2008/08/08 23:24:16  nathan
; fix SUN?CEN description
;
; Revision 1.7  2008/05/21 17:02:20  nathan
; fixed problem where roll had too many chars in it for very small neg numbers
;
; Revision 1.6  2008/04/30 21:28:17  nathan
; Clarified that xcen,ycen are defined as IDL coordinates
;
; Revision 1.5  2008/04/10 21:38:12  nathan
; automatically set truecolor if input is 3 dimensions
;
; Revision 1.4  2008/03/17 12:37:27  mcnutt
; added basic header info for wiki page
;
; Revision 1.3  2008/01/11 16:33:32  nathan
; Permanently moved from dev/movie to secchi/idl/display/movie
;
; Revision 1.12  2008/01/09 21:28:43  nathan
; use xcen/ycen in input hdr as default, else use scc_sun_center
;
; Revision 1.11  2007/12/21 15:31:31  nathan
; fix case where sunxcen and hdr.cprix1 are undefined
;
; Revision 1.10  2007/12/11 19:27:53  nathan
; use correct math for xcen,ycen
;
; Revision 1.9  2007/12/10 22:30:15  nathan
; Use crpix instead of xycen for xcen/ycen in mvi header
;
; Revision 1.8  2007/12/07 21:50:31  sheeley
; change debug message
;
; Revision 1.7  2007/12/07 21:46:25  nathan
; set time_obs if not in date field
;
; Revision 1.6  2007/11/16 16:00:23  nathan
; allow hdr to be structure SCCMVIHDR
;
; Revision 1.5  2007/10/17 21:20:20  nathan
; fixed computation of sec_pix for cunit=deg, and fixed computation of xcen,ycen
;
; Revision 1.4  2007/10/17 18:57:51  nathan
; Implemented standard version 6 header structure for SECCHI and LASCO. Should
; be backwards compatible with previous headers labeled version 6.
;
; Revision 1.2  2007/08/30 15:32:10  sheeley
; change roll to float becuase double too big for minhdr when roll is < .01 (nbr)
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst (mvihdr ver 6), Summer 2007
;
; Modified    : SEP  05 Feb 1997 - Mods for mvi version 1 format.
;               SEP  08 Sep 1997 - Added REPLACE option.
;			010711	the jake	Added Version 3 to handle RTHETA Movies
;		thejake	011109 - After testing, additions do not seem to have done any harm so adding
;				WRUNMOVIEM3, MVIPLAY3, WRITE_DISK_MOVIE3, and READ_MVI3 to library.
;				Once an MVI is written with the version 3 software, it will need to
;				be read with it as well.
;		nbr,03.09.10 - 	Incorporate write_disk_movie3.pro; add RECTIFY to mvi v4 header
;		rah, 16 Sep 2004 - Add truecolor keyword
;               aee, 29 Sep 2006 - Added check for SECCHI (vs LASCO/EIT).
;
; 	Written by:	RA Howard, NRL, 16 Mar 1996
;-
;
IF(DATATYPE(hdr0) NE 'STC') THEN hdr=fitshead2struct(hdr0,/dash2underscore) ELSE hdr=hdr0
;debug=1
sz = size(img)

;nb = 128 	;version 5 number of bytes reserved for image header
nb = 136	;version 6 number of bytes reserved for image header

;f_nb = 20	;          ** bytes in file_hdr before color table
;f_nb = 32	;version 4 adds RTHETA
;f_nb = 34	;version 5 adds flag for true color
f_nb = 42	;version 6 adds CCD_POS 
file_hdr_nb = f_nb+256*3 	;** bytes in file_hdr with color table

det = STRTRIM(hdr.DETECTOR)
date_obs=strmid(hdr.date_obs,0,10)
time_obs=strmid(hdr.date_obs,11,12)
IF time_obs EQ '' THEN time_obs=hdr.time_obs
;ind= WHERE(det EQ ['EUVI','COR1','COR2','HI1','HI2'], secchi) 
secpol=hdr.polar
IF tag_exist(hdr,'WAVELNTH') THEN IF hdr.wavelnth GT 0 THEN secpol=hdr.wavelnth
;IF det EQ 'EIT'  THEN secpol=hdr.sector corrected in convert2secchi

d2a=1.
IF tag_exist(hdr,'cunit1') THEN IF strpos(hdr.cunit1,'deg') GE 0 THEN d2a=3600.
; HI is different cdelt
IF (KEYWORD_SET(sec_pix))THEN begin
   if sec_pix gt 0 then arcs = sec_pix else arcs=abs(hdr.cdelt1) 
ENDIF ELSE arcs=abs(hdr.cdelt1*d2a)

IF (KEYWORD_SET(R_SUN))   THEN rsun = r_sun   ELSE rsun=hdr.rsun    ; arcsec!!
IF (~keyword_set(ROLL)) THEN IF tag_exist(hdr,'crota') THEN roll = hdr.crota $	; deg
    ELSE roll=hdr.roll	    ; for structure SCCMVIHDR
; remaining units are "pixels"
xcen=hdr.xcen
ycen=hdr.ycen
; I expect that hdr comes either from scc_mkmovie or generic_movie, so xcen should be there;
; as of 2/4/10, x/ycen is converted to MVI definition in scc_mkframe.pro 

IF (KEYWORD_SET(SUNXCEN)) THEN xcen = sunxcen ;ELSE IF  THEN xcen=hdr.crpix1 - hdr.crval1/hdr.cdelt1
IF (KEYWORD_SET(SUNYCEN)) THEN ycen = sunycen ;ELSE IF  THEN ycen=hdr.crpix2 - hdr.crval2/hdr.cdelt2
; crval is coordinate of crpix!!
IF ~keyword_set(CCD_POS) and tag_exist(hdr,'r1row') THEN ccd_pos = [hdr.r1row, hdr.r1col, hdr.r2row, hdr.r2col]

IF (datatype(truecolor) EQ 'UND')  THEN truecolor=0
IF (sz[0] EQ 3) THEN truecolor=1

IF (KEYWORD_SET(new)) THEN BEGIN
	OPENW,lu,Fname,/GET_LUN
	nf = 0
	IF (truecolor EQ 0)  THEN toff=0 ELSE toff=1
	if keyword_set(nx) then nx=nx else nx = sz(1+toff)
	if keyword_set(ny) then ny=ny else ny = sz(2+toff)
	fhdr = INTARR(f_nb/2)
	IF (KEYWORD_SET(maxnum)) THEN maxn = maxnum ELSE maxn=128
	fhdr(0) = 0
	fhdr(1) = nx
	fhdr(2) = ny
	fhdr(3) = maxn
	fhdr(4) = nb
;	fhdr(5) = 4		;** version 4 - nbr, 8/29/03
;	fhdr(5) = 5		;   version 5 - rah, 9/15/04
	fhdr(5) = 6		;   version 6 - adh, 8/07 added ccd crop, roll, filename is 25 not 15 bytes
	fhdr(6) = file_hdr_nb
	fhdr(7) = FIX(xcen * 10)
	fhdr(8) = FIX(ycen * 10)
	arcsecfac=100.
	;IF det EQ 'HI2' THEN arcsecfac=10.
	; HI-2 requires this, but DETECTOR not in MVI header
	fhdr(9) = UINT(arcs * arcsecfac)

	if KEYWORD_SET(RTHETA) or KEYWORD_SET(RTCOORDS) and (N_ELEMENTS(rtcoords) ge 4) then begin	;"<-------------------------
		message, "--Saving RTHETA movie ",/info	;"	by the jake
		if KEYWORD_SET(RTHETA) then fhdr[10] = RTHETA	;2 for ylog 1 for y linear ;" rtheta flag TRUE
		fhdr[11] = FIX(rtcoords[0]*10)	;" radius0 (bottom radius)
		fhdr[12] = FIX(rtcoords[1]*10)	;" radius1 (top radius)
		fhdr[13] = FIX(rtcoords[2]*10)	;" theta0 (left theta)
		fhdr[14] = FIX(rtcoords[3]*10)	;" theta1 (right theta)
                if (N_ELEMENTS(rtcoords) eq 6) then begin
		  fhdr[7] = FIX(rtcoords[4] * 10) ;  sun center
		  fhdr[8] = FIX(rtcoords[5] * 10) ;
		endif  
	endif else begin
		fhdr[10] = 0					;" rtheta flag FALSE
		fhdr[11] = 0
		fhdr[12] = 0
		fhdr[13] = 0
		fhdr[14] = 0
	endelse		;"<-------------------------

	IF keyword_set(RECTIFIED) and datatype(RECTIFIED) ne 'STR' THEN BEGIN
		rec=rectified
		IF rec EQ 1 THEN rec=180
		IF rec LT 0 THEN rec=rec+360
help,rec
		fhdr[15]=round(rec)
	ENDIF ELSE fhdr[15]=0
	IF KEYWORD_SET(truecolor) THEN fhdr[16]=truecolor ELSE fhdr[16]=0

	IF(keyword_set(CCD_POS)) THEN fhdr[17:20] = ccd_pos

	file_hdr = ASSOC(lu,INTARR(f_nb/2))
	IF keyword_set(DEBUG) THEN BEGIN
	    help,/str,hdr
	    message,'2 sec before continuing..',/info
	    wait,2
	ENDIF
ENDIF ELSE BEGIN
	OPENU,lu,Fname,/GET_LUN
	file_hdr = ASSOC(lu,INTARR(file_hdr_nb/2))
	fhdr = file_hdr(0)
	nf = fhdr(0)
	nx = fhdr(1)
	ny = fhdr(2)
	maxn = fhdr(3)
	nb = fhdr(4)
	;help,nf,nx,ny,maxn,nb,roll
ENDELSE

IF NOT(KEYWORD_SET(REPLACE)) THEN nf = nf+1	;** adding to end
IF (nf GT maxn) THEN BEGIN
	PRINT,'ERROR, number of images greater than maximum allowed, exiting'
	FREE_LUN,lu
	RETURN
ENDIF
fhdr(0) = nf
file_hdr(0) = fhdr

IF (KEYWORD_SET(new)) THEN BEGIN	;** save colortable
	TVLCT, r, g, b, /GET
	red = BYTARR(256) 
	red(0:N_ELEMENTS(r)-1) = r 
	red(N_ELEMENTS(r)-1:255) = r(N_ELEMENTS(r)-1)
	green = BYTARR(256) 
	green(0:N_ELEMENTS(g)-1) = g  
	green(N_ELEMENTS(g)-1:255) = g(N_ELEMENTS(g)-1)
	blue = BYTARR(256) 
	blue(0:N_ELEMENTS(b)-1) = b 
	blue(N_ELEMENTS(b)-1:255) = b(N_ELEMENTS(b)-1)
	file_hdr_r = ASSOC(lu,BYTARR(256), f_nb)
	file_hdr_g = ASSOC(lu,BYTARR(256), f_nb+256)
	file_hdr_b = ASSOC(lu,BYTARR(256), f_nb+256+256)
	file_hdr_r(0) = red
	file_hdr_g(0) = green
	file_hdr_b(0) = blue
ENDIF

img_hdrs = ASSOC(lu,BYTARR(nb),file_hdr_nb)
minhdr = BYTARR(nb)

; -- START Frame header definition
minhdr(0)  = BYTE(date_obs)
minhdr(15) = BYTE(time_obs)
minhdr(30) = BYTE(hdr.filename) ; ver 5 was 15 bytes long
minhdr(55) = BYTE(hdr.filter)
minhdr(65) = BYTE(trim(secpol))
minhdr(75) = BYTE(det)
;-- following new for version 6
minhdr(85) = BYTE(strtrim(xcen,2))
minhdr(95) = BYTE(strtrim(ycen,2))
minhdr(105) = BYTE(strtrim(arcs,2))
minhdr(115) = BYTE(strtrim(rsun,2))
roll1=trim(float(roll))
len=strlen(roll1)
IF len GT 11 THEN $
; needs to be LE 11 chars
    roll2=strmid(roll1,0,7)+rstrmid(roll1,0,4) ELSE $
    roll2=roll1
br=BYTE(roll2) 
;
IF keyword_set(DEBUG) THEN BEGIN
    message,'in debug mode',/info
    print,minhdr
    help,br,minhdr
    ;help,/str,hdr
    help,date_obs,time_obs,secpol,det,xcen,ycen,arcs,rsun,roll,d2a
    print,hdr.filename
    wait,2
ENDIF
minhdr(125) = br
; -- END Frame header definition
IF KEYWORD_SET(REPLACE) THEN index = FIX(replace_index) ELSE index = nf-1
img_hdrs(index) = minhdr

IF datatype(img) eq 'STR' THEN imgs=ASSOC(lu,BYTARR(strlen(img)),LONG(file_hdr_nb)+LONG(nb)*(LONG(maxn))) ELSE BEGIN
  IF KEYWORD_SET(TRUECOLOR) THEN BEGIN
     imgs = ASSOC(lu,BYTARR(3,nx,ny),LONG(file_hdr_nb)+LONG(nb)*(LONG(maxn)))
  ENDIF ELSE BEGIN
     imgs = ASSOC(lu,BYTARR(nx,ny),LONG(file_hdr_nb)+LONG(nb)*(LONG(maxn)))
  ENDELSE
ENDELSE
imgs(index) = BYTE(img)

CLOSE,lu
FREE_LUN,lu

return

END
