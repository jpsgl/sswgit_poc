;$Id: pngmvi_select.pro,v 1.4 2009/04/06 18:38:33 mcnutt Exp $
;
; Project     : SECCHI
;                   
; Name        : pngmvi_select
;               
; Purpose     : called from scc_wrunmoviem when saving a png movie.
;               
; Explanation : 
;
; Use         : must be called in scc_wrunmoviem nedd common block sWIN_INDEXM and moviev
;
;
; Optional Inputs:
;   	
;
; KEYWORDS: 	
;               
; Calls       : 
;
; Side effects: will write over an existing movie if name is the same
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL March 2009.
;               
;	
;-            
;$Log: pngmvi_select.pro,v $
;Revision 1.4  2009/04/06 18:38:33  mcnutt
;added extension select for png or gif files
;
;Revision 1.3  2009/04/02 18:09:18  mcnutt
;added gif option
;
;Revision 1.2  2009/03/20 13:33:55  mcnutt
;check dir permission before creating out movie name
;
;Revision 1.1  2009/03/10 18:57:36  mcnutt
;defines directories for png movies
;

PRO PNGMVI_EVENT, ev

COMMON PNGVAL, pngbase, inbase,filext

WIDGET_CONTROL, ev.id, GET_UVALUE=input
        WIDGET_CONTROL, pngbase, GET_UVALUE=pnginfo
        WIDGET_CONTROL, inbase, GET_UVALUE=moviev

CASE strupcase(input) OF

    'CANCEL' : BEGIN       ;** exit program
    	    WIDGET_CONTROL, /DESTROY, ev.top
	    WIDGET_CONTROL, pnginfo.groupleader, /DESTROY
    	    RETURN
    	    END

    'DONE' : BEGIN       ;
           dash=strsplit(pnginfo.pngdir,'/')
           IF (CHECK_PERMISSION(strmid(pnginfo.pngdir,0,dash(n_Elements(dash)-1))) GT 0) THEN BEGIN
  	      moviev.filepath=pnginfo.pngdir
	      moviev.filename=moviev.filepath+pnginfo.pngfile+filext
              WIDGET_CONTROL, inbase, SET_UVALUE=moviev
    	      WIDGET_CONTROL, /DESTROY, ev.top
	      WIDGET_CONTROL, pnginfo.groupleader, /DESTROY
    	      RETURN
            ENDIF ELSE BEGIN
              ptxt='You do not have permission to make directory '
              widget_control,pnginfo.pngtxt,set_value=ptxt
            ENDELSE
	    END


    'PNGFILEV' : BEGIN 	
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    pngfile=val[0]
	    pnginfo.pngfile = pngfile
    	    END

    'PNGDIRV' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    pngdir=val[0]
	    pnginfo.pngdir = pngdir
    	    END

    'EXTOV' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    filext=val[ev.index]
    	    END

    ELSE : BEGIN
            PRINT, '%%SCC_PLOT_EVENT.  Unknown event.'
    	    END

ENDCASE
        WIDGET_CONTROL, pngbase, SET_UVALUE=pnginfo

END


PRO pngmvi_select,inbase1,gif=gif

COMMON PNGVAL

  inbase=inbase1
   WIDGET_CONTROL, inbase, GET_UVALUE=moviev  ; get structure from UVALUE

        pngdir=moviev.img_hdrs(0).detector+strmid(moviev.img_hdrs(0).FILENAME,strpos(moviev.img_hdrs(0).FILENAME,'.ft')-1,1)+'_'+ $
                            strmid(moviev.img_hdrs(0).DATE_OBS,0,4)+strmid(moviev.img_hdrs(0).DATE_OBS,5,2)+strmid(moviev.img_hdrs(0).DATE_OBS,8,4)+'/'
        pngfile='yyyymmdd_hhmm_cam'

        cd,current=cdir
	cdir=cdir+'/'
	pngdir=cdir+pngdir
	IF (CHECK_PERMISSION(cdir) GT 0) THEN ptxt='Select Directory for PNG/GIF movie files' ELSE ptxt='You do not have permission to make directory '
        

        groupleader = Widget_Base(Map=0)
        Widget_Control, groupleader, /Realize

	pngbase = WIDGET_BASE(/COL, XOFFSET=275, YOFFSET=275, TITLE='PNG MOVIE FILE NAME',group_leader=groupleader,/modal)
	rowb = WIDGET_BASE(pngbase, /ROW)

	pngdirv =     CW_FIELD(rowb, VALUE=pngdir, /ALL_EVENTS, XSIZE=75, YSIZE=1, UVALUE='PNGDIRV', title='Directory: ')
	rowb1 = WIDGET_BASE(pngbase, /ROW)
	pngfilev =   CW_FIELD(rowb1, VALUE=pngfile, /ALL_EVENTS, XSIZE=45, YSIZE=1, UVALUE='PNGFILEV', title='Output Filenames (optional): ')

        if keyword_set(gif) then filext='.gif' else filext='.png'
        exout=['.png','.gif']
        extov= WIDGET_COMBOBOX(rowb1, VALUE=exout, UVALUE="extov")
        IF keyword_set(gif) THEN extype=1 else extype=0
        WIDGET_CONTROL, extov, SET_COMBOBOX_SELECT= extype
        WIDGET_CONTROL, extov, SENSITIVE=1

	rowc = WIDGET_BASE(pngbase, /ROW)
        pngtxt=widget_text(rowc,value=ptxt,uvalue='pngtxt',font='-1',xsize=48,ysize=1)

	done = WIDGET_BUTTON(rowc, VALUE='  DONE  ', UVALUE='DONE')
	cancel = WIDGET_BUTTON(rowc, VALUE=' CANCEL ', UVALUE='CANCEL')

    	;**--------------------Done Creating Widgets-----------------------------**

    	pnginfo={pngtxt:pngtxt, pngfile:pngfile,  pngdir:pngdir, groupleader:groupleader}
	
	WIDGET_CONTROL, pngbase, /REALIZE
        WIDGET_CONTROL, pngbase, SET_UVALUE=pnginfo

	XMANAGER, 'PNGMVI_SELECT', pngbase, EVENT_HANDLER='PNGMVI_EVENT'
    
	
    	;**-----------------End widget --- start writing file ----------------**

END
  

