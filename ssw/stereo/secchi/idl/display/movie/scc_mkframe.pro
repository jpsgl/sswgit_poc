;+
;$Id: scc_mkframe.pro,v 1.191 2017/08/10 21:50:56 nathan Exp $
;
; Project     : STEREO - SECCHI
;                   
; Name        : SCC_MKFRAME
;               
; Purpose     : Generates a SECCHI movie frame.
;               
; Explanation : This procedure uses parameters and keywords from SCC_MKMOVIE to generate
;               a single frame.
;               
; Use         : For the average user, SCC_MKFRAME should not be called on it's own. See scc_mkmovie.pro or
;   	    	scc_mk_image.pro for examples.
;    
; Inputs      : Most of the inputs correspond to keywords in SCC_MKMOVIE.pro. See keyword definitions
;   	    	and call to scc_mkframe in scc_mkmovie.pro.
;               
;   sharpen_ratio   = /USE_SHARP = sharpen(img, ref, factor) = (img/ref) + factor*(img-smooth(img) - (ref-smooth(ref)))
;    	    	    	    sharpen(/NO_RATIO) = img-smooth(img) - (ref-smooth(ref))
;   unsharp 	    = img - smooth(img)
;
; Outputs     : byte-scaled image array and header structure
;               
; Keywords    : /VERBOSE   : Print more info about each frame
;    	    	/GETSUBFIELD: If set, then use box_cursor to retrieve subfield from first frame
;		/AUTOMAX   : Compute bmin and bmax based on median (for EUVI, calls scc_bytscl.pro)
;   	    	/FMASK	   : Apply a mask from file in $SECCHI_CAL
;   	    	/NOPOPUP   : Do not display frame after processing
;   	    	/TWO	     Retrieve corresponding image from other spacecraft and make a B/A movie.
;   	    	    	    Set =2 to display A on left and B on right (default for HI)
;
; Calls       : secchi_prep.pro, scc_sun_center.pro. Option or Input dependent: tvcircle.pro, box_cursor2.pro, 
;   	    	scc_putin_array.pro, scc_add_logo.pro, scc_bytscl.pro, sharpen.pro, scc_getbkgimg.pro, hi_prep, 
;   	    	leefilt.pro, rot.pro, get_stereo_lonlat.pro, getsccsecpix.pro 
;
; Side effects: Creates pixmap.
;               
; Category    : Image Display calibration inout
;               
; See Also    : SCC_MKMOVIE.PRO calls this routine.
;               WSCC_MKMOVIE.PRO is a widget front-end to SCC_MKMOVIE.PRO.
;               
; Prev. Hist. : Removed sections from WMKMOVIE and modified for SECCHI. 
;
; History     : Ed Esfandiari, NRL, Sep 2006.
;               
;-            
;$Log: scc_mkframe.pro,v $
;Revision 1.191  2017/08/10 21:50:56  nathan
;do not set rolld; do not use lamodel=3
;
;Revision 1.190  2015/08/26 17:12:20  hutting
;skips incomplete pbseries and sets skipped for scc_mkmovie
;
;Revision 1.189  2015/01/27 19:37:03  hutting
;remove missing block before calculating median correction
;
;Revision 1.188  2015/01/26 20:21:23  hutting
;added option to adjust difference base median / image median
;
;Revision 1.187  2014/10/22 21:21:50  nathan
;fix tvcircle color=long()
;
;Revision 1.186  2014/09/03 19:51:23  secchia
;return_float set to 0 by default then check bmin and bmax only if autmax is not set
;
;Revision 1.185  2014/04/25 17:41:51  nathan
;IF bmin EQ bmax THEN return_float; moved scc_add_datetime
;
;Revision 1.184  2013/10/28 18:46:38  nathan
;Always do /NOCALFAC with secchi_prep; do HI_PREP of bkg in scc_getbkgimg;
;set SATURATION_LIMIT=65000 if /CALIMG_OFF
;
;Revision 1.183  2013/10/16 16:04:53  nathan
;do not precommcorrect cor2 by default - should not be necessary for pretty pictures
;
;Revision 1.182  2013/09/05 19:22:06  nathan
;utilize REF_BOX=box0 with mk_img.pro
;
;Revision 1.181  2013/09/05 18:16:38  mcnutt
;set ref_box[ab] to 1 if keyword_set box and isla before mk_img is called for lasco images
;
;Revision 1.180  2013/07/03 16:51:35  mcnutt
;defines box_ref before calling mk_img for lasco movies
;
;Revision 1.179  2013/04/25 14:28:59  nathan
;do not use is_tile_compressed.pro, use is_fits instead
;
;Revision 1.178  2013/02/12 22:53:49  nathan
;try again
;
;Revision 1.177  2013/02/12 21:05:28  nathan
;do mreadfits_tilecomp if naxis=0 (for compressed AIA files)
;
;Revision 1.176  2013/01/24 13:53:09  mcnutt
;corrected errors in crop by coord
;
;Revision 1.175  2012/10/17 18:51:30  secchib
;nr - return array early if > 95% zero
;
;Revision 1.174  2012/10/10 12:37:32  mcnutt
;now works with full res AIA fits
;
;Revision 1.173  2012/08/30 19:13:34  nathan
;fix fhsize/hsize again; init putin_array; change init box_cursor box size
;
;Revision 1.172  2012/08/29 20:03:59  nathan
;use fh/vsize not h/vsize for prevbimg and window size
;
;Revision 1.171  2012/08/23 12:35:33  mcnutt
;sets sunc after crop_by_coord
;
;Revision 1.170  2012/08/22 19:51:42  nathan
;print instructions and info if coordcrop
;
;Revision 1.169  2012/08/22 19:27:39  mcnutt
;adjust center in coordcrop to match img_pan
;
;Revision 1.168  2012/08/22 17:59:50  mcnutt
;changed pan to work with FFV when nointerp keyword is set for crop_by_corrds
;
;Revision 1.167  2012/08/16 13:54:11  mcnutt
;correccted use of imagecnt getsubfield and coords keywords
;
;Revision 1.166  2012/07/26 19:36:28  nathan
;scc_sun_center output = type FLOAT not DOUBLE
;
;Revision 1.165  2012/07/13 23:00:57  nathan
;set_colors=256 for Z buffer (in lieu of w256 in make_daily_mvi.pro)
;
;Revision 1.164  2012/05/25 19:42:44  nathan
;oops
;
;Revision 1.163  2012/05/25 19:41:06  nathan
;in some places change first to yloc LE 0; add _EXTRA to scc_getbkgimg()
;
;Revision 1.162  2012/04/12 22:44:59  nathan
;add DAILY= and /INTERP to secchi_prep call for COR1
;
;Revision 1.161  2012/03/23 16:52:21  nathan
;tvcircle bug workaround
;
;Revision 1.160  2012/03/13 20:06:58  nathan
;If /GETSUBFIELD: reset img_xy, change default cursor box, set color of box,
;unset getsubfield after done, decouple getsubfield and coordcrop.
;
;Revision 1.159  2012/03/06 18:45:11  mcnutt
;changed fillcol to a fixed variable in call to tvcircle line 1519
;
;Revision 1.158  2012/03/01 15:28:29  mcnutt
;default to 1024X1024 for AIA
;
;Revision 1.157  2012/01/31 21:04:28  nathan
;reverse previous change and remove getsubfield check from previous coords check
;
;Revision 1.156  2012/01/27 17:21:35  mcnutt
;sets img_xy to coords if set
;
;Revision 1.155  2012/01/25 17:44:08  nathan
;change zero values in ref to median of result; condition LASCO rectify on dorotate
;
;Revision 1.154  2012/01/06 18:25:04  secchib
;corrected error when keyword TWO is not set LS
;
;Revision 1.153  2012/01/04 20:31:16  mcnutt
;added an outer field mask for hi2 1020/img_pan
;
;Revision 1.152  2012/01/03 17:15:00  mcnutt
;sends title into slect_bytscl, and added a title to select crop
;
;Revision 1.151  2011/12/29 18:05:17  mcnutt
;created new varibles for carrington coords
;
;Revision 1.150  2011/12/28 18:24:19  mcnutt
;returns after coordcrop subfield selection
;
;Revision 1.149  2011/12/27 20:31:34  nathan
;do not let fhsize GT fsize
;
;Revision 1.148  2011/12/27 19:11:22  mcnutt
;added crop by coord and AIA combination options
;

;Revision 1.147  2011/11/29 18:17:18  nathan
;fix ~isla for scc_getbkgimg
;
;Revision 1.146  2011/11/28 18:20:30  nathan
;update wscc_mkmovie widget after select_bytscl
;
; Inputs      : Most of the inputs correspond to keywords
;Revision 1.145  2011/11/21 17:52:22  nathan
;new /verbose for bkg; no bkg for aia and print message
;
;Revision 1.144  2011/11/07 22:20:37  nathan
;use /rectify with mk_img.pro
;
;Revision 1.143  2011/10/20 20:49:16  nathan
;adjust lasco bkg labels
;
;Revision 1.142  2011/10/20 18:51:17  mcnutt
;set lasco background to use_model added lasco logo
;
;Revision 1.141  2011/10/19 21:15:10  nathan
;fix if bzero not present
;
;Revision 1.140  2011/10/19 19:31:52  nathan
;updates for LASCO level-1
;
;Revision 1.139  2011/10/07 19:05:47  secchib
;nr - adjusted rejection criteria for imgm
;
;Revision 1.138  2011/08/24 21:49:07  nathan
;fix ind0threshold and mask outer 2 pixels
;
;Revision 1.137  2011/08/09 22:05:50  nathan
;fix hdr.exptime for HI again
;
;Revision 1.136  2011/08/09 21:12:43  nathan
;Set hdr.exptime=1 for HI after secchi_prep
;Return empty array if no bkg found
;Check for hdrm.distcorr NE 'T' before hi_prep on bkg
;Set bmin,bmax if scc_bytscl called
;
;Revision 1.135  2011/08/03 17:59:55  nathan
;fix skip bad polar
;
;Revision 1.134  2011/08/02 19:11:21  mcnutt
;added checks for img(TB) seq(polorizer) cor1 combination movie options
;
;Revision 1.133  2011/07/25 21:00:44  nathan
;forgot to init smooth_box
;
;Revision 1.132  2011/07/21 19:32:44  nathan
;fix coords for subfield
;
;Revision 1.131  2011/07/21 19:08:54  nathan
;fix LASCO sharpen_ratio
;
;Revision 1.130  2011/05/19 23:06:04  nathan
;call mk_img with /INIT
;
;Revision 1.129  2011/05/18 22:13:29  nathan
;fix default coords for cor1 or lasco input
;
;Revision 1.128  2011/05/02 20:15:30  nathan
;rename use_sharp to sharpen_ratio for clarity
;
;Revision 1.127  2011/04/28 18:33:20  mcnutt
;change NO_NORMAL keyword in call to mk_img
;
;Revision 1.126  2011/04/06 19:03:35  nathan
;adjust ind0threshold for noexpcorr case
;
;Revision 1.125  2011/04/06 18:55:20  nathan
;Do not do point_filter in mk_img.pro; use scalefac for point_filter; use
;ind0threshold from biasmean; use date_obs not date_cmd for computing time
;between diff images.
;
;Revision 1.124  2011/04/04 22:38:09  nathan
;add POINTFILT= and NEXPCORR= to mk_img(); move point_filter for other
;
;Revision 1.123  2011/03/22 19:45:29  nathan
;for point_filter use 5,3,3 instead of 5,7,3
;
;Revision 1.122  2011/03/22 19:24:13  mcnutt
;added point filter
;
;Revision 1.121  2011/02/17 21:22:41  nathan
;fix /getsubfield for /two case
;
;Revision 1.120  2011/02/17 17:40:13  nathan
;for AB, rotate to ecliptic plane
;
;Revision 1.119  2011/02/17 16:29:54  nathan
;reverse previous changes
;
;Revision 1.117  2011/02/15 15:36:19  mcnutt
;corrected fmask to work with scc_normailze if TWO and COR
;
;Revision 1.116  2011/01/19 21:00:37  nathan
;adjust isl1 detection and h0,h00 derivation
;
;Revision 1.115  2010/12/29 16:19:15  nathan
;change lower limit to skip from 30 to 10 (c2)
;
;Revision 1.114  2010/12/22 23:42:47  nathan
;add ref_box, box, automax, verbose to mk_img call; re-do AUTOMAX again
;
;Revision 1.113  2010/12/13 20:06:43  mcnutt
;does bytscl if automax and wavelength eq 0
;
;Revision 1.112  2010/11/24 18:16:46  nathan
;change calls to mreadfits, convert2secchi, subfield criteria
;
;Revision 1.111  2010/11/17 23:30:28  nathan
;do all roll correction with scc_roll_image; do all color table with load_secchi_color
;
;Revision 1.110  2010/11/10 21:23:39  nathan
;add ROLL option for lasco; change nan criteria
;
;Revision 1.109  2010/10/13 20:09:16  nathan
;if available, do 1024 size image for subfield
;
;Revision 1.108  2010/10/13 19:41:01  nathan
;re-ordered FIXGAPS actions to fix mis-match with roll-corrected images
;
;Revision 1.107  2010/09/23 18:50:01  mcnutt
;moved final scc_putin_array to before byte scale and masks,  sets sun center mvi definition
;
;Revision 1.106  2010/09/21 18:57:51  nathan
;fix fhsize/hsize/foutsize issues, hopefully
;
;Revision 1.105  2010/09/21 17:47:46  nathan
;adjust fhsize for cor2, hi
;
;Revision 1.104  2010/09/17 22:23:03  nathan
;use mk_img for LASCO, including bkg; fix use of fhsize for RUNNING_DIFF
;
;Revision 1.103  2010/09/15 18:38:56  mcnutt
;put outsize back in call to secchi_prep
;
;Revision 1.102  2010/09/15 18:02:39  mcnutt
;use scc_putin_array to match to background then again for output size and added coordadj to getsubfield to work with lasco and eit images
;
;Revision 1.101  2010/09/14 19:00:04  mcnutt
;added lasco background
;
;Revision 1.100  2010/09/13 20:12:20  nathan
;set h00
;
;Revision 1.99  2010/08/25 20:58:45  nathan
;fix box_ref; fix scc_putin_array criteria; compute r_sun only once
;
;Revision 1.98  2010/08/19 20:36:52  nathan
;fix dorotate criteria; always use FFV for prev and ref image (moved scc_putin_array earlier)
;
;Revision 1.97  2010/08/19 16:08:56  mcnutt
;added level1 keyword to skip secchi_prep is level1 and expo correction
;
;Revision 1.96  2010/08/19 13:58:49  mcnutt
;added lasco occ masks
;
;Revision 1.95  2010/08/18 22:45:57  nathan
;break up verbose help commands
;
;Revision 1.94  2010/08/18 16:58:32  nathan
;use eit_prep for eit
;
;Revision 1.93  2010/08/17 17:59:43  nathan
;fix mreadfits call; change automax criteria
;
;Revision 1.92  2010/08/17 16:04:15  nathan
;Changes to accomodate non-secchi FITS files, mostly in the order things are
;done relative to reading in file.
;
;Revision 1.91  2010/07/08 20:20:35  nathan
;fix ishi bug
;
;Revision 1.90  2010/06/17 16:40:47  nathan
;make sure bmin/max are float
;
;Revision 1.89  2010/06/09 18:12:08  mcnutt
;added outer plantes and Stereo sc to planet labels
;
;Revision 1.88  2010/06/08 15:58:13  secchib
;increased the char size for planets
;
;Revision 1.87  2010/06/08 14:07:51  mcnutt
;added label planet keyword
;
;Revision 1.86  2010/05/18 20:25:54  nathan
;fix cam detection from filename; define pan
;
;Revision 1.85  2010/05/17 21:55:03  nathan
;define running_diff and nocal so doesnot crashcvss diff scc_mkframe.pro
;
;Revision 1.84  2010/05/13 17:40:49  mcnutt
;sets outsize for secchi_prep to 1024 for COR2 to match background images
;
;Revision 1.83  2010/05/06 20:40:27  nathan
;fix automax bug from moving md0
;
;Revision 1.82  2010/04/29 15:10:25  mcnutt
;moves fill with previous image after wnan is defined and image is rotated
;
;Revision 1.81  2010/04/29 15:01:04  mcnutt
;corrected fill with previous image
;
;Revision 1.80  2010/04/28 22:08:31  nathan
;define prevfimg for output size; apply fillcol to NaN from roll IFF fixgaps=1
;
;Revision 1.79  2010/04/27 17:15:48  mcnutt
;resizes output image after background and scaling
;
;Revision 1.78  2010/04/22 16:09:14  nathan
;added fflag to common scc_movie to optionally override non-matching POLAR logic
;
;Revision 1.77  2010/04/20 15:18:50  mcnutt
;change heigth size for nrg filter and added noscl keyword to select_bytscl
;
;Revision 1.76  2010/04/19 18:38:59  mcnutt
;added nrg filter and interactive bytescale select
;
;Revision 1.75  2010/02/26 19:28:37  nathan
;define running_diff
;
;Revision 1.74  2010/02/17 18:07:17  nathan
;define running_diff if not input
;
;Revision 1.73  2010/02/08 22:06:42  secchia
;nr - init firstpol, running_diff
;
;Revision 1.72  2010/02/04 22:30:27  nathan
;always define x/ycen to match MVI definition
;
;Revision 1.71  2010/02/02 23:53:46  nathan
;renamed some common block vars; reorder refcmn defn; set pbser=0 if not cor
;
;Revision 1.70  2010/01/14 21:22:43  nathan
;use smooth_box for sharpen; rejigger datecolor
;
;Revision 1.69  2010/01/14 20:06:22  mcnutt
;added frame select option for ab movies
;
;Revision 1.68  2010/01/14 13:31:36  mcnutt
;added options for other SC on top/left/bottom/right
;
;Revision 1.67  2009/09/24 19:37:21  mcnutt
;update Common block WSCC_MKMOVIE_COMMON to include textframe used in annotate_image
;
;Revision 1.66  2009/06/24 12:09:37  secchib
;added findx to WSCC_MKMOVIE_COMMON block
;
;Revision 1.65  2009/03/18 19:13:26  nathan
;Changed meaning of opts[10] from A and B use all to A and B put A on left
;(TWO=2)
;
;Revision 1.64  2008/11/10 17:58:37  mcnutt
;return ERROR string for bimage if image is skipped do to an error
;
;Revision 1.63  2008/11/05 17:37:35  thompson
;Use select_windows instead of set_plot for better cross-platform compatibility
;Increase search window for pB series:
;
;Revision 1.62  2008-10-28 17:33:48  mcnutt
;added keyword nan_off to scc_roll_image call
;
;Revision 1.61  2008/10/21 17:36:44  mcnutt
;added object option to add Earth, SOHO and other stereo sc positions to HI2 movies
;
;Revision 1.60  2008/10/10 18:25:07  mcnutt
;print hours between image for running difference only
;
;Revision 1.59  2008/10/10 18:17:03  mcnutt
;corrected running difference offset
;
;Revision 1.58  2008/10/08 21:52:09  nathan
;fixed refsz bug
;
;Revision 1.57  2008/10/08 21:43:00  nathan
;fixed rdiff=1 case
;
;Revision 1.56  2008/09/26 23:20:54  secchib
;nr - fix case where running_diff is undefined
;
;Revision 1.55  2008/09/26 19:33:37  nathan
;updated hi_prep for imgm; implement /AUTOMAX for bmax/bmin * median
;
;Revision 1.54  2008/09/26 19:06:02  mcnutt
;added lag variable (n previous images) in running differences movie type
;
;Revision 1.53  2008/09/25 15:33:12  nathan
;Added DOMEDIAN= kw; removed a 2sec wait; if bmin =und or =bmax, then return
;before bytscl
;
;Revision 1.52  2008/09/24 17:18:48  nathan
;use /interp for dorotate to get rid of banding in linear features
;
;Revision 1.51  2008/09/22 16:11:39  nathan
;put time on display image even if NOT /times
;
;Revision 1.50  2008/09/18 21:53:53  nathan
;Added SMOOTH_BOX option; do hi_prep on model before applying model to image;
;use scc_add_datetime to apply timestamp
;
;Revision 1.49  2008/09/15 18:57:10  nathan
;committed 9/9,12 changes
;
;
; 080912,nr - do precommcorrect if crpix = NaN
; 080909,nr - fixed case where nocal,noexpcorr are undefined
;
;Revision 1.48  2008/08/26 20:38:09  nathan
;correct for bad polar value in cor2 double image headers
;
;Revision 1.47  2008/08/25 16:57:48  nathan
;Renamed ratio to doratio so as not to conflict with ratio.pro; added /PHOTOCAL;
;rewrote how keywords are set for secchi_prep, hi_prep, and cor if model is used
;to fix problems with calibration being applied twice; fixed use of first frame
;for base frame; implemented calibration for COR if model is used with DIFF
;
;Revision 1.46  2008/08/20 17:17:03  mcnutt
;works with wscc_mkmovie with pB series options
;
;Revision 1.45  2008/08/14 16:40:11  nathan
;account for img_pan when computing new crpix for subfield
;
;Revision 1.44  2008/08/13 16:05:22  nathan
;fixed bug with polar check (last edit)
;
;Revision 1.43  2008/08/08 23:23:33  nathan
;Check for consistency of polar/wavelnth; extended common scc_movie; retabbed
;secchi_prep section
;
;Revision 1.42  2008/06/20 17:07:51  mcnutt
;does not normalize hi ab movies
;
;Revision 1.41  2008/06/10 14:38:53  secchia
;defined pbser if keyword not set
;
;Revision 1.40  2008/05/29 18:28:32  mcnutt
;made total brightness changes compatable with scc_mk_image
;
;Revision 1.39  2008/05/28 15:55:52  mcnutt
;returns blank frame if PB series is incomplete
;
;Revision 1.38  2008/05/22 18:34:25  mcnutt
;added total brightness option for COR1 and COR2
;
;Revision 1.37  2008/05/20 19:44:37  nathan
;set bimage before skipim
;
;Revision 1.36  2008/05/20 19:05:14  nathan
;use scc_roll_image.pro to correct roll; update common wscc_mkmovie_common
;
;Revision 1.35  2008/05/02 20:00:52  nathan
;define sunc type double; add /nowarp to secchi_prep call
;
;Revision 1.34  2008/04/29 15:39:02  nathan
;redefined first = yloc eq 0; implement box_ref[ab]
;
;Revision 1.33  2008/04/28 19:23:35  nathan
;ensure do not use_model if euvi
;
;Revision 1.32  2008/04/21 15:52:25  nathan
;changed first to look at yloc  not i
;
;Revision 1.31  2008/04/15 20:28:22  nathan
;Update for using Ahead first if /TWO; do side-by-side in mkframe instead
;of scc_mkmovie.pro
;
;Revision 1.30  2008/04/14 15:59:24  nathan
;added daily_med option for use_model
;
;Revision 1.29  2008/04/08 16:37:46  nathan
;set datecolor special for EUVI 171
;
;Revision 1.28  2008/04/04 19:01:20  nathan
;Use sun center not crpix for /DOROTATE; do not DOROTATE if /TWO; apply
;scc_normalize two both A and B for epipolar plane; save binned mask in
;common block and apply scc_normalize to it if applicable; decrease radius
;of outer mask for cor2
;
;Revision 1.27  2008/04/02 16:05:31  nathan
;implemented /TWO
;
;Revision 1.26  2008/03/31 20:29:17  nathan
;always set /bkgimg_off in secchi_prep (for COR1)
;
;Revision 1.25  2008/03/31 19:33:28  nathan
;Initial implementation of /TWO for displaying A and B next to each other.
;To do this required moving the window and tv of final frame to scc_mkframe.pro
;loop.
;
;Revision 1.24  2008/03/11 19:42:17  nathan
;Updated header info; use same fillcol for A and B
;
;Revision 1.23  2008/03/05 17:28:54  nathan
;updated auto-scaling for euvi; added saturation_limit=16384 for HI
;
;Revision 1.22  2008/02/27 17:31:00  mcnutt
;finished removing keyword wsccmv
;
;Revision 1.21  2008/02/27 17:00:09  mcnutt
;removed keyword wsccmv and uses common block WSCC_MKMOVIE_COMMON always
;
;Revision 1.20  2008/02/27 14:18:01  mcnutt
;returns coords from getsubfield to wscc_mkmovie using WSCC_MKMOVIE_COMMON
;
;Revision 1.19  2008/02/26 16:54:23  mcnutt
;added quick fix for getsubfield hsize and vsize
;
;Revision 1.18  2008/02/22 22:49:19  nathan
;Added /GETSUBFIELD -- still need to see about returning COORDS to wscc_mkmovie
;display; implemented foutsize for cases where output is bigger than native
;(such as 2048x2048 HI); rearranged some of the set_plot and window calls
;
;Revision 1.17  2008/02/22 15:40:48  nathan
;consolidate common block var definitions and update criteria for redefinition
;
;Revision 1.16  2008/02/21 21:47:51  nathan
;changed common block variables to contain separate A and B versions of themselves
;
;Revision 1.15  2008/02/21 13:24:01  mcnutt
;uses Zbuffer instead of xwin for euvi pipeline
;
;Revision 1.14  2008/02/20 21:20:36  nathan
;always dorotate outside of secchi_prep
;
;Revision 1.13  2008/02/13 17:23:49  mcnutt
;added ref image to common block, and options for cor rotote and hi exp correction for use wwith use_model
;
;Revision 1.12  2008/02/12 21:01:12  nathan
;reduced edge masking width; use fixed r_occ_out for cor2
;
;Revision 1.11  2008/02/12 19:41:58  nathan
;fix FILL_COL keyword operation
;
;Revision 1.10  2008/02/08 19:37:40  mcnutt
;added automax for euvi and adds one to fill color so keyword works, changed log to scale to add 3 as done in scc_mk_image and added hiprep key word for scc_mk_image
;
;Revision 1.9  2008/02/01 14:58:22  nathan
;remove common mvmask from scc_mkmovie (define in scc_mkframe only)
;
;Revision 1.8  2008/01/24 17:39:38  nathan
;Use /FULL in secchi_prep call; change mdnotbx threshold
;
;Revision 1.7  2008/01/22 21:21:47  nathan
;added _EXTRA to allow other secchi_prep keywords (such as /rotate_on)
;
;Revision 1.6  2008/01/15 14:47:22  mcnutt
;adjusted in and outer occulter masks for cor2 and euvi added limb as input
;
;Revision 1.5  2008/01/11 19:41:37  mcnutt
;added solar limb keyword outer to work with fmask
;
;Revision 1.4  2008/01/11 16:33:31  nathan
;Permanently moved from dev/movie to secchi/idl/display/movie
;
;Revision 1.3  2008/01/11 15:29:11  mcnutt
;added fmask keyword to use the masks from files and changed scc_add_mvi_logo to scc_add_logo
;
;Revision 1.10  2007/12/19 21:33:30  nathan (last revision in dev/movie)
;added /bkgimg_off to secchi_prep call for cor1
;
;Revision 1.9  2007/12/10 22:27:44  nathan
;Implemented subfields; moved much of frame-specific settings from mkmovie to mkframe;
;changed some of the mkframe input parameters; changed date color cutoff from 100 to 130
;
;Revision 1.8  2007/12/06 21:03:33  nathan
;Decoupled NOCAL and NOEXPCORR; print tel above date; variable charsize and color
;for tel/date
;
;Revision 1.2  2007/10/17 21:48:51  nathan
;latest revision from dev/movie (1.7)
;
;Revision 1.7  2007/10/16 20:30:56  nathan
;change error checking to use -9999 instead of -1
;
;Revision 1.6  2007/10/05 21:06:58  nathan
;Major update for SECCHI, using secchi_prep, and making sure options work
;correctly. Still TBD: image subfields, optimize masks, etc.
;
;Revision 1.5  2007/09/06 14:51:43  nathan
;fix getbkgimg call use scc_add_mvi_logo
;
;Revision 1.4  2007/08/17 15:57:51  herbst
;Mods for N.Sheeley Summer 2007
;
;Revision 1.3  2007/03/28 19:41:00  esfand
;secchi movie routines as of Mar 28, 07
;
;Revision 1.2  2006/10/10 20:27:09  esfand
;removed unused comments
;
;Revision 1.1  2006/10/05 20:50:35  esfand
;first virsion - aee
;
;
; 
;____________________________________________________________________________
;

FUNCTION SCC_MKFRAME,fnamen,tai_all,exp_all,bias_all,rolld,fpwvl_all,filter_all, $
                      hdr,i,startind,img_xy,dofull,bmin,bmax,cam, $
                      box_ref,r_occ,r_occ_out,sunc,r_sun, $
                      linethickness, pan, hsize, vsize, coords, $
                      yloc,win_index,camt,xouts,youts,mask_occ, sunxcen, sunycen, $
                      lg_mask_occ,diff,hide_pylon,use_model,box,no_normal,noexpcorr, $
                      doratio,sharpen_ratio,sharp_fact,sqrt_scl,log_scl,running_diff,unsharp,lee_filt, $
                      fill_col,fixgaps,outer,limb,tiff,png,pict,times,ftimes,dbiasx, NEWBKG= newbkg, $
                      bad_skip,logo,sdir,save,tlen,nocal,skipped, DOMEDIAN=domedian, PHOTOCAL=photocal, $
                      docolor,domask,dorotate, TWO=two, GETSUBFIELD=getsubfield, SMOOTH_BOX=smooth_box, $
		      VERBOSE=verbose,AUTOMAX=automax, FMASK=fmask, nopop=nopop, pbser=pbser,objects=objects, $
		      donrfg=donrfg,selectbmin=selectbmin,planets=planets,level1=level1,PFILTER=pfilter,$
		      coordcrop=coordcrop,imagecnt=imagecnt,norm_diff=norm_diff, _EXTRA=_extra

common scc_movie, prevfimg, prevhdr, prevbimg, modtai, hdrm, refcmn ,ref_exp,  fillcol, datecolor, firstpol, fflag, normvals
common mvmask , mvmsk, mskhdr, sunc1
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
;
; --Initializations
;

    !x.crange=[0,0] 	; tvcircle bug workaround
    IF datatype(box_ref) eq 'UND' THEN box_ref=[0.,0.,0.]
    
    IF (DATATYPE(bmin) EQ 'UND' OR DATATYPE(bmax) EQ 'UND') THEN automax=1
    doautomax=keyword_set(AUTOMAX)
    ; automax is changed in mk_img()
    
    IF keyword_set(DEGRID) THEN nodegrid=0 ELSE nodegrid=1
    IF not(keyword_set(pbser)) THEN pbser=0
    IF not(keyword_set(DBIAS)) THEN dbias=0
    IF     keyword_set(DBIAS)  THEN dbiasx=dbias ELSE dbiasx=0
    IF not(keyword_set(SMOOTH_BOX)) THEN smooth_box=0

    IF datatype(yloc) EQ 'UND' THEN yloc=0
    
    IF datatype(running_diff) EQ 'UND'	THEN running_diff=0
    IF datatype(use_model) EQ 'UND' 	THEN use_model=0
    IF datatype(nocal) EQ 'UND'     	THEN nocal=0

   IF keyword_set(LABELPOS) THEN BEGIN
	xouts=labelpos[0]
	youts=labelpos[1]
   ENDIF
   
    bkgoff=1
    pb=0 & mu=0 & percent=0 ; defaults
    IF keyword_set(doratio) THEN nocal=1	; vignetting/flatfield/photometry irrelevant for doratio
    nocalfac=~keyword_set(PHOTOCAL) 	; calfac is photometry; cal is vignetting/flatfield
    doprecommcorrect=0
    cor1pbdiff=0
    nocalimgval=keyword_set(nocal)	    ; these are values used in secchi_prep
    nocalfacval=nocalfac
    noexpcorrval=keyword_set(noexpcorr)
    return_float=0
    IF ~keyword_set(automax) THEN IF bmin EQ bmax THEN return_float=1
;    
;--BEGIN size math
;
   ; -- ALL we need HERE is pan, which is output FFV size 
    IF keyword_set(VIDEO) THEN BEGIN
    	;tlen =16
	IF not(keyword_set(NY)) THEN coords=[0,2047,257,1792]  ;equatorial region
	tlen =19
	IF not(keyword_set(NX)) THEN nx = 640.
	IF not(keyword_set(NY)) THEN ny = 480.
	IF keyword_set(SQUARE) THEN BEGIN
    	    nx = 480
    	    coords = [0,2047,0,2047]
	ENDIF ELSE BEGIN
    	    IF not(keyword_set(PAN)) THEN pan = nx
	ENDELSE
	xouts=36    	;-- location of timestamp
	youts=34 	;+ (ny-480)/2.
    ENDIF ELSE BEGIN
	IF NOT(keyword_set(NY)) THEN ny=0
	IF     keyword_set(NX)  THEN IF (NOT(keyword_set(NY)) or NY EQ NX) THEN pan =nx
	xouts=10
	youts=10
	;tlen =16
	tlen=19
    ENDELSE

; end size math for which knowledge of instrume not needed



   




skipped=0
first = yloc EQ 0	    ; first image displayed
IF yloc LE 0 THEN modtai=[0.,0.,0.]

IF fnamen EQ '' THEN return,bytarr(hsize,vsize) ; this is half of a pair that is not there

BREAK_FILE, fnamen, a, dir, name, ext

    REPEAT BEGIN
        file_exists = file_exist(fnamen)
    	image=-9999
    	
    	IF (file_exists) THEN BEGIN
    	    isfits=is_fits(fnamen,ext)
	    ; check for compressed AIA image
	    IF ext EQ 'BINTABLE' THEN mreadfits_tilecomp,fnamen,h00,image ELSE $
    	    mreadfits,fnamen,h00,image,/dash2underscore,/silent

	    instrume=h00.INSTRUME
	    isse= (instrume eq 'SECCHI')
	    IF isse THEN fichar=reform(byte(strmid(h00.filename,16,1))) ELSE fichar=0
	    IF (fichar LE 57 AND fichar GE 48) or keyword_set(level1) THEN isl1=1 else isl1=0
	    IF not isse or isl1 THEN h0=convert2secchi(h00,/NOCORRECTION) else h0=h00
	    cam=strlowcase(h0.detector)
	    IF isse THEN aorb=STRMID(h0.obsrvtry,7,1) ELSE aorb=h0.TELESCOP
	    IF aorb EQ 'B' THEN ab=1 ELSE ab=0
	    IF aorb EQ 'SDO' THEN ab=2
	    ishi= (cam eq 'hi1' or cam eq 'hi2')
	    isc2= (cam eq 'cor2')
	    isc1= (cam eq 'cor1')
	    iseu= (cam eq 'euvi')
	    isai= (cam eq 'aia' or cam eq 'hmi')
	    isla= (h0.INSTRUME eq 'LASCO')
	    isc3= (cam EQ 'c3')
	    isei= (h0.INSTRUME eq 'EIT')
	    isso=isei+isla
	    isuv=iseu+isai+isei
	    IF isso THEN BEGIN
	    	fichar=strmid(h00.filename,1,1)
    	    	IF not valid_num(fichar) THEN isl1=1 ELSE IF fichar GT 3 THEN isl1=1
    	    ENDIF
	    ; ** Begin size math which require knowledge of instrument

	    ; img_pan, hsize,vsize are the size of the final product, could be subfield
	    ; img0_pan, fhsize, fvsize are for internal use, the size of the full square field of view which comes out of secchi_prep.

	    outsize=0
   	    IF KEYWORD_SET(COORDS) THEN BEGIN
	    	IF (isla or isei) and max(coords GT 1023) THEN coords=coords/2
                IF (isai) and max(coords GT h0.naxis1) THEN coords=coords/2
		img_xy=coords 
	    ENDIF ELSE BEGIN
	    	IF (isse) THEN img_xy=[0,2047,0,2047]  ELSE img_xy=[0,1023,0,1023]
                
    	    ENDELSE
	    
;	    IF datatype(pan) EQ 'UND' or keyword_set(coordcrop) THEN BEGIN
	    IF datatype(pan) EQ 'UND' THEN BEGIN
		IF isc1 THEN pan=512 ELSE IF ishi or isc2 or isla or isei or isai THEN pan=1024 ELSE pan=2048
		; native (background) size as of 2010/05/18
	    ENDIF
	    ;if (isc2) and use_model gt 0 and pan lt 2048 then fhsize=1024 
	    if (isc2) and use_model gt 0 and pan gt 1024 and yloc eq 0 then $
		popup_msg,['COR2 Backgrounds are 1024x1024', $
			   'For larger FFV output you may want to', $
			   'request 2048x2048 backgrounds'],title='SCC_MKFRAME_MSG'
            if isla or isei then maxres=1024. else maxres=2048.
            IF (isai) then maxres=h00.naxis1*1.0
	    IF keyword_set(PAN) THEN img_pan=pan/maxres ELSE img_pan=0.5     ; default is 1024x1024 (half size) 512x512 for LASCO and EIT 

            IF keyword_set(coordcrop) then begin
                if n_elements(coordcrop) eq 6 then begin
		  if ~keyword_set(GETSUBFIELD) then img_xy=[0,coordcrop(3)-1,0,coordcrop(4)-1]
                  if coordcrop(5) ne 1 then img_pan=1.0
                endif
                if n_elements(coordcrop) eq 8 then begin
		  if ~keyword_set(GETSUBFIELD) then img_xy=[0,coordcrop(5)-1,0,coordcrop(6)-1]
                  if coordcrop(7) ne 1 then img_pan=1.0
                endif
       	    endif

	    foutsize=fix(img_pan*maxres)

	    ; need another size var for expanding nominally 1024x1024 images (esp. HI)
	    fhsize=foutsize
	    ; fhsize is for image processing. foutsize is the final, pre-subfield dimension to match the background size
	    IF ((ishi) or (isc2)) and fhsize LT pan THEN fhsize=1024

	    fvsize=fhsize
	    ;    hsize= fix(img_pan*hsize)
	    ;    vsize= fix(img_pan*vsize)
	    IF ~keyword_set(coordcrop) then img_xy=fix(img_pan*img_xy)
	    img0_pan=img_pan

	    ;moved down to be determined after img_pan corrections
	    hsize = img_xy[1]-img_xy[0]+1
	    vsize = img_xy[3]-img_xy[2]+1
	    linethickness=img_pan*4
	    ;
	    ;--END size math
	    ;

	    ; some cases never do these corrections in secchi_prep 
	    ;IF (ishi) THEN  noexpcorrval=1
	    IF isc1 or isc2 THEN BEGIN
    		nocalimgval=1
		nocalfacval=1
		; if background is subtracted, need to apply these after, except in case of COR1 pbseries (see below)
	    ENDIF
	    ;IF isc2 THEN doprecommcorrect=1 

	    IF keyword_set(VERBOSE) THEN quiet=0 ELSE quiet=1
	    ;IF keyword_set(nocal) THEN noexpcorr=1
	    
	    IF datatype(swmoviev) NE 'UND' THEN pbser=swmoviev.pbs
    	    IF ~isc1 and ~isc2 THEN pbser=0
	    ;jk=sccreadfits(fnamen,h0,/nodata)
	    ;--Correct for bad header info 
	    IF h0.seb_prog EQ 'DOUBLE' THEN h0.polar=1001
	    ; check for polar/sector match
	    IF ~finite(h0.crpix1) THEN doprecommcorrect=1
	    IF h0.wavelnth NE 0 THEN thispol=h0.wavelnth ELSE thispol=h0.polar
	    IF (yloc LE 0) or datatype(firstpol) EQ 'UND' THEN BEGIN
	    	firstpol=thispol
		fflag='d'
	    ENDIF
	    IF thispol NE firstpol THEN BEGIN
    	    	print,fnamen
		print,'WARNING: polar/wavelnth ',trim(thispol),' does not match previous value of ',trim(firstpol)
		IF fflag NE 'a' THEN BEGIN
		    fflag=''
		    print,''
		    read,'Enter s to skip, a to accept all, or Return to continue:',fflag
		    
    	    	    ;wait,2
		    IF fflag EQ 's' THEN BEGIN
                    	bimage='ERROR -- Skipping '+fnamen+' because polar/wavelnth of '+trim(thispol)+' does not match first value of '+trim(firstpol)
    	    	    	skipped=1
    	    	    	return,0
		    ENDIF ELSE firstpol=thispol
		ENDIF ;ELSE wait,1
	    ENDIF

            if pbser GT 0 and thispol lt 1000 then begin
	    ; Set polarization series options here
                  if pbser eq 2 then pB=1 
                  if pbser eq 3 then mu=1 
                  if pbser eq 4 then percent=1 
    	    	if isc1  and keyword_set(USE_MODEL) then begin
    	    	    IF keyword_set(DIFF) THEN BEGIN
			bkgoff=0
    	    		cor1pbdiff=1
    	    		nocalimgval=keyword_set(nocal)	   
    	    		nocalfacval=nocalimgval
		    ENDIF
    	    	endif 
    	    	fnamen=get_pbseries(fnamen, SILENT=~keyword_set(VERBOSE))    	    

;	IF n_elements(fnamen) NE 3 THEN return,bytarr(hsize,vsize) ; this is half of a pair that is not there
	    IF n_elements(fnamen) NE 3 THEN begin 
                bimage = 'ERROR -- Skipping '+fnamen+' because part of the pB sequence is missing'
                skipped = 1
                return, 0
             endif 
                pbser_4sp = pbser
            endif else pbser_4sp=0

	    IF keyword_set(VERBOSE) THEN BEGIN
	    	help,pan,img_pan,fhsize,hsize,nocal,first, fill_col , use_model
		help,nocalfacval,ishi,nocalimgval,outsize, noexpcorrval
		help,quiet,docolor,domask,bkgoff,doprecommcorrect,pbser,mu,percent,pb,dorotate
	    	wait,2
		;if dorotate ne 0 then stop
    	    ENDIF 
	    
	    IF isse and not  isl1 THEN BEGIN
    	    	print,'Reading ',fnamen
		IF (nocalimgval) THEN satlimit=65000 ELSE satlimit=16385
		secchi_prep,fnamen, hdr,image0, CALFAC_OFF=nocalfacval, /NOCALFAC, calimg_off=nocalimgval, /update_hdr_off, $
      	    		 OUTSIZE=fhsize,  exptime_off=noexpcorrval, DESMEAR_OFF=nocalimgval, SILENT=quiet, $
;    	    		  exptime_off=noexpcorrval, SILENT=quiet, $
    	    	         smask_on=domask, BKGIMG_OFF=bkgoff,  /nowarp, DAILY=(use_model EQ 2), /INTERPO, $
    	    		/FULL, saturation_limit=satlimit, PRECOMMCORRECT=doprecommcorrect, $
			polariz_on=(pbser_4sp GT 0) ,mu=mu, percent=percent, pB=pB, _EXTRA=_extra  
	    	; if subfield put in FFV, bias is subtracted from zeros
     	    	image=image0>0
	    
	    	;--Correct for bad header info 
    	    	IF hdr.seb_prog EQ 'DOUBLE' THEN hdr.polar=1001

	    	hdr0=hdr
	    ENDIF ELSE IF isei THEN BEGIN 
    	    	print,'Reading ',fnamen
    	    	EIT_PREP, fnamen, fitshdr, image, /RESP, /NRL, /FLOAT, /FILTER, /NO_ROLL, $
    	    	    	    NO_CALIBRATE=nocalimgval, FILL=fltarr(32,32), VERBOSE=~quiet
		; always do roll correction with scc_roll_image.pro later
		hdr = convert2secchi(fitshdr)
		IF ~(NOEXPCORRval) and (NOCALIMGVAL) THEN image=image/hdr.exptime

	    ENDIF ELSE IF isla THEN BEGIN 
                if keyword_set(box) and box_ref[0] EQ 0 then box0=1 ELSE box0=box_ref[0]
	    	lamodel=use_model
		; Map wscc_mkmovie numbers to mk_img numbers:
		;IF use_model EQ 2 THEN lamodel=3
		; always do roll correction with scc_roll_image.pro later
		; but always rectify in mk_img
		; DO NOT do /UNSHARP in mk_img; DO /SHARPEN_RATIO. Use UNSHARP for box size though.
		IF lamodel GT 0 THEN $
		image=mk_img(fnamen,bmin,bmax,fitshdr,USE_MODEL=lamodel,RATIO=doratio, DIFF=diff, SQRT_SCL=sqrt_scale, $
		    LOG_SCL=log_scl, UNSHARP=(smooth_box>3), HIDE_PYLON=isc3, /NO_DISPLAY, PAN=float(fhsize)/1024, INIT=first, $
		    REF_BOX=box0, BOX=box, ROLL=0, AUTOMAX=automax, VERBOSE=verbose, NO_NORMAL=noexpcorrval, $
		    SHARP_FACTOR=sharp_fact, USE_SHARP=sharpen_ratio, RECTIFY=~dorotate) ELSE $

		image=mk_img(fnamen,0,5000,fitshdr, /NO_DISPLAY, PAN=float(fhsize)/1024, ROLL=0, REF_BOX=box0, INIT=first, $
		    BOX=box , VERBOSE=verbose, NOEXPCORR=noexpcorrval,/no_pylon_offs,RECTIFY=~dorotate)

		hdr=convert2secchi(fitshdr, NOCORRECTION=isl1) ; because h00 is strarr 
		box_ref[0]=box0
	    ;ENDIF ELSE IF isai THEN BEGIN
	    
	    ENDIF ELSE  BEGIN
	    	hdr=h0
		IF ~(NOEXPCORRval) and not isl1 THEN image=image/hdr.exptime
		
    	    	IF isai THEN BEGIN
		   ;hdr.filename=name+ext
		   wavelength=fix(hdr.WAVELNTH)
        	   case wavelength of
            	     171: image = image * 1.1
           	     193: image = image / 1.9
           	    ; 284: image = image / 3.4
           	     304: image = image * ssc_get_aia_304_factor(date)
           	    else:
        	   endcase
                ENDIF
	    ENDELSE
	ENDIF ; read in FITS file

       IF image(0) EQ -9999 THEN BEGIN
    	    help,image
	    print,fnamen,': READFITS error?'
    	    print,'Pausing 10 seconds....'
    	    wait, 10
                ;print,'Type .cont to continue...'
                ;stop
                ;goto,skipped
    	ENDIF
       ;IF (hdr.naxis1 GT 2048) THEN BEGIN
       ;  fn= strmid(fnamen,rstrpos(fnamen,'/')+1,50)
       ;  message,fn+' image naxis1= '+STRTRIM(hdr.naxis1,2)+' > 2048  -- Making it 2048x2048.',/CONT
;
       ;  hdr.p1col = hdr.p1col > 51
       ;  hdr.p2col = hdr.p2col < 2098
       ;  image= image(hdr.p1col:hdr.p2col,*)

       ;ENDIF
    ENDREP UNTIL image[0] NE -9999
    
; ** Resize image to match fhsize ----------------------------------------------------
    
    IF hdr.naxis1 ne fhsize or hdr.naxis2 ne fvsize THEN image=scc_putin_array(image,hdr,fhsize,NEW=first)

; -------------------------------------------------------------------------

    IF keyword_set(VERBOSE) THEN  BEGIN
    	help,dbiasx,image
    	print,'Image after secchi_prep:'
    	maxmin,image
	wait,2
    ENDIF

    ; image is corrected for IP including IPSUM, so ipsum=1
    
    IF ~(NOEXPCORRval) and ~ishi THEN hdr.exptime=1.  ; default for secchi_prep
    ; except for HI, which needs original exptime to pass to getbkgimg so bkg, which is divided by exptime but not 
    ; in secchi_prep, has correct value when passed through hi_prep


    image = image -dbiasx

;
; Do point filter before any other corrections (right?)
; MOST importantly, before it goes to refcmn
;
    IF KEYWORD_SET(PFILTER) THEN BEGIN
      scalefac=(16000./max(image))>1.
      help,scalefac
      point_filter,image*scalefac,5,7,3,tmp,pts
      image=tmp/scalefac
    ENDIF

    IF KEYWORD_SET(BOX) THEN BEGIN
    	box1=box*(hdr.naxis1/maxres)	;img_pan*box*fhsize/foutsize
    ENDIF ELSE BEGIN	
    	box1=[176,432,184,440]		; Box around pylon, 8/11/00
    ENDELSE

    IF KEYWORD_SET(BOX) and not (isla)THEN BEGIN
    	box_img = DOUBLE(image(box1[0]:box1[1],box1[2]:box1[3]))
    	good = WHERE(box_img GT 0)
        IF keyword_set(VERBOSE) THEN help,box_ref,box_img,good
	IF keyword_set(VERBOSE) THEN print,'Using box:',box1
        IF (good(0) GE 0) THEN BEGIN
            box_avg=avg(box_img(good))
            IF keyword_set(VERBOSE) THEN help,box_avg
            IF keyword_set(VERBOSE) THEN maxmin,box_img(good)
	    IF box_ref[ab] EQ 0 THEN $
	    IF yloc LE 0 THEN box_ref[ab]=box_avg
            corr_fact = (box_ref[ab]/box_avg)       ;** normalize to counts in box
     	    image = image*corr_fact
	    IF yloc LE 0 THEN normvals=corr_fact ELSE normvals=[normvals,corr_fact]
    	ENDIF ELSE if keyword_set(VERBOSE) THEN print,'Box is all zeros.'
    ENDIF 

;    IF KEYWORD_SET(LG_MASK_OCC) or keyword_set(MASK_OCC) or keyword_set(HIDE_PYLON) THEN $
;    rolld[i]=0 ; AEE

    if tag_exist(h00,'exptime') then ind0threshold=0-0.9*(hdr.biasmean*(hdr.exptime/h00.exptime)) else ind0threshold = 0.
    ind0 = WHERE(rebin(image,fhsize,fhsize) LE ind0threshold, n0)
    IF ind0[0] LT 0 and tag_exist(h00,'bzero') THEN ind0=where(rebin(image,fhsize,fhsize) EQ float(h00.bzero))
    IF ind0[0] LT 0 THEN ind0=where(rebin(image,fhsize,fhsize) LE 0)
        ; ** ind00 is replaced with ind0 and ind0 is is replaced with ind0ref
	;    NOTE: image must be square FFV here!!
;stop
;    IF n0 GT 0.95*(float(fhsize)*fvsize) THEN BEGIN
;    ; image is bad
;    	help,ind0,fhsize,fvsize
;	message,'Bad image detected; returning.',/info
;	wait,30
;	return,intarr(fhsize,fvsize)
;    ENDIF
    ;
    ;--Set base frame
    ;

    dumhdr = hdr
    tai= UTC2TAI(STR2UTC(hdr.date_obs))
    ;hdrm=hdr  ;set to image header used if MODEL is not read in

    IF KEYWORD_SET(USE_MODEL) and (first or (tai-modtai[ab] GT 6*3600) or keyword_set(NEWBKG)) and ~isla THEN BEGIN
    	IF not(cor1pbdiff) and not(isuv) THEN BEGIN
    	; refresh every 6 hours min.
	    szm = size(use_model)
	    IF keyword_set(VERBOSE) THEN print,'Interval since last model is ',trim((tai-modtai[ab])/3600.),' hours; refreshing...'
    	    IF szm(0) LT 2 THEN BEGIN	    
                  IF (use_model EQ 2) THEN use_daily=1 ELSE use_daily=0
		  imgm = scc_GETBKGIMG(hdr, OUTHDR=hdrm, DAILY=use_daily, /match,/interpo,/nonlinearity, _EXTRA=_extra) ;, SILENT=~keyword_set(VERBOSE))      

        	; ** /FFV is 512 or 1024
	    
    	    	IF median(imgm) LE 0 THEN BEGIN
		    message,'invalid bkgimg; returning',/info
		    wait,5
		    return,bytarr(hsize,vsize)
		ENDIF ELSE $
		    message,'Read in background Model',/info

    	    ENDIF ELSE imgm = use_model

            ; for hi do flat field and exposure correction with desmear before model subtraction OR ratio
            ;noexpcorrval=keyword_set(noexpcorr)
	    
	    ; ## hi_prep  is now run only in scc_getbkgimg.pro. -NR, 10/22/13
	    
	    ; 8/9/11, nbr - if exposure correction is run on the bkg image, DISTCORR should be 'T'
	    IF ishi and hdrm.distcorr EQ 'T' THEN imgm=imgm/hdr.exptime
            IF keyword_set(VERBOSE) THEN BEGIN
                print,'Bkg after prep...'
                maxmin,imgm
                print,'hdr.exptime=',hdr.exptime
		window
		cols=img_pan*[1600,400]
		acol=median(reform(image[cols[ab],*]-imgm[cols[ab],*]),5)
		nac=n_elements(acol)
		acol[0:4]=0
		acol[nac-5:nac-1]=0
		plot,acol,titl='Image - Imgm at COL='+trim(cols[ab])
            ENDIF
    	    IF ~(NOEXPCORRval) THEN hdr.exptime=1.


 	    modtai[ab]=tai
    	ENDIF ELSE BEGIN
	    print,''
	    message,'USE_MODEL not valid; ignoring.',/info
	    wait,1
	ENDELSE

    ENDIF

    ;maxfac=2.
    ;minfac=0.
;--Initialize common block variables for a and b sides
    IF (first) or yloc LT 0 THEN  BEGIN
      refsz=size(refcmn)
;refsz=size(refcmn,/dimensions)
      IF refsz[0] EQ 0 THEN fourthd=0 ELSE IF refsz[0] EQ 3 THEN fourthd=1 ELSE fourthd=refsz[4]
      IF refsz[2] NE fhsize or fourthd ne running_diff+1 THEN BEGIN
        refcmn = make_array(3,fhsize,fvsize,running_diff+1,/float)
	; reference image is always square FFV, applied after putin_array
        ref_exp = make_array(3,running_diff+1,/float)
        prevhdr=make_array(3,value=def_secchi_hdr())
    ;fillcol=make_array(2,/float)
        sunc1=make_array(3,value={xcen:0.,ycen:0.})
        datecolor=bytarr(3)
      ENDIF
    ENDIF

    szpfi=size(prevfimg)
    IF szpfi[2] NE fhsize THEN prevfimg=make_array(3,fhsize,fvsize,/float)
    ; ** prevfimg is used to fill gaps with previous image; use rebinned images for this

; **
;
    IF KEYWORD_SET(USE_MODEL) and ~cor1pbdiff and cam NE 'euvi' and not isla and $
    	(first or (tai-modtai[ab] GT 6*3600) or keyword_set(NEWBKG)) THEN BEGIN
    ; IF not model, then first frame or save current frame for running_diff
   	    IF (N_ELEMENTS(imgm) EQ 1) THEN BEGIN
        	PRINT, '%%%SCC_MKFRAME: Model not found'
    	    ENDIF ELSE BEGIN
        	refcmn[ab,*,*] = imgm
        	ref_exp[ab] = hdr.exptime
    	    ENDELSE
    ENDIF ELSE IF yloc LT 0 and ~keyword_set(running_diff) THEN refcmn[ab,*,*] = image

    IF yloc LT 0 THEN refcmn[ab,*,*,running_diff] = image
 
    if keyword_set(RUNNING_DIFF) then begin
        for ird=0,running_diff-1 do begin
	   refcmn[ab,*,*,ird]=refcmn[ab,*,*,ird+1] ;shuffle down ref images
           ref_exp[ab,ird]=ref_exp[ab,ird+1]
	endfor
        ref=(reform(refcmn[ab,*,*,0]))<max(image)
        hdrm=hdr
    endif else begin
        ref=(reform(refcmn[ab,*,*]))<max(image)
    endelse

    ind0ref=WHERE(rebin(ref,fhsize,fhsize) LE ind0threshold)

    IF keyword_set(VERBOSE) THEN BEGIN
    	help,sharpen_ratio,sqrt_scl,log_scl,running_diff,unsharp,fixgaps,lee_filt
	help,automax,noexpcorrval,nocalimgval,ind0ref,ind0,ind0threshold,smooth_box,domedian,diff,doratio
    	wait,2
    ENDIF
    
    if keyword_set(norm_diff) then begin
       z=where(ref ge 0 and image ge 0)
       normval=median(ref[z])/median(image[z])
       print,'normval ='+string(normval)
    endif else normval=1.0    
    IF KEYWORD_SET(DIFF) and ~cor1pbdiff and not(isla and keyword_set(USE_MODEL)) THEN image = TEMPORARY(image*normval) - ref 
                              ;** subtract reference image
    IF KEYWORD_SET(doratio) and not(isla and keyword_set(USE_MODEL)) THEN BEGIN
         
	 IF KEYWORD_SET(sharpen_ratio) THEN $
            image = SHARPEN(temporary(image), ref, sharp_fact, BOX_SIZE=(smooth_box>3)) $
         ELSE IF KEYWORD_SET(SQRT_SCL) THEN $
	    image = SQRT(TEMPORARY(image)>0) / SQRT(ref>0) $
         ELSE IF KEYWORD_SET(LOG_SCL) THEN $
	    image = ALOG10(TEMPORARY(image)>1) / ALOG10(ref>1) $
         ELSE BEGIN                                     ;** divide by reference image
            nonzero = WHERE(ref NE 0, complement=zero, ncomplement=nz)
	    
            image[nonzero] = TEMPORARY(image[nonzero]) / ref[nonzero]
    	    IF keyword_set(VERBOSE) THEN BEGIN
	    	help,zero
    	    	wait,2	    
	    ENDIF
	    IF nz GT 1 THEN image[zero]=median(image)
            uplim=15
            toobig = where(image GT uplim)
         ENDELSE
    ENDIF

    IF KEYWORD_SET(RUNNING_DIFF) THEN BEGIN
         help,running_diff
         refcmn[ab,*,*,running_diff] = image
         image = TEMPORARY(image*normval) - ref                         ;** subtract reference image
	 ref_exp[ab,running_diff] = anytim2tai(hdr.DATE_OBS)
         if yloc ge 0 then begin
	   hdr.exptime=ref_exp[ab,running_diff]-ref_exp[ab,0]
           print,'running difference - Hours between image and ref image = ',hdr.exptime/3600.
         endif
    ENDIF

; ** Resize image here ----------------------------------------------------
    
;    IF hdr.naxis1 ne img_xy[1]-img_xy[0]+1 or hdr.naxis2 ne img_xy[3]-img_xy[2]+1 THEN image=scc_putin_array(image,hdr,fhsize)

; -------------------------------------------------------------------------

    nng=0
    IF datatype(bmin) NE 'UND' THEN ngxy=where(image LT bmin or image GT bmax,nng) 
    IF KEYWORD_SET(UNSHARP) AND NOT(KEYWORD_SET(sharpen_ratio)) THEN BEGIN
         ;IF unsharp EQ 1 THEN uns=25 ELSE uns=unsharp
	 IF keyword_set(SMOOTH_BOX) THEN uns=(smooth_box>3) ELSE uns=3
	 IF nng GT 0 and nng LT (fhsize^2.)/10 THEN image[ngxy]=!values.f_nan
	 tmp = SMOOTH(image,uns,/NAN)
         image = TEMPORARY(image) - tmp                         ;** subtract smoothed image
    ENDIF 

    IF keyword_set(SMOOTH_BOX) and ~keyword_set(UNSHARP) and ~keyword_set(sharpen_ratio) THEN BEGIN
    	IF nng GT 0 THEN image[ngxy]=!values.f_nan
    	image = smooth(temporary(image),smooth_box,/NAN)
	print,'Applying SMOOTH() with box=',smooth_box
    ENDIF
    IF keyword_set(DOMEDIAN) THEN image = median(temporary(image),domedian)
; for cor1 polarized or cor2 do flat field after model subtraction, if not doratio

    IF ((isc1 and ~pbser) or isc2) and ~keyword_set(DORATIO) THEN BEGIN
    	IF (nocal) THEN calimg=1.0 ELSE calimg = GET_CALIMG(hdr)
    	IF (nocalfac) THEN calfac=1.0 ELSE calfac = GET_CALFAC(hdr)
	IF keyword_set(VERBOSE) THEN BEGIN
	    help,calimg,calfac
	    print,'Range of calimg in denominator: '
	    IF ~(nocal) THEN maxmin,calimg
	ENDIF
    	image=calfac*ratio(temporary(image),float(calimg))
    ENDIF

    IF keyword_set(DONRFG) THEN begin
        IF strpos(hdr.cunit1,'deg') GE 0 THEN d2a=1. else d2a=3600.; to convert arcs to deg

         CASE STRLOWCASE(hdr.detector) OF
          'euvi' : htrange=[0,.5]
          'cor1' : htrange=[.4,1.3]
          'cor2' : htrange=[.6,4.5]
          'hi1'  : htrange=[3.9,28]
          'hi2'  : htrange=[22,120]
        ENDCASE
        tsunc = SCC_SUN_CENTER(hdr,FULL=0)	
        image=nrgf(image,htrange=htrange,crpix1=tsunc.xcen,crpix2=tsunc.ycen,pix_size=hdr.cdelt1/d2a)
   endif

    IF (cam EQ 'euvi' and keyword_set(AUTOMAX)) THEN BEGIN
    	IF ~keyword_set(LOG_SCL) THEN BEGIN
	    print,''
	    print,'For EUVI Automatic Scaling, LOG scaling is always applied first.'
	    print,''
	ENDIF
    ENDIF ELSE BEGIN
      IF ( KEYWORD_SET(LOG_SCL) AND NOT(KEYWORD_SET(doratio)) ) THEN BEGIN
          image=image+3
;         b=where(image le 0,dummy)
;         IF dummy ne 0 THEN image(b)=1
          image=ALOG10(image)>0
      ENDIF
      IF ( KEYWORD_SET(SQRT_SCL) AND NOT(KEYWORD_SET(doratio)) ) THEN BEGIN
         image = SQRT((image)>0)
	 ;maxfac=37.
	 ;minfac=0.
      ENDIF
    ENDELSE
    
      IF KEYWORD_SET(LEE_FILT) THEN BEGIN
        lf1 = systime(1)
        image = LEEFILT(temporary(image),3,3)   ; ** NBR, set 4/7/99
        lf2 = systime(1)
        IF keyword_set(VERBOSE) THEN print,'Leefilt took',lf2-lf1,' seconds.'
      ENDIF

    bimage=bytarr(hsize,vsize)
    IF yloc LT 0 THEN goto,skipim   	; first image is base

    if (yloc eq 0 and isai) then begin
       scen = GET_SUN_CENTER(hdr)
       sunc1[ab].xcen=scen.xcen
       sunc1[ab].ycen=scen.ycen
       hdr.xcen=scen.xcen
       hdr.ycen=scen.ycen
    endif
    if (yloc eq 0) then sunc1[ab] = SCC_SUN_CENTER(hdr,FULL=0)
    sunc=sunc1[ab]

     IF keyword_set(VERBOSE) THEN print,'sunc:',sunc

    nz = where (image GT 0,count)
    IF nz(0) NE -1 THEN md0 = median(image(nz)) ELSE md0=0
    IF keyword_set(VERBOSE) THEN help,md0
    
   IF keyword_set(FIXGAPS) THEN BEGIN
    	IF ((fixgaps EQ 1) OR (yloc LE 0)) THEN BEGIN
	    IF (ind0[0] NE -1)	THEN image[ind0] = md0                  ;** gaps in this image
   	    IF keyword_set(RUNNING_DIFF) and (ind0ref[0] NE -1) THEN image[ind0ref] = md0  ;** gaps in ref image
    	ENDIF ELSE BEGIN
 ; ** Use previous image for missing blocks. **   	
     	    IF max(prevfimg[ab,*,*]) EQ 0 THEN prevfimg[ab,*,*]=md0
	    prevn=reform(prevfimg[ab,*,*])
	    IF (ind0[0] NE -1)	THEN image[ind0] = prevn[ind0]   
   	    IF keyword_set(RUNNING_DIFF) and (ind0ref[0] NE -1) THEN image[ind0ref] = prevn[ind0ref]  ;** gaps in ref image
	ENDELSE                
    ENDIF

    prevfimg[ab,*,*]=image
    prevhdr[ab] = hdr 	; set prevhdr and prevfimg AFTER any mods from original full image, EXCEPT roll
    	    	    	; NOTE this is not what is used for RUNNING_DIFF which uses refcmn

    ;--Rotate Solar North Up
    IF keyword_set(DOROTATE) and ~ishi and ~keyword_set(TWO) and abs(hdr.crota) GT 0.7 THEN BEGIN
 	dorotate=-hdr.crota 	; to pass back to scc_mkmovie
   ; Do not rectify SOHO first; rather keep sun center in same location.
    	scc_roll_image,hdr,image, /INTERP    ; --rolls to Solar north up and updates hdr (!)
    	IF keyword_set(VERBOSE) THEN message,'Image rotated to Solar North Up',/info
	
    ENDIF
    ;
    ; MUST DO WNAN AFTER ROTATION
    ;
    wnan=where(finite(image,/nan),nnan)
    if nnan gt 0 then image[wnan]=0
    ;
    ; Do Fill Gaps before rotation and Fill WNAN after rotation; always fill WNAN with median of current image before bytscl
    ;
    IF keyword_set(FIXGAPS) and nnan GT 0    	THEN image[wnan] = md0

; -- End image processing -- Start prep for display
;
; -- If /TWO, need to normalize 2nd image (behind) to match the first one (ahead) before byte-scaling

    hlenz=fhsize/2
    vlenz=hlenz
    y1z=hsize/16
    x1z=y1z
    IF keyword_set(TWO) and ~keyword_set(coordcrop) THEN BEGIN
    	; Ahead is first; do for both, because need to rotate to epipolar plane (?) use B only if A not used ie B|SDO
	if prevhdr[0].naxis1 eq 0 then hdra=prevhdr[1] else hdra=prevhdr[0]
	 if ~isai then eroll = get_stereo_roll(hdr.date_obs, aorb, system='HAE') else eroll=0.0
	if ~ishi then image=scc_normalize(temporary(image),hdr,ref_hdr=hdra, ROTATE_BY=eroll)
	; Note that CRVAL is set to zero and CRPIX is sun center
	IF ab THEN BEGIN
	    ; Behind only
	    x1z=(coords[0])/4.
	    y1z=(coords[2])/4.
	    hlenz=(coords[1])/4. - x1z +1
	    vlenz=(coords[3])/4. - y1z +1
    	; TBD
	; --Adjust coord for rotation difference
    	;latlona=euvi_heliographic(hdra,[coords[0],coords[2]],/carrington)
	; location of lower right corner of box on b
    	    if (yloc eq 0) then sunc1[ab] = SCC_SUN_CENTER(hdr,FULL=0)
    	    sunc=sunc1[ab]
	ENDIF
    	IF keyword_set(DOROTATE) THEN print,'NOTE: Rotating image perpendicular to ECLIPTIC Plane, NOT to solar north.'
    	device,get_screen_size=ss
    ENDIF 
    ;x1=x1z
    ;y1=y1z
    ;hlen=hlenz
    ;vlen=vlenz

    IF fhsize NE foutsize THEN image=scc_putin_array(image,hdr,foutsize)
    sunc = SCC_SUN_CENTER(hdr,FULL=0)

        ;IF keyword_set(VERBOSE) and count GT 1 THEN maxmin,image(nz)
    
    ;IF keyword_set(VERBOSE) THEN BEGIN
    print,'Image before bytscl:'
    maxmin,image

    IF keyword_set(AUTOMAX) THEN md = md0 ELSE md=1.

     IF (first) and datatype(bmin) NE 'UND' THEN BEGIN
        bmax = md * bmax
        bmin = md * bmin
    	; for now, if /TWO, uses min/max of first B image - nr
    ENDIF


    IF keyword_set(VERBOSE) THEN help,bmin,bmax
    IF keyword_set(VERBOSE) THEN wait,2

    if keyword_set(docolor) then begin 
	IF first THEN device, decomposed=0
	; need hdr for wavelnth
	IF first or fflag EQ '' or fflag EQ 'a' THEN load_secchi_color,hdr
    endif ;else loadct,0

    if keyword_set(selectbmin) then begin ; and (first) then begin
        label=hdr.obsrvtry+'  '+hdr.detector
        bimage=select_bytscl(image,bmin,bmax,/noscl,label=label)
    	IF keyword_set(VERBOSE) THEN help,bmin,bmax
    	IF datatype(swmoviev) NE 'UND' THEN BEGIN
    	    swmoviev.bmin=float(bmin)
    	    swmoviev.bmax=float(bmax)
            if keyword_set(imagecnt) then telcnt= imagecnt else telcnt= 0
    	    swmoviev.ubmin(telcnt)=float(bmin)
    	    swmoviev.ubmax(telcnt)=float(bmax)
;   set bmin and bmax values values 
    	    if widget_info(swmoviev.bselminmax,/valid_id) then begin
    	    	swmoviev.selminmax=0
    	    	WIDGET_CONTROL, swmoviev.bselminmax, SET_VALUE=swmoviev.selminmax	    
    	    	WIDGET_CONTROL, swmoviev.bmint, SET_VALUE=trim(swmoviev.bmin)	    
    	    	WIDGET_CONTROL, swmoviev.bmaxt, SET_VALUE=trim(swmoviev.bmax)	    
    	    ENDIF

	ENDIF
	;selectbmin=0
	automax=0   ; make sure is not on
    endif

    IF return_float THEN $
    	bimage=image $
    ELSE $
    IF keyword_set(AUTOMAX) THEN BEGIN
    	bimage=scc_bytscl(image,hdr,minmax=newminmax) 
    	; scc_bytscl works for any image
    	bmin=newminmax[0]
	bmax=newminmax[1]
    ENDIF ELSE bimage= BYTE(((image > bmin < bmax)-bmin)*(!D.TABLE_SIZE-1)/(bmax-bmin))
    automax=doautomax
    
help,bmin,bmax
    nz = where(bimage NE 0, count)
    IF nz(0) GE 0 THEN img_med=median(bimage(nz)) ELSE img_med=0

      IF keyword_set(BAD_SKIP) THEN BEGIN
                IF img_med GT 200 or img_med LT 10 or count LE 1 THEN BEGIN
                        print,'Skipping ',fnamen
                        maxmin,bimage
                        wait,2
			skipped=1
                        GOTO, skipim
                ENDIF
      ENDIF

;
; -- Get stuff for mask
;
    arcs = hdr.cdelt1  ; plate scale (arcs/pixel), updated in secchi_prep
    IF arcs LE 0.0 THEN  arcs = GETSCCSECPIX(hdr) ; also accounts for summing
   ;arcs = arcs/(dofull*(hdr.sumcol * hdr.ipsum)) ; account for summing
   ;arcs = arcs/dofull

    IF (first) THEN BEGIN
    	fillcol=img_med
	; fillcol is used for outer/inner mask only
   ; r_sun is used for cor1 and cor2 to block occulter.
    	r_sun = hdr.rsun   ; radius of sun (arcs) FOR FFV - we are still working with square FFV image
    	IF (r_sun EQ 0.0) THEN BEGIN ; use spice routines to get radius of sun
    	    hee_r= GET_STEREO_LONLAT(hdr.date_obs, aorb, $
                              /au, /degrees , system='HEE',_extra=_extra)
    	    r_sun = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0) ; convert from radian to arcs
    	ENDIF
    	r_sun = r_sun/arcs  ; radius of sun (pixels)
    	help,r_sun, fillcol
    ENDIF
    if datatype(r_sun) eq 'UND' then  r_sun=hdr.rsun/arcs  ; radius of sun (pixels)

    ; for now, if /TWO, uses min/max of first B image - nr
    IF KEYWORD_SET(FILL_COL) THEN IF fill_col GT 0 THEN fillcol=fill_col-1   ;-1 so it can be set as a keyword >0

;
; Apply mask(s)
;
    IF isc1+isc2+ishi+isla GT 0 THEN BEGIN
    	szim = SIZE(bimage)
        horx = szim(1)
        ver1 = 0
        ver2 = szim(2)
        edge = (2*img_pan)>2
        bimage(0:edge-1,*)=fillcol               ; mask edge of  bimage
        bimage(horx-edge:horx-1,*)=fillcol
        bimage(*,ver1:ver1+edge-1)=fillcol
    	bimage(*,ver2-edge:ver2-1)=fillcol
        help,edge
    ENDIF



   rectify=0

IF KEYWORD_SET(FMASK) THEN BEGIN
    szmvsk=size(mvmsk)
    IF szmvsk[2] NE foutsize THEN mvmsk=make_array(2,foutsize,foutsize,/byte)
    if max(mvmsk[ab,*,*]) EQ 0 or yloc eq 0 then begin 
         r_occ=-1
         CASE STRLOWCASE(hdr.detector) OF
          'cor1' : begin 
	              maskfile=getenv('SECCHI_CAL')+'/cor1_mask.fts'
                      r_occ=r_sun * 1.2
                   end
          'cor2' : begin 
	             maskfile=getenv('SECCHI_CAL')+'/cor2'+aorb+'_mask.fts'
	             r_occ=r_sun * 2.4
                   end
          'hi1'  : maskfile=''
          'hi2'  : maskfile=getenv('SECCHI_CAL')+'/hi2'+aorb+'_mask.fts'
          'euvi' : maskfile=getenv('SECCHI_CAL')+'/euvi_mask.fts'
          ELSE   : maskfile=''
        ENDCASE
        if maskfile ne '' then begin
    	    print,'reading in ',maskfile
	    hdr1=prevhdr[ab]	;bin mask to match header size
	    mskim=rebin(sccreadfits(maskfile,mhdr)+1,hdr1.naxis1,hdr1.naxis2)
	    hdr1.filename=mhdr.filename
    	    IF keyword_set(TWO) and  strpos(strlowcase(hdr.detector),'hi') lt 0 THEN $
	    	    	    mvmsk[ab,*,*]=rebin(scc_normalize(mskim,hdr1,ref_hdr=hdra),foutsize,foutsize) ELSE mvmsk[ab,*,*]=rebin(mskim,foutsize,foutsize)
    	    IF datatype(mskhdr) EQ 'UND' THEN mskhdr=make_array(2,value=def_secchi_hdr())
    	    mskhdr[ab]=mhdr
        endif
    endif  
      ;if n_elements(mvmsk) gt 1 then begin 
    bmask=reform(mvmsk[ab,*,*])
    ;nimage=bimage
    bimage(where(bmask LT 2))=fillcol
      ;endif  
endif  

    mdnotbx=median(bimage[xouts:xouts+(hsize/6),youts:youts+20])
    IF (mdnotbx GT 240 and ~(first)) or (mdnotbx GT 180) THEN datecolor[ab]=0 ELSE BEGIN
	datecolor[ab]=255	
	IF hdr.wavelnth EQ 171 THEN datecolor[ab]=240
	; something weird with this secchi_prep color table
    ENDelse

;stop
    IF keyword_set(TIMES) THEN BEGIN
	bimage=scc_add_datetime(bimage,hdr,color=datecolor[ab], /ADDCAM,MVI=times)
    ENDIF
    ; add timestamp without using tvrd() on bimage

IF return_float THEN return,bimage

      CASE STRLOWCASE(hdr.detector) OF
      'cor1' : BEGIN
                r_occ = r_sun * 1.2
                IF KEYWORD_SET(LG_MASK_OCC) THEN r_occ=r_sun * 1.5
                r_occ_out = r_sun * 4.0 
             END
      'cor2' : BEGIN
                if (STRLOWCASE(aorb) eq 'b') THEN r_occ = r_sun * 2.6 else r_occ = r_sun * 2.4
                IF KEYWORD_SET(LG_MASK_OCC) THEN $
		if (STRLOWCASE(aorb) eq 'b') THEN r_occ = r_sun * 3.3 else r_occ = r_sun * 3.0
                ;if(STRLOWCASE(aorb) eq 'b')THEN r_occ_out = r_sun * 16 else r_occ_out = r_sun * 15
                ; R_sun varies with time, so use fixed pixel distance - nr
		if (STRLOWCASE(aorb) eq 'b') THEN r_occ_out = 1036.*foutsize/2048 else r_occ_out = 1031.*foutsize/2048
		;linethickness=5
             END
      'euvi' : BEGIN  ; "Mask Occulter" should be off for EUVI and HI instruments
               LG_MASK_OCC=0 & MASK_OCC=0 & LIMB=0 
               r_occ_out = r_sun * 1.6
             END
      'c1' : BEGIN
                r_occ = r_sun * 1.1
                IF KEYWORD_SET(LG_MASK_OCC) THEN r_occ=r_sun * 1.2
                r_occ_out = r_sun * 3.0
             END
      'c2' : BEGIN
                r_occ = r_sun * 2.1
                IF KEYWORD_SET(LG_MASK_OCC) THEN r_occ=r_sun * 2.2
                r_occ_out = r_sun * 7.95	; was 7.8, 9-19-03
             END
      'c3' : BEGIN
                r_occ = r_sun * 4.2
                IF KEYWORD_SET(LG_MASK_OCC) THEN r_occ=r_sun * 4.3
                r_occ_out = r_sun * 31.0	; changed from 32, 5/25/99
             END
      'eit' : BEGIN  ; "Mask Occulter" should be off for EUVI and HI instruments
               LG_MASK_OCC=0 & MASK_OCC=0 & LIMB=0 
               r_occ_out = r_sun * 1.6
             END
       'hi2'  :  BEGIN  ; "Mask Occulter" should be off for EUVI and HI instruments
               LG_MASK_OCC=0 & MASK_OCC=0 & LIMB=0 
               r_occ_out = 1040*img_pan ;based on a 2048 image size
             END
      ELSE : BEGIN  ; "Mask Occulter" should be off for EUVI and HI instruments
               LG_MASK_OCC=0 & MASK_OCC=0 & LIMB=0 & OUTER=0
             END
      ENDCASE

    set_plot,'z'     
    device,set_resolution=[foutsize,foutsize],set_colors=256

    IF keyword_set(OUTER)THEN BEGIN
    	; for now, do not return sunxcen and sunycen; this is computed for each frame 
	; from frame header in scc_wrunmovie.pro. -nr
    	; hdr should have correct values for WCS routine

    	tmp_img = bimage & tmp_img(*) = 0
;	WSET,2 & TV,tmp_img
        TV,tmp_img
        TVCIRCLE, r_occ_out,hdr.crpix1,hdr.crpix2, /FILL, COLOR=long(1), /device ;255
        tmp_img = TVRD()
        ind1 = WHERE(tmp_img EQ 0)
        IF (ind1[0] NE -1) THEN bimage[ind1] = fillcol
	
    ENDIF
    IF keyword_set(VERBOSE) THEN help,r_occ_out,r_occ,fillcol,img_med
    IF keyword_set(VERBOSE) THEN wait,2

;    WSET, 2
    TV, bimage
      IF (KEYWORD_SET(MASK_OCC) OR KEYWORD_SET(LG_MASK_OCC)) THEN BEGIN
         xadj=0.0 & yadj=0.0
         IF (STRLOWCASE(aorb) eq 'b' and STRLOWCASE(hdr.detector) eq 'cor2' and KEYWORD_SET(LG_MASK_OCC)) then begin
            xadj=(r_sun/3.25) & yadj=(r_sun/1.65)
         endif
	 TVCIRCLE, r_occ, sunc.xcen+xadj, sunc.ycen-yadj, /FILL, COLOR=long(fillcol), /device
      ENDIF

       IF KEYWORD_SET(LIMB) THEN begin
          IF keyword_set(VERBOSE) THEN print,'sunc:',sunc
          TVCIRCLE, r_sun, sunc.xcen, sunc.ycen, COLOR=long(255), THICK=linethickness, /device
	  help,r_sun
      endif

; -- END mask section
; -- Add objects HI2 only
    if keyword_set(objects) and STRLOWCASE(hdr.detector) eq 'hi2' then begin
        IF (first) THEN datecolor[ab]=255
	wcshdr=fitshead2wcs(hdr)
        objcoords=get_object_coords(hdr,wcshdr,STRLOWCASE(aorb))
        if STRLOWCASE(aorb) eq 'a' then  osc = 'B' else osc = 'A'
	xyouts,objcoords(0,0,0),objcoords(0,0,1),osc,charsize=1,color=datecolor[ab],/device
        xyouts,objcoords(1,0,0),objcoords(1,0,1),'E',charsize=1,color=datecolor[ab],/device
        xyouts,objcoords(2,0,0),objcoords(2,0,1),'S',charsize=1,color=datecolor[ab],/device
    endif

; -- Add planets
    if keyword_set(planets) then begin
        IF (first) THEN datecolor[ab]=255
	    wcshdr=fitshead2wcs(hdr)

;  Step through the planets to see if they appear in the image.
;
                   planets = ['Mercury', 'Venus', 'Earth', 'Mars', $
                                 'Jupiter Barycenter', 'Saturn Barycenter','URANUS BARYCENTER',$
				 'NEPTUNE BARYCENTER','PLUTO BARYCENTER','STEREO AHEAD','STEREO BEHIND']
                   names = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn','Uranus','Neptune','Pluto','A','B']

            for iplanet = 0,n_elements(planets)-1 do begin
                lonlat = get_stereo_lonlat(hdr.date_obs, hdr.obsrvtry, $
                               system='HPC', target=planets[iplanet], /deg)
                lonlat = lonlat[1:2]
                if wcshdr.cunit[0] eq 'arcsec' then lonlat = lonlat * 3600.
                pixel = wcs_get_pixel(wcshdr, lonlat)

;  If the planet is within the image, then mark it and label it.
;
                if (pixel[0] gt 0) and (pixel[0] lt n_elements(bimage(*,0))) and (pixel[1] gt 0) $
                  and (pixel[1] lt n_elements(bimage(0,*))) then begin
                    ;circle_sym, thick=2, symsize=3img_xy
                    ;plots, pixel[0], pixel[1], /device, psym=8
;
                    name = names[iplanet]
                    nlength = strlen(name) * !d.x_ch_size
                    istart = pixel[0] + (n_elements(bimage(0,*))/512)*5
                    if istart lt 1 then istart = 1
                    iend = istart + nlength
                    if iend gt n_elements(bimage(0,*))-1 then istart = (n_elements(bimage(0,*))-1) - nlength
                    ;jpos = pixel[1] - 30
                    ;if jpos lt 1 then jpos = jpos + 60
                    jpos= pixel[1] + (n_elements(bimage(0,*))/512)*5
                    xyouts, istart, jpos, name, charsize=n_elements(bimage(0,*))/512 ,color=datecolor[ab],/device
                endif
            endfor              ;iplanet
    endif



    bimage0=bimage
    bimage = TVRD()
;    set_plot,'x'
    select_windows
	

; -- BEGIN subfield extraction
    IF keyword_set(GETSUBFIELD) and (first)  and ~keyword_set(coordcrop) THEN BEGIN
    	device,get_screen_size=ss
	IF ss[1] GT 1023 and foutsize GT 512 THEN ssx=1024 ELSE ssx=512
    	WINDOW, 0,XSIZE = ssx, YSIZE = ssx
	tv,rebin(bimage,ssx,ssx)
        label=hdr.obsrvtry+'  '+hdr.detector
        xyouts,5,5,label,color=255,charsize=2,charthick=2,/device
	IF keyword_set(TWO) and (ab) THEN BEGIN
	    print,''
	    print,'ONLY HORIZONTAL POSITION WILL BE USED'
	    print,''
	ENDIF
	coordadj=fix(maxres/ssx)
	IF keyword_set(TWO) AND keyword_set(imagecnt) THEN BEGIN; and (ab) using imagecnt keyword for SDA|A
	; Special case for 2nd image of A/B pair - keep original size and vertical location
	    x1z=coords[0]/coordadj
	    y1z=coords[2]/coordadj
	ENDIF 
	hlen=ssx/2
    	vlen=hlen
    	x1=  ssx/4
    	y1=  x1
	help,x1,y1,hlen,vlen
	
    	box_cursor2,x1,y1,hlen,vlen,/message,/init,color=1000
	IF keyword_set(VERBOSE) THEN help,x1z,y1z,hlenz,vlenz,x1,y1,hlen,vlen
    	
	IF keyword_set(TWO) and keyword_set(imagecnt) $;(ab)
	THEN coords=[x1,x1+hlenz-1,y1z,y1z+vlenz-1]*coordadj $
	ELSE coords=[x1,x1+hlen-1 ,y1 ,y1 +vlen-1] *coordadj
	; Adjust to be div by 8 so is consistent to 256x256, and matching A/B sizes
	coords=((coords+1.)/8)*8 - 1
    	IF datatype(swmoviev) NE 'UND' THEN BEGIN
    	    swmoviev.x1=coords(0)
    	    swmoviev.x2=coords(1)
    	    swmoviev.y1=coords(2)
    	    swmoviev.y2=coords(3)
   	    if widget_info(swmoviev.bsub,/valid_id) then begin
                swmoviev.gsub=0
	        WIDGET_CONTROL, swmoviev.bsub, SET_VALUE=swmoviev.gsub	    
    	    	WIDGET_CONTROL, swmoviev.x1t, SET_VALUE=swmoviev.x1	    
    	    	WIDGET_CONTROL, swmoviev.x2t, SET_VALUE=swmoviev.x2	    
    	    	WIDGET_CONTROL, swmoviev.y1t, SET_VALUE=swmoviev.y1	    
    	    	WIDGET_CONTROL, swmoviev.y2t, SET_VALUE=swmoviev.y2	    
    	    ENDIF
	ENDIF
	img_xy=img_pan*coords
    	hsize = img_xy[1]-img_xy[0]+1
    	vsize = img_xy[3]-img_xy[2]+1
;	stop
    	getsubfield=0
    ENDIF

    IF img_xy[0] NE 0 or img_xy[1] NE foutsize-1 or img_xy[2] NE 0 or img_xy[3] NE foutsize-1 and ~keyword_set(coordcrop) THEN BEGIN
    	
        print,'Extracting ',img_xy

    	bimage = bimage[img_xy[0]:img_xy[1],img_xy[2]:img_xy[3]]           ; moved 12/20/01, nbr
    	imsz = size(bimage)

	;IF hsize MOD imsz(1) NE 0 and imsz(1) MOD hsize NE 0 THEN BEGIN
    	;    bimage = CONGRID(bimage, hsize, vsize,/interp)      
        ;    IF keyword_set(VERBOSE) THEN print,'Using CONGRID.'
	;ENDIF ELSE BEGIN
        ;    IF img0_x2-img0_x1 LT 256 or img0_y2-img0_y1 LT 256 THEN smpl = 1 ELSE smpl=0
    	;    bimage = REBIN(bimage, hsize, vsize,SAMPLE=smpl)               
	;ENDELSE
	
	; Image should only be resized in secchi_prep. nr, 12/7/07
	
	; -- Need to reset header parameters for scc_sun_center.pro
	hdr.crpix1=hdr.crpix1-coords[0]*img_pan
	hdr.crpix2=hdr.crpix2-coords[2]*img_pan
	sunc.xcen=sunc.xcen  -coords[0]*img_pan
	sunc.ycen=sunc.ycen  -coords[2]*img_pan
	IF keyword_set(VERBOSE) THEN print,'New crpix:',hdr.crpix1,hdr.crpix2
	; -- Reset R-vals for ccd_pos keyword in mvi header (set in sccwrite_disk_movie.pro)
	r1col=hdr.r1col
	r1row=hdr.r1row
	hdr.r1col=r1col+coords[0]
	hdr.r2col=r1col+coords[1]
	hdr.r1row=r1row+coords[2]
	hdr.r2row=r1row+coords[3]
    ENDIF
; -- END subfield

    if keyword_set(coordcrop) then begin
       IF keyword_set(GETSUBFIELD) and (first) and ~keyword_set(imagecnt) THEN BEGIN
          wcs = fitshead2wcs(hdr)
    	  device,get_screen_size=ss
	  IF ss[1] GT 1023 and foutsize GT 512 THEN ssx=1024 ELSE ssx=512
    	  WINDOW, 0,XSIZE = ssx, YSIZE = ssx
	  tv,rebin(bimage,ssx,ssx)
          label=hdr.obsrvtry+'  '+hdr.detector
          xyouts,5,5,label,color=255,charsize=2,charthick=2,/device
	  coordadj=fix((maxres*img_pan)/ssx)
          if n_elements(coordcrop) eq 6 then begin
	    print,'Click location to follow on the image.'
 	    cursor,x1,y1,/device
	    print,'Using pixel',x1,',',y1
  	    cl=euvi_heliographic(wcs,[x1*coordadj,y1*coordadj],/CARRINGTON)
	    print,'which is CRLN,CRLT ',cl
	    coordcrop(0:1)=cl
	    IF datatype(swmoviev) NE 'UND' THEN BEGIN
   	      swmoviev.carx1=coordcrop(0)
   	      swmoviev.cary1=coordcrop(1)
	    ENDIF
          endif
	  if n_elements(coordcrop) eq 8 then begin
	    hlen=hlenz/coordadj
	    vlen=vlenz/coordadj
	    x1=   x1z
	    y1=   y1z
	    box_cursor2,x1,y1,hlen,vlen,/message,/init
  	    cl1=euvi_heliographic(wcs,[x1*coordadj,y1*coordadj],/CARRINGTON)
            coordcrop(0)=cl1(0) & coordcrop(2)=cl1(1)
  	    cl2=euvi_heliographic(wcs,[(x1+hlen-1)*coordadj,(y1+vlen-1)*coordadj],/CARRINGTON)
            coordcrop(1)=cl2(0) & coordcrop(3)=cl2(1)
            if coordcrop(1) lt coordcrop(0) then $
	      coordcrop(5)=fix(((coordcrop(1)+360.)-coordcrop(0))/coordcrop(4)) else $
	        coordcrop(5)=fix((coordcrop(1)-coordcrop(0))/coordcrop(4))
            coordcrop(6)=fix((coordcrop(3)-coordcrop(2))/coordcrop(4))
       	    IF datatype(swmoviev) NE 'UND' THEN BEGIN
	        swmoviev.carx1=coordcrop(0)
	    	swmoviev.carx2=coordcrop(1)
 	   	swmoviev.cary1=coordcrop(2)
 	   	swmoviev.cary2=coordcrop(3)
  	   	swmoviev.coordsize=coordcrop(4:6)
	    ENDIF
            hsize=coordcrop(5)
            vsize=coordcrop(6)
 	  ENDIF
   	    if widget_info(swmoviev.bsub,/valid_id) then begin
              swmoviev.gsub=0
	      WIDGET_CONTROL, swmoviev.bsub, SET_VALUE=swmoviev.gsub	    
	      WIDGET_CONTROL, swmoviev.carx1t, SET_VALUE=swmoviev.carx1	    
	      WIDGET_CONTROL, swmoviev.carx2t, SET_VALUE=swmoviev.carx2	    
	      WIDGET_CONTROL, swmoviev.cary1t, SET_VALUE=swmoviev.cary1	    
	      WIDGET_CONTROL, swmoviev.cary2t, SET_VALUE=swmoviev.cary2	    
	      for i=0,2 do WIDGET_CONTROL, swmoviev.coordw(i), SET_VALUE=swmoviev.coordsize(i)
	    endif
          return,coordcrop
        endif
	if n_elements(coordcrop) eq 8 then bimage=crop_by_coord( bimage, hdr, [coordcrop(0),coordcrop(2)], [coordcrop(1),coordcrop(3)], [coordcrop(5),coordcrop(6)],nointerp=coordcrop(7), _EXTRA=_extra)
	if n_elements(coordcrop) eq 6 then bimage=crop_by_coord( bimage, hdr, [coordcrop(0)], [coordcrop(1)], [coordcrop(3),coordcrop(4)],nointerp=coordcrop(5), _EXTRA=_extra)
        sunc.xcen=hdr.xcen
        sunc.ycen=hdr.ycen
   endif 

    hdr.xcen=sunc.xcen
    hdr.ycen=sunc.ycen

    ; Now this field matches MVI definition; used without change in sccwrite_disk_movie.pro
    
    IF (DATATYPE(hdr) EQ 'STC') THEN BEGIN
    	;efac = 1.
    	;ebias = GET_BIASMEAN(hdr,/silent) 
    	;exp_all(i) = GET_EXPTIME(hdr,/silent)
    	;bias_all(i) = ebias
    	;fpwvl=hdr.wavelnth
    	;filter=hdr.filter
    	;fpwvl_all(i)=fpwvl
    	;filter_all(i)=filter
    	;all_hdr(i) = hdr
    	;tai_all(i)= tai
	;--I believe all these arrays are unused. nr, 2/20/08
    ENDIF
    
;    IF keyword_set(LOGO) THEN bimage = SCC_ADD_mvi_LOGO(temporary(bimage))
    IF keyword_set(LOGO) and not(isla) and not(isai) THEN bimage = SCC_ADD_LOGO(temporary(bimage))
    IF keyword_set(LOGO) and isla THEN bimage = add_lasco_logo(temporary(bimage))

;    WINDOW, XSIZE = hsize, YSIZE = vsize, /PIXMAP ,/FREE
    ;device,set_resolution=[hsize,vsize]
    ;TV, bimage

    ftimes(yloc)= STRMID(UTC2STR(TAI2UTC(tai), /ECS, /TRUNCATE),0,tlen)

;help,i,yloc,ftimes(yloc),times

    IF keyword_set(VERBOSE) THEN help,xouts,youts,times,mdnotbx

    ;bimage = TVRD()
    pimgsz=size(prevbimg)
    IF pimgsz[2] NE hsize THEN prevbimg=make_array(3,hsize,vsize,/byte)
    IF n_elements(pimgsz) GT 3 THEN if pimgsz[3] NE vsize THEN prevbimg=make_array(3,hsize,vsize,/byte)
    prevbimg[ab,*,*]=bimage
    
    hsize2=hsize
    vsize2=vsize
    abgo=0
    IF keyword_set(TWO) then if ab eq 1 and two lt 7 then abgo=1
    if ab eq 2 then abgo=1
    IF keyword_set(TWO) and (abgo) THEN BEGIN
    ; 2nd time
	;need to recreate the image with both images
	xf=1
	yf=1
	xs=0    ; for dividing line
	ys=0
;	IF hsize*2 GT ss[0] and vsize*2 LT ss[1] THEN BEGIN
        IF TWO ge 3 and TWO lt 5 THEN BEGIN
	    yf=2 
	    ys=2
	ENDIF ELSE BEGIN
	    xf=2
	    xs=2
	ENDELSE
        IF TWO eq 7 THEN THREE=1 ELSE THREE=0
	hsize2=hsize*(xf+three)+(xs*(three+1))
	vsize2=vsize*(yf)+ys
    	set_plot,'z'
	device,set_resolution=[hsize2, vsize2] 
;	IF strmid(cam,0,2) EQ 'hi' or two EQ 2 THEN BEGIN
	IF two EQ 2 or two EQ 3 THEN BEGIN
	    ; reverse A and B so A is on left or A is on bottom
	    image1=0
	    image2=1
	ENDIF 
	IF two EQ 1 or two EQ 4 THEN  BEGIN
	    image1=1
	    image2=0
	ENDIF 
	IF two EQ 5 THEN  BEGIN
	    image1=2
	    image2=0
	ENDIF 
	IF two EQ 6 THEN  BEGIN
	    image1=1
	    image2=2
	ENDIF 
	IF two EQ 7 THEN BEGIN
	    image1=1
	    image2=2
            image3=0
	ENDIF
    	TV, prevbimg[image1,*,*]
	TV, prevbimg[image2,*,*],hsize*(xf-1)+xs,vsize*(yf-1)+ys
	IF keyword_set(VERBOSE) and (two EQ 2 or two EQ 3) THEN print,'Behind image offset by ',trim(hsize*(xf-1)+xs),',',trim(vsize*(yf-1)+ys)
	IF keyword_set(VERBOSE) and (two EQ 5 or two EQ 1 or two eq 4) THEN print,'Ahead image offset by ',trim(hsize*(xf-1)+xs),',',trim(vsize*(yf-1)+ys)
	IF two EQ 7 THEN TV, prevbimg[image3,*,*],hsize*(xf)+(xs*2),vsize*(yf-1)+ys
	IF keyword_set(VERBOSE) and two EQ 7 THEN print,'Ahead image offset by ',trim(hsize*(xf)+(xs*2)),',',trim(vsize*(yf-1)+ys)
    	bimage=TVRD()
	;set_plot,'x'
	select_windows
    ENDIF    	
    
    tvlct,r,g,b,/get
    rgb=[[r],[g],[b]]

;next lines added for zbuffer change over
    
    IF ~keyword_set(nopop) THEN BEGIN
    	IF (first) THEN WINDOW, 0,XSIZE = hsize2, YSIZE = vsize2
	wset,0
    	IF keyword_set(TWO) and ~first THEN BEGIN
	    IF (ab) THEN TV, bimage
	ENDIF ELSE tv, bimage
    	IF ~keyword_set(TIMES) THEN XYOUTS,10,10, ftimes(yloc), /DEVICE, COLOR=datecolor[ab]

    ENDIF

    IF keyword_set(VERBOSE) THEN wait,2
    skipim:

    RETURN, bimage

END
