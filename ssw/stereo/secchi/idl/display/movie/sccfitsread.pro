;+
;
; $Id: sccfitsread.pro,v 1.2 2008/03/17 12:37:27 mcnutt Exp $
;
; Name        : SCCFITSREAD.PRO -- load a LASCO or SECCHI fits file
;
; Purpose     : read fits files
;               
; Explanation : calls secchi_prep for files in list and resaves output fits files
;               
; Use         : to write fits files after secchi_prep
;    
; Inputs      : list, savedir
;               
; Outputs     : hdrs, imgs  : returned from secchi_prep
;               
; Keywords    : /NODATA : return header only
;		/SECCHI : absolete deteremind by fits files name
;		/PREP   : image return from secchi_prep with no additional keywords
;		/LOCAL  : use sccload instead of sccreadfits
;
; Calls       : secchi_prep.pro or sccload.pro or sccreadfits or lasco_readfits
;
; Side effects: 
;               
; Category    : Image Display calibration inout
;
;
; $Log: sccfitsread.pro,v $
; Revision 1.2  2008/03/17 12:37:27  mcnutt
; added basic header info for wiki page
;
; Revision 1.1  2007/08/17 17:31:38  nathan
; moved from dev/movie
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;
;-

FUNCTION sccfitsread, filename, img, hdr, NODATA=nodata, SECCHI=secchi, PREP=prep, LOCAL=local

prep = keyword_set(PREP)
local = keyword_set(LOCAL)

BREAK_FILE, filename, dl, dir, name, ext

secchi = STRLEN(name + ext) EQ 25

IF(secchi) THEN BEGIN
	IF(prep) THEN secchi_prep, filename, hdr, img $
	ELSE IF(local) THEN sccload, filename, hdr, img, NODATA=nodata $
	ELSE img = sccreadfits(filename, hdr, NODATA=nodata)
ENDIF ELSE BEGIN
	img = lasco_readfits(filename, hdr, NO_IMG=nodata)
ENDELSE

RETURN, img

END
