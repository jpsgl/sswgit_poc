;+
PRO printmvihdr, filename, FRAME=frame
;
;$Id: printmvihdr.pro,v 1.2 2009/11/13 18:00:15 nathan Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Purpose     : Print contents of header of MVI file
;
; INput:
;   filename	Name of MVI file
;               
; Keywords:
;   FRAME=  	Set frame number for which to print header; default is frame=0
;
; Explanation : 
;
; Calls       : sccread_mvi, sccmvihdr2struct
;
; Category    : movie mvi diagnostic utility
;               
; Written     : N.Rich, Nov.2009
;-               
;$Log: printmvihdr.pro,v $
;Revision 1.2  2009/11/13 18:00:15  nathan
;header doc
;
;Revision 1.1  2009/11/13 17:46:50  nathan
;commit
;

OPENR,lu,filename,/GET_LUN
SCCREAD_MVI, lu, file_hdr, ihdrs, imgsptr, swapflag

IF keyword_set(FRAME) THEN inx=frame ELSE inx=0
ahdr = SCCMVIHDR2STRUCT(ihdrs[inx],file_hdr.ver)

Print,filename
help,/str,file_hdr
print,file_hdr.ccd_pos
print,'Frame ',trim(inx),':'
help,/str,ahdr

END
