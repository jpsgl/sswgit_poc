;---------------------------------------------------------------------------------------------------
;$Id: define_wcshdrs.pro,v 1.26 2013/10/09 15:32:42 nathan Exp $
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : define_wcshdrs.pro
;               
; Purpose     : to converts movie frame hdrs to wcshdrs 
;               
; Explanation : 
; KEYWORDS:
;	CURRENT	 return current frame wcs header only
;	WCS	variable to contain current wcs header moviev.wcshdrs not defined
;	system	sets wcs system
;       HDRS : defeins hdrs when out side of movie routines when moviev is not defined
;
;$Log: define_wcshdrs.pro,v $
;Revision 1.26  2013/10/09 15:32:42  nathan
;only use hi_fix_pointing if valid pointing calibration found
;
;Revision 1.25  2013/07/05 12:18:12  mcnutt
;removed stop
;
;Revision 1.24  2013/07/05 10:57:50  mcnutt
;sets osc after defining moviev.osczero
;
;Revision 1.23  2013/07/02 17:28:56  mcnutt
;check for osczero before defining osc
;
;Revision 1.22  2013/07/02 14:52:20  mcnutt
;added check for valid sun center in frame header when determining if HI image is a subfield
;
;Revision 1.21  2013/06/17 18:33:59  mcnutt
;does not change file to sc A if not an AB movie if fname eq nofits
;
;Revision 1.20  2013/06/12 16:41:40  nathan
;If ishi always use hi_fix_pointing because original keywords in header are
;way off. Just check for subfield.
;
;Revision 1.19  2013/05/30 22:01:23  nathan
;change sc1 to sc after if fname eq nofits
;
;Revision 1.18  2013/05/23 13:33:59  mcnutt
;corrected logic for AB movies when fits file are not available
;
;Revision 1.17  2013/03/13 16:18:43  nathan
;clarify notes, /info
;
;Revision 1.16  2012/11/19 18:58:38  mcnutt
;always use ahdr crpix and sec_pix values
;
;Revision 1.15  2012/07/30 13:26:09  mcnutt
;check for secchi dir if not found sets nofits to 1
;
;Revision 1.14  2012/06/14 20:57:22  nathan
;implement correction to xcen/ycen from generic_movie bug
;
;Revision 1.13  2012/05/21 18:27:23  nathan
;remove wait
;
;Revision 1.12  2012/05/21 15:59:52  nathan
;if ffv HI, use hi_fix_pointing with assumed crpix, else not
;
;Revision 1.11  2012/05/02 19:36:23  mcnutt
;sets sc for wavelet filenames based on _L and _R define_wcshdrs.pro
;
;Revision 1.10  2012/02/14 20:01:29  mcnutt
;added work around for AIA wcs headers position.HEE_OBS keyword
;
;Revision 1.9  2011/09/26 18:30:14  mcnutt
;added flatplane movie grid and measuring options
;
;Revision 1.8  2011/09/20 19:53:09  nathan
;removed test nofits
;
;Revision 1.7  2011/09/20 15:34:51  nathan
;fix stop
;
;Revision 1.6  2011/09/19 21:32:13  nathan
;use scc_read_summary to find FITS filename to get around ssdfs0 problems
;
;Revision 1.5  2011/08/31 20:56:31  nathan
;if nofits always use convert2secchi.pro
;
;Revision 1.4  2011/08/26 16:21:19  mcnutt
;defines nx and ny
;
;Revision 1.3  2011/05/20 16:54:55  mcnutt
;added B light travel filename adjustment
;
;Revision 1.2  2011/05/12 17:26:46  mcnutt
;corrected error in hdrs keyword
;
;Revision 1.1  2011/05/12 15:44:30  mcnutt
;moved from inside scc_movie_win add hdrs keyword and nofits options
;

PRO define_WCSHDRS,current=current,wcs=wcs,system=system,hdrs=hdrs

COMMON scc_playmv_COMMON, moviev
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

undfinemoviev=0
if ~is_dir(getenv('secchi')) then moviev.nofits=1
if keyword_set(hdrs) then begin
  def_mvi_hdr,file_hdr
  if tag_exist(hdrs,'nx') THEN begin
     file_hdr.nx=hdrs.nx & file_hdr.ny=hdrs.ny
  endif else begin
     file_hdr.nx=hdrs.naxis1 & file_hdr.ny=hdrs.naxis2
  endelse
  IF (file_hdr.radius0 ne 0 or file_hdr.radius1 ne 0 or file_hdr.theta0 ne 0 or file_hdr.theta1 ne 0) and file_hdr.rtheta eq 0 THEN flatplane=1 ELSE flatplane=0
  moviev={img_hdrs:hdrs, hsize:file_hdr.nx, vsize:file_hdr.ny, frames:hdrs.filename, osczero:[0,0], file_hdr:file_hdr, current:0, debugon:0, nofits:0, flatplane:flatplane}
  debug_on=0
  undfinemoviev=1
  current=1
endif

;stop
if moviev.file_hdr.rtheta lt 1 and moviev.flatplane eq 0 then begin
    print,'Defining WCS headers.'
    det=moviev.img_hdrs(0).detector
    dindx=where(['C2','C3','EIT'] EQ det,issoho)
    d2indx=where(['EIT'] EQ det, iseit)
    eindx=where(['EUVI'] EQ det, iseuv)
    cindx=where(['COR2'] EQ det, iscor2)
    dindx=where(['COR1'] EQ det, iscor1)
    dindx=where(['HI'] EQ strmid(det,0,2), ishi)
    IF ishi THEN cdeltfac=3600. ELSE cdeltfac=1.

;  test for A and B image based on size will be added to mvi header at some point
    if undfinemoviev eq 0 then begin  ;is a movie
      if datatype(imgs) ne 'UND' then begin
        if (moviev.hsize eq moviev.vsize*2+2)then begin
	   if (total(imgs(moviev.hsize-moviev.vsize-2:moviev.hsize-moviev.vsize-1,0:moviev.vsize-1,0)) eq 0.0)then moviev.osczero=[moviev.hsize-moviev.vsize,0]
        endif
        if (moviev.vsize eq moviev.hsize*2+2)then begin
	   if (total(imgs(0:moviev.hsize-1,moviev.vsize-moviev.hsize-2:moviev.vsize-moviev.hsize-1),0) eq 0.0)then moviev.osczero=[0,moviev.vsize-moviev.hsize]
        endif
      endif else begin
        winsave=!d.window
        wset,moviev.win_index(0)
        timg=tvrd()
        wset,winsave
        if (moviev.hsize eq moviev.vsize*2+2)then begin
	   if (total(timg(moviev.hsize-moviev.vsize-2:moviev.hsize-moviev.vsize-1,0:moviev.vsize-1)) eq 0.0)then moviev.osczero=[moviev.hsize-moviev.vsize,0]
        endif
        if (moviev.vsize eq moviev.hsize*2+2)then begin
	  if (total(timg(0:moviev.hsize-1,moviev.vsize-moviev.hsize-2:moviev.vsize-moviev.hsize-1)) eq 0.0)then moviev.osczero=[0,moviev.vsize-moviev.hsize]
        endif
      endelse
    endif
    if keyword_set(current) then begin
       i1=moviev.current & i2=moviev.current
    endif else begin
        i1=0 & i2=n_elements(moviev.frames)-1
    endelse
    
    sc= strmid(moviev.frames[0],strpos(moviev.frames[0],'.')-1,1)
    if strmid(moviev.frames[0],strpos(moviev.frames[0],'.')-2,2) eq '_R' then sc='A' ;special case for wavelet file names
    if strmid(moviev.frames[0],strpos(moviev.frames[0],'.')-2,2) eq '_L' then sc='B'
    osc=''
    IF sc eq 'A' and total(moviev.osczero) gt 1 then osc='B' 
    IF sc eq 'B' and total(moviev.osczero) gt 1 then osc='A'

    for i=i1,i2 do begin
        mvhhdr=moviev.file_hdr
    	ahdr=moviev.img_hdrs(i)
	isflat=(strmid(ahdr.filter,0,9) EQ 'Deproject')
	fname=''
	dut=anytim2utc(ahdr.date_obs+' '+strmid(ahdr.time_obs,0,5)) ; truncated to minutes
	dutp1=dut
	dutp1.time=dut.time+60000 ; add 1 minute
	filen='nofits'
	fname='nofits'
    	if osc NE '' then begin
	; SECCHI only
    	    if moviev.osczero(0) gt 0 then mvhhdr.nx=(moviev.file_hdr.nx-2)/2
    	    if moviev.osczero(1) gt 0 then mvhhdr.ny=(moviev.file_hdr.ny-2)/2
            if total(moviev.osczero) gt 0 then sc1='A' else sc1=sc
	    ; always A then B required for scc_normalize
	    IF moviev.nofits ne 1 THEN BEGIN
	      s=scc_read_summary(date=[dut,dutp1],spacecraft=sc1,telescope=det)
	    ; here date is DATE-OBS
    	      IF datatype(s) EQ 'STC' THEN filen=s[0].filename $
	      ELSE BEGIN
		IF sc EQ 'B' THEN BEGIN
		    dutp1.time=dutp1.time+60000	
		    ; Time from filename could be DATE-OBS or DATE-CMD.
		    ; DATE-OBS may be up to about 65 seconds later than DATE-CMD.
	    	    s=scc_read_summary(date=[dut,dutp1],spacecraft=sc1,telescope=det)
		ENDIF
		IF datatype(s) EQ 'STC' THEN filen=s[0].filename $
		ELSE BEGIN
		    message,'No file found for '+ahdr.date_obs+' '+ahdr.time_obs+' '+det+'-'+sc1,/info
		ENDELSE
	      ENDELSE
	      IF filen NE 'nofits' THEN fname=sccfindfits(filen)
	      IF fname[0] EQ '' THEN fname='nofits'
            ENDIF
	ENDIF
	print,'FNAME=',fname
	;fname='nofits'
    	if fname EQ 'nofits' THEN BEGIN
    	    if osc NE '' then begin
	      filen=strmid(ahdr.filename,0,strpos(ahdr.filename,'.ft')-1)+sc1+'.fts'
    	      ahdr.filename=filen
            endif
	    fhdr = convert2secchi(ahdr,mvhhdr, debug=debug_on) 
	; IF ahdr is already a SECCHI FITS hdr, then convert2secchi returns it unchanged.
	ENDIF ELSE BEGIN
	; If ahdr is a MVI frame header we need to get original FITS file header, 
	; otherwise is a FITS header from scc_mkframe (or user input) and we can trust it.
	
	    jk=sccreadfits(fname,fhdr,/nodata)
;	;--Between 2007-06-01 and 2007-07-22 Level 0.5 header values of COR2 differ from
;	;  more recent values by as much as 0.5 arcsec in CROTA and 1200 km farther from sun (DSUN_OBS)
;    	;IF (iscor2) THEN BEGIN
;		    cor2_point,fhdr, /NOJITTER, ROLLZERO=rolliszero, _EXTRA=_extra
;		    ; Note that cor2_point sets hdr.CROTA
;		    IF tag_exist(moviev.img_hdrs(i),'ROLL') THEN moviev.img_hdrs(i).roll=fhdr.crota
;    	ENDIF
    	    matchfitshdr2mvi,fhdr,mvhhdr,ahdr,issoho=issoho
	ENDELSE
    	IF abs(fhdr.crpix1-fhdr.xcen-1) LT 0.1 and abs(fhdr.crpix2-fhdr.ycen-1) LT 0.1 THEN BEGIN
	; Correct xcen/ycen error from generic_movie not setting it properly. Xcen and Ycen in  MVI header have different
	; definition than in FITS hdr (where xcen, ycen is angle coordinate of image center).
	; crval is 0.
	    fhdr.crpix1 =  mvhhdr.nx/2. + 0.5 - fhdr.xcen/fhdr.cdelt1
	    fhdr.crpix2 =  mvhhdr.ny/2. + 0.5 - fhdr.ycen/fhdr.cdelt2
    	ENDIF
	
	IF (ishi) and ~isflat THEN begin 
	; IF movie is FFV, then take advantage of corrected pointing from hi_fix_pointing, otherwise use sun center in MVI header.
	        nhdr=fhdr
		hi_fix_pointing,nhdr
		; IF CRVAL is 0, it is changed in fov2radec.pro even if no pointing file found.
		nhdr.crpix1=mvhhdr.nx/2. + 0.5
		nhdr.crpix2=mvhhdr.ny/2. + 0.5
		IF nhdr.ravg GT 0 THEN fhdr=nhdr
		; only use nhdr if valid pointing calibration found
		nc=get_sun_center(fhdr)
		if (moviev.debugon) then print,nc
		IF (abs(nc.xcen-ahdr.xcen) GT 10 or abs(nc.ycen-ahdr.ycen) GT 10) and ahdr.xcen + ahdr.ycen ne 0 THEN  BEGIN
		    message,'Subfield detected; using MVI xcen,ycen:'+trim(ahdr.xcen)+','+trim(ahdr.ycen),/info
		    fhdr.crpix1=ahdr.xcen+1
		    fhdr.crpix2=ahdr.ycen+1
		    fhdr.crval1=0
		    fhdr.crval2=0
		    fc=get_sun_center(fhdr)
		    if (moviev.debugon) then print,fc
		    if (moviev.debugon) then wait,1
		ENDIF
                if tag_exist(ahdr,'roll') then ahdr.roll=fhdr.crota
    	endif
    	if total(moviev.osczero) gt 0 then begin  
	; define wcshdr for two-sc STEREO movie 
    	    filen=strmid(filen,0,strpos(filen,'.ft')-1)+'B'+'.fts'
    	    ahdr.filename=filen
    	    posk=strpos(filen,'0B')
    	    if posk gt -1 then filen=strmid(filen,0,posk)+'?'+strmid(filen,posk+2,strlen(filen))
    	    if (moviev.debugon) then help,filen
            if moviev.nofits ne 1 then begin
    	      IF tag_exist(moviev,'fitsdir') then fname=moviev.fitsdir+'/'+filen else fname=sccfindfits(filen)
    	      IF fname EQ '' THEN BEGIN
    	    	  strput,filen,'?',16
    	    	  fname = sccfindfits(filen)
    	      ENDIF
            endif else fname='nofits'
    	    if fname NE  'nofits' then BEGIN
		jk=sccreadfits(fname,ohdr,/nodata)
    		IF (ishi) and ~isflat THEN hi_fix_pointing,ohdr
    		matchfitshdr2mvi,ohdr,mvhhdr,ahdr,issoho=issoho
	    ENDIF ELSE BEGIN
		ohdr = convert2secchi(ahdr,mvhhdr)
    	    ENDELSE
    	    help,fname
    	    IF (ishi) then begin
	    	wcsn=  fitshead2wcs(fhdr, KEYWORD_NULL=0, system=system)
	    	wcsn2= fitshead2wcs(ohdr, KEYWORD_NULL=0, system=system)
	    	IF isflat THEN BEGIN 
		    wcsn = wcs2arc(wcsn, [mvhhdr.nx,mvhhdr.ny], [ahdr.xcen,ahdr.ycen],mvhhdr.sec_pix/3600)
	    	    wcsn2= wcs2arc(wcsn2,[mvhhdr.nx,mvhhdr.ny], [ahdr.xcen,ahdr.ycen],mvhhdr.sec_pix/3600)
		ENDIF
    	    endif else begin
	    ; for SCIP images, need to normalize as original images were normalized
	    ; ref_hdr is ALWAYS Ahead = fhdr
	    	wcsn =scc_normalize(-1,fhdr,ref_hdr=fhdr,/WCSCORR)
		wcsn2=scc_normalize(-1,ohdr,ref_hdr=fhdr,/WCSCORR)
            endelse
	    
            if i EQ i1 then begin
		wcshdrs =wcsn
		wcshdrs2=wcsn2
            endif else begin
		wcshdrs =[wcshdrs, wcsn]
		wcshdrs2=[wcshdrs2,wcsn2]
	    endelse
	    
	    IF osc EQ 'A' THEN BEGIN
	    	wcsleft=wcsn2 
		hdrleft=ohdr
	    ENDIF ELSE BEGIN
	    	wcsleft=wcsn
		hdrleft=fhdr
	    ENDELSE
        ENDIF ELSE BEGIN    
	; single-spacecraft movie
	    wcsn= fitshead2wcs(fhdr, KEYWORD_NULL=0, system=system)
	    IF isflat THEN wcsn = wcs2arc(wcsn,[mvhhdr.nx,mvhhdr.ny],[ahdr.xcen,ahdr.ycen],mvhhdr.sec_pix/cdeltfac)
    	    if tag_exist(wcsn.position,'HEE_OBS') and det eq 'AIA' then begin
    	      tposition=wcsn.position
    	      tposition=rem_tag(tposition,'HEE_OBS')
    	      wcsn=rem_tag(wcsn,'position')
    	      wcsn=add_tag(wcsn,tposition,'position') 
	    endif
	    IF i EQ i1 then wcshdrs= wcsn else wcshdrs=[wcshdrs,wcsn]
	    wcsleft=wcsn
	    hdrleft=fhdr
	ENDELSE

	IF tag_exist(moviev,'sunc') then moviev.sunc[i].xcen=wcsleft.crpix[0]-1
	IF tag_exist(moviev,'sunc') then moviev.sunc[i].ycen=wcsleft.crpix[1]-1
	IF tag_exist(moviev,'rolls') then moviev.rolls[i]=wcsleft.roll_angle
	IF tag_exist(moviev,'arcs') then moviev.arcs[i]=wcsleft.cdelt[0]
	IF tag_exist(moviev,'solr') then $
	    IF hdrleft.rsun EQ 0 THEN moviev.solr[i]=3600*(180/!pi)*1000*onersun()/wcsleft.position.dsun_obs $
	    	    ELSE moviev.solr[i]=hdrleft.rsun
   ENDFOR 
IF debug_on THEN BEGIN
    help,wcshdrs
    wait,2
endif

   if total(moviev.osczero) gt 0 and osc eq 'B' then wcshdrs=[wcshdrs,wcshdrs2]
   if total(moviev.osczero) gt 0 and osc eq 'A' then wcshdrs=[wcshdrs2,wcshdrs]
   if ~keyword_set(current) then moviev=add_tag(moviev,wcshdrs,'wcshdrs') else wcs=wcshdrs
   if undfinemoviev eq 1 then undefine,moviev
endif $ ; not Rtheta MVI
else IF debug_on THEN print,'wcshdrs are not needed for rtheta movies'
END

