;+
;
;$Id: exit_playmovie.pro,v 1.4 2011/11/28 18:23:42 nathan Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : exit_playmovie
;               
; Explanation : closes the open scc_playmovie or scc_playmoviem
;               
; Use         : used to avoid error when trying running 2 scc_playmovie routines
;
; Calls       : exit_playmovie
;
; Comments    : closes current scc_playmovie or scc_playmoviem 
;               
; Side effects: closes current scc_playmovie or scc_playmoviem
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL Sep. 29 2011.
;               
;-            
;$Log: exit_playmovie.pro,v $
;Revision 1.4  2011/11/28 18:23:42  nathan
;check moviev.base validity
;
;Revision 1.3  2011/10/26 19:20:25  mcnutt
;checks for winbase value
;
;Revision 1.2  2011/10/26 18:22:21  mcnutt
;added cmndbase and retain win_index keyword
;
;Revision 1.1  2011/09/29 18:51:08  mcnutt
;closes open scc_playmovie(m)
;
;;----------------exit movie widget --------------------------------------
pro exit_playmovie,retain_win_index=retain_win_index
COMMON scc_playmv_COMMON, moviev
  if datatype(moviev) eq 'STC' then $
  IF widget_info(moviev.base,/valid_id) THEN begin
    IF moviev.cmbbase gt -1 THEN WIDGET_CONTROL, /DESTROY, moviev.cmbbase
    IF moviev.zoombase gt -1 THEN WIDGET_CONTROL, /DESTROY, moviev.zoombase
    IF moviev.boxbase gt -1 THEN WIDGET_CONTROL, /DESTROY, moviev.boxbase
    if TAG_EXIST(moviev, 'framehdrinfo') then if moviev.framehdrinfo.showfhdr eq 1 then WIDGET_CONTROL, /DESTROY, moviev.framehdrinfo.hdrinfo
    WIDGET_CONTROL, /DESTROY, moviev.base
    if moviev.winbase ne -1 then WIDGET_CONTROL, /DESTROY, moviev.winbase
    WIDGET_CONTROL, /DESTROY, moviev.cmndbase
    if ~keyword_set(retain_win_index) then FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
    device,/cursor_crosshair	; return to default cursor
    device,WINDOW_STATE=ws
		   ;wait,1
    IF ws[0] EQ 1 THEN wdelete,0
    undefine,moviev
  endif
END
