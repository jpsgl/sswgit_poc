PRO sccREAD_MVI, lu, file_hdr, ihdrs, imgs, swapflag, NO_COLOR=no_color, RGBVEC=rgbvec, pngmvi=pngmvi
;+
; $Id: sccread_mvi.pro,v 1.18 2017/01/03 14:34:10 mcnutt Exp $
;
; Project     : STEREO - SECCHI, SOHO - LASCO/EIT
;
; Name        : sccREAD_MVI
;
; Purpose     : Read MVI format files. Works on both SECCHI and old LASCO/EIT MVIs
;
; Use         : IDL> openr,1,'file.mvi'
;		IDL> read_mvi, 1, mvihdr, framehdrs, images
;
; Inputs      : 
;	lu	INT	logical unit of MVI file to read
;
; Optional Inputs:
;
; Outputs     :
;	file_hdr  STRUCT	describes movie
;	ihdrs  	BYTE File Array	describes frame headers
;	imgs  	BYTE File Array	contains frame data (images)
;	swapflag  BYTE	byte-swap ON (1) or OFF (0)
;
; Keywords    :
;	/NO_COLOR	Do not load color tables in mvi file 
;	RGBVEC=[r,g,b]	Returns color table vectors
;
; Comments    :
;
; Side effects: creates structure FILE_HDR
;
; Category    : Image Display.  Animation. MVI
;
; Added keyword rgbvec to allow color tables from mvi file to be passed back
; to calling routine. SPP (USRA/NRL) 97/10/10.
;
; Modifications:
;
; $Log: sccread_mvi.pro,v $
; Revision 1.18  2017/01/03 14:34:10  mcnutt
; Use select_windows instead of set_plot
;
; Revision 1.17  2010/12/02 18:28:19  nathan
; set file_hdr.sec_pix same as ahdr.cdelt1 if it is defined
;
; Revision 1.16  2010/10/27 17:04:20  mcnutt
; does not use sec_pix with rtheta movies allows rtheta = 2 for ylog
;
; Revision 1.15  2010/10/20 16:48:00  mcnutt
; sets sec_pix for rtheta movies
;
; Revision 1.14  2010/10/05 20:19:49  nathan
; set file_hdr.sec_pix from frame header, not file_hdr
;
; Revision 1.13  2010/09/30 22:50:48  nathan
; special sec_pix case for hi-2 256 and 128
;
; Revision 1.12  2010/09/30 22:03:56  nathan
; Fix bug for HI2 file_hdr.sec_pix; now save value is UINT and when read in
; if neg assume it was saved as UINT
;
; Revision 1.11  2010/09/20 18:45:41  mcnutt
; removed get_hdr_movie_img function
;
; Revision 1.10  2010/09/20 18:00:53  mcnutt
; added get_hdr_movie_img function
;
; Revision 1.9  2009/04/06 18:42:35  mcnutt
; only associates the gif or png name for imgs if keyword set pngmvi
;
; Revision 1.8  2009/03/11 18:33:01  mcnutt
; correct pngmvi keyword
;
; Revision 1.7  2009/03/11 18:08:37  mcnutt
; changed png movie to read in img file names without dir
;
; Revision 1.6  2009/01/14 14:10:51  mcnutt
; gets mvi header struct from def_mvi_hdr.pro
;
; Revision 1.5  2008/05/22 19:43:17  nathan
; do decomposed=0
;
; Revision 1.4  2008/04/30 21:28:17  nathan
; Clarified that xcen,ycen are defined as IDL coordinates
;
; Revision 1.3  2008/01/14 19:13:45  nathan
; make sure file_hdr structure has unique name
;
; Revision 1.2  2007/10/17 21:48:52  nathan
; latest revision from dev/movie
;
; Revision 1.2  2007/10/17 18:55:32  nathan
; Implemented standard version 6 header structure for SECCHI and LASCO. Should
; be backwards compatible with previous headers labeled version 6.
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;
;       @(#)read_mvi.pro	1.7, 09/23/04 : NRL LASCO LIBRARY
;	010711	the jake	Added Version 3 to handle RTHETA Movies
;	011109  thejake		After testing, additions do not seem to have done any harm so adding
;				WRUNMOVIEM3, MVIPLAY3, WRITE_DISK_MOVIE3, and READ_MVI3 to library.
;				Once an MVI is written with the version 3 software, it will need to
;				be read with it as well.
;	030828, nbr - Incorporate read_mvi3.pro/RTheta header; add version 4 header w/ RECTIFY
;-

def_mvi_hdr,file_hdr
nb = file_hdr.nb	
f_nb = file_hdr.fh_nb	

	tmp_file_hdr = ASSOC(lu,INTARR(f_nb/2))
	tmp_fh = tmp_file_hdr(0)

	ver = tmp_fh(5)
	IF (ver LT 0) OR (ver GT 10) THEN BEGIN
		BYTEORDER, tmp_fh
		swapflag = 1
	ENDIF ELSE swapflag = 0

	file_hdr.nf = tmp_fh(0)
	file_hdr.nx = tmp_fh(1)
	file_hdr.ny = tmp_fh(2)
	file_hdr.mx = tmp_fh(3)
	file_hdr.nb = LONG(tmp_fh(4))
	file_hdr.ver = tmp_fh(5)
	file_hdr.fh_nb = f_nb

	IF (file_hdr.ver GE 1) AND (file_hdr.ver LE 9) THEN BEGIN
		;f_nb = 20
		file_hdr.fh_nb = LONG(tmp_fh(6))
		f_nb = file_hdr.fh_nb - 256*3	; MVI header length, assuming 8-bit color table
		file_hdr.sunxcen = tmp_fh(7)
		file_hdr.sunycen = tmp_fh(8)
		
		sec_pix0=FLOAT(tmp_fh(9))/100
		;help,sec_pix0
		sec_pix=sec_pix0
		IF fix(abs(sec_pix0)) EQ  327 THEN sec_pix=519.2  ELSE $ 	; hi-2 512x512 case if saved as INT
		IF fix(sec_pix0) EQ -270 THEN sec_pix=1040.25 ELSE $ 	; hi-2 256x256 case if saved as UINT
		IF fix(sec_pix0) EQ  114 THEN sec_pix=2080.5  ELSE $ 	; hi-2 128x128 case if saved as UNIT
		IF sec_pix0 LT 0 THEN sec_pix=sec_pix0+655.36
		; was saved as unsigned int (HI2)
		file_hdr.sec_pix = sec_pix
		IF file_hdr.ver GE 2 THEN BEGIN
			file_hdr.sunxcen = FLOAT(file_hdr.sunxcen)/10
			file_hdr.sunycen = FLOAT(file_hdr.sunycen)/10
		ENDIF
		if file_hdr.ver GE 3 then begin	;"<--------------------by the jake
			;PRINT, "Reading Version 3 Header"
			;f_nb = 30
			file_hdr.rtheta = tmp_fh[10]
			if file_hdr.rtheta ge 1 then PRINT, "--RTHETA Movie"
			file_hdr.radius0 = FLOAT(tmp_fh[11])/10
			file_hdr.radius1 = FLOAT(tmp_fh[12])/10
			file_hdr.theta0 = FLOAT(tmp_fh[13])/10
			file_hdr.theta1 = FLOAT(tmp_fh[14])/10
		endif	;"<-------------------------
		IF file_hdr.ver GE 4 THEN file_hdr.rectified=	tmp_fh[15]
    	    	IF file_hdr.ver GE 5 THEN file_hdr.truecolor= 	tmp_fh[16] 
		IF file_hdr.ver GE 6 THEN file_hdr.ccd_pos= 	tmp_fh[17:20]

		red = ASSOC(lu,BYTARR(256), f_nb)
		green = ASSOC(lu,BYTARR(256), f_nb+256)
		blue = ASSOC(lu,BYTARR(256), f_nb+256+256)
		r = red(0)
		g = green(0)
		b = blue(0)
		IF NOT(KEYWORD_SET(NO_COLOR)) THEN TVLCT, r, g, b
		IF KEYWORD_SET(RGBVEC) THEN rgbvec = [[r],[g],[b]]
	ENDIF

	ihdrs = ASSOC(lu,BYTARR(file_hdr.nb), file_hdr.fh_nb)
    	ahdr = SCCMVIHDR2STRUCT(ihdrs[0],file_hdr.ver)
	IF ahdr.cdelt1 GT 0 THEN file_hdr.sec_pix = ahdr.cdelt1

        if ~keyword_set(pngmvi) then pngmvi='0' else pngmvi=pngmvi
        IF pngmvi ne '0' THEN BEGIN 
           BREAK_FILE, pngmvi, a, dir, name, ext
	   imgs = ASSOC(lu,BYTARR(strlen(name+ext)),file_hdr.fh_nb+file_hdr.nb*(file_hdr.mx)) 
	ENDIF ELSE BEGIN
          IF (file_hdr.truecolor EQ 0)  THEN BEGIN
	     imgs = ASSOC(lu,BYTARR(file_hdr.nx,file_hdr.ny),file_hdr.fh_nb+file_hdr.nb*(file_hdr.mx))
	  ENDIF ELSE BEGIN
	     imgs = ASSOC(lu,BYTARR(3,file_hdr.nx,file_hdr.ny),file_hdr.fh_nb+file_hdr.nb*(file_hdr.mx))
	  ENDELSE
        ENDELSE	
   ; Is this a 24-bit TrueColor device? If so, turn
   ; color decomposition OFF.

thisRelease = Float(!Version.Release)
IF thisRelease GE 5.1 THEN BEGIN
   select_windows ;set_plot,'x'
   Device, Get_Visual_Name=thisVisual
   IF thisVisual EQ 'TrueColor' THEN Device, Decomposed=0
ENDIF ELSE Device, Decomposed=0

END
