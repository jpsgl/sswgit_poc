pro matchfitshdr2mvi, fhdr, mvi_file_hdr, mvi_frame_hdr, issoho=issoho, XY=xy, notsecchi=notsecchi
;+
; $Id: matchfitshdr2mvi.pro,v 1.18 2012/05/03 13:22:01 mcnutt Exp $
;
; NAME:	MATCHFITSHDR2MVI
;
; PURPOSE:
;	Update fits header to match subfield, roll, cdelt in MVI frame 
;
; CATEGORY: movie, header, fits, wcs
;
; CALLING SEQUENCE:
;
; INPUTS:
;	fhdr		FITS header structure to be modified
;	mvi_file_hdr	MVI header structure as defined in sccread_mvi.pro
;	mvi_frame_hdr	MVI frame header structure as defined in def_mvi_strct.pro
;
; OPTIONAL INPUTS:
;	
; KEYWORD PARAMETERS: 
;   	/ISSOHO     add tag for PC's
;   	XY= 	INTARR[2] MVI frame dimension if mvi_file_hdr input should be ignored 

;
; OUTPUTS:
;	fhdr	FITS header structure modified in place.
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;-
; MODIFICATION HISTORY:
;
; $Log: matchfitshdr2mvi.pro,v $
; Revision 1.18  2012/05/03 13:22:01  mcnutt
; removed last unintentional commit
;
; Revision 1.17  2012/05/02 19:36:23  mcnutt
; sets sc for wavelet filenames based on _L and _R define_wcshdrs.pro
;
; Revision 1.16  2011/11/23 18:18:13  mcnutt
; crpix defaults frame header sun center
;
; Revision 1.15  2011/10/28 15:02:49  mcnutt
; add keyword notsecchi, not used to avoid bad calls from other programs
;
; Revision 1.14  2011/10/25 15:23:25  mcnutt
; sets rollv ot 0 if less then 0.1
;
; Revision 1.13  2011/09/28 14:39:36  nathan
; add XY= option
;
; Revision 1.12  2011/09/26 18:30:14  mcnutt
; added latplane movie grrid and measuring options
;
; Revision 1.11  2011/05/13 17:58:31  mcnutt
; added 1 to sun center
;
; Revision 1.10  2011/05/13 15:01:32  mcnutt
; sets cdelt for fhdr if file_hdr and frame are 0
;
; Revision 1.9  2011/02/14 23:33:17  nathan
; obsolete /ISSOHO
;
; Revision 1.8  2010/10/08 23:53:36  nathan
; accomodate inconsistency in cdelt1 definition
;
; Revision 1.7  2010/08/17 15:54:45  nathan
; added /NOTSECCHI
;
; Revision 1.6  2010/01/25 14:35:06  mcnutt
; corrects unitf if mvi_frame_hdr has cunit1 and its eq to deg
;
; Revision 1.5  2009/09/08 16:32:45  mcnutt
; changed deprojected type to TAN
;notsecchi
; Revision 1.4  2009/09/04 13:22:44  mcnutt
; sets PV2_1 to 1 if movie is deprojected
;
; Revision 1.3  2008/12/19 13:10:12  mcnutt
;  added issoho keyword to add PC tags to structure
;
; Revision 1.2  2008/05/13 22:35:58  nathan
; fix crpix computation
;
; Revision 1.1  2008/05/07 22:54:40  nathan
; implement matchfitshdr2mvi in scc_wrunmoviem
;
if tag_exist(mvi_frame_hdr,'roll') then IF strmid(mvi_frame_hdr.filter,0,9) eq 'Deproject' THEN mvi_frame_hdr.roll=0
if tag_exist(mvi_frame_hdr,'roll') then rollv=mvi_frame_hdr.roll else rollv=fhdr.crota
if rollv lt 0.1 then rollv=0

fhdr.crota = rollv

dr=!DPI/180.d
if tag_exist(fhdr,'PC1_1') then begin
   fhdr.PC1_1= cos(rollv*dr)
   fhdr.PC1_2=-sin(rollv*dr) 
   fhdr.PC2_1= sin(rollv*dr) 
   fhdr.PC2_2= cos(rollv*dr)
endif else begin
   fhdr=add_tag(fhdr,cos(rollv*dr),'PC1_1')
   fhdr=add_tag(fhdr,-sin(rollv*dr),'PC1_2')
   fhdr=add_tag(fhdr,sin(rollv*dr),'PC2_1')
   fhdr=add_tag(fhdr,cos(rollv*dr),'PC2_2')
endelse
;--correct for resizing
IF keyword_set(XY) THEN BEGIN
    mvix=xy[0]
    mviy=xy[1]
    mvisecpix=0
    noccdpos=1
ENDIF ELSE BEGIN
    mvix=mvi_file_hdr.nx
    mviy=mvi_file_hdr.ny
    mvisecpix=mvi_file_hdr.sec_pix
    NOccdpos=(total(mvi_file_hdr.ccd_pos) LE 0)
ENDELSE
if mvisecpix eq 0 and mvi_frame_hdr.cdelt1 eq 0 then mvi_frame_hdr.cdelt1=fhdr.cdelt1/((mvix*1.)/(fhdr.naxis1*1.))

fhdr.naxis1=mvix
fhdr.naxis2=mviy
;
; There is an inconsistency in definition of mvi_frame_hdr.cdelt1. Sometimes for HI movies
; it is arcsec and sometimes it is degrees. Need to handle both correctly. nr, 10/8/10
;
if mvi_frame_hdr.cdelt1 lt 0.002 then mvi_frame_hdr.cdelt1=mvisecpix
IF mvi_frame_hdr.cdelt1 Gt 0.2  THEN mvi2arc=1. $   ; units in MVI frame header are arcsec
    	    	    	    	ELSE mvi2arc=3600.  ; units in MVI frame header are deg
IF fhdr.cunit1 EQ 'deg'     	THEN fit2arc=3600 ELSE fit2arc=1.

; convert both to arcsec before getting ratio

scl=(fhdr.cdelt1*fit2arc)/(mvi_frame_hdr.cdelt1*mvi2arc)

;--Need to remove platescl adjustments in addition to binning
IF scl LT 1 THEN scl=1./round(1./scl) ELSE scl=round(scl)
;--If ccd_pos is zero than it is undefined, assume no subfield
IF noccdpos THEN BEGIN
    offsetx=0
    offsety=0
ENDIF ELSE BEGIN
    offsetx=(mvi_file_hdr.ccd_pos[1]-fhdr.r1col)/2^(fhdr.summed-1)
    offsety=(mvi_file_hdr.ccd_pos[0]-fhdr.r1row)/2^(fhdr.summed-1)
ENDELSE
IF strmid(mvi_frame_hdr.filter,0,9) ne 'Deproject' THEN begin
   if mvi_frame_hdr.xcen eq 0 and mvi_frame_hdr.YCEN eq 0 then begin
     fhdr.crpix1=0.5+(fhdr.crpix1-0.5-offsetx)*scl
     fhdr.crpix2=0.5+(fhdr.crpix2-0.5-offsety)*scl
   endif else begin
     fhdr.crpix1=mvi_frame_hdr.XCEN+1
     fhdr.crpix2=mvi_frame_hdr.YCEN+1
     fhdr.crval1=0.0
     fhdr.crval2=0.0
  endelse
endif else begin
   fhdr.crpix1=mvi_frame_hdr.XCEN+1
   fhdr.crpix2=mvi_frame_hdr.YCEN+1
   fhdr.ctype1=strmid(fhdr.ctype1,0,5)+'TAN'
   fhdr.ctype2=strmid(fhdr.ctype2,0,5)+'TAN'
   fhdr.crval1=0.0
   fhdr.crval2=0.0
   fhdr.PV2_1=1.0
endelse

fhdr.cdelt1=fhdr.cdelt1/scl
fhdr.cdelt2=fhdr.cdelt2/scl
end
