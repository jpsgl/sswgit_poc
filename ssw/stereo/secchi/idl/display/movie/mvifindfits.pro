;+
;$Id: mvifindfits.pro,v 1.2 2011/05/12 20:40:13 nathan Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	MVIFINDFITS()
;
; Purpose     :	Find FITS data files given MVI or FITS header structure
;
; Category    :	FITS, Filename, movie, mvi, database
;
; Explanation : 
;
; Syntax      :	Path+file(s) = MVIFINDFITS( img_hdrs )
;
; Examples    :	
;
; Inputs      :	img_hdrs    One or more header structures
;
; Opt. Inputs :	None
;
; Outputs     :	The result of the function is the path to the file + filename.
;
; Opt. Outputs:	None.
;
; Keywords    :	
;
; Env. Vars.  : See SCC_DATA_PATH
;
; Calls       :	FILE_EXIST, BREAK_FILE, SCC_DATA_PATH, CONCAT_DIR, ALL_VALS
;   	    	sccfindfits, lz_from_ql, 
;
; Common      :	None.
;
; Restrictions:	Must have FILENAME, DETECTOR, DATE_OBS in  header
;   	    	Currently only working at NRL
;
; Side effects:	None.
;
; $Log: mvifindfits.pro,v $
; Revision 1.2  2011/05/12 20:40:13  nathan
; added aia
;
; Revision 1.1  2011/04/20 16:08:18  nathan
; used by scc_movie_win.pro
;
;-
;
function mvifindfits, imghdrs

common mvipaths, aiadir, prevaiadate
;
;  Create an empty RESULT array with the same dimensions as the input array.
;
sz = size(imghdrs)
n=sz[1]
if sz[0] eq 0 then result = '' else result = strarr(n)
tel=strlowcase(imghdrs[0].detector)
dobs=imghdrs.date_obs+' '+imghdrs.time_obs
IF tel EQ 'aia' THEN BEGIN
    print,'Looking for AIA files in ',getenv('aia')+'/YYYY/MM/DD/WAVE/'
    aiadir=''
    read, 'If files are somewhere else, enter path here, else Return: ',aiadir
    IF aiadir NE '' THEN help,aiadir
    print
    imgcad=''
    print,'By default all available files for the input interval will be returned.'
    read, 'If you want fewer, enter the cadence desired in minutes: ',imgcad
    
    result=nrlgetaia([dobs[0],dobs[n-1]],imghdrs[0].wavelnth,imgcad,DIRECTORY=aiadir)
ENDIF ELSE $
FOR i=0,n-1 DO BEGIN
    filen=imghdrs[i].filename
    w=where(['blank','euvi','cor1','cor2','hi1','hi2'] EQ tel, isse)
    IF isse THEN result[i]=sccfindfits(filen) $
    ELSE BEGIN
    	w=where(['blank','c1','c2','c3','eit'] EQ tel, isso)
	IF isso THEN result[i]=lz_from_ql(time=dobs[i],camera=tel) $
	ELSE BEGIN	     	
	    message,'Need to write pro to find '+tel
	ENDELSE
    ENDELSE
ENDFOR

help,result
finish:
return, result
end
