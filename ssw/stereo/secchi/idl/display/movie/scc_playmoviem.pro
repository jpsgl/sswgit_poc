;+
;$Id: scc_playmoviem.pro,v 1.70 2018/02/13 14:05:52 mcnutt Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_PLAYMOVIEM
;               
; Purpose     : Widget tool to display animation sequence.
;               
; Explanation : This tool allows the user to view a series of images as
;		an animation sequence.  The user can control the direction,
;		speed, and number of frames with widget controls.
;               
; Use         : IDL> SCC_PLAYMOVIEM [, arg1 [, NAMES=names [,/PREVIOUS]]]
;
;		Without any inputs, program will prompt user to select an existing .mvi file.
;    Example  : IDL> SCC_PLAYMOVIEM
;
; Optional Input:              Or you could have one argument, the .mvi file you want to load.
;    Example  : IDL> SCC_PLAYMOVIEM, 'mymovie.mvi'
;
;               Or if you have pre-loaded images into pixmaps (like MKMOVIEM.PRO does) call:
;		Where win_index is an array of the window numbers and names is optionally
;		a STRARR() containing names of each frame.
;    Example  : IDL> SCC_PLAYMOVIEM, win_index, NAMES=names
; 
; Optional Keywords:
;   START=n  	Make frame n first frame of movie (starting at n=0)
;   LENGTH= 	Number of frames to read in from movie
;   AVERAGE_FRAMES= Number of frames to average together using rebin; implies /EDIT
;   SMOOTH_BOX=     Smooth each frame with given bin size; implies /EDIT
;   /USE_R  	Display units of solar radius for distance from Sun center (default for COR, EUV outside of 1 Rsun)
;   /USE_E  	Display units of degrees (Elongation) for distance from Sun (default for HI)
;   /USE_STPLN	Display angle relative to perpendicular to epipolar plane (STEREO mission plane)
;   /PRECOMMCORRECT 	Calls cor2_Point.pro for COR2 only (not necessary for movies made after 2008-03-28)   
;   NAMES   = title for each frame
;   SUNXCEN=,SUNYCEN=	array coordinate of sun center
;   SEC_PIX=	arcsec/pixel
;   /PREVIOUS	    Play last loaded movie
;   /ROLL_PER_FRAME 	Compute roll for each frame (LASCO/EIT only)
;   /EDIT   	Allow color table adjustment (slower)
;   /DEBUG  	Print debug messages
;   /LOWERXY	Move the x,y coordinate display down 
;   /LOWERDN	Move the DN display down
;   /CROSSHAIR_CURSOR	Use crosshair for cursor instead of arrow
;   /BIGFONT	Use Triplex Roman size=2 for annotation instead of default hardware font
;   /CARRINGTON Display location as carrington lat and long on solar disk (default for EUVI and EIT)
;   /NOSUBFIELD Override RCOL/RROW values in header to default to FFV (in case of bad MVI header)
;   /TIMES  	Write detector and frame time on each frame
;   /nofits  	use mk_short_scc_hdr and do not look for SECCHI fits files (REDUNDANT)
;   fitsdir     directory where the fits files used to creat movie are located if not in the standard SECCHI dir structure.
;
;		If after exiting SCC_PLAYMOVIEM you want to re-play the movie without reading it again call:
;    Example  : IDL> SCC_PLAYMOVIEM, /PREVIOUS
;    
; Calls       : 
;
; Comments    : Use MKMOVIEM.PRO to load into pixmaps.
;               
; Side effects: If /USE_R and HI, will need to read in FITS headers
;               
; Category    : Image Display.  Animation.
;               
; Written     : Scott Paswaters, NRL Feb. 13 1996.
;               
; Modified    : SEP  29 May 1996 - Changed to multiple pixmaps for images.
;				   Added buttons to save and load movie files (.mvi).
;				   Seperated control buttons from display window.
;               SEP   9 Jul 1996 - Added keyword to pass image headers.
;
;		SHH  12 Jul 1996 - Enabled display of cursor position in draw window
;			 	   Added height-time plotting capability
;				   Added Edit Frame option	  
;				   Modified effects of mouse buttons
;				   Added buttons above draw window
;				   Modified display of Control window
;
;               SEP  29 Sep 1996 - Added routines from DAB for applying C3 geometric distortion and
;                                  calculating solar radius as function of time.
;               SEP  01 Oct 1996 - Added call to C3_DISTORTION for applying C3 geometric distortion
;
;               RAH  01 Nov 1996 - Prior to PLOT_HT call, don't ask for filename
;
;		SHH  09 Jan 1997 - Added "Start H-T" button
;				   Calls XEDITFRAME
;               SEP  05 Feb 1997 - Mods for mvi version 1 format.
;               SEP  21 Mar 1997 - use sun center and arc_sec/pixel if saved in .mvi file.
;               SEP  16 May 1997 - added DN output to window, added button to update center.
;               SEP  23 Sep 1997 - added active slider widget for current frame.
;		NBR  06 Jan 1999 - changed sec_pix check in file_hdr
;		DW   11 Jan 1999 - added C2_DISTORTION and roll angle
;		NBR  02 Mar 1999 - eliminated sec_pix check for getting sun center 
;		NBR 09 Jul 1999  - Add warning if frame headers not saved
;		NBR, 05 Mar 2002 - Only compute roll angle once per day; use AVG instead of STAR for roll; extend common block
;		NBR, 24 Sep 2003 - Add mvi header roll correction (rect) in moviev; print mvi header; save MVIs with rect
;		NBR, 29 Sep 2003 - Add RECTIFIED keyword for call from WRUNMOVIE
;		NBR, 20 Oct 2003 - Allow case where xcen is REALLY zero
;		KB,  Dec 15,2003 - Added slider so full-res images can be used with smaller screens
;               KB,  Sep07, 2004 - When displaying Pos. Ang, if nominal_roll_attitude.dat can't be found, use default values
;               AEE, Jan25, 2005 - Generate roll angles (using new database) when reading in frames
;                                  and keep around to use later when going back and forth between
;                                  frames (to make it quicker). The get_roll_or_xy is called for 
;                                  first frame of the movie and also fo multi-day movies when a day 
;                                  boundry is crossed. I added keyword ROLL_PER_FRAME to calculate
;                                  a roll for each frame if present. Otherwise, default is to calculate
;                                  one roll per day instead of one roll per frame. (rev. 1.29)
;               AEE, Jan27, 2005 - Calculate one roll per day if SCC_PLAYMOVIEM is called from within scc_wrunmovie (since 
;                                  movie frames are already readin without calculating rolls in scc_wrunmovie). (rev. 1.30) 
;               AEE, Jan31, 2005 - set roll to zero when image header does not have valid date/time. (rev. 1.31)
;
;		D 1.32  06/06/15 13:43:44 esfand        32 31   00016/00000/00877
;		handle SECCHI headers -- this is the revision in LASCO library
;
;               AEE, Oct04, 2006 -   Added code to handle SECCHI headers. (rev. 1.33)  
;
;   	    	#### sccs Rev. 1.33 is identical to cvs Rev. 1.1 except for all ####
;		#### instances of wrunmoviem are replaced with SCC_PLAYMOVIEM   ####
;
; $Log: scc_playmoviem.pro,v $
; Revision 1.70  2018/02/13 14:05:52  mcnutt
; added description of fitsdir keyword
;
; Revision 1.69  2013/05/28 14:25:42  mcnutt
; corrected time_obs in in_hdrs if hdrs keyword is set
;
; Revision 1.68  2013/01/07 16:25:53  mcnutt
; corrected error when reading in movie without setting hdrs keyword
;
; Revision 1.67  2013/01/04 19:40:07  mcnutt
; set variable in_hdrs if keyword_set hdrs
;
; Revision 1.66  2012/10/26 14:37:54  mcnutt
; corrected osczero for flatplane movies compare fix of sec_pixs *1000
;
; Revision 1.65  2012/09/18 13:40:28  mcnutt
; added window 12 white used in drawbox
;
; Revision 1.64  2012/08/29 14:11:42  mcnutt
; added wcs_combine bpos options
;
; Revision 1.63  2012/08/17 18:42:52  mcnutt
; added Labels ht points
;
; Revision 1.62  2012/08/09 21:19:52  avourlid
; added R_SUN keyword
;
; Revision 1.61  2012/07/10 16:05:28  mcnutt
; added stonyhusrt corr option
;
; Revision 1.60  2012/05/17 14:05:50  mcnutt
; enhanced crop options
;
; Revision 1.59  2012/05/08 12:33:03  mcnutt
; removed scc_draw_box use draw_box and add wait when cropping with auto advance
;
; Revision 1.58  2012/05/07 17:00:00  mcnutt
; added auto advance option when cropping
;
; Revision 1.57  2012/04/25 14:17:33  mcnutt
; added x and y size option for crop and zoom
;
; Revision 1.56  2011/12/01 15:46:33  mcnutt
; movied device cursor to wset draw_win
;
; Revision 1.55  2011/11/28 18:24:56  nathan
; add CARRINGTON option for sync MVI frames
;
; Revision 1.54  2011/11/23 15:12:17  mcnutt
; corrected error defining basexoff and baseyoff
;
; Revision 1.53  2011/11/22 16:22:23  mcnutt
; base(xy)offset are now an aray of 2 for bottom and right side positioning
;
; Revision 1.52  2011/11/15 21:49:39  nathan
; make sure input does not get redefined
;
; Revision 1.51  2011/11/07 16:14:11  nathan
; fix case where ahdr.crota not defined
;
; Revision 1.50  2011/11/01 16:28:13  mcnutt
; crrected sec_pix for flatplane movies uses theta and x axis
;
; Revision 1.49  2011/10/26 18:19:51  mcnutt
; updated sec_pix for flatplane movies
;
; Revision 1.47  2011/09/26 18:30:14  mcnutt
; added latplane movie grrid and measuring options
;
; Revision 1.46  2011/09/07 18:17:57  mcnutt
; put hdrs keyword back
;
; Revision 1.45  2011/09/07 12:10:16  mcnutt
; does run box events if zoom window is clicked
;
; Revision 1.44  2011/08/31 21:10:38  nathan
; HDRS= is no longer supported
;
; Revision 1.43  2011/08/31 16:59:27  mcnutt
; changed zoom tool to work with box and added an option to save zoomed movie
;
; Revision 1.42  2011/08/10 18:40:42  mcnutt
; updates file_hdr and hdrs with sunxcen,sunycen and sec_pix if keywords are set if movie is save the keyword values will be used
;
; Revision 1.41  2011/08/03 18:00:37  nathan
; always use ahdr.crota, not ahdr.roll
;
; Revision 1.40  2011/05/24 20:15:37  nathan
; only call get_crota if issoho
;
; Revision 1.39  2011/05/24 20:13:43  nathan
; handle nom_crota error
;
; Revision 1.38  2011/05/18 22:40:10  nathan
; change references to wrunmovie to playmovie
;
; Revision 1.37  2011/05/18 14:01:45  mcnutt
; added rtheta measurements for wcs_combined movies
;
; Revision 1.36  2011/05/12 15:36:57  mcnutt
; added nofits keyword
;
; Revision 1.35  2011/04/28 17:00:54  mcnutt
; added auto advance option for sccwrite_ht
;
; Revision 1.34  2011/04/20 16:07:48  nathan
; change tabbing only
;
; Revision 1.33  2011/03/24 13:20:51  mcnutt
; corrected stonyhurst maps coords
;
; Revision 1.32  2011/03/18 16:49:59  mcnutt
; corrected synoptic coords
;
; Revision 1.31  2011/03/09 19:43:47  mcnutt
; add rtheta 3 and 4 for carrington/stonyhurst synoptic movie
;
; Revision 1.30  2011/01/28 18:59:33  nathan
; workaround for unset rectified keyword in some LASCO daily MVI files
;
; Revision 1.29  2010/11/19 22:35:00  nathan
; add /TIMES; prompt for rectified correction
;
; Revision 1.28  2010/10/27 17:04:48  mcnutt
; mearsures rtheta movies correctly
;
; Revision 1.27  2010/10/20 16:48:38  mcnutt
; corrected coords for rtheta movies
;
; Revision 1.26  2010/10/05 20:16:27  nathan
; recognize ARC (radial) projection
;
; Revision 1.25  2010/09/30 22:04:50  nathan
; order in output of hpc2hpr changed to match WCS convention
;
; Revision 1.24  2010/09/21 17:03:58  mcnutt
; call get_hdr_movie_img when reading in a png for gif movie
;
; Revision 1.23  2010/08/17 23:02:02  nathan
; comment out issoho section because is done in define_wcshdrs pro
;
; Revision 1.22  2010/08/17 16:05:11  nathan
; default osc=""
;
; Revision 1.21  2010/08/12 16:39:37  mcnutt
; added coords for rtheta movies
;
; Revision 1.20  2010/08/12 15:07:41  nathan
; auto fix case where cdelt in mvi headers is in deg
;
; Revision 1.19  2010/07/13 16:22:33  mcnutt
; added keyword fitsdir for fits files other then Level0
;
; Revision 1.18  2010/06/24 20:11:19  nathan
; define base vars in moviev as long
;
; Revision 1.17  2010/05/24 15:18:06  mcnutt
; added tag moviev.adjcolor
;
; Revision 1.16  2010/03/16 17:52:05  mcnutt
; changes to work with sccwrite_ht ab movies
;
; Revision 1.15  2010/01/20 13:51:57  mcnutt
; added keyword osczero for ab movies
;
; Revision 1.14  2010/01/15 20:08:03  mcnutt
; removed wcshdrs(moved to scc_movie_win) and object keyword
;
; Revision 1.13  2010/01/14 13:46:50  mcnutt
; removed stop
;
; Revision 1.12  2010/01/14 13:34:02  mcnutt
; uses file_search to cor2 TB headers
;
; Revision 1.11  2009/12/08 16:15:29  mcnutt
; removed some add_tags
;
; Revision 1.10  2009/12/07 19:57:27  mcnutt
; middle mouse button will always create sync file
;
; Revision 1.9  2009/11/19 16:23:31  mcnutt
; creates wcshdrs for cor2 when movie is read in
;
; Revision 1.8  2009/11/09 16:41:09  mcnutt
; added keyword wcshdr
;
; Revision 1.7  2009/11/06 13:41:35  mcnutt
; corrected plot_sync and removed errors when middle and right mouse buttons pressed if not sync
;
; Revision 1.6  2009/10/29 16:04:16  nathan
; slight change to debug message
;
; Revision 1.5  2009/10/21 14:52:38  mcnutt
; sets moivev.instxt for box selection
;
; Revision 1.4  2009/10/19 20:29:47  nathan
; Add /NOSUBFIELD as alternative to subfield prompt
;
; Revision 1.3  2009/10/08 18:54:43  mcnutt
; objectv from moviev structure
;
; Revision 1.2  2009/10/07 17:27:24  mcnutt
; changed common blocks moviev is now in common block scc_playmv_COMMON
;
; Revision 1.1  2009/10/05 18:53:15  mcnutt
; created from scc_wrunoviem.pro
;
;
;
; See Also    : MKMOVIEM.PRO
;
;-            


PRO SCC_PLAYMOVIEM_DRAW, ev

COMMON scc_playmv_COMMON, moviev

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe


   evx=ev.x
   evy=ev.y
   IF (TAG_EXIST(moviev, 'zoombase')) THEN zmbs=moviev.zoombase else zmbs=-1
   IF ((ev.press and 1) eq 1) and moviev.showzoom eq 1 and ev.top eq zmbs THEN markzoom=1 else markzoom=0 
   IF ((ev.press and 1) eq 1) and moviev.showzoom eq 0 THEN nozoom=1 else nozoom=0 

   if moviev.showzoom eq 1 and ev.top eq zmbs then begin
     evx=(evx/moviev.zstate.scale)+moviev.zstate.x0
     evy=(evy/moviev.zstate.scale)+moviev.zstate.y0
   endif 

   IF (moviev.deleted(moviev.current)) THEN RETURN

;   IF ((ev.press and 4) eq 4) THEN BEGIN    ; right mouse button pressed
;      sccSHOW_MENU
;      RETURN
;   ENDIF

   ;**********
   ;** Set up annotation text parameters
    IF (moviev.usebigfont) THEN BEGIN
    	csize=2
	fonttype=-1
	fontfac=1.2 ; for Triplex Roman font width
    	greek='!7'  ; complex greek
    ENDIF ELSE BEGIN
    	csize=1
	fonttype=0
	fontfac=1.
	greek='!4'  ; Simplex Greek
    ENDELSE
    texthgt=10*csize/fontfac
    ;pfx2=greek+'h!X='   
    

  ;**********
   ;** Display cursor xy location in lower right of draw window
    yoff = moviev.lowerxyval*40*moviev.vsize/1024
    WSET, moviev.draw_win
;   IF(edit) THEN TV, moviev.images[*,*,moviev.current]
    ;-- Determine size of output text
    text='(' + trim(evx) + ',' + trim(evy) + ')' 
    textlen=(3+strlen(trim(moviev.hsize))+strlen(trim(moviev.vsize)))*6*csize
    xout=moviev.hsize-textlen-1
    yout= 5+yoff
    DEVICE, COPY = [0, 0, textlen, texthgt+1, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, '!17'+text, /DEVICE, font=fonttype, charsize=csize, color=255

    xyout1='x,y=(' + string(evx,'(i4)') + ',' + string(evy,'(i4)') + ')'

    ;--use Triplex Roman Hershey vector font IFF CHARSIZE keyword set
   ;**********
   ;** Display DN in lower left of draw window
    yoff = 40*moviev.vsize/1024
    dnoff= moviev.lowerdnval*2*yoff
    ;-- Determine size of output text
    text='DN='+ TRIM(FIX((TVRD(evx,evy,1,1))(0))) 
    textlen=6*6*csize*fontfac
    xout=1
    yout= 5+dnoff
    DEVICE, COPY = [0, 0, textlen, texthgt, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, text, /DEVICE, font=fonttype, charsize=csize, color=255
    xyout2='DN='+ string(FIX((TVRD(evx,evy,1,1))(0)),'(i3)')

    thistime = moviev.img_hdrs[moviev.current].date_obs+' '+moviev.img_hdrs[moviev.current].time_obs
    aorb=strmid(moviev.img_hdrs[moviev.current].filename,strpos(moviev.img_hdrs[moviev.current].filename,'.f')-1,1)
   ;** calculate radius in pixels
    cx=     moviev.sunc(moviev.current).xcen	; IDL coord
    cy=     moviev.sunc(moviev.current).ycen
    solrc=  moviev.solr(moviev.current)     ; solar radius in arcsec
    arcsc=  moviev.arcs(moviev.current)     ; platescl arcsec/pixel
    ;--Do spherical projection correction (currently HI only)
    ;  or for EUVI do carrington lon/lat
    ;  or for COR2 just use wcs for computation
    unitf=3600.
    otherside=0

    crlnobs=0
    crltobs=0
 
     IF (TAG_EXIST(moviev, 'wcshdrs')) THEN BEGIN
       IF [evx ge moviev.osczero(0) and evy ge moviev.osczero(1) and n_elements(moviev.wcshdrs) eq moviev.len*2] THEN IF aorb eq 'A' then aorb='B' else aorb='A'
    	;--Compute coordinates of cursor location
	wcs=moviev.wcshdrs[moviev.current]
        IF (evx ge moviev.osczero(0) and evy ge moviev.osczero(1) and n_elements(moviev.wcshdrs) eq moviev.len*2) THEN BEGIN
          evx=evx-moviev.osczero(0)
          evy=evy-moviev.osczero(1)
          IF (moviev.debugon) THEN print,evx,evy
	  wcs=moviev.wcshdrs[moviev.current+moviev.len]
          otherside=1
	ENDIF else otherside=0

	IF (moviev.debugon) THEN BEGIN
	    cxy=wcs_get_pixel(wcs,[0,0])
	    cxd=cxy[0]
	    cyd=cxy[1]
	ENDIF
	cunit=wcs.cunit[0]
	IF cunit EQ 'deg' THEN unitf=1.
	arcsc=ABS(wcs.cdelt[0])*3600./unitf
    	xy=wcs_get_coord(wcs,[evx,evy])
	
	rx=xy[0]	; degrees or arcsec
	ry=xy[1]
	;rsec=sqrt(rx^2+ry^2)
	IF wcs.projection EQ 'ARC' THEN $
	    rth=[xy[0],xy[1]+90] ELSE $
	
	    rth=hpc2hpr(xy, DEGREES=(unitf LT 3600),/silent)
	; output always degrees
	; order in output of hpc2hpr changed to match WCS convention, nbr 9/30/11
	   
	r=rth[1]
	rpix=r*unitf/wcs.cdelt[0]
	;--Compute latitude angle
	    ;angle=-atan(rx,ry)*180./!pi
    	angle=rth[0]
	
	IF (moviev.img_hdrs(moviev.current).detector) EQ 'COR2' THEN BEGIN
 ;   	    aorb=rstrmid(moviev.img_hdrs(moviev.current).filename,4,1)
    	    rpix1 = Cor2_DISTORTION(rpix, aorb, arcsc) 	; input is pixels
	    ; output is pixels
	    r=r*rpix1/rpix
	    rpix=rpix1
	END
    	IF TAG_EXIST(wcs.position,'CRLN_OBS') THEN BEGIN
	    crlnobs=wcs.position.crln_obs
	    crltobs=wcs.position.crlt_obs
    	ENDIF
    ENDIF ELSE BEGIN

      IF (evx ge moviev.osczero(0) and evy ge moviev.osczero(1) and total(moviev.osczero) gt 0) THEN BEGIN
            evx=evx-moviev.osczero(0)
            evy=evy-moviev.osczero(1)
	    IF (moviev.debugon) THEN print,evx,evy
            otherside=1
      ENDIF ELSE otherside=0
      IF moviev.flatplane ne 0 then begin
          theta=[moviev.file_hdr.theta0,moviev.file_hdr.theta1]
          IF otherside eq 1 THEN theta=theta(sort(theta*(-1)))*(-1)
	    angle=double(evy*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0)
            r=double(evx*moviev.file_hdr.sec_pix+theta(0))
    	    rth=hpc2hpr([r,angle], /degrees, /silent);WCS_CONV_HPC_HPR,angle ,r ,HRLN, HRLT ,/ZERO_CENTER,ang_units='degrees',QUICK=doquick
            r=rth(1) & angle=rth(0)
      endif ELSE begin
    	;--Compute r
    	rpix = SQRT( FLOAT(evx-cx)^2 + FLOAT(evy-cy)^2 ) 
   ;** convert to degrees
    	CASE (moviev.img_hdrs(moviev.current).detector) OF
    	'C2' : BEGIN 	;** apply C2 geometric distortion 
            	d = C2_DISTORTION(rpix, arcsc)
		;returns arcsec
            	r = d / 3600.
            END
    	'C3' : BEGIN 	;** apply C3 geometric distortion 
            	;d = C3_DISTORTION(r*1024/moviev.hsize)
            	d = C3_DISTORTION(rpix, arcsc)
 		;returns arcsec
           	r = d / 3600.
            END
	'COR2' : BEGIN
	; in case no wcs headers retrieved
    	    	d = Cor2_DISTORTION(rpix, aorb, arcsc) 	; input is pixels
	    	;returns pixels
	    	r = d * arcsc/ 3600. 
	    END
    	ELSE : BEGIN	
            	r = rpix * arcsc / 3600.
            END
    	ENDCASE
    	;--Compute angle
    	angle = ATAN( FLOAT(cx-evx), FLOAT(evy-cy) ) * 180./!PI
   	;standard is counter-cw from north is positive
    	angle = angle + moviev.rolls[moviev.current]
      endelse
    ENDELSE
    angle0=angle
    r0=r
   ;angle = angle - moviev.rect + (roll * 180./!PI)
   
   ;**********
   ;** Compute values for chosen coordinate system and corresponding formatting
   ;
   ; vlong['Solar radii','Degrees ','Carrington','Heliographic Cartesian']

    ; moviev.elong EQ 1 , elongation in units Degrees
    pfx= 'E='
    pfx2='PA='
    sfx= '!9%!3!X'    ; deg symbol, then revert to original
    sfx2='!9%!3'
    sfxw= 'deg'
    ;sfx2w='deg'
    fmt= '(F6.2)'
    fmt2='(F6.2)'
    len =9
    len2=10
    IF arcsc LT 36 then begin
    ; at platescale of 36 arcsec/pixel, 1 pixel = 0.01 deg
    	fmt= '(F7.4)'
	len= 10
    endif
    fonttype=-1   

    ;
    IF moviev.elong EQ 2 THEN BEGIN   ;--Display Carrington latitude and longitude
    	IF (r * 3600./ solrc) LT 1.0 and datatype(wcs) NE 'UND' THEN BEGIN
	    cl=euvi_heliographic(wcs,[evx,evy],/CARRINGTON)
    	    r=	    cl[0]   ; longitude
    	    angle=  cl[1]   ; latitude
    	    pfx ='CLN='
    	    pfx2='CLT='
    	    fmt='(F6.2)'
	    len=11
	    IF arcsc LT 4 THEN BEGIN
	    	fmt='(F8.4)'
		len=13
	    ENDIF
    	    len2=len
	    fmt2=fmt
	ENDIF
    ENDIF 
    IF moviev.elong EQ 4 THEN BEGIN   ;--Display Stonyhurst latitude and longitude
    	IF (r * 3600./ solrc) LT 1.0 and datatype(wcs) NE 'UND' THEN BEGIN
	    cl=euvi_heliographic(wcs,[evx,evy])
    	    r=	    cl[0]   ; longitude
    	    angle=  cl[1]   ; latitude
    	    pfx ='LON='
    	    pfx2='LAT='
    	    fmt='(F6.2)'
	    len=11
	    IF arcsc LT 4 THEN BEGIN
	    	fmt='(F8.4)'
		len=13
	    ENDIF
    	    len2=len
	    fmt2=fmt
	ENDIF
    ENDIF 

    IF (moviev.elong EQ 0) THEN BEGIN	    ; elongation in solar radii
	r = r * 3600./ solrc   	; Rsun
	pfx='R='
    	sfx=''
	sfxw='   '
	;IF ~(moviev.usebigfont) THEN fonttype=0 
    ENDIF
    IF (moviev.elong EQ 3) THEN BEGIN	    ; heliographic cartesian
	IF r LT 5 THEN doquick=1 ELSE doquick=0
        IF moviev.flatplane eq 0 then begin
    	    WCS_CONV_HPR_HPC,angle ,r ,HPLN, HPLT ,/ZERO_CENTER,ang_units='degrees',QUICK=doquick
            r=HPLN & angle=HPLT
        endif ELSE begin
            angle=evy*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0
            r=evx*moviev.file_hdr.sec_pix+theta(0)
    	    fmt='(F7.2)'
	    len=13
        endelse
    	pfx= 'X='
    	pfx2='Y='
	fmt2=fmt
	len2=len
    ENDIF
    IF moviev.systemstr EQ 'STPLN' THEN BEGIN	; epipolar plane angle
    	IF thistime NE moviev.lasttime THEN $
	moviev.deltaroll =   get_stereo_roll(thistime, aorb, system='STPLN') $
             	    - get_stereo_roll(thistime, aorb, system='RTN')
        angle =  angle - moviev.deltaroll
	pfx2='STPLN='
	len2=len2+3
    ENDIF
    IF (moviev.elong EQ 5) THEN BEGIN	    ; Rtheta movie
    	if moviev.file_hdr.rtheta le 3 then begin 
	  angle=moviev.file_hdr.theta0+evx/(moviev.file_hdr.nx-1.)*(moviev.file_hdr.theta1-moviev.file_hdr.theta0)
          r=moviev.file_hdr.radius0+evy/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.radius1-moviev.file_hdr.radius0)
	  if moviev.file_hdr.rtheta eq 2 then $
		r=10^( alog10(moviev.file_hdr.radius0)+evy/(moviev.file_hdr.ny-1.) $     	
		  *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) )
    	  pfx= 'E='
    	  pfx2='PA='
	  fmt2='(F7.2)'
	  len2=len
        endif
	if moviev.file_hdr.rtheta ge 5 then begin
	  r=moviev.file_hdr.radius0+evx/(moviev.file_hdr.nx-1.)*(moviev.file_hdr.radius1-moviev.file_hdr.radius0)
          angle=moviev.file_hdr.theta0+evy/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.theta1-moviev.file_hdr.theta0)
	    if moviev.file_hdr.rtheta eq 6 then $
	       r=10^( alog10(moviev.file_hdr.radius0)+evx/(moviev.file_hdr.nx-1.) $     	
		  *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) ) 
    	  pfx= 'E='
    	  pfx2='PA='
	  fmt2='(F7.2)'
	  len2=len
    	endif 
        if moviev.file_hdr.rtheta eq 3 or moviev.file_hdr.rtheta eq 4 then begin
            angle=evy*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0
            r=evx*moviev.file_hdr.sec_pix+moviev.file_hdr.theta0
            if moviev.file_hdr.rtheta eq 3 then  begin
              pfx ='CLN='
              pfx2='CLT='
            endif else begin
              pfx ='LON='
    	      pfx2='LAT='
	    endelse
    	    fmt='(F7.2)'
	    len=13
    	endif
    ENDIF


    IF (angle lt 0) and (pfx ne 'CLN=') and (pfx ne 'X=')  and (pfx ne 'LON=') and (moviev.file_hdr.rtheta lt 1) THEN angle=angle+360.  
    
   ;**********
   ;** Display angle in upper left of draw window
   ;
    ;-- Determine size of output text
    text=pfx + TRIM(STRING(r,FORMAT=fmt)) +sfx
    IF cx GE moviev.hsize/2 THEN maxx=cx ELSE maxx=moviev.hsize-cx
    IF cy GE moviev.vsize/2 THEN maxy=cy ELSE maxy=moviev.vsize-cy
    maxr=fix(sqrt(float(maxx)^2+float(maxy)^2)* arcsc / solrc)
    textlen=len*6*csize
    xout=1
    yout= moviev.vsize-18-yoff
    DEVICE, COPY = [0, 0, textlen, texthgt, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, text, /DEVICE, font=fonttype, charsize=csize, color=255
    xyout3=pfx + TRIM(STRING(r,FORMAT=fmt)) +sfxw


   ;**********
   ;** Display angle in upper right of draw window (CCW positive) -OR-
   ;           Declination -OR- solar latitude
    ;-- Determine size of output text
    text=pfx2 + TRIM(STRING(angle,FORMAT=fmt2)) + sfx2
    textlen=len2*6*csize
    xout=moviev.hsize-textlen-1
    yout= moviev.vsize-18-yoff
    DEVICE, COPY = [0, 0, textlen, texthgt, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, text, /DEVICE, font=fonttype,charsize=csize, color=255
    xyout4=pfx2+TRIM(STRING(angle,FORMAT=fmt2))+'deg'

    coordval=xyout1+' '+xyout2+' '+xyout3+' '+xyout4;,format='(a17,1x,a6,1x,a10,1x,a12)')
    widget_control,moviev.mousecoords,set_value=coordval

    moviev.lasttime=thistime
    
if moviev.debugon THEN help,maxr,solrc,arcsc,cx,cy,angle,rpix,cxd,cyd
IF moviev.debugon THEN print,evx,evy
if moviev.debugon THEN wait,2


    temptime=strmid(moviev.img_hdrs(moviev.current).date_obs,0,10)+'T'+moviev.img_hdrs(moviev.current).time_obs	    
    IF (ev.press GE 1) THEN BEGIN
    	textf=6
    	if moviev.featwin ne moviev.current then moviev.nhtfeat=0 else moviev.nhtfeat=moviev.nhtfeat+1
    	moviev.featwin=moviev.current
    	if moviev.nhtfeat lt 10 then featnums=string(moviev.nhtfeat,'(i1.1)') else featnums=string(moviev.nhtfeat,'(i2.2)')
    	if moviev.nhtfeat ge 10 then textf=textf*2 
    	if pfx eq 'X=' or pfx eq 'CLN=' or pfx eq 'LON=' then fstring='(f9.4,1x,a19,f9.4,a5,2x,a,2i5)' else fstring='(f9.3,1x,a19,f9.1,a5,2x,a,2i5)'
    	if pfx eq 'CLN=' then nhtstr=     '#  CRLN      DATE     TIME      CRLT   TEL  FC  COL  ROW' else $
    	if pfx eq 'LON=' then nhtstr=     '#   LON      DATE     TIME       LAT   TEL  FC  COL  ROW' else $
    	if pfx eq 'X=' then nhtstr=       '#     X      DATE     TIME        Y    TEL  FC  COL  ROW' else $
	                           nhtstr='# HEIGHT     DATE     TIME      ANGLE  TEL  FC  COL  ROW'
      
    	;if nhtfeat eq 0 then PRINT,nhtstr
    	if moviev.celong ne moviev.elong then PRINT, nhtstr 	      
    	if moviev.celong ne moviev.elong then moviev.celong=moviev.elong      
	
	PRINT,r,temptime,angle,moviev.img_hdrs(moviev.current).detector,featnums,evx,evy,FORMAT=fstring	      
    ENDIF
    IF (ev.press EQ 2) THEN BEGIN  ;Middle mouse button pressed
	tempheight=r0
	posang=angle0
	carrflag=''
	; by default save Elong, Pos Angle, otherwise:
    	IF pfx EQ 'CLN=' THEN BEGIN
	    tempheight=r
	    posang=angle
	    carrflag='CARRINGTON'
	ENDIF ELSE $
	IF (r * 3600./ solrc) LT 1.0 and datatype(wcs) NE 'UND' THEN BEGIN
	    cl=euvi_heliographic(wcs,[evx,evy],/CARRINGTON)
    	    tempheight= cl[0]   ; longitude
    	    posang= 	cl[1]   ; latitude
	    carrflag='CARRINGTON'
	ENDIF
	taitime=utc2tai(str2utc(temptime))
	syncpoint=arr2str([taitime,tempheight,posang],' ')
	;help,temptime,tempheight,posang
	PRINT,'Saving ',temptime,tempheight,posang	      
	OPENW, lun,'$HOME/syncjmapwithmovie.dat', /get_lun
	printf,lun, syncpoint+'  '+carrflag
	free_lun,lun
	WSET, moviev.draw_win
        DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index[moviev.current]]
	moviev.sync_point[*,moviev.current]=[evx,evy]
	moviev.sync_color=1
	
;        if ~tag_exist(moviev,'sync_point') then moviev=add_tag(moviev,[ev.x,ev.y],'sync_point') else moviev.sync_point=[ev.x,ev.y]
    	;print,'cxy=     '+string(xy[0],'(f10.6)')+string(xy[1],'(f12.6)')+' rth='+string(r,'(f10.6)')+string(angle,'(f12.6)')+' at frame '+trim(moviev.current)
	plot_sync
    ENDIF

    IF (otherside eq 1) THEN BEGIN  ;return evx and evy back to original value for zoom window and ht mark
      evx=evx+moviev.osczero(0)
      evy=evy+moviev.osczero(1)
      IF (moviev.debugon) THEN print,evx,evy
   ENDIF
;   LOADCT, 0, /SILENT
   ;**********
   ; Save data points for Height-Time Plots
   IF ((ev.press and 1) eq 1) THEN BEGIN          ; Left mouse button pressed
     IF (STRLEN(moviev.htfile) ne 0) and (markzoom eq 1 or nozoom eq 1)THEN BEGIN
       sccFILL_HT,htfile,moviev.img_hdrs(moviev.current),r,angle,evx,evy,featnum,pfx,advance,label,ab=otherside
       ; sccfill_ht is compiled in sccwrite_ht.pro
       moviev.htfile=htfile
       if label eq 'donotlabel' then featnums=string(featnum,'(i1.1)') else featnums=label
       WSET, moviev.draw_win
       textlen=(textf*csize*fontfac)*strlen(featnums)
       if featnum ne -1 then begin
         if label ne 'donotlabel' then DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
         DEVICE, COPY = [0, 0, textlen, texthgt, evx,evy, 11] ; clear space for letters
         xyouts,evx,evy,featnums, /DEVICE, font=fonttype, charsize=csize, color=255
         if label ne 'donotlabel' then begin
             WSET, moviev.win_index(moviev.current)
	     if datatype(imgs) eq 'BYT' then begin
               wset,11
               timg=bytarr(moviev.hsize, moviev.vsize)*0
               timg(evx-2:evx+textlen,evy-2:evy+texthgt)=125
               tv,timg,true=0
	       xyouts,evx,evy,featnums, /DEVICE, font=fonttype, charsize=csize, color=255
               limg=tvrd(true=0)
               if moviev.file_hdr.truecolor eq 1 then begin
                  img=reform(imgs(*,*,*,moviev.current))
	          for nc = 0,2 do begin
                     timg=reform(img(nc,*,*))
		     timg(where(limg gt 100))=0
		     z=where(limg gt 200)
                     if z(0) gt -1 then timg(z)=255
		     img(nc,*,*)=timg
                  endfor 
                  imgs[*,*,*,moviev.current] = img
	       endif else begin
 		 img=imgs[*,*,moviev.current]
 		 img(where(limg gt 100))=0
		 z=where(limg gt 200)
                 if z(0) gt -1 then img(z)=255
                 imgs[*,*,moviev.current] = img
	       endelse
               erase,0
               WSET, moviev.win_index(moviev.current)
	       tv,img,true=moviev.truecolor
               wset,moviev.draw_win
               DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.draw_win]
	     endif else begin
               WSET, moviev.win_index(moviev.current)
               DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.draw_win]
               WSET, moviev.draw_win
	    endelse
         endif
         if markzoom eq 1 then begin
           save_win = !D.WINDOW
           WSET, moviev.zstate.zoom_win
           DEVICE, COPY = [0, 0, textlen, texthgt, ev.x,ev.y, 11] ; clear space for letters
           xyouts,ev.x,ev.y,featnums, /DEVICE, font=fonttype, charsize=csize, color=255
           WSET,save_win
         endif
       endif
     ENDIF 
   END 

; select box

   IF ((ev.press and 1) eq 1) and (TAG_EXIST(moviev, 'zoombase')) THEN zmbs=moviev.zoombase else zmbs=-1
   if (ev.top ne zmbs and (ev.press and 1) eq 1) then begin 
     if ((ev.press and 1) eq 1) and (moviev.box(0) eq 3) then begin ; recenter box
       xcen=moviev.box(1)+(moviev.box(3)-moviev.box(1))/2
       ycen=moviev.box(2)+(moviev.box(4)-moviev.box(2))/2
           xadj=ev.x-xcen
           yadj=ev.y-ycen
      if (moviev.box(9) eq 0) then begin
          WIDGET_CONTROL, moviev.box(5), GET_VALUE=valx
          WIDGET_CONTROL, moviev.box(6), GET_VALUE=valy
	  if moviev.box(1)+xadj lt 0 then moviev.box(1)=0
	  if moviev.box(1)+xadj ge 0 and moviev.box(3)+xadj le moviev.hsize-1 then moviev.box(1)=moviev.box(1)+xadj
	  if moviev.box(3)+xadj gt moviev.hsize-1 then moviev.box(1)= (moviev.hsize-valx) -1
          moviev.box(3)=(moviev.box(1)+valx) -1
	  if moviev.box(2)+yadj lt 0 then moviev.box(2)=0
	  if moviev.box(2)+yadj ge 0 and moviev.box(4)+yadj le moviev.vsize-1 then moviev.box(2)=moviev.box(2)+yadj
	  if moviev.box(4)+yadj gt moviev.vsize-1 then moviev.box(2)= (moviev.vsize-valy) -1
          moviev.box(4)=(moviev.box(2)+valy) -1
          xadj=0 & yadj=0 & ubox=moviev.box
      endif
      if (moviev.box(9) eq 1) then begin
          moviev.centadj(moviev.current,0:1)=[xadj,yadj]
          gd=indgen(moviev.len)+1
          uv=where(moviev.centadj(*,0) ne -1,cnt)
	  if cnt ge 2 then moviev.centadj(*,2)=interpol(reform(moviev.centadj(uv,0)),gd(uv),gd) else moviev.centadj(*,2)=xadj
          uv=where(moviev.centadj(*,1) ne -1,cnt)
	  if cnt ge 2 then moviev.centadj(*,3)=interpol(reform(moviev.centadj(uv,1)),gd(uv),gd) else moviev.centadj(*,3)=yadj
          if moviev.box(8) eq 1 then begin
             z=where(moviev.centadj(*,2)+moviev.box(3) gt moviev.hsize,cnt)
	     if cnt gt 0 then moviev.centadj(z,2) = (moviev.hsize-moviev.box(3))-1
	     z=where(moviev.centadj(*,3)+moviev.box(4) gt moviev.vsize,cnt)
	     if cnt gt 0 then moviev.centadj(z,3) = (moviev.vsize-moviev.box(4))-1
             z=where(moviev.box(1)+moviev.centadj(*,2) lt 0,cnt)
	     if cnt gt 0 then moviev.centadj(z,2) = moviev.box(1)*(-1)
             z=where(moviev.box(2)+moviev.centadj(*,3) lt 0,cnt)
	     if cnt gt 0 then moviev.centadj(z,3) = moviev.box(2)*(-1)
	  endif
      endif
      draw_box, event=ev
      WIDGET_CONTROL, moviev.box(5), SET_VALUE=string(moviev.box(3)-moviev.box(1)+1)
      WIDGET_CONTROL, moviev.box(6), SET_VALUE=string(moviev.box(4)-moviev.box(2)+1)
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='click on image to recenter'
     endif
 
   if ((ev.press and 1) eq 1) and (moviev.box(0) eq 2) then begin ; second corner selected
      moviev.box(3)=ev.x
      moviev.box(4)=ev.y
      moviev.box(0)=3 ;set to 3 when rxycen get defined.
      if moviev.box(1) gt moviev.box(3) then begin
          tmp=moviev.box(3) & moviev.box(3) = moviev.box(1) & moviev.box(1)=tmp
      endif
      if moviev.box(2) gt moviev.box(4) then begin
          tmp=moviev.box(4) & moviev.box(4) = moviev.box(2) & moviev.box(2)=tmp
      endif
      WIDGET_CONTROL, moviev.box(5), SET_VALUE=string(moviev.box(3)-moviev.box(1)+1)
      WIDGET_CONTROL, moviev.box(6), SET_VALUE=string(moviev.box(4)-moviev.box(2)+1)
      draw_box
   endif

   if ((ev.press and 1) eq 1) and (moviev.box(0) eq 1) then begin ; first corner selected
      moviev.box(1)=ev.x
      moviev.box(2)=ev.y
      moviev.box(0)=2
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='Select upper right corner'
   endif

   if ((ev.press and 1) eq 1) and (moviev.box(0) eq 6) then begin ; first corner selected
      if ev.x lt moviev.box(1) then moviev.box(4)=moviev.box(1)-ev.x else moviev.box(4)=ev.x-moviev.box(1)
      moviev.box(0)=0
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='use Re-select ROI to change coords'
      wset,moviev.boxwin
      tvcircle,moviev.box(4),moviev.box(1),moviev.box(2),color=255
      wset,moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.boxwin]
   endif
   if ((ev.press and 1) eq 1) and (moviev.box(0) eq 5) then begin ; second corner selected
      if ev.x lt moviev.box(1) then moviev.box(3)=moviev.box(1)-ev.x else moviev.box(3)=ev.x-moviev.box(1)
      moviev.box(0)=6
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='Select outer radius in X coords'
      wset,moviev.boxwin
      tvcircle,moviev.box(3),moviev.box(1),moviev.box(2),color=255
      wset,moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.boxwin]
   endif
   if ((ev.press and 1) eq 1) and (moviev.box(0) eq 4) then begin ; first corner selected
      moviev.box(1)=ev.x
      moviev.box(2)=ev.y
      moviev.box(0)=5
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='Select inner radius in X coords'
      wset,moviev.boxwin
      tmp=imgs(*,*,moviev.current)
      tmp(ev.x,ev.y-5:ev.y+5)=255
      tmp(ev.x-5:ev.x+5,ev.y)=255
      tv,tmp
      wset,moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.boxwin]
   endif
   endif

  if datatype(advance) ne 'INT' then advance=moviev.box(7)
       if ((ev.press and 1) eq 1) and advance eq 1 then begin
                 if (moviev.box(0) eq 7) then wait,2
		 moviev.forward = 1
		 REPEAT BEGIN
		    moviev.current = (moviev.current + 1) MOD moviev.len
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.first
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.first
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                 WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                 WIDGET_CONTROL, moviev.slidenw, SET_VALUE=moviev.current
                 if (moviev.showzoom eq 1) then begin
                     ;WIDGET_CONTROL, base, SET_UVALUE=moviev
                    draw_zoom
                 endif 
                 IF moviev.sync_color GE 1 THEN plot_sync
                 if (moviev.box(0) eq 7) then draw_box 
       endif

END

PRO SCC_PLAYMOVIEM, win_index0, bytmin, bytmax, NAMES=names, HDRS=hdrs, START=start, LENGTH=length, $
    	PAN=scale, SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, R_SUN=R_SUN, DEBUG=debug, $
	PREVIOUS=previous, RECTIFIED=rectified, ARROW_CURSOR=arrow_cursor, NOSUBFIELD=nosubfield, $
        ROLL_PER_FRAME=roll_per_frame, EDITk=editk, LOWERXY=lowerxy, LOWERDN=raisedn, BIGFONT=bigfont, $
	USE_R=use_r, USE_E=use_e, USE_STPLN=use_stpln, objects=objects, file_hdr=file_hdr, filename=filename,$
	CROSSHAIR_CURSOR=crosshair_cursor, CARRINGTON=carrington, PRECOMMCORRECT=precommcorrect, TIMES=times, $
	AVERAGE_FRAMES=AVERAGE_FRAMES,SMOOTH_BOX=SMOOTH_BOX,wcshdr=wcshdr,osczero=osczero,fitsdir=fitsdir,nofits=nofits, _EXTRA=_extra

COMMON WSCC_MKMOVIE_COMMON   ; defined above
COMMON scc_playmv_COMMON

   IF KEYWORD_SET(HDRS)THEN in_hdrs = hdrs ; so input headers do not get changed
   dotimes=keyword_set(TIMES)
   debugon=keyword_set(DEBUG)
   debug_on=debugon ; for another common block
   IF keyword_set(LOWERXY) THEN lowerxyval=0. ELSE lowerxyval=1.
   IF keyword_set(LOWERDN) THEN lowerdnval=0. ELSE lowerdnval=1.
   IF keyword_set(BIGFONT) THEN usebigfont=bigfont ELSE usebigfont=0
   bkgcolor=0	; background for annotation text
   radec=0  	; flag for future use
   elong=1  	; flag for type of altitude designation; 1 = Distance from Sun is printed as Elongation in Degrees
   IF keyword_set(USE_STPLN) THEN systemstr='STPLN' ELSE systemstr=''   ; systemstr in common block

    bell = STRING(7B)
   IF XRegistered("SCC_PLAYMOVIEM") THEN BEGIN
      PRINT, bell
      print,'%%There is an instance SCC_PLAYMOVIEM already running.  Only one instance may run at a time.'
      print,'%%Recommend either exitting existing PLAYMOVIE session or type WIDGET_CONTROL,/RESET.'
      RETURN
   ENDIF


   edit=0
   edit = keyword_set(EDITk) or keyword_set(AVERAGE_FRAMES) or keyword_set(SMOOTH_BOX)
   IF ~(edit) THEN begin
    	print,''
	print,'Note that you may not change color table, edit or delete frames without the /EDIT keyword set.'
	print,''
   ENDIF
   if keyword_set(objects) then edit=1

   IF(datatype(bytmin) NE 'UND' AND datatype(bytmax) NE 'UND') THEN BEGIN
	bmin = bytmin
	bmax = bytmax
   ENDIF
   IF(datatype(scale) NE 'UND') THEN pan = scale

   lastdate = ''
   lasttime=''
   stall0 = 80
   stall = (100 - stall0)/50.
   stallsave = stall
   if datatype(filename) eq 'UND' then inpfilename = '' else  inpfilename = filename
   filepath = './'
   ftitle=''
   rect=0
;   undefine,wcshdrs

   ;** display previous movie
   IF (KEYWORD_SET(PREVIOUS) NE 0) THEN BEGIN
      win_index = moviev.win_index2
      hdrs = moviev.hdrs2
      rolls= moviev.rolls2
   ENDIF ELSE $

   ;** have user select movie file (.mvi)
   IF (DATATYPE(win_index0) EQ 'UND') THEN BEGIN
      file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path)
      IF (file EQ '') THEN RETURN
      BREAK_FILE, file, a, dir, name, ext
      IF (dir EQ '') THEN win_index = path+file ELSE win_index = file
   ENDIF ELSE win_index=win_index0

    images = 0
    mviflag = 0
	
    if keyword_set(wcshdr) then wcshdrs=wcshdr
    ; ####### read movie in from movie file (.mvi)
    IF (DATATYPE(win_index) EQ 'STR') THEN BEGIN
      mviflag = 1
      BREAK_FILE, win_index, a, dir, name, ext
      ftitle = name+ext
      inpfilename = win_index
      filepath = dir
      IF (dir EQ '') THEN filepath = './'
      undefine,imgs
      if strpos(ftitle,'.hdr') gt -1 then pngmvi=inpfilename else pngmvi='0'
      OPENR,lu,inpfilename,/GET_LUN
      SCCREAD_MVI, lu, file_hdr, ihdrs, imgsin, swapflag , pngmvi=pngmvi

      IF ~keyword_set(START) THEN start=0
      ; define lastframe of current MVI file
      IF KEYWORD_SET(LENGTH) THEN $
      	lastframe=start+length-1 ELSE $
	lastframe=file_hdr.nf-1
      
      lastframe = lastframe<(file_hdr.nf-1)
      
      nf=lastframe-start+1
      ;nf = file_hdr.nf
      nx = file_hdr.nx
      ny = file_hdr.ny
      rect=file_hdr.rectified

    	mviversion=file_hdr.ver
	print,''
    	help,mviversion
	print,''
     	IF (debugon) THEN BEGIN
	    help,/str,file_hdr
	    wait,3
	ENDIF 

    ;--Check for out of order frames and omit them
    hdrs0=SCCMVIHDR2STRUCT(ihdrs[start],file_hdr.ver)
    alltai=utc2tai(anytim2utc(hdrs0.date_obs+' '+hdrs0.time_obs))
    findx=start
    if hdrs0.date_obs ne '' then begin
	xind=0
	FOR i=start+1,lastframe DO BEGIN
    	    hdri=SCCMVIHDR2STRUCT(ihdrs[i],file_hdr.ver)
	    date_obsi=hdri.date_obs+' '+hdri.time_obs
	    taii=utc2tai(anytim2utc(date_obsi))
    	    IF taii GT alltai[xind] THEN BEGIN
		hdrs0=[hdrs0,hdri]
		findx=[findx,i]
		alltai=[alltai,taii]
		xind=xind+1
	    ENDIF ELSE BEGIN
		print,'Frame',i,' ',date_obsi,' out of order'
		wait,2
	    ENDELSE
	ENDFOR
    endif else begin
    	xind=nf-1
    	findx=indgen(nf)
    	FOR i=start+1,lastframe DO hdrs0=[hdrs0,SCCMVIHDR2STRUCT(ihdrs[i],file_hdr.ver)]
    endelse
      nf1=n_elements(findx)
      IF nf1-1 NE xind THEN message,'error in findx'
      if keyword_set(AVERAGE_FRAMES) then bufsz=AVERAGE_FRAMES else  bufsz=1
      nf1=nf1/bufsz 
      win_index = INTARR(nf1)
      names = STRARR(nf1)
      frame_date=''
      aroll=0.0
      IF(edit) and file_hdr.truecolor le 0 THEN imgs = bytarr(nx, ny, nf1)  
      IF(edit) and file_hdr.truecolor gt 0 THEN imgs = bytarr(3,nx, ny, nf1)
      IF keyword_set(AVERAGE_FRAMES) and file_hdr.truecolor gt 0 THEN begin
         print,'Can not average frames of a truecolor movie:  returning'
	 return
     endif
      IF keyword_set(SMOOTH_BOX) and file_hdr.truecolor gt 0 THEN begin
         print,'Can not smooth frames of a truecolor movie:  returning'
	 return
     endif

;  -- START read each frame from MVI file
      FOR i=0,nf1-1 DO BEGIN
         WINDOW, XSIZE = nx, YSIZE = ny, /PIXMAP, /FREE
         win_index(i) = !D.WINDOW
         IF (pngmvi eq '0') THEN BEGIN
	   IF (file_hdr.truecolor EQ 0) THEN imgi=imgsin[*,*,findx[i]] ELSE imgi=imgsin[*,*,*,findx[i]]
         ENDIF ELSE imgi=get_hdr_movie_img(filepath+string(imgsin[findx[i]]),truecolor=file_hdr.truecolor)

	 
	 IF bufsz GT 1 THEN begin   ;if called with average_frames keyword set
             PRINT, '%%SCC_PLAYMOVIEM averaging frames ', STRING(findx[(i*bufsz)]+1,FORMAT='(I4)'), ' - ', $
	     	STRING(findx[(i*bufsz)+bufsz]+1,FORMAT='(I4)'), ' of ',STRING(nf,FORMAT='(I4)'),' from movie file ', inpfilename
	    bufim=bytarr(nx,ny,bufsz)
            for ib=0,bufsz-1 do begin
  	       bufim(*,*,ib)=imgsin[*,*,findx[(i*bufsz)+ib]]
               print,findx[(i*bufsz)+ib]
            endfor
	    image1=round(rebin(float(bufim),nx,ny,1))
         endif  else begin
             PRINT, '%%SCC_PLAYMOVIEM reading frame ', STRING(findx[i]+1,FORMAT='(I4)'), ' of ',STRING(nf,FORMAT='(I4)'), $
                ' from movie file ', inpfilename
	    if file_hdr.truecolor le 0 THEN image1=imgi else image1=imgi
         endelse
         if keyword_set(SMOOTH_BOX)then begin
   	    image1 = smooth(temporary(image1),smooth_box,/NAN)
	    print,'Applying SMOOTH() with box=',smooth_box
	 endif 
	 IF(edit) and file_hdr.truecolor le 0 THEN imgs[*,*,i] = image1
	 IF(edit) and file_hdr.truecolor gt 0 THEN imgs[*,*,*,i] = image1
         TV, image1,true=file_hdr.truecolor
         ahdr = hdrs0[(i*bufsz)]    ; hdrs0 is formed when frames are omitted due to time order
;         IF ( swapflag EQ 1 ) THEN BYTEORDER, ahdr
;         IF (i EQ 0) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
         IF (i EQ 0) THEN BEGIN
           hdrs=ahdr
           rolls= aroll 
         ENDIF ELSE BEGIN
           hdrs=[hdrs,ahdr]
           rolls= [rolls,aroll]
         ENDELSE
    	IF (debugon) THEN help,/str,ahdr
	
	 IF dotimes THEN BEGIN
            IF not keyword_set(CHSZ) then charsz=1.0 else charsz=CHSZ
	    IF times GT 1 THEN time_color=times ELSE time_color=255
            xouts=10 & youts=10
            XYOUTS, xouts, youts, strmid(ahdr.date_obs,0,4)+'/'+strmid(ahdr.date_obs,5,2)+'/'+strmid(ahdr.date_obs,8,2)+' '+strmid(ahdr.time_obs,0,8),/DEVICE, CHARSIZE=charsz, COLOR=time_color
            IF ~keyword_set(NOCAM) THEN begin
                no_cam=1	    
		sc=strmid(ahdr.filename,strpos(ahdr.filename,'.')-1,1)
                camt=ahdr.detector
		IF sc EQ 'A' or sc EQ 'B' THEN camt=camt+'-'+sc
	        cyouts=youts+ charsz*10 + 5
     	        XYOUTS, xouts, cyouts, strupcase(camt), /DEVICE,  CHARSIZE=charsz, COLOR=time_color
            ENDIF else no_cam=0


	 ENDIF

      ENDFOR
; -- END  read each frame from MVI file

      names = hdrs.filename
      CLOSE,lu
      FREE_LUN,lu
    ENDIF  $ ; ######## read in MVI file
    ELSE BEGIN
        nf = n_elements(hdrs)
    	;nx = !d.xsize
    	;ny = file_hdr.ny
    	;rect=file_hdr.rectified
    	mviversion=6
    ENDELSE

   win_index2 = win_index	;** save in COMMON
   hdrs2 = hdrs	;** save in COMMON

;   IF (datatype(rolls) eq 'UND') THEN IF tag_exist(hdrs,'crota') THEN rolls = hdrs.crota else rolls=hdrs.roll 
;   rolls2= rolls
   
   ;** load movies from existing pixmaps
   WSET, win_index(0)
   hsize = !D.X_SIZE
   vsize=!D.Y_SIZE             ; see about 10 lines above  SHH 7/12/96
   ;** get length of movie

   len = N_ELEMENTS(win_index)
   if len eq 1 then stall =10000
   stallsave = stall


   frames = STRARR(len)	;** array of movie frame names (empty)
   IF (KEYWORD_SET(names) NE 0) THEN frames = names

   first = 0
   last = len-1
   current = 0
   forward = 1
   dirsave = 1
   bounce = 0
   pause = 0
   dopause = 1
   deleted = BYTARR(len)
   showmenu = 0
   showzoom = 0
   showtext = 0
   htfile = ''
    nframes= n_elements(hdrs)
    rolls= fltarr(nframes)
    frame_date=''
    ahdr=hdrs[0]
    det=ahdr.detector
    dindx=where(['C2','C3','EIT'] EQ det,issoho)
    d2indx=where(['EIT'] EQ det,iseit)
    eindx=where(['EUVI'] EQ det, iseuv)
    cindx=where(['COR2'] EQ det, iscor2)
    dindx=where(['COR1'] EQ det, iscor1)
    IF strmid(ahdr.detector,0,2) EQ 'HI' THEN ishi=1 ELSE ishi=0
    osc= strmid(ahdr.filename,strpos(ahdr.filename,'.f')-1,1)
    IF osc NE 'A' and osc NE 'B' THEN osc=''
    IF ahdr.detector EQ 'HI2' THEN ishi2=1 ELSE ishi2=0
    IF datatype(file_hdr) EQ 'STC' THEN arcs = file_hdr.sec_pix ELSE arcs=0
    IF arcs LE 0 THEN arcs=ahdr.cdelt1 
    
    IF ishi and arcs LT 1 THEN BEGIN
    	; fix case if cdelt1 is in degrees
	arcs=ahdr.cdelt1*3600
	hdrs[*].cdelt1=arcs
    ENDIF
    IF arcs LE 0 and issoho THEN arcs = GET_SEC_PIXEL(ahdr, FULL=hsize) ;ELSE BEGIN
;    	arcs0=arcs
;	aorb=strmid(ahdr.filename,20,1)
;	cdelt=getsccsecpix(det,aorb,/arcsec,/fast)
;	arcs=cdelt*2048./nx
;    	message,'Invalid CDELT='+trim(arcs0)+' found; using '+trim(arcs)+' instead.',/info
;    ENDELSE
    IF arcs LT 0 THEN BEGIN
	inp=0
	print
	print,'Error: MVI platescl=',trim(arcs)
	print,'Please enter the FFV pixel size of the movie frame:'
	read, ' 2048   1024   512   256 :',inp
	farcs=getsccsecpix(ahdr.detector,osc)
	arcs=farcs*2048./inp
	IF ishi THEN arcs=arcs*3600
    ENDIF


    solr=ahdr.rsun	    ; no radius in file_hdr
    if solr eq 0. then begin 
       if datatype(in_hdrs) ne 'UND' then uhdr=in_hdrs[0] else uhdr=ahdr
       fhdr = convert2secchi(uhdr,file_hdr, debug=debug_on) 
       solr=fhdr.rsun
    endif
    if solr eq 0. then solr = 1.  ;to avoid divid by zero errors.

    sunc = {sunc, xcen:0., ycen:0.}

    IF keyword_set(SUNXCEN) THEN BEGIN
    	sunc.xcen=sunxcen
	IF datatype(file_hdr) EQ 'STC' THEN file_hdr.sunxcen=sunxcen
    ENDIF 
    IF keyword_set(SUNYCEN) THEN BEGIN
	sunc.ycen=sunycen
	IF datatype(file_hdr) EQ 'STC' THEN file_hdr.sunycen=sunycen
    ENDIF 
    IF keyword_set(SEC_PIX) THEN BEGIN
	IF datatype(file_hdr) EQ 'STC' THEN file_hdr.sec_pix=sec_pix
    ENDIF 

    IF datatype(file_hdr) EQ 'STC' THEN BEGIN
    	sunc.xcen=file_hdr.sunxcen
	sunc.ycen=file_hdr.sunycen
    ENDif
    IF datatype(hdrs) EQ 'STC' and sunc.xcen eq 0 THEN BEGIN
    	sunc.xcen=hdrs(0).xcen
	sunc.ycen=hdrs(0).ycen
    ENDif
   IF KEYWORD_SET(SUNXCEN) THEN FOR i=0,len-1 DO hdrs[i].xcen=sunxcen
   IF KEYWORD_SET(SUNYCEN) THEN FOR i=0,len-1 DO hdrs[i].ycen=sunycen
   IF KEYWORD_SET(SEC_PIX) THEN FOR i=0,len-1 DO hdrs[i].cdelt1=sec_pix
   IF KEYWORD_SET(R_SUN)   THEN FOR i=0,len-1 DO hdrs[i].rsun=r_sun

    asunc=sunc
    
;        IF (SECCHI GT 0) THEN BEGIN
;           solr = hdrs(0).rsun   ; radius of sun (arcs)
;           IF (solr EQ 0.0) THEN BEGIN
;            hee_r= GET_STEREO_LONLAT(hdrs(0).date_obs, STRMID(hdrs(0).obsrvtry,7,1), $
;                                /au, /degrees , system='HEE',_extra=_extra)
;            solr = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0) ; convert from radian to arcs
;           ENDIF
;         ENDIF 
	 
    sunc = REPLICATE(sunc,nframes)
    arcs = REPLICATE(arcs,nframes)
    solr = REPLICATE(solr,nframes)

   IF len NE nframes THEN message,'pixmap and hdr mismatch'

ccdpos=file_hdr.ccd_pos
IF keyword_set(NOSUBFIELD) THEN file_hdr.ccd_pos[*]=0 ELSE $
IF total(ccdpos) NE 0 THEN BEGIN
; some HI2 movies have weird values for ccd_pos; do double-check
; based on R1COL value
    print,'R1ROW, R1COL, R2ROW, R2COL:',ccdpos
    if debugon then wait,3
    IF	(ishi and osc EQ 'A' and ccdpos[1] NE 51) OR $
     	(ishi and osc EQ 'B' and ccdpos[1] NE 79) OR $
	(iscor2 and osc EQ 'A' and ccdpos[1] NE 129) or $
	(iscor2 and osc EQ 'B' and ccdpos[1] NE 1) or $
	(iscor1 and osc EQ 'B' and ccdpos[1] NE 129) or $
	(iscor1 and osc EQ 'A' and ccdpos[1] NE 1) THEN BEGIN
    	inp=''
	print,''
	print,'       !!!!!!!! WARNING: Subfield detected. !!!!!!!!'
	print,' If this movie is not a subfield, override with /NOSUBFIELD.'
	print,''
	wait,3
;	print,'If this is a subfield, enter y'
;	read, 'else return if this is a FFV movie: ',inp
;	IF inp EQ '' THEN file_hdr.ccd_pos[*]=0
    ENDIF
ENDIF

; get all per-frame header values here
    FOR i=0, nframes-1 DO BEGIN 
    	ahdr=hdrs[i]
	ahdr.cdelt1=arcs[i]
        if strlen(ahdr.date_obs) lt 10 then print,'!!!ERROR IN MOVIE FRAME HEADER DATE-OBS MISSING!!!'
    	dateobs=strmid(ahdr.date_obs,0,10)
          ; if SCC_PLAYMOVIEM is called from scc_wrunmovie and scc_wrunmovie is
           ; called from scc_mkmovie (so input to wrunmoivem is not a
           ; .mvi file), then hdrs are .fts type hdrs (not .mvi hdrs).
           ; So, must check to see what type of hdrs we are working with:
    	IF strlen(ahdr.date_obs) GT 12 THEN begin 
	   hdrs[i].time_obs=strmid(ahdr.date_obs,11,8)
    	   hdrs[i].date_obs=strmid(ahdr.date_obs,0,10)
           IF datatype(in_hdrs) ne 'UND' THEN begin  ;corrected time_obs in in_hdrs if hdrs keyword is set
	     in_hdrs[i].time_obs=strmid(ahdr.date_obs,11,8)
    	     in_hdrs[i].date_obs=strmid(ahdr.date_obs,0,10)
           endif
	endif
	asunc.xcen=ahdr.xcen
	asunc.ycen=ahdr.ycen
	asolr=ahdr.rsun
    	IF tag_exist(ahdr,'ROLL') THEN aroll=ahdr.roll ELSE begin 
	  aroll=ahdr.crota
    	  if issoho or ahdr.naxis1 ne file_hdr.nx then matchfitshdr2mvi,ahdr,file_hdr,ahdr,issoho=issoho
          tsunc = SCC_SUN_CENTER(ahdr,FULL=0)	
	  ; IDL coords
   	  asunc.xcen=tsunc.xcen
	  asunc.ycen=tsunc.ycen
       endelse
;    	IF mviversion LT 6 or issoho THEN BEGIN
;    	    IF KEYWORD_SET(ROLL_PER_FRAME) or (dateobs NE frame_date) THEN BEGIN
;    		; get roll only 1x per day (faster)
;		fhdr=mk_lasco_hdr(ahdr)
;	        aroll=fhdr.crota
;		asunc.xcen=fhdr.crpix1/(fhdr.naxis1/file_hdr.nx)	; account for structure defn mismatch
;		asunc.ycen=fhdr.crpix2/(fhdr.naxis2/file_hdr.ny)
;    	    	asolr = fhdr.rsun
;    	    ENDIF ELSE BEGIN
;		asolr=solr[0]
;		asunc=sunc[0]
;		aroll=rolls[0]
;	    ENDELSE
;        ENDIF

    	frame_date= dateobs
    	rolls[i]= aroll

	IF asunc.xcen GT 0 and ~keyword_set(SUNXCEN) THEN sunc[i]=  asunc
	IF asolr GT 0 THEN solr[i]=  asolr
    ENDFOR

; end per-frame header values
    IF ~keyword_set(USE_R) and (ishi) THEN BEGIN
	elong=1
	print,''
	print,'Distance from Sun is printed as Elongation in Degrees.'
    ENDIF
    IF (iseuv) or (iseit) THEN elong=2  	; display carrington lon/lat inside limb
    IF keyword_set(USE_R) THEN elong=0
    IF keyword_set(USE_E) THEN elong=1
    if file_hdr.rtheta ge 1 then elong=5
    print,''
    help,mviversion
    print,''
    IF (iscor2) THEN BEGIN
    	jk=cor2_distortion(1,'a','0',INFO=histinfo)
	print,'Utilizing ',histinfo,' to compute Rsun.'
    ENDIF
    IF keyword_set(DEBUG) and (iscor2) and keyword_set(PRECOMMCORRECT) THEN BEGIN
    	; see how attitude params change
	!p.multi=[0,1,3,0,1]
	!x.style=1
	!y.style=0
	window,4,xsiz=400,ysiz=800
	plot,float(fhdrs1.crota-rolls),psym=-2,charsi=2,ytitle='Deg', $
	    title='Change in CROTA after cor2_point from MVI'
	!y.style=3
	plot,sqrt((newcens.xcen-sunc.xcen)^2+(newcens.ycen-sunc.ycen)^2), $
	    psym=-2,ytitle='Magnitude in Pixels', charsi=2,  $
	    title='Change in suncenter after cor2_point from MVI'
	newcen2=scc_sun_center(fhdrs2)
	plot,sqrt((fhdrs2.crval1-fhdrs1.crval1)^2+(fhdrs2.crval2-fhdrs1.crval2)^2)/fhdr.cdelt1, $
	    psym=-2,ytitle='Magnitude in Pixels',charsi=2, $
	    title='Change in suncenter with DIR defined (jitter)'
	    
	!p.multi=0
    ENDIF

    IF keyword_set(DEBUG) THEN wait,4

    WINDOW, 11, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='SCC_PLAYMOVIEM label bkg'
    erase,bkgcolor  ; this is the color behind annotations on the image from the cursor position
    WINDOW, 12, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='SCC_PLAYMOVIE box high light'
    erase,255  ; this is the color behind annotations on the image from the cursor position
    
; frame header values are over-ridden by keywords
    IF KEYWORD_SET(SEC_PIX) THEN arcs[*] = sec_pix
    IF keyword_set(RECTIFIED) THEN rect=rectified
    
    IF (issoho) THEN BEGIN
        nom_crota=get_crota(dateobs)
	
    	IF ABS(rect - nom_crota) GT 10 $
    	and (abs(aroll) GT 0.1 OR aroll EQ 0) THEN BEGIN
	    ; workaround for unset rectified keyword in some LASCO daily MVI files
    	    print,''
	    print,bell
	    help,rect,nom_crota
	    IF nom_crota NE 999 THEN $
    	    message,'Mismatch between MVI hdr.rectified and get_crota.',/info
	    inp=''
	    read,'[Enter] to assume movie is roll-corrected (Solar North up), else [n]:',inp
	    IF inp EQ '' THEN BEGIN
		rect=180
		file_hdr.rectified=180
	    ENDIF
	ENDIF
    ENDIF



   IF (datatype(rolls) eq 'UND') THEN rolls=hdrs.roll 
   rolls2= rolls
  
   IF (datatype(missedfits) ne 'UND') THEN popup_msg,['Unable to locate the following FITS files:',' ',missedfits], title='SCC_PLAYMOVIEM MSG'

    

   evtpro='SCC_PLAYMOVIEM_DRAW'

   IF KEYWORD_SET(nofits) THEN nofits = 1 ELSE nofits = 0
   IF datatype(in_hdrs) ne 'UND' THEN img_hdrs = in_hdrs ELSE img_hdrs = hdrs
 ;  IF (KEYWORD_SET(osczero) NE 0) THEN osczero=osczero ELSE osczero = [[-1,-1,-1],[-1,-1,-1]]
   IF (KEYWORD_SET(osczero) NE 0) THEN osczero=osczero ELSE osczero = [0,0]

   IF (file_hdr.radius0 ne 0 or file_hdr.radius1 ne 0 or file_hdr.theta0 ne 0 or file_hdr.theta1 ne 0) and file_hdr.rtheta eq 0 THEN BEGIN
     if fix(((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx)*1000) ne fix(((file_hdr.radius1-file_hdr.radius0)/file_hdr.ny)*1000) then begin ; check for abmovie
        if ((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx) lt ((file_hdr.radius1-file_hdr.radius0)/file_hdr.ny) then begin
            file_hdr.sec_pix=((file_hdr.radius1-file_hdr.radius0)/file_hdr.ny) & osczero = [file_hdr.nx/2,0]
	endif else begin
            file_hdr.sec_pix=((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx) & osczero = [0,file_hdr.ny/2]
	endelse        
     endif else file_hdr.sec_pix=((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx)
     flatplane=1
     print,file_hdr.sec_pix
   ENDIF ELSE flatplane=0

   moviev = { $
 		current:current, $ 
 		forward:forward, $ 
 		dirsave:dirsave, $ 
 		bounce:bounce, $ 
 		first:first, $ 
 		last:last, $ 
 		pause:pause, $ 
 		dopause:dopause, $ 
 		len:len, $ 
 		hsize:hsize, $ 
 		vsize:vsize, $ 
 		win_index:win_index, $ 
 		frames:frames, $ 
 		deleted:deleted, $ 
                showmenu:showmenu, $
                showzoom:0, $
                filename:inpfilename, $
                filepath:filepath, $
                htfile:htfile, $	; Added by SHH 7/12/96
                sunc:sunc, $
                arcs:arcs, $
		rolls:rolls, $
                solr:solr, $
                flatplane:flatplane, $
                img_hdrs:img_hdrs, $
                file_hdr:file_hdr, $
 		stallsave:stallsave, $ 
 		stall:stall, $ 
 		stall0:stall0, $ 
 		nskip:0, $ 
                basexoff:[0,0], $ 
                baseyoff:[0,0], $ 
		rect:rect, $		; NRich, 03.09.23
		osczero:osczero, $		
                box:intarr(10), $
		truecolor:file_hdr.TRUECOLOR, $
                celong:-1 , $
                elong:elong , $
		evtpro:evtpro, $
      	    	win_index2:win_index2, $
      	    	hdrs2:hdrs2, $
                rolls2:rolls2, $
                debugon:debugon, $
                lowerxyval:lowerxyval, $
                lowerdnval:lowerdnval, $
                usebigfont:usebigfont, $
                systemstr:systemstr, $
                edit:edit, $
                nhtfeat:-1, $
		featwin:-1, $
		lasttime:lasttime, $
		deltaroll:0.0, $
                instxt:0, $
    	    	sliden:-1, $
    	    	cframe:-1, $
    	    	cname:-1, $
    	    	base:-1L, $
		winbase:-1L, $
		cmndbase:-1L, $
		slidenw:-1, $
		draw_win:-1, $
                boxsel:'', $
  		boxbase:-1L, $
		zoom_w:-1, $
  		zoombase:-1L, $
		zslide:-1, $
;		zstate:-1, $
    	    	mousecoords:'', $
    	    	cmbbase:-1L, $             
    	    	trackspotby:'', $
  		adjcolor:0, $
  		solead:-1, $
		sobase:-1L, $
                sync_point:fltarr(2,len)-1, $
		sync_color:-1, $
                objectv:['A','E','S','1.0','255'], $
                nofits:nofits $
}

   if datatype(wcshdrs) ne 'UND' then moviev=add_tag(moviev,wcshdrs,'wcshdrs')
   if keyword_set(fitsdir) then moviev=add_tag(moviev,fitsdir,'fitsdir')
   if datatype(in_hdrs) ne 'UND' then hdrs=in_hdrs ; so not to change input hdrs
   scc_movie_win,mvicombo=mvicombo
   scc_movie_control

   WSET, moviev.draw_win
    IF ~keyword_set(CROSSHAIR_CURSOR) THEN BEGIN
    	print,'Using arrow for cursor instead of crosshair. Position is under the edge of arrow tip.'
	device,/CURSOR_ORIGINAL   	; uses arrow pointer
    ENDIF
   XMANAGER, 'SCC_PLAYMOVIEM', moviev.cmndbase, EVENT_HANDLER='SCC_MOVIE_CONTROL_EVENT'

END
