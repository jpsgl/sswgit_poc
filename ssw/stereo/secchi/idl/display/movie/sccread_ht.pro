pro sccread_ht,filename,tai,rs,pa,comment,tel,feat,col,row,date_obs,time_obs,desc,sc, hunit, rsun,arcpix, $
    	silent=silent, DEBUG=debug,sort_off=sort_off
;
;+
; $Id: sccread_ht.pro,v 1.12 2011/08/17 15:24:24 colaninn Exp $
;
; NAME:     sccREAD_HT
;
; PURPOSE:  This procedure is used to read height-time files.
;
; CATEGORY: plot analysis movie cme 
;
; Example:
;
;	sccREAD_HT,'A_EUVI_2008-04-26.ht', time, ht, angles
;
; INPUTS:
;	Filename:	name of height-time file
;
; KEYWORDS:
;   /SILENT 	No messages
;   /DEBUG  	Extra info
;
; OUTPUTS:
;   	tai 	DBLARR	Time of measurements in seconds
;   	rs  	FLTARR	Height of measurement (elongation); units specified in header
;   	pa  	FLTARR	Position angle (latitude at solar limb) in degrees
;
; OPTIONAL OUTPUTS: 
;   	contents of file header:
;   	    comment 	STRING	user's comment if any
;   	    tel     	STRARR	telescope(s) of measurements
;   	    date_obs	STRING	date of first measurement
;   	    time_obs	STRING	time of first measurement
;   	    desc    	STRARR	Descriptions of features measured
;   	    sc	    	STRING	Spacecraft
;   	    hunit   	STRING	units of height values (rs)
;   	    rsun   	FLOAT	apparent angular extent of solar radius from observer (arcsec)
;
;   	rest of data
;   	    feat    	STRARR	IDs of feature being meaured
;   	    col, row	INTARR	pixel position of mesarements

; COMMON BLOCKS:
;	None
;
; RESTRICTIONS:
;	The file should not have any blank lines. 
;   	It is assumed all height values have same units.
;
; PROCEDURE:
;   	Reads in the height-time file, stores the height, time and position
;	angle into arrays.  Time is converted to tai.
;
; MODIFICATION HISTORY:
; $Log: sccread_ht.pro,v $
; Revision 1.12  2011/08/17 15:24:24  colaninn
; add sort_off keyword
;
; Revision 1.11  2010/12/27 16:38:38  nathan
; add /debug; define unique descfeat values
;
; Revision 1.10  2010/07/13 20:17:14  colaninn
; *** empty log message ***
;
; Revision 1.9  2010-01-14 20:56:40  nathan
; changed hrsun to rsun
;
; Revision 1.8  2009/08/05 16:18:56  mcnutt
; read to handle different formats
;
; Revision 1.7  2008/11/06 15:16:28  colaninn
; added silent keyword
;
; Revision 1.6  2008/10/23 20:07:28  nathan
; omit convert to Rsun units
;
; Revision 1.5  2008/10/23 19:44:05  nathan
; simplified hdr keyword id; convert input to Rsun if deg
;
; Revision 1.4  2008/09/15 18:56:39  nathan
; committed 9/11/08 change
;
; 080911,nr - added rsun argument
;
; Revision 1.3  2008/07/16 22:00:22  nathan
; updated doc
;
; 	Written by:	Scott H. Hawley, NRL Summer Student, June 1996
;       02 May 97       RAH, Made into a separate file
;	30 Sep 97	RAH, Mods for version 2
;	 3 Feb 98	RAH, Correct read of feature description, add array to comments
;	 3 Jul 02	NBR, Provide for case of no feature description
;
; 07/03/02 @(#)read_ht.pro	1.8 :LASCO IDL LIBRARY
;
;
;-
;
IF keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0

  maxpts=2000
  rs  = FLTARR(maxpts)
  rs0=rs
  tai = DBLARR(maxpts)
  pa  = FLTARR(maxpts)
  pa0=pa
  tel = STRARR(maxpts)
  row = FLTARR(maxpts)
  col = FLTARR(maxpts)
  feat= INTARR(maxpts)
  desc= STRARR(100)
  comment = STRARR(100)
  timestr=''
  temp=0.0
  i=0
  instr=''
  date_obs=''
  time_obs=''
  comm=''
  version=1
  hunit='R/Rsun'
  sc=''
  rsun=0.
  rsunfac=1.
  arcpix=0.
  
;
; Read in the height-time file
; Test to see if the first character is a #, which means a header value
; Convert the date/time string to TAI time
;
IF ~keyword_set(silent) THEN print,'Reading ',filename
  OPENR,lu,filename,/get_lun
  WHILE (not eof(lu)) DO BEGIN
     READF,lu,instr
     IF ~keyword_set(silent) THEN print,instr
     char0=strmid(instr,0,1)
     IF (char0 ne '#') THEN BEGIN
        i=i+1
        t=''
        f=0
        x=0
        y=0
        CASE version OF
        1:  BEGIN 
                 READS,instr,temp,timestr,p,FORMAT='(F6.2,A20,f6.0)'
                 x = temp*sin(p)
                 y = temp*cos(p)
            END
        2:  READS,instr,temp,timestr,p,t,f,x,y, $
                  FORMAT='(F6.2,A20,f6.0,a3,i3,2f8.2)'
        3:  READS,instr,temp,timestr,p,t,f,x,y, $
                  FORMAT='(F6.2,A20,f6.0,2x,a3,i4,2f8.2)'
        4: BEGIN
    	    READS,instr,temp,timestr,p,FORMAT='(F8.4,A20,f6.0)'
    	    x = temp*sin(p)
    	    y = temp*cos(p)
    	END ; version 4 handles TRACE data nrs and hw 12/05/03
        5:  BEGIN
             if strlen(instr) eq 52 then READS,instr,temp,timestr,p,t,f,x,y, $
                  FORMAT='(F8.3,A20,f6.1,a5,i3,2i5)' else begin
                 bk=strsplit(instr)
                 temp=float(strtrim(strmid(instr,bk(0),bk(1)-bk(0))))
                 timestr=strtrim(strmid(instr,bk(1),bk(2)-bk(1)))
                 p=float(strtrim(strmid(instr,bk(2),bk(3)-bk(2))))
                 t=strtrim(strmid(instr,bk(3),bk(4)-bk(3)))
                 f=fix(strtrim(strmid(instr,bk(4),bk(5)-bk(4))))
                 x=fix(strtrim(strmid(instr,bk(5),bk(6)-bk(5))))
                 y=fix(strtrim(strmid(instr,bk(6),strlen(instr)-bk(5))))
             endelse
        END
        ENDCASE
	IF debugon THEN BEGIN
	    help,bk,temp,timestr,p,t,f,x,y
	    wait,1
	ENDIF
	rs0[i]=temp
	pa0[i]=p
	rsunfac=1.
	IF 0 THEN BEGIN
	; Rsun is often not defined--so don't do this here, for now.
	;IF strpos(hunit,'DEG') GE 0 THEN BEGIN
	    message,'Converting height to Rsun',/info
	    rsunfac=rsun[0]/3600.    ; degrees/rsun
	    IF strpos(hunit,'CLN') GT 0 THEN BEGIN
	    	; ignore lat,lon - compute directly from pixel values
		rsunfac=rsun/arcpix	; units pixel/rsun
		; arcpix and hrsun MUST be defined for this to work
		IF i LE 1 THEN temp=rsunfac $
		ELSE BEGIN
		    ; compute roll angle and then position angle of vector
		    angsol= atan((p-pa0[i-1])/(temp-rs0[i-1]))
		    angpix= atan((y-row[i-1])/(x-col[i-1]))
		    rota = (angsol-angpix)*180./!pi
		    xydist=sqrt((x-col[i-1])^2 + (y-row[i-1])^2)
		    temp=rsunfac*rs[i-1]+xydist ; units pixels
		ENDELSE
	    ENDIF 
	    hunit='R/Rsun'
	ENDIF 
        rs(i)=temp/rsunfac  	; units always Rsun
        tai(i)=UTC2TAI(str2utc(timestr))
        pa(i)=p		; position angle
	; IF hunit is CLN, then compute this value from last value
        IF (t eq '') THEN tel(i)=detector ELSE tel(i)=t
        feat(i)=f
        col(i)=x
        row(i)=y
     ENDIF ELSE BEGIN
        kword=STRMID(instr,0,8)
	words=strsplit(instr)
	IF n_elements(words) GT 1 THEN val0=words[1] ELSE val0=12
	len=strlen(instr)-val0
	charval=STRMID(instr,val0,len)
	
	IF (kword eq '#VERSION') THEN version=	FIX(STRMID(instr,9,2))
        IF (kword eq '#DATE-OB') THEN date_obs= charval
        IF (kword eq '#TIME-OB') THEN time_obs= charval 
        IF (kword eq '#COMMENT') THEN BEGIN
           w = WHERE (comment EQ '')
           comment[w[0]]=charval
        ENDIF
        IF (kword eq '#DETECTO') THEN detector= strupcase(charval)
        IF (kword eq '#SPACECR') THEN sc=strupcase(charval)
        IF (kword eq '#RSUN:  ') THEN rsun=float(charval)  ; always arcsec
    	IF (kword eq '#PLATESC') THEN arcpix=float(charval) ; arcsec/pixel
        IF (kword eq '#UNITS: ') THEN hunit=strupcase(charval)
	IF (kword eq '#FEATURE' OR $
            kword eq '#FEAT_CO') THEN BEGIN
    	    	poseq = STRPOS(instr,'=')
    	    	feat_num = FIX(STRMID(instr,poseq-2,2))
    	    	descfeat = trim(STRMID(instr,poseq+1,80))
    	    	IF descfeat EQ '' THEN descfeat = trim(feat_num)
    	    	desc(feat_num) = descfeat
        ENDIF
     ENDELSE
  ENDWHILE
  FREE_LUN,lu

IF debugon THEN BEGIN
    help,version,date_obs,time_obs,detector,sc,rsun,arcpix,hunit,desc,descfeat
    wait,2
ENDIF
;
;   Truncate the array to include only valid points
;
IF (i GT 0)  THEN BEGIN
  tai= tai(1:i)
  rs = rs(1:i)
  pa = pa(1:i)
  feat = feat(1:i)
  col = col(1:i)
  row = row(1:i)
  tel = tel(1:i)
;
;   Then sort by time
;
  IF ~keyword_set(sort_off) THEN BEGIN
    ind = SORT (tai)
    rs=rs(ind)
    tai=tai(ind)
    pa = pa(ind)
    feat = feat(ind)
    col = col(ind)
    row = row(ind)
    tel = tel(ind)
  ENDIF
ENDIF


IF ~keyword_set(silent) THEN help,hunit,detector,sc,rsun,arcpix
RETURN
END
