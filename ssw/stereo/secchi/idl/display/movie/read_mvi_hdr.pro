;+
PRO read_mvi_hdr, filename, file_hdr, frame_hdrs, DISPLAY=display
;
;$Id: read_mvi_hdr.pro,v 1.1 2011/09/09 21:52:03 nathan Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Purpose     : Put contents of header of MVI file into variables
;
; INput:
;   filename	Name of MVI file
;               
; OUTPUT:
;   file_hdr	MVI file header structure
;   frame_hdrs	array of MVI frame header structures
;
; Keywords:
;   /DISPLAY	Print header structure to display
;
; Explanation : 
;
; Calls       : sccread_mvi, sccmvihdr2struct
;
; Category    : movie mvi diagnostic utility
;               
; Written     : N.Rich, NRL, Sep.2011
;-               
;$Log: read_mvi_hdr.pro,v $
;Revision 1.1  2011/09/09 21:52:03  nathan
;based on printmvihdr.pro
;

OPENR,lu,filename,/GET_LUN
SCCREAD_MVI, lu, file_hdr, ihdrs, imgsptr, swapflag

IF keyword_set(FRAME) THEN inx=frame ELSE inx=0

nf=file_hdr.nf
frame_hdrs = SCCMVIHDR2STRUCT(ihdrs[0],file_hdr.ver)
FOR i=1,nf-1 DO frame_hdrs=[frame_hdrs,SCCMVIHDR2STRUCT(ihdrs[i],file_hdr.ver)]

IF keyword_set(DISPLAY) THEN BEGIN
    Print,filename
    help,/str,file_hdr
    print,file_hdr.ccd_pos
    print,'Frame ',trim(inx),':'
    help,/str,ahdr
ENDIF
free_lun,lu

END
