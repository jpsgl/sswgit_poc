;+
;$Id: scc_annotate.pro,v 1.6 2017/01/03 14:34:22 mcnutt Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_annotate
;               
; Purpose     : Widget tool to annotate a movie.
;               
; Explanation : This tool allows the user to add text to movie frames
;               
; Use         : called from scc_wrunmoviem requires common blocks WSCC_MKMOVIE_COMMON and sWIN_INDEXM defined in scc_wrunmoviem
;
;
; Comments    : Called from scc_wrunmoviem
;               
; Side effects: 
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL Feb. 27 2009.
;               
; $Log: scc_annotate.pro,v $
; Revision 1.6  2017/01/03 14:34:22  mcnutt
; Use select_windows instead of set_plot
;
; Revision 1.5  2009/04/29 18:57:41  mcnutt
; rearanged buttoms
;
; Revision 1.4  2009/04/29 13:43:13  mcnutt
; allows only Hershey vector or true type fonts use embedded formatting
;
; Revision 1.3  2009/04/27 18:39:27  mcnutt
; added arrow thick and now controls arrow head size
;
; Revision 1.2  2009/03/05 12:34:40  mcnutt
; added arrow feature
;
; Revision 1.1  2009/02/27 15:41:03  mcnutt
; Anotates a movie works with scc_wrunmoviem
;



PRO scc_annotate_event, ev
COMMON WSCC_MKMOVIE_COMMON
COMMON sWIN_INDEXM
   WIDGET_CONTROL, base, GET_UVALUE=moviev  ; get structure from UVALUE
   WIDGET_CONTROL, textbase, GET_UVALUE=tstate   ; get structure from UVALUE
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test

   if moviev.file_hdr.truecolor eq 1 then device,decompose=1

    CASE (input) OF

       'MVitxt'   : tstate.txt=ev.value 
       'MVtxtsz'  : tstate.sze=ev.value 
       'MVtxtfnt' : tstate.fnt=tstate.fntval(ev.value)
       'MVtxtf1'  : tstate.f1=ev.value
       'MVtxtf2'  : BEGIN
		      if ev.value le moviev.len-1 then tstate.f2=ev.value else begin 
		        tstate.f2=moviev.len-1
                        widget_control,ev.id,set_value=moviev.len-1
                      endelse
		    END
       'MVtxtclr' :  tstate.clr=tstate.clrval(ev.value)
       'MVtxtbk'  :  if moviev.file_hdr.truecolor eq 1 then tstate.bkgrd=reform(tstate.bkgval(*,ev.value)) else tstate.bkgrd=tstate.bkgval(ev.value)
       'MVtxtx'   :  tstate.xp=ev.value
       'MVtxty'   :  tstate.yp=ev.value
       'MVtxtalg'   : tstate.alg=tstate.algval(ev.value)
       'MVtxtorn'   : tstate.orn=ev.value
       'txtsel' : moviev.annotate=4


       'MVarrx1' : tstate.arxy(0)=ev.value
       'MVarry1' : tstate.arxy(1)=ev.value
       'MVarrx2' : tstate.arxy(2)=ev.value
       'MVarry2' : tstate.arxy(3)=ev.value
       'Arrsel1' : moviev.annotate=2
       'Arrsel2' : moviev.annotate=3
       'MVtxtarrclr' :  tstate.arrclr=tstate.clrval(ev.value)
       'MVtxtarrsz' :  tstate.arxy(5)=ev.value
       'MVtxtarrth' :  tstate.arxy(4)=ev.value 

   	'Pannotate' : BEGIN	;add text to current window
                    WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                     moviev.annotate=1
                    xadj=strlen(tstate.txt)*(tstate.sze*6)
                    xr=[tstate.xp-tstate.alg*xadj,tstate.xp+xadj-tstate.alg*xadj]
                    yr=[tstate.yp-tstate.sze*2,tstate.yp+tstate.sze*8]
                 
                    xm=where(xr gt moviev.hsize) & if xm(0) gt -1 then xr(xm)=moviev.hsize-1
                    xm=where(xr lt 0) & if xm(0) gt -1 then xr(xm)=0
                    xm=where(yr gt moviev.vsize) & if xm(0) gt -1 then yr(xm)=moviev.vsize-1
                    xm=where(yr lt 0) & if xm(0) gt -1 then yr(xm)=0
                    
                    for apn=tstate.f1 ,tstate.f2 do begin
			if moviev.file_hdr.truecolor eq 0 then begin
                          set_plot,'z'
                          device,set_resolution=[moviev.hsize,moviev.vsize]
			   timg=imgs(*,*,apn)
                           if tstate.bkgrd ne -1 then timg(xr(0):xr(1),yr(0):yr(1))=fix(tstate.bkgrd)
		        endif else begin
                           select_windows ;set_plot,'x'
                           wset,moviev.win_index(apn)
			   timg=imgs(*,*,*,apn)
                           if tstate.bkgrd(0) ne -1 then for nc = 0,2 do timg(nc,xr(0):xr(1),yr(0):yr(1))=tstate.bkgrd(nc)
			endelse
                        TV,timg,true=moviev.file_hdr.truecolor
			stop
                         if strpos(tstate.txt,'Frame#') gt -1 then tstxt='Frame '+string(apn,'(i3.3)') else tstxt=tstate.txt
		        xyouts,tstate.xp,tstate.yp,tstxt+'!X',charsize=tstate.sze,font=tstate.fnt,color=tstate.clr,ALIGNMENT=tstate.alg,orientation=tstate.orn,/device
                        timg=tvrd (true=moviev.file_hdr.truecolor)
                        select_windows ;set_plot,'x'
                        wset,moviev.win_index(apn)
 	                tv, timg,true=moviev.file_hdr.truecolor
                        if moviev.stall eq 10000 and apn eq moviev.current then begin
                           WSET, moviev.draw_win
                           DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                           wset,moviev.win_index(moviev.current)
			endif
                     endfor
                  WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Put timer on hold
                 END

   	'Aannotate' : BEGIN	;add text to movie frames and imgs
                     moviev.annotate=1
                    xadj=strlen(tstate.txt)*(tstate.sze*6)
                    xr=[tstate.xp-tstate.alg*xadj,tstate.xp+xadj-tstate.alg*xadj]
                    yr=[tstate.yp-tstate.sze*2,tstate.yp+tstate.sze*8]
                    xm=where(xr gt moviev.hsize) & if xm(0) gt -1 then xr(xm)=moviev.hsize-1
                    xm=where(xr lt 0) & if xm(0) gt -1 then xr(xm)=0
                    xm=where(yr gt moviev.vsize) & if xm(0) gt -1 then yr(xm)=moviev.vsize-1
                    xm=where(yr lt 0) & if xm(0) gt -1 then yr(xm)=0
                 
                    for apn=tstate.f1 ,tstate.f2 do begin
			if moviev.file_hdr.truecolor eq 0 then begin
                           set_plot,'z'
                           device,set_resolution=[moviev.hsize,moviev.vsize]
			   timg=imgs(*,*,apn)
                           if tstate.bkgrd ne -1 then timg(xr(0):xr(1),yr(0):yr(1))=fix(tstate.bkgrd)
		        endif else begin
                           select_windows ;set_plot,'x'
                           wset,moviev.win_index(apn)
			   timg=imgs(*,*,*,apn)
                           if tstate.bkgrd(0) ne -1 then for nc = 0,2 do timg(nc,xr(0):xr(1),yr(0):yr(1))=tstate.bkgrd(nc)
			endelse
                        TV,timg,true=moviev.file_hdr.truecolor
                        TV,timg,true=moviev.file_hdr.truecolor
                         if strpos(tstate.txt,'Frame#') gt -1 then tstxt='Frame '+string(apn,'(i3.3)') else tstxt=tstate.txt
		        xyouts,tstate.xp,tstate.yp,tstxt+'!X',charsize=tstate.sze,font=tstate.fnt,color=tstate.clr,ALIGNMENT=tstate.alg,orientation=tstate.orn,/device
                       timg=tvrd (true=moviev.file_hdr.truecolor)
                        if moviev.file_hdr.truecolor eq 0 then imgs(*,*,apn)=timg else imgs(*,*,*,apn)=timg
                        select_windows ;set_plot,'x'
                        wset,moviev.win_index(apn)
 	                tv, timg,true=moviev.file_hdr.truecolor
                        if moviev.stall eq 10000 and apn eq moviev.current then begin
                           WSET, moviev.draw_win
                           DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                           wset,moviev.win_index(moviev.current)
			endif
                     endfor
                     wset,moviev.win_index(moviev.current)
                 END


   	'Parrow' : BEGIN	;add text to current window
                     moviev.annotate=1
                    WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                    for apn=tstate.f1 ,tstate.f2 do begin
			if moviev.file_hdr.truecolor eq 0 then begin
                           set_plot,'z'
                           device,set_resolution=[moviev.hsize,moviev.vsize]
			   timg=imgs(*,*,apn)
		        endif else begin
                           select_windows ;set_plot,'x'
                           wset,moviev.win_index(apn)
			   timg=imgs(*,*,*,apn)
 			endelse
                        TV,timg,true=moviev.file_hdr.truecolor
		        ARROW,tstate.arxy(0),tstate.arxy(1),tstate.arxy(2),tstate.arxy(3),color=tstate.arrclr,hsize=tstate.arxy(5),thick=tstate.arxy(4),/solid
                        timg=tvrd (true=moviev.file_hdr.truecolor)
                        select_windows ;set_plot,'x'
                        wset,moviev.win_index(apn)
 	                tv, timg,true=moviev.file_hdr.truecolor
                        if moviev.stall eq 10000 and apn eq moviev.current then begin
                           WSET, moviev.draw_win
                           DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                           wset,moviev.win_index(moviev.current)
			endif
                     endfor
                    WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Put timer on hold
                 END

   	'Arrow' : BEGIN	;add text to movie frames and imgs
                     moviev.annotate=1

                    for apn=tstate.f1 ,tstate.f2 do begin
			if moviev.file_hdr.truecolor eq 0 then begin
                           set_plot,'z'
                           device,set_resolution=[moviev.hsize,moviev.vsize]
			   timg=imgs(*,*,apn)
		        endif else begin
                           select_windows ;set_plot,'x'
                           wset,moviev.win_index(apn)
			   timg=imgs(*,*,*,apn)
			endelse
                        TV,timg,true=moviev.file_hdr.truecolor
		        ARROW,tstate.arxy(0),tstate.arxy(1),tstate.arxy(2),tstate.arxy(3),color=tstate.arrclr,hsize=tstate.arxy(5),thick=tstate.arxy(4),/solid
                        timg=tvrd (true=moviev.file_hdr.truecolor)
                        if moviev.file_hdr.truecolor eq 0 then imgs(*,*,apn)=timg else imgs(*,*,*,apn)=timg
                        select_windows ;set_plot,'x'
                        wset,moviev.win_index(apn)
 	                tv, timg,true=moviev.file_hdr.truecolor
                        if moviev.stall eq 10000 and apn eq moviev.current then begin
                           WSET, moviev.draw_win
                           DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                           wset,moviev.win_index(moviev.current)
			endif
                     endfor
                     wset,moviev.win_index(moviev.current)
                 END



   	'DONE' : BEGIN	;add text to movie frames and imgs
	             WIDGET_CONTROL, textbase , /DESTROY   ; DONE
                     moviev.annotate=0
                    for apn=0 ,moviev.len-1 do begin
                        if moviev.file_hdr.truecolor eq 0 then timg=imgs(*,*,apn) else timg=imgs(*,*,*,apn)
                        select_windows ;set_plot,'x'
                        wset,moviev.win_index(apn)
 	                tv, timg,true=moviev.file_hdr.truecolor
                     endfor
                     wset,moviev.win_index(moviev.current)
                 END
 


	ELSE : BEGIN
                 PRINT, '%%scc_annotate.  Unknown event.'
	     END

    ENDCASE

    if input ne 'DONE' then WIDGET_CONTROL, textbase, SET_UVALUE=tstate
    WIDGET_CONTROL, base, SET_UVALUE=moviev


END


PRO scc_annotate
COMMON WSCC_MKMOVIE_COMMON
COMMON sWIN_INDEXM
device,get_screen_size=ss
WIDGET_CONTROL, base, GET_UVALUE=moviev  ; get structure from UVALUE

tmp=size(imgs)
                txt=		''
                sze=          1.0
                fnt=          1
                f1=          0
                f2=          moviev.len-1
                nx=          tmp(tmp(0)-2)
                ny=          tmp(tmp(0)-1)
                xp=          0
                yp=          0
                orn=          0 


   textbase = WIDGET_BASE(/COLUMN,  TITLE='SCC_annotate',XOFFSET=ss(0)-200, YOFFSET=0);,group_leader=groupleader,/modal
   ctext1 = WIDGET_BASE(textbase,/COLUMN,/frame)
     text1 = WIDGET_BASE(ctext1,/ROW)
       mvtxt= CW_FIELD(text1,  /ALL_EVENTS, value=txt, UVALUE='MVitxt' , XSIZE=70, YSIZE=1, title='TEXT : ')

   text1a = WIDGET_BASE(ctext1,/ROW)
       mvtxt= CW_FIELD(text1a,  /ALL_EVENTS, value=sze , UVALUE='MVtxtsz' , XSIZE=4, YSIZE=1, title='Char Size : ')
       mvtxtc =   WIDGET_LABEL(text1a, VALUE='Position: ')
       mvtxtxp= CW_FIELD(text1a,  /ALL_EVENTS, Value=xp , UVALUE='MVtxtx' , XSIZE=4, YSIZE=1, title='X:')
       mvtxtyp= CW_FIELD(text1a,  /ALL_EVENTS, Value=yp, UVALUE='MVtxty' , XSIZE=4, YSIZE=1, title='Y:')
       tmp=  CW_BGROUP(text1a,['Select'],BUTTON_UVALUE = ['txtsel'],/ROW)


 
    text2 = WIDGET_BASE(ctext1,/ROW)
       if moviev.file_hdr.truecolor eq 0 then begin
          clrarr=['White','Median','Black']
          clrval=[255,127,0]
          clr=clrval(0)
          bkgarr=['none','White','Median','Black']
          bkgval=[-1,255,127,0]
          bkgrd=bkgval(0)
          mvtxtc =   WIDGET_LABEL(text2, VALUE='Color: ')
          mvtxtc =   CW_BSELECTOR2(text2, clrarr, SET_VALUE=0, UVALUE='MVtxtclr')
          mvtxtc =   WIDGET_LABEL(text2, VALUE='Bkgnd: ')
          mvtxtc =   CW_BSELECTOR2(text2, bkgarr, SET_VALUE=0, UVALUE='MVtxtbk')
       endif else begin
          clrarr=['White',  'Red',    'Blue',   'Green',  'yellow', 'Black']
          clrval=['ffffff'x,'0000ff'x,'ff0000'x,'00ff00'x,'38e5e5'x,'000000'x]
          clr=clrval(0)
          bkgarr=['none',    'White',      'Red',        'Blue',       'Green',     'yellow',       'Black']
          bkgval=[[-1,-1,-1],[255,255,255],[255,000,000],[000,000,255],[000,255,000],[229,229,056],[000,000,000]]
          bkgrd=reform(bkgval(*,0))
          mvtxtc =   WIDGET_LABEL(text2, VALUE='Color: ')
          mvtxtc =   CW_BSELECTOR2(text2, clrarr, SET_VALUE=0, UVALUE='MVtxtclr')
          mvtxtc =   WIDGET_LABEL(text2, VALUE='Bkgnd: ')
          mvtxtc =   CW_BSELECTOR2(text2, bkgarr, SET_VALUE=0, UVALUE='MVtxtbk')
       endelse
          fntarr=['Hershey Vector','True Type'];,'Hardware']
          fntval=[-1,1,0]
          fnt=fntval(0)
       mvtxtc =   WIDGET_LABEL(text2, VALUE='Font: ')
       mvtxtc =   CW_BSELECTOR2(text2, fntarr, SET_VALUE=0, UVALUE='MVtxtfnt')
          algarr=['Left','Center','Right']
          algval=[0.0,0.5,1.0]
          alg=algval(0)
       mvtxtc =   WIDGET_LABEL(text2, VALUE='Align: ')
       mvtxtc =   CW_BSELECTOR2(text2, algarr, SET_VALUE=0, UVALUE='MVtxtalg')
;       mvtxt= CW_FIELD(text2,  /ALL_EVENTS, value=orn , UVALUE='MVtxtorn' , XSIZE=3, YSIZE=1, title='Orientation : ')

     text3 = WIDGET_BASE(ctext1,/ROW)
       tmp=  CW_BGROUP(text3,['Preview','Add text'],BUTTON_UVALUE = ['Pannotate','Aannotate'],/ROW,/frame,xoffset=20)


arxy=[0,0,0,0,1,fix(moviev.hsize/64.0)]
arrclr=clrval(0)
   ctext5 = WIDGET_BASE(textbase,/colum,/frame)
   text5 = WIDGET_BASE(ctext5,/row)
          mvtxtc =   WIDGET_LABEL(text5, VALUE='ARROW : ')
          mvarrx1= CW_FIELD(text5,  /ALL_EVENTS, Value=arxy(0) , UVALUE='MVarrx1' , XSIZE=4, YSIZE=1, title='X1:')
          mvarry1= CW_FIELD(text5,  /ALL_EVENTS, Value=arxy(1) , UVALUE='MVarry1' , XSIZE=4, YSIZE=1, title='Y1:')
       tmp=  CW_BGROUP(text5,['Select'],BUTTON_UVALUE = ['Arrsel1'],/ROW)

          mvarrx2= CW_FIELD(text5,  /ALL_EVENTS, Value=arxy(2) , UVALUE='MVarrx2' , XSIZE=4, YSIZE=1, title='X2:')
          mvarry2= CW_FIELD(text5,  /ALL_EVENTS, Value=arxy(3) , UVALUE='MVarry2' , XSIZE=4, YSIZE=1, title='Y2:')
       tmp=  CW_BGROUP(text5,['Select'],BUTTON_UVALUE = ['Arrsel2'],/ROW)

   text6 = WIDGET_BASE(ctext5,/row)
          mvtxtc =   WIDGET_LABEL(text6, VALUE='Color:')
          mvtxtc =   CW_BSELECTOR2(text6, clrarr, SET_VALUE=0, UVALUE='MVtxtarrclr')
          mvtxt= CW_FIELD(text6,  /ALL_EVENTS, value=arxy(4) , UVALUE='MVtxtarrth' , XSIZE=4, YSIZE=1, title='Thickness :')
          mvtxt= CW_FIELD(text6,  /ALL_EVENTS, value=arxy(5) , UVALUE='MVtxtarrsz' , XSIZE=4, YSIZE=1, title='Size:')

   text6a = WIDGET_BASE(ctext5,/row)
       tmp=  CW_BGROUP(text6a,['Preview','Add Arrow'],BUTTON_UVALUE = ['Parrow','Arrow'],/ROW,/frame)

   ctext6 = WIDGET_BASE(textbase,/colum,/frame)
     text7 = WIDGET_BASE(ctext6,/row)
       mvtxtc =   WIDGET_LABEL(text7, VALUE="Select Frame #'s :")
     text7a = WIDGET_BASE(ctext6,/row)
       mvtxt= CW_FIELD(text7a,  /ALL_EVENTS, Value=f1 , UVALUE='MVtxtf1' , XSIZE=3, YSIZE=1, title='First Frame : ')
       mvtxt= CW_FIELD(text7a,  /ALL_EVENTS, Value=f2, UVALUE='MVtxtf2' , XSIZE=3, YSIZE=1, title='Last Frame : ')

   ctext7 = WIDGET_BASE(textbase,/colum,/frame)
       tmp=  CW_BGROUP(ctext7,['Close'],BUTTON_UVALUE = ['DONE'],/ROW)



   WIDGET_CONTROL, textbase, MAP=1
   WIDGET_CONTROL, /REAL, textbase

tstate={        txt:txt, $
                sze:sze, $
                f1:f1, $
                f2:f2, $
                nx:nx, $
                ny:ny, $
                xp:xp, $
                yp:yp, $
                clr:clr, $
                bkgrd:bkgrd, $
                clrval:clrval, $
                bkgval:bkgval, $
                algval:algval, $
                alg:alg, $
                fntval:fntval, $
                fnt:fnt, $
                orn:orn, $
               mvtxtxp:mvtxtxp, $
               mvtxtyp:mvtxtyp, $ 
               mvarrx1:mvarrx1, $
               mvarry1:mvarry1, $
               mvarrx2:mvarrx2, $
               mvarry2:mvarry2, $
               arrclr:arrclr, $
               arxy:arxy $
 
            }
       
 

   WIDGET_CONTROL, textbase, SET_UVALUE=tstate
   XMANAGER, 'SCC_annotate', textbase, EVENT_HANDLER='scc_annotate_event'

end
