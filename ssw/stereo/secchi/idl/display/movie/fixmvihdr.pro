;$Id: fixmvihdr.pro,v 1.9 2013/01/17 21:24:49 nathan Exp $
;
; Project     : SECCHI
;                   
; Name        : fixmvihdr
;               
; Purpose     : make corrections to xcen and ycen in MVI header 
;               
; Explanation : Recomputes xcen,ycen based on filenames, nx,ny,sec_pix and
;   	    	replaces sunxcen, sunycen, xcen, ycen. 
;
; Use         : fixmvihdr,'name.mvi'
;
; Inputs:   	mviname    Name of MVI file to fix.
;
; Optional Inputs:
;   	
;
; KEYWORDS:  
;   OUTMVI= 	output movie name; if not will save in mviname+'f.mvi'.
;   /OVERWRITE	If set, fixed MVI will replace input MVI name	
;   /NOHIFIXPOINTING	Do not apply hi_fix_pointing.pro
;
; Calls       : hi_fix_pointing.pro
;
; Side effects: 
;               
; Category    : mvi movie coordinates
;               
; Written     : Lynn Simpson, 
;               
;	
;-            
;$Log: fixmvihdr.pro,v $
;Revision 1.9  2013/01/17 21:24:49  nathan
;fix case where FITS file is not available and fix_hi_pointing
;
;Revision 1.8  2012/11/20 17:32:39  nathan
;implement /NOHIFIXPOINTING
;
;Revision 1.7  2012/11/19 18:58:38  mcnutt
;always use ahdr crpix and sec_pix values
;
;Revision 1.6  2012/11/16 22:26:11  nathan
;do hi_fix_pointing for each frame
;
;Revision 1.5  2012/11/16 21:54:04  nathan
;wait before mv
;
;Revision 1.4  2012/11/16 21:49:49  nathan
;require /OVERWRITE to overwrite, and so with shell mv
;
;Revision 1.3  2012/11/16 21:00:28  nathan
;add free_lun,lu1
;
;Revision 1.2  2012/09/18 18:19:47  mcnutt
;added program
;
;Revision 1.1  2012/09/13 22:39:06  nathan
;stub
;

pro fixmvihdr,mviname, OUTMVI=outmvi, OVERWRITE=overwrite, NOhifixPOINTING=nohifixpointing
  if keyword_set(outmvi) then outmvi=outmvi ELSE outmvi=mviname
  IF outmvi EQ mviname THEN BEGIN
    break_file,mviname,di,pa,ro,sf
    outmvi=pa+ro+'f'+sf
  ENDIF
  OPENR, lu1, mviname,/get_lun
  rgbvec=bytarr(!d.n_colors,3)
  SCCREAD_MVI, lu1, file_hdr, ihdrs, imgs, swapflag, rgbvec=rgbvec, pngmvi=pngmvi
   FOR i=0, file_hdr.nf-1 DO BEGIN
      if strpos(mviname,'.hdr') gt -1 then imgt=get_hdr_movie_img(string(imgs[i]),mviname=mviname,truecolor=file_hdr.truecolor) else imgt= imgs[i]     
      ahdrt = SCCMVIHDR2STRUCT(ihdrs(i),file_hdr.ver)
      ahdr = convert2secchi(ahdrt,file_hdr, debug=debug_on)
      if strlen(ahdr.time_obs) lt 5 then begin
         ahdr.time_obs=strmid(ahdr.date_obs,11,8)
         ahdr.date_obs=strmid(ahdr.date_obs,0,10)
      endif

;      if file_hdr.sunxcen le 0 then file_hdr.sunxcen=ahdr.crpix1
;      if file_hdr.sunycen le 0 then file_hdr.sunycen=ahdr.crpix2
;      if file_hdr.sec_pix le 0 and file_hdr.rtheta eq 0 then file_hdr.sec_pix=ahdr.CDELT1  ;correct hi2 sec_pix not saved correctly in sccwrite_disk_movie
;always use ahdr values
      file_hdr.sunxcen=ahdr.crpix1
      file_hdr.sunycen=ahdr.crpix2
      file_hdr.sec_pix=ahdr.CDELT1  ;correct hi2 sec_pix not saved correctly in sccwrite_disk_movie

      filen=ahdr.filename
      fname=sccfindfits(filen)
      IF fname EQ '' THEN BEGIN
	 strput,filen,'?',16
	 fname = sccfindfits(filen)
      ENDIF
      if fname NE  '' then BEGIN
      	jk=sccreadfits(fname,fhdr,/nodata) 
      	matchfitshdr2mvi,fhdr,file_hdr,ahdr;,notsecchi=notsecchi
      ENDIF else $
      	fhdr=ahdr   ;mk_short_scc_hdr(ahdr,file_hdr1)
      IF (ahdr.detector eq 'HI2' or ahdr.detector eq 'HI1') and file_hdr.rtheta eq 0 and ~keyword_set(NOHIFIXPOINTING) THEN BEGIN
        message,'Assuming FFV.',/info
	fhdr.crpix1=(fhdr.naxis1/2)+0.5
	fhdr.crpix2=(fhdr.naxis2/2)+0.5
	fhdr.crpix1a=(fhdr.naxis1/2)+0.5
	fhdr.crpix2a=(fhdr.naxis1/2)+0.5
	hi_fix_pointing,fhdr
      ENDIF
      fcen=scc_sun_center(fhdr)
      print,fcen
      
      IF (i EQ 0) THEN BEGIN                        
        file_hdr.sunxcen=fcen.xcen
        file_hdr.sunycen=fcen.ycen
      ENDIF
      ahdr.xcen=fcen.xcen
      ahdr.ycen=fcen.ycen
      IF (i EQ 0) THEN first=1 else first=0
      scc_save_mvframe,i,outmvi,imgt,ahdr,file_hdr,first=first,inline=file_hdr.nf
   ENDFOR
  free_lun,lu1
  IF keyword_set(OVERWRITE) THEN BEGIN
    cmd='mv '+outmvi+' '+mviname
    print,cmd
    wait,1
    spawn,cmd,/sh
  ENDIF
END
