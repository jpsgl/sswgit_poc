;+
;
;$Id: scc_playmovie.pro,v 1.46 2015/04/01 19:03:03 hutting Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_PLAYMOVIE
;               
; Purpose     : Widget tool to display animation sequence.
;               
; Explanation : This tool allows the user to view a series of images as
;		an animation sequence.  The user can control the direction,
;		speed, and number of frames with widget controls.
;               
; Use         : IDL> SCC_PLAYMOVIE [, arg1 [, NAMES=names [,SKIP=skip]]]
;
;		Use the VIDEO keyword to automatically prepare video-ready format (640x480);
;		use with IMG_REBIN if you want to keep full x-field
;	  	IDL> SCC_PLAYMOVIE, /VIDEO [,/IMG_REBIN]
;
;		Without any inputs, program will prompt user to select an existing .mvi file.
;    Example  : IDL> SCC_PLAYMOVIE
;
;               Or you could have one argument, the .mvi file you want to load.
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi'
;
;               Or if you have pre-loaded images into pixmaps (like MKMOVIE.PRO does) call:
;    Example  : IDL> SCC_PLAYMOVIE, win_index, NAMES=names
;		Where win_index is an array of the window numbers and names is optionally
;		a STRARR() containing names of each frame.
;;    
;		Use the keyword SKIP to skip every n frames (good for large movies).
;    Example  : IDL> SCC_PLAYMOVIE, SKIP=1		;* to skip every other frame
;
;		Use the keyword START to start reading movie at frame n, where first frame is n=0 (good for large movies).
;    Example  : IDL> SCC_PLAYMOVIE, START=100		;* frame 100 becomes 1st frame of movie
;
;		Use the keyword LENGTH to specify number of frames to read in (good for large movies).
;    Example  : IDL> SCC_PLAYMOVIE, START=100, LENGTH=60	;* to load frames 100-159
;
;		Use the keyword TIMES to display detector & date & time on frames (if not already there).
;    Example  : IDL> SCC_PLAYMOVIE, /TIMES
;
;		Use the keyword NOCAM with TIMES keyword to omit the detector from TIMES display.
;    Example  : IDL> SCC_PLAYMOVIE, /TIMES, /NOCAM
;
;		Use the keyword COORDS to display subframe of movie images. [x1,x2,y1,y2]
;		Note: COORDS is applied before IMG_REBIN if both are selected.
;		OR just use /COORDS to select coordinates interactively
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', COORDS=[256,256+511,175,175+255]
;
;		Use the keyword IMG_REBIN to resize movie.  If shrinking by integer factor REBIN 
;		is used otherwise CONGRID with linear interpolation is used.
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', IMG_REBIN=[512,512]
;
;		Use the keyword SAVE to just save the movie and exit (for use in batch mode).
;    Example  : IDL> SCC_PLAYMOVIE, win_index, SAVE='mymovie.mvi'
;
;		Use the keyword DIFF to subtract a base frame from all frames in the movie.
;		The base frame is the first frame.  Or you can use the keyword START to set it.
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', /DIFF
;
;		Use the keyword RUNNING_DIFF to create a running difference movie.
;		The default is to subtract the previous frame.  Use RUNNING_DIFF=2 to subtract
;		2 frames prior from each image.
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', /RUNNING_DIFF
;
;		Use the keyword /COSMIC to removie cosmic rays.
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', /COSMIC
;
;		Use the keyword /FIXGAPS to replace data gaps with data from previous frame
;		The default is to assume missing blocks are 32x32, 
;		For 1/2 resolution images use BLOCK_SIZE=16 for example
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', /FIXGAPS
;
;		Use the keyword /DRAW_LIMB to draw a circle at the solar limb
;    Example  : IDL> SCC_PLAYMOVIE, 'mymovie.mvi', /DRAW_LIMB
;
; OTHER KEYWORDS:
;   	/LAST	Use currently loaded pixmaps (from previously loaded movie)
;   	MARGIN= Force display window to allow at least this many pixels free on side and bottom
;	LOAD	Set to use saved keyword values, if any
;	KEEP	Set to not delete pixmaps 
;	CHSZ	Set to desired size of time label (default=1.5)
;	SPOKE	Display spoked images *** NEEDS WORK ***
;	DIF_MIN, DIF_MAX	Set to desired range for scaling difference images; default is +/-70
;	RECTIFIED	Set to number of degrees images rotated to put solar north up (affects header only)
;	/DORECTIFY	Rotate frames 180 degrees
;	/CENRECTIFY	Compute new center for 180-deg-rotation
;	/TRUECOLOR	Set to treat images as true color images
;   	/NOXCOLORS   	Set to disable ability to change color tables, which saves memory
;   	SUNXCEN=, SUNYCEN=  Override sun center in header if any (IDL coords)
;   /nofits  	use mk_short_scc_hdr and do not look for SECCHI fits files
;
;
; Calls       : 
;
; Comments    : Use MKMOVIE.PRO to load into pixmaps.
;               
; Side effects: None.
;               
; Category    : Image Display.  Animation.
;               
; Written     : Scott Paswaters, NRL Feb. 13 1996.
;               
; Modified    : SEP  29 May 1996 - Changed to multiple pixmaps for images.
;				   Added buttons to save and load movie files (.mvi).
;				   Seperated control buttons from display window.
;               SEP   9 Jul 1996 - Added keyword to pass image headers.
;               SEP   7 Jan 1997 - Added skip keyword.
;               SEP   9 Jan 1997 - Added START, LENGTH keywords.
;               SEP  05 Feb 1997 - Mods for mvi version 1 format.
;               SEP  18 Apr 1997 - Added .mpg output option with 1/2 resolution.
;               SEP  16 May 1997 - Added save option and IMG_REBIN option.
;               SEP  19 May 1997 - Added COORDS option.
;               SEP  27 Jun 1997 - Added permission checks for output.
;               SEP  22 Sep 1997 - Added current frame scrolling widget.
;               SEP  02 Oct 1997 - Added ability to interactively select subimage coords.
;               SEP  18 Nov 1997 - Added /COSMIC  /FIXGAPS and /DRAW_LIMB keywords.
;               SEP  11 Dec 1997 - Added button to call SCC_PLAYMOVIEM, only save frames first->last
;               SEP  08 May 1998 - Added BLOCK_SIZE keyword
;               SP   02 Mar 1999 - Added Scroll bars for large images
;		NBR  26 Mar 1999 - Added LOGO keyword; Add Save-movie-as-GIFS button
;		NBR  09 Apr 1999 - Use short_names for frame names
;		NBR     Jul 1999 - Add VIDEO keyword; Add detector to TIMES label
;		NBR     Aug 1999 - Add detector to GIF names
;		NBR     Sep 1999 - Add NOCAM keyword
;               ???  06 FEB 2000 - Add bytscl range for DIFF and RUNNING_DIFF images (DIF_MIN/MAX keywords)
;               JIE  14 JUN 2000 - ADD keyword CHSZ to adjust the size of displayed time
;               JIE   2 MAR 2000 - ADD keyword SPOKE to display spoked images
;	NBR	 3 Oct 2000 - Save gif files in current directory by default
;	RAH	18 Oct 2000 - Added option to not rescale a postscript image. Default was to rescale
;	NBR	15 Dec 2000 - Allow setting of TIMES keyword to color desired
;	NBR	 3 Jan 2001 - Put win_index in common block, add KEEP keyword
;	NBR	 4 Jan 2001 - Remove win_index from common block
;	NBR	10 Apr 2001 - Change output gif filenames and reconcile diverging versions of this procedure
;	NBR	25 Apr 2002 - Change default movie speed
;	nbr	 3 sep 2002 - allow user input of root name for saving movie as gifs
;	nbr	24 sep 2003 - Add RECTIFIED keyword; add rect to moviev and saving mvis
;	nbr	26 Sep 2003 - Add /DORECTIFY, /CENRECTIFY
;	nbr 	29 Sep 2003 - Add RECTIFIED to SCC_PLAYMOVIEm call
;	nbr	 1 Oct 2003 - Fix rect=0
;	nbr 	 6 Feb 2004 - Save some keywords via common block
;				- IF 24-bit display, loadct,0 after loading mvi
;				- use ftvread.pro if saving 1 GIF frame
;	nbr	12 Feb 2004 - Fix START; add dolimb to COMMON block
;	nbr	26 Feb 2004 - move loadct,0 for 24-bit display
;       KB      Aug 19 2004 - Fix "LOAD" bug 
;	rah	Sep 16 2004 - Add capability for true color images
;       aee     Jun 15 2006 - Added coed to handle SECCHI headers
;       aee     Jun 22 2006 - coorected non-secchi date-obs bug.
;       aee     Sep    2006 - Added more SECCHI related code.
;
; Version     : 
;       @(#)SCC_PLAYMOVIE.pro	1.27, 09/23/04 : NRL LASCO LIBRARY
;
; See Also    : MKMOVIE.PRO
;
;-            
;$Log: scc_playmovie.pro,v $
;Revision 1.46  2015/04/01 19:03:03  hutting
;uses mvi header for hsize when finding suncenter if lasco file hdrs are blank
;
;Revision 1.45  2013/07/01 18:16:16  mcnutt
;if fits headrs have keyword rectify set rect to it
;
;Revision 1.44  2012/10/26 15:09:48  mcnutt
;corrected osczero for flatplane movies compare fix of sec_pixs *1000
;
;Revision 1.43  2012/08/29 14:11:20  mcnutt
;added wcs_combine bpos options
;
;Revision 1.42  2012/08/13 17:15:07  avourlid
;Fixed typo introduced during R_SUN keyword addition
;
;Revision 1.41  2012/08/13 17:12:27  avourlid
;Added R_SUN keyword
;
;Revision 1.40  2012/05/17 14:05:50  mcnutt
;enhanced crop options
;
;Revision 1.39  2012/05/10 15:27:26  nathan
;add MARGIN= and /LAST to scc_playmovie
;
;Revision 1.38  2012/05/08 12:33:03  mcnutt
;removed scc_draw_box use draw_box and add wait when cropping with auto advance
;
;Revision 1.37  2012/05/07 16:59:59  mcnutt
;added auto advance option when cropping
;
;Revision 1.36  2012/04/25 14:17:33  mcnutt
;added x and y size option for crop and zoom
;
;Revision 1.35  2012/02/09 16:31:17  mcnutt
;will not set secs if rtheta movie is read in
;
;Revision 1.34  2012/01/25 18:06:25  nathan
;add rolls to moviev
;
;Revision 1.33  2011/12/08 15:34:58  mcnutt
;corrected sun center in frame hdrs if keyword coords is set
;
;Revision 1.32  2011/12/08 15:26:56  mcnutt
;coords now work with truecolor
;
;Revision 1.31  2011/11/28 18:24:56  nathan
;add CARRINGTON option for sync MVI frames
;
;Revision 1.30  2011/11/22 16:22:11  mcnutt
;base(xy)offset are now an aray of 2 for bottom and right side positioning
;
;Revision 1.29  2011/11/01 16:28:13  mcnutt
;crrected sec_pix for flatplane movies uses theta and x axis
;
;Revision 1.28  2011/10/26 18:06:10  mcnutt
;updated sec_pix for flatplane movies
;
;Revision 1.27  2011/09/26 18:30:14  mcnutt
;added latplane movie grrid and measuring options
;
;Revision 1.26  2011/08/31 16:59:23  mcnutt
;changed zoom tool to work with box and added an option to save zoomed movie
;
;Revision 1.25  2011/08/29 17:37:15  mcnutt
;updates file and frame hdr info when img_rebin keyword is set
;
;Revision 1.24  2011/08/23 17:18:04  mcnutt
;added a moving center position for croped movies
;
;Revision 1.23  2011/08/10 18:40:39  mcnutt
;updates file_hdr and hdrs with sunxcen,sunycen and sec_pix if keywords are set if movie is save the keyword values will be used
;
;Revision 1.22  2011/05/12 15:37:00  mcnutt
;added nofits keyword
;
;Revision 1.21  2011/01/28 18:52:34  mcnutt
;added rectified keyword to call to scc_movieout if save keyword_set
;
;Revision 1.20  2010/12/02 18:38:41  mcnutt
;creates new file_hdr for new movies if not defined by keyword or sccread_mvi
;
;Revision 1.19  2010/11/24 18:18:34  nathan
;fix s/c in timestamp for SOHO; fix debugon; always new file_hdr for new movie
;
;Revision 1.18  2010/09/20 18:00:20  mcnutt
;moved get_hdr_movie_img to sccread_mvi.pro
;
;Revision 1.17  2010/08/12 16:48:25  mcnutt
;added rtheta header values
;
;Revision 1.16  2010/06/24 20:11:07  nathan
;retain SKIP= and /DEBUG from command line call; define base vars in moviev as long
;
;Revision 1.15  2010/06/23 17:56:51  nathan
;fix problem with SKIP=; debug messages
;
;Revision 1.14  2010/05/24 15:18:03  mcnutt
;added tag moviev.adjcolor
;
;Revision 1.13  2010/03/15 15:51:21  mcnutt
;work with lasco get_sun_center v1.4
;
;Revision 1.12  2010/02/04 22:28:22  nathan
;debug enhancements
;
;Revision 1.11  2010/01/20 13:51:57  mcnutt
;added keyword osczero for ab movies
;
;Revision 1.10  2010/01/14 20:06:53  mcnutt
;osczero is set to -1,-1 at start
;
;Revision 1.9  2010/01/14 16:05:20  mcnutt
;defines osczero for new movie
;
;Revision 1.8  2010/01/14 15:26:35  mcnutt
;defines osczero based on size of movie
;
;Revision 1.7  2009/12/08 16:15:26  mcnutt
;removed some add_tags
;
;Revision 1.6  2009/11/09 16:41:09  mcnutt
;added keyword wcshdr
;
;Revision 1.5  2009/10/21 19:01:50  mcnutt
;now works with hdr movies
;
;Revision 1.4  2009/10/21 14:52:35  mcnutt
;sets moivev.instxt for box selection
;
;Revision 1.3  2009/10/07 18:09:45  mcnutt
;added edit to moviev set by noxcolors
;
;Revision 1.2  2009/10/07 17:27:20  mcnutt
;changed common blocks moviev is now in common block scc_playmv_COMMON
;
;Revision 1.1  2009/10/05 18:53:20  mcnutt
;created from scc_wrunovie.pro
;
;
;
;____________________________________________________________________________
PRO SCC_PLAYMOVIE_DRAW, ev

COMMON scc_playmv_COMMON, moviev

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

   IF (TAG_EXIST(moviev, 'zoombase')) THEN zmbs=moviev.zoombase else zmbs=-1
   IF (ev.press GT 0 or ev.top eq zmbs) THEN RETURN 		;** only look at PRESS events
   IF ev.release eq 1 THEN begin 		;** only look at PRESS events
   if (moviev.box(0) eq 3) then begin ; recenter box
       xcen=moviev.box(1)+(moviev.box(3)-moviev.box(1))/2
       ycen=moviev.box(2)+(moviev.box(4)-moviev.box(2))/2
           xadj=ev.x-xcen
           yadj=ev.y-ycen
      if (moviev.box(9) eq 0) then begin
          WIDGET_CONTROL, moviev.box(5), GET_VALUE=valx
          WIDGET_CONTROL, moviev.box(6), GET_VALUE=valy
	  if moviev.box(1)+xadj lt 0 then moviev.box(1)=0
	  if moviev.box(1)+xadj ge 0 and moviev.box(3)+xadj le moviev.hsize-1 then moviev.box(1)=moviev.box(1)+xadj
	  if moviev.box(3)+xadj gt moviev.hsize-1 then moviev.box(1)= (moviev.hsize-valx) -1
          moviev.box(3)=(moviev.box(1)+valx) -1
	  if moviev.box(2)+yadj lt 0 then moviev.box(2)=0
	  if moviev.box(2)+yadj ge 0 and moviev.box(4)+yadj le moviev.vsize-1 then moviev.box(2)=moviev.box(2)+yadj
	  if moviev.box(4)+yadj gt moviev.vsize-1 then moviev.box(2)= (moviev.vsize-valy) -1
          moviev.box(4)=(moviev.box(2)+valy) -1
          xadj=0 & yadj=0 & ubox=moviev.box
      endif
      if (moviev.box(9) eq 1) then begin
          moviev.centadj(moviev.current,0:1)=[xadj,yadj]
          gd=indgen(moviev.len)+1
          uv=where(moviev.centadj(*,0) ne -1,cnt)
	  if cnt ge 2 then moviev.centadj(*,2)=interpol(reform(moviev.centadj(uv,0)),gd(uv),gd) else moviev.centadj(*,2)=xadj
          uv=where(moviev.centadj(*,1) ne -1,cnt)
	  if cnt ge 2 then moviev.centadj(*,3)=interpol(reform(moviev.centadj(uv,1)),gd(uv),gd) else moviev.centadj(*,3)=yadj
          if moviev.box(8) eq 1 then begin
             z=where(moviev.centadj(*,2)+moviev.box(3) gt moviev.hsize,cnt)
	     if cnt gt 0 then moviev.centadj(z,2) = (moviev.hsize-moviev.box(3))-1
	     z=where(moviev.centadj(*,3)+moviev.box(4) gt moviev.vsize,cnt)
	     if cnt gt 0 then moviev.centadj(z,3) = (moviev.vsize-moviev.box(4))-1
             z=where(moviev.box(1)+moviev.centadj(*,2) lt 0,cnt)
	     if cnt gt 0 then moviev.centadj(z,2) = moviev.box(1)*(-1)
             z=where(moviev.box(2)+moviev.centadj(*,3) lt 0,cnt)
	     if cnt gt 0 then moviev.centadj(z,3) = moviev.box(2)*(-1)
	  endif
      endif
      draw_box, event=ev
      WIDGET_CONTROL, moviev.box(5), SET_VALUE=string(moviev.box(3)-moviev.box(1)+1)
      WIDGET_CONTROL, moviev.box(6), SET_VALUE=string(moviev.box(4)-moviev.box(2)+1)
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='click on image to recenter'
    endif

   if (moviev.box(0) eq 2) then begin ; second corner selected
      moviev.box(3)=ev.x
      moviev.box(4)=ev.y
      moviev.box(0)=3 ;set to 3 when rxycen get defined.
      if moviev.box(1) gt moviev.box(3) then begin
          tmp=moviev.box(3) & moviev.box(3) = moviev.box(1) & moviev.box(1)=tmp
      endif
      if moviev.box(2) gt moviev.box(4) then begin
          tmp=moviev.box(4) & moviev.box(4) = moviev.box(2) & moviev.box(2)=tmp
      endif
      WIDGET_CONTROL, moviev.box(5), SET_VALUE=string(moviev.box(3)-moviev.box(1)+1)
      WIDGET_CONTROL, moviev.box(6), SET_VALUE=string(moviev.box(4)-moviev.box(2)+1)
      draw_box
   endif

   if (moviev.box(0) eq 1) then begin ; first corner selected
      moviev.box(1)=ev.x
      moviev.box(2)=ev.y
      moviev.box(0)=2
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='Select upper right corner'
   endif

   if (moviev.box(0) eq 6) then begin ; first corner selected
      if ev.x lt moviev.box(1) then moviev.box(4)=moviev.box(1)-ev.x else moviev.box(4)=ev.x-moviev.box(1)
      moviev.box(0)=0
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='use Re-select ROI to change coords'
      wset,moviev.boxwin
      tvcircle,moviev.box(4),moviev.box(1),moviev.box(2),color=255
      wset,moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.boxwin]
   endif
   if (moviev.box(0) eq 5) then begin ; second corner selected
      if ev.x lt moviev.box(1) then moviev.box(3)=moviev.box(1)-ev.x else moviev.box(3)=ev.x-moviev.box(1)
      moviev.box(0)=6
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='Select outer radius in X coords'
      wset,moviev.boxwin
      tvcircle,moviev.box(3),moviev.box(1),moviev.box(2),color=255
      wset,moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.boxwin]
   endif
   if (moviev.box(0) eq 4) then begin ; first corner selected
      moviev.box(1)=ev.x
      moviev.box(2)=ev.y
      moviev.box(0)=5
      if moviev.instxt ne 0 then widget_control,moviev.instxt,set_value='Select inner radius in X coords'
      wset,moviev.boxwin
      tmp=imgs(*,*,moviev.current)
      tmp(ev.x,ev.y-5:ev.y+5)=255
      tmp(ev.x-5:ev.x+5,ev.y)=255
      tv,tmp
      wset,moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.boxwin]
   endif
   ENDIF ;release eq 1
   if moviev.box(7) ge 1 and ev.release eq 4 then advance=1
   if datatype(advance) ne 'INT' then advance=0
       if advance eq 1 then begin
		 moviev.forward = 1
		 REPEAT BEGIN
		    moviev.current = (moviev.current + 1) MOD moviev.len
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.first
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.first
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                 WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                 WIDGET_CONTROL, moviev.slidenw, SET_VALUE=moviev.current
                 if (moviev.showzoom eq 1) then begin
                     ;WIDGET_CONTROL, base, SET_UVALUE=moviev
                    draw_zoom
                 endif 
                 IF moviev.sync_color GE 1 THEN plot_sync
                 if (moviev.box(7) ge 1) then draw_box 
       endif

END



;----------------------- SCC_PLAYMOVIE ---------------------------------

PRO SCC_PLAYMOVIE, win_indx, win_index2, NAMES=names, HDRS=hdrs, SKIP=skip, TIMES=times, $
    START=start, LENGTH=length, IMG_REBIN=img_rebin, SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, R_SUN = R_SUN,$
    DIFF=diff,  RUNNING_DIFF=running_diff, COORDS=coords, COSMIC=cosmic, FIXGAPS=fixgaps, $
    DRAW_LIMB=draw_limb, BLOCK_SIZE=block_size, LOGO=logo, VIDEO=video, NOCAM=nocam, MARGIN=margin, $
    DIF_MIN=dif_min, DIF_MAX=dif_max, KEEP=keep, FULL=full, LOAD=load, DEBUG=debug, SAVE=save, LAST=last, $
    CHSZ=chsz, SPOKE=spoke, R1=r1, R2=r2, TH1=th1, TH2=th2, N_R=n_r, N_TH=n_th, NOXCOLORS=noxcolors, $
    RECTIFIED=rectified, DORECTIFY=dorectify, CENRECTIFY=cenrectify, TRUECOLOR=truecolor, mvicombo=mvicombo,ccdpos=ccdpos,$
    file_hdr=file_hdr,wcshdr=wcshdr,osczero=osczero,nofits=nofits,RTHETA=RTHETA,RTCOORDS=rtcoords,ZOOM=ZOOM

COMMON scc_playmv_COMMON   ; is defined above
COMMON WSCC_MKMOVIE_COMMON
common SCC_COMBINE, cmbmovie,cmbstate,struct

if datatype(swmoviev) eq 'UND' then single=0 else single=swmoviev.single
if keyword_set(file_hdr) then file_hdr=file_hdr else undefine,file_hdr

IF keyword_set(ccdpos) then ccd_pos=ccdpos ELSE undefine,ccd_pos
IF keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0
;IF datatype(debug_on) NE 'UND' THEN if debug_on THEN debugon=1
IF debugon THEN debug_on=1 ELSE debug_on=0  ; for common wscc_mkmovie_common
rnx=0 & rny=0

   IF XRegistered("SCC_PLAYMOVIE") THEN BEGIN
      bell = STRING(7B)
      PRINT, bell
      print,'%%There is an instance SCC_PLAYMOVIE already running.  Only one instance may run at a time.'
      print,'%%Recommend either exitting existing wrunmovie session or type WIDGET_CONTROL,/RESET.'
      RETURN
   ENDIF


   IF (N_PARAMS() NE 0) THEN win_index = win_indx
   IF keyword_set(LAST) THEN win_index = moviev.win_index
   IF (datatype(truecolor) EQ 'UND')  THEN truecolor=0
   IF datatype(dofull) EQ 'UND' or NOT keyword_set(LOAD) THEN dofull=0
   IF keyword_set(FULL) THEN dofull=1

   ;nskip=0
   stall0 = 95
   stall = (100 - stall0)/50.
if single eq 1 then stall = 10000
   stallsave = stall
   filename = ''
   filepath = './'
   ftitle=''
   rect=0 	; default
   ;set_plot,'x' ;
   select_windows

   IF datatype(nskip) EQ 'UND' or NOT keyword_set(LOAD) THEN nskip=0
   IF KEYWORD_SET(SKIP) THEN nskip = skip
   IF datatype(dolimb) EQ 'UND' or NOT keyword_set(LOAD) THEN dolimb=0
   IF keyword_set(DRAW_LIMB) THEN dolimb=1

   IF KEYWORD_SET(START) THEN dostart=start ELSE start=0
   IF datatype(dostart) EQ 'UND' THEN dostart=0
   IF keyword_set(LOAD) THEN start = dostart

   IF datatype(dif_min0) EQ 'UND' or NOT keyword_set(LOAD) THEN dif_min0=-70
   IF keyword_set(DIF_MIN) THEN dif_min0=dif_min
   IF datatype(dif_max0) EQ 'UND' or NOT keyword_set(LOAD) THEN dif_max0=70
   IF keyword_set(DIF_MAX) THEN dif_max0=dif_max
   dif_min=dif_min0
   dif_max=dif_max0
   
   IF datatype(dotimes) EQ 'UND' or NOT keyword_set(LOAD) THEN dotimes=0
   IF KEYWORD_SET(TIMES) THEN dotimes=times
   IF KEYWORD_SET(NOCAM) THEN NO_CAM=1

   IF KEYWORD_SET(SUNXCEN) THEN xcen = sunxcen ELSE xcen = 0
   IF KEYWORD_SET(SUNYCEN) THEN ycen = sunycen ELSE ycen = 0
   IF KEYWORD_SET(SEC_PIX) THEN secs = sec_pix ELSE secs = 0
   IF KEYWORD_SET(R_SUN)   THEN rsun = r_sun   ELSE rsun= 0

IF datatype(moviev) NE 'UND' THEN BEGIN
; NOT called from command line
    ; These are currently defined in moviev structure
    nskip=moviev.nskip
    
    ;debugon=moviev.debugon
    ;debug_on=debugon
ENDIF
    
    
   ;** have user select movie file (.mvi)
   IF (DATATYPE(win_index) EQ 'UND') THEN BEGIN
      file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path)
      IF (file EQ '') THEN RETURN
      BREAK_FILE, file, a, dir, name, ext
      IF (dir EQ '') THEN win_index = path+file ELSE win_index = file
   ENDIF

    if keyword_set(wcshdr) then wcshdrs=wcshdr

;>>>>>>>>>>** read movie in from movie file (.mvi)
IF (DATATYPE(win_index) EQ 'STR') THEN BEGIN

all_files = win_index

IF datatype(dordiff) EQ 'UND' or NOT keyword_set(LOAD) THEN dordiff=0
IF keyword_set(RUNNING_DIFF) THEN dordiff=running_diff
IF (N_ELEMENTS(all_files) GT 1) AND dordiff THEN BEGIN
   IF (dordiff GT 1) THEN BEGIN
      PRINT, '%%SCC_PLAYMOVIE Unable to difference from ',dordiff,' images  when appending .mvi files'
      RETURN
   ENDIF
ENDIF
IF datatype(dodiff) EQ 'UND' or NOT keyword_set(LOAD) THEN dodiff=0
IF keyword_set(DIFF) THEN dodiff=diff
IF (N_PARAMS() EQ 2) THEN do2=1 ELSE do2=0  	; display 2 movies side by side

IF KEYWORD_SET(COSMIC) THEN BEGIN
   test = CCOSMICS(BYTARR(10,10))
   IF (N_ELEMENTS(test) EQ 1) THEN use_ccosmics=0 ELSE use_ccosmics=1
   IF (use_ccosmics EQ 0) THEN PRINT, '%%SCC_PLAYMOVIE Using POINT_FILTER instead of CCOSMICS for cosmic ray removal.'
ENDIF

ii=0
FOR f=0, N_ELEMENTS(all_files)-1 DO BEGIN
    curr_file = all_files(f)
      BREAK_FILE, curr_file, a, dir, name, ext
      IF (do2 EQ 1) THEN BREAK_FILE, win_index2, a2, dir2, name2, ext2
      ftitle = name+ext
      filename = curr_file
      filepath = dir
      IF (dir EQ '') THEN filepath = './'
      if strpos(ftitle,'.hdr') gt -1 then pngmvi=filename else pngmvi='0'
      OPENR,lu,filename,/GET_LUN
      SCCREAD_MVI, lu, file_hdr, ihdrs, imgsptr, swapflag , pngmvi=pngmvi
      print,'MVI version ',trim(file_hdr.ver)

    ahdr = SCCMVIHDR2STRUCT(ihdrs(0),file_hdr.ver)
    det=ahdr.detector
    dindx=where(['C2','C3','EIT'] EQ det,issoho)	

    ; do2 means 2 movies side-by-side
     IF (do2 EQ 1) THEN BEGIN
        BREAK_FILE, win_index2, a2, dir2, name2, ext2
        OPENR,lu2,win_index2,/GET_LUN
        filepath2 = dir2
        IF (dir2 EQ '') THEN filepath2 = './'
        if strpos(win_index2,'.hdr') gt -1 then pngmvi2=win_index2 else pngmvi2='0'
        SCCREAD_MVI, lu2, file_hdr2, ihdrs2, imgsptr2, swapflag2 , pngmvi=pngmvi2
      ENDIF

    secs = file_hdr.sec_pix
    IF secs LE 0 and file_hdr.rtheta eq 0 THEN secs=ahdr.cdelt1
    IF secs LE 0 and issoho  and file_hdr.rtheta eq 0 THEN secs = GET_SEC_PIXEL(ahdr, FULL=file_hdr.nx)
    
    solr=ahdr.rsun	    ; no radius in file_hdr
    IF solr EQ 0 and issoho THEN solr = GET_SOLAR_RADIUS(ahdr)
    xcen=file_hdr.sunxcen
    ycen=file_hdr.sunycen
    IF xcen EQ 0 and issoho THEN BEGIN
    	print,'Calculating SOHO sun center for ',ahdr.date_obs,' frame # 0'
        fhdr=mk_lasco_hdr(ahdr)
	sunc = GET_SUN_CENTER(fhdr, FULL=hsize,/DOCHECK)
	xcen=sunc.xcen
	ycen=sunc.ycen
    ENDIF
    
; frame header values are over-ridden by keywords
    IF (KEYWORD_SET(SUNXCEN)) THEN xcen = sunxcen
    IF (KEYWORD_SET(SUNYCEN)) THEN ycen = sunycen
    IF (KEYWORD_SET(SEC_PIX)) THEN secs = sec_pix

      nx = file_hdr.nx
      ny = file_hdr.ny
      rect=file_hdr.rectified
      rnx = nx
      rny = ny
      truecolor = file_hdr.truecolor

;      IF (truecolor) THEN BEGIN
;    	noxcolors=1 
;	print,'Images are TrueColor; Adjust Color feature not supported.'
;      ENDIF
    
      IF datatype(mlength) EQ 'UND' THEN mlength=0   ; KB
      
      ; define lastframe of current MVI file
      IF KEYWORD_SET(LENGTH) THEN BEGIN
      	lastframe=start+length-1
	mlength=length
      ENDIF ELSE $
      	IF keyword_set(LOAD) THEN $
		IF mlength EQ 0 THEN $
			lastframe=file_hdr.nf-1 $
		ELSE lastframe=start+mlength-1 $
	ELSE lastframe=file_hdr.nf-1
      
      lastframe = lastframe<(file_hdr.nf-1)

      IF f GT 0 THEN nf=nf+(lastframe-start+1) ELSE nf=lastframe-start+1
      IF debugon THEN help, nx,ny,rect,true_color,nf,imgsptr,imgsptr2
      IF debugon THEN wait,2

      IF keyword_set(VIDEO) THEN BEGIN
	 xout = 36
	 yout = 34
	 IF not(keyword_set(IMG_REBIN)) THEN BEGIN
	    IF ny GT 480 THEN BEGIN
		y1 = (ny-480)/2
		y2 = ny-1-(ny-480)/2
	    ENDIF ELSE BEGIN
		y1 = 0
		y2 = ny-1
	    ENDELSE
	    coords=[(nx-640)/2,nx-1-(nx-640)/2,y1,y2]
	 ENDIF ELSE BEGIN
		new_y = (float(ny)/nx)*640
		IMG_REBIN=[640,new_y]
		IF new_y GT 480 THEN coords=[0,nx-1,(nx/640.)*80,ny-1-(nx/640.)*80] $
		    ELSE coords=[0,nx-1,0,ny-1]
	 ENDELSE
      ENDIF ELSE BEGIN
	xout=15
	yout=15
      ENDELSE
      IF datatype(docoords) EQ 'UND' or NOT keyword_set(LOAD) THEN docoords=0
      IF KEYWORD_SET(COORDS) THEN docoords=coords
      coords=docoords
      IF keyword_set(COORDS) THEN BEGIN
         IF (N_ELEMENTS(COORDS) EQ 1) THEN BEGIN	;** interactive selection
            if (pngmvi ne '0') THEN tmp=get_hdr_movie_img(filepath+string(imgsptr(0)),truecolor=file_hdr.truecolor) else tmp=imgsptr(0)
	    AWIN,tmp
            SETIMAGE
            EXPTV, tmp, /NOBOX
            if file_hdr.truecolor eq 0 then tmp=TVSUBIMAGE(tmp, img0_x1, img0_x2, img0_y1, img0_y2)
            WDELETE, !D.WINDOW
         ENDIF ELSE BEGIN				;** user passed in [x1,x2,y1,y2]
            img0_x1=coords(0)
            img0_x2=coords(1)
            img0_y1=coords(2)
            img0_y2=coords(3)
         ENDELSE
         rnx = img0_x2-img0_x1+1
         rny = img0_y2-img0_y1+1
         xcen = xcen - img0_x1
         ycen = ycen - img0_y1
      ENDIF

      IF datatype(dorebin) EQ 'UND' or NOT keyword_set(LOAD) THEN dorebin=0 
      IF KEYWORD_SET(IMG_REBIN) THEN dorebin=img_rebin
      IF KEYWORD_SET(ZOOM) THEN dorebin=[rnx*zoom,rny*zoom]
      IF n_elements(dorebin) GT 1 THEN BEGIN
         rrnx = dorebin(0)
         rrny = dorebin(1)
         if FLOAT(rrnx)/rnx ne FLOAT(rrny)/rny then rrny= rny * (FLOAT(rrnx)/rnx)
;** if shrinking by integer factor use REBIN otherwise use CONGRID with linear interpolation
         IF ((nx MOD rrnx) EQ 0) THEN use_rebin=1 ELSE use_rebin=0
         xcen = xcen * FLOAT(rrnx)/rnx
         ycen = ycen * FLOAT(rrny)/rny
         secs = secs / (FLOAT(rrny)/rny)
         rnx = rrnx
         rny = rrny
      ENDIF

    ;WINDOW,2,xsiz=nx,ysiz=ny,/pixmap
    IF dodiff or dordiff THEN loadct,0

    IF ~keyword_set(NOXCOLORS) and ~(TRUECOLOR) THEN BEGIN
    	IF f EQ 0 THEN imgs=bytarr(rnx*(do2+1),rny, (nf/(nskip+1))+(nf mod (nskip+1) ne 0)) $
	ELSE BEGIN
	    sz=size(imgs)
	    imgs2=bytarr(sz[1],sz[2],nf)
	    imgs2[*,*,0:sz[3]-1]=imgs
	    imgs=imgs2
	    undefine,imgs2
	ENDELSE
    ENDIF

    IF ~keyword_set(NOXCOLORS) and (TRUECOLOR) THEN BEGIN
    	IF f EQ 0 THEN imgs=bytarr(3,rnx*(do2+1),rny, (nf/(nskip+1))+(nf mod (nskip+1) ne 0)) $
	ELSE BEGIN
	    sz=size(imgs)
	    imgs2=bytarr(sz(1),sz[2],sz[3],nf)
	    imgs2[0,*,*,0:sz[3]-1]=imgs(0,*,*,*)
	    imgs2[1,*,*,0:sz[3]-1]=imgs(1,*,*,*)
	    imgs2[2,*,*,0:sz[3]-1]=imgs(2,*,*,*)
	    imgs=imgs2
	    undefine,imgs2
	ENDELSE
    ENDIF


    IF debugon THEN help,imgs,start,do2,dordiff,dodiff
    IF debugon THEN wait,2

    FOR i=start,lastframe,nskip+1 DO BEGIN
      PRINT, '%%SCC_PLAYMOVIE reading frame ', STRING(i+1,FORMAT='(I4)'), ' of ',STRING(file_hdr.nf,FORMAT='(I4)'), $
                ' from movie file ', filename
		
      set_plot,'z'
      device,set_resolution=[rnx*(do2+1),rny]
	
;    	WINDOW, XSIZE = rnx*(do2+1), YSIZE = rny, /PIXMAP, /FREE    ;** nbr, 12/17/98
;	IF ((f EQ 0) AND (i EQ start)) THEN win_index = !D.WINDOW ELSE win_index = [win_index, !D.WINDOW]

    	ahdr = SCCMVIHDR2STRUCT(ihdrs(i),file_hdr.ver)
	
      if (pngmvi ne '0') THEN image=get_hdr_movie_img(filepath+string(imgsptr(i)),truecolor=file_hdr.truecolor) else image = imgsptr(i)

;         IF ( swapflag EQ 1 ) THEN BYTEORDER, ahdr

      if ~truecolor then begin
         IF (dordiff) THEN BEGIN
;** if cat'ing together .mvi files you have to do a running diff of prev img only
         IF (N_ELEMENTS(all_files) GT 1) THEN BEGIN
            IF (i GT start+dordiff-1) OR (f GT 0) THEN BEGIN
               tmp = image
               image = BYTSCL(FIX(image)-prev, min=dif_min,max=dif_max)
               prev = tmp
            ENDIF ELSE prev = image
         ENDIF ELSE BEGIN
            IF (i GT start+dordiff-1) THEN BEGIN
               prev=imgsptr(i-dordiff)
               image = BYTSCL(FIX(image)-prev, min=dif_min,max=dif_max)
            ENDIF
         ENDELSE
         ENDIF

         IF dodiff THEN BEGIN
            IF (i GT start) OR (f GT 0) THEN BEGIN
               image = BYTSCL(FIX(image)-prev, min=dif_min,max=dif_max)
            ENDIF ELSE prev = image
         ENDIF

         IF KEYWORD_SET(COORDS) THEN BEGIN
            image = image(img0_x1:img0_x2,img0_y1:img0_y2)
         ENDIF 

         IF n_elements(dorebin) GT 1 THEN BEGIN
            IF (use_rebin EQ 1) THEN $
               image = REBIN(image, rnx, rny) $
            ELSE $
               image = CONGRID(image, rnx, rny, /INTERP)
            ahdr.XCEN=ahdr.XCEN*(rnx/(nx*1.0))
            ahdr.YCEN=ahdr.YCEN*(rny/(ny*1.0))
            ahdr.CDELT1=ahdr.CDELT1*(nx/(rnx*1.0))
         ENDIF 
	 
	 IF keyword_set(RECTIFIED) and keyword_set(DORECTIFY) THEN $
	 IF rectified EQ 180 THEN image = rotate(image,2)

         IF KEYWORD_SET(COSMIC) THEN BEGIN
            IF (use_ccosmics EQ 1) THEN image = CCOSMICS(image) ELSE BEGIN
               POINT_FILTER,image,5,7,3,tmp,pts
	       IF debugon THEN help,pts
               image = tmp
            ENDELSE
         ENDIF 

    	IF keyword_set(LOGO) THEN BEGIN
	    IF (issoho) THEN image = add_lasco_logo(temporary(image)) ELSE $
	    	    	     image = scc_add_logo(temporary(image))
	ENDIF
	
    	TV, image, TRUE=TRUECOLOR

;	IF ~keyword_set(NOXCOLORS) THEN imgs[0:rnx-1,*,i]=image
	
    	IF (do2 EQ 1) THEN BEGIN
            if (pngmvi2 ne '0') THEN image=get_hdr_movie_img(filepath2+string(imgsptr2(i)),truecolor=file_hdr.truecolor) else image = imgsptr2(i)
            IF n_elements(dorebin) GT 1 THEN BEGIN
               IF (use_rebin EQ 1) THEN $
                  image = REBIN(image, rnx, rny) $
               ELSE $
                  image = CONGRID(image, rnx, rny, /INTERP)
            ENDIF
            TV, image, rnx, 0, TRUE=truecolor
	    IF ~keyword_set(NOXCOLORS) THEN imgs[rnx:2*rnx-1,*,ii]=image
    	ENDIF

    	IF debugon THEN help,solr,i,ii
         IF dolimb THEN $
            TVCIRCLE, solr/secs, xcen, ycen, COLOR=255, THICK=2
    
         IF KEYWORD_SET(SPOKE) THEN begin           
            image_tmp=tvrd()
            sz=size(image_tmp)
            if not keyword_set(R1) then r1=0		; 0 or helio-centric center as default
            if not keyword_set(R2) then r2=sz[1]/2  	; (pixels) half image size as default
                                                    	; maximum horizontal size
            if not keyword_set(TH1) then th1=0		; 0 degree as default
            if not keyword_set(TH2) then th2=360	; 360 degree as default
            if not keyword_set(N_R) then n_r=300	; samples of radius
            if not keyword_set(N_TH) then n_th=th2-th1+1 	; samples of angles
            print,'making spoked image, angle sample=',n_th,'  radius sample=',n_r
            rad_spoke,image_tmp,image_rad,x0=xcen,y0=ycen,r1=r1,r2=r2,th1=th1,th2=th2,n_r=n_r,n_th=n_th
            tv,congrid(rotate(image_rad,1),(size(image_tmp))[1],(size(image_tmp))[2])
	    file_hdr.RTHETA = 1
	    file_hdr.RADIUS0 = r1
	    file_hdr.RADIUS1 = r2
	    file_hdr.THETA0  = th1
	    file_hdr.THETA1  = th2
        ENDIF

	 IF dotimes THEN BEGIN
            IF not keyword_set(CHSZ) then charsz=1.0 else charsz=CHSZ
	    IF dotimes GT 1 THEN time_color=times ELSE time_color=255
            xouts=10 & youts=10
            XYOUTS, xouts, youts, strmid(ahdr.date_obs,0,4)+'/'+strmid(ahdr.date_obs,5,2)+'/'+strmid(ahdr.date_obs,8,2)+' '+strmid(ahdr.time_obs,0,8),/DEVICE, CHARSIZE=charsz, COLOR=time_color
            IF ~keyword_set(NOCAM) THEN begin
                no_cam=1	    
		sc=strmid(ahdr.filename,strpos(ahdr.filename,'.')-1,1)
                camt=ahdr.detector
		IF sc EQ 'A' or sc EQ 'B' THEN camt=camt+'-'+sc
	        cyouts=youts+ charsz*10 + 5
     	        XYOUTS, xouts, cyouts, strupcase(camt), /DEVICE,  CHARSIZE=charsz, COLOR=time_color
            ENDIF else no_cam=0


	 ENDIF

         image=tvrd()

      endif   ;if ~truecolor 

      IF KEYWORD_SET(COORDS) and truecolor THEN BEGIN
         image = image(*,img0_x1:img0_x2,img0_y1:img0_y2)
      ENDIF 

      ;set_plot,'x'
      select_windows
            

      WINDOW, XSIZE = rnx*(do2+1), YSIZE = rny, /PIXMAP, /FREE    ;** nbr, 12/17/98
      tv,image ,true=truecolor
      IF ~keyword_set(NOXCOLORS) and ~(truecolor) THEN imgs[0:rnx*(do2+1)-1,*,ii]=image
      IF ~keyword_set(NOXCOLORS) and (truecolor) THEN imgs[*,0:rnx*(do2+1)-1,*,ii]=image

      IF ((f EQ 0) AND (i EQ start)) THEN win_index = !D.WINDOW ELSE win_index = [win_index, !D.WINDOW]

      IF ((i EQ start) AND (f EQ 0)) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
      IF (i EQ start) THEN aname = ahdr.filename ELSE aname = [aname, ahdr.filename]

      start=0
      ii=ii+1
    ENDFOR
    IF (f EQ 0) THEN names = aname ELSE names = [names,aname]
    CLOSE,lu
    FREE_LUN,lu
ENDFOR		;** looping over all input .mvi files

ENDIF	;>>>>>>>>>>>>** End read movie in from .mvi file

IF keyword_set(NOXCOLORS) THEN undefine, imgs
IF keyword_set(NOXCOLORS) THEN edit=0 else edit=1
   ;** load movies from existing pixmaps (later)
   
   WSET, win_index(0)
   hsize = !D.X_SIZE
   vsize = !D.Y_SIZE
   ;** get length of movie
   len = N_ELEMENTS(win_index)
   frames = STRARR(len)	;** array of movie frame names (empty)
   IF (KEYWORD_SET(names) NE 0) THEN BEGIN
	BREAK_FILE, names, as, dirs, short_names, exts
	frames = short_names+exts
   ENDIF

   IF datatype(dofixgaps) EQ 'UND' or NOT keyword_set(LOAD) THEN dofixgaps=0
   IF keyword_set(FIXGAPS) THEN dofixgaps=1
   
   IF dofixgaps THEN BEGIN
    	PRINT, '%%SCC_PLAYMOVIE: Fixing missing blocks.'
    	IF NOT(KEYWORD_SET(BLOCK_SIZE)) THEN block_size = 32*(FLOAT(rnx)/nx)
    	SET_DATA_GAPS, win_index, 0, /AFTER, BLOCK_SIZE=block_size, N_BLOCK=n_block
    	PRINT, '%%SCC_PLAYMOVIE frame: ', 0, ' Num blocks replaced: ', n_block
    	FOR i=1, len-1 DO BEGIN
    	    SET_DATA_GAPS, win_index, i, /BEFORE, BLOCK_SIZE=block_size, N_BLOCK=n_block
    	    PRINT, '%%SCC_PLAYMOVIE frame: ', i, ' Num blocks replaced: ', n_block
      	ENDFOR
   ENDIF

   first = 0
   last = len-1
   current = 0
   forward = 1
   dirsave = 1
   bounce = 0
   pause = 0
   dopause = 1
   deleted = BYTARR(len)
   showmenu = 0
   if tag_exist(hdrs,'rectify') then rect=hdrs[0].rectify
   IF keyword_set(RECTIFIED) THEN BEGIN
   	rect=rectified 
	IF keyword_set(CENRECTIFY) and rect EQ 180 THEN BEGIN
		xcen = hsize-1-xcen
		ycen = vsize-1-ycen
	ENDIF
   ENDIF 
   
   IF tag_exist(hdrs,'roll') THEN rolls=hdrs.roll ELSE rolls=hdrs.crota
   
   ; As an interim fix to problems with 24-bit color, do the following
   ; so that saved frames are readable.
   
   ;device,get_visual_depth=depth
   ;IF depth GT 8 THEN loadct,0
   
   ; ***********************
;stop

;**--------------------Create Widgets-------------------------------**
hhsize = (hsize+20) > (512+20)
yscsz= vsize < 1024
xscsz= hsize < 1024
title = 'SCC_RUNMOVIE: '+ftitle
baseyoff=[0,0]
basexoff=[0,0]
IF keyword_set(MARGIN) THEN basexoff=[margin,margin]



  

;   base = WIDGET_BASE(/COLUMN, XOFFSET=basexoff, YOFFSET=baseyoff, TITLE='SCC_PLAYMOVIE Control')

   
   IF keyword_set(SAVE) THEN draw_win=-1; ELSE WSET, draw_win

;**--------------------Done Creating Widgets-----------------------------**

;--Finish creating frame headers
   IF KEYWORD_SET(SUNXCEN) THEN FOR i=0,len-1 DO hdrs[i].xcen=xcen
   IF KEYWORD_SET(SUNYCEN) THEN FOR i=0,len-1 DO hdrs[i].ycen=ycen
   IF KEYWORD_SET(SEC_PIX) THEN FOR i=0,len-1 DO hdrs[i].cdelt1=secs
   IF KEYWORD_SET(R_SUN)   THEN FOR i=0,len-1 DO hdrs[i].rsun=rsun
   IF KEYWORD_SET(COORDS)  THEN FOR i=0,len-1 DO hdrs[i].xcen=hdrs[i].xcen-img0_x1
   IF KEYWORD_SET(COORDS)  THEN FOR i=0,len-1 DO hdrs[i].ycen=hdrs[i].ycen-img0_y1

;--Define file_hdr if a new movie
  IF (DATATYPE(file_hdr) eq 'UND') then begin
      if (tag_exist(hdrs(0),'r1row')) THEN def_mvi_hdr,file_hdr,hdr=hdrs(0) else begin
      	IF datatype(file_hdr) EQ 'UND' THEN def_mvi_hdr,file_hdr
      	file_hdr.SUNXCEN= xcen
      	file_hdr.SUNYCEN= ycen
      	file_hdr.RECTIFIED= rect
      	file_hdr.TRUECOLOR= truecolor
      	file_hdr.sec_pix= secs   
      	IF (datatype(CCD_POS) eq 'UND') THEN file_hdr.CCD_POS=[0,0,0,0] else file_hdr.CCD_POS=CCD_POS
      endelse 
      file_hdr.NF= Len
      file_hdr.NX= hsize
      file_hdr.NY= Vsize
      file_hdr.TRUECOLOR= truecolor
      IF KEYWORD_SET(RTHETA) THEN begin
         file_hdr.RTHETA = RTHETA
	 file_hdr.RADIUS0 = RTCOORDS(0)
	 file_hdr.RADIUS1 = RTCOORDS(1)
	 file_hdr.THETA0  = RTCOORDS(2)
	 file_hdr.THETA1  = RTCOORDS(3)
      ENDIF
   endif

   IF xcen ne 0 THEN file_hdr.SUNXCEN = xcen
   IF ycen ne 0 THEN file_hdr.SUNYCEN = ycen
;   IF KEYWORD_SET(SEC_PIX) THEN file_hdr.sec_pix = sec_pix
   IF secs ne 0 THEN file_hdr.sec_pix = secs 
   IF rnx ne 0 THEN file_hdr.nx = rnx 
   IF rny ne 0 THEN file_hdr.ny = rny 

   if keyword_set(mvicombo) then evtpro='SCC_CMBMOVIE_DRAW' else evtpro='SCC_PLAYMOVIE_DRAW'

      IF debugon THEN BEGIN
	    help,/str,ahdr,file_hdr
	    message,'Debug',/info
	    wait,3
      ENDIF
   IF KEYWORD_SET(nofits) THEN nofits = 1 ELSE nofits = 0
   IF (KEYWORD_SET(HDRS) NE 0) THEN img_hdrs = hdrs ELSE img_hdrs = 0
   IF (KEYWORD_SET(osczero) NE 0) THEN osczero=osczero ELSE osczero = [-1,-1]
   IF (file_hdr.radius0 ne 0 or file_hdr.radius1 ne 0 or file_hdr.theta0 ne 0 or file_hdr.theta1 ne 0) and file_hdr.rtheta eq 0 THEN BEGIN
     if fix(((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx)*1000) ne fix(((file_hdr.radius1-file_hdr.radius0)/file_hdr.ny)*1000) then begin ; check for abmovie
        if ((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx) lt ((file_hdr.radius1-file_hdr.radius0)/file_hdr.ny) then begin
            file_hdr.sec_pix=((file_hdr.radius1-file_hdr.radius0)/file_hdr.ny) & osczero = [file_hdr.nx/2,0]
	endif else begin
            file_hdr.sec_pix=((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx) & osczero = [0,file_hdr.ny/2]
	endelse        
     endif else file_hdr.sec_pix=((file_hdr.theta1-file_hdr.theta0)/file_hdr.nx)
     flatplane=1
     print,file_hdr.sec_pix
   ENDIF ELSE flatplane=0

   moviev = { $
; 		base:base, $ 
		rolls:rolls, $
 		xcen:xcen, $ 
 		ycen:ycen, $ 
                rsun:rsun, $
 		secs:secs, $ 
 		current:current, $ 
 		forward:forward, $ 
 		dirsave:dirsave, $ 
 		bounce:bounce, $ 
 		first:first, $ 
 		last:last, $ 
 		pause:pause, $ 
 		dopause:dopause, $ 
 		len:len, $ 
 		hsize:hsize, $ 
 		vsize:vsize, $ 
 		win_index:win_index, $ 
 		frames:frames, $ 
 		deleted:deleted, $ 
                showmenu:showmenu, $
                showzoom:0, $
                filename:filename, $
                filepath:filepath, $
                flatplane:flatplane, $
                img_hdrs:img_hdrs, $
                file_hdr:file_hdr, $
 		nskip:nskip, $  	    	; We need this in case new movie read in from pickfile. NR, 6/23/10
 		stallsave:stallsave, $ 
  		stall:stall, $ 
                basexoff:basexoff, $ 
                baseyoff:baseyoff, $ 
                stall0:stall0, $ 
                rect:rect, $
		evtpro:evtpro, $
		truecolor:truecolor, $
		box:intarr(10), $
                debugon:debugon, $
                edit:edit, $
                instxt:0, $
    	    	sliden:-1, $
    	    	cframe:-1, $
    	    	cname:-1, $
    	    	base:-1L, $
		winbase:-1L, $
		cmndbase:-1L, $
		slidenw:-1, $
		draw_win:-1, $
                boxsel:'', $
  		boxbase:-1L, $
		zoom_w:-1, $
  		zoombase:-1L, $
		zslide:-1, $
;		zstate:-1, $
    	    	cmbsliden:-1, $  ;add combine movie values
    	    	cmbvals:[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0], $
    	    	cropinst:-1, $           
    	    	cmbbase:-1L, $             
    	    	showcmb:-1, $             
;            	bkgval:[-1,-1,-1], $ ;add_tag still need to be removed from wscc_combine_mvi
            	brclr:-1, $ 
                bgclr:-1, $ 
                bbclr:-1, $ 
    	    	trackspotby:'', $
  		adjcolor:0, $
  		solead:-1, $
		sobase:-1L, $
                sync_point:fltarr(2,len)-1, $
		sync_color:-1, $
		osczero:osczero, $		
                objectv:['A','E','S','1.0','255'], $
                nofits:nofits $
		} 

   if datatype(wcshdrs) ne 'UND' then moviev=add_tag(moviev,wcshdrs,'wcshdrs')

    bkgcolor=0	; background for annotation text
    WINDOW, 11, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='SCC_PLAYMOVIE label bkg'
    erase,bkgcolor  ; this is the color behind annotations on the image from the cursor position
    WINDOW, 12, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='SCC_PLAYMOVIE box high light'
    erase,255  ; this is the color behind annotations on the image from the cursor position

    IF debugon THEN BEGIN
    	help,moviev
    	help,/str,moviev
    ENDIF
   scc_movie_win,mvicombo=mvicombo
   scc_movie_control
   if keyword_set(mvicombo) then scc_cmbmovie,mvicombo
   IF KEYWORD_SET(SAVE) THEN BEGIN
        moviev.filename=save
	IF (CHECK_PERMISSION(save) EQ 1) THEN scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=moviev.filename,rect=RECTIFIED,/saveonly 
        exit_playmovie,retain_win_index=keep
   ENDIF ELSE BEGIN
      WSET, moviev.draw_win
      XMANAGER, 'SCC_PLAYMOVIE', moviev.cmndbase, EVENT_HANDLER='SCC_MOVIE_CONTROL_EVENT'
   ENDELSE



END
