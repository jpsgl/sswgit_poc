FUNCTION median_file, filename, MIN=min, MONTH=month
;+
; $Id: median_file.pro,v 1.2 2008/03/17 12:37:27 mcnutt Exp $
;
; Name        : median_file
;
; Purpose     : return path of median file
;               
; Explanation : 
;               
; Use         : median_file, filename 
;    
; Inputs      : filename
;               
; Outputs     : path
;               
; Keywords    : /MIN ,/MONTH
;
; Calls       : 
;
; Side effects: 
;               
; Category    : Image Display calibration inout
;
;
; $Log: median_file.pro,v $
; Revision 1.2  2008/03/17 12:37:27  mcnutt
; added basic header info for wiki page
;
; Revision 1.1  2007/08/17 17:31:38  nathan
; moved from dev/movie
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;

COMMON DIR_COMMON, mvi_dir, jmap_dir, median_dir, diff_dir, image_dir

len = strlen(filename)
day = strmid(filename, len-25, 8)
IF(keyword_set(MONTH)) THEN day = strmid(day,0,6)
sc = strlowcase(strmid(filename, len-5, 1))
CASE strlowcase(strmid(filename, len-7, 2)) OF
	'eu': tel = 'euvi'
	'c1': tel = 'cor1'
	'c2': tel = 'cor2'
	'h1': tel = 'hi_1'
	'h2': tel = 'hi_2'
	ELSE: BEGIN
		print, 'No matching telescope'
		RETURN, -1
		END
ENDCASE
path = median_dir + tel + '/' + day + sc + (keyword_set(MIN) ? '_min.fts' : '_med.fts')
RETURN, path

END
