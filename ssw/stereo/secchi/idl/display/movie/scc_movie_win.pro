;+
;
;$Id: scc_movie_win.pro,v 1.180 2014/06/12 15:48:27 hutting Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_MOVIE_WIN
;               
; Purpose     : Widget tool to display animation sequence.
;               
; Explanation : This tool allows the user to view a series of images as
;		an animation sequence.  The user can control the direction,
;		speed, and number of frames with widget controls.
;               
; Use         : standard movie widget mustcall from scc_playmovie and scc_playmoviem 
;
; Calls       : Can not be called directly
;
; Comments    : Widget controls for scc_playmovie, scc_playmoviem and wscc_combine_mvi.
;               
; Side effects: None.
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL Oct. 13 2009.
;               
;-            
;$Log: scc_movie_win.pro,v $
;Revision 1.180  2014/06/12 15:48:27  hutting
;added fits option to save frame
;
;Revision 1.179  2013/10/02 17:01:42  nathan
;change all cases of Ttime_obs to <sp>time_obs
;
;Revision 1.178  2013/08/23 20:20:25  nathan
;change default time_avg_diff output resolution
;
;Revision 1.177  2013/08/21 17:26:11  nathan
;time_avg_diff widget label change
;
;Revision 1.176  2013/08/21 15:40:04  nathan
;modify plot for track spot option
;
;Revision 1.175  2013/06/28 14:13:19  mcnutt
;changed mask_event to use convert2secchi for lasco images
;
;Revision 1.174  2013/06/12 16:36:58  nathan
;change default grid spacing for hi2 from 20 to 10
;
;Revision 1.173  2013/05/28 16:58:56  nathan
;change grid and axis labeling
;
;Revision 1.172  2012/11/28 17:26:01  mcnutt
;corrected error when restarting scc_playmovie after projection is applied
;
;Revision 1.171  2012/08/17 14:17:25  mcnutt
;correccted apply mask
;
;Revision 1.170  2012/07/31 14:44:19  mcnutt
;corrected masks on truecolor and looks for first character before . for secchi sc
;
;Revision 1.169  2012/07/10 16:05:19  mcnutt
;added stonyhusrt corr option
;
;Revision 1.168  2012/05/17 14:05:41  mcnutt
;enhanced crop options
;
;Revision 1.167  2012/05/10 15:27:26  nathan
;add MARGIN= and /LAST to scc_playmovie
;
;Revision 1.166  2012/05/08 12:32:57  mcnutt
;removed scc_draw_box use draw_box and add wait when cropping with auto advance
;
;Revision 1.165  2012/05/07 16:59:51  mcnutt
;added auto advance option when cropping
;
;Revision 1.164  2012/04/25 14:17:25  mcnutt
;added x and y size option for crop and zoom
;
;Revision 1.163  2012/02/09 17:35:35  nathan
;update default grid spacing; apply_spoke label
;
;Revision 1.162  2012/01/05 15:22:57  mcnutt
;box control brings command box to top if allready open or closes previously opened box and open new
;
;Revision 1.161  2012/01/04 20:38:18  mcnutt
;will not start a new box_control widget until previous on has been cancelled
;
;Revision 1.160  2011/12/01 17:02:36  nathan
;do not print if not unix
;
;Revision 1.159  2011/12/01 15:10:55  mcnutt
;changed cursor crosshair for cross-platform compatibility
;
;Revision 1.158  2011/12/01 14:26:45  mcnutt
;Use select_windows instead of set_plot for cross-platform compatibility
;
;Revision 1.157  2011/11/28 20:46:02  nathan
;fix plot_sync debug
;
;Revision 1.156  2011/11/28 20:17:17  nathan
;add CARRINGTON option for sync MVI frames
;
;Revision 1.155  2011/11/22 16:20:50  mcnutt
;passes wcshdrs to scc_playmovie and scc_playmoviem, adjusted all control window positions
;
;Revision 1.154  2011/11/17 13:44:44  mcnutt
;crop by coordinates no longer removes frames, frame movement stops after the coordinates pass the solar limb
;
;Revision 1.152  2011/11/14 20:09:23  mcnutt
;corrected start_ht for flatplane movies
;
;Revision 1.151  2011/11/14 17:30:11  mcnutt
;corrected ysize of coordinate crop movie
;
;Revision 1.150  2011/11/14 17:04:15  mcnutt
;corrected crop by coordinate
;
;Revision 1.149  2011/11/07 16:15:23  nathan
;define new crosshair cursor bitmap
;
;Revision 1.148  2011/10/26 19:57:36  mcnutt
;by default zoom will create a full size 2 X image
;
;Revision 1.147  2011/10/26 19:20:02  mcnutt
;separates the command window only if required by screen./movie size
;
;Revision 1.146  2011/10/26 18:23:00  mcnutt
;detached command bar from draw window
;
;Revision 1.145  2011/09/29 18:51:40  mcnutt
;calls exit_playmovie on exit
;
;Revision 1.144  2011/09/26 18:30:14  mcnutt
;added latplane movie grrid and measuring options
;
;Revision 1.143  2011/09/22 17:05:52  mcnutt
;corrected grid for rtheta movies
;
;Revision 1.142  2011/09/07 18:18:16  mcnutt
;redraws zoom before marking sync
;
;Revision 1.141  2011/09/07 12:44:58  mcnutt
;plot_sync marks zoom window if open
;
;Revision 1.140  2011/08/31 16:59:14  mcnutt
;changed zoom tool to work with box and added an option to save zoomed movie
;
;Revision 1.139  2011/08/25 20:04:52  mcnutt
;corrected sun center and edges of centered adjusted croped movies frames will run off edge
;
;Revision 1.138  2011/08/23 19:55:04  mcnutt
;corrected sun center and edges of centered adjusted croped movies
;
;Revision 1.137  2011/08/23 18:21:19  mcnutt
;corp movie corrected sun centers and good counter
;
;Revision 1.135  2011/08/22 22:57:33  nathan
;rewrite pro box_event to do different crop for each frame
;
;Revision 1.134  2011/08/05 16:41:21  mcnutt
;corrected changing units when ht file is open
;
;Revision 1.133  2011/08/05 15:40:42  mcnutt
;disallows changing units when ht file is open
;
;Revision 1.132  2011/06/10 17:57:12  nathan
;added do_smooth widget
;
;Revision 1.131  2011/05/24 19:49:40  nathan
;fix missing button_uvalue DELETE
;
;Revision 1.130  2011/05/20 14:48:56  nathan
;adjust widget labels
;
;Revision 1.129  2011/05/20 13:38:41  mcnutt
;added edit button to movie conrtrol page
;
;Revision 1.128  2011/05/18 15:50:07  mcnutt
;corrected truecolor imgs when calling scc_playmoviem
;
;Revision 1.127  2011/05/18 14:05:31  mcnutt
;updated rtheta values
;
;Revision 1.126  2011/05/17 21:52:01  nathan
;rename Remove Stars; fix save_frame input
;
;Revision 1.125  2011/05/12 15:37:40  mcnutt
;removed define_wcshdrs now its own pro
;
;Revision 1.124  2011/05/02 17:48:22  mcnutt
;changed spoke projection to use scccart2logpol
;
;Revision 1.123  2011/04/22 16:41:07  nathan
;desc menu typo?; print unknown event
;
;Revision 1.122  2011/04/20 22:01:08  nathan
;pretty up menu definitions; move savefile into scc_synoptic
;
;Revision 1.121  2011/04/20 19:22:02  nathan
;fix coords ]; move good defn to front of each pro
;
;Revision 1.120  2011/04/20 16:09:54  nathan
;use mvifindfits.pro; change scc_synoptic coords input format
;
;Revision 1.119  2011/03/31 19:08:55  mcnutt
;added times to time avg diff save file
;
;Revision 1.118  2011/03/31 19:06:38  mcnutt
;added time avg diff otion for euvi,eit and aia movies
;
;Revision 1.117  2011/03/31 15:37:47  nathan
;update info for user for track_spot
;
;Revision 1.116  2011/03/24 13:55:28  mcnutt
;added labels for stonyhurst maps
;
;Revision 1.115  2011/03/22 20:14:07  mcnutt
;added planet labels for euvi synoptic maps
;
;Revision 1.114  2011/03/22 18:56:12  mcnutt
;remove stars for all movie except euvi and eit
;
;Revision 1.113  2011/03/18 16:49:07  mcnutt
;added grip for synoptic movies
;
;Revision 1.112  2011/03/09 19:43:56  mcnutt
;add rtheta 3 and 4 for carrington/stonyhurst synoptic movie
;
;Revision 1.111  2010/12/01 19:52:25  mcnutt
;added option to save preview frames
;
;Revision 1.110  2010/11/29 20:01:47  nathan
;move help,fname; plane-project ptitle; cmt out widget_control, poptions
;
;Revision 1.109  2010/11/23 13:09:17  mcnutt
;improved deprojection with same sun center options
;
;Revision 1.108  2010/11/05 16:25:36  mcnutt
;removed errors in hi deproject
;
;Revision 1.107  2010/11/02 19:04:14  mcnutt
;sets sunxcen and sunycen for RTHETA movies to [theta(0),radius(0)]
;
;Revision 1.106  2010/11/02 14:55:23  mcnutt
;added grid for RTHETA movies
;
;Revision 1.105  2010/10/27 17:05:18  mcnutt
;corrected rtheta coords and add ylog option
;
;Revision 1.104  2010/10/20 16:47:34  mcnutt
;changes to rad_spoke projection now set sec_pix
;
;Revision 1.103  2010/10/15 19:50:48  nathan
;with modified drawcoordgrid, centers of a-b movies seems to be ok now
;
;Revision 1.102  2010/10/15 18:16:24  mcnutt
;correct osc to be right side image in define_wcshdrs and correct cancel in draw_grid
;
;Revision 1.101  2010/10/14 21:33:01  nathan
;tried to clarify A/B and left/right in define_wcshdrs, but still not working
;
;Revision 1.100  2010/10/14 19:00:55  nathan
;debug in plane_project call
;
;Revision 1.99  2010/10/14 17:53:50  mcnutt
;only get wcs hdr for current frame for previews
;
;Revision 1.97  2010/10/05 20:15:44  nathan
;Recognize non-fits frame names for generating wcs headers and planet positions;
;do not call label_planets,/cancel; silence hpc2hpr call
;
;Revision 1.96  2010/09/30 22:09:51  nathan
;update to use HR*-ARC projection WCS hdr if mvi_frame_hdr.filter=Deproject
;
;Revision 1.95  2010/09/28 15:01:04  nathan
;made a few corrections to wcshdrs after projection; allow Carrington for all
;
;Revision 1.94  2010/09/21 13:00:53  mcnutt
;corrected soho masks loads soho soice and set sc to SOHO
;
;Revision 1.93  2010/08/30 17:15:24  mcnutt
;corrected projection events
;
;Revision 1.92  2010/08/27 19:40:36  nathan
;get solr from wcs hdr if not defined
;
;Revision 1.91  2010/08/25 20:55:56  nathan
;fix get_stereo_lonlat call if time_obs=blank; rename projection widgetlabels
;
;Revision 1.90  2010/08/19 20:37:27  nathan
;do not read in FITS header if ahdr is FITS header
;
;Revision 1.89  2010/08/18 20:35:45  nathan
;use new keyword for fithead2wcs
;
;Revision 1.88  2010/08/18 17:58:17  mcnutt
;aded a deproject widget
;
;Revision 1.87  2010/08/17 22:59:33  nathan
;reset moviev vals in define_wcshdrs pro
;
;Revision 1.86  2010/08/17 16:08:30  nathan
;Changes in pro define_wcshdrs to use convert2secchi.pro
;
;Revision 1.85  2010/08/12 16:43:53  mcnutt
;added rtheta to scc_playmoviem units
;
;Revision 1.84  2010/08/06 18:00:21  mcnutt
;added radial spoke deproject option
;
;Revision 1.83  2010/07/13 16:23:40  mcnutt
;looks in fitsdir for fits files if defined when calling scc_playmoviem in define_wcshdrs
;
;Revision 1.82  2010/07/12 13:19:43  mcnutt
;fix typo in draw_grid carrington
;
;Revision 1.81  2010/07/06 19:54:55  nathan
;Do not undefine ev structures on exit; some updated messages
;
;Revision 1.80  2010/06/24 20:17:51  nathan
;Use widget_offset.pro to position child widgets; do not skip frames
;
;Revision 1.79  2010/06/23 12:22:15  mcnutt
;grid spacing options
;
;Revision 1.78  2010/06/22 19:20:04  mcnutt
;change to draw grid
;
;Revision 1.77  2010/06/22 18:37:42  mcnutt
;added draw grid widget
;
;Revision 1.76  2010/06/18 12:16:52  mcnutt
;added font selection to label planets
;
;Revision 1.75  2010/06/17 19:08:59  mcnutt
;removed SOHO from planet labels
;
;Revision 1.74  2010/06/17 18:42:21  mcnutt
;label plantes will work with LASCO movies
;
;Revision 1.73  2010/06/10 16:49:00  mcnutt
;moved label planets into its own widget window
;
;Revision 1.72  2010/06/09 16:14:52  mcnutt
;add label planets tool
;
;Revision 1.71  2010/06/01 21:43:33  nathan
;tweaked mask widget labels
;
;Revision 1.70  2010/06/01 19:03:13  mcnutt
;correced masks to work with soho movies
;
;Revision 1.69  2010/05/24 15:17:42  mcnutt
;draws frame with new color table if moviev.adjcolor eq 1
;
;Revision 1.68  2010/05/13 17:06:51  mcnutt
;creates name for gif and tiff files if movie name is not defined
;
;Revision 1.67  2010/05/12 13:26:41  mcnutt
;added save frame as tiff and decressed the size of y scroll bar
;
;Revision 1.66  2010/05/07 20:21:47  nathan
;Always define imgs in define_imgs, using pixmaps if necessary; use
;respond_widg.pro to halt for user response; added movie.stall and
;wset,moviev.draw_win in sadjust_ct; added debug msgs
;
;Revision 1.65  2010/04/23 14:18:41  nathan
;load colorseit.tbl from SSW_LASCO
;
;Revision 1.64  2010/04/20 15:20:27  mcnutt
;added scroll bar to hdr info to work with fits headers
;
;Revision 1.63  2010/03/31 18:06:56  mcnutt
;added view frame hdr tools will write a text file with frame header info
;
;Revision 1.62  2010/03/16 17:51:12  mcnutt
;sends keyword ab to sccwrite_ht if ab movie
;
;Revision 1.61  2010/03/11 14:18:21  mcnutt
;corrected labeling on carrington grid
;
;Revision 1.60  2010/03/08 22:22:35  nathan
;call sxcolors, not xcolors
;
;Revision 1.59  2010/03/03 18:05:06  mcnutt
;corrected grid to display on single image;+
;
;$Id: scc_movie_win.pro,v 1.180 2014/06/12 15:48:27 hutting Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_MOVIE_WIN
;               
; Purpose     : Widget tool to display animation sequence.
;               
; Explanation : This tool allows the user to view a series of images as
;		an animation sequence.  The user can control the direction,
;		speed, and number of frames with widget controls.
;               
; Use         : standard movie widget mustcall from scc_playmovie and scc_playmoviem 
;
; Calls       : Can not be called directly
;
; Comments    : Widget controls for scc_playmovie, scc_playmoviem and wscc_combine_mvi.
;               
; Side effects: None.
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL Oct. 13 2009.
;               

;
;Revision 1.58  2010/02/26 19:28:24  nathan
;fix gif message
;
;Revision 1.57  2010/02/04 22:27:33  nathan
;debug messages
;
;Revision 1.56  2010/02/04 17:49:35  mcnutt
;corrected timer stall in replacing moviev.base with moviev.winbase
;
;Revision 1.55  2010/02/04 17:43:22  mcnutt
;corrected timer stall in import sync
;
;Revision 1.54  2010/02/03 22:13:05  nathan
;Fix debug; define imgs if Adjust Color is selected; info message in define_imgs;
;toggle sync reverse; print Import Sync message.
;
;Revision 1.53  2010/02/03 15:58:42  mcnutt
;added radial grid option
;
;Revision 1.52  2010/02/02 23:51:32  nathan
;define_wcshdrs BEFORE realizing the widget
;
;Revision 1.51  2010/01/26 19:31:22  mcnutt
;added wset to read img if imgs are not defined in define_wcshdrs
;
;Revision 1.50  2010/01/26 19:15:14  mcnutt
;sends issoho keyword to matchfitshdr2mvi in define_wcshdrs
;
;Revision 1.49  2010/01/22 18:19:21  mcnutt
;set osczero if imgs are not defined
;
;Revision 1.48  2010/01/22 13:13:56  mcnutt
;define_wcshdrs matches fhdr before defining ohdr
;
;Revision 1.47  2010/01/20 14:00:18  mcnutt
;sends wcs header to drawcoordgrid
;
;Revision 1.46  2010/01/20 13:51:28  mcnutt
;pass wcshdrs to scc_playmoviem and draws grid for ab movies
;
;Revision 1.45  2010/01/19 17:10:21  mcnutt
;correct define wcshdrs to work with B onleft or bottom
;
;Revision 1.44  2010/01/15 20:09:01  mcnutt
;corrected wcshdrs for ab movies and draw grid will now work with ab movies
;
;Revision 1.43  2010/01/15 18:57:08  nathan
;halt movie if adjust_ct
;
;Revision 1.42  2010/01/14 15:26:10  mcnutt
;defines wcs headers for other sc
;
;Revision 1.41  2010/01/11 21:56:16  nathan
;only updates color table on all frames when Accept button is pressed
;
;Revision 1.40  2009/12/15 14:53:38  mcnutt
;added print frame option
;
;Revision 1.39  2009/12/11 17:54:50  mcnutt
;corrected add mask to work if imag_hdrs are the fits headers not mvi frame headers
;
;Revision 1.38  2009/12/09 17:23:44  mcnutt
;added mask option
;
;Revision 1.37  2009/12/08 16:15:11  mcnutt
;corrected remove sync and removed some add_tags
;
;Revision 1.36  2009/12/07 19:58:13  mcnutt
;remove sync only redraws image without sync mark does rmove tag sync_point
;
;Revision 1.35  2009/11/20 19:51:53  mcnutt
;cleanup up carrington longitude grid
;
;Revision 1.34  2009/11/19 16:22:54  mcnutt
;updates sync_point
;
;Revision 1.33  2009/11/18 20:20:15  mcnutt
;correct plot_sync by adding common block scc_playmv_COMMON
;
;Revision 1.32  2009/11/18 19:30:04  mcnutt
;moved sync control to movie control page
;
;Revision 1.31  2009/11/18 14:51:54  mcnutt
;Added remove star option for COR2 and his
;
;Revision 1.30  2009/11/18 14:19:42  mcnutt
;Does not include deproject option if filter is = deproject
;
;Revision 1.29  2009/11/18 13:54:37  mcnutt
;removed fname test line
;
;Revision 1.28  2009/11/17 14:29:45  mcnutt
;added deproject option for COR2 movies
;
;Revision 1.27  2009/11/12 17:16:57  mcnutt
;added pdraw carrington grid option for EUVI and EIT
;
;Revision 1.26  2009/11/09 16:40:44  mcnutt
;call scc_playmovie or playmoviem after deproject with wcshdrs, corrected motion event for scc_playmoviem
;
;Revision 1.25  2009/11/06 19:47:17  mcnutt
; tracks spot and crops by pixels and coords
;
;Revision 1.24  2009/11/06 13:40:52  mcnutt
;corrected plot_sync calls plot_sync when movie frame changes
;
;Revision 1.23  2009/11/02 19:53:59  mcnutt
;removed test line
;
;Revision 1.22  2009/11/02 19:53:12  mcnutt
;added pixel option to track_spot and hi_point to define_wcshdrs
;
;Revision 1.21  2009/10/29 16:09:00  nathan
;in define_imgs, use moviev.frames instead of file_hdr.nf; comment out ismoviem=1
;
;Revision 1.20  2009/10/21 18:54:44  nathan
;use define_widget_sizes.pro (in nrlgen)
;
;Revision 1.19  2009/10/21 18:30:57  nathan
;modified after using MacOSX at 800x600 resolution; adjust y_scroll_size for moviem
;
;Revision 1.18  2009/10/21 18:18:28  nathan
;added wsizes structure for holding widget and window feature sizes
;
;Revision 1.17  2009/10/21 17:49:08  mcnutt
;removed motion_events from draw window if not playmoviem
;
;Revision 1.16  2009/10/21 17:18:19  mcnutt
;removed edit column and added control button
;
;Revision 1.15  2009/10/21 17:17:53  nathan
;fix scroll_size args
;
;Revision 1.14  2009/10/21 16:13:41  nathan
;update widget_draw(winbase) call
;
;Revision 1.13  2009/10/21 14:39:28  mcnutt
;added widget box_control for crop and track spot
;
;Revision 1.12  2009/10/21 12:59:36  mcnutt
;removed define_imgs option and will define_images if needed
;
;;----------------define imgs --------------------------------------

PRO define_imgs,filename

COMMON scc_playmv_COMMON, moviev

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

common SCC_COMBINE, cmbmovie,cmbstate,struct 

;if (moviev.edit eq 0 )then $
;    var=respond_widg(message=['WARNING:', $
;    	    	'SCC_PLAYMOVIE was called with /NOXCOLORS', $
;		'or GENERIC_MOVIE was called without /DOXCOLORS.', $
;		'Defining imgs array from pixmaps.'],button='OK',title='SCC_MOVIE_WIN MSG',/column) $
;else if datatype(findx) eq 'UND' then $
;    var=respond_widg(message=[ 'WARNING: SCC_PLAYMOVIEM did not define findx', $
;    	    	'Defining imgs array from pixmaps.'],title='SCC_MOVIE_WIN MSG' ,button='OK',/column)
;ELSE BEGIN   
    IF moviev.filename EQ '' THEN BEGIN
    	print,'Defining imgs array from pixmaps.'
	if moviev.truecolor lt 1 then imgs=bytarr(moviev.hsize,moviev.vsize,moviev.len) else imgs = bytarr(3,moviev.hsize,moviev.vsize,moviev.len)
    ENDIF ELSE BEGIN
	BREAK_FILE, moviev.filename, a, dir, name, ext
    	print,'Reading in ',name+ext,' to define imgs array.'
    	if strpos(moviev.filename,'.hdr') gt -1 then pngmvi=moviev.filename else pngmvi='0'
    	OPENR, lu1, moviev.filename,/get_lun
    	SCCREAD_MVI, lu1, file_hdr1, ihdrs1, imgs1, swapflag,  pngmvi=pngmvi
    	if file_hdr1.truecolor lt 1 then imgs = bytarr(file_hdr1.nx, file_hdr1.ny, file_hdr1.nf) else imgs = bytarr(3,file_hdr1.nx, file_hdr1.ny, file_hdr1.nf)
    ENDELSE
    FOR i=0,n_elements(moviev.frames)-1 DO BEGIN
    	IF moviev.filename EQ '' THEN BEGIN
	    ;read from pixmaps
    	    wset,moviev.win_index[i]
	    if moviev.truecolor lt 1 then imgs[*,*,i]=tvrd() else imgs[*,*,*,i]=tvrd(true=1)
	ENDIF ELSE $
	 if (pngmvi ne '0') THEN begin  
              ; imgs1 is an array of filenames
	      filein=string(imgs1(findx[i]))
	      if strpos(filein,'.png') gt -1 then imgt=read_png(dir+string(imgs1(findx[i])),r,g,b)
	      if strpos(filein,'.gif') gt -1 then read_gif,dir+string(imgs1(findx[i])),imgt,r,g,b
	      IF (file_hdr1.truecolor EQ 0) then imgs(*,*,i)=imgt else begin
                imgi=bytarr(3,n_Elements(imgt(*,0)),n_Elements(imgt(0,*)))
                imgs(0,*,*,i)=r(imgt) & imgs(1,*,*,i)=g(imgt) & imgs(2,*,*,i)=b(imgt)
	      ENDELSE
         ENDIF ELSE BEGIN
	   IF (file_hdr1.truecolor EQ 0) THEN imgs(*,*,i)=imgs1[*,*,findx[i]] ELSE imgs(*,*,*,i)=imgs1[*,*,*,findx[i]]
         ENDELSE
     ENDFOR
     print,'Done.'
;endelse

END


;----------------adjust color-------------------------------
PRO sADJUST_CT, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON

if moviev.truecolor eq 1 then popup_msg,['Can not adjust color of a Truecolor movie'],title='SCC_MOVIE MSG' $
ELSE BEGIN
  moviev.adjcolor=1
  input = Tag_Names(ev, /Structure_Name)
  if input eq '' then input = ev.value
;help,/str,ev

  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
  print,"sADJUST_CT: " + input

  CASE (input) OF

      'WIDGET_BUTTON': BEGIN
    	    WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
    	    moviev.stall = 10000.
   	;window,1,xsize=512,ysize=512
	colorseit=filepath('colorseit.tbl',root_dir=getenv('SSW_LASCO'),subdir=['idl','nrleit','display'])
        
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(1),moviev.baseyoff(1),newsize=[265,430]
    	IF (moviev.debugon) then help,moviev.basexoff(1),moviev.baseyoff(1)
        sXColors, Group_Leader=ev.top, NotifyID=[ev.id,ev.top],file=colorseit,xoffset=moviev.basexoff(1), yoffset=moviev.baseyoff(1)
	
      END
      'XCOLORS_LOAD': BEGIN
          Device, Get_Visual_Depth=thisDepth
        ;IF thisDepth GT 8 THEN BEGIN
	  wset,moviev.draw_win
	  tv, imgs[*,*,moviev.current]
	  ;image = tvrd(true=1)
	  ;DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
      ;ENDIF
      END
    'XCOLORS_ACCEPT': BEGIN
    	winsave = !d.window
    	nim = n_elements(moviev.win_index)
	print,"Please stand by while ",trim(nim)," frames are re-drawn..."
    	for i = 0, nim-1 do begin
    	    wset,moviev.win_index(i)
    	    tv, imgs[*,*,i]
    	endfor
    	print,'Done with xcolors.pro'
    	WSET,winsave
        WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall
        moviev.adjcolor=0
    END	    
  ENDCASE
ENDELSE
END

;---------------Save frame----------------------------------
PRO SCC_SAVE_FRAME_EVENT, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine

    WIDGET_CONTROL, ev.id, GET_UVALUE=input

    IF (DATATYPE(input) EQ 'STR') THEN BEGIN

      winsav=!d.window

      if ev.top eq moviev.boxbase and moviev.boxsel eq 'zoom' then WSET, moviev.zstate.zoom_win
      BREAK_FILE, moviev.filename, a, dir, name, ext
      if name eq '' then name='scc_movie_frame'

      IF (debug_on) THEN BEGIN
    	help,/str,moviev
	print,'In SCC_SAVE_FRAME_EVENT.pro, '
	help,input
	wait,3
      ENDIF
      BREAK_FILE, moviev.filename, a, dir, name, ext
      if name eq '' then name='scc_movie_frame'

      CASE strlowcase(input) OF

	'gif'  : junk=fTVREAD(/gif, filename=name)
	'png'  : junk=fTVREAD(/png, filename=name)
	'tiff' : junk=fTVREAD(/tiff, filename=name)
	'pict' : junk=fTVREAD(/pict, filename=name)
	'jpeg' : junk=fTVREAD(/jpeg, filename=name)
	'bmp'  : junk=fTVREAD(/BMP, filename=name)
        'fts'  : begin
	        	if datatype(imgs) ne 'BYT' then img=tvrd() else img=imgs[*,*,moviev.current]
			thdr = convert2secchi(moviev.img_hdrs[moviev.current],moviev.file_hdr)
                        if name eq 'scc_movie_frame' then begin
			  BREAK_FILE, thdr.filename, a, dir, name, ext
                          if strlen(name) ge 16 then strput,name,'mvf',16 else name=name+'_mvf'
                        endif
			thdr.filename=name+'.fts'
                        ihdr = struct2fitshead(thdr,/allow_crota,/dateunderscore)
			writefits,thdr.filename,img,ihdr
	         end
 	ELSE : BEGIN
                 PRINT, '%%SCC_SAVE_FRAME.  Unknown event ',input
                 return
	     END

      ENDCASE

      message, 'Saved frame to '+input+' file: '+ name,/info
      wset,winsav
    ENDIF

END 

;---------------Save frame----------------------------------

;----------------Zoom --------------------------------------

PRO sccSHOW_ZOOM

COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON

   if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
   IF (debug_on) THEN help,imgs
   if datatype(imgs) eq 'BYT' then begin
     device,get_screen_size=ss
   IF (debug_on) THEN help,/str,moviev
      IF (moviev.showzoom NE 1) THEN WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
      IF (moviev.showzoom EQ 0) or moviev.zoombase eq -1 THEN BEGIN
	moviev.zstate.x_zm_sz=abs(moviev.zstate.x1-moviev.zstate.x0)*moviev.zstate.scale
        moviev.zstate.y_zm_sz=abs(moviev.zstate.y1-moviev.zstate.y0)*moviev.zstate.scale
        if moviev.zstate.x_zm_sz gt 1024 or moviev.zstate.y_zm_sz gt 1024 then xyscroll=[moviev.zstate.x_zm_sz<1025,moviev.zstate.y_zm_sz<1025] else xyscroll=[0,0]
        moviev.zoombase = WIDGET_BASE(/COLUMN, TITLE='SCC_ZOOM')
        moviev.zoom_w = WIDGET_DRAW(moviev.zoombase, /BUTTON_EVENTS, /MOTION_EVENTS, /FRAME, RETAIN = 2, XSIZE=moviev.zstate.x_zm_sz, YSIZE=moviev.zstate.y_zm_sz, $
                                                  X_SCROLL_SIZE=xyscroll[0], Y_SCROLL_SIZE=xyscroll[1] ,EVENT_PRO=moviev.evtpro) 

        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(1),moviev.baseyoff(1),NEWBASE=moviev.zoombase
    	IF (moviev.debugon) then help,moviev.basexoff(1),moviev.baseyoff(1)
    	WIDGET_CONTROL, moviev.zoombase,xoffset=moviev.basexoff(1), yoffset=moviev.baseyoff(1)
        WIDGET_CONTROL, /REAL, moviev.zoombase
        moviev.showzoom = 1
        WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall
	draw_zoom
     ENDIF else IF (moviev.showzoom EQ 1) THEN BEGIN
         WIDGET_CONTROL, /DESTROY, moviev.zoombase
         moviev.zoombase=-1
	 moviev.showzoom = 0
     ENDIF
  ENDIF
  ;WIDGET_CONTROL, base, SET_UVALUE=moviev;  ,yoffset=0,xoffset=tmp(0)-tmp(0)/2.0

  RETURN
END

PRO draw_zoom, zuse=zuse , zoomimgout=zoomimgout

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
; zuse is IDL position +1 in order to set keyword for position 0
  if keyword_set(zuse) then zuse=zuse-1 else zuse=moviev.current
  if moviev.file_hdr.truecolor le 0 then orig_image=imgs(*,*,zuse) else orig_image=imgs(*,*,*,zuse)


  ; compute size of rectangle in original image
  ; round up to make sure image fills zoom window
  rect_x = long(moviev.zstate.x_zm_sz / float(moviev.zstate.scale) + 0.999)
  rect_y = long(moviev.zstate.y_zm_sz / float(moviev.zstate.scale) + 0.999)

  ; Plan to erase if the zoom rectangle is larger than the original
  ;  image size.
  doerase = (rect_x GT moviev.zstate.x_im_sz OR rect_y GT moviev.zstate.y_im_sz)

  rect_x = rect_x < moviev.zstate.x_im_sz
  rect_y = rect_y < moviev.zstate.y_im_sz

  ; compute location of origin of rect (user specified center)
  x0 = moviev.zstate.xc - rect_x/2
  y0 = moviev.zstate.yc - rect_y/2

  ; make sure rectangle fits into original image
  ;left edge from center
  x0 = x0 > 0
  ; limit right position
  x0 = x0 < (moviev.zstate.x_im_sz - rect_x)

  ;bottom
  y0 = y0 > 0
  y0 = y0 < (moviev.zstate.y_im_sz - rect_y)

  ;Save window number
  save_win = !D.WINDOW
  WSET, moviev.zstate.zoom_win

  IF (moviev.zstate.scale EQ 1) THEN BEGIN
    IF doerase THEN ERASE

    ; don't use tvscl here, to preserve same range as in unzoomed image
    if moviev.file_hdr.truecolor le 0 then TV, orig_image[x0:x0+rect_x-1,y0:y0+rect_y-1] else $
       TV, orig_image[*,x0:x0+rect_x-1,y0:y0+rect_y-1],true=1

  ENDIF ELSE BEGIN
    ;Make integer rebin factors.  These may be larger than the zoom image
    dim_x = rect_x * moviev.zstate.scale
    dim_y = rect_y * moviev.zstate.scale

    ; Constrain upper right edge to within original image.
    x1 = (x0 + rect_x - 1) < (moviev.zstate.x_im_sz-1)
    y1 = (y0 + rect_y - 1) < (moviev.zstate.y_im_sz-1)

    if moviev.file_hdr.truecolor le 0 then temp_image = rebin(orig_image[x0:x1,y0:y1], $
                            	    	    	    	    	dim_x, dim_y, $
                            	    	    	    	    	sample=moviev.zstate.sample)

    if moviev.file_hdr.truecolor eq 1 then begin
       temp_image = bytarr( 3,dim_x, dim_y)        
       for iz=0,2 do begin
           temp_img1=reform(orig_image(iz,x0:x1,y0:y1))
           temp_img2=rebin(temp_img1,dim_x, dim_y,sample=moviev.zstate.sample)
           temp_image(iz,*,*) = temp_img2
        endfor
    endif

    ;Save the zoomed image
    fill_x = dim_x < moviev.zstate.x_zm_sz
    fill_y = dim_y < moviev.zstate.y_zm_sz
    if moviev.file_hdr.truecolor le 0 then begin
      zoom_image=bytarr(moviev.zstate.x_zm_sz,moviev.zstate.y_zm_sz)
      zoom_image[0:fill_x-1,0:fill_y-1] = temp_image[0:fill_x-1,0:fill_y-1]
      ; Pad with zoomed image with black if necessary.
      if (fill_x LT moviev.zstate.x_zm_sz) then $
        zoom_image[fill_x:moviev.zstate.x_zm_sz-1, *] = 0
      if (fill_y LT moviev.zstate.y_zm_sz) then $
        zoom_image[*, fill_y:moviev.zstate.y_zm_sz-1] = 0
    endif else begin
      zoom_image=bytarr(3,moviev.zstate.x_zm_sz,moviev.zstate.y_zm_sz)
      zoom_image[*,0:fill_x-1,0:fill_y-1] = temp_image[*,0:fill_x-1,0:fill_y-1]
      ; Pad with zoomed image with black if necessary.
      if (fill_x LT moviev.zstate.x_zm_sz) then $
        zoom_image[*,fill_x:moviev.zstate.x_zm_sz-1, *] = 0
      if (fill_y LT moviev.zstate.y_zm_sz) then $
        zoom_image[*,*, fill_y:moviev.zstate.y_zm_sz-1] = 0
   endelse    

    ;Save the corners in original image
    moviev.zstate.x0 = x0
    moviev.zstate.y0 = y0
    moviev.zstate.x1 = x1
    moviev.zstate.y1 = y1

    ;Display the new zoomed image
    ;Save window number

    ; don't use tvscl here, to preserve same range as in unzoomed image
    TV, zoom_image,true=moviev.file_hdr.truecolor

 ENDELSE

if keyword_set(zoomimgout) then zoomimgout=zoom_image
 ;Restore window
 WSET, save_win


END
;;-----------------END ZOOM----------------------------------------------------


;----------------Objects widget --------------------------------------
pro object_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN help,input
    CASE (input) OF
        'obj0' : BEGIN
	               moviev.objectv(0)=ev.value
		END
        'obj1' : BEGIN
	               moviev.objectv(1)=ev.value
		END
        'obj2' : BEGIN
	               moviev.objectv(2)=ev.value
		END
         'objsz' : BEGIN
	               moviev.objectv(3)=ev.value
		END
         'objcl' : BEGIN
	               moviev.objectv(4)=ev.value
		END

         'SOPREV': BEGIN              ; 
    	    	  ;IF moviev.debugon THEN stop
                  if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs,/current,wcs=wcsi else begin
		     wcsi=moviev.wcshdrs(moviev.current)
                     if total(moviev.osczero) gt 0 then wcsi=[wcsi,moviev.wcshdrs(moviev.current+n_elements(moviev.img_hdrs))]
                  endelse
		  ;if datatype(moviev.wcshdrs) ne 'UND' then begin
                  winsave=!D.WINDOW
		  sc=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)
                  if ~(TAG_EXIST(moviev, 'objects')) then begin
                     obhdrs=wcsi(0)
                     objects=get_object_coords(moviev.img_hdrs,obhdrs,sc)
                     ;moviev=add_tag(moviev,objects,'objects')
                  endif
                  if sc eq 'A' then osc='B' else osc='A'
                  if n_elements(wcsi) eq 2 then begin
                    if ~(TAG_EXIST(moviev, 'objectosc')) then begin
                      obhdrs=wcsi(1)
                      objectosc=get_object_coords(moviev.img_hdrs,obhdrs,osc)
                      ;moviev=add_tag(moviev,objectosc,'objectosc')
                    endif
                  endif
                  
                   i=moviev.current
                   WSET,11
                   erase,0
		   DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                      if moviev.objectv(0) ne '' then xyouts,objects(0,i,0),objects(0,i,1),moviev.objectv(0),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      if moviev.objectv(1) ne '' then xyouts,objects(1,i,0),objects(1,i,1),moviev.objectv(1),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      if moviev.objectv(2) ne '' then xyouts,objects(2,i,0),objects(2,i,1),moviev.objectv(2),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      if n_elements(wcsi) eq 2 then begin
                          if moviev.objectv(0) ne '' then begin
			     sctxt=moviev.objectv(0)
                             lbl=strpos(sctxt,strlowcase(osc))
			     if lbl(0) gt -1 then strput,sctxt,strlowcase(sc),lbl
                             lbl=strpos(sctxt,osc)
			     if lbl(0) gt -1 then strput,sctxt,sc,lbl
			     xyouts,objectosc(0,i,0)+moviev.osczero(0),objectosc(0,i,1)+moviev.osczero(1),sctxt,charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
			  endif
                          if moviev.objectv(1) ne '' then xyouts,objectosc(1,i,0)+moviev.osczero(0),objectosc(1,i,1)+moviev.osczero(1),moviev.objectv(1),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                          if moviev.objectv(2) ne '' then xyouts,objectosc(2,i,0)+moviev.osczero(0),objectosc(2,i,1)+moviev.osczero(1),moviev.objectv(2),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      endif
                   WSET, moviev.draw_win
                   DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, 11]
                   WSET,11
                   erase,0
                   WSET, winsave
                   return
		END                

         'SODONE': BEGIN              ; 
    	    	  ;IF moviev.debugon THEN stop
                  if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs
                  ;if datatype(moviev.wcshdrs) ne 'UND' then begin
                  winsave=!D.WINDOW
		  sc=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)
                  if ~(TAG_EXIST(moviev, 'objects')) then begin
                     obhdrs=moviev.wcshdrs(0:n_elements(moviev.img_hdrs)-1)
                     objects=get_object_coords(moviev.img_hdrs,obhdrs,sc)
                     moviev=add_tag(moviev,objects,'objects')
                  endif
                  if sc eq 'A' then osc='B' else osc='A'
                  if n_elements(moviev.wcshdrs) eq n_Elements(moviev.img_hdrs)*2 then begin
                    if ~(TAG_EXIST(moviev, 'objectosc')) then begin
                      obhdrs=moviev.wcshdrs(n_elements(moviev.img_hdrs):n_elements(moviev.wcshdrs)-1)
                      objectosc=get_object_coords(moviev.img_hdrs,obhdrs,osc)
                      moviev=add_tag(moviev,objectosc,'objectosc')
                    endif
                  endif

                   FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                      if moviev.edit then begin
                          set_plot,'z'
                          device,set_resolution=[moviev.hsize,moviev.vsize]
		          IF(n_elements(imgs) GT 1) THEN  img = imgs[*,*,i]
                          tv,img
   		      endif else WSET,moviev.win_index(i)
                      if moviev.objectv(0) ne '' then xyouts,moviev.objects(0,i,0),moviev.objects(0,i,1),moviev.objectv(0),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      if moviev.objectv(1) ne '' then xyouts,moviev.objects(1,i,0),moviev.objects(1,i,1),moviev.objectv(1),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      if moviev.objectv(2) ne '' then xyouts,moviev.objects(2,i,0),moviev.objects(2,i,1),moviev.objectv(2),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      if n_elements(moviev.wcshdrs) eq n_Elements(moviev.img_hdrs)*2 then begin
                          if moviev.objectv(0) ne '' then begin
			     sctxt=moviev.objectv(0)
                             lbl=strpos(sctxt,strlowcase(osc))
			     if lbl(0) gt -1 then strput,sctxt,strlowcase(sc),lbl
                             lbl=strpos(sctxt,osc)
			     if lbl(0) gt -1 then strput,sctxt,sc,lbl
			     xyouts,moviev.objectosc(0,i,0)+moviev.osczero(0),moviev.objectosc(0,i,1)+moviev.osczero(1),sctxt,charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
			  endif
                          if moviev.objectv(1) ne '' then xyouts,moviev.objectosc(1,i,0)+moviev.osczero(0),moviev.objectosc(1,i,1)+moviev.osczero(1),moviev.objectv(1),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                          if moviev.objectv(2) ne '' then xyouts,moviev.objectosc(2,i,0)+moviev.osczero(0),moviev.objectosc(2,i,1)+moviev.osczero(1),moviev.objectv(2),charsize=float(moviev.objectv(3)),color=fix(moviev.objectv(4)),/device
                      endif
                      if moviev.edit then begin
                           newimg=tvrd ()
                           select_windows ;set_plot,'x'
                           WSET,moviev.win_index(i)
	                   tv, newimg
                           IF(n_elements(imgs) GT 1) THEN imgs[*,*,i] = newimg
   		      endif
                  endfor
                   WSET, winsave
                   WIDGET_CONTROL, /DESTROY, moviev.sobase
                   WIDGET_CONTROL, /DESTROY, moviev.solead
    	    	    ;IF moviev.debugon THEN stop
                   return
		END                


          'SOCANCEL': BEGIN              ; 
                   WIDGET_CONTROL, /DESTROY, moviev.sobase
                   WIDGET_CONTROL, /DESTROY, moviev.solead
                  return
		END                
 



	ELSE : BEGIN
                 PRINT, '%%object_event.  Unknown event ',input
	     END

    ENDCASE

END 

pro select_objects
COMMON scc_playmv_COMMON


        solead = Widget_Base(Map=0)
        Widget_Control, solead, /Realize
	
	sobase = WIDGET_BASE(/COL, TITLE='SELECT OBJECTS',group_leader=solead,/modal)
	rowa = WIDGET_BASE(sobase, /ROW, /FRAME)
 	tmp  = WIDGET_LABEL( rowa,value='Objects set to blank strings will not be labeled')

	rowb = WIDGET_BASE(sobase, /ROW)
	col1 = WIDGET_BASE(rowb, /COLUMN)
	col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)

; use CW_FIELD function
       sc=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)
       if sc eq 'A' then moviev.objectv(0)='B' else moviev.objectv(0)='A'
        moviev.sobase=sobase
        moviev.solead=solead

	obj0 =   CW_FIELD(col2, VALUE=moviev.objectv(0), /ALL_EVENTS, UVALUE='obj0' , XSIZE=15, YSIZE=1, title='SECCHI-'+moviev.objectv(0))
	obj1 =   CW_FIELD(col2, VALUE=moviev.objectv(1), /ALL_EVENTS, UVALUE='obj1' , XSIZE=15, YSIZE=1, title='EARTH   ')
	obj2 =   CW_FIELD(col2, VALUE=moviev.objectv(2), /ALL_EVENTS, UVALUE='obj2' , XSIZE=15, YSIZE=1, title='SOHO    ')

	col3 = WIDGET_BASE(col1, /COLUMN, /FRAME)

	objsz =   CW_FIELD(col3, VALUE=moviev.objectv(3), /ALL_EVENTS, UVALUE='objsz' , XSIZE=15, YSIZE=1, title='Size   ')
	objcl =   CW_FIELD(col3, VALUE=moviev.objectv(4), /ALL_EVENTS, UVALUE='objcl' , XSIZE=15, YSIZE=1, title='Color  ')

	rowc = WIDGET_BASE(sobase, /ROW)
	blk= '          '
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['SOPREV','SODONE','SOCANCEL'])

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '0\gif\SCC_SAVE_FRAME_EVENT', '2\fts\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)

	
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=sobase
    	IF (moviev.debugon) then help,moviev.basexoff(0),moviev.baseyoff(0)
    	WIDGET_CONTROL, sobase,xoffset=moviev.basexoff(0), yoffset=moviev.baseyoff(0)

	WIDGET_CONTROL, sobase, /REALIZE

	XMANAGER, 'select_objects', sobase, EVENT_HANDLER='object_event'
	

end
;**-----------------End object widget ------------------**

;----------------Planet widget --------------------------------------
pro label_planets,cancel=cancel
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON planets_select, planetstr

    	    	   if datatype(imgs) ne 'BYT' and ~keyword_set(cancel) then define_imgs,moviev.filename
		   
                   winsave=!D.WINDOW
                   WINDOW, XSIZE = moviev.file_hdr.nx, YSIZE = moviev.file_hdr.ny, /PIXMAP, /FREE
                   gridw = !D.WINDOW
                   sc=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)
                   uplanets=planetstr.planets
                   unames=planetstr.names
		   i2=0
		   noplanets=0
                   det=moviev.img_hdrs(0).detector
                   dindx=where(['C2','C3','EIT'] EQ det,issoho)
                   if issoho then begin
                     load_soho_spice 
		     sc='SOHO'
                   endif
                   fstart=moviev.current
		   fend=moviev.current
                   if planetstr.nf le 0 then begin
                       if moviev.file_hdr.rtheta eq 0 and moviev.flatplane eq 0 then begin
                         if ~(TAG_EXIST(moviev, 'wcshdrs')) and ~keyword_set(cancel) then define_wcshdrs,/current,wcs=wcsi else begin
		           wcsi=moviev.wcshdrs(moviev.current)
                           if total(moviev.osczero) gt 0 then wcsi=[wcsi,moviev.wcshdrs(moviev.current+n_elements(moviev.img_hdrs))]
                         endelse
		       endif
                       fstart=moviev.current
		       fend=moviev.current
		   endif else begin
                       if ~(TAG_EXIST(moviev, 'wcshdrs')) and ~keyword_set(cancel) and moviev.file_hdr.rtheta eq 0 and moviev.flatplane eq 0 then define_wcshdrs
                       fstart=0
		       fend=planetstr.nf-1
		   endelse
                   if ~keyword_set(cancel) then if total(moviev.osczero) gt 0 then i2=1 else i2=0 
		   if i2 eq 1 then begin
		      xlen=moviev.file_hdr.nx-(moviev.osczero(0))
		      ylen=moviev.file_hdr.ny-(moviev.osczero(1))
                      onames=unames
                      oplanets=uplanets
                      scl=unames(9)
                      if where(oplanets eq 'STEREO AHEAD') gt -1 then oplanets(9)='STEREO BEHIND' else oplanets(9)='STEREO AHEAD'
		      if strupcase(sc) eq 'A' then osc='B' else osc='A'
		      if strpos(scl,strupcase(osc)) gt -1  then strput,scl,sc,strpos(scl,osc)
                      if strpos(scl,strlowcase(osc)) gt -1  then strput,scl,strlowcase(sc),strpos(scl,strlowcase(osc))
                      onames(9)=scl
		   endif else begin
		      xlen=moviev.file_hdr.nx
		      ylen=moviev.file_hdr.ny
                  endelse
		   FOR i=fstart,fend DO BEGIN 
                       print,'Labeling Planets for Frame #',' ',string(i)
 		       wset,gridw
		       erase,0
                       if planetstr.nf gt -1 and ~keyword_set(cancel) then begin
                         for i3=0,i2 do begin
                           if moviev.file_hdr.rtheta eq 0 and moviev.flatplane eq 0 then begin
                             if TAG_EXIST(moviev, 'wcshdrs') then wcs=moviev.wcshdrs(i+moviev.len*i3) else wcs=wcsi(i3)
                           endif
                           usc=strupcase(sc)
                           
                           if i3 eq 1 then begin
                             usc=strupcase(osc)
                             xzero=moviev.osczero(0)
			     yzero=moviev.osczero(1)
                             planets=oplanets(where(planetstr.names ne ''))
			     names=onames(where(planetstr.names ne ''))
			   endif else begin
                             xzero=5 & yzero=5 & planets=uplanets(where(planetstr.names ne '')) & names=unames(where(planetstr.names ne ''))
                             if moviev.file_hdr.rtheta eq 3 then begin
                               xzero=-2 & yzero=-2
			     endif                            
			   endelse
                           if moviev.debugon then begin
                             print,usc,i+moviev.len*i3,xzero,yzero,xlen,ylen,'  ',moviev.img_hdrs(i).date_obs+'T'+moviev.img_hdrs(i).time_obs,'  ',wcs.cunit[0]
			   endif

                           for iplanet = 0,n_elements(planets)-1 do begin
                              if moviev.file_hdr.rtheta eq 0 and moviev.flatplane eq 0 then begin
                                lonlat = get_stereo_lonlat(moviev.img_hdrs(i).date_obs+' '+moviev.img_hdrs(i).time_obs, usc, $
                                system='HPC', target=planets[iplanet], /deg)
                                lonlat = lonlat[1:2]
			        if moviev.debugon THEN print,'hpc lonlat=',lonlat
		    	    	  IF wcs.projection EQ 'ARC' THEN BEGIN
    	    	    	    	    lonlat=hpc2hpr(lonlat,/degrees,silent=~moviev.debugon)
				    lonlat[1]=lonlat[1]-90
				    if moviev.debugon then print,'hpr lonlat=',lonlat
			    	; WCS does not handle distance from sun center directly for ARC (HPR) projection
				  ENDIF
                                if wcs.cunit[0] eq 'arcsec' then lonlat = lonlat * 3600.
                                pixel = wcs_get_pixel(wcs, lonlat)
;  If the planet is within the image, then mark it and label it.
                              	  if moviev.debugon then print,names[iplanet],' at pixel ',pixel
    	    	    	    	  if moviev.debugon then wait,2
                              endif
                              if moviev.file_hdr.rtheta eq 3 then begin
                                  lonlat = get_stereo_lonlat(moviev.img_hdrs(i).date_obs+' '+moviev.img_hdrs(i).time_obs, planets[iplanet], /au, /degrees, system='Carrington')
                                  lonlat = lonlat[1:2]
                                  pixel=[lonlat[0]-moviev.file_hdr.theta0,lonlat[1]-moviev.file_hdr.radius0]/moviev.file_hdr.sec_pix
                              endif
                             if moviev.file_hdr.rtheta eq 4 then begin
                                  lonlat = get_stereo_lonlat(moviev.img_hdrs(i).date_obs+' '+moviev.img_hdrs(i).time_obs, planets[iplanet], /au, /degrees, system='Carrington')
                                  elonlat = get_stereo_lonlat(moviev.img_hdrs(i).date_obs+' '+moviev.img_hdrs(i).time_obs, 'Earth', /au, /degrees, system='Carrington')
                                  lonlat = lonlat[1:2]
                                  lonlat(0)=lonlat(0)-elonlat(1) & if lonlat(0) gt 180 then lonlat(0)=lonlat(0)-360.
                                  pixel=[lonlat[0]-moviev.file_hdr.theta0,lonlat[1]-moviev.file_hdr.radius0]/moviev.file_hdr.sec_pix
                              endif
                              if (pixel[0] gt 0) and (pixel[0] lt xlen) and (pixel[1] gt 0) $
                                 and (pixel[1] lt ylen) then begin
                                name = names[iplanet]
                                nlength = strlen(name) * planetstr.plsize
                                istart = pixel[0]; + (xlen/512)*5
                                if istart lt 1 then istart = 1
                                   iend = istart + nlength
                                if iend gt xlen-1 then istart = (xlen-1) - nlength
                                jpos= pixel[1]; + (ylen/512)*5
                                print,name,'  x=',string(istart+xzero),'  y=',string(jpos+yzero)
                                xyouts, istart+xzero, jpos+yzero, planetstr.bfnt+name+'!X', color=255,/device,charsize=planetstr.plsize,font=planetstr.pfnt
                              endif
                           endfor              ;iplanet
                         endfor ;ab sides
                         pimg=tvrd()
                         if max(pimg eq 255) then begin
                          if moviev.file_hdr.truecolor eq 1 then begin
 		            img=imgs(*,*,*,i)
			    for nc = 0,2 do begin
                               timg=reform(imgs(nc,*,*,i))
		               timg(where(pimg eq 255))=planetstr.plclr
		               img(nc,*,*)=timg
                            endfor 
                            if planetstr.nf ne 0 then imgs(*,*,*,i)=img
			  endif else begin
 		            img=imgs(*,*,i)
                            img(where(pimg eq 255))=planetstr.plclr
			    if planetstr.nf ne 0 then imgs(*,*,i)=img
			  endelse
                        endif ELSE noplanets=1
			; no planets in FOV
			    
  		      endif else noplanets=1
		      	IF (noplanets) THEN BEGIN
		      	    message,'No planets in FOV.',/info
			    if moviev.file_hdr.truecolor eq 1 then img=imgs(*,*,*,i) else img=imgs(*,*,i)
    	    	    	ENDIF
		      wset, moviev.win_index[i]
		      
                      tv,img,true=moviev.truecolor
                      if i eq moviev.current then begin
                        WSET, moviev.draw_win
                        DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index[i]]
                      endif
                  ENDFOR              ;frames
                  wdel,gridw
                  wset,winsave
	     END

;----------------Planet widget --------------------------------------
pro planet_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON planets_select

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN help,input
   IF strpos(input,'hv!') gt -1 or strpos(input,'ps!') gt -1 THEN input='fontsel'

    CASE (input) OF
        'objsel' : BEGIN
                       nc=where(planetstr.objv eq ev.id)
	               planetstr.names(nc)=ev.value
		END
        'plsz' : planetstr.plsize=ev.value
        'plcl' : planetstr.plclr=ev.value
        'fontsel' : BEGIN
                      print,'Using font '+ev.value
	              planetstr.bfnt=strtrim(strmid(ev.value,strpos(ev.value,'!'),3),2)
	              if strpos(ev.value,'hv!') gt -1 then planetstr.pfnt=-1 else planetstr.pfnt=1 
      		END
         'SOPREV': BEGIN              ; 
    	    	  ;IF moviev.debugon THEN stop
                  planetstr.nf=0
                  label_planets
		END                

         'SODONE': BEGIN              ; 
    	    	  ;IF moviev.debugon THEN stop
                  planetstr.nf=n_elements(moviev.frames)
                  label_planets
                  WIDGET_CONTROL, /DESTROY, moviev.sobase
                  WIDGET_CONTROL, /DESTROY, moviev.solead
    	    	  ;IF moviev.debugon THEN stop
                  return
		END                


          'SOCANCEL': BEGIN              ; 
                   planetstr.nf=(-1)
                   ;label_planets,/cancel
                   WIDGET_CONTROL, /DESTROY, moviev.sobase
                   WIDGET_CONTROL, /DESTROY, moviev.solead
                  return
		END                

	ELSE : BEGIN
                 PRINT, '%%planet_event.  Unknown event ',input
	     END

    ENDCASE

END 

pro select_planets
COMMON scc_playmv_COMMON
COMMON planets_select

        solead = Widget_Base(Map=0)
        Widget_Control, solead, /Realize
	sobase = WIDGET_BASE(/COL, TITLE='SELECT PLANETS',group_leader=solead,/modal)
	rowa = WIDGET_BASE(sobase, /ROW, /FRAME)
 	tmp  = WIDGET_LABEL( rowa,value='Planets/Objects set to blank strings will not be labeled')

	rowb = WIDGET_BASE(sobase, /ROW)
	col1 = WIDGET_BASE(rowb, /COLUMN)
	col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)
        moviev.sobase=sobase
        moviev.solead=solead

         pfonts=['1\Font','1\Vector','0\hv!3 - Simplex Roman (default)','0\hv!5 - Duplex Roman','0\hv!6 - Complex Roman',$
	 '0\hv!8 - Complex Italic','0\hv!11 - Gothic English','0\hv!12 - Simplex Script',$
	 '0\hv!13 - Complex Script','0\hv!14 - Gothic Italian','0\hv!15 - Gothic German','0\hv!17 - Triplex Roman','2\hv!18 - Triplex Italic',$
	 '1\Post Script','0\ps!3 - Helvetica','0\ps!4 - Helvetica Bold','0\ps!5 - Helvetica Italic','0\ps!6 - Helvetica Bold Italic',$
         '0\ps!7 - Times','0\ps!8 - Times Italic','0\ps!11 - Courier','0\ps!12 - Courier Italic','0\ps!13 - Courier Bold','0\ps!14 - Courier Bold Italic',$
	 '0\ps!15 - Times Bold','0\ps!16 - Times Bold Italic']

; use CW_FIELD function
        det=moviev.img_hdrs(0).detector
        dindx=where(['C2','C3','EIT'] EQ det,issoho)
        if issoho then begin
           planets = ['Mercury', 'Venus', 'Earth', 'Mars', $
                                 'Jupiter Barycenter', 'Saturn Barycenter','URANUS BARYCENTER',$
				 'NEPTUNE BARYCENTER','PLUTO BARYCENTER']
           names = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn','Uranus','Neptune','Pluto']
        endif else begin
           sc=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)
           if sc eq 'A' then osc='BEHIND' else osc='AHEAD'
           planets = ['Mercury', 'Venus', 'Earth', 'Mars', $
                                 'Jupiter Barycenter', 'Saturn Barycenter','URANUS BARYCENTER',$
				 'NEPTUNE BARYCENTER','PLUTO BARYCENTER','STEREO '+osc];,'SOHO']
           names = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn','Uranus','Neptune','Pluto','ST-'+strmid(osc,0,1)];,'SOHO']
         endelse
        if moviev.file_hdr.rtheta ge 3 then begin
           planets = ['Earth','A','B']
           names = ['E','A','B']
        endif

            objv=intarr(n_Elements(names))
	for i=0,n_Elements(names)-1 do begin
	    label='         ' & strput,label,names(i),0
	    objv(i) = CW_FIELD(col2, VALUE=names(i), /ALL_EVENTS, UVALUE='objsel' , XSIZE=15, YSIZE=1, title=label)
        endfor
        ;if ~issoho then names(10)='' 
	col3 = WIDGET_BASE(col1, /ROW, /FRAME)
        plsize=2.
        plclr=255
	plsz =   CW_FIELD(col3, VALUE=plsize, /ALL_EVENTS, UVALUE='plsz' , XSIZE=5, YSIZE=1, title='Size:')
	plcl =   CW_FIELD(col3, VALUE=plclr, /ALL_EVENTS, UVALUE='plcl' , XSIZE=5, YSIZE=1, title='Color:')
        tmp = CW_PDMENU(col3, pfonts, /RETURN_NAME)


	rowc = WIDGET_BASE(sobase, /ROW)
	blk= '          '
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['SOPREV','SODONE','SOCANCEL'])

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)

        planetstr={planets:planets, names:names, objv:objv, plsize:plsize, plclr:plclr, nf:0, bfnt:'!3', pfnt:-1}
	
        
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(1),moviev.baseyoff(1),NEWBASE=sobase
    	IF (moviev.debugon) then help,moviev.basexoff(1),moviev.baseyoff(1)
    	WIDGET_CONTROL, sobase, xoffset=moviev.basexoff(1), yoffset=moviev.baseyoff(1)
	WIDGET_CONTROL, sobase, /REALIZE

	XMANAGER, 'select_planets', sobase, EVENT_HANDLER='planet_event'
	

end
;**-----------------End planet widget ------------------**

;----------------spoke widget --------------------------------------
pro spoke_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON spoke_common, spkim, spokev, spkbase, spklead,spkwin,spkimg

tmp = INDGEN(N_ELEMENTS(moviev.win_index))
good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
			    
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN BEGIN
    	help,/str,ev
	print,'In spoke_event.pro, '
	help,input
	wait,3
    ENDIF
    CASE (input) OF
        'spokesunx' : BEGIN
	               ;spokev(0)=ev.value
		END
        'spokesuny' : BEGIN
	               ;spokev(1)=ev.value
		END
        'spoker1' : BEGIN
	               spokev(2)=ev.value
		END
        'spoker2' : BEGIN
	               spokev(3)=ev.value
		END
        'spoketh1' : BEGIN
	               spokev(4)=ev.value
		END
        'spoketh2' : BEGIN
	               spokev(5)=ev.value
		END
        'spoken_r' : BEGIN
	               spokev(6)=ev.value
		END
        'spoken_th' : BEGIN
	               spokev(7)=ev.value
		END

        'spokeradscl' : BEGIN
	               spokev(9)=ev.index
		END

         'spkPREV': BEGIN              ; 
                  if spkim gt 0 then wdel,spkim-1
                  i=moviev.current
                  define_wcshdrs,/current,system='B',wcs=wcs
                  if spokev(9) eq 1 then linear=0 else linear=1
                  p=[ptr_new(imgs(*,*,i))]
                  ph=[ptr_new(wcs)]
                  newimg=scccart2logpol(p,ph,[spokev(7),spokev(6)],[spokev(2),spokev(3)],[spokev(4),spokev(5)],wcsout=wcsout,imelongminmax=imelongminmax,linear=linear)
                  newimg=byte(rotate(newimg,1))
                  window,xsize=spokev(7),ysize=spokev(6)
 		  tv,newimg
    	    	  spkim=!d.window              
    	    	  IF moviev.debugon THEN stop
         	END                

         'spkDONE': BEGIN              ; 
                       if datatype(spkimg) ne 'UND' then undefine,newimgt
   	    	       imgsnew=bytarr(spokev(7),spokev(6),N_ELEMENTS(moviev.win_index))
                       define_wcshdrs,system='B'
		       FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                           widget_control,moviev.instxt,set_value='Applying Radial Spoke to frame # '+string(i)
                           if spokev(9) eq 1 then linear=0 else linear=1
                           p=[ptr_new(imgs(*,*,i))]
                           ph=[ptr_new(moviev.wcshdrs(i))]
                           newimg=scccart2logpol(p,ph,[spokev(7),spokev(6)],[spokev(2),spokev(3)],[spokev(4),spokev(5)],wcsout=wcsout,imelongminmax=imelongminmax,linear=linear)
                           newimg=byte(rotate(newimg,1))
		    	   if spokev(7) ne moviev.file_hdr.nx or spokev(6) ne moviev.file_hdr.ny then imgsnew(*,*,i)=newimg else begin
                           	WSET,moviev.win_index(i)
	                     	tv, newimg
                             	imgs[*,*,i] = newimg
                           endelse
                        ENDFOR

			moviev.file_hdr.RTHETA = 1+spokev(9)
    	    	    	moviev.file_hdr.RADIUS0 = spokev(2)
    	    	    	moviev.file_hdr.RADIUS1 = spokev(3)
    	    	    	moviev.file_hdr.THETA0  = spokev(4)
    	    	    	moviev.file_hdr.THETA1  = spokev(5)
                        moviev.file_hdr.sunxcen=0.0  ;RADIUS0 pixel 
                        moviev.file_hdr.sunycen=0.0  ;THETA0 pixel 
                        moviev.img_hdrs.xcen=0.0  ;RADIUS0 pixel 
                        moviev.img_hdrs.ycen=0.0  ;THETA0 pixel 
     	    	    	moviev.file_hdr.nx=spokev(7)
			moviev.file_hdr.ny=spokev(6)

                       if spkim gt 0 then wdel,spkim-1
                       WIDGET_CONTROL, /DESTROY, spkbase
                       WIDGET_CONTROL, /DESTROY, spklead
                       moviev.instxt=0
		       if spokev(7) ne moviev.file_hdr.nx or spokev(6) ne moviev.file_hdr.ny then begin
		    	    HDRS=moviev.img_hdrs(good) 
		    	    NAMES=moviev.frames(good)
                    	    if (TAG_EXIST(moviev, 'wcshdrs')) then begin
			       wcshdrs=moviev.wcshdrs(good)
                    	       if n_elements(moviev.wcshdrs) eq moviev.len*2 then wcshdrs=[wcshdrs,moviev.wcshdrs(good+moviev.len)]
                            endif else wcshdrs=0
                    	    imgs = imgsnew(*,*,good)
                    	    osczero=moviev.osczero
	            	    exit_playmovie
                    	    movie_in=intarr(n_elements(good))
                    	    for nw=0,n_elements(movie_in)-1 do begin
                     	    	WINDOW, XSIZE =moviev.file_hdr.nx, YSIZE = moviev.file_hdr.ny, /PIXMAP, /FREE
                     	    	movie_in(nw) = !D.WINDOW
                     	    	tv,imgs(*,*,nw)
                    	    endfor
                    	    if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW' then SCC_playmoviem, movie_in, HDRS=HDRS, NAMES=NAMES, file_hdr=moviev.file_hdr,wcshdr=wcshdrs,osczero=osczero else $
		    	    	    SCC_playmovie, movie_in, HDRS=HDRS, NAMES=NAMES, file_hdr=moviev.file_hdr,truecolor=moviev.file_hdr.truecolor,wcshdr=wcshdrs,osczero=osczero

                       endif



                   RETURN

		    ;endif else widget_control,moviev.instxt,set_value='spoke must be Previewed before being applied '
		END                


          'spkCANCEL': BEGIN              ; 
                   if datatype(spkimg) ne 'UND' then undefine,newimgt
                   if spkim gt 0 then wdel,spkim-1
                   WIDGET_CONTROL, /DESTROY, spkbase
                   WIDGET_CONTROL, /DESTROY, spklead
                   moviev.instxt=0
                  return
		END                
 



	ELSE : BEGIN
                 PRINT, '%%spoke_setect.  Unknown event ',input
	     END

    ENDCASE

END 

pro apply_rad_spoke
COMMON scc_playmv_COMMON
COMMON spoke_common

        spkim=-1
        det=moviev.img_hdrs(0).detector
        dindx=where(['C2','C3','EIT'] EQ det,issoho)
        dindx=where(['HI2'] EQ det, ishi2)
        dindx=where(['HI1'] EQ det, ishi1)
        if ~issoho then sc=strupcase(strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)) else sc='S'
        define_wcshdrs,/current,system='B',wcs=wcs
    	cunit=wcs.cunit[0]
    	IF cunit EQ 'deg' THEN unitf=1. else unitf=3600.
    	arcsc=ABS(wcs.cdelt[0])*3600./unitf
	if moviev.file_hdr.sunxcen lt moviev.file_hdr.nx then p1=moviev.file_hdr.nx-1 else p1=0
    	xy=wcs_get_coord(wcs,[p1,moviev.file_hdr.sunycen])
    	xyc=wcs_get_coord(wcs,[moviev.file_hdr.sunxcen,moviev.file_hdr.sunycen])
    	IF wcs.projection EQ 'ARC' THEN rth=[xy[0],xy[1]+90] ELSE rth=hpc2hpr(xy, DEGREES=(unitf LT 3600),/silent)
    	IF wcs.projection EQ 'ARC' THEN rthc=[xyc[0],xyc[1]+90] ELSE rthc=hpc2hpr(xyc, DEGREES=(unitf LT 3600),/silent)
    	r=rth[1] & rc=rthc[1]
    	IF (moviev.img_hdrs(moviev.current).detector) EQ 'COR2' THEN BEGIN
    	     rpix=r*unitf/wcs.cdelt[0]
    	     aorb=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)
	     rpix1 = Cor2_DISTORTION(rpix, aorb, arcsc) 	; input is pixels
    	     r=r*rpix1/rpix; output is pixels
    	     rc=rc*rpix1/rpix; output is pixels
        ENDIF


 	spokev=[moviev.file_hdr.sunxcen,moviev.file_hdr.sunycen,rc,r,0,360,moviev.file_hdr.nx,moviev.file_hdr.ny,0,0,rc,r,0,360.,moviev.file_hdr.nx,moviev.file_hdr.ny]
        inst=''

        spklead = Widget_Base(Map=0)
        Widget_Control, spklead, /Realize
	spkbase = WIDGET_BASE(/COL, TITLE='APPLY RADIAL SPOKE',group_leader=spklead,/modal)

	rowb = WIDGET_BASE(spkbase, /ROW)
	col1 = WIDGET_BASE(rowb, /COLUMN)
	col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)
;	row1 = WIDGET_BASE(col2, /ROW)
;	    spokesun =   WIDGET_LABEL(row1, VALUE='SUN Center   ')
;            spokesunx =  CW_FIELD(row1, VALUE=spokev(0), UVALUE='spokesunx'  , XSIZE=15, YSIZE=1, title='        X =')
;            spokesuny =  CW_FIELD(row1, VALUE=spokev(1), UVALUE='spokesuny'  , XSIZE=15, YSIZE=1, title='        Y =')
  
	row2 = WIDGET_BASE(col2, /ROW)
	    spoker =     WIDGET_LABEL(row2, VALUE='Radius (deg)  ')
            spoker1 =    CW_FIELD(row2, VALUE=spokev(2), /ALL_EVENTS, UVALUE='spoker1'    , XSIZE=15, YSIZE=1, title='    inner =')
            spoker2 =    CW_FIELD(row2, VALUE=spokev(3), /ALL_EVENTS, UVALUE='spoker2'    , XSIZE=15, YSIZE=1, title='    outer =')
            spoken_r =   CW_FIELD(row2, VALUE=spokev(6), /ALL_EVENTS, UVALUE='spoken_r'   , XSIZE=15, YSIZE=1, title='N Samples =')

	row3 = WIDGET_BASE(col2, /ROW)
	    spoketh =    WIDGET_LABEL(row3, VALUE='azimuth angle')
            spoketh1 =   CW_FIELD(row3, VALUE=spokev(4), /ALL_EVENTS, UVALUE='spoketh1'   , XSIZE=15, YSIZE=1, title='starting  =')
            spoketh2 =   CW_FIELD(row3, VALUE=spokev(5), /ALL_EVENTS, UVALUE='spoketh2'   , XSIZE=15, YSIZE=1, title='  ending  =')
            spoken_th =  CW_FIELD(row3, VALUE=spokev(7), /ALL_EVENTS, UVALUE='spoken_th'  , XSIZE=15, YSIZE=1, title='N Samples =')



	rowc = WIDGET_BASE(spkbase, /ROW)
	blk= '          '
            spokeradscl=widget_droplist(rowc, VALUE=['Linear','Log'], UVALUE='spokeradscl',title='Radius Scale :')
            widget_control,spokeradscl,set_droplist_select=spokev(9)
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['spkPREV','spkDONE','spkCANCEL'])

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)


	instxt =   widget_text(rowc, VALUE=inst,  UVALUE='spokeinst' , XSIZE=65, YSIZE=1)
	
        moviev.instxt=instxt
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=spkbase
    	IF (moviev.debugon) then help,moviev.basexoff(0),moviev.baseyoff(0)
    	WIDGET_CONTROL, spkbase,xoffset=moviev.basexoff(0), yoffset=moviev.baseyoff(0)
	WIDGET_CONTROL, spkbase, /REALIZE

	XMANAGER, 'select_spoke', spkbase, EVENT_HANDLER='spoke_event'
	

end
;**-----------------end spoke widget-----------------**
;----------------project widget --------------------------------------
pro project_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON project_common, plnim, projectv, plnbase, plnlead, plnwin, plnoptions,plnmvout

tmp = INDGEN(N_ELEMENTS(moviev.win_index))
good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN BEGIN
    	help,/str,ev
	print,'In project_event.pro, '
	help,input
	wait,3
    ENDIF

    CASE (input) OF
           'plnSAVE' : begin
	         plnoptions(0)=ev.select  
                 if plnoptions(0) eq 1 then begin
                    ofn=moviev.filename
                    ofp=moviev.filepath
                    scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=ofn,len=moviev.len,first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect,/selectonly;scc_movieout,moviev.base ,/selectonly
                    IF (ofn ne moviev.filename and ofn ne '') THEN BEGIN
                        plnmvout=ofn
                     ENDIF else begin
		         print,'******************************************'
		         print,'*****  Movie filename not selected   *****'
		         print,'******************************************'
	                 plnoptions(0)=0
			 widget_control,ev.id,SET_VALUE=plnoptions
                     ENDELSE
                  ENDIF
		END                
		 
           'plnNORM' : plnoptions(1)=ev.select  

           'plnPREV': BEGIN              ; 
                  i=moviev.current
                  if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs,/current,wcs=wcs else wcs=moviev.wcshdrs(i)
		  coords=wcs_get_coord(wcs)
 		  IF(n_elements(imgs) GT 1) THEN  img = imgs[*,*,i]
		   if (moviev.img_hdrs(0).detector eq 'HI2' or  moviev.img_hdrs(0).detector eq 'HI1') then begin
                      print,'running Plane Project on Frame #',' ',string(i)
		      coords=wcs_get_coord(wcs)
		      plane_project, img, coords, newimg, SCEN=scen, RANGE=range, spher=1, INTERP=1, debug=debug_on
		      IF (moviev.debugon) THEN help,img,newimg,coords,scen,range
		      IF (moviev.debugon) THEN wait,2
                   endif
		   if (moviev.img_hdrs(0).detector eq 'COR2') then begin
                       print,'running COR2_WARP on Frame #',' ',string(i)
                       newimg=cor2_warp(img,moviev.img_hdrs(i))
                       scen=[moviev.img_hdrs(i).xcen,moviev.img_hdrs(i).ycen] ;cor2_warp does not return suncenter
		       IF (moviev.debugon) THEN help,img,newimg,scen
		       IF (moviev.debugon) THEN wait,2
                  endif
                  sz=size(newimg)
                  device,retain=2
                  window,xsize=sz(1),ysize=sz(2)
                  plnim=!d.window +1
		  tv,newimg
    	    	  IF moviev.debugon THEN stop
		END                

         'plnDONE': BEGIN              ; 
                    winsave=!D.WINDOW
                    if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs

		    HDRS=moviev.img_hdrs(good) 
		    NAMES=moviev.frames(good)
                    wcshdrstmp=moviev.wcshdrs(good)
                    file_hdr=moviev.file_hdr
                    if n_elements(moviev.wcshdrs) eq moviev.len*2 then wcshdrstmp=[wcshdrstmp,moviev.wcshdrs(good+moviev.len)]

		       FOR i=0,n_elements(good)-1 DO BEGIN 
 			 wcs=wcshdrstmp(i)
		         coords=wcs_get_coord(wcs)
 		         IF(n_elements(imgs) GT 1) THEN  img = imgs[*,*,good(i)]
		         if (HDRS(0).detector eq 'HI2' or  HDRS(0).detector eq 'HI1') then begin
                           print,'running Plane Project on Frame #',' ',string(i)
			   coords=wcs_get_coord(wcs)
		           plane_project, img, coords, newimg, SCEN=scen, RANGE=range, spher=1, INTERP=1, NEW_CDELT=ncdelt, debug=debug_on
			   IF (moviev.debugon) THEN help,img,newimg,coords,scen,range,ncdelt
			   IF (moviev.debugon) THEN wait,2
                         endif
		         if (HDRS(0).detector eq 'COR2') then begin
                           print,'running COR2_WARP on Frame #',' ',string(i)
                           newimg=cor2_warp(img,HDRS(i))
                           scen=[HDRS(i).xcen,HDRS(i).ycen] ;cor2_warp does not return suncenter
			   IF (moviev.debugon) THEN help,img,newimg,scen
			   IF (moviev.debugon) THEN wait,2
                         endif
			 ;
			 ; Update Headers: file_hdr, img_hdrs, wcshdrs
			 ;
			 IF wcs.cunit[0] EQ 'deg' THEN factor=3600. ELSE factor=1.
                         if i eq 0 then begin
			    sx=n_elements(newimg(*,0))
			    sy=n_elements(newimg(0,*))
                               ; increase frame ysize size to avoid cutting off top and bottom 
                            if (HDRS(0).detector eq 'HI2' or  HDRS(0).detector eq 'HI1') and plnoptions(1) eq 1 then begin
                               wcshdrs0 = wcs2arc(wcshdrstmp[i],[sx,sy],scen,ncdelt)
                               adj=fix(320.*(moviev.vsize/1024.))
			       sy=sy+adj
			       scen(1)=scen(1)+adj/2
                               adj=fix(160.*(moviev.hsize/1024.))
			       sx=sx+adj
			       scen(0)=scen(0)+adj/2
                               wcshdrstmp[i] = wcs2arc(wcshdrstmp[i],[sx,sy],scen,ncdelt)
                               scen1=scen
			       ncdelt1=ncdelt
			       coord1=wcs_get_coord(wcshdrstmp[i])
			       pixel = wcs_get_pixel( wcshdrs0, coord1)
                               newimg = reform( interpolate( newimg, pixel[0,*,*], pixel[1,*,*],missing=300 ))
                               newimg(where(newimg eq 300))=0
			    endif
                            first=1
			    file_hdr.sunxcen=scen(0)
			    file_hdr.sunycen=scen(1)
			    file_hdr.sec_pix = abs(ncdelt*factor)
                            hsize=sx
			    vsize=sy
			    file_hdr.nx=sx
			    file_hdr.ny=sy
			    if plnoptions(0) ne 1 then imgsnew=bytarr(sx,sy,n_elements(good))
                            set_plot,'z'
                            device,set_resolution=[hsize,vsize]
                            ;window,xsize=hsize,ysize=vsize
                            if plnoptions(0) eq 1 then BREAK_FILE, plnmvout, a, outdir, outname, outext
 			 endif			 
                         HDRS(i).xcen=scen(0)
                         HDRS(i).ycen=scen(1)
			 HDRS[i].cdelt1=abs(ncdelt*factor)
                         ;moviev.wcshdrs(i).coord_type='Heliocentric-Cartesian'
                         HDRS(i).filter='Deproject'
                         if tag_exist(HDRS(i),'roll') then HDRS(i).roll=0 else HDRS(i).crota=0

			 wcshdrstmp[i] = wcs2arc(wcshdrstmp[i],[sx,sy],scen,ncdelt)
			 ;
			 ; Re-make imgs array and redisplay frames
			 ;
			 ; Align sun centers
                         if plnoptions(1) eq 1 and i gt 0 then begin
			    pixel = wcs_get_pixel( wcshdrstmp[i], coord1)
                               newimg = reform( interpolate( fix(newimg), pixel[0,*,*], pixel[1,*,*],missing=300 ))
                               newimg(where(newimg eq 300))=0
                            ;newimg = reform( interpolate( newimg, pixel[0,*,*], pixel[1,*,*] ))
                            HDRS(i).xcen=scen1(0)
                            HDRS(i).ycen=scen1(1)
			    HDRS[i].cdelt1=abs(ncdelt1*factor)
			    wcshdrstmp[i] = wcs2arc(wcshdrstmp[i],[sx,sy],scen1,ncdelt1)
			 endif
			 ; in case one frame is different size than the first ???
                         if n_elements(newimg(*,0)) lt sx then sx2=n_elements(newimg(*,0)) else sx2=sx
                         if n_elements(newimg(0,*)) lt sy then sy2=n_elements(newimg(0,*)) else sy2=sy
                         tv,newimg[0:(sx2-1),0:(sy2-1)]
                         newimg=tvrd ()
                         if plnoptions(0) ne 1 then imgsnew[*,*,i] = newimg else begin
                           PRINT, '%%Deproject saving frame ', STRING(i,FORMAT='(I4)'), $
                              ' to movie file ', plnmvout
                           if outext ne '.mpg' then begin
                             if outext ne '.mvi' then begin
                               timestr = HDRS(i).date_obs + ' '+HDRS(i).time_obs
                               IF strmid(outname,0,4) eq 'yyyy' THEN $
				  filename=strmid(timestr,0,4)+strmid(timestr,5,2)+strmid(timestr,8,2)+'_'+ $
				  strmid(timestr,11,2)+strmid(timestr,14,2)+'_'+HDRS(0).detector $
                               ELSE filename=outname+'_'+STRING(i+1,FORMAT='(I4.4)')
            
                               newimg=filename+outext
                               if outext eq '.png' then tmp=fTVREAD(/png, filename=outdir+filename,/noprompt)	; ftvread writes the file
                               if outext eq '.gif' then tmp=fTVREAD(/gif, filename=outdir+filename,/noprompt)	; ftvread writes the file                       endelse
                               if first eq 1 then outmovie=outdir+filename+'.hdr'
                            endif
                            IF (first EQ 1) THEN BEGIN
                               SCCWRITE_DISK_MOVIE, plnmvout, newimg, HDRS(i), /NEW, MAXNUM=n_elements(good), $
            	                 RECTIFIED=file_hdr.RECTIFIED, DEBUG=debugon ,truecolor=file_hdr.truecolor,CCD_POS=file_hdr.ccd_pos, nx=hsize, ny=vsize
                               first = 0
                            ENDIF ELSE BEGIN
                              SCCWRITE_DISK_MOVIE, plnmvout, newimg, HDRS(i), DEBUG=debugon ,truecolor=file_hdr.truecolor,$
    	    	                CCD_POS=file_hdr.ccd_pos, nx=hsize, ny=vsize
                            ENDELSE
                          endif else begin
                              PIXMAPS2MPEG, plnmvout , !D.WINDOW, inline=n_elements(good),firstframe=first, _EXTRA=_extra
                              IF (first EQ 1) THEN first = 0
                          endelse
                        endelse
                      endfor


                    
                    select_windows ;set_plot,'x'
		    WSET, winsave
                    if plnim gt 0 then wdel,plnim-1
                    WIDGET_CONTROL, /DESTROY, plnbase
                    WIDGET_CONTROL, /DESTROY, plnlead
                    if plnoptions(0) ne 1 then begin
                          movie_in=intarr(n_elements(good))
                          imgs = imgsnew(*,*,*)
                          undefine,imgsnew
                          osczero=moviev.osczero
			  evtpro=moviev.evtpro
                          exit_playmovie
		          for nw=0,n_elements(movie_in)-1 do begin
                             WINDOW, XSIZE =sx, YSIZE = sy, /PIXMAP, /FREE
                             movie_in(nw) = !D.WINDOW
                             tv,imgs(*,*,nw)
                           endfor
                           if evtpro eq 'SCC_PLAYMOVIEM_DRAW' then SCC_playmoviem, movie_in, HDRS=HDRS, NAMES=NAMES, file_hdr=file_hdr,truecolor=file_hdr.truecolor,wcshdr=wcshdrstmp,osczero=osczero else $
		    	    	    SCC_playmovie, movie_in, HDRS=HDRS, NAMES=NAMES, file_hdr=file_hdr,truecolor=file_hdr.truecolor,wcshdr=wcshdrstmp,osczero=osczero
                    endif
                   RETURN
		    ;endif else widget_control,moviev.instxt,set_value='project must be Previewed before being applied '
		END                


          'plnCANCEL': BEGIN              ; 
                   if plnim gt 0 then wdel,plnim-1
                   WIDGET_CONTROL, /DESTROY, plnbase
                   WIDGET_CONTROL, /DESTROY, plnlead
                   moviev.instxt=0
                  return
		END                
 



	ELSE : BEGIN
                 PRINT, '%%project_event.  Unknown event ',input
	     END

    ENDCASE

END 

pro apply_project
COMMON scc_playmv_COMMON
COMMON project_common

        plnim=-1
        plnoptions=[0,0]
        det=moviev.img_hdrs(0).detector
        dindx=where(['HI2'] EQ det, ishi2)
        dindx=where(['HI1'] EQ det, ishi1)
        dindx=where(['COR2'] EQ det, iscor2)
 
        plnlead = Widget_Base(Map=0)
        Widget_Control, plnlead, /Realize

        if (iscor2) then ptitle='APPLY COR2_WARP' else ptitle= 'APPLY PLANE_PROJECT'
	plnbase = WIDGET_BASE(/COL, TITLE=ptitle,group_leader=plnlead,/modal,xsize=300,ysize=140)
        rowb  = WIDGET_BASE(plnbase, /ROW)
        ;poptions = CW_BGROUP(rowb, ['SAVE MOVIE (use for larger movies)','Normalize Sun Centers'], BUTTON_UVALUE=['plnSAVE','plnNORM'], /NONEXCLUSIVE,/Column)
        ; aliign sun center needs correcting
        plnoptions=[0,0]
        poptions = CW_BGROUP(rowb, ['SAVE MOVIE (use for larger movies)','align Sun Centers'], BUTTON_UVALUE=['plnSAVE','plnNORM'], /NONEXCLUSIVE,/Column)
;        plnoptions=[0]
;        poptions = CW_BGROUP(rowb, ['SAVE MOVIE (use for larger movies)'], BUTTON_UVALUE=['plnSAVE'], /NONEXCLUSIVE,/Column)

	rowc = WIDGET_BASE(plnbase, /ROW)
	blk= '          '
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['plnPREV','plnDONE','plnCANCEL'])
;	instxt =   widget_text(rowc, VALUE=inst,  UVALUE='projectinst' , XSIZE=65, YSIZE=1)

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)
	
;        moviev.instxt=instxt
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=plnbase
    	IF (moviev.debugon) then help,moviev.basexoff(0),moviev.baseyoff(0)
    	WIDGET_CONTROL, plnbase,xoffset=moviev.basexoff(0), yoffset=moviev.baseyoff(0)
	WIDGET_CONTROL, plnbase, /REALIZE
        ;WIDGET_CONTROL, poptions, SET_VALUE=plnoptions

	XMANAGER, 'project', plnbase, EVENT_HANDLER='project_event'

end
;**-----------------end project widget-----------------**
;**-----------------Start grid widget ------------------**
pro draw_grid,cancel=cancel
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON grid_select, gridstr, smval

              
  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
  if datatype(imgs) eq 'BYT' then begin
  winsave=!D.WINDOW
  nf=n_elements(moviev.frames)
  IF debug_on THEN dopixm=0 ELSE dopixm=1
  WINDOW, XSIZE = moviev.file_hdr.nx, YSIZE = moviev.file_hdr.ny, PIXMAP=dopixm, /FREE
  gridw = !D.WINDOW
  mvix=moviev.file_hdr.nx
  mviy=moviev.file_hdr.ny
  if moviev.flatplane eq 1 then begin
    if moviev.osczero(0) gt 0 then nx=(mvix-2)/2 else nx=mvix
    if moviev.osczero(1) gt 0 then ny=(mviy-2)/2 else ny=mviy
     coords=fltarr(2,nx,ny)
     lat=replicate(1,ny)##lgen(nx,[moviev.file_hdr.theta0,moviev.file_hdr.theta1])
     lng=lgen(ny,[moviev.file_hdr.radius0,moviev.file_hdr.radius1])##replicate(1,nx)
     ;lat=(findgen(nx)+moviev.file_hdr.theta0*moviev.file_hdr.sec_pix)
     ;lng=(findgen(ny)+moviev.file_hdr.radius0*moviev.file_hdr.sec_pix)
     coords[0,*,*]=lat
     coords[1,*,*]=lng
  endif


  if gridstr.nf le 0 then begin
    if ~keyword_set(cancel) and gridstr.name ne 'Axis'  and gridstr.name ne 'Grid' then begin
      if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs,/current,wcs=wcsi else begin
	  wcsi=moviev.wcshdrs(moviev.current)
          if total(moviev.osczero) gt 0 then wcsi=[wcsi,moviev.wcshdrs(moviev.current+n_elements(moviev.img_hdrs))]
       endelse
     endif
     fstart=moviev.current
     fend=moviev.current
  endif else begin
     if ~(TAG_EXIST(moviev, 'wcshdrs')) and ~keyword_set(cancel) then define_wcshdrs
     fstart=0
     fend=gridstr.nf-1
  endelse

;'Draw Carrington Grid'
   if gridstr.name eq 'Carrington' then begin
       plevels=findgen(360/gridstr.grspace(0)+1)*gridstr.grspace(0)+(-180.)
       ; create labels for longitute 0-360 not (-180) - 180
       lonlevels=findgen(360/gridstr.grspace(1)+1)*gridstr.grspace(1)+(0.)
   endif
;'Draw Radial Grid'
   if gridstr.name eq 'Radial' then begin
       lonlevels=findgen(180/gridstr.grspace(0)+1)*gridstr.grspace(0)+(0.)
       plevels=findgen(360/gridstr.grspace(1)+1)*gridstr.grspace(1)+(0.)
       doday=moviev.img_hdrs(fstart).date_obs
   endif

   if gridstr.name eq 'HPLN/HPLT' then begin
     if total(gridstr.grspace) gt 0 then begin
       cset=0 & dx=gridstr.grspace(0) & dy=gridstr.grspace(1)
     endif else cset=1
     doday=moviev.img_hdrs(fstart).date_obs
   endif

   FOR i=fstart,fend DO BEGIN 
 	wset,gridw
	erase,0
        if ~keyword_set(cancel) then begin
            print,'Drawing grid for Frame #',' ',string(i)
             if gridstr.name ne 'Axis' and  gridstr.name ne 'Grid' and moviev.flatplane eq 0 then begin
               if ~(TAG_EXIST(moviev, 'wcshdrs')) then wcs=wcsi(0) else wcs=moviev.wcshdrs(i)
             endif
;'Draw HPLN/HPLT Grid'
             if total(gridstr.grspace) eq 0 then cset=1
    	    if debug_on then begin
    	    	print,'waiting'
		wait,2
    	    endif
             if gridstr.name eq 'HPLN/HPLT' then begin
                 if i eq fstart or moviev.img_hdrs(i).date_obs ne doday then begin
                         doday=moviev.img_hdrs(i).date_obs
		         if moviev.flatplane eq 0 then coords=wcs_get_coord(wcs)
                         if total(moviev.osczero) gt 0 then begin
                            if moviev.osczero(0) gt 0 then nx=(mvix-2)/2 else nx=mvix
                            if moviev.osczero(1) gt 0 then ny=(mviy-2)/2 else ny=mviy
                            drawcoordgrid, wcs, coords=coords, DX=dx,DY=dy, /debug, c_charsize=gridstr.grsize,position=[0,0,nx-1,ny-1]
                            if moviev.flatplane eq 0 then begin
			       if ~(TAG_EXIST(moviev, 'wcshdrs')) then wcs=wcsi(1) else wcs=moviev.wcshdrs(i+moviev.len)
		               coords=wcs_get_coord(wcs)
                            endif
                            drawcoordgrid, wcs, coords=coords, DX=dx,DY=dy, /debug, c_charsize=gridstr.grsize,$
			    	position=[moviev.osczero(0),moviev.osczero(1),mvix-1,mviy-1]
		         endif else $
			    drawcoordgrid, wcs, coords=coords, DX=dx,DY=dy, /debug, c_charsize=gridstr.grsize

                         if cset eq 1 then begin
			    gridstr.grspace=[dx,dy]
                            widget_control,gridstr.grspv(0),set_value=gridstr.grspace(0)
                            widget_control,gridstr.grspv(1),set_value=gridstr.grspace(1)
                            cset=0
			 endif
                         cgrid=tvrd()
                   endif
               endif

;'Draw Carrington Grid'
               if gridstr.name eq 'Carrington' then begin

		       if moviev.flatplane eq 0 then begin
		         coords=wcs_get_coord(wcs)
                         WCS_CONVERT_from_COORD, wcs, coords, 'HG', clong, clat, /CARRINGTON
		       endif else begin
			    WCS_CONV_HPC_HG,coords(0,*,*),coords(1,*,*)  ,clong, clat ,/CARRINGTON,ang_units='degrees'
    	    	            clong=reform(clong[0,*,*])
		            clat=reform(clat[0,*,*]) 
                        endelse
                       clong(where(clong lt 0))=clong(where(clong lt 0))+360 
                       clong=round(clong)
		       IF debug_on THEN BEGIN
		            help,clong,clat
			    maxmin,clong
			    maxmin,clat
			    wait,2
			ENDIF
                       if total(moviev.osczero) gt 0 then begin
                          if moviev.osczero(0) gt 0 then nx=(mvix-2)/2 else nx=mvix
                          if moviev.osczero(1) gt 0 then ny=(mviy-2)/2 else ny=mviy
                          contour,clat,/device,xstyle=5,ystyle=5,levels=plevels,position=[0,0,nx-1,ny-1],thick=1,c_charsize=gridstr.grsize
                          contour,clong,/noerase,/device,xstyle=5,ystyle=5,levels=lonlevels,position=[0,0,nx-1,ny-1],thick=1,c_charsize=gridstr.grsize,min_value=5
                          if moviev.flatplane eq 0 then begin
                            if ~(TAG_EXIST(moviev, 'wcshdrs')) then wcs=wcsi(1) else wcs=moviev.wcshdrs(i+moviev.len)
		            coords=wcs_get_coord(wcs)
       			    WCS_CONVERT_from_COORD, wcs, coords, 'HG', clong, clat, /CARRINGTON
                          endif else begin
			    WCS_CONV_HPC_HG,coords(0,*,*),coords(1,*,*)  ,clong, clat ,/CARRINGTON,ang_units='degrees'
    	    	            clong=reform(clong[0,*,*])
		            clat=reform(clat[0,*,*]) 
                        endelse
       			  WCS_CONVERT_from_COORD, wcs, coords, 'HG', clong, clat, /CARRINGTON
                          clong(where(clong lt 0))=clong(where(clong lt 0))+360 
                          clong=round(clong)
                          contour,clat,/noerase,/device,xstyle=5,ystyle=5,levels=plevels,position=[moviev.osczero(0),moviev.osczero(1),mvix-1,mviy-1],thick=1,c_charsize=gridstr.grsize
                          contour,clong,/noerase,/device,xstyle=5,ystyle=5,levels=lonlevels,position=[moviev.osczero(0),moviev.osczero(1),mvix-1,mviy-1],thick=1,c_charsize=gridstr.grsize,min_val=5
		       endif else begin
                         contour,clat,/device,xstyle=5,ystyle=5,levels=plevels,position=[0,0,mvix-1,mviy-1],thick=1,c_charsize=gridstr.grsize
                         contour,clong,/noerase,/device,xstyle=5,ystyle=5,levels=lonlevels,position=[0,0,mvix-1,mviy-1],thick=1,c_charsize=gridstr.grsize,min_value=5
                       endelse
                       cgrid=tvrd()
                endif

;'Draw Radial Grid'
                if gridstr.name eq 'Radial' then begin
                    arc=0
                    if i eq fstart or moviev.img_hdrs(i).date_obs ne doday then begin
                         doday=moviev.img_hdrs(i).date_obs
		        if moviev.flatplane eq 0 then begin
			   coords=wcs_get_coord(wcs)
		    	   IF wcs.projection EQ 'ARC' THEN BEGIN
    	    	    	      xr=reform(coords[0,*,*])
			      yr=reform(coords[1,*,*]+90) 
			; WCS does not handle distance from sun center directly for ARC (HPR) projection
                              arc=1
                           endif
			ENDIF
		    	IF arc ne 1 THEN BEGIN
			    if moviev.flatplane eq 0 then WCS_CONVERT_from_COORD, wcs, coords, 'HPR', xr, yr, /ZERO_CENTER else begin
			    	WCS_CONV_HPC_HPR,coords(0,*,*),coords(1,*,*)  ,xr, yr ,/ZERO_CENTER,ang_units='degrees',QUICK=doquick
    	    	    		xr=reform(xr[0,*,*])
				yr=reform(yr[0,*,*]) 
                            endelse
                            xrzero=where(xr lt 0)
                            if xrzero(0) gt -1 then xr(where(xr lt 0))=xr(where(xr lt 0))+360 
			    xr=round(xr)
			ENDIF
		         wset,gridw
                         if total(moviev.osczero) gt 0 then begin
                            if moviev.osczero(0) gt 0 then nx=(mvix-2)/2 else nx=mvix
                            if moviev.osczero(1) gt 0 then ny=(mviy-2)/2 else ny=mviy
                            contour,xr,/device,xstyle=5,ystyle=5,levels=plevels,position=[0,0,nx-1,ny-1],thick=1,c_charsize=gridstr.grsize,min_val=5
                            contour,yr,/noerase,/device,xstyle=5,ystyle=5,levels=lonlevels,position=[0,0,nx-1,ny-1],thick=1,c_charsize=gridstr.grsize
                            if moviev.flatplane eq 0 then begin
                              if ~(TAG_EXIST(moviev, 'wcshdrs')) then wcs=wcsi(1) else wcs=moviev.wcshdrs(i+moviev.len)
		              coords=wcs_get_coord(wcs)
		    	      IF wcs.projection EQ 'ARC' THEN BEGIN
    	    	    		xr=reform(coords[0,*,*])
				yr=reform(coords[1,*,*]+90) 
                                arc=1
			    ; WCS does not handle distance from sun center directly for ARC (HPR) projection
                              endif
                            endif
		    	    IF arc ne 1 THEN BEGIN
                        	if moviev.flatplane eq 0 then WCS_CONVERT_from_COORD, wcs, coords, 'HPR', xr, yr, /ZERO_CENTER else begin
			    	    WCS_CONV_HPC_HPR,coords(0,*,*),coords(1,*,*)  ,xr, yr ,/ZERO_CENTER,ang_units='degrees',QUICK=doquick
    	    	    		    xr=reform(xr[0,*,*])
				    yr=reform(yr[0,*,*]) 
                                endelse
                        	xrzero=where(xr lt 0)
                        	if xrzero(0) gt -1 then xr(where(xr lt 0))=xr(where(xr lt 0))+360 
				xr=round(xr)
			    ENDIF
                            contour,xr,/noerase,/device,xstyle=5,ystyle=5,levels=plevels,position=[moviev.osczero(0),moviev.osczero(1),mvix-1,mviy-1],thick=1,c_charsize=gridstr.grsize,min_val=5
                            contour,yr,/noerase,/device,xstyle=5,ystyle=5,levels=lonlevels,position=[moviev.osczero(0),moviev.osczero(1),mvix-1,mviy-1],thick=1,c_charsize=gridstr.grsize
		         endif else begin
    	    	    	    contour,xr,/device,xstyle=5,ystyle=5,levels=plevels, position=[0,0,mvix-1,mviy-1], thick=1,c_charsize=gridstr.grsize,min_val=5
			    contour,yr,/noerase,/device,xstyle=5,ystyle=5,levels=lonlevels, position=[0,0,mvix-1,mviy-1], thick=1,c_charsize=gridstr.grsize
                         endelse
                         cgrid=tvrd()
                    endif
                 endif
		       
;'Draw RTHETA Grid'
                if gridstr.name eq 'Axis' or gridstr.name eq 'Grid' then begin
		    wset,gridw
                    if i eq fstart then begin
                       yticknames=REPLICATE(' ', 30)
                       xticknames=REPLICATE(' ', 30)
                       ylogv=0 & xlogv=0
                       dx=gridstr.grspace(0) & dy=gridstr.grspace(1)
                       IF moviev.file_hdr.rtheta lt 5 THEN BEGIN
		       	 yr=[moviev.file_hdr.radius0,moviev.file_hdr.radius1]
                         xr=[moviev.file_hdr.theta0,moviev.file_hdr.theta1]
                       ENDIF ELSE BEGIN
		       	 xr=[moviev.file_hdr.radius0,moviev.file_hdr.radius1]
                         yr=[moviev.file_hdr.theta0,moviev.file_hdr.theta1]
		       ENDELSE
                       if gridstr.name eq 'Grid' then yxtick=1 else yxtick=0.02
                       IF moviev.file_hdr.rtheta EQ 2 THEN BEGIN
		          nypix=10^(alog10(yr(0))+0.02*(alog10(yr(1))-alog10(yr(0))))
                          ylogv=1
		       ENDIF ELSE nypix=(yr(0))+0.02*(yr(1)-yr(0))
                       IF moviev.file_hdr.rtheta EQ 6 THEN BEGIN
		          nxpix=10^(alog10(xr(0))+0.02*(alog10(xr(1))-alog10(xr(0))))
                          xlogv=1
		       ENDIF ELSE nxpix=(xr(0))+0.02*(xr(1)-xr(0))
                       if dy-fix(dy) gt 0 then yfrmt='(f5.2)' else yfrmt='(i2.2)'
                       xouts=findgen((xr(1)-xr(0)-1)/dx)+1
                       youts=findgen((yr(1)-yr(0)-1)/dy)+1
                       if dy-fix(dy) gt 0 then begin
		         yfrmt='(f5.2)'
                         youtst=strarr(n_elements(youts))
                         for nt=0,n_elements(youts)-1 do youtst(nt)=string(youts(nt)*dy+yr(0),yfrmt)
                       endif else begin
                         youtst=strarr(n_elements(youts))
                         for nt=0,n_elements(youts)-1 do youtst(nt)=strcompress(string(fix(youts(nt)*dy+fix(yr(0)))),/remove_all)
		       endelse
                       if dx-fix(dx) gt 0 then begin
		         yfrmt='(f5.2)'
                         xoutst=strarr(n_elements(xouts))
                         for nt=0,n_elements(xouts)-1 do xoutst(nt)=string(xouts(nt)*dx+xr(0),yfrmt)
                       endif else begin
                         xoutst=strarr(n_elements(xouts))
                         for nt=0,n_elements(xouts)-1 do xoutst(nt)=strcompress(string(fix(xouts(nt)*dx+fix(xr(0)))),/remove_all)
		       endelse
                       if total(moviev.osczero) gt 0 then begin
                          if moviev.osczero(0) gt 0 then nx=(mvix-2)/2 else nx=mvix
                          if moviev.osczero(1) gt 0 then ny=(mviy-2)/2 else ny=mviy
                          plot,[0,0],[0,0],xstyle=1,ystyle=1,xrange=xr,yrange=yr,position=[mvix+2,mviy+2,nx-1,ny-1],ylog=ylogv,color=255,ytickname=yticknames,xtickname=xticknames,/device,$
			    	    ticklen=yxtick,ytickv=youts,xtickv=xouts,XTICKINTERVAL=dx,YTICKINTERVAL=dy
                          for nt=0,n_elements(xouts)-1 do xyouts,xoutst(nt),nypix,xoutst(nt),color=255,align=.5,charsize=gridstr.grsize
                          for nt=0,n_elements(youts)-1 do xyouts,nxpix,youtst(nt),youtst(nt),color=255,align=0,charsize=gridstr.grsize
                          plot,[0,0],[0,0],xstyle=1,ystyle=1,xrange=xr,yrange=yr,position=[moviev.osczero(0),moviev.osczero(1),mvix-1,mviy-1],ylog=ylogv,color=255,ytickname=yticknames,/noerase,/device,$
			    	    ticklen=yxtick,ytickv=youts,xtickv=xouts,XTICKINTERVAL=dx,YTICKINTERVAL=dy
                          for nt=0,n_elements(xouts)-1 do xyouts,xoutst(nt),nypix,xoutst(nt),color=255,align=.5,charsize=gridstr.grsize
                          for nt=0,n_elements(youts)-1 do xyouts,nxpix,youtst(nt),youtst(nt),color=255,align=0,charsize=gridstr.grsize
                       endif else begin
                          plot,[0,0],[0,0],xstyle=1,ystyle=1,xrange=xr,yrange=yr,position=[0,0,mvix-1,mviy-1],xlog=xlogv,ylog=ylogv,color=255,ytickname=yticknames,xtickname=xticknames,/device,$
			    	    ticklen=yxtick,ytickv=youts,xtickv=xouts,XTICKINTERVAL=dx,YTICKINTERVAL=dy
                          for nt=0,n_elements(xouts)-1 do xyouts,xoutst(nt),nypix,xoutst(nt),color=255,align=.5,charsize=gridstr.grsize
                          for nt=0,n_elements(youts)-1 do xyouts,nxpix,youtst(nt),youtst(nt),color=255,align=0,charsize=gridstr.grsize
		       endelse
                       cgrid=tvrd()
                    endif
                 endif
	endif else cgrid=tvrd()

	WSET,moviev.win_index(i)
	
        if max(cgrid eq 255) then begin
           if moviev.file_hdr.truecolor eq 1 then begin
 		img=imgs(*,*,*,i)
		for nc = 0,2 do begin
                   timg=reform(imgs(nc,*,*,i))
		   timg(where(cgrid eq 255))=gridstr.grclr
		   img(nc,*,*)=timg
                 endfor 
                 if gridstr.nf ne 0 then imgs(*,*,*,i)=img
	     endif else begin
 		  img=imgs(*,*,i)
                  img(where(cgrid eq 255))=gridstr.grclr
		  if gridstr.nf ne 0 then imgs(*,*,i)=img
	    endelse
  	 endif else if moviev.file_hdr.truecolor eq 1 then img=imgs(*,*,*,i) else img=imgs(*,*,i)
	 WSET,moviev.win_index(i)
         tv,img,true=moviev.truecolor
         if i eq moviev.current then begin
	    WSET, moviev.draw_win
            DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
         endif
    	IF (moviev.debugon) THEN BEGIN
	    help,coords,dx,dy,xr,yr,img
	    wait,2
	ENDIF
    endfor
    print,'Done.'
    WSET, winsave
    WDEL,gridw
  endif
END                

 
pro grid_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON grid_select

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN help,input

    CASE (input) OF
        'gridsel' : begin
    	    	    gridstr.name=ev.str
		    ; Do not set default values for HPLN/HPLT here; they are set in pro add_grid.
                    if gridstr.name eq 'Carrington' then begin
                     	gridstr.cnames=['Lat(deg):','Lon(deg):']
                     	gridstr.grspace=[30.,30.]
		    endif ELSE BEGIN
			;if iscor1 or iseuv then gridstr.grspace(1)=45
    	    	    	if gridstr.name eq 'HPLN/HPLT' then gridstr.cnames=['Lat(deg):','Lon(deg):']
		      	if gridstr.name eq 'Radial' then BEGIN
			    gridstr.cnames=['E(deg):','PA(deg):']
			    gridstr.grspace[1]=15
			ENDIF
    	    	    ENDELSE
                  widget_control,gridstr.cnamev(0),set_value=gridstr.cnames(0)
                  widget_control,gridstr.cnamev(1),set_value=gridstr.cnames(1)
                  widget_control,gridstr.grspv(0),set_value=gridstr.grspace(0)
                  widget_control,gridstr.grspv(1),set_value=gridstr.grspace(1)
                END
        'grsz' : gridstr.grsize=ev.value
        'grcl' : gridstr.grclr=ev.value
        'grsp0' : gridstr.grspace(0)=ev.value
        'grsp1' : gridstr.grspace(1)=ev.value

         'SOPREV': BEGIN              ; 
    	    	  ;IF moviev.debugon THEN stop
                  gridstr.nf=0
                  draw_grid
		END                

         'SODONE': BEGIN              ; 
    	    	  ;IF moviev.debugon THEN stop
                  gridstr.nf=n_elements(moviev.frames)
                  draw_grid
                  WIDGET_CONTROL, /DESTROY, moviev.sobase
                  WIDGET_CONTROL, /DESTROY, moviev.solead
    	    	  ;IF moviev.debugon THEN stop
                  return
		END                


          'SOCANCEL': BEGIN              ; 
                   WIDGET_CONTROL, /DESTROY, moviev.sobase
                   WIDGET_CONTROL, /DESTROY, moviev.solead
                   if gridstr.nf gt (-1) then draw_grid,/cancel
                   gridstr.nf=0
                  return
		END                

	ELSE : BEGIN
                 PRINT, '%%add_grid.  Unknown event ',input
	     END

    ENDCASE

END 



pro add_grid
COMMON scc_playmv_COMMON
COMMON grid_select
        grspace=fltarr(2) +0.
	cnames=['Lat(deg):','Lon(deg):']
	cnamev=intarr(2)


        solead = Widget_Base(Map=0)
        Widget_Control, solead, /Realize
	sobase = WIDGET_BASE(/COL, TITLE='DRAW GRID',group_leader=solead,/modal)

	row1 = WIDGET_BASE(sobase, /ROW)
	col1 = WIDGET_BASE(row1, /COLUMN)
        moviev.sobase=sobase
        moviev.solead=solead


; use CW_FIELD function
        ; default values for HPLN/HPLT: 
	rowa = WIDGET_BASE(col1, /ROW, /FRAME)
	det=moviev.img_hdrs[0].detector
	eindx=where(['EUVI','EIT','AIA'] EQ det, iseuv)
	dindx=where(['HI2'] EQ det, ishi2)
	dindx=where(['HI1'] EQ det, ishi1)
	cindx=where(['COR2','C2','C3'] EQ det, iscor2)
	dindx=where(['COR1'] EQ det, iscor1)
	grspace[0]=1.
	if ishi2  then grspace[0]=10.
	if ishi1  then grspace[0]=5.
	if iscor1 then grspace[0]=.25
	if iseuv  then grspace[0]=.1
	grspace[1]=grspace[0]
    	names=['HPLN/HPLT','Radial','Carrington'] 
	;endif else names=['HPLN/HPLT','Radial']
        if moviev.file_hdr.rtheta ge 1 and moviev.file_hdr.rtheta le 2 then begin
           cndx=where(['COR2','COR1','C3','C2','C1'] EQ det, iscor)
           names=['Axis','Grid']
	   if iscor then grspace=[90.,1.] else grspace=[90.,10.]
	   if total(abs([moviev.file_hdr.radius0,moviev.file_hdr.radius1])) lt 180 then grspace(0)=45.
	   if total(abs([moviev.file_hdr.theta0,moviev.file_hdr.theta1])) lt 20 then grspace(1)=1.
	   cnames=['PA(deg):','E(deg):']
	   cnamev=intarr(2)
	endif
        if moviev.file_hdr.rtheta ge 3 and moviev.file_hdr.rtheta le 4 then begin
           names=['Axis','Grid']
           grspace=[20.,10.]
	   if total(abs([moviev.file_hdr.theta0,moviev.file_hdr.theta1])) lt 50 then grspace=[5.,5.]
	   if total(abs([moviev.file_hdr.theta0,moviev.file_hdr.theta1])) lt 20 then grspace=[1.,1.]
	   cnames=['Lon(deg):','Lat(deg):']
	   cnamev=intarr(2)
	endif
        if moviev.file_hdr.rtheta ge 5 and moviev.file_hdr.rtheta le 6 then begin
           cndx=where(['COR2','COR1','C3','C2','C1'] EQ det, iscor)
           names=['Axis','Grid']
	   if iscor then grspace=[1.,90.] else grspace=[10.,90.]
	   if total(abs([moviev.file_hdr.radius0,moviev.file_hdr.radius1])) lt 20 then grspace(0)=1.
	   if total(abs([moviev.file_hdr.theta0,moviev.file_hdr.theta1])) lt 180 then grspace(1)=45.
	   cnames=['E(deg):','PA(deg):']
	   cnamev=intarr(2)
	endif

        if moviev.file_hdr.rtheta eq 7 then begin
           cndx=where(['COR2','COR1','C3','C2','C1'] EQ det, iscor)
           names=['Axis','Grid']
	   if iscor then grspace=[1.,90.] else grspace=[10.,90.]
	   if total(abs([moviev.file_hdr.radius0,moviev.file_hdr.radius1])) lt 20 then grspace(0)=1.
	   if total(abs([moviev.file_hdr.theta0,moviev.file_hdr.theta1])) lt 180 then grspace(1)=45.
	   cnames=['E(deg):','PA(deg):']
	   cnamev=intarr(2)
	endif

	name=names(0)
        gridv= WIDGET_COMBOBOX(rowa, VALUE=names, UVALUE="gridsel")
        grsize=1.
        grclr=255
	grspv=intarr(2)
        cnamev(0)=widget_label(rowa, value=cnames(0))
	grspv(0) =   CW_FIELD(rowa, VALUE=grspace(0), /ALL_EVENTS, UVALUE='grsp0' , XSIZE=5, YSIZE=1,title='')
        cnamev(1)=widget_label(rowa, value=cnames(1))
	grspv(1) =   CW_FIELD(rowa, VALUE=grspace(1), /ALL_EVENTS, UVALUE='grsp1' , XSIZE=5, YSIZE=1,title='')
	rowb = WIDGET_BASE(col1, /ROW)
	grsz =   CW_FIELD(rowb, VALUE=grsize, /ALL_EVENTS, UVALUE='grsz' , XSIZE=5, YSIZE=1, title='Size:')
	grcl =   CW_FIELD(rowb, VALUE=grclr, /ALL_EVENTS, UVALUE='grcl' , XSIZE=5, YSIZE=1, title='Color:')


	rowc = WIDGET_BASE(sobase, /ROW)
	blk= '          '
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['SOPREV','SODONE','SOCANCEL'])

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)

        gridstr={name:name, grsize:grsize, grclr:grclr, grspace:grspace, cnames:cnames, cnamev:cnamev, grspv:grspv, nf:(-1)}
	
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=sobase
    	IF (moviev.debugon) then help,moviev.basexoff(0),moviev.baseyoff(0)
     	WIDGET_CONTROL, sobase, xoffset=moviev.basexoff(0), yoffset=moviev.baseyoff(0)
	WIDGET_CONTROL, sobase, /REALIZE

	XMANAGER, 'add_grid', sobase, EVENT_HANDLER='grid_event'
	

end
;**-----------------End grid widget ------------------**

pro do_smooth
COMMON scc_playmv_COMMON
COMMON grid_select


        solead = Widget_Base(Map=0)
        Widget_Control, solead, /Realize
	sobase = WIDGET_BASE(/COL, TITLE='SMOOTH',group_leader=solead,/modal)

	row1 = WIDGET_BASE(sobase, /ROW)
	col1 = WIDGET_BASE(row1, /COLUMN)
        moviev.sobase=sobase
        moviev.solead=solead

    	IF datatype(smval) EQ 'UND' THEN smval=3

; use CW_FIELD function
	rowa = WIDGET_BASE(col1, /ROW, /FRAME)
	smv =   CW_FIELD(rowa, VALUE=smval, /ALL_EVENTS, UVALUE='smval', XSIZE=5, YSIZE=1,title='Smooth box')


	rowc = WIDGET_BASE(sobase, /ROW)
	blk= '          '
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['SOPREV','SODONE','SOCANCEL'])

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)

	
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=sobase
    	IF (moviev.debugon) then help,moviev.basexoff(0),moviev.baseyoff(0)
     	WIDGET_CONTROL, sobase, xoffset=moviev.basexoff(0), yoffset=moviev.baseyoff(0)
	WIDGET_CONTROL, sobase, /REALIZE

	XMANAGER, 'do_smooth', sobase, EVENT_HANDLER='smooth_event'
	

end
pro smooth_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON grid_select

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value 
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN help,input

    CASE (input) OF
    	'smval' : smval=ev.value[0]

    	'SOPREV': BEGIN              ; 
    	    	if moviev.file_hdr.truecolor le 0 then begin
    	    	    if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	    if datatype(imgs) ne 'UND' then begin
			i=moviev.current
			img = smooth(reform(imgs[*,*,i]), smval)
			wset, moviev.draw_win
			tv, img
		    	;print, 'Done smoothing'
                    endif 
	        endif else popup_msg,'Cannot smooth truecolor images',title='SCC_MOVIE_WIN MSG'
		END                

    	'SODONE': BEGIN              ; 
                if moviev.file_hdr.truecolor le 0 then begin
    	    	  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	  if datatype(imgs) ne 'UND' then begin
		    FOR i=0,n_elements(moviev.win_index)-1 DO BEGIN
			imgs[*,*,i] = smooth(temporary(imgs[*,*,i]), smval)
			wset, moviev.win_index[i]
			tv, imgs[*,*,i]
                        if i eq moviev.current then begin
		          WSET, moviev.draw_win
                          DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                        endif
		    ENDFOR
		  print, 'Done smoothing'
                  endif 
	        endif else popup_msg,'Cannot smooth truecolor images',title='SCC_MOVIE_WIN MSG'
                  WIDGET_CONTROL, /DESTROY, moviev.sobase
                  WIDGET_CONTROL, /DESTROY, moviev.solead
    	    	  ;IF moviev.debugon THEN stop
                  return
		END                


          'SOCANCEL': BEGIN              ; 
			wset, moviev.draw_win
			tv, imgs[*,*,moviev.current]
    	    	    WIDGET_CONTROL, /DESTROY, moviev.sobase
    	    	    WIDGET_CONTROL, /DESTROY, moviev.solead
    	    	    return
		END                

	ELSE : BEGIN
                 PRINT, '%%do_smooth Unknown event: ',input
	     END

    ENDCASE

END 

;**-----------------End smooth widget ------------------**

;----------------box widget --------------------------------------
pro draw_box, ubox=ubox, event=event, xyadj=xyadj
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
common crop_mvi, timgs, doneframes
 
      if keyword_set(ubox) then ubox=ubox else ubox=moviev.box 
      if TAG_EXIST(moviev, 'centadj') then xyadj=[moviev.centadj(moviev.current,2),moviev.centadj(moviev.current,3)] else xyadj=[0,0]
      ;if keyword_set(xyadj) then xyadj=xyadj
      WSET, moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
      DEVICE, COPY = [0, 0, ubox(3)-ubox(1), 2,ubox(1)+xyadj(0), ubox(2)+xyadj(1), 11] ;bottom line
      DEVICE, COPY = [0, 0, ubox(3)-ubox(1), 2,ubox(1)+xyadj(0), ubox(4)+xyadj(1), 11] ;top line
      DEVICE, COPY = [0, 0, 2, ubox(4)-ubox(2), ubox(1)+xyadj(0), ubox(2)+xyadj(1), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, ubox(4)-ubox(2), ubox(3)+xyadj(0), ubox(2)+xyadj(1), 11] ;right side
      DEVICE, COPY = [0, 0, ubox(3)-ubox(1)+4, 2,(ubox(1)+xyadj(0))-2, ubox(2)+xyadj(1)-2, 12] ;bottom line
      DEVICE, COPY = [0, 0, ubox(3)-ubox(1)+4, 2,(ubox(1)+xyadj(0))-2, ubox(4)+xyadj(1)+2, 12] ;top line
      DEVICE, COPY = [0, 0, 2, ubox(4)-ubox(2)+4, (ubox(1)+xyadj(0))-2, ubox(2)+xyadj(1)-2, 12] ;leftside 
      DEVICE, COPY = [0, 0, 2, ubox(4)-ubox(2)+4, (ubox(3)+xyadj(0))+2, ubox(2)+xyadj(1)-2, 12] ;right side

       xcen=moviev.box(1)+(moviev.box(3)-moviev.box(1))/2
       ycen=moviev.box(2)+(moviev.box(4)-moviev.box(2))/2

      if ubox(9) eq 1 then for nc=0,moviev.len-1 do DEVICE, COPY = [0, 0, 5, 5,xcen+moviev.centadj(nc,2)-2,ycen+moviev.centadj(nc,3)-2, 12]
      if ubox(9) eq 1 then for nc=0,moviev.len-1 do DEVICE, COPY = [0, 0, 3, 3,xcen+moviev.centadj(nc,2)-1,ycen+moviev.centadj(nc,3)-1, 11]
      if ubox(9) eq 1 then DEVICE, COPY = [0, 0, 7, 2,xcen+moviev.centadj(moviev.current,2)-3,ycen+moviev.centadj(moviev.current,3), 11]
      if ubox(9) eq 1 then DEVICE, COPY = [0, 0, 2, 7,xcen+moviev.centadj(moviev.current,2),ycen+moviev.centadj(moviev.current,3)-3, 11]

      IF moviev.showzoom eq 1 THEN BEGIN          ; Left mouse button pressed
          if keyword_set(event) then begin
	    moviev.zstate.xc=event.x & moviev.zstate.yc=event.y
          endif
          draw_zoom    
      ENDIF

      if moviev.trackspotby eq 'zoommode' and  moviev.showzoom eq 0 then begin
         moviev.zstate.x0=ubox(1)+xyadj(0)
         moviev.zstate.x1=ubox(3)+xyadj(0)
         moviev.zstate.y0=ubox(2)+xyadj(1)
         moviev.zstate.y1=ubox(4)+xyadj(1)
         moviev.zstate.xc=moviev.zstate.x0+abs(moviev.zstate.x1-moviev.zstate.x0) / 2L
         moviev.zstate.yc=moviev.zstate.y0+abs(moviev.zstate.y1-moviev.zstate.y0) / 2L
         sccSHOW_zoom   
      endif

END

pro box_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
common crop_mvi, timgs, doneframes

   tmp = INDGEN(N_ELEMENTS(moviev.win_index))
   good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
    
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN BEGIN
    	help,/str,moviev
	print,'In box_event.pro, '
	help,input
	wait,3
    ENDIF
    CASE (input) OF
	'Crop Movie' : BEGIN	;select box coords using mouse
                 if datatype(imgs) ne 'UND' and total(moviev.box(1:4)) gt 0 then begin
                   widget_control,moviev.instxt,set_value='**** Cropping Movie*******'
		   ;message,'Crop Movie',/info
                   moviev.box(0)=0
    	    	   if moviev.trackspotby eq 'zoommode'then moviev.box(1:4)=[moviev.zstate.x0,moviev.zstate.y0,moviev.zstate.x1,moviev.zstate.y1]
		   HDRS=moviev.img_hdrs(good) 
		   NAMES=moviev.frames(good)
                   deleted=intarr(n_elements(good))
                   typ=strmid(moviev.filename,strpos(moviev.filename,'.'),strlen(moviev.filename)-strpos(moviev.filename,'.'))
                   filename=strmid(moviev.filename,0,strpos(moviev.filename,'.'))+'_box'+typ
                   FILE_HDR=moviev.FILE_HDR
                   FILE_HDR.NX=(moviev.box(3)-moviev.box(1))+1
                   FILE_HDR.NY=(moviev.box(4)-moviev.box(2))+1
                   mccd_pos=moviev.file_hdr.CCD_POS ;[y1,x1,y2,x2]
                   ccdadj=(mccd_pos(2)-mccd_pos(0)+1)/moviev.file_hdr.ny
                   if moviev.trackspotby eq 'pixels' or moviev.trackspotby eq 'zoommode'then begin
                     FILE_HDR.ccd_pos(0)=mccd_pos(0)+(moviev.box(2)+1)*ccdadj
                     FILE_HDR.ccd_pos(2)=FILE_HDR.ccd_pos(0)+FILE_HDR.NY*ccdadj
                     FILE_HDR.ccd_pos(1)=mccd_pos(1)+(moviev.box(1)+1)*ccdadj
                     FILE_HDR.ccd_pos(3)=FILE_HDR.ccd_pos(1)+FILE_HDR.NX*ccdadj
                     if FILE_HDR.RTHETA eq 0 and total([file_hdr.RADIUS0,file_hdr.RADIUS1,file_hdr.THETA0,file_hdr.THETA1]) eq 0 then begin
                       FILE_HDR.SUNXCEN=FILE_HDR.SUNXCEN-moviev.box(1)
                       FILE_HDR.SUNYCEN=FILE_HDR.SUNYCEN-moviev.box(2)
		     ; NOTE!!! Definition of X/Ycen in MVI frame header and FITS header is not the same!!!
		     ; Header value of X/YCEN is now changed in scc_mkframe.pro to match MVI definition.
                       HDRS.xcen=HDRS.xcen-moviev.box(1)
                       HDRS.ycen=HDRS.ycen-moviev.box(2)
                         if TAG_EXIST(moviev, 'wcshdrs')then begin
                           moviev.wcshdrs.crpix(0)=moviev.wcshdrs.crpix(0)-moviev.box(1)
                           moviev.wcshdrs.crpix(1)=moviev.wcshdrs.crpix(1)-moviev.box(2)
			 endif
                     endif else begin
		    	if moviev.file_hdr.rtheta le 3 then begin 
    			  tht=moviev.file_hdr.theta0+moviev.box/(moviev.file_hdr.nx-1.)*(moviev.file_hdr.theta1-moviev.file_hdr.theta0)
    	    	          rad=moviev.file_hdr.radius0+moviev.box/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.radius1-moviev.file_hdr.radius0)
    	    	          if moviev.file_hdr.rtheta eq 2 then $
		    	    	rad=10^( alog10(moviev.file_hdr.radius0)+moviev.box/(moviev.file_hdr.ny-1.) $     	
    	    	    		  *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) )
    	    	        endif
	    	    	if moviev.file_hdr.rtheta ge 5 then begin
    	    		  rad=moviev.file_hdr.radius0+moviev.box/(moviev.file_hdr.nx-1.)*(moviev.file_hdr.radius1-moviev.file_hdr.radius0)
    	    	          tht=moviev.file_hdr.theta0+moviev.box/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.theta1-moviev.file_hdr.theta0)
    	    	          if moviev.file_hdr.rtheta eq 6 then $
	      	    	     rad=10^( alog10(moviev.file_hdr.radius0)+moviev.box/(moviev.file_hdr.nx-1.) $     	
		    	    	 *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) ) 
    	        	endif 
            	    	if moviev.file_hdr.rtheta eq 3 or moviev.file_hdr.rtheta eq 4 or moviev.file_hdr.rtheta eq 0 then begin
            	    	   rad=moviev.box*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0
            	           tht=moviev.box*moviev.file_hdr.sec_pix+moviev.file_hdr.theta0
    	        	endif
                        file_hdr.RADIUS0=rad(2)
		        file_hdr.RADIUS1=rad(4)
		        file_hdr.THETA0=tht(1)
		        file_hdr.THETA1=tht(3)
		        if moviev.file_hdr.rtheta eq 3 then begin 
                          FILE_HDR.SUNXCEN=FILE_HDR.SUNXCEN-moviev.box(1)
                          FILE_HDR.SUNYCEN=FILE_HDR.SUNYCEN-moviev.box(2)
                          HDRS.xcen=HDRS.xcen-moviev.box(1)
                          HDRS.ycen=HDRS.ycen-moviev.box(2)
                        endif else begin
                          FILE_HDR.SUNXCEN=0
                          FILE_HDR.SUNYCEN=0
                          HDRS.xcen=0
                          HDRS.ycen=0
                        endelse
                     endelse
                     if moviev.trackspotby eq 'zoommode'then begin
                        FILE_HDR.NX=FILE_HDR.NX*moviev.zstate.scale
                        FILE_HDR.NY=FILE_HDR.NY*moviev.zstate.scale
                        FILE_HDR.SUNXCEN=FILE_HDR.SUNXCEN*moviev.zstate.scale-moviev.zstate.scale/2.
                        FILE_HDR.SUNYCEN=FILE_HDR.SUNYCEN*moviev.zstate.scale-moviev.zstate.scale/2.
                        HDRS.xcen=HDRS.xcen*moviev.zstate.scale-moviev.zstate.scale/2.
                        HDRS.ycen=HDRS.ycen*moviev.zstate.scale-moviev.zstate.scale/2.
			FILE_HDR.sec_pix= FILE_HDR.sec_pix/moviev.zstate.scale
			HDRS.CDELT1=HDRS.CDELT1/moviev.zstate.scale
                         if TAG_EXIST(moviev, 'wcshdrs')then begin
                           moviev.wcshdrs.crpix(0)=moviev.wcshdrs.crpix(0)*moviev.zstate.scale-moviev.zstate.scale/2.
                           moviev.wcshdrs.crpix(1)=moviev.wcshdrs.crpix(1)*moviev.zstate.scale-moviev.zstate.scale/2.
                           moviev.wcshdrs.cdelt(0)=moviev.wcshdrs.cdelt(0)/moviev.zstate.scale
                           moviev.wcshdrs.cdelt(1)=moviev.wcshdrs.cdelt(1)/moviev.zstate.scale
			endif
                        IF file_hdr.truecolor le 0 THEN timgs=bytarr(FILE_HDR.NX,FILE_HDR.NY,n_elements(good)) else timgs=bytarr(3,FILE_HDR.NX,FILE_HDR.NY,n_elements(good))
                        FOR i=0,n_elements(good)-1 DO BEGIN 
                          timg=1
                          draw_zoom,zuse=good(i)+1,zoomimgout=timg
                          IF file_hdr.truecolor le 0 THEN timgs(*,*,i) = timg else timgs(*,*,*,i) =timg
                        endfor
                        imgs=timgs
			undefine,timgs
                     endif else begin
                       IF file_hdr.truecolor le 0 THEN imgs = imgs(moviev.box(1):moviev.box(3),moviev.box(2):moviev.box(4),good)  
                       IF file_hdr.truecolor eq 1 THEN imgs = imgs(*,moviev.box(1):moviev.box(3),moviev.box(2):moviev.box(4),good)
                     endelse
                   endif

                   if moviev.trackspotby eq 'Coordinate' then begin
                     if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs
    	    	     xcen=moviev.box(1)+(moviev.box(3)-moviev.box(1))/2
    	    	     ycen=moviev.box(2)+(moviev.box(4)-moviev.box(2))/2
		     clc=euvi_heliographic(moviev.wcshdrs(moviev.current),[xcen,ycen],/CARRINGTON)
                     IF file_hdr.truecolor le 0 THEN timgs = bytarr((moviev.box(3)-moviev.box(1))+1, (moviev.box(4)-moviev.box(2))+1 ,n_elements(moviev.frames))
                     IF file_hdr.truecolor eq 1 THEN timgs = bytarr(3,(moviev.box(3)-moviev.box(1))+1, (moviev.box(4)-moviev.box(2))+1 ,n_elements(moviev.frames))
                     centadj=fltarr(n_elements(moviev.frames),2)
		     FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
    	    	    	 WCS_CONVERT_to_COORD, moviev.wcshdrs(i), coord, 'HG', clc(0), clc(1), /CARRINGTON
                         cent=wcs_get_pixel(moviev.wcshdrs(i),coord)
                         if (FINITE(cent(0),/NAN)) and datatype(pcent) ne 'UND' then cent=pcent
                         if ~(FINITE(cent(0),/NAN)) and datatype(pcent) eq 'UND' then for i2=0,i do centadj(i2,0:1)=cent else centadj(i,0:1)=cent 
			 if ~(FINITE(cent(0),/NAN)) then pcent=cent
                     ENDFOR
                     centadj(*,1)=round(centadj(*,1)-ycen)
                     centadj(*,0)=round(centadj(*,0)-xcen)
                     xr=[0,moviev.box(1)+min(centadj(*,0)),moviev.box(3)+max(centadj(*,0)),moviev.FILE_HDR.NX]
                     yr=[0,moviev.box(2)+min(centadj(*,1)),moviev.box(4)+max(centadj(*,1)),moviev.FILE_HDR.NY]
    	    	     IF file_hdr.truecolor le 0 THEN tmpimg=bytarr(max(xr)-min(xr)+1,max(yr)-min(yr)+1) else tmpimg=bytarr(3,max(xr)-min(xr)+1,max(yr)-min(yr)+1)
		     FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                        widget_control,moviev.instxt,set_value='Working on Frame # '+string(i+1)
                         xyoff=centadj(i,0:1)
                         HDRS(i).xcen=HDRS(i).xcen-moviev.box(1)-xyoff(0)
                         HDRS(i).ycen=HDRS(i).ycen-moviev.box(2)-xyoff(1)
                         if TAG_EXIST(moviev, 'wcshdrs')then begin
                           moviev.wcshdrs(i).crpix(0)=moviev.wcshdrs(i).crpix(0)-moviev.box(1)-xyoff(0)
                           moviev.wcshdrs(i).crpix(1)=moviev.wcshdrs(i).crpix(1)-moviev.box(2)-xyoff(1)
			 endif
                         if i eq 0 then begin
                             FILE_HDR.SUNXCEN=FILE_HDR.SUNXCEN-moviev.box(1)+xyoff(0)
                             FILE_HDR.SUNYCEN=FILE_HDR.SUNYCEN-moviev.box(2)+xyoff(1)
                             FILE_HDR.ccd_pos(0)=0;mccd_pos(0)+(moviev.box(2)+1+xyoff(1))*ccdadj
                             FILE_HDR.ccd_pos(2)=0;FILE_HDR.ccd_pos(0)+FILE_HDR.NY*ccdadj
                             FILE_HDR.ccd_pos(1)=0;mccd_pos(1)+(moviev.box(1)+1+xyoff(0))*ccdadj
                             FILE_HDR.ccd_pos(3)=0;FILE_HDR.ccd_pos(1)+FILE_HDR.NX*ccdadj
                        endif
                         xzero=fix(-1*min(xr))
                         yzero=fix(-1*min(yr))
			 xyoff(0)=xyoff(0)+xzero
			 xyoff(1)=xyoff(1)+yzero
                         tmpimg=tmpimg*0
                         IF file_hdr.truecolor le 0 THEN tmpimg(xzero:moviev.FILE_HDR.NX-1+xzero,yzero:moviev.FILE_HDR.NY-1+yzero)=imgs(*,*,i)
                         IF file_hdr.truecolor eq 1 THEN tmpimg(*,xzero:moviev.FILE_HDR.NX-1+xzero,yzero:moviev.FILE_HDR.NY-1+yzero)=imgs(*,*,*,i)
;                         if moviev.box(4)+xyoff(1) lt moviev.FILE_HDR.NY and moviev.box(3)+xyoff(0) lt moviev.FILE_HDR.NX and moviev.box(1)+xyoff(0) ge 0 and moviev.box(2)+xyoff(1) ge 0 then begin
			     IF file_hdr.truecolor le 0 THEN timgs(*,*,i)=tmpimg(moviev.box(1)+xyoff(0):moviev.box(3)+xyoff(0),moviev.box(2)+xyoff(1):moviev.box(4)+xyoff(1))
                             IF file_hdr.truecolor eq 1 THEN timgs(*,*,*,i)=tmpimg(*,moviev.box(1)+xyoff(0):moviev.box(3)+xyoff(0),moviev.box(2)+xyoff(1):moviev.box(4)+xyoff(1))
;                         endif 
                     ENDFOR
                     IF file_hdr.truecolor le 0 THEN imgs = timgs(*,*,good) else imgs = timgs(*,*,*,good)
                     undefine,timgs
                  endif

                   if  moviev.trackspotby eq 'centers' then begin
                     IF file_hdr.truecolor le 0 THEN timgs = bytarr((moviev.box(3)-moviev.box(1))+1, (moviev.box(4)-moviev.box(2))+1 ,n_elements(moviev.frames))
                     IF file_hdr.truecolor eq 1 THEN timgs = bytarr(3,(moviev.box(3)-moviev.box(1))+1, (moviev.box(4)-moviev.box(2))+1 ,n_elements(moviev.frames))
                     xr=[0,moviev.box(1)+min(moviev.centadj(*,2)),moviev.box(3)+max(moviev.centadj(*,2)),moviev.FILE_HDR.NX]
                     yr=[0,moviev.box(2)+min(moviev.centadj(*,3)),moviev.box(4)+max(moviev.centadj(*,3)),moviev.FILE_HDR.NY]
    	    	     IF file_hdr.truecolor le 0 THEN tmpimg=bytarr(max(xr)-min(xr)+1,max(yr)-min(yr)+1) else tmpimg=bytarr(3,max(xr)-min(xr)+1,max(yr)-min(yr)+1)
		     FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                        widget_control,moviev.instxt,set_value='Working on Frame # '+string(i+1)
                         xyoff=moviev.centadj(i,2:3)
                         HDRS(i).xcen=HDRS(i).xcen-moviev.box(1)-xyoff(0)
                         HDRS(i).ycen=HDRS(i).ycen-moviev.box(2)-xyoff(1)
                         if TAG_EXIST(moviev, 'wcshdrs')then begin
                           moviev.wcshdrs(i).crpix(0)=moviev.wcshdrs(i).crpix(0)-moviev.box(1)-xyoff(0)
                           moviev.wcshdrs(i).crpix(1)=moviev.wcshdrs(i).crpix(1)-moviev.box(2)-xyoff(1)
			 endif
                         if i eq 0 then begin
                             FILE_HDR.SUNXCEN=FILE_HDR.SUNXCEN-moviev.box(1)+xyoff(0)
                             FILE_HDR.SUNYCEN=FILE_HDR.SUNYCEN-moviev.box(2)+xyoff(1)
                             FILE_HDR.ccd_pos(0)=0;mccd_pos(0)+(moviev.box(2)+1+xyoff(1))*ccdadj
                             FILE_HDR.ccd_pos(2)=0;FILE_HDR.ccd_pos(0)+FILE_HDR.NY*ccdadj
                             FILE_HDR.ccd_pos(1)=0;mccd_pos(1)+(moviev.box(1)+1+xyoff(0))*ccdadj
                             FILE_HDR.ccd_pos(3)=0;FILE_HDR.ccd_pos(1)+FILE_HDR.NX*ccdadj
                        endif
                         xzero=fix(-1*min(xr))
                         yzero=fix(-1*min(yr))
			 xyoff(0)=xyoff(0)+xzero
			 xyoff(1)=xyoff(1)+yzero
                         tmpimg=tmpimg*0
                         IF file_hdr.truecolor le 0 THEN tmpimg(xzero:moviev.FILE_HDR.NX-1+xzero,yzero:moviev.FILE_HDR.NY-1+yzero)=imgs(*,*,i)
                         IF file_hdr.truecolor eq 1 THEN tmpimg(*,xzero:moviev.FILE_HDR.NX-1+xzero,yzero:moviev.FILE_HDR.NY-1+yzero)=imgs(*,*,*,i)
;                         if moviev.box(4)+xyoff(1) lt moviev.FILE_HDR.NY and moviev.box(3)+xyoff(0) lt moviev.FILE_HDR.NX and moviev.box(1)+xyoff(0) ge 0 and moviev.box(2)+xyoff(1) ge 0 then begin
			     IF file_hdr.truecolor le 0 THEN timgs(*,*,i)=tmpimg(moviev.box(1)+xyoff(0):moviev.box(3)+xyoff(0),moviev.box(2)+xyoff(1):moviev.box(4)+xyoff(1))
                             IF file_hdr.truecolor eq 1 THEN timgs(*,*,*,i)=tmpimg(*,moviev.box(1)+xyoff(0):moviev.box(3)+xyoff(0),moviev.box(2)+xyoff(1):moviev.box(4)+xyoff(1))
;                         endif 
                     ENDFOR
                    
                     IF file_hdr.truecolor le 0 THEN imgs = timgs(*,*,good) else imgs = timgs(*,*,*,good)
                     undefine,timgs
                   endif
    	    	   evtpro=moviev.evtpro
	           if TAG_EXIST(moviev, 'wcshdrs')then wcshdrs=moviev.wcshdrs else wcshdrs=0
		   exit_playmovie
                   good = WHERE(deleted NE 1)
                   IF file_hdr.truecolor le 0 THEN imgs = imgs(*,*,good) else imgs = imgs(*,*,*,good)
                   hdrs=hdrs(good)
                   if datatype (wcshdrs) eq 'STC' then wcshdrs=wcshdrs(good)
                   movie_in=intarr(n_elements(good))
                   for nw=0,n_elements(movie_in)-1 do begin
                     WINDOW, XSIZE =file_hdr.nx, YSIZE = file_hdr.ny, /PIXMAP, /FREE
                     movie_in(nw) = !D.WINDOW
                     if file_hdr.truecolor eq 1 then tv,imgs(*,*,*,nw),true=1 else tv,imgs(*,*,nw)
		   endfor
                   if evtpro eq 'SCC_PLAYMOVIEM_DRAW' then SCC_playmoviem, movie_in, HDRS=HDRS, NAMES=NAMES, file_hdr=file_hdr, wcshdr=wcshdrs else $
		    	    	    	    	    	    	  SCC_playmovie,  movie_in, HDRS=HDRS, NAMES=NAMES, file_hdr=file_hdr,truecolor=file_hdr.truecolor, wcshdr=wcshdrs
                   RETURN
                 ENDIF else widget_control,moviev.instxt,set_value='You must Select box area before Cropping'

	    END

	'Track Spot' : BEGIN	
              !x.style=1
	      if datatype(imgs) ne 'UND' and total(moviev.box(1:4)) gt 0 then begin
                widget_control,moviev.instxt,set_value='**** Tracking spot ************'
                if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs
		if moviev.trackspotby ne 'pixels' then begin
                  cl2=euvi_heliographic(moviev.wcshdrs(moviev.current),[moviev.box(3),moviev.box(4)],/CARRINGTON)
                  cl1=euvi_heliographic(moviev.wcshdrs(moviev.current),[moviev.box(1),moviev.box(2)],/CARRINGTON)
                endif else begin
		  xyoff=[((moviev.box(3)-moviev.box(1))/2),((moviev.box(4)-moviev.box(2))/2)]
                  clc=euvi_heliographic(moviev.wcshdrs(moviev.current),[moviev.box(1)+xyoff(0),moviev.box(2)+xyoff(1)],/CARRINGTON)
                endelse
                sptav=fltarr(n_elements(moviev.frames))
                spttime=strarr(n_elements(moviev.frames))
		break_file,moviev.filename,di,pa,fi,sf
                if moviev.trackspotby eq 'coords' then $
		    spttitle=fi+sf+' ROI CRLN['+string(cl1[0],'(f7.2)')+','+string(cl2[0],'(f7.2)') $
		    	    	     +'],CRLT['+string(cl1[1],'(f7.2)')+','+string(cl2[1],'(f7.2)')+']' else $
		    spttitle='Center long='+string(clc(0),'(f7.2)')+' Center Lat ='+string(clc(1),'(f7.2)')
                
		winsave=!D.WINDOW
                if moviev.trackfile ne '' then begin
		    openw,tracklun,moviev.trackfile,/get_lun
		    
		    printf,tracklun,spttitle
                    if moviev.img_hdrs(0).DETECTOR ne 'EIT' then $
		    	sc='STEREO_'+strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1) else sc='SOHO'
		    printf,tracklun,'OBSRVTRY  ',sc
                    printf,tracklun,'DETECTOR  ',moviev.img_hdrs(0).DETECTOR
                    printf,tracklun,'FILTER    ',moviev.img_hdrs(0).FILTER
                    printf,tracklun,'WAVELNTH  ',moviev.img_hdrs(0).WAVELNTH
                    printf,tracklun,'DATE/TIME                    Total        N_pixels    Average'
                endif
		FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                     if moviev.deleted(i) ne 1 then begin
                       widget_control,moviev.instxt,set_value='Working on Frame # '+string(i)
                       spotmsk=fltarr(moviev.hsize,moviev.vsize)+40
                       if moviev.trackspotby ne 'pixels' then begin
                         clt=euvi_heliographic(moviev.wcshdrs(i),/CARRINGTON)                                  
                         cllng=reform(clt(0,*,*))
                         cllat=reform(clt(1,*,*))
                         cllng(where (~Finite(cllng)))=-3000
                         cllat(where (~Finite(cllat)))=-3000
                         if cl2(0) lt cl1(0) then begin
                              cllng(where(cllng lt 100))=cllng(where(cllng lt 100))+360 
			      cl2_0=cl2(0)+360
		         endif else cl2_0=cl2(0)
                         boxa=where(cllng ge cl1(0) and cllng le cl2_0 and cllat ge cl1(1) and cllat le cl2(1))
                         if boxa(0) gt -1 then spotmsk(boxa)=0
	
		       endif else begin
    	    	    	 WCS_CONVERT_to_COORD, moviev.wcshdrs(i), coord, 'HG', clc(0), clc(1), /CARRINGTON
                         cent=wcs_get_pixel(moviev.wcshdrs(i),coord)
                         cent=round(cent)
                         tst=where(~Finite(cent))
                         if tst(0) gt -1 then cent(where (~Finite(cent)))=-3000
			 if fix(cent(1))+xyoff(1) lt moviev.FILE_HDR.NY and fix(cent(0))+xyoff(0) lt moviev.FILE_HDR.NX and fix(cent(0))-xyoff(0) ge 0 and fix(cent(1))-xyoff(1) ge 0 then $
			      spotmsk(fix(cent(0))-xyoff(0):fix(cent(0))+xyoff(0),fix(cent(1))-xyoff(1):fix(cent(1))+xyoff(1))=0
                         boxa=where(spotmsk eq 0)
		       endelse
                       if boxa(0) gt -1 then begin
                         timg=reform(imgs(*,*,i))
                         print,'Frame # ',i,' Total = ', total(timg(boxa)),' N pixels = ',n_elements(boxa),' Average = ',total(timg(boxa))/n_elements(boxa)
                         if moviev.trackfile ne '' then printf,tracklun,moviev.img_hdrs(i).DATE_OBS,' ',moviev.img_hdrs(i).TIME_OBS,total(timg(boxa)),n_elements(boxa),total(timg(boxa))/n_elements(boxa)
                         sptav(i)=total(timg(boxa))/n_elements(boxa)
                         spttime(i)=moviev.img_hdrs(i).DATE_OBS+' '+moviev.img_hdrs(i).TIME_OBS
                       endif
                       wset,moviev.win_index(i)
                       tv,(fix(imgs(*,*,i))-spotmsk)>0
                       if i eq n_Elements(moviev.frames)-1 then begin
                          window,/free
			  wheresptavgt0=where(sptav gt 0,nwg)
			  help,wheresptavgt0
			  IF nwg LT 2 THEN stop
			  
                          utplot,spttime(wheresptavgt0),sptav(wheresptavgt0),/ynozero,title=spttitle, ytitle='Average Byte Value in ROI', charsi=1.5
		       stop
		       endif
                     endif
               ENDFOR
              	if moviev.trackfile ne '' then BEGIN
    	    	    close,tracklun
              	    FREE_LUN,tracklun
              	    message,'Values  saved in '+moviev.trackfile,/info
	    	ENDIF
              wset,winsave
	      moviev.box(0:4)=0
              moviev.box(7:9)=0
              moviev.forward = moviev.dirsave
              moviev.stall = moviev.stallsave
              WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall   ; Request another timer event
              widget_control,moviev.instxt,set_value='Press select to redo or cancel to exit'
	    ENDIF else widget_control,moviev.instxt,set_value='You must Select box area before Tracking Spot'

	    END

	'Run time avg diff' : BEGIN	;select box coords using mouse
                 if total(moviev.box(1:4)) gt 0 then begin
                   widget_control,moviev.instxt,set_value='**** Calling SCC_SYNOPTIC.PRO ****'
		   ;message,'Crop Movie',/info
		   
                   filenames=moviev.img_hdrs(good).filename
                   files=mvifindfits(moviev.img_hdrs[good])
                   if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs,/current,wcs=wcs else wcs=moviev.wcshdrs[good[0]]
                   cl2=euvi_heliographic(wcs,[moviev.box(3),moviev.box(4)],/CARRINGTON)
                   cl1=euvi_heliographic(wcs,[moviev.box(1),moviev.box(2)],/CARRINGTON)
                   ncoords=1
                   coords=[cl1[0],cl2[0],cl1[1],cl2[1]]
                   trackfile=moviev.trackfile & debugon=moviev.debugon
                   trackspotby=moviev.trackspotby
		   exit_playmovie
		   scc_synoptic, files, times, datacube, COORDS=coords, NCOORDS=ncoords, SCALE=float(trackspotby), $
		    	/display,/applycorr,LOUD=debugon, SAVEFILE=trackfile
		   RETURN
                 ENDIF else widget_control,moviev.instxt,set_value='You must Select box area before running time avg diff'

              END

    	'create omap': BEGIN	
                 if total(moviev.box(1:4)) gt 0 then begin
                   widget_control,moviev.instxt,set_value='****Creating Omap*******'
		   ;message,'Crop Movie',/info
                   good = WHERE(moviev.deleted NE 1)
                   filenames=moviev.img_hdrs(good).filename
                   files=sccfindfits(moviev.img_hdrs(good).filename)
                  savefile=moviev.trackfile 
                   break_file,savefile,disk,dir,root,ext
		   if ext ne '' then savefile=savefile		                     
    	    	    if moviev.box(4) lt moviev.box(3) then radius=(moviev.box(3)-moviev.box(4)) else radius=(moviev.box(4)-moviev.box(3))
                    mvi2omap, moviev.box(1),moviev.box(2), radius/2., radius, float(moviev.trackspotby),/display,/png
		   RETURN
                 ENDIF else widget_control,moviev.instxt,set_value='You must Select box area before creating Omap'

              END


	'box cancel' : BEGIN	
                   moviev.box(0:4)=0
                   moviev.box(7:9)=0
                   moviev.instxt=0
                   moviev.forward = moviev.dirsave
                   moviev.stall = moviev.stallsave
                   WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall   ; Request another timer event
	           WIDGET_CONTROL, /DESTROY, moviev.boxbase
                   moviev.boxbase=-1
                   if moviev.trackspotby eq 'zoommode' then sccSHOW_ZOOM
                   moviev.trackspotby='pixels'
                   if tag_exist(moviev,'centadj')then moviev=rem_tag(moviev,'centadj')
                   
	 END

	'Select box' : BEGIN	
		   moviev.box(0:4)=1	
                   if tag_exist(moviev,'centadj')then begin 
                         moviev.centadj(*,0:1)=-1
  	    	         moviev.centadj(*,0:3)=0
                   endif
                   if moviev.trackspotby eq 'zoommode' then sccSHOW_ZOOM
                   WSET, moviev.draw_win
                   DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                   widget_control,moviev.instxt,set_value='Use mouse to select lower left corner'
                   if moviev.boxsel eq 'o_map' then begin
                      widget_control,moviev.instxt,set_value='use mouse to select center'
		      moviev.box(0)=4	
                   endif
	    END

        'select centers' : BEGIN
	              moviev.box(9) = ev.select
                      if ev.select eq 1 then begin
	                if moviev.box(0) eq 3 then widget_control,moviev.instxt,set_value='select frame to center and use mouse to recenter' else $
			widget_control,moviev.instxt,set_value='select crop area before selecting centers'    
                        if ~tag_exist(moviev,'centadj')then begin 
		          centadj=fltarr(moviev.len,4)
                          centadj(*,0:1)=-1
			  centadj(moviev.current,0:1)=0
		          moviev=add_tag(moviev,centadj,'centadj')
                        endif else begin
			   moviev.centadj(*,0:1)=-1
  	    	           moviev.centadj(*,2:3)=0
                           moviev.centadj(moviev.current,0:1)=0
			endelse
                       moviev.trackspotby='centers'
                    endif else begin
                       if tag_exist(moviev,'centadj')then begin 
                         moviev.centadj(*,0:1)=-1
  	    	         moviev.centadj(*,2:3)=0
			 moviev.centadj(moviev.current,0:1)=0
                       endif
                       moviev.trackspotby='Pixels'
                    endelse
    	    END

        'TRCKSPTBY' : moviev.trackspotby=ev.str

	'sptfilev' :  BEGIN 	
	             WIDGET_CONTROL, ev.id, GET_VALUE=val
       	             fname=val[0]
                     moviev.trackfile=fname
    	    END

	'PKFILE' :   BEGIN 	
	           fname = PICKFILE(file=moviev.trackfile, filter='*.txt', TITLE='Select track_spot output file')
                   if fname ne moviev.trackfile then begin
                     moviev.trackfile=fname
                     widget_control,moviev.sptfilev,set_value=moviev.trackfile
		   endif
    	    END

	'boxxsize' :  BEGIN 	
	             WIDGET_CONTROL, moviev.box(5), GET_VALUE=val
                     moviev.box(3)=moviev.box(1)+val-1
	             WIDGET_CONTROL, moviev.box(6), GET_VALUE=val2
                     if val2 eq 0 then begin
                       moviev.box(4)=moviev.box(2)+val-1
	               WIDGET_CONTROL, moviev.box(6), SET_VALUE=val
		     endif
                     if moviev.trackspotby eq 'centers' then moviev.box(0)=7 else moviev.box(0)=3
                     if moviev.trackspotby eq 'zoommode' then sccSHOW_ZOOM
                     draw_box
    	    END

	'boxysize' :  BEGIN 	
	             WIDGET_CONTROL, moviev.box(6), GET_VALUE=val
                     moviev.box(4)=moviev.box(2)+val-1
	             WIDGET_CONTROL, moviev.box(5), GET_VALUE=val2
                     if val2 eq 0 then begin
                       moviev.box(3)=moviev.box(1)+val-1
	               WIDGET_CONTROL, moviev.box(5), SET_VALUE=val
		     endif
                     if moviev.trackspotby eq 'zoommode' then sccSHOW_ZOOM
                     moviev.box(0)=3
                     draw_box
    	    END

        'autov' : moviev.box(7) = ev.select

        'extframe' : moviev.box(8) = ev.select

	ELSE : BEGIN
                 PRINT, '%%box movie.  Unknown event ',input
	     END

    ENDCASE

END 

pro box_control,type
;moviev.box {0= 0 not set 1 bottom selected 2 top selected 3 center box
;            1= x1
;            2= y1
;            3= x2
;            4= y2
;            5= xsize
;            6= ysize
;            7= Right click advance
;            8= stop at frames edge
;            9= select centers}  

COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
common crop_mvi
if moviev.boxbase ne -1 and type eq moviev.boxsel then begin
   WIDGET_CONTROL, moviev.boxbase, MAP=0
   WIDGET_CONTROL, moviev.boxbase, MAP=1
endif 
if moviev.boxbase ne -1 and type ne moviev.boxsel then begin
    moviev.box(0:4)=0
    moviev.instxt=0
    moviev.forward = moviev.dirsave
    moviev.stall = moviev.stallsave
    WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall   ; Request another timer event
    WIDGET_CONTROL, /DESTROY, moviev.boxbase
    moviev.boxbase=-1
    if moviev.trackspotby eq 'zoommode' then sccSHOW_ZOOM
    moviev.trackspotby='pixels'
    if tag_exist(moviev,'centadj')then moviev=rem_tag(moviev,'centadj')
endif 
if moviev.boxbase eq -1 then begin
  moviev.boxsel=type 
  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
  doneframes=intarr(moviev.len)
  ; init for box_event
  if datatype(imgs) eq 'BYT' then begin
        WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
        moviev.stall = 10000.
	moviev.box(0)=1 
        WSET, moviev.draw_win
        DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]

    	title='Select ROI'
        if moviev.boxsel eq 'crop' then title='CROP CONTROL'
        if moviev.boxsel eq 'spot' then title='SPOT TRACKING CONTROL'
        if moviev.boxsel eq 'zoom' then title='ZOOM CONTROL'

	boxbase = WIDGET_BASE(/col, TITLE=title)

	rowb = WIDGET_BASE(boxbase, /ROW,/frame)
	if moviev.boxsel eq 'zoom' then instxt =   widget_text(rowb, VALUE='',  UVALUE='boxinst' , XSIZE=40, YSIZE=1) else $
	    instxt =   widget_text(rowb, VALUE='Use mouse to select lower left corner',  UVALUE='boxinst' , XSIZE=40, YSIZE=1)

	moviev.box(5) =   cw_field(rowb, VALUE='0',  UVALUE='boxxsize' , XSIZE=4, YSIZE=1, /RETURN_EVENT, title='XSIZE :')
	moviev.box(6) =   cw_field(rowb, VALUE='0',  UVALUE='boxysize' , XSIZE=4, YSIZE=1, /RETURN_EVENT, title='YSIZE :')
 

	rowc = WIDGET_BASE(boxbase, /ROW)
	tel=moviev.img_hdrs(0).DETECTOR
	if moviev.boxsel eq 'crop' then begin
	    tmp = CW_BGROUP(rowc,  ['Select Frame Centers','Right click Advance','Stop on frame edges'],/NONEXCLUSIVE, /ROW, $
	         BUTTON_UVALUE = ['select centers','autov', 'extframe'])

	rowc1 = WIDGET_BASE(boxbase, /ROW)
	    tmp = CW_BGROUP(rowc1,  ['SELECT Crop Area', 'Crop Movie',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['Select box' ,'Crop Movie','box cancel']);,'Crop Frame'
              moviev.trackspotby='pixels'
	      rowc2 = WIDGET_BASE(boxbase, /ROW)
            if tel eq 'EIT' or tel eq 'EUVI' or tel EQ 'AIA' then begin
                bxlbl=widget_label(rowc2,value='Select center by :')
	        sptrkb= WIDGET_COMBOBOX(rowc2, VALUE=['Pixels','Coordinate'], UVALUE="TRCKSPTBY")
                WIDGET_CONTROL, sptrkb, SET_COMBOBOX_SELECT= 0
                WIDGET_CONTROL, sptrkb, SENSITIVE=1
	    endif



;          autot=['off','on'] & moviev.box(7)=0
;          autov=widget_droplist(rowc2, VALUE=autot, UVALUE='autov',title='Auto Advance :')
;          widget_control,autov,set_droplist_select=moviev.box(7)   
        endif
 	if moviev.boxsel eq 'spot' then begin
                bxlbl=widget_label(rowc,value='Track Spot by :')
	        sptrkb= WIDGET_COMBOBOX(rowc, VALUE=['coords','pixels'], UVALUE="TRCKSPTBY")
                WIDGET_CONTROL, sptrkb, SET_COMBOBOX_SELECT= 0
                WIDGET_CONTROL, sptrkb, SENSITIVE=1
                moviev.trackspotby='coords'
                tmp = CW_BGROUP(rowc,  ['Re-select ROI', 'Continue',' CANCEL '], /ROW, $
	                BUTTON_UVALUE = ['Select box' ,'Track Spot','box cancel'])
	        rowc3 = WIDGET_BASE(boxbase, /ROW)
                trackfile=''
	        sptfilev =   CW_FIELD(rowc3, VALUE=trackfile, /ALL_EVENTS, XSIZE=45, YSIZE=1, UVALUE='sptfilev', title='Output Filename: ')
	        pkfile = WIDGET_BUTTON(rowc3, VALUE=' PICKFILE ', UVALUE='PKFILE')
                IF (~TAG_EXIST(moviev, 'sptfilev')) THEN  moviev=add_tag(moviev,sptfilev,'sptfilev') else moviev.sptfilev=sptfilev
                IF (~TAG_EXIST(moviev, 'trackfile')) THEN  moviev=add_tag(moviev,trackfile,'trackfile') else moviev.trackfile=trackfile
        endif
	
 	if moviev.boxsel eq 'timediff' then begin
                bxlbl=widget_label(rowc,value='Carr Deg per pixel of output:')
	        sptrkb= WIDGET_COMBOBOX(rowc, VALUE=['1.0','0.5','0.25','0.125','0.0625'], UVALUE="TRCKSPTBY")
                WIDGET_CONTROL, sptrkb, SET_COMBOBOX_SELECT= 3
                WIDGET_CONTROL, sptrkb, SENSITIVE=1
                moviev.trackspotby='0.125'
                tmp = CW_BGROUP(rowc,  ['Re-select ROI', 'Continue',' CANCEL '], /ROW, $
	                BUTTON_UVALUE = ['Select box' ,'Run time avg diff','box cancel'])
	        rowc3 = WIDGET_BASE(boxbase, /ROW)
                trackfile=''
	        sptfilev =   CW_FIELD(rowc3, VALUE=trackfile, /ALL_EVENTS, XSIZE=45, YSIZE=1, UVALUE='sptfilev', title='Output Filename: ')
	        pkfile = WIDGET_BUTTON(rowc3, VALUE=' PICKFILE ', UVALUE='PKFILE')
                IF (~TAG_EXIST(moviev, 'sptfilev')) THEN  moviev=add_tag(moviev,sptfilev,'sptfilev') else moviev.sptfilev=sptfilev
                IF (~TAG_EXIST(moviev, 'trackfile')) THEN  moviev=add_tag(moviev,trackfile,'trackfile') else moviev.trackfile=trackfile
        endif

 	if moviev.boxsel eq 'o_map' then begin
                widget_control,instxt,set_value='use mouse to select center'
	        moviev.box(0)=4 
                WINDOW, XSIZE = moviev.hsize, YSIZE = moviev.vsize, /PIXMAP, TITLE='SCC_PLAYMOVIE temp'
                boxwin=!d.window
                WSET, moviev.draw_win
                bxlbl=widget_label(rowc,value='Deg per pixel :')
	        sptrkb= WIDGET_COMBOBOX(rowc, VALUE=['1.0','0.5','0.25'], UVALUE="TRCKSPTBY")
                WIDGET_CONTROL, sptrkb, SET_COMBOBOX_SELECT= 1
                WIDGET_CONTROL, sptrkb, SENSITIVE=1
                moviev.trackspotby='0.5'
                tmp = CW_BGROUP(rowc,  ['Re-select ROI', 'Continue',' CANCEL '], /ROW, $
	                BUTTON_UVALUE = ['Select box' ,'create omap','box cancel'])
	        rowc3 = WIDGET_BASE(boxbase, /ROW)
                trackfile=''
	        sptfilev =   CW_FIELD(rowc3, VALUE=trackfile, /ALL_EVENTS, XSIZE=45, YSIZE=1, UVALUE='sptfilev', title='Output Filename: ')
	        pkfile = WIDGET_BUTTON(rowc3, VALUE=' PICKFILE ', UVALUE='PKFILE')
                IF (~TAG_EXIST(moviev, 'sptfilev')) THEN  moviev=add_tag(moviev,sptfilev,'sptfilev') else moviev.sptfilev=sptfilev
                IF (~TAG_EXIST(moviev, 'trackfile')) THEN  moviev=add_tag(moviev,trackfile,'trackfile') else moviev.trackfile=trackfile
                IF (~TAG_EXIST(moviev, 'boxwin')) THEN  moviev=add_tag(moviev,boxwin,'boxwin') else moviev.boxwin=boxwin
        endif
        


	if moviev.boxsel eq 'zoom' then begin
    	       sample = 1L
    	    	scale = 2L
    	     zstate={        orig_image:	BYTARR(moviev.hsize,moviev.vsize), $
		zoom_win:	-1L, $
		x_im_sz:	moviev.hsize, $
		y_im_sz:	moviev.vsize, $
		scale:		scale, $
		sample:		sample, $
		x_zm_sz:	0, $
		y_zm_sz:	0, $
		xc:		moviev.hsize / 2L, $
		yc:		moviev.vsize / 2L, $
		x0:		0L, $
		y0:		0L, $
		x1:		moviev.hsize*1l, $
		y1:		moviev.vsize*1l, $
                timer:          0.01 $

            }

    	    IF (~TAG_EXIST(moviev, 'zstate')) THEN  moviev=add_tag(moviev,zstate,'zstate') else moviev.zstate=zstate
	    moviev.box(0)=0
	    tmp = CW_BGROUP(rowc,  ['SELECT zoom Area','Crop/Zoom Movie'], /ROW, $
	      BUTTON_UVALUE = ['Select box' ,'Crop Movie']);,'Crop Frame'
              moviev.trackspotby='zoommode'
    	     desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
    	     tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)
	     tmp = CW_BGROUP(rowc,  [' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['box cancel']);,'Crop Frame'
    	    ; rowb = WIDGET_BASE(boxbase, /ROW)
    	    ; if moviev.len ne 1 then    tmp = CW_BGROUP(rowb,  ['Prev. Frame','Next Frame', 'Continuous'], /ROW,  $
	    ;      BUTTON_UVALUE = ['PREV','NEXT', 'CONTINUOUS'], EVENT_FUNCT='SCC_MOVIE_CONTROL_FUNC')
            ;  tmp = CW_BGROUP(rowb,  ['Close Window'], /ROW, BUTTON_UVALUE = ['ZOOM'], EVENT_FUNCT='SCC_MOVIE_CONTROL_FUNC')
  	     rowd = WIDGET_BASE(boxbase, /ROW)
             moviev.zstate.scale=2
    	     zslide = WIDGET_SLIDER(rowd, TITLE='Scale Factor', VALUE=moviev.zstate.scale, UVALUE='ZSCALE', $
            	     /DRAG, /SCROLL, MIN=1 , MAX=10, EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
    	     moviev.zslide=zslide
             sccSHOW_zoom
         endif
 

        moviev.instxt=instxt
        moviev.boxbase=boxbase
	basexoff=moviev.basexoff
	baseyoff=moviev.baseyoff
        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=boxbase,/vertical
	widget_control,boxbase,XOFFSET=moviev.basexoff(0), YOFFSET=moviev.baseyoff(0)
	WIDGET_CONTROL, boxbase, /REALIZE

	XMANAGER, 'box_movie', boxbase, EVENT_HANDLER='box_event'
  ENDIF	
endif
end
    	;**-----------------End box widget ------------------**

;----------------mask widget --------------------------------------
pro mask_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON
COMMON mask_common, mskim, maskv, mskbase, msklead

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN BEGIN
    	help,/str,ev
	print,'In mask_event.pro, '
	help,input
	wait,3
    ENDIF
    CASE (input) OF
        'mask0' : BEGIN
	               maskv(0)=ev.index
		END
        'mask1' : BEGIN
	               maskv(1)=ev.value
		END
        'mask2' : BEGIN
	               maskv(2)=ev.value
		END
        'mask3' : BEGIN
	               maskv(3)=ev.index
		END
        'maskcl' : BEGIN
	               maskv(4)=ev.value
		END
        'maskcllim' : BEGIN
	               maskv(5)=ev.value
		END
        'masklimthck' : BEGIN
	               maskv(6)=ev.value
		END
        'maskadjx' : BEGIN
	               maskv(7)=ev.value
		END
        'maskadjy' : BEGIN
	               maskv(8)=ev.value
		END

         'MSKPREV': BEGIN              ; 
    	    	  IF moviev.debugon THEN stop
                  det=moviev.img_hdrs(0).detector
                  dindx=where(['C2','C3','EIT'] EQ det,issoho)
                  if issoho then begin
                     load_soho_spice 
		     sc='SOHO'
                  endif else sc=strupcase(strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1))
                  arcs=moviev.file_hdr.sec_pix
                  sunxcen=moviev.file_hdr.sunxcen
                  sunycen=moviev.file_hdr.sunycen
                ;  IF arcs LE 0 and ~issoho THEN BEGIN
                 IF arcs LE 0 THEN BEGIN
		    fhdr=convert2secchi(moviev.img_hdrs(0),moviev.file_hdr)
                    arcs=fhdr.cdelt1
                    sunxcen=fhdr.CRPIX1
                    sunycen=fhdr.CRPIX2
                  ENDIF
		;  IF issoho THEN begin
                ;     IF arcs LE 0 and issoho THEN arcs = GET_SEC_PIXEL(moviev.img_hdrs(0), FULL=hsize)
		;      fhdr=mk_lasco_hdr(moviev.img_hdrs(0))
		;      sunxcen=fhdr.crpix1/(fhdr.naxis1/moviev.file_hdr.nx)	; account for structure defn mismatch
		;      sunycen=fhdr.crpix2/(fhdr.naxis2/moviev.file_hdr.ny)
                ;  endif
                  set_plot,'z'     
		  device,set_resolution=[moviev.hsize,moviev.vsize]
                  erase,0
                  mskim=bytarr(moviev.hsize,moviev.vsize)
		  if maskv(0) eq 1 then begin
                    CASE STRLOWCASE(moviev.img_hdrs(0).detector) OF
          		'cor1' : maskfile=getenv('SECCHI_CAL')+'/cor1_mask.fts'
          		'cor2' : maskfile=getenv('SECCHI_CAL')+'/cor2'+sc+'_mask.fts'
          		'hi1'  : maskfile=''
          		'hi2'  : maskfile=getenv('SECCHI_CAL')+'/hi2'+sc+'_mask.fts'
          		'euvi' : maskfile=getenv('SECCHI_CAL')+'/euvi_mask.fts'
         	    	ELSE   : maskfile=''
        	     ENDCASE
                     mskfile=file_search(maskfile)
        	     if maskfile ne '' then begin
                        mskim=sccreadfits(mskfile,mhdr)
                        test=(mhdr.naxis1/(moviev.hsize*1.0)+mhdr.naxis2/(moviev.vsize*1.0))-(mhdr.naxis1/(moviev.hsize)+mhdr.naxis2/(moviev.vsize))
		        if test eq 0 then tv,rebin(mskim*255,moviev.hsize,moviev.vsize) else erase,255
                        mskim=tvrd()
                     endif else mskim=-1
                   endif else begin
                      mskim=1
                      if STRLOWCASE(moviev.img_hdrs(0).detector) ne 'hi2' then begin
                        if strlen(moviev.img_hdrs(0).date_obs) gt 10 then date=moviev.img_hdrs(0).date_obs else date=moviev.img_hdrs(0).date_obs+' '+moviev.img_hdrs(0).time_obs
                        hee_r= GET_STEREO_LONLAT(date, sc, /au, /degrees , system='HEE',_extra=_extra)
                        r_sun = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0) ; convert from radian to arcs
                        r_sun = r_sun/arcs  ; radius of sun (pixels)
                        r_occ = r_sun * maskv(1)
                        r_occ_out = r_sun * maskv(2)
                        ;if abs(maskv(7)) gt 0 then xadj=(r_sun/maskv(7)) else xadj=0
                        ;if abs(maskv(8)) gt 0 then yadj=(r_sun/maskv(8)) else yadj=0
		        if r_occ_out gt 0 then TVCIRCLE, r_occ_out,sunxcen,sunycen, /FILL, COLOR=255
                        if r_occ gt 0 then TVCIRCLE, r_occ,sunxcen+maskv(7),sunycen+maskv(8), /FILL, COLOR=0
		      endif else if STRLOWCASE(moviev.img_hdrs(0).detector) eq 'hi2' and  maskv(2) gt 0 then TVCIRCLE, maskv(2),maskv(7),maskv(8), /FILL, COLOR=255
                      mskim=tvrd()
                   endelse
                   lmb=-1
                   if maskv(3) eq 1 and mskim(0) ne -1 then begin
                        if datatype(r_sun) eq 'UND' then begin
                          if strlen(moviev.img_hdrs(0).date_obs) gt 10 then date=moviev.img_hdrs(0).date_obs else date=moviev.img_hdrs(0).date_obs+' '+moviev.img_hdrs(0).time_obs
                          hee_r= GET_STEREO_LONLAT(date, sc, /au, /degrees , system='HEE',_extra=_extra)
                          r_sun = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0) ; convert from radian to arcs
                          r_sun = r_sun/arcs  ; radius of sun (pixels)
		       endif
                        erase,0
		        TVCIRCLE, r_sun, sunxcen,sunycen, COLOR=255, THICK=maskv(6) 
                        lmb=tvrd()
		        z=where(lmb gt 200)
                        if z(0) gt -1 then mskim(z)=200
                   endif
                   if moviev.file_hdr.truecolor eq 1 then begin
 		            img=imgs[*,*,*,moviev.current]
			    for nc = 0,2 do begin
                               timg=reform(imgs(nc,*,*,moviev.current))
		               timg(where(mskim lt 99))=maskv(4)
		               img(nc,*,*)=timg
		               z=where(mskim eq 200)
                               if z(0) gt -1 then timg(z)=maskv(5)
		               img(nc,*,*)=timg
                            endfor 
	           endif else begin
 		             img=imgs[*,*,moviev.current]
 		             img(where(mskim lt 99))=maskv(4)
		             z=where(mskim eq 200)
                             if z(0) gt -1 then img(z)=maskv(5)
	           endelse
                   select_windows ;set_plot,'x'
		   WSET, moviev.draw_win
		   tv,img,true=moviev.truecolor
		END                

         'MSKDONE': BEGIN              ; 
                    if mskim(0) ne -1 then begin
		       FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                           widget_control,moviev.instxt,set_value='Adding Mask to frame # '+string(i)
                           if moviev.file_hdr.truecolor eq 1 then begin
 		             img=imgs[*,*,*,i]
			     for nc = 0,2 do begin
                               timg=reform(imgs(nc,*,*,i))
		               timg(where(mskim lt 99))=maskv(4)
		               img(nc,*,*)=timg
		               z=where(mskim eq 200)
                               if z(0) gt -1 then timg(z)=maskv(5)
		               img(nc,*,*)=timg
                             endfor 
                             imgs[*,*,*,i] = img
	                   endif else begin
 		              img=imgs[*,*,i]
 		              img(where(mskim lt 99))=maskv(4)
		              z=where(mskim eq 200)
                              if z(0) gt -1 then img(z)=maskv(5)
                              imgs[*,*,i] = img
	                   endelse
                           WSET,moviev.win_index(i)
	                   tv, img, true=moviev.truecolor
                      ENDFOR
                       WIDGET_CONTROL, /DESTROY, mskbase
                       WIDGET_CONTROL, /DESTROY, msklead
                       moviev.instxt=0
                       return
		    endif else widget_control,moviev.instxt,set_value='Mask must be Previewed before being applied '
		END                


          'MSKCANCEL': BEGIN              ; 
                   WIDGET_CONTROL, /DESTROY, mskbase
                   WIDGET_CONTROL, /DESTROY, msklead
                   moviev.instxt=0
                  return
		END                
 



	ELSE : BEGIN
                 PRINT, '%%Mask_setect.  Unknown event ',input
	     END

    ENDCASE

END 

pro add_mask
COMMON scc_playmv_COMMON
COMMON mask_common

        mskim=-1
        det=moviev.img_hdrs(0).detector
        dindx=where(['C2','C3','EIT'] EQ det,issoho)
        dindx=where(['C2'] EQ det,isc2)
        dindx=where(['C3'] EQ det,isc3)
        cindx=where(['C2','C3','COR2','COR1'] EQ det,iscor)
        eindx=where(['EUVI','EIT'] EQ det, iseuv)
        dindx=where(['HI2'] EQ det, ishi2)
        cindx=where(['COR2'] EQ det, iscor2)
        dindx=where(['COR1'] EQ det, iscor1)
        if ~issoho then sc=strupcase(strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.')-1,1)) else sc='S'
        maskv=fltarr(9) +0
        if iscor2 and sc eq 'A' then maskv =[0,2.6,15.0,0,0,255,3,0,0]
	if iscor2 and sc eq 'B' then  maskv =[0,3.3,16.0,0,0,255,3,0,-9]
	if iscor1 then maskv =[0,1.2,4.0,0,0,255,3,0,0]
	if iseuv then maskv =[0,0,1.6,0,0,0,0,0,0]
        if isc3 then maskv=[0,4.0,29.0,0,0,255,3,0,0]
        if isc2 then maskv=[0,2.2,7.5,0,0,255,3,0,0]
        if ishi2 then maskv=[0,0,260,0,0,0,0,moviev.hsize/2,moviev.vsize/2]
        msklead = Widget_Base(Map=0)
        Widget_Control, msklead, /Realize
	mskbase = WIDGET_BASE(/COL, TITLE='SELECT MASK',group_leader=msklead,/modal)
;	rowa = WIDGET_BASE(mskbase, /ROW, /FRAME)
; 	tmp  = WIDGET_LABEL( rowa,value='frames times may be overwritten by mask')
; 	tmp  = WIDGET_LABEL( rowa,value='use annotate to add times after mask is applied')

	rowb = WIDGET_BASE(mskbase, /ROW)
	col1 = WIDGET_BASE(rowb, /COLUMN)
 	tmp  = WIDGET_LABEL( col1,value='frames times may be overwritten by mask')
 	tmp  = WIDGET_LABEL( col1,value='use annotate to add times after mask is applied')

	col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)

; use CW_FIELD function

        if ~issoho then mask0 =   widget_droplist(col2, VALUE=['NO','YES'], UVALUE='mask0',title='Use Mask File (FFV only):')
        if ~ishi2 and ~iseuv then $
	    mask1 =   CW_FIELD(col2, VALUE=maskv(1), /ALL_EVENTS, UVALUE='mask1' , XSIZE=15, YSIZE=1, title='Inner Mask Size (Rs)=')
        if (iscor2 and sc eq 'B') then begin
            tmp  = WIDGET_LABEL( col2,value='Adjust Inner Mask center by n pixels')
            maskadjx =   CW_FIELD(col2, VALUE=maskv(7), /ALL_EVENTS, UVALUE='maskadjx' , XSIZE=15, YSIZE=1, title='Adjust X : ')
            maskadjy =   CW_FIELD(col2, VALUE=maskv(8), /ALL_EVENTS, UVALUE='maskadjy' , XSIZE=15, YSIZE=1, title='Adjust Y : ')
	endif
        if ishi2 then begin
	    tmp  = WIDGET_LABEL( col2,value='Outer Mask Center (Pixels)')    
            maskadjx =   CW_FIELD(col2, VALUE=maskv(7), /ALL_EVENTS, UVALUE='maskadjx' , XSIZE=15, YSIZE=1, title='X Center =')
            maskadjy =   CW_FIELD(col2, VALUE=maskv(8), /ALL_EVENTS, UVALUE='maskadjy' , XSIZE=15, YSIZE=1, title='Y Center =')
	    mask2 =   CW_FIELD(col2, VALUE=maskv(2), /ALL_EVENTS, UVALUE='mask2' , XSIZE=15, YSIZE=1, title='Outer Mask Size (pixels)=')
	endif ELSE $
	    mask2 =   CW_FIELD(col2, VALUE=maskv(2), /ALL_EVENTS, UVALUE='mask2' , XSIZE=15, YSIZE=1, title='Outer Mask Size (Rs) =')
	maskcl =   CW_FIELD(col2, VALUE=fix(maskv(4)), /ALL_EVENTS, UVALUE='maskcl' , XSIZE=15, YSIZE=1, title='Color (0-255) ')

        if iscor then begin
	   col3 = WIDGET_BASE(col1, /COLUMN, /FRAME)
           mask3 =   widget_droplist(col3, VALUE=['NO','YES'], UVALUE='mask3',title='Draw Limb : ')
	   maskcllim =   CW_FIELD(col3, VALUE=fix(maskv(5)), /ALL_EVENTS, UVALUE='maskcllim' , XSIZE=15, YSIZE=1, title='Color (0-255) =')
	   masklimthck =   CW_FIELD(col3, VALUE=maskv(6), /ALL_EVENTS, UVALUE='masklimthck' , XSIZE=15, YSIZE=1, title='Thickness =')
        endif

	rowc = WIDGET_BASE(mskbase, /ROW)
	blk= '          '
 	tmp = CW_BGROUP(rowc,  ['PREVIEW ','  APPLY  ',' CANCEL '], /ROW, $
	      BUTTON_UVALUE = ['MSKPREV','MSKDONE','MSKCANCEL'])

        desc=['1\Save Frame', '0\tiff\SCC_SAVE_FRAME_EVENT', '0\png\SCC_SAVE_FRAME_EVENT', '0\jpeg\SCC_SAVE_FRAME_EVENT', '0\pict\SCC_SAVE_FRAME_EVENT', '0\BMP\SCC_SAVE_FRAME_EVENT', '2\gif\SCC_SAVE_FRAME_EVENT']
	tmp = CW_PDMENU(rowc, desc, /RETURN_NAME)

	rowd = WIDGET_BASE(mskbase, /ROW)
	instxt =   widget_text(rowd, VALUE='Mask must be Previewed before being applied ',  UVALUE='mskinst' , XSIZE=40, YSIZE=1)
	
        moviev.instxt=instxt
        if moviev.winbase gt -1 then base1=long(moviev.winbase) else base1=long(moviev.cmndbase)
    	widget_offset,base1,moviev.basexoff(0),moviev.baseyoff(0),NEWBASE=mskbase
    	IF (moviev.debugon) then help,moviev.basexoff(0),moviev.baseyoff(0)
    	WIDGET_CONTROL, mskbase,xoffset=moviev.basexoff(0), yoffset=moviev.baseyoff(0)
	WIDGET_CONTROL, mskbase, /REALIZE

	XMANAGER, 'select_maskects', mskbase, EVENT_HANDLER='mask_event'
	

end
    	;**-----------------End mask widget ------------------**
;----------------Frame Header info widget --------------------------------------

pro fhdr_event, ev
COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   if strpos(input,'tagsel') gt -1 then begin
      tag=strmid(input,6,strlen(input)-6)
      input=strmid(input,0,6)
   endif

   IF (debug_on) THEN BEGIN
    	help,/str,moviev
	print,'In fhdr_event.pro, '
	help,input
	wait,3
    ENDIF
    CASE (input) OF


	'fhfilev' :  BEGIN 	
	             WIDGET_CONTROL, ev.id, GET_VALUE=val
       	             fname=val[0]
                     moviev.framehdrinfo.fhdrfile=fname
    	    END

	'PKFILE' :   BEGIN 	
	           fname = PICKFILE(file=moviev.framehdrinfo.fhdrfile, filter='*.txt', TITLE='Frame header output file')
                   if fname ne moviev.framehdrinfo.fhdrfile and fname ne '' then begin
                     moviev.framehdrinfo.fhdrfile=fname
                     widget_control,moviev.framehdrinfo.fhfilev,set_value=moviev.framehdrinfo.fhdrfile
		   endif
    	    END

        'fhclose' :   BEGIN              ; 
                   WIDGET_CONTROL, /DESTROY, moviev.framehdrinfo.hdrinfo
                   moviev.framehdrinfo.showfhdr=0
                  return
		END                

        'tagsel' :  moviev.framehdrinfo.stags(where( moviev.framehdrinfo.tags eq tag))=ev.select             

        'SVFILE' :   BEGIN              ; 
                    z=(where( moviev.framehdrinfo.stags eq 1))
                    openw,hdrlun,moviev.framehdrinfo.fhdrfile,width=n_elements(i)*13,/get_lun
                    fmt='('+string(n_Elements(z))+'(a13))'
                    printf,hdrlun,format=fmt,moviev.framehdrinfo.tags(z)
                    line=strarr(n_elements(z))
                    for l=0,moviev.len-1 do begin
		      for n=0,n_elements(z)-1 do line(n)=string(moviev.img_hdrs(l).(z(n)))
                      printf,hdrlun,format=fmt,line
                    endfor
                    close,hdrlun
                    free_lun,hdrlun
                  return
		END                

	ELSE : BEGIN
                 PRINT, '%%Header info  Unknown event ',input
	     END

    ENDCASE

return
END 

pro fhdr_info

COMMON scc_playmv_COMMON
COMMON WSCC_MKMOVIE_COMMON

	hdrinfo = WIDGET_BASE(/COL, TITLE='Movie Frame Header Info')
        tags=tag_names(moviev.img_hdrs)
        tagn=intarr(n_elements(tags))

        stags=intarr(n_elements(tags))
        stags(where(strlowcase(tags) eq 'date_obs'))=1
        stags(where(strlowcase(tags) eq 'time_obs'))=1
        stagt='tagsel'+tags
	cola = WIDGET_BASE(hdrinfo, /COL,/frame,Y_scroll_size=600,ysize=600)
        for fh=0,n_elements(tags)-1 do begin
	        row2 = WIDGET_BASE(cola, /ROW)
	        col1 = WIDGET_BASE(row2, /COL,XSIZE=100)
 	        tmp = CW_BGROUP(col1,  tags(fh), IDS=pauseb ,BUTTON_UVALUE = stagt(fh),/NONEXCLUSIVE)
	        col2 = WIDGET_BASE(row2, /COL)
                WIDGET_CONTROL, tmp, SET_VALUE=stags(fh)
	        tagn(fh) =   widget_text(col2, VALUE=string(moviev.img_hdrs(moviev.current).(fh)), UVALUE=tags(fh),XSIZE=30, YSIZE=1)
	endfor
        BREAK_FILE, moviev.filename, a2, dir2, name2, ext2
        fhdrfile=dir2+name2+'_hdr.txt'
	fhfilev =   CW_FIELD(hdrinfo, VALUE=fhdrfile, /ALL_EVENTS, XSIZE=45, YSIZE=1, UVALUE='fhfilev', title='Output Filename: ')
        tmp= WIDGET_LABEL(hdrinfo,VALUE='* Only selected keywords will be saved')
	rowd = WIDGET_BASE(hdrinfo, /ROW,/frame)
	pkfile = WIDGET_BUTTON(rowd, VALUE=' PICKFILE ', UVALUE='PKFILE')
	svfile = WIDGET_BUTTON(rowd, VALUE=' Save Hdr File ', UVALUE='SVFILE')
	fhclose = WIDGET_BUTTON(rowd, VALUE=' CLOSE ', UVALUE='fhclose')

        framehdrinfo={hdrinfo:hdrinfo,fhfilev:fhfilev,showfhdr:1,tagn:tagn,stags:stags,tags:tags,fhdrfile:fhdrfile} 
        IF (~TAG_EXIST(moviev, 'framehdrinfo')) THEN  moviev=add_tag(moviev,framehdrinfo,'framehdrinfo') else moviev.framehdrinfo=framehdrinfo

        if moviev.winbase ne -1 then base1=moviev.winbase else base1=moviev.cmndbase
	widget_offset,long(base1),moviev.basexoff(1),moviev.baseyoff(1),NEWBASE=hdrinfo
    	IF (moviev.debugon) then help,moviev.basexoff(1),moviev.baseyoff(1)
    	WIDGET_CONTROL, hdrinfo, xoffset=moviev.basexoff(1), yoffset=moviev.baseyoff(1)
	WIDGET_CONTROL,hdrinfo , /REALIZE

	XMANAGER, 'hdrinfo', hdrinfo, EVENT_HANDLER='fhdr_event'

return
end
    	;**-----------------End Frame Header info widget ------------------**
	

;;;-----------------SYNC----------------------------------------------------
PRO plot_sync, reversec=reversec ;if there is a jmap sync saved, this will plot it until the remove sync button is pressed
COMMON scc_playmv_COMMON

    	debug_on=moviev.debugon
    	syncflag=moviev.sync_color 
	reversecolor=keyword_set(REVERSEC)
	syncflag=syncflag+reversecolor
	;help,syncflag
	IF syncflag GE 1 THEN BEGIN
	    	;print,sync_point
		x=moviev.sync_point[0,moviev.current]
		y=moviev.sync_point[1,moviev.current]
		IF (debug_on) THEN help,syncflag
		IF (debug_on) THEN print,'xyouts ',trim(x),', ',trim(y)
		IF x LT 0 or x GT moviev.hsize-1 or y LT 0 or y GT moviev.vsize-1 THEN $
		    message,'WARNING: sync point is outside of current FOV.',/info
    		IF syncflag mod 2 EQ 1 THEN BEGIN
			XYOUTS,x,y-1,'[(',alignment=1., /DEVICE
			XYOUTS,x,y-1,')]',alignment=0., /DEVICE,color=0
		ENDIF ELSE BEGIN
			XYOUTS,x,y-1,'[(',alignment=1., /DEVICE,color=0
			XYOUTS,x,y-1,')]',alignment=0., /DEVICE
		ENDELSE
		if moviev.showzoom eq 1 then begin
                    draw_zoom
            	    save_win = !D.WINDOW
            	    WSET, moviev.zstate.zoom_win
		     x=(x-moviev.zstate.x0)*moviev.zstate.scale
		     y=(y-moviev.zstate.y0)*moviev.zstate.scale
		    IF (debug_on) THEN print,'in ZOOM xyouts ',trim(x),', ',trim(y)
    		    IF syncflag mod 2 EQ 1 THEN BEGIN
			XYOUTS,x,y-1,'[(',alignment=1., /DEVICE
			XYOUTS,x,y-1,')]',alignment=0., /DEVICE,color=0
		    ENDIF ELSE BEGIN
			XYOUTS,x,y-1,'[(',alignment=1., /DEVICE,color=0
			XYOUTS,x,y-1,')]',alignment=0., /DEVICE
		    ENDELSE
            	    WSET,save_win
   		endif 
	ENDIF
	moviev.sync_color=syncflag
END


PRO SCC_MOVIE_CONTROL_EVENT, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine


tmp = INDGEN(N_ELEMENTS(moviev.win_index))
good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
     
 IF (ev.id EQ ev.handler) THEN BEGIN   ; A top level base timer event
    IF (moviev.forward) THEN BEGIN		;** going forward
        IF ((moviev.bounce) AND (moviev.current EQ moviev.last)) THEN BEGIN
            moviev.current = moviev.last-1
	    moviev.pause = 1
	    moviev.forward = 0		;** set direction to reverse
        ENDIF $
        ELSE BEGIN
            moviev.current = (moviev.current  + 1) MOD moviev.len
	    IF ((moviev.current EQ 0) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
            IF ((moviev.current GT moviev.last) OR (moviev.current LT moviev.first)) THEN BEGIN
	   	moviev.pause = 1	
		moviev.current = moviev.first
	    ENDIF
        ENDELSE
    ENDIF $
    ELSE BEGIN			;** going in reverse
        IF ((moviev.bounce) AND (moviev.current EQ moviev.first)) THEN BEGIN
            moviev.current = moviev.first+1
	    moviev.pause = 1
	    moviev.forward = 1		;** set direction to forward
        ENDIF $
        ELSE BEGIN
            moviev.current = (moviev.current - 1) MOD moviev.len
	    IF ((moviev.current EQ moviev.last) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
            IF ((moviev.current LT moviev.first) OR (moviev.current GT moviev.last)) THEN BEGIN
		moviev.current = moviev.last
		moviev.pause = 1
	    ENDIF
        ENDELSE
    ENDELSE

    IF (moviev.pause AND moviev.dopause) THEN BEGIN
	moviev.pause = 0
	WAIT, .75
    ENDIF

    ;** out of range error checks (to stop bombs from COPY)
    IF (moviev.current LT 0) THEN moviev.current = moviev.first
    IF (moviev.current GE moviev.len) THEN moviev.current = moviev.last
    IF NOT(moviev.deleted(moviev.current)) THEN BEGIN		;** if not deleted then display
       WSET, moviev.draw_win
       DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
       if (moviev.box(0) eq 3) or (moviev.box(0) eq 7) then draw_box
       WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
       WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current), /REMOVE_ALL)
       if moviev.sliden ne -1 then WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
       if moviev.slidenw ne -1 then WIDGET_CONTROL, moviev.slidenw, SET_VALUE=moviev.current
       WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall   ; Request another timer event
       if (moviev.showzoom eq 1) then draw_zoom
       IF moviev.sync_color GE 1 THEN plot_sync
    ENDIF ELSE $	;** else immediately look for the next non-deleted frame
      WIDGET_CONTROL, moviev.cmndbase, TIMER=0.0 ;.000001
      
  ENDIF 
  
    IF (TAG_EXIST(ev, 'value')) THEN  IF (DATATYPE(ev.value) EQ 'STR') THEN input=ev.value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
    IF (DATATYPE(input) EQ 'STR') THEN BEGIN
	; nbr, 04.02.26 - for 24-bit displays
  	device,get_visual_depth=depth
   	;IF depth GT 8 THEN loadct,0
      if strpos(input,'Draw Carrington Grid') gt -1 then begin
	 carrspace=strmid(input,strlen(input)-2,2)
         input='Draw Carrington Grid'
      endif

   IF (debug_on) THEN BEGIN
    	help,/str,moviev
	print,'In SCC_MOVIE_CONTROL_EVENT.pro, '
	help,input
	wait,3
    ENDIF
     CASE (input) OF

	'PS-Rescale' : BEGIN 	;save ps image with rescaling
                BREAK_FILE, moviev.filename, a, dir, name, ext
                if name eq '' then name='default'
                filename = name + '.ps'
                filename = PICKFILE(file=filename, filter='*.ps', $
                                   TITLE='Enter name of postscript file to save', $
                                   PATH=moviev.filepath,GET_PATH=path)
                IF (filename EQ '') THEN RETURN
                BREAK_FILE, filename, a, dir, name, ext
                IF (dir EQ '') THEN filename=path+filename
                BREAK_FILE, filename, a, dir, name, ext
                moviev.filepath=dir
                moviev.filename=moviev.filepath+name+ext
		WSET, moviev.draw_win
		WIN2PS, moviev.filename,rescale=0
	    END
	'Postscript' : BEGIN 	;save ps image
                BREAK_FILE, moviev.filename, a, dir, name, ext
                if name eq '' then name='default'
                filename = name + '.ps'
                filename = PICKFILE(file=filename, filter='*.ps', $
                                   TITLE='Enter name of postscript file to save', $
                                   PATH=moviev.filepath,GET_PATH=path)
                IF (filename EQ '') THEN RETURN
                BREAK_FILE, filename, a, dir, name, ext
                IF (dir EQ '') THEN filename=path+filename
                BREAK_FILE, filename, a, dir, name, ext
                moviev.filepath=dir
                moviev.filename=moviev.filepath+name+ext
		WSET, moviev.draw_win
		WIN2PS, moviev.filename,/rescale
	    END

	'Print Frame' : BEGIN	;print current frame
                 winsave=!D.WINDOW
		 WSET, moviev.win_index(moviev.current)
		 ;PRINTIT
                 if datatype(imgs) eq 'UND' then img=TVRD(true=moviev.truecolor) else begin
		   if moviev.truecolor eq 1 then img = imgs(*,*,*,moviev.current) else img = imgs(*,*,moviev.current)
                 endelse
                 if moviev.truecolor eq 1 then begin
		    img = Color_Quan(img, 1, r, g, b)
                    tvlct,r,g,b
                 endif
		 outfile=concat_dir(getenv('HOME'),'movieframe.ps')
                 PRINT,'Saving image to ',outfile
                 SET_PLOT,'ps'
                 DEVICE,FILENAME=outfile
                 coloropt=''
                 READ,'Use 8-bit color, or black&white halftone (c/h)? ',coloropt
                 IF (STRLOWCASE(STRMID(coloropt,0,1)) eq 'c') THEN DEVICE,/COLOR,BITS=8
		 device, xoffset=0, yoffset=0, xsize=8.5, ysize=11, /inches, bits=8, /times, /bold, /portrait
		 pos = [0.1,0.0,0.8,0.0]
		 ht = pos[2] * (8.5/11.0) * float(moviev.vsize) / float(moviev.hsize)
		 pos[1] = (1.0 - ht)/2.0
		 pos[3] = ht
                 TV, img, /normal, pos[0], pos[1], xsize = pos[2], ysize = pos[3]
                 DEVICE,/CLOSE
		 IF !version.os_family Eq 'unix' THEN BEGIN
                     cmnd='lpstat -a'
                     spawn,cmnd,result
                     printer=strarr(n_elements(result)+1)
                     print,string(0,'(i2)'),'  Default'
                     for i=0,n_elements(result)-1 do begin
                	printer(i+1)=strmid(result(i),0,strpos(result(i),'accepting'))
                	print,string(i+1,'(i2)'),'  ',printer(i+1)
		     endfor
                     READ,'select printer? ',i
                     printer=strcompress(printer(i),/remove_all)                 
		     IF printer EQ '' THEN printdest='' ELSE printdest=['-d',printer]
		     cmd=['lp',printdest,outfile]
                     PRINT,cmd
                     SPAWN,cmd[where(cmd)],/NOSHELL
		 ENDIF
                 select_windows ;set_plot,'x'
                 ;PRINT,'Request sent.'
                 WSET,winsave
	     END

        'Load Movie' : BEGIN	;** load movie from file
                ; if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW' then mfilter='*.mvi,*.hdr' else mfilter='*.mvi'
                 mfilter='*.mvi,*.hdr' 
		 win_index = PICKFILE(filter=mfilter,file=moviev.filename,/MUST_EXIST, $
                                      TITLE='Select movie file to load', $
                                      PATH=moviev.filepath, GET_PATH=path)
                 IF ((win_index EQ '') OR (win_index EQ moviev.filename)) THEN RETURN
                 BREAK_FILE, win_index, a, dir, name, ext
                 moviev.filepath = dir
                 IF (dir EQ '') THEN BEGIN
                    moviev.filepath = './'
                    win_index = path+win_index
                 ENDIF
                 evtpro=moviev.evtpro
	         exit_playmovie
                 if evtpro eq 'SCC_PLAYMOVIEM_DRAW' then SCC_playmoviem, win_index, /LOAD else SCC_playmovie, win_index, /LOAD
                 RETURN
	     END

	'Save Movie': BEGIN 	;save to .mvi file
                  scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=moviev.filename, len=moviev.len,$
		  first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect;  scc_movieout,base
	    END

         'Annotate' : BEGIN
    	    	if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
		
    	    	if datatype(imgs) eq 'BYT' then annotate_image,moviev.win_index,/template,true=moviev.truecolor,ahdrs=moviev.img_hdrs,debug=moviev.debugon
	    END
 
	'Movie Control' : BEGIN	;delete current frame
              IF (moviev.showmenu EQ 0) THEN BEGIN
                 WIDGET_CONTROL, moviev.base, MAP=1
                 moviev.showmenu = 1
              ENDIF ELSE BEGIN
                WIDGET_CONTROL, moviev.base, MAP=0
                moviev.showmenu = 0
              ENDELSE
	    END

	'Combine Control' : BEGIN	;delete current frame
              IF (moviev.showcmb EQ 0) THEN BEGIN
                 WIDGET_CONTROL, moviev.cmbbase, MAP=1
                 moviev.showcmb = 1
              ENDIF ELSE BEGIN
                WIDGET_CONTROL, moviev.cmbbase, MAP=0
                moviev.showcmb = 0
              ENDELSE
	    END

        'Define Imgs' : BEGIN	;** save movie to file
    	    	if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
	    END

	'Crop' : box_control,'crop'

	'Zoom': BEGIN
	          box_control,'zoom'
                 RETURN
            END 

	'Track Spot' : box_control,'spot'

	'Omap' : box_control,'o_map'

	'Time_Avg_Diff' : box_control,'timediff'

	'Polar Azimuthal Equidistant': BEGIN
                if moviev.file_hdr.truecolor le 0 then begin
    	    	  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	  if datatype(imgs) ne 'UND' then apply_project
                endif else popup_msg,'Can not run projection truecolor images',title='SCC_MOVIE_WIN MSG'
	     END

        'Draw Grid': add_grid

     	; Point Filter
 	' + (all frames)': BEGIN
                if moviev.file_hdr.truecolor le 0 then begin
    	    	  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	  if datatype(imgs) ne 'UND' then begin
		    FOR i=0,n_elements(moviev.win_index)-1 DO BEGIN
			point_filter,imgs[*,*,i],5,7,3,tmp,pts
                        imgs[*,*,i] = tmp
			wset, moviev.win_index[i]
			tv, imgs[*,*,i]
                        if i eq moviev.current then begin
		          WSET, moviev.draw_win
                          DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                        endif

		    ENDFOR
		  print, 'Done Removing stars'
                  endif 
	        endif else popup_msg,'Can not Smooth truecolor images',title='SCC_MOVIE_WIN MSG'
	     END

     	; Point Filter
 	' - (all frames)': BEGIN
                if moviev.file_hdr.truecolor le 0 then begin
    	    	  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	  if datatype(imgs) ne 'UND' then begin
		    FOR i=0,n_elements(moviev.win_index)-1 DO BEGIN
                        tmp=imgs[*,*,i]*(-1)
			point_filter,tmp,5,7,3,tmp2,pts
			imgs[*,*,i] = tmp2*(-1)
			wset, moviev.win_index[i]
			tv, imgs[*,*,i]
                        if i eq moviev.current then begin
		          WSET, moviev.draw_win
                          DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                        endif

		    ENDFOR
		  print, 'Done Removing stars'
                  endif 
	        endif else popup_msg,'Can not Smooth truecolor images',title='SCC_MOVIE_WIN MSG'
	     END

	'Polar Equidistant Cylindrical': BEGIN
                if moviev.file_hdr.truecolor le 0 then begin
    	    	  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	  if datatype(imgs) ne 'UND' then apply_rad_spoke
                endif else popup_msg,'Can not run Rad_spoke truecolor images',title='SCC_MOVIE_WIN MSG'
	     END


	'Label Planets': select_planets

	'Smooth': do_smooth

        'Start HT': BEGIN
                     if (tag_exist(moviev,'wcshdrs')) then if n_elements(moviev.wcshdrs) eq moviev.len*2 then ab=1 else ab=0
		     sccWRITE_HT,moviev.img_hdrs(moviev.current),moviev.elong,moviev.file_hdr,ab=ab
                     moviev.htfile='started'
                   END

        'Stop HT': BEGIN
                     moviev.htfile=''
                   END

	'Plot HT': BEGIN 
	
                    IF (STRLEN(moviev.htfile) eq 0 and moviev.htfile ne 'started') THEN sXPLOT_HT $
                    ELSE BEGIN
                        sXPLOT_HT, moviev.htfile
                        moviev.htfile = ''
                    END
                  END 

   'SCC_PLAYMOVIEM': BEGIN	;call scc_wrunmoviem

		 movie_in=moviev.win_index(good) 
		 HDRS=moviev.img_hdrs(good) 
		 NAMES=moviev.frames(good)
                 SUNXCEN=moviev.xcen 
		 SUNYCEN=moviev.ycen 
		 SEC_PIX=moviev.secs
                 RECTIFIED=moviev.rect
                 filename=moviev.filename
                 FILE_HDR=moviev.FILE_HDR
                 if (TAG_EXIST(moviev, 'wcshdrs')) then begin
		   wcshdrs=moviev.wcshdrs(good)
                   if (n_elements(moviev.wcshdrs) eq moviev.len*2) then wcshdrs=[wcshdrs,moviev.wcshdrs(good+moviev.len)]
                 endif else wcshdrs=0
                 if datatype(imgs) ne 'UND' then begin
		    if moviev.truecolor eq 0 then imgs = imgs(*,*,good) else imgs = imgs(*,*,*,good)
                 endif
                 osczero=moviev.osczero
	         exit_playmovie,/retain_win_index
                 SCC_PLAYMOVIEM, movie_in, HDRS=HDRS, NAMES=NAMES, $
                    file_hdr=file_hdr,filename=filename,/editk,wcshdr=wcshdrs,osczero=osczero, DEBUG=debug_on

                 RETURN
	    END 

       'Objects' : select_objects
       'Add Mask' :  BEGIN     	    	
                if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	if datatype(imgs) eq 'BYT' then add_mask
                 RETURN
	     END

       'View Frame Hdrs' : fhdr_info
       'Crosshair' : BEGIN
            	; bitmap cursor as defined in IDL device keywords
            	  imgbmp=[ 0,0,0,0,0,0,40776,40776,65400,3847,7,3847,65400,40776,40776,0]
		  mskbmp=[ 0,0,0,0,0,7,32783,32783,57402,0,14560,0,57402,32783,32783,7]
    	    	  CASE OS_FAMILY() OF
		    'Windows': device,/cursor_crosshair
		    'MacOS':   device,/cursor_standard
		    ELSE:      device,cursor_image=imgbmp,cursor_mask=mskbmp,cursor_xy=[9,5]
	          ENDCASE
    	    	END
		
       'Arrow' : device,/CURSOR_ORIGINAL
       'Debug' : BEGIN
            	if moviev.debugon eq 1 then BEGIN
            	    moviev.debugon=0 
		    debug_on=0 
		ENDIF else BEGIN
		    moviev.debugon=1
      	    	    debug_on=1
		ENDELSE
		help,debug_on
		END

       'EXIT': exit_playmovie

        'Edit Frame': BEGIN             
;		  sccSHOW_MENU,/SHOW
                if moviev.file_hdr.truecolor le 0 then begin
    	    	  if datatype(imgs) ne 'BYT' then define_imgs,moviev.filename
    	    	  if datatype(imgs) eq 'BYT' then begin
                    winsave=!D.WINDOW
                    WSET,moviev.win_index(moviev.current)
		    IF(n_elements(imgs) GT 1) THEN img = imgs[*,*,moviev.current] $
                    ELSE img = TVRD(0,0,moviev.hsize, moviev.vsize)
                    SCCXEDITFRAMEX,img,newimg,moviev
                    WSET,moviev.win_index(moviev.current)
	            tv, newimg
		    WSET, moviev.draw_win
                    DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
		    WSET,winsave
                    IF(n_elements(imgs) GT 1) THEN imgs[*,*,moviev.current] = newimg
                  endif
	        endif else popup_msg,'Edit Frame does not handle truecolor images',title='SCC_MOVIE_WIN MSG'
   
		END

 	ELSE : BEGIN
                 PRINT, '%%SCC_MOVIE_CONTROL.  Unknown event ',input
	     END

    ENDCASE

  ENDIF
  if datatype(moviev) eq 'STC' then if (TAG_EXIST(moviev, 'framehdrinfo')) then if moviev.framehdrinfo.showfhdr eq 1 then $
            for fh=0,n_elements(moviev.framehdrinfo.tagn)-1 do widget_control,moviev.framehdrinfo.tagn(fh),set_value=string(moviev.img_hdrs(moviev.current).(fh))       

END 

FUNCTION SCC_MOVIE_CONTROL_FUNC, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine

   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   IF (debug_on) THEN BEGIN
    	;help,/str,moviev
	print,'In SCC_MOVIE_CONTROL_FUNC.pro, '
	help,input
	wait,3
    ENDIF
   if input ne '' then begin
    CASE (input) OF

	'DELETE' : BEGIN	;delete current frame
	    	 message,'Deleting frame '+trim(moviev.current),/info
                 WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
		 moviev.deleted(moviev.current) = 1
	    END
	'UNDELETE' : BEGIN	;
                 if moviev.current eq 0 then fnum=moviev.len-1 else fnum= moviev.current-1
	    	 message,'UnDeleting frame '+trim(fnum) ,/info
                 WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
		 moviev.deleted(fnum) = 0
	    END

	'UNDELETEALL' : BEGIN	;
	    	 message,'UnDeleting all deleted frames ' ,/info
                ; WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
		 moviev.deleted(*) = 0
	    END

        'NEXT' : IF (ev.select NE 0) THEN BEGIN	;next frame
                 WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                 moviev.stall = 10000.

		 moviev.forward = 1

		 REPEAT BEGIN
		    moviev.current = (moviev.current + 1) MOD moviev.len
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.first
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.first
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                 if (moviev.box(0) eq 3) or (moviev.box(0) eq 7) then draw_box
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                 WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                 WIDGET_CONTROL, moviev.slidenw, SET_VALUE=moviev.current
                 if (moviev.showzoom eq 1) then begin
                     ;WIDGET_CONTROL, base, SET_UVALUE=moviev
                    draw_zoom
                 endif 
                 IF moviev.sync_color GE 1 THEN plot_sync
	     ENDIF
	    
        'PREV' : IF (ev.select NE 0) THEN BEGIN	;previous frame
                 WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                 moviev.stall = 10000.

		 moviev.forward = 0

		 REPEAT BEGIN
		    moviev.current = (moviev.current - 1) MOD moviev.len
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.last
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.last
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                 if (moviev.box(0) eq 3) or (moviev.box(0) eq 7) then draw_box
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                WIDGET_CONTROL, moviev.slidenw, SET_VALUE=moviev.current
                if (moviev.showzoom eq 1) then begin
                  ;WIDGET_CONTROL, base, SET_UVALUE=moviev
                  draw_zoom
               endif 
               IF moviev.sync_color GE 1 THEN plot_sync

	     ENDIF

        'CONTINUOUS' : IF (ev.select NE 0) THEN BEGIN	;continuous
		 ;** set direction to saved direction
		 moviev.forward = moviev.dirsave
                 moviev.stall = moviev.stallsave
                 WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall   ; Request another timer event
	     ENDIF

        'FORWARD' : IF (ev.select NE 0) THEN BEGIN	;forward
		 moviev.bounce = 0
		 moviev.forward = 1
		 moviev.dirsave = 1
	     ENDIF

        'REVERSE' : IF (ev.select NE 0) THEN BEGIN	;reverse
		 moviev.bounce = 0
		 moviev.forward = 0
		 moviev.dirsave = 0
	     ENDIF

        'BOUNCE' : IF (ev.select NE 0) THEN BEGIN	;bounce
		 moviev.bounce = 1
	     ENDIF

        'PAUSE' : IF (ev.select NE 0) THEN BEGIN	;pause
		 moviev.dopause = 1
	     ENDIF

        'NO_PAUSE' : IF (ev.select NE 0) THEN BEGIN	;no pause
		 moviev.dopause = 0
	     ENDIF

        'FRAME_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.cframe, GET_VALUE=val
                val = 0 > FIX(val(0)) < moviev.last
                moviev.current=val
                DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                 if (moviev.box(0) eq 3) or (moviev.box(0) eq 7) then draw_box
                WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                WIDGET_CONTROL, moviev.slidenw, SET_VALUE=moviev.current
                if (moviev.showzoom eq 1) then begin
                  draw_zoom
               endif 
               IF moviev.sync_color GE 1 THEN plot_sync

          END

        'DRAW' : BEGIN	;image window
	 END

	'SPEED' : BEGIN 	;speed
	    IF (ev.value EQ 0) THEN moviev.stall = 5. ELSE moviev.stall = (100 - ev.value)/50.
            moviev.stallsave = moviev.stall
            WIDGET_CONTROL, moviev.cmndbase, TIMER=moviev.stall   ; Request another timer event
	 END
		
	 'SLIDE_FIRST' : BEGIN	;first frame
		moviev.first = ev.value
	 END
		
	'SLIDE_LAST' : BEGIN	;last frame
		moviev.last = ev.value
	 END
		
	;'SLIDE_SKIP' : BEGIN	;skip frame
	;	 moviev.nskip = ev.value
	 ;END

        'SLIDE_NUMW' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.slidenw, GET_VALUE=val
                WIDGET_CONTROL, moviev.sliden, SET_VALUE=val
                val = FIX(val(0))
                IF (NOT(moviev.deleted(val)) AND (val GE moviev.first) AND (val LE moviev.last)) THEN BEGIN
                  moviev.current = val
                  DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                 if (moviev.box(0) eq 3) or (moviev.box(0) eq 7) then draw_box
                  WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                  WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                  if (moviev.showzoom eq 1) then draw_zoom
               ENDIF
                WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                moviev.stall = 10000.
          END
        'SLIDE_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.sliden, GET_VALUE=val
                WIDGET_CONTROL, moviev.slidenw, SET_VALUE=val
                val = FIX(val(0))
                IF (NOT(moviev.deleted(val)) AND (val GE moviev.first) AND (val LE moviev.last)) THEN BEGIN
                  moviev.current = val
                  DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                 if (moviev.box(0) eq 3) or (moviev.box(0) eq 7) then draw_box
                  WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                  WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                  if (moviev.showzoom eq 1) then draw_zoom
                ENDIF
                IF moviev.sync_color GE 1 THEN plot_sync
                WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                moviev.stall = 10000.
          END

	'DONE' : BEGIN       ;** exit program
                 exit_playmovie
                 RETURN, 0
	     END

	'MVCONTROL' : BEGIN	;show control window
              IF (moviev.showmenu EQ 0) THEN BEGIN
                 WIDGET_CONTROL, moviev.base, MAP=1
                 moviev.showmenu = 1
              ENDIF ELSE BEGIN
                WIDGET_CONTROL, moviev.base, MAP=0
                moviev.showmenu = 0
              ENDELSE
	    END

	'CMBCONTROL' : BEGIN	;show cmbcontrol window
              IF (moviev.showcmb EQ 0) THEN BEGIN
                 WIDGET_CONTROL, moviev.cmbbase, MAP=1
                 moviev.showcmb = 1
              ENDIF ELSE BEGIN
                WIDGET_CONTROL, moviev.cmbbase, MAP=0
                moviev.showcmb = 0
              ENDELSE
	    END

        'ZSCALE': BEGIN 
                   WIDGET_CONTROL, moviev.zslide, GET_VALUE=val
                   sccSHOW_ZOOM
                   moviev.zstate.scale=val
                   sccSHOW_ZOOM
		   END 

	'ZOOM': BEGIN 
                    if (datatype(imgs) ne 'BYT') then define_imgs,inpfilename
		    if (datatype(imgs) eq 'BYT') then sccSHOW_ZOOM else print,'IMGS must defined to run ZOOM'
                   RETURN, 0
                  END 

        'ELONG' :  BEGIN 
                     
                    if moviev.htfile eq '' then moviev.elong= ev.index else begin
			widget_control,ev.id,SET_COMBOBOX_SELECT= moviev.elong
		    	popup_msg,['Changing units while writing a HT file is not allowed.','Please select "Stop HT" to change display units.'],title='SCC_MOVIE_WIN MSG'
                    endelse
		   END 

	'Import Sync' : BEGIN ;sync with jMaps tool2a
		iscarr=0
		line=''
		OPENR, lun, '$HOME/syncjmapwithmovie.dat', /get_lun
		READF, lun, line
		FREE_LUN, lun  ;read in data of format [Time, Height(elongation angle), Position Angle]
		temp=strsplit(line,/extract)
		IF n_elements(temp) GT 3 THEN IF temp[3] EQ 'CARRINGTON' THEN iscarr=1
		stai =double(temp[0])

    		print,'Read in ',temp[1],', ',temp[2],' at ',utc2str(tai2utc(stai),/ecs)
		frametimes=utc2tai(str2utc(moviev.img_hdrs.date_obs + ' ' + moviev.img_hdrs.time_obs))
		tempframe=[0,50000.]
		
		FOR i=0, n_elements(frametimes)-1 DO BEGIN  ;find frame closest in time
			tempdiff=ABS(frametimes[i]-stai)
			IF(tempdiff LT tempframe[1]) THEN tempframe=[i,tempdiff]
		ENDFOR
		IF(tempframe[1] EQ 50000.) THEN print,'No movie frame near sync time.'
		
		IF tempframe[1] NE 50000. or iscarr THEN BEGIN
                	WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
			moviev.stall=10000.
			moviev.current=tempframe[0]
                        if ~(TAG_EXIST(moviev, 'wcshdrs')) then define_wcshdrs
			i=moviev.current
			wcs=moviev.wcshdrs(i)
			IF iscarr THEN BEGIN
			; coordinates are CRLN and CRLT
			; different for each frame
			    print,'Computing Carrington location of sync for '+trim(moviev.len)+' frames.'
			    FOR i=0,moviev.len-1 DO BEGIN
			    	wcs=moviev.wcshdrs[i]
			    	wcs_convert_to_coord,wcs,hpc,'HG',float(temp[1]),float(temp[2]),/CARRINGTON
			    	pixels=wcs_get_pixel(wcs,hpc)
			    	moviev.sync_point[0,i]=pixels[0]
			    	moviev.sync_point[1,i]=pixels[1]
			    ENDFOR
			    print,'Done.'
			ENDIF ELSE $
			IF(wcs.cunit[0] EQ 'arcsec') THEN BEGIN  ;COR1 and COR2
				xcen=moviev.sunc(i).xcen ;suns coords in pixels
				ycen=moviev.sunc(i).ycen
				tempangle=(temp[2]-wcs.roll_angle) * !PI/180.  ;from height and position angle, find x and plot x y coordinates
				r=temp[1]*!PI/180. ;elongation angle(height)
				pix_rad=moviev.arcs[moviev.current]^(-1)*3600*180./!PI
				;x=round(xcen-r*sin(tempangle)*pix_deg)
				;y=round(r*cos(tempangle)*pix_deg+ycen)
				x=round(xcen-atan(tan(r)*sin(tempangle))*pix_rad)
				y=round(asin(sin(r)*cos(tempangle))*pix_rad+ycen)
			    	moviev.sync_point[0,*]=x
				moviev.sync_point[1,*]=y
			ENDIF ELSE BEGIN  ;HI1 and HI2
				tempangle=(temp[2]) * !PI/180.  ;from height and pa, find x and y coordinates with respect to sun
				r=temp[1]*!PI/180. ;elongation angle(height)
				;x=-asin(sin(r)*sin(tempangle)/SQRT(1-(sin(r)^2)*cos(tempangle)^2))*180./!pi
				x=-atan(tan(r)*sin(tempangle))*180./!PI
				y=asin(sin(r)*cos(tempangle))*180./!pi
				;WCS_CONV_HPR_HPC,tempangle,r,x,y,/zero_center,ang_units='radians'
				;print,'computed '+string(x,'(f10.6)')+string(y,'(f12.6)')+' from'+string(temp[1],'(f10.6)')+string(temp[2],'(f12.6)')+' at frame '+trim(i)
				coords=wcs_get_pixel(wcs,[x,y])
				x=coords[0] & y=coords[1]
			    	moviev.sync_point[0,*]=x
				moviev.sync_point[1,*]=y
			ENDELSE
                        moviev.sync_color=1
			WSET, moviev.draw_win
                	DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index[moviev.current]]
    			WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    			WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames[moviev.current],/REMOVE_ALL)
                	WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
			plot_sync
		ENDIF
     		END
   'Remove Sync': BEGIN 
                moviev.sync_color=-1
		WSET,moviev.draw_win
		DEVICE, COPY=[0,0,moviev.hsize,moviev.vsize,0,0,moviev.win_index[moviev.current]]
	       END

   'Reverse Sync': plot_sync,/reverse

	ELSE : BEGIN
                 PRINT, '%%SCC_MOVIE_CONTROL.  Unknown event ',input
	     END

    ENDCASE
    if moviev.adjcolor eq 1 then begin
          Device, Get_Visual_Depth=thisDepth
        ;IF thisDepth GT 8 THEN BEGIN
	  wset,moviev.draw_win
	  tv, imgs[*,*,moviev.current]
    endif

  ENDIF
  if (TAG_EXIST(moviev, 'framehdrinfo')) then if moviev.framehdrinfo.showfhdr eq 1 then $
            for fh=0,n_elements(moviev.framehdrinfo.tagn)-1 do widget_control,moviev.framehdrinfo.tagn(fh),set_value=string(moviev.img_hdrs(moviev.current).(fh))       


END 


PRO SCC_MOVIE_CONTROL 


COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON

   base = WIDGET_BASE(/COLUMN, XOFFSET=moviev.basexoff(0), YOFFSET=moviev.baseyoff(0), TITLE='SCC_MOVIE Control',EVENT_PRO='SCC_MOVIE_CONTROL_EVENT')
    base1 = WIDGET_BASE(base, /ROW)
	base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)
	    flabel = WIDGET_LABEL(base2, VALUE=' #   Frame')
	    base25 = WIDGET_BASE(base2, /ROW)
                cframe = WIDGET_TEXT(base25, VALUE='0', XSIZE=3, YSIZE=1, /EDITABLE, UVALUE='FRAME_NUM', EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
	        cname = WIDGET_TEXT(base25, VALUE=' ', XSIZE=28, YSIZE=1)


if moviev.len gt 1 then begin

	slide2 = WIDGET_SLIDER(base1, TITLE='First Frame', $
		 VALUE=0, UVALUE='SLIDE_FIRST', MIN=0, MAX=moviev.len, EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
	slide3 = WIDGET_SLIDER(base1, TITLE='Last Frame', $
		 VALUE=moviev.len-1, UVALUE='SLIDE_LAST', MIN=0, MAX=moviev.len-1, EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
;	slide4 = WIDGET_SLIDER(base1, TITLE='Skip Every n Frames', $
;		 VALUE=0, UVALUE='SLIDE_SKIP', MIN=0, MAX=10)
 	slide1 = WIDGET_SLIDER(base1, TITLE='Playback Speed', SCROLL=1, $
		 VALUE=moviev.stall0, UVALUE='SPEED', MIN=0, MAX=100, /DRAG, EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')

        base3 = WIDGET_BASE(base, /ROW)

        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Forward', 'Reverse', 'Bounce'], /EXCLUSIVE, /COLUMN, IDS=dirb, $
	      BUTTON_UVALUE = ['FORWARD', 'REVERSE','BOUNCE'],EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
        WIDGET_CONTROL, dirb(0), SET_BUTTON=1

        col = WIDGET_BASE(base3, /COLUMN)
 	tmp = CW_BGROUP(col,  ['Pause At End', 'No Pause'], /EXCLUSIVE, /COLUMN, IDS=pauseb, /FRAME, $
	      BUTTON_UVALUE = ['PAUSE','NO_PAUSE'],EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
        WIDGET_CONTROL, pauseb(0), SET_BUTTON=1

 	tmp = CW_BGROUP(col,  ['  Edit Frame  '], /COLUMN, IDS=otherb, $
	      BUTTON_UVALUE = ['Edit Frame'])

        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
 	tmp = CW_BGROUP(col,  ['Continuous', 'Next Frame', 'Prev. Frame'], /COLUMN, $
	      BUTTON_UVALUE = ['CONTINUOUS', 'NEXT','PREV'],EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')

        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
 	tmp = CW_BGROUP(col,  ['Delete Frame','Undelete Prev Frame','Undelete All'], /COLUMN, IDS=otherb, $
	      BUTTON_UVALUE = ['DELETE','UNDELETE','UNDELETEALL'], EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
;        col = WIDGET_BASE(base3, /COLUMN)
; 	tmp = CW_BGROUP(col,  ['CLOSE'], /COLUMN, $
;	      BUTTON_UVALUE = ['MVCONTROL'], EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')

        if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW'  then begin
          col = WIDGET_BASE(base3, /COLUMN, /FRAME)
 	  tmp = CW_BGROUP(col,  ['Import Sync', 'Remove Sync', 'Reverse Sync'], /COLUMN, $
	      BUTTON_UVALUE = ['Import Sync', 'Remove Sync', 'Reverse Sync'], EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
        endif
	
        sliden = WIDGET_SLIDER(base, TITLE='Current Frame', VALUE=0, UVALUE='SLIDE_NUM', $
             /DRAG, /SCROLL, MIN=0, MAX=moviev.len-1, EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')


ENDIF ELSE sliden=-1

 	tmp = CW_BGROUP(base,  ['CLOSE'], /row, $
	      BUTTON_UVALUE = ['MVCONTROL'], EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC',xoffset=75)

    moviev.sliden=sliden
    moviev.cframe=cframe
    moviev.cname=cname
    moviev.base=base

   WIDGET_CONTROL, base, MAP=0
   WIDGET_CONTROL, /REAL, base

END


pro SCC_MOVIE_WIN,mvicombo=mvicombo

COMMON scc_playmv_COMMON

device,get_screen_size=ss
wsizes=define_widget_sizes()
	    
ismoviem=0
if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW' and moviev.hsize ge 1024 then ismoviem=1
testhsize=ss[0]-wsizes.border
testvsize=ss[1]-wsizes.border-wsizes.winhdr-wsizes.dsktop-(ismoviem+1)*wsizes.onerow
if moviev.vsize lt testvsize then attach=1 else begin
  attach=0
  testvsize=ss[1]-wsizes.border-wsizes.winhdr-wsizes.dsktop
endelse
yoff=wsizes.onerow+wsizes.border+wsizes.dsktop
if testvsize-yoff lt moviev.vsize then yoff=0  
if (moviev.hsize gt testhsize) or (moviev.vsize gt testvsize) THEN $
    scrlsize=[((testhsize-wsizes.scrbar)<(moviev.hsize+2)), $
    	      ((testvsize-wsizes.scrbar)<(moviev.vsize+2))] ELSE $
    scrlsize=[0,0]

IF moviev.basexoff[0] GT 0 THEN BEGIN
; I think this is always zero when called from playmovie, before I added the margin option. -NR
    margin=moviev.basexoff[0]
    IF scrlsize[0] GT testhsize-margin THEN scrlsize[0]=testhsize-margin
    IF scrlsize[1] GT testvsize-margin THEN scrlsize[1]=testvsize-margin
    moviev.basexoff=[0,0]
ENDIF

BREAK_FILE, moviev.filename, a, dir, name, ext
title=strmid(moviev.evtpro,0,strpos(strlowcase(moviev.evtpro),'_draw'))
cmndbase = WIDGET_BASE(TITLE=title,/COLUMN)

    det=moviev.img_hdrs(0).detector

    dindx=where(['C2','C3','EIT'] EQ det,issoho)
    d2indx=where(['EIT'] EQ det,iseit)
    eindx=where(['EUVI'] EQ det, iseuv)
    cindx=where(['COR2'] EQ det, iscor2)
    dindx=where(['COR1'] EQ det, iscor1)
    dindx=where(['HI1'] EQ det, ishi1)
    dindx=where(['HI2'] EQ det, ishi2)
    isai=(det EQ 'AIA')
    
winrow = WIDGET_BASE(cmndbase ,/row)

        desc = ['1\File', $
            	'0\Save Movie', $
		'0\Load Movie' , $
		'1\Save Frame', $
		    '0\Postscript', $
		    '0\PS-Rescale', $
		    '0\TIFF\SCC_SAVE_FRAME_EVENT', $
		    '0\PNG\SCC_SAVE_FRAME_EVENT', $
	    	    '0\JPEG\SCC_SAVE_FRAME_EVENT', $
		    '0\PICT\SCC_SAVE_FRAME_EVENT', $
    	    	    '0\BMP\SCC_SAVE_FRAME_EVENT', $
		    '0\GIF\SCC_SAVE_FRAME_EVENT', $
		    '2\fts\SCC_SAVE_FRAME_EVENT', $
		'0\Print Frame']
        if moviev.evtpro ne 'SCC_PLAYMOVIEM_DRAW'  then $
	desc=[desc, $
	    	'0\SCC_PLAYMOVIEM']
        desc=[desc, $
	    	'0\EXIT' ]
		
	tmp = CW_PDMENU(winrow, desc, /RETURN_NAME)


       htdesc=[ '1\Tools', $
            	'0\Annotate']
		
       if ~keyword_set(mvicombo)then begin
	     htdesc = [htdesc, $
	     
	    	'0\Smooth', $
		'0\Adjust Color\sADJUST_CT', $
		'0\Edit Frame', $
		'0\Draw Grid', $
		'0\Crop', $
    	    	'0\Zoom']

    	    if moviev.len gt 1 then begin
    	    	if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW'  then htdesc = [htdesc,$
	    	 
		'1\Height-Time', $
		    '0\Start HT', $
		    '0\Stop HT', $
		    '2\Plot HT'] ;, $
		    
    	    	    ;'1\Sync' , '0\Import Sync', '0\Remove Sync', '2\Reverse Sync']      
    	    	htdesc = [htdesc, $
		
		'0\Track Spot', $
		'0\Time_Avg_Diff']  ;,'0\Omap']
		
    	    endif
    	    if moviev.file_hdr.rtheta lt 1 then begin
	    	if (ishi2 or ishi1 or iscor2) and moviev.img_hdrs(0).filter ne 'Deproject' then htdesc = [htdesc, $
    	     	
		'1\Projection', $
		    '0\Polar Azimuthal Equidistant', $
		    '2\Polar Equidistant Cylindrical'] else  htdesc = [htdesc, $
		    
	    	'1\Projection', $
		    '2\Polar Equidistant Cylindrical']
	    endif   
    	    if (~iseuv or ~iseit) then htdesc = [htdesc, $
	    	
 		'1\Point Filter', $
 		    '0\ + (all frames)', $
 		    '2\ - (all frames)']
	 
    	    if ~ishi1 then htdesc = [htdesc, $
	    
	    	'0\Add Mask']
		
 	    if ishi2 then htdesc = [htdesc, $
	    	
		'0\Objects']
		
            htdesc = [htdesc, $
	    	
		'0\Label Planets']
       endif
       htdesc = [htdesc , $
       
            	'0\View Frame Hdrs' , $
		'1\Cursor', $
		    '0\Crosshair', $
		    '2\Arrow', $
		'0\Debug']
		
       tmp = CW_PDMENU(winrow, htdesc, /RETURN_NAME)

       eddesc =['0\Movie Control']
       if keyword_set(mvicombo) then $
       eddesc=[ '1\Controls', $
            	    '0\Movie Control', $
		    '0\Combine Control']
		    
       tmp = CW_PDMENU(winrow, eddesc, /RETURN_NAME)


     if moviev.len gt  1 then begin
 	tmp = CW_BGROUP(winrow,  ['<'], /ROW, $
	      BUTTON_UVALUE = ['PREV'],EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')
        slidenw = WIDGET_SLIDER(winrow,  VALUE=0, UVALUE='SLIDE_NUMW', $
             /DRAG, /SCROLL, MIN=0, MAX=moviev.len-1,  EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC' ,xsize=moviev.hsize-275 <(512-275)>100)
 	tmp = CW_BGROUP(winrow,  ['>','Cont'], /ROW, $
	      BUTTON_UVALUE = ['NEXT','CONTINUOUS'],EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC')

    endif else slidenw =-1

    if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW'  then begin
    ;  vlong=['Solar radius for distance from Sun center (default for COR)',$
    ;    'Degrees (elongation) for distance from Sun (default for HI)',$
    ;    'Carrington latitude and longitude (EUVI and EIT only)']
      vlong=['Solar radii','Degrees ','Carrington','HPLN/HPLT','Stonyhurst']
      if moviev.file_hdr.rtheta ge 1 then vlongl='Rtheta'
      if moviev.file_hdr.rtheta eq 3 then vlongl='Carrington'
      if moviev.file_hdr.rtheta eq 4 then vlongl='Degrees'
      if moviev.hsize lt 1024 then begin
        winrow1 = WIDGET_BASE(cmndbase ,/row)
;        txt=widget_label (winrow1,  value='Display Units :')
        if moviev.file_hdr.rtheta lt 1 then begin
	  vlongv= WIDGET_COMBOBOX(winrow1, VALUE=vlong, UVALUE="ELONG", EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC',ysize=1)
          WIDGET_CONTROL, vlongv, SET_COMBOBOX_SELECT= moviev.elong
          WIDGET_CONTROL, vlongv, SENSITIVE=1
        endif else vlongv= widget_label(winrow1,value=vlongl)
        mousecoords=widget_text(winrow1,value='0',uvalue='mousecoords',font='-1',xsize=48,ysize=1)
      endif else begin
        txt=widget_label (winrow,  value='Display Units :')
        if moviev.file_hdr.rtheta lt 1 then begin
          vlongv= WIDGET_COMBOBOX(winrow, VALUE=vlong, UVALUE="ELONG", EVENT_FUNC='SCC_MOVIE_CONTROL_FUNC',ysize=1)
          WIDGET_CONTROL, vlongv, SET_COMBOBOX_SELECT= moviev.elong
          WIDGET_CONTROL, vlongv, SENSITIVE=1
        endif else vlongv= widget_label(winrow,value=vlongl)
        mousecoords=widget_text(winrow,value='0',uvalue='mousecoords',font='-1',xsize=48,ysize=1)
     endelse
     moviev.mousecoords=mousecoords
   endif

BREAK_FILE, moviev.filename, a, dir, name, ext
title=name+ext

if attach eq 0 then winbase = WIDGET_BASE(TITLE=title,/COLUMN,yoffset=yoff) else winbase=-1
if winbase eq -1 then tmpbase=cmndbase else tmpbase=winbase
if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW' and  ~(TAG_EXIST(moviev, 'wcshdrs')) then DEFINE_WCSHDRS

            if moviev.evtpro eq 'SCC_PLAYMOVIEM_DRAW' then draw_w= WIDGET_DRAW(tmpbase, XSIZE=moviev.hsize, YSIZE=moviev.vsize, $
                                                    X_SCROLL_SIZE=scrlsize[0], Y_SCROLL_SIZE=scrlsize[1], $
                                                    EVENT_PRO=moviev.evtpro, /FRAME, $
                                                   /BUTTON_EVENTS, /MOTION_EVENTS, RETAIN=2) else $
                                               draw_w= WIDGET_DRAW(tmpbase, XSIZE=moviev.hsize, YSIZE=moviev.vsize, $
                                                    X_SCROLL_SIZE=scrlsize[0], Y_SCROLL_SIZE=scrlsize[1], $
                                                    EVENT_PRO=moviev.evtpro, /FRAME, $
                                                   /BUTTON_EVENTS, RETAIN=2)

if moviev.debugon THEN BEGIN
    help,/str,wsizes
    print,'moviev.hsize=',moviev.hsize,', moviev.vsize=',moviev.vsize
    help,testhsize,testvsize
    print,'screensize=',ss
    print,'scrlwidgetsize=',scrlsize
    wait,2
endif

   WIDGET_CONTROL, /REAL, cmndbase
   if winbase ne -1 then WIDGET_CONTROL, /REAL, winbase
   WIDGET_CONTROL, draw_w, GET_VALUE=draw_win

   if winbase eq -1 then WIDGET_CONTROL, cmndbase,TLB_GET_OFFSET=tbloff,TLB_GET_SIZE=tblsize else $
    	    	         WIDGET_CONTROL, winbase,TLB_GET_OFFSET=tbloff,TLB_GET_SIZE=tblsize
   WIDGET_CONTROL, cmndbase,map=1
   
   ; child widget positioning logic
   if tblsize(1)+tbloff(1)+253 gt ss(1) then begin
      moviev.baseyoff(0)=ss(1)-253 
      moviev.basexoff(0)=ss(0)-539
   endif else begin
      moviev.baseyoff(0)=tblsize(1)+tbloff(1)
      moviev.basexoff(0)=0
   endelse
   if tblsize(0)+tbloff(0)+539 gt ss(0) then begin
      moviev.baseyoff(1)=ss(1)-253 
      moviev.basexoff(1)=ss(0)-539
   endif else begin
      moviev.baseyoff(1)=0
      moviev.basexoff(1)=tblsize(0)+tbloff(0)
   endelse

if moviev.debugon THEN BEGIN
    print,'moviev.baseyoff=', moviev.baseyoff
    print,'moviev.basexoff=',moviev.basexoff
    print,'TLB_OFFSET',tbloff
    print,'TLB_SIZE',tblsize
ENDIF
moviev.winbase=winbase
moviev.cmndbase=cmndbase
moviev.slidenw=slidenw
moviev.draw_win=draw_win
moviev.boxsel=''

    WIDGET_CONTROL, cmndbase, TIMER=0. 

if moviev.debugon THEN wait,5

END
