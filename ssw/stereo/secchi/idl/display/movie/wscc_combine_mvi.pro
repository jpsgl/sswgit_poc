;+
; $Id: wscc_combine_mvi.pro,v 1.77 2012/08/08 17:34:17 mcnutt Exp $
; Project     : STEREO - SECCHI 
;
; Name        : SCC_COMBINE_MVI
;
; Purpose     : Insert frames from one mvi into another to create combined movie.
;
; Explanation : 
;
; Use         : SCC_COMBINE_MVI
;
;      Example:
;
; Optional Inputs : mvifiles	STRARR - List of MVI files to combine
;
; Outputs     : None.
;
; Keywords    : /TRUECOLOR - Convert color tables in input MVIs to 24-bit color output MVI
;   	    	BSIDE=[1,2,3] = ['x','Same sun center','Left side','Right side']
;   	    	/AUTO - Skip first file input widget
;
; Calls       : scc_playmovie.pro
;
; Category    : Image Processing/Display Movie Animation 
;
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Jul 2008.
; $Log: wscc_combine_mvi.pro,v $
; Revision 1.77  2012/08/08 17:34:17  mcnutt
; corrected aorb selection if filename is not .fts
;
; Revision 1.76  2011/11/22 16:21:28  mcnutt
; adjusted window positions
;
; Revision 1.75  2011/11/01 13:16:12  mcnutt
; coorected crop selection uses moviev.cmndbase
;
; Revision 1.74  2011/11/01 12:30:42  mcnutt
; uses scc_save_mvframe when saving movie
;
; Revision 1.73  2011/10/13 13:34:21  mcnutt
; corrected mov avi movie out options
;
; Revision 1.72  2011/08/10 18:27:42  mcnutt
; added sec_pix keyword to calls for sccwrite_disk_movie
;
; Revision 1.71  2011/07/26 16:50:31  mcnutt
;  version 1.70 with stop on line 754 removed
;
; Revision 1.70  2011/05/18 22:40:10  nathan
; change references to wrunmovie to playmovie
;
; Revision 1.69  2011/05/03 19:13:22  mcnutt
; added convert2secchi to file mvi file headers
;
; Revision 1.68  2011/02/08 18:08:33  mcnutt
; added forcesize keyword to scc_combine_mvi, will not work with widget only direct call
;
; Revision 1.67  2011/02/07 19:25:15  mcnutt
; added save_only key word to create lasco combine png files
;
; Revision 1.66  2010/12/10 14:00:25  mcnutt
; removed stop
;
; Revision 1.65  2010/12/10 13:59:57  mcnutt
; uses hdr.detector for tel not file name
;
; Revision 1.64  2010/12/10 13:28:57  mcnutt
; changes notsecchi detector mask setup
;
; Revision 1.63  2010/12/08 20:07:33  mcnutt
; Added SDO and unknown to detector list
;
; Revision 1.62  2010/11/23 13:08:41  mcnutt
; added deprojected HI1 mask options
;
; Revision 1.61  2010/11/18 19:07:21  mcnutt
; selects aorb strpos "." -1
;
; Revision 1.60  2010/11/02 19:04:04  mcnutt
; sets sunxcen and sunycen for RTHETA movies to [theta(0),radius(0)]
;
; Revision 1.59  2010/10/27 17:09:03  mcnutt
; will now combine rtheta movies
;
; Revision 1.58  2010/10/08 23:52:48  nathan
; fix sec_pix bug again; add debug_on to pro get_scc_combine_mvi; define origx/y as float
;
; Revision 1.57  2010/09/29 17:09:40  mcnutt
; now works with SDO movies
;
; Revision 1.56  2010/09/21 16:56:52  mcnutt
; sets notsecchi to 0 if not soho
;
; Revision 1.55  2010/09/21 12:52:01  mcnutt
; added SOHO detectors
;
; Revision 1.54  2010/09/20 18:05:24  mcnutt
; will now read in mvi hdr movies(gif and png movies)
;
; Revision 1.53  2010/08/17 21:09:43  nathan
; bad stop
;
; Revision 1.52  2010/08/17 21:05:14  nathan
; last fix was not working for hi2 binned...
;
; Revision 1.51  2010/08/11 21:48:19  nathan
; auto fix case where cdelt in mvi headers is in deg
;
; Revision 1.50  2010/05/28 12:24:03  mcnutt
; change the crop inputs to be more user friendly
;
; Revision 1.49  2010/03/03 18:03:44  mcnutt
; added correction to crop and correct hi1 deprojected mask
;
; Revision 1.48  2010/02/24 18:09:26  mcnutt
; added option to write mpg movie while creating frames
;
; Revision 1.47  2010/02/24 12:51:24  mcnutt
; corrected size of text template for ture color movies
;
; Revision 1.46  2010/02/22 19:31:56  mcnutt
; removed stop
;
; Revision 1.45  2010/02/22 19:14:11  mcnutt
; removed round from scale size
;
; Revision 1.44  2010/02/04 20:01:38  mcnutt
; added sun center correction for cropped movies
;
; Revision 1.43  2010/02/03 19:01:03  mcnutt
; uses ccdpos to set scale and coordadj to needed to work with cropped movies
;
; Revision 1.42  2010/01/13 14:53:22  mcnutt
; corrected time start to use correct date
;
; Revision 1.41  2010/01/11 21:57:26  nathan
; adjusted ytsize of timestamp box
;
; Revision 1.40  2009/12/08 16:15:39  mcnutt
; removed some add_tags
;
; Revision 1.39  2009/11/17 18:16:43  mcnutt
; added maks for deprojected hi1
;
; Revision 1.38  2009/11/16 18:51:49  mcnutt
; prints status to screen while creating frames
;
; Revision 1.37  2009/11/16 18:43:59  mcnutt
; added backgound color section
;
; Revision 1.36  2009/10/21 11:40:29  mcnutt
; corrected mvi size on info line
;
; Revision 1.35  2009/10/20 19:43:20  mcnutt
; added line on control that output movie size
;
; Revision 1.34  2009/10/20 18:47:39  mcnutt
; defines path for movie names
;
; Revision 1.33  2009/10/19 19:26:40  mcnutt
; defines draw window to work with scc_movie_win.pro
;
; Revision 1.32  2009/09/24 19:37:03  mcnutt
; adds textframe to movie if defined
;
; Revision 1.31  2009/09/21 21:57:38  nathan
; added some debug lines, put wscc_mkmovie_common in main
;
; Revision 1.30  2009/09/04 13:22:02  mcnutt
; corrected ccdpos for deprojected hi movies
;
; Revision 1.29  2009/08/21 17:44:14  mcnutt
; changes frame header to match mvihdr before writing movie and if CCDPOS eq 0 it does not get changed
;
; Revision 1.28  2009/05/08 16:52:46  mcnutt
; now works with truecolor input movies
;
; Revision 1.27  2009/04/10 15:41:07  mcnutt
; can save movie to a file while creating frames
;
; Revision 1.26  2009/04/09 13:32:20  mcnutt
; removed imgs from cmbmovie structure
;
; Revision 1.25  2009/04/06 18:45:08  mcnutt
; removed save png and added define imgs png movies can be saved from scc_wrunmovie with out defining imgs
;
; Revision 1.24  2009/03/20 11:18:53  mcnutt
; sets maks area to -1
;
; Revision 1.23  2009/03/17 18:40:04  mcnutt
; removed stops
;
; Revision 1.22  2009/03/13 21:32:52  avourlid
;
; simplified r_sun calculation
; removed redundant operations
;
; Revision 1.21  2009/03/13 18:35:36  mcnutt
; creates movie frame by frame and will write pngs instead of creating imgs array
;
; Revision 1.20  2009/03/12 14:06:22  mcnutt
; added PNG save option
;
; Revision 1.19  2009/03/12 12:22:34  mcnutt
; removed for loop when converting to truecolor
;
; Revision 1.18  2009/03/11 19:10:30  mcnutt
; removed mviout array and undefines mvoutwt and mviouttc
;
; Revision 1.17  2009/02/20 18:05:43  mcnutt
; changed auto to skip preview
;
; Revision 1.16  2009/01/15 22:25:36  nathan
; add /AUTO and BSIDE keywords
;
; Revision 1.15  2009/01/13 15:56:15  mcnutt
; change frame hdr times to smallest cadance
;
; Revision 1.14  2009/01/09 18:06:47  mcnutt
; corrected offsets for b right side movies
;
; Revision 1.13  2008/11/03 12:26:21  mcnutt
; adjusted ccd_postion for movie to work with coord in scc_wrunmoviem
;
; Revision 1.12  2008/10/07 17:55:54  nathan
; added comments
;

PRO GET_SCC_COMBINE_MVI, mvis

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
common SCC_COMBINE, cmbmovie,cmbstate,struct;,mviout,mvihdr,mviahdrs,mviouttc,win_index
   ;** MVI1

cmbmovie=-1
cmbstate=-1
mvtype=strarr(n_elements(mvis))
cmbmovie={ mvis:mvis, mvtype:mvtype }
 if (datatype(debug_on) eq 'UND') then debug_on=0

for rdm=0,n_elements(mvis)-1 do begin

  if strpos(mvis(rdm),'.hdr') gt -1 then pngmvi=mvis(rdm) else pngmvi='0'
  OPENR, lu1, mvis(rdm),/get_lun

; Check if call to read_mvi should return rgb color vectors (true if
; split_colors keyword is set.)

;rgbvec1=-1
;   IF NOT(KEYWORD_SET(SPLIT_COLORS)) THEN $
;    READ_MVI, lu1, file_hdr1, ihdrs1, imgs1, swapflag $
;   ELSE BEGIN
       rgbvec=bytarr(!d.n_colors,3)
       SCCREAD_MVI, lu1, file_hdr1, ihdrs1, imgs1, swapflag, rgbvec=rgbvec, pngmvi=pngmvi
;   ENDELSE   
   cmbmovie.mvtype(rdm)=pngmvi
;   images = bytarr(file_hdr1.nx, file_hdr1.ny, file_hdr1.nf)
   FOR i=0, file_hdr1.nf-1 DO BEGIN
;      images(*,*,i)= imgs1[i]     
      ahdrt = SCCMVIHDR2STRUCT(ihdrs1(i),file_hdr1.ver)
      ahdr = convert2secchi(ahdrt,file_hdr1, debug=debug_on)
      if strlen(ahdr.time_obs) lt 5 then begin
         ahdr.time_obs=strmid(ahdr.date_obs,11,8)
         ahdr.date_obs=strmid(ahdr.date_obs,0,10)
      endif
      if file_hdr1.sunxcen le 0 then file_hdr1.sunxcen=ahdr.crpix1
      if file_hdr1.sunycen le 0 then file_hdr1.sunycen=ahdr.crpix2
      if file_hdr1.sec_pix le 0 and file_hdr1.rtheta eq 0 then file_hdr1.sec_pix=ahdr.CDELT1  ;correct hi2 sec_pix not saved correctly in sccwrite_disk_movie
      IF (ahdr.detector eq 'HI2' or ahdr.detector eq 'HI1') and i eq 0  and file_hdr1.rtheta eq 0 THEN BEGIN 
                        filen=ahdr.filename
         	  	fname=sccfindfits(filen)
		        IF fname EQ '' THEN BEGIN
			     strput,filen,'?',16
			     fname = sccfindfits(filen)
   		        ENDIF
 		        if fname NE  '' then jk=sccreadfits(fname,fhdr,/nodata) else fhdr=mk_short_scc_hdr(ahdr,file_hdr1)
                        hi_fix_pointing,fhdr
                        matchfitshdr2mvi,fhdr,file_hdr1,ahdr;,notsecchi=notsecchi
                        fcen=scc_sun_center(fhdr)
                        IF (i EQ 0) THEN BEGIN                        
                          file_hdr1.sunxcen=fcen.xcen
                          file_hdr1.sunycen=fcen.ycen
                        ENDIF
                       ahdr.xcen=fcen.xcen
                       ahdr.ycen=fcen.ycen
      ENDIF

       IF (i EQ 0) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
   ENDFOR
    IF file_hdr1.sec_pix LT 0.2  and file_hdr1.rtheta eq 0 THEN file_hdr1.sec_pix=ahdr.cdelt1*3600. ; fix case where sec_pix and cdelt1 saved as deg in generic_movie.pro,v1.30 or previous line
    IF file_hdr1.sec_pix LT 0.2  and file_hdr1.rtheta eq 0 THEN file_hdr1.sec_pix=file_hdr1.sec_pix*3600. ; fix case where sec_pix and cdelt1 saved as deg in generic_movie.pro,v1.30 or previous line
    IF debug_on then begin
    	print,'framehdr:',ahdr
	print,'file_hdr:',file_hdr1
	wait,2
    ENDIF

   if rdm eq 0 then tel=strmid(hdrs(0).FILENAME,strpos(hdrs(0).FILENAME,'.f')-3,3) else tel=[tel,strmid(hdrs(0).FILENAME,strpos(hdrs(0).FILENAME,'.f')-3,3)]
   if rdm eq 0 then proj=hdrs(0).FILTER else proj=[proj,hdrs(0).FILTER]
   if rdm eq 0 then mvihdrs={file_hdr:file_hdr1} else mvihdrs={file_hdr:[mvihdrs.file_hdr,file_hdr1]}
   if strpos(HDRS(0).OBSRVTRY,'STEREO') gt -1 then sc=strmid(hdrs(0).OBSRVTRY,strlen(hdrs(0).OBSRVTRY)-1,1) else sc=''
   if rdm eq 0 then det=hdrs(0).detector+sc else det=[det,hdrs(0).detector+sc]
  
;   mvi={img:images,hdrs:hdrs,rgbvec:rgbvec}
   mvi={hdrs:hdrs,rgbvec:rgbvec}
   
   tagname='mvi'+string(rdm,'(i1)')
   cmbmovie=add_tag(cmbmovie,mvi,tagname)

   
   close, lu1
   free_lun,lu1  

endfor
if strlowcase(det(0)) eq 'c2' or strlowcase(det(0)) eq 'c3' or strlowcase(det(0)) eq 'eit' then tel=det
cmbmovie=add_tag(cmbmovie,mvihdrs,'mvihdrs')
cmbmovie=add_tag(cmbmovie,det,'tels')
cmbmovie=add_tag(cmbmovie,proj,'proj')
cmbmovie=add_tag(cmbmovie,[0,0,0],'bkgrd')

end

;
;-------------------------------------------------------------------------------------------
;

pro scc_combine_mvi ,cropxy=cropxy, pixscale=pixscale, TRUECOLOR=truecolor, bpos=bpos,times=times,$
    preview=preview, restore=restore, defimgs=defimgs, outmovie=outmovie, save_only=save_only, forcesize=forcesize, _EXTRA=_extra

common SCC_COMBINE
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

loadct,0

if datatype(imgs) ne 'UND' then undefine,imgs

wdel,/all

ishi=where(strpos(cmbmovie.tels,'h') gt -1)
as=where(strpos(cmbmovie.tels,'A') gt -1)
bs=where(strpos(cmbmovie.tels,'B') gt -1)
ls=where(cmbmovie.tels eq 'AIA',isaia)
ls=where(cmbmovie.tels eq 'HDMI',ishdmi)
ls=where(cmbmovie.tels eq 'MDI',ismdi)
ls=where(cmbmovie.tels eq 'C2',isc2)
ls=where(cmbmovie.tels eq 'C3',isc3)
ls=where(cmbmovie.tels eq 'EIT',iseit)
ls=where(cmbmovie.tels eq 'SDO',issdo)
ls=where(cmbmovie.tels eq '',isunk)
issoho=total([isc2,isc3,iseit])
if isunk then begin
  for tn=0,n_Elements(cmbmovie.tels)-1 do begin
    if cmbmovie.tels(tn) eq '' then begin
       tmptel=''
       read,'Enter telescope name for movie '+cmbmovie.mvis(tn)+' (ie AIA HMDI MDI) :',tmptel
       cmbmovie.tels(tn)=strupcase(tmptel)
     if tn eq 0 then cmbmovie.mvi0.hdrs.detector=strupcase(tmptel)
     if tn eq 1 then cmbmovie.mvi1.hdrs.detector=strupcase(tmptel) 
     if tn eq 2 then cmbmovie.mvi2.hdrs.detector=strupcase(tmptel)   
     if tn eq 3 then cmbmovie.mvi3.hdrs.detector=strupcase(tmptel)   
     if tn eq 4 then cmbmovie.mvi4.hdrs.detector=strupcase(tmptel)   
     if tn eq 5 then cmbmovie.mvi5.hdrs.detector=strupcase(tmptel)   
     if tn eq 6 then cmbmovie.mvi6.hdrs.detector=strupcase(tmptel)   
     if tn eq 7 then cmbmovie.mvi7.hdrs.detector=strupcase(tmptel)   
     if tn eq 8 then cmbmovie.mvi8.hdrs.detector=strupcase(tmptel)  
     if tn eq 9 then cmbmovie.mvi9.hdrs.detector=strupcase(tmptel)   
    endif
  endfor
endif
notsecchi=total([isaia,ishdmi,ismdi,issoho,issdo,isunk])
if datatype(cmbstate) ne 'STC' then begin
   detectors=['HI2A','HI1A','COR2A','COR1A','EUVIA','EUVIB','COR1B','COR2B','HI1B','HI2B','C3','C2','EIT','AIA','HDMI','MDI']   
   masks=['hi2A_mask.fts','','cor2A_mask.fts','cor1_mask.fts','euvi_mask.fts','euvi_mask.fts','cor1_mask.fts','cor2B_mask.fts','','hi2B_mask.fts','','','','','','']
   outer=[-1,0,-1,3.5,1.6,1.6,3.5,-1,0,-1,0,4.25,1.25,1.25,1.25,1.25]
   if notsecchi gt 0 then begin
      masks=strarr(n_Elements(cmbmovie.tels))
      touter=fltarr(n_Elements(cmbmovie.tels))
      for tn=0,n_Elements(cmbmovie.tels)-1 do begin
          z=where(detectors eq cmbmovie.tels(tn))
	  if z(0) ne -1 then touter(tn)=outer(z)
      endfor
      detectors=cmbmovie.tels & outer=touter
   endif 
    cmbstate={ $
        origx:[0.,1.] ,$
	origy:[0.,1.] ,$
        sec_pixs:fltarr(10) ,$
        scale:1.0 ,$
        origscale:0.0 ,$
        truv:0 ,$
        times:0.0 ,$
        newx:[0.,1.] ,$
        newy:[0.,1.] ,$
        rzcen:[0,0] ,$
        crop:0 ,$
        coordadj:0 ,$
	bpos:0 ,$ 
	estmvisiz:0.0 ,$ 
        detectors:detectors ,$
        rtheta:0 ,$
        ylog:0 ,$
        rtaxis:0 ,$
        rtsize:[1024,1024] ,$
        rttha:[0,360] ,$
        rtthb:[0,360] ,$
        rtra:[0.5,88.] ,$
        masks:masks,$
        outer:outer,$
	notsecchi:0 }
endif

rtheta=where(cmbmovie.mvihdrs.file_hdr.rtheta eq 1)
if rtheta(0) gt -1 and cmbstate.rtheta eq 0 then begin
   cmbstate.rtheta=1
   cmbstate.outer(*)=0
   cmbstate.ylog=1
   cmbstate.rtsize=[1024,1024]
   cmbstate.rttha=[0,360]
   cmbstate.rtthb=[0,360]
   cmbstate.rtra=[0.5,88.]
endif
if keyword_set(bpos) then cmbstate.bpos=bpos
if as(0) eq -1 then cmbstate.bpos = 0  ;ignore bpos if movies are all from the same spacecraft
if bs(0) eq -1 then cmbstate.bpos = 0
if notsecchi gt 0 then begin
   cmbstate.bpos = 0
   cmbstate.notsecchi=notsecchi
endif else cmbstate.notsecchi= 0

fsc=intarr(n_elements(cmbmovie.tels))
if cmbstate.bpos eq 1 and  bs(0) gt -1 then fsc(bs)=20
if cmbstate.bpos eq 2  and  as(0) gt -1 then fsc(as)=20
if cmbstate.bpos eq 0 and as(0) gt -1 and bs(0) gt -1 then fsc(as)=20

sctext=['SECCHI  ','']
if cmbstate.bpos eq 1 then sctext=['SECCHI-B  ','SECCHI-A ']
if cmbstate.bpos eq 2 then sctext=['SECCHI-A  ','SECCHI-B ']
if cmbstate.bpos eq 0 and as(0) gt -1 and bs(0) gt -1 then sctext=['SECCHI-A  ','SECCHI-B ']
if cmbstate.bpos eq 0 and issoho gt 0 then sctext=['SOHO ','']


if keyword_set(cropxy) then cropxy=round(cropxy/cmbstate.scale) ;correct cropxy if movie was previouly scaled.

if keyword_set(restore) then begin
   cmbstate.crop=0
   cmbstate.origscale=0.
endif
if keyword_set(TRUECOLOR) then cmbstate.truv=1 else cmbstate.truv=0

if keyword_set(times) then begin
   cmbstate.times=times
   ytsize=fix(cmbstate.times*7.+2 ) ;add text line in multiples of 16
endif  else  begin
   cmbstate.times=0
   ytsize=0
endelse

if cmbstate.origscale eq 0 and cmbstate.crop eq 0 then begin
;set default scale 512 by 512
  sec_pixs=cmbmovie.mvihdrs.file_hdr.SEC_PIX
  hsizes=cmbmovie.mvihdrs.file_hdr.nx
  maxpix=(where(sec_pixs*hsizes eq max(sec_pixs*hsizes))) & maxpix=maxpix(0)
  cmbstate.sec_pixs=sec_pixs
   ;use ccd position if available required if input movie is cropped
  if cmbstate.rtheta eq 0 then begin
    if issoho eq 0 then begin
      scalefac=4. & fullsize=2048
    endif else begin
      scalefac=2. & fullsize=1024
    endelse 
    if total(cmbmovie.mvihdrs.file_hdr(maxpix).CCD_POS) gt 0 then begin
       cmbstate.scale=((cmbmovie.mvihdrs.file_hdr(maxpix).ccd_pos(3)-cmbmovie.mvihdrs.file_hdr(maxpix).ccd_pos(1)+1)/scalefac)/hsizes(maxpix)
       cmbstate.coordadj=round((cmbmovie.mvihdrs.file_hdr(maxpix).ccd_pos(3)-cmbmovie.mvihdrs.file_hdr(maxpix).ccd_pos(1)+1.)/hsizes(maxpix))
    endif else begin
       cmbstate.scale=512./hsizes(maxpix)
       cmbstate.coordadj=fullsize/hsizes(maxpix)
    endelse
  endif
  cmbstate.origscale=cmbstate.scale
endif

if keyword_set(pixscale) then begin
   cmbstate.newx=round(cmbstate.newx/cmbstate.scale)  ;clear old scale factor
   cmbstate.newy=round(cmbstate.newy/cmbstate.scale)
   cmbstate.rzcen=round(cmbstate.rzcen/cmbstate.scale)
   cmbstate.scale=(max(cmbstate.sec_pixs)/pixscale)  ;scale is set in as output pixsize
   cmbstate.newx=round(cmbstate.newx*cmbstate.scale)
   cmbstate.newy=round(cmbstate.newy*cmbstate.scale)
   cmbstate.rzcen=round(cmbstate.rzcen*cmbstate.scale)
endif 

sec_pixs=(cmbmovie.mvihdrs.file_hdr.SEC_PIX/cmbstate.scale)
sunx=cmbmovie.mvihdrs.file_hdr.sunxcen*cmbstate.scale
suny=cmbmovie.mvihdrs.file_hdr.sunycen*cmbstate.scale
vsizes=(cmbmovie.mvihdrs.file_hdr.ny*cmbstate.scale)
hsizes=(cmbmovie.mvihdrs.file_hdr.nx*cmbstate.scale)
nf=cmbmovie.mvihdrs.file_hdr.nf

xisize=round(hsizes/(max(sec_pixs)/sec_pixs))
yisize=round(vsizes/(max(sec_pixs)/sec_pixs))
msize=[max(xisize),max(yisize)]

order=reverse(sort(xisize+fsc)) ;largest to smallest image size
big=order(0)

 msuny=suny(big)
 msunx=sunx(big)
 msec_pixs=sec_pixs(big)

;x1=round(msunx-sunx/(max(sec_pixs)/sec_pixs))
;x2=round(x1+xisize)-1
;y1=round(msuny-suny/(max(sec_pixs)/sec_pixs))
;y2=round(y1+yisize)-1
x1=(msunx-sunx/(max(sec_pixs)/sec_pixs))
x2=(x1+xisize)-1
y1=(msuny-suny/(max(sec_pixs)/sec_pixs))
y2=(y1+yisize)-1

ajdx=where(x2 gt msize(0))
if ajdx(0) gt -1 then msize=[max(x2)+1,msize(1)]

ajdx=where(x1 lt 0)
if ajdx(0) gt -1 then begin
   msize=[msize(0)+abs(min(x1)),msize(1)]
   x1=x1+abs(min(x1))
   x2=(x1+xisize)-1
endif

ajdy=where(y1 lt 0)
if ajdy(0) gt -1 then begin
   msize=[msize(0),msize(1)+abs(min(y1))]
   y1=y1+abs(min(y1))
   y2=(y1+yisize)-1
endif  

ajdy=where(y2 ge msize(1))
if ajdy(0) gt -1 then msize=[msize(0),max(y2)+1]

   if cmbstate.origx(1) eq 1 then begin  ;set orig movie size 1 time only.
      cmbstate.origx=[0,msize(0)/cmbstate.scale]
      cmbstate.origy=[0,msize(1)/cmbstate.scale]
  endif

if cmbstate.crop eq 0 and ~keyword_set(pixscale) then begin
     cmbstate.newx=[0,msize(0)-1]
     cmbstate.newy=[0,msize(1)-1]
     cmbstate.rzcen=[0,0]
     cmbstate.crop=0
endif

if keyword_set(cropxy) then begin
     cmbstate.crop=1
     xvs=[cropxy(0),cropxy(2)] & xvs=xvs(sort(xvs)) 
     yvs=[cropxy(1)-ytsize,cropxy(3)-ytsize] & yvs=yvs(sort(yvs)) 
     if cmbstate.newx(0) ne cmbstate.origx(0) then begin ;account for previous zoom
        xvs=fix(xvs+cmbstate.newx(0)/cmbstate.scale)
        yvs=fix(yvs+cmbstate.newy(0)/cmbstate.scale)
     endif
     if (xvs(1) ge cmbstate.origx(1))then xvs(1)=cmbstate.origx(1)
;     xscale=round((cmbstate.origx(1)*1.-cmbstate.origx(0))/(xvs(1)-xvs(0)))
;     yscale=round((cmbstate.origy(1)*1.-cmbstate.origy(0))/(yvs(1)-yvs(0)))
;     if ~keyword_set(pixscale) then cmbstate.scale=max(xscale,yscale)
     cmbstate.newx=round(xvs*cmbstate.scale)
     cmbstate.newy=round(yvs*cmbstate.scale)
     if (n_elements(cropxy) gt 4) then cmbstate.rzcen(0:1)=cropxy(4:5)*cmbstate.scale
endif

if keyword_set(forcesize) then begin
    cmbstate.newx(1)=(cmbstate.newx(0)+forcesize(0))-1
    cmbstate.newy(1)=(cmbstate.newy(0)+forcesize(1))-(1+ytsize)
endif

xysize=[cmbstate.newx(1)-cmbstate.newx(0)+1,cmbstate.newy(1)-cmbstate.newy(0)+1]

axy=[0,xysize(0)-1,0,xysize(1)-1]
bxy=axy
aoff=[0,0,0,0] & boff=[0,0,0,0]
leftxmax=cmbstate.newx(1)-cmbstate.newx(0)
leftymax=cmbstate.newy(1)-cmbstate.newy(0)


if cmbstate.bpos eq 1 and ishi(0) eq -1 then begin 
    xysize(0)=xysize(0)*2
    axy=[xysize(0)/2,xysize(0)-1,0,xysize(1)-1]
    aoff(0:1)=cmbstate.rzcen(0) & aoff(2:3)=cmbstate.rzcen(1)
endif
if cmbstate.bpos eq 2 and ishi(0) eq -1 then begin 
    xysize(0)=xysize(0)*2
    bxy=[xysize(0)/2,xysize(0)-1,0,xysize(1)-1]
    boff(0:1)=cmbstate.rzcen(0) & boff(2:3)=cmbstate.rzcen(1)
endif

if cmbstate.bpos eq 2 and ishi(0) gt -1 and cmbstate.crop eq 0 then begin
    as=where(strpos(cmbmovie.tels,'A') gt -1)
    bs=where(strpos(cmbmovie.tels,'B') gt -1)
    axy=[0,max(x2(as)),0,xysize(1)-1]
    bxy(0)=axy(1)+1
    bxy(1)=max(x2(bs))+(max(x2(as))-min(x1(bs)))+1
    xysize=[bxy(1)+1,bxy(3)+1]
    boff=[cmbstate.newx(1)-(bxy(1)-bxy(0)),0,0,0]
    aoff=[0,(axy(1)-cmbstate.newx(1)),0,0]
    leftxmax=axy(1)
endif

if cmbstate.bpos eq 2 and ishi(0) gt -1 and cmbstate.crop eq 1 then begin
    xysize(0)=xysize(0)*2
    bs=where(strpos(cmbmovie.tels,'B') gt -1)
    bxy=[xysize(0)/2,xysize(0)-1,0,xysize(1)-1]
    boff(0:1)=cmbstate.rzcen(0)+min(x1(bs))
    boff(2:3)=cmbstate.rzcen(1)
endif

if cmbstate.rtheta eq 1 then begin
   xysize=cmbstate.rtsize
   msize=cmbstate.rtsize
   axy=[0,xysize(0)-1,0,xysize(1)-1]
   bxy=axy
   if cmbstate.bpos eq 1 then begin
     msize(0)=cmbstate.rtsize(0)/2
     axy=[xysize(0)/2,xysize(0)-1,0,xysize(1)-1]
     bxy=[0,(xysize(0)/2)-1,0,xysize(1)-1]
   endif
   if cmbstate.bpos eq 2 then begin
     msize(0)=cmbstate.rtsize(0)/2
     bxy=[xysize(0)/2,xysize(0)-1,0,xysize(1)-1]
     axy=[0,(xysize(0)/2)-1,0,xysize(1)-1]
   endif
   if cmbstate.bpos eq 0 then cmbstate.rtthb=cmbstate.rttha
endif


mtaic=dblarr(max(nf),n_Elements(cmbmovie.mvis))
for tn=0,n_Elements(cmbmovie.mvis)-1 do begin
     if tn eq 0 then mvin=cmbmovie.mvi0
     if tn eq 1 then mvin=cmbmovie.mvi1  
     if tn eq 2 then mvin=cmbmovie.mvi2  
     if tn eq 3 then mvin=cmbmovie.mvi3  
     if tn eq 4 then mvin=cmbmovie.mvi4  
     if tn eq 5 then mvin=cmbmovie.mvi5  
     if tn eq 6 then mvin=cmbmovie.mvi6  
     if tn eq 7 then mvin=cmbmovie.mvi7  
     if tn eq 8 then mvin=cmbmovie.mvi8 
     if tn eq 9 then mvin=cmbmovie.mvi9  
     mtaic(0:nf(tn)-1,tn)=anytim2tai(mvin.hdrs.date_obs+' '+strmid(mvin.hdrs.time_obs,0,6))
endfor
cadance=min(mtaic(1,*)-mtaic(0,*))
alltimes=mtaic(sort(mtaic)) & alltimes=alltimes(where(alltimes ne 0))
n=0
while n le n_Elements(alltimes)-1 do begin
  n2=where(alltimes le alltimes(n)+cadance/2)
  if n eq 0 then tai1=alltimes(n_Elements(n2)-1) else tai1=[tai1,alltimes(n_Elements(n2)-1)]
  n=n2(n_Elements(n2)-1) +1
endwhile
tframes=n_elements(tai1)
if keyword_set(preview) then begin
   nframes=3 
   tai1=[tai1(0),tai1(n_Elements(tai1)/2),tai1(n_Elements(tai1)-1)]
   defimgs=1
endif else begin 
  nframes=n_elements(tai1)
  print,'Creating movie frames...It may take a while depending on the movie size/number of frames'
endelse 


mviahdrs=replicate(cmbmovie.mvi0.hdrs(0),nframes)

   mvihdr=cmbmovie.mvihdrs.file_hdr(big)
   mvihdr.NF=n_elements(nframes)
   mvihdr.NX=xysize(0)
   mvihdr.NY=xysize(1)+ytsize
   mvihdr.MX =n_elements(nframes)
;   mvihdr.NB              LONG               136
;   mvihdr.VER             INT              6
;   mvihdr.FH_NB           INT            810
   mvihdr.SUNXCEN =msunx - (cmbstate.newx(0)-x1(big)) 
   mvihdr.SUNYCEN =msuny - (cmbstate.newy(0)-y1(big)) + ytsize
   mvihdr.SEC_PIX =msec_pixs 
if cmbstate.rtheta eq 1 then begin
   mvihdr.RTHETA          =cmbstate.rtheta+cmbstate.ylog
   mvihdr.RADIUS0=cmbstate.rtra(0);         FLOAT           0.00000
   mvihdr.RADIUS1=cmbstate.rtra(1);         FLOAT           0.00000
   mvihdr.THETA0=cmbstate.rttha(0);          FLOAT           0.00000
   mvihdr.THETA1=cmbstate.rttha(1);          FLOAT           0.00000
   mvihdr.sunycen=0+ytsize
   mvihdr.sunxcen=0
endif

;   mvihdr.RECTIFIED       INT              0
if total(cmbmovie.mvihdrs.file_hdr(big).CCD_POS) gt 0 then begin
        mccd_pos=cmbmovie.mvihdrs.file_hdr(big).CCD_POS ;[y1,x1,y2,x2]
        ccdsize=([cmbstate.newy(1)-cmbstate.newy(0)+1,cmbstate.newx(1)-cmbstate.newx(0)+1]*cmbstate.coordadj)/cmbstate.scale
        if cmbmovie.proj(big) eq 'Deproject' then  ccdsize(0) = ccdsize(0)/(vsizes(big)/hsizes(big))
        if cmbstate.newy(0) gt  y1(big)then ccdy0=cmbstate.newy(0)-y1(big) else ccdy0=cmbstate.newy(0)-(cmbstate.newy(0) -y1(big))*(-1)
        if cmbstate.newx(0) gt  x1(big)then ccdx0=cmbstate.newx(0)-x1(big) else ccdx0=cmbstate.newx(0)-(cmbstate.newx(0) -x1(big))*(-1)
	mvihdr.ccd_pos(1)=mccd_pos(1)+(ccdx0*cmbstate.coordadj)/cmbstate.scale
        mvihdr.ccd_pos(3)=mvihdr.ccd_pos(1)+ccdsize(1)-1
	mvihdr.ccd_pos(0)=mccd_pos(0)+(ccdy0*cmbstate.coordadj)/cmbstate.scale
        mvihdr.ccd_pos(2)=mvihdr.ccd_pos(0)+ccdsize(0)-1
        mvihdr.ccd_pos(0)=mvihdr.ccd_pos(0)-((ytsize*cmbstate.coordadj)/cmbstate.scale)
        print,mvihdr.ccd_pos
	print,mccd_pos
endif else mvihdr.ccd_pos=cmbmovie.mvihdrs.file_hdr(big).CCD_POS

if keyword_set(truecolor) then mvihdr.TRUECOLOR=1 else mvihdr.TRUECOLOR=0

if keyword_set(outmovie) then  begin
   if datatype(outmovie) ne 'STC' then outmovie={outfile:outmovie, SCALE:1, REFERENCE:'', mframes:1, idlencode:0,colorquan:1}
   BREAK_FILE, outmovie.outfile, a, outdir, outname, outext
endif
if keyword_set(defimgs) and ~keyword_set(truecolor) then imgs = bytarr(xysize(0),mvihdr.NY,nframes)
if keyword_set(truecolor) and keyword_set(defimgs) then  imgs = bytarr(3,xysize(0),mvihdr.NY,nframes)
if keyword_set(times) or cmbstate.times gt 0 then tximg=intarr(xysize(0),mvihdr.NY)

cmbstate.estmvisiz=(tframes*1.0)*(1.0+truecolor*2.0) * xysize(0) * mvihdr.NY

outmask = bytarr(msize(0),msize(1),n_Elements(cmbmovie.mvis))
if cmbstate.rtheta eq 0 then tmp2 = intarr(xysize(0),xysize(1)) else tmp2 = intarr(msize(0),msize(1))
cimgt = intarr(xysize(0),xysize(1))
msktmp = bytarr(msize(0),msize(1),n_Elements(cmbmovie.mvis))
tmp = intarr(msize(0),msize(1))
usef = intarr(nframes,n_Elements(cmbmovie.mvis))
ftimes = strarr(nframes,n_Elements(cmbmovie.mvis))
lus=intarr(10) +(-1)

for n=0,n_Elements(cmbmovie.mvis)-1 do begin
     if order(n) eq 0 then mvin=cmbmovie.mvi0
     if order(n) eq 1 then mvin=cmbmovie.mvi1  
     if order(n) eq 2 then mvin=cmbmovie.mvi2  
     if order(n) eq 3 then mvin=cmbmovie.mvi3  
     if order(n) eq 4 then mvin=cmbmovie.mvi4  
     if order(n) eq 5 then mvin=cmbmovie.mvi5  
     if order(n) eq 6 then mvin=cmbmovie.mvi6  
     if order(n) eq 7 then mvin=cmbmovie.mvi7  
     if order(n) eq 8 then mvin=cmbmovie.mvi8 
     if order(n) eq 9 then mvin=cmbmovie.mvi9  
     if notsecchi gt 0 then aorb='' else aorb=strmid(mvin.hdrs(0).FILENAME,strpos(mvin.hdrs(0).FILENAME,'.')-1,1)
     taic=anytim2tai(mvin.hdrs.date_obs+' '+strmid(mvin.hdrs.time_obs,0,6))

     for nc=0,nframes-1 do begin
         uts=max(where(taic le tai1(nc)))
         if uts ne -1 then usef(nc,n)=uts else usef(nc,n)=0
         if n eq 0 then begin
	     ftimes(nc,n)= strmid(utc2str(tai2utc(tai1(nc))),0,10)+'  '+mvin.hdrs(0).detector+aorb+' '+strmid(mvin.hdrs(usef(nc,n)).time_obs,0,8)
	 endif else ftimes(nc,n)=mvin.hdrs(0).detector+aorb+' '+strmid(mvin.hdrs(usef(nc,n)).time_obs,0,8)
;         if taic(usef(nc)) eq tai1(nc) then mviahdrs(nc)=mvin.hdrs(usef(nc))
     endfor
     if n eq 0 then begin
        red = [mvin.rgbvec(indgen(256), 0)]
        green = [mvin.rgbvec(indgen(256), 1)]
        blue = [mvin.rgbvec(indgen(256), 2)]
     endif else begin
        red = [red,mvin.rgbvec(indgen(256), 0)]
        green = [green,mvin.rgbvec(indgen(256), 1)]
        blue = [blue,mvin.rgbvec(indgen(256), 2)]
     endelse  

   if aorb eq 'B' then uxy=bxy else uxy=axy
   if aorb eq 'B' then xyoff=boff else xyoff=aoff
   if n eq 0 then begin
     mviahdrs=mvin.hdrs(usef(*,n))
     mviahdrs.date_obs=strmid(utc2str(tai2utc(tai1)),0,10)
     mviahdrs.time_obs=strmid(utc2str(tai2utc(tai1)),11,12)
   endif
if cmbstate.rtheta eq 0 then begin
   outmask(cmbstate.newx(0)+xyoff(0):(cmbstate.newx(1)-1)+xyoff(1),cmbstate.newy(0)+xyoff(2):(cmbstate.newy(1)-1)+xyoff(3),n)=1
   umsk=where(cmbstate.detectors eq mvin.hdrs(0).detector+aorb) 
   umsk=umsk(0)
   if cmbstate.outer(umsk) eq -1 then begin
        maskfile=getenv('SECCHI_CAL')+'/'+ cmbstate.masks(umsk)
        mskim=congrid(sccreadfits(maskfile,mhdr)+1,xisize(order(n)),yisize(order(n)))
   endif
   if cmbstate.outer(umsk) eq 0 then mskim=bytarr(xisize(order(n)),yisize(order(n)))+2
   if cmbstate.outer(umsk) gt 0 and mvin.hdrs(0).detector ne 'HI1' and mvin.hdrs(0).detector ne 'HI2' then begin  ; add interactivee scip masks   

        device,get_decomposed=decom
        device,decompose=1
        if aorb eq '' then begin
	   aorb='SOHO'
           load_soho_spice 
        endif
        hee_r= GET_STEREO_LONLAT(mvin.hdrs(0).date_obs, aorb, /degrees , system='HEE',_extra=_extra)
;        r_sun = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0) ; convert from radian to arcs
;        r_sun = r_sun/cmbmovie.mvihdrs.file_hdr(order(n)).SEC_PIX  ; radius of sun (pixels)
        r_sun = 6.96d5/hee_r(0) * 206265/cmbmovie.mvihdrs.file_hdr(order(n)).SEC_PIX ;convert from radian to arcs
        r_out_occ = r_sun*cmbstate.outer(umsk)
;        set_plot,'z'
;        device,set_resolution=[cmbmovie.mvihdrs.file_hdr(order(n)).NX,cmbmovie.mvihdrs.file_hdr(order(n)).NY]        
	WINDOW,  XSIZE = cmbmovie.mvihdrs.file_hdr(order(n)).NX, YSIZE =cmbmovie.mvihdrs.file_hdr(order(n)).NY, /PIXMAP,/FREE
        wtmp=!d.window
        TVCIRCLE, r_out_occ, cmbmovie.mvihdrs.file_hdr(order(n)).SUNXCEN, cmbmovie.mvihdrs.file_hdr(order(n)).SUNYCEN, /FILL, COLOR=255
        mskimt=TVRD()
        mskimt(where(mskimt eq 255))=1
        mskim=congrid(mskimt+1,xisize(order(n)),yisize(order(n)))
        device,decompose=decom
        wdel,wtmp
        wtmp=-1
    endif
    if mvin.hdrs(0).detector eq 'HI1' and mvin.hdrs(0).filter eq 'Deproject' and cmbstate.outer(umsk) gt 0 then begin ;create a mask for hi1 if deprojected only
       print,'Creating mask HI1 Deprojected image'
       uimg = bytarr(cmbmovie.mvihdrs.file_hdr(order(n)).nx, cmbmovie.mvihdrs.file_hdr(order(n)).ny)
       OPENR, lutmp, cmbmovie.mvis(order(n)),/get_lun
       SCCREAD_MVI,lutmp, file_hdr, ihdrs, imgst, swapflag, pngmvi=cmbmovie.mvtype(order(n))
       if (cmbmovie.mvtype(order(n)) ne '0') THEN uimg(*,*)=get_hdr_movie_img(string(imgst(0)),mviname=cmbmovie.mvis(order(n))) else uimg(*,*) = imgst(0)
       close,lutmp
       free_lun,lutmp
        device,get_decomposed=decom
        device,decompose=1
	WINDOW,  XSIZE = cmbmovie.mvihdrs.file_hdr(order(n)).NX, YSIZE =cmbmovie.mvihdrs.file_hdr(order(n)).NY, /PIXMAP,/FREE
        wtmp=!d.window
        contour,uimg,/device,xstyle=5,ystyle=5,levels=[1],position=[0,0,cmbmovie.mvihdrs.file_hdr(order(n)).NX,cmbmovie.mvihdrs.file_hdr(order(n)).NY],thick=1,/fill,color=2 
        mskimt=TVRD()
        for nm1=0,cmbmovie.mvihdrs.file_hdr(order(n)).NY-1 do begin
            nmsk=where(mskimt(*,nm1) eq 2)
	    if nmsk(0) gt -1 then mskimt(min(nmsk):max(nmsk),nm1)=2
	endfor
        mskim=congrid(mskimt,xisize(order(n)),yisize(order(n)))
        device,decompose=decom
        if cmbstate.outer(umsk) eq 1 then begin
	   wdel,wtmp
           wtmp=-1
	endif
    endif

   msktmp(*,*,n)=0
   msktmp(x1(order(n)):x2(order(n)),y1(order(n)):y2(order(n)),n)=mskim
endif
     if order(n) eq 0 then begin
        OPENR, lu, cmbmovie.mvis(0),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs0, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(0)=lu
     endif
     if order(n) eq 1 then  begin
        OPENR, lu, cmbmovie.mvis(1),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs1, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(1)=lu
     endif
     if order(n) eq 2 then  begin
        OPENR, lu, cmbmovie.mvis(2),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs2, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(2)=lu
     endif  
     if order(n) eq 3 then  begin
        OPENR, lu, cmbmovie.mvis(3),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs3, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(3)=lu
     endif  
     if order(n) eq 4 then  begin
        OPENR, lu, cmbmovie.mvis(4),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs4, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(4)=lu
     endif  
     if order(n) eq 5 then  begin
        OPENR, lu, cmbmovie.mvis(5),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs5, imgs5, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(5)=lu
     endif  
     if order(n) eq 6 then  begin
        OPENR, lu, cmbmovie.mvis(6),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs6, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(6)=lu
     endif 
     if order(n) eq 7 then  begin
        OPENR, lu, cmbmovie.mvis(7),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs7, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(7)=lu
     endif  
     if order(n) eq 8 then  begin
        OPENR, lu, cmbmovie.mvis(8),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs8, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(8)=lu
     endif 
     if order(n) eq 9 then  begin
        OPENR, lu, cmbmovie.mvis(9),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs9, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(9)=lu
     endif  
    IF (debug_on) THEN BEGIN
    	help,/str,file_hdr
	wait,2
    ENDIF
     undefine,lu
endfor

win_index=intarr(nframes)
first=1
for nm=0,nframes-1 do begin
   print,'Creating Frame # ',string(nm)
;   if cmbstate.rtheta gt 0 then begin
;     set_plot,'z'
;     device,set_resolution=[xysize(0),xysize(1)]
;   endif
   timgs=intarr(xysize(0),xysize(1)) -1
   uimg=intarr(xysize(0),xysize(1))
   tmp(*,*)=0
   tmp2(*,*)=0
   for n=0,n_Elements(cmbmovie.mvis)-1 do begin
     nc1=n*256 & nc2=((n+1)*256)-1
     if order(n) eq 0 then mvin=cmbmovie.mvi0
     if order(n) eq 1 then mvin=cmbmovie.mvi1  
     if order(n) eq 2 then mvin=cmbmovie.mvi2  
     if order(n) eq 3 then mvin=cmbmovie.mvi3  
     if order(n) eq 4 then mvin=cmbmovie.mvi4  
     if order(n) eq 5 then mvin=cmbmovie.mvi5  
     if order(n) eq 6 then mvin=cmbmovie.mvi6  
     if order(n) eq 7 then mvin=cmbmovie.mvi7  
     if order(n) eq 8 then mvin=cmbmovie.mvi8 
     if order(n) eq 9 then mvin=cmbmovie.mvi9  
     if order(n) eq 0 then imgsin=imgs0
     if order(n) eq 1 then imgsin=imgs1  
     if order(n) eq 2 then imgsin=imgs2  
     if order(n) eq 3 then imgsin=imgs3  
     if order(n) eq 4 then imgsin=imgs4  
     if order(n) eq 5 then imgsin=imgs5  
     if order(n) eq 6 then imgsin=imgs6  
     if order(n) eq 7 then imgsin=imgs7  
     if order(n) eq 8 then imgsin=imgs8 
     if order(n) eq 9 then imgsin=imgs9  
     if notsecchi gt 0 then aorb='' else aorb=strmid(mvin.hdrs(0).FILENAME,strpos(mvin.hdrs(0).FILENAME,'.')-1,1)
     if aorb eq 'B' then uxy=bxy else uxy=axy
     if aorb eq 'B' then xyoff=boff else xyoff=aoff
        
     if cmbstate.rtheta eq 0 then filltest=where(msktmp(*,*,n) eq 2 and outmask(*,*,n) eq 1) else filltest=[0]
     if (filltest(0) gt -1) then begin
;       if nm ne 0 then usefm1=usef(nm-1,n) else usefm1=-1
;       if usef(nm) ne usefm1 then begin
        if cmbmovie.mvihdrs.file_hdr(order(n)).truecolor eq 0 then  begin
           uimg = bytarr(cmbmovie.mvihdrs.file_hdr(order(n)).nx, cmbmovie.mvihdrs.file_hdr(order(n)).ny)
           if (cmbmovie.mvtype(order(n)) ne '0') THEN uimg(*,*)=get_hdr_movie_img(string(imgsin(usef(nm,n))),mviname=cmbmovie.mvis(order(n))) else uimg(*,*) = imgsin(usef(nm,n))
        endif else begin
           uimg = bytarr(3,cmbmovie.mvihdrs.file_hdr(order(n)).nx, cmbmovie.mvihdrs.file_hdr(order(n)).ny)
           if (cmbmovie.mvtype(order(n)) ne '0') THEN uimg(*,*,*)=get_hdr_movie_img(string(imgsin(usef(nm,n))),mviname=cmbmovie.mvis(order(n))) else uimg(*,*,*) = imgsin(usef(nm,n))
           uimg=Color_Quan(uimg, 1, redt, greent, bluet, COLORS=256)
           red(nc1:nc2) = redt
           green(nc1:nc2) = greent
           blue(nc1:nc2) = bluet
	endelse
        if cmbstate.rtheta eq 0 then begin
          timg=congrid(uimg,xisize(order(n)),yisize(order(n)))
          tmp(*,*)=0
          tmp(x1(order(n)):x2(order(n)),y1(order(n)):y2(order(n)))=timg

	  block=where(msktmp(*,*,n) ne 2 or outmask(*,*,n) lt 1)
          if mvin.hdrs(usef(nm,n)).filter eq 'Deproject' and mvin.hdrs(usef(nm,n)).detector eq 'HI1' and cmbstate.outer(n) eq 2 then begin
            device,get_decomposed=decom
            device,decompose=1
	    wset,wtmp
            erase,0
            contour,uimg,/device,xstyle=5,ystyle=5,levels=[1],position=[0,0,cmbmovie.mvihdrs.file_hdr(order(n)).NX,cmbmovie.mvihdrs.file_hdr(order(n)).NY],thick=1,/fill,color=2 
            mskimt=TVRD()
	    for nm1=0,cmbmovie.mvihdrs.file_hdr(order(n)).NY-1 do begin
              nmsk=where(mskimt(*,nm1) eq 2)
	      if nmsk(0) gt -1 then mskimt(min(nmsk):max(nmsk),nm1)=2
	    endfor
            mskim=congrid(mskimt,xisize(order(n)),yisize(order(n)))
            device,decompose=decom
            msktmp(*,*,n)=0
            msktmp(x1(order(n)):x2(order(n)),y1(order(n)):y2(order(n)),n)=mskim
          endif 

	  block=where(msktmp(*,*,n) ne 2 or outmask(*,*,n) lt 1)

	  tmp(block)=-1
          ztmp=where(tmp gt -1)
          if ztmp(0) ne -1 then begin
            if keyword_set(truecolor)then tmp(where(tmp ne -1))=tmp(where(tmp ne -1))+nc1
            tmp2(*,*)=0
            cimgt(*,*)=0
	    tmp2(uxy(0),uxy(2))=tmp(cmbstate.newx(0)+xyoff(0):cmbstate.newx(1)+xyoff(1),cmbstate.newy(0)+xyoff(2):cmbstate.newy(1)+xyoff(3))
            cimgt(uxy(0),uxy(2))=timgs(uxy(0):uxy(1),uxy(2):uxy(3))
            cimgt(where(tmp2 ne -1))=tmp2(where(tmp2 ne -1))
            timgs(uxy(0),uxy(2))=cimgt(uxy(0):uxy(1),uxy(2):uxy(3))
          endif else stop
       endif else begin
         if aorb eq 'B' then rtth=cmbstate.rtthb else rtth=cmbstate.rttha
         if aorb eq 'B' and cmbstate.bpos ne 0 then utmp=tmp2 else utmp=tmp
         xrng=[cmbmovie.mvihdrs.file_hdr(order(n)).theta0,cmbmovie.mvihdrs.file_hdr(order(n)).theta1]
	 xpts=xrng(0)+findgen(cmbmovie.mvihdrs.file_hdr(order(n)).nx)/(cmbmovie.mvihdrs.file_hdr(order(n)).nx-1.)*(xrng(1)-xrng(0)) 
	 yrng=[cmbmovie.mvihdrs.file_hdr(order(n)).radius0,cmbmovie.mvihdrs.file_hdr(order(n)).radius1]
	 if cmbmovie.mvihdrs.file_hdr(order(n)).rtheta eq 1 then $
	    ypts=yrng(0)+findgen(cmbmovie.mvihdrs.file_hdr(order(n)).ny)/(cmbmovie.mvihdrs.file_hdr(order(n)).ny-1.)*(yrng(1)-yrng(0)) else $ 
  	        ypts=10^(alog10(yrng(0))+findgen(cmbmovie.mvihdrs.file_hdr(order(n)).ny)/(cmbmovie.mvihdrs.file_hdr(order(n)).ny-1.)*(alog10(yrng(1))-alog10(yrng(0))))
           timg=scale_image_axis(uimg,xpts,ypts,xrange=rtth,yrange=cmbstate.rtra,outsize=msize,ylog=cmbstate.ylog)
;         timg=tvrd ()
         if timg(0) ne -1 then begin
            timg=long(timg)
            if keyword_set(truecolor)then timg(where(timg gt 0))=timg(where(timg gt 0))+nc1
            utmp(where(timg gt 0))=timg(where(timg gt 0))
            timgs(uxy(0):uxy(1),uxy(2):uxy(3))=utmp
            if aorb eq 'B' and cmbstate.bpos ne 0 then tmp2=utmp else tmp=utmp
         endif
       endelse
     endif
  endfor

     if cmbstate.rtheta gt 0 and cmbstate.rtaxis eq 1 then begin
        if nm eq 0 then begin        
          yticknames=REPLICATE(' ', 30)
          WINDOW, XSIZE = xysize(0), YSIZE = xysize(1), /PIXMAP, /FREE
          gridw=!d.window
          IF cmbstate.ylog EQ 1 THEN  nypix=10^(alog10(cmbstate.rtra(0))+0.02*(alog10(cmbstate.rtra(1))-alog10(cmbstate.rtra(0)))) ELSE nypix=cmbstate.rtra(0)+0.02*(cmbstate.rtra(1)-cmbstate.rtra(0))
          nxpix=cmbstate.rttha(0)+0.05*(cmbstate.rttha(1)-cmbstate.rttha(0))
          plot,[0,0],[0,0],xstyle=1,ystyle=1,xrange=cmbstate.rttha,yrange=cmbstate.rtra,position=[axy(0),axy(2),axy(1),axy(3)],ylog=cmbstate.ylog,color=255,ytickname=yticknames,/device
          xouts=indgen(max(cmbstate.rttha)/100)+1
          for nt=0,n_elements(xouts)-1 do xyouts,xouts(nt)*100,nypix,string(xouts(nt)*100,'(i3.3)'),color=255,align=.5
          youts=indgen(max(cmbstate.rtra)/10)+1
          for nt=0,n_elements(youts)-1 do xyouts,nxpix,youts(nt)*10,string(youts(nt)*10,'(i3.3)'),color=255,align=0
          if cmbstate.bpos ge 1 then begin
            nxpix=cmbstate.rtthb(0)+0.05*(cmbstate.rtthb(1)-cmbstate.rtthb(0))
            plot,[0,0],[0,0],xstyle=1,ystyle=1,xrange=cmbstate.rtthb,yrange=cmbstate.rtra,position=[bxy(0),bxy(2),bxy(1),bxy(3)],ylog=cmbstate.ylog,color=255,/noerase,ytickname=yticknames,/device
            xouts=indgen(max(cmbstate.rtthb)/100)+1
            for nt=0,n_elements(xouts)-1 do xyouts,xouts(nt)*100,nypix,string(xouts(nt)*100,'(i3.3)'),color=255,align=.5
            for nt=0,n_elements(youts)-1 do xyouts,nxpix,youts(nt)*10,string(youts(nt)*10,'(i2.2)'),color=255,align=0
          endif
          raxisg=tvrd()
          wdel,gridw
	endif
	timgs(where(raxisg eq 255))=255
     endif

     if keyword_set(times) or cmbstate.times gt 0 then begin
        timestr=sctext(0)
        for nt=0,n_Elements(ftimes(nm,*))-1 do timestr=timestr+ftimes(nm,nt)+'  '
        WINDOW, XSIZE = xysize(0), YSIZE = ytsize, /PIXMAP, /FREE
        xyouts,1,1,timestr,charsize=cmbstate.times,color=255,/device
        xyouts,xysize(0)-1,1,sctext(1),charsize=cmbstate.times,color=255,/device,alignment=1
        twindo=tvrd()
        timgs=[[twindo],[timgs]]
     endif
     IF (debug_on) THEN BEGIN
     	print,'SCTEXT=',sctext
     	wait,2
     ENDIF


     IF (debug_on) THEN BEGIN
     	print,'SCTEXT=',sctext
     	wait,2
     ENDIF
     if cmbstate.truv eq 0 then begin
           img=timgs 
           img(where(timgs eq -1))=cmbmovie.bkgrd(0)
     endif else begin
           red=[red,cmbmovie.bkgrd(0)]
           green=[green,cmbmovie.bkgrd(1)]
           blue=[blue,cmbmovie.bkgrd(2)]
           if cmbstate.rtheta eq 0 then timgs(where(timgs eq -1))=n_Elements(red)-1
           img=bytarr(3,xysize(0),mvihdr.NY)
           img(0,*,*)=red(timgs(*,*))
           img(1,*,*)=green(timgs(*,*))
           img(2,*,*)=blue(timgs(*,*))
     endelse

     if datatype(textframe) ne 'UND' then begin
       sze=size(textframe)
       if total(textframe) gt 0 then begin
         if cmbstate.truv eq 0 then begin
	   if sze(1) eq 2 then begin
             textimg=reform(textframe(1,*,*))
             textclr=reform(textframe(0,*,*))
	    img(where(textimg eq 255))=textclr(where(textimg eq 255))
           endif else print,'Can not use textframe it was created in truecolor mode'
         endif
	 if cmbstate.truv eq 1 then begin
	   if sze(1) eq 4 then begin
             textimg=reform(textframe(3,*,*))
             for nc=0,2 do begin
                bimg=reform(img(nc,*,*))
                textclr=reform(textframe(nc,*,*))
                bimg(where(textimg eq 255))=textclr(where(textimg eq 255))
                img(nc,*,*)=bimg
             endfor
           endif else print,'Can not use textframe it was created in grayscale mode'
         endif
       endif
     endif 

     WINDOW, XSIZE = xysize(0), YSIZE = mvihdr.NY, /PIXMAP, /FREE
     win_index(nm) = !D.WINDOW
     tv,img,true=cmbstate.truv

     if keyword_set(outmovie) then scc_save_mvframe,nm,outmovie.outfile, img, mviahdrs(nm),mvihdr,first=first,inline=nframes,SCALE=outmovie.SCALE, $
               REFERENCE=outmovie.REFERENCE , mframes=outmovie.mframes, idlencode=outmovie.idlencode,colorquan=outmovie.colorquan,_EXTRA=_extra
     IF (first EQ 1) THEN first = 0


     if keyword_set(defimgs) then begin
        if cmbstate.truv eq 0 then imgs(*,*,nm)=img else imgs(*,*,*,nm)=img 
     endif

endfor
if datatype(wtmp) eq 'UND' then wtmp=-1
if wtmp ne -1 then wdel,wtmp
for nl=0,9 do begin
  if lus(nl) ne -1 then begin
    close, lus(nl)
    free_lun,lus(nl)  
  endif
endfor

undefine,outmask
undefine,cimgt
undefine,msktmp
undefine,tmp
undefine,timgs
undefine,simg


  print,leftxmax,leftymax


if ~keyword_set(save_only) then scc_playmovie,win_index,hdrs=mviahdrs,SUNXCEN=mvihdr.sunxcen, SUNYCEN=mvihdr.sunycen, SEC_PIX=mvihdr.sec_pix,$
    TRUECOLOR=TRUECOLOR,mvicombo=[leftxmax,leftymax,cmbstate.times*10],ccdpos=mvihdr.ccd_pos, DEBUG=debug_on

;endif

END

PRO draw_combined_crop
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

COMMON scc_playmv_COMMON, moviev
 
common SCC_COMBINE, cmbmovie,cmbstate,struct ;needs to match definition in wscc_combine_mvi.pro

      moviev.cmbvals(1)=moviev.cropsz(2)-fix(moviev.cropsz(0)/2)
      moviev.cmbvals(3)=moviev.cropsz(2)+fix(moviev.cropsz(0)/2)
      moviev.cmbvals(2)=moviev.cropsz(3)-fix(moviev.cropsz(1)/2)
      moviev.cmbvals(4)=moviev.cropsz(3)+fix(moviev.cropsz(1)/2)

      moviev.cmbvals(8)=moviev.cropsz(4)-moviev.cmbvals(6)-moviev.cropsz(2)
      moviev.cmbvals(9)=moviev.cropsz(5)-moviev.cropsz(3)

      WSET, moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
      DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1), moviev.cmbvals(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1), moviev.cmbvals(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(1), moviev.cmbvals(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(3), moviev.cmbvals(2), 11] ;right side
      if moviev.hsize gt moviev.cmbvals(6)+1 then begin
         DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(2)+moviev.cmbvals(9), 11] ;bottom line
         DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(4)+moviev.cmbvals(9), 11] ;top line
         DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(1)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(2)+moviev.cmbvals(9), 11] ;leftside 
         DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(3)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(2)+moviev.cmbvals(9), 11] ;right side
       endif
END

PRO SCC_CMBMOVIE_DRAW, ev
COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine

   IF (ev.press GT 0) THEN RETURN 		;** only look at PRESS events

   ;;WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE


 IF (TAG_EXIST(moviev, 'cmbvals')) THEN BEGIN
     if (moviev.cmbvals(0) eq 3) then begin ; center crop windows
       if ev.x le moviev.cmbvals(6) then begin   ;left side
	 moviev.cropsz(2)=ev.x
         WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cropsz(2)
	 moviev.cropsz(3)=ev.y
         WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cropsz(3)
       endif
       if ev.x gt moviev.cmbvals(6) then begin ; center right image
	  moviev.cropsz(4)=ev.x
          WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cropsz(4)
	  moviev.cropsz(5)=ev.y
          WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cropsz(5)
       endif
       draw_combined_crop
   endif

   if (moviev.cmbvals(0) eq 2) then begin ;second corner selected
      if ev.x gt moviev.cmbvals(6) then evx=moviev.cmbvals(6) else evx=ev.x
      if ev.y gt moviev.cmbvals(7) then evy=moviev.cmbvals(7) else evy=ev.y
      moviev.cmbvals(3)=evx
      moviev.cmbvals(4)=evy
      moviev.cmbvals(0)=3 ;set to 3 when rxycen get defined.
      if moviev.cmbvals(1) gt moviev.cmbvals(3) then begin
          tmp=moviev.cmbvals(3) & moviev.cmbvals(3) = moviev.cmbvals(1) & moviev.cmbvals(1)=tmp
      endif
      if moviev.cmbvals(2) gt moviev.cmbvals(4) then begin
          tmp=moviev.cmbvals(4) & moviev.cmbvals(4) = moviev.cmbvals(2) & moviev.cmbvals(2)=tmp
      endif
      moviev.cropsz(0)=moviev.cmbvals(3)-moviev.cmbvals(1)+1
      WIDGET_CONTROL,moviev.cmbfield(0),SET_VALUE=moviev.cropsz(0)
      moviev.cropsz(1)=moviev.cmbvals(4)-moviev.cmbvals(2)+1
      WIDGET_CONTROL,moviev.cmbfield(1),SET_VALUE=moviev.cropsz(1)

      if moviev.cropsz(2) le 0 then begin
	moviev.cropsz(2)=moviev.cmbvals(6)/2 -1
        WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cropsz(2)
      endif
      if moviev.cropsz(3) le 0 then begin
	moviev.cropsz(3)=moviev.vsize/2 -1
        WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cropsz(3)
      endif
      if cmbstate.bpos gt 0 then begin
	if moviev.cropsz(4) le 0 then begin
	  moviev.cropsz(4)=moviev.cmbvals(6)+ moviev.cmbvals(6)/2 +1
          WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cropsz(4)
        endif
        if moviev.cropsz(5) le 0 then begin
	  moviev.cropsz(5)=moviev.vsize/2 -1
          WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cropsz(5)
        endif
      endif
      draw_combined_crop
      insttxt='click on image to recenter crop position'
      widget_control,moviev.cropinst,set_value=insttxt
   endif

   if (moviev.cmbvals(0) eq 1) then begin ; first corner selected
      if ev.x gt moviev.cmbvals(6) then evx=moviev.cmbvals(6) else evx=ev.x
      if ev.y gt moviev.cmbvals(7) then evy=moviev.cmbvals(7) else evy=ev.y
      moviev.cmbvals(1)=evx
      moviev.cmbvals(2)=evy
      moviev.cmbvals(0)=2
      widget_control,moviev.cropinst,set_value='Select upper right corner'
   endif
endif

  ;WIDGET_CONTROL, base, SET_UVALUE=moviev

END

PRO SCC_CMBMOVIE_CONTROL_EVENT, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine

;WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

    IF (TAG_EXIST(ev, 'value')) THEN  IF (DATATYPE(ev.value) EQ 'STR') THEN input=ev.value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
    IF (DATATYPE(input) EQ 'STR') THEN BEGIN

      CASE (input) OF

          'mcor2av':cmbstate.outer(2)=ev.value
          'mcor1av':cmbstate.outer(3)=ev.value
          'meuviav':cmbstate.outer(4)=ev.value
          'meuvibv':cmbstate.outer(5)=ev.value
          'mcor1bv':cmbstate.outer(6)=ev.value
          'mcor2bv':cmbstate.outer(7)=ev.value
          'mhi2av':cmbstate.outer(0)=ev.value
          'mhi2bv':cmbstate.outer(9)=ev.value
          'mhi1v': begin
	       cmbstate.outer(1)=ev.index
	       cmbstate.outer(8)=ev.index
           END
          'mdetv':begin
	       dx=widget_info(ev.id,/uname)
               cmbstate.outer(where(cmbstate.detectors eq dx))=ev.value
           end
;          'mc3sv':cmbstate.outer(10)=ev.value
;          'mc2sv':cmbstate.outer(11)=ev.value
;          'meitsv':cmbstate.outer(12)=ev.value

          'mcharsv':moviev.cmbvals(11)=ev.value*10
	  'cmbzoomsel' : BEGIN	;select zoom coords using any mouse
                 WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                 moviev.cmbvals(5)=float(string(val,'(f6.2)'))
                 WIDGET_CONTROL, moviev.cmbfield(6), SET_VALUE=moviev.cmbvals(5)
           END
    	'cmszoomt': BEGIN	;select zoom coords using any mouse
                 moviev.cmbvals(5)=ev.value
                 WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
           END


        'mcolor' : moviev.truecolor= ev.index
        'bpos'  : cmbstate.bpos = ev.index
        'brclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, moviev.brclr, GET_VALUE=val
                cmbmovie.bkgrd(0)=val 
          END
        'bgclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, moviev.bgclr, GET_VALUE=val
                cmbmovie.bkgrd(1)=val
          END
        'bbclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, moviev.bbclr, GET_VALUE=val
                cmbmovie.bkgrd(2)=val
          END
       'bkselect'  :  BEGIN; if moviev.true eq 1 then moviev.bkgrd=reform(moviev.bkgval(*,ev.value)) else moviev.bkgrd=moviev.bkgval(ev.value)
                        if moviev.truecolor eq 1 then begin
                              if ev.index ne 0 then begin
			        cmbmovie.bkgrd=reform(moviev.bkgval(*,ev.index))
                                WIDGET_CONTROL, moviev.brclr, SET_VALUE=cmbmovie.bkgrd(0)
                                WIDGET_CONTROL, moviev.bgclr, SET_VALUE=cmbmovie.bkgrd(1)
                                WIDGET_CONTROL, moviev.bbclr, SET_VALUE=cmbmovie.bkgrd(2)
                              endif
                              if ev.index eq 0 then begin
			        WIDGET_CONTROL, moviev.brclr, GET_VALUE=val
				cmbmovie.bkgrd(0)=val
                                WIDGET_CONTROL, moviev.bgclr, GET_VALUE=val
				cmbmovie.bkgrd(1)=val
                                WIDGET_CONTROL, moviev.bbclr, GET_VALUE=val
				cmbmovie.bkgrd(2)=val
                              endif
			endif else begin
			      if ev.index ne 0 then cmbmovie.bkgrd(0)=moviev.bkgval(ev.index)
                              WIDGET_CONTROL, moviev.brclr, SET_VALUE=cmbmovie.bkgrd(0)
                              if ev.index eq 0 then begin
                                WIDGET_CONTROL, moviev.brclr, GET_VALUE=val
				cmbmovie.bkgrd(0)=val
                              endif
                        endelse
                 END

	'xcrop' : BEGIN	;select zoom coords using any mouse
                      moviev.cropsz(0)=ev.value
		      if moviev.cropsz(1) gt 0 then begin
                          if moviev.cropsz(2) le 0 then begin
			     moviev.cropsz(2)=moviev.cmbvals(6)/2 -1
                             WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cropsz(2)
                          endif
                          if moviev.cropsz(3) le 0 then begin
			     moviev.cropsz(3)=moviev.hsize/2 -1
                             WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cropsz(3)
                          endif
                          if cmbstate.bpos gt 0 then begin
			     if moviev.cropsz(4) le 0 then begin
			       moviev.cropsz(4)=moviev.cmbvals(6)+ moviev.cmbvals(6)/2 +1
                               WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cropsz(4)
                            endif
                            if moviev.cropsz(5) le 0 then begin
			       moviev.cropsz(5)=moviev.hsize/2 -1
                               WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cropsz(5)
                            endif
                          endif
                         WIDGET_CONTROL, moviev.winbase, TIMER=10000.   ; Put timer on hold
			 moviev.stall = 10000.
                         moviev.cmbvals(0)=3
                         inst='use mouse to recenter crop'
                         draw_combined_crop
                      endif else inst='Enter Ysize'
                      widget_control,moviev.cropinst,set_value=inst
	    END

	'ycrop' : BEGIN	;select zoom coords using any mouse
                      moviev.cropsz(1)=ev.value
		      if moviev.cropsz(0) gt 0 and moviev.cropsz(1) gt 0 then begin
                          if moviev.cropsz(2) le 0 then begin
			     moviev.cropsz(2)=moviev.cmbvals(6)/2 -1
                             WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cropsz(2)
                          endif
                          if moviev.cropsz(3) le 0 then begin
			     moviev.cropsz(3)=moviev.vsize/2 -1
                             WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cropsz(3)
                          endif
                          if cmbstate.bpos gt 0 then begin
			     if moviev.cropsz(4) le 0 then begin
			       moviev.cropsz(4)=moviev.cmbvals(6)+ moviev.cmbvals(6)/2 +1
                               WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cropsz(4)
                            endif
                            if moviev.cropsz(5) le 0 then begin
			       moviev.cropsz(5)=moviev.vsize/2 -1
                               WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cropsz(5)
                            endif
                          endif
                         WIDGET_CONTROL, moviev.winbase, TIMER=10000.   ; Put timer on hold
			 moviev.stall = 10000.
                         moviev.cmbvals(0)=3
                         inst='use mouse to recenter crop'
                         draw_combined_crop
                      endif else inst='enter Xsize'
                      widget_control,moviev.cropinst,set_value=inst
	    END
	'xcent' : BEGIN	;select zoom coords using any mouse
                      moviev.cropsz(2)=ev.value
		      if moviev.cropsz(1) gt 0 and moviev.cropsz(0) gt 0 then begin
                         WIDGET_CONTROL, moviev.winbase, TIMER=10000.   ; Put timer on hold
			 moviev.stall = 10000.
                         moviev.cmbvals(0)=3
                         inst='use mouse to recenter crop'
                         draw_combined_crop
                      endif else begin
		         inst='Select Crop size'
                         moviev.cmbvals(0)=1
                      endelse
                      widget_control,moviev.cropinst,set_value=inst
	    END
	'ycent' : BEGIN	;select zoom coords using any mouse
                      moviev.cropsz(3)=ev.value
		      if moviev.cropsz(1) gt 0 and moviev.cropsz(0) gt 0 then begin
                         WIDGET_CONTROL, moviev.winbase, TIMER=10000.   ; Put timer on hold
			 moviev.stall = 10000.
                         moviev.cmbvals(0)=3
                         inst='use mouse to recenter crop'
                         draw_combined_crop
                      endif else begin
		         inst='Select Crop size'
                         moviev.cmbvals(0)=1
                      endelse
                      widget_control,moviev.cropinst,set_value=inst
	    END

	'xcentr' : BEGIN	;select zoom coords using any mouse
                      moviev.cropsz(4)=ev.value
		      if moviev.cropsz(1) gt 0 and moviev.cropsz(0) gt 0 then begin
                         WIDGET_CONTROL, moviev.winbase, TIMER=10000.   ; Put timer on hold
			 moviev.stall = 10000.
                         moviev.cmbvals(0)=3
                         inst='use mouse to recenter crop'
                         draw_combined_crop
                      endif else begin
		         inst='Select Crop size'
                         moviev.cmbvals(0)=1
                      endelse
                      widget_control,moviev.cropinst,set_value=inst
	    END
	'ycentr' : BEGIN	;select zoom coords using any mouse
                      moviev.cropsz(5)=ev.value
		      if moviev.cropsz(1) gt 0 and moviev.cropsz(0) gt 0 then begin
                         WIDGET_CONTROL, moviev.winbase, TIMER=10000.   ; Put timer on hold
			 moviev.stall = 10000.
                         moviev.cmbvals(0)=3
                         inst='use mouse to recenter crop'
                         draw_combined_crop
                      endif else begin
		         inst='Select Crop size'
                         moviev.cmbvals(0)=1
                      endelse
                      widget_control,moviev.cropinst,set_value=inst
	    END

	'rtxsize' : cmbstate.rtsize(0)=ev.value
	'rtysize' : cmbstate.rtsize(1)=ev.value
	'rtth0a'  : cmbstate.rttha(0)=ev.value
	'rtth1a'  : cmbstate.rttha(1)=ev.value
	'rtth0b'  : cmbstate.rtthb(0)=ev.value
	'rtth1b'  : cmbstate.rtthb(1)=ev.value
	'rtra0'   : cmbstate.rtra(0)=ev.value
	'rtra1'   : cmbstate.rtra(1)=ev.value
	'ylogv'   : cmbstate.ylog= ev.index
	'raxisv'   : cmbstate.rtaxis= ev.index

	ELSE : BEGIN
                 PRINT, '%%SCC_CMBMOVIE_CONTROL.  Unknown event.'
	     END

    ENDCASE

  ENDIF

 ; WIDGET_CONTROL, moviev.base, SET_UVALUE=moviev

;  RETURN, 0

END 

FUNCTION SCC_CMBMOVIE_CONTROL_FUNC, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine


   input = ev.value

   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   if input ne '' then begin
    CASE (input) OF


	'cmbcrop' : BEGIN	;select crop coords using any mouse
                      WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                      moviev.stall = 10000.
		      moviev.cmbvals(0)=1 ;crop selected
                      if moviev.hsize gt moviev.cmbvals(6)+1 then inst='select lower right corner on left image' else inst='select lower left corner'
                      widget_control,moviev.cropinst,set_value=inst
	    END

                     
	'cmbcropcen' : BEGIN	;select crop coords using any mouse
                      if mvoiev.cropsz(0) gt 0 and mvoiev.cropsz(1) gt 0 then begin
                        WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                        moviev.stall = 10000.
		        moviev.cmbvals(0)=3 ;crop selected
                        inst='use mouse to select center'
		      endif else inst='crop size not set'
                      widget_control,moviev.cropinst,set_value=inst
	    END

	'cmbcropcenr' : BEGIN	;select crop coords using any mouse
                      if mvoiev.cropsz(0) gt 0 and mvoiev.cropsz(1) gt 0 then begin
                        WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                        moviev.stall = 10000.
		        moviev.cmbvals(0)=3 ;crop selected
                        inst='use mouse to select center'
		      endif else inst='crop size not set'
                      widget_control,moviev.cropinst,set_value=inst
	    END

	'cmbcancel' : BEGIN
		      moviev.cmbvals(0)=0 ;crop unselected
	    	      moviev.cmbvals(0:5)=0
	    	      moviev.cmbvals(8:9)=0
                      z=where(moviev.cropsz gt 0)
                      moviev.cropsz(*)=0
                      for n=0,n_Elements(z)-1 do WIDGET_CONTROL,moviev.cmbfield(z(n)),SET_VALUE=moviev.cropsz(z(n))
                      WSET, moviev.draw_win
                      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                      widget_control,moviev.cropinst,set_value='Crop is not selected'
	    END
	'cmbreset' : BEGIN	;select zoom coords using any mouse
	    	    moviev.cmbvals(0:5)=0
	    	    moviev.cmbvals(8:9)=0
	    	    moviev.cmbvals(10)=1
	    END

         'CMBPREVIEW' : BEGIN
                        if moviev.cmbvals(5) gt 0 then begin
                           WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                           if moviev.cmbvals(5) ne val then begin
                              moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			      WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                           endif
			   cmbscl=moviev.cmbvals(5) 
		        endif else cmbscl=0
		    if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                    if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                    if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                    moviev.cmbvals(*)=0
                    truecolor=moviev.truecolor
                    exit_playmovie
 		    SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=truecolor,/preview,restore=restore,times=times
                    RETURN, 0
	    END
	 
         'CMBMKMOVIE' : BEGIN
                        if moviev.cmbvals(5) gt 0 then begin
                           WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                           if moviev.cmbvals(5) ne val then begin
                              moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			      WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                           endif
			   cmbscl=moviev.cmbvals(5) 
		        endif else cmbscl=0
                    if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                    if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                    if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                    moviev.cmbvals(*)=0
                    truecolor=moviev.truecolor
                    exit_playmovie
		    SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=truecolor,restore=restore,times=times
                    RETURN, 0
	    END
         'CMBSVMOVIE' : BEGIN
	 
                      if moviev.cmbvals(5) gt 0 then begin
                           WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                           if moviev.cmbvals(5) ne val then begin
                              moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			      WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                           endif
			   cmbscl=moviev.cmbvals(5) 
		        endif else cmbscl=0
                    ofn=moviev.filename
                    ofp=moviev.filepath
                    outmovie=''
                    scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=ofn,len=moviev.len,first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect,/selectonly,outmovie=outmovie;scc_movieout,moviev.base ,/selectonly
		    IF (outmovie.outfile ne moviev.filename and outmovie.outfile ne '') THEN BEGIN
                        moviev.filename=outmovie.outfile
                        BREAK_FILE, outmovie.outfile, a, dir1, name, ext
		        spawn,['mkdir',dir1],/NOSHELL
                        if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                        if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                        if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                        moviev.cmbvals(*)=0 & truecolor=moviev.truecolor
                        exit_playmovie
		        SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=truecolor,restore=restore,times=times,outmovie=outmovie
                        RETURN, 0
                     ENDIF else begin
		         print,'******************************************'
		         print,'*****  Movie filename not selected   *****'
		         print,'******************************************'
                    ENDELSE
	    END
         'CMBWIMGS' : BEGIN
	 
                   if moviev.cmbvals(5) gt 0 then begin
                        WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                        if moviev.cmbvals(5) ne val then begin
                           moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			   WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                        endif
			cmbscl=moviev.cmbvals(5) 
		    endif else cmbscl=0
                    if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                    if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                    if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                    moviev.cmbvals(*)=0
                    truecolor=moviev.truecolor
		    exit_playmovie
		    SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=truecolor,restore=restore,times=times,/defimgs
                    RETURN, 0
	    END
         'CMBSETUP' : BEGIN
                    exit_playmovie
		    WSCC_COMBINE_MVI
                    RETURN, 0
	    END

         'CMBADDTEXT' : BEGIN
                     annotate_image,moviev.win_index,/template,true=moviev.truecolor
	    END
	'CMBCONTROL' : BEGIN	;show cmbcontrol window
              IF (moviev.showcmb EQ 0) THEN BEGIN
                 WIDGET_CONTROL, moviev.cmbbase, MAP=1
                 moviev.showcmb = 1
              ENDIF ELSE BEGIN
                WIDGET_CONTROL, moviev.cmbbase, MAP=0
                moviev.showcmb = 0
              ENDELSE
	    END

        'CMBCLRTEXT' : BEGIN
                     undefine,textframe
	    END

	ELSE : BEGIN
                 PRINT, '%%SCC_CMBMOVIE_CONTROL.  Unknown event.'
	     END

    ENDCASE

  ENDIF


;  RETURN, 0

END 

PRO SCC_CMBMOVIE,mvicombo

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
COMMON scc_combine
   cmbbase = WIDGET_BASE(/COLUMN, XOFFSET=moviev.basexoff(1), YOFFSET=moviev.baseyoff(1), TITLE='SCC_COMBINE_MVI Control',EVENT_PRO='SCC_CMBMOVIE_CONTROL_EVENT')

              cmbsliden=0
              cropinst=0	
	      cmbvals=moviev.cmbvals; 0=zoom select 1=x1 2=x2 3=y1 4=y2 5=scale factor 6=max(crop x) 7=max(crop y) 8=rxcen 9=rycen 10 restore 11 times size
              bkgcolor=0	; background for annotation text
              WINDOW, 11, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='SCC_COMBINE_MVI zoom position'
              erase,bkgcolor  ; this is the color behind annotations on the image from the cursor position
              cmbvals(6)=mvicombo(0)
              cmbvals(7)=mvicombo(1)
              cmbvals(11)=mvicombo(2)
              cmbbase4a = WIDGET_BASE(cmbbase, /ROW, /Frame)

              cropsz=intarr(6) +0
              cmbfield=intarr(8)
              if cmbstate.rtheta eq 0 then begin
                cmbbase4ba = WIDGET_BASE(cmbbase4a, /colum,/frame,title='Crop')
                tmp=widget_label(cmbbase4ba,  value='Crop')
  
                cmbbase4b2 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(0)=CW_FIELD(cmbbase4b2, value=string(cropsz(0),'(i5)'), uvalue="xcrop", $
	      	    title='Xsize',/all_events,xsize=5 )
                cmbfield(1)=CW_FIELD(cmbbase4b2, value=string(cropsz(1),'(i5)'), uvalue="ycrop", $
	      	    title='Ysize',/all_events,xsize=5 )
                cmbbase4b3a = WIDGET_BASE(cmbbase4ba, /row)
                tmp=widget_label(cmbbase4ba,  value='Center Positions')

                cmbbase4b3 = WIDGET_BASE(cmbbase4ba, /row)
                if cmbstate.bpos gt 0 then xt='X (L)' else xt='X Cen'
                cmbfield(2)=CW_FIELD(cmbbase4b3, value=string(cropsz(2),'(i5)'), uvalue="xcent", $
	      	    title=xt,/all_events,xsize=5 )
                if cmbstate.bpos gt 0 then yt='Y (L)' else yt='Y Cen'
                cmbfield(3)=CW_FIELD(cmbbase4b3, value=string(cropsz(3),'(i5)'), uvalue="ycent", $
	      	    title=yt,/all_events,xsize=5 )

                if cmbstate.bpos gt 0 then begin
                 cmbbase4b4 = WIDGET_BASE(cmbbase4ba, /row)
                  cmbfield(4)=CW_FIELD(cmbbase4b4, value=string(cropsz(4),'(i5)'), uvalue="xcentr", $
	      	    title='X (R)',/all_events,xsize=5 )
                  cmbfield(5)=CW_FIELD(cmbbase4b4, value=string(cropsz(5),'(i5)'), uvalue="ycentr", $
	      	    title='Y (R)',/all_events,xsize=5 )
                endif
                cmbbase4b5 = WIDGET_BASE(cmbbase4ba, /row)
                cropinst=widget_text(cmbbase4b5,value='Not selected',uvalue='cropinst',font='-1',XSIZE=30)
                cmbbase4b6 = WIDGET_BASE(cmbbase4ba, /row)
  	        tmp = CW_BGROUP(cmbbase4b6,  ['Select w/Mouse','Cancel Crop'], /ROW, $
	        BUTTON_UVALUE = ['cmbcrop','cmbcancel'], EVENT_FUNCT='SCC_CMBMOVIE_CONTROL_FUNC')

               cmbbase4c = WIDGET_BASE(cmbbase4a, /colum,/frame)
              	  tmp=widget_label(cmbbase4c,  value='Masks')
    	    	  tmp=widget_label(cmbbase4c,  value=' 0 = No Mask')
    	    	  if cmbstate.notsecchi eq 0 then tmp=widget_label(cmbbase4c,  value='-1 = Use Mask File') 
                if cmbstate.notsecchi eq 0 then begin
                  cmbbase4e = WIDGET_BASE(cmbbase4c, /row)
                    mhi2av=CW_FIELD(cmbbase4e, value=cmbstate.outer(0), uvalue="mhi2av",title='HI2_A',/all_events,xsize=5,/floating)
                    mhi2bv=CW_FIELD(cmbbase4e, value=cmbstate.outer(9), uvalue="mhi2bv",title='HI2_B',/all_events,xsize=5,/floating)
                  cmbbase4f = WIDGET_BASE(cmbbase4c, /row)
    	    	    mcor2av=CW_FIELD(cmbbase4f, value=cmbstate.outer(2), uvalue="mcor2av",title='COR2A',/all_events,xsize=5,/floating)
    	    	    mcor2bv=CW_FIELD(cmbbase4f, value=cmbstate.outer(7), uvalue="mcor2bv",title='COR2B',/all_events,xsize=5,/floating)
                  cmbbase4g = WIDGET_BASE(cmbbase4c, /row)
                    mcor1av=CW_FIELD(cmbbase4g, value=cmbstate.outer(3), uvalue="mcor1av",title='COR1A',/all_events,xsize=5,/floating)
    	    	    mcor1bv=CW_FIELD(cmbbase4g, value=cmbstate.outer(6), uvalue="mcor1bv",title='COR1B',/all_events,xsize=5,/floating)
                  cmbbase4h = WIDGET_BASE(cmbbase4c, /row)
                    meuviav=CW_FIELD(cmbbase4h, value=cmbstate.outer(4), uvalue="meuviav",title='EUVIA',/all_events,xsize=5,/floating)
                    meuvibv=CW_FIELD(cmbbase4h, value=cmbstate.outer(5), uvalue="meuvibv",title='EUVIB',/all_events,xsize=5,/floating)
		  hdtest=where(cmbmovie.proj eq 'Deproject' and strpos(cmbmovie.tels,'h1') gt -1)
                  if hdtest(0) gt - 1 then begin
                     cmbbase4i = WIDGET_BASE(cmbbase4c, /row)
                     mhi1v =   widget_droplist(cmbbase4i, VALUE=['None','First Frame','All Frames'], UVALUE='mhi1v',title='HI1 Mask :')
                     widget_control,mhi1v,set_droplist_select=cmbstate.outer(1)
                  endif
                endif else begin
                  for nd =0,n_elements(cmbstate.detectors)-1 do begin
                    cmbbase4e = WIDGET_BASE(cmbbase4c, /row)
                    mc3sv=CW_FIELD(cmbbase4e, value=cmbstate.outer(nd), uname=cmbstate.detectors(nd), uvalue="mdetv",title=cmbstate.detectors(nd),/all_events,xsize=5,/floating)
                  endfor
		  ;cmbbase4f = WIDGET_BASE(cmbbase4c, /row)
    	    	  ;  mc2sv=CW_FIELD(cmbbase4f, value=cmbstate.outer(11), uvalue="mc2sv",title='C2',/all_events,xsize=5,/floating)
		  ;diskname=cmbstate.detectors(n_Elements(cmbstate.detectors)-1)
                  ;cmbbase4g = WIDGET_BASE(cmbbase4c, /row)
                  ;  meitsv=CW_FIELD(cmbbase4g, value=cmbstate.outer(12), uvalue="meitsv",title=diskname,/all_events,xsize=5,/floating)
                endelse
              endif else begin
                cmbbase4ba = WIDGET_BASE(cmbbase4a, /colum,/frame,title='Position/Crop')
                tmp=widget_label(cmbbase4ba,  value='Position')
                cmbbase4b2 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield=intarr(8)
                cmbfield(0)=CW_FIELD(cmbbase4b2, value=string(cmbstate.rtsize(0),'(i5)'), uvalue="rtxsize", $
	      	    title='Xsize',/all_events,xsize=5 )
                cmbfield(1)=CW_FIELD(cmbbase4b2, value=string(cmbstate.rtsize(1),'(i5)'), uvalue="rtysize", $
	      	    title='Ysize',/all_events,xsize=5 )
                cmbbase4b3a = WIDGET_BASE(cmbbase4ba, /row)
                 tmp=widget_label(cmbbase4ba,  value='radius (0-88) | theta (0-360) Same sun center uses A only')
                cmbbase4b3 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(2)=CW_FIELD(cmbbase4b3, value=string(cmbstate.rttha(0),'(i5)'), uvalue="rtth0a", $
	      	    title='A theta 0',/all_events,xsize=5 )
                cmbfield(3)=CW_FIELD(cmbbase4b3, value=string(cmbstate.rttha(1),'(i5)'), uvalue="rtth1a", $
	      	    title='A theta 1',/all_events,xsize=5 )
                cmbbase4b4 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(2)=CW_FIELD(cmbbase4b4, value=string(cmbstate.rtthb(0),'(i5)'), uvalue="rtth0b", $
	      	    title='B theta 0',/all_events,xsize=5 )
                cmbfield(3)=CW_FIELD(cmbbase4b4, value=string(cmbstate.rtthb(1),'(i5)'), uvalue="rtth1b", $
	      	    title='B theta 1',/all_events,xsize=5 )
                cmbbase4b5 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(4)=CW_FIELD(cmbbase4b5, value=string(cmbstate.rtra(0),'(f6.2)'), uvalue="rtra0", $
	      	    title='A/B Radius 0',/all_events,xsize=5 )
                cmbfield(5)=CW_FIELD(cmbbase4b5, value=string(cmbstate.rtra(1),'(f6.2)'), uvalue="rtra1", $
	      	    title='A/B Radius 1',/all_events,xsize=5)
                 cmbbase4b6 = WIDGET_BASE(cmbbase4ba,/row)
                     ylogt=['Linear','Log']
                     ylogv=widget_droplist(cmbbase4b6, VALUE=ylogt, UVALUE='ylogv',title='Radius Scale :')
                     widget_control,ylogv,set_droplist_select=cmbstate.ylog
                     raxis=['off','on']
                     ylogv=widget_droplist(cmbbase4b6, VALUE=raxis, UVALUE='raxisv',title='Plot Axis :')
                     widget_control,ylogv,set_droplist_select=cmbstate.rtaxis
	      endelse

              bkgrow = WIDGET_BASE(cmbbase4a,/colum)
              lab2='Background:'
              f4sz=140

              if cmbstate.notsecchi eq 0 then begin
                     fnt3bp = WIDGET_BASE(bkgrow,/row,/frame)
                     bpost=['Same sun center','Left side','Right side']
                     bposv=widget_droplist(fnt3bp, VALUE=bpost, UVALUE='bpos',title='B position :')
                     widget_control,bposv,set_droplist_select=cmbstate.bpos
              endif

                     fnt3ba = WIDGET_BASE(bkgrow,/row,/frame)
                     tru=['Grayscale','Truecolor']
                     mvtru =   widget_droplist(fnt3ba, VALUE=tru, UVALUE='mcolor',title='Movie Color:')
                     widget_control,mvtru,set_droplist_select=moviev.truecolor
 
	       if moviev.truecolor eq 0 then begin
                     bkgarr=['Select','White','Median','Black']
                     bkgval=[-2,255,127,0]
                     ;bkgrd=bkgval(0)
                     fnt3bb = WIDGET_BASE(bkgrow,/colum,/frame,title='Background Color')
                     mvtxtc =   widget_droplist(fnt3bb, VALUE=bkgarr, UVALUE='bkselect',title=lab2)
                     fnt3bbr1 = WIDGET_BASE(fnt3bb,/row)
                     mvtxt=widget_label (fnt3bbr1,  value='Val:')
                     brclr = WIDGET_SLIDER(fnt3bbr1,  VALUE=cmbmovie.bkgrd(0), UVALUE='brclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
              endif else begin
                    bkgarr=[  'Select',      'Black',    'White',      'Red',        'Blue',       'Green',     'yellow']
                    bkgval=[[-2,-2,-2],[000,000,000],[255,255,255],[255,000,000],[000,000,255],[000,255,000],[229,229,056]]
                    ;bkgrd=reform(bkgval(*,0))
                    fnt3bb = WIDGET_BASE(bkgrow,/colum,/frame,title='Background Color')
                    mvtxtc =   widget_droplist(fnt3bb, VALUE=bkgarr, UVALUE='bkselect',title=lab2)
                    fnt3bbr1 = WIDGET_BASE(fnt3bb,/row)
                    mvtxt=widget_label (fnt3bbr1,  value='R:')
                    brclr = WIDGET_SLIDER(fnt3bbr1,  VALUE=cmbmovie.bkgrd(0), UVALUE='brclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
                    fnt3bbr2 = WIDGET_BASE(fnt3bb,/row)
                    mvtxt=widget_label (fnt3bbr2,  value='G:')
                    bgclr = WIDGET_SLIDER(fnt3bbr2,  VALUE=cmbmovie.bkgrd(1), UVALUE='bgclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='G:')
                    fnt3bbr3 = WIDGET_BASE(fnt3bb,/row)
                    mvtxt=widget_label (fnt3bbr3,  value='B:')
                    bbclr = WIDGET_SLIDER(fnt3bbr3,  VALUE=cmbmovie.bkgrd(2), UVALUE='bbclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='B:')
                    moviev.bgclr=bgclr 
                    moviev.bbclr=bbclr 
              endelse



              cmbbase4b2a = WIDGET_BASE(cmbbase, /row)
              slz=where(cmbstate.sec_pixs gt 0)

              if cmbstate.rtheta eq 0 then begin
                cmbbase4b2c = WIDGET_BASE(cmbbase4b2a, /colum,/frame)
                cmbbase4b2z = WIDGET_BASE(cmbbase4b2c, /row)
    	        cmbfield(6) = CW_FIELD(cmbbase4b2z, VALUE=string(max(cmbstate.sec_pixs(slz))/cmbstate.scale,'(f6.2)'), XSIZE=10, YSIZE=1, UVALUE='cmszoomt', /ALL_EVENTS, TITLE='Zoom: ',/floating)
                cmbsliden = CW_FSLIDER (cmbbase4b2z, TITLE='Select platescale value (arcsec/pixel)', VALUE=(max(cmbstate.sec_pixs(slz))/cmbstate.scale), UVALUE='cmbzoomsel', $
                   /DRAG, MIN=(min(cmbstate.sec_pixs(slz)))-1, MAX=(max(cmbstate.sec_pixs(slz))/cmbstate.origscale)+1,/SUPPRESS_VALUE,scroll=0.01,double=0)
                widget_control, cmbsliden, set_val=(max(cmbstate.sec_pixs(slz))/cmbstate.scale)
              endif 

              cmbbase4bca = WIDGET_BASE(cmbbase4b2a, /colum,/frame)
;              cmbbase4b3a = WIDGET_BASE(cmbbase, /row)
              cmbfield(7)=CW_FIELD(cmbbase4bca, value=string(cmbvals(11)/10.,'(f3.1)'), uvalue="mcharsv", $
	      	    title='Timestamp Charsize (0=none)',/all_events,xsize=3,/floating)

 
 	      tmp = CW_BGROUP(cmbbase,   ['Restore Original Size/undo crop ','Return to Setup ','Clear Text'], /ROW, $
	      BUTTON_UVALUE = ['cmbreset','CMBSETUP','CMBCLRTEXT'], EVENT_FUNCT='SCC_CMBMOVIE_CONTROL_FUNC')

	      tmp = CW_BGROUP(cmbbase,  ['Preview','Make Movie w/o img array','Make/Save Movie ',' Make img array','CLOSE'], /ROW, $
	      BUTTON_UVALUE = ['CMBPREVIEW','CMBMKMOVIE','CMBSVMOVIE','CMBWIMGS','CMBCONTROL'], EVENT_FUNCT='SCC_CMBMOVIE_CONTROL_FUNC')

              tmp=widget_label(cmbbase, /frame ,$
	           value='Estimated MVI size = '+string(round(cmbstate.estmvisiz/(1024.*1024.)),'(i5)')+$
	          'MB  PNG or GIF movie size = '+string(round((cmbstate.estmvisiz/(1024.*1024.))/(1.0+cmbstate.truv*2.0)),'(i5)')+'MB')
             

;
    	    moviev.cmbbase=cmbbase
    	    moviev.cmbsliden=cmbsliden
    	    moviev.cmbvals=cmbvals
    	    moviev.cropinst=cropinst             
            moviev.showcmb=1
;            moviev.bkgval=bkgval
            moviev.brclr=brclr

           moviev=add_tag(moviev,bkgval,'bkgval')         
           moviev=add_tag(moviev,cmbfield,'cmbfield')
           moviev=add_tag(moviev,cropsz,'cropsz')

	 WIDGET_CONTROL, cmbbase, MAP=1 
         WIDGET_CONTROL, /REAL, cmbbase

END



PRO WSCC_COMBINE_MVI_EVENT, event

  common SCC_COMBINE
  COMMON WSCC_MKMOVIE_COMMON

  WIDGET_CONTROL, event.id, GET_UVALUE=uval
  ;WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE (values
                                                  ; are not retained. use common).

  CASE (uval) OF

    'QUIT': BEGIN
              WIDGET_CONTROL, /DESTROY, struct.base
            END
    'PROCEED': BEGIN
              WIDGET_CONTROL, /DESTROY, struct.base
              mvis=[struct.mvi0,struct.mvi1,struct.mvi2,struct.mvi3,struct.mvi4,struct.mvi5,struct.mvi6,struct.mvi7,struct.mvi8,struct.mvi9]
              mvt=where(mvis ne '')
	      if mvt(0) ne -1 then begin
	        mvis=mvis(mvt)
                get_scc_combine_mvi,mvis
	        scc_combine_mvi,truecolor=struct.mcolor,bpos=struct.bpos,times=struct.times,/preview
              return
              endif else print,'No movies selected exiting'
              return
            END
    'CLEAR': BEGIN
             struct.mvi0= '' 
             WIDGET_CONTROL,struct.MV0, SET_VALUE= struct.mvi0
             struct.mvi1= '' 
             WIDGET_CONTROL,struct.MV1, SET_VALUE= struct.mvi1
             struct.mvi2= '' 
             WIDGET_CONTROL,struct.MV2, SET_VALUE= struct.mvi2
             struct.mvi3= '' 
             WIDGET_CONTROL,struct.MV3, SET_VALUE= struct.mvi3
             struct.mvi4= '' 
             WIDGET_CONTROL,struct.MV4, SET_VALUE= struct.mvi4
             struct.mvi5= '' 
             WIDGET_CONTROL,struct.MV5, SET_VALUE= struct.mvi5
             struct.mvi6= '' 
             WIDGET_CONTROL,struct.MV6, SET_VALUE= struct.mvi6
             struct.mvi7= '' 
             WIDGET_CONTROL,struct.MV7, SET_VALUE= struct.mvi7
             struct.mvi8= '' 
             WIDGET_CONTROL,struct.MV8, SET_VALUE= struct.mvi8
             struct.mvi9= '' 
             WIDGET_CONTROL,struct.MV9, SET_VALUE= struct.mvi9
            END
    'mcolor' : struct.mcolor= event.index
    'bpos' : struct.bpos= event.index

    'times' : struct.times= event.index

    'MV0': BEGIN
             WIDGET_CONTROL,struct.MV0, GET_VALUE= mname
             struct.mvi0= STRTRIM(mname,2) 
          END
    'MV1': BEGIN
             WIDGET_CONTROL,struct.MV1, GET_VALUE= mname
             struct.mvi1= STRTRIM(mname,2) 
          END
    'MV2': BEGIN
             WIDGET_CONTROL,struct.MV2, GET_VALUE= mname
             struct.mvi2= STRTRIM(mname,2) 
          END
    'MV3': BEGIN
             WIDGET_CONTROL,struct.MV3, GET_VALUE= mname
             struct.mvi3= STRTRIM(mname,2) 
          END

    'MV4': BEGIN
             WIDGET_CONTROL,struct.MV4, GET_VALUE= mname
             struct.mvi4= STRTRIM(mname,2) 
          END
    'MV5': BEGIN
             WIDGET_CONTROL,struct.MV5, GET_VALUE= mname
             struct.mvi5= STRTRIM(mname,2) 
          END
    'MV6': BEGIN
             WIDGET_CONTROL,struct.MV6, GET_VALUE= mname
             struct.mvi6= STRTRIM(mname,2) 
          END
    'MV7': BEGIN
             WIDGET_CONTROL,struct.MV7, GET_VALUE= mname
             struct.mvi7= STRTRIM(mname,2) 
          END
    'MV8': BEGIN
             WIDGET_CONTROL,struct.MV8, GET_VALUE= mname
             struct.mvi8= STRTRIM(mname,2) 
          END

    'MV9': BEGIN
             WIDGET_CONTROL,struct.MV9, GET_VALUE= mname
             struct.mvi9= STRTRIM(mname,2) 
          END


    'smv0': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi0= file 
             WIDGET_CONTROL,struct.MV0, SET_VALUE= struct.mvi0
          END
    'smv1': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi1= file 
             WIDGET_CONTROL,struct.MV1, SET_VALUE= struct.mvi1
           END
    'smv2': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi2= file 
             WIDGET_CONTROL,struct.MV2, SET_VALUE= struct.mvi2
           END
    'smv3': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi3= file 
             WIDGET_CONTROL,struct.MV3, SET_VALUE= struct.mvi3
           END
    'smv4': BEGIN
             file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi4= file 
             WIDGET_CONTROL,struct.MV4, SET_VALUE= struct.mvi4
          END
    'smv5': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi5= file 
             WIDGET_CONTROL,struct.MV5, SET_VALUE= struct.mvi5
          END
    'smv6': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi6= file 
             WIDGET_CONTROL,struct.MV6, SET_VALUE= struct.mvi6
           END
    'smv7': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi7= file 
             WIDGET_CONTROL,struct.MV7, SET_VALUE= struct.mvi7
           END
    'smv8': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi8= file 
             WIDGET_CONTROL,struct.MV8, SET_VALUE= struct.mvi8
           END
    'smv9': BEGIN
             file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi9= file 
             WIDGET_CONTROL,struct.MV9, SET_VALUE= struct.mvi9
          END

  ENDCASE
  if datatype(path) ne 'UND' then struct.mvipath = path
 
  RETURN
END


PRO WSCC_COMBINE_MVI,mvifiles,TRUECOLOR=truecolor, BSIDE=bside, AUTO=auto, DEBUG=debug, _EXTRA=_extra
  common SCC_COMBINE
  COMMON WSCC_MKMOVIE_COMMON
;  undefine,textframe
undefine,cmbstate
IF keyword_set(DEBUG) THEN debug_on=1 ELSE debug_on=0
CD, CURRENT = mvipath

           mvi0=''
           mvi1=''
           mvi2=''
           mvi3=''
           mvi4=''
           mvi5=''
           mvi6=''
           mvi7=''
           mvi8=''
           mvi9=''
           if keyword_set(truecolor) then mcolor=1 else mcolor=0
           times=1
           bpos=0

  if datatype(mvifiles) ne 'UND' then begin
           if (n_elements(mvifiles) ge 1) then mvi0=mvifiles(0)
           if (n_elements(mvifiles) ge 2) then mvi1=mvifiles(1)
           if (n_elements(mvifiles) ge 3) then mvi2=mvifiles(2)
           if (n_elements(mvifiles) ge 4) then mvi3=mvifiles(3)
           if (n_elements(mvifiles) ge 5) then mvi4=mvifiles(4)
           if (n_elements(mvifiles) ge 6) then mvi5=mvifiles(5)
           if (n_elements(mvifiles) ge 7) then mvi6=mvifiles(6)
           if (n_elements(mvifiles) ge 8) then mvi7=mvifiles(7)
           if (n_elements(mvifiles) ge 9) then mvi8=mvifiles(8)
           if (n_elements(mvifiles) ge 10) then mvi9=mvifiles(9)
           if keyword_set(truecolor) then mcolor=1 else mcolor=0
  endif
  if datatype(struct) eq 'STC' and datatype(mvifiles) eq 'UND' then begin
           mvi0=struct.mvi0
           mvi1=struct.mvi1
           mvi2=struct.mvi2
           mvi3=struct.mvi3
           mvi4=struct.mvi4
           mvi5=struct.mvi5
           mvi6=struct.mvi6
           mvi7=struct.mvi7
           mvi8=struct.mvi8
           mvi9=struct.mvi9
           mcolor=struct.mcolor
           bpos=struct.bpos
           times=struct.times
  endif 

  base= WIDGET_BASE(title='SECCHI Combine Movie Tool',/COLUMN,/FRAME, XOFFSET= 1, YOFFSET=0)

  c3= WIDGET_BASE(base,/colum)
  mlab1= WIDGET_LABEL(c3,VALUE='Movies to combine')

  r0=WIDGET_BASE(c3,/row)
  mv0= CW_FIELD(r0, VALUE=MV0, /ALL_EVENTS, UVALUE='MV0' , XSIZE=70, YSIZE=1, title='MVI0 : ')
  smv0= WIDGET_BUTTON(r0, VALUE='Select', UVALUE='smv0') 
  WIDGET_CONTROL,mv0, SET_VALUE= mvi0

  mv5= CW_FIELD(r0, VALUE=MV0, /ALL_EVENTS, UVALUE='MV5' , XSIZE=70, YSIZE=1, title='MVI5 : ')
  smv5= WIDGET_BUTTON(r0, VALUE='Select', UVALUE='smv5') 
  WIDGET_CONTROL,mv5, SET_VALUE= mvi5

  r1=WIDGET_BASE(c3,/row)
  mv1= CW_FIELD(r1, VALUE=MV1, /ALL_EVENTS, UVALUE='MV1' , XSIZE=70, YSIZE=1, title='MVI1 : ')
  smv1= WIDGET_BUTTON(r1, VALUE='Select', UVALUE='smv1') 
  WIDGET_CONTROL,mv1, SET_VALUE= mvi1
  mv6= CW_FIELD(r1, VALUE=MV0, /ALL_EVENTS, UVALUE='MV6' , XSIZE=70, YSIZE=1, title='MVI6 : ')
  smv6= WIDGET_BUTTON(r1, VALUE='Select', UVALUE='smv6') 
  WIDGET_CONTROL,mv6, SET_VALUE= mvi6

  r2=WIDGET_BASE(c3,/row)
  mv2= CW_FIELD(r2, VALUE=MV2, /ALL_EVENTS, UVALUE='MV2' , XSIZE=70, YSIZE=1, title='MVI2 : ') 
  smv2= WIDGET_BUTTON(r2, VALUE='Select', UVALUE='smv2') 
  WIDGET_CONTROL,mv2, SET_VALUE= mvi2
  mv7= CW_FIELD(r2, VALUE=MV0, /ALL_EVENTS, UVALUE='MV7' , XSIZE=70, YSIZE=1, title='MVI7 : ')
  smv7= WIDGET_BUTTON(r2, VALUE='Select', UVALUE='smv7') 
  WIDGET_CONTROL,mv7, SET_VALUE= mvi7

  r3=WIDGET_BASE(c3,/row)
  mv3= CW_FIELD(r3, VALUE=MV3, /ALL_EVENTS, UVALUE='MV3' , XSIZE=70, YSIZE=1, title='MVI3 : ') 
  smv3= WIDGET_BUTTON(r3, VALUE='Select', UVALUE='smv3') 
  WIDGET_CONTROL,mv3, SET_VALUE= mvi3
  mv8= CW_FIELD(r3, VALUE=MV0, /ALL_EVENTS, UVALUE='MV8' , XSIZE=70, YSIZE=1, title='MVI8 : ')
  smv8= WIDGET_BUTTON(r3, VALUE='Select', UVALUE='smv8') 
  WIDGET_CONTROL,mv8, SET_VALUE= mvi8

  r4=WIDGET_BASE(c3,/row)
  mv4= CW_FIELD(r4, VALUE=MV4, /ALL_EVENTS, UVALUE='MV4' , XSIZE=70, YSIZE=1, title='MVI4 : ') 
  smv4= WIDGET_BUTTON(r4, VALUE='Select', UVALUE='smv4')
  WIDGET_CONTROL,mv4, SET_VALUE= mvi4
  mv9= CW_FIELD(r4, VALUE=MV0, /ALL_EVENTS, UVALUE='MV9' , XSIZE=70, YSIZE=1, title='MVI9 : ')
  smv9= WIDGET_BUTTON(r4, VALUE='Select', UVALUE='smv9') 
  WIDGET_CONTROL,mv9, SET_VALUE= mvi9

  r5=WIDGET_BASE(c3,/row)
 proceed= WIDGET_BUTTON(r5, VALUE='Proceed', UVALUE='PROCEED')
 clear= WIDGET_BUTTON(r5, VALUE='clear', UVALUE='CLEAR')
 quit= WIDGET_BUTTON(r5, VALUE='Quit', UVALUE='QUIT')

  txt=widget_label (r5,  value='Color :')
  tru=['Grayscale','Truecolor']
  truv= WIDGET_COMBOBOX(r5, VALUE=tru, UVALUE="mcolor")
  WIDGET_CONTROL, truv, SET_COMBOBOX_SELECT= mcolor
  WIDGET_CONTROL, truv, SENSITIVE=1

  txt=widget_label (r5,  value='B position :')
  bpost=['Same sun center','Left side','Right side']
  bposv= WIDGET_COMBOBOX(r5, VALUE=bpost, UVALUE="bpos")
  IF keyword_set(BSIDE) THEN bpos=bside-1
  WIDGET_CONTROL, bposv, SET_COMBOBOX_SELECT= bpos
  WIDGET_CONTROL, bposv, SENSITIVE=1

  txt=widget_label (r5,  value='Time Stamps: ')
  timet=['off','on']
  timev= WIDGET_COMBOBOX(r5, VALUE=timet, UVALUE="times")
  WIDGET_CONTROL, timev, SET_COMBOBOX_SELECT= times
  WIDGET_CONTROL, timev, SENSITIVE=1

  WIDGET_CONTROL, base, /REALIZE

  struct= {base: base,             $
           quit: quit,             $
           proceed: proceed,       $
           clear: clear,       $
           truv: truv,       $
           mcolor: mcolor,       $
           bposv: bposv,       $
           bpos: bpos,       $
           timev: timev,       $
           times: times,       $
           mvi0: mvi0,       $
           mvi1: mvi1,       $
           mvi2: mvi2,       $
           mvi3: mvi3,       $
           mvi4: mvi4,       $
           smv0: smv0,       $
           smv1: smv1,       $
           smv2: smv2,       $
           smv3: smv3,       $
           smv4: smv4,       $
           mv0: mv0,       $
           mv1: mv1,       $
           mv2: mv2,       $
           mv3: mv3,       $
           mv4: mv4,	   $
           mvi5: mvi5,       $
           mvi6: mvi6,       $
           mvi7: mvi7,       $
           mvi8: mvi8,       $
           mvi9: mvi9,       $
           smv5: smv5,       $
           smv6: smv6,       $
           smv7: smv7,       $
           smv8: smv8,       $
           smv9: smv9,       $
           mv5: mv5,       $
           mv6: mv6,       $
           mv7: mv7,       $
           mv8: mv8,       $
           mv9: mv9,       $
           mvipath:mvipath }

   IF KEYWORD_SET(AUTO) THEN BEGIN
    	WIDGET_CONTROL, /DESTROY, struct.base
    	mvis=[struct.mvi0,struct.mvi1,struct.mvi2,struct.mvi3,struct.mvi4,struct.mvi5,struct.mvi6,struct.mvi7,struct.mvi8,struct.mvi9]
    	mvt=where(mvis ne '')
    	if mvt[0] ne -1 then begin
	        mvis=mvis(mvt)
                get_scc_combine_mvi,mvis
		help,bpos
	        scc_combine_mvi,truecolor=struct.mcolor,bpos=struct.bpos,times=struct.times, _EXTRA=_extra ;,/preview
    	endif
   ENDIF ELSE BEGIN

    	WIDGET_CONTROL, base, SET_UVALUE= struct 

    	XMANAGER, 'WSCC_COMBINE_MVI', base
	
   ENDELSE
 RETURN
END

