;SCC_FINDFILE.PRO -- find a file from a SECCHI filename without preceding directory
;+
; $Id: scc_findfile.pro,v 1.3 2008/03/17 12:37:27 mcnutt Exp $
;
; Name        : scc_findfile
;
; Purpose     : find a file from a SECCHI filename without preceding directory.
;               
; Explanation : 
;               
; Use         : scc_findfile, filename (without path)
;    
; Inputs      : filename (20070201_141540_s4euA.fts)
;               
; Outputs     : path for fits file
;               
; Keywords    : /TEL, /SIDE  not used determind from file name
;
; Calls       : 
;
; Side effects: 
;               
; Category    : Image Display calibration inout
;
; $Log: scc_findfile.pro,v $
; Revision 1.3  2008/03/17 12:37:27  mcnutt
; added basic header info for wiki page
;
; Revision 1.2  2008/01/11 16:33:31  nathan
; Permanently moved from dev/movie to secchi/idl/display/movie
;

FUNCTION scc_findfile, filename, TEL=tel, SIDE=side

t = STRMID(filename, 18,2)
fname = getenv('secchi') + '/lz/L0'
IF(STRMID(filename, 20,1) EQ 'A') THEN side = 'a' ELSE side = 'b'
fname = fname + '/' + side + '/img/'
CASE t OF
	'c1': tel = 'cor1'
	'c2': tel = 'cor2'
	'eu': tel = 'euvi'
	'h1': tel = 'hi_1'
	'h2': tel = 'hi_2'
	ELSE: BEGIN
		print, '%%SCC_FINDFILE: Could not find source FITS file'
		fname = ''
	END
ENDCASE
IF(fname EQ '') THEN RETURN, fname

fname = fname + tel + '/' + STRMID(filename, 0,8) + '/' + filename

RETURN, fname

END
