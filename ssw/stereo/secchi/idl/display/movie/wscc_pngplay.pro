;+
; $Id: wscc_pngplay.pro,v 1.35 2016/02/17 18:56:02 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : WSCC_PNGPLAY
;               
; Purpose   : Widget wrapper for scc_pngplay
;               
; Explanation: 
;               
; Use       : IDL> wscc_pngplay
;
; Examples:   IDL> wscc_pngplay
;    
; Inputs    :
;            
; Outputs   : None
;
; Keywords  : /SPWX 	Use images from stereo-ssc.nascom.nasa.gov.
;
; Calls from LASCO :
;
; Common    : 
;               
; Restrictions: Requires definition of $SECCHI_PNG or $SECCHI_JPG
;               
; Side effects: 
;               
; Category    : DISPLAY
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Sep 2011
;               
; $Log: wscc_pngplay.pro,v $
; Revision 1.35  2016/02/17 18:56:02  mcnutt
; set truecolor =0  if displaying one telescope
;
; Revision 1.34  2013/07/05 18:11:49  mcnutt
; corrected SECCHI web dirs and added HMI option
;
; Revision 1.33  2012/12/28 17:57:43  mcnutt
; added inital size of wavelet pngs
;
; Revision 1.32  2012/10/01 20:38:06  nathan
; document /spwx
;
; Revision 1.31  2012/08/29 14:11:54  mcnutt
; added wcs_combine bpos options
;
; Revision 1.30  2012/08/08 17:07:46  mcnutt
; added mmpgname to titles movie generic_movie call
;
; Revision 1.29  2012/08/03 14:55:13  mcnutt
; corrected combined movie axis titles
;
; Revision 1.28  2012/08/03 14:46:45  mcnutt
; corrected xsize for combined movies
;
; Revision 1.27  2012/08/02 12:41:08  mcnutt
; added running difference option one telescope only
;
; Revision 1.26  2012/07/30 16:53:58  mcnutt
; adjust pan to first image size
;
; Revision 1.25  2012/07/19 14:58:12  mcnutt
; changed if statement to if then when calling generic movie to avoid returning before other condition are check
;
; Revision 1.24  2012/07/17 18:11:30  mcnutt
; corrected error if exited before scc_playmovie
;
; Revision 1.23  2012/07/17 16:42:02  mcnutt
; added AIA option to becon selection, defaults current day for becon images
;
; Revision 1.22  2012/07/16 21:58:03  nathan
; tweak some labels
;
; Revision 1.21  2012/07/16 16:34:43  mcnutt
; corrected fit to screen and colums interface
;
; Revision 1.20  2012/07/12 18:31:58  mcnutt
; added becon image option with spwx keyword
;
; Revision 1.19  2012/04/30 17:28:26  mcnutt
; defaults to cor2 doubles t= total brightness and * = both
;
; Revision 1.18  2012/04/03 17:56:32  mcnutt
; added lasco rdiff options
;
; Revision 1.17  2012/01/30 19:01:12  mcnutt
; added COR2 event options
;
; Revision 1.16  2012/01/23 20:48:15  mcnutt
; will not crash if no images are found
;
; Revision 1.15  2011/11/07 13:15:08  mcnutt
; reset x and y value if movie type changes
;
; Revision 1.14  2011/11/03 15:10:14  mcnutt
; added frametimes to generic_movie call to set frame hdr times
;
; Revision 1.13  2011/11/03 14:16:03  mcnutt
; added date_obs to hdr in scc_movieout call
;
; Revision 1.12  2011/11/01 12:31:16  mcnutt
; uses scc_save_mvframe when saving movie
;
; Revision 1.11  2011/10/07 19:14:17  nathan
; set imagesz for wavelets to 2048
;
; Revision 1.10  2011/10/06 18:19:01  mcnutt
; corrected tlist error
;
; Revision 1.9  2011/10/06 17:21:36  mcnutt
; sets filename to 0 if movie is not being saved
;
; Revision 1.8  2011/10/03 14:41:59  mcnutt
; added time stamp option
;
; Revision 1.7  2011/09/29 18:52:53  mcnutt
; added save movie option and coord limits for combined/polar movies
;
; Revision 1.6  2011/09/28 18:48:58  nathan
; fix how pan and fit-to-screen works; change default date input
;
; Revision 1.5  2011/09/26 18:30:14  mcnutt
; added latplane movie grrid and measuring options
;
; Revision 1.4  2011/09/20 20:26:28  nathan
; Modified mimgsz, mmvtp, pngvar.d2; added masks to pngvar, but not using it
;
; Revision 1.3  2011/09/19 18:29:06  mcnutt
; added hdr info and corrected use of pan
;
;
pro read_pngvals
COMMON scc_png_COMMON,pngvar,imglist,cadencemins,times, wevent
   WIDGET_CONTROL,pngvar.day1 , GET_VALUE=tmp
   pngvar.d1 = tmp
   WIDGET_CONTROL,pngvar.day2 , GET_VALUE=tmp
   pngvar.d2 = tmp
   if pngvar.x0v gt 0 then begin
     WIDGET_CONTROL,pngvar.x0v , GET_VALUE=tmp
     pngvar.x0 = tmp
     WIDGET_CONTROL,pngvar.x1v , GET_VALUE=tmp
     pngvar.x1 = tmp
     WIDGET_CONTROL,pngvar.y0v , GET_VALUE=tmp
     pngvar.y0 = tmp
     WIDGET_CONTROL,pngvar.y1v , GET_VALUE=tmp
     pngvar.y1 = tmp
    endif

   ;WIDGET_CONTROL,pngvar.bminv , GET_VALUE=tmp
   ;pngvar.bmin =  float(tmp)
   ;WIDGET_CONTROL,pngvar.bmaxv , GET_VALUE=tmp
   ;pngvar.bmax =  float(tmp)
   WIDGET_CONTROL,pngvar.winbase, TLB_GET_OFFSET=tmp
   pngvar.offset=tmp
   WIDGET_CONTROL,pngvar.panv , GET_VALUE=tmp
   pngvar.frxsize=fix(tmp[0])
   pan=float(pngvar.frxsize)/(pngvar.colums*pngvar.imagesz)
   pngvar.pan=pan
   WIDGET_CONTROL,pngvar.timsz , GET_VALUE=tmp
   pngvar.tsize=tmp[0]
;   WIDGET_CONTROL,pngvar.fnamev , GET_VALUE=tmp
;   pngvar.mpegname = float(tmp)

end
pro events_control_event,ev
COMMON scc_png_COMMON
    WIDGET_CONTROL, ev.id, GET_UVALUE=input
    IF (TAG_EXIST(ev, 'value')) THEN  IF (DATATYPE(ev.value) EQ 'STR') THEN input=ev.value

     CASE (input) OF

       'EVSEL': BEGIN       ;** exit program
                pngvar.d1=utc2str(tai2utc(utc2tai(anytim2utc(strmid(wevent.events(ev.index),0,19)))-(60l*60*4)))
                pngvar.d2=utc2str(tai2utc(utc2tai(anytim2utc(strmid(wevent.events(ev.index),0,19)))+(60l*60*20)))
                WIDGET_CONTROL,pngvar.day1 , SET_VALUE=pngvar.d1
                WIDGET_CONTROL,pngvar.day2 , SET_VALUE=pngvar.d2
                 pngvar.colums=2  
                 pngvar.rows=1  
		 pngvar.rdiff=0
                 read_pngvals
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 wscc_pngplay,spwx=pngvar.spwx
		 pngvar.tels(0)='COR2b'
		 pngvar.tels(1)='COR2a'
                 widget_control,pngvar.rct(0),set_value=pngvar.tels(0)
                 widget_control,pngvar.rct(1),set_value=pngvar.tels(1)
                RETURN
	     END
       'UPDATE': BEGIN       ;** exit program
                 widget_control,wevent.evfile,get_value=eventfile
	         WIDGET_CONTROL, /DESTROY, wevent.eventbase
                 undefine,wevent
                 events=get_events(outfile=eventfile,/update)
                 select_event
                 RETURN
	     END
       'READEVF': BEGIN       ;** exit program
                 widget_control,wevent.evfile,get_value=eventfile
	         WIDGET_CONTROL, /DESTROY, wevent.eventbase
                 undefine,wevent
                 select_event,eventfile=eventfile
                 RETURN
	     END
       'GETEVENTS': BEGIN       ;** exit program
                 WIDGET_CONTROL, wevent.syear, get_value=yr
                 syr=widget_info(wevent.syear, /DROPLIST_SELECT)
	         WIDGET_CONTROL, /DESTROY, wevent.eventbase
                 undefine,wevent
                 events=get_events(yr(syr))
                 select_event,events,syr=syr,eventfile='From Database'
                 RETURN
	     END
       'evfile': BEGIN  
                 widget_control,wevent.evfile,get_value=tmp
                 wevent.eventfile = tmp
                 RETURN
	     END

       'EXIT': BEGIN       ;** exit program
	         WIDGET_CONTROL, /DESTROY, wevent.eventbase
                 undefine,wevent
                 RETURN
	     END

       ELSE : BEGIN   
                 RETURN
	     END
    ENDCASE


end

pro select_event,events,syr=syr,eventfile=eventfile
COMMON scc_png_COMMON
   if datatype(events) eq 'UND' then begin
      if ~keyword_set(eventfile) then eventfile='~/secchi/data/logs/Event_Triggers.txt'
      events=readlist(eventfile)
   endif
   if strpos(events(0), 'First') gt -1 and n_elements(events) gt 1 then begin
     elabel=events(0)
     events=events(2:n_elements(events)-1)
   endif else elabel='First Trigger       | Last Trigger        | A | B |'
   events=reverse(events)
   offset=[400,200]
   title='EVENTS'
    eventbase = WIDGET_BASE(TITLE=title,/COLUMN,EVENT_PRO='EVENTS_CONTROL_EVENT',xoffset=offset(0),yoffset=offset(1))
    wincol = WIDGET_BASE(eventbase ,/column,/frame)
    winrow = WIDGET_BASE(wincol ,/row)
    	tmp = WIDGET_LABEL(winrow,VALUE=elabel)
    winrow = WIDGET_BASE(wincol ,/row)
    eventlist= WIDGET_LIST(winrow,value=events,UVALUE='EVSEL',scr_xsize=350,scr_ysize=400,/frame)
    winrow = WIDGET_BASE(eventbase ,/row)
    years=reverse(string(indgen(strmid(utc2str(tai2utc(utc2tai(anytim2utc('')))),0,4)-2007+1) +2007,'(i4)'))
    tmp = CW_BGROUP(winrow,  ['Get From Database Events For: '], /ROW, $
	      BUTTON_UVALUE = ['GETEVENTS'])
    syear =   widget_droplist(winrow, VALUE=years, UVALUE='SYEAR')
    if keyword_set(syr) then widget_control,syear,SET_DROPLIST_SELECT=syr
    winrow = WIDGET_BASE(eventbase ,/row)
    evfile = WIDGET_TEXT(winrow, VALUE=eventfile, XSIZE=40, YSIZE=1, UVALUE='evfile', /EDITABLE)
    winrow = WIDGET_BASE(eventbase ,/row)
    tmp = CW_BGROUP(winrow,  ['Update Event File','Read Event File','EXIT'], /ROW, $
	      BUTTON_UVALUE = ['UPDATE','READEVF','EXIT'])
 
   wevent={eventbase:eventbase , eventlist:eventlist, events:events, eventfile:eventfile, syear:syear, evfile:evfile }
   WIDGET_CONTROL, /REAL, eventbase
   XMANAGER, 'WSCC_PNGPLAY', eventbase, EVENT_HANDLER='EVENTS_CONTROL_EVENT', /NO_BLOCK


end

pro mkscc_pngplay_movie,mkmovieval
COMMON scc_png_COMMON

  exit_playmovie

  read_pngvals
  if strpos(pngvar.d2,'YYYY') gt -1 or strlen(trim(pngvar.d2)) LT 2 then pngvar.d2 = pngvar.d1
  if strpos(pngvar.d1,'YYYY') gt -1 then begin
      print,'day1 must be set '+pngvar.d1
      return
  endif
  if pngvar.mvtype gt 0 then tels=pngvar.tels(where(pngvar.tels ne '     ?     ') )  else tels=pngvar.tels
  tmp=where(tels eq '     ?     ',cnt)
  if cnt gt 0 then begin
    print,'for Tiled movies all selection must be filled'
    return
  endif
  if datatype(imglist) eq 'UND' or mkmovieval eq 'test' then begin
     imglist=1
     times=1
     scc_pngplay,pngvar.d1,pngvar.d2,tels,cadence=pngvar.cadence,imgsize=pngvar.IMAGESZ,pnglist=imglist,pngtime=times,spwx=pngvar.spwx
     if mkmovieval eq 'test' and imglist(0) ne 'NoImages' then begin
       if pngvar.mvtype gt 0 then begin
	tmp=[0,n_elements(imglist(*,0))/2,n_elements(imglist(*,0))-1]
        tlist=imglist(tmp,*) & ttimes= times (tmp)     
       endif else begin
	tmp=[0,n_elements(imglist(*,0,0))/2,n_elements(imglist(*,0,0))-1]
        tlist=imglist(tmp,*,*) & ttimes= times (tmp)           
       endelse 
     endif else begin
       ttimes= times 
       tlist=imglist
       undefine,imglist
     endelse      
  endif else begin
     ttimes= times 
     tlist=imglist
     undefine,imglist
  endelse      

  if tlist(0) ne 'NoImages' then begin 
  if mkmovieval eq 'write' then begin
    ofn=''
    ofp=''
    thdr={filename:'', detector:'ssuff',date_obs:ttimes(0)}
    moviev={filename:ofp}
    outmovie=''
    scc_movieout,0,'',hdrs=thdr,filename=ofn,len=n_elements(tlist(*,0,0)),first=0, $
    	    last=n_elements(tlist(*,0,0))-1, deleted=intarr(n_elements(tlist(*,0,0)))+0, rect=0, /selectonly ,outmovie=outmovie;scc_movieout,moviev.base ,/selectonly
    IF (outmovie.outfile ne moviev.filename and outmovie.outfile ne '') THEN BEGIN
	moviev.filename=outmovie.outfile
        BREAK_FILE, ofn, a, dir1, name, ext
	spawn,['mkdir',dir1],/NOSHELL
    ENDIF else begin
	print,'******************************************'
	print,'*****  Movie filename not selected   *****'
	print,'******************************************'
	RETURN
    ENDELSE
  endif

  if tlist(0) ne '' then begin
    labels=0 & lcolor=0 & tsize=0
    frametimes=ttimes 
    if pngvar.tsize gt 0 then begin
      labels=frametimes
      lcolor=pngvar.clruse
      tsize=pngvar.tsize
    endif
  
    if datatype(outmovie) eq 'STC' then begin 
        mpegname=outmovie.outfile
	SCALE=outmovie.SCALE 
	REFERENCE=outmovie.REFERENCE
	mframes=outmovie.mframes
        idlencode=outmovie.idlencode
	colorquan=outmovie.colorquan
    endif else begin
        mpegname=0
    	SCALE=0
	REFERENCE=0
	mframes=0
        idlencode=0
	colorquan=0
    endelse

    if pngvar.mvtype eq 0 then begin $
    ;always set pan with the first image size compared to total outsize .
       if strpos(tlist(0),'/512/') gt -1 then imgsz=512.
       if strpos(tlist(0),'_512_') gt -1 then imgsz=512.
       if strpos(tlist(0),'_1024_') gt -1 then imgsz=1024.
       if strpos(tlist(0),'/1024/') gt -1 then imgsz=1024.
       if strpos(tlist(0),'/256/') gt -1 then imgsz=256.
       if strpos(tlist(0),'_256_') gt -1 then imgsz=256.
       if strpos(tlist(0),'/2048/') gt -1 then imgsz=2048.
       if strpos(tlist(0),'_2048_') gt -1 then imgsz=2048.
       if strpos(tlist(0),'/wavelets/') gt -1 then imgsz=2048.
       if strpos(tlist(0),'/wavelets/') gt -1 then imgsz=2048.
       pngvar.pan=(pngvar.frxsize/pngvar.colums)/imgsz
       lsize=size(tlist)
       if lsize[0] ge 1 then pngvar.true=0
       if pngvar.true eq 0 then grayscale=1 else grayscale=0
       generic_movie,tlist,pngvar.bmin,pngvar.bmax,rdiff=pngvar.rdiff,truecolor=pngvar.true, grayscale=grayscale,mpegname=mpegname,$
	  pan=pngvar.pan ,half=pngvar.half,_EXTRA=_extra, usemask=maskvals,labels=labels,times=tsize,lcolor=lcolor,frametimes=frametimes
        return
    endif
    if pngvar.mvtype gt 0 then begin
        if pngvar.x0 ne 0 or pngvar.x1 ne 0 then xr=[pngvar.x0,pngvar.x1]
        if pngvar.y0 ne 0 or pngvar.y1 ne 0 then yr=[pngvar.y0,pngvar.y1]
        abmovie=pngvar.bpos
	if pngvar.bpos eq 1 then abmovie='A'
	if pngvar.bpos eq 2 then abmovie='B'
	generic_movie,tlist,pngvar.bmin,pngvar.bmax,truecolor=pngvar.true,/wcscombine,hsize=pngvar.frxsize,$
	    rtheta=pngvar.mvtype-1,_EXTRA=_extra, usemask=maskvals,xminmax=xr,yminmax=yr,labels=labels,times=tsize,lcolor=lcolor,$
	    mpegname=mpegname,SCALE=SCALE, REFERENCE=REFERENCE , mframes=mframes,idlencode=idlencode,colorquan=colorquan,frametimes=frametimes,abmovie=abmovie
        return
    endif
  endif
  endif

end



PRO WSCC_PNGPLAY_CONTROL_EVENT, ev

COMMON scc_png_COMMON

    WIDGET_CONTROL, ev.id, GET_UVALUE=input
    IF (TAG_EXIST(ev, 'value')) THEN  IF (DATATYPE(ev.value) EQ 'STR') THEN input=ev.value
    ;help,input
    IF (DATATYPE(input) EQ 'STR') THEN BEGIN
      if strpos(input,'*.') eq 0 then begin
    	parts=strsplit(input,'.',/extract)
	np=n_elements(parts)
	if parts[1] NE 'Clear' then begin
    	    tel=trim(parts[2])
    	    if strpos(parts[1],'SECCHI-') gt -1 then sc=strlowcase(rstrmid(parts[1],0,1)) else sc=''
    	    if tel EQ 'EUVI' or tel EQ 'AIA'  or tel EQ 'EIT' then wl='_'+trim(parts[np-1]) else wl=''
            if tel EQ 'EUVI' and pngvar.spwx EQ 1 then wl='_195'
            if strpos(input,'Stars') gt -1 then sta='s' else sta=''
            if strpos(input,'RunDiff') GT 0 or strpos(input,'Star_rem') GT 0 then rem='d' else rem=''
            if strpos(input,'Wavelets') gt -1 then wav='w' else wav=''
    	    if tel EQ 'COR2' and strpos(input,'Total Brightness') GT 0 then rem='t'
    	    if tel EQ 'COR2' and strpos(input,'Both') GT 0 then rem='*'
    	    if tel EQ 'HMI' and strpos(input,'Magnetogram') GT 0 then wl='B'
    	    if tel EQ 'HMI' and strpos(input,'Continuum') GT 0 then wl='I'
    	    if tel EQ 'HMI' and strpos(input,'Dopplergram') GT 0 then wl='D'
            tel=tel+wl+sta+wav+rem+sc
	    IF wav EQ 'w' THEN pngvar.imagesz=2048
        endif else tel='     ?     '
        widget_control,pngvar.rct(where(pngvar.rc eq ev.id)),set_value=tel
	
        pngvar.tels(where(pngvar.rc eq ev.id))=tel
      endif
      
     ;IF ~(pngvar.masks) THEN maskvals=intarr(n_elements(pngvar.tels))
     CASE (input) OF

       'usespwx': BEGIN      
                 undefine,imglist
                 read_pngvals
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 wscc_pngplay,/spwx
                 RETURN
	     END
       'usedb': BEGIN      
                 undefine,imglist
                 read_pngvals
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 wscc_pngplay
                 RETURN
	     END
       'mrow': BEGIN      
                 undefine,imglist
                 pngvar.rows=ev.index+1  
                 read_pngvals
                 if pngvar.rows gt 1 and pngvar.rdiff eq 1 then pngvar.rdiff=0
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 wscc_pngplay,spwx=pngvar.spwx
                 RETURN
	     END
       'mcol': BEGIN      
                 undefine,imglist
                 pngvar.colums=ev.index+1  
                 if pngvar.colums gt 1 and pngvar.rdiff eq 1 then pngvar.rdiff=0
                 read_pngvals
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 wscc_pngplay,spwx=pngvar.spwx
                 RETURN
	     END

       'mcad':  BEGIN  
                read_pngvals
                ;widget_control,ev.id,get_value=tmp
                pngvar.cadence=cadencemins[ev.index]
                RETURN
	     END
       'mimgsz': BEGIN  
                 undefine,imglist
                read_pngvals
                widget_control,ev.id,get_value=tmp
                pngvar.imagesz=tmp(ev.index)
		IF ~pngvar.touched THEN widget_control,pngvar.panv,set_value=trim(pngvar.imagesz*pngvar.colums)
                 RETURN
	     END
       'mmvtp': BEGIN      
                 prevtype=pngvar.mvtype
		 pngvar.mvtype=ev.index
    	    	IF prevtype EQ 1 and ev.index EQ 0 THEN BEGIN
		    pngvar.colums=1
		    pngvar.rows=1
		ENDIF
                 IF pngvar.rdiff eq 1 and pngvar.mvtype ne 0 then pngvar.rdiff=0
                 read_pngvals
                 if prevtype ne pngvar.mvtype THEN BEGIN
                   pngvar.x0 = 0
                   pngvar.x1 = 0
                   pngvar.y0 = 0
                   pngvar.y1 = 0
		ENDIF
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 wscc_pngplay,spwx=pngvar.spwx
                 RETURN
	     END
       'mclr': pngvar.true=ev.index
       'CLRS': pngvar.clruse=pngvar.clrval(ev.index)      
      	'panv': BEGIN
	    	    pngvar.touched=1
		    WIDGET_CONTROL,pngvar.panv , GET_VALUE=tmp
    	    	    pngvar.frxsize=fix(tmp[0])
		END

    	'pant': BEGIN
	; Fit to screen
                if pngvar.touched then begin
	    	    pngvar.touched=0
		    widget_control,pngvar.panv,set_value=trim(pngvar.imagesz*pngvar.colums)
		endif else begin
	    	    pngvar.touched=1
		; change Frame xSize, maintain aspect ratio
	    	    device,get_screen_size=ss
		    wsizes=define_widget_sizes()
		    ssx=ss[0]-2*wsizes.border
		    ssy=ss[1]-(2*wsizes.border+wsizes.onerow+wsizes.dsktop+wsizes.winhdr+20)
			help,ssx,ssy
		    ; depending on which type of movie, compute xsiz and ysiz differently
		    IF pngvar.mvtype eq 0 THEN BEGIN 
		    	ysiz=pngvar.rows*pngvar.imagesz
		    	xsiz=pngvar.colums*pngvar.imagesz
		    	arat=float(xsiz)/ysiz
		    	ydif=ssy-ysiz
		    	xdif=ssx-xsiz
		    	IF ydif LT xdif THEN $
		    	pngvar.frxsize=arat*ssy ELSE $
		    	pngvar.frxsize=ssx
		    	frxsize=pngvar.frxsize
		    ENDIF ELSE BEGIN
		    	pngvar.frxsize=-1
			frxsize=-1
		    ENDELSE
		    widget_control,pngvar.panv,set_value=string(frxsize,'(i4)')
                  endelse
    	    	END

    	'rdifft': BEGIN
	; set up for one telescope rdiff movie
                if pngvar.rdiff then begin
	    	    pngvar.rdiff=0
		    pngvar.true=1
		endif else begin
	    	    pngvar.rdiff=1
	    	    pngvar.true=0
                    if pngvar.mvtype ne 0 then pngvar.mvtype=0
                    if pngvar.rows gt 1 or pngvar.colums gt 1 then begin
                      undefine,imglist
                      pngvar.rows=1 & pngvar.colums=1
                      read_pngvals
	              WIDGET_CONTROL, /DESTROY, pngvar.winbase
                      wscc_pngplay,spwx=pngvar.spwx
                      RETURN
		    endif
                  endelse
    	    	END
		
       'day1': BEGIN       
                 undefine,imglist
	        WIDGET_CONTROL, ev.id, GET_VALUE=tmp
                 pngvar.d1 = tmp
	     END

       'day2':  BEGIN      
                 undefine,imglist
	        WIDGET_CONTROL, ev.id, GET_VALUE=tmp
                 pngvar.d2 = tmp
	     END    

       'resetxy':  BEGIN      
                 pngvar.x0=0. & pngvar.x1=0. & pngvar.y0=0. & pngvar.y1=0.
	         WIDGET_CONTROL, pngvar.x0v, SET_VALUE=string(pngvar.x0,'(F6.2)')
	         WIDGET_CONTROL, pngvar.x1v, SET_VALUE=string(pngvar.x1,'(F6.2)')
	         WIDGET_CONTROL, pngvar.y0v, SET_VALUE=string(pngvar.y0,'(F6.2)')
	         WIDGET_CONTROL, pngvar.y1v, SET_VALUE=string(pngvar.y1,'(F6.2)')
	     END    


       'PNGSVMOVIE' : BEGIN 
            	 mkscc_pngplay_movie,'write'
                 RETURN
	     END

      'mkmovie': BEGIN 
            	mkscc_pngplay_movie,'make'
                 RETURN
	     END
      
       'testmovie':  BEGIN 
            	mkscc_pngplay_movie,'test'
                 RETURN
	     END
       'selevents' :  BEGIN 
            	 select_event
                 RETURN
	     END

        'bpos'  : pngvar.bpos = ev.index

       
       'EXIT': BEGIN       ;** exit program
                 if datatype(wevent) eq 'STC' then begin
	           WIDGET_CONTROL, /DESTROY, wevent.eventbase
                   undefine,wevent
                 endif
	         WIDGET_CONTROL, /DESTROY, pngvar.winbase
                 undefine,list
                 undefine,pngvar
                 RETURN
	     END

       ELSE : BEGIN   
                 RETURN
	     END
    ENDCASE


  ENDIF

END 



pro WSCC_PNGPLAY,spwx=spwx

COMMON scc_png_COMMON
if keyword_set(spwx) then spwx=spwx else spwx=0
cadences=['Full',' 5 min','10 min','15 min','20 min','30 min','45 min',' 1 hr',' 2 hr']
cadencemins=[0,5,10,15,20,30,45,60,120]
imgsizes=[256,512,1024]
mvtypes=['Tiled','Combined','Polar','Polar xlog']
colors=['Grayscale','Truecolor']
 
 bmin=0.
 bmax=0.
 rdiff=0
 doxcolors=0
 mpegname=''
 pan=0. 
 half=0

if datatype(pngvar) ne 'STC' then begin
   colums=1 & rows=1
   cadence=0
   imagesz=512
   mvtype=0
   true=1
   d1='YYYYMMDD_hhmm'
   d2='YYYYMMDD_hhmm'
   x0=0.
   x1=0.
   y0=0.
   y1=0.
   bmin=0
   bmax=0
   rdiff=0
   doxcolors=0
   ;mpegname=''
   pan=1. 
   half=0
   offset=[200,200]
   touched=0
   frxsize=512
   clruse='FFFFFF'x
   tsize=0.
   bpos=0
endif else begin
   colums=pngvar.colums & rows=pngvar.rows
   cadence=pngvar.cadence
   imagesz=pngvar.imagesz
   mvtype=pngvar.mvtype
   rc=intarr(colums,rows)
   true=pngvar.true
   d1=pngvar.d1
   d2=pngvar.d2
   x0=pngvar.x0
   x1=pngvar.x1
   y0=pngvar.y0
   y1=pngvar.y1
   bmin=pngvar.bmin
   bmax=pngvar.bmax
   rdiff=pngvar.rdiff
   doxcolors=pngvar.doxcolors
   ;mpegname=pngvar.mpegname
   pan=pngvar.pan
   half=pngvar.half
   offset=pngvar.offset
   touched=pngvar.touched
   frxsize=pngvar.frxsize
   clruse=pngvar.clruse
   tsize=pngvar.tsize
   bpos=pngvar.bpos
endelse  

if mvtype ne 0 then begin
  rows=2
  colums=5
endif 

rc=intarr(colums,rows)
rct=intarr(colums,rows)
tels=strarr(colums,rows)+'     ?     '
if datatype(pngvar) eq 'STC' then begin
  tmp=where(pngvar.tels ne '     ?     ',cnt)
  if cnt gt 0 then begin
    tmp=strarr(colums*rows)+'     ?     '
    gd=where(pngvar.tels ne '     ?     ',ntels)
    IF ntels GT colums*rows THEN ngt=colums*rows-1 ELSE ngt=ntels-1
    tels(0)=pngvar.tels(gd[0:ngt])
    ;tels(0)=tmp(0:n_elements(tels)-1)
  endif
endif


IF (spwx) THEN BEGIN
  title='WSCC_PNGPLAY Beacon'
  IF d1 eq 'YYYYMMDD_hhmm' THEN BEGIN
    caldat,systime(/utc,/jul),mm,dd,yyyy 
    d1=string(yyyy,'(i4.4)')+string(mm,'(i2.2)')+string(dd,'(i2.2)')+'_0000'
    d2=string(yyyy,'(i4.4)')+string(mm,'(i2.2)')+string(dd,'(i2.2)')+'_2359'
  ENDIF
ENDIF ELSE title='WSCC_PNGPLAY NRL'

winbase = WIDGET_BASE(TITLE=title,/COLUMN,EVENT_PRO='WSCC_PNGPLAY_CONTROL_EVENT',xoffset=offset(0),yoffset=offset(1))
    	wincol = WIDGET_BASE(winbase ,/column,/frame)
    	winrow = WIDGET_BASE(wincol ,/row)
       	 tmp = WIDGET_LABEL(winrow, VALUE='Start: ')
       	 day1 = WIDGET_TEXT(winrow, VALUE=d1, XSIZE=20, YSIZE=1, UVALUE='day1', /EDITABLE)
         tmp = CW_BGROUP(winrow,  ['Select Event'], /column, BUTTON_UVALUE = ['selevents'])

    	winrow = WIDGET_BASE(wincol ,/row)
       	 tmp = WIDGET_LABEL(winrow, VALUE='  End: ')
       	 day2 = WIDGET_TEXT(winrow, VALUE=d2, XSIZE=20, YSIZE=1, UVALUE='day2', /EDITABLE)
          if keyword_set(spwx) then tmp = CW_BGROUP(winrow,  ['Use Daily Browse'], /column, BUTTON_UVALUE = ['usedb'])
        if ~keyword_set(spwx) then tmp = CW_BGROUP(winrow,  ['Use Beacon from SSC'], /column, BUTTON_UVALUE = ['usespwx'])


        winrow = WIDGET_BASE(wincol ,/row)
        cadt =   widget_droplist(winrow, VALUE=cadences, UVALUE='mcad',title='Cadence:')
        tmp=where(cadencemins eq cadence)
        widget_control,cadt,set_droplist_select=tmp[0]
     	rdiffv = CW_BGROUP(winrow, ' ', LABEL_LEFT=' RDiff (1 tel only):', UVALUE='rdifft', /NONEXCLUSIVE, /COLUMN);, /NO_RELEASE)
        widget_control,rdiffv,set_value=rdiff
;        clrt =   widget_droplist(winrow, VALUE=colors, UVALUE='mclr',title='Color :')
;        widget_control,clrt,set_droplist_select=true

        winrow = WIDGET_BASE(wincol ,/row)
        mvtpt =   widget_droplist(winrow, VALUE=mvtypes, UVALUE='mmvtp',title='Display Format:')
        widget_control,mvtpt,set_droplist_select=mvtype

        if mvtype eq 1 then begin
          bpost=['Same sun center','Left side','Right side']
          bposv=widget_droplist(winrow, VALUE=bpost, UVALUE='bpos',title='B position :')
          widget_control,bposv,set_droplist_select=bpos
	endif else bpos=0

        winrow = WIDGET_BASE(wincol ,/row)
       	 ;tmp = WIDGET_LABEL(winrow, VALUE='Bmin: ')
       	 ;bminv = WIDGET_TEXT(winrow, VALUE=string(bmin,'(f5.3)'), XSIZE=6, YSIZE=1, UVALUE='bminv', /EDITABLE)
       	 ;tmp = WIDGET_LABEL(winrow, VALUE='Bmax: ')
       	 ;bmaxv = WIDGET_TEXT(winrow, VALUE=string(bmax,'(f5.3)'), XSIZE=6, YSIZE=1, UVALUE='bmaxv', /EDITABLE)
	 bminv=0
	 bmaxv=0
        imgszt =    widget_droplist(winrow, VALUE=string(imgsizes,'(I4)'), UVALUE='mimgsz', $
	    	    title='Image source size:')
        tmp=where(imgsizes eq imagesz)
	widget_control,imgszt,set_droplist_select=tmp(0)
        
	winrow = WIDGET_BASE(wincol ,/row)
    	tmp = WIDGET_LABEL(winrow, $
	    	    VALUE='Final Frame xSize:')
	IF mvtype EQ 0 THEN frxsize=imagesz*colums
    	panv = WIDGET_TEXT(winrow, VALUE=string(frxsize,'(i4)'), XSIZE=4, YSIZE=1, UVALUE='panv', /EDITABLE)
	;winbut = widget_base(winrow,/exclusive,/align_right)
	;pant = WIDGET_BUTTON(winbut,VALUE='Fit')
     	pant = CW_BGROUP(winrow, ' ', LABEL_LEFT=' Fit:', UVALUE='pant', /NONEXCLUSIVE, /COLUMN);, /NO_RELEASE)
        widget_control,pant,set_value=touched
;       	 tmp = WIDGET_LABEL(winrow, VALUE='mpegname: ')
;       	 fnamev = WIDGET_TEXT(winrow, VALUE=mpegname, XSIZE=6, YSIZE=1, UVALUE='fnamev', /EDITABLE)
    	
	winrow = WIDGET_BASE(wincol ,/row)
    	tmp = WIDGET_LABEL(winrow, $
	    	    VALUE='Time stamp Size (0=none): ')
    	timsz = WIDGET_TEXT(winrow, VALUE=string(tsize,'(f3.1)'), XSIZE=4, YSIZE=1, UVALUE='timesz', /EDITABLE)
        clrarr=['White',   'Black',  'Red',    'Blue',  'Green',  'yellow']
        clrval=['FFFFFF'x,'000001'x,'0000FF'x,'FF0000'x,'00FF00'x,'38E5E5'x]
        clrv =   widget_droplist(winrow, VALUE=clrarr, UVALUE='CLRS',title='Color: ')
        tmp=where(clrval eq clruse)
    	widget_control,clrv,set_droplist_select=tmp(0)

        if mvtype gt 0 then begin
    	  wincol = WIDGET_BASE(winbase ,/column,/frame)
    	  winrow = WIDGET_BASE(wincol ,/row)
       	  if mvtype eq 1 then tmp = WIDGET_LABEL(winrow, VALUE='Longitude = X0: ') else tmp = WIDGET_LABEL(winrow, VALUE='Radius = X0: ')
       	  x0v = WIDGET_TEXT(winrow, VALUE=string(x0,'(f6.2)'), XSIZE=6, YSIZE=1, UVALUE='X0', /EDITABLE)
       	  tmp = WIDGET_LABEL(winrow, VALUE='X1: ')
       	  x1v = WIDGET_TEXT(winrow, VALUE=string(x1,'(f6.2)'), XSIZE=6, YSIZE=1, UVALUE='X1', /EDITABLE)
    	  winrow = WIDGET_BASE(wincol ,/row)
       	  if mvtype eq 1 then tmp = WIDGET_LABEL(winrow, VALUE='Lattitude = Y0: ') else tmp = WIDGET_LABEL(winrow, VALUE='Theta = Y0: ')
       	  y0v = WIDGET_TEXT(winrow, VALUE=string(y0,'(f6.2)'), XSIZE=6, YSIZE=1, UVALUE='Y0', /EDITABLE)
       	  tmp = WIDGET_LABEL(winrow, VALUE='Y1: ')
       	  y1v = WIDGET_TEXT(winrow, VALUE=string(y1,'(f6.2)'), XSIZE=6, YSIZE=1, UVALUE='Y1', /EDITABLE)
          tmp = CW_BGROUP(winrow,  ['Clear'], /ROW, BUTTON_UVALUE = ['resetxy'])
        endif else begin
             	    X0V=0 & X1V=0 & Y0V=0 &  Y1V=0
           winrow = WIDGET_BASE(wincol ,/row)
           rowt =   widget_droplist(winrow, VALUE=string([1,2,3,4],'(I1)'), UVALUE='mrow',title='Rows:')
           widget_control,rowt,set_droplist_select=rows-1
           colt =   widget_droplist(winrow, VALUE=string([1,2,3,4,5],'(I1)'), UVALUE='mcol',title='Columns:')
           widget_control,colt,set_droplist_select=colums-1
	endelse


        if ~keyword_set(spwx) then $
    	desc = ['1\*', $
            	'1\SECCHI-A', $
		    '1\EUVI','1\Regular','0\ 171 ','0\ 195 ','0\ 284 ','2\ 304 ',$
                             '1\RunDiff','2\ 195 ',$
                             '3\Wavelets','0\ 171 ','0\ 195 ','0\ 284 ','2\ 304 ', $ 
		    '1\COR1','0\Regular','2\RunDiff',$
		    '1\COR2','0\Double','0\Total Brightness','0\Both','2\RunDiff',$
		    '1\ HI1','0\Regular','0\Star_rem','2\Stars',$
		    '3\ HI2','0\Regular','0\Star_rem','2\Stars',$
            	'1\SECCHI-B', $
		    '1\EUVI','1\Regular','0\ 171 ','0\ 195 ','0\ 284 ','2\ 304 ',$
                             '1\RunDiff','2\ 195 ',$
                             '3\Wavelets','0\ 171 ','0\ 195 ','0\ 284 ','2\ 304 ',$  
		    '1\COR1','0\Regular','2\RunDiff',$
		    '1\COR2','0\Double','0\Total Brightness','0\Both','2\RunDiff',$
		    '1\ HI1','0\Regular','0\Star_rem','2\Stars',$
		    '3\ HI2','0\Regular','0\Star_rem','2\Stars',$
            	'1\LASCO', $
		    '1\C2C3','0\Regular','2\RunDiff',$
		    '1\ C2 ','0\Regular','2\RunDiff',$
		    '3\ C3 ','0\Regular','2\RunDiff',$
            	'1\SDO ',$
		    '1\ AIA ','0\ 171 ','0\ 193 ','0\ 211 ','2\ 304 ',$
		    '3\ HMI ','0\ Magnetogram ','0\ Continuum ','2\ Dopplergram ',$
            	'2\Clear']  else $
	       	desc = ['1\*', $
            	'1\SECCHI-A', $
		    '0\EUVI',$
		    '0\COR1',$
		    '1\COR2','0\Double','2\RunDiff',$
		    '1\ HI1','0\Regular','2\RunDiff',$
		    '2\ HI2',$
            	'1\SECCHI-B', $
		    '0\EUVI',$
		    '0\COR1',$
		    '1\COR2','0\Double','2\RunDiff',$
		    '1\ HI1','0\Regular','2\RunDiff',$
		    '2\ HI2',$
            	'1\SDO ',$
		    '1\ AIA ','0\ 171 ','0\ 193 ','0\ 211 ','2\ 304 ',$
		    '3\ HMI ','0\ Magnetogram ','0\ Continuum ','2\ Dopplergram ',$
		'2\Clear']

	IF colums+rows GT 2 THEN stxt='Select Telescopes:' ELSE stxt='Select Telescope:'
    	winrow = WIDGET_BASE(winbase ,/row)
	tmp=WIDGET_LABEL(winrow,VALUE=stxt)
        
	 for rw=rows-1,0,-1 do begin
           winrow = WIDGET_BASE(winbase ,/row)
           for cl=0,colums-1 do begin
       	     rct(cl,rw) = WIDGET_TEXT(winrow, VALUE=tels(cl,rw), XSIZE=12, YSIZE=1, UVALUE='rcedit'+string((rw+1)*(cl+1),'(i3)'))
	     rc(cl,rw) = CW_PDMENU(winrow, desc, /RETURN_FULL_NAME)
           endfor
         endfor
	 
; bottom row
    	winrow = WIDGET_BASE(winbase ,/row)
    	tmp = CW_BGROUP(winrow,  ['Preview','Make/Show Movie','Make/Save Movie ','EXIT'], /ROW, $
	      BUTTON_UVALUE = ['testmovie','mkmovie','PNGSVMOVIE','EXIT'])

   pngvar={ rows:rows, colums:colums, $
    	    rc:rc, rct:rct, $
    	    winbase:winbase, $
    	    cadence:cadence, $
	    masks:0, $
    	    tels:tels,$
    	    d1:'', d2:'',$
    	    imagesz:imagesz, $
    	    mvtype:mvtype, $
    	    true:true, $
    	    day1:day1, day2:day2, $
    	    X0:X0, X1:X1, Y0:Y0, Y1:Y1, $
    	    X0V:X0V, X1V:X1V, Y0V:Y0V, Y1V:Y1V, $
    	    bmin:bmin, bmax:bmax, $
    	    rdiff:rdiff,rdiffv:rdiffv, doxcolors:doxcolors,$
            pan:pan, frxsize:frxsize, panv:panv, touched:touched,$
    	    half:half, $
    	    bminv:bminv, bmaxv:bmaxv, $
            tsize:tsize, $
            timsz:timsz, $
            bpos:bpos, $
            clrval:clrval, $
            clruse:clruse, $
            clrarr:clrarr, $
            clrv:clrv, $
	    offset:offset,$
	    spwx:spwx }

	    ;, mpegname:mpegname, fnamev:fnamev}

   WIDGET_CONTROL, /REAL, winbase
  XMANAGER, 'WSCC_PNGPLAY', winbase, EVENT_HANDLER='WSCC_PNGPLAY_CONTROL_EVENT', /NO_BLOCK
END
