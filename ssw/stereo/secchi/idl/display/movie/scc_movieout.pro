;$Id: scc_movieout.pro,v 1.52 2015/01/27 18:59:10 hutting Exp $
;
; Project     : SECCHI
;                   
; Name        : scc_movieout
;               
; Purpose     : called from scc_movie_win when saving movies 
;               
; Explanation : 
;
; Use         : need common block sWIN_INDEXM and mvoinfo
;
;
; Optional Inputs:
;   	
;
; KEYWORDS: 	
;                 hdrs= movie frame headers
;                 filename= output filename
;                 len= number of frames
;                 first= first frame of movie
;                 last= last frame of movie
;                 deleted = delete value for win_index 1= deleted
;                 rect=rect = header rectified keyword
;                 selectonly = returns movie name and does not save movie
;                 saveonly = saves movie without displaying widget
;                 DEBUG = SECCHI movie tools debug option             
;
; Calls       : 
;
; Side effects: will write over an existing movie if name is the same
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL March 2009.
;               
;	
;-            
;$Log: scc_movieout.pro,v $
;Revision 1.52  2015/01/27 18:59:10  hutting
;corrected truecolor error when file_hdr is undefined
;
;Revision 1.51  2014/06/11 13:06:14  hutting
;added a fits output option
;
;Revision 1.50  2012/04/26 16:18:41  nathan
;fix typo in last edit
;
;Revision 1.49  2012/04/25 12:49:11  mcnutt
;added jpeg option
;
;Revision 1.48  2012/01/25 18:21:48  mcnutt
;added rectified keyword to scc_save_mvframe call
;
;Revision 1.47  2011/12/01 16:40:12  nathan
;mkdir for Windows
;
;Revision 1.46  2011/11/01 12:31:05  mcnutt
;uses scc_save_mvframe when saving movie
;
;Revision 1.45  2011/10/17 20:07:48  nathan
;Modify widget messages
;
;Revision 1.44  2011/10/17 17:49:27  mcnutt
;added .ffmpg option to write a ffmpg .mpg movie
;
;Revision 1.43  2011/09/30 13:15:17  mcnutt
;defines flatplane form rtheta values before writting first frame
;
;Revision 1.42  2011/09/26 18:30:14  mcnutt
;added latplane movie grrid and measuring options
;
;Revision 1.41  2011/07/27 13:56:24  mcnutt
;correct frame time if not a secchi header use of date_cmd
;
;Revision 1.40  2011/07/26 18:40:32  mcnutt
;sets timestr to mvoinfo.img_hdrs(i).date_cmd if not blank
;
;Revision 1.39  2011/05/20 15:01:05  mcnutt
;uses date_cmd for string if tag exists
;
;Revision 1.38  2011/05/18 22:40:10  nathan
;change references to wrunmovie to playmovie
;
;Revision 1.37  2011/05/16 20:46:21  secchib
;nr - fix info messages
;
;Revision 1.36  2011/05/05 21:18:50  nathan
;allow YYMM.. input for filenames
;
;Revision 1.35  2011/05/02 16:58:20  mcnutt
;added seconds back to png/gif/jpg file names
;
;Revision 1.34  2011/03/18 16:51:28  mcnutt
;added sun(xy)cen to rtcoords for rtheta movies
;
;Revision 1.33  2011/01/05 18:27:20  nathan
;use binfound to test for ffmpeg
;
;Revision 1.32  2010/12/22 17:58:21  mcnutt
;added .mov and .avi movie out option if ffmpeg is available
;
;Revision 1.31  2010/10/27 17:06:22  mcnutt
;allows for rtheta ylog option
;
;Revision 1.30  2010/10/13 15:16:56  nathan
;enhanced OVERWRITE message
;
;Revision 1.29  2010/08/31 18:12:34  mcnutt
;fixed AB or L selection for png movie
;
;Revision 1.28  2010/08/06 18:00:55  mcnutt
;added keyword rtcoords to write rtheta movies
;
;Revision 1.27  2010/06/24 20:57:18  nathan
;widget labeling
;
;Revision 1.26  2010/04/14 15:52:59  nathan
;fix error in ovwrt check if DONE
;
;Revision 1.25  2010/04/12 18:07:09  mcnutt
;added idl mpeg_encode option
;
;Revision 1.24  2010/02/24 18:09:57  mcnutt
;added mpg option to selectonly
;
;Revision 1.23  2009/10/14 12:22:19  mcnutt
;corrected mpeg rebin keyword
;
;Revision 1.22  2009/10/09 18:35:35  mcnutt
;added slow option for mpg movies
;
;Revision 1.21  2009/10/07 17:28:45  mcnutt
;added saveonly keyword
;
;Revision 1.20  2009/09/04 13:56:17  mcnutt
;loads color table before writting png and gif movies
;
;Revision 1.19  2009/08/24 16:14:28  nathan
;fix undefined debug_on
;
;Revision 1.18  2009/08/21 17:40:02  mcnutt
;set outfile to blank string if cancel
;
;Revision 1.17  2009/08/20 19:55:09  nathan
;Added filnotchanged, dirnotchanged, mvoutdebug to COMMON sccmvout; improved
;(hopefully) functionality of directory and file naming.
;
;Revision 1.16  2009/08/20 16:09:43  nathan
;allow case where frame headers are undefined
;
;Revision 1.15  2009/08/14 22:51:52  nathan
;remove stop; use debug_on in wscc_mkmovie_common
;
;Revision 1.14  2009/07/29 16:31:28  nathan
;fix bug in write-gif
;
;Revision 1.13  2009/07/24 18:10:27  mcnutt
;added pick file button which call PICKFILE to select filename
;
;Revision 1.12  2009/07/17 18:55:40  nathan
;Use /NOSHELL with spawn; always use concat_dir to create directory; apply
;resolve_path.pro to input directory
;
;Revision 1.11  2009/06/30 17:14:43  mcnutt
;send sec_pix keyword to sccwrite_disk_movie
;
;Revision 1.10  2009/06/29 18:10:56  mcnutt
;will write a mvi movie even if imgs are not defined
;
;Revision 1.9  2009/06/29 11:59:03  mcnutt
;sets outfile to blank string if not defined before returning
;
;Revision 1.8  2009/06/25 17:31:37  mcnutt
;uses keywords instead of moviev structure
;
;Revision 1.7  2009/04/14 15:26:12  nathan
;fix outputdir check logic
;
;Revision 1.6  2009/04/14 13:56:59  mcnutt
;corrected premission check
;
;Revision 1.5  2009/04/13 17:45:08  mcnutt
;corrected output dir for true color png and gif movies
;
;Revision 1.4  2009/04/10 19:03:29  mcnutt
;handles png and gif file names with and with out hhmm
;
;Revision 1.3  2009/04/10 18:26:02  mcnutt
;set filename to default and does not change mvoinfo.filename
;
;Revision 1.2  2009/04/10 17:01:47  mcnutt
;change filename if image time_obs is blank
;
;Revision 1.1  2009/04/10 15:40:30  mcnutt
;save movies all formats
;
;
PRO scc_save_movie,_EXTRA=_extra

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
COMMON sccmvout, mobase ,filext, outfile, filnotchanged, dirnotchanged, mvoutdebug, mvoinfo

  mframes=  fix(WIDGET_INFO(mvoinfo.mpgmfv, /COMBOBOX_GETTEXT))
  SCALE=  fix(WIDGET_INFO( mvoinfo.mpgcmpv,/COMBOBOX_GETTEXT)) 
  REFERENCE=WIDGET_INFO(mvoinfo.mpgrefv,/COMBOBOX_GETTEXT)
  if strpos(REFERENCE,'colorquan') gt -1 then colorquan=1 else colorquan=0
  if strpos(REFERENCE,'ORIGINAL') gt -1 then REFERENCE='ORIGINAL' else REFERENCE='DECODED'

  if mvoinfo.filext eq '.idlmpg' then begin
    idlencode=1 
    outfile=concat_dir(mvoinfo.mvdir,mvoinfo.mvfile+'.mpg')
  endif else idlencode=0
if mvoinfo.selectonly eq 0 then begin
  first = 1
  good = WHERE(mvoinfo.deleted NE 1)
  good = good(where((good GE mvoinfo.first) AND (good LE mvoinfo.last),nframes))
  FOR i=0, nframes-1 DO BEGIN
     if datatype(imgs) ne 'BYT' then img_win=mvoinfo.win_index(good(i)) else if mvoinfo.file_hdr.truecolor eq 0 then img_win=imgs(*,*,(good(i))) else img_win=imgs(*,*,*,(good(i)))
     scc_save_mvframe,i,outfile,img_win,mvoinfo.img_hdrs(good(i)),mvoinfo.file_hdr,first=first,inline=nframes, SCALE=SCALE, $
                REFERENCE=REFERENCE , mframes=mframes, idlencode=idlencode,colorquan=colorquanrect,RECTIFIED=mvoinfo.rect,_EXTRA=_extra
     first=0
  ENDFOR
endif else begin
  mvoinfo.outmovie.outfile=outfile
  mvoinfo.outmovie.SCALE=SCALE
  mvoinfo.outmovie.REFERENCE=REFERENCE
  mvoinfo.outmovie.mframes=mframes
  mvoinfo.outmovie.idlencode=idlencode
  mvoinfo.outmovie.colorquan=colorquan
endelse
 ; undefine,mvoinfo

end ; pro scc_save_movie

pro update_combox,id,newlist
    WIDGET_CONTROL, id, get_value=tmp
    for n=0,n_elements(newlist)-1 do WIDGET_CONTROL, id,combobox_additem=newlist(n),COMBOBOX_INDEX=n
    for n=n_elements(newlist)+n_elements(tmp)-1,n_elements(newlist),-1 do WIDGET_CONTROL, id,combobox_deleteitem=n
    WIDGET_CONTROL, id,SET_COMBOBOX_SELECT= 0
end

PRO MOVIEOUT_EVENT, ev

COMMON sccmvout ;, mobase ,filext, outfile

WIDGET_CONTROL, ev.id, GET_UVALUE=input
if datatype(input) eq 'STR' then begin
  CASE strupcase(input) OF

    'CANCEL' : BEGIN       ;** exit program
            outfile=''
    	    WIDGET_CONTROL, /DESTROY, ev.top
	    WIDGET_CONTROL, mvoinfo.groupleader, /DESTROY
    	    RETURN
    	    END

    'DONE' : BEGIN       ;
           dash=strsplit(mvoinfo.mvdir,'/')
	   
           ovwrt=file_search(concat_dir(mvoinfo.mvdir,mvoinfo.mvfile+mvoinfo.filext))
	   IF !version.os_family EQ 'unix' THEN mkdircmd=['mkdir','-p',mvoinfo.mvdir] ELSE mkdircmd=['mkdir',mvoinfo.mvdir]
	   print,'MVDIR=',mvoinfo.mvdir
    	    if file_search(mvoinfo.mvdir) eq '' then spawn,mkdircmd,/NOSHELL
	   ;--in order for check_permission to work, directory must end with slash
	   mvdirpermission=CHECK_PERMISSION(concat_dir(mvoinfo.mvdir,''))
           IF (mvdirpermission GT 0) and ovwrt eq '' THEN BEGIN
  	      ;mvoinfo.filepath=mvoinfo.mvdir
	      outfile=concat_dir(mvoinfo.mvdir,mvoinfo.mvfile+mvoinfo.filext)
              ;WIDGET_CONTROL, inbase, SET_UVALUE=mvoinfo
                ptxt='Saving Movie '
                widget_control,mvoinfo.mvtxt,set_value=ptxt
	        scc_save_movie
    	        WIDGET_CONTROL, /DESTROY, ev.top
	        WIDGET_CONTROL, mvoinfo.groupleader, /DESTROY
    	        RETURN
            ENDIF 
	    IF (mvdirpermission LE 0) THEN BEGIN
              ptxt='You do not have permission to make directory '
	      help,mvoinfo.mvdir
              widget_control,mvoinfo.mvtxt,set_value=ptxt
            ENDIF
            IF ovwrt ne '' THEN BEGIN
              print,string(7B)		; ring bell
	      ptxt='***** File exists. Press OVERWRITE to save, or change filename. *****'
              widget_control,mvoinfo.mvtxt,set_value=ptxt
	    ENDIF
	    
	    END

    'OVERWRITE' : BEGIN       ;
           IF (CHECK_PERMISSION(mvoinfo.mvdir) GT 0) THEN BEGIN
  	      ;mvoinfo.filepath=mvoinfo.mvdir
	      outfile=concat_dir(mvoinfo.mvdir,mvoinfo.mvfile+mvoinfo.filext)
              ;WIDGET_CONTROL, inbase, SET_UVALUE=mvoinfo
                ptxt='Saving Movie '
                widget_control,mvoinfo.mvtxt,set_value=ptxt
		scc_save_movie
    	        WIDGET_CONTROL, /DESTROY, ev.top
	        WIDGET_CONTROL, mvoinfo.groupleader, /DESTROY
    	        RETURN
            ENDIF ELSE BEGIN
              ptxt='You do not have permission to make directory '
              widget_control,mvoinfo.mvtxt,set_value=ptxt
            ENDELSE
	    
	    END
    'PKFILE' : BEGIN
                fname = PICKFILE(file=mvoinfo.mvfile, filter='*'+mvoinfo.filext, $
                                   TITLE='Select output movie file', $
                                   PATH=mvoinfo.mvdir,GET_PATH=path)
                if fname ne '' then begin
                   BREAK_FILE, fname, a, mvdir, mvfile, ext
                   mvoinfo.mvdir=mvdir
		   mvoinfo.mvfile=mvfile
                   widget_control,mvoinfo.mvdirv,set_value=mvoinfo.mvdir
                   widget_control,mvoinfo.mvfilev,set_value=mvoinfo.mvfile
		endif
             END


    'MVFILEV' : BEGIN 	
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    mvfile=val[0]
	    mvoinfo.mvfile = mvfile
	    filnotchanged=0
    	    END

    'MVDIRV' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    IF strlen(val[0]) GT 2 THEN BEGIN
	    	mvdir=resolve_path(val[0])
	    	mvoinfo.mvdir = mvdir
    	    ENDIF
	    dirnotchanged=0
	    END

    'EXTOV' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    mvoinfo.filext=val[ev.index]
	    mvdir = mvoinfo.mvdir
	    mvfile = mvoinfo.mvfile
	    IF mvoinfo.img_hdrs(0).filename EQ '' THEN BEGIN
	    ; Frame headers are probably empty.
	    	mvoinfo.img_hdrs(0).filename='frame'
		tmp=mvfile
	    ENDIF ELSE $
	        tmp=mvoinfo.img_hdrs(0).detector+strmid(mvoinfo.img_hdrs(0).FILENAME,strpos(mvoinfo.img_hdrs(0).FILENAME,'.ft')-1,1)+'_'+ $
                                     strmid(mvoinfo.img_hdrs(0).DATE_OBS,0,4)+strmid(mvoinfo.img_hdrs(0).DATE_OBS,5,2)+strmid(mvoinfo.img_hdrs(0).DATE_OBS,8,4)+'/'
	    
	    if (mvoinfo.filext eq '.png' or mvoinfo.filext eq '.gif' or mvoinfo.filext eq '.jpg' or mvoinfo.filext eq '.fts') then begin
                if strpos(mvdir,tmp) eq -1 and (dirnotchanged) then mvdir = concat_dir(mvdir,tmp)
		IF (filnotchanged) THEN BEGIN
		    mvfile='frame'
		    camx=strlowcase(mvoinfo.img_hdrs[0].detector)
		    if TAG_EXIST(mvoinfo.img_hdrs,'INSTRUME') then begin
                      if mvoinfo.img_hdrs[0].INSTRUME eq 'SECCHI' then camx=camx+strmid(mvoinfo.img_hdrs[0].OBSRVTRY,0,1,/reverse)
                      if mvoinfo.img_hdrs[0].INSTRUME eq 'LASCO' then camx=camx+'L'
		    endif
                   if camx NE '' THEN mvfile='yyyymmdd_hhmmss_ssuff'
		ENDIF
                ptxt='For png/gif movies, yyyymmddhhmm will be replaced with date/time, ssuff will be replced with detector if available.'
                widget_control,mvoinfo.mvtxt,set_value=ptxt
                WIDGET_CONTROL, mvoinfo.mpgmfv, MAP= 0
                WIDGET_CONTROL, mvoinfo.mpgcmpv, MAP= 0
                if  mvoinfo.filext ne '.jpg' then begin
		  mvoinfo.mpgref=['full color', 'use colorquan']
                  update_combox,mvoinfo.mpgrefv,mvoinfo.mpgref
                  WIDGET_CONTROL, mvoinfo.mpgrefv, MAP= 1
                endif else WIDGET_CONTROL, mvoinfo.mpgrefv, MAP= 0
            endif

	    if (mvoinfo.filext eq '.mvi') then begin
	        tmp=mvoinfo.img_hdrs(0).detector+strmid(mvoinfo.img_hdrs(0).FILENAME,strpos(mvoinfo.img_hdrs(0).FILENAME,'.ft')-1,1)+'_'+ $
                                     strmid(mvoinfo.img_hdrs(0).DATE_OBS,0,4)+strmid(mvoinfo.img_hdrs(0).DATE_OBS,5,2)+strmid(mvoinfo.img_hdrs(0).DATE_OBS,8,4)
                if strpos(mvdir,tmp) gt -1 and (dirnotchanged) then mvdir = strmid(mvdir,0,strpos(mvdir,tmp))
		IF (filnotchanged) THEN mvfile=tmp
                ptxt='Select Dir & rootname for .mvi movie file.'
                widget_control,mvoinfo.mvtxt,set_value=ptxt
                WIDGET_CONTROL, mvoinfo.mpgmfv, MAP= 0
                WIDGET_CONTROL, mvoinfo.mpgcmpv, MAP= 0
		WIDGET_CONTROL, mvoinfo.mpgrefv, MAP= 0
            endif

            if (mvoinfo.filext eq '.mpg') then begin
		;IF (filnotchanged) THEN mvfile='default'
                ptxt='Select Dir & rootname for .mpg movie file using mpeg_encode.'
                widget_control,mvoinfo.mvtxt,set_value=ptxt
                WIDGET_CONTROL, mvoinfo.mpgmfv, MAP= 1
		mvoinfo.mpgcmp=['4','8','16','24','32']
                update_combox,mvoinfo.mpgcmpv,mvoinfo.mpgcmp
                WIDGET_CONTROL, mvoinfo.mpgcmpv, MAP= 1
                mvoinfo.mpgref=['High quality="DECODED"', 'Low quality="ORIGINAL"']
                update_combox,mvoinfo.mpgrefv,mvoinfo.mpgref
                WIDGET_CONTROL, mvoinfo.mpgrefv, MAP= 1
	    endif

            if (mvoinfo.filext eq '.ffmpg') or (mvoinfo.filext eq '.mov') or (mvoinfo.filext eq '.avi') then begin
		;IF (filnotchanged) THEN mvfile='default'
                ptxt='Select Dir & rootname for '+mvoinfo.filext+' movie file.'
                widget_control,mvoinfo.mvtxt,set_value=ptxt
                WIDGET_CONTROL, mvoinfo.mpgmfv, MAP= 1
		mvoinfo.mpgcmp=['1','2','4','8','16']
                update_combox,mvoinfo.mpgcmpv,mvoinfo.mpgcmp
                WIDGET_CONTROL, mvoinfo.mpgcmpv, MAP= 1
;                mvoinfo.mpgref=['High quality use -sameq', 'Low quality']
;                update_combox,mvoinfo.mpgrefv,mvoinfo.mpgref
                WIDGET_CONTROL, mvoinfo.mpgrefv, MAP= 0

	    endif
            if (mvoinfo.filext eq '.idlmpg') then begin
		;IF (filnotchanged) THEN mvfile='default'
                ptxt='Select Dir & rootname for '+mvoinfo.filext+' movie file.'
                widget_control,mvoinfo.mvtxt,set_value=ptxt
                WIDGET_CONTROL, mvoinfo.mpgmfv, MAP= 1
		mvoinfo.mpgcmp=['100','80','60','40','20']
                update_combox,mvoinfo.mpgcmpv,mvoinfo.mpgcmp
                WIDGET_CONTROL, mvoinfo.mpgcmpv, MAP= 1
               WIDGET_CONTROL, mvoinfo.mpgrefv, MAP= 0

	    endif

	    mvoinfo.mvdir = mvdir
	    mvoinfo.mvfile = mvfile
            widget_control,mvoinfo.mvdirv,set_value=mvoinfo.mvdir
            widget_control,mvoinfo.mvfilev,set_value=mvoinfo.mvfile
    	    if mvoutdebug THEN help,dirnotchanged,filnotchanged
    	    END

    ELSE : BEGIN
          ;  PRINT, '%%SCC_PLOT_EVENT.  Unknown event.'
    	    END

  ENDCASE
endif 

END


PRO scc_movieout,win_index,file_hdr,hdrs=hdrs,filename=filename,len=len, $
    first=first,last=last,deleted=deleted,rect=rect,selectonly=selectonly,outmovie=outmovie, saveonly=saveonly, $
    DEBUG=debug

COMMON sccmvout
COMMON WSCC_MKMOVIE_COMMON

   if ~keyword_set(filename) then filename=''
   if ~keyword_set(len) then len = n_elements(win_index)
   if ~keyword_set(deleted) then deleted = BYTARR(len)
   if ~keyword_set(first) then first=0 else first=first
   if ~keyword_set(last) then last=len-1 else last=last
   if ~keyword_set(rect) then rect=0 else rect=rect

    IF keyword_set(DEBUG) THEN debug_on=1
    IF datatype(debug_on) EQ 'UND' THEN debug_on=0
    IF debug_on THEN mvoutdebug=1 ELSE mvoutdebug=0
;  inbase=inbase1
;   WIDGET_CONTROL, inbase, GET_UVALUE=moviev  ; get structure from UVALUE

    ;--Do not automodify filename or directory if it has been manually editted.
    filnotchanged=1
    dirnotchanged=1

        BREAK_FILE, filename, a, mvdir, mvfile, ext

         if mvfile eq '' then mvfile='default'
         if mvdir eq '' then begin
           cd,current=curr
           mvdir=curr+'/'
	 endif
	 
IF (debug_on) THEN help,filename,mvdir,mvfile,file_hdr,len,first,last,delted,rect,selectonly
IF (debug_on) THEN wait,3

        groupleader = Widget_Base(Map=0)
        Widget_Control, groupleader, /Realize

	mobase = WIDGET_BASE(/COL, XOFFSET=275, YOFFSET=275, TITLE='Select directory and file name for movie',group_leader=groupleader,/modal)
	rowb = WIDGET_BASE(mobase, /ROW)
	mvdirv =     CW_FIELD(rowb, VALUE=mvdir, /ALL_EVENTS, XSIZE=75, YSIZE=1, UVALUE='mvdirV', title='Directory: ')

	rowb1 = WIDGET_BASE(mobase, /ROW)
	mvfilev =   CW_FIELD(rowb1, VALUE=mvfile, /ALL_EVENTS, XSIZE=45, YSIZE=1, UVALUE='mvfileV', title='Output Filename(s): ')

	 ptxt='Select Dir/name for MVI/MPG/PNG/GIF movie files'

         tcolor=0
         if datatype(file_hdr) ne 'STR' then tcolor= file_hdr.truecolor
         IF tcolor eq 0 then exout=['.mvi','.mpg','.png','.jpg','.gif','.fts'] else exout=['.mvi','.mpg','.png','.jpg','.gif'] 
         IF binfound('ffmpeg')  then exout=[exout,'.ffmpg','.mov','.avi','.idlmpg']
;         if datatype(imgs) eq 'UND' then begin 
;           exout=['.mpg','.png','.gif']
;	   ptxt='You must be in Edit Mode to save a .mvi file' 
;	 ENDIF

;         if keyword_set(selectonly) then begin 
;           exout=['.mvi','.png','.gif','.fts']
;	   ptxt='Select Dir/name for MVI/PNG/GIF movie files' 
;           selectonly=1
;	 ENDIF else selectonly=0
         if keyword_set(selectonly) then selectonly=1 else selectonly=0 

        filext=exout(0)
	extype=0

        txt=widget_label (rowb1,  value='Movie Type:')
        extov= WIDGET_COMBOBOX(rowb1, VALUE=exout, UVALUE="extov")
        WIDGET_CONTROL, extov, SET_COMBOBOX_SELECT= extype
        WIDGET_CONTROL, extov, SENSITIVE=1

	pkfile = WIDGET_BUTTON(rowb1, VALUE=' PICKFILE ', UVALUE='PKFILE')

	   rowb2 = WIDGET_BASE(mobase, /ROW,/frame)
           txt=widget_label (rowb2,  value='Format Options:')

	   col22 = WIDGET_BASE(rowb2, /COL)
	   col22 = WIDGET_BASE(col22, /row)
           txt=widget_label (col22,  value='Compression:')
           mpgcmp=['4','8','16','24','32'] ; values for mpg_encode
           mpgcmpv= WIDGET_COMBOBOX(col22, VALUE=mpgcmp);, UVALUE="mpgcmpv")
           WIDGET_CONTROL, mpgcmpv, SET_COMBOBOX_SELECT= 0
           WIDGET_CONTROL, mpgcmpv, MAP= 0

	   col23 = WIDGET_BASE(rowb2, /COL)
	   col23 = WIDGET_BASE(col23, /row)
           txt=widget_label (col23,  value='Reference:')
           mpgref=['High quality="DECODED"', 'Low quality="ORIGINAL"']
           mpgrefv= WIDGET_COMBOBOX(col23, VALUE=mpgref);, UVALUE="mpgrefv")
           WIDGET_CONTROL, mpgrefv, SET_COMBOBOX_SELECT= 0
           WIDGET_CONTROL, mpgrefv, MAP= 0

	   col21 = WIDGET_BASE(rowb2, /COL)
	   col21 = WIDGET_BASE(col21, /row)
           txt=widget_label (col21,  value='Frames per Image: SLOW *')
           mpgmf=['1','2','4','8']
           mpgmfv= WIDGET_COMBOBOX(col21, VALUE=mpgmf);, UVALUE="mpgmfv")
           WIDGET_CONTROL, mpgmfv, SET_COMBOBOX_SELECT= 0
           WIDGET_CONTROL, mpgmfv, MAP= 0

	rowc = WIDGET_BASE(mobase, /ROW)
        mvtxt=widget_text(rowc,value=ptxt,uvalue='mvtxt',font='-1',xsize=70,ysize=1)

	done = WIDGET_BUTTON(rowc, VALUE='  DONE  ', UVALUE='DONE')
	overwrite = WIDGET_BUTTON(rowc, VALUE='  OVERWRITE  ', UVALUE='OVERWRITE')
	cancel = WIDGET_BUTTON(rowc, VALUE=' CANCEL ', UVALUE='CANCEL')




    	;**--------------------Done Creating Widgets-----------------------------**
        outmovie={outfile:'',REFERENCE:'',scale:1,mframes:1,idlencode:0,colorquan:0}
    	mvoinfo={mvtxt:mvtxt, mvfile:mvfile, mvfilev:mvfilev, mvdir:mvdir, mvdirv:mvdirv, mpgmf:mpgmf,mpgmfv:mpgmfv,$
                   mpgcmp:mpgcmp, mpgcmpv:mpgcmpv, mpgref:mpgref,mpgrefv:mpgrefv, selectonly:selectonly, filext:filext, $
                   filename:filename,win_index:win_index,file_hdr:file_hdr,img_hdrs:hdrs,len:len,first:first,last:last,deleted:deleted,rect:rect, $
		   groupleader:groupleader,outmovie:outmovie}
	

	  WIDGET_CONTROL, mobase, /REALIZE
          if keyword_set(saveonly) and filename ne '' then begin    
                BREAK_FILE, filename, a, dir, name, ext
                mvoinfo.mvfile=name
                mvoinfo.mvdir=dir
                mvoinfo.filext=ext
                outfile=dir+name+ext
                scc_save_movie
    	        WIDGET_CONTROL, /DESTROY, mobase
	  endif else begin

	     XMANAGER, 'SCC_MOVIEOUT', mobase, EVENT_HANDLER='MOVIEOUT_EVENT'
       endelse
        if keyword_set(selectonly) then outmovie=mvoinfo.outmovie
    	;**-----------------End widget --- start writing file ----------------**

END
  

