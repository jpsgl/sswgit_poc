;+
;
;$Id: scc_wrunmovie.pro,v 1.54 2011/05/18 22:25:10 nathan Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_WRUNMOVIE
;               
; Purpose     : Widget tool to display animation sequence.
;               
; Explanation : This tool allows the user to view a series of images as
;		an animation sequence.  The user can control the direction,
;		speed, and number of frames with widget controls.
;               
; Use         : IDL> SCC_WRUNMOVIE [, arg1 [, NAMES=names [,SKIP=skip]]]
;
;		Use the VIDEO keyword to automatically prepare video-ready format (640x480);
;		use with IMG_REBIN if you want to keep full x-field
;	  	IDL> SCC_WRUNMOVIE, /VIDEO [,/IMG_REBIN]
;
;		Without any inputs, program will prompt user to select an existing .mvi file.
;    Example  : IDL> SCC_WRUNMOVIE
;
;               Or you could have one argument, the .mvi file you want to load.
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi'
;
;               Or if you have pre-loaded images into pixmaps (like MKMOVIE.PRO does) call:
;    Example  : IDL> SCC_WRUNMOVIE, win_index, NAMES=names
;		Where win_index is an array of the window numbers and names is optionally
;		a STRARR() containing names of each frame.
;;    
;		Use the keyword SKIP to skip every n frames (good for large movies).
;    Example  : IDL> SCC_WRUNMOVIE, SKIP=1		;* to skip every other frame
;
;		Use the keyword START to start reading movie at frame n, where first frame is n=0 (good for large movies).
;    Example  : IDL> SCC_WRUNMOVIE, START=100		;* frame 100 becomes 1st frame of movie
;
;		Use the keyword LENGTH to specify number of frames to read in (good for large movies).
;    Example  : IDL> SCC_WRUNMOVIE, START=100, LENGTH=60	;* to load frames 100-159
;
;		Use the keyword TIMES to display detector & date & time on frames (if not already there).
;    Example  : IDL> SCC_WRUNMOVIE, /TIMES
;
;		Use the keyword NOCAM with TIMES keyword to omit the detector from TIMES display.
;    Example  : IDL> SCC_WRUNMOVIE, /TIMES, /NOCAM
;
;		Use the keyword COORDS to display subframe of movie images. [x1,x2,y1,y2]
;		Note: COORDS is applied before IMG_REBIN if both are selected.
;		OR just use /COORDS to select coordinates interactively
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', COORDS=[256,256+511,175,175+255]
;
;		Use the keyword IMG_REBIN to resize movie.  If shrinking by integer factor REBIN 
;		is used otherwise CONGRID with linear interpolation is used.
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', IMG_REBIN=[512,512]
;
;		Use the keyword SAVE to just save the movie and exit (for use in batch mode).
;    Example  : IDL> SCC_WRUNMOVIE, win_index, SAVE='mymovie.mvi'
;
;		Use the keyword DIFF to subtract a base frame from all frames in the movie.
;		The base frame is the first frame.  Or you can use the keyword START to set it.
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', /DIFF
;
;		Use the keyword RUNNING_DIFF to create a running difference movie.
;		The default is to subtract the previous frame.  Use RUNNING_DIFF=2 to subtract
;		2 frames prior from each image.
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', /RUNNING_DIFF
;
;		Use the keyword /COSMIC to removie cosmic rays.
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', /COSMIC
;
;		Use the keyword /FIXGAPS to replace data gaps with data from previous frame
;		The default is to assume missing blocks are 32x32, 
;		For 1/2 resolution images use BLOCK_SIZE=16 for example
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', /FIXGAPS
;
;		Use the keyword /DRAW_LIMB to draw a circle at the solar limb
;    Example  : IDL> SCC_WRUNMOVIE, 'mymovie.mvi', /DRAW_LIMB
;
; OTHER KEYWORDS:
;	LOAD	Set to use saved keyword values, if any
;	KEEP	Set to not delete pixmaps 
;	CHSZ	Set to desired size of time label (default=1.5)
;	SPOKE	Display spoked images *** NEEDS WORK ***
;	DIF_MIN, DIF_MAX	Set to desired range for scaling difference images; default is +/-70
;	RECTIFIED	Set to number of degrees images rotated to put solar north up (affects header only)
;	/DORECTIFY	Rotate frames 180 degrees
;	/CENRECTIFY	Compute new center for 180-deg-rotation
;	/TRUECOLOR	Set to treat images as true color images
;   	/NOXCOLORS   	Set to disable ability to change color tables, which saves memory
;   	SUNXCEN=, SUNYCEN=  Override sun center in header if any (IDL coords)
;
;
; Calls       : 
;
; Comments    : Use MKMOVIE.PRO to load into pixmaps.
;               
; Side effects: None.
;               
; Category    : Image Display.  Animation.
;               
; Written     : Scott Paswaters, NRL Feb. 13 1996.
;               
; Modified    : SEP  29 May 1996 - Changed to multiple pixmaps for images.
;				   Added buttons to save and load movie files (.mvi).
;				   Seperated control buttons from display window.
;               SEP   9 Jul 1996 - Added keyword to pass image headers.
;               SEP   7 Jan 1997 - Added skip keyword.
;               SEP   9 Jan 1997 - Added START, LENGTH keywords.
;               SEP  05 Feb 1997 - Mods for mvi version 1 format.
;               SEP  18 Apr 1997 - Added .mpg output option with 1/2 resolution.
;               SEP  16 May 1997 - Added save option and IMG_REBIN option.
;               SEP  19 May 1997 - Added COORDS option.
;               SEP  27 Jun 1997 - Added permission checks for output.
;               SEP  22 Sep 1997 - Added current frame scrolling widget.
;               SEP  02 Oct 1997 - Added ability to interactively select subimage coords.
;               SEP  18 Nov 1997 - Added /COSMIC  /FIXGAPS and /DRAW_LIMB keywords.
;               SEP  11 Dec 1997 - Added button to call SCC_WRUNMOVIEM, only save frames first->last
;               SEP  08 May 1998 - Added BLOCK_SIZE keyword
;               SP   02 Mar 1999 - Added Scroll bars for large images
;		NBR  26 Mar 1999 - Added LOGO keyword; Add Save-movie-as-GIFS button
;		NBR  09 Apr 1999 - Use short_names for frame names
;		NBR     Jul 1999 - Add VIDEO keyword; Add detector to TIMES label
;		NBR     Aug 1999 - Add detector to GIF names
;		NBR     Sep 1999 - Add NOCAM keyword
;               ???  06 FEB 2000 - Add bytscl range for DIFF and RUNNING_DIFF images (DIF_MIN/MAX keywords)
;               JIE  14 JUN 2000 - ADD keyword CHSZ to adjust the size of displayed time
;               JIE   2 MAR 2000 - ADD keyword SPOKE to display spoked images
;	NBR	 3 Oct 2000 - Save gif files in current directory by default
;	RAH	18 Oct 2000 - Added option to not rescale a postscript image. Default was to rescale
;	NBR	15 Dec 2000 - Allow setting of TIMES keyword to color desired
;	NBR	 3 Jan 2001 - Put win_index in common block, add KEEP keyword
;	NBR	 4 Jan 2001 - Remove win_index from common block
;	NBR	10 Apr 2001 - Change output gif filenames and reconcile diverging versions of this procedure
;	NBR	25 Apr 2002 - Change default movie speed
;	nbr	 3 sep 2002 - allow user input of root name for saving movie as gifs
;	nbr	24 sep 2003 - Add RECTIFIED keyword; add rect to moviev and saving mvis
;	nbr	26 Sep 2003 - Add /DORECTIFY, /CENRECTIFY
;	nbr 	29 Sep 2003 - Add RECTIFIED to scc_wrunmoviem call
;	nbr	 1 Oct 2003 - Fix rect=0
;	nbr 	 6 Feb 2004 - Save some keywords via common block
;				- IF 24-bit display, loadct,0 after loading mvi
;				- use ftvread.pro if saving 1 GIF frame
;	nbr	12 Feb 2004 - Fix START; add dolimb to COMMON block
;	nbr	26 Feb 2004 - move loadct,0 for 24-bit display
;       KB      Aug 19 2004 - Fix "LOAD" bug 
;	rah	Sep 16 2004 - Add capability for true color images
;       aee     Jun 15 2006 - Added coed to handle SECCHI headers
;       aee     Jun 22 2006 - coorected non-secchi date-obs bug.
;       aee     Sep    2006 - Added more SECCHI related code.
;
; Version     : 
;       @(#)scc_wrunmovie.pro	1.27, 09/23/04 : NRL LASCO LIBRARY
;
; See Also    : MKMOVIE.PRO
;
;-            
;$Log: scc_wrunmovie.pro,v $
;Revision 1.54  2011/05/18 22:25:10  nathan
;obsolete message and return
;
;Revision 1.53  2009/09/24 19:34:49  mcnutt
;update Common block WSCC_MKMOVIE_COMMON to include textframe used in annotate_image and added annotate button
;
;Revision 1.52  2009/09/21 19:04:20  mcnutt
;changed plate scale slider to CW_fslider to allow a float value
;
;Revision 1.51  2009/08/21 17:40:51  mcnutt
;does not write combined movie if canceled in scc_movieout
;
;Revision 1.50  2009/07/29 16:48:19  mcnutt
;put back save frame option
;
;Revision 1.49  2009/07/21 21:52:55  nathan
;make sure keywords affecting frame hdrs are acknowledged
;
;Revision 1.48  2009/07/20 22:10:53  nathan
;adjust widget labels
;
;Revision 1.47  2009/07/17 18:54:15  nathan
;use /NOSHELL with spawn command for Windows compatability
;
;Revision 1.46  2009/06/25 17:31:59  mcnutt
;changed call to scc_movieout
;
;Revision 1.45  2009/04/10 18:27:02  mcnutt
;does not set filename to default if movie is not read in set it to default when saving movie
;
;Revision 1.44  2009/04/10 15:41:49  mcnutt
;calls scc_movieout to define output movie name for wscc_combine_mvi.pro
;
;Revision 1.43  2009/04/06 18:39:49  mcnutt
;writes and reads in both gif and png movies files
;
;Revision 1.42  2009/04/02 18:08:54  mcnutt
;calls pngmvi_select with gif keyword when saving gif files
;
;Revision 1.41  2009/03/25 17:26:57  nathan
;Do not pop any visible windows if SAVE= is set
;
;Revision 1.40  2009/03/12 14:06:44  mcnutt
;added PNG save button for wscc_combine_mvi.pro
;
;Revision 1.39  2009/03/11 18:33:29  mcnutt
;sets sinlge to 0 if swmovie is not defined
;
;Revision 1.38  2009/03/10 18:53:09  mcnutt
;works with a single image movie
;
;Revision 1.37  2009/03/03 22:01:15  nathan
;undefine ccd_pos if CCDPOS keyword not set
;
;Revision 1.36  2009/02/18 20:20:19  nathan
;Support DirectColor graphics by calling XLOADCT if imgs is not 3-d
;
;Revision 1.35  2009/01/14 14:09:45  mcnutt
;call to scc_wrunmoviem now works file_hdr is defined at start up
;
;Revision 1.34  2009/01/13 12:23:43  mcnutt
;added ccdpos keyword used when called from wscc_combine_mvi
;
;Revision 1.33  2008/12/19 22:56:28  nathan
;adjusted meaning and computation of lastframe
;
;Revision 1.32  2008/11/05 17:21:12  thompson
;Use select_windows instead of set_plot for cross-platform compatibility
;
;Revision 1.31  2008/08/20 13:36:43  mcnutt
;added mask options for hi2 when called from wscc_combine_mvi
;
;Revision 1.30  2008/08/20 12:24:59  mcnutt
;passes file_hdr to scc_wrunmoviem
;
;Revision 1.29  2008/08/14 18:37:56  mcnutt
;added buttom for wscc_combine_mvi char size
;
;Revision 1.28  2008/08/14 14:24:24  mcnutt
;corrected cmbscl value when scaling a combine_mvi
;
;Revision 1.27  2008/08/13 17:17:06  mcnutt
;reads and displays truecolor movies
;
;Revision 1.26  2008/08/13 16:13:03  mcnutt
;change scale to match wscc_combine_mvi
;
;Revision 1.25  2008/08/12 21:24:20  nathan
;adjust scrollbar-window sizes
;
;Revision 1.24  2008/08/08 20:40:12  nathan
;Added message for deleted frame
;
;Revision 1.23  2008/08/07 17:46:34  mcnutt
;made change to combine zoom to allow center adjustments
;
;Revision 1.22  2008/08/06 21:25:43  nathan
;fix bug in save_mvi when not using fits files
;
;Revision 1.21  2008/08/05 19:03:28  mcnutt
;added mask variable for combine mvi
;
;Revision 1.20  2008/08/04 17:52:22  mcnutt
;started to add combine_mvi buttons when called from wscc_combine_mvi
;
;Revision 1.19  2008/07/24 14:47:50  nathan
;change stop to message in scc_save_mvi
;
;Revision 1.18  2008/06/25 18:31:02  mcnutt
;calculates the positions of earth soho and other stereo sc
;
;Revision 1.17  2008/06/19 21:49:09  nathan
;allow saving appended movies (store all frames in imgs)
;
;Revision 1.16  2008/06/18 14:58:02  nathan
;For saving current frame as gif, use pixmap not draw_win. Save mvi as gifs used
;pixmap and E.Robbrecht said these were better. (Slight difference in color table?)
;
;Revision 1.15  2008/05/27 19:04:04  mcnutt
;modified times
;
;Revision 1.14  2008/05/22 17:05:37  nathan
;replaced xcolors call with sxcolors
;
;Revision 1.13  2008/05/20 19:14:58  nathan
;extend WSCC_MKMOVIE_COMMON
;
;Revision 1.12  2008/05/14 15:31:17  mcnutt
;corrected dotimes error now defined even when times keyword not set
;
;Revision 1.11  2008/05/13 13:16:24  mcnutt
;moves time stap variables to sccMVI_COMMON
;
;Revision 1.10  2008/05/12 17:54:39  mcnutt
;replaces times when color table is loaded and keyword set times
;
;Revision 1.9  2008/05/09 16:10:22  mcnutt
;change menu window location and call to scc_wrunmoviem
;
;Revision 1.8  2008/04/15 23:06:15  nathan
;Only save pixmaps as TrueColor if /TRUECOLOR set
;
;Revision 1.7  2008/04/15 20:35:45  nathan
;Removed imgs from sccmvi_common; added pro call_xcolors; put mvi save into
;sub-pro scc_save_mvi; added /NOXCOLORS option; renamed read_mvi output
;imgsptr; increase threshold for scroll-bars
;
;Revision 1.6  2008/04/08 16:36:00  nathan
;update scc_wrunmovie already running message
;
;Revision 1.5  2008/02/05 17:18:25  nathan
;rename common block to not conflict with LASCO wrunmovie
;
;Revision 1.4  2008/01/11 16:33:31  nathan
;Permanently moved from dev/movie to secchi/idl/display/movie
;
;Revision 1.11  2007/12/21 17:31:44  nathan
;fixed save-as-gifs filenames
;
;Revision 1.10  2007/12/21 15:31:00  nathan
;use ftvread instead of write_gif for GIF output
;
;Revision 1.9  2007/12/10 22:29:25  nathan
;added COMMON MVI_COMMON to all subpros (for debugon)
;
;Revision 1.8  2007/10/19 15:08:25  nathan
;improved handling of xcen/ycen/secs/solr; updated /LOGO option
;
;Revision 1.7  2007/10/17 21:47:28  nathan
;change debug messages
;
;Revision 1.6  2007/10/17 18:48:42  nathan
;For normal SAVE to MVI, dispense with creation of a dummy FITS header
;before passing to sccwrite_disk_movie; instead just use moviev.img_hdrs(i);
;Added /DEBUG. Still need to update other SAVE methods. Just tested with
;COR2 and HI.
;
;Revision 1.5  2007/08/30 21:33:04  nathan
;expand framename window
;
;Revision 1.4  2007/08/17 16:06:02  herbst
;Mods for N.Sheeley Summer 2007
;
;Revision 1.3  2007/08/17 15:57:51  herbst
;Mods for N.Sheeley Summer 2007
;
;Revision 1.2  2007/03/28 19:41:00  esfand
;secchi movie routines as of Mar 28, 07
;
;Revision 1.1  2006/10/10 20:28:56  esfand
;for dual SECCHI/LASCO use
;
;
;____________________________________________________________________________
;
PRO SCC_WRUNMOVIE_DRAW, ev

;COMMON WIN_INDEX, base
COMMON sccMVI_COMMON, base, nskip, dotimes, dostart, mlength, dorebin, dodiff, $
		dordiff, docoords, dofixgaps, dif_min0, dif_max0, dofull, dolimb, $
		debugon , yout, xout, charsz, NO_CAM, ccd_pos, pngbase 
 
common SCC_COMBINE, cmbmovie,cmbstate,struct ;needs to matche definition in wscc_combine_mvi.pro

   WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

   IF (ev.press GT 0) THEN RETURN 		;** only look at PRESS events
   if (moviev.cmbvals(0) eq 0) then begin
     IF (moviev.showmenu EQ 0) THEN BEGIN
        WIDGET_CONTROL, moviev.base, MAP=1
        moviev.showmenu = 1
     ENDIF ELSE BEGIN
        WIDGET_CONTROL, moviev.base, MAP=0
        moviev.showmenu = 0
     ENDELSE
   endif
   if (moviev.cmbvals(0) eq 3) then begin ; center zoom windows
       WSET, moviev.draw_win
       DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
       xcen=moviev.cmbvals(1)+(moviev.cmbvals(3)-moviev.cmbvals(1))/2
       ycen=moviev.cmbvals(2)+(moviev.cmbvals(4)-moviev.cmbvals(2))/2
       if ev.x le moviev.cmbvals(6) then begin   ;left side
           xadj=ev.x-xcen
           yadj=ev.y-ycen
           moviev.cmbvals(1)=moviev.cmbvals(1)+xadj
           moviev.cmbvals(2)=moviev.cmbvals(2)+yadj
           moviev.cmbvals(3)=moviev.cmbvals(3)+xadj
           moviev.cmbvals(4)=moviev.cmbvals(4)+yadj
           moviev.cmbvals(8)=moviev.cmbvals(8)-xadj   ; don't move right side when recentering left should not effect same sun center movies.
           moviev.cmbvals(9)=moviev.cmbvals(9)-yadj
       endif
      DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1), moviev.cmbvals(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1), moviev.cmbvals(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(1), moviev.cmbvals(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(3), moviev.cmbvals(2), 11] ;right side
      if moviev.hsize gt moviev.cmbvals(6) then begin ;right side
         if ev.x gt moviev.cmbvals(3) then begin 
           moviev.cmbvals(8)=(ev.x-moviev.cmbvals(6)+1)-xcen
           moviev.cmbvals(9)=ev.y-ycen
         endif
         DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(2)+moviev.cmbvals(9), 11] ;bottom line
         DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(4)+moviev.cmbvals(9), 11] ;top line
         DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(1)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(2)+moviev.cmbvals(9), 11] ;leftside 
         DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(3)+moviev.cmbvals(6)+1+moviev.cmbvals(8), moviev.cmbvals(2)+moviev.cmbvals(9), 11] ;right side
      endif
   
   if moviev.hsize gt moviev.cmbvals(6)+1 then insttxt='('+string(moviev.cmbvals(1),'(i4)')+','+string(moviev.cmbvals(2),'(i4)')+')('+$
      string(moviev.cmbvals(3),'(i4)')+','+string(moviev.cmbvals(4),'(i4)')+$
      ') Roff='+string(moviev.cmbvals(8),'(i4)')+','+string(moviev.cmbvals(9),'(i4)') else $
      insttxt='('+string(moviev.cmbvals(1),'(i4)')+','+string(moviev.cmbvals(2),'(i4)')+')('+$
      string(moviev.cmbvals(3),'(i4)')+','+string(moviev.cmbvals(4),'(i4)')+')'
      widget_control,moviev.cropinst,set_value=insttxt


   endif

   if (moviev.cmbvals(0) eq 2) then begin ; first corner selected
      if ev.x gt moviev.cmbvals(6) then evx=moviev.cmbvals(6) else evx=ev.x
      if ev.y gt moviev.cmbvals(7) then evy=moviev.cmbvals(7) else evy=ev.y
      moviev.cmbvals(3)=evx
      moviev.cmbvals(4)=evy
      moviev.cmbvals(0)=3 ;set to 3 when rxycen get defined.
      if moviev.cmbvals(1) gt moviev.cmbvals(3) then begin
          tmp=moviev.cmbvals(3) & moviev.cmbvals(3) = moviev.cmbvals(1) & moviev.cmbvals(1)=tmp
      endif
      if moviev.cmbvals(2) gt moviev.cmbvals(4) then begin
          tmp=moviev.cmbvals(4) & moviev.cmbvals(4) = moviev.cmbvals(2) & moviev.cmbvals(2)=tmp
      endif
      DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1), moviev.cmbvals(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1), moviev.cmbvals(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(1), moviev.cmbvals(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(3), moviev.cmbvals(2), 11] ;right side
      if moviev.hsize gt moviev.cmbvals(6)+1 then begin
        DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1)+moviev.cmbvals(6)+1, moviev.cmbvals(2), 11] ;bottom line
        DEVICE, COPY = [0, 0, moviev.cmbvals(3)-moviev.cmbvals(1), 2,moviev.cmbvals(1)+moviev.cmbvals(6)+1, moviev.cmbvals(4), 11] ;top line
        DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(1)+moviev.cmbvals(6)+1, moviev.cmbvals(2), 11] ;leftside 
        DEVICE, COPY = [0, 0, 2, moviev.cmbvals(4)-moviev.cmbvals(2), moviev.cmbvals(3)+moviev.cmbvals(6)+1, moviev.cmbvals(2), 11] ;right side
      endif
      insttxt='click on image to recenter crop position'
      widget_control,moviev.cropinst,set_value=insttxt
   endif
   if (moviev.cmbvals(0) eq 1) then begin ; first corner selected
      if ev.x gt moviev.cmbvals(6) then evx=moviev.cmbvals(6) else evx=ev.x
      if ev.y gt moviev.cmbvals(7) then evy=moviev.cmbvals(7) else evy=ev.y
      moviev.cmbvals(1)=evx
      moviev.cmbvals(2)=evy
      moviev.cmbvals(0)=2
      widget_control,moviev.cropinst,set_value='Select upper right corner'
   endif


  WIDGET_CONTROL, base, SET_UVALUE=moviev

END

;----------------adjust color-------------------------------

PRO call_xcolors, ev

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
COMMON sccMVI_COMMON

WIDGET_CONTROL,ev.top, Get_UValue=moviev
WIDGET_CONTROL, moviev.base, GET_UVALUE=moviev

;input = ev.value
szi=size(imgs)
IF szi[0] LT 3 THEN BEGIN
    ;help,/device
    print,''
    print,'In order for XCOLORS to work with Visual Class: TrueColor,'
    print,'GENERIC_MOVIE must be called with /DOXCOLORS.'
    xloadct
ENDIF ELSE BEGIN
    ;WIDGET_CONTROL, ev.id, GET_UVALUE=test
    ;IF (DATATYPE(test) EQ 'STR') THEN input = test
    input = Tag_Names(ev, /Structure_Name)
    ;input = ev.value
    IF debug_on THEN print,"CALL_XCOLORS: " + input
    ;help,/str,ev
    ;stop
    CASE (input) OF

	'WIDGET_BUTTON': BEGIN
   	    ;window,1,xsize=512,ysize=512
      	    sXColors, Group_Leader=ev.top, NotifyID=[ev.id,ev.top], /DRAG
	END
	'XCOLORS_LOAD': tv,imgs[*,*,moviev.current]
    	    ; update current window only

	'XCOLORS_ACCEPT': BEGIN
    	    ; update all windows
	  ;Device, Get_Visual_Depth=thisDepth
	  ;IF thisDepth GT 8 THEN BEGIN
 	    winsave = !d.window
	    nim = n_elements(moviev.win_index)
	    for i = 0, nim-1 do begin
		wset,moviev.win_index[i]
		tv, imgs[*,*,i] ; to match scc_wrunmoviem images array
        	ahdr=moviev.img_hdrs(i)
    ;	    IF dotimes gt 0 THEN BEGIN
    ;	       IF NO_CAM gt 0 THEN label = ahdr.date_obs + ' ' + STRMID(ahdr.DATE_OBS,0,5) ELSE $
    ;	          label = ahdr.detector+'  '+ahdr.DATE_OBS + ' ' + STRMID(ahdr.time_obs,0,5)
    ;	       IF dotimes GT 1 THEN time_color=dotimes ELSE time_color=255
    ;               XYOUTS, xout, yout, label, CHARSIZE=charsz, /DEVICE, COLOR=time_color
    ;	    ENDIF

	    endfor
	    ;image = tvrd(true=1)
	    wset,moviev.draw_win
	    DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
	    WSET,winsave
	  ;ENDIF
	END
    ENDCASE
ENDELSE
Widget_Control, ev.top, Set_UValue=moviev, /No_Copy

END

PRO scc_save_mvi, moviev

common sccmvi_common
COMMON WSCC_MKMOVIE_COMMON

first = 1
maxnum = N_ELEMENTS(WHERE(moviev.deleted NE 1))
truecolor = moviev.truecolor
FOR i=0, moviev.len-1 DO BEGIN
;;                    IF (NOT(moviev.deleted(i))) THEN BEGIN
    deleted=moviev.deleted[i]
    IF (debugon) THEN help,deleted
    IF (NOT(deleted) AND (i GE moviev.first) AND (i LE moviev.last)) THEN BEGIN
	PRINT, '%%SCC_WRUNMOVIE saving frame ', STRING(i,FORMAT='(I4)'), $
              ' to movie file ', moviev.filename

	xcen= moviev.xcen 
	ycen= moviev.ycen 
	arcs= moviev.secs
	rsun= moviev.rsun
	  ;IF rsun EQ 0.0 THEN BEGIN
	  ;  hee_r= GET_STEREO_LONLAT(moviev.img_hdrs(i).date_obs, $
	  ;         STRMID(moviev.img_hdrs(i).obsrvtry,7,1),/au,   $
	  ;         /degrees, system='HEE',_extra=_extra) 
            ; convert from radian to arcs: 
	  ;  rsun = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0)
	  ;ENDIF

	  ;scl = float(moviev.img_hdrs[i].NAXIS1) / moviev.hsize
	  ;IF(scl EQ 0.) THEN BEGIN
	;	scl = 1.
	;	print, '%SCC_WRUNMOVIE: Could not determine scaling'
	;  ENDIF
	  ;rsun = rsun * scl
	  ;xcen = xcen * scl
	  ;ycen = ycen * scl
	;  arcs = arcs * scl * float(ccd_pos[2]-ccd_pos[0]) / 2048.0
	;###
	; ccd_pos is defined in sccwrite_disk_movie.pro 
	; I don't think scale can be accurately determined from window size and image size
	; nbr, 10/17/07
	;###

    	szi=size(imgs)
	
    	IF szi[0] LT 3 THEN BEGIN
	    
	    WSET, moviev.win_index(i)
	    ;img=ftvread() 
    	    IF (truecolor EQ 0)  THEN $
            	img = tvrd(0,0,moviev.hsize, moviev.vsize) $
            ELSE $
            	img = TVRD(0,0,moviev.hsize, moviev.vsize, TRUE=truecolor)
	ENDIF else begin
      	  IF (truecolor EQ 0)  THEN img=reform(imgs[*,*,i]) else img=reform(imgs[*,*,*,i])
        ENDELSE
	    
	IF debugon THEN help,xcen,ycen,arcs,rsun,img,i
	IF debugon THEN wait,2
	IF moviev.img_hdrs(i).filename EQ '' THEN moviev.img_hdrs(i).filename=moviev.frames(i)
	IF moviev.img_hdrs(i).filename EQ '' THEN message,'Error: filename for frame undefined.'
        IF (first EQ 1) THEN BEGIN
	  ;IF debugon THEN help,/str,moviev.img_hdrs(i)
	  SCCWRITE_DISK_MOVIE, moviev.filename, img, moviev.img_hdrs(i), /NEW, MAXNUM=moviev.len, $
             SUNXCEN=xcen, SUNYCEN=ycen, SEC_PIX=arcs, R_SUN= rsun, $
	     RECTIFIED=moviev.rect,truecolor=truecolor, DEBUG=debugon,ccd_pos=ccd_pos
	; make sure if using ROLL keyword it is not overriding individual frame roll
	  first = 0
       ENDIF ELSE BEGIN
	  ;WRITE_DISK_MOVIE, moviev.filename, img, hdr,truecolor=truecolor
	  SCCWRITE_DISK_MOVIE, moviev.filename, img, moviev.img_hdrs(i), $
	  SUNXCEN=xcen, SUNYCEN=ycen, SEC_PIX=arcs, R_SUN= rsun, $
	  TRUECOLOR=truecolor, DEBUG=debugon,ccd_pos=ccd_pos
       ENDELSE
    ENDIF
ENDFOR
end ; pro scc_mvi_save

;------------------ runmovie event -------------------------------
PRO SCC_WRUNMOVIE_EVENT, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON sccMVI_COMMON
COMMON scc_combine

WIDGET_CONTROL, ev.top, GET_UVALUE=moviev   ; get structure from UVALUE

 IF (ev.id EQ ev.handler) THEN BEGIN   ; A top level base timer event
    IF (moviev.forward) THEN BEGIN		;** going forward
        IF ((moviev.bounce) AND (moviev.current EQ moviev.last)) THEN BEGIN
            moviev.current = moviev.last-1
	    moviev.pause = 1
	    moviev.forward = 0		;** set direction to reverse
        ENDIF $
        ELSE BEGIN
            moviev.current = (moviev.current + moviev.nskip + 1) MOD moviev.len
	    IF ((moviev.current EQ 0) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
            IF ((moviev.current GT moviev.last) OR (moviev.current LT moviev.first)) THEN BEGIN
	   	moviev.pause = 1	
		moviev.current = moviev.first
	    ENDIF
        ENDELSE
    ENDIF $
    ELSE BEGIN			;** going in reverse
        IF ((moviev.bounce) AND (moviev.current EQ moviev.first)) THEN BEGIN
            moviev.current = moviev.first+1
	    moviev.pause = 1
	    moviev.forward = 1		;** set direction to forward
        ENDIF $
        ELSE BEGIN
            moviev.current = (moviev.current - 1) MOD moviev.len
	    IF ((moviev.current EQ moviev.last) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
            IF ((moviev.current LT moviev.first) OR (moviev.current GT moviev.last)) THEN BEGIN
		moviev.current = moviev.last
		moviev.pause = 1
	    ENDIF
        ENDELSE
    ENDELSE

    IF (moviev.pause AND moviev.dopause) THEN BEGIN
	moviev.pause = 0
	WAIT, .75
    ENDIF

    ;** out of range error checks (to stop bombs from COPY)
    IF (moviev.current LT 0) THEN moviev.current = moviev.first
    IF (moviev.current GE moviev.len) THEN moviev.current = moviev.last
    IF (NOT(moviev.deleted(moviev.current)) AND ((moviev.current MOD (moviev.nskip+1)) EQ 0) ) THEN BEGIN		;** if not deleted then display
       WSET, moviev.draw_win
       DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
       
       WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
       WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current), /REMOVE_ALL)
       if moviev.sliden ne -1 then WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
       WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
    ENDIF ELSE $	;** else immediately look for the next non-deleted frame
      WIDGET_CONTROL, moviev.base, TIMER=0.0 ;.000001

 
 ENDIF ELSE BEGIN	;** event other than timer
 
    IF (TAG_EXIST(ev, 'value')) THEN BEGIN
    IF (DATATYPE(ev.value) EQ 'STR') THEN BEGIN
	; nbr, 04.02.26 - for 24-bit displays
  	device,get_visual_depth=depth
   	;IF depth GT 8 THEN loadct,0

      CASE (ev.value) OF

	'PS-Rescale' : BEGIN 	;save ps image with rescaling
                BREAK_FILE, moviev.filename, a, dir, name, ext
                if name eq '' then name='default'
                filename = name + '.ps'
                filename = PICKFILE(file=filename, filter='*.ps', $
                                   TITLE='Enter name of postscript file to save', $
                                   PATH=moviev.filepath,GET_PATH=path)
                IF (filename EQ '') THEN RETURN
                BREAK_FILE, filename, a, dir, name, ext
                IF (dir EQ '') THEN filename=path+filename
                BREAK_FILE, filename, a, dir, name, ext
                moviev.filepath=dir
                moviev.filename=moviev.filepath+name+ext
		WSET, moviev.draw_win
		WIN2PS, moviev.filename,rescale=0
	    END
	'Postscript' : BEGIN 	;save ps image
                BREAK_FILE, moviev.filename, a, dir, name, ext
                if name eq '' then name='default'
                filename = name + '.ps'
                filename = PICKFILE(file=filename, filter='*.ps', $
                                   TITLE='Enter name of postscript file to save', $
                                   PATH=moviev.filepath,GET_PATH=path)
                IF (filename EQ '') THEN RETURN
                BREAK_FILE, filename, a, dir, name, ext
                IF (dir EQ '') THEN filename=path+filename
                BREAK_FILE, filename, a, dir, name, ext
                moviev.filepath=dir
                moviev.filename=moviev.filepath+name+ext
		WSET, moviev.draw_win
		WIN2PS, moviev.filename,/rescale
	    END

	'   gif    ' : BEGIN 	;save gif image
                BREAK_FILE, moviev.filename, a, dir, name, ext
                ;filename = name + '.gif'
                ;filename = PICKFILE(file=filename, filter='*.gif', $
                ;                   TITLE='Enter name of gif file to save', $
                ;                   PATH=moviev.filepath,GET_PATH=path)
                ;IF (filename EQ '') THEN RETURN 
                ;BREAK_FILE, filename, a, dir, name, ext
                ;IF (dir EQ '') THEN filename=path+filename
                ;BREAK_FILE, filename, a, dir, name, ext
                ;moviev.filepath=dir
                ;moviev.filename=moviev.filepath+name+ext
                ;IF (CHECK_PERMISSION(moviev.filename) EQ 0) THEN RETURN
		
		;WSET, moviev.draw_win
		wset, moviev.win_index[moviev.current]
		
		;TVLCT, r, g, b, /GET
		PRINT, '%%SCC_WRUNMOVIE: Saving window to gif file: '+ moviev.filename
		;PRINT, '%%SCC_WRUNMOVIE: Use mvi2frames.pro to save entire mvi to individual frames.'
		;WRITE_GIF, moviev.filename, TVRD(), r, g, b
		
		; ** NOTE : This should give color images, even if loadct,0 has run
		; because pixmaps were generated before loadct was called.
		junk=fTVREAD(/gif, filename=name)
	    END

       ENDCASE

       ENDIF ELSE BEGIN
                        
    		WIDGET_CONTROL, ev.id, GET_UVALUE=input

    		CASE (input) OF

        		'DRAW' : BEGIN	;image window
	    		END

			'SPEED' : BEGIN 	;speed
				IF (ev.value EQ 0) THEN $
		                   moviev.stall = 5. $
		                ELSE $
				   moviev.stall = (100 - ev.value)/50.
		                moviev.stallsave = moviev.stall
		                WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
			    END
		
		        'SLIDE_FIRST' : BEGIN	;first frame
				 moviev.first = ev.value
			     END
		
		        'SLIDE_LAST' : BEGIN	;last frame
				 moviev.last = ev.value
			     END
		
		        'SLIDE_SKIP' : BEGIN	;skip frame
				 moviev.nskip = ev.value
			     END

        'SLIDE_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.sliden, GET_VALUE=val
                val = FIX(val(0))
                IF (NOT(moviev.deleted(val)) AND (val GE moviev.first) AND (val LE moviev.last)) THEN BEGIN
                moviev.current = val
                DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                ENDIF
                WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                moviev.stall = 10000.
          END
		
	'cmbzoomsel' : BEGIN	;select zoom coords using any mouse
                 WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                 moviev.cmbvals(5)=float(string(val,'(f6.2)'))
                 WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
          END

          'mcor2av':cmbstate.outer(2)=ev.value
          'mcor1av':cmbstate.outer(3)=ev.value
          'meuviav':cmbstate.outer(4)=ev.value
          'meuvibv':cmbstate.outer(5)=ev.value
          'mcor1bv':cmbstate.outer(6)=ev.value
          'mcor2bv':cmbstate.outer(7)=ev.value
          'mhi2av':cmbstate.outer(0)=ev.value
          'mhi2bv':cmbstate.outer(9)=ev.value
          'mcharsv':moviev.cmbvals(11)=ev.value*10


			ELSE : BEGIN
		                 PRINT, '%%SCC_WRUNMOVIE_EVENT.  Unknown event.'
			     END

		ENDCASE

      ENDELSE
      ENDIF ELSE BEGIN
              WIDGET_CONTROL, ev.id, GET_UVALUE=input

              CASE (input) OF

        'FRAME_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.cframe, GET_VALUE=val
                val = 0 > FIX(val(0)) < moviev.last
                moviev.current=val
                DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
          END
      ELSE : BEGIN
                 PRINT, '%%RTMVIPLAY_EVENT.  Unknown event.'
           END
      ENDCASE

      ENDELSE


   ENDELSE

  WIDGET_CONTROL, ev.top, SET_UVALUE=moviev

END ;------------------ end runmovie_event -------------------------


;------------------ runmovie event for CW_BGROUP buttons ---------

FUNCTION SCC_WRUNMOVIE_BGROUP, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_combine

   WIDGET_CONTROL, ev.top, GET_UVALUE=moviev   ; get structure from UVALUE

   input = ev.value

    CASE (input) OF

	'DONE' : BEGIN       ;** exit program
	         WIDGET_CONTROL, /DESTROY, moviev.base
	         WIDGET_CONTROL, /DESTROY, moviev.winbase
	         FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
                 RETURN, 0
	     END

	;'ADJCT' : BEGIN	;color table adjustment
	;	 XLOADCT
	;    END

	'DELETE' : BEGIN	;delete current frame
	    	 message,'Deleting frame '+trim(moviev.current),/info
                 WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
		 moviev.deleted(moviev.current) = 1
	    END

	'WRUNMOVIEM' : BEGIN	;call scc_wrunmoviem

                 tmp = INDGEN(N_ELEMENTS(moviev.win_index))
                 good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
		 movie_in=moviev.win_index(good) 
		 HDRS=moviev.img_hdrs(good) 
		 NAMES=moviev.frames(good)
                 SUNXCEN=moviev.xcen 
		 SUNYCEN=moviev.ycen 
		 SEC_PIX=moviev.secs
                 RECTIFIED=moviev.rect
                 filename=moviev.filename
                 FILE_HDR=moviev.FILE_HDR
	         WIDGET_CONTROL, /DESTROY, moviev.base
	         WIDGET_CONTROL, /DESTROY, moviev.winbase
	         ;FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
                 ;RETURN, 0
                 SCC_WRUNMOVIEM, movie_in, HDRS=HDRS, NAMES=NAMES, $
                    file_hdr=file_hdr,filename=filename,/editk

                 RETURN,0
	    END

	'cmbcrop' : BEGIN	;select zoom coords using any mouse
                      WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                      moviev.stall = 10000.
		      moviev.cmbvals(0)=1 ;zoom selected
                      if moviev.hsize gt moviev.cmbvals(6)+1 then inst='select lower right corner on left image' else inst='select lower left corner'
                      widget_control,moviev.cropinst,set_value=inst
	    END
	'cmbcancel' : BEGIN
		      moviev.cmbvals(0)=0 ;zoom unselected
	    	      moviev.cmbvals(0:5)=0
	    	      moviev.cmbvals(8:9)=0
                      WSET, moviev.draw_win
                      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                      widget_control,moviev.cropinst,set_value='Crop is not selected'
	    END
	'cmbreset' : BEGIN	;select zoom coords using any mouse
	    	    moviev.cmbvals(0:5)=0
	    	    moviev.cmbvals(8:9)=0
	    	    moviev.cmbvals(10)=1
	    END

         'CMBPREVIEW' : BEGIN
                        if moviev.cmbvals(5) gt 0 then begin
                           WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                           if moviev.cmbvals(5) ne val then begin
                              moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			      WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                           endif
			   cmbscl=moviev.cmbvals(5) 
		        endif else cmbscl=0
	 
	    	    WIDGET_CONTROL, /DESTROY, moviev.base
	    	    WIDGET_CONTROL, /DESTROY, moviev.winbase
                    if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                    if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                    if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                    moviev.cmbvals(*)=0
 		    SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=moviev.truecolor,/preview,restore=restore,times=times
                    RETURN, 0
	    END
	 
         'CMBMKMOVIE' : BEGIN
                        if moviev.cmbvals(5) gt 0 then begin
                           WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                           if moviev.cmbvals(5) ne val then begin
                              moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			      WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                           endif
			   cmbscl=moviev.cmbvals(5) 
		        endif else cmbscl=0

	    	    WIDGET_CONTROL, /DESTROY, moviev.base
	    	    WIDGET_CONTROL, /DESTROY, moviev.winbase
                    if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                    if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                    if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                    moviev.cmbvals(*)=0
		    SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=moviev.truecolor,restore=restore,times=times
                    RETURN, 0
	    END
         'CMBSVMOVIE' : BEGIN
	 
                      if moviev.cmbvals(5) gt 0 then begin
                           WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                           if moviev.cmbvals(5) ne val then begin
                              moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			      WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                           endif
			   cmbscl=moviev.cmbvals(5) 
		        endif else cmbscl=0
  
                    WIDGET_CONTROL, moviev.base, GET_UVALUE=moviev
                    ofn=moviev.filename
                    ofp=moviev.filepath
                    scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=ofn,len=moviev.len,first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect,/selectonly;scc_movieout,moviev.base ,/selectonly
                    IF (ofn ne moviev.filename and ofn ne '') THEN BEGIN
                        moviev.filename=ofn
                        BREAK_FILE, ofn, a, dir1, name, ext
		        spawn,['mkdir',dir1],/NOSHELL
	    	        WIDGET_CONTROL, /DESTROY, moviev.base
	    	        WIDGET_CONTROL, /DESTROY, moviev.winbase
                        if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                        if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                        if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                        moviev.cmbvals(*)=0
		        SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=moviev.truecolor,restore=restore,times=times,outmovie=moviev.filename
                        RETURN, 0
                     ENDIF else begin
		         print,'******************************************'
		         print,'*****  Movie filename not selected   *****'
		         print,'******************************************'
                        ;WIDGET_CONTROL, moviev.base, GET_UVALUE=moviev
                        ;moviev.filename=ofn
                        ;moviev.filepath=ofp
                        ;WIDGET_CONTROL, moviev.base, SET_UVALUE=moviev
                    ENDELSE
	    END
         'CMBWIMGS' : BEGIN
	 
                   if moviev.cmbvals(5) gt 0 then begin
                        WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                        if moviev.cmbvals(5) ne val then begin
                           moviev.cmbvals(5)=float(string(val,'(f6.2)'))
			   WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                        endif
			cmbscl=moviev.cmbvals(5) 
		    endif else cmbscl=0
 	    	    WIDGET_CONTROL, /DESTROY, moviev.base
	    	    WIDGET_CONTROL, /DESTROY, moviev.winbase
                    if moviev.cmbvals(1) gt 0 then cropxy= [moviev.cmbvals(1),moviev.cmbvals(2),moviev.cmbvals(3),moviev.cmbvals(4),moviev.cmbvals(8),moviev.cmbvals(9)] else cropxy=0
                    if moviev.cmbvals(10) eq 1 then restore=1 else restore=0
                    if moviev.cmbvals(11) gt 0 then times=moviev.cmbvals(11)/10. else times=0
                    moviev.cmbvals(*)=0
		    SCC_COMBINE_MVI,cropxy=cropxy,pixscale=cmbscl,truecolor=moviev.truecolor,restore=restore,times=times,/defimgs
                    RETURN, 0
	    END
         'CMBSETUP' : BEGIN
	 
	    	    WIDGET_CONTROL, /DESTROY, moviev.base
	    	    WIDGET_CONTROL, /DESTROY, moviev.winbase
		    WSCC_COMBINE_MVI
                    RETURN, 0
	    END

         'CMBADDTEXT' : BEGIN

                     annotate_image,moviev.win_index,/template,true=moviev.truecolor

	    END
         'ANNOTATE' : BEGIN

                     annotate_image,moviev.win_index,/template,true=moviev.truecolor

	    END
         'CMBCLRTEXT' : BEGIN

                     undefine,textframe

	    END

        'NEXT' : IF (ev.select NE 0) THEN BEGIN	;next frame
                 WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                 moviev.stall = 10000.

		 moviev.forward = 1

		 REPEAT BEGIN
		    moviev.current = (moviev.current + 1) MOD moviev.len
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.first
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.first
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                 WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
	     ENDIF
	    
        'PREV' : IF (ev.select NE 0) THEN BEGIN	;previous frame
                 WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                 moviev.stall = 10000.

		 moviev.forward = 0

		 REPEAT BEGIN
		    moviev.current = (moviev.current - 1) MOD moviev.len
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.last
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.last
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
	     ENDIF

        'CONTINUOUS' : IF (ev.select NE 0) THEN BEGIN	;continuous
		 ;** set direction to saved direction
		 moviev.forward = moviev.dirsave
                 moviev.stall = moviev.stallsave
                 WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
	     ENDIF

        'FORWARD' : IF (ev.select NE 0) THEN BEGIN	;forward
		 moviev.bounce = 0
		 moviev.forward = 1
		 moviev.dirsave = 1
	     ENDIF

        'REVERSE' : IF (ev.select NE 0) THEN BEGIN	;reverse
		 moviev.bounce = 0
		 moviev.forward = 0
		 moviev.dirsave = 0
	     ENDIF

        'BOUNCE' : IF (ev.select NE 0) THEN BEGIN	;bounce
		 moviev.bounce = 1
	     ENDIF

        'PAUSE' : IF (ev.select NE 0) THEN BEGIN	;pause
		 moviev.dopause = 1
	     ENDIF

        'NO_PAUSE' : IF (ev.select NE 0) THEN BEGIN	;no pause
		 moviev.dopause = 0
	     ENDIF

        'LOAD' : BEGIN	;** load movie from file
                 win_index = PICKFILE(filter='*.mvi',file=moviev.filename,/MUST_EXIST, $
                                      TITLE='Select movie file to load', $
                                      PATH=moviev.filepath, GET_PATH=path)
                 IF ((win_index EQ '') OR (win_index EQ moviev.filename)) THEN RETURN, 0
                 BREAK_FILE, win_index, a, dir, name, ext
                 moviev.filepath = dir
                 IF (dir EQ '') THEN BEGIN
                    moviev.filepath = './'
                    win_index = path+win_index
                 ENDIF
	         WIDGET_CONTROL, /DESTROY, moviev.base
	         WIDGET_CONTROL, /DESTROY, moviev.winbase
                 FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
                 SCC_WRUNMOVIE, win_index, /LOAD	
                 RETURN, 0
	     END

	'SAVEMV': BEGIN 	;save to .mvi file
                  scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=moviev.filename,len=moviev.len,first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect;  scc_movieout,base
	    END


	ELSE : BEGIN
                 PRINT, '%%SCC_WRUNMOVIE_BGROUP.  Unknown event.'
	     END

    ENDCASE

    WIDGET_CONTROL, ev.top, SET_UVALUE=moviev

    RETURN, 0

END



;----------------------- scc_wrunmovie ---------------------------------

PRO SCC_WRUNMOVIE, win_indx, win_index2, NAMES=names, HDRS=hdrs, SKIP=skip, TIMES=times, $
    START=start, LENGTH=length, IMG_REBIN=img_rebin, SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, $
    DIFF=diff, SAVE=save, RUNNING_DIFF=running_diff, COORDS=coords, COSMIC=cosmic, FIXGAPS=fixgaps, $
    DRAW_LIMB=draw_limb, BLOCK_SIZE=block_size, LOGO=logo, VIDEO=video, NOCAM=nocam,$
    DIF_MIN=dif_min, DIF_MAX=dif_max, KEEP=keep, FULL=full, LOAD=load, DEBUG=debug, $
    CHSZ=chsz, SPOKE=spoke, R1=r1, R2=r2, TH1=th1, TH2=th2, N_R=n_r, N_TH=n_th, NOXCOLORS=noxcolors, $
    RECTIFIED=rectified, DORECTIFY=dorectify, CENRECTIFY=cenrectify, TRUECOLOR=truecolor, mvicombo=mvicombo,ccdpos=ccdpos
;COMMON WIN_INDEX, base

message,'#######################################################################################',/info
message,'##############    SCC_WRUNMOVIE is obsolete. Please use SCC_PLAYMOVIE    ##############',/info
message,'#######################################################################################',/info

return

COMMON sccMVI_COMMON   ; is defined above
COMMON WSCC_MKMOVIE_COMMON
COMMON scc_combine

if datatype(swmoviev) eq 'UND' then single=0 else single=swmoviev.single

IF keyword_set(ccdpos) then ccd_pos=ccdpos ELSE undefine,ccd_pos
IF keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0
IF debugon THEN debug_on=1 ELSE debug_on=0  ; for common wscc_mkmovie_common

   IF XRegistered("SCC_WRUNMOVIE") THEN BEGIN
      bell = STRING(7B)
      PRINT, bell
      print,'%%There is an instance SCC_WRUNMOVIE already running.  Only one instance may run at a time.'
      print,'%%Recommend either exitting existing wrunmovie session or type WIDGET_CONTROL,/RESET.'
      RETURN
   ENDIF


   IF (N_PARAMS() NE 0) THEN win_index = win_indx
   IF (datatype(truecolor) EQ 'UND')  THEN truecolor=0
   IF datatype(dofull) EQ 'UND' or NOT keyword_set(LOAD) THEN dofull=0
   IF keyword_set(FULL) THEN dofull=1

   ;nskip=0
   stall0 = 95
   stall = (100 - stall0)/50.
if single eq 1 then stall = 10000
   stallsave = stall
   filename = ''
   filepath = './'
   ftitle=''
   rect=0 	; default
   ;set_plot,'x' ;
   select_windows

   IF datatype(nskip) EQ 'UND' or NOT keyword_set(LOAD) THEN nskip=0
   IF KEYWORD_SET(SKIP) THEN nskip = skip
   IF datatype(dolimb) EQ 'UND' or NOT keyword_set(LOAD) THEN dolimb=0
   IF keyword_set(DRAW_LIMB) THEN dolimb=1

   IF KEYWORD_SET(START) THEN dostart=start ELSE start=0
   IF datatype(dostart) EQ 'UND' THEN dostart=0
   IF keyword_set(LOAD) THEN start = dostart

   IF datatype(dif_min0) EQ 'UND' or NOT keyword_set(LOAD) THEN dif_min0=-70
   IF keyword_set(DIF_MIN) THEN dif_min0=dif_min
   IF datatype(dif_max0) EQ 'UND' or NOT keyword_set(LOAD) THEN dif_max0=70
   IF keyword_set(DIF_MAX) THEN dif_max0=dif_max
   dif_min=dif_min0
   dif_max=dif_max0
   
   IF datatype(dotimes) EQ 'UND' or NOT keyword_set(LOAD) THEN dotimes=0
   IF KEYWORD_SET(TIMES) THEN dotimes=times
   IF KEYWORD_SET(NOCAM) THEN NO_CAM=1

   IF KEYWORD_SET(SUNXCEN) THEN xcen = sunxcen ELSE xcen = 0
   IF KEYWORD_SET(SUNYCEN) THEN ycen = sunycen ELSE ycen = 0
   IF KEYWORD_SET(SEC_PIX) THEN secs = sec_pix ELSE secs = 0
   IF KEYWORD_SET(R_SUN)   THEN rsun = r_sun   ELSE rsun= 0

   ;** have user select movie file (.mvi)
   IF (DATATYPE(win_index) EQ 'UND') THEN BEGIN
      file = PICKFILE(filter='*.mvi', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path)
      IF (file EQ '') THEN RETURN
      BREAK_FILE, file, a, dir, name, ext
      IF (dir EQ '') THEN win_index = path+file ELSE win_index = file
   ENDIF

;>>>>>>>>>>** read movie in from movie file (.mvi)
IF (DATATYPE(win_index) EQ 'STR') THEN BEGIN

all_files = win_index

IF datatype(dordiff) EQ 'UND' or NOT keyword_set(LOAD) THEN dordiff=0
IF keyword_set(RUNNING_DIFF) THEN dordiff=running_diff
IF (N_ELEMENTS(all_files) GT 1) AND dordiff THEN BEGIN
   IF (dordiff GT 1) THEN BEGIN
      PRINT, '%%SCC_WRUNMOVIE Unable to difference from ',dordiff,' images  when appending .mvi files'
      RETURN
   ENDIF
ENDIF
IF datatype(dodiff) EQ 'UND' or NOT keyword_set(LOAD) THEN dodiff=0
IF keyword_set(DIFF) THEN dodiff=diff
IF (N_PARAMS() EQ 2) THEN do2=1 ELSE do2=0  	; display 2 movies side by side

IF KEYWORD_SET(COSMIC) THEN BEGIN
   test = CCOSMICS(BYTARR(10,10))
   IF (N_ELEMENTS(test) EQ 1) THEN use_ccosmics=0 ELSE use_ccosmics=1
   IF (use_ccosmics EQ 0) THEN PRINT, '%%SCC_WRUNMOVIE Using POINT_FILTER instead of CCOSMICS for cosmic ray removal.'
ENDIF

ii=0
FOR f=0, N_ELEMENTS(all_files)-1 DO BEGIN
    curr_file = all_files(f)
      BREAK_FILE, curr_file, a, dir, name, ext
      IF (do2 EQ 1) THEN BREAK_FILE, win_index2, a2, dir2, name2, ext2
      ftitle = name+ext
      filename = curr_file
      filepath = dir
      IF (dir EQ '') THEN filepath = './'
      OPENR,lu,filename,/GET_LUN
      SCCREAD_MVI, lu, file_hdr, ihdrs, imgsptr, swapflag

      print,'MVI version ',trim(file_hdr.ver)

    ahdr = SCCMVIHDR2STRUCT(ihdrs(0),file_hdr.ver)
    det=ahdr.detector
    dindx=where(['C2','C3','EIT'] EQ det,issoho)	

    ; do2 means 2 movies side-by-side
      IF (do2 EQ 1) THEN OPENR,lu2,win_index2,/GET_LUN
      IF (do2 EQ 1) THEN SCCREAD_MVI, lu2, file_hdr2, ihdrs2, imgsptr2, swapflag2

    secs = file_hdr.sec_pix
    IF secs LE 0 THEN secs=ahdr.cdelt1
    IF secs LE 0 and issoho THEN secs = GET_SEC_PIXEL(ahdr, FULL=hsize)
    
    solr=ahdr.rsun	    ; no radius in file_hdr
    IF solr EQ 0 and issoho THEN solr = GET_SOLAR_RADIUS(ahdr)
    xcen=file_hdr.sunxcen
    ycen=file_hdr.sunycen
    IF xcen EQ 0 and issoho THEN BEGIN
    	print,'Calculating SOHO sun center for ',ahdr.date_obs,' frame # 0'
    	sunc = GET_SUN_CENTER(ahdr, FULL=hsize,/DOCHECK)
	xcen=sunc.xcen
	ycen=sunc.ycen
    ENDIF
    
; frame header values are over-ridden by keywords
    IF (KEYWORD_SET(SUNXCEN)) THEN xcen = sunxcen
    IF (KEYWORD_SET(SUNYCEN)) THEN ycen = sunycen
    IF (KEYWORD_SET(SEC_PIX)) THEN secs = sec_pix
    
      nx = file_hdr.nx
      ny = file_hdr.ny
      rect=file_hdr.rectified
      rnx = nx
      rny = ny
      truecolor = file_hdr.truecolor
      IF (truecolor) THEN BEGIN
    	noxcolors=1 
	print,'Images are TrueColor; Adjust Color feature not supported.'
    ENDIF
    
      IF datatype(mlength) EQ 'UND' THEN mlength=0   ; KB
      
      ; define lastframe of current MVI file
      IF KEYWORD_SET(LENGTH) THEN BEGIN
      	lastframe=start+length-1
	mlength=length
      ENDIF ELSE $
      	IF keyword_set(LOAD) THEN $
		IF mlength EQ 0 THEN $
			lastframe=file_hdr.nf-1 $
		ELSE lastframe=start+mlength-1 $
	ELSE lastframe=file_hdr.nf-1
      
      lastframe = lastframe<(file_hdr.nf-1)

      IF f GT 0 THEN nf=nf+(lastframe-start+1) ELSE nf=lastframe-start+1
      IF debugon THEN help, nx,ny,rect,true_color,nf,imgsptr,imgsptr2
      IF debugon THEN wait,2

      IF keyword_set(VIDEO) THEN BEGIN
	 xout = 36
	 yout = 34
	 IF not(keyword_set(IMG_REBIN)) THEN BEGIN
	    IF ny GT 480 THEN BEGIN
		y1 = (ny-480)/2
		y2 = ny-1-(ny-480)/2
	    ENDIF ELSE BEGIN
		y1 = 0
		y2 = ny-1
	    ENDELSE
	    coords=[(nx-640)/2,nx-1-(nx-640)/2,y1,y2]
	 ENDIF ELSE BEGIN
		new_y = (float(ny)/nx)*640
		IMG_REBIN=[640,new_y]
		IF new_y GT 480 THEN coords=[0,nx-1,(nx/640.)*80,ny-1-(nx/640.)*80] $
		    ELSE coords=[0,nx-1,0,ny-1]
	 ENDELSE
      ENDIF ELSE BEGIN
	xout=15
	yout=15
      ENDELSE
      IF datatype(docoords) EQ 'UND' or NOT keyword_set(LOAD) THEN docoords=0
      IF KEYWORD_SET(COORDS) THEN docoords=coords
      coords=docoords
      IF keyword_set(COORDS) THEN BEGIN
         IF (N_ELEMENTS(COORDS) EQ 1) THEN BEGIN	;** interactive selection
            AWIN, imgsptr(0)
            SETIMAGE
            EXPTV, imgsptr(0), /NOBOX
            tmp=TVSUBIMAGE(imgsptr(0), img0_x1, img0_x2, img0_y1, img0_y2)
            WDELETE, !D.WINDOW
         ENDIF ELSE BEGIN				;** user passed in [x1,x2,y1,y2]
            img0_x1=coords(0)
            img0_x2=coords(1)
            img0_y1=coords(2)
            img0_y2=coords(3)
         ENDELSE
         rnx = img0_x2-img0_x1+1
         rny = img0_y2-img0_y1+1
         xcen = xcen - img0_x1
         ycen = ycen - img0_y1
      ENDIF

      IF datatype(dorebin) EQ 'UND' or NOT keyword_set(LOAD) THEN dorebin=0 
      IF KEYWORD_SET(IMG_REBIN) THEN dorebin=img_rebin
      IF n_elements(dorebin) GT 1 THEN BEGIN
         rrnx = dorebin(0)
         rrny = dorebin(1)
;** if shrinking by integer factor use REBIN otherwise use CONGRID with linear interpolation
         IF ((nx MOD rrnx) EQ 0) THEN use_rebin=1 ELSE use_rebin=0
         xcen = xcen * FLOAT(rrnx)/rnx
         ycen = ycen * FLOAT(rrny)/rny
         secs = secs / (FLOAT(rrny)/rny)
         rnx = rrnx
         rny = rrny
      ENDIF

    ;WINDOW,2,xsiz=nx,ysiz=ny,/pixmap
    IF dodiff or dordiff THEN loadct,0

    IF ~keyword_set(NOXCOLORS) THEN BEGIN
    	IF f EQ 0 THEN imgs=bytarr(rnx*(do2+1),rny, nf/(nskip+1)) $
	ELSE BEGIN
	    sz=size(imgs)
	    imgs2=bytarr(sz[1],sz[2],nf)
	    imgs2[*,*,0:sz[3]-1]=imgs
	    imgs=imgs2
	    undefine,imgs2
	ENDELSE
    ENDIF
	    	    	
    IF debugon THEN help,imgs
    IF debugon THEN wait,2

    FOR i=start,lastframe,nskip+1 DO BEGIN
      PRINT, '%%SCC_WRUNMOVIE reading frame ', STRING(i+1,FORMAT='(I4)'), ' of ',STRING(file_hdr.nf,FORMAT='(I4)'), $
                ' from movie file ', filename
		
      set_plot,'z'
      device,set_resolution=[rnx*(do2+1),rny]
	
;    	WINDOW, XSIZE = rnx*(do2+1), YSIZE = rny, /PIXMAP, /FREE    ;** nbr, 12/17/98
;	IF ((f EQ 0) AND (i EQ start)) THEN win_index = !D.WINDOW ELSE win_index = [win_index, !D.WINDOW]

    	ahdr = SCCMVIHDR2STRUCT(ihdrs(i),file_hdr.ver)
	
      IF debugon THEN BEGIN
	    help,/str,ahdr
	    wait,1
      ENDIF
    	
      image = imgsptr(i)

;         IF ( swapflag EQ 1 ) THEN BYTEORDER, ahdr
      IF ((i EQ start) AND (f EQ 0)) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
      IF (i EQ start) THEN aname = ahdr.filename ELSE aname = [aname, ahdr.filename]

      if ~truecolor then begin
         IF (dordiff) THEN BEGIN
;** if cat'ing together .mvi files you have to do a running diff of prev img only
         IF (N_ELEMENTS(all_files) GT 1) THEN BEGIN
            IF (i GT start+dordiff-1) OR (f GT 0) THEN BEGIN
               tmp = image
               image = BYTSCL(FIX(image)-prev, min=dif_min,max=dif_max)
               prev = tmp
            ENDIF ELSE prev = image
         ENDIF ELSE BEGIN
            IF (i GT start+dordiff-1) THEN BEGIN
               prev=imgsptr(i-dordiff)
               image = BYTSCL(FIX(image)-prev, min=dif_min,max=dif_max)
            ENDIF
         ENDELSE
         ENDIF

         IF dodiff THEN BEGIN
            IF (i GT start) OR (f GT 0) THEN BEGIN
               image = BYTSCL(FIX(image)-prev, min=dif_min,max=dif_max)
            ENDIF ELSE prev = image
         ENDIF

         IF KEYWORD_SET(COORDS) THEN BEGIN
            image = image(img0_x1:img0_x2,img0_y1:img0_y2)
         ENDIF 

         IF n_elements(dorebin) GT 1 THEN BEGIN
            IF (use_rebin EQ 1) THEN $
               image = REBIN(image, rnx, rny) $
            ELSE $
               image = CONGRID(image, rnx, rny, /INTERP)
         ENDIF 
	 
	 IF keyword_set(RECTIFIED) and keyword_set(DORECTIFY) THEN $
	 IF rectified EQ 180 THEN image = rotate(image,2)

         IF KEYWORD_SET(COSMIC) THEN BEGIN
            IF (use_ccosmics EQ 1) THEN image = CCOSMICS(image) ELSE BEGIN
               POINT_FILTER,image,5,7,3,tmp,pts
	       IF debugon THEN help,pts
               image = tmp
            ENDELSE
         ENDIF 

    	IF keyword_set(LOGO) THEN BEGIN
	    IF (issoho) THEN image = add_lasco_logo(temporary(image)) ELSE $
	    	    	     image = scc_add_logo(temporary(image))
	ENDIF
	
    	TV, image, TRUE=TRUECOLOR

;	IF ~keyword_set(NOXCOLORS) THEN imgs[0:rnx-1,*,i]=image
	
    	IF (do2 EQ 1) THEN BEGIN
            image = imgsptr2(i)
            IF n_elements(dorebin) GT 1 THEN BEGIN
               IF (use_rebin EQ 1) THEN $
                  image = REBIN(image, rnx, rny) $
               ELSE $
                  image = CONGRID(image, rnx, rny, /INTERP)
            ENDIF
            TV, image, rnx, 0, TRUE=truecolor
	    IF ~keyword_set(NOXCOLORS) THEN imgs[rnx:2*rnx-1,*,ii]=image
    	ENDIF

    	help,solr
         IF dolimb THEN $
            TVCIRCLE, solr/secs, xcen, ycen, COLOR=255, THICK=2
    
         IF KEYWORD_SET(SPOKE) THEN begin           
            image_tmp=tvrd()
            sz=size(image_tmp)
            if not keyword_set(R1) then r1=0		; 0 or helio-centric center as default
            if not keyword_set(R2) then r2=sz[1]/2  	; (pixels) half image size as default
                                                    	; maximum horizontal size
            if not keyword_set(TH1) then th1=0		; 0 degree as default
            if not keyword_set(TH2) then th2=360	; 360 degree as default
            if not keyword_set(N_R) then n_r=300	; samples of radius
            if not keyword_set(N_TH) then n_th=th2-th1+1 	; samples of angles
            print,'making spoked image, angle sample=',n_th,'  radius sample=',n_r
            rad_spoke,image_tmp,image_rad,x0=xcen,y0=ycen,r1=r1,r2=r2,th1=th1,th2=th2,n_r=n_r,n_th=n_th
            tv,congrid(rotate(image_rad,1),(size(image_tmp))[1],(size(image_tmp))[2])
         ENDIF
	 
	 IF dotimes THEN BEGIN
            IF not keyword_set(CHSZ) then charsz=1.0 else charsz=CHSZ
	    IF dotimes GT 1 THEN time_color=times ELSE time_color=255
            xouts=10 & youts=10
            XYOUTS, xouts, youts, strmid(ahdr.date_obs,0,4)+'/'+strmid(ahdr.date_obs,5,2)+'/'+strmid(ahdr.date_obs,8,2)+' '+strmid(ahdr.time_obs,0,8),/DEVICE, CHARSIZE=charsz, COLOR=time_color
            IF ~keyword_set(NOCAM) THEN begin
                no_cam=1	    
                camt=ahdr.detector+'-'+strmid(ahdr.filename,strpos(ahdr.filename,'.f')-1,1)
	        cyouts=youts+ charsz*10 + 5
     	        XYOUTS, xouts, cyouts, strupcase(camt), /DEVICE,  CHARSIZE=charsz, COLOR=time_color
            ENDIF else no_cam=0


	 ENDIF

         image=tvrd()

      endif   ;if ~truecolor 
      ;set_plot,'x'
      select_windows
      
      WINDOW, XSIZE = rnx*(do2+1), YSIZE = rny, /PIXMAP, /FREE    ;** nbr, 12/17/98
      tv,image ,true=truecolor
      IF ~keyword_set(NOXCOLORS) THEN imgs[0:rnx-1,*,ii]=image
      IF ((f EQ 0) AND (i EQ start)) THEN win_index = !D.WINDOW ELSE win_index = [win_index, !D.WINDOW]


      start=0
      ii=ii+1
    ENDFOR
    IF (f EQ 0) THEN names = aname ELSE names = [names,aname]
    CLOSE,lu
    FREE_LUN,lu
ENDFOR		;** looping over all input .mvi files

ENDIF	;>>>>>>>>>>>>** End read movie in from .mvi file

IF keyword_set(NOXCOLORS) THEN undefine, imgs
   ;** load movies from existing pixmaps (later)
   
   WSET, win_index(0)
   hsize = !D.X_SIZE
   vsize = !D.Y_SIZE
   ;** get length of movie
   len = N_ELEMENTS(win_index)
   frames = STRARR(len)	;** array of movie frame names (empty)
   IF (KEYWORD_SET(names) NE 0) THEN BEGIN
	BREAK_FILE, names, as, dirs, short_names, exts
	frames = short_names+exts
   ENDIF

   IF datatype(dofixgaps) EQ 'UND' or NOT keyword_set(LOAD) THEN dofixgaps=0
   IF keyword_set(FIXGAPS) THEN dofixgaps=1
   
   IF dofixgaps THEN BEGIN
    	PRINT, '%%SCC_WRUNMOVIE: Fixing missing blocks.'
    	IF NOT(KEYWORD_SET(BLOCK_SIZE)) THEN block_size = 32*(FLOAT(rnx)/nx)
    	SET_DATA_GAPS, win_index, 0, /AFTER, BLOCK_SIZE=block_size, N_BLOCK=n_block
    	PRINT, '%%SCC_WRUNMOVIE frame: ', 0, ' Num blocks replaced: ', n_block
    	FOR i=1, len-1 DO BEGIN
    	    SET_DATA_GAPS, win_index, i, /BEFORE, BLOCK_SIZE=block_size, N_BLOCK=n_block
    	    PRINT, '%%SCC_WRUNMOVIE frame: ', i, ' Num blocks replaced: ', n_block
      	ENDFOR
   ENDIF

   first = 0
   last = len-1
   current = 0
   forward = 1
   dirsave = 1
   bounce = 0
   pause = 0
   dopause = 1
   deleted = BYTARR(len)
   showmenu = 0
   IF keyword_set(RECTIFIED) THEN BEGIN
   	rect=rectified 
	IF keyword_set(CENRECTIFY) and rect EQ 180 THEN BEGIN
		xcen = hsize-1-xcen
		ycen = vsize-1-ycen
	ENDIF
   ENDIF 
   
   ; As an interim fix to problems with 24-bit color, do the following
   ; so that saved frames are readable.
   
   ;device,get_visual_depth=depth
   ;IF depth GT 8 THEN loadct,0
   
   ; ***********************
;stop

;**--------------------Create Widgets-------------------------------**
hhsize = (hsize+20) > (512+20)
yscsz= vsize < 1024
xscsz= hsize < 1024
title = 'SCC_RUNMOVIE: '+ftitle
baseyoff=0
basexoff=0

winbase = WIDGET_BASE(TITLE=title)

    ;** create window widget to display images in
;    draw_w = WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, EVENT_PRO='SCC_WRUNMOVIE_DRAW', $
;                /FRAME, /BUTTON_EVENTS, RETAIN=2)
;    draw_w = WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, X_SCROLL_SIZE=xscsz, $
;		Y_SCROLL_SIZE=yscsz, EVENT_PRO='SCC_WRUNMOVIE_DRAW', /FRAME, /BUTTON_EVENTS, RETAIN=2)
IF debugon THEN help,hsize,vsize
if debugon THEN wait,2

IF ~keyword_set(SAVE) THEN BEGIN

    device,get_screen_size=ss
    if (hsize gt 0.98*ss(0) OR vsize gt 0.98*ss(1)) AND NOT(dofull) then $
    	draw_w= WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, $
                       X_SCROLL_SIZE=(hsize < 0.965*ss(0)), $
                       Y_SCROLL_SIZE=(vsize < 0.89*ss(1)), $
                       EVENT_PRO='SCC_WRUNMOVIE_DRAW', /FRAME, $
                       /BUTTON_EVENTS, RETAIN=2) $ 
    else $
    	draw_w= WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, $
                        EVENT_PRO='SCC_WRUNMOVIE_DRAW', /FRAME, $
                        /BUTTON_EVENTS, RETAIN=2)

   WIDGET_CONTROL, /REAL, winbase
   WIDGET_CONTROL, draw_w, GET_VALUE=draw_win

   WIDGET_CONTROL, winbase,TLB_GET_OFFSET=tbloff,TLB_GET_SIZE=tblsize
   if tblsize(1)+tbloff(1)+253 gt ss(1) then begin
      baseyoff=ss(1)-253 
      basexoff=ss(0)-539
   endif else begin
      baseyoff=tblsize(1)+tbloff(1)
      basexoff=0
   endelse
ENDIF	; not SAVE

   base = WIDGET_BASE(/COLUMN, XOFFSET=basexoff, YOFFSET=baseyoff, TITLE='SCC_WRUNMOVIE Control')

    base1 = WIDGET_BASE(base, /ROW)
	base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)
	    flabel = WIDGET_LABEL(base2, VALUE=' #   Frame')
	    base25 = WIDGET_BASE(base2, /ROW)
                cframe = WIDGET_TEXT(base25, VALUE='0', XSIZE=3, YSIZE=1, /EDITABLE, UVALUE='FRAME_NUM')
	        cname = WIDGET_TEXT(base25, VALUE=' ', XSIZE=23, YSIZE=1)
if single ne 1 then 	slide2 = WIDGET_SLIDER(base1, TITLE='First Frame', $
		 VALUE=0, UVALUE='SLIDE_FIRST', MIN=0, MAX=len)
if single ne 1 then 	slide3 = WIDGET_SLIDER(base1, TITLE='Last Frame', $
		 VALUE=len-1, UVALUE='SLIDE_LAST', MIN=0, MAX=len-1)
;	slide4 = WIDGET_SLIDER(base1, TITLE='Skip Every n Frames', $
;		 VALUE=0, UVALUE='SLIDE_SKIP', MIN=0, MAX=10)
if single ne 1 then 	slide1 = WIDGET_SLIDER(base1, TITLE='Playback Speed', SCROLL=1, $
		 VALUE=stall0, UVALUE='SPEED', MIN=0, MAX=100, /DRAG)

    base3 = WIDGET_BASE(base, /ROW)

if single ne 1 then begin
        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Forward', 'Reverse', 'Bounce'], /EXCLUSIVE, /COLUMN, IDS=dirb, $
	      BUTTON_UVALUE = ['FORWARD', 'REVERSE','BOUNCE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
endif

        col = WIDGET_BASE(base3, /COLUMN)
if single ne 1 then 	tmp = CW_BGROUP(col,  ['Pause At End', 'No Pause'], /EXCLUSIVE, /COLUMN, IDS=pauseb, /FRAME, $
	      BUTTON_UVALUE = ['PAUSE','NO_PAUSE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	tmp = CW_BGROUP(col,  ['Call scc_wrunmoviem'], /COLUMN, $
	      BUTTON_UVALUE = ['WRUNMOVIEM'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	tmp = CW_BGROUP(col,  ['Annotate'], /COLUMN, $
	      BUTTON_UVALUE = ['ANNOTATE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')

if single ne 1 then begin
        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
 	tmp = CW_BGROUP(col,  ['Continuous', 'Next Frame', 'Prev. Frame'], /COLUMN, $
	      BUTTON_UVALUE = ['CONTINUOUS', 'NEXT','PREV'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
endif

         junk={CW_PDMENU_S, flags:0, name:''}
        desc = [ { CW_PDMENU_S, 1, 'Save Frame' }, $
                 { CW_PDMENU_S, 0, 'Postscript' }, $
                 { CW_PDMENU_S, 0, 'PS-Rescale' }, $
                 { CW_PDMENU_S, 0, '   gif    ' } ]

        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	
	tmp = Widget_Button(col, Value="Adjust Color", Event_Pro="call_xcolors")
if single ne 1 then 	tmp = CW_BGROUP(col,  ['Delete Frame'], /COLUMN, IDS=otherb, $
	      BUTTON_UVALUE = ['DELETE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	;tmp = CW_BGROUP(col,  ['Adjust Color', 'Delete Frame'], /COLUMN, IDS=otherb, $
	;      BUTTON_UVALUE = ['ADJCT', 'DELETE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')

	tmp = CW_PDMENU(col, desc, /RETURN_NAME)


        col = WIDGET_BASE(base3, /COLUMN, /FRAME)
	tmp = CW_BGROUP(col,  ['Save Movie' ,' Load Movie ', '    Quit    '], /COLUMN, $
	      BUTTON_UVALUE = ['SAVEMV','LOAD','DONE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')

if single ne 1 then $
    sliden = WIDGET_SLIDER(base, TITLE='Current Frame', VALUE=0, UVALUE='SLIDE_NUM', $
             /DRAG, /SCROLL, MIN=0, MAX=len-1)  else sliden=-1



        cmbsliden=0
        cropinst=0	
	cmbvals=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]; 0=zoom select 1=x1 2=x2 3=y1 4=y2 5=scale factor 6=max(crop x) 7=max(crop y) 8=rxcen 9=rycen 10 restore 11 times size
        if keyword_set(mvicombo) then begin
              bkgcolor=0	; background for annotation text
              WINDOW, 11, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='scc_wrunmovie combine zoom position'
              erase,bkgcolor  ; this is the color behind annotations on the image from the cursor position
              cmbvals(6)=mvicombo(0)
              cmbvals(7)=mvicombo(1)
              cmbvals(11)=mvicombo(2)
              base4a = WIDGET_BASE(base, /ROW, /Frame,title='Combine_MVI Controls')
              base4b = WIDGET_BASE(base4a, /colum)

              txt=widget_label (base4b,  value='Combine_MVI Controls')

              cropinst=widget_text(base4b,value='Not selected',uvalue='cropinst',font='-1',XSIZE=40)

              base4b1 = WIDGET_BASE(base4b, /row)
 	      tmp = CW_BGROUP(base4b1,  ['Crop '], /COLUMN, $
	      BUTTON_UVALUE = ['cmbcrop'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
 	      tmp = CW_BGROUP(base4b1,  ['Clear Crop'], /COLUMN, $
	      BUTTON_UVALUE = ['cmbcancel'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')

              slz=where(cmbstate.sec_pixs gt 0)
              cmbsliden = CW_FSLIDER (base4b, TITLE='Zoom: Select platescale value (arcsec/pixel)', $
	      	    VALUE=(max(cmbstate.sec_pixs(slz))/cmbstate.scale), UVALUE='cmbzoomsel', $
                   MIN=(min(cmbstate.sec_pixs(slz))), MAX=(max(cmbstate.sec_pixs(slz))/cmbstate.origscale), $
		   /DRAG, format='(f6.2)',edit=0,scroll=0.01,double=0)
              widget_control, cmbsliden, set_val=(max(cmbstate.sec_pixs(slz))/cmbstate.scale)

	      tmp = CW_BGROUP(base4b,  ['Restore Original Size '], /COLUMN, $
	      BUTTON_UVALUE = ['cmbreset'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
              base4b3a = WIDGET_BASE(base4b, /row)
              mcharsv=CW_FIELD(base4b3a, value=string(cmbvals(11)/10.,'(f3.1)'), uvalue="mcharsv", $
	      	    title='Timestamp Charsize (0=none)',/all_events,xsize=3,/floating)
;	      tmp = CW_BGROUP(base4b3a,  ['Add Text'], /COLUMN, $
;	      BUTTON_UVALUE = ['CMBADDTEXT'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
;	      tmp = CW_BGROUP(base4b3a,  ['Clear Text'], /COLUMN, $
;	      BUTTON_UVALUE = ['CMBCLRTEXT'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
;              txt=widget_label (base4b,  value='** Add text after cropping and selecting platscale')
              base4b3 = WIDGET_BASE(base4b, /row)
 	      tmp = CW_BGROUP(base4b3,  ['Preview'], /COLUMN, $
	      BUTTON_UVALUE = ['CMBPREVIEW'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	      tmp = CW_BGROUP(base4b3,  ['Return to Setup '], /COLUMN, $
	      BUTTON_UVALUE = ['CMBSETUP'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	      tmp = CW_BGROUP(base4b3,  ['Clear Text'], /COLUMN, $
	      BUTTON_UVALUE = ['CMBCLRTEXT'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
               base4b2 = WIDGET_BASE(base4b, /row)
	      tmp = CW_BGROUP(base4b2,  ['Make Movie w/o img array'], /COLUMN, $
	      BUTTON_UVALUE = ['CMBMKMOVIE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	      tmp = CW_BGROUP(base4b2,  ['Make/Save Movie '], /COLUMN, $
	      BUTTON_UVALUE = ['CMBSVMOVIE'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')
	      tmp = CW_BGROUP(base4b2,  [' Make img array'], /COLUMN, $
	      BUTTON_UVALUE = ['CMBWIMGS'], EVENT_FUNCT='SCC_WRUNMOVIE_BGROUP')



              base4c = WIDGET_BASE(base4a, /colum)
              	tmp=widget_label(base4c,  value='Masks')
    	    	tmp=widget_label(base4c,  value=' 0 = No Mask')
    	    	tmp=widget_label(base4c,  value='-1 = Use FITS File') 
              base4e = WIDGET_BASE(base4c, /row)
                mhi2av=CW_FIELD(base4e, value=cmbstate.outer(0), uvalue="mhi2av",title='HI2_A',/all_events,xsize=5,/floating)
                mhi2bv=CW_FIELD(base4e, value=cmbstate.outer(9), uvalue="mhi2bv",title='HI2_B',/all_events,xsize=5,/floating)
              base4f = WIDGET_BASE(base4c, /row)
    	    	mcor2av=CW_FIELD(base4f, value=cmbstate.outer(2), uvalue="mcor2av",title='COR2A',/all_events,xsize=5,/floating)
    	    	mcor2bv=CW_FIELD(base4f, value=cmbstate.outer(7), uvalue="mcor2bv",title='COR2B',/all_events,xsize=5,/floating)
              base4g = WIDGET_BASE(base4c, /row)
                mcor1av=CW_FIELD(base4g, value=cmbstate.outer(3), uvalue="mcor1av",title='COR1A',/all_events,xsize=5,/floating)
    	    	mcor1bv=CW_FIELD(base4g, value=cmbstate.outer(6), uvalue="mcor1bv",title='COR1B',/all_events,xsize=5,/floating)
              base4h = WIDGET_BASE(base4c, /row)
                meuviav=CW_FIELD(base4h, value=cmbstate.outer(4), uvalue="meuviav",title='EUVIA',/all_events,xsize=5,/floating)
                meuvibv=CW_FIELD(base4h, value=cmbstate.outer(5), uvalue="meuvibv",title='EUVIB',/all_events,xsize=5,/floating)
 

        endif 


   WIDGET_CONTROL, base, MAP=0
   WIDGET_CONTROL, /REAL, base
if single ne 1 then    WIDGET_CONTROL, dirb(0), SET_BUTTON=1
if single ne 1 then    WIDGET_CONTROL, pauseb(0), SET_BUTTON=1
   WIDGET_CONTROL, base, TIMER=0. ;.000001
   
   IF keyword_set(SAVE) THEN draw_win=-1 ELSE WSET, draw_win

;**--------------------Done Creating Widgets-----------------------------**

;--Finish creating frame headers
   IF KEYWORD_SET(SUNXCEN) THEN FOR i=0,len-1 DO hdrs[i].xcen=xcen
   IF KEYWORD_SET(SUNYCEN) THEN FOR i=0,len-1 DO hdrs[i].ycen=ycen
   IF KEYWORD_SET(SEC_PIX) THEN FOR i=0,len-1 DO hdrs[i].cdelt1=secs
   IF KEYWORD_SET(R_SUN)   THEN FOR i=0,len-1 DO hdrs[i].rsun=rsun

;--Define file_hdr if a new movie
   if datatype(file_hdr) eq 'UND' then begin
      if (tag_exist(hdrs(0),'r1row')) THEN def_mvi_hdr,file_hdr,hdr=hdrs(0) else begin
      	def_mvi_hdr,file_hdr
      	file_hdr.SUNXCEN= xcen
      	file_hdr.SUNYCEN= ycen
      	file_hdr.RECTIFIED= rect
      	file_hdr.TRUECOLOR= truecolor
      	file_hdr.sec_pix= secs   
      	IF (datatype(CCD_POS) eq 'UND') THEN file_hdr.CCD_POS=[0,0,0,0] else file_hdr.CCD_POS=CCD_POS
      endelse 
      file_hdr.NF= Len
      file_hdr.NX= hsize
      file_hdr.NY= Vsize
      file_hdr.TRUECOLOR= truecolor
   endif

   IF (KEYWORD_SET(HDRS) NE 0) THEN img_hdrs = hdrs ELSE img_hdrs = 0
   moviev = { $
 		base:base, $ 
 		xcen:xcen, $ 
 		ycen:ycen, $ 
                rsun:rsun, $
 		secs:secs, $ 
 		winbase:winbase, $ 
 		current:current, $ 
 		forward:forward, $ 
 		dirsave:dirsave, $ 
 		bounce:bounce, $ 
 		first:first, $ 
 		last:last, $ 
 		pause:pause, $ 
 		dopause:dopause, $ 
 		len:len, $ 
 		hsize:hsize, $ 
 		vsize:vsize, $ 
 		win_index:win_index, $ 
 		draw_win:draw_win, $ 
 		cframe:cframe, $ 
 		cname:cname, $ 
 		frames:frames, $ 
 		deleted:deleted, $ 
                showmenu:showmenu, $
                filename:filename, $
                filepath:filepath, $
                img_hdrs:img_hdrs, $
                file_hdr:file_hdr, $
 		nskip:nskip, $ 
 		stallsave:stallsave, $ 
 		sliden:sliden, $ 
 		stall:stall, $ 
		rect:rect, $
		truecolor:truecolor, $
                cmbsliden:cmbsliden, $
                cmbvals:cmbvals, $
                cropinst:cropinst $               

;    IMG_REBIN:img_rebin, SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, $
;    DIFF=diff, SAVE=save, RUNNING_DIFF=running_diff, COORDS=coords, COSMIC=cosmic, FIXGAPS=fixgaps, $
;    DRAW_LIMB=draw_limb, BLOCK_SIZE=block_size, LOGO=logo, VIDEO=video, NOCAM=nocam,$
;    DIF_MIN=dif_min, DIF_MAX=dif_max, KEEP=keep, FULL=full, $
;    CHSZ=chsz, SPOKE=spoke, R1=r1, R2=r2, TH1=th1, TH2=th2, N_R=n_r, N_TH=n_th, $
;    RECTIFIED=rectified, DORECTIFY=dorectify, CENRECTIFY=cenrectify, TRUECOLOR=truecolor $
            }


   IF KEYWORD_SET(SAVE) THEN BEGIN
    	moviev.filename = save
	
    	IF (CHECK_PERMISSION(moviev.filename) EQ 1) THEN scc_save_mvi, moviev

    	WIDGET_CONTROL, /DESTROY, moviev.base
    	WIDGET_CONTROL, /DESTROY, moviev.winbase
    	IF NOT(keyword_set(KEEP)) THEN FOR i=0, moviev.len-1 DO WDELETE, moviev.win_index(i)
   ENDIF ELSE BEGIN

    	WIDGET_CONTROL, base, SET_UVALUE=moviev

    	XMANAGER, 'SCC_WRUNMOVIE', base, EVENT_HANDLER='SCC_WRUNMOVIE_EVENT'

   ENDELSE

END
