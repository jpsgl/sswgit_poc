pro def_mvi_hdr,file_hdr,hdr=hdr
; $Id: def_mvi_hdr.pro,v 1.5 2011/12/29 23:12:37 nathan Exp $
;
; Project     : STEREO - SECCHI, SOHO - LASCO/EIT
;
; Name        : def_mvi_hdr
;
; Purpose     : defines MVI File header struct
;
;   	The MVI frame header structure is defined in def_mvi_strct.pro
;
; Use         : IDL> def_mvi_hdr,file_hdr,hdr
;
; Inputs      : 
;	lu	INT	logical unit of MVI file to read
;
; Optional Inputs: hdr, if hdr fits file header from wscc_mkmovie then the mvi header filled.
;
; Outputs     :
;	file_hdr  STRUCT	describes movie
;
; Keywords    :
;
; $Log: def_mvi_hdr.pro,v $
; Revision 1.5  2011/12/29 23:12:37  nathan
; do not change unit of cdelt because that is done in sccwrite_disk_movie if necessary
;
; Revision 1.4  2011/03/09 19:45:05  mcnutt
; add rtheta 3 and 4 for carrington/stonyhurst synoptic movie
;
; Revision 1.3  2010/11/17 23:26:30  nathan
; clarify recttified def
;
; Revision 1.2  2009/01/14 19:13:25  nathan
; added clarifying comments
;
; Revision 1.1  2009/01/14 14:08:34  mcnutt
; new pro to define movie mvi header used by sccread_mvi and scc_wrunmovie
;

	file_hdr = {sfile_hdr,   nf:0, 	$; # images in file
				nx:0, 	$; # columns in each image
				ny:0, 	$; # rows in each image
				mx:0, 	$; # maximum number of images in file
				nb:0L, 	$; # bytes in frame header
				ver:0, 	$; # mvi version number
;following for version 1
				fh_nb:0,	$; bytes in MVI header (incl. color table)
				sunxcen:0.0,	$; sun x center in IDL coordinates!
				sunycen:0.0,	$; sun y center
				sec_pix:0.0,	$; arc seconds per pixel
;following for version 3			by the jake
				rtheta:0,	$; rtheta mapping flag (1 =rtheta, 2=rtheta yaxis log, 3= Carrington coords, 4= stonyhust coords)
				radius0:0.0,	$; radius at bottom of image (Rsun)
				radius1:0.0,	$; radius at top of image (Rsun)
				theta0:0.0,	$; theta at left of image (deg, 0@N, CCW+)
				theta1:0.0,	$; theta at right of image (deg, 0@N, CCW+)
;following for version 4 			- nbr, 8/28/03
				rectified:0,$	; value of CW roll correction, degrees, positive integer (rounded) (opposite sign of CROTA)
;following for version 5
				truecolor:0,$	; flag for true color images
;following for version 6
				ccd_pos:intarr(4)}     ; region of CCD used for movie (row1, col1, row2, col2)

;nb = 128 	;version 5 number of bytes reserved for image header
file_hdr.nb = 136	;version 6 number of bytes reserved for image header

;f_nb = 20	;          ** bytes in file_hdr before color table
;f_nb = 32	;version 4 adds RTHETA
;f_nb = 34	;version 5 adds flag for true color
file_hdr.fh_nb = 42	;version 6 adds CCD_POS 


if (keyword_set(hdr))then begin
    IF(DATATYPE(hdr) NE 'STC') THEN hdr=fitshead2struct(hdr,/dash2underscore) ELSE hdr=hdr
   ;debug=1
   d2a=1.
   ;IF tag_exist(hdr,'cunit1') THEN IF strpos(hdr.cunit1,'deg') GE 0 THEN d2a=3600.
  ; HI is different cdelt
   file_hdr.sec_pix=hdr.cdelt1*d2a
   file_hdr.SUNXCEN=hdr.xcen
   file_hdr.SUNYCEN=hdr.ycen
   ; I expect that hdr comes either from scc_mkmovie or generic_movie, so xcen should be there,
   ; even though the defn of x/ycen for MVI is different than SolarSoft 

    IF tag_exist(hdr,'crpix1') and tag_exist(hdr,'crpix2') THEN BEGIN
    ; Use WCS coordinates
       sunc = SCC_SUN_CENTER(hdr,FULL=0)
      ; IDL coords!!
      file_hdr.SUNXCEN = sunc.xcen
      file_hdr.SUNYCEN = sunc.ycen
   ENDIF
   IF tag_exist(hdr,'r1row') THEN file_hdr.ccd_pos = [hdr.r1row, hdr.r1col, hdr.r2row, hdr.r2col]

endif

END
