;+
; $Id: sccwrite_ht.pro,v 1.20 2014/05/05 17:25:31 nathan Exp $
; NAME:
;	sccWRITE_HT
;
; PURPOSE:
;	This procedure writes out the height-time
;	file.  It is used by the movie program and is not intended to be
;	used in a standalone fashion.
;
; CATEGORY:
;	MOVIE
;
; CALLING SEQUENCE:
;	sccWRITE_HT,Callt,Htfile,Hdr,R,Pa,Feat,X,Y
;
; INPUTS:
;	Callt:	Parameter indicating whether header or observation 
;		information is being written
;		0 = header, 1 = observation
;	Htfile:	String containing the file name to be written to
;	Hdr:	Structure containing the frame header information
;	R:	Radius of observation in units of solar radii (float)
;	Pa:	Position angle of observation in degrees (float)
;	X:	Column of observation (pixels)
;	Y:	Row of observation (pixels)
;
;
; SIDE EFFECTS:
;	Appends information to height-time file if existing or opens a new
;	one if none exists.
;
; MODIFICATION HISTORY:
; $Log: sccwrite_ht.pro,v $
; Revision 1.20  2014/05/05 17:25:31  nathan
; put in correct spacecraft if not STEREO
;
; Revision 1.19  2013/05/22 16:58:59  mcnutt
; Allways defines label in sccfill_ht
;
; Revision 1.18  2012/08/17 18:43:08  mcnutt
; added Labels for ht points
;
; Revision 1.17  2012/07/10 18:08:20  mcnutt
; added stonyhusrt corr option
;
; Revision 1.16  2011/10/28 17:59:57  nathan
; use parse_secchi_filename to get sc
;
; Revision 1.15  2011/04/28 17:00:18  mcnutt
; added auto advance option
;
; Revision 1.14  2010/03/16 17:50:22  mcnutt
; creates/writes two ht files for AB movies
;
; Revision 1.13  2010/01/15 19:09:05  nathan
; fix conflict
;
; Revision 1.12  2010/01/14 20:57:40  nathan
; updated header keywords, unit defn
;
; Revision 1.11  2009/08/26 17:52:25  mcnutt
; changed print formats
;
; Revision 1.10  2009/08/05 16:18:33  mcnutt
; changed formats for different elong values to handle Heliographic Cartesian
;
; Revision 1.9  2009/04/27 16:05:09  mcnutt
; varys column headings according to the type of coordinate system being written
;
; Revision 1.8  2008/10/28 14:49:32  mcnutt
; changes htfile name in structure when home dir is used
;
; Revision 1.7  2008/07/11 17:47:33  mcnutt
; added subfield [0,0] ccd pos to header
;
; Revision 1.6  2008/07/11 13:11:06  mcnutt
; set featnum to -1 if all features for a frame have been selected
;
; Revision 1.5  2008/05/13 15:53:56  mcnutt
; added groupleader and made it a modal widget
;
; Revision 1.4  2008/05/05 18:25:05  mcnutt
; added untis for height and corrected output filename
;
; Revision 1.3  2008/05/05 14:42:40  mcnutt
; New version widget version, sccWRITE sets up output file and sccFILL adds the data points'
;
; Revision 1.5  2008/04/30 14:15:41  nathan
; incomplete attempt to widgetize input
;
; Revision 1.4  2007/11/21 16:04:03  nathan
; Do not read_ht if file exists; check for TIME_OBS=blank
;
; Revision 1.3  2007/11/16 19:19:59  nathan
; Version=5 (version 4 handles TRACE data nrs and hw 12/05/03)
;
; Revision 1.2  2007/11/15 19:06:04  nathan
; changes to allow tracking multiple features at once
;
; Revision 1.1  2007/10/19 14:13:00  nathan
; copied from NRL_LIB/lasco/movie and renamed
;
; 	Written by:	Scott Hawley, NRL summer student, July 1996
;	V2  5/3/97	RAHoward,NRL	Combined all writes to ht-file
;	V3  9/30/97	RAHoward,NRL	Defined Version 2 for HT file
;	7/25/01, N.Rich - Add CHECK_PERMISSION for htfile and check first line
;		of htfile for validity
;       10/02/06        Ed Esfandiari - Modified to handle SECCHI headers.
; 
; @(#)write_ht.pro	1.9 07/25/01 :NRL Solar Physics
;
;-
pro sccFILL_ht,htfile,hdr0,r,pa,x,y,featnum,pfx,advance,label,ab=ab
;-
COMMON scc_height_time,curr_feat,curr_time,filename,npts,tel,time_tai, $
       radii,posangle,desc,col,row, features, nf, hdr,units, groupleader,mvhhdr

COMMON sccplot, shtev, ptev

htfile=shtev.htfile(0)
if keyword_set(ab) then htfile=shtev.htfile(1)

if htfile(0) eq '' then begin
  featnum=-1
  return
endif

hdr=hdr0
; scc_wrunmoviem should properly populate hdr.time_obs
hdr_date_obs = strmid(hdr.date_obs,0,10)
hdr_time_obs = hdr.time_obs
cam=hdr.detector
sc=strmid(hdr.filename,strpos(hdr.filename,'.')-1,1)

OPENW,outfile,htfile,/get_lun,/APPEND
;help,hdr_time_obs,curr_time,curr_feat


       npts = npts+1
       
       IF (npts EQ 1) THEN BEGIN
    	if pfx eq 'CLN=' then nhtstr=     '#  CRLN      DATE     TIME      CRLT   TEL  FC  COL  ROW' else $
    	if pfx eq 'LON=' then nhtstr=     '#   LON      DATE     TIME       LAT   TEL  FC  COL  ROW' else $
    	if pfx eq 'X=' then nhtstr=       '#     X      DATE     TIME        Y    TEL  FC  COL  ROW' else $
	                           nhtstr='# HEIGHT    DATE     TIME   ANGLE  TEL  FC  COL  ROW'

	  PRINTF,outfile,nhtstr 
          curr_feat = 0
          curr_time=''
          tel =[hdr.detector]
          time_tai = [UTC2TAI(STR2UTC(hdr_date_obs+' '+hdr_time_obs))]
          posangle = [pa]
          radii = [r]
          features = [curr_feat]
          col = [x]
          row = [y]
          nptsff=npts
       ENDIF ELSE BEGIN
          tel =[tel,hdr.detector]
          time_tai = [time_tai,UTC2TAI(STR2UTC(hdr_date_obs+' '+hdr_time_obs))]
          posangle = [posangle,pa]
          radii = [radii,r]
          features = [features,curr_feat]
          col = [col,x]
          row = [row,y]
          nptsff=n_elements(where(time_tai eq UTC2TAI(STR2UTC(hdr_date_obs+' '+hdr_time_obs))))
      ENDELSE
    	; nr - I don't see any use for these arrays, so leaving unchanged

    	
	; Keep track of clicks in each frame
	
	IF hdr_time_obs EQ curr_time and hdr_time_obs NE '' THEN BEGIN
	    curr_feat=curr_feat+1
	ENDIF ELSE BEGIN
	    curr_feat=0
	    curr_time=hdr_time_obs
	ENDELSE
   	if pfx eq 'X=' or pfx eq 'CLN=' or pfx eq 'LON=' then fstring='(f9.4,a11,a1,a8,f9.4,a5,2x,i1,2i5)' else fstring='(f8.3,a11,a1,a8,f6.1,a5,2x,i1,2i5)'
	label='donotlabel'
        featnum=curr_feat
        advance=0
        if keyword_set(ab) then featnum=featnum-nf	
	IF curr_feat LT nf*n_elements(shtev.htfile) and nptsff LE nf*n_elements(shtev.htfile) THEN BEGIN
;    	    PRINT,         r,hdr_date_obs,'T',hdr_time_obs,pa,hdr.detector,curr_feat,x,y,FORMAT=fstring	      
    	    PRINTF,outfile,r,hdr_date_obs,'T',hdr_time_obs,pa,hdr.detector,featnum,x,y,FORMAT=fstring
            if shtev.auto eq 1 and curr_feat eq (nf*n_elements(shtev.htfile))-1 then advance=1
	    if shtev.label eq 1 then label=shtev.desc(featnum)
	ENDIF ELSE BEGIN
	    print,'All features chosen in this frame.'
	    print,'Go to next frame or start over.'
            featnum=-1  
	    IF hdr_time_obs EQ '' THEN print,'NOTE: No TIME_OBS in frame header; only 1 feature per frame allowed.'
	ENDELSE
  
FREE_LUN,outfile

RETURN
END

PRO SCChtpoint_EVENT, ev

COMMON scc_height_time
COMMON sccplot

WIDGET_CONTROL, ev.id, GET_UVALUE=input

CASE (input) OF

    'CANCEL' : BEGIN       ;** exit program
	    htfile=''
            shtev.htfile=htfile
    	    WIDGET_CONTROL, /DESTROY, ev.top
    	    RETURN
    	    END

    'desc1' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc1=val[0]
	    shtev.desc(0) = desc1 
    	    END
    'desc2' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc2=val[0]
	    shtev.desc(1) = desc2 
    	    END
    'desc3' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc3=val[0]
	    shtev.desc(2) = desc3 
    	    END
    'desc4' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc4=val[0]
	    shtev.desc(3) = desc4 
    	    END
    'desc5' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc5=val[0]
	    shtev.desc(4) = desc5 
    	    END
    'desc6' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc6=val[0]
	    shtev.desc(5) = desc6 
    	    END
    'desc7' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc7=val[0]
	    shtev.desc(6) = desc7 
    	    END
    'desc8' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc8=val[0]
	    shtev.desc(7) = desc8 
    	    END
    'desc9' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    desc9=val[0]
	    shtev.desc(8) = desc9 
    	    END

    'DONE' : BEGIN       ;

	    WIDGET_CONTROL, /DESTROY, ev.top
	    WIDGET_CONTROL, groupleader, /DESTROY

            for n=0,n_Elements(shtev.htfile) -1 do begin
    	      OPENW,outfile,shtev.htfile(n),/get_lun,/APPEND
	      FOR i=0,shtev.nf-1 DO 	PRINTF,outfile,'#FEAT_CODE: ',STRING(i,format='(i1)'),'=',shtev.desc(i)
	      free_lun,outfile
	    endfor
    	    RETURN
    	    END

ENDCASE

end

pro sccsetpoints_ht
COMMON scc_height_time
COMMON sccplot


        groupleader = Widget_Base(Map=0)
        Widget_Control, groupleader, /Realize
	htpbase = WIDGET_BASE(/COL, XOFFSET=275, YOFFSET=275, TITLE='SECCHI_HT_POINTS',group_leader=groupleader,/modal)
	rowa = WIDGET_BASE(htpbase, /ROW, /FRAME)

 	tmp  = WIDGET_LABEL( rowa,value='HT file = '+shtev.htfile(0))
 	if n_elements(shtev.htfile) eq 2 then begin
		rowa2 = WIDGET_BASE(htpbase, /ROW, /FRAME)
                tmp  = WIDGET_LABEL( rowa2,value='HT file = '+shtev.htfile(1))
        endif
	rowb = WIDGET_BASE(htpbase, /ROW)
	col1 = WIDGET_BASE(rowb, /COLUMN)
	col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)

; use CW_FIELD function

 	tmp  = WIDGET_LABEL( col2,value='Suggested feature codes: LE (leading edge), RHB (Right hand boundary), ')
	tmp  = WIDGET_LABEL( col2,value='LHB (Left hand boundary), BW (Black/white boundary), COR (Prominence Core), ')
	tmp  = WIDGET_LABEL( col2,value='DIS (Disconnection event), or none.')
       

               descv1= shtev.desc(0)
               descv2= shtev.desc(1)
               descv3= shtev.desc(2)
               descv4= shtev.desc(3)
               descv5= shtev.desc(4)
               descv6= shtev.desc(5)
               descv7= shtev.desc(6)
               descv8= shtev.desc(7)
               descv9= shtev.desc(8)

	if shtev.nf ge 1 then desc1 =   CW_FIELD(col2, VALUE=descv1, /ALL_EVENTS, UVALUE='desc1' , XSIZE=15, YSIZE=1, title='Code 1:')
	if shtev.nf ge 2 then desc2 =   CW_FIELD(col2, VALUE=descv2, /ALL_EVENTS, UVALUE='desc2' , XSIZE=15, YSIZE=1, title='Code 2:')
	if shtev.nf ge 3 then desc3 =   CW_FIELD(col2, VALUE=descv3, /ALL_EVENTS, UVALUE='desc3' , XSIZE=15, YSIZE=1, title='Code 3:')
	if shtev.nf ge 4 then desc4 =   CW_FIELD(col2, VALUE=descv4, /ALL_EVENTS, UVALUE='desc4' , XSIZE=15, YSIZE=1, title='Code 4:')
	if shtev.nf ge 5 then desc5 =   CW_FIELD(col2, VALUE=descv5, /ALL_EVENTS, UVALUE='desc5' , XSIZE=15, YSIZE=1, title='Code 5:')
	if shtev.nf ge 6 then desc6 =   CW_FIELD(col2, VALUE=descv6, /ALL_EVENTS, UVALUE='desc6' , XSIZE=15, YSIZE=1, title='Code 6:')
	if shtev.nf ge 7 then desc7 =   CW_FIELD(col2, VALUE=descv7, /ALL_EVENTS, UVALUE='desc7' , XSIZE=15, YSIZE=1, title='Code 7:')
	if shtev.nf ge 8 then desc8 =   CW_FIELD(col2, VALUE=descv8, /ALL_EVENTS, UVALUE='desc8' , XSIZE=15, YSIZE=1, title='Code 8:')
	if shtev.nf eq 9 then desc9 =   CW_FIELD(col2, VALUE=descv9, /ALL_EVENTS, UVALUE='desc9' , XSIZE=15, YSIZE=1, title='Code 9:')
	
	rowc = WIDGET_BASE(htpbase, /ROW)
	blk= '          '
	tmp  = WIDGET_LABEL(rowc, VALUE=blk)
	done = WIDGET_BUTTON(rowc, VALUE='  DONE  ', UVALUE='DONE')
	tmp  = WIDGET_LABEL(rowc, VALUE=blk)
	done = WIDGET_BUTTON(rowc, VALUE=' CANCEL ', UVALUE='CANCEL')

	
	WIDGET_CONTROL, htpbase, /REALIZE

	XMANAGER, 'sccsetpoints_ht', htpbase, EVENT_HANDLER='SCChtpoint_EVENT'
	
    	;**-----------------End widget --- start writing file ----------------**

end


PRO SCCPLOT_EVENT, ev

COMMON scc_height_time
COMMON sccplot

;WIDGET_CONTROL, ev.top, GET_UVALUE=htev   ; get structure from UVALUE

WIDGET_CONTROL, ev.id, GET_UVALUE=input

CASE strupcase(input) OF

    'CANCEL' : BEGIN       ;** exit program
	    htfile=''
            shtev.htfile=htfile
    	    WIDGET_CONTROL, /DESTROY, ev.top
	    WIDGET_CONTROL, groupleader, /DESTROY
    	    RETURN
    	    END

    'DONE' : BEGIN       ;

            for n=0,n_elements(shtev.htfile)-1 do begin
	    htfile=shtev.htfile(n)
	    type=shtev.type
	    nf=shtev.nf
	    comment=shtev.comment
	    help,htfile,type,nf,comment

    	    ;READ, 'Output filename for Height-Time Plot? ', tmp

	    IF rstrmid(htfile,0,2) NE 'ht' THEN htfile=htfile+'.ht'
	    filename = htfile
	    f = FINDFILE(htfile)

	    ok = CHECK_PERMISSION(htfile)
	    IF not ok THEN BEGIN
		break_file,htfile,a,dir,name,ext
		htfile = getenv_slash('HOME')+name+'.ht'
	        shtev.htfile(n) = htfile
 	    ENDIF

	    IF (f(0) NE '')  THEN BEGIN
		openr,lu,htfile,/get_lun
		fs=fstat(lu)
		first_line='VERSION'
		IF fs.size GT 0 THEN readf,lu,first_line  
		close,lu
		free_lun,lu
		IF STRPOS(first_line,'VERSION') NE -1 THEN BEGIN
		;sccREAD_HT,htfile,tai,rs,pa,comment,tel,curr_feat,col,row,date_obs,time_obs,fd
		;desc = fd
		ENDIF ELSE BEGIN
	    	    print,'A non-HT file exists with this name...please try again.'
	    	    return
		ENDELSE
       	    ENDIF
 	    print,'Saving ',htfile
    	    OPENW,outfile,htfile,/get_lun
            sc=strmid(hdr.filename,strpos(hdr.filename,'.')-1,1)
	    
	    parse_secchi_filename,hdr.filename,dobs,det,sc,wvl
	    IF sc NE 'A' and sc NE 'B' THEN BEGIN
	    	dets=['C1','C2','C3','EIT']
		x=where(dets EQ hdr.detector,nx)
		IF nx GT 0 THEN sc='SOHO' ELSE BEGIN
		    dets=['AIA','HMI']
		    x=where(dets EQ hdr.detector,nx)
		    IF nx GT 0 THEN sc='SDO' ELSE BEGIN
		    	inp=''
			read,'Spacecrafted not detected; please enter spacecraft name:',inp
			sc=strupcase(inp)
		    ENDELSE
		ENDELSE
	    ENDIF
	    ;tmp=''
	    ;PRINT,'Type of images? (difference, running difference, unsharp masking, etc.'
	    ;PRINT,'If difference, also enter date & time of base frame): ',tmp
	    ;nf = 0
	    PRINTF,outfile,'#VERSION=5'
	    PRINTF,outfile,'#DATE-OBS:	',strmid(hdr.date_obs,0,10)
	    PRINTF,outfile,'#TIME-OBS:	',hdr.time_obs
            PRINTF,outfile,'#SPACECRAFT:',sc
	    PRINTF,outfile,'#DETECTOR:	',hdr.detector
	    PRINTF,outfile,'#FILTER:	',hdr.filter
	    IF hdr.detector eq 'EIT' or hdr.detector EQ 'EUVI' THEN $
	    PRINTF,outfile,'#WAVELNTH:  ',hdr.wavelnth ELSE $
	    PRINTF,outfile,'#POLAR: 	',hdr.polar
	    PRINTF,outfile,'#OBSERVER:  ',getenv('USER')
	    PRINTF,outfile,'#IMAGE_TYPE:',STRUPCASE(type)
	    PRINTF,outfile,'#UNITS: 	',units
	    PRINTF,outfile,'#PLATESCALE:',string(hdr.cdelt1,format='(f8.4)')	; arcsec
	    Printf,outfile,'#RSUN:  	',string(hdr.rsun,format='(f9.4)')  ; arcsec
	    PRINTF,outfile,'#SUBFIELD[0,0]: ','[',string(mvhhdr.ccd_pos(1),'(i4)'),',',string(mvhhdr.ccd_pos(0),'(i4)'),']'
	    PRINTF,outfile,'#COMMENT: '+comment
	    ;nf=1
	    ;print,''
	    ;print,'You may click on up to 9 points (features) to follow in each frame.'
	    ;print,'A fit will be calculated for each feature.'
	    ;print,'Each frame must have the same number of clicks in the same order.'
	    ;read, 'How many points would you like to follow for this event? ',nf
	    free_lun,outfile

            ENDFOR

	    WIDGET_CONTROL, /DESTROY, ev.top
	    WIDGET_CONTROL, groupleader, /DESTROY
	    shtev.nf=nf<9

            sccsetpoints_ht

	    ;tmp = ''
	    ;READ,'Comment? ',tmp
	    ;return
	    END
    'HTFILEV' : BEGIN 	
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    htfile=val[0]
            if strpos(htfile,'.ht') eq -1 then htfile=val[0]+'.ht'
	    shtev.htfile(0) = htfile
    	    END

    'HTFILEVOSC' : BEGIN 	
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
       	    htfile=val[0]
            if strpos(htfile,'.ht') eq -1 then htfile=val[0]+'.ht'
	    shtev.htfile(1) = htfile
    	    END

    'TYPEV' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    type=val[0]
	    shtev.type = type 
    	    END

   'NFV' : BEGIN
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
            nf=fix(val[0])
	    shtev.nf=nf
	    ;WIDGET_CONTROL, htev.nft, SET_VALUE=STRTRIM(nf,2)
	    ;htev.nf = nf
    	    END

    'AUTOV' : shtev.auto = ev.index
    'LABELV' : shtev.label = ev.index

    'COMMENTV' : BEGIN 	
	    WIDGET_CONTROL, ev.id, GET_VALUE=val
	    COMMENT=val[0]
	    shtev.COMMENT = COMMENT
    	    END

    ELSE : BEGIN
            PRINT, '%%SCC_PLOT_EVENT.  Unknown event.'
    	    END

ENDCASE

;WIDGET_CONTROL, ev.top, SET_UVALUE=htev

END


PRO sccWRITE_HT,hdr0,elong,mvhhdr0,ab=ab

COMMON scc_height_time	; defined above
COMMON sccplot


IF elong eq 1 then units='Degrees' else $
IF elong EQ 2 THEN units='Deg(CLN)' else units='R/Rsun'

mvhhdr=mvhhdr0
hdr=hdr0
; scc_wrunmoviem should properly populate hdr.time_obs
hdr_date_obs = strmid(hdr.date_obs,0,10)
hdr_time_obs = hdr.time_obs
sc=strmid(hdr.filename,strpos(hdr.filename,'.')-1,1)
cam=hdr.detector
npts = 0

resetv=0
IF (DATATYPE(shtev) EQ 'UND') THEN resetv=1 else begin
  if shtev.cam ne cam then resetv=1
  if shtev.sc ne sc then resetv=1
  if shtev.hdr_date_obs ne hdr_date_obs then resetv=1
  if n_elements(shtev.htfile) eq 2 and ~keyword_set(ab) then resetv=1
endelse
  	 ;** restore previous values

;help,hdr_time_obs,curr_time,curr_feat

  ; First get input from user via widget
        if keyword_set(ab) then htfile=strarr(2) else htfile=strarr(1)
	IF (resetv eq 1) THEN BEGIN	;** use defaults
		htfile(0) = sc+'_'+cam+'_'+hdr_date_obs+'.ht'
		type = 'rd'
		nf = 1
		comment=''
                if keyword_set(ab) then begin
		  if strlowcase(sc) eq 'a' then osc='B' else osc='A'
		  htfile(1)=osc+'_'+cam+'_'+hdr_date_obs+'.ht'
                endif
	ENDIF ELSE BEGIN	 ;** restore previous values
		htfile(0) = shtev.htfile(0)
                if keyword_set(ab) then htfile(1) = shtev.htfile(1)
		type = shtev.type
		nf = shtev.nf
		comment=shtev.comment
	ENDELSE

	;**--------------------Create Widget-------------------------------**
        groupleader = Widget_Base(Map=0)
        Widget_Control, groupleader, /Realize
	whtbase = WIDGET_BASE(/COL, XOFFSET=275, YOFFSET=275, TITLE='SECCHI_HT_PLOT',group_leader=groupleader,/modal)
	rowb = WIDGET_BASE(whtbase, /ROW)
	col1 = WIDGET_BASE(rowb, /COLUMN)


	col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)

; use CW_FIELD function

	htfilev =   CW_FIELD(col2, VALUE=htfile(0), /ALL_EVENTS, XSIZE=25, YSIZE=1, UVALUE='HTFILEV', title='Output Filename:')
	if keyword_set(ab) then htfilev =   CW_FIELD(col2, VALUE=htfile(1), /ALL_EVENTS, XSIZE=25, YSIZE=1, UVALUE='HTFILEVOSC', title='OSC Output Filename:')
	typev =     CW_FIELD(col2, VALUE=type, /ALL_EVENTS, XSIZE=15, YSIZE=1, UVALUE='TYPEV', title='Type of images:')
	if keyword_set(ab) then nfv = CW_FIELD(col2, VALUE=trim(nf), /ALL_EVENTS, XSIZE=2, YSIZE=1, UVALUE='NFV', title='Number of Points (per side) Max = 9 :')else $
	     nfv = CW_FIELD(col2, VALUE=trim(nf), /ALL_EVENTS, XSIZE=2, YSIZE=1, UVALUE='NFV', title='Number of Points Max = 9 :')
        autot=['off','on'] & auto=0
        autov=widget_droplist(col2, VALUE=autot, UVALUE='autov',title='Auto Advance :')
        widget_control,autov,set_droplist_select=auto        
        labelt=['off','on'] & label=0
        labelv=widget_droplist(col2, VALUE=labelt, UVALUE='labelv',title='Label Features :')
        widget_control,labelv,set_droplist_select=label        
	commentv =  CW_FIELD(col2, VALUE=comment, /ALL_EVENTS, XSIZE=25, YSIZE=1, UVALUE='COMMENTV', title='Comment:')

	rowc = WIDGET_BASE(whtbase, /ROW)
	blk= '          '
	tmp  = WIDGET_LABEL(rowc, VALUE=blk)
	done = WIDGET_BUTTON(rowc, VALUE='  DONE  ', UVALUE='DONE')
	tmp  = WIDGET_LABEL(rowc, VALUE=blk)
	cancel = WIDGET_BUTTON(rowc, VALUE=' CANCEL ', UVALUE='CANCEL')

    	;**--------------------Done Creating Widgets-----------------------------**

    	shtev={htfile:htfile,  type:type,  nf:nf,  comment:comment, sc:sc, cam:cam, $
	     hdr_date_obs:hdr_date_obs, desc:strarr(9), auto:auto, label:label }
	
	WIDGET_CONTROL, whtbase, /REALIZE

	XMANAGER, 'sccWRITE_HT', whtbase, EVENT_HANDLER='SCCPLOT_EVENT'
	
    	;**-----------------End widget --- start writing file ----------------**

END
  

