;+
; $Id: get_hdr_movie_img.pro,v 1.3 2014/06/11 14:34:54 hutting Exp $
;
; Project     : STEREO - SECCHI, SOHO - LASCO/EIT
;
; Name        : get_hdr_movie_img
;
; Purpose     : Reads in png or gif files listed in hdr movie files. 
;
; Use         : IDL> img=get_hdr_movie_img(filein,truecolor=truecolor,mviname=mviname)
;
; Inputs      : filein name of image file read in from sccread_mvi.
;	          
;
; Optional Inputs:
;
; Outputs     :
;	imgi  	image byte array
;
; Keywords    :truecolor 
;              mviname = name of input movie used to get the path of the image file.
;
; Comments    :
;
; Side effects: 
;
; Category    : Image Display.  Animation. MVI
;
; Modifications:
;
; $Log: get_hdr_movie_img.pro,v $
; Revision 1.3  2014/06/11 14:34:54  hutting
; Will read in a fts file movie
;
; Revision 1.2  2012/04/25 12:49:39  mcnutt
; added jpeg option
;
; Revision 1.1  2010/09/20 18:44:35  mcnutt
; moved for inside scc_playmovie and sccread_mvi for compiling pruposes
;
;-
;-----------------------------------------------------------------------
;
function get_hdr_movie_img,filein,truecolor=truecolor,mviname=mviname
              if keyword_set(mviname) then begin
	         BREAK_FILE, mviname, a, dir, name, ext
                 filein=dir+filein
	      endif
;              filein=string(imgsin(findx[i]))
	      if strpos(filein,'.fts') gt -1 then imgt=sccreadfits(filein,hdr)
	      if strpos(filein,'.png') gt -1 then imgt=read_png(filein,r,g,b)
	      if strpos(filein,'.gif') gt -1 then read_gif,filein,imgt,r,g,b
	      if strpos(filein,'.jpg') gt -1 then begin
	        if ~keyword_set(truecolor) then begin 
                  READ_JPEG,filein , imgt ,/TWO_PASS_QUANTIZE ,/grayscale
                endif else begin
		  READ_JPEG, filein, imgt ,/TWO_PASS_QUANTIZE ,ctable,COLORS=!D.N_COLORS-1,DITHER=1
                  r=ctable(*,0) & g=ctable(*,1) & b=ctable(*,2)
                endelse
              endif
              tcheck=size(imgt)
	      IF ~keyword_set(truecolor) or tcheck(0) eq 3 then begin
	        imgi=imgt 
	      endif else begin
                imgi=bytarr(3,n_Elements(imgt(*,0)),n_Elements(imgt(0,*)))
                imgi(0,*,*)=r(imgt) & imgi(1,*,*)=g(imgt) & imgi(2,*,*)=b(imgt)
	      ENDELSE
         return,imgi
END 
;____________________________________________________________________________
