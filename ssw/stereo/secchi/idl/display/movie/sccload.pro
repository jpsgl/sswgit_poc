;SCCLOAD -- load pre-secchi-prepped image list from local directory

PRO sccload, files, hdrs, imgs, NODATA=nodata, NOLOCAL=nolocal, EXPCORR=expcorr
;+
; $Id: sccload.pro,v 1.4 2009/01/15 22:17:12 nathan Exp $
;
; Name        : sccload
;
; Purpose     : load pre-secchi-prepped image list from local directory
;               
; Explanation : 
;               
; Use         : sccload, files, hdrs, imgs
;    
; Inputs      : files
;               
; Outputs     : hdrs, imgs
;               
; Keywords    : /NODATA : return header only
;             : /NOLOCAL
;   	/EXPCORR    Do multi-exposure readout correction for HI images 
;
; Calls       : 
;
; Side effects: 
;               
; Category    : Image Display calibration inout
;
; $Log: sccload.pro,v $
; Revision 1.4  2009/01/15 22:17:12  nathan
; print filename before reading
;
; Revision 1.3  2008/08/20 17:40:27  nathan
; added /EXPCORR option
;
; Revision 1.2  2008/03/17 12:37:27  mcnutt
; added basic header info for wiki page
;
; Revision 1.1  2007/08/17 17:31:38  nathan
; moved from dev/movie
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;
nim = n_elements(files)

local = ~keyword_set(NOLOCAL)
IF(local) THEN BEGIN
	pos = STRPOS(files[0], 'lz/L0')
	list = '/home/herbst/images/secchi/' + STRMID(files, pos)
ENDIF ELSE list = files

;stop
fn=list[0]

IF keyword_set(EXPCORR) THEN BEGIN
    secchi_prep,fn,hdrs,imgs, saturation_limit=16384 
    hdrs.exptime=1.
ENDIF ELSE $
    imgs = sccreadfits(fn, hdrs, NODATA=nodata)
IF(nim EQ 1) THEN RETURN

s = size(imgs)
img = imgs
imgs = fltarr(s[1], s[2], nim)
imgs[*,*,0] = img
hdrs = replicate(hdrs, nim)

FOR i=1, nim-1 DO BEGIN
    	fn=list[i]
	print,fn
	IF keyword_set(EXPCORR) THEN BEGIN
	    secchi_prep,fn,hdr,img, saturation_limit=16384 
	    hdr.exptime=1.
	ENDIF ELSE $
	    img = sccreadfits(fn, hdr, NODATA=nodata)
	imgs[*,*,i] = img
	hdrs[i] = hdr
ENDFOR

END
