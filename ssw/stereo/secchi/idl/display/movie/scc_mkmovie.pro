;+
;$Id: scc_mkmovie.pro,v 1.80 2015/08/26 17:13:28 hutting Exp $
;
; Project     : STEREO - SECCHI
;                   
; Name        : SCC_MKMOVIE
;               
; Purpose     : Load processed FITS files into pixmaps and call SCC_PLAYMOVIE for animation.
;               
; Explanation : This procedure is a wrapper for scc_mkframe.pro which processes individual FITS 
;   	    	files for display. It sorts the images by time, passes options 
;   	    	to scc_mkframe, stores pixmap information, and then calls SCC_PLAYMOVIE to start 
;   	    	a widget interface for the animation sequence.  
;               
; Use         : SCC_MKMOVIE, list [, bmin, bmax, ... keywords ]

;                        /RATIO, /USE_MODEL,/MASK_OCC,/LG_MASK_OCC
;                        /RUNNING_DIFF, /DEGRID, /FIXGAPS, /LOG_SCL, /SQRT_SCL, $
;			 FILL_COL=fill_col, SAVE=save, /NOCAL ]
;
;      Example: IDL> SCC_MKMOVIE, list_of_files, 0.9,1.5, /TIMES, /RATIO, PAN=512, $ 
;   	    	    	COORDS=[0,511,0,511], /USE_MODEL, /MASK_OCC, /LIMB, /OUTER
;
;   	    	will generate a 128x128 movie of the lower-left 16th of a full frame of the type of 
;   	    	files in list_of_files that have been divided by a monthly_min image scaled between 
;   	    	0.9 and 1.5 with the area outside the field of view and around the occulter masked 
;   	    	with the median of the image and a circle drawn where the limb should be.
;
;      Example: If you want to display BYTARR images straight from the FITS files without
;		any scaling use:
;               IDL> SCC_MKMOVIE, list, 0,8000(ish), /NO_SORT
;    
; Inputs      : list : Name of file containing names of FITS files.
;		       Or a STRARR of the image names.
;
; Optional Inputs:
;               bmin, bmax : Minimum and maximum DN for BYTSCL after processing. 
;   	    	    	     Defaults to 0, 2*median, except EUVI calls scc_bytscl.
;   	    	cam 	   : lowercase detector. Used to determine which color table to load.
;               
; Outputs     : None.
;               
; Keywords    : /TWO	     Retrieve corresponding image from other spacecraft and make a B/A movie.
;   	    	    	    Set =2 to display A on left and B on right (default for HI)
;   	    	/NEWBKG     Refresh background model for each frame
;   	    	AVERAGE_FRAMES= Number of frames to average together using rebin
;   	    	/USE_LAST_COORDS    Use the subfield in common block
;   	    	/VERBOSE   : Print more info about each frame
;		/DIFF      : Set this keyword to make a difference movie.  The first image is subtracted 
;   	    	    	     from all subsequent images.
;		/RUNNING_DIFF: Make a difference movie, subtracting the preceding image.
;		/RATIO     : Display data as ratio with the reference image
;		/USE_MODEL : If using /DIFF or /RATIO options, use image from $SECCHI_BKG/?/bkgtype where 
;   	    	    	     bkgtype=(['','monthly_min','daily_med'])[use_model value] as reference image.
;   	    	    	     May also set this to an array to use as reference image.
;   	    	    	     For LASCO, bkgtype= (['',current year, any-year])[use_model value]
;		/AUTOMAX   : Compute bmin and bmax based on median (for EUVI, calls scc_bytscl.pro)
;		PAN=       : Size of square FFV, one side
;    	    	/GETSUBFIELD: If set, then use box_cursor to retrieve subfield from first frame
;		COORDS=    : Set to 4 element array of image coordinates [x1,x2,y1,y2] to use relative 
;   	    	    	     to 2048x2048 image.
;			     Example: COORDS=[0,2047,512,1535] for Equatorial Field
;   	    	/USE_SHARP : Use sharpen.pro to produce a "Sharpen Ratio" image
;   	    	SHARP_FACTOR=	3rd argument to sharpen.pro
;               /UNSHARP    :Set this keyword to make a movie of unsharp masked images. 
;   	    	SMOOTH_BOX : If GT 1 and /USE_SHARP is not set, smooth with this parameter.
;   	    	    	     If /USE_SHARP, use this box size (min 3).
;		/LOGO	   : Add SECCHI logo to bottom right corner
;               /TIMES	   : Set this keyword to display date, time and detector in lower left corner 
;   	    	    	     of frame. Set equal to a value GT 1 to increase the size of hershey font.
;   	    	/FMASK	   : Apply a mask from file in $SECCHI_CAL
;               /MASK_OCC  : applies a circular mask over occulter 
;   	    	    	     (COR1 1.2*Rsun, COR2-A 2.4*Rsun, COR2-B 2.6*Rsun)
;               /LG_MASK_OCC: applies a larger circular mask over occulter 
;   	    	    	     (COR1 1.5*Rsun, COR2-A 3*Rsun, COR2-B 3.3*Rsun)
;		/OUTER	   : Fill in outer mask only 
;   	    	    	     (COR1 4*Rsun, COR2-A 1033*img_pan, COR2-B 1035*img_pan, EUVI 1.6*Rsun) 
;   	    	/LIMB	   : Draw a circle where the limb of the sun would be
;		/FIXGAPS   : Set to 1 to fill data gaps in image with color specified by FILL_COL
;		             Set to 2 to fill data gaps in image with values from previous image
;		FILL_COL=  : Set this keyword to the color index (1-255) to use for data gaps and masks. 
;   	    	    	     Default is to use image median.
;		/NOEXPCORR : Don't do exposure time correction, but subtract bias
;		/STAIND	   : Start with this position in the input file (for output filenames)
;   	    	/DCOLOR    : Load color table for given telescope
;		/NO_SORT   : Don't sort input list by DATE-CMD (filename)
;		/BAD_SKIP  : Skip bad frames based on nonzero median
;		SAVE=	   : For use in batch mode.  Set this keyword to the name of the .mvi file
;			     to save as. IF SDIR is set it will be prepended to this value. Routine 
;   	    	    	     will save movie and then exit.
;   	    	    	     To save as individual frames, you can set SAVE='/directory/yyyymmdd_hhmmss_anytext.png'
;   	    	    	     and frames will be saved in /directory with timestamp as indicated. 
;		SDIR=	   : Directory is appended to SAVE= value.
;		/TIFF, /PNG, /PICT, /GIF, /JPG, /MPG, /MOV, /AVI	   
;   	    	    	    : Save movie frames in given format. Filename will be SAVE value (see SAVE= above).
;   	    	    	      Optionally set TIFF= (etc.) to path for saving filename.
;		/LOG_SCL   : Applies ALOG10() function to image before byte scaling 
;		/SQRT_SCL  : Applies SQRT() function to image before byte scaling
;		/LEE_FILT  : Apply Leefilt function to filter noise.
;		BOX=       : Set to 4 element array of image coordinates to use for box normalization 
;			     relative to (rectified) 2048x2048 image. Images are scaled relative to mean 
;   	    	    	     of this subfield of first image. 
;		REF_BOX    : Set to avg counts of BOX (after secchi_prep) to use as reference otherwise 
;   	    	    	     first image is used
;               /NOCAL	   : Sets /CALIMG_OFF for secchi_prep
;   	    	/PHOTOCAL  : Apply photometric calibration (default not to)
;   	    	/DOROTATE  : Make Solar north up
;   	    	DBIAS =      amount (DN) to subtract from raw image, in addition to bias
;
;		
; Calls       : SCC_MKFRAME, SCC_PLAYMOVIE
;
; Side effects: Creates multiple pixmaps.
;               
; Category    : Image Display movie animation 
;               
; See Also    : WSCC_MKMOVIE.PRO is a widget front-end to this procedure.
;               GENERIC_MOVIE.PRO reads in bytescaled fits or gif files and creates a movie.
;               
; Prev. Hist. : Based on MKMOVIE from LASCO/EIT. 
;
; History     : Ed Esfandiari, NRL, Sep 2006.
;               Ed Esfandiari, Oct 10, 06 - Changed call from wrunmovie to scc_wrunmovie.
;               Ed Esfandiari, Aug 15, 07 - Added call to hi_correction.
;               Ed Esfandiari, Sep 24, 07 - corrected scale factor.
;               
;-            
;$Log: scc_mkmovie.pro,v $
;Revision 1.80  2015/08/26 17:13:28  hutting
;checks to see if skipped was set after scc_mkframe call
;
;Revision 1.79  2013/08/23 21:16:23  nathan
;add * to file_exist test in case of .gz files
;
;Revision 1.78  2012/10/10 12:37:45  mcnutt
;now works with full res AIA fits
;
;Revision 1.77  2012/09/05 14:21:04  mcnutt
;corrected ffmpg, pict, and tiff output movie types
;
;Revision 1.76  2012/08/22 17:59:44  mcnutt
;changed pan to work with FFV when nointerp keyword is set for crop_by_corrds
;
;Revision 1.75  2012/08/16 17:03:00  mcnutt
;added uselectbmin array, works like ubmin and ubmax for A/B/SDO combo movies
;
;Revision 1.74  2012/08/01 14:41:33  mcnutt
;does not set imagecnt if twoval eq 0
;
;Revision 1.73  2012/07/27 21:08:07  nathan
;do not get_screen_size so works with Z device
;
;Revision 1.72  2012/07/25 21:52:19  nathan
;restore expected behavior of SDIR
;
;Revision 1.71  2012/07/20 11:49:28  mcnutt
;sets file_hdr.nx and ny to image size before saving first frame of movie
;
;Revision 1.70  2012/07/18 15:26:41  mcnutt
;moved save movie for loop to scc_save_mvframe
;
;Revision 1.69  2012/07/13 23:04:00  nathan
;Removed mviname and mpegname keywords and replaced with MPG, MOV, AVI, PNG,
;TIFF, PICT, GIF keyword options; if SAVE is set, always use scc_save_mvframe.pro
;to save, do not call scc_playmovie.pro with SAVE
;
;Revision 1.68  2012/07/13 19:16:33  nathan
;added swmoviev.margin
;
;Revision 1.67  2012/07/13 18:05:17  mcnutt
;corrected last change
;
;Revision 1.66  2012/07/13 18:04:13  mcnutt
;do not write pixmaps if mpegname or mviname keywords are set
;
;Revision 1.65  2012/07/13 17:49:01  mcnutt
;added mpegname and mviname keywords used to write movie inline and not call scc_playmovie
;
;Revision 1.64  2012/01/25 17:47:32  mcnutt
;use yloc instead of i to set selectbmin after first frame
;
;Revision 1.63  2012/01/04 17:56:05  mcnutt
;check for keyword selectbmin not value
;
;Revision 1.62  2012/01/03 17:15:35  mcnutt
;added 3 byte scale options
;
;Revision 1.61  2011/12/28 18:23:07  mcnutt
;call scc_mkframe on first image to select coordcrop subfield
;
;Revision 1.60  2011/12/27 19:11:15  mcnutt
;added crop by coord and AIA combination options
;
;Revision 1.59  2011/11/01 20:49:25  nathan
;do not file_exist(files*) because of ssdfs0
;
;Revision 1.58  2011/10/19 19:31:13  nathan
;check for file_exist(names*)
;
;Revision 1.57  2011/06/14 18:04:52  nathan
;fix rectified call to scc_playmovie
;
;Revision 1.56  2011/05/18 22:40:10  nathan
;change references to wrunmovie to playmovie
;
;Revision 1.55  2011/05/18 22:13:30  nathan
;fix default coords for cor1 or lasco input
;
;Revision 1.54  2011/04/28 19:33:37  nathan
;accomodate pan lt 1
;
;Revision 1.53  2011/03/22 19:24:08  mcnutt
;added point filter
;
;Revision 1.52  2011/01/03 19:45:48  nathan
;define hi.naxis for case where COORDS= not set and NO_SORT
;
;Revision 1.51  2010/12/23 16:37:21  nathan
;check for missing files
;
;Revision 1.50  2010/12/13 19:58:17  mcnutt
;set coords based on hdr naxis values to work with lasco and eit
;
;Revision 1.49  2010/11/24 18:22:18  nathan
;changes so dorotate passed to scc_playmovie; use sccreadfits for lasco hdr time order
;
;Revision 1.48  2010/11/17 23:28:42  nathan
;remove set color table; do time sort for SOHO; pass rectified to scc_playmovie
;
;Revision 1.47  2010/08/25 20:54:58  nathan
;fix sort filenames
;
;Revision 1.46  2010/08/17 16:01:24  nathan
;added /DEBUG
;
;Revision 1.45  2010/04/19 18:37:11  mcnutt
;added nrg filter and interactive bytescale select
;
;Revision 1.44  2010/01/22 13:13:18  mcnutt
;corrected hdr1 if single sc movie
;
;Revision 1.43  2010/01/15 12:58:58  mcnutt
;corrected A on left
;
;Revision 1.42  2010/01/14 20:06:09  mcnutt
;added frame select option for ab movies
;
;Revision 1.41  2010/01/14 13:31:26  mcnutt
;added options for other SC on top/left/bottom/right
;
;Revision 1.40  2009/10/15 16:58:55  mcnutt
;correct tif file ext and Reversed the Tiff image
;
;Revision 1.39  2009/10/15 16:28:06  mcnutt
;check for keywords tiff png or pict before truncating imgs array
;
;Revision 1.38  2009/09/24 19:38:16  mcnutt
;addes textframe to images if defined
;
;Revision 1.37  2009/04/13 22:14:23  nathan
;add findx to common block to match generic_movie.pro
;
;Revision 1.36  2009/03/25 17:26:58  nathan
;Do not pop any visible windows if SAVE= is set
;
;Revision 1.35  2009/03/20 21:46:52  nathan
;fixed averaging buffer logic so skipped frames are handled
;
;Revision 1.34  2009/03/18 19:13:28  nathan
;Changed meaning of opts[10] from A and B use all to A and B put A on left
;(TWO=2)
;
;Revision 1.33  2009/03/17 14:45:23  mcnutt
;defines single if swmoviev is not defined
;
;Revision 1.32  2009/03/10 19:00:17  mcnutt
;works with a single image
;
;Revision 1.31  2008/11/10 17:59:50  mcnutt
;returns if image from scc_mkframe is an error message and not a bytarr
;
;Revision 1.30  2008/10/22 18:22:52  nathan
;fix incorrect value for NAMES= to scc_wrunmovie
;
;Revision 1.29  2008/10/21 17:36:51  mcnutt
;added object option to add Earth, SOHO and other stereo sc positions to HI2 movies
;
;Revision 1.28  2008/09/26 19:05:56  mcnutt
;added lag variable (n previous images) in running differences movie type
;
;Revision 1.27  2008/09/22 16:54:48  nathan
;changed MEDIAN_FILTER to AVERAGE_FRAMES
;
;Revision 1.26  2008/09/18 21:56:20  nathan
;Added keyword SMOOTH_BOX and average_frames, which uses rebin in this pro
;to combine several frames into one
;
;Revision 1.25  2008/08/25 16:39:10  nathan
;added /PHOTOCAL
;
;Revision 1.24  2008/07/07 21:04:08  nathan
;fix bug when complement missing (name2); remove suncen and sec_pix from wrunmovie call
;
;Revision 1.23  2008/05/20 19:11:47  nathan
;Implemented /USE_LAST_COORDS which entailed adding coordsab to
;WSCC_MKMOVIE_COMMON; ensure frame header is for left/bottom image; fixed
;bug when input file in list is skipped
;
;Revision 1.22  2008/04/29 15:40:09  nathan
;use yloc eq 0 instead of yloc eq startind
;
;Revision 1.21  2008/04/21 15:53:17  nathan
;always skip if /two and ahead is missing
;
;Revision 1.20  2008/04/15 20:39:03  nathan
;Changes to accomodate ability to update color tables in scc_wrunmovie:
;- disabled loadct,0 as default
;- save images to imgs in common block
;- do side-by-side in scc_mkframe using Z-buffer
;Do A first not B so not shrinking A
;
;Revision 1.19  2008/04/14 15:59:24  nathan
;added daily_med option for use_model
;
;Revision 1.18  2008/04/08 16:37:19  nathan
;fix /png feature
;
;Revision 1.17  2008/04/04 18:55:05  nathan
;added verbose message
;
;Revision 1.16  2008/04/02 16:06:28  nathan
;Implmented /TWO
;
;Revision 1.15  2008/03/31 19:33:28  nathan
;Initial implementation of /TWO for displaying A and B next to each other.
;To do this required moving the window and tv of final frame to scc_mkframe.pro
;loop.
;
;Revision 1.14  2008/03/11 19:42:28  nathan
;updated header info
;
;Revision 1.13  2008/02/26 16:54:49  mcnutt
;loads ct 0 is docolor is not selected
;
;Revision 1.12  2008/02/21 21:37:50  nathan
;changed names and hdr arguments for mkframe to be scalar
;
;Revision 1.11  2008/02/21 13:24:36  mcnutt
;modified to work with Zbuffer in scc_mkframe.pro
;
;Revision 1.10  2008/02/13 17:24:32  mcnutt
;works with new scc_mkframe
;
;Revision 1.9  2008/02/08 19:36:13  mcnutt
;added keyword automax and docolor
;
;Revision 1.8  2008/02/01 14:58:22  nathan
;remove common mvmask from scc_mkmovie (define in scc_mkframe only)
;
;Revision 1.7  2008/01/22 21:21:47  nathan
;added _EXTRA to allow other secchi_prep keywords (such as /rotate_on)
;
;Revision 1.6  2008/01/15 14:48:08  mcnutt
;added sunc1 to mask common block and added limb as input
;
;Revision 1.5  2008/01/11 17:19:50  mcnutt
;change automax to keyword to work with mkfram
;
;Revision 1.4  2008/01/11 16:33:31  nathan
;Permanently moved from dev/movie to secchi/idl/display/movie
;
;Revision 1.3  2008/01/11 15:27:51  mcnutt
;added keyword and common block for mask save files
;
;Revision 1.11  2007/12/10 22:27:44  nathan
;Implemented subfields; moved much of frame-specific settings from mkmovie to mkframe;
;changed some of the mkframe input parameters; changed date color cutoff from 100 to 130
;
;Revision 1.10  2007/12/06 21:00:31  nathan
;updated definition of TIMES keyword
;
;Revision 1.2  2007/10/17 21:48:51  nathan
;latest revision from dev/movie (rev 1.9)
;
;Revision 1.9  2007/10/17 18:43:58  nathan
;added /DEBUG call to scc_wrunmovie
;
;Revision 1.8  2007/10/05 21:06:57  nathan
;Major update for SECCHI, using secchi_prep, and making sure options work
;correctly. Still TBD: image subfields, optimize masks, etc.
;
;Revision 1.7  2007/09/24 19:08:40  esfand
;AEE - Fixed scale and re-enabled calibrations
;
;Revision 1.6  2007/09/06 15:03:00  nathan
;fix sharp_factor; use scc_add_mvi_logo; omit calibration until ready
;
;Revision 1.5  2007/08/17 15:57:51  herbst
;Mods for N.Sheeley Summer 2007
;
;Revision 1.4  2007/03/28 19:41:00  esfand
;secchi movie routines as of Mar 28, 07
;
;Revision 1.3  2006/10/10 20:26:29  esfand
;change call from wrunmovie to scc_wrunmovie
;
;Revision 1.2  2006/10/05 20:53:55  esfand
;first virsion - aee
;
;
; 
;____________________________________________________________________________
;

PRO SCC_MKMOVIE, list, bmin, bmax, cam, LEE_FILT=lee_filt, DIFF=diff, times=times, DBIAS=dbias,$
RUNNING_DIFF=running_diff, NO_NORMAL=no_normal, AUTOMAX=automax, DCOLOR=dcolor, LOGO=logo, $
NO_SORT=no_sort, UNSHARP=unsharp, RATIO=ratio, USE_MODEL=use_model, TWO=two, SMOOTH_BOX=smooth_box, $
MASK_OCC=mask_occ, FIXGAPS=fixgaps, LOG_SCL=log_scl, GETSUBFIELD=getsubfield, $
LG_MASK_OCC=lg_mask_occ, DOROTATE=dorotate, PHOTOCAL=photocal, AVERAGE_FRAMES=average_frames, $
SQRT_SCL=sqrt_scl, FILL_COL=fill_col, PAN=pan, COORDS=coords, USE_LAST_COORDS=use_last_coords, $
BOX=box, SAVE=save, REF_BOX=box_ref, USE_SHARP=use_sharp, SHARP_FACTOR=sharp_fact, $
SDIR=sdir, STAIND=staind, OUTER=outer, LIMB=limb, NOEXPCORR=noexpcorr, $
BAD_SKIP=bad_skip, TIFF=tiff, PNG=png, PICT=pict, JPG=jpg, GIF=gif, MOV=mov, AVI=avi, MPG=mpg,$
VERBOSE=verbose, DEBUG=debug, $
NOCAL=nocal, FMASK=fmask,objects=objects, donrfg=donrfg,selectbmin=selectbmin, PFILTER=pfilter, coordcrop=coordcrop, _EXTRA=_extra

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

IF keyword_set(DEBUG) THEN verbose=1

; -- First, set all paramaters without needing to know image header; THEN read in first image
; -- with scc_mkframe.pro
;
If keyword_set(PAN) THEN IF pan LT 3 THEN pan=pan*1024.
; accomodate LASCO definition of PAN

   if datatype(swmoviev) ne 'UND' then single=swmoviev.single else single=0
   if datatype(swmoviev) ne 'UND' then margin=swmoviev.margin else margin=0
   
   IF (N_ELEMENTS(list) EQ 1) and single ne 1 THEN BEGIN	;** read names from file
      str = STRARR(1)
      OPENR, UNIT, list, /GET_LUN
      READF, UNIT, str
      names = str
      WHILE (NOT EOF(UNIT)) DO BEGIN
         READF, UNIT, str
         names = [names, str]
      ENDWHILE
      CLOSE, UNIT
      FREE_LUN, UNIT
   ENDIF ELSE BEGIN
      names = list
   ENDELSE
   
   ;--Number of input filenames
    len = N_ELEMENTS(names)
    message,'Verifying existence of '+trim(len)+' files.',/info
    exist=file_exist(names+'*')
    ok=where(exist,nok,complement=missing)
    IF nok LT len THEN BEGIN
    	print,'Skipping these files which are missing:'
	print,names[missing]
	wait,5
	names=names[ok]
	len=nok
    ENDIF
   tai_all = DBLARR(len)
   exp_all = FLTARR(len)
   bias_all = FLTARR(len)	
   fpwvl_all=fltarr(len)
   filter_all=strarr(len)
;   all_hdr = replicate(def_secchi_hdr(),len)
;   roll_times,statai,entai	; ** Get SOHO roll times
;   n_rolls = n_elements(statai)
   rolld=bytarr(len)
   ftimes= STRARR(len)
   totskipped=0
   naxis1=0
   naxis2=0

   ;** sort on date/time in header
   ; use filenames instead of date_obs of each image to save time (assuimg
   ; date_obs is used in filename):

   IF NOT(KEYWORD_SET(NO_SORT)) THEN BEGIN
	msg='filename.'
	break_file,names,di,pa,ro,su
	IF strlen(ro[0]) LT 14 THEN BEGIN
	    jk=sccreadfits(names,hi,/nodata,/lasco)
	    nm=hi.date_obs+hi.time_obs
	    msg='date-obs+time-obs.'
	    naxis1=hi[0].naxis1
	    naxis2=hi[0].naxis2
	ENDIF ELSE $
	    nm=ro
	
    	srt= SORT(nm)
    	names= names(srt)
    	print,'Files sorted by ',msg
   ENDIF ELSE print, '!!! Files not sorted. !!!'


IF keyword_set(SAVE) or keyword_set(SDIR) THEN BEGIN
    types=['.mvi','.mpg','.ffm','.avi','.mov','.png','.gif','.pic','.tif','.jpg']
    outtyps=[1, keyword_set(MPG), 0, keyword_set(AVI), keyword_set(MOV), keyword_set(PNG), keyword_set(GIF), keyword_set(PICT), keyword_set(TIFF), keyword_set(JPG)]
    ; Type is always MVI unless input SAVE= has a different suffix.
    IF keyword_set(SAVE) THEN $
    	break_file,save,di1,pa1,ro1,sfx1 $
    ELSE BEGIN
    	save=1
	break_file,names[0],di0,pa0,ro0,sfx1
	ttt=rstrmid(ro0,0,6)
	ro1='yyyymmdd_hhmmss'+ttt
    ENDELSE
    wfx=where(types EQ strmid(sfx1,0,4),nwfx)
    IF nwfx GT 0 THEN outtyps[wfx]=1
    
    IF sfx1 NE '' and sfx1 NE '.mvi' THEN outtyps[0]=0
    
    IF keyword_set(SDIR) THEN mviout=concat_dir(sdir,ro1) ELSE mviout=di1+pa1+ro1
    wtypes=where(outtyps,ntyps)
    outfiles=strarr(ntyps)
    FOR i=0,ntyps-1 DO $
    CASE (types[wtypes[i]]) of 
    	'.mvi': outfiles[i]=mviout+'.mvi'
	'.mpg': IF datatype(mpg) EQ 'STR' THEN outfiles[i]=concat_dir(mpg,ro1+'.mpg') ELSE outfiles[i]=mviout+'.mpg'
	'.ffm': IF datatype(ffm) EQ 'STR' THEN outfiles[i]=concat_dir(mpg,ro1+'.ffmpg') ELSE outfiles[i]=mviout+'.ffmpg'
	'.avi': IF datatype(avi) EQ 'STR' THEN outfiles[i]=concat_dir(avi,ro1+'.avi') ELSE outfiles[i]=mviout+'.avi'
	'.mov': IF datatype(mov) EQ 'STR' THEN outfiles[i]=concat_dir(mov,ro1+'.mov') ELSE outfiles[i]=mviout+'.mov'
	'.png': IF datatype(png) EQ 'STR' THEN outfiles[i]=concat_dir(png,ro1+'.png') ELSE outfiles[i]=mviout+'.png'
	'.gif': IF datatype(gif) EQ 'STR' THEN outfiles[i]=concat_dir(gif,ro1+'.gif') ELSE outfiles[i]=mviout+'.gif'
	'.pic': IF datatype(pict) EQ 'STR' THEN outfiles[i]=concat_dir(pict,ro1+'.pict') ELSE outfiles[i]=mviout+'.pict'
	'.tif': IF datatype(tiff) EQ 'STR' THEN outfiles[i]=concat_dir(tiff,ro1+'.tiff') ELSE outfiles[i]=mviout+'.tiff'
	'.jpg': IF datatype(jpg) EQ 'STR' THEN outfiles[i]=concat_dir(jpg,ro1+'.jpg') ELSE outfiles[i]=mviout+'.jpg'
    ENDCASE
	
ENDIF ELSE mviout=0

   label = STRARR(len)
   FOR i=0, len-1 DO BEGIN
      pos = RSTRPOS(names(i), '/')
      IF (pos GE 0) THEN $
         label(i) = STRMID(names(i), pos+1, STRLEN(names(i))-pos) $
      ELSE $
         label(i) = names(i)
   ENDFOR

   ind0 = -1		; Indices for this image
   ind0ref = -1		; Indices for reference image

   t1 = systime(1)

;
; -- Set up Window settings
;
;    wdel, /ALL	    ; DELETES ALL OPEN WINDOWS!!! (including pixmaps)
    win_index = INTARR(1)	   
    startind=0  ; This is the subscript of list that corresponds to yloc=0
    yloc=0  	; This is the frame number
    IF keyword_set(STAIND) THEN startind=staind ELSE $
    IF keyword_set(DIFF) or keyword_set(RATIO) THEN BEGIN
    	yloc=-1     	    ; first image saved is 2nd image
	startind=1	    ; first image is base
    ENDIF
    IF KEYWORD_SET(RUNNING_DIFF) THEN BEGIN
    	yloc=RUNNING_DIFF*(-1)     	    ; start after image(n) n=running_diff
        startind=RUNNING_DIFF
    endif    
    IF KEYWORD_SET(USE_MODEL) THEN BEGIN
    	startind = 0     ; first image is NOT base
	yloc=0
    ENDIF
    ; get screen size for /TWO case
    if keyword_set(TWO) then device,get_screen_size=ss

   ;--Number of frames in movie
   IF keyword_set(average_frames) THEN bufsz=fix(average_frames) ELSE bufsz=1
   nframes=(len-startind)/bufsz
   bufcnt=0
   help,bufsz
   
; -- END image or hdr not needed. 

; ****************************************
; ********* BEGIN MAKING FRAMES **********
; ****************************************
if keyword_set(fmask) then print,'*********using mask from save file*************'

;input keywords used by scc_mkimage
domask=0
name2prev=''
IF keyword_set(DOROTATE) THEN rotateset=1 ELSE rotateset=0
IF ~keyword_set(USE_LAST_COORDS) THEN BEGIN
    coordsab=intarr(3,4)
    IF keyword_set(COORDS) THEN coordsab[0,*]=coords ELSE BEGIN
	IF naxis1+naxis2 EQ 0 THEN BEGIN
    	    mreadfits,names[0],hi
	    naxis1=hi.naxis1
	    naxis2=hi.naxis2
	ENDIF
    	coordsab[0,*]=[0,2047,0,2047]
    ENDELSE
ENDIF ELSE IF datatype(coordsab) NE 'UND' THEN getsubfield=0

IF keyword_set(selectbmin)THEN uselectbmin=intarr(3)+1 else uselectbmin=intarr(3)+0
IF keyword_set(GETSUBFIELD) and keyword_set(coordcrop) THEN BEGIN
	    image= SCC_MKFRAME(names[0],tai_all,exp_all,bias_all,rolld,fpwvl_all,filter_all, $
                            hdr,i,startind,img_xy,dofull,bmin,bmax,cam, $
                            box_ref,r_occ,r_occ_out,sunc,r_sun, $
                            linethickness, pan, hsize, vsize, coords, $
                            yloc,win_index,camt,xouts,youts,mask_occ, sunxcen, sunycen, $
                            lg_mask_occ,diff,hide_pylon,use_model,box,no_normal,noexpcorr, $
                            ratio,use_sharp,sharp_fact,sqrt_scl,log_scl,running_diff,unsharp,lee_filt, $
                            fill_col,fixgaps,outer,limb,tiff,png,pict,0,ftimes,dbiasx, $
                            bad_skip,logo,sdir,save,tlen,nocal, skipped, PHOTOCAL=photocal, $
                            dcolor,domask,dorotate, TWO=twoval, GETSUBFIELD=getsubfield,  SMOOTH_BOX=smooth_box, $ 
			    VERBOSE=verbose, AUTOMAX=automax, NOPOP=save, FMASK=fmask, objects=objects, donrfg=donrfg, $
			    selectbmin=selectbmin,PFILTER=pfilter, coordcrop=coordcrop,imagecnt=0, _EXTRA=_extra)
            uselectbmin(0)=0
	    GETSUBFIELD=0
ENDIF
if datatype(swmoviev) ne 'UND' then ubmin=swmoviev.ubmin else ubmin=[bmin,bmin,bmin]
if datatype(swmoviev) ne 'UND' then ubmax=swmoviev.ubmax else ubmax=[bmax,bmax,bmax]

FOR i = 0, len-1 DO BEGIN
        PRINT, '%%SCC_MKMOVIE: Processing file: ', i+1 , ' of: ', len
	name1=names[i]
    	if keyword_set(TWO) then do2=TWO else do2=0
	dorotate=rotateset
	; dorotate gets reset in scc_mkframe
	if do2 gt 10 then twoval=do2-10 else twoval=do2  ;TWO is greater then ten if doall is set else only matching file will be done
	IF (twoval gt 0 and twoval lt 5) or twoval eq 7 THEN BEGIN
	    name2 = scc_find_synced(name1)
	    IF name2 EQ '' and do2 gt 10 THEN name2=name2prev  ; use previous image if matching image is missing
	    IF name2 EQ '' THEN BEGIN
		if do2 lt 10 THEN print,'Skipping ',name1, ' because complement is missing.' else print,'Skipping First image(s) ',name1, ' because complement is missing.' 
		print,''
		wait,1
		skipped=1
		goto, skipim
	    ENDIF
	    IF strpos(name1,'B.f') gt -1 THEN namesf=[name2,name1] ELSE namesf=[name1,name2]
	    ; namesf is always [ ahead, behind ]. Ahead is closer to sun. Behind sees stuff first.
            name2prev=name2
	ENDIF ELSE namesf=name1
	IF twoval ge 5 THEN BEGIN ;add SDO
	    tmp=sccreadfits(name1,hdr,/nodata)
	    scc_get_nearest_aia_synoptic, hdr.date_obs, hc, image, wavelength=hdr.WAVELNTH ,/wladj,/nameonly
	    namesdo = image
	    IF namesdo EQ '' THEN namesdo=name2prev  ; use previous image if matching image is missing
	    namesf=[namesf,namesdo]
            name2prev=namesdo
	ENDIF

	FOR j=0,n_elements(namesf)-1 DO BEGIN
            coords=reform(coordsab[j,*])
	    bmin=ubmin(j) & bmax=ubmax(j)
            if twoval gt 0 then imagecnt=j+i else imagecnt=0
	   
	    image= SCC_MKFRAME(namesf[j],tai_all,exp_all,bias_all,rolld,fpwvl_all,filter_all, $
                            hdr,i,startind,img_xy,dofull,bmin,bmax,cam, $
                            box_ref,r_occ,r_occ_out,sunc,r_sun, $
                            linethickness, pan, hsize, vsize, coords, $
                            yloc,win_index,camt,xouts,youts,mask_occ, sunxcen, sunycen, $
                            lg_mask_occ,diff,hide_pylon,use_model,box,no_normal,noexpcorr, $
                            ratio,use_sharp,sharp_fact,sqrt_scl,log_scl,running_diff,unsharp,lee_filt, $
                            fill_col,fixgaps,outer,limb,tiff,png,pict,0,ftimes,dbiasx, $
                            bad_skip,logo,sdir,save,tlen,nocal, skipped, PHOTOCAL=photocal, $
                            dcolor,domask,dorotate, TWO=twoval, GETSUBFIELD=getsubfield,  SMOOTH_BOX=smooth_box, $ 
			    VERBOSE=verbose, AUTOMAX=automax, NOPOP=save, FMASK=fmask, objects=objects, donrfg=donrfg, $
			    selectbmin=uselectbmin(j),PFILTER=pfilter, coordcrop=coordcrop,imagecnt=imagecnt, _EXTRA=_extra)
    	    ; ** NOTE: image is from tvrd of Z-buffer
if skipped then goto, skipim
            if keyword_set(selectbmin) then begin
               ubmin(j)=bmin & ubmax(j)=bmax
	       if datatype(swmoviev) ne 'UND' then begin
                 swmoviev.ubmin(j)=bmin & swmoviev.ubmax(j)=bmax
	      endif
	    endif

	    IF namesf[j] NE '' THEN $
	    IF j EQ 0 THEN BEGIN
		hdr1=hdr
	    	; A is on left or A is on Bottom
		IF yloc EQ 0 and ~keyword_set(USE_LAST_COORDS) THEN BEGIN
    	    	    coordsab[0,*]=coords
		    coordsab[1,*]=coords
		ENDIF 
	    ENDIF ELSE BEGIN
		IF twoval EQ 1 or twoval EQ 4 THEN hdr1=hdr
		; B is on left or B is on Bottom
		IF yloc EQ 0 and ~keyword_set(USE_LAST_COORDS) THEN coordsab[1,*]=coords
    	    ENDELSE
	    if yloc eq 0 and keyword_set(selectbmin) eq 1 then uselectbmin(j)=0

	ENDFOR
	szim=size(image)

        IF n_Elements(szim) eq 3 and szim(1) eq 7 then begin ; check for ERROR message and return to wscc_mkmovie if there is an ERROR
    	    message,image,/info
	     return
     	endif

	IF yloc LT 0 THEN goto, next

	
	doavg:
	;--Do frame averaging
	IF bufcnt EQ 0 THEN BEGIN
	    bufim=bytarr(szim[1],szim[2],bufsz)
	    newbuf=1
    	ENDIF
	IF ~skipped THEN BEGIN
	    IF newbuf THEN BEGIN
		IF yloc EQ 0 THEN all_hdr=hdr1 ELSE all_hdr=[all_hdr,hdr1]
		; save info for first image in buffered interval that is not skipped

		;--Mvi headers always have info for frame in lower or left image 
    		break_file,name1,a,dir,root,ext
		IF do2 THEN BEGIN
		; remove S/C from rootname
		    name=strmid(root,0,strlen(root)-1)
		ENDIF ELSE BEGIN
		    name=root
		ENDELSE
		newbuf=0
	    ENDIF
    	    bufim[*,*,bufcnt]=image
    	ENDIF
	IF bufsz GT 1 THEN bufcnt=bufcnt+1
	
	IF bufsz GT 1 THEN IF bufcnt LT bufsz THEN goto, skipim
	;
	
	;--Do median filtering
	IF bufsz GT 1 THEN BEGIN
	; check bufim for skipped images
	    FOR j=0,bufsz-1 DO BEGIN
	    	bufimcnt=total(bufim[*,*,j])
		IF bufimcnt NE 0 THEN BEGIN
		    IF n_elements(goodframes) EQ 0 THEN goodframes=j ELSE goodframes=[goodframes,j]
		ENDIF
	    ENDFOR
	    ngf=n_elements(goodframes)
	    IF ngf LT bufsz THEN BEGIN
	    	print
		print,'Missing '+trim(bufsz-ngf)+' frames out of '+trim(bufsz)+' for averaging.'
		print
		wait,2
	    ENDIF
	    ;print,'Using frames ',goodframes,' in buffer for average.'
	    skipped=0
	    IF ngf GT 0 THEN image1=rebin(bufim[*,*,goodframes],szim[1],szim[2],1) $
	    	ELSE skipped=1
	ENDIF ELSE image1=bufim
	undefine,goodframes
        IF keyword_set(VERBOSE) THEN help,yloc,win_index,hsize,vsize,bufim,bufcnt,dorotate
        IF keyword_set(VERBOSE) THEN wait,2  
	
    	IF (skipped) THEN goto, skipim

	;--Add datetime  
	
	IF keyword_set(TIMES) THEN image1=scc_add_datetime(image1,all_hdr[yloc],color=250, /ADDCAM,MVI=times)
	;stop
	    
	    help,yloc
	    ;wait,5
        IF datatype(textframe) ne 'UND' then begin
           textimg=reform(textframe(1,*,*))
           textclr=reform(textframe(0,*,*))
	   image1(where(textimg eq 255))=textclr(where(textimg eq 255))
	ENDIF
	
    	IF yloc EQ 0 THEN imgs=bytarr(szim[1],szim[2],nframes)  ; dimensions to match output of sccread_mvi.pro
    	imgs[*,*,yloc]=image1	
    	; imgs is stored in common block; it needs to be saved in case the color table changes, because
    	; 24-bit graphics devices do not preserve 8-bit dynamic range or 8-bit color table information; also
	; it is used to save the MVI
	
; nbr, 7/13/12 - This functionality is moved to scc_save_mvframe.pro. But maybe not the numbering.
;
;    	IF (keyword_set(TIFF) or keyword_set(PNG) or keyword_set(PICT)) THEN BEGIN
;    	    IF NOT(keyword_set(SDIR)) THEN sdir = './'
;	    IF cam EQ 'euvi' THEN name=name+trim(hdr1.wavelnth)
;    	    IF keyword_set(SAVE) THEN $
;		fname=concat_dir(sdir,save+'_'+string(format='(I4.4)',i)) ELSE $
;		fname=concat_dir(sdir,name)
;   	    IF not(keyword_set(STAIND)) OR i NE startind THEN BEGIN
;    		print,'Saving ',fname
;		IF keyword_set(PNG) THEN write_png, fname+'.png',image1,rgb[*,0],rgb[*,1],rgb[*,2]
;		; rgb is set in scc_mkframe
;		IF keyword_set(PICT) THEN write_pict, fname+'.pict',image1,rgb[*,0],rgb[*,1],rgb[*,2]
;		IF keyword_set(TIFF) THEN write_tiff, fname+'.tif',Reverse(image1, 2),red=rgb[*,0],green=rgb[*,1],blue=rgb[*,2]
;    	    ENDIF ELSE print,'Not saving ',fname,': starting with next one.'



	IF yloc GE 0 THEN BEGIN
	    IF keyword_set(SAVE) THEN BEGIN
   		IF not(keyword_set(STAIND)) OR i NE startind THEN BEGIN
    		IF yloc eq 0 then BEGIN
	    	    def_mvi_hdr,file_hdr,hdr=all_hdr[0]
                    file_hdr.nx=n_Elements(image1(*,0))
                    file_hdr.ny=n_Elements(image1(0,*))
		    first=1 
		ENDIF else first=0
                if i eq len-1 then last = yloc else last = 0
    		scc_save_mvframe,yloc,outfiles, image1, all_hdr[yloc], file_hdr, first=first, inline=nframes, last=last, _EXTRA=_extra
    		ENDIF ELSE print,'Not saving ',yloc,': starting with next one.'
	    ENDIF ELSE BEGIN
	    	WINDOW, XSIZE = szim[1], YSIZE = szim[2], /PIXMAP ,/FREE 
    	    	TV, image1	; with current color table 
    	    	IF yloc EQ 0 THEN win_index[0] = !D.WINDOW ELSE win_index=[win_index,!D.WINDOW]
            ENDELSE
    	ENDIF

 
        IF keyword_set(VERBOSE) THEN BEGIN
        ;        print,'Waiting 2 sec...'
	    print,'win_index',win_index
                wait,2
        ENDIF
	next:
    	yloc = yloc + 1
	bufcnt=0
	
	skipim:
	totskipped=totskipped+skipped
	
ENDFOR

    IF yloc EQ 0 THEN BEGIn
    	message,'No images found. Returning.',/info
	return
    ENDIF
    IF (startind EQ 1) THEN BEGIN	;** first frame is base -- not using model
         label = label(1:len-1)
         ;IF KEYWORD_SET(all_hdr) THEN all_hdr = all_hdr(1:len-1)
         ;all_hdr = all_hdr(1:len-1)
    ENDIF
    
   t2 = systime(1)
   print,'SCC_MKMOVIE took',t2-t1,' seconds (',(t2-t1)/float(nframes),' sec per frame)'
;   WDELETE,2  not opened when using Zbuffer in scc_mkframe
   
    IF totskipped GT 0 THEN BEGIN
	print
	print,'Skipped '+trim(totskipped)+' frames out of '+trim(len)+'.'
	IF totskipped GT 0.25*len and do2 THEN $
	print,'Hint: To skip fewer frames, try using input from other spacecraft.'
	print
    ENDIF

    if keyword_set(coordcrop) then if n_elements(coordcrop) eq 8 then begin
      def_mvi_hdr,file_hdr,hdr=all_hdr(0)
      file_hdr.RTHETA = 3
      file_hdr.RADIUS0 = coordcrop(2)
      file_hdr.RADIUS1 = coordcrop(3)
      file_hdr.THETA0  = coordcrop(0)
      file_hdr.THETA1  = coordcrop(1)
      file_hdr.sunxcen=0.0-coordcrop(0) ;RADIUS0 pixel 
      file_hdr.sunycen=0.0-coordcrop(2);THETA0 pixel 
      file_hdr.nx=coordcrop(5)
      file_hdr.ny=coordcrop(6)
      file_hdr.nf=len
      file_hdr.sec_pix=coordcrop(4)
      file_hdr.truecolor=0
    endif else file_hdr=0
   
; If image has been corrected for roll, the value by which last image has been corrected is dorotate, else there has been no correction.
IF Abs(dorotate) NE 1 THEN rect=dorotate ELSE rect=0

   IF NOT(keyword_set(SAVE) ) THEN BEGIN
         imgs=imgs[*,*,0:yloc-1]
   	IF ~keyword_set(SAVE) THEN WDELETE,0            ; Added by SHH 7/12/96 to destroy viewing window
   	SCC_PLAYMOVIE, win_index, NAMES=all_hdr.filename, HDRS=all_hdr, DEBUG=verbose, RECTIFIED=rect, $
	    FILE_HDR=file_hdr, MARGIN=margin
   ENDIF


END
