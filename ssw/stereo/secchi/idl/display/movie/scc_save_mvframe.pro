;$Id: scc_save_mvframe.pro,v 1.16 2014/06/11 13:06:37 hutting Exp $
;
; Project     : SECCHI
;                   
; Name        : scc_save_mvframe
;               
; Purpose     : called from scc_movieout, wcs_combine_mv, wscc_combine_mvi, and generic_movie when saving movies 
;               
; Explanation : 
;
; Use         :scc_save_mvframe,i,mvname,img_win,imghdr,file_hdr
;
; Inputs:   i =count, 
;    	    mvmane= name of movie, where suffix indicates format to save in.
;   	Possible formats: .mvi .mpg .avi .mov .png .gif .pict .tiff .jpg
;       .png .gif .pict .tiff .jpg movie frames will be numbered 0 - n unless the following time formats are in the movie title
;  	file name formats for movie frames are:
;   	    	    ssuff - will be replaced with standard SECCHI telescope IDs calling scc_suffix
;		    yyyy - wll be replace with year from frame headers
;		    mmdd - wll be replace with month and day from frame headers
;   	    	    hhmmss or hhmm - wll be replace with time from frame headers
;	    img_win= byte image or window index, 
;	    imghdr= image header, 
;	    file_hdr= movie file header
;
; Optional Inputs:
;   	
;
; KEYWORDS: 	
;                 inline= number of frames
;                 first= first frame of movie
;                 colorquan= use color_quan on png and gif movies.
;                 last= if set  lastframe keyword is set for pixmaps2mpg 
;   	    	  SVADDTIMES= FLTARR of same dimension as mvname. Add time/tel to lower left corner of frame if 
;   	    	    	      value is gt 0; value corresponds to charsize.
;
; Calls       : 
;
; Side effects: 
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL November 2011.
;               
;	
;-            
;$Log: scc_save_mvframe.pro,v $
;Revision 1.16  2014/06/11 13:06:37  hutting
;added a fits output option
;
;Revision 1.15  2012/11/19 18:58:38  mcnutt
;always use ahdr crpix and sec_pix values
;
;Revision 1.14  2012/08/07 18:23:48  nathan
;undo strmid outext--did not work if .ffmpeg
;
;Revision 1.13  2012/08/02 14:50:22  mcnutt
;add time stamp option
;
;Revision 1.12  2012/08/01 17:31:06  mcnutt
;added last keyword to set lastframe keyword when calling pixmap2mpeg
;
;Revision 1.11  2012/07/27 21:07:40  nathan
;fix syntax error
;
;Revision 1.10  2012/07/18 15:28:26  mcnutt
;add loop to save more then one movie type
;
;Revision 1.9  2012/07/16 16:57:26  mcnutt
;added date and suffix to input description
;
;Revision 1.8  2012/07/13 23:00:01  nathan
;add .pic and .tif options
;
;Revision 1.7  2012/07/13 18:31:58  mcnutt
;always defines outmovie to write mpg and mvi simultaneously
;
;Revision 1.6  2012/04/25 12:49:23  mcnutt
;added jpeg option
;
;Revision 1.5  2011/11/21 19:16:02  secchib
;does not look for file_hdr if mov mpg or avi movie
;
;Revision 1.4  2011/11/08 20:03:24  mcnutt
;will look for file_hdr in _extra
;
;Revision 1.3  2011/11/08 17:51:36  mcnutt
;defines rtheta
;
;Revision 1.2  2011/11/01 13:28:51  mcnutt
;corrected movie file name in pximaps2mpeg call
;
;Revision 1.1  2011/11/01 12:29:40  mcnutt
;new pro common save frame program to be be call by movie tools when saving a movie
;
;
pro scc_save_mvframe,i,mvname,img_win,imghdr,file_hdr,first=first,inline=inline,colorquan=colorquan,last=last,svaddtimes=svaddtimes,_EXTRA=_extra
common sscsvmv,outmovie,fmcnt
if keyword_set(first) then first=1 else first=0
if first eq 1 then outmovie=mvname
if first eq 0 then fmcnt=fmcnt+1 else fmcnt=0
if keyword_set(svaddtimes) then svaddtimes=svaddtimes else svaddtimes=intarr(n_elements(outmovie))+0
IF datatype(_extra) eq 'STC' THEN IF tag_exist(_extra,'file_hdr') THEN IF datatype(_extra.file_hdr) eq 'STC' THEN file_hdr=_extra.file_hdr

FOR j=0,n_Elements(mvname) -1 DO BEGIN
  BREAK_FILE, mvname(j), a, outdir, outname, outext
  ;outext=strmid(outext,0,4)
  if first eq 1 then outmovie(j)=mvname(j)
    	  szim=size(img_win)
          IF szim[0] GE 2 THEN img=img_win else img='na'
	  if datatype(img) ne 'BYT' then begin
    	     WSET,img_win
	     img=tvrd(true=file_hdr.truecolor) 
	  endif
	  if svaddtimes(j) gt 0 then img=scc_add_datetime(img,imghdr,color=250, /ADDCAM,MVI=svaddtimes(j))
          IF strpos(outext,'mpg') eq -1 and outext ne '.mov'  and outext ne '.avi' THEN BEGIN
                      if file_hdr.RADIUS0 ne 0 or file_hdr.RADIUS1 ne 0 or file_hdr.THETA0 ne 0 or file_hdr.THETA1 ne 0 then flatplane=1 else flatplane=0
                                RTHETA=file_hdr.RTHETA
                      if file_hdr.RTHETA ge 1 or flatplane ne 0 then dortcoords=1 else dortcoords=0
                      if dortcoords gt 0 then $
	                    rtcoords=[file_hdr.RADIUS0,file_hdr.RADIUS1,file_hdr.THETA0,file_hdr.THETA1,file_hdr.sunxcen,file_hdr.sunycen] else rtcoords=0
                      if  (TAG_EXIST(imghdr, 'ccd_pos')) then ccd_pos=imghdr.ccd_pos else ccd_pos=0
                      if  (TAG_EXIST(file_hdr, 'ccd_pos')) and ccd_pos eq 0 then ccd_pos=file_hdr.ccd_pos else ccd_pos=0
                     if outext ne '.mvi' then begin
                         if tag_exist(imghdr,'date_cmd') then timestr=imghdr.date_cmd else timestr=''
                         if timestr eq '' then timestr = imghdr.date_obs + ' '+imghdr.time_obs
	                 if timestr eq ' ' then parse_secchi_filename, imghdr.FILENAME, timestr, det, sc, wvl, cname
                         tmp=outname
			 ttmp=strlowcase(tmp)
                         if timestr eq ' ' THEN tmp=tmp+STRING(i,FORMAT='(I4.4)') $
			 ELSE BEGIN
  		             if strpos(ttmp,'ssuff') gt -1 then strput,tmp,scc_suffix(imghdr),strpos(ttmp,'ssuff')-1
			     if strpos(ttmp,'yyyy') gt -1 then strput,tmp,strmid(timestr,0,4),strpos(ttmp,'yyyy')
                             if strpos(ttmp,'mmdd') gt -1 then strput,tmp,strmid(timestr,5,2)+strmid(timestr,8,2),strpos(ttmp,'mmdd')
			     if strpos(ttmp,'hhmmss') gt -1 then strput,tmp,strmid(timestr,11,2)+strmid(timestr,14,2)+strmid(timestr,17,2),strpos(ttmp,'hhmmss') ELSE $
                             if strpos(ttmp,'hhmm') gt -1 then strput,tmp,strmid(timestr,11,2)+strmid(timestr,14,2),strpos(ttmp,'hhmm') ELSE $
			     	tmp=tmp+'_'+STRING(fmcnt,FORMAT='(I4.4)')
                         ENDELSE
			 framefn=tmp
                         r=0 & g=0 & b=0
                         if file_hdr.truecolor eq 0 then TVLCT, r, g, b, /GET 
                         if file_hdr.truecolor eq 1 and keyword_set(colorquan) then img = Color_Quan(img, 1, r, g, b, Colors=colors, Dither=dither)
		    	 IF outext eq '.pic' THEN write_pict, concat_dir(outdir,framefn+'.pict'),img,r,g,b
		    	 IF outext eq '.tif' THEN write_tiff, concat_dir(outdir,framefn+'.tiff'),Reverse(img, 2), red=r, green=g, blue=b
                         if outext eq '.png' then write_png,  concat_dir(outdir,framefn+'.png'),img,r,g,b
                         if outext eq '.gif' then write_gif,  concat_dir(outdir,framefn+'.gif'),img,r,g,b
                         if outext eq '.jpg' then begin
			   GIF2JPG24,img,r,g,b,jpgimg
			   write_jpeg,concat_dir(outdir,framefn+'.jpg'),jpgimg,true=3
			 endif
                         if outext eq '.fts' then begin
			   thdr = convert2secchi(imghdr,file_hdr)
                           ihdr = struct2fitshead(thdr,/allow_crota,/dateunderscore)
			   writefits,concat_dir(outdir,framefn+'.fts'),img,ihdr
			 endif


			 img=framefn+outext
                         if first eq 1 then outmovie(j)=outdir+framefn+'.hdr'
                       
                    endif
                    if datatype(img) ne 'STR' then message,'saving frame '+trim(i+1)+' of '+trim(inline)+' to movie file '+outmovie[j], /info $
		        else  message,' saving file '+img,/info

                  SCCWRITE_DISK_MOVIE, outmovie(j), img, imghdr, NEW=first, MAXNUM=inline, sec_pix=imghdr.CDELT1,nx=file_hdr.nx,ny=file_hdr.ny,$
            	               DEBUG=debugon ,truecolor=file_hdr.truecolor,CCD_POS=ccd_pos, RTHETA=RTHETA,RTCOORDS=rtcoords, _EXTRA=_extra

           ENDIF ELSE PIXMAPS2MPEG, outmovie(j) , img, inline=inline,firstframe=first, lastframe=last,_EXTRA=_extra

ENDFOR

END
