;+
; Project     : SOHO, STEREO
;
; Name        : MVI2CARRMAP
;
; Purpose     : Creates carrington maps from mvi movies
;
; Explanation : 
;
; Use         : mvi2carrmaps, mvis, radious, dr, dtheta, crn, display=display, dirout=dirout, diff=diff,synoptic=synoptic,png=png,fits=fits
;
; Example     : mvi2carrmap, mvis, 1.10, 10, .25 ,1984 ,dirout='/net/earth/lasco/carr_maps/eit/limbs'
;
; Inputs      : mvis	STRARR	of MVI file names as returned from select_car_mvi_files or file_search
;   	    	radius	FLOAT 	radius of the circle in pix
;   	    	dr  	FLOAT 	radial number of steps
;   	    	dtheta	FLOAT 	size angular step : 1. gives 360 samples
;   	    	crn 	INT 	carrington rotation number
;
; Outputs     : east and west limb carrington map fits files or synoptic maps
;
; Keywords    : /display    show the carrington map
;   	    	/dirout     defines the output directory else the current directory will be used
;   	    	/diff	    will create a carrington map subtracing the previous movie frame
;               /fits       write fits files
;               /synoptic   make syniptic map not carrinton map
;               /png        write png files (work with synoptic maps only for now)
;
;
; Calls       : dev/arnaudutil/sample_circ.pro, resamplemap, carrlong
;
; Category    : Image Processing/Display.
;
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Oct 2008.
;
; MODIFICATION HISTORY:
; $Log: mvi2carrmap.pro,v $
; Revision 1.11  2011/12/01 21:06:38  nathan
; use select_windows for windows compat
;
; Revision 1.10  2010/03/05 14:17:20  mcnutt
; correct xticks for synoptic maps
;
; Revision 1.9  2009/07/17 21:34:13  nathan
; rename DATE FITS keywords
;
; Revision 1.8  2009/06/02 11:47:05  mcnutt
; uses solar_ephem to get distance from sun for soho
;
; Revision 1.7  2009/06/01 17:32:51  mcnutt
; added start and end date to synoptic map fits file output
;
; Revision 1.6  2009/06/01 16:53:52  mcnutt
; added synoptic map and png options
;
; Revision 1.5  2009/05/29 17:19:09  mcnutt
; crn can be undefined will use time for d1 and d2
;
; Revision 1.4  2009/01/30 17:06:30  nathan
; moved select_car_mvi_files into separate file
;
; Revision 1.3  2009/01/29 13:52:23  mcnutt
; added documentation for inputs outs and keywords
;


PRO mvi2carrmap, mvis, radius, dr,dtheta,crn,display=display,dirout=dirout,diff=diff,synoptic=synoptic,png=png,fits=fits;,labels=labels,fitsout=fitsout

if keyword_set(display) then display=1 else display=0
if keyword_set(dirout) then if strmid(dirout,strlen(dirout),1) ne '/' then dirout=dirout+'/'
if ~keyword_set(dirout) then dirout=''

SOHO_detc=['EIT','C1','C2','C3','C4']
STEREO_detc=['EUVI','COR1','COR2','HI1','HI2']
for rdm=0,n_elements(mvis)-1 do begin
   OPENR, lu1, mvis(rdm),/get_lun
   READ_MVI, lu1, file_hdr, ihdrs, imgs
   xs=360./dtheta
   xxx=findgen(xs)
   nbrad=n_elements(radius)
   nbim=file_hdr.nf
   mvisynomap=fltarr(nbim,xs,nbrad) 
   images = bytarr(file_hdr.nx, file_hdr.ny, file_hdr.nf)
   rsun=fltarr( file_hdr.nf )
   time=strarr( file_hdr.nf )
   carlong=fltarr( file_hdr.nf )
   carrot=fltarr( file_hdr.nf )
   cdelt=  file_hdr.sec_pix
   if datatype(cxc) eq 'UND' then xc= file_hdr.sunxcen else xc =cxc
   if datatype(cyc) eq 'UND' then yc= file_hdr.sunycen else yc =cyc
   ahdr = MVIHDR2STRUCT(ihdrs(0))
   sc=where(ahdr.DETECTOR eq SOHO_detc)
   carrlong,ahdr.date_obs+' 12:00:00',cr,clong,el=el,wl=wl
   if display and rdm eq 0 then window,xsize=file_hdr.nx,ysize=file_hdr.ny
   if sc(0) eq -1 then sc='stereo' else sc='soho' 
   FOR i=0, file_hdr.nf-1 DO BEGIN
      images(*,*,i)= imgs[i]     
      ahdr = MVIHDR2STRUCT(ihdrs(i))
      if i eq 0 then begin
        tags=tag_names(ahdr)
        zrsun=where(strlowcase(tags) eq 'rsun') & zrsun=zrsun(0)
        if sc eq 'soho' then begin
	   ;dsun=DIST2SUN(ahdr.date_obs)*1e3 ;distance in meters
           km=oneau('km')
           solar_ephem,strmid(ahdr.date_obs,2,2)+strmid(ahdr.date_obs,5,2)+strmid(ahdr.date_obs,8,2),dist=dist,/soho
           dsun=dist*km*1e3
	endif
        if sc eq 'stereo' then begin
	    if zrsun gt -1 then dsun=(6.95508e8 * 648d3 / !dpi ) /ahdr.rsun else begin
            hee_d=get_stereo_lonlat(ahdr.date_obs,strmid(ahdr.filename,strpos(ahdr.filename,'.f')-1,1), /meters, system='HEE',_extra=_extra)
            DSUN=hee_d(0)
	endelse
       endif
       endif
       if zrsun eq -1 then rsun(i)=(6.95508e8 * 648d3 / !dpi ) / dsun else rsun(i)=ahdr.rsun     
       time(i)=ahdr.date_obs+' '+ahdr.time_obs       
       carrlong,time(i),cr,clong,el=el,wl=wl
       carlong(i)=clong
       carrot(i)=cr

       im=reform(images(*,*,i))
;       radinpix=atan(radius*onersun()*1e3/DSUN)/cdelt
        radinpix=(radius*rsun(i))/cdelt

; recenter EIT 
       if ahdr.DETECTOR eq 'EIT' then begin
            window,2,xsize=file_hdr.nx,ysize=file_hdr.ny
            newcen=eit_point(time(i),195)
            crota=get_crota(time(i))
	    if crota eq 180 then newcen=1024-newcen
            xc=newcen(0)/(1024/file_hdr.nx)
            yc=newcen(1)/(1024/file_hdr.nx)
        endif

       if i eq 0 and rdm eq 0 then imref=im*0
       imu=im-imref
       if keyword_set(diff)then imref=im
       
       sample_circ,xc,yc,imu,radinpix,dr,dtheta,profile,trace,ierr,imaout

    ;#########################
    ;pile up the profile in the synoptic image
    ;#########################
    mvisynomap[i,*,*]=profile
    if display then tv,imaout

   endfor
   close, lu1
   free_lun,lu1  
   if rdm eq 0 then begin
        synomap=mvisynomap
        times=time
        carlongs=carlong
        carrots =carrot
   endif else begin
        synomap=[synomap,mvisynomap]
        times=[times,time]
        carlongs=[carlongs,carlong]
        carrots =[carrots,carrot]
   endelse
endfor


tm=utc2tai(anytim2utc(times))
if datatype(crn) ne 'UND' then begin
  d1=utc2tai(carrdate2(crn,wl=0,el=1))
  d2=utc2tai(carrdate2(crn+1,wl=1,el=0))
endif else begin
  d1=min(tm) & d2=max(tm)
endelse
  



stepsiz=(d2-d1)/((360+180)*4)
tgrid=dindgen((360+180)*4)*stepsiz+d1

resamplemap,synomap,tm,tgrid,srbinned,sreg,sreginterp,halfinterpsize=.25

synomap=sreginterp

stepang=360./n_elements(synomap(0,*))
steplong=(360.+180.)/n_elements(synomap(*,0))
stimeout=(360L+180)*1./steplong

if datatype(crn) ne 'UND' then crnn=string(crn,'(i4)') else begin
  td1=utc2str(tai2utc(d1))
  td2=utc2str(tai2utc(d2))
  crnn=strmid(td1,0,4)+strmid(td1,5,2)+strmid(td1,8,2)+'_'+strmid(td2,0,4)+strmid(td2,5,2)+strmid(td2,8,2)
endelse

if keyword_set(synoptic) then begin
  shiftlat=90./stepang
  synomap=shift(synomap,0,shiftlat)
  if keyword_set(display) or keyword_set(png) then begin
    xsz=n_Elements(synomap(*,0))
    ysz=n_Elements(synomap(0,*))

    tmp=bytarr(xsz/2+75,ysz/2+100)+255    
    pos=[70,50,70+xsz/2,50+ysz/2]
    utc1=tai2utc(d1)
    utc2=tai2utc(d2)
    xtname=string(indgen(utc2.mjd-utc1.mjd+2)+1,'(i2)') & xtname(n_elements(xtname)-1)=' '  

    if keyword_set(display) then begin
      window,xsize=xsz/2+75,ysize=ysz/2+100 
      tv,tmp
      tvscl,rebin(synomap,xsz/2,ysz/2),70,50,/device
      plot,findgen(10),/nodata,xrange=[1,(utc2.mjd-utc1.mjd+2)+1],xstyle=1,yrange=[0,4],$
        ytickname=['-90','0W','+90','0E','-90'],ystyle=1,xtitle='Days starting '+strmid(td1,0,10),$
        ytitle='PA [Deg.]',title=strupcase(sc)+' '+ahdr.DETECTOR+'!C'+strtrim(string(radius,'(f4.1)'),2)+' Rsun',/noerase,position=pos,$
        /device,xticklen=-.005,yticklen=-0.015,xticks=n_elements(xtname)-1,xminor=4,$
        xtickname=xtname,color=0
    endif
    if keyword_set(png) then begin
      set_plot,'z'
      device,set_resolution=[xsz/2+75,ysz/2+100]        
      loadct,0
      tv,tmp
      tvscl,rebin(synomap,xsz/2,ysz/2),70,50,/device
      plot,findgen(10),/nodata,xrange=[1,(utc2.mjd-utc1.mjd+2)+1],xstyle=1,yrange=[0,4],$
        ytickname=['-90','0W','+90','0E','-90'],ystyle=1,xtitle='Days starting '+strmid(td1,0,10),$
        ytitle='PA [Deg.]',title=strupcase(sc)+' '+ahdr.DETECTOR+'!C'+strtrim(string(radius,'(f4.1)'),2)+' Rsun',/noerase,position=pos,$
        /device,xticklen=-.005,yticklen=-0.015,xticks=n_elements(xtname)-1,xminor=4,$
        xtickname=xtname,color=0
      trd=string(radius,'(f4.1)') & trd=strtrim(trd,2) & strput,trd,'_',strpos(trd,'.')
      pngfile=dirout+strupcase(sc)+'_'+ahdr.DETECTOR+'_'+crnn+'_'+trd+'_Rsun'
      tmp=ftvread(filename=pngfile,/png)
      select_windows	
    endif
  endif
   ;#########################
   ;build fits header for synomap
   ;#########################
  if keyword_set(fits) then begin
    mkhdr,hdrsyno,synomap[*,*,0]
    sxaddpar,hdrsyno,'RADIUS',radius[0]
    sxaddpar,hdrsyno,'RADUNITS';(rsunradius ? 'Rsun' : 'Pix')
    sxaddpar,hdrsyno,'RADINPIX',radinpix[0]
    sxaddpar,hdrsyno,'DR',dr
    sxaddpar,hdrsyno,'DTHETA',dtheta
    sxaddpar,hdrsyno,'XSUNCNTR',xc
    sxaddpar,hdrsyno,'YSUNCNTR',yc
    sxaddpar,hdrsyno,'DATE-STA',td1
    sxaddpar,hdrsyno,'DATE-END',td2
    outfile=dirout+ahdr.DETECTOR+'_'+crnn+'_synoptic_'+string(radius*100,'(i3.3)')+'_'+string(dr,'(i2.2)')+'.fts'
    if keyword_set(diff) then outfile=dirout+ahdr.DETECTOR+'_'+crnn+'_synoptic_diff_'+string(radius*100,'(i3.3)')+'_'+string(dr,'(i2.2)')+'.fts'
    writefits,outfile,synomap,hdrsyno
  endif
endif


if ~keyword_set(synoptic) then begin
  ; ---- cut out the Carrington maps
  shiftlat=90./stepang
  synomap=reverse(shift(synomap,0,shiftlat),1)
  times=utc2str(tai2utc(reverse(tgrid)))

  ; -- cutting out east
  lastpos=long(360./steplong)-1
  poslat=long(180./stepang)
  carrmap_west=synomap(0:lastpos,0:poslat)
  timesw=[times(lastpos),times(0)]

  ; -- cutting out west
  synomap=shift(synomap,0,-1)
  firstpos=long(180./steplong)
  carrmap_east=reverse(synomap(firstpos:stimeout-1,(poslat-1):*),2)
  timese=[times(stimeout-1),times(firstpos)]


  if keyword_set(display) then begin
    xsz=n_Elements(carrmap_east(*,0))
    ysz=n_Elements(carrmap_east(0,*))
    window,1,xsize=xsz,ysize=ysz,title='East Limb'+timese(0)+' '+timese(1)
    tvscl,carrmap_east
    xsz=n_Elements(carrmap_west(*,0))
    ysz=n_Elements(carrmap_west(0,*))
    window,2,xsize=xsz,ysize=ysz,title='West Limb'+timesw(0)+' '+timesw(1)
    tvscl,carrmap_west
  endif


  if keyword_set(fits) then begin
    mkhdr,hdrsyno,carrmap_east[*,*,0]
    sxaddpar,hdrsyno,'RADIUS',radius[0]
    sxaddpar,hdrsyno,'RADUNITS','Pix';(rsunradius ? 'Rsun' : 'Pix')
    sxaddpar,hdrsyno,'RADINPIX',radinpix[0]
    sxaddpar,hdrsyno,'DR',dr
    sxaddpar,hdrsyno,'DTHETA',dtheta
    sxaddpar,hdrsyno,'XSUNCNTR',xc
    sxaddpar,hdrsyno,'YSUNCNTR',yc
    sxaddpar,hdrsyno,'LIMB','East    '
    sxaddpar,hdrsyno,'DATE-STA',timese(0)
    sxaddpar,hdrsyno,'DATE-END',timese(1)
    sxaddpar,hdrsyno,'DETECTOR',ahdr.DETECTOR
    sxaddpar,hdrsyno,'PLATESCL',cdelt
    outfile=dirout+ahdr.DETECTOR+'_'+crnn+'_el_'+string(radius*100,'(i3.3)')+'_'+string(dr,'(i2.2)')+'.fts'
    if keyword_set(diff) then outfile=dirout+ahdr.DETECTOR+'_'+crnn+'_el_diff_'+string(radius*100,'(i3.3)')+'_'+string(dr,'(i2.2)')+'.fts'
    writefits,outfile,carrmap_east,hdrsyno

    mkhdr,hdrsyno,carrmap_west[*,*,0]
    sxaddpar,hdrsyno,'RADIUS',radius[0]
    sxaddpar,hdrsyno,'RADUNITS','Pix';(rsunradius ? 'Rsun' : 'Pix')
    sxaddpar,hdrsyno,'RADINPIX',radinpix[0]
    sxaddpar,hdrsyno,'DR',dr
    sxaddpar,hdrsyno,'DTHETA',dtheta
    sxaddpar,hdrsyno,'XSUNCNTR',xc
    sxaddpar,hdrsyno,'YSUNCNTR',yc
    sxaddpar,hdrsyno,'LIMB','West    '
    sxaddpar,hdrsyno,'DATE-STA',timesw(0)
    sxaddpar,hdrsyno,'DATE-END',timesw(1)
    sxaddpar,hdrsyno,'DETECTOR',ahdr.DETECTOR
    sxaddpar,hdrsyno,'PLATESCL',cdelt
    outfile=dirout+ahdr.DETECTOR+'_'+crnn+'_wl_'+string(radius*100,'(i3.3)')+'_'+string(dr,'(i2.2)')+'.fts'
    if keyword_set(diff) then outfile=dirout+ahdr.DETECTOR+'_'+crnn+'_wl_diff_'+string(radius*100,'(i3.3)')+'_'+string(dr,'(i2.2)')+'.fts'
    writefits,outfile,carrmap_west,hdrsyno
  endif
endif
end
