PRO PREP_IMAGES, list, hdrs, imgs, savedir
;+
; $Id: prep_images.pro,v 1.2 2008/03/17 12:37:27 mcnutt Exp $
;
; Name        : PREP_IMAGES.PRO 
;
; Purpose     : load images with secchi_prep and resave locally for future use
;               
; Explanation : calls secchi_prep for files in list and resaves output fits files
;               
; Use         : to write fits files after secchi_prep
;    
; Inputs      : list, savedir
;               
; Outputs     : hdrs, imgs  : returned from secchi_prep
;               
; Keywords    : 
;
; Calls       : secchi_prep.pro with no keywords
;
; Side effects: default output directory is /home/herbst/images/secchi/lz/L0/
;               
; Category    : Image Display calibration inout
;
; $Log: prep_images.pro,v $
; Revision 1.2  2008/03/17 12:37:27  mcnutt
; added basic header info for wiki page
;
; Revision 1.1  2007/08/17 17:31:38  nathan
; moved from dev/movie
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;
IF(DATATYPE(savedir) EQ 'UND') THEN savedir = '/home/herbst/images/secchi/lz/L0/'

secchi_prep, list, hdrs, imgs

detectors = ['EUVI', 'COR1', 'COR2', 'HI1', 'HI2']
detdir = ['euvi', 'cor1', 'cor2', 'hi_1', 'hi_2']

nim = n_elements(list)
FOR i=0, nim-1 DO BEGIN

	hdr = hdrs[i]
	img = imgs[*,*,i]

	; secchi_prep changes this character for some reason 6/8/07
	hdr.filename = strmid(hdr.filename, 0,16) + 's' + strmid(hdr.filename, 17,8)
	hdr.polar = 0  ; sccwritefits isn't working 6/7/07 with polar < 0

	path = savedir + (STRTRIM(hdr.obsrvtry, 2) EQ 'STEREO_A' ? 'a/' : 'b/') + 'img/' + $
		detdir[WHERE(STRTRIM(hdr.detector, 2) EQ detectors) > 0] + '/' + $
		strmid(hdr.date_obs,0,4) + strmid(hdr.date_obs,5,2) + strmid(hdr.date_obs,8,2) + '/'

	IF((findfile(path + '*'))[0] EQ '') THEN SPAWN, 'mkdir ' + path
;stop
	sccwritefits, path + hdr.filename, img, hdr
ENDFOR

END
