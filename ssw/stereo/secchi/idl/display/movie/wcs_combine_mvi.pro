;+
; $Id: wcs_combine_mvi.pro,v 1.19 2012/12/12 17:39:08 mcnutt Exp $
; Project     : STEREO - SECCHI 
;
; Name        : wcs_combine_mvi
;
; Purpose     : Insert frames from one mvi into another to create combined movie.
;
; Explanation : 
;
; Use         : wcs_combine_mvi
;
;      Example:
;
; Optional Inputs : mvifiles	STRARR - List of MVI files to combine
;
; Outputs     : None.
;
; Keywords    : 
;
; Calls       : scc_playmovie.pro
;
; Category    : Image Processing/Display Movie Animation 
;
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Aug 2011.
; $Log: wcs_combine_mvi.pro,v $
; Revision 1.19  2012/12/12 17:39:08  mcnutt
; defines all HI headers and corrected error in fix_hi_pointing
;
; Revision 1.18  2012/08/29 14:11:37  mcnutt
; added wcs_combine bpos options
;
; Revision 1.17  2012/08/21 17:21:46  nathan
; tweak CMBWIMGS button label
;
; Revision 1.16  2012/08/20 14:08:22  mcnutt
; moved x and y size row down to above sec_pix scaling
;
; Revision 1.15  2012/08/08 12:08:13  mcnutt
; corrected wcs_Comb common block
;
; Revision 1.14  2012/08/07 19:11:47  mcnutt
; save movie option will not play movie and added mask rsun/deg option
;
; Revision 1.13  2012/08/03 20:37:39  nathan
; changed some labels
;
; Revision 1.12  2012/08/03 17:19:05  mcnutt
; set c3 rtheta mask to 8.0 and loads color table 0 if grayscale
;
; Revision 1.11  2012/05/22 16:12:29  mcnutt
; added ?? to filename at position 16 to work with mass fits file names
;
; Revision 1.10  2011/11/22 16:21:28  mcnutt
; adjusted window positions
;
; Revision 1.9  2011/11/09 20:11:28  mcnutt
; added recenter of crop and scale adjustment slider
;
; Revision 1.8  2011/11/01 17:36:38  mcnutt
; corrected frame hdr times
;
; Revision 1.7  2011/11/01 16:42:24  mcnutt
; corrected crop tool
;
; Revision 1.6  2011/11/01 12:30:27  mcnutt
; uses scc_save_mvframe when saving movie
;
; Revision 1.5  2011/10/24 18:07:59  mcnutt
; sends file_hdr into scc_playmovie so scc_playmoviem can be called
;
; Revision 1.4  2011/10/21 20:54:59  nathan
; change default cor1 outer
;
; Revision 1.3  2011/10/13 13:34:15  mcnutt
; corrected mov avi movie out options
;
; Revision 1.2  2011/09/28 18:46:23  nathan
; update wcs_combine() call
;
; Revision 1.1  2011/08/26 14:56:00  mcnutt
; wscc_combine_mvi modified to work wcs_combine
;

PRO GET_cs_combine_MVI, mvis

common wcs_combine, cmbmovie,cmbstate,struct ;needs to match definition in wcs_combine_mvi.pro
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
   ;** MVI1

cmbmovie=-1
cmbstate=-1
mvtype=strarr(n_elements(mvis))
cmbmovie={ mvis:mvis, mvtype:mvtype}
 if (datatype(debug_on) eq 'UND') then debug_on=0

for rdm=0,n_elements(mvis)-1 do begin

  if strpos(mvis(rdm),'.hdr') gt -1 then pngmvi=mvis(rdm) else pngmvi='0'
  OPENR, lu1, mvis(rdm),/get_lun

; Check if call to read_mvi should return rgb color vectors (true if
; split_colors keyword is set.)

;rgbvec1=-1
;   IF NOT(KEYWORD_SET(SPLIT_COLORS)) THEN $
;    READ_MVI, lu1, file_hdr1, ihdrs1, imgs1, swapflag $
;   ELSE BEGIN
       rgbvec=bytarr(!d.n_colors,3)
       SCCREAD_MVI, lu1, file_hdr1, ihdrs1, imgs1, swapflag, rgbvec=rgbvec, pngmvi=pngmvi
;   ENDELSE   
   cmbmovie.mvtype(rdm)=pngmvi
;   images = bytarr(file_hdr1.nx, file_hdr1.ny, file_hdr1.nf)
   FOR i=0, file_hdr1.nf-1 DO BEGIN
;      images(*,*,i)= imgs1[i]     
      ahdrt = SCCMVIHDR2STRUCT(ihdrs1(i),file_hdr1.ver)
      ahdr = convert2secchi(ahdrt,file_hdr1, debug=debug_on)
      if strlen(ahdr.time_obs) lt 5 then begin
         ahdr.time_obs=strmid(ahdr.date_obs,11,8)
         ahdr.date_obs=strmid(ahdr.date_obs,0,10)
      endif
      if file_hdr1.sunxcen le 0 then file_hdr1.sunxcen=ahdr.crpix1
      if file_hdr1.sunycen le 0 then file_hdr1.sunycen=ahdr.crpix2
      if file_hdr1.sec_pix le 0 and file_hdr1.rtheta eq 0 then file_hdr1.sec_pix=ahdr.CDELT1  ;correct hi2 sec_pix not saved correctly in sccwrite_disk_movie
      IF ahdr.TELESCOP eq 'STEREO' then aorb=strmid(ahdr.FILENAME,strpos(ahdr.FILENAME,'.')-1,1) else aorb=''
      IF (ahdr.detector eq 'HI2' or ahdr.detector eq 'HI1') and file_hdr1.rtheta eq 0 THEN BEGIN 
                        filen=ahdr.filename
         	  	fname=sccfindfits(filen)
		        IF fname EQ '' THEN BEGIN
			     strput,filen,'??',16
			     fname = sccfindfits(filen)
   		        ENDIF
 		        if fname NE  '' then jk=sccreadfits(fname,fhdr,/nodata) else fhdr=mk_short_scc_hdr(ahdr,file_hdr1)
                         matchfitshdr2mvi,fhdr,file_hdr1,ahdr;,notsecchi=notsecchi
                         hi_fix_pointing,fhdr
                         fcen=scc_sun_center(fhdr)
                        IF (i EQ 0) THEN BEGIN                        
                          file_hdr1.sunxcen=fcen.xcen
                          file_hdr1.sunycen=fcen.ycen
                        ENDIF
                       ahdr.xcen=fcen.xcen
                       ahdr.ycen=fcen.ycen
      ENDIF

       IF (i EQ 0) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
   ENDFOR
    IF file_hdr1.sec_pix LT 0.2  and file_hdr1.rtheta eq 0 THEN file_hdr1.sec_pix=ahdr.cdelt1*3600. ; fix case where sec_pix and cdelt1 saved as deg in generic_movie.pro,v1.30 or previous line
    IF file_hdr1.sec_pix LT 0.2  and file_hdr1.rtheta eq 0 THEN file_hdr1.sec_pix=file_hdr1.sec_pix*3600. ; fix case where sec_pix and cdelt1 saved as deg in generic_movie.pro,v1.30 or previous line
    IF debug_on then begin
    	print,'framehdr:',ahdr
	print,'file_hdr:',file_hdr1
	wait,2
    ENDIF

   if rdm eq 0 then tel=strmid(hdrs(0).FILENAME,strpos(hdrs(0).FILENAME,'.f')-3,3) else tel=[tel,strmid(hdrs(0).FILENAME,strpos(hdrs(0).FILENAME,'.f')-3,3)]
   if rdm eq 0 then proj=hdrs(0).FILTER else proj=[proj,hdrs(0).FILTER]
   if rdm eq 0 then mvihdrs={file_hdr:file_hdr1} else mvihdrs={file_hdr:[mvihdrs.file_hdr,file_hdr1]}
   if rdm eq 0 then det=hdrs(0).detector+aorb else det=[det,hdrs(0).detector+aorb]
   
;   mvi={img:images,hdrs:hdrs,rgbvec:rgbvec}
   mvi={hdrs:hdrs,rgbvec:rgbvec}
   
   tagname='mvi'+string(rdm,'(i1)')
   cmbmovie=add_tag(cmbmovie,mvi,tagname)

   
   close, lu1
   free_lun,lu1  

endfor
if strlowcase(det(0)) eq 'c2' or strlowcase(det(0)) eq 'c3' or strlowcase(det(0)) eq 'eit' then tel=det
cmbmovie=add_tag(cmbmovie,mvihdrs,'mvihdrs')
cmbmovie=add_tag(cmbmovie,det,'tels')
cmbmovie=add_tag(cmbmovie,proj,'proj')
cmbmovie=add_tag(cmbmovie,[0,0,0],'bkgrd')

end

;
;-------------------------------------------------------------------------------------------
;

pro wcs_combine_movies ,hsize=hsize,yminmax=yminmax,xminmax=xminmax,TRUECOLOR=truecolor, bpos=bpos,times=times,$
    preview=preview, restore=restore, defimgs=defimgs, outmovie=outmovie, save_only=save_only, _EXTRA=_extra

common wcs_combine
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe
common wcs_comb,coord,masks,imo,rimo,gimo,bimo,imsizeout,cdate,wcssecchi,wcshdr,wcssoho,wcsptr,cfile_hdr,cdelts,wcsorder,coord2,imo2,rimo2,gimo2,bimo2,dets
loadct,0

if datatype(imgs) ne 'UND' then undefine,imgs
wdel,/all

ishi=where(strpos(cmbmovie.tels,'h') gt -1)
as=where(strpos(cmbmovie.tels,'A') gt -1)
bs=where(strpos(cmbmovie.tels,'B') gt -1)
ls=where(cmbmovie.tels eq 'AIA',isaia)
ls=where(cmbmovie.tels eq 'HDMI',ishdmi)
ls=where(cmbmovie.tels eq 'MDI',ismdi)
ls=where(cmbmovie.tels eq 'C2',isc2)
ls=where(cmbmovie.tels eq 'C3',isc3)
ls=where(cmbmovie.tels eq 'EIT',iseit)
ls=where(cmbmovie.tels eq 'SDO',issdo)
ls=where(cmbmovie.tels eq '',isunk)
issoho=total([isc2,isc3,iseit])
if isunk then begin
  for tn=0,n_Elements(cmbmovie.tels)-1 do begin
    if cmbmovie.tels(tn) eq '' then begin
       tmptel=''
       read,'Enter telescope name for movie '+cmbmovie.mvis(tn)+' (ie AIA HMDI MDI) :',tmptel
       cmbmovie.tels(tn)=strupcase(tmptel)
     if tn eq 0 then cmbmovie.mvi0.hdrs.detector=strupcase(tmptel)
     if tn eq 1 then cmbmovie.mvi1.hdrs.detector=strupcase(tmptel) 
     if tn eq 2 then cmbmovie.mvi2.hdrs.detector=strupcase(tmptel)   
     if tn eq 3 then cmbmovie.mvi3.hdrs.detector=strupcase(tmptel)   
     if tn eq 4 then cmbmovie.mvi4.hdrs.detector=strupcase(tmptel)   
     if tn eq 5 then cmbmovie.mvi5.hdrs.detector=strupcase(tmptel)   
     if tn eq 6 then cmbmovie.mvi6.hdrs.detector=strupcase(tmptel)   
     if tn eq 7 then cmbmovie.mvi7.hdrs.detector=strupcase(tmptel)   
     if tn eq 8 then cmbmovie.mvi8.hdrs.detector=strupcase(tmptel)  
     if tn eq 9 then cmbmovie.mvi9.hdrs.detector=strupcase(tmptel)   
    endif
  endfor
endif
notsecchi=total([isaia,ishdmi,ismdi,issoho,issdo,isunk])

if datatype(cmbstate) ne 'STC' then stup=1 else stup=0
if stup eq 0 then if cmbstate.rtheta ne cmbstate.prtheta then stup=2 
if stup eq 1 then begin
   detectors=['HI2A','HI1A','COR2A','COR1A','EUVIA','EUVIB','COR1B','COR2B','HI1B','HI2B','C3','C2','EIT','AIA','HDMI','MDI']   
   masks=['hi2A_mask.fts','','cor2A_mask.fts','cor1_mask.fts','euvi_mask.fts','euvi_mask.fts','cor1_mask.fts','cor2B_mask.fts','','hi2B_mask.fts','','','','','','']
   masks=getenv('SECCHI_CAL')+'/'+ masks
   outer=[-1,0,15.,4.0,1.6,1.6,4.0,15.,0,-1,0,4.5,1.25,1.25,1.25,1.25] ;default mask in Rsun
   fmasks=strarr(n_Elements(cmbmovie.tels))
   touter=fltarr(n_Elements(cmbmovie.tels))
   for tn=0,n_Elements(cmbmovie.tels)-1 do begin
       z=where(strupcase(detectors) eq cmbmovie.tels(tn))
       if z(0) ne -1 then begin
	  touter(tn)=outer(z)
          fmasks(tn)=masks(z)
       endif
   endfor
   detectors=cmbmovie.tels & outer=touter & masks=fmasks
endif 
if stup eq 2 then cmbstate.prtheta=cmbstate.rtheta

if datatype(cmbstate) ne 'STC' then begin
    cmbstate={ $
        sec_pixs:0.0 ,$
        truv:0 ,$
        times:0.0 ,$
        ytsize:0.0 ,$
        rzcen:[0,0] ,$
        crop:0 ,$
        coordadj:0 ,$
	bpos:0 ,$ 
	estmvisiz:0.0 ,$ 
        detectors:detectors ,$
        rtheta:4 ,$
        prtheta:4 ,$
        ylog:0 ,$
        rtaxis:0 ,$
        rtsize:[1024,0] ,$
        rttha:[0.,0.] ,$
        rtthb:[0.,0.] ,$
        rtra:[0.,0.] ,$
        masks:masks,$
        outer:outer,$
        maskindeg:0,$
	notsecchi:0 }
endif
if keyword_set(hsize) then cmbstate.rtsize(0)=hsize
if keyword_set(yminmax) then cmbstate.rttha=xminmax

if keyword_set(xminmax) then cmbstate.rtra=yminmax
     if cmbstate.rttha(0) ne 0 or cmbstate.rttha(1) ne 0 then xminmax=cmbstate.rttha else xminmax=0
     if cmbstate.rtra(0) ne 0 or cmbstate.rtra(1) ne 0 then yminmax=cmbstate.rtra else yminmax=0

if keyword_set(bpos) then cmbstate.bpos=bpos

if as(0) eq -1 then cmbstate.bpos = 0  ;ignore bpos if movies are all from the same spacecraft
if bs(0) eq -1 then cmbstate.bpos = 0
if notsecchi gt 0 then begin
   cmbstate.bpos = 0
   cmbstate.notsecchi=notsecchi
endif else cmbstate.notsecchi= 0

fsc=intarr(n_elements(cmbmovie.tels))
if cmbstate.bpos eq 1 and  bs(0) gt -1 then fsc(bs)=20
if cmbstate.bpos eq 2  and  as(0) gt -1 then fsc(as)=20
if cmbstate.bpos eq 0 and as(0) gt -1 and bs(0) gt -1 then fsc(as)=20

abmovie=0
if cmbstate.bpos eq 1 then abmovie='A' ; right side sc
if cmbstate.bpos eq 2 then abmovie='B'

sctext=['SECCHI  ','']
if cmbstate.bpos eq 1 then sctext=['SECCHI-B  ','SECCHI-A ']
if cmbstate.bpos eq 2 then sctext=['SECCHI-A  ','SECCHI-B ']
if cmbstate.bpos eq 0 and as(0) gt -1 and bs(0) gt -1 then sctext=['SECCHI-A  ','SECCHI-B ']
if cmbstate.bpos eq 0 and issoho gt 0 then sctext=['SOHO ','']

if keyword_set(restore) then begin
        cmbstate.rtsize=[1024,0]
        cmbstate.rttha=[0.,0.]
        cmbstate.rtthb=[0.,0.]
        cmbstate.rtra=[0.,0.]
endif
if keyword_set(TRUECOLOR) then cmbstate.truv=1 else cmbstate.truv=0
if keyword_set(TRUECOLOR) then bkgrd=cmbmovie.bkgrd else bkgrd=cmbmovie.bkgrd(0)
if ~keyword_set(TRUECOLOR) then loadct,0

if keyword_set(times) then begin
   cmbstate.times=times
   cmbstate.ytsize=fix(cmbstate.times*7.+2 ) 
endif  else  begin
   cmbstate.times=0
   cmbstate.ytsize=0
endelse

sec_pixs=cmbmovie.mvihdrs.file_hdr.SEC_PIX
hsizes=cmbmovie.mvihdrs.file_hdr.nx
nf=cmbmovie.mvihdrs.file_hdr.nf

xisize=round(hsizes/(max(sec_pixs)/sec_pixs))

order=reverse(sort(xisize+fsc)) ;largest to smallest image size
big=order(0)
msec_pixs=sec_pixs(big)

mtaic=dblarr(max(nf),n_Elements(cmbmovie.mvis))
for tn=0,n_Elements(cmbmovie.mvis)-1 do begin
     if tn eq 0 then mvin=cmbmovie.mvi0
     if tn eq 1 then mvin=cmbmovie.mvi1  
     if tn eq 2 then mvin=cmbmovie.mvi2  
     if tn eq 3 then mvin=cmbmovie.mvi3  
     if tn eq 4 then mvin=cmbmovie.mvi4  
     if tn eq 5 then mvin=cmbmovie.mvi5  
     if tn eq 6 then mvin=cmbmovie.mvi6  
     if tn eq 7 then mvin=cmbmovie.mvi7  
     if tn eq 8 then mvin=cmbmovie.mvi8 
     if tn eq 9 then mvin=cmbmovie.mvi9  
     mtaic(0:nf(tn)-1,tn)=anytim2tai(mvin.hdrs.date_obs+' '+strmid(mvin.hdrs.time_obs,0,6))
endfor
cadance=min(mtaic(1,*)-mtaic(0,*))
alltimes=mtaic(sort(mtaic)) & alltimes=alltimes(where(alltimes ne 0))
n=0
while n le n_Elements(alltimes)-1 do begin
  n2=where(alltimes le alltimes(n)+cadance/2)
  if n eq 0 then tai1=alltimes(n_Elements(n2)-1) else tai1=[tai1,alltimes(n_Elements(n2)-1)]
  n=n2(n_Elements(n2)-1) +1
endwhile
tframes=n_elements(tai1)
if keyword_set(preview) then begin
   nframes=3 
   tai1=[tai1(0),tai1(n_Elements(tai1)/2),tai1(n_Elements(tai1)-1)]
   defimgs=1
endif else begin 
  nframes=n_elements(tai1)
  print,'Creating movie frames...It may take a while depending on the movie size/number of frames'
endelse 


mviahdrs=replicate(cmbmovie.mvi0.hdrs(0),nframes)

if keyword_set(outmovie) then  BREAK_FILE, outmovie.outfile, a, outdir, outname, outext
first = 1

usef = intarr(nframes,n_Elements(cmbmovie.mvis))
ftimes = strarr(nframes,n_Elements(cmbmovie.mvis))
lus=intarr(10) +(-1)

for n=0,n_Elements(cmbmovie.mvis)-1 do begin
     if order(n) eq 0 then mvin=cmbmovie.mvi0
     if order(n) eq 1 then mvin=cmbmovie.mvi1  
     if order(n) eq 2 then mvin=cmbmovie.mvi2  
     if order(n) eq 3 then mvin=cmbmovie.mvi3  
     if order(n) eq 4 then mvin=cmbmovie.mvi4  
     if order(n) eq 5 then mvin=cmbmovie.mvi5  
     if order(n) eq 6 then mvin=cmbmovie.mvi6  
     if order(n) eq 7 then mvin=cmbmovie.mvi7  
     if order(n) eq 8 then mvin=cmbmovie.mvi8 
     if order(n) eq 9 then mvin=cmbmovie.mvi9  
     if notsecchi gt 0 then aorb='' else aorb=strmid(mvin.hdrs(0).FILENAME,strpos(mvin.hdrs(0).FILENAME,'.')-1,1)
     taic=anytim2tai(mvin.hdrs.date_obs+' '+strmid(mvin.hdrs.time_obs,0,6))

     for nc=0,nframes-1 do begin
         uts=max(where(taic le tai1(nc)))
         if uts ne -1 then usef(nc,n)=uts else usef(nc,n)=0
         if n eq 0 then begin
	     ftimes(nc,n)= strmid(utc2str(tai2utc(tai1(nc))),0,10)+'  '+mvin.hdrs(0).detector+aorb+' '+strmid(mvin.hdrs(usef(nc,n)).time_obs,0,8)
	 endif else ftimes(nc,n)=mvin.hdrs(0).detector+aorb+' '+strmid(mvin.hdrs(usef(nc,n)).time_obs,0,8)
;         if taic(usef(nc)) eq tai1(nc) then mviahdrs(nc)=mvin.hdrs(usef(nc))
     endfor
     if n eq 0 then begin
        red = [mvin.rgbvec(indgen(256), 0)]
        green = [mvin.rgbvec(indgen(256), 1)]
        blue = [mvin.rgbvec(indgen(256), 2)]
     endif else begin
        red = [red,mvin.rgbvec(indgen(256), 0)]
        green = [green,mvin.rgbvec(indgen(256), 1)]
        blue = [blue,mvin.rgbvec(indgen(256), 2)]
     endelse  

   if n eq 0 then begin
     mviahdrs=mvin.hdrs(usef(*,n))
     mviahdrs.date_obs=strmid(utc2str(tai2utc(tai1)),0,10)
     mviahdrs.time_obs=strmid(utc2str(tai2utc(tai1)),11,12)
   endif


;   umsk=where(cmbstate.detectors eq mvin.hdrs(0).detector+aorb) 
;   umsk=umsk(0)
;   if cmbstate.outer(umsk) eq -1 then begin
;        maskfile=getenv('SECCHI_CAL')+'/'+ cmbstate.masks(umsk)
;        mskim=congrid(sccreadfits(maskfile,mhdr)+1,xisize(order(n)),yisize(order(n)))
;   endif




     if order(n) eq 0 then begin
        OPENR, lu, cmbmovie.mvis(0),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs0, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(0)=lu
     endif
     if order(n) eq 1 then  begin
        OPENR, lu, cmbmovie.mvis(1),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs1, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(1)=lu
     endif
     if order(n) eq 2 then  begin
        OPENR, lu, cmbmovie.mvis(2),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs2, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(2)=lu
     endif  
     if order(n) eq 3 then  begin
        OPENR, lu, cmbmovie.mvis(3),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs3, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(3)=lu
     endif  
     if order(n) eq 4 then  begin
        OPENR, lu, cmbmovie.mvis(4),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs4, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(4)=lu
     endif  
     if order(n) eq 5 then  begin
        OPENR, lu, cmbmovie.mvis(5),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs5, imgs5, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(5)=lu
     endif  
     if order(n) eq 6 then  begin
        OPENR, lu, cmbmovie.mvis(6),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs6, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(6)=lu
     endif 
     if order(n) eq 7 then  begin
        OPENR, lu, cmbmovie.mvis(7),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs7, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(7)=lu
     endif  
     if order(n) eq 8 then  begin
        OPENR, lu, cmbmovie.mvis(8),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs8, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(8)=lu
     endif 
     if order(n) eq 9 then  begin
        OPENR, lu, cmbmovie.mvis(9),/get_lun
        SCCREAD_MVI,lu, file_hdr, ihdrs, imgs9, swapflag, pngmvi=cmbmovie.mvtype(order(n))
        lus(9)=lu
     endif  
    IF (debug_on) THEN BEGIN
    	help,/str,file_hdr
	wait,2
    ENDIF
     undefine,lu
endfor







win_index=intarr(nframes)

for nm=0,nframes-1 do begin
   print,'Creating Frame # ',string(nm),' of ',trim(nframes-1)
   for n=0,n_Elements(cmbmovie.mvis)-1 do begin
     nc1=n*256 & nc2=((n+1)*256)-1
     if order(n) eq 0 then mvin=cmbmovie.mvi0
     if order(n) eq 1 then mvin=cmbmovie.mvi1  
     if order(n) eq 2 then mvin=cmbmovie.mvi2  
     if order(n) eq 3 then mvin=cmbmovie.mvi3  
     if order(n) eq 4 then mvin=cmbmovie.mvi4  
     if order(n) eq 5 then mvin=cmbmovie.mvi5  
     if order(n) eq 6 then mvin=cmbmovie.mvi6  
     if order(n) eq 7 then mvin=cmbmovie.mvi7  
     if order(n) eq 8 then mvin=cmbmovie.mvi8 
     if order(n) eq 9 then mvin=cmbmovie.mvi9  
     if order(n) eq 0 then imgsin=imgs0
     if order(n) eq 1 then imgsin=imgs1  
     if order(n) eq 2 then imgsin=imgs2  
     if order(n) eq 3 then imgsin=imgs3  
     if order(n) eq 4 then imgsin=imgs4  
     if order(n) eq 5 then imgsin=imgs5  
     if order(n) eq 6 then imgsin=imgs6  
     if order(n) eq 7 then imgsin=imgs7  
     if order(n) eq 8 then imgsin=imgs8 
     if order(n) eq 9 then imgsin=imgs9  
     aorb=strmid(mvin.hdrs(0).FILENAME,strpos(mvin.hdrs(0).FILENAME,'.f')-1,1)
          
        if cmbmovie.mvihdrs.file_hdr(order(n)).truecolor eq 0 then  begin
           uimg = bytarr(cmbmovie.mvihdrs.file_hdr(order(n)).nx, cmbmovie.mvihdrs.file_hdr(order(n)).ny)
           if (cmbmovie.mvtype(order(n)) ne '0') THEN uimg(*,*)=get_hdr_movie_img(string(imgsin(usef(nm,n))),mviname=cmbmovie.mvis(order(n))) else uimg(*,*) = imgsin(usef(nm,n))
           loadct,0
        endif else begin
           uimg = bytarr(3,cmbmovie.mvihdrs.file_hdr(order(n)).nx, cmbmovie.mvihdrs.file_hdr(order(n)).ny)
           if (cmbmovie.mvtype(order(n)) ne '0') THEN uimg(*,*,*)=get_hdr_movie_img(string(imgsin(usef(nm,n))),mviname=cmbmovie.mvis(order(n))) else uimg(*,*,*) = imgsin(usef(nm,n))
           uimg=Color_Quan(uimg, 1, redt, greent, bluet, COLORS=256)
           red(nc1:nc2) = redt
           green(nc1:nc2) = greent
           blue(nc1:nc2) = bluet
	endelse

      
      if order(n) eq 0 then begin
         uimgs0=uimg  & hdr0=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs0)] else nimg=[nimg,ptr_new(uimgs0)]
         if n eq 0 then hdrs=[ptr_new(hdr0)] else hdrs=[hdrs,ptr_new(hdr0)]
      endif
      if order(n) eq 1 then begin
         uimgs1=uimg  & hdr1=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs1)] else nimg=[nimg,ptr_new(uimgs1)]
         if n eq 0 then hdrs=[ptr_new(hdr1)] else hdrs=[hdrs,ptr_new(hdr1)]
      endif
      if order(n) eq 2 then begin
         uimgs2=uimg  & hdr2=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs2)] else nimg=[nimg,ptr_new(uimgs2)]
         if n eq 0 then hdrs=[ptr_new(hdr2)] else hdrs=[hdrs,ptr_new(hdr2)]
      endif
      if order(n) eq 3 then begin
         uimgs3=uimg  & hdr3=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs3)] else nimg=[nimg,ptr_new(uimgs3)]
         if n eq 0 then hdrs=[ptr_new(hdr3)] else hdrs=[hdrs,ptr_new(hdr3)]
      endif
      if order(n) eq 4 then begin
         uimgs4=uimg  & hdr4=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs4)] else nimg=[nimg,ptr_new(uimgs4)]
         if n eq 0 then hdrs=[ptr_new(hdr4)] else hdrs=[hdrs,ptr_new(hdr4)]
      endif
      if order(n) eq 5 then begin
         uimgs5=uimg  & hdr5=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs5)] else nimg=[nimg,ptr_new(uimgs5)]
         if n eq 0 then hdrs=[ptr_new(hdr5)] else hdrs=[hdrs,ptr_new(hdr5)]
      endif
      if order(n) eq 6 then begin
         uimgs6=uimg  & hdr6=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs6)] else nimg=[nimg,ptr_new(uimgs6)]
         if n eq 0 then hdrs=[ptr_new(hdr6)] else hdrs=[hdrs,ptr_new(hdr6)]
      endif
      if order(n) eq 7 then begin
         uimgs7=uimg  & hdr7=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs7)] else nimg=[nimg,ptr_new(uimgs7)]
         if n eq 0 then hdrs=[ptr_new(hdr7)] else hdrs=[hdrs,ptr_new(hdr7)]
      endif
      if order(n) eq 8 then begin
         uimgs8=uimg  & hdr8=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs8)] else nimg=[nimg,ptr_new(uimgs8)]
         if n eq 0 then hdrs=[ptr_new(hdr8)] else hdrs=[hdrs,ptr_new(hdr8)]
      endif
      if order(n) eq 9 then begin
         uimgs9=uimg  & hdr9=mvin.hdrs(usef(nm,n))
         if n eq 0 then nimg=[ptr_new(uimgs9)] else nimg=[nimg,ptr_new(uimgs9)]
         if n eq 0 then hdrs=[ptr_new(hdr9)] else hdrs=[hdrs,ptr_new(hdr9)]
      endif
    endfor

     if nm eq 0 then first=1 else first=0

     img=wcs_combine(nimg,file_hdr,hdrs=hdrs,truecolor=cmbstate.truv,ctable=[[red],[green],[blue]],hsize=cmbstate.rtsize(0),first=first,rtheta=cmbstate.rtheta-4,$
     	    	usemask=cmbstate.outer(order),xminmax=xminmax,yminmax=yminmax,bkgclr=bkgrd,fmask=cmbstate.masks(order),maskindeg=cmbstate.maskindeg,abmovie=abmovie,_EXTRA=_extra)

     if nm eq 0 then begin
       ufile_hdr=file_hdr
       ufile_hdr.NF=n_elements(nframes)
       if file_hdr.RTHETA le 4 then ufile_hdr.RADIUS0=file_hdr.RADIUS0-(cmbstate.ytsize*((file_hdr.RADIUS1-file_hdr.RADIUS0)/file_hdr.ny)) else $
            ufile_hdr.THETA0=file_hdr.THETA0-(cmbstate.ytsize*((file_hdr.THETA1-file_hdr.THETA0)/file_hdr.ny))
       ufile_hdr.ny=file_hdr.ny+cmbstate.ytsize
       ufile_hdr.SUNYCEN=file_hdr.SUNYCEN+cmbstate.ytsize
       leftxmax=file_hdr.nx
       leftymax=file_hdr.ny
       cmbstate.estmvisiz=(tframes*1.0)*(1.0+truecolor*2.0) * ufile_hdr.nx * ufile_hdr.ny
       ;cmbstate.rtheta=file_hdr.RTHETA
       cmbstate.rtsize=[file_hdr.nx,file_hdr.ny]
       cmbstate.rttha=[file_hdr.THETA0,file_hdr.THETA1]
       cmbstate.rtthb=[file_hdr.THETA0,file_hdr.THETA1]
       cmbstate.rtra=[file_hdr.RADIUS0,file_hdr.RADIUS1]
     endif


     if keyword_set(times) or cmbstate.times gt 0 then begin
        timestr=sctext(0)
        for nt=0,n_Elements(ftimes(nm,*))-1 do timestr=timestr+ftimes(nm,nt)+'  '
        WINDOW, XSIZE = file_hdr.nx, YSIZE = cmbstate.ytsize, /PIXMAP, /FREE
        xyouts,1,1,timestr,charsize=cmbstate.times,color=255,/device
        xyouts,file_hdr.nx-1,1,sctext(1),charsize=cmbstate.times,color=255,/device,alignment=1
        twindo=tvrd()
        if cmbstate.truv eq 0 then img=[[twindo],[img]] else begin
            timg=bytarr(3,file_hdr.nx,file_hdr.ny+cmbstate.ytsize)
            timg(0,*,*)=[[twindo],[reform(img(0,*,*))]]
            timg(1,*,*)=[[twindo],[reform(img(1,*,*))]]
            timg(2,*,*)=[[twindo],[reform(img(2,*,*))]]
            img=timg
	    undefine,timg
	endelse
     endif

;     IF (debug_on) THEN BEGIN
;     	print,'SCTEXT=',sctext
;     	wait,2
;     ENDIF



     if datatype(textframe) ne 'UND' then begin
       sze=size(textframe)
       if total(textframe) gt 0 then begin
         if cmbstate.truv eq 0 then begin
    	   if sze(1) eq 2 then begin
            textimg=reform(textframe(1,*,*))
             textclr=reform(textframe(0,*,*))
	    img(where(textimg eq 255))=textclr(where(textimg eq 255))
           endif else print,'Can not use textframe it was created in truecolor mode'
         endif
	 if cmbstate.truv eq 1 then begin
	   if sze(1) eq 4 then begin
             textimg=reform(textframe(3,*,*))
             for nc=0,2 do begin
                bimg=reform(img(nc,*,*))
                textclr=reform(textframe(nc,*,*))
                bimg(where(textimg eq 255))=textclr(where(textimg eq 255))
                img(nc,*,*)=bimg
             endfor
           endif else print,'Can not use textframe it was created in grayscale mode'
         endif
       endif
     endif 
     if ~keyword_set(outmovie) then begin
        WINDOW, XSIZE = ufile_hdr.nx, YSIZE = ufile_hdr.ny, /PIXMAP, /FREE
        win_index(nm) = !D.WINDOW
        tv,img,true=cmbstate.truv
     endif else begin
        ahdr=(*hdrs[0])
        ahdr.date_obs=utc2str(tai2utc(tai1(nm)))
        scc_save_mvframe,nm,outmovie.outfile, img, ahdr,ufile_hdr,first=first,inline=nframes,SCALE=outmovie.SCALE, $
               REFERENCE=outmovie.REFERENCE , mframes=outmovie.mframes, idlencode=outmovie.idlencode,colorquan=outmovie.colorquan,_EXTRA=_extra
      endelse
      IF (first EQ 1) THEN first = 0


     if keyword_set(defimgs) then begin
        if nm eq 0 and cmbstate.truv eq 0 then imgs=bytarr(ufile_hdr.nx,ufile_hdr.ny,nframes)
        if nm eq 0 and cmbstate.truv eq 1 then imgs=bytarr(3,ufile_hdr.nx,ufile_hdr.ny,nframes)
        if cmbstate.truv eq 0 then imgs(*,*,nm)=img else imgs(*,*,*,nm)=img 
     endif

endfor
if datatype(wtmp) eq 'UND' then wtmp=-1
if wtmp ne -1 then wdel,wtmp
for nl=0,9 do begin
  if lus(nl) ne -1 then begin
    close, lus(nl)
    free_lun,lus(nl)  
  endif
endfor

undefine,outmask
undefine,cimgt
undefine,msktmp
undefine,tmp
undefine,timgs
undefine,simg

rtcoords=[ufile_hdr.RADIUS0,ufile_hdr.RADIUS1,ufile_hdr.THETA0,ufile_hdr.THETA1]
if cmbstate.bpos gt 0 then osczero=[cmbstate.rtsize(0)/2-1,0] else osczero=0
if ~keyword_set(save_only) and ~keyword_set(outmovie) then scc_playmovie,win_index,hdrs=mviahdrs,file_hdr=ufile_hdr,$;,SUNXCEN=ufile_hdr.sunxcen, SUNYCEN=ufile_hdr.sunycen, SEC_PIX=ufile_hdr.sec_pix,$
    TRUECOLOR=TRUECOLOR,mvicombo=[leftxmax,leftymax,cmbstate.times*10], DEBUG=debug_on, osczero=osczero;,RTHETA=ufile_hdr.RTHETA,RTCOORDS=rtcoords

;endif

END

PRO draw_combined_crop
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

COMMON scc_playmv_COMMON, moviev
 
common wcs_combine, cmbmovie,cmbstate,struct ;needs to match definition in wcs_combine_mvi.pro
      WSET, moviev.draw_win
      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
      DEVICE, COPY = [0, 0, moviev.cmbvals(6), 2,moviev.cmbvals(8), moviev.cmbvals(9)+moviev.cmbvals(7), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.cmbvals(6), 2,moviev.cmbvals(8), moviev.cmbvals(9), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(7),moviev.cmbvals(8), moviev.cmbvals(9), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.cmbvals(7),moviev.cmbvals(8)+moviev.cmbvals(6), moviev.cmbvals(9), 11] ;right side
END

PRO SCC_CMBMOVIE_DRAW, ev
COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
common wcs_combine

   IF (ev.press GT 0) THEN RETURN 		;** only look at PRESS events

   ;;WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE
 IF (TAG_EXIST(moviev, 'cmbvals')) THEN BEGIN

  if (moviev.cmbvals(0) ne 3) then begin ;
   if moviev.file_hdr.rtheta ge 5 then begin
	  r=moviev.file_hdr.radius0+ev.x/(moviev.cmbvals(6)-1.)*(moviev.file_hdr.radius1-moviev.file_hdr.radius0)
          angle=moviev.file_hdr.theta0+ev.y/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.theta1-moviev.file_hdr.theta0)
	    if moviev.file_hdr.rtheta eq 6 then $
	       r=10^( alog10(moviev.file_hdr.radius0)+ev.x/(moviev.cmbvals(6)-1.) $     	
		  *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) ) 
   endif 
   if moviev.file_hdr.rtheta eq 0 then begin
            angle=ev.y*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0
            r=ev.x*moviev.file_hdr.sec_pix+moviev.file_hdr.theta0
   endif 

   if (moviev.cmbvals(0) eq 2) then begin ;second corner selected
      moviev.cmbvals(3)=r
      moviev.cmbvals(4)=angle
      moviev.cmbvals(6)=ev.x-moviev.cmbvals(8)
      moviev.cmbvals(7)=ev.y-moviev.cmbvals(9)
      WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cmbvals(3)
      WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cmbvals(4)
      WIDGET_CONTROL,moviev.cmbfield(0),SET_VALUE=moviev.cmbvals(6)
      WIDGET_CONTROL,moviev.cmbfield(1),SET_VALUE=moviev.cmbvals(7)
      draw_combined_crop
      insttxt='use mouse to select center'
      widget_control,moviev.cropinst,set_value=insttxt
      moviev.cmbvals(0)=3
   endif

   if (moviev.cmbvals(0) eq 1) then begin ; first corner selected
      moviev.cmbvals(1)=r
      moviev.cmbvals(2)=angle
      moviev.cmbvals(8)=ev.x
      moviev.cmbvals(9)=ev.y
      moviev.cmbvals(0)=2
      WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cmbvals(1)
      WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cmbvals(2)
      widget_control,moviev.cropinst,set_value='Select upper right corner'
   endif
  endif else begin
    moviev.cmbvals(8)=ev.x+(moviev.cmbvals(6)/2)
    moviev.cmbvals(9)=ev.y+(moviev.cmbvals(7)/2)
    if moviev.file_hdr.rtheta ge 5 then begin
	  r=moviev.file_hdr.radius0+moviev.cmbvals(8)/(moviev.cmbvals(6)-1.)*(moviev.file_hdr.radius1-moviev.file_hdr.radius0)
          angle=moviev.file_hdr.theta0+moviev.cmbvals(9)/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.theta1-moviev.file_hdr.theta0)
	    if moviev.file_hdr.rtheta eq 6 then $
	       r=10^( alog10(moviev.file_hdr.radius0)+moviev.cmbvals(8)/(moviev.cmbvals(6)-1.) $     	
		  *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) ) 
    endif 
    if moviev.file_hdr.rtheta eq 0 then begin
            angle=moviev.cmbvals(9)*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0
            r=moviev.cmbvals(8)*moviev.file_hdr.sec_pix+moviev.file_hdr.theta0
    endif 
    moviev.cmbvals(3)=r
    moviev.cmbvals(4)=angle
    moviev.cmbvals(8)=ev.x-(moviev.cmbvals(6)/2)
    moviev.cmbvals(9)=ev.y-(moviev.cmbvals(7)/2)
    if moviev.file_hdr.rtheta ge 5 then begin
	  r=moviev.file_hdr.radius0+moviev.cmbvals(8)/(moviev.cmbvals(6)-1.)*(moviev.file_hdr.radius1-moviev.file_hdr.radius0)
          angle=moviev.file_hdr.theta0+moviev.cmbvals(9)/(moviev.file_hdr.ny-1.)* (moviev.file_hdr.theta1-moviev.file_hdr.theta0)
	    if moviev.file_hdr.rtheta eq 6 then $
	       r=10^( alog10(moviev.file_hdr.radius0)+moviev.cmbvals(8)/(moviev.cmbvals(6)-1.) $     	
		  *(alog10(moviev.file_hdr.radius1) -alog10(moviev.file_hdr.radius0)) ) 
    endif 
    if moviev.file_hdr.rtheta eq 0 then begin
            angle=moviev.cmbvals(9)*moviev.file_hdr.sec_pix+moviev.file_hdr.radius0
            r=moviev.cmbvals(8)*moviev.file_hdr.sec_pix+moviev.file_hdr.theta0
    endif 

    moviev.cmbvals(1)=r
    moviev.cmbvals(2)=angle
    WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cmbvals(3)
    WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cmbvals(4)
    WIDGET_CONTROL,moviev.cmbfield(0),SET_VALUE=moviev.cmbvals(6)
    WIDGET_CONTROL,moviev.cmbfield(1),SET_VALUE=moviev.cmbvals(7)
    WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cmbvals(1)
    WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cmbvals(2)
    draw_combined_crop
  endelse
endif

  ;WIDGET_CONTROL, base, SET_UVALUE=moviev

END

PRO SCC_CMBMOVIE_CONTROL_EVENT, ev

common wcs_comb,coord,masks,imo,rimo,gimo,bimo,imsizeout,cdate,wcssecchi,wcshdr,wcssoho,wcsptr,cfile_hdr,cdelts,wcsorder ; needed for mask units
COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
common wcs_combine

;WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

    IF (TAG_EXIST(ev, 'value')) THEN  IF (DATATYPE(ev.value) EQ 'STR') THEN input=ev.value else WIDGET_CONTROL, ev.id, GET_UVALUE=input
   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
    IF (DATATYPE(input) EQ 'STR') THEN BEGIN

      CASE (input) OF

          'mdetv':begin
	       dx=widget_info(ev.id,/uname)
               cmbstate.outer(where(cmbstate.detectors eq dx))=ev.value
           end

          'mcharsv':moviev.cmbvals(11)=ev.value*10
	  'cmbzoomsel' : BEGIN	;select zoom coords using any mouse
	         if moviev.file_hdr.rtheta eq 0 then arcadj=3600. else arcadj=1.
                 WIDGET_CONTROL, moviev.cmbsliden, GET_VALUE=val
                 moviev.cmbvals(5)=float(string(val,'(f6.2)'))
                 WIDGET_CONTROL, moviev.cmbfield(6), SET_VALUE=moviev.cmbvals(5)
                 moviev.cmbvals(6)= (moviev.cmbvals(3)-moviev.cmbvals(1))/(moviev.cmbvals(5)/arcadj)
	         moviev.cmbvals(7)=(moviev.cmbvals(4)-moviev.cmbvals(2))/(moviev.cmbvals(5)/arcadj)
                 WIDGET_CONTROL, moviev.cmbfield(0), SET_VALUE=moviev.cmbvals(6)
                 WIDGET_CONTROL, moviev.cmbfield(1), SET_VALUE=moviev.cmbvals(7)
           END
    	'cmszoomt': BEGIN	;select zoom coords using any mouse
	         if moviev.file_hdr.rtheta eq 0 then arcadj=3600. else arcadj=1.
                 moviev.cmbvals(5)=ev.value
                 WIDGET_CONTROL, moviev.cmbsliden, SET_VALUE=moviev.cmbvals(5)
                 moviev.cmbvals(6)= (moviev.cmbvals(3)-moviev.cmbvals(1))/(moviev.cmbvals(5)/arcadj)
	         moviev.cmbvals(7)=(moviev.cmbvals(4)-moviev.cmbvals(2))/(moviev.cmbvals(5)/arcadj)
                 WIDGET_CONTROL, moviev.cmbfield(0), SET_VALUE=moviev.cmbvals(6)
                 WIDGET_CONTROL, moviev.cmbfield(1), SET_VALUE=moviev.cmbvals(7)
           END


        'mcolor' : moviev.truecolor= ev.index
        'bpos'  : cmbstate.bpos = ev.index
        'brclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, moviev.brclr, GET_VALUE=val
                cmbmovie.bkgrd(0)=val 
          END
        'bgclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, moviev.bgclr, GET_VALUE=val
                cmbmovie.bkgrd(1)=val
          END
        'bbclr' : BEGIN   ;set red color value or color value if not true
                WIDGET_CONTROL, moviev.bbclr, GET_VALUE=val
                cmbmovie.bkgrd(2)=val
          END
       'bkselect'  :  BEGIN; if moviev.true eq 1 then moviev.bkgrd=reform(moviev.bkgval(*,ev.value)) else moviev.bkgrd=moviev.bkgval(ev.value)
                        if moviev.truecolor eq 1 and moviev.bgclr  gt 0 then begin
                              if ev.index ne 0 then begin
			        cmbmovie.bkgrd=reform(moviev.bkgval(*,ev.index))
                                WIDGET_CONTROL, moviev.brclr, SET_VALUE=cmbmovie.bkgrd(0)
                                WIDGET_CONTROL, moviev.bgclr, SET_VALUE=cmbmovie.bkgrd(1)
                                WIDGET_CONTROL, moviev.bbclr, SET_VALUE=cmbmovie.bkgrd(2)
                              endif
                              if ev.index eq 0 then begin
			        WIDGET_CONTROL, moviev.brclr, GET_VALUE=val
				cmbmovie.bkgrd(0)=val
                                WIDGET_CONTROL, moviev.bgclr, GET_VALUE=val
				cmbmovie.bkgrd(1)=val
                                WIDGET_CONTROL, moviev.bbclr, GET_VALUE=val
				cmbmovie.bkgrd(2)=val
                              endif
			endif else begin
			      if ev.index ne 0 then cmbmovie.bkgrd(0)=moviev.bkgval(ev.index)
                              WIDGET_CONTROL, moviev.brclr, SET_VALUE=cmbmovie.bkgrd(0)
                              if ev.index eq 0 then begin
                                WIDGET_CONTROL, moviev.brclr, GET_VALUE=val
				cmbmovie.bkgrd(0)=val
                              endif
                        endelse
                 END

	'xsize' : moviev.cmbvals(6)=ev.value
	'ysize' : moviev.cmbvals(7)=ev.value
	'rtth0a'  : moviev.cmbvals(1)=ev.value
	'rtth1a'  : moviev.cmbvals(3)=ev.value
	'rtra0'   : moviev.cmbvals(2)=ev.value
	'rtra1'   : moviev.cmbvals(4)=ev.value
	'mtypev'   :  BEGIN
                    if cmbstate.rtheta ne ev.index+4 then begin
                       cmbstate.rtheta=ev.index+4 
	    	       moviev.cmbvals(1:4)=0
	    	       moviev.cmbvals(6:9)=0
    	    	       WIDGET_CONTROL,moviev.cmbfield(3),SET_VALUE=moviev.cmbvals(3)
    	    	       WIDGET_CONTROL,moviev.cmbfield(5),SET_VALUE=moviev.cmbvals(4)
    	    	       WIDGET_CONTROL,moviev.cmbfield(0),SET_VALUE=moviev.cmbvals(6)
    	    	       WIDGET_CONTROL,moviev.cmbfield(1),SET_VALUE=moviev.cmbvals(7)
    	    	       WIDGET_CONTROL,moviev.cmbfield(2),SET_VALUE=moviev.cmbvals(1)
    	    	       WIDGET_CONTROL,moviev.cmbfield(4),SET_VALUE=moviev.cmbvals(2)
		    endif
	     END
	'masktypev'   :  BEGIN
                     if ev.index ne cmbstate.maskindeg then begin
		       wcst=(*wcsptr(0))
		       if ev.index eq 0 then cmbstate.outer=(wcst.position.dsun_obs/((180/!pi)*1000*onersun()))*cmbstate.outer
                       if ev.index eq 1 then cmbstate.outer=((180/!pi)*1000*onersun()/wcst.position.dsun_obs)*cmbstate.outer
		       for nd =0,n_elements(cmbstate.detectors)-1 do WIDGET_CONTROL,moviev.mc3sv(nd),SET_VALUE=cmbstate.outer(nd)
                       cmbstate.maskindeg=ev.index
		     endif
	     END
	'raxisv'   : cmbstate.rtaxis= ev.index

	ELSE : BEGIN
                 PRINT, '%%SCC_CMBMOVIE_CONTROL.  Unknown event.'
	     END

    ENDCASE

  ENDIF

 ; WIDGET_CONTROL, moviev.base, SET_UVALUE=moviev

;  RETURN, 0

END 

FUNCTION SCC_CMBMOVIE_CONTROL_FUNC, ev

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
common wcs_combine


   input = ev.value

   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
   if input ne '' then begin
    CASE (input) OF


	'cmbcrop' : BEGIN	;select crop coords using any mouse
                      WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                      moviev.stall = 10000.
		      moviev.cmbvals(0)=1 ;crop selected
                      inst='select lower left corner'
                      widget_control,moviev.cropinst,set_value=inst
	    END

                     
	'cmbcropcen' : BEGIN	;select crop coords using any mouse
                      if mvoiev.cropsz(0) gt 0 and mvoiev.cropsz(1) gt 0 then begin
                        WIDGET_CONTROL, moviev.cmndbase, TIMER=10000.   ; Put timer on hold
                        moviev.stall = 10000.
		        moviev.cmbvals(0)=3 ;crop selected
                        inst='use mouse to select center'
		      endif else inst='crop size not set'
                      widget_control,moviev.cropinst,set_value=inst
	    END


	'cmbcancel' : BEGIN
		      moviev.cmbvals(0)=0 ;crop unselected
	    	      moviev.cmbvals(1:4)=0
	    	      moviev.cmbvals(6:9)=0
                      WSET, moviev.draw_win
                      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                      widget_control,moviev.cropinst,set_value='Crop is not selected'
	    END
	'cmbreset' : BEGIN	;select zoom coords using any mouse
	    	    moviev.cmbvals(0:5)=0
	    	    moviev.cmbvals(8:9)=0
	    	    moviev.cmbvals(10)=1
	    END

         'CMBPREVIEW' : BEGIN
                    cmbvals=moviev.cmbvals
                    true=moviev.truecolor
	    	    exit_playmovie
                    if cmbvals(10) eq 1 then restore=1 else restore=0
                    if cmbvals(11) gt 0 then times=cmbvals(11)/10. else times=0
 		    wcs_combine_movies,hsize=cmbvals(6),yminmax=[cmbvals(2),cmbvals(4)],xminmax=[cmbvals(1),cmbvals(3)],$
		    	    	    pixscale=cmbscl,truecolor=true,/preview,restore=restore,times=times
                    RETURN, 0
	    END
	 
         'CMBMKMOVIE' : BEGIN
                    cmbvals=moviev.cmbvals
                    true=moviev.truecolor
	    	    exit_playmovie
                    if cmbvals(10) eq 1 then restore=1 else restore=0
                    if cmbvals(11) gt 0 then times=cmbvals(11)/10. else times=0
		    wcs_combine_movies,hsize=cmbvals(6),yminmax=[cmbvals(2),cmbvals(4)],xminmax=[cmbvals(1),cmbvals(3)],pixscale=cmbscl,truecolor=true,restore=restore,times=times
                    RETURN, 0
	    END
         'CMBSVMOVIE' : BEGIN
                    ofn=moviev.filename
                    ofp=moviev.filepath
                    outmovie=''
                    scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=ofn,len=moviev.len,first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect,/selectonly,outmovie=outmovie;scc_movieout,moviev.base ,/selectonly
		    IF (outmovie.outfile ne moviev.filename and outmovie.outfile ne '') THEN BEGIN
                        moviev.filename=outmovie.outfile
                        BREAK_FILE, outmovie.outfile, a, dir1, name, ext
		        spawn,['mkdir',dir1],/NOSHELL
                        cmbvals=moviev.cmbvals
                        true=moviev.truecolor
	    	        exit_playmovie
                        if cmbvals(10) eq 1 then restore=1 else restore=0
                        if cmbvals(11) gt 0 then times=cmbvals(11)/10. else times=0
		        wcs_combine_movies,hsize=cmbvals(6),yminmax=[cmbvals(2),cmbvals(4)],xminmax=[cmbvals(1),cmbvals(3)],pixscale=cmbscl,truecolor=true,restore=restore,times=times,outmovie=outmovie
                        RETURN, 0
                     ENDIF else begin
		         print,'******************************************'
		         print,'*****  Movie filename not selected   *****'
		         print,'******************************************'
                    ENDELSE
	    END
         'CMBWIMGS' : BEGIN
                    cmbvals=moviev.cmbvals
                    true=moviev.truecolor
	    	    exit_playmovie
                    if cmbvals(10) eq 1 then restore=1 else restore=0
                    if cmbvals(11) gt 0 then times=cmbvals(11)/10. else times=0
		    wcs_combine_movies,hsize=cmbvals(6),yminmax=[cmbvals(2),cmbvals(4)],xminmax=[cmbvals(1),cmbvals(3)],pixscale=cmbscl,truecolor=true,restore=restore,times=times,/defimgs
                    RETURN, 0
	    END
         'CMBSETUP' : BEGIN
	    	    exit_playmovie
		    Wcs_combine_MVI
                    RETURN, 0
	    END

         'CMBADDTEXT' : BEGIN
                     annotate_image,moviev.win_index,/template,true=moviev.truecolor
	    END
	'CMBCONTROL' : BEGIN	;show cmbcontrol window
              IF (moviev.showcmb EQ 0) THEN BEGIN
                 WIDGET_CONTROL, moviev.cmbbase, MAP=1
                 moviev.showcmb = 1
              ENDIF ELSE BEGIN
                WIDGET_CONTROL, moviev.cmbbase, MAP=0
                moviev.showcmb = 0
              ENDELSE
	    END

        'CMBCLRTEXT' : BEGIN
                     undefine,textframe
	    END

	ELSE : BEGIN
                 PRINT, '%%SCC_CMBMOVIE_CONTROL.  Unknown event.'
	     END

    ENDCASE

  ENDIF


;  RETURN, 0

END 

PRO SCC_CMBMOVIE,mvicombo

COMMON WSCC_MKMOVIE_COMMON
COMMON scc_playmv_COMMON
common wcs_combine
   cmbbase = WIDGET_BASE(/COLUMN, XOFFSET=moviev.basexoff(1), YOFFSET=moviev.baseyoff(1), TITLE='wcs_combine_MVI Control',EVENT_PRO='SCC_CMBMOVIE_CONTROL_EVENT')

              cmbsliden=0
              cropinst=0	
	      cmbvals=moviev.cmbvals; 0=zoom select 1=x1range 2=x2range 3=y1range 4=y2range 5=scale factor 6=xsize 7=ysize 8=x0 9=y0 10=restore 11=times size
              bkgcolor=0	; background for annotation text
              WINDOW, 11, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='cs_combine_MVI zoom position'
              erase,bkgcolor  ; this is the color behind annotations on the image from the cursor position
              cmbvals(6)=mvicombo(0)
              cmbvals(7)=mvicombo(1)
              cmbvals(11)=mvicombo(2)
              cmbbase4a = WIDGET_BASE(cmbbase, /ROW, /Frame)

              cropsz=intarr(6) +0
              cmbfield=intarr(8)

               cmbbase4c = WIDGET_BASE(cmbbase4a, /colum,/frame)
              	  tmp=widget_label(cmbbase4c,  value='Masks')
    	    	  tmp=widget_label(cmbbase4c,  value='(0=none)')
    	    	  if cmbstate.notsecchi eq 0 then tmp=widget_label(cmbbase4c,  value='-1 = Use Mask File') 
                  ;if moviev.file_hdr.rtheta eq 4 or moviev.file_hdr.rtheta eq 0 then begin
                    masktype=['Rsun','Deg']
                    masktypev=widget_droplist(cmbbase4c, VALUE=masktype, UVALUE='masktypev',title='Units :')
                    widget_control,masktypev,set_droplist_select=cmbstate.maskindeg
                  ;endif
                  mc3sv=intarr(n_elements(cmbstate.detectors))
                  for nd =0,n_elements(cmbstate.detectors)-1 do begin
                    cmbbase4e = WIDGET_BASE(cmbbase4c, /row)
                    mc3sv(nd)=CW_FIELD(cmbbase4e, value=cmbstate.outer(nd), uname=cmbstate.detectors(nd), uvalue="mdetv",title=cmbstate.detectors(nd),/all_events,xsize=5,/floating)
                  endfor

                cmbbase4ba = WIDGET_BASE(cmbbase4a, /colum,/frame);,title='Position/Crop')

                fnt3bt = WIDGET_BASE(cmbbase4ba,/colum,/frame)
                mtype=['Sun-centered','Polar','Polar Xlog']
                mtypev=widget_droplist(fnt3bt, VALUE=mtype, UVALUE='mtypev',title='Movie Type :')
                widget_control,mtypev,set_droplist_select=cmbstate.rtheta-4



    	    	cmbvals(6)=cmbstate.rtsize(0)
                if cmbstate.bpos gt 0 then begin
		   cmbvals(6)=(cmbstate.rtsize(0)/2)-2
		   cmbstate.rtsize(0)=cmbvals(6)
                endif
    	    	cmbvals(7)=cmbstate.rtsize(1)
                if cmbstate.rtheta lt 5 then begin
    	    	  cmbvals(1)=cmbstate.rttha(0)
		  cmbvals(2)=cmbstate.rtra(0)
    	    	  cmbvals(3)=cmbstate.rttha(1)
		  cmbvals(4)=cmbstate.rtra(1)
                endif else begin
    	    	  cmbvals(2)=cmbstate.rttha(0)
		  cmbvals(1)=cmbstate.rtra(0)
    	    	  cmbvals(4)=cmbstate.rttha(1)
		  cmbvals(3)=cmbstate.rtra(1)
		endelse
                cmbbase4ba = WIDGET_BASE(cmbbase4ba, /colum,/frame,title='Position/Crop')
                tmp=widget_label(cmbbase4ba,  value=' Crop ')
                cmbfield=intarr(8)

                ;cmbbase4b3a = WIDGET_BASE(cmbbase4ba, /row)
                ; tmp=widget_label(cmbbase4ba,  value='radius (0-88) | theta (0-360) Same sun center uses A only')
                cmbbase4b3 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(2)=CW_FIELD(cmbbase4b3, value=string(cmbvals(1),'(f6.2)'), uvalue="rtth0a", $
	      	    title='Xmin',/all_events,xsize=7)
                cmbfield(3)=CW_FIELD(cmbbase4b3, value=string(cmbvals(3),'(f6.2)'), uvalue="rtth1a", $
	      	    title='Xmax',/all_events,xsize=7 )

                cmbbase4b5 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(4)=CW_FIELD(cmbbase4b5, value=string(cmbvals(2),'(f6.2)'), uvalue="rtra0", $
	      	    title='Ymin',/all_events,xsize=7 )
                cmbfield(5)=CW_FIELD(cmbbase4b5, value=string(cmbvals(4),'(f6.2)'), uvalue="rtra1", $
 	      	    title='Ymax',/all_events,xsize=7)
                cmbbase4b6 = WIDGET_BASE(cmbbase4ba,/row)
    	    	tmp=widget_label(cmbbase4b6,  value='XY units are degrees')
 
                cmbbase4b6 = WIDGET_BASE(cmbbase4ba,/row)
  	        tmp = CW_BGROUP(cmbbase4b6,  ['Select/Position Crop','Cancel Crop'], /ROW, $
	        BUTTON_UVALUE = ['cmbcrop','cmbcancel'], EVENT_FUNCT='SCC_CMBMOVIE_CONTROL_FUNC')

                cmbbase4b5 = WIDGET_BASE(cmbbase4ba, /row)
                cropinst=widget_text(cmbbase4b5,value='Not selected',uvalue='cropinst',font='-1',XSIZE=30)


                cmbbase4b2 = WIDGET_BASE(cmbbase4ba, /row)
                cmbfield(0)=CW_FIELD(cmbbase4b2, value=string(cmbstate.rtsize(0),'(i5)'), uvalue="xsize", $
	      	    title='Xsize',/all_events,xsize=5 )
                cmbfield(1)=CW_FIELD(cmbbase4b2, value=string(cmbstate.rtsize(1),'(i5)'), uvalue="ysize", $
	      	    title='Ysize',xsize=5 )
 
                if moviev.file_hdr.rtheta eq 4 or moviev.file_hdr.rtheta eq 0 then begin
                  cmbvals(5)=((cmbvals(3)-cmbvals(1))/cmbvals(6))*3600.
                  cmbbase4b2z = WIDGET_BASE(cmbbase4ba, /row)
    	          cmbfield(6) = CW_FIELD(cmbbase4b2z, VALUE=string(cmbvals(5),'(f6.2)'), XSIZE=10, YSIZE=1, UVALUE='cmszoomt', /ALL_EVENTS, TITLE='Zoom:',/floating)
    	    	    tmp=widget_label(cmbbase4b2z,  value='(arcsec/pixel)')
                  cmbbase4b2z = WIDGET_BASE(cmbbase4ba, /row)
                   cmbsliden = CW_FSLIDER (cmbbase4b2z, TITLE='Select platescale value', VALUE=cmbstate.sec_pixs, UVALUE='cmbzoomsel', $
                   /DRAG, MIN=min(cmbmovie.mvihdrs.file_hdr.sec_pix)-1, MAX=max(cmbmovie.mvihdrs.file_hdr.sec_pix)+1,/SUPPRESS_VALUE,scroll=0.01,double=0)
                   widget_control, cmbsliden, set_val=cmbvals(5)
                endif

 
     ;            cmbbase4b6 = WIDGET_BASE(cmbbase4ba,/row)
     ;                ylogt=['Linear','Log']
     ;                ylogv=widget_droplist(cmbbase4b6, VALUE=ylogt, UVALUE='ylogv',title='Radius Scale :')
     ;                widget_control,ylogv,set_droplist_select=cmbstate.ylog
     ;                raxis=['off','on']
     ;                ylogv=widget_droplist(cmbbase4b6, VALUE=raxis, UVALUE='raxisv',title='Plot Axis :')
     ;                widget_control,ylogv,set_droplist_select=cmbstate.rtaxis


              bkgrow = WIDGET_BASE(cmbbase4a,/colum)
              lab2='Background:'
              f4sz=140

              if cmbstate.notsecchi eq 0 then begin
                     fnt3bp = WIDGET_BASE(bkgrow,/row,/frame)
                     bpost=['Same sun center','Left side','Right side']
                     bposv=widget_droplist(fnt3bp, VALUE=bpost, UVALUE='bpos',title='B position :')
                     widget_control,bposv,set_droplist_select=cmbstate.bpos
              endif


                     fnt3ba = WIDGET_BASE(bkgrow,/row,/frame)
                     tru=['Grayscale','Truecolor']
                     mvtru =   widget_droplist(fnt3ba, VALUE=tru, UVALUE='mcolor',title='Movie Color:')
                     widget_control,mvtru,set_droplist_select=moviev.truecolor
 
	       if moviev.truecolor eq 0 then begin
                     bkgarr=['Select','White','Median','Black']
                     bkgval=[-2,255,127,0]
                     ;bkgrd=bkgval(0)
                     fnt3bb = WIDGET_BASE(bkgrow,/colum,/frame,title='Background Color')
                     mvtxtc =   widget_droplist(fnt3bb, VALUE=bkgarr, UVALUE='bkselect',title=lab2)
                     fnt3bbr1 = WIDGET_BASE(fnt3bb,/row)
                     mvtxt=widget_label (fnt3bbr1,  value='Val:')
                     brclr = WIDGET_SLIDER(fnt3bbr1,  VALUE=cmbmovie.bkgrd(0), UVALUE='brclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
              endif else begin
                    bkgarr=[  'Select',      'Black',    'White',      'Red',        'Blue',       'Green',     'yellow']
                    bkgval=[[-2,-2,-2],[000,000,000],[255,255,255],[255,000,000],[000,000,255],[000,255,000],[229,229,056]]
                    ;bkgrd=reform(bkgval(*,0))
                    fnt3bb = WIDGET_BASE(bkgrow,/colum,/frame,title='Background Color')
                    mvtxtc =   widget_droplist(fnt3bb, VALUE=bkgarr, UVALUE='bkselect',title=lab2)
                    fnt3bbr1 = WIDGET_BASE(fnt3bb,/row)
                    mvtxt=widget_label (fnt3bbr1,  value='R:')
                    brclr = WIDGET_SLIDER(fnt3bbr1,  VALUE=cmbmovie.bkgrd(0), UVALUE='brclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='R:')
                    fnt3bbr2 = WIDGET_BASE(fnt3bb,/row)
                    mvtxt=widget_label (fnt3bbr2,  value='G:')
                    bgclr = WIDGET_SLIDER(fnt3bbr2,  VALUE=cmbmovie.bkgrd(1), UVALUE='bgclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='G:')
                    fnt3bbr3 = WIDGET_BASE(fnt3bb,/row)
                    mvtxt=widget_label (fnt3bbr3,  value='B:')
                    bbclr = WIDGET_SLIDER(fnt3bbr3,  VALUE=cmbmovie.bkgrd(2), UVALUE='bbclr', /DRAG, MIN=0, MAX=255,scr_xsize=f4sz,/scroll);, title='B:')
                    moviev.bgclr=bgclr 
                    moviev.bbclr=bbclr 
              endelse



        ;      cmbbase4b2a = WIDGET_BASE(cmbbase, /row)


              cmbbase4bca = WIDGET_BASE(bkgrow, /colum,/frame)
;              cmbbase4b3a = WIDGET_BASE(cmbbase, /row)
              cmbfield(7)=CW_FIELD(cmbbase4bca, value=string(cmbvals(11)/10.,'(f3.1)'), uvalue="mcharsv", $
	      	    title='Timestamp Charsize (0=none)',/all_events,xsize=3,/floating)

 
 	      tmp = CW_BGROUP(cmbbase,   ['Restore to Original ','Return to Setup ','Clear Text '], /ROW, $
	      BUTTON_UVALUE = ['cmbreset','CMBSETUP','CMBCLRTEXT'], EVENT_FUNCT='SCC_CMBMOVIE_CONTROL_FUNC')

	      tmp = CW_BGROUP(cmbbase, [ $
	      ' Preview ', $
	      'Make/Show Movie ', $
	      'Make/Save Movie ', $
	      'Make/Show Movie for Annotation', $
	      ' CLOSE '], /ROW, $
	      BUTTON_UVALUE = ['CMBPREVIEW','CMBMKMOVIE','CMBSVMOVIE','CMBWIMGS','CMBCONTROL'], EVENT_FUNCT='SCC_CMBMOVIE_CONTROL_FUNC')

              tmp=widget_label(cmbbase, /frame ,$
	           value='Estimated MVI size = '+string(round(cmbstate.estmvisiz/(1024.*1024.)),'(i5)')+$
	          'MB  PNG or GIF movie size = '+string(round((cmbstate.estmvisiz/(1024.*1024.))/(1.0+cmbstate.truv*2.0)),'(i5)')+'MB')
             

;
    	    moviev.cmbbase=cmbbase
    	    moviev.cmbsliden=cmbsliden
    	    moviev.cmbvals=cmbvals
    	    moviev.cropinst=cropinst             
            moviev.showcmb=1
 ;           moviev.bkgval=bkgval
            moviev.brclr=brclr

           moviev=add_tag(moviev,bkgval,'bkgval')         
           moviev=add_tag(moviev,cmbfield,'cmbfield')
           moviev=add_tag(moviev,cropsz,'cropsz')
           moviev=add_tag(moviev,mc3sv,'mc3sv')

	 WIDGET_CONTROL, cmbbase, MAP=1 
         WIDGET_CONTROL, /REAL, cmbbase

END



PRO Wcs_combine_MVI_EVENT, event

  common wcs_combine
  COMMON WSCC_MKMOVIE_COMMON

  WIDGET_CONTROL, event.id, GET_UVALUE=uval
  ;WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE (values
                                                  ; are not retained. use common).

  CASE (uval) OF

    'QUIT': BEGIN
              WIDGET_CONTROL, /DESTROY, struct.base
            END
    'PROCEED': BEGIN
              WIDGET_CONTROL, /DESTROY, struct.base
              mvis=[struct.mvi0,struct.mvi1,struct.mvi2,struct.mvi3,struct.mvi4,struct.mvi5,struct.mvi6,struct.mvi7,struct.mvi8,struct.mvi9]
              mvt=where(mvis ne '')
	      if mvt(0) ne -1 then begin
	        mvis=mvis(mvt)
                get_cs_combine_mvi,mvis
	        wcs_combine_movies,truecolor=struct.mcolor,bpos=struct.bpos,times=struct.times,/preview
              return
              endif else print,'No movies selected exiting'
              return
            END
    'CLEAR': BEGIN
             struct.mvi0= '' 
             WIDGET_CONTROL,struct.MV0, SET_VALUE= struct.mvi0
             struct.mvi1= '' 
             WIDGET_CONTROL,struct.MV1, SET_VALUE= struct.mvi1
             struct.mvi2= '' 
             WIDGET_CONTROL,struct.MV2, SET_VALUE= struct.mvi2
             struct.mvi3= '' 
             WIDGET_CONTROL,struct.MV3, SET_VALUE= struct.mvi3
             struct.mvi4= '' 
             WIDGET_CONTROL,struct.MV4, SET_VALUE= struct.mvi4
             struct.mvi5= '' 
             WIDGET_CONTROL,struct.MV5, SET_VALUE= struct.mvi5
             struct.mvi6= '' 
             WIDGET_CONTROL,struct.MV6, SET_VALUE= struct.mvi6
             struct.mvi7= '' 
             WIDGET_CONTROL,struct.MV7, SET_VALUE= struct.mvi7
             struct.mvi8= '' 
             WIDGET_CONTROL,struct.MV8, SET_VALUE= struct.mvi8
             struct.mvi9= '' 
             WIDGET_CONTROL,struct.MV9, SET_VALUE= struct.mvi9
            END
    'mcolor' : struct.mcolor= event.index
    'bpos' : struct.bpos= event.index

    'times' : struct.times= event.index

    'MV0': BEGIN
             WIDGET_CONTROL,struct.MV0, GET_VALUE= mname
             struct.mvi0= STRTRIM(mname,2) 
          END
    'MV1': BEGIN
             WIDGET_CONTROL,struct.MV1, GET_VALUE= mname
             struct.mvi1= STRTRIM(mname,2) 
          END
    'MV2': BEGIN
             WIDGET_CONTROL,struct.MV2, GET_VALUE= mname
             struct.mvi2= STRTRIM(mname,2) 
          END
    'MV3': BEGIN
             WIDGET_CONTROL,struct.MV3, GET_VALUE= mname
             struct.mvi3= STRTRIM(mname,2) 
          END

    'MV4': BEGIN
             WIDGET_CONTROL,struct.MV4, GET_VALUE= mname
             struct.mvi4= STRTRIM(mname,2) 
          END
    'MV5': BEGIN
             WIDGET_CONTROL,struct.MV5, GET_VALUE= mname
             struct.mvi5= STRTRIM(mname,2) 
          END
    'MV6': BEGIN
             WIDGET_CONTROL,struct.MV6, GET_VALUE= mname
             struct.mvi6= STRTRIM(mname,2) 
          END
    'MV7': BEGIN
             WIDGET_CONTROL,struct.MV7, GET_VALUE= mname
             struct.mvi7= STRTRIM(mname,2) 
          END
    'MV8': BEGIN
             WIDGET_CONTROL,struct.MV8, GET_VALUE= mname
             struct.mvi8= STRTRIM(mname,2) 
          END

    'MV9': BEGIN
             WIDGET_CONTROL,struct.MV9, GET_VALUE= mname
             struct.mvi9= STRTRIM(mname,2) 
          END


    'smv0': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi0= file 
             WIDGET_CONTROL,struct.MV0, SET_VALUE= struct.mvi0
          END
    'smv1': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi1= file 
             WIDGET_CONTROL,struct.MV1, SET_VALUE= struct.mvi1
           END
    'smv2': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi2= file 
             WIDGET_CONTROL,struct.MV2, SET_VALUE= struct.mvi2
           END
    'smv3': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi3= file 
             WIDGET_CONTROL,struct.MV3, SET_VALUE= struct.mvi3
           END
    'smv4': BEGIN
             file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi4= file 
             WIDGET_CONTROL,struct.MV4, SET_VALUE= struct.mvi4
          END
    'smv5': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi5= file 
             WIDGET_CONTROL,struct.MV5, SET_VALUE= struct.mvi5
          END
    'smv6': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi6= file 
             WIDGET_CONTROL,struct.MV6, SET_VALUE= struct.mvi6
           END
    'smv7': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi7= file 
             WIDGET_CONTROL,struct.MV7, SET_VALUE= struct.mvi7
           END
    'smv8': BEGIN
            file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi8= file 
             WIDGET_CONTROL,struct.MV8, SET_VALUE= struct.mvi8
           END
    'smv9': BEGIN
             file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path,PATH=struct.mvipath)
             struct.mvi9= file 
             WIDGET_CONTROL,struct.MV9, SET_VALUE= struct.mvi9
          END

  ENDCASE
  if datatype(path) ne 'UND' then struct.mvipath = path
 
  RETURN
END


PRO Wcs_combine_MVI,mvifiles,TRUECOLOR=truecolor, BSIDE=bside, AUTO=auto, DEBUG=debug, _EXTRA=_extra
  common wcs_combine
  COMMON WSCC_MKMOVIE_COMMON
;  undefine,textframe
undefine,cmbstate
IF keyword_set(DEBUG) THEN debug_on=1 ELSE debug_on=0
CD, CURRENT = mvipath

           mvi0=''
           mvi1=''
           mvi2=''
           mvi3=''
           mvi4=''
           mvi5=''
           mvi6=''
           mvi7=''
           mvi8=''
           mvi9=''
           if keyword_set(truecolor) then mcolor=1 else mcolor=0
           times=1
           bpos=0

  if datatype(mvifiles) ne 'UND' then begin
           if (n_elements(mvifiles) ge 1) then mvi0=mvifiles(0)
           if (n_elements(mvifiles) ge 2) then mvi1=mvifiles(1)
           if (n_elements(mvifiles) ge 3) then mvi2=mvifiles(2)
           if (n_elements(mvifiles) ge 4) then mvi3=mvifiles(3)
           if (n_elements(mvifiles) ge 5) then mvi4=mvifiles(4)
           if (n_elements(mvifiles) ge 6) then mvi5=mvifiles(5)
           if (n_elements(mvifiles) ge 7) then mvi6=mvifiles(6)
           if (n_elements(mvifiles) ge 8) then mvi7=mvifiles(7)
           if (n_elements(mvifiles) ge 9) then mvi8=mvifiles(8)
           if (n_elements(mvifiles) ge 10) then mvi9=mvifiles(9)
           if keyword_set(truecolor) then mcolor=1 else mcolor=0
  endif
  if datatype(struct) eq 'STC' and datatype(mvifiles) eq 'UND' then begin
           mvi0=struct.mvi0
           mvi1=struct.mvi1
           mvi2=struct.mvi2
           mvi3=struct.mvi3
           mvi4=struct.mvi4
           mvi5=struct.mvi5
           mvi6=struct.mvi6
           mvi7=struct.mvi7
           mvi8=struct.mvi8
           mvi9=struct.mvi9
           mcolor=struct.mcolor
           bpos=struct.bpos
           times=struct.times
  endif 

  base= WIDGET_BASE(title='SECCHI Combine Movie Tool',/COLUMN,/FRAME, XOFFSET= 1, YOFFSET=0)

  c3= WIDGET_BASE(base,/colum)
  mlab1= WIDGET_LABEL(c3,VALUE='Movies to combine')

  r0=WIDGET_BASE(c3,/row)
  mv0= CW_FIELD(r0, VALUE=MV0, /ALL_EVENTS, UVALUE='MV0' , XSIZE=70, YSIZE=1, title='MVI0 : ')
  smv0= WIDGET_BUTTON(r0, VALUE='Select', UVALUE='smv0') 
  WIDGET_CONTROL,mv0, SET_VALUE= mvi0

  mv5= CW_FIELD(r0, VALUE=MV0, /ALL_EVENTS, UVALUE='MV5' , XSIZE=70, YSIZE=1, title='MVI5 : ')
  smv5= WIDGET_BUTTON(r0, VALUE='Select', UVALUE='smv5') 
  WIDGET_CONTROL,mv5, SET_VALUE= mvi5

  r1=WIDGET_BASE(c3,/row)
  mv1= CW_FIELD(r1, VALUE=MV1, /ALL_EVENTS, UVALUE='MV1' , XSIZE=70, YSIZE=1, title='MVI1 : ')
  smv1= WIDGET_BUTTON(r1, VALUE='Select', UVALUE='smv1') 
  WIDGET_CONTROL,mv1, SET_VALUE= mvi1
  mv6= CW_FIELD(r1, VALUE=MV0, /ALL_EVENTS, UVALUE='MV6' , XSIZE=70, YSIZE=1, title='MVI6 : ')
  smv6= WIDGET_BUTTON(r1, VALUE='Select', UVALUE='smv6') 
  WIDGET_CONTROL,mv6, SET_VALUE= mvi6

  r2=WIDGET_BASE(c3,/row)
  mv2= CW_FIELD(r2, VALUE=MV2, /ALL_EVENTS, UVALUE='MV2' , XSIZE=70, YSIZE=1, title='MVI2 : ') 
  smv2= WIDGET_BUTTON(r2, VALUE='Select', UVALUE='smv2') 
  WIDGET_CONTROL,mv2, SET_VALUE= mvi2
  mv7= CW_FIELD(r2, VALUE=MV0, /ALL_EVENTS, UVALUE='MV7' , XSIZE=70, YSIZE=1, title='MVI7 : ')
  smv7= WIDGET_BUTTON(r2, VALUE='Select', UVALUE='smv7') 
  WIDGET_CONTROL,mv7, SET_VALUE= mvi7

  r3=WIDGET_BASE(c3,/row)
  mv3= CW_FIELD(r3, VALUE=MV3, /ALL_EVENTS, UVALUE='MV3' , XSIZE=70, YSIZE=1, title='MVI3 : ') 
  smv3= WIDGET_BUTTON(r3, VALUE='Select', UVALUE='smv3') 
  WIDGET_CONTROL,mv3, SET_VALUE= mvi3
  mv8= CW_FIELD(r3, VALUE=MV0, /ALL_EVENTS, UVALUE='MV8' , XSIZE=70, YSIZE=1, title='MVI8 : ')
  smv8= WIDGET_BUTTON(r3, VALUE='Select', UVALUE='smv8') 
  WIDGET_CONTROL,mv8, SET_VALUE= mvi8

  r4=WIDGET_BASE(c3,/row)
  mv4= CW_FIELD(r4, VALUE=MV4, /ALL_EVENTS, UVALUE='MV4' , XSIZE=70, YSIZE=1, title='MVI4 : ') 
  smv4= WIDGET_BUTTON(r4, VALUE='Select', UVALUE='smv4')
  WIDGET_CONTROL,mv4, SET_VALUE= mvi4
  mv9= CW_FIELD(r4, VALUE=MV0, /ALL_EVENTS, UVALUE='MV9' , XSIZE=70, YSIZE=1, title='MVI9 : ')
  smv9= WIDGET_BUTTON(r4, VALUE='Select', UVALUE='smv9') 
  WIDGET_CONTROL,mv9, SET_VALUE= mvi9

  r5=WIDGET_BASE(c3,/row)
 proceed= WIDGET_BUTTON(r5, VALUE='Proceed', UVALUE='PROCEED')
 clear= WIDGET_BUTTON(r5, VALUE='clear', UVALUE='CLEAR')
 quit= WIDGET_BUTTON(r5, VALUE='Quit', UVALUE='QUIT')

  txt=widget_label (r5,  value='Color :')
  tru=['Grayscale','Truecolor']
  truv= WIDGET_COMBOBOX(r5, VALUE=tru, UVALUE="mcolor")
  WIDGET_CONTROL, truv, SET_COMBOBOX_SELECT= mcolor
  WIDGET_CONTROL, truv, SENSITIVE=1

  txt=widget_label (r5,  value='B position :')
  bpost=['Same sun center','Left side','Right side']
  bposv= WIDGET_COMBOBOX(r5, VALUE=bpost, UVALUE="bpos")
  IF keyword_set(BSIDE) THEN bpos=bside-1
  WIDGET_CONTROL, bposv, SET_COMBOBOX_SELECT= bpos
  WIDGET_CONTROL, bposv, SENSITIVE=1

  txt=widget_label (r5,  value='Time Stamps: ')
  timet=['off','on']
  timev= WIDGET_COMBOBOX(r5, VALUE=timet, UVALUE="times")
  WIDGET_CONTROL, timev, SET_COMBOBOX_SELECT= times
  WIDGET_CONTROL, timev, SENSITIVE=1

  WIDGET_CONTROL, base, /REALIZE

  struct= {base: base,             $
           quit: quit,             $
           proceed: proceed,       $
           clear: clear,       $
           truv: truv,       $
           mcolor: mcolor,       $
           bposv: bposv,       $
           bpos: bpos,       $
           timev: timev,       $
           times: times,       $
           mvi0: mvi0,       $
           mvi1: mvi1,       $
           mvi2: mvi2,       $
           mvi3: mvi3,       $
           mvi4: mvi4,       $
           smv0: smv0,       $
           smv1: smv1,       $
           smv2: smv2,       $
           smv3: smv3,       $
           smv4: smv4,       $
           mv0: mv0,       $
           mv1: mv1,       $
           mv2: mv2,       $
           mv3: mv3,       $
           mv4: mv4,	   $
           mvi5: mvi5,       $
           mvi6: mvi6,       $
           mvi7: mvi7,       $
           mvi8: mvi8,       $
           mvi9: mvi9,       $
           smv5: smv5,       $
           smv6: smv6,       $
           smv7: smv7,       $
           smv8: smv8,       $
           smv9: smv9,       $
           mv5: mv5,       $
           mv6: mv6,       $
           mv7: mv7,       $
           mv8: mv8,       $
           mv9: mv9,       $
           mvipath:mvipath }

   IF KEYWORD_SET(AUTO) THEN BEGIN
    	WIDGET_CONTROL, /DESTROY, struct.base
    	mvis=[struct.mvi0,struct.mvi1,struct.mvi2,struct.mvi3,struct.mvi4,struct.mvi5,struct.mvi6,struct.mvi7,struct.mvi8,struct.mvi9]
    	mvt=where(mvis ne '')
    	if mvt[0] ne -1 then begin
	        mvis=mvis(mvt)
                get_cs_combine_mvi,mvis
		help,bpos
	        wcs_combine_movies,truecolor=struct.mcolor,bpos=struct.bpos,times=struct.times, _EXTRA=_extra ;,/preview
    	endif
   ENDIF ELSE BEGIN

    	WIDGET_CONTROL, base, SET_UVALUE= struct 

    	XMANAGER, 'Wcs_combine_MVI', base
	
   ENDELSE
 RETURN
END

