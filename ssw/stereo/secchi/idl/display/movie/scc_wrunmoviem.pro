;+
;$Id: scc_wrunmoviem.pro,v 1.122 2017/01/03 14:33:49 mcnutt Exp $
;
; Project     : SECCHI and SOHO - LASCO/EIT (dual-use)
;                   
; Name        : SCC_WRUNMOVIEM
;               
; Purpose     : Widget tool to display animation sequence.
;               
; Explanation : This tool allows the user to view a series of images as
;		an animation sequence.  The user can control the direction,
;		speed, and number of frames with widget controls.
;               
; Use         : IDL> SCC_WRUNMOVIEM [, arg1 [, NAMES=names [,/PREVIOUS]]]
;
;		Without any inputs, program will prompt user to select an existing .mvi file.
;    Example  : IDL> SCC_WRUNMOVIEM
;
; Optional Input:              Or you could have one argument, the .mvi file you want to load.
;    Example  : IDL> SCC_WRUNMOVIEM, 'mymovie.mvi'
;
;               Or if you have pre-loaded images into pixmaps (like MKMOVIEM.PRO does) call:
;		Where win_index is an array of the window numbers and names is optionally
;		a STRARR() containing names of each frame.
;    Example  : IDL> SCC_WRUNMOVIEM, win_index, NAMES=names
; 
; Optional Keywords:
;   START=n  	Make frame n first frame of movie (starting at n=0)
;   LENGTH= 	Number of frames to read in from movie
;   AVERAGE_FRAMES= Number of frames to average together using rebin; implies /EDIT
;   SMOOTH_BOX=     Smooth each frame with given bin size; implies /EDIT
;   /USE_R  	Display units of solar radius for distance from Sun center (default for COR, EUV outside of 1 Rsun)
;   /USE_E  	Display units of degrees (Elongation) for distance from Sun (default for HI)
;   /USE_STPLN	Display angle relative to perpendicular to epipolar plane (STEREO mission plane)
;   /PRECOMMCORRECT 	Calls cor2_Point.pro for COR2 only (not necessary for movies made after 2008-03-28)   
;   NAMES   = title for each frame
;   HDRS    = image or frame header structures
;   SUNXCEN=,SUNYCEN=	array coordinate of sun center
;   SEC_PIX=	arcsec/pixel
;   /PREVIOUS	    Play last loaded movie
;   /ROLL_PER_FRAME 	Compute roll for each frame (LASCO/EIT only)
;   /EDIT   	Allow color table adjustment (slower)
;   /DEBUG  	Print debug messages
;   /LOWERXY	Move the x,y coordinate display down 
;   /LOWERDN	Move the DN display down
;   /CROSSHAIR_CURSOR	Use crosshair for cursor instead of arrow
;   /BIGFONT	Use Triplex Roman size=2 for annotation instead of default hardware font
;   /CARRINGTON Display location as carrington lat and long on solar disk (default for EUVI and EIT)
;
;		If after exiting SCC_WRUNMOVIEM you want to re-load the movie call:
;    Example  : IDL> SCC_WRUNMOVIEM, /PREVIOUS
;    
; Calls       : 
;
; Comments    : Use MKMOVIEM.PRO to load into pixmaps.
;               
; Side effects: If /USE_R and HI, will need to read in FITS headers
;               
; Category    : Image Display.  Animation.
;               
; Written     : Scott Paswaters, NRL Feb. 13 1996.
;               
; Modified    : SEP  29 May 1996 - Changed to multiple pixmaps for images.
;				   Added buttons to save and load movie files (.mvi).
;				   Seperated control buttons from display window.
;               SEP   9 Jul 1996 - Added keyword to pass image headers.
;
;		SHH  12 Jul 1996 - Enabled display of cursor position in draw window
;			 	   Added height-time plotting capability
;				   Added Edit Frame option	  
;				   Modified effects of mouse buttons
;				   Added buttons above draw window
;				   Modified display of Control window
;
;               SEP  29 Sep 1996 - Added routines from DAB for applying C3 geometric distortion and
;                                  calculating solar radius as function of time.
;               SEP  01 Oct 1996 - Added call to C3_DISTORTION for applying C3 geometric distortion
;
;               RAH  01 Nov 1996 - Prior to PLOT_HT call, don't ask for filename
;
;		SHH  09 Jan 1997 - Added "Start H-T" button
;				   Calls XEDITFRAME
;               SEP  05 Feb 1997 - Mods for mvi version 1 format.
;               SEP  21 Mar 1997 - use sun center and arc_sec/pixel if saved in .mvi file.
;               SEP  16 May 1997 - added DN output to window, added button to update center.
;               SEP  23 Sep 1997 - added active slider widget for current frame.
;		NBR  06 Jan 1999 - changed sec_pix check in file_hdr
;		DW   11 Jan 1999 - added C2_DISTORTION and roll angle
;		NBR  02 Mar 1999 - eliminated sec_pix check for getting sun center 
;		NBR 09 Jul 1999  - Add warning if frame headers not saved
;		NBR, 05 Mar 2002 - Only compute roll angle once per day; use AVG instead of STAR for roll; extend common block
;		NBR, 24 Sep 2003 - Add mvi header roll correction (rect) in moviev; print mvi header; save MVIs with rect
;		NBR, 29 Sep 2003 - Add RECTIFIED keyword for call from WRUNMOVIE
;		NBR, 20 Oct 2003 - Allow case where xcen is REALLY zero
;		KB,  Dec 15,2003 - Added slider so full-res images can be used with smaller screens
;               KB,  Sep07, 2004 - When displaying Pos. Ang, if nominal_roll_attitude.dat can't be found, use default values
;               AEE, Jan25, 2005 - Generate roll angles (using new database) when reading in frames
;                                  and keep around to use later when going back and forth between
;                                  frames (to make it quicker). The get_roll_or_xy is called for 
;                                  first frame of the movie and also fo multi-day movies when a day 
;                                  boundry is crossed. I added keyword ROLL_PER_FRAME to calculate
;                                  a roll for each frame if present. Otherwise, default is to calculate
;                                  one roll per day instead of one roll per frame. (rev. 1.29)
;               AEE, Jan27, 2005 - Calculate one roll per day if scc_wrunmoviem is called from within scc_wrunmovie (since 
;                                  movie frames are already readin without calculating rolls in scc_wrunmovie). (rev. 1.30) 
;               AEE, Jan31, 2005 - set roll to zero when image header does not have valid date/time. (rev. 1.31)
;
;		D 1.32  06/06/15 13:43:44 esfand        32 31   00016/00000/00877
;		handle SECCHI headers -- this is the revision in LASCO library
;
;               AEE, Oct04, 2006 -   Added code to handle SECCHI headers. (rev. 1.33)  
;
;   	    	#### sccs Rev. 1.33 is identical to cvs Rev. 1.1 except for all ####
;		#### instances of wrunmoviem are replaced with scc_wrunmoviem   ####
;
; $Log: scc_wrunmoviem.pro,v $
; Revision 1.122  2017/01/03 14:33:49  mcnutt
; Use select_windows instead of set_plot
;
; Revision 1.121  2011/05/18 22:25:10  nathan
; obsolete message and return
;
; Revision 1.120  2009/10/06 12:44:11  mcnutt
; corrected zoom mouse coords
;
; Revision 1.119  2009/10/02 12:49:27  mcnutt
; replaced lost crop function
;
; Revision 1.118  2009/10/02 12:42:50  mcnutt
; replaced lost crop function
;
; Revision 1.117  2009/10/01 16:39:44  mcnutt
; added track spot feature
;
; Revision 1.116  2009/09/24 19:29:11  mcnutt
; update Common block WSCC_MKMOVIE_COMMON to include textframe used in annotate_image
;
; Revision 1.115  2009/09/24 18:47:37  mcnutt
; change annotate to use annotate_image
;
; Revision 1.114  2009/09/10 12:14:09  mcnutt
; added popup message when fits files are not located
;
; Revision 1.113  2009/09/08 16:33:27  mcnutt
; does not clear projection for deprojected hi movies
;
; Revision 1.112  2009/09/04 13:35:52  mcnutt
; added corrections to deproject wcshdrs
;
; Revision 1.111  2009/08/28 20:13:25  nathan
; further adjustments to position output format
;
; Revision 1.110  2009/08/26 17:51:47  mcnutt
; changed coords print formats
;
; Revision 1.109  2009/08/20 12:20:03  mcnutt
; corrected WCSHDRS setting if read in movie is deprojected
;
; Revision 1.108  2009/08/14 20:43:52  sheeley
; nr - fixed ahdr assignment from hdrs0 if frame(s) skipped
;
; Revision 1.107  2009/08/05 16:18:19  mcnutt
; changed formats for different elong values to handle Heliographic Cartesian
;
; Revision 1.106  2009/07/31 19:31:39  nathan
; if error in MVI header cdelt, prompt for correct value
;
; Revision 1.105  2009/07/31 19:30:57  nathan
; make sure arcsc is positive
;
; Revision 1.104  2009/07/30 13:50:04  mcnutt
; corrected zoom frames
;
; Revision 1.103  2009/07/28 16:14:59  nathan
; Define arcsc from wcshdr if available; correct elongation for cor2_distortion;
; elongation stored as degrees not Rsun for SOHO; re-write scc_wrunmovie_drawm
; output formatting
;
; Revision 1.102  2009/07/28 14:51:09  cooper
; corrected sync to account for movies with a roll
;
; Revision 1.101  2009/07/24 20:57:53  cooper
; Fixed Carrington coordinates
;
; Revision 1.100  2009/07/23 20:06:28  cooper
; altered sync to account for hi_fix_pointing and added sync color reverse
;
; Revision 1.99  2009/07/21 19:46:38  nathan
; Put all of the coord conversion stuff in one place so it is easier to
; keep track of; functionality should be unchanged.
;
; Revision 1.98  2009/07/21 14:22:34  cooper
; correct angle measurements
;
; Revision 1.97  2009/07/20 22:10:54  nathan
; adjust widget labels
;
; Revision 1.96  2009/07/20 21:37:47  cooper
; removed extra conversion factor from Rsun to degrees
;
; Revision 1.95  2009/07/17 18:54:16  nathan
; use /NOSHELL with spawn command for Windows compatability
;
; Revision 1.94  2009/07/17 13:30:12  mcnutt
; corrected heliographic cartesian coordinates
;
; Revision 1.93  2009/07/14 21:59:44  nathan
; added hi_fix_pointing call
;
; Revision 1.92  2009/07/14 18:46:22  cooper
; completed sync with tool2a
;
; Revision 1.91  2009/07/13 19:26:53  cooper
; fixed error in setting movie to correct frame in sync with jmap, still inaccurate for hi2
;
; Revision 1.90  2009/07/13 19:19:52  cooper
; added capability to sync with jmaps tool2a
;
; Revision 1.88  2009/06/25 17:32:39  mcnutt
; changed call to scc_movieout
;
; Revision 1.87  2009/04/27 16:05:55  mcnutt
; send pfxs to sccfill_ht to indicate coordinate system
;
; Revision 1.86  2009/04/14 15:56:55  nathan
; finish fixing previous bug
;
; Revision 1.85  2009/04/14 15:50:21  mcnutt
; corrected average frames
;
; Revision 1.84  2009/04/13 22:16:52  nathan
; remove redundant common block definition
;
; Revision 1.83  2009/04/10 19:04:00  mcnutt
; returns draw grid if imgs are not defined
;
; Revision 1.82  2009/04/10 18:26:34  mcnutt
; does not set filename to default if movie is not read in
;
; Revision 1.81  2009/04/10 15:38:51  mcnutt
; added define imgs buttom and calls scc_movieout to save movies
;
; Revision 1.80  2009/04/06 18:39:44  mcnutt
; writes and reads in both gif and png movies files
;
; Revision 1.79  2009/04/02 18:11:51  mcnutt
; updated call to pngmvi_select
;
; Revision 1.78  2009/04/01 15:15:53  nathan
; tweak output option list, and update frame print procedure
;
; Revision 1.77  2009/03/20 13:34:39  mcnutt
; Does not write png movie is pngmvi_select is canceled
;
; Revision 1.76  2009/03/13 19:06:39  mcnutt
; added png.hdr to filter for pickfile
;
; Revision 1.75  2009/03/12 17:54:38  mcnutt
; call matchfitshdr2mvi for sun center if soho only
;
; Revision 1.74  2009/03/11 18:07:48  mcnutt
; change png movie to save img file names without dir
;
; Revision 1.73  2009/03/10 18:53:13  mcnutt
; works with a single image movie
;
; Revision 1.72  2009/03/05 12:35:17  mcnutt
; works with scc_annotate arrow
;
; Revision 1.71  2009/02/27 15:40:01  mcnutt
; add call to scc_annotate if in edit mode
;
; Revision 1.70  2009/02/25 16:15:31  nathan
; added cor1,cor2 to subfield check
;
; Revision 1.69  2009/02/20 16:07:31  mcnutt
; works with TB cor2 and precommcorrect keyword
;
; Revision 1.68  2009/02/18 22:53:42  nathan
; prompt for confirmation if subfield is detected
;
; Revision 1.67  2009/02/12 19:32:48  mcnutt
; calls matchfitshdr2mvi before scc_sun_center if not ahdr.roll
;
; Revision 1.66  2009/02/05 15:44:52  mcnutt
; changes mvi file name when cropped
;
; Revision 1.65  2009/02/03 19:59:50  mcnutt
; add crop option to menu
;
; Revision 1.64  2009/01/14 14:10:10  mcnutt
; works when called from scc_wrunmovie
;
; Revision 1.63  2009/01/12 17:40:06  nathan
; fixed incorrect header assignment for AVERAGE_FRAMES
;
; Revision 1.62  2009/01/09 15:30:50  mcnutt
; rearrange top of widget to allow 1024 full display if sreen is large enough
;
; Revision 1.61  2008/12/19 23:04:19  nathan
; allow changing size of output of plane_project; set charsize and info msg for
; drawcoordgrid; add START and LENGTH keywords
;
; Revision 1.60  2008/12/19 13:10:48  mcnutt
; now works with most LASCO and EIT movies
;
; Revision 1.59  2008/11/14 14:41:11  mcnutt
; added draw grid and deproject for hi images, rearanged control window
;
; Revision 1.58  2008/11/03 12:27:19  mcnutt
; corrected ccd_pos check for hi movies
;
; Revision 1.57  2008/10/28 17:54:49  nathan
; do not redefine ccd_pos for HI
;
; Revision 1.56  2008/09/25 15:19:26  nathan
; imply /EDIT with AVERAGE_FRAMES or SMOOTH_BOX
;
; Revision 1.55  2008/09/24 19:30:18  nathan
; change bufim to float before rebin
;
; Revision 1.54  2008/09/24 19:10:36  nathan
; updated header
;
; Revision 1.53  2008/09/24 17:59:27  mcnutt
; added keyword SMOOTH_BOX to smooth movies frames when movie is read in
;
; Revision 1.52  2008/09/24 17:41:28  mcnutt
; added keyword AVERAGE_FRAMES to rebin several frames into 1 when movie is read in
;
; Revision 1.51  2008/08/28 15:29:20  nathan
; left stop in
;
; Revision 1.50  2008/08/28 15:14:41  nathan
; when reading in mvi, skip frames where time goes backwards using findx
;
; Revision 1.49  2008/08/26 18:20:30  mcnutt
; saves movie using first and last frame values
;
; Revision 1.48  2008/08/20 13:28:38  mcnutt
; corrected error in save and removed zbuffer
;
; Revision 1.47  2008/08/20 12:24:22  mcnutt
; loads images from movie when zoom is seleceted if not started with eidtk
;
; Revision 1.46  2008/08/15 17:07:50  nathan
; Do not allow Zoom without /EDIT keyword; add inpfilename to common swin_indexm
;
; Revision 1.45  2008/08/13 18:21:29  mcnutt
; read in a truecolor movie, will not work with editk keyword arrays need to be resized
;
; Revision 1.44  2008/08/08 20:38:56  nathan
; Added message for deleted frame; disable SAVE if /EDIT not set
;
; Revision 1.43  2008/07/23 13:04:41  mcnutt
; zoom window size now selected by clicking on movie draw window
;
; Revision 1.42  2008/07/22 16:51:14  mcnutt
; added scale adjustment to zoom
;
; Revision 1.41  2008/07/22 15:30:08  mcnutt
; animated zoom window
;
; Revision 1.40  2008/07/17 17:40:54  nathan
; for printing frame, use /portrait
;
; Revision 1.39  2008/07/11 17:47:53  mcnutt
; added movie header to sccwrite_ht call
;
; Revision 1.38  2008/07/11 13:09:47  mcnutt
; added ZOOM feature
;
; Revision 1.37  2008/07/09 14:21:42  mcnutt
; added display values above movie
;
; Revision 1.36  2008/07/08 18:30:00  mcnutt
; Allow switching between different units on screen with drop-down menu
;
; Revision 1.35  2008/07/08 12:53:32  mcnutt
; Prints and marks HT values to screen regardless of whether h-t file is started or not
;
; Revision 1.34  2008/06/26 18:46:13  mcnutt
; corrected order of objects
;
; Revision 1.33  2008/06/26 18:14:51  nathan
; use hpc2hpr.pro to get radial coords for HI and cor2 with /precomm (corrects near-sun
; approximation error of up to 16 deg in position angle in HI2 with previous versions)
;
; Revision 1.32  2008/06/25 18:48:39  mcnutt
; redfines ccd_pos for osc wcshdrs
;
; Revision 1.31  2008/06/25 18:36:54  mcnutt
; removed stop
;
; Revision 1.30  2008/06/25 18:31:03  mcnutt
; calculates the positions of earth soho and other stereo sc
;
; Revision 1.29  2008/06/04 14:22:04  mcnutt
; added text box to mark ht feature
;
; Revision 1.28  2008/05/20 19:14:58  nathan
; extend WSCC_MKMOVIE_COMMON
;
; Revision 1.27  2008/05/16 18:30:56  mcnutt
; uses zbuffer when saving movies and blocks edit frame if keyword editk is not set
;
; Revision 1.26  2008/05/14 15:30:20  mcnutt
; works with input from scc_wrunmovie when called from wscc_mkmovie
;
; Revision 1.25  2008/05/09 18:44:57  mcnutt
; corrected aorb strmid call
;
; Revision 1.24  2008/05/09 16:09:46  mcnutt
; changed menu window location for 1024 and larger change quit buttom to close menu
;
; Revision 1.23  2008/05/08 13:56:37  mcnutt
; now opens menu window under movie window
;
; Revision 1.22  2008/05/07 22:54:40  nathan
; implement matchfitshdr2mvi in scc_wrunmoviem
;
; Revision 1.21  2008/05/07 16:42:56  nathan
; fixed typo that caused compile error
;
; Revision 1.20  2008/05/06 20:47:00  nathan
; implement /USE_E and /USE_STPLN
;
; Revision 1.19  2008/05/06 18:20:33  nathan
; fixed incorrect units for cor2 /precommcorrect
;
; Revision 1.18  2008/05/05 18:25:40  mcnutt
; send elong to sccwrite_ht
;
; Revision 1.17  2008/05/05 14:48:55  mcnutt
; modifies to work with widget sccwrite_ht.pro scc_wrunmovie.pro
;
; Revision 1.16  2008/05/02 22:24:19  nathan
; fixed /USE_R for euvi case
;
; Revision 1.15  2008/05/02 20:37:07  nathan
; Implemented rolliszero for cor2
;
; Revision 1.14  2008/05/02 20:07:58  nathan
; Added /PRECOMMCORRECT option which calls cor2_point.pro; will also plot
; differences in sun center and roll when used with /DEBUG
;
; Revision 1.13  2008/04/10 21:41:46  nathan
; allow xcolors.pro new output
;
; Revision 1.12  2008/03/19 19:56:33  nathan
; Display carrington lon/lat for EUVI frames
;
; Revision 1.11  2008/03/19 16:36:44  nathan
; use ? in fname for getting wcs headers; move device cleanup from end of pro to quit button
;
; Revision 1.10  2008/03/17 22:15:14  nathan
; Delete extra window from device,/cursor command; make /ARROW_CURSOR the default
;
; Revision 1.9  2008/03/17 21:56:33  nathan
; fixed bug for LASCO mvis
;
; Revision 1.8  2008/02/07 21:12:18  nathan
; display distance to sun as elongation if HI and not /USE_R
;
; Revision 1.7  2008/02/06 22:47:15  nathan
; Load wcs headers from original FITS for spherical projection correction, HI only
;
; Revision 1.6  2008/01/31 18:13:48  nathan
; use same method for saving as in scc_wrunmovie
;
; Revision 1.5  2008/01/14 19:13:19  nathan
; make sure pros and common blocks have unique names to not conflict with lasco
;
; Revision 1.4  2008/01/11 16:33:31  nathan
; Permanently moved from dev/movie to secchi/idl/display/movie
;
; Revision 1.15  2008/01/09 21:27:31  nathan
; updates to display parameters, debugging
;
; Revision 1.14  2007/12/11 19:48:22  nathan
; black background for annotations, and /BIGFONT option
;
; Revision 1.13  2007/12/10 22:20:00  nathan
; change default location of DN value to not conflict with new tel position; display R to 2 decimals
;
; Revision 1.12  2007/11/16 15:59:21  nathan
; Removed call to getsccsecpix.pro; added /LOWERXY and /RAISEDN and
; /ARROW_CURSOR; updated Keyword documentation
;
; Revision 1.11  2007/11/15 19:07:02  nathan
; calls sccwrite_ht; allow call from scc_wrunmovie
;
; Revision 1.10  2007/10/29 19:00:55  nathan
; added smarts to fix bad CDELT in MVI header
;
; Revision 1.9  2007/10/19 14:54:16  nathan
; Cleaned up a lot of spaghetti code; call sxplot_ht; print MVI header version
;
; Revision 1.8  2007/10/17 21:19:26  nathan
; Add /DEBUG; always roll in frame header if it is there; use cdelt1 from
; frame header if arcs from file header is lt 0
;
; Revision 1.7  2007/08/17 21:09:12  sheeley
; changed font color
;
; Revision 1.6  2007/08/17 20:44:53  nathan
; allow mvihdr.roll if called from wrunmovie
;
; Revision 1.5  2007/08/17 16:06:02  herbst
; Mods for N.Sheeley Summer 2007
;
; Revision 1.4  2007/08/17 15:57:51  herbst
; Mods for N.Sheeley Summer 2007
;
; Revision 1.3  2007/06/05 20:06:00  nathan
; more sccs revision clarification
;
; Revision 1.2  2007/05/29 22:24:12  nathan
; clarify sccs tag line
;
; Revision 1.1  2006/10/10 20:29:02  esfand
; for dual SECCHI/LASCO use
;
;
; See Also    : MKMOVIEM.PRO
;
;-            

PRO sccSHOW_MENU, SHOW=show

COMMON sWIN_INDEXM, win_index2, hdrs2, base, lastdate, deltaroll, bmin, bmax, pan, edit, debugon, inpfilename, lowerdnval, $
		    lowerxyval, usebigfont, radec, lasttime, wcshdrs, elong, systemstr, nhtfeat, featwin, zstate, textbase, sync_point
COMMON sALL_ROLLS, rolls, rolls2
COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe


   WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

   IF KEYWORD_SET(show) THEN moviev.showmenu=0

   IF (moviev.showmenu EQ 0) THEN BEGIN
      WIDGET_CONTROL, moviev.base, MAP=1
      moviev.showmenu = 1
   ENDIF ELSE BEGIN
      WIDGET_CONTROL, moviev.base, MAP=0
      moviev.showmenu = 0
   ENDELSE
  WIDGET_CONTROL, base, SET_UVALUE=moviev;  ,yoffset=0,xoffset=tmp(0)-tmp(0)/2.0
  RETURN
END
;
;----------------define imgs --------------------------------------

PRO define_imgs,filename
COMMON WSCC_MKMOVIE_COMMON

   BREAK_FILE, filename, a, dir, name, ext

   if strpos(filename,'.hdr') gt -1 then pngmvi=filename else pngmvi='0'
   OPENR, lu1, filename,/get_lun
   SCCREAD_MVI, lu1, file_hdr1, ihdrs1, imgs1, swapflag,  pngmvi=pngmvi
    if file_hdr1.truecolor lt 1 then imgs = bytarr(file_hdr1.nx, file_hdr1.ny, file_hdr1.nf) else imgs = bytarr(3,file_hdr1.nx, file_hdr1.ny, file_hdr1.nf)
    FOR i=0,file_hdr1.nf-1 DO BEGIN
         if (pngmvi ne '0') THEN begin  
              filein=string(imgs1(findx[i]))
	      if strpos(filein,'.png') gt -1 then imgt=read_png(dir+string(imgs1(findx[i])),r,g,b)
	      if strpos(filein,'.gif') gt -1 then read_gif,dir+string(imgs1(findx[i])),imgt,r,g,b
	      IF (file_hdr1.truecolor EQ 0) then imgs(*,*,i)=imgt else begin
                imgi=bytarr(3,n_Elements(imgt(*,0)),n_Elements(imgt(0,*)))
                imgs(0,*,*,i)=r(imgt) & imgs(1,*,*,i)=g(imgt) & imgs(2,*,*,i)=b(imgt)
	      ENDELSE
         ENDIF ELSE BEGIN
	   IF (file_hdr1.truecolor EQ 0) THEN imgs(*,*,i)=imgs1[*,*,findx[i]] ELSE imgs(*,*,*,i)=imgs1[*,*,*,findx[i]]
         ENDELSE
     ENDFOR

END
;----------------Zoom --------------------------------------

PRO sccSHOW_ZOOM

COMMON sWIN_INDEXM
COMMON sALL_ROLLS
   device,get_screen_size=ss
   WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE
   IF (moviev.showzoom NE 1) THEN WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
   IF (moviev.showzoom EQ 2) THEN begin
      moviev.showzoom = 3 
      print,'Use left mouse button to select upper right hand corner for zoom window'
   endif else IF (moviev.showzoom EQ 0) THEN begin
     moviev.showzoom = 2
      print,'Use left mouse button to select lower left hand corner for zoom window'
   endif else  IF (moviev.showzoom EQ 3) THEN BEGIN
      if (zstate.x_zm_sz gt 0.75*ss(0) OR zstate.y_zm_sz gt 0.9*ss(1)) then begin
          maxs=[fix(0.75*ss(0)/(zstate.x_zm_sz/zstate.scale)),fix(0.9*ss(1)/(zstate.y_zm_sz/zstate.scale))]
          print,'Zoomed image to large reduceing scale from ',zstate.scale,' to ',min(maxs)
          zstate.scale=min(maxs)
          WIDGET_CONTROL, moviev.zslide, SET_VALUE=zstate.scale
	  zstate.x_zm_sz=abs(zstate.x1-zstate.x0)*zstate.scale
          zstate.y_zm_sz=abs(zstate.y1-zstate.y0)*zstate.scale
      endif
      WIDGET_CONTROL, moviev.zoom_w, DRAW_XSIZE=zstate.x_zm_sz
      WIDGET_CONTROL, moviev.zoom_w, DRAW_YSIZE=zstate.y_zm_sz
      WIDGET_CONTROL, moviev.zoombase, MAP=1
      moviev.showzoom = 1
      WIDGET_CONTROL, moviev.base, TIMER=moviev.stall
      print,'Click left mouse button on movie window to change center position of zoom'
   ENDIF else IF (moviev.showzoom EQ 1) THEN BEGIN
       WIDGET_CONTROL, moviev.zoombase, MAP=0
       moviev.showzoom = 0
   ENDIF

  WIDGET_CONTROL, base, SET_UVALUE=moviev;  ,yoffset=0,xoffset=tmp(0)-tmp(0)/2.0

  RETURN
END


PRO sccWRUNMOVIE_DRAWM, ev

COMMON sWIN_INDEXM
COMMON sALL_ROLLS

;   LOADCT, 0, /SILENT
;   color = 255
   WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE
   evx=ev.x
   evy=ev.y
   IF ((ev.press and 1) eq 1) and moviev.showzoom eq 1 and ev.top eq moviev.zoombase THEN markzoom=1 else markzoom=0 
   IF ((ev.press and 1) eq 1) and moviev.showzoom eq 0 THEN nozoom=1 else nozoom=0 

   if moviev.showzoom eq 1 and ev.top eq moviev.zoombase  then begin
     evx=(evx/zstate.scale)+zstate.x0
     evy=(evy/zstate.scale)+zstate.y0
   endif 

   IF (moviev.deleted(moviev.current)) THEN RETURN

   IF ((ev.press and 4) eq 4) THEN BEGIN    ; right mouse button pressed
      sccSHOW_MENU
      RETURN
   ENDIF

   ;**********
   ;** Set up annotation text parameters
    IF (usebigfont) THEN BEGIN
    	csize=2
	fonttype=-1
	fontfac=1.2 ; for Triplex Roman font width
    	greek='!7'  ; complex greek
    ENDIF ELSE BEGIN
    	csize=1
	fonttype=0
	fontfac=1.
	greek='!4'  ; Simplex Greek
    ENDELSE
    texthgt=10*csize/fontfac
    ;pfx2=greek+'h!X='   
    
;   IF ((ev.press and 1) eq 1) and moviev.annotate eq 4 THEN BEGIN          ;
;          WIDGET_CONTROL, textbase, GET_UVALUE=tstate
;          tstate.xp=evx & tstate.yp=evy
;          WIDGET_CONTROL, tstate.mvtxtxp, SET_VALUE=evx 
;          WIDGET_CONTROL, tstate.mvtxtyp, SET_VALUE=evy 
;          WIDGET_CONTROL, textbase, SET_UVALUE=tstate 
;          moviev.annotate = 1
;       endif   
;   IF ((ev.press and 1) eq 1) and moviev.annotate eq 3 THEN BEGIN          ;
;          WIDGET_CONTROL, textbase, GET_UVALUE=tstate
;          tstate.arxy(2)=evx & tstate.arxy(3)=evy
;          WIDGET_CONTROL, tstate.mvarrx2, SET_VALUE=evx 
;          WIDGET_CONTROL, tstate.mvarry2, SET_VALUE=evy 
;          WIDGET_CONTROL, textbase, SET_UVALUE=tstate 
;          moviev.annotate = 1
;       endif   
;   IF ((ev.press and 1) eq 1) and moviev.annotate eq 2 THEN BEGIN          ;
;          WIDGET_CONTROL, textbase, GET_UVALUE=tstate
;          tstate.arxy(0)=evx & tstate.arxy(1)=evy
;          WIDGET_CONTROL, tstate.mvarrx1, SET_VALUE=evx 
;          WIDGET_CONTROL, tstate.mvarry1, SET_VALUE=evy 
;          WIDGET_CONTROL, textbase, SET_UVALUE=tstate 
;          moviev.annotate = 3
;       endif   
 
   IF ((ev.press and 1) eq 1) and moviev.showzoom ge 2 THEN BEGIN          ; Left mouse button pressed to set zoom window
       textlen=(3+strlen(trim(moviev.hsize))+strlen(trim(moviev.vsize)))*2*csize
       if moviev.showzoom eq 2 then begin
          zstate.x0=evx & zstate.y0=evy
         DEVICE, COPY = [0, 0, textlen, texthgt, evx,evy, 11] ; clear space for letters
         xyouts,evx,evy,'Z1', /DEVICE, font=fonttype, charsize=csize, color=255
       endif   
       if moviev.showzoom gt 2 then begin 
          if zstate.x0 gt evx then begin 
             zstate.x1=zstate.x0 & zstate.x0=evx
	  endif else zstate.x1=evx
          if zstate.y0 gt evy then begin 
             zstate.y1=zstate.y0 & zstate.y0=evy
	  endif else zstate.y1=evy
          zstate.x_zm_sz=abs(zstate.x1-zstate.x0)*zstate.scale
          zstate.y_zm_sz=abs(zstate.y1-zstate.y0)*zstate.scale
          zstate.xc=zstate.x0+abs(zstate.x1-zstate.x0) / 2L
          zstate.yc=zstate.y0+abs(zstate.y1-zstate.y0) / 2L
         DEVICE, COPY = [0, 0, textlen, texthgt, evx,evy, 11] ; clear space for letters
         xyouts,evx,evy,'Z2', /DEVICE, font=fonttype, charsize=csize, color=255
       endif
       sccSHOW_zoom
       WIDGET_CONTROL, base, GET_UVALUE=moviev
       if moviev.showzoom eq 1 then begin
          WIDGET_CONTROL, base, SET_UVALUE=moviev
          draw_zoom
          WIDGET_CONTROL, base, GET_UVALUE=moviev
       endif
      RETURN
   ENDIF


  ;**********
   ;** Display cursor xy location in lower right of draw window
    yoff = lowerxyval*40*moviev.vsize/1024
    WSET, moviev.draw_win
;   IF(edit) THEN TV, moviev.images[*,*,moviev.current]
    ;-- Determine size of output text
    text='(' + trim(evx) + ',' + trim(evy) + ')' 
    textlen=(3+strlen(trim(moviev.hsize))+strlen(trim(moviev.vsize)))*6*csize
    xout=moviev.hsize-textlen-1
    yout= 5+yoff
    DEVICE, COPY = [0, 0, textlen, texthgt+1, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, '!17'+text, /DEVICE, font=fonttype, charsize=csize, color=255

    xyout1='x,y=(' + string(evx,'(i4)') + ',' + string(evy,'(i4)') + ')'

    ;--use Triplex Roman Hershey vector font IFF CHARSIZE keyword set
   ;**********
   ;** Display DN in lower left of draw window
    yoff = 40*moviev.vsize/1024
    dnoff= lowerdnval*2*yoff
    ;-- Determine size of output text
    text='DN='+ TRIM(FIX((TVRD(evx,evy,1,1))(0))) 
    textlen=6*6*csize*fontfac
    xout=1
    yout= 5+dnoff
    DEVICE, COPY = [0, 0, textlen, texthgt, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, text, /DEVICE, font=fonttype, charsize=csize, color=255
    xyout2='DN='+ string(FIX((TVRD(evx,evy,1,1))(0)),'(i3)')

   ;	roll = get_roll_or_xy(moviev.img_hdrs(moviev.current),'ROLL',/AVG)
   ;	lastdate = moviev.img_hdrs[moviev.current].date_obs
   ;	print,'Using sun center ',suncenx, sunceny
   ;ENDIF
   ;IF moviev.img_hdrs[moviev.current].date_obs NE lastdate THEN BEGIN
;   IF STRMID(moviev.img_hdrs[moviev.current].date_obs,0,10) NE lastdate THEN BEGIN
;        IF NOT file_exist(getenv('ANCIL_DATA')+'/attitude/roll/nominal_roll_attitude.dat') THEN BEGIN
;        print,'%% WARNING: Can not find nominal_roll_attitude.dat file.'
;        print, 'Using roll angle from fits header instead.'
;        print,' This may cause problems with Position Angle display.'
;        angle = moviev.rect
;        ENDIF ELSE BEGIN
;   	roll = get_roll_or_xy(moviev.img_hdrs(moviev.current),'ROLL',/AVG) ; This line stays!!
;        IF (DATATYPE(rolls) EQ 'UND') THEN BEGIN
;          roll = moviev.img_hdrs(moviev.current).CROTA
;		;get_roll_or_xy(moviev.img_hdrs(moviev.current),'ROLL',/AVG,/SILENT) ; This line stays!!
;        ENDIF ELSE BEGIN
;          roll= rolls(moviev.current)
;        ENDELSE
;        angle = angle - moviev.rect + (roll * 180./!PI)  ; This was below
;        ENDELSE
;        ;
;	lastdate = STRMID(moviev.img_hdrs[moviev.current].date_obs,0,10)
;	print,'Using sun center ',suncenx, sunceny
;   ENDIF
;   print, 'Using roll ', rolls[moviev.current]
;   print, 'Using sun center ', suncenx, sunceny
    
    thistime = moviev.img_hdrs[moviev.current].date_obs+' '+moviev.img_hdrs[moviev.current].time_obs
    aorb=strmid(moviev.img_hdrs[moviev.current].filename,strpos(moviev.img_hdrs[moviev.current].filename,'.f')-1,1)

   ;** calculate radius in pixels
    cx=     moviev.sunc(moviev.current).xcen	; IDL coord
    cy=     moviev.sunc(moviev.current).ycen
    solrc=  moviev.solr(moviev.current)     ; solar radious in arcsec
    arcsc=  moviev.arcs(moviev.current)     ; platescl arcsec/pixel
    ;--Do spherical projection correction (currently HI only)
    ;  or for EUVI do carrington lon/lat
    ;  or for COR2 just use wcs for computation
    unitf=3600.
    IF datatype(wcshdrs) NE 'UND' THEN BEGIN
    	;--Compute coordinates of cursor location
	wcs=wcshdrs[moviev.current]
	IF (debugon) THEN BEGIN
	    cxy=wcs_get_pixel(wcs,[0,0])
	    cx=cxy[0]
	    cy=cxy[1]
	ENDIF
	cunit=wcs.cunit[0]
	IF cunit EQ 'deg' THEN unitf=1.
	arcsc=ABS(wcs.cdelt[0])*3600./unitf
    	xy=wcs_get_coord(wcs,[evx,evy])
	
	rx=xy[0]	; degrees or arcsec
	ry=xy[1]
	;rsec=sqrt(rx^2+ry^2)
	rth=hpc2hpr(xy, DEGREES=(unitf LT 3600),/silent)
	; output always degrees
	   
	r=rth[0]
	rpix=r*unitf/wcs.cdelt[0]
	;--Compute latitude angle
	    ;angle=-atan(rx,ry)*180./!pi
    	angle=rth[1]
	
	IF (moviev.img_hdrs(moviev.current).detector) EQ 'COR2' THEN BEGIN
 ;   	    aorb=rstrmid(moviev.img_hdrs(moviev.current).filename,4,1)
    	    rpix1 = Cor2_DISTORTION(rpix, aorb, arcsc) 	; input is pixels
	    ; output is pixels
	    r=r*rpix1/rpix
	    rpix=rpix1
	END

    ENDIF ELSE BEGIN
    	;--Compute r
    	rpix = SQRT( FLOAT(evx-cx)^2 + FLOAT(evy-cy)^2 ) 
   ;** convert to degrees
    	CASE (moviev.img_hdrs(moviev.current).detector) OF
    	'C2' : BEGIN 	;** apply C2 geometric distortion 
            	d = C2_DISTORTION(rpix, arcsc)
		;returns arcsec
            	r = d / 3600.
            END
    	'C3' : BEGIN 	;** apply C3 geometric distortion 
            	;d = C3_DISTORTION(r*1024/moviev.hsize)
            	d = C3_DISTORTION(rpix, arcsc)
 		;returns arcsec
           	r = d / 3600.
            END
	'COR2' : BEGIN
	; in case no wcs headers retrieved
    	    	d = Cor2_DISTORTION(rpix, aorb, arcsc) 	; input is pixels
	    	;returns pixels
	    	r = d * arcsc/ 3600. 
	    END
    	ELSE : BEGIN	
            	r = rpix * arcsc / 3600.
            END
    	ENDCASE
    	;--Compute angle
    	angle = ATAN( FLOAT(cx-evx), FLOAT(evy-cy) ) * 180./!PI
   	;standard is counter-cw from north is positive
    	angle = angle + rolls[moviev.current]
    ENDELSE
   ;angle = angle - moviev.rect + (roll * 180./!PI)
   
   ;**********
   ;** Compute values for chosen coordinate system and corresponding formatting
   ;
   ; vlong=['Solar radii','Degrees ','Carrington','Heliographic Cartesian']

    ; elong EQ 1 , Elongation in units Degrees
    pfx= 'E='
    pfx2='PA='
    sfx= '!9%!3!X'    ; deg symbol, then revert to original
    sfx2='!9%!3'
    sfxw= 'deg'
    ;sfx2w='deg'
    fmt= '(F6.2)'
    fmt2='(F6.2)'
    len =9
    len2=10
    IF arcsc LT 36 then begin
    ; at platescale of 36 arcsec/pixel, 1 pixel = 0.01 deg
    	fmt= '(F7.4)'
	len= 10
    endif
    fonttype=-1   

    ;
    IF elong EQ 2 THEN BEGIN   ;--Display Carrington latitude and longitude
    	IF (r * 3600./ solrc) LT 1.0 and datatype(wcs) NE 'UND' THEN BEGIN
	    cl=euvi_heliographic(wcs,[evx,evy],/CARRINGTON)
    	    r=	    cl[0]   ; longitude
    	    angle=  cl[1]   ; latitude
    	    pfx ='CLN='
    	    pfx2='CLT='
    	    fmt='(F6.2)'
	    len=11
	    IF arcsc LT 4 THEN BEGIN
	    	fmt='(F8.4)'
		len=13
	    ENDIF
    	    len2=len
	    fmt2=fmt
	ENDIF
    ENDIF 
    IF (elong EQ 0) THEN BEGIN	    ; Elongation in solar radii
	r = r * 3600./ solrc   	; Rsun
	pfx='R='
    	sfx=''
	sfxw='   '
	;IF ~(usebigfont) THEN fonttype=0 
    ENDIF
    IF (elong EQ 3) THEN BEGIN	    ; heliographic cartesian
	IF r LT 5 THEN doquick=1 ELSE doquick=0
    	WCS_CONV_HPR_HPC,angle ,r ,HPLN, HPLT ,/ZERO_CENTER,ang_units='degrees',QUICK=doquick
        r=HPLN & angle=HPLT
    	pfx= 'X='
    	pfx2='Y='
	fmt2=fmt
	len2=len
    ENDIF
    IF systemstr EQ 'STPLN' THEN BEGIN	; epipolar plane angle
    	IF thistime NE lasttime THEN $
	deltaroll =   get_stereo_roll(thistime, aorb, system='STPLN') $
             	    - get_stereo_roll(thistime, aorb, system='RTN')
        angle =  angle - deltaroll
	pfx2='STPLN='
	len2=len2+3
    ENDIF
    IF (angle lt 0) and (pfx ne 'CLN=') and (pfx ne 'X=')  THEN angle=angle+360.  
    
   ;**********
   ;** Display angle in upper left of draw window
   ;
    ;-- Determine size of output text
    text=pfx + TRIM(STRING(r,FORMAT=fmt)) +sfx
    IF cx GE moviev.hsize/2 THEN maxx=cx ELSE maxx=moviev.hsize-cx
    IF cy GE moviev.vsize/2 THEN maxy=cy ELSE maxy=moviev.vsize-cy
    maxr=fix(sqrt(float(maxx)^2+float(maxy)^2)* arcsc / solrc)
    textlen=len*6*csize
    xout=1
    yout= moviev.vsize-18-yoff
    DEVICE, COPY = [0, 0, textlen, texthgt, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, text, /DEVICE, font=fonttype, charsize=csize, color=255
    xyout3=pfx + TRIM(STRING(r,FORMAT=fmt)) +sfxw


   ;**********
   ;** Display angle in upper right of draw window (CCW positive) -OR-
   ;           Declination -OR- solar latitude
    ;-- Determine size of output text
    text=pfx2 + TRIM(STRING(angle,FORMAT=fmt2)) + sfx2
    textlen=len2*6*csize
    xout=moviev.hsize-textlen-1
    yout= moviev.vsize-18-yoff
    DEVICE, COPY = [0, 0, textlen, texthgt, xout, yout, 11] ; clear space for letters
    XYOUTS, xout, yout, text, /DEVICE, font=fonttype,charsize=csize, color=255
    xyout4=pfx2+TRIM(STRING(angle,FORMAT=fmt2))+'deg'

    coordval=xyout1+' '+xyout2+' '+xyout3+' '+xyout4;,format='(a17,1x,a6,1x,a10,1x,a12)')
    widget_control,moviev.mousecoords,set_value=coordval

    lasttime=thistime
    
if debugon THEN help,maxr,solrc,arcsc,cx,cy,angle,rpix
IF debugon THEN print,evx,evy
if debugon THEN wait,2


    IF (ev.press GE 1) THEN BEGIN
    	textf=6
    	if datatype(featwin) eq 'UND' then featwin=-1
    	if featwin ne moviev.current then nhtfeat=0 else nhtfeat=nhtfeat+1
    	featwin=moviev.current
    	if nhtfeat lt 10 then featnums=string(nhtfeat,'(i1.1)') else featnums=string(nhtfeat,'(i2.2)')
    	if nhtfeat ge 10 then textf=textf*2 
    	if pfx eq 'X=' or pfx eq 'CLN=' then fstring='(f9.4,a11,a1,a8,f9.4,a5,2x,a,2i5)' else fstring='(f9.3,a11,a1,a8,f9.1,a5,2x,a,2i5)'
    	if pfx eq 'CLN=' then nhtstr=     '#  CRLN      DATE     TIME      CRLT   TEL  FC  COL  ROW' else $
    	if pfx eq 'X=' then nhtstr=       '#     X      DATE     TIME        Y    TEL  FC  COL  ROW' else $
	                           nhtstr='# HEIGHT     DATE     TIME      ANGLE  TEL  FC  COL  ROW'
      
    	;if nhtfeat eq 0 then PRINT,nhtstr
    	if moviev.celong ne elong then PRINT, nhtstr 	      
    	if moviev.celong ne elong then moviev.celong=elong      
    	PRINT,r,strmid(moviev.img_hdrs(moviev.current).date_obs,0,10),'T',moviev.img_hdrs(moviev.current).time_obs,angle,moviev.img_hdrs(moviev.current).detector,featnums,evx,evy,FORMAT=fstring	      
    ENDIF
    IF (ev.press EQ 2) THEN BEGIN  ;Middle mouse button pressed
	posang=angle
	temptime=strmid(moviev.img_hdrs(moviev.current).date_obs,0,10)+'T'+moviev.img_hdrs(moviev.current).time_obs
	tempheight=r
	temptime=utc2tai(str2utc(temptime))
	sync_point=[temptime,tempheight,posang]
	;help,temptime,tempheight,posang
	OPENW, lun,'$HOME/syncjmapwithmovie.dat', /get_lun
	printf,lun, sync_point
	free_lun,lun
	WSET, moviev.draw_win
        DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index[moviev.current]]
	sync_point=[ev.x,ev.y]
    	;print,'cxy=     '+string(xy[0],'(f10.6)')+string(xy[1],'(f12.6)')+' rth='+string(r,'(f10.6)')+string(angle,'(f12.6)')+' at frame '+trim(moviev.current)
	plot_sync
    ENDIF

;   LOADCT, 0, /SILENT
   ;**********
   ; Save data points for Height-Time Plots
   IF ((ev.press and 1) eq 1) THEN BEGIN          ; Left mouse button pressed
     IF (STRLEN(moviev.htfile) ne 0) and (markzoom eq 1 or nozoom eq 1)THEN BEGIN
       sccFILL_HT,htfile,moviev.img_hdrs(moviev.current),r,angle,evx,evy,featnum,pfx
       moviev.htfile=htfile
       featnums=string(featnum,'(i1.1)')
       WSET, moviev.draw_win
       textlen=textf*csize*fontfac
       if featnum ne -1 then begin
         DEVICE, COPY = [0, 0, textlen, texthgt, evx,evy, 11] ; clear space for letters
         xyouts,evx,evy,featnums, /DEVICE, font=fonttype, charsize=csize, color=255
         if markzoom eq 1 then begin
           save_win = !D.WINDOW
           WSET, zstate.zoom_win
           DEVICE, COPY = [0, 0, textlen, texthgt, ev.x,ev.y, 11] ; clear space for letters
           xyouts,ev.x,ev.y,featnums, /DEVICE, font=fonttype, charsize=csize, color=255
           WSET,save_win
         endif
       endif
     ENDIF 
   END 
   IF ((ev.press and 1) eq 1) and moviev.showzoom eq 1 and ev.top ne moviev.zoombase THEN BEGIN          ; Left mouse button pressed
       zstate.xc=evx & zstate.yc=evy
       WIDGET_CONTROL, base, SET_UVALUE=moviev
       draw_zoom    
   ENDIF
; crop
   if (((ev.press and 1) eq 1) and moviev.crop(0) eq 3) then begin ; center zoom windows
       WSET, moviev.draw_win
       DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
       xcen=moviev.crop(1)+(moviev.crop(3)-moviev.crop(1))/2
       ycen=moviev.crop(2)+(moviev.crop(4)-moviev.crop(2))/2
           xadj=ev.x-xcen
           yadj=ev.y-ycen
          moviev.crop(1)=moviev.crop(1)+xadj & if moviev.crop(1) lt 0 then moviev.crop(1)=0
           moviev.crop(2)=moviev.crop(2)+yadj & if moviev.crop(2) lt 0 then moviev.crop(2)=0
           moviev.crop(3)=moviev.crop(3)+xadj & if moviev.crop(3) gt moviev.hsize then moviev.crop(3)=moviev.hsize
           moviev.crop(4)=moviev.crop(4)+yadj & if moviev.crop(4) gt moviev.vsize then moviev.crop(4)=moviev.vsize
      DEVICE, COPY = [0, 0, moviev.crop(3)-moviev.crop(1), 2,moviev.crop(1), moviev.crop(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.crop(3)-moviev.crop(1), 2,moviev.crop(1), moviev.crop(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.crop(4)-moviev.crop(2), moviev.crop(1), moviev.crop(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.crop(4)-moviev.crop(2), moviev.crop(3), moviev.crop(2), 11] ;right side
    endif
   if (((ev.press and 1) eq 1) and (moviev.crop(0) eq 2)) then begin ; second corner selected
      moviev.crop(3)=evx
      moviev.crop(4)=evy
      moviev.crop(0)=3 ;set to 3 when rxycen get defined.
      if moviev.crop(1) gt moviev.crop(3) then begin
          tmp=moviev.crop(3) & moviev.crop(3) = moviev.crop(1) & moviev.crop(1)=tmp
      endif
      if moviev.crop(2) gt moviev.crop(4) then begin
          tmp=moviev.crop(4) & moviev.crop(4) = moviev.crop(2) & moviev.crop(2)=tmp
      endif
      DEVICE, COPY = [0, 0, moviev.crop(3)-moviev.crop(1), 2,moviev.crop(1), moviev.crop(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.crop(3)-moviev.crop(1), 2,moviev.crop(1), moviev.crop(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.crop(4)-moviev.crop(2), moviev.crop(1), moviev.crop(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.crop(4)-moviev.crop(2), moviev.crop(3), moviev.crop(2), 11] ;right side
      print,'press go to crop movie -or- click on image to recenter crop position -or- press crop to start over'
   endif
   if (((ev.press and 1) eq 1) and moviev.crop(0) eq 1) then begin ; first corner selected
      moviev.crop(1)=evx
      moviev.crop(2)=evy
      moviev.crop(0)=2
      print,'Select upper right corner'
   endif

; track_spot
   if (((ev.press and 1) eq 1) and moviev.tspot(0) eq 3) then begin ; center zoom windows
       WSET, moviev.draw_win
       DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
       xcen=moviev.tspot(1)+(moviev.tspot(3)-moviev.tspot(1))/2
       ycen=moviev.tspot(2)+(moviev.tspot(4)-moviev.tspot(2))/2
           xadj=ev.x-xcen
           yadj=ev.y-ycen
           moviev.tspot(1)=moviev.tspot(1)+xadj & if moviev.tspot(1) lt 0 then moviev.tspot(1)=0
           moviev.tspot(2)=moviev.tspot(2)+yadj & if moviev.tspot(2) lt 0 then moviev.tspot(2)=0
           moviev.tspot(3)=moviev.tspot(3)+xadj & if moviev.tspot(3) gt moviev.hsize then moviev.tspot(3)=moviev.hsize
           moviev.tspot(4)=moviev.tspot(4)+yadj & if moviev.tspot(4) gt moviev.vsize then moviev.tspot(4)=moviev.vsize
      DEVICE, COPY = [0, 0, moviev.tspot(3)-moviev.tspot(1), 2,moviev.tspot(1), moviev.tspot(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.tspot(3)-moviev.tspot(1), 2,moviev.tspot(1), moviev.tspot(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.tspot(4)-moviev.tspot(2), moviev.tspot(1), moviev.tspot(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.tspot(4)-moviev.tspot(2), moviev.tspot(3), moviev.tspot(2), 11] ;right side
    endif
   if (((ev.press and 1) eq 1) and (moviev.tspot(0) eq 2)) then begin ; second corner selected
      moviev.tspot(3)=evx
      moviev.tspot(4)=evy
      moviev.tspot(0)=3 ;set to 3 when rxycen get defined.
      if moviev.tspot(1) gt moviev.tspot(3) then begin
          tmp=moviev.tspot(3) & moviev.tspot(3) = moviev.tspot(1) & moviev.tspot(1)=tmp
      endif
      if moviev.tspot(2) gt moviev.tspot(4) then begin
          tmp=moviev.tspot(4) & moviev.tspot(4) = moviev.tspot(2) & moviev.tspot(2)=tmp
      endif
      DEVICE, COPY = [0, 0, moviev.tspot(3)-moviev.tspot(1), 2,moviev.tspot(1), moviev.tspot(2), 11] ;bottom line
      DEVICE, COPY = [0, 0, moviev.tspot(3)-moviev.tspot(1), 2,moviev.tspot(1), moviev.tspot(4), 11] ;top line
      DEVICE, COPY = [0, 0, 2, moviev.tspot(4)-moviev.tspot(2), moviev.tspot(1), moviev.tspot(2), 11] ;leftside 
      DEVICE, COPY = [0, 0, 2, moviev.tspot(4)-moviev.tspot(2), moviev.tspot(3), moviev.tspot(2), 11] ;right side
      print,'press go to crop movie -or- click on image to recenter crop position -or- press crop to start over'
   endif
   if (((ev.press and 1) eq 1) and moviev.tspot(0) eq 1) then begin ; first corner selected
      moviev.tspot(1)=evx
      moviev.tspot(2)=evy
      moviev.tspot(0)=2
      print,'Select upper right corner'
   endif

  WIDGET_CONTROL, moviev.base, SET_UVALUE=moviev

END

;----------------adjust color-------------------------------
PRO sADJUST_CT, ev

COMMON WSCC_MKMOVIE_COMMON

WIDGET_CONTROL,ev.top, Get_UValue=moviev
WIDGET_CONTROL, moviev.base, GET_UVALUE=moviev

;input = ev.value

;WIDGET_CONTROL, ev.id, GET_UVALUE=test
;IF (DATATYPE(test) EQ 'STR') THEN input = test
input = Tag_Names(ev, /Structure_Name)
;input = ev.value
print,"sADJUST_CT: " + input
;help,/str,ev
;stop
CASE (input) OF

    'WIDGET_BUTTON': BEGIN
   	;window,1,xsize=512,ysize=512
      XColors, Group_Leader=ev.top, NotifyID=[ev.id,ev.top]
    END
    'XCOLORS_LOAD': BEGIN
      	Device, Get_Visual_Depth=thisDepth
      ;IF thisDepth GT 8 THEN BEGIN
 	winsave = !d.window
	nim = n_elements(moviev.win_index)
	for i = 0, nim-1 do begin
	print,'re-displaying frame'
	wset,moviev.win_index(i)
	tv, imgs[*,*,i]
	endfor
	;image = tvrd(true=1)
	wset,moviev.draw_win
	DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
	WSET,winsave
      ;ENDIF
    END
    'XCOLORS_ACCEPT': print,'Done with xcolors.pro'
ENDCASE
Widget_Control, ev.top, Set_UValue=moviev, /No_Copy

END

PRO draw_zoom 

COMMON WSCC_MKMOVIE_COMMON
; always define common blocks just once - nr
COMMON sWIN_INDEXM
COMMON sALL_ROLLS

  WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

  if (datatype(imgs) ne 'BYT') then define_imgs,inpfilename

  if moviev.file_hdr.truecolor le 0 then orig_image=imgs(*,*,moviev.current) else orig_image=imgs(*,*,*,moviev.current)


  ; compute size of rectangle in original image
  ; round up to make sure image fills zoom window
  rect_x = long(zstate.x_zm_sz / float(zstate.scale) + 0.999)
  rect_y = long(zstate.y_zm_sz / float(zstate.scale) + 0.999)

  ; Plan to erase if the zoom rectangle is larger than the original
  ;  image size.
  doerase = (rect_x GT zstate.x_im_sz OR rect_y GT zstate.y_im_sz)

  rect_x = rect_x < zstate.x_im_sz
  rect_y = rect_y < zstate.y_im_sz

  ; compute location of origin of rect (user specified center)
  x0 = zstate.xc - rect_x/2
  y0 = zstate.yc - rect_y/2

  ; make sure rectangle fits into original image
  ;left edge from center
  x0 = x0 > 0
  ; limit right position
  x0 = x0 < (zstate.x_im_sz - rect_x)

  ;bottom
  y0 = y0 > 0
  y0 = y0 < (zstate.y_im_sz - rect_y)

  ;Save window number
  save_win = !D.WINDOW
  WSET, zstate.zoom_win

  IF (zstate.scale EQ 1) THEN BEGIN
    IF doerase THEN ERASE

    ; don't use tvscl here, to preserve same range as in unzoomed image
    if moviev.file_hdr.truecolor le 0 then TV, orig_image[x0:x0+rect_x-1,y0:y0+rect_y-1] else $
       TV, orig_image[*,x0:x0+rect_x-1,y0:y0+rect_y-1],true=1

  ENDIF ELSE BEGIN
    ;Make integer rebin factors.  These may be larger than the zoom image
    dim_x = rect_x * zstate.scale
    dim_y = rect_y * zstate.scale

    ; Constrain upper right edge to within original image.
    x1 = (x0 + rect_x - 1) < (zstate.x_im_sz-1)
    y1 = (y0 + rect_y - 1) < (zstate.y_im_sz-1)


    if moviev.file_hdr.truecolor le 0 then temp_image = rebin(orig_image[x0:x1,y0:y1], $
                            	    	    	    	    	dim_x, dim_y, $
                            	    	    	    	    	sample=zstate.sample)

    if moviev.file_hdr.truecolor eq 1 then begin
       temp_image = bytarr( 3,dim_x, dim_y)        
       for iz=0,2 do begin
           temp_img1=reform(orig_image(iz,x0:x1,y0:y1))
           temp_img2=rebin(temp_img1,dim_x, dim_y,sample=zstate.sample)
           temp_image(iz,*,*) = temp_img2
        endfor
    endif

    ;Save the zoomed image
    fill_x = dim_x < zstate.x_zm_sz
    fill_y = dim_y < zstate.y_zm_sz
    if moviev.file_hdr.truecolor le 0 then begin
      zoom_image=bytarr(zstate.x_zm_sz,zstate.y_zm_sz)
      zoom_image[0:fill_x-1,0:fill_y-1] = temp_image[0:fill_x-1,0:fill_y-1]
      ; Pad with zoomed image with black if necessary.
      if (fill_x LT zstate.x_zm_sz) then $
        zoom_image[fill_x:zstate.x_zm_sz-1, *] = 0
      if (fill_y LT zstate.y_zm_sz) then $
        zoom_image[*, fill_y:zstate.y_zm_sz-1] = 0
    endif else begin
      zoom_image=bytarr(3,zstate.x_zm_sz,zstate.y_zm_sz)
      zoom_image[*,0:fill_x-1,0:fill_y-1] = temp_image[*,0:fill_x-1,0:fill_y-1]
      ; Pad with zoomed image with black if necessary.
      if (fill_x LT zstate.x_zm_sz) then $
        zoom_image[*,fill_x:zstate.x_zm_sz-1, *] = 0
      if (fill_y LT zstate.y_zm_sz) then $
        zoom_image[*,*, fill_y:zstate.y_zm_sz-1] = 0
   endelse    

    ;Save the corners in original image
    zstate.x0 = x0
    zstate.y0 = y0
    zstate.x1 = x1
    zstate.y1 = y1

   
    ;Display the new zoomed image
    ;Save window number

    ; don't use tvscl here, to preserve same range as in unzoomed image
    TV, zoom_image,true=moviev.file_hdr.truecolor

 ENDELSE

 ;Restore window
 WSET, save_win

  WIDGET_CONTROL, base, SET_UVALUE=moviev


END

;------------------ runmovie event -------------------------------
PRO sccWRUNMOVIE_EVENTM, ev
COMMON sWIN_INDEXM

WIDGET_CONTROL, ev.top, GET_UVALUE=moviev   ; get structure from UVALUE
; Next line added by SHH 7/12/96.  Necessary because current information on widgets stored in main "control" base
WIDGET_CONTROL, moviev.base, GET_UVALUE=moviev   ; get structure from UVALUE    

 IF (ev.id EQ ev.handler) THEN BEGIN   ; A top level base timer event
 
    IF (moviev.forward) THEN BEGIN		;** going forward
        IF ((moviev.bounce) AND (moviev.current EQ moviev.last)) THEN BEGIN
            moviev.current = moviev.last-1
	    moviev.pause = 1
	    moviev.forward = 0		;** set direction to reverse
        ENDIF $
        ELSE BEGIN
            moviev.current = (moviev.current + 1) MOD moviev.len
	    IF ((moviev.current EQ 0) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
            IF ((moviev.current GT moviev.last) OR (moviev.current LT moviev.first)) THEN BEGIN
	   	moviev.pause = 1	
		moviev.current = moviev.first
	    ENDIF
        ENDELSE
    ENDIF $
    ELSE BEGIN			;** going in reverse
        IF ((moviev.bounce) AND (moviev.current EQ moviev.first)) THEN BEGIN
            moviev.current = moviev.first+1
	    moviev.pause = 1
	    moviev.forward = 1		;** set direction to forward
        ENDIF $
        ELSE BEGIN
            moviev.current = (moviev.current - 1) MOD moviev.len
	    IF ((moviev.current EQ moviev.last) AND (moviev.bounce EQ 0)) THEN moviev.pause = 1
            IF ((moviev.current LT moviev.first) OR (moviev.current GT moviev.last)) THEN BEGIN
		moviev.current = moviev.last
		moviev.pause = 1
	    ENDIF
        ENDELSE
    ENDELSE

    IF (moviev.pause AND moviev.dopause) THEN BEGIN
	moviev.pause = 0
	WAIT, .75
    ENDIF

    ;** out of range error checks (to stop bombs from COPY)
    IF (moviev.current LT 0) THEN moviev.current = moviev.first
    IF (moviev.current GE moviev.len) THEN moviev.current = moviev.last
    IF (NOT(moviev.deleted(moviev.current))) THEN BEGIN		;** if not deleted then display
       WSET, moviev.draw_win
       DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
       WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
       WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current), /REMOVE_ALL)
       if moviev.sliden ne -1 then  WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
       if (moviev.showzoom eq 1) then begin
         WIDGET_CONTROL, base, SET_UVALUE=moviev
         draw_zoom
       endif 
       WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
    ENDIF ELSE $	;** else immediately look for the next non-deleted frame
      WIDGET_CONTROL, moviev.base, TIMER=.01

 IF(datatype(sync_point) NE 'UND') THEN plot_sync
 ENDIF ELSE BEGIN	;** event other than timer
 
    WIDGET_CONTROL, ev.id, GET_UVALUE=input
IF debugon THEN print,input
    CASE (input) OF

        'DRAW' : BEGIN	;image window
	    END

	'SPEED' : BEGIN 	;speed
		IF (ev.value EQ 0) THEN $
                   moviev.stall = 5. $
                ELSE $
		   moviev.stall = (100 - ev.value)/50.
                moviev.stallsave = moviev.stall
            ; Next line commented out by SHH 7/12/96
                ;WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
	    END

        'SLIDE_FIRST' : BEGIN	;first frame
		 moviev.first = ev.value
	     END

        'SLIDE_LAST' : BEGIN	;last frame
		 moviev.last = ev.value
	     END

        'FRAME_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.cframe, GET_VALUE=val
                val = 0 > FIX(val(0)) < moviev.last
                moviev.current=val
                DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
          END

	ELSE : BEGIN
                 PRINT, '%%sccWRUNMOVIE_EVENTM.  Unknown event.'
	     END

    ENDCASE

   ENDELSE

  WIDGET_CONTROL, moviev.base, SET_UVALUE=moviev

END ;------------------ end runmovie_event -------------------------


;------------------ runmovie event for CW_BGROUP buttons ---------

FUNCTION sccWRUNMOVIE_BGROUPM, ev
COMMON WSCC_MKMOVIE_COMMON
COMMON sWIN_INDEXM
COMMON sALL_ROLLS

;   WIDGET_CONTROL, ev.top, GET_UVALUE=moviev   ; get structure from UVALUE
   WIDGET_CONTROL, base, GET_UVALUE=moviev   ; get structure from UVALUE

   input=''
   if(where(tag_names(ev) eq 'VALUE') gt -1)then input=ev.Value

   WIDGET_CONTROL, ev.id, GET_UVALUE=test
   IF (DATATYPE(test) EQ 'STR') THEN input = test
    CASE (input) OF

	'MENU' : BEGIN
                   sccSHOW_MENU
                   RETURN, 0
                 END

        'SLIDE_NUM' : BEGIN   ;change current frame number
                WIDGET_CONTROL, moviev.sliden, GET_VALUE=val
                val = 0 > FIX(val(0)) < moviev.last
                moviev.current=val
                DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
                WIDGET_CONTROL, moviev.cname, SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
		plot_sync
                WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                moviev.stall = 10000.
                if (moviev.showzoom eq 1) then begin
                  WIDGET_CONTROL, base, SET_UVALUE=moviev
		  draw_zoom
	        endif
          END

	'SMOOTH': BEGIN
                if moviev.file_hdr.truecolor le 0 then begin
		FOR i=0,n_elements(moviev.win_index)-1 DO BEGIN
			imgs[*,*,i] = smooth(temporary(imgs[*,*,i]), 5)
			wset, moviev.win_index[i]
			tv, imgs[*,*,i]
		ENDFOR
		print, 'Done smoothing'
                endif else print, 'Smoothing does not work with truecolor'
	     END
        'EDIT': BEGIN              ; EDIT Added by SHH 7/12/96
;		  sccSHOW_MENU,/SHOW
                  if keyword_set(edit) then begin
                    winsave=!D.WINDOW
                    WSET,moviev.win_index(moviev.current)
		    IF(n_elements(imgs) GT 1) THEN img = imgs[*,*,moviev.current] $
                    ELSE img = TVRD(0,0,moviev.hsize, moviev.vsize)
                    SCCXEDITFRAMEX,img,newimg,moviev
                    WSET,moviev.win_index(moviev.current)
	            tv, newimg
		    WSET, moviev.draw_win
                    DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
		    WSET,winsave
                    IF(n_elements(imgs) GT 1) THEN imgs[*,*,moviev.current] = newimg
                  endif else print,'Must start scc_wrunmoviem with keyword editk set to enable the edit frame'
   
		END

        'OBJECTSEL' : BEGIN
	               moviev.objectv(ev.value)=ev.select
		END
                
        'OBJECT': BEGIN              ; 
    	    	    IF (debugon) THEN stop
                  if datatype(wcshdrs) ne 'UND' then begin
                  winsave=!D.WINDOW
		  sc=strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.fts')-1,1)
                  obhdrs=wcshdrs(0:n_elements(moviev.img_hdrs)-1)
                  objects=get_object_coords(moviev.img_hdrs,obhdrs,sc)
                  if sc eq 'A' then osc='B' else osc='A'
                  if n_elements(wcshdrs) eq n_Elements(moviev.img_hdrs)*2 then begin
                      obhdrs=wcshdrs(n_elements(moviev.img_hdrs):n_elements(wcshdrs)-1)
                      objectosc=get_object_coords(moviev.img_hdrs,obhdrs,osc)
                   endif

                   FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                      if keyword_set(edit) then begin
                          set_plot,'z'
                          device,set_resolution=[moviev.hsize,moviev.vsize]
		          IF(n_elements(imgs) GT 1) THEN  img = imgs[*,*,i]
                          tv,img
   		      endif else WSET,moviev.win_index(i)
                      if moviev.objectv(0) eq 1 then xyouts,objects(0,i,0),objects(0,i,1),osc,charsize=1,/device
                      if moviev.objectv(1) eq 1 then xyouts,objects(1,i,0),objects(1,i,1),'E',charsize=1,/device
                      if moviev.objectv(2) eq 1 then xyouts,objects(2,i,0),objects(2,i,1),'S',charsize=1,/device
                      if n_elements(wcshdrs) eq n_Elements(moviev.img_hdrs)*2 then begin
                          if moviev.objectv(0) eq 1 then xyouts,objectosc(0,i,0)+moviev.osczero(0),objectosc(0,i,1)+moviev.osczero(1),sc,charsize=1,/device
                          if moviev.objectv(1) eq 1 then xyouts,objectosc(1,i,0)+moviev.osczero(0),objectosc(1,i,1)+moviev.osczero(1),'E',charsize=1,/device
                          if moviev.objectv(2) eq 1 then xyouts,objectosc(2,i,0)+moviev.osczero(0),objectosc(2,i,1)+moviev.osczero(1),'S',charsize=1,/device
                      endif
                      if keyword_set(edit) then begin
                           newimg=tvrd ()
                           select_windows ;set_plot,'x'
                           WSET,moviev.win_index(i)
	                   tv, newimg
                           IF(n_elements(imgs) GT 1) THEN imgs[*,*,i] = newimg
   		      endif
                  endfor
                   WSET, winsave
                  endif else print,'WCSHDRS must be defined to add objects' 
    	    	    IF (debugon) THEN stop

		END                


        'PROJECT': BEGIN              ; 
                   if keyword_set(edit) and (moviev.img_hdrs(0).detector eq 'HI2' or  moviev.img_hdrs(0).detector eq 'HI1') then begin
                    winsave=!D.WINDOW
		       FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                         print,'running Plane Project on Frame #',' ',string(i)

			 wcs=WCSHDRS(i)
		         coords=wcs_get_coord(wcs)
 		         IF(n_elements(imgs) GT 1) THEN  img = imgs[*,*,i]
		         plane_project, img, coords, newimg, SCEN=scen, RANGE=range, spher=1, INTERP=1
			 IF (debugon) THEN help,img,newimg,coords,scen,range
			 IF (debugon) THEN wait,2
                         if i eq 0 then begin
			    moviev.file_hdr.sunxcen=scen(0)
			    moviev.file_hdr.sunycen=scen(1)
			    sx=n_elements(newimg(*,0))
			    sy=n_elements(newimg(0,*))
                            moviev.hsize=sx
			    moviev.vsize=sy
			    moviev.file_hdr.nx=sx
			    moviev.file_hdr.ny=sy
 			 endif			 
                         moviev.img_hdrs(i).xcen=scen(0)
                         moviev.img_hdrs(i).ycen=scen(1)
                         set_plot,'z'
                         device,set_resolution=[moviev.hsize,moviev.vsize]
                         if n_elements(newimg(*,0)) lt sx then sx2=n_elements(newimg(*,0)) else sx2=sx
                         if n_elements(newimg(0,*)) lt sy then sy2=n_elements(newimg(0,*)) else sy2=sy
                         tv,newimg[0:sx2-1,0:sy2-1]
                         newimg=tvrd ()

                         select_windows ;set_plot,'x'
                         WSET,moviev.win_index(i)
	                 tv, newimg
                         if i eq 0 then imgsnew=bytarr(n_elements(newimg(*,0)),n_elements(newimg(0,*)),n_elements(moviev.frames))
                         imgsnew[*,*,i] = newimg
                         ;WCSHDRS(i).coord_type='Heliocentric-Cartesian'
                         WCSHDRS(i).CTYPE=strmid(WCSHDRS(i).CTYPE,0,5)+'TAB'
                         WCSHDRS(i).CRPIX=[scen(0),scen(1)]
                         WCSHDRS(i).CRval=[0.0,0.0]
                         WCSHDRS(i).PROJECTION=''
                         WCSHDRS(i).ROLL_ANGLE= 0
                         WCSHDRS(i).PC(0,0)=1.0  ; for roll angle 0 PC vals =   ( 1.0, -0.0, 0.0, 1.0)
                         WCSHDRS(i).PC(0,1)=-0.0	
                         WCSHDRS(i).PC(1,0)=0.0
                         WCSHDRS(i).PC(1,1)=1.0
    	    	    	 wcshdrs(i).naxis(0)=moviev.file_hdr.nx
    	    	    	 wcshdrs(i).naxis(1)=moviev.file_hdr.ny
                         moviev.img_hdrs(i).filter='Deproject'
                         moviev.img_hdrs(i).roll=0
                      endfor
                       imgs=imgsnew
		       WSET, winsave
                  endif else print,'Option only works on HI movies, you must call scc_wrunmovie with keyword edit to project image'
		END                

        'DRAWGRID': BEGIN              ; 
                   if datatype(imgs) eq 'UND' then begin
                     print,'unable to add grid - imgs are not defined'
                     return, 0
		   endif
 
                   winsave=!D.WINDOW
                   nf=n_elements(moviev.frames)
		   FOR i=0,nf-1 DO BEGIN 
                       print,'Drawing grid for Frame #',' ',string(i)

                       if total(size(wcshdrs)) eq 0 then begin
                           filen=strmid(moviev.img_hdrs(i).filename,0,strpos(moviev.img_hdrs(i).filename,'.'))+'.fts'
                           posk=strpos(filen,'0B')
			   if posk gt -1 then filen=strmid(filen,0,posk)+'?'+strmid(filen,posk+2,strlen(filen))
			   if (debugon) then help,filen
		           fname=sccfindfits(filen)
		           IF fname EQ '' THEN BEGIN
			     strput,filen,'?',16
			     fname = sccfindfits(filen)
		           ENDIF
  	    	           jk=sccreadfits(fname,fhdr,/nodata)
		            matchfitshdr2mvi,fhdr,moviev.file_hdr,moviev.img_hdrs(i)
                            wcs=fitshead2wcs(fhdr)
                       endif else wcs=WCSHDRS(i)
		       coords=wcs_get_coord(wcs)
 
                      if keyword_set(edit) and moviev.file_hdr.truecolor eq 0 then begin
                          set_plot,'z'
			  device,set_resolution=[moviev.hsize,moviev.vsize]
		          img=imgs(*,*,i)
                          tv,img
  		      endif else WSET,moviev.win_index(i)

                      drawcoordgrid, fhdr, coords=coords, /loud, c_charsize=1.7

;                      if n_elements(wcshdrs) eq n_Elements(moviev.img_hdrs)*2 then drawcoordgrid,fhdr ;need offset for right hand movie
                      if keyword_set(edit) then begin
                           newimg=tvrd (true=moviev.file_hdr.truecolor)
			   if moviev.file_hdr.truecolor eq 0 then begin 
			     select_windows ;set_plot,'x'
                             WSET,moviev.win_index(i)
	                     tv, newimg
			   endif
                           if moviev.file_hdr.truecolor eq 0 then imgs(*,*,i)=newimg else imgs(*,*,*,i) = newimg
   		      endif
                  endfor
                   WSET, winsave
		END                
	'CROPMV' : BEGIN	;select crop coords using mouse
                 if datatype(imgs) ne 'UND' then begin
                      WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                      moviev.stall = 10000.
		      moviev.crop(0)=1 ;zoom selected
                      WSET, moviev.draw_win
                      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                      print,'select lower left corner'
                 ENDIF else popup_msg,'imgs must be defined to crop movie',title='SCC_WRUNMOVIEM MSG'
	    END
	'CROPGO' : BEGIN	;select crop coords using mouse
                 if datatype(imgs) ne 'UND' then begin
                   moviev.crop(0)=0
                   tmp = INDGEN(N_ELEMENTS(moviev.win_index))
                   good = WHERE((moviev.deleted NE 1) AND (tmp GE moviev.first) AND (tmp LE moviev.last))
		   HDRS=moviev.img_hdrs(good) 
		   NAMES=moviev.frames(good)
                   typ=strmid(moviev.filename,strpos(moviev.filename,'.'),strlen(moviev.filename)-strpos(moviev.filename,'.'))
                   filename=strmid(moviev.filename,0,strpos(moviev.filename,'.'))+'_crop'+typ
                   FILE_HDR=moviev.FILE_HDR
                   FILE_HDR.NX=(moviev.crop(3)-moviev.crop(1))+1
                   FILE_HDR.NY=(moviev.crop(4)-moviev.crop(2))+1
                   FILE_HDR.SUNXCEN=FILE_HDR.SUNXCEN-moviev.crop(1)
                   FILE_HDR.SUNYCEN=FILE_HDR.SUNYCEN-moviev.crop(2)
                   HDRS.xcen=HDRS.xcen-moviev.crop(1)
                   HDRS.ycen=HDRS.ycen-moviev.crop(2)
                   mccd_pos=moviev.file_hdr.CCD_POS ;[y1,x1,y2,x2]
                   ccdadj=(mccd_pos(2)-mccd_pos(0)+1)/moviev.file_hdr.ny
                   FILE_HDR.ccd_pos(0)=mccd_pos(0)+(moviev.crop(2)+1)*ccdadj
                   FILE_HDR.ccd_pos(2)=FILE_HDR.ccd_pos(0)+FILE_HDR.NY*ccdadj
                   FILE_HDR.ccd_pos(1)=mccd_pos(1)+(moviev.crop(1)+1)*ccdadj
                   FILE_HDR.ccd_pos(3)=FILE_HDR.ccd_pos(1)+FILE_HDR.NX*ccdadj
                   IF file_hdr.truecolor le 0 THEN imgs = imgs(moviev.crop(1):moviev.crop(3),moviev.crop(2):moviev.crop(4),good)  
                   IF file_hdr.truecolor eq 1 THEN imgs = imgs(*,moviev.crop(1):moviev.crop(3),moviev.crop(2):moviev.crop(4),good)
	           WIDGET_CONTROL, /DESTROY, moviev.base
	           WIDGET_CONTROL, /DESTROY, moviev.winbase

                   movie_in=intarr(n_elements(good))
                   for nw=0,n_elements(movie_in)-1 do begin
                     WINDOW, XSIZE =file_hdr.nx, YSIZE = file_hdr.ny, /PIXMAP, /FREE
                     movie_in(nw) = !D.WINDOW
                    if file_hdr.truecolor eq 1 then tv,imgs(*,*,*,nw),true=1 else tv,imgs(*,*,nw)
                  endfor
                  SCC_WRUNMOVIEM, movie_in, HDRS=HDRS, NAMES=NAMES, $
                     file_hdr=file_hdr,filename=filename,/editk

                   RETURN,0
                 ENDIF else popup_msg,'imgs must be defined to crop movie',title='SCC_WRUNMOVIEM MSG'

	    END
	'TSPOT' : BEGIN	;select crop coords using mouse
                 if datatype(imgs) ne 'UND' then begin
                      WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                      moviev.stall = 10000.
		      moviev.tspot(0)=1 ;zoom selected
                      WSET, moviev.draw_win
                      DEVICE, COPY = [0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
                      print,'select lower left corner'
                 ENDIF else popup_msg,'imgs must be defined to crop movie',title='SCC_WRUNMOVIEM MSG'
	    END

	'TRACK_SPOT' : BEGIN	
                cl2=euvi_heliographic(wcshdrs(moviev.current),[moviev.tspot(3),moviev.tspot(4)],/CARRINGTON)
                cl1=euvi_heliographic(wcshdrs(moviev.current),[moviev.tspot(1),moviev.tspot(2)],/CARRINGTON)
                sptav=fltarr(n_elements(moviev.frames))
                spttime=strarr(n_elements(moviev.frames))
                spttitle='long 1 ='+string(cl1(0),'(f8.3)')+' lat 1 ='+string(cl1(1),'(f8.3)')+' long 2 ='+string(cl2(0),'(f8.3)')+' lat 2 ='+string(cl2(1),'(f8.3)')
                winsave=!D.WINDOW
                trackfile=PICKFILE(filter='*.txt',TITLE='Select track_spot output file')
                if trackfile ne '' then begin
		    openw,tracklun,trackfile,/get_lun
		    printf,tracklun,spttitle
                    if moviev.img_hdrs(0).DETECTOR ne 'EIT' then $
		    	sc='STEREO_'+strmid(moviev.img_hdrs(0).filename,strpos(moviev.img_hdrs(0).filename,'.fts')-1,1) else sc='SOHO'
		    printf,tracklun,'OBSRVTRY  ',sc
                    printf,tracklun,'DETECTOR  ',moviev.img_hdrs(0).DETECTOR
                    printf,tracklun,'FILTER    ',moviev.img_hdrs(0).FILTER
                    printf,tracklun,'WAVELNTH  ',moviev.img_hdrs(0).WAVELNTH
                    printf,tracklun,'DATE/TIME                    Total        N_pixels    Average'
                endif
		FOR i=0,n_elements(moviev.frames)-1 DO BEGIN 
                     if moviev.deleted(i) ne 1 then begin
                       clt=euvi_heliographic(wcshdrs(i),/CARRINGTON)                                  
                       cllng=reform(clt(0,*,*))
                       cllat=reform(clt(1,*,*))
                       cllng(where (~Finite(cllng)))=-3000
                       cllat(where (~Finite(cllat)))=-3000
                       if cl2(0) lt cl1(0) then begin
                          cllng(where(cllng lt 100))=cllng(where(cllng lt 100))+360 & cl2_0=cl2(0)+360
		       endif else cl2_0=cl2(0)
                       box=where(cllng ge cl1(0) and cllng le cl2_0 and cllat ge cl1(1) and cllat le cl2(1))
                       spotmsk=fltarr(moviev.hsize,moviev.vsize)+40
                       if box(0) gt -1 then begin
                         spotmsk(box)=0
                         timg=reform(imgs(*,*,i))
                         print,'Frame # ',i,' Total = ', total(timg(box)),' N pixels = ',n_elements(box),' Average = ',total(timg(box))/n_elements(box)
                         if trackfile ne '' then printf,tracklun,moviev.img_hdrs(i).DATE_OBS,'T',moviev.img_hdrs(i).TIME_OBS,total(timg(box)),n_elements(box),total(timg(box))/n_elements(box)
                         sptav(i)=total(timg(box))/n_elements(box)
                         spttime(i)=moviev.img_hdrs(i).DATE_OBS+'T'+moviev.img_hdrs(i).TIME_OBS
                       endif
                       wset,moviev.win_index(i)
                       tv,(fix(imgs(*,*,i))-spotmsk)>0
                       if i eq n_Elements(moviev.frames)-1 then begin
                          window,/free
                          utplot,spttime(where(sptav gt 0)),sptav(where(sptav gt 0)),/ynozero,title=spttitle
		       endif
                     endif
               ENDFOR
              if trackfile ne '' then close,tracklun
              if trackfile ne '' then FREE_LUN,tracklun
              wset,winsave
	      moviev.tspot(0)=0
              moviev.forward = moviev.dirsave
              moviev.stall = moviev.stallsave
              WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event

	    END

	'annotate' : BEGIN	; open/close text widget
                 if datatype(imgs) ne 'UND' then begin
;                    moviev.annotate=1
;                    scc_annotate
                    annotate_image,moviev.win_index
                 ENDIF else popup_msg,'imgs must be defined to annotate movie',title='SCC_WRUNMOVIEM MSG'
                 END

	'DONE' : BEGIN       ;** exit program
		undefine,sync_point
	         WIDGET_CONTROL, /DESTROY, moviev.base
	         If (moviev.base ne moviev.winbase) then  WIDGET_CONTROL, /DESTROY, moviev.winbase
                 WIDGET_CONTROL, /DESTROY, moviev.zoombase
                 If (moviev.annotate ne 0) then WIDGET_CONTROL, /DESTROY, textbase
		   device,/cursor_crosshair 	; return to default cursor
		   device,WINDOW_STATE=ws
		   ;wait,1
		   IF ws[0] EQ 1 THEN wdelete,0
                 RETURN, 0
	     END
	
	'Sync' : BEGIN ;sync with jMaps tool2a
		temp=dblarr(3)
		OPENR, lun, '$HOME/syncjmapwithmovie.dat', /get_lun
		READF, lun, temp
		FREE_LUN, lun  ;read in data of format [Time, Height(elongation angle), Position Angle]
		temptime=temp[0]
		frametimes=utc2tai(str2utc(moviev.img_hdrs.date_obs + 'T' + moviev.img_hdrs.time_obs))
		tempframe=[0,50000.]
		print,'Read in E=',trim(temp[1]),' PA=',trim(temp[2]),' at ',utc2str(tai2utc(double(temptime)),/ecs)
		FOR i=0, n_elements(frametimes)-1 DO BEGIN  ;find frame closest in time
			tempdiff=ABS(frametimes[i]-temptime)
			IF(tempdiff LT tempframe[1]) THEN tempframe=[i,tempdiff]
		ENDFOR
		IF(tempframe[1] EQ 50000.) THEN print,'No movie frame within time range'$
		ELSE BEGIN
                	WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
			moviev.stall=10000.
			moviev.current=tempframe[0]
			i=moviev.current
			IF(datatype(WCSHDRS) EQ 'UND') THEN BEGIN  ;get wcshdr for cor1 and cor2
				filen=strmid(moviev.img_hdrs(i).filename,0,strpos(moviev.img_hdrs(i).filename,'.'))+'.fts'
                           	posk=strpos(filen,'0B')
			   	if posk gt -1 then filen=strmid(filen,0,posk)+'?'+strmid(filen,posk+2,strlen(filen))
		           	fname=sccfindfits(filen)
		           	IF fname EQ '' THEN BEGIN
			   		strput,filen,'?',16
			   		fname = sccfindfits(filen)
			  	ENDIF
  	    		  	jk=sccreadfits(fname,fhdr,/nodata)
				matchfitshdr2mvi,fhdr,moviev.file_hdr,moviev.img_hdrs(i)
                		wcs=fitshead2wcs(fhdr)
			ENDIF ELSE wcs=WCSHDRS(i)
			IF(wcs.cunit[0] EQ 'arcsec') THEN BEGIN  ;COR1 and COR2
				xcen=moviev.sunc(i).xcen ;suns coords in pixels
				ycen=moviev.sunc(i).ycen
				tempangle=(temp[2]-wcs.roll_angle) * !PI/180.  ;from height and position angle, find x and plot x y coordinates
				r=temp[1]*!PI/180. ;elongation angle(height)
				pix_rad=moviev.arcs[moviev.current]^(-1)*3600*180./!PI
				;x=round(xcen-r*sin(tempangle)*pix_deg)
				;y=round(r*cos(tempangle)*pix_deg+ycen)
				x=round(xcen-atan(tan(r)*sin(tempangle))*pix_rad)
				y=round(asin(sin(r)*cos(tempangle))*pix_rad+ycen)
			ENDIF ELSE BEGIN  ;HI1 and HI2
				tempangle=(temp[2]) * !PI/180.  ;from height and pa, find x and y coordinates with respect to sun
				r=temp[1]*!PI/180. ;elongation angle(height)
				;x=-asin(sin(r)*sin(tempangle)/SQRT(1-(sin(r)^2)*cos(tempangle)^2))*180./!pi
				x=-atan(tan(r)*sin(tempangle))*180./!PI
				y=asin(sin(r)*cos(tempangle))*180./!pi
				;WCS_CONV_HPR_HPC,tempangle,r,x,y,/zero_center,ang_units='radians'
				;print,'computed '+string(x,'(f10.6)')+string(y,'(f12.6)')+' from'+string(temp[1],'(f10.6)')+string(temp[2],'(f12.6)')+' at frame '+trim(i)
				coords=wcs_get_pixel(wcs,[x,y])
				x=coords[0] & y=coords[1]
			ENDELSE

			sync_point=[x,y]
			WSET, moviev.draw_win
                	DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index[moviev.current]]
    			WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    			WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames[moviev.current],/REMOVE_ALL)
                	WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
			plot_sync
		ENDELSE
     		END
   'rmv_sync': BEGIN undefine,sync_point
		WSET,moviev.draw_win
		DEVICE, COPY=[0,0,moviev.hsize,moviev.vsize,0,0,moviev.win_index[moviev.current]]
	       END
   'sync_color': plot_sync,/reverse
   'ENDCONTROL' : BEGIN       ;** exit program
                   moviev.showmenu = 1
                   sccSHOW_MENU
                   RETURN, 0
	     END
	'ADJCT' : BEGIN	;color table adjustment
		 XLOADCT
	    END

	'DELETE' : BEGIN	;delete current frame
	    	 message,'Deleting frame '+trim(moviev.current),/info
                 WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
		 moviev.deleted(moviev.current) = 1
	    END

        'NEXT' : IF (ev.select NE 0) THEN BEGIN	;next frame
                 WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                 moviev.stall = 10000.

		 moviev.forward = 1

		 REPEAT BEGIN
		    moviev.current = (moviev.current + 1) MOD moviev.len
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.first
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.first
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                 WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                 if (moviev.showzoom eq 1) then begin
                    WIDGET_CONTROL, base, SET_UVALUE=moviev
		    draw_zoom
	         endif
		plot_sync
	     ENDIF
	    
        'PREV' : IF (ev.select NE 0) THEN BEGIN	;previous frame
                 WIDGET_CONTROL, moviev.base, TIMER=10000.   ; Put timer on hold
                 moviev.stall = 10000.

		 moviev.forward = 0

		 REPEAT BEGIN
		    moviev.current = (moviev.current - 1) MOD moviev.len
		    IF (moviev.current LT moviev.first) THEN moviev.current = moviev.last
		    IF (moviev.current GT moviev.last) THEN moviev.current = moviev.last
                 ENDREP UNTIL (NOT(moviev.deleted(moviev.current)))
                 WSET, moviev.draw_win
                 DEVICE, COPY=[0, 0, moviev.hsize, moviev.vsize, 0, 0, moviev.win_index(moviev.current)]
    		 WIDGET_CONTROL, moviev.cframe, SET_VALUE=STRCOMPRESS(moviev.current, /REMOVE_ALL)
    		 WIDGET_CONTROL, moviev.cname,SET_VALUE=STRCOMPRESS(moviev.frames(moviev.current),/REMOVE_ALL)
                 WIDGET_CONTROL, moviev.sliden, SET_VALUE=moviev.current
                 if (moviev.showzoom eq 1) then begin 
                    WIDGET_CONTROL, base, SET_UVALUE=moviev
		    draw_zoom
	         endif
		plot_sync
	     ENDIF

        'CONTINUOUS' : IF (ev.select NE 0) THEN BEGIN	;continuous
		 ;** set direction to saved direction
		 moviev.forward = moviev.dirsave
                 moviev.stall = moviev.stallsave
                 WIDGET_CONTROL, moviev.base, TIMER=moviev.stall   ; Request another timer event
	     ENDIF

        'FORWARD' : IF (ev.select NE 0) THEN BEGIN	;forward
		 moviev.bounce = 0
		 moviev.forward = 1
		 moviev.dirsave = 1
	     ENDIF

        'REVERSE' : IF (ev.select NE 0) THEN BEGIN	;reverse
		 moviev.bounce = 0
		 moviev.forward = 0
		 moviev.dirsave = 0
	     ENDIF

        'BOUNCE' : IF (ev.select NE 0) THEN BEGIN	;bounce
		 moviev.bounce = 1
	     ENDIF

        'PAUSE' : IF (ev.select NE 0) THEN BEGIN	;pause
		 moviev.dopause = 1
	     ENDIF

        'NO_PAUSE' : IF (ev.select NE 0) THEN BEGIN	;no pause
		 moviev.dopause = 0
	     ENDIF

        'HTSTART': BEGIN
                     sccWRITE_HT,moviev.img_hdrs(moviev.current),elong,moviev.file_hdr
                     moviev.htfile='started'
                   END

        'HTSTOP': BEGIN
                     moviev.htfile=''
                   END

	'HTPLOT': BEGIN 
	
                    IF (STRLEN(moviev.htfile) eq 0 and moviev.htfile ne 'started') THEN sXPLOT_HT $
                    ELSE BEGIN
                        sXPLOT_HT, moviev.htfile
                        moviev.htfile = ''
                    END
                  END 

	'NEWCEN': BEGIN 
                    POPUP_MSG, 'Click cursor in window to define new center for all images', /MODAL
                    CURSOR, x, y, /DEVICE, /DOWN
                    moviev.sunc(*).xcen = x
                    moviev.sunc(*).ycen = y
                  END 

	'ZOOM': BEGIN 
;                   IF ~(edit) THEN BEGIN
;		    	print,'It is necessary to use the /EDIT keyword when calling scc_wrunmoviem'
;			print,'in order for Zoom to work properly. Continuing.'
;		    ENDIF ELSE $
                        WIDGET_CONTROL, base, SET_UVALUE=moviev
		    	sccSHOW_ZOOM
                   RETURN, 0
                  END 

        'ZSCALE': BEGIN 
                   WIDGET_CONTROL, moviev.zslide, GET_VALUE=val
                   zstate.scale=val
                   WIDGET_CONTROL, base, SET_UVALUE=moviev
                   draw_zoom 
                   WIDGET_CONTROL, base, GET_UVALUE=moviev
		   END 

        'ELONG' : elong= ev.index

	'PRINT' : BEGIN	;print current frame
                 winsave=!D.WINDOW
		 WSET, moviev.win_index(moviev.current)
		 ;PRINTIT
                 img=TVRD(0,0,moviev.hsize,moviev.vsize)
		 outfile=concat_dir(getenv('HOME'),'movieframe.ps')
                 PRINT,'Saving image to ',outfile
                 SET_PLOT,'ps'
                 printer=''
                 READ,'Name of printer? ',printer
		 IF printer EQ '' THEN printdest='' ELSE printdest=['-d',printer]
                 DEVICE,FILENAME=outfile
                 coloropt=''
                 READ,'Send as 8-bit color, or black&white halftone (c/h)? ',coloropt
                 IF (STRLOWCASE(STRMID(coloropt,0,1)) eq 'c') THEN DEVICE,/COLOR,BITS=8
		 device, xoffset=0, yoffset=0, xsize=8.5, ysize=11, /inches, bits=8, /times, /bold, /portrait
		 pos = [0.1,0.0,0.8,0.0]
		 ht = pos[2] * (8.5/11.0) * float(moviev.vsize) / float(moviev.hsize)
		 pos[1] = (1.0 - ht)/2.0
		 pos[3] = ht
                 TV, img, /normal, pos[0], pos[1], xsize = pos[2], ysize = pos[3]
                 DEVICE,/CLOSE
		 cmd=['lp',printdest,outfile]
                 PRINT,cmd
                 SPAWN,cmd[where(cmd)],/NOSHELL
                 select_windows ;set_plot,'x'
                 ;PRINT,'Request sent.'
                 WSET,winsave
	     END

        'SAVE' : BEGIN	;** save movie to file
                  scc_movieout,moviev.win_index,moviev.file_hdr,hdrs=moviev.img_hdrs,filename=moviev.filename,len=moviev.len,first=moviev.first,last=moviev.last,deleted=moviev.deleted,rect=moviev.rect;  scc_movieout,base
               END

        'DEFIMGS' : BEGIN	;** save movie to file
                 if datatype(imgs) eq 'BYT' then print,'Imgs are all ready defined' else $
                       if inpfilename eq '' then print,'Movie was not read in from file can not define imgs' else $
		             define_imgs,inpfilename
               END


        'LOAD' : BEGIN	;** load movie from file
		undefine,sync_point
                 win_index = PICKFILE(filter='*.mvi,*.hdr',file=moviev.filename,/MUST_EXIST, $
                                      TITLE='Select movie file to load', $
                                      PATH=moviev.filepath, GET_PATH=path)
                 IF ((win_index EQ '') OR (win_index EQ moviev.filename)) THEN RETURN, 0
                 BREAK_FILE, win_index, a, dir, name, ext
                 moviev.filepath = dir
                 IF (dir EQ '') THEN BEGIN
                    moviev.filepath = './'
                    win_index = path+win_index
                 ENDIF
	         WIDGET_CONTROL, /DESTROY, moviev.base
	         WIDGET_CONTROL, /DESTROY, moviev.winbase
                 if edit eq 1 then SCC_WRUNMOVIEM, win_index,/editk else SCC_WRUNMOVIEM, win_index 
                 RETURN, 0
	     END

         'CHNG2EDIT' : BEGIN	;** load movie from file
	         WIDGET_CONTROL, /DESTROY, moviev.base
	         WIDGET_CONTROL, /DESTROY, moviev.winbase
                 SCC_WRUNMOVIEM, moviev.filename,/editk
                 RETURN, 0
	     END

	ELSE : BEGIN
                 PRINT, '%%sccWRUNMOVIE_BGROUPM.  Unknown event.'
	     END

    ENDCASE

   WIDGET_CONTROL, base, SET_UVALUE=moviev

    RETURN, 0

END

PRO plot_sync, reverse=reverse ;if there is a jmap sync saved, this will plot it until the remove sync button is pressed
COMMON sWIN_INDEXM
	IF(datatype(sync_point) NE 'UND') THEN BEGIN
	    	;print,sync_point
		x=sync_point[0]
		y=sync_point[1]
		IF(~keyword_set(reverse)) THEN BEGIN
			XYOUTS,x,y-1,'[(',alignment=1., /DEVICE
			XYOUTS,x,y-1,')]',alignment=0., /DEVICE,color=0
		ENDIF ELSE BEGIN
			XYOUTS,x,y-1,'[(',alignment=1., /DEVICE,color=0
			XYOUTS,x,y-1,')]',alignment=0., /DEVICE
		ENDELSE
	ENDIF
END
;----------------------- wrunmovie ---------------------------------

PRO SCC_WRUNMOVIEM, win_index, bytmin, bytmax, NAMES=names, HDRS=hdrs, START=start, LENGTH=length, $
    	PAN=scale, SUNXCEN=sunxcen, SUNYCEN=sunycen, SEC_PIX=sec_pix, DEBUG=debug, $
	PREVIOUS=previous, RECTIFIED=rectified, ARROW_CURSOR=arrow_cursor, $
        ROLL_PER_FRAME=roll_per_frame, EDITk=editk, LOWERXY=lowerxy, LOWERDN=raisedn, BIGFONT=bigfont, $
	USE_R=use_r, USE_E=use_e, USE_STPLN=use_stpln, objects=objects, file_hdr=file_hdr, filename=filename,$
	CROSSHAIR_CURSOR=crosshair_cursor, CARRINGTON=carrington, PRECOMMCORRECT=precommcorrect, $
	AVERAGE_FRAMES=AVERAGE_FRAMES,SMOOTH_BOX=SMOOTH_BOX, _EXTRA=_extra

message,'#######################################################################################',/info
message,'#############    SCC_WRUNMOVIEM is obsolete. Please use SCC_PLAYMOVIEM    #############',/info
message,'#######################################################################################',/info

return

COMMON WSCC_MKMOVIE_COMMON   ; defined above
COMMON sWIN_INDEXM   ; defined above
COMMON sALL_ROLLS    ; defined above

   debugon=keyword_set(DEBUG)
   debug_on=debugon ; for another common block
   IF keyword_set(LOWERXY) THEN lowerxyval=0. ELSE lowerxyval=1.
   IF keyword_set(LOWERDN) THEN lowerdnval=0. ELSE lowerdnval=1.
   IF keyword_set(BIGFONT) THEN usebigfont=bigfont ELSE usebigfont=0
   bkgcolor=0	; background for annotation text
   radec=0  	; flag for future use
   elong=1  	; flag for type of altitude designation; 1 = Distance from Sun is printed as Elongation in Degrees
   IF keyword_set(USE_STPLN) THEN systemstr='STPLN' ELSE systemstr=''   ; systemstr in common block

   IF XRegistered("SCC_WRUNMOVIEM") THEN BEGIN
      bell = STRING(7B)
      PRINT, bell
      print,'%%There is an instance SCC_WRUNMOVIEM already running.  Only one instance may run at a time.'
      print,'%%Recommend either exitting existing wrunmovie session or type WIDGET_CONTROL,/RESET.'
      RETURN
   ENDIF


   osczero=[-1,-1]
   edit=0
   edit = keyword_set(EDITk) or keyword_set(AVERAGE_FRAMES) or keyword_set(SMOOTH_BOX)
   IF ~(edit) THEN begin
    	print,''
	print,'Note that you may not change color table, edit or delete frames without the /EDIT keyword set.'
	print,''
   ENDIF
   if keyword_set(objects) then edit=1

   IF(datatype(bytmin) NE 'UND' AND datatype(bytmax) NE 'UND') THEN BEGIN
	bmin = bytmin
	bmax = bytmax
   ENDIF
   IF(datatype(scale) NE 'UND') THEN pan = scale

   lastdate = ''
   lasttime=''
   stall0 = 80
   stall = (100 - stall0)/50.
   stallsave = stall
   if datatype(filename) eq 'UND' then inpfilename = '' else  inpfilename = filename
   filepath = './'
   ftitle=''
   rect=0
   undefine,wcshdrs

   ;** display previous movie
   IF (KEYWORD_SET(PREVIOUS) NE 0) THEN BEGIN
      win_index = win_index2
      hdrs = hdrs2
      rolls= rolls2
   ENDIF

   ;** have user select movie file (.mvi)
   IF (DATATYPE(win_index) EQ 'UND') THEN BEGIN
      file = PICKFILE(filter='*.mvi,*.hdr', /MUST_EXIST, TITLE='Select movie file to load',GET_PATH=path)
      IF (file EQ '') THEN RETURN
      BREAK_FILE, file, a, dir, name, ext
      IF (dir EQ '') THEN win_index = path+file ELSE win_index = file
   ENDIF

    images = 0
    mviflag = 0
	
    ; ####### read movie in from movie file (.mvi)
    IF (DATATYPE(win_index) EQ 'STR') THEN BEGIN
      mviflag = 1
      BREAK_FILE, win_index, a, dir, name, ext
      ftitle = name+ext
      inpfilename = win_index
      filepath = dir
      IF (dir EQ '') THEN filepath = './'
      undefine,imgs
      if strpos(ftitle,'.hdr') gt -1 then pngmvi=inpfilename else pngmvi='0'
      OPENR,lu,inpfilename,/GET_LUN
      SCCREAD_MVI, lu, file_hdr, ihdrs, imgsin, swapflag , pngmvi=pngmvi

      IF ~keyword_set(START) THEN start=0
      ; define lastframe of current MVI file
      IF KEYWORD_SET(LENGTH) THEN $
      	lastframe=start+length-1 ELSE $
	lastframe=file_hdr.nf-1
      
      lastframe = lastframe<(file_hdr.nf-1)
      
      nf=lastframe-start+1
      ;nf = file_hdr.nf
      nx = file_hdr.nx
      ny = file_hdr.ny
      rect=file_hdr.rectified
    	mviversion=file_hdr.ver
	print,''
    	help,mviversion
	print,''
     	IF (debugon) THEN BEGIN
	    help,/str,file_hdr
	    wait,3
	ENDIF 

    ;--Check for out of order frames and omit them
    hdrs0=SCCMVIHDR2STRUCT(ihdrs[start],file_hdr.ver)
    alltai=utc2tai(anytim2utc(hdrs0.date_obs+' '+hdrs0.time_obs))
    findx=start
    if hdrs0.date_obs ne '' then begin
	xind=0
	FOR i=start+1,lastframe DO BEGIN
    	    hdri=SCCMVIHDR2STRUCT(ihdrs[i],file_hdr.ver)
	    date_obsi=hdri.date_obs+' '+hdri.time_obs
	    taii=utc2tai(anytim2utc(date_obsi))
    	    IF taii GT alltai[xind] THEN BEGIN
		hdrs0=[hdrs0,hdri]
		findx=[findx,i]
		alltai=[alltai,taii]
		xind=xind+1
	    ENDIF ELSE BEGIN
		print,'Frame',i,' ',date_obsi,' out of order'
		wait,2
	    ENDELSE
	ENDFOR
    endif else begin
    	xind=nf-1
    	findx=indgen(nf)
    	FOR i=start+1,lastframe DO hdrs0=[hdrs0,SCCMVIHDR2STRUCT(ihdrs[i],file_hdr.ver)]
    endelse
      nf1=n_elements(findx)
      IF nf1-1 NE xind THEN message,'error in findx'
      if keyword_set(AVERAGE_FRAMES) then bufsz=AVERAGE_FRAMES else  bufsz=1
      nf1=nf1/bufsz 
      win_index = INTARR(nf1)
      names = STRARR(nf1)
      frame_date=''
      aroll=0.0
      IF(edit) and file_hdr.truecolor le 0 THEN imgs = bytarr(nx, ny, nf1)  
      IF(edit) and file_hdr.truecolor gt 0 THEN imgs = bytarr(3,nx, ny, nf1)
      IF keyword_set(AVERAGE_FRAMES) and file_hdr.truecolor gt 0 THEN begin
         print,'Can not average frames of a truecolor movie:  returning'
	 return
     endif
      IF keyword_set(SMOOTH_BOX) and file_hdr.truecolor gt 0 THEN begin
         print,'Can not smooth frames of a truecolor movie:  returning'
	 return
     endif

;  -- START read each frame from MVI file
      FOR i=0,nf1-1 DO BEGIN
         WINDOW, XSIZE = nx, YSIZE = ny, /PIXMAP, /FREE
         win_index(i) = !D.WINDOW
         if (pngmvi ne '0') THEN begin  
              filein=string(imgsin(findx[i]))
	      if strpos(filein,'.png') gt -1 then imgt=read_png(filepath+string(imgsin(findx[i])),r,g,b)
	      if strpos(filein,'.gif') gt -1 then read_gif,filepath+string(imgsin(findx[i])),imgt,r,g,b
	      IF (file_hdr.truecolor EQ 0) then begin
	        imgi=imgt 
	      endif else begin
                imgi=bytarr(3,n_Elements(imgt(*,0)),n_Elements(imgt(0,*)))
                imgi(0,*,*)=r(imgt) & imgi(1,*,*)=g(imgt) & imgi(2,*,*)=b(imgt)
	      ENDELSE
         ENDIF ELSE BEGIN
	   IF (file_hdr.truecolor EQ 0) THEN imgi=imgsin[*,*,findx[i]] ELSE imgi=imgsin[*,*,*,findx[i]]
         ENDELSE
	 
;  test for A and B image based on size will be added to mvi header at some point
        if i eq 0 then begin
          if (nx eq ny*2+2)then begin
	    if (total(imgi(nx-ny-2:nx-ny-1,0:ny-1)) eq 0.0)then osczero=[nx-ny,0]
          endif
          if (ny eq nx*2+2)then begin
	    if (total(imgi(ny-nx-2:ny-nx-1,0:nx-1)) eq 0.0)then osczero=[0,ny-nx]
          endif
        endif


	 IF bufsz GT 1 THEN begin   ;if called with average_frames keyword set
             PRINT, '%%SCC_WRUNMOVIEM averaging frames ', STRING(findx[(i*bufsz)]+1,FORMAT='(I4)'), ' - ', $
	     	STRING(findx[(i*bufsz)+bufsz]+1,FORMAT='(I4)'), ' of ',STRING(nf,FORMAT='(I4)'),' from movie file ', inpfilename
	    bufim=bytarr(nx,ny,bufsz)
            for ib=0,bufsz-1 do begin
  	       bufim(*,*,ib)=imgsin[*,*,findx[(i*bufsz)+ib]]
               print,findx[(i*bufsz)+ib]
            endfor
	    image1=round(rebin(float(bufim),nx,ny,1))
         endif  else begin
             PRINT, '%%SCC_WRUNMOVIEM reading frame ', STRING(findx[i]+1,FORMAT='(I4)'), ' of ',STRING(nf,FORMAT='(I4)'), $
                ' from movie file ', inpfilename
	    if file_hdr.truecolor le 0 THEN image1=imgi else image1=imgi
         endelse
         if keyword_set(SMOOTH_BOX)then begin
   	    image1 = smooth(temporary(image1),smooth_box,/NAN)
	    print,'Applying SMOOTH() with box=',smooth_box
	 endif 
	 IF(edit) and file_hdr.truecolor le 0 THEN imgs[*,*,i] = image1
	 IF(edit) and file_hdr.truecolor gt 0 THEN imgs[*,*,*,i] = image1
         TV, image1,true=file_hdr.truecolor
         ahdr = hdrs0[(i*bufsz)]    ; hdrs0 is formed when frames are omitted due to time order
;         IF ( swapflag EQ 1 ) THEN BYTEORDER, ahdr
;         IF (i EQ 0) THEN hdrs=ahdr ELSE hdrs=[hdrs,ahdr]
         IF (i EQ 0) THEN BEGIN
           hdrs=ahdr
           rolls= aroll 
         ENDIF ELSE BEGIN
           hdrs=[hdrs,ahdr]
           rolls= [rolls,aroll]
         ENDELSE
    	IF (debugon) THEN help,/str,ahdr
      ENDFOR
; -- END  read each frame from MVI file

      names = hdrs.filename
      CLOSE,lu
      FREE_LUN,lu
    ENDIF  $ ; ######## read in MVI file
    ELSE BEGIN
        nf = n_elements(hdrs)
    	;nx = !d.xsize
    	;ny = file_hdr.ny
    	;rect=file_hdr.rectified
    	mviversion=6
    ENDELSE

   win_index2 = win_index	;** save in COMMON
   hdrs2 = hdrs	;** save in COMMON

;   IF (datatype(rolls) eq 'UND') THEN IF tag_exist(hdrs,'crota') THEN rolls = hdrs.crota else rolls=hdrs.roll 
;   rolls2= rolls
   
   ;** load movies from existing pixmaps
   WSET, win_index(0)
   hsize = !D.X_SIZE
   vsize=!D.Y_SIZE             ; see about 10 lines above  SHH 7/12/96
   ;** get length of movie

   len = N_ELEMENTS(win_index)
   if len eq 1 then stall =10000
   stallsave = stall


   frames = STRARR(len)	;** array of movie frame names (empty)
   IF (KEYWORD_SET(names) NE 0) THEN frames = names

   first = 0
   last = len-1
   current = 0
   forward = 1
   dirsave = 1
   bounce = 0
   pause = 0
   dopause = 1
   deleted = BYTARR(len)
   showmenu = 0
   showzoom = 0
   showtext = 0
   htfile = ''
    nframes= n_elements(hdrs)
    rolls= fltarr(nframes)
    frame_date=''
    ahdr=hdrs[0]
    det=ahdr.detector
    dindx=where(['C2','C3','EIT'] EQ det,issoho)
    d2indx=where(['EIT'] EQ det,iseit)
    eindx=where(['EUVI'] EQ det, iseuv)
    cindx=where(['COR2'] EQ det, iscor2)
    dindx=where(['COR1'] EQ det, iscor1)
    IF strmid(ahdr.detector,0,2) EQ 'HI' THEN ishi=1 ELSE ishi=0
    osc='A'
    IF strmid(ahdr.filename,strpos(ahdr.filename,'.f')-1,1) NE 'A' THEN osc='B'
    IF ahdr.detector EQ 'HI2' THEN ishi2=1 ELSE ishi2=0
    IF datatype(file_hdr) EQ 'STC' THEN arcs = file_hdr.sec_pix ELSE arcs=0
    IF arcs LE 0 THEN arcs=ahdr.cdelt1 
    IF arcs LE 0 and issoho THEN arcs = GET_SEC_PIXEL(ahdr, FULL=hsize) ;ELSE BEGIN
;    	arcs0=arcs
;	aorb=strmid(ahdr.filename,20,1)
;	cdelt=getsccsecpix(det,aorb,/arcsec,/fast)
;	arcs=cdelt*2048./nx
;    	message,'Invalid CDELT='+trim(arcs0)+' found; using '+trim(arcs)+' instead.',/info
;    ENDELSE
    IF arcs LT 0 THEN BEGIN
	inp=0
	print
	print,'Error: MVI platescl=',trim(arcs)
	print,'Please enter the FFV pixel size of the movie frame:'
	read, ' 2048   1024   512   256 :',inp
	farcs=getsccsecpix(ahdr.detector,osc)
	arcs=farcs*2048./inp
	IF ishi THEN arcs=arcs*3600
    ENDIF


    
    solr=ahdr.rsun	    ; no radius in file_hdr
    sunc = {sunc, xcen:0., ycen:0.}

    IF keyword_set(SUNXCEN) THEN BEGIN
    	sunc.xcen=sunxcen
	sunc.ycen=sunycen
    ENDIF 
    IF datatype(file_hdr) EQ 'STC' THEN BEGIN
    	sunc.xcen=file_hdr.sunxcen
	sunc.ycen=file_hdr.sunycen
    ENDif
    IF datatype(hdrs) EQ 'STC' and sunc.xcen eq 0 THEN BEGIN
    	sunc.xcen=hdrs(0).xcen
	sunc.ycen=hdrs(0).ycen
    ENDif

    asunc=sunc
    
;         IF (SECCHI GT 0) THEN BEGIN
;           solr = hdrs(0).rsun   ; radius of sun (arcs)
;           IF (solr EQ 0.0) THEN BEGIN
;            hee_r= GET_STEREO_LONLAT(hdrs(0).date_obs, STRMID(hdrs(0).obsrvtry,7,1), $
;                                /au, /degrees , system='HEE',_extra=_extra)
;            solr = (6.96d5 * 648d3 / !dpi / 1.496d8) / hee_r(0) ; convert from radian to arcs
;           ENDIF
;         ENDIF 
	 
    sunc = REPLICATE(sunc,nframes)
    arcs = REPLICATE(arcs,nframes)
    solr = REPLICATE(solr,nframes)

   IF len NE nframes THEN message,'pixmap and hdr mismatch'

ccdpos=file_hdr.ccd_pos
IF total(ccdpos) NE 0 THEN BEGIN
; some HI2 movies have weird values for ccd_pos; do double-check
; based on R1COL value
    print,'R1ROW, R1COL, R2ROW, R2COL:',ccdpos
    if debugon then wait,3
    IF	(ishi and osc EQ 'A' and ccdpos[1] NE 51) OR $
     	(ishi and osc EQ 'B' and ccdpos[1] NE 79) OR $
	(iscor2 and osc EQ 'A' and ccdpos[1] NE 129) or $
	(iscor2 and osc EQ 'B' and ccdpos[1] NE 1) or $
	(iscor1 and osc EQ 'B' and ccdpos[1] NE 129) or $
	(iscor1 and osc EQ 'A' and ccdpos[1] NE 1) THEN BEGIN
    	inp=''
	print,''
	print,'If this is a subfield, enter y'
	read, 'else return if this is a FFV movie: ',inp
	IF inp EQ '' THEN file_hdr.ccd_pos[*]=0
    ENDIF
ENDIF

; get all per-frame header values here
    FOR i=0, nframes-1 DO BEGIN 
    	ahdr=hdrs[i]
	ahdr.cdelt1=arcs[i]
        if strlen(ahdr.date_obs) lt 10 then print,'!!!ERROR IN MOVIE FRAME HEADER DATE-OBS MISSING!!!'
    	dateobs=strmid(ahdr.date_obs,0,10)
          ; if scc_wrunmoviem is called from scc_wrunmovie and scc_wrunmovie is
           ; called from scc_mkmovie (so input to wrunmoivem is not a
           ; .mvi file), then hdrs are .fts type hdrs (not .mvi hdrs).
           ; So, must check to see what type of hdrs we are working with:

    	IF strlen(ahdr.date_obs) GT 12 THEN begin 
	   hdrs[i].time_obs=strmid(ahdr.date_obs,11,8)
    	   hdrs[i].date_obs=strmid(ahdr.date_obs,0,10)
        endif
	asunc.xcen=ahdr.xcen
	asunc.ycen=ahdr.ycen
	asolr=ahdr.rsun
    	IF tag_exist(ahdr,'ROLL') THEN aroll=ahdr.roll ELSE begin 
	  aroll=ahdr.crota
    	  if issoho or ahdr.naxis1 ne file_hdr.nx then matchfitshdr2mvi,ahdr,file_hdr,ahdr,issoho=issoho
          tsunc = SCC_SUN_CENTER(ahdr,FULL=0)	
	  ; IDL coords
   	  asunc.xcen=tsunc.xcen
	  asunc.ycen=tsunc.ycen
       endelse
    	IF mviversion LT 6 or issoho THEN BEGIN
    	    IF KEYWORD_SET(ROLL_PER_FRAME) or (dateobs NE frame_date) THEN BEGIN
    		; get roll only 1x per day (faster)
		stop
		fhdr=mk_lasco_hdr(ahdr)
	        aroll=fhdr.crota
		asunc.xcen=fhdr.crpix1/(fhdr.naxis1/file_hdr.nx)	; account for structure defn mismatch
		asunc.ycen=fhdr.crpix2/(fhdr.naxis2/file_hdr.ny)
    	    	asolr = fhdr.rsun
    	    ENDIF ELSE BEGIN
		asolr=solr[0]
		asunc=sunc[0]
		aroll=rolls[0]
	    ENDELSE
        ENDIF

    	frame_date= dateobs
    	rolls[i]= aroll

	IF asunc.xcen GT 0 and ~keyword_set(SUNXCEN) THEN sunc[i]=  asunc
	IF asolr GT 0 THEN solr[i]=  asolr
	
	;++ Load original image headers for accurate coordinate information
	;
        mvhhdr=file_hdr
	IF ((iscor2) and keyword_set(PRECOMMCORRECT)) or $
	   (ishi) or keyword_set(CARRINGTON) or $
	   (iseuv and ~keyword_set(USE_R)) or $
	   (iseit and ~keyword_set(USE_R)) or $
	    keyword_set(objects) THEN BEGIN
	    print,'Loading WCS structure from FITS for frame ',trim(i)
	    IF tag_exist(ahdr,'CRVAL1') THEN fhdr=ahdr $
	    ELSE BEGIN
                if ~(issoho) then begin 
		  filen=ahdr.filename
                  if strmid(filen,17,1) ne 'B' and  strmid(filen,17,1) ne 'p' THEN BEGIN
		  ; omit total brightness/polarized level-1 images
		  	fname=sccfindfits(filen)
		        IF fname EQ '' THEN BEGIN
			     strput,filen,'?',16
			     fname = sccfindfits(filen)
   		        ENDIF
                  endif else fname=''
                  if fname eq '' then if datatype(missedfits) eq 'UND' then missedfits=filen else missedfits=[missedfits,filen]
		  IF (debugon) THEN print,fname
		  if fname NE  '' then jk=sccreadfits(fname,fhdr,/nodata) else fhdr=mk_short_scc_hdr(ahdr,file_hdr)
                endif else begin
		   fhdr=mk_lasco_hdr(ahdr)
		endelse
		;--correct for roll correction in MVI
		rolliszero=0
		IF aroll EQ 0 THEN BEGIN
    	    	    IF i EQ 0 THEN BEGIN
		    	print,'Setting header roll values to zero before wcs.'
		    	wait,1
		    ENDIF
		    rolliszero=1
		ENDIF
		fhdr0=fhdr
		;--Between 2007-06-01 and 2007-07-22 Level 0.5 header values of COR2 differ from
		;  more recent values by as much as 0.5 arcsec in CROTA and 1200 km farther from sun (DSUN_OBS)
		IF i EQ 0 THEN fhdrs0=fhdr0 ELSE fhdrs0=[fhdrs0,fhdr0]
		IF (iscor2) THEN BEGIN
		    cor2_point,fhdr, /NOJITTER, ROLLZERO=rolliszero, _EXTRA=_extra
		    ; Note that cor2_point sets hdr.CROTA
		    IF tag_exist(ahdr,'ROLL') THEN ahdr.roll=fhdr.crota
		    IF (debugon) THEN BEGIN
			IF i EQ 0 THEN fhdrs1=fhdr ELSE fhdrs1=[fhdrs1,fhdr]
		    	fhdr2=fhdr0
			cor2_point,fhdr2, ROLLZERO=rolliszero
			IF i EQ 0 THEN fhdrs2=fhdr2 ELSE fhdrs2=[fhdrs2,fhdr2]
		    ENDIF
		ENDIF
		IF (ishi) and strmid(ahdr.filter,0,9) ne 'Deproject' THEN hi_fix_pointing,fhdr
	        if osczero(0) gt 0 then mvhhdr.nx=(nx-2)/2
	        if osczero(1) gt 0 then mvhhdr.ny=(ny-2)/2
    	    	matchfitshdr2mvi,fhdr,mvhhdr,ahdr,issoho=issoho
    		IF (debugon) THEN BEGIN
		    fcen=scc_sun_center(fhdr)
		    IF i EQ 0 THEN newcens=fcen ELSE newcens=[newcens,fcen]
		ENDIF
	    ENDELSE
            if ishi then IF tag_exist(ahdr,'ROLL') THEN ahdr.roll = fhdr.crota
            if (total(mvhhdr.ccd_pos) eq 0) then mvhhdr.ccd_pos= [fhdr.r1row, fhdr.r1col, fhdr.r2row, fhdr.r2col]
	    IF i EQ 0 THEN wcshdrs=fitshead2wcs(fhdr) ELSE wcshdrs=[wcshdrs,fitshead2wcs(fhdr)]
	ENDIF
    ENDFOR

;;  !!!! IF A and B movie read in wcshdrs for other spacecraft used mainly for HI2 objects !!!!!!!
 	IF ((iscor2) and keyword_set(PRECOMMCORRECT)) or $
	   (ishi) or keyword_set(CARRINGTON) or $
	   (iseuv and ~keyword_set(USE_R)) or $
	    keyword_set(objects) and (osczero(0) ne -1) THEN BEGIN
 	     print,'Loading WCS structure for right side SC frame ',trim(i)
             for i=0,nf1-1 do begin
	        print,'Loading WCS structure for right side SC frame ',trim(i)
		filen=hdrs(i).filename
                if(strmid(filen,strpos(filen,'.f')-1,1) eq 'A')then osc='B' else osc='A'
                strput,filen,osc,strpos(filen,'.f')-1
                 if strmid(filen,17,1) ne 'B' and  strmid(filen,17,1) ne 'p' THEN BEGIN
		  	fname=sccfindfits(filen)
		        IF fname EQ '' THEN BEGIN
			     strput,filen,'?',16
			     fname = sccfindfits(filen)
   		        ENDIF
                 endif else fname=''
		 IF (debugon) THEN print,fname
		 if fname NE  '' then jk=sccreadfits(fname,fhdr,/nodata) else fhdr=mk_short_scc_hdr(ahdr,file_hdr)
		;--correct for roll correction in MVI
		rolliszero=0
		IF aroll EQ 0 THEN BEGIN
    	    	    IF i EQ 0 THEN BEGIN
		    	print,'Setting header roll values to zero before wcs.'
		    	wait,1
		    ENDIF
		    rolliszero=1
		ENDIF
		fhdr0=fhdr
		;--Between 2007-06-01 and 2007-07-22 Level 0.5 header values of COR2 differ from
		;  more recent values by as much as 0.5 arcsec in CROTA and 1200 km farther from sun (DSUN_OBS)
		IF i EQ 0 THEN fhdrs0=fhdr0 ELSE fhdrs0=[fhdrs0,fhdr0]
		IF (iscor2) THEN BEGIN
		    cor2_point,fhdr, /NOJITTER, ROLLZERO=rolliszero, _EXTRA=_extra
		    ; Note that cor2_point sets hdr.CROTA
		    IF tag_exist(ahdr,'ROLL') THEN ahdr.roll=fhdr.crota
		    IF (debugon) THEN BEGIN
			IF i EQ 0 THEN fhdrs1=fhdr ELSE fhdrs1=[fhdrs1,fhdr]
		    	fhdr2=fhdr0
			cor2_point,fhdr2, ROLLZERO=rolliszero
			IF i EQ 0 THEN fhdrs2=fhdr2 ELSE fhdrs2=[fhdrs2,fhdr2]
		    ENDIF
		ENDIF
                mvhhdr=file_hdr
	        if osczero(0) gt 0 then mvhhdr.nx=(nx-2)/2
	        if osczero(1) gt 0 then mvhhdr.ny=(ny-2)/2
                if ishi then IF tag_exist(ahdr,'ROLL') THEN ahdr.roll = fhdr.crota
                mvhhdr.ccd_pos = [fhdr.r1row, fhdr.r1col, fhdr.r2row, fhdr.r2col]
   	    	matchfitshdr2mvi,fhdr,mvhhdr,ahdr
	        wcshdrs=[wcshdrs,fitshead2wcs(fhdr)]
              endfor
	    endif
;; end of AB movie only wcshdr block

            objectv=[0,0,0]
            if keyword_set(objects) and ishi2 then begin
                if where(strlowcase(objects) eq 'earth') gt -1 then objectv(1)=1
                if where(strlowcase(objects) eq 'sta') gt -1 then objectv(0)=1
                if where(strlowcase(objects) eq 'stb') gt -1 then objectv(0)=1
                if where(strlowcase(objects) eq 'stereo') gt -1 then objectv(0)=1
                if where(strlowcase(objects) eq 'soho') gt -1 then objectv(2)=1
                winsave=!D.WINDOW
                  sc=strmid(hdrs(0).filename,strpos(hdrs(0).filename,'.fts')-1,1)
                  obhdrs=wcshdrs(0:n_elements(hdrs)-1)
                  objects=get_object_coords(hdrs,obhdrs,sc)
                  if sc eq 'A' then osc='B' else osc='A'
                  if n_elements(wcshdrs) eq n_Elements(hdrs)*2 then begin
                      obhdrs=wcshdrs(n_elements(hdrs):n_elements(wcshdrs)-1)
                      objectosc=get_object_coords(hdrs,obhdrs,osc)
                  endif
                   FOR i=0,n_elements(frames)-1 DO BEGIN 
                      set_plot,'z'
                      device,set_resolution=[hsize,vsize]
		      img = images[*,*,i]
                      tv,img
                      if objectv(0) eq 1 then xyouts,objects(0,i,0),objects(0,i,1),osc,charsize=1,/device
                      if objectv(1) eq 1 then xyouts,objects(1,i,0),objects(1,i,1),'E',charsize=1,/device
                      if objectv(2) eq 1 then xyouts,objects(2,i,0),objects(2,i,1),'S',charsize=1,/device
                      if n_elements(wcshdrs) eq n_Elements(hdrs)*2 then begin
                          if objectv(0) eq 1 then xyouts,objectosc(0,i,0)+osczero(0),objectosc(0,i,1)+osczero(1),sc,charsize=1,/device
                          if objectv(1) eq 1 then xyouts,objectosc(1,i,0)+osczero(0),objectosc(1,i,1)+osczero(1),'E',charsize=1,/device
                          if objectv(2) eq 1 then xyouts,objectosc(2,i,0)+osczero(0),objectosc(2,i,1)+osczero(1),'S',charsize=1,/device
                      endif
                      newimg=tvrd ()
                      select_windows ;set_plot,'x'
                      WSET,win_index(i)
	              tv, newimg
                      IF(n_elements(images) GT 1) THEN images[*,*,i] = newimg
                  endfor
                  WSET, winsave
            endif

; end per-frame header values
    IF ~keyword_set(USE_R) and (ishi) THEN BEGIN
	elong=1
	print,''
	print,'Distance from Sun is printed as Elongation in Degrees.'
    ENDIF
    IF (iseuv) or (iseit) THEN elong=2  	; display carrington lon/lat inside limb
    IF keyword_set(USE_R) THEN elong=0
    IF keyword_set(USE_E) THEN elong=1
    print,''
    help,mviversion
    print,''
    IF (iscor2) THEN BEGIN
    	jk=cor2_distortion(1,'a','0',INFO=histinfo)
	print,'Utilizing ',histinfo,' to compute Rsun.'
    ENDIF
    IF keyword_set(DEBUG) and (iscor2) and keyword_set(PRECOMMCORRECT) THEN BEGIN
    	; see how attitude params change
	!p.multi=[0,1,3,0,1]
	!x.style=1
	!y.style=0
	window,4,xsiz=400,ysiz=800
	plot,float(fhdrs1.crota-rolls),psym=-2,charsi=2,ytitle='Deg', $
	    title='Change in CROTA after cor2_point from MVI'
	!y.style=3
	plot,sqrt((newcens.xcen-sunc.xcen)^2+(newcens.ycen-sunc.ycen)^2), $
	    psym=-2,ytitle='Magnitude in Pixels', charsi=2,  $
	    title='Change in suncenter after cor2_point from MVI'
	newcen2=scc_sun_center(fhdrs2)
	plot,sqrt((fhdrs2.crval1-fhdrs1.crval1)^2+(fhdrs2.crval2-fhdrs1.crval2)^2)/fhdr.cdelt1, $
	    psym=-2,ytitle='Magnitude in Pixels',charsi=2, $
	    title='Change in suncenter with DIR defined (jitter)'
	    
	!p.multi=0
    ENDIF

    IF keyword_set(DEBUG) THEN wait,4

    WINDOW, 11, XSIZE = nx, YSIZE = ny, /PIXMAP, TITLE='scc_wrunmoviem label bkg'
    erase,bkgcolor  ; this is the color behind annotations on the image from the cursor position
    
; frame header values are over-ridden by keywords
    IF keyword_set(RECTIFIED) THEN rect=rectified
    IF KEYWORD_SET(SEC_PIX) THEN arcs[*] = sec_pix

;stop
    IF ~keyword_set(CROSSHAIR_CURSOR) THEN BEGIN
    	print,'Using arrow for cursor instead of crosshair. Position is under the edge of arrow tip.'
	device,/CURSOR_ORIGINAL   	; uses arrow pointer
    ENDIF

   IF (datatype(rolls) eq 'UND') THEN rolls=hdrs.roll 
   rolls2= rolls
  
   IF (datatype(missedfits) ne 'UND') THEN popup_msg,['Unable to locate the following fit files:',' ',missedfits],title='SCC_WRUNMOVIEM MSG'
 
;**--------------------Create Widgets-------------------------------**
hhsize = (hsize+20) > (512+20)
yscsz= vsize < 1024  ; ****KB****
xscsz= hsize < 1024  ; ****KB****
title = 'SCC_WRUNMOVIEM: '+ftitle
winbase = WIDGET_BASE(TITLE=title,/COLUMN)

    ;** create window widget to display images in
device,get_screen_size=ss

winrow = WIDGET_BASE(winbase ,/row)
if len ne 1 then tmp = CW_BGROUP(winrow,  ['Next Frame', 'Prev. Frame','Continuous','Start H-T', 'Show H-T Plot','Stop H-T','ZOOM','Menu','QUIT'], /ROW,  $
	      BUTTON_UVALUE = ['NEXT', 'PREV','CONTINUOUS','HTSTART','HTPLOT','HTSTOP','ZOOM','MENU','DONE'], EVENT_FUNCT='sccWRUNMOVIE_BGROUPM') else $
tmp = CW_BGROUP(winrow,  ['Start H-T', 'Show H-T Plot','Stop H-T','ZOOM','Menu','QUIT'], /ROW,  $
	      BUTTON_UVALUE = ['HTSTART','HTPLOT','HTSTOP','ZOOM','MENU','DONE'], EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')	      


;  vlong=['Solar radius for distance from Sun center (default for COR)',$
;    'Degrees (Elongation) for distance from Sun (default for HI)',$
;    'Carrington latitude and longitude (EUVI and EIT only)']
vlong=['Solar radii','Degrees ','Carrington','Heliographic Cartesian']

if hsize lt 1024 then begin

  winrow1 = WIDGET_BASE(winbase ,/row)
  txt=widget_label (winrow1,  value='Display Units :')
  vlongv= WIDGET_COMBOBOX(winrow1, VALUE=vlong, UVALUE="ELONG", EVENT_FUNC='sccWRUNMOVIE_BGROUPM',ysize=1)
  WIDGET_CONTROL, vlongv, SET_COMBOBOX_SELECT= elong
  WIDGET_CONTROL, vlongv, SENSITIVE=1

   mousecoords=widget_text(winrow1,value='0',uvalue='mousecoords',font='-1',xsize=48,ysize=1)

endif else begin

  txt=widget_label (winrow,  value='Display Units :')
  vlongv= WIDGET_COMBOBOX(winrow, VALUE=vlong, UVALUE="ELONG", EVENT_FUNC='sccWRUNMOVIE_BGROUPM',ysize=1)
  WIDGET_CONTROL, vlongv, SET_COMBOBOX_SELECT= elong
  WIDGET_CONTROL, vlongv, SENSITIVE=1
  mousecoords=widget_text(winrow,value='0',uvalue='mousecoords',font='-1',xsize=48,ysize=1)

endelse
if len ne 1 then begin
     winrowa = WIDGET_BASE(winbase ,/row)
     txt=widget_label (winrowa,  value='Current Frame:',xsize=85)
     sliden = WIDGET_SLIDER(winrowa,  VALUE=0, UVALUE='SLIDE_NUM', $
             /DRAG, /SCROLL, MIN=0, MAX=len-1, EVENT_FUNC='sccWRUNMOVIE_BGROUPM',xsize=xscsz-85)
endif else sliden=-1
; ******KB***** this is where the scroll bar stuff begins 

if (hsize gt 0.9*ss(0) OR vsize gt 0.9*ss(1)) AND NOT(keyword_set(FULL)) then $
    draw_w= WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, $
                       X_SCROLL_SIZE=hsize < 0.9*ss(0), $
                       Y_SCROLL_SIZE=vsize < 0.8*ss(1), $  
                       EVENT_PRO='sccWRUNMOVIE_DRAWM', /FRAME, $
                       /BUTTON_EVENTS, /MOTION_EVENTS, RETAIN=2) $ 
else $
 	draw_w = WIDGET_DRAW(winbase, XSIZE=hsize, YSIZE=vsize, EVENT_PRO='sccWRUNMOVIE_DRAWM', $
               /FRAME, /BUTTON_EVENTS,/MOTION_EVENTS, RETAIN=2)


; ******KB**** end of scroll bar stuff	

   WIDGET_CONTROL, /REAL, winbase
   WIDGET_CONTROL, draw_w, GET_VALUE=draw_win

   WIDGET_CONTROL, winbase,TLB_GET_OFFSET=tbloff,TLB_GET_SIZE=tblsize
   if tblsize(1)+tbloff(1)+185 gt ss(1) then begin
      baseyoff=ss(1)-185 
      basexoff=ss(0)-539
   endif else begin
      baseyoff=tblsize(1)+tbloff(1)
      basexoff=0
   endelse
   base = WIDGET_BASE(/COLUMN, XOFFSET=basexoff, YOFFSET=baseyoff, TITLE='SCC_WRUNMOVIEM Control')

    base1 = WIDGET_BASE(base, /ROW)
	base2 = WIDGET_BASE(base1, /COLUMN, /FRAME)
	    flabel = WIDGET_LABEL(base2, VALUE=' #   Frame')
	    base25 = WIDGET_BASE(base2, /ROW)
                cframe = WIDGET_TEXT(base25, VALUE='0', XSIZE=3, YSIZE=1, /EDITABLE, UVALUE='FRAME_NUM')
	        cname = WIDGET_TEXT(base25, VALUE=' ', XSIZE=24, YSIZE=1)
if len ne 1 then begin
	slide2 = WIDGET_SLIDER(base1, TITLE='First Frame', $
		 VALUE=0, UVALUE='SLIDE_FIRST', MIN=0, MAX=len)
	slide3 = WIDGET_SLIDER(base1, TITLE='Last Frame', $
		 VALUE=len-1, UVALUE='SLIDE_LAST', MIN=0, MAX=len-1)
	slide1 = WIDGET_SLIDER(base1, TITLE='Playback Speed', SCROLL=1, $
		 VALUE=stall0, UVALUE='SPEED', MIN=0, MAX=100, /DRAG)
endif

    base3 = WIDGET_BASE(base, /ROW)

if len ne 1 then begin
	tmp = CW_BGROUP(base3,  ['Forward', 'Reverse', 'Bounce'], /EXCLUSIVE, /COLUMN, IDS=dirb, $
	      BUTTON_UVALUE = ['FORWARD', 'REVERSE','BOUNCE'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')

    base4 = WIDGET_BASE(base3,/COLUMN)
	tmp = CW_BGROUP(base4,  ['Pause At End', 'No Pause'], /EXCLUSIVE, /COLUMN, IDS=pauseb, $
	      BUTTON_UVALUE = ['PAUSE','NO_PAUSE'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
	tmp=  CW_BGROUP(base4, ['Import Sync', 'Remove Sync', 'Reverse Sync'], /COLUMN, IDS=syncb ,/FRAME, $
	      BUTTON_UVALUE = ['Sync', 'rmv_sync','sync_color'], EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
        tmp = CW_BGROUP(base3,  ['Continuous', 'Next Frame', 'Prev. Frame','Delete Frame'], /COLUMN, $
	      BUTTON_UVALUE = ['CONTINUOUS', 'NEXT','PREV','DELETE'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
endif

    base5 = WIDGET_BASE(base3,/COLUMN)
                crop=intarr(5)
                tspot=fltarr(5)
                textbase=-1
	IF(edit) THEN BEGIN
		tmp = Widget_Button(base5,Value="Adjust Color",Event_Pro="sADJUST_CT")
                tmp=  CW_BGROUP(base5,'Smooth      ',UVALUE='SMOOTH', EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
                tmp=  CW_BGROUP(base5,'Edit Frame  ',UVALUE='EDIT', EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
                if (ishi) then tmp=  CW_BGROUP(base5,'Deproject   ',UVALUE='PROJECT', EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
                tmp=  CW_BGROUP(base5,'Draw Grid   ',UVALUE='DRAWGRID', EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
                tmp=  CW_BGROUP(base5,'Annotate    ',BUTTON_UVALUE = ['annotate'], EVENT_FUNCT='sccWRUNMOVIE_BGROUPM',/ROW)
                tmp=  CW_BGROUP(base5,['Crop   ','Go'],BUTTON_UVALUE = ['CROPMV','CROPGO'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM',/ROW)
	    if (iseuv) or (iseit) then  $
                tmp=  CW_BGROUP(base5,['Select Spot','Track'],BUTTON_UVALUE = ['TSPOT','TRACK_SPOT'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM',/ROW)


	ENDIF ELSE tmp = CW_BGROUP(base5,  ['Adjust Color', $
	    	    	    	    	    ' Draw Grid  ', $
					    ' Edit Mode  '], /COLUMN, IDS=otherb, $
		      BUTTON_UVALUE = ['ADJCT', 'DRAWGRID','CHNG2EDIT'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')

	IF(ishi2) THEN BEGIN
           base6 = WIDGET_BASE(base3,/COLUM)
               objeststr=['STEREO','Earth','SOHO']
               objectv1=[0,1,2]
               objectctl=CW_BGROUP(base6,objeststr,BUTTON_UVALUE=objectv1,uvalue='OBJECTSEL',/frame,/NONEXCLUSIVE, $
                    EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
               WIDGET_CONTROL, objectctl, SET_VALUE= objectv
               tmp=CW_BGROUP(base6,'Add Objects',BUTTON_UVALUE='OBJECT', EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
         ENDIF

	tmp = CW_BGROUP(base3,  [   'Save Movie', $
	    	    	    	    'Define Imgs' , $
				    'Load Movie  ', $
				    'Print Frame ', $
				    'Exit Control'], /COLUMN, $
	      BUTTON_UVALUE = ['SAVE','DEFIMGS', 'LOAD','PRINT','ENDCONTROL'], /FRAME, EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
  

;-------------------ZOOM window-----------------------------------------**
   WIDGET_CONTROL, winbase,TLB_GET_OFFSET=tbloff,TLB_GET_SIZE=tblsize
   if tblsize(0)+tbloff(0)+539 gt ss(0) then begin
      baseyoff=ss(1)-185 
      basexoff=ss(0)-539
   endif else begin
      baseyoff=0
      basexoff=tblsize(0)+tbloff(0)
   endelse
x_zsize=250 & y_zsize=250
sample = 1L
scale = 4L
zstate={        orig_image:	BYTARR(hsize,vsize), $
		zoom_image:	BYTARR(x_zsize,y_zsize), $
		zoom_win:	-1L, $
		x_im_sz:	hsize, $
		y_im_sz:	vsize, $
		scale:		scale, $
		sample:		sample, $
		x_zm_sz:	x_zsize, $
		y_zm_sz:	y_zsize, $
		xc:		hsize / 2L, $
		yc:		vsize / 2L, $
		x0:		0L, $
		y0:		0L, $
		x1:		0L, $
		y1:		0L, $
                timer:          0.01 $

            }

   zoombase = WIDGET_BASE(/COLUMN, XOFFSET=basexoff, YOFFSET=baseyoff, TITLE='SCC_ZOOM')
if len ne 1 then    tmp = CW_BGROUP(zoombase,  ['Next Frame', 'Prev. Frame','Continuous','Close Window'], /ROW,  $
	      BUTTON_UVALUE = ['NEXT', 'PREV','CONTINUOUS','ZOOM'], EVENT_FUNCT='sccWRUNMOVIE_BGROUPM') else $
	      tmp = CW_BGROUP(zoombase,  ['Close Window'], /ROW,  $
	      BUTTON_UVALUE = ['ZOOM'], EVENT_FUNCT='sccWRUNMOVIE_BGROUPM')
   zoom_w = WIDGET_DRAW(zoombase, /BUTTON_EVENTS, /MOTION_EVENTS, /FRAME, RETAIN = 2,  EVENT_PRO='sccWRUNMOVIE_DRAWM') 
   zslide = WIDGET_SLIDER(zoombase, TITLE='Scale Factor', VALUE=scale, UVALUE='ZSCALE', $
             /DRAG, /SCROLL, MIN=1, MAX=10, EVENT_FUNC='sccWRUNMOVIE_BGROUPM')



;**--------------------Done Creating Widgets-----------------------------**

   WIDGET_CONTROL, base, MAP=0
   WIDGET_CONTROL, /REAL, base
   WIDGET_CONTROL, zoombase, MAP=0
   WIDGET_CONTROL, /REAL, zoombase

if len ne 1 then    WIDGET_CONTROL, dirb(0), SET_BUTTON=1
if len ne 1 then    WIDGET_CONTROL, pauseb(0), SET_BUTTON=1
   ;WIDGET_CONTROL, otherb(2), SENSITIVE=0
   WIDGET_CONTROL, base, TIMER=.01

   WSET, draw_win

   IF (KEYWORD_SET(HDRS) NE 0) THEN img_hdrs = hdrs ELSE img_hdrs = 0
   moviev = { $
 		base:base, $ 
 		winbase:winbase, $ 
 		zoombase:zoombase, $
 		current:current, $ 
 		forward:forward, $ 
 		dirsave:dirsave, $ 
 		bounce:bounce, $ 
 		first:first, $ 
 		last:last, $ 
 		pause:pause, $ 
 		dopause:dopause, $ 
 		len:len, $ 
 		hsize:hsize, $ 
 		vsize:vsize, $ 
 		win_index:win_index, $ 
 		draw_win:draw_win, $ 
 		zoom_w:zoom_w, $ 
 		cframe:cframe, $ 
 		cname:cname, $ 
 		frames:frames, $ 
 		deleted:deleted, $ 
                showmenu:showmenu, $
                showzoom:showzoom, $
                filename:inpfilename, $
                filepath:filepath, $
                htfile:htfile, $	; Added by SHH 7/12/96
                sunc:sunc, $
                arcs:arcs, $
		rolls:rolls, $
                solr:solr, $
                img_hdrs:img_hdrs, $
                file_hdr:file_hdr, $
                sliden:sliden, $
 		stallsave:stallsave, $ 
 		stall:stall, $ 
		rect:rect, $		; NRich, 03.09.23
		objectv:objectv, $		
		osczero:osczero, $		
;		images:images, $  now WSCC_MKMOVIE_COMMON imgs
		zslide:zslide, $
                mousecoords:mousecoords, $
                crop:crop, $
                tspot:tspot, $
                celong:-1 , $
                annotate: 0 }

   WIDGET_CONTROL, base, SET_UVALUE=moviev
   WIDGET_CONTROL, winbase, SET_UVALUE=moviev

   XMANAGER, 'SCC_WRUNMOVIEM', base, EVENT_HANDLER='SCCWRUNMOVIE_EVENTM'
END
