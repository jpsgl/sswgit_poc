;+
;$Id: wscc_mkmovie.pro,v 1.92 2015/01/26 20:21:08 hutting Exp $
;
; Project     : SECCHI
;                   
; Name        : WSCC_MKMOVIE
;               
; Purpose     : Widget front end to SCC_MKMOVIE.PRO
;               
; Explanation : This tool allows the user to select scc_mkmovie options with
;		a widget interface.
;
; Use         : IDL> WSCC_MKMOVIE [, arg1, /debug]
;
;   	Example  : IDL> WSCC_MKMOVIE		 
;   	;Without any inputs, program will call scclister.pro
;
; Optional Inputs:
;   	ARG1	List of SECCHI FITS filenames with paths, or name of file containing such a list
;
; KEYWORDS: 	/DEBUG  Print lots of internal variable values during processing (also turns off secchi_prep /SILENT)
;               
; Calls       : SCC_MKMOVIE.PRO, SCCLISTER.PRO
;
; Side effects: None.
;               
; Category    : Image Display.  Animation.
;               
; Written     : Ed Esfandiari, NRL September 2006 - Based on wmkmovie from LASCO/EIT.
;               
; See Also    : SCC_MKMOVIE.PRO
;	
;-            
;$Log: wscc_mkmovie.pro,v $
;Revision 1.92  2015/01/26 20:21:08  hutting
;added option to adjust difference base median / image median
;
;Revision 1.91  2013/10/16 16:05:32  nathan
;cut/paste error from last edit
;
;Revision 1.90  2013/10/15 17:35:35  nathan
;use is_fits not is_tile_compressed to check for compressed AIA
;
;Revision 1.89  2013/02/12 22:53:49  nathan
;try again
;
;Revision 1.88  2013/02/12 21:05:28  nathan
;do mreadfits_tilecomp if naxis=0 (for compressed AIA files)
;
;Revision 1.87  2013/01/24 13:50:42  mcnutt
;corrected errors in crop by coord
;
;Revision 1.86  2013/01/23 17:27:09  mcnutt
;added eit to coordinate subfield if statement
;
;Revision 1.85  2012/10/10 12:37:41  mcnutt
;now works with full res AIA fits
;
;Revision 1.84  2012/09/05 14:20:13  mcnutt
;added save movie button
;
;Revision 1.83  2012/08/30 18:16:25  mcnutt
;corrected cropbycoord keyword input
;
;Revision 1.82  2012/08/22 17:59:40  mcnutt
;changed pan to work with FFV when nointerp keyword is set for crop_by_corrds
;
;Revision 1.81  2012/07/13 19:16:33  nathan
;added swmoviev.margin
;
;Revision 1.80  2012/03/01 15:28:19  mcnutt
;default to 1024X1024 for AIA
;
;Revision 1.79  2012/01/12 20:04:25  mcnutt
;sets ubmin(0) and ubmax(0) if bmin or bmax is changed
;
;Revision 1.78  2012/01/03 17:15:39  mcnutt
;added 3 byte scale options
;
;Revision 1.77  2011/12/29 18:05:09  mcnutt
;created new varibles for carrington coords
;
;Revision 1.76  2011/12/28 17:31:32  mcnutt
;removed extra if statement line 1380
;
;Revision 1.75  2011/12/27 19:11:11  mcnutt
;added crop by coord and AIA combination options
;
;Revision 1.74  2011/11/28 18:21:54  nathan
;set defaults for aia; default selminmax=1; call exit_playmovie before mkmovie
;
;Revision 1.73  2011/10/20 20:49:16  nathan
;adjust lasco bkg labels
;
;Revision 1.72  2011/10/20 18:51:31  mcnutt
;added lasco logo option
;
;Revision 1.71  2011/10/20 18:29:35  mcnutt
;added lasco background opttions
;
;Revision 1.70  2011/08/24 21:36:30  nathan
;remove wmoviev.unsharp = 0; remember norm box values
;
;Revision 1.69  2011/05/18 22:40:10  nathan
;change references to wrunmovie to playmovie
;
;Revision 1.68  2011/05/18 22:18:32  nathan
;fix exptime label on widget
;
;Revision 1.67  2011/05/18 21:34:38  nathan
;Base default bmin/bmax on exptime; change bmin/bmax if expcorr changes
;
;Revision 1.66  2011/05/02 20:14:23  nathan
;change default Unsharp/Smooth Box back to zero, and set to 25 if Sharpen Ratio or Unsharp is selected
;
;Revision 1.65  2011/04/28 19:19:03  mcnutt
;set default unsharp value to 25
;
;Revision 1.64  2011/04/28 19:10:39  mcnutt
;corrected unsharp keyword if type(3) then unsharp=vmovie.unsharp
;
;Revision 1.63  2011/04/04 22:33:10  nathan
;change how opts array is defined
;
;Revision 1.62  2011/03/22 19:42:00  mcnutt
;correct point filter for lasco and movied mask options over a column
;
;Revision 1.61  2011/03/22 19:24:05  mcnutt
;added point filter
;
;Revision 1.60  2010/10/13 19:41:27  nathan
;update widget label
;
;Revision 1.59  2010/09/14 18:59:39  mcnutt
;added coord values for lasco/eit
;
;Revision 1.58  2010/08/17 17:59:59  nathan
;fix mreadfits call
;
;Revision 1.57  2010/08/17 16:00:40  nathan
;Call convert2secchi if not secchi; remove mask from euvi defaults; fix
;default x2,y2
;
;Revision 1.56  2010/06/08 14:08:19  mcnutt
;added option to label planets
;
;Revision 1.55  2010/05/25 21:47:12  nathan
;use trim to reset bmin/bmax in widget
;
;Revision 1.54  2010/04/26 19:19:01  mcnutt
;added input values for domedian
;
;Revision 1.53  2010/04/20 15:19:37  mcnutt
;moved interactive bytscale button
;
;Revision 1.52  2010/04/19 18:39:09  mcnutt
;added nrg filter and interactive bytescale select
;
;Revision 1.51  2010/02/02 23:51:58  nathan
;tweaked some labels
;
;Revision 1.50  2010/01/22 19:48:21  mcnutt
;check to see if scc_playmovie or scc_playmoviem
;
;Revision 1.49  2010/01/14 20:06:13  mcnutt
;added frame select option for ab movies
;
;Revision 1.48  2010/01/14 13:31:19  mcnutt
;added options for other SC on top/left/bottom/right
;
;Revision 1.47  2009/09/24 19:47:25  mcnutt
;undefines textframe when called
;
;Revision 1.46  2009/09/24 19:37:45  mcnutt
;update Common block WSCC_MKMOVIE_COMMON to include textframe used in annotate_image and added clear text button
;
;Revision 1.45  2009/04/13 22:14:22  nathan
;add findx to common block to match generic_movie.pro
;
;Revision 1.44  2009/03/18 19:13:29  nathan
;Changed meaning of opts[10] from A and B use all to A and B put A on left
;(TWO=2)
;
;Revision 1.43  2009/03/12 16:39:50  mcnutt
;does not remember single between wscc_image and wscc_mkmovie
;
;Revision 1.42  2009/03/10 18:59:49  mcnutt
;works with a single image
;
;Revision 1.41  2009/03/09 17:46:19  nathan
;remember skipt, change skipt labels
;
;Revision 1.40  2008/11/05 17:29:00  thompson
;Use select_windows instead of set_plot
;
;Revision 1.39  2008-10-22 19:00:02  nathan
;fix opts for EUVI case
;
;Revision 1.38  2008/10/22 18:27:21  nathan
;added /NEWBKG option
;
;Revision 1.37  2008/10/21 17:36:31  mcnutt
;added object option to add Earth, SOHO and other stereo sc positions to HI2 movies
;
;Revision 1.36  2008/10/08 21:42:49  nathan
;fixed domedian
;
;Revision 1.35  2008/10/03 15:05:04  mcnutt
;added do median and no longer change bmin and bmax or the reference image when movie type is changed
;
;Revision 1.34  2008/09/26 19:05:53  mcnutt
;added lag variable (n previous images) in running differences movie type
;
;Revision 1.33  2008/09/22 16:54:49  nathan
;changed MEDIAN_FILTER to AVERAGE_FRAMES
;
;Revision 1.32  2008/09/22 16:10:31  nathan
;testlen depends on rdiff
;
;Revision 1.31  2008/09/18 22:01:16  nathan
;Added hooks for rdifft but left off widget; added Smooth option in Unsharp
;Box space; added 'Use skipped frames' option for combining frames
;
;Revision 1.30  2008/08/25 17:02:57  nathan
;Split up Calibrate option to separate CALIMG and CALFAC
;
;Revision 1.29  2008/08/20 17:58:05  nathan
;Moved pb Series on widget
;
;Revision 1.28  2008/08/20 17:16:12  mcnutt
;added COR pB series options, polarized brightness, polarization angle and percent
;
;Revision 1.27  2008/08/08 23:20:16  nathan
;Only allow input from one spacecraft
;
;Revision 1.26  2008/06/17 14:57:21  nathan
;make sure use_model=0 if movie type straight
;
;Revision 1.25  2008/05/28 15:55:22  mcnutt
;selects files which are at least 2 minutes apart for COR TB movies
;
;Revision 1.24  2008/05/22 18:34:14  mcnutt
;added total brightness option for COR1 and COR2
;
;Revision 1.23  2008/05/20 19:14:58  nathan
;extend WSCC_MKMOVIE_COMMON
;
;Revision 1.22  2008/05/09 21:31:58  nathan
;IF type=running_diff, then set basefpd=0 (first frame)
;
;Revision 1.21  2008/04/29 15:50:30  nathan
;added skip widget
;
;Revision 1.20  2008/04/15 20:25:50  nathan
;added imgs to wscc_mkmovie_common (for scc_wrunmovie.pro)
;
;Revision 1.19  2008/04/14 15:59:24  nathan
;added daily_med option for use_model
;
;Revision 1.18  2008/04/04 18:56:04  nathan
;Added /TWO (match A&B) option; fixed fillcol
;
;Revision 1.17  2008/04/02 16:03:14  nathan
;changed order of scc_mkmovie args
;
;Revision 1.16  2008/03/11 19:39:27  nathan
;Updated header info
;
;Revision 1.15  2008/02/27 17:00:40  mcnutt
;removed keyword wsccmv in call to scc_mkmovie
;
;Revision 1.14  2008/02/27 14:17:00  mcnutt
;get coords from scc_mkframe if getsubfield is selected and added clear subfield button
;
;Revision 1.13  2008/02/26 16:56:09  mcnutt
;added interactive subfield button and set plot x incase scc_mkframe died
;
;Revision 1.12  2008/02/13 17:24:25  mcnutt
;works with new scc_mkframe
;
;Revision 1.11  2008/02/12 19:42:35  nathan
;Fixed FILL_COL keyword and updated labeling
;
;Revision 1.10  2008/02/08 19:35:31  mcnutt
;added automax and docolor option, presets bnim and max based on tel
;
;Revision 1.9  2008/01/24 17:39:01  nathan
;fix LOGO setting in mkmovie call
;
;Revision 1.8  2008/01/23 15:50:11  nathan
;added xregister check
;
;Revision 1.7  2008/01/22 21:24:25  nathan
;Put OUTER and LIMB in opts array; added ROTATE_ON option in opts array;
;set default coords according to first input image
;
;Revision 1.6  2008/01/15 14:48:23  mcnutt
;added outer mask option
;
;Revision 1.5  2008/01/11 19:40:56  mcnutt
;added solar limb option to set keyword outer in mkframe
;
;Revision 1.4  2008/01/11 16:33:32  nathan
;Permanently moved from dev/movie to secchi/idl/display/movie
;
;Revision 1.3  2008/01/11 15:24:13  mcnutt 
;(in display/movie, edit from rev 1.3 in dev/movie)
;Added keyword for using mask save file and now allows the list from scclister as input
;
;Revision 1.5  2007/12/10 22:28:41  nathan
;Implemented subfields; added /DEBUG
;
;Revision 1.4  2007/12/06 21:05:57  nathan
;Added input for timestamp size; some line reformatting and tidying up
;
;Revision 1.3  2007/10/05 21:06:57  nathan
;Major update for SECCHI, using secchi_prep, and making sure options work
;correctly. Still TBD: image subfields, optimize masks, etc.
;
;Revision 1.2  2007/03/28 19:42:17  esfand
;secchi movie routines as of Mar 28, 07
;
;Revision 1.1  2006/10/05 20:56:55  esfand
;first virsion - aee
;
;
;____________________________________________________________________________

PRO WSCC_MKMOVIE_EVENT, ev

COMMON WSCC_MKMOVIE_COMMON, swmoviev, debug_on, rgb, imgs, coordsab, findx, textframe

WIDGET_CONTROL, ev.top, GET_UVALUE=wmoviev   ; get structure from UVALUE

    WIDGET_CONTROL, ev.id, GET_UVALUE=input

IF debug_on THEN help,input
    CASE (1) OF

	input EQ 'CANCEL' : BEGIN       ;** exit program
	         WIDGET_CONTROL, /DESTROY, wmoviev.base
                 RETURN
	     END

 	input EQ 'CLRTXT' : BEGIN
                    undefine,textframe
	    END

	(input EQ 'DONE') OR (input EQ 'TEST') OR (input EQ 'SAVEMV') : BEGIN       ;** call scc_mkmovie
                 if wmoviev.type(2) gt 0 then begin
	           WIDGET_CONTROL, wmoviev.rdifft, GET_VALUE=val & rdiff = fix(val(0))
                 endif else rdiff=0
                 wmoviev.rdiff = rdiff
	         WIDGET_CONTROL, wmoviev.skipt, GET_VALUE=val & skip = (fix(val(0)))>1 
		 wmoviev.skip=skip
		 dnoise=1
		 IF wmoviev.dnoise GT 0 THEN BEGIN
		    dnoise=skip
		    skip=1
		 ENDIF
	         WIDGET_CONTROL, wmoviev.dtszt, GET_VALUE=val & dtsz = FLOAT(val(0))
                 wmoviev.dtsz = dtsz
	         WIDGET_CONTROL, wmoviev.bmint, GET_VALUE=val & bmin = FLOAT(val(0))
                 wmoviev.bmin = bmin
                 wmoviev.ubmin(0) = bmin
	         WIDGET_CONTROL, wmoviev.bmaxt, GET_VALUE=val & bmax = FLOAT(val(0))
                 wmoviev.bmax = bmax
                 wmoviev.ubmax(0) = bmax
	         WIDGET_CONTROL, wmoviev.unst, GET_VALUE=val & uns = FLOAT(val(0))
                 wmoviev.unsharp = uns
	         WIDGET_CONTROL, wmoviev.colt, GET_VALUE=val & col = FIX(val(0))
                 IF wmoviev.x1t ne -1 THEN BEGIN
	           WIDGET_CONTROL, wmoviev.x1t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.x1 = val
	           WIDGET_CONTROL, wmoviev.x2t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.x2 = val
	           WIDGET_CONTROL, wmoviev.y1t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.y1 = val
	           WIDGET_CONTROL, wmoviev.y2t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.y2 = val
                 ENDIF
                 if wmoviev.coordsub eq 1 then coordsubvals=[wmoviev.carx1,wmoviev.carx2,wmoviev.cary1,wmoviev.cary2,wmoviev.coordsize,wmoviev.nointerp] else coordsubvals = 0
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 1 then coordsubvals=[wmoviev.carx1,wmoviev.cary1,wmoviev.coordsize,wmoviev.nointerp]
		 WIDGET_CONTROL, wmoviev.bsub, GET_VALUE=val & wmoviev.gsub = val
 		 if wmoviev.bbox ne -1 then begin
	            WIDGET_CONTROL, wmoviev.bx1t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.bx1 = val
	            WIDGET_CONTROL, wmoviev.bx2t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.bx2 = val
	            WIDGET_CONTROL, wmoviev.by1t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.by1 = val
	            WIDGET_CONTROL, wmoviev.by2t, GET_VALUE=val & val = FIX(val(0)) & wmoviev.by2 = val
		 endif
		 WIDGET_CONTROL, wmoviev.bselminmax, GET_VALUE=val & wmoviev.selminmax = val
                 swmoviev = wmoviev 
		 outfile=0
	        IF (input EQ 'SAVEMV') THEN BEGIN
	            ofn=''
  	          ofp=''
  	          thdr={filename:'', detector:'ssuff',date_obs:''}
		  moviev={filename:ofp}
  	          outmovie=''
  	          scc_movieout,0,'',hdrs=thdr,filename=ofn,len=n_elements(wmoviev.files),first=0, $
  	          	    last=n_elements(wmoviev.files)-1, deleted=intarr(n_elements(wmoviev.files))+0, rect=0, /selectonly ,outmovie=outmovie;scc_movieout,moviev.base ,/selectonly
  	          IF (outmovie.outfile ne moviev.filename and outmovie.outfile ne '') THEN BEGIN
	        	outfile=outmovie.outfile
   	                BREAK_FILE, ofn, a, dir1, name, ext
	        	spawn,['mkdir',dir1],/NOSHELL
  	          ENDIF else begin
	        	print,'******************************************'
	        	print,'*****  Movie filename not selected   *****'
	        	print,'******************************************'
	        	RETURN
  	          ENDELSE
		ENDIF

	        IF (input EQ 'DONE') OR (input EQ 'SAVEMV') THEN WIDGET_CONTROL, /DESTROY, wmoviev.base
		
    	    	IF (input EQ 'DONE') OR (input EQ 'SAVEMV') THEN exit_playmovie    ; in case test movie still running

                 ;IF (wmoviev.type(3) EQ 1) OR (wmoviev.type(5) EQ 1) THEN uns = wmoviev.unsharp ELSE uns = 0 
                 IF (wmoviev.scale EQ 1) THEN ss = 1 ELSE ss = 0 
                 IF (wmoviev.scale EQ 2) THEN ls = 1 ELSE ls = 0 
                 IF (wmoviev.scale EQ 3) THEN nrfg = 1 ELSE nrfg = 0 

                 IF (wmoviev.opts(1) EQ 1) THEN nn = 0 ELSE nn = 1 
                 IF (wmoviev.opts(2) EQ 1) THEN ns = 0 ELSE ns = 1 
                 ir = [wmoviev.hdr0.naxis1,wmoviev.hdr0.naxis2]
                 ;panarr = [0.25, 0.5, 1.0, 2.0, 4.0]
                 panarr = [128,256,512,1024,2048,4096]
                 pan = panarr(wmoviev.pan)

                 IF (wmoviev.occ EQ 1) THEN fmo = 1 ELSE fmo = 0 
                 IF (wmoviev.occ EQ 2) THEN mo = 1 ELSE mo = 0
                 IF (wmoviev.occ EQ 3) THEN lmo = 1 ELSE lmo = 0 

                 IF (wmoviev.gap EQ 1) THEN fg = 1 ELSE $
                 IF (wmoviev.gap EQ 2) THEN fg = 2 ELSE fg = 0 
    	    	 um=0
                 IF (wmoviev.basef GT 0) THEN um = wmoviev.basef
;                 IF (wmoviev.opts4(0) EQ 1) AND wmoviev.hdr0.detector EQ 'EIT' $
;			THEN um = 0 		;** don't allow use model if degrid chosen w/ EIT

                 IF (wmoviev.box(0) EQ 1) THEN box = [wmoviev.bx1,wmoviev.bx2,wmoviev.by1,wmoviev.by2] ELSE box=0

                 ;IF (wmoviev.type(4) EQ 1) OR (wmoviev.type(6) EQ 1) THEN do_ratio = 1 ELSE do_ratio = 0
                 IF (wmoviev.type(4) EQ 1) OR (wmoviev.type(5) EQ 1) THEN do_ratio = 1 ELSE do_ratio = 0
    	    	 ;--Straight movie, no model
		 IF (wmoviev.type[0] EQ 1) or (rdiff GT 0) THEN um=0
		 
;		 IF wmoviev.ddomed GT 0 THEN BEGIN
;		    medval=uns
;		    medval=uns
;		    uns=0
;		 ENDIF ELSE medval=0
		 
                 if wmoviev.pbs eq 1 then begin   
		 ;use 2 minute cadence to select only one polarizer position for TB movies
                   st1=strmid(wmoviev.files,strpos(wmoviev.files(0),'.f')-21,15)
                   t1=(strmid(st1,9,2)*60l*60l)+(strmid(st1,11,2)*60l)+(strmid(st1,13,2))
 		   tempfiles=wmoviev.files([0,where(t1-shift(t1,1) gt 120)])
		 endif ELSE tempfiles=wmoviev.files
		 
		 len=n_elements(tempfiles)
		 newlen=ceil(float(len)/skip)
    	    	 subs=skip*indgen(newlen)
                 testlen = ((3*dnoise) < newlen)+rdiff
		 tempfiles=tempfiles[subs]
	         IF (input EQ 'TEST') THEN $
                    files=tempfiles[0:testlen-1] $
                 ELSE $
                    files=tempfiles

                 tmp= [1,0]
                 calval= tmp(wmoviev.opts(3)) ; changes 0 to 1 or 1 to 0. 

		 if wmoviev.both gt 0 then twoval=wmoviev.both+(wmoviev.fboth*10) else twoval=0
                 
                 fillcol=col+1
		 swmoviev = wmoviev

                if wmoviev.type[3] eq 1 then unsharp=wmoviev.unsharp else unsharp=0
;		help,debug_on
		IF debug_on THEN help,uns,calval,nn,ns,do_ration,pan,um,mo,lmo, $
		    	fg,ls,ss,box,fillcol,skip,files,rdiff,medval
		IF debug_on THEN wait,2
	
                 SCC_MKMOVIE, files, wmoviev.bmin, wmoviev.bmax,wmoviev.cam, DIFF=wmoviev.type(1), VERBOSE=debug_on, $
         	; RUNNING_DIFF=rdiff, UNSHARP=wmoviev.type[3], TIMES=dtsz*wmoviev.opts(0), $
         	 RUNNING_DIFF=rdiff, UNSHARP=unsharp, TIMES=dtsz*wmoviev.opts(0), $
 		 NOCAL=calval,LOGO=wmoviev.opts[6], NOEXPCORR=nn, NO_SORT=ns, RATIO=do_ratio, $
                 PAN=pan, OUTER=wmoviev.opts[8], LIMB=wmoviev.opts[7], DOROTATE=wmoviev.opts[5], $
                 USE_MODEL = um, MASK_OCC=mo, LG_MASK_OCC=lmo, FIXGAPS=fg, AVERAGE_FRAMES=dnoise, $
		 LOG_SCL=ls, SQRT_SCL=ss, TWO=twoval, PHOTOCAL=wmoviev.opts[4], SMOOTH_BOX=uns, $
 		 FILL_COL=fillcol, COORDS=[wmoviev.x1,wmoviev.x2,wmoviev.y1,wmoviev.y2], BOX=box, GETSUBFIELD=wmoviev.gsub,  $
                 USE_SHARP=wmoviev.type(5), SHARP_FACTOR=wmoviev.fact, NEWBKG=wmoviev.newbkgval, $
                 FMASK=fmo, AUTOMAX=wmoviev.auto, DCOLOR=wmoviev.dcolor,DOMEDIAN=wmoviev.ddomed, OBJECTS=wmoviev.opts[9], $
		 PLANETS=wmoviev.opts[10], DONRFG=nrfg,SELECTBMIN=wmoviev.selminmax,PFILTER=wmoviev.opts[11],coordcrop=coordsubvals, $
		 SAVE=outfile,norm_diff=wmoviev.opts[12]

;the following is now being set on scc_mkframe if called though widget
;   set x1,x2,y1,y2 values 
;	         if (swmoviev.gsub eq 1) and input EQ 'TEST' then begin
;                    swmoviev.gsub=0
;		    WIDGET_CONTROL, wmoviev.bsub, SET_VALUE=swmoviev.gsub	    
;                    if swmoviev.coordsub eq 1 then begin
;		       WIDGET_CONTROL, wmoviev.carx1t, SET_VALUE=swmoviev.carx1	    
;		       WIDGET_CONTROL, wmoviev.carx2t, SET_VALUE=swmoviev.carx2	    
;		       WIDGET_CONTROL, wmoviev.cary1t, SET_VALUE=swmoviev.cary1	    
;		       WIDGET_CONTROL, wmoviev.cary2t, SET_VALUE=swmoviev.cary2	    
;	               for i=0,2 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=swmoviev.coordsize(i)
;		    endif else begin
;		       WIDGET_CONTROL, wmoviev.x1t, SET_VALUE=swmoviev.x1	    
;		       WIDGET_CONTROL, wmoviev.x2t, SET_VALUE=swmoviev.x2	    
;		       WIDGET_CONTROL, wmoviev.y1t, SET_VALUE=swmoviev.y1	    
;		       WIDGET_CONTROL, wmoviev.y2t, SET_VALUE=swmoviev.y2	    
;		    endelse
;		    wmoviev=swmoviev
;		  ENDIF

;   set bmin and bmax values values 
;	         if (swmoviev.selminmax eq 1) and input EQ 'TEST' then begin
;                    swmoviev.selminmax=0
;		    WIDGET_CONTROL, wmoviev.bselminmax, SET_VALUE=swmoviev.selminmax	    
;		    WIDGET_CONTROL, wmoviev.bmint, SET_VALUE=trim(swmoviev.bmin)	    
;		    WIDGET_CONTROL, wmoviev.bmaxt, SET_VALUE=trim(swmoviev.bmax)	    
;		  ENDIF
;		    wmoviev.ubmin=swmoviev.ubmin
;		    wmoviev.ubmax=swmoviev.ubmax

                    wmoviev=swmoviev
                    IF (input EQ 'TEST') THEN WIDGET_CONTROL, wmoviev.base, SET_UVALUE=wmoviev

		  RETURN
	     END

        input EQ 'DTSZT' : BEGIN		;bytscl min
	         WIDGET_CONTROL, wmoviev.dtszt, GET_VALUE=anumber
                 dtsz = FLOAT(anumber(0))
                 wmoviev.dtsz = dtsz
	    END

        input EQ 'BMINT' : BEGIN		;bytscl min
	         WIDGET_CONTROL, wmoviev.bmint, GET_VALUE=min_text
                 bmin = FLOAT(min_text(0))
                 wmoviev.bmin = bmin
                 wmoviev.ubmin(0) = bmin
	    END

	input EQ 'BMAXT' : BEGIN 	;bytscl max
	         WIDGET_CONTROL, wmoviev.bmaxt, GET_VALUE=max_text
                 bmax = FLOAT(max_text(0))
                 wmoviev.bmax = bmax
                 wmoviev.ubmax(0) = bmax
	    END


	input EQ 'UNST' : BEGIN 	;unsharp box size
	         WIDGET_CONTROL, wmoviev.unst, GET_VALUE=uns_text
                 uns = FLOAT(uns_text(0))
                 wmoviev.unsharp = uns
	    END

	input EQ 'RDIFFT' : BEGIN 	;lag for running difference
	        WIDGET_CONTROL, wmoviev.rdifft, GET_VALUE=val
                wmoviev.rdiff = fix(val[0])
	    END

	input EQ 'SKIPT' : BEGIN 	;skip frames
	         WIDGET_CONTROL, wmoviev.skipt, GET_VALUE=skip_text
                 wmoviev.skip = fix(skip_text[0])
	    END

	input EQ 'XLOADCT' : BEGIN
                 XLOADCT
                 RETURN
	     END

	input EQ 'PAN' : BEGIN
                 wmoviev.pan = ev.index
	     END

	input EQ 'SCALE' : BEGIN	
                 wmoviev.scale = ev.index
	    END

	input EQ 'OCC' : BEGIN	
                 wmoviev.occ = ev.index
	    END
	input EQ 'LIMB' : BEGIN	
                 wmoviev.limb = ev.index
	    END

	input EQ 'OUTER' : BEGIN	
                 wmoviev.outer = ev.index
	    END

	input EQ 'GAP' : BEGIN	
                 wmoviev.gap = ev.index
	    END

	input EQ 'BASEF' : BEGIN	
                 wmoviev.basef = ev.index
                 ;if moviev.single eq 1 and wmoviev.len
	    END
	input EQ 'ddomedv' :  BEGIN
	         if ev.index ne 0 then wmoviev.ddomed = fix(wmoviev.medval(ev.index)) else wmoviev.ddomed=0
 	     END

	input EQ 'COLT' : BEGIN
	         WIDGET_CONTROL, wmoviev.colt, GET_VALUE=col_text
                 col = FIX(col_text[0])
	         ;WIDGET_CONTROL, wmoviev.colt, SET_VALUE=STRTRIM(FIX(col),2)
                 wmoviev.col = col
	     END

	input EQ 'FACT' : BEGIN
	         WIDGET_CONTROL, wmoviev.factt, GET_VALUE=text
                 fact = FLOAT(text(0))
	         WIDGET_CONTROL, wmoviev.factt, SET_VALUE=STRTRIM(fact,2)
                 wmoviev.fact = fact
	     END

	input EQ 'X1T' : BEGIN
	         WIDGET_CONTROL, wmoviev.x1t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.x1 = val
	     END
	input EQ 'X2T' : BEGIN
	         WIDGET_CONTROL, wmoviev.x2t, GET_VALUE=val
                   val = FIX(val(0))
                   wmoviev.x2 = val
	     END
	input EQ 'Y1T' : BEGIN
	         WIDGET_CONTROL, wmoviev.y1t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.y1 = val
	     END
	input EQ 'Y2T' : BEGIN
	         WIDGET_CONTROL, wmoviev.y2t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.y2 = val
	     END
	input EQ 'CARX1T' : BEGIN
	         WIDGET_CONTROL, wmoviev.carx1t, GET_VALUE=val
                   val = float(val(0))
                  wmoviev.carx1 = val
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   pscale=wmoviev.coordsize(0)
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   wmoviev.coordsize=[pscale,fix(((wmoviev.carx2+c360)-wmoviev.carx1)/pscale),fix((wmoviev.cary2-wmoviev.cary1)/pscale)]
	           for i=0,2 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=wmoviev.coordsize(i)
		 endif
	     END
	input EQ 'CARX2T' : BEGIN
	         WIDGET_CONTROL, wmoviev.carx2t, GET_VALUE=val
                   val = float(val(0))
                   wmoviev.carx2 = val
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   pscale=wmoviev.coordsize(0)
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   wmoviev.coordsize=[pscale,fix(((wmoviev.carx2+c360)-wmoviev.carx1)/pscale),fix((wmoviev.cary2-wmoviev.cary1)/pscale)]
	           for i=0,2 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=wmoviev.coordsize(i)
		 endif
	     END
	input EQ 'CARY1T' : BEGIN
	         WIDGET_CONTROL, wmoviev.cary1t, GET_VALUE=val
                   val = float(val(0))
                   wmoviev.cary1 = val
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   pscale=wmoviev.coordsize(0)
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   wmoviev.coordsize=[pscale,fix(((wmoviev.carx2+c360)-wmoviev.carx1)/pscale),fix((wmoviev.cary2-wmoviev.cary1)/pscale)]
	           for i=0,2 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=wmoviev.coordsize(i)
		 endif
	     END
	input EQ 'CARY2T' : BEGIN
	         WIDGET_CONTROL, wmoviev.cary2t, GET_VALUE=val
		 val = float(val(0))
                 wmoviev.y2 = val
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   pscale=wmoviev.coordsize(0)
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   wmoviev.coordsize=[pscale,fix(((wmoviev.carx2+c360)-wmoviev.carx1)/pscale),fix((wmoviev.cary2-wmoviev.cary1)/pscale)]
	           for i=0,2 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=wmoviev.coordsize(i)
		 endif
	     END
	input EQ 'COORDPSIZE' : BEGIN
	         WIDGET_CONTROL, wmoviev.coordw(0), GET_VALUE=val
		 val=float(val)
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   pscale=val
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   wmoviev.coordsize=[pscale,fix(((wmoviev.carx2+c360)-wmoviev.carx1)/pscale),fix((wmoviev.cary2-wmoviev.cary1)/pscale)]
	           for i=1,2 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=wmoviev.coordsize(i)
                 endif else wmoviev.coordsize(0)=val
	     END
	input EQ 'COORDXSIZE' : BEGIN
	         WIDGET_CONTROL, wmoviev.coordw(1), GET_VALUE=val
		 val=float(val)
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   pscale=((wmoviev.carx2+c360)-wmoviev.carx1)/val
                   wmoviev.coordsize=[pscale,val,fix((wmoviev.cary2-wmoviev.cary1)/pscale)]
	           i2=[0,2]
		   for i=0,i do WIDGET_CONTROL, wmoviev.coordw(i2(i)), SET_VALUE=wmoviev.coordsize(i2(i))
                 endif else wmoviev.coordsize(1)=fix(val)
	     END
	input EQ 'COORDYSIZE' : BEGIN
	         WIDGET_CONTROL, wmoviev.coordw(2), GET_VALUE=val
		 val=float(val)
                 if wmoviev.coordsub eq 1 and wmoviev.coordcenter eq 0 then begin
                   pscale=(wmoviev.cary2-wmoviev.cary1)/val
                   if wmoviev.carx1 gt wmoviev.carx2 then c360=360 else c360=0
                   wmoviev.coordsize=[pscale,fix(((wmoviev.carx2+c360)-wmoviev.carx1)/pscale),val]
	           for i=0,1 do WIDGET_CONTROL, wmoviev.coordw(i), SET_VALUE=wmoviev.coordsize(i)
                 endif else wmoviev.coordsize(2)=fix(val)
	     END
	input EQ 'BX1' : BEGIN
	         WIDGET_CONTROL, wmoviev.bx1t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.bx1 = val
	     END
	input EQ 'BX2' : BEGIN
	         WIDGET_CONTROL, wmoviev.bx2t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.bx2 = val
	     END
	input EQ 'BY1' : BEGIN
	         WIDGET_CONTROL, wmoviev.by1t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.by1 = val
	     END
	input EQ 'BY2' : BEGIN
	         WIDGET_CONTROL, wmoviev.by2t, GET_VALUE=val
                 val = FIX(val(0))
                 wmoviev.by2 = val
	     END
	input EQ 'pbsv' :   BEGIN
		 WIDGET_CONTROL, wmoviev.pbsv, GET_VALUE=val
                 if wmoviev.hdr0.detector eq 'COR1' or wmoviev.hdr0.detector eq 'COR2' then begin
                   if wmoviev.hdr0.detector eq 'COR1' then begin
                     ind=where(wmoviev.type eq 1)
	   	     if ind(0) ge 4 then begin
                       print,'Ratio movie can not be created from COR1 Total Brightness images'
		       WIDGET_CONTROL, wmoviev.btype, SET_VALUE=0;                   WIDGET_CONTROL, wmoviev.pbsv, SET_VALUE=val
		       wmoviev.type(*) = 0
		       wmoviev.type(0) = 1
		      endif
		   endif
		   wmoviev.pbs = val
                 endif else begin
		   wmoviev.pbs = 0
                   WIDGET_CONTROL, wmoviev.pbsv, SET_VALUE=0
                 endelse
 	     END
	input EQ 'bothv' : wmoviev.both = ev.index
	input EQ 'fbothv' : wmoviev.fboth = ev.index

	ELSE : BEGIN
                 PRINT, '%%WSCC_MKMOVIE_EVENT.  Unknown event.'
	     END

    ENDCASE

  WIDGET_CONTROL, ev.top, SET_UVALUE=wmoviev

END



FUNCTION WSCC_MKMOVIE_BGROUP, ev, _EXTRA=_extra

COMMON WSCC_MKMOVIE_COMMON

   WIDGET_CONTROL, ev.top, GET_UVALUE=wmoviev   ; get structure from UVALUE

   input = ev.value

IF debug_on THEN help,input
    CASE (input) OF

	'TYPE' : BEGIN
		 WIDGET_CONTROL, wmoviev.btype, GET_VALUE=val
                 ind=where(wmoviev.type eq 1)
		 wmoviev.type(*) = 0
		 wmoviev.type(val) = 1
		 ;IF ((wmoviev.type(4) EQ 1) OR (wmoviev.type(6) EQ 1) ) THEN BEGIN ;** ratio or unsharp ratio
		 IF ((wmoviev.type(4) EQ 1) OR (wmoviev.type(5) EQ 1) ) THEN BEGIN ;** ratio or unsharp ratio
                  ;  IF wmoviev.bmin LT 0 or wmoviev.bmin GT 1 THEN wmoviev.bmin = 0.5
                  ;  IF wmoviev.bmax LT 1 or wmoviev.bmax GT 10 THEN wmoviev.bmax = 1.5
                    if wmoviev.hdr0.detector eq 'COR1' and wmoviev.pbs gt 1 then begin
                       print,'Ratio movie can not be created from COR1 Total Brightness images'
		       WIDGET_CONTROL, wmoviev.btype, SET_VALUE=ind(0);                   WIDGET_CONTROL, wmoviev.pbsv, SET_VALUE=val
		       wmoviev.type(*) = 0
		       wmoviev.type(ind(0)) = 1
		    endif
		 ENDIF 			;** other
		 IF (wmoviev.type(2) EQ 1 and wmoviev.single eq 1) THEN BEGIN ;**no running difference for single images
                       print,'Can not do a running difference for single image'
		       WIDGET_CONTROL, wmoviev.btype, SET_VALUE=ind(0);                   WIDGET_CONTROL, wmoviev.pbsv, SET_VALUE=val
		       wmoviev.type(*) = 0
		       wmoviev.type(ind(0)) = 1
		 ENDIF
		 IF ((wmoviev.type(3) EQ 1) OR (wmoviev.type(5) EQ 1) ) THEN BEGIN ;** sharpen ratio or unsharp ratio
                    wmoviev.unsharp = (wmoviev.unsharp)>25
		    message,'Setting Unsharp/Smooth Box to default value for type: '+trim(wmoviev.unsharp),/info
		 ENDIF ELSE BEGIN		;** other
		    wmoviev.unsharp = 0
    	    	 ENDELSE
	         WIDGET_CONTROL, wmoviev.unst, SET_VALUE=STRTRIM(wmoviev.unsharp,2)
		 
		 IF wmoviev.type(2) EQ 1 THEN begin
		    wmoviev.basef = 0
	            WIDGET_CONTROL, wmoviev.rdifft, GET_VALUE=val & rdiff = fix(val(0))
                    if rdiff eq 0 then rdiff=1
		    WIDGET_CONTROL, wmoviev.rdifft, SET_VALUE=trim(rdiff)
		 endif
		 WIDGET_CONTROL, wmoviev.basefpd, SET_VALUE=wmoviev.basef
;	         WIDGET_CONTROL, wmoviev.bmint, SET_VALUE=STRTRIM(wmoviev.bmin,2)
;	         WIDGET_CONTROL, wmoviev.bmaxt, SET_VALUE=STRTRIM(wmoviev.bmax,2)
	        END

	'OPT' : BEGIN	
		 expopt=wmoviev.opts[1]
		 WIDGET_CONTROL, wmoviev.bopt, GET_VALUE=val
		 WIDGET_CONTROL, wmoviev.dtszt, SET_VALUE=trim(wmoviev.dtsz)
;help,val,help,wmoviev.opts
IF debug_on THEN print,'BOPT val=',val
		 wmoviev.opts = val
		 doexpcor=val[1]
		 IF doexpcor NE expopt THEN BEGIN
		    print,'EXPTIME=',wmoviev.hdr0.exptime
		    ; actually change bmin, bmax values
		    IF doexpcor THEN fac=1/wmoviev.hdr0.exptime ELSE fac=wmoviev.hdr0.exptime
		    wmoviev.bmin=wmoviev.bmin*fac
		    wmoviev.bmax=wmoviev.bmax*fac
		    wmoviev.ubmin(0)=wmoviev.bmin*fac
		    wmoviev.ubmax(0)=wmoviev.bmax*fac
		    WIDGET_CONTROL, wmoviev.bmint, SET_VALUE=trim(wmoviev.bmin)	    
		    WIDGET_CONTROL, wmoviev.bmaxt, SET_VALUE=trim(wmoviev.bmax)	    
		ENDIF 
	    END

;	'OPT1' : BEGIN	
;		 WIDGET_CONTROL, wmoviev.bopt1, GET_VALUE=val
;		 wmoviev.opts1 = val
;	    ENDsxplot_ht.pro

	'OPT23' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bopt23, GET_VALUE=val
		 wmoviev.opts23 = val
	    END

;	'OPT4' : BEGIN	
;		 WIDGET_CONTROL, wmoviev.bopt4, GET_VALUE=val
;		 wmoviev.opts4 = val
;	    END

	'BOX' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bbox, GET_VALUE=val
		 wmoviev.box = val
	    END
	'SELMINMAX' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bselminmax, GET_VALUE=val
		 wmoviev.selminmax = val
		 if val eq 1 and wmoviev.auto eq 1 then begin
		    WIDGET_CONTROL, wmoviev.autom, SET_VALUE=0
		    wmoviev.auto = 0
	        endif
                
	    END
	'GSUB' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bsub, GET_VALUE=val
		 wmoviev.gsub = val
	    END
	'nointerp' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bnointerp, GET_VALUE=val
		 wmoviev.nointerp = val
	    END
	'coordcenter' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bcoordcenter, GET_VALUE=val
		 wmoviev.coordcenter = val
	    END
	'carsub' : BEGIN	
		 WIDGET_CONTROL, wmoviev.bcarsub, GET_VALUE=val
		 wmoviev.coordsub = val
                 swmoviev = wmoviev
	         WIDGET_CONTROL, /DESTROY, wmoviev.base
    	    	 exit_playmovie    ; in case test movie still running
                 wscc_mkmovie,swmoviev.files,/coordsub
	         RETURN, 0
	    END
	'pixsub' : BEGIN	
		 wmoviev.coordsub = 0
		 if wmoviev.inst eq 'SECCHI' then begin
		    wmoviev.x1t =0	    
		    wmoviev.x2t =2047	    
		    wmoviev.y1t =0	    
		    wmoviev.y2t =2047	    
                    wmoviev.csub =0	
                 endif
		 if (wmoviev.inst eq 'LASCO' or wmoviev.inst eq 'EIT' or wmoviev.inst eq 'AIA') then begin
		    wmoviev.x1t =0	    
		    wmoviev.x2t =1023    
		    wmoviev.y1t =0	    
		    wmoviev.y2t =1023	    
                    wmoviev.csub =0	
                 endif
                 swmoviev = wmoviev
	         WIDGET_CONTROL, /DESTROY, wmoviev.base
    	    	 exit_playmovie    ; in case test movie still running
                 wscc_mkmovie,swmoviev.files
	         RETURN, 0
	    END
	'CSUB' : BEGIN	
		 WIDGET_CONTROL, wmoviev.csub, GET_VALUE=val
		 if (val eq 1) and wmoviev.inst eq 'SECCHI' then begin
		    WIDGET_CONTROL, wmoviev.x1t, SET_VALUE=0	    
		    WIDGET_CONTROL, wmoviev.x2t, SET_VALUE=2047	    
		    WIDGET_CONTROL, wmoviev.y1t, SET_VALUE=0	    
		    WIDGET_CONTROL, wmoviev.y2t, SET_VALUE=2047	    
                    WIDGET_CONTROL, wmoviev.csub, SET_VALUE=0	
                 endif
		 if (val eq 1) and (wmoviev.inst eq 'LASCO' or wmoviev.inst eq 'EIT' or wmoviev.inst eq 'AIA') then begin
		    WIDGET_CONTROL, wmoviev.x1t, SET_VALUE=0	    
		    WIDGET_CONTROL, wmoviev.x2t, SET_VALUE=1023    
		    WIDGET_CONTROL, wmoviev.y1t, SET_VALUE=0	    
		    WIDGET_CONTROL, wmoviev.y2t, SET_VALUE=1023	    
                    WIDGET_CONTROL, wmoviev.csub, SET_VALUE=0	
                 endif
	    END
	'autom' :   BEGIN
		 WIDGET_CONTROL, wmoviev.autom, GET_VALUE=val
		 wmoviev.auto = val
		 if val eq 1 and wmoviev.selminmax eq 1 then begin
		    WIDGET_CONTROL, wmoviev.bselminmax, SET_VALUE=0
		    wmoviev.selminmax = 0
	        endif
	     END
	'dcolorv' :   BEGIN
		 WIDGET_CONTROL, wmoviev.dcolorv, GET_VALUE=val
		 wmoviev.dcolor = val
 	     END
	'dnoisev' :   BEGIN
		 WIDGET_CONTROL, wmoviev.dnoisev, GET_VALUE=val
		 wmoviev.dnoise = val
 	     END
	'dnewbkg' :   BEGIN
		 WIDGET_CONTROL, wmoviev.dnewbkg, GET_VALUE=val
		 wmoviev.newbkgval = val
 	     END

	ELSE : BEGIN
                 PRINT, '%%WSCC_MKMOVIE_BGROUP.  Unknown event.'
	     END

    ENDCASE

    WIDGET_CONTROL, ev.top, SET_UVALUE=wmoviev

    RETURN, 0

END



PRO WSCC_MKMOVIE, arg1, VERBOSE=verbose, DEBUG=debug, RESET=reset, single=single, coordsub=coordsub, MARGIN=margin

;set_plot,'x' ;incase scc_mkframe left the zbuffer set
select_windows
COMMON WSCC_MKMOVIE_COMMON  	; defined above
  undefine,textframe

    IF ~keyword_set(MARGIN) THEN margin=0
    if keyword_set(single) then single=1 else single=0
  ; IF keyword_set(OLD) THEN oldflagset=1 ELSE oldflagset=0
    IF keyword_set(VERBOSE) THEN debug_on=1 ELSE debug_on=0 ; debug_on in COMMON block
    IF keyword_set(DEBUG) THEN debug_on=1
    oldflagset=0

   IF XRegistered("SCC_PLAYMOVIEM") or XRegistered("SCC_PLAYMOVIE") THEN BEGIN
      bell = STRING(7B)
      PRINT, bell
      print,'%%There is an instance SCC_PLAYMOVIE already running.  Only one instance may run at a time.'
      print,'%%Recommend either exitting existing PLAYMOVIE session or type WIDGET_CONTROL,/RESET.'
      RETURN
   ENDIF

   IF (DATATYPE(arg1) EQ 'UND') THEN $	;** call scclister to select image files
      arg1= SCCLISTER(/AORB)

   IF (N_ELEMENTS(arg1) EQ 1 and DATATYPE(arg1) EQ 'STR' and single ne 1) THEN BEGIN	;** read in names of images from file
      IF arg1 EQ '' THEN return
      str = STRARR(1)
      OPENR, UNIT, arg1, /GET_LUN
      READF, UNIT, str
      files = str
      WHILE (NOT EOF(UNIT)) DO BEGIN
         READF, UNIT, str
         files = [files, str]
      ENDWHILE
      CLOSE, UNIT
      FREE_LUN, UNIT
      len = N_ELEMENTS(files)
   ENDIF 
   IF (DATATYPE(arg1) EQ 'STC') THEN BEGIN	;** read in names of images from scclister structure
        tfiles= arg1
        tn= tag_names(tfiles)
	inp=0
	IF n_elements(tn) GT 1 THEN BEGIN
	    print,'WSCC_MKMOVIE accepts arguments for SC-A -OR- SC-B only, not both.'
	    print,'You will have the option to make a movie with both, matching to the input.'
	    read, 'Please enter 0 to match with ST-A images or 1 to match with ST-B images: ',inp
	ENDIF
	    
        status= execute("tfn= tfiles."+tn(inp))
        files= tfn
	
   ENDIF ELSE BEGIN				;** names of images have been passed
      files = arg1
   ENDELSE
   IF (DATATYPE(files) EQ 'UND') OR (files(0) EQ '') THEN RETURN
   good = WHERE(files NE '')
   IF (good(0) EQ -1) THEN RETURN
   files=files(good)

    isfits=is_fits(files[0],ext)
    ; check for compressed AIA image
    IF ext EQ 'BINTABLE' THEN mreadfits_tilecomp,files[0],hdrin,image ELSE $
   mREADFITS,files[0], hdrin, /dash2underscore
   
   IF hdrin.INSTRUME NE 'SECCHI' THEN hdr0=convert2secchi(hdrin) ELSE hdr0=hdrin
     
   ; IF we do this, it should be with scc_img_trim.pro --nr
   ;IF (hdr0.naxis1 GT 2048) THEN BEGIN
   ;  fn= strmid(files(0),rstrpos(files(0),'/')+1,50)
   ;  message,fn+' image naxis1= '+STRTRIM(hdr0.naxis1,2)+' > 2048  -- Making it 2048x2048.',/CONT
   ;  hdr0.p1col = hdr0.p1col > 51
   ;  hdr0.p2col = hdr0.p2col < 2098
   ;  junk= junk(hdr0.p1col:hdr0.p2col,*)
   ;ENDIF
 
;
;***************** Is the header the same structure name? need a single structure.

    medval=['no','3','5','7','9']
   scalearr=['Use Linear Scaling    ', $
             'Use SQRT Scaling      ', $
             'Use LOG Scaling       ', $
             'Use NRG Filter']
   typearr=['Straight', $
            'Difference', $
            'Running Diff', $
            'Unsharp', $
            'Ratio', $
            'Sharpen Ratio']
;            'Sharpen Ratio',
;            'Wavelet Enhancement']
   optarr=['Display Date        ', $
           'Divide by EXPTIME', $
           'Sort by Date        ', $
           'Flat field/vignet   ', $  ; AEE - added
	   'Physical units      ', $
	   'Rotate North Up     ', $
	   'Use SECCHI Logo     ', $
	   'Show Limb           ', $
	   'Mask Outer Field    ', $
;	   'Both A and B (Match)', $
;	   'Both, A on left     ', $
	   'Add Objects (HI2)  ', $
	   'Add Planets  ', $
	   'Run Point Filter',$
	   'Adjust Rdiff by Median']

   optarr1=[ 'Flat Field Correct        ', $
             'Use Radial Filter         ']
;   basefarr=['Use Frame Zero for Base   ', $
;             'Use Any-Year Monthly Model for Base ', $
;             'Use Monthly Model for Base']
   basefarr=[ ' First Frame ', $
              ' Monthly Min ', $
	      ' Daily Median']

;   optarr4=[ 'Degrid                    ']
   panarr=   ['    128x128    ', $
              '    256x256    ', $
              '    512x512    ', $
              '   1024x1024   ', $
              '   2048x2048   ']

   occarr=['Do Not Mask Occulter ', $
           'Use Mask from file   ', $
           'Mask Occulter        ', $
           'Large Mask Occulter  ']
   occlimb=['no limb ', $
           'show limb   ']
   occouter=['no outer mask ', $
           'outer mask   ']
   gaparr=['Do Not Fill Data Gaps', $
           'Fill Gaps w/color    ', $
           'Fill Gaps w/prev img ']
    pbsarr=['None',$
            'Total Brightness',$
            'Polarized Brightness',$
	    'Polarization Angle',$
	    'Percent Polarized']
   botharr=['Single SC',$
            'Both, B on Left',$
            'Both, A on Left',$
	    'Both, B on Top',$
	    'Both, A on Top']
   IF (DATATYPE(swmoviev) NE 'UND') THEN IF swmoviev.cam eq 'aia' or swmoviev.cam eq 'euvi' then  $
      botharr=[botharr,$
            'SDO | A',$
            'B | SDO',$
	    'B | SDO | A']

;print,swmoviev.bmin, swmoviev.bmax
   IF (DATATYPE(swmoviev) NE 'UND') THEN BEGIN
     if swmoviev.cam ne STRLOWCASE(hdr0.detector) THEN RESET=1
   ENDIF
   IF (DATATYPE(swmoviev) EQ 'UND') or keyword_set(RESET) THEN BEGIN	;** use defaults
    	cam=STRLOWCASE(hdr0.detector)
    	bmin=-100./hdr0.exptime
	bmax= 100./hdr0.exptime
	auto=0

      skip=1
      newbkgval=0
      rdiff=1
      dcolor=0
      ddomed=0
      dnoise=0
      pbs=0
      unsharp = 0;  25
      ; **  If non-zero, this value indicates that smooth(image, unsharp) will be applied, unless Unsharp or 
      ;     Sharpen Ratio is selected, in which case this number is the size of the box input to these options.
      col = -1
      fact = 0.015
      pan = 2
      scale = 0
      occ = 0
      ; moved limb and outer to opt array - nr, 1/22/08
      ;limb = 0
      ;outer = 0
      gap = 0
      basef = 1     ; default monthly min
 
      box = 0
      gsub = 0
      coordsub = 0
      selminmax = 1
      csub = 0
      dtsz= 1.
      both=0
      fboth=0
      type = INTARR(N_ELEMENTS(typearr))
      opts1 = INTARR(N_ELEMENTS(optarr1))
;      opts4 = INTARR(N_ELEMENTS(optarr4))
	x1 = 0
	x2 = 2047
	y1 = 0
	y2 = 2047
	boxlabel='Relative to 2048x2048 image'
	
    	type[1] = 1	;** default is difference movie
	
    	;optarr=['Display Date        ', $
    	;    	'Normalize to exptime', $
    	;    	'Sort by Date        ', $
    	;    	'Flat field/vignet   ', $  
    	;    	'Physical units      ', $
    	;    	'Rotate North Up     ', $
    	;    	'Use SECCHI Logo     ', $
    	;    	'Show Limb           ', $
    	;    	'Mask Outer Field    ', $
    	;    	'Add Objects (HI2)  ', $
    	;    	'Add Planets  ', $
    	;    	'Run Point Filter', $
    	;    	'Adjust Rdiff by Median']
    	;
	; Set default opts (See optarr defined above)
	datopt=1
	expopt=1
	srtopt=1
	vigopt=0
	calopt=0
	rotopt=1
	logopt=1
	limopt=1
	mskopt=1
	objopt=0
	plaopt=0
	pfiopt=0
	rdmedadj=0
        nointerp=0
        coordcenter=0
    	CASE cam OF
           'aia':BEGIN
    	    	bmin = -2.
    	    	bmax = 2.
    	    	;auto=1
		type[*]=0
    	    	type[0]=1	;** default is straight movie
		limopt=0
		mskopt=0
		logopt=0
    	    	;** default is display date, normalize, sort by time, no flatfield/vign, no physical units, 
		;   north up, logo, no limb, no mask outer, logo
   		botharr=[botharr,$
         		   'SDO | A',$
         		   'B | SDO',$
			   'B | SDO | A']
             END

           'euvi':BEGIN
      	    	selminmax = 1
    	    	bmin = -2.
    	    	bmax = 2.
    	    	auto=0
		type[*]=0
    	    	type[0]=1	;** default is straight movie
		limopt=0
		mskopt=0
   		botharr=[botharr,$
         		   'SDO | A',$
         		   'B | SDO',$
			   'B | SDO | A']
    	    	;** default is display date, normalize, sort by time, no flatfield/vign, no physical units, 
		;   north up, logo, no limb, no mask outer, logo
              END
           'cor2':BEGIN
                 auto=0
                 if strlowcase(hdr0.OBSRVTRY) eq 'stereo_a'  THEN BEGIN
    	             bmax=1.4
    	             bmin=0.0
	         ENDIF ELSE BEGIN
	             bmax=1.8
	             bmin=0.2
	         ENDELSE
                 END
           'cor1':BEGIN
                 bmin = -2.
                 bmax = 2.
                 auto=0
               END
           'hi1':BEGIN
                 if strlowcase(hdr0.OBSRVTRY) eq 'stereo_a'  THEN BEGIN
	           bmax=0.85
	           bmin=0.38
	         ENDIF ELSE BEGIN
    	           bmax=0.88
	           bmin=0.33
	         ENDELSE
                 auto=0
		 rotopt=0
               END
           'hi2':BEGIN
	         bmax=1.0
	         bmin=0.3
                 auto=0
		 rotopt=0
               END
	    'eit':BEGIN
		type[*]=0
    	    	type[0]=1	;** default is straight movie
		limopt=0
		mskopt=0
    	    	x1 = 0
    	    	x2 = 1023
    	    	y1 = 0
    	    	y2 = 1023
    	    	boxlabel='Relative to 1024x1024 image'
		logopt=0
    	    	bmin= 0.
	    	bmax= 4.
    	       END    	
	    
	       ELSE:
    	ENDCASE
	  
        inst = hdr0.instrume

	IF (inst EQ 'SECCHI')  THEN BEGIN
	    xy0=fix(sccrorigin(hdr0))
	    x0=xy0[0]
	    y0=xy0[1]
	    x1 = (hdr0.r1col-x0)>0
	    x2 = (hdr0.r2col-x0)<2047
	    IF (x2 LE 0)  THEN x2=2047
	    y1 = (hdr0.r1row-y0) > 0
	    ;y2 = hdr0.p2row-3
	    y2 = (hdr0.r2row-y0)<2047
	    IF (y2 LE 0) THEN y2 = 2047
    	    boxlabel='Relative to 2048x2048 image'
	ENDIF 
	IF (inst EQ 'LASCO') or (inst EQ 'AIA')  THEN BEGIN
	    x1 = 0
	    x2 = 1023
	    y1 = 0
	    y2 = 1023
    	    boxlabel='Relative to 1024x1024 image'
	    ;expopt=0	
	    logopt=0    	
    	    ;bmin=-100.
	    ;bmax= 100.
	ENDIF 
	IF (inst EQ 'AIA') and hdr0.naxis1 ge 4096 THEN BEGIN
	    x1 = 0
	    x2 = 4095
	    y1 = 0
	    y2 = 4095
    	    boxlabel='Relative to 4096x4096 image'
	    ;expopt=0	
	    logopt=0    	
            panarr=   [panarr,'   4096x4096   ']
    	    ;bmin=-100.
	    ;bmax= 100.
	ENDIF 
	bx1 = (x2-x1)/2-50
	bx2 = bx1+99
	by1 = y2-99
	by2 = y2
      	carx1=0. & carx2=360. & cary1=-90. & cary2=90.
        coordsize=[.5,fix((carx2-carx1+1)/.5),fix((cary2-cary1+1)/.5)]
        coordw=intarr(3)
        ubmin=[bmin,bmin,bmin]
        ubmax=[bmax,bmax,bmax]
	if keyword_set(coordsub) then  coordsub= 1 else coordsub= 0
	
    	opts = [datopt,expopt,srtopt,vigopt,calopt,rotopt,logopt,limopt,mskopt,objopt,plaopt,pfiopt,rdmedadj]	
    	;** default is display date, normalize, sort by time, no flatfield/vign, no physical units, 
	;   north up, logo, show limb, mask outer, logo, Not objects, not planets, not point filter
    ENDIF ELSE BEGIN	 ;** restore previous values
	bmin = swmoviev.bmin
	bmax = swmoviev.bmax
	ubmin = swmoviev.ubmin
	ubmax = swmoviev.ubmax
        cam = STRLOWCASE(hdr0.detector)
        inst = hdr0.instrume
        auto = swmoviev.auto
	newbkgval=swmoviev.newbkgval
        dcolor = swmoviev.dcolor
        ddomed = swmoviev.ddomed
        dnoise = swmoviev.dnoise
        pbs = swmoviev.pbs
	unsharp = swmoviev.unsharp
	scale = swmoviev.scale
	type = swmoviev.type
	opts = swmoviev.opts
	opts1 = swmoviev.opts1
	;      opts4 = swmoviev.opts4
	pan = swmoviev.pan
	occ = swmoviev.occ
	;limb = swmoviev.limb
	;outer = swmoviev.outer
	gap = swmoviev.gap
	basef = swmoviev.basef
	col = swmoviev.col
	fact = swmoviev.fact
	box = swmoviev.box
	gsub = swmoviev.gsub
	coordsub = swmoviev.coordsub
        coordsize=swmoviev.coordsize
        coordw=swmoviev.coordw
	selminmax = 0
	dtsz= swmoviev.dtsz
	x1 = swmoviev.x1
	x2 = swmoviev.x2
	y1 = swmoviev.y1
	y2 = swmoviev.y2
	carx1 = swmoviev.carx1
	carx2 = swmoviev.carx2
	cary1 = swmoviev.cary1
	cary2 = swmoviev.cary2
	skip=swmoviev.skip
	rdiff=swmoviev.rdiff
	both=swmoviev.both
	fboth=swmoviev.fboth
	boxlabel=swmoviev.boxlabel
	bx1=swmoviev.bx1
	bx2=swmoviev.bx2
	by1=swmoviev.by1
	by2=swmoviev.by2
	nointerp=swmoviev.nointerp
	coordcenter=swmoviev.coordcenter
    ENDELSE

	IF (inst EQ 'SECCHI')  THEN BEGIN
    	    boxlabel='Relative to 2048x2048 image'
            basefarr=[ ' First Frame ', $
              ' Monthly Min ', $
	      ' Daily Median']
    	    optarr(6)='Use SECCHI Logo     '
	ENDIF 
	IF (inst EQ 'LASCO') THEN BEGIN
            basefarr=[ ' First Frame ', $
             	       ' Monthly Min',$
                       ' All-Years']

    	    optarr(6)='Use LASCO Logo     '
	ENDIF 


;help,x1,x2,y1,y2

;**--------------------Create Widgets-------------------------------**
   base = WIDGET_BASE(/COL, XOFFSET=275, YOFFSET=275, TITLE='WSCC_MKMOVIE')
   rowb = WIDGET_BASE(base, /ROW)

;-- First column
    col1 = WIDGET_BASE(rowb, /COLUMN)

    base1 = WIDGET_BASE(col1, /ROW)
    col2 = WIDGET_BASE(base1, /COLUM,/frame)
    tmp = WIDGET_LABEL(col2, VALUE='pB Series Options:')
    pbsv =   CW_BSELECTOR2(col2, pbsarr, SET_VALUE=pbs, UVALUE='pbsv')

    tmp = WIDGET_LABEL(col1, VALUE='Image Scaling')
    col2 = WIDGET_BASE(col1, /COLUMN , /FRAME)

    base1 = WIDGET_BASE(col2, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='BYTSCL Min')
    	bmint = WIDGET_TEXT(base1, VALUE=STRTRIM(bmin,2), XSIZE=6, YSIZE=1, UVALUE='BMINT', /EDITABLE)
    ;      base1 = WIDGET_BASE(col2, /ROW)
    ;         tmp = WIDGET_LABEL(base1, VALUE='BYTSCL Max')
    ;	 bmaxt = WIDGET_TEXT(base1, VALUE=STRTRIM(bmax,2), XSIZE=5, YSIZE=1, UVALUE='BMAXT', /EDITABLE)
    tmp = WIDGET_LABEL(base1, VALUE='Max')
    	bmaxt = WIDGET_TEXT(base1, VALUE=STRTRIM(bmax,2), XSIZE=6, YSIZE=1, UVALUE='BMAXT', /EDITABLE)

    base1 = WIDGET_BASE(col2, /ROW)
     bselminmax = CW_BGROUP(base1, 'Use interactive bytscl', BUTTON_UVALUE='SELMINMAX', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
    base1 = WIDGET_BASE(col2, /ROW)
     autom = CW_BGROUP(base1, 'Automatic scaling', BUTTON_UVALUE='autom', $
     	/NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')

    base1 = WIDGET_BASE(col2, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='Unsharp/Smooth Box')
    	unst = WIDGET_TEXT(base1, VALUE=STRTRIM(unsharp,2), XSIZE=3, YSIZE=1, UVALUE='UNST', /EDITABLE)

    base1 = WIDGET_BASE(col2, /ROW)
     dcolort=['Use SECCHI_PREP color table']
     dcolorv = CW_BGROUP(base1, dcolort, BUTTON_UVALUE='dcolorv', $
     	/NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')


    base1 = WIDGET_BASE(col2, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='Mask Color (1-256)')
    	colt = WIDGET_TEXT(base1, VALUE=STRTRIM(col,2), XSIZE=3, YSIZE=1, UVALUE='COLT', /EDITABLE)
    base1 = WIDGET_BASE(col2, /ROW)
    	tmp = WIDGET_LABEL(base1, VALUE='(-1 for median)')
    base1 = WIDGET_BASE(col2, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='Sharpen Ratio Factor')
    	factt= WIDGET_TEXT(base1, VALUE=STRTRIM(fact,2), XSIZE=6, YSIZE=1, UVALUE='FACT', /EDITABLE)
    
     base1 = WIDGET_BASE(col2, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='Do Median:')
;     ddomedv = widget_droplist(base1, VALUE=medval, UVALUE='ddomedv',title='Do Median ');, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
;     ddomedv = CW_BGROUP(base1, ddomedt, BUTTON_UVALUE='ddomedv', $
;     	/NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')

    ddomedv = cw_bselector2(base1, medval, set_value=ddomed, uvalue='ddomedv')

     base1 = WIDGET_BASE(col2, /ROW)
    scalepd = CW_BSELECTOR2(base1, scalearr, SET_VALUE=scale, UVALUE='SCALE')

;-- Second column
    base1 = WIDGET_BASE(col2, /ROW)
   ;scalepd = CW_BSELECTOR2(base1, scalearr, SET_VALUE=scale, UVALUE='FFV Size')
    col1 = WIDGET_BASE(rowb, /COLUMN)
    tmp = WIDGET_LABEL(col1, VALUE='Type of Movie')
    	btypearr=STRARR(N_ELEMENTS(typearr))
    	btypearr(*)='TYPE'
    	btype = CW_BGROUP(col1,  typearr, BUTTON_UVALUE=btypearr, $
             /EXCLUSIVE, /COLUMN, /FRAME, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')

    base1 = WIDGET_BASE(col1, /ROW)
    	tmp = WIDGET_LABEL(base1, VALUE='Running diff of')
    	rdifft = WIDGET_TEXT(base1, VALUE=trim(rdiff), XSIZE=2, YSIZE=1, UVALUE='RDIFFT', /EDITABLE)
	

   ;col1 = WIDGET_BASE(col1, /COLUMN,/FRAME)
    base1 = WIDGET_BASE(col1, /ROW)
       tmp = WIDGET_LABEL(base1, 	VALUE='Base Frame:',/align_left)
    	basefpd = CW_BSELECTOR2(base1, basefarr, SET_VALUE=basef, UVALUE='BASEF')
    base1 = WIDGET_BASE(col1, /ROW)
    	dnewbkg = CW_BGROUP(base1, 'New bkg each frame', BUTTON_UVALUE='dnewbkg', $
     	/NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')

    base1 = WIDGET_BASE(col1, /ROW)
    	tmp = WIDGET_LABEL(base1, VALUE='Use every')
    	skipt = WIDGET_TEXT(base1, VALUE=trim(skip), XSIZE=3, YSIZE=1, UVALUE='SKIPT', /EDITABLE)
    	tmp = WIDGET_LABEL(base1, VALUE='file from input.')
    	;tmp = widget_label(col1,VALUE='from input.')
	
    base1 = WIDGET_BASE(col1, /ROW)
    	dnoisev = CW_BGROUP(base1, 'Average skipped frames', BUTTON_UVALUE='dnoisev', $
     	/NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')

    base1 = WIDGET_BASE(col1, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='FFV size:')
    	panpd =   CW_BSELECTOR2(base1, panarr, SET_VALUE=pan, UVALUE='PAN')
    base1 = WIDGET_BASE(col1, /ROW)
    tmp = WIDGET_LABEL(base1, VALUE='Date size:')
    	dtszt = WIDGET_TEXT(base1, VALUE=trim(dtsz), XSIZE=5, YSIZE=1, UVALUE='DTSZT', /EDITABLE)


;-- Third column
    col1 = WIDGET_BASE(rowb, /COLUMN)
    tmp = WIDGET_LABEL(col1, VALUE='Options')
    	boptarr=STRARR(N_ELEMENTS(optarr))
    	boptarr1=STRARR(N_ELEMENTS(optarr1))
;   boptarr4=STRARR(N_ELEMENTS(optarr4))
    	boptarr(*)='OPT'
    	boptarr1(*)='OPT1'
;   boptarr4(*)='OPT4'
    	bopt = CW_BGROUP(col1, optarr, BUTTON_UVALUE=boptarr, $
            /NONEXCLUSIVE, /COLUMN, /FRAME, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
	    
    col2 = WIDGET_BASE(col1, /COLUMN,/frame)
;    base1 = WIDGET_BASE(col2, /ROW)
    IF (inst NE 'LASCO') THEN BEGIN
      tmp = WIDGET_LABEL(col2, VALUE='S/C Options:')
      bothv =   CW_BSELECTOR2(col2, botharr, SET_VALUE=both, UVALUE='bothv')
      fbothv =   CW_BSELECTOR2(col2, ['Matching Only','All (fill w/prev)'], SET_VALUE=fboth, UVALUE='fbothv')
    endif
    occpd = CW_BSELECTOR2(col1, occarr, SET_VALUE=occ, UVALUE='OCC') 

   ;tmp = WIDGET_LABEL(col1, VALUE='For Base Frame Use')
   ;basefpd = CW_BSELECTOR2(col1, basefarr, SET_VALUE=basef, UVALUE='BASEF')

;-- Fourth column
    col1 = WIDGET_BASE(rowb, /COLUMN)
    tmp = WIDGET_LABEL(col1, VALUE='Subfield Coordinates')
    col2 = WIDGET_BASE(col1, /COLUMN, /FRAME)
    subfarr=['Use interactive subfield']
    bsub = CW_BGROUP(col2, subfarr, BUTTON_UVALUE='GSUB', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
    if coordsub eq 0 then begin 
       clrsubfarr=['Clear Subfield']
       csub = CW_BGROUP(col2, clrsubfarr, BUTTON_UVALUE='CSUB', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
       xl1='X1' & xl2='X2' & yl1='Y1' & yl2='Y2'
    endif else begin
      boxlabel='Carrington Coordinates'
       xl1='CRLN1' & xl2='CRLN2' & yl1='CRLT1' & yl2='CRLT2'
       bcarsub=-1 & csub=-1
    endelse
    tmp = WIDGET_LABEL(col2, VALUE=boxlabel)
     if coordsub eq 0 then begin 
      base1 = WIDGET_BASE(col2, /ROW)
;    	tmp = WIDGET_LABEL(base1, VALUE='X1')
;    	x1t = WIDGET_TEXT(base1, VALUE=STRTRIM(x1,2), XSIZE=5, YSIZE=1, UVALUE='X1', /EDITABLE)
;    	tmp = WIDGET_LABEL(base1, VALUE='X2')
;    	x2t = WIDGET_TEXT(base1, VALUE=STRTRIM(x2,2), XSIZE=5, YSIZE=1, UVALUE='X2', /EDITABLE)
         x1t=cw_field(base1, value=x1, uvalue="X1T",title=xl1,/all_events,xsize=5)
         x2t=cw_field(base1, value=x2, uvalue="X2T",title=xl2,/all_events,xsize=5)

      base1 = WIDGET_BASE(col2, /ROW)
;    	tmp = WIDGET_LABEL(base1, VALUE='Y1')
;    	y1t = WIDGET_TEXT(base1, VALUE=STRTRIM(y1,2), XSIZE=5, YSIZE=1, UVALUE='Y1', /EDITABLE)
;    	tmp = WIDGET_LABEL(base1, VALUE='Y2')
;    	y2t = WIDGET_TEXT(base1, VALUE=STRTRIM(y2,2), XSIZE=5, YSIZE=1, UVALUE='Y2', /EDITABLE)
         y1t=cw_field(base1, value=y1, uvalue="Y1T",title=yl1,/all_events,xsize=5)
         y2t=cw_field(base1, value=y2, uvalue="Y2T",title=yl2,/all_events,xsize=5)

      	base1 = WIDGET_BASE(col2, /ROW)
    	;tmp = WIDGET_LABEL(base1, VALUE='Use Integer Factors (ex: y1=512 y2=1535)')
    	tmp = WIDGET_LABEL(base1, VALUE='   Use Integer Factors')
    	base1 = WIDGET_BASE(col2, /ROW)
    	tmp = WIDGET_LABEL(base1, VALUE='  (Ex: Y1=512 Y2=1535)')
        if cam eq 'euvi' or strupcase(cam) eq 'AIA' or cam eq 'eit' then  $
        bcarsub = CW_BGROUP(col2, 'Use carrington subfield', BUTTON_UVALUE='carsub', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP') else bcarsub=-1
        bpixsub=-1
        bnointerp=-1
        bcoordcenter=-1
	 carx1t = -1
	 carx2t = -1
	 cary1t = -1
	 cary2t = -1
     endif else begin
      base1 = WIDGET_BASE(col2, /ROW)
;    	tmp = WIDGET_LABEL(base1, VALUE='X1')
;    	x1t = WIDGET_TEXT(base1, VALUE=STRTRIM(x1,2), XSIZE=5, YSIZE=1, UVALUE='X1', /EDITABLE)
;    	tmp = WIDGET_LABEL(base1, VALUE='X2')
;    	x2t = WIDGET_TEXT(base1, VALUE=STRTRIM(x2,2), XSIZE=5, YSIZE=1, UVALUE='X2', /EDITABLE)
         carx1t=cw_field(base1, value=carx1, uvalue="CARX1T",title=xl1,/all_events,xsize=5)
         carx2t=cw_field(base1, value=carx2, uvalue="CARX2T",title=xl2,/all_events,xsize=5)

      base1 = WIDGET_BASE(col2, /ROW)
;    	tmp = WIDGET_LABEL(base1, VALUE='Y1')
;    	y1t = WIDGET_TEXT(base1, VALUE=STRTRIM(y1,2), XSIZE=5, YSIZE=1, UVALUE='Y1', /EDITABLE)
;    	tmp = WIDGET_LABEL(base1, VALUE='Y2')
;    	y2t = WIDGET_TEXT(base1, VALUE=STRTRIM(y2,2), XSIZE=5, YSIZE=1, UVALUE='Y2', /EDITABLE)
         cary1t=cw_field(base1, value=cary1, uvalue="CARY1T",title=yl1,/all_events,xsize=5)
         cary2t=cw_field(base1, value=cary2, uvalue="CARY2T",title=yl2,/all_events,xsize=5)
       bcoordcenter = CW_BGROUP(col2, 'Use Center Coord', BUTTON_UVALUE='coordcenter', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
    	tmp = WIDGET_LABEL(col2, VALUE='(CRLN2,CRLT2 and pixel size not used)')
       WIDGET_CONTROL, bcoordcenter, SET_VALUE=coordcenter
       bnointerp = CW_BGROUP(col2, 'Do not project onto regular grid', BUTTON_UVALUE='nointerp', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
       WIDGET_CONTROL, bnointerp, SET_VALUE=nointerp
       base1 = WIDGET_BASE(col2, /ROW)
       coordw(0)=cw_field(base1, value=coordsize(0), uvalue="COORDPSIZE",title='Pixel size in degress ',/all_events,xsize=5)
       base1 = WIDGET_BASE(col2, /ROW)
       coordw(1)=cw_field(base1, value=coordsize(1), uvalue="COORDXSIZE",title='Xsize ',/all_events,xsize=5)
       coordw(2)=cw_field(base1, value=coordsize(2), uvalue="COORDYSIZE",title='Ysize ',/all_events,xsize=5)
       bpixsub = CW_BGROUP(col2, 'Use pixel subfield', BUTTON_UVALUE='pixsub', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
	 x1t = -1
	 x2t = -1
	 y1t = -1
	 y2t = -1
     endelse

     if ~(keyword_set(coordsub)) then begin 
       col1a = WIDGET_BASE(col1, /COLUMN, /FRAME)
       boxarr=['Use Box Normalization']
       bbox = CW_BGROUP(col1a, boxarr, BUTTON_UVALUE='BOX', $
            /NONEXCLUSIVE, /COLUMN, EVENT_FUNCT='WSCC_MKMOVIE_BGROUP')
       tmp = WIDGET_LABEL(col1a, VALUE=boxlabel)
       col2 = WIDGET_BASE(col1a, /COLUMN)
       base1 = WIDGET_BASE(col2, /ROW)
         tmp = WIDGET_LABEL(base1, VALUE='X1')
	 bx1t = WIDGET_TEXT(base1, VALUE=STRTRIM(bx1,2), XSIZE=5, YSIZE=1, UVALUE='X1', /EDITABLE)
         tmp = WIDGET_LABEL(base1, VALUE='X2')
	 bx2t = WIDGET_TEXT(base1, VALUE=STRTRIM(bx2,2), XSIZE=5, YSIZE=1, UVALUE='X2', /EDITABLE)
       base1 = WIDGET_BASE(col2, /ROW)
         tmp = WIDGET_LABEL(base1, VALUE='Y1')
	 by1t = WIDGET_TEXT(base1, VALUE=STRTRIM(by1,2), XSIZE=5, YSIZE=1, UVALUE='Y1', /EDITABLE)
         tmp = WIDGET_LABEL(base1, VALUE='Y2')
	 by2t = WIDGET_TEXT(base1, VALUE=STRTRIM(by2,2), XSIZE=5, YSIZE=1, UVALUE='Y2', /EDITABLE)
     endif else begin
        bbox = -1
	 bx1t = -1
	 bx2t = -1
	 by1t = -1
	 by2t = -1
     endelse

    ;limbpd = CW_BSELECTOR2(col1, occlimb, SET_VALUE=limb, UVALUE='LIMB') 
    ;outerpd = CW_BSELECTOR2(col1, occouter, SET_VALUE=outer, UVALUE='OUTER') 
    gappd = CW_BSELECTOR2(col1, gaparr, SET_VALUE=gap, UVALUE='GAP')

   ;done = WIDGET_BUTTON(base, VALUE='CREATE MOVIE', UVALUE='DONE')
   ;done = WIDGET_BUTTON(base, VALUE=' TEST MOVIE ', UVALUE='TEST')
   ;done = WIDGET_BUTTON(base, VALUE='  XLOADCT   ', UVALUE='XLOADCT')
   ;done = WIDGET_BUTTON(base, VALUE='   CANCEL   ', UVALUE='CANCEL')

   rowc = WIDGET_BASE(base, /ROW)
   blk= '        '
   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
   done = WIDGET_BUTTON(rowc, VALUE='CREATE MOVIE', UVALUE='DONE')
   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
   done = WIDGET_BUTTON(rowc, VALUE='SAVE MOVIE', UVALUE='SAVEMV')
   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
   done = WIDGET_BUTTON(rowc, VALUE=' TEST MOVIE ', UVALUE='TEST')
   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
   done = WIDGET_BUTTON(rowc, VALUE='  XLOADCT   ', UVALUE='XLOADCT')
   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
;   done = WIDGET_BUTTON(rowc, VALUE='  ADD TEXT  ', UVALUE='ADDTXT')
;   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
   done = WIDGET_BUTTON(rowc, VALUE=' CLEAR TEXT ', UVALUE='CLRTXT')
   tmp  = WIDGET_LABEL(rowc, VALUE=blk)
   done = WIDGET_BUTTON(rowc, VALUE='   CANCEL   ', UVALUE='CANCEL')

;**--------------------Done Creating Widgets-----------------------------**

   WIDGET_CONTROL, /REALIZE, base
   ind = WHERE(type EQ 1)
   WIDGET_CONTROL, btype, SET_VALUE=ind(0)
   WIDGET_CONTROL, bopt, SET_VALUE=opts
   IF debug_on THEN print,'BOPT value in main',opts
;   WIDGET_CONTROL, bopt1, SET_VALUE=opts1
;   WIDGET_CONTROL, bopt4, SET_VALUE=opts4
   if bbox gt 0 then WIDGET_CONTROL, bbox, SET_VALUE=box
   WIDGET_CONTROL, bsub, SET_VALUE=gsub
   if bcarsub gt 0 and (cam eq 'euvi' or cam eq 'AIA') then WIDGET_CONTROL, bcarsub, SET_VALUE=coordsub
   WIDGET_CONTROL, dcolorv, SET_VALUE=dcolor
   ;WIDGET_CONTROL, ddomedv, SET_DROPLIST_SELECT=ddomed
   WIDGET_CONTROL, dnoisev, SET_VALUE=dnoise
   WIDGET_CONTROL, dnewbkg, SET_VALUE=newbkgval
;   WIDGET_CONTROL, pbsv, SET_VALUE=pbs
   WIDGET_CONTROL, autom, SET_VALUE=auto
   WIDGET_CONTROL, bselminmax, SET_VALUE=selminmax

   wmoviev = { $
 		base:base, $ 
 		files:files, $ 
 		hdr0:hdr0, $ 
 		cam:cam, $ 
                inst:inst, $
 		bmin:bmin, $ 
 		bmax:bmax, $ 
 		ubmin:ubmin, $ 
 		ubmax:ubmax, $ 
                auto:auto, $ 
                dcolor:dcolor, $ 
                ddomed:ddomed, $ 
                medval:medval, $ 
                pbs:pbs, $ 
 		type:type, $ 
 		x1:x1, $ 
 		x2:x2, $ 
 		y1:y1, $ 
 		y2:y2, $ 
 		x1t:x1t, $ 
 		x2t:x2t, $ 
 		y1t:y1t, $ 
 		y2t:y2t, $ 
 		carx1:carx1, $ 
 		carx2:carx2, $ 
 		cary1:cary1, $ 
 		cary2:cary2, $ 
 		carx1t:carx1t, $ 
 		carx2t:carx2t, $ 
 		cary1t:cary1t, $ 
 		cary2t:cary2t, $ 
 		box:box, $ 
 		csub:csub, $ 
 		gsub:gsub, $ 
 		coordsub:coordsub, $ 
       		coordsize:coordsize, $ 
       		coordw:coordw, $
 		selminmax:selminmax, $ 
 		bbox:bbox, $ 
 		bsub:bsub, $ 
 		bcarsub:bcarsub, $ 
 		bpixsub:bpixsub, $ 
 		bnointerp:bnointerp, $ 
 		nointerp:nointerp, $ 
		bcoordcenter:bcoordcenter, $ 
		coordcenter:coordcenter, $ 
 		bselminmax:bselminmax, $ 
 		bx1:bx1, $ 
 		bx2:bx2, $ 
 		by1:by1, $ 
 		by2:by2, $ 
 		bx1t:bx1t, $ 
 		bx2t:bx2t, $ 
 		by1t:by1t, $ 
 		by2t:by2t, $ 
 		gap:gap, $ 
 		gappd:gappd, $ 
 		occ:occ, $ 
 		occpd:occpd, $ 
 		;limb:limb, $ 
 		;limbpd:limbpd, $ 
 		;outer:outer, $ 
 		;outerpd:outerpd, $ 
 		pan:pan, $ 
 		panpd:panpd, $ 
 		basef:basef, $ 
 		basefpd:basefpd, $ 
 		col:col, $ 
 		colt:colt, $ 
 		fact:fact, $ 
 		factt:factt, $ 
 		scale:scale, $ 
 		scalepd:scalepd, $ 
 		opts:opts, $ 
 		opts1:opts1, $ 
; 		opts4:opts4, $ 
 		unsharp:unsharp, $ 
 		bmint:bmint, $
 		bmaxt:bmaxt, $
                autom:autom, $
                dcolorv:dcolorv, $
                ddomedv:ddomedv, $
                pbsv:pbsv, $
 		unst:unst, $
 		btype:btype, $
		dtsz:dtsz, $
	    	dtszt:dtszt, $
		skip:skip, $
		skipt:skipt, $
		dnoise:dnoise, $
		dnoisev:dnoisev, $
		rdiff:rdiff, $
		rdifft:rdifft, $
		dnewbkg:dnewbkg, $
		newbkgval:newbkgval, $
		single:single, $
		both:both, $
		fboth:fboth, $
		boxlabel:boxlabel, $
 		bopt:bopt, $
		margin:margin $
; 		bopt1:bopt1, $
; 		bopt4:bopt4 $
            }
   


   WIDGET_CONTROL, base, SET_UVALUE=wmoviev
   WIDGET_CONTROL, base, /SHOW

   XMANAGER, 'WSCC_MKMOVIE', base, EVENT_HANDLER='WSCC_MKMOVIE_EVENT', /NO_BLOCK

END
