function test_frame_hdr, fn
; $Id: test_frame_hdr.pro,v 1.1 2011/10/06 17:21:34 nathan Exp $
; Purpose: Generate a MVI frame header from a filename
;
; Input: filename of format YYYYMMDD_HHMMSS_XXttT.xxx
;
; Output: frame header structure
; 
; $Log: test_frame_hdr.pro,v $
; Revision 1.1  2011/10/06 17:21:34  nathan
; for testing
;


   ahdr = {filename:fn, $
    	  detector:'', $
    	  date_obs:'', $
    	  time_obs:'', $
	  r1col:0, r2col:0, r1row:0, r2row:0, $
    	  filter:'', $
          polar:'', $
    	  wavelnth:'', $
    	  exptime:0.0,  $   	    ; seconds
    	  xcen:0.0, ycen:0.0,  $    ; pixels (FITS coordinates)
    	  cdelt1:0.0,  $    	    ; arcsec/pixel
    	  rsun:0.0,  $	    	    ; arcsec
    	  roll:0.0 }	    	    ; degrees

   parse_secchi_filename, fn, dobs, det, sc, wvl

   ahdr.detector=det
   ahdr.wavelnth = wvl
   ahdr.date_obs=strmid(dobs,0,10)
   ahdr.time_obs=strmid(dobs,11,8)

return, ahdr

end
