;+
; Project     : SOHO, STEREO
;
; Name        : select_car_mvi_files
;
; Purpose     : Given location and carrington rotation number, 
;   	    	returns MVI files for that rotation
;
; Explanation : 
;
; Example     : select_car_mvi_files,1984,mvis,'/net/earth/wavelets/daily_mvi_wav/*/c4/*.mvi'
;
; Inputs      : crn 	INT 	carrington rotation number
;   	    	dir 	STRING	REGEXP for mvi files to search
;
; Outputs     : mvifiles STRARR  of mvi files 
;
; Keywords    : 
;
; Restriction : MVI files must have YYMMDD in filename
;
; Calls       : 
;
; Category    : Image Processing Display file system search
;
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Oct 2008.
;
; MODIFICATION HISTORY:
; $Log: select_car_mvi_files.pro,v $
; Revision 1.1  2009/01/30 17:06:31  nathan
; moved select_car_mvi_files into separate file
;
;

pro select_car_mvi_files,crn,mvifiles,dir
    d1=utc2tai(carrdate2(crn,wl=0,el=1))
    d2=utc2tai(carrdate2(crn+1,wl=1,el=0))
    days=dindgen(((d2-d1)/(60l*60l*24))+1)*(60l*60l*24)+d1
    days=utc2str(tai2utc(days))
    files=strarr(n_elements(days)) +dir
    yyyymm=strmid(days,0,4)+strmid(days,5,2)
    if strpos(dir,'yyyymm') gt -1 then files=strmid(files,0,strpos(files(0),'yyyymm'))+yyyymm+strmid(files,strpos(files(0),'yyyymm')+6,strlen(files(0))) 
    yymmdd=strmid(days,2,2)+strmid(days,5,2)+strmid(days,8,2)
    if strpos(dir,'yymmdd') gt -1 then files=strmid(files,0,strpos(files(0),'yymmdd'))+yymmdd+strmid(files,strpos(files(0),'yymmdd')+6,strlen(files(0))) 
    fileu=strarr(n_elements(files))
    for nf=0,n_Elements(files)-1 do fileu(nf)=file_search(files(nf))
    mvifiles=fileu(where(fileu ne ''))
end

