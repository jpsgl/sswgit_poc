PRO srem_movie, files, hdrs, noshift = noshift, side = side, tel=tel, local=local, AUTOSAVE=autosave, $
	diff=diff, proj=proj, dir=dir, nomovie=nomovie, pos=pos, nreg=nreg, scl=scl, mfilt=mfilt, $
	bmin=bmin, bmax=bmax, pan=pan, noprep=noprep, write_png=write_png, save_frames=save_frames, $
	usedate=usedate, model=model, box=box, NOSNOW=nosnow, DEBUG=debug, USE_DAILY=use_daily, $
	EXPCORR=expcorr, MED=med, deproject=deproject, usebase=usebase, baseskip=baseskip, $
	notimes=notimes, MAXROLL=maxroll, RENORM=renorm
;
; $Id: srem_movie.pro,v 1.25 2015/12/29 19:37:38 mcnutt Exp $
;
; Project: STEREO - SECCHI
;                   
; Purpose: Use RDIF_HI.PRO to make clean difference images for HI1 or HI2, and compile these with generic_movie
;
; Inputs:
;   files   STRARR list of files to process --or--
;   	    STRING  Date YYYY-MM-DD to do movie for --or--
;   	    STRARR[2]	Start and end dates to do movie for
;
; INPUT KEYWORDS:
;   /RENORM 	Subtracts a median image from all frames created. Do this if there is a persistent, non-moving background in your movie.
;   /PAN    	Set to output 1024x1024, default is 512x512 output
;   USEDATE = 'yyyy-mm-dd' date for which to retrieve background instead of default
;   /USE_DAILY  Use daily median for background instead of monthly min
;   USEBASE=#	Use a set base frame for subtracting,input number of frames to make before changing to a more recent base:input -1 to use the first frame as the  
;               base frame for the entire movie
;   BASESKIP=#	Use frame baseskip frames later than current frame as base frame for subtracting-leaves out the first baseskip(number) frames
;   /EXPCORR    Do multi-exposure readout correction for HI images 
;   /NOSNOW 	Do point_filter on images
;   BOX = [x1,x2,y1,y2]     Box to use for cross_correlation (defaults to area
;   	    near center of fov offset toward sun) based on 512x512 FFV
;   HDR = Header to use to getbkgimg, if input MODEL= is undefined
;   /NOSHIFT	Returns zero image
;   MED=    	Value to which to normalize result instead of median of model (~38248.0), only used
;   	    	if ratio of medians of 1st and 2nd image is LT 0.97.
;   SIDE=   	'a' or 'b' or 'ab'
;   TEL=    	'hi_1' or 'hi_2' (default is hi_1)
;   NREG=   	purpose unclear
;   /LOCAL  	Use sccreadfits instead of secchi_prep if input are filenames
;   SCALE=  	Factor by which to scale input before test_crosscorrelation
;   SCL=    	Factor by which to scale output of test_crosscorr before shift and diff
;   POS=    	Array containing x,y of start corner of x1,y1,x2,y2 of subfield to use
;   /SPLIT  	Not used
;   MODEL=  	Filename of Background model image to use (optional)
;   MFILT=  	Apply median filter to result
;   /DEBUG  	Print info messages and display images in process
;   AUTOSAVE=	name of file to save resulting mvi to; automatic for SIDE='ab'
;  /NOTIMES	remove timestamp from movie
;   MAXROLL=  Set to nonzero value to override default criteria of 10 degrees difference from value of CROTA in first image
;
; EXAMPLES:
;   IDL> srem_movie,file_list, box=[10,200,10,500]  
;   ; generates a movie from list of files using left side of image for cross-correlation
;   IDL> srem_movie,'2008-10-10',tel='hi_2',side='ab'	
;   ; generate movie for HI2-A and HI2-B on 2008-10-10 and then display side-by-side
;
; Side Effects:
;   	Writes temporary FITS files in a subdirectory of /tmp or in diff_dir (if set)
;
; $Log: srem_movie.pro,v $
; Revision 1.25  2015/12/29 19:37:38  mcnutt
; corrected for roll values above 350
;
; Revision 1.24  2012/09/13 21:26:20  nathan
; do hi_fix_pointing
;
; Revision 1.23  2011/04/11 21:31:48  nathan
; add /RENORM keyword
;
; Revision 1.22  2010/05/11 16:00:38  nathan
; print file being processed
;
; Revision 1.21  2009/10/13 22:05:14  nathan
; extend MAXROLL to all CROTA check criteria
;
; Revision 1.20  2009/08/25 21:49:59  nathan
; screen files for CROTA excursions
;
; Revision 1.19  2009/07/31 13:22:47  cooper
; corrected movie length for baseskip/usebase movies
;
; Revision 1.18  2009/07/21 18:38:16  cooper
; added keyword notimes to remove timestamp
;
; Revision 1.17  2009/07/20 20:42:29  cooper
; added baseskip and usebase inputs
;
; Revision 1.16  2009/07/07 14:39:49  nathan
; fixed write_png filename; added some debug messages
;
; Revision 1.15  2009/04/23 19:38:26  nathan
; add /DEPROJECT keyword
;
; Revision 1.14  2009/01/16 20:58:33  nathan
; Added option to input date instead of file list and to do both A and B movies
; and display side-by-side with wscc_combine_mvi.pro; added /AUTOSAVE option;
; added documentation
;
; Revision 1.13  2008/12/10 00:02:53  nathan
; Write output fits with indexed filename to avoid overwriting; added some
; debug stuff
;
; Revision 1.12  2008/08/20 16:19:48  nathan
; added keywords /EXPCORR and /USE_DAILY
;
; Revision 1.11  2008/06/17 18:54:35  nathan
; Correct for binning in MODEL input
;
; Revision 1.10  2008/06/17 18:36:23  nathan
; Added /debug and some test code in comments
;
; Revision 1.9  2007/12/04 19:06:44  nathan
; Added /NOSNOW option; use /tmp for diff_dir if not set
;
; Revision 1.8  2007/12/04 16:30:48  nathan
; syntax error
;
; Revision 1.7  2007/12/04 16:25:08  nathan
; added BOX=[x1,x2,y1,y2] functionality for rdif_hi.pro
;
; Revision 1.6  2007/12/03 21:36:03  sheeley
; fixed prints and add divdate
;
; Revision 1.5  2007/12/03 21:21:31  sheeley
; nr - add USEDATE and retain original meaning of MODEL keywords
;
; Revision 1.4  2007/11/30 16:08:00  sheeley
; added /match to getbkgimg call
;
; Revision 1.3  2007/11/29 18:33:40  nathan
; version from adam/star_rem/ on Nov 7, 2007
;
COMMON DIR_COMMON, mvi_dir, jmap_dir, median_dir, diff_dir, image_dir

if keyword_set(DEBUG) THEN quiet=0 ELSE quiet=1
IF keyword_set(USE_DAILY) THEN usedaily=1 ELSE usedaily=0

    	ndy=1
	IF keyword_set(NDAYS) THEN ndy=ndays
	IF(NOT(keyword_set(SIDE))) THEN side = 'a'
	side = STRLOWCASE(side)
	IF(NOT(keyword_set(TEL))) THEN tel = 'hi_1'
	tel = STRLOWCASE(tel)
	IF(tel EQ 'hi1' OR tel EQ 'hi2') THEN tel = 'hi_' + strmid(tel,2)
	IF(NOT(keyword_set(DIFF))) THEN diff = 1
	local = keyword_set(LOCAL)
	shift = ~keyword_set(NOSHIFT)
	proj = keyword_set(PROJ)
	IF(datatype(dir) NE 'UND') THEN IF(strmid(dir,strlen(dir)-1) NE '/') THEN dir = dir + '/'

	filenames = DATATYPE(files) EQ 'STR'
	nim = n_elements(files)
	
	IF nim LE 2 THEN BEGIN
	    ; input is date(s)
	    IF side EQ 'ab' THEN sc=['a','b'] ELSE sc=side
	    nps=n_elements(sc)
	    IF n_elements(files) EQ 2 THEN IF strpos(files[1],':') LT 0 THEN files[1]=files[1]+' 23:59:59'
	    summary=scc_read_summary(DATE=files,SPACECRAFT=sc,TELESCOPE=tel, /NOBEACON, type='img')
	    wg=where(summary.xsize EQ 1024)
	    
	    files=sccfindfits(summary[wg].filename)
	ENDIF ELSE nps=1
pss=0	    	
REPEAT BEGIN	; A/B-side loop

	IF nps GT 1 THEN BEGIN
	    IF pss EQ 0 THEN subs=where(summary[wg].spacecraft EQ 'A') ELSE subs=where(summary[wg].spacecraft EQ 'B')
	    list=files[subs]
	ENDIF ELSE $
	    list = files
	IF~(keyword_set(baseskip) OR keyword_set(usebase)) THEN nim=n_elements(list)-diff
	IF(filenames) THEN BEGIN
		IF(local) THEN BEGIN
			spos = STRPOS(list[0], 'lz/L0')
			list = image_dir + STRMID(list, spos)
			sccload, list, hdrs, imgs
		ENDIF ELSE BEGIN
;			IF(~keyword_set(NOPREP)) THEN secchi_prep, list, hdrs, imgs $
;			ELSE sccload, list, hdrs, imgs, /nolocal
;			sccload, list, hdrs, imgs, /nolocal
		ENDELSE
	ENDIF ELSE imgs = files

	newfile = ''
	help,list
	IF ~quiet THEN wait,2

	i0=0
	j=sccreadfits(list[i0],hdrs0,/nodata)
    	; need original header for hi_prep on bkg
	; Also need header to check roll angle. -nathan, 8/24/09
	IF ~keyword_set(MAXROLL) THEN maxroll=10
	rota=abs(hdrs0.crota)	
	if hdrs0.crota gt 180 then rota=360.-hdrs0.crota else rota=abs(hdrs0.crota)
	WHILE rota GT maxroll DO BEGIN
	    message,'Skipping '+hdrs0.filename+' because CROTA='+trim(hdrs0.crota)+' GT '+trim(maxroll),/info
	    i0=i0+1
	    j=sccreadfits(list[i0],hdrs0,/nodata)
	ENDWHILE 
	    
    	;IF (local) THEN $
	IF (keyword_set(usebase) OR keyword_set(baseskip)) THEN $
	sccload, list[i0],         hdrs, imgs, /nolocal, EXPCORR=expcorr ELSE $
	sccload, list[i0:i0+diff], hdrs, imgs, /nolocal, EXPCORR=expcorr 
	; $ ; avoid memory allocation failure--store as few as possible 
	;ELSE secchi_prep, list[0:diff], hdrs,imgs, /nocalfac
    	twofn=list[i0:i0+diff]
	
	;IF ~quiet THEN BEGIN
	    openw,dlu,'~/srem_movie.log',/get_lun
	IF ~quiet THEN    window,xsi=512,ysi=512
	;ENDIF

    	IF keyword_set(USEDATE) THEN BEGIN
		umod=anytim2utc(usedate)
		smod=utc2str(umod)
		hdrs0.date_obs=smod
		hdrs0.date_avg=smod
		;IF(datatype(model) EQ 'STR') THEN model = sccreadfits(model, hdr)
		modelim=scc_getbkgimg(hdrs0,outhdr=medhdr,/match, DAILY=usedaily)
	ENDIF ELSE IF keyword_SET(MODEL) THEN BEGIN
		modelfn=model		
		print,'Using ',modelfn,' for background.'
		modelim = sccreadfits(modelfn, hdr)
		expfac=hdrs0.exptime/hdr.exptime
		IF round(expfac) NE 1 THEN BEGIN
			print,'Background corrected for EXPTIME ( *'+trim(expfac)+' )'
			modelim=modelim*expfac
		ENDIF
		sumdif=hdrs0.ipsum - hdr.ipsum
		IF  sumdif NE 0  THEN BEGIN
    		    binfac=4^(sumdif)
		    print,'Background corrected for binning ( *'+trim(binfac)+' )'
		    modelim=modelim*binfac
		ENDIF
	ENDIF ELSE modelim = scc_getbkgimg(hdrs0, outhdr=medhdr,/match, DAILY=usedaily)
	
	IF ~quiet THEN BEGIN
		openw,dlu,'~/srem_movie.log',/get_lun
		window,xsi=512,ysi=512
	ENDIF
	IF keyword_set(EXPCORR) THEN BEGIN
	    hi_prep,hdrs0,modelim, saturation_limit=16384
	    medhdr.exptime=1.
	ENDIF
	IF(~keyword_set(BMIN)) THEN bmin = -1000.*(medhdr.exptime/hdrs0.exptime)
	IF(~keyword_set(BMAX)) THEN bmax =  1000.*(medhdr.exptime/hdrs0.exptime)

	tmp = scc_findfile(hdrs[0].filename, TEL=tel, SIDE=side)
	; side is returned 
	help,side
	IF(tel EQ 'hi_2' AND datatype(mfilt) EQ 'UND') THEN mfilt = 1

    	date = hdrs[0].date_obs
    	IF (tel EQ 'hi_1' OR tel EQ 'hi_2') THEN t = 'hi' + strmid(tel,3,1) ELSE t = tel
    	fid=strmid(date,0,4) + strmid(date,5,2) + strmid(date,8,2) + '_' + t 
	IF(datatype(dir) EQ 'UND') THEN $  ;default save directory
		dir = fid + side+ '/'

    	IF datatype(diff_dir) EQ 'UND' THEN diff_dir='/tmp/'
	;stop
	path = diff_dir + fid + side
	IF((findfile(path + '*'))[0] EQ '') THEN BEGIN
	    cmd='mkdir ' + path
	    print,cmd
	    SPAWN, cmd,/sh
    	ENDIF
	
	IF(tel EQ 'hi_2' AND side EQ 'b' AND ~keyword_set(POS)) THEN BEGIN ; black out corners to mitigate flickering
		mask = bytarr(hdrs[0].naxis1, hdrs[0].naxis2)
		s=size(mask)
		xcen = hdrs[0].naxis1/2
		ycen = hdrs[0].naxis2/2
		dist = 1.2*(long(hdrs[0].naxis1/2))^2
		FOR i=0L,s[1]-1 DO FOR j=0L,s[2]-1 DO BEGIN
			IF((i-xcen)^2 + (j-ycen)^2 LT dist) THEN mask[i,j] = 1
		ENDFOR
	ENDIF

	count = 0
	LOADCT,0
	TVLCT,rr,gg,bb,/GET
	IF keyword_set(usebase) THEN BEGIN
		FOR i=1, nim-1 DO BEGIN
			count++   ;hdrs and imgs are hdr and img of baseframe
			print, 'Frame ', i, ' of ', nim-1
			print,list[i]
			sccload,list[i],hdr,img,/nolocal,expcorr=expcorr
			IF ABS(hdr.crota-hdrs0.crota) GT maxroll THEN BEGIN
			    print,''
	    		    message,'Skipping '+hdr.filename+' because ',/info
			    print,'CROTA= '+trim(hdr.crota)+' NE '+trim(hdrs0.crota)
			    wait,2
			    goto, nextfile1
			ENDIF 
			IF(keyword_set(POS)) THEN tmp_pos=pos
			IF ~quiet THEN printf,dlu,hdrs.filename+hdr.filename
			srem_img = RDIF_HI(imgs, img, rawdif=rawdir, min=min, hdr=hdr, noshift=noshift,side=side, tel=tel, local=local,$
				 nreg=nreg, scl=scl, box=box, med=med, model=modelim, pos=tmppos, mfilt=mfilt, debug=debug,deproject=deproject)
			IF(n_elements(srem_img) EQ 1) THEN RETURN
			hdr.filename=strmid(hdr.filename,0,16) + 's' + strmid(hdr.filename,17,8)
			hdr.polar=0
			IF(keyword_set(POS)) THEN BEGIN a=scc_crop_img(srem_img,hdr,pos,new_hdr,/NODATA)
				hdr=new_hdr
			ENDIF
			IF(tel EQ 'hi_2' AND side EQ 'b' AND ~keyword_set(POS) and ~keyword_set(DEPROJECT)) THEN $
			srem_img = mask * temporary(srem_img)
			IF keyword_set(NOSNOW) THEN BEGIN
		    		point_filter, srem_img, 5, 0.1, 3, b, z1 ; for c2 images
		 		c = -b
    		    		point_filter,c, 5, 0.1, 3, d, z2 ; for c2
		    		srem_img = -d ; desired output file    	    	    
			ENDIF
			wrfn='rdifhi'+string(i,format='(i03)')+'.fts'
			sccwritefits, path + '/'+wrfn, srem_img, hdr, silent=quiet
			newfile = [newfile, path + '/'+wrfn]
			IF(keyword_set(WRITE_PNG)) THEN BEGIN
				png_path = hdr.date_obs
				png_path = strmid(png_path,0,4) + strmid(png_path,5,2) + strmid(png_path,8,2) $
				+ '_' + strmid(png_path,11,2) + strmid(png_path,14,2) + '_HI2' + strupcase(side) + '.png'
				png_path = diff_dir + dir + png_path
				write_png, png_path, bytscl(srem_img >bmin <bmax), rr, gg, bb
				print, 'Image written to ' + png_path
			ENDIF
			IF(count EQ usebase) THEN BEGIN
				count=0
				imgs=img
				hdrs=hdr
			ENDIF
			nextfile1:
		ENDFOR
	ENDIF ELSE BEGIN
	IF(keyword_set(baseskip)) THEN BEGIN
	FOR i=0, nim-baseskip-1 DO BEGIN
		print, 'Frame ', i+1, ' of ', nim-baseskip
		print,list[i]
		sccload, list[i],hdrs,imgs,/nolocal,expcorr=expcorr
		sccload, list[i+baseskip],hdr,img,/nolocal,expcorr=expcorr
		IF ABS(hdr.crota-hdrs0.crota) GT maxroll THEN BEGIN
		    print,''
	    	    message,'Skipping '+hdr.filename+' because ',/info
		    print,'CROTA= '+trim(hdr.crota)+' NE '+trim(hdrs0.crota)
		    wait,2
		    goto, nextfile2
		ENDIF 
		IF ABS(hdrs.crota-hdrs0.crota) GT maxroll THEN BEGIN
		    print,''
	    	    message,'Skipping '+hdrs.filename+' because ',/info
		    print,'CROTA= '+trim(hdrs.crota)+' NE '+trim(hdrs0.crota)
		    wait,2
		    goto, nextfile2
		ENDIF 
		IF(keyword_set(POS)) THEN tmp_pos=pos
		IF ~quiet THEN printf,dlu,hdrs.filename+' '+hdr.filename
		srem_img=RDIF_HI(imgs,img,rawdif=rawdif,min=min,hdr=hdr,noshift=noshift,side=side,tel=tel,local=local,nreg=nreg,scl=scl,$
			box=box,med=med,model=modelim,pos=tmp_pos,mfilt=mfilt,debug=debug,deproject=deproject)
		IF(n_elements(srem_img) EQ 1) THEN RETURN
		hdr.filename=strmid(hdr.filename,0,16) + 's' + strmid(hdr.filename,17,8)
		hdr.polar=0
		IF(keyword_set(POS)) THEN BEGIN a=scc_crop_img(srem_img,hdr,pos,new_hdr,/NODATA)
			hdr=new_hdr
		ENDIF
		IF(tel EQ 'hi_2' AND side EQ 'b' AND ~keyword_set(POS) and ~keyword_set(DEPROJECT)) THEN $
		srem_img = mask * temporary(srem_img)
		IF keyword_set(NOSNOW) THEN BEGIN
		   	point_filter, srem_img, 5, 0.1, 3, b, z1 ; for c2 images
		 	c = -b
    		    	point_filter,c, 5, 0.1, 3, d, z2 ; for c2
		    	srem_img = -d ; desired output file    	    	    
		ENDIF

		wrfn='rdifhi'+string(i,format='(i03)')+'.fts'
		sccwritefits, path + '/'+wrfn, srem_img, hdr, silent=quiet
		newfile = [newfile, path + '/'+wrfn]

		IF(keyword_set(WRITE_PNG)) THEN BEGIN
			png_path = hdr.date_obs
			png_path = strmid(png_path,0,4) + strmid(png_path,5,2) + strmid(png_path,8,2) $
			+ '_' + strmid(png_path,11,2) + strmid(png_path,14,2) + '_HI2' + strupcase(side) + '.png'
			png_path = diff_dir + dir + png_path
			write_png, png_path, bytscl(srem_img >bmin <bmax), rr, gg, bb
			print, 'Image written to ' + png_path
		ENDIF
		nextfile2:
	ENDFOR
	ENDIF ELSE BEGIN
	j=0
	i=i0
	;FOR i=i0, nim-1 DO BEGIN
	REPEAT BEGIN
	;--Since some files in list might be skipped, need to separate list subscript from loop-action subscript.
	;  We will use i for the list subscript and j for loop-action. They are incremented separately.
		print, 'Making Frame ', i+1, ' of ', nim
		hdr1 =hdrs[            j MOD (diff+1)]
		hdr2 =hdrs[     (j+diff) MOD (diff+1)]
		imgs1=imgs[*,*,        j MOD (diff+1)]
		imgs2=imgs[*,*, (j+diff) MOD (diff+1)]
		hdr = hdr2
		IF ABS(hdr.crota-hdrs0.crota) GT maxroll THEN BEGIN
		    print,''
	    	    message,'Skipping '+hdr.filename+' because ',/info
		    print,'CROTA= '+trim(hdr.crota)+' NE '+trim(hdrs0.crota)
		    wait,2
		    goto, nextfile3
		ENDIF 
		IF(keyword_set(POS)) THEN tmp_pos = pos
		;IF ~keyword_set(USEDATE) and ~keyword_set(MODEL) THEN $
		;    	modelim = scc_getbkgimg(hdr, outhdr=medhdr,/match,/interpolate)
		printf,dlu,hdr1.filename+' '+hdr2.filename
		print,hdr1.filename+' '+hdr2.filename
		srem_img = RDIF_HI(imgs1, imgs2, rawdif=rawdif, min=min, $
			hdr=hdr, noshift=noshift, side=side, tel=tel, local=local, nreg=nreg, scl=scl, box=box, $
			med=med, model=modelim, pos=tmp_pos, mfilt=mfilt, debug=debug, deproject=deproject)

		IF(n_elements(srem_img) EQ 1) THEN RETURN
		; secchi_prep changes this character for some reason 6/8/07
		hdr.filename = strmid(hdr.filename, 0,16) + 's' + strmid(hdr.filename, 17,8)
		hdr.polar = 0  ; sccwritefits isn't working 6/7/07 with polar < 0
		IF(keyword_set(POS)) THEN BEGIN
			a = scc_crop_img(srem_img, hdr, pos, new_hdr, /NODATA)
			hdr = new_hdr
		ENDIF
		IF(tel EQ 'hi_2' AND side EQ 'b' AND ~keyword_set(POS) and ~keyword_set(DEPROJECT)) THEN BEGIN
			srem_img = mask * temporary(srem_img)
		ENDIF
		
		IF strmid(tel,0,2) EQ 'hi' THEN hi_fix_pointing,hdr
		
		IF keyword_set(NOSNOW) THEN BEGIN
		    point_filter, srem_img, 5, 0.1, 3, b, z1 ; for c2 images
		    ; 5-pix box, 0.1 intens., 3 iters., b output array, z1 array of pts.
		    ; removes positive points
		    c = -b
    		    point_filter,c, 5, 0.1, 3, d, z2 ; for c2
    	    	    ; removes negative points
		    srem_img = -d ; desired output file
		ENDIF
		
    	    	;hi_prep,hdr,srem_img ;,/silent  
    	    	; this normalizes im to 1 sec.
    	    	; this is done here because bkg does not have calibration
    	    	;hdr.exptime=1.0		
		wrfn='rdifhi'+string(i,format='(i03)')+'.fts'
		sccwritefits, path + '/'+wrfn, srem_img, hdr, silent=quiet
		newfile = [newfile, path + '/'+wrfn]

		IF(keyword_set(WRITE_PNG)) THEN BEGIN
			png_path = hdr.date_obs
			png_path = strmid(png_path,0,4) + strmid(png_path,5,2) + strmid(png_path,8,2) $
				+ '_' + strmid(png_path,11,2) + strmid(png_path,14,2) + '_'+tel + strupcase(side) + '.png'
			png_path = diff_dir + dir + png_path
			write_png, png_path, bytscl(srem_img >bmin <bmax), rr, gg, bb
			print, 'Image written to ' + png_path
		ENDIF
    	    	j=j+1
    	    	nextfile3:
		IF(i LT nim-1) THEN BEGIN
			;IF (local) THEN nextimg=sccreadfits(list[i+diff+1], hdr) $
			;    	   ELSE secchi_prep,list[i+diff+1],hdr,nextimg,/nocalfac
			fn=list[i+diff+1]
			IF keyword_set(EXPCORR) THEN BEGIN
			    secchi_prep,fn,hdr,nextimg, saturation_limit=16384 
			    hdr.exptime=1.
			ENDIF ELSE $
			    nextimg = sccreadfits(fn, hdr)
			trow= 20*( (i+diff+1) mod 10)
			IF ~quiet THEN BEGIN
			    nextimg[0:20,trow:(trow+19)]=0
			    help,diff,i
			ENDIF
			next2ndimage=(abs(j-1) MOD (diff+1))
			imgs[*,*, next2ndimage] = nextimg
			twofn[next2ndimage] = fn
			hdrs [next2ndimage] = hdr
		ENDIF
	;ENDFOR
	    	i=i+1
	ENDREP UNTIL i GE nim

	ENDELSE & ENDELSE
	newfile=newfile[1:*]
	newfile0=newfile[0]
	usehdrs=0

	IF keyword_set(RENORM) THEN BEGIN
	    n=n_elements(newfile)
    	    images=sccreadfits(newfile,hdrs)
	    nx=hdrs[0].naxis1
	    ny=hdrs[0].naxis2
	    medar=fltarr(nx,ny)
	    print,'Computing new median image...'
	    FOR i=0,nx-1 DO BEGIN
		FOR j=0,ny-1 DO BEGIN
	    	    medar[i,j]=median(images[i,j,*])
		ENDFOR
	    ENDFOR
	    newims=fltarr(nx,ny,n)
	    print,'Doing renorm...'
	    FOR i=0,n-1 DO newims[*,*,i]=images[*,*,i]-medar
	    newfile=newims
	    usehdrs=hdrs
	ENDIF
	
	;IF ~quiet THEN BEGIN
		close,dlu
		free_lun,dlu
	;ENDIF
	undefine, srem_img
	undefine, imgs
	IF(~keyword_set(NOMOVIE)) THEN BEGIN
		IF(~keyword_set(PAN)) THEN BEGIN
			IF(~keyword_set(POS)) THEN pan = 0.5 ELSE pan = 1.0
			print, 'Using default pan of ', pan, ' (keyword PAN)'
		ENDIF
		help,bmin,bmax
		outmvi=0
		IF keyword_set(AUTOSAVE) or nps EQ 2 THEN BEGIN
		    IF datatype(autosave) EQ 'STR' THEN outmvi=autosave ELSE outmvi=concat_dir(getenv('HOME'),fid+side+'.mvi')
		ENDIF
		IF keyword_set(notimes) THEN dotimes=0 ELSE dotimes=1
		
		GENERIC_MOVIE, newfile, bmin, bmax, pan=pan, times=dotimes, local=local, DEBUG=debug, SAVE=outmvi, HDRS=usehdrs
	ENDIF
	IF(~keyword_set(SAVE_FRAMES)) THEN BEGIN
		break_file, newfile0, disk, dir, name, ext
		spawn,'/bin/rm -r ' + dir
	ENDIF ELSE message,'Not deleting '+dir,/info
	pss=pss+1
ENDREP UNTIL (pss EQ nps)

;--DIsplay A/B movie
IF nps EQ 2 THEN BEGIN
    mvis=findfile(concat_dir(getenv('HOME'),fid+'*.mvi'))
    wscc_combine_mvi,mvis,/auto,bside=3
ENDIF

END
