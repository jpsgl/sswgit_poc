FUNCTION fdiff, img1, img2, xshift, yshift
; $Id: fdiff.pro,v 1.2 2009/04/23 19:33:25 nathan Exp $
;
; Purpose: shift one image by a fractional pixel amount before subtracting another from it
;
; $Log: fdiff.pro,v $
; Revision 1.2  2009/04/23 19:33:25  nathan
; update comments
;
s=size(img1)
a = rebin(img1, 2*s[1], 2*s[2], /sample)
b = rebin(img2, 2*s[1], 2*s[2], /sample)
d = shift(b, floor(xshift)*2 + 1, floor(yshift)*2 + 1) - a
xfrac = xshift - floor(xshift)
yfrac = yshift - floor(yshift)
diff = img1
;FOR i=0,s[1]-1 DO FOR j=0,s[2]-1 DO diff[i,j] = xfrac*yfrac*d[2*i,2*j] + (1-xfrac)*yfrac*d[2*i+1,2*j] $
;	+ xfrac*(1-yfrac)*d[2*i,2*j+1] + (1-xfrac)*(1-yfrac)*d[2*i+1,2*j+1]
RETURN, d

END
