; CHOOSE.PRO ; a program to choose every nth file

; assumes that there is a file called 'list' in the idl session
; output is a file called 'listout' in that idl session
; useful for changing the gain in running difference movies

; created by NRS sometime prior to the summer of 2008

; $Id: choose.pro,v 1.2 2008/08/27 19:08:13 sheeley Exp $

read,'enter n (=2 for everyother element) = ',n
; let's suppose that a list, called list, exists outside this procedure

   imax=n_elements(list)-1
   jmax=fix(imax/n)
 
b=strarr(jmax+1) 
for j=0,jmax do begin
   b(j) = list(n*j)
endfor

listout=b

end
    
