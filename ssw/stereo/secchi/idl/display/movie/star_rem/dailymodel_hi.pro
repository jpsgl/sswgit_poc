   pro dailymodel_hi, day, sc = sc, tel=tel, local=local, med=med, min=min
   
   ; $Id: dailymodel_hi.pro,v 1.3 2009/04/23 19:33:24 nathan Exp $
   ;
   ; Calling Sequence: dailymodel_hi, '20070201', sc='a'
   ; 
   ; $Log: dailymodel_hi.pro,v $
   ; Revision 1.3  2009/04/23 19:33:24  nathan
   ; update comments
   ;
   COMMON DIR_COMMON, mvi_dir, jmap_dir, median_dir, diff_dir, image_dir
   
   IF(NOT(keyword_set(SC))) THEN sc = 'a'
   sc = STRLOWCASE(sc)
   IF(NOT(keyword_set(TEL))) THEN tel = 'hi_1'
   tel = STRLOWCASE(tel)
   IF(tel EQ 'hi1' OR tel EQ 'hi2') THEN tel = 'hi_' + strmid(tel, 2)

   local = keyword_set(LOCAL)
   min = keyword_set(MIN)

   monthly = strlen(day) EQ 6
   IF monthly THEN day = day + '??'  ; find monthly median/minimum

   path = (local ? image_dir : getenv('secchi')) $
	+ '/lz/L0/' + sc + '/img/' + tel + '/' + day + '/' + day + '_??????_s4*.fts'
   files = findfile(path)
   n = n_elements(files)
   print, '%DAILYMODEL_HI: Found ', n, ' files'

;stop
     
   ; ---------------------- MODEL to remove F-corona ---------------
   ; it is necessary to remove the slow varying component
   ;
   ; I create here just a daily model for testing purposes -----


	; compute the median/minimum for predetermined blocks of the list and then merge these after
	; to avoid memory allocation failure 

	block = 50
	nb = n/block + 1
	IF(n MOD block EQ 0) THEN nb = nb-1
	img = sccreadfits(files[0],hdr)
	s=size(img)
	models = fltarr(s[1],s[2],nb)
	FOR i=0, nb-1 DO BEGIN

		first = i*block
		last = (i LT nb-1 ? (i+1)*block : n) - 1
		len = last-first+1
		IF len LE 0 THEN BREAK
		medians = fltarr(len)
	
		IF(local) THEN sccload, files[first:last], hdrs, imgs $
		ELSE secchi_prep, files[first:last], hdrs, imgs

		FOR j=0, len-1 DO medians[j] = median(imgs[*,*,j])
		medn = median(medians)

	   for j=0, len-1 do begin
		test = medn / medians[j]
		IF(test GT 1) THEN test = 1. / test
	        imgs[*,*,j] =  temporary(imgs[*,*,j]) * (test GT 0.99 ? 1. : medn / medians[j])
	   endfor
	   IF(keyword_set(MED)) THEN BEGIN
		model = fltarr(s[1],s[2])
		FOR j=0, s[1]-1 DO $
			FOR k=0, s[2]-1 DO model[j,k] = med(imgs[j,k,*], med)
	   ENDIF ELSE BEGIN
		IF(min) THEN model = MIN(imgs, DIMENSION=3) $
		ELSE model = MEDIAN(imgs, DIMENSION=3)
	   ENDELSE
	   models[*,*,i] = model
	   undefine, model
	   undefine, hdrs
	   undefine, imgs
	   print, 'done with block ', i+1, ' of ', nb
	ENDFOR

	IF(min) THEN model = MIN(models, DIMENSION=3) $
	ELSE model = MEDIAN(models, DIMENSION=3)
   ;--------------------------------------

;stop
   IF monthly THEN day = strmid(day, 0,6)
   IF(keyword_set(MED)) THEN BEGIN
	frac = strtrim(string(float(med)),2)
	suffix = '_' + strmid(frac, 0,1) + '_' + strmid(frac,2,2)
   ENDIF ELSE suffix = (min ? '_min' : '_med')
   name = day + sc + suffix + '.fts'
   writefits, median_dir + name, model, hdr
   
   end
   
   
