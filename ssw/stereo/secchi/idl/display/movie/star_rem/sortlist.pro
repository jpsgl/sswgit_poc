; PROGRAM SORTLIST.PRO

; created by NRS to make enhanced EIT movies

; assumes a file called 'list' is in the idl session

; asks for a lag
; lag is the desired running difference in frames 

; output is a file called 'newlist'

$Id: sortlist.pro,v 1.1 2008/08/27 19:17:07 sheeley Exp $

read, 'enter the lag = ',lag
imax=n_elements(list)-lag-1


newlist = strarr(2*imax+2)

for i = 0, imax do begin
   newlist(2*i) = list(i)
   newlist(2*i+1) = list(i+lag)
endfor

stop
end
