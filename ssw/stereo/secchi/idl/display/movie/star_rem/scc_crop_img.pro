;SCC_CROP_IMG -- extract a portion of an image and modify its header accordingly

FUNCTION scc_crop_img, img, hdr, coords, new_hdr, NODATA=nodata
; $Id: scc_crop_img.pro,v 1.1 2008/01/08 20:19:22 nathan Exp $
;
; $Log: scc_crop_img.pro,v $
; Revision 1.1  2008/01/08 20:19:22  nathan
; copied from adam/data_anal, last mod 7/11/07
;
new_hdr = hdr
new_hdr.naxis1 = coords[2]
new_hdr.naxis2 = coords[3]
new_hdr.crpix1 = new_hdr.crpix1 - coords[0]
new_hdr.crpix2 = new_hdr.crpix2 - coords[1]

x1 = float(coords[0]) / hdr.naxis1
x2 = float(coords[0]+coords[2]) / hdr.naxis1
y1 = float(coords[1]) / hdr.naxis2
y2 = float(coords[1]+coords[3]) / hdr.naxis2

IF(hdr.p2row - hdr.p1row GT 0) THEN BEGIN
	new_hdr.p1row = hdr.p1row + x1*(hdr.p2row+1 - hdr.p1row)
	new_hdr.p2row = hdr.p1row + x2*(hdr.p2row+1 - hdr.p1row) - 1
ENDIF
IF(hdr.p2col - hdr.p1col GT 0) THEN BEGIN
	new_hdr.p1col = hdr.p1col + y1*(hdr.p2col+1 - hdr.p1col)
	new_hdr.p2col = hdr.p1col + y2*(hdr.p2col+1 - hdr.p1col) - 1
ENDIF
IF(hdr.r2row - hdr.r1row GT 0) THEN BEGIN
	new_hdr.r1row = hdr.r1row + x1*(hdr.r2row+1 - hdr.r1row)
	new_hdr.r2row = hdr.r1row + x2*(hdr.r2row+1 - hdr.r1row) - 1
ENDIF
IF(hdr.r2col - hdr.r1col GT 0) THEN BEGIN
	new_hdr.r1col = hdr.r1col + y1*(hdr.r2col+1 - hdr.r1col)
	new_hdr.r2col = hdr.r1col + y2*(hdr.r2col+1 - hdr.r1col) - 1
ENDIF

IF(~keyword_set(NODATA)) THEN new_img = img[coords[0]:coords[0]+coords[2]-1, coords[1]:coords[1]+coords[3]-1] $
ELSE new_img = -1

RETURN, new_img
END
