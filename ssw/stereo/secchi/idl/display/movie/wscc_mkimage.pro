;+
;$Id: wscc_mkimage.pro,v 1.1 2009/03/10 18:52:18 mcnutt Exp $
;
; Project     : SECCHI
;                   
; Name        : wscc_mkimage
;               
; Purpose     : Widget front end to WSCC_MKMOVIE.PRO for single image
;               
; Explanation : This tool allows the user to select scc_mkmovie options with
;		a widget interface. 
;
; Use         : IDL> wscc_mkimage [, list, /bkgrnd]
;
;   	Example  : IDL> wscc_mkimage		 
;   	;Without any inputs, program will call scclister.pro
;
; Optional Inputs:
;   	list	SECCHI FITS filename with path   if an array of then 0=bkgrnd (difference images only) and 1 = image to be displayed
;
; KEYWORDS: 	/bkgrnd  if set will call scclister to select background image
;               
; Calls       : WSCC_MKMOVIE.PRO, SCCLISTER.PRO
;
; Side effects: if not creating a difference image and two fits files are given it will produce a 2 frame movie
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson, NRL March 2009.
;               
;	
;-            
;$Log: wscc_mkimage.pro,v $
;Revision 1.1  2009/03/10 18:52:18  mcnutt
;create a single image frame into scc_wrunmovie and then can be displayed with scc_wrunmoviem
;
pro wscc_mkimage,img,bkgrnd=bkgrnd


if datatype(img) ne 'UND' then list=sccfindfits(img) else begin
       arg1= SCCLISTER(/AORB)
        tn= tag_names(arg1)
	inp=0
        status= execute("tfn= arg1."+tn(inp))
        files= tfn
        list=tfn
endelse

if keyword_set(bkgrnd) then begin
  if bkgrnd ne 1 then bname=sccfindfits(bkgrnd) else begin
       arg1= SCCLISTER(/AORB)
        tn= tag_names(arg1)
	inp=0
        status= execute("tfn= arg1."+tn(inp))
        files= tfn
        list=tfn
  endelse
  list=[bname,list]
endif
print,list

wscc_mkmovie,list,/single

return
end
