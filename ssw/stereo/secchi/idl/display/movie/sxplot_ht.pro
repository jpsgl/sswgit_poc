;+
; $Id: sxplot_ht.pro,v 1.16 2014/05/05 17:24:58 nathan Exp $
; NAME:
;	sXPLOT_HT
;
; PURPOSE:
;	This procedure is used to display height-time curves. It reads in a
;       height-time file created by one of the movie programs and generates
;       a plot.
;
; CATEGORY:
;	MOVIE
;
; CALLING SEQUENCE:
;	sXPLOT_HT
;
; INPUTS:
;
; OPTIONAL INPUT PARAMETERS:
;	Filename:	If filename is present then it is used immediately
;
; KEYWORDS:
;   	/NODELTA    Always assume data is in plane of sky (delta=0)
;
; OUTPUTS:
;	A plot is generated on the screen, and optionally a print file is
;	generated of the form idlplot.psnnn, where nnn is a sequential
;	number, or a GIF file, or a text file with speed at each point.
;
; OPTIONAL OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
;	scom_xplot_ht
;
; SIDE EFFECTS:
;	Initiates the XMANAGER if it is not already running.
;
; RESTRICTIONS:
;
; PROCEDURE:
;	The various widgets are set up and registered.  The user selects the
;	height-time file to be processed.  The file is read in and the data
;	points plotted.  The user is then able to fit the data to polynomial
;	functions of degree 1,2, or 3.  The plot can be printed.  The speeds
;	can be saved to a file.
;
; MODIFICATION HISTORY:
; $Log: sxplot_ht.pro,v $
; Revision 1.16  2014/05/05 17:24:58  nathan
; compute correct delta for large CRLT
;
; Revision 1.15  2012/04/27 14:05:21  mcnutt
; changed  (bang)delimiter to get_delim
;
; Revision 1.14  2011/03/25 18:37:47  nathan
; added /nodelta option
;
; Revision 1.13  2010/12/28 15:50:47  mcnutt
; corrected feature selection in ht_splot
;
; Revision 1.12  2010/12/27 16:40:24  nathan
; rename feat->featid; do not filter invalid dates
;
; Revision 1.11  2010/04/27 15:17:05  nathan
; clarify y.title for distance plot
;
; Revision 1.10  2010/02/26 19:29:26  nathan
; fix source region CRLN input
;
; Revision 1.9  2010/01/20 22:27:10  nathan
; Choose units of y-axis; correct speed save file; blob mode; better info.
;
; Revision 1.8  2010/01/15 22:01:45  nathan
; change speed file output
;
; Revision 1.7  2010/01/14 20:59:57  nathan
; forgot stop
;
; Revision 1.6  2010/01/14 20:53:55  nathan
; Now computes everything based on linear (not angular) distance. Assumes
; out-of-sky-plane angle and allows input of source region to define
; out-of-sky-plance angle. Have not tested blob mode or user-defined
; function.
;
; Revision 1.5  2009/06/12 18:38:00  nathan
; this is not quite done yet; revert to rev 1.4 for working version
;
; Revision 1.4  2008/10/23 20:08:06  nathan
; replace s/c names for parse_stereo_name
;
; Revision 1.3  2008/07/23 17:04:33  nathan
; started implementing pro change_angle (see wdeltanum,wdeltaname widgets)(Bug 320)
;
; Revision 1.2  2008/05/05 18:26:47  mcnutt
; added units from sccread_ht to titles
;
; Revision 1.1  2008/01/14 21:56:11  nathan
; moved from dev/movie
;
; Revision 1.5  2008/01/14 21:13:52  nathan
; rename common blocks and pros to not conflict with xplot_ht.pro
;
; Revision 1.4  2007/11/15 19:02:52  nathan
; commented out wset,winsave because was getting frame from wrunmoviem
;
; Revision 1.3  2007/10/19 14:54:48  nathan
; put Id label on widget
;
; Revision 1.2  2007/10/19 14:23:19  nathan
; cosmetic enhancements
;
; Revision 1.1  2007/10/19 14:13:00  nathan
; copied from NRL_LIB/lasco/movie and renamed
;
; 	Written by:	Scott H. Hawley, NRL Summer Student, June 1996
;	Version 2	RA Howard, NRL, Modified plot calls to use utplot
;	15 Oct 96	RAH, widgetized
;       27 Oct 96       RAH, Corrected overplots of interpolated values
;                            Set new window number before plotting
;       08 Nov 96       RAH, Corrected situation if called without argument
;       10 Nov 96       RAH, Corrected Acceleration = 2*fit_coeff
;       11 Nov 96       RAH, Added plot of position angles
;       04 Apr 97       RAH, Force first smoothed time to be at least first observed time
;	10 Jan 96	SHH, Added custom curve fit
;			     Protected against null filenames 
;       10 Apr 97       RAH, Permit negative velocities to be plotted
;       30 Sep 97       RAH, Modified call to READ_HT
;       03 Oct 97       SEP, added ability to save to .ps file
;       10 Dec 97       SEP, added user & timestamp to .ps file and hardcopy
;       24 Dec 97       RAH, Plot time start includes the extrapolated time to limb
;       03 Feb 98       RAH, Plot individual feature codes
;       10 Aug 98       RAH, Add path name to file name
;
; @(#)xplot_ht.pro	1.16 07/18/00 :LASCO IDL LIBRARY
;
;-

pro sdo_plot
;
;  section to plot the height-time curves and overplot of the curve fits
;
common scom_xplot_ht,wcontrolbase,wcurrhtlabl,whunits, filename,htname,fit,  $
                    wlimblon,winnum,fac_rsec, uts,rs,tsint,degree, $
                    tint,rint,vel,tai,date_obs,time_obs,comment,pa, $
                    winsave,last_plot,wcustname,wcustparams,$
                    wblobbutton,blobmode,sc,hunits,debugon,rs0,rsun, $
		    wlatnum, wlonnum, wsrctime, wopbuttonval, delta, unitopts, nodeltaset
common scom_feat,featid,feat_desc,featnames,wfeatbase,wfeatselect,featval,featst

;widget_control,wopbutton,GET_VALUE=val 
dooplot=wopbuttonval and (last_plot EQ 'ht')
;help,dooplot

!x.style=1
!y.style=3
  !x.title = 'Time (UT)'
  !y.title = 'Distance from Sun Surface ('+hunits+')'	; units of rs should be KM

  !p.title = filename+': '+comment[0]+', Feature= '+featst
  !p.multi = [0,1,degree]
  IF (!d.name NE 'PS') AND (winnum GT -1) THEN wset,winnum
  ;maxr=FIX(MAX(rs)) > 30
IF debugon THEN help,tsint,rint,uts,reltimes,fullrs,fullts,hunits,rs
IF debugon THEN print,rs
  IF (blobmode) THEN BEGIN
        reltimes=(tai-MIN(tai))/3600.
        IF (MAX(reltimes) gt 25) THEN MESSAGE,'Maximum time exceeds 25 hours.',/CONT
    	IF (dooplot) THEN oplot,reltimes,rs,psym=2 ELSE $
        PLOT,reltimes,rs,psym=2, XTITLE='Time (hours)',TITLE=filename+': '+date_obs+' '+time_obs
        OPLOT,tint/3600.,rint,psym=0
  ENDIF $
  ELSE BEGIN
     IF (dooplot) THEN oUTPLOT,tsint,rint,psym=0 ELSE $
     UTPLOT,tsint,rint,psym=0 
     OUTPLOT,uts,rs,psym=2
     !p.title=''
  ENDELSE
  ;wait,2

  np = n_elements(uts)
  !x.title='Distance from Sun ('+hunits+')'
  
  IF (degree gt 1) THEN BEGIN
     velfit=fit(1:degree)*(findgen(degree)+1.0)		; 1st deriv wrt time
     vel= poly((tai-tai(0)),velfit)	; interpolated speeds, km/sec
     velint = poly(tint,velfit)		; interpolated speeds, 
     absvel = ABS(vel)
     maxv=FIX(MAX(ROUND(absvel/100.)))
     maxv=100*(maxv+1)
     ;!x.range=[0,30]
     ;IF (maxv lt 500)  THEN maxv=500
     IF (MAX(vel) EQ MAX(absvel)) THEN BEGIN
        minv = 0
        IF (blobmode) THEN BEGIN
           IF (maxv gt 500) THEN $
              MESSAGE,'Warning: Maximum speed exceeds 500 km/s.',/CONT
           maxv = 500
           !x.range = [0,30]
        ENDIF
     ENDIF ELSE BEGIN
        IF (blobmode) THEN BEGIN
           IF (maxv gt 500) THEN $
              MESSAGE,'Warning: Maximum speed exceeds -500 km/s.',/CONT
           minv = -500
           !x.range = [0,30]
        ENDIF ELSE minv = -maxv
        maxv = 0
     ENDELSE
     !y.range = [minv,maxv]
     !y.title = 'Velocity (km/s)'
     IF (dooplot) THEN oPLOT,rs,vel,psym=2 ELSE $
     PLOT,rs,vel,psym=2
     OPLOT,rint,velint
     !x.range=''
     !y.range=''
     !p.title=''
     IF (degree gt 2) THEN BEGIN
        accfit=velfit(1:degree-1)*(findgen(degree-1)+1.0)   ; second derivative
        !y.title = 'Acceleration (km/s^2)'
        PLOT,rint,poly(tint,accfit)
     ENDIF ELSE BEGIN
         accel = 2*fit(degree)
	 
         str = STRING(accel*1000,format='(f6.2)')+' m/s!E2!X'
     	 IF ~(dooplot) THEN xyouts,0.5,0.12,'delta = '+string(delta,format='(f6.2)')+'!9%!3!X',/normal,charsize=1.3
         IF ~(dooplot) THEN xyouts,0.5,0.16,'Avg Position Angle = '  +STRING(avg(pa),   format='(f6.2)')+'!9%!3!X',/normal,charsize=1.3
         IF ~(dooplot) THEN xyouts,0.5,0.20,'Acceleration = '+str,/normal,charsize=1.3
     ENDELSE
  ENDIF ELSE BEGIN
     speed = fit(degree)
     str = STRING(speed,format='(f8.1)')+' km/s'
     ypos = .12
     ydel = .035
     IF ~(dooplot) THEN xyouts, 0.5,ypos,'delta = '+string(delta,format='(f6.2)')+'!9%!3!X',/normal,charsize=1.3
     ypos = ypos+ydel
     IF ~(dooplot) THEN xyouts,0.5,ypos,'Avg. Position Angle = '+STRING(avg(pa),format='(f6.2)')+'!9%!3!X',/normal,charsize=1.3
     ypos = ypos+ydel
     IF ~(dooplot) THEN xyouts,0.5,ypos,'Velocity = '+str ,/normal,charsize=1.3
     ypos = ypos+ydel
     
     ;IF ~(dooplot) THEN xyouts,0.5,ypos,comment,/normal,charsize=1.3
     ;ypos = ypos+ydel
  ENDELSE
  
  !x.title=''
  !y.title=''
  !p.title=''
  !P.multi=''
  IF (!D.NAME EQ 'PS') THEN BEGIN 
     SPAWN, 'whoami', user, /NOSHELL
     user = STRTRIM(user(0),2)
     SPAWN,'hostname',machine, /NOSHELL
     machine = STRTRIM(machine(0),2)
     GET_UTC, utc
     utc = UTC2STR(utc, /ECS, /TRUNCATE)
     IF ~(dooplot) THEN XYOUTS, 1.0, 0.0, user+'@'+machine+' '+utc, /NORMAL, CHARSIZE=0.7, ALIGNMENT=1.0
  ENDIF
  ;IF (!d.name NE 'PS') AND (winsave GT -1)  THEN WSET,winsave
  
RETURN
END

pro sht_print,lpname
;
;  prints the current plot onto the printer designated by lpname
;
common scom_xplot_ht
		    
  ;???   IF (last_plot NE 'pa') AND (last_plot NE 'ht')  THEN RETURN  
IF (not blobmode) THEN ps_setup,0,lpname $
ELSE BEGIN
     ps_setup,0
     DEVICE,/PORTRAIT,XSIZE=6.5,YSIZE=9.5,/INCHES,YOFFSET=0.75
     !x.range=[0,30]       	; impose a constant axis range for comparison
ENDELSE
CASE last_plot OF
       'pa':  pa_splot
       'ht':  ht_splot                   ; do the plot
       'custom': custom_splot
ENDCASE
pstst = STRPOS(lpname, '.ps')
IF (pstst[0] GT 0) THEN BEGIN
        print,'Saving ',lpname
	ps_setup, 3, lpname
ENDIF ELSE $
    IF (strpos(lpname,'lp') EQ -1 and lpname NE '') THEN ps_setup,4,lpname,/gif $
    ELSE BEGIN
        IF (lpname EQ '') OR (STRLOWCASE(lpname) EQ 'lp') $
        THEN ps_setup,1 ELSE ps_setup,1,lpname
    ENDELSE
!x.range=''
RETURN
END

pro ht_splot, RESET=reset, DOPA=dopa, VFILE=vfile
;
;   fits the data to a polynomial of the input degree and plots the
;   curve over the current plot
;
; Keywords: /RESET  Compute default out-of-sky angle
;   	    /DOPA     Plot position angle(s)
;   	    VFILE=  Save instantaneous velocity at each point to this filename
;
common scom_xplot_ht
common scom_feat
                     
;IF strmid(hunits,0,1) EQ 'R' THEN fac_rsec= onersun()/3600. $   ; factor to convert Rsun/hour to km/sec
;ELSE stop

  ;IF (!d.name EQ 'X')  THEN  erase
  fullts = uts
  fullrs = rs0	; rs0 is always RADIANS 
  fulltai = tai
  fullpa = pa
  fullfeat=featid
  sz = SIZE(tint)
  IF (sz(0) NE 0)  THEN BEGIN
     fulltint = tint
     fulltsint = tsint
     fullrint = rint
  END
;
;  If all features are not to be plotted then select out the subset
;
  IF (featval(0) EQ 0)  THEN BEGIN
     selected = WHERE (featval EQ 1,nselected)
     IF (nselected GT 0)  THEN BEGIN
        FOR iif=0,nselected-1 DO BEGIN
            jf = selected(iif)		; ordinal number of the selection (including ALL)
            w  = WHERE (featnames(jf) EQ STRMID(feat_desc,0,5),nw)
	    	    	    	    	; feat_desc does not include ALL
            IF (nw EQ 1)  THEN BEGIN
               featnum = w(0)
               w = WHERE (fullfeat EQ featnum,nw)
	       print,'Found ',trim(nw),' values for ',feat_desc[featnum]
	       IF debugon THEN wait,5
               IF (nw GT 0)  THEN BEGIN
                  IF (iif EQ 0)  THEN BEGIN
                     uts = fullts(w)
                     rs0 = fullrs(w)
                     tai = fulltai(w)
		     pa  = fullpa[w]
		     featid= fullfeat[w]
;                     tsint = fulltsint(w)
;                     rint = fullrint(w)
                   ENDIF ELSE BEGIN
                     uts = [uts,fullts(w)]
                     rs0 = [rs0,fullrs(w)]
                     tai = [tai,fulltai(w)]
		     pa  = [pa, fullpa[w]]
		     featid= [featid, fullfeat[w]]
;                     tsint = [tsint,fulltsint(w)]
;                     rint = [rint,fullrint(w)]
                   ENDELSE
               ENDIF
            ENDIF ELSE BEGIN
               PRINT,'sdo_plot:  ERROR in finding the feature description:  ',featnames(jf)
            ENDELSE
        ENDFOR
     ENDIF
  ENDIF
   numpts = n_elements(uts)
   help,numpts

; Derive Feature Label featst
     w = WHERE (featval GT 0, nw)
     IF (nw GT 0)   THEN BEGIN
        featst = ''
        i = 0
        WHILE (nw NE i) DO BEGIN
           IF (i GT 0)  THEN featst=featst+', '
           featst = featst+featnames[w[i]]
           i = i+1
        ENDWHILE
     ENDIF

; 
    change_angle, RESET=reset
; rs0 is original radians of selected features
; rs is now KM adjusted for delta, same dim. as rs0

;
; always do linear fit to get the extrapolated limb time
;
   fit=poly_fit((tai-tai(0)),rs,1) ; per second
   rmin = 0  ; minimum height to extrapolate to
   offset = (rmin-fit(0))/fit(1)<0              ; force offset to be <=0
    PRINT,'Speed from linear fit (km/s):  ',fit(1)
;wait,2
;
;  now fit to desired degree
;
   fit=poly_fit((tai-tai(0)),rs,degree) ; per second
   str='Fit = '+string(fit(0))+' + '+string(fit(1))+'x'
   IF (degree ge 2) THEN FOR i=2,degree DO $
         str=str+' + '+string(fit(i))+'x^'+strtrim(string(i),2)
   print,str
;
;  Compute an array of evenly spaced times beginning at the time
;  of projected initiation from rmin
;  tint is time in hours from the start time
;
   t0 = MIN(uts.mjd)
   t1 = MAX(uts.mjd)+1
   t0 = MIN(tai)+offset
   t1 = MAX(tai)+3600
   tint = findgen(100)/100.*(t1-t0) +offset
;
;  rint is an array of interpolated heights at the evenly spaced times
;
   rint = poly(tint,fit)
;
;  tsint is an array of CDS time structures for the evenly spaced times
;
   tsint = TAI2UTC(tint+t0-offset)

; 
;  Convert to desired units
;
widget_control, whunits, GET_VALUE=unitval 
hunits=unitopts[unitval]
Case hunits of
;unitopts=['KM','R/Rsun','Degrees']
    '1M KM': BEGIN
    	rs=rs/1.e6 
	rint=rint/1.e6
    END	; 
    'R/Rsun': BEGIN
     	rs=rs/onersun()
	rint=rint/onersun()
    END
;    'Degrees': BEGIN
;	stop
;    	rs=rs0*180/!pi
;    END
    ELSE:
ENDCASE
print,'Plotting Height in units of ',hunits,'. (Velocity is always km/s.)'
IF keyword_set(DOPA) THEN BEGIN
    pa_splot
ENDIF ELSE $
IF keyword_set(VFILE) THEN BEGIN
    sht_save,vfile
ENDIF ELSE BEGIN
    sdo_plot
    last_plot='ht'
ENDELSE ; Not PA plot
;
;  Restore the full data points
;
   uts = fullts
   rs0 = fullrs	    
   tai = fulltai
   pa  = fullpa
   featid= fullfeat
   sz = SIZE(fulltint)
   IF (sz(0) NE 0)  THEN BEGIN
      tint = fulltint
      tsint = fulltsint
      rint = fullrint
   ENDIF
RETURN
END

pro pa_splot
;
;  Plots the position angle vs R
;
common scom_xplot_ht
common scom_feat
		    
  !y.title = 'Position Angle (Deg)'
  !x.title = 'Distance from Sun ('+hunits+')'

  !p.title = filename+': '+comment[0]+', Feature = '+featst
  !p.multi = [0,1,1]
  IF (!d.name NE 'PS') AND (winnum GT -1)  THEN wset,winnum
  PLOT,rs,pa,psym=2 ,/ynozero, xrang=[0,max(rs)]
  !x.title=''
  !y.title=''
  !p.title=''
  !P.multi=''
  IF (!d.name NE 'PS') AND (winsave GT -1)  THEN WSET,winsave
  last_plot='pa'
RETURN
END

pro change_angle, RESET=reset
;
; adjust height relative to angle out of solar sky plane
;
; Keywords: /RESET  Compute default out-of-sky angle
;
common scom_xplot_ht

IF (nodeltaset) THEN delta=0 ELSE BEGIN
    widget_control, wlatnum, GET_VALUE=latval & lat=float(latval[0])
    widget_control, wlonnum, GET_VALUE=lonval & lon=float(lonval[0])
    help,lon,lat
    widget_control, wsrctime, GET_VALUE=srctime 
    dobs0=date_obs+'T'+strmid(time_obs,0,8)
    IF strlen(srctime[0]) LT 6 or keyword_set(RESET) THEN srctime=dobs0
    srcutc=anytim2utc(srctime[0])
    srctai=utc2tai(srcutc)
    avgrs=avg(rs0)*180/!pi	; Degrees
    avgpa=avg(pa)	    	; Degrees

    ; 216.229 2008-04-26T13:56:15   8.2 EUVI 
    dt = (tai-srctai)/86400. ; days
    arr=get_stereo_lonlat(srcutc,sc,system='carrington',/degr)
    crln=arr[1]	; this is CRLN of center of disk from the spacecraft at srctime
    crlt=arr[2]
    IF crln LT 0 THEN crln=crln+360
    print,'CRLN_OBS=',string(crln,'(f6.2)')
    print,'CRLT_OBS=',string(crlt,'(f6.2)')

    arr0=get_stereo_lonlat(dobs0,sc,system='carrington',/degr)
    crln0=arr0[1]
    IF crln0 LT 0 THEN crln0=crln0+360

    ;--Compute Limb Long
    IF avgpa LT 180 THEN limblon=crln-90 ELSE limblon=crln+90
    IF limblon LT 0 THEN limblon=limblon+360
    IF limblon GT 360 THEN limblon=limblon-360
    widget_control,wlimblon,set_value=string(limblon,'(f5.1)')

    ;--Compute out-of-solar-plane angle
    IF sc EQ 'A' THEN BEGIN
	crlnrate=-13.146544 
    ; For ST-A, CRLN-OBS varies by -13.146544 deg/day
    ; --These are elongation of CRPIX of FFV:
	    ; hi1-a = 14  ,  89.5  x deg at y from north 
	    ; hi1-a = 19.8, 110.1 
	    ; hi1-a = 14.0,  94.9 
	    ; hi1-a = 14.0,  91.4
	    ; hi1-a = 14.0,  83.9

	    ; hi1-b = 14.0, 276.2

	    ; hi2-a = 53.5,  95.5
	    ; hi2-a = 53.5,  91.7
	    ; hi2-a = 53.5,  82.8

	    ; hi2-b = 53.6, 276.5
	    ; hi2-b = 53.6, 274.4


    ENDIF ELSE IF sc EQ 'B' THEN BEGIN
	crlnrate=-13.267435 
    ; For ST-B, CRLN-OBS varies by -13.267435 deg/day
    ENDIF ELSE BEGIN
	crlnrate=-13.21 	; earth
    ENDELSE


    IF lon LT 0 or keyword_set(RESET) THEN BEGIN

	print,''
	print,'Assuming propagation vector perpendicular to vector of first measurement=',string((rs0[0]*180/!pi),'(f6.2)'),' deg'
	print,'and in the plane of the average position angle=',string(avgpa,'(f6.2)'),' deg at ',srctime,'.'
	print,'To change, enter Source Region location and time.'
	print,'CRLN_OBS at ',dobs0,' is ',trim(crln0),' deg.'
	print,''
	wait,2

	IF avgpa LE 180. THEN lat=90.-avgpa ELSE lat=-(270-avgpa)
	lon=crln[0]-(90-avgrs)*sin(avgpa*!pi/180)
	IF lon LT 0 THEN lon=360.+lon
	IF lon GT 360 THEN lon=lon-360.

	widget_control, wlatnum, SET_VALUE=lat
	widget_control, wlonnum, SET_VALUE=lon
	widget_control, wsrctime, SET_VALUE=srctime 
    ENDIF

    ; CRLN decreases going LEFT!

    ;IF lon LT 180 THEN lon= lon+360
    dlon=crln+90-lon
    dlat=lat-crlt
    help,dlon,dlat
    delta = (180/!pi)*asin(sin(dlon*!pi/180)*cos(dlat*!pi/180))  ; out-of-solar-plane (positive toward observer) angles
    help,delta

    IF abs(delta) GT 90 THEN delta = 90 + (crln-lon)

ENDELSE
; need sun-earth distance; can compute from rsun

rsunkm=onersun()
dobs= (rsunkm * 3600 * 180/!dpi)/rsun    ; km

if debugon then begin
    help,avgpa,avgrs,lon,lat,crln
    ;print,crln[0]
    help,dt
    print,dt[0],' days'
    help,srctai,delta,rsun,dobs
    ;maxmin,delta
    wait,2
endif

;--If input time is greater than start date or less than start date - 14 days
;  then do not do projection
IF 0 THEN BEGIN
;IF dt[0] lt -0.01 or dt[0] GT 14 THEN BEGIN
    print,srctime,' is not a valid date'
    delta=0.
ENDIF 

delta1 = delta*!dpi/180 ; delta1 is now radians

; rs0 is in radians
rs = (dobs * sin(rs0)/cos(rs0-delta1)) - rsunkm
; ref. Sheeley, N.R., Jr., et al. 2008, ApJ, 675, 860
; rs now in units of km
;hunits='KM'	**** rs should either be in KM, or = radians when input to change_angle
;rs = rs/onersun()
;hunits='R/Rsun'

;rst = dobs * cos(!pi/2 - delta) - dobs * sin(!pi/2 - delta)/tan(rs0 - !pi/2 + delta)

end

pro custom_splot
;
; plots according to a user-defined procedure, which has either already
;  been compiled, or resides in a .pro file where IDL can find it
;
common scom_xplot_ht

  WIDGET_CONTROL,wcustname,GET_VALUE=functname
  WIDGET_CONTROL,wcustparams,GET_VALUE=functparams
  functname=functname(0)
  functparams=functparams(0)

  IF (functname eq '') THEN BEGIN
     MESSAGE,'Must provide a function name',/CONT
     RETURN
  END
  IF (functparams eq '') THEN BEGIN
     MESSAGE,'Must provide paramaters for function.',/CONT
     RETURN
  END

  !P.multi=[0,1,2]
  IF (!d.name NE 'PS') AND (winnum GT -1)  THEN wset,winnum
  a=DOUBLE(STR_SEP(functparams,','))
           
        ts2=tai-tai(0)
        x = ts2
        y = rs
        w = y
        w(*) = 1.
           
        FOR kk = 0,100 DO BEGIN
           yfit=curvefit(x,y,w,a,sigma_a,function_name=functname)
           IF (kk gt 98) THEN PRINT,a
           IF (kk gt 98) THEN PRINT,sigma_a
           IF (kk gt 98) THEN PRINT,sigma_a/a
        ENDFOR
            
                     
        ts2_extra=findgen(100)/100.*(ts2(N_ELEMENTS(ts2)-1)-ts2(0))+ts2(0)
           
        ;fsmooth=a(0)+(2./a(1))*alog(.5*(1.+a(2)/a(3))*exp(a(1)*a(3)*ts2_extra/2.)+ $
        ;        .5*(1.-a(2)/a(3))*exp(-a(1)*a(3)*ts2_extra/2.))
        CALL_PROCEDURE,functname,ts2_extra,a,fsmooth
           
           
        PLOT,ts2,rs,psym=2,xtitle='Time (hours)',ytitle='Distance '+hunits,$
            TITLE=filename+': '+date_obs+'  '+time_obs
        OPLOT,ts2_extra,fsmooth
                   
        ; calculate the observed speeds
        rss = fltarr(n_elements(ts2)-4)
        vss = rss
           
        FOR jj = 0, n_elements(ts2)-5 DO BEGIN
           k = jj + 2
           ; compute the four averages:
           xm = (ts2(k-2)+ts2(k-1)+ts2(k)+ts2(k+1)+ts2(k+2))/5.
           ym = (rs(k-2)+rs(k-1)+rs(k)+rs(k+1)+rs(k+2))/5.
           x2m = (ts2(k-2)^2+ts2(k-1)^2+ts2(k)^2+ts2(k+1)^2+ts2(k+2)^2)/5.
           xym = (ts2(k-2)*rs(k-2)+ts2(k-1)*rs(k-1)+ts2(k)*rs(k)+ $
                  ts2(k+1)*rs(k+1)+ts2(k+2)*rs(k+2))/5.
           
           ; compute the slope m = vss
           vss(jj) = (xym-xm*ym)/(x2m-xm^2)
           rss(jj) = rs(k)
           
        ENDFOR
           
        vss=vss*6.96E5/3600.
                 
        PLOT,rss,vss,psym=2, xtitle='Distance '+hunits,ytitle='Velocity (km/s)'
           
        ; now plot the fitted curve of the form v^2 = exponential etc.
;***** SOMEONE NEEDS TO DECIDE WHAT TO DO WITH THIS.  -SHH, 01/10/97
        rmax=max(rs)
        rmin=min(rs)
   ;     jj=fix(100.*(rmax-2.))
   ;     rs_extra=2.+.01*findgen(jj)
        jj=fix(100.*(rmax-rmin))
        rs_extra=rmin+.01*findgen(jj)
        ;v= ;???
        ;OPLOT,rs_extra,v
                   
        ;print the fitted values
        ;XYOUTS,205,45,'r0 = '+strtrim(a(0),2)+',   Rc = '+$
        ;    strtrim(1./a(1),2)+',  v0 = '+strtrim(a(2)*6.96E5/3600.,2)+' km/s,'+$
        ;    ',  va = '+strtrim(a(3)*6.96E5/3600.,2)+' km/s',/device,charsize=1     
           
        
        ;PRINT,a2008-04-26T13:56:15
        ;PRINT,'r0 = ',a(0)
        ;PRINT,'Rc = ',1./a(1)
        ;PRINT,'v0 = ',a(2)*6.96E5/3600.
        ;PRINT,'va = ',a(3)*6.96E5/3600.
   last_plot='custom'
RETURN
END

pro sht_save,velname
;
;  saves the velocity information into a file
;
common scom_xplot_ht
common scom_feat

  IF (velname EQ '')  THEN RETURN
  print,' saving velocities to '+velname
  OPENW,lu,velname,/GET_LUN,/APPEND
  
  printf,lu,'# '+filename+': '+comment[0]+', Feature= '+featst

         accel = 2*fit(degree)
	 
         str = STRING(accel*1000,format='(f6.2)')+' m/s^2'
     	 printf,lu,'# Delta =              '+string(delta,format='(f6.2)')+' deg'
         printf,lu,'# Avg Position Angle = '+STRING(avg(pa),   format='(f6.2)')+' deg'
         printf,lu,'# Acceleration =       '+str
	 printf,lu,'# Unit of Height:      ',hunits
	 printf,lu,'# Unit of Velocity:     km/sec'
;
; Compute the speed for each of the input times
;
  np = n_elements(rs)
  IF (degree GT 1)  THEN BEGIN
     velfit=fit(1:degree)*(findgen(degree)+1.0)		; 1st deriv wrt time
     vel = poly((tai-tai(0)),velfit)
  ENDIF ELSE BEGIN
     vel = fltarr(np)+fit(1)
  ENDELSE
  
  fstring='(a19,f13.2,f9.2,a8)'  
  PRINTF,lu,	'# TIME                    HEIGHT VELOCITY FEATURE
	     ;   2000/03/09 06:45:06 123456789.12 12345.78 1234567
  FOR i=0,np-1 DO PRINTF,lu,utc2str(uts[i],/ecs,/trunc),rs[i],vel[i],featnames[featid[i]+1],FORMAT=fstring
  FREE_LUN,lu
  RETURN
END

function sreadhtfile

common scom_xplot_ht
common scom_feat

if (htname ne '') THEN BEGIN
   delim=get_delim()
    parts = str_sep(htname,delim)
    filename = parts(n_elements(parts)-1)
    ;
    ;  Update the current file name
    ;
    widget_control,wcurrhtlabl,set_value=filename
    ;
    ; read in the file and plot the h-t values
    ;
    sccREAD_HT,htname,tai,rs,pa,comment,tel,featid,col,row,date_obs,time_obs,feat_desc,sc0,hunits,hrsun, DEBUG=debugon
    sc=parse_stereo_name(sc0,['A','B'])
    
    ; store height in DEGREES
    fac_rsec = onersun()/3600.    ; factor to convert Rsun/hour to km/sec
    rsun=hrsun
    udobs=anytim2utc(date_obs+'T'+time_obs)
    IF rsun LT 500 THEN BEGIN
    ; Rsun is not defined; compute it
    	IF sc EQ 'A' or sc EQ 'B' THEN rsun=stereo_rsun(udobs,sc) $
	ELSE BEGIN
	    solar_ephem,utc2yymmdd(udobs),radius=rsundeg,/soho
	    rsun=rsundeg*3600
	ENDELSE
    ENDIF
    hunits=strupcase(hunits)
    IF strmid(hunits,0,1) NE 'D' THEN BEGIN
    	IF hunits EQ 'ARCSEC' THEN rs=rs/3600. ELSE rs=rs*rsun/3600.
	hunits='DEGREES'
    ENDIF
    ; convert rs0 to radians for trig
    rs0=rs*!dpi/180
    w = WHERE(feat_desc NE '')
    featnames = ['All',STRMID(feat_desc(w),0,5)]
    nfeat = N_ELEMENTS(featnames)
    widget_control,/destroy,wfeatselect
    wfeatselect = cw_bgroup(wfeatbase,featnames,/nonexclusive,column=3,uvalue='Feat')
    featval= INTARR(nfeat)
    featval(0) = 1
    widget_control,wfeatselect,set_value=featval
    uts = TAI2UTC(tai)
    degree = 1
;stop    
    ht_splot, /reset
    return,1
ENDIF
end

;------------------------------------------------------------------------------
;	procedure plot_ht_ev
;------------------------------------------------------------------------------
; This procedure processes the events being sent by the XManager.
; This is the event handling routine for the plot_ht widget.  It is
; responsible for dealing with the widget events such as mouse clicks on
; buttons in the plot_ht widget.  The tool menu choice routines are
; already installed.  This routine is required for the plot_ht widget to
; work properly with the XManager.
;------------------------------------------------------------------------

PRO splot_ht_ev, event
common scom_xplot_ht
common scom_feat

WIDGET_CONTROL, event.id, GET_UVALUE = eventval		;find the user value
			                        	;of the widget where
							;the event occured
;print,eventval

CASE eventval OF

; here is where you would add the actions for your events.  Each widget
; you add should have a unique string for its user value.  Here you add
; a case for each of your widgets that return events and take the
; appropriate action.

  'prnplt':   BEGIN                    ; Print the plot to the printer
                IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                IF (filename ne '') THEN BEGIN
                   pbase = widget_base()
                   lpname = cw_field(pbase,/return_events, /string, /column, /frame, $
                           title='Enter Print Command (default is lp). ' + $
			         'Enter name.ps to save to a file.')
                   widget_control,pbase,/realize
                   lpn = widget_event(pbase)
  ;                 print,lpn.value
                   widget_control,/destroy,pbase
                   sht_print,lpn.value
                ENDIF
              END
  'savegif':  BEGIN
		IF filename NE '' THEN sht_print,filename
	      END
  'readht':   BEGIN
    	    	htname = dialog_pickfile (title='Select Height-Time File',filter='*ht')
		readok=sreadhtfile()
              END
  '1st':	  BEGIN                   ; fit to a linear function
                  IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                  IF (filename ne '') THEN BEGIN
                     degree = 1
                     ht_splot
                  END
  			  END
  '2nd':      BEGIN                   ; fit to a quadratic
                  IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                  IF (filename ne '') THEN BEGIN
                     degree = 2
                     ht_splot
                  END
  			  END
  '3rd':      BEGIN                   ; fit to a 3rd degree polynomial
                  IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                  IF (filename ne '') THEN BEGIN
                     degree = 3
                     ht_splot
                  END
  	      END
  'PA':       BEGIN
                  IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                  IF (filename ne '') THEN ht_splot,/dopa
                  degree = -1
              END
  'units':  	; do nothing
  'Custom':   BEGIN
                  IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                  IF (filename ne '') THEN custom_splot
                  degree = -2
              END
  'deltaname':   BEGIN
                  IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                  IF (filename ne '') THEN BEGIN
		    
		    ht_splot
		  ENDIF
              END
  'oplotmode': BEGIN
    	    	WIDGET_CONTROL,event.id, GET_VALUE= val
		wopbuttonval=~wopbuttonval
		;help,wopbuttonval
	      END
  'Feat':     BEGIN	; select feature codes
                   if event.value eq 0 and event.select eq 1 then featval(*)=0
                   if event.value ne 0 and event.select eq 1 then featval(0)=0
		   featval(event.value)=event.select
		   WIDGET_CONTROL, event.id, SET_VALUE = featval
              END
  'blobmode': BEGIN
                blobmode = (blobmode eq 0)
                IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                IF (filename ne '') THEN $
                CASE degree OF
                   -2 : custom_splot
                   -1 : pa_splot
                  else: ht_splot
                ENDCASE
              END
  'savevl':   BEGIN                   ; save the speeds to a file
                IF (N_ELEMENTS(filename) eq 0) THEN filename=''
                IF (filename ne '') THEN BEGIN
                   velfile = pickfile(/write,get_path=test)
;
;   Add the full path if not returned as the function result
;
                   IF (STRMID(velfile,0,1) NE '/')  THEN velfile = test+velfile
;                   print,velfile
;		print,test
                   ht_splot,vfile=velfile
                ENDIF
              END


  'quit': WIDGET_CONTROL, event.top, /DESTROY	   ;There is no need to
						   ;"unregister" a widget
						   ;application.  The
						   ;XManager will clean
						   ;the dead widget from
						   ;its list.

  ELSE: BEGIN
    	help,eventval
	MESSAGE, "Event User Value Not Found"	   ;When an event occurs
	END					   ;in a widget that has
						   ;no user value in this
						   ;case statement, an
						   ;error message is shown
ENDCASE
;print,eventval
END ;=========== end of plot_ht event handling routine task ============



;-------------------------------------------------------------------------
;	procedure xplot_ht
;-------------------------------------------------------------------------
; This routine creates the widget and registers it with the XManager.
; This is the main routine for the plot_ht widget.  It creates the
; widget and then registers it with the XManager which keeps track of the
; currently active widgets.
;-------------------------------------------------------------------------
PRO sxplot_ht, fname, GROUP = GROUP, DEBUG=debug, NODELTA=nodelta

common scom_xplot_ht
common scom_feat

usefile = 0
wopbuttonval=0
blobmode=0
rsunkm=6.96e5
plotwidth=680
IF keyword_set(NODELTA) THEN nodeltaset=1 ELSE nodeltaset=0

filename=''
IF (N_PARAMS() EQ 1) THEN IF (fname NE '')  THEN usefile=1
IF (!D.WINDOW eq -1) THEN winsave=-1 else winsave=!D.WINDOW
IF keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0

unitopts=['1M KM','R/Rsun']
;fac_rsec = 6.96E5/3600.    ; factor to convert Rsun/hour to km/sec
last_plot = ''

;    plot_ht cannot have multiple copies running.

IF(XRegistered("sxplot_ht") NE 0) THEN RETURN		;only one instance of
							;the plot_ht widget
							;is allowed.  If it is
							;already managed, do
							;nothing and return

;  Create the main base.  The ROW keyword arranges the widget visually.

plot_htbase = WIDGET_BASE(TITLE = "splot_ht",/row)

; To add other routines or remove any of these, remove them both below
; and in the plot_ht_ev routine.


plot_htbase = widget_base(/column, TITLE='PLOT HEIGHT-TIME CURVE')
wtopbase = widget_base(plot_htbase,/row)

wcontrolbase = widget_base(wtopbase,/column,/frame, xpad=10,ypad=10)	;,space=10)
;
;wreadhtbase=wcontrolbase
wreadhtbase = widget_base(wcontrolbase,/column)
wreadhtbutton = widget_button(wreadhtbase,uvalue='readht', value='Read Height-Time File')
curr_ht='            unknown            '
wreadhtlabl = widget_label(wreadhtbase,/align_center, value='Current HT File:')
wcurrhtlabl = widget_label(wreadhtbase,/align_center,value=curr_ht)
;
wcurvtpbase = widget_base(wcontrolbase,/column)
;wcurvtpbase=wreadhtbase
wcurvftlabl = widget_label(wcurvtpbase,/align_center, value='Select Type of Fit')
wcurvftbase = widget_base (wcurvtpbase,/row)
wlinrlabl = widget_button(wcurvftbase,uvalue='1st',value='1st')
wquadlabl = widget_button(wcurvftbase,uvalue='2nd',value='2nd')
wthrdlabl = widget_button(wcurvftbase,uvalue='3rd',value='3rd')
wcustlabl = widget_button(wcurvftbase,uvalue='Custom',value='Custom')

wcustname=   cw_field(wcurvtpbase,uvalue='CustomName',value='',TITLE='Function',XSIZE=16)
wcustparams= cw_field(wcurvtpbase,uvalue='CustomParams',value='',TITLE=' Params',XSIZE=16)
;
wdeltalabl  = widget_label(wcurvtpbase,/align_center,value='Enter Source Region Info:')
wlatlonbase = widget_base(wcurvtpbase,/row)
wlatnum     = cw_field(wlatlonbase,uvalue='clatvalue',value=0,TITLE='CarLat',xsize=5)
wlonnum     = cw_field(wlatlonbase,uvalue='clonvalue',value=-1,TITLE='CarLon',xsize=5)
wsrctime    = cw_field(wcurvtpbase,uvalue='csrctime',value='',TITLE='DateTime',XSIZE=20)
wlimbbase   = widget_base(wcurvtpbase,/row)
wlimblonlab = widget_label(wlimbbase,/align_left,value='CRLN@Limb@DateTime')
wlimblon    = widget_text (wlimbbase,xsize=5) 

; Select height units
wunitrow    = widget_base  (wcurvtpbase,/row)
wunitlabel  = widget_label (wunitrow,value='Y-axis unit:')
whunits     = cw_bselector2(wunitrow,unitopts, uvalue='units', set_value=0)

wplotbase   = widget_base  (wcurvtpbase,/row)
wdeltaname  = widget_button(wplotbase,uvalue='deltaname',value='   Update Plot   ')
wpalabl     = widget_button(wplotbase,uvalue='PA',value=' Plot PA ')
wopbase     = widget_base(wcontrolbase,/NONEXCLUSIVE)
wopbutton   = widget_button(wopbase,uvalue='oplotmode', value='Overplot')

wfeatbase   = widget_base(wcontrolbase,/column)
;wfeatbase=wreadhtbase
wfeatlabl   = widget_label(wfeatbase,/align_center,value='Select Features to Plot')
featnames= ['All']
wfeatselect = cw_bgroup(wfeatbase,featnames,/nonexclusive,column=3,uvalue='Feat')
featval= INTARR(1)
featval(0) = 1
widget_control,wfeatselect,set_value=featval
;
;wprnbase    = widget_base(wcontrolbase,/column)
;wprnbase=wreadhtbase
wprnpltbutton=widget_button(wcontrolbase,uvalue='prnplt', value='Print the Plot')
wpgifbutton = widget_button(wcontrolbase,uvalue='savegif',value='Save plot to GIF')

wblobbase   = widget_base(wcontrolbase,/NONEXCLUSIVE)
;wblobbase=wreadhtbase
wblobbutton = widget_button(wblobbase,uvalue='blobmode',  value='Blob Mode')

wsavevlbutton=widget_button(wcontrolbase,uvalue='savevl',value='Save Speeds To File')
wquitbutton = widget_button (wcontrolbase,uvalue='quit',value='Quit')


wdrawbase   = widget_base(wtopbase,/column,/frame,space=20,xpad=10)
wdraw 	    = widget_draw(wdrawbase,xsize=plotwidth,ysize=512)
wfileid     = widget_label(wdrawbase,/align_right, $
                     value='$Id: sxplot_ht.pro,v 1.16 2014/05/05 17:24:58 nathan Exp $')
widgtext = 'Distance is computed assuming direction of motion at an angle (Delta) relative to ' $
    	 + 'the "plane of the sky", which is perpendicular to the observer-sun line. ' $
	 + 'Positive is toward the observer. To turn off this feature, call sxplot_ht with /NODELTA.'

wtext1	    = widget_text(wdrawbase,value=widgtext, /wrap,scr_xsize=plotwidth,ysize=3) ; 512 pix by 3 lines

;                     value='1 Rsun= '+trim(onersun())+' km')

; Typically, any widgets you need for your application are created here.
; Create them and use plot_htbase as their base.  They will be realized
; (brought into existence) when the following line is executed.

WIDGET_CONTROL, plot_htbase, /REALIZE			;create the widgets
							;that are defined
winnum = !d.window
IF (usefile EQ 1) THEN BEGIN
   filename = fname
   ff = FINDFILE(filename)
   sz = SIZE(ff)
   IF (sz(0) NE 0)  THEN BEGIN
    	htname=filename
    	readok=sreadhtfile()
   ENDIF ELSE print,fname,' not found!'
ENDIF ELSE BEGIN
    htname = dialog_pickfile (title='Select Height-Time File',filter='*ht')
    readok=sreadhtfile()
ENDELSE

XManager, "splot_ht", plot_htbase, $			;register the widgets
		EVENT_HANDLER = "splot_ht_ev", $	;with the XManager
		GROUP_LEADER = GROUP			;and pass through the
							;group leader if this
							;routine is to be
							;called from some group
							;leader.
IF (winsave GT -1)   THEN WSET,winsave

END ;================== end of xplot_ht main routine ====================
