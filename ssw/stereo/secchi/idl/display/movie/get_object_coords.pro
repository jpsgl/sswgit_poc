;+
;$Id: get_object_coords.pro,v 1.3 2010/06/04 17:18:57 mcnutt Exp $
;
; Project     : SECCHI 
;                   
; Name        : get_object_coords
;               
; Purpose     : to calculate positions of the Earth, SOHO and the other STEREO spacecrafts in the HI2 images
;               
; Explanation :
;               
; Use         : objects=get_object_coords,hdrs,wcshdrs,'a'
;
; Inputs      :  hdrs= array of secchi fits headers
;                wcshdrs = arry of matching world cooridinat headers
;                sc = stereo spacecraft a or b   
;            
; Outputs     : objects  a (3,n_frames,2) array of pixel positions
;                 objects(0,i,*)=other spacecraft position
;                 objects(1,i,*)=earth position
;                 objects(2,i,*)=soho position
;
; Comments    : Called from scc_movie_win, scc_mkframe
;               
; Side effects: position may shift slightly between frames
;               
; Category    : Image Display.  Animation.
;               
; Written     : Lynn Simpson


; $Log: get_object_coords.pro,v $
; Revision 1.3  2010/06/04 17:18:57  mcnutt
; added header info
;
; Revision 1.2  2008/11/14 14:41:46  mcnutt
; prints status
;
; Revision 1.1  2008/06/25 18:31:02  mcnutt
; calculates the positions of earth soho and other stereo sc
;

function get_object_coords,hdrs,wcshdrs,sc

rt=180.0/!pi
rearth=6371.0 ; in km

s1=wcshdrs(0).naxis(0)
s2=wcshdrs(0).naxis(1)
;if s1 eq s2*2+2 then s1=s2
xs=fltarr(s1,s2)
ys=fltarr(s1,s2)
;angsun=fltarr(s1,s2)    ; angular distance from Sun
;angearth=fltarr(s1,s2)  ; angular distance from Earth
tst2=bytarr(s1,s2)
for i=0,s1-1 do xs(i,*)=i
for i=0,s2-1 do ys(*,i)=i
;
n_frames=n_elements(wcshdrs)
gse=fltarr(n_frames,6)
gse_sun=fltarr(n_frames,6)
gse_earth=fltarr(n_frames,3)
gse_osc=fltarr(n_frames,6)
gse_soho=fltarr(n_frames,3)

if (strlen(hdrs(0).date_obs) lt 11) then dates=hdrs.date_obs+'T'+hdrs.time_obs else dates=hdrs.date_obs

sunpix=dblarr(n_frames,2)
earthpix=dblarr(n_frames,2)
angsun=fltarr(n_frames,s1,s2)
angearth=fltarr(n_frames,s1,s2)
objects=fltarr(3,n_frames,2)

if strupcase(sc) eq 'A' then osc='B' else osc='A'

  for i=0,n_frames-1 do begin
     print,'calculating object coords for frame # ',string(i)
     coords_sc=wcs_get_coord(wcshdrs(i))
     earthcoord=get_stereo_lonlat(dates(i),sc,system='hpc',target='earth',/degrees)
     suncoord=get_stereo_lonlat(dates(i),sc,system='hpc',target='sun',/degrees)

     angsun(i,*,*)=acos(cos(coords_sc(1,*,*)/rt)*cos(coords_sc(0,*,*)/rt))*rt

     angearth(i,*,*)=acos(sin(earthcoord(2)/rt)*sin(coords_Sc(1,*,*)/rt)+$
        cos(earthcoord(2)/rt)*cos(coords_sc(1,*,*)/rt)*cos(abs(earthcoord(1)-$
         coords_sc(0,*,*))/rt))*rt

     sunpix(i,*)=wcs_get_pixel(wcshdrs(i),suncoord(1:2))

     earthpix(i,*)=wcs_get_pixel(wcshdrs(i),earthcoord(1:2))

   endfor

  for i=0,n_frames-1 do gse(i,*)=GET_STEREO_COORD(dates(i),sc,system='gse')

  for i=0,n_frames-1 do gse_osc(i,*)=GET_STEREO_COORD(dates(i),osc,system='gse')

  for i=0,n_frames-1 do gse_sun(i,*)=GET_STEREO_COORD(dates(i),'sun',system='gse')

;  gse_earth(*,*)=[0.0,0.0,0.0] ; by definition

  for i=0,n_frames-1 do begin
    orbt_soho=get_orbit(dates(i))
    gse_soho(i,*)=[orbt_soho.gse_x,orbt_soho.gse_y,orbt_soho.gse_z];*rearth
  endfor


for i=0,n_frames-1 do begin

  dist_s=reform(sqrt(total((gse(i,0:2)-gse_sun(i,0:2))^2)))
  dist_os=reform(sqrt(total((gse_osc(i,0:2)-gse_sun(i,0:2))^2)))
  dist_cs=reform(sqrt(total((gse_soho(i,0:2)-gse_sun(i,0:2))^2)))
  dist_e=reform(sqrt(total(gse(i,0:2)^2)))
  dist_oe=reform(sqrt(total(gse_osc(i,0:2)^2)))
  dist_ce=reform(sqrt(total(gse_soho(i,0:2)^2)))
  dist_c=reform(sqrt(total((gse(i,0:2)-gse_soho(i,0:2))^2)))
  dist_osc=reform(sqrt(total((gse(i,0:2)-gse_osc(i,0:2))^2)))

  ;Compute angle between Sun, osc from sc using law of cosines
  theta_s=rt*acos((dist_osc^2+dist_s^2-dist_os^2)/(2.0*dist_osc*dist_s)) & theta_s=theta_s(0)
  ;Compute angle between Earth, osc from sc using law of cosines
  theta_e=rt*acos((dist_osc^2+dist_e^2-dist_oe^2)/(2.0*dist_osc*dist_e)) & theta_e=theta_e(0)
  ;Compute angle between Sun, SOHO from sc using law of cosines
  theta_cs=rt*acos((dist_s^2+dist_c^2-dist_cs^2)/(2.0*dist_s*dist_c)) & theta_cs=theta_cs(0)
  ;Compute angle between Earth, SOHO from sc using law of cosines
  theta_ce=rt*acos((dist_e^2+dist_c^2-dist_ce^2)/(2.0*dist_e*dist_c)) & theta_ce=theta_ce(0)

  m=reform((earthpix(i,1)-sunpix(i,1))/(earthpix(i,0)-sunpix(i,0))) & m=m(0)
  b=reform(sunpix(i,1)-m*sunpix(i,0)) & b=b(0)
  flag=where(ys gt m*xs+b)
  ;
  ;Determine OSC (A) location as seen from SC (B)
  ;
  m2=reform(gse(i,1)/(gse(i,1)-gse_osc(i,1))) & m2=m2(0)
  z=reform(gse(i,2)+m2*(gse_osc(i,2)-gse(i,2))) & z=z(0)
  tst2(*)=0
  if z gt 0 then begin                 ; if LOS above Earth-Sun line
    tst2(flag)=1
  endif else begin                     ; if LOS below Earth-Sun line
    tst2(*)=1
    tst2(flag)=0
  endelse
  asun=reform(angsun(i,*,*))
  aearth=reform(angearth(i,*,*))
  tst1=(1.0/(abs(asun-theta_s)^2+abs(aearth-theta_e)^2))*tst2
  tmp=max(tst1,ind)
  indy=fix(float(ind)/float(s1))
  indx=ind-indy*s1
  oscpix=[indx,indy]
  ;
  ;Determine SOHO location as seen from STEREO B
  ;
  m2=reform(gse(i,1)/(gse(i,1)-gse_soho(i,1))) & m2=m2(0)
  z=reform(gse(i,2)+m2*(gse_soho(i,2)-gse(i,2))) & z=z(0)
  tst2(*)=0
  if z gt 0 then begin                 ; if LOS above Earth-Sun line
    tst2(flag)=1
  endif else begin                     ; if LOS below Earth-Sun line
    tst2(*)=1
    tst2(flag)=0
  endelse
  asun=reform(angsun(i,*,*))
  aearth=reform(angearth(i,*,*))
  tst1=(1.0/(abs(asun-theta_cs)^2+abs(aearth-theta_ce)^2))*tst2
  tmp=max(tst1,ind)
  indy=fix(float(ind)/float(s1))
  indx=ind-indy*s1
  sohopix=[indx,indy]


   objects(0,i,*)=oscpix
   objects(1,i,*)=earthpix(i,*)
   objects(2,i,*)=sohopix

endfor

return,objects

end

