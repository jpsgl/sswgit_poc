FUNCTION def_mvi_strct, _EXTRA=_extra
;+
; $Id: def_mvi_strct.pro,v 1.5 2011/07/18 18:36:28 nathan Exp $
;
; NAME:
;	DEF_MVI_STRCT
;
; PURPOSE:
;	Define MVI frame header structure.  
;
;   	The MVI file header structure is defined in def_mvi_hdr.pro
;
; CATEGORY:
;	MVI
;
; CALLING SEQUENCE:
;	Result = DEF_MVI_STRCT()
;
; OUTPUTS:
;	A default jMap information structure.  
;
; EXAMPLE:
;	Struct = DEF_MVI_STRCT()
;
; MODIFICATION HISTORY:
; $Log: def_mvi_strct.pro,v $
; Revision 1.5  2011/07/18 18:36:28  nathan
; add crota to mvi frame header
;
; Revision 1.4  2009/01/14 19:13:26  nathan
; added clarifying comments
;
; Revision 1.3  2008/08/08 23:09:25  nathan
; update coord definition
;
; Revision 1.2  2007/10/17 21:48:49  nathan
; latest revision from dev/movie
;
; Revision 1.4  2007/10/17 18:57:51  nathan
; Implemented standard version 6 header structure for SECCHI and LASCO. Should
; be backwards compatible with previous headers labeled version 6.
;
; Revision 1.1  2007/08/17 15:23:06  nathan
; goes with sccmvihdr2struct.pro
;
; 	Written by:	Adam Herbst, 2007
;-
   hdr = {sccmvihdr, $
    	  filename:'', $
    	  detector:'', $
    	  date_obs:'', $
    	  time_obs:'', $
    	  filter:'', $
          polar:'', $
    	  wavelnth:'', $
    	  exptime:0.0,  $   	    ; seconds
    	  xcen:0.0, ycen:0.0,  $    ; pixels (IDL coord. consistent with scc_wrunmoviem.pro)
    	  cdelt1:0.0,  $    	    ; arcsec/pixel
    	  rsun:0.0,  $	    	    ; arcsec
	  crota:0.0, $	    	    ; for compatability with FITS header
    	  roll:0.0 }	    	    ; degrees, same sign as CROTA
   RETURN, hdr
END
