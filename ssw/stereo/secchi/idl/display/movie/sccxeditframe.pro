;+
; $Id: sccxeditframe.pro,v 1.1 2007/08/17 17:31:38 nathan Exp $
;
; Project       : SOHO -LASCO/EIT
;
; Name          : XEDITFRAME
;
; Purpose       : This tool allows the user to edit a particular image "on the
;                 fly," while the WRUNMOVIEM procedure is running, using a widget
;		  interface.
;
; Use           : XEDITFRAME, in, out, [moviev]
;
; Arguments     : InImg - The input, or original image
;                 FinalImg - The output, or final image
;                 moviev - a structure generated by the WRUNMOVIE3 program,
;                       containing information about the movie.  If moviev
;                       is not provided, then certain features of the program,
;                       such as referencing images by "Current frame", will
;                       not be supported.
;
; Calls         : LASCO_READFITS, POINT_FILTER, GET_FILENAME
;	    	  GETSUPERBASENAME   (currently included in xeditframe.pro)	
;
; Comments      : 
;		  The field widgets require the user to press RETURN in order for changes
;		  to take effect.
;		  Much of the "overhead" in this program is for the "Undo" feature,
;		  which necessitates extra bookkeeping.
;
;	 	 The resulting image is = (Primary)*Normalization - (Base), which is then
;		  clipped at saturation points (cutoffs) provided by the user.
;
;		"Super Base Frame" should probably be changed to a term easily
;		  recognized by LASCO team members.  Procedure GETSUPERBASENAME
;		 needs to be extended significantly.
;
;		 "Remove Stars" currently operates on both primage & base images
;		 simultaneously, and only does so when the "Remove Stars" button is
;		 pressed.  For example, if primary and base images are loaded and
;		 "remove stars" is selected, and then a different primary image is loaded,
;	 	 remove stars will need to be run again -- but then the base image will
;		 have been operated on TWICE.
;
;		 On the "Primary Image" and "Base Image" buttons: If you want to 
;		 "re-select" a given button (i.e. load the original image again even
;		 though "Original Image" is already selected), you need to click
;		 on a different option (e.g. "None") and then click back to the selection
;		 you want.
;
; Side effects  : XEDITFRAME disables all other widgets (e.g. WRUNMOVIEM's interface)
;		  until it is finished.
;
; Category      : Image Processing
;
; Written       : Scott Hawley, NRL Jan 09, 1996 (from editframe.pro, SHH Jul 12, 1996)
;
; $Log: sccxeditframe.pro,v $
; Revision 1.1  2007/08/17 17:31:38  nathan
; moved from dev/movie
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;
; Modified	: N. Rich	971211	Change EDIT_GETFILENAME function to use
;					GET_FILENAME procedure--retrieves final data
;					if it exists
;	03.09.23, NRich - If moviev.rect THEN rotate image
;
; Version       : 
;
; See Also      : WRUNMOVIEM.PRO
; 
; @(#)xeditframe.pro	1.7 09/24/03 :NRL Solar Physics
;
;-
;_______________________________________________________________________________

FUNCTION CROP_TO_EQ,inimg
; cuts a 1024x1024 image into the shape used in an "equatorial" picture
  RETURN, inimg(*,224:224+576-1)
END

FUNCTION EDIT_LOADIMG,filename,hsize,vsize,gaps
  IF (N_ELEMENTS(filename) EQ 0) THEN BEGIN
     filename=''
     READ,'Enter filename of image (RETURN to cancel): ',filename
  ENDIF
  IF (filename EQ '') THEN RETURN,-1
  openr,input,filename,ERROR=error,/GET_LUN
  IF (error) THEN BEGIN
    MESSAGE,'Error: File "'+filename+'" not found.',/CONT
    gaps=INTARR(hsize,vsize)
    RETURN,FLTARR(hsize,vsize)
  ENDIF $
  ELSE BEGIN
     free_lun,input
     big=FLOAT(FITSREAD(filename,h,SECCHI=secchi))
     temp=size(big)
     bighsize=temp(1)
     bigvsize=temp(2)
     IF ((hsize mod 512) eq 0) and ((vsize mod 288) eq 0) and $
       ((bighsize eq 1024) and (bigvsize eq 1024)) THEN BEGIN
          MESSAGE,'Automatically cropping image.',/CONT       
          big=CROP_TO_EQ(big)
     ENDIF
     img=REBIN(big,hsize,vsize)
     gaps=FIX(img le 1)
     IF(~secchi) THEN BEGIN
	bias = OFFSET_BIAS(h)
	img = img - bias
     ENDIF
     RETURN,img
  ENDELSE
END

PRO SWAP,x,y
  tmp=x
  x=y
  y=tmp
  RETURN
end

FUNCTION FLOAT2STR,num,FORMAT=format
  IF (not KEYWORD_SET(format)) THEN format='(g10.3)'
  RETURN, STRCOMPRESS(STRING(num,FORMAT=format),/REMOVE_ALL)
END

FUNCTION EDIT_GETFILENAME,moviev,index
  hdr=moviev.img_hdrs(index)
  
  name = hdr.filename
  IF(STRLEN(name) EQ 25) THEN filename = SCC_FINDFILE(name) $
  ELSE filename=GET_FILENAME(hdr)	; Returns filename with path. NBR 12/11/97

  RETURN,filename
END

FUNCTION GETSUPERBASENAME,month,day,year,camera
; This function is supposed to return a filename corresponding to a "super base frame"
; or daily/monthly minimum image.  It should take the information given and go
; searching for the file, and the return the filename.

  filename='/net/corona/cplex3/monthly/3m_clcl_960703.fts'
  RETURN,filename
END

FUNCTION EDIT_GETSUPERBASENAME,moviev,index
  hdr=moviev.img_hdrs(index)
  date=hdr.date_obs
  year=FIX(strmid(date,2,2))
  mon=FIX(strmid(date,5,2))
  day=FIX(strmid(date,8,2))

  filename=GETSUPERBASENAME(mon,day,year,hdr.detector)
  RETURN,filename
END

PRO EDIT_SHOWIMAGES
common com_editframe,EditVars,Backup,nomoviev,moviev
common com_wid_editframe, editframebase, $
    primbuttons,primfilenamelabel,basebuttons,basefilenamelabel,$
    togglebuttons, MaxCutLabel,MaxCutField,MinCutLabel,MinCutField,$
    KernelField,NormLabel,NormField, donebutton, undobutton,cancelbutton,$
    rwindow,pwindow,bwindow

  IF (EditVars.mincutval ge Editvars.maxcutval) THEN BEGIN  ; Protect against contrast probs
     MESSAGE,'Invalid contrast parameters. Min='+FLOAT2STR(EditVars.mincutval)+', Max='+$
         FLOAT2STR(EditVars.maxcutval),/CONT
     RETURN
  ENDIF

  EditVars.result=EditVars.primimg*EditVars.normval-EditVars.baseimg

  IF (EditVars.togglevals(0)) THEN BEGIN      ; if "fix data gaps" selected
      index=where(EditVars.gaps NE 0,count)
      help,count
      IF (count ne 0) THEN EditVars.result(index)=0.0
    ENDIF

  winsave2=!D.WINDOW

    WSET,rwindow
    TV,byte( ((EditVars.result > EditVars.mincutval < EditVars.maxcutval)- $
         EditVars.mincutval)*(!D.N_COLORS-1)/(EditVars.maxcutval-EditVars.mincutval))  $
         < (!D.N_COLORS-1)

    IF (EditVars.togglevals(1)) THEN  XYOUTS, 10, 10, EditVars.date_obs+' '+strmid(EditVars.time_obs,0,8),/DEVICE, CHARSIZE=1.5

    WSET,pwindow
    TV,byte( ((EditVars.primimg > EditVars.mincutval < EditVars.maxcutval)- $
         EditVars.mincutval)*(!D.N_COLORS-1)/(EditVars.maxcutval-EditVars.mincutval))  $
         < (!D.N_COLORS-1)
    WSET,bwindow
    TV,byte( ((EditVars.baseimg > EditVars.mincutval < EditVars.maxcutval)- $
         EditVars.mincutval)*(!D.N_COLORS-1)/(EditVars.maxcutval-EditVars.mincutval))  $
         < (!D.N_COLORS-1)
    WSET,winsave2

  RETURN
END

PRO EDIT_UPDATEWIDGETS
; only updates a few widgets.
common com_editframe,EditVars,Backup,nomoviev,moviev
common com_wid_editframe, editframebase, $
    primbuttons,primfilenamelabel,basebuttons,basefilenamelabel,$
    togglebuttons, MaxCutLabel,MaxCutField,MinCutLabel,MinCutField,$
    KernelField,NormLabel,NormField, donebutton, undobutton,cancelbutton,$
    rwindow,pwindow,bwindow
    
    maxcutvalsuggest=MAX(EditVars.result)
    WIDGET_CONTROL,MaxCutLabel,SET_VALUE='Cutoff Max ('+FLOAT2STR(maxcutvalsuggest)+')'

    mincutvalsuggest=MIN(EditVars.result)
    WIDGET_CONTROL,MinCutLabel,SET_VALUE='Cutoff Min ('+FLOAT2STR(mincutvalsuggest)+')'

    WIDGET_CONTROL,NormLabel,SET_VALUE='('+$
          FLOAT2STR(EditVars.normsuggest,FORMAT='(g10.4)')+')'
  RETURN
END

PRO EDITFRAME_EV, Event
common com_editframe,EditVars,Backup,nomoviev,moviev
common com_wid_editframe, editframebase, $
    primbuttons,primfilenamelabel,basebuttons,basefilenamelabel,$
    togglebuttons, MaxCutLabel,MaxCutField,MinCutLabel,MinCutField,$
    KernelField,NormLabel,NormField, donebutton, undobutton,cancelbutton,$
    rwindow,pwindow,bwindow

  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  If (Ev ne 'Undo') THEN Backup=EditVars

  filename=''
  noupdate=0			; Trivial changes to menu will set noupdate to 1,
				; which tells the program not to bother updating everyting 
  CASE Ev OF 

  'primbuttons': BEGIN
      error=0
      EditVars.primbuttonval=Event.value
      CASE Event.Value OF
         0: BEGIN
              EditVars.primimg=EditVars.InImg
              EditVars.primfilename='        < No File >         '
              WIDGET_CONTROL,primfilenamelabel,SET_VALUE=EditVars.primfilename
            END
         1: IF (nomoviev) THEN BEGIN
               MESSAGE,'No frame information available.',/CONT
               error=1
            ENDIF $
            ELSE filename=EDIT_GETFILENAME(moviev,moviev.current)
         2: filename=PICKFILE(TITLE='Select  FITS  File') ; DON'T USE FILTER KEYWORD.  IT MALFUNCTIONS
         ELSE: BEGIN
                  MESSAGE,'Unknown button pressed',/CONT
                  error=1
               END
      ENDCASE
      if (filename EQ '') THEN error=1
      IF (not error)  THEN BEGIN
         EditVars.primimg=EDIT_LOADIMG(filename,EditVars.hsize,EditVars.vsize,temp)
	 help,moviev.rect
	 IF moviev.rect EQ 180 THEN BEGIN
	 	EditVars.primimg = ROTATE(EditVars.primimg,2)
		temp = ROTATE(temp,2)
	 ENDIF
         ;if moviev.rect then stop
	 EditVars.primgaps=temp
         EditVars.gaps=EditVars.basegaps or EditVars.primgaps
         EditVars.normsuggest=MEDIAN(EditVars.baseimg)/MEDIAN(EditVars.primimg)
         parts = str_sep(filename,'/')
         filename = parts(n_elements(parts)-1)
         EditVars.primfilename=filename
         WIDGET_CONTROL,primfilenamelabel,SET_VALUE=EditVars.primfilename
      ENDIF ELSE IF (Event.value ne 0) THEN BEGIN
          EditVars.primbuttonval=Backup.primbuttonval
          WIDGET_CONTROL,primbuttons,SET_VALUE=EditVars.primbuttonval
      ENDIF
    END
  'togglebuttons': BEGIN
        WIDGET_CONTROL,togglebuttons,GET_VALUE=togglevals
        EditVars.togglevals=togglevals
	;  Might want to make "remove stars" a regular button rather than part of CW_BGROUP
        IF (togglevals(2)) and (Backup.togglevals(2) eq 0) THEN BEGIN	; Remove Stars
           point_filter,EditVars.primimg,5,7,3,tmp,pts
           EditVars.primimg=tmp
           point_filter,EditVars.baseimg,5,7,3,tmp,pts
           EditVars.baseimg=tmp
        ENDIF
      END
  'Done': BEGIN
         WSET,rwindow
         EditVars.result=TVRD(0,0,EditVars.hsize,EditVars.vsize)
         EditVars.doneexit=1
         WIDGET_CONTROL, event.top,/DESTROY
      END
  'basebuttons': BEGIN
      error=0
      IF (nomoviev) and (Event.Value ge 1) and (Event.Value le 3) THEN BEGIN
         MESSAGE,'No frame information available.',/CONT
         error=1
      ENDIF $
      ELSE CASE Event.Value OF
         0: BEGIN 	; None
              EditVars.baseimg=fltarr(EditVars.hsize,EditVars.vsize)
              EditVars.basefilename='         < No File >          '
              WIDGET_CONTROL,basefilenamelabel,SET_VALUE=EditVars.basefilename
              EditVars.normsuggest=1.0
            END
         1: filename=EDIT_GETFILENAME(moviev,0)     	; Zeroth frame 
         2: 	IF (moviev.current lt 1) THEN BEGIN		; Previous frame
                  	MESSAGE,'No previous frame.',/CONT
                  	error=1
               	ENDIF ELSE begin
;stop
			filename=EDIT_GETFILENAME(moviev,moviev.current-1) 
		ENDELSE
         3: IF (moviev.current lt 2) THEN BEGIN
                  MESSAGE,'No image exists two frames prior to current frame.',/CONT
                  error=1
               ENDIF $
               ELSE filename=EDIT_GETFILENAME(moviev,moviev.current-2)
         4: filename=EDIT_GETSUPERBASENAME(moviev,moviev.current) 
         5: filename=PICKFILE(TITLE='Select  FITS  File')
      ELSE: MESSAGE,'Unknown button pressed',/CONT
      ENDCASE
      if (filename eq '') THEN error=1
      IF (not error) THEN BEGIN
         EditVars.basebuttonval=Event.value
         EditVars.baseimg=EDIT_LOADIMG(filename,EditVars.hsize,EditVars.vsize,temp)
	 help,moviev.rect
	 IF moviev.rect EQ 180 THEN BEGIN
	 	EditVars.baseimg = ROTATE(EditVars.baseimg,2)
		temp = ROTATE(temp,2)
	 ENDIF
         EditVars.basegaps=temp
         EditVars.gaps=EditVars.basegaps or EditVars.primgaps
         EditVars.normsuggest=MEDIAN(EditVars.baseimg)/MEDIAN(EditVars.primimg)
         parts = str_sep(filename,'/')
         filename = parts(n_elements(parts)-1)
         EditVars.basefilename=filename
         WIDGET_CONTROL,basefilenamelabel,SET_VALUE=EditVars.basefilename

         IF (Event.value ne 4) THEN BEGIN   ; Anything except Super Base Frame
              MESSAGE,'Automatically adjusting contrast to -100, 100.',/CONT 
              EditVars.maxcutval=100
              EditVars.mincutval=-100
         ENDIF ELSE BEGIN 
              MESSAGE,'Automatically adjusting contrast to 0, 1000.',/CONT 
              EditVars.maxcutval=1000
              EditVars.mincutval=0
              EditVars.normsuggest=1.0
         ENDELSE
        WIDGET_CONTROL,MaxCutField,SET_VALUE=FLOAT2STR(EditVars.maxcutval)
        WIDGET_CONTROL,MinCutField,SET_VALUE=FLOAT2STR(EditVars.mincutval)
      ENDIF ELSE IF (Event.value ne 0) THEN BEGIN
          EditVars.basebuttonval=Backup.basebuttonval
          WIDGET_CONTROL,basebuttons,SET_VALUE=EditVars.basebuttonval
      ENDIF
    END
  'NormField': BEGIN
        ; If (Event.type eq 3) THEN noupdate=1   ; Only useful if /ALL_EVENTS set in widget declaration
        WIDGET_CONTROL,NormField,GET_VALUE=temp
        EditVars.normval=temp(0)
      END
  'MaxCutField': BEGIN
        ; If (Event.type eq 3) THEN noupdate=1
        WIDGET_CONTROL,MaxCutField,GET_VALUE=temp
        EditVars.maxcutval=temp(0)
      END
  'MinCutField': BEGIN
        ; If (Event.type eq 3) THEN noupdate=1
        WIDGET_CONTROL,MinCutField,GET_VALUE=temp
        EditVars.mincutval=temp(0)
      END
  'KernelField': BEGIN
        ; If (Event.type eq 3) THEN noupdate=1
        EditVars.kernelval=Event.Value
        IF (EditVars.kernelval GT 2) THEN BEGIN
          EditVars.primimg=EditVars.primimg-smooth(EditVars.primimg,EditVars.kernelval)
          EditVars.baseimg=EditVars.baseimg-smooth(EditVars.baseimg,EditVars.kernelval)
        ENDIF
      END
  'Undo': BEGIN
        SWAP,EditVars,Backup 
	; reset all the widget values accordingly
        WIDGET_CONTROL,primbuttons,SET_VALUE=EditVars.primbuttonval
        WIDGET_CONTROL,primfilenamelabel,SET_VALUE=EditVars.primfilename
        WIDGET_CONTROL,basebuttons,SET_VALUE=EditVars.basebuttonval
        WIDGET_CONTROL,basefilenamelabel,SET_VALUE=EditVars.basefilename
        WIDGET_CONTROL,togglebuttons,SET_VALUE=EditVars.togglevals
        WIDGET_CONTROL,MaxCutField,SET_VALUE=FLOAT2STR(EditVars.maxcutval)
        WIDGET_CONTROL,MinCutField,SET_VALUE=FLOAT2STR(EditVars.mincutval)
        WIDGET_CONTROL,KernelField,SET_VALUE=EditVars.kernelval
        WIDGET_CONTROL,NormField,SET_VALUE=FLOAT2STR(EditVars.normval)
      END
  'Quit': BEGIN
      WIDGET_CONTROL, event.top, /DESTROY
      END
   ELSE: MESSAGE, "Even User Value Not Found",/CONT
  ENDCASE

  IF ((Ev ne 'Quit') and (Ev ne 'Done')) and (noupdate eq 0) THEN BEGIN
      EDIT_SHOWIMAGES
      EDIT_UPDATEWIDGETS
  ENDIF
END


PRO EDIT_DEFINEWIDGETS
common com_editframe,EditVars,Backup,nomoviev,moviev
common com_wid_editframe, editframebase, $
    primbuttons,primfilenamelabel,basebuttons,basefilenamelabel,$
    togglebuttons, MaxCutLabel,MaxCutField,MinCutLabel,MinCutField,$
    KernelField,NormLabel,NormField, donebutton, undobutton,cancelbutton,$
    rwindow,pwindow,bwindow

  ;junk   = { CW_PDMENU_S, flags:0, name:'' }  ;   ???  IDL's "wided" wrote this line.

  EditFrameBase = WIDGET_BASE(/COLUMN, TITLE='Edit  Frame')
  EDITFrameRowBase = WIDGET_BASE(EditFrameBase,/ROW)
  LeftBase = WIDGET_BASE(EditFrameRowBase,/COLUMN,/MAP,TITLE='First Third', $
      UVALUE='LeftBase')
  BASE5 = WIDGET_BASE(LeftBase,/COLUMN,FRAME=1,/MAP,TITLE='Primary Image', $
      UVALUE='BASE5')
  LABEL6 = WIDGET_LABEL( BASE5, UVALUE='LABEL6', VALUE='Primary Image')

  Btns125 = ['Current Movie Frame','Original Image','Pick Filename' ]
  primbuttons = CW_BGROUP( BASE5, Btns125, /COLUMN, EXCLUSIVE=1, $
      UVALUE='primbuttons', SET_VALUE=EditVars.primbuttonval)
  primfilenamelabel = WIDGET_LABEL( BASE5, UVALUE='primfilenamelabel', $
      VALUE=EditVars.primfilename)

  Btns128 = ['Fix Data Gaps', 'Time Stamp', 'Remove Stars'] 
  ;  Might want to make "remove stars" a regular button rather than part of CW_BGROUP
  togglebuttons = CW_BGROUP( LeftBase, Btns128, /COLUMN, NONEXCLUSIVE=1, $
      UVALUE='togglebuttons',SET_VALUE=EditVars.togglevals)

  MiddleBase = WIDGET_BASE(EditFrameRowBase, /COLUMN, /MAP, TITLE='Second Third', $
      UVALUE='MiddleBase')
  BASE46 = WIDGET_BASE(MiddleBase, /COLUMN, FRAME=1, /MAP, TITLE='Base Image', $
      UVALUE='BASE46')
  BASE12 = WIDGET_BASE(BASE46, /ROW, /MAP, UVALUE='BASE12')
  LABEL44 = WIDGET_LABEL( BASE46, UVALUE='LABEL44', VALUE='Base Image')

  Btns135 = ['None','Zeroth Frame','Previous Image','Two Frames Prior', $
    'Super Base Frame','Pick Filename' ]
  basebuttons = CW_BGROUP( BASE46, Btns135, /COLUMN, EXCLUSIVE=1, $
      UVALUE='basebuttons', SET_VALUE=EditVars.basebuttonval)
  basefilenamelabel = WIDGET_LABEL( BASE46, $
      UVALUE='basefilenamelabel', VALUE=EditVars.basefilename)

  undobutton = WIDGET_BUTTON( LeftBase, UVALUE='Undo', VALUE='Undo  Last  Change')
  cancelbutton = WIDGET_BUTTON( MiddleBase, UVALUE='Quit', VALUE='Quit  (Don''t Save)')

  RightBase = WIDGET_BASE(EditFrameRowBase, /COLUMN, /MAP, $
      TITLE='Third Third (Options)', UVALUE='RightBase')

  ContrastBase = WIDGET_BASE(RightBase, /COLUMN, FRAME=1, /MAP, $
      TITLE='Contrast Base', UVALUE='ContrastBase')
  ContrastLabel = WIDGET_LABEL(ContrastBase,VALUE='Adjust Contrast',YSIZE=15)
  MaxCutBase = WIDGET_BASE(ContrastBase,/ROW,/MAP)
; All the "field" widgets will only return events when the user presses RETURN.  This was
; done deliberately, so that the program isn't constantly updating the images while
; the user is trying to type.  To make the fields return events for all actions in the
; field, add the keyword /ALL_EVENTS.
; NOTE: WIDGETLABEL & WIDGET_TEXT are used instead of the ordinary CW_FIELD because
;    CW_FIELD won't allow one to dynamically assign the "title" of the field.
  maxcutvalsuggest=FLOAT(!D.N_COLORS)
  MaxCutLabel=WIDGET_LABEL(MaxCutBase,VALUE='Cutoff Max ('+FLOAT2STR(maxcutvalsuggest)+')',$
       /ALIGN_RIGHT, YSIZE=30)
  maxcutval=FLOAT(!D.N_COLORS)
  MaxCutField=WIDGET_TEXT(MaxCutBase,/EDITABLE,UVALUE='MaxCutField',$
           VALUE=FLOAT2STR(EditVars.maxcutval), XSIZE=5)
  MinCutBase = WIDGET_BASE(ContrastBase,/ROW,/MAP)
  mincutvalsuggest=FLOAT(0)
  MinCutLabel=WIDGET_LABEL(MinCutBase,VALUE='Cutoff Min ('+FLOAT2STR(mincutvalsuggest)+')',$
      /ALIGN_RIGHT, YSIZE=30)
  mincutval=0.
  MinCutField=WIDGET_TEXT(MinCutBase,/EDITABLE,UVALUE='MinCutField',$
           VALUE=FLOAT2STR(EditVars.mincutval), XSIZE=5)

  UnsharpBase = WIDGET_BASE(RightBase, /COLUMN, FRAME=1, /MAP, $
      TITLE='Unsharp Mask Base', UVALUE='UnsharpBase')
  LABEL54 = WIDGET_LABEL( UnsharpBase, UVALUE='LABEL54', $
        VALUE='      Unsharp Mask',YSIZE=15)
  KernelField = CW_FIELD( UnsharpBase,VALUE=EditVars.kernelval, /ROW, /INTEGER, /RETURN_EVENTS,$
      TITLE='    Kernel Size', UVALUE='KernelField', XSIZE=3)

  NormBase = WIDGET_BASE(RightBase, /COLUMN, FRAME=1, /MAP, $
      TITLE='Normalization Base', UVALUE='NormBase')
  normval=1.0
  NormTopLabel = WIDGET_LABEL( NormBase, UVALUE='NormTopLabel', YSIZE=16,$
      VALUE='Normalization')
  NormBase2= WIDGET_BASE(NormBase,/ROW)
  NormLabel = WIDGET_LABEL( NormBase2, UVALUE='NormLabel', YSIZE=30,$
      VALUE= '     ('+FLOAT2STR(EditVars.normsuggest)+')      ',/ALIGN_RIGHT)
  NormField = WIDGET_TEXT(NormBase2,VALUE=FLOAT2STR(EditVars.normval),/EDITABLE, $
          UVALUE='NormField',XSIZE=6)

  donebutton = WIDGET_BUTTON( RightBase, UVALUE='Done', VALUE='Done')

  RETURN
END

PRO sccXEDITFRAME, InImg, FinalImg,Inmoviev, GROUP=Group
common com_editframe,EditVars,Backup,nomoviev,moviev
common com_wid_editframe, editframebase, $
    primbuttons,primfilenamelabel,basebuttons,basefilenamelabel,$
    togglebuttons, MaxCutLabel,MaxCutField,MinCutLabel,MinCutField,$
    KernelField,NormLabel,NormField, donebutton, undobutton,cancelbutton,$
    rwindow,pwindow,bwindow

  winsave=!D.WINDOW
  IF (XRegistered("xeditframe") NE 0) THEN RETURN

  ;IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0      ;??? IDL's "wided" wrote this


;** Initialize variables
  IF (N_ELEMENTS(Inmoviev) ne 0) THEN BEGIN
      hdr=Inmoviev.img_hdrs(Inmoviev.current)
      date_obs=hdr.date_obs
      IF(STRLEN(date_obs) EQ 10) THEN time_obs=hdr.time_obs $
	ELSE BEGIN
		time_obs = STRMID(date_obs, 11)
		date_obs = STRMID(date_obs, 0,10)
	ENDELSE
      nomoviev=0
      moviev=Inmoviev
  ENDIF $
  ELSE BEGIN
      date_obs=''
      time_obs=''
      nomoviev=1
  END

  IF (N_ELEMENTS(InImg) eq 0) THEN InImg=dist(100,100)
  tmp=size(InImg)
  hsize=tmp(1)
  vsize=tmp(2)

  primimg=float(InImg)
  result=primimg

  doneexit=0

  baseimg=FLTARR(hsize,vsize)
  primgaps=INTARR(hsize,vsize)
  basegaps=primgaps
  gaps=basegaps OR primgaps
help,gaps
  window,XSIZE=hsize,YSIZE=vsize,TITLE='Edit Frame: Base Image',/FREE,XPOS=1100-hsize,YPOS=0
  bwindow=!D.WINDOW
  window,XSIZE=hsize,YSIZE=vsize,TITLE='Edit Frame: Primary Image',/FREE,XPOS=1100-hsize,YPOS=300
  pwindow=!D.WINDOW
  window,XSIZE=hsize,YSIZE=vsize,TITLE='Edit Frame: Result Image',/FREE,XPOS=1100-hsize,YPOS=700
  rwindow=!D.WINDOW

  primbuttonval=0
  primfilename='        < No File >         '
  togglevals=[1,1,0]
  basebuttonval=0
  basefilename='             < No File >             '
  maxcutvalsuggest=FLOAT(!D.N_COLORS)
  maxcutval=FLOAT(!D.N_COLORS)
  mincutvalsuggest=FLOAT(0)
  mincutval=0.
  kernelval=0
  normsuggest=1.0
  normval=1.0

  EditVars={$
      primimg:primimg,$
      baseimg:baseimg,$
      result:result,$
      InImg:InImg,$
      gaps:gaps,$
      primgaps:primgaps,$
      basegaps:basegaps,$
      hsize:hsize,$
      vsize:vsize,$
      nomoviev:nomoviev,$
      date_obs:date_obs,$
      time_obs:time_obs,$
      primbuttonval:primbuttonval,$
      basebuttonval:basebuttonval,$
      primfilename:primfilename,$
      basefilename:basefilename,$
      togglevals:togglevals,$
      maxcutval:maxcutval,$
      mincutval:mincutval,$
      kernelval:kernelval,$
      normsuggest:normsuggest,$
      normval:normval,$
      doneexit:doneexit}

  Edit_ShowImages

  EDIT_DEFINEWIDGETS
  WIDGET_CONTROL, editframebase, /REALIZE   ; make widgets appear on screen
  XMANAGER, 'xeditframe', EditFrameBase, EVENT_HANDLER='editframe_ev',$
         GROUP_LEADER=GROUP, /MODAL    ; /MODAL freezes all other widgets until
				       ;  this process is finished.  Currently necessary.

  IF (EditVars.doneexit) THEN FinalImg=EditVars.result $; If "Done" was used instead of "Quit"
    ELSE FinalImg=InImg

  IF (winsave GT -1) THEN WSET,winsave

  WDELETE,rwindow
  WDELETE,pwindow
  WDELETE,bwindow

END
