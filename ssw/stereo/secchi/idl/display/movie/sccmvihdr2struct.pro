FUNCTION sccMVIHDR2STRUCT, hdr, ver, _EXTRA=_extra
;
;+
; $Id: sccmvihdr2struct.pro,v 1.4 2011/07/18 18:36:28 nathan Exp $
;
; NAME:     MVIHDR2STRUCT 
;
; PURPOSE:  This function generates a mvi structure for a movie hdr frame.
;
; CATEGORY: LASCO/SECCHI MOVIE
;
; CALLING SEQUENCE: ahdr = MVIHDR2STRUCT(mvi_hdr) 
;
; INPUTS:   Mvihdr:    mvi movie header
;   	    ver:    MVI version from file header
;
; OUTPUTS:  LASCO/EIT/SECCHI: hdr structure =  {sccmvihdr, $
;    	  filename:'', $
;    	  detector:'', $
;    	  date_obs:'', $
;    	  time_obs:'', $
;    	  filter:'', $
;         polar:'', $
;    	  wavelnth:'', $
; Following new for MVI version 6 (SECCHI and LASCO)
;    	  xcen:0.0, ycen:0.0,  $    ; pixels (FITS coordinates)
;    	  cdelt1:0.0,  $    	    ; arcsec/pixel
;    	  rsun:0.0,  $	    	    ; arcsec
;    	  roll:0.0,  $	    	    ; degrees
; Following not in MVI header
;    	  exptime:0.0 }   	    ; seconds
;
; KEYWORD PARAMETERS:
;
; RESTRICTIONS: Mvi's from LASCO library will not have values for all fields in structure.
;
; MODIFICATION HISTORY:
;
; $Log: sccmvihdr2struct.pro,v $
; Revision 1.4  2011/07/18 18:36:28  nathan
; add crota to mvi frame header
;
; Revision 1.3  2010/08/17 15:54:01  nathan
; add AIA case
;
; Revision 1.2  2007/10/17 21:48:52  nathan
; latest revision from dev/movie
;
; Revision 1.4  2007/10/17 18:57:51  nathan
; Implemented standard version 6 header structure for SECCHI and LASCO. Should
; be backwards compatible with previous headers labeled version 6.
;
; Revision 1.2  2007/09/24 19:10:35  esfand
; AEE - used separate mvihdr struct for LASCO and SECCHI to
;       avoid problems. Also used wavelnth for EUVI to match fts hdrs.
;
; Revision 1.1  2007/08/17 15:11:01  nathan
; Modified from LASCO by Adam Herbst, Summer 2007
;
;
;       Written by:     
;
; Modified    :  AEE 29 Sep 2006 - Added check for SECCHI (vs LASCO/EIT)
;                                  and extended ahdr for SECCHI.
;                AEE 24 Oct 2006 - Defined ahdr separately for LASCO and SECCHI headers. 
;
;
; 10/24/06 @(#)mvihdr2struct.pro	1.2	: NRL LASCO IDL Library
;
;-
;

ahdr = def_mvi_strct()

IF n_params() LT 2 THEN BEGIN 
    message,'MVI version not defined; assuming pre-SECCHI version',/info
    ver=5
ENDIF
IF ver LE 5 THEN dfn=0 ELSE dfn=10

ahdr.date_obs = STRTRIM(hdr(0:14),2)
ahdr.time_obs = STRTRIM(hdr(15:29),2)
ahdr.filename = STRTRIM(hdr(30:44+dfn),2)
ahdr.filter = STRTRIM(hdr(45+dfn:54+dfn),2)
ahdr.detector = STRTRIM(hdr(65+dfn:74+dfn),2)
IF ahdr.detector EQ 'EIT' or ahdr.detector EQ 'EUVI' or ahdr.detector EQ 'AIA' THEN $
    ahdr.wavelnth = STRTRIM(hdr(55+dfn:64+dfn),2) $
ELSE $
    ahdr.polar    = STRTRIM(hdr(55+dfn:64+dfn),2)

IF ver GT 5 THEN BEGIN
    ahdr.xcen = STRTRIM(hdr(85:94),2)
    ahdr.ycen = STRTRIM(hdr(95:104),2)
    ahdr.cdelt1 = STRTRIM(hdr(105:114),2)
    ahdr.rsun = STRTRIM(hdr(115:124),2)
    IF (n_elements(hdr) GT 135) THEN ahdr.roll = STRTRIM(hdr(125:135),2) ELSE $
    	message,'Roll not defined in MVI header',/info
    ahdr.crota=ahdr.roll
ENDIF

;help,/st,ahdr

   RETURN, ahdr

END
