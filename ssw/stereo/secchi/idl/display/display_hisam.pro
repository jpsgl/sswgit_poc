pro display_hisam,sc,d1,d2,hi2=hi2,bin=bin,png=png

;+
; $Id: display_hisam.pro,v 1.3 2011/12/01 21:06:08 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : display_hisam
;               
; Purpose   : This routine will display n days of hi sample fits files.
;
;               
; Explanation: 
;               
; Use       : IDL> display_hisam,'a','2009-06-06','2009-06-08'
;    
; Inputs    :sc = spacecraft 'a' or 'b'
;            d1 = first day '2009-06-06'
;            d2 = last day '2009-06-09'
;            
;               
; Outputs   : 
;
; Keywords  : 
;   	     /hi2 display hi2 samples default is hi1	Return float image array
;            /png = write a png image file
;            /bin = sets  output size default is 512 for the yaxis. 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : DISPLAY
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL/I2, JUNE 2009
;               
; $Log: display_hisam.pro,v $
; Revision 1.3  2011/12/01 21:06:08  nathan
; use select_windows for windows compat
;
; Revision 1.2  2009/10/08 15:15:40  mcnutt
; corrected date on plot
;
; Revision 1.1  2009/06/12 11:50:44  mcnutt
; new pro to display hi samples
;

if keyword_set(hi2) then hi='hi_2' else hi='hi_1'
ud1=anytim2utc(d1)
IF n_params() GT 1 THEN ud2=anytim2utc(d2) ELSE ud2=ud1
ud=ud1
if ~keyword_set(bin) then bin=4 else bin=2048/bin ;defaults to 512

FOR mjd=ud1.mjd,ud2.mjd DO BEGIN
	ud.mjd=mjd
	yyyymmdd=utc2yymmdd(ud,/yyyy)
	
    outdir=getenv('secchi')+'/lz/L0/'+strlowcase(sc)+'/seq/'+hi	
    hisams=file_search(outdir+'/'+yyyymmdd+'/'+'*.fts')
    if hisams(0) ne '' then begin ;should never be more then one file just in case merge them together
        outfile=hisams(0)
    	tmp=sccreadfits(outfile,sccht,/nodata)
        if sccht.obs_prog eq 'HISAM' then begin
	  if datatype(img) eq 'UND' then begin 
             img=sccreadfits(outfile,scch)
	     fits_hdr=headfits(outfile)
	     exthdr=mrdfits(outfile,1)
             SAMseqtbl=string(readfits(outfile,/EXTEN))
           endif else begin
	     exthdr=[exthdr,mrdfits(outfile,1)]
             SAMseqtbl=[SAMseqtbl,string(readfits(outfile,/EXTEN))]
             img=[img,sccreadfits(outfile)]
	   endelse
        endif
     endif
ENDFOR
     z=where(exthdr.date_clr ne '')
      times=utc2tai(anytim2utc(exthdr(z).date_clr))
      t=(shift(times,1)-times)*(-1.)
      caden=min(t(where(t gt 0)))
      nimg=round((max(times)-min(times))/caden +1)
      newimg=uintarr(nimg,n_Elements(img(0,*))) 
      newtime=findgen(nimg)*caden +times(0)
      utimes=intarr(nimg)
      for n=0,(nimg)-1 do utimes(n)=where(times gt newtime(n)-caden/2 and times lt newtime(n)+caden/2)
      newimg(where(utimes ne -1),*)=img(z,*)
;make xaxis even for rebin
      while (n_Elements(newimg(*,0))/(bin*1.0))-(fix(n_Elements(newimg(*,0))/(bin*1.0))) ne 0.0 do begin
          newimg=[newimg,uintarr(1,n_Elements(newimg(0,*)))]
          newtime=[newtime,newtime(n_Elements(newtime)-1)+caden]
      endwhile
      tmp=bytarr(n_Elements(newimg(*,0))/bin+75,n_Elements(newimg(0,*))/bin+100)+255    
      pos=[70,50,70+n_Elements(newimg(*,0))/bin,50+n_Elements(newimg(0,*))/bin]
      wcshdr=fitshead2wcs(scch)
      if strlowcase(sc) eq 'a' then sunside=2047 else sunside=0
      pixs=intarr(2,2048) & pixs(0,*)=sunside & pixs(1,*)=indgen(2048) *1
      xy=wcs_get_coord(wcshdr,pixs)
      window,xsize=n_Elements(newimg(*,0))/bin+75,ysize=n_Elements(newimg(0,*))/bin+100
      tv,tmp
      tvscl,rebin(newimg,n_Elements(newimg(*,0))/bin,n_Elements(newimg(0,*))/bin),70,50,/device
      utplot,utc2str(tai2utc(newtime(0:10))),findgen(10),/nodata,/noerase,position=pos,title='SECCHI-'+strupcase(sc)+' '+hi+' Sample',$
             xrange=[utc2str(tai2utc(newtime(0))),utc2str(tai2utc(newtime(n_Elements(newtime)-1)))],xstyle=1,$
             yrange=[min(xy(1,*)),max(xy(1,*))],ytitle='HPC Lat. (Deg)',ystyle=1,$
             /device,xticklen=-.0075,yticklen=-0.0075,color=0

      if keyword_set(png) then begin
        set_plot,'z'
        device,set_resolution=[n_Elements(newimg(*,0))/bin+75,n_Elements(newimg(0,*))/bin+100]        
        loadct,0
        tv,tmp
        tvscl,rebin(newimg,n_Elements(newimg(*,0))/bin,n_Elements(newimg(0,*))/bin),70,50,/device
        utplot,utc2str(tai2utc(newtime(0:10))),findgen(10),/nodata,/noerase,position=pos,title='SECCHI-'+strupcase(sc)+' '+hi+' Sample',$
             xrange=[utc2str(tai2utc(newtime(0))),utc2str(tai2utc(newtime(n_Elements(newtime)-1)))],xstyle=1,$
             yrange=[min(xy(1,*)),max(xy(1,*))],ytitle='HPC Lat. (Deg)',ystyle=1,$
             /device,xticklen=-.0075,yticklen=-0.0075,color=0
        pngfile='SECCHI_'+strupcase(sc)+'_'+hi+'_Sample'+'_'+utc2yymmdd(ud1,/yyyy)+'_'+utc2yymmdd(ud2,/yyyy)
        tmp=ftvread(filename=pngfile,/png)
        select_windows	
    endif



END
