;+
; Project     :	STEREO - SECCHI
;
; Name        :	SECCHI_COLORS
;
; Purpose     :	Load appropriate SECCHI color table
;
; Category    :	STEREO, SECCHI, Color-table
;
; Explanation :	Restores the appropriate IDL save file containing the color
;               table for the selected telescope/wavelength combination.
;
; Syntax      :	SECCHI_COLORS, TELESCOPE, WAVELENGTH, RED, GREEN, BLUE
;
; Examples    :	SECCHI_COLORS, 'EUVI', 195, R, G, B
;               SECCHI_COLORS, 'COR1', 0,   R, G, B
;               SECCHI_COLORS, 'COR2', /LOAD
;
; Inputs      :	TELESCOPE  = The name of the telescope (EUVI,COR1,COR2,HI1,HI2)
;               WAVELENGTH = The wavelength (171,195,284,304).  Ignored for
;                            telescopes other than EUVI.
;
; Opt. Inputs :	None.
;
; Outputs     :	RED, GREEN, BLUE = The color table arrays.
;
; Opt. Outputs:	None.
;
; Keywords    :	LOAD = If set, then call TVLCT to load the color table.
;
; Calls       :	FILEPATH, FILE_EXIST
;
; Common      :	COLORS:	The IDL color table common block.
;
; Restrictions:	None.
;
; Side effects: If an error condition is found, then the program returns the
;               currently loaded color table using TVLCT, /GET.
;
; Prev. Hist. :	Based on code found in SECCHI_PREP by Robin Colannino
;
; History     :	Version 1, 10-Apr-2007, William Thompson, GSFC
;               Version 2, 28-Jun-2007, WTT, added common block COLORS
;-
;
pro secchi_colors, telescope, wavelength, r, g, b, load=load
;
common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
;
;  Form a simplified version of the telescope name from the first (and last)
;  letters, i.e. E, C1, C2, H1, H2.
;
tel = strupcase(strmid(telescope,0,1))
if tel ne 'E' then tel = tel + strmid(telescope,strlen(telescope)-1,1)
case tel of
    'E':  color_table = strtrim(wavelength,2) + '_EUVI_color.dat'
    'C1': color_table = 'COR1_color.dat'
    'C2': color_table = 'COR2_color.dat'
    'H1': color_table = 'HI1_color.dat'
    'H2': color_table = 'HI2_color.dat'
    else: begin
        message, /informational, 'Telescope ' + strtrim(telescope,2) + $
          'not recognized -- using current color table'
        tvlct, r, g, b, /get
        return
    end
endcase
;
;  Form the path to the file, and make sure the file exists.
;
file = filepath(color_table, root_dir=getenv('SSW_SECCHI'), $
                subdirectory=['data','color'])
if not file_exist(file) then begin
    message, /informational, 'File ' + file + $
      ' not found -- using current color table'
    tvlct, r, g, b, /get
    return
endif
;
;  Restore the color table from the IDL save file.
;
restore, file
;
;  If the /LOAD keyword was set, then load the color table.
;
if keyword_set(load) then begin
    tvlct, r, g, b
    r_orig = r  &  r_curr = r
    g_orig = g  &  g_curr = g
    b_orig = b  &  b_curr = b
endif
;
return
end
