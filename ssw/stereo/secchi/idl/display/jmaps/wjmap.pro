;NAME: wjmap.pro
;
;PURPOSE: improved widget for jmaps
;
;INPUTS/KEYWORDS: none
;
;OUTPUTS: various jmaps

;COMMON BLOCK INFO: comb_data:filenames and instrument for each file to be combined
;		    combine_info:info for each combination (save location, etc.)

;  $Id: wjmap.pro,v 1.7 2012/08/10 18:40:52 cooper Exp $

;  $Log: wjmap.pro,v $
;  Revision 1.7  2012/08/10 18:40:52  cooper
;  defaults now asks for angles even if hi2 not there
;
;  Revision 1.6  2012/06/26 15:57:49  cooper
;  removed stop...
;
;  Revision 1.5  2012/06/26 15:57:15  cooper
;  really fixed links for saving
;
;  Revision 1.4  2012/06/21 15:35:54  cooper
;  fixed link sjmaps for saving
;
;  Revision 1.3  2012/06/19 20:21:54  cooper
;  added automation options
;

;sets slider,label times in options widget and returns start and end in correct format
FUNCTION set_times,times,noset=noset
COMMON JMP_COMMON,data, path, import_fields, sats, instruments, main_base, options_base, options, opts_arr,$ 
	    save_dir, combine_base, combinations, comb_data, no_comb, combine_info, debugon
	times[2:3] = temporary(times[2:3]) > times[0] < times[1]
	IF(times[2] EQ times[1]) THEN times[2] = times[0]
	IF(times[3] EQ times[0]) THEN times[3] = times[1]
	t0 = UTC2STR(TAI2UTC(double(times[2])))
	tpos = strpos(t0, 'T')
	IF(~keyword_set(noset)) THEN FOR i=0,1 DO WIDGET_CONTROL, options[i+9], SET_VALUE=[times[i+2], $
			(i EQ 0) ? times[0] : times[2], (i EQ 1) ? times[1] : times[3]]
	IF(tpos GT 0) THEN t0 = strmid(t0, 0, tpos) + ' ' + strmid(t0, tpos+1)
	t1 = UTC2STR(TAI2UTC(double(times[3])))
	tpos = strpos(t1, 'T')
	IF(tpos GT 0) THEN t1 = strmid(t1, 0, tpos) + ' ' + strmid(t1, tpos+1)
	temp_times=[t0,t1]
	return,{times:times,temp_times:temp_times}
END

;converts angles from string input to correct array format for making jmaps:copied from v2a
FUNCTION fix_angs, angles
COMMON JMP_COMMON
	pa = strsplit(angles, ' ,.;', /extract)
	ang_range = fix(strsplit(pa[0], '-', /extract))
		IF(n_elements(ang_range) EQ 2) THEN BEGIN
			IF(n_elements(pa) GT 1) THEN diff = fix(pa[1]) ELSE diff = 1
			pa = ang_range[0]
			next_ang = pa + diff
			WHILE(next_ang LE ang_range[1]) DO BEGIN
			pa = [pa, next_ang]
			next_ang = next_ang + diff
			ENDWHILE
	ENDIF ELSE pa = fix(temporary(pa))
	return,pa
END

PRO MAIN_EVENT, event
COMMON JMP_COMMON
	IF(datatype(event) EQ 'UND') THEN return
	WIDGET_CONTROL,event.id,get_uvalue=ev
	IF(ev EQ '') THEN return
	CASE ev OF
	'default':BEGIN
		;set default jmap settings
		anga=''
		angb=''
		save_dir='/'
		WIDGET_CONTROL,import_fields[14],set_value=save_dir
		tempopts=opts_arr
		tdata=data
		FOR i=0,n_elements(opts_arr)-1 DO BEGIN
			opts=tempopts[i]
			tempdata=tdata[*,i]
			t0=anytim2cal(opts.start_time,/date,form=1)
			t1=anytim2cal(opts.end_time,/date,form=1)
			d0=strmid(t0,0,2) & m0=strlowcase(strmid(t0,3,3))
			d1=strmid(t1,0,2) & m1=strlowcase(strmid(t1,3,3))
			IF m0 EQ m1 THEN m1=''
			instr=strlowcase(tempdata[2]) & sat=strlowcase(tempdata[3])
			tempdata[1]=m0+d0+'_'+m1+d1+'_'+instr+sat+'.jmp' ;default name
			opts.jmap_name=tempdata[1]
			y=strmid(t0,7,2)
			loc='/sjmaps'+y+'/'+m0+'/'+m0+d0+'_'+m1+d1+'/'+sat+'/'+instr+sat+'/' ;default location
			opts.location=loc
			tempopts[i]=opts
			tdata[*,i]=tempdata
			IF instr EQ 'hi2' THEN BEGIN ;input the angles
				IF sat EQ 'a' THEN ang=anga ELSE ang=angb
				response='n'
				WHILE response EQ 'n' DO BEGIN
				ang=wtext('Enter '+strupcase(sat)+' angles',/nocancel,title=sat+' Angles',intext=ang)
				 ;run first frame of movie with p.a. slits to check angles
				angles=fix_angs(ang)
				temp=STRPOS(tempdata[5],'~!@')
				IF (temp NE -1) THEN file=STRMID(tempdata[5],0,temp) ELSE file=tempdata[5]
				fitsdir=opts.fitsdir
				width=opts.slit_width
				width = (width/215.) * (180./!pi) ;from solar radii to degrees
				proj_type=opts.proj_type
				ranges=opts.ranges
				x=jmap_construct2a(file, angles, fitsdir=fitsdir, width=width, proj_type=proj_type,range=ranges,/check_angs)
				response=wopts(['Yes','No'],outlist=['y','n'],label='Use these angles?')
				ENDWHILE
				IF sat EQ 'a' THEN anga=ang ELSE angb=ang
			ENDIF
		ENDFOR
		wa=where(tdata eq 'A') ;just sneaky code to set all angles to
		IF wa[0] NE -1 THEN BEGIN
			IF anga EQ '' THEN anga=wtext('Enter A angles',/nocancel,title='A Angles',intext=ang)
			tdata[wa+1]=anga ;correct values--angles are right after sats
			tempopts[wa/6].angles=anga ;Nx6 array, dividing by 6 gives row number
		ENDIF
		wb=where(tdata eq 'B')
		IF wb[0] NE -1 THEN BEGIN
			IF angb EQ '' THEN angb=wtext('Enter B angles',/nocancel,title='B Angles',intext=ang)
			tdata[wb+1]=angb
			tempopts[wb/6].angles=angb
		ENDIF
		opts_arr=tempopts
		data=tdata
		WIDGET_CONTROL,import_fields[5],set_value=data ;update the table
		;set default combo settings
		comb=wopts(['Yes','No'],outlist=['y','n'],label='Make combination jMaps?')
		IF comb EQ 'y' THEN BEGIN
		undefine,comb_data
		undefine,combine_info
		no_comb=0
		na=size(wa,/n_elem)
		IF na GT 1 THEN BEGIN
			acombinfo={files_per_comb:na,loc:'',name:''}
			tempjname=data[wa[0]-2] ;first A jmap name in table
			temppos=strpos(tempjname,'_',/reverse_search)
			acombinfo.name=strmid(tempjname,0,temppos+1)+'all3a_.jmp' ;name for combo
			tempint=wa[0]/6 ;data is nX6 table...want to know what row is for this entry
			temploc=opts_arr[tempint].location
			temppos=strpos(temploc,'/a/')
			acombinfo.loc=strmid(temploc,0,temppos+3)+'all3a/' ;save location for combo
			FOR i=0,na-1 DO BEGIN ;now need to make the data array (names and instruments of jmaps going into combo)
				j=wa[i]/6 ;again, have nx6 table, want to know row for this entry
				tempdat=data[*,j]
				jnam=tempdat[1]
				jnam=strmid(jnam,0,strlen(jnam)-5)+'_.jmp'
				inst=tempdat[2]
				loc=opts_arr[j].location
				tmpstr={file:loc+jnam,instr:inst} ;one entry of this type for each jmap
				IF datatype(comb_data) EQ 'UND' THEN comb_data=[tmpstr] ELSE comb_data=[comb_data,tmpstr]
			ENDFOR
			combine_info=[acombinfo]
		ENDIF ;this endif for IF combining A
		nb=size(wb,/n_elem)
		IF nb GT 1 THEN BEGIN ;see above if statement for A for comments
			bcombinfo={files_per_comb:nb,loc:'',name:''}
			tempjname=data[wb[0]-2]
			temppos=strpos(tempjname,'_',/reverse_search)
			bcombinfo.name=strmid(tempjname,0,temppos+1)+'all3b_.jmp'
			tempint=wb[0]/6
			temploc=opts_arr[tempint].location
			temppos=strpos(temploc,'/b/')
			bcombinfo.loc=strmid(temploc,0,temppos+3)+'all3b/'
			FOR i=0,nb-1 DO BEGIN
				j=wb[i]/6
				tempdat=data[*,j]
				jnam=tempdat[1]
				jnam=strmid(jnam,0,strlen(jnam)-5)+'_.jmp'
				inst=tempdat[2]
				loc=opts_arr[j].location
				tmpstr={file:loc+jnam,instr:inst}
				IF datatype(comb_data) EQ 'UND' THEN comb_data=[tmpstr] ELSE comb_data=[comb_data,tmpstr]
			ENDFOR
			IF data_type(combine_info) EQ 'UND' THEN combine_info=bcombinfo ELSE combine_info=[combine_info,bcombinfo]
		ENDIF ;this endif for IF combining B
		prep_combine,0
		IF na GT 1 AND nb GT 1 THEN temp=['0','1'] ELSE temp='0'
		WIDGET_CONTROL,combinations[13],set_value=temp
		WIDGET_CONTROL,combinations[13],set_droplist_select=0
		ENDIF ;this endif for IF comb
		END
	'import': BEGIN
		file = dialog_pickfile(filter = '*.mvi', path = path, get_path=thispath,/multiple_files)
		IF(file[0] NE '') THEN BEGIN
			path=thispath
			WIDGET_CONTROL,import_fields[8],set_value=path
			WIDGET_CONTROL,import_fields[5],insert_rows=1
			name=''
			IF (n_elements(file) NE 1) THEN BEGIN FOR i=0, n_elements(file)-1 DO BEGIN
				slash=STRPOS(file[i],'/',/reverse_search)+1
				IF (i EQ 0) THEN name=name+STRMID(file[i],slash)$
					ELSE name=name+','+STRMID(file[i],slash)
				IF (i EQ 0) THEN files=file[i] ELSE files=files+'~!@'+file[i] ;adds ~!@ between each file name to distinguish between files
			ENDFOR
			ENDIF ELSE BEGIN slash=STRPOS(file[0],'/',/reverse_search)+1
				name=name+STRMID(file[0],slash)
				files=file
			      ENDELSE
			instr_num=WIDGET_INFO(import_fields[7],/droplist_select)
			instr=instruments[instr_num]
			sat=sats[WIDGET_INFO(import_fields[1],/droplist_select)]
			fitsdir=''
			angles=''
			loc=''
			q=STRPOS(name,'.mvi')
			jmap_name=STRMID(name,0,q)
			CASE instr_num OF
				0: slit_width = 0.4
				1: slit_width = 2.0
				2: slit_width = 0.12
				3: slit_width = 0.3
				4: slit_width = 1.0
				5: slit_width = 2.5
				6: slit_width = 3.6
			ENDCASE
			TEL_RANGES = [[0.0,1.68],[1.07,7.99],[0.0,0.45],[0.0,1.07],[0.0,4.2],[3.28,24.5],[18.36,88.36]]
			ranges=transpose(tel_ranges[*,instr_num])
			frame_skip=0
			times = [1e+100, -1e+100, 0, 0]
			FOR i=0,n_elements(file)-1 DO BEGIN
				openr, lu, file[i], /get_lun
				sccread_mvi, lu, file_hdr, ihdrs, imgs, swapflag
				FOR j=0,1 DO BEGIN
					img_hdr = sccmvihdr2struct(ihdrs[j*(file_hdr.nf-1)],file_hdr.ver)
					str = STRING(img_hdr.date_obs) + ' ' + STRING(img_hdr.time_obs)
					curr_tai = LONG(UTC2TAI(STR2UTC(str)))
					IF(curr_tai*(2*j-1) GT times[j]*(2*j-1)) THEN BEGIN
						IF(times[j+2] EQ times[j]) THEN times[j+2] = curr_tai
						times[j] = curr_tai
					ENDIF
				ENDFOR
			close, lu
			free_lun, lu
			ENDFOR
			t=set_times(times,/noset)   ;noset prevents slider values from changing if an options window is open
			times=t.times
			t=t.temp_times
			start_time=t[0]
			end_time=t[1]
			IF (datatype(data) EQ 'UND') THEN BEGIN
				data=[name,jmap_name,instr,sat,angles,files]
			ENDIF ELSE BEGIN
				temp=[name,jmap_name,instr,sat,angles,files]
				data=[[data],[temp]]
			ENDELSE
			WIDGET_CONTROL, import_fields[5],set_value=data
			opts_strct={ranges:ranges, slit_width:slit_width, frame_skip:0, start_time:start_time, end_time:end_time, no_movie:0, $
				  chop:0, point_filter:0, enhance_contrast:0, proj_type:1, angles:angles, jmap_name:jmap_name, $
				   fitsdir:fitsdir,location:loc,times:times}
			IF(datatype(opts_arr) EQ 'UND') THEN opts_arr=[opts_strct] ELSE opts_arr=[opts_arr,opts_strct]
		ENDIF
		END
	'join': BEGIN ;join together movies from same instrument/satellite
		l=size(opts_arr,/n_elem)
		IF l EQ 0 THEN break
		inds=indgen(l)
		tojoin=wopts2(string(inds),outlist=inds,label='Select movies to join (number from table)',/c)
		len=n_elements(tojoin)
		IF len EQ 1 THEN break ;this also includes cancel button..
		tempopts=opts_arr[tojoin]
		tempdata=data[*,tojoin]
		sat=tempdata[3,0] & instr=tempdata[2,0]
		brk=0
		FOR i=1,len-1 DO BEGIN ;cancel if we have conflicting data sources
			IF tempdata[3,i] NE sat OR tempdata[2,i] NE instr THEN BEGIN
				mess=dialog_message('Cannot join data from multiple instruments/satellites')
				brk=1
				break
			ENDIF
		ENDFOR
		IF brk EQ 1 THEN break
		names=tempdata[0,0]
		files=tempdata[5,0]
		times=tempopts[0].times
		FOR i=1,len-1 DO BEGIN ;join the file locations and movie names
			files+='~!@'+tempdata[5,i]
			names+=','+tempdata[0,i]
			times[0]=times[0]<(tempopts[i].times)[0]
			times[1]=times[1]>(tempopts[i].times)[1]
		ENDFOR
		tempdata[0,0]=names ;update the data for the first file to include data for all files
		tempdata[5,0]=files
		times[2]=times[0] & times[3]=times[1]
		tempopts[0].times=times
		t=set_times(times,/noset)
		t=t.temp_times
		tempopts[0].start_time=t[0]
		tempopts[0].end_time=t[1]
		;put combined data back into data stored by program, remove extra data
		;simplest way is to just make the data into lists of all the unjoined data,
		;then add this to the end of that. comp is indices of unjoined data
		w=intarr(l)-1 ;array of -1 of length inds
		w[tojoin]=tojoin ;set the values where we did join to the correct index
		w=where(w NE inds) ;this is the list of unjoined movie indices
		IF w[0] NE -1 THEN BEGIN
			data=data[*,w] & opts_arr=opts_arr[w]
			data=[[data],[tempdata[*,0]]] & opts_arr=[opts_arr,tempopts[0]]
		ENDIF ELSE BEGIN
			data=tempdata[*,0] & opts_arr=tempopts[0]
		ENDELSE
		widget_control,import_fields[5],delete_rows=len-1
		widget_control,import_fields[5],set_value=data
		END
	'change_mvi':BEGIN filenum=WIDGET_INFO(import_fields[5],/table_select)
			filenum=filenum[1]
			instr_num=WIDGET_INFO(import_fields[7],/droplist_select)
			instr=instruments[instr_num]
			sat=sats[WIDGET_INFO(import_fields[1],/droplist_select)]
			data[2,filenum]=instr
			data[3,filenum]=sat
			CASE instr_num OF
				0: slit_width = 0.4
				1: slit_width = 2.0
				2: slit_width = 0.12
				3: slit_width = 0.3
				4: slit_width = 1.0
				5: slit_width = 2.5
				6: slit_width = 3.6
			ENDCASE
			opts_arr[filenum].slit_width=slit_width
			TEL_RANGES = [[0.0,1.68],[1.07,7.99],[0.0,0.45],[0.0,1.07],[0.0,4.2],[3.28,24.5],[18.36,88.36]]
			opts_arr[filenum].ranges=transpose(tel_ranges[*,instr_num])
			WIDGET_CONTROL,import_fields[5],set_value=data
			prep_options
		     END	
	'path': BEGIN WIDGET_CONTROL, import_fields[8], get_value=path
		print,'Path:', path
		END
	'instr':BEGIN   ;set satellite to A or B if Cor1,cor2,hi1,hi2,euvi, SOHO if using C1,C2
		instr = WIDGET_INFO(import_fields[7], /DROPLIST_SELECT)
        	SWITCH instr of
                0:
		1: BEGIN
		   WIDGET_CONTROL, import_fields[1], SET_DROPLIST_SELECT=0
		   BREAK
                   END
                2:
		3:
		4:
		5:
		6: BEGIN satnum=WIDGET_INFO(import_fields[1],/droplist_select)
	           IF(satnum EQ 0) THEN WIDGET_CONTROL, import_fields[1], SET_DROPLIST_SELECT=1
		   END
        	ENDSWITCH
		set_button
		END
	'sat':  set_button
	'edit': BEGIN prep_options   ;show options widget
		WIDGET_CONTROL,options_base,map=1
		XMANAGER, 'opts', options_base
		END
	'combine_button': BEGIN WIDGET_CONTROL,combine_base,map=1  ;show combination widget
			  XMANAGER,'combine', combine_base
			  END
	'remove_movie':BEGIN filenum=WIDGET_INFO(import_fields[5],/table_select)
			filenum=filenum[1]
			WIDGET_CONTROL,import_fields[5],/delete_rows
			IF (size(data,/n_dimensions) EQ 1) THEN undefine,data ELSE WIDGET_CONTROL,import_fields[5],get_value=data
			num=n_elements(opts_arr)
			IF (num EQ 1) THEN BEGIN undefine,opts_arr
			ENDIF ELSE BEGIN 
				IF (filenum EQ 0) THEN BEGIN opts_arr=opts_arr[1:num-1]
				ENDIF ELSE BEGIN IF (filenum EQ num-1) THEN BEGIN opts_arr=opts_arr[0:num-2]
				ENDIF ELSE BEGIN opts_arr=[opts_arr[0:filenum-1],opts_arr[filenum+1:num-1]]
				ENDELSE & ENDELSE & ENDELSE
		       END
	'files':
	'save':BEGIN
		save_dir = dialog_pickfile(PATH = save_dir, filter='*.jmp',title='Select jMap Filename',/directory)
		WIDGET_CONTROL,import_fields[14],set_value=save_dir
		IF(STRPOS(save_dir,'/',/reverse_search) NE STRLEN(save_dir)-1) THEN save_dir+='/'
	       END
	'help': help_message
	'quit': BEGIN undefine, data ;;undefine all significant data holding objects so program can rerun without restarting idl
		undefine, opts_arr
		undefine, comb_data
		undefine, combine_info
		WIDGET_CONTROL,MAIN_BASE,/destroy 
		END
	'create':BEGIN 	
			make_dirs
			IF(datatype(opts_arr) NE 'UND') THEN make_jmaps
			IF(~keyword_set(no_comb))THEN combine
		END	
	ENDCASE
END

PRO make_jmaps
COMMON JMP_COMMON
	FOR i=0,n_elements(opts_arr)-1 DO BEGIN
		jmap_name=opts_arr[i].jmap_name
		IF(STRPOS(jmap_name,'.jmp') EQ -1) THEN jmap_name=jmap_name+'.jmp'
		loc=opts_arr[i].location
		temp=STRLEN(loc)-1
		IF(loc NE '') THEN BEGIN
			IF(STRPOS(jmap_name,'/') NE 0 AND STRPOS(loc,'/') NE temp) THEN loc=loc+'/'
			IF(STRMID(loc,0,1) EQ '/') THEN loc=STRMID(loc,1)
		ENDIF
		save=save_dir+loc+jmap_name
		ranges=opts_arr[i].ranges
		angles=opts_arr[i].angles
		angles=fix_angs(angles)
		width=opts_arr[i].slit_width
		width = (width/215.) * (180./!pi) ;from solar radii to degrees
		pfilter=opts_arr[i].point_filter
		contrast=opts_arr[i].enhance_contrast
		chop=opts_arr[i].chop
		proj_type=opts_arr[i].proj_type
		frame_skip=opts_arr[i].frame_skip
		satellite=data[3,i]
		file=data[5,i]
		IF(STRPOS(file,'~!@') EQ -1) THEN files=file $
		ELSE BEGIN files=''
			WHILE(STRPOS(file,'~!@') NE -1) DO BEGIN
				temploc=STRPOS(file,'~!@')
				tempfile=STRMID(file,0,temploc)
				files=[files,tempfile]
				file=STRMID(file,temploc+3)
				IF(STRPOS(file,'~!@') EQ -1) THEN files=[files,file]
			ENDWHILE
			files=transpose(files) ;needs to be a 2-d array of movies
		ENDELSE
		fitsdir=opts_arr[i].fitsdir
		times=opts_arr[i].times

		jmaps2a, mvi_files = files, time0=times[2], time1=times[3], ranges = ranges, $
		pa = angles, savefile = save, pfilter = pfilter,$
		dfilter = contrast, chop = chop, proj_type = proj_type, width=width, frame_skip=frame_skip, $
		telescope=satellite, fitsdir=fitsdir

	ENDFOR	
END

PRO set_button  ;sets the change_mvi button
COMMON JMP_COMMON
	instr=instruments[WIDGET_INFO(import_fields[7],/droplist_select)]
	sat=sats[WIDGET_INFO(import_fields[1],/droplist_select)]
	WIDGET_CONTROL,import_fields[13],set_value='Set To ' + instr + ' ' +sat
END	

PRO prep_combine, comb_num   ;set the combinations table to correct comb_num
COMMON JMP_COMMON
	IF(combine_info[comb_num].files_per_comb EQ 0) THEN this_comb={file:'',instr:''}$
	ELSE BEGIN
		IF(comb_num EQ 0) THEN num_skipped=0 ELSE num_skipped=TOTAL(combine_info[0:comb_num-1].files_per_comb)
		files_end=num_skipped+combine_info[comb_num].files_per_comb-1
		this_comb=comb_data[num_skipped:files_end]
	ENDELSE
	WIDGET_CONTROL,combinations[2],table_ysize=n_elements(this_comb)
	FOR i=0,n_elements(this_comb)-1 DO BEGIN
		tmp=[this_comb[i].file,this_comb[i].instr]
		IF(datatype(temp_data) EQ 'UND') THEN temp_data=tmp ELSE temp_data=[[temp_data],[tmp]]
	ENDFOR
	WIDGET_CONTROL, combinations[2],set_value=temp_data
	IF(combine_info[comb_num].loc EQ '') THEN WIDGET_CONTROL,combinations[5],set_value='Input Save Location' $
	ELSE WIDGET_CONTROL,combinations[5],set_value=combine_info[comb_num].loc
	IF(combine_info[comb_num].name EQ '') THEN WIDGET_CONTROL,combinations[6],set_value='Input Combination Name' $
		ELSE WIDGET_CONTROL,combinations[6],set_value=combine_info[comb_num].name
END

PRO COMBINE_EVENT, event
	;;comb_data is an array of combine_strct. files_per_comb tells how many entries in comb_data go into each combination
COMMON JMP_COMMON
	IF(datatype(event) EQ 'UND') THEN return
	WIDGET_CONTROL,event.id,get_uvalue=ev
	IF(ev EQ '') THEN return
	CASE ev OF
	'comb_num': prep_combine,WIDGET_INFO(combinations[13],/droplist_select)
	'new_comb':BEGIN IF(datatype(combine_info) EQ 'UND') THEN BEGIN WIDGET_CONTROL,combinations[13],set_value='0'
			  WIDGET_CONTROL,combinations[3],/sensitive
			  WIDGET_CONTROL,combinations[9],/sensitive
			  no_comb=0
		   ENDIF ELSE BEGIN WIDGET_CONTROL,combinations[13],get_value=temp ;add 1 to droplist of comb selections
			temp2=STRING(FIX(temp[n_elements(temp)-1]+1))
			temp=[temp,temp2]
			WIDGET_CONTROL,combinations[13],set_value=temp
			WIDGET_CONTROL,combinations[13],set_droplist_select=n_elements(temp)-1
		   ENDELSE
		   tmpstr={files_per_comb:0,loc:'',name:''}
	           IF (datatype(combine_info) EQ 'UND') THEN combine_info=tmpstr ELSE combine_info=[combine_info,tmpstr]
		   prep_combine,WIDGET_INFO(combinations[13],/droplist_select)
		   END
	'import_comb':BEGIN WIDGET_CONTROL,combinations[3],get_value=temp
		IF(temp LT n_elements(opts_arr)) THEN BEGIN
		 jmap_name=opts_arr[temp].jmap_name
		 loc=opts_arr[temp].location
		 IF(STRPOS(jmap_name,'.jmp') EQ -1) THEN jmap_name=jmap_name+'_.jmp' $
		 ELSE jmap_name=STRMID(jmap_name,0,STRPOS(jmap_name,'.jmp')-1)+'_.jmp'
		 loc=opts_arr[temp].location
		 slash=STRLEN(loc)-1
		 IF(STRPOS(jmap_name,'/') NE 0 AND STRPOS(loc,'/') NE slash) THEN loc=loc+'/'
		 IF(STRMID(loc,0,1) EQ '/') THEN loc=STRMID(loc,1)
		 file=save_dir+loc+jmap_name
		 instr=data[2,temp]
		 combine_strct={file:file,instr:instr}
		 comb_num=WIDGET_INFO(combinations[13],/droplist_select)
		 IF(datatype(comb_data) EQ 'UND') THEN comb_data=[combine_strct] ELSE BEGIN
			brk_point=TOTAL(combine_info[0:comb_num].files_per_comb)-1
			IF(brk_point EQ n_elements(comb_data)-1) THEN comb_data=[comb_data,combine_strct]$
			ELSE BEGIN
				temp1=comb_data[0:brk_point]
				temp2=comb_data[brk_point+1:n_elements(comb_data)-1]
				comb_data=[temp1,combine_strct,temp2]
			ENDELSE
		ENDELSE
		 combine_info[comb_num].files_per_comb++
		 prep_combine,comb_num
		ENDIF
		WIDGET_CONTROL,combinations[3],set_value=''	
	       END
	'import_jmp': 	BEGIN file=dialog_pickfile(filter='*.jmp',PATH='/sjmaps09',/MUST_EXIST)
			IF(file[0] EQ '') THEN return
			edits=STRPOS(file,'_t.jmp')
			IF(edits NE -1) THEN file=STRMID(file,0,edits-1)+'.jmp'
		      	instr=instruments[WIDGET_INFO(combinations[10],/droplist_select)]
		      	combine_strct={file:file,instr:instr}
		 	comb_num=WIDGET_INFO(combinations[13],/droplist_select)
		 	IF(datatype(comb_data) EQ 'UND') THEN comb_data=[combine_strct] ELSE BEGIN
				brk_point=TOTAL(combine_info[0:comb_num].files_per_comb)-1
				IF(brk_point EQ n_elements(comb_data)-1) THEN comb_data=[comb_data,combine_strct]$
				ELSE BEGIN
					temp1=comb_data[0:brk_point]
					temp2=comb_data[brk_point+1:n_elements(comb_data)-1]
					comb_data=[temp1,combine_strct,temp2]
				ENDELSE
			ENDELSE
			combine_info[comb_num].files_per_comb++
			prep_combine,comb_num
			END
	'comb_sav':BEGIN temp=WIDGET_INFO(combinations[13],/droplist_select)
		   WIDGET_CONTROL,combinations[5],get_value=tmp
		   combine_info[temp].loc=tmp[0]
		   END
	'comb_name':BEGIN temp=WIDGET_INFO(combinations[13],/droplist_select)
		   WIDGET_CONTROL,combinations[6],get_value=tmp
		   combine_info[temp].name=tmp[0]
		   END
	'combo_instr':
	'remove_combo': BEGIN
	 comb_num=WIDGET_INFO(combinations[13],/droplist_select)
			IF(datatype(combine_info) EQ 'UND') THEN return
			num_jmps=combine_info[comb_num].files_per_comb
			IF(num_jmps NE 0) THEN BEGIN
				IF(comb_num EQ 0 AND n_elements(combine_info) EQ 1) THEN undefine, comb_data $
				ELSE IF(comb_num EQ 0) THEN comb_data=comb_data[num_jmps:n_elements(comb_data)-1] $
				ELSE IF(comb_num EQ n_elements(comb_data-1)) THEN comb_data=comb_data[0:n_elements(comb_data)-1-num_jmps] $
				ELSE BEGIN brk_point=TOTAL(combine_info[0:combine_num].files_per_comb)-1
					tmp1=comb_data[0:brk_point-1]
					tmp2=comb_data[brk_point+num_jmps:n_elements(comb_data)-1]
					comb_data=[tmp1,tmp2]
				ENDELSE
			ENDIF
			IF(comb_num EQ 0 AND n_elements(combine_info) EQ 1) THEN undefine,combine_info $
			ELSE IF(comb_num EQ 0) THEN combine_info=combine_info[1:n_elements(combine_info)-1] $
				   ELSE IF(comb_num EQ n_elements(combine_info)-1) THEN combine_info=combine_info[0:n_elements(combine_info)-2] $
				   ELSE BEGIN	tmp1=combine_info[0:comb_num-1]
						tmp2=combine_info[comb_num+1:n_elements(combine_info)-1]
						combine_info=[tmp1,tmp2]
				   ENDELSE
			IF(datatype(combine_info) NE 'UND') THEN BEGIN 
				WIDGET_CONTROL,combinations[13],get_value=array_combs
				array_combs=array_combs[0:n_elements(array_combs)-2]
				WIDGET_CONTROL,combinations[13],set_value=array_combs
				WIDGET_CONTROL,combinations[13],set_droplist_select=0
				prep_combine,0 
			ENDIF ELSE BEGIN WIDGET_CONTROL,combinations[13],set_value='-1'
				no_comb=1
				WIDGET_CONTROL,combinations[3],sensitive=0
				WIDGET_CONTROL,combinations[9],sensitive=0
				WIDGET_CONTROL,combinations[2],table_ysize=0
				WIDGET_CONTROL,combinations[2],set_value=['','']
			ENDELSE
			END
	'remove_jmp': 	BEGIN comb_num=WIDGET_INFO(combinations[13],/droplist_select)
			IF(datatype(combine_info) EQ 'UND') THEN return
			IF(combine_info[comb_num].files_per_comb EQ 0) THEN return
			jmp_num=WIDGET_INFO(combinations[2],/table_select)
			jmp_num=jmp_num[1]
			IF(comb_num EQ 0) THEN brk_point=0 ELSE brk_point=TOTAL(combine_info[0:comb_num].files_per_comb)-1
			IF(n_elements(comb_data) EQ 1) THEN undefine,comb_data $
			ELSE IF(comb_num EQ 0 AND jmp_num EQ 0) THEN comb_data=comb_data[1:n_elements(comb_data)-1] $
			ELSE IF(brk_point+jmp_num GE n_elements(comb_data)-1) THEN comb_data=comb_data[0:n_elements(comb_data)-2] $
			ELSE IF(comb_num EQ 0) THEN comb_data=[comb_data[0:jmp_num-1],comb_data[jmp_num+1:n_elements(comb_data)-1]] $
			ELSE BEGIN tmp1=comb_data[0:brk_point-1]
				tmp2=comb_data[brk_point+1:n_elements(comb_data)-1]
				comb_data=[tmp1,tmp2]
			ENDELSE
			combine_info[comb_num].files_per_comb--
			prep_combine,comb_num
			END
	'done':	WIDGET_CONTROL,combine_base,map=0
	ENDCASE
END

PRO COMBINE
COMMON JMP_COMMON
	FOR i=0, n_elements(combine_info)-1 DO BEGIN  ;loop through each combination
		IF(i EQ 0) THEN num_skipped=0 ELSE num_skipped=TOTAL(combine_info[0:i-1].files_per_comb)  ;find number of jmaps for combination
		files_end=num_skipped+combine_info[i].files_per_comb-1
		this_comb=comb_data[num_skipped:files_end]
		location=combine_info[i].loc
		name=combine_info[i].name
		IF(location NE '') THEN BEGIN
			IF(STRPOS(location,'/') EQ 0) THEN location=STRMID(location,1)
			IF(STRPOS(name,'/') NE 0 AND STRPOS(location,'/',/reverse_search) NE STRLEN(location)-1) THEN location+='/'
		ENDIF
		IF(STRPOS(name,'.jmp') NE -1) THEN name=STRMID(name,0,STRPOS(name,'.jmp')-1)
		IF(datatype(regions) NE 'UND') THEN undefine,regions
		IF(datatype(this_combo) NE 'UND') THEN undefine,this_combo
		FOR j=0,combine_info[i].files_per_comb-1 DO BEGIN   ;loop through each jmap in this combination
			tmp=this_comb[j].file
			instr=this_comb[j].instr  ;regions: 0=cor1, 1=cor2, 2=hi1, 3=hi2
			CASE instr OF
				'COR1':region=0
				'COR2':region=1
				'HI1': region=2
				'HI2': region=3
				'C2' : region=4
				'C3' : region=5
			ENDCASE
			IF (datatype(regions) EQ 'UND') THEN regions=region ELSE regions=[regions,region]
			tmp=STRMID(tmp,0,STRPOS(tmp,'_',/reverse_search)-1)
			tmp=file_search(tmp+'*')   ;find all pas for each jmap in a combination
			IF(datatype(this_combo) EQ 'UND') THEN this_combo=tmp ELSE this_combo=[[this_combo],[tmp]]
		ENDFOR
		FOR k=0, n_elements(this_combo[*,0])-1 DO BEGIN  ;loop through each pa in a combination and make jmap
				jmp=TRANSPOSE(this_combo[k,*])
				first_file=this_combo[k,0] ;to get position angle
				this_pa=STRMID(first_file,STRPOS(first_file,'_',/reverse_search))
				save=save_dir+location+name+this_pa
				jmap_combine,jmp,save,debug=debugon, regions=regions
		ENDFOR
	ENDFOR
END

PRO OPTS_EVENT, event
COMMON JMP_COMMON
	IF(datatype(event) EQ 'UND') THEN return
	WIDGET_CONTROL,event.id,get_uvalue=ev
	IF(ev EQ '') THEN return
	filenum=WIDGET_INFO(import_fields[5],/table_select)
	filenum=filenum[1]
	CASE ev OF
	'done':BEGIN WIDGET_CONTROL,options_base,map=0
		     wset,0
		     wdelete
		     END
	'proj_type':BEGIN WIDGET_CONTROL,options[8],get_value=temp
		    opts_arr[filenum].proj_type=temp
		    END
	'opts':BEGIN WIDGET_CONTROL,options[7],get_value=temp
		opts_arr[filenum].no_movie=temp[0]
		opts_arr[filenum].point_filter=temp[1]
		opts_arr[filenum].enhance_contrast=temp[2]
		opts_arr[filenum].chop=temp[3]
		END
	'pa':BEGIN WIDGET_CONTROL,options[1],get_value=temp
		temp=STRTRIM(STRING(temp),2)
		opts_arr[filenum].angles=temp
		data[4,filenum]=temp
		WIDGET_CONTROL,import_fields[5],set_value=data
		END
	'range1':BEGIN WIDGET_CONTROL,options[3],get_value=temp
		opts_arr[filenum].ranges[0]=float(temp)
	        END
	'range2':BEGIN WIDGET_CONTROL,options[4],get_value=temp
		opts_arr[filenum].ranges[1]=float(temp)
		END
	'width':BEGIN WIDGET_CONTROL,options[5],get_value=temp
		opts_arr[filenum].slit_width=float(temp)
		END
	'frame_skip':BEGIN WIDGET_CONTROL, options[6],get_value=temp
		opts_arr[filenum].frame_skip=fix(temp)
		END
	'start_time':BEGIN WIDGET_CONTROL, options[9], GET_VALUE=temp
		times=opts_arr[filenum].times
		times[2] = temp[0]
		t=set_times(times)
		times=t.times
		t=t.temp_times
		opts_arr[filenum].times=times
		opts_arr[filenum].start_time=t[0]
		WIDGET_CONTROL,options[11],set_value=t[0]
		END
	'end_time':BEGIN WIDGET_CONTROL, options[10], get_value=temp
		times=opts_arr[filenum].times
		times[3]=temp[0]
		t=set_times(times)
		times=t.times
		t=t.temp_times
		opts_arr[filenum].times=times
		opts_arr[filenum].end_time=t[1]
		WIDGET_CONTROL,options[12],set_value=t[1]
		END
	'rename':BEGIN WIDGET_CONTROL,options[14],get_value=temp   ;give jmap new name
		temp=STRTRIM(temp,2)
		data[1,filenum]=temp
		opts_arr[filenum].jmap_name=temp
		WIDGET_CONTROL,import_fields[5],set_value=data
		END
	'dir':BEGIN WIDGET_CONTROL,options[15],get_value=temp  ;pick subdir within save_dir to save jmap
		temp=STRTRIM(temp,2)
		opts_arr[filenum].location=temp
		END
	'check':BEGIN    ;run first frame of movie with p.a. slits to check angles
		angles=opts_arr[filenum].angles
		angles=fix_angs(angles)
		file=data[5,filenum]
		temp=STRPOS(file,'~!@')
		IF (temp NE -1) THEN file=STRMID(file,0,temp)
		fitsdir=opts_arr[filenum].fitsdir
		width=opts_arr[filenum].slit_width
		width = (width/215.) * (180./!pi) ;from solar radii to degrees
		proj_type=opts_arr[filenum].proj_type
		ranges=opts_arr[filenum].ranges
		x=jmap_construct2a(file, angles, fitsdir=fitsdir, width=width, proj_type=proj_type,range=ranges,/check_angs)
		END
	'file_check':BEGIN names=data[5,filenum]  ;print file names
			IF(STRPOS(names,'~!@') EQ -1) THEN files=names $
			ELSE BEGIN files=''
			WHILE(STRPOS(names,'~!@') NE -1) DO BEGIN
				temploc=STRPOS(names,'~!@')
				tempfile=STRMID(names,0,temploc)
				files=[files,tempfile]
				names=STRMID(names,temploc+3)
				IF(STRPOS(names,'~!@') EQ -1) THEN files=[files,names]
			ENDWHILE
			files=transpose(files) ;needs to be a 2-d array of movies
			ENDELSE
			print,files
		     END
	'next':BEGIN temp=WIDGET_INFO(import_fields[5],/table_select)
		     IF(temp[1]+1 EQ n_elements(opts_arr)) THEN temp=[0,0,0,0] ELSE temp+=[0,1,0,1]
		     WIDGET_CONTROL,import_fields[5],set_table_select=temp
		     prep_options
		END
	'prev':BEGIN temp=WIDGET_INFO(import_fields[5],/table_select)
			row=n_elements(opts_arr)-1
			IF(temp[1] EQ 0) THEN temp=[0,row,0,row] ELSE temp-=[0,1,0,1]
			WIDGET_CONTROL,import_fields[5],set_table_select=temp
			prep_options
		END
	ENDCASE
END

PRO prep_options  ;sets up the options widget for selected jmap
COMMON JMP_COMMON
	if(datatype(opts_arr) EQ 'UND') THEN return
	filenum=widget_info(import_fields[5], /table_select)
	filenum=filenum[1]
	temp_opts=opts_arr[filenum]
	WIDGET_CONTROL,options[1],set_value=temp_opts.angles
	WIDGET_CONTROL,options[3],set_value=STRTRIM(STRING(temp_opts.ranges[0]),1)
	WIDGET_CONTROL,options[4],set_value=STRTRIM(STRING(temp_opts.ranges[1]),1)
	WIDGET_CONTROL,options[5],set_value=STRTRIM(STRING(temp_opts.slit_width),1)
	WIDGET_CONTROL,options[6],set_value=STRTRIM(STRING(temp_opts.frame_skip),1)
	WIDGET_CONTROL,options[7],set_value=[temp_opts.no_movie,temp_opts.point_filter,temp_opts.enhance_contrast,temp_opts.chop]
	WIDGET_CONTROL,options[8],set_value=temp_opts.proj_type
	WIDGET_CONTROL,options[11],set_value=temp_opts.start_time
	WIDGET_CONTROL,options[12],set_value=temp_opts.end_time
	WIDGET_CONTROL,options[14],set_value=temp_opts.jmap_name
	WIDGET_CONTROL,options[15],set_value=temp_opts.location
	times=temp_opts.times
	t=set_times(times)
	wset,0		
	wdelete
END

PRO make_dirs ;make directories that don't exist-#CHANGE--replace this with file_mkdir..much easier
COMMON JMP_COMMON
	locs=strarr(n_elements(opts_arr)+n_elements(combine_info))
	FOR i=0,n_elements(opts_arr)-1 DO locs[i]=opts_arr[i].location
	FOR i=0,n_elements(combine_info)-1 DO locs[i+n_elements(opts_arr)]=combine_info[i].loc
 	FOR i=0,n_elements(locs)-1 DO BEGIN
	loc=locs[i]
	IF(loc NE '') THEN BEGIN
		IF(STRPOS(loc,'/') EQ 0) THEN loc=STRMID(loc,1) ;save_dir ends with '/'
		IF(STRPOS(loc,'/',/reverse_search) NE STRLEN(loc)-1) THEN loc=loc+'/'
		IF(~file_test(save_dir+loc,/directory)) THEN BEGIN
			temp=''   ;name of each directory to be made
			useloc=loc   ;location,used to get each individual directory name
			temploc=''  ;each directory name added to this as it is made
			;;runs through each '/' in the save file and makes the directory if it does not exist
			WHILE (~file_test(save_dir+loc,/directory)) DO BEGIN
				pos=STRPOS(useloc,'/')
				temp=STRMID(useloc,0,pos+1)
				useloc=STRMID(useloc,pos+1)
				temploc=temploc+temp
				IF(~file_test(save_dir+temploc,/directory)) THEN BEGIN
					print,'Making directory: ' + save_dir+temploc
					cmd='mkdir '+save_dir+temploc
					spawn,cmd
				ENDIF
			ENDWHILE
		ENDIF
	ENDIF
	ENDFOR
END

PRO help_message
msg=[	'HELP MAKING jMAPS',$
	'Importing Movies: In the textbox, enter the directory to take the movies from.',$
	'	Change the instrument and satellite droplists to the appropriate values.',$
	'	Click import movies--multiple movies can be in the same file, but will',$
	'	only make one jmap. If the movie names do not fit into the table, the',$
	'	check files button in the options menu will print the file/movie names.',$
	' 	If a movie is imported with the wrong instrument or sat, use the set',$
	'	to button to correct this.','',$
	'DEFAULT BUTTON: Click this to make jmaps and combinations using default naming',$
		'and combination options.',$
	'Setting Options: Click select save location to select the main directory for',$
	'	all jMaps to be saved to.',$
	'	Click set options to open the options window for the selected movie file.',$
	'	Enter Angle(s) in the form of low-high,interval in degrees i.e. 56-136,3.',$
	'	Press check angles to see the position angles of the jmap relative to the movie.',$
	'	Enter the name you wish to give the jMap (default is the movie title).',$
	'	Enter the location within the main directory for this jMap to be saved to.',$
	'	If any directories beyond the main directory do not exist, they will be added.',$
	'	For example, if the main directory is /sjmaps09 location could be may/may01_04.',$
	'	If there is no may directory in /sjmaps09, may and may01_04 will be made.',$
	'	Slashes in all names and locations are automatically taken care of.','',$
	'Making Combined jMaps: This can be done with previously made jMaps or jMaps being made.',$
	'	Also, this program can be used solely for combining jMaps.',$
	'	Combinations have the same main save directory as normal jMaps.',$
	'	Press New Combination for each combination you wish to make, select the',$
	'	combination number to go to a combprint,ination.',$
	'	Enter the save location and the combination name, same format as with jMaps.',$
	'	To use jMaps that are to be made by Wjmap, enter the row number of the',$
	'	movie in the Current Movie Files table.',$
	'	To import already made jMaps, select the correct instrument and import.',$
	'	** Ensure that all position angles match.',$
	'	Only select one position angle from the directory, and all will be used.',$
	'	To combine jMaps if one has position angles that dont have matching jMaps',$
	'	in the other directory, temporarily store the desired position angles',$
	'	in a new directory, and import from there']
	XDISPLAYFILE,text=msg,TITLE='WJMAP HELP',HEIGHT=35, WIDTH=90
END
	

PRO wjmap, debug=debug
COMMON JMP_COMMON
	path='/smov12'
	save_dir='/sjmaps09/'
	no_comb=1
	debugon=debugon
	main_base=WIDGET_BASE(UVALUE='main', TITLE='jMap Constructor',column=4)
	options_base=WIDGET_BASE(UVALUE='options', COLUMN=1, map=0, group_leader=main_base,xoffset=900,title='jMap Options')
	combine_base=WIDGET_BASE(UVALUE='combine_base', COLUMN=3, map=0, group_leader=main_base,yoffset=400, title='Combine jMaps')
	
	instruments=['C2','C3','EUVI','COR1','COR2','HI1','HI2']  ;main widget
	sats=['SOHO','A','B']
	tempbase=WIDGET_BASE(main_base, UVALUE='movies',TITLE='import_base', COLUMN=1, FRAME=4)
	import_fields=WIDGET_LABEL(tempbase,value='Import Movies',/align_center)
	import_fields=replicate(import_fields,18)
	import_fields[7]=WIDGET_DROPLIST(tempbase, UVALUE='instr', value=instruments, title='Instrument')
	import_fields[1]=WIDGET_DROPLIST(tempbase, UVALUE='sat', value=sats, title='Satellite')
	import_fields[8]=WIDGET_TEXT(tempbase, UVALUE='path', VALUE=path,/Editable)
	import_fields[2]=WIDGET_BUTTON(tempbase, UVALUE='import', VALUE='Import Movies')
	import_fields[17]=WIDGET_BUTTON(tempbase, UVALUE='join', VALUE='Join Movies')
	import_fields[13]=WIDGET_BUTTON(tempbase, UVALUE='change_mvi', VALUE='Set To C2 SOHO')
	import_fields[3]=WIDGET_BUTTON(tempbase, UVALUE='remove_movie', VALUE='Remove Movie')
	tempbase=WIDGET_BASE(main_base, UVALUE='table', COLUMN=1, FRAME=4)
	import_fields[4]=WIDGET_LABEL(tempbase, value='Current Movie Files')
	table_label=['Movie Names','jMap Name','Instr','Sat','Angles']
	table_widths=[160,125,35,35,75]
	import_fields[5]=WIDGET_TABLE(tempbase, UVALUE='files', COLUMN_LABELS=table_label, x_scroll_size=5, y_scroll_size=4,$
					/scroll,/resizeable_columns,ysize=1,scr_ysize=210,$
					column_widths=table_widths,alignment=table_align)
	WIDGET_CONTROL,import_fields[5],/delete_row
	
	tempbase=WIDGET_BASE(main_base,UVALUE='base', COLUMN=1, FRAME=4)
	import_fields[15]=WIDGET_LABEL(tempbase,VALUE='Settings')
	import_fields[16]=WIDGET_BUTTON(tempbase,UVALUE='default',VALUE='Defaults')
	import_fields[6]=WIDGET_BUTTON(tempbase, UVALUE='edit', VALUE='Set Options')
	import_fields[12]=WIDGET_BUTTON(tempbase, UVALUE='combine_button', VALUE='Combination Settings')
	import_fields[14]=WIDGET_LABEL(tempbase,UVALUE='sav_lbl', value=save_dir,/dynamic_resize,frame=2)
	import_fields[10]=WIDGET_BUTTON(tempbase,UVALUE='save', value='Select Save Location')
	import_fields[11]=WIDGET_BUTTON(tempbase,UVALUE='create',value='Make jMaps')
	temp=WIDGET_BUTTON(tempbase,UVALUE='help',VALUE='Help')
	import_fields[9]=WIDGET_BUTTON(tempbase, UVALUE='quit', value='Quit')

	options=WIDGET_LABEL(options_base, value='jMap Options',/align_center)  ;options widget
	options=replicate(options,20)
	options[1]=CW_FIELD(options_base, VALUE='', FRAME=2, /all_events, TITLE='Angle(s)', UVALUE='pa', XSIZE=10)
	tempbase=WIDGET_BASE(options_base,COLUMN=1,FRAME=4)
	options[2]=WIDGET_LABEL(tempbase, UVALUE='range_label', VALUE='Range (degrees along ecliptic):')
	options[3]=WIDGET_TEXT(tempbase, UVALUE = 'range1', XSIZE = 5, YSIZE = 1, /EDITABLE,/all_events)
	options[4]=WIDGET_TEXT(tempbase, UVALUE = 'range2', XSIZE = 5, YSIZE = 1, /EDITABLE,/all_events)
	options[5]=CW_FIELD(options_base, UVALUE = 'width', value='0.4', XSIZE = 5, YSIZE = 1, title='Slit Width(solar radii)',/all_events, frame=2)
	options[6]=CW_FIELD(options_base, UVALUE='frame_skip', VALUE=0, XSIZE=4, frame=2, title='Frames to skip',/all_events)
	tempbase=WIDGET_BASE(options_base,COLUMN=2,FRAME=4)
	options[7]=CW_BGROUP(tempbase, ['No Movie', 'Point Filter', 'Enhance Contrast', 'Chop'], /NONEXCLUSIVE, $
      		LABEL_TOP='jMap Settings', UVALUE='opts', SET_VALUE = [0, 0, 0, 0], frame=2)
	options[8]=CW_BGROUP(tempbase, ['None','Spherical'],/exclusive, UVALUE='proj_type', SET_VALUE=1,LABEL_TOP='Projection Type',frame=2)
	tempbase=WIDGET_BASE(options_base,COLUMN=1,FRAME=4)
	options[11] = WIDGET_LABEL(tempbase, VALUE='Start time: none                     ')
	options[9]=CW_FSLIDER(tempbase, minimum=0.0, maximum=1.0, value=0.0, uvalue='start_time', xsize=200,$
			      scroll=.005,/suppress_value, /drag)
	options[12] = WIDGET_LABEL(tempbase, VALUE='End time: none                     ')
	options[10]=CW_FSLIDER(tempbase, minimum=0.0, maximum=1.0, value=1.0, uvalue='end_time', xsize=200,$
			      scroll=.005,/suppress_value, /drag)
	tempbase=WIDGET_BASE(options_base,COLUMN=1,FRAME=4)
	options[14]=CW_FIELD(tempbase,uvalue='rename',value='', title='jMap Name', /all_events, YSIZE=1, XSIZE=20)
	options[15]=CW_FIELD(tempbase,uvalue='dir',value='',title='Location', /all_events, YSIZE=1, XSIZE=20)
	tempbase=WIDGET_BASE(options_base,ROW=1)
	options[16]=WIDGET_BUTTON(tempbase, VALUE='Check Angles', uvalue='check')
	options[19]=WIDGET_BUTTON(tempbase, VALUE='Print Movie Names', uvalue='file_check')
	tempbase=WIDGET_BASE(options_base,ROW=1)
	options[17]=WIDGET_BUTTON(tempbase, VALUE='Next',uvalue='next')
	options[18]=WIDGET_BUTTON(tempbase, VALUE='Previous', uvalue='prev')	
	options[13]=WIDGET_BUTTON(tempbase, uvalue='done', value='Done')

	tempbase=WIDGET_BASE(combine_base,COLUMN=1,FRAME=4)   ;combinations widget
	combinations=WIDGET_LABEL(tempbase,value='Combinations')
	combinations=replicate(combinations,16)
	combinations[13]=WIDGET_DROPLIST(tempbase, UVALUE='comb_num', value='-1', title='Combination Number',/dynamic_resize)
	combinations[1]=WIDGET_BUTTON(tempbase,UVALUE='new_comb',value='New Combination')
	combine_label=['jMap Name','Instr']
	combinations[6]=WIDGET_TEXT(tempbase,UVALUE='comb_name',value='Input Combination Name',ysize=1,xsize=20,/editable,/all_events)
	combinations[5]=WIDGET_TEXT(tempbase,UVALUE='comb_sav',value='Input Save Location',ysize=1,xsize=20,/editable,/all_events)
	combinations[7]=WIDGET_BUTTON(tempbase,UVALUE='remove_combo',value='Remove Combination')
	tempbase=WIDGET_BASE(combine_base,COLUMN=1,FRAME=4)
	combinations[14]=WIDGET_LABEL(tempbase,VALUE='jMaps For This Combination')
	combinations[2]=WIDGET_TABLE(tempbase,UVALUE='comb_files', column_labels=combine_label, x_scroll_size=2,$
			y_scroll_size=4,/scroll,/resizeable_columns,ysize=1,scr_ysize=160,column_widths=[300,35])
	tempbase=WIDGET_BASE(combine_base,COLUMN=1,FRAME=4)
	combinations[11]=WIDGET_LABEL(tempbase,VALUE='Import jMaps')
	combinations[3]=CW_FIELD(tempbase,UVALUE='import_comb',title='jMap # In Table',value='',YSIZE=1, XSIZE=4,/return_events)
	tempbase2=WIDGET_BASE(tempbase,COLUMN=1,FRAME=2)
	combinations[9]=WIDGET_BUTTON(tempbase2,UVALUE='import_jmp',VALUE='Import jMap (Unedited)')
	combinations[10]=WIDGET_DROPLIST(tempbase2,UVALUE='combo_instr',VALUE=instruments,TITLE='Instrument')
	combinations[8]=WIDGET_BUTTON(tempbase,UVALUE='remove_jmp',VALUE='Remove jMap')
	combinations[12]=WIDGET_BUTTON(tempbase,UVALUE='done',VALUE='Done')
	
	WIDGET_CONTROL,combinations[3],sensitive=0
	WIDGET_CONTROL,combinations[9],sensitive=0
	WIDGET_CONTROL, main_base, /REALIZE
	WIDGET_CONTROL, options_base,/REALIZE
	WIDGET_CONTROL, combine_base,/REALIZE
	XMANAGER, 'main', main_base
END
