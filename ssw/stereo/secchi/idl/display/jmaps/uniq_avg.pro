FUNCTION uniq_avg, hts

 n = MAX(hts.plot_num)
 n_hts = hts
 min = MIN(hts.plot_num)

 FOR i=min, n-1 DO BEGIN
    indices = WHERE(hts.plot_num eq i)
    temp = hts(indices)
    indices2 = FIND_DUP(temp.time)
    IF (indices2(0) ne -1) THEN BEGIN
       height = TOTAL(temp(indices2).height) / n_elements(temp)
       time = TOTAL(temp(indices2).time) / n_elements(temp)
       temp(indices2).height = height
       temp(indices2).time = time
       temp(indices2).plot_num = i
       n_hts(indices) = temp
    ENDIF
 ENDFOR
 
 u = UNIQ(n_hts.time)
 n_hts = n_hts(u)
 RETURN, n_hts
END
    
       
      