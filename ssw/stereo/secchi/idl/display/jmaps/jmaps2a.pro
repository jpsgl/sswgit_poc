PRO jmaps2a, mvi_files = mvi_files, TIME0 = time0, TIME1 = time1, $
   NOMOVIE = nomovie, RANGES = ranges, PA = pa, SAVEFILE = savefile, $
   DATE_MADE = date_made, PFILTER=pfilter, WIDTH=width, FRAME_SKIP=frame_skip, $
   DFILTER = dfilter, CHOP = chop, FITSDIR = fitsdir, PROJ_TYPE=proj_type, DEBUG=debug, TELESCOPE=telescope

;+
; $Id: jmaps2a.pro,v 1.8 2012/12/19 17:26:55 mcnutt Exp $
; NAME:
;	jmaps2
;
; PURPOSE:
;	A wrapper for jmaps2_CONSTRUCT that is typically called by V1.  
;	Handles basic error checking of input parameters and also 
;	the 'putting together' of composite maps.  
;
; CATEGORY:
;	jmaps2
;
; CALLING SEQUENCE:
;	jmaps2
;
; OPTIONAL INPUTS:
;	START_DT:	The starting time of the map. If not set, then 
;		the full length of the supplied movies is used.
;	END_DATE:	The ending date of the map.  If not set, then 
;		the full length of the supplied movies is used.
;	MVI_FILES:	A 2-d array of mvi filenames
;	NOMOVIE:	If set, then no movie is displayed while the jMap;
;			is made.
;	TELESCOPE:	'c2' or 'c3'
;	SAVEFILE:	The name of the file to save the map as.
;	DATE_MADE:	The date that the map was made.
;	RANGES:		Nx2 array containing ranges (in solar radii) for the movies from each telescope
;	
;	
; KEYWORD PARAMETERS:
;	PFILTER:	If set, a point filter is used. 
;	DFILTER:	If set, a standard deviation normalization filter
;			is used.
;	CHOP:		If set, a chop is performed.
;	TELESCOPES:	List of the telescope index (as above) corresponding to each set of mvi files
;
; OUTPUTS:
;	The map is saved to SAVEFILE.
; PROCEDURE:
;	Calls JMAP_CONSTRUCT2 once or twice (twice for composite).
;
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Walters, Aug 2001
; $Log: jmaps2a.pro,v $
; Revision 1.8  2012/12/19 17:26:55  mcnutt
; replace variable list
;
; Revision 1.7  2011/02/24 18:42:24  nathan
; fix case where fitsdir has more than one element
;
; Revision 1.6  2010/01/19 19:11:54  nathan
; auto-add .jmp extension and do not print output filename (done in write_jmap2a)
;
; Revision 1.5  2009/11/04 17:10:24  nathan
; save temp files in /tmp
;
; Revision 1.4  2009/06/15 16:16:24  cooper
; modified widget, added satellite info
;
; Revision 1.3  2008/06/26 19:14:59  nathan
; added debug features
;
; Revision 1.2  2008/05/07 21:06:21  sheeley
; nr - commented out coords0/1 defn
;
; Revision 1.1  2007/11/08 16:56:40  nathan
; moved from adam/jmaps2a
;
;-
   
MEAN_ADJ = 1
ntel = (size(ranges))[1]
n_ang = n_elements(pa)
res = fltarr(ntel) ; the resolution for each telescope used, in pixels per solar radius
dim = lonarr(ntel, 2) ; pixel width and height of the jmap for each telescope
tai = fltarr(ntel, 2) ; start and end times
border = intarr(ntel, 4)  ; left, right, bottom, top pixel coords in final map

IF(~keyword_set(time0)) THEN BEGIN
	time0 = 1e+100
	time1 = -1e+100
ENDIF

FOR i=0,ntel-1 DO BEGIN
	mvilist = mvi_files[i,*]
	mvilist = mvilist[where(mvilist NE '')]
	tmp_range = ranges[i,*]
	tmp0 = time0
	tmp1 = time1
	IF keyword_set(FITSDIR) THEN fdir=fitsdir[i]
	map = jmap_construct2a(mvilist, pa, range=tmp_range, time0=tmp0, time1=tmp1, coords=coords, $
	  pfilter = pfilter, dfilter = dfilter, chop = chop, fitsdir=fdir, proj_type=proj_type, $
	  width=width[i], frame_skip=frame_skip[i], DEBUG=debug)
	;IF(proj_type GT 0) THEN IF(i EQ 1) THEN coords0 = coords ELSE coords1 = coords

	ranges[i,*] = tmp_range
	tai[i,*] = [tmp0, tmp1]

	undefine, tmp_range
	undefine, tmp0
	undefine, tmp1

	dim[i,*] = (size(map))[1:2]
	res[i] = dim[i,0] / float(ranges[i,1]-ranges[i,0])
	OPENW, lu, '/tmp/idltemp' + STRTRIM(STRING(i),2) + '.dat', /GET_LUN
	WRITEU,lu, map
	CLOSE,lu
	FREE_LUN,lu
	
	;*** Note that if time0 and time1 were not set by the user, then
	;*** they have now been set to the start and end times of the 
	;*** movie list by JMAP_CONSTRUCT2.
ENDFOR

start_rd = min(ranges)
end_rd = max(ranges)

time0 = min(tai[*,0])
time1 = max(tai[*,1])

fullres = max(res)
range = end_rd - start_rd
new_len = ceil(dim[*,1] * (time1-time0) / (tai[*,1]-tai[*,0]))
length = max(new_len)
height = ceil(fullres * (end_rd - start_rd))
rad_sort = reverse(sort(ranges[*,1]))

new_len = new_len[rad_sort]
width = width[rad_sort]
dim = dim[rad_sort, *]
tai = tai[rad_sort, *]
ranges = ranges[rad_sort, *]

fullmap = bytarr(total(long(dim[*,0])*long(dim[*,1])), n_ang)  ;bytarr(height, length, n_ang)

curr_pix = 0
FOR j=0,ntel-1 DO BEGIN ; add maps to final map in reverse order to give lower-radius images precedence
	i = rad_sort[j]
	map = BYTARR(dim[j,0], dim[j,1], n_ang)
	OPENR,lu,'/tmp/idltemp' + STRTRIM(STRING(i),2) + '.dat', /GET_LUN
	READU,lu, map
	CLOSE,lu
	FREE_LUN,lu
;	new_map = bytarr(dim[j,0], new_len[j], n_ang)
;	FOR k=0,n_ang-1 DO new_map[*,*,k] = map[0,0,k]  ; fill with median brightness
	border[j,0] = floor(length * float(tai[j,0]-time0) / (time1-time0))
	border[j,1] = border[j,0] + floor(float(dim[j,1]) * length / new_len[j])
;	new_map[*, x1:x1+dim[j,1]-1, *] = map
	border[j,2] = floor(height*(ranges[j,0] - start_rd)/range)
	border[j,3] = floor(height*(ranges[j,1] - start_rd)/range)
;	border = [transpose([y1[j],y2[j]]),border]
;	IF(border[0,1] GT border[1,0]) THEN border[1,0] = border[0,1]
;	fin_map = BYTARR(y2[j]-y1[j], length, n_ang)
	FOR k=0,n_ang-1 DO BEGIN
;		fin_map[*,*,k] = congrid(new_map[*,*,k], y2[j]-y1[j], length)
		IF(mean_adj) THEN BEGIN
			avg = mean(map[*,*,k])
			map[*,*,k] = bytscl(temporary(map[*,*,k]) - avg)
;			fin_map[*,*,k] = bytscl(temporary(fin_map[*,*,k]) - avg)
		ENDIF
	ENDFOR
	IF(n_ang EQ 1) THEN map = transpose(temporary(map)) ELSE map = transpose(temporary(map), [1,0,2])
	fullmap[curr_pix:curr_pix + long(dim[j,0])*long(dim[j,1])-1, *] = reform(map, long(dim[j,0])*long(dim[j,1]), n_ang)
	curr_pix = curr_pix + long(dim[j,0])*long(dim[j,1])
;	fullmap[y1[j]:y2[j]-1,*,*] = fin_map
	undefine, map
;	undefine, new_map
;	undefine, fin_map
ENDFOR

spawn, '/bin/rm /tmp/idltemp*.dat'

;border = border(0:ntel-1,*)

start_rd = min(ranges)
end_rd = max(ranges)

;IF(n_ang EQ 1) THEN fullmap = transpose(temporary(fullmap)) $
;ELSE fullmap = transpose(temporary(fullmap), [1,0,2])  ; switch first 2 dimensions only

   ;*** Create header structure
   h = create_jmap_strct2a()
   h.version = 2  ; 7/24/07
   h.start_time = double(time0)
   h.end_time = double(time1)
;   print, 'h.start_time '+h.start_time
;   print, 'h.end_time '+h.end_time
   h.proj_type = proj_type
   h.min_r = start_rd
   h.max_r = end_rd
   h.border[0:ntel-1,*] = border
   h.width[0:ntel-1] = width
   h.nreg = ntel
   get_utc, date_made
   h.date_made = utc2tai(date_made)
   h.compressed = 0
   s = size(fullmap)
   h.nx = length
   h.ny = height
   h.reg_dim[0:ntel-1,*] = reverse(dim,2)
   IF(datatype(telescope) NE 'UND') THEN BEGIN 
	h.satellite=telescope
	h.hassat='yes~'
   ENDIF
   ;help, h, /str

   break_file, savefile[0], disk, dir, name, ext
   ext='.jmp'
   ;IF(n_elements(savefile) LT n_ang) THEN BEGIN
	savefile = replicate(dir+name, n_ang)
	FOR i=0,n_ang-1 DO BEGIN
		curr_ang = strtrim(string(pa[i]),2)
		WHILE(strlen(curr_ang) LT 3) DO curr_ang = '0' + curr_ang
		savefile[i] = savefile[i] + '_' + curr_ang + ext
	ENDFOR
   ;ENDIF
   
   FOR i=0,n_ang-1 DO BEGIN
	h.pa = pa[i]
	write_jmap2a, fullmap[*,i], savefile[i], h
	;MESSAGE,'File saved as ' + savefile[i], /INFO
   ENDFOR
END
