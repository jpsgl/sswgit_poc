FUNCTION create_jmap_strct2a
;+
; $Id: create_jmap_strct2a.pro,v 1.4 2009/07/20 18:38:21 nathan Exp $
; NAME:
;	CREATE_JMAP_STRCT
;
; PURPOSE:
;	Create a structure that is used to hold the jMap imformation
;	that is used for the header in a .jmp file.  The structure
;	is initialized with default values.
;
; CATEGORY:
;	JMAPS
;
; CALLING SEQUENCE:
;	Result = CREATE_JMAP_STRCT()
;
; OUTPUTS:
;	A default jMap information structure.  
;
; EXAMPLE:
;	Struct = CREATE_JMAP_STRCT()
;
; MODIFICATION HISTORY:
; $Log: create_jmap_strct2a.pro,v $
; Revision 1.4  2009/07/20 18:38:21  nathan
; reformat structure defn to ease interpretation
;
; Revision 1.1  2007/11/08 16:56:36  nathan
; moved from adam/jmaps2a
;
;-
   max_reg = 10
   j = {jmap_strcta, $
    	version:0, $
	filename:'', $
	start_time:double(0), $
	end_time:double(0), $
	proj_type:0, $
	min_r:0.0, $
	max_r:0.0, $
	border:lonarr(max_reg,4), $
	nreg:0, $
	nx:0L, ny:0L, $
	reg_dim:lonarr(max_reg,2), $
	pa:0, $
	width:fltarr(max_reg), $
	date_made:double(0), $
	compressed:0, $
	hassat:'', $
	satellite:''}

   str = replicate(j,1)
   RETURN, str
END
