; JMAP_CONVERT -- reform old jmap files to version 2 format

PRO jmap_convert, filenames, new_dir
; $Id: jmap_convert.pro,v 1.1 2007/11/08 18:56:09 nathan Exp $
;
; $Log: jmap_convert.pro,v $
; Revision 1.1  2007/11/08 18:56:09  nathan
; moved from adam/jmaps2a
;
message, 'JMap Conversion', /inf

FOR i=0,n_elements(filenames)-1 DO BEGIN

print,'converting ', filenames[i], ' (', i+1, ' of ', n_elements(filenames), ')'
map = read_old_jmap(filenames[i], header=h)
break_file, filenames[i], disk, dir, name, ext
write_jmap2a, map, new_dir+name+ext, h

ENDFOR

END
