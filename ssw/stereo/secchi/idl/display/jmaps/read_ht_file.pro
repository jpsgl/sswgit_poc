PRO read_ht_file, file, tai, ht, angle, date
; $Id: read_ht_file.pro,v 1.2 2008/07/16 18:31:32 casto Exp $
;
; $Log: read_ht_file.pro,v $
; Revision 1.2  2008/07/16 18:31:32  casto
; Added dates
;
; Revision 1.1  2008/03/20 21:28:57  sheeley
; nbr - copied from adam/caryn on birch
;
openr,lu,file,/get_lun
a0=''
FOR i=0,9 DO readf,lu,format='(a80)',a0

m=0
WHILE(eof(lu) EQ 0) DO BEGIN
	readf,lu,format='(f6.2,a20,f6.1)',a1,a2,a3
	m=m+1
ENDWHILE

close,lu

ht = fltarr(m)
ang = fltarr(m)
date = strarr(m)
tai = dblarr(m)

openr,lu,file

a0=''
FOR i=0,9 DO readf,lu,format='(a80)',a0
FOR i=0,m-1 DO BEGIN
	d = ''
	readf,lu,format='(f6.2,a20,f6.1)',h,d,a
	ht[i] = h
	date[i] = d
	tai[i] = anytim2tai(date[i])
	ang[i] = a
ENDFOR

s = sort(tai)
tai = tai[s]
date = date[s]
ang = ang[s]
ht = ht[s]

;angle = ang[0]
p = strsplit(file, '_')
n = n_elements(p)
angle = ang[0];float(strmid(file, p[n-2], p[n-1]-p[n-2]-1))
date = date[0]

close,lu
free_lun,lu

END
