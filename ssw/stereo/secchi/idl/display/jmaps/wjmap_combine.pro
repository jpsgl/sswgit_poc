;+
; $Id: wjmap_combine.pro,v 1.7 2012/06/05 20:15:18 cooper Exp $
;  NAME: wjmap_combine
;
;  PURPOSE: widget front-end for jmap_combine
;
;  USAGE: wjmap_combine
;
;  KEYWORDS: None
;
;  WRITTEN: Adam Herbst, 2007/08/09
;
; $Log: wjmap_combine.pro,v $
; Revision 1.7  2012/06/05 20:15:18  cooper
; remembers jmaps directory
;
; Revision 1.6  2011/07/05 14:32:57  cooper
; added EUVI to list of telescopes
;
; Revision 1.5  2009/07/20 18:47:39  nathan
; added /DEBUG option; define jmap_dir if not defined
;
; Revision 1.4  2009/06/15 16:16:25  cooper
; modified widget, added satellite info
;
; Revision 1.3  2009/05/26 19:54:30  cooper
; nr - fix hardcoded path
;
; Revision 1.2  2008/07/01 17:08:59  lee
; Changed default save directory to /sjmaps08
;
; Revision 1.1  2007/11/08 18:56:10  nathan
; moved from adam/jmaps2a
;
;-

PRO set_times

COMMON combine_common, filenames, hdrs, n_maps, time0, time1, start_time, end_time, regions, telescopes, tel_num, $
	savefile, list_num, list_len, multi_maps, list_fields, tel_fields, scroll_fields, opt_fields, file_fields, comb_fields, $
	debugon

WIDGET_CONTROL, comb_fields[1], SET_VALUE=[start_time>0.0, time0>0.0, end_time>1.0]
WIDGET_CONTROL, comb_fields[3], SET_VALUE=[end_time>0.0, start_time>0.0, time1>1.0]
empty = n_elements(filenames) LE 1
start_str = (empty ? 'none' : utc2str(tai2utc(double(start_time))))
end_str = (empty ? 'none' : utc2str(tai2utc(double(end_time))))
WIDGET_CONTROL, comb_fields[0], SET_VALUE='Start Time: ' + start_str
WIDGET_CONTROL, comb_fields[2], SET_VALUE='End Time: ' + end_str

END


PRO set_tels

COMMON combine_common

IF(n_maps LT 1) THEN RETURN
top = WIDGET_INFO(list_fields[1], /LIST_TOP)
num = WIDGET_INFO(list_fields[1], /LIST_NUM_VISIBLE) < list_len
FOR i=0,num-1 DO WIDGET_CONTROL, tel_fields[1+i], SET_VALUE=telescopes[regions[(top+i)*n_maps+1]]
FOR i=num,5 DO WIDGET_CONTROL, tel_fields[1+i], SET_VALUE=''
END


PRO base_event, event

COMMON combine_common
COMMON DIR_COMMON, mvi_dir, jmap_dir, median_dir, diff_dir, image_dir

WIDGET_CONTROL, event.id, GET_UVALUE=ev

CASE ev OF

'base2': BEGIN
	set_tels
END
'file_list': BEGIN
	set_tels
END
'next_list': BEGIN
	IF(n_maps LT 1) THEN RETURN
	list_num = (list_num + 1) MOD n_maps
	names = filenames[indgen(list_len)*n_maps + list_num + 1]
	WIDGET_CONTROL, list_fields[1], SET_VALUE=names
	WIDGET_CONTROL, scroll_fields[2], SET_VALUE='Viewing: ' + strtrim(string(list_num+1),2) + ' of ' + strtrim(string(n_maps),2)
END
'prev_list': BEGIN
	IF(n_maps LT 1) THEN RETURN
	list_num = (list_num - 1 + n_maps) MOD n_maps
	names = filenames[indgen(list_len)*n_maps + list_num + 1]
	WIDGET_CONTROL, list_fields[1], SET_VALUE=names
	WIDGET_CONTROL, scroll_fields[2], SET_VALUE='Viewing: ' + strtrim(string(list_num+1),2) + ' of ' + strtrim(string(n_maps),2)
END
'add_file': BEGIN
	IF datatype(jmap_dir) EQ 'UND' THEN jmap_dir=curdir()
	names = dialog_pickfile(PATH=jmap_dir, /MULTIPLE_FILES, /MUST_EXIST, GET_PATH=temp)
	IF(names[0] EQ '') THEN RETURN
	IF(n_maps LT 1) THEN BEGIN
		IF(multi_maps) THEN n_maps = n_elements(names) ELSE n_maps = 1
		WIDGET_CONTROL, opt_fields[3], SENSITIVE=0
	ENDIF ELSE IF(multi_maps AND n_elements(names) NE n_maps) THEN BEGIN
		message, 'File list is the wrong length', /inf
		RETURN
	ENDIF
	jmap_dir=temp
	filenames = [filenames,names]
	IF(multi_maps) THEN list_len = list_len + 1 ELSE list_len = list_len + n_elements(names)
	FOR i=0,n_elements(names)-1 DO BEGIN
		junk = read_jmap2a(names[i], HEADER=hdr, /NODATA)
		hdrs = [hdrs, hdr]
	ENDFOR
	regions = [regions, replicate(tel_num, n_elements(names))]
	IF(start_time LT 0.0 OR start_time EQ time0) THEN start_time = min(hdrs[1:*].start_time)
	IF(end_time LT 0.0 OR end_time EQ time1) THEN end_time = max(hdrs[1:*].end_time)
	time0 = min(hdrs[1:*].start_time)
	time1 = max(hdrs[1:*].end_time)
	IF(start_time LT 2.0) THEN start_time = time0
	IF(end_time LT 2.0) THEN end_time = time1
	set_times
	set_tels
	names = filenames[indgen(list_len)*n_maps + list_num + 1]
	WIDGET_CONTROL, list_fields[1], SET_VALUE=names
	WIDGET_CONTROL, scroll_fields[2], SET_VALUE='Viewing: ' + strtrim(string(list_num+1),2) + ' of ' + strtrim(string(n_maps),2)
END
'remove_file': BEGIN
	IF(n_maps LT 1) THEN RETURN
	inds = WIDGET_INFO(list_fields[1], /LIST_SELECT)
	IF(inds[0] LT 0) THEN RETURN
	new_inds = intarr(n_elements(inds)*n_maps)
	FOR i=0,n_elements(inds)-1 DO new_inds[i*n_maps:i*n_maps+n_maps-1] = indgen(n_maps) + inds[i]*n_maps + 1
	filenames[new_inds] = '???'
	keep = where(filenames NE '???')
	hdrs = hdrs[keep]
	filenames = filenames[keep]
	regions = regions[keep]
	list_len = list_len - n_elements(inds)
	IF(n_elements(filenames) EQ 1) THEN BEGIN
		WIDGET_CONTROL, list_fields[1], SET_VALUE=filenames
		WIDGET_CONTROL, scroll_fields[2], SET_VALUE='Viewing: none'
		time0 = -1d
		time1 = -1d
		n_maps = 0
	ENDIF ELSE BEGIN
		names = filenames[indgen(list_len)*n_maps + list_num + 1]
		WIDGET_CONTROL, list_fields[1], SET_VALUE=names
		time0 = min(hdrs[1:*].start_time)
		time1 = max(hdrs[1:*].end_time)
	ENDELSE
	start_time = start_time > time0 < time1
	end_time = end_time > time0 < time1
	set_times
	set_tels
END
'telescope': BEGIN
	tel_num = event.index
	WIDGET_CONTROL, file_fields[4], SET_VALUE='Set to ' + telescopes[tel_num]
END
'vertical': BEGIN
END
'multiple': BEGIN
	IF(event.select) THEN multi_maps = 1 ELSE multi_maps = 0
END
'set_start': BEGIN
	IF(n_maps LT 1) THEN RETURN
	ind = WIDGET_INFO(list_fields[1], /LIST_SELECT)
	ind = ind[0]
	IF(ind LT 0) THEN RETURN
	start_time = hdrs[ind+1].start_time
	end_time = end_time > start_time
	set_times
END
'set_end': BEGIN
	IF(n_maps LT 1) THEN RETURN
	ind = WIDGET_INFO(list_fields[1], /LIST_SELECT)
	ind = ind[0]
	IF(ind LT 0) THEN RETURN
	end_time = hdrs[ind+1].end_time
	start_time = start_time < end_time
	set_times
END
'set_tel': BEGIN
	IF(n_maps LT 1) THEN RETURN
	list_inds = WIDGET_INFO(list_fields[1], /LIST_SELECT)
	IF(list_inds[0] LT 0) THEN RETURN
	FOR i=0,n_elements(list_inds)-1 DO regions[indgen(n_maps) + list_inds[i]*n_maps + 1] = tel_num
	set_tels
END
'start_time': BEGIN
	start_time = double(event.value)
	set_times
END
'end_time': BEGIN
	end_time = double(event.value)
	set_times
END
'savefile': BEGIN
	savefile = dialog_pickfile(PATH=jmap_dir, FILTER='*.jmp')
	savefile = savefile[0]
END
'combine': BEGIN

	IF(n_elements(filenames) LE 1) THEN RETURN
	IF(savefile[0] EQ '') THEN savefile = dialog_pickfile(PATH=jmap_dir, FILTER='*.jmp')
	IF(savefile[0] EQ '') THEN RETURN
	savefile = savefile[0]
	break_file, savefile, disk, dir, name, ext
	WIDGET_CONTROL, opt_fields[2], GET_VALUE=vert
	vert = vert[0]
	FOR i=0,n_maps-1 DO BEGIN
		inds = indgen(list_len)*n_maps + i + 1
		s = strmid(filenames[inds[0]], strpos(filenames[inds[0]],'_',/reverse_search))
		IF(vert) THEN regions1=0 ELSE regions1=regions[inds]
		jmap_combine, filenames[inds], dir + name + s, START_TIME=start_time, END_TIME=end_time, REGIONS=regions1, DEBUG=debugon
	ENDFOR
END

ENDCASE

END


PRO wjmap_combine, DEBUG=debug

COMMON combine_common

if keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0

hdrs = create_jmap_strct2a()
start_time = -1d
end_time = -1d
time0 = -1d
time1 = -1d
regions = 0
n_maps = 0
multi_maps = 1
list_len = 0
list_num = 0
filenames = ''
savefile = ''

telescopes = ['EUVI','COR1', 'COR2', 'HI1', 'HI2'] ;to add new telescope, just include name here
tel_num = 0

base = WIDGET_BASE(COLUMN=1, TITLE='JMap Combination', UVALUE='base')

base2 = WIDGET_BASE(base, ROW=1, UVALUE='base2', /TRACKING_EVENTS)

list_base = WIDGET_BASE(base2, COLUMN=1, MAP=1, UVALUE='list_base')
	list_fields = WIDGET_LABEL(list_base, VALUE='Selected Files')
	list_fields = replicate(list_fields, 2)
	list_fields[1] = WIDGET_LIST(list_base, UVALUE='file_list', XSIZE=60, YSIZE=6, VALUE='')

tel_base = WIDGET_BASE(base2, COLUMN=1, MAP=1, UVALUE='tel_base')
	tel_fields = WIDGET_LABEL(tel_base, VALUE='Telescope')
	tel_fields = replicate(tel_fields, 7)
	FOR i=1,6 DO tel_fields[i] = WIDGET_LABEL(tel_base, VALUE='       ', /ALIGN_LEFT)

scroll_base = WIDGET_BASE(base, ROW=1, MAP=1, UVALUE='scroll_base')
	scroll_fields = WIDGET_BUTTON(scroll_base, UVALUE='prev_list', VALUE='Previous Map')
	scroll_fields = replicate(scroll_fields, 3)
	scroll_fields[1] = WIDGET_BUTTON(scroll_base, UVALUE='next_list', VALUE='Next Map')
	scroll_fields[2] = WIDGET_LABEL(scroll_base, VALUE='Viewing: none      ', /ALIGN_LEFT)

file_base = WIDGET_BASE(base, ROW=1, MAP=1, UVALUE='file_base')
	file_fields = WIDGET_BUTTON(file_base, UVALUE='add_file', VALUE='Add Files')
	file_fields = replicate(file_fields, 5)
	file_fields[1] = WIDGET_BUTTON(file_base, UVALUE='remove_file', VALUE='Remove Selected')
	file_fields[2] = WIDGET_BUTTON(file_base, UVALUE='set_start', VALUE='Use Start Time')
	file_fields[3] = WIDGET_BUTTON(file_base, UVALUE='set_end', VALUE='Use End Time')
	file_fields[4] = WIDGET_BUTTON(file_base, UVALUE='set_tel', VALUE='Set to ' + telescopes[tel_num])

opt_base = WIDGET_BASE(base, ROW=1, MAP=1, UVALUE='opt_base')
	opt_fields = WIDGET_LABEL(opt_base, VALUE='Importing:')
	opt_fields = replicate(opt_fields, 4)
	opt_fields[1] = WIDGET_DROPLIST(opt_base, UVALUE='telescope', VALUE=telescopes)
	opt_fields[2] = CW_BGROUP(opt_base, ['Vertical Only'], UVALUE='vertical', /NONEXCLUSIVE, SET_VALUE=[0])
	opt_fields[3] = CW_BGROUP(opt_base, ['Multiple Maps'], UVALUE='multiple', /NONEXCLUSIVE, SET_VALUE=[1])

comb_base = WIDGET_BASE(base, COLUMN=1, MAP=1, UVALUE='comb_base')
	comb_fields = WIDGET_LABEL(comb_base, VALUE='Start Time: none                    ', /ALIGN_LEFT)
	comb_fields = replicate(comb_fields, 6)
	comb_fields[1] = CW_FSLIDER(comb_base, MINIMUM=0.0, MAXIMUM=1.0, VALUE=0.0, UVALUE='start_time', $
		XSIZE=200, SCROLL=.005, /SUPPRESS_VALUE)
	comb_fields[2] = WIDGET_LABEL(comb_base, VALUE='End Time: none                    ', /ALIGN_LEFT)
	comb_fields[3] = CW_FSLIDER(comb_base, MINIMUM=0.0, MAXIMUM=1.0, VALUE=1.0, UVALUE='end_time', $
		XSIZE=200, SCROLL=.005, /SUPPRESS_VALUE)
	comb_fields[4] = WIDGET_BUTTON(comb_base, UVALUE='savefile', VALUE='Select Savefile')
;	comb_fields[5] = WIDGET_BUTTON(comb_base, UVALUE='concat', VALUE='Concatenate')
	comb_fields[5] = WIDGET_BUTTON(comb_base, UVALUE='combine', VALUE='Combine')

WIDGET_CONTROL, base, /REALIZE

XMANAGER, 'base', base

END
