PRO ht_convert, HTS = hts, DATAFILE = datafile, CARR_NUM = carr_num, EL = el, WL = wl, PLOT_NUM = plot_num, $
	              OUTFILE = outfile, STACK = stack, BATCH = batch, ANGLE = angle, START_TIME = start_time,$
	              END_TIME = end_time
;+
; $Id: ht_convert.pro,v 1.1 2007/11/08 18:30:35 nathan Exp $
; NAME:
;	HT_CONVERT
;
; PURPOSE:
;	This procedure converts raw ht data, as made by jmaps, to the .ht
;	format read by xplot_ht.
;
; CATEGORY:
;	Utility
;
; CALLING SEQUENCE:
;	HT_CONVERT
;
; INPUTS:
;	None
;
; OPTIONAL INPUTS:
;	HTS:	A height time structure that will be converted to .ht format.
;	DATAFILE: The name of the raw datafile.  If not supplied (and DATA not supplied)
;		  a pickfile widget will be given.
;	CARR_NUM: The Carrington Rotation number of the data.  Required if DATA
;		  is used.
;	PLOT_NUM: The plot number of a specific ht plot within the hts structure to 
;		  be converted.
;	OUTFILE:  The name of of the .ht file to write to.  If not supplied, the user will
;		  be prompted for one via the pickfile widget.
;
;	START_TIME: New input to accommodate non-Carrington rotation based jmaps.  If this is set
;		    then CARR_NUM, EL, and WL are all unnecessary.  
; KEYWORD PARAMETERS:
;	EL, WL:	The limb on which the data was taken.  Required if DATA is supplied.
;	STACK:  Makes the first point of each individual plot be at time 0.
;	BATCH:  If set, a multiple data files can be selected and fed into the 
;		same .ht file.
; OUTPUTS:
;	None
; OPTIONAL OUTPUTS:
;	None
;
; COMMON BLOCKS:
;	None
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;	/STACK can lead to misleading plots if tracks start at varying heights.
;
; PROCEDURE:
;	
;
; EXAMPLE:
;	HT_CONVERT
;	HT_CONVERT, OUTFILE = 'c1924_el.ht', /STACK
;	HT_CONVERT, DATA = hts, CARR_NUM = 1924, /EL, PLOT_NUM = 3
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Walters, NRL Summer Student, Sept. 1997
; $Log: ht_convert.pro,v $
; Revision 1.1  2007/11/08 18:30:35  nathan
; moved from adam/jprogs
;
;	Sept. 1998	Fixed bug in /WL, /EL keywords, Jeff Walters
;       Aug. 2001       Modified to accommodate new jmaps that are 
;                       not complete Carrington rotations
;       Dec. 05, 2003  Harry Warren (and nrs) modified it to add
;                      extra decimals for TRACE data.
;-

   COMMON DIR_COMMON, mvi_dir, jmap_dir, median_dir, diff_dir, image_dir

   IF NOT(KEYWORD_SET(hts) OR KEYWORD_SET(datafile)) THEN BEGIN
      datafile = PICKFILE(GET_PATH = path, /READ, FILTER = '*.idl', /MUST_EXIST, PATH = '/net/redwood/data1')
   ENDIF
   
   limb = -1
   
   
   
   IF ((limb eq -1) AND KEYWORD_SET(hts)) THEN $
      limb = KEYWORD_SET(el)
      
   IF NOT(KEYWORD_SET(hts)) THEN RESTORE, datafile
   
   IF (DATATYPE(angle) eq 'UND' AND NOT(KEYWORD_SET(el)) AND $
       NOT(KEYWORD_SET(wl))) THEN $
      READ, angle, PROMPT = 'Enter the position angle: '
      
   IF KEYWORD_SET(datafile) THEN limb = ((angle ge 0) AND (angle le 180))
   
   IF (DATATYPE(plot_num) ne 'UND') THEN BEGIN
      index = WHERE(hts.plot_num eq plot_num)
      IF (index(0) ne -1) THEN ht_dat = hts(index)
   ENDIF ELSE ht_dat = hts
   
  
   
   IF KEYWORD_SET(stack) THEN BEGIN
      n = MAX(ht_dat.plot_num)
      FOR i=0, n DO BEGIN
         indices = WHERE(ht_dat.plot_num eq i)
         ht_dat(indices).time = ht_dat(indices).time - ht_dat(indices(0)).time
      ENDFOR
      good = WHERE(ht_dat.time le 30*3600.)
      ht_dat = ht_dat(good)
   ENDIF
   
   ht_dat = UNIQ_AVG(ht_dat)
   
   
   
   IF KEYWORD_SET(start_time) THEN BEGIN
   ;*** This is a new section that does not rely on Carrington rotations.
      tai = LONG(ht_dat.time);+LONG(ANYTIM2TAI(start_time))
   ENDIF ELSE BEGIN
      IF limb THEN BEGIN
         tai = ht_dat.time + ANYTIM2TAI(CARRDATE2(carr_num, /EL)) 
      ENDIF ELSE BEGIN
         tai = ht_dat.time + ANYTIM2TAI(CARRDATE2(carr_num, /WL))
      ENDELSE
   ENDELSE
         
   sort = SORT(ht_dat.height)
   ht_dat = ht_dat(sort)
   tai = tai(sort)  

   npts = n_elements(ht_dat)
   
   IF NOT(KEYWORD_SET(outfile)) THEN BEGIN
      outfile = DIALOG_PICKFILE(/WRITE, GET_PATH = path, PATH = jmap_dir, FILTER = '*.ht')
      outfile = path+outfile
   ENDIF

   filename = outfile
   OPENW, Unit, outfile, /GET_LUN
   outfile = Unit
   temp = ANYTIM2UTC(DOUBLE(tai(0)), /ecs)
   temp = STR_SEP(temp, ' ')
   date = temp(0)
   time = temp(1)
   
   ans4=''
;   read,'Is this for TRACE data(y/n)? ',ans4
   if (ans4 eq 'y') then begin
      PRINTF, outfile, '#VERSION: 4' 
      FORMAT='(f8.4,a11,1x,a8,f6.1,a3,i3,2f6.2)'
   endif else begin
      FORMAT='(f6.2,a11,1x,a8,f6.1,a3,i3,2f6.2)'
   endelse
      
   PRINTF, outfile, '#DATE-OBS: ', date
   PRINTF, outfile, '#TIME-OBS: ', time
   PRINTF, outfile, '#DETECTOR: N/A'
   PRINTF, outfile, '#FILTER: N/A'
   PRINTF, outfile, '#POLAR: N/A'
   PRINTF, outfile, '#OBSERVER: ', GETENV('USER')
   PRINTF, outfile, '#FEATURE_CODE: N/A'
   PRINTF, outfile, '#IMAGE_TYPE: jmap'
   PRINTF, outfile, '#COMMENT:'
   
   PRINTF, outfile, '# HEIGHT   DATE     TIME   ANGLE' 
   
;   IF NOT(KEYWORD_SET(datafile)) THEN angle = 90 + 180*(1 - limb)
   FOR i=0, npts-1 DO BEGIN
      temp = ANYTIM2UTC(DOUBLE(tai(i)), /ECS)
      temp = STR_SEP(temp, ' ')
      date = temp(0)
      time = temp(1)
      ;ht_dat.height = ht_dat.height * 100 ; nrs & hw
      ;ht_dat.height = ROUND(ht_dat.height); nrs & hw
      ;ht_dat.height = ht_dat.height / 100.; nrs & hw
      ;PRINTF, outfile, ht_dat(i).height, date, time, angle, FORMAT='(f6.2,a11,1x,a8,f6.1,a3,i3,2f6.2)'
      PRINTF, outfile, ht_dat(i).height, date, time, angle, FORMAT = FORMAT ; nrs 12/05/03 (hw)
        
   ENDFOR
   
   MESSAGE, 'The ht file has been saved as ' + filename + '.', /INFORMATIONAL
   FREE_LUN, Unit
END
   	   
   
   	   
   			           
   						   
   						   			  
   
   
  
   
   
         
