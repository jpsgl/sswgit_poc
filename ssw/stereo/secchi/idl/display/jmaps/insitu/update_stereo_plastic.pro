; ; $Id: update_stereo_plastic.pro,v 1.3 2012/01/03 15:57:53 cooper Exp $
;
;PRO update_omni
;NOTE:environment variable INSITU_PATH must be defined
;Download STEREO plastic insitu data and store it to path given by INSITU_PATH
;By default, data is reformatted for use by read_is/jmap_in_situ
;KEYWORDS
;	all:get all data, set to start from 2007, this could be modified
;	noreformat:do not reformat the data
;written by Tom Cooper
;
; $Log: update_stereo_plastic.pro,v $
; Revision 1.3  2012/01/03 15:57:53  cooper
; b changed to V09
;
; Revision 1.2  2011/06/02 18:05:41  cooper
; data changed from version7 to version8
;
; Revision 1.1  2010/06/29 15:28:06  cooper
; Version 1
;

;reformat files to style in line marked by ##
PRO reformat_plastic,files
	FOR j=0,n_elements(files)-1 DO BEGIN
		file=files[j]
		;read in original file
		temp={y:0,d:0,h:0,m:0,date:0.,dens:0.,vel:0.,t:0.,a4:0,a5:0,a6:0,a7:0,a8:0,a9:0,$
			a10:0,a11:0,vr:0.,vt:0.,vn:0.}
		d=replicate(temp,366L*24*6) ;need to increase for higher res (i.e if we go to 1min files)
		OPENR,lun,file,/get_lun
		comment=strarr(2)
		readf,lun,comment
		i=0L
		WHILE eof(lun) NE 1 DO BEGIN
			readf,lun,temp
			d[i]=temp
			i++
		ENDWHILE
		d=d[0:i-1]
		free_lun,lun
		dates=strtrim(d.y,2)+'-'+strtrim(d.d,2)+'T'+strtrim(d.h,2)+':'+strtrim(d.m,2)
		dates=anytim2utc(dates,/ccsds)
		data=strarr(i)
		;string(10B) skips to next line, so that the date can be read in by itself as a string
		data=dates+string(10B)+strtrim(d.dens,1)+string(d.vel)+string(d.t)+string(d.vr)+string(d.vt)+string(d.vn)
		;write to new file
		OPENW,lun,file,/get_lun
		printf,lun,'TIME DENS VEL TEMP VR VT VN ;##
		printf,lun, data
		free_lun,lun
	ENDFOR
END

;update stereo, if keyword all is set then it gets all data available, otherwise starts with the dates
;given in the plasticdates.txt file
PRO update_stereo_plastic,all=all,noreformat=noreformat
	datesfile=concat_dir(getenv('INSITU_PATH'),'plasticdates.txt')
	IF (file_test(datesfile) AND ~keyword_set(all)) THEN BEGIN
		OPENR,lun,datesfile,/get_lun
		pldates=''
		readf,lun,pldates
		free_lun,lun
		ayear=strmid(pldates,0,4)
		ay=fix(ayear) ;store year as 2 variables, since is incremented in finding which data to get
		amonth=strmid(pldates,4,2)
		byear=strmid(pldates,6,4)
		by=fix(byear)
		bmonth=strmid(pldates,10,2)
	ENDIF ELSE all=1
	IF keyword_set(all) THEN BEGIN
		ayear='2007'
		ay=2007
		amonth='02'
		byear='2007'
		by=2007
		bmonth='03'
	ENDIF
	output_dir=getenv('INSITU_PATH');saved here
	site='http://stereo-ssc.nascom.nasa.gov'
	plasticpath='/data/ins_data/plastic/level2/Protons/ASCII/10min/'
	filenamesA=strarr(500)
	filenamesB=strarr(500)

	i=0
	WHILE sock_check(site+plasticpath+'A/'+ayear+'/STA_L2_PLA_1DMax_10min_'+ayear+amonth+'_V08.txt') DO BEGIN
		apath=plasticpath+'A/'+ayear+'/'
		;this line may need to be modified, if the name of the filenames
		;is changed if a new version of the data is released
		fileA='STA_L2_PLA_1DMax_10min_'+ayear+amonth+'_V08.txt'
		filenamesA(i)=sock_find(site,fileA,path=apath)
		i+=1
		amonth=fix(amonth)
		ayear=fix(ayear)
		print,'Found A data for',ayear,amonth
		IF amonth NE 12 THEN amonth+=1 ELSE BEGIN amonth=1 & ayear+=1 & ENDELSE
		ayear=strtrim(ayear,2)
		amonth=strtrim(amonth,2)
		IF strlen(amonth) EQ 1 THEN amonth='0'+amonth
	ENDWHILE
	IF i eq 0 THEN print,'Too early to update A' ELSE filenamesA=filenamesA[0:i-1]
	;seperate loops for A and B, since dates dont line up exactly
	j=0
	WHILE sock_check(site+plasticpath+'B/'+byear+'/STB_L2_PLA_1DMax_10min_'+byear+bmonth+'_V09.txt') DO BEGIN
		bpath=plasticpath+'B/'+byear+'/'
		fileB='STB_L2_PLA_1DMax_10min_'+byear+bmonth+'_V09.txt'
		filenamesB(j)=sock_find(site,fileB,path=bpath)
		j+=1
		bmonth=fix(bmonth)
		byear=fix(byear)
		print,'Found B data for',byear,bmonth
		IF bmonth NE 12 THEN bmonth+=1 ELSE BEGIN bmonth=1 & byear+=1 & ENDELSE
		byear=strtrim(byear,2)
		bmonth=strtrim(bmonth,2)
		IF strlen(bmonth) EQ 1 THEN bmonth='0'+bmonth
	ENDWHILE
	IF j eq 0 THEN print,'Too early to update B' ELSE filenamesB=filenamesB[0:j-1]
	
	IF i eq 0 AND j eq 0 THEN BEGIN print,'Too early to update stereo' & return & ENDIF

	date=strtrim(ayear,2)+strtrim(amonth,2)+strtrim(byear,2)+strtrim(bmonth,2)
	OPENW,lun,datesfile,/get_lun
	printf,lun,date
	free_lun,lun
	file_chmod,datesfile,/g_write,/o_write,/o_read,/u_read,/u_write
	IF n_elements(filenamesA) NE 500 THEN BEGIN
		FOR j=0,n_elements(filenamesA)-1 DO BEGIN
			print,'Downloading A file: ' +file_basename(filenamesA[j])
			sock_copy,filenamesA[j],out_dir=output_dir
		ENDFOR
	ENDIF
	IF n_elements(filenamesB) NE 500 THEN BEGIN
		FOR j=0,n_elements(filenamesB)-1 DO BEGIN
			print,'Downloading B file: ' +file_basename(filenamesB[j])
			sock_copy,filenamesB[j],out_dir=output_dir;all files copied,from here on making into one year files and moving into correct directories
		ENDFOR
	ENDIF
	filenamesA=concat_dir(output_dir,file_basename(filenamesA)) ;we need to use local file names from here on out
	filenamesB=concat_dir(output_dir,file_basename(filenamesB))
	nyearsa=fix(strmid(file_basename(filenamesA[n_elements(filenamesA)-1]),23,4))-ay+1
	nyearsb=fix(strmid(file_basename(filenamesB[n_elements(filenamesB)-1]),23,4))-by+1
	FOR s=0,1 DO BEGIN
		CASE s OF
		0:BEGIN & sat='A' & filenames=filenamesA & nyears=nyearsa & year=ay &END
		1:BEGIN & sat='B' & filenames=filenamesB & nyears=nyearsb & year=by &END
		ENDCASE
		IF n_elements(filenames) EQ 500 THEN continue;if we found no files for this satellite
		IF ~keyword_set(noreformat) THEN BEGIN  ;don't do anything with it (finding no files means left at default size)
			print,'Reformatting plastic files for '+ sat
			reformat_plastic,filenames
		ENDIF
		;remove files if all keyword is set
		IF keyword_set(all) THEN BEGIN
			y=ay
			FOR i=1,nyears DO BEGIN
				output_file=concat_dir(concat_dir(output_dir,'ST_'+sat,/dir),'plastic'+sat+strtrim(y,2)+'.txt')
				IF file_test(output_file) THEN file_delete,output_file
				y++
			ENDFOR
		ENDIF
		FOR i=1,nyears DO BEGIN
			output_file=concat_dir(concat_dir(output_dir,'ST_'+sat,/dir),'plastic'+sat+strtrim(year,2)+'.txt')
			files=filenames[where(STRPOS(filenames,strtrim(year,2)) NE -1)]
;if the all keyword is set, overwrites all previous files stored in the output directory
;if all keyword is not set, appends files to the end of the file for their respective years
;if no files yet made for a year, makes a new yearly file
			IF file_test(output_file) THEN BEGIN ;yearly file exists
				OPENU,lun,output_file,/get_lun,/append
				FOR k=0,n_elements(files)-1 DO BEGIN
					print,'Updating file: '+file_basename(output_file)$ 
						+'  Adding month '+strmid(file_basename(files[k]),27,2)
					OPENR,lun2,files[k],/get_lun,/delete
					dummy=strarr(2)
					READF,lun2,dummy
					copy_lun,lun2,lun,/eof
					free_lun,lun2
				ENDFOR
				free_lun,lun
				year+=1	
			ENDIF ELSE BEGIN
				firstfile=files[0] ;make new yearly file
				copypath=concat_dir(file_dirname(firstfile),'ST_'+sat,/dir)
				copy=concat_dir(copypath,'plastic'+sat+strtrim(year,2)+'.txt')
				file_copy,firstfile,copy
				file_delete,firstfile
				print,'Adding file: plastic'+sat+strtrim(year,2)+'.txt'
				IF n_elements(files) EQ 1 THEN year+=1 ELSE BEGIN 
					i-=1
				;i-1 because if there are more files for this year, we need to append them i.e. the first if statement must happen
					filenames=filenames[where(filenames NE firstfile)]
				ENDELSE
			ENDELSE
		ENDFOR
	ENDFOR
END