; $Id: update_wind.pro,v 1.2 2012/01/03 15:02:29 cooper Exp $
;
;PRO update_omni
;NOTE:environment variable INSITU_PATH must be defined
;Download OMNI insitu data and store it to path given by INSITU_PATH
;By default, data is reformatted for use by read_is/jmap_in_situ
;OPTIONAL INPUT
;	y:get data from given year to the most recent data
;KEYWORDS
;	all:get all data, set to start from 2007, this could be modified
;	noreformat:do not reformat the data
;written by Tom Cooper
;
; $Log: update_wind.pro,v $
; Revision 1.2  2012/01/03 15:02:29  cooper
; changed file path
;
; Revision 1.1  2010/06/29 15:28:07  cooper
; Version 1
;

;reformats wind files to format at line marked with ##
PRO reformat_wind,filenames
	FOR j=0,n_elements(filenames)-1 DO BEGIN
		file=filenames[j]
		temp={y:0,d:0,h:0,m:0,num:0,perc:0,flag:0,timeshift:0,xphase:0,yphase:0,zphase:0,$
			btotal:0.,bx:0.,bye:0.,bze:0.,bym:0.,bzm:0.,tshiftrms:0.,rmsphase:0.,$
			rmsb:0.,rmsvec:0.,num2:0.,vel:0.,vx:0.,vy:0.,vz:0.,dens:0.,temp:0}
		d=replicate(temp,366L*24*12)
		OPENR,lun,file,/get_lun
		i=0L
		WHILE eof(lun) NE 1 DO BEGIN
			readf,lun,temp
			d[i]=temp
			i++
		ENDWHILE
		d=d[0:i-1]
		free_lun,lun
		dates=strtrim(d.y,2)+'-'+strtrim(d.d,2)+'T'+strtrim(d.h,2)+':'+strtrim(d.m,2)
		dates=anytim2utc(dates,/ccsds)
		;string(10B) skips to next line, so that the date can be read in by itself as a string
		data=dates+string(10B)+strtrim(d.dens,2)+string(d.vel)+string(d.temp)+string(d.bx)+string(d.bye)+$
			string(d.bze)+string(d.bym)+string(d.bzm)+string(d.vx)+string(d.vy)+string(d.vz)
		OPENW,lun,file,/get_lun
		printf,lun,'TIME DENS VEL TEMP BX BY(GSE) BZ(GSE) BY(GSM) BZ(GSM) VX VY VZ ;##
		printf,lun,data
		free_lun,lun
		print,'WIND file reformatted: '+file_basename(file)
	ENDFOR
END
;update wind data, keyword all gets all data starting from 2007, y=year gets starting starting from
;year until present, keyword reformat puts the data into format used by read_is
PRO update_wind,all=all,y=y,noreformat=noreformat
	IF keyword_set(all) THEN year=2007 ;can start as early as 1998
	IF keyword_set(y) THEN year=y
	output_dir=concat_dir(getenv('INSITU_PATH'),'WIND',/dir)
	IF ~file_test(output_dir) THEN file_mkdir,output_dir
	IF datatype(year) EQ 'UND' THEN BEGIN
		count=0
		x=file_search(concat_dir(output_dir,'wind*'),count=count)
			IF count eq 0 THEN year=2007 ELSE BEGIN
			x=x[reverse(sort(x))]
			f=file_basename(x[0])
			len=strlen(f)
			year=strmid(f,len-8,4)
		ENDELSE
	ENDIF
	;filepath changed 1/3/12 cooper
	site='http://nssdcftp.gsfc.nasa.gov/spacecraft_data/omni/high_res_omni/sc_specific/5min/'
	filenames=strarr(200)

	i=0
	WHILE sock_check(site+'wind_5min_b'+strtrim(year,2)+'.txt') DO BEGIN
		file='wind_5min_b'+strtrim(year,2)+'.txt'
		print,'Found WIND data for '+strtrim(year,2)
		filenames[i]=sock_find(site,file)
		i+=1
		year=fix(year)+1
		year=strtrim(year,2)
	ENDWHILE
	IF i eq 0 THEN BEGIN print,'Too early to update WIND data' & return & endif
	filenames=filenames[0:i-1]
	FOR j=0,n_elements(filenames)-1 DO BEGIN
		print,'Downloading wind file:' +file_basename(filenames[j])
		sock_copy,filenames[j],out_dir=output_dir
	ENDFOR
	filenames=concat_dir(output_dir,file_basename(filenames))
	newnames=concat_dir(output_dir,'wind'+strmid(file_basename(filenames),7,4,/reverse_offset)+'.txt')
	file_move,filenames,newnames,/overwrite
	IF ~keyword_set(noreformat) THEN reformat_wind,newnames
END