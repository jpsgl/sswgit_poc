; $Id: reformat_impact.pro,v 1.3 2012/06/12 14:59:27 cooper Exp $
;
;***INSTRUCTIONS FOR GETTING IMPACT DATA***
;Go to http://aten.igpp.ucla.edu/forms/stereo/level2_plasma_and_magnetic_field.html
;On the upper right menu, select 'Set to ASCII'
;Select A or B, and the third set of parameters (Time, Br, Bt, Bn...)
;Select desired dates-example, to get all of 2008, set start time to Jan 1, 2008, and end time to Jan 1, 2009
;Click on 'Get Data', then 'STEREO data'
;save the page in the format of impactA/BYYYY.txt. ex impactA2008.txt
;then call this procedure on it (can get many files and call this on all at once)
;The reformatted files are saved to the correct directory, and the files downloaded can be erased if desired
;
;
;PRO reformat_impact
;INPUT: files-names of the impact files to be reformatted
;reformat the stereo impact data after getting it,
;Currently, impact data is not available via an ftp server
;but is downloaded in yearly files from http://aten.igpp.ucla.edu/forms/stereo/level2_plasma_and_magnetic_field.html
;IMPORTANT:save downloaded files as impact(A/B)YYYY.txt ex impactA2008.txt
;written by Tom Cooper
;
; $Log: reformat_impact.pro,v $
; Revision 1.3  2012/06/12 14:59:27  cooper
; fixed a typo
;
; Revision 1.2  2012/06/12 14:56:20  cooper
; slightly modified instructions in comements
;
; Revision 1.1  2010/06/29 15:28:06  cooper
; Version 1
;

PRO reformat_impact,files
	FOR j=0,n_elements(files)-1 DO BEGIN
		file=files[j]
		;read in original file
		temp={y:0,m:0,d:0,h:0,min:0,s:0,ms:0,br:0.,bt:0.,bn:0.}
		d=replicate(temp,366L*24*6) ;need to increase for higher res
		OPENR,lun,file,/get_lun
		comment=''
		WHILE comment NE 'DATA:' DO readf,lun,comment
		i=0L
		WHILE eof(lun) NE 1 DO BEGIN
			readf,lun,temp
			d[i]=temp
			i++
		ENDWHILE
		d=d[0:i-1]
		free_lun,lun
		dates=strtrim(d.y,2)+'-'+strtrim(d.m,2)+'-'+strtrim(d.d,2)+'T'+strtrim(d.h,2)+':'$
			+strtrim(d.min,2)+':'+strtrim(d.s,2)+'.'+strtrim(d.ms,2)
		;string(10B) skips to next line, so that the date can be read in by itself as a string
		data=anytim2utc(dates,/ccsds)+string(10B)+strtrim(d.br,1)+string(d.bt)+string(d.bn)
		;write to new file
		savfile=concat_dir(concat_dir(getenv('INSITU_PATH'),'ST_'+strmid(file_basename(file),6,1),/dir),file_basename(file))
		OPENW,lun,savfile,/get_lun
		printf,lun,'TIME BR BT BN'
		printf,lun,data
		free_lun,lun
	ENDFOR
END