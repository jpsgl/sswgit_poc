;returns the (approx) postion angle of the ecliptic
FUNCTION get_ecliptic_pa, date, satellite
	solar_roll=get_stereo_roll(date, satellite, SYSTEM='HEEQ')
	ecl_roll=get_stereo_roll(date,satellite, SYSTEM='HEE')
	delta=ecl_roll-solar_roll
  	sat = parse_stereo_name(satellite, ['A','B'])
	if (sat eq 'A') then return,(90 + delta)
	if (sat eq 'B') then return,(270 + delta)
END