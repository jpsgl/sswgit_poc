; $Id: update_insitu_data.pro,v 1.1 2010/07/07 13:05:29 cooper Exp $
;
;PRO update_insitu_data
;Calls each individual program to download insitu data, currently gets STEREO plastic,omni,ace,wind
;INPUT:none
;KEYWORDS:all-download 'all' data (starting in 2007) for each satelite, instead of only the most recent files
;written by Tom Cooper
;
; $Log: update_insitu_data.pro,v $
; Revision 1.1  2010/07/07 13:05:29  cooper
; Version 1
;

PRO update_insitu_data,all=all
	IF getenv('INSITU_PATH') EQ '' THEN message,'The environment variable INSITU_PATH must be defined as the location to which insitu data files are stored'
	If keyword_set(all) THEN all=1 ELSE all=0
	update_stereo_plastic,all=all
	update_omni,all=all
	update_ace,all=all
	update_wind,all=all
	
END