; $Id: update_ace.pro,v 1.1 2010/06/29 15:28:06 cooper Exp $
;
;PRO update_ace
;NOTE:environment variable INSITU_PATH must be defined
;Download ACE insitu data and store it to path given by INSITU_PATH
;By default, data is reformatted for use by read_is/jmap_in_situ
;OPTIONAL INPUT
;	y:get data from given year to the most recent data
;KEYWORDS
;	all:get all data, set to start from 2007, this could be modified
;	reformat:do not reformat the data
;written by Tom Cooper
;
; $Log: update_ace.pro,v $
; Revision 1.1  2010/06/29 15:28:06  cooper
; Version 1
;

;reformat ace files to be in the style of the line marked by ##
PRO reformat_ace,filenames
	FOR j=0,n_elements(filenames)-1 DO BEGIN
		file=filenames[j]
		temp={y:0,day:0,h:0,m:0,scx:0,scy:0,scz:0,bt:0,bx:0.0,by:0.0,bz:0.0,nv:0.0,$
			q:0.0,dens:0.0,temp:0.0,ratio:0.0,pv:0.0,vx:0.0,vy:0.0,vz:0.0}
		d=replicate(temp,366L*24*15)
		i=0L
		OPENR,lun,file,/get_lun
		WHILE eof(lun) NE 1 DO BEGIN
			readf,lun,temp
			d[i]=temp
			i++
		ENDWHILE
		d=d[0:i-1]
		free_lun,lun
		dates=strtrim(d.y,2)+'-'+strtrim(d.day,2)+'T'+strtrim(d.h,2)+':'+strtrim(d.m,2)
		dates=anytim2utc(dates,/ccsds)
		;string(10B) skips to next line, so that the date can be read in by itself as a string
		data=dates+string(10B)+strtrim(d.dens,1)+string(d.pv)+string(d.temp)+string(d.bx)+string(d.by)+$
			string(d.bz)+string(d.ratio)
		OPENW,lun,file,/get_lun
		printf,lun,'TIME DENS VEL TEMP BX(GSE) BY BZ RATIO(HE/H)' ;##
		printf,lun,data
		free_lun,lun
		print,'ACE file reformatted:' +file_basename(file)
	ENDFOR
END
;update ace data, keyword all gets all data starting from 2007, y=year gets starting starting from
;year until present, keyword reformat puts the data into format used by read_is
PRO update_ace,all=all,y=y,noreformat=noreformat
	IF keyword_set(all) THEN year=2007; can start as early as 1997...
	IF keyword_set(y) THEN year=y
	output_dir=concat_dir(getenv('INSITU_PATH'),'ACE',/dir)
	IF ~file_test(output_dir) THEN file_mkdir,output_dir
	IF datatype(year) EQ 'UND' THEN BEGIN
		count=0
		x=file_search(concat_dir(output_dir,'ace*'),count=count)
		IF count eq 0 THEN year=2007 ELSE BEGIN ;find most recent year
			x=x[reverse(sort(x))]
			f=file_basename(x[0])
			len=strlen(f)
			year=strmid(f,len-8,4) ;-8 for YYYY.txt
		ENDELSE
	ENDIF
	site='http://nssdcftp.gsfc.nasa.gov/spacecraft_data/ace/4_min_merged_mag_plasma/'
	filenames=strarr(200)

	i=0
	WHILE sock_check(site+'ace_m'+strtrim(year,2)+'.dat') DO BEGIN
		file='ace_m'+strtrim(year,2)+'.dat'
		print,'Found ace data for '+strtrim(year,2)
		filenames[i]=sock_find(site,file)
		i+=1
		year=fix(year)+1
		year=strtrim(year,2)
	ENDWHILE
	IF i eq 0 THEN BEGIN print,'Too early to update ACE data' & return & ENDIF
	filenames=filenames[0:i-1]
	FOR j=0,n_elements(filenames)-1 DO BEGIN
		print,'Downloading ace file:'+file_basename(filenames[j])
		sock_copy,filenames[j],out_dir=output_dir
	ENDFOR
	filenames=concat_dir(output_dir,file_basename(filenames))
	newnames=concat_dir(output_dir,'ace'+strmid(file_basename(filenames),7,4,/reverse_offset)+'.txt')
	file_move,filenames,newnames,/overwrite
	IF ~keyword_set(noreformat) THEN reformat_ace,newnames
END