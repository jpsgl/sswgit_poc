; $Id: update_omni.pro,v 1.1 2010/06/29 15:28:06 cooper Exp $
;
;PRO update_omni
;NOTE:environment variable INSITU_PATH must be defined
;Download OMNI insitu data and store it to path given by INSITU_PATH
;By default, data is reformatted for use by read_is/jmap_in_situ
;OPTIONAL INPUT
;	y:get data from given year to the most recent data
;KEYWORDS
;	all:get all data, set to start from 2007, this could be modified
;	noreformat:do not reformat the data
;written by Tom Cooper
;
; $Log: update_omni.pro,v $
; Revision 1.1  2010/06/29 15:28:06  cooper
; Version 1
;


;reformat omni data to be in the style of the line marked by ##
PRO reformat_omni,filenames
	FOR j=0,n_elements(filenames)-1 DO BEGIN
		file=filenames[j]
		temp={y:0,d:0,h:0,m:0,id:0,id2:0,num1:0,num2:0,percent:0,tshift:0,rmst:0,rmsphase:0,timeint:0,$
			bav:0.,bx:0.,bye:0.,bze:.0,bym:0.,bzm:0.,scalar:0.,field_vector:0.,$;e for GSE, m for GSM
			vel:0.,vx:0.,vy:0.,vz:0.,dens:0.,temp:0.,pressure:0.,efield:0.,beta:0}
		d=replicate(temp,366L*24*12) ;increase if higher res
		OPENR,lun,file,/get_lun
		i=0L
		WHILE eof(lun) NE 1 DO BEGIN
			readf,lun,temp
			d[i]=temp
			i++
		ENDWHILE
		d=d[0:i-1]
		free_lun,lun
		dates=strtrim(d.y,2)+'-'+strtrim(d.d,2)+'T'+strtrim(d.h,2)+':'+strtrim(d.m,2)
		dates=anytim2utc(dates,/ccsds) ;convert dates to ccsds, for quick comparisons by read_is
		;string(10B) skips to next line, so that the date can be read in by itself as a string
		data=dates+string(10B)+strtrim(d.dens,1)+string(d.vel)+string(d.temp)+string(d.bx)+string(d.bye)+$
			string(d.bze)+string(d.bym)+string(d.bzm)+string(d.vx)+string(d.vy)+string(d.vz)
		OPENW,lun,file,/get_lun
		printf,lun,'TIME DENS VEL TEMP BX BY(GSE) BZ(GSE) BY(GSM) BZ(GSM) VX VY VZ' ;##
		printf,lun,data
		free_lun,lun
		print,'OMNI file reformatted: ' +file_basename(file)
	ENDFOR
END

;update omni data, keyword all gets all data starting from 2007, y=year gets starting starting from
;year until present, keyword reformat puts the data into format used by read_is
PRO update_omni,all=all,y=y,noreformat=noreformat
	IF keyword_set(all) THEN year=2007 ;can start as early as 1995..
	IF keyword_set(y) THEN year=y
	output_dir=concat_dir(getenv('INSITU_PATH'),'OMNI',/dir)
	IF ~file_test(output_dir) THEN file_mkdir,output_dir
	IF datatype(year) EQ 'UND' THEN BEGIN
		count=0
		x=file_search(concat_dir(output_dir,'omni*'),count=count)
		IF count eq 0 THEN year=2007 ELSE BEGIN
			x=x[reverse(sort(x))] ;if any omni files exist, get the year of the most recent
			f=file_basename(x[0])
			len=strlen(f)
			year=strmid(f,len-8,4) ;year of last data file
		ENDELSE
	ENDIF
	site='http://nssdcftp.gsfc.nasa.gov/spacecraft_data/omni/high_res_omni/'
	filenames=strarr(200)
	
	i=0
	WHILE sock_check(site+'omni_5min'+strtrim(year,2)+'.asc') DO BEGIN
		file='omni_5min'+strtrim(year,2)+'.asc'
		print,'Found OMNI data for '+strtrim(year,2)
		filenames[i]=sock_find(site,file)
		i+=1
		year=fix(year)+1
		year=strtrim(year,2)
	ENDWHILE
	IF i eq 0 THEN BEGIN print,'Too early to update OMNI data' & return & ENDIF
	filenames=filenames[0:i-1]
	
	FOR j=0,n_elements(filenames)-1 DO BEGIN
		print,'Downloading omni file:' +file_basename(filenames[j])
		sock_copy,filenames[j],out_dir=output_dir
	ENDFOR
	;need to get local filenames now...
	filenames=concat_dir(output_dir,file_basename(filenames))
	newnames=concat_dir(output_dir,'omni'+strmid(file_basename(filenames),7,4,/reverse_offset)+'.txt')
	file_move,filenames,newnames,/overwrite
	IF ~keyword_set(noreformat) THEN reformat_omni,newnames
END