; $Id: wopts2.pro,v 1.2 2012/07/19 15:39:04 cooper Exp $

; $Log: wopts2.pro,v $
; Revision 1.2  2012/07/19 15:39:04  cooper
; fixed output, added no cancel
;
; Revision 1.1  2012/06/19 20:24:53  cooper
; first version
;

;Written by Tom Cooper Summer 2012

;pop-up widget for selecting multiple options via radio buttons

;Input:	opts-array of options which will appear as the buttons. If outlist is not set, the returned value
;		is the selected element of  opts
;Optional Inputs:label-label on the widget, used to describe what is being selected
; 		outlist-array with same number of elements as opts. If set, the returned value will be the value
;			of outlist that corresponds to the selected element of opts
;Keywords-Cancel-cancel button is included on widget, if pressed returns -1
;Output:	selected element of opts, or corresponding element of outlist
PRO readwopts2_event,event
	widget_control,event.id,get_uvalue=uval
	widget_control,event.top,get_uvalue=info
	widget_control,event.id,get_value=val
	CASE uval OF
	'cancel':BEGIN (*info).cancel=1
		widget_control,event.top,/destroy
		END
	'done': widget_control,event.top,/destroy
	'opts':(*info).val=val
	ELSE:
	ENDCASE
END

FUNCTION wopts2,opts,label=label,outlist=outlist,nocancel=nocancel
	IF keyword_set(outlist) AND n_elements(outlist) NE n_elements(opts) THEN $
		x=dialog_message('Output list and options list have different sizes',/error)
	base0=widget_base()
	base=widget_base(row=2,/modal,group_leader=base0,/floating,xoffset=10,yoffset=8,units=1,title=title)
	IF keyword_set(label) THEN lab=widget_label(base,value=label)
	options=cw_bgroup(base,opts,frame=2,/nonexclusive,uvalue='opts',/column)
	IF ~keyword_set(nocancel) THEN cancel=WIDGET_BUTTON(base,uvalue='cancel',value='Cancel')
	done=WIDGET_BUTTON(base,uvalue='done',value='Finish')
	len=n_elements(opts)
	info=ptr_new({val:intarr(len),cancel:0})
	widget_control,base,set_uvalue=info
	widget_control,base,/realize
	xmanager,'readwopts2',base
	widget_control,base0,/destroy
	inf=*info
	ptr_free,info
	IF inf.cancel EQ 1 THEN BEGIN out=-1
	ENDIF ELSE BEGIN
		val=fix(inf.val)
		val=where(val EQ 1)
		IF val[0] EQ -1 THEN return,'none'
		IF keyword_set(outlist) THEN out=outlist[val] ELSE out=opts[val]
	ENDELSE
	return,out
END