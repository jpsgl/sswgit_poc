
; $Id: tanh__define.pro,v 1.2 2008/07/15 20:21:32 nathan Exp $
; $Log: tanh__define.pro,v $
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:28  casto
; Beginning of curvefit
;
FUNCTION tanh::br, x
	return, alog(cosh(x))
end

FUNCTION tanh::bv, x
	return, tanh(x)
end

FUNCTION tanh::ba, x
	return, 1/cosh(x)^2
end


PRO tanh__define
	struct = {tanh, INHERITS curvefunc}
end

