
; $Id: curvefunc__define.pro,v 1.6 2008/07/30 15:19:34 casto Exp $
; $Log: curvefunc__define.pro,v $
; Revision 1.6  2008/07/30 15:19:34  casto
; Added type to toString
;
; Revision 1.5  2008/07/17 15:57:56  casto
; Have to make the trim function a method
;
; Revision 1.4  2008/07/17 13:09:18  casto
; Fixed trim namespace bug
;
; Revision 1.3  2008/07/16 18:29:46  casto
; Added to-string method
;
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:27  casto
; Beginning of curvefit
;
FUNCTION curvefunc::init, arr
	self.r0 = arr[0]
	self.vi = arr[1]
	self.vf = arr[2]
	self.tau = arr[3]
	self.t0 = arr[4]
	return, 1
end

FUNCTION curvefunc::cleanup
end

FUNCTION curvefunc::x, t
	return, (t-self.t0)/self.tau
end

FUNCTION curvefunc::r, t
	x = self->x(t)
	return, self.r0 +  0.5*(self.vf+self.vi)*self.tau*x + $
			0.5*(self.vf-self.vi)*self.tau*self->br(x)
end

FUNCTION curvefunc::v, t
	return, 0.5*(self.vf+self.vi+(self.vf-self.vi)*self->bv(self->x(t)))
end

FUNCTION curvefunc::a, t
	return, 0.5*(self.vf-self.vi)/self.tau*self->ba(self->x(t))	
end


FUNCTION curvefunc::dr0, t
	return, 1
end

FUNCTION curvefunc::dvi, t
	x = self->x(t)
	return, 0.5*self.tau*(x - self->br(x))
end

FUNCTION curvefunc::dvf, t
	x = self->x(t)
	return, 0.5*self.tau*(x + self->br(x))
end

FUNCTION curvefunc::dtau, t
	x = self->x(t)
	return, 0.5*(self.vf-self.vi)*(self->br(x) - x*self->bv(x))
end

function curvefunc::trim_num, x, sf
	return, strmid(strtrim(string(x),1),0,sf)
end

FUNCTION curvefunc::toString, type
	return, string(["t0 = " + self->trim_num(self.t0,4) + " hr", $
			"r0 = " + self->trim_num(self.r0,4) + " R_s", $
			"vi = " + self->trim_num(self.vi*232./1.2,5) + " km/s",$
			"vf = " + self->trim_num(self.vf*232./1.2,5) + " km/s",$
			"!4s!3 = " + self->trim_num(self.tau,4) + " hr",type])
end	

PRO curvefunc::printout
	print, 't0 = ', self.t0
	print, 'r0 = ', self.r0
	print, 'vi = ', self.vi*232./1.2
	print, 'vf = ', self.vf*232./1.2
	print, 'tau = ', self.tau
	;print, 'a0 = ', self->a(self.t0)*232./(1.2*3.6)
	;print, 't0 + tau = ', self.t0 + self.tau
end


PRO curvefunc__define
	struct = {curvefunc, del:0.0d, r0:0.0d, vi:0.0d, vf:0.0d, tau:0.0d, t0:0.0d}
end

