
; $Id: poly4__define.pro,v 1.2 2008/07/15 20:21:32 nathan Exp $
; $Log: poly4__define.pro,v $
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:27  casto
; Beginning of curvefit
;
FUNCTION poly4::br, x
	y = x*0.0
	for i=0, n_elements(x)-1 do begin
		a = abs(x[i])
		if (a le 1) then begin
			y[i] = 1./5.*a^5-1./2.*a^4+a^2
		endif else begin
			y[i] = a-3./10.
		endelse
	endfor
	return, y
end

FUNCTION poly4::bv, x
	y = x*0.0
	for i=0, n_elements(x)-1 do begin
		a = x[i]
		if (abs(a) le 1) then begin
			y[i] = a*(abs(a)^3-2*abs(a)^2+2)
		endif else begin
			y[i] = a/abs(a)
		endelse
	endfor
	return, y
end

FUNCTION poly4::ba, x
	y = x*0.0
	for i=0, n_elements(x)-1 do begin
		a = abs(x[i])
		if (abs(a) le 1) then begin
			y[i] = 4*a^3-6*a^2+2
		endif else begin
			y[i] = 0.0
		endelse
	endfor
	return, y
end

PRO poly4__define
	struct = {poly4, INHERITS curvefunc}
end
