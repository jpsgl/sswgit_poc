; $Id: abconvert.pro,v 1.1 2009/11/03 20:34:13 nathan Exp $
;+
; PROJECT:  STEREO
;
; NAME:     abconvert
;
; PURPOSE: Converts angles between Ahead and Behind views
;
; CALLING SEQUENCE: result=abconvert( pa, delta, sat, date [, /degrees])
;
; INPUTS:
;   pa	    (radians)
;   delta   (radians)
;   sat     'A' or 'B'
;   date    input to anytim2utc.pro
;
; OUTPUTS:
;   result = [pa_sat, delta_sat]    (radians)
;
; KEYWORDS:
;   /DEGREES	Input is degrees and print output in degrees (still returns radians)
;-
;
; $Log: abconvert.pro,v $
; Revision 1.1  2009/11/03 20:34:13  nathan
; renamed from convert.pro; added header
;
; Revision 1.7  2008/08/04 22:11:46  sheeley
; fixed degrees bug and added alphap =
;
; Revision 1.6  2008/08/04 16:09:37  casto
; Added alpha-conversion
;
; Revision 1.5  2008/08/04 11:34:12  casto
; Changed formatting of print
;
; Revision 1.4  2008/08/01 14:32:44  casto
; Took out the extra prints
;
; Revision 1.3  2008/08/01 14:25:22  sheeley
; cleaned up B0 fix and renamed g -> phip
;
; Revision 1.2  2008/07/31 22:31:07  sheeley
; corrected the B0 formulas
;
; Revision 1.1  2008/07/29 16:01:40  casto
; Converts between A and B
;

function convert, pa, delta, sat, date, degrees=degrees
	if(keyword_set(degrees)) then begin
		pa *= !pi/180.
		delta *= !pi/180.
	end
	date = anytim2utc(date)
	if sat eq 'A' then satp = 'B' else satp = 'A'
	if (sat eq 'B') then pa = 2*!pi - pa
	;b0 = get_stereo_roll(date, sat, /radians)
	b0 = (get_stereo_lonlat(date, sat, system='HCI',/radians))[2]
	b0p = (get_stereo_lonlat(date, satp, system='HCI',/radians))[2]
	;b0p = get_stereo_roll(date, satp, /radians)
	;print, "b0: ", b0*180./!pi
	;print, "b0p: ", b0p*180./!pi
	theta = acos(cos(b0)*cos(delta)*cos(pa)+sin(b0)*sin(delta))
	phi = atan(sin(pa)*cos(delta), $
		-cos(delta)*cos(pa)*sin(b0) + cos(b0)*sin(delta))
	;print, "phi: ", phi*180./!pi
	carra = (get_stereo_lonlat(date, 'A', system='Carrington'))[1]
	carrb = (get_stereo_lonlat(date, 'B', system='Carrington'))[1]	
	;print, "carra: ", carra*180./!pi
	;print, "carrb: ", carrb*180./!pi
	phip = carra - carrb - phi
	pap = atan(sin(theta)*sin(phip), $
		cos(theta)*cos(b0p) - sin(theta)*cos(phip)*sin(b0p))
	deltap = asin(sin(theta)*cos(phip)*cos(b0p) + cos(theta)*sin(b0p))
	while (pap lt 0) do pap += 2*!pi
	if (satp eq 'B') then pap = 2*!pi - pap
	if(keyword_set(degrees)) then begin
		print, "theta: ", theta*180./!pi
		if sat eq 'A' then L0 = carra - phi else L0 = carrb + phi
		while L0 lt 0 do L0 += 2.*!pi
		print, 'L0: ', L0*180./!pi
		print, "---"
		print, "pap: ", pap*180/!pi
		print, "deltap: ", deltap*180/!pi
	end
	return, [pap, deltap]
end

function convert_alpha, delta, alpha, deltap, sat, date, degrees=degrees
	if(keyword_set(degrees)) then begin
		delta *= !pi/180.
		alpha *= !pi/180.
		deltap *= !pi/180.
	end
	if sat eq 'A' then satp = 'B' else satp = 'A'
	r = (get_stereo_lonlat(date, sat))[0]/(695500.)
	rp = (get_stereo_lonlat(date, satp))[0]/(695500.)
	alphap = atan(cos(deltap), $
		rp/r*cos(alpha-delta)/sin(alpha) - sin(deltap))
	if(keyword_set(degrees)) then print, 'alphap = ',alphap*180/!pi
	return, alphap
end
