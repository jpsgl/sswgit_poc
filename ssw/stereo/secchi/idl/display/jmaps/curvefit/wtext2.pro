; $Id: wtext2.pro,v 1.2 2012/08/02 19:41:40 cooper Exp $

; $Log: wtext2.pro,v $
; Revision 1.2  2012/08/02 19:41:40  cooper
; moved widged
;
; Revision 1.1  2012/06/29 21:01:27  cooper
; added keywords
;

;pop-up widget for entering several different text entries

;Input:	labels-array of labels that describe the entry fields
;Optional Input: title-just a title for the top of the pop-up widget
; 		intext-default text that is already in the text fields
;		toplabel-an array of strings containing overall label for the widget, one label for each element
;		options-an array of string names, whose values go into an exclusive button list
;		optslabel-label for the options...
;Keywords:	nocancel-No cancel button, to be used if a value must be returned
;			first-makes an exclusive button group giving user option to have all returned values equal to the first one
;			align--line up the text boxes--makes it look nicer...
;Output:	structure with 2 fields: TEXT-array of string that was entered into the textbox
;									OPTS-integer corresponding to element of opts that was selected

;Written by Tom Cooper Summer 2012
;Example:	fullname=wtex2t('First Name','Last Name'],intext=['Tom','Cooper'],options=['Undergrad','Grad'],optslabel='Student Type')

PRO readwtext2_event,event
	WIDGET_CONTROL,event.id,get_uvalue=uval
	WIDGET_CONTROL,event.top,get_uvalue=info
	WIDGET_CONTROL,event.id,get_value=val
	inf=*info
	d=0 ;if d=1, destroy widget
	CASE uval OF
	'CANCEL':BEGIN
		inf.text[0]='cancel'
		d=1
		END
	'DONE':d=1
	'FIRST':inf.first=val
	'OPTS':inf.opt=val
	ELSE: BEGIN
		num=fix(strmid(uval,0,/reverse)) ;find the number corresponding to the textbox
		inf.text[num]=val
		ENDELSE
	ENDCASE
	*info=inf
	IF d EQ 1 THEN WIDGET_CONTROL,event.top,/destroy
END

FUNCTION wtext2,labels,intext=intext,title=title,nocancel=nocancel,toplabel=toplabel,options=options,$
		first=first,optslabel=optslabel,align=align
	base0=WIDGET_BASE()
	labelon=keyword_set(toplabel)
	optson=keyword_set(options)
	firston=keyword_set(first)
	base=WIDGET_BASE(column=1,/modal,group_leader=base0,/floating,xoffset=10,yoffset=4,units=1,title=title,/base_align_center)
	IF labelon THEN BEGIN ;makes a label at the top
		n=n_elements(toplabel)
		base1=wIDGET_BASE(base,row=n)
		FOR i=0,n-1 DO lab=WIDGET_LABEL(base1,value=toplabel[i])
	ENDIF
	IF optson THEN BEGIN ;make the button group for options
		base1=WIDGET_BASE(base)
		IF keyword_set(optslabel) THEN lab=optslabel ELSE lab=''
		buttons=CW_BGROUP(base1,options,set_value=0,row=ceil(n_elements(options)/3.),/exclusive,label_left=lab,uvalue='OPTS',frame=2)
	ENDIF
	IF firston THEN BEGIN
		base1=WIDGET_BASE(base)
		buttons=CW_BGROUP(base1,['Yes','No'],set_value=1,row=1,/exclusive,label_left='Use first entry for all',uvalue='FIRST',frame=2)
	ENDIF
	n=n_elements(labels)
	tbase=WIDGET_BASE(base,column=(n-1)/10+1,frame=2) ;want rows of 10
	FOR i=0,n-1 DO BEGIN
		IF i mod 10 EQ 0 THEN base1=WIDGET_BASE(tbase,column=1,base_align_right=align)
		IF n_elements(intext) GT i THEN inval=intext[i] ELSE inval='' 
		text=CW_FIELD(base1,uvalue='TEXT'+strtrim(i,2),value=inval,title=labels[i],/all_events)
	ENDFOR
	base1=WIDGET_BASE(base,column=2)
	done=WIDGET_BUTTON(base1,value='Done',uvalue='DONE')
	IF ~keyword_set(nocancel) THEN cancel=WIDGET_BUTTON(base1,value='Cancel',uvalue='CANCEL')
	text=replicate('',n)
	IF keyword_set(intext) THEN BEGIN
		n=n_elements(intext)
		text[0:n-1]=intext
	ENDIF
	info=ptr_new({text:text,opt:0,first:1})
	WIDGET_CONTROL,base,set_uvalue=info
	WIDGET_CONTROL,base,/realize
	xmanager,'readwtext2',base
	WIDGET_CONTROL,base0,/destroy
	inf=*info
	IF inf.first EQ 0 THEN inf.text[*]=inf.text[0]
	ptr_free,info
	return,inf
END