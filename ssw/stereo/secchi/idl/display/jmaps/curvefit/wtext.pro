; $Id: wtext.pro,v 1.2 2011/07/11 19:53:18 cooper Exp $

; $Log: wtext.pro,v $
; Revision 1.2  2011/07/11 19:53:18  cooper
; Added nocancel and intext
;
; Revision 1.1  2011/06/28 13:56:16  cooper
; First version
;

;pop-up widget for entering text

;Input:	label-label that says what value should be entered
;Optional Input: title-just a title for the top of the pop-up widget
; 		intext-default text that is already in the text field
;Keywords:	nocancel-No cancel button, to be used if a value must be returned
;Output:	string that was entered into the textbox

;Example:	title=wtext('Enter Plot Title',intext='Default Title')

PRO readwtext_event,event
	widget_control,event.id,get_uvalue=uval
	widget_control,event.top,get_uvalue=info
	widget_control,event.id,get_value=val
	CASE uval OF
	'cancel':(*info).val='cancel'
	'text':(*info).val=val
	ELSE:
	ENDCASE
	widget_control,event.top,/destroy
END

FUNCTION wtext,label,title=title,intext=intext,nocancel=nocancel
	base0=widget_base()
	base=widget_base(row=3,/modal,group_leader=base0,/floating,xoffset=10,yoffset=8,units=1,title=title)
	lab=widget_label(base,value=label)
	text=widget_text(base,/editable,uvalue='text',value=intext)
	IF ~keyword_set(nocancel) THEN cancel=widget_button(base,value='Cancel',uvalue='cancel')
	info=ptr_new({val:''})
	widget_control,base,set_uvalue=info
	widget_control,base,/realize
	xmanager,'readwtext',base
	widget_control,base0,/destroy
	val=(*info).val
	ptr_free,info
	return,val
END