
; $Id: atan__define.pro,v 1.2 2008/07/15 20:21:32 nathan Exp $
; $Log: atan__define.pro,v $
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:27  casto
; Beginning of curvefit
;
FUNCTION atan::br, x
	y = x*!pi/2
	return, (2/!pi)^2*(y*atan(y) - alog(y^2+1)/2)
end

FUNCTION atan::bv, x
	y = x*!pi/2
	return, (2/!pi)*atan(y)
end

FUNCTION atan::ba, x
	y = x*!pi/2
	return, 1/(y^2+1)
end

PRO atan__define
	struct = {atan, INHERITS curvefunc}
end
