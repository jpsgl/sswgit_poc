
; $Id: nexp__define.pro,v 1.6 2008/07/30 15:50:15 casto Exp $
; $Log: nexp__define.pro,v $
; Revision 1.6  2008/07/30 15:50:15  casto
; Added type
;
; Revision 1.5  2008/07/17 15:58:05  casto
; Have to make the trim function a method
;
; Revision 1.4  2008/07/17 15:32:44  casto
; Duh, forgot to change to trim_num in nexp
;
; Revision 1.3  2008/07/17 12:39:34  casto
; Overrode methods so that nexp wouldn't display r0
;
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:27  casto
; Beginning of curvefit
;

FUNCTION nexp::dr0, t
	return, 1
end

FUNCTION nexp::dvi, t
	return, self.tau*self->br(self->x(t))
end

FUNCTION nexp::dvf, t
	x = self->x(t)
	return, self.tau*(x-self->br(x))
end

FUNCTION nexp::dtau, t
	x = self->x(t)
	return, (self.vi-self.vf)*(self->br(x) - x*self->bv(x))
end

FUNCTION nexp::r, t
	x = self->x(t)
	return, 1 + self.vf*x*self.tau + $
		(self.vi-self.vf)*self.tau*self->br(x)
end

FUNCTION nexp::v, t
	return, self.vf + (self.vi-self.vf)*self->bv(self->x(t))
end

FUNCTION nexp::a, t
	return, (self.vi-self.vf)/self.tau*self->ba(self->x(t))
end

FUNCTION nexp::br, x
	return, 1 - exp(-x)
end

FUNCTION nexp::bv, x
	return, exp(-x)
end

FUNCTION nexp::ba, x
	return, -exp(-x)
end

PRO nexp::printout
	print, 't0 = ', self.t0
	print, 'vi = ', self.vi*232./1.2
	print, 'vf = ', self.vf*232./1.2
	print, 'tau = ', self.tau
end


FUNCTION nexp::toString, type
	return, string(["t0 = " + self->trim_num(self.t0,4) + " hr", $
			"vi = " + self->trim_num(self.vi*232./1.2,5) + " km/s",$
			"vf = " + self->trim_num(self.vf*232./1.2,5) + " km/s",$
			"!4s!3 = " + self->trim_num(self.tau,4) + " hr",type])
end	

PRO nexp__define
	struct = {nexp, INHERITS curvefunc}
end

