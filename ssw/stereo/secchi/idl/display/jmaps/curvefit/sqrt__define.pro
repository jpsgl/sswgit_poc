
; $Id: sqrt__define.pro,v 1.2 2008/07/15 20:21:32 nathan Exp $
; $Log: sqrt__define.pro,v $
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:28  casto
; Beginning of curvefit
;
FUNCTION sqrt::br, x
	return, sqrt(x^2+1)-1
end

FUNCTION sqrt::bv, x
	return, x/sqrt(x^2+1)
end

FUNCTION sqrt::ba, x
	return, (1+x^2)^(-1.5)
end


PRO sqrt__define
	struct = {sqrt, INHERITS curvefunc}
end

