PRO viewcarr,car_rot=car_rot,phi=phi,theta=theta
	aorb=''
	notdone='y'
	instr=''
	criteria=''
	usecarr=keyword_set(car_rot)
	WHILE(notdone EQ 'y') DO BEGIN
	read, 'Which instrumnet? (cor1/cor2/euvi): ', instr
	read, 'Which satellite? (a/b): ', aorb
	ew=''
	CASE instr OF
		'cor2':BEGIN read, 'East or West limb? (E/W): ',ew
			ew=STRUPCASE(ew)
			read,'What solar radius? (3/5/7/10/13): ', criteria		
			CASE criteria OF
				'3':criteria=117  ;sid# in names Arnaud gave the files
				'5':criteria=113
				'7':criteria=121
				'10':criteria=125
				'13':criteria=129
				ELSE:print,'Not a valid solar radius'
			ENDCASE
			IF(datatype(criteria) NE 'STR') THEN BEGIN
				IF(aorb EQ 'b') THEN criteria++
				criteria='sid'+STRTRIM(STRING(criteria),2)
			ENDIF
			END
		'euvi':read, 'What wavelength? (171,195,284,304): ',criteria
		'cor1':BEGIN read, 'East or West limb? (E/W):  ',ew
			ew=STRUPCASE(ew)
			read,'What solar radius? (1.5/1.8/2.2/2.6/3.0/3.4):  ',criteria
			CASE criteria OF
				'1.5':criteria=221
				'1.8':criteria=223
				'2.2':criteria=219
				'2.6':criteria=225
				'3.0':criteria=227
				'3':criteria=227
				'3.':criteria=227
				'3.4':criteria=229
				ELSE:print,'Not a valid solar radius'
			ENDCASE
			IF(datatype(criteria) NE 'STR') THEN BEGIN
				IF(aorb EQ 'b') THEN criteria++
				criteria='sid'+STRTRIM(STRING(criteria),2)
			ENDIF
			END	
		ELSE: print,'Not a valid image'
	ENDCASE
	IF(~usecarr) THEN read, 'Which Carrington Rotation?: ',car_rot
	scar_rot=STRTRIM(STRING(FIX(car_rot)),2)
	map_img=findfile('/net/earth/secchi/carrmaps/'+aorb+'/'+instr+'/cm/png/*'+criteria+'*cr'+scar_rot+ew+'*')
	IF(map_img[0] EQ '') THEN BEGIN
		print, 'Carrington Map for '+scar_rot+' not found: /net/earth/secchi/carrmaps/'+aorb+'/'+instr+'/cm/png/'
	ENDIF ELSE BEGIN
		map_img=map_img[n_elements(map_img)-1]
		map_img=read_png(map_img)
		WINDOW,2,xsize=880,ysize=500
		tvscl,map_img
	
		IF(datatype(phi) NE 'UND') THEN BEGIN
			x=(phi*180./!pi)*2+79
			y=69-((theta*180./!pi)-180.)*2
			xyouts,x-2,y-1,'(',/device
			xyouts,x+2,y-1,')',/device,color=1
		ENDIF
		;;;Needs to be fixed, I used numbers rather than variables because I didn't know how to find values such as the lower left corner of the map
		print,'Right Click to leave map reading mode'
		cursor,x,y,/device
		lat0='' & lon0=''
		WHILE(!MOUSE.button NE 4) DO BEGIN
			x=(x-79)/2. ;(80,70) is lower left corner of map in pixels with respect to the screen. Divide by two because
			y=ABS((y-69)/2.-180) ;map is (720x360 pixels). -180 to get 0 at north pole, instead of south
			lat='Latitude: '+STRTRIM(STRING(y),2)
			lon='Longitude: '+STRTRIM(STRING(x),2)
			IF(lat NE lat0 OR lon NE lon0) THEN BEGIN
				DEVICE,COPY=[600,450,250,50,500,10]
				xyouts,500,30,lon,/device,color=1,charsize=2
				xyouts,500,10,lat,/device,color=1,charsize=2
				lat0=lat & lon0=lon
			ENDIF
			cursor,x,y,/device
		ENDWHILE
		prnt=''
		read,'Want to print this carrmap? (y/n): ',prnt
		IF strlowcase(prnt) EQ 'y' THEN BEGIN
			temp=tvrd()
			tempname='printable.ps'
			set_plot,'ps'
			device,filename=tempname
			tvscl,temp
			device,/close
			set_plot,'x'
			spawn,'lp -d xeroxphaser '+tempname
			spawn,'rm -f '+tempname
		ENDIF
	ENDELSE
	read,'Want to see another carrmap? (y/n): ', notdone
	ENDWHILE
	print,'*** Viewcarr finished ***'
END