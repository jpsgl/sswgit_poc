;$Id: wcrop.pro,v 1.4 2012/06/15 15:44:39 cooper Exp $
;for cropping images
;Written by Tom Cooper 2011-07
;$Log: wcrop.pro,v $
;Revision 1.4  2012/06/15 15:44:39  cooper
;fixed comments at top
;
;Revision 1.3  2012/06/15 15:42:40  cooper
;removed unused parameters
;Revision 1.2  2011/08/04 14:53:00  cooper
;widget improvements
;
;Revision 1.1  2011/07/29 18:48:46  cooper
;First version
;

;Returns cropped image OR array locations of where to crop if keyword /pixels is set
;USAGE-img=wcrop(img)
PRO drawlines,crops
	loadct,12,/silent
	x0=crops[0] & x1=crops[1] & y0=crops[2] & y1=crops[3]
	PLOTS,[x0,x0],[y0,y1],/device,color=35,thick=2
	PLOTS,[x0,x1],[y1,y1],/device,color=100,thick=2
	PLOTS,[x1,x1],[y1,y0],/device,color=120,thick=2
	PLOTS,[x1,x0],[y0,y0],/device,color=205,thick=2
	loadct,0,/silent
END

PRO readwcrop_event,event
widget_control,event.id,get_uvalue=uval
widget_control,event.top,get_uvalue=info
CASE uval OF
'cancel':BEGIN
	(*info).cancel=1
	widget_control,event.top,/destroy
	END
'easel':BEGIN
	CASE event.press OF
	1:BEGIN	widget_control,(*info).opts,get_value=temp
; 		IF temp LT 3 THEN widget_control,(*info).opts,set_value=temp+1
		IF temp LT 2 THEN (*info).crops[temp]=event.x $
		ELSE (*info).crops[temp]=event.y
		tvscl,(*info).im
		drawlines,(*info).crops
		END
	4:BEGIN widget_control,(*info).opts,get_value=temp
		IF temp EQ 3 THEN temp=0 ELSE temp++
		widget_control,(*info).opts,set_value=temp
		END
	else:
	ENDCASE
	END
'ok':	widget_control,event.top,/destroy
'reset':BEGIN
	crops=[0,1199,0,599] ;back to defaults
	(*info).crops=crops
	tvscl,(*info).im
	drawlines,crops
	END
ELSE:
ENDCASE
END

FUNCTION wcrop,image,pixels=pixels
base0=widget_base()
base=widget_base(row=2,/modal,group_leader=base0,/floating,xoffset=10,yoffset=8,units=1)
easel=widget_draw(base,frame=2,uvalue='easel',/button_events,xsize=1200,ysize=600)
base2=widget_base(base,column=3)
opts=cw_bgroup(base2,['Left','Right','Bottom','Top'],frame=2,/exclusive,uvalue='opts',$
	/column,set_value=0)
base3=widget_base(base2,row=2)
base4=widget_base(base3,column=3)
ok=widget_button(base4,uvalue='ok',value='Finish')
cancel=widget_button(base4,uvalue='cancel',value='Cancel')
reset=widget_button(base4,uvalue='reset',value='Reset')
label=widget_label(base3,value='Right click to go to next side')
widget_control,base,/realize
widget_control,easel,get_value=wid
im=congrid(image,1200,600) ;need to change default crops array if change image size
dims1=float(size(image,/dimensions))-1
dims2=float(size(im,/dimensions))-1
xratio=dims1[0]/dims2[0] ;to get back to original image scaling
yratio=dims1[1]/dims2[1]
wset,wid & tvscl,im
crops=[0,1199,0,599] ;need to change this if change default image size
drawlines,crops
info=ptr_new({im:im,crops:crops,opts:opts,cancel:0})
widget_control,base,set_uvalue=info
xmanager,'readwcrop',base
widget_control,base0,/destroy
crops=(*info).crops
cancel=(*info).cancel
ptr_free,info
IF cancel EQ 1 THEN return,'cancel'
x0=crops[0]*xratio & x1=crops[1]*xratio
y0=crops[2]*yratio & y1=crops[3]*yratio
IF ~keyword_set(pixels) THEN return,image[x0:x1,y0:y1] ;default is to return cropped image
return,[x0,x1,y0,y1]
END