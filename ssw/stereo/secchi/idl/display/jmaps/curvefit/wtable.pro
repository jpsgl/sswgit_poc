;$Id: wtable.pro,v 1.1 2012/06/29 21:01:59 cooper Exp $

;$Log: wtable.pro,v $
;Revision 1.1  2012/06/29 21:01:59  cooper
;first version
;

;pop-up Widget for selecting rows from a table. Can easily be modified to do some other neat things
;Written by Tom Cooper Summer 2012

;Input: entries--array of entries to go into the table
;Optional	labels--labels to put on the columns
;			label--label to put above the table in the widget
;			widths=widths of the columns in the table
;Keywords	rows--if set, just returns the indices of the rows that have been selected
;			columns--if set, just returns the indices of the columns that have been selected

;example: x=wtable(indgen(10,10),/row,toplabel='Look,Numbers!')
PRO readwtable_event,event
	WIDGET_CONTROL,event.id,get_uvalue=uval
	WIDGET_CONTROL,event.top,get_uvalue=info
	WIDGET_CONTROL,event.id,get_value=val
	inf=*info
	d=0 ;if d=1, destroy widget
	CASE uval OF
	'DONE': BEGIN *(inf.locs)=WIDGET_INFO(inf.table,/table_select)
			d=1
		END
	'TABLE':
	ELSE:
	ENDCASE
	*info=inf
	IF d EQ 1 THEN WIDGET_CONTROL,event.top,/destroy
END

FUNCTION wtable,entries,labels=labels,toplabel=toplabel,nocancel=nocancel,widths=widths,rows=rows,columns=columns
	base0=WIDGET_BASE()
	base=WIDGET_BASE(column=1,/modal,group_leader=base0,/floating,xoffset=10,yoffset=8,units=1,/base_align_center)
	IF keyword_set(toplabel) THEN label=toplabel ELSE label='Select Entries'
	label=WIDGET_LABEL(base,value=label)
	dims=size(entries)
	table=WIDGET_TABLE(base,value=entries,x_scroll_size=dims[1],y_scroll_size=10,/scroll,/resizeable_columns,$
		ysize=dims[2],scr_ysize=400,/disjoint_selection,column_labels=labels,column_widths=widths,uvalue='TABLE')
	button=WIDGET_BUTTON(base,value='Done',uvalue='DONE')
	
	WIDGET_CONTROL,base,/realize
	WIDGET_CONTROL,table,set_table_select=[-1,-1]
	info=ptr_new({locs:ptr_new(0),table:table})
	WIDGET_CONTROL,base,set_uvalue=info
	
	xmanager,'readwtable',base
	
	WIDGET_CONTROL,base0,/destroy
	inf=*info
	ptr_free,info
	w=*(inf.locs)
	ptr_free,inf.locs
	IF keyword_set(rows) THEN BEGIN
		w=w[1,*]
		w=w[sort(w)]
		w=w[uniq(w)]
	ENDIF
	IF keyword_set(columns) THEN BEGIN
		w=w[0,*]
		w=w[sort(w)]
		w=w[uniq(w)]
	ENDIF
	return,w
END