; $Id: wgraph.pro,v 1.23 2013/06/19 15:24:38 jasonlee Exp $

; $Log: wgraph.pro,v $
; Revision 1.23  2013/06/19 15:24:38  jasonlee
; fixed carrmap bug
;
; Revision 1.22  2012/08/24 13:10:30  jasonlee
; fixed minor issue with graph formatting
;
; Revision 1.21  2012/08/17 15:03:26  jasonlee
; fixed minor b0 issue in calcdelta method
;
; Revision 1.20  2012/08/10 16:36:20  jasonlee
; calculates delta from ht point
;
; Revision 1.19  2012/08/08 16:04:11  sheeley
; deleted 4 meaningless lines
;
; Revision 1.18  2012/08/06 16:05:08  jasonlee
; set automatic a for d0
;
; Revision 1.17  2012/08/03 16:10:43  jasonlee
; added non-constant acc for fix delta method
;
; Revision 1.16  2012/07/30 14:30:42  jasonlee
; added save ps for fix delta method
;
; Revision 1.15  2012/07/25 21:33:21  jasonlee
; added curvefit for fix delta method
;
; Revision 1.14  2012/07/25 16:48:24  jasonlee
; fixed a few errors, added buttons to widget
;
; Revision 1.13  2012/07/24 21:01:07  jasonlee
; added fix delta method
;
; Revision 1.12  2012/07/16 15:32:21  jasonlee
; fixed small error
;
; Revision 1.11  2012/07/16 14:24:07  cooper
; doesnt display chi^2 unless asked to
;
; Revision 1.10  2012/07/10 20:33:20  cooper
; added some statistics
;
; Revision 1.9  2012/06/15 16:27:29  cooper
; checks for available printers to list
;
; Revision 1.8  2012/05/25 17:48:46  sheeley
; interchanged v and a for output graphs
;
; Revision 1.7  2011/06/30 13:25:09  cooper
; Modified legend on printed graphs
;
; Revision 1.6  2011/06/30 13:16:09  cooper
; Modified legend on printed graphs
;
; Revision 1.5  2011/06/29 14:17:10  cooper
; return ps device to portrain when finished
;
; Revision 1.4  2011/06/28 21:11:08  cooper
; Removed w/omega (angular velocity of observer) from fitting procedure
;
; Revision 1.3  2011/06/28 13:56:51  cooper
; Added comments, included more fit types
;
; Revision 1.2  2011/06/24 21:30:28  cooper
; slight changes to widget
;

;Updated graphing procedure
;Optional Inputs: f-name of ht file to load

;Common variables are: plot#id-window number; alpha-array of elongation angles; pa-position angle; t-array of times; date-starting time
;t1,delta,r1,vf_up-fit parameters from upper fit; trials-array of widget id's for getting trial values; vcon-conversion factor from R_sun/hr to km/s
;a-distance of spacecraft from sun; outs-array of widget_labels to display outputs from curvefit
;r-converted alpha to radial distance ;file-name of ht_file ;vf_full,fit_type,t0,r0,vi,tau-outputs from full curvefit ;cutt-time to cut final plots to
;fitalpha-alpha values obtained from the fit ;carrlon-carrington longitude at first datapoint ;b0-b0 angle ;shortfile-ht_file name without path
;theta,phi,car_rot-cr number and estimated location of start of event

;Compute the upper fit
PRO hifit, time, params,func, pders
COMMON GRAPHSHARED,plot1id,plot2id,alpha,pa,t,date,t1,delta,r1,vf_up,trials,trials2,trials3,vcon,a,outs,r,file,vf_full,fit_type,t0,r0,vi,$
	tau,outs2,cutt,fitalpha,plot3id,plot4id,carrlon,b0,theta,phi,car_rot,shortfile,chisq_up,chisq_down,dochi,dodelta,fixdelta,v0,a0,$
	fr0,d0,toggleupfit,upt,upfit,ftau,acc_type,outs3,chisq_delta,theta_fix,phi_fix,car_rotfix,car_rot_keep,plot_rightfdeltafit,ydelta,$
	r_fix_manualfit,r_fix_curvefit,fixdeltafit,t0_new,r_fit,rsq_modify,v0_curvefit,a0_curvefit,is_soho,calcdelta_flag,cd_theta,cd_phi,$
	percentdiff_fit
delta=params[0]
r1=params[1]
vf_up=params[2]
rho=(r1+vf_up*time)/a
h=1.-2.*rho*sin(delta)+rho^2
func=acos((1.-rho*sin(delta))/sqrt(h))
IF n_params() GE 4 THEN BEGIN
	pders[*,0]=rho*(rho-sin(delta))/h
	pders[*,1]=cos(delta)/(a*h)
	pders[*,2]=time*cos(delta)/(a*h)
ENDIF
END

;fixed delta curvefit method
PRO fixdeltafit2, x, a_fix, f, pder
COMMON GRAPHSHARED
	roft=(fr0+a_fix[0]*x*3600.-0.5*a_fix[1]*x*x*3600*3600)/d0 ;this is only for constant acceleration
	IF acc_type EQ 'exponential' THEN roft=(fr0+a_fix[0]*x*3600.-a_fix[1]*ftau*(ftau*(exp(-x*3600./ftau)-1.)+x*3600.))/d0
	deltanum=(roft)*cos(fixdelta)
	deltadenom=(1.-(roft)*sin(fixdelta))
	f=atan(deltanum/deltadenom)
   if (N_PARAMS() ge 4 AND acc_type EQ 'constant') then begin
     pder[*,0] = (x*3600.)*cos(fixdelta)/(d0*(1.-2.*roft*sin(fixdelta)+roft^2))
     pder[*,1] = -(x*3600.)^2*cos(fixdelta)/(2.*d0*(1.-2.*roft*sin(fixdelta)+roft^2))
   endif
   if (N_PARAMS() ge 4 AND acc_type EQ 'exponential') then begin
     		pder[*,0] = (x*3600.)*cos(fixdelta)/(d0*(1.-2.*roft*sin(fixdelta)+roft^2))
    		pder[*,1] = -ftau*(ftau*(exp(-x*3600./ftau)-1.)+x*3600.)*cos(fixdelta)/(d0*(1.-2.*roft*sin(fixdelta)+roft^2))
   endif
end

PRO fixdeltafit_nexp, x, a_fix, f, pder
COMMON GRAPHSHARED

roft=fr0+a_fix[1]*x*3600.+(a_fix[0]-a_fix[1])*a_fix[2]*(1.-exp(-x/a_fix[2]))
	IF n_params() GE 4 THEN BEGIN
	pders[*,0]=a_fix[2]*(1.-exp(-x/a_fix[2]))
	pders[*,1]=x*3600.-a_fix[2]*(1.-exp(-x/a_fix[2]))
	pders[*,2]=(a_fix[0]-a_fix[1])*(1./a_fix[2])*(exp(-x/a_fix[2]))*a_fix[2]+(a_fix[0]-a_fix[1])*(1.-exp(-x/a_fix[2]))
ENDIF
	
end

;after having used fitting procedure, this is used to get the function [r(t)] now that fit parameters are known
;If ftimes set, only computed fit for times given in ftimes
FUNCTION getr,ftimes=ftimes
COMMON graphshared
IF ~keyword_set(ftimes) THEN ftimes=t
tt=(ftimes-t0)/tau
IF fit_type EQ 'nexp' THEN rtemp=1+vf_full*tt*tau+(vi-vf_full)*tau*(1-exp(-tt)) $
ELSE BEGIN fit_params,tt,br,bv,ba ;all fits besides nexp of the same form (see fit_params)
	rtemp=r0+0.5*(vf_full+vi)*tau*tt+0.5*(vf_full-vi)*tau*br
ENDELSE
return,rtemp
END

;after having used fitting procedure, this uses fit parameters to get velocities
FUNCTION getrfix_new,ftimes=ftimes
COMMON graphshared
IF ~keyword_set(ftimes) THEN ftimes=t
tt=(ftimes-t0_new)/ftau
CASE acc_type OF 
	'constant':BEGIN
		
		rtemp_fix=fr0+v0*tt*ftau*3600.-0.5*a0*tt*ftau*tt*ftau*3600.*3600.
		IF plot_rightfdeltafit EQ 2 THEN rtemp_fix=fr0+v0_curvefit*tt*ftau*3600.-0.5*a0_curvefit*tt*ftau*tt*ftau*3600.*3600.
	END
	'exponential':BEGIN
		rtemp_fix=fr0+v0*tt*ftau*3600.-a0*ftau*(ftau*(exp(-tt*3600.)-1.)+tt*ftau*3600.)
		IF plot_rightfdeltafit EQ 2 THEN rtemp_fix=fr0+v0_curvefit*tt*ftau*3600.-a0_curvefit*ftau*(ftau*(exp(-tt*3600.)-1.)+tt*ftau*3600.)
	END
ENDCASE
return,rtemp_fix
END

;after having used fitting procedure, this uses fit parameters to get velocities
FUNCTION getv,ftimes=ftimes
COMMON graphshared
IF ~keyword_set(ftimes) THEN ftimes=t
tt=(ftimes-t0)/tau
IF fit_type EQ 'nexp' THEN vtemp=vf_full+(vi-vf_full)*exp(-tt) $
ELSE BEGIN fit_params,tt,br,bv,ba ;all fits besides nexp of the same form
	vtemp=0.5*(vf_full+vi+(vf_full-vi)*bv)
ENDELSE
return,vtemp
END

;after having used fitting procedure, this uses fit parameters to get velocities
FUNCTION getvfix,ftimes=ftimes
COMMON graphshared
IF ~keyword_set(ftimes) THEN ftimes=t
tt=(ftimes-t0_new)/ftau
CASE acc_type OF 
	'constant':BEGIN
		vtemp_fix=v0-a0*tt*ftau*3600.
		IF plot_rightfdeltafit EQ 2 THEN vtemp_fix=v0_curvefit-a0_curvefit*tt*ftau*3600.
	END
	'exponential':BEGIN
		vtemp_fix=-a0*ftau*(1.-exp(-tt*3600.))+v0
		IF plot_rightfdeltafit EQ 2 THEN vtemp_fix=-a0_curvefit*ftau*(1.-exp(-tt*3600.))+v0_curvefit
	END
ENDCASE
return,vtemp_fix
END

;after having used fitting procedure, this uses fit parameters to get accel
FUNCTION geta,ftimes=ftimes
COMMON graphshared
IF ~keyword_set(ftimes) THEN ftimes=t
tt=(ftimes-t0)/tau
IF fit_type EQ 'nexp' THEN atemp=double((vi-vf_full)/tau*(-exp(-tt))) $
ELSE BEGIN fit_params,tt,br,bv,ba   ;all fits besides nexp of the same form
	atemp=0.5*(vf_full-vi)/tau*ba
ENDELSE
return,atemp
END

;after having used fitting procedure, this uses fit parameters to get accel
FUNCTION getafix,ftimes=ftimes
COMMON graphshared
IF ~keyword_set(ftimes) THEN ftimes=t
tt=(ftimes-t0_new)/ftau
CASE acc_type OF
	'constant':BEGIN
		atemp_fix=-a0+0.*t
		IF plot_rightfdeltafit EQ 2 THEN atemp_fix=-a0_curvefit+0.*t
	END
	'exponential':BEGIN
		atemp_fix=-a0*exp(-tt*3600.)
		IF plot_rightfdeltafit EQ 2 THEN atemp_fix=-a0_curvefit*exp(-tt*3600.)
	END
ENDCASE
return,atemp_fix
END

;this is where all the fit parameters for curvefit are defined
PRO fit,time,params,func,pders
COMMON graphshared
r0=params[0] ;get the input values
vi=params[1]
vf_full=params[2]
tau=params[3]
t0=params[4]
tt=(time-t0)/tau
IF fit_type EQ 'nexp' THEN BEGIN
	func=1+vf_full*tt*tau+(vi-vf_full)*tau*(1-exp(-tt))
	IF n_params() GE 4 THEN BEGIN
	pders[*,0]=1 ;0 is df/dr0
	pders[*,1]=tau*(1-exp(-tt));1 is df/dvi
	pders[*,2]=tau*(tt+exp(-tt)-1);2 is df/dvf
	pders[*,3]=(vi-vf_full)*(1-(1+tt)*exp(-tt)) ;3 is df/dtau
	pders[*,4]=-(vf_full+(vi-vf_full)*exp(-tt)) ;4 is df/dt0
	ENDIF
ENDIF ELSE BEGIN
	fit_params,tt,br,bv,ba ;all but nexp of this form
	func=r0+0.5*(vf_full+vi)*tau*tt+0.5*(vf_full-vi)*tau*br
	IF n_params() GE 4 THEN BEGIN
	pders[*,0]=1 ;df/dr0
	pders[*,1]=0.5*tau*(tt-br) ;df/dvi
	pders[*,2]=0.5*tau*(tt+br) ;df/dvf
	pders[*,3]=0.5*(vf_full-vi)*(br-tt*bv) ;df/dtau
	pders[*,4]=-0.5*(vf_full+vi+(vf_full-vi)*bv) ;df/dt0 (-df/dt)
	ENDIF
ENDELSE
END

;compute the component of the fit that differs between fits-for all fit types
;except nexp (fit types are v1*r+v2*f(r), where f(r) is what varies)
;returns br,bv,ba
;To add a new fit_type (if it is of the same type as these), just put br,bv,ba in the list
;and include the name of the fit in the combobox widget by adding to the fits array
PRO fit_params,tt,br,bv,ba
Common graphshared
CASE fit_type OF
'sqrt':BEGIN br=sqrt(tt^2+1)-1 ;the only component of r(t) that differs
	bv=tt/sqrt(tt^2+1) ;derivative of changing component
	ba=(tt^2+1)^(-1.5) ;second derivative of changing component (not used in fit)
	END
'tanh':BEGIN br=alog(cosh(tt))
	bv=tanh(tt)
	ba=1/cosh(tt)^2
	END
'atan':	BEGIN y=tt*!pi/2
	br=(2/!pi)^2*(y*atan(y)-alog(y^2+1)/2)
	bv=(2/!pi)*atan(y)
	ba=1/(y^2+1)
	END
'poly4':BEGIN br=tt*0.0 & bv=tt*0.0 & ba=tt*0.0
	FOR i=0,n_elements(tt)-1 DO BEGIN
	tmp=tt[i] ;poly4 and poly5 are piecewise fits, with no accel for abs([t-t0]/tau)>1
	IF abs(tmp) LE 1 THEN BEGIN
		br[i]=(1./5.)*abs(tmp)^5-(1./2.)*abs(tmp)^4+abs(tmp)^2
		bv[i]=tmp*(abs(tmp)^3-2.*abs(tmp)^2+2)
		ba[i]=4.*abs(tmp)^3-6.*abs(tmp)^2+2
	ENDIF ELSE BEGIN
		br[i]=abs(tmp)-(3./10.)
		bv[i]=tmp/abs(tmp)
		ba[i]=0.0
	ENDELSE
	ENDFOR
	END
'poly5':BEGIN br=tt*0.0 & bv=tt*0.0 & ba=tt*0.0
	FOR i=0,n_elements(tt)-1 DO BEGIN
	tmp=tt[i]
	IF abs(tmp) LE 1 THEN BEGIN
	br[i]=(1./16)*tmp^2*(tmp^4-5*tmp^2+15)
	bv[i]=(1./8)*tmp*(3*tmp^4-10*tmp^2+15)
	ba[i]=(15./8)*(tmp^2-1)^2
	ENDIF ELSE BEGIN
	br[i]=abs(tmp)-(5./16)
	bv[i]=tmp/abs(tmp)
	ba[i]=0.0
	ENDELSE
	ENDFOR
	END
ENDCASE
END

;load ht file, and set some variables (a,b0, etc..)
PRO loadht ;loading in a file
COMMON GRAPHSHARED
IF file_test(file) THEN BEGIN
	toggleupfit=0
	read_ht_file,file,t,alpha,pa,date
	temp=strsplit(file,'/') ;extract just file name (must be something that does this..)
	temp2=n_elements(temp)-1
	shortfile=strmid(file,temp[temp2])
	IF pa LT 180 THEN sat='A' ELSE sat='B'
	date=anytim2utc(date)
	IF is_soho NE 1 THEN BEGIN
		a=(get_stereo_lonlat(date,sat))[0]/695500.
		b0=(get_stereo_lonlat(date,sat,system='HCI'))[2]
	ENDIF
	IF is_soho EQ 1 THEN BEGIN
		a=212.84327
		b0=0. ;is this correct?
	ENDIF
	carrlon=(get_stereo_lonlat(date,sat,system='Carr'))[1]
	IF carrlon LT 0 THEN carrlon+=2*!pi ;add 2pi if needed
	car_rot=get_stereo_carr_rot(date,sat)
	car_rotfix=get_stereo_carr_rot(date,sat)
	car_rot_keep=car_rotfix
	t=(t-t[0])/3600
	alpha=alpha*!pi/180.
	wset,plot1id
	plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
	;!4a makes an alpha, !3 is normal font
ENDIF
END

PRO main_event,event ;This is the main program!
COMMON graphshared

IF datatype(event) EQ 'UND' THEN return
WIDGET_CONTROL,event.id,get_uvalue=ev
IF ev EQ '' THEN return
;This is where we go when we press each button
;should be in 'chronological' orders
CASE ev OF
'calcdelta': BEGIN
	IF(file_test(concat_dir(getenv('HOME'),'syncjmapwithmovie.dat')) GT 0) THEN BEGIN
		saved_click=[0.0,0.0,'test']
		OPENR,lun,concat_dir(getenv('HOME'),'syncjmapwithmovie.dat'),/get_lun
		READF,lun,saved_click
		free_lun,lun
		got_t=saved_click[2]*!pi/180. ;Carrington latitude in radians
		got_n=saved_click[1]*!pi/180. ;Carrington longitude in radians
	ENDIF ELSE print,'%% Point data does not exist'
	IF pa LT 180 THEN sat='A' ELSE sat='B'
	calcdelta_flag=1

	cd_theta=!pi/2.-got_t
	cd_phi=((get_stereo_lonlat(date,sat,system='CAR'))[1])-got_n
	cd_b0=((get_stereo_lonlat(date,sat,system='CAR'))[2])*!pi/180.
	new_delta=asin((sin(cd_theta)*cos(cd_phi)*cos(cd_b0) + cos(cd_theta)*sin(cd_b0)))*180./!pi ;in degrees
	disp_not=['Delta is: ',strmid(strtrim(String(new_delta),2),0,5)+String("260B)]
	cd_l=dialog_message(disp_not,/center,/information)
	cd_phi=got_n
	END
'load': BEGIN ;load in a file
	file=dialog_pickfile(filter='*.ht',path='/sjmaps11/ht_data',title='Select H-T data file',/must_exist)
	IF datatype(file) EQ 'UND' THEN return
	window,plot3id,/pixmap & wdelete ;make window thats off screen (so as not too mess up widget
	window,plot4id,/pixmap & wdelete ;when it is deleted, then delete it. (to remove windows with old plots)
	temp=['Delta :','Vf    :','r1    :','Theta :','Phi   :','CR    :']
	FOR i=0,5 DO widget_control,outs[i],set_value=temp[i] ;reset labels to be blank
	temp=['t0    :','Tau   :','r0    :','Vi    :','Vf    :']
	FOR i=0,4 DO widget_control,outs2[i],set_value=temp[i]
	wset,plot2id & erase ;clear second plot
	loadht
	END
'fixdeltafit_widget':BEGIN ;curvefit of fixed delta fit method
	window,plot3id,/pixmap & wdelete
	window,plot4id,/pixmap & wdelete
	plot_rightfdeltafit = 2
	wset,plot1id & erase ;clear both windows, then plot again
	wset,plot2id & erase ;clear both windows, then plot again
	widget_control,trials3[1],get_value=fixdelta & fixdelta=double(fixdelta[0])*!pi/180. ;radians
	widget_control,trials3[2],get_value=v0 & v0=double(v0[0]) ;km/s units
	widget_control,trials3[3],get_value=a0 & a0=double(a0[0])/1000. ;m/s^2 to km/s^2 units
	widget_control,trials3[4],get_value=fr0 & fr0=double(fr0[0])*(0.696*1000000.) ;SR to km units
	widget_control,trials3[5],get_value=ftau & ftau=double(ftau[0])*3600. ;time dilation constant, hours to seconds
	d0=a*695500.
	range_delta=where(t GE 0.)
	t_curvefit=t[range_delta]
	ydelta_curvefit=alpha[range_delta]
	v0_temp=v0
	a0_temp=a0
	vf=v0-a0*ftau
	ftau_temp=ftau
	params2=[v0_temp,a0_temp] ;call the fit procedure
	params_nexp=[v0_temp,vf,ftau_temp]
	params_exponential=[v0_temp,a0_temp]
	keepconst=[0,0,1,0,1]

	fixdeltafit=CURVEFIT(t_curvefit,ydelta_curvefit, 1.+0.*t_curvefit,params2,fita=[1,1],function_name='fixdeltafit2',/double,chisq=chisq_delta)
	IF acc_type EQ 'nexp' THEN BEGIN
		fixdeltafit=CURVEFIT(t_curvefit,ydelta_curvefit, 1.+0.*t_curvefit,params2,fita=[1,1,0],function_name='fixdeltafit_nexp',/double,chisq=chisq_delta)
	ENDIF
	tempy_rsq=TOTAL(ydelta_curvefit)/n_elements(ydelta_curvefit)
	rsq_modify=1.-(TOTAL(ydelta_curvefit-fixdeltafit)^2)/(TOTAL(ydelta_curvefit-tempy_rsq)^2) 
	r_fit=a*sin(alpha)/cos(alpha-fixdelta);this is conversion from elongation to distance
	print,'v0 = ',params2[0]
	v0_curvefit=params2[0]
	print,'a0 = ',params2[1]
	a0_curvefit=params2[1]
	percentdiff_fit=total(abs(fixdeltafit-ydelta_curvefit)/ydelta_curvefit)*100./double(n_elements(fixdeltafit))
	
	widget_control,outs3[1],set_value='AVG%DIF:'+strmid(strtrim(percentdiff_fit,2),0,5)
	r_fix_curvefit=d0*sin(fixdeltafit)/(cos(fixdeltafit-fixdelta)*696000.);this is conversion from elongation to distance
	par_fix=pa*!pi/180. ;position angle in radians
	theta_fix=acos(cos(b0)*cos(fixdelta)*cos(par_fix)+sin(b0)*sin(fixdelta)) ;all this business copied from jgraph
	phi0_fix=atan(sin(par_fix)*cos(fixdelta),cos(b0)*sin(fixdelta)-cos(fixdelta)*cos(par_fix)*sin(b0))
	phi_fix=carrlon-phi0_fix ;get theta,phi,cr#
	car_rotfix=car_rot_keep
	WHILE phi_fix lt 0 DO BEGIN
		phi_fix+=2*!pi
		car_rotfix+=1
	ENDWHILE
	car_rotfix-=fix(phi_fix/(2*!pi))
	phi_fix=phi_fix MOD (2*!pi)

	wset,plot1id
	plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
	oplot,t_curvefit,fixdeltafit*180./!pi, linestyle=2 
	wset,plot2id
	plot,t,r_fit,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='r/R_sun'
	oplot,t,r_fix_curvefit,linestyle=2 

	END
'upfit':BEGIN ;compute upper fit
	;get values out of the widget
	toggleupfit=1
	dodelta=0
	t0_new=1.
	temp=['t0    :','Tau   :','r0    :','Vi    :','Vf    :']
	FOR i=0,4 DO widget_control,outs2[i],set_value=temp[i]
	window,plot3id,/pixmap & wdelete
	window,plot4id,/pixmap & wdelete
	wset,plot2id & erase
	wset,plot1id & erase ;clear both windows, then plot again
	plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
	widget_control,trials[0],get_value=t1 & t1=double(t1[0]) ;get trial values
	widget_control,trials[1],get_value=delta & delta=double(delta[0])*!pi/180.
	widget_control,trials[2],get_value=vf_up & vf_up=double(vf_up[0])/vcon
	widget_control,trials[3],get_value=r1 & r1=double(r1[0])

	uprange=where(t GE t1)
	IF uprange[0] EQ -1 THEN return
	upt=t[uprange]
	upalpha=alpha[uprange]
	params=double([delta,r1,vf_up]) ;call the fit procedure
	upfit=CURVEFIT(upt,upalpha,1.+0.*upt,params,function_name='hifit',/double,chisq=chisq_up)
	corr_up=correlate(upfit,upalpha,/double)
	delta=params[0]
	r1=params[1]
	vf_up=params[2] ;set output values
	widget_control,outs[0],set_value='Delta : '+strmid(strtrim(delta*180./!pi,2),0,5)
	widget_control,outs[2],set_value='r1    : '+strmid(strtrim(r1,2),0,5)
	widget_control,outs[1],set_value='Vf    : '+strmid(strtrim(vf_up*vcon,2),0,6)
	widget_control,outs[6],set_value='CHISQ : '+strmid(strtrim(chisq_up,2),0,6)
	wset,plot1id
	oplot,upt,upfit*180./!pi 
	
	wset,plot2id
	r=a*sin(alpha)/cos(alpha-delta) ;this is conversion from elongation to distance
	plot,t,r,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='r/R_sun'
	oplot,t,r1+vf_up*t
	par=pa*!pi/180. ;position angle in radians
	theta=acos(cos(b0)*cos(delta)*cos(par)+sin(b0)*sin(delta)) ;all this business copied from jgraph
	phi0=atan(sin(par)*cos(delta),cos(b0)*sin(delta)-cos(delta)*cos(par)*sin(b0))
	phi=carrlon-phi0 ;get theta,phi,cr#
	WHILE phi lt 0 DO BEGIN
		phi+=2*!pi
		car_rot+=1
	ENDWHILE
	car_rot-=fix(phi/(2*!pi))
	phi=phi MOD (2*!pi)
	widget_control,outs[3],set_value='Theta : '+strmid(strtrim(theta*180/!pi,2),0,5)
	widget_control,outs[4],set_value='Phi   : '+strmid(strtrim(phi*180/!pi,2),0,5)
	widget_control,outs[5],set_value='CR    : '+strmid(strtrim(car_rot,2),0,5)
	END
'type':	BEGIN 
		acc_type=strlowcase(widget_info(trials3[0],/combobox_gettext))
		fit_type=strlowcase(widget_info(trials2[0],/combobox_gettext))
		;entry boxes grayed out for linear fit
		IF fit_type NE 'linear' AND fit_type NE 'select fit type' THEN $
			FOR i=1,4 DO widget_control,trials2[i],/sensitive $
		ELSE FOR i=1,4 DO widget_control,trials2[i],sensitive=0
	END
'do_fit':BEGIN ;computes the full fit based on the function given
	window,plot3id,/pixmap & wdelete
	window,plot4id,/pixmap & wdelete
	toggleupfit=2
	IF fit_type NE 'linear' AND fit_type NE 'select fit type' THEN BEGIN
		widget_control,trials2[1],get_value=t0 & t0=double(t0[0]) ;get trial values
		widget_control,trials2[2],get_value=tau & tau=double(tau[0])
		widget_control,trials2[3],get_value=r0 & r0=double(r0[0])
		widget_control,trials2[4],get_value=vi & vi=double(vi[0])/vcon
		
		vf_full=vf_up
		params=[r0,vi,vf_full,tau,t0]
		fit=CURVEFIT(t,r,1.+0*t,params,FUNCTION_NAME='fit',/double,chisq=chisq_down)
		r0=params[0] & vi=params[1] & vf_full=params[2]
		tau=params[3] & t0=params[4] ;set output values
		widget_control,outs2[0],set_value='t0    : '+strmid(strtrim(t0,2),0,5)
		widget_control,outs2[1],set_value='Tau   : '+strmid(strtrim(tau,2),0,5)
		widget_control,outs2[2],set_value='r0    : '+strmid(strtrim(r0,2),0,5)
		widget_control,outs2[3],set_value='Vi    : '+strmid(strtrim(vi*vcon,2),0,5)
		widget_control,outs2[4],set_value='Vf    : '+strmid(strtrim(vf_full*vcon,2),0,5)
		widget_control,outs2[5],set_value='CHISQ : '+strmid(strtrim(chisq_down,2),0,6)
		rfit=getr()
	ENDIF
	IF fit_type EQ 'linear' THEN BEGIN ;linear fits are done differently
		lowrange=where(t LE t1) ;do linear fit on the part not fit above
		IF lowrange[0] EQ -1 THEN return
		fit=LINFIT(t[lowrange],r[lowrange],/double)
		r0=fit[0]
		vf_full=fit[1]
		rfit=r0+vf_full*t
		widget_control,outs2[0],set_value='V     :'+strmid(strtrim(vf_full*vcon,2),0,6)
		widget_control,outs2[1],set_value='Y-int :'+strmid(strtrim(r0,2),0,5)
		FOR i=2,4 DO widget_control,outs2[i],set_value=''
	ENDIF

	rho=rfit/a
	wset,plot1id
	plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
	fitalpha=acos((1.-rho*sin(delta))/sqrt(1.-2.*rho*sin(delta)+rho^2))
	oplot,t,fitalpha*180./!pi
	wset,plot2id
	plot,t,r,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='r/R_sun'
	oplot,t,r1+vf_up*t
	oplot,t,rfit
	END

'plot_fdelta':BEGIN ;plots the fix delta graph on plot1id
	;this method attempts to find the acceleration of an event assuming that delta is known. 
	;delta can be estimated by looking at EIT or EUVI images, or by trial and error.
	;since t is in hours, and we use km and s, we convert t to seconds with t*3600.
	t0_new=0.
	widget_control,trials3[1],get_value=fixdelta & fixdelta=double(fixdelta[0])*!pi/180. ;radians
	widget_control,trials3[2],get_value=v0 & v0=double(v0[0]) ;km/s units
	widget_control,trials3[3],get_value=a0 & a0=double(a0[0])/1000. ;m/s^2 to km/s^2 units
	widget_control,trials3[4],get_value=fr0 & fr0=double(fr0[0])*(0.696*1000000.) ;SR to km units
	widget_control,trials3[5],get_value=ftau & ftau=double(ftau[0])*3600. ;time dilation constant, hours to seconds
	d0=a*695500
	suggested_r0=d0*214.94*tan(alpha[0])/149598000.

	widget_control,outs3[0],set_value='rec r0: '+strmid(strtrim(suggested_r0,2),0,5)
	setnum=0
	CASE acc_type OF ;needs more acceleration cases
		'constant': BEGIN 
			roft=fr0+v0*t*3600.-0.5*a0*t*t*3600*3600 ;a = constant
			setnum=1
		END
		'exponential': BEGIN
			roft=fr0+v0*t*3600.-a0*ftau*(ftau*(exp(-t*3600./ftau)-1.)+t*3600.)
			setnum=2
		END
	ENDCASE

	par_fix=pa*!pi/180. ;position angle in radians
	theta_fix=acos(cos(b0)*cos(fixdelta)*cos(par_fix)+sin(b0)*sin(fixdelta)) ;all this business copied from jgraph
	phi0_fix=atan(sin(par_fix)*cos(fixdelta),cos(b0)*sin(fixdelta)-cos(fixdelta)*cos(par_fix)*sin(b0))
	phi_fix=carrlon-phi0_fix ;get theta,phi,cr#
	car_rotfix = car_rot_keep
	WHILE phi_fix lt 0 DO BEGIN
		phi_fix+=2*!pi
		car_rotfix+=1
	ENDWHILE
	car_rotfix-=fix(phi_fix/(2*!pi))
	phi_fix=phi_fix MOD (2*!pi)

	plot_rightfdeltafit = 1
	deltanum=(roft/d0)*cos(fixdelta)
	deltadenom=(1.-(roft/d0)*sin(fixdelta))
	ydelta=atan(deltanum/deltadenom)
	r_fix_manualfit=a*sin(ydelta)/(cos(ydelta-fixdelta));this is conversion from elongation to distance
	ydelta *=(180./!pi)
	fix_t0_1 = 0.
	fix_t0_2 = 0.
	r_fit=a*sin(alpha)/(cos(alpha-fixdelta)) ;this is conversion from elongation to distance
	CASE setnum OF
		1: BEGIN
			IF a0 EQ 0. THEN BEGIN
				fix_t0_1=(1.-d0*sin(alpha[0])/cos(alpha[0]-fixdelta))/v0
			ENDIF
			IF a0 NE 0. AND a0 NE 0 THEN BEGIN
				fix_quada=0.5*a0
				fix_quadb=v0
				fix_quadc=d0*sin(alpha[0])/cos(alpha[0]-fixdelta)-1.
				fix_t0_1=(-fix_quadb+sqrt(fix_quadb*fix_quadb-4*fix_quada*fix_quadc))/(2.*fix_quada)
				fix_t0_2=(-fix_quadb-sqrt(fix_quadb*fix_quadb-4*fix_quada*fix_quadc))/(2.*fix_quada)
			ENDIF
			IF fix_t0_1 LT 0. THEN print,'possible t0 = ',strmid(strtrim(fix_t0_1/3600.,2),0,6),' hours'
			IF fix_t0_2 LT 0. AND fix_t0_2 NE fix_t0_1 THEN print,'possible t0 = ',strmid(strtrim(fix_t0_2/3600.,2),0,6),' hours'
		END
		2: BEGIN
		END
	ENDCASE
	percentdiff_manual=total(abs(ydelta-(alpha*180./!pi))/(alpha*180./!pi))*100./double(n_elements(ydelta))
	widget_control,outs3[1],set_value='AVG%DIF:'+strmid(strtrim(percentdiff_manual,2),0,5)

	IF toggleupfit EQ 0 THEN BEGIN
		wset,plot2id & erase
		wset,plot1id & erase ;clear both windows, then plot again
		wset,plot1id
		plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
		IF dodelta EQ 1 THEN oplot,t,ydelta,linestyle=2
		wset,plot2id
		r=a*sin(alpha)/cos(alpha-fixdelta) ;this is conversion from elongation to distance
		plot,t,r,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='r/R_sun'
		IF dodelta EQ 1 THEN oplot,t,r_fix_manualfit,linestyle=2
	ENDIF
	IF toggleupfit EQ 1 THEN BEGIN
		wset,plot2id & erase
		wset,plot1id & erase ;clear both windows, then plot again
		wset,plot1id
		plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
		oplot,upt,upfit*180./!pi
		IF dodelta EQ 1 THEN oplot,t,ydelta,linestyle=1
		wset,plot2id
		r=a*sin(alpha)/cos(alpha-fixdelta) ;this is conversion from elongation to distance
		plot,t,r,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='r/R_sun'
		oplot,t,r_fix_manualfit,linestyle=2
	ENDIF
	IF toggleupfit EQ 2 THEN BEGIN
		wset,plot2id & erase
		wset,plot1id & erase ;clear both windows, then plot again
		rfit=getr()
		rho=rfit/d0
		wset,plot1id
		plot,t,alpha*180./!pi,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)',title=shortfile
		fitalpha=acos((1.-rho*sin(delta))/sqrt(1.-2.*rho*sin(delta)+rho^2))
		oplot,t,fitalpha*180./!pi
		IF dodelta EQ 1 THEN oplot,t,ydelta,linestyle=2
		r=a*sin(alpha)/cos(alpha-fixdelta) ;this is conversion from elongation to distance
		wset,plot2id
		plot,t,r,psym=2,charsize=2.5,xtitle='Time (hrs)',ytitle='r/R_sun'
		oplot,t,r_fix_manualfit,linestyle=1
	ENDIF
END

'cuttime':BEGIN ;this plots the window with v/a vs t/r
		IF fit_type EQ 'linear' THEN return
		window,plot3id,/pixmap & wdelete
		window,plot4id,/pixmap & wdelete
		widget_control,event.id,get_value=cutt
		cutt=double(cutt[0])
		cutt=findgen(300)*cutt/300.
		cutr=getr(ftime=cutt);get r,v,a to plot
		cutv=getv(ftime=cutt)*vcon
		cuta=geta(ftime=cutt)*vcon/3.6 ;need to adjust for accel in m/s^2
		window,plot3id,xsize=900,ysize=600
		!p.multi=[0,2,2,0,0] ;for plotting 2x2 on 1 window
		plot,cutt,cutv,charsize=1.5,xtitle='Time (hrs)',ytitle='v (km/s)'
		plot,cutt,cuta,charsize=1.5,xtitle='Time (hrs)',ytitle='a (m/s^2)'
		plot,cutr,cutv,charsize=1.5,xtitle='r/R_sun',ytitle='v (km/s)'
		plot,cutr,cuta,charsize=1.5,xtitle='r/R_sun',ytitle='a (m/s^2)'
		!p.multi=[0,1,1,0,0] ;need this to be set back to 1 plot per window
	  END
'ok':	BEGIN ;pop-up fourth window, which both plots of points and fit curves. Also, make idl.ps file
	window,plot4id,xsize=900,ysize=600 ;legends for the fits
	IF dodelta EQ 1 THEN BEGIN
		cutr_fix = getrfix_new(ftime=0.)/696000.
		cutv_fix=getvfix(ftime=0.)
		cuta_fix=getafix(ftime=0.)
		vf=cutv_fix(n_elements(cutv_fix)-1)
		window,plot4id,xsize=900,ysize=600 ;legends for the fits
		theta_temporary=theta_fix
		phi_temporary=phi_fix
		IF datatype(calcdelta_flag) NE 'UND' THEN BEGIN
			IF calcdelta_flag EQ 1 THEN BEGIN
				theta_temporary=cd_theta
				phi_temporary=cd_phi
			ENDIF
		ENDIF
		arr=[	'!4d!3'+'0 = '+strmid(strtrim(fixdelta*180./!pi,2),0,4)+String("260B), $
			'pa = ' + strmid(strtrim(pa,2),0,4)+String("260B), $
			'!4h!3 = '+strmid(strtrim(theta_temporary*180./!pi,2),0,4)+String("260B), $
			'!4u!3 = '+strmid(strtrim(phi_temporary*180./!pi,2),0,5)+String("260B), $
			'CR' +strmid(strtrim(car_rotfix,2),0,4)]
		arra=['AVG%DIFF = '+strmid(strtrim(-1.0,2),0,6)]
		IF plot_rightfdeltafit NE 2 THEN BEGIN
		arr2=[	'r0 = ' +strmid(strtrim(fr0/696000.,2),0,4)+' '+'!SR!D!9n!R', $
			'!3v0 = ' +strmid(strtrim(v0,2),0,5)+ ' km/s', $
			'vf = ' +strmid(strtrim(vf,2),0,5)+ ' km/s', $
			'a0 = ' +strmid(strtrim(-a0,2),0,4)+' m/s'+'!U2', $
			'!3d0 = ' +strmid(strtrim(d0/(149598000.),2),0,4)+' AU', $
			'!4s!3 = '+strmid(strtrim(ftau/3600.,2),0,4)+ ' hr']
		ENDIF
		IF plot_rightfdeltafit EQ 2 THEN BEGIN
			arra=['AVG%DIFF = '+strmid(strtrim(percentdiff_fit,2),0,4)]
			arr2=[	'r0 = ' +strmid(strtrim(fr0/696000.,2),0,4)+' '+'!SR!D!9n!R', $
			'!3v0 = ' +strmid(strtrim(v0_curvefit,2),0,5)+ ' km/s', $
			'vf = ' +strmid(strtrim(vf,2),0,5)+ ' km/s', $
			'a0 = ' +strmid(strtrim(-a0_curvefit*1000.,2),0,4)+' m/s'+'!U2', $
			'!3d0 = ' +strmid(strtrim(d0/(149598000.),2),0,4)+' AU', $
			'!4s!3 = '+strmid(strtrim(ftau/3600.,2),0,4)+ ' hr']
		ENDIF
		
		!p.multi=[0,2,2,0,0]
		plot,t,alpha*180./!pi,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)'
		IF plot_rightfdeltafit EQ 1 THEN oplot,t,ydelta
		IF plot_rightfdeltafit EQ 2 THEN oplot,t,fixdeltafit*180./!pi
		legend,arr,/top,/left,spacing=1.2,box=0,charsize=1.2
		IF plot_rightfdeltafit EQ 2 AND dochi EQ 1 THEN legend,arra,/bottom,/right,spacing=1.2,charsize=1.2,box=0
		plot,cutr_fix,cutv_fix,charsize=1.5,xtitle='r/R_sun',ytitle='v (km/s)'
		IF plot_rightfdeltafit EQ 2 THEN BEGIN
			plot,t,r_fit,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='r/R_sun'
			oplot,t,r_fix_curvefit
		ENDIF
		IF plot_rightfdeltafit EQ 1 THEN BEGIN
			plot,t,r_fit,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='r/R_sun'
			oplot,t,r_fix_manualfit
		ENDIF
		legend,arr2,/top,/left,spacing=1.2,box=0,charsize=1.2
		plot,cutr_fix,cuta_fix*1000.,charsize=1.5,xtitle='r/R_sun',ytitle='a (m/s'+'!S!U2!R'+' !3)'		
		
		set_plot,'ps'
		device,ysize=9.5,/in
		device,yoffset=0.75,/in
		device,/landscape
		!p.position=[0.05,0.55,0.45,0.95]
		plot,t,alpha*180./!pi,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)'
		IF plot_rightfdeltafit EQ 2 THEN oplot,t,fixdeltafit*180./!pi
		IF plot_rightfdeltafit EQ 1 THEN oplot,t,ydelta
		legend,arr,/top,/left,spacing=1.2,box=0,charsize=1.2
		IF plot_rightfdeltafit EQ 2 AND dochi EQ 1 THEN legend,arra,/bottom,/right,spacing=1.2,box=0,charsize=1.2
		!p.position=[0.05,0.05,0.45,0.45]
		IF plot_rightfdeltafit EQ 2 THEN BEGIN
			plot,t,r_fit,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='r/R_sun'
			oplot,t,r_fix_curvefit
		ENDIF
		IF plot_rightfdeltafit EQ 1 THEN BEGIN
			plot,t,r_fit,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='r/R_sun'
			oplot,t,r_fix_manualfit
		ENDIF
		legend,arr2,/top,/left,spacing=1.2,box=0,charsize=1.2
		!p.position=[0.59,0.55,0.99,0.95]
		plot,cutr_fix,cutv_fix,charsize=1.5,xtitle='r/R_sun',ytitle='v (km/s)'
		!p.position=[0.59,0.05,0.99,0.45]
		plot,cutr_fix,cuta_fix*1000.,charsize=1.5,xtitle='r/R_sun',ytitle='a (m/s'+'!S!U2!R'+' !3)'

		dates=strmid(utc2str(date),0,10)+' '+strmid(utc2str(date),11,5)
		xyouts,0.5,1.0,file+'   '+dates+' UT',alignment=0.5,/normal
		device,/close & device,/portrait ;return ps to normal type of output
		set_plot,'x'
		!p.position=-1 ;finished with ps file
		!p.multi=[0,1,1,0,0] 
	ENDIF
	IF dodelta EQ 0 THEN BEGIN
		arr=[	't1 = ' +strmid(strtrim(t1,2),0,4)+' hr', $
			'vf = ' +strmid(strtrim(vf_up*vcon,2),0,6)+' km/s', $
			'r1 = ' +strmid(strtrim(r1,2),0,4), $
			'!4d!3'+'0 = '+strmid(strtrim(delta*180./!pi,2),0,4)+String("260B), $
			'pa = ' + strmid(strtrim(pa,2),0,4), $
			'!4h!3 = '+strmid(strtrim(theta*180./!pi,2),0,4)+String("260B), $
			'!4u!3 = '+strmid(strtrim(phi*180./!pi,2),0,4)+String("260B), $
			'CR' +strmid(strtrim(car_rot,2),0,4)]
		arra=['CHISQ = '+strmid(strtrim(chisq_up,2),0,6)]
		IF fit_type NE 'linear' THEN $ ;second legend depends on fit type
		arr2=[	't0 = ' +strmid(strtrim(t0,2),0,4)+' hr', $
			'vi = ' +strmid(strtrim(vi*vcon,2),0,5)+ ' km/s', $
			'vf = ' +strmid(strtrim(vf_full*vcon,2),0,5)+ ' km/s', $
			'!4S!3 = '+strmid(strtrim(tau,2),0,4)+ 'hr', $
			fit_type]$
		ELSE arr2=['V = '+strmid(strtrim(vf_full*vcon,2),0,6)+' km/s', $
			'y-int = '+strmid(strtrim(r0,2),0,5)+' R_s', $
			'linear']
		IF fit_type NE 'linear' AND dochi THEN arr2=[arr2,'CHISQ = '+strmid(strtrim(chisq_down,2),0,6)]
		IF fit_type NE 'linear' THEN BEGIN ;different output for a linear fit
			cutr=getr(ftime=cutt)
			cutv=getv(ftime=cutt)*vcon
			cuta=geta(ftime=cutt)*vcon/3.6
			!p.multi=[0,2,2,0,0]
			plot,t,alpha*180./!pi,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)'
			oplot,t,fitalpha*180./!pi
			legend,arr,/top,/left,spacing=1.2,box=0,charsize=1.2
			IF dochi THEN legend,arra,/bottom,/right,spacing=1.2,charsize=1.2,box=0
			plot,cutr,cutv,charsize=1.5,xtitle='r/R_sun',ytitle='v (km/s)'
			plot,t,r,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='r/R_sun'
			oplot,t,getr()
			legend,arr2,/top,/left,spacing=1.2,box=0,charsize=1.2
			plot,cutr,cuta,charsize=1.5,xtitle='r/R_sun',ytitle='a (m/s'+'!S!U2!R )'
		ENDIF ELSE BEGIN
			!p.multi=[0,2,1,0,0]
			plot,t,alpha*180./!pi,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)'
			oplot,t,fitalpha*180./!pi
			legend,arr,/top,/left,spacing=1.2,box=0,charsize=1.2
			plot,t,r,psym=2,charsize=1.5,xtitle='t (hrs)',ytitle='r/R_sun'
			oplot,t,r0+vf_full*t
			legend,arr2,/top,/left,spacing=1.2,box=0,charsize=1.2
		ENDELSE

		set_plot,'ps';need to make display window and temp ps file seperately
		IF fit_type NE 'linear' THEN BEGIN
			device,ysize=9.5,/in
			device,yoffset=0.75,/in
			device,/landscape
			!p.position=[0.05,0.55,0.45,0.95]
			plot,t,alpha*180./!pi,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='!4a!3 (deg)'
			oplot,t,fitalpha*180./!pi
			legend,arr,/top,/left,spacing=1.2,box=0,charsize=1.2
			IF dochi THEN legend,arra,/bottom,/right,spacing=1.2,box=0,charsize=1.2
			!p.position=[0.05,0.05,0.45,0.45]
			plot,t,r,psym=2,charsize=1.5,xtitle='Time (hrs)',ytitle='r/R_sun'
			oplot,t,getr()
			legend,arr2,/top,/left,spacing=1.2,box=0,charsize=1.2
			!p.position=[0.59,0.55,0.99,0.95]
			plot,cutr,cutv,charsize=1.5,xtitle='r/R_sun',ytitle='v (km/s)'
			!p.position=[0.59,0.05,0.99,0.45]
			plot,cutr,cuta,charsize=1.5,xtitle='r/R_sun',ytitle='a (m/s'+'!S!U2!R'+' !3)'
		ENDIF ELSE BEGIN
			device,ysize=13,/in
			device,xsize=13,/in
			device,/landscape
			!p.position=[0.08,0.15,0.48,0.95]
			plot,t,alpha*180./!pi,psym=2,charsize=1.5,xtitle='Time (hrs)', ytitle='!4a!3 (deg)'
			oplot,t,fitalpha*180./!pi
			legend,arr,/top,/left,spacing=1.2,box=0,charsize=1.2
			!p.position=[0.58,0.15,0.98,0.95]
			plot,t,r,psym=2,charsize=1.5,xtitle='t (hrs)',ytitle='r/R_sun'
			oplot,t,r0+vf_full*t
			legend,arr2,/top,/left,spacing=1.2,box=0,charsize=1.2	
		ENDELSE
		dates=strmid(utc2str(date),0,10)+' '+strmid(utc2str(date),11,5)
		xyouts,0.5,1.0,file+'   '+dates+' UT',alignment=0.5,/normal
		device,/close & device,/portrait ;return ps to normal type of output
		set_plot,'x'
		!p.position=-1 ;finished with ps file
		!p.multi=[0,1,1,0,0] 
	ENDIF
	END
'print':BEGIN
		spawn,"lpstat -a|cut -f 1 -d ' '",name ;get printer names
		name=[name,'default'] ;and add default to the list
		name=wopts2(name,label='Select Printer')
		IF name EQ 'default' THEN spawn, 'lp idl.ps' ELSE spawn,'lp -d '+name+' idl.ps'
	END
'carr':	BEGIN
	IF dodelta EQ 1 THEN BEGIN
		theta_temporary=theta_fix
		phi_temporary=phi_fix
		car_rot_temporary=car_rotfix
	ENDIF
	IF dodelta EQ 0 THEN BEGIN
		theta_temporary=theta
		phi_temporary=phi
		car_rot_temporary=car_rot
	ENDIF
	IF datatype(calcdelta_flag) NE 'UND' THEN BEGIN
		IF calcdelta_flag EQ 1 THEN BEGIN
			theta_temporary=cd_theta
			phi_temporary=cd_phi
		ENDIF
	ENDIF
	print,'car_rot=',car_rot_temporary
	print,'theta=',theta_temporary
	print,'phi=',phi_temporary
	wviewcarr,car_rot=car_rot_temporary,theta=theta_temporary,phi=phi_temporary
	END
'dochi': dochi=WIDGET_INFO(event.id,/button_set)
'dodelta': dodelta=WIDGET_INFO(event.id,/button_set)
'save':	BEGIN
		name=dialog_pickfile(filter='*.ps',path='/sjmaps11/ht_data/ps_files')
		spawn,'cp idl.ps '+name
	END
'quit': BEGIN
	window,plot3id,/pixmap & wdelete
	window,plot4id,/pixmap & wdelete
	widget_control,event.top,/destroy
	IF file_test('idl.ps') NE -1 THEN spawn,'rm -f idl.ps'
	END
else:
ENDCASE

END

;first, make the GUI
PRO wgraph,f=f,soho=soho
COMMON graphshared
is_soho=0
IF keyword_set(soho) then begin
	is_soho=1
	print,'Calling wgraph with SOHO'
endif
;this is mostly setting up the widget
dochi=0 ;by default, don't display chi^2
dodelta=0
vcon=232/1.2 ;conversion factor from R_SUN/HR to KM/S
main_base=WIDGET_BASE(UVALUE='main',title='jMap CurveFit',column=2)
plots_base=WIDGET_BASE(main_base,UVALUE='plots', row=2)
inputs_base=WIDGET_BASE(main_base,uvalue='inputs',row=6)

plot1=WIDGET_DRAW(plots_base,frame=3,retain=2,uvalue='plot1',xsize=600,ysize=400)
plot2=WIDGET_DRAW(plots_base,frame=3,retain=2,uvalue='plot2',xsize=600,ysize=400)

; temp=WIDGET_LABEL(inputs_base,Value='Follow steps in order')

tempbase=WIDGET_BASE(inputs_base,row=7,frame=3) ;top box on left side
temp=WIDGET_LABEL(tempbase,Value='Trial Values') ;t stands for trial
tempbase2=WIDGET_BASE(tempbase,row=2)
trials=CW_FIELD(tempbase2,uvalue='t1',title='T1    ',value=10,ysize=1,xsize=5)
trials=Replicate(trials,4)
trials[1]=CW_FIELD(tempbase2,uvalue='tdelta',title='Delta ',value=0,ysize=1,xsize=5)
trials[2]=CW_FIELD(tempbase2,uvalue='tvf',title='Vf    ',value=300,ysize=1,xsize=5)
trials[3]=CW_FIELD(tempbase2,uvalue='tr1',title='r1    ',value=10,ysize=1,xsize=5)

temp=WIDGET_BUTTON(tempbase,uvalue='upfit',value='Compute Upper Fit')
temp=WIDGET_LABEL(tempbase,value='Output Values')
tempbase2=WIDGET_BASE(tempbase,column=2)
tempbase2a=WIDGET_BASE(tempbase2,row=4,xsize=100)
tempbase2b=WIDGET_BASE(tempbase2,row=4)
outs=WIDGET_LABEL(tempbase2a,value='Delta :',/dynamic_resize)
outs=replicate(outs,7)
outs[1]=WIDGET_LABEL(tempbase2a,value='Vf    :',/dynamic_resize)
outs[2]=WIDGET_LABEL(tempbase2a,value='r1    :',/dynamic_resize)
outs[3]=WIDGET_LABEL(tempbase2b,value='Theta :',/dynamic_resize)
outs[4]=WIDGET_LABEL(tempbase2b,value='Phi   :',/dynamic_resize)
outs[5]=WIDGET_LABEL(tempbase2b,value='CR    :',/dynamic_resize)
outs[6]=WIDGET_LABEL(tempbase2a,value='CHISQ :',/dynamic_resize)
;outs[7]=WIDGET_LABEL(tempbase2b,value='r^2   :',/dynamic_resize)


fits=['Linear','Nexp','Sqrt','Tanh','Atan','Poly4','Poly5'] ;need to add new fit types to list list!
tempbase=WIDGET_BASE(inputs_base,row=13,frame=3)
trials2=WIDGET_COMBOBOX(tempbase,/dynamic_resize,uvalue='type',value=['Select Fit Type',fits])
trials2=replicate(trials2,6)
temp=WIDGET_LABEL(tempbase,value='Trial Values')
tempbase2=WIDGET_BASE(tempbase,row=2)
trials2[1]=CW_FIELD(tempbase2,uvalue='t0',title='t0   ',value=-1.5,ysize=1,xsize=5)
trials2[2]=CW_FIELD(tempbase2,uvalue='tau',title='Tau  ',value=3,ysize=1,xsize=5)
trials2[3]=CW_FIELD(tempbase2,uvalue='r0',title='r0   ',value=5,ysize=1,xsize=5)
trials2[4]=CW_FIELD(tempbase2,uvalue='vi',title='Vi   ',value=500,ysize=1,xsize=5)

temp=WIDGET_BUTTON(tempbase,uvalue='do_fit',value='Compute Fit')
temp=WIDGET_LABEL(tempbase,value='Output Values')
tempbase2=WIDGET_BASE(tempbase,column=2)
tempbase2a=WIDGET_BASE(tempbase2,row=3,xsize=100)
tempbase2b=WIDGET_BASE(tempbase2,row=3)
outs2=WIDGET_LABEL(tempbase2a,value='t0    :',/dynamic_resize)
outs2=Replicate(outs2,6)
outs2[1]=WIDGET_LABEL(tempbase2a,value='Tau   :',/dynamic_resize)
outs2[2]=WIDGET_LABEL(tempbase2a,value='r0    :',/dynamic_resize)
outs2[3]=WIDGET_LABEL(tempbase2b,value='Vi    :',/dynamic_resize)
outs2[4]=WIDGET_LABEL(tempbase2b,value='Vf    :',/dynamic_resize)
outs2[5]=WIDGET_LABEL(tempbase2a,value='CHISQ :',/dynamic_resize)
;outs2[6]=WIDGET_LABEL(tempbase2b,value='r^2  :',/dynamic_resize)

fits2=['Constant','Exponential','Nexp']
tempbase=WIDGET_BASE(inputs_base,frame=3,row=5)
trials3=WIDGET_COMBOBOX(tempbase,/dynamic_resize,uvalue='type',value=['Select Acc Type',fits2])
WIDGET_CONTROL,trials3,SET_COMBOBOX_SELECT=1
acc_type=strlowcase('constant')
tempbase2=WIDGET_BASE(tempbase,column=2)
temp=WIDGET_LABEL(tempbase2,value='Fixed Delta Trial Values')
tempbase3=WIDGET_BASE(tempbase,column=2)
trials3=Replicate(trials3,6)
trials3[1]=CW_FIELD(tempbase3,uvalue='tfixdelta',title='Delta0',value=0,ysize=1,xsize=5)
trials3[2]=CW_FIELD(tempbase3,uvalue='tv0',title='V0    ',value=700,ysize=1,xsize=5)
trials3[3]=CW_FIELD(tempbase3,uvalue='ta0',title='a0    ',value=0,ysize=1,xsize=5)
trials3[4]=CW_FIELD(tempbase3,uvalue='tr0',title='r0    ',value=7.5,ysize=1,xsize=5)
trials3[5]=CW_FIELD(tempbase3,uvalue='ftau',title='ftau  ',value=1,ysize=1,xsize=5)
tempbase5=WIDGET_BASE(tempbase,column=3)
temp=WIDGET_BUTTON(tempbase5,uvalue='plot_fdelta',value='Compute Fit')
temp=WIDGET_BUTTON(tempbase5,uvalue='fixdeltafit_widget',value='Curvefit')
temp=WIDGET_BUTTON(tempbase5,uvalue='calcdelta',value='Find Delta')

tempbase4=WIDGET_BASE(tempbase,column=2)
tempbase4a=WIDGET_BASE(tempbase4,row=1,xsize=100)
tempbase4b=WIDGET_BASE(tempbase4,row=1)
outs3=WIDGET_LABEL(tempbase4a,value='rec r0:',/dynamic_resize)
outs3=Replicate(outs3,2)
outs3[1]=WIDGET_LABEL(tempbase4b,value='AVG%DIF:',/dynamic_resize)

tempbase=WIDGET_BASE(inputs_base,frame=3,row=2)
tempbase2=WIDGET_BASE(tempbase,row=1)
temp=CW_FIELD(tempbase2,uvalue='cuttime',title='Cut Time',/return_events,ysize=1,xsize=5)
temp=WIDGET_BUTTON(tempbase2,value='Show Final Plots',uvalue='ok')

nonexbase=WIDGET_BASE(tempbase,row=1,/nonexclusive)
temp=WIDGET_BUTTON(nonexbase,value='Display REG',uvalue='dochi')
temp=WIDGET_BUTTON(nonexbase,value='Show/Hide Delta Graph',uvalue='dodelta')

temp=WIDGET_BUTTON(inputs_base,value='View Carr',uvalue='carr',frame=3)

tempbase=WIDGET_BASE(inputs_base,column=4,frame=3)
temp=WIDGET_BUTTON(tempbase,uvalue='load',value='Load')
temp=WIDGET_BUTTON(tempbase,uvalue='print',value='Print')
temp=WIDGET_BUTTON(tempbase,uvalue='save',value='Save')
temp=WIDGET_BUTTON(tempbase,uvalue='quit',value='Quit')

WIDGET_CONTROL,main_base,/realize
widget_control,plot1,get_value=plot1id
widget_control,plot2,get_value=plot2id
plot5id=117 ;arbitrary number to prevent swapping of plot1id and plot2id
plot3id=10 & plot4id=11 ;set to somewhat arbitrary numbers--allows us to open and close plot windows
FOR i=1,4 DO widget_control,trials2[i],sensitive=0
IF keyword_set(f) THEN BEGIN ;load ht_file if one is given in call to program
	file=f
	loadht
ENDIF

XMANAGER, 'main', main_base

END
