; $Id: wopts.pro,v 1.2 2011/06/28 13:55:36 cooper Exp $

; $Log: wopts.pro,v $
; Revision 1.2  2011/06/28 13:55:36  cooper
; Added comments, outlist parameter
;
; Revision 1.1  2011/06/24 20:12:02  cooper
; First Version, widget for selecting options
;

;pop-up widget for selecting options via radio buttons

;Input:	opts-array of options which will appear as the buttons. If outlist is not set, the returned value
;		is the selected element of  opts
;Optional Inputs:label-label on the widget, used to describe what is being selected
; 		outlist-array with same number of elements as opts. If set, the returned value will be the value
;			of outlist that corresponds to the selected element of opts
;Output:	selected element of opts, or corresponding element of outlist
PRO readwopts_event,event
	widget_control,event.id,get_uvalue=uval
	widget_control,event.top,get_uvalue=info
	widget_control,event.id,get_value=val
	CASE uval OF
	'opts':(*info).val=val
	ELSE:
	ENDCASE
	widget_control,event.top,/destroy
END

FUNCTION wopts,opts,label=label,outlist=outlist
	IF keyword_set(outlist) AND n_elements(outlist) NE n_elements(opts) THEN $
		x=dialog_message('Output list and options list have different sizes',/error)
	base0=widget_base()
	base=widget_base(row=2,/modal,group_leader=base0,/floating,xoffset=10,yoffset=8,units=1,title=title)
	IF keyword_set(label) THEN lab=widget_label(base,value=label)
	options=cw_bgroup(base,opts,frame=2,/exclusive,uvalue='opts',/column)
	info=ptr_new({val:''})
	widget_control,base,set_uvalue=info
	widget_control,base,/realize
	xmanager,'readwopts',base
	widget_control,base0,/destroy
	val=fix((*info).val)
	ptr_free,info
	IF keyword_set(outlist) THEN out=outlist[val] ELSE out=opts[val]
	return,out
END