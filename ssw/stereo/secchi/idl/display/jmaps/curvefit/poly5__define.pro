
; $Id: poly5__define.pro,v 1.2 2008/07/15 20:21:32 nathan Exp $
; $Log: poly5__define.pro,v $
; Revision 1.2  2008/07/15 20:21:32  nathan
; fixed ID tag
;
; Revision 1.1  2008/07/15 15:04:27  casto
; Beginning of curvefit
;
FUNCTION poly5::br, x
	y = x*0.0
	for i=0, n_elements(x)-1 do begin
		a = x[i]
		if (abs(a) le 1) then begin
			y[i] = 1./16.*a^2*(a^4-5*a^2+15)
		endif else begin
			y[i] = abs(a)-5./16.
		endelse
	endfor
	return, y
end

FUNCTION poly5::bv, x
	y = x*0.0
	for i=0, n_elements(x)-1 do begin
		a = x[i]
		if (abs(a) le 1) then begin
			y[i] = 1./8.*a*(3*a^4-10*a^2+15)
		endif else begin
			y[i] = a/abs(a)
		endelse
	endfor
	return, y
end

FUNCTION poly5::ba, x
	y = x*0.0
	for i=0, n_elements(x)-1 do begin
		a = x[i]
		if (abs(a) le 1) then begin
			y[i] = 15./8.*(a^2-1)^2
		endif else begin
			y[i] = 0.0
		endelse
	endfor
	return, y
end

PRO poly5__define
	struct = {poly5, INHERITS curvefunc}
end
