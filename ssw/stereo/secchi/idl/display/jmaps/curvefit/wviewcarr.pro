; $Id: wviewcarr.pro,v 1.7 2013/06/14 15:56:49 chung Exp $

; $Log: wviewcarr.pro,v $
; Revision 1.7  2013/06/14 15:56:49  chung
; changed euvi ew= to ew=C to show correct carrmap.
;
; Revision 1.6  2012/07/30 18:04:13  jasonlee
; fixed minor printing issue
;
; Revision 1.5  2012/07/19 15:39:17  cooper
; can save maps
;
; Revision 1.4  2011/07/05 20:24:49  cooper
; Reset ps file to portrait when finished
;
; Revision 1.3  2011/06/28 21:27:23  cooper
; Really fixed printing error
;
; Revision 1.2  2011/06/28 20:49:55  cooper
; Fixed Printing Error
;
; Revision 1.1  2011/06/28 13:55:02  cooper
; Update viewcarr program
;

;Updated program for viewing carrington maps, doesn't use command line
;Uses wopts and wtext for pop-up widgets instead
;Optional Inputs: car_rot-carrington rotation to use
;		phi-azimuthal angle at which to place a marker, in deg
;		theta-polar angle at which to place a marker, in deg

PRO wviewcarr,car_rot=car_rot,phi=phi,theta=theta
line0:
instr=wopts(['cor1','cor2','euvi'],label='Select Instrument')
aorb=wopts(['A','B'],label='Select Satellite',outlist=['a','b'])
CASE instr OF
'cor2':BEGIN
	ew=wopts(['East','West'],label='Select Limb',outlist=['E','W'])
	outlist=[117,113,121,125,129]	;sid# Arnaud gave to file names
	criteria=wopts(['3','5','7','10','13'],label='Select Distance (R_Sun)',outlist=outlist)
	IF aorb EQ 'b' THEN criteria++ ;increment by 1 for b
	criteria='sid'+strtrim(criteria,2)
	END
'cor1':BEGIN
	ew=wopts(['East','West'],label='Select Limb',outlist=['E','W'])
	outlist=[221,223,219,225,227,229]
	criteria=wopts(['1.5','1.8','2.2','2.6','3.0','3.4'],label='Select Distance (R_Sun)',outlist=outlist)
	IF aorb EQ 'b' THEN criteria++
	criteria='sid'+strtrim(criteria,2)
	END
'euvi':BEGIN
	criteria=wopts(['171','195','284','304'],label='Select Wavelength')
	ew='C'
	END
ENDCASE
IF ~keyword_set(car_rot) THEN crnum=wtext('CR #') ELSE crnum=car_rot
scar_rot=strtrim(fix(crnum),2)
map_img=find_file('/net/earth/secchi/carrmaps/'+aorb+'/'+instr+'/cm/png/*'+criteria+'*cr'+scar_rot+ew+'*')
IF map_img[0] EQ '' THEN BEGIN
	temp=dialog_message('Carrington Map for CR ' +scar_rot+' not found: /net/earth/secchi/carrmaps/'+aorb+'/'+instr+'/cm/png/')
	again=wopts(['Yes','No'],label='Want to see another carrmap')
	IF again EQ 'Yes' THEN goto,line0 ELSE return
ENDIF
map_img=map_img[n_elements(map_img)-1] ;if several versions, take most recent one
map_img=read_png(map_img)
WINDOW,6,xsize=880,ysize=500
tvscl,map_img
IF (datatype(phi) NE 'UND') THEN BEGIN ;plot point if theta and phi are given
	x=(phi*180./!pi)*2+79
	y=69-2*((theta*180./!pi)-180.)
	xyouts,x-2,y-1,'(',/device
	xyouts,x+2,y-1,')',/device,color=1
ENDIF
xyouts,600,450,'Right Click To Continue',/device,color=1,charsize=2
cursor,x,y,/device ;track the cursor point to print lat and lon of cursor
lat0='' & lon0=''
WHILE !MOUSE.button NE 4 DO BEGIN
	x=(x-79)/2 ;(80,70) is lower left corner of map in pixels WRT screen. Divide by 2 since map is 720X360
	y=ABS((y-69)/2.-180.)
	lat='Latitude: '+strtrim(y,2)
	lon='Longitude: '+strtrim(x,2)
	IF lat NE lat0 OR lon NE lon0 THEN BEGIN
		DEVICE,COPY=[10,450,250,50,500,10] ;copy blank spot over to write onto
		xyouts,500,30,lon,/device,color=1,charsize=2
		xyouts,500,10,lat,/device,color=1,charsize=2
		lat0=lat & lon0=lon
	ENDIF
	cursor,x,y,/device
ENDWHILE
device,copy=[10,450,250,50,600,440] ;remove right click instruction
temp=tvrd()
tempname='printable.ps'
set_plot,'ps'
device,/landscape
device,/color,bits=8
device,filename=tempname
tvscl,temp
device,/close & device,/portrait
set_plot,'x'
sp=wopts2(['Save','Print'],label='Save and/or Print?',outlist=['s','p'],/nocancel)
p=where(sp EQ 'p')
IF p[0] NE -1 THEN BEGIN ;print the map if asked to
	spawn,"lpstat -a|cut -f 1 -d ' '",name ;get printer names
	name=[name,'default'] ;and add default to the list
	name=wopts(name,label='Select Printer')
	IF name EQ 'default' THEN spawn, 'lp printable.ps' ELSE spawn,'lp -d '+name+' printable.ps'
ENDIF
s=where(sp EQ 's')
IF s[0] NE -1 THEN BEGIN ;save the map as .ps if asked to
	fname=dialog_pickfile(path='/sjmaps11/ht_data/ps_files',filter=['*.ps'],default_extension='.ps')
	file_copy,'printable.ps',fname
ENDIF
file_delete,'printable.ps'
again=wopts(['Yes','No'],label='Want to see another carrmap')
IF again EQ 'Yes' THEN goto,line0
window,6,/pixmap & wdelete,6
END
