;+
; $Id: adddirs.pro,v 1.7 2012/06/25 16:21:51 sheeley Exp $
; NAME: adddirs
; 
; PURPOSE: provides directory paths
; 
; USAGE: adddirs
;
; KEYWORDS: None
; 
; $Log: adddirs.pro,v $
; Revision 1.7  2012/06/25 16:21:51  sheeley
; nr - change dirs to /net/toy from /net/alamo
;
; Revision 1.6  2009/05/26 19:17:38  cooper
; updated for summer09
;
; Revision 1.5  2008/07/01 19:03:01  lee
; adddirs.pro
;
; Revision 1.4  2008/07/01 18:59:25  lee
; Added $Id and $Log
;-


PRO ADDDIRS

COMMON DIR_COMMON, mvi_dir, jmap_dir, median_dir, diff_dir, image_dir

;mvi_dir = '/home/sheeley/stereo/adam/movies/'

mvi_dir = '/net/toy/data1/secchi/movies/'
jmap_dir = '/net/toy/data1/secchi/jmaps/'

;jmap_dir = '/home/sheeley/stereo/adam/jmaps/'
;median_dir = '/home/sheeley/stereo/adam/star_rem/medians/'
;diff_dir = '/home/sheeley/stereo/adam/star_rem/temp/'
;image_dir = '/home/sheeley/stereo/adam/images/secchi/'

;cd,'/home/sheeley/stereo/adam/movie_progs'
;addpath
;cd,'../programs'
;addpath
;cd,'/home/sheeley/stereo/adam/data_anal'
;addpath
;cd,'../star_rem'
;addpath
;cd,'/home/sheeley/stereo/adam/dev/movie'
;addpath
;cd,'~/dev/jmaps'
;addpath
;cd,'../star_rem'
;addpath
;cd,'../../jprogs'
;addpath
;cd,'../jmaps2a'
;addpath
;cd

END
