FUNCTION chop, img, factor
;+
; NAME:
;	CHOP
;
; PURPOSE:
;	This function increases the contrast of an image.
;
; CATEGORY:
;	Image tool.
;
; CALLING SEQUENCE:
;	Result = CHOP(Img, 4)
;
; INPUTS:
;	Img:	The image (2-D array) to be enhanced.
;
; OUTPUTS:
;	The function returns the enhanced image.
;
; OPTIONAL OUTPUTS:
;	Describe optional outputs here.  If the routine doesn't have any, 
;	just delete this section.
;
; COMMON BLOCKS:
;	BLOCK1:	Describe any common blocks here. If there are no COMMON
;		blocks, just delete this entry.
;
; SIDE EFFECTS:
;	Describe "side effects" here.  There aren't any?  Well, just delete
;	this entry.
;
; RESTRICTIONS:
;	Describe any "restrictions" here.  Delete this section if there are
;	no important restrictions.
;
; PROCEDURE:
;	You can describe the foobar superfloatation method being used here.
;	You might not need this section for your routine.
;
; EXAMPLE:
;	Please provide a simple example here. An example from the PICKFILE
;	documentation is shown below.
;
;	Create a PICKFILE widget that lets users select only files with 
;	the extensions 'pro' and 'dat'.  Use the 'Select File to Read' title 
;	and store the name of the selected file in the variable F.  Enter:
;
;		F = PICKFILE(/READ, FILTER = ['pro', 'dat'])
;
; MODIFICATION HISTORY:
; 	Written by:	Your name here, Date.
;	July, 1994	Any additional mods get described here.  Remember to
;			change the stuff above if you add a new keyword or
;			something!
;
;	%W% %H% LASCO IDL LIBRARY
;-   
   result = moment(img)
   std_dev = SQRT(result(1))
   s = size(img)
   cols = s(1)
   rows = s(2)
   top_chop = MEDIAN(img) + factor*std_dev
   bottom_chop = MEDIAN(img) - factor*std_dev
   new_img = img
   
   indeces = WHERE (img gt top_chop)
   IF(indeces(0) ne -1) THEN $
      new_img(indeces) = top_chop
   indeces = WHERE (img le bottom_chop)    
   IF (indeces(0) ne -1) THEN $
      new_img(indeces) = bottom_chop



   RETURN, new_img
END