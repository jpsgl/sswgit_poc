;$Id: edit_sdo_list.pro,v 1.3 2012/12/19 17:30:26 mcnutt Exp $
; Name:edit_sdo_list
; 
; PURPOSE: Edits list of sdo fits files so that time between
; 	consecutive frames is never less than given interval.
; 
; INPUTS: flist-list of sdo fits files to be editted
; OPTIONAL INPUTS: Interval-interval between output frames in seconds. Default 20

;Written by Tom Cooper, 2011-06-16 (based on edit_list for Soho data)

;$Log: edit_sdo_list.pro,v $
;Revision 1.3  2012/12/19 17:30:26  mcnutt
;replace variable list
;
;Revision 1.2  2011/08/04 13:53:04  cooper
;change GT to GE
;
;Revision 1.1  2011/07/26 20:55:50  cooper
;first version
;
FUNCTION edit_sdo_list,flist,interval=interval,minute=minute
IF datatype(flist) EQ 'UND' THEN BEGIN
	message,'List not given', /informational
	return,-1
ENDIF

read_sdo,flist,hdrs

times=anytim2tai(hdrs.date_obs) ;put in sequential order
ind=sort(times)
times=times[ind] & flist=flist[ind]

IF ~keyword_set(interval) THEN interval=20 ELSE IF keyword_set(minute) THEN interval*=60.
n=n_elements(times)
ind=intarr(1)
last_time=times[0]
FOR i=1,n-1 DO BEGIN
	curr_time=times[i]
	IF curr_time-last_time GE interval THEN BEGIN
		last_time=curr_time
		ind=[ind,i]
	ENDIF
ENDFOR

return,flist[ind]
END
