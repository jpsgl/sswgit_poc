;+
; $Id: multidisplay2a.pro,v 1.13 2017/01/03 15:27:40 mcnutt Exp $
;
; NAME:     multidisplay2a.pro
;
; PURPOSE:
;	Plot figures for display or printing from tool2a.pro
;
; CATEGORY:
;	JMAPS display printing postscript plotting
;
; CALLING SEQUENCE:
;	MULTIDISPLAY, inputstring
;
; INPUTS:
;	inputstring 	String:	'PREVIEW'  or 'PRINT' or 'SAVE'
;
; OPTIONAL INPUTS:
;	None
;	
; KEYWORD PARAMETERS:
;	None
;
; OUTPUTS:
;	plot window or postscript file
;
; EXAMPLE:
;
; Restrictions: Must have COMMON blocks defined from tool2a.pro:
;   	    TOOL2_COMMON
;   	    WIDGET_ID
;   	    DIR_COMMON
;   	This may be avoided by using command line input for required variables, if
;   	someone wants to implement that.
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Walters, Aug 2001
;-
; $Log: multidisplay2a.pro,v $
; Revision 1.13  2017/01/03 15:27:40  mcnutt
; Use select_windows instead of set_plot
;
; Revision 1.12  2012/07/20 13:00:36  jasonlee
; added cancel feature for print ps
;
; Revision 1.11  2012/06/26 16:48:50  cooper
; fixed printing bug
;
; Revision 1.10  2012/06/19 12:51:23  cooper
; checks for printers and uses wopts to select
;
; Revision 1.9  2012/06/18 20:15:16  cooper
; change idl.ps to idlps
;
; Revision 1.8  2011/10/20 20:15:30  nathan
; use SAVE_DIR for output, and prompt for new directory if user does not have permission
;
; Revision 1.7  2010/02/24 17:05:53  nathan
; set suggested output filenames
;
; Revision 1.6  2009/12/08 18:57:34  nathan
; utilize variable dpi in common pm1
;
; Revision 1.5  2009/12/07 22:36:53  nathan
; removed date from bottom title
;
; Revision 1.4  2009/12/07 21:21:13  nathan
; Implemented GIF input option; save PS image as 1200 dpi.
;
; Revision 1.3  2009/12/04 21:34:33  nathan
; add start time to bottom label
;
; Revision 1.2  2009/12/04 18:55:37  nathan
; changed contour call so x and y axes are in data units
;
; Revision 1.1  2009/12/04 17:46:39  nathan
; copied directly from tool2a.pro,v 1.24
;
;**************POSTSCRIPT FILE CODE, MODIFIED FROM LASERLAND.PRO******************

PRO PLOTjMAP,imcol,imrow,font,inputstring

common PM1, files, ximsize, yimsize, margins, borders, xoffset, yoffset, nimcols, nimrows, curr_x, curr_y, dpi
COMMON Tool2_Common, map, map_pix, newmap, new_pix, h, draw_mode, opts, allopts, prevmode, $
	edit_mode, plotted, save_region, movie_files, movie_frame, crop_hdr, start_time, end_time, $
	units, xscale, yscale, curr_pos, curr_pix, hts, vels, gauge_vel, vel_mode, busy, satellite, mvi_point

disp_row = nimrows-1 - imrow
position= [ (imcol*(curr_x+margins)+xoffset)/8.5, $
	(disp_row*(curr_y+margins)+yoffset)/11.0, $
	(imcol*(curr_x+margins)+curr_x+xoffset)/8.5, $
	(disp_row*(curr_y+margins)+curr_y+yoffset)/11.0]

charsz=1.2
fontcmd=''
xtickln=-0.037
charthk=1
;print, 'plotting',imrow,imcol,' at:'
;print, position

filenum = imcol*nimcols + imrow + 1
jmap = read_jmap2a(files[filenum], HEADER=hdr)
help,jmap
s0=size(jmap)

if (inputstring eq 'PREVIEW') or (inputstring eq 'GIF') then begin
	wset,0  ; preview window
	jmap=CONGRID( jmap, (position(2)-position(0))*!d.x_vsize, $
		(position(3)-position(1))*!d.y_vsize )
	help,jmap
	tv, jmap, position(0), position(1), /normal
	IF inputstring EQ 'GIF' THEN charsz=2.5 
	fontcmd='!17'	;Triplex Roman
	xtickln=-0.02
endif else begin
    	; most laser printers max out at 1200 pixel/inch 
	; otherwise entire array is saved in PS file
	IF dpi GT 100 THEN BEGIN
	    print,'Saving image with ',trim(dpi),' DPI.'
	    jmap=congrid(jmap,(curr_x*dpi)<s0[1],(curr_y*dpi)<s0[2])
	ENDIF ELSE $
	    print,'Saving image with about ',trim(long((s0[1]/curr_x)>(s0[2]/curr_y))),' DPI.'
	
	help,jmap
	tv, jmap, position(0), position(1), $
		/normal, xsize=position(2)-position(0), $
		ysize=position(3)-position(1)
endelse

s = size(jmap)

get_units, labels
left = labels[0]
right = 'PA = ' + strtrim(string(hdr.pa),2)
;IF(hdr.satellite NE '') THEN right+='      SAT : ' + hdr.satellite
utc = tai2utc([hdr.start_time, hdr.end_time])
date_str = mjd2str(utc.mjd)
top = date_str[0] + ' to ' + date_str[1]

dxticks = [1,2,3,4,6,8,12,24,48,72]  ; use only factors of 24 hours for ease in viewing
; compute ticks using hours
time1 = hdr.start_time / (3600)
time2 = hdr.end_time / (3600)
ntickx = 8
dxtick0 = (time2 - time1) / float(ntickx)
tmp = abs(dxticks - dxtick0)
tmp = min(tmp, tick_ind)
dxtick = dxticks[tick_ind]
IF(dxtick EQ 12) THEN dxtick = 24
xtickv0 = ((floor(time1) / dxtick) + 1) * dxtick
xcurr = xtickv0 + dxtick
WHILE(xcurr LT time2) DO BEGIN
	xtickv0 = [xtickv0, xcurr]
	xcurr = xcurr + dxtick
ENDWHILE
IF(dxtick LT 24) THEN BEGIN
	xtickname = strtrim(string(xtickv0 MOD 24), 2)
	bottom = 'Time (hr)'
	hr2day=1.
ENDIF ELSE BEGIN
	u = tai2utc(double(xtickv0 * 3600.))
	mjd2date, u.mjd + (u.time GT 80000000), yr, mo, dy
	xtickname = strtrim(string(dy),2)
	bottom = 'Time (day of month)'
	hr2day=24.
ENDELSE
; xtickv is hours
xtickv= (float(xtickv0) - time1) / hr2day
;xtickv = s[1] * (float(xtickv0) - time1) / (time2 - time1)
xtick = n_elements(xtickv)
;bottom=bottom+' from '+utc2str(utc[0],/ecs,/trunc)

; Y
;convert to proper units
tmp = hdr.min_r
convert_units, tmp, 1, units[0]
hdr.min_r = tmp
tmp = hdr.max_r
convert_units, tmp, 1, units[0]
hdr.max_r = tmp


nticky = 6
dytick = round((hdr.max_r - hdr.min_r) / float(nticky))
IF(dytick EQ 0) THEN dytick = 1
ytickv = ((floor(hdr.min_r) / dytick) + 1) * dytick
curr = ytickv + dytick
WHILE(curr LT hdr.max_r) DO BEGIN
	ytickv = [ytickv, curr]
	curr = curr + dytick
ENDWHILE
ytickname = strtrim(string(ytickv), 2)
;ytickv = s[2] * (float(ytickv) - hdr.min_r) / (hdr.max_r - hdr.min_r)
ytick = n_elements(ytickv)
blank = replicate(' ',50)

contour, jmap, /nodata, /noerase, xticks=1, yticks=1, $
	xstyle=1, ystyle=1, position=position, /normal, $
	xticklen=0.0, yticklen=0.0, xtickname=blank, $
        ytickname=blank, font=font  ,yrange=[hdr.min_r,hdr.max_r]   , xrange=([time1,time2]-time1)/hr2day


if (imrow eq 0) then axis, xaxis=1, xticks=xtick-1, xtickv=xtickv, $
	xticklen=xtickln, xtickname=blank, xtitle=fontcmd+top, xstyle=1, $
	color=0, charsize=charsz, charthick=charthk, font=font
if (imrow eq nimrows-1) then axis, xaxis=0, xticks=xtick-1, xtickv=xtickv, $
	xticklen=xtickln, xtickname=xtickname, xstyle=1, $
	color=0, charsize=charsz, charthick=charthk, font=font, xtitle=bottom
if (imcol eq 0) then axis, yaxis=0, yticks=ytick-1, ytickv=ytickv,  $
	yticklen=-0.012, ytickname=ytickname,  ytitle=left, ystyle=1,$
	color=0, charsize=charsz, charthick=charthk, font=font
if (imcol eq nimcols-1) then axis, yaxis=1, yticks=ytick-1, ytickv = ytickv, $
	yticklen=-0.012, ytickname=blank, ytitle=right, ystyle=1, $
	color=0, charsize=charsz, charthick=charthk, font=font

;print,''
;print,'Here you have the option of over-plotting data on the image.'
;print,'The units of the y-axis are ',left
;print,'The units of the x-axis are ',bottom
;print,'When you are finished, type .cont'
;stop

return
end


PRO MULTIDISPLAY2a, inputstring

COMMON WIDGET_ID, main1, draw6, draw6_id, reg_fields, scale_fields, coord_fields, plot_fields, ps_fields, $
	unit_fields, dev_fields, point_fields, chop_fields, denoise_fields, smooth_fields, file_fields, sync_fields
common PM1
COMMON DIR_COMMON, mvi_dir, jmap_dir, save_dir, diff_dir, image_dir

; save_dir = median_dir
IF datatype(save_dir) EQ 'UND' THEN save_dir=jmap_dir

WIDGET_CONTROL, ps_fields[11], GET_VALUE=tmp
ximsize = float(tmp[0])
WIDGET_CONTROL, ps_fields[13], GET_VALUE=tmp
yimsize = float(tmp[0])
WIDGET_CONTROL, ps_fields[2], GET_VALUE = tmp
nimrows = fix(tmp[0])
WIDGET_CONTROL, ps_fields[4], GET_VALUE = tmp
nimcols = fix(tmp[0])

IF inputstring NE 'GIF' THEN BEGIN
    curr_x = ximsize < ((8.5-borders[0]-borders[1]) / float(nimcols) - margins)
    curr_y = yimsize < ((11.0-borders[2]-borders[3]) / float(nimrows) - margins)

    xused= nimcols*curr_x + (nimcols-1)*margins
    yused= nimrows*curr_y + (nimrows-1)*margins
    xoffset= borders[0] + (8.5-xused-borders[0]-borders[1])/2.0
    yoffset= borders[2] + (11.0-yused-borders[2]-borders[3])/2.0
ENDIF ELSE BEGIN
    ; sizes are relative to 8.5x11 inches
    xoffset=8.5*100./!d.x_vsize ; 100 pixels
    yoffset=11*100./!d.y_vsize
    curr_x=( 8.5- 2*xoffset - (nimcols-1)*(margins))/nimcols
    curr_y=(11-   2*yoffset - (nimrows-1)*(margins))/nimrows
ENDELSE	    
help,save_dir
idlps=concat_dir(save_dir,'idl.ps')
if (inputstring EQ 'PRINT') or (inputstring EQ 'SAVE') then begin
	set_plot, 'ps'
	device, xoffset=0, yoffset=0, xsize=8.5, ysize=11, /inches, bits=8, $
		/times, /bold
	!p.charsize=1.0
	scale=1
	font=0
    	IF not check_permission(idlps) THEN BEGIN
    	    idlps = DIALOG_PICKFILE(FILTER = '*.ps', PATH = getenv('HOME'), FILE='idl.ps') 
	    break_file,idlps,vo,di,ro,su
	    save_dir=di
    	ENDIF
    	device,filename=idlps
endif else begin
	select_windows ;set_plot,'x'
	erase, 255
	!p.charsize=0.5
	scale=0.5
	font=-1
	;device,set_font='Times Bold',/tt_font
	;device,set_font='-adobe-times-bold-r-normal--18-180-75-75-p-99-iso8859-1'
endelse

nf = n_elements(files) < (nimrows*nimcols + 1)
row = 0
col = 0
FOR i=1, nf-1 DO BEGIN
	row = (i-1) / nimrows
	col = (i-1) MOD nimrows
	PLOTjMAP,row,col,font,inputstring
ENDFOR
break_file,files[1],vo,di,ro,su

if (inputstring EQ 'PRINT') or (inputstring EQ 'SAVE') then begin
	device, /close
	if (inputstring eq 'PRINT') then begin
;****************************************************************
; modification to allow multiple printers  nrs 7/07/08
;    	    	ans = ''
;    	    	print,'Which printer (default is lp)?'
;    	    	print,'- hp4-136 (Enter h)'
;    	    	print,'- xeroxphaser (Enter x)'
;    	    	read,'Or enter name of printer: ',ans
;    	    	dprtr=''
;    	    	if (ans eq 'h')      then dprtr='-d hp4-136' ELSE $
;    	    	if (ans eq 'x')      then dprtr='-d xeroxphaser' ELSE $
;    	    	IF strlen(ans) GT 1  THEN dprtr='-d '+ans
				spawn,'lpstat -a|cut -f 1 -d " "',name ;get the printer names
				name=[name,'default','cancel'] ;add default to the list
				dprtr=wopts(name,label='Select Printer')
    	    	IF dprtr EQ 'default' THEN spawn,'lp '+idlps ELSE spawn,'lp -d '+dprtr+' '+idlps
		
;*************************************************************************
;    	    	spawn, 'lp -d hp4-136 idl.ps'
    	    	spawn, '\rm '+idlps;idl.ps'
	endif else begin
		psname = DIALOG_PICKFILE(FILTER = '*.ps', PATH = save_dir, FILE=ro+'.ps')
		if (strpos(psname,'.ps') lt 0) then psname = psname + '.ps'
		spawn, '\mv '+idlps+' ' + psname
		print, 'File saved as ', psname
		break_file,psname,vol,dir,fi,su
		save_dir=dir
	endelse
	select_windows ;set_plot,'x'
endif

IF inputstring EQ 'GIF' THEN jk=ftvread(/GIF, filename=ro+'.gif')
return
end

;********************END POSTSCRIPT CODE*******************


