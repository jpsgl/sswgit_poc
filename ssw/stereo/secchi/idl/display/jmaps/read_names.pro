;Read a string array from a free format file

PRO READ_NAMES, filename, arr

arr = ''
tmp = ''

OPENR, lun, filename, /GET_LUN
WHILE ~ EOF(lun) DO BEGIN
	READF, lun, tmp
	arr = [arr, tmp]
ENDWHILE

UNDEFINE, tmp
arr = arr(1:*)

END
