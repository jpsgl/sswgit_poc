FUNCTION edit_list, flist, INTERVAL = interval, DEBUG=debug
;+
; $Id: edit_list.pro,v 1.3 2012/12/19 17:29:12 mcnutt Exp $
;
; NAME:
;	EDIT_LIST
;
; PURPOSE:
;	The function edits a list of fits files and removes files so that time between consecutive
;	frames is never less than a predetermined interval.
;
; CATEGORY:
;	Util, Movies
;
; CALLING SEQUENCE:
;	Result = EDIT_LST(flist, INTERVAL = 60)
;	Result = EDIT_LIST(flist)
;
; INPUTS:
;	fList:  A STRARR that contains the filenames of fits files.  Normally
;		the output of LASCO_LISTER
;
; OPTIONAL INPUTS:
;	Interval: A minimum time interval in minutes.  The default is 45.
;	
;
; OUTPUTS:
;	This function returns a list of fits filenames, intended to be used
;	by wmkmovie.
;
; OPTIONAL OUTPUTS:
;	Describe optional outputs here.  If the routine doesn't have any, 
;	just delete this section.
;
; $Log: edit_list.pro,v $
; Revision 1.3  2012/12/19 17:29:12  mcnutt
; replace variable list
;
; Revision 1.2  2010/05/14 15:36:48  nathan
; use lasco_readfits; remove progress bar
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Walters, Dec 1997
;	Modified:
;		Jul 2001, Ensure that files are in
;			  time order.  JHW
;		Sept. 5, 2001, restore dtmin calc. from prev. version.(nrs)
;	%W% %H% LASCO IDL LIBRARY
;-


   
   IF keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0
   
   IF (DATATYPE(flist) eq 'UND') THEN BEGIN
      MESSAGE, 'Calling sequence: IDL> newlist = edit_list(flist)', /INFORMATIONAL
      RETURN, -1
   ENDIF
   
   DEVICE, GET_SCREEN_SIZE = scr_size
   xsize = scr_size(0)
   ysize = scr_size(1)
   
   
   nfiles = N_ELEMENTS(flist)
   times = DBLARR(nfiles)
   ;window, 31, XSIZE = 200, YSIZE = 20, TITLE = 'Progress', XPOS = xsize / 2 - 100, $
   ;	      YPOS = ysize / 2 - 10
   
   IF debugon THEN help,nfiles
   FOR i=0, nfiles - 1 DO BEGIN
    	jk=lasco_readfits(flist[i],hdr,/no_img,silent=~debugon)      
      time = ANYTIM2TAI(hdr.date_obs+' '+hdr.time_obs)
      times(i) = time
      percent = float(i) / nfiles
      ;wset, 31
      ;POLYFILL, [0, percent, percent, 0], [0, 0, 1, 1], /NORMAL
   ENDFOR
   
   ;** ensure that the files are in time order
   ind = sort(times)
   times = times(ind)
   flist = flist(ind)
   
   ;** return to the previous version of this program (nrs 9/05/01)
   
   IF KEYWORD_SET(interval) THEN dtmin = interval ELSE dtmin = 45.0
   
       times = times/60.
       intervals = fltarr(nfiles)
       intervals(0) = 99999.
       i = 0
       j = 1
back:  dt = times(j) - times(i)
       if (dt gt dtmin) then begin
       intervals(j) = 99999.
       i = j
       j = j+1
       endif else begin
       intervals(j) = -99999.
       j = j+1
       endelse
       if (j le nfiles-1) then goto, back
   
   good = WHERE(intervals gt dtmin)
   new_list = flist(good)
   ;WDELETE, 31
   RETURN, new_list
END                                                                                                                                                                                                                                                            
