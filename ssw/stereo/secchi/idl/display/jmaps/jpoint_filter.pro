; $Id: jpoint_filter.pro,v 1.1 2008/03/26 17:49:50 nathan Exp $
;
; $Log: jpoint_filter.pro,v $
; Revision 1.1  2008/03/26 17:49:50  nathan
; moved from adam/programs, was dated Nov 7, 2007
;
PRO jpoint_filter, img1, a, b, c, img2
   img2 = img1 - MEDIAN(img1)
   point_filter2, img2, a, b, c, img2, o
   img2 = -TEMPORARY(img2)
   point_filter2, img2, a, b, c, img2, o
   img2 = -TEMPORARY(img2)
END
