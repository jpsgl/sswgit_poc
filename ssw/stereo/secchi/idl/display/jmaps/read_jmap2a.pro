FUNCTION read_jmap2a, filename, HEADER = header, REGION=region, PIXELS=pixels, NODATA=nodata
;
; $Id: read_jmap2a.pro,v 1.3 2011/10/20 16:32:43 nathan Exp $
;
; $Log: read_jmap2a.pro,v $
; Revision 1.3  2011/10/20 16:32:43  nathan
; use openr not openu
;
; Revision 1.2  2009/06/15 16:16:24  cooper
; modified widget, added satellite info
;
; Revision 1.1  2007/11/08 16:56:40  nathan
; moved from adam/jmaps2a
;
   header = create_jmap_strct2a()
     
   int_hdr_sz = -1
   flt_hdr_sz = -1
   dbl_hdr_sz = -1
   lon_hdr_sz = -1
   version = -1
   
   OPENr, lu, filename, /GET_LUN
   READU, lu, version
   READU, lu, dbl_hdr_sz, int_hdr_sz, lon_hdr_sz, flt_hdr_sz

   IF ((dbl_hdr_sz lt 0) OR (dbl_hdr_sz gt 100)) THEN BEGIN
      MESSAGE, 'Invalid .jmp file.  Unable to open ' + filename
      RETURN, -1
   ENDIF

;   hdr_string = string(replicate(32B,string_hdr_sz))
   hdr_dbl = DBLARR(dbl_hdr_sz)
   hdr_int = INTARR(int_hdr_sz)
   hdr_lon = LONARR(lon_hdr_sz)
   hdr_flt = FLTARR(flt_hdr_sz)
   readu, lu, hdr_dbl
   readu, lu, hdr_int
   readu, lu, hdr_lon
   readu, lu, hdr_flt
   hassat='Four'
   READU,lu,hassat
   header.hassat=hassat
   IF (hassat EQ 'yes~') THEN BEGIN
  	 sat='Four' ;number of characters in sat is number it reads..so tells it to read four chars into sat
   	readu, lu, sat
   	sat=strtrim(sat,0) ;remove trailing blanks
   	header.satellite=sat
   ENDIF ELSE BEGIN ;move pointer back one spot
   	position=fstat(lu)
	position=position.cur_ptr
	point_lun, lu, position-4B
   ENDELSE
   header.version = version
   header.filename = filename
   header.proj_type = hdr_int(0)
   header.pa = hdr_int(1)
   header.compressed = hdr_int(2)
   header.nreg = hdr_int(3)
   header.nx = hdr_lon(0)
   header.ny = hdr_lon(1)
   header.reg_dim = hdr_lon(2:21)
   header.border = hdr_lon(22:61)
   header.min_r = hdr_flt(0)
   header.max_r = hdr_flt(1)
   header.width = hdr_flt(2:11)
   
;   hdr_string = STRING(hdr_string)
   header.start_time = hdr_dbl[0]
   header.end_time = hdr_dbl[1]
   header.date_made = hdr_dbl[2]

   IF(keyword_set(NODATA)) THEN BEGIN
	close, lu
	free_lun, lu
	return, -1
   ENDIF

   pixels = bytarr(total(header.reg_dim[*,0]*header.reg_dim[*,1], /preserve_type))
   readu, lu, pixels
   curr_pix = 0
   IF(~keyword_set(REGION)) THEN BEGIN
	   map = bytarr(header.nx, header.ny)
	   FOR i=0,header.nreg-1 DO BEGIN
		n_pix = header.reg_dim[i,0]*header.reg_dim[i,1]
		curr_map = reform(pixels[curr_pix:curr_pix+n_pix-1], header.reg_dim[i,0], header.reg_dim[i,1])
		map[*,header.border[i,2]:header.border[i,3]-1] = curr_map[0,0]  ; fill with median brightness
		map[header.border[i,0]:header.border[i,1]-1, header.border[i,2]:header.border[i,3]-1] = $
			congrid(curr_map, header.border[i,1]-header.border[i,0], header.border[i,3]-header.border[i,2])
		undefine, curr_map
		curr_pix = curr_pix + n_pix
	   ENDFOR
   ENDIF ELSE BEGIN
	   map = 0
	   FOR i=0,region-1 DO BEGIN
		undefine, map
		n_pix = header.reg_dim[i,0]*header.reg_dim[i,1]
		map = pixels[curr_pix:curr_pix+n_pix-1]
		curr_pix = curr_pix + n_pix
	   ENDFOR
	   pixels = map
	   map = reform(temporary(map), header.reg_dim[region-1,0], header.reg_dim[region-1,1])
   ENDELSE
;stop
   CLOSE, lu
   FREE_LUN, lu
   RETURN, map
   
   END
   
