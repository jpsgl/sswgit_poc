PRO write_jmap2a, map, filename, hdr_strct
;+
; $Id: write_jmap2a.pro,v 1.8 2012/05/11 17:12:36 nathan Exp $
; NAME:
;	WRITE_JMAP
;
; PURPOSE:
;	Save a jMap in the .jmp file format.
;
; CATEGORY:
;	JMAP
;
; CALLING SEQUENCE:
;	WRITE_JMAP, Map, Filename, Hdr_strct
;
; INPUTS:
;	Map:	The map image.
;	Filename:	The file to which to save to map.
;	Hdr_strct:	A structure of the type made by
;	CREATE_JMAP_STRCT that contains map information.
;
;
;
; PROCEDURE:
;	Note that the map is BYTSCL'd before saving. 
;
; EXAMPLE:
;	WRITE_JMAP, MyMap, 'Jive.jmp', Header
;
; MODIFICATION HISTORY:
; 	Written by:	Jeff Walters, Aug 2001
; $Log: write_jmap2a.pro,v $
; Revision 1.8  2012/05/11 17:12:36  nathan
; finish previous fix
;
; Revision 1.7  2012/05/11 16:45:58  nathan
; use test_open2 instead of check_permission because latter did not test for ro file system
;
; Revision 1.6  2011/10/20 20:15:30  nathan
; use SAVE_DIR for output, and prompt for new directory if user does not have permission
;
; Revision 1.5  2011/02/14 23:25:44  nathan
; automatically append .jmp to filename if not there
;
; Revision 1.4  2009/12/07 18:14:00  nathan
; fix print sttmt
;
; Revision 1.3  2009/12/04 15:49:50  nathan
; automatically rename file if it already exists
;
; Revision 1.2  2009/06/15 16:16:25  cooper
; modified widget, added satellite info
;
; Revision 1.1  2007/11/08 16:56:43  nathan
; moved from adam/jmaps2a
;
;-
COMMON DIR_COMMON, mvi_dir, jmap_dir, save_dir, diff_dir, image_dir

    IF rstrmid(filename,0,3) NE 'jmp' THEN filename=filename+'.jmp'
   h = hdr_strct
;   STRING_HDR_SZ = 69 ;(characters)
   INT_HDR_SZ = 4 ;(integers)
   FLT_HDR_SZ = 12 ;(floats)
   DBL_HDR_SZ = 3 ;(doubles)
   LON_HDR_SZ = 62

   ;**
   ; Ensure that dates are all in proper format
   ;**
;   h.start_time = utc2str(anytim2utc(h.start_time),/ecs)
;   h.end_time = utc2str(anytim2utc(h.end_time),/ecs)
;   h.date_made = utc2str(anytim2utc(h.date_made),/ecs)
   h.start_time = anytim2tai(h.start_time)
   h.end_time = anytim2tai(h.end_time)
   h.date_made = anytim2tai(h.date_made)

   ;**
   ; Ensure that the following are all integers
   ;**
   h.proj_type = floor(h.proj_type)
   h.border = long(h.border)
   h.pa = floor(h.pa)
   h.compressed = floor(h.compressed)
   h.nx = long(h.nx)
   h.ny = long(h.ny)
   h.nreg = floor(h.nreg)
   h.reg_dim = long(h.reg_dim)
   h.width = float(h.width)
   sat=h.satellite
   while(strlen(sat) LT 4) do sat=sat+' '  ;add blanks to sat name until its length is four-if satellites with names longer than SOHO added, need to change
   h.satellite=sat
   ; test filename
    break_file,filename,vo,di,ro,su
    
    IF not test_open2(filename,/write) THEN BEGIN
    	
	newfile=concat_dir(save_dir,ro+su)
	IF not test_open2(newfile,/write) THEN $
    	    filename = DIALOG_PICKFILE(FILTER = '*.jmp', PATH = getenv('HOME'), FILE=ro+su) ELSE $
	    filename = newfile
	break_file,filename,vo,di,ro,su
	save_dir=di
    ENDIF

    if file_exist(filename) THEN BEGIN
    	message,filename+' exists...renaming.',/info
	wait,2
    	break_file,filename,vo,di,ro,su
	filename=vo+di+ro+'.0'+su
    endif
   OPENW, lu, filename, /GET_LUN
   WRITEU, lu, h.version, DBL_HDR_SZ, INT_HDR_SZ, LON_HDR_SZ, FLT_HDR_SZ, h.start_time, h.end_time, $
      h.date_made, h.proj_type, h.pa, h.compressed, h.nreg, h.nx, h.ny, h.reg_dim, h.border, $
      h.min_r, h.max_r, h.width, h.hassat, h.satellite, BYTSCL(map)
   CLOSE, lu
   FREE_LUN, lu

   spawn, 'chmod g+w ' + filename
   PRINT, 'File saved as ',filename,'.'

END
   
