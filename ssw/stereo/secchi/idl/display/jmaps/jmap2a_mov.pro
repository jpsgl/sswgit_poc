PRO jmap2a_mov, list
;read in the data

IF datatype(list) EQ 'UND' THEN $
list=DIALOG_PICKFILE(TITLE='Load jMaps', FILTER='*.jmp',/MULTIPLE_FILES)

map=read_jmap2a(list[0],header=h)

xsize=h.nx<1024
ysize=h.ny<512
map=congrid(map,xsize,ysize)
a=bytarr(xsize,ysize)
n=n_elements(list)
xinteranimate,set=[xsize,ysize,n], /showload
for i =0,n-1 do begin
  map=read_jmap2a(list[i])
  xinteranimate,frame=i,image=congrid(map,xsize,ysize)
endfor

xinteranimate,/keep_pixmaps


end
