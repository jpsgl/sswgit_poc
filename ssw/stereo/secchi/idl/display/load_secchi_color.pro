pro load_secchi_color,hdr, SILENT=SILENT, _EXTRA=_extra
;+
; $Id: load_secchi_color.pro,v 1.1 2010/11/17 22:49:54 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : 
;               
; Purpose   : This function loads color table depending on detector and wavlngth

; Use       : IDL> load_secchi_color, hdr
;    
; Inputs    : hdr -image header, either fits or SECCHI structure
;   	    	can be AIA or LASCO or EIT instr
;
; Outputs   : none
;
; Calls     : 
;
; Procedure : 
;               
; Category    : display
;               
; Prev. Hist. : None.
;
; Written     : N.Rich, NRL/I2 2010/11/16
;               
; $Log: load_secchi_color.pro,v $
; Revision 1.1  2010/11/17 22:49:54  nathan
; called from scc_mkframe.pro
;
;-            


IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

tel=hdr.detector
IF strmid(tel,0,1) EQ 'E' THEN prfx=trim(hdr.wavelnth)+'_' ELSE prfx=''
;IF tel EQ 'C2' THEN tel='HI1'
IF tel EQ 'C3' THEN tel='COR2'
IF tel EQ 'C1' THEN tel='COR1'
IF tel EQ 'EIT' or tel EQ 'AIA' THEN tel='EUVI'

IF tel EQ 'C2' THEN loadct,3 ELSE BEGIN
    color_table=prfx+tel+'_color.dat'

    path = concat_dir(getenv('SSW'),'stereo')
    RESTORE, FILEPATH(color_table ,ROOT_DIR=path,$
            SUBDIRECTORY=['secchi','data','color'])
    tvlct, r,g,b
ENDELSE

END
