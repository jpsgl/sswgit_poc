pro se_summary_images,dir,outdir=outdir
;
;+
; NAME:
;	SE_SUMMARY_IMAGES
;
; PURPOSE:
;	This procedure writes a series of gif image files of up to 25
;       browse images per gif image located in the specified directory
;
; CATEGORY:
;	UTIL
;
; CALLING SEQUENCE:
;	SE_SUMMARY_IMAGES,List
;
; OPTIONAL INPUTS:
;	dir:	If dir is not present, then the current directory is
;       	used
;               If dir is a string then it is the directory to be used
;               If dir is a string array then it is the list of filenames to be used
;
; KEYWORD PARAMETERS
;	OUTDIR:	If set, specifies the output directory to write the images to.
;		If not set, writes to the users' home directory
;	
; OUTPUTS:
;	A series of files are written.
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;	At this time, the page size, the number of images/page, and
;	the image annotation is coded into the routine.  
;
;	It is easy to add a branch point for different number of images
;	per page, i.e., 25, 36, etc.  It is not clear how to change
;	the annotation.
;
;       Assumes that all files in the directory are from the same date
;       Assumes that all files are in the standard form which begins
;       as:
;                : YYYYMMDD_HHMMSS
;
;
; PROCEDURE:
;	Make gifs that are 650 by 900.  Portrait mode
;
; EXAMPLE:
;
; MODIFICATION HISTORY:
; 	Written by:	M.D. Andrews, 18 Aug 1997
;	Modifications:
;	19 Oct 1998	RAH	use lasco_readfits, test for bad image
;       21 Jan 2005     RAH     modified for SECCHI TVAC images
;
;	%W% %H% SECCHI IDL LIBRARY
;-
window,retain=2,xsize=650,ysize=940,/free
loadct,0
tvlct,r,g,b,/get
;
IF (KEYWORD_SET(outdir)) THEN outputdir=outdir ELSE outputdir=getenv('HOME')
IF (outputdir NE '') THEN outputdir=outputdir+'/'
;
;   Test the three input cases
;        no input parameter
;        input parameter = string => pointer to directory
;        input parameter = string array => list of file names
;
np = N_PARAMS()
IF (np EQ 0)  THEN BEGIN
   CD,current=olddir
   cdir=olddir
   SE_GET_ALL_FITS,list
ENDIF ELSE BEGIN
   nn = N_ELEMENTS(dir)
   IF (nn EQ 1)  THEN BEGIN
      cd,dir,current=olddir
      cd,current=cdir
      SE_GET_ALL_FITS,list
  ENDIF ELSE BEGIN
      list = dir
      cd,current=olddir
      cdir=olddir
  ENDELSE
ENDELSE

nn = N_ELEMENTS(list)
IF (nn EQ 0)  THEN BEGIN
   PRINT,'No (FITS,FIT,FTS) files found in olddir
   RETURN
ENDIF
;
;  Enter number of frames/page and number or rows and collumns.
;
nf=25
nr=5
nc=5
;
np=(nn/nf)
;
;  Next for loops to make the plots.
;
	for i=0,np do begin
erase

;
		for j=0,nr-1 do begin
;
			for k=0,nc-1 do begin
;
index=i*nf + j*nc + k     ; Image Index
IF (j EQ 0)  THEN ifirst=1 ELSE ifirst=0
if index eq nn then goto, done
print,'Processing Image',index
;
;  Calculate coordinates for image and labels.
;
ix=k*130
iy=5 + (4-j)*170 + 40
y1=iy+103
y2=iy-20
y3=iy-40
;
;  Read image, resize, extract info from headers, plot and label.
;
wp = STRPOS(list(index),'.')
nwp = N_ELEMENTS(wp)
IF (wp(nwp-1) LT 15) THEN BEGIN
   img = -1
   std = 0
   name = list(index)
   camera = ''
ENDIF ELSE BEGIN
   img=readfits(list(index),h)
   std = 1
ENDELSE
IF (img(0) NE -1)  THEN BEGIN
   sz = SIZE(img)
   nx = sz(1)
   ny = sz(2)
   IF (nx GE ny)  THEN BEGIN
      ny = FIX(ny*(100./nx)) 
      nx = 100
      ENDIF ELSE BEGIN
      nx = FIX(nx*(100./ny))
      ny = 100
  ENDELSE
   az = CONGRID(img,nx,ny)
   mn = MIN(az)
   mx = MAX(az)
   IF ((mx-mn) GT 20)  THEN  img = HIST_EQUAL(az) $
                       ELSE  img = BYTSCL(az)
ENDIF ELSE img=bytarr(100,100)

IF (std EQ 1)  THEN BEGIN
   name=sxpar(h,'filename')
   camera = SXPAR(h,'BOARD')
ENDIF
IF (ifirst EQ 1) THEN BEGIN
   xyouts,10,900,'DIRECTORY: '+cdir,size=1.3,/device
   IF (std EQ 1)  THEN rootname=STRMID(name,0,9) ELSE rootname=''
   xyouts,10,870,'FILENAMES START WITH:  '+rootname,size=1.3,/device
   ifirst = 0
ENDIF
subname = STRMID(name,9)
IF (STRLEN(subname) LT 4)  THEN subname='['+name+']'
name=subname
;
IF (std EQ 1)  THEN tv,img,ix,iy,/device ELSE xyouts,ix,iy+50,'Not Processed',size=1.0,/device
;
xyouts,ix,y1,name,/device,size=1.5
xyouts,ix,y2,'CAM BOARD = '+STRING(camera,FORMAT='(i1)'),/device,size=1.2
;
		    endfor
;
	    endfor
;
done:
;
IF (rootname EQ '')  THEN BEGIN
   chrsep = PATH_SEP()
   wp = STR_SEP(cdir,chrsep)
   nwp = N_ELEMENTS(wp)
   rootname=wp(nwp-1)+'_'
ENDIF
out=outputdir+'sum_'+rootname+strtrim( string(i),2)+'.gif'
t=tvrd()
print,out
write_gif,out,t,r,g,b
;
      endfor
;
cd,olddir
return
;
end
