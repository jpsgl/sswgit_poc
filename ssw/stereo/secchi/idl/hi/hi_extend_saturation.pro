;+
;
; $Id: hi_extend_saturation.pro,v 1.2 2008/11/06 21:55:08 crothers Exp $
;
; Project: STEREO-SECCHI
;
;
; Name: hi_extend_saturation
;
; Purpose: Mask out extra columns around saturated columns to allow
;          for cases where bleeding has occured in the readout register
;
; Category: STEREO, SECCHI, HI
;
; Explanation: Bleeding can occur across columns in the readout register
;              if this happens then columns next to a saturated columns
;              will have excess photons subtracted in the shutterless
;              correction routine and therefore appear dimmer than they
;              should;  this will then be selected as the minima for 
;              background subtraction resulting in light streaks in
;              the background subtracted images.
;
; Syntax: hi_extend_saturation,im,ncols
;
; Example: hi_extend_saturation,im,ncols
;
; Inputs: data - 2d image
;         ncols - number of addition columns on each side of saturated
;
; Keywords: None
;
; Calls: None
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: Written 19 February 2008 by Steve Crothers 
;
; $Log: hi_extend_saturation.pro,v $
; Revision 1.2  2008/11/06 21:55:08  crothers
; fixed bug in finding unique values rather than unique index
;
; Revision 1.1  2008/02/19 15:10:16  crothers
; Initial version taking in new hi_prep with NAN's
;
;
;-
pro hi_extend_saturation,data,ncols

; find saturated columns by looking for columns where all rows masked

if ncols lt 1 then return

satcols=where(total(finite(data),2) eq 0,nsat)

if nsat eq 0 then return

sz=size(data)

; generate orthangonal matrix -ncol:-1 1:ncol and satcols and add
; to generate array of all possible indexes

i1=indgen(ncols)+1
index1=rebin(reform([i1,-i1],1,ncols*2),nsat,ncols*2)
index=rebin(satcols,nsat,ncols*2)+index1

; remove any out of bounds values and duplicates
 
w=where(index ge 0 and index lt sz[1],nrepl)
if nrepl eq 0 then return
index=index[w]
index=index[sort(index)]
index=index[uniq(index)]

; add extra columns to the masking data

data[index,*]=!values.f_nan

end
