pro scc_HICOSMICSplotall, START=start
;
;+
; NAME:
;	SCC_HICOSMICSPLOTALL
;
; PURPOSE:
;	This procedure generates standard plots of the cosmic scrubbing
;	algorithm after restoring the save sets.
;
; CATEGORY:
;	SECCHI DATA ANALYSIS
;
; CALLING SEQUENCE:
;	SCC_HICOSMICSPLOTALL
;
; INPUTS:
;	There are no inputs
;
; OPTIONAL INPUTS:
;	None
;	
; KEYWORD PARAMETERS:
;	START:  If present, gives an ascii string of the month&year to be
;		processed
;
; OUTPUTS:
;	The procedure writes PNG files to the same directory that the
;	save sets are in.  No other outputs are generated.
;
; PROCEDURE:
;	For each save set in the user's home directory that start with 
;	"cosmics" the data are restored and the plots generated.
;	The routine SCC_HICOSMICSPLOT is called.
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, 5 September 2007
;
;	%W% %H% SECCHI IDL LIBRARY
;-

f = FILE_SEARCH ('~/cosmics*.sav')
nf = N_ELEMENTS(f)
IF KEYWORD_SET(START) THEN BEGIN
   q = STRPOS(f,'cosmics_'+start)
   w = WHERE (q ne -1,nw)
   IF (nw eq 0)  THEN BEGIN
      PRINT,'ERROR: No cosmics save sets were found for '+start
      RETURN
   ENDIF ELSE BEGIN
      f = f(w)
      nf = nw
   ENDELSE
ENDIF
FOR jf=0,nf-1 DO BEGIN
    RESTORE,f(jf)
    nst = STRLEN(f(jf))
    fname = STRMID(f(jf),0,nst-4)
    nst = STRLEN(fname)
    fname = STRMID(fname,0,nst-12)+STRMID(fname,nst-3)
    tele = STRMID(f(jf),STRLEN(f(jf))-6,2)
    SCC_HICOSMICSPLOT,cosmscrub,time,tele
    np = N_ELEMENTS(time)
    start_mjd = time(0).mjd
    last_mjd = time(np-1).mjd
    q = TVRD()
    WRITE_PNG,fname+'.png',not q
    name_ext = '_w'+['1','2','3','4','5']+'.png'
    FOR i=0,4 DO BEGIN
        start_day = start_mjd+i*6
        w = WHERE (time.mjd GE start_day,nw)
        IF (nw GT 1)  THEN BEGIN
           nst = w(0)
           endday = start_day+5
           w = WHERE (time.mjd LE endday,nw)
           IF (nw GE 1)  THEN BEGIN
              nen = w(nw-1)
	      IF (nst LT nen)  THEN BEGIN
                 SCC_HICOSMICSPLOT,cosmscrub(*,nst:nen),time(nst:nen),tele
	         q = TVRD()
                 WRITE_PNG,fname+name_ext(i), not q
              ENDIF
           ENDIF
        ENDIF
    ENDFOR
ENDFOR
RETURN
END
