pro scc_hicosmicsread,startt,endt,tel,time,files,cosmics
;
;   
;
;   startt, endt = date string in format yyyymmdd
;   tel is the telescope 1A, 1B, 2A, 2B
;

;   array is a 2D array of the cosmics recovered from the images pointed to by
;   time is the time corresponding to each of the images (CDS Time structure)
;
;+
; NAME:
;	SCC_HICOSMICSREAD
;
; PURPOSE:
;	This procedure reads in all the HI image files for a date range,
;	extracts the cosmic ray scrubbing algorithm results and writes
;	out the date into a save set.
;
; CATEGORY:
;	SECCHI Data Analysis
;
; CALLING SEQUENCE:
;	SCC_HICOSMICSREAD,Startt,Endt,Tel,Time,Files,Cosmics

; INPUTS:
;	Startt:	A string giving the start date to process YYYYMMDD
;	Endt:	A string giving the end date to process YYYYMMDD
;	Tel:	A string giving the telescope, '1a','1b','1c','1d'
;
; OPTIONAL INPUTS:
;	None

;	
; KEYWORD PARAMETERS:
;	None
;
; OUTPUTS:
;	This procedure returns arrays of the image times, the file names
;	and the cosmics scrubbing array
;
; OPTIONAL OUTPUTS:
;	None
;
; COMMON BLOCKS:
;	None
;
; SIDE EFFECTS:
;	
;
; RESTRICTIONS:
;	
;
; PROCEDURE:
;	
;
; EXAMPLE:
;	
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, September 5, 2007.
;	19 Sep 2007	RAH.  Corrected for number of sums reported in individual
;			statistics not equal to that reported in the image header.
;
;	%W% %H% SECCHI IDL LIBRARY
;-
ttel=STRLOWCASE(tel)
hi = STRMID(ttel,0,1)
sc = STRMID(ttel,1,1)
IF (hi NE 1) AND (hi NE 2)  THEN BEGIN

    PRINT,'Invalid argument for tel:',tel,hi
    RETURN
ENDIF
IF (sc NE 'a') AND (sc NE 'b')  THEN BEGIN
    PRINT,'Invalid argument for tel:', tel,sc
    RETURN
ENDIF
CD,GETENV('secchi'),current=orig
CD,'lz/L0/'+sc+'/img/hi_'+hi
startu=YYMMDD2UTC(startt)


endu = YYMMDD2UTC(endt)
cosmscrub = LONARR(105)
GET_UTC,t
time=[t]
np = 0
FOR iday=startu.mjd,endu.mjd DO BEGIN	; Loop over each day
    t.mjd = iday
    date = UTC2YYMMDD(t,/yyyy)
    f = FILE_SEARCH(date,'*s4h*.fts')
    nf = N_ELEMENTS(f)
    IF (nf GT 0) THEN BEGIN
       PRINT,'Processing ',nf,' images on '+date
       FOR jf=0,nf-1 DO BEGIN
;
;  Put in some code to make sure that the file is OK
;  Found a file that had a header only with naxis=0
;
           ha = HEADFITS (f(jf),errmsg=errmsg,/silent)
	   sz = SIZE(ha)
	   IF (sz(0) EQ 1) THEN BEGIN
	      IF (FXPAR(ha,'NAXIS') EQ 2) THEN BEGIN
;
;  image is OK, so process	      
;
                 a = SCCREADFITS(f(jf),ha,/silent)
                 n_images = ha.n_images
                 nx = ha.naxis1
                 ny = ha.naxis2
                 IF (sc EQ 'a')  THEN BEGIN
                    aa = a(nx-n_images-1:*,ny-1)
                 ENDIF ELSE BEGIN
                    aa = REVERSE(a(0:n_images,0))
                 ENDELSE
                 wc = WHERE (aa LE n_images,nwc)
                 IF (np EQ 0)  THEN BEGIN
                    time = STR2UTC(ha.date_obs)
	            files = f(jf)
	            cosmscrub = LONARR(105)
                    cosmscrub(0) = n_images
                    cosmscrub(1) = ha.cosmics
                    IF (nwc GT 0)  THEN cosmscrub(2) = aa(wc(0):*)
                 ENDIF ELSE BEGIN
                    time = [time,STR2UTC(ha.date_obs)]
	            files = [files,f(jf)]
	            cosmscrub = REFORM(cosmscrub,105L*np)
   	            cosmscrub = [cosmscrub,LONARR(105)]
	            cosmscrub = REFORM(cosmscrub,105,np+1)
	            cosmscrub(0,np) = n_images
	            cosmscrub(1,np) = ha.cosmics
                    IF (nwc GT 0)  THEN cosmscrub(2,np) = aa(wc(0):*)
                 ENDELSE
                 np = np+1
              ENDIF
	   ENDIF
       ENDFOR
    ENDIF
ENDFOR
CD,orig
SAVE,cosmscrub,files,time,ttel,filename='~/cosmics_'+startt+'_'+endt+'_'+ttel+'.sav'
RETURN
END
