PRO SCC_HICOSMICSVIEW,Yearmonth,Tele
;
;+
; NAME:
;	SCC_HICOSMICSVIEW
;
; PURPOSE:
;	This procedure reads in the cosmics report and
;	displays the individual images in the report
;
; CATEGORY:
;	SECCHI DATA ANALYSIS
;
; CALLING SEQUENCE:
;	SCC_HICOSMICSVIEW
;
; INPUTS:
;	Yearmonth:  String giving the year and month to be processed ('YYYYMM')
;	Tele:	    String giving the telescope ('1a','1b','2a',2b')
;
; OPTIONAL INPUTS:
;	None
;	
; KEYWORD PARAMETERS:
;	None
;
; OUTPUTS:
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, 11 September 2007
;
;	%W% %H% SECCHI IDL LIBRARY
;-

f = FILE_SEARCH ('~/cosmics*report.txt')
nf = N_ELEMENTS(f)
q = STRPOS(f,'cosmics_'+yearmonth)
w = WHERE (q ne -1,nw)
IF (nw eq 0)  THEN BEGIN
   PRINT,'ERROR: No cosmics save sets were found for '+yearmonth
   RETURN
ENDIF ELSE BEGIN
   f = f(w)
   nf = nw
ENDELSE
tel = STRLOWCASE(tele)
pos = STRPOS (f,tele)
w = WHERE (pos NE -1,nw)
IF (nw EQ 0)  THEN BEGIN
   PRINT,'ERROR:  No cosmics save sets were found for '+yearmonth,tele
   RETURN
ENDIF ELSE BEGIN
   f = f(w)
   nf = nw
ENDELSE
;
;  Completed finding the filename of the desired report
;  Open it and and skip over the first two lines
;
ln = ''
OPENR,lua,f(0),/GET_LUN
FOR i=0,1 DO READF,lua,ln
;
;  Now determine the s/c
;
IF (STRMID(tel,1,1) EQ 'a')  THEN sc='a' ELSE sc='b'

;
;  Now change to the right directory
;
CD,getenv('secchi')+'/lz/L0/'+sc+'/img/hi_'+STRMID(tele,0,1),current=curr
;
;  Now process each of the lines in the report
;
fn=''
scrub=0L
nscrub=0
scrubpos=0
WHILE NOT EOF(lua) DO BEGIN
    READF,lua,typ,fn,ratio,scrub,scrubpos,nscrub,format='(i8,1x,a25,f13.5,i12,2i8)'
    PRINT,typ,fn,ratio,scrub,nscrub
    fn = STRMID(fn,0,8)+'/'+fn
    bad = SCCREADFITS(fn,hbad)
    WINDOW,xsize=1024,ysize=1024,/free,title=fn
    z = bad-SCC_GETBKGIMG(hbad)
    TV,HIST_EQUAL(z)
;    TV,HIST_EQUAL(CONGRID(z(512:*,512:*),1024,1024))
;    TV,CONGRID(HIST_EQUAL(bad-SCC_GETBKGIMG(hbad)),512,512),0
;    TV,CONGRID(HIST_EQUAL(bad-SMOOTH(bad,91)),512,512),1
    READ,fn
    IF (fn EQ 'z')  THEN BEGIN
       FREE_LUN,lua
       RETURN
     ENDIF
ENDWHILE
RETURN
END
