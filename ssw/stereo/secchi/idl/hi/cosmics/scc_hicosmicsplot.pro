PRO SCC_HICOSMICSPLOT,cosmscrub,time,tele
;
;  plots the cosmic counts produced by scc_hicosmicsread
;  with the range within the summing plotted as error bars
;
;+
; NAME:
;	SCC_HICOSMICSPLOT
;
; PURPOSE:
;	This procedure generates standard plots of the cosmic scrubbing
;	algorithm results
;
; CATEGORY:
;	SECCHI Data Analysis
;
; CALLING SEQUENCE:
;	SCC_HICOSMICSPLOT,Cosmics,Time,tele
;
; INPUTS:
;	Cosmics:  A 2-D array returned by SCC_READHICOSMICS that contains
;		the cosmic ray scrubbing for each image
;	Time:	An array of image times in the CDS time structure
;
; OPTIONAL INPUTS:
;	Tele:  If present then the telescope is written as a label
;	
; KEYWORD PARAMETERS:
;	None
;
; OUTPUTS:
;	Generates a plot on the screen
;
; OPTIONAL OUTPUTS:
;	None
;
; COMMON BLOCKS:
;	None
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;	None
;
; PROCEDURE:
;	First plots the average number of scrubbed pixels
;	Then overplots the minimum and maximum of the scrubbed pixels in 
;	each of the summed images as error bars.
;
; EXAMPLE:
;	SCC_HICOSMICSREAD,'20070501','20070531','1a',time,files,cosmics
;	SCC_HICOSMICSPLOT,Cosmics,Time,'1a'
;	This will generate a single plot for the entire month.
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, 5 September 2007
;	5 Sep 2007	Initial Release
;
;	%W% %H% SECCHI IDL LIBRARY
;-

sz=SIZE(cosmscrub)
np=sz(2)
avg = FLTARR(np)
;avg=REFORM(cosmscrub(1,*))
upper=FLTARR(np)
lower=upper
FOR i=0,np-1 DO BEGIN
    w = WHERE(cosmscrub(4:*,i) GT 0,nw)
    IF (nw GT 0)  THEN BEGIN
       avg(i) = AVERAGE(cosmscrub(w,i))
       upper(i) = MAX(cosmscrub(4+w,i))
       lower(i) = MIN(cosmscrub(4+w,i))
       IF (upper(i) EQ 0) THEN upper(i)=avg(i)
       IF (lower(i) EQ 0)  THEN lower(i)=avg(i)
    ENDIF
ENDFOR
w = WHERE(avg GT 0)
ymax = 2*MEDIAN(cosmscrub(3,w))
ymin=min(lower(w))
IF (N_PARAMS() EQ 2)  THEN tel='' ELSE tel=' for HI-'+STRUPCASE(tele)
!y.title='Number of Scrubbed Pixels'+tel
UTPLOT,time(w),avg(w),yrange=[0,1.5e5],psym=10,charsize=2.
!y.title=''
UTERRPLOT,time(w),upper(w),lower(w)
OUTPLOT,time(w),REFORM(cosmscrub(3,w)),psym=1
RETURN
END
