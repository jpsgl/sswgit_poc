FUNCTION SCC_HICOSMICSREPORT,Saveset,THRESHOLD=threshold
;+
; NAME:
;	SCC_COSMICSREPORT
;
; PURPOSE:
;	This procedure generates a report of the cosmic scrubbing
;	and writes it to a file.
;
; CATEGORY:
;	SECCHI DATA ANALYSIS
;
; CALLING SEQUENCE:
;	SCC_COSMICSREPORT,Saveset
;
; INPUTS:
;	None
;
; OPTIONAL INPUTS:
;	Saveset:  If present then a single saveset is processed, otherwise
;		all of the cosmics savesets in the user's home directory 
;		are processed
;	
; KEYWORD PARAMETERS:
;	None
;
; OUTPUTS:
;	Nothing gets returned to the calling program.  A file gets written
;	to the same directory as the saveset.
;
; OPTIONAL OUTPUTS:
;	None
;
; COMMON BLOCKS:
;	None
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;	None
;
; PROCEDURE:
;	Restores the saveset pointed to by the program argument and looks
;	for (1) average scrubbed values that are 2x the 51 point median 
;	smoothed value and (2) Individual scrubbed values that exceed 2x the
;	average value.
;
; EXAMPLE:
;	
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard, September, 2007.
;	2007 Sep 7	Initial Release
;
;	%W% %H% SECCHI IDL LIBRARY
;-
f = FILE_SEARCH('~/cosmics_*.sav')
IF (N_PARAMS() EQ 1)  THEN f = [saveset]
nf = N_ELEMENTS(f)
IF KEYWORD_SET(THRESHOLD)  then thresh=threshold ELSE thresh=2.
FOR jf=0,nf-1 DO BEGIN
    RESTORE,f(jf)
    nst = STRLEN(f(jf))
    fname = STRMID(f(jf),0,nst-4)
    nst = STRLEN(fname)
    fname = STRMID(fname,0,nst-12)+STRMID(fname,nst-3)
    OPENW,lu,fname+'_report.txt',/GET_LUN
    PRINTF,lu,' Analysis of File:  '+f(jf)
    PRINTF,lu,'     Type       FILENAME                Ratio       ', $
          'MaxScrub    PosMax   Nscrub>'
    tele = STRMID(f(jf),STRLEN(f(jf))-6,2)
;
;   Position 1 is the average value reported in the header.
;   Because that number isn't (wasn't) always correct recompute the header
;   from the individual scrubbing counts.
;
;    avg = REFORM(cosmscrub(1,*))
    np = N_ELEMENTS(files)
    avg = FLTARR(np)
    FOR i=0,np-1 DO BEGIN
        w = where(cosmscrub(4:*,i) GT 0,nw)
        IF (nw GT 0)  THEN avg(i)=AVERAGE(cosmscrub(4+w,i))
    ENDFOR
;
;   Find those images where the averages are greater than 2x normal
;   And those images where the individual scrubbing count is 2x average
;   And those images where there are no scrubbing numbers
;
    medavg = FLOAT(MEDIAN(avg,51))
    ratio = avg/medavg
    w = WHERE (ratio GT thresh,nw)
    iw = 0
    badfiles = [' ']
    FOR ip=0,np-1 DO BEGIN
        type = 0
        IF (w(iw) EQ ip)  THEN BEGIN
	   type=1
	   IF (iw LT nw-1)  THEN iw = iw+1
	ENDIF
	maxscrub=MAX(cosmscrub(4:*,ip))
	IF (maxscrub GT thresh*avg(ip))  THEN type=type+2
	IF (avg(ip) EQ 0)  THEN type=type+4
	ww = WHERE(cosmscrub(4:*,ip) GT thresh*avg(ip),nww)
        IF (type GT 0)  THEN BEGIN
	   www = WHERE(cosmscrub(4:*,ip) EQ maxscrub)
	   IF (type EQ 4) THEN PRINTF,lu,type,' '+FILE_BASENAME(files(ip)) $
	   ELSE $
	   PRINTF,lu,type,' '+FILE_BASENAME(files(ip)) $
	   ,ratio(ip),maxscrub,FIX(www(0)),FIX(nww)
	   badfiles=[badfiles,files(ip)]
	ENDIF
    ENDFOR
    FREE_LUN,lu
ENDFOR
PRINT,'Done.  Ignore following error message'
RETURN,badfiles(1:*)
END
