;+
; $Id: min_filter.pro,v 1.2 2008/04/23 09:52:56 bewsher Exp $
;
; Name: min_filter
;
; Purpose: Determines and subtracts a minimum background from the data
;
; Category: STEREO, SECCHI, HI, Background
;
; Explanation: Calculates a minimum background from the data entered
; into the code. Then subtracts this minimum background from each
; image.
;
; Syntax: new_data = min_filter(data,base=base)
;
; Inputs: data - data cube
;
; Opt. Inputs: None
;
; Outputs: base - background which has been calculated and subtracted
;
; Opt. Outputs: None
;
; Keywords: None
;
; Calls: None
;
; Common: None
;
; Restrictions: None
;
; Side effects: None
;
; $Log: min_filter.pro,v $
; Revision 1.2  2008/04/23 09:52:56  bewsher
; Added /nan keyword to call to min - and altered so that data and background subtracted data are both returned from code
;
; Revision 1.1  2007/11/20 22:37:47  nathan
; from D.Bewsher via J.Morrill via J.Hall
;
;
; History: Version 2, 12-09-07, added documentation, DB
;          Version 1, Jan-2007, Danielle Bewsher (RAL) 
;
; Contact: Danielle Bewsher (d.bewsher@rl.ac.uk)
;-
;

FUNCTION min_filter,data,base=base

;ignore any pixels with intensity <=0 in the determination of the
;minimum background
ii = where(data le 0.)
IF (ii(0) ne -1) THEN data(ii) = max(data)*10.
;calculate minimum background
base = min(data,dimension=3,/nan)

dsz = size(data)
;;take pixel to pixel minimum of data
;base = fltarr(dsz(1),dsz(2))*0.-1.
;FOR i=0,dsz(1)-1 DO BEGIN
;  FOR j=0,dsz(2)-1 DO BEGIN
;    tmp = data(i,j,*)
;    ii = where(tmp ge 0)    
;    IF (ii(0) ne -1) THEN base(i,j) = min(tmp(ii))
;  ENDFOR
;ENDFOR

ii = where(base eq -1.,nii)
IF (ii(0) ne -1) THEN BEGIN
  base2 = fltarr(dsz(1)+2,dsz(2)+2)
  base2(1:dsz(1),1:dsz(2)) = base
  FOR i=0,nii-1 DO BEGIN
    y = fix(ii(i)/dsz(1))
    x = ii(i) - (y*dsz(1))
    ;take into account ghost cells
    y = y+1
    x = x+1

    tmp = base2(x-1:x+1,y-1:y+1)
    base2(x,y) = min(tmp)
  ENDFOR
  base = base2(1:dsz(1),1:dsz(2)) 
ENDIF 

;subtract minimum background from each image in turn
new_data = data
FOR i=0,dsz(3)-1 DO new_data(*,*,i) = data(*,*,i) - base

return,new_data

END

