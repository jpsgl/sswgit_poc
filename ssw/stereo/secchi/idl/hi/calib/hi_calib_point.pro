;+
; $Id: hi_calib_point.pro,v 1.7 2015/12/22 12:31:15 crothers Exp $
;
; Project: STEREO-SECCHI
;
; Name: hi_calib_point
;
; Purpose: To fix HI headers with calibrated pointing information
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation: This uses the s/c pointing and the latest HI
;              calibration information to update the header structures
;              with the best HI pointing information.
;
; Syntax: hi_calib_point,index,/nominal
;
; Example: hi_calib_point, index
;
; Inputs: index - one HI header structure
;
; Opt. Inputs: None
;
; Outputs: index - one HI header structure containing calibrated
;                  pointing information
;
; Opt. Outputs: None
;
; Keywords: None
;
; Calls: hi_calib_roll, fov2radec, get_hi_params
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: 15-Oct-2007, Daniel Brown and Danielle Bewsher
;
; Contact: Daniel Brown (dob@aber.ac.uk) and 
;          Danielle Bewsher (d.bewsher@rl.ac.uk)
;
;
; 2015/12/18 tappin
;   Correct interpretation of PCi_j parameters. GET_HI_ROLL also
;   corrected. 
;
; $Log: hi_calib_point.pro,v $
; Revision 1.7  2015/12/22 12:31:15  crothers
; updates from James Tappin for correct handling of data both before and after conjunction.
;
; Revision 1.6  2013/11/14 00:26:59  nathan
; always use square images based on index.summed, if present
;
; Revision 1.5  2008/03/14 14:26:54  bewsher
; Corrected direction of ins_* keywords
;
; Revision 1.4  2008/03/03 12:21:45  bewsher
; Corrected update to CROTA keyword
;
; Revision 1.3  2008/02/27 13:01:44  bewsher
; Added call to update crota keyword
;
; Revision 1.2  2007/12/14 13:19:38  bewsher
; Added _EXTRA keyword to this and all sub routines called so that HI_NOMINAL keyword can propagate
;
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-
pro hi_calib_point,index,_extra=extra

; sort out total HI roll keywords
; first in gei
roll=hi_calib_roll(index, system='gei',_extra=extra)
index.pc1_1a = cos(roll*!dpi/180.)
index.pc1_2a = -sin(roll*!dpi/180.)
index.pc2_1a = sin(roll*!dpi/180.)
index.pc2_2a = cos(roll*!dpi/180.)

; now in hpc
roll=hi_calib_roll(index, system='hpc',_extra=extra)

index.crota = -roll
index.pc1_1 = cos(roll*!dpi/180.)
index.pc1_2 = -sin(roll*!dpi/180.)
index.pc2_1 = sin(roll*!dpi/180.)
index.pc2_2 = cos(roll*!dpi/180.)

; sort out pointing in RA/DEC
IF tag_exist(index,'summed') THEN BEGIN
    naxis1=2048/2^(index.summed-1)
    naxis2=naxis1
ENDIF ELSE BEGIN
    naxis1=float(index.naxis1)
    naxis2=float(index.naxis2)
ENDELSE
IF naxis1 LE 0 THEN naxis1=1024.	; for header-only case
IF naxis2 LE 0 THEN naxis2=1024.
xv=[0.5*naxis1,naxis1]
yv=[0.5*naxis2,0.5*naxis2]

; first for gei
radec=fov2radec(xv,yv,index, system='gei',_extra=extra)

index.crval1a = radec(0,0)
index.crval2a = radec(1,0)

; next for hpc
radec=fov2radec(xv,yv,index, system='hpc',_extra=extra)

index.crval1 = -radec(0,0)
index.crval2 = radec(1,0)

; sort out mu parameter
get_hi_params,index,pitch_hi,offset_hi,roll_hi,mu,d,_extra=extra
index.pv2_1a = mu
index.pv2_1 = mu

; sort out platescale parameters. 
fp = fparaxial(d,mu,index.naxis1,index.naxis2,fp_mm=fp_mm,plate=plate)

; first for gei
if (index.cunit1a eq 'deg') then begin
   xsize=plate/3600.
endif
if (index.cunit2a eq 'deg') then begin
   ysize=plate/3600.
endif

if (index.cunit1a eq 'arcsec') then begin
   xsize=plate
endif
if (index.cunit2a eq 'arcsec') then begin
   ysize=plate
endif

index.cdelt1a = -xsize
index.cdelt2a = ysize

; now for hpc
if (index.cunit1 eq 'deg') then begin
   xsize=plate/3600.
endif
if (index.cunit2 eq 'deg') then begin
   ysize=plate/3600.
endif

if (index.cunit1 eq 'arcsec') then begin
   xsize=plate
endif
if (index.cunit2 eq 'arcsec') then begin
   ysize=plate
endif

index.cdelt1 = xsize
index.cdelt2 = ysize

; yaw, pitch and roll of HI keywords (sort of) fixed

index.ins_x0 = -offset_hi
index.ins_y0 = pitch_hi
index.ins_r0 = -roll_hi

end
