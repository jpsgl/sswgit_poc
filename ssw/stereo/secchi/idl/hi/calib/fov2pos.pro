;+
; $Id: fov2pos.pro,v 1.7 2015/12/22 12:31:15 crothers Exp $
;
; Project: STEREO-SECCHI
;
; Name: fov2pos
;
; Purpose: To convert HI pixel positions to solar plane of sky
;          coordinates in units of AU (actually, not quite, 1 is
;          defined as the distance from the S/C to the Sun) and 
;          the Sun at (0,0)
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation: HI pixel position is converted to general AZP, then to
;              Cartesian coordinates in the HI frame of
;              reference. This is then rotated to be in the S/C frame
;              and finally to the end reference frame. The final frame
;              is a left handed Cartesian system with x into the
;              screen (towards the reference point), and z pointing up.
;
; Syntax: xy = fov2pos(x, y, index)
;
; Example: xy = fov2pos(512,512,index)
;
; Inputs: x     - an n-element array of x pixel positions
;         y     - an n-element array of y pixel positions
;         index - the fits header for the HI image in question. This is 
;                 used to deduce the pointings and instrument - etc.
;
; Opt. Inputs: None
;
; Outputs: xy    - a 4xn array of (x,y,z,w) quadruplets, where x,y,z
;                  have the meaning described above and w is a scale
;                  factor (see '3D computer graphics' by Alan Watt for
;                  further details). The w column can be discounted
;                  for almost all user applications.
;
; Opt. Outputs: None
;
; Keywords: system - which coordinate system to work in 'hpc' or 'gei'
;
; Calls: azp2cart, hi2sc, sc2cart, get_hi_params
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: 27-Jun-2007, Daniel Brown
;
; Contact: Daniel Brown (dob@aber.ac.uk) 
;
;  2015/12/18  tappin
;  Back out Nathan's post conjunction fix, correct central pixel.
;
; $Log: fov2pos.pro,v $
; Revision 1.7  2015/12/22 12:31:15  crothers
; updates from James Tappin for correct handling of data both before and after conjunction.
;
; Revision 1.6  2015/07/21 15:12:48  nathan
; change value of roll for post-conjunction case
;
; Revision 1.5  2008/02/14 21:35:20  secchib
; nr - use 1024 for naxis if zero
;
; Revision 1.4  2008/01/16 14:34:22  bewsher
; Corrected documentation
;
; Revision 1.3  2008/01/04 21:29:58  colaninn
; changed fltarr to dblarr
;
; Revision 1.2  2007/12/14 13:21:16  bewsher
; Added _EXTRA keyword
;
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-
function fov2pos, xv, yv, index, $
                  yaw = yaw, pitch = pitch, roll = roll, $
                  hi_offsetx = offset_hi, hi_offsety = pitch_hi, $
                  hi_roll = roll_hi, mu = mu, fov = d, $
                  ccd_offsetx = ccdosx, ccd_offsety = $
                  ccdosy, pixmult = pmult, system = sys, _extra = extra

  if (n_elements(sys) eq 0) then sys = 'hpc'

  if (sys eq 'hpc') then begin
     if (n_elements(yaw) eq 0) then yaw = index.sc_yaw
     if (n_elements(pitch) eq 0) then pitch = index.sc_pitch
     if (n_elements(roll) eq 0) then roll = -index.sc_roll
     
  endif else begin
     if (n_elements(yaw) eq 0) then yaw = index.sc_yawa
     if (n_elements(pitch) eq 0) then pitch = index.sc_pita
     if (n_elements(roll) eq 0) then roll = index.sc_rolla
  endelse

  if (n_elements(ccdosx) eq 0) then ccdosx = 0.
  if (n_elements(ccdosy) eq 0) then ccdosy = 0.

  naxis = double([index.naxis1, index.naxis2])
  locs = where(naxis eq 0, nz)
  if nz ne 0 then naxis[locs] = 1024.
  if (n_elements(pmult) eq 0) then pmult = naxis/2.0 - 0.5

  get_hi_params, index, pitch_hi, offset_hi, roll_hi, mu, d, _extra = extra

  ang = (90. - 0.5*d)*!dpi/180.
  rng = (1.0+mu)*cos(ang)/(sin(ang)+mu)

  vfov = transpose([[reform(xv)], [reform(yv)]])

  nst = n_elements(vfov(0, *))

  vv4 = dblarr(4, nst)
  vv4(0, *) = ((vfov(0, *)-ccdosx)/pmult[0] - 1.0)*rng
  vv4(1, *) = ((vfov(1, *)-ccdosy)/pmult[1] - 1.0)*rng
  vv4(2, *) = 1.0
  vv4(3, *) = 1.0

  vv3 = azp2cart(vv4, mu)
  vv3(2, *) = 1.0

  vv2 = hi2sc(vv3, roll_hi, pitch_hi, offset_hi)

  vv = sc2cart(vv2, roll, pitch, yaw)
  return, vv
end
