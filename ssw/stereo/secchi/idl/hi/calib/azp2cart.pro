;+
; $Id: azp2cart.pro,v 1.1 2007/11/28 12:05:53 bewsher Exp $
;
; Project: STEREO-SECCHI
;
; Name: azp2cart
;
; Purpose: To convert points seen with an AZP projection with
;          parameter, mu to a Cartesian frame. Note, this is a low level code,
;          and would usually not be called directly. 
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation: For details, see "Coordinate systems for solar image
;              data", W.T. Thompson, A&A
;
; Syntax: out = azp2cart(vec,mu)
;
; Example:
;
; Inputs: vec - (3xN) array of vector postions to transform
;         mu - HI distortion parameter
;
; Opt. Inputs: None
;
; Outputs: xy    - a 2xn array of transformed vector positions
;
; Opt. Outputs: None
;
; Keywords: None
;
; Calls: None
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: 27-Jun-2007 by Daniel Brown
;          15-Oct-2007, added if statement, DOB
;
; Contact: Daniel Brown (dob@aber.ac.uk) 
;
; $Log: azp2cart.pro,v $
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-
function azp2cart,vec,mu

nstars=n_elements(vec(0,*))

vout=vec

for i=0,nstars-1 do begin
    rth = sqrt(vec(0,i)^2 + vec(1,i)^2)
    rho = rth/(mu+1.0)
    cc = sqrt(1.0 + rho^2)
    th = acos(1.0/cc) + asin(mu*rho/cc)
    zz = cos(th)
    rr = sin(th)
    IF (rth lt 1.0e-6) THEN BEGIN
      vout(0:1,i) = rr*vec(0:1,i)
    ENDIF ELSE BEGIN
      vout(0:1,i) = rr*vec(0:1,i)/rth
    ENDELSE
    vout(2,i) = zz
endfor

return,vout
end
