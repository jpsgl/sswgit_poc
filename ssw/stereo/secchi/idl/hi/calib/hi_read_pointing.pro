;+
; $Id: hi_read_pointing.pro,v 1.2 2008/07/30 12:26:13 bewsher Exp $
;
; Project: STEREO-SECCHI
;
; Name: HI_READ_POINTING
;
; Purpose:  To read in the pointing information from the appropriate 
;           pnt_HI??_yyyy-mm-dd_fix_mu_fov.fts file 
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation:
;
; Syntax: hd = hi_read_pointing(file)
;
; Example: hd = hi_read_pointing('$SCC_DATA/hi/pnt_HI1A_2008-07-24_fix_mu_fov.fts')
;
; Inputs: file - HI pointing calibration fits file
;
; Optional Inputs: None
;
; Outputs: hd - HI pointing calibration fits file header structure
;
; Optional Outputs: None
; 
; Keywords: template - pass a template structure for the program to
;                      use. Useful if you want to read in multiple
;                      pointing files and concatenate the results (so
;                      you would use one element of the returned
;                      structure array from the first file read as the
;                      template for reading other pointing files). 
;
; Calls: fits_info, headfits, fitshead2struct, anytim2tai
;
; Common: None
;
; Restrictions: None
;
; Side effects: None
;
; Previous History: 17/04/2008 Written by Daniel Brown, Aberystwyth University
;                   24/07/2008 Added sort to heads to put them in
;                              chronological order, Danielle Bewsher, RAL
;
; Contact: Daniel Brown (dob@aber.ac.uk) and Danielle Bewsher (d.bewsher@rl.ac.uk)
;
; $Log: hi_read_pointing.pro,v $
; Revision 1.2  2008/07/30 12:26:13  bewsher
; First release of code to read pointing calibration files
;
;-

FUNCTION hi_read_pointing, file, template=tmpl

; determine existance of pointing file
ff = file_search(file,count=exists)

IF (exists GT 0) THEN BEGIN
   ; get number of FITS extensions from file
   fits_info,file,N_ext=nxt,/silent

   IF (nxt GE 1) THEN BEGIN
      ; read first extension
      h1 = headfits(file, exten=1, /silent)

      ; correct date-avg to date_avg terminology
      aa=where(strmid(h1, 0, 8) EQ 'DATE-AVG' OR strmid(h1, 0, 8) EQ 'date-avg', naa)
      IF (naa NE 0) THEN BEGIN
         FOR i=0,naa-1 DO BEGIN
            dum = h1(aa(i))
            strput, dum, 'DATE_AVG', 0
            h1(aa(i)) = dum
         ENDFOR
      ENDIF

      ; convert header to IDL structure, using template if appropriate
      IF (n_elements(tmpl) EQ 0) THEN BEGIN
         stc = fitshead2struct(h1)
         tmpl = stc
      ENDIF ELSE BEGIN
         stc = fitshead2struct(h1, template=tmpl)
      ENDELSE
      
      ; read in other extensions if they exist
      IF (nxt GE 2) THEN BEGIN
         heads = replicate(stc, nxt)
         FOR i=2,nxt DO BEGIN
            h1 = headfits(file, exten=i, /silent)

            ; date-avg becomes date_avg
            aa=where(strmid(h1, 0, 8) EQ 'DATE-AVG' OR strmid(h1, 0, 8) EQ 'date-avg', naa)
            IF (naa NE 0) THEN BEGIN
               FOR j=0,naa-1 DO BEGIN
                  dum = h1(aa(j))
                  strput, dum, 'DATE_AVG', 0
                  h1(aa(j)) = dum
               ENDFOR
            ENDIF
            ; convert to IDL structure
            heads(i-1) = fitshead2struct(h1, template=tmpl)
         ENDFOR
      ENDIF ELSE BEGIN
         heads = stc
      ENDELSE
   ENDIF ELSE BEGIN
      ; error code if no FITS extensions found
      heads = -998.0
   ENDELSE
ENDIF ELSE BEGIN 
   ; error code if file not found
   print,'File not found'
   heads = -999.0
ENDELSE

;sort the entries in the pnt file into chronological order
IF (datatype(heads) eq 'STC') THEN BEGIN
    tim = heads.date_avg
    tim_tai = anytim2tai(tim)
    ss = sort(tim_tai)
    heads = heads(ss)
ENDIF

RETURN, heads
END
