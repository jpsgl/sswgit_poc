;+
; $Id: hi_fix_pointing.pro,v 1.10 2013/11/14 00:28:24 nathan Exp $
;
; Project: STEREO-SECCHI
;
; Name: HI_FIX_POINTING
;
; Purpose:  To read in the pointing information from the appropriate 
;           pnt_HI??_yyyy-mm-dd_fix_mu_fov.fts file and update the
;           supplied HI index with the best fit pointing information 
;           and optical parameters calculated by the minimisation 
;           process of Brown, Bewsher and Eyles (2008).
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation:
;
; Syntax: hi_fix_pointing, index
;
; Example: hi_fix_pointing,index
;
; Inputs: index - HI header structure(s)
;
; Optional Inputs: None
;
; Outputs: index - HI header structures(s)
;
; Optional Outputs: None
; 
; Keywords: path - directory path to the location of the pointing
;                  files. Will use location in the $SCC_DATA environment
;                  variable otherwise.
;           ravg - if ravg le 5, then pointing is updated. If better
;                  accuracy is needed then use this keyword to
;                  specify criteria for ravg 
;
; Calls: hi_calib_point, rd_fits_ext
;
; Common: None
;
; Restrictions: None
;
; Side effects: None
;
; Previous History: 17/04/2008 Written by Daniel Brown, Aberystwyth University
;                   23/04/2008 Added tvary keyword, Danielle Bewsher (DB), RAL
;                   15/07/2008 Enabled ravg to be recorded in header, DB
;                   17/07/2008 Updated for use with pnt files in ssw
;                              tree, DB
;
; Contact: Daniel Brown (dob@aber.ac.uk) and Danielle Bewsher (d.bewsher@rl.ac.uk)
;
; $Log: hi_fix_pointing.pro,v $
; Revision 1.10  2013/11/14 00:28:24  nathan
; accomodate subfield images
;
; Revision 1.9  2013/06/12 16:44:17  nathan
; do not use subscripted array is output var to procedure
;
; Revision 1.8  2011/09/21 20:47:46  nathan
; ensure that date for filename is correctly formatted
;
; Revision 1.7  2009/11/13 20:02:34  mcnutt
; added common block to track current hipoint file read and will select row from date_avg or filename
;
; Revision 1.6  2008/10/14 18:24:26  nathan
; added info msg if not /silent
;
; Revision 1.5  2008/10/08 14:16:47  mcnutt
; Takes into account naxis size from secchi_prep
;
; Revision 1.4  2008/08/28 08:15:58  crothers
; removed spurious dollar sign in getenv
;
; Revision 1.3  2008/08/22 09:15:15  crothers
; Change path code to use getenv and path_sep for portability
;
; Revision 1.2  2008/07/30 12:25:12  bewsher
; Added minor changes to printed output statements
;
;-


PRO hi_fix_pointing,indices,path=path,ravg=ravg,tvary=tvary, SILENT=silent

common current_hipoint ,hipoint,hipointfile

if datatype(hipointfile) eq "UND" then hipointfile=''

; determine directory containing pointing files
dir = getenv('SCC_DATA')+path_sep()+'hi'+path_sep()
IF (n_elements(path) eq 1) THEN dir = path + '/'
ni = n_elements(indices)

; set up ravg quality parameter
rtmp = 5.
IF KEYWORD_SET(ravg) THEN rtmp = ravg

; iterate through each header structure
FOR i=0, ni-1 DO BEGIN
   ; determine file name
   yymmdd=utc2str(anytim2utc(indices[i].date_avg),/date_only)
   IF KEYWORD_SET(tvary) THEN BEGIN
      fle = dir + 'pnt_' + indices(i).detector + strmid(indices(i).obsrvtry,7,1) + '_' + yymmdd + '.fts'
   ENDIF ELSE BEGIN
      fle = dir + 'pnt_' + indices(i).detector + strmid(indices(i).obsrvtry,7,1) + '_' + yymmdd + '_fix_mu_fov.fts'         
   ENDELSE

   ; determine existance of file
;   file = file_search(fle,count=exists)
   IF fle ne hipointfile THEN begin
      IF ~keyword_set(SILENT) THEN message,'Reading '+fle,/info
      hipoint = hi_read_pointing(fle)
      hipointfile=fle
   endif

;   IF (exists eq 0) THEN BEGIN
   IF (datatype(hipoint) ne "STC") THEN BEGIN
      ; flag for file not found
      print, 'No pointing calibration file found for file ', indices(i).filename
      print, 'Using calibrated fixed instrument offsets'
      index=indices[i]
      hi_calib_point,index
      
      index.ravg = -881.
      indices[i]=index
   ENDIF ELSE BEGIN
      IF ~keyword_set(SILENT) THEN message,'Using '+hipointfile,/info
      ; search for correct FITS extension

       ec=where(hipoint.extname eq indices(i).date_avg)
       if ec(0) eq -1 then ec=where(strmid(hipoint.filename,0,16) eq strmid(indices(i).filename,0,16))

       ;rd_fits_ext, file=file(0), extname=indices(i).date_avg, header=head, status=stat, err_code=ec, err_msg=em,/silent
       ;IF (ec EQ 0) THEN BEGIN
       IF (ec gt -1) THEN BEGIN
         ; convert header to IDL structure
         stc = hipoint(ec)  ;fitshead2struct(head)
	 stcravg=stc.ravg
	 stcnst1=stc.nst1
	 
	 if indices[i].naxis1 ne 0. then sumdif=round(indices[i].cdelt1/stc.cdelt1) else sumdif=1

    	 IF stcnst1 LT 20 THEN BEGIN
	    message,'Subfield presumed',/info
	    print,'Using calibrated fixed instrument offsets.'
	    help,stcravg,stcnst1
         ; if extension exists and ravg is small enough then
    	    index=indices[i]
    	    hi_calib_point,index
      
    	    index.ravg = -894.
    	    indices[i]=index
	 ENDIF ELSE $
        ; modify the pointing information
         IF (stc.ravg le rtmp and stc.ravg ge 0.) THEN BEGIN
            IF ~keyword_set(SILENT) THEN help,stcravg,stcnst1
            indices(i).crval1a = stc.crval1a
            indices(i).crval2a = stc.crval2a
            indices(i).pc1_1a = stc.pc1_1a
            indices(i).pc1_2a = stc.pc1_2a
            indices(i).pc2_1a = stc.pc2_1a
            indices(i).pc2_2a = stc.pc2_2a
            indices(i).cdelt1a = stc.cdelt1a*sumdif
            indices(i).cdelt2a = stc.cdelt2a*sumdif
            indices(i).pv2_1a = stc.pv2_1a
            indices(i).crval1 = stc.crval1
            indices(i).crval2 = stc.crval2
            indices(i).pc1_1 = stc.pc1_1
            indices(i).pc1_2 = stc.pc1_2
            indices(i).pc2_1 = stc.pc2_1
            indices(i).pc2_2 = stc.pc2_2
            indices(i).cdelt1 = stc.cdelt1*sumdif
            indices(i).cdelt2 = stc.cdelt2*sumdif
            indices(i).pv2_1 = stc.pv2_1
            indices(i).xcen = stc.xcen
            indices(i).ycen = stc.ycen
            indices(i).crota = stc.crota
            indices(i).ins_x0 = stc.ins_x0
            indices(i).ins_y0 = stc.ins_y0
            indices(i).ins_r0 = stc.ins_r0
            indices(i).ravg = stc.ravg
         ENDIF ELSE BEGIN
            ; flag if ravg criteria is not met
            print, 'R_avg does not meet criteria for file ',indices(i).filename
            print, 'Using calibrated fixed instrument offsets'
            hi_calib_point,indices(i)
            indices(i).ravg = -883.
         ENDELSE
      ENDIF ELSE BEGIN
         ; flag if no data exists in pointing file
         print, 'No pointing calibration data found for file ',indices(i).filename
         print, 'Using calibrated fixed instrument offsets'
      	 index=indices[i]
         hi_calib_point,index
         index.ravg = -882.
         indices[i]=index
      ENDELSE
   ENDELSE
   
   if indices[i].cdelt1 eq 0 then message,'!!! WARNING !!! CDELT set to ZERO !!!',/info
ENDFOR

END
