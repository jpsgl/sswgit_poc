;+
; $Id: fparaxial.pro,v 1.1 2007/11/28 12:05:53 bewsher Exp $
;
; Project: STEREO-SECCHI
;
; Name: fparaxial
;
; Purpose: Calculate paraxial platescale of HI
;
; Category: STEREO, SECCHI, HI, Calibration, Coordinates
;
; Explanation: This routine calculates the paraxial platescale from
; the calibrated fov and distortion parameter 
;
; Syntax: fp = fparaxial(fov,mu,fp_mm=fp_mm,plate=plate)
;
; Example: get_hi_params,index,pitch_hi,offset_hi,roll_hi,mu,d
;          fp = paraxial(d,mu,plate=plate)
;
; Inputs: calibrated field-of-view and distortion parameter
;
; Opt. Inputs: None
;
; Outputs: fp - ?
;
; Opt. Outputs: fp_mm - fp in mm
;               plate - paraxial platescale in arcsec per pixel
;
; Keywords: None
;
; Calls: None
;
; Common: None
;
; Restrictions: None
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: Version 1, Feb-2007, Danielle Bewsher, RAL 
;          Version 2, 18-Oct-2007, Danielle Bewsher
;                     Added documentation
;
; Contact: Danielle Bewsher (d.bewsher@rl.ac.uk)
;
; $Log: fparaxial.pro,v $
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-

FUNCTION fparaxial,fov,mu,naxis1,naxis2,fp_mm=fp_mm,plate=plate

;pointed at centre of ccd
theta = (90.-(fov/2.))*!dpi/180.

;azp projection
tmp1 = (sin(theta)+mu)/((1+mu)*cos(theta))

;pixel range
fp = (naxis1/2.)*tmp1

; CCD pixel size is 0.0135 mm
widthmm = 0.0135*2048.
fp_mm = widthmm*tmp1/2.

;units arcsec/bin
plate = (0.0135/fp_mm)*(180/!pi)*3600.
;units arcsec/pixel
plate = plate*2048./naxis1

return,fp

END
