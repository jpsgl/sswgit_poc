;+
; $Id: hi_calib_roll.pro,v 1.5 2015/12/22 12:31:15 crothers Exp $
;
; Project: STEREO-SECCHI
;
; Name: hi_calib_roll
;
; Purpose: To calculate the total roll angle of the HI image including
;         contributions from the pitch and roll of the spacecraft 
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation: The total HI roll is a non-straighforward combination
;              of the individual rolls of the s/c and HI, along with
;              the pitch of the s/c and the offsets of HI. This
;              routine calculates the total roll by taking 2 test
;              points in the HI fov, transforms them to the
;              appropriate frame of reference (given by the system
;              keyword) and calculates the angle they make in this frame.
;
; Syntax: roll = hi_calib_roll(index)
;
; Example: roll = hi_calib_roll(index)
;
; Inputs: index - one HI header structure
;
; Opt. Inputs: None
;
; Outputs: roll - roll of image (in degrees)
;
; Opt. Outputs: None
;
; Keywords: system - which coordinate system to work in 'hpc' or 'gei'
;
; Calls: fov2pos
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: 15-Oct-2007, Daniel Brown and Danielle Bewsher
;
; Contact: Daniel Brown (dob@aber.ac.uk) and 
;          Danielle Bewsher (d.bewsher@rl.ac.uk)
;
; 2015/12/18 tappin
;  Reverse sign of roll (for corrected PCi_j, interpretation), rotate
;  by 180 degrees after conjunction.
;
; $Log: hi_calib_roll.pro,v $
; Revision 1.5  2015/12/22 12:31:15  crothers
; updates from James Tappin for correct handling of data both before and after conjunction.
;
; Revision 1.4  2013/11/14 00:26:59  nathan
; always use square images based on index.summed, if present
;
; Revision 1.3  2008/02/14 21:35:20  secchib
; nr - use 1024 for naxis if zero
;
; Revision 1.2  2007/12/14 13:20:32  bewsher
; Added _EXTRA keyword
;
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-
function hi_calib_roll, index, system = sys, _extra = extra

  if (n_elements(sys) eq 0) then sys = 'hpc'

; setup two points along x-axis to transform to measure HI+SC roll

  IF tag_exist(index, 'summed') THEN BEGIN
     naxis1 = 2048/2^(index.summed-1)
     naxis2 = naxis1
  ENDIF ELSE BEGIN
     naxis1 = float(index.naxis1)
     naxis2 = float(index.naxis2)
  ENDELSE
  IF naxis1 LE 0 THEN naxis1 = 1024. ; for header-only case
  IF naxis2 LE 0 THEN naxis2 = 1024.
  cpix = [naxis1, naxis2]/2. - 0.5
  xv = cpix[0] + [0., 512.]
  yv = replicate(cpix[1], 2)

; convert fov pixel place to sky pos
  xy = fov2pos(xv, yv, index, system = sys, _extra = extra)

; generate vector of transformed points
  z = xy(2, 1)-xy(2, 0)
  x = -(xy(1, 1)-xy(1, 0))
  y = (xy(0, 1)-xy(0, 0))

; generate comparative tangent of circle at fixed height
  tx = xy(0, 0)
  ty = xy(1, 0)
  tz = 0.0

; use inverted dot product to calculate angle
  a = sqrt(tx^2 + ty^2)
  b = sqrt(x^2 + y^2 + z^2)
  ab = x*tx + y*ty

; noting that ambiguity is resolved with up/down direction of vector
; Sign changed to use corrected interpretation of PCi_j when i /= j.
  val = min([max([ab/(a*b), -1.0]), 1.0])
  if (z ge 0.0) then begin
     oroll = acos(val)*180./!dpi
  endif else begin
     oroll = -acos(val)*180./!dpi
  endelse

;  Rotating the roll here is much simpler that figuring out which
;  parameters need to be rotated and/or flipped in fov2pos.

  ispostconj = ssw_time_compare(index.date_obs, '2015/07/01', /later)
  if ispostconj then oroll -= 180.

  return, oroll
end
