;+
; $Id: hi2sc.pro,v 1.3 2008/01/16 14:36:56 bewsher Exp $
;
; Project: STEREO-SECCHI
;
; Name: hi2sc
;
; Purpose: To transform the given position from the HI frame of
;          reference to the spacecraft frame of reference.
;          Note, this is a low level code, and would usually not be
;          called directly. 
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation: For the transformation we use 4x4 transformation
;              matrices discussed in e.g. '3D computer graphics' by Alan Watt
;
; Syntax: out = hi2sc(vec,roll_hi_deg,pitch_hi_deg,yaw_hi_deg)
;
; Example:
;
; Inputs: vec - (3xN) array of vector postions to transform
;         roll_hi_deg - HI roll angle relative to spacecraft (in
;                       degrees)
;         pitch_hi_deg - HI pitch angle relative to spacecraft (in
;                        degrees)
;         yaw_hi_deg - HI yaw angle relative to spacecraft (in degrees)
;
; Opt. Inputs: None
;
; Outputs: xy    - a 3xn array of transformed vector positions
;
; Opt. Outputs: None
;
; Keywords: None
;
; Calls: None
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: 27-Jun-2007 by Daniel Brown
;
; Contact: Daniel Brown (dob@aber.ac.uk) 
;
; $Log: hi2sc.pro,v $
; Revision 1.3  2008/01/16 14:36:56  bewsher
; Modified to be more robust to single point inputs
;
; Revision 1.2  2008/01/04 21:35:04  colaninn
; changed fltarr to dblarr
;
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-
function hi2sc,vec, roll_hi_deg, pitch_hi_deg, offset_hi_deg


;sz=size(vec)
;npts=sz(2)
npts = n_elements(vec(0,*))

; pitch has different effect
theta=float(90-pitch_hi_deg)*!dpi/180.
phi=float(offset_hi_deg)*!dpi/180.
roll=float(roll_hi_deg)*!dpi/180.

; normal direction (los direction)
normz=sin(theta)*cos(phi)
normx=sin(theta)*sin(phi)
normy=cos(theta)

; an 'up' direction ('screen y')
; basic up - changed due to change of coordinate system from cart2sc
vdx=0.
vdy=1.
vdz=0.

; calculate vd.norm
vd_norm=vdx*normx + vdy*normy + vdz*normz

; calculate perpendicular up
vxtmp=vdx - vd_norm*normx
vytmp=vdy - vd_norm*normy
vztmp=vdz - vd_norm*normz

; normalise
ndiv=sqrt(vxtmp^2 + vytmp^2 + vztmp^2)
vx=vxtmp/ndiv
vy=vytmp/ndiv
vz=vztmp/ndiv

; a sideways direction ('screen x'), defined negative for left-hand rule
ux=-(normy*vz - normz*vy)
uy=-(normz*vx - normx*vz)
uz=-(normx*vy - normy*vx)

; location of eye
cx=0.
cy=0.
cz=0.


; transformation matrices for screen space
tmat=[[1., 0., 0., -cx], [0., 1., 0., -cy], [0., 0., 1., -cz], [0., 0., 0., 1.]]
rmat=[[ux, uy, uz, 0.], [vx, vy, vz, 0.], [normx, normy, normz, 0.], [0., 0., 0., 1.]]

; transformation for roll
rollmat=[[cos(roll), -sin(roll), 0., 0.], [sin(roll), cos(roll), 0., 0.], [0., 0., 1., 0.], [0., 0., 0., 1.]]

; combine transformation matrices
tview=rollmat##(rmat##tmat)

; now invert the transformation matrix to go from hi to sc
itview=invert(tview,stat)
if (stat ne 0) then print,'matrix inversion failed'

; apply transformation to data
vout=dblarr(4,npts)*0.
for i=0,npts-1 do begin
    vout(*,i)=transpose(itview##transpose(vec(*,i)))
endfor

return,vout
end 
