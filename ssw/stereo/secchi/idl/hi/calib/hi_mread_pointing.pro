;+
; $Id: hi_mread_pointing.pro,v 1.3 2008/10/15 20:59:23 nathan Exp $
;
; Project: STEREO-SECCHI
;
; Name: HI_MREAD_POINTING
;
; Purpose:  To read multiple pointing calibration files 
;           (e.g. pnt_HI??_yyyy-mm-dd_fix_mu_fov.fts file) 
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation:
;
; Syntax: hd = hi_mread_pointing(filelist)
;
; Example: files = findfile('$SCC_DATA/hi/pnt_HI1A_2008-07-*.fts')
;          hd = hi_mread_pointing(files)
;
; Inputs: file - HI pointing calibration fits file
;
; Optional Inputs: None
;
; Outputs: hd - HI pointing calibration fits file header structure
;
; Optional Outputs: None
; 
; Keywords: template - structure template to pass to hi_read_pointing
;
; Calls: hi_read_pointing 
;
; Common: None
;
; Restrictions: None
;
; Side effects: None
;
; Previous History: 17/04/2008 Written by Daniel Brown, Aberystwyth University
;
; Contact: Daniel Brown (dob@aber.ac.uk) and Danielle Bewsher (d.bewsher@rl.ac.uk)
;
; $Log: hi_mread_pointing.pro,v $
; Revision 1.3  2008/10/15 20:59:23  nathan
; add common block so previously read files do not need to be re-read
;
; Revision 1.2  2008/07/30 12:26:40  bewsher
; First release of code to read multiple pointing calibration files
;
;-

FUNCTION hi_mread_pointing, files, template=tmpl

common secchi_pointing, hifilesread, hiheaders

nfiles=n_elements(files)

FOR i=0, nfiles-1 DO BEGIN
    haveit=0
    IF datatype(hiheaders) EQ 'STC' THEN w=where(hifilesread EQ files[i],haveit)
    IF ~haveit THEN BEGIN
    	IF i GT 0 THEN print,'Reading',i,'  of',nfiles,'  pointing calibration files
    	hds = hi_read_pointing(files(i), template=tmpl)
    	IF i EQ 0 THEN heads=hds ELSE heads = [heads, hds]
    ENDIF ELSE BEGIN
    	flen=strlen(files[i])
	strdate=strmid(files[i],flen-25,10)
    	ok=where(strmid(hiheaders.date_avg,0,10) EQ strdate,nk)
	IF n_elements(heads) EQ 0 THEN heads=hiheaders[ok] ELSE heads=[heads,hiheaders[ok]]
    ENDELSE
	
ENDFOR
hiheaders=heads
hifilesread=files
RETURN, heads

END
