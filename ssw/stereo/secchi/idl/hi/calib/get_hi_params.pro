;+
; $Id: get_hi_params.pro,v 1.5 2008/07/28 13:20:45 bewsher Exp $
;
; Project: STEREO-SECCHI
;
; Name: get_hi_params
;
; Purpose: To detect which HI telescope is being used and return the
;         instrument offsets relative to the spacecraft along with
;         the mu parameter and the fov in degrees
;
;         Should use latest fits 
;
; Category: STEREO, SECCHI, HI, Calibration
;
; Explanation: As 'best pointings' may change as further calibration
;              is done, it was thought more useful if there was one
;              central routine to provide this data, rather than
;              having to make the same changes in many different
;              codes. Note, if you set one of the output variables to
;              some value, then it will retain that value.
;
; Syntax: get_hi_params, index, hi_offy, hi_offx, hi_roll, mu, fov
;
; Example: get_hi_params,index,hi_x,hi_y,hi_roll,mu,fov
;
; Inputs: index - one HI header structure
;
; Opt. Inputs: None
;
; Outputs: hi_offx - HI yaw offset
;          hi_offy - HI pitch offfset
;          hi_roll - HI roll
;          mu - HI distortion parameter
;          fov - HI fov
;
; Opt. Outputs: None
;
; Keywords: /hi_nominal - retrieve nominal values at launch.
;           Default is most up to date calibrated values 
;           a la Bewsher and Brown.
;
; Calls: None
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: 15-Oct-2007, Daniel Brown and Danielle Bewsher
;
; Contact: Daniel Brown (dob@aber.ac.uk) and 
;          Danielle Bewsher (d.bewsher@rl.ac.uk)
;
; $Log: get_hi_params.pro,v $
; Revision 1.5  2008/07/28 13:20:45  bewsher
; Updated calibrated instrument offsets with results from Brown, Bewsher & Eyles (2008). Derived from all data up to the end of April 2008.
;
; Revision 1.4  2008/02/27 12:46:54  bewsher
; Corrected nominal 'd' parameters
;
; Revision 1.3  2007/12/14 13:16:48  bewsher
; Changed NOMINAL keyword to HI_NOMINAL
;
; Revision 1.2  2007/11/29 14:34:15  bewsher
; Updated HI 2B calibrated parameters
;
; Revision 1.1  2007/11/28 12:05:53  bewsher
; First releases of hi_calib_point and associated code
;
;-
pro get_hi_params,index,pitch_hi,offset_hi,roll_hi,mu,d,hi_nominal=hi_nominal,_extra=extra

if keyword_set(hi_nominal) then begin
  ;print,'Using nominal pointing parameters'
   if ((index.obsrvtry eq 'STEREO_A') and (index.detector eq 'HI1')) then begin
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.0
      if (n_elements(offset_hi) eq 0) then offset_hi=-13.98
      if (n_elements(roll_hi) eq 0) then roll_hi=0.0
      if (n_elements(mu) eq 0) then mu=0.16677
      if (n_elements(d) eq 0) then d=20.2663
   endif
   if ((index.obsrvtry eq 'STEREO_A') and (index.detector eq 'HI2')) then begin
      if (n_elements(offset_hi) eq 0) then offset_hi=-53.68
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.
      if (n_elements(roll_hi) eq 0) then roll_hi=0.
      if (n_elements(mu) eq 0) then mu=0.83329
      if (n_elements(d) eq 0) then d=70.8002
   endif
   if ((index.obsrvtry eq 'STEREO_B') and (index.detector eq 'HI1')) then begin
      if (n_elements(offset_hi) eq 0) then offset_hi=13.98
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.
      if (n_elements(roll_hi) eq 0) then roll_hi=0.
      if (n_elements(mu) eq 0) then mu=0.10001
      if (n_elements(d) eq 0) then d=20.2201
   endif
   if ((index.obsrvtry eq 'STEREO_B') and (index.detector eq 'HI2')) then begin
      if (n_elements(offset_hi) eq 0) then offset_hi=53.68
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.
      if (n_elements(roll_hi) eq 0) then roll_hi=0.
      if (n_elements(mu) eq 0) then mu=0.65062
      if (n_elements(d) eq 0) then d=69.8352
   endif
endif else begin
; best as of 15-Oct-2007
  ;print,'Using calibrated pointing parameters'
   if ((index.obsrvtry eq 'STEREO_A') and (index.detector eq 'HI1')) then begin
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.1159
      if (n_elements(offset_hi) eq 0) then offset_hi=-14.0037
      if (n_elements(roll_hi) eq 0) then roll_hi=1.0215
      if (n_elements(mu) eq 0) then mu=0.102422
      if (n_elements(d) eq 0) then d=20.27528
   endif
   if ((index.obsrvtry eq 'STEREO_A') and (index.detector eq 'HI2')) then begin
      if (n_elements(offset_hi) eq 0) then offset_hi=-53.4075
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.0662
      if (n_elements(roll_hi) eq 0) then roll_hi=0.1175
      if (n_elements(mu) eq 0) then mu=0.785486
      if (n_elements(d) eq 0) then d=70.73507
   endif
   if ((index.obsrvtry eq 'STEREO_B') and (index.detector eq 'HI1')) then begin
      if (n_elements(offset_hi) eq 0) then offset_hi=14.10
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.022
      if (n_elements(roll_hi) eq 0) then roll_hi=0.37
      if (n_elements(mu) eq 0) then mu=0.09509
      if (n_elements(d) eq 0) then d=20.23791
   endif
   if ((index.obsrvtry eq 'STEREO_B') and (index.detector eq 'HI2')) then begin
      if (n_elements(offset_hi) eq 0) then offset_hi=53.690
      if (n_elements(pitch_hi) eq 0) then pitch_hi=0.213
      if (n_elements(roll_hi) eq 0) then roll_hi=-0.052
      if (n_elements(mu) eq 0) then mu=0.68886
      if (n_elements(d) eq 0) then d=70.20152
   endif
endelse

end
