;+
;$Id: hi_point_v2.pro,v 1.14 2015/07/21 15:12:11 nathan Exp $
; Project: STEREO-SECCHI
;
; Name: hi_point_v2
;
; Purpose: Correct pointing values in HI headers
;
; Category: STEREO, SECCHI, HI, Calibration, Coordinates
;
; Explanation: This routine applies the nominal pointing calibration
;              to a HI image header. The following keywords are affected:
;                      crval1, crval2, crval1a,crval2a
;                      pc1_1, pc1_2, pc2_1, pc2_2
;                      pc1_1a, pc1_2a, pc2_1a, pc2_2a, xcen, ycen,
;                      ins_x0,ins_y0, ins_r0
;             This routine also adds the spacecraft pitch and yaw from
;             the guide telescope data and the spacecraft roll angle
;             from the SPICE data to the following keywords:
;                      sc_pitch, sc_yaw, sc_roll
;                      sc_pita, sc_yawa, sc_rolla
;
; Syntax: index = hi_point_v2(index,exthdr)
;
; Example: a = sccreadfits(filename,index)
;          index = hi_point_v2(index,exthdr)
;
; Inputs: index - SECCHI header structure
;         exthdr - extended header
;
; Opt. Inputs: None
;
; Outputs: index - modified header is returned
;
; Opt. Outputs: None
;
; Keywords: None
;
; Calls: hi_get_avpoint, hi_calib_point
;
; Common: None
;
; Restrictions: None
;
; Side effects: None
;
; Prev. Hist.: Original code written with information given by Bill
;              Thompson in a series of emails
;
; History:
; $Log: hi_point_v2.pro,v $
; Revision 1.14  2015/07/21 15:12:11  nathan
; do not change sc_* keywords
;
; Revision 1.13  2008/03/12 21:07:54  nathan
; ensure input index variable is not changed
;
; Revision 1.12  2008/02/07 13:47:42  bewsher
; Changed procedure to a function
;
; Revision 1.11  2007/12/18 12:33:05  bewsher
; Updated code to fix discrepancy between coordinate systems (bugzilla no. 237). Added call to hi_calib_point - using nominal pointing parameters
;
; Revision 1.10  2007/08/30 19:09:10  nathan
; added INS_[xyr]0 (Bug 91)
;
; Revision 1.9  2007/08/23 23:53:42  nathan
; add /Silent
;
; Revision 1.8  2007/08/20 18:48:37  nathan
; Add _extra keyword so DIR can be passed to scc_sunvec
;
; Revision 1.7  2007/08/13 19:41:02  nathan
; updated hi_get_avpoint.pro
;
; Revision 1.6  2007/08/03 18:06:08  nathan
; add error from scc_sunvec
;
; Revision 1.5  2007/08/03 14:29:29  crothers
; Added error handling code around get_stero_att_file
;
; Revision 1.4  2007/07/31 19:28:14  nathan
; Made changes from D.Bewsher: Use average yaw, pitch, roll of exposures in
; summed image, where possible; now need extended header as input
;
; Revision 1.3  2007/07/20 19:21:15  nathan
; Obtain scc_sunvec error; get ATT_FILE; fix sc_pitcha; use deg for sc_yaw,pitch
;
; Revision 1.2  2007/07/10 20:05:55  nathan
; Reversed order of scc_sunvec output; use hpc_point roll value;
; compute XCEN/YCEN; use arcsec for SC_YAW/SC_PITCH
;
; Revision 1.1  2007/07/10 17:43:19  nathan
; version from D.Bewsher; added CVS keywords
;
; Version 3, 21-Jun-2007, Danielle Bewsher, RAL
;   	    Use date_avg for attitude routines
; Version 2, 13-Jun-2007, Danielle Bewsher, RAL
;   	    Added calls to scc_sunvec for spacecraft pitch
;   	    and yaw from guide telescope
; Version 1, 27-Feb-2007, Danielle Bewsher, RAL
;
; Contact: Danielle Bewsher (d.bewsher@rl.ac.uk)
;-
;
FUNCTION hi_point_v2,index0,exthdr, SILENT=silent, _extra=_extra

info="$Id: hi_point_v2.pro,v 1.14 2015/07/21 15:12:11 nathan Exp $"
IF ~keyword_set(SILENT) THEN print,info
index=index0
;wait,5
;
; Calculate average (where possible) yaw, pitch and roll for exposures
; in summed image in HPC (spoint) and GEI (cpoint) coordinates
; 
spoint = hi_get_avpoint(index,exthdr,av_cpoint=cpoint,error=error, _extra=_extra)
IF ~keyword_set(SILENT) THEN print,'spoint=',spoint
;;
;; Up date sc_yaw, sc_pitch, sc_roll keywords
;;
;index.sc_yaw =	 spoint[0]  ;d[1]
;index.sc_pitch = spoint[1]  ;d[0]
;index.sc_roll =  spoint[2]
;;
;; Up date sc_yawa, sc_pita, sc_rolla keywords
;;
;index.sc_yawa = cpoint(0)
;index.sc_pita = cpoint(1)
;index.sc_rolla= cpoint(2)
;
; Update header with nominal pointing information
;
hi_calib_point,index,/hi_nominal
;
; Update xcen and ycen keywords
;
if(index.NAXIS1 gt 0 and index.NAXIS2 gt 0)then begin
  WCS = FITSHEAD2WCS(index)
  ; For RA/Dec, we need to specify system=A
  XYCEN = WCS_GET_COORD(WCS, [(index.NAXIS1-1.)/2., (index.NAXIS2-1.)/2.])
  index.XCEN=XYCEN[0]
  index.YCEN=XYCEN[1]
endif
;
; Header housekeeping
;
att_file = get_stereo_att_file(index.date_avg,index.obsrvtry)
IF error GT 0 THEN errstr=trim(fix(error)) ELSE errstr=''
att_file = att_file+'+'+errstr+'GT'
; update att_file
if tag_exist(index,'att_file') then index.att_file = att_file
IF ~keyword_set(SILENT) THEN help,error,att_file

len=strlen(info)
histinfo=strmid(info,1,len-2)
if tag_exist(index,'history') then index=scc_update_history(index, histinfo)

return,index

END
