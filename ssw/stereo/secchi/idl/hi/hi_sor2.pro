;+
; $Id: hi_sor2.pro,v 1.1 2008/01/21 16:17:09 bewsher Exp $
;
; Project: STEREO-SECCHI
;
; Name: hi_sor2
;
; Purpose: To reconstruct pre-identified pixels in the data using an
;          SOR method. This solves del^2=f(x,y), where the values for
;          f are passed
;
; Category: STEREO, SECCHI, HI
;
; Explanation: 
;
; Syntax: recon=hi_sor2(data,fdata,index)
;
; Example: 
;
; Inputs: data - 2d image 
;         fdata - the f values (must be of same size as data)
;         index - 2xN array containing index of pixels to be
;                  reconstructed. May not contain edge pixels
;
; Opt. Inputs: omega - the overrelaxation parameter, defaults to 1.5
;              nits  - the number of SOR iterations to make. Defaults to
;                      100 which should be okay in general, but would need
;                      to be higher if larger blocks of pixels are to be
;                      reconstructed
;
; Outputs: recon - reconstructed image
;
; Opt. Outputs: None
;
; Keywords: None
;
; Calls: None
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: Written 27 February 2007 by Daniel Brown (DOB)
;          23-Mar-07, documentation added (DOB)
;
; Contact: Daniel Brown (dob@aber.ac.uk)
;
; $Log: hi_sor2.pro,v $
; Revision 1.1  2008/01/21 16:17:09  bewsher
; First release of hi_remove_starfield and associated code
;
;-
function hi_sor2,dat,ddat,ind,omega=w,nits=nits

; set some default values
if (n_elements(w) eq 0) then w=1.5
if (n_elements(nits) eq 0) then nits=100

; how many pixels are to be reconstructed
npts=n_elements(ind(0,*))

; make a working copy of the data
recon=float(dat)

; perform SOR reconstruction
for n=1,nits do begin
    for k=0l,npts-1l do begin

        dum = 0.25*(recon(ind(0,k),ind(1,k)-1) + recon(ind(0,k)-1,ind(1,k)) + recon(ind(0,k)+1,ind(1,k)) + recon(ind(0,k),ind(1,k)+1) - ddat(ind(0,k),ind(1,k)))
        dum2 = w*dum + (1.0-w)*recon(ind(0,k),ind(1,k))
        recon(ind(0,k),ind(1,k)) = dum2

    endfor
endfor

; return reconstructed data
return,recon
end








