; $Id: hi_get_avpoint.pro,v 1.7 2010/12/09 22:36:43 nathan Exp $
;+
;
; Project: STEREO-SECCHI
;
; Name: hi_get_point
;
; Purpose: Calculates the average spacecraft pointing during summed image
;
; Category: STEREO, SECCHI, HI, Calibration, Coordinates
;
; Explanation: Calculates the pointing of the spacecraft at each
;              exposure that makes up an HI image. 
;              Then calculates the average of these to use as the 
;              spacecraft pointing which goes in to the header.  
;
; Syntax: spoint = hi_get_point(index, exthdr, av_cpoint=cpoint)
;
; Example: a = sccreadfits(filename,index)
;          exthdr = mrdfits(filename,1)
;          spoint = hi_get_avpoint(index,exthdr,av_cpoint=cpoint)
;
; Inputs: index - SECCHI header structure
;         exthdr - SECCHI extended header structure
;
; Opt. Inputs: None
;
; Outputs: spoint - average spacecraft yaw, pitch and roll in HPC coordinates
;
; Opt. Outputs: cpoint - average spacecraft yaw, pitch and roll in GEI 
;               coordinates
;
; Keywords: 
;    DIR      = directory containing level 0 secchi telem. files 
;               Can be a 2-element string vector for ['Ahead','Behind']
;               If not present: no jitter correction, only offpoint (keyword for scc_sunvec.pro)
;
; Calls: get_stereo_hpc_point, get_stereo_lonlat, scc_sunvec, 
;        convert_stereo_lonlat
;
; Common: None
;
; Restrictions: Use of DIR keyword requires level-0 raw telemetry files. 
;   	At NRL their location is defined as:
;
;	IF [more than 45 days old] THEN $
;	sdspath=getenv('SDS_FIN_DIR')+'/'+aorblc+'/'+yyyy+'/'+mm ELSE $
;	sdspath=getenv('SDS_DIR')+'/'+scname+'/data_products/level_0_telemetry/secchi'
;
;   	where aorblc = 'a' or 'b' and scname = 'ahead' or 'behind'
;
; Side effects: None
;
; $Log: hi_get_avpoint.pro,v $
; Revision 1.7  2010/12/09 22:36:43  nathan
; add SPOINT=
;
; Revision 1.6  2008/01/04 21:28:05  colaninn
; changed fltarr to dblarr
;
; Revision 1.5  2007/12/03 22:04:14  nathan
; added documentation for DIR keyword
;
; Revision 1.4  2007/08/20 18:48:37  nathan
; Add _extra keyword so DIR can be passed to scc_sunvec
;
; Revision 1.3  2007/08/13 19:38:51  nathan
; use date-avg if exthdr undefined
;
; Revision 1.2  2007/08/03 18:05:38  nathan
; add error from scc_sunvec
;
; Revision 1.1  2007/07/31 19:30:24  nathan
; for hi_point_v2.pro,v 1.4+
;
; Prev. Hist.: Method proposed by Chris Eyles and Bill Thompson
;
; History: Version 1, 18-Jul-2007, Danielle Bewsher, RAL 
;
; Contact: Danielle Bewsher (d.bewsher@rl.ac.uk)
;-
;
FUNCTION hi_get_avpoint,index,exthdr,av_cpoint=av_cpoint, SPOINT=spoint, CPOINT=cpoint, $
error=error, _extra=_extra

;
;Detemine how many exposures there are
;
IF datatype(exthdr) NE 'STC' THEN BEGIN
    n_exp=1
    all_exp=30	    ; dummy value
ENDIF ELSE BEGIN
    all_exp = n_elements(exthdr.date_ro)
    all_exp = float(all_exp)

    ;
    ;Check for null records date_ro = ''
    ;Record how many 'good' records there are in n_exp
    ;
    ii = where(exthdr.date_ro ne '',n_exp)
    n_exp = float(n_exp)
ENDELSE
;
;If more than 80% of exposures are 'good' then use average pointing
;over all exposures. Else revert to using date_avg
;
IF (n_exp/all_exp gt 0.8) THEN BEGIN 
  ;
  ;Extract only 'good' exposure times from extended header
  ;
  dat = exthdr(ii).date_ro
ENDIF ELSE BEGIN
  ;
  ;Revert to using date_avg
  ;
  print,'Using date_avg'
  dat = index.date_avg
ENDELSE

;
;Determine how many date/times we're using for the rest of the calculations
;
ndat = n_elements(dat)
;print,ndat

;
;initialise arrays to collect spacecraft pointing in HPC and GEI
;coordinate systems at each exposure
;
spoint = dblarr(3,ndat) ;spacecraft pointing in HPC coordinates
cpoint = dblarr(3,ndat) ;spacecraft pointing in GEI coordinates

;
;Get the spacecraft pointing from the guide telescope data
;Get the spacecraft roll angle from the SPICE data
;
stmp = get_stereo_hpc_point(dat,index.obsrvtry,/degrees)

tmp = get_stereo_lonlat(dat,index.obsrvtry,/au)

dsun = tmp(0,*)
d = dblarr(2,ndat)
FOR i=0,ndat-1 DO d(*,i) = scc_sunvec(dat(i),dsun(i),obs=index.obsrvtry, error=error, _extra=_extra)

;
;scc_sunvec returns sun vector [pitch,yaw] in guide telescope frame 
;in arcseconds so convert to degrees to compare with everything else
;
stmp2 = dblarr(2,ndat)
stmp2[0,*] = d[1,*]/3600.d0
stmp2[1,*] = d[0,*]/3600.d0
spoint[0:1,*] = stmp2
spoint[2,*] = stmp[2,*]

convert_stereo_lonlat,dat,stmp2,'HPC','GEI',spacecraft=index.obsrvtry,/degrees
ctmp2 = stmp2
cpoint[0:1,*] = ctmp2

roll = get_stereo_roll(dat,index.obsrvtry,system='GEI',/degrees)
cpoint[2,*] = roll

;
;Calculate average pointing for HPC coordiantes
;
av_spoint = dblarr(3)
av_spoint[0] = mean(spoint[0,*])
av_spoint[1] = mean(spoint[1,*])
av_spoint[2] = mean(spoint[2,*])

;
;Calculate average pointing for GEI coordiantes
;
av_cpoint = dblarr(3)
av_cpoint[0] = mean(cpoint[0,*])
av_cpoint[1] = mean(cpoint[1,*])
av_cpoint[2] = mean(cpoint[2,*])

return,av_spoint

END
