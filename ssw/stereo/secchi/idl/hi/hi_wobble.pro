;+
;
; $Id: hi_wobble.pro,v 1.1 2009/06/18 13:54:50 crothers Exp $
;
; Project: STEREO-SECCHI
;
;
; Name: hi_align_image
;
; Purpose: To test for, calibrate and optionally align images from one time to
;          another where these is a risk of the image calibration changing from
;          one image to another
;
; Category: STEREO, SECCHI, HI
;
; Explanation: In the HI-1B camera there are noticable shifts in the image
;              inconsistent with the spacecraft motion.  This routine can be
;              used to detect these shifts and to align a image with its
;              expected location.  To align the image first a transformation
;              is done to the reference dataset using the calibrated pointing
;              the reverse transformation is then performed using either the 
;              best fit spacecraft location data or if the keyword hi_nominal
;              is set then the pre launch nominal offset
;
; Examples;
;
;         a=file_search('20090101/20090101*s4h1B.fts')
;         image=sccreadfits(a[0:1],hdr,/nodata)
;         d=hi_wobble(hdr[0],hdr[1],/test,x=x,y=y)
;         
;         returns the maximum displacement of a nine point sample of the
;         data and [optionally] the furthest displacements in x and y
;         coordinates.
;
;         coords=hi_wobble(hdr[0],hdr[1])
;
;         returns the x/y locations to displace image by to align the
;         two images.
;
;         aligned_image=hi_wobble(hdr[0],hdr[1],image=image[*,*,0])
;
;         returns an image aligned with image[*,*,1] using the
;         IDL interpolate function

function hi_wobble,a_hdr,ref_hdr,image=image,test=test,x=x,y=y,_extra=extra

; get the standard calibration pointing change

a_c_hdr=a_hdr
hi_calib_point,a_c_hdr,_extra=extra

ref_c_hdr=ref_hdr
hi_calib_point,ref_c_hdr,_extra=extra

out=fitshead2wcs(a_c_hdr)
in=fitshead2wcs(ref_c_hdr)

s=size(test)

if n_elements(test) eq 0 then begin
; not test mode - working on full image

; get the image coordinates in default coordinates
    coords=wcs_get_coord(out)

; convert to pixels in coordinates of other image
    pixels=wcs_get_pixel(in,coords)

; convert to coordinates in proper calibrated coordinates
    in=fitshead2wcs(ref_hdr)
    coords=wcs_get_coord(in,pixels)

; convert to pixels in other image
    out=fitshead2wcs(a_hdr)
    pixels=wcs_get_pixel(out,coords)

    if n_elements(image) eq 0 then return,pixels

    dest=interpolate(image,pixels[0,*,*],pixels[1,*,*],missing=!values.F_NAN)
    return,dest
endif

; generate a test sampling if not provided, a [2,n,m] array of
; pixel coordinates - default is pixels  256, 512 and 768
if s[0] eq 3 then begin
    zz=test
endif else begin
    x=indgen(3)*256+256
    xx=rebin(x,3,3)
    yy=transpose(xx)
    zz=intarr(2,3,3)
    zz[0,*,*]=xx
    zz[1,*,*]=yy
endelse

; same transformations as full image, but only using sample data set

coords=wcs_get_coord(out,zz)

pixels=wcs_get_pixel(in,coords)

in=fitshead2wcs(ref_hdr)
coords=wcs_get_coord(in,pixels)

out=fitshead2wcs(a_hdr)
pixels=wcs_get_pixel(out,coords)

s=size(pixels)

dz=pixels-zz

x=max(dz[0,*,*],min=minx)
if abs(minx) gt x then x=minx
y=max(dz[1,*,*],min=miny)
if abs(miny) gt y then y=miny

d=max(sqrt(total(dz*dz,1)))

return,d

end
