pro exthdr_report, filelist, filename=filename, MISSING=missing
; $Id: exthdr_report.pro,v 1.2 2010/12/09 22:36:17 nathan Exp $
;
; Purpose:  Appends report of extended headers (of missing image info) 
;
; Input:    filelist	list of fits files with or without path
;
; Output:  Text file in current dir or in FILENAME=
;
; Keywords:
;   FILENAME=	Name of output report which will be appended
;   /MISSING	Only print missing data else print everything
;
; $Log: exthdr_report.pro,v $
; Revision 1.2  2010/12/09 22:36:17  nathan
; updates
;
; Revision 1.1  2007/11/14 22:50:07  nathan
; I intend this routine to be a template for othere reports
;


IF keyword_set(filename) THEN outpf=filename ELSE outpf='./exthdreport.txt'
openw,1,outpf,/append

nf=n_elements(filelist)
outpformat='(A26,2X,I3,2X,A11,1X,A14,I6)'
for i=0,nf-1 DO BEGIN
    len=strlen(filelist[i])
    IF len LT 27 THEN filen=sccfindfits(filelist[i]) ELSE filen=filelist[i]
    print,filen
    s=mrdfits(filen,1)
    ns=n_elements(s)
    IF ns GT 1 THEN BEGIN
    	z=where(s.ccdsum EQ 0,nz)
    	junk=sccreadfits(filen,h,/nodata)
	fn=h.filename 
    	FOR j=0,ns-1 DO BEGIN
	    wz=where(z eq j,nwz)
	    IF nwz GT 0 THEN BEGIN
	    	sfx='Missing ' 
	    	print,'Missing ',h.filename, j
	    ENDIF ELSE sfx=''
	    printf,1, fn, ns, sfx+trim(s[j].imgseq), h.fileorig, s[j].imgctr, $
	    format=outpformat
    	ENDFOR  ; bad rows
    ENDIF
ENDFOR	; fits files    

close,1
end
