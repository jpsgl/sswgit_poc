;+
;$Id: convert_to_orig_secchi.pro,v 1.2 2010/06/11 22:09:04 nathan Exp $
;
; Project     : SECCHI
;
; Name        : CONVERT_TO_ORIG_SECCHI 
;
; Purpose     : converts selected integer fields to text, when possible
;               (i.e. camera numbers to names).
;
; Category    : Widget
;
; Explanation : Used by sqdb_tools to display select database query results.
;
; Use         : IDL>text= convert_to_orig_secchi(vals,tag,cam,cvals,elem,obs)
;
; Examples    : IDL>text= convert_to_orig_secchi(vals,tag,cam,cvals,elem,obs)
;
; Inputs      : vals  : values to be converted, if possible 
;               tag   : field name of values to be converted
;               cam   : 0: no camera field, > 0 camera field count
;               cvals : camera fields values
;               elem  : length (number) of values
;               obs   : observation programs
;               scid  : spacecraft ID 'A' or 'B'
;
; Opt. Inputs : None
;
; Outputs     : text: converted values, if any
;
; Opt. Outputs: None
;
; Keywords    : None
;
; History     : Ed Esfandiari November 2005
;               First Version.
;               Ed Esfandiari 12/19/06 - corrected door_pos stat (0=closed, 1=in transit, 2=open, 3=unknown)
;
;$Log: convert_to_orig_secchi.pro,v $
;Revision 1.2  2010/06/11 22:09:04  nathan
;modified Oct 2008 in $NRL_LIB
;
;Revision 1.1  2007/01/12 22:41:50  nathan
;moved from nrl_lib/dev/database
;
;Revision 1.3  2006/12/19 19:54:14  esfand
;corrected door_stat
;
;Revision 1.2  2006/01/26 15:57:41  esfand
;minor changes in some routines
;
;
;
; This funcation translates the interger values for certain fields in a 
; structure returned by QDB (passed to this routine as array of cur_vals) to
; to their original meaningful text equivalent.


function convert_to_orig_secchi,cur_vals,cur_tag,camera_cnt,camera_field,elem,obs_nums,scid,exptime2

      str_vals= strarr(elem)
      pos= strpos(cur_tag,'DETECTOR')
      if(pos ge 0 and pos+strlen('DETECTOR') eq strlen(cur_tag)) then begin 
        ;pos= strpos(cur_tag,'OBSRVTRY') ; SpaceCraft A or B
        ;if(pos ge 0 and pos+strlen('OBSRVTRY') eq strlen(cur_tag)) then begin
        ;  ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='SC-A'
        ;  ind=where(cur_vals eq 2,cnt)&if(cnt gt 0) then str_vals(ind)='SC-B'
        ;end
       s=size(cur_vals) ;s(2)=2 for integer & s(2)=7 for string. it must be 2. 
       if(s(0) eq 0) then s= s(1) ;scalar: s(1)=2 => integer & s(1)=7 => string
       if(s(0) eq 1) then s= s(2) ;array: s(2)=2 => integer & s(2)=7 => string
       if(s eq 2) then begin 
        ind= where(cur_vals eq 3, cnt) & if (cnt gt 0) then str_vals(ind)= 'EUVI'
        ind= where(cur_vals eq 2, cnt) & if (cnt gt 0) then str_vals(ind)= 'COR1'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'COR2'
        ind= where(cur_vals eq 5, cnt) & if (cnt gt 0) then str_vals(ind)= 'HI1'
        ind= where(cur_vals eq 4, cnt) & if (cnt gt 0) then str_vals(ind)= 'HI2'
        return, str_vals
       end
      end
      ;pos= strpos(cur_tag,'OBS_PROG') ; LEB Program
      ;if(pos ge 0 and pos+strlen('OBS_PROG') eq strlen(cur_tag)) then begin
      pos= strpos(cur_tag,'SEB_PROG') ; LEB Program
      if(pos ge 0 and pos+strlen('SEB_PROG') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 2,cnt)&if(cnt gt 0) then str_vals(ind)='Double '
        ind=where(cur_vals eq 0,cnt)&if(cnt gt 0) then str_vals(ind)='Normal'
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='CalDark'
        ind=where(cur_vals eq 3,cnt)&if(cnt gt 0) then str_vals(ind)='CalLED'
        ind=where(cur_vals eq 4,cnt)&if(cnt gt 0) then str_vals(ind)='Continuous'
        ind=where(cur_vals eq 5,cnt)&if(cnt gt 0) then str_vals(ind)='Seq'
        return, str_vals
      end
      pos= strpos(cur_tag,'OBSRVTRY') ; SpaceCraft A or B
      if(pos ge 0 and pos+strlen('OBSRVTRY') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='SC-A'
        ind=where(cur_vals eq 2,cnt)&if(cnt gt 0) then str_vals(ind)='SC-B'
        return, str_vals
      end
      pos= strpos(cur_tag,'SYNC') ; Synced images (Yes or No)
      if(pos ge 0 and pos+strlen('SYNC') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 0,cnt)&if(cnt gt 0) then str_vals(ind)='No'
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='Yes'
        return, str_vals
      end
      pos= strpos(cur_tag,'HDR_ONLY')
      if(pos ge 0 and pos+strlen('HDR_ONLY') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 0,cnt)&if(cnt gt 0) then str_vals(ind)='No'
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='Yes'
        return, str_vals
      end
      pos= strpos(cur_tag,'DOOR_POS')
      if(pos ge 0 and pos+strlen('DOOR_POS') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 0,cnt)&if(cnt gt 0) then str_vals(ind)='Closed'
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='Transit'
        ind=where(cur_vals eq 2,cnt)&if(cnt gt 0) then str_vals(ind)='Open'
        ind=where(cur_vals eq 3,cnt)&if(cnt gt 0) then str_vals(ind)='Unknown'
        return, str_vals
      end
      pos= strpos(cur_tag,'FILETYPE')
      if(pos ge 0 and pos+strlen('FILETYPE') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 0,cnt)&if(cnt gt 0) then str_vals(ind)='raw image'
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='level_0.5'
        ind=where(cur_vals eq 2,cnt)&if(cnt gt 0) then str_vals(ind)='level_1'
        ind=where(cur_vals eq 3,cnt)&if(cnt gt 0) then str_vals(ind)='level_2'
        return, str_vals
      end
      pos= strpos(cur_tag,'SOURCE')
      if(pos ge 0 and pos+strlen('SOURCE') eq strlen(cur_tag)) then begin
        ind=where(cur_vals eq 0,cnt)&if(cnt gt 0) then str_vals(ind)='SSR1'
        ind=where(cur_vals eq 1,cnt)&if(cnt gt 0) then str_vals(ind)='SSR2'
        ind=where(cur_vals eq 2,cnt)&if(cnt gt 0) then str_vals(ind)='RT'
        ind=where(cur_vals eq 3,cnt)&if(cnt gt 0) then str_vals(ind)='SW'
        return, str_vals
      end
      pos= strpos(cur_tag,'DATALEVEL')
      if(pos ge 0 and pos+strlen('DATALEVEL') eq strlen(cur_tag)) then begin
      ;if(pos ge 0) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'Level_0.5'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'Level_1'
        ind= where(cur_vals eq 2, cnt) & if (cnt gt 0) then str_vals(ind)= 'Level_2'
        ind= where(cur_vals eq 3, cnt) & if (cnt gt 0) then str_vals(ind)= 'Level_3'
        ind= where(cur_vals eq 4, cnt) & if (cnt gt 0) then str_vals(ind)= 'PreFlight'
        ind= where(cur_vals eq 5, cnt) & if (cnt gt 0) then str_vals(ind)= 'QuickLook'
        return, str_vals
      end
      pos= strpos(cur_tag,'CCD_SIDE')
      if(pos ge 0 and pos+strlen('CCD_SIDE') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'frontside'
        ind= where(cur_vals eq 16, cnt) & if (cnt gt 0) then str_vals(ind)= 'backside'
        return, str_vals
      end
      pos= strpos(cur_tag,'LED_ID')
      if(pos ge 0 and pos+strlen('LED_ID') eq strlen(cur_tag)) then begin
        if(camera_cnt gt 0) then begin
          str_vals= LEDS2STR(camera_field,cur_vals,obs_nums) 
        endif
        return, str_vals
      end
      pos= strpos(cur_tag,'LAMP')
      if(pos ge 0 and pos+strlen('LAMP') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'off'
        ind= where(cur_vals eq 2, cnt) & if (cnt gt 0) then str_vals(ind)= 'door on'
        ind= where(cur_vals eq 3, cnt) & if (cnt gt 0) then str_vals(ind)= 'shutter on'
        return, str_vals
      end
      pos= strpos(cur_tag,'SHUTTER')
      if(pos ge 0 and pos+strlen('SHUTTER') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'Open'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'Closed'
        return, str_vals
      end
      pos= strpos(cur_tag,'FILTER')
      if(pos ge 0 and pos+strlen('FILTER') eq strlen(cur_tag)) then begin
        s=size(cur_vals) ; s(2)=2 for integer & s(2)=7 for string. it must be 2.
        if(s(0) eq 0) then s= s(1) ;scalar: s(1)=2 => integer & s(1)=7 => string
        if(s(0) eq 1) then s= s(2) ;array: s(2)=2 => integer & s(2)=7 => string
        if(camera_cnt gt 0 and s eq 2) then begin 
          ;str_vals= cnvrt_filter(camera_field,cur_vals)
          str_vals= SECCHIFW2STR(camera_field,cur_vals)
          return, str_vals
        end 
      end
      pos= strpos(cur_tag,'POLAR_QUAD')
      if(pos ge 0 and pos+strlen('POLAR_QUAD') eq strlen(cur_tag)) then begin
        ;if(camera_cnt gt 0) then str_vals= cnvrt_polar(camera_field,cur_vals) 
        if(camera_cnt gt 0) then str_vals= SECCHIPW2STR(camera_field,cur_vals,obs_nums,1,scid,fltarr(n_elements(scid)))
        return, str_vals 
      end
      pos= strpos(cur_tag,'POLAR2')
      if(pos ge 0 and pos+strlen('POLAR2') eq strlen(cur_tag)) then begin
        ;if(camera_cnt gt 0) then str_vals= cnvrt_polar(camera_field,cur_vals)
        if(camera_cnt gt 0) then str_vals= SECCHIPW2STR(camera_field,cur_vals,obs_nums,2,scid,exptime2)
        return, str_vals
      end
      pos= strpos(cur_tag,'LINE_SYNC')
      if(pos ge 0 and pos+strlen('LINE_SYNC') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'no'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'yes'
        return, str_vals
      end
      pos= strpos(cur_tag,'CAMERA_ERR')
      if(pos ge 0 and pos+strlen('CAMERA_ERR') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'no'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'yes'
        return, str_vals
      end
      pos= strpos(cur_tag,'SEND_DATA')
      if(pos ge 0 and pos+strlen('SEND_DATA') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'yes'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'no'
        return, str_vals
      end
      pos= strpos(cur_tag,'TRANS_IMAGE')
      if(pos ge 0 and pos+strlen('TRANS_IMAGE') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'no'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'yes'
        return, str_vals
      end
      pos= strpos(cur_tag,'TRANS_DET')
      if(pos ge 0 and pos+strlen('TRANS_DET') eq strlen(cur_tag)) then begin
        ind= where(cur_vals eq 0, cnt) & if (cnt gt 0) then str_vals(ind)= 'no'
        ind= where(cur_vals eq 1, cnt) & if (cnt gt 0) then str_vals(ind)= 'yes'
        return, str_vals
      end

      return, cur_vals  ; field values for this tag was not converted (was not one of the above). 

end
