pro check_img_scripts,dtst,dtend,mdays=mdays, docpimgdb=docpimgdb, _EXTRA=_extra
;+
; $Id: check_img_scripts.pro,v 1.2 2013/03/15 15:30:23 secchib Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : check_img_scripts
;               
; Purpose   : to unzip and copy database scripts from pipeline archive to the database.
;               
; Explanation: 
;               
; Use       : IDL> check_img_scripts,'YYYYMMDD','YYYYMMDD'
;    
; Inputs    :	dtst= starting day yyyy-mm-dd (or yyyymmdd)
;		dtend= ending day yyyy-mm-dd 
;               
; Outputs   : 
;
; Keywords  : mdays = if set will change dtst to dtend minus mdays.  dtend defaults to dtst if not set.
;             docpimgdb = run cpimgdb if script files are moved back to the db directory.
;   	      _EXTRA  
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need permissions write in /net/lambda/data/mysql/mysql_x/databases/secchi/IMG/
;               
; Side effects: 
;               
; Category    : database
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2013
;               
; $Log: check_img_scripts.pro,v $
; Revision 1.2  2013/03/15 15:30:23  secchib
; changed database dir
;
; Revision 1.1  2013/03/15 10:30:08  mcnutt
; new checks database for img scripts if not found moves szip script files back to the db directory
;
;

ut0=nrlanytim2utc(dtst)
IF n_params() LT 2 THEN BEGIN
    ut1=ut0
ENDIF ELSE ut1=nrlanytim2utc(dtend)
IF keyword_set(mdays) THEN ut0.mjd=ut1.mjd-mdays

runcpimgdb=0
dbdir=getenv('seb_img')+'/db/'
mjd=ut0
ab=getenv('sc');aorb ;['a','b']
FOR i=ut0.mjd,ut1.mjd DO BEGIN
   mjd.mjd=i
   yyyymmdd=utc2yymmdd(mjd,/yyyy)
   zipscripts=file_search(dbdir+'attic/*'+yyyymmdd+'*.script.gz')
   if zipscripts[0] ne '' then begin 
      BREAK_FILE, zipscripts, a, outdir, zpname, outext
      for n=0,n_Elements(zpname)-1 do begin
        dbscripts=file_search('/net/earth/lasco/corona/data1/database/secchi/IMG/INSERTS/POST_LAUNCH/IN_DB/'+zpname[n]+'.script')
        if dbscripts[0] eq '' then begin
            runcpimgdb=1
	    FILE_MOVE,zipscripts[n],dbdir,/verbose
            syscmnd='gunzip '+dbdir+zpname[n]+'.script'
	    spawn,syscmnd
	  endif
       endfor
   endif
ENDFOR
if keyword_set(docpimgdb) and runcpimgdb eq 1 then begin
   cpdb_cmd =  concat_dir(getenv('ITOS_GROUPDIR'),'bin/cpimgdb ')
   spawn,cpdb_cmd,/sh
endif

end
