
FUNCTION SECCHI_DOORS_CONVERT, door_data, cnv_str

  d0= WHERE(door_data EQ 0, c0)
  d1= WHERE(door_data EQ 1, c1)
  d2= WHERE(door_data EQ 2, c2)
  d3= WHERE(door_data EQ 3, c3)
  d4= WHERE(door_data EQ 4, c4)
  d5= WHERE(door_data GE 5, c5)  ; 5-7 range for HB_DOOR_ENCSEL_DSC = OFF
  
  door_pos= STRARR(N_ELEMENTS(door_data))

  CASE cnv_str OF 
    'HB_HIDOR_ENC_DSC': BEGIN
        IF (c0 GT 0) THEN door_pos(d0)= STRTRIM(door_data(d0),2)+'=Closed   '
        IF (c1 GT 0) THEN door_pos(d1)= STRTRIM(door_data(d1),2)+'=InTransit'
        IF (c2 GT 0) THEN door_pos(d2)= STRTRIM(door_data(d2),2)+'=Error    '
        IF (c3 GT 0) THEN door_pos(d3)= STRTRIM(door_data(d3),2)+'=Open/Off '               
       END
    'HB_SCIPDOR_ENC_DSC': BEGIN
        IF (c0 GT 0) THEN door_pos(d0)= STRTRIM(door_data(d0),2)+'=InTransit'               
        IF (c1 GT 0) THEN door_pos(d1)= STRTRIM(door_data(d1),2)+'=Closed   '
        IF (c2 GT 0) THEN door_pos(d2)= STRTRIM(door_data(d2),2)+'=Open     '
        IF (c3 GT 0) THEN door_pos(d3)= STRTRIM(door_data(d3),2)+'=Off      '
       END
    'HB_DOOR_ENCSEL_DSC': BEGIN
        IF (c0 GT 0) THEN door_pos(d0)= STRTRIM(door_data(d0),2)+'=Off '               
        IF (c1 GT 0) THEN door_pos(d1)= STRTRIM(door_data(d1),2)+'=COR2'
        IF (c2 GT 0) THEN door_pos(d2)= STRTRIM(door_data(d2),2)+'=COR1'
        IF (c3 GT 0) THEN door_pos(d3)= STRTRIM(door_data(d3),2)+'=EUVI'
        IF (c4 GT 0) THEN door_pos(d4)= STRTRIM(door_data(d4),2)+'=HI  '
        IF (c5 GT 0) THEN door_pos(d5)= STRTRIM(door_data(d5),2)+'=Off '
       END
    ELSE: BEGIN
        PRINT,'Invalid conversion string: '+cnv_str
        door_pos(0)='Invalid conversion string: '+cnv_str 
       END
  ENDCASE

  RETURN,door_pos

END
