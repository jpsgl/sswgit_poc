PRO SE_GET_ALL_FITS,Result
;+
; NAME:
;	SE_GET_ALL_FITS
;
; PURPOSE:
;       This procedure returns an array of all FITS file names
;
; CATEGORY:
;	UTILITY
;
; CALLING SEQUENCE:
;	SE_GET_ALL_FITS,Result
;
; INPUTS:
;       NONE
;
; OUTPUTS
;	Result is a string array of all FITS files with FTS, FIT, FITS
;	extensions.
;
; RESTRICTIONS:
;	None.
;
; MODIFICATION HISTORY:
; 	Written by:	R.A. Howard Jan 21, 2005.
;
;	%W% %H% SECCHI IDL LIBRARY
;-

lista = FINDFILE('*.fit')
listb = FINDFILE('*.fts')
listc = FINDFILE('*.fits')
IF (lista(0) NE '') then list=lista ELSE list=''
IF (listb(0) NE '') THEN BEGIN
   IF (list(0) NE '') THEN list=[list,listb] $
   ELSE list=listb
ENDIF
IF (listc(0) NE 0)  THEN BEGIN
   IF (list(0) NE 0)  THEN list=[list,listc] $
   ELSE list=listc
ENDIF
Result=list
RETURN
END
