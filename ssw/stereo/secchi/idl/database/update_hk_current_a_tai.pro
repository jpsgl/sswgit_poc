pro update_hk_current_a_tai,dte

  tai= utc2tai(str2utc(dte))

  n=n_elements(dte)

  openw,1,'hk_current_a_tai_update.script'

  printf,1,'use secchi_hk_A;'
  for i=0L, n-1 do begin
    printf,1,'update hk_current_a set tai='+string(tai(i),'(i11)')+' where pckt_time="'+tm(i)+'";'

  end

  close,1

end

