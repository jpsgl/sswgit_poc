; $Id: plot_rows.pro,v 1.5 2011/12/01 21:05:37 nathan Exp $
;
;$Log: plot_rows.pro,v $
;Revision 1.5  2011/12/01 21:05:37  nathan
;use select_windows for windows compat
;
;Revision 1.4  2008/07/24 18:09:05  esfand
;doubled utplot ydata to take care of linux blank plots
;
;Revision 1.3  2007/03/30 22:05:22  thompson
;Added calls to allow_font() and get_font
;
;Revision 1.2  2007/01/16 23:27:37  nathan
;fix call to GET_CONVERT_STRING after removing reference to NRL_LIB/dev
;
; Ed Esfandiari September 2004 - Adpated for SECCHI
;               February  2005 - Changed call to minmax to min and max
;                                (minmax no longer works with idl5.3).
;               January   2006 - Corrected min,max values for plots.
;
;               July      2006 - Added code to save plot data points as idl saveset.
;               July      2008 - doubled utplot ydata to take care of linux black plots.
;

PRO PLOT_DATA, adj_xscale=adj_xscale, adj_yscale=adj_yscale
 common disp_hk_sdata, data_rows,selected_cols,ps_save_filename,printer_name, $
        t,equals,tags,ntags,elem,new_Cols,tag_select,num_param,message, plot_save_filename, $
        old_select,viewed,png_save_filename,db_sc,plt_select,units,punits,useunit,plotunit, $
        new_cf,new_ut,tagscf,tagsut,plt_cf,plt_ut, ymn, ymx, diag, tme, ct, eng_tags, yout, csize, $
        cur_x1, cur_x2, x1p, x2p, ydata, all_tme 

            widget_control,/hour
            fields= where(tag_select eq 1,tcnt)
            ;if(tcnt eq 1) then begin
            if(tcnt eq 1 or tcnt gt 5) then begin
              msg='Only 1 to 4 params may be selected for plotting vs PCKT_TIME.'
              res=widget_message(msg,title='No Selection')
            endif else begin
              viewed= 'true'
              ;tmp= where(tag_select ne old_select,scnt) 
              tmp= where(tag_select ne plt_select,scnt) 
              ;if(scnt gt 0) then begin
              ;if(scnt gt 0 OR useunit ne plotunit) then begin
              if(scnt gt 0 OR useunit ne plotunit OR $
                 keyword_set(adj_xscale) OR keyword_set(adj_yscale)) then begin
                new_tags= tags(fields)
                num_cols= n_elements(fields)
                new_cols= ''
                ;IF (datatype(tme) EQ 'UND' OR n_elements(tme) NE elem) THEN BEGIN 
                IF (datatype(all_tme) EQ 'UND' OR n_elements(all_tme) NE elem) THEN BEGIN 
                  widget_control,diag,set_value='Converting '+strtrim(elem,2)+ $
                   ' Date Strings to UTC; This Is Done Only Once And May Take Some Time - Please Be Patient.......................'
                  ;tme= str2utc(t(0,*))
                  ;all_tme= str2utc(t(0,*))
                  all_tme= TAI2UTC(DOUBLE(t(0,*)),/NOCORRECT)
                ENDIF
                ;window,retain=2
                erase
                ;tme= str2utc(t(0,*))
                !x.style= 1
                case num_cols of
                  2: !p.multi=[0,0,0,0,0]
                  3: !p.multi=[0,1,2,0,1]
                  4: !p.multi=[0,1,3,0,1]
                  5: !p.multi=[0,1,4,0,1]
                endcase

                widget_control,diag,set_value='Plotting Selected Parameters.......................'

                CASE num_cols OF 
                  2: BEGIN  ; 1 plot per page
                     yout=[1.0,0.2]
                     csize=1.0
                    END
                  3: BEGIN  ; 2 plots per page
                     yout=[1.0,0.65,0.15]
                     csize=1.2
                    END
                  4: BEGIN  ; 3 plots per page
                     yout=[1.0,0.76,0.44,0.1]
                     csize=1.8
                    END
                  5: BEGIN  ; 4 plots per page
                     yout=[1.0,0.85,0.6,0.35,0.1]
                     csize=2.0
                    END
                ENDCASE

                for j= 1, num_cols-1 do begin
                  ;plot first col (pckt_time) vs each of remaining columns:
                  ; there are "elem" rows of data:

                  ;utplot,tme, t(fields(j),*) * new_cf(fields(j)),ytitle=new_tags(j)+new_ut(fields(j)), $
                  ;       title='SECCHI HouseKeeing '+db_sc,yrange=minmax(t(fields(j),*)*new_cf(fields(j)))

                  ; minmax no longer works with idl5.3 (generates errors):

                  ;mn= MIN(t(fields(j),*)*new_cf(fields(j)))
                  ;mx= MAX(t(fields(j),*)*new_cf(fields(j)))

                  ;utplot,tme, t(fields(j),*) * new_cf(fields(j)), $
                  ;        ;title=db_sc+'  '+new_tags(j)+new_ut(fields(j)),yrange=[mn,mx], $
                  ;        title=db_sc+'  '+new_tags(j)+new_ut(fields(j)), $
                  ;        yrange=[ymn(j-1),ymx(j-1)], charsize=2 

                  IF (useunit EQ 0) THEN $
                    ydata= t(fields(j),*) $
                  ELSE $
                    ydata= ct(fields(j),*)

                  tme= all_tme(x1p:x2p)
                  ydata= ydata(x1p:x2p)

                  IF (STRPOS(new_ut(fields(j)),'=') GT 0) THEN BEGIN
                    ; add door pos description for converted door mnemonics to plot:
                    utplot,tme, double(ydata), title=db_sc+'  '+new_tags(j)+eng_tags(fields(j)), $
                           yrange=[ymn(j-1),ymx(j-1)], charsize= csize, psym=2 
                           xyouts,0.2,yout(j),new_ut(fields(j)),/NORMAL
                  ENDIF ELSE BEGIN ; non-door mnemonics or not converted doors
                    utplot,tme, double(ydata), title=db_sc+'  '+new_tags(j)+eng_tags(fields(j)), $
                           ytitle=new_ut(fields(j)), yrange=[ymn(j-1),ymx(j-1)], charsize= csize
                  ENDELSE

                endfor

                !p.multi=[0,0,0,0,0]
                plt_cf= new_cf
                plt_ut= new_ut
                plotunit= useunit 
                plt_select= tag_select
                old_select= tag_select

                widget_control,diag,set_value='Done Plotting'
                
              endif
            endelse

  RETURN
END


pro plot_rows_event,event
 common disp_hk_sdata, data_rows,selected_cols,ps_save_filename,printer_name, $
        t,equals,tags,ntags,elem,new_Cols,tag_select,num_param,message, plot_save_filename, $
        old_select,viewed,png_save_filename,db_sc,plt_select,units,punits,useunit,plotunit, $
        new_cf,new_ut,tagscf,tagsut,plt_cf,plt_ut, ymn, ymx, diag, tme, ct, eng_tags, yout, csize, $
        cur_x1, cur_x2, x1p, x2p, ydata, all_tme 

 WIDGET_CONTROL, event.id, GET_UVALUE=uval
 WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE

 case (uval) of
  'done': begin
            WIDGET_CONTROL, /DESTROY, struct.base
          end
  'units': begin
             useunit= event.index
             ;help,punits(useunit)
             new_ut= tagsut(*,useunit)
             ;new_cf= tagscf(*,useunit)
             widget_control,diag,set_value='New Unit Selected. Now press "Plot Selected Params".'

           end
  'plt': begin
            tg= where(tag_select EQ 1, tgc)
            IF (tgc LE 1 OR tgc GT 5) THEN BEGIN
              msg='Only 1 to 4 params may be selected for plotting vs PCKT_TIME.'
              res=widget_message(msg,title='No Selection')
              widget_control,diag,set_value='Select Parameters To Be Plotted' 
              RETURN
            ENDIF 

            useunit= event.index
            new_ut= tagsut(*,useunit)
            
            widget_control,diag,set_value='Setting Up Plot Parameters......................'
            ymn=fltarr(tgc-1)
            ymx=fltarr(tgc-1)
            for i=1,tgc-1 do begin
              IF (useunit EQ 0) THEN BEGIN
                ymn(i-1)= min(LONG(t(tg(i),*)))
                ymx(i-1)= max(LONG(t(tg(i),*)))
              ENDIF ELSE BEGIN
                ymn(i-1)= min(FLOAT(ct(tg(i),*)))
                ymx(i-1)= max(FLOAT(ct(tg(i),*)))
              ENDELSE
            endfor
            PLOT_DATA
          end

  'adj_xs': begin
            if (viewed eq 'false') then begin
              msg= ['Plot the data first.']
              res=widget_message(msg,title='Note:')
              return
            end
            tg= where(tag_select EQ 1, tgc)
            tmp= where(tag_select ne plt_select,scnt) 
            IF (scnt EQ 0) THEN BEGIN
              res= NEW_XSCALE(t(0,0),t(0,elem-1),cur_x1,cur_x2)
              if (res) then begin 
                 widget_control,diag,set_value='Setting Up Plot Parameters......................'
                 xrng= WHERE(t(0,*) GE cur_x1 AND t(0,*) LE cur_x2, rcnt)
                 IF (rcnt GT 1) THEN BEGIN
                   x1p= xrng(0)
                   x2p= xrng(rcnt-1)
                   PLOT_DATA, /adj_xscale
                 ENDIF ELSE BEGIN
                   widget_control,diag,set_value='Invalid X-range - Try again.'
                   return 
                 ENDELSE
              endif else widget_control,diag,set_value='Adjust X-Scale Command Cancelled'
            ENDIF ELSE BEGIN
              widget_control,diag,set_value='New Paramaters Selected - Use "Plot Selected Params" Before Changing Scale'
            ENDELSE
          end

  'save_pdata': begin
              ; fs= where(tag_select eq 1,fcnt)
              ; if (fcnt lt 2) then begin
              ;   msg= ['Empty Plot - Nothing to Save.']
              ;   res=widget_message(msg,title='Note:')
              ;   return
              ; endif else begin
              ;   if (fcnt gt 1) then begin
              ;     plot_tags= tags(fs) 
              ;     if (useunit eq 0) then $
              ;       plot_data= t(fs,*)    $
              ;     else                    $
              ;       plot_data= ct(fs,*)
              ;     utc_times= tme
              ;     help,tme,plot_data, plot_save_filename 
              ;     stop
              ;   endif
              ; endelse

           if (viewed eq 'false') then begin
             msg= ['Plot the data first. It will configure the layout',$
                   'for Save options.']
             res=widget_message(msg,title='Note:')
             return
           end
           msg=['Latest (displayed) plotted data will be saved.',$
                '',$
                'Data will be saved in "'+plot_save_filename(0)+'" containing',$
                '   "utc_times", "plot_tags", and "plot_data".',$
                '',$
                '              Proceed with Save ?']
           res=widget_message(msg,/question,title='Confirm')
           if(res eq 'Yes') then begin
              widget_control,/hour
              widget_control,diag,set_value='Saving plot to '+plot_save_filename(0)+'.......' 
              plot_save= strtrim(plot_save_filename(0),2)
              fs= where(tag_select eq 1,fcnt)
              plot_tags= tags(fs) 
              if (useunit eq 0) then $
                plot_data= t(fs,*)    $
              else                    $
                plot_data= ct(fs,*)
              utc_times= tme
              save, filename=plot_save,utc_times,plot_tags,plot_data
              widget_control,diag,set_value='Done saving '+plot_save
            endif
          end

  'adj_ys': begin
            if (viewed eq 'false') then begin
              msg= ['Plot the data first.']
              res=widget_message(msg,title='Note:')
              return
            end
            tg= where(tag_select EQ 1, tgc)
            tmp= where(tag_select ne plt_select,scnt) 
            IF (scnt EQ 0) THEN BEGIN
              res= new_yscale(tgc-1,ymn,ymx,tags(tg(1:*)))
              if (res) then begin 
                 widget_control,diag,set_value='Setting Up Plot Parameters......................'
                 PLOT_DATA,/adj_yscale
              endif else widget_control,diag,set_value='Adjust Y-Scale Command Cancelled'
            ENDIF ELSE BEGIN
              widget_control,diag,set_value='New Paramaters Selected - Use "Plot Selected Params" Before Changing Scale'
            ENDELSE
          end

  'save_png': begin
           if (viewed eq 'false') then begin
             msg= ['Plot the data first. It will configure the layout',$
                   'for Save options.']
             res=widget_message(msg,title='Note:')
             return
           end
           msg=['Latest (displayed) plotted data will be saved.',$
                '',$
                'Data will be saved in "'+png_save_filename(0)+'".',$
                '',$
                '                 Proceed with Save ?']
           res=widget_message(msg,/question,title='Confirm')
           if(res eq 'Yes') then begin
              widget_control,/hour
              widget_control,diag,set_value='Saving plot to '+png_save_filename(0)+'.......' 
              png_save= png_save_filename(0)
              png_save= strmid(png_save,0,STRPOS(png_save,'.'))
              img = EDFTVREAD(/PNG,FILENAME=png_save)
              ; Now, image is captured in png_save+.png (new .png is added to png_save by EDFTVREAD)
              ; and img is -1.
              widget_control,diag,set_value='Done saving '+png_save_filename(0)
            endif
          end
  'save_ps': begin
           if (viewed eq 'false') then begin
             msg= ['Plot the data first. It will configure the layout',$
                   'for Save options.']
             res=widget_message(msg,title='Note:')
             return
           end
           msg=['Latest (displayed) plotted data will be saved.',$
                '',$
                'Data will be saved in "'+ps_save_filename(0)+'".',$
                '',$
                '                 Proceed with Save ?']
           res=widget_message(msg,/question,title='Confirm')
           if(res eq 'Yes') then begin
             widget_control,/hour
             widget_control,diag,set_value='Saving plot to '+ps_save_filename(0)+'.......' 
             set_plot,'ps'
             device,/landscape,file=ps_save_filename(0)

             ;fields= where(tag_select eq 1,scnt) 
             fields= where(plt_select eq 1,scnt)  ; to use currently displayed plot.
             if(scnt gt 0) then begin
                new_tags= tags(fields)
                num_cols= n_elements(fields)
                new_cols= ''
                ;tme= str2utc(t(0,*))
                ;all_tme= str2utc(t(0,*))
                !x.style= 1
                case num_cols of
                  2: !p.multi=[0,0,0,0,0]
                  3: !p.multi=[0,1,2,0,1]
                  4: !p.multi=[0,1,3,0,1]
                  5: !p.multi=[0,1,4,0,1]
                endcase
                for j= 1, num_cols-1 do begin
                  ;plot first col (pckt_time) vs each of remaining columns:
                  ; there are "elem" rows of data:

                  ;utplot,tme, t(fields(j),*) * new_cf(fields(j)),ytitle=new_tags(j)+new_ut(fields(j)), $
                  ;       title='SECCHI HouseKeeing '+db_sc,yrange=minmax(t(fields(j),*)*new_cf(fields(j)))

                  ; minmax no longer works with idl5.3 (generates errors):

                  ;mn= MIN(t(fields(j),*)*new_cf(fields(j)))
                  ;mx= MAX(t(fields(j),*)*new_cf(fields(j)))
                  ;utplot,tme, t(fields(j),*) * new_cf(fields(j)), $
                  ;        title=db_sc+'  '+new_tags(j)+new_ut(fields(j)),yrange=[mn,mx], $
                  ;        charsize=1.2

                  ;utplot,tme, t(fields(j),*) * new_cf(fields(j)), $
                  ;        title=db_sc+'  '+new_tags(j)+new_ut(fields(j)), $
                  ;        yrange=[ymn(j-1),ymx(j-1)], charsize=1.2 

                  ; need to have these ydata calculations since it chages with every j:

                  IF (useunit EQ 0) THEN $
                    ydata= t(fields(j),*) $
                  ELSE $
                    ydata= ct(fields(j),*)

                  ydata= ydata(x1p:x2p)

                  ;utplot,tme, ydata, title=db_sc+'  '+new_tags(j)+eng_tags(fields(j)), $
                  ;       ytitle=new_ut(fields(j)), yrange=[ymn(j-1),ymx(j-1)], charsize=2

                  IF (STRPOS(new_ut(fields(j)),'=') GT 0) THEN BEGIN
                    ; add door pos description for converted door mnemonics to plot:
                    utplot,tme, double(ydata), title=db_sc+'  '+new_tags(j)+eng_tags(fields(j)), $
                           yrange=[ymn(j-1),ymx(j-1)], psym=2 
                           xyouts,0.2,yout(j),new_ut(fields(j)),/NORMAL
                  ENDIF ELSE BEGIN ; non-door mnemonics or not converted doors
                    utplot,tme, double(ydata), title=db_sc+'  '+new_tags(j)+eng_tags(fields(j)), $
                           ytitle=new_ut(fields(j)), yrange=[ymn(j-1),ymx(j-1)]
                  ENDELSE
                endfor

                device,/close_file
                select_windows	
                !p.multi=[0,0,0,0,0]
                widget_control,diag,set_value='Done saving '+ps_save_filename(0)
             endif
           end ;confirm
          end
  'ps_save_name': begin
                 WIDGET_CONTROL, event.id ,GET_VALUE= ps_save_filename
                 ps_save_filename= strtrim(ps_save_filename,2)
               end
  'png_save_name': begin
                 WIDGET_CONTROL, event.id ,GET_VALUE= png_save_filename
                 png_save_filename= strtrim(png_save_filename,2)
               end
  'plot_save_name': begin
                 WIDGET_CONTROL, event.id ,GET_VALUE= plot_save_filename
                 plot_save_filename= strtrim(plot_save_filename,2)
              end
  'hcopy': begin
           if (viewed eq 'false') then begin
             msg= ['View the data first. It will configure the layout',$
                   'for Save options.']
             res=widget_message(msg,title='Note:')
             return
           end
            msg=['Latest viewed data will be printed. Changes to the selected',$
                'available columns made after the last view will be ignored.',$
                '',$
                'Printer to be used is "'+printer_name(0)+'".',$
                '',$
                '                  Proceed with Print?']
            res=widget_message(msg,/question,title='Confirm')
            if(res eq 'Yes') then begin
             widget_control,/hour
             openw,unit,'print_data.db',/get_lun
             for i=0L,n_elements(new_cols)-1 do printf,unit,new_cols(i) 
             free_lun,unit
             lpr= "lpr -P "+printer_name(0)+" print_data.db"
             spawn,lpr
             openr,unit,'print_data.db',/get_lun,/delete
             free_lun,unit
            end; confirm
           end
  'lp_name': begin
               WIDGET_CONTROL, event.id ,GET_VALUE= printer_name
               printer_name= strtrim(printer_name,2)
             end
   'select': begin
               old_select= tag_select
               ind = WHERE(struct.button_id EQ event.id)
               IF (event.select) THEN $
                  tag_select(ind)= 1 $
               ELSE $
                  tag_select(ind)= 0
             end
 endcase

end


pro plot_rows, st, info
 common disp_hk_sdata, data_rows,selected_cols,ps_save_filename,printer_name, $
        t, equals,tags,ntags,elem, new_cols,tag_select,num_param,message, plot_save_filename, $
        old_select, viewed, png_save_filename, db_sc, plt_select, units, punits, useunit, plotunit, $
        new_cf,new_ut,tagscf,tagsut,plt_cf,plt_ut, ymn, ymx, diag, tme, ct, eng_tags, yout, csize, $
        cur_x1, cur_x2, x1p, x2p, ydata, all_tme

  num_param=n_params()
  if(num_param lt 1) then return

  PRINT,''
  PRINT,' Reading/formatting input.......'
  PRINT,''

; defaults:
  selected_cols= 'all'
  ps_save_filename= 'plothk.ps'
  png_save_filename= 'plothk.png'
  plot_save_filename= 'plothk.sav'
  printer_name= 'hp4-247'
  viewed= 'false'
  print,''  
  s=size(st)

  db_sc= ''

  if(s(0) eq 1 and s(2) eq 8) then begin ; input is a strucutre
    tags=tag_names(st)

    tname= tag_names(st,/structure_name)
    if(STRLEN(tname) gt 0) THEN $
      ;db_sc= '('+strmid(tname,0,strpos(tname,'$'))+')' $
      db_sc= strmid(tname,0,strpos(tname,'$')) $
    else $
      db_sc= ''

;   first, pickup the camera field values because it is needed for "filter" and 
;   "polar" calculation when conversion to original strings is specified.
 
    camera_cnt=0
    camera_field=0
    for i= 0, n_elements(tags)-1 do begin
      pos= strpos(tags(i),'CAMERA')
      if(pos ge 0 and (pos+strlen('CAMERA')) eq strlen(tags(i))) then begin
        camera_cnt= 1
        out= execute("camera_field= st."+tags(i))
      end
    end
 
    ntags= n_elements(tags)
    maxl= max(strlen(tags))+2
    out= execute("s= size(st."+tags(0)+")") ; get # of elements (rows)
    elem= s(n_elements(s)-1)
    tot_rows= elem
;    if(elem gt 2000) then begin
;      print,'Too many rows of data. Processing to display the first 2000 rows.'
;      elem=2000
;    end
    t= strarr(ntags,elem) 
    obs_nums= STRARR(elem)+'-1'
    oind= WHERE(tags EQ 'OBS_PROG',ocnt)
    IF (ocnt GT 0) THEN obs_nums= st.obs_prog(oind)
    for j=0,ntags-1 do begin
      out= execute("cur_field_vals= st."+tags(j))
      ;if(strlen(cur_field_vals(0)) gt 500) then begin  ;img_browse column, keep 
      ;  cur_field_vals= strmid(cur_field_vals,0,10)+'...' ; first 10 characters.
      ;end
      cur_field_vals= cur_field_vals(0:elem-1) ;in case more than 2000 elements.

      ;t(j,*)= cur_field_vals
      ;t(j,*)= convert_to_orig_secchi(cur_field_vals,tags(j),camera_cnt,camera_field,elem,obs_nums)

;help,cur_field_vals
;print,cur_field_vals(0:2)

      t(j,*)= TEMPORARY(STRTRIM(cur_field_vals,2))
      IF (camera_cnt GT 0) THEN t(j,*)= convert_to_orig_secchi(transpose(t(j,*)),tags(j),camera_cnt,camera_field,elem,obs_nums)
    end

    ;for j=0,ntags-1 do begin
    ;  maxtlen= max(strlen(t(j,*)))
    ;  taglen= strlen(tags(j))
    ;  if(taglen lt maxtlen) then for i=1,maxtlen-taglen do tags(j)=tags(j)+' '
    ;  if(taglen gt maxtlen) then maxtlen= taglen
    ;  mintlen= min(strlen(t(j,*)))
    ;  if(mintlen lt maxtlen) then t(j,*)= adjst_strarr_len(t(j,*),maxtlen)
    ;end

    data_rows= ''
    for j=0,ntags-1 do data_rows= data_rows+tags(j)+'   '
    equals='==================================================================='
    divider= ''
    for j=0,ntags-1 do divider= divider+strmid(equals,0,strlen(tags(j)))+'   '
    data_rows= [data_rows,divider]
;    for i=0L,elem-1 do begin
;      this_row= ''
;      for j=0,ntags-1 do this_row= this_row+t(j,i)+'   '
;      data_rows= [data_rows,this_row]
;    end
    new_cols= data_rows

    font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
    if not allow_font(font) then get_font, font

    ;base= widget_base(title='PLOT HK Options',xsize=600,ysize=400,xoffset=10,/frame)
    ;base= widget_base(title='PLOT HK Options',xsize=1200,ysize=600,xoffset=10,/frame)
    base= widget_base(title='PLOT HK Options',/frame)
    ;b1=widget_base(base,/ROW)
    b0= widget_base(base,/COL)
    b1= widget_base(b0,/ROW)
    colb1= widget_base(b1,/COLUMN)
    rowb1= widget_base(colb1,/ROW)
;    lab=WIDGET_LABEL(rowb1,VALUE='                             ')
    exit=  WIDGET_BUTTON(rowb1, VALUE='Exit',UVALUE='done',FONT=font,/frame)
    r1= widget_base(colb1,/ROW)
    com='                                                                    '
    lab=WIDGET_LABEL(colb1,VALUE=com)
    rr= widget_base(colb1,/ROW)
;    lab=WIDGET_LABEL(rr,VALUE='          ')
    
    ;select= WIDGET_BUTTON(rr, UVALUE='plt',value=' Plot Selected Params ')

    ;lab= WIDGET_LABEL(rr,VALUE='Plot Unit:')
    ;punits= ['DN','Convert']

    punits= ['Plot Selected Params (DN)','Plot Selected Params (Convert)']
    useunit= 0 ; DN
    plotunit= useunit
    tagscf= FLTARR(ntags,2)
    tagsut= STRARR(ntags,2)
    tagscf(*,0) = 1.0
    tagsut(*,0)= 'DN'

    ;unit= WIDGET_DROPLIST(rr,VALUE= punits, UVALUE='units')

    select= WIDGET_DROPLIST(rr,VALUE= punits, UVALUE='plt', FONT=font)


;    lab= WIDGET_LABEL(rr,VALUE='-> columns:')
;    select= WIDGET_TEXT(rr,UVALUE='col_sel',VALUE='all',/EDITABLE,/ALL_EVENTS)

    r2= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(r2,VALUE='  ')
    select= WIDGET_BUTTON(r2, UVALUE='save_ps',VALUE=' Save Plot as PS  ',FONT=font)
    lab= WIDGET_LABEL(r2,VALUE=' in ')
    select= WIDGET_TEXT(r2,UVALUE='ps_save_name',VALUE='plothk.ps',/EDITABLE, $
                        /ALL_EVENTS,xsize=15)

    rr=widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(rr,VALUE='  ')
    select= WIDGET_BUTTON(rr, UVALUE='save_png',VALUE='Save Plot as PNG',FONT=font)
    lab= WIDGET_LABEL(rr,VALUE=' in ')
    select= WIDGET_TEXT(rr,UVALUE='png_save_name',VALUE='plothk.png',/EDITABLE, $
                        /ALL_EVENTS,xsize=15)

    rs= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(rs,VALUE='  ')
    select= WIDGET_BUTTON(rs, UVALUE='save_pdata',VALUE='Save Plotted Data as SaveSet',FONT=font)
    lab= WIDGET_LABEL(rs,VALUE=' in ')
    select= WIDGET_TEXT(rs,UVALUE='plot_save_name',VALUE='plothk.sav',/EDITABLE, $
                        /ALL_EVENTS,xsize=15)


    r3= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(r3,VALUE='          ')
;    select= WIDGET_BUTTON(r3, UVALUE='hcopy',VALUE='Print Plot  ',FONT=font)
;    lab= WIDGET_LABEL(r3,VALUE='-> printer: ')
;    select= WIDGET_TEXT(r3,UVALUE='lp_name',VALUE='hp4-247',/EDITABLE, $
;                        /ALL_EVENTS)

    rc= widget_base(colb1,/COLUMN)
    com=''
    lab= WIDGET_LABEL(rc,VALUE=com)
    ;com='(Only selected params are used in Plot and Save options.)'
    com='Max of 4 parameters per plot'
    lab= WIDGET_LABEL(rc,VALUE=com)
    ;b2= widget_base(colb1,/frame)
    b2= widget_base(rc,/COL,/frame)
    r4= widget_base(b2,/column,x_scroll_size=300,y_scroll_size=400,/FRAME)
    com='Available Params:  ' 
    lab= WIDGET_LABEL(r4,VALUE=com,FONT=font)
    tags= STRTRIM(tags,2)

    ;b_id= lonarr(ntags)
    ;tag_select= intarr(ntags) 
    ;old_select= tag_select
    ;plt_select= tag_select
    ;tags= STRTRIM(tags,2)

    pind= where(strlowcase(tags) eq 'unix_time',pcnt)
    IF (pcnt EQ 0) THEN BEGIN
      PRINT,'No unix_time. Look for pckt_time, instead...'
      pind= where(strlowcase(tags) eq 'pckt_time',pcnt)
      IF (pcnt EQ 0) THEN BEGIN
        PRINT,'ERROR - HK structure does not have pckt_time or unix_time fields. Can''t plot.'
        return
      ENDIF ELSE BEGIN
        ;Note: Must change packt_times from string to tai with NOCORRECT at this point:
        ;      since from this point on, times are expected to be in tai format only:
        t(pind,*)= STRING(long(utc2tai(str2utc(t(pind,*)),/NOCORRECT)))
      ENDELSE
    ENDIF ELSE BEGIN

      ; unix time exists. if 'pckt_time' also exists remove it. Also change 'unix_time'
      ; tag name to 'PCKT_TIME':

      npt= WHERE(strlowcase(tags) NE 'pckt_time',nptcnt)
      IF (nptcnt GT 0) THEN  BEGIN 
        tags= tags(npt) 
        t= TEMPORARY(t(npt,*))
      ENDIF
      utm= where(strlowcase(tags) eq 'unix_time')
      tags(utm(0))= 'PCKT_TIME'

      ; TAI2UTC(double(t(utm(0),*))+378691200d0,/NOCORRECT) 

      ; change unix_time to tai (but kep as string with percise long type (not double):
      t(utm(0),*)= TEMPORARY(STRING(long(t(utm(0),*))+378691200L))
    ENDELSE

    ntags= N_ELEMENTS(tags)
    b_id= lonarr(ntags)
    tag_select= intarr(ntags) 
    old_select= tag_select
    plt_select= tag_select
    tags= STRTRIM(tags,2)

    ; now only need to use TAI2UTC(double(t(utm(0),*)),/NOCORRECT) to change to utc

;print,'Note: pckt_time as this point in now in tai format so remove str2utc stuff when'
;print,'      plotting later and use /NOCORRECT,/ECS keywords with TAI2UTC instead. '

;stop

    pind= where(strlowcase(tags) eq 'pckt_time',pcnt)
    ; move pckt_time to the front, if not so already:   
    pind= pind(0)
    IF (pind GT 0) THEN BEGIN
      tm= t(pind,*)
      t(1:pind,*)= TEMPORARY(t(0:pind-1,*))
      t(0,*)= TEMPORARY(tm)

      tmtag= tags(pind)
      tags(1:pind)=tags(0:pind-1)
      tags(0)=tmtag

      tcf= tagscf(pind,*)
      tagscf(1:pind,*)= tagscf(0:pind-1,*)
      tagscf(0,*)= tcf

      tut= tagsut(pind,*)
      tagsut(1:pind,*)= tagsut(0:pind-1,*)
      tagsut(0,*)= tut
    ENDIF

    new_cf= tagscf(*,useunit)
    new_ut= tagsut(*,useunit)

    ; Now convert data, t, to proper units and save as ct:
    
    ct= t

    eng_tags= STRARR(ntags)
    For i=1,ntags-1 DO BEGIN
      english= GET_CONVERT_STRING(tags(i),GETENV('SSW_SECCHI')+'/idl/database/hk1067_mnemonics_info.txt',/english)
      eng_tags(i)= ' ('+english+')' 
      cnv_str= GET_CONVERT_STRING(tags(i),GETENV('SSW_SECCHI')+'/idl/database/hk1067_mnemonics_info.txt')
      SECCHI_DN_CONVERT, TRANSPOSE(t(i,*)), REPLICATE(cnv_str,tot_rows), dn_cnvt, cnv_units
      ct(i,*)= TEMPORARY(dn_cnvt)
      ;tagsut(i,1)= cnv_units(0)
      srt= uniq_nosort(cnv_units) ; keep unique door descriptions
      srtunits= STRTRIM(cnv_units(srt),2)
      srtunits= srtunits(sort(srtunits))
      tagsut(i,1)= srtunits(0)
      FOR j= 1, N_ELEMENTS(srt)-1 DO $
        tagsut(i,1)= tagsut(i,1)+','+srtunits(j)
    ENDFOR

    font1= 'adobe-courier-medium-r-normal--0-0-75-75-m-0-iso8859-1'
    font1= '-b&h-lucida bright-demibold-r-normal--0-120-75-75-p-0-iso8859-1'
    font1= '8x12'

    for i=0,ntags-1 do begin
      tag_select(0)= 1 ; set tag_select default for pckt_time to selected (on) 
      row2 = widget_base(r4, /COL ,/NONEXCLUSIVE)
      b_id(i) = widget_button(row2,VALUE=tags(i),UVALUE='select',FONT=font)
      IF (i GT 0) THEN BEGIN
        row3= widget_base(r4, /ROW)
        txt= widget_label(row3, VALUE= '    '+eng_tags(i),FONT='6x12')
      ENDIF
    end

;    for i=0,ntags-1 do begin
;      tag_select(0)= 1 ; set tag_select default for pckt_time to selected (on) 
;      row1= widget_base(r4, /ROW)
;      row2 = widget_base(row1, /ROW ,/NONEXCLUSIVE)
;      trow= widget_base(row1,/column)
;      b_id(i) = widget_button(row2,VALUE=tags(i),UVALUE='select')
;      txt= widget_label(trow, VALUE= eng_tags(i), FONT=font1)
;    end


;    if(num_param eq 2 and tot_rows gt 2000) then $
;      message= [message,'','List of first 2000 rows:']

    d2=widget_base(b1,/COL)
    ;d1=widget_base(d2,/ROW,/frame)
    d1=widget_base(d2,/COL,/frame)
    ;draw= widget_draw(d1,xsize=800,ysize=600,retain=2, $
    draw= widget_draw(d1,xsize=800,ysize=650,retain=2, $
                    ;/viewport_events,UVALUE='pwin',/frame,x_scroll_size=700,y_scroll_size=600)
                    /viewport_events,UVALUE='pwin',/frame)

    cur_x1= t(0,0)
    x1p= 0
    cur_x2= t(0,elem-1)
    x2p= elem-1

    yrow=widget_base(d1,/ROW)
    tmp =widget_label(yrow,VALUE='                  ')
    tmp = WIDGET_BUTTON(yrow,VALUE='Adjust X-Scale and Re-Plot', UVALUE='adj_xs',/FRAME)
    tmp =widget_label(yrow,VALUE='                    ')
    tmp = WIDGET_BUTTON(yrow,VALUE='Adjust Y-Scale and Re-Plot', UVALUE='adj_ys',/FRAME)

    dg= widget_base(b0,/ROW)
    blk='                                                                                                  '
    diag= widget_label(dg,VALUE=blk+blk+blk,FONT=font)

;    txt= widget_label(yrow,VALUE='   Adjust Y-Scale:    P1:')
;    yp1= WIDGET_TEXT(yrow,UVALUE='adj_yp1',VALUE='????,????',/EDITABLE, $
;                        /ALL_EVENTS,xsize=18)
;    txt= widget_label(yrow,VALUE='  p2:')
;    yp2= WIDGET_TEXT(yrow,UVALUE='adj_yp2',VALUE='????,????',/EDITABLE, $
;                        /ALL_EVENTS,xsize=18)
;    txt= widget_label(yrow,VALUE='  p3:')
;    yp3= WIDGET_TEXT(yrow,UVALUE='adj_yp3',VALUE='????,????',/EDITABLE, $
;                        /ALL_EVENTS,xsize=18)
;    txt= widget_label(yrow,VALUE='  p4:')
;    yp4= WIDGET_TEXT(yrow,UVALUE='adj_yp4',VALUE='????,????',/EDITABLE, $
;                        /ALL_EVENTS,xsize=18)


    widget_control,base,/realize
    struct={base:base,button_id:b_id}
    ;widget_control,base, SET_UVALUE=struct, SET_BUTTON= 1 ;set all buttons on
    widget_control,base, SET_UVALUE=struct
    widget_control,b_id(0),SENSITIVE=0 

    widget_control,diag,set_value='Select Parameters To Be Plotted' 

    ; set date_mod columns selection to off, if any:
    date_mods= where(strpos(strlowcase(tags),'date_mod') ne -1, dcnt)
    if(dcnt gt 0) then begin
      for i=0,dcnt-1 do $
        widget_control,b_id(date_mods(i)),SET_BUTTON=0 
      tag_select(date_mods)= 0 ; set date_mod defaults to off.
    end
    xmanager,'plot_rows',base,EVENT_HANDLER='plot_rows_event'

  endif else begin ; input is not a structure  
    msg='Input is not a valid qdb structure.'
    res=widget_message(msg,title='Bad Input')
  end

return
end
