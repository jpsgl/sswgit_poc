;+
; $Id: scc_data_path.pro,v 1.6 2012/08/23 22:47:50 thompson Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	SCC_DATA_PATH()
;
; Purpose     :	Find path to SECCHI FITS data files.
;
; Category    :	SECCHI, Filename
;
; Explanation : This routine returns the path to the requested part of the
;               SECCHI FITS data tree.  Some sites might have a combination of
;               realtime, playback, and level-0 generated data, while other
;               sites may have only the level-0 data.  This routine resolves
;               the various possible site configurations.
;
;               One can use this routine to return the "top" of the directory
;               tree (defined as the directory containing cal,img,seq,summary),
;               or to descend into various levels of subdirectories.
;
; Syntax      :	Path = SCC_DATA_PATH( Spacecraft  [, keywords ])
;
; Examples    :	Path = SCC_DATA_PATH('Ahead')
;               Path = SCC_DATA_PATH('a',/BEACON)
;               Path = SCC_DATA_PATH('a',SOURCE='rt')
;               PATH = SCC_DATA_PATH('a',TYPE='summary')
;               PATH = SCC_DATA_PATH('a',TYPE='cal',TEL='cor1')
;               PATH = SCC_DATA_PATH('a',TYPE='cal',TEL='cor1',DATE='20061101')
;               PATH = SCC_DATA_PATH('a',type='summary',/totalb)
;
; Inputs      :	SPACECRAFT = Can be one of the following (case-insensitive)
;                            forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the directory path.
;
; Opt. Outputs:	None.
;
; Keywords    :	BEACON  = If set, then return the path to the beacon data.
;
;               SOURCE  = Telemetry source, either "rt" (realtime), "pb"
;                         (playback), or "lz" (level-zero).  If passed, and the
;                         environment variable "secchi" is defined, then
;                         $secchi is used to form the path.
;
;               LEVEL   = Data level.  The default is 0.  Can also be passed as
;                         a (case-sensitive) directory name, e.g. LEVEL="L0".
;
;               TYPE    = Data type, either "cal", "img", "seq" or "summary".
;
;               TELESCOPE = Either "cor1", "cor2", "euvi", "hi_1" ("hi1"), or
;                         "hi_2" ("hi2").  Usually used with TYPE, but defaults
;                         to TYPE="*" if not defined.
;
;               DATE    = The date string of the observation, e.g. "20061130",
;                         but can also be any date type supported by
;                         ANYTIM2CAL.  Usually used with both TYPE and
;                         TELESCOPE, each of which defaults to "*" if
;                         undefined.
;
;               SLASH   = If set, then the directory path is appended with a
;                         slash character.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then a null string is
;                         returned.  In order to use this feature, ERRMSG must
;                         be defined first, e.g.
;
;                               ERRMSG = ''
;                               Path = SCC_DATA_PATH( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               TOTALB  = If set, returns path to the NRL total-B archive for
;                         COR1 and COR2 data
;
; Env. Vars.  : Most sites will have either the $secchi or the $SECCHI_LZ
;               environment variable defined.  Other optional environment
;               variables are available for use, if appropriate.
;
;               secchi              = (Lowercase)  The top directory for the
;                                     overall SECCHI tree, i.e. the directory
;                                     containing the rt, pb, and lz
;                                     directories.  Used primarily at NRL.
;
;               SECCHI_LZ           = The top directory for the main SECCHI
;                                     tree.  (Equivalent to $secchi/lz.)
;                                     Contains the directory L0, which contains
;                                     directories a and b.
;
;               SECCHI_RT           = Equivalent to SECCHI_LZ, but for the
;               SECCHI_PB             realtime (rt) and playback (pb) trees.
;
;               SECCHI_DATA_AHEAD   = The top directories for the SECCHI-A and
;               SECCHI_DATA_BEHIND    B data trees, i.e. the directories
;                                     containing the cal, img, seq, and summary
;                                     directories.  (Equivalent to
;                                     $secchi/lz/L0/a.)  Use these when none of
;                                     the other environment variables are
;                                     appropriate.
;
;               SECCHI_BEACON_AHEAD = Same for beacon data
;               SECCHI_BEACON_BEHIND
;
; Calls       :	CONCAT_DIR, VALID_NUM, ANYTIM2CAL
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 12-Jun-2006, William Thompson, GSFC
;               Version 2, 29-Jun-2006, William Thompson, GSFC
;                       Incorporated SECCHI_LZ/PB/RT.  Added LEVEL.
;               Version 3, 12-Feb-2007, Karl Battams, NRL
;                        Add COR[1,2] totalb keyword (NRL-only option)
;		Version 4, 23-Aug-2012, William Thompson, GSFC
;			Fixed typo when checking type = 'summary'
;
; Contact     :	WTHOMPSON
;-
;
function scc_data_path, spacecraft, beacon=beacon, source=source, $
                        type=type, telescope=telescope, date=date, $
                        level=level, slash=slash, errmsg=errmsg, $
                        totalb=totalb
;
;  Check the input parameters.
;
on_error, 2
if n_params() ne 1 then begin
    message = 'Syntax: SCC_DATA_PATH, SPACECRAFT'
    goto, handle_error
endif
if datatype(spacecraft,1) ne 'String' then begin
    message = 'SPACECRAFT must be a string variable'
    goto, handle_error
endif
;
;  Parse the spacecraft specification
;
sc = strtrim(strlowcase(spacecraft),2)
n = strlen(sc)
if (sc eq 'sta') or (sc eq strmid('ahead',0,n))  or $
  (sc eq strmid('stereo ahead',0,n)) or $
  (sc eq strmid('stereo-ahead',0,n)) then sc = 'a'
if (sc eq 'stb') or (sc eq strmid('behind',0,n)) or $
  (sc eq strmid('stereo behind',0,n)) or $
  (sc eq strmid('stereo-behind',0,n)) then sc = 'b'
if (sc ne 'a') and (sc ne 'b') then begin
    message = 'Unrecognized spacecraft specification ' + spacecraft
    goto, handle_error
endif
;
;  Find the top directory.  The default is to use the environment variables
;  SECCHI_DATA_AHEAD or SECCHI_DATA_BEHIND, but may be modified depending on
;  the keywords passed.
;
case sc of
    'a': path = getenv('SECCHI_DATA_AHEAD')
    'b': path = getenv('SECCHI_DATA_BEHIND')
endcase
;
;  If the above didn't define PATH, and SOURCE hasn't been defined, then
;  try defaulting to SOURCE='lz'.
;
if (path eq '') and (n_elements(source) eq 0) then source='LZ'
;
;  If /BEACON is set, then return one of the beacon directories.
;
if keyword_set(beacon) then begin
    case sc of
        'a': path = getenv('SECCHI_BEACON_AHEAD')
        'b': path = getenv('SECCHI_BEACON_BEHIND')
    endcase
;
;  If SOURCE is passed, then use either SECCHI_LZ/RT/PB or "secchi" (lowercase)
;  to define the path.  If the source is not lz, and the environment variables
;  are undefined, then generate an error.
;
end else if n_elements(source) eq 1 then begin
    lvl = strtrim(strupcase(source), 2)
    test_env = 'SECCHI_' + lvl
    secchi = getenv(test_env)
    if secchi eq '' then begin
        secchi = getenv('secchi')
        if secchi ne '' then secchi = concat_dir(secchi, strlowcase(lvl))
    endif
    if secchi ne '' then begin
;
;  Determine the data level.  The default is L0
;
        if n_elements(level) eq 0 then lvl = 'L0' else begin
            lvl = ntrim(level)
            if valid_num(lvl) then lvl = 'L' + lvl
        endelse
        path = concat_dir(secchi, lvl)
        path = concat_dir(path, sc)
    end else if lvl ne 'LZ' then begin
        message = 'Neither environment variables "secchi" nor "' + test_env + $
          '" defined'
        goto, handle_error
    endif
endif

    
;
;  If totalb is set, do this stuff... (NRL ONLY!!!)
;   
if keyword_set(totalb) THEN BEGIN
    if strlowcase(sc) EQ '' THEN BEGIN
        message='No spacecraft defined.'
        goto, handle_error
    endif
    tbdir=GETENV('SECCHI_P0')
    if tbdir EQ '' THEN BEGIN
        message='$SECCHI_P0 environment variable not defined.'
        goto, handle_error
    endif
    path=concat_dir(tbdir, sc)
    IF strlowcase(type) EQ 'summary' THEN BEGIN
        spath='summary'
        path=concat_dir(path, spath)
    ENDIF ELSE path=concat_dir(path, telescope)
    goto, finish
endif
    

;
;  If PATH still isn't defined, then generate an error.
;
if path eq '' then begin
    message = 'SECCHI environment variables undefined'
    goto, handle_error
endif
;
;  If requested, append the data type.  If the data type is 'summary', then
;  we're done.
;
if n_elements(type) eq 1 then begin
    stype = strtrim(strlowcase(type), 2)
    path = concat_dir(path, stype )
    if stype eq 'summary' then goto, finish
endif
;
;  If requested, append the telescope name.  If TYPE wasn't passed, substitute
;  "*" for all data types.
;
if n_elements(telescope) eq 1 then begin
    if n_elements(type) eq 0 then path = concat_dir(path, '*')
    stelescope = strtrim(strlowcase(telescope), 2)
    if stelescope eq 'hi1' then stelescope = 'hi_1'
    if stelescope eq 'hi2' then stelescope = 'hi_2'
    path = concat_dir(path, stelescope)
endif
;
;  If requested, append the date string.  First, parse the string.
;
if n_elements(date) eq 1 then begin
    if datatype(date,1) eq 'String' then sdate = strtrim(date,2) else $
      sdate = ''
    if not valid_num(sdate) then begin
        message = ''
        sdate = anytim2cal(date, form=8, /date, errmsg=message)
        if message ne '' then goto, handle_error
    endif
;
;  If TYPE or TELESCOPE weren't passed, then substitute '*'.
;
    if n_elements(telescope) eq 0 then begin
        if n_elements(type) eq 0 then path = concat_dir(path, '*')
        path = concat_dir(path, '*')
    endif
;
;  Append the date string.
;
    path = concat_dir(path, sdate)
endif
;
;  Exit point.  If the slash keyword was passed, then append the correct
;  closing character for the given operating system.
;
finish:
if keyword_set(slash) then path = concat_dir(path, '')
return, path
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'scc_data_path: ' + message
return, ''
;
end
