FUNCTION secchifw2str,cameras,fws

  n = N_ELEMENTS(cameras)
  filters= STRARR(n)

  c= WHERE(cameras EQ 3, cnt) ; EUVI
  IF (cnt GT 0) THEN BEGIN
    f= WHERE(fws(c) GE 0 AND fws(c) LE 44, fcnt)
    IF (fcnt GT 0) THEN filters(c(f))= "S1  "
    f= WHERE(fws(c) GE 45 AND fws(c) LE 89, fcnt)
    IF (fcnt GT 0) THEN filters(c(f))= "DBL "
    f= WHERE(fws(c) GE 90 AND fws(c) LE 134, fcnt)
    IF (fcnt GT 0) THEN filters(c(f))= "S2  "
    f= WHERE(fws(c) GE 135 AND fws(c) LE 179, fcnt)
    IF (fcnt GT 0) THEN filters(c(f))= "OPEN"
  ENDIF

  c= WHERE(cameras NE 3, cnt) ; COR1, COR2, HI1, and HI2
  IF (cnt GT 0) THEN filters(c)= "None"

  RETURN,filters

END 
