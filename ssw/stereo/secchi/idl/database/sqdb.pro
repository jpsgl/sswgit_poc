;+
;$Id: sqdb.pro,v 1.15 2016/12/20 21:15:33 nathan Exp $
;
; Project     : SECCHI
;
; Name        : SQDB 
;
; Purpose     : perfroms a SECCHI database query  
;
; Category    : Widget
;
; Explanation : This functionqueries secchi database and returns selected 
;               results as an IDL structure. If no parametrs are provided,
;               it displays names of existing databases, tables, and table
;               fields so that user can make a custom query. Otherwise,
;               provided database and query are used.
;
; Use         : IDL>data= SQDB(database,query) 
;
; Examples    : IDL>data= SQDB()
;
; Inputs      : None
;
; Opt. Inputs : database: name of database to use
;                query:   query to use
;
; Outputs     : data: IDL structure of selected data
;
; Opt. Outputs: None
;
; Keywords    : 
;   	    /QUIET	Do not print informational messages
;   	    /NOFIX  	Do not fix bad paths
;   	    USEBINARY=	Set equal to query app to use instead of default
;
; History     : Ed Esfandiari November 2005
;               First Version.
;               Ed Esfandiari 2006/07/14 Added code to handle linux systems.
;               Ed Esfandiari 2007/10/17 Added LASCO table joins comment.
;               Ed Esfandiari 2007/12/05 Added PRINT_MSSG utilitly.
;               Ed Esfandiari 2008/04/18 Check % values and set them to '' to avoid linux
;                                        type conversion errors.
;
;$Log: sqdb.pro,v $
;Revision 1.15  2016/12/20 21:15:33  nathan
;add USEBINARY keyword; change default binary to mysql_query
;
;Revision 1.14  2012/10/15 16:22:38  nathan
;fix bug if no data diskpath selected
;
;Revision 1.13  2012/08/16 18:03:53  nathan
;workaround for /net/venus in diskpath
;
;Revision 1.12  2012/05/07 19:12:06  nathan
;change how unique temporary filename is derived
;
;Revision 1.11  2011/08/19 14:56:53  nathan
;typo
;
;Revision 1.10  2011/08/18 22:38:34  nathan
;find mysql_query_lambda in $SECCHI_LIB
;
;Revision 1.9  2010/12/09 22:29:03  nathan
;documentation
;
;Revision 1.8  2008/04/18 18:05:26  esfand
;changed % values to blanks to stop linux conversion errors
;
;Revision 1.7  2008/03/07 22:01:44  nathan
;always use /sh with spawn
;
;Revision 1.6  2007/12/05 17:16:17  esfand
;added no widgets hint
;
;Revision 1.5  2007/10/17 18:32:41  esfand
;added LASCO join comment
;
;Revision 1.4  2007/03/28 22:04:54  nathan
;print query
;
;Revision 1.3  2007/01/19 19:35:25  antunes
;Two updates: "quiet=1" mode, and also uses HOME to store its
;temporary files.
;
;Revision 1.2  2007/01/19 16:02:09  antunes
;Added optional 'quiet=1' flag which makes sqdb run without
;status and invocation news.
;
;Revision 1.1  2006/09/22 21:42:35  nathan
;moved from dev/database
;
;Revision 1.3  2006/07/14 19:41:46  esfand
;added code to handle linux
;
;Revision 1.2  2006/01/26 15:57:41  esfand
;minor changes in some routines
;
;
;
;Examples:
;  s= SQDB('secchi','select * from img_seb_hdr')
;  s= SQDB('secchi','SELECT filename,camera,date_obs FROM img_seb_hdr')
;  s= SQDB('secchi','select img_seb_hdr.filename,camera,date_obs,flag,info from img_seb_hdr,img_info WHERE img_seb_hdr.filename = img_info.filename and date_obs > "2006-05-15"')
;
;-
;


PRO PRINT_MSSG,dbase,cmnd
 IF (dbase NE '' and cmnd NE '') THEN BEGIN 
  PRINT,''
  PRINT,'============================================================='
  PRINT,'Hint:'
  PRINT,'To run the same query, a modified version, or a fixed version'
  PRINT,'without the widgets interface, do:'
  PRINT,"data= sqdb(dbase,cmnd)"
  PRINT,'  Where'
  PRINT,"  dbase= '"+dbase+"'"
  PRINT,'  and'
  PRINT,"  cmnd= '"+cmnd+"'"
  PRINT,''
  PRINT,'Note: cmnd can be modified as needed. Remember both dbase and'
  PRINT,'cmnd are strings enclosed in single-quotes.'
  PRINT,'============================================================='
  PRINT,''
 ENDIF
 RETURN
END
   

FUNCTION PARSE_SECCHI_DATA, fn, dbase, noisy=noisy

  ; parse_secchi_data parses a MySql input query file, creates, and returns 
  ; an IDL structure containing all of the data fields and rows in the
  ; query file. It also deletes the file upon completion.
  ;
  ; Note: the data fields in database (such as comment and history fields) can't contain 
  ;       a "|" since this character is inserted by the C interface to MySql to separate
  ;       the fields in a row when creating the query result file used here.

  if (n_elements(noisy) eq 0) then noisy=1; default is print

  ;f= FINDFILE(fn)
  f= FILE_SEARCH(fn)
  IF (f(0) EQ '') THEN BEGIN
    PRINT,''
    PRINT,'Input file "'+fn+'" Does not exist.'
    PRINT,''
    RETURN,'bad input file'
  ENDIF
 
  tmp= ''
  OPENR,unit,fn,/get_lun,/delete
  result= FSTAT(unit)
  IF (result.size EQ 0) THEN BEGIN 
    FREE_LUN,unit
    CLOSE,unit
    PRINT,''
    PRINT,'no data'
    PRINT,''
    RETURN,'no data'
  END 

  READF,unit,tmp ; first line has number of fields comment
  READF,unit,tmp ; 2nd line has field names separated by |
  field_names= STR_SEP(tmp,'|') 
  n_fields= N_ELEMENTS(field_names)
  READF,unit,tmp ; 3rd line has fields types comment
  READF,unit,tmp ; 4th line has field types separated by |
  field_types= FIX(STR_SEP(tmp,'|'))
;help,field_types
;stop
  READF,unit,tmp ; 5th line has number of  selected rows comment
  num_rows= STR_SEP(tmp,' ')
  num_rows= LONG(num_rows(0)) 

  IF (num_rows EQ 0) THEN BEGIN
    FREE_LUN,unit
    CLOSE,unit
    PRINT,''
    PRINT,'no data'
    PRINT,''
    RETURN,'no data'
  END

  ;structure name must be unique:
  str= dbase+"$"+arr2str([strmid(field_names,0,1),strmid(field_names,4,1)],'')

  i=0
  CASE 1 OF
    field_types(i) EQ 1: ft= 0   ; 1 = tinyint 
    field_types(i) EQ 2: ft= 0   ; 2 = smallint
    field_types(i) EQ 3: ft=0L   ; 3 = integer
    field_types(i) EQ 5: BEGIN 
      IF (field_names(i) EQ 'unix_time') THEN ft=0L 
      IF (field_names(i) NE 'unix_time') THEN ft=0.0 
    END
    ELSE: ft= ''   ; 12 = datetime , 253 = varchar
  ENDCASE
  struct= create_struct(field_names(i),ft)

  FOR i= 1, n_fields-1 DO BEGIN
    CASE 1 OF
      field_types(i) EQ 1: ft= 0   ; 1 = tinyint
      field_types(i) EQ 2: ft= 0   ; 2 = smallint
      field_types(i) EQ 3: ft=0L   ; 3 = integer
      field_types(i) EQ 5: BEGIN
        IF (field_names(i) EQ 'unix_time') THEN ft=0L
        IF (field_names(i) NE 'unix_time') THEN ft=0.0
      END
      ELSE: ft= ''   ; 12 = datetime , 253 = varchar
    ENDCASE
    struct= create_struct(struct,field_names(i),ft)
  ENDFOR

 
  struct= create_struct(name= str, struct)  ; add name to struct
 
  qry= REPLICATE(struct,num_rows)
 
  data= STRARR(num_rows,n_fields)

  rows=0L
  WHILE (NOT EOF(unit)) DO BEGIN
    READF,unit,tmp ; read row of data
    params= STRTRIM(STR_SEP(tmp,'|'),2)
    IF (N_ELEMENTS(params) EQ n_fields) THEN BEGIN
      per= WHERE(params EQ '%',per_cnt)
      IF (per_cnt GT 0) THEN params(per)=''
      data(rows,*)= params 
      rows= rows+1
    ENDIF
  ENDWHILE

  FOR i= 0, n_fields-1 DO BEGIN
    str= "qry."+field_names(i)+"= data(*,"+STRTRIM(i,2)+")" 
    ret= EXECUTE(str)
  ENDFOR

  FREE_LUN,unit
  CLOSE,unit

  if (noisy) then begin
      PRINT,''
      PRINT,'Query returned '+STRTRIM(rows,2)+' rows of '+STRTRIM(n_fields,2)+' fields each.'
      PRINT,''
  end

  RETURN,qry

END


FUNCTION SQDB,dbase,cmnd,QUIET=quiet, NOFIX=nofix, USEBINARY=usebinary

common sqdbcmn, binary, out_file

  if (n_elements(quiet) ne 0) then noisy=0 else noisy=1; default is print

    now=systime()
  if (noisy) then print,now


; get unique temporary file name used in current session.

  idl_pid=trim(total(byte(now)))
  out_file= concat_dir(getenv('HOME'),"op"+idl_pid+".dat")

  ; find the right binary to use 
  
  IF keyword_set(USEBINARY) THEN binary=usebinary

  IF datatype(binary) NE 'STR' THEN BEGIN
    opsys= !version.os
    spawn,'arch',arch,/sh
    dm=get_delim()

    binary=GETENV('SECCHI_LIB')+dm+opsys+dm+arch[0]+dm+'mysql_query'
  ENDIF
  print,'Using ',binary
  
  IF(N_PARAMS() LT 2) THEN BEGIN
    cmnd= 'no data'
    ALL_SECCHI_MENUS,dbase,cmnd ; use menus to form a query. 
  END

IF (noisy) THEN BEGIN
    print
    print,cmnd
    print
ENDIF

  IF(STRPOS(cmnd,'no data') NE -1) THEN BEGIN
    PRINT_MSSG,dbase,cmnd
    return,'no data' ;SQL server down?
  ENDIF

  IF(STRLEN(cmnd) LE 7) THEN BEGIN
    PRINT,'Ill formed query command - Done.'
    PRINT_MSSG,dbase,cmnd
    RETURN,'Query command was invalid.'
  END
 
spawn_cmnd=binary+" '"+dbase+"' '"+cmnd+"' > "+out_file

  if (noisy) then begin
      PRINT,'IDL calling the C routines at'
      print,systime()
  end
  SPAWN,spawn_cmnd,/sh
  if (noisy) then begin
      PRINT,'Control returned from C routines to IDL at'
      SPAWN,'date',/sh
      If (datatype(s) eq 'STR' and strupcase(dbase) eq 'LASCO') then $
        PRINT,'*** For LASCO database with joins use qdb (not sqdb) ***'
       PRINT_MSSG,dbase,cmnd
  endif

  ; open the text file created by spawn, parse, capture the data, delete the file, and return
  ; the data in an IDL structure: 

  s= PARSE_SECCHI_DATA(out_file, dbase, noisy=noisy)

IF datatype(s) EQ 'STC' and strpos(cmnd,'diskpath') GT 0 and ~keyword_set(NOFIX) THEN BEGIN
    wv=where(strpos(s.diskpath,'venus') GT 0,nwv)
    IF nwv GT 0 THEN BEGIN
    	message,'Repairing path for '+trim(nwv)+' files.',/info
    	s[wv].diskpath='/net/corona'+strmid(s[wv].diskpath,10)
    ENDIF
ENDIF

  RETURN,s

END


