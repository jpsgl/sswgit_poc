
FUNCTION PARSE_db_DATA, fn, dbase

  ; parse_secchi_data parses a MySql input query file, creates, and returns 
  ; an IDL structure containing all of the data fields and rows in the
  ; query file. It also deletes the file upon completion.
  ;
  ; Note: the data fields in database (such as comment and history fields) can't contain 
  ;       a "|" since this character is inserted by the C interface to MySql to separate
  ;       the fields in a row when creating the query result file used here.

  ;f= FINDFILE(fn)
  f= FILE_SEARCH(fn)
  IF (f(0) EQ '') THEN BEGIN
    PRINT,''
    PRINT,'Input file "'+fn+'" Does not exist.'
    PRINT,''
    RETURN,'bad input file'
  ENDIF
 
  tmp= ''
  OPENR,unit,fn,/get_lun,/delete
  result= FSTAT(unit)
  IF (result.size EQ 0) THEN BEGIN 
    FREE_LUN,unit
    CLOSE,unit
    RETURN,'no data'
  END 

  READF,unit,tmp ; first line has number of fields comment
  READF,unit,tmp ; 2nd line has field names separated by |
  field_names= STR_SEP(tmp,'|') 
  n_fields= N_ELEMENTS(field_names)
  READF,unit,tmp ; 3rd line has fields types comment
  READF,unit,tmp ; 4th line has field types separated by |
  field_types= FIX(STR_SEP(tmp,'|'))
;help,field_types
;stop
  READF,unit,tmp ; 5th line has number of  selected rows comment
  num_rows= STR_SEP(tmp,' ')
  num_rows= LONG(num_rows(0)) 

  IF (num_rows EQ 0) THEN BEGIN
    FREE_LUN,unit
    CLOSE,unit
    RETURN,'no data'
  END

  ;structure name must be unique:
  str= dbase+"$"+arr2str([strmid(field_names,0,1),strmid(field_names,4,1)],'')

  i=0
  CASE 1 OF
    field_types(i) EQ 1: ft= 0   ; 1 = tinyint 
    field_types(i) EQ 2: ft= 0   ; 2 = smallint
    field_types(i) EQ 3: ft=0L   ; 3 = integer
    field_types(i) EQ 5: BEGIN 
      IF (field_names(i) EQ 'unix_time') THEN ft=0L 
      IF (field_names(i) NE 'unix_time') THEN ft=0.0 
    END
    ELSE: ft= ''   ; 12 = datetime , 253 = varchar
  ENDCASE
  struct= create_struct(field_names(i),ft)

  FOR i= 1, n_fields-1 DO BEGIN
    CASE 1 OF
      field_types(i) EQ 1: ft= 0   ; 1 = tinyint
      field_types(i) EQ 2: ft= 0   ; 2 = smallint
      field_types(i) EQ 3: ft=0L   ; 3 = integer
      field_types(i) EQ 5: BEGIN
        IF (field_names(i) EQ 'unix_time') THEN ft=0L
        IF (field_names(i) NE 'unix_time') THEN ft=0.0
      END
      ELSE: ft= ''   ; 12 = datetime , 253 = varchar
    ENDCASE
    struct= create_struct(struct,field_names(i),ft)
  ENDFOR

 
  struct= create_struct(name= str, struct)  ; add name to struct
 
  qry= REPLICATE(struct,num_rows)
 
  data= STRARR(num_rows,n_fields)

  rows=0L
  WHILE (NOT EOF(unit)) DO BEGIN
    READF,unit,tmp ; read row of data
    params= STRTRIM(STR_SEP(tmp,'|'),2)
    IF (N_ELEMENTS(params) EQ n_fields) THEN BEGIN
      data(rows,*)= params 
      rows= rows+1
    ENDIF
  ENDWHILE

  FOR i= 0, n_fields-1 DO BEGIN
    str= "qry."+field_names(i)+"= data(*,"+STRTRIM(i,2)+")" 
    ret= EXECUTE(str)
    print,str,ret
  ENDFOR

  FREE_LUN,unit
  CLOSE,unit

  PRINT,''
  PRINT,'Query returned '+STRTRIM(rows,2)+' rows of '+STRTRIM(n_fields,2)+' fields each.'
  PRINT,''

  RETURN,qry

END



