;+
; Project     :	STEREO - SECCHI
;
; Name        :	SCCINGEST_BKG
;
; Purpose     :	Ingest and catalog SECCHI FITS files
;
; Category    :	SECCHI, Catalog
;
; Explanation :	This routine takes a collection of SECCHI background FITS files
;               and arranges them into the proper tree structure.
;
; Syntax      :	SCCINGEST_BKG, [ DIRECTORY ]  [, FILES=FILES ]
;
; Examples    :	SCCINGEST_BKG, '$HOME'
;               SCCINGEST_BKG, FILES=FILE_SEARCH('mc1A_p*.fts')
;
; Inputs      :	None required.
;
; Opt. Inputs :	DIRECTORY = String array containing one or more directories to
;                           search for FITS files.
;
; Outputs     :	Generates summary files.
;
; Opt. Outputs:	None.
;
; Keywords    :	FILES   = String array containing a list of files to process.
;                         If passed, then overrides the DIRECTORY parameter.
;
;               NOSUB   = If set, don't descend into subdirectories.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then a null string is
;                         returned.  In order to use this feature, ERRMSG must
;                         be defined first, e.g.
;
;                               ERRMSG = ''
;                               SCCINGEST_BKG, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
; Env. Vars.  : SECCHI_BKG points to the top of the output directory tree.
;
; Calls       :	
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	Modified from sccingest.pro
;
; History     :	Version 1, 27-Sep-2007, William Thompson, GSFC
;               Version 2, 02-Oct-2007, WTT, Added /NOSUB
;
; Contact     :	WTHOMPSON
;-
;
pro sccingest_bkg, directory, files=k_files, nosub=nosub, errmsg=errmsg
;
on_error, 2
on_ioerror, ioerror
;
;  Get  the list of files to process.
;
if n_elements(k_files) eq 0 then begin
    if n_elements(directory) eq 0 then begin
        message = 'Either DIRECTORY or the FILES keyword must be passed'
        goto, handle_error
    endif
    if keyword_set(nosub) then $
      files = file_search(concat_dir(directory, '*.fts')) else $
      files = file_search(directory, '*.fts')
end else files = k_files
;
files = strtrim(files,2)
w = where(files ne '', count)
if count eq 0 then begin
    message = 'No files found'
    goto, handle_error
endif
files = files[w]
;
;  Break down the filenames into their component parts.
;
break_file, files, disks, dirs, names
type = strmid(names,0,1)
sc = strlowcase(strmid(names,3,1))
date = '20' + strmid(names,10,4)
;
;  Step through and process each file.
;
for ifile=0,n_elements(files)-1 do begin
    datapath = ''
    if (sc[ifile] eq 'a') or (sc[ifile] eq 'b') then begin
        datapath = concat_dir(getenv('SECCHI_BKG'), sc[ifile])
        case type[ifile] of
            'm': datapath = concat_dir(datapath, 'monthly_min')
            'd': datapath = concat_dir(datapath, 'daily_med')
            else: datapath = ''
        endcase
        if datapath ne '' then begin
            if valid_num(date[ifile]) then $
              datapath = concat_dir(datapath, date[ifile]) else $
              datapath = ''
        endif
    endif
;
;  Make sure the directory exists, and move the file into the directory.
;
    if datapath ne '' then begin
        if not file_exist(datapath) then file_mkdir, datapath
        file_move, files[ifile], datapath, /allow_same, /overwrite
    end else print, 'Unable to process files[ifile]
endfor
;
;  Exit point.
;
return
;
;  Error handling point.
;
ioerror: message = !error_state.msg
;
handle_error:
if n_elements(unit) gt 0 then free_lun, unit
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'sccingest_bkg: ' + message
;
end
