function get_scc_hk,sc,tbl,mnemonics,d1,d2,res=res, WRITE_DIR=write_DIR

; $Id: get_scc_hk.pro,v 1.3 2009/07/28 19:32:45 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : get_scc_hk
;               
; Purpose     : get converted SECCHI hk mnemonics from MySQL database
;               
; Explanation : 
;               
; Use         : IDL> s=get_scc_hk('b','temps','HBHI1CCDT,HBHI2CCDT','20081001','20081031',write='.')
;    
; Inputs      :   sc = A or B 
;   	    	  tbl = name of database table from [ current, doors, events, fm, ic, ih, pointing, 
;   	    	    	    	    	     temps, tm, voltage, xf ]
;   	    	  Mnemonics = string of mnemonics 'HBCOR2ZONE1T,HBCOR1ZONE1T,HBEUVIZONE1T'
;   	    	    	(MUST match tbl)
;   	     	  d1 = yyyymmdd -or- standard format date/time
;   	    	      	first day or full day requested
;    	    	    	if only d1 is set then the full day is returned
;   	    	  d2 = ending date/time optional
;               
; Outputs     :   structure of packettimes, unix_time, and mnemonic values
;
; Keywords :	res = resolution in minutes (60,30,10,1,0.1) 0.1 for full, defaults to 1 min
;               WRITE_DIR  = directory in which to write a comma-delimited flatfile containing the output
;
; Calls from LASCO : none
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Database 
;               
; Prev. Hist. : get_hbtemps.pro,v 1.5  2007/01/23 19:49:06  mcnutt
;
; Written     : Lynn McNutt, NRL/I2, Aug 2006
;               
; Modified    :
;
; $Log: get_scc_hk.pro,v $
; Revision 1.3  2009/07/28 19:32:45  nathan
; change format of time input
;
; Revision 1.2  2008/12/05 21:17:01  nathan
; update header doc
;
; Revision 1.1  2008/12/05 21:13:17  nathan
; check-in
;
;

tbls=[ 'current', 'doors', 'events', 'fm', 'ic', 'ih', 'pointing', 'temps', 'tm', 'voltage', 'xf' ]
ind=where(strlowcase(tbl) EQ tbls,nind)
IF nind EQ 0 THEN BEGIN
    print,'Second argument must be one of : ',tbls
    return,-1
ENDIF
hk_temps='hk_'+tbl+'_'+strlowcase(sc) 
secchi_hk='secchi_flight_hk_'+strupcase(sc)
resol=' group by mid('+hk_temps+'.pckt_time,1,16)'
if(keyword_set(res)) then begin
 if (Res eq 60) then resol=' group by mid('+hk_temps+'.pckt_time,1,13)'
 if (Res eq 30) then resol=' and (CAST('+hk_temps+'.pckt_time as CHAR) like "%:00:0%" OR CAST('+hk_temps+'.pckt_time as CHAR) like "%:30:0%") group by mid('+hk_temps+'.pckt_time,1,16)'
 if (Res eq 10) then resol=' and CAST('+hk_temps+'.pckt_time as CHAR) like "%:%0:0%" group by mid('+hk_temps+'.pckt_time,1,16)'
 if (Res eq 0.1) then resol=''
endif

;--Parse input time
IF strpos(d1,'/') LT 0 and strpos(d1,'-') LT 0 THEN $
    time1=utc2str(yymmdd2utc(d1)) ELSE $
    time1=d1
IF datatype(d2) NE 'UND' THEN BEGIN
    IF strpos(d2,'/') LT 0 and strpos(d2,'-') LT 0 THEN $
    	time2=utc2str(yymmdd2utc(d2)) ELSE $
    	time2=d2
ENDIF ELSE BEGIN
    ut1=str2utc(time1)
    ut1.mjd=ut1.mjd+1
    ut1.time=0
    time2=utc2str(ut1)
ENDELSE
    
cmnd='select pckt_time,unix_time,'+mnemonics+' from '+hk_temps+' where pckt_time between "'+time1+'" and "'+time2+'"'+resol
stemps=SQDB (secchi_hk,cmnd)
z=size(stemps)
np=z[1]
if (z[0] ne 0) then begin
    tags=tag_names(stemps) & ntags=n_Elements(tags)
    hbtemps=create_struct(tags(0),stemps.(0),tags(1),stemps.(1))
    for i=2,ntags-1 do begin
    	cnv_str= GET_CONVERT_STRING(tags(i),GETENV('SSW_SECCHI')+'/idl/database/hk1067_mnemonics_info.txt')
    	SECCHI_DN_CONVERT, stemps.(i), REPLICATE(cnv_str,np), dn_cnvt, cnv_units
    	hbtemps=create_struct(hbtemps,tags(i),dn_cnvt)
    endfor


    IF keyword_set(WRITE_DIR) THEN BEGIN
	outfn='scc_'+hk_temps+'_'+d1+'.csv'
	outfile=concat_dir(write_dir,outfn)
	ok = CHECK_PERMISSION(outfile)
	IF not ok THEN outfile = getenv_slash('HOME')+outfn
	openw,wlu,outfile,/get_lun
	line='PCKT_TIME'
	FOR i=2,ntags-1 DO line=line+', '+tags[i]
	printf,wlu,line
	FOR j=0L,np-1 DO BEGIN
	    line=hbtemps.pckt_time[j]
	    FOR i=2,ntags-1 DO line=line+', '+string((hbtemps.(i))[j])
	    printf,wlu,line
	ENDFOR
	close,wlu
	free_lun,wlu
    ENDIF
	    
endif else hbtemps=stemps


return,hbtemps

end
