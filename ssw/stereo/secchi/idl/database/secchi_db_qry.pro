;+
;$Id: secchi_db_qry.pro,v 1.5 2016/12/28 16:01:35 nathan Exp $
;
; Project     : SECCHI
;
; Name     : SECCHI_DB_QRY
;
; Purpose     : idl based query
;
; Category    : database 
;
; Explanation :
; This function interfaces with a C routine that uses sybase or mysql for retrieving
; names of data bases, table names within a database, or column names within
; a table. Input to this pro is either no parameter, one parameter (database 
; name), or two parameters (database name and table name).
; Output is an array of strings containing databases, tables, or fields names.
; This function is the same as db_help procedure; As it is obvious, it returns
; the output instead of just printing it to the screen.
;
; Use         : data= SECCHI_DB_QRY(dbase,cmnd)
;
; Inputs      : dbase : database name
;             : cmnd  : sql command
; Outputs     : data: IDL structure of selected data
;
; Opt. Outputs: None
;
; Keywords    : None
;
; History     : Ed Esfandiari  Nov 2005 - First version
;               Ed Esfandiari, 2006/07/14 Added code to handle linux systems. 
;
;$Log: secchi_db_qry.pro,v $
;Revision 1.5  2016/12/28 16:01:35  nathan
;fix spawn cmd
;
;Revision 1.4  2016/12/20 21:14:09  nathan
;print command
;
;Revision 1.3  2011/08/19 14:56:53  nathan
;typo
;
;Revision 1.2  2011/08/18 22:38:34  nathan
;find mysql_query_lambda in $SECCHI_LIB
;
;Revision 1.1  2007/01/12 22:41:51  nathan
;moved from nrl_lib/dev/database
;
;Revision 1.2  2006/07/14 19:42:05  esfand
;added code to handle linux
;
;
;-
;

function SECCHI_DB_QRY,dbase,cmnd 

common sqdbcmn, binary, out_file

 arg= n_params() ; get number of input parameters
 if(arg eq 0) then begin 
   dbase='mysql' 
   cmnd='show databases' 
 end
 if(arg eq 1) then begin
   cmnd='show tables in '+dbase
 end
 if(arg eq 2) then begin
   cmnd='select * from '+cmnd+' where 1=2'
 end


  valid= 0

;generate the spawn command and run it. 
; binary and out_file are from common sqdbcmn

spawn_cmnd=binary+" '"+dbase+"' '"+cmnd+"' > "+out_file
print,spawn_cmnd
 spawn,spawn_cmnd,/sh

;open the text file created by calling the above C routine and format and
;output the table or column names:

 q= PARSE_DB_INFO(out_file)

 return,q

 end



