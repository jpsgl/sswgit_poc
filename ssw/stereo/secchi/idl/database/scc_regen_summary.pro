;+
; Project     :	STEREO - SECCHI
;
; Name        :	SCC_REGEN_SUMMARY
;
; Purpose     :	Regenerate SECCHI summary files
;
; Category    :	
;
; Explanation :	This procedure is used to regenerate SECCHI summary files, such
;               as those created by sccingest, to put them in the correct
;               format.  The summary file format changed in April 2007, and
;               files created using the old format are no longer supported.
;               This routine creates new summary files out of the FITS headers.
;
;               The summary files distributed by the SECCHI project will
;               already be in the new format.  This routine only applies to
;               locally generated summary files, such as those generated by
;               sccingest.pro.
;
;               The old summary files had names like "sccA200704.seq".  The new
;               summary files are broken up by telescope, and have names like
;               "sccA200704.seq.c1".  Once all the new summary files are
;               created, the old summary files can be deleted.
;
;               The new summary files are first written to files with "_new"
;               appended to the filename.  The "_new" is removed from the
;               filename in the final stage of the process.  In some cases, it
;               may be necessary to do the renaming by hand, particularly if
;               this routine is run multiple times.
;
;               This routine reads the header of every SECCHI FITS file in
;               one's local archive.  This can be very time consuming, and
;               should only be done if necessary.
;
; Syntax      :	SCC_REGEN_SUMMARY
;
; Examples    :	SCC_REGEN_SUMMARY, '/home/thompson/secchidata/L0'
;
; Inputs      :	DIRECTORY = The name of the top of the SECCHI data tree to
;                           process.  This directory should contain the "cal",
;                           "img", "seq", and "summary" directories.
;
;                           One can also give the name of the directory
;                           containing the "a" and "b" (or "L0/a" and "L0/b")
;                           trees, in which case the routine is called
;                           recursively to process both trees.
;
; Opt. Inputs :	None.
;
; Outputs     :	The summary files are written in the "summary" directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	FILE_EXIST, CONCAT_DIR, DELVARX, FXHREAD, SCC_FITSHDR2STRUCT,
;               SCCWRITESUMMARY
;
; Common      :	None.
;
; Restrictions:	If the "_old" version of the summary file already exists, the
;               program will not be able to move the current version to
;               "_old".  This could then interfere with making the "_new"
;               version the current version.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 13-April-2007, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro scc_regen_summary, directory
on_error, 2
;
;  Check the input parameter.
;
if n_params() ne 1 then message, 'Syntax: SCC_REGEN_SUMMARY, DIRECTORY'
if (n_elements(directory) ne 1) or (datatype(directory) ne 'STR') then $
  message, 'DIRECTORY must be a scalar string'
;
;  Make sure that the path exists.
;
path = strtrim(directory,2)
if not file_exist(path) then message, 'Directory "' + path + $
  '" does not exist'
;
;  Check to see if the directory contains an L0 directory.  If so, then descend
;  into that directory.
;
newpath = concat_dir(path,'L0')
if file_exist(newpath) then begin
    scc_regen_summary, newpath
    return
endif
;
;  Test to see if the directory contains directories called "a" and/or "b".  If
;  so, then descend into those directories.
;
apath = concat_dir(path,'a')  &  atest = file_exist(apath)
bpath = concat_dir(path,'b')  &  btest = file_exist(bpath)
if atest then scc_regen_summary, apath
if btest then scc_regen_summary, bpath
if atest or btest then return
;
;  If we got this far, then make sure that at least one of the directories
;  "cal", "img", or "seq" exists.
;
type = ['cal', 'img', 'seq']
tpaths = concat_dir(path, type)
w = where(is_dir(tpaths), ntypes)
if ntypes eq 0 then message, 'Directories cal,img,seq not found'
type = type[w]
tpaths = tpaths[w]
;
;  If the "summary" directory doesn't exist, then create it.
;
sumpath = concat_dir(path, 'summary')
if not file_exist(sumpath) then file_mkdir, sumpath
;
;  Keep track of the names of the summary files created.
;
delvarx, sumfiles
;
;  Step into each subdirectory in turn, and look for the instrument
;  subdirectories.
;
for itype = 0,ntypes-1 do begin
    ins = ['cor1','cor2','euvi','hi_1','hi_2']
    ipaths = concat_dir(tpaths[itype], ins)
    w = where(is_dir(ipaths), nins)
    if nins gt 0 then begin
        ins = ins[w]
        ipaths = ipaths[w]
;
;  Step through the instrument subdirectories and look for the date
;  subdirectories.
;
        for iins=0,nins-1 do begin
            dpaths = file_search(concat_dir(ipaths[iins],'*'), count=ndates)
            if ndates gt 0 then begin
                s = sort(dpaths)
                dpaths = dpaths[s]
                for idate=0,ndates-1 do begin
                    print,dpaths[idate]
;
;  Get the list of files to process.
;
                    files = file_search(dpaths[idate], '*.fts', count=nfiles)
                    if nfiles gt 0 then begin
                        s = sort(files)
                        files = files[s]
                    endif
;
;  Step through and process each file.
;
                    for ifile=0,nfiles-1 do begin
                        openr, unit, files[ifile], /get_lun
                        fxhread, unit, header, status
                        free_lun, unit
                        if status ne 0 then begin
                            print, 'Error reading file ' + files[ifile] + $
                              ' -- skipping'
                        end else begin
;
;  From the header, determine the telescope, the date, the observatory, and
;  other information needed to determine the file type.
;
                            header = scc_fitshdr2struct(header)
                            observatory= strtrim(strupcase(header.obsrvtry), 2)
                            observatory = strmid(observatory, 7, 1)
                            detector   = strtrim(strupcase(header.detector), 2)
                            filename   = strtrim(header.filename, 2)
;
;  Form the name of the summary file.  Make sure the summary directory exists.
;
                            sumfile = 'scc' + observatory + $
                              strmid(filename,0,6) + '.' + type[itype]
                            sumfile = concat_dir(sumpath, sumfile)
                            case detector of
                                'COR1': sumfile = sumfile + '.c1'
                                'COR2': sumfile = sumfile + '.c2'
                                'EUVI': sumfile = sumfile + '.eu'
                                'HI1':  sumfile = sumfile + '.h1'
                                'HI2':  sumfile = sumfile + '.h2'
                                else:
                            endcase
;
;  Save the name of the summary file.
;
                            if n_elements(sumfiles) eq 0 then $
                              sumfiles = sumfile else begin
                                w = where(sumfiles eq sumfile, count)
                                if count eq 0 then sumfiles = $
                                  [sumfiles, sumfile]
                            endelse
;
;  Append "_new" to the summary file, so that older files are undisturbed.
;
                            sumfile = sumfile + '_new'
;
;  Write the header information to the summary file.
;
                            sccwritesummary, header, sumfile, /nolock
;
                        endelse ;Able to read FITS header
                    endfor      ;ifile
                endfor          ;idate
            endif               ;ndates gt 0
        endfor                  ;iins
    endif                       ;nins gt 0
endfor                          ;itype
;
;  Step through all the summary files.  If there's an older version of the
;  file, then append "_old" to the name.  Remove the "_new" from the new files.
;
if n_elements(sumfiles) eq 0 then message, 'No summary files were created'
for isum = 0,n_elements(sumfiles)-1 do begin
    if file_exist(sumfiles[isum]) then begin
        command = "file_move, sumfiles[isum], sumfiles[isum] + '_old'"
        if not execute(command) then message, /continue, $
          'Unable to move ' + sumfiles[isum] + ' to ' + sumfiles[isum] + '_old'
    endif
    command = "file_move, sumfiles[isum] + '_new', sumfiles[isum]"
    if not execute(command) then message, /continue, $
      'Unable to move ' + sunfiles[isum] + '_new to ' + sumfiles[isum]
endfor
;
end
