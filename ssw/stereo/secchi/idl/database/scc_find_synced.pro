function scc_find_synced, fn

;+
; $Id: scc_find_synced.pro,v 1.4 2010/10/12 18:00:01 nathan Exp $
;
; Project   : STEREO SECCHI
;
; Name      : scc_find_synced
;
; Purpose   : This function returns path/name of image with same commanded date-obs
;   	    	as input path/name.
;
; Explanation: uses file_exist to determine if there is a match
;
; Use       : IDL> result=scc_find_synced('$secchi/lz/L0/a/img/euvi/20080102/20080102_092330_n4euA.fts')
;
; Inputs    : filename, A or B
;
; Outputs   :  if there is a match, returns filename; else returns ''
;
; Keywords  :
;
; Common    :
;
; Calls     : 
;
; Category    : database stereo pair display
;
; Prev. Hist. : None.
;
; Written     : Nathan Rich, Interferometrics/NRL, 2/20/08
;
; $Log: scc_find_synced.pro,v $
; Revision 1.4  2010/10/12 18:00:01  nathan
; use strsplit not strlen to accomodate .gz file input
;
; Revision 1.3  2010/07/01 17:53:44  nathan
; use get_delim()
;
; Revision 1.2  2008/04/02 20:26:21  nathan
; use sccfindfits if fn has no path
;
; Revision 1.1  2008/02/20 20:12:13  nathan
; planning to use this with scc_mkframe.pro
;

inx=strsplit(fn,'.f')
aorb=strmid(fn,inx[1]-3,1)
IF aorb EQ 'A' THEN ab2='B' ELSE ab2='A'
dlm=get_delim()
ab2d=dlm+strlowcase(ab2) +dlm
ab1d=dlm+strlowcase(aorb)+dlm
IF inx[1] LT 26 THEN BEGIN
    fn=sccfindfits(fn)
    inx=strsplit(fn,'.f')
ENDIF
fn2=fn
strput,fn2,ab2d,strpos(fn,ab1d)
strput,fn2,ab2,inx[1]-3

IF  file_exist(fn2) THEN return,fn2 $
ELSE BEGIN
    PRINT,'Could not find match ',fn2
    return,''
ENDELSE

END
