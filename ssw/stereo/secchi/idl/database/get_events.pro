;+
; $Id: get_events.pro,v 1.2 2012/01/30 19:00:19 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_events
;               
; Purpose   : read Solar Events from database
;               
; Explanation: 
;               
; Use       : IDL> events=get_events(yr) or events=get_events(outfile='',/update)
;
; Examples:   IDL> get_events
;    
; Inputs    :
;            
; Outputs   : Events string
;
; Keywords  : outfile = outfile
;   	      update = update output file to current date requires output file to be set 
;
; Calls from LASCO :
;
; Common    : 
;               
; Restrictions: requires database
;               
; Side effects: 
;               
; Category    : 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Jan 2012
;               
; $Log: get_events.pro,v $
; Revision 1.2  2012/01/30 19:00:19  mcnutt
; creates/updates or returns a list of cor2 solar event triggers
;
; Revision 1.1  2012/01/27 20:08:30  mcnutt
; creates/updates or returns a list of cor2 solar event triggers
;

function get_events,yrs,outfile=outfile,update=update

events=''
if keyword_set(update) then begin
  events=readlist(outfile)
  starttime=utc2str(tai2utc(utc2tai(anytim2utc(strmid(events(n_elements(events)-1),22,19)))+30*60))
  endtime=utc2str(tai2utc(utc2tai(anytim2utc(''))))
  yrs=[strmid(endtime,0,4)]
endif

for ny=0,n_elements(yrs)-1 do begin

  if ~keyword_set(update) then begin
    starttime=string(yrs(ny),'(i4)')+'-01-01'
    endtime=string(yrs(ny),'(i4)')+'-12-31'
  endif

  eva=gethkevents('a',starttime,endtime,code=850)
  evb=gethkevents('b',starttime,endtime,code=850)

  sc=''
  dates=''
  times=0

  if datatype(eva) ne 'STR' then begin
    sca=strarr(n_elements(eva)) +'A'
    sc=[sc,sca] 
    dates=[dates,eva.pckt_time]
    times=[times,eva.unix_time]
  endif

  if datatype(evb) ne 'STR' then begin
    ; remove event triggers from mdump Door commanding error on 03-10-2011
    ;if yr eq 2011 then begin
    ;  z=where(strpos(evb.pckt_time,'2011-03-10') eq -1)
    ;  evb=evb(z)
    ;  z=where(strpos(evb.pckt_time,'2011-03-11') eq -1)
    ;  evb=evb(z)
    ;endif
    scb=strarr(n_elements(evb)) +'B'
    sc=[sc,scb] 
    dates=[dates,evb.pckt_time]
    times=[times,evb.unix_time]
  endif

  if datatype(evb) ne 'STR' or datatype(eva) ne 'STR' then begin
    sc=sc(1:n_elements(sc)-1)
    dates=dates(1:n_elements(dates)-1)
    times=times(1:n_elements(times)-1)

    z=sort(dates)
    times=times(z)
    sc=sc(z)
    dates=dates(z)
    event=(times-shift(times,1))/60.
    tmp=where(event gt 120,cnt)
    if cnt gt 0 then tmp=[0,tmp] else tmp=[0,n_elements(event)-1]
    print,'First Trigger       | Last Trigger        | A | B |
    print,'---------------------------------------------------'
    if ny eq 0 and keyword_set(update) then openw,evout,outfile,/append,/get_lun
    if ny eq 0 and keyword_set(outfile)  and ~keyword_set(update) then begin
      openw,evout,outfile,/get_lun
      printf,evout,'First Trigger       | Last Trigger        | A | B |
      printf,evout,'---------------------------------------------------'
    endif
;      2010-01-05 23:59:48 | 2010-01-05 23:59:48 | 0 | 1
    for n=1,n_elements(tmp)-1 do begin
       nas=where(sc(tmp(n-1):tmp(n)-1) eq 'A',cnta)
       nbs=where(sc(tmp(n-1):tmp(n)-1) eq 'B',cntb)
       print,dates(tmp(n-1)),' | ',dates(tmp(n)-1),' | ',string(cnta,'(i2)'),'| ',string(cntb,'(i2)'),'|'
       event=dates(tmp(n-1))+' | '+dates(tmp(n)-1)+' | '+string(cnta,'(i2)')+'| '+string(cntb,'(i2)')+'|'
       events=[events,event]
       if keyword_set(outfile) then $
         printf,evout,dates(tmp(n-1)),' | ',dates(tmp(n)-1),' | ',string(cnta,'(i2)'),'| ',string(cntb,'(i2)'),'|'
    endfor
    if ny eq n_elements(yrs)-1 and keyword_set(outfile) then begin
      close,evout
      free_lun,evout
    endif
  endif
endfor
if events(0) eq '' and n_elements(events) gt 1 then events=events(1:n_elements(events)-1)
return,events
end

