;pro sccplotictemps, stadt, enddt, RES=res, SA=sa, SB=sb
;+
; $Id: sccplotictemps.pro,v 1.3 2013/04/29 16:15:53 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : 
;               
; Purpose   : plot all IC temperatures for both S/C mnemonics from database
;               
; Use       : IDL> sccplotictemps,'2010/11/01','2011/11/01'
;    
; Inputs    : stadt 	First date of interval (anytim2utc)
;   	      enddt 	Last date of interval
;               
; Outputs   :         
;
; Keywords  : 
;   	    RES=    Interval between points in minutes; default/max is 60
;   	    SA=, SB=	Structure returned from get_ictemps.pro;
;   	    	values returned, or if set  then uses values as input
;               
; Calls     : get_ictemps.pro
;               
; Category    : plot telemetry hk safety
;               
; Prev. Hist. : None.
;
; Written     :N.Rich
;               
; $Log: sccplotictemps.pro,v $
; Revision 1.3  2013/04/29 16:15:53  nathan
; implement /uselast
;
; Revision 1.2  2012/04/10 17:31:57  nathan
; it works now
;
; Revision 1.1  2011/03/25 21:45:51  nathan
; plot 2x2 graphs
;
; Revision 1.1  2011/03/24 20:19:26  nathan
; plot all hb temps
;

pro plot_ict, t, y, YRANGE=yrange

COMMON plothk, s, indx, w, ptitle

nr=n_elements(t)

IF keyword_set(YRANGE) THEN BEGIN
    pmn=yrange[0]
    pmx=yrange[1]
ENDIF ELSE BEGIN
    pmn=min(y,max=pmx)
ENDELSE
;window,w
utplot,t,y,title=ptitle,ytitle='Deg C',yrange=[pmn,pmx],psym=1


end

pro sccplotictemps, stadt, enddt, RES=res,  USELAST=uselast

COMMON plotic, sa, sb

IF n_params() GT 0 THEN BEGIN

    IF n_params() LT 2 THEN enddt=stadt

    IF ~keyword_set(USELAST) THEN BEGIN
    	IF keyword_set(RES) THEN intrv=res<60 ELSE intrv=60

    	sa=get_ictemps('a',stadt,enddt,RES=intrv)
    	sb=get_ictemps('b',stadt,enddt,RES=intrv)
    ENDIF
    
ENDIF
help,/str,sa,sb

plotprep

!p.multi=[0,2,2]    ; 2 rows, 2 column
window,1,xsi=1000,ysi=800

ptitle='SECCHI-A ICSCIPHKTEMP'
plot_ict,sa.pckt_time,sa.icsciphktemp,yrang=[35,65]

ptitle='SECCHI-B ICSCIPHKTEMP'
plot_ict,sb.pckt_time,sb.icsciphktemp,yrang=[35,65]

ptitle='SECCHI-A ICHIHKTEMP'
plot_ict,sa.pckt_time,sa.ichihktemp,yrang=[-15,0]

ptitle='SECCHI-B ICHIHKTEMP'
plot_ict,sb.pckt_time,sb.ichihktemp,yrang=[-15,0]

!p.multi=0

stop
end
