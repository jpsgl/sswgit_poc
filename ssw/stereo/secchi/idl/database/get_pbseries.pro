function get_pbseries, fnamen, SILENT=silent
;+
; $Id: get_pbseries.pro,v 1.3 2010/10/15 15:34:07 nathan Exp $
; Project     :	STEREO - SECCHI
;
; Name        :	GET_PBSERIES
;
; Purpose     :	Return PB series of 3 images given one seq FITS filename 
;
; Category    :	STEREO, SECCHI, COR1, COR2, Analysis, database
;
; Explanation :	This routine differs from COR1_PBSERIES in that it takes as input a filename
;   	    	rather than a time.
;
; Syntax      :	GET_PBSERIES, filename
;
; Inputs      :	filename    STRING name of seq FITS file at 0,120 or 240 polarizer position
;
; Opt. Inputs :	None.
;
; Outputs     :	STRARR[3]  3 filenames at 0,120,240 deg 
;   	    	-or- 
;   	    	STRING=''  if 3 are not found
;
; Keywords    :	/SILENT Suppress info messages.
;
; Calls       :	COR1_PBSERIES
;
; Common      :	None.
;
; Restrictions:	
;
; Side effects:	None.
;
; Prev. Hist. :	taken from scc_mkframe.pro,v 1.75
;
; $Log: get_pbseries.pro,v $
; Revision 1.3  2010/10/15 15:34:07  nathan
; check for .gz files
;
; Revision 1.2  2010/08/25 19:55:55  nathan
; search ssr1 and ssr2
;
; Revision 1.1  2010/04/15 19:41:30  nathan
; checkin
;
;-
;
SSRval=3   ;Synoptic + Special event
IF strpos(fnamen,'7c') GT 0 THEN BEGIN
    IF ~keyword_set(SILENT) THEN print,'Beacon image; excluding normal images from query'
    ssrval=7
ENDIF
isc2=(strpos(fnamen,'c2')>0)
st1=strmid(fnamen,strpos(fnamen,'.f')-21,15)
sct=strmid(fnamen,strpos(fnamen,'.f')-1,1)
t1=(strmid(st1,9,2)*60l*60l)+(strmid(st1,11,2)*60l)-60 
t2=(strmid(st1,9,2)*60l*60l)+(strmid(st1,11,2)*60l)+60+60      ; sec's ignored...so bump up to next minute.       	  
st1t=strmid(st1,0,4)+'-'+strmid(st1,4,2)+'-'+strmid(st1,6,2)+' '+string((t1/(60*60)),'(i2.2)')+':'+string((t1/60-((t1/3600)*60)),'(i2.2)')+':'+strmid(st1,13,2)
st2t=strmid(st1,0,4)+'-'+strmid(st1,4,2)+'-'+strmid(st1,6,2)+' '+string((t2/(60*60)),'(i2.2)')+':'+string((t2/60-((t2/3600)*60)),'(i2.2)')+':'+strmid(st1,13,2)
IF ~keyword_set(SILENT) THEN BEGIN
    print,'looking for sequence from ',fnamen
    print,' between ',st1t,' and ',st2t 
ENDIF

s=cor1_pbseries([st1t,st2t],sct,COR2=isc2, ssr=ssrval)  

IF ~keyword_set(SILENT) THEN print,s
;
; Verify file existence, try .gz
;
IF n_elements(s) NE 3 THEN return,'' ELSE BEGIN
    FOR i=0,2 DO $
    IF ~file_exist(s[i].filename) THEN $
    if file_exist(s[i].filename+'.gz') THEN s[i].filename=s[i].filename+'.gz' $
    ELSE BEGIN
    	message,s[i].filename+' not found.'
	return,''
    ENDELSE
ENDELSE

return,s.filename

end
