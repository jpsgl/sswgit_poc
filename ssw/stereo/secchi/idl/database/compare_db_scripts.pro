pro compare_db_scripts,yyyymm,usrc=usrc

;+
; $Id: compare_db_scripts.pro,v 1.3 2018/10/22 16:15:45 mcnutt Exp $
;
;NAME: 

;PURPOSE:
;       compare and update missed image database scripts

;CALLING SEQUENCE:
;	compare_db_scripts,'201810'
;
;INPUTS:  yyyymm= string year and month
;
;KEYWORDS: 
;
;OUTPUTS:

;AUTHOR: L. Hutting 10/19/2018
;
;MODIFIED: 
;$Log: compare_db_scripts.pro,v $
;Revision 1.3  2018/10/22 16:15:45  mcnutt
;updated search with src value
;
;Revision 1.2  2018/10/19 15:57:44  secchia
;first run corrections
;
;Revision 1.1  2018/10/19 14:52:33  mcnutt
;checks prosecced script to attic scripts
;
SPAWN,'hostname',machine,/sh  
machine = machine(N_ELEMENTS(machine)-1)
src=strmid(machine,0,1)+rstrmid(machine,0,1)
if keyword_set(usrc) then src=usrc
dbindir='/net/earth/lasco/corona/data1/database/secchi/IMG/INSERTS/POST_LAUNCH/'
indbfiles=file_search(dbindir+'IN_DB/sccAlz'+src+yyyymm+'*')
ifiles=rstrmid(indbfiles,0,30)
dbdir=getenv('seb_img')+'/db/attic/'
pipedbfiles=file_search(dbdir+'sccAlz'+src+yyyymm+'*')
pfiles=rstrmid(pipedbfiles,3,30)

print,'Number of pipeline files: '+string(n_Elements(pfiles))
print,pfiles
print,'Number of db files:       '+string(n_Elements(ifiles))
print,ifiles

for n=0,n_elements(pfiles)-1 do begin 
  intest=where(ifiles eq pfiles[n],fcnt)


print,intest

  if fcnt eq 0 then begin
print,fcnt
    print,'INSERTING '+pfiles[n]+'  '+pipedbfiles[n]
    cmnd='cp '+pipedbfiles[n]+' '+dbindir
    spawn,cmnd
    wait,2
    cmnd='gunzip '+dbindir+pfiles[n]
    spawn,cmnd
    wait,2
  endif


endfor   

end
