PRO make_mysql_script,flist,fresult,no_path=no_path

; INPUTS:
  ;flist= '/net/ares/data/esfand/currs_raw*0501*_sql.script'
  ;fresult= 'insert_currs0501.script'

; OUTPUT:
  ;fresult contains mysql insert commands

  ;f=findfile(flist)
  f=file_search(flist)
  n= N_ELEMENTS(f)
  IF (n GT 0 AND f(0) NE '') THEN BEGIN
    IF (KEYWORD_SET(no_path)) THEN BEGIN
      toks=str_sep(f(0),'/')
      fn0= toks(N_ELEMENTS(toks)-1)
      f= STRMID(f,STRPOS(f(0),fn0),50)      
    ENDIF
    cmd= 'mysql -uroot -psecchi -vvv < '
    OPENW,in,fresult,/GET_LUN
    FOR i=0, n-1 DO BEGIN
      PRINTF,in,cmd+f(i)+' > '+f(i)+'.out'
    ENDFOR
    CLOSE,in
    FREE_LUN,in
  ENDIF ELSE BEGIN
    PRINT,'No such files: '+flist
  ENDELSE

  RETURN
END
