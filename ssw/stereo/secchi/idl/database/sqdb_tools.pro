; Oct 1996
; Ed Esfandiari

; "qdb_tools" uses a qdb output structure as input and, using widget interface,
; lets the user manipulate the data. User options are:
; 1. list the structure of the input (tag names and number of elements)
; 2. display rows of data for all or subset of column, write them to a file,
;    or send them to a printer.
; 3. display the browse images, subselect images, print the screen to a GIF
;    file, save the selected filenames, etc.


pro stools_event,event
 common which_sevent,tool_names,out_fname,input_select,input_list

 WIDGET_CONTROL, event.id, GET_UVALUE=uval
 WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE

 case (uval) of
  'done': begin
            tool_names='exit'
            WIDGET_CONTROL, /DESTROY, struct.base
          end
  'helps_win': begin
            tool_names='helps_win'
            WIDGET_CONTROL, /DESTROY, struct.base
          end
  'rows':   begin
             tool_names='print_srows'
             WIDGET_CONTROL, /DESTROY, struct.base
          end
  'plt': begin
             tool_names='plot_rows'
             WIDGET_CONTROL, /DESTROY, struct.base
          end
  'bros': begin
           exists= where(input_list eq out_fname(0),cnt)
           if(cnt gt 0) then begin
;             res=widget_message('output file already exists.',/cancel)
             res=widget_message('Output file already exists. Overwrite?', $
                 /question,title='Warning')
             if(res eq 'Yes') then begin
               tool_names='disp_select'
               WIDGET_CONTROL, /DESTROY, struct.base
             end
           endif else begin
             tool_names='disp_select'
             WIDGET_CONTROL, /DESTROY, struct.base
           endelse
          end
  'out_name': begin
             WIDGET_CONTROL, event.id ,GET_VALUE=out_fname
             out_fname= strtrim(out_fname,2)
           end
  'in_name': begin
               input_select= input_list(event.index)
             end
 endcase

end


pro sqdb_tools,in_st
  common which_sevent,tool_names,out_fname,input_select,input_list

  IF (!version.release GE 5.5) THEN BEGIN
    PRINT,''
    PRINT,' Please report all widget problems to Ed Esfandiari'
    PRINT,''
  ENDIF

  original= in_st
  st= original
  out_fname= ['out1']
  input_list=['original']
  input_select= 'st'
next:
  ;base= widget_base(title='SECCHI SQDB Tools',xsize=470,ysize=200,xoffset=10,/frame)
  ;base= widget_base(title='SECCHI SQDB Tools',/frame)
  base= widget_base(title='SECCHI SQDB Tools',/frame,xoffset=10,yoffset=10)
  b1=widget_base(base,/ROW)
  colb1= widget_base(b1,/COLUMN)
  rowb1= widget_base(colb1,/ROW)
  exit=  WIDGET_BUTTON(rowb1, VALUE='Exit',UVALUE='done')
  r1= widget_base(colb1,/ROW)
  lab= WIDGET_LABEL(r1,VALUE='Input used for the following tools is')
  ind=where(input_list eq input_select,cnt)
  if(cnt eq 0) then ind= [0] 
  in_struct= cw_bselector(r1,UVALUE='in_name',input_list,set_value=ind(0))
  colb2= widget_base(colb1,/COLUMN)
  select= WIDGET_BUTTON(colb2, UVALUE='helps_win',value='About Current Input')
  select= WIDGET_BUTTON(colb2, UVALUE='rows',value='Display Rows of Data')
  rr= widget_base(colb1,/ROW)
  select= WIDGET_BUTTON(colb2, UVALUE='plt',value='Plot HK Data')
  select= WIDGET_BUTTON(rr, UVALUE='bros',value='Display Browse Images')
  lab= WIDGET_LABEL(rr,VALUE='and store those selected in')
  select= WIDGET_TEXT(rr,UVALUE='out_name',VALUE= out_fname,/EDITABLE, $
                      /ALL_EVENTS)

  widget_control,/realize,base
  struct={base:base}
  widget_control,base, SET_UVALUE=struct
  xmanager,'tools',base,EVENT_HANDLER='stools_event'

  exx= execute("st= "+input_select)

  case (tool_names) of
   'exit': return
   'helps_win': begin
             tn= tag_names(st,/struct)
             if(strlen(tn) eq 0) then tn='Anonymous'
             msg= ['Structure Name:  '+tn,$
                   'Number of rows: '+strtrim(n_elements(st),2),$
                   'Number of tags:  '+strtrim(n_tags(st),2),$
                   'Tag Names:',$
                   '          '+tag_names(st)]

              ;res=widget_message(msg,title='About Current Structure',/info)
              mtgs= 30
              IF (!version.release GE 5.5) THEN mtgs= 50 

              if(n_tags(st) le mtgs) then begin
                res=widget_message(msg,title='About Current Structure',/info)
              endif else begin
                res=widget_message(msg(0:mtgs+3),title='First '+strtrim(mtgs,2)+' Tags Info ',/info)
              endelse 
 
              ;helps_win,st
              end
   'print_srows': begin
               print,''
               print,'reading and formatting the data....'
               print,''
               print_srows,st
             end
   'plot_rows': begin
               ;help,/st,st
               IF (n_elements(st) lt 3) THEN $ 
                 PRINT,'Less than 3 data points in structure. Plot HK igored.' $
               ELSE $
                 plot_rows,st
             end
   'disp_select': begin
                    out_fname= out_fname(0)
                    sub_st= disp_select(st)
                    out_size= size(sub_st)
                    if(out_size(2) eq 8) then begin ; valid output
                      exx=execute(out_fname+"= sub_st")
;                      save,file=out_fname,sub_st
                      ind= where(input_list eq out_fname,cnt)
;                     add output name to the list only if already not in
;                     the list. If it is already in the list, then we have
;                     overwritten the old output file with the new (if vaild)
;                     output.
                      if(cnt eq 0) then input_list=[input_list,out_fname] 
                    end
                  end
  endcase
  goto,next   
end
