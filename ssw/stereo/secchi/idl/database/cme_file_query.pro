function cme_file_query, atime, etime, AHEAD=ahead, BEHIND=behind, C2=c2, C3=c3, cor1=cor1, hi1=hi1, SILENT=silent
;+
; $Id: cme_file_query.pro,v 1.5 2011/02/17 18:37:19 mcnutt Exp $
;
;NAME: 

;PURPOSE:
;       Return list of files of based on input time. Queries data sets of
;   	COR2-A, COR2-B, LASCO C2 and/or LASCO C3

;CALLING SEQUENCE:
;	list=cme_file_query('2009/12/11 13:22')
;
;INPUTS:
;   	atime	Start time of CME in C2 FOV (input to anytim2utc)
;
;KEYWORDS: 
;	SILENT: Supress output of all error messages except lower-level IDL
;		and system messages. 
;
;   	/AHEAD, /Behind, /C2, /C3   Limit output to specified telescopes; default is all

;OUTPUTS:
;      STRARR	List of files in order of LASCO-C2, LASCO-C3,  COR2-A, COR2-B

;AUTHOR: N.B.Rich   NRL/I2, Aug. 2010
;
;MODIFIED: 
;$Log: cme_file_query.pro,v $
;Revision 1.5  2011/02/17 18:37:19  mcnutt
;added cor1 and hi1 keywords
;
;Revision 1.4  2010/09/08 14:32:28  mcnutt
;added optional end time
;
;Revision 1.3  2010/08/17 19:10:41  nathan
;filter out cor2 test images
;
;Revision 1.2  2010/08/17 18:59:00  mcnutt
;returns list in a structure format
;
;Revision 1.1  2010/08/16 20:11:19  nathan
;input for cme mass calculation programs
;

errstr=''

kc2a=keyword_set(AHEAD)
kc2b=keyword_set(BEHIND)
kc2 =keyword_set(C2)
kc3 =keyword_set(C3)
kcor1 =keyword_set(cor1)
khi1 =keyword_set(hi1)
kall=kc2a+kc2b+kc2+kc3+kcor1+khi1

; estimate several images previous to get pre-event image
; and end of CME
if datatype(etime) eq 'UND' then begin
  earlier=3*3600.     ;3 hours
  later  =18*3600.    ;18 hours
  ut1=tai2utc(anytim2tai(atime)-earlier)
  ut2=tai2utc(anytim2tai(atime)+later)
endif else begin
  ut1=tai2utc(anytim2tai(atime))
  ut2=tai2utc(anytim2tai(etime))
endelse

print,''
print,'For interval ',utc2str(ut1,/ecs,/trunc),' to ',utc2str(ut2,/ecs,/trunc)

; Get C2 images
IF kall EQ 0 or kc2 THEN BEGIN
    s=las_read_summary(date=[ut1,ut2],tel='c2')
    if datatype(s) ne 'STC' then s=las_read_summary(date=[ut1,ut2],tel='c2',source='ql')
    if datatype(s) eq 'STC' then begin
      nc2=n_elements(s)
      message,'Found '+trim(nc2)+' C2 images.',/info
      c2=s.dir+s.filename
    endif else begin
        message,'**NO C2 images found**',/info
        c2=''
    endelse
ENDIF else c2=''

; Get C3 images

IF kall EQ 0 or kc3 THEN BEGIN
    s=las_read_summary(date=[ut1,ut2],tel='c3')
    if datatype(s) ne 'STC' then s=las_read_summary(date=[ut1,ut2],tel='c3',source='ql')
    if datatype(s) eq 'STC' then begin
      nc3=n_elements(s)
      message,'Found '+trim(nc3)+' C3 images.',/info
      c3=s.dir+s.filename 
    endif else begin
        message,'**NO C3 images found**',/info
        c3=''
    endelse
ENDIF else c3=''

; Get COR2-A images

IF kall EQ 0 or kc2a THEN BEGIN
    s=scc_read_summary(date=[ut1,ut2],tel='cor2',spacec='a',type='img',/nobeacon)
    if datatype(s) eq 'STC' then begin
    	w=where(s.exptime GT 3,nw)
	IF nw GT 1 THEN s=s[w]
        nc2a=n_elements(s)
        message,'Found '+trim(nc2a)+' COR2-A images.',/info
        sc_a=sccfindfits(s.filename)
    endif else begin
        message,'**NO COR2-A images found**',/info
        sc_a=''
    endelse
ENDIF else sc_a=''


; Get COR2-B images

IF kall EQ 0 or kc2b THEN BEGIN
    s=scc_read_summary(date=[ut1,ut2],tel='cor2',spacec='b',type='img',/nobeacon)
    if datatype(s) eq 'STC' then begin
    	w=where(s.exptime GT 3,nw)
	IF nw GT 1 THEN s=s[w]
    	nc2b=n_elements(s)
    	message,'Found '+trim(nc2b)+' COR2-B images.',/info
    	sc_b=sccfindfits(s.filename)
    endif else begin
    	message,'**NO COR2-B images found**',/info
    	sc_b=''
   endelse
ENDIF else sc_b=''

; Get COR1-A images 
cor1a='' & cor1b='' & hi1a='' & hi1b=''
IF keyword_set(cor1) THEN BEGIN
    s=scc_read_summary(date=[ut1,ut2],tel='cor1',spacec='a',type='seq',/nobeacon)
    if datatype(s) eq 'STC' then begin
    	w=where(s.value EQ 0 and s.dest eq 'SSR1',nw)
	IF nw GT 1 THEN s=s[w]
    	nc1b=n_elements(s)
    	message,'Found '+trim(nc1b)+' COR1- images.',/info
    	cor1a=sccfindfits(s.filename)
    endif else begin
    	message,'**NO COR1-B images found**',/info
    	cor1a=''
   endelse
; Get COR1-B images
    s=scc_read_summary(date=[ut1,ut2],tel='cor1',spacec='b',type='seq',/nobeacon)
    if datatype(s) eq 'STC' then begin
    	w=where(s.value EQ 0 and s.dest eq 'SSR1',nw)
	IF nw GT 1 THEN s=s[w]
    	nc1b=n_elements(s)
    	message,'Found '+trim(nc1b)+' COR1- images.',/info
    	cor1b=sccfindfits(s.filename)
    endif else begin
    	message,'**NO COR1-B images found**',/info
    	cor1b=''
   endelse
ENDIF 
IF keyword_set(hi1) THEN BEGIN
    s=scc_read_summary(date=[ut1,ut2],tel='hi1',spacec='a',type='img',/nobeacon)
    if datatype(s) eq 'STC' then begin
    	w=where(s.exptime GT 1000,nw)
	IF nw GT 1 THEN s=s[w]
    	nc1b=n_elements(s)
    	message,'Found '+trim(nc1b)+' hi1- images.',/info
    	hi1a=sccfindfits(s.filename)
    endif else begin
    	message,'**NO hi1-A images found**',/info
    	hi1a=''
   endelse
; Get hi1-B images
    s=scc_read_summary(date=[ut1,ut2],tel='hi1',spacec='b',type='img',/nobeacon)
    if datatype(s) eq 'STC' then begin
    	w=where(s.exptime GT 1000,nw)
	IF nw GT 1 THEN s=s[w]
    	nc1b=n_elements(s)
    	message,'Found '+trim(nc1b)+' hi1- images.',/info
    	hi1b=sccfindfits(s.filename)
    endif else begin
    	message,'**NO hi1-B images found**',/info
    	hi1b=''
   endelse
ENDIF 

li={sc_a:sc_a,sc_b:sc_b,c2:c2,c3:c3,cor1b:cor1b,cor1a:cor1a,hi1a:hi1a,hi1b:hi1b}

return,li

end

