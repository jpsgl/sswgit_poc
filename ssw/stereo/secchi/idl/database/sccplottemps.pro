;pro sccplottemps, sc, stadt, enddt, RES=res, SAVE=save, SUBSET=subset
;+
; $Id: sccplottemps.pro,v 1.4 2013/04/29 16:15:32 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : 
;               
; Purpose   : plot all HB temperature mnemonics from database
;               
; Use       : IDL> sccplottemps,'a','2010/11/01','2011/11/01'
;    
; Inputs    : sc    'a' or 'b'
;   	      stadt 	First date of interval (anytim2utc)
;   	      enddt 	Last date of interval
;               
; Outputs   :         
;
; Keywords  : 
;   	    /SAVE   Save plots in ~/plots or set to savedir
;   	    RES=    Interval between points in minutes; default/max is 60
;   	    SUBSET= description of subset to plot: COR1, SCIPCCD
;               
; Calls     : get_hbtemps.pro
;               
; Category    : plot telemetry hk safety
;               
; Prev. Hist. : None.
;
; Written     :N.Rich
;               
; $Log: sccplottemps.pro,v $
; Revision 1.4  2013/04/29 16:15:32  nathan
; fix label offset
;
; Revision 1.3  2013/03/25 21:35:43  nathan
; add /USELAST and set yrange for plots
;
; Revision 1.2  2012/02/10 20:08:15  nathan
; fix some structure subscripts; add SUBSET=
;
; Revision 1.1  2011/03/24 20:19:26  nathan
; plot all hb temps
;

pro plot_t, YRANGE=yrange, YTITLE=ytitle

COMMON plothk, s, indx, w, ptitle

tags=tag_names(s)
print,'Plotting ',tags[indx]
nm=n_elements(indx)
nr=n_elements(s.pckt_time)
IF keyword_set(YTITLE) THEN ytit=ytitle ELSE ytit='Deg C'

IF keyword_set(YRANGE) THEN BEGIN
    pmn=yrange[0]
    pmx=yrange[1]
ENDIF ELSE BEGIN
    pmn=min(s.(indx[0]),max=pmx)
    FOR i=1,nm-1 DO BEGIN
	pmn=min([pmn,min(s.(indx[i]),max=mxi)])
	pmx=max([pmx,mxi])
    ENDFOR
ENDELSE
window,w
utplot,s.pckt_time,s.(indx[0]),title=ptitle,ytitle=ytit,yrange=[pmn,pmx], xmargin=[10,9]

valn=(s.(indx[0]))[nr-1]
cols=0
FOR i=1,nm-1 DO BEGIN
    outplot,s.pckt_time,s.(indx[i]),color=i+1
    valn=[valn,(s.(indx[i]))[nr-1]]
    cols=[cols,i+1]
ENDFOR

srt=sort(valn)
yran=float(pmx)-pmn
incr=yran/(nm-1)
help,yran,incr
FOR i=0,nm-1 DO $
    ut_label,s.pckt_time[nr-1],pmn+i*incr,tags[indx[srt[i]]],labcolor=cols[srt[i]]

end

pro sccplottemps, sc, stadt, enddt, RES=res, SAVE=save, SUBSET=subset, USELAST=uselast

COMMON plothk

ab=strupcase(sc)
IF n_params() LT 3 THEN enddt=stadt

IF keyword_set(RES) THEN intrv=res<60 ELSE intrv=60

IF keyword_set(USELAST) THEN message,'Using previous query:',/info ELSE $ 
s=get_hbtemps(sc,'all',stadt,enddt,RES=intrv)

help,/str,s
np=n_elements(s.pckt_time)

IF keyword_set(SUBSET) THEN tel=strupcase(subset) ELSE tel='ALL'

IF keyword_set(SAVE) THEN BEGIN
    if datatype(save) EQ 'STR' THEN savedir=save ELSE savedir='~/plots'
    ut1=anytim2utc(stadt)
    ut2=anytim2utc(enddt)
    yrs=(ut2.mjd-ut1.mjd+1)/365.
ENDIF
plotprep

IF tel EQ 'ALL' THEN BEGIN

; plot HI and CCDs

indx=[34,35,36,37,38,39,40,20,21,22,23,24]  ; NOTE: this depends on order of mnemonics in get_hbtemps.pro (v 1.7)
w=1
IF np GT 10000 THEN yran=[-85,0] ELSE yran=0
ptitle='SECCHI-'+ab+' HI and CCD Temperatures, '+trim(intrv)+' Min. Resolution'
plot_t, YRANGE=yran
IF keyword_set(SAVE) THEN BEGIN
    outfile=concat_dir(savedir,'hb'+ab+'hiccdtemps_'+trim(round(yrs))+'yr')
    print,'Saving ',outfile
    jk=ftvread(/png,filen=outfile,/noprompt)
ENDIF

; plot EUVI and electronics boxes

indx=[2,3,4,5,6,7,8,25,26,27,28,30,31,32]
w=2
IF np GT 10000 THEN yran=[10,50] ELSE yran=0
ptitle='SECCHI-'+ab+' EUVI and Electronics Temperatures, '+trim(intrv)+' Min. Resolution'
plot_t, YRANGE=yran
IF keyword_set(SAVE) THEN jk=ftvread(/png,filen=concat_dir(savedir,'hb'+ab+'euvielecttemps_'+trim(round(yrs))+'yr'),/noprompt)

; plot COR1 and COR2 and SCIP bench

indx=[9,10,11,12,13,14,15,16,17,18,19,29,33]
w=4
;IF np GT 10000 THEN yran=[10,50] ELSE yran=0
ptitle='SECCHI-'+ab+' COR and SCIP Bench Temperatures, '+trim(intrv)+' Min. Resolution'
plot_t, YRANGE=yran
IF keyword_set(SAVE) THEN jk=ftvread(/png,filen=concat_dir(savedir,'hb'+ab+'corsciptemps_'+trim(round(yrs))+'yr'),/noprompt)

ENDIF ELSE BEGIN
    w=0
    IF tel EQ 'COR1' THEN indx=[15,16,17,18,19]
    IF tel EQ 'SCIPCCD' THEN indx=[20,21,22]
    ptitle='SECCHI-'+ab+' '+tel+' Temperatures, '+trim(intrv)+' Min. Resolution'
    plot_t
    IF keyword_set(SAVE) THEN jk=ftvread(/png,filen=concat_dir(savedir,'hb'+ab+strlowcase(tel)+'temps_'+trim(round(yrs))+'yr'),/noprompt)
ENDELSE

stop
end
