function get_hbtemps,sc,mnemonics,d1,d2,t1,t2,res=res, WRITE_OUT=write_out
; $Id: get_hbtemps.pro,v 1.10 2011/10/18 15:27:17 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : get_hbtemps
;               
; Purpose     : get converted hkb temps from database
;               
; Explanation : 
;               
; Use         : IDL> hbtemps=get_hbtemps,sc,mnemonics,d1,d2,t1,t2,res=10
;    
; Inputs      :   sc = A or B 
;   	    	  Mnemonics = string of temperature mnemonics 'HBCOR2ZONE1T,HBCOR1ZONE1T,HBEUVIZONE1T'
;   	    	    	    ='all'  	for all HB temperatures
;   	     	  d1 =   first day or full day requested (input to nrlanytim2utc)
;    	    	    	    if only d1 is set then the full day is returned
;   	    	  d2 = ending day optional - 00:00 of the day unless d2=d1 or t2 is set
;   	    	  t1 = start time hhmm for d1 optional
;   	    	  t2 = end time hhmm for d2 optional
;               
; Outputs     :   hbtemps = structure of packettimes, unix_time, and mnemonics
;
; Keywords :	RES= 	    resolution in minutes (60,30,10,1,0.1) 0.1 for full, defaults to 1 min
;               /WRITE_OUT  Write result to a comma-delimited flatfile; optionally specify filename
;
; Calls from LASCO : none
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Database 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Aug 2006
;               
; Modified    :
;
; $Log: get_hbtemps.pro,v $
; Revision 1.10  2011/10/18 15:27:17  nathan
; fix error with one-day-input
;
; Revision 1.9  2011/05/27 18:33:28  secchib
; for same date, change time not date for d2, so do not get next day packet
;
; Revision 1.8  2011/04/06 21:08:45  secchib
; nr - do not change inputs
;
; Revision 1.7  2011/03/25 21:44:48  nathan
; allow any time format as input
;
; Revision 1.6  2010/04/19 19:43:47  nathan
; if res= >60 default to 60
;
; Revision 1.5  2007/01/23 19:49:06  mcnutt
; cahnged full res to 0.1
;
; Revision 1.4  2007/01/16 23:27:38  nathan
; fix call to GET_CONVERT_STRING after removing reference to NRL_LIB/dev
;
; Revision 1.3  2006/10/31 17:53:10  mcnutt
; updated to use flight database
;
; Revision 1.2  2006/10/14 15:29:25  secchib
; LCM corrected resol lines
;
; Revision 1.1  2006/09/22 21:42:35  nathan
; moved from dev/database
;
; Revision 1.1  2006/08/03 19:55:10  mcnutt
; returns a structure of converted HB temperatures from sqdb
;
;
;
hk_temps='hk_temps_'+strlowcase(sc) 
secchi_hk='secchi_flight_hk_'+strupcase(sc)
resol=' group by mid('+hk_temps+'.pckt_time,1,16)'
if(keyword_set(res)) then begin
 if (Res ge 60) then resol=' group by mid('+hk_temps+'.pckt_time,1,13)'
 if (Res eq 30) then resol=' and (CAST('+hk_temps+'.pckt_time as CHAR) like "%:00:0%" OR CAST('+hk_temps+'.pckt_time as CHAR) like "%:30:0%") group by mid('+hk_temps+'.pckt_time,1,16)'
 if (Res eq 10) then resol=' and CAST('+hk_temps+'.pckt_time as CHAR) like "%:%0:0%" group by mid('+hk_temps+'.pckt_time,1,16)'
 if (Res eq 0.1) then resol=''
endif

ut1=nrlanytim2utc(d1)
time1=utc2str(ut1,/ecs)
if(datatype(t1) NE 'UND')then begin
    t1=string(t1,'(i4.4)')
    time1=strmid(time1,0,11)+strmid(t1,0,2)+':'+strmid(t1,2,2)
endif

if(datatype(d2) NE 'UND')then begin
    ut2=nrlanytim2utc(d2)
endif ELSE BEGIN
    ut2=ut1
ENDELSE
time2=utc2str(ut2,/ecs)
if(datatype(t2) NE 'UND')then begin
    t2=string(t2,'(i4.4)')
    time2=strmid(time2,0,11)+strmid(t2,0,2)+':'+strmid(t2,2,2)
endif

IF time1 GE time2 THEN BEGIN
    ut2.time=ut2.time+(86399.*1e3)
    time2=utc2str(ut2,/ecs)
ENDIF

IF strupcase(mnemonics) EQ 'ALL' THEN mnemonics='HBHKPBRDT,HBRAD750T,HBPWRCVTYT,HBPIBT,HBMEBENCLT,HBSCIPCEBENCLT,HBEUVIZONET,HBCOR2ZONE1T,HBCOR2ZONE2T,HBCOR2ZONE3T,HBCOR2HRMRRT,HBCOR2FLDLNST,HBCOR2RLYLNST,HBCOR1POLDOUB1T,HBCOR1TUBEOCCT,HBCOR1ZONE1T,HBCOR1ZONE2T,HBCOR1DOUB2T,HBEUVICCDT,HBCOR2CCDT,HBCOR1CCDT,HBHI1CCDT,HBHI2CCDT,HBEUVIFWDMNTT,HBGTFWDFILTRT,HBEUVISECMIRT,HBGTPREAMPT,HBSCIPBNCHAFTT,HBEUVIAFTMNTT,HBEUVIENTRT,HBEUVIAFTSHTRT,HBSCIPBNCHFWDT,HBHICEBENCLT,HBHIZONE1T,HBHIZONE2T,HBHIBACKSTRT,HBHIFRNTSTRT,HBHIBASESTRT,HBHIFINT'

cmnd='select pckt_time,unix_time,'+mnemonics+' from '+hk_temps+' where pckt_time between "'+time1+'" and "'+time2+'"'+resol
stemps=SQDB (secchi_hk,cmnd)
z=size(stemps)
if (z(1) ne 7) then begin
    tags=tag_names(stemps) & ntags=n_Elements(tags)
    hbtemps=create_struct(tags(0),stemps.(0),tags(1),stemps.(1))
    for i=2,ntags-1 do begin
    	cnv_str= GET_CONVERT_STRING(tags(i),GETENV('SSW_SECCHI')+'/idl/database/hk1067_mnemonics_info.txt')
    	SECCHI_DN_CONVERT, stemps.(i), REPLICATE(cnv_str,n_elements(stemps.(i))), dn_cnvt, cnv_units
    	hbtemps=create_struct(hbtemps,tags(i),dn_cnvt)
    endfor
endif else hbtemps=stemps

return,hbtemps

end
