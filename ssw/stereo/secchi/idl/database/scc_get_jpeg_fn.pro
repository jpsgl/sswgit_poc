function scc_get_jpeg_fn,filename0, URL=url, OUTSIZE=outsize
;
;+
; $Id: scc_get_jpeg_fn.pro,v 1.1 2008/02/21 18:11:10 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_get_jpeg_fn.pro
;               
; Purpose   : Compute name of jpeg filename from fits filename and header
;               
; Explanation: 
;               
; Use       : IDL> URL = scc_get_jpeg_fn('20070815_000900_s4h1A.fts')
;    
; Inputs    : name of fits file
;               
; Outputs   : name of jpeg pretty picture equivalent including path/url
;
; Keywords  : /URL  Return URL rather than local network path
;   	      OUTSIZE = 256,512, or 1024  - default is 512
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: requires sector or wavelength for euvi files
;               
; Side effects: 
;               
; Category    : DAILY database www website browse display
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich I2/NRL, 2/21/08
;               
; $Log: scc_get_jpeg_fn.pro,v $
; Revision 1.1  2008/02/21 18:11:10  nathan
; template for cgi script
;


IF keyword_set(OUTSIZE) THEN sizedir=trim(256*((fix(outsize)/256)>1)) ELSE sizedir='512'
IF keyword_set(URL) THEN rootpath='http://secchi.nrl.navy.mil/jpg' ELSE $
    rootpath=getenv('SECCHI_JPG')
    
break_file,filename0,a,b,root,sfx
filename=root+sfx
cam=strmid(filename,18,2)
sc =strlowcase(strmid(filename,20,1))
datedir=strmid(filename,0,8)
type=strmid(filename,16,1)
IF type EQ 'd' THEN return,'Not available'
; there are no browse images for cor2 type double images

CASE cam OF
    'c1':  cam_str='cor1'
    'c2':  cam_str='cor'
    'h1':  cam_str='hi'
    'h2':  cam_str='hi'
    'eu':  cam_str='euvi' 
    ELSE:  
ENDCASE



path = rootpath + '/' + sc + '/' + cam_str + '/' + datedir 


; now get filenames...
if cam_str EQ 'euvi' then BEGIN
    xx=sccreadfits(sccfindfits(filename),outhdr,/nodata)
    wave=outhdr.WAVELNTH
    wave=strmid(trim(wave),0,2)
    fn=strmid(trim(filename),0,16)+wave+cam+strupcase(sc)+'.jpg'
endif else BEGIN
    wave='tb'
    ; need to find a way to retrieve filename of first image in a series
    strput,filename,'???',12
    fn0=strmid(trim(filename),0,16)+wave+cam+strupcase(sc)+'.jpg'
    list=file_search(getenv('SECCHI_JPG')+'/' + sc + '/' + cam_str + '/' + datedir+'/'+sizedir+'/'+fn0)
    
    IF list[0] EQ '' THEN return,'Not available' ELSE BEGIN
    	break_file,list[0],a,b,root1,sfx1
	fn=root1+sfx1
    ENDELSE
ENDELSE
;stop

outfile=path+'/'+sizedir+'/'+fn

return,outfile
END
