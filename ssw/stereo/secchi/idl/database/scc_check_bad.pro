;+
; Project     :	STEREO - SECCHI
;
; Name        :	SCC_CHECK_BAD
;
; Purpose     :	Checks filenames against lists of bad files.
;
; Category    :	SOHO, SECCHI, Catalog
;
; Explanation :	This routine takes one or more filenames, and checks them
;               against lists of bad files in the directory
;               $SSW/stereo/secchi/data/badimages.  The lists of bad files are
;               cached in a common block to save time on subsequent calls.
;
; Syntax      :	Result = SCC_CHECK_BAD(FILENAME)
;
; Examples    :	if scc_check_bad(filename) then return
;
; Inputs      :	FILENAME = The name of the file to be checked.  Can also be an
;                          array of filenames.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is 0 for a good file, or 1 for a bad
;               file.  If FILENAME is an array, then the result will be a byte
;               array with the same dimensionality.
;
; Opt. Outputs:	None.
;
; Keywords    :	DEBRIS = If set, then also check debris image lists.  Currently
;                        only implemented for COR2.
;
; Calls       :	CONCAT_DIR, BREAK_FILE, BOOST_ARRAY
;
; Common      :	Internal common block SCC_CHECK_BAD caches lists of bad files.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 9-Jan-2008, William Thompson, GSFC
;               Version 2, 4-Feb-2008, WTT, include check for EUVI
;               Version 3, 7-Apr-2008, WTT, fixed bug with fully qualified
;                                           filenames
;               Version 4, 25-Jul-2008, WTT/FA, change loops to long
;
; Contact     :	WTHOMPSON
;-
;
function scc_check_bad, filename, debris=debris
;
common scc_check_bad, cor1a_bad, cor1b_bad, cor2a_bad, cor2b_bad, $
  hi1a_bad, hi1b_bad, hi2a_bad, hi2b_bad, euvia_bad, euvib_bad, $
  cor2a_debris, cor2b_debris
;
badpath = concat_dir(getenv('SSW_SECCHI'), 'data')
badpath = concat_dir(badpath, 'badimages')
;
n_files = n_elements(filename)
result = bytarr(n_files)
break_file, filename, disk, dir, name, ext
tel = strmid(name,18,2)
sc  = strmid(name,20,1)
;
;  EUVI-A
;
w = where((tel eq 'eu') and (sc eq 'A'), count)
if count gt 0 then begin
    if n_elements(euvia_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'euvia_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, euvia_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    if n_elements(euvia_bad) gt 0 then for i=0L,count-1 do begin
        ww = where(name[w[i]] eq euvia_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  EUVI-B
;
w = where((tel eq 'eu') and (sc eq 'B'), count)
if count gt 0 then begin
    if n_elements(euvib_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'euvib_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, euvib_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    if n_elements(euvib_bad) gt 0 then for i=0L,count-1 do begin
        ww = where(name[w[i]] eq euvib_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  COR1-A
;
w = where((tel eq 'c1') and (sc eq 'A'), count)
if count gt 0 then begin
    if n_elements(cor1a_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'cor1a_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, cor1a_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq cor1a_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  COR1-B
;
w = where((tel eq 'c1') and (sc eq 'B'), count)
if count gt 0 then begin
    if n_elements(cor1b_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'cor1b_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, cor1b_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq cor1b_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  COR2-A
;
w = where((tel eq 'c2') and (sc eq 'A'), count)
if count gt 0 then begin
    if n_elements(cor2a_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'cor2a_mixed.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, cor2a_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq cor2a_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
;
    if keyword_set(debris) then begin
        if n_elements(cor2a_debris) eq 0 then begin
            openr, unit, concat_dir(badpath, 'cor2a_debris.txt'), /get_lun
            line = 'string'
            while not eof(unit) do begin
                readf, unit, line
                if strmid(line,0,1) ne ';' then begin
                    words = strsplit(line,/extract)
                    break_file, words[0], disk0, dir0, name0
                    boost_array, cor2a_debris, name0
                endif
            endwhile
            free_lun, unit
        endif
        for i=0L,count-1 do begin
            ww = where(name[w[i]] eq cor2a_debris, nwhere)
            result[w[i]] = result[w[i]] or (nwhere gt 0)
        endfor
    endif
endif
;
;  COR2-B
;
w = where((tel eq 'c2') and (sc eq 'B'), count)
if count gt 0 then begin
    if n_elements(cor2b_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'cor2b_mixed.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, cor2b_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq cor2b_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
;
    if keyword_set(debris) then begin
        if n_elements(cor2b_debris) eq 0 then begin
            openr, unit, concat_dir(badpath, 'cor2b_debris.txt'), /get_lun
            line = 'string'
            while not eof(unit) do begin
                readf, unit, line
                if strmid(line,0,1) ne ';' then begin
                    words = strsplit(line,/extract)
                    break_file, words[0], disk0, dir0, name0
                    boost_array, cor2b_debris, name0
                endif
            endwhile
            free_lun, unit
        endif
        for i=0L,count-1 do begin
            ww = where(name[w[i]] eq cor2b_debris, nwhere)
            result[w[i]] = result[w[i]] or (nwhere gt 0)
        endfor
    endif
endif
;
;  HI1-A
;
w = where((tel eq 'h1') and (sc eq 'A'), count)
if count gt 0 then begin
    if n_elements(hi1a_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'hi1a_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, hi1a_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq hi1a_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  HI1-B
;
w = where((tel eq 'h1') and (sc eq 'B'), count)
if count gt 0 then begin
    if n_elements(hi1b_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'hi1b_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, hi1b_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq hi1b_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  HI2-A
;
w = where((tel eq 'h1') and (sc eq 'A'), count)
if count gt 0 then begin
    if n_elements(hi2a_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'hi2a_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, hi2a_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq hi2a_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
;  HI2-B
;
w = where((tel eq 'h1') and (sc eq 'B'), count)
if count gt 0 then begin
    if n_elements(hi2b_bad) eq 0 then begin
        openr, unit, concat_dir(badpath, 'hi2b_bad.txt'), /get_lun
        line = 'string'
        while not eof(unit) do begin
            readf, unit, line
            if strmid(line,0,1) ne ';' then begin
                words = strsplit(line,/extract)
                break_file, words[0], disk0, dir0, name0
                boost_array, hi2b_bad, name0
            endif
        endwhile
        free_lun, unit
    endif
    for i=0L,count-1 do begin
        ww = where(name[w[i]] eq hi2b_bad, nwhere)
        result[w[i]] = nwhere gt 0
    endfor
endif
;
sz = size(filename)
if sz[0] eq 0 then result = result[0] else $
  result = reform(result, sz[1:sz[0]], /overwrite)
return, result
end
