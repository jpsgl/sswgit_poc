;+
;$Id: lister.pro,v 1.1 2007/01/12 22:41:51 nathan Exp $
;
; Project     : SECCHI & SOHO - LASCO
;
; Name        : LISTER
;
; Purpose     : Get a list of selected images.
;
; Category    : Widget 
;
; Explanation : This widget is a wrapper for selecting and using LASCO/EIT's wlister
;               or STEREO's scclister routines. It calls the selected function and 
;               returns an IDL structure of string array(s) containing filenames
;               and absolute paths of selected images. It will return an empty
;               string if no file is found.
;
; Use         : IDL>list=lister()
;
; Examples    : IDL>list=lister()
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : An IDL structure of string arrays (an empty string
;               if no files found).
;
; Opt. Outputs: IDL save set of image filenames and image info (scclister only).
;
; Keywords    : None
;
; Restrictions: Requires specification of enviroment variables for wlister
;               defined in file: instrument.def                              
;               Also requires specification of "secchi" enviroment variable
;               for scclister.   
;
;		Environment variables used:
;			IMAGES,LASCO_MSG,WORK,USER,HOME,LZ_IMG,QL_IMG
;			MONTHLY_IMAGES,FLATS,secchi
;
; Side effects: Not known
;
; History     : Ed Esfandiari November 2005
;               First Version.
;
;     %H% %W% LASCO NRL IDL LIBRARY
;
;$Log: lister.pro,v $
;Revision 1.1  2007/01/12 22:41:51  nathan
;moved from nrl_lib/dev/database
;
;Revision 1.2  2005/11/16 19:27:43  esfand
;*** empty log message ***
;
;Revision 1.1  2005/11/16 18:55:01  esfand
;Wrapper for wlister and scclister - first version - AEE
;
;
;-



PRO lister_event,event

COMMON results, msgs, res, observ, sc_sel

WIDGET_CONTROL, event.id, GET_UVALUE=uval  ; get the UVALUE of the event

 CASE uval OF

 "Help":  BEGIN
    msg= ['1. Select an observatory entry.', $
          '2. Press "Proceed" to execute selection or "Quit" to exit.']
    msg = WIDGET_MESSAGE(msg,/INFORMATION)
    ;xdisplayfile,TEXT=msg,height=20,width=65,title='Help',GROUP=event.top
   END

 "Observatory": BEGIN
    sc_sel= event.index
    WIDGET_CONTROL, msgs, SET_VALUE='Press "Proceed" to use "'+observ(sc_sel)+'"'
  END
                 	 
 "Proceed": BEGIN
    ;msg = WIDGET_MESSAGE("Not implemented yet!")
    WIDGET_CONTROL, msgs, SET_VALUE='Processing '+observ(sc_sel)+'....'
    WIDGET_CONTROL, /HOUR

    CASE sc_sel OF
      0:BEGIN
         WIDGET_CONTROL, event.top, /DESTROY ; use sc_sel (0) at end of lister.pro
                                             ; to call wlister from there.
        END

      1: BEGIN
           WIDGET_CONTROL, event.top, /DESTROY ; call slister at the end.
         END

      2: res= WIDGET_MESSAGE("Not implemented yet!",/INFORMATION)
      ELSE: res= WIDGET_MESSAGE("Invalid Observatory") 
    ENDCASE
    ;WIDGET_CONTROL, msgs, SET_VALUE='Finished processing "'+observ(sc_sel)+'"'
   END

 "Cancel": BEGIN
    sc_sel= -1
    WIDGET_CONTROL, event.top, /DESTROY
   END
  ELSE: donothing=0 
 ENDCASE

END




function lister

COMMON results, msgs, res, observ, sc_sel
 
IF (XRegistered("lister") NE 0) THEN RETURN,-1

 
font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
             
lbase = WIDGET_BASE(TITLE = "SECCHI & LASCO/EIT Image Selection Tool v.3.0", $
                   XSIZE=500, YSIZE= 180, XOFFSET=5, YOFFSET=5, /ROW)

row= WIDGET_BASE(lbase, /COL)
row1= WIDGET_BASE(row, /ROW)

;observ= ['SOHO (LASCO/EIT)', $
;         'STEREO A (SECCHI)', $
;         'STEREO B (SECCHI)', $
;         'STEREO A+B (SECCHI - all images)', $
;         'SECCHI A+B (SECCHI - synched images)', $
;         'STEREO A+B+SOHO (SECCHI+LASCO/EIT)']

observ= ['SOHO (LASCO/EIT)', $
         'STEREO (SECCHI)', $
         'STEREO+SOHO (SECCHI+LASCO/EIT)']

sc_sel= 0 ; default

obs= CW_BSELECTOR2(row1,observ,UVALUE='Observatory',LABEL_LEFT= '   Select Observatory: ',FONT=font)

row2= WIDGET_BASE(row, /ROW)
lab=  WIDGET_LABEL(row2, VALUE='    ')


row2= WIDGET_BASE(row, /ROW)
lab=  WIDGET_LABEL(row2, VALUE='    ')
exec= WIDGET_BUTTON(row2, VALUE='Proceed', UVALUE='Proceed',FONT=font)
lab=  WIDGET_LABEL(row2, VALUE='                      ')
help= WIDGET_BUTTON(row2, VALUE='Help', UVALUE='Help',FONT=font)
lab=  WIDGET_LABEL(row2, VALUE='                      ')
quit= WIDGET_BUTTON(row2, VALUE='Quit', UVALUE='Cancel',FONT=font)
row2= WIDGET_BASE(row, /ROW)
lab=  WIDGET_LABEL(row2, VALUE='    ')

blanks= '                                                    '
msgs= WIDGET_LABEL(row,VALUE=blanks+blanks, FONT=font, /FRAME)

WIDGET_CONTROL, lbase, /REALIZE

WIDGET_CONTROL, msgs, SET_VALUE='Press "Proceed" to use "'+observ(sc_sel)+'"'

;
; Load the structure into the main base's UVALUE for one way trip to handler
;

XMANAGER,"lister",lbase

PRINT,''
CASE sc_sel OF 
  0: BEGIN
       PRINT,'Calling WLISTER.....'
       res= WLISTER()
       IF (res(0) NE '') THEN res= {lasco:res}
     END
  1: BEGIN
       ;PRINT,'Calling SLISTER.....'
       ;res= SLISTER()
       PRINT,'Calling SCCLISTER.....'
       res= SCCLISTER()
     END

  ELSE: PRINT,'Exit without a selection.'
ENDCASE
PRINT,''
PRINT,'Done.'

IF datatype(res) EQ 'UND' THEN res = ''
return, res

END


