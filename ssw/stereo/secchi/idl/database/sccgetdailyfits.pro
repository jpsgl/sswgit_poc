function sccgetdailyfits, tel0, sc, WRITE_UPDATE=write_update
;+
; $Id: sccgetdailyfits.pro,v 1.1 2011/05/20 21:27:06 nathan Exp $
; NAME:
;
; PURPOSE:
;       Return list of 1 file per day for mission
;
; CATEGORY: UTILITY database statistics
;
; CALLING SEQUENCE:
;	list=segetdailyfits('hi2','a')
;
; INPUTS:
;       NONE - reads file from summary/dailyfiles_+tel+sc+.txt, if available
;
; OUTPUTS
;	Result is a string array 
;
; Keywords:
;   	/WRITE_UPDATE	Replace existing dailyfiles file in summary directory with result
;
; RESTRICTIONS:
;	/WRITE option requires write permission in SCC_DATA_PATH(sc,TYPE='summary')
;
; MODIFICATION HISTORY:
; $Log: sccgetdailyfits.pro,v $
; Revision 1.1  2011/05/20 21:27:06  nathan
; first commit
;
;-

sPATH = SCC_DATA_PATH(sc,TYPE='summary')

tel = getscctt ( tel0 )


lstfile=concat_dir(spath,'dailyfiles_'+tel+strlowcase(sc)+'.txt')
lis=readlist(lstfile[0])
nf=n_elements(lis)
stype='img'
IF tel EQ 'c1' THEN stype='seq'
;
; Need to get list up to date
IF lis[0] EQ '' THEN lastdate='20070101' ELSE lastdate=strmid(lis[nf-1],25,8)
utc=yymmdd2utc(lastdate)
utc0=utc
get_utc,utc1
FOR i=utc0.mjd+1,utc1.mjd-2 DO BEGIN
    utc.mjd=i
    s=scc_read_summary(date=utc,type=stype, /nobeacon, telescope=tel0, spacecr=sc, /verbose)
    ns=n_elements(s)
    IF ns GT 1 THEN BEGIN
	fn=sccfindfits(s[ns/2].filename)
	lz=strpos(fn,'lz')
	len=strlen(fn)
	line='$secchi/'+strmid(fn,lz,len-lz)
	IF lis[0] EQ '' THEN lis=line ELSE lis=[lis,line]
    ENDIF
ENDFOR

IF keyword_set(WRITE_UPDATE) THEN BEGIN
    openw,1,lstfile[0]
    nf2=n_elements(lis)
    FOR i=0,nf2-1 DO printf,1,lis[i]
    close,1
ENDIF

return,lis

END
