;+
;
; Project     : STEREO - SECCHI
;
; Name        : NEW_XSCALE
;
; Purpose     : This function uses widgets to prompt the user for entering
;               new x-axis begin and end dates. 
;
; Explanation : The earliest and last obsevation date as well currently
;               used start and stop times is presented to the user. The newly 
;               entered dates is then returned. 
;
; Use         : ret_val= NEW_XSCALE(first_dte,last_dte,cur_x1,cur_x2)
;
; Inputs      : first_dte  DateString  Earliest x-axis date in data
;               last_dte   DateString  Last date x_axis in data
; IN/Out:     : cur_x1     DateString  current/new x1 date
;               cur_x2     DateString  current/new x2 date           
;
; Opt. Inputs : caller     Structure containing id of caller. 
;
; Outputs     : ret_val    Integer (0=cancelled,1=submited)
;
; Opt. Outputs: None
;
; Keywords    : None 
;
; Written by  : Ed Esfandiari, NRL, March 2005 - First Version.
;
; Modification History:
;
;-

PRO NEW_XSCALE_EVENT, event

COMMON NEW_XSCALE_SHARE, ret_val, cur_d1, cur_d2

  WIDGET_CONTROL, event.top, GET_UVALUE=newx   ; get structure from UVALUE

    CASE (event.id) OF

	newx.textd1 : BEGIN
	   WIDGET_CONTROL, newx.textd1, GET_VALUE=val
	   WIDGET_CONTROL, newx.textd1, SET_VALUE=val(0)
	END

        newx.textd2 : BEGIN 
           WIDGET_CONTROL, newx.textd2, GET_VALUE=val
           WIDGET_CONTROL, newx.textd2, SET_VALUE=val(0)
        END

	newx.cancel : BEGIN	;** cancel and exit program
           ret_val = 0
	   WIDGET_CONTROL, /DESTROY, newx.base
	END

	newx.apply : BEGIN	;** return date to shift to exit program
	   WIDGET_CONTROL, newx.textd1, GET_VALUE=val
           cur_d1= val(0) 
           WIDGET_CONTROL, newx.textd2, GET_VALUE=val
           cur_d2= val(0)
           ret_val= 1
	   WIDGET_CONTROL, /DESTROY, newx.base
	END

        ELSE : BEGIN
        END

   ENDCASE

END

;__________________________________________________________________________________________________________
;

FUNCTION NEW_XSCALE, CALLER=caller, first_dte, last_dte, cur_x1, cur_x2

COMMON NEW_XSCALE_SHARE, ret_val, cur_d1, cur_d2

    
    IF XRegistered("NEW_XSCALE") THEN RETURN, -1

    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    cur_d1= cur_x1
    cur_d2= cur_x2

    IF (KEYWORD_SET(caller)) THEN $
       base = WIDGET_BASE(/COLUMN, TITLE='Rescale X-Axis', /FRAME, GROUP_LEADER=caller.id) $
    ELSE $
       base = WIDGET_BASE(/COLUMN, TITLE='Rescale X-Axis', /FRAME)


    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='     Data date-range: '+first_dte+' to '+last_dte)
    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='Enter New X-range: ')
     textd1 = WIDGET_TEXT(row, VALUE=cur_x1, /EDITABLE, XSIZE=25)
     textd2 = WIDGET_TEXT(row, VALUE=cur_x2, /EDITABLE, XSIZE=25)
    row = WIDGET_BASE(base, /ROW)
    row = WIDGET_BASE(base, /ROW)
     tmp = WIDGET_LABEL(row, VALUE='                                   ')
     apply = WIDGET_BUTTON(row, VALUE=" Apply ")
     tmp = WIDGET_LABEL(row, VALUE='                          ')
     cancel = WIDGET_BUTTON(row, VALUE=" Cancel ")

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, base, /REALIZE

    newx = CREATE_STRUCT( 'base', base, 		$
                             'apply', apply, 		$
                             'cancel', cancel, 		$
                             'textd1', textd1,          $
                             'textd2', textd2)

   WIDGET_CONTROL, base, SET_UVALUE=newx

   XMANAGER, 'NEW_XSCALE', base, /MODAL

   cur_x1= cur_d1
   cur_x2= cur_d2

   RETURN, ret_val

END
