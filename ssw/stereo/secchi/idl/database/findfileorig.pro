; $Id: findfileorig.pro,v 1.1 2009/08/10 19:46:25 nathan Exp $
;
; Name:     findfileorig
;
; Purpose:  given FITS filename (with path), return fileorig (with path)
;
; Inputs: 	string filename
;			
; Outputs:	string filename
;   	    
; Side effects:	
;
; Caveats:  
;
; Category:	
;
; $Log: findfileorig.pro,v $
; Revision 1.1  2009/08/10 19:46:25  nathan
; commit
;

function findfileorig, fitsfile

break_file,fitsfile,j,p,r,s
yymmdd=strmid(r,2,6)
j=sccreadfits(fitsfile,h,/nodata)
if h.obsrvtry EQ 'STEREO_A' THEN dir=getenv_slash('scia') ELSE dir=getenv_slash('scib')

return,dir+yymmdd+'/'+h.fileorig

end
