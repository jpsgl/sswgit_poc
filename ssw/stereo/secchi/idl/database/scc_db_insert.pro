pro scc_db_insert,a
;+	
; $Id: scc_db_insert.pro,v 1.5 2008/04/10 17:00:02 secchia Exp $
; PROJECT:  	    	    	STEREO-SECCHI
; NAME:				scc_db_insert
; PURPOSE:			writes statements to insert a record 
;                               into a MySQL dbms table
; CATEGORY:			DBMS, database, structures, header
; CALLING SEQUENCE:		scc_db_insert,a
; INPUTS:			a = a dbms table structure where the first
;                                   element of the structure is the data base 
;				    name, the second element is the table name
;                                   and other structure elements are the 
;                                   column names of the table.
;       
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		None
; OUTPUTS:			None
; OPTIONAL OUTPUT PARAMETERS:   None
; COMMON BLOCKS:		dbms
;				ludb = unit number of the file to write to
; SIDE EFFECTS:
; RESTRICTIONS:
; PROCEDURE:
; MODIFICATION HISTORY:		
;  Based on db_insert.pro from LASCO library
;
; $Log: scc_db_insert.pro,v $
; Revision 1.5  2008/04/10 17:00:02  secchia
; corrected delete line now based on db entry
;
; Revision 1.4  2008/04/10 16:09:36  mcnutt
; added filename and fileorig to database delete Bug 299
;
; Revision 1.3  2007/01/11 15:58:47  nathan
; implement delflag
;
; Revision 1.2  2006/10/03 19:31:53  nathan
; only write use and start if new file
;
; Revision 1.1  2006/09/08 16:57:21  nathan
; updated from LASCO db_insert.pro
;
;-
common dbms 	;,ludb, delflag - is defined in secchi_reduce.pro

nt=n_tags(a)
tags=tag_names(a)
dbfstat=fstat(ludb)
IF dbfstat.size EQ 0 THEN BEGIN
; only at beginning of each script file
    printf,ludb,'use '+a.(0)+';'
    printf,ludb,'start transaction;'
ENDIF

; In case this replaces a previous instance
IF (delflag) THEN BEGIN
    if a.table_name eq 'img_seb_hdr_ext' then $
    	printf,ludb,'delete from '+a.(1)+' where date_obs = "'+a.date_obs+'" and fileorig = "'+a.fileorig+'";' 
    if a.table_name eq 'img_hist_cmnt' then $
    	printf,ludb,'delete from '+a.(1)+' where date_obs = "'+a.date_obs+'" and filename = "'+a.filename+'";' 
    if a.table_name eq 'img_seb_hdr' then $
	printf,ludb,'delete from '+a.(1)+' where date_obs = "'+a.date_obs+'" and fileorig = "'+a.fileorig+'" and filename = "'+a.filename+'";'
ENDIF

printf,ludb,'insert ignore '+a.(1)  	; table name
printf,ludb,' ('+strlowcase(tags(2))+','
for i=3,nt-2 do printf,ludb,strlowcase(tags(i)),','
printf,ludb,strlowcase(tags(nt-1))+')'
printf,ludb,' values('
for i=2,nt-2 do begin
  s=size(a.(i))
  if (s(s(0)+1) eq 7) then begin  ; if eq then a.(i) is a string, so quote it, unless NULL
    if a.(i) EQ 'NULL' THEN printf,ludb,a.(i),',' ELSE printf,ludb,'"'+a.(i)+'",'
  endif else begin
    if(a.(1) eq 'img_browse' and strlowcase(tags(i)) eq 'browse_img') then begin
      ; initialize the browse_img column of img_browse table to zero and save 
      ; the byte type data values to be inserted later using hex format:
      printf,ludb,'0x00',','
      byte_data= a.(i)
    endif else begin
      printf,ludb,a.(i),','
    endelse
  endelse
endfor
; Last value
i=nt-1
s=size(a.(i))
if (s(s(0)+1) eq 7) then begin  ; if eq then a.(i) is a string, so quote it, unless NULL
   if a.(i) EQ 'NULL' THEN printf,ludb,a.(i),');' ELSE printf,ludb,'"'+a.(i)+'");'
endif else begin
   printf,ludb,a.(i),');'
endelse
;printf,ludb,'commit;'
; commit must be added by shell script which moves file to lambda

; For the "browse_img" column of "img_browse" table, use writetext to add the
; data since data is of type "image". This requires to change the byte values
; to their equivalent hex values expected by sybase:
 
if (a.(1) eq 'img_browse') then begin
  printf,ludb,'use '+a.(0)
  printf,ludb,'go'
  printf,ludb,'declare @val varbinary(16)'
  printf,ludb,'select @val = textptr(browse_img) from img_browse'
  printf,ludb,'where '+strlowcase(tags(2))+' = "'+a.(2)+'"' 
  printf,ludb,'writetext img_browse.browse_img @val 0x\'
  hex= string(byte_data(0),'(z2.2)')
  for i=1,n_elements(byte_data)-2 do begin
    if (i mod 20 eq 0) then begin
      printf,ludb,hex+'\'
      hex=''
    end
    hex= hex+string(byte_data(i),'(z2.2)') 
  end
  hex= hex+string(byte_data(i),'(z2.2)')
  printf,ludb,hex
  printf,ludb,'go'
endif
return
end
