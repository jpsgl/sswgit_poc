; Ed Esfandiari, September 04

FUNCTION PARSE_DB_INFO, fn

; parse_db_info parses a MySql input query file containig database or table informatio, 
; creates, and returns a string array containing the info.
; It also deletes the file upon completion.
;

; Inputs to this routine is a file containing results for one of these 3 queries:
;  show databases
;  show tables in a database
;  show columns in a table
;
;Examples:
;
;show databases:
;  esfand@lambda% ./mysql_query_lambda 'secchi' 'show databases'
;  =>
;  1 field_names are:
;  Database
;  field_types:
;  254
;  5 selected rows are:
;  lasco
;  mysql
;  secchi
;  solwind
;  test
;
;show tables in a database:
;  esfand@lambda% ./mysql_query_lambda 'secchi' 'show tables in secchi'
;  =>
;  1 field_names are:
;  Tables_in_secchi
;  field_types:
;  254
;  3 selected rows are:
;  img_info
;  img_seb_hdr
;  img_stats
;
;show columns in a table:
;  esfand@lambda% ./mysql_query_lambda 'secchi' 'select * from img_seb_hdr where 1=2'
;  =>
;  39 field_names are:
;  spacecraft|sync|os_num|obs_prog|set_id|led_id|led_pulses|fileorig|filename|synced_fn|date_mod|date_obs|camera|shutter|filter|polar_quad1|exptime1|polar_quad2|exptime2|hdr_only|image_cntr|seq_cntr|p1row|p1col|p2row|p2col|ccdxsum|ccdysum|sebxsum|sebysum|naxis1|naxis2|door_pos|dateorig|datalevel|source|ip_steps|diskpath|syncpath
;  field_types:
;  1|1|2|1|2|1|3|253|253|253|12|12|1|1|1|1|5|1|5|1|3|1|2|2|2|2|1|1|1|1|2|2|1|12|1|1|253|253|253
;  0 selected rows are:


  ;f= FINDFILE(fn)
  f= FILE_SEARCH(fn)
  IF (f(0) EQ '') THEN BEGIN
    PRINT,''
    PRINT,'Input file "'+fn+'" Does not exist.'
    PRINT,''
    RETURN,'bad input file'
  ENDIF
 
  tmp= ''
  OPENR,unit,fn,/get_lun,/delete
  result= FSTAT(unit)
  IF (result.size EQ 0) THEN BEGIN 
    FREE_LUN,unit
    CLOSE,unit
    RETURN,'no data'
  END 

  READF,unit,tmp ; first line has number of fields comment
  READF,unit,tmp ; 2nd line has field names separated by |
  field_names= STR_SEP(tmp,'|') 
  n_fields= N_ELEMENTS(field_names)
  READF,unit,tmp ; 3rd line has fields types comment
  READF,unit,tmp ; 4th line has field types separated by |
  field_types= FIX(STR_SEP(tmp,'|'))
  READF,unit,tmp ; 5th line has number of  selected rows comment
  num_rows= STR_SEP(tmp,' ')
  num_rows= LONG(num_rows(0)) 

  ;help,num_rows

  idl_types= STRARR(n_fields) 
  FOR i= 0, n_fields-1 DO BEGIN
    CASE 1 OF
      field_types(i) EQ 1 OR $  ;1 = tinyint
      field_types(i) EQ 2: BEGIN ;2 = smallint
                             idl_types(i)= "Integer"
                           END 
      field_types(i) EQ 3: BEGIN ; integer
                             idl_types(i)= "Long"
                            END
      field_types(i) EQ 5: BEGIN ; real
                             idl_types(i)= "Real"
                           END
      field_types(i) EQ 12: BEGIN ; datetime
                             idl_types(i)= "String"
                           END
      field_types(i) EQ 253: BEGIN ; varchar
                               idl_types(i)= "String"
                             END
      ELSE: BEGIN ; 254 = database names & table names
              idl_types(i)= "String"
            END
    ENDCASE
  ENDFOR

  IF (num_rows EQ 0) THEN BEGIN   
    info= [field_names,idl_types] ; table columns and their types are returned.
  ENDIF ELSE BEGIN
    info= STRARR(num_rows)
    rows=0
    WHILE (NOT EOF(unit)) DO BEGIN ; database names or table names in a database are returned.
      READF,unit,tmp ; read row of data
      params= STRTRIM(STR_SEP(tmp,'|'),2)
      IF (N_ELEMENTS(params) EQ n_fields) THEN BEGIN
        info(rows)= params(0)  ; each row has only one field (database name or table name)
        rows= rows+1
      ENDIF
    ENDWHILE
  ENDELSE

  FREE_LUN,unit
  CLOSE,unit

  RETURN,info

END

