function read_scc_pwr,sc,d1,d2,preflight=preflight,pb=pb,mon=mon
;+
; $Id: read_scc_pwr.pro,v 1.3 2007/06/22 18:21:22 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : read_scc_pwr.pro
;               
; Purpose   : read SC POWER sequenial print file from ITOS
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    : 	sc = spacecraft 'a' or 'b', d1='yyyy-mm-dd', d2 (optional) 'yyyy-mm-dd'
;
; Outputs   :  report of images with sequence errors and missing packets image_pckt_errors_X_YYYY_DDD.rpt

; Optional Outputs: 
;
; Keywords  : preflight to use preflight data
;   	      mon for monitor directory 
;   	      pb for playback directory	
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : database SC apid's
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Apr 06

if(strlowcase(sc) eq 'b')then sc='secchib'
if(strlowcase(sc) eq 'a')then sc='secchia'
if keyword_set(pb)then rtpb='/pb/' else if keyword_set(mon)then rtpb='/monitor/' else rtpb='/'
print,rtpb
doy1=utc2doy(anytim2utc(d1))
if(strmid(d1,0,2) eq 20)then yr1=strmid(d1,0,4) else yr1='20'+strmid(d1,0,2)
doys=[doy1] & yrs=[yr1]

if(datatype(d2) ne 'UND')then begin
  doy2=utc2doy(anytim2utc(d2))
  if(strmid(d2,0,2) eq 20)then yr2=strmid(d2,0,4) else yr2='20'+strmid(d2,0,2)
  if(yr2 eq yr1)then doys=indgen(doy2-doy1+1)+doy1
  if(yr2 gt yr1)then begin
    doyyrend=utc2doy(anytim2utc(yr1+'-12-31'))
    doyt= indgen(doyyrend-doy1+1)+doy1  & yrs1=intarr(n_elements(doyt))+yr1
    doyt2=  indgen(doy2)+1  & yrs2=intarr(n_elements(doyt2))+yr2
    doys=[doyt,doyt2] & yrs=[yrs1,yrs2]
  endif else begin
    doys=indgen(doy2-doy1+1)+doy1
    yrs=intarr(n_elements(doys))+yr1
  endelse
endif

hkfiles=strarr(n_Elements(doys))
print,doys
for n=0,n_elements(doys)-1 do begin
  fname='stereopwr*'+strmid(string(yrs(n),'(i4.4)'),2,4)+string(doys(n),'(i3.3)')+'*.txt'
  IF keyword_set(preflight) THEN $
    fdir=getenv('scc'+strlowcase(ab))+'/preflight/outputs/prints/' else $
    fdir=getenv('scc'+strlowcase(ab))+'/ln*/'+rtpb+'/outputs/prints/'
  cschkfiles=findfile(fdir+fname)
  if(n eq 0)then hkfiles=cschkfiles else hkfiles=[hkfiles,cschkfiles]
endfor

hkfiles=hkfiles(where(hkfiles ne ''))

if (hkfiles(0) ne '')then begin
  line='' & d=long(0) & d2=0
  for n=0,n_Elements(hkfiles)-1 do begin
    openr,1,hkfiles(n)
    print,hkfiles(n)
    while (not eof(1)) do begin
      readf,1,line
      if(strpos(line,'APID') ne 0)then begin
        if(d2 eq 1)then begin
          schkdata=strsplit(line,',',/extract)
          sepwr(d,0:n_Elements(labels)-2)=schkdata
          sepwr(d,n_Elements(labels)-1)=(n*24*60l*60l)+(strmid(sepwr(d,0),9,2)*60l*60l)+(strmid(sepwr(d,0),12,2)*60l)+strmid(sepwr(d,0),15,2)
          d=d+1
        endif
      endif
      if(strpos(line,'APID') eq 0)then begin
        if(n eq 0 and d2 ne 1)then begin 
          d2=1 
          labels=strsplit(line,',',/extract)
          labels=[[labels],'seconds']
          labels=strtrim(labels)
          sepwr=strarr((60l*60l*24l*n_Elements(doys)),n_Elements(labels))
        endif     
      endif
    endwhile
    close,1      
  endfor   

  z=where(sepwr(*,0) ne '')
  sepwr=sepwr(z,*)
  t=n_Elements(sepwr(*,0))-1
  z=sort(sepwr(*,n_Elements(labels)-1))
  sepwr=sepwr(z,*)
  sta=create_struct('Spacecraft',sc)
  for ns=0,n_Elements(labels)-1 do begin
    vals=strtrim(sepwr(*,ns))
    if(strpos(strlowcase(labels(ns)),'cur') ne -1 or strpos(strlowcase(labels(ns)),'volt') ne -1)then vals=float(vals)
    if(labels(ns) eq 'seconds')then vals=long(vals)
    st1=create_struct(sta,labels(ns),vals)
    sta=st1
   endfor
endif else begin
sepwr=''
endelse

return,sta
   
end

pro plot_scc_dec_power,sepwr,rng=rng
;keywords rng when set it will allow you to select the x range using the cursor on the current plot

clr=['FFFFFF'xl,'000000'xl,'FF0000'xl,'00FF00'xl]
zcnt=where(sepwr.seconds ge 0)
cnt=n_Elements(zcnt) & xtval=indgen(7)*fix(cnt/6)
if keyword_set(rng) then begin
  plot,sepwr.seconds,sepwr.DECCUR01C,ytitle='AMPS',title=strupcase(sepwr.spacecraft)+' DECON POWER '+sepwr.APID00BTIME(0)+' - '+sepwr.APID00BTIME(cnt-1) ,$
    background=clr(0),color=clr(1),charsize=1.5,font=0,$
    xtickname=strmid(sepwr.APID00BTIME(xtval),9,8),xtickv=sepwr.seconds(xtval),xticks=n_Elements(xtval)-1
  print,'use cursor to select first X axis value'
  cursor,x1,y1
  wait,1
  print,'use cursor to select second X axis value'
  cursor,x2,y2
  xrng=[long(round(x1)),long(round(x2))]
  zcnt=where(sepwr.seconds ge xrng(0) and sepwr.seconds le xrng(1))
  cnt=n_Elements(zcnt) & xtval=(indgen(7)*fix(cnt/6)) +zcnt(0)
endif else xrng=[min(sepwr.seconds),max(sepwr.seconds)]

!p.multi=[0,0,3,0,0]

if (strlowcase(sepwr.spacecraft) eq 'secchia')then nhtrs=2 else nhtrs=3

plot,sepwr.seconds,sepwr.DECVOLT01D,ytitle='VOLTS',title=strupcase(sepwr.spacecraft)+' DECON POWER '+sepwr.APID00BTIME(zcnt(0))+' - '+sepwr.APID00BTIME(zcnt(cnt-1)) ,$
    background=clr(0),color=clr(1),charsize=1.5,font=0,$
    xtickname=strmid(sepwr.APID00BTIME(xtval),9,8),xtickv=sepwr.seconds(xtval),xticks=n_Elements(xtval)-1,xrange=xrng,xstyle=1

plot,sepwr.seconds,sepwr.DECCUR01C,ytitle='AMPS' ,background=clr(0),color=clr(1),charsize=1.5,font=0,$
    xtickname=strmid(sepwr.APID00BTIME(xtval),9,8),xtickv=sepwr.seconds(xtval),xticks=n_Elements(xtval)-1,xrange=xrng,xstyle=1

plot,sepwr.seconds,sepwr.DECCUR01C - (sepwr.DECVOLT01D*(nhtrs)/110) - (sepwr.DECVOLT01D*(2)/135) ,background=clr(0),color=clr(1),charsize=1.5,font=0,$
    xtickname=strmid(sepwr.APID00BTIME(xtval),9,8),xtickv=sepwr.seconds(xtval),xticks=n_Elements(xtval)-1,xrange=xrng,xstyle=1

!p.multi=0

end

