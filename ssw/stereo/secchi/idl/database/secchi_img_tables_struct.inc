;
;<html><head>SECCHI Image Database Definition</head><body>
;<h2>SECCHI Image Database Definitions</h2>
;$Id: secchi_img_tables_struct.inc,v 1.2 2006/11/06 14:37:44 nathan Exp $
;
;$Log: secchi_img_tables_struct.inc,v $
;Revision 1.2  2006/11/06 14:37:44  nathan
;update db_name for flight
;
;Revision 1.1  2006/09/11 19:03:56  nathan
;define image database table structures
;
;Log: secchi_img_tables_def.html,v 
;Revision 1.5  2006/08/30 21:33:11  nathan
;added ; for all comments
;
;Revision 1.4  2006/07/19 19:17:09  esfand
;made more changes - aee
;
;Revision 1.3  2006/07/19 18:28:12  nathan
;made some values
;
;<h3>For databases SECCHI and SECCHI_FLIGHT</h3>
;<pre>
;TABLE_DESC   tinyint	= 1 byte
;TABLE_DESC   smallint	= 2 byte
;TABLE_DESC   mediumint	= 3 byte
;TABLE_DESC   int	= 4 byte
;TABLE_DESC   bigint	= 8 byte
;TABLE_DESC   char: fixed-length string
;TABLE_DESC   varchar: variable length (up to max) string
;TABLE_DESC   datetime: UTC time string

tinyintu=0UB
smallintu=0US
mediumintu=0UL
intu=0UL

tinyint=0B
smallint=0
mediumint=0L
int=0L
flt=0.0
char=''
varchar=''
datetime=''
;TABLE_DESC   Indexing fields are for most commonly searched values;
;TABLE_DESC   noted with TABLE_INDX.

;TABLE_NAME   IMG_SEB_HDR
img_seb_hdr = { img_seb_hdr, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_seb_hdr", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   Image headers produced by the SECCHI FSW; Where applicable, values are as found in the raw science header.
;TABLE_DESC   For field names, preference is given to SECCHI FITS Header tag names, then FSW header field names. 
;TABLE_DESC   FSW header structure field names are noted in parenthesis as first item in description. If not the database
;TABLE_DESC   field name, $ FITS header tag name is noted in parenthesis after item description. 
  OBSRVTRY   :tinyint, $        ;(platformid) 1=STEREO-A, 2=STEREO-B 
  SYNC       :tinyint, $        ;(sync) 0=Not Synced, 1=SC-AB Synced 
  OBS_ID    :smallintu, $       ;(osnumber) Observing Sequence number 
  SEB_PROG   :tinyint, $        ;(imagetype) SEB program: 0=Normal, 1=Dark, 2=Double,
                                        ;3=LED, 4=Continuous, 5=Series, 6=Normal 
  OBS_PROG   :varchar, $	;Proc name or JOP ID  
  OBSSETID   :smallint, $       ;(campaignset) Set/Campaign ID number 
  LEDCOLOR   :tinyint, $        ;(cmdledmode) For calibration LED images:   
                                        ;LEDCOLOR =   0                      1           2     
                                        ;EUVI:	FPA-Blue                Tel-Purple   Tel-Blue
                                        ;COR2:	Tel-Red (door-close)    FPA-Violet   FPA-Blue
                                        ;Others:FPA-Red	                FPA-Violet   FPA-Blue
                                        ;For non-calibration LED images: LEDCOLOR = 0 (None)
  FILEORIG   :varchar, $	;(filename) YMDDaaaa.APT 
  FILENAME  :char, $       ;yyyymmdd_hhmmss_LATTS.fts  (FSW n/a) where
                                        ;yyyymmdd_hhmmss: DATE_OBS
                                        ;L:     Image type:
                                        ;         n = Normal Image
                                        ;         m = Multiple Exposures Combined Onboard
                                        ;         d = Double Image
                                        ;         k = Dark Image
                                        ;         e = LED Image
                                        ;         c = Continuous Image
                                        ;         s = Sequence Image
                                        ;         1 = Total Brightness (Level-1 Image)
                                        ;A:     Destination (3=RT, 4=SSR1, 5=SSR2, 7=SPW)
                                        ;TT:    Telescope (eu,c1,c2,h1,h2,gt)
                                        ;S:     A or B, S/C C=other, N=non-flight
  THUMB_FN   :varchar, $	;Thumbnail image filename yyyymmdd_hhmmss_LATTS.jpg 
  SYNCED_FN  :varchar, $	;Null or filename if sync is set (FSW n/a)
  SPWX       :tinyint, $        ;1 if image was also sent down SPWX channel, else 0
  DATE_MOD   :datetime, $       ;Date/time (UTC) processed  (FSW n/a)
  DATE_OBS   :datetime, $       ;(actualexptime) Date/time (UTC) command sent to expose shutter 
  DATE_AVG   :datetime, $       ;Mid point of exposure(s) Date/time (UTC) of single or summed images
  DATE_END   :datetime, $       ;Date/time (UTC) End of the (last) exposure for current image
  DETECTOR   :tinyint, $        ;(telescopeid) Telescope number: 3=EUVI,2=COR1,1=COR2,5=HI1,4=HI2 
  FILTER     :254UB, $ ;(cmdfilterposition) 0-179 EUVI filter wheel commanded step number mapped to 4
                                        ;        positions:   0-44        45-89       90-134      135-179
                                        ;                  S1 (Clear)  DBL (Al+1a)  S2 (Al+1b)  OPEN (Al+2)
                                        ;        (Other four cameras are filterless.) 
  POLAR_QUAD :254UB, $ ;COR1&2:(cmdpolarposition) 0-143 Polarizer wheel commanded positions mapped 
                                        ;        to (x - fwz)*2.5 degrees, $ where fwz=0 except COR1A fwz=4 (FITS: POLAR)
                                        ;        EUVI:(cmdquadposition) 0-23 quadrant selector mapped to 4 sectors (FITS: SECTOR)
                                        ;                 2       8       14      20   (assuming CW motion)
                                        ;                304A    195A    284A    175A
                                        ;        (HI1&2 have no polarizer)
  POLAR2     :254UB, $ ;(cmdpolarposition_2) Commanded polarizer position for 2nd of double exposure images 
                                        ;        (COR only; FITS POLAR in secondary header)
  EXPTIME   :-1., $  	;(actualexpduration) Exposure duration (seconds) 
  EXPTIME2  :flt, $  	;(actualexpduration_2) Exposure duration used for 2nd of double exposure images 
  BIASMEAN  :-1., $  	;(meanbias) Bias calculated as average of 15th column of readout 
  BIASSDEV  :-1., $  	;(stddevbias) Standard deviation of 15th column 
  IMGSEQ     :254UB, $ ;(imgseq) Image sequence counter for SCIP/HI sequences  
  IMGCTR     :smallintu, $ ;Image counter (reset every midnight)
  DATE_CMD   :datetime, $       ;(cmdexptime) Date_obs - travel_time - same as filename
  P1ROW      :smallint, $       ;(p1row) Readout coordinate P1 row: 1-2176 
  P1COL      :smallint, $       ;(p1col) Readout coordinate P1 col: 1-2176 
  P2ROW      :smallint, $       ;(p2row) Readout coordinate P2 row: 1-2176 
  P2COL      :smallint, $       ;(p2col) Readout coordinate P2 col: 1-2176 
  SUMROW     :tinyint, $        ;(sumrow) On-chip x summing value (FITS SUMMED, CCDSUM, IPSUM) 
  SUMCOL     :tinyint, $        ;(sumcol) On chip y summing value (FITS SUMMED, CCDSUM, IPSUM) 
  SEBXSUM    :tinyint, $        ;(sebxsum) Image processing x summing value (FITS SUMMED, CCDSUM, IPSUM) 
  SEBYSUM    :tinyint, $        ;(sebysum) Image processing y summing value (FITS SUMMED, CCDSUM, IPSUM) 
  NAXIS1     :smallint, $       ;Number of columns in image: 0-2176 (0=header only)(FSW n/a)
  NAXIS2     :smallint, $       ;Number of rows in image: 0-2176    (0=header only) (FSW n/a)
  DOORSTAT   :-1B, $        ;(actualdoorposition) Door position: 2=open, 0=closed, 1=in_transit, 3=off, otherwise error 
  DATALEVEL  :tinyint, $        ;0=Level-0.5, 1=Level-1, 2= level-2, 3=level-3, ...., (FSW n/a)
  DOWNLINK   :tinyint, $        ;(11th:char of filename) 4=SSR1, 5=SSR2, 3=RealTime, 7=SpaceWeather, 1=GroundTest 
  IP_00_19   :varchar, $    ;(ipcmdlog) description of image processing steps, three numeral:chars per step
  DISKPATH   :varchar, $    ;Full disk path of file (FSW n/a)
  SYNCPATH   :varchar, $    ;Full disk path of synced file, if any, or null (FSW n/a)
  EVENT      :smallintu, $       ;(critevent) Image block number of centroid of a detected event (<0 means none) 
  COSMICS   :int $       	;Number of pixels removed by cosmic ray scrub; If NAXIS1>0, then it is the mean. 
}

;TABLE_NAME   IMG_SEB_HDR_EXT
img_seb_hdr_ext = { img_seb_hdr_ext, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_seb_hdr_ext", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   Extended portion of image headers; header values not included in IMG_SEB_HDR table
;TABLE_DESC   
  FILEORIG   :varchar, $    ;(filename) YMDDaaaa.APT 
  DATE_MOD   :datetime, $       ;Date/time (UTC) processed  (FSW n/a) 
  DATE_OBS   :datetime, $       ;(actualexptime) Date/time (UTC) command sent to expose shutter 
  EXPCMD     :-1., $          ;(cmdexpduration) Exposure duration commanded (sec) 
  EXPCMD2    :flt, $          ;(cmdexpduration2) Exposure duration commanded (sec) (FITS EXPCMD in secondary header)
  OFFSET     :9999, $       ;(offset) Commanded Offset value used in camera 
  GAINCMD    :-1, $	;(gain) Commanded Gain value used in camera 
  GAINMODE   :tinyint, $        ;(gainmode) FPGA Mode (1=LOW or 0=HIGH) 
  ENCODERP   :254UB, $        ;(actualpolarposition) actual encoder value of polarizer 
  ENCODERP2  :254UB, $        ;(actualpolarposition2) actual encoder value of polarizer for second exposure  (FITS ENCODERP in secondary header)
  SHUTTDIR   :-1B, $        ;(actualshutterdirection) CCW, CW, or NONE means no shutter used 
  READFILE   :varchar, $	;The name of file of readout tables used onboard (from pipeline), version number appended
  WAVEFILE   :varchar, $	;The name file of waveforms used onboard (from pipeline), version number appended
  READ_TBL   :-1B, $        ;(readouttableid) [0-7] which table in WGA_FILE used for readout
  CLR_TBL    :-1B, $        ;(clrtableid) [0-7] which table in WGA_FILE used for clear
  DATE_CLR   :datetime, $       ;(actualccdclearstarttime) Date/time of start of clear table 
  DATE_RO    :datetime, $       ;(actualimageretrievestarttime) Date/time of start of image retrieval 
  READTIME   :-1., $          ;(actualreadouttime) Duration of readout (sec) 
  IP_TIME    :-1, $       ;(ipprocessingtime) duration of onboard processing time (sec) 
  COMPRSSN   :tinyint, $        ;(ipcmdlog) code indicating algorithm used for compression
  COMPFACT  :flt, $          ; compression factor achieved  (FSW n/a)
  NMISSING   :-1, $       ; number of blocks missing  (FSW n/a)
  OBJECT     :varchar, $    ; What is supposed to be in image  (FSW n/a)
  CROTA     :9999., $          ;Rotation angle of solar north in image (degrees) (FSW n/a)
  DSUN_OBS  :flt, $          ;Distance to the sun (meters) (FSW n/a)
  LEDPULSE  :intu, $	;(cmdledpulses) Number of LED pulses 			
  VERSION    :254UB, $       ; Identifier of FSW header version
  CEB_STAT   :254UB, $       ;(cebintfstatus) CEB-Link-status (enum CAMERA_INTERFACE_STATUS)
  CAM_STAT   :254UB, $       ;(ccdintfstatus) CCD-Interface-status (enum CAMERAPROGRAM_STATE)
  DATAAVG   :flt, $			;Average value of image pixels
  DATASIG   :flt, $			;Standard deviation of average over image
  LIGHTTRAVELOFFSETTIME     :int, $		;light travel time offset in ms as set by IS
  IMGBUFFEROFFSET            :tinyint, $	;SDRAM image buffer slot number for image capture [0-26] 
  PREEXPSCSTATUS             :-1, $	;Spacecraft Status byte before exposure
  POSTEXPSCSTATUS            :-1, $	;Spacecraft Status byte after exposure
  ACTUALOPENTIME            :int, $	;Raw reading from MEB of actual shutter open time (when the opening edge ofthe blade crosses the center of the CCD) source: mechanism readback
  ACTUALCLOSETIME           :int, $	;Raw reading from MEB of actual shutter close time (when the closing edge of theblade crosses the center of the CCD) source: mechanism readback
  ACTUALOPENTIME_2          :int, $	
  ACTUALCLOSETIME_2         :int, $	
  ACTUALSHUTTERPOSITION      :tinyint, $	;State of shutter during exposure open=1 closed=0
  ACTUALPOLARDIRECTION       :tinyint, $	; polarizer or quadrant selector CW or CCW
  DATASATPERC               :flt $		;    Percent of image with saturated pixels
}


;TABLE_NAME   IMG_CAL_BIAS
img_cal_bias = { img_cal_bias, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_cal_bias", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   Image calibration bias
  FILEORIG   :varchar, $	;YMDDaaaa.APT (filename)
  DATE_MOD   :datetime, $       ;Date/time (UTC) processed  (FSW n/a)
  DATE_OBS   :datetime, $       ;Date/time (UTC) command sent to expose shutter (actualexptime)
  BIAS      :flt, $          ;Bias (under/over scan) computed from dark
  BIASSTD   :flt, $          ;Standard deviation
  METHOD     :tinyint $        ;Method of calculating bias (0-3):
                                        ;   	0=underscan,    1=overscan, 
                                        ;   	2=second dark,  3=backward readout
}

;TABLE_NAME   IMG_CAL_LED
img_cal_led = { img_cal_led, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_cal_led", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   Image calibration LED
  FILEORIG   :varchar, $	;YMDDaaaa.APT (filename)
  DATE_MOD   :datetime, $       ;Date/time (UTC) processed  (FSW n/a)
  DATE_OBS   :datetime, $       ;Date/time (UTC) command sent to expose shutter (actualexptime)
  NUM_DARK_PIX:intu, $ ;Number of dark pixels (FSW n/a)
  LEDCOLOR   :tinyint, $        ;For calibration LED images (cmdledmode):   
					;	LEDCOLOR =   1          3           2     
					;	EUVI:	FPA-Blue   Tel-Blue   Tel-Purple 
					;	COR2:	Tel-Red    FPA-Blue   FPA-Violet
					;	Others:	FPA-Red	   FPA-Blue   FPA-Violet
					;	For non-calibration LED images: LEDCOLOR = 0 (None)
  LEDPULSE  :intu $	;Number of LED pulses (cmdledpulses)
}

;TABLE_NAME   IMG_CAL_DARK
img_cal_dark = { img_cal_dark, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_cal_dark", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   Image calibration dark
  FILEORIG   :varchar, $	;YMDDaaaa.APT (filename)
  DATE_MOD   :datetime, $	;Date/time (UTC) processed  (FSW n/a)
  DATE_OBS   :datetime, $	;Date/time (UTC) command sent to expose shutter (actualexptime)
  EXPTIME   :flt, $		;Exposure duration (seconds) (actualexpduration)
  DARK_CURRENT  :flt, $	;Dark current (DN/sec) (FSW n/a)
  NUM_HOT_PIX   :int $       	;Number of hot pixels (FSW n/a)
}

;TABLE_NAME   IMG_HIST_CMNT
img_hist_cmnt = { img_hist_cmnt, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_hist_cmnt", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   Image Comment/History Information 
  FILENAME  :char, $       ;yyyymmdd_hhmmss_LATTS.fts  (FSW n/a) where
  DATE_MOD   :datetime, $	;Date/time (UTC) processed
  DATE_OBS   :datetime, $	;Date/time (UTC/ECS) command sent to expose shutter
  FLAG       :varchar, $	;'comment' = entry is a comment
                                	;'history' = entry is a history
  INFO       :varchar $	;comment/history text
}

;TABLE_NAME   IMG_EUVI_GT
img_euvi_gt = { img_euvi_gt, $ 
  db_name   :"secchi_flight", $    	;Database Name
  table_name:"img_euvi_gt", $	;Name of this Table
;TABLE_AUTH   Author: esfand 
;TABLE_DATE   Date: 2006/07/19 19:17:09  
;TABLE_VERS   Revision: 1.4  
;TABLE_DESC   EUVI GT data (extended header)
  FILEORIG   :varchar, $    ;(filename) YMDDaaaa.APT 
  DATE_MOD   :datetime, $       ;Date/time (UTC) processed  (FSW n/a) 
  DATE_OBS   :datetime, $       ;(actualexptime) Date/time (UTC) command sent to expose shutter 
  FPS_CMD    :tinyint, $        ; (useFPS)
  FPS_ON     :tinyint, $        ; (actualFPSmode)
  ENCODERF   :254UB, $	; actual encoder value of filter wheel (actualfilterposition)
  ENCODERQ   :254UB, $	;Quadrant selector encoder value (actualpolarposition)
  ACTUALFILTERDIRECTION :tinyint, $
  ACTUALSCFINEPOINTMODE :tinyint, $
  FPSOFFY    :smallint, $ 	;YOFFSET
  FPSOFFZ    :smallint, $ 	;ZOFFSET
  SPARE1    :smallintu, $
  FPSNUMS   :99999L, $ 	;NUMFPSSAMPLES
  FPSGTSY   :int, $ 	;FPSYSUM
  FPSGTSZ   :int, $ 	;FPSZSUM
  FPSGTQY   :int, $ 	;FPSYSQUARE
  FPSGTQZ   :int, $ 	;FPSZSQUARE
  FPSERS1   :int, $ 	;PZTERRSUM1
  FPSERS2   :int, $ 	;PZTERRSUM2
  FPSERS3   :int, $ 	;PZTERRSUM3
  FPSERQ1   :int, $ 	;PZTERRSQUARE1
  FPSERQ2   :int, $ 	;PZTERRSQUARE2
  FPSERQ3   :int, $ 	;PZTERRSQUARE3
  FPSDAS1   :int, $ 	;PZTDACSUM1
  FPSDAS2   :int, $ 	;PZTDACSUM2
  FPSDAS3   :int, $ 	;PZTDACSUM3
  FPSDAQ1   :int, $ 	;PZTDACSQUARE1
  FPSDAQ2   :int, $ 	;PZTDACSQUARE2
  FPSDAQ3   :int, $ 	;PZTDACSQUARE3
  SPARE2            :smallintu, $ 
  PZT1YCOORDXFORM    :smallint, $ 
  PZT2YCOORDXFORM    :smallint, $ 
  PZT3YCOORDXFORM    :smallint, $ 
  PZT1ZCOORDXFORM    :smallint, $ 
  PZT2ZCOORDXFORM    :smallint, $
  PZT3ZCOORDXFORM    :smallint, $
  PZT1VOLTPREDICT0   :smallint, $
  PZT1VOLTPREDICT1   :smallint, $
  PZT1VOLTPREDICT2   :smallint, $
  PZT1VOLTPREDICT3   :smallint, $
  PZT1VOLTPREDICT4   :smallint, $
  PZT2VOLTPREDICT0   :smallint, $
  PZT2VOLTPREDICT1   :smallint, $
  PZT2VOLTPREDICT2   :smallint, $
  PZT2VOLTPREDICT3   :smallint, $
  PZT2VOLTPREDICT4   :smallint, $
  PZT3VOLTPREDICT0   :smallint, $
  PZT3VOLTPREDICT1   :smallint, $
  PZT3VOLTPREDICT2   :smallint, $
  PZT3VOLTPREDICT3   :smallint, $
  PZT3VOLTPREDICT4   :smallint, $
  PZT1PIDCOE0        :smallint, $
  PZT1PIDCOE1        :smallint, $
  PZT1PIDCOE2        :smallint, $
  PZT1PIDCOE3        :smallint, $
  PZT2PIDCOE0        :smallint, $
  PZT2PIDCOE1        :smallint, $
  PZT2PIDCOE2        :smallint, $
  PZT2PIDCOE3        :smallint, $
  PZT3PIDCOE0        :smallint, $
  PZT3PIDCOE1        :smallint, $
  PZT3PIDCOE2        :smallint, $
  PZT3PIDCOE3        :smallint $
}
;</pre></body></html>
