function gethkevents,sc,d1,d2, CODE=code, DAT1=dat1, SSYS=ssys, STR=str, SRCFILE=srcfile
; $Id: gethkevents.pro,v 1.5 2007/05/22 18:53:07 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : gethkevents
;               
; Purpose     : get event message info from SQL database
;               
; Explanation : 
;               
; Use         : IDL> ev=gethkevents('a','2006-11-01 12:34:12','2006-11-05 23:12:09',code=23)
;    
; Inputs      :   sc = A or B 
;   	     	  d1 = date/time in UTC string or struct format (time optional)
;   	    	  d2 = date/time in UTC string or struct format (time optional)
;               
; Outputs     :   ev = structure of packettime (string & unix), and HKEVENT mnemonics 
;		(see $SSW_SECCHI/data/secchi_hk_scevent_table_def.html)
;
; Keywords :	Allow specification of HKEVENT keyword values in database call
;               
; Calls from LASCO : none
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Database 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Dec 2006
;               
; Modified    :
;
; $Log: gethkevents.pro,v $
; Revision 1.5  2007/05/22 18:53:07  nathan
; allow one date input
;
; Revision 1.4  2007/03/16 17:45:57  nathan
; correct comments
;
; Revision 1.3  2006/12/16 00:32:00  nathan
; check for ssys=99
;
; Revision 1.2  2006/12/14 15:45:00  nathan
; not ready yet
;
; Revision 1.1  2006/12/08 21:31:55  nathan
; for getting event message info from SQL db
;
; Log: get_hbtemps.pro,v 
; Revision 1.3  2006/10/31 17:53:10  mcnutt
; updated to use flight database

tbl='hk_events_'+strlowcase(sc) 
dtb='secchi_flight_hk_'+strupcase(sc)
;resol=' group by mid('+hk_temps+'.pckt_time,1,16)'
if (keyword_set(res)) then begin
	 if (Res eq 60) then resol=' group by mid('+hk_temps+'.pckt_time,1,13)'
	 if (Res eq 30) then resol=' and (CAST('+hk_temps+'.pckt_time as CHAR) like "%:00:0%" OR CAST('+hk_temps+'.pckt_time as CHAR) like "%:30:0%") group by mid('+hk_temps+'.pckt_time,1,16)'
	 if (Res eq 10) then resol=' and CAST('+hk_temps+'.pckt_time as CHAR) like "%:%0:0%" group by mid('+hk_temps+'.pckt_time,1,16)'
	 if (Res eq 0) then resol=''
endif

d1=utc2str(anytim2utc(d1),/ecs)
IF n_params() GT 2 THEN d2=utc2str(anytim2utc(d2),/ecs) ELSE BEGIN
    utc1=anytim2utc(d1)
    utc2=utc1
    utc2.mjd=utc1.mjd+1
    d2=utc2str(utc2,/ecs)
ENDELSE

datvars='HKEVENTDAT1, HKEVENTDAT2, HKEVENTDAT3, HKEVENTDAT4, HKEVENTSEV, '
IF keyword_set(CODE) THEN IF code EQ 420 THEN $
datvars='HKEVENTDAT1, HKEVENTDAT2, '
IF keyword_set(SSYS) THEN IF ssys EQ 99 THEN $
datvars='HKEVENTDAT1, HKEVENTDAT2, '

cmnd='select pckt_time, unix_time, HKEVENTCODE, '+datvars+'HKEVENTSSYS, HKEVENTSTR, sourcefileid from '+tbl+' where pckt_time between "'+d1+'" and "'+d2+'"'

IF keyword_set(CODE) THEN cmnd=cmnd+' and HKEVENTCODE='+string(code)
IF keyword_set(DAT1) THEN cmnd=cmnd+' and HKEVENTDAT1='+string(dat1)
IF keyword_set(SSYS) THEN cmnd=cmnd+' and HKEVENTSSYS='+string(ssys)
IF keyword_set(STR)  THEN cmnd=cmnd+' and HKEVENTSTR="'+string(str)+'"'
IF keyword_set(SRCFILE) THEN cmnd=cmnd+' and sourcefileid="'+string(srcfile)+'"'

print,cmnd
outp=SQDB (dtb,cmnd)


return, outp

end
