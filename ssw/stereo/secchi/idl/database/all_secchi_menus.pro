;+
;$Id: all_secchi_menus.pro,v 1.10 2011/01/07 19:01:32 nathan Exp $
;
; Project     : SECCHI
;
; Name        : ALL_SECCHI_MENUS 
;
; Purpose     : select a database and form a query 
;
; Category    : Widget
;
; Explanation : Used by sqdb to let user select database, table(s),
;               and form a query.
;
; Use         : IDL>all_secchi_menu,dbase,cmnd
;
; Examples    : IDL>all_secchi_menu,dbase,cmnd
;
; Inputs      : None
;
; Opt. Inputs : None
;
; Outputs     : dbase: selected database name
;               cmnd:  query to select data
;
; Opt. Outputs: None
;
; Keywords    : None
;
; History     : Ed Esfandiari November 2005 - First Version.
;               Ed Esfandiari jan 2006    - corrected bug.
;               Ed Esfandiari Mar 30, 2007 - Correct table joints for secchi_fight tables.
;               Ed Esfandiari Oct 17, 2007 - Correct table joints for img_hist_cmnt and img_seb_hdr
;               Ed Esfandiari Dec 04, 2007 - Modified help pages. Also added ip_00_19 pattern matching,
;                                            and usage help.
;               Ed Esfandiari Feb 25, 2008 - Updated Help menu.
;               Ed Esfandiari Oct 10, 2008 - Use correct Field Help for LASCO and SECCHI tables.
;               Ed Esfandiari Oct 15, 2008 - Remove un-related database names for display menu.
;
;$Log: all_secchi_menus.pro,v $
;Revision 1.10  2011/01/07 19:01:32  nathan
;improve scroll bar, widget sizes
;
;Revision 1.9  2010/12/09 21:53:30  nathan
;car syn sort group; cols validity check; y_scroll_size=500 not 300
;
;Revision 1.8  2008/10/15 18:44:43  esfand
;only show related database names
;
;Revision 1.7  2008/10/10 19:53:37  esfand
;separate help for lasco and secchi tables
;
;Revision 1.6  2008/02/25 16:06:56  esfand
;updated Help menu
;
;Revision 1.5  2007/12/05 17:35:37  esfand
;corrected table select help
;
;Revision 1.4  2007/12/05 17:11:29  esfand
;added ip_00_19 help and search
;
;Revision 1.3  2007/10/17 18:35:58  esfand
;correctd img_hist_cmnt table joins
;
;Revision 1.2  2007/03/30 13:48:49  esfand
;Corrected table joint for secchi_flight tables - AEE
;
;Revision 1.1  2007/01/12 22:41:50  nathan
;moved from nrl_lib/dev/database
;
;Revision 1.2  2006/01/26 15:57:40  esfand
;minor changes in some routines
;
;
;
; This pro uses IDL widgets to interact with the user to select a database
; from the list of present databases and then to select fields from 
; one or more (join) tables from that database and to form a sybase
; query command.
; First, it calls db_help_func function to select a database using IDL widgets. 
; Then, it calls db_help_func to get table names in the database and using IDL
; widgets displays the table names in a menu. User selects table(s) and
; then db_help_func is called again to get the field names for the selected
; table(s). Fields of the selected table(s) are then displayed, one table
; at a time, by calls to get_qrycmnd; It allows the user to select desired 
; fields and set optional field conditions.  Get_qrycmnd returns a generated
; sybase query command based on the selected fields and conditions. If more
; than one table was selected, this pro uses all returned query commands to
; generate a single join query command. Connection between the tables are
; made using Key fields such as pck_time (hk tables), filename (image tables),
; and fileorig (image tables). 
; Join will take place even if only one of the tables is actually used and 
; the rest are discarded. To operate on only one table, select only one table
; from the menu. For joins, all selected fields and conditions in the query
; command are prefixed with the table names which they belong to (i.e. pck_time
; becomes hk_relays.pck_time).
;
;

@db_help_func.pro 
@form_qry.pro
 
pro scc_select_event,event
  common shared_var,tab_select
  widget_control,event.id,get_uvalue=uval
  widget_control,event.top,get_uvalue=tab_struct

   db_help=[$
   'To select a database simply click on the database name. To de-select',$
   'the same database click on it again. When a database is selected, the',$
   'small box next to the database name changes appearance or it is checked.',$
   'Click on the "Done" button to exit the menu. To progress forward, at',$
   'least one database must be selected.',$
   '',$
   'Note: Only one database at a time should be selected. Can not join',$
   '      tables across databases since packet times are different, etc..']

   tab_sel_help=[$
   'To select a table simply click on the table name. To de-select the',$
   'same table click on it again. When a table is selected, the small',$
   'box next to the table name changes appearance or it is checked.',$
   'Click on the "Done" button to exit the menu. To progress forward, at',$
   'least one table must be selected.',$
   '',$
   'Note:',$
   '   Only tables from same APID (i.e. tablenames for which first 3',$
   '   characters are the same such as hk_* tables) may be joined.']

    case (uval) of 
        'Done' : BEGIN  ;** exit program and continue
                WIDGET_CONTROL, /DESTROY, tab_struct.base
                GOTO, done
            END
        'quit' : BEGIN  ;** exit program
                tind= WHERE(tab_select EQ 1, tcnt)
                if (tcnt GT 0) THEN tab_select(tind)= 0 
                WIDGET_CONTROL, /DESTROY, tab_struct.base
            END
        'Help': BEGIN     ; need help
           xdisplayfile,TITLE='Help',$
           GROUP=event.top, HEIGHT=12, WIDTH=75, $
           ;TEXT= ip_help
           TEXT= tab_sel_help
         END
        'DB_Help': BEGIN 
           xdisplayfile,TITLE='Help',$
           GROUP=event.top, HEIGHT=12, WIDTH=75, $
           TEXT= db_help
         END
        'SELECT' : BEGIN
                ind = WHERE(tab_struct.button_id EQ event.id)
                IF (event.select) THEN $
                   tab_select(ind)= 1 $
                ELSE $
                   tab_select(ind)= 0

                ; For table selection (not databases), only show/activate those 
                ; house-keeping tables that are related and can be joined:

                IF (MAX(STRPOS(tab_struct.fields,'hk_temp')) GE 0) THEN BEGIN
                  ; If a hk table is selected, only show other tables of same type (for joins):
                  othr= where(strpos(tab_struct.fields,strmid(tab_struct.fields(ind),0,3)) NE 0, nothr)
                  FOR i=0, nothr-1 DO widget_control,tab_struct.button_id(othr(i)),sensitive=0
                  ; If no table is selected, show all:
                  n= N_ELEMENTS(tab_struct.fields)
                  mx= MAX(tab_select(0:n-1))
                  IF (mx EQ 0) THEN BEGIN
                    FOR i=0, n-1 DO widget_control,tab_struct.button_id(i),sensitive=1 
                  ENDIF
                ENDIF
            END
   ENDCASE

done:

end


PRO SQL_QRY_EVENT, event
common shared_field,qry,field_struct,tablename,dbase_used
common shared_res, res_tble, is_hk, res_cond

WIDGET_CONTROL, event.id, GET_UVALUE=uval
WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE

help_info=[$
   'To select a field from a table simply click on the field name. To',$
   'deselect the same field click on it again. When a field is selected,',$
   'the small box next to the field name changes appearance. The empty',$
   'box to the right hand side of each field name is provided for placing',$
   'a condition on the query based on the values of that field. Data type',$
   'of each field is displayed on the right hand side.  Selecting',$
   'field(s) without entring any conditions causes the return of data',$
   'for ALL the rows in the table for the selected field(s); however,',$
   'if condition(s) are placed, only a subset of rows that satisfy the',$
   'condition(s) are returned.',$
   '',$
   'Character and DATETIME data types must be entered with single or',$
   'double quotation marks around them. Data of type binary is specified',$
   'with the characters 0 to 9 and A to F, and must be preceded with 0x',$
   'when it is input. Other data types such as integers and floats are',$
   'entered as normally.',$
   '',$
   'When it is desirable to have a condition that represents a range of',$
   'values such as between 10 and 20 or between "1995/12/05 10:00:00" and',$
   '"1995/12/12 22:10:15", use the ampersand (&) to separate the values',$
   '(i.e. 10&20 or "1995/12/05 10:00:00"&"1995/12/12 22:10:15").',$
   'Use =, <, >, !=, <=, and >= to represent equal, less than, greater',$
   'than, not equal, less than equal, and greater than equal, respectively.',$
   'Here are some examples (dashed lines represent the condition box):',$
   '',$
   ' +-------------------------------------------+',$
   ' | "1995/12/05 10:19"&"1995/12/25 15:11"   |',$
   ' +-------------------------------------------+',$
   '',$
   ' +-------+',$
   ' | 10&20 |',$
   ' +-------+',$
   '',$
   ' +------+',$
   ' | = 10 |',$
   ' +------+',$
   '',$
   ' +-----------------------+',$
   ' | < "1995/12/02 10:11"  |',$
   ' +-----------------------+',$
   '',$
   ' +--------------------------+',$
   ' | >= "1995/02/12 22:11:45" |',$
   ' +--------------------------+',$
   '',$
   ' +--------------------+',$
   ' | like "1995-10-31%" |    "like" is case and space sensitive',$
   ' +--------------------+',$
   '',$
   ' +--------------+',$
   ' | > 0x5C       |',$
   ' +--------------+',$
   '',$
   'Selecting and using more than one table causes a join to take place.',$
   'A table is ''used'' when at least a field is selected or a condition is',$
   'entered. A selected table that is not used is ignored. Tables involved',$
   'in a join may be selected from one or many databases.  Depending on',$
   'the selected tables, user may or may not have to provide a condition',$
   'for joining the tables. Joining image tables, for instance, does not',$
   'require user input; It is done by the software using filename and/or',$
   'fileorig fields. However, joining house keeping tables require a date/',$
   'time condition to be given using pck_time field of any of the selected',$
   'tables (i.e. ="Feb 4 1996 15:23:10"). User can include other conditions',$
   'in addition to the ones that joins different tables. Since two or more',$
   'selected fields in a join may have the same name, each selected field',$
   'is automatically prefixed with the database and table name containing',$
   'that field.']
   help_info= [help_info, $
   'Use IDL''s "HELP,/STRUCT,name" to view the field names in the structure',$
   'returned by this program.',$
   '',$
   'After field(s) are selected and optional condition(s) are entered,',$
   'click on the "Done" button to form the query and exit the menu.']

    CASE (uval) OF
        'Done': BEGIN     ; exit 
           WIDGET_CONTROL, /DESTROY, field_struct.base
           GOTO, done
         END
        'Help': BEGIN     ; need help
           xdisplayfile,TITLE='Help',$
           GROUP=event.top, HEIGHT=20, WIDTH=71, $
           TEXT= help_info
         END
        'IPHelp': BEGIN   
           ip_how=['Up to 20 Image Processing, IP, steps may be used for an image.',$
                   'Currently, 131 IP steps (0-130) are in use. The ip_00_19 field',$
                   'contains step numbers used to process an image separated by ";".',$
                   'For example, an image going to SSR1 using Rice compression',$
                   "is represented as ;41;7; string where IP_num 41 indicates SSR1",$
                   'and IP_num 7 means Rice Compression. A complete list of IP_num',$
                   'description is provided below.',$
                   'To search using a partial ip_00_19, one can use pattern matching.',$
                   'For example, to restrict a query result to images with Rice compression,',$
                   'use a like statement by entering ''like "%;7;%"'' (without the single-',$
                   'quotes) in the ip_00_19 condition box. Or, to look for Rice and ICER4',$
                   'images, enter ''like "%;7;%" or like "%;94;%"'' (again, without the',$
                   'single-quotes). Similarly, to locate Rice images going to SSR1, enter',$ 
                   '''like "%;7;%" and like "%;41;%"'' without the single-quotes.',$
                   ''] 

           ipsel= scc_read_ip_dat()
           ip_info= strarr(n_elements(ipsel))
           ipcnt= WHERE(ipsel.ip_description EQ 'ERROR',cnt)
           IF (cnt GT 0) THEN ip_info= strarr(ipcnt(0))
           FOR i= 0, n_elements(ip_info)-1 DO $
             ip_info(i)= string(ipsel(i).ip_num,'(i6)')+'     '+ipsel(i).ip_description
           ip_info= ['IPnum  Description',' ',ip_info]
           ip_info= [ip_how,ip_info]
           xdisplayfile,TITLE='IP_00_19 Help',$
           GROUP=event.top, HEIGHT=20, WIDTH=75, $
           TEXT= ip_info 
         END


        'resolution': BEGIN
          WIDGET_CONTROL, struct.resl, GET_VALUE= index & index= index(0)
          res_cond= res_tble(index)
        END

        'fields'' info' : BEGIN     ; help on fields
           ind = WHERE(field_struct.fhelp EQ event.id)
           table= tablename(ind)

           openw,unit,'tab.txt',/get_lun
           printf,unit,'Sorry - No field information is available for'
           printf,unit,table+' table. Please contact the programmer.'
           close,unit
           free_lun,unit

;           machine= 'unknown'
;           opsys= strupcase(!version.os)
;           if(opsys eq 'OSF') then begin  ; VMS/OSF at LAS
;             machine= 'lascos'
;           endif else begin
;             if(opsys eq 'VMS') then begin  ; VMS
;               machine= 'unknownVMS'
;             endif else begin  ; UNIX
;               machine= 'corona'
;             endelse
;           endelse

;           spawn,'rcp sybsrv@'+machine+':tab_def/'+table+' tab.txt'

;;           table= strupcase(table)
;;           cmnd="xdisplayfile,'tab.txt',TITLE='"+table+ $
;;               "',GROUP=event.top,HEIGHT=20, WIDTH=65"
;;           ret=execute(cmnd)

;           xdisplayfile,'tab.txt',TITLE='Fields' ,$

            if(dbase_used eq 'lasco') then begin 
              tstr= GETENV('NRL_LIB')+'/lasco/database/tab_def/'+table
              ;infile= findfile(tstr(0))
              infile= file_search(tstr(0))
              if(strlen(infile(0)) gt 0) then $
                info_file= infile(0) $
              else $
                info_file= 'tab.txt' 
              xdisplayfile,info_file, TITLE='Fields' ,$
              GROUP=event.top, HEIGHT=20, WIDTH=65
              openr,unit,'tab.txt',/get_lun,/delete
              free_lun,unit
            endif else begin
              if(dbase_used eq 'secchi' or dbase_used eq 'secchi_flight') then begin
                info_file= GETENV('SSW_SECCHI')+'/doc/secchi_img_tables_def.html'
              endif else begin
                info_file= 'tab.txt'
              endelse
              ; print,info_file
              xdisplayfile,info_file, TITLE='Fields' ,$
              GROUP=event.top, HEIGHT=20, WIDTH=120
            endelse

         END

        'SELECT' : BEGIN
                ind = WHERE(field_struct.button_id EQ event.id)
                IF (event.select) THEN $
                   qry.field(ind)= 1 $
                ELSE $
                   qry.field(ind)= 0
            END

        'ENTER' : BEGIN
                ind = WHERE(field_struct.text_id EQ event.id)
                WIDGET_CONTROL, event.id ,GET_VALUE=str
                qry.cond(ind)= str
            END

   ENDCASE

done:

END




function get_tokens,str,dbname,tname,separator
 tokens= ''
 i=0
 while(i ne -1) do begin
   i= strpos(str,separator)
;  Ignore the ' and ' that comes with between (ie between 5 and 10) when
;  the separator is ' and ':
   tmp= strmid(str,0,i)
   if(strpos(tmp,' between ') ne -1) then begin
     tmp= strmid(str,i+1,strlen(str))
     tmp= str
     strput,tmp,'_',i
     i= strpos(tmp,separator) 
   end
   if(i ne -1) then begin
     col= strmid(str,0,i)
     col= strtrim(col,2)
     str=strmid(str,i+strlen(separator),strlen(str))
   endif else begin ; last col
     col= strtrim(str,2)
   endelse 
   if(strlen(col) gt 0 ) then $ ; may not have a field selected from a table in
                                ; the join.
;    For MySql, qualify the fields with the database, owner (using .) and table name:
;     tokens= [tokens,dbname+'.'+tname+'.'+col] 
     tokens= [tokens,tname+'.'+col] ; no - don't use database name
 end

 if(n_elements(tokens) gt 1) then tokens= tokens(1:n_elements(tokens)-1)

 return,tokens

end



pro add_hk_clause,jqry,tab_name,req_pck_time

  if(req_pck_time eq '') then begin
    print,''
    print,'ERROR: No pck_time was given for joining the tables.....' 
    print,''
    jqry=''
  endif else begin
    tab= where(tab_name eq 'hk_data_tables',count)
    if(count gt 0) then begin ; 'hk_data_tables' is not joinable.
      print,''
      print,'ERROR: Can''t join hk_data_tables; It does not have a pck_time.'
      print,''
      jqry=''
      return
    endif
    tab= where(tab_name eq 'hk_1_time',count)
    if(count gt 0) then begin ; 'hk_1_time' table was selected
      tab= where(tab_name ne 'hk_1_time',count)
      tab_name= tab_name(tab) ; remove 'hk_1_time' from the working list 
    endif else begin ; add 'hk_1_time' table name to the query 
      pos= strpos(jqry,'where')
      tmp= strmid(jqry,0,pos-1)+','+'hk_test.hk_1_time'   
      jqry= tmp+strmid(jqry,pos-1,strlen(jqry))
    end

    if(req_pck_time eq 'HK_IMG_JOIN') then begin
      tab= where(tab_name eq 'img_leb_hdr',count)
      if(count le 0) then begin ; add 'img_leb_hdr' table name
        pos= strpos(jqry,'where')
        tmp= strmid(jqry,0,pos-1)+','+'lasco.img_leb_hdr'
        jqry= tmp+strmid(jqry,pos-1,strlen(jqry))
      end
      jqry= jqry+'hk_test.hk_1_time.hk1_lobt = lasco.img_leb_hdr.date_obs'
    endif else begin ; only hk tables exist (no joins with image tables)
      if(strpos(req_pck_time,'between') ge 0) then begin ;a range is given
        jqry= jqry+'hk_test.hk_1_time.pck_time '+req_pck_time 
      endif else begin ; not a range, = 'date_time', was entered.
;       use a 14.999 second window (from req_pck_time - 15 seconds 
;                                   to   req_pck_time - 1 milliseconds)
        jqry= jqry+'hk_test.hk_1_time.pck_time between dateadd(ss,-15,'+req_pck_time+')'
        jqry= jqry+' and dateadd(ms,-1,'+req_pck_time+')'
      end
    end
    for i=0,n_elements(tab_name)-1 do begin
     if(strpos(tab_name(i),'hk_') ge 0) then begin ; only use hk tables
      jqry= jqry+' and hk_test.'+tab_name(i)+'.pck_time between hk_test.hk_1_time.pck_time and '
      jqry= jqry+'dateadd(ss,12,hk_test.hk_1_time.pck_time)'
     end
    end
  end

  return
end




pro add_img_clause,jqry,tab_name
  
  ; This code Works for MySql:

  andjoin= ''

  fc= strpos(jqry,' from ')+6
  fw= strpos(jqry,' where')
  dbtab= strmid(jqry,fc,fw-fc)
  dbtab= str_sep(dbtab,',')
  count= n_elements(dbtab)

;print,dbtab
;print,jqry

  addme=''
  ;hc_ind= where(dbtab EQ 'secchi.img_hist_cmnt', hc_cnt)
  hc_ind= where(strpos(dbtab,'img_hist_cmnt') ge 0, hc_cnt)
  IF (hc_cnt GT 0) THEN BEGIN
    ;addme= ' and secchi.img_seb_hdr.filename = secchi.img_hist_cmnt.filename'
    ; now remove img_hist_cmnt entry:
    ;o_ind= where(dbtab NE 'secchi.img_hist_cmnt', count)
    o_ind= where(dbtab NE 'img_hist_cmnt', count)
    ;IF (count GT 0) THEN dbtab= [dbtab(o_ind)]
    dbtab= [dbtab(o_ind)] 
    ;addme= ' secchi.img_seb_hdr.filename = secchi.img_hist_cmnt.filename'
    addme= ' img_seb_hdr.filename = img_hist_cmnt.filename'
    ;IF (count GT 1) THEN addme= ' and'+addme
    ;IF (strpos(jqry,'secchi.img_seb_hdr') LT 0) THEN BEGIN
    ;sh_ind= where(dbtab EQ 'secchi.img_seb_hdr', sh_cnt)
    sh_ind= where(dbtab EQ 'img_seb_hdr', sh_cnt)
    IF (sh_cnt EQ 0) THEN BEGIN  
      p= strpos(jqry,' where')
      ;jqry= strmid(jqry,0,p)+',secchi.img_seb_hdr where '
      jqry= strmid(jqry,0,p)+',img_seb_hdr where '
      ;addme= ' '+dbtab(count-1)+'.fileorig = secchi.img_seb_hdr.fileorig and'+addme
      addme= ' '+dbtab(count-1)+'.fileorig = img_seb_hdr.fileorig and'+addme
    ENDIF
    IF (count GT 1) THEN addme= ' and'+addme

  ENDIF

  for i= 0,count-2 do begin ; tables to be joined
    ;db1= (str_sep(dbtab(i),'.'))(0)
    ;db2= (str_sep(dbtab(i+1),'.'))(0)

;help,dbtab(i),dbtab(i+1)
;stop

    ;; At this point img_hist_cmnt, if any, is removed from dbtab list:
    ;;if (strpos(dbtab(i),'img_seb') ge 0 and strpos(dbtab(i+1),'img_seb') ge 0) then begin
    ;;if (strpos(dbtab(i),'secchi.img') ge 0 and strpos(dbtab(i+1),'secchi.img') ge 0 OR $
    ;;    strpos(dbtab(i),'secchi_flight.img') ge 0 and strpos(dbtab(i+1),'secchi_flight.img') ge 0) then begin
    ;  join_field= '.fileorig' 
    ;endif else begin
    ;  join_field= '.filename'
    ;endelse

    ; Note: only secchi IMG tables are joined (no HK table joins) and at this point with img_hist_cmnt
    ;       removed, all remaining tables for IMG use fileorig for joins:

    join_field= '.fileorig'

    jqry= jqry+andjoin+dbtab(i)+join_field+' = '+dbtab(i+1)+join_field 
    andjoin= ' and '
;help,jqry
;stop
  endfor 

  jqry= jqry+addme

;print,jqry
;stop

  return
end



pro add_hk_img_clause,jqry,tab_name,req_pck_time

  add_hk_clause,jqry,tab_name,'HK_IMG_JOIN'
  jqry= jqry+' and '

; Remove 'hk_' table names before calling add_img_clause:

  tmp_tab= ''
  for i=0, n_elements(tab_name)-1 do begin
    if(strpos(tab_name(i),'img_') ge 0) then tmp_tab= [tmp_tab,tab_name(i)]
  end
  tmp_tab= tmp_tab(1:n_elements(tmp_tab)-1)

; See if img_leb_hdr should be added:

  tmp= where(tmp_tab eq 'img_leb_hdr',pos)
  if(pos eq 0) then tmp_tab= [tmp_tab,'img_leb_hdr'] 
 
  add_img_clause,jqry,tmp_tab

  len= strlen(jqry)
  if (strmid(jqry,len-5,len) eq ' and ') then jqry= strmid(jqry,0,len-5)

return
end




function add_join_clause,jqry,tab_name,req_pck_time
;help,jqry,tab_name,req_pck_time

  tab_cnt= n_elements(tab_name)
  tab_group= strmid(tab_name(0),0,3)
  tmp_group= tab_group
  for i=1,tab_cnt -1 do begin
    ch3=strmid(tab_name(i),0,3)
    if(strpos(tmp_group,ch3) eq -1) then begin
      tab_group= [tab_group,ch3] 
      tmp_group= tmp_group+' '+ch3
    end
  end
 
  tab_group= tab_group(sort(tab_group))
  sort_group= tab_group(0)
  for i= 1,n_elements(tab_group)-1 do sort_group= sort_group+' '+tab_group(i)  

;help,tab_group,sort_group


  case (sort_group) of
    ;'hk_': add_hk_clause,jqry,tab_name,req_pck_time
    'hk_': begin
       jqry= jqry+tab_name(0)+'.pckt_time='+tab_name(1)+'.pckt_time'
       for i= 2, n_elements(tab_name)-1 do begin
         jqry= jqry+' and '+tab_name(i-1)+'.pckt_time='+tab_name(i)+'.pckt_time'
       endfor
    end
    'img': add_img_clause,jqry,tab_name
    'hk_ img': add_hk_img_clause,jqry,tab_name,'HK_IMG_JOIN'
    'car syn': jqry=jqry+'carrbld.synoid=synobld.synoid'
    else: begin
      print,''
      message,'*** SORRY, "'+sort_group+'" table groups can not be joined. .c to continue.'
      jqry= 'select'
    end
  endcase

;print,jqry
;stop
 
  return, jqry
end




function join_quries,quries,qry_cnt,req_pck_time 

;help,quries,qry_cnt,req_pck_time 
;stop

  q_cols= ''
  q_conds= ''
  for i=0,qry_cnt-1 do begin
   cmnd=quries.cmnd(i)
   if(strlen(cmnd) gt 0) then begin
    tab_name= quries.tab_name(i)
    db_name= quries.db_name(i)
;   pick and qualify columns with dbase and table name:
    p1= strpos(cmnd,'select')+6
    p2= strpos(cmnd,' from ')
    cols= strmid(cmnd,p1,p2-p1)
    IF cols NE ' ' THEN q_cols= [q_cols,get_tokens(cols,db_name,tab_name,',')]
;   pick and qualify conditions with dbase and table name:
    p1= strpos(cmnd,' where ')

    if(p1 ne -1 and p1+7 lt strlen(cmnd)) then begin
      conds= strmid(cmnd,p1+6,strlen(cmnd))
      q_conds= [q_conds,get_tokens(conds,db_name,tab_name,' and ')] 
;help,conds,q_conds
;stop
    end
   end
  end

 q_cols= q_cols(1:*) ; 1st col is blank
 ncols= n_elements(q_cols)

 ; remove duplicate column names, if any:

  newcols= strarr(ncols)
  for i= 0, ncols-1 do begin 
    ;newcols(i)= (str_sep(q_cols(i),'.'))(2) ; pick col name from database.table.column
    newcols(i)= (str_sep(q_cols(i),'.'))(1) ; pick col name from table.column
  end
  unq= uniq_nosort(newcols)
  q_cols= q_cols(unq)
 
  jqry= 'select '
  for i=0,n_elements(q_cols)-1 do begin 
    ;colname= (str_sep(q_cols(i),'.'))(2) ; pick col name from database.table.column
    colname= (str_sep(q_cols(i),'.'))(1) ; pick col name from table.column
    cind= where(strpos(q_cols,colname) ge 0, ccnt)

    if( strlen(q_cols(i)) gt 0) then jqry= jqry+q_cols(i)+',' 
  end
  jqry= strmid(jqry,0,strlen(jqry)-1) ; remove the last comma
  jqry= jqry+' from '
; For MySql, qualify table names with database and owner (using .) name:
;  for i=0,qry_cnt-1 do jqry= jqry+quries.db_name(i)+'.'+quries.tab_name(i)+','
   for i=0,qry_cnt-1 do jqry= jqry+quries.tab_name(i)+',' ; don't add db_name
  jqry= strmid(jqry,0,strlen(jqry)-1) ; remove the last comma
  jqry= jqry+' where ' 
  for i=1,n_elements(q_conds)-1 do jqry= jqry+q_conds(i)+' and '
;  jqry= strmid(jqry,0,strlen(jqry)-5) ; remove the last ' and ' 

  jqry= add_join_clause(jqry,quries.tab_name(0:qry_cnt-1),req_pck_time)
  ;print,jqry
  return,jqry
end



function convert_types,field_types
  elems= n_elements(field_types)
  idl_types= strarr(elems)

  for i= 0,elems-1 do begin
    case 1 of
      field_types(i) eq 59 or field_types(i) eq 62: idl_types(i)= 'Float'
      field_types(i) eq 61 or field_types(i) eq 47 or $
      field_types(i) eq 58 or field_types(i) eq 111: idl_types(i)= 'String'
      field_types(i) eq 52: idl_types(i)= 'Integer'
      field_types(i) eq 50 or field_types(i) eq 48: idl_types(i)= 'Byte'
      field_types(i) eq 56: idl_types(i)= 'Long' 
      field_types(i) eq 35: idl_types(i)= 'Text'
      field_types(i) eq 34: idl_types(i)= 'Image'
    else: idl_types(i)= '???'
    end ;case
  end
  return,idl_types
end



pro all_secchi_menus,dbase,cmnd
 common shared_var,tab_select
 common shared_field,qry,field_struct,tablename,dbase_used
 common shared_res, res_tble, is_hk, res_cond

 ;res_cond= ''
 device,get_screen_size=screen_size
 wsizes=define_widget_sizes()
 wxoffs=2*wsizes.border + 100 + wsizes.scrbar
 wyoffs=2*wsizes.border + wsizes.dsktop + wsizes.winhdr + wsizes.scrbar
 
IF screen_size[0] LT 801 THEN xsz=780 ELSE xsz=0 
help,xsz,wxoffs,wyoffs

;Get existing database names and display them:

 dbases=SECCHI_DB_QRY()
 iF ((SIZE(dbases))(0) EQ 0) THEN RETURN
 dbases= strtrim(dbases,2) ; remove leading and trailing blanks
 ;dbases= dbases(where(dbases ne "mysql")) ; remove mysql database 
 dbases= dbases(where(strpos(dbases,'mysql') lt 0)) ; remove mysql databases
 dbases= dbases(where(strpos(dbases,'information_schema') lt 0)) ; remove schema dbase
 dbases= dbases(where(strpos(dbases,'test') lt 0)) ; remove test databases 
 dbases= dbases(where(strpos(dbases,'vote') lt 0)) ; remove vote databases

 selection=''

;Generate a menu of databases and let user to choose a database:

 button_id = lonarr(n_elements(dbases))
 tab_select= intarr(n_elements(dbases))

 ;base=widget_base(title='Choose a database(s) ',/COLUMN,x_scroll_size=200,$
 ;base=widget_base(title='Choose a database(s) ',/COLUMN,x_scroll_size=200, y_scroll_s=500,xoffset=10,yoffset=10,/SCROLL,/FRAME)
base=widget_base(title='Choose a database',/COLUMN,xsize=250, xoffset=10,yoffset=10)
                 ;y_scroll_size=300,/SCROLL,/FRAME)
 proc = WIDGET_BUTTON(base, VALUE='Proceed', UVALUE='Done')
 quit = WIDGET_BUTTON(base, VALUE='quit', UVALUE='quit')
 col = widget_base(base, /ROW)
 col1= widget_base(col, /ROW)
 help = widget_button(col1, VALUE='Help', UVALUE='DB_Help') 

 row2= widget_base(base, /COL, /EXCLUSIVE)
 for i=0,n_elements(dbases)-1 do begin
   ;row = widget_base(base, /ROW)
   ;row2 = widget_base(row, /ROW, /NONEXCLUSIVE)
   button_id(i) = widget_button(row2,VALUE=dbases(i),UVALUE='SELECT')
 endfor
 widget_control,/realize,base
 tab_struct = {base:base,fields:dbases,button_id:button_id}
 widget_control,base, SET_UVALUE=tab_struct
 xmanager,'select',base,EVENT_HANDLER='scc_select_event'

 dbase= ''
 cmnd= ''
 t_ind= where(tab_select eq 1,count)
 sdb= count
 if(count gt 0) then begin ; atleast one database was selected
   tab_cnt= n_elements(t_ind)
;  print,'select database: ',dbases(t_ind)
   dbase= dbases(t_ind)
 endif else return 

;Get tables names belonging to the database:

 tit= 'Choose table(s) from '
 for i= 0, count-2 do tit= tit+ strupcase(dbase(i))+', '
 if(count eq 1) then i= 0
 tit= tit+ strupcase(dbase(i))
; if(sdb eq 2) then base= widget_base(title= tit,xoffset=10,x_scroll_size=550, $
;                                      y_scroll_size=550,/frame)
; if(sdb gt 2) then base= widget_base(title= tit,xoffset=10,x_scroll_size=750, $
;                                      y_scroll_size=550,/frame)
base= widget_base(title= tit,xoffset=10,/frame)

 b1= widget_base(base,/ROW)
 colb1= widget_base(b1,/COLUMN)
 rowb1= widget_base(colb1,/ROW)

 quit = WIDGET_BUTTON(rowb1, VALUE='Done', UVALUE='Done')
 help= WIDGET_BUTTON(colb1, VALUE='Help',UVALUE='Help')

 max_len= 100

 b_id= lonarr(sdb,max_len) ; assuming at most 100 tables per database
 tab_select= intarr(sdb,max_len)
 all_tables= strarr(sdb,max_len)

 for j=0, count-1 do begin
   db_tables= SECCHI_DB_QRY(dbase(j))
   IF ((SIZE(db_tables))(0) EQ 0) THEN RETURN
   db_tables= strtrim(db_tables,2) ; remove leading and trailing blanks
   all_tables(j,0:n_elements(db_tables)-1)= db_tables
   selection=''
   condition=''

;  Generate menu(s) of database tables and let user to choose table(s):

;   row = widget_base(b1,/COLUMN,x_scroll_size=200, y_scroll_size=500,/SCROLL,/FRAME)
   row = widget_base(b1,/COLUMN,/FRAME)

   tmp= widget_base(row, /ROW)
   tmp1= widget_label(tmp,value='"'+strupcase(dbase(j))+'"')

   IF (STRPOS(db_tables(0),'Tables_in') GE 0) THEN BEGIN
     row2 = widget_base(row, /ROW)
     b_id(j,i) = widget_label(row2,value='No Tables in Database')
   ENDIF ELSE BEGIN
     for i=0,n_elements(db_tables)-1 do begin
       row2 = widget_base(row, /ROW, /NONEXCLUSIVE)
       b_id(j,i) = widget_button(row2, VALUE=db_tables(i), UVALUE='SELECT')
     endfor
   ENDELSE
 end

 widget_control,/realize,base
 tab_struct = {base:base,fields:db_tables,button_id:b_id}
 widget_control,base, SET_UVALUE=tab_struct
 xmanager,'select',base,EVENT_HANDLER='scc_select_event'

 cmnd= ''

 t_ind= where(tab_select eq 1,count)
 db_tables= all_tables

 ip_00_cond= ''

 if(count gt 0) then begin ; at least one table was selected
   tab_cnt= n_elements(t_ind)
   quries= {db_name: strarr(tab_cnt),tab_name: strarr(tab_cnt), $
            cmnd:strarr(tab_cnt)}
   req_pck_time= ''

   is_hk=0
   tit= 'Choose field(s) from '
   for i=0,tab_cnt-2 do begin 
     ;tit= tit + strupcase(db_tables(t_ind(i) mod sdb,t_ind(i)/sdb))+', '
     tbn= strupcase(db_tables(t_ind(i) mod sdb,t_ind(i)/sdb))
     tit= tit + tbn+', '
     IF (STRPOS(tbn,'HK_') GE 0) THEN is_hk=1 
   endfor

   if(tab_cnt eq 1) then i= 0
   ;tit= tit+ strupcase(db_tables(t_ind(i) mod sdb,t_ind(i)/sdb))
   tbn= strupcase(db_tables(t_ind(i) mod sdb,t_ind(i)/sdb))
   tit= tit+tbn 
   IF (STRPOS(tbn,'HK_') GE 0) THEN is_hk=1

xscrsz=350*tab_cnt + wxoffs
;   if(tab_cnt eq 1) then base= widget_base(title= tit,xoffset=10, $
;                               x_scroll_size=380,y_scroll_size=550,/frame)
;   if(tab_cnt eq 2) then base= widget_base(title= tit,xoffset=10, $
;                               x_scroll_size=710,y_scroll_size=550,/frame)
;   if(tab_cnt gt 2) then base= widget_base(title= tit,xoffset=10, $
;                               x_scroll_size=900,y_scroll_size=550,/frame)
base= widget_base(title= tit,xoffset=10,x_scroll_size=xscrsz,y_scroll_size=screen_size[1]-wyoffs-200,/frame)
print,screen_size

   b1= widget_base(base,/ROW)
   colb1= widget_base(b1,/COLUMN)
   rowb1= widget_base(colb1,/ROW)
   quit = WIDGET_BUTTON(rowb1, VALUE='Done', UVALUE='Done')
   help= WIDGET_BUTTON(colb1, VALUE='Help',UVALUE='Help')
   img_seb_hdr= where(db_tables(t_ind) eq 'img_seb_hdr',seb_cnt)
   IF seb_cnt GT 0 THEN $
     iphelp= WIDGET_BUTTON(colb1, VALUE='ip_00_19_Help',UVALUE='IPHelp')

   resl= 0
   ;res_tble= ['Full','1hr','30min','10min','1min']
   res_tble= ['1hr','30min','10min','1min','Full'] 
   res_cond= res_tble(0) 
   IF (is_hk) THEN BEGIN
     txt= Widget_label(colb1, VALUE='')
     txt= Widget_label(colb1, VALUE='Resolution:')
     resl= CW_BSELECTOR2(colb1, res_tble, UVALUE= 'resolution', SET_VALUE=0)
   ENDIF ELSE res_cond= ''

   b_id= lonarr(tab_cnt,max_len) ; assuming at most 100 fields per table
   tab_select= intarr(tab_cnt,max_len)
   all_fields= strarr(tab_cnt,max_len)
   text_id   = lonarr(tab_cnt,max_len)
   fhelp= intarr(tab_cnt)

   for i=0,tab_cnt-1 do begin
     quries.tab_name(i)= db_tables(t_ind(i) mod sdb,t_ind(i)/sdb)
;    print,'select table: ',quries.tab_name(i)
     quries.db_name(i)= dbase(t_ind(i) mod sdb)
;    print,'belongs to Dbase: ',quries.db_name(i)

;    table(s) selected now. Generate a menu displaying fields within that
;    table so that user can select from it: 

     ;db_all_fields= db_help_func(quries.db_name(i),quries.tab_name(i))
     db_all_fields= SECCHI_DB_QRY(quries.db_name(i),quries.tab_name(i))
     ;stop
     ;if(db_all_fields(0) eq 'no data') then return
     IF((SIZE(db_all_fields))(0) EQ 0) THEN RETURN
     db_all_fields= strtrim(db_all_fields,2) ;remove leading and trailing blanks

     ; 1st half of db_all_fields contains the field names and 2nd half idl field 
     ; types, so, separate them:

     elems= n_elements(db_all_fields)

     idl_field_types= db_all_fields(elems/2:elems-1)
     db_all_fields= db_all_fields(0:elems/2 - 1)
     select_db_all_fields= intarr(n_elements(db_all_fields))

     all_fields(i,0:n_elements(db_all_fields)-1)= db_all_fields

     ;row = widget_base(b1,/COLUMN,x_scroll_size=280, y_scroll_size=500,/SCROLL,/FRAME)
     row = widget_base(b1,/COLUMN,/FRAME)

     tmp= widget_base(row, /ROW)
     tmp1= widget_label(tmp,value='"'+strupcase(quries.tab_name(i))+'"')
     fhelp(i)= widget_button(tmp,value='fields'' info',uvalue='fields'' info')

     for j=0,n_elements(db_all_fields)-1 do begin
       row1= widget_base(row, /ROW)
       row2 = widget_base(row1, /ROW, /NONEXCLUSIVE)
       b_id(i,j) = widget_button(row2, VALUE=db_all_fields(j), UVALUE='SELECT')
       row3 = widget_base(row1, /ROW)
       text_id(i,j) = WIDGET_TEXT(row3, UVALUE='ENTER', /EDITABLE, /ALL_EVENTS)
       ss= WIDGET_LABEL(row3,value=idl_field_types(j))
     endfor
   end

   widget_control,/realize,base
   field_struct= { $
      base:base,                      $
      fhelp:fhelp,                    $
      fields:db_all_fields,           $
      resl:resl,                      $
      button_id:b_id,                 $
      text_id:text_id                 $
   }

   qry= {field:tab_select,cond:strarr(tab_cnt,max_len)}

   tablename= quries.tab_name
   dbase_used= dbase(0)

   widget_control,base, SET_UVALUE=field_struct
   xmanager,'sql_qry',base,EVENT_HANDLER='sql_qry_event'

   f_ind= where(qry.field eq 1,f_select)

   if(f_select eq 0) then return ;no field was selected

   f_ind= where(qry.field eq 1,f_select)
   if(f_select gt 0) then begin ; atleast one field was selected
     used_tab= 0
     for qry_num=0, tab_cnt -1 do begin
       select_ind= where(f_ind mod tab_cnt eq qry_num, count)
       if(count gt 0) then select_ind= f_ind(select_ind) / tab_cnt
       fields= all_fields(qry_num,*)
       fields= fields(0:max_len-1) ; change from arr(1,max_len) to arr(max_len)
       conds= qry.cond(qry_num,*)
       conds= conds(0:max_len-1)
       tmp= where(conds ne '',tmp_cnt)

       ; change all 'and like' to 'AND like' in conds so they are not used as separators:
       for tind=0, tmp_cnt-1 do begin
         andind= strpos(conds(tmp(tind)),'and like')
         while (andind gt 0) do begin
           conds(tmp(tind))= strmid(conds(tmp(tind)),0,andind)+'AND'+ $
                             strmid(conds(tmp(tind)),andind+3,500)
           andind= strpos(conds(tmp(tind)),'and like')
         endwhile
       endfor

       if(not (count eq 0 and tmp_cnt eq 0)) then begin 
         
;help,qry_num,quries.tab_name(qry_num),fields,select_ind,conds,tab_cnt,req_pck_time
;stop
 
          quries.cmnd(used_tab)= form_qry(quries.tab_name(qry_num), $
                             fields,select_ind,conds,tab_cnt,req_pck_time) 
          quries.tab_name(used_tab)= quries.tab_name(qry_num)
          quries.db_name(used_tab)= quries.db_name(qry_num)
          ;ip_00= where(strpos(fields(select_ind),'ip_00_19') ge 0, ipcnt)
          ; use field name from cond in case ip_00_19 field is not selected but cond is given:
          ipcnt= 0
          if (tmp_cnt gt 0) then ip_00= where(strpos(fields(tmp),'ip_00_19') ge 0, ipcnt) 
          if (ipcnt gt 0) then begin
            ;ip_00_cond= conds(select_ind(ip_00(0)))
            ip_00_cond= conds(tmp(ip_00(0)))
          end
          used_tab= used_tab+1
       end
     end
   end ; return database(s) from which table(s) were selected


   cmnd= quries.cmnd(0)
   dbase= quries.db_name(0) ; return a database from which a table was selected
;help,cmnd,dbase,tab_cnt,used_tab,ip_00_cond
;stop

;  Join the queried if more than one table was selected:

;   if(tab_cnt gt 1) then cmnd= join_quries(quries,tab_cnt,req_pck_time) 

   if(used_tab lt tab_cnt) then tab_cnt= used_tab ; some selected tables 
                                                  ; were not used
   if(used_tab gt 1) then cmnd= join_quries(quries,used_tab,req_pck_time) 

 end

toks= str_sep(strlowcase(ip_00_cond),'like')
if (n_elements(toks) gt 2) then begin
  ; If more than one like in ip_00_cond, correct it by adding ip_00_19 before
  ; all likes and put all between () so that ORs, don't interfer with other
  ; query conditions:
  ind= strpos(cmnd,'img_seb_hdr.ip_00_19 '+ip_00_cond)
  offset= strlen('img_seb_hdr.ip_00_19 ')
  if (ind eq -1) then begin
    ind= strpos(cmnd,'ip_00_19 '+ip_00_cond)
    offset= strlen('ip_00_19 ')
  endif
  if (ind ge 0) then begin
    cmnd1= strmid(cmnd,0,ind)
    cond= strmid(cmnd,ind,500)
    fixit= strmid(cond,0,offset+strlen(ip_00_cond))
    rest= strmid(cond,offset+strlen(ip_00_cond),500)
    toks=str_sep(fixit,'like')
    if (n_elements(toks) gt 1) then begin
      fixit= '('
      for t=1, n_elements(toks)-2 do $ 
        fixit= fixit+'ip_00_19 like'+toks(t)
      fixit= fixit+'ip_00_19 like'+toks(t)+')'
    endif
 ;   print,cmnd
    cmnd= cmnd1+fixit+rest
 ;   print,cmnd 
  endif
endif

 IF (res_cond NE '') THEN BEGIN
   toks=str_sep(cmnd,' ')
   ind= where(toks eq 'from', find)
   IF (find GT 0 AND res_cond NE 'Full') THEN BEGIN
     tbl= (str_sep(toks(ind(0)+1),','))(0) 
     ;if (STRPOS(cmnd,' where ') GT 0) THEN $
     ;  cmnd= cmnd+' and ' $
     ;else $
     ;  cmnd= cmnd+' where '
     CASE res_cond OF
       ; '1hr':   cmnd= cmnd+'CAST('+tbl+'.pckt_time as CHAR) like "%:00:0%"'
       ; '30min': cmnd= cmnd+'(CAST('+tbl+'.pckt_time as CHAR) like "%:00:0%" OR '+ $
       ;                     'CAST('+tbl+'.pckt_time as CHAR) like "%:30:0%")' 
       ; '10min': cmnd= cmnd+'CAST('+tbl+'.pckt_time as CHAR) like "%:%0:0%"'
       ; '1min':  cmnd= cmnd+'CAST('+tbl+'.pckt_time as CHAR) like "%:%:0%"'

       '1hr': cmnd= cmnd+' group by mid('+tbl+'.pckt_time,1,13)' ; keep 1st row of yyyy-mm-dd hh
       '30min':   begin
            cast_str= '(CAST('+tbl+'.pckt_time as CHAR) like "%:00:0%" OR '+ $
                       'CAST('+tbl+'.pckt_time as CHAR) like "%:30:0%")'
            ; keep 1st row of matches using group where matches are for yyyy-mm-dd hh:mm (sql mid):
            IF (STRPOS(cmnd,' where ') GT 0) THEN $  
              cmnd= cmnd+' and '+cast_str+' group by mid('+tbl+'.pckt_time,1,16)' $
            ELSE $
              cmnd= cmnd+' where '+cast_str+' group by mid('+tbl+'.pckt_time,1,16)'  
          end
       '10min': begin
            cast_str= 'CAST('+tbl+'.pckt_time as CHAR) like "%:%0:0%"'
            ; keep 1st row of matches using group where matches are for yyyy-mm-dd hh:mm (sql mid):
            IF (STRPOS(cmnd,' where ') GT 0) THEN $  
              cmnd= cmnd+' and '+cast_str+' group by mid('+tbl+'.pckt_time,1,16)' $
            ELSE $
              cmnd= cmnd+' where '+cast_str+' group by mid('+tbl+'.pckt_time,1,16)'
         end
       '1min': cmnd= cmnd+' group by mid('+tbl+'.pckt_time,1,16)' ; keep 1st row of yyyy-mm-dd hh:mm
     ENDCASE
   ENDIF
 ENDIF

 ; Add 'unix_time' to select statement for HK tables if not present:

 IF (STRMID(cmnd,STRPOS(cmnd,' from ')+6,9) EQ 'secchi_hk' OR $
     STRMID(cmnd,STRPOS(cmnd,' from ')+6,3) EQ 'hk_') THEN BEGIN
   IF (STRPOS(cmnd,'unix_time') LT 0) THEN BEGIN
     selflds= STRMID(cmnd,7,STRPOS(cmnd,' from ')-7)
     fst= (STR_SEP(selflds,','))(0)
     add_fld=''
     IF (RSTRPOS(fst,'.') GE 0) THEN add_fld= STRMID(fst,0,RSTRPOS(fst,'.')+1)
     cmnd= 'select '+add_fld+'unix_time,'+STRMID(cmnd,7,STRLEN(cmnd))
   ENDIF
 ENDIF

 ;PRINT,'query= ',cmnd
 ;stop

return
end


