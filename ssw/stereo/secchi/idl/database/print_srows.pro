; Ed Esfandiari September 2004 - Adpated for SECCHI
;
; Oct 1996
; Ed Esfandiari

; "print_rows" procedure displays, saves, or prints rows of a valid qdb
; output structure for selected columns. User interaction and displayed
; information are done using IDL widgets. This routine can be called
; individually (ex: print_rows,st) but it it also used by "qdb_tools.pro"

; Apr 1996  Ed Esfandiari
; Set the maximum nember of rows to be displayed to the first 2000 rows.

; Jan 1999 Ed Esfandiari
; Added code to display such statistics as total rows (images), number of
; QL images and perecntage, and number of level_0 images and percentage.

pro disp_srows_event,event
 common disp_sdata, data_rows,selected_cols,save_filename,printer_name, $
        t,equals,tags,ntags,elem,new_Cols,tag_select,num_param,message, $
        old_select,viewed
 common orientation, lp_orient

 WIDGET_CONTROL, event.id, GET_UVALUE=uval
 WIDGET_CONTROL, event.top, GET_UVALUE=struct   ; get structure from UVALUE

 case (uval) of
  'done': begin
            WIDGET_CONTROL, /DESTROY, struct.base
          end
  'disp': begin
            viewed= 'true'
            widget_control,/hour
            fields= where(tag_select eq 1,tcnt)
            if(tcnt eq 0) then begin
              msg='Select column(s) to be displayed.'
              res=widget_message(msg,title='No Selection')
            endif else begin
              tmp= where(tag_select ne old_select,scnt) 
              if(scnt gt 0) then begin
                new_tags= tags(fields)
                num_cols= n_elements(fields)
                new_cols= ''
                for j= 0, num_cols-1 do new_cols= new_cols+new_tags(j)+'   ' 
                d= ''
                for j=0, num_cols-1 do $
                  d= d+strmid(equals,0,strlen(new_tags(j)))+'   '
                new_cols= [new_cols,d]
                if(num_param eq 2) then begin
                  new_cols= [message,'',new_cols] 
                end
                for i=0L,elem-1 do begin
                  this_row= ''
                  for j=0,num_cols-1 do this_row= this_row+t(fields(j),i)+'   '
                  new_cols= [new_cols,this_row]
                end
                old_select= tag_select
              endif
              xdisplayfile,TITLE='"View Data"',GROUP=base,HEIGHT=45,$
                           WIDTH= 40 > STRLEN(new_cols(1)) < 180, TEXT= new_cols 
                           ;WIDTH=80, TEXT= new_cols 
            endelse
          end
  'save': begin
           if (viewed eq 'false') then begin
             msg= ['View the data first. It will configure the layout',$
                   'for Save and Print options.']
             res=widget_message(msg,title='Note:')
             return
           end
           msg=['Latest viewed data will be saved. Changes to the selected',$
                'available columns made after the last view will be ignored.',$
                '',$
                'Data will be saved in "'+save_filename(0)+'".',$
                '',$
                '                 Proceed with Save ?']
           res=widget_message(msg,/question,title='Confirm')
           if(res eq 'Yes') then begin
             widget_control,/hour
             openw,unit,save_filename(0),/get_lun
             for i=0L,n_elements(new_cols)-1 do printf,unit,new_cols(i)
             free_lun,unit
           end ;confirm
          end
  'save_name': begin
                 WIDGET_CONTROL, event.id ,GET_VALUE= save_filename
                 save_filename= strtrim(save_filename,2)
               end
  'orient': begin
              IF (event.index EQ 0) THEN $
                lp_orient= 'Portrait' $
              ELSE $
                lp_orient= 'LandScape' 
            end
  'hcopy': begin
           if (viewed eq 'false') then begin
             msg= ['View the data first. It will configure the layout',$
                   'for Save and Print options.']
             res=widget_message(msg,title='Note:')
             return
           end
            msg=['Latest viewed data will be printed. Changes to the selected',$
                'available columns made after the last view will be ignored.',$
                '',$
                '       Printer to be used is "'+printer_name(0)+'" and selected',  $
                '              orientation is "'+lp_orient+'".', $
                '',$
                '                  Proceed with Print?']
            res=widget_message(msg,/question,title='Confirm')
            if(res eq 'Yes') then begin
             widget_control,/hour
             openw,unit,'print_data.db',/get_lun
             for i=0L,n_elements(new_cols)-1 do printf,unit,new_cols(i) 
             free_lun,unit
             IF (lp_orient EQ 'Portrait') THEN BEGIN
               lpr= "lpr -P "+printer_name(0)+" print_data.db"
             ENDIF ELSE BEGIN ; landsacpe
               lpr= "/usr/lib/lp/postscript/postprint -pland print_data.db | lp -d"+printer_name(0)
             ENDELSE
             spawn,lpr
             openr,unit,'print_data.db',/get_lun,/delete
             free_lun,unit
            end; confirm
           end
  'lp_name': begin
               WIDGET_CONTROL, event.id ,GET_VALUE= printer_name
               printer_name= strtrim(printer_name,2)
             end
   'select': begin
               old_select= tag_select
               ind = WHERE(struct.button_id EQ event.id)
               IF (event.select) THEN $
                  tag_select(ind)= 1 $
               ELSE $
                  tag_select(ind)= 0
             end
 endcase

end


pro print_srows,st,info
 common disp_sdata, data_rows,selected_cols,save_filename,printer_name, $
        t, equals,tags,ntags,elem, new_cols,tag_select,num_param,message, $
        old_select, viewed
 common orientation, lp_orient

  num_param=n_params()
  if(num_param lt 1) then return

; defaults:
  selected_cols= 'all'
  save_filename= 'rows.txt'
  printer_name= 'hp4-247'
  viewed= 'false'
  print,''  
  s=size(st)

  if(s(0) eq 1 and s(2) eq 8) then begin ; input is a strucutre
    tags=tag_names(st)

    ql_rows= 0
    p5_rows= 0
    l1_rows= 0
    tmp= where(strlowcase(tags) eq 'datalevel', dlvl)
    if(dlvl gt 0) then begin
      tmp= where(st.datalevel eq 0,p5_rows)
      tmp= where(st.datalevel eq 1,l1_rows)
      tmp= where(st.datalevel eq 5,ql_rows)
    endif

;   first, pickup the camera field values because it is needed for "filter" and 
;   "polar" calculation when conversion to original strings is specified.
 
    camera_cnt=0
    camera_field=0
    for i= 0, n_elements(tags)-1 do begin
      ;pos= strpos(tags(i),'CAMERA')
      ;if(pos ge 0 and (pos+strlen('CAMERA')) eq strlen(tags(i))) then begin
      pos= strpos(tags(i),'DETECTOR')
      if(pos ge 0 and (pos+strlen('DETECTOR')) eq strlen(tags(i))) then begin
        camera_cnt= 1
        out= execute("camera_field= st."+tags(i))
      end
    end
 
    ntags= n_elements(tags)
    maxl= max(strlen(tags))+2
    out= execute("s= size(st."+tags(0)+")") ; get # of elements (rows)
    elem= s(n_elements(s)-1)

    if(elem gt 2000) then begin
      msg= ['Too many, '+STRTRIM(elem,2)+', rows of data to process and View, Save, or Print.', $
            '',$
            '                 Press "Yes" to use first 2000 rows.',$
            '                 Press "No"  to use all rows.']
 
      res = WIDGET_MESSAGE(msg, /Question)
      IF (STRLOWCASE(res) EQ 'yes') THEN BEGIN
        PRINT,'Will use first 2000 of '+STRTRIM(elem,2)+' rows.'
        elem= 2000
      ENDIF ELSE PRINT,'Will use all '+STRTRIM(elem,2)+' rows.'

      ;PRINT,''
      ;PRINT,'Too many, '+STRTRIM(elem,2)+', rows of data to process and View, Save, or Print.'
      ;PRINT,'Process First 2000 rows (yes,no)?'
      ;ans=''
      ;READ, ans
      ;PRINT,''
      ;IF (STRLOWCASE(STRTRIM(ans,2)) EQ 'yes') THEN BEGIN
      ;  elem= 2000
      ;  PRINT,'Processing to View, Save, or Print first 2000 rows.........'
      ;ENDIF ELSE PRINT,'Processing to View, Save, or Print all rows...........' 

    end

    tot_rows= elem

    t= strarr(ntags,elem) 
    obs_nums= STRARR(elem)+'-1'
    ;oind= WHERE(tags EQ 'OBS_PROG',ocnt)
    ;IF (ocnt GT 0) THEN obs_nums= st.obs_prog(oind)
    oind= WHERE(tags EQ 'SEB_PROG',ocnt)
    IF (ocnt GT 0) THEN obs_nums= st.seb_prog(oind)
    obs_nums= obs_nums(0:elem-1)
    If (camera_cnt GT 0) THEN camera_field= camera_field(0:elem-1)

    find= WHERE(tags EQ 'FILENAME',fcnt)
    scid= STRARR(elem)+'A'
    IF (fcnt GT 0) THEN BEGIN
      scid= st.filename(find)
      scid= scid(0:elem-1)
      scid= STRMID(scid,20,1) ; pickup A or B
    ENDIF

    exptime2= FLTARR(elem)    
    ex2ind= WHERE(tags EQ 'EXPTIME2',ex2cnt)
    IF (ex2cnt GT 0) THEN BEGIN
      exptime2= st.exptime2(ex2ind)
      exptime2= exptime2(0:elem-1)
    ENDIF

    for j=0,ntags-1 do begin
      out= execute("cur_field_vals= st."+tags(j))
      if(strlen(cur_field_vals(0)) gt 500) then begin  ;img_browse column, keep 
        cur_field_vals= strmid(cur_field_vals,0,10)+'...' ; first 10 characters.
      end
      cur_field_vals= cur_field_vals(0:elem-1) ;in case more than 2000 elements.
      t(j,*)= cur_field_vals
      t(j,*)= convert_to_orig_secchi(cur_field_vals,tags(j),camera_cnt,camera_field,elem,obs_nums,$
                                     scid,exptime2)
    end

    for j=0,ntags-1 do begin
      maxtlen= max(strlen(t(j,*)))
      taglen= strlen(tags(j))
      if(taglen lt maxtlen) then for i=1,maxtlen-taglen do tags(j)=tags(j)+' '
      if(taglen gt maxtlen) then maxtlen= taglen
      mintlen= min(strlen(t(j,*)))
      if(mintlen lt maxtlen) then t(j,*)= adjst_strarr_len(t(j,*),maxtlen)
    end

    data_rows= ''
    for j=0,ntags-1 do data_rows= data_rows+tags(j)+'   '
    equals='==================================================================='
    divider= ''
    for j=0,ntags-1 do divider= divider+strmid(equals,0,strlen(tags(j)))+'   '
    data_rows= [data_rows,divider]
;    for i=0L,elem-1 do begin
;      this_row= ''
;      for j=0,ntags-1 do this_row= this_row+t(j,i)+'   '
;      data_rows= [data_rows,this_row]
;    end
    new_cols= data_rows

    font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
    if not allow_font(font) then get_font, font

;    base= widget_base(title='Options',xsize=470,ysize=600,xoffset=10,/frame)
    ;base= widget_base(title='Options',/frame)
    base= widget_base(title='Options',/frame,xoffset=20,yoffset=20)
    b1=widget_base(base,/ROW)
    colb1= widget_base(b1,/COLUMN)
    rowb1= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(rowb1,VALUE='                             ')
    exit=  WIDGET_BUTTON(rowb1, VALUE='Exit',UVALUE='done',FONT=font,/frame)
    r1= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(colb1,VALUE='')
    com='                                                                    '
    lab=WIDGET_LABEL(colb1,VALUE=com)
    rr= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(rr,VALUE='          ')
    select= WIDGET_BUTTON(rr, UVALUE='disp',value='  View  ',FONT=font)
;    lab= WIDGET_LABEL(rr,VALUE='-> columns:')
;    select= WIDGET_TEXT(rr,UVALUE='col_sel',VALUE='all',/EDITABLE,/ALL_EVENTS)
    r2= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(r2,VALUE='          ')
    select= WIDGET_BUTTON(r2, UVALUE='save',VALUE='  Save  ',FONT=font)
    lab= WIDGET_LABEL(r2,VALUE='-> filename:')
    select= WIDGET_TEXT(r2,UVALUE='save_name',VALUE='rows.txt',/EDITABLE, $
                        /ALL_EVENTS)
    r3= widget_base(colb1,/ROW)
    lab=WIDGET_LABEL(r3,VALUE='          ')
    select= WIDGET_BUTTON(r3, UVALUE='hcopy',VALUE='  Print  ',FONT=font)
    lab= WIDGET_LABEL(r3,VALUE='-> printer: ')
    select= WIDGET_TEXT(r3,UVALUE='lp_name',VALUE='hp4-247',/EDITABLE, $
                        /ALL_EVENTS)

    lp_orient= 'Portrait'
    lptype= WIDGET_DROPLIST(r3, VALUE=['Portrait','LandScape'], UVALUE='orient')

    rc= widget_base(colb1,/COLUMN)
    com=''
    lab= WIDGET_LABEL(rc,VALUE=com)
    lab= WIDGET_LABEL(rc,VALUE=com)
    com='Available Columns:                                                   '
    lab= WIDGET_LABEL(rc,VALUE=com+'                ',FONT=font)
    com='(Only selected columns are used in View, Save, and Print.)'
    lab= WIDGET_LABEL(rc,VALUE=com)
;    b2= widget_base(colb1,xsize=280,ysize=310)
    b2= widget_base(colb1,/frame)
;    r4= widget_base(b2,/column,x_scroll_size=220,y_scroll_size=250,/SCROLL,/FRAME)
;    r4= widget_base(b2,/column,x_scroll_size=180,y_scroll_size=130,/SCROLL,/FRAME)
    r4= widget_base(b2,/column,/SCROLL,x_scroll_size=380,y_scroll_size=130,/FRAME)
    b_id= lonarr(ntags)
    tag_select= intarr(ntags) 
    old_select= tag_select
    for i=0,ntags-1 do begin
      tag_select(i)= 1 ; set tag_select defaults to selected (on) 
      row1= widget_base(r4, /ROW)
      row2 = widget_base(row1, /ROW ,/NONEXCLUSIVE)
      b_id(i) = widget_button(row2,VALUE=tags(i),UVALUE='select')
      widget_control,b_id(i),SET_BUTTON=1
    end

    r7= widget_base(colb1,/col)
    lab= WIDGET_LABEL(r7,VALUE='')
    lab= WIDGET_LABEL(r7,VALUE='Available Rows (Images):',FONT=font)
    r6= widget_base(colb1,/col,xsize=100,/frame)
    lab= WIDGET_LABEL(r6,VALUE='')
    if(num_param eq 2) then message= info
    if(dlvl gt 0) then begin 

     qpct= string(float(ql_rows)/(tot_rows)*100,'(f6.2)')
     com='QL rows:         '+string(ql_rows,'(i6)')+ '  ('+qpct+'%)'
     if(num_param eq 2) then message= [message,'  '+com]
     lab= WIDGET_LABEL(r6,VALUE=com)

     p5ct= string(float(p5_rows)/(tot_rows)*100,'(f6.2)')
     com='Level.5 rows:    '+string(p5_rows,'(i6)')+ '  ('+p5ct+'%)'
     if(num_param eq 2) then message= [message,'  '+com]
     lab= WIDGET_LABEL(r6,VALUE=com)

     zpct= string(float(l1_rows)/(tot_rows)*100,'(f6.2)')
     com='Level_1 rows:    '+string(l1_rows,'(i6)')+ '  ('+zpct+'%)' 
     if(num_param eq 2) then message= [message,'  '+com]
     lab= WIDGET_LABEL(r6,VALUE=com)

    endif else begin
     com='QL rows:              ? (datalevel not available)'
     if(num_param eq 2) then message= [message,'  '+com]
     lab= WIDGET_LABEL(r6,VALUE=com)

     com='Level0.5 rows:        ? (datalevel not available)'
     if(num_param eq 2) then message= [message,'  '+com]
     lab= WIDGET_LABEL(r6,VALUE=com)

     com='Level_1 rows:         ? (datalevel not available)'
     if(num_param eq 2) then message= [message,'  '+com]
     lab= WIDGET_LABEL(r6,VALUE=com)
    endelse
    com='Total rows:      '+string(tot_rows,'(i6)')+'          '
    if(num_param eq 2) then message= [message,'  '+com]
    lab= WIDGET_LABEL(r6,VALUE=com)
    if(num_param eq 2 and tot_rows gt 2000) then $
      message= [message,'','List of first 2000 rows:']

    widget_control,base,/realize
    struct={base:base,button_id:b_id}
    widget_control,base, SET_UVALUE=struct, SET_BUTTON= 1 ;set all buttons on

    ; se date_mod columns selection to off, if any:
    date_mods= where(strpos(strlowcase(tags),'date_mod') ne -1, dcnt)
    if(dcnt gt 0) then begin
      for i=0,dcnt-1 do $
        widget_control,b_id(date_mods(i)),SET_BUTTON=0 
      tag_select(date_mods)= 0 ; set date_mod defaults to off.
    end
    xmanager,'disp_srows',base,EVENT_HANDLER='disp_srows_event'

  endif else begin ; input is not a structure  
    msg='Input is not a valid qdb structure.'
    res=widget_message(msg,title='Bad Input')
  end

return
end
