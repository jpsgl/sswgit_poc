FUNCTION GET_CONVERT_STRING, tag, fn, english=english
;
; $Id: get_convert_string.pro,v 1.2 2008/07/24 17:26:20 esfand Exp $
;
; 080724 Ed Esfandiari - Return empty string if paramater name translation is not found.
;
; $Log: get_convert_string.pro,v $
; Revision 1.2  2008/07/24 17:26:20  esfand
; Return empty string if paramater name translation is not found
;
; Revision 1.1  2006/09/22 21:42:35  nathan
; moved from dev/database
;
; Revision 1.2  2006/09/22 17:04:53  nathan
; add Id and Log
;

  OPENR,unit,fn,/get_lun
  done= 0 ;false
  str=''
  WHILE (NOT EOF(unit) AND NOT done) DO BEGIN
    READF,unit,str
    IF (STRPOS(str,tag) GE 0) THEN done=1
  ENDWHILE
 
  CLOSE,unit
  FREE_LUN,unit

  IF (STRPOS(str,tag) LT 0) THEN RETURN,'no translation' 

  toks= STR_SEP(str,',') 
  IF (KEYWORD_SET(english)) THEN $
    RETURN, STRTRIM(toks(2),2)   $   ; return english version of mnemonic
  ELSE                           $
    RETURN, STRTRIM(toks(1),2)       ; retrun mnemonic conversion string

END
