;+
; Project     :	STEREO - SECCHI
;
; Name        :	SECCHI_VSO_INGEST
;
; Purpose     :	Retrieve SECCHI data from the Virtual Solar Observatory
;
; Category    :	STEREO, SECCHI, Catalog
;
; Explanation : This routine uses VSO search to search for SECCHI data served
;               by the STEREO Science Center, and then copy it to the local
;               machine.  The data are first copied to a temporary directory.
;               If a local archive is set up, recognized by one of the standard
;               SECCHI environment variables (e.g. $secchi or $SECCHI_LZ), then
;               the routine SCCINGEST will be run against this directory.
;
; Syntax      :	SECCHI_VSO_INGEST, DATE1  [, DATE2 ]
;
; Examples    :	SECCHI_VSO_INGEST, '2008-10-01 12', '2008-10-01 14', TEL='COR1'
;               SECCHI_VSO_INGEST, '2008-10-01', TELESCOPE='EUVI', WAVE=304
;
; Inputs      :	DATE1 = The start date/time to search over.
;
; Opt. Inputs :	DATE2 = The end date/time.  If not passed, then the entire day
;                       given by DATE1 will be searched.
;
; Outputs     :	The relevant files are copied over.
;
; Opt. Outputs:	None.
;
; Keywords    :	SPACECRAFT = Either "Ahead" or "Behind", or one of the other
;                            recognized variants supported by
;                            PARSE_STEREO_NAME.  If not passed, then both
;                            spacecraft are searched.
;
;               TELESCOPE  = "EUVI", "COR1", "COR2", "HI1" or "HI2".  If not
;                            passed, then all telescopes are searched.
;
;               WAVELENGTH = EUVI wavelength, 171, 195, 284, or 304.
;
;               OUT_DIR    = Directory to receive FITS files.  If this keyword
;                            is used, then SCCINGEST is not called.
;
;               QUIET      = If set, then various informational messages are
;                            not printed.
;
;               NOPROMPT   = If set, then download without prompting.
;
; Calls       :	PARSE_STEREO_NAME, ANYTIM2UTC, UTC2TAI, SCC_DATA_PATH,
;               BREAK_FILE, MK_TEMP_DIR, DELVARX, TAI2UTC, VSO_SEARCH, UNIQ,
;               VSO_GET, SCCINGEST, XANSWER
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 29-Oct-2008, William Thompson, GSFC
;               Version 2, 31-Oct-2008, WTT, move mk_dir to later in program
;               Version 3, 04-Nov-2008, WTT, use XANSWER instead of ASK
;               Version 4, 16-Mar-2010, WTT, fixed bug creating temp dir
;               Version 5, 18-Oct-2016, WTT, Call VSO_GET(/USE_NETWORK)
;                                            Rename variable LIST to LST
;
; Contact     :	WTHOMPSON
;-
;
pro secchi_vso_ingest, date1, date2, spacecraft=spacecraft, $
                       telescope=telescope, wavelength=wavelength, $
                       out_dir=out_dir, quiet=quiet, noprompt=noprompt, $
                       _extra=_extra
;
on_error, 2
;
if n_params() eq 0 then message, $
  'Syntax: SECCHI_VSO_INGEST, DATE1  [, DATE2]  [, keywords ... ]'
;
;  Parse the spacecraft name.
;
delvarx, source
if n_elements(spacecraft) eq 1 then $
  source = parse_stereo_name(spacecraft, ['STEREO_A', 'STEREO_B'])
;
;  Parse the search dates.  If only one date was passed, then search the entire
;  day.
;
utc1 = anytim2utc(date1)
if n_params() eq 2 then utc2 = anytim2utc(date2) else begin
    utc1.time = 0
    utc2 = utc1
    utc2.mjd = utc2.mjd + 1
endelse
tai1 = utc2tai(utc1)
tai2 = utc2tai(utc2)
;
;  Search in increments of four hours until the entire time span is covered.
;
delvarx, lst
t1 = tai1
t2 = (t1 + 14400d0) < tai2
while t1 lt tai2 do begin
    d1 = tai2utc(t1, /ccsds)
    d2 = tai2utc(t2, /ccsds)
    if not keyword_set(quiet) then begin
    	help,source,telescope,wavelength
    	print, 'Searching between ' + d1 + ' and ' + d2
    endif
    
    l = vso_search(d1, d2, provider='ssc', instrument='secchi', $
                   source=source, detector=telescope, wave=wavelength, $
                   quiet=quiet, _extra=_extra)
    
    if datatype(l) eq 'STC' then begin
        if n_elements(lst) eq 0 then lst = l else lst = [lst, l]
    endif
    t1 = t2
    t2 = (t1 + 14400d0) < tai2
endwhile
;
if n_elements(lst) eq 0 then begin
    print, 'No files found'
    return
endif
;
;  Remove any duplicates from the lst.
;
s = sort(lst.fileid)
lst = lst[s]
u = uniq(lst.fileid)
lst = lst[u]
;
;  Make sure the user wants to copy over these files.
;
if not keyword_set(noprompt) then begin
    text = ['A total of ' + ntrim(n_elements(lst)) + ' results were found.', $
            'Do you want to copy these files over?']
    if not xanswer(text) then return
endif
;
;  Form the name of the top output directory, and create a temporary directory
;  underneath this directory.  The keyword OUT_DIR overrides this.
;
if n_elements(out_dir) eq 1 then begin
    if not dir_exist(out_dir) then begin
        mk_dir, out_dir
        if not dir_exist(out_dir) then begin
            print, 'Unable to create directory ' + out_dir
            goto, create_temporary_directory
        endif
    endif
    temp_dir = out_dir
    do_sccingest = 0
    if not keyword_set(quiet) then $
      print, 'Copying files to directory ' + temp_dir
end else begin
    delete_if_empty = 1
    path = scc_data_path('a')
    break_file, path, disk, dir
    path = disk + dir
    if not dir_exist(path) then mk_dir, path
    mk_temp_dir, path, temp_dir, err=err
    do_sccingest = 1
    if (err ne '') or (temp_dir eq '') then begin
create_temporary_directory:
        path = getenv('HOME')
        mk_temp_dir, path, temp_dir
        do_sccingest = 0
    endif
    if not keyword_set(quiet) then $
      print, 'Copying files to temporary directory ' + temp_dir
endelse
;
;  Copy over the files to the temporary directory.
;
status = vso_get(lst, out_dir=temp_dir, /force, quiet=quiet, /use_network, $
                 _extra=_extra)
;
;  Run SCCINGEST over the temporary directory, and then delete the (presumably
;  empty) temporary directory.
;
if do_sccingest then begin
    if not keyword_set(quiet) then $
      print, 'Ingesting data files into local archive'
    sccingest, temp_dir
    if not keyword_set(quiet) then $
      print, 'Deleting temporary directory ' + temp_dir
    file_delete, temp_dir
endif
;
end
