
PRO SECCHI_DN_CONVERT, dn_arr, cnv_arr, dn_cnvt, cnv_units
; $Id: secchi_dn_convert.pro,v 1.1 2006/09/22 21:42:35 nathan Exp $
;
; Purpose: SECCHI HKB Telemetry Analog Conversion Definitions:
;
; $Log: secchi_dn_convert.pro,v $
; Revision 1.1  2006/09/22 21:42:35  nathan
; moved from dev/database
;
; Revision 1.2  2006/09/22 17:07:37  nathan
; add Id and Log
;

   alg_st= {INST_BUS_VOLT_CONV:        0.0122,   $
            SEB_BUS_LOW_VOLT_CONV:     0.002442, $
            SEB_BUS_HI_VOLT_CONV:      0.0049,   $
            SEB_BUS_HI_NEG_VOLT_CONV: -0.0049,   $
            INST_BUS_CUR_CONV:         0.00099,  $
            SEB_BUS_CUR_CONV:          0.0023,   $
            MEB_CUR_CONV:              0.00016,  $
            SCIP_MOTOR_CUR_CONV:       0.00095,  $
            RAW_CONV:                  1.0,      $
            INST_PRT_TEMP_CNV:         {const: -243.1244,      $
                                        a:        0.2306895,   $ 
                                        b:        1.206202e-5, $
                                        f:        0.9768},     $
            INST_YSI_TEMP_CNV:         {const: -273.15,             $
                                        a:        0.001470873889,   $ 
                                        b:        0.00023779052299, $ 
                                        c:        1.03257793629e-7, $ 
                                        f:        9.768} $
           }  
             

   dn_cnvt= FLOAT(dn_arr)
   cnv_units= STRARR(N_ELEMENTS(dn_arr))

   hidr= WHERE(cnv_arr EQ 'HB_HIDOR_ENC_DSC', ind)
   IF (ind GT 0) THEN BEGIN
     cnv_units(hidr)= SECCHI_DOORS_CONVERT(dn_arr(hidr),'HB_HIDOR_ENC_DSC')
   ENDIF
   scipdr= WHERE(cnv_arr EQ 'HB_SCIPDOR_ENC_DSC', ind)
   IF (ind GT 0) THEN BEGIN
     cnv_units(scipdr)= SECCHI_DOORS_CONVERT(dn_arr(scipdr),'HB_SCIPDOR_ENC_DSC')
   ENDIF
   dr_encl= WHERE(cnv_arr EQ 'HB_DOOR_ENCSEL_DSC', ind)
   IF (ind GT 0) THEN BEGIN
     cnv_units(dr_encl)= SECCHI_DOORS_CONVERT(dn_arr(dr_encl),'HB_DOOR_ENCSEL_DSC')
   ENDIF

   vlt= WHERE(cnv_arr EQ 'INST_BUS_VOLT_CONV', ind)
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(vlt)= alg_st.INST_BUS_VOLT_CONV * dn_arr(vlt)
     cnv_units(vlt)= 'Voltage (V)'
   ENDIF
   vlt= WHERE(cnv_arr EQ 'SEB_BUS_LOW_VOLT_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(vlt)= alg_st.SEB_BUS_LOW_VOLT_CONV * dn_arr(vlt)
     cnv_units(vlt)= 'Voltage (V)'
   ENDIF
   vlt= WHERE(cnv_arr EQ 'SEB_BUS_HI_VOLT_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(vlt)= alg_st.SEB_BUS_HI_VOLT_CONV * dn_arr(vlt)
     cnv_units(vlt)= 'Voltage (V)'
   ENDIF
   vlt= WHERE(cnv_arr EQ 'SEB_BUS_HI_NEG_VOLT_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(vlt)= alg_st.SEB_BUS_HI_NEG_VOLT_CONV * dn_arr(vlt)
     cnv_units(vlt)= 'Voltage (V)'
   ENDIF

   cur= WHERE(cnv_arr EQ 'INST_BUS_CUR_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(cur)= alg_st.INST_BUS_CUR_CONV * dn_arr(cur)
     cnv_units(cur)= 'Current (Amp)'
   ENDIF
   cur= WHERE(cnv_arr EQ 'SEB_BUS_CUR_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(cur)= alg_st.SEB_BUS_CUR_CONV * dn_arr(cur)
     cnv_units(cur)= 'Current (Amp)'
   ENDIF
   cur= WHERE(cnv_arr EQ 'MEB_CUR_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(cur)= alg_st.MEB_CUR_CONV * dn_arr(cur)
     cnv_units(cur)= 'Current (Amp)'
   ENDIF
   cur= WHERE(cnv_arr EQ 'SCIP_MOTOR_CUR_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(cur)= alg_st.SCIP_MOTOR_CUR_CONV * dn_arr(cur)
     cnv_units(cur)= 'Current (Amp)'
   ENDIF

   raw= WHERE(cnv_arr EQ 'RAW_CONV', ind) 
   IF (ind GT 0) THEN BEGIN
     dn_cnvt(raw)= alg_st.RAW_CONV * dn_arr(raw)
     cnv_units(raw)= 'Current (Amp)'
   ENDIF
 
   prt= WHERE(cnv_arr EQ 'INST_PRT_TEMP_CNV', ind)
   IF (ind GT 0) THEN BEGIN
     r = alg_st.INST_PRT_TEMP_CNV.f * dn_arr(prt)
     dn_cnvt(prt)= alg_st.INST_PRT_TEMP_CNV.const + $
                alg_st.INST_PRT_TEMP_CNV.a * r + $
                alg_st.INST_PRT_TEMP_CNV.b * r^2.0
     cnv_units(prt)= 'Temp (Deg C)'
   ENDIF 
   ysi= WHERE(cnv_arr EQ 'INST_YSI_TEMP_CNV', ind)
   IF (ind GT 0) THEN BEGIN
     r = alg_st.INST_YSI_TEMP_CNV.f * dn_arr(ysi)
     dn_cnvt(ysi)= alg_st.INST_YSI_TEMP_CNV.const + $
                   (alg_st.INST_YSI_TEMP_CNV.a + $
                   alg_st.INST_YSI_TEMP_CNV.b * $
                   alog(r) + alg_st.INST_YSI_TEMP_CNV.c * $
                   ((alog(r))^3.0))^(-1.0)

     ; can't take log of 0 or negatives (Floating illegal operand is returned);
     ; set to 0, if any:

     bad= WHERE(r LE 0, bcnt)
     IF (bcnt GT 0) THEN dn_cnvt(ysi(bad))= 0.0

     cnv_units(ysi)= 'Temp (Deg C)'
   ENDIF      

  RETURN
END
