;+
;$Id: scclister.pro,v 1.40 2015/11/30 18:37:45 mcnutt Exp $
;
; Project     : SECCHI 
;
; Name        : SCCLISTER
;
; Purpose     : Get a list of selected images.
;
; Category    : Widget 
;
; Explanation : This widget function will return an IDL structure of string
;               arrays with filenames and absolute path of selected images.
;               It will return an empty string if no file is found.
;
; Use         : IDL>list=scclister()
;
; Examples    : IDL>list=scclister()
;
; Inputs      : None
;
; Opt. Inputs : None
;   	    	date: '20070804' or '20070804..20070805'
;   	    	telescope:          'EUVI', 'COR1', 'COR2', 'HI_1', or 'HI_2'

; Outputs     : An IDL structure of string arrays (or an empty string
;               if no files found).
;
; Opt. Outputs: IDL save set of structure with fnames and info tags. Each
;               tag is an string array. 
;
; Keywords    : 
;		/MODAL	    For calling from other widgets
;    	    	/HELP	    Print usage info
;   	    	/AORB	    Give option for ST-A or ST-B only, not both (with wscc_mkmovie.pro)
;   	    	/VERBOSE
;		scid=scid, imgtype=imgtype, src=src, size=size, exp=exp, osnum=osnum, 
;		dest=dest, sebprog=sebprog, filt=filt, polar=polar, compression=compression,
;		sort_type=sort_type, sort_by=sort_by, data_level=data_level,data_src=data_src,
;
; Common      : info1, scraft1, fdisplay1, queries1, sorts1, savefiles1
;
; Restrictions:                                  
;
;		Environment variables used:
;                       secchi
;
; Side effects: Not known
;
; History     : Ed Esfandiari November 2005 
;               First Version.
;               Ed Esfandiari  02/03/06 - Added Error Checking."
;               Ed Esfandiari  02/06/06 - Added LED to title (for cal LED images)
;               Ed Esfandiari  03/21/06 - Added notSW to list of APID selection.
;               Ed Esfandiari  03/22/06 - Added SERI and notSERI to list of Prog selection.
;               Ed Esfandiari  05/30/06 - Changed Data Type Names and added more help info.
;               Ed Esfandiari  06/01/06 - Added lz, pb, and rt as data sources.
;               William Thompson, 19-June-2006, Changed to call scc_data_path.
;               Ed Esfandiari  10/14/06 - Display selected SpaceCraft, Data Level, Data
;                                         Source, and Data Type from previous scclister calls.
;               Ed Esfandiari  01/19/07 - Added option to save point-and-click files to ./myfiles.list
;               Ed Esfandiari  01/29/07 - Re-arranged query labels to match query fields for all
;                                         platforms and screen sizes. Also added COR polar positions
;                                         to query.
;               Ed Esfandiari  03/26/07 - Pick up both SW and SWX destinations for SW query.
;               Ed Esfandiari  03/27/07 - Added place holder to display and query on compressions.
;               Ed Esfandiari  03/27/07 - Changed Point and Click to display selected items in draw
;                                         widget instead of appending to ./myfiles.list file.
;               Ed Esfandiari  04/09/07 - For .img summary, first look for and use .img.eu, .img.co, or 
;                                         img.hi file (depending on telescope) before looking for .img file.
;               Ed Esfandiari  04/12/07 - search latest summary files (*.eu, *.c1, *.c2, *.h1, *.h2). Also
;                                         modified code to run much faster.
;               Ed Esfandiari  04/18/07 - Added more enhancements.
;               Ed Esfandiari  06/14/07 - Throw out files that exist in directory but not in
;                                         summary file.
;               Ed Esfandiari  09/10/07 - Added non-graphical interface for batch mode processing. Use
;                                         /help keyword for list of parameters and keywords:
;                                         idl> files= sccclister(/help)
;               Ed Esfandiari  10/03/07 - Added 'TB (double)+polarized' option and also added data_src, 
;                                         data_level, sort keywords for non-graphical batch processing.
;               Ed Esfandiari  12/10/07 - Added drop-down/edit statement for file selection.
;
;               Ed Esfandiari  02/13/08 - Added NRL-only TBPP (TotalBrightess/PercentPolarized) option.
;               Ed Esfandiari  02/14/08 - Added polarizer sub-selects for 1001 to 1004 options and 
;                                         updated the Help page.
;               Ed Esfandiari  04/15/08 - Fixed TB (double)+Polarized bug (When both SC were 
;                                         selected, SC-B .img files were not returned).
;
;     %H% %W% LASCO NRL IDL LIBRARY
;
;$Log: scclister.pro,v $
;Revision 1.40  2015/11/30 18:37:45  mcnutt
;added yearly directory if P0
;
;Revision 1.39  2013/07/17 11:18:41  mcnutt
;added fix to subquery to check for widget before defining sub set at bottom:
;
;Revision 1.38  2011/08/18 20:07:05  nathan
;added clarifying words to img type menu, and allow previous options for type input
;
;Revision 1.37  2011/08/04 18:22:49  mcnutt
;corrected resets if qclist is not defined
;
;Revision 1.36  2011/08/02 21:31:19  nathan
;add polar 1001+0 option and re-order combobox list
;
;Revision 1.35  2011/08/02 13:15:19  mcnutt
;added remove subset changed image type to dir names ie img seq pol
;
;Revision 1.34  2010/08/26 20:55:12  nathan
;fix info message sc_dr
;
;Revision 1.33  2010/07/06 16:39:50  nathan
;do not use suffix in filename comparison
;
;Revision 1.32  2010/07/02 18:51:12  nathan
;removed all use of / in path for windows compatibility
;
;Revision 1.31  2010/03/08 16:19:42  nathan
;accept .gz files in file list output
;
;Revision 1.30  2010/03/03 22:35:49  nathan
;really fix scroll bars
;
;Revision 1.29  2010/03/03 22:22:50  nathan
;fix scroll bars of widget
;
;Revision 1.28  2010/03/03 21:44:15  nathan
;mods to check_matches and how missing files are handled
;
;Revision 1.27  2010/02/19 21:46:46  nathan
;print more info if FITS files missing
;
;Revision 1.26  2009/11/03 23:41:39  nathan
;if end date before begin date then assume begin date
;
;Revision 1.25  2009/03/12 18:54:25  nathan
;only check first 25 chars of file to allow for .gz files
;
;Revision 1.24  2009/03/12 18:33:44  nathan
;use regexp filtering filenames
;
;Revision 1.23  2008/09/15 18:54:08  nathan
;committed 9/3/08 change
;
;
; nbr, 9/3/08 - commented out call to yymmdd.pro
;
;Revision 1.22  2008/08/08 23:18:28  nathan
;added /AORB restriction for wscc_mkmovie.pro; doc keywords
;
;Revision 1.21  2008/06/24 20:41:13  colaninn
;added modal keyword so program can be call from other widgets
;
;Revision 1.20  2008/04/15 19:03:50  esfand
;AEE - Fixed TB (double)+Polarized bug
;
;Revision 1.19  2008/02/14 18:27:07  esfand
;Added NRL-only TBPP options. Also added polar 1001..1004 - AEE
;
;Revision 1.18  2007/12/10 20:39:18  esfand
;Added drop-down/edit file selection help
;
;Revision 1.17  2007/10/03 16:40:33  esfand
;initialized rind - AEE
;
;Revision 1.16  2007/10/03 13:08:12  esfand
;added data_src, data_level, sort keywords and also "TB (double)+polarized" combo - AEE
;
;Revision 1.15  2007/09/10 19:53:38  esfand
;AEE - added non-graphical interface (use /help for details)
;
;Revision 1.14  2007/08/28 21:11:09  nathan
;add debug messages
;
;Revision 1.13  2007/06/14 19:37:05  esfand
;throw out files found in directoy but not in summary file
;
;Revision 1.12  2007/04/18 16:23:06  esfand
;Ed E. Added new ehancements
;
;Revision 1.11  2007/04/12 15:25:45  esfand
;faster search and using per-tele summary files
;
;Revision 1.10  2007/04/10 19:28:50  esfand
;also search img.tt summary files
;
;Revision 1.9  2007/03/30 22:05:22  thompson
;Added calls to allow_font() and get_font
;
;Revision 1.8  2007/03/28 12:58:30  esfand
;removed debug info - AEE
;
;Revision 1.7  2007/03/27 19:59:25  esfand
;Added compression and query place-holder and directed point and clickoutput to draw widget insead of myfiles.list - AEE
;
;Revision 1.6  2007/03/26 20:11:24  esfand
;use SW+SWX in a SW query
;
;Revision 1.5  2007/01/29 16:15:13  esfand
;AEE - added COR pol. positions to query search
;
;Revision 1.4  2007/01/29 14:57:39  esfand
;AEE - added COR pol. positions to query search
;
;Revision 1.3  2007/01/29 14:53:07  esfand
;AEE - added COR pol. positions to query search
;
;Revision 1.2  2007/01/19 20:50:38  esfand
;AEE - added option to point-and-click-and-save filenames
;
;Revision 1.1  2007/01/16 21:10:41  nathan
;moved from nrl_lib/dev/database
;
;Revision 1.11  2006/11/14 19:33:47  esfand
;Display previously selected droplist items - AEE
;
;Revision 1.10  2006/06/19 19:38:53  thompson
;*** empty log message ***
;
;Revision 1.9  2006/06/09 15:13:59  esfand
;changed common block names
;
;Revision 1.8  2006/06/01 19:32:37  esfand
;Added lz, pb, and rt as data sources
;
;Revision 1.7  2006/05/30 21:12:24  esfand
;minor changes
;
;Revision 1.6  2006/02/06 19:12:19  esfand
;added LED to display title line
;
;Revision 1.4  2006/02/03 21:27:35  esfand
;added error checking
;
;Revision 1.3  2006/01/26 15:57:41  esfand
;minor changes in some routines
;
;Revision 1.2  2005/11/16 19:57:40  esfand
;expanded years to 2030
;
;Revision 1.1  2005/11/16 18:56:04  esfand
;Secchi lister - First Version - AEE
;
;
;-

FUNCTION CHECK_MATCHES, files, uv , fmsg, DONE=DONE

COMMON info1,text,text1,text2,tmps,tmpp,new_tmpp,sipath,s_sflag,inst,path,filter,wave_set,hnh, sec_sflag, sec_inst
COMMON scraft1, sc_sel, sclist, sel_id, sec_select, sec_lvl, lvl_ind, dtype, p_type, p_ind, $
               sec_level, sources, src_used, src_ind
COMMON fdisplay1, draw, selfiles, dobs1, dobs2, cad1, cad2, cad_sel, cadence
COMMON queries1, qtel, qexpt1, qexpt2, qxsize, qysize, qfltr, qpolr, qprog, qosn, $
                qry, rmvsub, reset, qtelval, qexpt1val, qexpt2val, qxsizeval, qysizeval, $
                qfltval, qpolval, qprogval, qosnval, qclist, qflist, qplist, $
                qprglist, matches, orig_matches, sc_dir, orig_files, orig_msg, $
                qdstlist,qdstval,qdst, qcmplist,qcmpval,qcmp
COMMON sorts1, sortid,sortit,sort_mtd,sortlist,last_sort_mtd,sort_order,sorder, debugon
COMMON savefiles1, saveid,savename,sname

;help,orig_matches,matches,files
;stop

  PRINT,'Checking and processing summary file matches....'
  fnames= ''

  add1= 0 
  IF (p_ind EQ 4) THEN add1=1 
  dotpos=strpos(matches[0],'|')
  
  IF (matches(0) NE '') THEN BEGIN

    ;fnames= STRMID(matches,0,25)
    fnames= STRMID(matches,0,dotpos)
    fnames= STRCOMPRESS(fnames,/remove_all)
    ; Filenames of found files without suffix so .gz files are accepted
    nfiles= N_ELEMENTS(files)
    
    IF (nfiles NE N_ELEMENTS(matches)) THEN BEGIN
    	print,'Mismatch found between FITS file list and Summary file--'
    	kfiles= ''
    	lastind=0
    	FOR i= 0, nfiles-1 DO BEGIN
	    break_file,files[i],di,pa,ro,su
	    ind = where(fnames EQ ro+su,cnt)
            IF cnt GT 0 THEN kfiles= [kfiles,files[i]] ELSE print,'Omitting ',files[i]
    	ENDFOR
    	IF (N_ELEMENTS(kfiles) GT 1) THEN  kfiles= kfiles(1:*) 
    	files=kfiles
    ENDIF ELSE kfiles= files

   aind= WHERE(STRPOS(fnames,'A.f') gt -1, acnt)
   ;aind= WHERE(STRMID(fnames,20+add1,1) EQ 'A', acnt)
   bind= WHERE(STRPOS(fnames,'B.f') gt -1, bcnt)
   ;bind= WHERE(STRMID(fnames,20+add1,1) EQ 'B', bcnt) 
   IF (debugon) THEN help,aind,bind,sc_sel,kfiles,files,matches

   CASE sc_sel OF
    0: BEGIN ; A
         flist= {SC_A:kfiles[aind]}
	 ;files=files[aind]
       END 
    1: BEGIN ; B
         flist= {SC_B:kfiles[bind]} 
	 ;files=files[bind]
       END 
    2: BEGIN ; A+B all images
         IF (acnt GT 0 AND bcnt EQ 0) THEN BEGIN 
            flist= {SC_A: kfiles(aind)}
	    ;files=files[aind]
         ENDIF
         IF (acnt EQ 0 AND bcnt GT 0) THEN BEGIN 
            flist= {SC_B: kfiles(bind)}
	    ;files=files[bind]
         ENDIF
         IF (acnt GT 0 AND bcnt GT 0) THEN BEGIN 
            flist= {SC_A: kfiles(aind), SC_B: kfiles(bind)}
	    abind=[aind,bind]
	    sx=sort(abind)
	    ;files=files[abind[sx]]
         ENDIF
       END
    3: BEGIN ; A+B syched images
         IF (acnt EQ 0 OR bcnt EQ 0) THEN BEGIN 
           nfiles= 0
           flist= ''
           files= ''
           matches= ''
           fmsg= 'Found no synced files.'
           IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg
           PRINT,'Found no synced files.'
         ENDIF ELSE BEGIN  
           ; keep fts name without A.fts (i.e. 20080101_001110_0B4c2[A.fts])
           af= STRMID(fnames(aind),0,20)
           ; keep fts name without B.fts 
           bf= STRMID(fnames(bind),0,20)

           ; now check to see where af matches bf to pickup syched files:
           async=''
           bsync=''

           matched=''
           FOR k= 0L, N_ELEMENTS(af)-1 DO BEGIN
             mind= WHERE(bf EQ af(k), mcnt)
             IF (mcnt GT 0) THEN BEGIN
               async= [async,kfiles(aind(k))]
               bsync= [bsync,kfiles(bind(mind(0)))] 
               matched= [matched,matches(aind(k)),matches(bind(mind(0)))]
             ENDIF 
           ENDFOR

           nfiles= N_ELEMENTS(async)
           IF (nfiles GT 1) THEN BEGIN
             ; remove first (dummy) entry:
             async= async(1:*)
             bsync= bsync(1:*)
             nfiles= nfiles - 1

             matched= TEMPORARY(matched(1:*))
             matches= TEMPORARY(matched) ; matched becomes undefined
             ;help,matched
              flist= {SC_A:async, SC_B:bsync}

          ;   res= execute('flist= {'+sc_arr(0)+':async,'+ $
          ;                           sc_arr(1)+':bsync}')

             files= [async(0),bsync(0)]
             FOR sf= 1, N_ELEMENTS(async)-1 DO $
               files= [files,async(sf),bsync(sf)]
           ENDIF ELSE BEGIN
            nfiles= 0
            flist= ''
            fmsg= 'Found no synced files.'
            PRINT, fmsg
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg
            files= ''
            matches= ''
           ENDELSE
         ENDELSE
        END 
    ENDCASE
  ENDIF ELSE BEGIN
    nfiles= 0
    flist= ''
    fmsg1=''
    fmsg2=''
    ftag=strmid(files[0],0,5)
    IF ftag EQ 'nofit' THEN BEGIN
    	fmsg='No FITS files found.'
	fmsg1='Contact Nathan or your data manager'
	fmsg2='with the telescope and dates you are seeking.'
    ENDIF ELSE $
    IF ftag EQ 'nosum' THEN BEGIN
    	fmsg='No summary files found.'
    ENDIF ELSE $
    	fmsg= 'Found no files.'
    PRINT, fmsg
    print, fmsg1
    print,fmsg2
    IF uv.draw GT 0 THEN BEGIN
	    WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg 
            WIDGET_CONTROL,uv.lab1,SET_VALUE= fmsg1 
            WIDGET_CONTROL,uv.lab2,SET_VALUE= fmsg2
    ENDIF
  ENDELSE
 

  sipath=''

  IF (nfiles GT 0) THEN BEGIN
    sc_dr= STRUPCASE(sc_dir) 
    sct= sc_dr(0)

    IF (sc_sel GT 1) THEN BEGIN
      sct= '('+STRTRIM(acnt,2)+' '+sct+' and '+ $
         STRTRIM(bcnt,2)+' '+sc_dr(1)+')'
      IF (sc_sel EQ 3) THEN sct= sct+' synched'
    
    ENDIF
    fmsg= 'Found total of '+STRTRIM(nfiles,2)+' '+sct+' files.'

    PRINT, fmsg

    ;WIDGET_CONTROL,draw,SET_VALUE= files

    ;WIDGET_CONTROL,uv.lab,SET_VALUE=" Found "+STRTRIM(nfiles,2)+" files."
    IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg

    sipath=''
    IF (nfiles GT 0) THEN BEGIN
       sipath= flist 
    ENDIF 
  ENDIF

;help,files, flist, sipath, matched, uv.draw
;stop

  IF uv.draw GT 0 THEN WIDGET_CONTROL,draw,SET_VALUE= matches

  ;cams=STRMID(fnames,STRPOS(fnames(0),'.fts')-3,2)
  ;IF (N_ELEMENTS(UNIQ(cams)) EQ 1) THEN BEGIN
    ; single camera files, can use cadence to subselet:
    ;WIDGET_CONTROL,cad1,SENSITIVE=1
    ;WIDGET_CONTROL,cad2,SENSITIVE=1
    ;WIDGET_CONTROL,cadence,SENSITIVE=1
  ;ENDIF ELSE BEGIN
    ; multi cameras files  can't use cadence to subselet:
    ;WIDGET_CONTROL,cad1,SENSITIVE=0
    ;WIDGET_CONTROL,cad2,SENSITIVE=0
    ;WIDGET_CONTROL,cadence,SENSITIVE=0
  ;ENDELSE

  ;SPAWN,'date'
  PRINT,'Checking Matches Done.'

  IF (KEYWORD_SET(DONE)) THEN $
    RETURN,sipath $
  ELSE $
    RETURN,matches
END



FUNCTION GET_FILES, year,month,day,iby,ibm,ibd,iey,iem,ied, $
                    sec_inst, sel_id, sc_sel, nfiles, fmsg, lvl, $
                    dtype, p_type, p_ind, draw, selfiles, uv, src_used

COMMON queries1
COMMON sorts1
COMMON savefiles1
COMMON pmatched1, vcyr,vcmo,vcdy,vchr,vcmn,vcse,cyrid,cmoid,cdyid,chrid,cmnid,cseid,fsid, search_string,cfsid

;help,uv,uv.draw
;stop
  sind= WHERE(sel_id EQ 1, scnt)
  IF (scnt EQ 0) THEN BEGIN
    PRINT,'Please select instrument(s).'
    fmsg='Please Select Instrument(s).'
    IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg
    
    nfiles= 0
    RETURN,''
  ENDIF

  add1= 0
  IF (p_ind GT 3) THEN add1= 1
  tinst= STRLOWCASE(sec_inst(sind))

; print, tinst
;help,sec_inst,sel_id
;stop
;help,sc_sel
 
  CASE sc_sel OF
    0: BEGIN
         sc_arr= ['secchi_a'] 
         sc_dir= ['a']
       END
    1: BEGIN 
         sc_arr= ['secchi_b']
         sc_dir= ['b']
       END
    2: BEGIN ; all images
         sc_arr= ['secchi_a','secchi_b'] 
         sc_dir= ['a','b']
       END
    3: BEGIN ; only synched images
         sc_arr= ['secchi_a_sync','secchi_b_sync'] 
         sc_dir= ['a','b']
       END
  ENDCASE

  fcnt= LONARR(N_ELEMENTS(sc_arr))

;  print
;  message,'Please wait for data files to be found...',/inform 

  ;help,sc_dir
  ;print,sc_dir

  butc= STR2UTC(year(iby)+'/'+month(ibm)+'/'+day(ibd)) 
  eutc= STR2UTC(year(iey)+'/'+month(iem)+'/'+day(ied))
  
  IF eutc.mjd LT butc.mjd THEN eutc=butc
  bmjd= butc.mjd
  days= eutc.mjd - butc.mjd 

  ; use regular expressions using wild characters to look for matching
  ; fts filenames:

  ; Build date_time part of filenames:

goto, skip_match_fts

  match_fts= ''
  FOR i=0,3 DO BEGIN
    ch= STRMID(vcyr,i,1)
    IF (ch EQ '?') THEN $
      match_fts= match_fts+'[0-9]'  $
    ELSE $
      match_fts= match_fts+'['+ch+']'
  ENDFOR
  FOR i=0,1 DO BEGIN
    ch= STRMID(vcmo,i,1) 
    IF (ch EQ '?') THEN $
      match_fts= match_fts+'[0-9]'  $
    ELSE $
      match_fts= match_fts+'['+ch+']'
  ENDFOR
  FOR i=0,1 DO BEGIN
    ch= STRMID(vcdy,i,1) 
    IF (ch EQ '?') THEN $
      match_fts= match_fts+'[0-9]'  $
    ELSE $
      match_fts= match_fts+'['+ch+']'
  ENDFOR
  match_fts= match_fts+'_'
  FOR i=0,1 DO BEGIN
    ch= STRMID(vchr,i,1) 
    IF (ch EQ '?') THEN $
      match_fts= match_fts+'[0-9]'  $
    ELSE $
      match_fts= match_fts+'['+ch+']'
  ENDFOR
  FOR i=0,1 DO BEGIN
    ch= STRMID(vcmn,i,1)
    IF (ch EQ '?') THEN $
      match_fts= match_fts+'[0-9]'  $
    ELSE $
      match_fts= match_fts+'['+ch+']'
  ENDFOR
  FOR i=0,1 DO BEGIN
    ch= STRMID(vcse,i,1)
    IF (ch EQ '?') THEN $
      match_fts= match_fts+'[0-9]'  $
    ELSE $
      match_fts= match_fts+'['+ch+']'
  ENDFOR

  ; Now add datatype(s) needed for level 0.5 data:

  ;IF (lvl EQ 'L1') THEN BEGIN
  ;  match_fts= match_fts+'_*.fts'
  ;ENDIF ELSE BEGIN
  ;  match_fts= match_fts+'_[' 
  ;  ftype= STR_SEP(dtype(p_ind),'+')
  ;  FOR k= 0, N_ELEMENTS(ftype)-1 DO BEGIN
  ;    ch= STRLOWCASE(STRMID(ftype(k),0,1))
  ;    match_fts= match_fts+ch+','
  ;  ENDFOR
  ;  match_fts= STRMID(match_fts,0,STRLEN(match_fts)-1)+']*.fts'
  ;ENDELSE

  match_fts= match_fts+'_*.fts'
skip_match_fts:
  match_fts=search_string+'*'	; Get .gz files too.

;help,match_fts
print,'Matching ',match_fts

  files=[""]
  matches=[""]
  missing=''
  tot_cnt= 0 
  mind= -1 

  PRINT,'' 
  ;SPAWN,'date'

  imgseq=''
  IF (p_type(p_ind) EQ 'img_seq') THEN imgseq='img_seq'
  IF (p_type(p_ind) EQ 'img_pol') THEN imgseq='img_pol'
  prev_pind= p_ind

  FOR j= 0, N_ELEMENTS(sc_dir)-1 DO BEGIN

;
;  Call SCC_DATA_PATH instead of using the $secchi environment variable
;  directly.  -- William Thompson, 19-June-2006, GSFC
;
    IF (lvl EQ 'L1') THEN $
      data_path = scc_data_path(sc_dir(j), level=lvl) $
      ;;data_path = GETENV('secchi')+'/'+lvl+'/'+sc_dir(j) $
    ELSE $
      data_path = scc_data_path(sc_dir(j), source=src_used, type=p_type(p_ind))

   iterate= 1
   rind= p_ind
   ;IF (p_type(p_ind) EQ 'img_seq') THEN BEGIN
   IF (imgseq EQ 'img_seq') THEN BEGIN
     iterate=2
     rind= [0,1] ; for "img" and "seq"
   ENDIF
   IF (imgseq EQ 'img_pol') THEN BEGIN
     iterate=2
     rind= [0,4] ; for "img" and "seq"
   ENDIF

   FOR r= 0, iterate-1 DO BEGIN
    p_ind= rind(r)
    data_path = scc_data_path(sc_dir(j), source=src_used, type=p_type(p_ind))
;print,'data_path='+data_path
;help,p_ind

    setenv,'IMAGES='+data_path

    prev_sfile= ''
    dlm=get_delim()
    FOR i= 0, N_ELEMENTS(tinst)-1 DO BEGIN
     IF (p_type(p_ind) EQ 'pol') THEN BEGIN
      img_dir   = GETENV("SECCHI_P0") + dlm + sc_dir(j) + dlm + tinst(i) + dlm
     ENDIF ELSE BEGIN
      img_dir   = data_path + dlm + tinst(i) + dlm
     ENDELSE
;help,img_dir,days
;print,img_dir

      FOR lday= 0L, days DO BEGIN
        dstr= UTC2STR({mjd:bmjd+lday,time:0L})
        ; change to yyyymmdd:
        dstr= STRMID(dstr,0,4)+STRMID(dstr,5,2)+STRMID(dstr,8,2)
        ;ff = FINDFILE(img_dir+dstr+'/*.fts')
  
        ; Note: FILE_SEARCH sometimes does not return all of the files
        ;       in the directory. Call twice with a second wait between
        ;       them and used the one that is, hopefully, complete:

    	if strpos(img_dir,'P0') gt -1 then searchpath=img_dir+strmid(dstr,0,4)+dlm+dstr+dlm+match_fts else searchpath=img_dir+dstr+dlm+match_fts
        fff = file_search(searchpath)
        IF (debugon) THEN help,fff
        ;WAIT,1 
        ff  = FILE_SEARCH(searchpath)
        IF (debugon) THEN help,ff,files
        IF (N_ELEMENTS(ff) LT N_ELEMENTS(fff)) THEN ff= fff  
	IF ff[0] EQ '' THEN Begin 
	    print,'No files found for ',searchpath
	    ff='none'
    	ENDIF
        ;print,ff(N_ELEMENTS(ff)-1)

;pmfind=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
;help,lday,dstr,pmfind, match_fts

        IF (tinst(i) EQ 'euvi') THEN $
          tt= STRMID(tinst(i),0,2)   $
        ELSE $
          tt= STRMID(tinst(i),0,1)+STRMID(tinst(i),3,1)

        sumtype=p_type(p_ind)
        ;IF (lvl EQ 'L1') THEN sumtype= '.brt'
        IF (lvl EQ 'L1') THEN sumtype= 'brt'

    	rootlen=21
        IF (sumtype eq 'pol') THEN BEGIN
          new_sfile= GETENV('SECCHI_P0')+dlm+sc_dir(j)+ dlm+ $
                     'summary'+dlm+'scc'+STRUPCASE(sc_dir(j))+STRMID(dstr,0,6)+'.'+sumtype
         prev_sfile= '' ; since cor1 and cor2 summary files for .pol have the same name (no c1[2] ext.)
	    rootlen=22
        ENDIF ELSE BEGIN
          new_sfile= scc_data_path(sc_dir(j), source=src_used)+ dlm+ $
                     'summary'+dlm+'scc'+STRUPCASE(sc_dir(j))+STRMID(dstr,0,6)+'.'+sumtype+'.'+tt
        ENDELSE
     
        IF (new_sfile NE prev_sfile) THEN BEGIN
          sf= FILE_SEARCH(new_sfile)
          IF (sf(0) EQ '') THEN BEGIN 
            PRINT,'Did not find '+new_sfile
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= 'Did not find '
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab1,SET_VALUE= new_sfile
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab2,SET_VALUE=''
            openflag= 0
	    sf='none'
          ENDIF ELSE BEGIN
            openflag= 1
            PRINT,'Reading '+sf(0)
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= 'Reading ' 
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab1,SET_VALUE= sf(0) 
            IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab2,SET_VALUE= ''
            row=''
            OPENR,lun, sf(0),/GET_LUN
          ENDELSE 
          prev_sfile= new_sfile
        ENDIF
	
        IF (sf(0) NE 'none') THEN BEGIN ;monthly summary file is open
         IF (ff(0) NE 'none') THEN BEGIN
          FOR k= 0, N_ELEMENTS(ff)-1 DO BEGIN
            IF uv.draw  GT 0 THEN WIDGET_CONTROL,uv.lab2,SET_VALUE= dstr+': '+STRTRIM(k,2) 
	    break_file,ff[k],di,pa,fi,su
	    ftsname=fi
	    ;--Skip rows in summary file until ff[k] is reached
	    
            WHILE (NOT EOF(lun) AND STRMID(row,0,rootlen) NE ftsname) DO READF,lun,row 
;help,k,ff(k),STRMID(row,0,25+add1)

            IF (STRMID(row,0,rootlen) EQ ftsname) THEN BEGIN
              files= [files,ff(k)]
              matches= [matches,row]
              mind= [mind,tot_cnt]
              tot_cnt= tot_cnt+1
              IF uv.draw  GT 0 THEN IF (k MOD 500 EQ 0) THEN WIDGET_CONTROL,draw,SET_VALUE= matches(1:*),/NO_COPY
            ENDIF ELSE BEGIN
              IF (EOF(lun) AND k LT N_ELEMENTS(ff)-1) THEN BEGIN
                ;start over to search for remaining ff filenames incase current filename was
                ; not found in summary file and we are at the end of file:
                POINT_LUN, lun, 0 

                IF (missing NE ftsname) THEN BEGIN 
;help,files,matches,ff
;stop
                  ; set k= k-1 to catch a missed file if ff is out of sorted order (by 
                  ; looking for same file again from start of summary file). But If
                  ; the missed file does not exist in summary file, avoid a infinte loop
                  ; by skipping k=k-1:
                  missing= ftsname
                  IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab2,SET_VALUE= 'Missing or out-of-order '+missing
                  PRINT,'Missing or out-of-order '+missing
                  k= k-1 
                ENDIF 
                ;help,k
              ENDIF
            ENDELSE
          ENDFOR
          IF uv.draw GT 0 THEN $
            IF (N_ELEMENTS(matches) GT 0) THEN WIDGET_CONTROL,draw,SET_VALUE= matches(1:*),/NO_COPY
          ENDIF ELSE BEGIN
	    files=[files,'nofits'+dstr]
	    ;matches=[matches,'nofits'+dstr]
	  ENDELSE
        ENDIF ELSE files=[files,'nosum'+dstr]
;         IF (ff(0) NE '') THEN BEGIN
        IF debugon THEN PRINT, ff
;          files = [files, ff]
;help,ff,files
;        END
      ENDFOR
      IF openflag THEN BEGIN
        CLOSE,lun  ; summary file
        FREE_LUN,lun
      ENDIF
    ENDFOR ; tinst
    fcnt(j)= N_ELEMENTS(files)-1
   ENDFOR ; iterate
  ENDFOR ; sc_dir 
  p_ind= prev_pind
 
 
;  matches= ''
  ;mind= -1 
  nfs= N_ELEMENTS(files)

  ;IF (nfs GT 0) THEN BEGIN
  IF (nfs GT 1) THEN BEGIN
     nfs= nfs-1
     files= files(1:*) ; first element is blank
  ENDIF 

;  matches= matches

;help,files,matches,matches
;stop

  orig_matches= matches
  orig_files= files

  PRINT,'Found '+STRING(N_ELEMENTS(matches)-1,'(I6)')+' matches.' 
  ;SPAWN,'date'

  IF(N_ELEMENTS(matches) GT 1) THEN BEGIN
    IF uv.draw  GT 0 THEN WIDGET_CONTROL,draw,SET_VALUE= matches(1:*) 
    matches= TEMPORARY(matches(1:*))
    orig_matches= matches
    orig_files= files
;help,files,matches,mind
;stop

    ;IF (N_ELEMENTS(files)+1 NE N_ELEMENTS(matches)) THEN BEGIN ; files does not have starting ''.
    
    ;IF (N_ELEMENTS(files) NE N_ELEMENTS(matches)) THEN BEGIN ; files does not have starting ''.
    ;  mind= mind(1:*)
    ;  files= ['',files(mind)]
    ;ENDIF ELSE files= ['',files]
  ENDIF ;ELSE files= ''

;help,mind,matches,files
;stop

;print,"*** at this point must reset fcnt array to reflect new match counts for each s/c (in case summary file didn't have entries for each filename."


  nfiles= N_ELEMENTS(files)
  ;IF (nfiles GT 1) THEN BEGIN 
  ;  files= files(1:*)  ; remove blank entry at start
  ;  nfiles= nfiles-1
    orig_files= files
  ;ENDIF

 IF uv.draw GT 0 THEN BEGIN 
  WIDGET_CONTROL,qtel,SENSITIVE=1
  WIDGET_CONTROL,qexpt1,SENSITIVE=1
  WIDGET_CONTROL,qexpt2,SENSITIVE=1
  WIDGET_CONTROL,qxsize,SENSITIVE=1
  WIDGET_CONTROL,qysize,SENSITIVE=1
  WIDGET_CONTROL,qfltr,SENSITIVE=1
  WIDGET_CONTROL,qpolr,SENSITIVE=1
  WIDGET_CONTROL,qprog,SENSITIVE=1
  WIDGET_CONTROL,qdst,SENSITIVE=1
  WIDGET_CONTROL,qcmp,SENSITIVE=1
  WIDGET_CONTROL,qosn,SENSITIVE=1
  WIDGET_CONTROL,qry,SENSITIVE=1
  WIDGET_CONTROL,rmvsub,SENSITIVE=1
  WIDGET_CONTROL,reset,SENSITIVE=1
  WIDGET_CONTROL,sorder,SENSITIVE=1
  WIDGET_CONTROL,sortid,SENSITIVE=1
  WIDGET_CONTROL,sortit,SENSITIVE=1
  WIDGET_CONTROL,saveid,SENSITIVE=1
  WIDGET_CONTROL,sname,SENSITIVE=1

  
  WIDGET_CONTROL,uv.lab,SET_VALUE= 'Checking and processing summary file matches....' 
  WIDGET_CONTROL,uv.lab1,SET_VALUE= ''
  WIDGET_CONTROL,uv.lab2,SET_VALUE= ''
 ENDIF

IF debugon THEN help,matches,ff,files

  ;SPAWN,'date'

  matches= CHECK_MATCHES(files,uv, fmsg)


  orig_matches= matches
  orig_files= files
  orig_msg= fmsg

IF debugon THEN help,files
;stop

  RETURN,files

END

PRO SORT_ROWS,files,matches,sort_mtd,sort_order,sord,add1

  sclist=strarr(n_elements(matches))
  aind= WHERE(STRPOS(matches,'A.fts') gt -1, acnt)
  bind= WHERE(STRPOS(matches,'B.fts') gt -1, bcnt)
  if acnt gt 0 then sclist(aind)='A'
  if bcnt gt 0 then sclist(bind)='B'
  CASE sort_mtd OF
   "Time": BEGIN
            ; use datetime+sc ID of filename:
            times= STRMID(matches,0,15)+sclist  ; use Time from filename
            srtind= SORT(times)
         END
   "DateObs": BEGIN
            ; use dateobs+sc ID of filename: (dateobs maybe 1958 while date in filename may be 2006) 
            times= STRMID(matches,28,19)+sclist  ; use DateObs
            srtind= SORT(times)
         END
   "Filename": BEGIN
            dotpos=strpos(matches[0],'|')
            fnames= STRMID(matches,0,dotpos)
            fnames= STRCOMPRESS(fnames,/remove_all)
            srtind= SORT(fnames)
         END
   "Telescope": BEGIN
            cams= STRTRIM(STRMID(matches,50,5),2)
            ; add A to EUVI so it is sorted as first camera:
            eind= WHERE(cams EQ 'EUVI', ecnt)
            IF (ecnt GT 0) THEN cams(eind)= 'A'+cams(eind)
              srtind= SORT(cams)
         END
   ;ELSE: WIDGET_CONTROL, uv.lab,SET_VALUE= "Invalid SORT Option.'
   ELSE: PRINT, "Invalid SORT Option.'
  ENDCASE
  sord= ' Ascending '
  IF (sort_order EQ 1) THEN BEGIN
    srtind= REVERSE(srtind)
    sord= ' Descending '
  ENDIF
  matches= TEMPORARY(matches(srtind))
  files= files(srtind)

  RETURN
END


PRO SUBQUERY, files,uv,DONE=DONE,REMOVE=REMOVE

common sorts1
COMMON queries1

           IF (matches(0) EQ '') THEN RETURN 
            toks= STRTRIM(STR_SEP(matches(0),'|'),2)
            flds_arr= STRARR(N_ELEMENTS(toks),N_ELEMENTS(matches))
            flds_arr(*,0)= toks
            FOR p= 1, N_ELEMENTS(matches)-1 DO BEGIN
              colvals= STRTRIM(STR_SEP(matches(p),'|'),2)
              IF (n_elements(colvals) NE N_ELEMENTS(toks)) THEN BEGIN
                IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= "Incomplete Entry for "+colvals(0)+": row# "+STRTRIM(p,2) 
                PRINT,"********************************"
                PRINT,"Incomplete Entry for "+colvals(0)+": row# "+STRTRIM(p,2)
                PRINT,"Please notify Secchi OPS at NRL."
                PRINT,"********************************"
                RETURN
              ENDIF ELSE BEGIN
                ;flds_arr(*,p)= STRTRIM(STR_SEP(matches(p),'|'),2)
                flds_arr(*,p)= STRTRIM(colvals,2)
              ENDELSE
            ENDFOR
	    
IF (debugon) THEN begin
    Help, qtelval,qfltval,qpolval,qprogval,qdstval, $
              qcmpval,qxsizeval,qysizeval,qosnval,qexpt1val,qexpt2val,   $
              qxsize, qysize,qosn,qexpt1,qexpt2    
    help,flds_arr
    print,flds_arr[*,0]
ENDIF
            orig_flds_arr= flds_arr
            IF (qtelval NE 'All') THEN BEGIN
              ;ind= WHERE(flds_arr(2,*) EQ qtelval,cnt)
              ; If HI, remove _ from it (ie. change HI_1 to HI1):
              qtval= qtelval
              IF (STRPOS(qtelval,'HI') GE 0) THEN qtval= 'HI'+STRMID(qtelval,3,1)
              if ~keyword_set(REMOVE) then ind= WHERE(flds_arr(2,*) EQ qtval,cnt) else ind= WHERE(flds_arr(2,*) NE qtval,cnt)
              IF (cnt GT 0) THEN BEGIN 
                flds_arr= TEMPORARY(flds_arr(*,ind)) 
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN 
                flds_arr= 0
                matches= '' 
                files= ''
                GOTO, bottom 
              ENDELSE 
            ENDIF
            IF (qfltval NE 'All') THEN BEGIN
              if ~keyword_set(REMOVE) then ind= WHERE(flds_arr(6,*) EQ qfltval,cnt) else ind= WHERE(flds_arr(6,*) NE qfltval,cnt)
              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind)) 
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN 
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
            IF (qpolval NE 'All') THEN BEGIN
              pols=reform(flds_arr[7,*])
	      IF strpos(qpolval,'+') GT 0 THEN BEGIN
	      ; 1001 + 0
	        pols=fix(pols)
	      	parts=fix(strsplit(qpolval,'+ ',/extract))
    	    	if keyword_set(REMOVE) then $
		ind= WHERE(pols NE parts[0] and pols NE parts[1],cnt) else  $
		ind= WHERE(pols EQ parts[0] or  pols EQ parts[1],cnt) 
    	      ENDIF ELSE $
	      	if keyword_set(REMOVE) then $
		ind= WHERE(pols NE qpolval,cnt) else  $
    	    	ind= WHERE(pols EQ qpolval,cnt) 

              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind))
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN 
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
            IF (qprogval NE 'All') THEN BEGIN
              ;ind= WHERE(flds_arr(8,*) EQ qprogval,cnt)
             if ~keyword_set(REMOVE) then begin
              IF (qprogval EQ 'notSERI') THEN $
                ind= WHERE(flds_arr(8,*) NE 'SERI', cnt) $
               ELSE $
                ind= WHERE(flds_arr(8,*) EQ qprogval,cnt)
	     endif else begin
              IF (qprogval EQ 'notSERI') THEN $
                ind= WHERE(flds_arr(8,*) EQ 'SERI', cnt) $
               ELSE $
                ind= WHERE(flds_arr(8,*) NE qprogval,cnt)
              endelse
              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind)) 
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN 
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
            IF (qdstval NE 'All') THEN BEGIN
              ;ind= WHERE(flds_arr(10,*) EQ qdstval,cnt)
;help,qdstval,flds_arr
;stop
             if ~keyword_set(REMOVE) then begin
              IF (qdstval EQ 'notSW') THEN $
                ;ind= WHERE(flds_arr(10,*) NE 'SW', cnt) $
                ind= WHERE(flds_arr(10,*) NE 'SW' AND flds_arr(10,*) NE 'SWX', cnt) $
              ELSE $
                ;ind= WHERE(flds_arr(10,*) EQ qdstval,cnt)
                ind=WHERE(STRPOS(flds_arr(10,*),qdstval) GE 0, cnt) ; to pickup SW and SWX
	     endif else begin
              IF (qdstval EQ 'notSW') THEN $
                ;ind= WHERE(flds_arr(10,*) NE 'SW', cnt) $
                ind= WHERE(flds_arr(10,*) EQ 'SW' OR flds_arr(10,*) EQ 'SWX', cnt) $
              ELSE $
                ;ind= WHERE(flds_arr(10,*) EQ qdstval,cnt)
                ind=WHERE(STRPOS(flds_arr(10,*),qdstval) lt 0, cnt) ; to pickup SW and SWX
              endelse


              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind))
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom
              ENDELSE
            ENDIF

            IF (qcmpval NE 'All') THEN BEGIN
             IF (N_ELEMENTS(flds_arr(*,0)) GT 12) THEN BEGIN
              ;ind= WHERE(flds_arr(13,*) EQ qcmpval,cnt)
;help,qcmpval,flds_arr
              if ~keyword_set(REMOVE) then ind= WHERE(flds_arr(13,*) EQ qcmpval, cnt) else ind= WHERE(flds_arr(13,*) NE qcmpval, cnt)
              IF cnt EQ 0 THEN BEGIN
                sind= STRPOS(qcmpval,'*')
                IF (sind GT 0) THEN BEGIN
                  tqcmpval= STRMID(qcmpval,0,sind)
                  ind=WHERE(STRPOS(flds_arr(13,*),tqcmpval) GE 0, cnt) ; to pickup all HC* or ICER* 
                ENDIF
              ENDIF
              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind))
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom
              ENDELSE
             ENDIF ELSE BEGIN
              cmsg= 'Compression Ignored - Not Available For This Data Set.'
              IF uv.draw GT 0 THEN WIDGET_CONTROL,uv.lab,SET_VALUE= cmsg 
              PRINT,''
              PRINT,cmsg
              PRINT,''
              WAIT,5
             ENDELSE
            ENDIF

            IF uv.draw GT 0 THEN WIDGET_CONTROL, qxsize, GET_VALUE= qxsizeval
            qxsizeval= strtrim(qxsizeval(0),2)
            IF (qxsizeval NE '') THEN BEGIN
              if ~keyword_set(REMOVE) then ind= WHERE(flds_arr(4,*) EQ qxsizeval,cnt) else ind= WHERE(flds_arr(4,*) NE qxsizeval,cnt)
              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind)) 
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN 
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
            IF uv.draw GT 0 THEN WIDGET_CONTROL, qysize, GET_VALUE= qysizeval
            qysizeval= strtrim(qysizeval(0),2)
            IF (qysizeval NE '') THEN BEGIN
              if ~keyword_set(REMOVE) then ind= WHERE(flds_arr(5,*) EQ qysizeval,cnt) else ind= WHERE(flds_arr(5,*) NE qysizeval,cnt)
              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind)) 
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN 
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
            IF uv.draw  GT 0 THEN WIDGET_CONTROL, qosn, GET_VALUE= qosnval
            qosnval= strtrim(qosnval(0),2)
            IF (qosnval NE '') THEN BEGIN
              ;ind= WHERE(LONG(flds_arr(9,*)) EQ LONG(qosnval),cnt)
            
              cnt = -1

              ps= STRPOS(qosnval,'=') 
              IF (ps GE 0) THEN BEGIN
                tos= LONG(STRMID(qosnval,ps+1,10))
                if ~keyword_set(REMOVE) then ind= WHERE(LONG(flds_arr(9,*)) EQ tos,cnt) else ind= WHERE(LONG(flds_arr(9,*)) NE tos,cnt)
              ENDIF

              ps= STRPOS(qosnval,'>')
              IF (ps GE 0) THEN BEGIN
                tos= LONG(STRMID(qosnval,ps+1,10))
                if ~keyword_set(REMOVE) then ind= WHERE(LONG(flds_arr(9,*)) GT tos,cnt) else ind= WHERE(LONG(flds_arr(9,*)) LE tos,cnt)
              ENDIF

              ps= STRPOS(qosnval,'<')
              IF (ps GE 0) THEN BEGIN
                tos= LONG(STRMID(qosnval,ps+1,10))
                if ~keyword_set(REMOVE) then ind= WHERE(LONG(flds_arr(9,*)) LT tos,cnt) else ind= WHERE(LONG(flds_arr(9,*)) GE tos,cnt)
              ENDIF

              ps= STRPOS(qosnval,'..')
              IF (ps GT 0) THEN BEGIN
                tos1= LONG(STRMID(qosnval,0,ps))
                tos2= LONG(STRMID(qosnval,ps+2,10))
                if ~keyword_set(REMOVE) then ind= WHERE(LONG(flds_arr(9,*)) GE tos1 AND LONG(flds_arr(9,*)) LE tos2,cnt) else $
		    	    	ind= WHERE(LONG(flds_arr(9,*)) lt tos1 AND LONG(flds_arr(9,*)) gt tos2,cnt)
              ENDIF

              IF (cnt EQ -1) THEN BEGIN
                if ~keyword_set(REMOVE) then ind= WHERE(LONG(flds_arr(9,*)) EQ LONG(qosnval),cnt) else ind= WHERE(LONG(flds_arr(9,*)) NE LONG(qosnval),cnt)
              ENDIF

              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind))
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
            IF uv.draw GT 0 THEN WIDGET_CONTROL, qexpt1 ,GET_VALUE= qexpt1val
            qexpt1val= strtrim(qexpt1val(0),2)
            IF uv.draw GT 0 THEN WIDGET_CONTROL, qexpt2 ,GET_VALUE= qexpt2val
            qexpt2val= strtrim(qexpt2val(0),2)
            IF (qexpt1val NE '' AND qexpt2val NE '') THEN BEGIN
              if ~keyword_set(REMOVE) then ind= WHERE(LONG(flds_arr(3,*)) GE LONG(qexpt1val) AND LONG(flds_arr(3,*)) LE LONG(qexpt2val), cnt) else $
	      	    	ind= WHERE(LONG(flds_arr(3,*)) lT LONG(qexpt1val) OR LONG(flds_arr(3,*)) GT LONG(qexpt2val), cnt)
              IF (cnt GT 0) THEN BEGIN
                flds_arr= TEMPORARY(flds_arr(*,ind))
                matches= TEMPORARY(matches(ind))
                files= TEMPORARY(files(ind))
              ENDIF ELSE BEGIN
                flds_arr= 0
                matches= ''
                files= ''
                GOTO, bottom 
              ENDELSE
            ENDIF
bottom:
            IF datatype(qclist) ne 'UND' and uv.draw ne 0 THEN BEGIN
              qtelval= qclist(0)
	      WIDGET_CONTROL,qtel,SET_DROPLIST_SELECT=0
              qexpt1val= ''
              WIDGET_CONTROL, qexpt1 ,SET_VALUE= qexpt1val
  	      qexpt2val= ''
              WIDGET_CONTROL, qexpt2 ,SET_VALUE= qexpt2val
              qxsizeval= ''
              WIDGET_CONTROL, qxsize ,SET_VALUE= qxsizeval
              qysizeval= ''
              WIDGET_CONTROL, qysize ,SET_VALUE= qysizeval
              qfltval= qflist(0)
              WIDGET_CONTROL,qfltr,SET_DROPLIST_SELECT=0
	      qpolval= STRTRIM(qplist(0),2)
              WIDGET_CONTROL,qpolr, SET_COMBOBOX_SELECT= 0
               qprogval= qprglist(0)
              WIDGET_CONTROL,qprog,SET_DROPLIST_SELECT=0
              qosnval= ''
              WIDGET_CONTROL, qysize ,SET_VALUE= qosnval
              qprogval= qprglist(0)
              WIDGET_CONTROL,qdst,SET_DROPLIST_SELECT=0
	      qcmpval= qcmplist(0)
              WIDGET_CONTROL,qcmp,SET_DROPLIST_SELECT=0
              qosnval= ''
              WIDGET_CONTROL, qosn ,SET_VALUE= qosnval
            ENDIF

  
           ;help,qtelval,qexpt1val,qexpt2val,qxsizeval,qysizeval,qfltval,qpolval,qprogval,qosnval,qsdtval

           ;help,orig_flds_arr, flds_arr, matches, orig_matches ,files,orig_files

           IF (KEYWORD_SET(done)) THEN $
             matches= CHECK_MATCHES(files,uv, fmsg, /DONE) $ ; return structure
           ELSE $
             matches= CHECK_MATCHES(files,uv, fmsg)

END



PRO SCCLISTER_EVENT,event

;@wload.com
;@chandle.com

COMMON dirs_files1, dirs, files, hdrs, sel_method, s_method, a_files, $
                   dirs_f,files_f,dirs_s,files_s, new_dirs, new_hdrs, new_text, $
                   img_dir,data_path

COMMON date_block1, month, day, year, ibm, ibd, iby, iem, ied, iey, iip, iil, idp, icx, ipi, sec_ipi
COMMON info1
COMMON ev,ms
COMMON scraft1
COMMON fdisplay1
COMMON queries1
COMMON sorts1
COMMON savefiles1
COMMON pmatched1
                  
WIDGET_CONTROL, event.id, GET_UVALUE=uval          ; get the UVALUE of the event
WIDGET_CONTROL, event.top, GET_UVALUE=uv           ; get structure from UVALUE

;***************************************************************************

   add1= 0 
   ;IF (p_ind GT 3) THEN add1= 1 

   IF (DATATYPE(uval) EQ 'UND' AND $
       event.id EQ qpolr) THEN uval='QPOL' ; since COMBOBOX is used, uval is undefined.

   k = where(uval eq ms.mnames, count)  ;Match with selection name?


            if (k(0) GE 0) then begin
               for t = 0, n_elements(ms.mnames)-1 do begin
                    WIDGET_CONTROL, ms.mbases(t), map=0, /NO_COPY
               endfor
               
               WIDGET_CONTROL, ms.mbases(k), map=1, /NO_COPY

            endif
            
;***************************************************************************

CASE uval OF

  "FNSH": BEGIN

;help,files,matches

            sipath= CHECK_MATCHES(files, uv, fmsg, /DONE)

            WIDGET_CONTROL, event.top, /DESTROY

          END


  "QRY":  BEGIN
            WIDGET_CONTROL,/HOUR
            SUBQUERY,files,uv
            WIDGET_CONTROL,draw,SET_VALUE= matches
          END

  "REMOVESUB":  BEGIN
            WIDGET_CONTROL,/HOUR
            SUBQUERY,files,uv,/remove
            WIDGET_CONTROL,draw,SET_VALUE= matches
          END

  "RESET":  BEGIN
;help,files,orig_files,matches,orig_matches
;stop
              WIDGET_CONTROL,/HOUR
              matches= orig_matches
              files= orig_files
              fmsg= orig_msg
              WIDGET_CONTROL,draw,SET_VALUE= matches
              WIDGET_CONTROL,qtel,SET_DROPLIST_SELECT= 0
              qtelval='All' 
              WIDGET_CONTROL,qexpt1,SET_VALUE= ''
              WIDGET_CONTROL,qexpt2,SET_VALUE= ''
              WIDGET_CONTROL,qxsize,SET_VALUE= ''
              WIDGET_CONTROL,qysize,SET_VALUE= ''
              WIDGET_CONTROL,qfltr,SET_DROPLIST_SELECT= 0
              qfltval='All'
              ;WIDGET_CONTROL,qpolr,SET_DROPLIST_SELECT= 0 
              WIDGET_CONTROL,qpolr,SET_COMBOBOX_SELECT= 0 
              qpolval='All'
              WIDGET_CONTROL,qprog,SET_DROPLIST_SELECT= 0 
              qprogval='All'
              WIDGET_CONTROL,qdst,SET_DROPLIST_SELECT= 0
              qdstval='All'
              WIDGET_CONTROL,qcmp,SET_DROPLIST_SELECT= 0
              qcmpval='All'
              WIDGET_CONTROL,qosn,SET_VALUE= ''
              WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg
              last_sort_mtd= ''
              sort_order=0 ; Ascending
              last_sort_mtd= last_sort_mtd+STRTRIM(sort_order,2)
              WIDGET_CONTROL,sorder,SET_DROPLIST_SELECT= sort_order 
          END

  "QTEL": BEGIN
               qtelval= qclist(event.index)
               ;help,qtelval
          END
  "QEXPT1": BEGIN
              WIDGET_CONTROL, qexpt1 ,GET_VALUE= qexpt1val
              qexpt1val= strtrim(qexpt1val,2)
              ;help,qexpt1val
          END
  "QEXPT2": BEGIN
              WIDGET_CONTROL, qexpt2 ,GET_VALUE= qexpt2val
              qexpt2val= strtrim(qexpt2val,2)
              ;help,qexpt2val
          END
  "QXZ": BEGIN
              WIDGET_CONTROL, qxsize ,GET_VALUE= qxsizeval
              qxsizeval= strtrim(qxsizeval,2)
              ;help,qxsizeval
          END
 "QYZ": BEGIN
              WIDGET_CONTROL, qysize ,GET_VALUE= qysizeval
              qysizeval= strtrim(qysizeval,2)
              ;help,qysizeval
          END
 "QFLT": BEGIN
               qfltval= qflist(event.index)
              ;help,qfltval
          END
 "QPOL": BEGIN
              qpolval= STRTRIM(qplist(event.index),2)
              ;help,qpolvl
          END
 "QPROG": BEGIN
               qprogval= qprglist(event.index)
               ;help,qprogval
          END
 "QOS": BEGIN
              WIDGET_CONTROL, qosn ,GET_VALUE= qosnval
              qosnval= strtrim(qosnval,2)
               ;help,qosnval
          END
 "QDST": BEGIN
              qdstval= qdstlist(event.index)
               ;help,qdstval
          END
 "QCMP": BEGIN
              qcmpval= qcmplist(event.index)
               ;help,qcmpval
          END
   "SAVEID": BEGIN
               WIDGET_CONTROL, sname, GET_VALUE= savename
               savename= STRTRIM(savename(0))
;help,matches,files

               IF (matches(0) NE '') THEN BEGIN
                 IF (N_ELEMENTS(files) EQ N_ELEMENTS(matches)) THEN BEGIN
                   ; this should always be true
                   images= {fnames:files, info:matches}
                 ENDIF ELSE BEGIN
                   mcnt= N_ELEMENTS(matches)
                   mfiles= STRARR(mcnt) 
                   FOR m= 0L, mcnt-1 DO BEGIN
                     mfiles(m)= files(WHERE(STRPOS(files,STRMID(matches(m),0,25+1)) GE 0))     
                   ENDFOR
                   images= {fnames:mfiles, info:matches}
                 ENDELSE
                 SAVE,filename= savename, images 
                 WIDGET_CONTROL, uv.lab, SET_VALUE='Saved displayed file names and info as '+savename+'.'
               ENDIF ELSE WIDGET_CONTROL, uv.lab, SET_VALUE='Nothing to Save.'
          END
   "SORDER": BEGIN
              sort_order= event.index
          END 
  "SRT_SEL": BEGIN
              sort_mtd= strtrim(sortlist[event.index],2)
              ;help, sort_mtd
          END
  "SORT": BEGIN
             help,event, sort_mtd, last_sort_mtd, matches, files
             WIDGET_CONTROL, /HOUR
             IF (matches(0) EQ '') THEN BEGIN
               WIDGET_CONTROL, uv.lab, SET_VALUE= 'Nothing to Sort.'
               RETURN
             END
             IF (sort_mtd+STRTRIM(sort_order,2) NE last_sort_mtd) THEN BEGIN
               last_sort_mtd= sort_mtd+STRTRIM(sort_order,2)
               SORT_ROWS,files,matches,sort_mtd,sort_order,sord, add1
               WIDGET_CONTROL,draw,SET_VALUE= matches
               WIDGET_CONTROL, uv.lab,SET_VALUE= "Sorted by"+sord+sort_mtd+"."
             ENDIF
          END
  "PDMENU": BEGIN
 	      CASE event.value OF
 
	        "Save":  BEGIN
         	          ;wsave
                 	 END

               "PointAndClickFiles": BEGIN
               ;"Point, Click, and Save Files": BEGIN
               ;"Point-Click-And-Save Filenames": BEGIN

                  IF (files(0) NE '') THEN BEGIN
                    ;resp= xmenu_sel(TIT="  (Save/Append filenames to ./myfiles.list)",$
                    resp= xmenu_sel(TIT="  (Select Files)",$
                                    ;files, nlines=30,group=ms.WLISTERBASE)
                                    matches, nlines=30,group=ms.WLISTERBASE)
                    IF resp(0) NE -1 THEN BEGIN 
                      pickedfiles= files(resp)
                      ;help,resp
                      ;help,pickedfiles

                      ;help,matches,files
                      matches= TEMPORARY(matches(resp))
                      files= TEMPORARY(files(resp))
                      ;help,matches,files
                      matches= CHECK_MATCHES(files,uv, fmsg)
                      WIDGET_CONTROL,draw,SET_VALUE= matches

;                      PRINT,''
;                      PRINT,'Adding selected files to ./myfiles.list'
;                      OPENW, fslun, './myfiles.list', /GET_LUN, /APPEND
;                      FOR fsel= 0, N_ELEMENTS(resp)-1 DO $
;                        ;PRINTF,fslun, files(resp(fsel))
;                        PRINTF,fslun, files((fsel)
;                      CLOSE,fslun
;                      FREE_LUN, fslun 
;                      PRINT,'Done.'

                    ENDIF 
                  ENDIF
               END


         	"Help":  BEGIN
     ;hmsg= ['Select SpaceCraft, data level, instrument(s), observation program(s),', $
     hmsg= ['Select SpaceCraft, data level, instrument(s), Data Type (see Notes below),', $
            'and observation dates from left pannel. To further limit your', $
            'selection, use "Restrict File Selection" area; The drop-down values', $
            'may be editted. For example, changing ?? under mm to ?0 will cause',$
            'images at every 10 minutes (00, 10, 20, 30, 40, 50) to be selected.',$
            'Pressing "Submit" button will start the search.', $
            '', $
	    'Note that when using WSCC_MKMOVIE, you will have the option of matching ', $
	    'images from ST-A with ST-B images or vice versa. (Select one or the other here.) ', $
	    '', $
            'A select number of image-header parameters are then displayed in the',$
            'top right pannel for each image found. These can further be sub-selected',$
            'using Query options in the bottom of the right pannel.',$
            'The "ReloadOrig" button may be used to re-display the original results.',$
            '', $
            'The displayed image information may be sorted by time, filename, or',$
            'telescope. Ascending or descending order can be used. Pressing the "Sort"',$
            'button performs the sort.',$
            '', $
            'To Save currently displayed info plus filenames, including full paths,',$
            'enter a new IDL save set filename (default is scc_images.sav) and press the',$
            '"Save" button. The resulting save set consists of an images structure with',$
            'fnames and info tags. These tag fields are string arrays of lengths ',$
            'matching number of rows for currently displayed image information.',$
            '', $
            'To exit and return filenames for displayed image information, press on the',$
            '"Done - Return Filenames". This ends the program and returns an IDL structure',$
            'of paths+filenames. Filenames for SC-A and SC-B are separated under',$
            'sc_a and sc_b tag names. An empty string is returned if no files were',$
            ;'found.']
            'found.',$
            '',$
            'Notes:',$
            ' Data Types Are:',$
            '     Intensity - All EUVI non-cal images (NORMAL and SEQU), COR combined onboard',$
            '                 (must be of type NORMAL), all HI combined onboard (type SEQU or',$
            '                 NORMAL).',$
            '     Polarized - All COR SEQU images (since they are never combined onboard), COR',$
            '                 NORMAL images if they are not result of combining images (depends',$
            '                 on IP).',$
            '     Calibration - All Dark, LED, and CONT images.',$
            '',$
            ' Also, for "Polar" under "Sub-Select Options", 304, 195, 284, and 171 are the EUVI',$
            ' wavelengths. Remaining polar values are positions of the COR1 and COR2 polarizer.',$
            ' If the image is computed from a sequence, then this is the sum of the positions',$
            ' during the sequence. Polarizer steps in increments of 2.5 degrees for 144 positions.',$ 
            ' For TotalB or %P images, polar is:',$
            '   1001 - Total Brightness',$
            '   1002 - Polarized Brightness',$
            '   1003 - Percent Polarized',$
            '   1004 - Polarization Angle',$
            '']

          
             XDISPLAYFILE,TEXT=hmsg,TITLE='SCCLISTER Help', $
             GROUP=event.top, HEIGHT=20, WIDTH=90
  
               	 END
	 
	         "Done": BEGIN
	         	; save settings
	         	    	
       	 	        WIDGET_CONTROL, event.top, /DESTROY
                                                            
               	        IF N_ELEMENTS(h_array) EQ 0 THEN RETURN
               	                                
                        ; new loaded images must be activ as default.
                        
                        nl = N_ELEMENTS(h_array)    ; how many images are loaded.
                        ns = N_ELEMENTS(sel_array)  ; how many images were activ. 
                        
                        nn = nl - uv.n_old ; number of new loaded images
                                                                                                  
                        IF (nn GT 0) AND (uv.n_old EQ 0) THEN BEGIN           
                         ; nl tell me how many images are loaded               
                         sel_array = INDGEN(nl)
                         sel_img = INTARR(nl)
                         sel_img(0:nl-1) = 1 
                        ENDIF
                        
                        IF (nn GT 0) AND (uv.n_old GT 0) THEN BEGIN
                          FOR i=1,nn DO BEGIN
                            sel_array = [sel_array,n_elements(sel_img)]
                            sel_img = [sel_img,1]                                                      
                          ENDFOR                                                
                        ENDIF
                        

                   	  RETURN
                 	 END
 
 		ELSE: donothing=0
 		ENDCASE
 
	      END	

   "BM": ibm = event.index
   "BD": ibd = event.index
   "BY": iby = event.index  
   "EM": iem = event.index
   "ED": ied = event.index
   "EY": iey = event.index

   "SCFT" : sc_sel= event.index
   
   "SB": BEGIN
          widget_control,uv.b_year, SET_DROPLIST_SELECT=iey
	  widget_control,uv.b_month,SET_DROPLIST_SELECT=iem
	  widget_control,uv.b_day,  SET_DROPLIST_SELECT=ied
           ibd = ied
           ibm = iem
           iby = iey
         END

   "SE": BEGIN
          widget_control,uv.e_year, SET_DROPLIST_SELECT=iby
	  widget_control,uv.e_month,SET_DROPLIST_SELECT=ibm
	  widget_control,uv.e_day,  SET_DROPLIST_SELECT=ibd
          ied = ibd
          iem = ibm
          iey = iby
         END
       

   "euvi": BEGIN & sec_sflag = 0 & sec_inst='euvi' & END 
   "cor1": BEGIN & sec_sflag = 1 & sec_inst='cor1' & END
   "cor2": BEGIN & sec_sflag = 2 & sec_inst='cor2' & END 
   "hi1":  BEGIN & sec_sflag = 3 & sec_inst='hi_1' & END 
   "hi2":  BEGIN & sec_sflag = 4 & sec_inst='hi_2' & END
   "All":  BEGIN & sec_sflag = 5 & sec_inst='All' & END
   "rh":   hnh = 1
   "dr":   hnh = 0  

  "CYR": BEGIN
            vcyr= STRTRIM(event.str,2)
            WIDGET_CONTROL,fsid,SET_VALUE=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
            ;help,vcyr
          END
  "CMO": BEGIN
            vcmo= STRTRIM(event.str,2)
            WIDGET_CONTROL,fsid,SET_VALUE=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
            ;help,vcmo
          END
  "CDY": BEGIN
            vcdy= STRTRIM(event.str,2)
            WIDGET_CONTROL,fsid,SET_VALUE=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
            ;help,vcdy
          END
  "CHR": BEGIN
            vchr= STRTRIM(event.str,2)
            WIDGET_CONTROL,fsid,SET_VALUE=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
            ;help,vchr
          END
  "CMN": BEGIN
            vcmn= STRTRIM(event.str,2)
            WIDGET_CONTROL,fsid,SET_VALUE=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
            ;help,vcmn
          END
  "CSE": BEGIN
            vcse= STRTRIM(event.str,2)
            WIDGET_CONTROL,fsid,SET_VALUE=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
            ;help,vcse
          END
  "FSID": BEGIN
              WIDGET_CONTROL, fsid ,GET_VALUE= search_string
	      ;message,'FSID val='+search_string,/info
          END
         

  "SEL_INST": BEGIN
            ;help,event
            ind= WHERE(sec_select EQ event.id)
            IF (event.select) THEN $
              sel_id(ind)= 1 $
            ELSE $
              sel_id(ind)= 0
;print,"sel_id= ",sel_id

;            tmp= WHERE(sel_id EQ 1, selcnt)
;            IF (selcnt EQ 1) THEN BEGIN
;              WIDGET_CONTROL, cyrid, SENSITIVE=1
;              WIDGET_CONTROL, cmoid, SENSITIVE=1
;              WIDGET_CONTROL, cdyid, SENSITIVE=1
;              WIDGET_CONTROL, chrid, SENSITIVE=1
;              WIDGET_CONTROL, cmnid, SENSITIVE=1
;              WIDGET_CONTROL, cseid, SENSITIVE=1
;            ENDIF ELSE BEGIN
;              WIDGET_CONTROL, cyrid, SENSITIVE=0
;              WIDGET_CONTROL, cmoid, SENSITIVE=0
;              WIDGET_CONTROL, cdyid, SENSITIVE=0
;              WIDGET_CONTROL, chrid, SENSITIVE=0
;              WIDGET_CONTROL, cmnid, SENSITIVE=0
;              WIDGET_CONTROL, cseid, SENSITIVE=0
;            ENDELSE

          END

  "CAD1": BEGIN
            WIDGET_CONTROL, cad1 ,GET_VALUE= cad1time 
            dobs1= strtrim(cad1time,2)
          END

   "CAD2": BEGIN
            WIDGET_CONTROL, cad2, GET_VALUE= cad_sel
            cad_sel= LONG(strtrim(cad_sel(0),2))
          END

   "DO_CAD": BEGIN
            WIDGET_CONTROL, cad1, GET_VALUE= dobs1
            dobs1= strtrim(dobs1(0),2)
            WIDGET_CONTROL,cad2, GET_VALUE= cad_sel
            cad_sel= LONG(strtrim(cad_sel(0),2))
            ;help,dobs1,cad_sel
           END

  "drawn": BEGIN
             ;print,"selected " + matches(event.index)
             dobs= (STR_SEP(matches(event.index),'|'))(1)
             ;print,'date_obs= ', dobs
             dobs1= dobs(0)
             ;WIDGET_CONTROL,cad1, SET_VALUE= dobs1
             ;PRINT,dobs1
             PRINT, files(event.index)
;help,event.index,matches,files
;stop

             ;IF (dobs1 EQ '') THEN BEGIN  
             ;  dobs1= dobs 
             ;  WIDGET_CONTROL,cad1, SET_VALUE= dobs1
             ;ENDIF ELSE BEGIN 
             ;  dobs2= dobs
             ;  WIDGET_CONTROL,cad2, SET_VALUE= dobs2
             ;ENDELSE

          END

  "FIND": BEGIN
     WIDGET_CONTROL,uv.lab2, SET_VALUE=''
     WIDGET_CONTROL,uv.lab1, SET_VALUE=''
     WIDGET_CONTROL,/HOUR
     WIDGET_CONTROL,uv.lab,SET_VALUE=" Working ........                       "

     WIDGET_CONTROL,qtel,SENSITIVE=0
     WIDGET_CONTROL,qexpt1,SENSITIVE=0
     WIDGET_CONTROL,qexpt2,SENSITIVE=0
     WIDGET_CONTROL,qxsize,SENSITIVE=0
     WIDGET_CONTROL,qysize,SENSITIVE=0
     WIDGET_CONTROL,qfltr,SENSITIVE=0
     WIDGET_CONTROL,qpolr,SENSITIVE=0
     WIDGET_CONTROL,qprog,SENSITIVE=0
     WIDGET_CONTROL,qosn,SENSITIVE=0
     WIDGET_CONTROL,qdst,SENSITIVE=0
     WIDGET_CONTROL,qcmp,SENSITIVE=0
     WIDGET_CONTROL,qry,SENSITIVE=0
     WIDGET_CONTROL,reset,SENSITIVE=0
     WIDGET_CONTROL,sorder,SENSITIVE=0
     WIDGET_CONTROL,sortid,SENSITIVE=0
     WIDGET_CONTROL,sortit,SENSITIVE=0
     WIDGET_CONTROL,saveid,SENSITIVE=0
     WIDGET_CONTROL,sname,SENSITIVE=0


;help, vcyr,vcmo,vcdy,vchr,vcmn,vcse
;pmfind=vcyr+vcmo+vcdy+'_'+vchr+vcmn+vcse+'*.fts'
;help,pmfind
;stop

     files= GET_FILES(year,month,day,iby,ibm,ibd,iey,iem,ied, $
                     sec_inst, sel_id, sc_sel, nfiles, fmsg, sec_level, $
                     dtype, p_type, p_ind, uv.draw, selfiles, uv, src_used)


     IF (nfiles GT 0) THEN BEGIN
       sipath= files
     ENDIF 

     ;WIDGET_CONTROL,uv.lab,SET_VALUE=" Found "+STRTRIM(nfiles,2)+" files."
     ;WIDGET_CONTROL,uv.lab,SET_VALUE= fmsg

         END

  "SRC": BEGIN
           src_ind= event.index
           src_used= sources(src_ind)
         END

  "IP2": BEGIN
            lvl_ind= event.index
            sec_level = sec_lvl(lvl_ind)
            IF (sec_level EQ 'L1') THEN BEGIN
            ;IF (sec_level EQ 'L1' OR p_type(p_ind) EQ 'pol') THEN BEGIN
              ; Only COR1 and COR2 level-1 (Brightness) images:
              WIDGET_CONTROL,sec_select(0),SENSITIVE=0  ; EUVI
              WIDGET_CONTROL,sec_select(0),SET_BUTTON=0
              sel_id(0)= 0
              WIDGET_CONTROL,sec_select(3),SENSITIVE=0  ; HI1 
              WIDGET_CONTROL,sec_select(3),SET_BUTTON=0
              sel_id(3)= 0
              WIDGET_CONTROL,sec_select(4),SENSITIVE=0  ; HI2 
              WIDGET_CONTROL,sec_select(4),SET_BUTTON=0
              sel_id(4)= 0
              WIDGET_CONTROL,uv.seb_id,SENSITIVE=0 ; level-1 not broken by types
              WIDGET_CONTROL,uv.seb_id,SET_BUTTON=0 
            ENDIF ELSE BEGIN
              ;IF (p_type(p_ind) NE 'seq' AND p_type(p_ind) NE 'img_seq') THEN BEGIN
              IF (p_type(p_ind) NE 'seq' AND p_type(p_ind) NE 'img_seq' AND $
                  p_type(p_ind) NE 'pol') THEN BEGIN
                WIDGET_CONTROL,sec_select(0),SENSITIVE=1
                WIDGET_CONTROL,sec_select(3),SENSITIVE=1
                WIDGET_CONTROL,sec_select(4),SENSITIVE=1
              ENDIF
              WIDGET_CONTROL,uv.seb_id,SENSITIVE=1
            ENDELSE
         END
   
  "SEB_PROG": BEGIN
            p_ind= event.index
            ;IF (p_type(p_ind) EQ 'seq' OR p_type(p_ind) EQ 'img_seq') THEN BEGIN
            IF (p_type(p_ind) EQ 'seq' OR p_type(p_ind) EQ 'img_seq' OR $
                p_type(p_ind) EQ 'pol') THEN BEGIN
              ; Only COR1 and COR2 level-1 images:
              WIDGET_CONTROL,sec_select(0),SENSITIVE=0  ; EUVI
              WIDGET_CONTROL,sec_select(0),SET_BUTTON=0
              sel_id(0)= 0
              WIDGET_CONTROL,sec_select(3),SENSITIVE=0  ; HI1
              WIDGET_CONTROL,sec_select(3),SET_BUTTON=0
              sel_id(3)= 0
              WIDGET_CONTROL,sec_select(4),SENSITIVE=0  ; HI2
              WIDGET_CONTROL,sec_select(4),SET_BUTTON=0
              sel_id(4)= 0
            ENDIF ELSE BEGIN
              WIDGET_CONTROL,sec_select(0),SENSITIVE=1
              WIDGET_CONTROL,sec_select(3),SENSITIVE=1
              WIDGET_CONTROL,sec_select(4),SENSITIVE=1
            ENDELSE

          END            

     "OK":  BEGIN
              WIDGET_CONTROL,uv.wtxt1,GET_VALUE=a_files
              a_files = WHERE(a_files GT "")
              hinfo = hdrsinfo(files_s, MEM=1)

            END

  "Pick Files": BEGIN
                 ib = WHERE( ms.mnames EQ 'Pick Files')
                 WIDGET_CONTROL, uv.bf(ib), SET_BUTTON=0
                   
            files = pickfiles(PATH = new_path)
            n = N_ELEMENTS(files)
            files_s = files
            dirs_s  = files
            FOR i=0,n-1 DO BEGIN
             sp = RSTRPOS(files(i),!delimiter)
             sl = STRLEN(files(i))
             files_s(i) = STRMID(files(i),sp+1, sl-sp)
             dirs_s(i)  = STRMID(files(i),0,sp+1)
            ENDFOR  
              
              image_path = STRMID(new_path,0,RSTRPOS(new_path,!delimiter))+!delimiter+'fits'
              
              files = files_s
              dirs  = dirs_s
              
              load_lasco,wbp=uv.WListerBase, path = image_path

;             WIDGET_CONTROL,uv.wtxt1,SET_VALUE=files_s
          END

    "LOAD": BEGIN

             ;WIDGET_CONTROL,uv.wtxt1,SET_VALUE=new_files                 

                   files = STRARR(N_ELEMENTS(files_s))
                   dirs  = STRARR(N_ELEMENTS(dirs_s))  
                                   
                   files = files_s
                   dirs =  dirs_s

             IF files(0) EQ "No files found" OR files(0) EQ "" THEN BEGIN 
               msg = WIDGET_MESSAGE(" Please, select loading method! ")
               RETURN
             ENDIF

             load_lasco,wbp=uv.WListerBase
           END

 "CANCEL": BEGIN
            cancel = 1
            ;msg = WIDGET_MESSAGE("Not applied yet!")

           END


  "HELPF":  BEGIN
              path_msg = GETENV('LASCO_MSG')
              tmp_msg  = path_msg + !delimiter + 'scclister.msg'
              xdisplayfile,tmp_msg,height=20,width=65,title='Help'
            END                	 
                 	 
 ELSE: donothing=0 
 ENDCASE

END


function SCCLISTER, date, tel, help=help, scid=scid, imgtype=imgtype, src=src, VERBOSE=verbose, $
                    size=size, exp=exp, osnum=osnum, dest=dest, sebprog=sebprog, filt=filt, $
                    polar=polar,compression=compression,sort_type=sort_type,sort_by=sort_by, $
                    data_level=data_level,data_src=data_src,modal=modal, AORB=aorb, DEBUG=debug

;@wload.com
;@chandle.com

COMMON dirs_files1, dirs, files, hdrs, sel_method, s_method, a_files, $
                   dirs_f,files_f,dirs_s,files_s, new_dirs, new_hdrs, new_text, $
                   img_dir,data_path

COMMON date_block1
COMMON info1
COMMON ev
COMMON scraft1
COMMON fdisplay1
COMMON queries1
COMMON sorts1
COMMON savefiles1
COMMON pmatched1

matches= ''
files= ''
parts=strsplit('$Id: scclister.pro,v 1.40 2015/11/30 18:37:45 mcnutt Exp $',/extract)
version=parts[2]
;help,files,qtelval,qfltval,qpolval,qprogval,qdstval,qcmpval,qxsizeval,qysizeval,qosnval,qexpt1val,qexpt2val

;
;  Use SCC_DATA_PATH to test for proper evar setup, rather than testing $secchi
;  directly.  -- William Thompson, 19-June-2006, GSFC
;
errmsg = ''
test = scc_data_path('ahead',errmsg=errmsg)
IF (errmsg NE '') THEN BEGIN
;;IF (GETENV('secchi') EQ '') THEN BEGIN
  PRINT,''
  PRINT,'Undefined SECCHI environment variables.'
  PRINT,''
  PRINT,'Exit IDL and type:'
  PRINT,''
  PRINT,'  source /net/cronus/opt/nrl_ssw_setup'
  PRINT,'  setup'
  PRINT,''
  PRINT,'or the equivalent for your site,'
  PRINT,'and start IDL again'
  PRINT,''
  RETURN,''
ENDIF

set_delimiter
IF keyword_set(VERBOSE) or keyword_set(DEBUG) THEN debugon=1 ELSE debugon=0

IF NOT KEYWORD_SET(sec_sflag) THEN sec_sflag = 5 
IF N_ELEMENTS(sec_inst) EQ 0 THEN sec_inst = 'All'
IF N_ELEMENTS(hnh) EQ 0 THEN hnh = 1

sec_lvl = ["L0","L1"]
slevel = ["Level-05","Level-1 (COR1/COR2 TB Images)"]
IF (DATATYPE(lvl_ind) EQ 'UND') THEN lvl_ind= 0 
sec_level = sec_lvl(lvl_ind)

sources = ["lz","pb","rt"]
IF (DATATYPE(src_ind) EQ 'UND') THEN src_ind= 0 
src_used= sources(src_ind)

IF N_ELEMENTS(string_name)  EQ 0 THEN string_name = "no image select"
IF N_ELEMENTS(files) EQ 0 THEN files = "no files selected"

n_old = n_elements(h_array)  ; number of previous loaded images

message = load_message() ; this must change

IF N_ELEMENTS(new_name) EQ 0 THEN new_name = ''
IF N_ELEMENTS(new_path) EQ 0 THEN new_path = GETENV('IMAGES')+!delimiter+sec_level+!delimiter
;
; if file setup.las exist then load it !
;

s_method = 4 ; *** default ***

setup_path = GETENV('HOME')
setup_las  = setup_path + !delimiter + 'setup.las'

xs = 50 ; XSIZE in widget WIDGET_TEXT
img_resize = 1 ; default rebin size

; ***  always reset dirs and files first ***

IF N_ELEMENTS(ld) EQ 0 THEN $
 ld = { load_defaults, net:0, dark:0, size:1, scl:0, byt:0 }
  
dirs = ''

files_s = ['']
dirs_s = ['']

;------------
;Init
;------------
tmps =['']
undefine,tmpp	;tmpp = ['']

text2 = ['']
undefine,files 	;files = ['']

undefine,new_hdrs
undefine,sipath

undefine,files_f	;files_f = ['']
undefine,dirs_f	;dirs_f  = ['']


files_s = ['']
dirs_s  = ['']

new_text = ['']
new_dirs = ['']

new_dirs_f = ['']
new_files_f= ['']

new_tmpp = ['']

;------------
; Reset 

undefine,tmps
undefine,tmpp

;------------
new_filter = "*.*"
new_name = "*.fts"

mnames = [ 'Load from Catalog(s)']
nmodes = n_elements(mnames)

ms = { scclister, WListerBase:0L, $
       mnames:mnames, $
       mbases:lonarr(nmodes) $
     }  

ImgPath = ["LZ_IMG"]
; why is this for LASCO? -nr, 9/3/08


FromTo = [" From .. To "," Single Day "]

month=['01','02','03','04','05','06','07','08','09','10','11','12']

day=[" 01"," 02"," 03"," 04"," 05"," 06"," 07"," 08"," 09"," 10"," 11"," 12"," 13"," 14"," 15", $
     " 16"," 17"," 18"," 19"," 20"," 21"," 22"," 23"," 24"," 25"," 26"," 27"," 28"," 29"," 30"," 31"]
day = STRCOMPRESS(day,/REMOVE_ALL)

year=[" 2004"," 2005"," 2006 "," 2007 "," 2008 "," 2009 "," 2010 "," 2011 "," 2012 "," 2013 "," 2014 "," 2015 "," 2016 "," 2017 "," 2018 "," 2019 "," 2020 "," 2021 "," 2022 "," 2023 "," 2024 "," 2025 "," 2026 "," 2027 "," 2028 "," 2029 "," 2030 "]


year= STRCOMPRESS(year,/REMOVE_ALL)

sec_ipi = 0 ; set ImgPath index to level_1 for secchi

;data_path = GETENV(ImgPath(sec_ipi))
; This was trying to access unavailable lasco data - nr, 9/3/08
data_path=getenv('secchi') + !delimiter + 'lz'
img_dir   = data_path + !delimiter + sec_level + !delimiter + 'a' + !delimiter + 'img' + !delimiter + 'hi_2'

load_method = [" List of images", $
               " Input file name and path"] 
            
sec_inst= ['EUVI','COR1','COR2','HI_1','HI_2']
ninst= N_ELEMENTS(sec_inst)
sec_select= LONARR(ninst)
sel_id= INTARR(ninst)

;dtype= ["Intensity","Polarized","Calibration"]
;p_type= ["img","seq","cal"]
;dtype= ["Intensity","Polarized","Calibration","TB (double)+Polarized"]
dtype= ["img (Intensity)","seq (Polarized)","cal","img+seq"]
p_type= ["img","seq","cal","img_seq"]
IF (GETENV('SECCHI_P0') NE '') THEN BEGIN
  ;dtype= [dtype,"Total Brightness","Percent Polarized"]
  ;p_type= [p_type,"pol","pol"]
  dtype= [dtype,"P0 (TotBr+PercPolar)","img+P0"]
  p_type= [p_type,"pol","img_pol"]
ENDIF

IF (DATATYPE(P_ind) EQ 'UND') THEN p_ind= 0
IF (N_ELEMENTS(dtype) LT 5 AND p_ind GT 3) THEN p_ind= 0
add1= 0
IF (p_ind gt 3) THEN add1= 1 

;cyr=['????','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014',$
;      '2015','2016','2017','2018','2019','2010','2011','2012','2013','2014','2015',$
;      '2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026',$
;      '2027','2028','2029','2030']
;
;cmo=['??','01','02','03','04','05','06','07','08','09','10','11','12']
;cdy=['??','01','02','03','04','05','06','07','08','09','10','11','12', $
;     '13','14','15','16','17','18','19','20','21','22','23','24','25','26','27', $
;     '28','29','30','31']
;chr=['??','00','01','02','03','04','05','06','07','08','09','10','11','12', $
;     '13','14','15','16','17','18','19','20','21','22','23']
;cmn=['??','00','01','02','03','04','05','06','07','08','09','10','11','12', $
;     '13','14','15','16','17','18','19','20','21','22','23','24','25','26','27', $
;     '28','29','30','31','32','33','34','35','36','37','38','39','40','41','42', $
;     '43','44','45','46','47','48','49','50','51','52','53','54','55','56','57',$ 
;     '58','59'] 
;cse=cmn
;vcyr= cyr(0)
;vcmo= cmo(0)
;vcdy= cdy(0)
;vchr= chr(0)
;vcmn= cmn(0)
;vcse= cse(0)
search_string='????????_??????_????*fts'

;qflist= ['All','Clear','Al+1a','Al+1b','Al+2','None'] ; EUVI only
qflist= ['All','S1','Dbl','S2','Open','None'] ; EUVI only
qfltval= 'All'

;qplist= ['All','304','195','284','171','None']
qplist=        ['   All  ','   304  ','   195  ','   284  ','   171  ','  None  ']
qplist= [qplist,'    1001','    1002','    1003','    1004','1001 + 0']
qplist= [qplist,STRING(INDGEN(144)*2.5,'(F8.1)')]
qpolval= 'All'

qdstlist= ['All','SSR1','SSR2','RT','SW']
qdstlist= ['All','SSR1','SSR2','RT','SW','notSW']
qdstval='All'

;qprglist= ['All','Norm','Doub','Dark','Led','Cont','Sequ']
qprglist= ['All','Norm','Doub','Dark','Led','Cont','Sequ','SERI','notSERI']
qprogval= 'All'

qcmplist= ['All','HDRO','NONE','RICE',$
           'HC*','HC0','HC1','HC2','HC2','HC4','HC5','HC6','HC7','HC8','HC9',$
           'ICER*','ICER0','ICER1','ICER2','ICER3','ICER4','ICER5','ICER6','ICER7','ICER8','ICER9',$
           'ICER10','ICER11']
qcmpval='All'

;sortlist=['Time','Filename','Telescope']
sortlist=['DateObs','Filename','Telescope']
sort_mtd= sortlist[0] ; Time
last_sort_mtd=''
sort_order= 0 ;Ascending
last_sort_mtd= last_sort_mtd+STRTRIM(sort_order,2)

;help,n_params()

IF (KEYWORD_SET(help)) THEN BEGIN
  PRINT,''
  PRINT,'SCCLISTER function provides a graphical/non-graphical user interface for retrieving a'
  PRINT,'list of SECCHI images.'
  PRINT,''
  PRINT,'1. Call SCCLISTER with /help keyword to display this help page:'
  PRINT,'   idl> files= scclister(/help)' 
  PRINT,''
  PRINT,'2. Call SCCLISTER without any parameters and keywords to get a graphical user interface:'
  PRINT,'   idl> files= scclister([/AORB])'
  PRINT,''
  PRINT,'3. Call SCCLISTER with date (date-range) and telescope parameters (required) and optional'
  PRINT,'   keywords to avoid graphical interface:'
  PRINT,''
  PRINT,'   Required parameters (case sensitive):'
  PRINT,"   date or date range: '20070804' or '20070804..20070805'"
  PRINT,"   telescope:          'EUVI', 'COR1', 'COR2', 'HI_1', or 'HI_2'
  PRINT,''
  PRINT,'   Possible keywords and possible values. All keywords must be be enclosed in single quotes'
  PRINT,'   and are case sensitive:'
  PRINT,'   KEYWORD        VALUES'
  PRINT,'   =======        ======='
  PRINT,"   sc             'a' or 'b' (default = a and b)" 
  PRINT,"   data_level     'Level-05', or 'Level-1' (default = Level-05)"
  PRINT,"   data_src       'lz', or 'pb' or 'rt' (default = lz)"
  PRINT,"   imgtype        'intensity', 'polarized', or 'calibration', or 'TB (double)+polarized' or 'TBPP'  or 'TB (double)+TBPP', (default = intensity)"
  PRINT,"   size           '1024' (use for both x and y) or '1024..512' (x=1024, y=512)"
  PRINT,"   exp            '1199.9' or '1190..1200' (for a range)" 
  PRINT,"   filt           'S1', Dbl', 'S2', or 'Open' (EUVI only)"
  PRINT,"   polar          '304', '195', '284', and '171' for EUVI"
  PRINT,"                  '0.0', '2.5', '5.0', ......., or '357.5' for CORs" 
  PRINT,"   osnum          '1516', '> 1516', '<1516', or '1516..1518'"
  PRINT,"   sebprog        'Norm', 'Doub', 'Dark', 'Led', 'Cont', 'Sequ', 'SERI', or 'notSERI'" 
  PRINT,"   dest           'SSR1', 'SSR2', 'RT', 'SW', or 'notSW'
  PRINT,"   compression    'HDRO', 'NONE', 'RICE', 'HC*', 'HC0', 'HC1', 'HC2', 'HC2', 'HC4', 'HC5',"
  PRINT,"                  'HC6', 'HC7', 'HC8', 'HC9', 'ICER*', 'ICER0', 'ICER1', 'ICER2', 'ICER3',"
  PRINT,"                  'ICER4', 'ICER5', 'ICER6', 'ICER7', 'ICER8', 'ICER9', 'ICER10', or 'ICER11'"
  PRINT,"   sort_type      'Ascending', 'Descending', or 'Nosort' (default = Ascending)"
  PRINT,"   sort_by        'DateObs','Filename', or 'Telescope' (default = DateObs) - ignored if sort_type='Nosort'" 
  PRINT,''
  PRINT,'   Examples:'
  PRINT,"   idl> files= scclister('20070804..20070805','HI_1')"
  PRINT,"   idl> files= scclister('20070804..20070805','HI_1',sc='a',size='1024..1024',exp='1199.9')"
  PRINT,"   idl> files= scclister('20070804..20070805','COR1',sc='A',imgtype='polarized',polar='120.0')
  PRINT,"   idl> files= scclister('20070804','EUVI',sc='A',imgtype='intensity',filt='S1',polar='304')
  PRINT,"   idl> files= scclister('20070804','EUVI',sc='A',imgtype='intensity',osnum='1640')
  PRINT,"   idl> files= scclister('20070901','COR2',sc='B',imgtype='TB (double)+polarized',dest='SSR1',sort_type='Ascending',sort_by='DateObs')
  PRINT,''
  PRINT,'   Note: Values for some keywords are case sensitive. Please enter keywords'
  PRINT,'         exactly as provided and between quotes.'

  PRINT,''

  RETURN,''
ENDIF

IF (n_params() gt 0) THEN BEGIN
  date= STRCOMPRESS(date,/REMOVE_ALL)
  iby= WHERE(year EQ STRMID(date,0,4))
  ibm= WHERE(month EQ STRMID(date,4,2))
  ibd= WHERE(day EQ STRMID(date,6,2))
  iey= iby
  iem= ibm
  ied= ibd
  IF (STRPOS(date,'..') GT 0) THEN BEGIN
    iey= WHERE(year EQ STRMID(date,10,4))
    iem= WHERE(month EQ STRMID(date,14,2))
    ied= WHERE(day EQ STRMID(date,16,2))
  ENDIF

  tel= STRCOMPRESS(STRUPCASE(tel),/REMOVE_ALL)
  IF (STRMid(tel,0,2) eq 'HI') THEN tel= STRMID(tel,0,2)+'_'+STRMID(tel,STRLEN(tel)-1,1) 
  sel_id[(where(sec_inst eq tel))(0)]= 1

  sc_sel= 2
  IF (KEYWORD_SET(scid)) THEN BEGIN
    IF (STRUPCASE(scid) EQ 'A') THEN sc_sel= 0
    IF (STRUPCASE(scid) EQ 'B') THEN sc_sel= 1
  ENDIF

  p_ind= 0
  IF (KEYWORD_SET(imgtype)) THEN BEGIN
    ;IF (STRPOS(STRUPCASE(imgtype),'INTENSITY') GE 0) THEN p_ind= 0
    ;IF (STRPOS(STRUPCASE(imgtype),'POLARIZED') GE 0) THEN p_ind= 1
    ;IF (STRPOS(STRUPCASE(imgtype),'CALIBRATION') GE 0) THEN p_ind= 2
    ;IF (STRPOS(STRUPCASE(imgtype),'TB (DOUBLE)+POLARIZED') GE 0) THEN p_ind= 3
    ;IF (STRPOS(STRUPCASE(imgtype),'TBPP') GE 0) THEN p_ind= 4 
    CASE STRTRIM(STRUPCASE(imgtype),2) OF
      'INTENSITY':             p_ind= 0
      'POLARIZED':             p_ind= 1
      'CALIBRATION':           p_ind= 2
      'TB (DOUBLE)+POLARIZED': p_ind= 3
      'TBPP':                  p_ind= 4
      'TB (DOUBLE)+TBPP':      p_ind= 5
      'IMG':            p_ind= 0
      'SEQ':            p_ind= 1
      'CAL':            p_ind= 2
      'IMG+SEQ':    	p_ind= 3
      'POL':            p_ind= 4
      'IMG+POL':      	p_ind= 5
      ELSE: BEGIN
        PRINT,'Invalid imgtype: '+imgtype
        RETURN,'Invalid imgtype: '+imgtype
      END
    ENDCASE

  ENDIF

  add1= 0 
  IF (p_ind GT 3) THEN add1= 1 

  lvl_ind= 0
  IF (KEYWORD_SET(data_level)) THEN BEGIN
    IF (STRPOS(STRUPCASE(data_level),'LEVEL-05') GE 0) THEN lvl_ind=0
    IF (STRPOS(STRUPCASE(data_level),'LEVEL-1') GE 0) THEN BEGIN
      lvl_ind=1
      IF (STRPOS(tel,'COR') LT 0) THEN BEGIN
        PRINT,''
        PRINT,'WARNING: Level-1 data is only valid for COR1/COR2 TB images.'
        PRINT,''
        RETURN,''
      ENDIF
    ENDIF
  ENDIF
  sec_level = sec_lvl(lvl_ind)
  
  IF (KEYWORD_SET(data_src)) THEN BEGIN
    IF (STRPOS(STRUPCASE(data_src),'LZ') GE 0) THEN src_ind= 0
    IF (STRPOS(STRUPCASE(data_src),'PB') GE 0) THEN src_ind= 1
    IF (STRPOS(STRUPCASE(data_src),'RT') GE 0) THEN src_ind= 2
  ENDIF 
  src_used = sources(src_ind)

  qexpt1val=''
  qexpt2val=''
  IF (KEYWORD_SET(exp)) THEN BEGIN
    exp= STRCOMPRESS(exp,/REMOVE_ALL)
    toks=str_sep(exp,'..')
    qexpt1val=toks(0)
    qexpt2val=toks(0)
    IF (N_ELEMENTS(toks) GT 1) THEN qexpt2val=toks(1)
  ENDIF

  qxsizeval= ''
  qysizeval= ''
  IF (KEYWORD_SET(size)) THEN BEGIN
    size= STRCOMPRESS(size,/REMOVE_ALL)
    toks=str_sep(size,'..')    
    qxsizeval=toks(0)
    qysizeval=toks(0)
    IF (N_ELEMENTS(toks) GT 1) THEN qysizeval=toks(1)
  ENDIF

  qosnval= ''
  IF (KEYWORD_SET(osnum)) THEN $ 
    qosnval= STRCOMPRESS(osnum,/REMOVE_ALL)

  qfltval= 'All'
  IF (KEYWORD_SET(filt)) THEN $
    qfltval= STRCOMPRESS(filt,/REMOVE_ALL)

  qpolval= 'All'
  IF (KEYWORD_SET(polar)) THEN $
    qpolval= STRCOMPRESS(polar,/REMOVE_ALL)

  qdstval='All'
  IF (KEYWORD_SET(dest)) THEN $
    qdstval= STRCOMPRESS(dest,/REMOVE_ALL)

  qcmpval='All'
  IF (KEYWORD_SET(compression)) THEN $
    qcmpval= STRCOMPRESS(compression,/REMOVE_ALL)

  qprogval= 'All'
  IF (KEYWORD_SET(sebprog)) THEN $
    qprogval= STRCOMPRESS(sebprog,/REMOVE_ALL)

  sort_order= 0 ;Ascending
  IF (KEYWORD_SET(sort_type)) THEN BEGIN 
    IF (STRUPCASE(STRCOMPRESS(sort_type,/REMOVE_ALL)) EQ 'NOSORT') THEN BEGIN
      sort_order= -1
    ENDIF ELSE BEGIN
      IF (STRUPCASE(STRCOMPRESS(sort_type,/REMOVE_ALL)) EQ 'ASCENDING') THEN $
        sort_order= 0 $
      ELSE $ 
        sort_order= 1 
    ENDELSE
  ENDIF

  sort_mtd= sortlist[0] 
  IF (KEYWORD_SET(sort_by)) THEN BEGIN 
    IF (STRUPCASE(STRCOMPRESS(sort_by,/REMOVE_ALL)) EQ 'FILENAME') THEN $
      sort_mtd= 'Filename'
    IF (STRUPCASE(STRCOMPRESS(sort_by,/REMOVE_ALL)) EQ 'TELESCOPE') THEN $
      sort_mtd= 'Telescope' 
  ENDIF

  qtelval= 'All'

 ;help, iby,ibm,ibd,iey,iem,ied,sel_id,sc_sel,p_ind,src_used
 ;help,qexpt1val,qexpt2val,qxsizeval,qysizeval
 ;stop

  uv1= {draw:0} 
  files= GET_FILES(year,month,day,iby,ibm,ibd,iey,iem,ied, $
                   sec_inst, sel_id, sc_sel, nfiles, fmsg, sec_level, $
                   dtype, p_type, p_ind, uv1.draw, selfiles, uv1, src_used)

  IF sort_order NE -1 THEN $
    SORT_ROWS,files,matches,sort_mtd,sort_order,sord, add1
  SUBQUERY,files,uv1,/DONE
  IF (files(0) NE '') THEN $
    PRINT,'Returning '+STRTRIM(N_ELEMENTS(files),2)+' Matches.'

  ;help,files,matches
  ;RETURN, files
  RETURN, matches
ENDIF

 
IF (XRegistered("scclister") NE 0) THEN RETURN,-1

DEVICE, GET_SCREEN_SIZE= scr
IF (debugon) THEN print,'SCREEN_SIZE=',scr
xs1=0
ys1=0
IF scr[0] LT 1300 or scr[1] LT 700 THEN BEGIN
    xs1=(scr[0]-40)<1295
    ys1=(scr[1]-40)<700
ENDIF

ms.WListerBase = WIDGET_BASE(TITLE = "SECCHI Image Selection Tool v"+version,/ROW, $
                             XOFFSET=5, YOFFSET=5, X_SCROLL_SIZE=xs1,Y_SCROLL_SIZE=ys1)

lcol      = WIDGET_BASE(ms.WListerBase, /COLUMN)

lcol1= WIDGET_BASE(ms.WListerBase,/COLUMN,/FRAME)
;txt=WIDGET_LABEL(lcol1,VALUE= 'Search Results:')

;colhdr='       FileName                  DateObs          Tel   Exptme  Xsize  Ysize  Filter   Polar  Prog   OSnum  Dest   FPS    '
;colhdr='      FileName                DateObs        Tel    Exptme  Xsize Ysize  Filter  Polar   Prog   OSnum  Dest   FPS    '
;colhdr='      FileName                 DateObs        Tel    Exptme  Xsize  Ysize  Filter  Polar   Prog   OSnum   Dest   FPS   LED '
;colhdr=' FileName                    DateObs              Tel   Exptme  Xsize  Ysize  Filter  Polar   Prog   OSnum  Dest   FPS    LED '

;colhdr=' FileName                    DateObs              Tel   Exptme  Xsize  Ysize  Filter  Polar   Prog   OSnum  Dest   FPS     LED   Cmpr'

colhdr=' FileName                    DateObs              Tel   Exptme  Xsize  Ysize  Filter  Polar   Prog   OSnum  Dest   FPS     LED   Cmpr'

;arow='20070131_020252_s4c2A.fts | 2007/01/31 02:02:52 | COR2 |   4.0| 2112 | 2048 |  None | 120.0 | Sequ | 1353 | SSR1 |     |   None'

;txt=WIDGET_LABEL(lcol1,VALUE= colhdr,/align_left)
txt=WIDGET_LABEL(lcol1,VALUE= colhdr,scr_xsize=820,scr_ysize=15,/align_left)
;draw= WIDGET_LIST(lcol1,UVALUE='drawn',/frame)

draw= WIDGET_LIST(lcol1,UVALUE='drawn',scr_xsize=820,scr_ysize=400,/frame)

lcol0= WIDGET_BASE(lcol1,/COL,/FRAME)

txt=WIDGET_LABEL(lcol0,VALUE= 'Sub-Select Options')

lcol2= WIDGET_BASE(lcol0,/ROW)
;qclist= ['All','EUVI','COR1','COR2','HI1','HI2']
qclist= ['All','EUVI','COR1','COR2','HI_1','HI_2']
qtelval= 'All' 

lcolflds= WIDGET_BASE(lcol2,/ROW)

lcolfld= WIDGET_BASE(lcolflds,/COL)

;txt=WIDGET_LABEL(lcolfld, VALUE= 'Telescope')
txt=WIDGET_LABEL(lcolfld, VALUE= 'Tel')
qtel = WIDGET_DROPLIST(lcolfld, VALUE=qclist, UVALUE="QTEL")
WIDGET_CONTROL,qtel,SENSITIVE=0

qexpt1val=''
lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'ExpBeg')
qexpt1= WIDGET_TEXT(lcolfld, VALUE='',UVALUE="QEXPT1", XSIZE=5,YSIZE=1,/EDITABLE)
WIDGET_CONTROL,qexpt1,SENSITIVE=0

qexpt2val=''
lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'ExpEnd')
qexpt2= WIDGET_TEXT(lcolfld, VALUE='',UVALUE="QEXPT2", XSIZE=5,YSIZE=1,/EDITABLE)
WIDGET_CONTROL,qexpt2,SENSITIVE=0

qxsizeval= ''
lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'Xsize')
qxsize= WIDGET_TEXT(lcolfld, VALUE='',UVALUE="QXZ", XSIZE=5,YSIZE=1,/EDITABLE)
WIDGET_CONTROL,qxsize,SENSITIVE=0

qysizeval= ''
lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'Ysize')
qysize= WIDGET_TEXT(lcolfld, VALUE='',UVALUE="QYZ", XSIZE=5,YSIZE=1,/EDITABLE)
WIDGET_CONTROL,qysize,SENSITIVE=0

lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'Filter')
qfltr= WIDGET_DROPLIST(lcolfld, VALUE=qflist, UVALUE="QFLT")
WIDGET_CONTROL,qfltr,SENSITIVE=0

lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'Polar')
;qpolr= WIDGET_DROPLIST(lcolfld, VALUE=qplist, UVALUE="QPOL")
;qpolr= WIDGET_COMBOBOX_SHELL(lcolfld, qplist, 0)
qpolr= WIDGET_COMBOBOX(lcolfld, VALUE=qplist)
WIDGET_CONTROL,qpolr, SET_COMBOBOX_SELECT= 0
WIDGET_CONTROL,qpolr,SENSITIVE=0

lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'SebProg')
qprog= WIDGET_DROPLIST(lcolfld, VALUE=qprglist, UVALUE="QPROG")
WIDGET_CONTROL,qprog,SENSITIVE=0

qosnval= ''
lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'OsNum')
qosn= WIDGET_TEXT(lcolfld, VALUE='', UVALUE="QOS", XSIZE=5,YSIZE=1,/EDITABLE)
WIDGET_CONTROL,qosn,SENSITIVE=0

lcolfld= WIDGET_BASE(lcolflds,/COL)
txt=WIDGET_LABEL(lcolfld, VALUE= 'Dest')
qdst= WIDGET_DROPLIST(lcolfld, VALUE=qdstlist, UVALUE="QDST")
WIDGET_CONTROL,qdst,SENSITIVE=0

lcolfld= WIDGET_BASE(lcolflds,/COL)
;txt=WIDGET_LABEL(lcolfld, VALUE= 'Cmpr')
txt=WIDGET_LABEL(lcolfld, VALUE= 'Compression')
qcmp= WIDGET_DROPLIST(lcolfld, VALUE=qcmplist, UVALUE="QCMP")
WIDGET_CONTROL,qcmp,SENSITIVE=0

font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
if not allow_font(font) then get_font, font

lcol3= WIDGET_BASE(lcol0,/ROW,/FRAME)

;txt= WIDGET_LABEL(lcol3,VALUE= '  ')
txt= WIDGET_LABEL(lcol3,VALUE= '                      ')
qry= WIDGET_BUTTON(lcol3, VALUE='Query Sub set', UVALUE="QRY", FONT=font,/ALIGN_LEFT,/FRAME)
WIDGET_CONTROL,qry,SENSITIVE=0
txt= WIDGET_LABEL(lcol3,VALUE= '                      ')
rmvsub= WIDGET_BUTTON(lcol3, VALUE='Remove Sub set', UVALUE="REMOVESUB", FONT=font,/ALIGN_LEFT,/FRAME)
WIDGET_CONTROL,rmvsub,SENSITIVE=0
txt= WIDGET_LABEL(lcol3,VALUE= '                      ')
;reset= WIDGET_BUTTON(lcol3, VALUE='ReloadOrigResult', UVALUE="RESET")
reset= WIDGET_BUTTON(lcol3, VALUE='ReloadOrig', UVALUE="RESET",/ALIGN_RIGHT,/FRAME)
WIDGET_CONTROL,reset,SENSITIVE=0
txt= WIDGET_LABEL(lcol1, VALUE='')
txt= WIDGET_LABEL(lcol1, VALUE='')

;;sortlist=['Time','Filename','Telescope']
;sortlist=['DateObs','Filename','Telescope']
;sort_mtd= sortlist(0) ; Time
;last_sort_mtd=''
lcol4= WIDGET_BASE(lcol1,/ROW)
txt= WIDGET_LABEL(lcol4, valUE='                              Sort Display by')
;sort_order= 0 ;Ascending
last_sort_mtd= last_sort_mtd+STRTRIM(sort_order,2)
sorder= WIDGET_DROPLIST(lcol4, VALUE=['Ascending','Descending'], UVALUE="SORDER")
WIDGET_CONTROL,sorder,SENSITIVE=0
sortid= WIDGET_DROPLIST(lcol4, VALUE=sortlist, UVALUE="SRT_SEL")
WIDGET_CONTROL,sortid,SENSITIVE=0
txt= WIDGET_LABEL(lcol4, VALUE='  ')
sortit= WIDGET_BUTTON(lcol4, VALUE='Sort', UVALUE="SORT", FONT=font)
WIDGET_CONTROL,sortit,SENSITIVE=0

dobs1= ''
dobs2= ''

;lcol3= WIDGET_BASE(lcol1,/ROW,/FRAME)
;txt= WIDGET_LABEL(lcol3, VALUE='Start DateObs at ') 
;cad1 = WIDGET_TEXT(lcol3, VALUE='', UVALUE="CAD1", XSIZE=20,YSIZE=1,/EDITABLE)
;WIDGET_CONTROL,cad1,SENSITIVE=0
;txt= WIDGET_LABEL(lcol3, VALUE='using cadence 0f ~')
;cad2 = WIDGET_TEXT(lcol3, VALUE='', UVALUE="CAD2", XSIZE=10,YSIZE=1,/EDITABLE)
;txt= WIDGET_LABEL(lcol3, VALUE='seconds.')
;
;WIDGET_CONTROL,cad2,SENSITIVE=0
;txt= WIDGET_LABEL(lcol3, VALUE='     ')
;cadence= WIDGET_BUTTON(lcol3, VALUE='Apply Cadence', UVALUE="DO_CAD")
;WIDGET_CONTROL,cadence,SENSITIVE=0
;

lcol5= WIDGET_BASE(lcol1,/ROW,/FRAME)
;txt= WIDGET_LABEL(lcol5, VALUE='                      ')
txt= WIDGET_LABEL(lcol5, VALUE='    Save Displayed Image File Names and Info as')
savename= 'scc_images.sav'
sname= WIDGET_TEXT(lcol5,VALUE=savename,UVALUE="SNAME",XSIZE=15,YSIZE=1,/EDITABLE)
WIDGET_CONTROL,sname,SENSITIVE=0
txt= WIDGET_LABEL(lcol5, VALUE='consisting of images.fnames and images.info')
saveid= WIDGET_BUTTON(lcol5, VALUE='Save', UVALUE="SAVEID", FONT=font)
WIDGET_CONTROL,saveid,SENSITIVE=0

txt= WIDGET_LABEL(lcol1, VALUE='')
;fnsh = WIDGET_BUTTON(lcol1, VALUE='Done/SaveResults', UVALUE = 'FNSH', FONT=font)
fnsh = WIDGET_BUTTON(lcol1, VALUE='Done - Return Filenames', UVALUE = 'FNSH', FONT=font)

junk = { CW_PDMENU_S, flags:0, name:'' }

pdm_desc = [ $
	     { CW_PDMENU_S, 0, 'Status' }, 	$
	     { CW_PDMENU_S, 1, 'Options' }, 	$
	     { CW_PDMENU_S, 0, 'Location' },	$
	     { CW_PDMENU_S, 0, 'Settings' }, 	$
	     { CW_PDMENU_S, 2, 'View Catalog'},	$
	     ;{ CW_PDMENU_S, 0, 'Header' }, 	$
	     { CW_PDMENU_S, 0, 'Help' }, 	$
	     { CW_PDMENU_S, 2, 'Done' } ]

pdm_desc = [ $
             { CW_PDMENU_S, 0, 'Help' },        $
             { CW_PDMENU_S, 2, 'Done' } ]

;pdm_desc = [ $
;             { CW_PDMENU_S, 0, 'Help' }]

pdm_desc = [ $
             { CW_PDMENU_S, 0, 'Help' },        $
             { CW_PDMENU_S, 2, 'PointAndClickFiles' }]
             ;{ CW_PDMENU_S, 2, 'Point, Click, and Save Files' }]
             ;{ CW_PDMENU_S, 2, 'Point-Click-And-Save Filenames' }]


menu = CW_PDMENU(lcol, pdm_desc, UVALUE = 'PDMENU', /RETURN_FULL_NAME)

files=[""]

;ff = findfile(img_dir)
ff = FILE_SEARCH(img_dir)

 IF ff(0) EQ '' THEN ff = ['20050101']

;stop
; ff = yymmdd(ff)
 nn = n_elements(ff)

 tyear = STRCOMPRESS(STRMID(ff(0),0,2),/REMOVE_ALL)


IF (DATATYPE(ibd) EQ 'UND') THEN ibd=0
IF (DATATYPE(ied) EQ 'UND') THEN ied=0
IF (DATATYPE(ibm) EQ 'UND') THEN ibm=0
IF (DATATYPE(iem) EQ 'UND') THEN iem=0
IF (DATATYPE(iby) EQ 'UND') THEN iby=0
IF (DATATYPE(iey) EQ 'UND') THEN iey=0

bday= day(ibd)
eday= day(ied)
bmonth= month(ibm)
emonth= month(iem)
byear= year(iby)
eyear= year(iey)
 
 DatePath = ff
 iip = 0
 iil = 0
 idp = 0
 icx = 0
 
llcol= WIDGET_BASE(lcol,/COLUMN,/FRAME)
lbl01= WIDGET_LABEL(llcol,VALUE='Loading Methods:')
junk = WIDGET_BASE(llcol,COLUMN=4,/EXCLUSIVE)
bf = lonarr(nmodes)
for i=0,nmodes-1 do $
  bf(i) = WIDGET_BUTTON(junk, VALUE=ms.mnames(i), UVALUE=ms.mnames(i), $
                            /NO_RELEASE)
                            
junk = WIDGET_BASE(lcol,/FRAME,/COLUMN)
mode_base = WIDGET_BASE(junk)

IF keyword_set(VERBOSE) THEN print,'start for loop'
for i=0,nmodes-1 do $
    ms.mbases(i) = WIDGET_BASE(mode_base, UVALUE=0L,/COLUMN)
    
;********************************************************************
;**** img_hdr.txt load ****
;********************************************************************

parent = ms.mbases(0)

IF keyword_set(VERBOSE) THEN print,' ;*** Instrument selection ***'

 xjunk = WIDGET_BASE(parent,column=1)
 bl1 = WIDGET_BASE(xjunk,/COLUMN)

 scft= WIDGET_BASE(bl1,/ROW)
 tmp = WIDGET_LABEL(scft,VALUE="Select STEREO SpaceCraft: ")
 IF keyword_set(AORB) THEN sclist= ['STEREO A','STEREO B'] ELSE $
 sclist= ['STEREO A','STEREO B','STEREO A+B (all images)','STEREO A+B (synched images)']
 ;sc_sel= 0 
 IF (DATATYPE(sc_sel) EQ 'UND') THEN sc_sel= 0 
 ;scp = WIDGET_DROPLIST(scft, VALUE=sclist, UVALUE="SCFT")
 scp = CW_BSELECTOR2(scft, sclist, UVALUE="SCFT", SET_VALUE=sc_sel)
 

 ;bl2 = WIDGET_BASE(bl1,/ROW)
 ;seli  = WIDGET_BASE(bl2,/ROW,/EXCLUSIVE)


 IF keyword_set(VERBOSE) THEN print,';*** Data level selection ***'

 r6  = WIDGET_BASE(bl1, /ROW)
 ;b6  = WIDGET_LABEL(r6,VALUE="Select Filetype and Source:")
 b6  = WIDGET_LABEL(r6,VALUE="Select Data Level:")

 rf  = WIDGET_BASE(r6,/ROW )	;,/EXCLUSIVE)

;ilevel= ['level_1']
;b64 = WIDGET_DROPLIST(rf, VALUE=ilevel, UVALUE="IP2")
;b63 = WIDGET_DROPLIST(rf, VALUE=ImgPath(0), UVALUE="IP2") ; only level-1

;b64 = WIDGET_DROPLIST(rf, VALUE=sec_lvl, UVALUE="IP2")
;b64 = WIDGET_DROPLIST(rf, VALUE=slevel, UVALUE="IP2")
b64 = CW_BSELECTOR2(rf, slevel, UVALUE="IP2", SET_VALUE= lvl_ind)

IF KEYWORD_SET(VERBOSE)  THEN PRINT,' *** SECCHI INSTRUMENT SELECTION ***'
 bl3 = WIDGET_BASE(bl1,/ROW)
 ;li3 = WIDGET_LABEL(bl3,VALUE = "SECCHI Instrument(s):")
 li3 = WIDGET_LABEL(bl3,VALUE = "Instrument(s):")
 ;sseli  = WIDGET_BASE(bl3,/ROW,/EXCLUSIVE)
 ;p_ssel0 = WIDGET_BUTTON(sseli,VALUE=' EUVI ',UVALUE='euvi',/NO_RELEASE)
 ;p_ssel1 = WIDGET_BUTTON(sseli,VALUE=' COR1 ',UVALUE='cor1',/NO_RELEASE)
 ;p_ssel2 = WIDGET_BUTTON(sseli,VALUE=' COR2 ',UVALUE='cor2',/NO_RELEASE)
 ;p_ssel3 = WIDGET_BUTTON(sseli,VALUE=' HI1  ',UVALUE='hi1',/NO_RELEASE)
 ;p_ssel4 = WIDGET_BUTTON(sseli,VALUE=' HI2  ',UVALUE='hi2',/NO_RELEASE)
 ;p_ssel5 = WIDGET_BUTTON(sseli,VALUE=' All  ',UVALUE='All',/NO_RELEASE)

 sseli  = WIDGET_BASE(bl3,/ROW,/NONEXCLUSIVE)
 ;sec_inst= ['EUVI','COR1','COR2','HI1','HI2']
 FOR i=0, ninst-1 DO sec_select(i)= WIDGET_BUTTON(sseli,VALUE=sec_inst(i), $
                                    UVALUE='SEL_INST')

 IF keyword_set(VERBOSE) THEN print,';*** Data Source selection ***'

 data_src= ['lz (Images from level-0 packets)',     $
            'pb (Images from SSR playback socket)', $
            'rt (Images rpm RealTime telemetry)']

 s1= WIDGET_BASE(bl1, /ROW)
 s2= WIDGET_LABEL(s1,VALUE="Select Data Source:")
 s3= WIDGET_BASE(s1,/ROW )    ;,/EXCLUSIVE)
 ;s4= WIDGET_DROPLIST(s3, VALUE=data_src, UVALUE="SRC")
 s4= CW_BSELECTOR2(s3, data_src, UVALUE="SRC",SET_VALUE=src_ind)

 r4= WIDGET_BASE(bl1, /ROW)
 txt= WIDGET_LABEL(r4,VALUE="Select Data Type:")

 IF (DATATYPE(p_ind) EQ 'UND') THEN BEGIN
   p_ind= 0 
   add1=0
 ENDIF ELSE BEGIN
   IF (p_ind GT 3) THEN BEGIN
     p_ind= 0
     add1=0
   ENDIF
 ENDELSE


 ;b63 = WIDGET_DROPLIST(r4, VALUE=dtype, UVALUE="SEB_PROG")
 b63 = CW_BSELECTOR2(r4, dtype, UVALUE="SEB_PROG", SET_VALUE=p_ind)
 

IF keyword_set(VERBOSE) THEN print,';*** Observation date selection ***'

 r6  = WIDGET_BASE(bl1, /ROW)
 b6  = WIDGET_LABEL(r6,VALUE="Select Observation Date:")

   rf1  = WIDGET_BASE(bl1, /FRAME,/COLUMN)
   r5  = WIDGET_BASE(rf1, /ROW)
   b5  = WIDGET_LABEL(r5,VALUE="From : ")

   w_m_b = WIDGET_DROPLIST(r5, VALUE=month, UVALUE="BM")
   w_d_b = WIDGET_DROPLIST(r5, VALUE=day,   UVALUE="BD")
   w_y_b = WIDGET_DROPLIST(r5, VALUE=year,  UVALUE="BY")
   w_s_e = WIDGET_BUTTON(r5, VALUE='Same  end  date', UVALUE="SE")

   r6  = WIDGET_BASE(rf1, /ROW)
   b6  = WIDGET_LABEL(r6,VALUE="To   : ")

   w_m_e = WIDGET_DROPLIST(r6, VALUE=month, UVALUE="EM")
   w_d_e = WIDGET_DROPLIST(r6, VALUE=day,   UVALUE="ED") 
   w_y_e = WIDGET_DROPLIST(r6, VALUE=year,  UVALUE="EY")
   w_s_b = WIDGET_BUTTON(r6, VALUE='Same begin date', UVALUE="SB")


 r8  = WIDGET_BASE(bl1, /ROW)
 ;b8  = WIDGET_LABEL(r8,VALUE="File Matching Pattern (Single Telescope Only):")
 b8  = WIDGET_LABEL(r8,VALUE="Restrict File Selection (? = all possible values for 1 char)")
 r9 = WIDGET_BASE(bl1, /ROW)
 b9 = WIDGET_LABEL(r9,VALUE="Edit as needed and Enter:")
 r10 = WIDGET_BASE(bl1, /ROW)


;   rfc  = WIDGET_BASE(bl1, /FRAME,/COLUMN)
;   r8  = WIDGET_BASE(rfc, /COL)
; txt= WIDGET_LABEL(r8, VALUE='yyyy      mn       dd   _   hh       mm       ss')

 ;bl4= WIDGET_BASE(bl1,/ROW)
; bl4= WIDGET_BASE(r8,/ROW)


 ;cyrid= WIDGET_DROPLIST(bl4,VALUE=cyr, UVALUE="CYR", TITLE="Pattern Match")
 ;cmoid= WIDGET_DROPLIST(bl4,VALUE=cmo, UVALUE="CMO")
 ;cdyid= WIDGET_DROPLIST(bl4,VALUE=cdy, UVALUE="CDY")
 ;chrid= WIDGET_DROPLIST(bl4,VALUE=chr, UVALUE="CHY")
 ;cmnid= WIDGET_DROPLIST(bl4,VALUE=cmn, UVALUE="CMN")
 ;cseid= WIDGET_DROPLIST(bl4,VALUE=cse, UVALUE="CSE")

; txt= WIDGET_LABEL(bl4,VALUE='  ')

 ;vcyr= cyr(0)
; cyrid= WIDGET_COMBOBOX(bl4,VALUE=cyr,UVALUE="CYR",/EDITABLE,XSIZE=60)
; WIDGET_CONTROL, cyrid, SET_COMBOBOX_SELECT= 0
; WIDGET_CONTROL, cyrid, SENSITIVE=1

 ;vcmo= cmo(0)
; cmoid= WIDGET_COMBOBOX(bl4,VALUE=cmo,UVALUE="CMO",/EDITABLE,XSIZE=50)
; WIDGET_CONTROL, cmoid, SET_COMBOBOX_SELECT= 0
; WIDGET_CONTROL, cmoid, SENSITIVE=1

 ;vcdy= cdy(0)
; cdyid= WIDGET_COMBOBOX(bl4,VALUE=cdy,UVALUE="CDY",/EDITABLE,XSIZE=50)
; WIDGET_CONTROL, cdyid, SET_COMBOBOX_SELECT= 0
; WIDGET_CONTROL, cdyid, SENSITIVE=1
 
 ;vchr= chr(0)
; chrid= WIDGET_COMBOBOX(bl4,VALUE=chr,UVALUE="CHR",/EDITABLE,XSIZE=50)
; WIDGET_CONTROL, chrid, SET_COMBOBOX_SELECT= 0
; WIDGET_CONTROL, chrid, SENSITIVE=1

 ;vcmn= cmn(0)
; cmnid= WIDGET_COMBOBOX(bl4,VALUE=cmn,UVALUE="CMN",/EDITABLE,XSIZE=50)
; WIDGET_CONTROL, cmnid, SET_COMBOBOX_SELECT= 0
; WIDGET_CONTROL, cmnid, SENSITIVE=1

 ;vcse= cse(0)
; cseid= WIDGET_COMBOBOX(bl4,VALUE=cse,UVALUE="CSE",/EDITABLE,XSIZE=50)
; WIDGET_CONTROL, cseid, SET_COMBOBOX_SELECT= 0 
; WIDGET_CONTROL, cseid, SENSITIVE=1

 ;fsid= WIDGET_LABEL(r8,VALUE= vcyr+vcmo+vcdy+"_"+vchr+vcmn+vcse+"_*.fts",/FRAME)
 fsid=  WIDGET_TEXT(r10,VALUE= search_string,UVALUE="FSID",XSIZE=28,ysize=1,/editable,/FRAME)
 ;qexpt1= WIDGET_TEXT(lcolfld, VALUE='',UVALUE="QEXPT1", XSIZE=5,YSIZE=1,/EDITABLE)


 ;font='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'

 bl3 = WIDGET_BASE(bl1,/ROW)
 txt = WIDGET_LABEL(bl3,VALUE='                       ')
 b3  = WIDGET_BUTTON(bl3,VALUE='  Submit  ',UVALUE='FIND',FONT=font)
 

 ;lab = WIDGET_LABEL(bl3, VALUE="                                                                                                       ",FONT=font)
 r7 = WIDGET_BASE(lcol, /COL)
 txt= WIDGET_LABEL(r7,VALUE= '')
 txt= WIDGET_LABEL(r7,VALUE= '')
 txt= WIDGET_LABEL(r7,VALUE= '')
 txt= WIDGET_LABEL(r7,VALUE= '')
 lab = WIDGET_LABEL(r7, vALUE="                                                                                                       ",FONT=font)
 lab1= WIDGET_LABEL(r7, vALUE="                                                                                                       ",FONT=font)
 lab2= WIDGET_LABEL(r7, vALUE="                                                                                                       ",FONT=font)
 
widget_control, parent, set_uvalue = xjunk

sws = ''
ml = strlen(message(0))
ws = (xs - ml) / 2
while strlen(sws) LT ws do sws = sws + ' ' ;Make ws chars long
message(0) = sws + message(0)

r6  = WIDGET_BASE(lcol, /COLUMN)
r7  = WIDGET_BASE(r6, /ROW)

;
; Create a structure that will be carried around as the UVALUE of the main base (WLOADBASE)
;


;CASE sec_sflag OF
; 0: BEGIN & WIDGET_CONTROL,p_ssel0, /SET_BUTTON & sec_inst='euvi' & END
; 1: BEGIN & WIDGET_CONTROL,p_ssel1, /SET_BUTTON & sec_inst='cor1' & END
; 2: BEGIN & WIDGET_CONTROL,p_ssel2, /SET_BUTTON & sec_inst='cor2' & END
; 3: BEGIN & WIDGET_CONTROL,p_ssel3, /SET_BUTTON & sec_inst='hi1' & END
; 4: BEGIN & WIDGET_CONTROL,p_ssel4, /SET_BUTTON & sec_inst='hi2' & END
; 5: BEGIN & WIDGET_CONTROL,p_ssel5, /SET_BUTTON & sec_inst='All' & END
; ELSE: donothing=0
;ENDCASE

dirs = ''
wl_uv = {  WListerBase:ms.WListerBase,   $  ; Base widget ID
           bf:bf,		  $  ; Widget buttons ID
           r7:r7,                 $
           files:files,           $
           dirs:dirs,             $                        
           nname:nn,	          $  ; Widget ID for the File text
           ;npath:np,              $          
           b_day:w_d_b,           $  ; Widget ID for From : day button
           b_month:w_m_b, 	  $  ; Widget ID for From : month button
           b_year:w_y_b,          $  ; Widget ID for From : year button
           e_day:w_d_e, 	  $
           e_month:w_m_e, 	  $
           e_year:w_y_e,          $
           w_s_e:w_s_e,		  $
	   w_s_b:w_s_b,		  $          
           bmonth:bmonth,	  $
           bday:bday,		  $
           byear:byear,		  $
           emonth:emonth,	  $
           eday:eday,		  $
           eyear:eyear,		  $
           seb_id:b63,            $
           ;cid:cid,		  $
           idc:b3,		  $
;           iLevel:iLevel,         $
           lpath2:b64,		  $
           ImgPath:ImgPath,       $
           DatePath:DatePath,     $
           ;Cx:Cx, 		  $
           n_old:n_old,		  $
           lab:lab,		  $
           lab1:lab1,             $
           lab2:lab2,             $
           draw:draw,             $
	   rf1:rf1 }

for i=0, nmodes-1 do widget_control, ms.mbases(i), MAP=0
WIDGET_CONTROL, ms.WListerBase, /REALIZE

;
; Load the structure into the main base's UVALUE for one way trip to handler
;

WIDGET_CONTROL, ms.WListerBase, SET_UVALUE = wl_uv, /NO_COPY
sel_method = 0
               
WIDGET_CONTROL, ms.mbases(0), map=1, /NO_COPY
WIDGET_CONTROL, bf(0), /SET_BUTTON
 

 IF n_elements(ibd) EQ 0 THEN ibd = ti1(0)
 IF n_elements(ied) EQ 0 THEN ied = ti2(0)

;WIDGET_CONTROL,b63,SET_DROPLIST_SELECT=sec_ipi
 
WIDGET_CONTROL,w_d_b,SET_DROPLIST_SELECT=ibd
WIDGET_CONTROL,w_d_e,SET_DROPLIST_SELECT=ied
WIDGET_CONTROL,w_m_b,SET_DROPLIST_SELECT=ibm
WIDGET_CONTROL,w_m_e,SET_DROPLIST_SELECT=iem

WIDGET_CONTROL,w_y_b,SET_DROPLIST_SELECT=iby
WIDGET_CONTROL,w_y_e,SET_DROPLIST_SELECT=iey
              
WIDGET_CONTROL, lab, SET_VALUE= 'Is "secchi" Environment Variable Set Correctly?'
WIDGET_CONTROL, lab1, SET_VALUE= 'secchi = "'+GETENV('secchi')+'"' 

XMANAGER,"scclister",ms.WListerBase, $               
                EVENT_HANDLER = "scclister_event", $
                GROUP_LEADER = GROUP, MODAL = keyword_set(modal)

IF datatype(sipath) EQ 'UND' THEN sipath = ''
return,sipath

END


