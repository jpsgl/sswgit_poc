;+
; $Id: sccingest.pro,v 1.8 2011/11/30 22:54:00 thompson Exp $
; Project     :	STEREO - SECCHI
;
; Name        :	SCCINGEST
;
; Purpose     :	Ingest and catalog SECCHI FITS files
;
; Category    :	SECCHI, Catalog
;
; Explanation :	This routine takes a collection of SECCHI FITS files and
;               arranges them into a data tree.  Also creates or updates
;               summary files describing the data.
;
; Syntax      :	SCCINGEST, [ DIRECTORY ]  [, FILES=FILES ]
;
; Examples    :	SCCINGEST, '$HOME'
;               SCCINGEST, FILES=FILE_SEARCH('20061104*.fts')
;
; Inputs      :	None required.
;
; Opt. Inputs :	DIRECTORY = String array containing one or more directories to
;                           search for FITS files.
;
; Outputs     :	Generates summary files.
;
; Opt. Outputs:	None.
;
; Keywords    :	FILES   = String array containing a list of files to process.
;                         If passed, then overrides the DIRECTORY parameter.
;
;               NOSUB   = If set, don't descend into subdirectories.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then a null string is
;                         returned.  In order to use this feature, ERRMSG must
;                         be defined first, e.g.
;
;                               ERRMSG = ''
;                               SCCINGEST, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
; Env. Vars.  : Either $secchi or $SECCHI_LZ.  See scc_data_path.pro for a
;               complete list of supported environment variables.
;
; Calls       :	FXHREAD, SCC_DATA_PATH, FILE_EXIST, FILE_MKDIR, FILE_MOVE,
;               CONCAT_DIR, SCC_FITSHDR2STRUCT, SCCWRITESUMMARY
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; $Log: sccingest.pro,v $
; Revision 1.8  2011/11/30 22:54:00  thompson
; Distinguish between COR1 "n" and "s" files after 1-Jan-2009
;
; Revision 1.7  2010/05/03 19:14:00  thompson
; *** empty log message ***
;
; Revision 1.6  2010-04-27 19:22:19  nathan
; doorstat=3 (OFF) should not be a cal image
;
; History     :	Version 1, 20-Jun-2006, William Thompson, GSFC
;               Version 2, 12-Apr-2007, William Thompson, GSFC
;                       Call SCCWRITESUMMARY
;               Version 3, 02-Oct-2007, WTT, Added /NOSUB, more error catching
;		Version 4, 19-Nov-2009, WTT, make loop variable long
;
; Contact     :	WTHOMPSON
;-
;
pro sccingest, directory, files=k_files, nosub=nosub, errmsg=errmsg
;
on_error, 2
on_ioerror, ioerror
;
;  Get  the list of files to process.
;
if n_elements(k_files) eq 0 then begin
    if n_elements(directory) eq 0 then begin
        message = 'Either DIRECTORY or the FILES keyword must be passed'
        goto, handle_error
    endif
    if keyword_set(nosub) then $
      files = file_search(concat_dir(directory, '*.fts')) else $
      files = file_search(directory, '*.fts')
end else files = k_files
;
files = strtrim(files,2)
w = where(files ne '', count)
if count eq 0 then begin
    message = 'No files found'
    goto, handle_error
endif
files = files[w]
;
;  Step through and process each file.
;
for ifile=0L,n_elements(files)-1 do begin
    openr, unit, files[ifile], /get_lun
    fxhread, unit, header, status
    free_lun, unit
    if status ne 0 then begin
        print, 'Error reading file ' + files[ifile] + ' -- skipping'
    end else begin
;
;  From the header, determine the telescope, the date, the observatory, and
;  other information needed to determine the file type.  Also make sure that it
;  really is a SECCHI file.
;
        header = scc_fitshdr2struct(header)
        observatory = strtrim(strupcase(header.obsrvtry), 2)
        instrument  = strtrim(strupcase(header.instrume), 2)
        detector    = strtrim(strupcase(header.detector), 2)
        filename    = strtrim(header.filename, 2)
        doorstat    = header.doorstat
        if ((observatory ne 'STEREO_A') and (observatory ne 'STEREO_B')) or $
          (instrument ne 'SECCHI') then $
          print, 'Skipping non-SECCHI file ' + files[ifile] else begin
;
;  Determine the type of file.
;
            type = 'img'
            char = strmid(filename,16,1)
            case char of
                'k' : type = 'cal' ; DARK
                'e' : type = 'cal' ; LED
                'c' : type = 'cal' ; CONT
                else:
            endcase
            if (detector eq 'COR1') then begin
                case strmid(filename,16,1) of
                    'n' : if strmid(filename,0,4) lt '2009' then type ='seq' ; NORMAL
                    's' : type ='seq' ; SERIES
                    else:
                endcase
            endif
            if (detector eq 'COR2') then begin
                case strmid(filename,16,1) of
                    'n' : type ='seq' ; NORMAL
                    's' : type ='seq' ; SERIES
                    else:
                endcase
            endif
;
;  If door is not open (2) or in transit (1) or off (3) then put in cal directory.
;
            if (doorstat lt 1) or (doorstat gt 3) then type = 'cal'
;
;  Form the directory to store the file.
;
            observatory = strmid(observatory, 7, 1)
            date = strmid(filename, 0, 8)
            if not valid_num(date) then $
              print, 'Skipping misnamed file ' + files[ifile] else begin
                datapath = scc_data_path(observatory, date=date, type=type, $
                                         telescope=detector)
;
;  Make sure the directory exists, and move the file into the directory.
;
                if not file_exist(datapath) then file_mkdir, datapath
                file_move, files[ifile], datapath, /allow_same, /overwrite
;
;  Form the name of the summary file.  Make sure the summary directory exists.
;
                sumpath = scc_data_path(observatory, type='summary')
                if not file_exist(sumpath) then file_mkdir, sumpath
                sumfile = concat_dir(sumpath, 'scc' + observatory + $
                                     strmid(filename,0,6) + '.' + type)
                case detector of
                    'COR1': sumfile = sumfile + '.c1'
                    'COR2': sumfile = sumfile + '.c2'
                    'EUVI': sumfile = sumfile + '.eu'
                    'HI1':  sumfile = sumfile + '.h1'
                    'HI2':  sumfile = sumfile + '.h2'
                    else:
                endcase
;
;  Write the header information to the summary file.
;
                sccwritesummary, header, sumfile, /nolock
            endelse             ;Filename correct
        endelse                 ;SECCHI file
    endelse                     ;Able to read FITS header
endfor                          ;ifile
;
;  Exit point.
;
return
;
;  Error handling point.
;
ioerror: message = !error_state.msg
;
handle_error:
if n_elements(unit) gt 0 then free_lun, unit
if n_elements(errmsg) eq 0 then message, message else $
  errmsg = 'sccingest: ' + message
;
end
