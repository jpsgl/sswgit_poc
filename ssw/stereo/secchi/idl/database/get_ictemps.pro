function get_ictemps,sc,d1,d2,t1,t2,res=res
; $Id: get_ictemps.pro,v 1.3 2011/04/06 21:08:45 secchib Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : get_ictemps
;               
; Purpose     : get converted ceb temps from database
;               
; Explanation : 
;               
; Use         : IDL> ictemps=get_ictemps,sc,d1,d2,t1,t2,res=10
;    
; Inputs      :   sc = A or B 
;   	    	  Mnemonics = not used always returns hi(0) and scip(1) CEB temps
;   	     	  d1 = yyyymmdd  first day or full day requested
;    	    	    	    if only d1 is set then the full day is returned
;   	    	  d2 = ending day optional
;   	    	  t1 = start time hhmm for d1 optional
;   	    	  t2 = end time hhmm for d2 optional
;               
; Outputs     :   ictemps = structure of packettimes, unix_time, and mnemonics
;
; Keywords :	res = resolution in minutes (60,30,10,1,0.1) 0.1 for full, defaults to 1 min
;               
; Calls from LASCO : none
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Database 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Jun 2007
;               
; Modified    :
;
; $Log: get_ictemps.pro,v $
; Revision 1.3  2011/04/06 21:08:45  secchib
; nr - do not change inputs
;
; Revision 1.2  2011/03/25 21:44:48  nathan
; allow any time format as input
;
; Revision 1.1  2007/06/13 14:51:07  mcnutt
; new pro reads in ceb temps from database
;
;
;
;ic_temps='ic_hk_temps_'+strlowcase(sc) 
ic_temps='hk_ic_'+strlowcase(sc) 
secchi_hk='secchi_flight_hk_'+strupcase(sc)
;mnemonics='hi_hk_temp,scip_hk_temp'
mnemonics='ichihktemp,icsciphktemp'
resol=' group by mid('+ic_temps+'.pckt_time,1,16)'
if(keyword_set(res)) then begin
 if (Res eq 60) then resol=' group by mid('+ic_temps+'.pckt_time,1,13)'
 if (Res eq 30) then resol=' and (CAST('+ic_temps+'.pckt_time as CHAR) like "%:00:0%" OR CAST('+ic_temps+'.pckt_time as CHAR) like "%:30:0%") group by mid('+ic_temps+'.pckt_time,1,16)'
 if (Res eq 10) then resol=' and CAST('+ic_temps+'.pckt_time as CHAR) like "%:%0:0%" group by mid('+ic_temps+'.pckt_time,1,16)'
 if (Res eq 0.1) then resol=''
endif

ut1=nrlanytim2utc(d1)
time1=utc2str(ut1,/ecs)
if(datatype(t1) NE 'UND')then begin
    t1=string(t1,'(i4.4)')
    time1=strmid(time1,0,11)+strmid(t1,0,2)+':'+strmid(t1,2,2)
endif

if(datatype(d2) NE 'UND')then begin
    ut2=nrlanytim2utc(d2)
endif ELSE BEGIN
    ut2=ut1
ENDELSE
time2=utc2str(ut2,/ecs)
if(datatype(t2) NE 'UND')then begin
    t2=string(t2,'(i4.4)')
    time2=strmid(time2,0,11)+strmid(t2,0,2)+':'+strmid(t2,2,2)
endif

IF time1 GE time2 THEN BEGIN
    ut2.mjd=ut2.mjd+1
    time2=utc2str(ut2,/ecs)
ENDIF

cmnd='select pckt_time,unix_time,'+mnemonics+' from '+ic_temps+' where pckt_time between "'+time1+'" and "'+time2+'"'+resol
stemps=SQDB (secchi_hk,cmnd)
z=size(stemps)
if (z(1) ne 7) then begin
    tags=tag_names(stemps) & ntags=n_Elements(tags)
    ictemps=create_struct(tags(0),stemps.(0),tags(1),stemps.(1))
    for i=2,ntags-1 do begin
    	cnv_str= GET_CONVERT_STRING(tags(i),GETENV('SSW_SECCHI')+'/idl/database/hk1067_mnemonics_info.txt')
    	SECCHI_DN_CONVERT, stemps.(i), REPLICATE(cnv_str,n_elements(stemps.(i))), dn_cnvt, cnv_units
    	ictemps=create_struct(ictemps,tags(i),dn_cnvt)
    endfor
endif else ictemps=stemps

return,ictemps

end
