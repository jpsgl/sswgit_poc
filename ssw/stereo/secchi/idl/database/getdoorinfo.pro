function getdoorinfo, scid, d1, d2, REPORT=report
;+
; $Id: getdoorinfo.pro,v 1.1 2007/03/08 15:43:48 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : getdoorinfo
;               
; Purpose   : get opening and closing information for all SCIP doors for given S/C and time interval
;               
; Explanation: 
;               
; Use       : IDL> info=getdoorinfo('a','2006-10-27','2007-01-02')
;    
; Inputs    : 
;               
; Outputs   : structure of packettime (string & unix), and HKEVENT mnemonics 
;		(see $SSW_SECCHI/data/secchi_hk_scevent_table_def.html)
;
; Keywords  : 
;
;   /REPORT 	Generate a text file with structure info translated to english
;
; Calls from LASCO : getenv_slash.pro
;
; Common    : SCC_REDUCE
;               
; Restrictions: Calls local MySQL secchi database
;               
; Side effects: 
;               
; Category    : database operations doors 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2004
;               
; $Log: getdoorinfo.pro,v $
; Revision 1.1  2007/03/08 15:43:48  nathan
; *** empty log message ***
;
;-            

; FSW tables are defined in $SSW_SECCHI/idl/database/fswtableids.txt

; HKEVENTCODE 951: Emergency Door Closure Commanded 
; HKEVENTDAT1,2,3 [None,EUVI,COR1,COR2]
;
; HKEVENTCODE 955; Door Operation Started 
; HKEVENTDAT1: [None,COR2,COR1,EUVI]
; HKEVENTDAT2: 0/1:Cls/OpnLoop | 0/1:Opn/Cls | 0/1:Hi/LoFreq: close=00000100
; HKEVENTDAT3: Number of steps commanded.





common sccevent, uploadevts, scevts, tableids

; For now at least, just save two event structures, and parse per tableid for each table 
; and each image.

ab=strlowcase(scid)
utc_obs=anytim2utc(d1)

;IF keyword_set(RESET) OR datatype(scevts)     EQ 'UND' THEN BEGIN
	scevts1=gethkevents(ab,d1,d2,CODE=951)	; Emerg door close
	scevts2=gethkevents(ab,d1,d2,CODE=955)	; Door operation
	scevts=[scevts1,scevts2]
	srt=sort(scevts.unix_time)
	scevts=scevts[srt]
;ENDIF
;;
teldat1= ['    ','EUVI','COR1','COR2']
teldat2= ['    ','COR2','COR1','EUVI']

nr=n_elements(scevts)

IF keyword_set(REPORT) THEN BEGIN
    IF keyword_set(DIR) THEN sdir=dir ELSE sdir=getenv('HOME')
    outfile=concat_dir(sdir,'doorinfo'+utc2yymmdd(utc_obs)+'.txt')
    openw,rlun,outfile,/get_lun
    FOR i=0,nr-1 DO BEGIN
    	dat1=scevts[i].HKEVENTDAT1
    	dat2=scevts[i].HKEVENTDAT2
    	dat3=scevts[i].HKEVENTDAT3
	strt=scevts[i].pckt_time
	dir =dat2 and 'F00'XL
	IF dir EQ 256 THEN dirstr='CLOSE' ELSE dirstr='OPEN '
	IF scevts.HKEVENTCODE EQ 951 THEN $
	    line=strt+'  CLOSE  '+teldat1[dat1]+'  '+teldat1[dat2]+'  '+teldat1[dat3]
	ELSE $
	    line=strt+'  '+dirstr+'  '+teldat2[dat1]
	print,line
    	printf,rlun,line
    ENDFOR
    close,rlun
    free_lun,rlun
ENDIF
return,scevts

end
