FUNCTION secchipw2str,cameras,pws,obs_nums,pq_num,scid,exptime2

  n = N_ELEMENTS(cameras)
  polars= STRARR(n)+'None'

  ;cor_str= STRING(FINDGEN(144)*2.5,'(F5.1)')

  c= WHERE(cameras EQ 3, cnt) ; EUVI
  IF (cnt GT 0) THEN BEGIN
    IF (pq_num EQ 1) THEN BEGIN
      p= WHERE(pws(c)/6 EQ 0, pcnt)
      IF (pcnt GT 0) THEN polars(c(p))= '304A'
      p= WHERE(pws(c)/6 EQ 1, pcnt)
      IF (pcnt GT 0) THEN polars(c(p))= '195A'
      p= WHERE(pws(c)/6 EQ 2, pcnt)
      IF (pcnt GT 0) THEN polars(c(p))= '284A'
      p= WHERE(pws(c)/6 EQ 3, pcnt)
      IF (pcnt GT 0) THEN polars(c(p))= '171A'
    ENDIF ELSE BEGIN ; pq_num=2 => polar2 elements (only for double images).
      ;nd= WHERE(obs_nums(c) EQ 0, ndcnt)  ; pickup only double images 
      nd= WHERE(exptime2(c) GT 0.0, ndcnt)  ; pickup only double images using exptime2
      IF (ndcnt GT 0) THEN BEGIN
        p= WHERE(pws(c(nd))/6 EQ 0, pcnt)
        IF (pcnt GT 0) THEN polars(c(nd(p)))= '304A'
        p= WHERE(pws(c(nd))/6 EQ 1, pcnt)
        IF (pcnt GT 0) THEN polars(c(nd(p)))= '195A'
        p= WHERE(pws(c(nd))/6 EQ 2, pcnt)
        IF (pcnt GT 0) THEN polars(c(nd(p)))= '284A'
        p= WHERE(pws(c(nd))/6 EQ 3, pcnt)
        IF (pcnt GT 0) THEN polars(c(nd(p)))= '171A'
      ENDIF
    ENDELSE
  ENDIF

  c1a= WHERE(cameras EQ 2 AND scid EQ 'A',c1acnt)
  IF(c1acnt GT 0) THEN pws(c1a)= pws(c1a)-4 ; COR1-A 0.0 deg starts at pos #4
  c= WHERE(cameras EQ 1 OR cameras EQ 2, cnt) ; COR2 and COR1
  ;IF (cnt GT 0) THEN polars(c)= STRING(pws(c)*2.5,'(F5.1)')
  IF (cnt GT 0) THEN BEGIN
    IF (pq_num EQ 1) THEN BEGIN
      polars(c)= STRTRIM(STRING(pws(c)*2.5,'(F5.1)'),2)+'deg'
    ENDIF ELSE BEGIN ; pq_num=2 => polar2 elements (only for double images).
      ;nd= WHERE(obs_nums(c) EQ 0, ndcnt)  ; pickup only double images
      nd= WHERE(exptime2(c) GT 0.0, ndcnt)  ; pickup only double images using exptime2
      IF (ndcnt GT 0) THEN BEGIN
        polars(c(nd))= STRTRIM(STRING(pws(c(nd))*2.5,'(F5.1)'),2)+'deg'
      ENDIF
    ENDELSE
  ENDIF

  c= WHERE(cameras  GT 3, cnt) ; 4=HI2 and 5=HI1
  IF (cnt GT 0) THEN polars(c)= 'None' 

  RETURN,polars

END 
