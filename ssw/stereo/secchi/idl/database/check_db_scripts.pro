pro check_db_scripts,dtst,dtend,hkupdate=hkupdate, _EXTRA=_extra
;+
; $Id: check_db_scripts.pro,v 1.2 2012/12/03 12:30:21 secchib Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : check_db_scripts
;               
; Purpose   : to unzip and copy database scripts from pipeline archive to the database.
;               
; Explanation: 
;               
; Use       : IDL> check_db_scripts,'YYYYMMDD','YYYYMMDD'
;    
; Inputs    :	dtst= starting day yyyy-mm-dd (or yyyymmdd)
;		dtend= ending day yyyy-mm-dd 
;               
; Outputs   : 
;
; Keywords  : hkupdate = if set the HK database update command will be sent
;   	      _EXTRA  
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need permissions write in /net/lambda/data/mysql/mysql_x/databases/secchi/HK/
;               
; Side effects: 
;               
; Category    : database
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2012
;               
; $Log: check_db_scripts.pro,v $
; Revision 1.2  2012/12/03 12:30:21  secchib
; added commit to end of db scripts
;
; Revision 1.1  2012/11/30 19:22:06  secchib
; new program to copy script file to the hk database that where not rsync due to network erros
;

ut0=nrlanytim2utc(dtst)
IF n_params() LT 2 THEN BEGIN
    ut1=ut0
ENDIF ELSE ut1=nrlanytim2utc(dtend)
runhkupdate=0

mjd=ut0
ab=getenv('sc');aorb ;['a','b']
FOR i=ut0.mjd,ut1.mjd DO BEGIN
    mjd.mjd=i
    yyyymmdd=utc2yymmdd(mjd,/yyyy)
    doy=utc2doy(mjd)
    yyyydoy=strmid(yyyymmdd,0,4)+string(doy,'(i3.3)')
    dbscripts=file_search('/net/lambda/data/mysql/mysql_x/databases/secchi/HK/INSERTS/SC_'+strupcase(ab)+'/*/IN_DB/*'+yyyydoy+'*.script')
    zipscripts=file_search('~/lz/outputs/prints/attic/*'+yyyydoy+'*.script.gz')
    if zipscripts(0) ne '' then begin 
      if n_elements(dbscripts) lt 11 then begin
       if dbscripts(0) ne '' then BREAK_FILE, dbscripts, a, outdir, dbname, outext else dbname=['']
       BREAK_FILE, zipscripts, a, outdir, zpname, outext
       for n=0,n_elements(zpname)-1 do begin
          tmp=where(dbname eq zpname(n),cnt)
	  if cnt eq 0 then begin
	    runhkupdate=1
            syscmnd='gunzip '+zipscripts(n)
	    spawn,syscmnd
	    if strpos(zpname(n),'currs') gt -1 then odir='/CURRENTS/'
	    if strpos(zpname(n),'gtraw') gt -1 then odir='/POINTING/'
	    if strpos(zpname(n),'temps') gt -1 then odir='/TEMPS/'
	    if strpos(zpname(n),'doors') gt -1 then odir='/DOORS/'
	    if strpos(zpname(n),'volts') gt -1 then odir='/VOLTS/'
	    if strpos(zpname(n),'scevt') gt -1 then odir='/EVENTS/'
	    if strpos(zpname(n),'gsevt') gt -1 then odir='/EVENTS/'
	    if strpos(zpname(n),'fm') gt -1 then odir='/FMHK/'
	    if strpos(zpname(n),'ic') gt -1 then odir='/ICHK/'
	    if strpos(zpname(n),'ih') gt -1 then odir='/IHHK/'
	    if strpos(zpname(n),'tm') gt -1 then odir='/TMHK/'
	    if strpos(zpname(n),'xf') gt -1 then odir='/XFHK/'
	    FILE_COPY,'~/lz/outputs/prints/attic/'+zpname(n)+'.script','/net/lambda/data/mysql/mysql_x/databases/secchi/HK/INSERTS/SC_'+strupcase(ab)+odir,/verbose
            syscmnd='gzip ~/lz/outputs/prints/attic/'+zpname(n)+'.script'
	    spawn,syscmnd
            openw,1,'/net/lambda/data/mysql/mysql_x/databases/secchi/HK/INSERTS/SC_'+strupcase(ab)+odir+zpname(n)+'.script',/append
	    printf,1,'commit;'
	    close,1
	  endif
       endfor
     endif
   endif
ENDFOR
if (keyword_set(hkupdate) and runhkupdate eq 1) then begin
  syscmnd='ssh nathan@lambda ./scc_hk_update2 '+strlowcase(ab)
  spawn,syscmnd
endif
end
