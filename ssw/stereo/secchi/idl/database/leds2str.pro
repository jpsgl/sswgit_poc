; Ed Esfandiari - Sep 2004
; Function to convert LED id numbers to LED position/color.

FUNCTION LEDS2STR,cameras,led_ids,obs_nums

  n = N_ELEMENTS(cameras)
  ledstr= STRARR(n)+'None'

  leds= WHERE(obs_nums EQ 3,lcnt)
  IF (lcnt GT 0) THEN BEGIN
    c= WHERE(cameras(leds) EQ 1, cnt) ; EUVI
    IF (cnt GT 0) THEN BEGIN
      l0= WHERE(led_ids(leds(c)) EQ 0, l0cnt)
      IF (l0cnt GT 0) THEN ledstr(leds(c(l0)))= "FPA Blue"
      l1= WHERE(led_ids(leds(c)) EQ 1, l1cnt) 
      IF (l1cnt GT 0) THEN ledstr(leds(c(l1)))= "TEL Blue"
      l2= WHERE(led_ids(leds(c)) EQ 2, l2cnt)
      IF (l2cnt GT 0) THEN ledstr(leds(c(l2)))= "TEL Purple" 
    ENDIF
    c= WHERE(cameras(leds) EQ 2, cnt) ; COR1
    IF (cnt GT 0) THEN BEGIN
      l0= WHERE(led_ids(leds(c)) EQ 0, l0cnt) 
      IF (l0cnt GT 0) THEN ledstr(leds(c(l0)))= "FPA Red"
      l1= WHERE(led_ids(leds(c)) EQ 1, l1cnt) 
      IF (l1cnt GT 0) THEN ledstr(leds(c(l1)))= "FPA Blue"
      l2= WHERE(led_ids(leds(c)) EQ 2, l2cnt)
      IF (l2cnt GT 0) THEN ledstr(leds(c(l2)))= "FPA Violet"
    ENDIF
    c= WHERE(cameras(leds) EQ 3, cnt) ; COR2
    IF (cnt GT 0) THEN BEGIN
      l0= WHERE(led_ids(leds(c)) EQ 0, l0cnt) 
      IF (l0cnt GT 0) THEN ledstr(leds(c(l0)))= "TEL Red (door-close)"
      l1= WHERE(led_ids(leds(c)) EQ 1, l1cnt) 
      IF (l1cnt GT 0) THEN ledstr(leds(c(l1)))= "FPA Blue"
      l2= WHERE(led_ids(leds(c)) EQ 2, l2cnt)
      IF (l2cnt GT 0) THEN ledstr(leds(c(l2)))= "FPA Violet"
    ENDIF
    c= WHERE(cameras(leds) GT 3, cnt) ; HI1 and HI2
    IF (cnt GT 0) THEN BEGIN
      l0= WHERE(led_ids(leds(c)) EQ 0, l0cnt) 
      IF (l0cnt GT 0) THEN ledstr(leds(c(l0)))= "FPA Red"
      l1= WHERE(led_ids(leds(c)) EQ 1, l1cnt) 
      IF (l1cnt GT 0) THEN ledstr(leds(c(l1)))= "FPA Blue"
      l2= WHERE(led_ids(leds(c)) EQ 2, l2cnt)
      IF (l2cnt GT 0) THEN ledstr(leds(c(l2)))= "FPA Violet"
    ENDIF
  ENDIF

  RETURN,ledstr

END
