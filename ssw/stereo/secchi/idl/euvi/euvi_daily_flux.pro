pro euvi_daily_flux,sc,yyyymmdd1,yyyymmdd2,outdir=outdir,saveoverlap=saveoverlap
;+
; $Id: euvi_daily_flux.pro,v 1.3 2011/12/01 21:05:51 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : euvi_daily_flux
;
; Purpose : creats text files of the EUVI total flux and daily averages,
;           of full disk and overlap regions. output files are write to 
;          getenv('SSWDB')+'/stereo/secchi/statistics/euvi/irradiance/
;
; Use     : IDL>euvi_daily_flux,'b','20090419','20090518'
;
; Inputs  :
;    sc = spacecraft 'a' or b'
;    yyyymmdd1 =first day
;    yyyymmdd2 =last day
;
; Outputs :
;    getenv('SSWDB')+'/stereo/secchi/statistics/euvi/irradiance/
;
; Keywords:
;    /outdir : set to output directory if files are not to be written to SSWDB
;    /saveoverlap :creates a png image of overlap region.
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Daily Irradiances
;
; Prev. Hist. :
;
; Written     : Lynn Simpson
;
; $Log: euvi_daily_flux.pro,v $
; Revision 1.3  2011/12/01 21:05:51  nathan
; use select_windows for windows compat
;
; Revision 1.2  2011/08/09 18:46:48  mcnutt
; corrected for days with no overlap
;
; Revision 1.1  2009/06/18 14:23:16  mcnutt
; creates euvi total flux text files in SSWDB
;
;


ut0=anytim2utc(strmid(yyyymmdd1,0,4)+'-'+strmid(yyyymmdd1,4,2)+'-'+strmid(yyyymmdd1,6,2))
if datatype(yyyymmdd2) ne 'UND' then ut1=anytim2utc(strmid(yyyymmdd2,0,4)+'-'+strmid(yyyymmdd2,4,2)+'-'+strmid(yyyymmdd2,6,2)) else ut1=ut0

mjd=ut0

wls=[171,195,284,304]
FOR i=ut0.mjd,ut1.mjd DO BEGIN

   mjd.mjd=i
   yyyymmdd_do=utc2yymmdd(mjd,/yyyy)
 
  if keyword_set(outdir) then outdir=outdir else outdir=getenv('SSWDB')+'/stereo/secchi/statistics/euvi/irradiance/'
  yyyymm=strmid(yyyymmdd_do,0,6) 
  yyyy=strmid(yyyymmdd_do,0,4) 
  afout=outdir+strupcase(sc)+'_daily_average_'+yyyy+'.txt'
 
  files=file_search(getenv('secchi')+'/lz/L0/'+strlowcase(sc)+'/img/euvi/'+yyyymmdd_do+'*/*.fts')
  ft=where(strmid(files,strpos(files(0),'.f')-4,1) ne 7)
  if ft(0) gt -1 then files=files(where(strmid(files,strpos(files(0),'.f')-4,1) ne 7)) else files=''
    irrads=CREATE_STRUCT('date_obs','','total_irrad',0.0,'overlap_irrad',0.0,'WAVELNTH',0,'CRLN_OBS',0.0,'DSUN_OBS',0.0d)
    irrads= REPLICATE(irrads, n_elements(files))
  
    for n=0,n_elements(files)-1 do begin
;  img=readfits(files(n),hdr)
     if files(n) ne '' then begin
       secchi_prep,files(n), hdr,image,/rectify,/normal_off
     
       if hdr.naxis1 eq hdr.naxis2 and hdr.mask_tbl eq 'NONE' then begin ;stop 
 
         if n eq 0 then begin
          overlap=get_euvi_overlap(hdr,/ushelio)
          if keyword_set(saveoverlap) then begin
            set_plot,'z'
            device,set_resolution=[hdr.naxis1/4,hdr.naxis2/4]        
            tv,rebin(image,hdr.naxis1/4,hdr.naxis2/4)+rebin(overlap,hdr.naxis1/4,hdr.naxis2/4)*100
            pngfile=outdir+'SECCHI_'+strupcase(sc)+'_EUVI_OVERLAP_'+yyyymmdd_do
            tmp=ftvread(filename=pngfile,/png,/noprompt)
            select_windows  
	   endif
         endif

        irrads(n).date_OBS=hdr.date_OBS
        irrads(n).total_irrad=total(image)
	ovl=where(overlap eq 255,cnt)
        if cnt ge 1 then irrads(n).overlap_irrad=total(image(where(overlap eq 255)))
        irrads(n).WAVELNTH=hdr.WAVELNTH
        irrads(n).CRLN_OBS=hdr.CRLN_OBS
        irrads(n).DSUN_OBS=hdr.DSUN_OBS
       endif
     endif
  endfor


  if n_elements(files) gt 1 then begin
    tmp=finite(irrads.total_irrad)
    irrads=irrads(where(tmp eq 1))
    adj_irrad=irrads.total_irrad*((irrads.DSUN_OBS/1.495978706e11)^2)
    adj_ovirrad=irrads.overlap_irrad*((irrads.DSUN_OBS/1.495978706e11)^2)
    average=fltarr(n_elements(wls))
    ovaverage=average
    for nw=0,n_elements(wls)-1 do begin
      dfout=outdir+strupcase(sc)+'_irrads_'+string(wls(nw),'(i3.3)')+'_'+yyyymm+'.txt'
      tmp=file_search(dfout)
      openw,etlun,dfout,/get_lun,/append
      if tmp(0) eq '' then printf,etlun,'DATE                      TOTAL                CRLN         Overlap TOTAL'
      za=where(irrads.wavelnth eq wls(nw))
      aver=total(adj_irrad(za))/n_Elements(za) & za2=where(adj_irrad(za) gt aver-aver*.5 and adj_irrad(za) lt aver+aver*.5)
      ovaver=total(adj_ovirrad(za))/n_Elements(za) & za3=where(adj_ovirrad(za) gt ovaver-ovaver*.5 and adj_ovirrad(za) lt ovaver+ovaver*.5)

      for nz=0,n_Elements(za)-1 do printf,etlun,irrads(za(nz)).date_obs,adj_irrad(za(nz)),irrads(za(nz)).CRLN_OBS,adj_ovirrad(za(nz))
      close,etlun
      free_lun,etlun
      if za2(0) ne -1 then average(nw) = total(adj_irrad(za(za2)))/n_Elements(za2) else average(nw)=-1
      if za3(0) ne -1 then ovaverage(nw) = total(adj_ovirrad(za(za3)))/n_Elements(za3) else ovaverage(nw)=-1
    endfor
    tmp=file_search(afout)
    openw,aflun,afout,/get_lun,/append,width=100
    if tmp(0) eq '' then printf,aflun,format='(a4,11x,4(i3.3,10x),2x,a4,4x,4(i3.3,10x))','DATE',wls,'CRLN',wls
    printf,aflun,format='(a10,4x,4(e12.5,1x),1x,f8.3,4x,4(e12.5,1x))',strmid(irrads(0).date_obs,0,10),average,irrads(0).CRLN_OBS,ovaverage
    close,aflun
    free_lun,aflun
  endif

ENDFOR

end
