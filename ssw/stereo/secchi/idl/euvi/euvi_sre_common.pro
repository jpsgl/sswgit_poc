;NAME:		euvi_sre_common
;PURPOSE:	Define common block that contains the SRE data for 
;		EUVI on STEREO A and B
;MODIFICATION HISTORY:
; 11-Apr-2008, N. V. Lemen
common euvi_sre_db, euvi_sre_common, Filename_common, Full_Filename_common
