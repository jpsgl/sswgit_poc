; $Id: mk_euvi_movie.pro,v 1.6 2008/09/17 19:03:25 nathan Exp $
;
; Project     : STEREO - SECCHI
;                   
; Name        : mk_euvi_movie
;               
; Purpose     : Generates a an EUVI A, B, or A/B subfield movie with standard settings and play with scc_wrunmovie.pro. 
;               
; Explanation : Essentially a wrapper for scc_mkmovie.pro. 
;
; Calling seq:	mk_euvi_movie, date, hours, wavelngth
;               
; Inputs      : date	STR 	'YYYY-MM-DD HH:MM'
;   	    	hours	INT 	Number of hours from date
;   	    	wavelngth   INT     one of 195,171,284,304
;               
; Outputs     : Movie displayed using scc_wrunmovie.pro. User has option to save in a number of formats.
;               
; Example   	: To generate a subfield movie of 195A EUVI A and B from 1500 to 1800 on 2008-01-25:
;
;   	    	    mk_euvi_movie,'2008-01-25 15',3,195, /BOTH
;    
; Keywords    : All keywords for SCC_MKMOVIE.PRO plus
;   	    	/SSR1_ONLY  	Do not retrieve SSR2 images, if any
;   	    	/USE_AHEAD  	Generate list of images from ahead, not Behind (default)
;   	    	/SKIP_MISSING	Skip frames without a corresponding image from other spacecraft
;   	    	/AHEAD, /BEHIND, /BOTH	Which spacecraft to query
;   	    	/PNG	Save each frame as a PNG file in current directory and then call generic_movie
;
; Calls       : scc_mkmovie.pro, scc_read_summary.pro
;
; Side effects: None
;               
; Category    : Image Display movie analysis
;               
; See Also    : WSCC_MKMOVIE.PRO
;               
; Prev. Hist. : Improvement on mk_scc_movie.pro 
;
; History     : N.Rich, 4/2/2008
;               
;-            
;$Log: mk_euvi_movie.pro,v $
;Revision 1.6  2008/09/17 19:03:25  nathan
;commit change from 9/9 (below)
;
;
; 080909, nr - call scc_mkmovie with /nocal
;
;Revision 1.5  2008/07/14 18:43:19  nathan
;added /USE_LAST_COORDS; use /nobeacon
;
;Revision 1.4  2008/04/08 16:33:43  nathan
;added /PNG keyword
;
;Revision 1.3  2008/04/04 18:51:42  nathan
;added PAN keyword (see scc_mkmovie.pro)
;
;Revision 1.2  2008/04/02 16:12:18  nathan
;updated comments
;
;Revision 1.1  2008/04/02 16:02:43  nathan
;front end for scc_mkmovie.pro
;

pro  mk_euvi_movie,date_obs,h1,wavelngth, SSR1_ONLY=ssr1_only, USE_AHEAD=use_ahead, PNG=png, $
    USE_LAST_COORDS=use_last_coords, $
    SKIP_MISSING=skip_missing, BOTH=both, AHEAD=ahead, BEHIND=behind, PAN=pan, _EXTRA=_extra

utcobs=anytim2utc(date_obs)
date=[utcobs, tai2utc( utc2tai(utcobs) + h1*3600.)]

IF keyword_set(PAN) THEN panval=pan ELSE panval=2048
nkp=0
IF keyword_set(BOTH) THEN nkp=1
IF nkp GT 0 and ~keyword_set(SKIP_MISSING) THEN nkp=2

IF keyword_set(USE_AHEAD) or keyword_set(AHEAD) THEN sc='A' ELSE sc='B'

List_B = SCC_READ_SUMMARY(DATE=date,telescope='euvi',spacecraft=sc, source='lz', /nobeacon)

nw=n_elements(wavelngth)

FOR i=0,nw-1 DO BEGIN
    wave=wavelngth[i]
    IF keyword_set(SSR1_ONLY) THEN $
    w_b = where(list_b.value eq wave and list_b.dest eq 'SSR1',num_b) ELSE $
    w_b = where(list_b.value eq wave, num_b)


    if (num_b eq 0) then begin
	print,'no EUVI-',sc,' ',wave,' images for ',utc2str(date[0]),' to ',utc2str(date[1])
	return
    endif else begin 


	print,'Found ',trim(num_b),' images'
	list= list_b(w_b)
	files= SCCFINDFITS(list.FILENAME)




    endelse 
    
    IF (nkp GT 0) THEN scs='AB' ELSE scs=sc
    IF i GT 0 THEN use_last_coords=1
    IF nw GT 1 THEN savef='./euvi'+trim(wave)+strlowcase(scs)+utc2yymmdd(utcobs)+'.mvi' ELSE savef=0

    scc_mkmovie,files,0,0,'euvi',/AUTO,/DCOLOR,TWO=nkp,/GETSUBFIELD,/TIMES,pan=panval, PNG=png, $
    	USE_LAST_COORDS=use_last_coords, SAVE=savef, _EXTRA=_extra, /nocal

    IF keyword_set(PNG) THEN BEGIN

	list1=file_search('./'+strmid(list[0].filename,0,8)+'*'+trim(wavelngth)+'.png')
	nl=n_elements(list1)
	ans=''
	IF nl gt num_b THEN BEGIN
    	    ans=''
    	    read,'Found ',trim(nl),' PNG files--OK to proceed with movie? [y]',ans
	ENDIF
	IF ans EQ '' OR ans EQ 'y' THEN generic_movie,list
    ENDIF
ENDFOR ; -- wavelngth
end


