
function scc_gtdiag,ts,te,s_c,file=file,dir=dir,no_grh=no_grh,nrl=nrl

;+
; $Id: scc_gtdiag.pro,v 1.3 2010/04/13 21:21:08 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gtdiag
;
; Purpose : reads a raw telemetry file (from SSC website) and extracts
;           the data from the gt diagnostic packets (Apid 1066)
;
; Use     : IDL> d = scc_gtdiag(file=file_search('secchi_ahead*ptp'))
;           IDL> d = scc_gtdiag(ts,te,'a',dir='/User/wuelser/telem_a')
;
; Inputs  :
;    ts = start time (anytim format).
;    te = end time.
;    s_c = 'a' or 'b'
; all 3 inputs required unless file keyword supplied
;
; Outputs :
;    d = structure vector containing gt high rate raw data.
;
; Keywords:
;    file = file list  (input, optional)
;    dir = directory for automatic file search (used only if file not supplied)
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;    /nrl : use default dir path at NRL (instead of LMSAL).
;           Ignored if either dir or file are supplied.
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 1-Nov-2006
;               JPW 20-Dec-2006 : added "int" time format for easier utplot
;
; $Log: scc_gtdiag.pro,v $
; Revision 1.3  2010/04/13 21:21:08  nathan
; find correct level-0 directory
;
; Revision 1.2  2007/06/22 18:34:27  mcnutt
; corrected directories
;
; Revision 1.1  2007/01/12 03:19:34  euvi
; initial version
;
;-

n = n_elements(file)
if n eq 0 then begin
   file = scc_time2tlmfile(ts,te,s_c, NRL=nrl,dir=dir)
   n = n_elements(file)
endif

if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words

d0 = {time:0L,day:0L,t:0d0,raw:intarr(4),dac:bytarr(3),n:0B,tunc:0d0}
     ; data structure
d0 = replicate(d0,21)               ; diag packet has 21 data entries
it1  =  6+ghb
it2  = 10+ghb
iraw = (lindgen(84) mod 4)+(lindgen(84)/4)*6+6+ghi  ; index to raw GT data
icnt = lindgen(21)*12 + 20                          ; index to cnt bytes
idac = (lindgen(63) mod 3) + (lindgen(63)/3)*12+21  ; index to dac byte data
toff = (dindgen(21)-20d0)*4d-3      ; time correction for ea. entry time
trc  = 0d0                          ; initial time of previous packet

rec = intarr(136+ghi)               ; 1 packet
dt = 7670L*24L*3600L                ; diff. betw. SC and anytim time def.
apid = 1066                         ; apid for gtdiag data

; intermediate size data vector to improve speed (d=[d,d0] is slow!)
n10 = 1000L
d10 = replicate(d0[0],n10*21L)
i10 = 0L

on_ioerror,endfil
for i=0,n-1 do begin

  openr,lun,file[i],/get_lun
  while 1 do begin
    readu,lun,rec
    if (rec[ghi] and 2047) eq apid then begin
      brc = byte(rec,ghb,272)      ; create byte type copy of record
      trn = double(long(rec,it1)-dt)+double(byte(rec,it2))/256d0
      d0.tunc = trn                ; uncorrected packet time
      trc = trn > (trc+84d-3)      ; make time monotonous
      d0.t = trc + toff
      d0.n = brc[icnt]
      d0.raw = reform(rec[iraw],4,21)
      d0.dac = reform(brc[idac],3,21)

      d10[i10*21L] = d0
      i10 = i10 + 1L
      if i10 eq n10 then begin
         if n_elements(d) gt 0L then d=[d,d10] else d=d10
         i10 = 0L
      endif

    endif
  endwhile
  endfil: free_lun,lun

endfor

if i10 gt 0L then begin
   d10 = d10[0L:i10*21L-1L]
   if n_elements(d) gt 0L then d=[d,d10] else d=d10
endif

if n_params() ge 2 and n_elements(d) gt 0 then begin
   ww = where(d.t ge anytim(ts) and d.t le anytim(te),nww)
   if nww gt 0 then d=d[ww] else d=0
endif

; add time in "int" format for easier utplot
siz=size(d)
if siz[siz[0]+1] eq 8 then begin
 ti = anytim(d.t,/int)
 d.time = ti.time
 d.day  = ti.day
endif else d=0

return,d

end
