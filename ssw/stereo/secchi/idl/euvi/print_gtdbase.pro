
pro print_gtdbase,obs,outfile=ofil,dbfile=dbfil,full=full

;+
; $Id: print_gtdbase.pro,v 1.2 2007/04/04 19:55:17 euvi Exp $
;
; Project : STEREO SECCHI
;
; Name    : print_gtdbase
;
; program to read and print GT table database
; Purpose : reads the GT database and prints a GT offpoint summary
;           to the terminal, or into an ASCII text file
;
; Use     : IDL> print_gtdbase
;           IDL> print_gtdbase,'a',outfile='gt_offpoints_ahead.txt'
;
; Inputs  :
;    obs : 'a' or 'b'.  Optional.  Default: print both observatories.
;
; Outputs : no explicit output
;
; Keywords:
;    outfile : name of text file to receive offpoint summary.
;              Default: print to terminal
;    dbfile  : path/name of gt database file
;              Default: $SCC_DATA/gt/secchi_gtdbase.geny
;    /full   : prints additional information from the database
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;    Creates a file if outfile keyword supplied
;    
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 26-Mar-2007
;
; $Log: print_gtdbase.pro,v $
; Revision 1.2  2007/04/04 19:55:17  euvi
; initial version
;
;-

if n_elements(dbfil) ne 1 then dbfil='$SCC_DATA/gt/secchi_gtdbase.geny'
if n_elements(obs) ne 1 then obs=''

; read the file and extract the unique information
restgenx,db,file=dbfil                                 ; - read file
wa = where(db.a.flg eq 1)                             ; - relevant data
wb = where(db.b.flg eq 1)                             ;   for A and B

; open output file, if filename given
if n_elements(ofil) eq 1 then openw,unit,ofil,/get_lun else unit=-1

; do Ahead
if strupcase(strmid(obs,0,1)) ne 'B' then begin
  n = n_elements(wa)
  dbo = db.a[wa]
  if keyword_set(full) then begin
    printf,unit,'Time (Ahead)          FileID     Version              GTgain' $
         +'             Svec-Y  Svec-Z'
    for i=0,n-1 do printf,unit,strmid(anytim(dbo[i].t,/ccsds),0,19), $
                         dbo[i].fid,dbo[i].ver,[dbo[i].cg2,dbo[i].cg4]
  endif else begin
    prired = strarr(n)+'    prime  '
    wp = where(dbo.cg2[0] lt 450,nwp)
    if nwp gt 0 then prired[wp] = '  redundant'
    printf,unit,'Time (Ahead)       prime/red Sunvec-Y Sunvec-Z'
    printf,unit,'                             (units: 2e-7 rad)'
    for i=0,n-1 do printf,unit,strmid(anytim(dbo[i].t,/stime),0,17), $
                         prired[i],dbo[i].cg4
  endelse
endif
if strupcase(strmid(obs,0,1)) ne 'A' then begin
  n = n_elements(wb)
  dbo = db.b[wb]
  if keyword_set(full) then begin
    printf,unit,'Time (Behind)         FileID     Version              GTgain' $
         +'             Svec-Y  Svec-Z'
    for i=0,n-1 do printf,unit,strmid(anytim(dbo[i].t,/ccsds),0,19), $
                         dbo[i].fid,dbo[i].ver,[dbo[i].cg2,dbo[i].cg4]
  endif else begin
    prired = strarr(n)+'    prime  '
    wp = where(dbo.cg2[0] lt 450,nwp)
    if nwp gt 0 then prired[wp] = '  redundant'
    printf,unit,'Time (Behind)      prime/red Sunvec-Y Sunvec-Z'
    printf,unit,'                             (units: 2e-7 rad)'
    for i=0,n-1 do printf,unit,strmid(anytim(dbo[i].t,/stime),0,17), $
                         prired[i],dbo[i].cg4
  endelse
endif

if n_elements(ofil) eq 1 then free_lun,unit

end
