function euvi_wavelnth_chan, fits_header
nt=n_elements(fits_header)
result=replicate(-1,nt)

if size(fits_header, /type) eq 7 then begin
   for i=0, nt-1 do begin 
      if fits_header[i] eq '171' then result[i]=0
      if fits_header[i] eq '195' then result[i]=1
      if fits_header[i] eq '284' then result[i]=2
      if fits_header[i] eq '304' then result[i]=3
   endfor
   goto, cont
endif
if size(fits_header, /type) ge 1 and size(fits_header,/type) le 5 then begin
   for i=0, nt-1 do begin 
      if fix(fits_header[i]) eq 171 then result[i]=0
      if fix(fits_header[i]) eq 195 then result[i]=1
      if fix(fits_header[i]) eq 284 then result[i]=2
      if fix(fits_header[i]) eq 304 then result[i]=3
   endfor
   goto, cont
endif

; size(fits_header,/type) eq 8
for i=0, nt-1 do begin 
   if fits_header[i].wavelnth eq 171 then result[i]=0
   if fits_header[i].wavelnth eq 195 then result[i]=1
   if fits_header[i].wavelnth eq 284 then result[i]=2
   if fits_header[i].wavelnth eq 304 then result[i]=3
endfor
cont:
return, result
end
