function euvi_filter_n, fits_header
nt=n_elements(fits_header)
result=replicate(-1,nt)

if size(fits_header, /type) eq 7 then begin
   for i=0, nt-1 do begin 
      if strupcase(fits_header[i]) eq 'OPEN' then result[i]=0
      if strupcase(fits_header[i]) eq 'S1' then result[i]=1
      if strupcase(fits_header[i]) eq 'S2' then result[i]=2
      if strupcase(fits_header[i]) eq 'DBL' then result[i]=3
   endfor
   goto, cont
endif


for i=0, nt-1 do begin 
   if strupcase(fits_header[i].filter) eq 'OPEN' then result[i]=0
   if strupcase(fits_header[i].filter) eq 'S1' then result[i]=1
   if strupcase(fits_header[i].filter) eq 'S2' then result[i]=2
   if strupcase(fits_header[i].filter) eq 'DBL' then result[i]=3
endfor


cont:

return, result


end
