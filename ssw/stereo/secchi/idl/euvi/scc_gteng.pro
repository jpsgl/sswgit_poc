
function scc_gteng,ts,te,s_c,file=file,dir=dir,no_grh=no_grh,nrl=nrl

;+
; $Id: scc_gteng.pro,v 1.3 2010/04/13 21:21:08 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gteng
;
; Purpose : reads a raw telemetry file (from SSC website) and extracts
;           the data from the gt engineering packets (Apid 1069)
;
; Use     : IDL> d = scc_gteng(file=file_search('secchi_ahead*ptp'))
;           IDL> d = scc_gteng(ts,te,'a',dir='/User/wuelser/telem_a')
;
; Inputs  :
;    ts = start time (anytim format).
;    te = end time.
;    s_c = 'a' or 'b'
; all 3 inputs required unless file keyword supplied
;
; Outputs :
;    d = structure vector containing gt engineering data.
;        the fps mnemonics are not yet included!  (tbd).
;
; Keywords:
;    file = file list  (input, optional)
;    dir = directory for automatic file search (used only if file not supplied)
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;    /nrl : use default dir path at NRL (instead of LMSAL).
;           Ignored if either dir or file are supplied.
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 1-Nov-2006
;               JPW 20-Dec-2006 : added "int" time format for easier utplot
;
; $Log: scc_gteng.pro,v $
; Revision 1.3  2010/04/13 21:21:08  nathan
; find correct level-0 directory
;
; Revision 1.2  2007/06/22 18:34:27  mcnutt
; corrected directories
;
; Revision 1.1  2007/01/12 03:19:34  euvi
; initial version
;
;-

n = n_elements(file)
if n eq 0 then begin
   file = scc_time2tlmfile(ts,te,s_c,NRL=nrl,dir=dir)
   n = n_elements(file)
endif

if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words

d0 = {time:0L,day:0L,t:0d0,n:0U,rawmin:intarr(4),rawmax:intarr(4), $ ; data
      sum1:lonarr(2),sum2:ulonarr(2),sumyz:0L, $                     ; struct.
      cg1:intarr(4),cg2:intarr(4),cg3:0,cg4:intarr(2), $
      ca:intarr(3,2),cb:intarr(3,5),cc:intarr(3,4)}
it1  =  6+ghb
it2  = 10+ghb
icnt = 12+ghb                                   ; index to sum count
irmi = lindgen(4)+7+ghi                         ; index to rawmin data
irma = irmi + 6                                 ; index to rawmax data
isu1 = 40 + ghb                                 ; index to sum1 (byte offset)
isu2 = 48 + ghb                                 ; index to sum2 (byte offset)
isyz = 56 + ghb                                 ; index to sumyz (byte offst)
icg1 = lindgen(4)+30+ghi                        ; index to cg1
icg2 = icg1 + 4                                 ; index to cg2
icg3 = 38 + ghi                                 ; index to cg3
icg4 = lindgen(2)+39+ghi                        ; index to cg4
ica  = (lindgen(6) mod 3)*11 +lindgen(6)/3 + 98+ghi  ; index to ca
icb  = (lindgen(15) mod 3)*11+lindgen(15)/3+100+ghi  ; index to cb
icc  = (lindgen(12) mod 3)*11+lindgen(12)/3+105+ghi  ; index to cc

rec = intarr(136+ghi)        ; 1 packet
dt = 7670L*24L*3600L         ; diff. betw. SC and anytim time def.
apid = 1069                  ; apid for gteng data

; intermediate size data vector to improve speed (d=[d,d0] is slow!)
n10 = 720L
d10 = replicate(d0,n10)
i10 = 0L

on_ioerror,endfil
for i=0,n-1 do begin

  openr,lun,file[i],/get_lun
  while 1 do begin
    readu,lun,rec
    if (rec[ghi] and 2047) eq apid then begin
      d0.t = double(long(rec,it1)-dt)+double(byte(rec,it2))/256d0
      d0.n = uint(rec,icnt)
      d0.rawmin = rec[irmi]
      d0.rawmax = rec[irma]
      d0.sum1 = long(rec,isu1,2)
      d0.sum2 = ulong(rec,isu2,2)
      d0.sumyz = long(rec,isyz)
      d0.cg1 = rec[icg1]
      d0.cg2 = rec[icg2]
      d0.cg3 = rec[icg3]
      d0.cg4 = rec[icg4]
      d0.ca  = reform(rec[ica],3,2)
      d0.cb  = reform(rec[icb],3,5)
      d0.cc  = reform(rec[icc],3,4)
      
      d10[i10] = d0
      i10 = i10 + 1L
      if i10 eq n10 then begin
         if n_elements(d) gt 0L then d=[d,d10] else d=d10
         i10 = 0L
      endif

    endif
  endwhile
  endfil: free_lun,lun

endfor

if i10 gt 0L then begin
   d10 = d10[0L:i10-1L]
   if n_elements(d) gt 0L then d=[d,d10] else d=d10
endif

if n_params() ge 2 and n_elements(d) gt 0 then begin
   ww = where(d.t ge anytim(ts) and d.t le anytim(te),nww)
   if nww gt 0 then d=d[ww] else d=0
endif

; add time in "int" format for easier utplot
siz=size(d)
if siz[siz[0]+1] eq 8 then begin
 ti = anytim(d.t,/int)
 d.time = ti.time
 d.day  = ti.day
endif else d=0

return,d

end

   
