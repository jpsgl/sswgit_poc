
function scc_gtparms_1tf,file,no_grh=no_grh

;+
; $Id: scc_gtparms_1tf.pro,v 1.1 2007/02/19 23:09:03 euvi Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gtparms_1tf
;
; Purpose : reads a raw telemetry file and extracts GT calibration
;           parameters from the gt engineering packets (Apid 1069)
;
; Use     : IDL> d = scc_gtparms_1tf(file)
;
; Inputs  :
;    file : name of a single Level 0 secchi telemetry packet file
;
; Outputs :
;    d = structure vector containing the following tags:
;        .t   : double     Time (anytim format in seconds)
;        .cg2 : intarr(4)  GT gain
;        .cg4 : intarr(2)  GT bias
;        .fid : long       file id (from file name: yr*100000+doy*100+n*10+obs)
;        .ver : int        file version (from file name)
;        .flg : int        -1 if no data found, else 1
;
;        Only returns the first of any series of identical entries
;
; Keywords:
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 17-Jan-2007 based on scc_gteng
;
; $Log: scc_gtparms_1tf.pro,v $
; Revision 1.1  2007/02/19 23:09:03  euvi
; initial version
;
;-

if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words

yr  = long(strmid(file,16,4,/reverse_offset))
doy = long(strmid(file,11,3,/reverse_offset))
num = long(strmid(file,7,1,/reverse_offset))
oid = long(strmid(file,22,1,/reverse_offset) ne 'a')
fid = yr*100000L + doy*100L + num*10L + oid
ver = fix(strmid(file,5,2,/reverse_offset))
typ = fix(strmid(file,2,3,/reverse_offset) eq 'fin')
; make version # of .fin files start with 100
ver = 100*typ+ver

; define structure (must be named for savegen to work)
d0 = {scc_gtdb1,t:0d0,cg2:intarr(4),cg4:intarr(2),fid:fid,ver:ver,flg:-1}
it1  =  6+ghb
it2  = 10+ghb
icg2 = lindgen(4)+34+ghi                        ; index to cg2
icg4 = lindgen(2)+39+ghi                        ; index to cg4

rec = intarr(136+ghi)        ; 1 packet
dt = 7670L*24L*3600L         ; diff. betw. SC and anytim time def.
apid = 1069                  ; apid for gteng data

  on_ioerror,endfil
  openr,lun,file,/get_lun
  while 1 do begin
    readu,lun,rec
    apid0 = rec[ghi]
    ieee_to_host,apid0
    if (apid0 and 2047) eq apid then begin
      cg2 = rec[icg2]
      ieee_to_host,cg2
      cg4 = rec[icg4]
      ieee_to_host,cg4
      w2 = where(cg2 eq d0.cg2,n2)
      w4 = where(cg4 eq d0.cg4,n4)
      if n2+n4 ne 6 then begin
        t1 = long(rec,it1)
        ieee_to_host,t1
        t2 = byte(rec,it2)
        d0.t = double(t1-dt)+double(t2)/256d0
        d0.cg2 = cg2
        d0.cg4 = cg4
        d0.flg = 1
        if n_elements(d) gt 0L then d=[d,d0] else d=d0
      endif
    endif
  endwhile
  endfil: if n_elements(lun) gt 0 then free_lun,lun

if n_elements(d) eq 0L then d=d0
return,d

end

   