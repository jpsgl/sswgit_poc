
function scc_time2gtparms,time,file,obs,quiet=quiet,no_grh=no_grh

;+
; $Id: scc_time2gtparms.pro,v 1.8 2013/10/16 14:16:07 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_time2gtparms
;
; Purpose : returns proper GT parameters for input time.  Uses either the
;           appropriate Level 0 telemetry packet file, or the GT database.
;           Looks for the most recent entry before "time".
;           Note: the gt parms are only telemetered once every 2 min., and
;                 we often take images just 2 min. after a pointing change.
;                 To avoid incorrect pointing, "time" should therefore not
;                 be the beginning of an exposure, but rather the end.
;                 (or it could be the beginning plus, say, 5-8 seconds).
;
; Use     : IDL> d = scc_time2gtparms(time,file,obs)
;                d = scc_time2gtparms(time,'secchi_ahead_2007_004_4_02.ptp')
;                d = scc_time2gtparms('2007-01-17T20:01:22','gt_db.geny','b')
;
; Inputs  :
;    time : time in any format accepted by anytim.pro.
;           for images, use time of end of exposure
;           (cannot be a vector of times)
;    file : two options:
;        1. path/name of a single Level 0 secchi telemetry packet file
;           (name must follow proper naming convention: *yyyy_doy_n_vv.???)
;        2. path/name of GT database file (created by scc_gtdbase_update.pro)
;           (name must end in 'geny')
;        Either file is read only if the name has changed since the last call.
;        Otherwise the copy in the common block is used
;    obs  : 'a' or 'b'.  Required if using GT dbase file, ignored for tlm file
;
; Outputs :
;    d = structure element containing the following tags:
;        .t   : double     Time (anytim format in seconds)
;        .cg2 : intarr(4)  GT gain
;        .cg4 : intarr(2)  GT bias
;        .fid : long       file id (from file name: yr*100000+doy*100+n*10+obs)
;        .ver : int        file version (from file name)
;        .flg : int        -1 if no GT table data found,
;                           0 if GT data found, but outdated,
;                           1 if recent GT data found for given time
;      If file couldn't be found or read, scc_time2gtparms returns -1 !
;
; Keywords:
;    /quiet  : suppress warnings for outdated or improper file
;    /no_grh : no ground receipt header (ignored if file is GT database file)
;
; Common  : gt_parms_db
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 24-Jan-2007
;
; $Log: scc_time2gtparms.pro,v $
; Revision 1.8  2013/10/16 14:16:07  nathan
; disable email if status change
;
; Revision 1.7  2013/09/23 18:30:08  nathan
; remove waits if /VERBOSE, but add wait for state change case
;
; Revision 1.6  2013/09/20 15:56:05  nathan
; fix lasterr UND case so does not mail when starting up
;
; Revision 1.5  2013/09/06 16:44:06  secchia
; nr - increase wait after error and send msg to secchipipeline@cronus
;
; Revision 1.4  2013/09/05 19:37:47  secchia
; nr - add lasterr to common gt_parms_db and spawn mail if err state changes
;
; Revision 1.3  2013/04/18 20:28:55  nathan
; refresh common block before declaring error
;
; Revision 1.2  2008/12/22 22:20:31  wuelser
; added concat_dir
;
; Revision 1.1  2007/02/19 23:09:03  euvi
; initial version
;
;-

common gt_parms_db,db,fname,ftype, lasterr

; if not supplied, try to find database file in a) $SCC_DATA b) current dir
if n_elements(file) ne 1 then begin
   file = file_search(concat_dir('$SCC_DATA','gt/secchi_gtdbase.geny'))
   file = file[0]
   if file eq '' then file = file_search('secchi_gtdbase.geny')
endif
file = file[0]
firsttry=1

retry:
; check if file needs to be read, or contents already in common block
readfile = 1
if n_elements(fname) eq 1 then if fname eq file then readfile=0

; read file, if necessary
if readfile then begin
  if strmid(file,3,4,/reverse_offset) eq 'geny' then begin  ; database file
    ftype = 1                                               ; set file type
    if file_search(file) ne '' then begin                   ; file exists:
      restgenx,db,file=file                                 ; - read file
      wa = where(db.a.flg eq 1)                             ; - relevant data
      wb = where(db.b.flg eq 1)                             ;   for A and B
      db = {a:db.a[wa],b:db.b[wb]}                          ; - data to keep
      fname = file                                          ; - update fname
    endif else fname=''                                     ; else reset fname
  endif else begin                                          ; L0 telem. file
    ftype = 0                                               ; set file type
    db = scc_gtparms_1tf(file)                              ; read tlm file
    if db[0].t gt 0d0 then fname=file $                     ; valid data ?
                      else fname=''                         ; else reset fname
  endelse

  ; if no valid data found: print error message and return
  if fname eq '' then begin                                 ; valid data?
    if keyword_set(quiet) eq 0 then $                       ; check if /quiet
      print,'Error: no GT table info found'                 ; no: print error
    return,-1                                               ; return
  endif
endif

; convert time to seconds (same as db.t)
t = anytim(time)

; search db and extract latest entry before or at "time", and a check time
; - db must be time sorted (true for dbase, assumed to be true for tlm file)
; - treat dbase and tlm file types separately
if ftype then begin                                         ; GT database
  if strlowcase(strmid(obs,0,1)) eq 'a' then o=0 else o=1   ; - Ahead or B?
  w = where(db.(o).t le t,nw)                               ; - search times
  if nw gt 0 then d=db.(o)[w[nw-1]] else d=db.(o)[0]        ; - extract match
  tlast = db.(o)[n_elements(db.(o))-1].t                    ; - t of last data
  ; set bad flag if t more than 4 hours after tlast
  bad = t gt tlast+1.44e4                                   ; - set bad flag
endif else begin                                            ; tlm file
  w = where(db.t le t,nw)                                   ; - search times
  if nw gt 0 then d=db[w[nw-1]] else d=db[0]                ; - extract match
  ; set bad flag if t more than 4 h after entry, or before 1st entry
  bad = t gt d.t+1.44e4 or t lt d.t                         ; - set bad flag
endelse

IF bad and firsttry THEN BEGIN
; refresh common block and try once more
    if keyword_set(quiet) eq 0 then BEGIN
	print,'Outdated or improper GT table info for '+anytim(t,/ccs)+'; trying refresh...'
	print,'Last time is ',anytim(tlast,/ccs)
	wait,1
    ENDIF
    fname=''
    firsttry=0
    goto, retry
ENDIF
; check if entry time not appropriate, if so, set output flg, print warning
if bad then begin
    d.flg = d.flg < 0                                         ; set flg to 0
    if keyword_set(quiet) eq 0 then BEGIN
    	print,'Warning: outdated or improper GT table info for '+anytim(t,/ccs)
    	print,'Last time is ',anytim(tlast,/ccs)
    	wait,3
    ENDIF
endif

if datatype(lasterr) EQ 'UND' THEN lasterr=d.flg
IF d.flg NE lasterr THEN BEGIN
; see what is happening with gterr
	msg=anytim(t,/ccs)+'  d.flg (validity) change from '+trim(lasterr)+' to '+trim(d.flg)+'. Last db.t is '+anytim(tlast,/ccs)
	print,'WARNING: ',msg
    	;if keyword_set(quiet) eq 0 then BEGIN
    	if 0 then BEGIN
	    wait,5
	    cmd='echo "'+msg+'" | mailx -s GTdb_'+obs+'_status_change secchipipeline@cronus'
	    spawn,cmd,/sh
	ENDIF
	lasterr=d.flg
ENDIF

return,d

end
