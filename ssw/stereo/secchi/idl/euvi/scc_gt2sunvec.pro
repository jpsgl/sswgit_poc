
function scc_gt2sunvec,time,sund,gtd,obs=obs,file=file,error=err, $
                       rad=rad,no_grh=no_grh, LOUD=loud, _EXTRA=_extra

;+
; $Id: scc_gt2sunvec.pro,v 1.4 2007/05/22 18:47:46 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gt2sunvec
;
; Purpose : returns sun vector in GT reference frame.  Uses GT table data
;           and tries to accurately model non-linearities of the GT signal
;
; Use     : IDL> d = scc_gt2sunvec(time,sundist,gtdata,obs=obs,file=file)
;
; Inputs  :
;    time    : time in any format accepted by anytim.pro.
;              for images, use time of end of exposure
;              (cannot be a vector of times)
;    sundist : float. Sun - S/C distance in AU
;    gtdata  : [y,z] GT data.  Photodiode difference signals from telemetry.
;              Alternatively, a quadruplet of raw GT photodiode signals
;
; Note: the keywords file and obs are also required
;
; Outputs :
;    sunvec : [y,z].  Y and Z sun vector components in GT frame, in arcsec
;
; Keywords (input):
;    file : required (!), two options:
;        1. path/name of a single Level 0 secchi telemetry packet file
;           (name must follow proper naming convention: *yyyy_doy_n_vv.???)
;        2. path/name of GT database file (created by scc_gtdbase_update.pro)
;           (name must end in 'geny')
;    obs     : 'a' or 'b'.  Required (!)
;    /rad    : output sunvector components in radian.  Default is arcsec
;    /LOUD  : print warnings for outdated or improper file
;    /no_grh : no ground receipt header (ignored if file is GT database file)
;
; Keywords (output):
;   error : 0B if no errors, 1B if "file" outdated, 3B if "file" invalid
;
; Common  : 
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 29-Jan-2007
;               JPW, 30-Apr-2007 updated GT gain
;
; $Log: scc_gt2sunvec.pro,v $
; Revision 1.4  2007/05/22 18:47:46  nathan
; ditto
;
; Revision 1.3  2007/05/22 18:46:55  nathan
; add /LOUD for scc_time2gtparms
;
; Revision 1.2  2007/05/08 22:50:38  euvi
; coefficient update
;
; Revision 1.1  2007/02/19 23:09:03  euvi
; initial version
;
;-
IF keyword_set(LOUD) THEN qt=0 ELSE qt=1
; Coefficients of GT linearity model, from gt_fit_model.pro,
; y/z calibrations (measured on-orbit, and gain difference redundant/prime
if strlowcase(strmid(obs,0,1)) eq 'a' then begin
  cc    = [[1.0000, 0.715e-8, 1.341e-5, 0.329e-8], $
           [1.2843, 1.883e-7, 2.203e-4, 0.625e-7], $
           [1.1739, 1.759e-6, 1.718e-3, 0.511e-6]]
  ; y/zgain are for the redundant diodes
  ; initial gain estimates for comparison with limb fit data
  ; yrgain = (378./256.) * 1.06   ; 2006-11-16 : from 1st GT en., sund corr'd
  ; zrgain = (378./256.) * 1.06   ; 2006-11-16 : from 1st GT en., sund corr'd
  ; 2007-02-16 : updated gains based on go_scc_fit_limb1.pro
  ; yrgain = (378./256.) * 1.104    ; as of 2007-02-16
  ; zrgain = (378./256.) * 1.104    ; as of 2007-02-16
  ; 2007-04-30 : updated gains based on go_scc_fit_limb2.pro
  yrgain = (378./256.) * 1.100    ; as of 2007-04-30
  zrgain = (378./256.) * 1.100    ; as of 2007-04-30
  ypgain = yrgain * 1.389  ; pri/red based on nominal preamp resistor values
  zpgain = zrgain * 1.389  ; pri/red based on nominal preamp resistor values
endif else begin
  cc    = [[1.0000, 0.176e-8, 0.641e-5, 0.162e-8], $
           [0.9505, 0.265e-7, 0.451e-4, 0.281e-7], $
           [4.7232, 0.915e-6, 1.020e-3, 0.065e-6]]
  ; yrgain = (378./256.) * 0.92   ; 2006-11-16 : from 1st GT en., sund corr'd
  ; zrgain = (378./256.) * 0.92   ; 2006-11-16 : from 1st GT en., sund corr'd
  ; 2007-02-16 : updated gains based on go_scc_fit_limb1.pro
  ; yrgain = (378./256.) * 0.929    ; as of 2007-02-16
  ; zrgain = (378./256.) * 0.929    ; as of 2007-02-16
  ; 2007-04-30 : updated gains based on go_scc_fit_limb2.pro
  yrgain = (378./256.) * 0.920    ; as of 2007-04-30
  zrgain = (378./256.) * 0.920    ; as of 2007-04-30
  ypgain = yrgain * 1.381  ; pri/red based on nominal preamp resistor values
  zpgain = zrgain * 1.381  ; pri/red based on nominal preamp resistor values
endelse

; Get the GT table parameters
db = scc_time2gtparms(time,file,obs,no_grh=no_grh, QUIET=qt)

; Check whether any parameters were found
sdb = size(db)
if sdb[sdb[0]+1] ne 8 then begin
   print,'Error: no GT table data found.'           ; print error message
   err = 3B                                         ; set error flag
   if n_elements(gtd) eq 2 then return,gtd $        ; return either input
      else return,[0.,0.]                           ;        or zero
endif

; set error depending on db.flg
if db.flg lt 1 then err=1B else err=0B

; Determine whether prime or redundant diodes, based on GT gain
if total(db.cg2) lt (450*4) then redun=1 else redun=0

; Create raw diode difference signal
case n_elements(gtd) of
  2 : begin  ; these are on-board calibrated signals
        ; "undo" on-board calibration: 1. re-apply bias
        gtr  = float(gtd) + float(db.cg4)
        ; "undo" on-board calibration: 2. de-apply gain
        yraw = gtr[0] * 512.0 / (db.cg2[0]+db.cg2[1])
        zraw = gtr[1] * 512.0 / (db.cg2[2]+db.cg2[3])
      end
  4 : begin  ; these are raw photodiode readings
        ; calculate raw difference signal
        yraw = gtd[0] - gtd[1]
        zraw = gtd[2] - gtd[3]
      end
  else : begin  ; no signal, just the bias
           gtr  = float(db.cg4)
           ; "undo" on-board calibration: 2. de-apply gain
           yraw = gtr[0] * 512.0 / (db.cg2[0]+db.cg2[1])
           zraw = gtr[1] * 512.0 / (db.cg2[2]+db.cg2[3])
         end
endcase

; Apply sun distance to the coefficients
rr = [1.0,sund-1.0,(sund-1.0)*(sund-1.0)]
cs  = rr ## cc

; Calibrate yraw,zraw for 1 AU (correction matrix is normalized to 1 AU)
; Flip signs of cs[2] for Y redundant and Z prime
; (signs of initial coefficient matrix are for Y prime and Z redundant)
flip = [1.0,1.0,-1.0,1.0]
if redun then begin
   y1 = yraw * yrgain
   z1 = zraw * zrgain
   cy = cs * flip
   cz = cs
endif else begin
   y1 = yraw * ypgain
   z1 = zraw * zpgain
   cy = cs
   cz = cs * flip
endelse

; prepare polynomial of GT signal for correction
yy = [y1,y1*y1*y1,y1*z1,y1*z1*z1]
zz = [z1,z1*z1*z1,z1*y1,z1*y1*y1]

; apply correction
svec = [cy ## transpose(yy), cz ## transpose(zz)]

; so far, units are 2e-7 rad.  Change to arcsec (default) or radian
if keyword_set(rad) then fact=2e-7 else fact= !radeg*3.6*2e-4
svec = svec * fact

return,svec

end
