
pro reportbigjitter, THRESHOLD=threshold

;+
; $Id: reportbigjitter.pro,v 1.2 2009/04/22 16:22:47 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : reportbigjitter
;
; Purpose : Generate report of pointing excursions from nominal based on GT slew telemetry
;
; Use     : 
;
; Inputs  : None- searches for previous report and continues up to current date
;
; Outputs :
;   	    Appends Text file $SCC_DATA/gt/bigjitterA.txt, ...B.txt 
; Keywords:
;   	    THRESHOLD=  number of arcsec of excursion to try to detect (default=5)
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : N.Rich, NRL/Interferometrics
; $Log: reportbigjitter.pro,v $
; Revision 1.2  2009/04/22 16:22:47  nathan
; updates
;
; Revision 1.1  2009/02/05 22:39:54  nathan
; uses output of secchi_gtslew to find anomalies
;
;-

ab=['A','B']
FOR j=0,1 DO BEGIN
IF keyword_set(THRESHOLD) THEN thresh=threshold ELSE thresh=5
intrvl=7
aorb=ab[j]
outfil=getenv('SCC_DATA')+'/gt/bigjitter'+aorb+trim(thresh)+'.txt'
IF file_exist(outfil) THEN BEGIN
    spawn,'tail -1 '+outfil,lastline,/sh
    print,lastline
    uts=anytim2utc(strmid(lastline[0],0,19))
    dur=float(strmid(lastline[0],21,9))
    help,dur
    uts.time=uts.time+1000+1000*dur
    get_utc,ute
    ute.mjd=ute.mjd-2
    openw,lun,outfil,/get_lun,/append
ENDIF ELSE BEGIN

    openw,lun,outfil,/get_lun

    printf,lun,'Start Time            Duration   Avg'
    printf,lun,'                      (sec)      (arcsec)'
    ;           2009-01-02 00:00:00   123.567    123.567


    uts=anytim2utc('2007-01-22 00:00:00')
    ute=anytim2utc('2007-06-01 00:00:00')
    ;uts=anytim2utc('2008-12-20 00:00:00')
    ;ute=anytim2utc('2008-12-29 00:00:00')
ENDELSE

ut0=uts
ut1=ut0

while ut0.mjd LT ute.mjd do begin
; do 7 days at a time in attempt to be efficient
    mjd1=ut0.mjd+intrvl
    ut1.mjd = mjd1<ute.mjd
    print,'SC-',aorb,' Reading ',trim(ut1.mjd-ut0.mjd),' days starting ',utc2str(ut0,/date)
    s=secchi_gtslew(ut0,ut1,aorb,/nrl)
    ns=n_elements(s)
    s=s[1:ns-1]
    ns=ns-1
    error=sqrt(total(s.ave^2.,1))
    w=where(error GT thresh,nw)
    ;stop
    IF nw GT 0 THEN BEGIN
    ; what if threshold is exceeded on first or last value? then event extends over input boundary
    ; need to try again with widened range
    ; assume it will not be first value
    	IF w[nw-1] EQ ns-1 THEN BEGIN
	    ; add one day to interval and retry
	    intrvl=intrvl+1
	    goto, tryagain
	ENDIF
    ; divide w into consecutive intervals	
	d =w-shift(w,-1)
	wd=where(d lt -2,nwd)
	IF nwd EQ 0 THEN BEGIN
	    wd=[-1,nw-1]
	    nwd=1
	ENDIF ELSE BEGIN
	    wd=[-1,wd,nw-1]
	    nwd=nwd+1
	ENDELSE
	FOR i=1,nwd DO BEGIN
	; for each excursion
	    w0=w[wd[i-1]+1]
	    w1=w[wd[i]]
    	    avrg=avg(error[w0:w1])
	    dur0 =(s[w1].time - s[w0].time)  ; millisec
	    ; use geometry to estimate leading and trailing portions
	    x=s[w1].time-s[w1-1].time	    ; millisec
	    h1=error[w0]-error[w0-1]
	    h2=error[w1]-error[w1+1]
	    r1= x* (error[w0]-thresh)/h1
	    r2= x* (error[w1]-thresh)/h2
	    dur = (dur0+r1+r2)/1000
;           2009-01-02 00:00:00   123.567    123.567
	    row=string(utc2str(s[w0],/truncate),dur,avrg,format='(a19,f10.3,f11.3)')
	    print,row
	    printf,lun,row
	ENDFOR
    ENDIF
    ; go to next interval
    ut0.mjd = ut0.mjd+intrvl
    tryagain:
endwhile

close,lun
free_lun,lun

ENDFOR

end
