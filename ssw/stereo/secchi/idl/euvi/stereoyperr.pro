pro stereoyperr, filenames, yprdg,yprds, times, hdrs=hdrs, _Extra=_extra
;+
; $Id: stereoyperr.pro,v 1.7 2008/05/01 19:26:31 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : stereoyperr
;
; Purpose : compute error or offset between GT-derived pointing and SPICE attitude info
;	    and also changes between SPICE values
;
; Use     : IDL> 
;
; Input:    filenames	list of FITS filenames to read in
;
; Output:   none
;
; Keyword Input/Output:
;	    yprdg, yprds, times
;   	    HDRS = array of header structures from filenames
;
; Common  :
;
; Restrictions: 
;
; Side effects: generates 5 plots in 2 windows
;
; Category   : attitude, analysis, calibration, pointing
;
; Prev. Hist.:

; Written    : N.Rich, NRL/I2, 6/5/07
;
; $Log: stereoyperr.pro,v $
; Revision 1.7  2008/05/01 19:26:31  nathan
; updated for initial case; updated yranges
;
; Revision 1.6  2007/06/18 18:58:32  nathan
; add mods for plotting
;
; Revision 1.5  2007/06/14 20:27:59  nathan
; always plot
;
; Revision 1.4  2007/06/14 20:09:38  nathan
; make more generic
;
; Revision 1.3  2007/06/05 22:58:28  nathan
; add plotting
;
; Revision 1.2  2007/06/05 22:42:23  nathan
; add HDRS keyword
;
; Revision 1.1  2007/06/05 22:26:04  nathan
; 1st draft
;
;-

IF n_params() EQ 4 THEN BEGIN
    t1=datatype(yprdg)
    t2=datatype(yprds)
    t3=datatype(times)
    w=where([t1,t2,t3] EQ 'UND',nt)
    IF nt EQ 0 THEN goto, startplot
ENDIF

IF datatype(HDRS) EQ 'UND' THEN junk=sccreadfits(filenames,hdrs,/nodata)

n=n_elements(hdrs)

lasthdrattfile=hdrs[0].att_file
lastnewattfile=lasthdrattfile
hattfiles=bytarr(n)
nattfiles=bytarr(n)
;attfiles= [hdrattfile,newattfile;n] =1 if new, else 0

!x.style=3
!y.style=3
yprds=fltarr(3,n)
yprdg=fltarr(3,n)

FOR i=0,n-1 DO BEGIN
	h=hdrs[i]
	ypr=get_stereo_hpc_point(h.date_obs,h.obsrvtry,tolerance=60)
	natt_file=get_stereo_att_file(h.date_obs,h.obsrvtry)
	yprds[0,i]=h.sc_yaw	-ypr[0]
	yprds[1,i]=h.sc_pitch	-ypr[1]
	yprds[2,i]=(h.sc_roll	-ypr[2])*3600.	; convert to arcsec for comparison
	;check att_file info
	IF h.att_file NE lasthdrattfile THEN BEGIN
		hattfiles[i]=1
		print,'New hdrattfile at ',h.date_obs,': ',h.att_file
	ENDIF
	lasthdrattfile=h.att_file
	IF natt_file NE lastnewattfile THEN BEGIN
		nattfiles[i]=1
		print,'New newattfile at ',h.date_obs,': ',natt_file
	ENDIF
	lastnewattfile=natt_file

	dsun = h.dsun_obs / 1.496e11                      ; sun distance in AU
	gpy=scc_gt2sunvec(h.date_obs,dsun,obs=h.obsrvtry)
	yprdg[0,i]=gpy[1]-ypr[0]
	yprdg[1,i]=gpy[0]-ypr[1]
ENDFOR
wchhaf=where(hattfiles EQ 1)
wchnaf=where(nattfiles EQ 1)
times=hdrs.date_obs
startplot:
seg=4

!p.multi=[0,0,3,0,0]
window,xsiz=512,ysize=1002
utplot,times,reform(yprds[0,*]),yrang=[-5,5],title='Difference in SPICE Yaw',ytitle='Arcsec',charsize=2,psym=-1,_extra=_extra
;outplot,hdrs[wchhaf].date_obs,0.99*yprds[0,wchhaf],psym=4
;print,'Diamond is for new header att_file.'
;outplot,hdrs[wchnaf].date_obs,1.01*yprds[0,wchhaf],psym=6
;print,'Square is for new current att_file.'
utplot,times,reform(yprds[1,*]),yrang=[-.5,.5],title='Difference in SPICE Pitch',ytitle='Arcsec',charsize=2,psym=-1,_extra=_extra
utplot,times,reform(yprds[2,*]),yrang=[-.5,.5],title='Difference in SPICE Roll',ytitle='Arcsec',charsize=2,psym=-1,_extra=_extra

!p.multi=[0,0,2,0,0]
window,2,xsiz=512,ysize=668
gr0=stdev(yprdg[0,*])
gr1=stdev(yprdg[1,*])
utplot,times,median(reform(yprdg[0,*]),seg),yrang=[-gr0,gr0],title='GT minus SC Yaw (E-W)',ytitle='Arcsec',psym=-1,_extra=_extra
utplot,times,median(reform(yprdg[1,*]),seg),yrang=[-gr1,gr1],title='GT minus SC Pitch (N-S)',ytitle='Arcsec',psym=-1,_extra=_extra
!p.multi=0


;stop

end
