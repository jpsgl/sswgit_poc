pro euvi_pretty,fin,fout,bin=bin,gam=gam,jpg=jpg,tif=tif,png=png,gif=gif
;+
; $Id: euvi_pretty.pro,v 1.1 2007/02/01 00:10:44 euvi Exp $
;
; Project : STEREO SECCHI
;
; Name    : euvi_pretty
;
; Purpose : write pretty jpg, tif, gif, and/or png file from a FITS file
;
; Use     : IDL> euvi_pretty,infile,'euvi_pic',bin=2
;
; Inputs  :
;    infile  : path/name of FITS file
;    outfile : name of output file (wihout the .xyz qualifier)
;
; Outputs :
;
; Keywords (input):
;    bin     ; binning factor.  One of 1,2,4,8,16,32
;    gam     : gamma of image.  default: 0.25
;    /png    : write a png file
;    /tif    : write a tiff file
;    /gif    ; write a gif file
;    /jpg    : write a jpeg file.  Default if no other format selected
;
; Common  : None
;
; Restrictions:
; Calls: mreadfits, sb_icerdiv2, sb_bytscl, despike_gen, med3x3gen, eit_colors
;
; Side effects: Creates image file(s)
;
; Category    : Image processing
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 25-Jan-2007
;
; $Log: euvi_pretty.pro,v $
; Revision 1.1  2007/02/01 00:10:44  euvi
; put here for euvi_pretty
;
;-

; defaults
if n_elements(bin) ne 1 then bin=1
if n_elements(gam) ne 1 then gam=0.25
if keyword_set(png) eq 0 and keyword_set(tif) eq 0 and keyword_set(gif) eq 0 $
   then jpg=1

; force valid bin
wbin = where(bin eq [1,2,4,8,16,32],okbin)
if okbin eq 0 then bin=1

; read file, remove bias
mreadfits,fin,i,d                  ; read file
sb_icerdiv2,i,d                    ; correct conditional div2
d = (fix(d)-fix(i.biasmean)) > 0   ; remove bias, force positive

; force to 2048^2
s = size(d)
if min(s[1:2]) lt 2048 then begin
  print,'Image must be 2048x2048 or larger for this routine'
  return
endif
obs = strupcase(strmid(i.obsrvtry,0,1,/reverse))
if s[1] ge 2112 and obs eq 'A' then d=d[64:2111,*] else d=d[0:2047,*]
if s[2] ge 2126 then d=d[*,78:2125] else d=d[*,0:2047]
s = size(d)

; remove cosmic rays
d = despike_gen(d,tn=8)   ; despike
d = despike_gen(d,tn=8)   ; 2nd run removes larger cosmic ray hits

; rebin if desired
if bin gt 1 then d=rebin(d,s[1]/bin,s[2]/bin)
s = size(d)

; scale image
col = {min:0.0,max:float(max(d)),gam:float(gam)}
b = sb_bytscl(d,col)

; read color table
eit_colors,i.wavelnth,rr,gg,bb

; prepare true color image for jpg,tif
if keyword_set(jpg) or keyword_set(tif) then begin
  lut = [[rr],[gg],[bb]]
  t = bytarr(3,s[1],s[2])
  for j=0,2 do begin
    l = lut[*,j]
    t[j,*,*] = l[b]
  endfor
endif

; write image
if keyword_set(jpg) then write_jpeg,fout+'.jpg',t,true=1,quality=90
if keyword_set(tif) then write_tiff,fout+'.tif',reverse(t,3)
if keyword_set(png) then write_png, fout+'.png',b,rr,gg,bb
if keyword_set(gif) then write_gif, fout+'.gif',b,rr,gg,bb

end
