
function scc_sunvec,time,dsun,obs=obs_s,dir=dir,error=err, $
                    gtdbfile=gfil,rad=rad,no_grh=no_grh,quiet=quiet

;+
; $Id: scc_sunvec.pro,v 1.7 2015/12/22 14:54:06 secchia Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gt2sunvec
;
; Purpose : returns sun vector in GT reference frame.
;           Uses level 0 GTSLEW telemetry packet files (for jitter)
;           and gtdbase data (for offpoint), and tries to accurately
;           model non-linearities of the GT signal
;
; Use     : IDL> d = scc_sunvec(time,dsun,obs=obs,dir=dir)
;
; Inputs  :
;    time    : time in any format accepted by anytim.pro.
;              use time of middle of exposure
;              (cannot be a vector of times)
;    dsun    : float. Sun - S/C distance in AU
;
; Notes:  - the keyword obs is also required
;         - the keyword dir is required if jitter correction is desired
;
; Outputs :
;    sunvec : [y,z].  Y and Z sun vector components in GT frame, in arcsec
;
; Keywords (input):
;    obs      : 'a' or 'b'.  Required (!)
;    dir      : top directory containing level 0 secchi telem. files for "obs"
;               Can be a 2-element string vector for ['Ahead','Behind']
;               If not present: no jitter correction, only offpoint
;    gtdbfile : gtdbase file.  Default: $SCC_DATA/gt/secchi_gtdbase.geny
;    /rad     : output sunvector components in radian.  Default is arcsec
;    /no_grh  : no ground receipt header (ignored if file is GT database file)
;    /quiet   : Turns off informational messages
;
; Keywords (output):
;   error : 0B if no errors, 1B if no jitter correction (no gtslew data)
;           2B if gtdbase (offpoint) file outdated, 6B if gtdbase invalid
;
; Common  : gtslew
;
; Restrictions: requires local archive of level 0 telemetry packet files
;               for jitter correction
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 5-Apr-2007
;
; $Log: scc_sunvec.pro,v $
; Revision 1.7  2015/12/22 14:54:06  secchia
; check to make sure dab is defined on line 100
;
; Revision 1.6  2015/12/14 20:58:29  secchia
; nr - use same version id for .fin and .ptp; always re-read if n_elements(dab) is zero
;
; Revision 1.5  2015/07/23 22:26:12  nathan
; remove wait if /quiet
;
; Revision 1.4  2010/12/09 22:33:57  nathan
; more info in errmsg; wait if DIR= set
;
; Revision 1.3  2007/08/20 18:56:18  nathan
; Make spacecraft determination more robust; enhanced info output
;
; Revision 1.2  2007/05/02 16:04:52  thompson
; Added keyword /quiet
;
; Revision 1.1  2007/04/05 16:50:45  euvi
; initial version
;
;-

common gtslew,dab,tab,ydn

; if dir exists then try to get gt_slew data for jitter removal
errmsg='DIR not set'
IF keyword_set(QUIET) THEN loud=0 ELSE loud=1
if keyword_set(dir) then begin
 IF (loud) THEN print,dir
 ; which observatory
 if strpos(strupcase(obs_s),'B') GE 0 then obs=1 else obs=0
 ;if strupcase(strmid(obs_s,0,1)) eq 'B' then obs=1 else obs=0

 ; derive ydn
 yr  = strmid(anytim(time,/ccsds),0,4)
 mjd = anytim(time,/mjd)
 doy = string(mjd2doy(mjd),form='(i3.3)')
 n6  = string(mjd.time / 14400000L + 1L,form='(i1.1)')
 ydni = yr+'_'+doy+'_'+n6

 ; check if gtslew-file needs to be read, or contents already in common block
 readfile = 1
 if n_elements(ydn) eq 2 then if ydni eq ydn[obs] then readfile=0
 if datatype(dab) NE 'UND' then IF n_elements(dab.(obs)) LT 2 THEN readfile=1
 ; read file, if necessary
 if readfile then begin
  IF (loud) THEN print,'$Id: scc_sunvec.pro,v 1.7 2015/12/22 14:54:06 secchia Exp $'
  if obs then pat='behind' else pat='ahead'
  pat = 'secchi_'+pat+'_'+ydni+'*'
  if n_elements(dir) gt 1 then dir1=dir[obs] else dir1=dir[0]
  ff = file_search(dir1,pat,count=nff)
  IF (loud) then help,ff,pat
  IF (loud) THEN print,ff
  errmsg='no matching ptp files in '+dir1
  if nff gt 0 then begin
    ff = ff[nff-1] 
    ver = fix(strmid(ff,5,2,/reverse_offset))           ; add version #
    d = secchi_gtslew(file=ff,/sec,/raw,no_grh=no_grh)
  endif else begin
    d = 0
    ydni = ''
  endelse
  if n_elements(dab) gt 0 then begin
     if obs then dab={a:dab.a,b:d} else dab={a:d,b:dab.b}
     ydn[obs] = ydni
  endif else begin
     if obs then dab={a:0,b:d} else dab={a:d,b:0}
     ydn = strarr(2)
     ydn[obs] = ydni
  endelse
 endif else d = dab.(obs)

 ; extract appropriate slew data
 s = size(d)
 if s[s[0]+1] eq 8 then begin         ; slew data exists
   t1 = anytim(time)
   dt = abs(d.t-t1)
   mdt = min(dt)
   ww = where(dt eq mdt)
   if mdt le 60.0 then yzs=d[ww[0]].ave ELSE errmsg='closest GT data is '+trim(mdt)+' sec.'
 endif ELSE errmsg='No gt data found for '+ydni
endif

if n_elements(yzs) ne 2 then begin
  ; no gt data within 1 minute of image
  yzs = [0.0,0.0]
  er2 = 1B
  IF (loud) THEN message,'Warning: '+errmsg+', i.e., no jitter correction',/info
  IF keyword_set(DIR) and (loud) THEN wait,2
endif else er2=0B

; get offpoint
yz = scc_gt2sunvec(time,dsun,yzs,obs=obs_s,error=err, $
                   rad=rad,file=gfil,no_grh=no_grh)
; update error status
err = err+err+er2

return,yz

end 
