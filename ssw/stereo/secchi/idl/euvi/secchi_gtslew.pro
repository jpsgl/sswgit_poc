
function secchi_gtslew,ts,te,s_c,file=file,dir=dir,no_grh=no_grh,nrl=nrl, $
                       seconds=secs,raw=raw, DRAWPLOT=drawplot

;+
; $Id: secchi_gtslew.pro,v 1.3 2009/02/05 22:40:48 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : secchi_gtslew
;
; Purpose : returns SCIP pointing averages and std.dev, using gtslew packets
;           in Level 0 SECCHI telemetry files
;
; Use     : IDL> d = secchi_gtslew(file=file_search('secchi_ahead*ptp'))
;           IDL> d = secchi_gtslew(ts,te,'a',dir='~/telem_a')
;
; Inputs  :
;    ts = start time (anytim format).
;    te = end time.
;    s_c = 'a' or 'b'
; all 3 inputs required unless file keyword supplied
;
; Outputs :
;    d = structure vector containing gt engineering data:
;    d.mjd, d.time :  time of center of sampling interval ("mjd" format)
;                     not present if /seconds set
;    d.t           ;  time in seconds (anytim format), only if /seconds set
;    d.ave         :  fltarr(2), suncenter location [Y,Z] in S/C coord. system,
;                     average relative to nominal in arcsec
;                     or in raw (2e-7 rad) units if /raw set
;              Note:  Y is nominally in ecliptic N-S,
;                     N positive on Ahead, S pos. on Behind.
;                     Z is nominally in ecliptic E-W,
;                     W positive on Ahead, E pos. on Behind.
;    d.sdev        :  fltarr(2), std.dev of pointing
;    d.n           :  number of samples used in ave, sdev
;
; Keywords:
;    file = file list  (input, optional)
;    dir = directory for automatic file search (used only if file not supplied)
;    /raw : units are 2e-7 rad instead of arcsec
;    /seconds : output structure has "t" tag w/time in secs
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;    /nrl : use default dir path at NRL (instead of LMSAL).
;           Ignored if either dir or file are supplied.
;   /DRAWPLOT	Plot sqrt(x^2 + z^2) and x and z pointing over given interval before returning result
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 8-Dec-2006
;               JPW, 5-Apr-2007, added /seconds and /raw, changed int to mjd
;
; $Log: secchi_gtslew.pro,v $
; Revision 1.3  2009/02/05 22:40:48  nathan
; added /DRAWPLOT option
;
; Revision 1.2  2007/04/05 16:49:08  euvi
; adding /seconds /raw, changing int to mjd format
;
; Revision 1.1  2007/01/12 03:19:34  euvi
; initial version
;
;-

if keyword_set(raw) then gt2arcsec=1.0 else $
   gt2arcsec = 2e-7 * (3.6e3*!radeg)
if keyword_set(secs) then d0 = {t:0d0,ave:fltarr(2),sdev:fltarr(2),n:0L} $
            else d0 = {mjd:0L,time:0L,ave:fltarr(2),sdev:fltarr(2),n:0L}

; read the telemetry file
if n_elements(ts) gt 0 and n_elements(te) gt 0 $
  then sa=scc_gtslew(ts,te,s_c,file=file,dir=dir,no_grh=no_grh,nrl=nrl) $
  else sa=scc_gtslew(file=file,no_grh=no_grh)
siz = size(sa)

if siz[siz[0]+1] eq 8 then begin
  ; create values for beginning of interval
  sa1 = shift(sa,1)
  sa1[0].n = 0
  sa1[0].sum1 = 0L
  sa1[0].sum2 = 0L

  ; value change over interval
  dn = double(sa.n > 1L)
  ds1 = double(sa.sum1)
  ds2 = double(sa.sum2)
  w = where(sa1.n lt sa.n,nw)
  if nw gt 0 then begin
    dn[w] = sa[w].n - sa1[w].n
    ds1[*,w] = sa[w].sum1 - sa1[w].sum1
    ds2[*,w] = sa[w].sum2 - sa1[w].sum2
  endif

  d = replicate(d0,n_elements(sa))

  ; mid-sample time
  if keyword_set(secs) then d.t=sa.t-2d-3*dn else begin
    ti = anytim(sa.t-2d-3*dn,/mjd)
    d.mjd  = ti.mjd
    d.time = ti.time
  endelse
  d.n    = dn

  ; averages
  d.ave[0] = gt2arcsec * reform(ds1[0,*]/dn)
  d.ave[1] = gt2arcsec * reform(ds1[1,*]/dn)

  ; sdevs
  d.sdev[0] = gt2arcsec * sqrt(reform(ds2[0,*]-ds1[0,*]*ds1[0,*]/dn)/dn)
  d.sdev[1] = gt2arcsec * sqrt(reform(ds2[1,*]-ds1[1,*]*ds1[1,*]/dn)/dn)
endif else d=0

IF keyword_set(DRAWPLOT) THEN BEGIN
    error=sqrt(total(d.ave^2.,1))
    ymn=min(d.ave)
    ymx=max(error)
    plotprep
    utplot,d,error,yrange=[ymn,ymx],title='GT Data',psym=-1,ytitle='Arcsec'
    outplot,d,d.ave[0],color=2
    outplot,d,d.ave[1],color=3
ENDIF
return,d

end
