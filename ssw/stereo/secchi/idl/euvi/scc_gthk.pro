
function scc_gthk,ts,te,s_c,file=file,dir=dir,no_grh=no_grh,nrl=nrl

;+
; $Id: scc_gthk.pro,v 1.3 2010/04/13 21:21:08 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gthk
;
; Purpose : reads a raw telemetry file (from SSC website) and extracts
;           the data from the gt HK packets (Apid 1067)
;
; Use     : IDL> d = scc_gthk(file=file_search('secchi_ahead*ptp'))
;           IDL> d = scc_gthk(ts,te,'a',dir='/User/wuelser/telem_a')
;
; Inputs  :
;    ts = start time (anytim format).
;    te = end time.
;    s_c = 'a' or 'b'
; all 3 inputs required unless file keyword supplied
;
; Outputs :
;    d = structure vector containing gt housekeeping data.
;        not all mnemonics are included yet.
;
; Keywords:
;    file = file list  (input, optional)
;    dir = directory for automatic file search (used only if file not supplied)
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;    /nrl : use default dir path at NRL (instead of LMSAL).
;           Ignored if either dir or file are supplied.
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 1-Nov-2006
;               JPW 20-Dec-2006 : added "int" time format for easier utplot
;
; $Log: scc_gthk.pro,v $
; Revision 1.3  2010/04/13 21:21:08  nathan
; find correct level-0 directory
;
; Revision 1.2  2007/06/22 18:34:27  mcnutt
; corrected directories
;
; Revision 1.1  2007/01/12 03:19:34  euvi
; initial version
;
;-

n = n_elements(file)
if n eq 0 then begin
   file = scc_time2tlmfile(ts,te,s_c,NRL=nrl,dir=dir)
   n = n_elements(file)
endif

if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words

d0 = {time:0L,day:0L,t:0d0,n:0U,raw:intarr(4),red:0B,gain:0B,cnt:0B}
     ; data structure
it1  =  6+ghb
it2  = 10+ghb
iraw = [14,13,16,15]+ghi
irg  = 20+ghb
icnt = 53+ghb

rec = intarr(136+ghi)        ; 1 packet
dt = 7670L*24L*3600L         ; diff. betw. SC and anytim time def.
apid = 1067                  ; apid for hk data

; intermediate size data vector to improve speed (d=[d,d0] is slow!)
n10 = 1440L
d10 = replicate(d0,n10)
i10 = 0L

on_ioerror,endfil
for i=0,n-1 do begin

  openr,lun,file[i],/get_lun
  while 1 do begin
    readu,lun,rec
    if (rec[ghi] and 2047) eq apid then begin
      d0.t = double(long(rec,it1)-dt)+double(byte(rec,it2))/256d0
      d0.raw = rec[iraw]
      d0.red = (byte(rec,irg) and 2B) / 2B
      d0.gain = (byte(rec,irg) and 1B)
      d0.cnt = byte(rec,icnt)

      d10[i10] = d0
      i10 = i10 + 1L
      if i10 eq n10 then begin
         if n_elements(d) gt 0L then d=[d,d10] else d=d10
         i10 = 0L
      endif

    endif
  endwhile
  endfil: free_lun,lun

endfor

if i10 gt 0L then begin
   d10 = d10[0L:i10-1L]
   if n_elements(d) gt 0L then d=[d,d10] else d=d10
endif

if n_params() ge 2 and n_elements(d) gt 0 then begin
   ww = where(d.t ge anytim(ts) and d.t le anytim(te),nww)
   if nww gt 0 then d=d[ww] else d=0
endif

; add time in "int" format for easier utplot
siz=size(d)
if siz[siz[0]+1] eq 8 then begin
 ti = anytim(d.t,/int)
 d.time = ti.time
 d.day  = ti.day
endif else d=0

return,d

end

   
