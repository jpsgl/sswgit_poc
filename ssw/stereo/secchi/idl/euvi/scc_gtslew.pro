
function scc_gtslew,ts,te,s_c,file=file,dir=dir,no_grh=no_grh,nrl=nrl

;+
; $Id: scc_gtslew.pro,v 1.4 2010/04/13 21:21:08 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gtslew
;
; Purpose : reads a raw telemetry file (from SSC website) and extracts
;           the data from the gt slew packets (Apid 1070)
;
; Use     : IDL> d = scc_gtslew(file=file_search('secchi_ahead*ptp'))
;           IDL> d = scc_gtslew(ts,te,'a',dir='/User/wuelser/telem_a')
;
; Inputs  :
;    ts = start time (anytim format).
;    te = end time.
;    s_c = 'a' or 'b'
; all 3 inputs required unless file keyword supplied
;
; Outputs :
;    d = structure vector containing gt engineering data.
;
; Keywords:
;    file = file list  (input, optional)
;    dir = directory for automatic file search (used only if file not supplied)
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;    /nrl : use default dir path at NRL (instead of LMSAL).
;           Ignored if either dir or file are supplied.
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 1-Nov-2006
;               JPW 20-Dec-2006 : added "int" time format for easier utplot
;               William Thompson, 1-Feb-2006, made platform independent
;
; $Log: scc_gtslew.pro,v $
; Revision 1.4  2010/04/13 21:21:08  nathan
; find correct level-0 directory
;
; Revision 1.3  2007/06/22 18:34:27  mcnutt
; corrected directories
;
; Revision 1.2  2007/02/02 14:44:13  thompson
; *** empty log message ***
;
; Revision 1.1  2007/01/12 03:19:34  euvi
; initial version
;
;-

n = n_elements(file)
if n eq 0 then begin
   file = scc_time2tlmfile(ts,te,s_c,dir=dir, nrl=nrl)
   n = n_elements(file)
endif

if keyword_set(no_grh) then ghb=0 else ghb=26   ; length of ground recpt hdr
ghi = ghb/2                                     ; length in 16bit words

d0 = {time:0L,day:0L,t:0d0,n:0U,rawmin:intarr(4),rawmax:intarr(4), $ ; data
      sum1:lonarr(2),sum2:ulonarr(2),sumyz:0L,tunc:0d0}              ; struct.
d0 = replicate(d0,5)               ; slew packet has 5 data entries
it1  =  6+ghb
it2  = 10+ghb
icnt = lindgen(5)*24+6                                ; index to sum count
irmi = (lindgen(20) mod 4)+(lindgen(20)/4)*24+7+ghi   ; index to rawmin data
irma = irmi + 6                                       ; index to rawmax data
isu1 = (lindgen(10) mod 2) + (lindgen(10)/2)*12 + 10  ; index to sum1 (long)
isu2 = isu1 + 2                                       ; index to sum2 (long)
isyz = lindgen(5)*12+14                               ; index to sumyz (long)
toff = (dindgen(5)-4d0)/5d0        ; rel. time correction for ea. entry time
trc  = 0d0                         ; initial time of previous packet

rec = intarr(136+ghi)              ; 1 packet
dt = 7670L*24L*3600L               ; diff. betw. SC and anytim time def.
apid = 1070                        ; apid for gteng data

; intermediate size data vector to improve speed (d=[d,d0] is slow!)
n10 = 1440L
d10 = replicate(d0[0],n10*5L)
i10 = 0L

on_ioerror,endfil
for i=0,n-1 do begin

  openr,lun,file[i],/get_lun
  while 1 do begin
    readu,lun,rec
    apid0 = rec[ghi]
    ieee_to_host, apid0
    if (apid0 and 2047) eq apid then begin
      lrc = long(rec,ghb,68)       ; create long type copy of record
      ieee_to_host, lrc
      urc = uint(rec,ghb,136)      ; create unsigned type copy of record
      ieee_to_host, urc
      ulc = ulong(rec,ghb,68)      ; create unsigned long copy of record
      ieee_to_host, ulc
      trn0 = long(rec,it1)
      ieee_to_host, trn0
      trn = double(trn0-dt)
      trn0 = double(byte(rec,it2))
      trn = trn + trn0/256d0
      d0.tunc = trn                ; uncorrected packet time
      dtnc = (trn-trc) < 120.      ; assuming at least 1 packet every 120 sec
      d0.t = trn + toff*dtnc       ; interpolate sample times
      trc = trn                    ; save uncorrected time for next packet
      d0.n = urc[icnt]
      rawmin0 = reform(rec[irmi],4,5)
      ieee_to_host, rawmin0
      d0.rawmin = rawmin0
      rawmax0 = reform(rec[irma],4,5)
      ieee_to_host, rawmax0
      d0.rawmax = rawmax0
      d0.sum1 = reform(lrc[isu1],2,5)
      d0.sum2 = reform(ulc[isu2],2,5)
      d0.sumyz = lrc[isyz]

      d10[i10*5L] = d0
      i10 = i10 + 1L
      if i10 eq n10 then begin
         if n_elements(d) gt 0L then d=[d,d10] else d=d10
         i10 = 0L
      endif

    endif
  endwhile
  endfil: free_lun,lun

endfor

if i10 gt 0L then begin
   d10 = d10[0L:i10*5L-1L]
   if n_elements(d) gt 0L then d=[d,d10] else d=d10
endif

if n_params() ge 2 and n_elements(d) gt 0 then begin
   ww = where(d.t ge anytim(ts) and d.t le anytim(te),nww)

   if nww gt 0 then d=d[ww] else d=0
endif

; add time in "int" format for easier utplot
siz=size(d)
if siz[siz[0]+1] eq 8 then begin
 ti = anytim(d.t,/int)
 d.time = ti.time
 d.day  = ti.day
endif else d=0

return,d

end
