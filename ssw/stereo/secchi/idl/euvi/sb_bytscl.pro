function sb_bytscl,i,col,autoscale=auto

;+
; $Id: sb_bytscl.pro,v 1.2 2008/04/16 20:42:12 euvi Exp $
;
; Project : STEREO SECCHI
;
; Name    : sb_bytscl
;
; Purpose : Scale image to 8-bit using parameters min, max, gamma 
;
; Use     : IDL> b_image = sb_bytscl(image,col)
;
; Inputs  :
;    image = input image (float or integer)
;    col   = structure containing (at least) the following tags:
;            .min pixel value to be mapped to 0
;            .max pixel value to be mapped to 255
;            .gam gamma value to be used for mapping
; Outputs :
;    b_image = byte scaled image
; Keywords:
;    /autoscale : derive min/max from image, and update col structure
; Common  : None
; Restrictions:
; Side effects:
; Category    : Image processing
; Prev. Hist. :
; Written     : Jean-Pierre Wuelser, LMSAL, 6-Sep-2006
;               JPW 20-mar-2008 : using log scaling for neg. col.gam
;
; $Log: sb_bytscl.pro,v $
; Revision 1.2  2008/04/16 20:42:12  euvi
; Update (log scaling)
;
;-

if keyword_set(auto) then begin
   col.min = min(i,max=dmax)
   col.max = dmax
endif

mn = col.min
mx = col.max
sz = size(i)

; min eq max not allowed
if mn eq mx then return,bytarr(sz[1],sz[2])

; min gt max is allowed, and creates a "negative"
if mn gt mx then begin
   d  = -i
   mn = -mn
   mx = -mx
endif else d = i

d = (mn > d < mx)

; if min/max symmetric about 0: apply gamma separately to pos/neg values
if -mn eq mx then begin
  s = fltarr(sz[1],sz[2])+1.0
  w = where(d lt 0.,nw)
  if nw gt 0 then s[w] = -1.0    ; s is sign(d)
  if col.gam ge 0.0 then b=byte(128.0+127.9*s*(s*d/mx)^col.gam) $
    else b=byte(128.0+127.9*s*(col.gam-alog10(s*d/mx>10.0^col.gam))/col.gam)
endif else begin
  if col.gam ge 0.0 then b=byte(255.9 * ((d-mn)/(mx-mn))^col.gam) $
    else b=byte(255.9*(col.gam-alog10((d-mn)/(mx-mn)>10.0^col.gam))/col.gam)
endelse

return,b

end
