
pro plotoffpoints, times, AHEAD=ahead, BEHIND=behind, DATA=data, REFRESH=refresh, _EXTRA=_extra

;+
; $Id: plotoffpoints.pro,v 1.2 2009/04/23 18:10:07 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : plotoffpoints
;
; Purpose : Read in and plot STEREO offpoint info from $SCC_DATA/gt/bigjitter?5.txt 
;
; Use     : plotoffpoints,['2008-12-25 12:00','2008-12-26 11:00'],/ahead
;
; Inputs  : times   STRING or CDS time structure    Returns data for that day
;   	    -or-
;   	    times   STRARR or CDS time array	    Plots between these two times
;   	    /Ahead or /Behind	Which spacecraft to get data for
;
; Optional Output : 
;   	    DATA=   Structure of arrays containing data of format:
;
;   TIME            STRING    Array   Start of excursion
;   DUR             FLOAT     Array   Duration excursion above 5 arcsec
;   MAGN            FLOAT     Array   Average magnitude of excursion during dur
;
; Keywords:
;   /REFRESH	Read in GT data again from data file
;
; Common  : BIGOFFPOINTS
;
; Restrictions: Requires $SCC_DATA
;   Uses files which are written by reportbigjitter.pro
;
; Side effects: Draws a plot on current window
;
; Category    : gt attitude pointing
;
; Prev. Hist. :
;
; Written     : N.Rich, NRL/Interferometrics
;
; $Log: plotoffpoints.pro,v $
; Revision 1.2  2009/04/23 18:10:07  nathan
; put range checks in time interval
;
; Revision 1.1  2009/04/23 16:46:27  nathan
; for plotting GT offpoint data
;
;-

COMMON bigoffpoints, sgta, sgtb

IF keyword_set(AHEAD) THEN BEGIN
    dtp=datatype(sgta) 
    aorb='A' 
ENDIF ELSE $
IF keyword_set(BEHIND) THEN BEGIN
    aorb='B' 
    dtp=datatype(sgtb) 
ENDIF ELSE BEGIN
    message,'Please specify /Ahead or /Behind.',/info
    return
ENDELSE

outfil=concat_dir(getenv_slash('SCC_DATA')+'gt','bigjitter'+aorb+'5.txt')

IF keyword_set(REFRESH) or dtp EQ 'UND' THEN BEGIN
    IF file_exist(outfil) THEN BEGIN
	restore,concat_dir(getenv_slash('SCC_DATA')+'templates','bigjittertmpl.sav')
	sgt=read_ascii(outfil,TEMPLATE=bigjittertmpl)
    ENDIF ELSE BEGIN
	print,outfil,' not found.'
	return
    ENDELSE
    IF aorb EQ 'A' THEN sgta=sgt ELSE sgtb=sgt
ENDIF ELSE $
    IF aorb EQ 'A' THEN sgt=sgta ELSE sgt=sgtb

data=sgt

; Get timerange to plot = tr
szgt=n_elements(sgt.time)
ut0=anytim2utc(sgt.time[0])
ut1=anytim2utc(sgt.time[szgt-1])
uts=ut0
ute=ut1
IF n_params() GE 1 THEN BEGIN
    ndt=n_elements(times)
    IF ndt GT 1 THEN BEGIN
    	uts=anytim2utc(times[0])
	ute=anytim2utc(times[1])
    ENDIF ELSE BEGIN
    ; get one day
    	utx=anytim2utc(times)
	utx.time=0
	uts=utx
	ute=utx
	ute.mjd=ute.mjd+1
    ENDELSE
ENDIF 
IF ut0.mjd LT uts.mjd THEN ut0=uts ELSE message,'Warning! First data point is at '+utc2str(ut0),/info
IF ut1.mjd GT ute.mjd THEN ut1=ute ELSE message,'Warning! Last data point is at '+utc2str(ut1),/info
tr=[ut0,ut1]

magx=0
durx=0
;y=sgt.magn
ylab='Arcsec'

w=where(sgt.dur GT durx and sgt.magn GT magx, nw)

IF nw GT 0 THEN BEGIN
    ugt=anytim2utc(sgt.time[w])
    mgt=sgt.magn[w]
    dgt=sgt.dur[w]
    ; get 3 more points for every existing point in w
    ugtx4=replicate(ut0,nw*4)
    mgtx4=fltarr(nw*4)
    FOr i=0,nw-1 DO BEGIN
    	ugtx4[i*4:(i*4)+3]=ugt[i]
	ugtx4[i*4].time=ugtx4[i*4].time-1
	mgtx4[i*4+1]=mgt[i]
	ugtx4[i*4+2].time=ugtx4[i*4].time+1000*dgt[i]
	mgtx4[i*4+2]=mgt[i]
	ugtx4[i*4+3].time=ugtx4[i*4+2].time+1
    ENDFOR
    utplot,ugtx4,mgtx4,title='GT Excursions on SECCHI-'+aorb,ytitle=ylab, timerange=tr, _EXTRA=_extra

ENDIF
end
