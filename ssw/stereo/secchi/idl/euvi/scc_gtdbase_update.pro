
pro scc_gtdbase_update,dbfile=dfil,tlmdir=tdir,no_grh=no_grh,nrl=nrl,quiet=quiet

;+
; $Id: scc_gtdbase_update.pro,v 1.4 2007/06/28 17:18:11 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : scc_gtdbase_update
;
; Purpose : creates or updates a GT calibration and offpoint database
;           based on the gt engineering packets (Apid 1069) in the Level 0
;           telemetry packet files for SECCHI
;
; Use     : IDL> scc_gtdbase_update
;
; Inputs  : none
;
; Outputs : none
;
; Keywords:
;    dbfile  : path/name of database file to be created/updated.
;              Default: secchi_gtdbase.geny in current directory
;    tlmdir  : directory(ies) with telemetry packet files.  Can be vector,
;              e.g., with different elements for Ahead and Behind.
;              Default: appropriate for local LMSAL installation
;    /nrl    : Change tlmdir default to one appropriate for NRL
;    /no_grh : no ground receipt header (default is 26 byte ground recpt hdr)
;    /quiet  : suppress list of read telemetry files
;
; Common  : None
;
; Restrictions:
;
; Side effects:
;    Creates/updates GT dbase file.
;    Contents: structure with tags a and b, stored in a .genx file.
;    Tags a and b each a structure vector described in scc_gtparms_1tf.pro
;    
; Category    : Pipeline
;
; Prev. Hist. :
;
; Written     : Jean-Pierre Wuelser, LMSAL, 18-Jan-2007 based on scc_gteng
;
; $Log: scc_gtdbase_update.pro,v $
; Revision 1.4  2007/06/28 17:18:11  nathan
; fix tdir
;
; Revision 1.3  2007/06/22 18:34:27  mcnutt
; corrected directories
;
; Revision 1.2  2007/02/27 00:12:09  nathan
; add info about input and output files
;
; Revision 1.1  2007/02/19 23:09:03  euvi
; initial version
;
;-

; set appropriate default tlmdir
if n_elements(tdir) eq 0 then begin
   obs = ['ahead','behind']
   if keyword_set(nrl) then tdir=getenv_slash('SDS_DIR') $
      +obs+'/data_products/level_0_telemetry/secchi' $
   else tdir='$secchi/telemetry/'+obs+'/secchi'
endif
ndir = n_elements(tdir)

; set default dbfile
if n_elements(dfil) ne 1 then dfil='$SCC_DATA/gt/secchi_gtdbase.geny'

; read the dbfile, if it exists
if file_search(dfil) ne '' then begin
   restgenx,db,file=dfil                      ; read existing db
   asiz = size(db.a)
   bsiz = size(db.b)
   dbn = [asiz[asiz[0]+2],bsiz[bsiz[0]+2]]   ; # of entries in existing db
   if asiz[asiz[0]+1] ne 8 then dbn[0]=0     ; check for 0 entries, ahead
   if bsiz[bsiz[0]+1] ne 8 then dbn[1]=0     ; check for 0 entries, behind
endif else dbn=[0L,0L]

IF ~keyword_set(QUIET) THEN BEGIN
    print,'Tlm dir= ',tdir
    print,'Dbase file= ',dfil
ENDIF
; find all relevant telemetry files
for i=0,ndir-1 do begin       ; loop through directories
  ; look for .ptp files
  ffi = file_search(tdir[i],'secchi_*.ptp',count=nffi,/nosort)
  if nffi gt 0 then begin
    if n_elements(ff) gt 0 then ff=[ff,ffi] else ff=ffi
  endif
  ; look for .fin files
  ffi = file_search(tdir[i],'secchi_*.fin',count=nffi,/nosort)
  if nffi gt 0 then begin
    if n_elements(ff) gt 0 then ff=[ff,ffi] else ff=ffi
  endif
endfor
if n_elements(ff) eq 0 then begin
  print,'No telemetry files found'
  return
endif

; create fid and ver as used in the dbase
yr  = long(strmid(ff,16,4,/reverse_offset))
doy = long(strmid(ff,11,3,/reverse_offset))
num = long(strmid(ff,7,1,/reverse_offset))
oid = long(strmid(ff,22,1,/reverse_offset) ne 'a')  ; observatory id a:0, b:1
fid = yr*100000L + doy*100L + num*10L + oid         ; fid as used in dbase
ver = fix(strmid(ff,5,2,/reverse_offset))           ; version
typ = fix(strmid(ff,2,3,/reverse_offset) eq 'fin')
; make version # of .fin files start with 100
ver = 100*typ+ver                                   ; ver as used in dbase
fiv = (fid-2000L*100000L)*200L+long(ver)            ; combination of fid,ver

; latest version of each fid, in sorted order
i = uniq(fid,sort(fiv))
ff  = ff[i]
fid = fid[i]
ver = ver[i]
oid = oid[i]
nff = n_elements(fid)

; loop through latest version telemetry files
for i=0L,nff-1 do begin
  rr = 1                                                  ; flag for tlm read
  oi = oid[i]                                             ; obs id this file

  ; if db non-empty, search for this fid, and reset rr if up-to-date
  if dbn[oi] gt 0 then begin                              ; db non-empty?
    ww = where(db.(oi).fid eq fid[i],nn)                  ; search db
    if nn gt 0 then begin                                 ; entries exist
      if min(db.(oi)[ww].ver) ge ver[i] $                 ; up-to-date?
        then rr=0 $                                       ;  y: reset rr
        else db.(oi)[ww].flg = -2                         ;  n: flag old db
    endif
  endif

  ; read tlm file, if rr set
  if rr eq 1 then begin
    if keyword_set(quiet) eq 0 then print,' reading '+ff[i]
    d1 = scc_gtparms_1tf(ff[i])                           ; read tlm file
    ; add data to new db vector "d"
    od1 = intarr(n_elements(d1))+oi                       ; obs id of data
    if n_elements(d) gt 0 then begin                      ; d defined?
      d  = [d,d1]
      od = [od,od1]
    endif else begin                                      ; d not defined:
      d  = d1
      od = od1
    endelse
  endif
endfor

; merge the old and new databases
; loop through ahead/behind
for oi=0,1 do begin
  ; determine which entries to merge
  if dbn[oi] gt 0 then kk=where(db.(oi).flg ne -2,nkk) $
                  else nkk=0                              ; old entries to keep
  if n_elements(od) gt 0 then jj=where(od eq oi,njj) $
                         else njj=0                       ; new entries for obs
  ; 4 cases:
  if njj eq 0 then begin                                  ; no new entries
    if nkk eq 0 then c=0 else c=db.(oi)[kk]
  endif else begin                                        ; new entries exist
    if nkk eq 0 then c=d[jj] else c=[db.(oi)[kk],d[jj]]
    c = c[sort(c.t)]                                      ; sort by time
  endelse

  ; update flags
  siz = size(c)
  if siz[siz[0]+1] ne 8 then nw=0 $                       ; find valid data
    else w=where(c.flg ge 0,nw)
  if nw gt 0 then c[w].flg=1                         ; set all to 1
  if nw gt 2 then begin
    ; find all entries (except last) where parameters haven't changed:
    d2 = abs(c[w[1:nw-2]].cg2 - c[w[0:nw-3]].cg2)
    d4 = abs(c[w[1:nw-2]].cg4 - c[w[0:nw-3]].cg4)
    ww = where(total(d2,1)+total(d4,1) eq 0,nww)
    ; set flg of those to 0:
    if nww gt 0 then c[w[ww+1L]].flg=0
  endif

  if oi eq 0 then a=c else b=c
endfor

; save updated database
db = {a:a,b:b}
savegenx,db,file=dfil,/overwrite

end
