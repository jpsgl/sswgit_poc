pro scc_att_correct,d1,d2,COR1=cor1,COR2=cor2,EUVI=euvi,HI1=hi1,HI2=hi2,ALL=all
;+
; $Id: scc_att_correct.pro,v 1.4 2009/04/29 15:33:54 secchib Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_att_correct
;               
; Purpose   : Revise headers of already processed FITS files
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :  scc_att_correct,'2006-10-26','2007-03-01',/all
;               
; Outputs   : 
;
; Keywords  :     COR1=cor1,COR2=cor2,EUVI=euvi,HI1=hi1,HI2=hi2, ALL=all
;
; Calls from LASCO : 
;
; Common    : SCC_REDUCE, dbms
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline (see http://durance.oamp.fr/lasco/fichiers/soft_categories.html)
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2004
;               
; $Log: scc_att_correct.pro,v $
; Revision 1.4  2009/04/29 15:33:54  secchib
; changes date_mod for history comment
;
; Revision 1.3  2009/04/15 11:55:31  secchib
; sets INS_(XY)0 to deg if HI
;
; Revision 1.2  2009/04/15 11:30:51  secchib
; defined crvals from header for print statements
;
; Revision 1.1  2009/04/15 11:22:51  mcnutt
; updates attitude info if fits where created with out cuurent ATT files
;

common scc_reduce,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, resetvars, $
    	    	    ipdat, nom_tlr, prevbasename, readfileidline, wavefileidline, $
		    setupfileidline, exposfileidline, maskfileidline, ipfileidline, icerdiv2flag, $
		    rotblidlines,hbtimes,hbtemps,cebtimes,cebtemps, subdir, date_fname_utc, ispb, islz, isrt, aorb, $
		    iserror, ephemfil, att_file
common dbms,ludb, dbfile, delflag, img_seb_hdr, img_seb_hdr_ext, img_cal_bias, $
	    img_cal_led, img_cal_dark, img_hist_cmnt, img_euvi_gt    	

idline='$Id: scc_att_correct.pro,v 1.4 2009/04/29 15:33:54 secchib Exp $'
idinfo=strmid(idline,1,29)
longwait=10
shortwait=2
aorb=getenv("SCC_PLATFORM")   
    aorblc=strlowcase(aorb)
    IF aorb EQ 'A' THEN BEGIN
    ; these are used in call to sccdopolar
    	aheadf=1
	behindf=0
	scname='ahead'
    ENDIF ELSE BEGIN
    	aheadf=0
	behindf=1
	scname='behind'
    ENDELSE

;defines compression
    ssss = scc_read_ip_dat(IPver=IPver)
    ipdat=replicate('ERROR',256)
    for i=0,n_elements(ssss.ip_description)-1 do ipdat[i]=ssss[i].ip_description

   z=where(strmid(ipdat,0,6) eq 'H-comp')
   ipcmprs=ipdat
   ipcmprs(z)='HC'+strmid(ipdat(z),strlen(ipdat(z(0)))-2,1)
   ipcmprs(where(ipdat eq 'No Compression')) = 'NONE'
   ipcmprs(where(ipdat eq 'Rice Compression')) = 'RICE'
   ipcmprs(where(ipdat eq 'Header Only')) = 'HDRO'

ud1=anytim2utc(d1)
IF n_params() GT 1 THEN ud2=anytim2utc(d2) ELSE ud2=ud1
ud=ud1

tels=['none']
IF keyword_set(COR1) THEN tels=[tels,'cor1']
IF keyword_set(COR2) THEN tels=[tels,'cor2']
IF keyword_set(EUVI) THEN tels=[tels,'euvi']
IF keyword_set(HI1) THEN tels=[tels,'hi_1']
IF keyword_set(HI2) THEN tels=[tels,'hi_2']
IF keyword_set(all) then tels=['none','euvi','cor1','cor2','hi_1','hi_2']
nt=n_elements(tels)-1
IF nt LT 1 THEN BEGIN
	message,'Must specify one or more telescopes. Returning.',/info
	return
ENDIF
tels=tels(1:n_elements(tels)-1)

IF ud1.mjd GT ud2.mjd THEN incr=-1 ELSE incr=1
FOR mjd=ud1.mjd,ud2.mjd,incr DO BEGIN
      ud.mjd=mjd
      yyyymmdd=utc2yymmdd(ud,/yyyy)
      isc='*'

         for i=0,n_Elements(tels)-1 do begin
           fitsfiles=findfile(getenv('SECCHI')+'/'+isc+'/'+tels(i)+'/'+yyyymmdd+'/*.fts')
            if i eq 0 then list=fitsfiles else list=[list,fitsfiles]
        endfor

      imgcheck=where(list ne '')
      if (imgcheck(0) gt -1) then begin
 	fitsfiles=list(where(list ne ''))
        pipeline ='atf'
        dbroot=yyyymmdd
        lulog=9
        sccattic=getenv('SCC_ATTIC')+'/'+strmid(yyyymmdd,2,6)+'/'
;        unblock_cmd = concat_dir(getenv('UTIL_DIR'),'unblockSciFile')
;open log file need for write db_script
    	logfile= concat_dir(getenv('SCC_LOG'),'scc'+getenv('SC')+pipeline+dbroot+'.log')
	openw,lulog,logfile,/append
;	printf,lulog,version
	printf,lulog,' '
	;text=readlist('sebhdr_struct.inc')
	text=[';*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO', $
	      ';*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Thu May 19 15:11:05 2005']
	printf,lulog,'NOTE: sebhdr info is static -- need to generate from sebhdr4_struct.inc'
	printf,lulog,text[0]
	printf,lulog,text[1]
	printf,lulog,'starting log file at '+systime()
	printf,lulog,' '
	printf,lulog,'SEB_IMG	',getenv('seb_img')
	printf,lulog,'SCI_SOURCE ',getenv('SCI_SOURCE')
	for i=0,n_Elements(tels)-1 do printf,lulog,'SECCHI	',getenv('SECCHI')+'/*/'+tels(i)+'/'+yyyymmdd
	printf,lulog,'SCC_ATTIC ',sccattic
;	printf,lulog,unblock_cmd
;	printf,lulog,uncompr_cmd,' or '
;	printf,lulog,huncompr_cmd,' or '
;	printf,lulog,iuncompr_cmd

	printf,lulog,'IDL_PATH	',getenv('IDL_PATH')
	printf,lulog,'TABLES	',getenv('TABLES')
	printf,lulog,'PT    	',getenv('PT')
	printf,lulog,'DATE: ',yyyymmdd
	printf,lulog,'==============================='


    	nf=n_elements(fitsfiles)

	; go ahead and make db files for SPWX or PB, even though not (yet) using them
        SPAWN,'hostname',machine,/sh  
         machine = machine(N_ELEMENTS(machine)-1)
        src=strmid(machine,0,1)+rstrmid(machine,0,1)
	dbfile=concat_dir(getenv('SCC_DB'),'scc'+getenv('SC')+pipeline+src+dbroot+'sql.script')
	openw,ludb,dbfile,/get_lun
	dbfstat=fstat(ludb)
	IF dbfstat.size EQ 0 THEN BEGIN
	; only at beginning of each script file
	    printf,ludb,'use secchi_flight;'
	    printf,ludb,'start transaction;'
	ENDIF
	get_utc,ustarttime
	starttime=utc2str(ustarttime,/ecs)
	printf,ludb,'# starting dbfile at '+starttime
	close,ludb
	free_lun,ludb

  FOR i=0,nf-1 DO BEGIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   print,fitsfiles[i]
    img=sccreadfits(fitsfiles[i],scch,/nodata)
    get_utc,date_mod,/ccsds,/noz
    openw,ludb,dbfile,/append,/get_lun

        
        crval1=scch.crval1
        crval2=scch.crval2
        crval1A=scch.crval1A
        crval2A=scch.crval2A

        time0=systime(1)

        date_obs_utc=anytim2utc(scch.DATE_CMD, /nocorrect)
	IF date_obs_utc.mjd LT ustarttime.mjd-45 THEN $
	sdspath=getenv('SDS_FIN_DIR')+'/'+aorblc+'/'+strmid(scch.DATE_CMD,0,4)+'/'+strmid(scch.DATE_CMD,5,2) ELSE $
	sdspath=getenv('SDS_DIR')+'/'+scname+'/data_products/level_0_telemetry/secchi'

        IF (scch.detector EQ 'COR2') THEN cor2_point,scch,dir=sdspath,ERROR=gterr    	
	IF (scch.detector EQ 'COR1') THEN cor1_point,scch,dir=sdspath,ERROR=gterr
        IF (scch.detector EQ 'EUVI') THEN  euvi_point,scch,/forceroll
        IF (scch.detector EQ 'HI1' or scch.detector EQ 'HI2') THEN scch = hi_point_v2(scch,exthdr,dir=sdspath)

	printf,lulog,'GT tlm dir='+sdspath
	crv1c=''
	print,' CRVAL1='+trim(scch.crval1)+', before GT was '+trim(crval1)
	printf,lulog,scch.date_obs+',CRVAL1=,'+trim(scch.crval1)+',before GT was,'+trim(crval1)
	crv2c=''
	print,' CRVAL2='+trim(scch.crval2)+', before GT was '+trim(crval2)
	printf,lulog,scch.date_obs+',CRVAL2=,'+trim(scch.crval2)+',before GT was,'+trim(crval2)
	crv1ac=''
	print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(crval1A)
	printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(crval1A)
	crv2ac=''
	print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(crval2A)
	printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(crval2A)
	crp1c=''
	crp2c=''
	fxhmodify,fitsfiles[i],'SC_YAW',scch.sc_yaw,'arcsec from GT'
	fxhmodify,fitsfiles[i],'SC_PITCH',scch.sc_pitch,'arcsec from GT'
	fxhmodify,fitsfiles[i],'SC_YAWA',scch.sc_yawa,'degrees from GT'
	fxhmodify,fitsfiles[i],'SC_PITA',scch.sc_pita,'degrees from GT'
       	fxhmodify,fitsfiles[i],'DATE',date_mod

	pointingspicetime=systime(1)-time0
	help,pointingspicetime
	printf,lulog,'pointing SPICE took'+string(pointingspicetime)
	fxhmodify,fitsfiles[i],'CRPIX1',scch.crpix1,crp1c
	fxhmodify,fitsfiles[i],'CRPIX2',scch.crpix2,crp2c
	fxhmodify,fitsfiles[i],'CRVAL1',scch.crval1,crv1c
	fxhmodify,fitsfiles[i],'CRVAL2',scch.crval2,crv2c
	fxhmodify,fitsfiles[i],'CDELT1',scch.cdelt1
	fxhmodify,fitsfiles[i],'CDELT2',scch.cdelt2
	fxhmodify,fitsfiles[i],'XCEN',scch.xcen
	fxhmodify,fitsfiles[i],'YCEN',scch.ycen
	fxhmodify,fitsfiles[i],'CRPIX1A',scch.crpix1a
	fxhmodify,fitsfiles[i],'CRPIX2A',scch.crpix2a
	fxhmodify,fitsfiles[i],'CRVAL1A',scch.crval1a
	fxhmodify,fitsfiles[i],'CRVAL2A',scch.crval2a
	fxhmodify,fitsfiles[i],'CDELT1A',scch.cdelt1a
	fxhmodify,fitsfiles[i],'CDELT2A',scch.cdelt2a
	fxhmodify,fitsfiles[i],'ATT_FILE',scch.att_file
	fxhmodify,fitsfiles[i],'CROTA',scch.crota
	fxhmodify,fitsfiles[i],'PC1_1',scch.pc1_1
	fxhmodify,fitsfiles[i],'PC1_2',scch.pc1_2
	fxhmodify,fitsfiles[i],'PC2_1',scch.pc2_1
	fxhmodify,fitsfiles[i],'PC2_2',scch.pc2_2
	fxhmodify,fitsfiles[i],'PC1_1A',scch.pc1_1a
	fxhmodify,fitsfiles[i],'PC1_2A',scch.pc1_2a
	fxhmodify,fitsfiles[i],'PC2_1A',scch.pc2_1a
	fxhmodify,fitsfiles[i],'PC2_2A',scch.pc2_2a
	IF (scch.detector EQ 'HI1' or scch.detector EQ 'HI2') THEN BEGIN 
	  fxhmodify,fitsfiles[i],'INS_X0',scch.ins_x0,' deg'
	  fxhmodify,fitsfiles[i],'INS_Y0',scch.ins_y0,' deg'
 	ENDIF ELSE BEGIN
	  fxhmodify,fitsfiles[i],'INS_X0',scch.ins_x0,' arcsec'
	  fxhmodify,fitsfiles[i],'INS_Y0',scch.ins_y0,' arcsec'
        ENDELSE
	fxhmodify,fitsfiles[i],'INS_R0',scch.ins_r0,' deg'
        tmpzind=where(strpos(scch.history,'_point') gt -1)
	ind=n_elements(tmpzind)-1
	hinfo=scch.history[tmpzind(ind)]
	fxhmodify,fitsfiles[i],'HISTORY',hinfo
       	fxhmodify,fitsfiles[i],'HISTORY',idinfo
        delflag=0

	printf,ludb,'update img_seb_hdr_ext set CROTA="'+strtrim(string(scch.CROTA))+'" where date_obs="'+scch.date_obs+'" and fileorig="'+scch.fileorig+'";' 
          IF datatype(img_hist_cmnt) EQ 'UND' THEN $
                                 @./secchi_img_tables_struct.inc
	  a=img_hist_cmnt
  	  a.FILENAME  	=scch.filename
  	  a.DATE_OBS  	=scch.date_obs
  	  a.FLAG      	='history'
  	  a.INFO      	=hinfo
          a.DATE_MOD  	=date_mod
          scc_db_insert,a
          date_mod=utc2str(tai2utc(utc2tai(date_mod)+1),/ecs)
 	  a=img_hist_cmnt
  	  a.FILENAME  	=scch.filename
  	  a.DATE_OBS  	=scch.date_obs
  	  a.FLAG      	='history'
  	  a.INFO      	=idinfo
          a.DATE_MOD  	=date_mod ; add one second for unique date_mod
         scc_db_insert,a


	  close,ludb
	  free_lun,ludb


  ENDFOR    ; each FITS file
endif	; imgcheck

if (datatype(lulog) ne 'UND')then close,lulog

ENDFOR ; each day

end
