; $Id: scc_move_attic.pro,v 1.4 2015/07/23 15:42:43 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : scc_move_attic
;               
; Purpose     : to create daily attic directory and move processed raw sci files to sci_attic
;               
; Explanation : 
;               
; Use         : IDL> scc_move_attic,yyyymmdd,aorblc
;    
; Inputs      : 
;               
; Outputs     : 
;
; Keywords :
;
; Calls from LASCO : getenv_slash
;
; Common      : 
;               
; Restrictions: Requires env variables defined in spacecraft- and pipeline-specific *.env 
;   	    	file in $SSW_SECCHI/idl/pipeline
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Jul 2012
; -              
; Modified    :
;
; $Log: scc_move_attic.pro,v $
; Revision 1.4  2015/07/23 15:42:43  secchia
; added chmod to allow meta files to be averwriten in sci(ab)
;
; Revision 1.3  2013/07/10 11:36:05  mcnutt
; change file_move to spwan,mv the keep time stamps correct
;
; Revision 1.2  2012/07/17 17:07:06  secchib
; corrected link added slash
;
; Revision 1.1  2012/07/17 12:22:41  secchib
; New
;
      
pro scc_move_attic,yyyymmdd,aorblc

        sciattic=getenv_slash('SCI_ATTIC')
        localattic=getenv_slash('SCC_ATTIC')
	scidir=getenv_slash('sci'+aorblc)
	yymmdd=strmid(yyyymmdd,2,6)
    	IF ~(is_dir(sciattic+yymmdd)) THEN BEGIN
	    spawn,'mkdir -p '+sciattic+yymmdd,/sh
	    wait,2
	ENDIF
	IF ~(is_dir(sciattic+yymmdd)) THEN BEGIN
           openw,dlun,'attic_dir_error.txt',/get_lun,/append
           printf,dlun,sciattic+yymmdd+' not creeated'
	   close,dlun
	ENDIF ELSE BEGIN
           files=file_search(localattic+yymmdd+'/*')
           if files(0) ne '' then begin
              i1=where(strpos(files,'.meta') eq -1)
              telnum=rstrmid(files(i1),0,1)
	      telnum=telnum(sort(telnum))
	      telnum=telnum(uniq(telnum))
              cmd='chmod ug+w '+sciattic+yymmdd+'/*.meta'
              spawn,cmd
	      wait,2
              spawn,'mv '+localattic+yymmdd+'/*'+' '+sciattic+yymmdd+'/'
              attfiles=file_search(sciattic+yymmdd+'/*.??['+telnum+']*')
	      ;file_move,files,sciattic+yymmdd
	      if n_elements(files) eq n_elements(attfiles) then spawn,'rmdir '+localattic+yymmdd,/sh else begin
                openw,dlun,'attic_dir_error.txt',/get_lun,/append
                printf,dlun,'Not deleting local attic Number of files moved to attic does not match '+localattic+yymmdd
	        close,dlun
	      endelse
	      wait,2
	   endif else begin
             openw,dlun,'attic_dir_error.txt',/get_lun,/append
             printf,dlun,'no files found in local attic '+localattic+yymmdd
	     close,dlun
	   endelse
	ENDELSE

	;--Check to see if need to make a link for sci dir
	lncmd='ln -s '+sciattic+yymmdd+' '+scidir+yymmdd
	IF sciattic NE scidir and ~file_exist(scidir+yymmdd) THEN BEGIN
	    print,lncmd
	    wait,5
	    spawn,lncmd,/sh
	ENDIF
	IF file_exist('attic_dir_error.txt') THEN BEGIN
;	    maillist='nathan.rich@nrl.navy.mil lynn.simpson@nrl.navy.mil'
	    maillist='lynn.simpson@nrl.navy.mil'
            free_lun,dlun
	    spawn,'mailx -s Attic_dir_error_'+aorblc+'_'+yymmdd+' '+maillist+' < attic_dir_error.txt',/sh
    	    spawn,'rm attic_dir_error.txt',/sh
       	ENDIF
END
