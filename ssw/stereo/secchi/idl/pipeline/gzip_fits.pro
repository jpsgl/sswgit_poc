pro gzip_fits,d1,d2,COR1=cor1,COR2=cor2,EUVI=euvi,HI1=hi1,HI2=hi2
;+
; $Id: gzip_fits.pro,v 1.5 2009/01/08 15:10:26 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : gzip_fits
;               
; Purpose   :copy and Gzip fits files into ;  SECCHI_GZ    '/net/tdspc1/data/secchi/lzgz/L0'
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :  gzip_fits,'2006-10-26','2007-03-01',/cor1,/cor2
;
; Outputs   : 
;
; Keywords  :     COR1=cor1,COR2=cor2,EUVI=euvi,HI1=hi1,HI2=hi2
;
; Calls from LASCO : 
;
; Common    :
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline (see http://durance.oamp.fr/lasco/fichiers/soft_categories.html)
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2004
;               
; $Log: gzip_fits.pro,v $
; Revision 1.5  2009/01/08 15:10:26  mcnutt
; removed stops accidently committed with secchi_reduce.pro v1.4
;
; Revision 1.4  2009/01/08 14:39:27  mcnutt
; date_fname_utc to date_cmd to complete v1.293 mods
;
; Revision 1.3  2008/01/16 11:55:15  secchia
; change findfiles to file_search
;
; Revision 1.2  2007/10/22 16:20:50  secchia
; added tel to output dir and corrected diskstatus to be last string in array
;
; Revision 1.1  2007/10/22 13:57:57  mcnutt
; new pro copies and gzips fits files stops when disk is 99% full
;
;

idline='$Id: gzip_fits.pro,v 1.5 2009/01/08 15:10:26 mcnutt Exp $'
idinfo=strmid(idline,5,34)
longwait=10
shortwait=2


ud1=anytim2utc(d1)
IF n_params() GT 1 THEN ud2=anytim2utc(d2) ELSE ud2=ud1
ud=ud1

tels=['none']
IF keyword_set(COR1) THEN tels=[tels,'cor1']
IF keyword_set(COR2) THEN tels=[tels,'cor2']
IF keyword_set(EUVI) THEN tels=[tels,'euvi']
IF keyword_set(HI1) THEN tels=[tels,'hi_1']
IF keyword_set(HI2) THEN tels=[tels,'hi_2']
nt=n_elements(tels)-1
IF nt LT 1 THEN BEGIN
	message,'Must specify one or more telescopes. Returning.',/info
	return
ENDIF
tels=tels(1:n_elements(tels)-1)


pipeline='gzip'
lulog=9
logfile= concat_dir(getenv('SCC_LOG'),'scc'+getenv('sc')+pipeline+utc2yymmdd(anytim2utc(d1),/yyyy)+'-'+utc2yymmdd(anytim2utc(d2),/yyyy)+'.log')
openw,lulog,logfile,/append


IF ud1.mjd GT ud2.mjd THEN incr=-1 ELSE incr=1
FOR mjd=ud1.mjd,ud2.mjd,incr DO BEGIN
      ud.mjd=mjd
      yyyymmdd=utc2yymmdd(ud,/yyyy)
;      gzdestdir='/net/tdspc1/data/secchi/lzgz/L0'
      gzdestdir=getenv('SECCHI_GZ')
      syscmd='df -h | grep '+strmid(gzdestdir,0,strpos(gzdestdir,'secchi')-1)
      spawn,syscmd,diskstat
      diskstatus=strsplit(diskstat(n_elements(diskstat)-1),/extract)
      gzdiskpercent= diskstatus(4)
      gzdiskpercent=strmid(gzdiskpercent,0,strlen(gzdiskpercent)-1)
       print,gzdiskpercent

      if (gzdiskpercent lt 99)then begin        
        for i=0,n_Elements(tels)-1 do begin
            fitsfiles=file_search(getenv('SECCHI')+'/*/'+tels(i)+'/'+yyyymmdd+'/*.fts')
            if i eq 0 then list=fitsfiles else list=[list,fitsfiles]

            imgcheck=where(fitsfiles ne '')
            if (imgcheck(0) gt -1) then begin
		cmd='/bin/rm -fr '+getenv('SECCHI_GZ')+'/'+getenv('sc')+'/*/'+tels[i]+'/'+yyyymmdd
		print,cmd
		print,''
		print,'Waiting 2...'
		wait,2	 ;5
		spawn,cmd,/sh
            endif            
        endfor
        imgcheck=where(list ne '')

        if (imgcheck(0) gt -1) then begin
 	  fitsfiles=list(where(list ne ''))
          for nfts=0,n_elements(fitsfiles)-1 do begin
            SUBDir=strmid(fitsfiles(nfts),strpos(fitsfiles(nfts),'/'+getenv('sc')+'/')+3,8)
	    outdirz=filepath( yyyymmdd,ROOT=getenv('SECCHI_GZ')+'/'+getenv('sc'),SUBD=SUBDir )
	    msg='Writing gzipped '+fitsfiles(nfts)+' to '+outdirz
	    print,msg
    	    spawn,'mkdir -p '+outdirz,/sh
    	    outfilez = concat_dir(outdirz,strmid(fitsfiles(nfts),strlen(fitsfiles(nfts))-25,25))
	    spawn,'cp '+fitsfiles(nfts)+' '+outfilez
	    spawn,'gzip '+outfilez
	    printf,lulog,msg
          endfor
        endif
    endif else printf,lulog,'stopping on day ',yyyymmdd,' gzip disk is ',diskstatus(4),' full'
ENDFOR

close,lulog

end
