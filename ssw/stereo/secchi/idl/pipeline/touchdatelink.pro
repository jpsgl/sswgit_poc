;+
; $Id: touchdatelink.pro,v 1.3 2011/06/29 19:30:23 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : touchdatelink
;               
; Purpose   : recreating date link after processing so timestamp is current
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :  touchdatelink,destdir,img_dir,tel
; Outputs   : 
;
; Keywords  :    
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline (see http://durance.oamp.fr/lasco/fichiers/soft_categories.html)
;               
; Prev. Hist. : None.
;
; Written     : pulled from secchi_reduce 01-14-2011
;               
; $Log: touchdatelink.pro,v $
; Revision 1.3  2011/06/29 19:30:23  nathan
; changed maillist to secchipipeline@cronus
;
; Revision 1.2  2011/01/28 13:22:59  secchib
; added keyword tellink for use with updatescchdr
;
; Revision 1.1  2011/01/14 14:12:46  secchib
; pulled from secchi_reduse to work with updatescchdr.pro
;
;
; pro for recreating date link after processing so timestamp is current
pro touchdatelink, destdir, img_dir,telj,yyyymmdd, tellink=tellink

    teldir=strlowcase(telj)

    IF teldir EQ 'hi1' THEN teldir='hi_1'
    IF teldir EQ 'hi2' THEN teldir='hi_2'

    link='none'
    if keyword_set(tellink) then telimgdir=tellink else telimgdir=getenv(strupcase(teldir)+'_'+strupcase(img_dir))
    IF strmid(telimgdir,0,25) NE strmid(destdir,0,25) THEN BEGIN
    	    ; create symbolic link in directory pointed to in $secchi
	    link=filepath( yyyymmdd,ROOT=destdir+'/'+img_dir+'/'+teldir)
	    ;outdir =filepath( yyyymmdd,ROOT=telimgdir)
	    spawn,['ls','-l',link],res,/noshell
	    parts=strsplit(res,/extract)
	    nr=n_elements(parts)
	    outdir=parts[nr-1]
	    
    ENDIF 

    IF file_exist(outdir) THEN BEGIN
    	IF link NE 'none' THEN BEGIN
	    spawn,'/bin/rm -f '+link
	    wait,5
	    IF file_exist(link) THEN message,'Error in telimgdir.'
	    ; Bad things happen if link is created inside yyyymmdd directory!
	    cmd='ln -s '+outdir+' '+link
	    print,cmd
	    spawn,cmd,/sh
            spawn,'ls -d '+outdir+'/'+yyyymmdd,lnck,/sh
	    if outdir+'/'+yyyymmdd eq lnck then begin
             	IF ~keyword_set(NOEVENT_NOTIFY)THEN BEGIN
	             openw,evlun,'errmsg.txt',/get_lun
	             printf,evlun,'******* link Error *******'
	             printf,evlun,''
	             printf,evlun,'pipeline=',string(pipeline,'(a8)')
	             printf,evlun,output+'/'+yyyymmdd+' link created'
	             printf,evlun,'deleting '+output+'/'+yyyymmdd+' link'
	             printf,evlun,''
	             close,evlun
	             free_lun,evlun
	             print,''
	             spawn,'cat errmsg.txt',/sh
	             wait,10
	             maillist='secchipipeline@cronus.nrl.navy.mil'
	             spawn,'mailx -s SECCHI-'+string(pipeline,'(a8)')+'_LINKERR_'+yyyymmdd+' '+maillist+' < errmsg.txt',/sh
	       ENDIF
              spawn,'/bin/rm -f '+outdir+'/'+yyyymmdd
	    endif 
	ENDIF
    ENDIF ELSE message,'outdir not found.',/info
end
;
