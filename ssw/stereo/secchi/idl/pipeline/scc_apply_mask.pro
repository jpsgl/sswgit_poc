pro scc_apply_mask, scch,image,fits_hdr
;+
; $Id: scc_apply_mask.pro,v 1.13 2018/03/08 16:50:32 secchia Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_apply_mask
;               
; Purpose   : This pro applies the onboard mask in the pipeline for none-Icer compressed images
;               
; Explanation: The function reads the planning tool mask tables to get
;              the masks for each detector. It then selects the mask
;              used in the image returns resized image and 
;              corrected header naxis[12], p[12]col and p[12]row.
;               
; Use       : IDL>scc_apply_mask, hdr,image,fits_hdr
;    
; Inputs    : hdr -image header, SECCHI structure
;           : image - image
;           : fits_hdr -image header, fits STRARR
;
; Outputs   : modified all three inputs
;
; Keywords  :
;
; Environments: SSW
;
; Calls     : SCC_FITSHDR2STRUCT, GETENV_SLASH, READ_MASK_TABLES
;            
; Restriction:	Must be called BEFORE rectification!!
;              
; Category    : Masking, Missing Blocks
;               
; Prev. Hist. : None.
;
; Written     : started with Robin Colaninno's scc_get_mask, modified to work in pipeline by Lynn Simpson
;               
; $Log: scc_apply_mask.pro,v $
; Revision 1.13  2018/03/08 16:50:32  secchia
; removed stop on line 217
;
; Revision 1.12  2015/07/20 17:19:49  nathan
; change secchipipeline@cronus to mvi@nrl so it does not bounce from ssc
;
; Revision 1.11  2013/11/13 23:15:26  nathan
; correct error in crpix calculation
;
; Revision 1.10  2013/10/21 23:05:25  nathan
; Use IP_CMD instead of tel=HI to determine treatment of mask; accomodate
; binning; not sure how to treat HI*SW function.
;
; Revision 1.9  2011/12/14 23:23:24  nathan
; fix so it works with Rice subfields
;
; Revision 1.8  2011/06/29 19:30:23  nathan
; changed maillist to secchipipeline@cronus
;
; Revision 1.7  2009/12/14 18:49:22  nathan
; corrected Purpose statement
;
; Revision 1.6  2009/03/17 18:18:36  mcnutt
; adds 1 to p1col and p1row if mask x0 or y0 is gt 0
;
; Revision 1.5  2009/03/16 17:20:53  mcnutt
; correct crpix when mask is applied
;
; Revision 1.4  2008/12/12 13:01:42  mcnutt
; removed pipeline keyword to read_mask_tables
;
; Revision 1.3  2008/12/11 20:32:11  mcnutt
; added keyword pipeline to read_mask_tables
;
; Revision 1.2  2008/12/11 20:26:13  mcnutt
; corrected call to read_mask_tables
;
; Revision 1.1  2008/12/11 19:27:26  mcnutt
; applys mask to image and adjusts naxis if not ICER compression
;
;
;-            

common apply_mask, ipdat

stch=scch

num =0 

if datatype(ipdat) EQ 'UND' THEN BEGIN
    ssss = scc_read_ip_dat(IPver=IPver)
    ipdat=replicate('ERROR',256)
    for i=0,n_elements(ssss.ip_description)-1 do ipdat[i]=ssss[i].ip_description
ENDIF

if strmid(ipdat[stch.comprssn],0,4) eq 'ICER' then return ; ICER mask apply on-board

;--Get the filename and spacecraft from the header
mask_table = stch.MASK_TBL
path = GETENV_SLASH('NRL_LIB')
d = get_delim()
tmp = strsplit(mask_table,d)
str = path+'secchi'+d+'data'+d+'tables'+d+strmid(mask_table,tmp(n_elements(tmp)-1),strlen(mask_table)-tmp(n_elements(tmp)-1))
filename = file_search(str,/TEST_REGULAR,count=flag)

IF flag EQ 0 THEN BEGIN
            openw,otlun,'occtblmsg.txt',/get_lun
            printf,otlun,'Mask table has not been added to Solarsoft library'
            printf,otlun,mask_table
            printf,otlun,str+' is Missing'
            close,otlun
	    free_lun,otlun
	    maillist='mvi@nrl.navy.mil'
;      	    maillist='lynn.simpson.ctr@nrl.navy.mil'
	    spawn,'mailx -s Pipeline_Missing_mask_table '+maillist+' < occtblmsg.txt',/sh
    	    spawn,'rm occtblmsg.txt',/sh

    message,'COULD NOT FIND MASK TABLE',/inform
    wait,5
    RETURN
ENDIF
sc = (str_sep(stch.OBSRVTRY,'_'))[1]

;--Read the mask table
READ_MASK_TABLES,filename, roi_mask, occ_mask, sc,/NORECTIFY

;--Find the index for each detector
det = stch.DETECTOR
CASE det OF
    'EUVI': tel=0 
    'COR1': tel=1
    'COR2': tel=2
    'HI1' : tel=3
    'HI2' : tel=4
ELSE: message, 'ONLY SECCHI DETECTORS ALLOWED'
ENDCASE

;--Select the mask from the mask table arrays
CASE 1 OF 
  stregex(stch.IP_00_19,' 27',/boolean): mi=-1
  stregex(stch.IP_00_19,' 25',/boolean): mi=0	
  stregex(stch.IP_00_19,' 45',/boolean): mi=1
  stregex(stch.IP_00_19,' 46',/boolean): mi=2
  stregex(stch.IP_00_19,' 47',/boolean): mi=3
ELSE: RETURN
ENDCASE

IF mi EQ -1 THEN mask = occ_mask[tel,*] ELSE mask = roi_mask[tel,*,mi]

IF stregex(stch.IP_00_19,' 29',/boolean) THEN mask = ~mask 

mask = reform(mask,34,34)
mask = rebin(mask, 2176,2176,/sample)

; determine if mask is before or after IP binning
rois=[' 27',' 25',' 45',' 46',' 47']
troi=rois[mi+1]
proi=strpos(stch.IP_00_19,troi)
psum=strpos(stch.IP_00_19,'  3')    	    	    ; 2x2 pixel sum
IF psum LT 0 THEN psum=strpos(stch.IP_00_19,' 53')  ; 2x2 pixel sum then conditional divide by 4
IF psum LT 0 THEN psum=255  ; ipsum = 1

if psum LT proi $       	    	; binning before ROI
or stregex(stch.IP_00_19,' 37',/boolean) $	; uses HI1 summing buffer
or stregex(stch.IP_00_19,' 38',/boolean) $	; uses HI2 summing buffer
or (stch.sumrow+stch.sumcol GT 2) $ 	    	; onchip summing
then begin
   mask=mask(0:stch.NAXIS1-1, n_elements(mask(*,0)) - stch.NAXIS2: n_elements(mask(*,0))-1 )
   mask=rebin(mask,stch.NAXIS1*stch.ipsum,stch.naxis2*stch.ipsum)
endif else mask = mask[stch.P1COl-1:stch.P2COL-1,stch.P1ROW-1:stch.P2ROW-1]

message,'DOES NOT INCLUDE RECTIFY IMAGE CORRECTION',/inform
help,scch,image
IF scch.RECTIFY EQ 'T' THEN stop

wg=where(mask EQ 0)
gxy=array_indices(mask,wg)
minx=min(gxy[0,*],max=maxx)
miny=min(gxy[1,*],max=maxy)

stch.p1col=minx+1
stch.P2COL=maxx+1
stch.p1row=miny+1
stch.p2row=maxy+1

;--indicate imaging area. ASSUMED TO BE UNRECTIFED! These are also corrected for summing
;  in secchi_rectify.pro.
;  naxis1
stch.dstart1	=(51-stch.p1col+1)>1
stch.dstop1	=stch.dstart1-1+((stch.p2col-stch.p1col+1)<2048)
;  naxis2
stch.dstart2	=1
stch.dstop2	=stch.dstart2-1+((stch.p2row-stch.p1row+1)<2048)

stch.crpix1=scch.crpix1-(stch.p1col-scch.p1col)/(2^(stch.summed-1))
stch.crpix2=scch.crpix2-(stch.p1row-scch.p1row)/(2^(stch.summed-1))
stch.crpix1a=stch.crpix1
stch.crpix2a=stch.crpix2

stch.naxis1 	=(stch.p2col - stch.p1col + 1)/(2^(stch.summed-1))
stch.naxis2 	=(stch.p2row - stch.p1row + 1)/(2^(stch.summed-1))

IF datatype(fits_hdr) EQ 'STR' THEN BEGIN
    fxaddpar,fits_hdr,'NAXIS1',stch.naxis1
    fxaddpar,fits_hdr,'NAXIS2',stch.naxis2
    fxaddpar,fits_hdr,'P1COL',stch.P1COL
    fxaddpar,fits_hdr,'P2COL',stch.P2COL
    fxaddpar,fits_hdr,'P1ROW',stch.P1ROW
    fxaddpar,fits_hdr,'P2ROW',stch.P2ROW
    fxaddpar,fits_hdr,'DSTART1',stch.dstart1
    fxaddpar,fits_hdr,'DSTART2',stch.dstart2
    fxaddpar,fits_hdr,'DSTOP1',stch.dstop1
    fxaddpar,fits_hdr,'DSTOP2',stch.dstop2
    fxaddpar,fits_hdr,'CRPIX1',stch.crpix1
    fxaddpar,fits_hdr,'CRPIX2',stch.crpix2
    fxaddpar,fits_hdr,'CRPIX1A',stch.crpix1
    fxaddpar,fits_hdr,'CRPIX2A',stch.crpix2
ENDIF

tpcol=(stch.p1col-scch.p1col)/(2^(stch.summed-1))
tprow=(stch.p1row-scch.p1row)/(2^(stch.summed-1))

image=image[tpcol:tpcol+stch.naxis1-1, tprow:tprow+stch.naxis2-1]


scch=stch   

END
