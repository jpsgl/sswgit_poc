pro sccwritesummary, scch, sumfile, NOLOCK=nolock
;+
; $Id: sccwritesummary.pro,v 1.16 2007/08/30 16:36:31 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : sccwritesummary
;               
; Purpose     : format image info and write to summary file
;               
; Explanation : 
;               
; Use         : IDL> sccwritesummary, scch, file
;    
; Inputs      : scch	SECCHI_FITS_hdr_structure
;   	    	file	Name of summary file to write to
;               
; Outputs     : 
;
; Keywords :	/NOLOCK     Do not apply lock (for file rewrite)
;               
; Calls from LASCO : 
;
; Common      : 
;               
; Restrictions: For lock to work, $SCC_ATTIC must be common location
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Jan 2007
;               
; Modified    :
;
; $Log: sccwritesummary.pro,v $
; Revision 1.16  2007/08/30 16:36:31  nathan
; change location of lockfile
;
; Revision 1.15  2007/04/12 19:37:21  nathan
; utilize scc_read_ip_dat.pro
;
; Revision 1.14  2007/04/10 15:39:07  nathan
; add NMISSING on right
;
; Revision 1.13  2007/04/05 22:18:59  nathan
; /NOLOG keyword
;
; Revision 1.12  2007/04/03 20:55:38  mcnutt
; BUG 119 FPS_ON eq 1 in summary file corrected
;
; Revision 1.11  2007/03/28 14:12:26  mcnutt
; BUG 101 added LED colum to img and seq summary files
;
; Revision 1.10  2007/03/19 16:57:47  secchia
; corrected typo
;
; Revision 1.9  2007/03/19 16:47:58  mcnutt
; change compression in summary file to match $SCC_DATA/ops/tables/current/ipcodes.h
;
; Revision 1.8  2007/03/19 15:33:02  mcnutt
; corrected compression values
;
; Revision 1.7  2007/03/12 20:02:33  mcnutt
; Bug 101 added compression to summary file
;
; Revision 1.6  2007/03/12 16:50:34  nathan
; implement apply_lock (bug 107)
;
; Revision 1.5  2007/03/06 11:17:49  nathan
; fix typo which cuased incorrect delimiter after filename
;
; Revision 1.4  2007/03/01 22:42:28  nathan
; variable filename format to allow for longer filenames
;
; Revision 1.3  2007/02/05 16:54:01  nathan
; variable format for COR polar
;
; Revision 1.2  2007/01/16 23:37:28  nathan
; fix fw string
;
; Revision 1.1  2007/01/09 18:14:49  nathan
; use sccwritesummary to write to summary file
;

lusummary=10

img_dir='img'
IF strpos(sumfile,'.cal') GT 0 THEN img_dir='cal'
IF strpos(sumfile,'.seq') GT 0 THEN img_dir='seq'
IF strpos(sumfile,'.pol') GT 0 THEN img_dir='pol'

isfile=findfile (sumfile)
dateObs=strmid(scch.date_obs,0,4)+'/'+strmid(scch.date_obs,5,2)+'/'+strmid(scch.date_obs,8,2)+' '+strmid(scch.date_obs,11,8)
dest=scch.DOWNLINK
if(dest eq 'SWX ') then dest='SW  '
IF scch.polar LT 1000 THEN pf='(f5.1)' ELSE pf='(i5)'
case trim(scch.seb_prog) of
  'NORMAL'   : sebp='Norm'
  'DARK'     : sebp='Dark'
  'DOUBLE'   : sebp='Doub'
  'LED'      : sebp='Led '
  'CONTIN'   : sebp='Cont'
  'SERIES'   : sebp='Sequ'
  else       : sebp=scch.seb_prog
endcase
;fps_on get changed in fitshead2struct called in secchi_reduce before scc_icerdiv2.pro
case trim(scch.fps_on) of
  'T'     : scchfps='ON '
  'F'     : scchfps='OFF'
  '1'     : scchfps='ON '     
  '0'     : scchfps='OFF'
  else    : scchfps=scch.fps_on
endcase
case trim(scch.filter) of
  'OPEN'    : scchfilt='OPEN'
  'S1'	    : scchfilt='S1'
  'S2'	    : scchfilt='S2'
  'DBL'     : scchfilt='DBL'
  else	    : scchfilt='None'
endcase
case trim(scch.detector) of
  'HI1'     : scchpol='None'
  'HI2'     : scchpol='None'
  'COR1'    : scchpol=string(format=pf,scch.polar)
  'COR2'    : scchpol=string(format=pf,scch.polar)
  'EUVI'    : scchpol=string(format='(i3)',scch.wavelnth)
  else      : scchpol='None'
endcase
if (scch.detector eq "HI1" or scch.detector eq "HI2" or scch.detector eq "COR1") then begin
  case trim(scch.ledcolor) of
    'RED'     : scchled='RedFPA'
    'BLUE'     : scchled='BluFPA'
    'PURPLE'    : scchled='PrpFPA'
    'NONE'    : scchled='None'
    else      : scchled='None'
   endcase
 endif
 if (scch.detector eq "COR2") then begin
  case trim(scch.ledcolor) of
    'RED'     : scchled='RedTel'
    'BLUE'     : scchled='BluFPA'
    'PURPLE'    : scchled='PrpFPA'
    'NONE'    : scchled='None'
    else      : scchled='None'
   endcase
 endif
if (scch.detector eq "EUVI") then begin
  case trim(scch.ledcolor) of
    'RED'     : scchled='RedFPA'
    'BLUE'     : scchled='BluTel'
    'PURPLE'    : scchled='PrpTel'
    'NONE'    : scchled='None'
    else      : scchled='None'
  endcase
endif

    ssss = scc_read_ip_dat(IPver=IPver)
    ipdat=replicate('ERROR',256)
    for i=0,n_elements(ssss.ip_description)-1 do ipdat[i]=ssss[i].ip_description

   z=where(strmid(ipdat,0,6) eq 'H-comp')
   ipcmprs=ipdat
   ipcmprs(z)='HC'+strmid(ipdat(z),strlen(ipdat(z(0)))-2,1)
   ipcmprs(where(ipdat eq 'No Compression')) = 'NONE'
   ipcmprs(where(ipdat eq 'Rice Compression')) = 'RICE'
   ipcmprs(where(ipdat eq 'Header Only')) = 'HDRO'



obsid=scch.obs_id
;  if(scch.obs_id eq 65535)then obsid=scch.imgctr
flen=strlen(scch.filename)
IF ~keyword_set(NOLOCK) THEN BEGIN
    ;--Check for sumfile lock
    break_file,sumfile,dlog,dir,root,ext
    lockfile=concat_dir(getenv('SCC_SUM'),root+ext+'.lock')
    WHILE check_lock(lockfile) NE 1 DO wait,2
    t0=systime(1)
    apply_lock,lockfile,expire=20,/nochmod
    print,'Apply_lock took',systime(1)-t0,' sec.'
    t1=systime(1)
    ;--End sumfile lock
ENDIF
IF flen EQ 25 THEN fnfm="(a25,' | '" ELSE fnfm="(a26,'| '"
openw,lusummary,sumfile,/append

if(isfile eq "")then begin
  printf,lusummary,'       FileName                  DateObs          Tel  Exptime Xsize  Ysize  Filter  Polar   Prog   OSnum   Dest   FPS    LED     CMPRS   NMISS' 
  printf,lusummary,'==============================================================================================================================================='
endif
fmt=  fnfm+   ",a19,     ' | ',a4,' |',f6.1,'| ',i4,' | ',i4,' | ',a5,' | ',a5,' | ',a4,' |',i5,' | ',a4,' | ',a3,' | ',a6,' | ',a6,' | ',i4)"
printf,lusummary,format=fmt,scch.filename,dateObs,scch.detector,scch.exptime,scch.NAXIS1,$
      scch.NAXIS2,scchfilt,scchpol,sebp,obsid,dest,scchfps,scchled,ipcmprs[scch.comprssn],scch.nmissing

;stoph
close,lusummary
;free_lun,lusummary        ; not necessary with specified lun
IF ~keyword_set(NOLOCK) THEN BEGIN
    rm_lock,lockfile
    print,'Lock applied for ',systime(1)-t1,' seconds'
ENDIF
end
