pro updatescchdr,sc,d1,d2,COR1=cor1,COR2=cor2,EUVI=euvi,HI1=hi1,HI2=hi2,icernmiss=icernmiss, $ 
    writedb=writedb,rrowcol=rrowcol,cmdoffse=cmdoffse, bug255=bug255, bug232=bug232, NOGT=nogt, $
    bug222=bug222,adjsumtimes=adjsumtimes,POINTING=pointing,himask=himask, dooreuvi=dooreuvi, $
    hkdb=hkdb, INPUT=input, FAST=fast, BLANK_CORRECT=blank_correct, FIX_HISTORY=fix_history, ATT_UPDATE=att_update,$
    dotzero=dotzero, DIV2=div2, DSTOP=dstop, HIDSTOP=hidstop, _EXTRA=_extra
;+
; $Id: updatescchdr.pro,v 1.78 2014/12/10 19:10:10 hutting Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : updatescchdr
;               
; Purpose   : Revise headers of already processed FITS files
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :  updatescchdr,'a','2006-10-26','2007-03-01',/cor1,/cor2,/icermiss
;             	-OR-
;   	    	updatescchdr, INPUT=list_of_FITS_files, .... (All in list must be from 1 spacecraft)
; Outputs   : 
;
; Keywords  :     COR1=cor1,COR2=cor2,EUVI=euvi,HI1=hi1,HI2=hi2
;   	    	/WRITEDB    Update values in database as well as FITS header (Not done by default)
;   	    	/icernmiss - corrects ICER NMISSING to correct reprocessed images 2006 all telescopes and HI 2007 Jan-March
;   	                  and processed images 2007/09/01 - 2007/09/27 all tels
;   	    	/POINTING   Redo all attitude keywords (replaces /pointeuvi and /c1point
;   	    	/NOGT	    Use SPICE values only for pointing keywords (/POINTING must also be set)
;   	    	INPUT=	    list_of_FITS_files  to update
;   	    	... and misc. specific correction keywords
;
; Calls from LASCO : 
;
; Common    : SCC_REDUCE, dbms
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline (see http://durance.oamp.fr/lasco/fichiers/soft_categories.html)
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2004
;               
; $Log: updatescchdr.pro,v $
; Revision 1.78  2014/12/10 19:10:10  hutting
; added hidstop correction
;
; Revision 1.77  2013/10/28 18:06:23  nathan
; fix DSTOP1 and 2
;
; Revision 1.76  2012/12/04 18:36:44  nathan
; change $SECCHI to destdir, depending on input not environment
;
; Revision 1.75  2012/03/20 21:54:32  secchib
; change order of printing
;
; Revision 1.74  2012/03/20 20:24:53  secchib
; nr - forgot to add ephemfil, crln_obs, crlt_obs
;
; Revision 1.73  2012/03/20 18:04:55  mcnutt
; added fxhmodiy call for to correct sc_roll and sc_rolla
;
; Revision 1.72  2012/03/16 20:06:20  nathan
; if /pointing, always redo ephemeris keywords also
;
; Revision 1.70  2011/10/18 18:35:30  secchia
; checks ccd temp before updating hkdb
;
; Revision 1.69  2011/08/30 21:56:01  secchib
; nr - make sure div2corr is set
;
; Revision 1.68  2011/08/16 18:10:47  secchib
; added cor1 and cor2 to div2 corrections
;
; Revision 1.67  2011/08/16 16:17:59  secchib
; commented out fitupdated if statement line 1142 not define else where in pro
;
; Revision 1.66  2011/08/16 12:05:54  mcnutt
; added DIV2 correction
;
; Revision 1.65  2011/03/23 14:47:23  secchia
; corrected dotzero to not add history to uncorrected fits
;
; Revision 1.64  2011/03/22 20:42:27  mcnutt
; added dotzero keyword
;
; Revision 1.63  2011/01/28 13:22:11  secchib
; determins directory and sends ot to touchdatelink if link
;
; Revision 1.62  2011/01/14 14:14:37  secchib
; added touchdatelink call
;
; Revision 1.61  2011/01/13 13:55:56  secchib
; change to ATT_UPDATE remove sd file if no corrections where made
;
; Revision 1.60  2011/01/13 13:01:39  mcnutt
; added ATT_UPDATE to correct fits files with missing attitude files
;
; Revision 1.59  2010/12/02 13:01:11  secchia
; defines cmtun if no nogt
;
; Revision 1.58  2010/07/01 19:15:36  nathan
; if sccreadfits outputs -1, skip and go to next one
;
; Revision 1.57  2010/07/01 18:17:54  secchia
; nr - always init db tables because was hanging on themis
;
; Revision 1.56  2010/06/30 20:44:22  nathan
; added knames check
;
; Revision 1.55  2010/06/30 20:16:45  nathan
; Add /FIX_HISTORY; add feature to record keyword in history; fix bug with
; multiple days in INPUT'
;
; Revision 1.54  2010/06/30 17:25:30  secchib
; nr - delflag undefined
;
; Revision 1.53  2010/06/30 17:21:20  nathan
; always do HISTORY, DATE and img_hist_cmnt update at end of loop; require /WRITEDB for any other db update
;
; Revision 1.52  2010/06/30 16:01:04  nathan
; update no-keyword check
;
; Revision 1.51  2010/06/28 19:51:02  nathan
; add /BLANK_CORRECT
;
; Revision 1.50  2010/06/01 18:14:55  nathan
; remove islz var
;
; Revision 1.49  2010/05/27 21:04:02  nathan
; Replaced /pointeuvi and /c1point with /POINTING to do all/any reprocessing
; of attitude values; added /NOGT to do pointing with SPICE only; allow
; alternate input of INPUT=list of FITS files
;
; Revision 1.48  2009/11/09 14:34:57  secchia
; as run for EUVI-A hkdb
;
; Revision 1.47  2009/11/09 14:10:31  mcnutt
; changed ceb database call to aorblc
;
; Revision 1.46  2009/11/09 14:08:05  mcnutt
; fix line 831
;
; Revision 1.45  2009/11/09 14:06:21  mcnutt
; added sc to call since scc_platform is no longer defined
;
; Revision 1.44  2009/11/09 13:54:05  mcnutt
; removed aorb=sc for hkdb update
;
; Revision 1.43  2009/11/09 13:50:43  mcnutt
; added hkdb to update fits with missing temp and table data
;
; Revision 1.42  2009/08/27 17:27:43  secchia
; corrected write db and summary logic
;
; Revision 1.41  2009/07/08 13:32:18  mcnutt
; added euvi door correction
;
; Revision 1.40  2009/05/06 16:24:59  nathan
; updated DOORSTAT logic and comments to correctly handle EUVI disabled door
;
; Revision 1.39  2009/04/14 19:13:46  mcnutt
; update header ATT file for euvipoint and changed euvipoit to pointeuvi
;
; Revision 1.38  2009/03/17 18:21:27  mcnutt
; corrects p1col in hi mask
;
; Revision 1.37  2009/03/16 17:29:33  mcnutt
; himask change crpix1a also
;
; Revision 1.36  2009/03/16 17:21:53  mcnutt
; added himask correction
;
; Revision 1.35  2009/02/04 17:10:31  mcnutt
; corrected exposure corrrection to COR2 adjsumtime
;
; Revision 1.34  2009/02/04 17:06:30  mcnutt
; added exposure corrrection to COR2 adjsumtime
;
; Revision 1.33  2009/02/02 19:05:14  mcnutt
; added pointeuvi correction
;
; Revision 1.32  2009/01/22 16:18:08  secchib
; removed delete lines from timeadj
;
; Revision 1.31  2009/01/21 16:10:00  secchib
; set delflag for cor2 timeadj
;
; Revision 1.30  2009/01/21 15:21:51  mcnutt
; corrects db for cor2 time adjust without using attic image files
;
; Revision 1.29  2009/01/15 17:42:16  mcnutt
; removed typo
;
; Revision 1.28  2009/01/15 17:39:06  mcnutt
; added comments to the dates in header when changed
;
; Revision 1.27  2009/01/12 19:10:02  mcnutt
; deletes cor2 database entries before change date_obs and date_cmnd
;
; Revision 1.26  2009/01/08 15:25:26  mcnutt
; added adjsumtimes keyword for cor2 summed image correction of date-obs,cmd, and avg
;
; Revision 1.25  2009/01/08 14:39:28  mcnutt
; date_fname_utc to date_cmd to complete v1.293 mods
;
; Revision 1.24  2008/08/28 16:51:45  mcnutt
; changed Bug222 to write DB
;
; Revision 1.23  2008/08/28 16:41:46  mcnutt
; added COR2 doulbe polar correction bug222
;
; Revision 1.22  2008/07/14 18:33:46  nathan
; added inline comments
;
; Revision 1.21  2008/03/20 11:13:15  secchia
; change call to unblock_img_hdrs to one input
;
; Revision 1.20  2008/03/11 12:11:37  mcnutt
; added bug232 COR2 CDELT1/2 correction
;
; Revision 1.19  2008/02/14 12:55:57  mcnutt
; corrected history comment for cor1_point
;
; Revision 1.18  2008/02/13 18:21:51  mcnutt
; corrected cor1point option
;
; Revision 1.17  2008/02/12 15:44:19  mcnutt
; added cor1point to correct precomissioning cor1 headers
;
; Revision 1.16  2007/11/27 15:58:19  mcnutt
; Bug 255 commented out lines to subtract 1 from dstop(12)
;
; Revision 1.15  2007/11/20 15:47:44  mcnutt
; aonther correction to bug255
;
; Revision 1.14  2007/11/20 12:47:39  mcnutt
; corrected bug 255 for rectify rotation
;
; Revision 1.13  2007/11/19 18:06:59  secchib
; corrected change for Bug 255
;
; Revision 1.12  2007/11/19 16:31:01  mcnutt
; added Bug255 correction dstart and dstop for 2176 binned images
;
; Revision 1.11  2007/11/13 19:03:56  secchib
; added cmdoffse to correction type check
;
; Revision 1.10  2007/11/13 18:57:55  mcnutt
; added CMDOFFSE Bug 256 to correct EUVI headers only
;
; Revision 1.9  2007/11/02 17:01:39  mcnutt
; added RC to dbroot if keyword rrowcol
;
; Revision 1.8  2007/11/02 17:00:18  mcnutt
; added r[12]row and r[12]col correction bug 248
;
; Revision 1.7  2007/10/16 17:16:02  secchib
; change or the and for not keyword check
;
; Revision 1.6  2007/10/12 16:42:12  secchib
; added check for image files before reading in fits files
;
; Revision 1.5  2007/10/12 15:11:09  secchib
; added writedb keyword
;
; Revision 1.4  2007/10/04 17:05:59  mcnutt
; added ICER NMISSING correction bug 248
;
; Revision 1.3  2007/06/22 21:51:41  nathan
; removed stuff about readout tables and do cdelt1a change for euvi
;
; Revision 1.2  2007/01/04 15:58:33  nathan
; rev on 12/20/06
;
; Revision 1.1  2006/12/16 00:32:34  nathan
; progress...
;
;
common scc_reduce,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, resetvars, $
    	    	    ipdat, nom_tlr, prevbasename, readfileidline, wavefileidline, $
		    setupfileidline, exposfileidline, maskfileidline, ipfileidline, icerdiv2flag, $
		    rotblidlines,hbtimes,hbtemps,cebtimes,cebtemps, subdir, date_fname_utc, ispb, islz, isrt, aorb, $
		    iserror, ephemfil, att_file
common dbms,ludb, dbfile, delflag, img_seb_hdr, img_seb_hdr_ext, img_cal_bias, $
	    img_cal_led, img_cal_dark, img_hist_cmnt, img_euvi_gt    	

idline='$Id: updatescchdr.pro,v 1.78 2014/12/10 19:10:10 hutting Exp $'
idinfo=strmid(idline,5,34)
longwait=10
shortwait=2
attcorr=0
IF keyword_set(INPUT) THEN BEGIN
    ;list=input
    jk=sccreadfits(input[0],h0,/nodata)
    ud1=anytim2utc(h0.date_obs)
    nf=n_elements(input)
    jk=sccreadfits(input[nf-1],hn,/nodata)
    ud2=anytim2utc(hn.date_obs)
    aorb=strmid(h0.obsrvtry,7,1)

ENDIF ELSE BEGIN    
    ud1=anytim2utc(d1)
    IF n_params() GT 2 THEN ud2=anytim2utc(d2) ELSE ud2=ud1
    ;aorb=getenv("SCC_PLATFORM")   
    if strpos(strupcase(sc),'A')+ strpos(strupcase(sc),'B') eq -2 then begin
      print,'MUST enter SC'
      return
    endif 
    aorb=strupcase(sc)
    tels=['none']
    IF keyword_set(COR1) THEN tels=[tels,'cor1']
    IF keyword_set(COR2) THEN tels=[tels,'cor2']
    IF keyword_set(EUVI) THEN tels=[tels,'euvi']
    IF keyword_set(HI1) THEN tels=[tels,'hi_1']
    IF keyword_set(HI2) THEN tels=[tels,'hi_2']
    IF keyword_set(pointeuvi) then tels=[tels,'euvi']
    IF keyword_set(dooreuvi) then tels=[tels,'euvi']
    IF keyword_set(himaks) then tels=[tels,'hi_1']
    nt=n_elements(tels)-1
    IF nt LT 1 and ~keyword_set(bug255)THEN BEGIN
	    message,'Must specify one or more telescopes. Returning.',/info
	    return
    ENDIF
    tels=tels(1:n_elements(tels)-1)

ENDELSE

    aorblc=strlowcase(aorb)
    IF aorb EQ 'A' THEN BEGIN
    ; these are used in call to sccdopolar
    	aheadf=1
	behindf=0
	scname='ahead'
    ENDIF ELSE BEGIN
    	aheadf=0
	behindf=1
	scname='behind'
    ENDELSE

;defines compression
    ssss = scc_read_ip_dat(IPver=IPver)
    ipdat=replicate('ERROR',256)
    for i=0,n_elements(ssss.ip_description)-1 do ipdat[i]=ssss[i].ip_description

   z=where(strmid(ipdat,0,6) eq 'H-comp')
   ipcmprs=ipdat
   ipcmprs(z)='HC'+strmid(ipdat(z),strlen(ipdat(z(0)))-2,1)
   ipcmprs(where(ipdat eq 'No Compression')) = 'NONE'
   ipcmprs(where(ipdat eq 'Rice Compression')) = 'RICE'
   ipcmprs(where(ipdat eq 'Header Only')) = 'HDRO'

ud=ud1

knames=['ICERNMISS','WRITEDB','RROWCOL','CMDOFFSE','BUG255','POINTING','BUG232','BUG222','ADSUMTIMES','HIMASK','DOOREUVI','HKDB','BLANK_CORRECT','FIX_HISTORY','ATT_UPDATE','DOTZERO','DIV2', 'DSTOP', 'HIDSTOP']
; this is for history
iskset=[keyword_set(icernmiss) , keyword_set(writedb) , keyword_set(rrowcol) , keyword_set(cmdoffse) , keyword_set(bug255) , keyword_set(POINTING) , keyword_set(bug232) , $
    	keyword_set(bug222) , keyword_set(adjsumtimes) , keyword_set(himask) , keyword_set(dooreuvi) , keyword_set(hkdb) , keyword_set(BLANK_CORRECT) , keyword_set(FIX_HISTORY), keyword_set(ATT_UPDATE),$
	keyword_set(DOTZERO), keyword_set(DIV2), keyword_set(DSTOP), keyword_set(HIDSTOP)]
if total(iskset) LT 1 then begin 
	message,'Must specify fits header correction keyword',/info
	return
ENDIF
if n_elements(knames) ne n_elements(iskset) THEN message,'Error: knames and iskset must be same size.'
idinfo=idinfo+' '+arr2str(knames(where(iskset)))

IF ud1.mjd GT ud2.mjd THEN incr=-1 ELSE incr=1
FOR mjd=ud1.mjd,ud2.mjd,incr DO BEGIN
      ud.mjd=mjd
      yyyymmdd=utc2yymmdd(ud,/yyyy)
      isc='*'
      destdir=getenv('secchi')+'/lz/L0/'+aorblc
    IF keyword_set(INPUT) THEN BEGIN
    ; extract from input those files of current date being processed
    	wd=where(strpos(input,yyyymmdd) ge 0,nwd)
	IF nwd GT 0 THEN list=input[wd] ELSE list=''
    ENDIF ELSE BEGIN
	  if keyword_set(rrowcol) then isc='cal'
	  if ~keyword_set(bug255) and ~keyword_set(bug232) then begin
            for i=0,n_Elements(tels)-1 do begin
        	fitsfiles=file_search(destdir+'/'+isc+'/'+tels(i)+'/'+yyyymmdd+'/*.fts')
        	if i eq 0 then list=fitsfiles else list=[list,fitsfiles]
            endfor
	  endif
	   if keyword_set(bug232) then begin ;cor2 only
            destdir='/net/earth/secchi/P0/'
            for i=0,n_Elements(tels)-1 do begin
        	fitsfiles=file_search(destdir+strlowcase(aorb)+'/cor2/'+yyyymmdd+'/*.fts')
        	if i eq 0 then list=fitsfiles else list=[list,fitsfiles]
            endfor
	  endif
	   if keyword_set(bug222) then begin ;cor2 only
            for i=0,n_Elements(tels)-1 do begin
        	isc='img'
        	fitsfiles=file_search(destdir+'/'+isc+'/cor2/'+yyyymmdd+'/*.fts')
        	if i eq 0 then list=fitsfiles else list=[list,fitsfiles]
            endfor
	  endif
	   if keyword_set(adjsumtimes) then begin ;cor2 only
             isc=['img','seq']
             for i=0,n_Elements(isc)-1 do begin
        	fitsfiles=file_search(destdir+'/'+isc(i)+'/cor2/'+yyyymmdd+'/*.fts')
        	if i eq 0 then list=fitsfiles else list=[list,fitsfiles]
            endfor
	  endif


	  if keyword_set(bug255) then begin
            list=''
            gdate=strmid(ymd2date(yyyymmdd),0,11)
            imglist=SCC_READ_SUMMARY(DATE=gdate,spacecraft=getenv('SC'))
            tm=[where(imglist.xsize eq 1088),where(imglist.ysize eq 1088)]
            tmz=where(tm ge 0)
            if tmz(0) gt -1 then begin
               tm=tm(tmz)
               for ntm=0,n_Elements(tm)-1 do begin
        	 if imglist(tm(ntm)).telescope eq 'HI1' then teles='hi_1' else $
	     	    if imglist(tm(ntm)).telescope eq 'HI2' then teles='hi_2' else $
		    teles=strlowcase(imglist(tm(ntm)).telescope)
   	            list=[list, destdir+'/' +imglist(tm(ntm)).type +'/' +teles+'/' +strmid(imglist(tm(ntm)).filename,0,8) +'/' +imglist(tm(ntm)).filename]
               endfor
            endif else list=''
	  endif

	  if keyword_set(hidstop) then begin
            list=''
            gdate=strmid(ymd2date(yyyymmdd),0,11)
            imglist=SCC_READ_SUMMARY(DATE=gdate,spacecraft=getenv('SC'))
            tm=[where(imglist.xsize gt 2048),where(imglist.ysize gt 2048)]
            tmz=where(tm ge 0)
            if tmz(0) gt -1 then begin
               tm=tm(tmz)
               for ntm=0,n_Elements(tm)-1 do begin
        	 if imglist(tm(ntm)).telescope eq 'HI1' then teles='hi_1' else $
	     	    if imglist(tm(ntm)).telescope eq 'HI2' then teles='hi_2' else $
		    teles=strlowcase(imglist(tm(ntm)).telescope)
   	            list=[list, destdir+'/' +imglist(tm(ntm)).type +'/' +teles+'/' +strmid(imglist(tm(ntm)).filename,0,8) +'/' +imglist(tm(ntm)).filename]
               endfor
            endif else list=''
	  endif
 
    ENDELSE   ; INPUT=0
      if keyword_set(hkdb) then begin
   	    euvitemps='HBEUVIAFTSHTRT,HBEUVIZONET,HBEUVIAFTMNTT,HBEUVISECMIRT,HBEUVIENTRT,HBEUVIFWDMNTT'
    	    cor1temps='HBCOR1ZONE2T,HBCOR1DOUB2T,HBCOR1POLDOUB1T,HBCOR1TUBEOCCT,HBCOR1ZONE1T'
    	    cor2temps='HBCOR2ZONE3T,HBCOR2RLYLNST,HBCOR2FLDLNST,HBCOR2HRMRRT,HBCOR2ZONE2T,HBCOR2ZONE1T'
    	    hitemps='HBHIBACKSTRT,HBHIFINT,HBHIZONE1T,HBHIZONE2T,HBHIFRNTSTRT,HBHIBASESTRT'
    	    ccdtemps='HBEUVICCDT,HBCOR1CCDT,HBCOR2CCDT,HBHI1CCDT,HBHI2CCDT'
    	    cebenctemps='HBSCIPCEBENCLT,HBHICEBENCLT'
    	    teltemp=euvitemps+','+cor1temps+','+cor2temps+','+hitemps+','+ccdtemps+','+cebenctemps
    	    stemps=get_hbtemps(aorblc,teltemp,yyyymmdd,res=0.1)
            if(datatype(stemps) ne 'STR')then begin
       	      tags=tag_names(stemps) & ntags=n_Elements(tags)
              timez=strmid(stemps.pckt_time,0,4)+strmid(stemps.pckt_time,5,2)+strmid(stemps.pckt_time,8,2)+'_'$
	    	+strmid(stemps.pckt_time,11,2)+strmid(stemps.pckt_time,14,2)+strmid(stemps.pckt_time,17,2)
              hbtimes=yymmdd2utc(timez) 
    	      hbtemps=fltarr(ntags-2,n_elements(stemps.(0)))
    	      for n=0,ntags-3 do hbtemps(n,*)=stemps.(n+2)
            endif
    	    ictemps=get_ictemps(aorblc,yyyymmdd,res=0.1)
            if(datatype(ictemps) ne 'STR')then begin
       	      tags=tag_names(ictemps) & ntags=n_Elements(tags)
              timez=strmid(ictemps.pckt_time,0,4)+strmid(ictemps.pckt_time,5,2)+strmid(ictemps.pckt_time,8,2)+'_'$
	    	+strmid(ictemps.pckt_time,11,2)+strmid(ictemps.pckt_time,14,2)+strmid(ictemps.pckt_time,17,2)
              cebtimes=yymmdd2utc(timez) 
    	      cebtemps=fltarr(ntags-2,n_elements(ictemps.(0)))
    	      for n=0,ntags-3 do cebtemps(n,*)=ictemps.(n+2)
            endif
            IF datatype(hbtimes) EQ 'UND' THEN list=''
      endif ; /HKDB
help,yyyymmdd,list
wait,2
      imgcheck=where(list ne '')
      if (imgcheck(0) gt -1) then begin
 	fitsfiles=list(where(list ne ''))
        pipeline ='uh'
        dbroot=yyyymmdd
        if keyword_set(icernmiss) then dbroot=dbroot+'IN' 
        if keyword_set(writedb) then dbroot=dbroot+'WD' 
        if keyword_set(rrowcol) then dbroot=dbroot+'RC' 
        if keyword_set(cmdoffse) then dbroot=dbroot+'OFSE' 
        if keyword_set(bug255) then dbroot=dbroot+'bug255' 
        if keyword_set(bug222) then dbroot=dbroot+'bug222' 
        if keyword_set(adjsumtimes) then dbroot=dbroot+'timeadj' 
        if keyword_set(pointeuvi) then dbroot=dbroot+'euvipnt' 
         if keyword_set(dooreuvi) then dbroot=dbroot+'euvidr' 
       if keyword_set(himask) then dbroot=dbroot+'himsk' 
       if keyword_set(hkdb) then dbroot=dbroot+'hkdb' 
    	IF keyword_set(POINTING) THEN dbroot=dbroot+'pointing'
    	IF keyword_set(BLANK_CORRECT) THEN dbroot=dbroot+'blank'
    	IF keyword_set(DOTZERO) THEN dbroot=dbroot+'DOTZERO'
        attcorr=0
    	IF keyword_set(ATT_UPDATE) THEN begin
	    dbroot=dbroot+'attupdate'
            attsc=scname+'_20'
        endif
        lulog=9
        sccattic=getenv('SCC_ATTIC')+'/'+strmid(yyyymmdd,2,6)+'/'
;        unblock_cmd = concat_dir(getenv('UTIL_DIR'),'unblockSciFile')
;open log file need for write db_script
    	logfile= concat_dir(getenv('SCC_LOG'),'scc'+getenv('SC')+pipeline+dbroot+'.log')
	openw,lulog,logfile,/append
;	printf,lulog,version
	printf,lulog,' '
	;text=readlist('sebhdr_struct.inc')
	text=[';*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO', $
	      ';*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Thu May 19 15:11:05 2005']
	printf,lulog,'NOTE: sebhdr info is static -- need to generate from sebhdr4_struct.inc'
	printf,lulog,text[0]
	printf,lulog,text[1]
	printf,lulog,'starting log file at '+systime()
	printf,lulog,' '
	printf,lulog,'SEB_IMG	',getenv('seb_img')
	printf,lulog,'SCI_SOURCE ',getenv('SCI_SOURCE')
	for i=0,n_Elements(tels)-1 do printf,lulog,'SECCHI	',getenv('SECCHI')+'/*/'+tels(i)+'/'+yyyymmdd
	printf,lulog,'SCC_ATTIC ',sccattic
;	printf,lulog,unblock_cmd
;	printf,lulog,uncompr_cmd,' or '
;	printf,lulog,huncompr_cmd,' or '
;	printf,lulog,iuncompr_cmd

	printf,lulog,'IDL_PATH	',getenv('IDL_PATH')
	printf,lulog,'TABLES	',getenv('TABLES')
	printf,lulog,'PT    	',getenv('PT')
	printf,lulog,'DATE: ',yyyymmdd
	printf,lulog,'==============================='


    	nf=n_elements(fitsfiles)

	get_utc,ustarttime
	starttime=utc2str(ustarttime,/ecs)


	; always need to add HISTORY row
        @./secchi_img_tables_struct.inc
        SPAWN,'hostname',machine,/sh  
         machine = machine(N_ELEMENTS(machine)-1)
        src=strmid(machine,0,1)+rstrmid(machine,0,1)
	dbfile=concat_dir(getenv('SCC_DB'),'scc'+getenv('SC')+pipeline+src+dbroot+'sql.script')
	openw,ludb,dbfile,/get_lun
	dbfstat=fstat(ludb)
	IF dbfstat.size EQ 0 THEN BEGIN
	; only at beginning of each script file
	    printf,ludb,'use secchi_flight;'
	    printf,ludb,'start transaction;'
	ENDIF
	printf,ludb,'# starting dbfile at '+starttime
	delflag=0
allreadydeleted=0
  FOR i=0,nf-1 DO BEGIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   print,fitsfiles[i]
    IF keyword_set(ATT_UPDATE) THEN  begin
       img=sccreadfits(fitsfiles[i],scch,/nodata)
       if strpos(scch.ATT_FILE,attsc) ne -1 then goto,skipcorrect else attcorr=1
    endif
    img=sccreadfits(fitsfiles[i],scch);,/nodata)
    IF img[0] LT 0 THEN BEGIN
    	message,'File not found; skipping and then continuing.',/info
	wait,2
	continue
    ENDIF
    IF keyword_set(ATT_UPDATE) THEN  if strpos(scch.ATT_FILE,attsc) ne -1 then goto,skipcorrect else attcorr=1
    get_utc,date_mod,/ccsds,/noz
    cmt='Updated '+date_mod
     do_db_sum=0   
     if keyword_set(icernmiss) then begin 
;    20070927_142615_n4euB.fts: NMISSING=  0 /out of 32 segments  DATE    = '2007-10-01T19:20:11.416' (first good euvi image)
;    20070927_104518_s4c1B.fts: NMISSING= -1 /out of 32 segments  DATE    = '2007-10-01T19:21:55.765' (last bad cor1 image)
;    20070927_105243_s4c2B.fts: NMISSING= -1 /out of 32 segments  DATE    = '2007-10-01T19:22:44.586' (last bad cor2 image)
;
        date_start='2007-09-19T10:00:00'
    	case trim(strlowcase(scch.detector)) of
    	    'euvi' : date_stop='2007-10-01T19:20:11'
    	    'cor1' : date_stop='2007-10-01T19:21:55'
    	    'cor2' : date_stop='2007-10-01T19:22:44'
    	    else   : date_stop='2007-10-01T19:20:00' 
    	endcase
	if (strpos(ipcmprs(scch.COMPRSSN),'ICER') ne -1 and scch.date lt date_stop and scch.date gt date_start)then begin
             printf,lulog,'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
    	      printf,lulog,scch.filename,'  ',scch.nmissing,'  ',scch.date,'  ',date_mod
              print,'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
    	      print,scch.filename,' ',scch.nmissing,' ',scch.date,' ',date_mod
    	      scch.NMISSING=scch.NMISSING+1
	      fxhmodify,fitsfiles[i],'NMISSING',scch.NMISSING,' updated '+date_mod
              if keyword_set(writedb) then do_db_sum=1
        endif
    endif   ; /ICERNMISS

     if keyword_set(rrowcol) and scch.r1row eq 0 then begin 
	      get_utc,date_mod,/ccsds,/noz
              printf,lulog,'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
    	      printf,lulog,scch.filename,'  ',scch.r1row,'  ',scch.date,'  ',date_mod
        stch=scch
        stch.rectify = 'F'
        stch.r1row=scch.p1row
        stch.r2row=scch.p2row
        stch.r1col=scch.p1col
        stch.r2col=scch.p2col
        ;stch.naxis1=scch.naxis1 & stch.naxis2=scch.naxis2
	stch.rectrota=0
	rotcmt='no rotation'
	xden=(2^(scch.ipsum+scch.sumcol-2))
	yden=(2^(scch.ipsum+scch.sumrow-2))
	stch.dstart1=fix(stch.dstart1/xden)>1
	stch.dstart2=fix(stch.dstart2/yden)>1
	stch.dstop1 =fix(stch.dstop1/xden)
	stch.dstop2 =fix(stch.dstop2/yden)
	IF xden GT 1 and stch.dstart1 GT 1 THEN stch.dstop1=stch.dstop1-1
	IF yden GT 1 and stch.dstart2 GT 1 THEN stch.dstop2=stch.dstop2-1

	if(stch.NAXIS1 gt 0 and stch.NAXIS2 gt 0)then begin
	  WCS = FITSHEAD2WCS(stch)
	  ; For RA/Dec, we need to specify system=A
	  XYCEN = WCS_GET_COORD(WCS, [(stch.NAXIS1-1.)/2., (stch.NAXIS2-1.)/2.])
	  stch.XCEN=XYCEN[0]
	  stch.YCEN=XYCEN[1]
	endif

	fxhmodify,fitsfiles[i],'NAXIS1',	stch.naxis1
	fxhmodify,fitsfiles[i],'NAXIS2',	stch.naxis2
	fxhmodify,fitsfiles[i],'R1COL',	stch.r1col
	fxhmodify,fitsfiles[i],'R2COL',	stch.r2col
	fxhmodify,fitsfiles[i],'R1ROW',	stch.r1row
	fxhmodify,fitsfiles[i],'R2ROW',	stch.r2row
	fxhmodify,fitsfiles[i],'SUMROW',	stch.sumrow
	fxhmodify,fitsfiles[i],'SUMCOL',	stch.sumcol
	fxhmodify,fitsfiles[i],'RECTIFY',	stch.rectify
	fxhmodify,fitsfiles[i],'CRPIX1',	stch.crpix1
	fxhmodify,fitsfiles[i],'CRPIX2',	stch.crpix2
	fxhmodify,fitsfiles[i],'XCEN',	stch.xcen
	fxhmodify,fitsfiles[i],'YCEN',	stch.ycen
	fxhmodify,fitsfiles[i],'CRPIX1A',	stch.crpix1
	fxhmodify,fitsfiles[i],'CRPIX2A',	stch.crpix2
	fxhmodify,fitsfiles[i],'DSTART1', stch.dstart1
	fxhmodify,fitsfiles[i],'DSTART2', stch.dstart2
	fxhmodify,fitsfiles[i],'DSTOP1',	stch.dstop1
	fxhmodify,fitsfiles[i],'DSTOP2',	stch.dstop2
;        fxhmodify,fitsfiles[i],'HISTORY',	histinfo
        fxhmodify,fitsfiles[i],'RECTROTA',	stch.rectrota, rotcmt
        if keyword_set(writedb) then do_db_sum=1
    endif   ; /RROWCOL

     if keyword_set(cmdoffse) then begin 
        unblock_image_hdrs,sccattic,scch.fileorig ;       spawn, unblock_cmd+' '+infile,/sh, unblockresult
        scch.CMDOFFSE   =nom_hdr.lightTravelOffsetTime/1000.
	fxhmodify,fitsfiles[i],'CMDOFFSE',scch.CMDOFFSE,BEFORE='READPORT'
        do_db_sum=0 ; database not changed for this update
    endif   ; /CMDOFFSE


     if (keyword_set(bug255) and scch.summed gt 1) or keyword_set(hidstop) then begin 
         stch=scch

	     if (stch.rectrota eq 0) then begin;
    	    	stch.dstart1	=(51-stch.p1col+1)>1
    	    	stch.dstop1	=stch.dstart1-1+((stch.p2col-stch.p1col+1)<2048)
    	    	;  naxis2
    	    	stch.dstart2	=1
    	    	stch.dstop2	=stch.dstart2-1+((stch.p2row-stch.p1row+1)<2048)
    	     endif
	     if (stch.rectrota eq 6) then begin;--indicate imaging area - rotate 6
		;  naxis1
		stch.dstart1	=(129-stch.r1col+1)>1
		stch.dstop1	=stch.dstart1-1+((stch.r2col-stch.r1col+1)<2048)
		;  naxis2
		stch.dstart2	=(79-stch.r1row+1)>1
		stch.dstop2	=stch.dstart2-1+((stch.r2row-stch.r1row+1)<2048)
             endif
  	     if (stch.rectrota eq 1) then begin;--indicate imaging area - rotate 1
		;  naxis1
		stch.dstart1	=(129-stch.r1col+1)>1
		stch.dstop1	=stch.dstart1-1+((stch.r2col-stch.r1col+1)<2048)
		;  naxis2
		stch.dstart2	=(51-stch.r1row+1)>1
		stch.dstop2	=stch.dstart2-1+((stch.r2row-stch.r1row+1)<2048)
             endif
  	     if (stch.rectrota eq 3) then begin;--indicate imaging area - rotate 3
		;  naxis1
		stch.dstart1	=1
		stch.dstop1	=stch.dstart1-1+((stch.r2col-stch.r1col+1)<2048)
		;  naxis2
		stch.dstart2	=(79-stch.r1row+1)>1
		stch.dstop2	=stch.dstart2-1+((stch.r2row-stch.r1row+1)<2048)
             endif
  	     if (stch.rectrota eq 2) then begin;--indicate imaging area - rotate 2
		;  naxis1
		stch.dstart1	=(79-stch.r1col+1)>1
		stch.dstop1	=stch.dstart1-1+((stch.r2col-stch.r1col+1)<2048)
		;  naxis2
		stch.dstart2	=(129-stch.r1row+1)>1
		stch.dstop2	=stch.dstart2-1+((stch.r2row-stch.r1row+1)<2048)
             endif
         xden=(2^(scch.ipsum+scch.sumcol-2))
         yden=(2^(scch.ipsum+scch.sumrow-2))
         stch.dstart1=fix(ceil(float(stch.dstart1)/xden))>1
         stch.dstart2=fix(ceil(float(stch.dstart2)/yden))>1
         stch.dstop1 =fix(float(stch.dstop1)/xden)
         stch.dstop2 =fix(float(stch.dstop2)/yden)
;         IF xden GT 1 and stch.dstart1 GT 1 THEN stch.dstop1=stch.dstop1-1
;         IF yden GT 1 and stch.dstart2 GT 1 THEN stch.dstop2=stch.dstop2-1
	fxhmodify,fitsfiles[i],'DSTART1', stch.dstart1
	fxhmodify,fitsfiles[i],'DSTART2', stch.dstart2
	fxhmodify,fitsfiles[i],'DSTOP1',	stch.dstop1
	fxhmodify,fitsfiles[i],'DSTOP2',	stch.dstop2
        do_db_sum=0
    endif   ; /BUG255

    IF keyword_set(POINTING) or keyword_set(ATT_UPDATE) THEN BEGIN
        do_db_sum=0 ; only CROTA in db
	scch0=scch
	date_obs_utc=anytim2utc(scch.DATE_CMD, /nocorrect)

     	time0=systime(1)
	
	;IF keyword_set(NOGT) THEN BEGIN
	    IF strmid(scch.detector,0,2) EQ 'HI' THEN BEGIN
		    pointingtime=scch.date_end
		    hpct=', using DATE-END'
		    scch.CTYPE1 ='HPLN-AZP'
		    scch.CTYPE2 ='HPLT-AZP'
		    scch.CTYPE1A='RA---AZP'
		    scch.CTYPE2A='DEC--AZP'
		    scch.CUNIT1='deg'
		    scch.CUNIT2='deg'
		    divisor=1.
		    cmtun='deg '
	    ENDIF ELSE BEGIN
		    pointingtime=scch.date_obs
		    hpct=', using DATE-OBS'
		    scch.CUNIT1='arcsec'
		    scch.CUNIT2='arcsec'
		    divisor=3600.   ; for conversion from arcsec to degrees for SCIP RA-DEC values
		    cmtun='arcs '
	    ENDELSE

	    crpix=getscccrpix(scch,/UPDATE, FAST=fast, _EXTRA=_extra)
	    print,'CRPIX: ',crpix
	    print,'Computing ephemeris info from SPICE...'

	    adndm=''
	    atf=get_stereo_att_file(pointingtime,aorb)
    	    EPHEMFIL    =get_stereo_spice_kernel(pointingtime,aorb)
	    IF atf EQ '' THEN att_file='NotFound' ELSE att_file=atf
	    help,att_file
	    print,''
	    IF keyword_set(ENDBADAH) THEN endbadaht=utc2str(anytim2utc(endbadah),/noz) ELSE endbadaht='2000/01/01'
	    IF att_file EQ 'NotFound' and (pointingtime LT endbadaht) THEN wait,1 
	    IF att_file EQ 'NotFound' and (pointingtime GT endbadaht) THEN BEGIN 
		  print,'Waiting 100....'
		  wait,100
		  load_stereo_spice,/reload,/verbose
		  adndm=' STEREO SPICE reloaded'
		  atf=get_stereo_spice_kernel(pointingtime,aorb,/ATTITUDE) 
		  IF atf NE '' THEN att_file=atf
    	    	  EPHEMFIL    =get_stereo_spice_kernel(pointingtime,aorb)
	    ENDIF 
	    
	    ;print,'Calling get_stereo_lonlat ...'
	    hee_d=get_stereo_lonlat(pointingtime,strmid(scch.obsrvtry,7,1), /meters, system='HEE',_extra=_extra)
	    ;print,'Calling get_stereo_lonlat ...'
	    heeq=get_stereo_lonlat(pointingtime,strmid(scch.obsrvtry,7,1), /degrees, system='HEEQ',_extra=_extra)
	    ;print,'Calling get_stereo_coord ...'
	    coord=get_stereo_coord(pointingtime,strmid(scch.obsrvtry,7,1) , /meters ,_extra=_extra,ltime=sctime)
	    ;print,'Calling get_stereo_coord ...'
	    E_coord=get_stereo_coord(pointingtime,'Earth', /meters ,_extra=_extra,ltime=etime)
	    ;printf,lulog,'Duration of 6 get_stereo calls ='+string(spicetime)
	    scch.RSUN=(6.95508e8 * 648d3 / !dpi ) / hee_d(0)
	    scch.HCIX_OBS=coord(0)
	    scch.HCIY_OBS=coord(1)
	    scch.HCIZ_OBS=coord(2)
	    scch.DSUN_OBS=hee_d(0)
	    scch.HGLN_OBS=heeq(1)
	    scch.HGLT_OBS=heeq(2)
	    scch.EAR_TIME=etime-sctime
	    scch.SUN_TIME=sctime

	    carlonlat=get_stereo_lonlat(pointingtime,strmid(scch.obsrvtry,7,1),/au, /degrees, system='Carrington')
	    scch.CRLN_OBS=carlonlat(1)
	    IF scch.crln_obs LT 0 THEN scch.crln_obs = scch.crln_obs + 360
	    scch.CRLT_OBS=carlonlat(2)
    
	    ;print,'Calling get_stereo_hpc_point ...'
	    coord=get_stereo_coord(pointingtime,strmid(scch.obsrvtry,7,1) , /meters , system='HAE')
		scch.HAEX_OBS=coord(0)
		scch.HAEY_OBS=coord(1)
		scch.HAEZ_OBS=coord(2)
	    ;print,'Calling get_stereo_hpc_point ...'
	    coord=get_stereo_coord(pointingtime,strmid(scch.obsrvtry,7,1) , /meters , system='HEE')
		scch.HEEX_OBS=coord(0)
		scch.HEEY_OBS=coord(1)
		scch.HEEZ_OBS=coord(2)
	    ;print,'Calling get_stereo_hpc_point ...'
	    coord=get_stereo_coord(pointingtime,strmid(scch.obsrvtry,7,1) , /meters , system='HEEQ')
		scch.HEQX_OBS=coord(0)
		scch.HEQY_OBS=coord(1)
		scch.HEQZ_OBS=coord(2)
		scch.EPHEMFIL=ephemfil		; in common scc_reduce

	    IF scch0.dsun_obs NE scch.dsun_obs THEN BEGIN
	    	message,'Updating ephemeris keywords.',/info
		fxhmodify,fitsfiles[i],'rsun',scch.rsun
		fxhmodify,fitsfiles[i],'hcix_obs',scch.hcix_obs
		fxhmodify,fitsfiles[i],'hciy_obs',scch.hciy_obs
		fxhmodify,fitsfiles[i],'hciz_obs',scch.hciz_obs
		fxhmodify,fitsfiles[i],'dsun_obs',scch.dsun_obs
		fxhmodify,fitsfiles[i],'hgln_obs',scch.hgln_obs
		fxhmodify,fitsfiles[i],'hglt_obs',scch.hglt_obs
		fxhmodify,fitsfiles[i],'ear_time',scch.ear_time
		fxhmodify,fitsfiles[i],'sun_time',scch.sun_time
		fxhmodify,fitsfiles[i],'haex_obs',scch.haex_obs
		fxhmodify,fitsfiles[i],'haey_obs',scch.haey_obs
		fxhmodify,fitsfiles[i],'haez_obs',scch.haez_obs
		fxhmodify,fitsfiles[i],'heex_obs',scch.heex_obs
		fxhmodify,fitsfiles[i],'heey_obs',scch.heey_obs
		fxhmodify,fitsfiles[i],'heez_obs',scch.heez_obs
		fxhmodify,fitsfiles[i],'heqx_obs',scch.heqx_obs
		fxhmodify,fitsfiles[i],'heqy_obs',scch.heqy_obs
		fxhmodify,fitsfiles[i],'heqz_obs',scch.heqz_obs
		fxhmodify,fitsfiles[i],'ephemfil',scch.ephemfil
		fxhmodify,fitsfiles[i],'crln_obs',scch.crln_obs
		fxhmodify,fitsfiles[i],'crlt_obs',scch.crlt_obs
	    ENDIF
    
	    ;--Get pointing in helioprojective cartesian coordinates
	    yprhpc=getsccpointing(scch,TOLERANCE=60, YPRSC=scp, FAST=fast, FOUND=ff, _EXTRA=_extra)
	    IF (scch.detector EQ 'COR2') THEN $
		  hpccmnt='Pointing info from SPICE, 1min. resolution'
	    print,'YPR hpc = ',yprhpc
	    IF ~keyword_set(FAST) THEN wait,2

	    ;--Get pointing in R.A.-Dec coordinates
	    yprrd=getsccpointing(scch,/RADEC, TOLERANCE=60, YPRSC=scpa, FAST=fast, FOUND=ff, _EXTRA=_extra)
	    help,ff
	    IF ~keyword_set(FAST) THEN wait,2
	    scch.ATT_FILE=att_file		; in common scc_reduce

	    IF (scch.detector ne 'EUVI') THEN BEGIN
	      scch.CRVAL1	=yprhpc[0]
	      scch.CRVAL2	=yprhpc[1]
	    ENDIF	
	    ; for EUVI contained in CRPIX and CRVAL=0

	    scch.CROTA	=yprhpc[2]
	    dr=!DPI/180.d
	    scch.PC1_1= cos(yprhpc[2]*dr)
	    scch.PC1_2=-sin(yprhpc[2]*dr) ;*(scch.CDELT1/scch.CDELT2) ;may be added later
	    scch.PC2_1= sin(yprhpc[2]*dr) ;*(scch.CDELT1/scch.CDELT2)
	    scch.PC2_2= cos(yprhpc[2]*dr)
	    scch.SC_YAW		=scp[0]
	    scch.SC_PITCH	=scp[1]
	    scch.SC_ROLL	=scp[2]

	    print,'YPR Ra,Dec = ',yprrd
	    IF ~keyword_set(FAST) THEN wait,2
	    ;IF (scch.detector ne 'EUVI') THEN BEGIN
	    scch.CRVAL1A = yprrd[0]
	    scch.CRVAL2A = yprrd[1]
	    ;ENDIF

	    scch.PC1_1A =  cos(yprrd[2]*dr)
	    scch.PC1_2A = -sin(yprrd[2]*dr) * scch.CDELT2A / scch.CDELT1A
	    scch.PC2_1A =  sin(yprrd[2]*dr) * scch.CDELT1A / scch.CDELT2A
	    scch.PC2_2A =  cos(yprrd[2]*dr)
	    scch.SC_YAWA	=scpa[0]
	    scch.SC_PITA	=scpa[1]
	    scch.SC_ROLLA	=scpa[2]

;
	    ; Update header with nominal pointing information
	    ; - this pro bases calculations on scch.SC_YAW/PITCH/ROLL
	    ;
    	    
	    IF (scch.detector EQ 'HI1') or (scch.detector EQ 'HI2') THEN BEGIN
	    	IF scch.naxis1/float(scch.naxis2) NE 1. THEN img=scc_img_trim(intarr(scch.naxis1,scch.naxis2),scch)
		; remove overscan if any because otherwise hi_point does not work
		hi_calib_point,scch,/hi_nominal
	    ENDIF
	    ;
	    ; Update xcen and ycen keywords
	    ;
	    if (scch.NAXIS1 gt 0 and scch.NAXIS2 gt 0)then begin
	      WCS = FITSHEAD2WCS(scch)
	      ; For RA/Dec, we need to specify system=A
	      XYCEN = WCS_GET_COORD(WCS, [(scch.NAXIS1-1.)/2., (scch.NAXIS2-1.)/2.])
	      scch.XCEN=XYCEN[0]
	      scch.YCEN=XYCEN[1]
	    endif
	
	IF ~keyword_set(NOGT) THEN BEGIN
	    IF date_obs_utc.mjd LT ustarttime.mjd-45 THEN $
	    sdspath=getenv('SDS_FIN_DIR')+'/'+aorblc+'/'+strmid(scch.DATE_CMD,0,4)+'/'+strmid(scch.DATE_CMD,5,2) ELSE $
	    sdspath=getenv('SDS_DIR')+'/'+scname+'/data_products/level_0_telemetry/secchi'
	    
	    IF (scch.detector EQ 'EUVI') THEN BEGIN
    		euvi_point,scch,ERROR=gterr
		crv1c=''
		crv2c=''
		crv1ac=' before GT was '+trim(crval1a)
		print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(scch0.crval1A)
		printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(scch0.crval1A)
		crv2ac=' before GT was '+trim(crval2a)
		print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(scch0.crval2A)
		printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(scch0.crval2A)
		crp1c=' before GT was '+trim(crpix1)
		print,'CRPIX1= '+trim(scch.CRPIX1)+', before GT was '+trim(scch0.CRPIX1)
		printf,lulog,scch.date_obs+',CRPIX1=,'+trim(scch.CRPIX1)+',before GT was,'+trim(scch0.CRPIX1)
		crp2c=' before GT was '+trim(crpix2)
		print,'CRPIX2= '+trim(scch.CRPIX2)+', before GT was '+trim(scch0.CRPIX2)
		printf,lulog,scch.date_obs+',CRPIX2=,'+trim(scch.CRPIX2)+',before GT was,'+trim(scch0.CRPIX2)
	    ENDIF

    	    IF (scch.detector EQ 'HI1' or scch.detector EQ 'HI2') THEN BEGIN
    	    	exthdr=mrdfits(fitsfiles[i],1)
    	    	scch = hi_point_v2(scch,exthdr,dir=sdspath)
	    ENDIF
	    IF scch.detector EQ 'COR1' THEN cor1_point,scch,dir=sdspath,ERROR=gterr
    	    IF scch.detector EQ 'COR2' THEN cor2_point,scch,dir=sdspath,ERROR=gterr

	    pointingspicetime=systime(1)-time0
	    help,pointingspicetime
	    printf,lulog,'pointing SPICE took'+string(pointingspicetime)

    	    IF (scch.detector ne 'EUVI') THEN BEGIN
		printf,lulog,'GT tlm dir='+sdspath
		crv1c=''
		print,' CRVAL1='+trim(scch.crval1)+', before GT was '+trim(scch0.crval1)
		printf,lulog,scch.date_obs+',CRVAL1=,'+trim(scch.crval1)+',before GT was,'+trim(scch0.crval1)
		crv2c=''
		print,' CRVAL2='+trim(scch.crval2)+', before GT was '+trim(scch0.crval2)
		printf,lulog,scch.date_obs+',CRVAL2=,'+trim(scch.crval2)+',before GT was,'+trim(scch0.crval2)
		crv1ac=''
		print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(scch0.crval1A)
		printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(scch0.crval1A)
		crv2ac=''
		print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(scch0.crval2A)
		printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(scch0.crval2A)
		crp1c=''
		crp2c=''
    	    ENDIF
            if scch.CUNIT1 eq 'deg' then cmtun='deg ' else cmtun='arcs '

	ENDIF     ; not /NOGT

	crp1c=cmt
	crp2c=cmt
	crv1c=cmt
	crv2c=cmt
	
	IF scch0.crpix1 NE scch.crpix1 and scch0.r1col EQ scch.r1col and scch0.r2row EQ scch.r2row THEN BEGIN
	    fxhmodify,fitsfiles[i],'CRPIX1',scch.crpix1,crp1c
	    fxhmodify,fitsfiles[i],'CRPIX2',scch.crpix2,crp2c
	    fxhmodify,fitsfiles[i],'CRPIX1A',scch.crpix1a
	    fxhmodify,fitsfiles[i],'CRPIX2A',scch.crpix2a
	ENDIF
	IF scch0.crval1 NE scch.crval1 or $
	   scch0.att_file NE scch.att_file THEN BEGIN
	    fxhmodify,fitsfiles[i],'CRVAL1',scch.crval1,crv1c
	    fxhmodify,fitsfiles[i],'CRVAL2',scch.crval2,crv2c
	    fxhmodify,fitsfiles[i],'XCEN',scch.xcen
	    fxhmodify,fitsfiles[i],'YCEN',scch.ycen
	    fxhmodify,fitsfiles[i],'CRVAL1A',scch.crval1a
	    fxhmodify,fitsfiles[i],'CRVAL2A',scch.crval2a
	    fxhmodify,fitsfiles[i],'ATT_FILE',scch.att_file
	ENDIF
	IF scch0.cdelt2 NE scch.cdelt2 THEN BEGIN
	    fxhmodify,fitsfiles[i],'CDELT1A',scch.cdelt1a
	    fxhmodify,fitsfiles[i],'CDELT2A',scch.cdelt2a
	    fxhmodify,fitsfiles[i],'CDELT1',scch.cdelt1
	    fxhmodify,fitsfiles[i],'CDELT2',scch.cdelt2
	ENDIF
	IF scch0.ins_x0 NE scch.ins_x0 THEN fxhmodify,fitsfiles[i],'INS_X0',scch.ins_x0,cmtun+cmt
	IF scch0.ins_y0 NE scch.ins_y0 THEN fxhmodify,fitsfiles[i],'INS_Y0',scch.ins_y0,cmtun+cmt
	IF scch0.ins_r0 NE scch.ins_r0 THEN fxhmodify,fitsfiles[i],'INS_R0',scch.ins_r0,cmt
	IF scch0.sc_roll NE scch.sc_roll THEN fxhmodify,fitsfiles[i],'SC_ROLL',scch.sc_roll
	IF scch0.sc_yaw NE scch.sc_yaw THEN fxhmodify,fitsfiles[i],'SC_YAW',scch.sc_yaw,cmtun+cmt
	IF scch0.sc_pitch NE scch.sc_pitch THEN fxhmodify,fitsfiles[i],'SC_PITCH',scch.sc_pitch,cmtun+cmt
	IF scch0.sc_yawa NE scch.sc_yawa THEN fxhmodify,fitsfiles[i],'SC_YAWA',scch.sc_yawa,cmt
	IF scch0.sc_pita NE scch.sc_pita THEN fxhmodify,fitsfiles[i],'SC_PITA',scch.sc_pita,cmt
	IF scch0.sc_rolla NE scch.sc_rolla THEN fxhmodify,fitsfiles[i],'SC_ROLLA',scch.sc_rolla

        tmpzind=where(strpos(scch.history,'_point') gt -1)
	ind=n_elements(tmpzind)-1
	hinfo=scch.history[tmpzind(ind)]
	;fxhmodify,fitsfiles[i],'HISTORY',hinfo
    	IF scch0.crota NE scch.crota or $
	   scch0.pc1_2 NE scch.pc1_2 THEN BEGIN
	    fxhmodify,fitsfiles[i],'CROTA',scch.crota
	    fxhmodify,fitsfiles[i],'PC1_1',scch.pc1_1
	    fxhmodify,fitsfiles[i],'PC1_2',scch.pc1_2
	    fxhmodify,fitsfiles[i],'PC2_1',scch.pc2_1
	    fxhmodify,fitsfiles[i],'PC2_2',scch.pc2_2
	    fxhmodify,fitsfiles[i],'PC1_1A',scch.pc1_1a
	    fxhmodify,fitsfiles[i],'PC1_2A',scch.pc1_2a
	    fxhmodify,fitsfiles[i],'PC2_1A',scch.pc2_1a
	    fxhmodify,fitsfiles[i],'PC2_2A',scch.pc2_2a
    	    printf,ludb,'update img_seb_hdr_ext set CROTA="'+trim(scch.CROTA)+'" where date_obs="'+scch.date_obs+'" and fileorig="'+scch.fileorig+'";' 
    	    printf,ludb,'update img_seb_hdr_ext set DATE_MOD="'+date_mod+'" where date_obs="'+scch.date_obs+'" and fileorig="'+scch.fileorig+'";' 
	endif else print,fitsfiles[i],': CROTA Correct not changing'
	
          delflag=0
    ENDIF   ;/POINTING

    IF keyword_set(bug232) THEN BEGIN
        scch.CDELT1 = 14.7*2^(scch.SUMMED-1)
        scch.CDELT2 = 14.7*2^(scch.SUMMED-1)
	fxhmodify,fitsfiles[i],'CDELT1',scch.CDELT1
	fxhmodify,fitsfiles[i],'CDELT2',scch.CDELT2
        do_db_sum=0
    ENDIF   ; /BUG232

    IF keyword_set(bug222) and scch.seb_prog eq 'DOUBLE'  and scch.POLAR ne 1001 THEN BEGIN
        scch.POLAR = 1001
	fxhmodify,fitsfiles[i],'POLAR',scch.POLAR
        if keyword_set(writedb) then do_db_sum=1
    ENDIF   ; /BUG222

    IF keyword_set(adjsumtimes) and scch.extend eq 'T' THEN BEGIN
        ; read in extended header
        exthd=mrdfits(fitsfiles[i],1)
        orig_date_obs=scch.date_obs
        if utc2tai(str2utc(scch.date_end)) - utc2tai(str2utc(scch.date_obs)) lt exthd(n_Elements(exthd)-1).DELTATIME then begin
          openw,ludb,dbfile,/append,/get_lun
    	  ;printf,ludb,'delete from img_seb_hdr_ext where date_obs = "'+orig_date_obs+'" and fileorig = "'+scch.fileorig+'";' 
    	  ;printf,ludb,'delete from img_hist_cmnt where date_obs = "'+orig_date_obs+'" and filename = "'+scch.filename+'";' 
	  ;printf,ludb,'delete from img_seb_hdr where date_obs = "'+orig_date_obs+'" and fileorig = "'+scch.fileorig+'" and filename = "'+scch.filename+'";'
          allreadydeleted=1
    	  nd_obs=utc2tai(str2utc(scch.date_obs))-round(exthd(n_Elements(exthd)-1).DELTATIME)
    	  nd_cmd=utc2tai(str2utc(scch.date_cmd))-round(exthd(n_Elements(exthd)-1).DELTATIME)
          od_end=utc2tai(str2utc(scch.date_end))
          nd_avg=nd_obs+(od_end-nd_obs)/2.
          scch.date_obs=utc2str(tai2utc(nd_obs))
          scch.date_cmd=utc2str(tai2utc(nd_cmd))
	  scch.date_avg=utc2str(tai2utc(nd_avg))
          cmmnt='updated '+date_mod
       	  fxhmodify,fitsfiles[i],'DATE-OBS',scch.date_obs,cmmnt
       	  fxhmodify,fitsfiles[i],'DATE-CMD',scch.date_cmd,cmmnt
	  fxhmodify,fitsfiles[i],'DATE-AVG',scch.date_avg,cmmnt
          if scch.exptime lt total(exthd.exptime) or scch.exptime lt n_Elements(exthd)*max(exthd.exptime) then begin
             cmmnt='updated '+date_mod
             z=where(exthd.exptime gt 0)
	     if n_elements(z) eq n_Elements(exthd) then scch.exptime=total(exthd.exptime) else begin
                scch.exptime=(total(exthd(z).exptime)/n_Elements(z))*n_Elements(exthd) 
		cmmnt='ESTIMATED -- some HDR0 files missing '+cmmnt
	     endelse
	     printf,ludb,'update img_seb_hdr set exptime="'+scch.exptime+'" where date_obs="'+orig_date_obs+'" and filename="'+scch.filename+'" and fileorig="'+scch.fileorig+'";' 
       	     fxhmodify,fitsfiles[i],'EXPTIME',scch.exptime,cmmnt
	  endif
	  printf,ludb,'update img_seb_hdr set date_cmd="'+scch.date_cmd+'" where date_obs="'+orig_date_obs+'" and filename="'+scch.filename+'" and fileorig="'+scch.fileorig+'";' 
 	  printf,ludb,'update img_seb_hdr set date_avg="'+scch.date_avg+'" where date_obs="'+orig_date_obs+'" and filename="'+scch.filename+'" and fileorig="'+scch.fileorig+'";' 
	  printf,ludb,'update img_seb_hdr set date_obs="'+scch.date_obs+'" where date_obs="'+orig_date_obs+'" and filename="'+scch.filename+'" and fileorig="'+scch.fileorig+'";' 
	  printf,ludb,'update img_seb_hdr_ext set date_obs="'+scch.date_obs+'" where date_obs="'+orig_date_obs+'" and fileorig="'+scch.fileorig+'";' 
 	  printf,ludb,'update img_hist_cmnt set date_obs="'+scch.date_obs+'" where date_obs = "'+orig_date_obs+'" and filename="'+scch.filename+'";' 
          delflag=0

	  ;close,ludb
	  ;free_lun,ludb
          do_db_sum=2

	endif else print,fitsfiles[i],': times are correct, no changes made'
    ENDIF   ; /ADJSUMTIMES

    IF keyword_set(dooreuvi) THEN BEGIN
       if scch.doorstat eq 3 then begin
          cmmnt='updated '+date_mod
          drcmnt='OPEN'
          scch.doorstat=2
          fxhmodify,fitsfiles[i],'DOORSTAT',scch.doorstat,drcmnt
          if keyword_set(writedb) then do_db_sum=1
	endif
    ENDIF   ; /DOOREUVI

    IF keyword_set(himask) THEN BEGIN
        do_db_sum=0
	scchx=scch
        crpix=getscccrpix(scchx,/UPDATE, FAST=fast, _EXTRA=_extra)
;        if scchx.crpix1 ne scch.crpix1 then begin
        if scchx.p1col eq 1330 then begin
           fxaddpar,fits_hdr,'P1COL',1331
           fxaddpar,fits_hdr,'CRPIX1',scchx.crpix1
           fxaddpar,fits_hdr,'CRPIX2',scchx.crpix2
           fxaddpar,fits_hdr,'CRPIX1A',scchx.crpix1
           fxaddpar,fits_hdr,'CRPIX2A',scchx.crpix2;
           WCS = FITSHEAD2WCS(scchx)
           XYCEN = WCS_GET_COORD(WCS, [(stch.NAXIS1-1.)/2., (stch.NAXIS2-1.)/2.])
           scchx.XCEN=XYCEN[0]
           scchx.YCEN=XYCEN[1]
           fxaddpar,fits_hdr,'XCEN',scchx.XCEN
           fxaddpar,fits_hdr,'YCEN',scchx.YCEN
           if keyword_set(writedb) then do_db_sum=1
        endif
    ENDIF   ; /HIMASK

    IF keyword_set(hkdb) and scch.TEMP_CCD ge 9998.0 THEN BEGIN
      if(scch.detector eq 'EUVI')then tempv=[0,1,2,3,4,5,23]
      if(scch.detector eq 'COR1')then tempv=[6,7,8,8,9,10,24] ;cor1 does not report TEMPMID2
      if(scch.detector eq 'COR2')then tempv=[11,12,13,14,15,16,25] 
      if(scch.detector eq 'HI1' )then tempv=[17,18,19,20,21,22,26]
      if(scch.detector eq 'HI2' )then tempv=[17,18,19,20,21,22,27]
      date_obs_utc=anytim2utc(scch.date_obs) 
      imtime=find_closest(date_obs_utc.(1),hbtimes.(1)) 
      if(imtime gt -1)then begin
        scch.TEMPAFT1=hbtemps(tempv(0),imtime)
        scch.TEMPAFT2=hbtemps(tempv(1),imtime)
        scch.TEMPMID1=hbtemps(tempv(2),imtime)
        if(scch.detector ne 'COR1')then scch.TEMPMID2=hbtemps(tempv(3),imtime)
        scch.TEMPFWD1=hbtemps(tempv(4),imtime)
        scch.TEMPFWD2=hbtemps(tempv(5),imtime)
        scch.TEMP_CCD=hbtemps(tempv(6),imtime)
        if(scch.detector ne 'HI1' and scch.detector ne 'HI2') then $
	    scch.TEMP_CEB=hbtemps(28,imtime) else $
  	    scch.TEMP_CEB=hbtemps(29,imtime)
      endif
      fxhmodify,fitsfiles[i],'TEMPAFT1',	scch.TEMPAFT1
      fxhmodify,fitsfiles[i],'TEMPAFT2',	scch.TEMPAFT2
      fxhmodify,fitsfiles[i],'TEMPMID1',	scch.TEMPMID1
      fxhmodify,fitsfiles[i],'TEMPMID2',	scch.TEMPMID2
      fxhmodify,fitsfiles[i],'TEMPFWD1',	scch.TEMPFWD1
      fxhmodify,fitsfiles[i],'TEMPFWD2',	scch.TEMPFWD2
      fxhmodify,fitsfiles[i],'TEMP_CCD',	scch.TEMP_CCD
      fxhmodify,fitsfiles[i],'TEMP_CEB',	scch.TEMP_CEB

      IF datatype(cebtimes) NE 'UND' THEN BEGIN
        imtimes=where(cebtimes.(1) ge date_obs_utc.(1)-(60*30*1000l) and cebtimes.(1) le  date_obs_utc.(1)+(60*30*1000l))
        if(imtimes(0) gt -1)then begin
        if(scch.detector ne 'HI1' and scch.detector ne 'HI2') then $
	    scch.CEB_T=median(cebtemps(1,imtimes)) else $
  	    scch.CEB_T=median(cebtemps(0,imtimes))
       endif
       fxhmodify,fitsfiles[i],'CEB_T',	scch.CEB_T
      ENDIF
      rotbchangedate='2006-12-08T16:00:01'
      IF (aorb EQ 'B') THEN rotbchangedate='2006-12-07T13:30:57'

      ap1=strmid(scch.fileorig,10,1)
      rowv=[[26,25],[28,27],[32,31],[34,33],[30,29]]
      tids=[35,rowv[0,ap1],rowv[1,ap1],36,38,6]
    ; see $SSW/secchi/idl/database/fswtableids.txt
      tbltagnames=['SETUPTBL', 'READFILE', 'WAVEFILE', 'EXPOSTBL', 'IP_TBL', 'MASK_TBL']
      tbltagindx=tag_index(scch,tbltagnames)
      ntids=n_elements(tids)
      reset=0

      FOR ix=0,ntids-1 DO BEGIN
        tblval=getscctableinfo(aorb,scch.date_obs,tids[ix], RESET=reset, /USE_LOADTIME)
    	reset=0
    	print,tbltagnames[ix],' = ',tblval
    	scch.(tbltagindx[ix])=tblval
    	fxhmodify,fitsfiles[i],tbltagnames[ix],	tblval
      ENDFOR

      ;  use NONE for MASK_TBL if no mask applied: functions 25,27,45,46,47
      xx=where(scch.IP_00_19 EQ 25, nmsk1)
      xx=where(scch.IP_00_19 EQ 27, nmsko)
      xx=where(scch.IP_00_19 EQ 45, nmsk2)
      xx=where(scch.IP_00_19 EQ 46, nmsk3)
      xx=where(scch.IP_00_19 EQ 47, nmsk4)
      IF (nmsk1+nmsko+nmsk2+nmsk3+nmsk4) LE 0 THEN begin 
	scch.mask_tbl='NONE'
        fxhmodify,fitsfiles[i],'MASK_TBL',	scch.mask_tbl
      endif ELSE BEGIN
	msg='IP has applied a mask.'
	printf,lulog,msg
	print,msg
	print
      ENDELSE

      if keyword_set(writedb) then do_db_sum=1
    ENDIF   ; /HKDB
   
    IF keyword_set(dotzero) THEN BEGIN
       do_db_sum=0       
       if strpos(scch.fileorig,'.0') eq 12 then begin
          scch.fileorig = strmid(scch.fileorig,0,12)
	  fxhmodify,fitsfiles[i],'FILEORIG',scch.FILEORIG
    	  printf,ludb,'delete from img_seb_hdr_ext where date_obs = "'+scch.date_obs+'" and fileorig = "'+scch.fileorig+'";' 
	  printf,ludb,'delete from img_seb_hdr where date_obs = "'+scch.date_obs+'" and fileorig = "'+scch.fileorig+'" and filename = "'+scch.filename+'";'
          scch.fileorig = strmid(scch.fileorig,0,12)
	  fxhmodify,fitsfiles[i],'FILEORIG',scch.FILEORIG
	endif else goto,skipcorrect
    ENDIF 

    IF keyword_set(div2) THEN BEGIN
       do_db_sum=0  
       div2corr='F'     
      if(scch.detector eq 'EUVI')then if scch.IP_PROG2 eq 1 and scch.DIV2CORR eq 'F' then DIV2CORR = 'T'
      if(scch.detector eq 'COR1')then if  scch.IP_PROG6 eq 1 and scch.DIV2CORR eq 'F' then DIV2CORR = 'T'
      if(scch.detector eq 'COR2')then if scch.DIV2CORR eq 'F' then DIV2CORR = 'T'
       if div2corr eq 'T' then begin
          scch.DIV2CORR = 'T'
	  fxhmodify,fitsfiles[i],'DIV2CORR',scch.DIV2CORR,'Array was modified by scc_icerdiv2.pro in secchi' 
	endif else goto,skipcorrect
    ENDIF 
    
    IF keyword_set(DSTOP) THEN BEGIN
    	ndstop1=scch.dstop1<scch.naxis1
	ndstop2=scch.dstop2<scch.naxis2
	IF ndstop1 EQ scch.dstop1 and ndstop2 EQ scch.dstop2 THEN goto, skipcorrect 
	IF ndstop1 NE scch.dstop1 THEN BEGIN
	    print,'Changing DSTOP1 to ',ndstop1
	    fxhmodify,fitsfiles[i],'DSTOP1',ndstop1
	ENDIF
	IF ndstop2 NE scch.dstop2 THEN BEGIN
	    print,'Changing DSTOP2 to ',ndstop2
	    fxhmodify,fitsfiles[i],'DSTOP2',ndstop2
	ENDIF
    ENDIF

    if (do_db_sum ge 1)then begin  ; do_db_sum eq 2 then only the summary file gets written.

        img_dir='img'
    	case strmid(scch.filename,16,1) of
    	    'k' : IF (scch.detector NE 'HI1') AND (scch.detector NE 'HI2') THEN img_dir='cal'   ; DARK
      	    'e' : img_dir='cal'   ; LED
      	    'c' : img_dir='cal'   ; CONT
    	 else : 
    	endcase
    	if(trim(strlowcase(scch.detector)) eq 'cor1' or trim(strlowcase(scch.detector)) eq 'cor2')then begin
    	    case strmid(scch.filename,16,1) of
    	    	'n' : img_dir='seq'
    	    	's' : img_dir='seq'
	    	else:
    	    endcase
    	endif

;eDoorStatus : $
;[   'CLOSED',	$
;    'IN_TRANSIT',	$
;    'OPEN ',	$
;    'OFF',	$
;    'ERROR',    $
;    replicate('invalid',250), $
;    'NO_DATA' ]   }
    
    ; SCIP door CLOSED or IN_TRANSIT go in cal; HI door CLOSED (only) go in cal
    ; disabled door results in OFF, which for now we assume is OPEN - nbr, 090506
    IF ((scch.doorstat EQ 1) AND  (scch.detector NE 'HI1') AND (scch.detector NE 'HI2')) OR $
        (scch.doorstat EQ 0)  $
       THEN img_dir='cal' 

    	case trim(strlowcase(scch.detector)) of
    	    'hi1' : BEGIN
	    	 SUBdir=img_dir+'/hi_1'
	     END
    	    'hi2' : BEGIN
	    	    SUBdir=img_dir+'/hi_2'
	    END
    	    else  : SUBdir=img_dir+'/'+trim(strlowcase(scch.detector))
    	endcase

  	    sfx='.'+strmid(scch.filename,18,2)
	    sumfile= concat_dir(getenv('SCC_SUM'), 'scc'+strmid(scch.filename,20,1) $
	    	    	    	    	    	    +strmid(scch.filename,0,6) $
						    +'.'+img_dir+sfx)

        if do_db_sum eq 1 then unblock_image_hdrs,sccattic+scch.fileorig ;       spawn, unblock_cmd+' '+infile,/sh, unblockresult
	if allreadydeleted eq 0 then delflag=1 else delflag=0
        if do_db_sum eq 1 then write_db_scripts, fitsfiles[i]	; deletes old rows from img_seb_hdr, img_hist_cmnt, img_seb_hdr_ext
	if do_db_sum eq 2 then sccwritesummary,scch,sumfile
	
    endif   ; do_db_sum
    
    IF keyword_set(BLANK_CORRECT) THEN fxhmodify,fitsfiles[i],'BLANK',	fix(-32768),cmt

    IF keyword_set(FIX_HISTORY) THEN BEGIN
    	IF scch.extend EQ 'F' THEN BEGIN
	    fitsh=headfits(fitsfiles[i])
	    hist0=fxpar(fitsh,'HISTORY')
	    uh=uniq(hist0)
	    nuh=n_elements(uh)
	    sxdelpar,fitsh,'history'
	    for j=0,nuh-1 do sxaddpar,fitsh,'history',hist0[uh[j]]
	    writefits,fitsfiles[i],img,fitsh
	ENDIF ELSE print,'Not changing HISTORY of file with extended header.'
	wait,0.5
    ENDIF
    	; always change these, i think    
   ; IF fitupdated eq 1 then begin
      fxhmodify,fitsfiles[i],'HISTORY',idinfo
      fxhmodify,fitsfiles[i],'DATE',date_mod
; Always need to add history row to database
	  a=img_hist_cmnt
  	  a.FILENAME  	=scch.filename
  	  a.DATE_OBS  	=scch.date_obs
  	  a.FLAG      	='history'
  	  a.INFO      	=idinfo
          a.DATE_MOD  	=date_mod
	  scc_db_insert,a
    ; endif    	
     skipcorrect:

  ENDFOR    ; each FITS file
endif	; imgcheck

if (datatype(lulog) ne 'UND')then close,lulog
if (datatype(ludb) ne 'UND')then close,ludb
if (datatype(ludb) ne 'UND')then free_lun,ludb
  if keyword_set(ATT_UPDATE) and attcorr eq 0 then begin  ;no corrections made remove db file
     syscmd='/bin/rm -f '+dbfile
     spawn,syscmd
     wait,2
     syscmd='/bin/rm -f '+logfile
     spawn,syscmd
     wait,2
   endif
  if ~keyword_set(ATT_UPDATE) or (keyword_set(ATT_UPDATE) and attcorr eq 1) then begin  ;
     if isc(0) eq '*' then isc=['img','seq','cal']
     for i=0,n_Elements(tels)-1 do begin
	for i2=0,n_Elements(isc)-1 do begin
	    spawn,['ls','-ld',destdir+"/"+isc(i2)+"/"+tels(i)+"/"+yyyymmdd],res,/noshell
	    islink=strpos(res,'->')
	    if islink gt 0 then begin
	       parts=strsplit(res,/extract)
	       nr=n_elements(parts)
	       tellink=parts[nr-1]
               touchdatelink,destdir,isc(i2),tels(i),yyyymmdd,tellink=tellink
	   endif
        endfor
     endfor
  endif


ENDFOR ; each day

end
