;+
;$Id: write_db_scripts.pro,v 1.20 2012/11/20 23:17:45 nathan Exp $
; Project     : STEREO - SECCHI
;                   
; Name        : write_db_scripts
;               
; Purpose     : Populate database table structures and write db sql scripts
;               
; Use         : IDL> write_db_scripts
;
; Inputs      : ftsfile = STRING FITS filename
;		-or-
;		ftsfile = STRARR FITS header
;		requires common block scc_reduce for nom_hdr, basehdr, ext_hdr
;               
; Outputs     : sql script file
;               
; Opt. Outputs: 
;               
; Keywords    : 
;
; Restrictions: Requires populated scch secchi header structure in 
;		scc_reduce COMMON block 
;   	    	
;
; Calls LASCO : scc_db_insert.pro
;
; Category    : database, pipeline, header
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL, 2006.08.31
;               
; $Log: write_db_scripts.pro,v $
; Revision 1.20  2012/11/20 23:17:45  nathan
; make sure encoder vals are signed byte (tinyint) to match mysql database definition
;
; Revision 1.19  2010/11/30 20:26:21  nathan
; change criteria for fileorig+rt
;
; Revision 1.18  2008/04/28 15:49:12  mcnutt
; use hstc.date_obs for all database tables
;
; Revision 1.17  2008/04/16 14:55:31  nathan
; use hstc.date_obs for db date_obs (Bug 304)
;
; Revision 1.16  2008/03/07 21:08:03  nathan
; added /SILENT
;
; Revision 1.15  2007/09/18 22:42:18  nathan
; print more info to lulog
;
; Revision 1.14  2007/08/08 15:06:37  nathan
; add fileorig to comment
;
; Revision 1.13  2007/04/24 18:24:51  secchib
; nr - ping ftsfile before readfits (Bug 151)
;
; Revision 1.12  2007/04/16 21:36:07  nathan
; append rt to fileorig if downlink eq RT (Bug 141)
;
; Revision 1.11  2007/03/15 15:26:17  nathan
; remove waits in img_hist_cmnt
;
; Revision 1.10  2007/03/13 19:26:39  secchib
; nr - path=None if header only
;
; Revision 1.9  2007/02/21 14:24:34  nathan
; reverse previous change (Bug 99)
;
; Revision 1.8  2007/02/10 00:03:30  nathan
; reduce wait time
;
; Revision 1.7  2007/01/11 15:57:51  nathan
; open and close dbfile in this routine not secchi_reduce
;
; Revision 1.6  2006/11/17 16:02:45  nathan
; fix sccreadfits call
;
; Revision 1.5  2006/11/06 14:37:01  nathan
; add comments for db script
;
; Revision 1.4  2006/10/04 17:29:18  nathan
; modify diskpath based on vchannel
;
; Revision 1.3  2006/09/20 21:10:37  nathan
; fix numstring
;
; Revision 1.2  2006/09/11 18:00:58  nathan
; ip_00_19 is ;-delimited list
;
; Revision 1.1  2006/09/08 16:56:29  nathan
; first checkin
;
;-            

function tinyint, inp

; translate inp to signed byte
while inp gt 127 do inp=inp-256

return,inp

end

PRO write_db_scripts, ftsfile, SILENT=silent

COMMON scc_reduce ;, lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs
common dbms 	  
	    ; dbms should be defined in secchi_reduce.pro only  	
	    ; ludb used by scc_db_insert.pro

; ++++ Initialize admin and common variables

version = '$Id: write_db_scripts.pro,v 1.20 2012/11/20 23:17:45 nathan Exp $'
len=strlen(version)
version = strmid(version,1,len-2)

IF datatype(img_seb_hdr) EQ 'UND' THEN $
@./secchi_img_tables_struct.inc

; Account for header only/no FITS file case
IF n_elements(ftsfile) GT 1 THEN BEGIN
    hstc=fitshead2struct(ftsfile,scch) 
    noimage=1
ENDIF ELSE BEGIN
    while not(file_exist(ftsfile)) do begin
        print,'Waiting 2 sec before retrying ',ftsfile
        wait,2
    endwhile

    nothing=sccreadfits(ftsfile,hstc,/nodata)
    noimage=0
ENDELSE
openw,ludb,dbfile,/append,/get_lun
get_utc,startdbtime,/ecs
;--Bug 141
fileorig=hstc.fileorig
;IF hstc.DOWNLINK EQ 'RT' THEN fileorig=fileorig+'rt'
printf,ludb,'# starting db script for '+hstc.filename+' ('+hstc.fileorig+') at '+startdbtime
printf,lulog,'# starting db script for '+hstc.filename+' ('+hstc.fileorig+') at '+startdbtime
;
;  update db table img_seb_hdr
;
PRINTF,lulog,'Updating DBMS table = img_seb_hdr'
IF ~keyword_set(SILENT) THEN PRINT,'Updating DBMS table = img_seb_hdr'
a=img_seb_hdr

  a.obsrvtry	=nom_hdr.platformid
  a.sync	=nom_hdr.sync


  a.OBS_ID	=basehdr.osnumber
  a.SEB_PROG	=nom_hdr.imagetype
  a.OBS_PROG  	=hstc.obs_prog
  a.OBSSETID  	=nom_hdr.campaignset
  a.LEDCOLOR  	=nom_hdr.cmdledmode
  a.FILEORIG  	=fileorig
  a.FILENAME  	=hstc.filename
  ;a.THUMB_FN  	=
  yyyymmdd=strmid(a.filename,0,8)
IF hstc.spwx EQ 'T' THEN $
  a.SPWX      	=1
  a.DATE_MOD  	=hstc.date
  a.DATE_OBS  	=hstc.date_obs
; For (HI) combo images, date_obs in database is not the same as DATE-OBS in FITS header
IF hstc.naxis1 GT 0 THEN BEGIN
  a.DATE_AVG  	=hstc.date_avg
  a.DATE_END  	=hstc.date_end
ENDIF
  a.DETECTOR  	=nom_hdr.telescopeid
  a.FILTER    	=nom_hdr.cmdfilterposition
IF hstc.detector EQ 'EUVI' THEN $
  a.POLAR_QUAD	=ext_hdr.cmdquadposition ELSE $
  a.POLAR_QUAD	=nom_hdr.cmdpolarposition
  a.POLAR2 	=nom_hdr.cmdpolarposition_2
  a.EXPTIME   	=hstc.exptime	; for combo images, exptime is the sum of all exposure durations
  a.EXPTIME2  	=basehdr.actualexpduration_2*4.0e-6
  a.BIASMEAN  	=hstc.biasmean
  a.BIASSDEV  	=hstc.biassdev
  a.IMGSEQ    	=basehdr.imgseq
  a.IMGCTR    	=basehdr.imgctr
  a.DATE_CMD  	=hstc.date_cmd
  a.P1ROW     	=nom_hdr.p1row
  a.P1COL     	=nom_hdr.p1col
  a.P2ROW     	=nom_hdr.p2row
  a.P2COL     	=nom_hdr.p2col
  a.SUMROW    	=basehdr.sumrow
  a.SUMCOL    	=basehdr.sumcol
  a.SEBXSUM   	=basehdr.sebxsum
  a.SEBYSUM   	=basehdr.sebysum
  a.NAXIS1    	=hstc.naxis1
  a.NAXIS2    	=hstc.naxis2
  a.DOORSTAT  	=nom_hdr.actualdoorposition
  a.DATALEVEL 	=0	; Level 0.5
  a.DOWNLINK  	=strmid(scch.filename,17,1)
CASE hstc.vchannel OF 
    6: branch='pb/'
    7: BEGIN
    	branch='rt/'
	fileorig=fileorig+'rt'
       END
    13: branch='lz/'
    ELSE: branch='pb/'
ENDCASE   
IF a.obsrvtry EQ 1 THEN abd='a' ELSE abd='b'
  ; subdir = img_dir/detector from COMMON scc_reduce
IF (noimage) THEN a.DISKPATH='None' ELSE a.DISKPATH  	=branch+'L0/'+abd+'/'+subdir+'/'+yyyymmdd+'/'
IF (a.sync) THEN BEGIN
	IF abd EQ 'a' THEN abn='b' ELSE abn='a'
	syncfn=a.filename
	strput,syncfn,strupcase(abn),20
  	a.SYNCED_FN 	=syncfn
  	IF (noimage) THEN a.SYNCPATH='None' ELSE a.SYNCPATH	=branch+'L0/'+abn+'/'+subdir+'/'+yyyymmdd+'/'
ENDIF
iplog=basehdr.ipcmdlog
nz=where(iplog,nnz)
numstring=';'
FOR i=0,nnz-1 DO numstring=numstring+strtrim(string(fix(iplog[nz[i]])),2)+';'
  a.IP_00_19  	=numstring
  a.EVENT     	=basehdr.CRITEVENT
  a.COSMICS   	=hstc.cosmics

;help,/str,a
scc_db_insert,a

;
;  update db table img_seb_hdr_ext
;
PRINTF,lulog,'Updating DBMS table = img_seb_hdr_ext'
IF ~keyword_set(SILENT) THEN PRINT,'Updating DBMS table = img_seb_hdr_ext'
a=img_seb_hdr_ext

  a.FILEORIG	=fileorig
  a.DATE_MOD  	=hstc.date
  a.DATE_OBS  	=hstc.date_obs
  a.EXPCMD	=hstc.expcmd
IF (scch.seb_prog EQ 'DARK' AND hstc.version GT 3) THEN $
  a.EXPCMD2 	=nom_hdr.cmdexpduration_2*2.0e-3 ELSE $
  a.EXPCMD2	=nom_hdr.cmdexpduration_2*1.024e-3     
  a.OFFSET    	=nom_hdr.offset
  a.GAINCMD   	=nom_hdr.gain
  a.GAINMODE  	=nom_hdr.gainmode
  a.ENCODERP  	=tinyint(basehdr.actualpolarposition)
  a.ENCODERP2 	=tinyint(basehdr.actualpolarposition2)
  a.SHUTTDIR  	=nom_hdr.actualshutterdirection
  a.READFILE  	=hstc.readfile
  a.WAVEFILE  	=hstc.wavefile
  a.READ_TBL  	=nom_hdr.readouttableid
  a.CLR_TBL   	=nom_hdr.clrtableid
  a.DATE_CLR  	=hstc.date_clr
  a.DATE_RO   	=hstc.date_ro
  a.READTIME  	=hstc.readtime
  a.IP_TIME   	=hstc.ip_time
  a.COMPRSSN  	=hstc.comprssn
IF hstc.naxis1 GT 0 THEN BEGIN
  a.COMPFACT  	=hstc.compfact
  a.NMISSING  	=hstc.nmissing
  a.DATAAVG   	=hstc.dataavg
  a.DATASIG   	=hstc.datasig
  a.DATASATPERC		=hstc.datasat/float(hstc.naxis1)*hstc.naxis2;    Percent of image with saturated pixels
ENDIF

  a.OBJECT    	=hstc.object
  a.CROTA     	=hstc.crota
  a.DSUN_OBS  	=hstc.dsun_obs
  a.LEDPULSE  	=nom_hdr.cmdledpulses
  a.VERSION   	=basehdr.version
  a.CEB_STAT  	=nom_hdr.cebintfstatus
  a.CAM_STAT  	=nom_hdr.ccdintfstatus
  a.LIGHTTRAVELOFFSETTIME=nom_hdr.lighttraveloffsettime	;light travel time offset in ms as set by IS
  a.IMGBUFFEROFFSET	=tinyint(nom_hdr.imgbufferoffset)
  	;SDRAM image buffer slot number for image capture [0-26] 
  a.PREEXPSCSTATUS	=nom_hdr.preexpscstatus		;Spacecraft Status byte before exposure
  a.POSTEXPSCSTATUS	=nom_hdr.postexpscstatus	;Spacecraft Status byte after exposure
  a.ACTUALOPENTIME	=nom_hdr.actualopentime		;Raw reading from MEB of actual shutter open time (when the opening edge ofthe blade crosses the center of the CCD) source: mechanism readback
  a.ACTUALCLOSETIME	=nom_hdr.actualclosetime	;Raw reading from MEB of actual shutter close time (when the closing edge of theblade crosses the center of the CCD) source: mechanism readback
  a.ACTUALOPENTIME_2	=nom_hdr.actualopentime_2
  a.ACTUALCLOSETIME_2	=nom_hdr.actualclosetime_2
  a.ACTUALSHUTTERPOSITION=tinyint(nom_hdr.actualshutterposition)
  	;State of shutter during exposure open=1 closed=0
  a.ACTUALPOLARDIRECTION=nom_hdr.actualpolardirection	;polarizer or quadrant selector CW or CCW

;help,/str,a
;stop
     scc_db_insert,a

goto, histcmnt
;
;  update db table img_cal_bias
;
PRINTF,lulog,'Updating DBMS table = img_cal_bias'
IF ~keyword_set(SILENT) THEN PRINT,'Updating DBMS table = img_cal_bias'
a=img_cal_bias
  a.FILEORIG	=fileorig
  a.DATE_MOD  	=hstc.date
  a.DATE_OBS  	=hstc.date_obs
 a.BIAS      	=0
 a.BIASSTD   	=0
 a.METHOD    	=''

     scc_db_insert,a
;
;  update db table img_cal_led
;
PRINTF,lulog,'Updating DBMS table = img_cal_led'
IF ~keyword_set(SILENT) THEN PRINT,'Updating DBMS table = img_cal_led'
a=img_cal_led 
  a.FILEORIG	=fileorig
  a.DATE_MOD  	=hstc.date
  a.DATE_OBS  	=hstc.date_obs
  a.NUM_DARK_PIX=-1
  a.LEDCOLOR  	=nom_hdr.cmdledmode
  a.LEDPULSE  	=nom_hdr.cmdledpulses

     scc_db_insert,a
;
;  update db table img_cal_dark
;
PRINTF,lulog,'Updating DBMS table = img_cal_dark'
IF ~keyword_set(SILENT) THEN PRINT,'Updating DBMS table = img_cal_dark'
a=img_cal_dark
  a.FILEORIG	=fileorig
  a.DATE_MOD  	=hstc.date
  a.DATE_OBS  	=hstc.date_obs
  a.EXPTIME   	=hstc.exptime
  a.DARK_CURRENT=-1
  a.NUM_HOT_PIX	=-1

     scc_db_insert,a

;
;  update db table img_euvi_gt
;
IF hstc.detector EQ 'EUVI' THEN BEGIN
  PRINTF,lulog,'Updating DBMS table = img_euvi_gt'
  IF ~keyword_set(SILENT) THEN PRINT,'Updating DBMS table = img_euvi_gt'
  a=img_euvi_gt 
  
  a.FILEORIG	=fileorig
  a.DATE_MOD  	=hstc.date
  a.DATE_OBS  	=hstc.date_obs
  a.FPS_CMD   	=ext_hdr.usefps
  a.FPS_ON    	=ext_hdr.actualfpsmode
  a.ENCODERF  	=basehdr.actualfilterposition
  a.ENCODERQ  	=basehdr.actualpolarposition
  a.ACTUALFILTERDIRECTION=nom_hdr.actualfilterdirection
  a.ACTUALSCFINEPOINTMODE=ext_hdr.actualscfinepointmode
  a.FPSOFFY   	=ext_hdr.fpsdata.yoffset
  a.FPSOFFZ   	=ext_hdr.fpsdata.zoffset
  a.FPSNUMS   	=ext_hdr.fpsdata.numfpssamples
  a.FPSGTSY   	=ext_hdr.fpsdata.fpsysum
  a.FPSGTSZ   	=ext_hdr.fpsdata.fpszsum
  a.FPSGTQY   	=ext_hdr.fpsdata.fpsysquare
  a.FPSGTQZ   	=ext_hdr.fpsdata.fpszsquare
  a.FPSERS1   	=ext_hdr.fpsdata.pzterrsum[0]
  a.FPSERS2   	=ext_hdr.fpsdata.pzterrsum[1]
  a.FPSERS3   	=ext_hdr.fpsdata.pzterrsum[2]
  a.FPSERQ1   	=ext_hdr.fpsdata.pzterrsquare[0]
  a.FPSERQ2   	=ext_hdr.fpsdata.pzterrsquare[1]
  a.FPSERQ3   	=ext_hdr.fpsdata.pzterrsquare[2]
  a.FPSDAS1   	=ext_hdr.fpsdata.pztdacsum[0]
  a.FPSDAS2   	=ext_hdr.fpsdata.pztdacsum[1]
  a.FPSDAS3   	=ext_hdr.fpsdata.pztdacsum[2]
  a.FPSDAQ1   	=ext_hdr.fpsdata.pztdacsquare[0]
  a.FPSDAQ2   	=ext_hdr.fpsdata.pztdacsquare[1]
  a.FPSDAQ3   	=ext_hdr.fpsdata.pztdacsquare[2]
  a.PZT1YCOORDXFORM	=ext_hdr.fpsdata.fpscoefs.pz[0].ycoordxform
  a.PZT2YCOORDXFORM	=ext_hdr.fpsdata.fpscoefs.pz[1].ycoordxform
  a.PZT3YCOORDXFORM	=ext_hdr.fpsdata.fpscoefs.pz[2].ycoordxform
  a.PZT1ZCOORDXFORM	=ext_hdr.fpsdata.fpscoefs.pz[0].zcoordxform
  a.PZT2ZCOORDXFORM	=ext_hdr.fpsdata.fpscoefs.pz[1].zcoordxform
  a.PZT3ZCOORDXFORM	=ext_hdr.fpsdata.fpscoefs.pz[2].zcoordxform
  a.PZT1VOLTPREDICT0	=ext_hdr.fpsdata.fpscoefs.pz[0].voltpredict[0]
  a.PZT1VOLTPREDICT1	=ext_hdr.fpsdata.fpscoefs.pz[0].voltpredict[1]
  a.PZT1VOLTPREDICT2	=ext_hdr.fpsdata.fpscoefs.pz[0].voltpredict[2]
  a.PZT1VOLTPREDICT3	=ext_hdr.fpsdata.fpscoefs.pz[0].voltpredict[3]
  a.PZT1VOLTPREDICT4	=ext_hdr.fpsdata.fpscoefs.pz[0].voltpredict[4]
  a.PZT2VOLTPREDICT0	=ext_hdr.fpsdata.fpscoefs.pz[1].voltpredict[0]
  a.PZT2VOLTPREDICT1	=ext_hdr.fpsdata.fpscoefs.pz[1].voltpredict[1]
  a.PZT2VOLTPREDICT2	=ext_hdr.fpsdata.fpscoefs.pz[1].voltpredict[2]
  a.PZT2VOLTPREDICT3	=ext_hdr.fpsdata.fpscoefs.pz[1].voltpredict[3]
  a.PZT2VOLTPREDICT4	=ext_hdr.fpsdata.fpscoefs.pz[1].voltpredict[4]
  a.PZT3VOLTPREDICT0	=ext_hdr.fpsdata.fpscoefs.pz[2].voltpredict[0]
  a.PZT3VOLTPREDICT1	=ext_hdr.fpsdata.fpscoefs.pz[2].voltpredict[1]
  a.PZT3VOLTPREDICT2	=ext_hdr.fpsdata.fpscoefs.pz[2].voltpredict[2]
  a.PZT3VOLTPREDICT3	=ext_hdr.fpsdata.fpscoefs.pz[2].voltpredict[3]
  a.PZT3VOLTPREDICT4	=ext_hdr.fpsdata.fpscoefs.pz[2].voltpredict[4]
  a.PZT1PIDCOE0		=ext_hdr.fpsdata.fpscoefs.pz[0].pidcoe[0]
  a.PZT1PIDCOE1		=ext_hdr.fpsdata.fpscoefs.pz[0].pidcoe[1]
  a.PZT1PIDCOE2		=ext_hdr.fpsdata.fpscoefs.pz[0].pidcoe[2]
  a.PZT1PIDCOE3		=ext_hdr.fpsdata.fpscoefs.pz[0].pidcoe[3]
  a.PZT2PIDCOE0		=ext_hdr.fpsdata.fpscoefs.pz[1].pidcoe[0]
  a.PZT2PIDCOE1		=ext_hdr.fpsdata.fpscoefs.pz[1].pidcoe[1]
  a.PZT2PIDCOE2		=ext_hdr.fpsdata.fpscoefs.pz[1].pidcoe[2]
  a.PZT2PIDCOE3		=ext_hdr.fpsdata.fpscoefs.pz[1].pidcoe[3]
  a.PZT3PIDCOE0		=ext_hdr.fpsdata.fpscoefs.pz[2].pidcoe[0]
  a.PZT3PIDCOE1		=ext_hdr.fpsdata.fpscoefs.pz[2].pidcoe[1]
  a.PZT3PIDCOE2		=ext_hdr.fpsdata.fpscoefs.pz[2].pidcoe[2]
  a.PZT3PIDCOE3		=ext_hdr.fpsdata.fpscoefs.pz[2].pidcoe[3]

;help,/str,a
  scc_db_insert,a
ENDIF else stop

histcmnt:
;
;  update db table img_hist_cmnt
;
get_utc,today
tainow=utc2tai(today)-1
; fake date-mod by adding 1 sec to today; primary key is DATE_OBS, FILENAME, DATE_MOD 
FOR i=0,19 DO BEGIN
	
	a=img_hist_cmnt
  	a.FILENAME  	=hstc.filename
  	a.DATE_OBS  	=hstc.date_obs
  	a.FLAG      	='history'
  	a.INFO      	=hstc.history[i]
	IF a.info NE '' THEN BEGIN
  	    	tainow=tainow+1
		a.DATE_MOD  	=utc2str(tai2utc(tainow),/ecs)
		PRINTF,lulog,'Updating HISTORY in DBMS table = img_hist_cmnt'
		IF ~keyword_set(SILENT) THEN print,'Updating HISTORY in DBMS table = img_hist_cmnt'
;help,/str,a
		scc_db_insert,a
		delflag=0   ; only set first time, if at all
		;wait,1
	ENDIF
	
	get_utc,today,/ecs
	a=img_hist_cmnt
  	a.FILENAME  	=hstc.filename
  	a.DATE_OBS  	=hstc.date_obs
  	a.FLAG      	='comment'
  	a.INFO      	=hstc.comment[i]
	IF a.info NE '' THEN BEGIN
  	    	tainow=tainow+1
		a.DATE_MOD  	=utc2str(tai2utc(tainow),/ecs)
		PRINTF,lulog,'Updating COMMENT in DBMS table = img_hist_cmnt'
		IF ~keyword_set(SILENT) THEN print,'Updating COMMENT in DBMS table = img_hist_cmnt'
;help,/str,a
		scc_db_insert,a
		delflag=0   ; only set first time, if at all
		;wait,1
	ENDIF
	
ENDFOR

close,ludb
free_lun,ludb

end
