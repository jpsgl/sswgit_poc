pro secchi_hi_low_merge,outfile,outdir,SUMMARY=summary
;+
; $Id: secchi_hi_low_merge.pro,v 1.33 2014/08/13 15:59:48 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : secchi_hi_low_merge
;               
; Purpose     : to merge HI two part images in pipeline proseccing
;               
; Explanation : 
;               
; Use         : IDL> secchi_hi_low_merge
;    
; Inputs      : 
;               
; Outputs     : 
;
; Keywords :	SUMMARY    set to add merge file name to summary file
;               
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt 02/05/2007 
;               
; Modified    :
;
; $Log: secchi_hi_low_merge.pro,v $
; Revision 1.33  2014/08/13 15:59:48  secchia
; correct crpix for sidelobe images
;
; Revision 1.32  2014/08/11 14:11:21  secchia
; Added check to filter out the space weather images and still work with sidelobe images
;
; Revision 1.31  2014/08/06 14:23:20  secchia
; added orig IP_00_19 to history comment if sidelobe
;
; Revision 1.30  2014/08/05 18:44:18  secchia
; changed side_lobe filename form n to s
;
; Revision 1.29  2014/08/05 17:55:20  hutting
; corrected if statement
;
; Revision 1.28  2014/08/05 17:51:06  hutting
; added check to see if scchhi is defined
;
; Revision 1.27  2014/08/05 17:33:34  hutting
; moved IP_00_19 correct to hi part header
;
; Revision 1.26  2014/08/05 17:14:30  hutting
; added sode lobe header corrections looks for 53 in IP to apply correctionssecchi_hi_low_merge.pro
;
; Revision 1.25  2014/07/31 14:18:08  secchia
; will multiply low part if divided by 4 while summimg sidelobe
;
; Revision 1.24  2010/06/28 19:48:49  nathan
; make sure BLANK is zero not -32768
;
; Revision 1.23  2010/06/28 15:47:15  mcnutt
; change hi part filename (seb_prog) to h for header only
;
; Revision 1.22  2010/01/28 19:09:21  secchia
; set BZERO=0 in khdr to be compatible with secchi_reduce.pre,v 1.339
;
; Revision 1.21  2009/12/14 19:32:46  nathan
; undo unintended commit
;
; Revision 1.19  2009/05/06 16:24:59  nathan
; updated DOORSTAT logic and comments to correctly handle EUVI disabled door
;
; Revision 1.18  2007/09/25 19:07:49  nathan
; added DSATVAL and DATASAT in header
;
; Revision 1.17  2007/09/18 22:44:51  nathan
; added COMMON scc_reduce to get lulog to add info to processing log, and so
; had to change all instances of scch to sch; do not write_db_script for low
; image header
;
; Revision 1.16  2007/08/22 18:39:34  nathan
; hilow write_db_scripts in secchi_hi_low_merge
;
; Revision 1.15  2007/08/13 19:27:39  nathan
; update to do HI summary and db in secchi_reduce, and make sure all HI images go to hi_point_v2
;
; Revision 1.14  2007/08/06 14:27:49  nathan
; return changed outfile
;
; Revision 1.13  2007/05/03 14:29:14  mcnutt
; Bug 100 and Bug 84 added misslist and nmissing for rice and HC
;
; Revision 1.12  2007/04/24 18:19:20  secchib
; nr - ping outdir before writefits (Bug 151)
;
; Revision 1.11  2007/04/20 19:38:23  mcnutt
; uses scc_read_ip_dat
;
; Revision 1.10  2007/04/20 19:27:54  mcnutt
; corrected findfile files names
;
; Revision 1.9  2007/04/18 17:11:33  mcnutt
; will work with both SSR2 and SSR1 2000 style file names
;
; Revision 1.8  2007/04/10 15:52:21  mcnutt
; use different summary file sfx for each telescope
;
; Revision 1.7  2007/04/09 15:04:37  mcnutt
; implement img.eu/co/hi filenames for summary file (Bug 118)
;
; Revision 1.6  2007/04/03 15:10:55  mcnutt
; removed last change does not create seperagte summary files for HI images
;
; Revision 1.5  2007/03/30 22:23:06  mcnutt
; BUG 118 write separate summary files for HI
;
; Revision 1.4  2007/03/14 19:10:28  mcnutt
; uses long and writes fits to passed outdir
;
; Revision 1.3  2007/03/02 17:43:45  mcnutt
; selects only H and L images parts
;
; Revision 1.2  2007/02/21 21:40:26  mcnutt
; uses the hearder fro the hi part of the image
;
; Revision 1.1  2007/02/05 15:33:28  mcnutt
; merges HI low hi images
;
COMMON scc_reduce ;, lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs
common dbms 	  
	    ; dbms should be defined in secchi_reduce.pro only  	
	    ; ludb used by scc_db_insert.pro
    	    ;--need to set delflag
IF (delflag) THEN delflagset=1 ELSE delflagset=0

;imfiles=findfile(strmid(imfile,0,strlen(imfile)-12)+'?'+strmid(imfile,strlen(imfile)-11,strlen(imfile)-4)+'?'+strmid(imfile,strlen(imfile)-2,strlen(imfile)-1))
imfiles=findfile(strmid(outfile,0,strlen(outfile)-12)+'?'+strmid(outfile,strlen(outfile)-11,4)+'?'+strmid(outfile,strlen(outfile)-6,6))
print,imfiles

;imfiles=imfiles(where(strmid(imfiles,strlen(imfiles(0))-8,1) eq 'H' or strmid(imfiles,strlen(imfiles(0))-8,1) eq 'L'))
nimf=n_elements(imfiles)
tmp=rstrmid(imfiles,6,1)
tmp2=rstrmid(imfiles,11,1)
if nimf gt 2 then begin
  imfiles=imfiles[where(tmp eq 4)]
  tmp=rstrmid(imfiles,6,1)
  tmp2=rstrmid(imfiles,11,1)
endif
nimf=n_elements(imfiles)

if (nimf eq 2 and total(fix(tmp))/2 eq tmp[0] and total(fix(tmp2))/2 ne 2) then begin
  sch = def_secchi_hdr()

  for n1=0,1 do begin
    img=readfits(imfiles(n1),hdr)
    sch=fitshead2struct(hdr,sch)
    if (strlen(sch.IP_00_19) lt 60) then IP_00_19=' '+sch.IP_00_19 else IP_00_19=sch.IP_00_19
    IPLOG=intarr(20)
    for n2=0,19 do IPLOG(n2)=strmid(IP_00_19,n2*3,3)
    if (where(IPLOG eq 120) gt -1) then begin
    ; High byte, fileorig= ????2???
      imghi=img & hi_hdr=hdr
      scchhi=sch
      hiip=IPLOG
      print,'using '+imfiles(n1)+' for high part'
      printf,lulog,'using '+imfiles(n1)+' for high part'
    endif 
    if (where(IPLOG eq 120) eq -1) then begin
    ; Low byte,  fileorig= ????0???
      imglow=img & low_hdr=hdr
      exthdr=readfits(imfiles(n1),/exten,xhdr)
      scchlow=sch
;********** start side lobe corrections **********
      psm=where(IPLOG eq 53,cnt)
      if cnt gt 0 then begin 
        imglow=imglow*long(4^cnt)
	newip0019=sch.ip_00_19
        for n2=0,cnt -1 do strput,newip0019,' ',strpos(newip0019,' 53')+1
        sidelobe_cmnt='Div4C'

    	s=size(imglow,/dimensions)

; DSTOP! and DSTOP2 used in SCC_IMG_TRIM

    	fxaddpar,low_hdr,'DSTOP1',s[0]
    	fxaddpar,low_hdr,'DSTOP2',s[1]

; SUMMED used in SCC_MAKE_ARRAY

    	if s[0] eq 512 then fxaddpar,low_hdr,'SUMMED',3
    	if s[0] eq 256 then fxaddpar,low_hdr,'SUMMED',4

; IPSUM, EXPTIME, N_IMAGES, IMGSEQ used in HI_DESMEAR
    	if s[0] eq 512 then fxaddpar,low_hdr,'IPSUM',3
    	if s[0] eq 256 then fxaddpar,low_hdr,'IPSUM',4
         crpix=s[0]/2 +.5
         fxaddpar,low_hdr,'CRPIX1',crpix
         fxaddpar,low_hdr,'CRPIX2',crpix
         fxaddpar,low_hdr,'CRPIX1A',crpix
         fxaddpar,low_hdr,'CRPIX2A',crpix

        if sch.detector eq 'HI1' then begin
    	  fxaddpar,low_hdr,'EXPTIME',1200
    	  fxaddpar,low_hdr,'N_IMAGES',30
    	  fxaddpar,low_hdr,'IMGSEQ',29
        endif
        if sch.detector eq 'HI2' then begin
    	  fxaddpar,low_hdr,'EXPTIME',4950
    	  fxaddpar,low_hdr,'N_IMAGES',99
    	  fxaddpar,low_hdr,'IMGSEQ',98
        endif
    	  fxaddpar,low_hdr,'HISTORY','Orig IP_00_19: '+sch.ip_00_19
         
        scchlow.filename=strmid(scchlow.filename,0,16)+'s'+strmid(scchlow.filename,17,8)

      endif
;********** end side lobe corrections **********

      print,'using '+imfiles(n1)+' for low part'
      printf,lulog,'using '+imfiles(n1)+' for low part'
    endif 
  endfor

  fits_hdr=low_hdr ;use Low part for header for correct ext  header keywords
  sch=scchlow

  merged = (imghi * long(65536)) + imglow
  print,sch.bitpix

    reduce_statistics,merged,fits_hdr
    
    IF (max(merged) gt 0) THEN BEGIN
    	ccdsat=14000. 
    	dsatval = long(ccdsat * scchlow.N_IMAGES * (2^(scchlow.IPSUM - 1))^2)
    	it=where(merged GE dsatval, nsat)
    	fxaddpar,fits_hdr,'DSATVAL',dsatval
    	fxaddpar,fits_hdr,'DATASAT',nsat
    	help,dsatval,nsat
    
    	printf,lulog,scchlow.filename+'  DSATVAL='+string(dsatval)+', DATASAT='+string(nsat)
    ENDIF
  
  if (scchlow.nmissing ne 0 or scchhi.nmissing ne 0) then begin
    if (scchhi.nmissing ne 0)then begin
      scc_missing_blocks,imghi,hi_hdr,hnmissing,hmissn,hmisslist,hnblocks
      misslist=hmisslist & nblocks=hnblocks & nmissing=hnmissing
    endif
    if (scchlow.nmissing ne 0)then begin
      scc_missing_blocks,imglow,low_hdr,lnmissing,lmissn,lmisslist,lnblocks
      misslist=lmisslist & nblocks=lnblocks & nmissing=lnmissing
    endif
    if (scchlow.nmissing ne 0 and scchhi.nmissing ne 0) then begin    
       missn=[lmissn,hmissn] & missn=missn(sort(missn)) & missn=missn(uniq(missn))
       nmissing=n_elements(missn)
       misslist=''
       miss34=inttobn(missn,34)
       for n=0,n_Elements(missn)-1 do MISSLIST=misslist+string(miss34(n),'(a2)')
    endif
    fxaddpar,fits_hdr,'NMISSING',nmissing,'out of '+trim(string(nblocks))+' blocks'
    sch.nmissing=nmissing
    fxaddpar,fits_hdr,'MISSLIST',strmid(misslist,0,60),'base 34 use bntoint.pro'
  endif    
      

  fxaddpar,fits_hdr,'filename',strmid(scchlow.filename,0,17)+strmid(scchlow.fileorig,9,1)+strmid(scchlow.filename,18,7)

  if datatype(newip0019) ne 'UND' then  fxaddpar,fits_hdr,'IP_00_19',newip0019,sidelobe_cmnt else $
    fxaddpar,fits_hdr,'IP_00_19',scchhi.IP_00_19

  if datatype(ipdat) EQ 'UND' THEN BEGIN
    ssss = scc_read_ip_dat(IPver=IPver)
    ipdat=replicate('ERROR',256)
    for i=0,n_elements(ssss.ip_description)-1 do ipdat[i]=ssss[i].ip_description
  ENDIF
  for i=0,9 do $
	fxaddpar,fits_hdr,'ip_prog'+trim(string(i)), fix(hiip[i]), $
	ipdat[hiip[i]]



   sch=fitshead2struct(fits_hdr,sch)

    img_dir='img'
    
    case strmid(sch.filename,16,1) of
      'k' : IF (sch.detector NE 'HI1') AND (sch.detector NE 'HI2') THEN img_dir='cal'   ; DARK
      'e' : img_dir='cal'   ; LED
      'c' : img_dir='cal'   ; CONT
    else : 
    endcase
    if(trim(strlowcase(sch.detector)) eq 'cor1' or trim(strlowcase(sch.detector)) eq 'cor2')then begin
    	case strmid(sch.filename,16,1) of
    	    'n' : img_dir='seq'
    	    's' : img_dir='seq'
	    else:
    	endcase
    endif
;create SUB directory name and make hin directories hi_n

;eDoorStatus : $
;[   'CLOSED',	$
;    'IN_TRANSIT',	$
;    'OPEN ',	$
;    'OFF',	$
;    'ERROR',    $
;    replicate('invalid',250), $
;    'NO_DATA' ]   }
    
    ; SCIP door CLOSED or IN_TRANSIT go in cal; HI door CLOSED (only) go in cal
    ; disabled door results in OFF, which for now we assume is OPEN - nbr, 090506
    IF ((scch.doorstat EQ 1) AND  (scch.detector NE 'HI1') AND (scch.detector NE 'HI2')) OR $
        (scch.doorstat EQ 0)  $
       THEN img_dir='cal' 

    case trim(strlowcase(sch.detector)) of
    	'hi1' : BEGIN
	    SUBdir=img_dir+'/hi_1'
	    END
    	'hi2' : BEGIN
	    SUBdir=img_dir+'/hi_2'
	    END
    	else  : SUBdir=img_dir+'/'+trim(strlowcase(sch.detector))
    endcase
    
    outfile = concat_dir(outdir,sch.filename)


    fhdr=['',fits_hdr]
    ;fhdr[1:105]=fits_hdr[0:104]
    ;fhdr[0]=''
    while not(file_exist(outdir)) do begin
		print,'Waiting 2 sec before retrying ',outdir
		wait,2
    endwhile
    
    writefits,outfile,merged,fhdr
    writefits,outfile,merged,fits_hdr
    d=readfits(outfile,khdr,/noscale)
    while not(file_exist(outdir)) do begin
		print,'Waiting 2 sec before retrying ',outdir
		wait,2
    endwhile
    fxaddpar,khdr,'BZERO',0.,''
    fxaddpar,khdr,'BLANK',0 	; not -32768
    writefits,outfile,merged,khdr

    dext=datatype(exthdr)
    if(dext ne 'LON')then writefits,outfile,exthdr,xhdr,/append

;  if keyword_set(summary) then begin    
;    img_dir='img'
;    
;    case strmid(sch.filename,16,1) of
;      'k' : IF (sch.detector NE 'HI1') AND (sch.detector NE 'HI2') THEN img_dir='cal'   ; DARK
;      'e' : img_dir='cal'   ; LED
;      'c' : img_dir='cal'   ; CONT
;    else : 
;    endcase
;    if(trim(strlowcase(sch.detector)) eq 'cor1' or trim(strlowcase(sch.detector)) eq 'cor2')then begin
;    	case strmid(sch.filename,16,1) of
;    	    'n' : img_dir='seq'
;    	    's' : img_dir='seq'
;	    else:
;    	endcase
;    endif
;    	sfx='.'+strmid(sch.filename,18,2)
;	sumfile= concat_dir(getenv('SCC_SUM'), 'scc'+strmid(sch.filename,20,1) $
;	    	    	    	    	    	    +strmid(sch.filename,0,6) $
;						    +'.'+img_dir+sfx)
;    sccwritesummary,scch,sumfile
;    write_db_scripts, outfile
;  endif

    ; Need to write most complete hdr first
    ;write_db_scripts,fits_hdr
    ; nr - since filename AND fileorig are unique for high and low, can leave db_script for secchi_reduce
    ; Then write high-byte info to get its fileorig in seb_ext_hdr table 
    ; (hi_hdr seb_hdr row will be rejected)
    if (delflagset) THEN delflag=1
    scchhi.filename=strmid(scchhi.filename,0,16)+'h'+strmid(scchhi.filename,17,8) ;change fits file name to h for header only database entry
    fxaddpar,hi_hdr,'FILENAME',scchhi.filename
    write_db_scripts,hi_hdr

endif else begin
    message,'Only '+trim(nimf)+' files available; skipping.',/info
    wait,5
    return
endelse

end
