;$Id: sebhdr_struct.inc,v 1.2 2005/05/04 01:50:38 nathan Exp $
;
;*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO
;*  from $stereo/dps/dpf/utils/rdSh/hdr.h on Tue Sep  7 17:51:17 2004

print,'Running sebhdr_struct.inc'
char   = 0UB 
double = 0d
uint8 = 0UB
uint16 = 0US
uint32 = 0UL
int8 = 0B
int16 = 0S
int32 = 0L
;  /*
;  */
;  //////typedef enum
;  //////{
;  //////      STEREO_A = 1
;  //////    , STEREO_B = 2
;  //////}ePlatformTypes;
;  //////typedef enum
;  //////{
;  //////    HKP_NONE = 0,
;  //////    HKP_COR_2,
;  //////    HKP_COR_1,
;  //////    HKP_EUVI,
;  //////    HKP_HI_2,
;  //////    HKP_HI_1,
;  //////    HKP_NUM_INSTRUMENTS    // This identifier must be last in the list!
;  //////} eHkpInstrument;
;  ////////////////////////////////////////////////////////
;  // Define Calibration LED Colors
;  ////////////////////////////////////////////////////////
;  //////typedef enum
;  //////{
;  //////    HKP_CAL_LED_NONE = 0,
;  //////    HKP_CAL_LED_RED,
;  //////    HKP_CAL_LED_PURPLE,
;  //////    HKP_CAL_LED_BLUE,
;  //////    HKP_CAL_NUM_COLORS    // This identifier must be last in the list!
;  //////} eHkpCalLedColor;
;  // FPS Table structures and enumerations
;  //
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 22 bytes
sfpspztcoef = { sfpspztcoef,$
          ycoordxform:	int16,$
          zcoordxform:	int16,$
          voltpredict:	replicate(int16,5),$
          pidcoe:	replicate(int16,4) }
;  // structure size is 66 bytes
sgtfpscoefs = { sgtfpscoefs,$
          pz:	replicate(sfpspztcoef,3) }
n_img_proc_cmd = 10
;  ///////////////////////////////////////////////////////////
;  // EUVI FPS Image Header structures and enumerations
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 144 bytes
sgtfpsimagehdr = { sgtfpsimagehdr,$
          imagenum:	uint16,$
          yoffset:	int16,$
          zoffset:	int16,$
          spare1:	uint16,$
          numfpssamples:	uint32,$
          fpsysum:	int32,$
          fpszsum:	int32,$
          fpsysquare:	int32,$
          fpszsquare:	int32,$
          pzterrsum:	replicate(int32,3),$
          pzterrsquare:	replicate(int32,3),$
          pztdacsum:	replicate(int32,3),$
          pztdacsquare:	replicate(int32,3),$
          fpscoefs:	sgtfpscoefs,$
          spare2:	uint16 }
;  /////
;  //  Overall  setup / configuration of the camera interface hardware
;  //
;  /////typedef enum
;  /////{
;  /////    CAMERA_NOT_READY = 0
;  /////  , CAMERA_READY
;  /////  , CAMERA_ERROR
;  /////  , CAMERA_UNKNOWN_STATE
;  /////}CAMERA_PROGRAM_STATE;
;  /////
;  //  Camera Interface error codes
;  //
;  /////typedef enum
;  /////{
;  /////    SUCCESSFUL_RESPONSE      = 0  // No errors occurred
;  /////  , NO_COMMANDED_ACTION      = -1 // No activity has yet occurred
;  /////  , START_NOT_ACKNOWLEDGED   = 1  // Start sequence command not acknowledged
;  /////  , STOP_NOT_ACKNOWLEDGED    = 2  // Stop sequence command not acknowledged
;  /////  , WRITE_NOT_ACKNOWLEDGED   = 3  // Data byte write to the Camera interface failed
;  /////  , INVALID_COMMAND_RESPONSE = 4  // Invalid command received by Camera box
;  /////  , PARITY_ERROR_RESPONSE    = 5  // Parity error detected by Camera box
;  /////  , NO_STOP_BIT_RESPONSE     = 6  // No stop bit error detected by Camera box
;  /////  , SEND_ON_LINK_FAILED      = 7  // Unable to send message on link
;  /////  , READ_ON_LINK_FAILED      = 8  // Unable to perform read on link
;  /////  , CAMERA_ADDRESS_NOT_ACKNOWLEDGED = 9 // Address error detected by Camera box
;  /////  , READ_ADDRESS_NOT_ACKNOWLEDGED = 10 // Read address for sequence not acknowledged
;  /////  , INVALID_OFFSET_VALUE     = 11 // Value of Offset is not within limits
;  /////  , INVALID_GAIN_VALUE       = 12 // Value of Gain is not within limits
;  /////  , INVALID_MODE_VALUE       = 13 // Value of Mode is not within limits
;  /////  , OFFSET_WRITE_FAILED      = 14 // CDS programming of offset failed
;  /////  , GAIN_WRITE_FAILED        = 15 // CDS programming of gain failed
;  /////  , MODE_WRITE_FAILED        = 16 // CDS programming of mode failed
;  /////  , FLIGHT_TABLE_NOT_AVAILABLE = 17 // Init of default flight tables failed
;  /////  , WAVE_TABLE_PATCH_FAILED    = 18 // Patching wave table data to camera failed
;  /////  , READOUT_TABLE_PATCH_FAILED = 19 // Patching readout table data to camera failed
;  /////}CAMERA_INTERFACE_STATUS ;
;  /////typedef enum
;  /////{
;  /////      SPACEWEATHER  =  0
;  /////    , NOMINAL       =  1
;  /////    , EXTENDED      =  2
;  /////}eHeaderType;
;  ///////
;  //    Definition of image header data sections.
;  // m_baseHeader - defines the space weather header
;  // m_nominalHeader - defines the nominal image data for CORs
;  // m_extendedHeader - defines the additional image data for EUVI
;  ///////////////////////////////////////////////////////////////////////
;  //   Base Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  /** headers present ( 0-base, 1-base+nominal, 2-all 3 )
;  Determined by IP logic */
;  /** Commanded exposure duration in units of 1.024 msec ( max of 67.1 seconds)
;  */
;  /** Commanded 2nd exposure duration in units of 1.024 msec
;  Image type is set to DOUBLE to use this value*/
;  /** Actual exp duration 24 bits with LSB = 4 usec ( Close time minus
;  Open time )*/
;  /** time (UTC) of actual exposure (Shutter command) NOT including
;  lt_offset */
;  /** time (UTC) of actual exposure #2 (Shutter command) NOT including
;  lt_offset */
m_baseheader = { m_baseheader1,$
          filename:	replicate(char,13),$
          eheadertypeflag:	uint8,$
          version:	uint16,$
          osnumber:	uint32,$
          critevent:	uint16,$
          imgctr:	uint16,$
          imgseq:	uint16,$
          telescopeimgcnt:	uint16,$
          sumrow:	uint16,$
          sumcol:	uint16,$
          cmdexpduration:	uint16,$
          cmdexpduration_2:	uint16,$
          actualfilterposition:	uint16,$
          actualpolarposition:	uint16,$
          sebxsum:	uint16,$
          sebysum:	uint16,$
          ipcmdlog:	replicate(uint8,n_img_proc_cmd),$
          actualexpduration:	uint32,$
          actualexptime:	double,$
          actualexptime_2:	double }
;  ///////////////////////////////////////////////////////////////////////
;  //   Nominal Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  //   Camera Setup params
;  //
;  /////
;  //  Actuals
;  //
;  /** Last status known of the spacewire interface to the CEB based
;  on codes defined as specified in enum CAMERA_INTERFACE_STATUS */
;  /** Last status known of the CCD interface within the CEB
;  based on codes as defined by enum CAMERA_PROGRAM_STATE */
;  ///////////////////////////////////////////////////////////////////////
;  //   Mechanism Setup / Actuals Data
;  //
;  /** Instrument uses a double format for time from epoch 2000 - Jan 01
;  11:58:55.816 UTC time of exposure uploaded NOT including lt_offset */
;  /** LED ID used or 0=no led used Physical location determines mechanism
;  sequence */
;  /////
;  //  Actuals
;  //
;  /** Readback of actual shutter open time (when the opening edge of the
;  source: mechanism readback*/
;  /** Readback of actual shutter close time (when the closing edge of the
;  source: mechanism readback*/
;  /** Readback of actual shutter open time #2 (when the opening edge of
;  source: mechanism readback*/
;  /** Readback of actual shutter close time #2(when the closing edge of
;  source: mechanism readback*/
;  /** Actual for 2nd exposure duration ( 24 bits with LSB = 4 usec )
;  source: mechanism readback*/
;  ///////////////////////////////////////////////////////////////////////
;  //   Image Processing Params / Record
;  //
m_nominalheader = { m_nominalheader1,$
          platformid:	uint8,$
          telescopeid:	uint8,$
          imagetype:	uint8,$
          sync:	uint16,$
          campaignset:	uint16,$
          offset:	uint16,$
          gain:	uint16,$
          gainmode:	uint16,$
          clrtableid:	uint16,$
          readouttableid:	uint16,$
          p1col:	uint16,$
          p1row:	uint16,$
          p2col:	uint16,$
          p2row:	uint16,$
          imgbufferoffset:	uint16,$
          cebintfstatus:	uint8,$
          ccdintfstatus:	uint8,$
          cmdexptime:	double,$
          lighttraveloffsettime:	uint32,$
          cmdpolarposition:	uint16,$
          cmdpolarposition_2:	uint16,$
          cmdledmode:	uint8,$
          cmdledpulses:	uint32,$
          preexpscstatus:	uint16,$
          postexpscstatus:	uint16,$
          actualopentime:	uint32,$
          actualclosetime:	uint32,$
          actualopentime_2:	uint32,$
          actualclosetime_2:	uint32,$
          actualexpduration_2:	uint32,$
          actualshutterposition:	uint16,$
          actualshutterdirection:	uint16,$
          actualpolardirection:	uint16,$
          actualdoorposition:	uint16,$
          actualreadouttime:	uint32,$
          ipfunction:	uint16,$
          ipcmd:	replicate(uint8,n_img_proc_cmd),$
          ipcmdcnt:	uint16,$
          ipprocessingtime:	uint32 }
;  ///////////////////////////////////////////////////////////////////////
;  //   Extended Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  //// Guide Telescope and FPS statistics
;  //// see Guide Telescope SICM for algorithm and variable definitions
;  ////
m_extendedheader = { m_extendedheader,$
          usefps:	uint16,$
          actualfpsmode:	uint16,$
          actualscfinepointmode:	uint16,$
          cmdfilterposition:	uint16,$
          cmdquadposition:	uint16,$
          actualfilterdirection:	uint16,$
          actualquaddirection:	uint16,$
          fpsdata:	sgtfpsimagehdr }
