pro merge_summary,yyyymm, TELESCOPE=telescope, NEW_SCC_SUM=new_scc_sum
;+
; $Id: merge_summary.pro,v 1.6 2009/12/14 18:49:22 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : 
;               
; Purpose     : remove repeated lines in and sort summary files 
;               
; Explanation : 
;               
; Use         : IDL> merge_summary, yyyymm
;    
; Inputs      : yyyymm = date with or without day
;               
; Outputs     : 
;
; Keywords :	TELESCOPE = 'cor1','cor2','hi1','hi2', or 'euvi'
;   	    	NEW_SCC_SUM = directory with reprocessed/new summary files
;
; Calls from LASCO : 
;
; Common      : 
;               
; Restrictions: Requires pipeline processing environment (user secchia or secchib)
;               
; Side effects: overwrites files in $SECCHI/summary
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Sep 2007
;               
; Modified    :
;
; $Log: merge_summary.pro,v $
; Revision 1.6  2009/12/14 18:49:22  nathan
; changed keyword TELCODE to TELESCOPE
;
; Revision 1.5  2009/01/08 14:58:34  mcnutt
; removed stops accidently committed with secchi_reduce.pro v1.4
;
; Revision 1.4  2009/01/08 14:39:27  mcnutt
; date_fname_utc to date_cmd to complete v1.293 mods
;
; Revision 1.3  2008/02/08 22:03:51  nathan
; added TELDIR keyword and made optional NEW_SCC_SUM for combining files
;
; Revision 1.2  2007/10/04 20:15:41  secchib
; checks only fits file names for repeats
;
; Revision 1.1  2007/09/20 14:46:24  mcnutt
; new
;

IF keyword_set(TELESCOPE) THEN BEGIN
    tel=strlowcase(telescope)
    IF tel EQ 'cor1' THEN telcode='c1'
    IF tel EQ 'cor2' THEN telcode='c2'
    IF tel EQ 'hi1' THEN telcode='h1'
    IF tel EQ 'hi2' THEN telcode='h2'
    IF tel EQ 'euvi' THEN telcode='eu'
    sfx='*'+telcode
ENDIF ELSE sfx='*'
sumdir=getenv('SECCHI')+'/summary'
yyyymm=strmid(yyyymm,0,6)

sumfiles=findfile(sumdir+'/*'+yyyymm+sfx)
sn=strmid(sumfiles,strlen(sumfiles(0))-17,17)
sumlist=sumfiles

newsum=1
IF keyword_set(NEW_SCC_SUM) THEN BEGIN
    addsumfiles=findfile(concat_dir(new_scc_sum,'*'+yyyymm+sfx))
    asn=strmid(addsumfiles,strlen(addsumfiles(0))-17,17)
    sumlist=addsumfiles
ENDIF ELSE newsum=0

for n=0,n_Elements(sumlist)-1 do begin
    sumlines=strarr(6000*31l) & d=0l & line=''
    IF newsum THEN zs=where(sn eq asn(n)) ELSE zs=n
    ; open and read in sumfile which matches addsumfile
    openr,2,sumfiles(zs)
    while (not eof(2)) do begin
    	readf,2,line
      	sumlines(d)=line
      	d=d+1
    endwhile   
    close,2
    IF newsum THEN BEGIN
    	openr,1,addsumfiles(n)
    	readf,1,line ;first two lines are the headers
    	readf,1,line
    	; open and read addsumfile
    	while (not eof(1)) do begin
    	    readf,1,line
    	    d2=where(strmid(sumlines,0,21) eq strmid(line,0,21))  ;always replace with .new summary line if file exists
    	    ; if addsumfile filename matches filename from sumfile, replace sumline with line from addsumfile
    	    if(d2(0) gt -1)then sumlines(d2)=line 
    	    ; else append the sumlines list
    	    if(d2(0) eq -1)then begin
    	    	sumlines(d)=line
    	    	d=d+1
      	    endif
    	endwhile   
    	close,1
	fname=asn[n]
    	syscmd='/bin/rm -f '+addsumfiles(n)
	print,syscmd
	wait,2
    	spawn,syscmd,/sh
    ENDIF ELSE fname=sn[n]
    d=d-1
    ; new list of rows
    header=sumlines[0:1]
    sumlines=sumlines[2:d]
    order=sort(sumlines)
    unq=uniq(sumlines[order])
    newsumlines=[header,sumlines[order[unq]]]

    ;write the sorted file
    outfile='/tmp/'+fname+'.new'
    openw,3,outfile,width=200
    for np=0l,long(n_elements(newsumlines)-1) do printf,3,newsumlines(np)
    close,3
    ;syscmd='/bin/rm -f '+sumfiles(zs)
    ;spawn,syscmd
    syscmd='mv '+outfile+' '+sumfiles(zs)
    print,syscmd
    spawn,syscmd
  

endfor


end
