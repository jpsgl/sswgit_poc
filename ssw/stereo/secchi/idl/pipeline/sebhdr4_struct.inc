;*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO
;*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Thu May 19 15:11:05 2005
; $Id: sebhdr4_struct.inc,v 1.2 2006/02/14 22:37:46 nathan Exp $
print,'Running sebhdr4_struct.inc'

char   = 0UB 
dble = 0d
;  /* $Workfile: $
;  */
uint8 = 0UB
uint16 = 0US
uint32 = 0UL
int8 = 0B
int16 = 0S
int32 = 0L
seconds_in_one_year = 31536000
seconds_in_one_day = 86400
seconds_in_one_hour = 3600
seconds_in_one_minute = 60
;  //                                (Jan 1 '58 to Jan 1'70)
seconds_from_epoch_to_1970 = 378691200
;  //                                (Jan 1 '58 to Jan 1'70) + 10 years + 2 leap days
;seconds_since_epoch_to_dos = (
dos_epoch_year = 1980
dos_epoch_month = 1
dos_epoch_day = 1
;  // FPS Table structures and enumerations
;  //
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 22 bytes
sfpspztcoef = { sfpspztcoef,$
          ycoordxform:	int16,$
          zcoordxform:	int16,$
          voltpredict:	replicate(int16,5),$
          pidcoe:	replicate(int16,4) }
;  // structure size is 66 bytes
sgtfpscoefs = { sgtfpscoefs,$
          pz:	replicate(sfpspztcoef,3) }
;  ///////////////////////////////////////////////////////////
;  // EUVI FPS Image Header structures and enumerations
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 144 bytes
sgtfpsimagehdr = { sgtfpsimagehdr,$
          imagenum:	uint16,$
          yoffset:	int16,$
          zoffset:	int16,$
          spare1:	uint16,$
          numfpssamples:	uint32,$
          fpsysum:	int32,$
          fpszsum:	int32,$
          fpsysquare:	int32,$
          fpszsquare:	int32,$
          pzterrsum:	replicate(int32,3),$
          pzterrsquare:	replicate(int32,3),$
          pztdacsum:	replicate(int32,3),$
          pztdacsquare:	replicate(int32,3),$
          fpscoefs:	sgtfpscoefs,$
          spare2:	uint16 }
;  #ifdef VER4
n_img_proc_cmd = 20
;  /*//////////////////////////////////////////////////////////////////////
;  */
;  /** headers present ( 0-base, 1-base+nominal, 2-all 3 )
;  Determined by IP logic */
;  /** Actual exp duration 24 bits with LSB = 4 usec
;  hardware timer and converted to 4 usec units. */
;  /** Actual for 2nd exposure duration ( 24 bits with LSB = 4 usec )
;  hardware timer and converted to 4 usec units. */
;  /** time (UTC) of actual exposure (Shutter command) NOT including
;  lt_offset */
;  /** time (UTC) of actual exposure #2 (Shutter command) NOT including
;  lt_offset */
m_baseheader = { m_baseheader,$
          filename:	replicate(char,13),$
          eheadertypeflag:	uint8,$
          version:	uint8,$
          imgseq:	uint8,$
          numinseq:	uint8,$
          sumrow:	uint8,$
          sumcol:	uint8,$
          actualfilterposition:	uint8,$
          actualpolarposition:	uint8,$
          actualpolarposition2:	uint8,$
          sebxsum:	uint8,$
          sebysum:	uint8,$
          ipcmdlog:	replicate(uint8,n_img_proc_cmd),$
          osnumber:	uint16,$
          critevent:	uint16,$
          imgctr:	uint16,$
          telescopeimgcnt:	uint16,$
          meanbias:	int32,$
          stddevbias:	uint32,$
          actualexpduration:	uint32,$
          actualexpduration_2:	uint32,$
	  padding:	replicate(uint8,4),$
          actualexptime:	dble,$
          actualexptime_2:	dble }
;  /*/////////////////////////////////////////////////////////////////////
;  */
;  /** LED ID used or 0=no led used
;  Physical location determines mechanism sequence */
;  /*////
;  */
;  /** Last status known of the spacewire interface to the CEB based
;  on codes defined as specified in enum CAMERA_INTERFACE_STATUS */
;  /** Last status known of the CCD interface within the CEB
;  based on codes as defined by enum CAMERA_PROGRAM_STATE */
;  /** Commanded exposure duration in units of 1.024 msec ( max of 67.1 seconds)
;  */
;  /** Commanded 2nd exposure duration in units of 1.024 msec
;  */
;  /*//////////////////////////////////////////////////////////////////////
;  */
;  /*//////////////////////////////////////////////////////////////////////
;  */
;  /** Instrument uses a double format for time from epoch 1958 Jan 01
;  00:00:00 UTC time of exposure uploaded NOT including lt_offset */
;  /*////
;  */
;  /** Readback of actual shutter open time (when the opening edge of the
;  source: mechanism readback*/
;  /** Readback of actual shutter close time (when the closing edge of the
;  source: mechanism readback*/
;  /** Readback of actual shutter open time #2 (when the opening edge of
;  source: mechanism readback*/
;  /** Readback of actual shutter close time #2(when the closing edge of
;  source: mechanism readback*/
;  /** State of door during exposure
;  */
;  /*////////////////////////////////////////////////////////////////////
;  */
;  /*////////////////////////////////////////////////////////////////////
;  */
;  /** Time (UTC) of actual start of the CCD clear operation.
;  Specifically, the start of the CEB command series to run the clear table. */
;  /** Time (UTC) of actual start of the image retrieve
;  Specifically, the start of the CEB command series to run the readout sequence. */
m_nominalheader = { m_nominalheader,$
          platformid:	uint8,$
          telescopeid:	uint8,$
          imagetype:	uint8,$
          cmdledmode:	uint8,$
          ipcmdcnt:	uint8,$
          cebintfstatus:	uint8,$
          ccdintfstatus:	uint8,$
          sync:	uint8,$
          cmdexpduration:	uint16,$
          cmdexpduration_2:	uint16,$
          campaignset:	uint16,$
          offset:	uint16,$
          gain:	uint8,$
          gainmode:	uint8,$
          clrtableid:	uint8,$
          readouttableid:	uint8,$
          p1col:	uint16,$
          p1row:	uint16,$
          p2col:	uint16,$
          p2row:	uint16,$
	  padding1:	replicate(uint8,4),$
          cmdexptime:	dble,$
          lighttraveloffsettime:	uint32,$
          imgbufferoffset:	uint8,$
          cmdfilterposition:	uint8,$
          cmdpolarposition:	uint8,$
          cmdpolarposition_2:	uint8,$
          cmdledpulses:	uint32,$
          preexpscstatus:	uint16,$
          postexpscstatus:	uint16,$
          actualopentime:	uint32,$
          actualclosetime:	uint32,$
          actualopentime_2:	uint32,$
          actualclosetime_2:	uint32,$
          actualshutterposition:	uint8,$
          actualshutterdirection:	uint8,$
          actualfilterdirection:	uint8,$
          actualpolardirection:	uint8,$
          actualdoorposition:	uint8,$
          ipfunction:	uint8,$
          ipcmd:	replicate(uint8,n_img_proc_cmd),$
	  padding2:	replicate(uint8,2),$
          actualreadouttime:	uint32,$
          ipprocessingtime:	uint32,$
	  padding3:	replicate(uint8,4),$
          actualccdclearstarttime:	dble,$
          actualimageretrievestarttime:	dble }
;  /*//////////////////////////////////////////////////////////////////////
;  */
;  /*////////////////////////////////////////////////////////////////////
;  */
m_extendedheader = { m_extendedheader,$
          usefps:	uint8,$
          actualfpsmode:	uint8,$
          actualscfinepointmode:	uint8,$
          cmdquadposition:	uint8,$
          fpsdata:	sgtfpsimagehdr }
;  #else

