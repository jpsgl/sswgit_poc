function getsccsecpix, scch, aorb, UPDATEHEADER=updateheader, FAST=fast, SILENT=silent, ARCSEC=arcsec
;+
; $Id: getsccsecpix.pro,v 1.15 2008/05/02 17:09:59 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : getsccsecpix
;               
; Purpose   : return platescale/subtense/cdelt for input header or telescope
;               
; Explanation: 
;               
; Use       : IDL> cdelt=getsccsecpix(scch) 
;    
; Inputs    : 	scch	STC	header structure -or-
;		scch 	STRARR	FITS header -or-
;   	    	scch	STRING	detector
;
; Optional Inputs:  aorb    STRING  'a/A' or 'b/B' (if scch is not a header)
;               
; Outputs   : arcsec/pixel FLOAT or 2 element FLTARR
;
; Keywords  :	UPDATEHEADER	If set then update CDELT*, PV*, HISTORY in scch
;   	    	/FAST	    Skip waits
;   	    	/ARCSEC     Always return value in units of arcsec/pixel
;               
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: detector, obsrvtry, sumrow, sumcol, and ipsum must be defined 
;		in input structure or array
;
; Side effects: 
;               
; Category    : pipeline, calibration
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2006
;               
; $Log: getsccsecpix.pro,v $
; Revision 1.15  2008/05/02 17:09:59  nathan
; added /SILENT option
;
; Revision 1.14  2008/04/17 15:14:01  nathan
; update values (bug 302)
;
; Revision 1.13  2007/10/29 19:02:28  nathan
; accomodate string input for tel and aorb
;
; Revision 1.12  2007/09/27 22:19:06  nathan
; use SUMMED not IPSUM for scaling
;
; Revision 1.11  2007/09/20 18:27:28  nathan
; updated COR2 platescl/cdelt
;
; Revision 1.10  2007/04/16 16:16:02  nathan
; adjust COR1 values from 4/3/07
;
; Revision 1.9  2007/03/20 19:26:09  nathan
; update EUVI-A platescl
;
; Revision 1.8  2007/03/13 19:12:04  secchib
; nr - do not wait if /fast
;
; Revision 1.7  2007/03/01 22:46:29  nathan
; update HI values; fix arcsec correction
;
; Revision 1.6  2007/01/12 21:41:36  nathan
; Doh!
;
; Revision 1.5  2007/01/12 21:41:03  nathan
; fix cdelt (bug 45) by using 2^(ipsum-1) not ipsum
;
; Revision 1.4  2006/10/04 17:19:47  nathan
; account for summing
;
; Revision 1.3  2006/09/20 21:04:09  nathan
; More details for HI from W.Thompson; fix header update
;
; Revision 1.2  2006/09/18 14:10:22  nathan
; add HI and /UPDATEHISTORY
;
; Revision 1.1  2006/09/13 21:23:53  nathan
; for initial header population of CDELT
;
;-            

scchtype=datatype(scch) 
IF scchtype EQ 'STC' THEN BEGIN
	tel=scch.detector
	aorb=strmid(scch.obsrvtry,7,1)
	sumcol=scch.sumcol
	sumrow=scch.sumrow
	ipsum=scch.ipsum
	dimsum=scch.summed
ENDIF ELSE $
IF n_elements(scch) GT 1 THEN BEGIN
	tel=	fxpar(scch,'DETECTOR')
	aorb=strmid(fxpar(scch,'OBSRVTRY'),7,1)
	sumcol=	fxpar(scch,'SUMCOL')
	sumrow=	fxpar(scch,'SUMROW')
	ipsum=	fxpar(scch,'IPSUM')
	dimsum= fxpar(scch,'SUMMED')
	
ENDIF ELSE BEGIN
	tel=trim(strupcase(scch))
	aorb=strupcase(aorb)
	IF ~keyword_set(SILENT) THEN message,'No summing correction applied.',/info
ENDELSE

info="$Id: getsccsecpix.pro,v 1.15 2008/05/02 17:09:59 nathan Exp $"
len=strlen(info)
histinfo=strmid(info,1,len-2)
platescl=0. 	; Error
pv2_1=0D

CASE tel OF
'COR1': CASE aorb OF
	'A': platescl=3.75215	; arcsec/pixel
	'B': platescl=3.75088	;  
    	; from cor1_point.pro,v 1.1  2007/04/03 18:52:32 thompson
	ELSE:
	ENDCASE
'COR2': CASE aorb OF
	'A': platescl=14.7	;arcsec/pixel
	'B': platescl=14.7	 
	; Bug 232, 9/20/07
	ELSE:
	ENDCASE
'EUVI': CASE aorb OF
	'A': platescl=1.59*0.9986   	; from euvi_point.pro,v 1.1
	'B': platescl=1.59 
	; J-P Wuelser, 9/13/06 email
	ELSE:
	ENDCASE
; HI numbers from get_hi_params.pro,v 1.4, calibrated values
'HI1': CASE aorb OF
    	'A': 	BEGIN
    		platescl=   0.009978  	; deg/pixel (binned value/2)
		pv2_1= 0.146		; AZP parameter
    	    	END
	'B':	BEGIN
    		platescl=   0.009963  	; deg/pixel (binned value/2)
		pv2_1= 0.1581		; AZP parameter
    	    	END
	ELSE:
	ENDCASE
'HI2': CASE aorb OF
    	'A': 	BEGIN
    		platescl=   0.033927  	; deg/pixel
		pv2_1= 0.820		; AZP parameter
    	    	END
	'B':	BEGIN
    		platescl=   0.033833  	; deg/pixel
		pv2_1= 0.65062		; AZP parameter
    	    	END
	ELSE:
	ENDCASE
ELSE: print,'%% GETSCCSECPIX: Error - Telescope "',tel,'" not found'
ENDCASE

IF strmid(tel,0,2) EQ 'HI' and ~keyword_set(ARCSEC) THEN BEGIN
    divisor=1. 
    IF ~keyword_set(SILENT) THEN print,'CDELT units are degrees '
ENDIF ELSE BEGIN
    divisor=3600.
    IF ~keyword_set(SILENT) THEN print,'CDELT units are arcsec '
ENDELSE
IF keyword_set(UPDATEHEADER) THEN BEGIN
    IF scchtype EQ 'STC' THEN BEGIN
    	print,'updating header'
    	scch.cdelt1=(2^(dimsum-1))*platescl
    	scch.cdelt2=(2^(dimsum-1))*platescl
    	scch.cdelt1a=-(2^(dimsum-1))*platescl/divisor
    	scch.cdelt2a=(2^(dimsum-1))*platescl/divisor
	scch.pv2_1=pv2_1
	scch.pv2_1a=pv2_1
	scch=scc_update_history(scch, histinfo)
    ENDIF ELSE print,'%% GETSCCSECPIX: Warning - /UPDATEHEADER not implemented for FITS header'
ENDIF	

IF strmid(tel,0,2) EQ 'HI' and keyword_set(ARCSEC) THEN multip=3600. ELSE multip=1.
IF scchtype EQ 'STR' THEN return,platescl*multip
;help,sumcol,ipsum,platescl
IF sumrow NE sumcol THEN return,[sumcol,sumrow]*(2^(ipsum-1))*platescl ELSE $
return, (2^(dimsum-1))*platescl

end
