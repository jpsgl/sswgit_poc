pro check_stereo_ah, d1,d2,NDAYS=ndays, AHEAD=ahead, BEHIND=behind
;
; $Id: check_stereo_ah.pro,v 1.1 2015/08/18 15:12:44 nathan Exp $
;
; Purpose: show available range of attitude history
;
; Optional Inputs:
;		D1	start date 
;		D2	end date
;		None	D1=today
;
; Keywords:
;		NDAYS=	How many days back to ago from D1
;
; $Log: check_stereo_ah.pro,v $
; Revision 1.1  2015/08/18 15:12:44  nathan
; diagnosis ATTFILE not found
;


IF n_params() GE 1 THEN u1=nrlanytim2utc(d1) ELSE get_utc,u1
u0=u1
IF keyword_set(NDAYS) THEN u0.mjd=u1.mjd-ndays
IF n_params() EQ 2 THEN u1=nrlanytim2utc(d2)
IF keyword_set(BEHIND) THEN aorb='B' ELSE aorb='A'
ti=maketimearr(u0,u1,0.25,/hours)
n=n_elements(ti)
ahf=strarr(n)
wf=intarr(n)

FOR i=0,n-1 DO ahf[i]=get_stereo_att_file(ti[i],aorb)

w=where(ahf NE '')
wf[w]=1

utplot,ti,wf,psym=-1

end
