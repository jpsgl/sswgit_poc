function def_scc_enums
;+
;$Id: def_scc_enums.pro,v 1.3 2006/01/27 20:06:36 nathan Exp $
; Project     : STEREO - SECCHI
;                   
; Name        : get_scc_enums
;               
; Purpose     : initialize and set values in a structure to account for 
;		FSW type definitions that are referenced in the SEB header
;               
; Use         : IDL> enums = get_scc_enums()
;
; Inputs      : none
;               
; Outputs     : structure of type scc_enums
;               
; Keywords    : 
;
; Restrictions: 
;
; Calls       : 
;
; Category    : Data_Handling, I_O, pipeline
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL, 2004.09.08
;               
; $Log: def_scc_enums.pro,v $
; Revision 1.3  2006/01/27 20:06:36  nathan
; use SERIES instead of SEQ for image type
;
; Revision 1.2  2005/05/28 20:25:50  secchib
; add edoornums
;
; Revision 1.1  2005/02/02 16:26:02  nathan
; moved to new directory
;
; Revision 1.1  2004/09/12 02:18:37  nathan
; first draft
;
;
;-            

scc_enums = {scc_enums, $

;sysinc/image/cCameraSetupTable.h:enum 	ePlatformTypes
;
;enum ePlatformTypes
;{
;      STEREO_A = 1
;    , STEREO_B = 2
;};

ePlatformTypes: $
	[ $
	  'STEREO' $
	, 'STEREO_A' $
	, 'STEREO_B'], $

;sysinc/image/cImageHdr.h:enum 		eHeaderType
;sysinc/image/cImageHdr.h:enum 		eImageTypeCodes
;
;/* Observation Progs */
;enum eImageTypeCodes
;{
;      OP_NORMAL  =  0  // clr CCD, shutter flipped, readout
;    , OP_DARK    =  1  // clr CCD, wait, readout
;    , OP_DOUBLE  =  2  // clr CCD, shutter flipped, move wheel, shutter flipped, readout 
;    , OP_LED     =  3  // clr CCD, LED's pulsed, readout
;    , OP_CONTIN  =  4  // shutter opened, clr CCD, readout, shutter closed
;    , OP_SEQ     =  5  // a series of normal types
;    , OP_DEFAULT =  OP_NORMAL
;};
eImageTypeCodes : $
[     'NORMAL' $	
    , 'DARK' $
    , 'DOUBLE' $ 
    , 'LED' $    
    , 'CONTIN' $ 
    , 'SERIES' $    
    , 'NORMAL' ], $

;/* Header Flag Values */
;enum eHeaderType
;{
;      SPACEWEATHER  =  0  
;    , NOMINAL       =  1  
;    , EXTENDED      =  2  
;};
eHeaderType : $
[     'SPACEWEATHER'  $  
    , 'NOMINAL'       $  
    , 'EXTENDED'      ]  , $

;sysinc/swire/cCameraInterface.h:enum 	CAMERA_INTERFACE_STATUS 
;sysinc/swire/cCameraInterface.h:enum 	CAMERA_PROGRAM_STATE
;
;/////
;//  Overall  setup / configuration of the camera interface hardware
;//
;enum CAMERA_PROGRAM_STATE
;{
;    CAMERA_NOT_READY = 0
;  , CAMERA_READY   
;  , CAMERA_ERROR   
;  , CAMERA_UNKNOWN_STATE
;};
CAMERA_PROGRAM_STATE : $
[   'CAMERA_NOT_READY' $
  , 'CAMERA_READY'   $
  , 'CAMERA_ERROR'   $
  , 'CAMERA_UNKNOWN_STATE' ], $

;/////
;//  Camera Interface error codes
;//
;enum CAMERA_INTERFACE_STATUS 
;{
;    SUCCESSFUL_RESPONSE      = 0  // No errors occurred 
;  , NO_COMMANDED_ACTION      = -1 // No activity has yet occurred
;  , START_NOT_ACKNOWLEDGED   = 1  // Start sequence command not acknowledged
;  , STOP_NOT_ACKNOWLEDGED    = 2  // Stop sequence command not acknowledged
;  , WRITE_NOT_ACKNOWLEDGED   = 3  // Data byte write to the Camera interface failed 
;  , INVALID_COMMAND_RESPONSE = 4  // Invalid command received by Camera box
;  , PARITY_ERROR_RESPONSE    = 5  // Parity error detected by Camera box
;  , NO_STOP_BIT_RESPONSE     = 6  // No stop bit error detected by Camera box
;  , SEND_ON_LINK_FAILED      = 7  // Unable to send message on link
;  , READ_ON_LINK_FAILED      = 8  // Unable to perform read on link
;  , CAMERA_ADDRESS_NOT_ACKNOWLEDGED = 9 // Address error detected by Camera box
;  , READ_ADDRESS_NOT_ACKNOWLEDGED = 10 // Read address for sequence not acknowledged
;  , INVALID_OFFSET_VALUE     = 11 // Value of Offset is not within limits
;  , INVALID_GAIN_VALUE       = 12 // Value of Gain is not within limits
;  , INVALID_MODE_VALUE       = 13 // Value of Mode is not within limits
;  , OFFSET_WRITE_FAILED      = 14 // CDS programming of offset failed
;  , GAIN_WRITE_FAILED        = 15 // CDS programming of gain failed
;  , MODE_WRITE_FAILED        = 16 // CDS programming of mode failed
;  , FLIGHT_TABLE_NOT_AVAILABLE = 17 // Init of default flight tables failed
;  , WAVE_TABLE_PATCH_FAILED    = 18 // Patching wave table data to camera failed
;  , READOUT_TABLE_PATCH_FAILED = 19 // Patching readout table data to camera failed
;};
CAMERA_INTERFACE_STATUS : $
;  , NO_COMMANDED_ACTION      = -1 // No activity has yet occurred
[   'SUCCESSFUL_RESPONSE'	$ 
  , 'START_NOT_ACKNOWLEDGED'	$
  , 'STOP_NOT_ACKNOWLEDGED'	$
  , 'WRITE_NOT_ACKNOWLEDGED'	$
  , 'INVALID_COMMAND_RESPONSE'	$
  , 'PARITY_ERROR_RESPONSE'	$
  , 'NO_STOP_BIT_RESPONSE'	$
  , 'SEND_ON_LINK_FAILED'	$
  , 'READ_ON_LINK_FAILED'	$
  , 'CAMERA_ADDRESS_NOT_ACKNOWLEDGED'	$
  , 'READ_ADDRESS_NOT_ACKNOWLEDGED'	$
  , 'INVALID_OFFSET_VALUE'	$
  , 'INVALID_GAIN_VALUE'	$
  , 'INVALID_MODE_VALUE'	$
  , 'OFFSET_WRITE_FAILED'	$
  , 'GAIN_WRITE_FAILED'	$
  , 'MODE_WRITE_FAILED'	$
  , 'FLIGHT_TABLE_NOT_AVAILABLE'	$
  , 'WAVE_TABLE_PATCH_FAILED'	$
  , 'READOUT_TABLE_PATCH_FAILED'	], $

;sysinc/hkpdev.h:} 			eHkpCalLedColor;
;sysinc/hkpdev.h:} 			eHkpInstrument;
;
;////////////////////////////////////////////////////////
;// Define Calibration LED Colors
;////////////////////////////////////////////////////////
;typedef enum
;{
;    HKP_CAL_LED_NONE = 0,
;    HKP_CAL_LED_RED,
;    HKP_CAL_LED_PURPLE,
;    HKP_CAL_LED_BLUE,
;    HKP_CAL_NUM_COLORS    // This identifier must be last in the list!
;} eHkpCalLedColor;
eHkpCalLedColor : $
[   'NONE',	$
    'RED',	$
    'PURPLE',	$
    'BLUE',	$
    'NUM_COLORS']    , $

;////////////////////////////////////////////////////////
;// Define Instrument identifiers
;////////////////////////////////////////////////////////
;typedef enum
;{
;    HKP_NONE = 0,
;    HKP_COR_2,
;    HKP_COR_1,
;    HKP_EUVI,
;    HKP_HI_2,
;    HKP_HI_1,
;    HKP_NUM_INSTRUMENTS    // This identifier must be last in the list!
;} eHkpInstrument;
eHkpInstrument : $
[   'NONE',	$
    'COR2',	$
    'COR1',	$
    'EUVI',	$
    'HI2',	$
    'HI1',	$
    'NUM_INSTRUMENTS' ]    , $
   
; sysinc/image/cImageHdr.h
;/* Header Door State Values */
;enum eDoorStatus
;{
;      DOOR_CLOSED      =  0
;    , DOOR_IN_TRANSIT  =  1
;    , DOOR_OPEN        =  2
;    , ENCODER_OFF      =  3
;    , ENCODER_ERROR    =  4
;    , NO_DOOR_DATA     =  255
;};
eDoorStatus : $
[   'CLOSED',	$
    'IN_TRANSIT',	$
    'OPEN ',	$
    'OFF',	$
    'ERROR',    $
    replicate('invalid',250), $
    'NO_DATA' ]   }

return, scc_enums
 
end

