;+
;$Id: scc_event_track.pro,v 1.16 2012/11/28 19:00:34 nathan Exp $
; Project     : STEREO - SECCHI
;                   
; Name        : scc_event_track
;               
; Purpose     : log IP CME events
;               
; Use         : IDL> scc_event_track,pipeline
;
; Inputs      : none
;               
; Outputs     : create event log files and send notification of IP CME events
;               
; Opt. Outputs:
; 
; Keywords    : NOEVENT_NOTIFYSCIP if set will nopt send email notification
;
; Restrictions: Recquires definition of scc_reduce COMMON block and 
;   	    	populating of SEB base header.
;
; Category    : Data_Handling, I_O, pipeline
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, 2011-01-04
;               
; $Log: scc_event_track.pro,v $
; Revision 1.16  2012/11/28 19:00:34  nathan
; change email from cronus.nrl to nrl
;
; Revision 1.15  2012/02/23 13:33:50  mcnutt
; corrected evmsg comment
;
; Revision 1.14  2012/02/21 20:30:11  mcnutt
; corrected error in last change
;
; Revision 1.13  2012/02/21 20:09:19  mcnutt
; checks yr before writting estimated event time to log file
;
; Revision 1.12  2012/01/12 17:39:18  nathan
; make sure sevh_arry has at least 5 elements
;
; Revision 1.11  2011/11/14 22:30:42  nathan
; subtract 1 from evcol to match event message dat4
;
; Revision 1.10  2011/06/27 20:34:14  secchib
; nr - handle out of order and duplicate entries in event log
;
; Revision 1.9  2011/03/25 16:25:17  mcnutt
; added time check to skip loggin of previoulsy processed images
;
; Revision 1.8  2011/02/04 16:25:43  mcnutt
; reads in previous month long file if event occurs on first image of the monthly log
;
; Revision 1.7  2011/02/02 13:33:14  mcnutt
; corrected estimated time
;
; Revision 1.6  2011/01/25 14:29:25  mcnutt
; corrected error in event email and evcount based on B 201101 log
;
; Revision 1.5  2011/01/11 19:46:11  mcnutt
; corrected mail2 removed test config
;
; Revision 1.4  2011/01/11 19:45:01  mcnutt
; added lz option and define_event_struct + more
;
; Revision 1.3  2011/01/04 19:50:16  mcnutt
; add more info to event occured message
;
; Revision 1.2  2011/01/04 18:46:05  mcnutt
; added SC to sav file
;
; Revision 1.1  2011/01/04 18:34:37  mcnutt
; added to pipeline for IP V1.11
;

pro define_event_struct,pipeline,evtlog=evtlog
common scc_event, sevh_arry, rlen
common scc_reduce

     sevh={filename:'',$
           fileorig:'',$
           date_obs:'',$
           CRITEVT:'',$
           seconds:double(0.0),$
           nblocks:0,$
           evcount:-1,$
           event:'F',$
           evrow:0,$
           evcol:0}
	   
      if pipeline eq 'spw' then sevh_arry=replicate(sevh,rlen+1) $
      else begin
          if keyword_set(evtlog) then evtlogname=evtlog else $
	    evtlogname= getenv('SCC_ATTIC')+'/event/scc'+strmid(scch.filename,strpos(scch.filename,'.f')-1,1)+'_'+strmid(scch.filename,0,6)+'_event.log'
          tmp=file_search(evtlogname)
          if tmp eq '' then sevh_arry=replicate(sevh,rlen+1) $
	  else begin
            lines=readlist(evtlogname)
            split=strsplit(lines(0))
            sevh_arry=replicate(sevh,n_Elements(lines))
            sevh_arry.filename=strmid(lines,split(0),split(1)-1)
            sevh_arry.fileorig=strmid(lines,split(1),split(2)-split(1))
            sevh_arry.date_obs=strmid(lines,split(2),split(3)-split(2))
            sevh_arry.CRITEVT= strmid(lines,split(3),split(4)-split(3))
            sevh_arry.seconds= double(strmid(lines,split(4),split(5)-split(4)))
            sevh_arry.nblocks= fix(strmid(lines,split(5),split(6)-split(5)))
            sevh_arry.evcount= fix(strmid(lines,split(6),split(7)-split(6)))
            sevh_arry.event=   strmid(lines,split(7),split(8)-split(7))
            sevh_arry.evrow=   fix(strmid(lines,split(8),split(9)-split(8)))
            sevh_arry.evcol=   fix(strmid(lines,split(9),strlen(lines(0))-split(9)))
	      ; handle out of order and duplicate entries

	      s=sort(sevh_arry.filename)
	      u=uniq(sevh_arry[s].filename)
	      IF n_elements(u) GT rlen THEN sevh_arry=sevh_arry[s[u]]
          endelse
      endelse
end

pro scc_event_track,fits_hdr,pipeline,NOEVENT_NOTIFY=NOEVENT_NOTIFY
common scc_event
common scc_reduce
rlen=4

if pipeline eq 'spw' then begin

  evtsavefile=getenv('SCC_LOG')+'/scc'+strmid(scch.filename,strpos(scch.filename,'.f')-1,1)+'_event_count.sav'
  tmp=file_search(evtsavefile)
  if tmp eq '' then define_event_struct,pipeline else restore,evtsavefile

  nblocks=0
  evcount=0
  if(strlowcase(strmid(scch.CRITEVT,2,1)) eq 'e')then begin 
     eventtype='E'
     eventcal=basehdr.CRITEVENT-(57344+1999)
     scch.EVROW=eventcal/34
     scch.EVCOL= eventcal - (34 * scch.EVROW) - 1
     scch.evcount=sevh_arry(rlen).evcount ; events count is not in basehdr
  endif
  if(strlowcase(strmid(scch.CRITEVT,2,1)) eq 'd')then begin 
     eventtype='D'
     reads,strmid(string(basehdr.CRITEVENT,'(b)'),7+4,7,/reverse),format='(b)',evcount
     scch.evcount=fix(evcount)
     reads,strmid(string(basehdr.CRITEVENT,'(b)'),4,5,/reverse),format='(b)',nblocks
     scch.EVROW=0
     scch.EVCOL=0
  endif

  spwseconds=utc2tai(anytim2utc(scch.date_obs))
  if spwseconds le sevh_arry(rlen).seconds then goto,allreadyprocessed ; spw image was previoulsy processed do not log again.

  sevh_arry(0:rlen-1)=sevh_arry(1:rlen)
  sevh_arry(rlen)={filename:scch.filename,$
           fileorig:scch.fileorig,$
           date_obs:scch.date_obs,$
           CRITEVT:scch.CRITEVT,$
           seconds:spwseconds,$
           nblocks:nblocks,$
           evcount:fix(scch.evcount),$
           event:sevh_arry(rlen-1).event,$
           evrow:scch.EVROW,$
           evcol:scch.EVCOL}

  mailevent=0

 ; SSR2 renabled reset EVENT flag
  if sevh_arry(rlen).evcount ne sevh_arry(rlen-1).evcount and $
     sevh_arry(rlen).event eq 'T' and $
     strmid(sevh_arry(rlen-1).CRITEVT,2,1) ne 'E' then sevh_arry(rlen).event='F' 

;event detected image set notify flag
  if EVENTtype EQ 'E' then begin  
     mailevent=1 & sevh_arry(rlen).event='T'
	openw,evlun,'evmsg.txt',/get_lun
	    printf,evlun,'******* CME EVENT DETECTED *******'
	    printf,evlun,''
	    printf,evlun,'pipeline=',string(pipeline,'(a8)')
	    printf,evlun,'obsrvtry=    ',scch.obsrvtry
	    printf,evlun,'filename=    ',scch.filename
	    printf,evlun,'date_obs=    ',scch.date_obs
	    printf,evlun,'fileorig=    ',scch.fileorig
	    printf,evlun,'ip_00_19=',scch.ip_00_19
	    printf,evlun,''
	    printf,evlun,'event=   ',string(sevh_arry(rlen).event,'(a8)')
	    printf,evlun,'evcount= ',string(scch.evcount,'(a8)')
	    printf,evlun,'evrow=   ',string(scch.evrow,'(i8)')
	    printf,evlun,'evcol=   ',string(scch.evcol,'(i8)')
	    critevent=string(basehdr.CRITEVENT,'(z4)')
	    printf,evlun,'critEvent=',string(critevent,'(a7)')
	 close,evlun
	 free_lun,evlun
  endif

; SSR2 disabled and dectect image missed set notify flag and estimate event time based on previous image counts and times
  if sevh_arry(rlen).evcount eq sevh_arry(rlen-1).evcount and sevh_arry(rlen).event eq 'F' then begin

     mailevent=1 & sevh_arry(rlen).event='T'

     t1=sevh_arry(rlen-3).seconds & t2=sevh_arry(rlen-2).seconds
     cnt=   sevh_arry(rlen-2).evcount  - sevh_arry(rlen-3).evcount 
     if cnt lt 0 then cnt=cnt+128
     cadence=(t2-t1)/cnt
     cnt2=   (sevh_arry(rlen).evcount)  - sevh_arry(rlen-2).evcount 
     if cnt2 lt 0 then cnt2=cnt2+128
     sectime=t2+(cadence*cnt2)
     esttime=utc2str(tai2utc(sectime))

     openw,evlun,'evmsg.txt',/get_lun
	    printf,evlun,'******* CME EVENT HAS OCCURED *******'
	    printf,evlun,''
	    printf,evlun,'pipeline=',string(pipeline,'(a8)')
	    printf,evlun,'obsrvtry=    ',scch.obsrvtry
	    printf,evlun,'Estimated Time=    ',esttime
	    printf,evlun,'Images used in estimate :
	    printf,evlun,'Image T1       Time= ',sevh_arry(rlen-3).date_obs,'  Count= ',string(sevh_arry(rlen-3).evcount ,'(i3)')
	    printf,evlun,'Image T2       Time= ',sevh_arry(rlen-2).date_obs,'  Count= ',string(sevh_arry(rlen-2).evcount ,'(i3)')
	    printf,evlun,'Previous Image Time= ',sevh_arry(rlen-1).date_obs,'  Count= ',string(sevh_arry(rlen-1).evcount ,'(i3)')
	    printf,evlun,'Current Image  Time= ',sevh_arry(rlen).date_obs,'  Count= ',string(sevh_arry(rlen).evcount ,'(i3)')
            if fix(strmid(esttime,0,4)) lt fix(strmid(sevh_arry(rlen).date_obs,0,4))-1 then printf,evlun,'**Event not added to event log - bad time estimate'
	    close,evlun
     free_lun,evlun

; Added missing EVENT image to log file
     if fix(strmid(esttime,0,4)) ge fix(strmid(sevh_arry(rlen).date_obs,0,4))-1 then begin
       filename=strmid(esttime,0,4)+strmid(esttime,5,2)+strmid(esttime,8,2)+'_'+strmid(esttime,11,2)+strmid(esttime,14,2)+strmid(esttime,17,2)+strmid(sevh_arry(rlen).filename,9,10,/reverse)     
       sevh_arry_new=sevh_arry 
       sevh_arry_new(0:rlen-3)=sevh_arry(1:rlen-2) ;shift to make room for missed event image stats
       sevh_arry_new(rlen-2)={filename:filename,$
           fileorig:'MISSING_FILE',$
           date_obs:esttime,$
           CRITEVT:'ESTIMA',$
           seconds:sectime,$
           nblocks:0,$
           evcount:fix(scch.evcount),$
           event:'T',$
           evrow:scch.EVROW,$
           evcol:scch.EVCOL}
       sevh_arry_new(rlen-1).event='T' ; set previous event to true           
       evtlog1= getenv('SCC_ATTIC')+'/event/scc'+strmid(sevh_arry_new(rlen-2).filename,strpos(sevh_arry_new(rlen-2).filename,'.f')-1,1)+'_'+strmid(sevh_arry_new(rlen-2).filename,0,6)+'_event.log'
       evtlog2= getenv('SCC_ATTIC')+'/event/scc'+strmid(sevh_arry_new(rlen).filename,strpos(sevh_arry_new(rlen).filename,'.f')-1,1)+'_'+strmid(sevh_arry_new(rlen).filename,0,6)+'_event.log'
       define_event_struct,'lz',evtlog= evtlog1 ; read in log file to update
       if evtlog1 ne evtlog2 then begin ;event dection occured file of monthly log
          sevh_arry_tmp=sevh_arry
          define_event_struct,'lz',evtlog= evtlog2 ; read in log file to update
          sevh_arry=[sevh_arry_tmp,sevh_arry]
       endif
       z=where(sevh_arry.seconds lt sectime)
       if z(0) gt -1 then sevh_arry_new=[sevh_arry(z),sevh_arry_new(rlen-2:rlen)] 
       if evtlog1 ne evtlog2 then begin ;event dection occured file of monthly log
         openw,evlog,evtlog1,/get_lun
         z=where(strmid(sevh_arry_new.filename,0,6) eq strmid(sevh_arry_new(rlen-2).filename,0,6))
         rlen2=n_Elements(z)
         for rlen1=0,rlen2-1 do printf,evlog,sevh_arry_new(rlen1),format='(a25,1x,a12,1x,a23,1x,a6,1x,e13.7,1x,i3.3,1x,i3.3,1x,a1,1x,i3.3,1x,i3.3)'
         close,evlog
         free_lun,evlog
         z=where(strmid(sevh_arry_new.filename,0,6) ne strmid(sevh_arry_new(rlen-2).filename,0,6))
         sevh_arry_new=sevh_arry_new(z)
       endif
       openw,evlog,evtlog2,/get_lun
       rlen2=n_Elements(sevh_arry_new)
       for rlen1=0,rlen2-2 do printf,evlog,sevh_arry_new(rlen1),format='(a25,1x,a12,1x,a23,1x,a6,1x,e13.7,1x,i3.3,1x,i3.3,1x,a1,1x,i3.3,1x,i3.3)'
       close,evlog
       free_lun,evlog
 
       if rlen2 ge rlen+1 then sevh_arry=sevh_arry_new(rlen2-(rlen+1):rlen2-1) else sevh_arry((rlen+1)-rlen2:rlen)=sevh_arry_new
     endif

  endif

  scch.event=sevh_arry(rlen).event

  save,filename=evtsavefile,sevh_arry

;log all event detection values 
    evtlogname= getenv('SCC_ATTIC')+'/event/scc'+strmid(sevh_arry(rlen).filename,strpos(sevh_arry(rlen).filename,'.f')-1,1)+'_'+strmid(sevh_arry(rlen).filename,0,6)+'_event.log'
    openw,evlog,evtlogname,/append,/get_lun
    printf,evlog,sevh_arry(rlen),format='(a25,1x,a12,1x,a23,1x,a6,1x,e13.7,1x,i3.3,1x,i3.3,1x,a1,1x,i3.3,1x,i3.3)'
    close,evlog
    free_lun,evlog

    IF ~keyword_set(NOEVENT_NOTIFY) and mailevent THEN BEGIN
	 spawn,'cat evmsg.txt',/sh
	 wait,2
	 mail2='sccevent@nrl.navy.mil'
	 spawn,'mailx -s SECCHI-'+aorb+'_Event_Detected_'+strmid(scch.filename,2,6)+' '+mail2+' < evmsg.txt',/sh
    ENDIF

    allreadyprocessed:
endif else begin  ;pipeline = 'lz'

    if datatype(sevh_arry) ne 'STC' then t2=0 else t2=max(sevh_arry.seconds)
    t1=utc2tai(anytim2utc(scch.date_obs))
    if t2 lt t1 then define_event_struct,pipeline
    useev=find_closest(t1(0),sevh_arry.seconds)
    scch.event=sevh_arry(useev).event
    scch.evcount=sevh_arry(useev).evcount
    scch.EVROW=sevh_arry(useev).evrow
    scch.EVCOL=sevh_arry(useev).evcol

endelse

fxaddpar,fits_hdr,'EVENT',scch.event
fxaddpar,fits_hdr,'EVCOUNT',scch.evcount
fxaddpar,fits_hdr,'EVROW',scch.EVROW
fxaddpar,fits_hdr,'EVCOL',scch.EVCOL

end
