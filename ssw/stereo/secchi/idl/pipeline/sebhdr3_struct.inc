;*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO
;*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Tue May  3 12:29:43 2005
; $Id: sebhdr3_struct.inc,v 1.4 2005/05/04 01:50:12 nathan Exp $
print,'Running sebhdr3_struct.inc'

char   = 0UB 
double = 0d
;  /* $Workfile: $
;  */
uint8 = 0UB
uint16 = 0US
uint32 = 0UL
int8 = 0B
int16 = 0S
int32 = 0L
seconds_in_one_year = 31536000
seconds_in_one_day = 86400
seconds_in_one_hour = 3600
seconds_in_one_minute = 60
;  //                                (Jan 1 '58 to Jan 1'70)
seconds_from_epoch_to_1970 = 378691200
;  //                                (Jan 1 '58 to Jan 1'70) + 10 years + 2 leap days
;seconds_since_epoch_to_dos = (
dos_epoch_year = 1980
dos_epoch_month = 1
dos_epoch_day = 1
;  // FPS Table structures and enumerations
;  //
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 22 bytes
sfpspztcoef = { sfpspztcoef,$
          ycoordxform:	int16,$
          zcoordxform:	int16,$
          voltpredict:	replicate(int16,5),$
          pidcoe:	replicate(int16,4) }
;  // structure size is 66 bytes
sgtfpscoefs = { sgtfpscoefs,$
          pz:	replicate(sfpspztcoef,3) }
n_img_proc_cmd = 20
;  ///////////////////////////////////////////////////////////
;  // EUVI FPS Image Header structures and enumerations
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 144 bytes
sgtfpsimagehdr = { sgtfpsimagehdr,$
          imagenum:	uint16,$
          yoffset:	int16,$
          zoffset:	int16,$
          spare1:	uint16,$
          numfpssamples:	uint32,$
          fpsysum:	int32,$
          fpszsum:	int32,$
          fpsysquare:	int32,$
          fpszsquare:	int32,$
          pzterrsum:	replicate(int32,3),$
          pzterrsquare:	replicate(int32,3),$
          pztdacsum:	replicate(int32,3),$
          pztdacsquare:	replicate(int32,3),$
          fpscoefs:	sgtfpscoefs,$
          spare2:	uint16 }
;  ///////
;  //    Definition of image header data sections.
;  // m_baseHeader - defines the space weather header
;  // m_nominalHeader - defines the nominal image data for CORs
;  // m_extendedHeader - defines the additional image data for EUVI
;  ///////////////////////////////////////////////////////////////////////
;  //   Base Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  /** headers present ( 0-base, 1-base+nominal, 2-all 3 )
;  Determined by IP logic */
;  #ifdef VER3
;  #else
;  //        uint32 osNumber;
;  #endif
;  /** Commanded exposure duration in units of 1.024 msec ( max of 67.1 seconds)
;  */
;  /** Commanded 2nd exposure duration in units of 1.024 msec
;  Image type is set to DOUBLE to use this value*/
;  #ifdef VER2
;  //        uint32 meanBias;
;  //        uint32 stddevBias;
;  #endif
;  #ifdef VER3
;  #endif
;  /** Actual exp duration 24 bits with LSB = 4 usec ( Close time minus
;  Open time )*/
;  /** time (UTC) of actual exposure (Shutter command) NOT including
;  lt_offset */
;  /** time (UTC) of actual exposure #2 (Shutter command) NOT including
;  lt_offset */
m_baseheader = { m_baseheader,$
          filename:	replicate(char,13),$
          eheadertypeflag:	uint8,$
          version:	uint16,$
          osnumber:	uint16,$
          critevent:	uint16,$
          imgctr:	uint16,$
          imgseq:	uint16,$
          numinseq:	uint16,$
          telescopeimgcnt:	uint16,$
          sumrow:	uint16,$
          sumcol:	uint16,$
          cmdexpduration:	uint16,$
          cmdexpduration_2:	uint16,$
          actualfilterposition:	uint16,$
          actualpolarposition:	uint16,$
          sebxsum:	uint16,$
          sebysum:	uint16,$
          meanbias:	int32,$
          stddevbias:	uint32,$
          ipcmdlog:	replicate(uint8,n_img_proc_cmd),$
          actualexpduration:	uint32,$
          actualexptime:	double,$
          actualexptime_2:	double }
;  ///////////////////////////////////////////////////////////////////////
;  //   Nominal Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  //   Camera Setup params
;  //
;  /////
;  //  Actuals
;  //
;  /** Last status known of the spacewire interface to the CEB based
;  on codes defined as specified in enum CAMERA_INTERFACE_STATUS */
;  /** Last status known of the CCD interface within the CEB
;  based on codes as defined by enum CAMERA_PROGRAM_STATE */
;  ///////////////////////////////////////////////////////////////////////
;  //   Mechanism Setup / Actuals Data
;  //
;  /** Instrument uses a double format for time from epoch
;  NOT including lt_offset */
;  /** LED ID used or 0=no led used Physical location determines mechanism
;  sequence */
;  /////
;  //  Actuals
;  //
;  /** Readback of actual shutter open time (when the opening edge of the
;  source: mechanism readback*/
;  /** Readback of actual shutter close time (when the closing edge of the
;  source: mechanism readback*/
;  /** Readback of actual shutter open time #2 (when the opening edge of
;  source: mechanism readback*/
;  /** Readback of actual shutter close time #2(when the closing edge of
;  source: mechanism readback*/
;  /** Actual for 2nd exposure duration ( 24 bits with LSB = 4 usec )
;  source: mechanism readback*/
;  /** State of door during exposure
;  */
;  ///////////////////////////////////////////////////////////////////////
;  //   Image Processing Params / Record
;  //
;  ///////////////////////////////////////////////////////////////////////
;  //   Time markers within the Image Taking logic
;  //
;  /** Time (UTC) of actual start of the CCD clear operation.
;  Specifically, the start of the CEB command series to run the clear table. */
;  /** Time (UTC) of actual start of the image retrieve
;  Specifically, the start of the CEB command series to run the readout sequence. */
m_nominalheader = { m_nominalheader,$
          platformid:	uint8,$
          telescopeid:	uint8,$
          imagetype:	uint8,$
          sync:	uint16,$
          campaignset:	uint16,$
          offset:	uint16,$
          gain:	uint16,$
          gainmode:	uint16,$
          clrtableid:	uint16,$
          readouttableid:	uint16,$
          p1col:	uint16,$
          p1row:	uint16,$
          p2col:	uint16,$
          p2row:	uint16,$
          imgbufferoffset:	uint16,$
          cebintfstatus:	uint8,$
          ccdintfstatus:	uint8,$
          cmdexptime:	double,$
          lighttraveloffsettime:	uint32,$
          cmdpolarposition:	uint16,$
          cmdpolarposition_2:	uint16,$
          cmdledmode:	uint8,$
          cmdledpulses:	uint32,$
          preexpscstatus:	uint16,$
          postexpscstatus:	uint16,$
          actualopentime:	uint32,$
          actualclosetime:	uint32,$
          actualopentime_2:	uint32,$
          actualclosetime_2:	uint32,$
          actualexpduration_2:	uint32,$
          actualshutterposition:	uint16,$
          actualshutterdirection:	uint16,$
          actualpolardirection:	uint16,$
          actualdoorposition:	uint16,$
          actualreadouttime:	uint32,$
          ipfunction:	uint16,$
          ipcmd:	replicate(uint8,n_img_proc_cmd),$
          ipcmdcnt:	uint8,$
          ipprocessingtime:	uint32,$
          actualccdclearstarttime:	double,$
          actualimageretrievestarttime:	double }
;  ///////////////////////////////////////////////////////////////////////
;  //   Extended Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  //// Guide Telescope and FPS statistics
;  //// see Guide Telescope SICM for algorithm and variable definitions
;  ////
m_extendedheader = { m_extendedheader,$
          usefps:	uint16,$
          actualfpsmode:	uint16,$
          actualscfinepointmode:	uint16,$
          cmdfilterposition:	uint16,$
          cmdquadposition:	uint16,$
          actualfilterdirection:	uint16,$
          actualquaddirection:	uint16,$
          fpsdata:	sgtfpsimagehdr }
