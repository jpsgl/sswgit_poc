pro sci2hdrs, scifn
;+
; $Id: sci2hdrs.pro,v 1.2 2009/03/04 22:12:01 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : sci2hdrs
;               
; Purpose     : return structure(s) with image header info from raw image files
;               
; Explanation : The program unblocks each file then reads header info into 3 variables
;               
; Use         : IDL> sci2hdrs, listoffiles
;    
; Inputs      : scifn	STRARR	names of raw image files with path
;               
; Outputs     : 3 structures basehdr(s), nom_hdr(s), ext_hdr(s) each of same dimension as input
;   	    	(program does not return values, but stops so user can use them before it returns)
;
; Keywords :	
;
; Calls from LASCO :
;
; Common      : scc_reduce
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline reformat decommutate image telemetry
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Feb 2009
; -              
; Modified    :
;
; $Log: sci2hdrs.pro,v $
; Revision 1.2  2009/03/04 22:12:01  nathan
; added printouts
;
; Revision 1.1  2009/03/02 18:12:42  nathan
; converts raw science files to header structures
;

common scc_reduce,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, resetvars, $
    	    	    ipdat, nom_tlr, prevbasename, readfileidline, wavefileidline, $
		    setupfileidline, exposfileidline, maskfileidline, ipfileidline, icerdiv2flag, $
		    rotblidlines,hbtimes,hbtemps,cebtimes,cebtemps, subdir, date_fname_utc, ispb, islz, isrt, aorb, $
		    iserror, ephemfil, att_file
                    
nf=n_elements(scifn)

unblock_image_hdrs, scifn[0]

IF nf GT 1 THEN BEGIN
    basehdrs=basehdr
    nom_hdrs=nom_hdr
    IF datatype(ext_hdr) NE 'UND' THEN ext_hdrs=ext_hdr
    FOR i=1,nf-1 DO BEGIN
    	unblock_image_hdrs, scifn[i]
     	basehdrs=[basehdrs,basehdr]
	nom_hdrs=[nom_hdrs,nom_hdr]
	IF datatype(ext_hdr) NE 'UND' THEN ext_hdrs=[ext_hdrs,ext_hdr]
    ENDFOR
ENDIF

cmd=nom_hdrs.cmdexptime
cmdu =tai2utc(nom_hdrs.cmdexptime, /nocorrect)
clr =nom_hdrs.actualccdclearstarttime
obs1=basehdrs.actualexptime
obs2=basehdrs.actualexptime_2
ro  =nom_hdrs.actualimageretrievestarttime

dur1=long(basehdrs.actualexpduration)*double(4.0e-6)
dur2=long(basehdrs.actualexpduration_2)*double(4.0e-6)
pol1=basehdrs.actualpolarposition
pol2=basehdrs.actualpolarposition2
cmdd=fix(nom_hdrs.cmdexpduration)
op1 =long(nom_hdrs.actualopentime)
cl1 =long(nom_hdrs.actualclosetime)
op2 =long(nom_hdrs.actualopentime_2)
cl2 =long(nom_hdrs.actualclosetime_2)

help,basehdrs,nom_hdrs,ext_hdrs
n=n_elements(ro)

dou=where(nom_hdrs.imagetype eq 2)
nor=where(nom_hdrs.imagetype eq 0)
help,dou,nor

print,'Start time = ',utc2str(cmdu[0],/noz)
print,'End time = ',utc2str(cmdu[n-1],/noz)
print,'Median/Stdev basehdrs.actalexpduration (sec) of type=NORMAL = '
printmedsdev,dur1[nor]
print,'Median/Stdev basehdrs.actalexpduration (sec) of type=DOUBLE = '
printmedsdev,dur1[dou]
print,'Median/Stdev basehdrs.actalexpduration_2 (sec) of type=DOUBLE = '
printmedsdev,dur2[dou]
print,'Median/Stdev (nom_hdrs.actualimageretrievestarttime - nom_hdrs.actualccdclearstarttime) (sec) of type=NORMAL = '
printmedsdev,ro[nor]-clr[nor]
print,'Median/Stdev (nom_hdrs.actualimageretrievestarttime - nom_hdrs.actualccdclearstarttime) (sec) of type=DOUBLE = '
printmedsdev,ro[dou]-clr[dou]



stop


end
