function check_encoder, pq, encoder, aorb, expected
;+
;$Id: check_encoder.pro,v 1.3 2011/11/21 18:55:54 nathan Exp $
; Project     : STEREO - SECCHI
;                   
; Name        : check_encoder()
;               
; Purpose     : check that mechanism encoder reading is as expected
;               
; Use         : IDL> IF check_encoder(304, 23, 1) THEN print,'position is wrong'
;
; Inputs      : INT scch.wavelnth
;               INT basehdr.actualpolarposition
;   	    	INT nom_hdr.platform_id [1 for A, 2 for B]
;   	    	
; Outputs     : Returns 1 if NOT OK, else 0
;               
; Optional Output:
;   	    defq    What is expected for encoderq
;
; Keywords    : 
;
; Category    : error checking, mechanism, pipeline, header
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL, 
;               
; $Log: check_encoder.pro,v $
; Revision 1.3  2011/11/21 18:55:54  nathan
; fix 171
;
; Revision 1.2  2011/11/10 21:19:30  nathan
; bug
;
; Revision 1.1  2011/11/10 21:18:43  nathan
; implement check_encoder() for euvi qs
;
;-            

; Table from MechParamSummary from LMSAL, SEC01167 Rev C
;									L10-FM Encoder Reading	L20-FM Encoder Reading
;Mirror / Filter Pos	Filter Type	Target CW	Target CCW	CW	CCW		CW	CCW
;171 (III)		Grid		20		23		22	22		21	21
;284 (II) (Reset)	Mesh		14		17		16	16		15	15
;195 (I)		Grid		8		11		10	10		9	9
;304 (IV)		Mesh		2		5		4	4		3	3

qreadings=[[22,16,10,4],[21,15,9,3]]
expected=0
CASE pq OF
171: expected=qreadings[0,aorb-1]
284: expected=qreadings[1,aorb-1]
195: expected=qreadings[2,aorb-1]
304: expected=qreadings[3,aorb-1]
ELSE : message,'Unrecognized polar_quad position='+trim(pq),/info
ENDCASE

IF expected eq encoder THEN return,0 ELSE return,1

end

