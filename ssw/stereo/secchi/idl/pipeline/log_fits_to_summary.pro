pro log_fits_to_summary,subdir,yyyy,mm,tel=tel, secchi_dir=secchi_dir
;+
; $Id: log_fits_to_summary.pro,v 1.10 2011/08/31 12:12:46 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : log_fits_to_summary
;               
; Purpose     : create monthly summary file from the processed fits files.
;               
; Explanation : 
;               
; Use         : IDL> log_fits_to_summary,[cal,img or seq],year,month 
;    
; Inputs      : 
;
; Keywords    : tel='cor1' or 'cor2' or 'euvi' or 'hi_1' or 'hi_2'
;   	    	SECCHI_DIR = directory to use instead of $SECCHI for FITS file location;
;   	    	    Example: '$secchi/lz/L0/a'
;               
; Outputs     : sccXyyyymm.[cal,img, or seq]_test
;
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt
;               
; Modified    :
;
; $Log: log_fits_to_summary.pro,v $
; Revision 1.10  2011/08/31 12:12:46  secchia
; looks for new summary file with year and month only
;
; Revision 1.9  2011/07/27 15:35:33  secchib
; create new file in $SCC_SUM.new
;
; Revision 1.8  2011/01/04 19:50:16  mcnutt
; add more info to event occured message
;
; Revision 1.7  2008/07/02 17:37:37  mcnutt
; creates new summary file in summary.new then prompts for answer before moving the file to summary
;
; Revision 1.6  2007/12/13 16:34:48  nathan
; added SECCHI_DIR= keyword
;
; Revision 1.5  2007/05/22 17:26:31  nathan
; add TEL= option
;
; Revision 1.4  2007/04/10 19:25:11  mcnutt
;  use different summary file sfx for each telescope
;
; Revision 1.3  2007/04/05 22:19:35  nathan
; tweaks
;
; Revision 1.2  2007/01/09 18:14:49  nathan
; use sccwritesummary to write to summary file
;
; Revision 1.1  2006/03/31 21:02:03  mcnutt
; creates a summary file from fits files
;
;
;

;read one months worth of (cal, img or seq) fits files and sort by time
othersumfiles=file_search(getenv('SCC_SUM')+'/*') ;look for existing summary files in summary.new

if(yyyy lt 2000)then yyyy=yyyy+2000 
yyyy=string(yyyy,'(i4.4)') & mm=string(mm,'(i2.2)')
if keyword_set(tel) THEN telvar=tel ELSE telvar='*'
IF ~keyword_set(SECCHI_DIR) THEN secchi_dir=getenv('SECCHI')
mdir=secchi_dir+'/'+subdir+'/'+telvar+'/'+yyyy+mm
help,mdir
ftsfiles=file_search(mdir+'*/*.fts')
lx=strlen(ftsfiles(0))
ftstimes=strmid(ftsfiles,lx-25,15)
ftsintimeorder=ftsfiles(sort(ftstimes))
;stop
; read fits headers and write new summary files
img_dir=subdir
if(ftsintimeorder(0) ne '')then begin
for n=0L,n_Elements(ftsintimeorder)-1 do begin
;  if(strmid(ftsfiles(n),17+13,1) eq 7)then isspwx=1 else isspwx=0 
    print,ftsintimeorder[n]
  
    tmp=sccreadfits(ftsintimeorder(n),scch,/nodata)

    sfx='.'+strmid(scch.filename,18,2)
    sumfile= getenv('SCC_SUM')+'.new/scc'+strmid(scch.filename,20,1) $
	    	    	    	    	    	    +strmid(scch.filename,0,6) $
						    +'.'+img_dir+sfx    
    sccwritesummary,scch,sumfile,/nolock
  
endfor
endif

newsumfiles=file_search(getenv('SCC_SUM')+'.new/*'+yyyy+mm+'*')
for n=0,n_elements(newsumfiles)-1 do begin
   new=where(othersumfiles eq newsumfiles(n),isold)
   if  ~isold and newsumfiles(n) ne '' then begin
     ansr=''
     read,'Do you want to move '+newsumfiles(n)+' to '+getenv('SCC_SUM')+' [y/n]:',ansr
     if ansr eq  'y' then spawn,'mv '+newsumfiles(n)+' '+getenv('SCC_SUM')
   endif
endfor

end				
