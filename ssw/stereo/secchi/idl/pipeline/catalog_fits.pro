;+
; $Id: catalog_fits.pro,v 1.1 2006/05/08 15:25:41 mcnutt Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : catalog_fits
;               
; Purpose     : move early fits files to new directories and add to summary files.
;               
; Explanation : 
;               
; Use         : IDL> catalog_fits,year,month 
;    
; Inputs      : 
;               
; Outputs     : sccXyyyymm.[cal,img, or seq]
;
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt
;               
; Modified    :
;
; $Log: catalog_fits.pro,v $
; Revision 1.1  2006/05/08 15:25:41  mcnutt
; added to move early image files into directories
;
;
;
;
pro catalog_fits,yyyy,mm

lusummary=10
;read one months worth of (cal, img or seq) fits files and sort by time
if(yyyy lt 2000)then yyyy=yyyy+2000 
yyyy=string(yyyy,'(i4.4)') & mm=string(mm,'(i2.2)')
ftsfilest=['']& ftsname=[''] & ftstimes=['000000000_000000']
dirs=['cor1','cor2','euvi','hi1','hi2']
for n=0,n_Elements(dirs)-1 do begin
  mdir=getenv('SECCHI_OLD')+'/'+dirs(n)+'/'+yyyy+mm
  ftsfiles=findfile(mdir+'*/*.fts')
  lx=strlen(ftsfiles(0))
  ftstimest=strmid(ftsfiles,lx-25,15)
  ftsnamet=strmid(ftsfiles,lx-25,25)
  nt=where(strmid(ftsfiles,lx-25,1) eq '/')
  if(nt(0) ne -1)then ftstimest(nt)=strmid(ftsfiles(nt),lx-24,15)
  ftsnamet=strmid(ftsfiles,lx-24,24)
  nt=where(strmid(ftsfiles,lx-25,1) ne '/')
  if(nt(0) ne -1)then ftsnamet(nt)=strmid(ftsfiles(nt),lx-25,25)
  ftstimes=[ftstimes,ftstimest]
  ftsfilest=[ftsfilest,ftsfiles]
  ftsname=[ftsname,ftsnamet]
print,dirs(n),n_Elements(ftsname)
endfor
nt2=where(ftsfilest ne '')

if(nt2(0) ne -1)then begin
  ftsfiles=ftsfilest(nt2)
  ftstimes=ftstimes(nt2)
  ftsname=ftsname(nt2)
  ftsintimeorder=ftsfiles(sort(ftstimes))
  ftsday=strmid(ftstimes(sort(ftstimes)),0,8)
; stop
; red fits headers and write new summary files

openw,1,getenv('SECCHI_OLD')+'/cp_fits_'+yyyy+mm+'.sh',width=200
openw,2,getenv('SECCHI_OLD')+'/rm_fits_'+yyyy+mm+'.sh'
for n=0,n_Elements(ftsintimeorder)-1 do begin
;  if(strmid(ftsfiles(n),17+13,1) eq 7)then isspwx=1 else isspwx=0 
;  print,ftsintimeorder(n)
  tmp=sccreadfits(ftsintimeorder(n),headers,/nodata)

    case trim(headers(0).seb_prog) of
     'NORMAL'   : L='n'
     'DARK'     : L='k'
     'DOUBLE'   : L='d'
     'LED'      : L='e'
     'CONTIN'   : L='c'
     'SERIES'   : L='s'
     'SEQ'      : L='s'
     'NORMAL'   : L='n'
    else        : L='n'
   endcase
 
    img_dir='img'
    if(trim(strlowcase(headers(0).detector)) eq 'cor1' or trim(strlowcase(headers(0).detector)) eq 'cor2')then begin
    	case strmid(headers(0).filename,16,1) of
    	    'n' : img_dir='seq'
    	    's' : img_dir='seq'
	    else:
    	endcase
    endif
;    case strmid(headers(0).filename,16,1) of
;  moved to prevent file name from moving the image to seq if it belongs in cal
    case L of
      'k' : img_dir='cal'   ; DARK
      'e' : img_dir='cal'   ; LED
      'c' : img_dir='cal'   ; CONT
    else : img_dir=img_dir
    endcase
    
;print,img_dir
    ; IF door is not OPEN or IN TRANSIT than put in cal directory.
    IF (headers(0).doorstat LT 1 OR headers(0).doorstat GT 2) THEN img_dir='cal' 
;print,img_dir
    case trim(strlowcase(headers(0).detector)) of
    	'hi1' : BEGIN
            SUBdir=img_dir+'/hi_1'
	    END
    	'hi2' : BEGIN
	    SUBdir=img_dir+'/hi_2'
	    END
    	else  : SUBdir=img_dir+'/'+trim(strlowcase(headers(0).detector))
    endcase
    sumfile= concat_dir(getenv('SCC_SUM'),'scc'+getenv('SCC_PLATFORM')+strmid(headers(0).filename,0,6)+'.'+img_dir)
    isfile=findfile (sumfile)
    openw,lusummary,sumfile,/append
    dateObs=strmid(headers(0).date_obs,0,4)+'/'+strmid(headers(0).date_obs,5,2)+'/'+strmid(headers(0).date_obs,8,2)+' '+strmid(headers(0).date_obs,11,8)
    dests=['x','x','x','RT  ','SSR1','SSR2','x','SW  ']
    dt=strmid(headers(0).filename,17,1)
    if(dt ne 'c' and dt ne 'h' and dt ne 'e')then dest=dests(dt) else dest=dests(3)
     case trim(headers(0).seb_prog) of
      'NORMAL'   : sebp='Norm'
      'DARK'     : sebp='Dark'
      'DOUBLE'   : sebp='Doub'
      'LED'      : sebp='Led '
      'CONTIN'   : sebp='Cont'
      'SERIES'   : sebp='Sequ'
      'SEQ  '    : sebp='Sequ'
      else       : sebp=headers(0).seb_prog
    endcase
    case trim(headers(0).fps_on) of
      'T'     : scchfps='ON '
      'F'     : scchfps='OFF'
      else    : scchfps=headers(0).fps_on
    endcase
    case trim(headers(0).filter) of
      'OPEN'     : scchfilt='Clear'
      'S1'       : scchfilt='Al+1a'
      'S2'       : scchfilt='Al+1b'
      'DBL'      : scchfilt='Al+2'
      ''         : scchfilt='None'
      else    : scchfilt='None'
    endcase
    case trim(headers(0).detector) of
      'HI1'     : scchpol='None'
      'HI2'     : scchpol='None'
      'COR1'    : scchpol=string(format='(f5.1)',headers(0).polar)
      'COR2'    : scchpol=string(format='(f5.1)',headers(0).polar)
      'EUVI'    : scchpol=string(format='(i3)',headers(0).sector)
      else      : scchpol='None'
    endcase
    if (headers(0).detector eq "HI1" or headers(0).detector eq "HI2" or headers(0).detector eq "COR1") then begin
      case trim(headers(0).ledcolor) of
        'RED'     : scchled='RedFPA'
        'BLUE'     : scchled='BluFPA'
        'PURPLE'    : scchled='PrpFPA'
        'NONE'    : scchled='None'
        else      : scchled='None'
       endcase
     endif
    if (headers(0).detector eq "COR2") then begin
      case trim(headers(0).ledcolor) of
        'RED'     : scchled='RedTel'
        'BLUE'     : scchled='BluFPA'
        'PURPLE'    : scchled='PrpFPA'
        'NONE'    : scchled='None'
        else      : scchled='None'
       endcase
     endif
    if (headers(0).detector eq "EUVI") then begin
      case trim(headers(0).ledcolor) of
        'RED'     : scchled='RedFPA'
        'BLUE'     : scchled='BluTel'
        'PURPLE'    : scchled='PrpTel'
        'NONE'    : scchled='None'
        else      : scchled='None'
       endcase
     endif
    obsid=headers(0).obs_id
;    if(headers(0).obs_id eq 65535)then obsid=headers(0).imgctr

    if(strmid(SUBdir,0,3) eq 'img' or strmid(SUBdir,0,3) eq 'seq')then begin
      if(isfile eq "")then begin
        printf,lusummary,'       FileName                  DateObs          Tel  Exptime Xsize  Ysize  Filter  Polar   Prog   OSnum   Dest   FPS' 
        printf,lusummary,'========================================================================================================================='
      endif
      fmt="(a25,' | ',a19,' | ',a4,' |',f6.1,'| ',i4,' | ',i4,' | ',a5,' | ',a5,' | ',a4,' |',i5,' | ',a4,' | ',a3)"
      printf,lusummary,format=fmt,headers(0).filename,dateObs,headers(0).detector,headers(0).exptime,headers(0).NAXIS1,$
	    headers(0).NAXIS2,scchfilt,scchpol,sebp,obsid,dest,scchfps
    endif else begin
      if(isfile eq "")then begin
        printf,lusummary,'       FileName                  DateObs          Tel  Exptime Xsize  Ysize  Filter  Polar   Prog   OSnum   Dest   FPS    LED' 
        printf,lusummary,'==============================================================================================================================='
      endif
      fmt="(a25,' | ',a19,' | ',a4,' |',f6.1,'| ',i4,' | ',i4,' | ',a5,' | ',a5,' | ',a4,' |',i5,' | ',a4,' | ',a3,' | ',a6)"
      printf,lusummary,format=fmt,headers(0).filename,dateObs,headers(0).detector,headers(0).exptime,headers(0).NAXIS1,$
	    headers(0).NAXIS2,scchfilt,scchpol,sebp,obsid,dest,scchfps,scchled
    endelse
    close,lusummary
    free_lun,lusummary        

    printf,1,'cp ',ftsintimeorder(n),' ',getenv('SECCHI')+'/'+SUBdir+'/'+ftsday(n)
    printf,2,'rm ',ftsintimeorder(n)
  
endfor
close,1 & close,2
endif
end				
