pro getrpscclist ,scclist, yyyymmdd, nosortv,COR1=cor1, COR2=cor2, EUVI=euvi, HI1=hi1, HI2=hi2, ATTIC=attic
;+
; $Id: getrpscclist.pro,v 1.19 2013/02/01 19:14:34 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : 
;               
; Purpose     : reprocess SECCHI image data
;               
; Explanation : 
;               
; Use         : IDL> getrpscclist, scclist,'20070708',/euvi
;    
; Inputs      : yyyymmdd    date
;               
; Outputs     : scclist STRARR	List of fileorig science files 
;
; Keywords :	/euvi,/cor1,/cor2,/hi1,/hi2	indicates which telescope to do
;   	    	ATTIC=	Directory to search instead of $SCC_ATTIC
;
; Calls from LASCO : 
;
; Common      : 
;               
; Restrictions: Requires pipeline processing environment (user secchia or secchib)
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Sep 2007
;               
; Modified    :
;
; $Log: getrpscclist.pro,v $
; Revision 1.19  2013/02/01 19:14:34  secchia
; removed last change sis not work on unix
;
; Revision 1.18  2013/02/01 19:03:19  mcnutt
; change ls sys call to return full date
;
; Revision 1.17  2012/07/17 12:57:12  secchia
; change attic dir to SCI_ATTIC
;
; Revision 1.16  2012/06/14 11:50:37  secchia
; check n_elements grp2
;
; Revision 1.15  2011/09/12 13:26:06  secchib
; searches for cor2 and cor1 seq
;
; Revision 1.14  2011/08/30 21:54:26  secchib
; nr - default moredays=0
;
; Revision 1.13  2010/07/01 19:48:13  nathan
; added ATTIC= to getrpscclist.pro
;
; Revision 1.12  2009/06/10 14:00:40  secchia
; corrected last change to look ssr2 images not spw
;
; Revision 1.11  2009/06/10 13:50:35  mcnutt
; added HI1 and HI2 SSR2 files
;
; Revision 1.10  2008/01/03 14:52:57  mcnutt
; corrected for year end
;
; Revision 1.9  2007/09/21 18:17:28  secchia
; uses maxdat not max(idate) to select only ssr2 or spw if they are the only changed images
;
; Revision 1.8  2007/09/21 15:26:43  secchia
; added if statements incase there are no spw and ssr2 images to be reprocessed
;
; Revision 1.7  2007/09/19 17:13:02  secchib
; added SPW and SSR2 images
;
; Revision 1.6  2007/09/18 20:31:02  secchib
; removed testing dirs
;
; Revision 1.5  2007/09/18 20:26:19  mcnutt
; hopefully works on mode transition and IS restart days
;
; Revision 1.4  2007/09/14 14:14:23  mcnutt
; handles first hi image and only selectes needed hi part images
;
; Revision 1.3  2007/09/12 19:40:51  secchib
; added nosort value
;
; Revision 1.2  2007/09/12 16:16:17  mcnutt
; if .0 files for hi all hi images for the day are reprocessed
;
; Revision 1.1  2007/09/11 20:13:08  mcnutt
; creats an image list for reprocessing newly replayed images only
;
imglist=getenv('SCC_LOG')+'/Reprocessed_images_'+yyyymmdd+'.txt'
;imglist=getenv('HOME')+'/Reprocessed_images_'+yyyymmdd+'.txt'
IF keyword_set(ATTIC) THEN atdir0=attic ELSE atdir0=getenv('SCI_ATTIC')
attdir=concat_dir(atdir0,strmid(yyyymmdd,2,6))
print,'Searching ',attdir
;attdir='/net/earth/secchi/raw/lz/a/sci'+'/'+strmid(yyyymmdd,2,6)
cd,attdir,current=curdir
syscmd='/bin/ls -l  >'+imglist
spawn,syscmd
cd,curdir


  b36=['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I',  $
       'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','10']
 


line=''
d=0
lines=strarr(15000)
openr,1,imglist
while not eof(1) do begin
  readf,1,line
  meta=strpos(line,'meta')
  if(meta eq -1)then begin
    lines(d)=line 
    d=d+1 
  endif
endwhile
close,1
lines=lines(1:d-1)

isize=strarr(n_elements(lines))
idate=strarr(n_elements(lines))
iname=strarr(n_elements(lines))
itime=strarr(n_elements(lines))
for n=0,n_elements(lines)-1 do begin
   tmp=strsplit(lines(n))
   iname(n)=strmid(lines(n),tmp(n_elements(tmp)-1),strlen(lines(n))-tmp(n_elements(tmp)-1))
   isize(n)=strmid(lines(n),tmp(n_elements(tmp)-5),tmp(n_elements(tmp)-4)-tmp(n_elements(tmp)-5))
   itime(n)=strmid(lines(n),tmp(n_elements(tmp)-2),tmp(n_elements(tmp)-1)-tmp(n_elements(tmp)-2))
   yr=strmid(yyyymmdd,0,4)
   if (strmid(yyyymmdd,4,2) eq '12' and strtrim(strmid(lines(n),tmp(n_elements(tmp)-4),tmp(n_elements(tmp)-3)-tmp(n_elements(tmp)-4))) eq 'Feb') then yr=string(yr+1)
   idate(n)=yr+string(utc2doy(str2utc(strtrim(strmid(lines(n),tmp(n_elements(tmp)-4),tmp(n_elements(tmp)-3)-tmp(n_elements(tmp)-4)))+'-'+ $
            strtrim(strmid(lines(n),tmp(n_elements(tmp)-3),tmp(n_elements(tmp)-2)-tmp(n_elements(tmp)-3)))+'-'+strmid(yyyymmdd,0,4))),'(i3.3)')
endfor

idate=long(idate)
mindate=min(idate) & maxdate=max(idate)
scclist='' & sccdate=''

;always include .SCI files
zsci=where(strmid(iname,11,1) eq 'I')
nsci=where(strmid(iname,11,1) ne 'I')
if (zsci(0) gt -1)then begin
  scclist=iname(zsci)
  sccdate=idate(zsci)
  isize=isize(nsci)
  itime=itime(nsci)
  idate=idate(nsci)
  iname=iname(nsci)
endif

;remove HI's hi word and HI's space weather images added after hi images are selected.
hi_spw_iname='           '      
xhi=where(strmid(iname,4,1) eq 2)
if (xhi(0) gt -1) then begin
  hi_spw_iname=iname(xhi)        
  hi_spw_idate=idate(xhi)
endif
nhs=where(strmid(iname,4,1) ne 2)
isize=isize(nhs)
itime=itime(nhs)
idate=idate(nhs)
iname=iname(nhs)
 
xspw=where(strmid(iname,9,1) eq 7)
if (xspw(0) gt -1) then begin
  spw_iname=iname(xspw)        
  spw_idate=idate(xspw)
endif
nspw=where(strmid(iname,9,1) ne 7)
isize=isize(nspw)
itime=itime(nspw)
idate=idate(nspw)
iname=iname(nspw)

xssr2=where(strmid(iname,9,1) eq 5)
if (xssr2(0) gt -1) then begin
  ssr2_iname=iname(xssr2)        
  ssr2_idate=idate(xssr2)
endif
nssr2=where(strmid(iname,9,1) ne 5)
isize=isize(nssr2)
itime=itime(nssr2)
idate=idate(nssr2)
iname=iname(nssr2)


imgnum=lonarr(n_Elements(iname))
for nc=0,n_Elements(imgnum)-1 do imgnum(nc)= (where(b36 eq  strmid(iname(nc),4,1))*36l^3 ) + ( where(b36 eq  strmid(iname(nc),5,1))*36l^2) +  (where(b36 eq  strmid(iname(nc),6,1))*36l) + (where(b36 eq  strmid(iname(nc),7,1)))

moredays=0
zdot0=where(strpos(iname,'.0') gt -1)
if (zdot0(0) gt -1) then begin
     grp1=where(idate le mindate+1)
     if (grp1(0) gt -1)then begin
        g1s=sort(itime(grp1))
        g1_names=iname(grp1(g1s))
        g1_nums=imgnum(grp1(g1s))
        g1_time=itime(grp1(g1s))
        g1_date=idate(grp1(g1s))
        g1_size=isize(grp1(g1s))
        tmp=shift(g1_nums,1)-g1_nums & tmp(0)=0
        nreset=where(tmp gt 100) & nreset=nreset(0)
        cnt2=indgen(n_elements(g1_names)-nreset) +nreset
        g1_nums(cnt2)=g1_nums(cnt2)+(3*36l^3)
     endif
     if(mindate lt maxdate-moredays) then begin
        grp2=where(idate ge maxdate-moredays)
        if (grp2(0) gt -1)then begin
         g2s=sort(itime(grp2))
         g2_names=iname(grp2(g2s))
         g2_nums=imgnum(grp2(g2s))
         g2_time=itime(grp2(g2s))
         g2_date=idate(grp2(g2s))
         g2_size=isize(grp2(g2s))
         tmp=shift(g2_nums,1)-g2_nums & tmp(0)=0
         nreset=where(tmp gt 100) & nreset=nreset(0)
         cnt2=indgen(n_elements(g2_names)-nreset) +nreset
         g2_nums(cnt2)=g2_nums(cnt2)+(3*36l^3)
         g1_names=[g1_names,g2_names]
         g1_nums=[g1_nums,g2_nums]
         g1_date=[g1_date,g2_date]
         g1_size=[g1_size,g2_size]
       endif
     endif
     zs=sort(g1_nums)
     iname=g1_names(zs)
     idate=g1_date(zs)     
     isize=g1_size(zs)     
endif

newimgs=where(idate ge maxdate-moredays)  ;-1 incase playback crossed midnight but will always be more then 3 day behind first playback
if (xssr2(0) gt -1) then newssr2=where(ssr2_idate ge maxdate-moredays) else newssr2=[-1]  
if (xspw(0) gt -1) then newspw=where(spw_idate ge maxdate-moredays) else newspw=[-1]

IF keyword_set(COR1) THEN BEGIN
        lchar=[2,7]
        zimg=-1
        if(newimgs(0) gt -1) then zimg=where(strmid(iname(newimgs),11,1) eq lchar(0) or strmid(iname(newimgs),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
           zcor1=where(strmid(iname,11,1) eq lchar(0) or strmid(iname,11,1) eq lchar(1)) 
           tmplist=iname(zcor1)        
           tmpsize=isize(zcor1)
    	   tmpdate=idate(zcor1)
           zdate=where(tmpdate ge maxdate-moredays)
           zsize=where(tmpsize*1.0 gt 600)
           if (min(zsize) lt min(zdate)) then zstart=max(zsize(where(zsize lt min(zdate)))+1) else zstart=min(zdate)
           if (max(zsize) gt max(zdate)) then zend=min(zsize(where(zsize gt max(zdate)))+2) else zend=max(zdate)
           if(zend gt n_elements(zcor1)-1)then zend=n_elements(zcor1)-1
           scclist=[scclist,tmplist(zstart:zend)]        
           sccdate=[sccdate,tmpdate(zstart:zend)]        
        endif
        zimg=-1
        if(newssr2(0) gt -1) then zimg=where(strmid(ssr2_iname(newssr2),11,1) eq lchar(0) or strmid(ssr2_iname(newssr2),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,ssr2_iname(newssr2(zimg))]        
          sccdate=[sccdate,ssr2_idate(newssr2(zimg))]        
        endif
        zimg=-1
        if(newspw(0) gt -1) then zimg=where(strmid(spw_iname(newspw),11,1) eq lchar(0) or strmid(spw_iname(newspw),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,spw_iname(newspw(zimg))]        
          sccdate=[sccdate,spw_idate(newspw(zimg))]        
        endif
ENDIF
IF keyword_set(COR2) THEN BEGIN
	lchar=[1,6]
        zimg=-1
        if(newimgs(0) gt -1) then zimg=where(strmid(iname(newimgs),11,1) eq lchar(0) or strmid(iname(newimgs),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
           zcor2=where(strmid(iname,11,1) eq lchar(0) or strmid(iname,11,1) eq lchar(1)) 
           tmplist=iname(zcor2)        
           tmpsize=isize(zcor2)
    	   tmpdate=idate(zcor2)
           zdate=where(tmpdate ge maxdate-moredays)
           zsize=where(tmpsize*1.0 gt 600)
           if (min(zsize) lt min(zdate)) then zstart=max(zsize(where(zsize lt min(zdate)))+1) else zstart=min(zdate)
           if (max(zsize) gt max(zdate)) then zend=min(zsize(where(zsize gt max(zdate)))+2) else zend=max(zdate)
           if(zend gt n_elements(zcor2)-1)then zend=n_elements(zcor2)-1
           scclist=[scclist,tmplist(zstart:zend)]        
           sccdate=[sccdate,tmpdate(zstart:zend)]        
        endif
        zimg=-1
        if(newssr2(0) gt -1) then zimg=where(strmid(ssr2_iname(newssr2),11,1) eq lchar(0) or strmid(ssr2_iname(newssr2),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,ssr2_iname(newssr2(zimg))]        
          sccdate=[sccdate,ssr2_idate(newssr2(zimg))]        
        endif
        zimg=-1
        if(newspw(0) gt -1) then zimg=where(strmid(spw_iname(newspw),11,1) eq lchar(0) or strmid(spw_iname(newspw),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,spw_iname(newspw(zimg))]        
          sccdate=[sccdate,spw_idate(newspw(zimg))]        
        endif
ENDIF
IF keyword_set(EUVI) THEN BEGIN
	lchar=[3,8]
        zimg=-1
        if(newimgs(0) gt -1) then zimg=where(strmid(iname(newimgs),11,1) eq lchar(0) or strmid(iname(newimgs),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,iname(newimgs(zimg))]        
          sccdate=[sccdate,idate(newimgs(zimg))]        
        endif
        zimg=-1
        if(newssr2(0) gt -1) then zimg=where(strmid(ssr2_iname(newssr2),11,1) eq lchar(0) or strmid(ssr2_iname(newssr2),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,ssr2_iname(newssr2(zimg))]        
          sccdate=[sccdate,ssr2_idate(newssr2(zimg))]        
        endif
        zimg=-1
        if(newspw(0) gt -1) then zimg=where(strmid(spw_iname(newspw),11,1) eq lchar(0) or strmid(spw_iname(newspw),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,spw_iname(newspw(zimg))]        
          sccdate=[sccdate,spw_idate(newspw(zimg))]        
        endif
ENDIF
IF keyword_set(HI1)   THEN BEGIN
	lchar=[5,0]
        zimg=-1
        if(newimgs(0) gt -1) then zimg=where(strmid(iname(newimgs),11,1) eq lchar(0) or strmid(iname(newimgs),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
           zh1=where(strmid(iname,11,1) eq lchar(0) or strmid(iname,11,1) eq lchar(1)) 
           tmplist=iname(zh1)        
           tmpsize=isize(zh1)
    	   tmpdate=idate(zh1)
           zdate=where(tmpdate ge maxdate-moredays)
           zsize=where(tmpsize*1.0 gt 600)
           if (min(zsize) lt min(zdate)) then zstart=max(zsize(where(zsize lt min(zdate)))+1) else zstart=min(zdate)
           if (max(zsize) gt max(zdate)) then zend=min(zsize(where(zsize gt max(zdate)))+2) else zend=max(zdate)
           if(zend gt n_elements(zh1)-1)then zend=n_elements(zh1)-1
           scclist=[scclist,tmplist(zstart:zend)]        
           sccdate=[sccdate,tmpdate(zstart:zend)]        
           for n=zstart,zend do begin
	      tmpz=where(strmid(hi_spw_iname,0,8) eq strmid(tmplist(n),0,4)+'2'+ strmid(tmplist(n),5,3))
	      if(tmpz(0) gt -1)then begin
                   scclist=[scclist,hi_spw_iname(tmpz)]        
                   sccdate=[sccdate,hi_spw_idate(tmpz)] 
              endif 
  	   endfor      
        endif
        zimg=-1
        if(newssr2(0) gt -1) then zimg=where(strmid(ssr2_iname(newssr2),11,1) eq lchar(0) or strmid(ssr2_iname(newssr2),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,ssr2_iname(newssr2(zimg))]        
          sccdate=[sccdate,ssr2_idate(newssr2(zimg))]        
        endif
ENDIF

IF keyword_set(HI2)   THEN BEGIN
	lchar=[4,9]
        zimg=-1
        if(newimgs(0) gt -1) then zimg=where(strmid(iname(newimgs),11,1) eq lchar(0) or strmid(iname(newimgs),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          zh2=where(strmid(iname,11,1) eq lchar(0) or strmid(iname,11,1) eq lchar(1)) 
          tmplist=iname(zh2)        
          tmpsize=isize(zh2)
	  tmpdate=idate(zh2)
          zdate=where(tmpdate ge maxdate-moredays)
          zsize=where(tmpsize*1.0 gt 600)
          if (min(zsize) lt min(zdate)) then zstart=max(zsize(where(zsize lt min(zdate)))+1) else zstart=min(zdate)
          if (max(zsize) gt max(zdate)) then zend=min(zsize(where(zsize gt max(zdate)))+2) else zend=max(zdate)
          if(zend gt n_elements(zh2)-1)then zend=n_elements(zh2)-1
          scclist=[scclist,tmplist(zstart:zend)]        
          sccdate=[sccdate,tmpdate(zstart:zend)]        
          for n=zstart,zend do begin
	     tmpz=where(strmid(hi_spw_iname,0,8) eq strmid(tmplist(n),0,4)+'2'+ strmid(tmplist(n),5,3))
	     if(tmpz(0) gt -1)then begin
                  scclist=[scclist,hi_spw_iname(tmpz)]        
                  sccdate=[sccdate,hi_spw_idate(tmpz)] 
	      endif 
	  endfor      
        endif 
        zimg=-1
        if(newssr2(0) gt -1) then zimg=where(strmid(ssr2_iname(newssr2),11,1) eq lchar(0) or strmid(ssr2_iname(newssr2),11,1) eq lchar(1))
        if (zimg(0) gt -1) then begin
          scclist=[scclist,ssr2_iname(newssr2(zimg))]        
          sccdate=[sccdate,ssr2_idate(newssr2(zimg))]        
        endif
ENDIF

sccdate=sccdate(where(scclist ne ''))
scclist=scclist(where(scclist ne ''))
scclist=attdir+'/'+scclist
;scclist=scclist(sort(scclist))


syscmd='/bin/rm -f '+imglist

spawn,syscmd

wait,2

openw,1,imglist,width=200
for n=0,n_elements(scclist)-1 do printf,1,sccdate(n),'   ',scclist(n)
close,1

end
