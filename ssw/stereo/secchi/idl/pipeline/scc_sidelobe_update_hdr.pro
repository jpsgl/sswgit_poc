; $Id: scc_sidelobe_update_hdr.pro,v 1.25 2015/11/10 18:41:41 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : scc_sidelobe_update_hdr
;               
; Purpose     : correct side lobe save area image headers
;               
; Explanation : 
;               
; Use         : IDL> scc_sidelobe_update_hdr
;    
; Inputs      : 
;               
; Outputs     : 
;
; Keywords :	
;               
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Hutting 08/06/2014 
;               
; Modified    :
;
; $Log: scc_sidelobe_update_hdr.pro,v $
; Revision 1.25  2015/11/10 18:41:41  secchia
; corrected for missing apid for itimes2
;
; Revision 1.24  2015/11/10 18:38:33  secchia
; corrected for missing apid for itimes2
;
; Revision 1.23  2015/08/18 20:39:27  mcnutt
; corrected hi_1 timmimg error lengthening downobsnum array to match downip
;
; Revision 1.22  2015/07/15 20:55:12  hutting
; added 1 minute to IP40 time not avoid over writing files
;
; Revision 1.21  2015/07/15 20:51:02  hutting
; added 34M IP40 for HI1
;
; Revision 1.20  2015/07/13 18:46:59  secchia
; added second schedule file check before reading it
;
; Revision 1.19  2015/02/19 14:16:24  secchia
; corrected yr when reading in schedules on mdump days
;
; Revision 1.18  2015/02/18 22:40:06  secchia
; correcged day count
;
; Revision 1.17  2015/02/18 15:32:43  secchia
; changes blocklst logic
;
; Revision 1.16  2015/02/18 14:59:17  hutting
; corrected line 99 blocklist1
;
; Revision 1.15  2015/02/12 19:42:29  secchia
; read in schedules for day n-3
;
; Revision 1.14  2015/02/05 13:32:37  secchia
; added correction for 34M hi1 exposure times
;
; Revision 1.13  2015/02/04 22:21:00  hutting
; added correction for HI1 34M track exposures
;
; Revision 1.12  2015/01/21 15:15:43  secchia
; iptimes for hi_1 now include IP40 HI_1 run during the 34M tracks
;
; Revision 1.11  2014/12/22 15:32:25  secchia
; correcct year for 2015 and 2016 schedules
;
; Revision 1.10  2014/09/02 18:49:54  hutting
; corrected outdir if date changes
;
; Revision 1.9  2014/08/22 19:10:40  hutting
; corrected EUVI sidelobe exposures 304 and 195 where reversed
;
; Revision 1.8  2014/08/20 20:54:16  secchia
; corrected wavelength for EUVI
;
; Revision 1.7  2014/08/20 14:01:11  hutting
; remove Mdump door lines when reading schedules
;
; Revision 1.6  2014/08/07 17:35:16  secchia
; read scch from common block not fitshead2struct caused rectify to be an intager
;
; Revision 1.5  2014/08/07 15:35:55  hutting
; change bsffile.txt to sidelobe dir
;
; Revision 1.4  2014/08/07 13:17:18  hutting
; added sidelobe dir to schedule search
;
; Revision 1.3  2014/08/06 14:22:40  secchia
; correct img_dir and outdir
;
; Revision 1.2  2014/08/05 21:16:32  hutting
; corrected filename and obs_id check
;
; Revision 1.1  2014/08/05 21:04:12  hutting
; first rev
;

pro get_sidelobe_save_times,sc,d1

common sidelobe_save_times,saveimg,iptimes,matchip,downip,scu,d1u,downobsnum

if datatype(d1u) ne 'UND' then if d1 eq d1u and scu eq sc then goto,noupdate

schdir=getenv('sched')+'/sidelobe/'+strupcase(sc)+'/'
schdir2=getenv('sched')+'/'+strupcase(sc)+'/dailyats/'
ud=anytim2utc(d1)
mjd1=ud
mjd1.mjd=ud.mjd-3
mjd=ud
FOR i=0,3 DO BEGIN
  mjd.mjd=mjd1.mjd+i
  ud=anytim2utc(mjd)
  yyyymmdd=utc2yymmdd(mjd,/yyyy)
  sfile=file_search(schdir+'/'+strmid(yyyymmdd,2,6)+'00.sch')
  if sfile eq '' then sfile=file_search(schdir2+'/'+strmid(yyyymmdd,0,4)+'/'+strmid(yyyymmdd,2,6)+'00.sch')
  if sfile ne '' then begin 
    lst=readlist(sfile) 
    print,sfile
    z=where(strpos(lst,'#') ne 0 and strpos(lst,'-') ne 0,cnt)    
    if cnt gt 0 then begin
     if datatype(blocklst) eq 'UND' then blocklst=lst[z] else blocklst=[blocklst,lst[z]] 
    endif
  endif
ENDFOR
if datatype(blocklst) eq 'UND' then goto,noupdate
z=where(strpos(blocklst,'.bsf') gt 0)
blocklst=blocklst[z]
yr=strmid(blocklst,0,1)
tmp=where(yr eq 'A',cnt)
if cnt gt 0 then yr[tmp] ='5'
tmp=where(yr eq 'B',cnt)
if cnt gt 0 then yr[tmp] ='6'
bsfile=strmid(blocklst,16,12)
btime='201'+yr+'-'+strmid(blocklst,1,2)+'-'+strmid(blocklst,3,2)+' '+strmid(blocklst,5,2)+':'+strmid(blocklst,7,2)+':'+strmid(blocklst,9,2)

bsffiles=readlist(getenv('sched')+'/sidelobe/bsffiles.txt')

saveimg=['IP29','IP55','IP56','IP66','IP79','IP82','IP40']
iptimes=strarr(n_Elements(saveimg),96)

for n=0,n_Elements(bsfile)-1 do begin
  z=where(strpos(bsffiles,bsfile[n]) gt -1,cnt)
  if cnt gt 0 then begin
    lst=readlist(getenv('sched')+'/'+bsffiles[z[0]])
    lst=lst(where(strpos(lst,'---') gt -1))
    offset=strmid(lst[0],5,2)*60*60+strmid(lst[0],7,2)*60+strmid(lst[0],9,2)
    break_file,bsffiles[z],path,dir,name,ext
    for n1=0,n_Elements(saveimg)-1 do begin
      cmd='grep '+saveimg[n1]+' '+getenv('sched')+'/'+dir+'SEC20'+strmid(name,0,6)+'00'+strmid(name,7,1)+'.summary'
      spawn,cmd,ipgrep
      zt=where(strpos(ipgrep,saveimg[n1]) gt 0,cnt)
      if cnt gt 0 then begin
        ipgrep=ipgrep[zt]
        t1=anytim2tai(btime[n])+strmid(ipgrep,18,2)*60*60+strmid(ipgrep,21,2)*60+strmid(ipgrep,24,2)-offset
        z=where(iptimes[n1,*] eq '',cnt)    
        iptimes[n1,z[0]:z[0]+n_Elements(ipgrep)-1]=utc2str(tai2utc(t1))
      endif
      if saveimg[n1] eq 'IP79' then begin
        cmd='grep IP40 '+getenv('sched')+'/'+dir+'SEC20'+strmid(name,0,6)+'00'+strmid(name,7,1)+'.summary'
        spawn,cmd,ipgrep
	zt=where(strpos(ipgrep,saveimg[n1]) gt 0,cnt)
        if cnt gt 0 then begin 
	  ipgrep=ipgrep[zt]
          t1=anytim2tai(btime[n])+strmid(ipgrep,18,2)*60*60+strmid(ipgrep,21,2)*60+strmid(ipgrep,24,2)-offset
          z=where(iptimes[n1,*] eq '',cnt)    
          iptimes[n1,z[0]:z[0]+n_Elements(ipgrep)-1]=utc2str(tai2utc(t1))
          z1=where(iptimes[n1,*] ne '',cnt)    
          z=sort(iptimes[n1,Z1])
          iptimes[n1,Z1]=iptimes[n1,Z1[z]]
        endif
      endif
    endfor
  endif
endfor  

scu=sc
d1u=d1
noupdate:
end  

pro scc_sidelobe_update_hdr,fits_hdr,outdir,img_dir
version = '$Id: scc_sidelobe_update_hdr.pro,v 1.25 2015/11/10 18:41:41 secchia Exp $'

common sidelobe_save_times
common scc_reduce

hdr=scch

matchip=['IP29','IP55','IP56','IP66','IP79','IP79','IP82','IP82','IP40','IP40']
downip= ['IP28','IP63','IP67','IP68','IP78','IP74','IP77','IP76','IP78','IP74']
downobsnum=[2176,2180,  2182,  2178,  2157,  2183,  2166,  2184,  2157,  2183]
wl=        [171,  284,   304,   195,     0,     0,     0,     0,     0,     0]
exp=         [4,   32,     4,     8,  1200,  1200,  4950,  4950,  1200,  1200]
sel=where(downobsnum eq hdr.obs_id)

if hdr.obs_id eq 1518 and hdr.exptime lt 50 then begin
  newhdr=hdr
  newhdr.exptime=1200  ;correct 34m hi1 header.
  fxaddpar,fits_hdr,'EXPTIME',newhdr.exptime,'sidelobeadj'
  scch=newhdr
endif

if sel[0] lt 0 then goto,nocorrection 

sc=rstrmid(hdr.obsrvtry,0,1)
d1=strmid(hdr.date_end,0,10)

get_sidelobe_save_times,sc,d1

if strlowcase(sc) eq 'a' then lighttravel=0 else lighttravel=0 
newhdr=hdr

imtype=matchip[sel]
itimes=reform(iptimes[where(saveimg eq imtype[0]),*])
itimes=anytim2tai(itimes[where(itimes ne '')])
if n_elements(sel) gt 1 then begin
  itimes2=reform(iptimes[where(saveimg eq imtype[1]),*])
  z=where(itimes2 ne '',cnt)
  if cnt gt 0 then begin
    itimes2=anytim2tai(itimes2[where(itimes2 ne '')])+60
    itimes2=[itimes,itimes2]
    itimes=itimes2[sort(itimes2)]
  endif
  sel=sel[0]
endif
z=where(itimes lt anytim2tai(hdr.date_end))
cmnt='sidelobe not corrected'
if z[0] ne -1 then begin
  itime=itimes[z[n_elements(z)-1]]
  newhdr.date_obs=utc2str(tai2utc(itime+lighttravel))
  newhdr.date_cmd=utc2str(tai2utc(itime))
  newhdr.date_end=utc2str(tai2utc(itime+exp[sel]+lighttravel))
  newhdr.date_avg=utc2str(tai2utc(itime+exp[sel]/2.+lighttravel))
  newhdr.filename	=utc2yymmdd(tai2utc(itime),/yyyy,/hhmmss)+'_n'+strmid(hdr.filename,17,8)
  cmnt='sidelobeadj'
endif

if sel[0] le 3 then begin ;HI exposure is corrected in hi_low_merge.
  newhdr.seb_prog='NORMAL'
  newhdr.exptime=exp[sel]
  newhdr.wavelnth=wl[sel]
  fxaddpar,fits_hdr,'WAVELNTH',newhdr.wavelnth,'sidelobeadj'
  fxaddpar,fits_hdr,'EXPTIME',newhdr.exptime,'sidelobeadj'
endif else newhdr.seb_prog='SERIES'

scch=newhdr
fxaddpar,fits_hdr,'DATE-CMD',newhdr.date_cmd,cmnt
fxaddpar,fits_hdr,'DATE-OBS',newhdr.date_obs,cmnt
fxaddpar,fits_hdr,'DATE-END',newhdr.date_end,cmnt
fxaddpar,fits_hdr,'DATE-AVG',newhdr.date_avg,cmnt
fxaddpar,fits_hdr,'SEB_PROG',newhdr.seb_prog,'sidelobeadj'
fxaddpar,fits_hdr,'FILENAME',newhdr.filename,cmnt
fxaddpar,fits_hdr,'HISTORY',version
fxaddpar,fits_hdr,'HISTORY','orig date_obs ='+hdr.date_obs

; correct outdir and img_dir to img from cal
strput,outdir,'img',strpos(outdir,img_dir)
img_dir='img'
strput,outdir,strmid(newhdr.filename,0,8),strpos(outdir,strmid(newhdr.filename,0,6))
scch=newhdr

nocorrection: 
end
