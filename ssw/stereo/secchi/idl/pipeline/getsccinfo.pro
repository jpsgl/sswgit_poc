;+
; $Id: getsccinfo.pro,v 1.10 2008/08/14 16:57:34 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : getsccinfo
;               
; Purpose     :widget controll for getsccarchiveinfo.pro.
;               
; Explanation : 
;               
; Use         : IDL> getsccinfo
;    
; Inputs      : 
;               
; Outputs     : see getsccarchiveinfo
;
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt
;               
; Modified    :
;
; $Log: getsccinfo.pro,v $
; Revision 1.10  2008/08/14 16:57:34  nathan
; fixed default value of ndf
;
; Revision 1.9  2008/03/10 17:41:19  mcnutt
; remove all selections and set defaults to 1st value in all colums
;
; Revision 1.8  2008/02/04 19:53:29  mcnutt
; add input for logfile name
;
; Revision 1.7  2008/02/04 18:55:22  mcnutt
; added radio buttons for sc apid and telescopes
;
; Revision 1.6  2008/01/25 18:15:04  nathan
; Add /DEBUG
;
; Revision 1.5  2008/01/25 17:48:51  nathan
; change default ndays
;
; Revision 1.4  2007/09/25 14:51:05  mcnutt
; Widget controll for getsccarchiveinfo added comments
;



common getsccarch ,vonf,scf,imgf,apidf,telf,scv,imgwv,apidv,telv,pltf,timef,ndf,von,spc,img,apid,tels,plt,cyr,$
    cmo,cdy,cyrt,cmot,cdyt,valt, crnttotal, crntdate, bm, loud,logf,logv

PRO getsccinfo_event, ev                                     ; event handler
common getsccarch ;,vonf,scf,imgf,apidf,telf,pltf,timef,ndf,von,spc,img,apid,tels,plt,cyr,cmo,cdy,cyrt,cmot,cdyt,valt, crnttotal, crntdate, bm
  widget_control, ev.id, get_uvalue=uvalue                        ; get the uvalue
  CASE uvalue OF                                                  ; choose case
  'vonv'   : vonf= von(ev.index)

   'scv'   : begin 

               scf(ev.value)=ev.select
;               if (ev.value eq 0 and ev.select eq 1) then scf(1:n_Elements(scf)-1)=1
;               if (ev.value ne 0 and scf(0) eq 1) then begin 
;	         scf(0:n_Elements(scf)-1)=0
;                 scf(ev.value)=1
;               endif
               WIDGET_CONTROL, scv, SET_VALUE= scf         
             end            
 
  'imgwv'   :  begin 
                imgf(0:n_elements(imgf)-1)=0
                imgf(ev.value)=ev.select
               WIDGET_CONTROL, imgwv, SET_VALUE= imgf         
              end

   'apidv'   : begin 
               apidf(ev.value)=ev.select
 ;              if (ev.value eq 0 and ev.select eq 1) then apidf(1:n_Elements(apidf)-1)=1
 ;              if (ev.value ne 0 and apidf(0) eq 1) then begin 
;	         apidf(0:n_Elements(apidf)-1)=0
;                 apidf(ev.value)=1
;               endif
               WIDGET_CONTROL, apidv, SET_VALUE= apidf         
             end            

   'telv'   : begin 
               telf(ev.value)=ev.select
;               if (ev.value eq 0 and ev.select eq 1) then telf(1:n_Elements(telf)-1)=1
;               if (ev.value ne 0 and telf(0) eq 1) then begin 
;	         telf(0:n_Elements(telf)-1)=0
;                 telf(ev.value)=1
;               endif
               WIDGET_CONTROL, telv, SET_VALUE= telf         
             end            
  
  'pltv'   : pltf= plt(ev.index)
  'CYR'   : begin
      	    cyrt= ev.index
            timef=cyr(cyrt)+'-'+cmo(cmot)+'-'+cdy(cdyt)
            end
  'CMO'   : begin
      	    cmot= ev.index
            timef=cyr(cyrt)+'-'+cmo(cmot)+'-'+cdy(cdyt)
            end
  'CDY'   : begin
      	    cdyt= ev.index
            timef=cyr(cyrt)+'-'+cmo(cmot)+'-'+cdy(cdyt)
            end
   'ndays' : ndf=ev.value

  'logv'  :  WIDGET_CONTROL,logv, GET_VALUE = logf


   'GO' : begin 
             if pltf eq 'PLOT' then pltv=0 else pltv=1
             if vonf eq 'nFILEs' then numv=1 else numv=0
             if vonf eq 'VOLUME' then volv=1 else volv=0

             if logf eq '' then logf=0

;    	     aheadv=scf(1)
;    	     behindv=scf(2)
    	     aheadv=scf(0)
    	     behindv=scf(1)

            tel=tels(where(telf eq 1)) 
;            tel=tel(where(tel ne 'ALL')) 

    	     imgv=imgf(0)
    	     seqv=imgf(1)
    	     sciv=imgf(2)
    	     fitsv=imgf(3)

;	     ssr1v=apidf(1)
;    	     ssr2v=apidf(2)
;    	     rtv=apidf(3)
;    	     spwxv=apidf(4)
	     ssr1v=apidf(0)
    	     ssr2v=apidf(1)
    	     rtv=apidf(2)
    	     spwxv=apidf(3)

	     ndf=fix(ndf[0])
	     IF (loud) THEN help,timef,ndf,aheadv,behindv,tel,numv,volv,ssr1v,ssr2v,rtf,spwxv,fitsv,sciv,imgv,seqv,pltv

  	     getsccarchiveinfo ,crnttotal, crntdate, bm, sta_date=timef, ndays=ndf, $
	                                   ahead=aheadv, behind=behindv,telescope=tel, $
					   number=numv, volume=volv,ssr1=ssr1v,ssr2=ssr2v,ssrt=rtv,spwx=spwxv, $
	                                   fits=fitsv, SCI=sciv, img=imgv, seq=seqv, noplot=pltv,LOGFILE=logf
          end
  "FNSH": WIDGET_CONTROL, ev.top, /DESTROY

  END
;  valt=scf+' '+imgf+' '+apidf+' '+telf+' '+timef+' ndays='+string(ndf)
;  print,scf,' ',imgf,' ',apidf,' ',telf,' ',timef,' ',ndf

END


PRO getsccinfo, DEBUG=debug
device,retain=2
common getsccarch ;,vonf,scf,imgf,apidf,telf,scv,imgv,apidv,telv,pltf,timef,ndf,von,spc,img,apid,tels,plt,cyr,cmo,cdy,cyrt,cmot,cdyt,valt, crnttotal, crntdate, bm
;set defaults

IF keyword_set(DEBUG) THEN loud=1 ELSE loud=0



ndf=0
  main = widget_base (title='GETSCCarchiveinfo', /colum)             ; main base


   row2 = widget_base (main, /row)

;  spc=['BOTH','AHEAD','BEHIND']
;  scf=[0,0,0]
;  scv1=[0,1,2]
  spc=['AHEAD','BEHIND']
  scf=[1,0]
  scv1=[0,1]

  scv= CW_BGROUP(row2, spc, UVALUE="scv", button_uvalue=scv1 ,/NONEXCLUSIVE, /COLUMN, /FRAME, label_top='Spacecraft')
  WIDGET_CONTROL, scv, SET_VALUE= scf         

  img=['img','seq','sci','fits']
  imgf=[1,0,0,0]
  imgv1=[0,1,2,3]
  imgwv= CW_BGROUP(row2, img, UVALUE="imgwv", button_uvalue=imgv1 ,/NONEXCLUSIVE, /COLUMN, /FRAME, label_top='Image files' )
  WIDGET_CONTROL, imgwv, SET_VALUE= imgf         


;  apid=['ALL','SSR1','SSR2','SSRT','SPWX']
;  apidf=[0,0,0,0,0]
;  apidv1=[0,1,2,3,4]
  apid=['SSR1','SSR2','SSRT','SPWX']
  apidf=[1,0,0,0]
  apidv1=[0,1,2,3]

  apidv = CW_BGROUP(row2, apid, UVALUE="apidv", button_uvalue=apidv1 ,/NONEXCLUSIVE, /COLUMN, /FRAME, label_top='ADIP' )
  WIDGET_CONTROL, apidv, SET_VALUE= apidf         



;  tels=['ALL','COR1','COR2','EUVI','HI1','HI2'];,'CORs','HIs']
;  telf=[0,0,0,0,0,0]
;  telv1=[0,1,2,3,4,5,6]
  tels=['COR1','COR2','EUVI','HI1','HI2'];,'CORs','HIs']
  telf=[1,0,0,0,0]
  telv1=[0,1,2,3,4,5]
  telv= CW_BGROUP(row2, tels, UVALUE="telv", button_uvalue=telv1 ,/NONEXCLUSIVE, /COLUMN, /FRAME , label_top='Telescope')
  WIDGET_CONTROL, telv, SET_VALUE= telf         

;  telf=tels(0)
;  telv= WIDGET_DROPLIST(row2, VALUE=tels, UVALUE="telv")

  row2b = widget_base (main, /row)

  von=['VOLUME','nFILEs']
  vonf=von(0)
  vonv= WIDGET_DROPLIST(row2b, VALUE=von, UVALUE="vonv")
  txt=widget_label (row2b,  value='           ')


  plt=['PLOT','NOPLOT']
  pltf=plt(0)
  pltv= WIDGET_DROPLIST(row2b,  VALUE=plt, UVALUE="pltv")
;  txt=widget_label (row2b,  value='     ')

  row3 = widget_base (main, /row)



 cyr=['2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014',$
      '2015','2016','2017','2018','2019','2010','2011','2012','2013','2014','2015',$
      '2016','2017','2018','2019','2020','2021','2022','2023','2024','2025','2026',$
      '2027','2028','2029','2030']

 cmo=['01','02','03','04','05','06','07','08','09','10','11','12']
 cdy=['01','02','03','04','05','06','07','08','09','10','11','12', $
     '13','14','15','16','17','18','19','20','21','22','23','24','25','26','27', $
     '28','29','30','31']

   row4 = widget_base (main, /row)


 txt= WIDGET_LABEL(row3, VALUE='Starting Date: ')

 cyrt= 2
 cyrid= WIDGET_COMBOBOX(row3, VALUE=cyr,UVALUE="CYR",/EDITABLE,XSIZE=80)
 WIDGET_CONTROL, cyrid, SET_COMBOBOX_SELECT= cyrt
 WIDGET_CONTROL, cyrid, SENSITIVE=1

 cmot= 10
 cmoid= WIDGET_COMBOBOX(row3, VALUE=cmo,UVALUE="CMO",/EDITABLE,XSIZE=60)
 WIDGET_CONTROL, cmoid, SET_COMBOBOX_SELECT= cmot
 WIDGET_CONTROL, cmoid, SENSITIVE=1

 cdyt= 25
 cdyid= WIDGET_COMBOBOX(row3, VALUE=cdy,UVALUE="CDY",/EDITABLE,XSIZE=60)
 WIDGET_CONTROL, cdyid, SET_COMBOBOX_SELECT=cdyt
 WIDGET_CONTROL, cdyid, SENSITIVE=1

 timef=cyr(cyrt)+'-'+cmo(cmot)+'-'+cdy(cdyt)

    row4 = widget_base (main, /row)

 ndf=0
 nday = cw_field(row4,  value = 0, /all_events, title = 'Number of days (0= up to most recent):', xsize=4, UVALUE="ndays")



   row5 = widget_base (main, /row)

;  logf='Enter output log file'
  logv=cw_field(row5, value='', uvalue="logv",title='Log file name',/all_events)
  logf=''

   row6 = widget_base (main, /row)

 gob=widget_button(row6, value='Run Getsccarchive', uvalue="GO")
  txt=widget_label (row6,  value='                         ')

 fnsh=widget_button(row6,  value='CLOSE', uvalue="FNSH")
 
  widget_control, main, /realize                                  ; create the widgets
  xmanager, 'getsccinfo', main,/no_block	    	                         ; wait for events

END




