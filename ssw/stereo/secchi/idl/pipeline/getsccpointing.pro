function getsccpointing, scch, aorb, dte, RADEC=radec, UPDATEHEADER=updateheader, $
	NOSPICE=nospice, _EXTRA=_extra, YPRSC=yprsc, FAST=fast, FOUND=ff
;+
; $Id: getsccpointing.pro,v 1.11 2011/09/21 21:23:43 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : getsccpointing
;               
; Purpose   : return pointing information for input header
;               
; Explanation: 
;               
; Use       : IDL> ypr=getsccpointing(scch) 
;    
; Input    : 	scch	STC	header structure -or-
;		scch 	STRARR	FITS header 
;
; Optional  :	scch	STRING	Name of SECCHI detector
; Inputs    	aorb	STRING	'a' or 'b' or 'ahead' or 'behind'
;   	    	dte 	STRING	Date-obs
;               
; Outputs   : 	3 element FLTARR [yaw(X), pitch(Y), roll]; yaw and pitch are in arcseconds,
;   	    	roll is in degrees; for HI or RADEC, all values are degrees
;
; Keywords  :	UPDATEHEADER	If set then update HISTORY (only) in scch
;		RADEC		Return results using RA-DEC coordinates
;		YPRSC		Set to variable which will contain spacecraft [yaw,pitch,roll] from SPICE
;   	    	    	    	Y,P in Arcsec, R in Deg; for HI or RADEC, all degrees
;   	    	FAST	    	Skip waits
;   	    	FOUND	    	Return 0 if results from prediction, 1 if it is actual data
;               
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: detector, obsrvtry, date_obs, must be defined 
;		in input structure or array;
;		images are ASSUMED TO BE RECTIFIED
;
; Side effects: 
;               
; Category    : pipeline, calibration, attitude, coordinates, pointing
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Feb 2007
;               
; $Log: getsccpointing.pro,v $
; Revision 1.11  2011/09/21 21:23:43  nathan
; Define values of COR2 pitch and yaw from median of CRVAL; ignore SPICE
; pitch and yaw if LT 0.1 deg
;
; Revision 1.10  2010/05/27 20:59:07  nathan
; for HI use date_avg per hdr defn; yprsc is always deg if HI
;
; Revision 1.9  2008/04/17 15:14:01  nathan
; update values (bug 302)
;
; Revision 1.8  2007/07/20 15:33:39  nathan
; Add /DEGREES functionality and set tel offsets in degrees
;
; Revision 1.7  2007/06/25 17:47:36  nathan
; do use SPICE yp for COR1; fix implementation of /NOSPICE
;
; Revision 1.6  2007/06/19 15:37:49  nathan
; Use DATE-END for attitude info for HI, else use DATE-OBS (Bug 178)
;
; Revision 1.5  2007/04/16 16:14:17  nathan
; implement COR1 offsets from 4/3/07; fix arcsec->degree conv for vec
;
; Revision 1.4  2007/04/04 17:05:00  nathan
; check if attitude file found and modify att_file accordingly (Bug 129)
;
; Revision 1.3  2007/03/13 19:12:04  secchib
; nr - do not wait if /fast
;
; Revision 1.2  2007/03/02 19:13:39  secchib
; nr - fix Id
;
; Revision 1.1  2007/03/01 22:45:18  nathan
; Implement getsccpointing.pro (Bug 27); add SC_[yaw,pitch,roll] (Bug 91)
;
;-            

scchtype=datatype(scch) 
IF scchtype EQ 'STC' THEN BEGIN
	tel=scch.detector
	aorb=strmid(scch.obsrvtry,7,1)
    	IF strmid(tel,0,2) EQ 'HI' THEN dateobs=scch.date_avg ELSE dateobs=scch.date_obs
ENDIF ELSE $
IF n_elements(scch) GT 1 THEN BEGIN
	tel=	fxpar(scch,'DETECTOR')
	aorb=strmid(fxpar(scch,'OBSRVTRY'),7,1)
    	dateobs=fxpar(scch,'DATE-OBS')	
ENDIF ELSE BEGIN
	tel=strupcase(scch)
	aorb=strupcase(strmid(aorb,0,1))
	dateobs=dte
ENDELSE

info="$Id: getsccpointing.pro,v 1.11 2011/09/21 21:23:43 nathan Exp $"
len=strlen(info)
histinfo=strmid(info,1,len-2)

factor=3600.
CASE tel OF
'COR1': CASE aorb OF
	'A': yproffs=[ 32.899852/3600.,  97.096261/3600., 0.203826]	; deg,deg,deg
	'B': yproffs=[-0.24846208/3600., -135.95599/3600.,  0.0983413]	; deg,deg,deg
    	; from cor1_point.pro,v 1.4 2007/05/14 19:51:13 thompson
	
	; 	
	;'A': yproffs=[32.71/3600, 96.18/3600, 0.20]
	;; This is median of actual daily CRVAL1(2) 2008/01/24 - 2011/04/09
	;; Orbital variation of CRVAL1(2) is +/- 12.7(6.0)  arcsec
	;'B': yproffs=[1.27/3600, -139.34/3600, 0.098]
	;; This is median of actual daily CRVAL1(2) 2007/11/22 - 2011/08/28
	;; Orbital variation of CRVAL1(2) is +/- 19.1(6.8) arcsec
	ELSE:
	ENDCASE
'COR2': CASE aorb OF
	;'A': yproffs=[0.,0.,0.45]		; deg,deg,deg
	;'B': yproffs=[0.,0.,-0.20]		; deg,deg,deg
	;; from cor2_point.pro,v 1.1
	
	; 	
	'A': yproffs=[-67.42/3600, -54.97/3600, 0.45]		; deg,deg,deg
	; This is median of actual daily CRVAL1(2) 2010/09/15 - 2011/08/28
	; Orbital variation of CRVAL1(2) is +/- 7.4(8.8)  arcsec
	'B': yproffs=[131.50/3600, 114.00/3600, -0.20]		; deg,deg,deg
	; This is median of actual daily CRVAL1(2) 2010/08/20 - 2011/09/10
	; Orbital variation of CRVAL1(2) is +/- 16.5(20.6) arcsec
	ELSE:
	ENDCASE
'EUVI': CASE aorb OF
	'A': yproffs=[0.,0.,1.245-1.125]		; deg,deg,deg
	'B': yproffs=[0.,0.,-1.125]		; deg,deg,deg
    	; from euvi_point.pro,v 1.5
	ELSE:
	ENDCASE
; HI numbers from get_hi_params.pro,v 1.4, calibrated values
'HI1':  BEGIN
	factor=1.
	CASE aorb OF
	'A': yproffs=[13.9789,0.075,1.0241]	; deg,deg,deg
	'B': yproffs=[-14.185,0.022,0.30141]	; deg,deg,deg
	ELSE:
	ENDCASE
	END
'HI2':  BEGIN
	factor=1.
	CASE aorb OF
	'A': yproffs=[53.51,0.23,0.130]	; deg,deg,deg
	'B': yproffs=[-53.68,0.,0.]	; deg,deg,deg
	ELSE:
	ENDCASE
	END
ELSE: 	BEGIN
    	
	message,'WARNING: Detector '+tel+' not found -- assuming 0 offset from spacecraft pointing.',/info
	yproffs=[0.,0.,0.]
	END
ENDCASE
IF keyword_set(DEGREES) THEN factor=1.

;print,'Units for [yaw,pitch,roll] are [deg,deg,deg]'

print,'Instr offset:  ',yproffs
IF ~keyword_set(FAST) THEN wait,2

;--Get offsets from S/C - [yaw,pitch,roll]
IF keyword_set(NOSPICE) THEN BEGIN
	message,'Returned values assume zero spacecraft attitude ',/info
	message,'(values are relative to ecliptic)',/info
	y0=0. & p0=0. & r0=0.
	yprsc=[y0,p0,r0]
	IF keyword_set(RADEC) THEN sign=-1d ELSE sign=1d
ENDIF ELSE IF keyword_set(RADEC) THEN BEGIN
	print,'Calling get_stereo_roll ...'
	r0 = GET_STEREO_ROLL(dateobs, aorb, y0, p0, SYSTEM='GEI', Found=ff, _EXTRA=_extra)
	; outputs all degrees               RA  Dec 
	factor=1.
	sign=-1d
	yprsc=[y0,p0,r0]
ENDIF ELSE BEGIN
	print,'Calling get_stereo_hpc_point ...'
	yprsc=get_stereo_hpc_point(dateobs,aorb, Found=ff, _EXTRA=_extra)
	;yprsc=three-element vector containing in order the yaw (X), pitch (Y) in Arcsec and roll in degrees.
	y0=yprsc[0]/3600 & p0=yprsc[1]/3600 & r0=yprsc[2]
	;convert values to degrees for matrix math
	;print,'Units for [yaw,pitch,roll] are [arcsec,arcsec,deg]'
	IF factor EQ 1 THEN yprsc=[y0,p0,r0]
	; Return all values as degrees in YPRSC if HI
	sign=1d

	; STEREO Yaw and pitch basically has no jitter information, only noise of about +/- 50 arcsec, plus off-point if any.
	; THEREFORE, I will ignore any yaw or pitch LT 0.1 deg (360 arcsec) for output.
	; Keyword YPRSC will not be affected.
	IF abs(y0) LT 0.1 THEN y0=0.
	IF abs(p0) LT 0.1 THEN p0=0
ENDELSE

yt=yproffs[0] & pt=yproffs[1] & rt=yproffs[2]
; all telescope offset values are degrees

print,'Spacecraft ypr:',y0,p0,r0,' degrees'
print,yprsc
;IF tel EQ 'COR1' THEN BEGIN
;	y0=0.
;	p0=0.
;;	message,'For COR1, use S/C pitch,yaw=0',/info
;ENDIF
IF ~keyword_set(FAST) THEN wait,2

dr=!DPI/180.d
sy0=sin(y0*dr) & sp0=sin(p0*dr) & sr0=sin(r0*dr)
cy0=cos(y0*dr) & cp0=cos(p0*dr) & cr0=cos(r0*dr)

mat=[ 	[                   cp0*cy0,              -sign*cp0*sy0,           sp0], $
	[sign*(cr0*sy0+sr0*sp0*cy0),        cr0*cy0-sr0*sp0*sy0, -sign*sr0*cp0], $
	[       sr0*sy0-cr0*sp0*cy0, sign*(sr0*cy0+cr0*sp0*sy0),       cr0*cp0] ]

syt=sin(yt*dr) & spt=sin(pt*dr)
cyt=cos(yt*dr) & cpt=cos(pt*dr)

vec=[ cyt*cpt, syt*cpt, spt ]

res=mat#vec
print,'res',res,' degrees'
; in degrees

IF keyword_set(UPDATEHEADER) THEN BEGIN
    IF scchtype EQ 'STC' THEN BEGIN
    	print,'updating history'
	scch=scc_update_history(scch, histinfo)
    ENDIF ELSE message,'Warning - /UPDATEHEADER not implemented for FITS header',/inform
ENDIF	

return, [-sign*atan(res[1],res[0])*factor/dr, asin(res[2])*factor/dr, r0+rt ]
; need to convert pitch and yaw to arcsec for SCIP HPC
end
