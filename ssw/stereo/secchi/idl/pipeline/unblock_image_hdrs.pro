pro unblock_image_hdrs,infile
;+
; $Id: unblock_image_hdrs.pro,v 1.4 2009/12/14 18:49:22 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : unblock_image_hdrs
;               
; Purpose   : Read in header structures from images files
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :  unblock_image_hdrs,'~/lz/pipeline/attic/070405/74050001.473'
;               
; Outputs   :  basehdr, nom_hdr, and ext_hdr in COMMON scc_reduce
;
; Keywords  :     
;
; Calls from LASCO : 
;
; Common    : SCC_REDUCE
;               
; Restrictions: Requires COMMON scc_reduce be already defined
;               
; Side effects:
;               
; Category    : Pipeline reformat decommutate image telemetry
;               
; Prev. Hist. : None.
;
; Written     : Created from secchi_reduce 10/03/07 LCM 
;               
; $Log: unblock_image_hdrs.pro,v $
; Revision 1.4  2009/12/14 18:49:22  nathan
; update to comments only
;
; Revision 1.3  2008/03/07 22:01:03  nathan
; Always use /sh with spawn; change input to single argument; do NOT cd to location of input
;
; Revision 1.2  2007/11/16 14:09:55  mcnutt
; corrected scifile for .0 images
;
; Revision 1.1  2007/10/04 17:07:33  mcnutt
; reads in image file headers used in updatescchdr.pro
;

common scc_reduce

    unblock_cmd = concat_dir(getenv('UTIL_DIR'),'unblockSciFile')

    ;cd,imgdir,current=curdir

    break_file,infile,v1,path,root,sfx
    infile0=root+sfx
    fileparts=strsplit(infile0,'.',/extract)
    if (n_Elements(fileparts) eq 2) then scifile=fileparts(0)+'-'+fileparts(1) else $
    	    scifile=fileparts(0)+'.'+fileparts(1)+'-'+fileparts(2)

    scidat = scifile+'.dat'	    ; always written in current directory
    scihdr = scifile+'.hdr'
    scitlr = scifile+'.tlr'
    sciimg = scifile+'.img'	    ; always written in current directory

    spawn, unblock_cmd+' '+infile,/sh, unblockresult
    print,unblockresult
    spawn,getenv('UTIL_DIR')+'/rdSh '+scihdr+ ' | grep ctrl.Version',versionline,/sh
    parts=str_sep(versionline,':')
    ; If there is a read error, core is dumped and output is empty. Assume hdr version 4.
    IF n_elements(parts) LT 2 THEN parts=[0,4]
    IF fix(parts[1]) EQ 0 THEN BEGIN
        spawn,getenv('UTIL_DIR')+'/rdShv1 '+scihdr+ ' | grep ctrl.Version',versionline,/sh
        parts=str_sep(versionline,':')
    ENDIF
    IF fix(parts[1]) LT 3 THEN BEGIN
        versionline=versionline+': loading sebhdr_struct.inc'
        nomoffset=80
        extoffset=192
@./sebhdr_struct.inc
    ENDIF ELSE IF fix(parts[1]) EQ 3 THEN BEGIN
;if datatype(ext_hdr) EQ 'UND' THEN $
        versionline=versionline+': loading sebhdr3_struct.inc'
        nomoffset=96
        extoffset=232
@./sebhdr3_struct.inc
    ENDIF ELSE BEGIN
;if datatype(ext_hdr) EQ 'UND' THEN $
        versionline=versionline+': loading sebhdr4_struct.inc'
        nomoffset=88
        extoffset=216
@./sebhdr4_struct.inc
    ENDELSE
    print,versionline
    IF NOT(file_exist(scihdr)) THEN spawn,'touch '+scihdr,/sh
    IF NOT(file_exist(scitlr)) THEN spawn,'touch '+scitlr,/sh
    print,'Reading ',scihdr
    openr,1,scihdr
    scitlrmissing='no'
    openr,2,scitlr
    hdrstat=fstat(1)
    tlrstat=fstat(2)
    readhdrinfo="hdrsize="+string(hdrstat.size)+" tlrsize="+string(tlrstat.size)
    IF (tlrstat.size GT 500) OR (tlrstat.size LT 10) THEN BEGIN
    	close,2
        command='cp '+scihdr+' '+scitlr
        print,command
        scitlrmissing='Trailer wrong size: '+command
        spawn, command,/sh
    	openr,2,scitlr
    	tlrstat=fstat(2)
    ENDIF
    IF tlrstat.size GT hdrstat.size THEN BEGIN
        mainlun=2 
        readhdrinfo=readhdrinfo+": Using "+scitlr+" for main header."
    ENDIF ELSE BEGIN
        mainlun=1
    ENDELSE
    print,readhdrinfo
    a = assoc(mainlun,m_baseheader)
    basehdr = a[0]
    ; so it works on linux PCs:
    swap_endian_inplace,basehdr,/SWAP_IF_LITTLE_ENDIAN
    IF (tlrstat.size LT hdrstat.size) THEN BEGIN
        command='cp '+scihdr+' '+scitlr
        print,command
        spawn, command,/sh
    ENDIF
    ;b = assoc(2,m_baseheader)
    ;basetlr = b[0]
    IF ((hdrstat.size>tlrstat.size) GT 100) THEN BEGIN
       a = assoc(mainlun,m_nominalheader,nomoffset)
       nom_hdr = a[0]
    	; so it works on linux PCs:
    	swap_endian_inplace,nom_hdr,/SWAP_IF_LITTLE_ENDIAN
       print,'Reading ',scitlr
       b = assoc(2,m_nominalheader,nomoffset)
       nom_tlr = b[0]
    	; so it works on linux PCs:
     	swap_endian_inplace,nom_tlr,/SWAP_IF_LITTLE_ENDIAN
    ENDIF
    IF ((hdrstat.size>tlrstat.size) GT 300) THEN BEGIN
        a = assoc(mainlun,m_extendedheader,extoffset)
        ext_hdr = a[0]
    	; so it works on linux PCs:
    	swap_endian_inplace,ext_hdr,/SWAP_IF_LITTLE_ENDIAN
        ;b = assoc(2,m_extendedheader,192)
        ;ext_tlr = b[0]
    ENDIF
    close,1
    close,2
   
    delcmd='/bin/rm -f '+scidat
    spawn,delcmd,/sh
    delcmd='/bin/rm -f '+scihdr
    spawn,delcmd,/sh
    delcmd='/bin/rm -f '+sciimg
    spawn,delcmd,/sh
    delcmd='/bin/rm -f '+scitlr
    spawn,delcmd,/sh
   
    
;cd,curdir


end
