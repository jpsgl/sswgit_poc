function insecchicalperiod, scch
;+
;$Id: insecchicalperiod.pro,v 1.2 2011/03/11 16:17:45 nathan Exp $
; Project     : STEREO - SECCHI
;                   
; Name        : INSECCHICALPERIOD
;               
; Purpose     : Check time intervals in this file to see if input header is in a period when
;   	    	calibration was going on
;               
; Use         : IDL> result=insecchicalperiod(headerstructure)
;
; Inputs      : scch	SECCHI header structure
;               
; Outputs     : 1 or 0
;               
; Opt. Outputs: 
;               
; Keywords    : 
;
; Restrictions: 
;
; Calls LASCO : 
;
; Category    : Data_Handling, I_O, pipeline, header
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL, 2010
;               
; $Log: insecchicalperiod.pro,v $
; Revision 1.2  2011/03/11 16:17:45  nathan
; mdump schedule error on B
;
; Revision 1.1  2010/05/06 16:40:59  nathan
; called by secchi_reduce.pro
;
;-            

inptai=anytim2tai(scch.date_obs)

; init value
intrvltai=anytim2tai(['2006-05-06 09:35:00','2006-05-06 11:05:00'])

; HI stray light offpoint:
IF scch.obsrvtry EQ 'STEREO_A' THEN intrvltai=anytim2tai(['2010-05-06 09:15:00','2010-05-06 11:45:00'])
IF scch.obsrvtry EQ 'STEREO_B' THEN intrvltai=anytim2tai(['2010-05-06 16:15:00','2010-05-06 19:45:00'])
; momentum dump scheduling error
IF scch.obsrvtry EQ 'STEREO_B' THEN intrvltai=anytim2tai(['2011-03-10 19:00:00','2011-03-10 20:00:00'])

IF inptai GT intrvltai[0] and inptai LE intrvltai[1] THEN return, 1 ELSE return, 0
END
