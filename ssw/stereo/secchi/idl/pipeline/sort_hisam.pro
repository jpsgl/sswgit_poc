;+
; $Id: sort_hisam.pro,v 1.6 2009/09/18 21:53:24 secchib Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : sort_hisam
;               
; Purpose     : sort and log hisam fits files created in secchi_reduce to database
;               
; Explanation : 
;               
; Use         : IDL> sort_hisam,'20090522','a'
;    
; Inputs      : 
;               
; Outputs     : 
;
; Keywords :	/DELETE_OLD    	passed from secchi_reduce to delete_old fits from database when reprocessing fits file.
;
; Calls from LASCO :
;
; Common      : 
;               
; Restrictions: Requires env variables defined in spacecraft- and pipeline-specific *.env 
;   	    	file in $SSW_SECCHI/idl/pipeline
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL/I2, May 2009
; -              
; Modified    :
;
; $Log: sort_hisam.pro,v $
; Revision 1.6  2009/09/18 21:53:24  secchib
; nr - fix bug where utimes not right
;
; Revision 1.5  2009/06/18 16:29:50  mcnutt
; set delflag to 1 to delete first header and replace with fits file (same fileorig and data_obs issue)
;
; Revision 1.4  2009/06/12 11:11:17  mcnutt
; set naxis1 = naxis2 before calling hi_point
;
; Revision 1.3  2009/06/11 16:37:22  mcnutt
; defined sds_dir and uses $secchi to find fits
;
; Revision 1.2  2009/06/11 14:55:19  mcnutt
; corrected new time array
;
; Revision 1.1  2009/06/02 18:54:40  mcnutt
; cleans up hisam fits file written in pipeline in header order and adds the fits file to the database
;



pro sort_hisam,yyyymmdd,sc,dir=sdspath,DELETE_OLD=delete_old
; both blocks are defined so sort_hisam can be run by its self or from secchi_reduce 
common scc_reduce,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, resetvars, $
    	    	    ipdat, nom_tlr, prevbasename, readfileidline, wavefileidline, $
		    setupfileidline, exposfileidline, maskfileidline, ipfileidline, icerdiv2flag, $
		    rotblidlines,hbtimes,hbtemps,cebtimes,cebtemps, subdir, date_fname_utc, ispb, islz, isrt, aorb, $
		    iserror, ephemfil, att_file
common dbms,ludb, dbfile, delflag, img_seb_hdr, img_seb_hdr_ext, img_cal_bias, $
	    img_cal_led, img_cal_dark, img_hist_cmnt, img_euvi_gt    	

teldir=['hi_2','hi_1']
img_dir='seq'
sccattic=getenv('SCC_ATTIC')+'/'+strmid(yyyymmdd,2,6)+'/'
if keyword_set(delete_old) then delflag=1 else delflag=0
destdir = getenv ('SECCHI')
logstat=fstat(lulog)
;   if datatype(dbfile) ne 'STR' then begin
   if logstat.name eq '' then begin
        pipeline ='hs'
        dbroot=yyyymmdd
        lulog=19
        sccattic=getenv('SCC_ATTIC')+'/'+strmid(yyyymmdd,2,6)+'/'
;        unblock_cmd = concat_dir(getenv('UTIL_DIR'),'unblockSciFile')
;open log file need for write db_script
    	logfile= concat_dir(getenv('SCC_LOG'),'scc'+getenv('SC')+pipeline+dbroot+'.log')
	openw,lulog,logfile,/append
;	printf,lulog,version
	printf,lulog,' '
	;text=readlist('sebhdr_struct.inc')
	text=[';*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO', $
	      ';*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Thu May 19 15:11:05 2005']
	printf,lulog,'NOTE: sebhdr info is static -- need to generate from sebhdr4_struct.inc'
	printf,lulog,text[0]
	printf,lulog,text[1]
	printf,lulog,'starting log file at '+systime()
	printf,lulog,' '
	printf,lulog,'SEB_IMG	',getenv('seb_img')
	printf,lulog,'SCI_SOURCE ',getenv('SCI_SOURCE')
	for i=0,n_Elements(teldir)-1 do printf,lulog,'SECCHI	',getenv('SECCHI')+'/*/'+teldir(i)+'/'+yyyymmdd
	printf,lulog,'SCC_ATTIC ',sccattic
;	printf,lulog,unblock_cmd
;	printf,lulog,uncompr_cmd,' or '
;	printf,lulog,huncompr_cmd,' or '
;	printf,lulog,iuncompr_cmd

	printf,lulog,'IDL_PATH	',getenv('IDL_PATH')
	printf,lulog,'TABLES	',getenv('TABLES')
	printf,lulog,'PT    	',getenv('PT')
	printf,lulog,'DATE: ',yyyymmdd
	printf,lulog,'==============================='

         SPAWN,'hostname',machine,/sh  
         machine = machine(N_ELEMENTS(machine)-1)
        src=strmid(machine,0,1)+rstrmid(machine,0,1)
	dbfile=concat_dir(getenv('SCC_DB'),'scc'+getenv('SC')+pipeline+src+dbroot+'sql.script')
	openw,ludb,dbfile,/get_lun
	dbfstat=fstat(ludb)
	IF dbfstat.size EQ 0 THEN BEGIN
	; only at beginning of each script file
	    printf,ludb,'use secchi_flight;'
	    printf,ludb,'start transaction;'
	ENDIF
	get_utc,ustarttime
	starttime=utc2str(ustarttime,/ecs)
	printf,ludb,'# starting dbfile at '+starttime
	close,ludb
	free_lun,ludb
    endif

  for i=0,n_Elements(teldir)-1 do begin
    writefile=''
    subdir=img_dir+'/'+teldir(i)
    outdir=getenv('secchi')+'/lz/L0/'+strlowcase(sc)+'/seq/'+teldir(i)
    hisams=file_search(outdir+'/'+yyyymmdd+'/'+'*.fts')
    if hisams(0) ne '' then for i3=0,n_Elements(hisams)-1 do begin ;should never be more then one file just in case merge them together
        outfile=hisams(i3)
    	tmp=sccreadfits(outfile,sccht,/nodata)
        if sccht.obs_prog eq 'HISAM' then begin
	  if i3 eq 0 then begin 
             writefile=outfile
             printf,lulog,'Sorting HISAM file '+writefile
             img=sccreadfits(outfile,scch)
	     fits_hdr=headfits(outfile)
	     exthdr=mrdfits(outfile,1)
             SAMseqtbl=string(readfits(outfile,/EXTEN))
           endif else begin
             printf,lulog,'Merging HISAM file '+outfile+' with '+writefile
	     exthdr=[exthdr,mrdfits(outfile,1)]
             SAMseqtbl=[SAMseqtbl,string(readfits(outfile,/EXTEN))]
             img=[img,sccreadfits(outfile)]
	   endelse
        endif
     endfor
     
    if writefile ne '' then begin
; sort and keep uniq times based on date_clr
      tmp=sort(exthdr.date_clr)       
      tmp=tmp(uniq(exthdr(tmp).date_clr))
      tmp=tmp(where(exthdr(tmp).date_clr ne ''))
      img=img(tmp,*)
      exthdr=exthdr(tmp)
      SAMseqtbl=SAMseqtbl(tmp)  
;define time array to add gaps for missed headers
      times=utc2tai(anytim2utc(exthdr.date_clr))
      t=(shift(times,1)-times)*(-1.)
      caden=min(t(where(t gt 0)))
      nimg=round((max(times)-min(times))/caden +1)
      newimg=lonarr(nimg,n_Elements(img(0,*))) 
      newSAMseqtbl=strarr(nimg)
      newtime=findgen(nimg)*caden +times(0)
      utimes=intarr(nimg)
      for n=0,(nimg)-1 do utimes(n)=where(times ge newtime(n)-caden/2 and times lt newtime(n)+caden/2)
      newSAMseqtbl(where(utimes ne -1))=SAMseqtbl
      if min(utimes) eq -1 then newSAMseqtbl(where(utimes eq -1))=string(' ','(a188)')
      newimg(where(utimes ne -1),*)=img
      newSAMseqtbl=string(newtime-newtime(0),'(f8.2)')+strmid(newSAMseqtbl,8,188)
      newSAMseqtbl=strmid(newSAMseqtbl,0,49)+string(indgen(nimg),'(i4)')+strmid(newSAMseqtbl,49+4,188)

;correct times 
 	  scch.date_end=utc2str(tai2utc(utc2tai(str2utc(scch.date_obs))+(times(n_Elements(times)-1)-times(0))+exthdr(n_Elements(exthdr)-1).exptime))
          scch.date_avg=utc2str(tai2utc(utc2tai(str2utc(scch.date_obs))+((times(n_Elements(times)-1)-times(0))+exthdr(n_Elements(exthdr)-1).exptime)/2.))
	  fxaddpar,fits_hdr,'DATE-AVG',scch.date_avg
	  fxaddpar,fits_hdr,'DATE-END',scch.date_end
          if strlowcase(sc) eq 'a' then scname ='ahead' else scname='behind'
          sdspath=getenv('SDS_DIR')+'/'+scname+'/data_products/level_0_telemetry/secchi'          
           get_utc,ustarttime
           ud=anytim2utc(strmid(yyyymmdd,0,4)+'-'+strmid(yyyymmdd,4,2)+'-'+strmid(yyyymmdd,2,2))	   
	   IF ud.mjd LT ustarttime.mjd-45 THEN $
	       sdspath=getenv('SDS_FIN_DIR')+'/'+strlowcase(sc)+'/'+strmid(yyyymmdd,0,4)+'/'+strmid(yyyymmdd,4,2) ELSE $
	       sdspath=getenv('SDS_DIR')+'/'+scname+'/data_products/level_0_telemetry/secchi'


          naxis1=scch.naxis1
	  scch.naxis1=scch.naxis2 ; hi point needs a square image
	  scch = hi_point_v2(scch,exthdr,dir=sdspath)
          scch.naxis1=naxis1
	  crp1c=''
	  crp2c=''
	  crv1c=' degrees'
	  crv2c=' degrees'

	fxaddpar,fits_hdr,'CRPIX1',scch.crpix1,crp1c
	fxaddpar,fits_hdr,'CRPIX2',scch.crpix2,crp2c
	fxaddpar,fits_hdr,'CRVAL1',scch.crval1,crv1c
	fxaddpar,fits_hdr,'CRVAL2',scch.crval2,crv2c
	fxaddpar,fits_hdr,'CDELT1',scch.cdelt1
	fxaddpar,fits_hdr,'CDELT2',scch.cdelt2
	fxaddpar,fits_hdr,'XCEN',scch.xcen,' degrees'
	fxaddpar,fits_hdr,'YCEN',scch.ycen,' degrees'
	fxaddpar,fits_hdr,'CRPIX1A',scch.crpix1a
	fxaddpar,fits_hdr,'CRPIX2A',scch.crpix2a
	fxaddpar,fits_hdr,'CRVAL1A',scch.crval1a
	fxaddpar,fits_hdr,'CRVAL2A',scch.crval2a
	fxaddpar,fits_hdr,'CDELT1A',scch.cdelt1a
	fxaddpar,fits_hdr,'CDELT2A',scch.cdelt2a
	fxaddpar,fits_hdr,'ATT_FILE',scch.att_file
	fxaddpar,fits_hdr,'CROTA',scch.crota
	fxaddpar,fits_hdr,'PC1_1',scch.pc1_1
	fxaddpar,fits_hdr,'PC1_2',scch.pc1_2
	fxaddpar,fits_hdr,'PC2_1',scch.pc2_1
	fxaddpar,fits_hdr,'PC2_2',scch.pc2_2
	fxaddpar,fits_hdr,'PC1_1A',scch.pc1_1a
	fxaddpar,fits_hdr,'PC1_2A',scch.pc1_2a
	fxaddpar,fits_hdr,'PC2_1A',scch.pc2_1a
	fxaddpar,fits_hdr,'PC2_2A',scch.pc2_2a
	fxaddpar,fits_hdr,'SC_ROLL',scch.sc_roll,' degrees'
	fxaddpar,fits_hdr,'SC_YAW',scch.sc_yaw,' degrees'
	fxaddpar,fits_hdr,'SC_PITCH',scch.sc_pitch,' degrees'
	fxaddpar,fits_hdr,'SC_ROLLA',scch.sc_rolla,' degrees'
	fxaddpar,fits_hdr,'SC_YAWA',scch.sc_yawa,' degrees'
	fxaddpar,fits_hdr,'SC_PITA',scch.sc_pita,' degrees'
	fxaddpar,fits_hdr,'INS_X0',scch.ins_x0,' deg'
	fxaddpar,fits_hdr,'INS_Y0',scch.ins_y0,' deg'
	fxaddpar,fits_hdr,'INS_R0',scch.ins_r0,' deg'
	
	ind=0
	REPEAT BEGIN
	    hinfo=scch.history[ind]
	    ind=ind+1 
	ENDREP UNTIL strpos(hinfo,'_point') GE 0 OR ind GT 19
	fxaddpar,fits_hdr,'HISTORY',hinfo

          writefits,writefile,newimg,fits_hdr
          exttblhdr = def_secchi_ext_hdr()
    	  writefits,writefile,byte(newSAMseqtbl),exttblhdr,/append

    	  sfx='.'+strmid(scch.filename,18,2)
	  sumfile= concat_dir(getenv('SCC_SUM'), 'scc'+strmid(scch.filename,20,1) $
	     	    	    	    	    	    +strmid(scch.filename,0,6) $
						    +'.'+img_dir+sfx)
          delflag=1
	  sccwritesummary,scch,sumfile
          unblock_image_hdrs,sccattic+scch.fileorig
	  write_db_scripts, writefile
          delflag=0

;        endif
;    endfor

       for i2=0,n_elements(hisams)-1 do begin
               if hisams(i2) ne writefile then begin
                  printf,lulog,'deleting hisam file '+hisams(i2)+' merged with '+writefile
                  delcmd='/bin/rm -f '+hisams(i2)
          	  spawn,delcmd
               endif
       endfor
    endif  ;if writefile ne ''
  endfor

if lulog eq 19 then close,lulog

END
