;+
; $Id: sccreprocess.pro,v 1.36 2013/08/13 16:01:22 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : 
;               
; Purpose     : reprocess SECCHI image data
;               
; Explanation : 
;               
; Use         : IDL> sccreprocess, '2006-10-26','2007-03-01',/euvi
;    
; Inputs      : start and end date for reprocessing (inclusive)
;               
; Outputs     : 
;
; Keywords :	/euvi,/cor1,/cor2,/hi1,/hi2	indicates which telescope to do
;		/DELETE_OLD		Remove all instances of each day in $SECCHI before processing each day
;		/RESUME		Cancels Delete_old and Delete gzip dir and enables 'Resuming where previously stopped'
;   	    	/DOGZ	Create gzip version of files if $SECCHI_GZ available
;   	    	/PIPELINE   Set this to do cpimgdb script in secchi_reduce, and email to Lynn
;		/NOPKTCNT	Do not run daily_ssr1_report
;
; Calls from LASCO : 
;
; Common      : 
;               
; Restrictions: Requires pipeline processing environment (user secchia or secchib)
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Apr 2007
;               
; Modified    :
;
; $Log: sccreprocess.pro,v $
; Revision 1.36  2013/08/13 16:01:22  secchia
; disabled pipeline keyword search $SECCHI for /lz/ to set test keyword to enable pretties
;
; Revision 1.35  2012/12/07 11:26:58  secchib
; added reprocess keyword to apid packet count
;
; Revision 1.34  2010/11/29 17:01:26  secchib
; added no eventdetect keyword to secchi_reduce call
;
; Revision 1.33  2010/07/02 22:21:41  nathan
; Get .0 files; removed /NOGZ and /TIMESORT as they are superfluous.
;
; Revision 1.32  2010/07/01 19:48:12  nathan
; added ATTIC= to getrpscclist.pro
;
; Revision 1.31  2010/06/28 19:52:09  nathan
; use sc instead of USER for atdir
;
; Revision 1.30  2010/05/12 17:07:57  secchia
; bug fix
;
; Revision 1.29  2010/05/12 16:44:26  secchia
; nr - Make /NOGZIP the default; add call to daily_ssr1_report unless /NOPKTCNT; get sci files
; from $scix not $SCC_ATTIC
;
; Revision 1.28  2009/09/14 13:41:42  secchia
; corrected ssr2only keyword
;
; Revision 1.27  2009/09/14 13:26:34  mcnutt
; added ssr2only keyword
;
; Revision 1.26  2009/08/12 21:34:01  nathan
; Accomodate symbolic links if /DELETE; look at $scix/older if none found
;
; Revision 1.25  2009/06/11 18:34:36  secchia
; nr - check scilist 1 more time
;
; Revision 1.24  2009/06/04 12:22:05  mcnutt
; added call to sort_hisam
;
; Revision 1.23  2009/04/10 19:31:44  secchib
; nr - By default do not sort by time; added /TIMESORT keyword
;
; Revision 1.22  2008/07/02 17:32:14  nathan
; update comment
;
; Revision 1.21  2008/06/06 18:46:36  secchib
; nr - added FAST keyword so var is passed to calling pro
;
; Revision 1.20  2008/03/14 18:15:28  mcnutt
; removes hilow daily directroy if reprocessing hi so not to write fits files twice
;
; Revision 1.19  2007/12/18 15:47:16  nathan
; add /PIPELINE; remove cpimgdb
;
; Revision 1.18  2007/12/13 16:06:56  mcnutt
; moves img db files after secchi_reduce completes
;
; Revision 1.17  2007/10/30 18:44:15  mcnutt
; wont delete fits directory if spwonly is set
;
; Revision 1.16  2007/10/30 18:41:06  mcnutt
; added spwonly keyword for spaceweather only reprocessing
;
; Revision 1.15  2007/09/28 17:41:47  secchia
; nr - add DELETE_OLD to secchi_reduce call
;
; Revision 1.14  2007/09/26 20:37:15  secchia
; added sort lines from secchi_reduce since when a file list is sent secchi_reduce does not sort
;
; Revision 1.13  2007/09/26 20:28:49  secchia
; added nosort keyword
;
; Revision 1.12  2007/09/26 17:38:14  nathan
; added /NOGZIP, /RESUME; change waits to 2 sec.
;
; Revision 1.11  2007/09/19 17:00:46  nathan
; Add NOEVENT_NOTIFY to secchi_reduce call
;
; Revision 1.10  2007/09/12 19:41:04  secchib
; added nosort value
;
; Revision 1.9  2007/09/11 20:08:26  mcnutt
; added partial keyword for calling getrpscclist
;
; Revision 1.8  2007/08/28 21:03:20  nathan
; add /resume
;
; Revision 1.7  2007/08/27 21:43:56  nathan
; separate HI and rm GZ dirs
;
; Revision 1.6  2007/07/19 16:52:11  nathan
; allow d1 GT d2
;
; Revision 1.5  2007/06/20 19:43:27  nathan
; always do gzip
;
; Revision 1.4  2007/06/13 18:13:26  nathan
; added tel-specific parts
;
; Revision 1.3  2007/05/23 18:30:12  nathan
; add  _EXTRA
;
; Revision 1.2  2007/05/21 20:43:37  nathan
; fix typos
;
; Revision 1.1  2007/05/18 20:35:30  nathan
; first draft
;

pro sccreprocess, d1,d2, COR1=cor1, COR2=cor2, EUVI=euvi, DELETE_OLD=delete_old, _EXTRA=_extra, $
	NOPKTCNT=nopktcnt, DOPRETTIES=dopretties, HI1=hi1, HI2=hi2, RESUME=resume, PARTIAL=partial, $
    	spwonly=spwonly, PIPELINE=pipeline, FAST=fast,ssr2only=ssr2only, DOGZ=dogz

IF keyword_set(NOGZIP) THEN dogz=0 
;IF keyword_set(PIPELINE) THEN test=0 ELSE test=1
destdir = getenv ('SECCHI')
IF strpos(destdir,'/lz/') GE 0 THEN test=0 ELSE test=1

IF keyword_set(dogz) THEN BEGIN
    print,'Checking $SECCHI_GZ...'
    dogz=file_exist(getenv('SECCHI_GZ'))
    IF ~dogz THEN print,'SECCHI_GZ not available; not doing gzip'
ENDIF

ud1=anytim2utc(d1)
IF n_params() GT 1 THEN ud2=anytim2utc(d2) ELSE ud2=ud1
ud=ud1

atdir=getenv_slash('sci'+getenv('sc'))
scitelvals=['HI1','COR2','COR1','EUVI','HI2','HI1','COR2','COR1','EUVI','HI2']

IF keyword_set(DOPRETTIES) THEN dnopretties=0 ELSE dnopretties=1
lastchar='['
tels='none'
IF keyword_set(COR1) THEN BEGIN
	lastchar=lastchar+'27'
	tels=[tels,'cor1']
ENDIF
IF keyword_set(COR2) THEN BEGIN
	lastchar=lastchar+'16'
	tels=[tels,'cor2']
ENDIF
IF keyword_set(EUVI) THEN BEGIN
	lastchar=lastchar+'38'
	tels=[tels,'euvi']
ENDIF
IF keyword_set(HI1)   THEN BEGIN
	lastchar=lastchar+'50'
	tels=[tels,'hi_1']
ENDIF
IF keyword_set(HI2)   THEN BEGIN
	lastchar=lastchar+'49'
	tels=[tels,'hi_2']
ENDIF
nt=n_elements(tels)-1
IF nt LT 1 THEN BEGIN
	message,'Must specify one or more telescopes. Returning.',/info
	return
ENDIF
ab=getenv('sc')
IF ab EQ 'a' THEN num=0 ELSE $
IF ab EQ 'b' THEN num=5 ELSE BEGIN
    message,'Must setenv sc a or b; Returning.',/info
    return
ENDELSE

lastchar=lastchar+']'
help,lastchar

IF ud1.mjd GT ud2.mjd THEN incr=-1 ELSE incr=1
FOR mjd=ud1.mjd,ud2.mjd,incr DO BEGIN
	ud.mjd=mjd
	yyyymmdd=utc2yymmdd(ud,/yyyy)
	yymmdd=strmid(yyyymmdd,2,6)
	IF keyword_set(DELETE_OLD) and ~keyword_set(RESUME)  and ~keyword_set(PARTIAL) and ~keyword_set(SPWONLY)  and ~keyword_set(SSR2ONLY) THEN BEGIN
		FOR i=1,nt DO BEGIN
			cmd='/bin/rm -fr '+getenv('SECCHI')+'/*/'+tels[i]+'/'+yyyymmdd+'/*'
			print,cmd
			print,''
			print,'Waiting 2...'
			wait,2	;10
			spawn,cmd,/sh
		ENDFOR
	ENDIF
	IF  ~keyword_set(RESUME) and $
	    ~keyword_set(PARTIAL) and $
	    ~keyword_set(SSR2ONLY) and $
	    keyword_set(DOGZ) and $
	    ~keyword_set(SPWONLY) THEN FOR i=1,nt DO BEGIN
		cmd='/bin/rm -fr '+getenv('SECCHI_GZ')+'/'+ab+'/*/'+tels[i]+'/'+yyyymmdd
		print,cmd
		print,''
		print,'Waiting 2...'
		wait,2	 ;5
		spawn,cmd,/sh

	ENDFOR
        IF keyword_set(HI1) or keyword_set(HI2)  THEN BEGIN
           rmhilow=getenv('seb_img')+'/hilow/'+yyyymmdd
           spawn,'/bin/rm -r '+rmhilow
        ENDIF

        IF keyword_set(PARTIAL) THEN $
	    getrpscclist ,scilist,yyyymmdd,COR1=cor1, COR2=cor2, EUVI=euvi, HI1=hi1, HI2=hi2, ATTIC=atdir else BEGIN
	    list1=	file_search(atdir+yymmdd+'/*.??'+lastchar)
	    list2=  	file_search(atdir+yymmdd+'/*.??'+lastchar+'.?')
            scilist=[list1,list2]
	    help,list1,list2
	ENDELSE
	help,scilist
	IF scilist[0] EQ '' THEN BEGIN
	    print,'None found, trying older..'
	    scilist=file_search(atdir+'older/'+yymmdd+'/*.??'+lastchar)
	    help,scilist
	ENDIF
	wait,10
    	
	; Sort is done by default in secchi_reduce.pro
	
       IF keyword_set(SPWONLY) THEN scilist=scilist(where(strmid(scilist,strlen(scilist(0))-3,1) eq '7'))
       IF keyword_set(SSR2ONLY) THEN scilist=scilist(where(strmid(scilist,strlen(scilist(0))-3,1) eq '5'))
	help,scilist
	wait,5

	secchi_reduce,scilist,/nowait,nopretties=dnopretties,TESTLZ=test, GZIP=dogz, $
	    	    /NOEVENT_NOTIFY, _EXTRA=_extra, RESUME=resume, DELETE_OLD=delete_old, FAST=fast,/reprocess,/noeventdect

        IF keyword_set(HI1) or keyword_set(HI2)  THEN sort_hisam,yyyymmdd,ab,DELETE_OLD=delete_old
	IF not keyword_set(NOPKTCNT) THEN daily_ssr1_report,ab,yyyymmdd,'/net/calliope/data/secchi/'+ab+'/lzsci/pcktcnt/',/pipeline,/reprocess
 
ENDFOR

end
