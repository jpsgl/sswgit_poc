;  ///////////////////////////////////////////////////////////////////////
;  //   Extended Image Description
;  //
;  ///////////////////////////////////////////////////////////////////////
;  ///////////////////////////////////////////////////////////////////////
;  //// Guide Telescope and FPS statistics
;  //// see Guide Telescope SICM for algorithm and variable definitions
;  ////
m_extendedheader = { m_extendedheader,$
          usefps:	uint16,$
          actualfpsmode:	uint16,$
          actualscfinepointmode:	uint16,$
          cmdfilterposition:	uint16,$
          cmdquadposition:	uint16,$
          actualfilterdirection:	uint16,$
          actualquaddirection:	uint16,$
          fpsdata:	sgtfpsimagehdr }
sfpspztcoef = { sfpspztcoef,$
          ycoordxform:	int16,$
          zcoordxform:	int16,$
          voltpredict:	replicate(int16,5),$
          pidcoe:	replicate(int16,4) }
;  // structure size is 66 bytes
sgtfpscoefs = { sgtfpscoefs,$
          pz:	replicate(sfpspztcoef,3) }
n_img_proc_cmd = 10
;  ///////////////////////////////////////////////////////////
;  // EUVI FPS Image Header structures and enumerations
;  // NOTE: Any modification of the following structure should
;  // be reflected in the increase or decrease of spare in the
;  // "sGtEngTlmPkt" structure defined in "gtpkts.h"
;  // structure size is 144 bytes
sgtfpsimagehdr = { sgtfpsimagehdr,$
          imagenum:	uint16,$
          yoffset:	int16,$
          zoffset:	int16,$
          spare1:	uint16,$
          numfpssamples:	uint32,$
          fpsysum:	int32,$
          fpszsum:	int32,$
          fpsysquare:	int32,$
          fpszsquare:	int32,$
          pzterrsum:	replicate(int32,3),$
          pzterrsquare:	replicate(int32,3),$
          pztdacsum:	replicate(int32,3),$
          pztdacsquare:	replicate(int32,3),$
          fpscoefs:	sgtfpscoefs,$
          spare2:	uint16 }
