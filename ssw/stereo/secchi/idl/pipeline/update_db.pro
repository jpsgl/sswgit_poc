pro update_db,fts,do_summary=do_summary
;+
; $Id: update_db.pro,v 1.2 2010/05/12 16:52:44 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : update_db
;               
; Purpose   : generate new database scripts for input FITS files; scripts will delete old rows (all tables)  
;               
; Explanation: 
;               
; Use       : IDL> 
;    
; Inputs    :  fts  STRARR  list of FITS files
;               
; Outputs   :  mysql script file(s) in $SCC_DB
;
; Keywords  :  /DO_SUMMARY  write summary file in $SCC_SUM
;
; Calls from LASCO : 
;
; Common    : SCC_REDUCE, dbms
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline (see http://durance.oamp.fr/lasco/fichiers/soft_categories.html)
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson 2009-08-27
;               
; $Log: update_db.pro,v $
; Revision 1.2  2010/05/12 16:52:44  nathan
; documentation; add insecchicalperiod call
;
; Revision 1.1  2009/08/27 17:43:58  secchia
; write fits file to db from list of fits files
;
;
;
common scc_reduce,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, resetvars, $
    	    	    ipdat, nom_tlr, prevbasename, readfileidline, wavefileidline, $
		    setupfileidline, exposfileidline, maskfileidline, ipfileidline, icerdiv2flag, $
		    rotblidlines,hbtimes,hbtemps,cebtimes,cebtemps, subdir, date_fname_utc, ispb, islz, isrt, aorb, $
		    iserror, ephemfil, att_file
common dbms,ludb, dbfile, delflag, img_seb_hdr, img_seb_hdr_ext, img_cal_bias, $
	    img_cal_led, img_cal_dark, img_hist_cmnt, img_euvi_gt    	

idline='$Id: update_db.pro,v 1.2 2010/05/12 16:52:44 nathan Exp $'
idinfo=strmid(idline,5,34)
longwait=10
shortwait=2
aorb=getenv("SCC_PLATFORM")   
    aorblc=strlowcase(aorb)
    IF aorb EQ 'A' THEN BEGIN
    ; these are used in call to sccdopolar
    	aheadf=1
	behindf=0
	scname='ahead'
    ENDIF ELSE BEGIN
    	aheadf=0
	behindf=1
	scname='behind'
    ENDELSE

;defines compression
    ssss = scc_read_ip_dat(IPver=IPver)
    ipdat=replicate('ERROR',256)
    for i=0,n_elements(ssss.ip_description)-1 do ipdat[i]=ssss[i].ip_description

   z=where(strmid(ipdat,0,6) eq 'H-comp')
   ipcmprs=ipdat
   ipcmprs(z)='HC'+strmid(ipdat(z),strlen(ipdat(z(0)))-2,1)
   ipcmprs(where(ipdat eq 'No Compression')) = 'NONE'
   ipcmprs(where(ipdat eq 'Rice Compression')) = 'RICE'
   ipcmprs(where(ipdat eq 'Header Only')) = 'HDRO'

dbroot='00000000cdbx'
FOR nf=0,n_elements(fts)-1 DO BEGIN
print,nf

      yyyymmdd=strmid(fts(nf),strpos(fts(nf),'.fts')-21,8)
      aorblc=strlowcase(strmid(fts(nf),strpos(fts(nf),'.fts')-1,1))
      sccattic=getenv('sci'+aorblc)+'/'+strmid(yyyymmdd,2,6)+'/'
      if (yyyymmdd ne strmid(dbroot,0,8)) then begin
        pipeline ='ud'
        dbroot=yyyymmdd
        dbroot=dbroot+'cdb'+ aorblc
        lulog=9
;        unblock_cmd = concat_dir(getenv('UTIL_DIR'),'unblockSciFile')
;open log file need for write db_script
        if nf eq 0 then begin
    	   logfile= concat_dir(getenv('SCC_LOG'),'scc'+getenv('SC')+pipeline+dbroot+'.log')
	   openw,lulog,logfile,/append
;	printf,lulog,version
	   printf,lulog,' '
	;text=readlist('sebhdr_struct.inc')
	   text=[';*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO', $
	      ';*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Thu May 19 15:11:05 2005']
	   printf,lulog,'NOTE: sebhdr info is static -- need to generate from sebhdr4_struct.inc'
	   printf,lulog,text[0]
	   printf,lulog,text[1]
	   printf,lulog,'starting log file at '+systime()
	   printf,lulog,' '
	   printf,lulog,'SEB_IMG	',getenv('seb_img')
	   printf,lulog,'SCI_SOURCE ',getenv('SCI_SOURCE')
	   for i=0,n_Elements(tels)-1 do printf,lulog,'SECCHI	',getenv('SECCHI')+'/*/'+tels(i)+'/'+yyyymmdd
	   printf,lulog,'SCC_ATTIC ',sccattic
;	printf,lulog,unblock_cmd
;	printf,lulog,uncompr_cmd,' or '
;	printf,lulog,huncompr_cmd,' or '
;	printf,lulog,iuncompr_cmd

	   printf,lulog,'IDL_PATH	',getenv('IDL_PATH')
	   printf,lulog,'TABLES	',getenv('TABLES')
	   printf,lulog,'PT    	',getenv('PT')
	   printf,lulog,'DATE: ',yyyymmdd
	   printf,lulog,'==============================='
        endif
	; go ahead and make db files for SPWX or PB, even though not (yet) using them
        SPAWN,'hostname',machine,/sh  
         machine = machine(N_ELEMENTS(machine)-1)
        src=strmid(machine,0,1)+rstrmid(machine,0,1)
	dbfile=concat_dir(getenv('SCC_DB'),'scc'+getenv('SC')+pipeline+src+dbroot+'sql.script')
	openw,ludb,dbfile,/get_lun
	dbfstat=fstat(ludb)
	IF dbfstat.size EQ 0 THEN BEGIN
	; only at beginning of each script file
	    printf,ludb,'use secchi_flight;'
	    printf,ludb,'start transaction;'
	ENDIF
	get_utc,ustarttime
	starttime=utc2str(ustarttime,/ecs)
	printf,ludb,'# starting dbfile at '+starttime
	close,ludb
	free_lun,ludb
    endif
    print,fts[nf]
    img=sccreadfits(fts[nf],scch,/nodata)
 
        img_dir='img'
    	case strmid(scch.filename,16,1) of
    	    'k' : IF (scch.detector NE 'HI1') AND (scch.detector NE 'HI2') THEN img_dir='cal'   ; DARK
      	    'e' : img_dir='cal'   ; LED
      	    'c' : img_dir='cal'   ; CONT
    	 else : 
    	endcase
    	if(trim(strlowcase(scch.detector)) eq 'cor1' or trim(strlowcase(scch.detector)) eq 'cor2')then begin
    	    case strmid(scch.filename,16,1) of
    	    	'n' : img_dir='seq'
    	    	's' : img_dir='seq'
	    	else:
    	    endcase
    	endif

;eDoorStatus : $
;[   'CLOSED',	$
;    'IN_TRANSIT',	$
;    'OPEN ',	$
;    'OFF',	$
;    'ERROR',    $
;    replicate('invalid',250), $
;    'NO_DATA' ]   }
    
    ; SCIP door CLOSED or IN_TRANSIT go in cal; HI door CLOSED (only) go in cal
    ; disabled door results in OFF, which for now we assume is OPEN - nbr, 090506
    IF ((scch.doorstat EQ 1) AND  (scch.detector NE 'HI1') AND (scch.detector NE 'HI2')) OR $
        (scch.doorstat EQ 0)  $
       THEN img_dir='cal' 

    ; Check for special events
    IF insecchicalperiod(scch) THEN img_dir='cal'
    
    	case trim(strlowcase(scch.detector)) of
    	    'hi1' : BEGIN
	    	 SUBdir=img_dir+'/hi_1'
	     END
    	    'hi2' : BEGIN
	    	    SUBdir=img_dir+'/hi_2'
	    END
    	    else  : SUBdir=img_dir+'/'+trim(strlowcase(scch.detector))
    	endcase

  	    sfx='.'+strmid(scch.filename,18,2)
	    sumfile= concat_dir(getenv('SCC_SUM'), 'scc'+strmid(scch.filename,20,1) $
	    	    	    	    	    	    +strmid(scch.filename,0,6) $
						    +'.'+img_dir+sfx)
        unblock_image_hdrs,sccattic+scch.fileorig ;       spawn, unblock_cmd+' '+infile,/sh, unblockresult
	delflag=1
        write_db_scripts, fts[nf]

	if keyword_set(do_summmary) then sccwritesummary,scch,sumfile
	
ENDFOR ; each image

if (datatype(lulog) ne 'UND')then close,lulog

end
