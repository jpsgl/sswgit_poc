function getscccrpix, scch, aorb, dateobs, UPDATEHEADER=updateheader, FAST=fast, LOUD=loud
;+
; $Id: getscccrpix.pro,v 1.11 2016/04/06 17:12:30 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : getscccrpix
;               
; Purpose   : enter values of CRPIX1[2] in header structure
;               
; Explanation: 
;               
; Use       : IDL> crpix=getsccsecpix(scch) 
;    
; Inputs    : 	scch	STC	header structure -or-
;		scch 	STRARR	FITS header 
;               
; Outputs   : pixel array FITS coordinates [x,y] 2 element FLTARR
;
; Keywords  :	UPDATEHEADER	If set then update CRPIX1[2], HISTORY in scch
;               
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: detector, obsrvtry, sumrow, sumcol, p1col, p1row, rectify, ipsum, cdelt1 
;   	    	must be defined in input structure or array
;
; Side effects: 
;               
; Category    : pipeline, calibration, astrometry
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2006
;               
; $Log: getscccrpix.pro,v $
; Revision 1.11  2016/04/06 17:12:30  mcnutt
; added post conjuntion crpix vals for EUVI-A
;
; Revision 1.10  2011/09/19 22:12:23  nathan
; Implement model of EUVI sun center during rectification; moved ipsum
; correction to after rectification
;
; Revision 1.9  2009/12/14 18:49:22  nathan
; comments only
;
; Revision 1.8  2007/03/30 19:31:59  nathan
; fix error introduced by last edit (bug 125)
;
; Revision 1.7  2007/03/22 13:55:28  nathan
; fix computation of crpix for binned SCIP images
;
; Revision 1.6  2007/03/15 15:24:05  nathan
; add FAST to getsccpointing call
;
; Revision 1.5  2007/03/13 19:12:04  secchib
; nr - do not wait if /fast
;
; Revision 1.4  2007/03/01 22:45:18  nathan
; Implement getsccpointing.pro (Bug 27); add SC_[yaw,pitch,roll] (Bug 91)
;
; Revision 1.3  2007/02/13 15:01:16  nathan
; fix binned values (Bug 82); added functionality for rectified images
;
; Revision 1.2  2007/02/01 23:16:38  nathan
; add some comments
;
; Revision 1.1  2006/09/20 21:36:56  nathan
; first draft
;
;-            

loud=keyword_set(LOUD)
scchtype=datatype(scch) 
IF scchtype EQ 'STC' THEN BEGIN
	tel=scch.detector
	aorb=strmid(scch.obsrvtry,7,1)
	sumcol=scch.sumcol
	sumrow=scch.sumrow
	ipsum=scch.ipsum
	p1col=scch.p1col
	p1row=scch.p1row
	IF scch.rectify EQ 'T' THEN rctfd=1 ELSE rctfd=0
	r1col=scch.r1col
	r1row=scch.r1row
	naxis1=scch.naxis1*2^(ipsum-1)
	naxis2=scch.naxis2*2^(ipsum-1)
	dateobs=scch.date_obs
	cdelt=scch.cdelt1
ENDIF ELSE $
IF n_elements(scch) GT 1 THEN BEGIN
	tel=	fxpar(scch,'DETECTOR')
	aorb=strmid(fxpar(scch,'OBSRVTRY'),7,1)
	sumcol=	fxpar(scch,'SUMCOL')
	sumrow=	fxpar(scch,'SUMROW')
	ipsum=	fxpar(scch,'IPSUM')
	p1col=	fxpar(scch,'P1COL')
	p1row=	fxpar(scch,'P1ROW')
	IF fxpar(scch,'RECTIFY') EQ 'T' THEN rctfd=1 ELSE rctfd=0
	r1col=	fxpar(scch,'R1COL')
	r1row=	fxpar(scch,'R1ROW')
	naxis1=	fxpar(scch,'NAXIS1')*2^(ipsum-1)
	naxis2=	fxpar(scch,'NAXIS2')*2^(ipsum-1)
	dateobs=fxpar(scch,'DATE-OBS')
	cdelt=	fxpar(scch,'CDELT1')
ENDIF ELSE BEGIN
; this part does not work yet!
	tel=strupcase(scch)
	aorb=strupcase(aorb)
	rctfd=1
    	IF (loud) THEN message,'Assuming full res rectified image.',/info
	IF tel EQ 'EUVI' THEN BEGIN
	    IF n_params() LT 3 THEN read,'EUVI: Please enter date_obs:',dateobs
	ENDIF
ENDELSE

info="$Id: getscccrpix.pro,v 1.11 2016/04/06 17:12:30 mcnutt Exp $"
len=strlen(info)
histinfo=strmid(info,1,len-2)
scxy=[0.,0.] 	; Error

ut=anytim2utc(dateobs)
mjd=ut.mjd+ut.time/86400000d
if dateobs gt '2015-07-01T00:00:00' then post_conj=1 else post_conj=0
CASE tel OF
'COR1': CASE aorb OF
	'A': scxy=[1070.39, 1025.58]+[1,3]   ; occulter center
	'B': scxy=[1069.52, 1017.72]+[1,3]   ; occulter center
	; 3 is because values are based on readout tables where first row was 3 not 1
	; Based on email from W.Thompson, 9/18/06
	; P1COL=1, P1ROW=3
	ELSE:
	ENDCASE
'COR2': CASE aorb OF
	'A': scxy=2*[510.4,512.5]+[51,3]   ; +/-2,  occulter center
	'B': scxy=2*[507.9,509.3]+[51,3]   ; +/-2,  occulter center
	; 3 is because values are based on readout tables where first row was 3 not 1
	; Based on email from S.Plunkett, 9/19/06
	; P1COL=51,P1ROW=3
	ELSE:
	ENDCASE
'EUVI': BEGIN
		CASE aorb OF	; for euvi should be sun center
			'A': scxy=[1024.5,1024.5]+[51,3]   ; array center (should be optical axis)
			'B': scxy=[1024.5,1024.5]+[51,3]   ; array center (should be optical axis)
			; 3 is because values are based on readout tables where first row was 3 not 1
			; P1COL=51,P1ROW=3
			; J-P is working on an algorithm for this, 2/1/07
			ELSE:
	    ENDCASE
	    ;ypr=getsccpointing(tel,aorb,dateobs, TOLERANCE=60, FAST=fast )
	    ; returns dx,dy in arcsec of sun center from optical axis
		; this offset should be applied to RECTIFIED coordinates
	END 
'HI1':  CASE aorb OF
	'A': scxy=[1024.5,1024.5]+[51,1]   ; array center (should be optical axis)
	'B': scxy=[1024.5,1024.5]+[51,1]   ; array center (should be optical axis)
	; P1COL=51,P1ROW=1
	ELSE:
	ENDCASE
'HI2':  CASE aorb OF
	'A': scxy=[1024.5,1024.5]+[51,1]   ; array center (should be optical axis)
	'B': scxy=[1024.5,1024.5]+[51,1]   ; array center (should be optical axis)
	; P1COL=51,P1ROW=1
	ELSE:
	ENDCASE
ELSE: print,'%% GETSCCCRPIX: Error - Telescope "',tel,'" not found'
ENDCASE

cx=scxy[0]-p1col
cy=scxy[1]-p1row
scxy=[cx,cy]
; this should take care of HI case also -- nbr, 3/20/07
;IF strpos(tel,'HI') GE 0 THEN $
;;-special case for HI since must fall on pixel boundary
;    scxy=[fix(cx)+0.5, fix(cy)+0.5] $
;ELSE scxy=[cx,cy]

IF (rctfd) THEN BEGIN
	scxy0=scxy
	; NOTE: naxis values in these equations need to be PRE-RECTIFIED, so they
	; should match the scch values in secchi_rectify. nbr,2/13/07
    IF aorb EQ 'A' THEN BEGIN
    	CASE tel OF
		'EUVI': begin
			;scxy[0]=naxis1-scxy0[1]+1
			;scxy[1]=naxis2-scxy0[0]+1
			;
			; nr - These values derived 2011/03/15 from 1324 days using find_cos_params.pro
			;
                        if post_conj then scxy[0]=1025.75 else scxy[0]=1021.81
			; for EUVI-A, crpix1 is constant (???)
			
			mjd0	=54298.574
			period	=345.16667 ; days
			radians=(2*!pi/period)*(mjd-mjd0)
                        if post_conj then a0=1124.74 else a0=926.298
			a1  	=0.532501
			scxy[1]=a0+a1*cos(radians)


	    		end
		'COR1': Begin
			scxy[0]=scxy[1]
			scxy[1]=naxis2-scxy0[0]+1
	    		end
		'COR2': Begin
			scxy[0]=naxis1-scxy0[1]+1
			scxy[1]=scxy0[0]
	    		end
		'HI1':	begin
	    		end
		'HI2':	begin
	    		end
    	ENDCASE
    ENDIF ELSE $
    IF aorb EQ 'B' THEN BEGIN
    	CASE tel OF
    	    	'EUVI': begin
			;scxy[0]=scxy[1]
			;scxy[1]=naxis2-scxy0[0]+1
			;
			; nr - These values derived 2011/03/15 empirically from 1324 days using find_cos_params.pro
			;
			mjd0	=54509.574
			period	=393.99897 	 ; days
			radians=(2*!pi/period)*(mjd-mjd0)
			a0  	=1033.70 
			a1  	=0.0449829 
			scxy[0]=a0+a1*cos(radians)
			
			mjd0	=54318.574 
			period	=391.99897  	 ; days
			radians=(2*!pi/period)*(mjd-mjd0)
			a0  	=1050.36  
			a1  	=3.13000  
			scxy[1]=a0+a1*cos(radians)
	    		end
		'COR1': Begin
			scxy[0]=naxis1-scxy0[1]+1
			scxy[1]=scxy[0]
	    		end
		'COR2': Begin
			scxy[0]=scxy[1]
			scxy[1]=naxis2-scxy0[0]+1
	    		end
		'HI1':	begin
			scxy[0]=naxis1-scxy0[0]+1
			scxy[1]=naxis2-scxy0[1]+1
	    		end
		'HI2':	begin
			scxy[0]=naxis1-scxy0[0]+1
			scxy[1]=naxis2-scxy0[1]+1
	    		end
    	ENDCASE
    ENDIF    
	IF (loud) THEN BEGIN
	    print,'Image is rectified, so changed values from'
	    print,scxy0,' to ',scxy
	ENDIF
	; only apply S/C offset if image is already rectified
	;IF tel EQ 'EUVI' THEN BEGIN
	;	IF (loud) THEN message,'Changing '+string(scxy[0])+string(scxy[1])+' by '+string(ypr[0]/cdelt)+string(ypr[1]/cdelt),/info
	;	scxy=scxy+(ypr[0:1]/cdelt)
	;	IF ~keyword_set(FAST) THEN wait,2
	;ENDIF
ENDIF ELSE IF (loud) THEN message,'Using CCD center for CRPIX.',/info

scxy= (scxy-0.5)/(2^(ipsum+sumcol-2)) +0.5

; cdelt must be correct for image

IF keyword_set(UPDATEHEADER) THEN BEGIN
    IF scchtype EQ 'STC' THEN BEGIN
    	scch.crpix1=scxy[0]
    	scch.crpix2=scxy[1]
    	scch.crpix1a=scch.crpix1
    	scch.crpix2a=scch.crpix2
	scch=scc_update_history(scch, histinfo)
    ENDIF ELSE print,'%% GETSCCCRPIX: Warning - /UPDATEHEADER not implemented for FITS header'
ENDIF	

return,scxy

end
