;+
; $Id: secchi_reduce.pro,v 1.432 2017/11/20 20:48:38 nathan Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : secchi_reduce
;               
; Purpose     : scan input directory for 8.3 files and do pipeline processing
;               
; Explanation : 
;               
; Use         : IDL> secchi_reduce
;    
; Inputs      : 
;               
; Outputs     : 
;
; Keywords :	WAITRATE    Set to number of seconds to wait before checking for new 
;   	    	    	    science files.
;   	    	/NOWAIT		(deprecated)
;   	    	/NOTELDIR	Do not put day directories in telescope subdirs
;               /FAST		If set, skip internal waits
;               _extra		set to indicate pre flight test for SPICE ie sim2='sim2'
;   	    	/NO_SPICE	If set add do NOT compute SPICE coords to image header
;   	    	IPver		set Flight software IP version for images v46 = V4.6
;		/TESTLZ		Set to use islz=1 for testing when lz not in $SECCHI
;		/NOHBTEMPS	Set to skip temperatures in header
;		/NOSUMMARY	Set to skip writing to summary file; also writes delete commands
;			    	in sql script.
;		/NOEXTHDR	Skips parts that write extended ASCII header
;   	    	/NOSORT		Do not sort by timestamp
;   	    	/HDR_ONLY	Get image from outfile rather than scifile
;			** WARNING ** does not work yet with HI
;   	    	/GZIP		Copy output file to $SECCHI_GZ and gzip it there
;   	    	/NOPRETTIES 	Do not run make_daily_pretties or sccdopolar
;   	    	/NOBKG	    	Do not update SECCHI_BKG 
;		ENDBADAH=	time to start reloading stereo_spice if att_file='NotFound'
;               /NOEVENT_NOTIFY Default is to send message to sccevent@cronus if there is
;   	    	    	    	an event detection. Set to disable. Set to 'a' or 'b' to disable one
;   	    	/FIND_HDR_SCRIPT    Search for shell script to modify header.
;   	    	/RESUME     	Use cadence.sav to determine what file in input list to start with
;               /nomvattic      dont move sci file to sci(aorb) allows for space issues with secchi attic
;   	    	/IGNORE_MASK	Do not do scc_apply_mask.pro
;   	    	/GET_VERSION	By default, assumes image header version is 4; set this to call rdSh to get header version from raw .hdr file
;
; Calls from LASCO : getenv_slash.pro
;
; Common      : 
;               
; Restrictions: Requires env variables defined in spacecraft- and pipeline-specific *.env 
;   	    	file in $SSW_SECCHI/idl/pipeline
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2004
; -              
; Modified    :
;
; $Log: secchi_reduce.pro,v $
; Revision 1.432  2017/11/20 20:48:38  nathan
; check for badhdr and skip
;
; Revision 1.431  2017/10/18 16:17:07  secchia
; correct mpeg movie directories
;
; Revision 1.430  2017/08/10 21:59:52  nathan
; do /use_p0 and add /mask_occ to cor2 pretties
;
; Revision 1.429  2016/03/17 18:31:44  secchia
; delete unblocked scifiles by name not wild card
;
; Revision 1.428  2015/11/20 15:38:59  nathan
; add _extra to mpegframes call
;
; Revision 1.427  2015/08/31 13:42:02  secchia
; delete attic dir if not a directory
;
; Revision 1.426  2015/07/20 17:19:49  nathan
; change secchipipeline@cronus to mvi@nrl so it does not bounce from ssc
;
; Revision 1.425  2015/02/03 19:34:57  secchia
; if sidelobe do day_med yyyymmdd-1
;
; Revision 1.424  2014/09/22 18:44:10  secchia
; write daily pretties for d-1 if sidelobe
;
; Revision 1.423  2014/09/09 20:15:08  secchia
; use sidelobe keyword to skip sccdoplar
;
; Revision 1.422  2014/09/09 19:37:59  secchia
; added sidelobe keyword to bypass image count for daily pretties
;
; Revision 1.421  2014/08/14 13:24:24  secchia
; added more SpW check for hilow HI images
;
; Revision 1.420  2014/08/06 21:15:14  hutting
; defines hilow directory using predefined seb_img to avoid using root
;
; Revision 1.419  2014/08/06 14:22:06  secchia
; send outdir and img_dir into scc_sidelobe_update_hdr to be corrected
;
; Revision 1.418  2014/08/05 21:16:07  hutting
; added sidelobe_update_hdr
;
; Revision 1.417  2014/08/05 19:05:28  secchia
; correction to work with HI hilow spw image
;
; Revision 1.416  2014/07/31 14:16:58  secchia
; added quick fix for sidelobe HI psummed images
;
; Revision 1.415  2014/01/08 19:55:20  mcnutt
; added check for reload to correct extended hdrs and exposure times for COR2 SSR1 and SSR2 test images
;
; Revision 1.414  2013/12/23 20:34:08  nathan
; by default do not call rdSh; make optional with /GET_VERSION
;
; Revision 1.413  2013/11/27 19:12:51  secchia
; nr - update end-of-day section to do pb or lz only
;
; Revision 1.412  2013/11/22 17:30:25  nathan
; do end-of-day processes also if ispb
;
; Revision 1.411  2013/11/21 18:06:07  secchia
; add checks for islz before setting sdspath and looking for attitude info
;
; Revision 1.410  2013/11/20 22:44:45  nathan
; update call to scc_mk_daily_med
;
; Revision 1.409  2013/10/22 16:21:06  nathan
; check TESTLZ for touchdatedir
;
; Revision 1.408  2013/10/21 23:06:08  nathan
; implement /IGNORE_MASK and check TESTLZ for outputdir
;
; Revision 1.407  2013/08/21 14:29:53  secchia
; nr - change secchipipeline@nrl to @cronus
;
; Revision 1.406  2013/04/05 19:33:25  nathan
; remove swap endian for image if darwin
;
; Revision 1.405  2012/12/21 19:10:27  secchia
; update exposure estimated time for ob_id 1790
;
; Revision 1.404  2012/12/06 19:30:32  secchia
; nathan - if input list, sort by filename and not by time because in
; reprocess input when sorted by timestamp was out of order
;
; Revision 1.403  2012/11/28 19:00:34  nathan
; change email from cronus.nrl to nrl
;
; Revision 1.402  2012/08/14 12:48:07  secchia
; added nomvattic keyword
;
; Revision 1.401  2012/08/09 20:25:09  secchib
; nr - add / to all mv commands to make directory explicit
;
; Revision 1.400  2012/07/18 17:26:36  secchia
; will call scc_move_attic if notreproc
;
; Revision 1.399  2012/07/17 12:18:47  secchib
; added call to scc_move_attic after the days processing is complete
;
; Revision 1.398  2012/07/16 18:48:39  secchib
; commented out lines to create link for attic
;
; Revision 1.397  2012/05/22 20:45:34  secchia
; nr - do daily_ssr1_report if COR2 not EUVI
;
; Revision 1.396  2012/04/09 14:17:24  secchib
; do not make weekly movies if nopretties keyword is set EUVI-B only
;
; Revision 1.395  2012/04/06 19:10:16  secchia
; update VERSION in header after euvi_point
;
; Revision 1.394  2012/04/06 18:43:51  secchib
; nr - omit sccimagestats; fix link telimgdir section
;
; Revision 1.393  2012/04/05 19:22:33  secchib
; nr - try to fix mkdir error in telimgdir
;
; Revision 1.392  2012/03/15 22:10:54  secchib
; nr - stop if attic dir not found; fix outdir typo
;
; Revision 1.391  2011/12/15 16:29:28  nathan
; do HI1 daily pretty for d-1 also
;
; Revision 1.390  2011/12/14 23:22:28  nathan
; fix mailx command and do not do scc_missing_blocks if ROI is used
;
; Revision 1.389  2011/11/25 20:31:02  secchib
; nr - adjust some diagnostic waits
;
; Revision 1.388  2011/11/16 19:40:39  nathan
; do not do HI daily_pretties minus30, in anticipation of implementation of updatehibkg
;
; Revision 1.387  2011/11/14 17:22:52  nathan
; Separate decomp error messages from other messages and put in decomp.txt.
; Non-decomp messages will have a new email subject.
;
; Revision 1.386  2011/09/15 21:57:35  nathan
; link comment in scc_icerdiv2 and secchi_reduce to check if mod was made in icerdiv2
;
; Revision 1.385  2011/09/07 16:02:18  secchia
; cd to seb_img before file_search
;
; Revision 1.384  2011/08/15 19:48:21  secchib
; typo
;
; Revision 1.383  2011/08/15 19:38:30  nathan
; put set div2corr=T lost in rev 1.377
;
; Revision 1.382  2011/08/11 18:49:54  secchib
; went back to strmid for hi cosmecs
;
; Revision 1.381  2011/08/11 13:20:14  secchib
; corrected syntax error
;
; Revision 1.380  2011/08/11 13:12:39  mcnutt
; corrected cosmics in hi extended header
;
; Revision 1.379  2011/08/11 12:22:29  secchib
; moved weekly mpeg to end of day
;
; Revision 1.378  2011/08/08 18:38:18  mcnutt
; added 5 minutes wait and repeat file search for lz pipeline
;
; Revision 1.377  2011/07/26 15:05:35  secchib
; nr - move reduce_statistics to just before FITS file is written to avoid doing it on HI high-bit image
;
; Revision 1.376  2011/07/20 15:35:12  nathan
; do COR2 type DBL daily medians
;
; Revision 1.375  2011/06/29 19:30:23  nathan
; changed maillist to secchipipeline@cronus
;
; Revision 1.374  2011/06/07 15:58:15  secchib
; nr - add some reprocess exceptions
;
; Revision 1.373  2011/05/27 16:32:06  secchia
; nr - fix error in ** link Error ** section
;
; Revision 1.372  2011/04/06 20:49:22  nathan
; test for crs2 before putting cosmics in header
;
; Revision 1.371  2011/02/04 16:51:31  secchia
; nr - open lulog before skipscifile
;
; Revision 1.370  2011/01/14 14:14:16  secchib
; moved touchdatelink link to it own pro
;
; Revision 1.369  2011/01/13 19:55:06  nathan
; /noevent_notify if /testspw
;
; Revision 1.368  2011/01/12 19:43:08  secchia
; run scc_event_track if not spw to file header from spw log file
;
; Revision 1.367  2011/01/11 19:43:44  mcnutt
; added call to scc_event_track for spw event notifications
;
; Revision 1.366  2011/01/04 18:36:24  mcnutt
;  modified event detect for IP V1.11
;
; Revision 1.365  2010/12/14 17:14:13  secchia
; nr - use errwait=2 for error wait interval
;
; Revision 1.364  2010/12/14 17:01:45  secchia
; nr - close folog
;
; Revision 1.363  2010/12/14 16:25:53  secchia
; correct aorb in fog filename
;
; Revision 1.362  2010/12/14 13:09:59  secchib
; moved defining foglogname down so aorb is defined
;
; Revision 1.361  2010/12/14 12:53:46  mcnutt
; added weekly movies for for all telescope pairs
;
; Revision 1.360  2010/12/09 20:48:57  nathan
; Moved attmsg.txt email notification from secchi_reduce to make_scc_hdr;
; should only email 1x/day
;
; Revision 1.359  2010/12/09 20:28:48  nathan
; add folog
;
; Revision 1.358  2010/12/02 14:31:12  secchia
; added doall keyword to HI2 and COR2 daily pretties with minus30
;
; Revision 1.357  2010/11/29 17:00:50  secchib
; added no event detect keyword
;
; Revision 1.356  2010/08/30 15:04:27  secchib
; nr - always call load_stereo_spice with /reload
;
; Revision 1.355  2010/08/06 19:17:46  nathan
; gracefully skip file with empty hdr/tlr (beacon pipeline error)
;
; Revision 1.354  2010/08/06 18:49:31  nathan
; fix case where input has empty string
;
; Revision 1.353  2010/08/04 18:23:57  secchib
; moved COR and HI display combined movie to HI2 B
;
; Revision 1.352  2010/08/04 12:37:38  secchia
; change sc to lowercase for event detect table selection
;
; Revision 1.351  2010/07/21 17:23:42  mcnutt
; added noshell to spawn in thouchlink
;
; Revision 1.350  2010/07/08 18:04:47  secchib
; nr - one more time
;
; Revision 1.349  2010/07/08 17:41:09  secchib
; nr - wasnot quite working...
;
; Revision 1.348  2010/07/07 15:21:23  mcnutt
; sends saved secchi hdr to make_scc_ext_hdr for COR2 and tests thresh table
;
; Revision 1.347  2010/07/02 22:19:55  nathan
; Touchdatelink: use existing symlink instead of env var; Make sortscilist pro
; to sort file list by counter+time, and do this with input list as well.
;
; Revision 1.346  2010/07/01 15:16:17  secchib
; nr - change SDS_FIN_DIR from -45 days to -62 days
;
; Revision 1.345  2010/06/28 19:52:49  nathan
; implement correct BLANK value for UINT
;
; Revision 1.344  2010/06/23 16:14:22  secchib
; nr - comment out sort_hisam call; debug resetvars=1
;
; Revision 1.343  2010/06/08 15:33:32  nathan
; temporary workaround for BLANK keyword issue
;
; Revision 1.342  2010/06/08 14:14:41  mcnutt
; added keyword planets to scc_daily_pretties
;
; Revision 1.341  2010/05/06 16:41:34  nathan
; call insecchicalperiod()
;
; Revision 1.340  2010/04/19 22:01:09  nathan
; fix typo in swap_endian_inplace near line 2270
;
; Revision 1.339  2009/12/02 23:31:54  nathan
; Init sttime; do byte-swap on image only if darwin intel platform.
;
; Revision 1.338  2009/11/18 22:25:37  nathan
; Mod so works on MacOS i386. Commented image=long and uncommented swap_if_little_endian after uncompress. Needs to be tested with linux and solaris!
;
; Revision 1.337  2009/10/16 16:41:34  secchia
; nr - aorb was missing from links.sh filename
;
; Revision 1.336  2009/10/13 21:27:58  secchia
; nr - make sure spawn commands use /sh so output does not include .tcsh echos
;
; Revision 1.335  2009/10/13 13:08:11  mcnutt
; added extra check for link so not to create a link inside image directory
;
; Revision 1.334  2009/09/14 12:47:31  mcnutt
; corrected HISAM pcol1
;
; Revision 1.333  2009/09/03 20:34:21  secchia
; nr - ensure sci_attic directory is created
;
; Revision 1.332  2009/09/02 22:43:33  nathan
; add /pipeline to daily_ssr1_report call to pass to daily_apid_seqprt to write save file
;
; Revision 1.331  2009/08/26 11:50:35  mcnutt
; changed cadence for scc_pngplay to work with new schedule blocks
;
; Revision 1.330  2009/08/20 16:45:19  nathan
; double wait time to 20 min. for realtime (spw) data
;
; Revision 1.329  2009/08/17 15:10:13  nathan
; fix iperror syntax error
;
; Revision 1.328  2009/08/14 12:10:19  mcnutt
; removed p1col adj for hispw
;
; Revision 1.327  2009/08/06 20:56:45  nathan
; make scidir link for lz only
;
; Revision 1.326  2009/08/06 20:04:20  nathan
; workaround for HISPW test
;
; Revision 1.325  2009/08/06 19:40:20  nathan
; check to see if scidir link exists
;
; Revision 1.324  2009/07/28 22:55:22  nathan
; updated for 2nd location of SCC_ATTIC
;
; Revision 1.323  2009/07/28 15:55:52  secchib
; add /DOALL; re-order mvicam
;
; Revision 1.322  2009/06/30 12:54:23  mcnutt
; corrected p2col for new HISPW image
;
; Revision 1.321  2009/06/22 17:37:48  nathan
; fix incorrect of use of pipeline var in T/F tests (for link)
;
; Revision 1.320  2009/06/22 16:23:42  secchib
; changed swap_endian from v1.317 to skip if icer
;
; Revision 1.319  2009/06/17 17:22:27  mcnutt
; change cor/hi weekly movie to use hi2 stars
;
; Revision 1.318  2009/06/11 18:37:14  secchia
; nr - modify keeplooking logic so endofday process is done for filename input
;
; Revision 1.317  2009/06/10 21:08:54  nathan
; added swap_endian for imgstat.size GE (2L*scch.naxis1*scch.naxis2)
;
; Revision 1.316  2009/06/10 20:21:52  nathan
; always do spice unless /no_spice
;
; Revision 1.315  2009/06/03 21:00:24  nathan
; Autoset keywords for pipeline=spw; only dospice if islz; further restrict
; FIND_HDR_SCRIPT actions
;
; Revision 1.314  2009/06/02 18:51:09  mcnutt
; added script to handle ihsam images
;
; Revision 1.313  2009/05/14 18:07:32  nathan
; only do daydir linking if pipeline is LZ
;
; Revision 1.312  2009/05/06 16:24:58  nathan
; updated DOORSTAT logic and comments to correctly handle EUVI disabled door
;
; Revision 1.311  2009/04/15 20:28:29  secchia
; nr - halt program if link (or dir) exists before creating it
;
; Revision 1.310  2009/04/10 19:28:12  secchib
; nr - set keeplooking=0 for last file in input list
;
; Revision 1.309  2009/04/09 22:30:16  secchia
; nr - fix mistake in previous
;
; Revision 1.308  2009/04/09 18:51:32  nathan
; use polar value to determine if img_dir should = seq
;
; Revision 1.307  2009/04/01 18:08:19  secchib
; moved scc_pngplay to run after processing on themis EUVIB
;
; Revision 1.306  2009/04/01 11:50:45  secchib
; corrected scc_pngplay call
;
; Revision 1.305  2009/03/31 14:51:04  mcnutt
; change format of POC display movies
;
; Revision 1.304  2009/03/30 12:08:53  secchib
; corrected call to scc_pngplay
;
; Revision 1.303  2009/03/30 11:49:10  mcnutt
; added lines to create weekly movie after mpegframes HI-A processing only
;
; Revision 1.302  2009/03/06 17:32:55  mcnutt
; changed logic to determine if image is TB from extended table
;
; Revision 1.301  2009/03/06 17:07:39  mcnutt
; sets scch.polar to 1001 if image is onboard summed total brightness using summing buffers
;
; Revision 1.300  2009/02/27 19:45:22  nathan
; append links script file in ~/bin
;
; Revision 1.299  2009/01/23 21:40:02  secchia
; nr - fixed bug if inputfilelist; only mkdir and/or link if it does not exist
;
; Revision 1.298  2009/01/14 21:02:38  nathan
; run new pro touchdatelink to refresh symlink after processing
;
; Revision 1.297  2009/01/09 22:49:35  secchia
; nr - moved telimgdir check so does not run for hdr-only
;
; Revision 1.296  2009/01/09 21:45:00  nathan
; Requires new env var definition for telimgdir; allows spec of separate
; outdir for each tel and each type.
;
; Revision 1.295  2009/01/09 20:15:31  nathan
; added code to create symbolic links for daydir
;
; Revision 1.294  2009/01/08 14:39:28  mcnutt
; date_fname_utc to date_cmd to complete v1.293 mods
;
; Revision 1.293  2009/01/08 13:52:07  mcnutt
; corrected date_obs and avg for summed images using the image save buffers
;
; Revision 1.292  2009/01/07 23:08:29  nathan
; To speed up all processing, remove wait before processing each file, and instead
; wait only when number of found files is <3. Also will not exit automatically.
;
; Revision 1.291  2008/12/11 19:27:55  mcnutt
; calls scc_aplly_mask if mask_tbl ne none
;
; Revision 1.290  2008/11/05 19:21:56  nathan
; changed sccevent@calliope to sccevent@cronus
;
; Revision 1.289  2008/11/03 16:30:51  mcnutt
; changed scc_daily_pretties calls hi1 and 2 with  histars keyword
;
; Revision 1.288  2008/11/03 15:37:42  mcnutt
; added calls to scc_daily_pretties hi enhance hi images
;
; Revision 1.287  2008/10/07 16:54:48  nathan
; implement method to key off of imgctr to determine when schedule tables were read
;
; Revision 1.286  2008/09/15 19:01:40  secchia
; nr - committed changes from 9/4/08
;

; 9/4/08, nr - Changed /DOBKG to /NOBKG; limit effect of /NOPRETTIES to dopolar, cor2 mk_monthly min, and pretties
;
; Revision 1.285  2008/07/15 19:07:21  nathan
; reduce qual threshold for icer misslist (bug 319)
;
; Revision 1.284  2008/05/20 14:50:38  secchia
; added cor1 daily pretties copied from ssc ahead hi processing
;
; Revision 1.283  2008/04/28 13:14:53  mcnutt
; add daily pretties for yyyymmdd-30 cor2 and his only
;
; Revision 1.282  2008/04/22 18:08:42  mcnutt
; change name of hdr only files to seb_prog h instead of s for database fits file name
;
; Revision 1.281  2008/04/16 20:54:41  nathan
; implemented cor2_point.pro
;
; Revision 1.280  2008/04/16 15:30:32  secchib
; corrected Syntax error
;
; Revision 1.279  2008/04/14 18:31:30  mcnutt
; moved Error message to after make_scc_hdr to set scch values
;
; Revision 1.278  2008/04/08 11:57:16  mcnutt
; corrected image error message if exptime eq 0 and no IP error
;
; Revision 1.277  2008/03/28 17:59:54  mcnutt
; added email notification for IP_ERROR,SKIPPED, or ABORT
;
; Revision 1.276  2008/03/18 14:35:34  secchib
; add sort summary at end of batch for j=0 only
;
; Revision 1.275  2008/03/14 15:21:37  mcnutt
; moves mpegframes to line 780 to run at the start of a new day to avoid running twice
;
; Revision 1.274  2008/03/12 14:36:14  mcnutt
; added call to mpegframes if A and HI1 to create mpeg movie list files
;
; Revision 1.273  2008/02/20 18:43:40  nathan
; update cdelt in HI FITS header after running hi_point_v2 (bug 237)
;
; Revision 1.272  2008/02/08 17:23:48  secchia
; nr - updated end-of-day logic
;
; Revision 1.271  2008/02/07 13:50:55  bewsher
; Changed call to hi_point_v2
;
; Revision 1.270  2008/01/31 19:10:25  nathan
; fix type conv error in last edit and move to ahead only for njpeg msg
;
; Revision 1.269  2008/01/29 22:51:25  secchia
; nr - fix weird hanging error by separating msg strings
;
; Revision 1.268  2008/01/29 21:55:39  nathan
; debugging messages in ipmsg.txt for njpegs lists
;
; Revision 1.267  2008/01/16 21:00:04  nathan
; more of previous
;
; Revision 1.266  2008/01/16 20:49:38  nathan
; implement daily_select_njpegs for mpeg movies (Bug 275)
;
; Revision 1.265  2008/01/14 19:38:48  nathan
; changed npd threshold to 50 from 500 (bug 276)
;
; Revision 1.264  2008/01/07 23:25:26  nathan
; updated scch when EXPTIME from summed (Bug 273)
;
; Revision 1.263  2007/12/18 16:24:56  nathan
; do not send email to dennis if /delete_old
;
; Revision 1.262  2007/12/13 16:38:12  nathan
; updated expcmt for DOUBLE images (bug 268)
;
; Revision 1.261  2007/12/13 13:23:22  mcnutt
; Bug 270 COR2 double n_images
;
; Revision 1.260  2007/12/05 21:21:02  nathan
; removed mail and some writes to impmsg.txt if isrt
;
; Revision 1.259  2007/12/04 14:24:15  secchib
; added final cor summed image to SIsttime incase all other headers are missing
;
; Revision 1.258  2007/11/09 16:58:22  nathan
; div2corr set in scc_icerdiv2 (Bug 252)
;
; Revision 1.257  2007/11/02 18:40:07  nathan
; workaround to Bug 252 BIASMEAN for summed SPWX; added /DOBKG
;
; Revision 1.256  2007/10/30 14:51:26  secchib
; removed exposure adjustment for COR2 summed SPW images
;
; Revision 1.255  2007/10/26 16:16:07  mcnutt
; added final COR2 summed images to the extended header and corrected summed SPW exposure times
;
; Revision 1.254  2007/10/25 11:16:48  mcnutt
; corrected estimate and actual exposure times for cor2 summed images
;
; Revision 1.253  2007/10/16 19:29:40  mcnutt
; corrected SIbasehdr typo when begin reset to -1
;
; Revision 1.252  2007/10/02 19:35:17  secchia
; added HIxseq_m1.sav to save previous extended header incase sort is out of order
;
; Revision 1.251  2007/10/01 19:21:52  secchib
; fix nmissing bug 243
;
; Revision 1.250  2007/09/24 22:06:02  nathan
; This is the version used to reprocess SECCHI through 12/06. In includes:
; Added /RESUME; fixed EPHEMFIL call; removed history logs to decrease length;
; moved make_scc_hdr call; correct sebx(y)sum for HI images in 1996 which do
; not show summing properly; use numinseq instead of clear-hi-sumbuf to
; determine start of new HIseqtbl; always set missing block vars
;
; Revision 1.249  2007/09/18 22:46:46  nathan
; removed condition for write_db_scripts; added /FIND_HDR_SCRIPT;
; put more information in subject line for sccevents email
;
; Revision 1.248  2007/09/07 16:31:49  mcnutt
; divid hibias by 1000
;
; Revision 1.247  2007/09/06 21:03:19  secchib
; change hibias arry to long to avoid negatives
;
; Revision 1.246  2007/09/06 18:08:41  mcnutt
; fills offsetcr for HI images with average meanbias Bug 223
;
; Revision 1.245  2007/09/04 21:43:08  nathan
; implement scc_mk_all_monthly.pro
;
; Revision 1.244  2007/08/31 21:31:14  nathan
; left in debug lines ... Doh!
;
; Revision 1.243  2007/08/31 19:41:05  nathan
; slight mod to previous
;
; Revision 1.242  2007/08/31 19:29:58  nathan
; put in workaround for Bug 81 (idecomp output)
;
; Revision 1.241  2007/08/31 15:25:58  secchib
; nr - better check for nseg in idecomp
;
; Revision 1.240  2007/08/30 16:39:16  nathan
; implement parsefcmetafile.pro and new idecomp output (Bug 186)
;
; Revision 1.239  2007/08/28 21:00:35  nathan
; Update gzdestdir; implement sc_yaw,pitch for cor1 and euvi;
; implement INS_[xyr]0 (Bug 91); begin implementation of
; parsing new idecomp output (Bug 186)
;
; Revision 1.238  2007/08/24 17:09:45  mcnutt
; added call to comp_archive
;
; Revision 1.237  2007/08/22 18:39:34  nathan
; hilow write_db_scripts in secchi_hi_low_merge
;
; Revision 1.236  2007/08/20 19:00:16  nathan
; Set sdspath earlier and use in calls to make_scc_ext_tbl and hi_point_v2 (Bug 178)
;
; Revision 1.235  2007/08/14 19:09:52  mcnutt
; move make_scc_hdr to before hiseqtbl to correctly fill extend hdr
;
; Revision 1.234  2007/08/13 19:27:39  nathan
; update to do HI summary and db in secchi_reduce, and make sure all HI images go to hi_point_v2
;
; Revision 1.233  2007/08/08 17:20:36  mcnutt
; added APID report to EUVI processing
;
; Revision 1.232  2007/08/07 17:04:06  nathan
; label pipeline rz for reprocessing
;
; Revision 1.231  2007/08/03 19:44:53  nathan
; update hi_point_v2 implementation (Bug 178)
;
; Revision 1.230  2007/08/03 17:12:08  mcnutt
; Bug 210 corrects biasmean for event detecet images
;
; Revision 1.229  2007/08/01 15:37:36  secchib
; corrected hicadence in cases where we dont have two consecutive headers
;
; Revision 1.228  2007/07/25 17:02:19  nathan
; remove common stereo_spice after conflict with load_stereo_spice.pro
;
; Revision 1.227  2007/07/19 19:50:25  nathan
; fix typo near mm
;
; Revision 1.226  2007/07/19 16:29:06  nathan
; Implement hi_point_v2.pro (Bug 178); update SDS_FIN_DIR
;
; Revision 1.225  2007/07/10 15:02:43  nathan
; Add event detection notification (Bug 148)
;
; Revision 1.224  2007/06/26 22:12:03  nathan
; implement SDS_FIN_DIR for COR1 reprocessing
;
; Revision 1.223  2007/06/25 17:49:26  nathan
; add pointing logging for EUVI
;
; Revision 1.222  2007/06/21 14:42:48  nathan
; fix cebtemps conflict
;
; Revision 1.221  2007/06/20 21:58:51  nathan
; fix error in cadencefile near beginning
;
; Revision 1.220  2007/06/20 15:55:35  secchia
; nr - fix typo
;
; Revision 1.219  2007/06/20 15:09:10  nathan
; Implement ENDBADAH keyword
;
; Revision 1.218  2007/06/19 16:34:23  nathan
; put scc_daily_pretties after mk_daily_med
;
; Revision 1.217  2007/06/19 15:39:23  nathan
; add logging for CRVAL,CRPIX,CRVALA; add /DELETE_OLD
;
; Revision 1.216  2007/06/19 15:02:28  mcnutt
; added Make daily median backgrounds after daily processing Bug 188
;
; Revision 1.215  2007/06/14 16:50:20  nathan
; use base34 for icer MISSLIST (Bug 183)
;
; Revision 1.214  2007/06/13 14:56:52  mcnutt
; Bug 70 calls get_ictemps to fill CEB_T
;
; Revision 1.213  2007/06/11 15:18:15  secchia
; added if statement to skip cosmic report if missing form image
;
; Revision 1.212  2007/06/08 20:17:03  mcnutt
; corrcted extended header when files are missing Bug 182 and 179
;
; Revision 1.211  2007/06/08 15:41:09  secchia
; nr - use nsubsi not subsi[0] for subsi tests; negate subsi and addsi in case of event detect (FSW 5.03)
;
; Revision 1.210  2007/06/06 17:25:21  nathan
; Remove comments for EUVI crpix; fix writing HI headers from last update;
; do sccimagestats.
;
; Revision 1.209  2007/06/05 16:40:55  nathan
; add counter arguments for testing HI IP values; create HIfile if HI and it
; does not exist; change location of and add logging if header-only and skipping
; to writedb; add previous values of pointing info for EUVI and COR1
;
; Revision 1.208  2007/06/04 21:25:11  mcnutt
; Bug 138 fills extended header with skipped image headers and estimates exposure time when headers are missing and corrects filename when first header is missing
;
; Revision 1.207  2007/05/21 20:48:30  nathan
; check cadence.sav in case of input list (Bug 144); do not cpimgdb if /TESTLZ;
; reload stereo_spice only if not /NOWAIT
;
; Revision 1.206  2007/05/14 16:51:02  nathan
; move dospice to after hbtemps, and only do load_stereo_spice 1x if not lz
;
; Revision 1.205  2007/05/04 21:48:53  nathan
; implement cor1_point.pro; add info from proc computer to db filename
;
; Revision 1.204  2007/05/03 14:28:26  mcnutt
; Bug 100 and Bug 84 added misslist and nmissing for rice and HC
;
; Revision 1.203  2007/05/02 15:47:07  secchib
; nr - make sure to /RELOAD load_stereo_spice if att_file=NotFound (Bug 129)
;
; Revision 1.202  2007/04/27 22:55:03  secchib
; nr - do not set img_dir in secchi_rectify section
;
; Revision 1.201  2007/04/26 19:23:56  secchib
; fix typo
;
; Revision 1.200  2007/04/26 19:17:11  secchib
; nr - add /NOPRETTIES to avoid writing to earth           
;__________________________________________________________________________________________________________
;
function getgndtstfile, basehdrstruct

common scc_reduce,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, resetvars, $
    	    	    ipdat, nom_tlr, prevbasename, readfileidline, wavefileidline, $
		    setupfileidline, exposfileidline, maskfileidline, ipfileidline, icerdiv2flag, $
		    rotblidlines,hbtimes,hbtemps,cebtimes,cebtemps, subdir, date_fname_utc, ispb, islz, isrt, aorb, $
		    iserror, ephemfil, att_file
                    
filename1=strmid(string(basehdrstruct.filename),5,3)
basehdrstruct.filename=[48,48,48,48,48,48,48,48,48,48,48,48]
scilist=findfile("sw*.sci")
nlist=n_elements(scilist)
help,scilist
IF (scilist[0] NE '') THEN for ii=0,nlist-1 do begin
    openr,3,scilist[ii]
    a = assoc(3,basehdrstruct)
    basehdrcopy = a[0]
    close,3
    print,'Saw sci file ',scilist[ii],' for ',string(basehdrcopy.filename)
    printf,lulog,'Saw sci file ',scilist[ii],' for ',string(basehdrcopy.filename)
    if (strmid(string(basehdrcopy.filename),5,3) EQ filename1) THEN return,scilist[ii]
    ii=ii+1
endfor
amessage="WARNING: Spacewire file for "+filename1+" not found !!!"
print,' '
print,amessage
print,' '
wait,5
return,amessage

end
;__________________________________________________________________________________________________________
;
pro sortscilist, list

print,'Sorting by time and then by counter.'
            	fileinfo=file_info(list)
		ng=n_elements(list)
		   ; sort by time of last mod
            	msl=sort(fileinfo.mtime) 
		    ; divide into segments based on ctr jump(s), and resort based on filename counter jumps
		blen=strlen(list[0])
            	list=list[msl]
		fiseq=bntoint(strmid(list,blen-7,3),36)
		diseq=fiseq-shift(fiseq,1)
		;diseq[0]=0
		wdbig=where(diseq lt -100,ndbig)
		; there are ndbig+1 segments in list
		IF ndbig GT 0 THEN BEGIN
		    print,ndbig-1,' IS resets detected in list.'
		    wait,5
		    wdbig=[wdbig,ng]
		    FOR i=0,ndbig-1 DO BEGIN
		    	seq2=fiseq[wdbig[i]:(wdbig[i+1]-1)]
		    	sseq2=sort(seq2)
			list[wdbig[i]:wdbig[i+1]-1]=list[sseq2+wdbig[i]]
		    ENDFOR
		ENDIF ELSE BEGIN
		    print,'Not sorting files by counter.'
		    wait,5
		ENDELSE

end
;
pro secchi_reduce, infilelist, WAITRATE=waitrate, NOWAIT=nowait, NOTELDIR=noteldir, TESTLZ=testlz, TESTSPW=testspw,$
    FAST=fast, NO_SPICE=no_spice, IPver=IPver, _extra=_extra,NOHBTEMPS=NOHBTEMPS, NOSUMMARY=nosummary, $
    NOEXTHDR=noexthdr, NOSORT=nosort, HDR_ONLY=hdr_only, GZIP=gzip, NOPRETTIES=nopretties, NOEVENTDECT=noeventdect,$
    DELETE_OLD=delete_old, ENDBADAH=endbadah, NOEVENT_NOTIFY=noevent_notify, FIND_HDR_SCRIPT=find_hdr_script, $
    RESUME=resume, NOBKG=nobkg,nomvattic=nomvattic, IGNORE_MASK=ignore_mask, GET_VERSION=get_version,sidelobe=sidelobe
;_extra is need for stereo spice to load attitude files from Mission Sim2 not needed after launch


common scc_reduce;,  lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs, enums, $
;   	    	    ipdat, nom_tlr,hbtimes,hbtemps
;common stereo_spice, def_ephem, ephem, attitude
common stereo_spice_conic, mu, maxdate, conic
common dbms,ludb, dbfile, delflag, img_seb_hdr, img_seb_hdr_ext, img_cal_bias, $
	    img_cal_led, img_cal_dark, img_hist_cmnt, img_euvi_gt    	
	    ; ludb used by scc_db_insert.pro

version = '$Id: secchi_reduce.pro,v 1.432 2017/11/20 20:48:38 nathan Exp $'
len=strlen(version)
version = strmid(version,1,len-2)
SPAWN,'hostname',machine,/sh  
machine = machine(N_ELEMENTS(machine)-1)
src=strmid(machine,0,1)+rstrmid(machine,0,1)
IF keyword_set(TESTLZ) THEN maillist='nathan@cronus' ELSE $
maillist='mvi@nrl.navy.mil'

notreproc=~keyword_set(NOEVENTDECT)
ludb=0
yyyymmdd='UND'
lastyyyymmdd='UND'
lastdobsyymmdd='UND'
ymdd='UND'
lastymdd='UND'
resetvars=1
lulog=9
;lusummary=10
HI1sttime=0
HI2sttime=0
HI1cmdtime=0
HI2cmdtime=0
HISTTIME=0
sttime=0
HISCANT='F'
SIscant='F'
delflag=0	; Controls whether all database rows of date_obs are deleted first
icerdiv2flag=0
iserror=0   	; Image Scheduling error / skipped image
scitelvals=['HI1','COR2','COR1','EUVI','HI2','HI1','COR2','COR1','EUVI','HI2']

IF keyword_set(WAITRATE) THEN waittime = waitrate ELSE waittime = 60
keeplooking=1
seb_img = getenv ('SEB_IMG')
destdir = getenv ('SECCHI')

gzdestdir=getenv ('SECCHI_GZ')
IF gzdestdir NE '' and keyword_set(GZIP) THEN dogz=1 ELSE dogz=0
;sci_attic = getenv_slash('SCC_ATTIC')
sci_attic = getenv_slash('SCC_ATTIC')
;aorb=getenv('SCC_PLATFORM')
;sci_attic= '/home/reduce/SECCHI/sci/attic/sci'              ; *********KB
unblock_cmd = concat_dir(getenv('UTIL_DIR'),'unblockSciFile')
;unblock_cmd= '/home/reduce/SECCHI/unblockSciFile'           ; *********KB
uncompr_cmd = 	concat_dir(getenv('UTIL_DIR'),'ricerecon64 ')
huncompr_cmd =  concat_dir(getenv('UTIL_DIR'),'hd64 -v ')
iuncompr_cmd =  concat_dir(getenv('UTIL_DIR'),'idecomp -v ')
cpdb_cmd =  concat_dir(getenv('ITOS_GROUPDIR'),'bin/cpimgdb ')
source = getenv ('SCI_SOURCE')	; a string: p, r, or 0
if datatype(scch) NE 'STC' THEN scch = def_secchi_hdr()

cadencefile= concat_dir(getenv('SCC_LOG'),'cadence.sav')
IF strpos(seb_img,'redo') GT 0 THEN Begin
	cmd='rm '+cadencefile
	print,'REDO: ',cmd
	wait,5
	spawn,cmd,/sh
; first CADENCE value will be incorrect
ENDIF
; check savefile for last-processed file if input list
if (findfile(cadencefile) NE '' and n_params() GT 0) and keyword_set(RESUME) then begin
	restore, cadencefile
	;From make_scc_hdr.pro: save,filename=cadencefile,cadence,iphicrs,basename
	lastsci=where(strpos(infilelist,basename) GE 0,nlast)
	IF nlast GT 0 THEN BEGIN
		message,'Resuming where previously stopped with '+infilelist[lastsci[0]],/info
		wait,10
		infilelist=infilelist[lastsci[0]:n_elements(infilelist)-1]
	ENDIF
endif  

islz=0
ispb=0
isrt=0
dospice=1
IF keyword_set(NO_SPICE) THEN dospice=0
pipeline='spw'

IF strpos(destdir,'/lz/') GE 0 OR keyword_set(TESTLZ) THEN BEGIN
    islz=1
    pipeline='lz'
    IF keyword_set(DELETE_OLD) THEN pipeline ='rz'
    ;IF keyword_set(TESTLZ) THEN BEGIN
    ;  nopretties=1
    ;  nobkg=1
    ;ENDIF
ENDIF
IF strpos(destdir,'/pb/') GE 0 THEN BEGIN
    ispb=1
    pipeline='pb'
ENDIF
IF strpos(destdir,'/rt/') GE 0 THEN BEGIN
    isrt=1
    pipeline='rt'
ENDIF
if keyword_set(TESTSPW) then pipeline='spw'
if keyword_set(TESTSPW) then noevent_notify=1
IF pipeline EQ 'spw' THEN BEGIN
; set default keywords for beacon pipeline at SSC
    noexthdr=1
    nosort=1
    nopretties=1
    nobkg=1
ENDIF

fologname= concat_dir(getenv('SCC_LOG'),'scc'+strupcase(strmid(destdir,0,1,/reverse))+pipeline+'scifileerror.log')

;stop
ndone=0L
ng=0L
nowaitset=0
IF n_params() GT 0 THEN BEGIN
    help,infilelist
    nz=where(infilelist ne '',nnz)
    ;nowaitset=1
    if nnz lt 1 THEN BEGIN
    	message,'Empty input; returning...',/info
	wait,5
	return
    endif ELSE BEGIN
        list=infilelist[nz]
    	ng=nnz
    ENDELSE

;    IF ~keyword_set(NOSORT) THEN sortscilist,list
    IF ~keyword_set(NOSORT) THEN BEGIN
	print,'Sorting by filename only'
	list=list[sort(list)]
    ENDIF
    
ENDIF
IF keyword_set(NOWAIT) THEN nowaitset=1
errwait=2

;--always work in $seb_img
cd,seb_img
spawn,'pwd',/sh
wait,2

repeat begin 
    IF n_params() GT 0 AND ndone LT ng THEN BEGIN
; ++++ file(s) to process is an argument ++++
	infile=list[ndone]
    	break_file, infile, dlog, dir, sciroot, ext
	scifile=sciroot+ext
	fileorig=scifile
        help,ndone,ng,scifile
    ENDIF ELSE BEGIN
; ++++ Look for new files to process. ++++
    	again:
	IF (ndone EQ ng) THEN BEGIN     ; either we just started, or we finished a batch
	    IF ng GT 0 and ((islz) or (ispb)) THEN BEGIN       ; ok, we just finished a batch
		; get days in list
		pos0=rstrpos(list[0],'/')+1
		allfnymdd=strmid(list,pos0,4)
		uymdd=uniq(allfnymdd)
		ndays=n_elements(uymdd)
		npd=lonarr(ndays)     	; number of files per day
		FOR k=0,ndays-1 DO BEGIN
		    if k EQ 0 THEN x=0 ELSE x=uymdd[k-1]
		    npd[k]=uymdd[k]-x
		ENDFOR
		ymdds=allfnymdd[uymdd]
		; get telescopes in list
		alltelid =strmid(list,pos0+11,1)
		tels=fix(alltelid[uniq(alltelid,sort(alltelid))])
		ntl=n_elements(tels)
		; for each day and tel in list, do daily processing

		FOR i=0,ndays-1 DO BEGIN
    	    	    tm=ymdd2utc(ymdds[i])
		    doy=utc2doy(tm)
		    yyyymmdd=utc2yymmdd(tm,/yyyy)
		    IF (doy+3) mod 7 EQ 0 THEN day4=1 ELSE day4=0
		    IF doy mod 7 EQ 0  THEN day7=1 ELSE day7=0
        	    ; get days for scc_mk_all_monthly.pro
        	    tm1=tm
        	    ; get today minus 5 days
		    tm1.mjd=tm1.mjd-1
		    tm1d=tm1
        	    tm1.mjd=tm1.mjd-4
        	    tm5d=tm1
        	    ; get today minus 30 days
        	    tm1.mjd=tm1.mjd-25 ; only subtract 25 since we already took off 5
        	    tm30d=tm1 
        	    tm1=tm
        	    ; get today minus 4 days
		    tm1.mjd=tm1.mjd-4
		    tm4d=tm1
        	    ; get today minus 29 days
        	    tm1.mjd=tm1.mjd-25 ; only subtract 25 since we already took off 5
        	    tm29d=tm1 
        	    ; convert those dates to YYYYMMDD
        	    dend=utc2yymmdd(tm5d,/yyyy)
        	    dstart=utc2yymmdd(tm30d,/yyyy)
        	    dendsl=utc2yymmdd(tm4d,/yyyy)
        	    dstartsl=utc2yymmdd(tm29d,/yyyy)
		    FOR j=0,ntl-1 DO BEGIN
			    telj=scitelvals[tels[j]]
			    teln=strlowcase(telj)
			    iscor2=(telj EQ 'COR2')

    	    	    	    IF teln EQ 'hi1' THEN teln='hi_1'
    	    	    	    IF teln EQ 'hi2' THEN teln='hi_2'
			    IF teln EQ 'euvi' THEN teln='e*'
			    
	    ;--Recreate link if it exists
	    	    	IF (islz) and ~keyword_set(TESTLZ) THEN BEGIN
    	    	    	    touchdatelink, destdir, 'cal',telj,yyyymmdd
    	    	    	    touchdatelink, destdir, 'img',telj,yyyymmdd
    	    	    	    touchdatelink, destdir, 'seq',telj,yyyymmdd
			    message,'Finished touching links...',/info
			    wait,3
    	    	    	ENDIF
			
		    	IF npd[i] GT 50 or keyword_set(sidelobe) THEN BEGIN
                           IF j eq 0 then sort_summary,yyyymmdd,telj,/logdiff

                            ;IF telj EQ 'HI2' or telj EQ 'HI1' THEN sort_hisam,yyyymmdd,aorb

			    IF telj EQ 'COR2' THEN begin 
			    ; dopolar required for mk_daily_med TBR
    	    	    	    	IF ~keyword_set(NOPRETTIES) and ~keyword_set(sidelobe) THEN $
				    sccdopolar,utc2str(tm),/COR2, AHEAD=aheadf, BEHIND=behindf, DOALL=delete_old, source=pipeline
			    	IF ~keyword_set(NOBKG) THEN BEGIN
    	    	    	    	    scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='0',DARKPIX=islz, source=pipeline
    	    	    	    	    scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='120',DARKPIX=islz, source=pipeline
    	    	    	    	    scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='240',DARKPIX=islz, source=pipeline
				    IF ~keyword_set(NOPRETTIES) THEN BEGIN
    	    	    	    		scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='TBR',DARKPIX=islz, source=pipeline
    	    	    	    		scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='DBL',DARKPIX=islz, source=pipeline
    	    	    	    		IF (day4) or (day7) and islz THEN $
    	    	    	    	    	    scc_mk_all_monthly,'cor2',aorb,dstart,dend,'all'
				    ENDIF
    	    	    	    	ENDIF
                                if islz and ~keyword_set(NOPRETTIES) then begin
                                   if aorblc eq 'a' then threstable=getenv('SSW')+'/stereo/secchi/data/tables/thresha.img1.36' else $
				    	    	threstable=getenv('SSW')+'/stereo/secchi/data/tables/threshb.img1.35'
				   if (notreproc) and islz then daily_test_thresh,aorb,yyyymmdd,yyyymmdd,threstable
			       endif
    	    	    	    	; create daily apid reports run only with the EUVI processing
    	    	    	    	IF islz THEN daily_ssr1_report,aorb,yyyymmdd,seb_img+'/pcktcnt/',/pipeline
                            ENDIF
                            IF telj EQ 'HI1' or telj EQ 'HI2' and ~keyword_set(NOBKG) THEN BEGIN
				scc_mk_daily_med,telj,aorb,yyyymmdd,polar='TBR',/noshift,DARKPIX=islz, source=pipeline
			        IF keyword_set(sidelobe) THEN scc_mk_daily_med,telj,aorb,utc2yymmdd(tm1d,/yyyy),polar='TBR',/noshift,DARKPIX=islz, source=pipeline
				; if not /noshift, then $SECCHI_DATA/hi must be uptodate
				;IF day7 THEN updatehibkg
				; this updates daily medians as HI pointing files are available and also re-does daily pretties
				    ; monthlys are on day 7
				IF (day4) or (day7) and islz THEN begin
                                   IF keyword_set(sidelobe) THEN scc_mk_all_monthly,telj,aorb,dstartsl,dendsl,'tbr'
				   scc_mk_all_monthly,telj,aorb,dstart,dend,'tbr'
                                ENDIF
				; make monthly min over period ending 30 days ago
			    ENDIF
			    IF ~keyword_set(NOPRETTIES) THEN BEGIN
			    	scc_daily_pretties,yyyymmdd,telj,aorb, DOALL=delete_old,/planets, source=pipeline, MASK_OCC=iscor2
				IF (iscor2) THEN scc_daily_pretties,yyyymmdd,'COR2',aorb, DOALL=delete_old,/planets, source=pipeline, /MASK_OCC, /USE_P0
				IF keyword_set(sidelobe) THEN scc_daily_pretties,utc2yymmdd(tm1d,/yyyy),telj,aorb, DOALL=delete_old,/planets, source=pipeline
				IF (notreproc) and islz and (telj EQ 'COR2') THEN scc_daily_pretties,yyyymmdd,telj,aorb,/minus30,/planets, /DOALL
				IF (notreproc) and islz and (telj EQ 'HI1')  THEN scc_daily_pretties,utc2yymmdd(tm1d,/yyyy),'HI1',aorb,/planets, /DOALL
				IF keyword_set(sidelobe) and ((telj EQ 'HI2') or (telj EQ 'HI1'))  THEN  $
				    	scc_daily_pretties,utc2yymmdd(tm1d,/yyyy),telj,aorb,/histars, DOALL=delete_old,/planets, source=pipeline
				; Since this uses daily median, need next day to do correct interpolation
				IF telj EQ 'HI1' or telj EQ 'HI2' THEN   scc_daily_pretties,yyyymmdd,telj,aorb,/histars, DOALL=delete_old,/planets, source=pipeline
                   ;create mpeg movies for previous 7 days (POC display movies)
    	    	    	    	if (notreproc) and islz and aorblc eq 'b' and telj EQ 'HI2'then begin  ;
                                   ud=anytim2utc(strmid(yyyymmdd,0,4)+'-'+strmid(yyyymmdd,4,2)+'-'+strmid(yyyymmdd,6,2))
                                   ud.mjd=ud.mjd
                                   yyyymmddm0=utc2yymmdd(ud,/yyyy)
                                   ud.mjd=ud.mjd-6
                                   yyyymmddm6=utc2yymmdd(ud,/yyyy)
                                   mvicam=[['hi2sa','hi1a','cor2a','cor1a'],['cor1b','cor2b','hi1b','hi2sb']]
                                   scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,pan=.9375,imgsize=512,mpegname='/net/earth/secchi/movies/mpegs/dailymv.mpg'
                                   mvicam=['hi2sa','hi2sb']
                                   scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512,mpegname='/net/earth/secchi/movies/mpegs/weekly_hi2.mpg'
                                   mvicam=['hi1a','hi1b']
                                   scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512,mpegname='/net/earth/secchi/movies/mpegs/weekly_hi1.mpg'
                                   mvicam=['cor2a','cor2b']
                                   scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512,mpegname='/net/earth/secchi/movies/mpegs/weekly_cor2.mpg'
                                   ud.mjd=ud.mjd+5
                                   yyyymmddm0=utc2yymmdd(ud,/yyyy)
                                   ud.mjd=ud.mjd-6
                                   yyyymmddm6=utc2yymmdd(ud,/yyyy)
                                   mvicam=['cor1a','cor1b']
                                   scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512,mpegname='/net/earth/secchi/movies/mpegs/weekly_cor1.mpg'
                               endif
			    	; Implement list of files for mpegs, Ahead only
				IF (aorblc eq 'a' and telj EQ 'HI1' and islz and ~keyword_set(sidelobe)) THEN mpegframes,yyyymmdd,['euvi','cor2','hi1','hi2','cor1'],_extra=_extra  			    
				 ;BEGIN;	  moved to line 780			    
;				    daily_select_njpegs,aorb,yyyymmdd,telj
;				    foutdir=concat_dir(getenv('SECCHI_JPG'),'mpegframes')
;				    cmd='mv '+concat_dir(getenv('SCC_LOG'),'mpegframes')+'/'+teln+'.lst '+foutdir
;				    print,cmd
;				    IF (day7) THEN spawn,cmd,/sh
;				    IF ~ishi THEN IF (day4) THEN spawn,cmd,/sh
				    
;	openw,elun,'ipmsg.txt',/get_lun,/append
;	;printf,elun,'dbfile/session= '+aorb+' '+pipeline+' '+dbroot
;	msg='njpegs: '+yyyymmdd+' '+telj+' '+trim(doy)+' day4='+trim(day4)+' day7='+trim(day7)+' i='+trim(i)
;	msg2=' npd='+arr2str(trim(npd))
;	printf,elun,msg+' '+msg2
;	printf,elun,''
;	close,elun
;	free_lun,elun
;	;printf,lulog,msg
;	print,msg,' ',msg2
;	wait,5	
;				ENDIF

			    ENDIF   	    	    ; END if ~keyword_set(NOPRETTIES)
		    	ENDIF ELSE BEGIN    	    ; END if npd[i] GT 50
			    openw,elun,'ipmsg.txt',/get_lun,/append
			    ;printf,elun,'dbfile/session= '+aorb+' '+pipeline+' '+dbroot
			    msg='skipped pretties: '+yyyymmdd+' i='+trim(i)+' npd='+arr2str(trim(npd))
			    msg2='list='+arr2str(strmid(list,pos0,12))
			    printf,elun,msg
			    printf,elun,msg2
			    printf,elun,''
			    close,elun
			    free_lun,elun
			    ;printf,lulog,msg
			    print,msg
			    wait,5
		    	ENDELSE	 
		    ENDFOR  	    	    	    ; telescopes 
                    if islz and (notreproc) and ~keyword_set(nomvattic) and ~keyword_set(TESTLZ) then scc_move_attic,yyyymmdd,aorblc ;move raw image files to sci_attic
		ENDFOR	    	    	    	    ; END FOR i=0,ndays-1

    		w=where(scitelvals[tels] EQ 'EUVI',iseuvi)
		IF (iseuvi) and (date_fname_utc.time GT 84600000) THEN begin 
		; assuming EUVI is always last to finish, this is when to compute archive stats, at the end of a day (after 23:30)
		   ;sccimagestats,date_fname_utc,ahead=aheadf,behind=behindf
                   call_comp_archive,aorb,yyyymmdd 
                   if aorblc eq 'b' and ~keyword_set(NOPRETTIES) then begin  ;
		   ;if 0 then begin
                      ud=anytim2utc(strmid(yyyymmdd,0,4)+'-'+strmid(yyyymmdd,4,2)+'-'+strmid(yyyymmdd,6,2))
                      ud.mjd=ud.mjd
                      yyyymmddm0=utc2yymmdd(ud,/yyyy)
                      ud.mjd=ud.mjd-6
                      yyyymmddm6=utc2yymmdd(ud,/yyyy)
                      mvicam=[['euvi_171a','euvi_195a','euvi_284a','euvi_304a'],['euvi_171b','euvi_195b','euvi_284b','euvi_304b']]
                      scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,pan=.9375,imgsize=512, mpegname='/net/earth/secchi/movies/mpegs/dailyeuvi.mpg'
                      mvicam=['euvi_171a','euvi_171b']
                      scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512, mpegname='/net/earth/secchi/movies/mpegs/weekly_euvi_171.mpg'
                      mvicam=['euvi_195a','euvi_195b']
                      scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512, mpegname='/net/earth/secchi/movies/mpegs/weekly_euvi_195.mpg'
                      mvicam=['euvi_284a','euvi_284b']
                      scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512, mpegname='/net/earth/secchi/movies/mpegs/weekly_euvi_284.mpg'
                      mvicam=['euvi_304a','euvi_304b']
                      scc_pngplay,yyyymmddm6,yyyymmddm0,mvicam,cadence=40,imgsize=512, mpegname='/net/earth/secchi/movies/mpegs/weekly_euvi_304.mpg'
                  endif
                endif
		IF n_params() GT 0 then return
	    ENDIF ; ng GT 0 and islz	; END just finished a batch
    	; do .0 files first, they are oldest

	    ;--always work in $seb_img
	    cd,seb_img
	    spawn,'pwd',/sh
	    wait,2

            list1 = file_search('????????.???', count=ng1)
            list2 = file_search('????????.???.?', count=ng2)
            IF ng1 EQ 0 THEN list=list2 ELSE $
            IF ng2 EQ 0 THEN list=list1 ELSE list=[list1,list2]
            ng=ng1+ng2
            IF ng gt 1 and islz then begin
	      print,'Waiting 5 min....'
	      wait,300
              list1 = file_search('????????.???', count=ng1)
              list2 = file_search('????????.???.?', count=ng2)
              IF ng1 EQ 0 THEN list=list2 ELSE $
              IF ng2 EQ 0 THEN list=list1 ELSE list=[list1,list2]
              ng=ng1+ng2
	    endif
	    IF ~keyword_set(NOSORT) THEN sortscilist,list

	    ;g = where(strlen(list) EQ 12, ng)
	    help,ng
            ndone=0L

            ; ++++ 
    	    IF ng LT 1 THEN BEGIN
                ;IF nowaitset THEN return $
                ;ELSE BEGIN
	            ;print,'Waiting ',trim(string(waittime/60.)),' minutes before trying again ...'
    	            wait,waittime
                    ndone=0L
	            goto, again
                ;ENDELSE     	    
    	    ENDIF
        ENDIF	    	    	    	; END just started or finished a batch
    	scifile=list[ndone]
        help,ndone,ng,scifile
	infile=scifile
        fileorig=scifile
    ENDELSE  ; look for new files to process or last file in list processed
; ++++ End new files to process. ++++
    dir=''
    spawn,'pwd',/sh
    ndone=ndone+1
    ;IF ng-ndone EQ 0 and nowaitset THEN keeplooking=0
    parts = str_sep(scifile,'.')
    sciroot=parts[0]
    basename=sciroot
    scisfx =parts[1]
    dashpos=rstrpos(scifile,'.')
    strput,scifile,'-',dashpos
; ++++ Make sure file is done being written. ++++
    waitcnt=0
    IF ng LT 3 THEN REPEAT BEGIN
    	lastsize=file_size(infile)
	print,'Waiting to make sure file is complete ...'
	wait, 2
	metasize=file_size(infile+'.meta')
	IF metasize EQ 0 THEN REPEAT BEGIN
		print,'Waiting 20 sec for ',infile+'.meta'
		wait,20
		waitcnt=waitcnt+1
		metasize=file_size(infile+'.meta')
	ENDREP UNTIL (metasize GT 100) OR (waitcnt GT 60)
	; wait 20 minutes for realtime image to come down
	; new HI2 spw are about 55 KB, at 0.6 kbps is 12.2 minutes
	IF waitcnt GT 60 THEN BEGIN
    	    openw,elun,'decomp.txt',/get_lun,/append
            msg=infile+'.meta did not complete'
	    printf,elun,msg
	    printf,elun,''
	    close,elun
	    free_lun,elun
            ;printf,lulog,msg
            print,msg
            wait,2
	ENDIF
	wait, 2
	
    ENDREP UNTIL (file_size(infile) EQ lastsize)
; ++++ Process file header. ++++
    scidat = scifile+'.dat'	    ; always written in current directory
    scihdr = scifile+'.hdr'
    scitlr = scifile+'.tlr'
    sciimg = scifile+'.img'	    ; always written in current directory
    spawn, unblock_cmd+' '+infile,/sh, unblockresult
    print,unblockresult
    IF keyword_set(GET_VERSION) THEN $ 
    	spawn,'$UTIL_DIR/rdSh '+scihdr+ ' | grep ctrl.Version',versionline,/sh ELSE $
    	versionline='header version assumed: 04' 
    parts=str_sep(versionline,':')
    ; If there is a read error, core is dumped and output is empty. Assume hdr version 4.
    IF n_elements(parts) LT 2 THEN parts=[0,4]
    IF fix(parts[1]) EQ 0 THEN BEGIN
        spawn,'$UTIL_DIR/rdShv1 '+scihdr+ ' | grep ctrl.Version',versionline,/sh
        parts=str_sep(versionline,':')
    ENDIF
    IF fix(parts[1]) LT 3 THEN BEGIN
        versionline=versionline+': loading sebhdr_struct.inc'
        nomoffset=80
        extoffset=192
@./sebhdr_struct.inc
    ENDIF ELSE IF fix(parts[1]) EQ 3 THEN BEGIN
;if datatype(ext_hdr) EQ 'UND' THEN $
        versionline=versionline+': loading sebhdr3_struct.inc'
        nomoffset=96
        extoffset=232
@./sebhdr3_struct.inc
    ENDIF ELSE BEGIN
;if datatype(ext_hdr) EQ 'UND' THEN $
        versionline=versionline+': loading sebhdr4_struct.inc'
        nomoffset=88
        extoffset=216
@./sebhdr4_struct.inc
    ENDELSE
    print,versionline
    IF NOT(file_exist(scihdr)) THEN spawn,'touch '+scihdr,/sh
    IF NOT(file_exist(scitlr)) THEN spawn,'touch '+scitlr,/sh
    print,'Reading ',scihdr
    openr,1,scihdr
    scitlrmissing='no'
    openr,2,scitlr
    hdrstat=fstat(1)
    tlrstat=fstat(2)
    readhdrinfo="hdrsize="+string(hdrstat.size)+" tlrsize="+string(tlrstat.size)
    openw,folog, fologname, /append, /get_lun
    IF hdrstat.size + tlrstat.size EQ 0 THEN BEGIN
    	print,'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
	print,'No header information detected; skipping...'
	wait,9
	printf,folog,scifile+' '+readhdrinfo
	openw,lulog,logfile,/append
	goto, skipscifile
    ENDIF
    IF (tlrstat.size GT 500) OR (tlrstat.size LT 10) THEN BEGIN
    	close,2
        command='cp '+scihdr+' '+scitlr
        print,command
        scitlrmissing='Trailer wrong size: '+command
        spawn, command,/sh
    	openr,2,scitlr
    	tlrstat=fstat(2)
	printf,folog,scifile+' '+readhdrinfo
   ENDIF
    IF tlrstat.size GT hdrstat.size THEN BEGIN
        mainlun=2 
        readhdrinfo=readhdrinfo+": Using "+scitlr+" for main header."
	printf,folog,scifile+' '+readhdrinfo
    ENDIF ELSE BEGIN
        mainlun=1
    ENDELSE
    print,readhdrinfo
    a = assoc(mainlun,m_baseheader)
    basehdr = a[0]
    ; so it works on linux PCs:
    swap_endian_inplace,basehdr,/SWAP_IF_LITTLE_ENDIAN
    IF (tlrstat.size LT hdrstat.size) THEN BEGIN
        command='cp '+scihdr+' '+scitlr
        print,command
        spawn, command,/sh
	printf,folog,scifile+' '+readhdrinfo
    ENDIF
    ;b = assoc(2,m_baseheader)
    ;basetlr = b[0]
    IF ((hdrstat.size>tlrstat.size) GT 100) THEN BEGIN
       a = assoc(mainlun,m_nominalheader,nomoffset)
       nom_hdr = a[0]
    	; so it works on linux PCs:
    	swap_endian_inplace,nom_hdr,/SWAP_IF_LITTLE_ENDIAN
       print,'Reading ',scitlr
       b = assoc(2,m_nominalheader,nomoffset)
       nom_tlr = b[0]
    	; so it works on linux PCs:
     	swap_endian_inplace,nom_tlr,/SWAP_IF_LITTLE_ENDIAN
    ENDIF
    IF ((hdrstat.size>tlrstat.size) GT 300) THEN BEGIN
        a = assoc(mainlun,m_extendedheader,extoffset)
        ext_hdr = a[0]
    	; so it works on linux PCs:
    	swap_endian_inplace,ext_hdr,/SWAP_IF_LITTLE_ENDIAN
        ;b = assoc(2,m_extendedheader,192)
        ;ext_tlr = b[0]
    ENDIF
    close,1
    close,2
    free_lun,folog

    date_fname_utc  =tai2utc(nom_hdr.cmdexptime, /nocorrect)
    badhdr=0
    yyyymmdd_hhmmss=utc2yymmdd(date_fname_utc,/yyyy,/hhmmss)
    yyyymmdd=strmid(yyyymmdd_hhmmss,0,8)
    yymmdd=strmid(yyyymmdd,2,6)
    help,yyyymmdd_hhmmss
    case (1) of
    	date_fname_utc.mjd GT 64693: badhdr=1	; 2036/01/01
	date_fname_utc.mjd LT 51544: badhdr=1	; 2000/01/01
	nom_hdr.platformid GT 2: badhdr=1
	nom_hdr.telescopeid GT 5: badhdr=1
    	else:
    endcase
    IF (badhdr) THEN BEGIN
    	print,'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
	print,'Corrupted header detected; skipping...'
	wait,9
	printf,folog,scifile+' '+yyyymmdd_hhmmss
	openw,lulog,logfile,/append
	goto, skipscifile
    ENDIF
    	
    ymdd=strmid(scifile,0,4)
    ePlatform=	[ $
	  'STEREO' $
	, 'STEREO_A' $
	, 'STEREO_B']
    aorb=strmid(ePlatform[nom_hdr.platformid],7,1)
    aorblc=strlowcase(aorb)
    IF aorb EQ 'A' THEN BEGIN
    ; these are used in call to sccdopolar
    	aheadf=1
	behindf=0
	scname='ahead'
    ENDIF ELSE BEGIN
    	aheadf=0
	behindf=1
	scname='behind'
    ENDELSE
    
    ; need to set actualexptime if Skipped image
    skippedimg=where(basehdr.ipcmdlog ge 254,skpim)
    IF (skpim gt 0) OR basehdr.actualexptime EQ 0 THEN BEGIN	; skipped image
	basehdr.actualexptime=nom_hdr.ipprocessingtime
	; So it goes in database
	iserror=1
    ENDIF
    
    date_obs_utc=tai2utc(basehdr.actualexptime, /nocorrect)
    dobsyymmdd=utc2yymmdd(date_obs_utc,/yyyy)
    yyyy=strmid(dobsyymmdd,0,4)
    mm  =strmid(dobsyymmdd,4,2)
    
; ++++ start log file ++++
    IF (dobsyymmdd NE lastdobsyymmdd) THEN BEGIN
 
      ; limit IP ERROR, SKIPPED and ABORT emails to 1 
      iserrormail=intarr(4) +0

      ;remove hilow dir for day-2
      rmhilow=filepath( utc2yymmdd(tai2utc(basehdr.actualexptime-(48l*60*60), /nocorrect),/yyyy),ROOT=seb_img+'/hilow' )
      spawn,'/bin/rm -r '+rmhilow
      
      if (aheadf and nom_hdr.TELESCOPEID ge 4 and ~keyword_set(NOPRETTIES)and (islz)) then scc_get_cor1_daily,yyyymmdd,nompeg=sidelobe, _EXTRA=_extra  

    	WHILE ~(is_dir(sci_attic+yymmdd)) DO BEGIN
	    dtmp=file_info(sci_attic+yymmdd)
	    if dtmp.DIRECTORY ne 1 then begin
  	      spawn,'/bin/rm -f '+sci_attic+yymmdd,/sh
	      wait,2
 	    endif
	    spawn,'mkdir -p '+sci_attic+yymmdd,/sh
	    wait,2
	ENDWHILE
	IF ~(is_dir(sci_attic+yymmdd)) THEN stop
	;--Check to see if need to make a link for sci dir
;	scidir=getenv_slash('sci'+aorblc)
;	lncmd='ln -s '+sci_attic+yymmdd+' '+scidir+yymmdd
;	IF sci_attic NE scidir and ~file_exist(scidir+yymmdd) and (islz) THEN BEGIN
;	    print,lncmd
;	    wait,5
;	    spawn,lncmd,/sh
;	ENDIF
        ;close,lulog
	;free_lun,lulog
	;++++ Copy previous day db scripts to lambda ++++
	IF (islz OR isrt) THEN $
    	IF (	~keyword_set(NOHBTEMPS) and $
		~keyword_set(TESTLZ) and $
	    	strpos(seb_img,'testimg') LT 0 and $
	    	strpos(seb_img,'edm') LT 0)  THEN BEGIN
	    print,'Running '+cpdb_cmd
	    spawn,cpdb_cmd,/sh
	ENDIF ELSE BEGIN
		print,'Not running ',cpdb_cmd
		wait,4
	ENDELSE
	
	; go ahead and make db files for SPWX or PB, even though not (yet) using them
	dbroot=yyyymmdd+strmid(yyyymmdd_hhmmss,9,4)
	dbfile=concat_dir(getenv('SCC_DB'),'scc'+aorb+pipeline+src+dbroot+'sql.script')
	openw,ludb,dbfile,/get_lun
	dbfstat=fstat(ludb)
	IF dbfstat.size EQ 0 THEN BEGIN
	; only at beginning of each script file
	    printf,ludb,'use secchi_flight;'
	    printf,ludb,'start transaction;'
	ENDIF
	get_utc,ustarttime
	starttime=utc2str(ustarttime,/ecs)
	printf,ludb,'# starting dbfile at '+starttime
	close,ludb
	free_lun,ludb
	
    	logfile= concat_dir(getenv('SCC_LOG'),'scc'+aorb+pipeline+dobsyymmdd+'.log')
	openw,lulog,logfile,/append
	printf,lulog,version
	printf,lulog,' '
	;text=readlist('sebhdr_struct.inc')
	text=[';*  DB Table IDL structure definition created by CPP2IDLSTRUCT.PRO', $
	      ';*  from $stereo/fsw/sw_dev/utils/rdSh/hdr.h on Thu May 19 15:11:05 2005']
	printf,lulog,'NOTE: sebhdr info is static -- need to generate from sebhdr4_struct.inc'
	printf,lulog,text[0]
	printf,lulog,text[1]
	printf,lulog,'starting log file at '+starttime
	printf,lulog,' '
	printf,lulog,'SEB_IMG	',seb_img
	printf,lulog,'SCI_SOURCE ',source
	printf,lulog,'SECCHI	',destdir
	printf,lulog,'SCC_ATTIC ',sci_attic
	printf,lulog,unblock_cmd
	printf,lulog,uncompr_cmd,' or '
	printf,lulog,huncompr_cmd,' or '
	printf,lulog,iuncompr_cmd

	printf,lulog,'IDL_PATH	',getenv('IDL_PATH')
	printf,lulog,'TABLES	',getenv('TABLES')
	printf,lulog,'PT    	',getenv('PT')
	printf,lulog,'DATE: ',scch.date
	printf,lulog,'==============================='
        ; Move any existing .sh files for previous day to attic 
	spawn, 'ls -l '+lastymdd+'*.sh', lsres,/sh,COUNT=nls
	IF nls GT 0 THEN BEGIN
	    ;openw,55,concat_dir(getenv('SCC_LOG'),'extra_sh_files.log'),/append
	    ;for ii=0,nls-1 do printf,55,lsres[ii]
	    ;close,55
	    ;command='mv *.sh '+sci_attic+strmid(lastyyyymmdd,2,6)
	    ;print,command
	    for ii=0,nls-1 do print,lsres[ii]
	    print
	    print,'Waiting 5 ...'
	    wait, 5
	    ;spawn,command,/sh
	    ;printf,lulog,command
	    for ii=0,nls-1 do printf,lulog,lsres[ii]		
	ENDIF
	; If playback, Copy any .sh files from realtime attic to current directory
	;IF (ispb) or (islz) THEN BEGIN
    	IF (ispb) or keyword_set(FIND_HDR_SCRIPT) THEN BEGIN
	; nr - matches what is below
            rtattic=getenv_slash('SCC_RTATTIC')
            command='cp -p '+rtattic+yymmdd+'/*.sh .'
            print,'%% secchi_reduce: ',command
            wait,5
            spawn,command,/sh
            wait,5
	ENDIF
	; End of day activities
	IF file_exist('ipmsg.txt') and ~isrt THEN BEGIN
	    spawn,'cat ipmsg.txt',/sh
	    ;maillist='nathan.rich@nrl.navy.mil lynn.mcnutt@nrl.navy.mil'
	    ;IF pipeline EQ 'lz' THEN maillist=maillist+' dennis.wang@nrl.navy.mil'
	    IF keyword_set(TESTLZ) THEN maillist='nathan.rich@nrl.navy.mil'
	    spawn,'mailx -s Non-Decomp_IP_Errors '+maillist+' < ipmsg.txt',/sh
	    wait,30
    	    spawn,'rm ipmsg.txt',/sh
	ENDIF
	IF file_exist('decomp.txt') and ~isrt THEN BEGIN
	    spawn,'cat decomp.txt',/sh
	    ;maillist='nathan.rich@nrl.navy.mil lynn.mcnutt@nrl.navy.mil'
	    ;IF pipeline EQ 'lz' THEN maillist=maillist+' dennis.wang@nrl.navy.mil'
	    IF keyword_set(TESTLZ) THEN maillist='nathan.rich@nrl.navy.mil'
	    spawn,'mailx -s Image_Processing_Errors '+maillist+' < decomp.txt',/sh
	    ; subject should really be 'Decomp_Errors', but prefer not to change mail filters.
 	    wait,30
   	    spawn,'rm decomp.txt',/sh
	ENDIF
        lastymdd=strmid(sciroot,0,4)
        ; get one days worth of HB temp data for lz only and test_img for testing mode only
       IF (keyword_set(NOHBTEMPS) or NOT(islz) ) THEN BEGIN
            message,"WARNING: HB temps not retrieved from database!!!",/info
	    print,''
	    print,''
	    IF not(keyword_set(FAST)) then wait,2
       ENDIF ELSE begin
    	    euvitemps='HBEUVIAFTSHTRT,HBEUVIZONET,HBEUVIAFTMNTT,HBEUVISECMIRT,HBEUVIENTRT,HBEUVIFWDMNTT'
    	    cor1temps='HBCOR1ZONE2T,HBCOR1DOUB2T,HBCOR1POLDOUB1T,HBCOR1TUBEOCCT,HBCOR1ZONE1T'
    	    cor2temps='HBCOR2ZONE3T,HBCOR2RLYLNST,HBCOR2FLDLNST,HBCOR2HRMRRT,HBCOR2ZONE2T,HBCOR2ZONE1T'
    	    hitemps='HBHIBACKSTRT,HBHIFINT,HBHIZONE1T,HBHIZONE2T,HBHIFRNTSTRT,HBHIBASESTRT'
    	    ccdtemps='HBEUVICCDT,HBCOR1CCDT,HBCOR2CCDT,HBHI1CCDT,HBHI2CCDT'
    	    cebenctemps='HBSCIPCEBENCLT,HBHICEBENCLT'
    	    teltemp=euvitemps+','+cor1temps+','+cor2temps+','+hitemps+','+ccdtemps+','+cebenctemps
    	    stemps=get_hbtemps(aorb,teltemp,dobsyymmdd,res=0.1)
            if(datatype(stemps) ne 'STR')then begin
       	      tags=tag_names(stemps) & ntags=n_Elements(tags)
              timez=strmid(stemps.pckt_time,0,4)+strmid(stemps.pckt_time,5,2)+strmid(stemps.pckt_time,8,2)+'_'$
	    	+strmid(stemps.pckt_time,11,2)+strmid(stemps.pckt_time,14,2)+strmid(stemps.pckt_time,17,2)
              hbtimes=yymmdd2utc(timez) 
    	      hbtemps=fltarr(ntags-2,n_elements(stemps.(0)))
    	      for n=0,ntags-3 do hbtemps(n,*)=stemps.(n+2)
            endif
    	    ictemps=get_ictemps(aorb,dobsyymmdd,res=0.1)
            if(datatype(ictemps) ne 'STR')then begin
       	      tags=tag_names(ictemps) & ntags=n_Elements(tags)
              timez=strmid(ictemps.pckt_time,0,4)+strmid(ictemps.pckt_time,5,2)+strmid(ictemps.pckt_time,8,2)+'_'$
	    	+strmid(ictemps.pckt_time,11,2)+strmid(ictemps.pckt_time,14,2)+strmid(ictemps.pckt_time,17,2)
              cebtimes=yymmdd2utc(timez) 
    	      cebtemps=fltarr(ntags-2,n_elements(ictemps.(0)))
    	      for n=0,ntags-3 do cebtemps(n,*)=ictemps.(n+2)
            endif
   	ENDELSE
	resetvars=1		; re-query events database
	lastimgctr=basehdr.imgctr
	;
        if (dospice eq 1) then begin 
    	    IF ndone LE 1 OR islz THEN BEGIN
		    print,'Calling load_stereo_spice'
    		    load_stereo_spice, _extra=_extra, /verbose, /Reload
	    ENDIF
    	    date_obs	=utc2str(date_obs_utc,/noz)
    	    ;print,'Calling get_stereo_spice_kernel /ATTITUDE...'
    	    ;ATT_FILE	=get_stereo_spice_kernel(date_obs,aorb,/ATTITUDE)
    	    ;IF ATT_FILE EQ '' THEN ATT_FILE="NotFound"
	    ;** att_file gotten in make_scc_hdr.pro
    	    print,'Calling get_stereo_spice_kernel to get ephemfil ...'
    	    EPHEMFIL	=get_stereo_spice_kernel(date_obs,aorb)
    	    IF EPHEMFIL EQ '' THEN EPHEMFIL="NotFound"
    	    help,ephemfil,att_file
    	    spawn,['rm','attmsg.txt'],/noshell
       endif
    	;--For cor?_point and hi_point_v2 (gt slew data)
	;--older than 45 days, then should look in SDS_FIN_DIR
	;  currently appears MOC is keeping 30 days of .fin files online, so last 60 days total. nr,6/26/07
	sdspath=0
	IF islz THEN $
	IF date_obs_utc.mjd LT ustarttime.mjd-62 THEN $
	sdspath=getenv('SDS_FIN_DIR')+'/'+aorblc+'/'+yyyy+'/'+mm ELSE $
	sdspath=getenv('SDS_DIR')+'/'+scname+'/data_products/level_0_telemetry/secchi'
 
    endif ELSE BEGIN
	openw,lulog,logfile,/append
	;openw,ludb,dbfile,/append,/get_lun
	basehdrimgctr=basehdr.imgctr
	IF basehdrimgctr LT lastimgctr THEN BEGIN
	    help,basehdrimgctr,lastimgctr
	    wait,3
	    resetvars=1
	    ; This means schedule and tables were read in
	ENDIF
	lastimgctr=basehdr.imgctr
    ENDELSE	; daily activities
    printf,lulog,' '
    printf,lulog,infile
    FOR i=0,n_elements(unblockresult)-1 DO printf,lulog,unblockresult[i]
    printf,lulog,versionline
    printf,lulog,readhdrinfo
    printf,lulog,'Processing time: ',scch.date
    printf,lulog,' '
    lastyyyymmdd=yyyymmdd
    lastdobsyymmdd=dobsyymmdd
    
    ;++++ markers for two part images IP prog 120 and 121++++
    
    lowhilow=where(basehdr.ipcmdlog eq 121, ishilow) 	; use low word from HI summing buffer
    lowhihi=where(basehdr.ipcmdlog eq 120, ishihigh)  	; use high word from HI summing buffer
    	    	    	    	    	    	    	; - img num is low num + 2000
    ;NOTE that highword image will have BOTH lowhilow and lowhihi true
    ;BUG 78 to correct FSW error "What should be 51 is 101 and what should be 2098 is 2020"
    if ((lowhihi(0) gt -1) and (nom_hdr.p1col eq 101)) then nom_hdr.p1col=51 
    if ((lowhihi(0) gt -1) and (nom_hdr.p2col eq 2020)) then nom_hdr.p2col=2098

    ;++++ check for CRS sigma to be set and save in cadence.sav++++
    CRS2im=where(basehdr.ipcmdlog eq 31, crs2)
    IF (crs2) THEN BEGIN
    ; this only matters for 2-image CRS
	IF datatype(iphicrs) EQ 'UND' THEN BEGIN
	; first time through
		; check savefile for last known value
		cadencefile= concat_dir(getenv('SCC_LOG'),'cadence.sav')
		cadencetemp=findfile(cadencefile)
        	if (cadencetemp eq '') then begin
        	; no cadence file, so set sigma to default
        		iphicrs=124b	; 1 sigma crs filter
                	cadence=dblarr(3,32767) -1	; set var to complete savset
		endif ELSE restore, cadencefile
                ; cadence file exists and iphicrs has previous value or default value
    	ENDIF 
        ; else cadence and iphicrs are set with previous values and cadencefile must exist
        ; now we have a valid value for iphicrs in secchi_reduce level
        	
	CRSval=where(basehdr.ipcmdlog ge 124 and basehdr.ipcmdlog le 128, sigmapresent)
	;IF ~(sigmapresent) THEN use previous value for iphicrs, and no need to update saveset
        ; but do need to update ipcmdlog
	IF (sigmapresent) THEN BEGIN
		IF (CRSval[0] NE iphicrs) then begin
		;only update if changed
			iphicrs=basehdr.ipcmdlog[CRSval[0]]
			save,filename=cadencefile,cadence,iphicrs,basename
		ENDIF		
        ENDIF ELSE BEGIN
		ipar=basehdr.ipcmdlog
		; 1st is always a destination, and last value is always compression
		; so need to insert iphicrs in the middle
		basehdr.ipcmdlog=[ipar[0],iphicrs,ipar[1:18]]
	ENDELSE
    endif
       

    ;++++ check to see if image is part of sequence and set up ASCII table++++
    hdronly=where(basehdr.ipcmdlog eq 5)    	; Header Only
    clrHI1=where(basehdr.ipcmdlog eq 33,nch1)    	; Clear HI-1 Summing Buffer
    clrHI2=where(basehdr.ipcmdlog eq 35,nch2)    	; Clear HI-2 Summing Buffer
    HI1sumF=where(basehdr.ipcmdlog eq 114,nfsh1)    	; Filtered_Add_hi_sumBuf1
    HI2sumF=where(basehdr.ipcmdlog eq 115,nfsh2)    	; Filtered_Add_hi_sumBuf2
    HI1sum=where(basehdr.ipcmdlog eq 34,nsh1)    	; Add image to HI-1 Summing Buffer
    HI2sum=where(basehdr.ipcmdlog eq 36,nsh2)    	; Add image to HI-2 Summing Buffer
    HI1seqimg=where(basehdr.ipcmdlog eq 37, nhi1seqimg)    ; Use HI-1 Summing Buffer as Output
    HI2seqimg=where(basehdr.ipcmdlog eq 38, nhi2seqimg)    ; Use HI-2 Summing Buffer as Output
    SpWximg=where(basehdr.ipcmdlog eq 40, isbeacon)    	; use SpaceWeather APID
    HI1SPW=where(basehdr.ipcmdlog eq 16, isHISPW)    	; HI1SPW image
    HI2SPW=where(basehdr.ipcmdlog eq 17, isHISPW)    	; HI2SPW image
    if where(basehdr.ipcmdlog eq 15) gt -1 and where(basehdr.ipcmdlog eq 42) gt -1 and total(basehdr.ipcmdlog) eq 57 then isHISAM=1 else isHISAM=0
    seqhdronly=0
    dateobscmnt=''  ;define dateobscmnt for sequence images get used if date obs is estimated.
    IF (nhi1seqimg+nhi2seqimg+nsh1+nsh2) GT 0 THEN BEGIN
    ; nr - correct for bad sebxsum in pre-?? images
    ; MUST be before make_scc_hdr
    	basehdr.sebxsum = (basehdr.sebxsum)>2
	basehdr.sebysum = (basehdr.sebysum)>2
    ENDIF

    if isHISAM eq 1 then  nom_hdr.p2col=nom_hdr.p1col

    ;IF (isHISPW) THEN nom_hdr.p2col=nom_hdr.p2col-(nom_hdr.p2col-nom_hdr.p1col)/2  ;2009-08-14 new HISPW uses a mask

;++++ populate FITS header ++++
    fits_hdr = make_scc_hdr(source, FAST=fast, spice=dospice, IPver=IPver, ENDBADAH=endbadah,_extra=_extra)
    
    iperror=where(basehdr.ipcmdlog ge 253)
    IF ((iserror) or iperror[0] gt -1)THEN BEGIN
        if iperror[0] eq -1 then isiperr=3 else isiperr=basehdr.ipcmdlog(iperror[0])-253
        iserrmsgs=['ERROR','SKIPPED','ABORTED','HDR ONLY EXPTIME = 0']
        iserrmsg=iserrmsgs(isiperr)
	date_cmd_utc =tai2utc(nom_hdr.CMDEXPTIME, /nocorrect) 
	datecmd=utc2str(date_cmd_utc,/noz)  
	iptime=utc2str(tai2utc(double(nom_hdr.ipprocessingtime), /nocorrect))      
	msg=string(basehdr.filename)+' DATE-CMD='+datecmd+', IPTIME='+iptime+' was '+iserrmsg+'!!!'
	openw,elun,'ipmsg.txt',/get_lun,/append
	;printf,elun,'dbfile/session= '+aorb+' '+pipeline+' '+dbroot
	printf,elun,msg
	printf,elun,''
	close,elun
	free_lun,elun
	printf,lulog,msg
	print,msg
	wait,errwait
	;goto, skipscifile
	; Should be handled as header only! (ie, goes in database)

  	IF (~keyword_set(NOEVENT_NOTIFY) and iserrormail(isiperr) eq 0)THEN BEGIN
	    openw,evlun,'errmsg.txt',/get_lun
	    printf,evlun,'******* IP '+iserrmsg+' *******'
	    printf,evlun,''
	    printf,evlun,'pipeline=',string(pipeline,'(a8)')
	    printf,evlun,'obsrvtry=    ',scch.obsrvtry
	    printf,evlun,'filename=    ',scch.filename
	    printf,evlun,'date_obs=    ',scch.date_obs
	    printf,evlun,'fileorig=    ',scch.fileorig
	    printf,evlun,'Critical Event=    ',string(basehdr.CRITEVENT,'(z4)')
	    if iperror(0) gt -1 then for nip=0,n_Elements(iperror)-1 do printf,evlun,'ipcmdlog',string(iperror(nip),'(i2.2)'),'=',basehdr.ipcmdlog(iperror(nip))
            if iperror(0) eq -1 then printf,evlun,'Header only no exposure time recorded'
	    printf,evlun,''
	    close,evlun
	    free_lun,evlun
	    print,''
	    spawn,'cat errmsg.txt',/sh
	    wait,errwait
	    ;maillist='nathan.rich@nrl.navy.mil lynn.mcnutt@nrl.navy.mil dennis.wang@nrl.navy.mil'
            spawn,'mailx -s SECCHI-'+aorb+'_IP_'+iserrmsg+'_'+yymmdd+' '+maillist+' < errmsg.txt',/sh
            wait,30
	    iserrormail(isiperr)=1
	 ENDIF
    ENDIF



    IF strmid(scch.detector,0,2) EQ 'HI' THEN ishi=1 ELSE ishi=0
    
    IF (iserror) then begin
      IF  (~keyword_set(NOEXTHDR) and basehdr.NUMINSEQ gt 1) then begin
          IF(basehdr.IMGSEQ gt 0 and nom_hdr.TELESCOPEID eq 5) then HI1sum=(1)
          IF(basehdr.IMGSEQ gt 0 and nom_hdr.TELESCOPEID eq 4) then HI2sum=(1)
          IF(basehdr.IMGSEQ eq 0 and nom_hdr.TELESCOPEID eq 5) then nch1=(1)	;nbr, 6/5/07
          IF(basehdr.IMGSEQ eq 0 and nom_hdr.TELESCOPEID eq 4) then nch2=(1)
      ENDIF
    ENDIF

    IF (scch.detector EQ 'HI1' and basehdr.numinseq GT 1) THEN BEGIN
    	HIfile=concat_dir(getenv('SCC_LOG'),'HI1seqtbl.sav')
    	HIfile_m1=concat_dir(getenv('SCC_LOG'),'HI1seqtbl_m1.sav')
	;ishi=1
	; set earlier
    ENDIF
    IF (scch.detector EQ 'HI2' and basehdr.numinseq GT 1) THEN BEGIN
    	HIfile=concat_dir(getenv('SCC_LOG'),'HI2seqtbl.sav')
    	HIfile_m1=concat_dir(getenv('SCC_LOG'),'HI2seqtbl_m1.sav')
	;ishi=1
    ENDIF

    IF ~keyword_set(NOEXTHDR) and isHISAM eq 0 THEN $
    IF (ishi and basehdr.numinseq GT 1 and basehdr.imgseq EQ 0) then begin 
      print,'Initting HIseqtbl'
      wait,3
      if file_search(hifile) ne '' then begin
        restore,HIfile
        save,filename=HIfile_m1,HIseqtbl,HIsttime,HIcmdtime,HIscant,HIfstimg,HIBias
      endif
      HIseqtbl=strarr(basehdr.numinseq)
      HIsttime=basehdr.actualexptime
      HIcmdtime=nom_hdr.cmdexptime
      HIfstimg=basehdr.TELESCOPEIMGCNT
      HIBias=lonarr(basehdr.numinseq)
      save,filename=HIfile,HIseqtbl,HIsttime,HIcmdtime,HIscant,HIfstimg,HIBias
    endif
    IF (isbeacon + ishihigh) EQ 0  and isHISAM eq 0 THEN $
    if (nfsh1+nsh1+nfsh2+nsh2) GT 0 or (ishi and basehdr.numinseq GT 1) then begin
	IF ~keyword_set(NOEXTHDR) THEN BEGIN
      		restore,HIfile
                if(basehdr.TELESCOPEIMGCNT lt HIfstimg and basehdr.actualexptime gt HIsttime)then HIcurimg=basehdr.TELESCOPEIMGCNT+long(65535) else HIcurimg=basehdr.TELESCOPEIMGCNT
                if(nch1+nch2 EQ 0)then begin	;nbr, 6/5/07
		; not first image
                  zx=where(hiseqtbl ne '')
                  seqtime =-1 & clrhiseq=0
		  if(n_elements(zx) gt 1)then seqtime=(float(strmid(hiseqtbl(zx(1)),0,8))-float(strmid(hiseqtbl(zx(0)),0,8)))*n_elements(hiseqtbl)
                  if(seqtime gt -1 and basehdr.actualexptime gt HIsttime+seqtime)then clrhiseq=1
                  if(HIcurimg gt HIfstimg+n_elements(hiseqtbl))then clrhiseq=1
                  if(clrhiseq eq 1 and ~(iserror))then begin
                     save,filename=HIfile_m1,HIseqtbl,HIsttime,HIcmdtime,HIscant,HIfstimg,HIBias
                     HIseqtbl=strarr(basehdr.numinseq)
                     HIsttime=basehdr.actualexptime
                     HIcmdtime=nom_hdr.cmdexptime
                     HIfstimg=basehdr.TELESCOPEIMGCNT
      	    	     HIBias=lonarr(basehdr.numinseq)
  		  endif      		
                  if(basehdr.actualexptime lt HIsttime and ~(iserror))then begin 
                      HIfile=HIfile_m1
       		      restore,HIfile
                      if(basehdr.TELESCOPEIMGCNT lt HIfstimg and basehdr.actualexptime gt HIsttime)then HIcurimg=basehdr.TELESCOPEIMGCNT+long(65535) else HIcurimg=basehdr.TELESCOPEIMGCNT
                      zx=where(hiseqtbl ne '')
                      seqtime =-1 & clrhiseq=0
		      if(n_elements(zx) gt 1)then seqtime=(float(strmid(hiseqtbl(zx(1)),0,8))-float(strmid(hiseqtbl(zx(0)),0,8)))*n_elements(hiseqtbl)
                      if(seqtime gt -1 and basehdr.actualexptime gt HIsttime+seqtime)then clrhiseq=1
                      if(HIcurimg gt HIfstimg+n_elements(hiseqtbl))then clrhiseq=1
                      if(basehdr.actualexptime lt HIsttime)then clrhiseq=1 
                      if(clrhiseq eq 1)then begin
                          HIseqtbl=strarr(basehdr.numinseq)
                          HIsttime=basehdr.actualexptime
                          HIcmdtime=nom_hdr.cmdexptime
                          HIfstimg=basehdr.TELESCOPEIMGCNT
      	    	          HIBias=lonarr(basehdr.numinseq)
  		      endif      		
 		  endif      		

		endif
                HIBias(basehdr.imgseq)=long(basehdr.meanbias/1000.)
		if(lowhihi(0) eq -1) then make_Scc_ext_tbl,HIseqtbl,HIsttime,basehdr,nom_hdr,basehdr.imgseq, FAST=fast,dir=sdspath
      		if(HIcmdtime ne 0)then date_fname_utc=tai2utc(HIcmdtime, /nocorrect)
		if(HI1seqimg(0) gt -1 or HI2seqimg(0) gt -1)then begin
		       seqhdronly=0
                        ;the following is done twice once here to get the file name correct when the first header is missing then again for exposure after the header is created
		       if(hiseqtbl(0) eq '')then begin
		          hifilled=where(hiseqtbl ne '')
                          if (n_Elements(hifilled) gt 1)then hicadence=(float(strmid(hiseqtbl(hifilled(0)+1),0,8))-float(strmid(hiseqtbl(hifilled(0)),0,8)))/(hifilled(1)-hifilled(0))
                          if (n_Elements(hifilled) le 1)then hicadence=60.  ;set default if all image headers are missing
                          HIcmdtime=nom_hdr.cmdexptime-(hicadence*(basehdr.numinseq-1))       
                          HIsttime=basehdr.actualexptime-(hicadence*(basehdr.numinseq-1))
                          date_fname_utc=tai2utc(HIsttime, /nocorrect)
	               endif
		       nom_hdr.cmdexptime=HIcmdtime
                       date_cmd_utc    =tai2utc(nom_hdr.cmdexptime, /nocorrect)
                       fxaddpar,fits_hdr,'DATE-CMD',utc2str(date_cmd_utc,/noz)
                       scch.date_cmd=utc2str(date_cmd_utc,/noz)
                       fxaddpar,fits_hdr,'filename',utc2yymmdd(date_fname_utc,/yyyy,/hhmmss)+strmid(scch.filename,15,10)
                       scch.filename = utc2yymmdd(date_fname_utc,/yyyy,/hhmmss)+strmid(scch.filename,15,10)
                       fxaddpar,fits_hdr,'offsetcr',total(HIBias(where(HIBias gt 0)))/n_elements(where(HIBias gt 0))
	    	       scch.offsetcr = total(HIBias(where(HIBias gt 0)))/n_elements(where(HIBias gt 0))
		endif
	ENDIF
	if ((strmid(string(nom_hdr.PREEXPSCSTATUS, '(b16.16)'),8,1) EQ 1) or $
    	    (strmid(string(nom_hdr.POSTEXPSCSTATUS,'(b16.16)'),8,1) EQ 1)) then HISCANT='T'
	if(hdronly(0) gt -1)then begin
		seqhdronly=1
		if(HI1sumF(0) gt -1 or HI1sum(0) gt -1)then lsthi1img=basehdr.imgseq else lsthi2img=basehdr.imgseq
	endif
	IF ~keyword_set(NOEXTHDR) THEN save,filename=HIfile,HIseqtbl,HIsttime,HIcmdtime,HIscant,HIfstimg,HIBias
    endif
;IF (ishi and basehdr.numinseq GT 1 and basehdr.imgseq EQ 0) then stop
;    if(seqhdronly eq 1)then goto, skipscifile

;check to see if image is stored or created from SAVE area
;check to see if image was reloaded and sent down on SSR1 and SSR2 allowing for extended headers in both cases. 
    if strpos(scch.IP_00_19,'117') eq 0 then $
    	bhipcmdlog=basehdr.ipcmdlog[where(basehdr.ipcmdlog eq 117):19] else $
	bhipcmdlog=basehdr.ipcmdlog
    
    tagSI=where(bhipcmdlog ge 56 and bhipcmdlog le 60)  	    ; Tag as Saved Image 0-4
    addSI=where(bhipcmdlog ge 61 and bhipcmdlog le 65, naddsi)  ; Add Saved Image 0-4 to Current Image
    subSI=where(bhipcmdlog ge 66 and bhipcmdlog le 70, nsubsi)  ; Subtract Saved Image 0-4 from Cur Img
    evtd =where(bhipcmdlog EQ 4, nevtd)
    nsubsi=nsubsi-nevtd	    ; nr - Negate presence of subsi if Event Detect exists (FSW 5.03)
    naddsi=naddsi-nevtd
    relSI=where(bhipcmdlog ge 71 and bhipcmdlog le 75, nrelSI)  ; Release Saved Image 0-4

    IF (~keyword_set(NOEXTHDR) and  SpWximg(0) eq -1) THEN if(tagSI(0) gt -1)then begin ;save header structures for use in extended table
       SIbasehdr=basehdr & SInom_hdr=nom_hdr & siscch=scch
       SIfile=concat_dir(getenv('SCC_LOG'),'SI'+string(basehdr.ipcmdlog(tagSI)-56,'(i1.1)')+'_hdr.sav')
       save,filename=SIfile,SIbasehdr,SInom_hdr,siscch
    endif          

    IF (~keyword_set(NOEXTHDR) and  SpWximg(0) eq -1) THEN if (naddsi+nsubsi GT 0)then begin
      tmpSI=([addSi,subSI]) & tmpSI=tmpSI(where(tmpSI gt -1))
      tmpSI=bhipcmdlog(tmpSI)-61
      if(max(tmpSI) gt 4)then tmpSI(where(tmpSI gt 4))=tmpSI(where(tmpSI gt 4))-5
      SIseqtbl=strarr(n_Elements(tmpSI)+1)
      SIsttime=dblarr(n_elements(tmpSI))
      SIcmdtime=dblarr(n_elements(tmpSI))
      for nadd=0,n_elements(tmpSI)-1 do begin  ;read in time first to get expo order
        SIfile=concat_dir(getenv('SCC_LOG'),'SI'+string(tmpSI(nadd),'(i1.1)')+'_hdr.sav')
        print,'Searching for ',sifile
	flchck=findfile(SIfile)
        if(flchck ne '')then begin 
          SIbasehdr=-1 & SInom_hdr=-1
          restore,SIfile
          SIsize=size(SIbasehdr)
	  if(SIsize(2) eq 8) then SIsttime(nadd)=SIbasehdr.actualexptime
          SIsize=size(SInom_hdr)
	  if(SIsize(2) eq 8) then SIcmdtime(nadd)=SInom_hdr.cmdexptime
        endif
      endfor  

      SIsttime=[SIsttime,basehdr.actualexptime] ;add current image to starttime incase other images are missing.
      SIcmdtime=[SIcmdtime,nom_hdr.cmdexptime] ;add current image to cmndtime incase other images are missing.
      gdsi=where(SIsttime gt 0)  ;check for and fill in missing hdr times
      if n_elements(gdsi) le n_elements(tmpSI) then begin
         tbtwnexp=-1
         if n_Elements(gdsi eq 1)then begin  ;use OBS_ID to get time between exposures
             if scch.obs_id eq 1693 or  scch.obs_id eq 1694  or  scch.obs_id eq 1715 then tbtwnexp=12
             if scch.obs_id eq 1695 or scch.obs_id eq 1698  or scch.obs_id eq 1701 or scch.obs_id eq 1790 then tbtwnexp=10
             if scch.obs_id eq 1707 then tbtwnexp=20
	 endif else tbtwnexp=round((SIsttime(gdsi(1))-SIsttime(gdsi(0)))/(gdsi(1)-gdsi(0)))
         if tbtwnexp eq -1 then begin
	    openw,evlun,'missedhdr.txt',/get_lun
	    printf,evlun,'******* unknown OBS_ID for summed image *******'
	    printf,evlun,'Can not estimate times!!!'
	    printf,evlun,'Using 10 secs, Fits file may need correcting'
	    printf,evlun,'pipeline=',string(pipeline,'(a8)')
	    printf,evlun,'obsrvtry=    ',scch.obsrvtry
	    printf,evlun,'filename=    ',scch.filename
	    printf,evlun,'date_obs=    ',scch.date_obs
	    printf,evlun,'fileorig=    ',scch.fileorig
	    printf,evlun,''
	    close,evlun
	    free_lun,evlun
	    ;maillist='nathan.rich@nrl.navy.mil lynn.mcnutt@nrl.navy.mil'
	    spawn,'cat missedhdr.txt',/sh
            spawn,'mailx -s SECCHI-'+aorb+'_'+yymmdd+'_unknown_obs-id_COR2 '+maillist+' < missedhdr.txt',/sh
	    wait,errwait
            tbtwnexp=10
         endif
         ztsi=where(SIsttime eq 0) 
	 SIsttime(ztsi)=SIsttime(n_elements(SIsttime)-1)-tbtwnexp*(n_elements(SIsttime)-(ztsi+1))
	 SIcmdtime(ztsi)=SIcmdtime(n_elements(SIcmdtime)-1)-tbtwnexp*(n_elements(SIcmdtime)-(ztsi+1))
         if ztsi(0) eq 0 then begin
	    dateobscmnt='Estimated time - first HDR missing'
	    fxaddpar,fits_hdr,'COMMENT','DATE-OBS and DATE-CMD are estimated first HDR missing'
         endif
     endif
      sttime=min(SIsttime)
      SIcmdtime=min(SIcmdtime)

      SIsttime=SIsttime(0:n_elements(SIsttime)-2); remove current image starttime before sorting
      tmpSI=tmpSI(sort(SIsttime))
      SISCANT='F'

      for nadd=0,n_elements(tmpSI)-1 do begin ;read in header save files for extended table
        SIfile=concat_dir(getenv('SCC_LOG'),'SI'+string(tmpSI(nadd),'(i1.1)')+'_hdr.sav')
        print,'Searching for ',sifile
        flchck=findfile(SIfile)
        if(flchck ne '')then begin 
          SIbasehdr=-1 & SInom_hdr=-1
          restore,SIfile
          SIsize=size(SIbasehdr)
	  if(SIsize(2) eq 8)then begin
	     make_Scc_ext_tbl,SIseqtbl,sttime,SIbasehdr,SInom_hdr,nadd,Fast=fast,dir=sdspath,extscch=siscch
              if ((strmid(string(SInom_hdr.PREEXPSCSTATUS,'(b16.16)'),8,1) EQ 1) $
    	         or (strmid(string(SInom_hdr.POSTEXPSCSTATUS,'(b16.16)'),8,1) EQ 1))then SISCANT='T'
          endif
        endif
      endfor
      ;add current image to extended header
      make_Scc_ext_tbl,SIseqtbl,sttime,basehdr,nom_hdr,n_elements(tmpSI),Fast=fast,dir=sdspath
      ; update cmdexptime to first exposure of summed image
      wait,2
      nom_hdr.cmdexptime=SIcmdtime
      date_cmd_utc    =tai2utc(nom_hdr.cmdexptime, /nocorrect)
      date_fname_utc    =tai2utc(nom_hdr.cmdexptime, /nocorrect)
      if dateobscmnt eq '' then fxaddpar,fits_hdr,'DATE-CMD',utc2str(date_cmd_utc,/noz) else fxaddpar,fits_hdr,'DATE-CMD',utc2str(date_cmd_utc,/noz),dateobscmnt
      scch.date_cmd=utc2str(date_cmd_utc,/noz)
      fxaddpar,fits_hdr,'filename',utc2yymmdd(date_fname_utc,/yyyy,/hhmmss)+strmid(scch.filename,15,10)
      scch.filename = utc2yymmdd(date_fname_utc,/yyyy,/hhmmss)+strmid(scch.filename,15,10)
      pols=strmid(SIseqtbl,26,6)  & pols=pols(where(trim(pols) ne ''))    
      if total(fix(pols)) ne fix(pols(0))*n_Elements(pols) then begin
        polcmt=pols(0)+' deg '
    	for np=1,n_elements(pols)-1 do polcmt=polcmt+' + '+ pols(np)+' deg '
    	scch.polar = 1001
        fxaddpar,fits_hdr,'polar',scch.polar,polcmt
      endif
    endif


;++++correct bais for eventdetect images bug 210
    if ((evtd(0) gt -1) and (scch.biasmean lt 400)) then fxaddpar,fits_hdr,'BIASMEAN',scch.biasmean*2
  
    fxaddpar,fits_hdr,'HISTORY',version
    fxaddpar,fits_hdr,'HISTORY','Processed on '+machine+': '+!version.arch+' '+!version.os_name+' IDL'+!version.release
    IF (scitlrmissing NE 'no') THEN fxaddpar,fits_hdr,'HISTORY',scitlrmissing
    IF (iserror) THEN fxaddpar,fits_hdr,'HISTORY','IS error SKIPPED detected'
    fxaddpar,fits_hdr,'FILEORIG',fileorig
    ;if (dospice eq 1) then fxaddpar,fits_hdr,'ATT_FILE',ATT_FILE 
    if ((nhi1seqimg+nhi2seqimg+naddsi+nsubsi GT 0) and  SpWximg(0) eq -1 and lowhihi(0) eq -1) then begin
      exttblhdr = def_secchi_ext_hdr()
;      fxaddpar,exttblhdr,'XTENSION','TABLE   ',before='SIMPLE'
;      sxdelpar,exttblhdr,'SIMPLE'
      if (HI1seqimg(0) gt -1 or HI2seqimg(0) gt -1)then numin_seq=basehdr.numinseq*1 else numin_seq=n_Elements(tmpSI)
      fxaddpar,exttblhdr,'NAXIS2',numin_seq
      fxaddpar,fits_hdr,'EXTEND','T'
      fxaddpar,fits_hdr,'N_IMAGES',numin_seq	; defaults to 1 in make_scc_hdr
      exptime=fxpar(fits_hdr,'EXPTIME',comment=expcmt)
	if (HI1seqimg(0) gt -1 or HI2seqimg(0) gt -1) then BEGIN
            if(HISTTIME ne 0)then firsttime=HIsttime else firsttime=basehdr.actualexptime
	    IF datatype(HIseqtbl) EQ 'UND' THEN BEGIN
		exp0=(scch.imgseq+1)*scch.exptime
		expcmt='ESTIMATED -- some HDR0 files missing'
	    ENDIF ELSE IF (n_Elements(hiseqtbl) ne n_elements(where(hiseqtbl ne '')))then begin
                hiunfilled=where(hiseqtbl eq '') 
		hifilled=where(hiseqtbl ne '')
;                ushdrs=hifilled(where(shift(hifilled,-1)-hifilled eq 1))
                hiexp0=strmid(hiseqtbl(hifilled(0)),9,7)                    
;                hicadence=float(strmid(hiseqtbl(ushdrs(0)+1),0,8))-float(strmid(hiseqtbl(ushdrs(0)),0,8))
                if (n_Elements(hifilled) gt 1)then hicadence=(float(strmid(hiseqtbl(hifilled(0)+1),0,8))-float(strmid(hiseqtbl(hifilled(0)),0,8)))/(hifilled(1)-hifilled(0))
                if (n_Elements(hifilled) le 1)then hicadence=60.  ;set default if all image headers are missing
                if(hiseqtbl(0) eq '')then firsttime=basehdr.actualexptime-(hicadence*(basehdr.numinseq-1))
		hiseqtbl(hiunfilled)=string(hicadence*hiunfilled,'(f8.3)')+string(hiexp0,'(f7.3)')+string(' ','(a169)')
		if (where(hiunfilled eq 0) gt -1) then hiseqtbl(hifilled)=string(hicadence*hifilled,'(f8.3)')+strmid(hiseqtbl(hifilled),8,strlen(hiseqtbl(hifilled(0)))-8)
		exp0=(scch.imgseq+1)*scch.exptime
		expcmt='ESTIMATED -- some HDR0 files missing'
	    ENDIF ELSE exp0=total(float(strmid(HIseqtbl,9,7)))
 	    fxaddpar,fits_hdr,'EXPTIME',exp0,expcmt
	    scch.exptime=exp0
	endif
	if (naddsi+nsubsi GT 0) then BEGIN
            if(STTIME ne 0)then firsttime=sttime else firsttime=basehdr.actualexptime
	    IF datatype(SIseqtbl) EQ 'UND' THEN BEGIN
		exp0=(naddsi+nsubsi+1)*scch.exptime
		expcmt='ESTIMATED -- some HDR0 files missing'
	    ENDIF ELSE IF (n_Elements(SIseqtbl) ne n_Elements(where(SIseqtbl ne '')))then begin
		exp0=n_Elements(SIseqtbl)*scch.exptime
		expcmt='ESTIMATED -- some HDR0 files missing'
	    ENDIF ELSE BEGIN
	    	exp0=total(double(strmid(SIseqtbl,9,10)))
		expcmt=' sum of'+expcmt
	    ENDELSE
            scch.N_images=naddsi+nsubsi+1
	    fxaddpar,fits_hdr,'N_IMAGES',naddsi+nsubsi+1
	    fxaddpar,fits_hdr,'EXPTIME',exp0,expcmt
	    scch.exptime=exp0
	endif
      first_date_obs=utc2str(tai2utc(firsttime, /nocorrect),/noz)
      if dateobscmnt eq '' then fxaddpar,fits_hdr,'DATE-OBS',first_date_obs else fxaddpar,fits_hdr,'DATE-OBS',first_date_obs,dateobscmnt 
      scch.DATE_OBS=first_date_obs
      date_av=tai2utc(((firsttime+basehdr.actualexptime+(basehdr.actualexpduration*4.0e-6))/2), /nocorrect)
      DATE_AVG=utc2str(date_av,/noz)
      scch.date_avg=date_avg
      fxaddpar,fits_hdr,'DATE-AVG',DATE_AVG
      if (HI1seqimg(0) gt -1 or HI2seqimg(0) gt -1 and hiscant eq 'T') then fxaddpar,fits_hdr,'SCANT_ON','T'
      if (naddsi+nsubsi GT 0 and SIscant eq 'T') then fxaddpar,fits_hdr,'SCANT_ON','T'
    endif
    
;COR2 space weather images not summed 10-30-2007
    if (naddsi-nsubsi GT 0 and SpWximg(0) gt -1) then BEGIN
;	exp0=(naddsi+nsubsi+1)*scch.exptime
;	fxaddpar,fits_hdr,'EXPTIME',exp0
    	fac=(naddsi-nsubsi+1)
	newbias=scch.biasmean/fac
	IF newbias GT 500 THEN BEGIN
    	    msg='% SECCHI_REDUCE correcting '+infile+' bias '+trim(scch.biasmean)+' to '+trim(newbias)
	    printf,lulog,msg
	    print,msg
	    wait,2
	    scch.biasmean=newbias
	    fxaddpar,fits_hdr,'BIASMEAN',newbias,' divided by '+trim(fac)+' from basehdr'
	    fxaddpar,fits_hdr,'COMMENT','workaround correction for Bug 252 applied to BIASMEAN'
	ENDIF
    endif   ; ((nhi1seqimg+nhi2seqimg+naddsi+nsubsi GT 0) and  SpWximg(0) eq -1 and lowhihi(0) eq -1)
    
;++++ Begin derive outdir

;select SUB directory for image img_dir set to img 
;  if (seqhdronly eq 0) then begin

    img_dir='img'
    IF scch.polar GE 0 and scch.polar LE 360 THEN img_dir='seq'
    
    case strmid(scch.filename,16,1) of
      'k' : IF (scch.detector NE 'HI1') AND (scch.detector NE 'HI2') THEN img_dir='cal'   ; DARK
      'e' : img_dir='cal'   ; LED
      'c' : img_dir='cal'   ; CONT
    else : 
    endcase

    if isHISAM eq 1 then img_dir='seq'

;create SUB directory name and make hin directories hi_n
;eDoorStatus : $
;[   'CLOSED',	$
;    'IN_TRANSIT',	$
;    'OPEN ',	$
;    'OFF',	$
;    'ERROR',    $
;    replicate('invalid',250), $
;    'NO_DATA' ]   }
    
    ; SCIP door CLOSED or IN_TRANSIT go in cal; HI door CLOSED (only) go in cal
    ; disabled door results in OFF, which for now we assume is OPEN - nbr, 090506
    IF ((scch.doorstat EQ 1) AND  (scch.detector NE 'HI1') AND (scch.detector NE 'HI2')) OR $
        (scch.doorstat EQ 0)  $
       THEN img_dir='cal' 

    ; Check for special events
    IF insecchicalperiod(scch) THEN img_dir='cal'
    
    case trim(strlowcase(scch.detector)) of
    	'hi1' : BEGIN
	    teldir='hi_1'
	    END
    	'hi2' : BEGIN
	    teldir='hi_2'
	    END
    	else  : teldir=trim(strlowcase(scch.detector))
    endcase
    subdir=img_dir+'/'+teldir
    
    IF keyword_set(NOTELDIR) THEN $
    	outdir =filepath( yyyymmdd,ROOT=destdir ) ELSE $
    	outdir =filepath( yyyymmdd,ROOT=destdir,SUBD=SUBDir )

    help,outdir,scch.date_cmd
;++++ End derive outdir
    
    imsize=0	; by default no image
    misslist=''
    nmissing=0 ;us to be -1 set to 0 for HISAM no-compressed images
    missn=-1
    nblocks=0
    
    IF keyword_set(HDR_ONLY) and (scch.comprssn NE 5) THEN BEGIN
    	indir=getenv_slash('secchi')+'lz/L0/'+strlowcase(aorb)+rstrmid(outdir,0,18)
	;stop
	finfil=concat_dir(indir,scch.filename)
	print,finfil
	printf,lulog,'Reading '+finfil
	IF file_exist(finfil) THEN BEGIN
	    image=sccreadfits(concat_dir(indir,scch.filename),khdr)
	    ;scch.rectify=khdr.rectify
	    goto,afterdecomp
	ENDIF ELSE BEGIN
	    print,'File does not exist--continuing with decomp.'
	    printf,lulog,'File does not exist--continuing with decomp.'
	ENDELSE
    ENDIF
    
    IF (scch.comprssn EQ 5) OR NOT(file_exist(scidat)) THEN BEGIN	; Header only
    	image=0
        fxaddpar,fits_hdr,'naxis',0
        fxaddpar,fits_hdr,'naxis1',0
        fxaddpar,fits_hdr,'naxis2',0
        scch.filename=strmid(scch.filename,0,16)+'h'+strmid(scch.filename,17,8) ;change fits file name to h for header only database entry
        fxaddpar,fits_hdr,'filename',scch.filename
        printf,lulog,sciimg+' is header only'
        print,sciimg+' is header only'
        IF (scch.comprssn NE 5) THEN BEGIN
            msg=sciimg+' ('+scch.filename+') is not supposed to be header only!!!'
	    IF ~isrt THEN BEGIN
    		openw,elun,'decomp.txt',/get_lun,/append
		printf,elun,'dbfile/session= '+aorb+' '+pipeline+' '+dbroot
		printf,elun,msg
		printf,elun,''
		close,elun
		free_lun,elun
	    ENDIF
            printf,lulog,msg
            print,msg
            wait,errwait
        ENDIF
    	msg='Not writing '+scch.fileorig+'/'+scch.filename+'; skipping to db scripts'
	print,msg
	IF ~keyword_set(FAST) THEN wait,2
	printf,lulog,msg
    	goto, writedb
    ENDIF 
    
    IF (strmid(sciimg,0,7) EQ 'WARNING') THEN BEGIN	; Bad spacewire file
    	image=0
        fxaddpar,fits_hdr,'naxis1',0
        fxaddpar,fits_hdr,'naxis2',0
        printf,lulog,sciimg
	goto, skipscifile

    ENDIF ELSE BEGIN	; start process Normal image

; ++++ process image ++++
    	fc_counts=parsefcmetafile(infile)
	fcmissing=fc_counts.packets_expected-fc_counts.packets_found
	fcpkts=fc_counts.packets_expected
	fcerrs=fc_counts.nerrors

	segqual=0
    	cfline='No compression error message detected.'
	IF keyword_set(HDR_ONLY) THEN BEGIN
	    message,'HDR_ONLY set -- NMISSING and MISSLIST not derived from decomp output!',/info
        ENDIF ELSE IF scch.comprssn EQ 7 THEN BEGIN    
	
; ++++ Rice compression
    	    
	    command=uncompr_cmd+' '+scidat+' '+sciimg
	    print,command
	    spawn,command ,uncompresult,/sh
	    print,uncompresult
            nres=n_elements(uncompresult)

	    FOR i=0L,nres-1 DO BEGIN
	    	IF strlen(uncompresult[i]) GT 500 $
		THEN prlin=strmid(uncompresult[i],0,120)+'...' $
		ELSE prlin=uncompresult[i]
		printf,lulog,prlin
		IF strpos(prlin,'error') GE 0 THEN cfline='ricerecon64: '+prlin
	    ENDFOR
            nblkx=(scch.naxis1 / 64)
            nblky=(scch.naxis2 / 64)
	    ;scch.naxis1 = nblkx * 64	; if compressed, naxis must be mult of 64
	    ;scch.naxis2 = nblky * 64
	    ; ++++ parse result from ricerecon64 ++++
	    IF scch.naxis1 LE 0 or scch.naxis1 GT 2176 THEN BEGIN
	        stmts=str_sep(uncompresult[2],' ')
	        ; Method=1 BCol1=0 BCol2=15 BRow1=0 BRow2=15 ncol=1024 nrow=1024
	        IF n_elements(stmts) EQ 7 THEN BEGIN
	    	    res1=execute(stmts[5])
		    res2=execute(stmts[6])
	    	    IF (res1 and res2) THEN BEGIN
		        scch.naxis1=ncol
		        scch.naxis2=nrow
		    ENDIF
	        ENDIF
	    ENDIF
	    ; ++++ extract missing blocks from ricerecon64 output ++++
    	    parts=strsplit(strmid(uncompresult[nres-3],2,strlen(uncompresult[nres-3])),/extract)  ; --iiii out of iiii blocks were good--
            gdbk=FIX(parts(0)) 
            nblocks=FIX(parts(3)) 
	    bfac=1.
	    IF ((nhi1seqimg+nhi2seqimg GT 0) and (lowhilow(0) eq -1) and (lowhihi(0) eq -1) and  (SpWximg(0) eq -1)) THEN bfac=2.	
	    	; Array is square and has 4-byte elements
            if(gdbk ne nblocks)then nmissing = ceil((bfac*nblkx*nblky+bfac - gdbk)/bfac) else nmissing=0
	    scch.nmissing=nmissing
;            stop
    	    nblocks=bfac*nblkx*nblky
            fxaddpar,fits_hdr,'NMISSING',scch.nmissing
	    ; ++++ extract compression factor ++++
            cfline=uncompresult[nres-4]	; Without packet overhead bpp=ff.ffffff CF=f.ffffff
            parts=str_sep(cfline,'=')
            IF (scch.nmissing EQ 0) THEN fxaddpar,fits_hdr,'COMPFACT', float(parts[2]), ' Without packet overhead'

        ENDIF ELSE IF scch.comprssn GT 7 and scch.comprssn LT 15 THEN BEGIN    	
	    
; ++++ H-compress

    	    command=huncompr_cmd+' < '+scidat+' > '+sciimg+' 2> hd64.out'
	    print,command
	    spawn, command,/sh
            uncompresult=readlist('hd64.out')
	    print,uncompresult
            nres=n_elements(uncompresult)

	    FOR i=0L,nres-1 DO BEGIN
	    	prlin=uncompresult[i]
		printf,lulog,prlin
		IF strpos(prlin,'error') GE 0 THEN cfline='hd64: '+prlin
    	    ENDFOR
            nblkx=ceil(scch.naxis1 / 64.)
            nblky=ceil(scch.naxis2 / 64.)
	    nblocks=nblkx*nblky
;stop
	    ;scch.naxis1 = nblkx * 64	; if compressed, naxis must be mult of 64
	    ;scch.naxis2 = nblky * 64	; it doesn't work this way -- NR, 1/4/07
	    ; ++++ extract missing blocks ++++
            ;gdbkline=uncompresult[nres-3]	; --iiii out of iiii blocks were good--
            ;pos=strpos(gdbkline,' ')
            ;gdbk=FIX(strmid(gdbkline,2,pos))
            ;scch.nmissing = nblkx*nblky+1 - gdbk
            ;fxaddpar,fits_hdr,'NMISSING',scch.nmissing
	    ; ++++ extract compression factor ++++
            cfline=uncompresult[nres-1]	; Compression Factor =    0.474 bpp = 33.74951
            parts=strsplit(cfline,/extract)
	    np=n_elements(parts)
           ;IF (scch.nmissing EQ 0) THEN 
	    ;for i=0,np-1 do print,i,'  ',parts[i]

	    IF np GE 7 THEN $
	    fxaddpar,fits_hdr,'COMPFACT', float(parts[6])  , ' from hd64 output' ELSE $
	    
            IF (not(keyword_set(FAST))) THEN wait, 5

        ENDIF ELSE IF scch.comprssn GT 89 and scch.comprssn LT 102 THEN BEGIN
    	    
; ++++ Icer

    	    expectedbytes=1
	    readbytes=0
    	    sgmt=0
	    icererr=0
	    nseg=0
    	    msg3=''


	    command=iuncompr_cmd+' -i '+scidat+' -o '+sciimg
	    print,command
    	    spawn, command,uncompresult,/sh
    	    ;print,uncompresult
    	    nres=n_elements(uncompresult)
	    IF nres EQ 1 THEN icererr=1
	    IF nres EQ 1 THEN goto, icererrstuff
    	    ;--Parse uncompresult
    	    print
;   0 Verbose ON(1)
;   1 Input file: 7602012C-548.dat
;   2 Output file: 7602012C-548.img
;   3 nseg: 32 status: 0 status1: 0
    	    i=3
	    print,uncompresult[i]
	    parts=strsplit(uncompresult[i],/extract)
	    nseg=fix(parts[1])
	    ;help,nseg
	    nmbk=nseg
	    IF nseg GT 64 or nseg LE 0 THEN icererr=1
	    IF nseg GT 64 or nseg LE 0 THEN goto, icererrstuff
	    nbits=fltarr(nseg)
	    dataseg=lonarr(nseg+1)   ; bytes
	    segstat=intarr(nseg)
	    segqual=dblarr(nseg)
;   4 Status:
;   5 Status1:
;   6 nbits[0]: 3040
	    i=5
	    print,uncompresult[i]
	    IF strpos(uncompresult[i],'ICER_DIV2_FLAG') GT 0 THEN BEGIN
	    	icerdiv2flag=1
		cfline='ICER_DIV2_FLAG detected in idecomp'
	    ENDIF
	    i=i+icerdiv2flag
	    i0=i+1
	    ; If there is a Status1 message, there is a blank line after it
	    REPEAT BEGIN
		i=i+1
	    	parts=strsplit(uncompresult[i],/extract)
		IF strmid(parts[0],0,4) EQ 'nbit' THEN nbits[i-i0]=long(parts[1])
	    ENDREP UNTIL strmid(parts[0],0,4) NE 'nbit'
;   37 nbits[31]: 3488
;   38 tot_nbits: 3257120  bytes: 407140
    	    
;   39 Read 474452 bytes
;   40 dataseg[0] 0 bytes
    	    IF strpos(uncompresult[i],'error') GE 0 THEN icererr=1
	    IF (icererr) THEN goto, icererrstuff
	    print,uncompresult[i:i+1]
	    parts=strsplit(uncompresult[i],/extract)
	    expectedbytes=long(parts[3])
	    i=i+1
	    parts=strsplit(uncompresult[i],/extract)
	    readbytes=long(parts[1])
	    dataseg[nseg]=readbytes
	    i0=i+1
	    REPEAT BEGIN
		i=i+1
	    	parts=strsplit(uncompresult[i],/extract)
		IF strmid(parts[0],0,7) EQ 'dataseg' THEN dataseg[i-i0]=long(parts[1])
	    ENDREP UNTIL strmid(parts[0],0,7) NE 'dataseg' 
    	    IF strpos(uncompresult[i],'error') GE 0 THEN icererr=1
	    IF (icererr) THEN goto, icererrstuff
;   71 dataseg[31] 406704 bytes
;   72
;   73 decompressing...
;   74 Segment statuses:
;   75     segment 0:        0
    	    print,uncompresult[i+1:i+2]
	    i=i+2
	    i0=i+1
	    REPEAT BEGIN
		i=i+1
	    	parts=strsplit(uncompresult[i],/extract)
		sgmt=i-i0
		IF strmid(parts[0],0,7) EQ 'segment' THEN segstat[sgmt]=fix(parts[2])
		
	    ENDREP UNTIL strmid(parts[0],0,7) NE 'segment' 
    	    IF strpos(uncompresult[i],'error') GE 0 THEN icererr=1
	    IF (icererr) THEN goto, icererrstuff
;   106     segment 31:        2
;   107 Status word:        0
;   108 Width: 2048  height: 2048
;   109 Wavelet filter: 0   Number of decompositions: 4  Number of segments: 32
;   110 (ystart, xstart, height, width)
;   111     segment 0: (0, 0, 320, 400); quality: 5.000000e+00
    	    print,uncompresult[i:i+2]
	    i=i+3
	    i0=i+1
	    REPEAT BEGIN
		i=i+1
	    	parts=strsplit(uncompresult[i],/extract)
		IF strmid(parts[0],0,7) EQ 'segment' THEN segqual[i-i0]=double(parts[7])
	    ENDREP UNTIL strmid(parts[0],0,7) NE 'segment' 
    	    IF strpos(uncompresult[i],'error') GE 0 THEN icererr=1
	    IF (icererr) THEN goto, icererrstuff
    	    print,''
	    nmissing=0
    	    FOR j=1,nseg DO BEGIN
    	    	sgmt=j-1
	    ; --Test each segment
	    	completeness=100.*8*(dataseg[j]-dataseg[j-1])/nbits[j-1]
    	    	qual=segqual[sgmt]
	    	print,'dataseg',string(j-1,'(i3)'),' is ', $
		    string(completeness,'(f6.2)'),'% of nbits ', $
		    'and quality ',trim(segqual[j-1]),' and status ',trim(segstat[j-1])
	    ; --Compute MISSLIST here	
	    	IF (qual GT 7 or qual LT 0.1) $
		or segstat[sgmt] NE 0 $
		or completeness NE 100 THEN BEGIN
		    IF segstat[sgmt] EQ 0 $
		    or (qual LT 7 and qual GT 1) THEN BEGIN
			msg3=' *** Check seg '+trim(sgmt)
			;icererr=1
		    ENDIF
		    ;--Do not count as missing if no file_capture errors are detected (Bug 81)
		    IF fc_counts.packets_found EQ 0 or fcerrs GT 0 THEN BEGIN
		    	miss34=inttobn(sgmt,34)
	    	    	misslist=misslist+string(miss34,format='(a2)')
		    	nmissing=nmissing+1
		    ENDIF ELSE msg3=' *** bad quality detected but 0 FC errors (not marked as missing)'
		ENDIF
		
    	    ENDFOR
	    
    	    ;IF ~icererr THEN goto, after_icererr
    	    icererrstuff:
	    FOR i=0L,nres-1 DO BEGIN
	    	prlin=uncompresult[i]
		IF (icererr) THEN print,prlin
		printf,lulog,prlin
		IF strpos(prlin,'error') GE 0 THEN cfline='idecomp: '+prlin
		IF nres EQ 1 and prlin EQ '' THEN cfline='idecomp: Segmentation fault?'
	    ENDFOR
	    
	    after_icererr:
	    IF expectedbytes NE readbytes and nmissing EQ 0 THEN BEGIN
		icererr=1
		nmissing=1
		misslist='0'
	    ENDIF
	    ;stop
	    IF nmissing LT 0 THEN nmissing=nseg
	    fxaddpar,fits_hdr,'NMISSING',nmissing,'out of '+trim(string(nseg))+' segments'
	    scch.nmissing=nmissing
	    IF nmissing GT 0 THEN BEGIN
		fxaddpar,fits_hdr,'MISSLIST',misslist,'base34 fmt=(a2); use bntoint(x,34)'
	    ENDIF
	    IF sgmt+1 NE nseg THEN icererr=1
	    IF (icererr) or (msg3 NE '') THEN BEGIN
		msg1='segments counted='+trim(sgmt+1)+', segments reported='+trim(string(nseg))
		msg2='expectedbytes='+trim(expectedbytes)+', readbytes='+trim(readbytes)
		print,scidat,' ',msg1
		print,scidat,' ',msg2
		print,scidat,' ',cfline+msg3
		print,scidat,' NMISSING='+trim(nmissing)+': '+misslist
		print
		IF ~isrt THEN BEGIN
		    print,'Waiting ',trim(errwait),'...'
		    wait,errwait
    		    openw,elun,'decomp.txt',/get_lun,/append
		    printf,elun,scidat+' idecomp error: dbfile/session= '+aorb+' '+pipeline+' '+dbroot
		    printf,elun,msg1
		    printf,elun,msg2
		    printf,elun,cfline+msg3
		    printf,elun,' NMISSING='+trim(nmissing)+': '+misslist
		    printf,elun,''
		    close,elun
		    free_lun,elun
		ENDIF
	    ENDIF
            nblkx=ceil(scch.naxis1 / 64.)
            nblky=ceil(scch.naxis2 / 64.)
	    ;scch.naxis1 = nblkx * 64	; if compressed, naxis must be mult of 64
	    ;scch.naxis2 = nblky * 64

            IF (not(keyword_set(FAST))) THEN wait, 2

        ENDIF ELSE IF scch.comprssn EQ 43 THEN BEGIN    ; Gnd Test image
            print,scch.fileorig,' IPCMD:',nom_hdr.ipcmd
            IF (not(keyword_set(FAST))) THEN wait, errwait
            sciimg=getgndtstfile(basehdr) 
        ENDIF ELSE sciimg=scidat
; ++++ End uncompress image
    	
	msg1=fileorig+', '+ipdat[scch.comprssn]+', nmissing=, '+trim(nmissing)+', numblk=, '+trim(nmbk)+', fcpkts=, '+trim(fcpkts)+', fcmissing=, '+trim(fcmissing)+', fcerrs=, '+trim(fcerrs)+', maxqual=, '+trim(max(segqual))
	printf,lulog,msg1
	IF scch.nmissing EQ 0 and (fcmissing GT 0 or  fcerrs GT 0) THEN BEGIN
	    openw,elun,'decomp.txt',/get_lun,/append
	    printf,elun,msg1
	    printf,elun,''
	    close,elun
	    free_lun,elun
	ENDIF

;--Treat missing sciimg file and sciimg file of size zero the same
    sciimgok=0
    IF FILE_EXIST(SCIIMG) THEN BEGIN
    	openr,1,sciimg
	imgstat=fstat(1)
	IF imgstat.size GT 0 THEN sciimgok=1
    ENDIF

    IF (sciimgok) THEN BEGIN        ;*** Added by Thompson ***
; ++++ Space Weather case where pcol/prow may be undefined
        IF (hdrstat.size LT 100) THEN BEGIN
            scch.naxis1=64*(fix(sqrt(imgstat.size/2)/64))
            ; SPWX images should always be square
            scch.naxis2=scch.naxis1
        ENDIF
	;stop
	IF ((nhi1seqimg+nhi2seqimg GT 0) and (lowhilow(0) eq -1) and (lowhihi(0) eq -1) and  (SpWximg(0) eq -1)) THEN BEGIN	; Array is square and has 4-byte elements
            ;scch.naxis1=64*(fix(sqrt(imgstat.size/4)/64))
            ;--updated 2/14/07, nbr
            side=scch.naxis1<scch.naxis2
            scch.naxis1=side
            scch.naxis2=side
            ; HI sum buff images should always be square
            scch.naxis2=scch.naxis1    
	    amessage='Reading INT array: '+string(scch.naxis1*2)+'  x'+string(scch.naxis2)
            print,amessage
	    printf,lulog,amessage
	    amessage='Converting INT array: to LONG array '+string(scch.naxis1)+'  x'+string(scch.naxis2)
            print,amessage
	    printf,lulog,amessage
   	    ;a = assoc(1,lonarr(scch.naxis1,scch.naxis2))
            a = assoc(1,uintarr(scch.naxis1*2,scch.naxis2))
    	    tmpimage=a[0]
            lword=indgen(scch.naxis1) *2
            sword=lword+1
    	    image=lonarr(scch.naxis1,scch.naxis2)
            image(*,*)=(tmpimage(lword,*)*65536l)+tmpimage(sword,*)
	    ; swap_endian_inplace, image, /SWAP_IF_LITTLE_ENDIAN
	    ; swap_endian was necessary for 6C3000SN.425 on iapetus
	    scch.p2row=(nom_hdr.p2row)<(nom_hdr.p1row+2047)
	    scch.p2col=(nom_hdr.p2col)<(nom_hdr.p1col+2047)
	    fxaddpar,fits_hdr,'P2ROW',scch.p2row
	    fxaddpar,fits_hdr,'P2COL',scch.p2col
	    ; workaround to fix Bug 66
        ENDIF ELSE IF ((lowhilow(0) gt -1) or (lowhihi(0) gt -1)) THEN BEGIN	; define long array for 2 part hi images
            nax1=scch.naxis1 & nax2=scch.naxis2
    	;quick fix for sidelobe HI psummed images to correct naxis size
           if imgstat.size ne (2L*scch.naxis1*scch.naxis2) then begin
    	    	nax1=long(sqrt(imgstat.size/2))
    	    	nax2=long(sqrt(imgstat.size/2))
    	    endif
	    amessage='Reading UINT array: '+string(nax1)+'  x'+string(nax2)
            print,amessage
	    printf,lulog,amessage

   	    a = assoc(1,uintarr(nax1,nax2))
    	    image=a[0]
	    print,'lowhilow',lowhilow,' lowhihi',lowhihi
	    ; first hi1,2 image [5,6], [-1]; second [6,5],[10,9]
	   
	   ; !! TEMPORARY fix until READFITS.PRO works correctly in secchi_hi_low_merge.pro with UINT
	    IF !version.os NE 'darwin' THEN image=long(image)
	    ; on darwin i386, this gives wrong result, presumably because the bytes get swapped
	    ; this does get done in secchi_hi_low_merge.pro, so should be unnecessary here
        ENDIF ELSE IF imgstat.size EQ (long(scch.naxis1)*scch.naxis2) THEN BEGIN
	    amessage='Reading BYTE array: '+string(scch.naxis1)+'  x'+string(scch.naxis2)
            print,amessage
	    printf,lulog,amessage
   	    a = assoc(1,bytarr(scch.naxis1,scch.naxis2))
    	    image=a[0]
	    
        ENDIF ELSE IF imgstat.size GE (2L*scch.naxis1*scch.naxis2) THEN BEGIN
	    amessage='Reading UINT array: '+string(scch.naxis1)+'  x'+string(scch.naxis2)
            print,amessage
	    printf,lulog,amessage
   	    a = assoc(1,uintarr(scch.naxis1,scch.naxis2))
    	    image=a[0]
    	    ;IF scch.comprssn LT 88 and scch.comprssn GT 101 THEN swap_endian_inplace, image, /SWAP_IF_LITTLE_ENDIAN	    
            ; The swap_endian was required for an uncompressed HI (HISAM) but then it messed up
	    ; euvi, so maybe it is not necesary after idecomp. Don't know about Rice.
	ENDIF ELSE IF imgstat.size EQ (scch.naxis2*2) THEN BEGIN
	    amessage='Reading UINT array: '+string(scch.naxis2)
            print,amessage
	    printf,lulog,amessage
   	    a = assoc(1,bytarr(scch.naxis2))
    	    image=a[0]
        ENDIF ELSE IF imgstat.size GT 0 THEN BEGIN
	    amessage='Reading UINT partial array: '+string(scch.naxis1)+'  x'+string(scch.naxis2)
            print,amessage
	    printf,lulog,amessage
    	    a = assoc(1,uintarr(imgstat.size/2))
	    image=uintarr(scch.naxis1,scch.naxis2)
	    tim=a[0]
	    image[0]=tim
	    
	ENDIF 
	;ENDIF ELSE BEGIN
        ;    print,'Error in image--using zeros.'
        ;    image=uintarr(scch.naxis1,scch.naxis2)
        ;    nblocks=(scch.naxis1/64)*(scch.naxis2/64)
        ;    fxaddpar,fits_hdr,'NMISSING',nblocks
        ;    ;fxaddpar,fits_hdr,'COMMENT','hd64 error, lastline= '+uncompresult[nres-1]
        ;ENDELSE
	
	;IF !version.os EQ 'darwin' and !version.arch NE 'ppc' THEN swap_endian_inplace, image, /SWAP_IF_LITTLE_ENDIAN
	; On darwin i386 or x86_64, /swap_if_little_endian is required. I presume this is because the decompression binaries
	; need to be recompiled for x86 (but aside from the byte-swap issue it appears they still work).
	; on linux x86, /swap is not required and if used then result is wrong.
	; on solaris, /swap_if_little has no effect and is not required.
	openr,datlun,scidat,/get_lun
	datstat=fstat(datlun)
	close,datlun
	free_lun,datlun
	
	compressionfactor=float(imgstat.size)/datstat.size
	printf,lulog,"Compression factor from file sizes = "+trim(string(compressionfactor))
	print,"Compression factor from file sizes = "+trim(string(compressionfactor))
	IF scch.comprssn GT 89 and scch.comprssn LT 102 THEN $	; Icer
	    fxaddpar,fits_hdr,'COMPFACT', compressionfactor, ' From file sizes'
	
        print,'image:'
        IF n_elements(image) GT 1 THEN maxmin,image
        
;  convert from signed to unsigned integer, if necessary 
;	ltz = where(image LT 0,nltz)
;	IF nltz GT 0 THEN BEGIN
;            scch.bzero=32768
;            fxaddpar,fits_hdr,'BZERO',32768
;        ENDIF
	IF scch.comprssn EQ 43 THEN BEGIN
 ; ++++ mask first row of GND TEST image because it contains header ++++
           image[*,0]=median(image)
           fxaddpar,fits_hdr,'HISTORY','Header file was '+scch.fileorig
           fxaddpar,fits_hdr,'FILEORIG',sciimg
        ENDIF

    ENDIF ELSE BEGIN   ;    	    	    *** Added by Thompson ***
    	IF scch.comprssn GT 89 and scch.comprssn LT 102 THEN $
	; already did ipmsg.txt 
	openw,elun,'junk',/get_lun ELSE $
    	openw,elun,'decomp.txt',/get_lun,/append
	print,scidat+' did not generate a decompressed image file'
	IF ~keyword_set(FAST) THEN wait,5
        image=0
        nblocks=(scch.naxis1/64)*(scch.naxis2/64)
        fxaddpar,fits_hdr,'NMISSING',nblocks
	scch.nmissing=nblocks
	printf,elun,''
	printf,elun,'dbfile/session= '+aorb+' '+pipeline+' '+dbroot
	printf,elun,sci_attic+yymmdd+'/'+scidat+' did not generate a decompressed image file: '+cfline
	printf,elun,''
	close,elun
	free_lun,elun
	printf,lulog,'ERROR: '+scidat+' did not generate a decompressed image file'
    ENDELSE	;FILE_EXIST(SCIIMG)
    close,1

    fxaddpar,fits_hdr,'COMMENT',cfline
    
    afterdecomp:
 
    imsize=size(image)
    print,'Waiting 3...'
    IF (not(keyword_set(FAST))) THEN wait,3
    ;IF imsize[0] EQ 2 THEN tvscl,rebin(image,512,512)
    ;wait,3
    print,'Continuing.'

; ++++ skip image writes and summary stuff if any header only / problem image
    if imsize[0] LT 2 then BEGIN
    	msg='Not writing '+scch.fileorig+'/'+scch.filename+'; skipping to db scripts'
	print,msg
	IF ~keyword_set(FAST) THEN wait,2
	printf,lulog,msg
    ENDIF
    if imsize[0] LT 2 then goto, writedb

; ++++ add cosmic report to extended table and fits header
    cosmicrpt=where(basehdr.ipcmdlog eq 113)
    if (crs2 GE 1 and cosmicrpt(0) gt -1 and imsize(0) eq 2 and SpWximg(0) eq -1 and ~keyword_set(NOEXTHDR) and lowhihi(0) eq -1)then begin
      fcsmcs=where(strmid(hiseqtbl,9,7) ne 0.0); or strmid(hiseqtbl,9,7) eq '')
      csmc1=-1
      if (aorb eq 'A') then crmove=anytim2tai('2006-12-18T12:00:00') else crmove=anytim2tai('2006-12-26T18:00:00')
      if(basehdr.actualexptime lt crmove)then csmc1=1 else $
        if(image((n_Elements(image)-basehdr.numinseq)-1) eq basehdr.numinseq)then csmc1=n_Elements(image)-basehdr.numinseq else $
      	  if(image((n_Elements(image)-n_elements(fcsmcs))-1) eq n_elements(fcsmcs))then csmc1=n_Elements(image)-n_elements(fcsmcs)
      if(csmc1 gt -1)then begin
        print,n_elements(fcsmcs),'  ',image(csmc1-1)
        csmcs=indgen(n_elements(fcsmcs)) +csmc1
        extseqtbl=strarr(basehdr.numinseq)
        cosmics=string(image(csmcs),'(i7)')
        if(HI1seqimg(0) gt -1 or HI2seqimg(0) gt -1) $
      	    	then for cn=0,n_elements(fcsmcs)-1 do $
      	    	    	extseqtbl(fcsmcs(cn))=strmid(hiseqtbl(fcsmcs(cn)),0,181)+string(cosmics(cn),'(i7)')+strmid(hiseqtbl(fcsmcs(cn)),188,13)
        if(image(csmc1-1) gt 0)then fxaddpar,fits_hdr,'COSMICS',total(image(csmcs))/image(csmc1-1)
        HIseqtbl=extseqtbl
      endif
    endif

;+++ notify of IP event +++
    critevent=string(basehdr.CRITEVENT,'(z4)')
    if(strmid(critevent,0,1) eq 'd' or  strmid(critevent,0,1) eq 'e' or pipeline ne 'spw')then scc_event_track,fits_hdr,pipeline,NOEVENT_NOTIFY=NOEVENT_NOTIFY
;+++ end notify of IP event +++

; check for divid by 2 and correct if needed
    IF scch.comprssn GT 89 and scch.comprssn LT 102 THEN BEGIN    	; Icer
; moving reduce_statistics later
        scc_icerdiv2,scch,image,/PIPELINE, icerh, div2msg,FAST=fast
	IF strmid(div2msg,0,3) EQ 'Cor' THEN BEGIN
	; correction may be applied even if div2corr is F; see scc_icerdiv2.pro
	    fxaddpar,fits_hdr,'DIV2CORR',scch.div2corr,'mod by scc_icerdiv2.pro in secchi_reduce'
	    fxaddpar,fits_hdr,'HISTORY',icerh
	ENDIF
    endif

;  if masked then apply mask if not ICER
    
    if scch.mask_tbl ne 'NONE' and ~ISHISPW and ~keyword_set(IGNORE_MASK) then scc_apply_mask, scch,image,fits_hdr

; ++++ correct side lobe files before rectify
;scc_sidelobe_update_hdr,fits_hdr,outdir,img_dir

; ++++ rectify image before writing fits file
; - At FITS keyword meeting on 2/1/07, it was agreed that all DARK, LED, and CONT images
;   would NOT be rectified, except HI DARK

    if (scch.seb_prog EQ 'LED') or $
	  (scch.seb_prog EQ 'CONTIN') or $
	  (scch.seb_prog EQ 'DARK' and $
	  (scch.detector NE 'HI1' and scch.detector NE 'HI2')) then begin
	msg='Not rectifying '+scch.filename
        tmpimg=secchi_rectify(image,scch,hdr=fits_hdr,/NOROTATE)
	; sets RECTROTA keyword
	printf,lulog,msg
	;img_dir='cal'
    endif else begin
        tmpimg=secchi_rectify(image,scch,hdr=fits_hdr)
	IF keyword_set(HDR_ONLY) THEN BEGIN
	    IF khdr.rectify NE 'T' THEN image=tmpimg ELSE print,'Image already rectified.'
	ENDIF ELSE image=tmpimg
	;img_dir='img'
    endelse
    IF scch.mask_tbl EQ 'NONE' and $
    	((scch.comprssn EQ 7 and scch.nmissing GT 0) or (scch.comprssn GT 7 and scch.comprssn LT 15)) THEN begin
       scc_missing_blocks,image,fits_hdr,nmissing,missn,misslist,nblocks
       fxaddpar,fits_hdr,'NMISSING',nmissing,'out of '+trim(string(nblocks))+' blocks'
       scch.nmissing=nmissing
       fxaddpar,fits_hdr,'MISSLIST',strmid(misslist,0,60),'base34 format=(a2); use bntoint(x,34)'
    endif
    help,nmissing,missn,misslist,nblocks

    if not keyword_set(FAST) then wait,2
    ;--populate EUVI+COR1 pointing values
    
    help,sdspath
    time0=systime(1)
    ; save values for SC attitude characterization
    crpix1=scch.crpix1
    crpix2=scch.crpix2
    crval1=scch.crval1
    crval2=scch.crval2
    crval1a=scch.crval1a
    crval2a=scch.crval2a
    xcen=scch.xcen
    ycen=scch.ycen
    scroll=scch.sc_roll
    IF (scch.detector EQ 'EUVI') THEN BEGIN
    	euvi_point,scch,ERROR=gterr
	crp1c=' before GT was '+trim(crpix1)
	print,'CRPIX1= '+trim(scch.CRPIX1)+', before GT was '+trim(CRPIX1)
	printf,lulog,scch.date_obs+',CRPIX1=,'+trim(scch.CRPIX1)+',before GT was,'+trim(CRPIX1)
	crp2c=' before GT was '+trim(crpix2)
	print,'CRPIX2= '+trim(scch.CRPIX2)+', before GT was '+trim(CRPIX2)
	printf,lulog,scch.date_obs+',CRPIX2=,'+trim(scch.CRPIX2)+',before GT was,'+trim(CRPIX2)
	crv1c=''
	crv2c=''
	crv1ac=' before GT was '+trim(crval1a)
	print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(crval1A)
	printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(crval1A)
	crv2ac=' before GT was '+trim(crval2a)
	print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(crval2A)
	printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(crval2A)
    ENDIF
    IF (scch.detector EQ 'COR1') THEN BEGIN
    	cor1_point,scch,dir=sdspath,ERROR=gterr
	printf,lulog,'GT tlm dir='+sdspath
	crv1c=''
	print,' CRVAL1='+trim(scch.crval1)+', before GT was '+trim(crval1)
	printf,lulog,scch.date_obs+',CRVAL1=,'+trim(scch.crval1)+',before GT was,'+trim(crval1)
	crv2c=''
	print,' CRVAL2='+trim(scch.crval2)+', before GT was '+trim(crval2)
	printf,lulog,scch.date_obs+',CRVAL2=,'+trim(scch.crval2)+',before GT was,'+trim(crval2)
	crv1ac=''
	print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(crval1A)
	printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(crval1A)
	crv2ac=''
	print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(crval2A)
	printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(crval2A)
	crp1c=''
	crp2c=''
	fxaddpar,fits_hdr,'SC_YAW',scch.sc_yaw,'arcsec from GT'
	fxaddpar,fits_hdr,'SC_PITCH',scch.sc_pitch,'arcsec from GT'
	fxaddpar,fits_hdr,'SC_YAWA',scch.sc_yawa,'degrees from GT'
	fxaddpar,fits_hdr,'SC_PITA',scch.sc_pita,'degrees from GT'
    ENDIF
    IF (scch.detector EQ 'COR2') THEN BEGIN
    	cor2_point,scch,dir=sdspath, NOSPICE=no_spice,ERROR=gterr
	printf,lulog,'GT tlm dir='+sdspath
	crv1c=''
	print,' CRVAL1='+trim(scch.crval1)+', before GT was '+trim(crval1)
	printf,lulog,scch.date_obs+',CRVAL1=,'+trim(scch.crval1)+',before GT was,'+trim(crval1)
	crv2c=''
	print,' CRVAL2='+trim(scch.crval2)+', before GT was '+trim(crval2)
	printf,lulog,scch.date_obs+',CRVAL2=,'+trim(scch.crval2)+',before GT was,'+trim(crval2)
	crv1ac=''
	print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(crval1A)
	printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(crval1A)
	crv2ac=''
	print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(crval2A)
	printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(crval2A)
	crp1c=''
	crp2c=''
	fxaddpar,fits_hdr,'SC_YAW',scch.sc_yaw,'arcsec from GT'
	fxaddpar,fits_hdr,'SC_PITCH',scch.sc_pitch,'arcsec from GT'
	fxaddpar,fits_hdr,'SC_YAWA',scch.sc_yawa,'degrees from GT'
	fxaddpar,fits_hdr,'SC_PITA',scch.sc_pita,'degrees from GT'
    ENDIF
    IF (scch.detector EQ 'EUVI') or (scch.detector EQ 'COR1') or (scch.detector EQ 'COR2') THEN BEGIN
	pointingspicetime=systime(1)-time0
	help,pointingspicetime
	printf,lulog,'pointing SPICE took'+string(pointingspicetime)
	fxaddpar,fits_hdr,'VERSION',scch.version    ; @4/6/2012 only changes in euvi_point
	fxaddpar,fits_hdr,'CRPIX1',scch.crpix1,crp1c
	fxaddpar,fits_hdr,'CRPIX2',scch.crpix2,crp2c
	fxaddpar,fits_hdr,'CRVAL1',scch.crval1,crv1c
	fxaddpar,fits_hdr,'CRVAL2',scch.crval2,crv2c
	fxaddpar,fits_hdr,'CDELT1',scch.cdelt1
	fxaddpar,fits_hdr,'CDELT2',scch.cdelt2
	fxaddpar,fits_hdr,'XCEN',scch.xcen
	fxaddpar,fits_hdr,'YCEN',scch.ycen
	fxaddpar,fits_hdr,'CRPIX1A',scch.crpix1a
	fxaddpar,fits_hdr,'CRPIX2A',scch.crpix2a
	fxaddpar,fits_hdr,'CRVAL1A',scch.crval1a
	fxaddpar,fits_hdr,'CRVAL2A',scch.crval2a
	fxaddpar,fits_hdr,'CDELT1A',scch.cdelt1a
	fxaddpar,fits_hdr,'CDELT2A',scch.cdelt2a
	fxaddpar,fits_hdr,'ATT_FILE',scch.att_file
	fxaddpar,fits_hdr,'CROTA',scch.crota
	fxaddpar,fits_hdr,'PC1_1',scch.pc1_1
	fxaddpar,fits_hdr,'PC1_2',scch.pc1_2
	fxaddpar,fits_hdr,'PC2_1',scch.pc2_1
	fxaddpar,fits_hdr,'PC2_2',scch.pc2_2
	fxaddpar,fits_hdr,'PC1_1A',scch.pc1_1a
	fxaddpar,fits_hdr,'PC1_2A',scch.pc1_2a
	fxaddpar,fits_hdr,'PC2_1A',scch.pc2_1a
	fxaddpar,fits_hdr,'PC2_2A',scch.pc2_2a
	fxaddpar,fits_hdr,'INS_X0',scch.ins_x0,' arcsec'
	fxaddpar,fits_hdr,'INS_Y0',scch.ins_y0,' arcsec'
	fxaddpar,fits_hdr,'INS_R0',scch.ins_r0,' deg'
	ind=0
	REPEAT BEGIN
	    hinfo=scch.history[ind]
	    ind=ind+1 
	ENDREP UNTIL strpos(hinfo,'_point') GE 0 OR ind GT 19
	fxaddpar,fits_hdr,'HISTORY',hinfo
        if not keyword_set(FAST) then wait,2
    ENDIF
    ENDELSE	; End Process Normal image

; ++++ Write FITS file only if not seq a sequence header and update with header shell script ++++

    ;filenameroot=strmid(scch.filename,0,21)
    ;IF basehdr.eheadertypeflag GT 1 THEN BEGIN	
    ;	outsavfile=concat_dir(outdir,filenameroot+".sav")
    ;	save,filename=outsavfile,ext_hdr
    ;ENDIF
    
    ;--Test telimgdir and create outdir 
    
   link='none'
    wasbad=0
    IF ~file_exist(outdir) THEN BEGIN
    	telimg=strupcase(teldir)+'_'+strupcase(img_dir)
	telimgdir=getenv(telimg)
    	IF strmid(telimgdir,0,25) NE strmid(destdir,0,25) and (islz) and ~keyword_set(TESTLZ) THEN BEGIN
    	    ; create symbolic link in directory pointed to in $secchi
	    link=outdir
	    outdir =filepath( yyyymmdd,ROOT=telimgdir)
	ENDIF
    	  spawn,'mkdir -p '+outdir,/sh
          spawn,'ls -d '+outdir,lnck,/sh
	  if outdir ne lnck then begin
             	IF ~keyword_set(NOEVENT_NOTIFY)THEN BEGIN
	             openw,evlun,'errmsg.txt',/get_lun
	             printf,evlun,'******* Directory Error *******'
	             printf,evlun,''
	             printf,evlun,'pipeline=',string(pipeline,'(a8)')
	             printf,evlun,outdir+' directory not created'
	             printf,evlun,''
	             close,evlun
	             free_lun,evlun
	             print,''
	             spawn,'cat errmsg.txt',/sh
	             wait,errwait
	             ;maillist='nathan.rich@nrl.navy.mil lynn.simpson.ctr@nrl.navy.mil'
	             spawn,'mailx -s SECCHI-'+aorb+'_'+pipeline+'_DIRERR_'+yyyymmdd+' '+maillist+' < errmsg.txt',/sh
            	     wait,30
	       ENDIF
	  endif 
    	  IF link NE 'none' THEN BEGIN
            spawn,'ls -d '+outdir,lnck,/sh
	    while link eq lnck or lnck EQ '' do begin
	    	wasbad=1
		message,'Error in telimgdir.',/info
		wait,50
    	    	spawn,'mkdir -p '+outdir,/sh
		wait,10
    	    	spawn,'ls -d '+outdir,lnck,/sh
	    endwhile

	    ; Bad things happen if link is created inside yyyymmdd directory!
            spawn,'ls -ld '+link,lnck,/sh
            IF strpos(lnck,link) eq -1 and strpos(lnck,outdir) eq -1 THEN begin 
               cmd='ln -s '+outdir+' '+link
	       print,cmd
	       spawn,cmd,/sh
	       openw,1,'~/bin/'+telimg+'_'+aorb+'_links.sh',/append
	       printf,1,cmd
	       close,1
            ENDIF
            spawn,'ls -d '+outdir+'/'+yyyymmdd,lnck,/sh
	    if outdir+'/'+yyyymmdd eq lnck then begin
             	IF ~keyword_set(NOEVENT_NOTIFY)THEN BEGIN
	             openw,evlun,'errmsg.txt',/get_lun
	             printf,evlun,'******* link Error *******'
	             printf,evlun,''
	             printf,evlun,'pipeline=',string(pipeline,'(a8)')
	             printf,evlun,outdir+'/'+yyyymmdd+' link created'
	             printf,evlun,'deleting '+outdir+'/'+yyyymmdd+' link'
	             printf,evlun,''
	             close,evlun
	             free_lun,evlun
	             print,''
	             spawn,'cat errmsg.txt',/sh
	             ;maillist='nathan.rich@nrl.navy.mil lynn.simpson.ctr@nrl.navy.mil'
	             spawn,'mailx -s SECCHI-'+aorb+pipeline+'_LINKERR_'+yyyymmdd+' '+maillist+' < errmsg.txt',/sh
	             wait,errwait
	       ENDIF
              spawn,'/bin/rm -f '+outdir+'/'+yyyymmdd
	    endif 
	  ENDIF
	IF ~keyword_set(NOEVENT_NOTIFY) and (wasbad) THEN BEGIN
	     openw,evlun,'errmsg.txt',/get_lun
	     printf,evlun,'******* Directory Error *******'
	     printf,evlun,''
	     printf,evlun,'pipeline=',string(pipeline,'(a8)')
	     printf,evlun,outdir+' ok'
	     printf,evlun,''
	     close,evlun
	     free_lun,evlun
	     print,''
	     spawn,'cat errmsg.txt',/sh
	     wait,errwait
	     ;maillist='nathan.rich@nrl.navy.mil lynn.simpson.ctr@nrl.navy.mil'
	     spawn,'mailx -s SECCHI-'+aorb+'_'+pipeline+'_DIRERR_'+yyyymmdd+' '+maillist+' < errmsg.txt',/sh
             wait,30
	ENDIF

    ENDIF
 
    ;--End derive and create outdir

    outfile = concat_dir(outdir,scch.filename)    


;    if (~keyword_set(HDR_ONLY) and (lowhilow(0) gt -1) or (lowhihi(0) gt -1) and SpWximg(0) eq -1) then begin
;changed for side lobe spw
    if (SpWximg(0) ge 0 and scch.ip_prog0 eq 41) then nothilowspw=1 else nothilowspw=0
    if (~keyword_set(HDR_ONLY) and (lowhilow(0) gt -1) or (lowhihi(0) gt -1) and (nothilowspw eq 0)) then begin
        tmpdir=filepath( yyyymmdd,ROOT=seb_img+'/hilow' )
        tmpfile=fileorig
        strput,tmpfile,'-',strpos(fileorig,'.')
        tmpfile=tmpfile+'.fts'
         outfile=concat_dir(tmpdir,tmpfile)
        spawn,'mkdir -p '+tmpdir,/sh
    endif ELSE $; ++++ fill image statistics in header
    if (max(image) gt 0) then reduce_statistics,image,fits_hdr ;,SATMAX=satmax, SATMIN=satminbegin

    IF (max(image) gt 0) THEN BEGIN
    	IF strmid(scch.detector,0,2) EQ 'HI' THEN ccdsat=14000. ELSE ccdsat=15000.
    	dsatval = long(ccdsat * fxpar(fits_hdr,'N_IMAGES') * (2^(scch.IPSUM - 1))^2)
    	IF strmid(scch.detector,0,2) NE 'HI' THEN dsatval = dsatval<60000
    	it=where(image GE dsatval, nsat)
    	fxaddpar,fits_hdr,'DSATVAL',dsatval
    	fxaddpar,fits_hdr,'DATASAT',nsat
    	help,dsatval,nsat
    
    	printf,lulog,scch.filename+'  DSATVAL='+string(dsatval)+', DATASAT='+string(nsat)
    ENDIF

    
    ;++++ Write FITS file
    print,'Writing '+outfile
    IF not keyword_set(FAST)THEN wait,2
    
    printf,lulog,'Writing '+outfile
    ; This stuff looks unnecessary -- we *should* just be able to to
    ; writefits,outfile,image,khdr  -- but this produces junk images.
    ; There's a problem with the initial fits header -- I've had to mod it so
    ; that the initial writefits header starts with a blank line.
    ; Then for whatever reason we have to write the file, read it back, and
    ; rewrite it using the originak data array and the *new* fits header. 
    ; Bizarre...
;    fhdr=['',fits_hdr]
    ;fhdr[1:105]=fits_hdr[0:104]
    ;fhdr[0]=''
;    writefits,outfile,image,fhdr

    while not(file_exist(outdir)) do begin
    	print,'Waiting 2 sec before retrying ',outdir
	wait,2
    endwhile

    if isHISAM eq 1 then begin    ;HISAM images are written tough out the day by merging the the one colum image files added to database but not summary
        scch.OBS_PROG='HISAM'
        scch.CTYPE1='TIME'
        scch.CUNIT1='s'
        fxaddpar,fits_hdr,'OBS_PROG',scch.OBS_PROG
        fxaddpar,fits_hdr,'CTYPE1',scch.CTYPE1
        fxaddpar,fits__hdr,'CUNIT1',scch.CUNIT1
        tmpfile=strmid(outfile,0,strpos(outfile,'.fts')-12)+'*'+strmid(outfile,strpos(outfile,'.fts')-6,strlen(outfile))
        hisamfile=file_search(tmpfile)
        hisamfile=hisamfile(0)
; if date obs gt then current start a new fits file will be merge in sort_hisams incase file are out of order to preserve date_cmd and date_obs of first image
       if hisamfile ne '' and long(strmid(hisamfile,strpos(hisamfile,'.fts')-12,6)) le long(strmid(outfile,strpos(outfile,'.fts')-12,6)) then begin
          img=sccreadfits(hisamfile,scchsam)
          SAMseqtbl=string(readfits(hisamfile,/EXTEN))
          SAMseqtbl=[SAMseqtbl,'']
	  scchsam.naxis1=scchsam.naxis1+scch.naxis1
          hisam_hdr = struct2fitshead(scchsam,/allow_crota,/dateunderscore)
          img=[img,image]
       endif else begin
           img=image & hisam_hdr=fits_hdr
           SAMseqtbl=[''] & scchsam=scch
           hisamfile=outfile
	endelse
        if datatype(exttblhdr) eq 'UND' then exttblhdr = def_secchi_ext_hdr()
        fxaddpar,exttblhdr,'NAXIS2',scchsam.naxis1
        fxaddpar,hisam_hdr,'EXTEND','T'
        fxaddpar,hisam_hdr,'N_IMAGES',scchsam.naxis1
        basehdr.actualexptime=anytim2tai(utc2str(tai2utc(basehdr.actualexptime, /nocorrect),/noz))
	make_Scc_ext_tbl,SAMseqtbl,anytim2tai(scchsam.date_obs),basehdr,nom_hdr,scchsam.naxis1-1,Fast=fast,dir=sdspath
        writefits,hisamfile,img,hisam_hdr
    	writefits,hisamfile,byte(SAMseqtbl),exttblhdr,/append
    	goto, writedb
    endif  

    IF datatype(image) EQ 'UIN' THEN fxaddpar,fits_hdr,'BLANK',-32768,' FITS standard for UINT'
    writefits,outfile,image,fits_hdr
    
;    d=readfits(outfile,khdr,/noscale)
;    writefits,outfile,image,khdr

    ;End bizarre...
    ;stop
    nsh=0
    IF (isrt) or (ispb) or keyword_set(FIND_HDR_SCRIPT) THEN BEGIN
    	scirootx=sciroot
    	strput,scirootx,'?',4
    	print,'Searching for ',scirootx+'_'+scisfx+'*.sh'
    	shfiles = findfile(getenv_slash('SCRIPT_DIR')+scirootx+'_'+scisfx+'*.sh',COUNT=nsh)
    ENDIF
    IF nsh GT 0 THEN BEGIN
    	shfile=shfiles[0] 
    	;spawn,'chmod g+x '+shfile,/sh
	command = shfile+' '+outfile
	;stop
	print,command
	printf,lulog,command
    	spawn,command,/sh,shfileresult
	print,shfileresult
	FOR i=0,n_elements(shfileresult)-1 DO printf,lulog,shfileresult[i]
	command='mv '+shfile+' '+sci_attic+yymmdd+'/'
	print,command
	printf,lulog,command
    	spawn,command,/sh
	IF (not(keyword_set(FAST))) THEN print,'Waiting 3'
        IF (not(keyword_set(FAST))) THEN wait, 3
	
	; read in header with values from .sh file
	fits_hdr=headfits(outfile)
	; If values from .sh file begin with "Default", replace with structure values
	scctags=tag_names(scch)
	tblfiles=['WAVEFILE','READFILE','SETUPTBL','EXPOSTBL','MASK_TBL','IP_TBL']
	FOR i=0,5 DO BEGIN 
	    indx=where(scctags EQ tblfiles[i])
	    stcval=scch.(indx)
	    ;help,stcval
	    print,tblfiles[i],'=',fxpar(fits_hdr,tblfiles[i])
	    IF strmid(fxpar(fits_hdr,tblfiles[i]),0,7) EQ 'Default' THEN BEGIN
	    	print,'Replacing ',tblfiles[i],' with ',stcval
	    	fxhmodify,outfile,tblfiles[i],stcval
	    ENDIF
	ENDFOR
	
    ENDIF ELSE BEGIN
    	print,'No header script file found.'
	print,'Not changing OBS_PROG.'
	;fxhmodify,outfile,'OBS_PROG','schedule'
	;scch.obs_prog='schedule'
        ;command = 'modhead '+outfile+' OBS_PROG schedule'
        ;print,command
	;printf,lulog,'fxhmodify '+outfile+' OBS_PROG schedule'
    	;spawn,command,/sh,shfileresult
	;print,shfileresult
	;FOR i=0,n_elements(shfileresult)-1 DO printf,lulog,shfileresult[i]
        IF (not(keyword_set(FAST))) THEN wait, 3
    ENDELSE
;   ENDIF

;; append ASCII table if end of seq
    IF ~keyword_set(NOEXTHDR) and datatype(hiseqtbl) ne 'UND' THEN if(HI1seqimg(0) gt -1 or HI2seqimg(0) gt -1 and SpWximg(0) eq -1 and lowhihi(0) eq -1) then $
    	writefits,outfile,byte(HIseqtbl),exttblhdr,/append
    IF (~keyword_set(NOEXTHDR) and  SpWximg(0) eq -1) THEN if (naddsi+nsubsi GT 0) then $
    	writefits,outfile,byte(SIseqtbl),exttblhdr,/append
    
;++++ Now we have all image info, time to clean up. ++++
    IF scch.comprssn EQ 43 THEN BEGIN	    ; gnd test image
            command='mv '+sciimg+' '+sci_attic+yymmdd+'/'
            print,command
            printf,lulog,command
            spawn,command,/sh
    ENDIF
; clear image save files after fits file is saved
    IF (~keyword_set(NOEXTHDR) and  SpWximg(0) eq -1) THEN if(relSI(0) gt -1)then begin ; reset header structures in released images save files
      SIbasehdr=-1 & SInom_hdr=-1
      for nrel=0,n_Elements(relSI)-1 do begin
        SIfile=concat_dir(getenv('SCC_LOG'),'SI'+string(basehdr.ipcmdlog(relSI(nrel))-71,'(i1.1)')+'_hdr.sav')
        save,filename=SIfile,SIbasehdr,SInom_hdr
      endfor
    endif          

mergelowhi:
outfile0=outfile

;     delflag is always set to zero in write_db_scripts.pro
IF keyword_set(DELETE_OLD) THEN delflag=1

;++++ Merge HI low+high
;if (ishihigh or ishilow) and SpWximg(0) eq -1) then secchi_hi_low_merge,outfile,outdir,/summary
; change for side lobe spw
if (SpWximg(0) ge 0 and scch.ip_prog0 eq 41) then nothilowspw=1 else nothilowspw=0
if (ishihigh or ishilow and nothilowspw eq 0) then secchi_hi_low_merge,outfile,outdir,/summary
; Note: outfile changes in secchi_hi_low_merge
if strpos(outfile,'hilow') GE 0 THEN goto, skipscifile

IF (scch.detector EQ 'HI1' or scch.detector EQ 'HI2') THEN BEGIN
    	nn=sccreadfits(outfile,scch,/nodata)
	fits_hdr=headfits(outfile)
	exthdr=mrdfits(outfile,1)
	print,scch.crpix1,scch.crpix2
	scch = hi_point_v2(scch,exthdr,dir=sdspath)
	crp1c=''
	crp2c=''
	crv1c=' degrees'
	print,' CRVAL1='+trim(scch.crval1)+', before GT was '+trim(crval1)
	printf,lulog,scch.date_obs+',CRVAL1=,'+trim(scch.crval1)+',before GT was,'+trim(crval1)
	crv2c=' degrees'
	print,' CRVAL2='+trim(scch.crval2)+', before GT was '+trim(crval2)
	printf,lulog,scch.date_obs+',CRVAL2=,'+trim(scch.crval2)+',before GT was,'+trim(crval2)
	crv1ac=' before GT was '+trim(crval1a)
	print,' CRVAL1A='+trim(scch.crval1A)+', before GT was '+trim(crval1A)
	printf,lulog,scch.date_obs+',CRVAL1A=,'+trim(scch.crval1A)+',before GT was,'+trim(crval1A)
	crv2ac=' before GT was '+trim(crval2a)
	print,' CRVAL2A='+trim(scch.crval2A)+', before GT was '+trim(crval2A)
	printf,lulog,scch.date_obs+',CRVAL2A=,'+trim(scch.crval2A)+',before GT was,'+trim(crval2A)
	printf,lulog,scch.date_obs+',XCEN=,'+trim(scch.xcen)+',before GT was,'+trim(xcen)
	printf,lulog,scch.date_obs+',YCEN=,'+trim(scch.ycen)+',before GT was,'+trim(ycen)
	scrollc=' before GT was '+trim(scroll)
	print,' SC_ROLL='+trim(scch.sc_roll)+','+trim(scrollc)
    	pointingspicetime=systime(1)-time0
	help,pointingspicetime

	printf,lulog,'pointing SPICE took'+string(pointingspicetime)
	fxhmodify,outfile,'CRPIX1',scch.crpix1,crp1c
	fxhmodify,outfile,'CRPIX2',scch.crpix2,crp2c
	fxhmodify,outfile,'CRVAL1',scch.crval1,crv1c
	fxhmodify,outfile,'CRVAL2',scch.crval2,crv2c
	fxhmodify,outfile,'CDELT1',scch.cdelt1
	fxhmodify,outfile,'CDELT2',scch.cdelt2
	fxhmodify,outfile,'XCEN',scch.xcen,' degrees'
	fxhmodify,outfile,'YCEN',scch.ycen,' degrees'
	fxhmodify,outfile,'CRPIX1A',scch.crpix1a
	fxhmodify,outfile,'CRPIX2A',scch.crpix2a
	fxhmodify,outfile,'CRVAL1A',scch.crval1a
	fxhmodify,outfile,'CRVAL2A',scch.crval2a
	fxhmodify,outfile,'CDELT1A',scch.cdelt1a
	fxhmodify,outfile,'CDELT2A',scch.cdelt2a
	fxhmodify,outfile,'ATT_FILE',scch.att_file
	fxhmodify,outfile,'CROTA',scch.crota
	fxhmodify,outfile,'PC1_1',scch.pc1_1
	fxhmodify,outfile,'PC1_2',scch.pc1_2
	fxhmodify,outfile,'PC2_1',scch.pc2_1
	fxhmodify,outfile,'PC2_2',scch.pc2_2
	fxhmodify,outfile,'PC1_1A',scch.pc1_1a
	fxhmodify,outfile,'PC1_2A',scch.pc1_2a
	fxhmodify,outfile,'PC2_1A',scch.pc2_1a
	fxhmodify,outfile,'PC2_2A',scch.pc2_2a
	fxhmodify,outfile,'SC_ROLL',scch.sc_roll,' degrees'
	fxhmodify,outfile,'SC_YAW',scch.sc_yaw,' degrees'
	fxhmodify,outfile,'SC_PITCH',scch.sc_pitch,' degrees'
	fxhmodify,outfile,'SC_ROLLA',scch.sc_rolla,' degrees'
	fxhmodify,outfile,'SC_YAWA',scch.sc_yawa,' degrees'
	fxhmodify,outfile,'SC_PITA',scch.sc_pita,' degrees'
	fxhmodify,outfile,'INS_X0',scch.ins_x0,' deg'
	fxhmodify,outfile,'INS_Y0',scch.ins_y0,' deg'
	fxhmodify,outfile,'INS_R0',scch.ins_r0,' deg'
	
	ind=0
	REPEAT BEGIN
	    hinfo=scch.history[ind]
	    ind=ind+1 
	ENDREP UNTIL strpos(hinfo,'_point') GE 0 OR ind GT 19
	fxhmodify,outfile,'HISTORY',hinfo
        if not keyword_set(FAST) then wait,2

ENDIF

;++++ Gzip file 
  IF (dogz) and (imsize[0] EQ 2) and (scch.comprssn NE 5) THEN BEGIN
    	IF file_exist(gzdestdir) THEN BEGIN
	    outdirz=filepath( yyyymmdd,ROOT=gzdestdir+'/'+aorblc,SUBD=SUBDir )
	    msg='Writing gzipped '+outfile+' to '+outdirz
	    print,msg
    	    spawn,'mkdir -p '+outdirz,/sh
    	    outfilez = concat_dir(outdirz,scch.filename)
	    spawn,'cp '+outfile+' '+outfilez
	    spawn,'gzip '+outfilez
	    printf,lulog,msg
	ENDIF ELSE BEGIN
	    print,'****** GZIP Destination does not exist; skipping! *******'
	    IF ~fast THEN wait,1
	ENDELSE
  ENDIF

;++++ Add image to monthly summary report
;  if ((lowhilow(0) gt -1) or (lowhihi(0) gt -1) and (SpWximg(0) eq -1)) then goto, mergelowhi
  if not keyword_set(nosummary) then begin
    	sfx='.'+strmid(scch.filename,18,2)
	sumfile= concat_dir(getenv('SCC_SUM'), 'scc'+strmid(scch.filename,20,1) $
	    	    	    	    	    	    +strmid(scch.filename,0,6) $
						    +'.'+img_dir+sfx)
	sccwritesummary,scch,sumfile
  endif ELSE delflag=1
  
;++++ if Summary file is not overwritten, then re-write rows in database (COMMON dbms)
;     delflag is always set to zero in write_db_scripts.pro

writedb:
IF keyword_set(DELETE_OLD) THEN delflag=1

; ++++ Generate database script
    ;IF ~(ishihigh or ishilow) THEN $
    ; write_db_scripts done in secchi_hi_low_merge.pro for HIGH BYTE IMAGE ONLY
    if (imsize[0] LT 2) or (isHISAM eq 1) then write_db_scripts,fits_hdr ELSE $
    write_db_scripts, outfile


skipscifile:
    IF not keyword_set(FAST) THEN BEGIN
        print,'Waiting 3....'
        wait,3
    ENDIF
    IF n_params() EQ 0 THEN BEGIN
    ;--If input list, then do not move fileorig
	command='mv '+sciroot+'.'+scisfx+'.meta* '+sci_attic+yymmdd+'/'
	print,command
	printf,lulog,command
	spawn,command,/sh

	command='mv '+infile+' '+sci_attic+yymmdd+'/'
	print,command
	printf,lulog,command
	spawn,command,/sh
    ENDIF
;    command='/bin/rm *.img'
;    print,command
;    spawn,command,/sh
;    command='/bin/rm *.dat *.hdr *.tlr '
    command='/bin/rm '+scifile+'.*'
    print,command
    spawn,command,/sh
    
    ; If no more images waiting, move remaining .sh files to attic
    IF (ndone EQ ng) THEN BEGIN
	spawn, 'ls -l *.sh', lsres,/sh,COUNT=nls
	; Also done for new day. Only .sh files from current day would remain.
	IF nls GT 0 THEN BEGIN
	    ;openw,55,concat_dir(getenv('SCC_LOG'),'extra_sh_files.log'),/append
	    ;for ii=0,nls-1 do printf,55,lsres[ii]
	    ;close,55
	    ;command='mv *.sh '+sci_attic+strmid(lastyyyymmdd,2,6)
	    ;print,command
	    for ii=0,nls-1 do print,lsres[ii]
	    print,'Waiting ',trim(errwait),' ...'
	    wait, errwait
	    ;spawn,command,/sh
	    ;printf,lulog,command
	    for ii=0,nls-1 do printf,lulog,lsres[ii]		
	ENDIF
    ENDIF

    close,lulog
    ;close,ludb 	; closed in write_db_script.pro
    free_lun,ludb
    ;free_lun,lulog    
    resetvars=0
    icerdiv2flag=0
    iserror=0
    help,waittime
ENDREP UNTIL not (keeplooking)
; Not returning to original directory
;close,lulog
;free_lun,lulog
end
