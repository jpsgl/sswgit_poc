pro make_Scc_ext_tbl,seqtbl,sttime,exbasehdr,exnom_hdr,placeinseq,extscch=extscch, _EXTRA=_extra
;+	
;$Id: make_scc_ext_tbl.pro,v 1.31 2017/01/03 20:31:40 secchia Exp $
;
; NAME:				make_Scc_ext_tbl
; PURPOSE:			fill a line of the sequence extended Header table for a Level 0.5 SECCHI Seq
; CATEGORY:			Administration, Pipeline
; CALLING SEQUENCE:		make_Scc_ext_tbl,seqtbl,sttime,exbasehdr,exexnom_hdr
; INPUTS:			seqtbl=name of table to fill  sttime=time a first image in sequence
; OPTIONAL INPUT PARAMETERS:	None
; KEYWORD PARAMETERS:		extscch used to input saved header instead of current header
;
; OUTPUTS:		seqtbl is returned with the new filled
;
; OPTIONAL OUTPUT PARAMETERS:
; COMMON BLOCKS:  scc_reduce
; SIDE EFFECTS:
; RESTRICTIONS:	output rows must match definition in def_secchi_ext_hdr.pro	 
; PROCEDURE:
;
; MODIFICATION HISTORY:		
;
; $Log: make_scc_ext_tbl.pro,v $
; Revision 1.31  2017/01/03 20:31:40  secchia
; removed stereo_spice_conic common block
;
; Revision 1.30  2017/01/03 20:25:21  secchia
; added load_sunspice common block
;
; Revision 1.29  2013/11/21 18:06:07  secchia
; add checks for islz before setting sdspath and looking for attitude info
;
; Revision 1.28  2013/11/14 16:15:14  secchia
; removed stop
;
; Revision 1.27  2013/11/14 00:25:58  nathan
; change how naxis of hdr-only is computed
;
; Revision 1.26  2011/11/25 20:19:15  secchib
; fix format of crval
;
; Revision 1.25  2010/07/07 15:22:12  mcnutt
; added keyword extscch for use with save area headers
;
; Revision 1.24  2010/07/07 11:51:48  mcnutt
; defines fileorig from basehdr if cor2
;
; Revision 1.23  2010/07/06 22:48:23  nathan
; add fileorig to row
;
; Revision 1.22  2009/06/02 18:51:51  mcnutt
; change deltatime to f8.2 for hisam
;
; Revision 1.21  2009/03/16 17:21:27  mcnutt
; defines polar= scch.polar before COR changes
;
; Revision 1.20  2009/03/06 17:06:38  mcnutt
; get pol position from nom_hdr
;
; Revision 1.19  2008/04/07 12:47:55  secchib
; removed stop for skipped image
;
; Revision 1.18  2008/04/01 16:10:12  mcnutt
; set naxis if not set for HI header only images remembers original header in scchorig
;
; Revision 1.17  2008/02/07 13:53:00  bewsher
; Changed call to hi_point_v2
;
; Revision 1.16  2007/12/13 16:39:21  nathan
; added 3 sig figs to EXPTIME and 1 digit to POLAR (Bug 268)
;
; Revision 1.15  2007/09/21 21:32:48  nathan
; add info printout
;
; Revision 1.14  2007/08/20 18:59:07  nathan
; Use obsrvtry not SCC_PLATFORM for AorB; add _extra to pointing calls for DIR in scc_sunvec
;
; Revision 1.13  2007/08/20 16:25:56  nathan
; if SCI set ap1 and detector from nom_hdr not fileorig
;
; Revision 1.12  2007/08/14 19:10:24  mcnutt
; added hi_point and cor1 point to file CRVAL1 and 2 and PC vals
;
; Revision 1.11  2007/04/09 21:29:57  nathan
; add _EXTRA so /FAST is passed to getsccpointing
;
; Revision 1.10  2007/04/05 14:55:10  mcnutt
; change RVAL format to allow for negative
;
; Revision 1.9  2007/04/03 18:28:31  mcnutt
; corrected call to getsccpointing.pro
;
; Revision 1.8  2007/03/30 13:15:03  mcnutt
; finished correcting BUG 76 added CRVAL1 and CRVAL2
;
; Revision 1.7  2007/02/12 14:58:28  mcnutt
; corrected ccdsum and ipsum comments
;
; Revision 1.6  2007/01/26 17:37:46  secchia
; defines enums if not defined
;
; Revision 1.5  2007/01/26 17:11:34  mcnutt
; added spice to extended header
;
; Revision 1.4  2006/09/11 21:09:52  mcnutt
; removed common block for header structures
;
; Revision 1.3  2006/07/25 16:41:37  mcnutt
; change cosmic format to i7
;
; Revision 1.2  2006/07/21 21:04:15  mcnutt
; removed imgcnt input use base hdr instead
;
; Revision 1.1  2006/07/21 18:30:13  mcnutt
; created to fill seq table called in secchi_reduce
;

COMMON scc_reduce; , ludb, lulog, basehdr, nom_hdr, ext_hdr, scch, leapsecs
common stereo_sunspice; common stereo_spice, def_ephem, ephem, attitude
;common stereo_spice_conic;, mu, maxdate, conic

EVENT='F' & evcount='' & evrow=9999 & evcol=9999
critevent=string(exbasehdr.CRITEVENT,'(z4)')
if(strmid(critevent,0,1) eq 'd' or  strmid(critevent,0,1) eq 'e')then begin
  EVENT='T'
  if(strmid(critevent,0,1) eq 'd')then begin 
     evcount='Event' 
     eventcal=exbasehdr.CRITEVENT-(53248+1999)
  endif else begin
     evcount='Big Ev'
     eventcal=exbasehdr.CRITEVENT-(57344+1999)
  endelse
  EVROW=eventcal/34
  EVCOL= eventcal - (34 * EVROW)
endif
IF scch.detector NE '' THEN detector=scch.detector ELSE BEGIN
    ap1=strmid(string(exbasehdr.filename),10,1)
    detectors=['COR1','COR2','HI1','HI2','EUVI']
    detector=detectors[ap1]
ENDELSE


IF (exnom_hdr.actualShutterPosition EQ 0) THEN shuttdir='F' else shuttdir='T'

date_clr_utc    =tai2utc(exnom_hdr.ACTUALCCDCLEARSTARTTIME, /nocorrect)
date_clr   =utc2str(date_clr_utc,/noz)
date_ro_utc	    =tai2utc(exnom_hdr.ACTUALIMAGERETRIEVESTARTTIME, /nocorrect)
date_ro    =utc2str(date_ro_utc,/noz)

f_or_t=['F','T']
ledclr=['N','R','B','P']

scchorig=scch

if keyword_set(extscch) then scch=extscch

IF detector EQ 'HI2' or detector EQ 'HI1' THEN  begin
   if scch.naxis1 eq 0 then scch.naxis1=2048/2^(scch.summed-1)
   if scch.naxis2 eq 0 then scch.naxis2=2048/2^(scch.summed-1)
   if scch.naxis eq 0 then scch.naxis=2
   if scch.naxis1 eq 1 and scch.naxis2 eq 2048 then scch.naxis1=2048 ;HISAM images
   scch = hi_point_v2(scch, _extra=_extra)
endif

IF detector EQ 'COR1' THEN  cor1_point,scch, _extra=_extra
;fileorig = scch.fileorig 
;polar =scch.polar
;degr = (exnom_hdr.cmdpolarposition) * 2.5
;IF scch.detector EQ 'COR2' or scch.detector EQ 'COR1' THEN BEGIN
;    IF (scch.detector EQ 'COR1' and scch.OBSRVTRY eq 'STEREO_A') THEN poloffset= -10. ELSE poloffset=0   ; assuming position 0 = 0 degrees
;    polar      =degr + poloffset
;    fileorig=string(exbasehdr.filename)
;ENDIF
IF (trim(scch.seb_prog) eq 'DOUBLE') then IF scch.detector EQ 'COR2' or scch.detector EQ 'COR1' THEN polar=1001

;--Check for correct retrieval of GT slew info
IF scch.sc_yaw EQ 0 and islz THEN BEGIN
    print,'ATT_FILE:',scch.att_file
    print,'SC_YAW:',scch.sc_yaw
    wait,10
ENDIF

;IF detector EQ 'COR2' THEN  begin    (cor2 values are filled in make_Scc_hdr)
;   if datatype(enums) EQ 'UND' THEN enums = def_scc_enums()
;   sccobsrvtry = enums.eplatformtypes[exnom_hdr.platformid]
;    date_obs_utc=tai2utc(exbasehdr.actualexptime, /nocorrect)
;    sccdate_obs=utc2str(date_obs_utc,/noz)
;    hcv=get_stereo_hpc_point(sccdate_obs,strmid(sccobsrvtry,7,1),_extra=_extra)
;    sccCROTA=hcv(2)
;    sccPC1_1=cos(sccCROTA*!DPI/180.d0)
;    sccPC1_2=-sin(sccCROTA*!DPI/180.d0) ;*(sccCDELT1/sccCDELT2) ;may be added later
 ;   sccPC2_1=sin(sccCROTA*!DPI/180.d0) ;*(sccCDELT1/sccCDELT2)
 ;   sccPC2_2=cos(sccCROTA*!DPI/180.d0)
 ;   yprhpc=getsccpointing(enums.ehkpinstrument[exnom_hdr.telescopeid],strmid(sccobsrvtry,7,1),sccdate_obs, TOLERANCE=60, YPRSC=scp, _EXTRA=_extra)
 ;   hpccmnt='Pointing info from SPICE, 1min. resolution'
 ;   print,'YPR hpc = ',yprhpc
;    sccCRVAL1	=yprhpc[0]      
;    sccCRVAL2	=yprhpc[1]
;endif

seqtbl(placeinseq)=string(exbasehdr.actualexptime-sttime,'(f8.2)')+' ' $  ;1 DELTATIME
          +string(scch.exptime,'(f10.6)')+' ' $     	    	    	;2 EXPTIME
          +string((exbasehdr.sumrow+exbasehdr.sumcol)/2,'(i2)')+' ' $   ;3 CCDSUM
	  +string((exbasehdr.sebxsum+exbasehdr.sebysum)/2,'(i2)')+' ' $ ;4 IPSUM
; Revision 1.116  2006/12/22 22:38:07  nathan
    	  +string(scch.polar,'(f6.1)')+' ' $       	    	        ;5 CMDed POL POS
    	  +string(shuttdir,'(a1)')+' ' $         	    	    	;6 SHUTTER used T or F
    	  +string(exbasehdr.actualpolarposition,'(i3)')+' ' $      	;7 actual encoder position of polarizer
    	  +string(ledclr(exnom_hdr.cmdledmode),'(a1)')+' ' $          	;8 LED COLOR
    	  +string(exnom_hdr.ACTUALDOORPOSITION,'(a1)')+' ' $            ;9 DOOR
    	  +string(exbasehdr.IMGCTR,'(i5)')+' ' $        	    	;10 IMGCTR
    	  +string(placeinseq,'(i4)')+' ' $        	    	    	;11 IMGSEQ
    	  +string(event,'(a1)')+' ' $         	    	    	    	;12 FLARE
    	  +string(evcount,'(a6)')+' ' $           	    	    	;13 FCOUNT
    	  +string(evrow,'(i4)')+' ' $           	    	    	;14 FROW
    	  +string(evcol,'(i4)')+' ' $           	    	    	;15 FCOL
    	  +string(date_clr,'(a23)')+' ' $   	    	   	    	;16 Time of start of clear operation
    	  +string(date_ro,'(a23)')+' ' $  	    	   	    	;17 Time of start of readout
    	  +string(scch.PC1_1,'(f9.6)')+' ' $  	    	   	    	;18 PC1_1
    	  +string(scch.PC1_2,'(f9.6)')+' ' $  	    	   	    	;19 PC1_2
    	  +string(scch.PC2_1,'(f9.6)')+' ' $  	    	   	    	;20 PC2_1
    	  +string(scch.PC2_2,'(f9.6)')+' ' $  	    	   	    	;21 PC2_2
    	  +string(scch.CRVAL1,'(f9.3)')+' ' $  	    	   	    	;22 CRVAL1
    	  +string(scch.CRVAL2,'(f9.3)')+' ' $  	    	   	    	;23 CRVAL2
    	  +string(0,'(i7)') +' ' $         	    	    	    	;24 COSMIC
    	  +string(scch.fileorig,'(a12)')              	    	        ;25 FILEORIG

scch=scchorig

print,'New seqtbl line=',seqtbl[placeinseq]
;IF exbasehdr.actualexptime-sttime LT 0 then stop
end
