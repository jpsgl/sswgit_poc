pro secchi_reduce, infile, WAITRATE=waitrate, NOWAIT=nowait, NOTELDIR=noteldir, $
    SCIP=scip
;+
; $Id: template.pro,v 1.1 2005/03/11 21:44:21 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : secchi_reduce
;               
; Purpose   : scan input directory $SEB_IMG for 8.3 files and do pipeline processing
;               
; Explanation: 
;               
; Use       : IDL> secchi_reduce [, infile]
;    
; Inputs    : optional infile: filename to process
;               
; Outputs   : FITS files in $SECCHI, log files in $SEB_IMG/log
;
; Keywords  : WAITRATE, NOWAIT, NOTELDIR, SCIP
;
;   WAITRATE    Set to number of seconds to wait before checking for new 
;               science files.
;   NOWAIT	    If set, don't wait for new files after done
;   NOTELDIR    Do not put day directories in telescope subdirs
;   SCIP 	    ='A' or 'B'; overrides observatory designation in 
;               image header
;               
; Calls from LASCO : getenv_slash.pro
;
; Common    : SCC_REDUCE
;               
; Restrictions: environment defined by sourcing sccimg[ab].env
;               
; Side effects: 
;               
; Category    : Pipeline (see http://durance.oamp.fr/lasco/fichiers/soft_categories.html)
;               
; Prev. Hist. : None.
;
; Written     : Nathan Rich, NRL/I2, Sep 2004
;               
; $Log: template.pro,v $
; Revision 1.1  2005/03/11 21:44:21  nathan
; template for IDL pros
;
;-            

end
