;+
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_TOTBPREP
;
; Purpose     :	Process filenames found through COR1_TOTBSERIES
;
; Category    :	STEREO, SECCHI, COR1, Analysis
;
; Explanation : This routine applies SECCHI_PREP to a list of filenames found
;               through the routine COR1_TOTBSERIES to derive total brightness.
;               Polarization sequences are processed with the /POLARIZ_ON
;               keyword.
;
; Syntax      :	COR1_TOTBPREP, FILENAME, HEADER, IMAGE
;
; Examples    :	CAT = COR1_PBSERIES( '8-Apr-2009', 'Behind' )
;               COR1_TOTBPREP, CAT.FILENAME, HEADER, IMAGE
;
; Inputs      :	FILENAME = Array containing the list of filenames to process,
;                          from COR1_TOTBSERIES.  This array can either have
;                          the dimensions [3,N] for one spacecraft, or [3,N,2]
;                          for two spacecraft.
;
; Opt. Inputs :	None.
;
; Outputs     :	HEADER  = The FITS header index structures.
;
;               IMAGE   = Array containing the images.
;
; Opt. Outputs:	None.
;
; Keywords    :	SSR     = If passed, has the following meaning:
;
;                               SSR=1   Synoptic data (default)
;                               SSR=2   Special event data
;                               SSR=3   Synoptic + Special event
;                               SSR=7   Space weather
;
;               OUTSIZE = Size of output images.  If not passed, then the size
;                         is based on the total brightness images.
;
;               ADJUST_COR2 = If set, then adjust COR2 double exposures to
;                             match pB series images.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then the input string is
;                         returned unchanged.  In order to use this feature,
;                         ERRMSG must be defined first, e.g.
;
;                               ERRMSG = ''
;                               Result = COR1_PBSERIES( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Along with any keywords to SECCHI_PREP or the other routines.
;
; Calls       :	ANYTIM2UTC, BREAK_FILE, MATCH, SCC_READ_SUMMARY, SECCHI_PREP,
;               COR1_PBSERIES
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 1-Apr-2010, William Thompson, GSFC
;               Version 2, 13-Jul-2011, WTT, fix error when only totb files
;               Version 3, 14-Jul-2011, WTT, correct /ADJUST_COR2 behavior
;               Version 4, 17-Nov-2011, WTT, fix another only totb error
;
; Contact     :	WTHOMPSON
;-
;
pro cor1_totbprep, filename, header, image, ssr=k_ssr, outsize=k_outsize, $
                   errmsg=errmsg, adjust_cor2=adjust_cor2, _extra=_extra
;
on_error, 2
;
;  Make sure that FILENAME is defined correctly.
;
if datatype(FILENAME) ne 'STR' then begin
    message = 'FILENAME must be a string array'
    goto, handle_error
endif
sz = size(filename)
if (sz[0] eq 0) or (sz[1] ne 3) then begin
    message = 'FILENAME must have 3 as the first dimension'
    goto, handle_error
endif
if (sz[0] eq 3) and (sz[3] ne 2) then begin
    message = 'The third dimension of FILENAME, if present, must be 2'
    goto, handle_error
endif
if sz[0] gt 3 then begin
    message = 'FILENAME has too many dimensions'
    goto, handle_error
endif
;
;  If combined data, then first temporarily reformat the list of filenames.
;
if sz[0] eq 3 then begin
    combined = 1
    nimages= sz[2]
    filename = reform(filename, 3, nimages*2L, /overwrite)
endif
;
;  Define various keywords.
;
if n_elements(k_outsize) eq 1 then outsize = k_outsize
;
;  Process the total brightness images.
;
wtotb = where(filename[0,*] eq filename[1,*], ntotb, complement=wpb, $
             ncomplement=npb)
if ntotb eq 0 then goto, process_pb
secchi_prep, sccfindfits(filename[0,wtotb]), hdr_totb, img_totb, $
  outsize=outsize, _extra=_extra
sz = size(img_totb)
outsize = sz[1]
;
;  Correct COR2 double exposure images for the non-linearity effect.
;
if keyword_set(adjust_cor2) then begin
    a0 = 1.04418
    a1 = -0.00645004
    for i = 0,n_elements(hdr_totb)-1 do begin
        temp = img_totb[*,*,i]
        if hdr_totb[i].ipsum ne 1 then begin
            scl = 4^(hdr_totb[i].ipsum-1)
            temp = temp / (scl > 1)
        endif
        scl = (a0 + a1*alog(hdr_totb[i].exptime)) + a1*alog(temp>1)
        img_totb[*,*,i] = img_totb[*,*,i] / (scl > 1)
    endfor
endif
;
;  Next, process the pbseries data.
;
process_pb:
if npb gt 0 then begin
    secchi_prep, sccfindfits(filename[*,wpb]), hdr_pb, img_pb, /polariz_on, $
      outsize=outsize, _extra=_extra
endif
;
;  Determine the total number of results, and handle the simple cases.
;
count = ntotb + npb
if count eq 0 then goto, no_results
if ntotb eq 0 then begin
    header = hdr_pb
    image = img_pb
    goto, reformat
end else if npb eq 0 then begin
    header = hdr_totb
    image = img_totb
    goto, reformat
endif
;
;  Create arrays to hold the output.
;
sz = size(img_totb)
dim = [sz[1:2], count]
image = make_array(dimension=dim, type=sz[sz[0]+1])
image[*,*,wtotb] = img_totb
image[*,*,wpb]   = img_pb
header = replicate(hdr_totb[0], dim[2:*])
header[wtotb] = hdr_totb
header[wpb]   = hdr_pb
;
;  If combined, then reformat the arrays.
;
reformat:
if keyword_set(combined) then begin
    count = count / 2
    filename = reform(filename, 3, count, 2, /overwrite)
    sz = size(image)
    dim = [sz[1:2], count, 2]
    image = reform(image, dim, /overwrite)
    header = reform(header, count, 2, /overwrite)
endif
return
;
;  Error point for no results.
;
no_results:
message = 'No results found'
;
;  Point for general error handling.
;
handle_error:
if keyword_set(combined) then $
  filename = reform(filename, 3, nimages, 2, /overwrite)
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = message
count = 0
return
end
