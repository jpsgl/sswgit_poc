;+
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_TOTBSERIES()
;
; Purpose     :	Return combined list of COR1 series and totB images
;
; Category    :	STEREO, SECCHI, COR1, Analysis
;
; Explanation :	This routine uses SCC_READ_SUMMARY and COR1_PBSERIES to combine
;               polarization sequences with total brightness images calculated
;               onboard to form a unified sequence of total brightness images.
;               Because the output list will include both total brightness
;               images and polarization sequences, use COR1_TOTBPREP instead of
;               SECCHI_PREP to process the images.  The routine COR1_TOTALB
;               combines the actions of COR1_TOTBSERIES and COR1_TOTBPREP into
;               a single call.
;
; Syntax      :	Result = COR1_TOTBSERIES( DATE, SPACECRAFT )
;
; Examples    :	CAT = COR1_PBSERIES( '8-Apr-2009', 'Behind' )
;               COR1_TOTBPREP, CAT[*,0].FILENAME, HEADER, IMAGE
;
;               DATE = ['7-Apr-2009 08:30', '8-Apr-2007 15:00']
;               CAT = COR1_TOTBSERIES( DATE, 'Combined' )
;
; Inputs      :	DATE    = Either the date of observation, or a beginning and
;                         end time to search on.  When a single date is passed,
;                         the entire day is returned.  Otherwise, only the
;                         portion between the beginning and end dates are
;                         returned.
;
;               SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;                               'STEREO_Ahead'  'STEREO_Behind'
;
;                            Alternatively, the word "Combined" (or "C") tells
;                            the routine to look for simultaneous sequences in
;                            both spacecraft.
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is a structure array containing
;               various information about the files.  See SCC_READ_SUMMARY for
;               more details.  The FILENAME tag will contain a complete path to
;               the file.
;
;               The dimensions of the result will be (3,N), where the first
;               dimension represents the three polarization angles, and the
;               second dimension is the individual sequences.  Total B images
;               will be triplicated.  If the spacecraft is specified as
;               "Combined", then the result is returned with the dimensions
;               (3,N,2), where the last dimension represents the two spacecraft
;               "A" and "B".
;
;               If no results are found, or an error occurs, then the single
;               value of -1 is returned, rather than a structure.
;
; Opt. Outputs:	None.
;
; Keywords    :	SSR     = If passed, has the following meaning:
;
;                               SSR=1   Synoptic data (default)
;                               SSR=2   Special event data
;                               SSR=3   Synoptic + Special event
;                               SSR=7   Space weather
;
;               COUNT   = Returns the number of sequences found.
;
;               VALID   = If set, then only series with no missing blocks
;                         (NMISSING LE 0) are returned.
;
;               SOURCE  = Telemetry source, either "rt" (realtime), "pb"
;                         (playback), or "lz" (level-zero, default).
;
;               COR2    = If set, search for COR2 images instead of COR1.
;
;               EXPMAX  = Maximum exposure time used to filter out unusual COR2
;                         images that are not part of the normal synoptic
;                         sequence.  Default is 20 (seconds).  Use this keyword
;                         for dates with long exposure campaigns.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then the input string is
;                         returned unchanged.  In order to use this feature,
;                         ERRMSG must be defined first, e.g.
;
;                               ERRMSG = ''
;                               Result = COR1_PBSERIES( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Along with any keywords to SECCHI_PREP or the other routines.
;
; Calls       :	ANYTIM2UTC, BREAK_FILE, MATCH, SCC_READ_SUMMARY, COR1_PBSERIES
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 1-Apr-2010, William Thompson, GSFC
;               Version 2, 13-Apr-2010, WTT, improved COR2 filtering.
;                       Return fully qualified filenames.
;               Version 3, 10-Mar-2011, WTT, fix bug with /BEACON
;               Version 4, 23-Aug-2011, WTT, Relax condition for "combined"
;                       that img files must match img files, and seq files must
;                       match seq files.
;               Version 5, 17-Nov-2011, WTT, Fix bug when no pB series found
;               Version 6, 21-Feb-2013, WTT, Fix bug when totB files non-existant
;
; Contact     :	WTHOMPSON
;-
;
function cor1_totbseries, date, spacecraft, ssr=k_ssr, valid=valid, $
                 source=k_source, count=count, cor2=cor2, errmsg=errmsg, $
                 expmax=expmax, _extra=_extra
;
;;on_error, 2
;
;  Make sure that DATE is set correctly.
;
if n_elements(date) eq 0 then begin
    message = 'DATE is undefined'
    goto, handle_error
endif
if n_elements(date) gt 2 then begin
    message = 'DATE must have 1 or 2 elements'
    goto, handle_error
endif
message = ''
utc = anytim2utc(date, /external, errmsg=message)
if message ne '' then goto, handle_error
;
;  Check the spacecraft name.
;
if n_elements(spacecraft) ne 1 then begin
    message = 'SPACECRAFT must be a scalar'
    goto, handle_error
endif
if datatype(spacecraft) ne 'STR' then begin
    message = 'SPACECRAFT must be either "A" or "B" (or "C" for both)'
    goto, handle_error
endif
;
;  If the spacecraft name was passed as "Combined", then find simultaneous
;  images for both spacecraft.
;
ntotb = 0
if strupcase(spacecraft) eq strmid('COMBINED',0,strlen(spacecraft)) then begin
    message = ''
    cata = cor1_totbseries(utc, 'A', ssr=k_ssr, valid=valid, source=k_source, $
                           cor2=cor2, errmsg=message, _extra=_extra)
    if message ne '' then goto, handle_error
    catb = cor1_totbseries(utc, 'B', ssr=k_ssr, valid=valid, source=k_source, $
                           cor2=cor2, errmsg=message, _extra=_extra)
    if message ne '' then goto, handle_error
;
;  Look for matching filenames.
;
    break_file, cata[0,*].filename, diska, dira, namea
    break_file, catb[0,*].filename, diskb, dirb, nameb
    namea = strmid(namea,0,15)
    nameb = strmid(nameb,0,15)
    match, namea, nameb, wa, wb, count=count
    if count eq 0 then goto, no_results
    list = reform([[cata[*,wa]], [catb[*,wb]]], 3, count, 2)
    return, list
endif
;
;  Define various keywords.
;
if n_elements(k_source) eq 1 then source = k_source else source = 'lz'
if n_elements(k_ssr) eq 1 then ssr = k_ssr else ssr=1
if keyword_set(cor2) then tel = 'cor2' else tel = 'cor1'
;
;  Look for the specified spacecraft.
;
totblist = scc_read_summary(date=utc, spacecraft=spacecraft, type='img', $
                            telescope=tel, source=source, _extra=_extra)
if datatype(totblist,1) ne 'Structure' then goto, process_pb
;
;  Filter the data by the SSR keyword.
;
case ssr of
    2:    w = where(totblist.dest eq 'SSR2', ntotb)
    3:    w = where(totblist.dest ne 'SW', ntotb)
    7:    w = where(totblist.dest eq 'SW', ntotb)
    else: w = where(totblist.dest eq 'SSR1', ntotb)
endcase
if ntotb gt 0 then totblist = totblist[w] else goto, process_pb
;
;  Remove some "extra" COR2 images that are not part of the desired synoptic
;  observations.
;
if keyword_set(cor2) then begin
    if not keyword_set(expmax) then expmax = 20
    w = where((totblist.dest eq 'SW') or ((totblist.xsize ge 512) and $
              (totblist.exptime lt expmax) and ((totblist.exptime ge 5) or $
              (totblist.date_obs lt '2009/06/01'))), ntotb)
    if ntotb eq 0 then goto, process_pb
    totblist = totblist[w]
endif
;
;  If the /VALID keyword was passed, then remove any files with missing pixels.
;
if keyword_set(valid) then begin
    w = where(totblist.nmiss eq 0, ntotb)
    if ntotb eq 0 then goto, process_pb
    totblist = totblist[w]
endif
;
;  Find the paths to the files.  If the /VALID keyword was passed, then filter
;  out files which aren't found.
;
totblist.filename = sccfindfits(totblist.filename, _extra=_extra)
if keyword_set(valid) then begin
    w = where(totblist.filename ne '', ntotblist)
    if ntotblist gt 0 then totblist=totblist[w]
endif
;
;  Triplicate the TOTB entries.
;
sz = size(totblist)
temp = temporary(totblist)
totblist = replicate(temp[0], [3, sz[1:sz[0]]])
totblist[0,*,*] = temp
totblist[1,*,*] = temp
totblist[2,*,*] = temp
;
;  Next, process the pbseries data.
;
process_pb:
message = ''
pblist = cor1_pbseries(utc, spacecraft, ssr=ssr, valid=valid, cor2=cor2, $
                       errmsg=message, count=npb, _extra=_extra)
if (message ne '') and (npb ne 0) then goto, handle_error
;
;  Determine the total number of results, and handle the simple cases.
;
count = ntotb + npb
if count eq 0 then goto, no_results
if ntotb eq 0 then return, pblist
if npb eq 0 then return, totblist
;
;  Create an array to hold the output.
;
dim = [3, count]
list = replicate(totblist[0], dim)
;
;  Sort the results by the observation date.
;
date_totb = reform(totblist[0,*,0].date_obs)
date_pb   = reform(  pblist[0,*,0].date_obs)
date_obs = [date_totb, date_pb]
s = sort(date_obs)
date_obs = date_obs[s]
;
match, date_obs, date_totb, wa, wb
list[*,wa,*] = totblist[*,wb,*]
;
match, date_obs, date_pb, wa, wb
list[*,wa,*] = pblist[*,wb,*]
return, list
;
;  Error point for no results.
;
no_results:
message = 'No results found'
;
;  Point for general error handling.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = message
count = 0
return, -1
end
