;+
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_FITPOL
;
; Purpose     :	Return fitted COR1 total and polarized brightness
;
; Category    :	STEREO, SECCHI, COR1, Analysis
;
; Explanation :	This routine differs from COR_POLARIZ and COR1_QUICKPOL in that
;               it imposes a restriction on the polarization that it must be
;               either tangential to the solar limb, or radial.  Radial
;               polarizations are returned as negative values.  The total
;               brightness calculation is unaffected.
;
;               A least-squares fit is applied to the three polarized images to
;               determine the polarization parameters.  The effect of this
;               fitting is to reduce the effect of image noise on the deduced
;               polarization signal, particularly in the faint parts of the
;               image.
;
;               Because the polarization can be both positive and negative,
;               remaining noise in the polarization can be reduced by binning.
;               With COR_POLARIZ or COR1_QUICKPOL, the binning must be applied
;               to the original images before calling these routines to reduce
;               the noise.
;
;               *** IMPORTANT ***
;
;               Images must not be rotated before calling this routine.  If
;               rotation is desired, use the /ROTATE_ON keyword in this routine
;               instead of SECCHI_PREP to rotate the images.
;
;               As of version 3, this routine also works with COR2 data.
;               Provisional offsets of 45.8 and -18.0 degrees for Ahead and
;               Behind respectively are applied.
;
; Syntax      :	COR1_FITPOL, HEADER, IMAGE, TOTB  PB  [, CHISQR ]
;
; Examples    :	CAT = COR1_PBSERIES('1-Apr-2007', 'Ahead')
;               SECCHI_PREP, CAT.FILENAME, HEADER, IMAGE
;               COR1_FITPOL, HEADER, IMAGE, TOTB, PB
;
; Inputs      :	HEADER  = Array containing the image header structures.
;
;               IMAGE   = Array containing the pB images to process, with
;                         polarizer angles of 0,120,240.  This must have either
;                         the dimensions (Nx,Ny,3*Ns) or (Nx,Ny,3,Ns), where Ns
;                         is the number of sequences.
;
; Opt. Inputs :	None.
;
; Outputs     :	TOTB    = The total brightness image(s)
;               PB      = The polarized brightness image(s)
;
; Opt. Outputs:	CHISQR  = Array of chi-squared values.
;
;               When called with /ROTATE_ON, the HEADER and IMAGE arrays are
;               modified.
;
; Keywords    :	DOUBLE  = If set, then the output images are returned as double
;                         precision.  The calculation is performed in double
;                         precision, but returned as floating point unless
;                         IMAGE is double precision or /DOUBLE is passed.
;
;               ROTATE_ON = Rotates both the input and output images such that
;                           solar North is at the top of the image.  This
;                           keyword must be passed here instead of in
;                           SECCHI_PREP.  Other SCC_ROLL_IMAGE keywords are
;                           also accepted.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then the input string is
;                         returned unchanged.  In order to use this feature,
;                         ERRMSG must be defined first, e.g.
;
;                               ERRMSG = ''
;                               COR1_FITPOL, ERRMSG=ERRMSG, ...
;                               IF ERRMSG NE '' THEN ...
;
; Calls       :	None.
;
; Common      :	None.
;
; Restrictions:	Images not be rotated before calling this routine.  Instead use
;               the /ROTATE_ON keyword in this routine to rotate the images.
;
;               To speed up and simplify the calculation, this routine assumes
;               that the position of Sun center is the same on all images.
;
;               Assumes that the reported polarization angles in the FITS
;               headers are correct.  The actual polarization angles may differ
;               by 1 or 2 degrees.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 28-Jul-2008, William Thompson, GSFC
;               Version 2, 23-Mar-2011, WTT, added /ROTATE_ON keyword
;               Version 3, 07-Jun-2013, WTT, added COR2 support
;
; Contact     :	WTHOMPSON
;-
;
pro cor1_fitpol, header, image, totb, pb, chisqr, double=k_double, $
                 rotate_on=rotate_on, errmsg=errmsg, _extra=_extra
on_error, 2
;
;  Check the number of input parameters.
;
if n_params() lt 4 then begin
    message = 'Syntax: COR1_FITPOL, HEADER, IMAGE, TOTB, PB  [, CHISQR ]'
    goto, handle_error
endif
;
;  Make sure that IMAGE has the right dimensions.
;
sz = size(image)
case sz[0] of
    3: begin
        if (sz[3] mod 3) ne 0 then begin
            message = 'Number of images must be divisible by 3'
            goto, handle_error
        endif
        save_dim = sz[1:3]
        nseq = sz[3]/3
        image = reform(image, sz[1], sz[2], 3, nseq, /overwrite)
    end
    4: begin
        if sz[3] ne 3 then begin
            message = 'Third IMAGE dimension must be 3'
            goto, handle_error
        endif
        nseq = sz[4]
        save_dim = sz[1:4]
    end
    else: begin
        message = 'IMAGE must have dimensions (Nx,Ny,3*Ns) or (Nx,Ny,3,Ns)'
        goto, handle_error
    end
endcase
;
;  If IMAGE is not double precision, then save the original image and make a
;  double precision version for processing.
;
test_dbl = datatype(image) eq 'DOU'
if test_dbl then begin
    delvarx, save_image
    a = temporary(image)
end else begin
    save_image = temporary(image)
    a = double(save_image)
endelse
;
;  Calculate the tangential angle at each point.
;
wcs = fitshead2wcs(header[0])
cen = wcs_get_pixel(wcs, [0,0])
x = dindgen(sz[1]) - cen[0]
y = dindgen(sz[2]) - cen[1]
x = rebin(reform(x,sz[1],1), sz[1],sz[2])
y = rebin(reform(y,1,sz[2]), sz[1],sz[2])
theta = atan(y,x)
dtor = !dpi / 180.d0
;
;  If COR2, then modify the angles accordingly.
;
if header[0].detector eq 'COR2' then begin
    theta = -theta
    if header[0].obsrvtry eq 'STEREO_A' then $
      theta = theta + (45.8 * dtor) else $      ;+45.8 degrees
      theta = theta - (18.0 * dtor)             ;-18.0 degrees
endif
;
;  Form the cosine-squared arrays, plus the average and average of the square.
;
cos2 = dblarr(sz[1],sz[2],3)
for i=0,2 do cos2[*,*,i] = cos(theta - header[i].polar*dtor)^2
acos2 = average(cos2,   3)
acos4 = average(cos2^2, 3)
;
;  Form the output arrays.
;
totb = dblarr(sz[1],sz[2],nseq)
pb   = dblarr(sz[1],sz[2],nseq)
if n_params() eq 5 then chisqr = dblarr(sz[1],sz[2],nseq)
;
;  For each sequence, calculate the total and polarized brightness.
;
for iseq = 0,nseq-1 do begin
    aa = a[*,*,*,iseq]
    aimage = average(aa,      3)
    aimcs2 = average(aa*cos2, 3)
    a1 = (aimcs2 - aimage*acos2) / (acos4 - acos2^2)
    a0 = aimage - a1*acos2
    totb[*,*,iseq] = 2*a0 + a1
    pb[*,*,iseq] = a1
;
;  If requested, calculate chi-squared.
;
    if n_params() eq 5 then $
      for i=0,2 do chisqr[*,*,iseq] = chisqr[*,*,iseq] + $
      (aa[*,*,i] - a0 - a1*cos2[*,*,i])^2
endfor
;
;  Restore the original IMAGE array.
;
if n_elements(save_image) gt 0 then image = temporary(save_image) else $
  image = temporary(a)
image = reform(image, save_dim, /overwrite)
;
;  If the original IMAGE was not double precision, then convert the output to
;  floating point.
;
if (not test_dbl) and (not keyword_set(k_double)) then begin
    totb = float(temporary(totb))
    pb   = float(temporary(pb))
    if n_params() eq 5 then chisqr = float(temporary(chisqr))
endif
;
;  If the /ROTATE_ON keyword was passed, then rotate the images.
;
if keyword_set(rotate_on) then begin
    image = reform(image, sz[1], sz[2], 3, nseq, /overwrite)
    header = reform(header, 3, nseq, /overwrite)
    for iseq=0,nseq-1 do begin
        htemp = header[0, iseq]
        temp = totb[*,*,iseq]
        scc_roll_image, htemp, temp, _extra=_extra
        totb[0,0,iseq] = temp
;
        htemp = header[0, iseq]
        temp = pb[*,*,iseq]
        scc_roll_image, htemp, temp, _extra=_extra
        pb[0,0,iseq] = temp
;
        if n_params() eq 5 then begin
            htemp = header[0, iseq]
            temp = chisqr[*,*,iseq]
            scc_roll_image, htemp, temp, _extra=_extra
            chisqr[0,0,iseq] = temp
        endif
;
        for ipol=0,2 do begin
            htemp = header[ipol, iseq]
            temp = image[*,*, ipol, iseq]
            scc_roll_image, htemp, temp, _extra=_extra
            header[ipol, iseq] = htemp
            image[0,0,ipol,iseq] = temp
        endfor
    endfor
    image = reform(image, save_dim, /overwrite)
    header = reform(header, save_dim[2:*], /overwrite)
endif
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else errmsg=message
return
end
