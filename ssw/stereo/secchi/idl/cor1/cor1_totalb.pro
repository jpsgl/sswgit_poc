;+
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_TOTALB
;
; Purpose     :	Combine COR1 series and totB images into totalB series
;
; Category    :	STEREO, SECCHI, COR1, Analysis
;
; Explanation :	This routine uses SCC_READ_SUMMARY and COR1_PBSERIES to combine
;               polarization sequences with total brightness images calculated
;               onboard to form a unified sequence of total brightness images.
;               Both types of images are processed via SECCHI_PREP.
;
; Syntax      :	COR1_TOTALB, DATE, SPACECRAFT, HEADER, IMAGE
;
; Examples    :	COR1_TOTALB, '8-Apr-2009', 'Behind', HEADER, IMAGE
;
;               DATE = ['7-Apr-2009 08:30', '8-Apr-2007 15:00']
;               COR1_TOTALB, DATE, 'Combined', HEADER, IMAGE, OUTSIZE=512
;
; Inputs      :	DATE    = Either the date of observation, or a beginning and
;                         end time to search on.  When a single date is passed,
;                         the entire day is returned.  Otherwise, only the
;                         portion between the beginning and end dates are
;                         returned.
;
;               SPACECRAFT = Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;                               'STEREO_Ahead'  'STEREO_Behind'
;
;                            Alternatively, the word "Combined" (or "C") tells
;                            the routine to look for simultaneous sequences in
;                            both spacecraft.
;
; Opt. Inputs :	None.
;
; Outputs     :	HEADER  = The FITS header index structures.
;
;               IMAGE   = Array containing the images.
;
; Opt. Outputs:	None.
;
; Keywords    :	SSR     = If passed, has the following meaning:
;
;                               SSR=1   Synoptic data (default)
;                               SSR=2   Special event data
;                               SSR=3   Synoptic + Special event
;                               SSR=7   Space weather
;
;               COUNT   = Returns the number of sequences found.
;
;               VALID   = If set, then only series with no missing blocks
;                         (NMISSING LE 0) are returned.
;
;               SOURCE  = Telemetry source, either "rt" (realtime), "pb"
;                         (playback), or "lz" (level-zero, default).
;
;               COR2    = If set, search for COR2 images instead of COR1.
;
;               ADJUST_COR2 = If set, then adjust COR2 double exposures to
;                             match pB series images.
;
;               OUTSIZE = Size of output images.  If not passed, then the size
;                         is based on the total brightness images.
;
;               ERRMSG  = If defined and passed, then any error messages will
;                         be returned to the user in this parameter rather than
;                         depending on the MESSAGE routine in IDL.  If no
;                         errors are encountered, then the input string is
;                         returned unchanged.  In order to use this feature,
;                         ERRMSG must be defined first, e.g.
;
;                               ERRMSG = ''
;                               Result = COR1_PBSERIES( ERRMSG=ERRMSG, ... )
;                               IF ERRMSG NE '' THEN ...
;
;               Along with any keywords to SECCHI_PREP or the other routines.
;
; Calls       :	COR1_TOTBSERIES, COR1_TOTBPREP
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 1-Apr-2010, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro cor1_totalb, date, spacecraft, header, image, count=count, errmsg=errmsg, $
                 _extra=_extra
;
on_error, 2
message = ''
list = cor1_totbseries(date, spacecraft, count=count, errmsg=message, $
                       _extra=_extra)
if message ne '' then goto, handle_error
cor1_totbprep, list.filename, header, image, errmsg=message, _extra=_extra
if message ne '' then goto, handle_error
return
;
;  Point for general error handling.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message, /continue else $
  errmsg = message
count = 0
return
end
