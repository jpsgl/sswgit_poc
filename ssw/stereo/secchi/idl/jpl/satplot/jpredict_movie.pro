; "Copyright 2012, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."

pro JPREDICT_MOVIE,datetime1,datetime2,pa,delta,spacecraft,SAVE=save

 if size(local,/type) eq 0 then local=0

 startup_flag	= 1
 position_angle	= pa
 arc_deg		= delta
 cadence_minutes = 30
 zoom		= 3
 sc		= strlowcase(spacecraft)
 m_pa		= position_angle*zoom
 m_delta		= arc_deg*zoom
 datetime=datetime1
 month=strmid(datetime,4,2)
 day=strmid(datetime,6,2)
 year=strmid(datetime,0,4)
 hour=strmid(datetime,9,2)
 minute=strmid(datetime,11,2)
 second=strmid(datetime,13,2)
 jd1		= JULDAY(month,day,year,hour,minute,second)
 datetime=datetime2
 month=strmid(datetime,4,2)
 day=strmid(datetime,6,2)
 year=strmid(datetime,0,4)
 hour=strmid(datetime,9,2)
 minute=strmid(datetime,11,2)
 second=strmid(datetime,13,2)
 jd2		= JULDAY(month,day,year,hour,minute,second)
 jd_cadence	= cadence_minutes/60./24.
 nsteps=(jd2-jd1)/jd_cadence
 curwin=!d.window
 for step=0,nsteps do begin
  jd=jd1+(jd_cadence*step)
  CALDAT,jd,mon,day,year,hour,minute,second
  IF mon LT 10 THEN mon='0'+STRTRIM(mon,2) ELSE mon=STRTRIM(mon,2)
  IF day LT 10 THEN day='0'+STRTRIM(day,2) ELSE day=STRTRIM(day,2)
  IF hour LT 10 THEN hour='0'+STRTRIM(hour,2) ELSE hour=STRTRIM(hour,2)
  IF minute LT 10 THEN minute='0'+STRTRIM(minute,2) ELSE minute=STRTRIM(minute,2)
  second			= FIX( second )
  IF second LT 10 THEN second='0'+STRTRIM(second,2) ELSE second=STRTRIM(second,2)
  date	= STRTRIM( year, 2 ) + mon + day
  time	= hour + minute + second
  filename=file_search(get_closest_jmap_filename( date, time, sc, LOCAL=local ))
  print,'filename = ',filename
  map=read_image(filename)
  m_size=min([n_elements(map[*,0]),n_elements(map[0,*])])
  while m_size GT min(get_screen_size()) do begin
   m_size=m_size/2
   if startup_flag eq 1 then begin
    m_pa=m_pa/2
    m_delta=m_delta/2
   endif
   map=congrid(map,m_size,m_size,cubic=-0.5)
  endwhile
  if startup_flag eq 1 then begin
   window,xsize=m_size,ysize=m_size,/free,retain=2
  endif
  ; Darken the un-highlighted areas a little bit (0.85).
  tv,map[0:m_pa-1,*]*0.85,0,0
  tv,map[m_pa:m_pa+m_delta,*],m_pa,0
  tv,map[m_pa+m_delta+1:m_size-1,*]*0.85,m_pa+m_delta+1,0
  xyouts,0.5,0.20,strmid(file_basename(filename),0,15),align=0.5,charsize=2,charthick=3,color=!d.n_colors-1
  xyouts,0.5,0.20,strmid(file_basename(filename),0,15),align=0.5,charsize=2,charthick=1,color=0
  xyouts,0.5,0.25,strmid('STEREO-'+strupcase(spacecraft),0,15),align=0.5,charsize=2,charthick=3,color=!d.n_colors-1
  xyouts,0.5,0.25,strmid('STEREO-'+strupcase(spacecraft),0,15),align=0.5,charsize=2,charthick=1,color=0
  startup_flag=0
  if size(save,/type) gt 0 then begin
   if save gt 0 then begin
    write_png,file_basename(filename)+'_jpredict_movie.png',tvrd()
   endif
  endif
 endfor
 wset,curwin
end
