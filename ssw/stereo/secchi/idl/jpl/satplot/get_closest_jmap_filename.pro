; $Id:
;+
; written by:   Jeffrey.R.Hall@jpl.nasa.gov
; written for:  Paulett.C.Liewer@jpl.nasa.gov
;               STEREO/SECCHI Project
;
; "Copyright 2009, by the California Institute of Technology. 
; ALL RIGHTS RESERVED. United States Government Sponsorship 
; acknowledged. Any commercial use must be negotiated with the 
; Office of Technology Transfer at the California Institute of 
; Technology.
;
; This software may be subject to U.S. export control laws. 
; By accepting this software, the user agrees to comply with 
; all applicable U.S. export laws and regulations. User has 
; the responsibility to obtain export licenses, or other 
; export authority as may be required before exporting such 
; information to foreign countries or providing access to 
; foreign persons."
;-

FUNCTION get_closest_jmap_filename, date, time, spacecraft

	secchi_env_var=GETENV('secchi')

	; If necessary, change "/ra/" to "/ra2/"
	parts=STRSPLIT(secchi_env_var,PATH_SEP(),/EXTRACT)
	ndx=(WHERE(parts EQ 'ra'))[0]
	IF ndx GE 0 THEN BEGIN
		temp=PATH_SEP()
		FOR p=0,ndx-1 DO BEGIN
			temp=temp + parts[p] + PATH_SEP()
		ENDFOR
		temp=temp + parts[ndx] + '2' + PATH_SEP()
		FOR p=ndx+1,N_ELEMENTS(parts)-1 DO BEGIN
			temp=temp + parts[p] + PATH_SEP()
		ENDFOR
		secchi_env_var=temp
	ENDIF

	find_command='find '+secchi_env_var+'/../../../../../secchi3/net/corona/secchi/flight/lz/L0/'+spacecraft+'/img/jmap/'+date+' -name ''*.png'' | sort'

        SPAWN,find_command,list

	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	julian_anchor = JULIANDAY( date + time )
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN ndx = ndx[0] ELSE ndx = 0
	map_filename = list[ndx]

	IF (ndx EQ 0) OR (ndx EQ count-1) THEN BEGIN
		julian_check = julian_anchor + ([-1,1])[ndx NE 0]
		CALDAT, julian_check, month, day, year, hour, minute, second
		IF (  year LT 10) THEN   year = '0' + STRTRIM(   year, 2 ) ELSE   year = STRTRIM(   year, 2 )
		IF ( month LT 10) THEN  month = '0' + STRTRIM(  month, 2 ) ELSE  month = STRTRIM(  month, 2 )
		IF (   day LT 10) THEN    day = '0' + STRTRIM(    day, 2 ) ELSE    day = STRTRIM(    day, 2 )
		IF (  hour LT 10) THEN   hour = '0' + STRTRIM(   hour, 2 ) ELSE   hour = STRTRIM(   hour, 2 )
		IF (minute LT 10) THEN minute = '0' + STRTRIM( minute, 2 ) ELSE minute = STRTRIM( minute, 2 )
		IF (second LT 10) THEN second = '0' + STRTRIM( second, 2 ) ELSE second = STRTRIM( second, 2 )
		date_new = year + month + day
		dir=secchi_env_var+'/../../../../../secchi3/net/corona/secchi/flight/lz/L0/'+spacecraft+'/img/jmap/'+date+'/'
		list_new=FILE_SEARCH(dir,'*.png',/FULLY_QUALIFY_PATH,COUNT=file_count)
		; Weed out any occurances of '_n4h'
		ndx=WHERE(STRPOS(list_new,'_n4h') EQ -1,ndx_count)
		IF (ndx_count GT 0) THEN list_new=list_new[ndx]
		; Weed out any occurances of '_k4h'
		ndx=WHERE(STRPOS(list_new,'_k4h') EQ -1,ndx_count)
		IF (ndx_count GT 0) THEN list_new=list_new[ndx]
		; Sort the list_new.
		list_new=list_new[SORT(list_new)]
		list = [ list , list_new ]
	ENDIF
	all_dates = STRMID( FILE_BASENAME( list ), 0, 8 )
	all_times = STRMID( FILE_BASENAME( list ), 9, 6 )
	all_juliandays = DBLARR( N_ELEMENTS( list ) )
	FOR jd = 0, N_ELEMENTS( list ) - 1 DO BEGIN
		all_juliandays[jd] = JULIANDAY( all_dates[jd] + all_times[jd] )
	ENDFOR
	all_diff = ABS(all_juliandays - julian_anchor)
	min_diff = MIN( all_diff )
	ndx = WHERE( all_diff EQ min_diff, count )
	IF (count GE 0) THEN ndx = ndx[0] ELSE ndx = 0
	map_filename = list[ndx]

	RETURN, map_filename
END
