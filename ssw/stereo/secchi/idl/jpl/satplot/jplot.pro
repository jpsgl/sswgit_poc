; $Id:
;+
;jplot.pro
;
;PURPOSE:
; image brightness plot reveals cme velocity curve.
;
;METHOD:
; plot narrow angular wedge from sun center to specified outer limit as time series columns in 
; cylindrical plot, where xaxis represents time and yaxis represents space and both are linear.
;
;OPTIONS:
; include specified detectors usually COR2/HI_1/HI_2.  (EUVI not relevant.)
; time cadence in minutes (hi_1 cadence is nominally 40 minutes, for example, so 40 might be a good choice)
; delta (degrees of angular spread sample)
;
;FUTURE OPTIONS:
; zoom (and/or outdisplay xsize,ysize or somesuch control)
; jd start (and or anytim(date,time) maybe?)
; jd end (and/or nsteps maybe?)
; instrument set (default:  cor2/hi_1/hi_2, add option for cor1)
; scale, or MAP_SET LIMIT parameter
;
;EXAMPLE:
; IDL> jplot,'20120710_000000','20120715_000000',90,10,'a'
; This example will create a jplot image that is composed 
; of data taken July 10,11,12,13,14 in 2012 (end time is 
; at the beginning of July 15.)
;
;DEPENDENCIES:
; solarsoft configured with the 'secchi' inst
; $secchi must be defined as the directory that contains "lz/"
;
;WRITTEN BY:
; Jeffrey R. Hall (for Paulett Liewer STEREO/SECCHI)
; Jet Propulsion Laboratory
; May 2009 (initial version)
; Jul 2012 (latest version)
;-
PRO JPLOT, datetime_start, datetime_end, position_angle, delta, spacecraft

	zoom			= 3
	cadence_minutes		= 30
	jd_start		= (ANYTIM2JD(datetime_start)).(0)+(ANYTIM2JD(datetime_start)).(1)
	jd_end			= (ANYTIM2JD(datetime_end)).(0)+(ANYTIM2JD(datetime_end)).(1)
	spacecraft		= strlowcase(spacecraft)

	jd			= jd_start
	nsteps			= ROUND((jd_end - jd_start) * (24. * (60. / cadence_minutes)))
	xsize			= nsteps
	ysize			= 360 * zoom
;	WINDOW,1,XSIZE=xsize,YSIZE=ysize

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 2.  MAKE SURE ORIGIN IS AT BOTTOM
;		REVERSE parameter in either map_set or map_patch, or some other technique
;
; 3.  CONSTRUCT X-AXIS AND Y-AXIS OF PLOT SPACE, POSITION JPLOT IN PLOT SPACE
;		Unset NOBORDER, XMARGIN, YMARGIN, allow them to default to plot space and take advantage of that
;
; 5.  BUILD GUI FOR THE FOLLOWING CONTROL VARIABLES:
;		position_angle	= 75
;		delta			= 3
;		zoom			= 3
;		cadence_minutes	= 40
;		spacecraft		= 'b'
;		jd_start		= JULDAY(9,1,2007,0,0,0)
;		jd_end			= JULDAY(9,31,2007,0,0,0)
;		instrument set (default:  cor2/hi_1/hi_2, add option for cor1)
;		scale, or MAP_SET LIMIT parameter (IS THIS THE SAME AS ZOOM?)
;
; 6.  OPTIONALLY EMPLOY ALTERNATE ALGORITHM
;		Algorithm 1:  cylindrical mapping
;		Algorithm 2:  corden-off box sampling (see test1_jplot.pro)
;
; 7.  ESTIMATE DISK SPACE
;		PNG difference images from SREM_MOVIE
;		PNG + FTS maps + jplots
;
; 8.  ESTIMATE RUN TIME
;		Run one jplot column to get time?
;		Possibly add progress bars
;			- build lists
;			- SREM_MOVIE difference images
;			- create jplots
;
; 9.  GRAPHICALLY SHOW ESTIMATES OF position_angle, delta, zoom
;
;10.  POSSIBLY REPLACE CONGRID WITH MEAN
;		test to see if it gives same result.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	jplot=BYTARR(xsize,ysize)
	column=MAKE_ARRAY(ysize,TYPE=SIZE(slice,/TYPE))
	dates=STRARR(nsteps)
	times=STRARR(nsteps)

	;----------------------------------------------------------
	; Generate lists of dates, times, HI_1 filenames, and HI_2 filenames.
	;----------------------------------------------------------
	FOR timeframe = 0L, nsteps-1 DO BEGIN
		;----------------------------------------------------------
		; Manage the date format.
		;----------------------------------------------------------
		CALDAT,jd,mon,day,year,hour,minute,second
		IF mon LT 10 THEN mon='0'+STRTRIM(mon,2) ELSE mon=STRTRIM(mon,2)
		IF day LT 10 THEN day='0'+STRTRIM(day,2) ELSE day=STRTRIM(day,2)
		IF hour LT 10 THEN hour='0'+STRTRIM(hour,2) ELSE hour=STRTRIM(hour,2)
		IF minute LT 10 THEN minute='0'+STRTRIM(minute,2) ELSE minute=STRTRIM(minute,2)
		second			= FIX( second )
		IF second LT 10 THEN second='0'+STRTRIM(second,2) ELSE second=STRTRIM(second,2)
		dates[timeframe]	= STRTRIM( year, 2 ) + mon + day
		times[timeframe]	= hour + minute + second
		;----------------------------------------------------------
		; Cadence, in minutes.
		;----------------------------------------------------------
		jd = jd + ( ((cadence_minutes / 60.0d) / 24.)  )
	ENDFOR
		; One more date calculation for the end-date in the PNG filename.
		CALDAT,jd,mon,day,year,hour,minute,second
		IF mon LT 10 THEN mon='0'+STRTRIM(mon,2) ELSE mon=STRTRIM(mon,2)
		IF day LT 10 THEN day='0'+STRTRIM(day,2) ELSE day=STRTRIM(day,2)
		IF hour LT 10 THEN hour='0'+STRTRIM(hour,2) ELSE hour=STRTRIM(hour,2)
		IF minute LT 10 THEN minute='0'+STRTRIM(minute,2) ELSE minute=STRTRIM(minute,2)
		second			= FIX( second )
		IF second LT 10 THEN second='0'+STRTRIM(second,2) ELSE second=STRTRIM(second,2)
		end_date	= STRTRIM( year, 2 ) + mon + day
		end_time	= hour + minute + second

	FOR frame = 0L, nsteps-1 DO BEGIN
;if frame mod 100 eq 0 then begin
; print,'Creating jplot columns.  Frame ', strtrim(frame,2), ' of ', strtrim(nsteps,2),'    ',dates[frame],'_',times[frame]
;l print,'file_search(get_closest_jmap_filename( dates[frame], times[frame], ',spacecraft,'  )) = ',file_search(get_closest_jmap_filename( dates[frame], times[frame], spacecraft  ))
;endif
		jmap_filename=file_search(get_closest_jmap_filename( dates[frame], times[frame], spacecraft  ))
print,jmap_filename
		map=read_image(jmap_filename)
		slice=map[position_angle*zoom:position_angle*zoom+delta*zoom,*]
;		slice=REVERSE(slice,2)
;		column=CONGRID(slice,1,ysize,CUBIC=-0.5)
		FOR row=0,ysize-1 DO column[row]=MEAN(slice[*,row])
		jplot[frame,*]=column
	ENDFOR

	zpad=''
;	png_filename='jplot_' + dates[0]+'_'+times[0] + '__' + dates[nsteps-1]+'_'+times[nsteps-1] + '__' + $
	png_filename='jplot_' + dates[0]+'_'+times[0] + '__' + end_date+'_'+end_time + '__' + $
		STRTRIM(position_angle,2) + '__' + STRTRIM(delta,2) + '__' + STRUPCASE(spacecraft) + '.png'
	print
	PRINT,'execute this:  '
	PRINT,'satplot,''',png_filename,''''
	WRITE_PNG, png_filename, jplot
END

