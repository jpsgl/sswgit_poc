#!/bin/csh
# $Id:
# Example script for downloading daily series (30 minute cadence) jmap sets (a.k.a. pcmap) from JPL.
# Change dates (yyyymmdd) as needed.
foreach date (20080324 20080325 20080326 20080327 20080328 20080329)
   foreach img (`curl -sl http://solarmuse.jpl.nasa.gov/data/jmap_a/$date/ | grep _a_jmap.png | cut -d ' ' -f 5 | cut -c 7-32`)
      echo transferring:  $img
      curl -s -O http://solarmuse.jpl.nasa.gov/data/jmap_a/$date/$img
   end
   foreach img (`curl -sl http://solarmuse.jpl.nasa.gov/data/jmap_b/$date/ | grep _b_jmap.png | cut -d ' ' -f 5 | cut -c 7-32`)
      echo transferring:  $img
      curl -s -O http://solarmuse.jpl.nasa.gov/data/jmap_b/$date/$img
   end
end

