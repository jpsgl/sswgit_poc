#!/bin/csh
# $Id: getjmaps.sh,v 1.1 2010/12/09 18:50:47 nathan Exp $
#
# Usage: getjmaps.sh a 20080324
#
set sc=$1
set date=$2
foreach img (`curl -s -l http://solarmuse.jpl.nasa.gov/data/jmap_$sc/$date/ | grep _${sc}_jmap.png | cut -d ' ' -f 5 | cut -c 7-32`)
echo transferring:  $img
curl -s -O http://solarmuse.jpl.nasa.gov/data/jmap_$sc/$date/$img
end
