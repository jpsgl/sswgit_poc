; $Id:
PRO JPREDICT,datetime,pa,delta,spacecraft,SAVE=save
position_angle	= pa
arc_deg		= delta
zoom		= 3
sc		= strlowcase(spacecraft)
m_pa		= position_angle*zoom
m_delta		= arc_deg*zoom
month=strmid(datetime,4,2)
day=strmid(datetime,6,2)
year=strmid(datetime,0,4)
hour=strmid(datetime,9,2)
minute=strmid(datetime,11,2)
second=strmid(datetime,13,2)
help,month,day,year,hour,minute,second
jd		= JULDAY(month,day,year,hour,minute,second)
CALDAT,jd,mon,day,year,hour,minute,second
IF mon LT 10 THEN mon='0'+STRTRIM(mon,2) ELSE mon=STRTRIM(mon,2)
IF day LT 10 THEN day='0'+STRTRIM(day,2) ELSE day=STRTRIM(day,2)
IF hour LT 10 THEN hour='0'+STRTRIM(hour,2) ELSE hour=STRTRIM(hour,2)
IF minute LT 10 THEN minute='0'+STRTRIM(minute,2) ELSE minute=STRTRIM(minute,2)
second			= FIX( second )
IF second LT 10 THEN second='0'+STRTRIM(second,2) ELSE second=STRTRIM(second,2)
date	= STRTRIM( year, 2 ) + mon + day
time	= hour + minute + second
filename=file_search(get_closest_jmap_filename( date, time, sc  ))
help,filename
print,'filename = ',filename
map=read_image(filename)
m_size=min([n_elements(map[*,0]),n_elements(map[0,*])])
while m_size GT min(get_screen_size()) do begin
 m_size=m_size/2
 m_pa=m_pa/2
 m_delta=m_delta/2
 map=congrid(map,m_size,m_size,cubic=-0.5)
endwhile
curwin=!d.window
window,xsize=m_size,ysize=m_size,/free,retain=2
tv,map[0:m_pa-1,*]*0.85,0,0
tv,map[m_pa:m_pa+m_delta,*],m_pa,0
tv,map[m_pa+m_delta+1:m_size-1,*]*0.85,m_pa+m_delta+1,0
xyouts,0.5,0.20,strmid(file_basename(filename),0,15),align=0.5,charsize=2,charthick=3,color=!d.n_colors-1
xyouts,0.5,0.20,strmid(file_basename(filename),0,15),align=0.5,charsize=2,charthick=1,color=0
xyouts,0.5,0.25,strmid('STEREO-'+strupcase(spacecraft),0,15),align=0.5,charsize=2,charthick=3,color=!d.n_colors-1
xyouts,0.5,0.25,strmid('STEREO-'+strupcase(spacecraft),0,15),align=0.5,charsize=2,charthick=1,color=0
wset,curwin
if size(save,/type) gt 0 then begin
 if save gt 0 then begin
  write_png,file_basename(filename)+'_jpredict.png',tvrd()
 endif
endif
END
