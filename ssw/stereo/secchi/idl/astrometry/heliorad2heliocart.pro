;+
; PURPOSE: transform from helioprojective radial to helioprojective cartesian. See W.T.Thompson, A&A Dec 15, 2005, section 4.
;
; INPUTS:
;   coord : helioprojective radial coordinates, in degrees.
;           either a two element array : [polar_angle,elongation]
;           or array of [0:1,*], with [0,*] the polar angle
;                                     [1,*] the elongation
;
; OUTPUTS: helioprojective cartesian coordinates [westward_angle,northward_angle], in the same format than the input.
;
;-

function heliorad2heliocart,coord

alphaP=0.
deltaP=0.

thetaP=!pi/2
phiP=0.

if n_elements(coord) eq 2 then begin
    phi=coord[0]*!dtor
    theta=!pi/2-coord[1]*!dtor
endif else begin
    phi=coord[0,*,*]*!dtor
    theta=!pi/2-coord[1,*,*]*!dtor
endelse

alpha=alphaP+atan(-cos(theta)*sin(phi-phiP),sin(theta)*cos(deltaP)-cos(theta)*sin(deltaP)*cos(phi-phiP))
delta=asin(sin(theta)*sin(deltaP)+cos(theta)*cos(deltaP)*cos(phi-phiP))



return,[alpha,delta]*!radeg
end
