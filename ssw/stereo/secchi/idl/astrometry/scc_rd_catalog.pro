PRO scc_rd_catalog, catalog, full=full, vmagmax=vmagmax

;+
; NAME:
;      SCC_RD_CATALOG
;
; PURPOSE:
;      Read SECCHI star catalogs for any of the SECCHI instruments into a useful structure.
;
; CATEGORY:
;       Astrometry
;      
; CALLING SEQUENCE:
;      scc_rd_catalog,catalog,[/full]
;
; INPUTS:
;
; KEYWORD PARAMETERS:
;      FULL: Read in full (35,000 stars!) catalog. Default is 6,000 star
;            sub-catalog of vmag < 7.0 stars.  
;      VMAGMAX = maximum visual magnitude in the final list
;
; OUTPUTS:
;      catalog: structure containing fields in catalog file.
;
; PROCEDURE:
;      
;
; MODIFICATION HISTORY:
;      Written by Karl Battams. June 2006
;               
; $Log: scc_rd_catalog.pro,v $
; Revision 1.4  2008/03/18 17:19:55  nathan
; fixed loss of revision 1.2
;
; Revision 1.3  2008/03/17 20:42:14  avourlid
; Added vmagmax keyword to allow return of stars up to maximum visual magnitude
;
; Revision 1.2  2008/03/10 15:41:24  nathan
; update infile path for SSW
;
; Revision 1.1  2008/03/10 14:59:30  nathan
; moved from dev/astrometry
;
; Revision 1.5  2008/03/07 15:04:48  reduce
; Reads new star catalogs. Karl B.
;
; Revision 1.4  2008/02/06 19:08:46  reduce
; Fixed a formatting bug and a path issue. KarlB
;
; Revision 1.3  2006/09/06 16:05:28  reduce
; Add /full kw for reading full secchi star catalog
;
; Revision 1.2  2006/09/06 14:17:14  reduce
; Change path to catalog file. Karl B.
;
;-            

IF keyword_set(full) then infile0 = 'secchi_catalog_full.dat' else infile0 = 'secchi_catalog.dat'
infile=concat_dir(concat_dir(getenv('SCC_DATA'),'astrometry'),infile0)

openr,lun,infile,/get_lun
nlines=file_lines(infile)

inline = {hicat, $
          Hic:0l, $             ; Hipparcos Number
          Alpha:0.0d, $         ; Right Ascension (degrees)
          Delta:0.0d, $         ; Declination (degrees)
          Lon:0., $             ; Longitude (degrees)
          Lat:0., $             ; Latitude (degrees)
          Vmag:0., $            ; Visual Magnitude
          Dvmag:0.,  $          ; Error on visual magnitude
          Bvmag:0.,  $          ; BV Magnitude
          Dbvmag:0.,  $         ; Error on BV Magnitude
          Spectype:'',  $       ; Spectral Type
          Hd:0l,  $             ; HD catalog number
          Sao:0l }              ; SAO catalog number

catalog = replicate({hicat}, nlines)

for i = 0L, nlines-1 do begin
    readf, lun, inline, format = $
      '(i6,x,f10.6,x,f10.6,x,f6.2,x, f5.3,2x, f6.3,x, f5.2,x, f6.3,x, f5.3,3x, a11,x, i6,3x, i6,x)'

    catalog(i) = inline
endfor

if keyword_set(vmagmax) then begin 
    idx=where(catalog.vmag le vmagmax,cnt)
    if (cnt gt 0) then catalog=catalog(idx) else begin &
        print,'No stars found below Vmag=',vmagmax
        print,'Catalog was not modified!'
    endelse
endif
;
free_lun, lun

return
end
