;+
;$Id: stereo_rsun.pro,v 1.3 2008/07/22 16:05:50 nathan Exp $
;
; Project     : STEREO - SECCHI
;
; NAME: stereo_rsun
;
; PURPOSE:  This function returns radius of sun in arcsec from one STEREO spacecraft
;
; CATEGORY: spice astrometry coordinates position ephemeris
;
; CALLING SEQUENCE:
;
;       Result = stereo_rsun(date, spacecraft)
;
; Inputs      : DATE         The date and time.  This can be input in any
;                            format accepted by ANYTIM2UTC, and can also be an
;                            array of values.
;
;               SPACECRAFT   Can be one of the following forms:
;
;                               'A'             'B'
;                               'STA'           'STB'
;                               'Ahead'         'Behind'
;                               'STEREO Ahead'  'STEREO Behind'
;                               'STEREO-Ahead'  'STEREO-Behind'
;
;                            Case is not important.  The NAIF numeric codes of
;                            -234 and -235 respectively can also be used.
;
;                            Also can be the name of a solar system body,
;                            e.g. "Earth", "Mars", "Moon", etc.
;
; Opt. Inputs : None.
;
; KEYWORDS: 	
;   	    	DISTANCE=named variable contains distance in km corresponding to output
;
; OUTPUTS:  	result: scalar or vector of solar radius in arcsec
;
; Calls       : GET_STEREO_COORD, CSPICE_RECLAT
;
; Common      : None.
;
; Restrictions: This procedure works in conjunction with the Icy/CSPICE
;               package, which is implemented as an IDL Dynamically Loadable
;               Module (DLM).  The Icy source code can be downloaded from
;
;                       ftp://naif.jpl.nasa.gov/pub/naif/toolkit/IDL
;
;               Because this uses dynamic frames, it requires Icy/CSPICE
;               version N0058 or higher.
;
; Side effects: Will automatically load the SPICE ephemeris files, if not
;               already loaded.;
;
; Prev. Hist. : None
;
;-
;$Log: stereo_rsun.pro,v $
;Revision 1.3  2008/07/22 16:05:50  nathan
;Added DISTANCE kword
;
;Revision 1.2  2008/07/16 14:42:11  nathan
;update doc
;
;Revision 1.1  2008/07/16 14:37:03  nathan
;gets solar radius
;

FUNCTION stereo_rsun, datetime, sc, DISTANCE=distance
  
heed=get_stereo_lonlat(datetime,sc,system='hee')
distance=heed[0,*]
r= (6.95508e5 * 648d3 / !dpi ) / distance
IF n_elements(r) GT 1 THEN return,r ELSE return,r[0]

END
