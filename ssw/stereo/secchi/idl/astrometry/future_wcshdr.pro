function future_wcshdr,sc,tel,dateobs,timeobs
;+
; $Id: future_wcshdr.pro,v 1.1 2009/10/06 18:55:58 mcnutt Exp $
;
; NAME:	future_wcshdr
;
; PURPOSE:
;	make future wcshdr from existing fits header by ajdusting times
;
; CATEGORY:  header, fits, wcs
;
; CALLING SEQUENCE:
;
; INPUTS:
;	sc= 'a' or 'b' ,tel ='hi_1' hi_2','cor2','cor1' or 'euvi' ,dateobs='yyyy-mm-dd' ,timeob='hh:mm:ss'
;
; OPTIONAL INPUTS:
;	
; KEYWORD PARAMETERS: 
;
; OUTPUTS:
;	fwcsh	
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; EXAMPLE:
;-
; MODIFICATION HISTORY:
;
; WRITEN: Lynn Simpson 2009-08-28
;
; $Log: future_wcshdr.pro,v $
; Revision 1.1  2009/10/06 18:55:58  mcnutt
; creates a wcs header for some time in the future
;
;

if strlowcase(tel) eq 'hi_1' then file='20090824_080901_s4h1'+strupcase(sc)+'.fts'
if strlowcase(tel) eq 'hi_2' then file='20090824_080921_s4h2'+strupcase(sc)+'.fts'
if strlowcase(tel) eq 'cor2' then file='20090824_000800_d4c2'+strupcase(sc)+'.fts'
if strlowcase(tel) eq 'cor1' then file='20090824_000500_s4c1'+strupcase(sc)+'.fts'
if strlowcase(tel) eq 'euvi' then file='20090824_000530_n4eu'+strupcase(sc)+'.fts'
fname = sccfindfits(file)
jk=sccreadfits(fname,scch,/nodata)


scch.date_obs=dateobs+'T'+timeobs
scch.DATE_CMD=dateobs+'T'+timeobs
scch.DATE_AVG=dateobs+'T'+timeobs
scch.DATE_END=dateobs+'T'+timeobs
 
hee_d=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1), /meters, system='HEE',_extra=_extra)
scch.RSUN=(6.95508e8 * 648d3 / !dpi ) / hee_d(0)
t=getscccrpix(scch,/UPDATE, FAST=fast, _EXTRA=_extra)

	yprhpc=getsccpointing(scch,/updateheader, TOLERANCE=60, YPRSC=scp, FAST=fast, FOUND=ff, _EXTRA=_extra)
	
	IF (scch.detector ne 'EUVI') THEN BEGIN
	    scch.CRVAL1	=yprhpc[0]
	    scch.CRVAL2	=yprhpc[1]
	ENDIF	
	; for EUVI contained in CRPIX and CRVAL=0
	
	scch.CROTA	=yprhpc[2]
	dr=!DPI/180.d
	scch.PC1_1= cos(yprhpc[2]*dr)
	scch.PC1_2=-sin(yprhpc[2]*dr) ;*(scch.CDELT1/scch.CDELT2) ;may be added later
	scch.PC2_1= sin(yprhpc[2]*dr) ;*(scch.CDELT1/scch.CDELT2)
	scch.PC2_2= cos(yprhpc[2]*dr)


if scch.detector eq 'EUVI' then begin
    carlonlat=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1),/au, /degrees, system='Carrington')
    scch.CRLN_OBS=carlonlat(1)
    scch.CRLT_OBS=carlonlat(2)
    heeq=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1), /degrees, system='HEEQ',_extra=_extra)
    scch.HGLN_OBS=heeq(1)
    scch.HGLT_OBS=heeq(2)
    hee_d=get_stereo_lonlat(scch.date_obs,strmid(scch.obsrvtry,7,1), /meters, system='HEE',_extra=_extra)
    scch.DSUN_OBS=hee_d(0)
endif

fwcsh=fitshead2wcs(scch)

return, fwcsh
END
