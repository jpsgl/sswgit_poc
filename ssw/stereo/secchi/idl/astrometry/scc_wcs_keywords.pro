pro scc_wcs_keywords, scch, ATTITUDE=attitude, UPDATEROLL=updateroll, RADEC=radec
;+
; $Id: scc_wcs_keywords.pro,v 1.8 2015/05/29 21:56:00 nathan Exp $
;
; NAME:	future_wcshdr
;
; PURPOSE:
;	Fill in WCS position keywords in SECCHI header structure from SPICE
;
; CATEGORY:  header fits wcs movie pipeline coordinates astrometry
;
; CALLING SEQUENCE:
;
; INPUTS:
;	SECCHI header structure for SECCHI image
;
; OPTIONAL INPUTS:
;	
; KEYWORD PARAMETERS: 
;   /ATTITUDE	Also derive and fill in pointing and roll values
;   /UPDATEROLL If /attitude set, use S/C roll from SPICE
;   /RADEC  	Also fill in header _____A values which are GEI or RA/Dec
;
; OUTPUTS:
;	Returns modified fhdr	
;
; RESTRICTIONS:
;
;   Requires following keywords filled in: detector, date_obs, obsrvtry 
;   Requires SPICE kernel for STEREO (or SOHO)
;
; PROCEDURE:
;
; EXAMPLE:
;-
; MODIFICATION HISTORY:
;
; $Log: scc_wcs_keywords.pro,v $
; Revision 1.8  2015/05/29 21:56:00  nathan
; always assume Earth if not known spacecraft (bug reported by C.StCyr)
;
; Revision 1.7  2013/07/23 16:13:10  mcnutt
; sets sc for mk4 to earthscc_wcs_keywords.pro
;
; Revision 1.6  2012/05/08 15:30:08  nathan
; make sure all values are nonzero
;
; Revision 1.5  2011/10/14 12:54:50  mcnutt
; sets sc SDO to earth, works with SDO/AIA
;
; Revision 1.4  2011/09/27 21:33:28  nathan
; do not add time_obs to date_obs
;
; Revision 1.3  2011/09/27 20:00:11  nathan
; fix spacecraft var; use exptime to set date_end, date_avg
;
; Revision 1.2  2011/09/26 18:04:07  nathan
; add /RADEC; fix scch.date
;
; Revision 1.1  2011/09/23 16:50:23  nathan
; for filling up wcs header, called from convert2secchi.pro
;
; Revision 1.1  2009/10/06 18:55:58  mcnutt
; creates a wcs header for some time in the future
;
 
;add SPICE 
spicetime=0
hpccmnt='check ATT_FILE for pointing source'
pointingtime=scch.date_obs  ;+' '+scch.time_obs
help,pointingtime
sc = scch.obsrvtry
;;IF  strpos(sc,'SDO') gt -1 or strpos(strlowcase(sc),'mk4') gt -1 THEN sc='Earth' ELSE $
IF  sc EQ 'SOHO' THEN load_soho_spice ELSE $
    sc = parse_stereo_name(sc, ['-234', '-235'])
if strupcase(sc) eq 'SOHO' then sc = '-21'

;  If not yet recognized, assume Earth

if not valid_num(sc) then sc='Earth'

    print,'Computing ephemeris info from SPICE...'

    adndm=''

ephemfil='NA'
IF strmid(sc,0,2) EQ 'ST' THEN EPHEMFIL    =get_stereo_spice_kernel(pointingtime,sc)
help,ephemfil
print,''
IF keyword_set(ENDBADAH) THEN endbadaht=utc2str(anytim2utc(endbadah),/noz) ELSE endbadaht='2000/01/01'

;time0=systime(1)
get_utc,now
scch.date=utc2str(now)
taiobs=anytim2tai(pointingtime)
scch.date_end=utc2str(tai2utc(taiobs+scch.exptime))
scch.date_avg=utc2str(tai2utc(taiobs+scch.exptime/2))
;print,'Calling get_stereo_lonlat ...'
hee_d=get_stereo_lonlat(pointingtime,sc, /meters, system='HEE',_extra=_extra)
;print,'Calling get_stereo_lonlat ...'
heeq=get_stereo_lonlat(pointingtime,sc, /degrees, system='HEEQ',_extra=_extra)
;print,'Calling get_stereo_coord ...'
coord=get_stereo_coord(pointingtime,sc , /meters ,_extra=_extra,ltime=sctime)

IF scch.RSUN LE 0 THEN scch.RSUN=(6.95508e8 * 648d3 / !dpi ) / hee_d(0)
scch.HCIX_OBS=coord(0)
scch.HCIY_OBS=coord(1)
scch.HCIZ_OBS=coord(2)
scch.DSUN_OBS=hee_d(0)
scch.HGLN_OBS=heeq(1)
scch.HGLT_OBS=heeq(2)
;print,'Calling get_stereo_hpc_point ...'
coord=get_stereo_coord(pointingtime,sc , /meters , system='HAE')
    scch.HAEX_OBS=coord(0)
    scch.HAEY_OBS=coord(1)
    scch.HAEZ_OBS=coord(2)
;print,'Calling get_stereo_hpc_point ...'
coord=get_stereo_coord(pointingtime,sc , /meters , system='HEE')
    scch.HEEX_OBS=coord(0)
    scch.HEEY_OBS=coord(1)>1e-07
    scch.HEEZ_OBS=coord(2)>1e-07
;print,'Calling get_stereo_hpc_point ...'
coord=get_stereo_coord(pointingtime,sc , /meters , system='HEEQ')
    scch.HEQX_OBS=coord(0)
    scch.HEQY_OBS=coord(1)>1e-07
    scch.HEQZ_OBS=coord(2)
    scch.EPHEMFIL=ephemfil		; in common scc_reduce


carlonlat=get_stereo_lonlat(pointingtime,sc,/au, /degrees, system='Carrington')
scch.CRLN_OBS=carlonlat(1)
IF scch.crln_obs LT 0 THEN scch.crln_obs = scch.crln_obs + 360
scch.CRLT_OBS=carlonlat(2)

carlonlat=get_stereo_lonlat(scch.date_obs,sc,/au, /degrees, system='Carrington')
scch.CRLN_OBS=carlonlat(1)
scch.CRLT_OBS=carlonlat(2)
heeq=get_stereo_lonlat(scch.date_obs,sc, /degrees, system='HEEQ',_extra=_extra)
scch.HGLN_OBS=heeq(1)
scch.HGLT_OBS=heeq(2)
hee_d=get_stereo_lonlat(scch.date_obs,sc, /meters, system='HEE',_extra=_extra)
scch.DSUN_OBS=hee_d(0)

IF keyword_set(ATTITUDE) THEN BEGIN
    IF strmid(sc,0,3) NE '-23' THEN BEGIN
    	message,'Sorry, /ATTITUDE only written for STEREO',/info
	return
    ENDIF
    atf=get_stereo_att_file(pointingtime,sc)
    IF atf EQ '' THEN att_file='NotFound' ELSE att_file=atf
    t=getscccrpix(scch,/UPDATE, FAST=fast, TOLERANCE=60, YPRSC=scp, _EXTRA=_extra)
    ;--Get pointing in helioprojective cartesian coordinates
    yprhpc=getsccpointing(scch, nospice=~keyword_set(UPDATEROLL), _EXTRA=_extra)

    IF (scch.detector ne 'EUVI') THEN BEGIN
	scch.CRVAL1	=yprhpc[0]
	scch.CRVAL2	=yprhpc[1]
    ENDIF	
    ; for EUVI contained in CRPIX and CRVAL=0

    IF keyword_set(UPDATEROLL) THEN BEGIN
	scch.CROTA	 =yprhpc[2]
	scch.SC_YAW	 =scp[0]
	scch.SC_PITCH=scp[1]
	scch.SC_ROLL =scp[2]
    ENDIF
    crota=scch.crota
    dr=!DPI/180.d
    scch.PC1_1= cos(crota*dr)
    scch.PC1_2=-sin(crota*dr) ;*(scch.CDELT1/scch.CDELT2) ;may be added later
    scch.PC2_1= sin(crota*dr) ;*(scch.CDELT1/scch.CDELT2)
    scch.PC2_2= cos(crota*dr)
ENDIF
IF keyword_set(RADEC) THEN BEGIN
    ;--Get pointing in R.A.-Dec coordinates
    cspice_str2et,pointingtime,et
    cspice_spkezr,'sun',et,'j2000','none',sc,state,ltime
    cspice_reclat,state[0:2],radius,ra,dec
    radec=[ra,dec]*!radeg
    print,'Ra,Dec = ',radec
    scch.CRVAL1A = yprrd[0]
    scch.CRVAL2A = yprrd[1]
    ;ENDIF

; Does this need a correction for EUVI? 
; this is set in getsccsecpix.pro
;scch.CDELT1A = -scch.CDELT1/divisor    ;Conversion from arcsec to degrees
;scch.CDELT2A =  scch.CDELT2/divisor    ;Conversion from arcsec to degrees
    scch.PC1_1A =  cos(yprrd[2]*dr)
    scch.PC1_2A = -sin(yprrd[2]*dr) ;* scch.CDELT2A / scch.CDELT1A
    scch.PC2_1A =  sin(yprrd[2]*dr) ;* scch.CDELT1A / scch.CDELT2A
    scch.PC2_2A =  cos(yprrd[2]*dr)
    scch.SC_YAWA	=scpa[0]
    scch.SC_PITA	=scpa[1]
    scch.SC_ROLLA	=scpa[2]

ENDIF

;spicetime=systime(1)-time0
;help,spicetime

END
