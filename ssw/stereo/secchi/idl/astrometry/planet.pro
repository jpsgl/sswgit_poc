PRO PLANET,target,date ; date in the form '7-May-2007'

; $Id: planet.pro,v 1.1 2008/03/10 14:59:30 nathan Exp $
;
; Calculates the angle (ang) between the the telescope
; axis (for A or B) and the target of interest.  Because A
; has a half fov of 35 deg, the target will be visible
; in A only if ang < 35 deg.  Likewise, the target will
; be visible in B only if ang < 10 deg.

; $Log: planet.pro,v $
; Revision 1.1  2008/03/10 14:59:30  nathan
; moved from dev/astrometry
;
; Revision 1.2  2007/11/16 21:24:24  nathan
; removed stop and <cr>s
;
;
; modified 09/05/07 by NRS from Bill Thompson's 12/15/06
; message to Simon Plunkett. 

utc = anytim2utc(date,/ext)
nn = 2881 ; 1440 minutes per day -> 2 days
utc=replicate(utc,nn)
utc.minute = utc.minute + lindgen(nn)
check_ext_time, utc
theta=13.28*!dtor & hi1=[cos(theta),0,-sin(theta)]
theta=53.36*!dtor & hi2=[cos(theta),0,-sin(theta)]
ma = get_stereo_coord(utc,'ahead',system='sci',target=target,/novel)
mb = get_stereo_coord(utc,'behind',system='sci',target=target,/novel)
;
hi1a = fltarr(nn)
hi2a = fltarr(nn)
hi1b = fltarr(nn)
hi2b = fltarr(nn)
for i=0,nn-1 do begin
    hi1a[i] = cspice_vsep(hi1,ma[*,i])
    hi2a[i] = cspice_vsep(hi2,ma[*,i])
    hi1b[i] = cspice_vsep(hi1,mb[*,i])
    hi2b[i] = cspice_vsep(hi2,mb[*,i])
endfor
hi1a = hi1a*!radeg
hi2a = hi2a*!radeg
hi1b = hi1b*!radeg
hi2b = hi2b*!radeg
linecolor, 1, 'red'
linecolor, 2, 'blue'
utplot,  utc, hi1a, xstyle=1, yrange=[0,90], color=1, line=2
outplot, utc, hi2a, color=1
outplot, utc, hi1b, color=2, line=2
outplot, utc, hi2b, color=2
oplot, !x.crange, [10, 10], line=2
oplot, !x.crange, [35, 35]
;
; hi2a & hi1a = red (solid & dotted)
; hi2b & hi1b = blue (solid & dotted)
;
end
