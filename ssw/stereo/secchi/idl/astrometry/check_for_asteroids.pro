PRO check_for_asteroids,aorb,tel,yyyymmdd1,yyyymmdd2,write=write,pipeline=pipeline
;+
; $Id: check_for_asteroids.pro,v 1.3 2013/06/13 11:46:26 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : check_for_asteroids
;               
; Purpose   : creates a list of asteroids in SECCHI HI images and pixel positions 
;               
; Explanation :	
;

; infile is a string that points to a SECCHI COR or HI FITS image
; EG IDL> check_for_asteroids,'a','hi1','20080829'
; Keywords   : 
;   /WRITE will write the results out to a file. Otherwise they just print in the terminal window
;   /PIPELINE is for the soul purpose of creating the text file for the sungrazer web site.
;
; $Log: check_for_asteroids.pro,v $
; Revision 1.3  2013/06/13 11:46:26  mcnutt
; changed output dir to SECCHI_PNG
;
; Revision 1.2  2012/10/19 10:23:33  mcnutt
; change output dir to /net/pluto/p2/secchi/asteroids/
;

if keyword_set(pipeline) then write = 1
if keyword_set(write) then write=1 else write=0

if strlowcase(tel) eq 'hi1' then tel='hi_1'
if strlowcase(tel) eq 'hi2' then tel='hi_2'

ut0=anytim2utc(strmid(yyyymmdd1,0,4)+'-'+strmid(yyyymmdd1,4,2)+'-'+strmid(yyyymmdd1,6,2))
if datatype(yyyymmdd2) ne 'UND' then ut1=anytim2utc(strmid(yyyymmdd2,0,4)+'-'+strmid(yyyymmdd2,4,2)+'-'+strmid(yyyymmdd2,6,2)) else ut1=ut0

;ast_names=readlist('/net/pluto/p2/secchi/asteroids/ast_names.txt')
ast_names=readlist(getenv('NRL_LIB')+'/dev/spice/ast_names.txt')

na=n_elements(ast_names)

mjd=ut0

;if datatype(aorb) eq 'UND' then aorb=getenv('sc') else aorb=strlowcase(aorb) ;aorb ;['a','b'] 

loaded=0
FOR i1=ut0.mjd,ut1.mjd DO BEGIN

    mjd.mjd=i1
    yyyymmdd=utc2yymmdd(mjd,/yyyy)

    infile=file_search(getenv('secchi')+'/lz/L0/'+strlowcase(aorb)+'/img/'+strlowcase(tel)+'/'+yyyymmdd+'/*s4h??.fts')
    infile=infile(0)
    ;if ~file_exist(infile) then infile=sccfindfits(infile)

    if file_exist(infile) then begin

    	im=sccreadfits(infile,h)

    	if (h.DETECTOR NE 'HI1') and (h.DETECTOR NE 'HI2') THEN sf=3600 ELSE sf=1
    	if (h.OBSRVTRY EQ 'STEREO_A') THEN loc='ahead' ELSE loc='behind'
    	xymax=h.NAXIS1 ; get image size

    	hi_calib_point,h ; should probably calibrate at some point


    	if write then begin
	    sc=strupcase(strmid(h.OBSRVTRY,7,1))
	    line=''
      	    fname=strcompress(h.DETECTOR,/r)+strcompress(sc,/r)+'_'+strmid(h.FILENAME,0,8)+'.txt'
            if keyword_set(pipeline) then begin
                dir=getenv('SECCHI_PNG')+'/asteroids/'+strcompress(h.DETECTOR,/r)+strcompress(sc,/r)+'/'+strmid(h.FILENAME,0,4)+'/'
                tmp=file_search(dir)
		if tmp(0) eq '' then spawn,'mkdir ' +dir
	    endif else dir=''
            fname=dir+fname
	    openw,lun,fname,/get_lun
            if keyword_set(pipeline) then begin
              line='<a href="http://sungrazer.nrl.navy.mil/index.php?p=asteroids/index.php&syear='+strmid(h.FILENAME,0,4)+'&stel='+strcompress(h.DETECTOR,/r)+strcompress(sc,/r)+'" target="_self">SECCHI Asteroids</a>'
	      printf,lun,line
              png=strmid(strcompress(h.FILENAME,/r),0,16)+'tbh'+strmid(strcompress(h.FILENAME,/r),019,2)+'.png'
              line='<a href="http://secchi.nrl.navy.mil/sccimages/HI_STARS_'+strupcase(sc)+'/'+yyyymmdd+'/1024/'+png+'" target="_blank">'+png+'</a>'	   
	      printf,lun,line
      	      printf,lun,''
            endif
	    line='VERY APPROXIMATE asteroid positions for '+strcompress(h.DETECTOR,/r)+' based on the file '+strcompress(h.FILENAME,/r)+':'
	    printf,lun,line
	    line='(Note: the pixel coordinate origin (0,0 location) is the lower-left corner) '
	    printf,lun,line
	    line=''
	    printf,lun,line
	    line='Asteroid designation/name:  X-position, Y-position'
	    printf,lun,line
	    line='----------------'
	    printf,lun,line
    	endif

   	if loaded eq 0 then begin
    	    ast=get_stereo_lonlat(h.date_obs, loc,system='hpc',target='Earth')  ; read coords for earth to avoid errors (loads stereo spice)	
;    	    cspice_furnsh,'/net/pluto/p2/secchi/asteroids/codes_300ast_20100725.bsp'  ;asteroids.bsp'
;    	    cspice_furnsh,'/net/pluto/p2/secchi/asteroids/codes_300ast_20100725.tf'  ;codes_300ast_20061020.tf'
    	    cspice_furnsh,getenv('NRL_LIB')+'/dev/spice/codes_300ast_20100725.bsp'  ;asteroids.bsp'
    	    cspice_furnsh,getenv('NRL_LIB')+'/dev/spice/codes_300ast_20100725.tf'  ;codes_300ast_20061020.tf'
            loaded=1
    	endif

    	wcs=fitshead2wcs(h) ; make wcs header

    	FOR i=0,na-1 DO BEGIN
	    ast=get_stereo_lonlat(h.date_obs, loc,system='hpc', /degrees, target=ast_names[i], corr='lt')	
	    pix=wcs_get_pixel(wcs,(sf*ast[1:2]))
	    IF ((pix[0] GT 0) and (pix[1] GT 0) and (pix[0] LT xymax) $
		and (pix[1] LT xymax) and (abs(ast[1]) lt 90)) THEN BEGIN
		line=ast_names[i]+':  '+strcompress(round(pix[0]),/r)+', '+strcompress(round(pix[1]),/r)
		PRINT,line
		if write then printf,lun,line
	    ENDIF
    	ENDFOR

    	if write then begin
	    line='----------------'
	    printf,lun,line
	    line='NOTE: Not all of these asteroids will be visible, and not all visible asteroids'
	    printf,lun,line
	    line='are recorded in this file.  This is just a guide, not an exhaustive list.'
	    printf,lun,line
	    line='Also note that for HI-1B, these positions have an error of up to +/- 10 pixels.'
	    printf,lun,line
            close,lun
	    free_lun, lun
    	endif

    endif

endfor

;cspice_unload,'/net/pluto/p2/secchi/asteroids/codes_300ast_20100725.bsp'  ;asteroids.bsp'
;cspice_unload,'/net/pluto/p2/secchi/asteroids/codes_300ast_20100725.tf'  ;codes_300ast_20061020.tf'
cspice_unload,getenv('NRL_LIB')+'/dev/spice/codes_300ast_20100725.bsp'  ;asteroids.bsp'
cspice_unload,getenv('NRL_LIB')+'/dev/spice/codes_300ast_20100725.tf'  ;codes_300ast_20061020.tf'

END


