function wcs2arc, wcs0, naxis0, suncen0, cdelt0
;+
; $Id: wcs2arc.pro,v 1.2 2010/12/09 21:28:10 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : wcs2arc
;               
; Purpose   : convert a wcs structure into a Helioprojective Radial - Polar Azimuthal Equidistant projection
;               
; Explanation: 
;               
; Use       : IDL> result= hpc2hpr( wcs, [nx,ny], [cx,cy], h.cdelt1)
;    
; Inputs    : 
;   	wcs 	WCS structure (assumes TIME and POSITION fields are present)
;
; Optional (recommended) inputs:
;   	naxis0	ARR(2)	Size of image that result is for
;   	suncen0 ARR(2)	Sun center for image that result is for, starting at 0 (IDL coords)
;   	cdelt0	FLOAT	Platescale of image that result is for in units of wcs.cunit
;
; OPtional inputs:
;               
; Outputs   : 
;   	WCS structure
;
; Keywords  : 
;               
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : astrometry wcs coordinates mapping 
;               
; Prev. Hist. : None.
;
; Written     : N.Rich, I2, 9/2010
;               
; $Log: wcs2arc.pro,v $
; Revision 1.2  2010/12/09 21:28:10  nathan
; correct suncen from crpix; dcdelt fudge factor
;
; Revision 1.1  2010/09/30 21:59:07  nathan
; called from scc_movie_win.pro
;
;-            

np=n_params()
naxis=[wcs0.naxis[0],wcs0.naxis[1]]
suncen=wcs0.crpix-1
cdelt=wcs0.cdelt[0]
IF np GT 1 THEN naxis=naxis0
IF np GT 2 THEN suncen=suncen0
IF np GT 3 THEN cdelt=cdelt0
dcdelt=0.013
; This is a fudge-factor based on slope of tangent of Position Angle (longitude) at CRPIX in HI2
wcs=wcs0
wcs.coord_type= 'Helioprojective-Radial'
wcs.wcsname=	'Helioprojective-Radial'
wcs.naxis=  	naxis
wcs.projection= 'ARC'
wcs.crpix=  	suncen+1
wcs.crval=  	[0,-90]
wcs.cdelt=  	[-1*abs(cdelt+dcdelt),abs(cdelt)]
wcs.roll_angle= 0
wcs.ctype=  	['HRLN-ARC', 'HRLT-ARC']
wcs.pc=     	[[1.,-0.],[0.,1.]]
wcs.proj_values[1]=0.

return,wcs

END
