;+
; $Id: get_att_dir.pro,v 1.5 2018/08/28 18:27:14 colaninno Exp $
;
; NAME:     get_att_dir
;
; PURPOSE: Gets directory path of raw level-0 telemetry for determination of GT jitter
;   	    (input for scc_sunvec.pro)
;
; CATEGORY: path packets gt attitude 
;
; CALLING SEQUENCE:
;       RESULT = get_att_dir('2007/03/02','a')
;
; INPUTS:   date    CDS structure or string
;   	    sc	    STRING  representing which STEREO spacecraft
;
; OUTPUT:   path
;
; Keywords: YEAR= returns string year YYYY
;
; RESTRICTIONS: Currently path is hardwired for UNIX and NRL      
;
; Calls : parse_stereo_name.pro
;
;--Created 2002-02-07, K.Baldwin
;-
; $Log: get_att_dir.pro,v $
; Revision 1.5  2018/08/28 18:27:14  colaninno
; updated for new dir structure
;
; Revision 1.4  2010/12/09 21:44:23  nathan
; change source directory logic
;
; Revision 1.3  2009/02/05 22:41:32  nathan
; added YEAR= output option
;
; Revision 1.2  2008/03/27 19:50:15  nathan
; added cvs tags, updated headers
;
function get_att_dir, date, sc, YEAR=yyyy
   
  d = datatype(date) 
  IF d eq 'STR' THEN BEGIN 
      str_date = date 
      date = anytim2utc(date)
  ENDIF ELSE str_date = utc2str(date)

  get_utc,now
  yyyy= strmid(str_date,0,4)
  mm = strmid(str_date,5,2)

  aorblc = parse_stereo_name(sc,['a','b'])
  IF aorblc eq 'a' THEN scname = 'ahead' ELSE scname = 'behind'
  
  sdspath=getenv('SDS_FIN_DIR')+'/'+sc+'/'+yyyy+'/'+mm
  IF ~file_exist(sdspath) or date.mjd GT now.mjd-45 THEN BEGIN 
     sdspath=getenv('SDS_DIR')+'/'+scname+$
             '/data_products/level_0_telemetry/secchi' 
  ENDIF

return, sdspath
end

