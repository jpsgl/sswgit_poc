function scc_update_hdr,im,hdr0,filename=filename,update_hdr_off=update_hdr_off,silent=silent,old_hi_names=old_hi_names,_extra=ex
;+
; $Id: scc_update_hdr.pro,v 1.18 2014/04/03 20:19:49 crothers Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_update_hdr
;               
; Purpose   : This function returns updated header structure for level
;             1 processing.
;               
; Explanation: This function updates the values of SECCHI header
;              keywords that change due to level 1 processing.
;               
; Use       : IDL> hdr = scc_update_hdr(im,hdr)
;    
; Inputs    : im - calibrated image
;             hdr -image header, SECCHI structure
;
; Outputs   : hdr - updated header
;
; Keywords  : filename - if set returns the level filename
;             missing - passes to SCC_IMG_STATS
;
; Calls     : SCC_IMG_STATS
;
; Category    : Administration, Calibration
;
; Written     : Robin C Colaninno NRL/GMU Oct 2006
;
; $Log: scc_update_hdr.pro,v $
; Revision 1.18  2014/04/03 20:19:49  crothers
; updated file name convention for HI level 1 files
;
; Revision 1.17  2012/06/26 21:46:53  nathan
; Implement HI special levchar case; try to standardize use of levchar=0;
; move CCDPIX in bunit to secchi_prep
;
; Revision 1.16  2012/04/10 17:52:08  nathan
; Set BUNIT=DN/s/CCDPIX if /NOCALFAC
;
; Revision 1.15  2011/12/27 19:12:15  nathan
; add datap50 to header
;
; Revision 1.14  2011/01/19 18:30:36  nathan
; fix exptime_off logic
;
; Revision 1.13  2010/11/15 18:43:28  nathan
; remove HI special case for BUNIT since get_calfac now returns MSB factor
;
; Revision 1.12  2007/11/07 23:18:21  nathan
; do not update filename if calfac off
;
; Revision 1.11  2007/09/24 20:59:56  nathan
; set BUNIT to DN or DN/s as appropriate
;
; Revision 1.10  2007/06/15 21:35:41  colaninn
; remove polar name change, fixed missing keyword, moved hdr_update_off to just off scc_img_stats
;
; Revision 1.9  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.8  2007/02/06 20:58:58  colaninn
; added structure check and corrected header
;
; Revision 1.7  2007/01/29 22:42:23  nathan
; put 2 in filename for /LEVEL_2
;
; Revision 1.6  2007/01/27 00:12:26  nathan
; remove Level-2 items to cor_polarize
;
; Revision 1.5  2007/01/18 20:43:03  colaninn
; added new values for level 2 data
;
; Revision 1.4  2007/01/08 20:10:21  colaninn
; fixed bug in hdr.fileaname
;
; Revision 1.3  2006/11/21 21:51:48  colaninn
; removed hdr.SUMMED update
;
; Revision 1.2  2006/11/21 21:21:04  colaninn
; add missing keyword
;
; Revision 1.1  2006/10/20 15:52:51  colaninn
; first entry
;
;-
;--Check Header-------------------------------------------------------
IF(DATATYPE(hdr0) NE 'STC') THEN hdr0=SCC_FITSHDR2STRUCT(hdr0) 
IF (TAG_NAMES(hdr0,/STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'


IF ~keyword_set(ex) THEN ex = create_struct('blank',-1)

hdr=hdr0    ; otherwise input will change to match output


;--Update Structure--------------------------
hdr.BSCALE= 1.0 
hdr.BZERO= 0.0

IF ~keyword_set(update_hdr_off) THEN BEGIN
  ;--Calculate Data Dependent Values
  SCC_IMG_STATS,im,mn,mx,zeros,nsat,mxval,mnval,nsatmin,men,sig,$
            percentile,_extra=ex
  hdr.DATAMIN= mn
  hdr.DATAMAX= mx
  hdr.DATAZER= zeros
  hdr.DATASAT= nsat
  hdr.DSATVAL= mxval
  hdr.DATAAVG= men
  hdr.DATASIG= sig
  hdr.DATAP01= percentile[0]
  hdr.DATAP10= percentile[1]
  hdr.DATAP25= percentile[2]
  hdr.DATAP50= percentile[3]
  hdr.DATAP75= percentile[4]
  hdr.DATAP90= percentile[5]
  hdr.DATAP95= percentile[6]
  hdr.DATAP98= percentile[7]
  hdr.DATAP99= percentile[8]
ENDIF ELSE IF ~keyword_set(silent) THEN $
  message,'Header Data Statistics not updated',/inform

;--Determine the value of BLANK Pixels

CASE 1 OF
  tag_exist(ex,'fill_mean') : blank=men
  tag_exist(ex,'fill_value') : blank=ex.fill_value
  ELSE : blank = 0
ENDCASE
hdr.BLANK=blank

;--Create Level1 FILENAME only if calfac and exptime correction and flat field applied
;  else use 0.
filename = hdr.filename

bunit='MSB' 	; default
levchar='1'
;--Determine the value of BUNIT

CASE hdr.DETECTOR OF
    'EUVI': IF ~tag_exist(ex,'dn2p_off') THEN bunit = 'PhotonFlux'
    'HI1' : levchar='b'
    'HI2' : levchar='b'
    ELSE:
ENDCASE

IF hdr.calfac EQ 1.0 THEN BEGIN
    bunit='DN/s'
    levchar='0'
ENDIF 
IF tag_exist(ex,'exptime_off') THEN IF (ex.exptime_off) THEN BEGIN
    IF hdr.calfac EQ 1. THEN bunit='DN' ELSE bunit='SB'
    levchar='0'
ENDIF
IF tag_exist(ex,'CALIMG_OFF') THEN IF (ex.calimg_off) THEN levchar='0'

strput, filename,levchar,16

if ~ keyword_set(old_hi_names) then begin
    if levchar eq 'b' then begin
        if hdr.detector eq 'HI1' or hdr.detector eq 'HI2' then begin
            strput,filename,'1',16
            strput,filename,levchar,17
        endif
    endif
endif

hdr.filename = filename

;IF tag_exist(ex,'do_polariz') THEN bunit='UNITLESS'

hdr.BUNIT = bunit

get_utc,date_mod,/ccsds,/noz
hdr.date	=date_mod

RETURN,hdr
END
