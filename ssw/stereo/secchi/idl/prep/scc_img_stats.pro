pro scc_img_stats,img,mn,mx,zeros,nsat,mxval,mnval,nsatmin,men,$
sig,percentile,medyen,bscale,SATMAX=satmax, SATMIN=satmin,verbose=verbose,missing=missing,_extra=ex
;+
;$Id: scc_img_stats.pro,v 1.11 2012/10/22 21:31:30 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name:	cor_REDUCE_STATISTICS
;
; Purpose: This procedure generates image statistics for the header.
;
; Explanation:
;              
;               
; Use       : IDl> SCC_IMG_STATS,image,mn,mx,zeros,nsat,mxval,$
;                        mnval,nsatmin,men,sig,percentile
;    
; Inputs    : image - 
;            
;               
; Outputs   : images statistics in the header
;             mn - Minimum Value Not Equal to Zero
;             mx - Maximum Value Less then Saturation
;             zeros - Number of Zero Pixels
;             nsat - Number of Saturated Pixels
;             mxval - Value used as saturated
;             mnval - Minimum value in scaled image
;             nsatmin - Number of pixels cut off on lower end
;             men - Mean of Image before BSCALE
;             sig - Standard Deviation of Image before BSCALE
;             percentile - Percentile Values [1%,10%,25%,50%,75%,90%,95%,98%,99%]
;
; Keywords  : SATMAX - set saturation value of image; default image maximum 
;             SATMIN - set minimum value of image; default image
;                      minimum gt 0      ;
;             missing - allows higher level programs to pass in index
;                       of missing pixels where the statistics should
;                       not be calucalated
;
; Written     : Robin C Colaninno NRL/GMU August 2006
;               
; $Log: scc_img_stats.pro,v $
; Revision 1.11  2012/10/22 21:31:30  nathan
; fix percentile if BLANK
;
; Revision 1.10  2011/12/27 19:11:51  nathan
; add datap50 to percentile output
;
; Revision 1.9  2007/06/25 16:18:18  colaninn
; set missing to NaN and added NaN keywords
;
; Revision 1.8  2007/06/15 21:33:56  colaninn
; updated to use missing keyword
;
; Revision 1.7  2007/04/12 21:21:33  thernis
; Moment needs at least 2 elements: again !
;
; Revision 1.6  2007/01/31 18:48:21  colaninn
; removed unneeded varibles
;
; Revision 1.5  2007/01/31 15:28:22  thernis
; Moment needs at least 2 elements
;
; Revision 1.4  2007/01/18 16:58:06  thernis
; Set stats to 0 and return instead of stop if the image is empty.
;
; Revision 1.3  2006/12/07 21:14:21  colaninn
; max value was wrong
;
; Revision 1.2  2006/11/21 21:20:32  colaninn
; added missing keyword
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.1  2006/09/14 13:44:08  colaninn
; moved from cor_stats.pro
;
; Revision 1.2  2006/08/04 19:44:54  colaninn
; re-ordered output to match hdr keywords
;
; Revision 1.1  2006/08/02 20:56:50  colaninn
; first entry
;
;-            
;--If keyword missing then don't calculate stats with these pixels
s =size(img)

IF keyword_set(missing) THEN BEGIN 
  zeros = n_elements(missing)
  IF zeros EQ s[4] THEN BEGIN
    message,'BLANK IMAGE',/info
    ;--If the image is blank set the stats to 0 and return
    mn=0.
    mx=0.
    zeros=0L
    nsat=0L
    mxval=0.
    men=0.
    sig=0.
    percentile=fltarr(9)
    RETURN
  ENDIF
  img[missing] = !Values.F_NaN
ENDIF ELSE BEGIN
  zeros = 0 
ENDELSE

;--Calculate Minimum Maximum
mn = MIN(img,MAX = mx,/NAN)

;--Calculate Maximum with Satuation Keyword
nsat = 0
IF keyword_set(SATMAX) THEN mxval = satmax ELSE mxval=mx
IF keyword_set(SATMIN) THEN mnval = satmin ELSE mnval=mn

;--Calculate Standard Dev and Mean
sig = stddev(img,/NAN)
men = mean(img,/NAN)

;-- Calculate Image Percentiles
limits = [0.01,0.10,0.25,0.5,0.75,0.90,0.95,0.98,0.99]
im_sort = img[sort(img)]
st = n_elements(im_sort)-zeros
limits = limits*st
percentile = im_sort[limits]

IF keyword_set(verbose) THEN BEGIN
print,'DATAMIN',float(mn),' Minimum Value Not Equal to Zero before BSCALE'
print,'DATAMAX',float(mx),' Maximum Value before BSCALE'
print,'DATAZER',zeros,	' Number of Zero Pixels'
print,'DATASAT',nsat,	' Number of Saturated Pixels'
print,'DSATVAL',mxval, 	' Value used as saturated'
print,'DATAAVG',men,   ' Mean of Image before BSCALE'
print,'DATASIG',sig,   ' Standard Deviation of Image before BSCALE'
print,'DATAP01',percentile[0]
print,'DATAP10',percentile[1]
print,'DATAP25',percentile[2]
print,'datap50',percentile[3]
print,'DATAP75',percentile[4]
print,'DATAP90',percentile[5]
print,'DATAP95',percentile[6]
print,'DATAP98',percentile[7]
print,'DATAP99',percentile[8]
ENDIF

RETURN
END
