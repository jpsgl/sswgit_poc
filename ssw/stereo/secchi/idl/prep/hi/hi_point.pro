pro hi_point,i,error=err,forceroll=forceroll,silent=silent
;+
; $Id: hi_point.pro,v 1.4 2007/06/19 17:22:23 nathan Exp $
;
; Project : STEREO SECCHI
;
; Name    : hi_point
;
; Purpose : update CDELTi & PCj_i, and calculate CRPIXi from SPICE
;
; Use     : IDL> hi_point,i,err=err
;
; Input/Output  :
;    i = FITS index structure.  Attitude related keywords are updated,
;        including .crpixi, .crvali, cdelti, .pci_j, .att_file (and others)
;
; Keywords input:
;			 /forceroll : Get roll info from SPICE, even if CRVAL1 is nonzero
;
; Keywords output:
;    error = byte.  0 if no error, otherwise nonzero error code:
;            7 : invalid observatory.  Must be STEREO_A or STEREO_B.
;            3 : error reading GT calibration file.
;            1 : outdated GT calibration data
;
; Common  :
;
; Restrictions:
;     1. Assumes that the image has been rectified, i.e., ecliptic N up!
;     2. Only for one image at a time (cannot use vector of input structures).
;
; Side effects:
;
; Category   : prep, calibration
;
; Prev. Hist.:

; Written    : 
;
; $Log: hi_point.pro,v $
; Revision 1.4  2007/06/19 17:22:23  nathan
; use date_end for get_stereo_lonlat
;
; Revision 1.3  2007/05/22 19:01:54  nathan
; fix typo in line 51
;
; Revision 1.2  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.1  2007/04/13 20:06:04  nathan
; obtain updated attitude info and put in header
;
;-

info="$Id: hi_point.pro,v 1.4 2007/06/19 17:22:23 nathan Exp $"

IF ~keyword_set(silent) THEN  message,info,/inform
len=strlen(info)
histinfo=strmid(info,1,len-2)

; valid observatory?
if tag_exist(i,'obsrvtry') then aorb = strmid(i.obsrvtry,7,1) $
                           else aorb = ''
if tag_exist(i,'detector') then dets = strupcase(strtrim(i.detector,2)) $
                           else dets = ''
err=0
if aorb EQ '' THEN err=7

platescl=getsccsecpix(i,/UPDATE, /FAST, _EXTRA=_extra)
; UPDATE keyword adds HISTORY, CDELT*, PV2_1 to scch
IF ~keyword_set(silent) THEN print,'CDELT: ',platescl
crpix=getscccrpix(i,/UPDATE, /FAST, _EXTRA=_extra)
IF ~keyword_set(silent) THEN print,'CRPIX: ',crpix

;add SPICE 
errmsg=''
att_file_spice = get_stereo_att_file(i.date_obs,aorb,errmsg=errmsg)  ; much faster
IF i.crval1 NE 0 THEN errmsg='CRVAL1 is nonzero.'
spicetime=0
hpccmnt=''
IF errmsg EQ '' or keyword_set(FORCEROLL) then begin

    time0=systime(1)

	;--Get pointing in helioprojective cartesian coordinates
	yprhpc=getsccpointing(i,/updateheader, TOLERANCE=60, YPRSC=scp, /FAST, FOUND=ff, _EXTRA=_extra)
	hpccmnt='Pointing info from SPICE, 1min. resolution'
        IF ~keyword_set(silent) THEN print,'YPR hpc = ',yprhpc
	;IF ~keyword_set(FAST) THEN wait,2
	
	i.CRVAL1	=yprhpc[0]
	i.CRVAL2	=yprhpc[1]
	
	i.CROTA	=yprhpc[2]
	dr=!DPI/180.d
	i.PC1_1= cos(yprhpc[2]*dr)
	i.PC1_2=-sin(yprhpc[2]*dr) ;*(i.CDELT1/i.CDELT2) ;may be added later
	i.PC2_1= sin(yprhpc[2]*dr) ;*(i.CDELT1/i.CDELT2)
	i.PC2_2= cos(yprhpc[2]*dr)
	i.SC_YAW	=scp[0]
	i.SC_PITCH	=scp[1]
	i.SC_ROLL	=scp[2]

	;--Get pointing in R.A.-Dec coordinates
	yprrd=getsccpointing(i,/RADEC, TOLERANCE=60, /FAST, FOUND=ff, _EXTRA=_extra)
	IF ~keyword_set(silent) THEN print,'YPR Ra,Dec = ',yprrd
	;IF ~keyword_set(FAST) THEN wait,2
    i.CRVAL1A = yprrd[0]
    i.CRVAL2A = yprrd[1]

    i.CDELT1A = -i.CDELT1    ;Conversion from arcsec to degrees
    i.CDELT2A =  i.CDELT2    ;Conversion from arcsec to degrees
    i.PC1_1A =  cos(yprrd[2]*dr)
    i.PC1_2A = -sin(yprrd[2]*dr) * i.CDELT2A / i.CDELT1A
    i.PC2_1A =  sin(yprrd[2]*dr) * i.CDELT1A / i.CDELT2A
    i.PC2_2A =  cos(yprrd[2]*dr)

    carlonlat=get_stereo_lonlat(i.date_end,aorb,/au, /degrees, system='Carrington')
    i.CRLN_OBS=carlonlat(1)
    i.CRLT_OBS=carlonlat(2)
    
	help,ff,att_file_spice
	i.ATT_FILE=att_file_spice
		
    spicetime=systime(1)-time0
    help,spicetime
	
	if(i.NAXIS1 gt 0 and i.NAXIS2 gt 0)then begin
	  WCS = FITSHEAD2WCS(i)
	  ; For RA/Dec, we need to specify system=A
	  XYCEN = WCS_GET_COORD(WCS, [(i.NAXIS1-1.)/2., (i.NAXIS2-1.)/2.])
	  i.XCEN=XYCEN[0]
	  i.YCEN=XYCEN[1]
	endif

endif else begin
	message,'!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!',/inform
	message,errmsg,/inform
	message,'HI header not updated.',/inform
	print
endelse    

if tag_exist(i,'history') then i=scc_update_history(i, histinfo)


end
