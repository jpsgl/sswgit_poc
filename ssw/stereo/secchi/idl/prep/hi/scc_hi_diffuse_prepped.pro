function scc_hi_diffuse_prepped,hdr
;+
;
; $Id: scc_hi_diffuse_prepped.pro,v 1.1 2013/10/22 14:17:35 crothers Exp $
;
; Project: STEREO-SECCHI
;
; Name: scc_hi_diffuse_prepped
;
; Purpose: Compute summing factor to use in scc_hi_diffuse to get correction factor for diffuse sources
;
; Category: STEREO, SECCHI, HI
;
; Explanation: Uses plate scale vs known plate scale to determine onboard summing
;
; Syntax: image=image*scc_hi_diffuse_prepped(hdr)
;
; Inputs: hdr - the secchi header for the input image
;
; Keywords: none
;
; Calls: scc_hi_diffuse
;
; Common: None
;
; Restrictions: None 
;
; Side effects: None
;
; Prev. Hist.: None
;
; History: Written 22 October 2013 Steve Crothers
;
; $Log: scc_hi_diffuse_prepped.pro,v $
; Revision 1.1  2013/10/22 14:17:35  crothers
; Diffuse source correction code
;
;
;-

; extract plate scale from header and from
; Brown, Bewsher & Eyles, Solar Phys (2009) 254: 185-335 table 2

if hdr.detector eq 'HI1' then begin
    if hdr.OBSRVTRY eq 'STEREO_A' then begin
        cdelt=35.96382/3600
    endif else if hdr.OBSRVTRY eq 'STEREO_B' then begin
        cdelt=35.89977/3600
    endif
endif else if hdr.detector eq 'HI2' then begin
    if hdr.OBSRVTRY eq 'STEREO_A' then begin
       cdelt=130.03175/3600
    endif else if hdr.OBSRVTRY eq 'STEREO_B' then begin
       cdelt=129.80319/3600
    endif
endif

; can compute summing factor by comparing header cdelt to known cdelt as powers of two

summing=nint(alog(hdr.cdelt1/cdelt)/alog(2.))+1

return,scc_hi_diffuse(hdr,ipsum=summing)

end
