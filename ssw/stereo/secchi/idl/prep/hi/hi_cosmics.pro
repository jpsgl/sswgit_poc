function hi_cosmics, hdr, img, _EXTRA=_extra

;+
; $Id: hi_cosmics.pro,v 1.6 2016/01/14 13:09:18 crothers Exp $
;
; Project   : STEREO SECCHI
;
; Name      : hi_cosmics
;
; Purpose   : This function extracts cosmic ray scrub reports from hi images.
;
; Explanation: The normal images are a sum of a series, one corner of the
;              image is replaced with a report from the image scrubber.
;              image data for the cosmic ray data is replaced with the
;              adjacent row.
;
; Use       : IDL> cosmics=hi_cosmics(hdr,im)
;
; Inputs    : image - level 0.5 image in DN (long)
;             hdr -image header, either fits or SECCHI structure
;
; Outputs   :  cosmics - cosmic ray scrub count
;
; Keywords  :
;
; Common    :
;
; Calls     : SCC_FITSHDR2STRUCT,
;
; Category    : Calibration
;
; Prev. Hist. : None.
;
;--Check Header-------------------------------------------------------
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF (TAG_NAMES(hdr, /STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE,'ONLY SECCHI HEADER STRUCTURE ALLOWED'

IF (hdr[0].DETECTOR NE 'HI1' AND hdr[0].DETECTOR NE 'HI2') THEN $
message,'Calibration for HI DETECTOR only'
;----------------------------------------------------------------------

if strpos(hdr.filename,'s4h') lt 0 then begin
; not summed on board
   cosmics=hdr.cosmics
endif else if hdr.n_images le 1 and hdr.imgseq le 1 then begin
   cosmics=hdr.cosmics
endif else begin
   count=hdr.imgseq+1

   inverted=0
   if hdr.rectify eq 'T' then begin
       if hdr.date_obs gt '2015-07-01T00:00:00' then begin
           inverted = hdr.OBSRVTRY eq 'STEREO_A'
       endif else begin
           inverted = hdr.OBSRVTRY eq 'STEREO_B'
       endelse
   endif
   if inverted then begin
   ; cosmic ray counts are at bottom row on left hand side
       cosmic_counter=img[count,0]
       if cosmic_counter eq count then begin
           cosmics=reverse(img[0:count-1,0])
   ; fill image from row above - use "count" because it includes counter field
           img[0:count,0]=img[0:count,1]
       endif else begin
   ; search for cosmic ray counter, should find index equal to count
   ; e.g. [12345, 22345, 22345, 3, 324444] indicates counter of 3
           seek=indgen(count)
           q=where(seek eq img[0:count-1,0],ctr)
           if ctr gt 0 then begin
    ; take the most optimistic value for count
               count=q[ctr-1]
               if count gt 1 then begin
                   cosmics=reverse(img[0:count-1,0])
                   img[0:count,0]=img[0:count,1]
     ; compare unique solations with possible alternatives
                   if ctr eq 1 then begin
                       message,'cosmic ray counter recovered',/inform
                   endif else begin
                       message,'cosmic ray counter possibly recovered',/inform
                   endelse
               endif else begin
     ; there would appear to be zero images in sum - are data missing?
                   if hdr.nmissing gt 0 then begin
                       catch,error_status
                       if error_status ne 0 then begin
                           miss=-1
                       endif else begin
                           miss=scc_get_missing(hdr,/silent)
                       endelse
                       if n_elements(miss) gt 0 then begin
                           if total(miss eq hdr.imgseq+1) gt 0 then begin
      ; cosmic ray field is missing
                               message,'cosmic ray report is missing',/inform
                           endif else begin
                               message,'cosmic ray report implies no images [missing blks?]',/inform
                           endelse
                       endif else begin
                           message,'cosmic ray report implies no images [missing blks?]',/inform
                       endelse
                   endif else begin
                       message,'cosmic ray report implies no images',/inform
                   endelse
                   cosmics= -1
               endelse
           endif else begin
               message,'cosmic ray counter not recovered',/inform
               cosmics= -1
           endelse
       endelse
   endif else begin
    ; cosmic counts in top left hand of image
       naxis1=hdr.naxis1
       cosmic_counter=img[naxis1-count-1,hdr.naxis2-1]
       if cosmic_counter eq count then begin
           cosmics=img[naxis1-count:naxis1-1,hdr.naxis2-1]
   ; fill image from row below - use "count" because it includes counter field
           img[naxis1-count-1:naxis1-1,hdr.naxis2-1]=img[naxis1-count-1:naxis1-1,hdr.naxis2-2]
       endif else begin
    ; search for cosmic ray counter, this time backwards
           seek=reverse(indgen(count))
           q=where(seek eq img[naxis1-count:naxis1-1,hdr.naxis2-1],ctr)
           if ctr gt 0 then begin
               count=seek[q[0]]
               if count gt 1 then begin
                   cosmics=img[naxis1-count:naxis1-1,hdr.naxis2-1]
                   img[naxis1-count-1:naxis1-1,hdr.naxis2-1]=img[naxis1-count-1:naxis1-1,hdr.naxis2-2]
               ; compare unique solations with possible alternatives
                   if ctr eq 1 then begin
                       message,'cosmic ray counter recovered',/inform
                   endif else begin
                       message,'cosmic ray counter possibly recovered',/inform
                   endelse
               endif else begin
     ; there would appear to be zero images in sum, are data missing
                   if hdr.nmissing gt 0 then begin
                       catch, error_status
                       if error_status ne 0 then begin
                           miss=0
                       endif else begin
                           miss=scc_get_missing(hdr,/silent)
                       endelse
                       catch,/cancel
                       if n_elements(miss) gt 0 then begin
                           if total((naxis1*hdr.naxis2-1-miss) eq hdr.imgseq+1) gt 0 then begin
      ; cosmic ray field is missing
                               message,'cosmic ray report is missing',/inform
                           endif else begin
                               message,'cosmic ray report implies no images [missing blks?]',/inform
                           endelse
                       endif else begin
                           message,'cosmic ray report implies no images [missing blks?]',/inform
                       endelse
                   endif else begin
                       message,'cosmic ray report implies no images',/inform
                   endelse
                   cosmics= -1
               endelse
           endif else begin
               message,'cosmic ray counter not recovered',/inform
               cosmics= -1
           endelse
       endelse
   endelse
endelse

return,cosmics

end
