pro hi_fill_missing, im, hdr
;+
; $Id: hi_fill_missing.pro,v 1.3 2012/06/26 21:39:25 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : hi_fill_missing
;               
; Purpose   : set missing block values sensibly
;               
; Explanation: Splitting image into hi/low words creates half missing
;               
; Use       : IDL> hi_fill_missing, im, hdr
;    
; Inputs    : im - level 0.5/1.0 image from sccreadfits
;             hdr -level 0.5/1.0 header from sccreadfits, SECCHI structure
;
; Outputs   : im - corrected image
;
; Calls     : 
;
; Category    : Administration, Calibration, Pipeline
;
; Written     : S.R.Crothers@rl.ac.uk
;
; $Log: hi_fill_missing.pro,v $
; Revision 1.3  2012/06/26 21:39:25  nathan
; add end
;
; Revision 1.2  2012/01/25 15:29:17  nathan
; workaround for nmissing bug
;
; Revision 1.1  2008/02/15 16:17:12  crothers
; Initial release, only NAN fill at this stage.
;
;-

on_error,2

if hdr.nmissing le 0 then return
IF strlen(hdr.misslist) LT 1 THEN BEGIN
    nmissing=hdr.nmissing
    misslist=hdr.misslist
    help,nmissing,misslist
    message,'NMISSING,MISSLIST mismatch; MISSING not filled',/info
    return
ENDIF
    


fields=scc_get_missing(hdr,/silent)

sd=size(im)
print,sd
if sd[0] ne 2 then return
case sd[3] of
   4: im[fields]=!values.f_nan
   5: im[fields]=!values.d_nan
   else: im[fields]=hdr.blank
endcase


end
