function hi_desmear, im, hdr, _EXTRA=ex,silent=silent
;+
; $Id: hi_desmear.pro,v 1.10 2015/12/01 14:43:21 crothers Exp $
;
; Project   : STEREO SECCHI
;
; Name      : hi_desmear
;
; Purpose   : This function removes smear caused by no shutter.
;
; Explanation: First compute the effective exposure time [time the ccd was
;        static, generate matrix with this in diagonal, the clear time
;        below and the read time above; invert and multiply by the image
;
; Use       : IDL> smear_free_image = hi_desmear(image,hdr)
;
; Inputs    : image - level 0.5 image in DN (long) - cosmic ray report removed [hi_cosmics]
;             hdr -image header, either fits or SECCHI structure
;                uses  EXPTIME - summed exposure time in images
;                      CLEARTIM - time to clear CCD
;                      LINE_CLR - time to clear single row on CCD
;                      LINE_RO -  time to readout single row on CCD
;                      RO_DELAY - delay between command and start of readout
;                      N_IMAGES - number of images summed
;                      IPSUM    - binning of image
;                      RECTIFY  - image rotation for B spacecraft
;                      DSTART[12], DSTOP[12] - imaged data location in array
;
;
; Outputs   :  smear_free_image - image convert from  DN (long) to DN/sec (float)
;
; Keywords  : none
;
; Common    :
;
; Calls     : SCC_FITSHDR2STRUCT, , GET_EXPTIME,
;
; Category    : Calibration
;
; Prev. Hist. : None.
;
; Written     : Steve Crothers, January 2007
;
; $Log: hi_desmear.pro,v $
; Revision 1.10  2015/12/01 14:43:21  crothers
; Modified code to correct matrix for inverted ahead after conjunction
;
; Revision 1.9  2013/10/17 22:25:26  nathan
; always use full image if square so works with subfields /full
;
; Revision 1.8  2007/09/04 17:01:21  crothers
; Removed cosmic ray count field code, now done in hi_cosmics
;
; Revision 1.7  2007/08/24 19:30:03  nathan
; updated info messages and hdr history
;
; Revision 1.6  2007/06/13 09:47:17  crothers
; uncomented matrix multiply
;
; Revision 1.5  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.4  2007/05/19 05:48:22  crothers
; changed underscan lests on DSTART to le to avoid crashing on bust headers
;
; Revision 1.3  2007/05/18 11:18:20  crothers
; Added option to access cosmic data arrays, changed shutterless correction to two alternives de_smear and exp_time
;
; Revision 1.2  2007/01/30 21:15:47  colaninn
; commented out image cropping
;
; Revision 1.1  2007/01/22 19:35:08  colaninn
; new from RAL
;
;
;-

compile_opt idl2

info="$Id: hi_desmear.pro,v 1.10 2015/12/01 14:43:21 crothers Exp $"
len=strlen(info)
version='Applied '+strmid(info,5,len-10)
IF ~keyword_set(silent) THEN message,version,/inform

;--Check Header-------------------------------------------------------
 IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF (tag_names(hdr, /STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
 message,'ONLY SECCHI HEADER STRUCTURE ALLOWED'

IF (hdr[0].DETECTOR NE 'HI1') AND (hdr[0].DETECTOR NE 'HI2') THEN $
message,'DeSmear for HI1 or HI2 DETECTOR only'
;----------------------------------------------------------------------

;--check valid values in header
if (hdr.CLEARTIM LT 0) THEN message,'CLEARTIM invalid'
if (hdr.RO_DELAY LT 0) THEN message,'RO_DELAY invalid'
if (hdr.LINE_CLR LT 0) THEN message,'LINE_CLR invalid'
if (hdr.LINE_RO LT 0) THEN message,'LINE_RO invalid'

;--Add file version to header history
hdr = SCC_UPDATE_HISTORY(hdr,version)
;get_utc,utc, /date_only,/ECS
;h_dex= 20-total(strmatch(hdr.HISTORY,''),1)
;hdr.HISTORY[h_dex]='Shutterless Correction: '+utc+' v.BETA'
;h_dex = h_dex+1

if hdr.date_obs gt '2015-07-01T00:00:00' then post_conj=1 else post_conj=0

img=float(im)

; extract image array if underscan present, this is not triming, the masked
; area not being exposed to light does not smear and should therefor not be corrected


if hdr.dstart1 le 1 OR (hdr.naxis1 EQ hdr.naxis2) then image=img else $
    image=img[hdr.dstart1-1:hdr.dstop1-1,hdr.dstart2-1:hdr.dstop2-1]

; Compute the inverted correction matrix

clearest=0.70d
exp_eff=hdr.EXPTIME+hdr.n_images*(clearest-hdr.CLEARTIM+hdr.RO_DELAY)

; need to weight the correction by the number of images [to match the total exptime]
; and the on board summing factor up a column

dataWeight=hdr.n_images*((2^(hdr.ipsum-1)))

; allow for flipped HI-B images by swapping coefficents

inverted=0

if hdr.rectify eq 'T' then begin
    if hdr.OBSRVTRY eq 'STEREO_B' then inverted=1
    if hdr.OBSRVTRY eq 'STEREO_A' and post_conj eq 1 then inverted=1
endif

if inverted eq 1 then begin
    fixup=sc_inverse(hdr.naxis2,exp_eff, $
                dataWeight*hdr.line_clr, $
                dataWeight*hdr.line_ro)
end else begin
    fixup=sc_inverse(hdr.naxis2,exp_eff, $
                dataWeight*hdr.line_ro, $
                dataWeight*hdr.line_clr)
end

; matrix multiply the smear array by the image to remove smear

image=(fixup##image)

; if underscan/overscan then patch the repaired image back into array
; leaving these areas untouched

if hdr.dstart1 le 1 OR (hdr.naxis1 EQ hdr.naxis2) then img = float(image) else $
   img[hdr.dstart1-1:hdr.dstop1-1,hdr.dstart2-1:hdr.dstop2-1]=float(image)

RETURN,img
END
