pro hi_prep, hdr,im,color_on=color_on,date_on=date_on,logo_on=logo_on, $
             cosmics=cosmics,_extra=ex,update_hdr_off=update_hdr_off, $
             fill_mean=fill_mean,fill_value=fill_value,smask_on=smask_on, $
             silent=silent, CALIBRATE_OFF=calibrate_off

;+
;$Id: hi_prep.pro,v 1.21 2013/10/28 18:27:00 nathan Exp $
;
; Project   : STEREO SECCHI
;
; Name      : hi_prep
;
; Purpose   : Processes a STEREO Heliospheric Imager (HI) image
;             to level 1.0
;de
; Explanation: The default correction procedure involves; correcting
;        for shutterless exposure and multiplying by the flatfield. This process
;        implicity performs conversion to DN/s. Procedure also adds
;        best calibrated pointing into HI header.
;
; Use      : IDL> hi_prep,hdr,im, cosmics=cosmics
;
; Inputs   : im - level 0.5 image in DN (long)
;            hdr -image header, either fits or SECCHI structure
;
; Outputs   : cosmics - cosmic ray count, from image for summed data, hdr otherwise
;
; Keywords  : DESMEAR_OFF - do not shutterless correct
;             EXPTME_OFF - do not correct for weighted exposure
;             UPDATE_HDR_OFF- if set, don't call SCC_UPDATE_HDR (saves time)
;             SMASK_ON - applied a smooth mask to the image
;             FILL_MEAN - if set then missing data and the mask are
;                         set to the mean of the images; the default
;                         is zero
;             FILL_VALUE - if set then missing data and the mask are
;                         set to the input value; the default
;                         is zero 
;
; Common    :
;
; Calls     : SCC_FITSHDR2STRUCT, HI_DESMEAR,
;             SCC_UPDATE_HDR
;
; Category    : Calibration
;
; Prev. Hist. : None.
;
; Written     : Steve Crothers, Jackie Davies, Chris Eyles, RAL/BHAM

;$Log: hi_prep.pro,v $
;Revision 1.21  2013/10/28 18:27:00  nathan
;add /CALIBRATE_OFF; fix /update_hdr_off check
;
;Revision 1.20  2008/10/14 18:23:35  nathan
;allow /silent in hi_fix_pointing call
;
;Revision 1.19  2008/08/11 15:10:12  crothers
;fixed typo
;
;Revision 1.18  2008/08/11 14:59:58  crothers
;Patch imgseq for hi-res images where it is not correct
;
;Revision 1.17  2008/07/31 13:48:48  bewsher
;Added call to hi_fix_pointing
;
;Revision 1.16  2008/01/16 23:19:18  thompson
;Don't call get_smask for HI1
;
;Revision 1.15  2008/01/16 23:05:51  thompson
;Added /smask support for HI2
;
;Revision 1.14  2007/12/11 13:18:04  bewsher
;Added call to hi_calib_point
;
;Revision 1.13  2007/09/04 17:13:33  crothers
;*** empty log message ***
;
;Revision 1.12  2007/08/29 11:38:59  crothers
;Exposed keyword cosmics in hi_correction
;
;Revision 1.11  2007/08/24 19:30:03  nathan
;updated info messages and hdr history
;
;Revision 1.10  2007/06/28 21:01:56  colaninn
;added on_error,2
;
;Revision 1.9  2007/05/21 20:16:06  colaninn
;added silent keyword
;
;Revision 1.8  2007/05/18 12:04:13  crothers
;Changed shutterless keyword to desmear and exptime, removed desmear application to under and overscan; added option to return cosmic ray count and patched cosmic ray count field before shutterless correction.
;
;Revision 1.7  2007/04/04 19:15:02  colaninn
;reversed output parameters
;
;Revision 1.6  2007/03/30 22:16:30  thompson
;Add keyword update_hdr_off
;
;Revision 1.5  2007/02/26 19:12:15  colaninn
;hi/hi_prep.pro
;
;Revision 1.4  2007/02/02 15:25:52  colaninn
;commented out image trimming
;
;Revision 1.3  2007/01/22 19:35:09  colaninn
;new from RAL
;
;
;-
ON_ERROR,2
IF datatype(ex) EQ 'UND' THEN ex = create_struct('blank',-1)
IF keyword_set(silent) THEN ex = CREATE_STRUCT('silent',1,ex)

;--Check Header-------------------------------------------------------
IF(datatype(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

IF (hdr[0].DETECTOR NE 'HI1' AND hdr[0].DETECTOR NE 'HI2' ) THEN $
 message,'Calibration for HI DETECTOR only'
;----------------------------------------------------------------------

;--Check Keywords
IF datatype(ex) EQ 'UND' THEN ex = create_struct('blank',-1)

;--Update IMGSEQ in the case of hi-res images where imgseq ne 0
;--bugzilla 322 - SteveCrothers

if hdr.naxis1 gt 1024 && hdr.imgseq ne 0 && hdr.n_images eq 1 then hdr.imgseq=0

;--endfix bugzilla 322

IF keyword_set(CALIBRATE_OFF) THEN cosmics=-1 ELSE BEGIN
    im=hi_correction(im,hdr,cosmics=cosmics,_extra=ex)

;Update header with best calibrated pointing
;hi_calib_point,hdr
    hi_fix_pointing,hdr, _EXTRA=ex
ENDELSE

;--Smooth Mask(OFF)  (HI2 only)
IF keyword_set(smask_on) AND ~keyword_set(calibrate_off) AND $
        (hdr[0].DETECTOR EQ 'HI2') THEN BEGIN 
  mask = get_smask(hdr)
  m_dex = where(mask EQ 0)
    CASE 1 OF
      keyword_set(fill_mean) : im[m_dex]=mean(im) 
      keyword_set(fill_value) : im[m_dex]=fill_value
      ELSE: im = im*mask
    ENDCASE
    IF ~keyword_set(SILENT) THEN message,'Mask applied',/info
ENDIF

;--Load Telescope Color Table (OFF)
IF keyword_set(color_on) THEN BEGIN
  path = concat_dir(getenv('SSW'),'stereo')
  color_table = strtrim(string(hdr.DETECTOR),2)+'_color.dat'
  RESTORE, FILEPATH(color_table ,ROOT_DIR=path,$
            SUBDIRECTORY=['secchi','data','color'])
  tvlct, r,g,b
ENDIF


;--Update Header to Level 1 values (ON) including FILENAME
if ~keyword_set(update_hdr_off) then hdr = SCC_UPDATE_HDR(im,hdr,_extra=ex)


;--Date and Logo Stamp (OFF)
IF keyword_set(date_on) THEN im = scc_add_datetime(im,hdr)

IF keyword_set(logo_on) THEN im = scc_add_logo(im,hdr)


;-----------------------------------------------------------------------------
RETURN
END

