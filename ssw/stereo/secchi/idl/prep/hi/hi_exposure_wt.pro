function hi_exposure_wt, hdr,silent=silent
;+
; $Id: hi_exposure_wt.pro,v 1.2 2007/05/21 20:16:06 colaninn Exp $
;
; Project   : STEREO SECCHI
;
; Name      : hi_exposure_wt
;
; Purpose   : This function returns a correction matrix to take account of
;             variable exposure times caused by having no shutter
;
; Explanation: The first rows read out have been exposed to light as
;              they run down the array for nrows * the line clear time
;              the last for nrows * the line readout time
;
; Use       : IDL> wt = hi_exposure_wt(hdr)
;
; Inputs    : hdr -image header, either fits or SECCHI structure
;
; Outputs   :  matrix to multiply image by to remove exposure weighting
;
; Category    : Calibration
;
; Prev. Hist. : None.
;
; Written     : Steve Crothers RAL May 2007
;
; $Log: hi_exposure_wt.pro,v $
; Revision 1.2  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.1  2007/05/18 11:16:12  crothers
; Initail release of alternative exposure correction to true desmearing
;
;
; new from RAL
;
;-
info="$Id: hi_exposure_wt.pro,v 1.2 2007/05/21 20:16:06 colaninn Exp $"
len=strlen(info)
version=strmid(info,1,len-2)
IF ~keyword_set(silent) THEN message,version,/inform

;--Check Header-------------------------------------------------------
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF (TAG_NAMES(hdr, /STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE,'ONLY SECCHI HEADER STRUCTURE ALLOWED'

IF (hdr[0].DETECTOR NE 'HI1' AND hdr[0].DETECTOR NE 'HI2') THEN $
message,'for HI DETECTOR only'
;----------------------------------------------------------------------

clearest=0.70d
exp_eff=hdr.EXPTIME+hdr.n_images*(clearest-hdr.CLEARTIM+hdr.RO_DELAY)

; need to weight the correction by the number of images [to match the total exptime]
; and the on board summing factor up a column

dataWeight=hdr.n_images*((2^(hdr.ipsum-1)))

wt0=indgen(hdr.naxis2)
wt1=reform(wt0,1,hdr.naxis2)
wt2=reform(reverse(wt0),1,hdr.naxis2)

if hdr.rectify eq 'T' AND hdr.OBSRVTRY eq 'STEREO_B' then begin
    IF ~keyword_set(silent) THEN message,"rectified",/inform
    wt=exp_eff + wt2*hdr.line_ro + wt1*hdr.line_clr
end else begin
    IF ~keyword_set(silent) THEN message,"normal",/inform
    wt=exp_eff + wt1*hdr.line_clr + wt2*hdr.line_ro
end

wt=rebin(wt,hdr.naxis1,hdr.naxis2)

RETURN,wt
END
