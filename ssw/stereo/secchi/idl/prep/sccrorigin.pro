function sccrorigin,scch,silent=silent
;+
; NAME:			SCCRORIGIN
; PURPOSE:
;	function to return rectified lower left (origin) value of full imaging area
; 
; CATEGORY:
; CALLING SEQUENCE:	coord = sccrorigin(scch)
;
; INPUTS:		scch = SECCHI header structure 
;
; KEYWORDs:		
;
; OUTPUTS:		coord = [r1col,r1row] of start of imaging area of CCD (FITS coord)
;
; OPTIONAL OUTPUT PARAMETERS:	
; COMMON BLOCKS:    
; SIDE EFFECTS:		
; RESTRICTIONS:
; PROCEDURE:
;
; Written     : Nathan Rich, NRL/I2, 1/18/08
;               
; Modified    :
;
; $Log: sccrorigin.pro,v $
; Revision 2.3  2011/08/29 16:27:09  nathan
; fix datatype issue
;
; Revision 2.2  2010/08/17 16:33:21  nathan
; LASCO/EIT case
;
; Revision 2.1  2008/01/22 17:24:13  nathan
; assumes all readouts begin on row 1
;
; Based on secchi_rectify.pro
; Revision 1.20  2007/11/27 15:58:06  mcnutt
;

info="$Id: sccrorigin.pro,v 2.3 2011/08/29 16:27:09 nathan Exp $"
len=strlen(info)
histinfo=strmid(info,1,len-2)

; For now, assume all readouts are the same and image area of readout begins on row 1
p1col=51
p1row=1
rectify=scch.rectify
IF datatype(rectify) NE 'STR' THEN rectify='T'
IF rectify EQ 'T' THEN BEGIN
  if (scch.OBSRVTRY eq 'STEREO_A') then begin
    case scch.detector of
      'EUVI':   begin
		r1col	=129
		r1row	=79
             end
      'COR1':   begin
		r1col	=1
		r1row	=79
             end
      'COR2':   begin
		r1col	=129
		r1row	=51
             end
      'HI1':   begin
      	    	r1col	=51
		r1row	=1
             end
      'HI2':   begin
      	    	r1col	=51
		r1row	=1
             end
     endcase
  endif ELSE $
  if (scch.OBSRVTRY eq 'STEREO_B') then begin
   case scch.detector of
      'EUVI':   begin
		r1col	=1
		r1row	=79
           end
      'COR1':   begin
		r1col	=129
		r1row	=51
             end
      'COR2':   begin
		r1col	=1
		r1row	=79
             end
      'HI1':   begin
		r1col	=79
		r1row	=129
             end
      'HI2':   begin
		r1col	=79
		r1row	=129
             end
   endcase
  endif ELSE BEGIN
    	; LASCO/EIT
    	r1col=20
    	r1row=1
    ENDELSE
endif else begin
    r1col=51
    r1row=1
endelse

return,([r1col,r1row])

end
