function get_calimg,hdr,fn,calimg_filename=calimg_filename, new_calimg=new_calimg,silent=silent
;+
; $Id: get_calimg.pro,v 1.22 2015/07/30 13:25:37 crothers Exp $
;
; Project   : STEREO SECCHI
;
; Name      : get_calimg
;
; Purpose   : returns calibration correction array
;
; Explanation: checks common block before opening calibration file
;	       saves calibration file to common block
;	       trims calibration array for  under/over scan
;	       re-scales calibration array for summing
;
; Use       : IDL> calimg = get_calimg(hdr)
;             IDL> calimg = get_calimg(hdr,/new_calimg)
;             IDL> calimg = get_calimg(hdr,calimg_filename=filename)
;
; Inputs    : hdr -image header FITS or SECCHI header structure
;
; Outputs   : calimg - floating point array
;
; Optional Output : fn - filename of the calibration image used
;
; Keywords  : NEW_CALIMG set the keyword to read the calibration image
;              from the file instead of the common block
;
;             CALIMG_FILENAME  Set this keyword to a filename if you
;             do not want to use the default calbration image.
;
; Restrictions : Assumes hdr is from trimmed image
;
; Calls     : SCC_FITSHDR2STRUCT, SCCREADFITS, SECCHI_RECTIFY
;
; Common    : SECCHI_CALIMG
;
; Category    : Calibration
;
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU June 2006
;
; $Log: get_calimg.pro,v $
; Revision 1.22  2015/07/30 13:25:37  crothers
; updated to link to new calimg from Tappin et al Solar Phys 2015
; clipped at factor 5 in extreem corners
;
; Revision 1.21  2015/07/17 17:26:13  thompson
; Rotate images by 180 degrees post-conjunction
;
; Revision 1.20  2014/07/23 16:48:40  colaninn
; updated COR2-B filename
;
; Revision 1.19  2010/06/07 14:23:08  crothers
; updated hi1 flatfields
;
; Revision 1.18  2009-07-23 22:58:05  thompson
; New COR1 flat field files, adjusted for scattered light when door closed.
;
; Revision 1.17  2008/01/30 16:51:01  crothers
; Update flat field for hi summed images
;
; Revision 1.16  2007/10/17 16:31:58  colaninn
; changed calimg trim again
;
; Revision 1.15  2007/10/04 20:21:06  thompson
; Corrected path specification
;
; Revision 1.14  2007/10/04 19:48:33  colaninn
; changed cor1 version;changed calimg trim
;
; Revision 1.13  2007/08/28 19:08:08  colaninn
; change filename creation so bkg image can be prepped
; corrected value of sum for HI images
;
; Revision 1.12  2007/08/23 23:54:31  nathan
; add info messages
;
; Revision 1.11  2007/05/21 20:16:05  colaninn
; added silent keyword
;
; Revision 1.10  2007/02/05 20:04:38  colaninn
; changed calibration image rectification
;
; Revision 1.9  2007/01/24 21:15:57  colaninn
; add code to handle HI summed calibration image
;
; Revision 1.8  2007/01/22 19:21:36  colaninn
; added HI call
;
; Revision 1.7  2006/12/05 21:04:57  colaninn
; corrected hdr.summed code
;
; Revision 1.6  2006/11/27 19:50:00  colaninn
; changed output text
;
; Revision 1.5  2006/11/21 21:22:00  colaninn
; updated output
;
; Revision 1.4  2006/11/09 16:23:45  colaninn
; corrected rescaling, rectify chdr, and trim
;
; Revision 1.3  2006/10/24 20:31:29  colaninn
; corrected code for rectify images
;
; Revision 1.2  2006/10/06 14:25:43  colaninn
; removed hard coded delimintors
;
;-
;--There is a math error caused by SECCHI_RECTIFY when trying to
;--update the incomplete calimg headers
!EXCEPT = 0

COMMON secchi_calimg, cal_image,cal_hdr, cal_filename

;*************************************
;VERSION NAMING CONVENTION - yyyymmdd_type
;  yyyymmdd = date image was created
;  type = type of calibration image
;  cal_version = ['20060727_flatfd','20060727_vigtest']
;*************************************

IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF (TAG_NAMES(hdr, /STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
message,'ONLY SECCHI HEADER STRUCTURE ALLOWED'

;--Flag to reopen calibration image
new_flag = 1
HIsum_flag = 0

;--Create calibration image filename
;++++++++++++++++++++++++++++++++++++++++++++++
path = getenv_slash('SECCHI_CAL')

CASE hdr.DETECTOR OF
  'COR1': BEGIN
    cal_version = '20090723_flatfd'
    tail = '_fCc1'+strupcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
  END

  'COR2': BEGIN
    IF hdr.OBSRVTRY EQ 'STEREO_A' THEN cal_version = '20060929_vignet' $
      ELSE cal_version = '20140723_vignet'
    tail = '_vCc2'+strupcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
  END

  'EUVI': BEGIN
    cal_version = '20060823_wav'
    wave = strtrim(string(hdr[0].WAVELNTH),2)
    tail = wave+'_fCeu'+strupcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
  END

  'HI1': BEGIN
    IF hdr.summed EQ 1 THEN BEGIN
       cal_version = '20061129_flatfld'
       tail = '_raw_h1'+strlowcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
    ENDIF ELSE BEGIN
       cal_version = '20100421_flatfld'
       tail = '_sum_h1'+strlowcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
        HIsum_flag = 1
    ENDELSE
  END

  'HI2': BEGIN

    IF hdr.summed EQ 1 THEN BEGIN
       cal_version = '20150701_flatfld'
       tail = '_raw_h2'+strlowcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
    ENDIF ELSE BEGIN
        cal_version = '20150701_flatfld'
        tail = '_sum_h2'+strlowcase(strmid(hdr[0].OBSRVTRY,7,1))+'.fts'
        HIsum_flag = 1
    ENDELSE
  END

  ELSE: message,'DETECTOR could not be found'
ENDCASE

filename = path+cal_version+tail

IF keyword_set(calimg_filename) THEN filename = calimg_filename
;++++++++++++++++++++++++++++++++++++++++++++++

;--Check Common Block
IF (datatype(cal_image) NE 'UND') THEN BEGIN
  IF(filename EQ cal_filename) THEN new_flag = 0
ENDIF

;--Check keyword
IF keyword_set(new_calimg) THEN new_flag = 1

;--Open Calibration image from memory
IF new_flag THEN BEGIN
  check = file_search(filename,count=count)
  IF count GT 0 THEN BEGIN
    IF ~keyword_set(silent) THEN $
      message, 'OPENING NEW CALIBRATION IMAGE',/inform
    cal_image = SCCREADFITS(filename,cal_hdr,/silent)
    cal_filename = filename
  ENDIF ELSE message,'CANNOT FIND CALIBRATION IMAGE: '+filename
ENDIF ELSE BEGIN
   IF ~keyword_set(silent) THEN $
     message,'USING CALIBRATION IMAGE FROM COMMON BLOCK',/inform
ENDELSE
IF ~keyword_set(silent) THEN message,filename,/inform

;--Trim calibration image to CCD coordinates
IF cal_hdr.P1COL LE 1 THEN BEGIN
  IF HIsum_flag THEN BEGIN
    x1 = 25    ;(hdr.P1COL-1)/2 ********HACK********
    x2 = 1048  ;(hdr.P2COL-1)/2  data problem in pipeline
    y1 = 0     ;(hdr.P1ROW-1)/2
    y2 = 1023  ;hdr.P2ROW-1)/2
    IF ~keyword_set(silent) THEN $
     message, 'using fixed CCD coordinates',/inform
  ENDIF ELSE BEGIN
    x1 = 50
    x2 = 2047+50
    y1 = 0
    y2 = 2047
  ENDELSE
 cal = cal_image[x1:x2,y1:y2]
ENDIF ELSE cal = cal_image

;--Correct calibration image for rectification
IF (hdr.RECTIFY EQ 'T') AND (cal_hdr.RECTIFY EQ 'F') THEN BEGIN
  chdr = cal_hdr
  cal = SECCHI_RECTIFY(cal,chdr,silent=silent)
  IF ~keyword_set(silent) THEN message,'RECTIFIED CALIBRATION IMAGE',/info
ENDIF

;--Correct calibration image for rescale
IF HIsum_flag THEN BEGIN
  IF hdr.SUMMED LE 2 THEN sum = 1 ELSE sum = 2^(hdr.SUMMED-2)
ENDIF ELSE sum = 2^(hdr.SUMMED-1)

s= size(cal,/dim)
cal = rebin(cal,s[0]/sum, s[1]/sum)

;--Filename of image returned
del = get_delim()
fn = strsplit(filename,del,/extract,count=last)
fn = fn[last-1]

if (hdr.date_obs ge '2015-05-19') then cal = rotate(cal, 2) ;post-conjuction

RETURN, cal
END
