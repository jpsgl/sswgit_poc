function scc_sebip, img, hdr, flag, SILENT=silent, _EXTRA=_extra
;+
; $Id: scc_sebip.pro,v 1.16 2010/11/19 18:39:20 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_sebip
;               
; Purpose   : This program returns the image corrected for any SEB
;             image processing.
;               
; Explanation:The on-board software can be set to take the square root
;             of the image or divide the image by a factor of 2, 3, 
;             or 4 multiple times. 
;               
; Use       : IDL> im_corrected = scc_sebip(im, hdr)
;    
; Inputs    : im - SECCHI image
;             hdr - image header, either fits or SECCHI structure
;               
; Outputs   : im_corrected - corrected image (float)
;              
; Optional Output : flag - returns 1 if any correction was applied; else 0
;
; Common    :                
;               
; Calls     : SCC_FITSHDR2STRUCT
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU September 2006
;               
; $Log: scc_sebip.pro,v $
; Revision 1.16  2010/11/19 18:39:20  nathan
; bug workaround for count53
;
; Revision 1.15  2009/08/17 19:48:14  nathan
; added HI?SPW correction (FSW 5.13)
;
; Revision 1.14  2008/01/31 16:17:07  colaninn
; added IP 82-88
;
; Revision 1.13  2007/09/25 15:46:03  nathan
; added _EXTRA keyword
;
; Revision 1.12  2007/09/20 20:49:34  nathan
; add message info about number of times correction was applied
;
; Revision 1.11  2007/08/24 19:30:49  nathan
; updated info messages and hdr history
;
; Revision 1.10  2007/08/24 14:42:35  nathan
; add IP function descriptions
;
; Revision 1.9  2007/04/12 14:14:25  colaninn
; refixed placement of SW IP trim
;
; Revision 1.8  2007/04/11 20:36:28  nathan
; fix placement of SW IP trim
;
; Revision 1.5  2007/02/02 15:24:29  colaninn
; corrected flag output for DIV2CORR
;
; Revision 1.4  2007/02/01 20:33:49  colaninn
; added check for DIV2CORR
;
; Revision 1.3  2007/01/31 23:26:55  nathan
; refine obtaining seb_ip from ip_00_19
;
; Revision 1.2  2007/01/31 18:54:29  colaninn
; correted header
;
 ; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.1  2006/09/22 18:30:31  colaninn
; created from inline code
;
;- 

IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr) 
IF (TAG_NAMES(hdr,/STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'
           
im = img
flag = 0

ip = hdr.ip_00_19
;--Fix length of ip string
IF strlen(ip) LT 60 THEN ip=' '+ip      ; ip should be 3x20 char long, but
IF strlen(ip) LT 60 THEN ip=' '+ip      ; could be as short as 58 if trimmed

seb_ip = fix(string(reform(byte(ip),3,20)))

;--Trim SW Images
x = where(seb_ip eq 117,cnt)
IF cnt EQ 1 THEN BEGIN
  ip = strmid(ip,[3*x],60)
  IF strlen(ip) LT 60 THEN repeat ip = ip +'  0' until strlen(ip) GE 59
  hdr.ip_00_19 = ip
ENDIF

;--Separate into array
seb_ip = fix(string(reform(byte(ip),3,20)))

;--Search for IP numbers [1,2,50,53,118]
xxx = where(seb_ip eq 1, count1)    	; Divide by 2
xxx = where(seb_ip eq 2, count2)    	; Square Root
xxx = where(seb_ip eq 16 or seb_ip EQ 17, countspw)      ; Divide by 64, HI?SPW
xxx = where(seb_ip eq 50, count50)  	; Divide by 4
xxx = where(seb_ip eq 53, count53)  	; Pixel Sum+Divide by 4
xxx = where(seb_ip eq 82, count82)      ; Divide by 2  
xxx = where(seb_ip eq 83, count83)      ; Divide by 4 
xxx = where(seb_ip eq 84, count84)      ; Divide by 8
xxx = where(seb_ip eq 85, count85)      ; Divide by 16 
xxx = where(seb_ip eq 86, count86)      ; Divide by 32
xxx = where(seb_ip eq 87, count87)      ; Divide by 64
xxx = where(seb_ip eq 88, count88)      ; Divide by 128
xxx = where(seb_ip eq 118, count118)	; Divide by 3

;--Remove 1 count for ICER correction
IF hdr.DIV2CORR EQ 'T' THEN count1 = count1-1
IF count1 LT 0 THEN count1 = 0

;--Apply correction
IF count1 GT 0 THEN BEGIN
  im = temporary(im)*(2.0^count1)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 2 x'+trim(count1),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 2 x'+trim(count1)) 
ENDIF
IF count2 GT 0 THEN BEGIN
  im = temporary(im)^(2.0^count2)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Square Root x'+trim(count2),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Square Root x'+trim(count2))
ENDIF
IF countspw GT 0 THEN BEGIN
  im = temporary(im)*(64.0^countspw)
  IF ~keyword_set(SILENT) THEN message,'Corrected for HI?SPW Divide by 64 x'+trim(countspw),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for HI?SPW Divide by 64 x'+trim(countspw)) 
ENDIF
IF count50 GT 0 THEN BEGIN
  im = temporary(im)*(4.0^count50)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 4 x'+trim(count50),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 4 x'+trim(count50)) 
ENDIF
IF count53 GT 0 THEN IF hdr.ipsum GT 1  THEN BEGIN
; account for imagetbl.img1.130 line 10 bug, see for example 20090306_080439_s4c1A.fts
; this will catch it ONLY of get_calfac has not been called yet.
    im = temporary(im)*(4.0^count53)
    IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 4 x'+trim(count53),/info
    hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 4 x'+trim(count53)) 
ENDIF
IF count82 GT 0 THEN BEGIN
 im = temporary(im)*(2.0^count82)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 2 x'+trim(count82),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 2 x'+trim(count82)) 
ENDIF
IF count83 GT 0 THEN BEGIN
  im = temporary(im)*(4.0^count83)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 4 x'+trim(count83),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 4 x'+trim(count83)) 
ENDIF
IF count84 GT 0 THEN BEGIN
  im = temporary(im)*(8.0^count84)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 8 x'+trim(count84),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 8 x'+trim(count84)) 
ENDIF
IF count85 GT 0 THEN BEGIN
  im = temporary(im)*(16.0^count85)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 16 x'+trim(count85),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 16 x'+trim(count85)) 
ENDIF
IF count86 GT 0 THEN BEGIN
  im = temporary(im)*(32.0^count86)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 32 x'+trim(count86),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 32 x'+trim(count86)) 
ENDIF
IF count87 GT 0 THEN BEGIN
  im = temporary(im)*(64.0^count87)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 64 x'+trim(count87),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 64 x'+trim(count87)) 
ENDIF
IF count88 GT 0 THEN BEGIN
  im = temporary(im)*(128.0^count88)
  IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 128 x'+trim(count88),/info
  hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 128 x'+trim(count88)) 
ENDIF
IF count118 GT 0 THEN BEGIN
 im =temporary(im)*(3.0^count118)
 IF ~keyword_set(SILENT) THEN message,'Corrected for Divide by 3 x'+trim(count118),/info
 hdr = SCC_UPDATE_HISTORY(hdr,'seb_ip Corrected for Divide by 3 x'+trim(count118)) 
ENDIF

IF (count1+count2+count50+count53+count82+count83+$
count84+count85+count86+count87+count88+count118) GE 1 THEN flag = 1

RETURN, im
END
