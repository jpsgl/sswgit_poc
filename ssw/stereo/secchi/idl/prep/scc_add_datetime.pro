function scc_add_datetime,image,hdr,circle=circle,color=color, VIDEO=video, $
    	LABELPOS=labelpos, ADDCAM=addcam, MVI=mvi
;+
; $Id: scc_add_datetime.pro,v 1.15 2012/08/22 18:00:33 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_add_datetime
;               
; Purpose   : This program writes thick vector font scaled to the
;             image size. The stamp will be the max image value.
;               
; Use       : IDL> out = scc_add_datetime(im,hdr)
;    
; Inputs    : im - image array to add datetime stamp to
;             hdr - image header either fits or SECCHI structure
;               
; Outputs   : out - image with datetime stamp included        
;
; Keywords  : 
;   	CIRCLE= Thickness of circle drawn at solar limb
;   	COLOR=	Specify color to use in chars; defaults to max of image
;   	LABELPOS=[x,y]	Specify location of chars
;   	/ADDCAM     Print detector and spacecraft above timestamp
;   	MVI=	    Use helvetica bold if > 2 else use device font
;               
; Calls     : SCC_FITSHDR2STRUCT
;               
; Category    : display movie image annotation
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU JAN 2007
;               
; $Log: scc_add_datetime.pro,v $
; Revision 1.15  2012/08/22 18:00:33  mcnutt
; limit bckgrnd color to < im size
;
; Revision 1.14  2010/08/17 16:33:59  nathan
; accomodate non-secchi hdrs
;
; Revision 1.13  2009/06/17 20:46:49  nathan
; use measured values for size of 256 and 1024 datetime; put ceiling on bkgcolor
;
; Revision 1.12  2009/03/30 11:51:57  mcnutt
; allows large times for daily pretties 256
;
; Revision 1.11  2009/03/25 17:28:20  nathan
; open z-buffer before defining font so visible window is not opened
;
; Revision 1.10  2008/09/26 19:28:47  nathan
; if /mvi, mask area behind datetime
;
; Revision 1.9  2008/09/23 15:37:31  nathan
; added options for use be scc_mkframe and MVI
;
; Revision 1.8  2007/07/11 21:29:50  colaninn
; added color keyword
;
; Revision 1.7  2007/07/05 19:26:16  colaninn
; added circle keyword
;
; Revision 1.6  2007/05/22 17:16:27  colaninn
; added spacecraft letter
;
; Revision 1.5  2007/03/30 19:52:51  colaninn
; left a help in this time
;
; Revision 1.4  2007/03/30 19:47:56  colaninn
; made change to pretty-up pretties
;
; Revision 1.3  2007/02/26 19:06:28  colaninn
; changed text size
;
; Revision 1.2  2007/01/31 18:32:43  colaninn
; corrected font resizing
;
; Revision 1.1  2007/01/17 15:32:27  colaninn
; created from RTMVIXY.pro
;
;-            ;

IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
;IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
;MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

im = image
s=size(image)

;--Make date-time string
IF strlen(hdr.date_obs) LT 12 THEN $	; MVI frame header
    str = hdr.date_obs             +' '+strmid(hdr.time_obs,0,8) ELSE $
    str = STRMID(hdr.date_obs,0,10)+' '+strmid(hdr.date_obs,11,8)
strput,str,'/',4
strput,str,'/',7

aorb =rstrmid(hdr.filename,4,1)
IF aorb EQ 'A' or aorb EQ 'B' THEN spc='-'+aorb ELSE spc=''
IF hdr.wavelnth GT 0 THEN wvl=' '+trim(hdr.wavelnth)+'A' ELSE wvl=''
;str = str+' '+sc
IF keyword_set(ADDCAM) THEN camt = hdr.detector+spc+wvl+' ' ELSE camt=''

col = 255
sum = 2048/s[1]
xp=10
yp=10
fontnum=0   ; use device font
csize=1

;--Open and clear 'Z' buffer
set_plot,'z'
device,set_resolution=[s[1],s[2]]
erase

IF keyword_set(color) THEN charcolor = color $
 ELSE charcolor = max(image)

;--Settings for MVI frames
IF keyword_set(VIDEO) THEN BEGIN
    xp=36    	;-- location of timestamp
    yp=34 	;+ (ny-480)/2.
    sum=8
ENDIF
IF keyword_set(MVI) THEN BEGIN
    xp=10
    yp=10
    sum=0
    IF mvi GT 2 THEN fontnum=1
    csize=mvi
    device,set_font='Helvetica Bold',/tt_font
    ;--Mask area behind datetime for readability
    stlens=[0,149,300,318,404,505,606]	;*s(1)/1024
    sthts =[0,  9, 18, 22, 31, 39, 47]	;*s(1)/1024
    stlen=stlens[mvi]
    stht =sthts[mvi]
    ;IF mvi GT 2 THEN stlen=fix(csize*5*strlen(str)) else stlen=fix(csize*8*strlen(str))
    ;stht=fix(csize*10)
    sz=size(im)
    bkgcol=median(im[xp-1:xp+stlen<sz(1)-1,yp-1:yp+stht<sz(2)-1])<(charcolor-50)
    help,bkgcol
    im[xp-1:xp+stlen<sz(1)-1,yp-1:yp+stht<sz(2)-1]=bkgcol

ENDIF
IF keyword_set(LABELPOS) THEN BEGIN
    xp=labelpos[0]
    yp=labelpos[1]
ENDIF


;--Draw vector characters for each image size 

CASE sum OF 
  1: BEGIN
     csize = 8.0
     ;xp = 10
     ;yp = 10
     cyouts=yp+ csize*10 + 5
     xyouts, xp,yp, 	/device,str, charsize=csize, color=col
     xyouts, xp,yp+1,	/device,str, charsize=csize, color=col
     xyouts, xp+1,yp,	/device,str, charsize=csize, color=col
     xyouts, xp+1,yp+1, /device,str, charsize=csize, color=col
     xyouts, xp,yp+2,	/device,str, charsize=csize, color=col
     xyouts, xp+1,yp+2, /device,str, charsize=csize, color=col
     xyouts, xp+2,yp,	/device,str, charsize=csize, color=col
     xyouts, xp+2,yp+1, /device,str, charsize=csize, color=col
     xyouts, xp+2,yp+2, /device,str, charsize=csize, color=col
     ;
     xyouts, xp,cyouts,     /device,camt, charsize=csize, color=col
     xyouts, xp,cyouts+1,   /device,camt, charsize=csize, color=col
     xyouts, xp+1,cyouts,   /device,camt, charsize=csize, color=col
     xyouts, xp+1,cyouts+1, /device,camt, charsize=csize, color=col
     xyouts, xp,cyouts+2,   /device,camt, charsize=csize, color=col
     xyouts, xp+1,cyouts+2, /device,camt, charsize=csize, color=col
     xyouts, xp+2,cyouts,   /device,camt, charsize=csize, color=col
     xyouts, xp+2,cyouts+1, /device,camt, charsize=csize, color=col
     xyouts, xp+2,cyouts+2, /device,camt, charsize=csize, color=col
  END
  2: BEGIN
     csize = 5.0
     ;xp = 5
     ;yp = 5
     cyouts=yp+ csize*10 + 5
     xyouts, xp,yp, 	/device,str, charsize=csize, color=col
     xyouts, xp,yp+1,	/device,str, charsize=csize, color=col
     xyouts, xp+1,yp,	/device,str, charsize=csize, color=col
     xyouts, xp+1,yp+1, /device,str, charsize=csize, color=col
     ;
     xyouts, xp,cyouts,     /device,camt, charsize=csize, color=col
     xyouts, xp,cyouts+1,   /device,camt, charsize=csize, color=col
     xyouts, xp+1,cyouts,   /device,camt, charsize=csize, color=col
     xyouts, xp+1,cyouts+1, /device,camt, charsize=csize, color=col
  END
  4: BEGIN
     csize = 3.0
     ;xp = 2
     ;yp = 2
     cyouts=yp+ csize*10 + 5
     xyouts, xp,yp, 	/device,str, charsize=csize, color=col
     xyouts, xp,yp+1,	/device,str, charsize=csize, color=col
     ;
     xyouts, xp,cyouts,     /device,camt, charsize=csize, color=col
     xyouts, xp,cyouts+1,   /device,camt, charsize=csize, color=col
  END
  8: BEGIN 
     csize = 1.0
     ;xp = 1
     ;yp = 1
     cyouts=yp+ csize*10 + 5
     xyouts, xp,yp, 	/device,str, charsize=csize, color=col
     ;
     xyouts, xp,cyouts, /device,camt, charsize=csize, color=col
  END 
  ELSE: BEGIN
     ; MVI case
     cyouts=yp+ csize*10 + 5
     xyouts, xp,yp, 	/device,str, charsize=csize, color=col, FONT=fontnum
     ;
     xyouts, xp,cyouts, /device,camt, charsize=csize, color=col, FONT=fontnum
  END  
ENDCASE

IF keyword_set(circle) THEN BEGIN
    IF tag_exist(hdr,'crpix1') THEN c=scc_sun_center(hdr) ELSE c=hdr
    tvcircle,(hdr.rsun/hdr.cdelt1),c.xcen,c.ycen, COLOR=255, thick=circle
ENDIF

datetime = tvrd(0)


;--Reset buffer 
os=strlowcase(os_family())
CASE os OF
  'windows': set_plot,'win'
  ELSE    : set_plot,'x'
ENDCASE

;--Set stamp value to data max
stamp_dex = where(datetime EQ col)

im[stamp_dex]=charcolor

RETURN,im 
END
