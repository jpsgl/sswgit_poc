function scc_get_mask, hdr,num=num, sub=sub
;+
; $Id: scc_get_mask.pro,v 1.3 2007/02/22 19:04:41 colaninn Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_get_mask
;               
; Purpose   : This function returns the index of the masked pixels
;               
; Explanation: The function reads the planning tool mask tables to get
;              the masks for each detector. It then selects the mask
;              used in the image and resizes the mask to match the
;              image. It then returns the index of all the masked
;              pixels in the image.

;               
; Use       : IDL> mask_index = scc_get_mask(hdr)
;    
; Inputs    : hdr -image header, either fits or SECCHI structure
;
; Outputs   : mask_index - the longword vector containing the
;                           subscripts of the masked pixels 
;
; Keywords  : num - if set returns the number of pixels returned 
;             sub - if then the value returned in mask_index is the
;                   index of the 34x34 transmission block 
;
; Environments: SSW
;
; Calls     : SCC_FITSHDR2STRUCT, GETENV_SLASH, READ_MASK_TABLES
;            
;               
; Category    : Masking, Missing Blocks
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU August 2006
;               
; $Log: scc_get_mask.pro,v $
; Revision 1.3  2007/02/22 19:04:41  colaninn
; changed mask table path
;
; Revision 1.2  2006/10/06 14:26:27  colaninn
; removed hard coded delimintors
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.8  2006/09/18 17:56:37  colaninn
; added sub keyword
;
; Revision 1.7  2006/09/15 21:24:11  colaninn
; changed image path to new SSW path
;
; Revision 1.6  2006/09/08 15:21:54  colaninn
; changed getenv to getenv_slash
;
; Revision 1.5  2006/09/07 17:25:15  colaninn
; check for MASK_TBL equal NONE
;
; Revision 1.4  2006/09/05 20:54:30  colaninn
; changed IFs to CASE
;
; Revision 1.3  2006/08/29 17:49:34  colaninn
; added correct env but not implemented
;
; Revision 1.2  2006/08/15 21:26:50  colaninn
; change path to env
;
; Revision 1.1  2006/08/09 15:54:11  colaninn
; first entry
;
;-            
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF (TAG_NAMES(hdr, /STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
message,'ONLY SECCHI HEADER STRUCTURE ALLOWED'

num =0 

;--Get the filename and spacecraft from the header
mask_table = hdr.MASK_TBL
IF mask_table EQ 'NONE' THEN RETURN,-1

path = GETENV_SLASH('SSW')
d = get_delim()
str = path+'stereo'+d+'secchi'+d+'data'+d+mask_table
filename = file_search(str,/TEST_REGULAR,count=flag)
IF flag EQ 0 THEN BEGIN
  message,'COULD NOT FIND MASK TABLE',/inform
  RETURN,-1
ENDIF
sc = (str_sep(hdr.OBSRVTRY,'_'))[1]

;--Read the mask table
READ_MASK_TABLES,filename, roi_mask, occ_mask, sc

;--Find the index for each detector
det = hdr.DETECTOR
CASE det OF
    'EUVI': tel=0 
    'COR1': tel=1
    'COR2': tel=2
    'HI1' : tel=3
    'HI2' : tel=4
ELSE: message, 'ONLY SECCHI DETECTORS ALLOWED'
ENDCASE

;--Select the mask from the mask table arrays
CASE 1 OF 
  stregex(hdr.IP_00_19,' 27',/boolean): mask = occ_mask[tel,*]
  stregex(hdr.IP_00_19,' 25',/boolean): mask = roi_mask[tel,*,0]
  stregex(hdr.IP_00_19,' 45',/boolean): mask = roi_mask[tel,*,1]
  stregex(hdr.IP_00_19,' 46',/boolean): mask = roi_mask[tel,*,2]
  stregex(hdr.IP_00_19,' 47',/boolean): mask = roi_mask[tel,*,3]
ELSE: RETURN, -1
ENDCASE

IF stregex(hdr.IP_00_19,' 29',/boolean) THEN mask = ~mask 

;--Reform, rebin, trim the mask to match the image
IF ~keyword_set(sub) THEN BEGIN 
  mask = reform(mask,34,34)
  mask = rebin(mask, 2176,2176,/sample)

  message,'DOES NOT INCLUDE RECTIFY IMAGE CORRECTION',/inform
  mask = mask[hdr[0].P1COl:hdr[0].P2COL,hdr[0].P1ROW:hdr[0].P2ROW]
  mask = rebin(mask,hdr.NAXIS1,hdr.NAXIS2,/sample)
ENDIF

;--Find the index of the mask pixels
mask = where(mask eq 1,num)

RETURN, mask
END
