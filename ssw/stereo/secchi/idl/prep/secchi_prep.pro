pro secchi_prep, filenames,headers,images,savepath=savepath,outsize=outsize, $
polariz_on=polariz_on, PRECOMMCORRECT_ON=precommcorrect_on, WRITE_FITS=write_fits, $
write_fts=write_fts,write_png=write_png,write_jpg=write_jpg, DEBUG=debug, $
trim_off=trim_off,silent=silent,cosmics=cosmics, UPDATE_HDR_OFF=update_hdr_off, $
CALIMG_OFF=calimg_off, EXPTIME_OFF=exptime_off, CALFAC_OFF=calfac_off, $
NOCALFAC_BUTCORRFORIPSUM=nocalfac_butcorrforipsum, RECTIFY=rectify,$
discri_pobj_on=discri_pobj_on, _EXTRA=ex
;+
; $Id: secchi_prep.pro,v 1.58 2013/11/07 10:34:45 nathan Exp $
;
; Project   : STEREO SECCHI
;
; Name      : secchi_prep
;
; Purpose   : Processes writes STEREO level 1.0 BETA release
;
; Explanation: Reads in the filename of a secchi pre-flight level 0.5
;              data and process data to level 1.0.
;              Please see the README.
;
; Use       : IDL> secchi_prep,filename,headers,images
;                   or 
;             IDL> secchi_prep,filename,/write_[fts,png,jpg]
;
; Inputs    : filenames - of secchi data
;
; Output    :headers - array of level 1 header sturctures
;
;            images - array of processed images, the returned
;                            array is the size of the largest image,
;                            smaller images not are scaled
;
; Keywords  : outsize - size of output array, if not set output array
;                       is the size of the largest image
;             /polariz_on - COR1 and COR2 ONLY! Total brightness
;                           images are returned
;   	    	    	    and/or saved. If set with
;   	      /PB or /MU or /PERCENT then the polarized brightness or polarization angle
;   	    	    	    or percent polarized are returned.
;             /write_fts - if set the program writes image to fits file
;             /write_png - if set the program writes image to png file
;             /write_jpg - if set the program writes image to jpg file
;             savepath - if any write_* is set and  you would like to
;                        save the image to a directory other then the
;                        current, set equal to the full path.
;             /trim_off - if set the under/over scan on the images in
;                        not removed; image size can be greater than 2048
;             cosmics - returns cosmic ray scrub report from image data [HI only]
;   	      /PRECOMMCORRECT_ON - Apply correction to pre-commissioned files
;   	      /CALIMG_OFF - Do not apply vignetting or flat-field calibration
;   	      /EXPTIME_OFF - Do not normalize to 1 sec exptime
;   	      /CALFAC_OFF - Do not apply calibration factor (including summing correction)
;   	      /UPDATE_HDR_OFF - Do not update filename or image stats (but everything else is updated)
;   	      /DEBUG - Extra waits and messages
;   	      /NOCALFAC_BUTCORRFORIPSUM - Sets /CALFAC_OFF and corrects for IP summing
;   	      /RECTIFY - Apply rectification if not already applied
;             /discri_pobj_on - or discri_pobj_on=[thres,bias] - Use discri_pobj.pro
;                        point sources and cosmic removal prog. /discri_pobj_on 
;                        uses default parameters. The second syntax allows 
;                        user's parameters.
;
; Common    : sec_prep
;
; Calls     : SCCREADFITS,SCC_MAKE_ARRAY, SCC_IMG_TRIM, COR_PREP,
;             EUVI_PREP, SCC_PUTIN_ARRAY,COR_POLARIZ, SCC_BYTSCL, 
;             SECCHI_RECTIFY
;
; Category    : Calibration
;
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU October 2006
;
; $Log: secchi_prep.pro,v $
; Revision 1.58  2013/11/07 10:34:45  nathan
; make sure levchar is 1 for unbinned HI /NOCALFAC
;
; Revision 1.57  2013/10/28 18:22:21  nathan
; If /EXPTIME_OFF then /UPDATE_HDR_OFF; do not do corrforipsum if hdr.ipsum GT 1
;
; Revision 1.56  2012/06/26 21:51:03  nathan
; Changes resulting from calfac/filename problem for HI: add /CALIMG_OFF,
; /EXPTIME_OFF,/CALFAC_OFF; set hdr.calfac; add CCDPIX to BUNIT for /NOCALFAC
; case; special HI levchar for /NOCALFAC
;
; Revision 1.55  2012/04/10 17:52:08  nathan
; Set BUNIT=DN/s/CCDPIX if /NOCALFAC
;
; Revision 1.54  2010/08/17 16:35:21  nathan
; graceful exit for empty input; change putin_array call
;
; Revision 1.53  2008/10/28 22:02:35  nathan
; added PRECOMM keyword to cor_prep call
;
; Revision 1.52  2008/08/26 19:45:52  nathan
; check for calfac_off before setting
;
; Revision 1.51  2008/05/21 19:16:09  nathan
; added /WRITE_FITS
;
; Revision 1.50  2008/05/16 20:58:44  thernis
; Use execute for discri_pobj to avoid compilation error for people that don't have the nrlgen directory installed
;
; Revision 1.49  2008/05/13 18:57:43  thernis
; Apply discri_pobj.pro after background subtraction for COR1
;
; Revision 1.48  2008/05/07 20:39:00  thernis
; Change dscri_pobj to discri_pobj_on to avoid compilation error
;
; Revision 1.47  2008/05/07 20:01:23  thernis
; Implement /discri_pobj
;
; Revision 1.46  2008/04/10 17:32:00  colaninn
; corrected COR1 call to cor_polariz and topographical
;
; Revision 1.45  2008/01/24 17:38:19  nathan
; do mask, rotate, etc. if /nocalfac set
;
; Revision 1.44  2008/01/22 19:44:43  nathan
; fixed last edit because I did it wrong
;
; Revision 1.43  2008/01/18 18:15:30  nathan
; Do not change value of OUTSIZE keyword for output
;
; Revision 1.42  2008/01/17 20:20:56  colaninn
; added sub-routine scc_make_array.pro
;
; Revision 1.41  2008/01/17 16:54:58  thompson
; *** empty log message ***
;
; Revision 1.40  2008/01/17 16:32:49  thompson
; Automatically apply precommcorrect to early COR1 images.
;
; Revision 1.39  2007/12/13 17:00:20  colaninn
; changed default size for multiple files
;
; Revision 1.38  2007/12/05 19:26:53  nathan
; fixed case where exptime incorrectly computed if precommcorrect and some null records in ext hdr
;
; Revision 1.37  2007/11/13 21:48:26  nathan
; skip images whose filenames indicate already prepped
;
; Revision 1.36  2007/11/02 21:50:22  nathan
; add /RECTIFY option and Bug248 RCOL/ROW correction
;
; Revision 1.35  2007/10/26 22:52:33  nathan
; removed print blank line if /silent
;
; Revision 1.34  2007/10/17 16:34:47  colaninn
; changed use message
;
; Revision 1.33  2007/09/25 15:46:47  nathan
; added /NOCALFAC_BUTCORRFORIPSUM (Bug 217)
;
; Revision 1.32  2007/09/20 20:51:48  nathan
; added use info
;
; Revision 1.31  2007/08/29 12:07:56  crothers
; added array catenation to cosmics
;
; Revision 1.30  2007/08/29 11:39:58  crothers
; Exposed keyword cosmics in hi_correction
;
; Revision 1.29  2007/08/24 19:30:49  nathan
; updated info messages and hdr history
;
; Revision 1.28  2007/08/23 23:54:31  nathan
; add info messages
;
; Revision 1.27  2007/06/29 14:49:28  colaninn
; added code to skip image when error occurs and continue processing
;
; Revision 1.26  2007/06/15 21:38:57  colaninn
; made change to better handle cropped images
;
; Revision 1.25  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.24  2007/05/18 19:44:30  colaninn
; removed output switch warning
;
; Revision 1.23  2007/04/04 19:11:51  colaninn
;  reversed output parameters for all prep programs
;
; Revision 1.22  2007/04/04 15:27:26  colaninn
; reversed output parameters for all prep programs
;
; Revision 1.21  2007/04/03 21:22:24  nathan
; total exptime in extended header for summed images (Bug 126)
;
; Revision 1.20  2007/03/13 17:41:49  nathan
; Take away explicit /PERCENT and remove /PPIMAGE and /PPHDR; add COMMON sec_prep;
; remove COMMON fits_comments; use spcomments not comments variable
;
; Revision 1.19  2007/02/13 15:12:07  colaninn
; updated get_fitscomments call
;
; Revision 1.18  2007/02/06 21:00:30  colaninn
; added keyword comment handling
;
; Revision 1.17  2007/02/05 15:53:09  nathan
; add PPIMAGE and PPHDR keyword outputs for sccdopolar.pro
;
; Revision 1.16  2007/02/02 00:11:57  nathan
; add /DEBUG, polcmnts, more FITS comments
;
; Revision 1.15  2007/01/31 18:29:51  nathan
; Add /PRECOMMCORRECT_ON; add header keyword comments for output FITS file
;
; Revision 1.14  2007/01/29 22:41:46  nathan
; implement /PERCENT; reorder some messages
;
; Revision 1.13  2007/01/27 00:11:00  nathan
; correct call to struct2fitshead
;
; Revision 1.12  2007/01/19 19:13:59  newmark
; 2007/01/19 Update for HI_PREP newmark
;
; Revision 1.11  2007/01/19 17:23:58  colaninn
; moved scc_putin_array to top of for loop
;
; Revision 1.10  2007/01/17 15:31:30  colaninn
; created from RTMVIXY.pro
;
; Revision 1.9  2006/12/14 21:31:49  colaninn
; updated header
;
; Revision 1.8  2006/12/07 21:15:26  colaninn
; made write_on work with polariz_on
;
; Revision 1.7  2006/12/06 20:06:19  colaninn
; added code for polariz_on
;
; Revision 1.6  2006/12/05 21:04:57  colaninn
; corrected hdr.summed code
;
; Revision 1.5  2006/11/27 19:54:04  colaninn
; changed trim_off keyword setting
;
; Revision 1.4  2006/11/21 22:15:13  colaninn
; made updates for Beta 2
;
; Revision 1.3  2006/10/24 20:37:35  colaninn
; added scc_img_trim.pro
;
; Revision 1.2  2006/10/10 13:44:18  colaninn
; corrected ! problem
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.1  2006/10/02 18:57:29  colaninn
; beta release
;
;-
COMMON sec_prep, spcomments

little_endian = (BYTE(1, 0, 1))[0]
IF ~keyword_set(SILENT) THEN print,''
IF ~keyword_set(SILENT) THEN $
print,'$Id: secchi_prep.pro,v 1.58 2013/11/07 10:34:45 nathan Exp $'

;--Check validity of input parameters and keywords---------------------
;----------------------------------------------------------------------
   IF n_params() LT 1 THEN BEGIN
     message,"Use: IDL> secchi_prep, filename, header, image",/infrom
     message,'or   IDL> secchi_prep, filename, /write_[fts,png,jpg]'
   ENDIF
   help,filenames
;--Allow graceful exit for empty input
  IF datatype(filenames) NE 'STR' or strlen(filenames[0]) LT 9 THEN BEGIN
	message,'Invalid input. Returning...',/info
	wait,4
	return
  ENDIF
  ;--Create structure to send keywords to subroutines
  IF datatype(ex) EQ 'UND' THEN ex = create_struct('blank',-1)

  ;--Check for WRITE_ON keyword
  write_flag = 0
  IF keyword_set(WRITE_FITS) THEN write_fts=1
  IF keyword_set(WRITE_FTS) THEN write_flag = 1
  IF keyword_set(WRITE_PNG) THEN write_flag = 1
  IF keyword_set(WTITE_JPG) THEN write_flag = 1
  IF NOT write_flag AND n_params() EQ 1 THEN $
   message, 'Keyword WRITE_* must be set when no output parameters are entered'

  ;--Check for TRIM_OFF keyword
  IF keyword_set(TRIM_OFF) THEN ex = create_struct('trim_off',1,ex)
  IF keyword_set(TRIM_OFF) AND ~tag_exist(ex,'calibrate_off') THEN $
   ex = create_struct('calibrate_off',1,ex)

  ;--Pass SILENT keyword to subroutines
  IF keyword_set(SILENT) THEN ex = create_struct('silent',1,ex)
  IF keyword_set(POLARIZ_ON) THEN ex = create_struct('polariz_on',1,ex)
  ;--Pass CALFAC_OFF if NOCALFAC set
  IF keyword_set(NOCALFAC_BUTCORRFORIPSUM) AND ~tag_exist(ex,'calfac_off') THEN $
    ex = create_struct('calfac_off',1,ex)
  IF keyword_set(CALFAC_OFF) AND ~tag_exist(ex,'calfac_off') THEN $
    ex = create_struct('calfac_off',1,ex)
  IF keyword_set(CALIMG_OFF) AND ~tag_exist(ex,'calimg_off') THEN $
    ex = create_struct('calimg_off',1,ex)
  IF keyword_set(EXPTIME_OFF) AND ~tag_exist(ex,'exptime_off') THEN $
    ex = create_struct('exptime_off',1,ex)
  IF keyword_set(EXPTIME_OFF) AND ~tag_exist(ex,'update_hdr_off') THEN $
    ex = create_struct('update_hdr_off',1,ex)

  ;--Check for SAVEPATH keyword
  IF keyword_set(SAVEPATH) THEN BEGIN
    savepath = file_search(SAVEPATH,/test_directory,/test_write,count=pflag)
    defsysv, '!DELIMITER', exists = df
    IF df NE 1 THEN sl = get_delim() ELSE sl = !delimiter
    IF pflag EQ 1 THEN savepath = savepath+sl ELSE $
     message,'Could not find entered SAVEPATH'
  ENDIF

;----------------------------------------------------------------------

;--Create array to return images to memory-----------------------------
;----------------------------------------------------------------------
  IF keyword_set(OUTSIZE) THEN outsize0 = outsize
  
  images = SCC_MAKE_ARRAY(filenames,headers,outout,outsize=outsize0,_extra=ex)
  ex = create_struct(ex,'outsize',outout)

  ;--Check for POLARIZ_ON keyword
  pn =3
  IF keyword_set(polariz_on) THEN BEGIN
    ss = size(images,/dim)  
    seq=fltarr(ss[0],ss[1],pn)
    seq_hdr = replicate(def_secchi_hdr(silent=silent),pn)
  ENDIF ELSE seq=0
  polariz_count=0
;----------------------------------------------------------------------

;--Loop to process each image------------------------------------------
;----------------------------------------------------------------------
  num = n_elements(filenames)
  str_num = strtrim(string(num),2)

    ;--Open image
  FOR i=0, num-1 DO BEGIN

    ;--Allows SECCHI_PREP to continue on errors
    ;------------------------------------------------------------------  
    CATCH, error_status
    IF error_status NE 0 THEN BEGIN
       print, 'ERROR WHILE PROCESSING '+hdr.filename+' IMAGE SKIPPED'
       help, /Last_Message, Output=traceback
       Print, 'Traceback Report from SECCHI_PREP :'
       FOR j=0,N_Elements(traceback)-2 DO Print, "     " + traceback[j]
       !ERROR_STATE.CODE =0
       IF keyword_set(polariz_on) THEN BEGIN
         i = i+(pn-polariz_count)
         polariz_count = 0
       ENDIF ELSE i=i+1
       IF i GT num-1 THEN RETURN
    ENDIF
    ;------------------------------------------------------------------  
    
    IF ~keyword_set(silent) THEN BEGIN
      message,'Processing image '+strtrim(string(i+1),2)+$
           ' out of '+str_num,/inform
      message,filenames[i],/inform
    ENDIF

    im = SCCREADFITS(filenames[i],hdr,silent=silent,header=str_hdr)
    spcomments = GET_FITSCOMMENTS(str_hdr,hdr)
    IF hdr.calfac EQ 0 THEN hdr.calfac=1.0

    ;--Correct for Bug 248; BUG CLOSED 2007-11-02
    IF hdr.rectify NE 'T' THEN BEGIN
    	hdr.r1col=hdr.p1col
	hdr.r2col=hdr.p2col
	hdr.r1row=hdr.p1row
	hdr.r2row=hdr.p2row
    ENDIF
    
    IF keyword_set(RECTIFY) and hdr.rectify NE 'T' THEN $
     im=SECCHI_RECTIFY(temporary(im),hdr)

    ;--Correct for pre commissioning bugs.
    ;--Automatically correct early COR1 images.
    ;------------------------------------------------------------------  
    if n_elements(precommcorrect_on) eq 1 then $
    	test=keyword_set(PRECOMMCORRECT_ON) $
    else begin
    	if hdr.obsrvtry eq 'STEREO_A' then $
	    date0 = '2007-02-03T13:15' else $
            date0 = '2007-02-21T21'
        test = (hdr.detector eq 'COR1') and (hdr.date_obs lt date0) and (hdr.date lt '2008-01-17')
    endelse
help,test

    IF test THEN BEGIN
        IF hdr.extend EQ 'T' and hdr.n_images GT 1 THEN BEGIN
    	    xh=mrdfits(filenames[i],1)
	    z=where(xh.exptime EQ 0,nz)
	    IF nz LE 0 THEN hdr.exptime=total(xh.exptime)
	ENDIF
    	precommcorrect, im, hdr, exthdr=xh, _extra=ex
    ENDIF
    ;------------------------------------------------------------------  
   
    ;--Trim image under/over scan
    IF ~keyword_set(TRIM_OFF) THEN BEGIN
      im = SCC_IMG_TRIM(im,hdr,_extra=ex)
    ENDIF

    ;--Rescale image to fit in output array
    im = SCC_PUTIN_ARRAY(im,hdr,outout,_extra=ex)

    ; -- apply discri_pobj.pro, a point sources removal prog
    if keyword_set(discri_pobj_on) then begin
	if n_elements(discri_pobj_on) eq 2 then begin
		foo=execute('im=discri_pobj(im,thres=discri_pobj_on[0],bias=discri_pobj_on[1])')
	endif else CASE hdr.DETECTOR OF
		'EUVI' : im=discri_pobj(im)
		'COR1' : ; done in cor1_calibrate ;im=discri_pobj(im)
		'COR2' : foo=execute('im=discri_pobj(im,thres=0.01,bias=0.)')
		'HI1' : im=discri_pobj(im)
		'HI2' : im=discri_pobj(im)
		else : message,'Unknown detector !'
	endcase
    endif

    ;--Check for Level 1 images
    levelb = reform(byte(strmid(hdr.filename,16,1)))
    IF levelb LE 57 AND levelb GE 48 THEN BEGIN
    ;--Filename indicates image has had calibration applied
      message,'17th char of filename indicates image has already been processed.',/inform
      message,'Skipping calibration.',/inform
      IF ~tag_exist(ex,'calibrate_off') THEN ex = CREATE_STRUCT('calibrate_off',1,ex)
      IF ~tag_exist(ex,'calfac_off') THEN ex = CREATE_STRUCT('calfac_off',1,ex)	
    ENDIF
    
  ;--BEGIN CALIBRATION--------------------------------

    CASE hdr.DETECTOR OF
      'EUVI': BEGIN
         EUVI_PREP,hdr,im,_extra=ex
       END
      'COR1':BEGIN
	 COR_PREP,hdr,im,discri_pobj_on=discri_pobj_on,_extra=ex
         ;--Background Substaction(OFF)

         ;--Polarization processing (OFF)
         IF keyword_set(polariz_on) THEN BEGIN
           polariz_count = polariz_count+1
           seq = shift(seq,0,0,1)
           ;seq[*,*,0] = SCC_PUTIN_ARRAY(im,hdr,outsize,_extra=ex)
           seq[*,*,0] = im
           seq_hdr = shift(seq_hdr,1)
           seq_hdr[0]=hdr
           IF  polariz_count EQ pn THEN BEGIN
             im = COR_POLARIZ(seq,seq_hdr,hdr,ppimage=ppim, pphdr=pph, _extra=ex)
           ENDIF
         ENDIF
       END
      'COR2': BEGIN
         COR_PREP,hdr,im, PRECOMMCORRECT_ON=precommcorrect_on, _extra=ex
         ;--Background Substaction(OFF)

         ;--Polarization processing (OFF)
         IF keyword_set(polariz_on) THEN BEGIN
           polariz_count = polariz_count+1
           seq = shift(seq,0,0,1)
           ;seq[*,*,0] = SCC_PUTIN_ARRAY(im,hdr,outsize,_extra=ex)
           seq[*,*,0] = im
           seq_hdr = shift(seq_hdr,1)
           seq_hdr[0]=hdr
           IF  polariz_count EQ pn THEN BEGIN
             IF keyword_set(DEBUG) THEN BEGIN
	     	print,'Starting COR_POLARIZ in 3...'
	     	wait,3
	     ENDIF
	     im = COR_POLARIZ(seq,seq_hdr,hdr,ppimage=ppim, pphdr=pph, _extra=ex)
           ENDIF
         ENDIF
       END
      'HI1': BEGIN
         HI_PREP,hdr,im,cosmics=cosmic2,_extra=ex
         if i eq 0 then cosmics=cosmic2 else cosmics=[cosmics, cosmic2]
       END
      'HI2': BEGIN
         HI_PREP,hdr,im,cosmics=cosmic2,_extra=ex
         if i eq 0 then cosmics=cosmic2 else cosmics=[cosmics, cosmic2]
       END
       ELSE: message,'STEREO SECCHI DETECTOR could not be found'
    ENDCASE
  ;----------------------------------------------------

    ;--Correct for IP Summing here OR in get_calfac.pro
    sumcount=0
    
    IF keyword_set(NOCALFAC_BUTCORRFORIPSUM) or (~tag_exist(ex,'CALFAC_OFF') and hdr.calfac EQ 1.0)  THEN BEGIN
	sumcount=hdr.ipsum-1
	divfactor=(2^(sumcount))^2
	im=im/divfactor
	IF hdr.ipsum GT 1 THEN BEGIN
	    IF ~keyword_set(SILENT) THEN BEGIN
    		message,'Divided image by '+trim(divfactor)+' to account for IPSUM',/info
		message,'IPSUM changed to 1 in header.',/info
	    ENDIF
	    hdr=scc_update_history(hdr,'image Divided by '+trim(divfactor)+' to account for IPSUM')   
	ENDIF
	hdr.ipsum=1
    	hdr.bunit=hdr.bunit+'/CCDPIX'
    	IF strmid(hdr.detector,0,2) EQ 'HI' THEN levchar='1' ELSE levchar='0'
	filename=hdr.filename
    	strput, filename,levchar,16
	IF ~keyword_set(silent) THEN message,'Adding levchar='+levchar+' to FILENAME',/info
    	hdr.filename = filename
	
    ENDIF

aftercalibration:

    ;--Fill output array
    IF n_params() GT 1 THEN BEGIN
    	IF keyword_set(polariz_on) THEN BEGIN
	    IF (polariz_count EQ pn) THEN BEGIN
    	    	p = (i-2)/3.0 ;p is 1 image for every 3 of i
    	    	headers[p] = hdr
    	    	images[*,*,p]=im
    	    	IF ~write_flag THEN polariz_count=0
	    ENDIF
    	ENDIF ELSE BEGIN
    	    headers[i] = hdr
    	    images[*,*,i]=im
    	ENDELSE
    ENDIF

   ;--Print file to disk
IF write_flag THEN BEGIN
  pfilename = hdr.filename
  tvlct, r,g,b,/get
  IF keyword_set(savepath) THEN pfilename = savepath+pfilename
  skip_flag = 0
  IF keyword_set(polariz_on) AND (polariz_count NE pn) THEN skip_flag =1

  IF NOT skip_flag THEN BEGIN
    CASE 1 OF
      keyword_set(write_fts): BEGIN
        SCCWRITEFITS,  hdr.filename,im,hdr,SAVEPATH=savepath, _EXTRA=ex
      END
      keyword_set(write_png): BEGIN
        pfilename = strmid(pfilename,0,strlen(pfilename)-3)+'png'
        IF ~keyword_set(silent)THEN message,'WRITING '+pfilename,/inform
        im = SCC_BYTSCL(im,hdr)
        IF little_endian THEN BEGIN
          write_png,pfilename,im,r,g,b
        ENDIF ELSE write_png,pfilename,rotate(im,7),r,g,b
      END
      keyword_set(write_jpg): BEGIN
        pfilename = strmid(pfilename,0,strlen(pfilename)-3)+'jpg'
        IF ~keyword_set(silent) THEN message,'WRITING '+pfilename,/inform
        write_jpeg,pfilename,bytscl(im,0,hdr.datap95)
      END
     ENDCASE
    polariz_count =0
  ENDIF
ENDIF


ENDFOR
IF ~keyword_set(silent) THEN message,'FINISHED!',/inform

;----------------------------------------------------------------------

RETURN
END

