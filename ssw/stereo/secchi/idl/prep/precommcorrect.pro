pro precommcorrect, im, hdr, exthdr=xh, SILENT=silent, _extra=ex
;+
; $Id: precommcorrect.pro,v 1.23 2012/07/30 16:41:54 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : precommcorrect
;               
; Purpose   : Apply corrections to images taken before all commissioning data
;   	    	is reprocessed.
;               
; Explanation: Applies misc corrections for all instruments.
;               
; Use       : IDL> precommcorrect, im, hdr
;    
; Inputs    : im - level 0.5 image from sccreadfits
;             hdr -level 0.5 header from sccreadfits, SECCHI structure
;
; Outputs   : im - corrected level 0.5 image
;   	      hdr - corrected level 0.5 header structure
;
; Keywords  : Exthdr= HI extended header
;
; Calls     : 
;
; Category    : Administration, Calibration, Pipeline
;
; Written     : Nathan.rich@nrl.navy.mil
;
; $Log: precommcorrect.pro,v $
; Revision 1.23  2012/07/30 16:41:54  nathan
; do not do hi pointing
;
; Revision 1.22  2008/04/03 18:18:57  nathan
; added /SILENT and print file version
;
; Revision 1.21  2008/03/28 19:59:25  nathan
; added cor2_point
;
; Revision 1.20  2008/02/07 13:57:37  bewsher
; Changed call to hi_point_v2
;
; Revision 1.19  2008/01/17 00:02:46  thompson
; Removed date restriction for COR1
;
; Revision 1.18  2007/12/19 16:39:53  colaninn
; Added path to pointing files for hi_point_v2
;
; Revision 1.17  2007/10/18 20:51:42  colaninn
; add more checks for DSTOP1(2)
;
; Revision 1.16  2007/10/17 18:03:07  colaninn
; added one updated DSTART and DSTOP
;
; Revision 1.15  2007/10/17 16:28:41  colaninn
; updates DSTART1(2) DSTOP(1)(2) keywords if missing
;
; Revision 1.14  2007/08/23 23:56:31  nathan
; fixed xh check and add _Extra to pro calls
;
; Revision 1.13  2007/06/28 20:47:32  colaninn
; added on_error,2
;
; Revision 1.12  2007/05/21 20:16:05  colaninn
; added silent keyword
;
; Revision 1.11  2007/04/13 20:06:20  nathan
; add hi_point.pro
;
; Revision 1.10  2007/04/11 20:18:01  colaninn
; removed SW IP correction
;
; Revision 1.9  2007/04/11 18:58:33  colaninn
; added SW IP correction
;
; Revision 1.8  2007/04/04 19:10:45  colaninn
; added date restriction to cor1_point
;
; Revision 1.7  2007/04/04 15:27:25  colaninn
; added cor1_point
;
; Revision 1.6  2007/03/27 17:06:09  colaninn
; added euvi_point correction
;
; Revision 1.5  2007/02/26 19:05:44  colaninn
; made change to None conditional
;
; Revision 1.4  2007/02/16 17:08:14  colaninn
; fixed stupid mistake
;
; Revision 1.3  2007/02/14 22:33:36  nathan
; add another DIV2 correction correction
;
; Revision 1.2  2007/02/13 19:05:48  colaninn
; remove mask table
;
; Revision 1.1  2007/01/31 18:21:52  nathan
; do scc_icerdiv2
;
;-
ON_ERROR,2

IF ~keyword_set(SILENT) THEN print,'$Id: precommcorrect.pro,v 1.23 2012/07/30 16:41:54 nathan Exp $'

;--Apply IcerDiv2 correction (Bug 49)
IF (hdr.comprssn GT 89 and hdr.comprssn LT 102) THEN BEGIN
    IF hdr.DIV2CORR EQ 'F' THEN $    	; Icer
    	scc_icerdiv2,hdr,im $
    ; HISTORY added in scc_icerdiv2
    ELSE BEGIN
    ;--Check to see if div2 erroneously corrected
    	biasmean=hdr.biasmean
	p01mbias  = hdr.datap01-biasmean           
	IF ~keyword_set(SILENT) THEN help,p01mbias  ,biasmean   
	if (p01mbias GT 0.8*hdr.biasmean) then begin
	    im = im/2
	    hdr.datap01 = hdr.datap01 / 2
	    hdr.datamin = hdr.datamin / 2
	    hdr.datamax = hdr.datamax / 2
	    hdr.dataavg = hdr.dataavg / 2
	    hdr.datap10 = hdr.datap10 / 2
	    hdr.datap25 = hdr.datap25 / 2
	    hdr.datap75 = hdr.datap75 / 2
	    hdr.datap90 = hdr.datap90 / 2
	    hdr.datap95 = hdr.datap95 / 2
	    hdr.datap98 = hdr.datap98 / 2
	    hdr.datap99 = hdr.datap99 / 2
	    hdr.div2corr = 'F'

	     IF ~keyword_set(SILENT) THEN message,'************Image corrected for incorrect icerdiv2 ************',/info
	endif
    ENDELSE
ENDIF

hdr.mask_tbl = 'NONE'

;--Correct Image Center
IF hdr.DETECTOR EQ 'EUVI' THEN euvi_point,hdr, quiet=keyword_set(SILENT)

IF hdr.DETECTOR EQ 'COR1' THEN cor1_point,hdr, SILENT=silent, _extra=ex

IF hdr.DETECTOR EQ 'COR2' THEN cor2_point,hdr, SILENT=silent, _extra=ex

;IF strmid(hdr.DETECTOR,0,2) EQ 'HI' THEN BEGIN
; hi_fix_pointing.pro and $SDD_DATA/hi have made this obsolete.
IF (0) THEN BEGIN
    IF datatype(xh) NE 'STC' THEN BEGIN
    	message,'EXTHDR=Extended_header_structure required for HI pointing; not changing header values.',/info
	;wait,5
	;stop
	return
    ENDIF
  ;--Find Path Pointing Files
  get_utc,utc
  yyyy= strmid(hdr.date_obs,0,4)
  mm = strmid(hdr.date_obs,5,2)
  aorblc =strlowcase(strmid(hdr.obsrvtry,7,1))
  IF aorblc eq 'a' THEN scname = 'ahead' ELSE scname = 'behind'

  IF (anytim2utc(hdr.date_obs)).(0) LT utc.mjd-45 THEN BEGIN 
    IF getenv('SDS_FIN_DIR') EQ '' THEN sdpath = 0 ELSE $
    sdspath=getenv('SDS_FIN_DIR')+'/'+aorblc+'/'+yyyy+'/'+mm
  ENDIF ELSE BEGIN
    IF getenv('SDS_FIN_DIR') EQ '' THEN sdpath = 0 ELSE $
    sdspath=getenv('SDS_DIR')+'/'+scname+$
   '/data_products/level_0_telemetry/secchi' 
  ENDELSE
    
  hdr = hi_point_v2(hdr,xh,dir=sdspath, _extra=ex)
ENDIF  


;--Add DSTART(STOP)1(2)------------------------------------
;--Code taken from revision 1.19 of scc_img_trim.pro  
;--Calculate data area from un-rectified image cooridinates
IF (hdr.DSTOP1 LT 1) OR (hdr.DSTOP1 GT hdr.NAXIS1) OR (hdr.DSTOP2 GT hdr.NAXIS2) THEN BEGIN
  x1 = 51-hdr.P1COL
  IF x1 LT 0 THEN x1 =0
  x2 = hdr.P2COL-hdr.P1COL
  IF x2 GT 2048+x1-1 THEN x2 = 2048+x1-1

  y1 = 1-hdr.P1ROW
  IF y1 LT 0 THEN y1=0
  y2 = hdr.P2ROW-hdr.P1ROW
  IF y2 GT 2048+y1-1 THEN y2 = 2048+y1-1

  ;--Reset P1(2)COL and P1(2)ROW to trimmed values
  IF hdr.P1COL LT 51 THEN hdr.P1COL=51
  hdr.P2COL = hdr.P1COL+(x2-x1)
  IF hdr.P1ROW LT 1 THEN hdr.P1ROW=1
  hdr.P2ROW = hdr.P1ROW+(y2-y1)

  ;--Correct data area cooridinates for summing
  x1 = fix(x1/2^(hdr.summed-1))
  x2 = ((hdr.P2COL-hdr.P1COL+1)/2^(hdr.summed-1))+x1-1 
  y1 = fix(y1/2^(hdr.summed-1))
  y2 =((hdr.P2ROW-hdr.P1ROW+1)/2^(hdr.summed-1))+y1-1

  ;--Convert data area cooridinates for rectified image
  ;--Reset R1(2)COL and R1(2)ROW to trimmed values
  IF hdr.RECTIFY EQ 'T' THEN BEGIN
    IF (hdr.OBSRVTRY EQ 'STEREO_A') THEN BEGIN
      CASE hdr.DETECTOR OF  
      'EUVI': BEGIN
        rx1 = hdr.naxis1-y2-1
        rx2 = hdr.naxis1-y1-1
        ry1 = hdr.naxis2-x2-1
        ry2 = hdr.naxis2-x1-1
        hdr.R1COL = 2176-hdr.P2ROW+1
        hdr.R2COL = 2176-hdr.P1ROW+1
        hdr.R1ROW = 2176-hdr.P2COL+1
        hdr.R2ROW = 2176-hdr.P1COL+1
      END   
      'COR1': BEGIN
       rx1 = y1
       rx2 = y2
       ry1 = hdr.naxis2-x2-1
       ry2 = hdr.naxis2-x1-1
       hdr.R1COL = hdr.P1ROW
       hdr.R2COL = hdr.P2ROW
       hdr.R1ROW = 2176-hdr.P2COL+1
       hdr.R2ROW = 2176-hdr.P1COL+1
     END
    'COR2': BEGIN 
       rx1 = hdr.naxis1-y2-1
       rx2 = hdr.naxis1-y1-1
       ry1 = x1
       ry2 = x2
       hdr.R1COL = 2176-hdr.P2ROW+1
       hdr.R2COL = 2176-hdr.P1ROW+1
       hdr.R1ROW = hdr.P1COL
       hdr.R2ROW = hdr.P2COL
   END
    'HI1': BEGIN
       rx1 = x1
       rx2 = x2
       ry1 = y1
       ry2 = y2
       hdr.R1COL = hdr.P1COL
       hdr.R2COL = hdr.P2COL
       hdr.R1ROW = hdr.P1ROW
       hdr.R2ROW = hdr.P2ROW
     END
    'HI2': BEGIN
       rx1 = x1
       rx2 = x2
       ry1 = y1
       ry2 = y2
       hdr.R1COL = hdr.P1COL
       hdr.R2COL = hdr.P2COL
       hdr.R1ROW = hdr.P1ROW
       hdr.R2ROW = hdr.P2ROW
     END  
     ENDCASE
    ENDIF

   IF (hdr.OBSRVTRY EQ 'STEREO_B') THEN BEGIN
    CASE hdr.DETECTOR OF
    'EUVI': BEGIN
       rx1 = y1
       rx2 = y2
       ry1 = hdr.naxis2-x2-1
       ry2 = hdr.naxis2-x1-1
       hdr.R1COL = hdr.P1ROW
       hdr.R2COL = hdr.P2ROW
       hdr.R1ROW = 2176-hdr.P2COL+1
       hdr.R2ROW = 2176-hdr.P1COL+1
   END
    'COR1': BEGIN
       rx1 = hdr.naxis1-y2-1
       rx2 = hdr.naxis1-y1-1
       ry1 = x1
       ry2 = x2
       hdr.R1COL = 2176-hdr.P2ROW+1
       hdr.R2COL = 2176-hdr.P1ROW+1
       hdr.R1ROW = hdr.P1COL
       hdr.R2ROW = hdr.P2COL    
   END
    'COR2': BEGIN 
       rx1 = y1
       rx2 = y2
       ry1 = hdr.naxis2-x2-1
       ry2 = hdr.naxis2-x1-1
       hdr.R1COL = hdr.P1ROW
       hdr.R2COL = hdr.P2ROW
       hdr.R1ROW = 2176-hdr.P2COL+1
       hdr.R2ROW = 2176-hdr.P1COL+1
   END
     'HI1': BEGIN
       rx1 = hdr.naxis1-x2-1
       rx2 = hdr.naxis1-x1-1
       ry1 = hdr.naxis2-y2-1
       ry2 = hdr.naxis2-y1-1
       hdr.R1COL = 2176-hdr.P2ROW
       hdr.R2COL = 2176-hdr.P1ROW
       hdr.R1ROW = 2176-hdr.P2COL
       hdr.R2ROW = 2176-hdr.P1COL
   END
     'HI2': BEGIN
       rx1 = hdr.naxis1-x2-1
       rx2 = hdr.naxis1-x1-1
       ry1 = hdr.naxis2-y2-1
       ry2 = hdr.naxis2-y1-1
       hdr.R1COL = 2176-hdr.P2ROW
       hdr.R2COL = 2176-hdr.P1ROW
       hdr.R1ROW = 2176-hdr.P2COL
       hdr.R2ROW = 2176-hdr.P1COL
    END
   ENDCASE
  ENDIF
  ;--Data area cooridinates for rectified image
   x1 = rx1
   x2 = rx2 
   y1 = ry1
   y2 = ry2
ENDIF


  hdr.DSTART1 = x1+1 
  hdr.DSTART2 = y1+1
  hdr.DSTOP1 = x2+1
  hdr.DSTOP2 = y2+1
ENDIF
;-----------------------------------------

 ; im = scc_img_trim(im,hdr)
  
 ; hdr.dstart1 =  hdr.P1COL
 ; hdr.dstop1 = hdr.P2COL
 ; hdr.dstart2 = hdr.P1ROW
 ; hdr.dstop2 = hdr.P2ROW

;--Moved to SCC_SEBIP :(
;--Trim IP commands for SW images
;ip = hdr.ip_00_19
;--Fix length of ip string
;IF strlen(ip) LT 60 THEN ip=' '+ip      ; ip should be 3x20 char long, but
;IF strlen(ip) LT 60 THEN ip=' '+ip      ; could be as short as 58 if trimmed
;--Seperate into array
;seb_ip = fix(string(reform(byte(ip),3,20)))
;x = where(seb_ip eq 117,cnt)
;IF cnt EQ 1 THEN BEGIN
;  ip = strmid(ip,[3*x],60)
;  IF strlen(ip) LT 60 THEN repeat ip = ip +'  0' until strlen(ip) GE 59
;ENDIF
;hdr.ip_00_19 = ip

END
