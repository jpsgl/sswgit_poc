function scc_putin_array,im,hdr,outsize,trim_off=trim_off,silent=silent, FULL=full, $
    	NEW=new, _extra=ex 
;+
; $Id: scc_putin_array.pro,v 2.13 2013/10/17 22:27:35 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_putin_array.pro
;               
; Purpose   : returns image in user defined array size
;               
; Explanation: 
;
; Use       : IDL> img_out = scc_putin_array(im, hdr,outsize)
;    
; Inputs    : im - any SECCHI image
;             hdr -image header (FITS or SECCHI header structure)
;
; Optional Input:
;             outsize - outsize must be a multiple of 256; implies /FULL
;               
; Outputs   : img - imaging area; floating point array  
;
; Side Effects : Rebin images if image is large then outsize. Buffers
;                image if outsize is larger then outsize.
;
; Keywords  : trim_off - if set then outsize is set to 2176
;   	    /NEW    Re-init common output_array; implies /FULL; otherwise
;   	    	    maintains what was last set in scc_make_array.pro
;   	    /FULL   Place subfield in FFV array
;
; Calls     : SCC_FITSHDR2STRUCT, FITSHEAD2WCS, WCS_GET_COORD
;
; Category    : Admistration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU October 2006
;               
; $Log: scc_putin_array.pro,v $
; Revision 2.13  2013/10/17 22:27:35  nathan
; update to work properly with subfields
;
; Revision 2.12  2011/12/21 23:32:37  nathan
; do not default to full=1 (for subfield case)
;
; Revision 2.11  2011/12/16 16:57:54  nathan
; fix case where subfield input has cols/rows outside of imaging area
;
; Revision 2.10  2011/12/07 23:03:44  nathan
; make /trim_off work as expected for trimmed input images
;
; Revision 2.9  2010/08/17 18:00:51  nathan
; accomodate different instrument ccd sizes
;
; Revision 2.8  2010/08/11 17:55:54  nathan
; Correct implementation of /NEW so it replicates what scc_make_array.pro does
; and clarify /FULL keyword (so it works outside of secchi_prep)
;
; Revision 2.7  2008/07/02 19:18:47  nathan
; fixed histinfo
;
; Revision 2.6  2008/05/09 17:03:19  nathan
; fixed crpix calculation (bug 265)
;
; Revision 2.5  2008/02/19 16:24:23  nathan
; make /NEW irrelevant--depend on outsize input
;
; Revision 2.4  2008/02/12 14:54:41  secchia
; nr - added /NEW keyword
;
; Revision 2.3  2008/02/11 17:16:31  nathan
; fixed outsize input
;
; Revision 2.2  2008/01/17 20:23:26  colaninn
; made many changes for sub-fields
;
; Revision 2.1  2007/12/13 17:01:42  colaninn
; add Resizing warning
;
; Revision 2.0  2007/10/17 16:26:31  colaninn
; now uses DSTART1(2) DSTOP(1)(2) keywords
;
; Revision 1.18  2007/10/05 20:58:42  nathan
; use summed difference to change header values
;
; Revision 1.17  2007/08/28 19:10:41  colaninn
; added update of hdr.DSTOP1(2)
;
;-
COMMON output_array, out

;ON_ERROR,2

  info = '$Id: scc_putin_array.pro,v 2.13 2013/10/17 22:27:35 nathan Exp $'
  len=strlen(info)
  histinfo=strmid(info,5,len-20)

IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

IF datatype(out) EQ 'UND' THEN new=1 

IF n_params() LT 3 THEN outsize=2048 ;ELSE full=1
IF keyword_set(TRIM_OFF) THEN outsize=2176

ccdfac=1.
IF hdr.INSTRUME EQ 'LASCO' THEN ccdfac=0.5  ; relative to SECCHI 2048x2048
IF hdr.INSTRUME EQ 'EIT' THEN ccdfac=0.5
IF hdr.INSTRUME EQ 'AIA' THEN ccdfac=2.     ; this reflects full-res AIA

IF keyword_set(NEW) or keyword_set(FULL) THEN BEGIN
    ; normally this is done in scc_make_array.pro
    start = SCCRORIGIN(hdr[0])
    IF keyword_set(TRIM_OFF) THEN offsetarr=[1,2176*ccdfac,1,2176*ccdfac] ELSE $
    ; --If input not 2176x2176, need to put it in the right place.
    ; --Offset is equivalent to r1(2)col(row)
    offsetarr = [start[0],start[0]+(2048*ccdfac-1),start[1],start[1]+(2048*ccdfac-1)]

    out = create_struct('outsize',[outsize,outsize],$
                        'offset',offsetarr,$
                        'readsize',[2048>outsize,2048>outsize]*ccdfac,$
			'summed',ceil(2048.*ccdfac/outsize))
ENDIF

output_img = im

IF (hdr.naxis1 NE out.outsize[0]) OR (hdr.naxis2 NE out.outsize[1]) THEN BEGIN
 
  ;--Compare CCD readout 
  w = where(out.readsize-1 EQ [hdr.R2COL-hdr.R1COL,hdr.R2ROW-hdr.R1ROW],nosub)
  IF nosub NE 2 THEN BEGIN
    sfac=1./2^(hdr.SUMMED-1)
    output_img = fltarr(out.readsize*sfac)
 ;--Calculate subfield coordinates in output image
    x1 =  fix(((hdr.R1COL-out.offset[0])>0)*sfac)
    x2 =  fix((hdr.R2COL-out.offset[0])*sfac)
    y1 =  fix(((hdr.R1ROW-out.offset[2])>0)*sfac)
    y2 =  fix((hdr.R2ROW-out.offset[2])*sfac)

;--Calculate subfield coordinates in input image
    x1i=((out.offset[0]-hdr.r1col)>0)*sfac
    y1i=((out.offset[2]-hdr.r1row)>0)*sfac
    x2i=((hdr.r2col-hdr.r1col-((hdr.r2col-out.offset[1])>0)))*sfac
    y2i=((hdr.r2row-hdr.r1row-((hdr.r2row-out.offset[3])>0)))*sfac
        
    output_img[x1,y1] = im[x1i:x2i,y1i:y2i]
    IF ~keyword_set(SILENT) and keyword_set(FULL) THEN message,'SUB-FIELD PUT IN FULL FIELD',/info

    hdr.DSTART1 = x1+1    
    hdr.DSTART2 = y1+1
    hdr.DSTOP1 = x2+1
    hdr.DSTOP2 = y2+1
    hdr.CRPIX1 = hdr.crpix1+x1
    hdr.CRPIX1A = hdr.CRPIX1A+x1
    hdr.CRPIX2 = hdr.crpix2+y1
    hdr.CRPIX2A = hdr.CRPIX2A+y1
    hdr.R1COL=out.offset[0]
    hdr.R2COL=out.offset[1]
    hdr.R1ROW=out.offset[2]
    hdr.R2ROW=out.offset[3]
  ENDIF

  ;--Compare Summed
  
  IF out.summed NE 2^(hdr.summed-1) THEN BEGIN
    sumdif = max(size(output_img,/dim))/max(float(out.outsize))  
    output_img = rebin(output_img,out.outsize[0],out.outsize[1]) 
    IF ~keyword_set(SILENT) THEN message,'IMAGE REBINNED FOR OUTPUT',/info
  
    help, sumdif
    hdr.summed = (alog(out.summed)/alog(2))+1
   ;-- the header keyword values reflect the original SUMMED value
    hdr.DSTOP1 = fix(hdr.DSTOP1/sumdif)
    hdr.DSTOP2 = fix(hdr.DSTOP2/sumdif)
  
    hdr.CRPIX1 = 0.5+(hdr.crpix1-0.5)/sumdif
    hdr.CRPIX1A= 0.5+(hdr.CRPIX1A-0.5)/sumdif
    hdr.CRPIX2 = 0.5+(hdr.crpix2-0.5)/sumdif
    hdr.CRPIX2A= 0.5+(hdr.CRPIX2A-0.5)/sumdif
    hdr.CDELT1 =  hdr.CDELT1*sumdif
    hdr.CDELT2 =  hdr.CDELT2*(sumdif)
    hdr.CDELT1A =  hdr.CDELT1A*(sumdif)
    hdr.CDELT2A =  hdr.CDELT2A*(sumdif)
  ENDIF 
  
  IF ~keyword_set(silent) THEN message, 'HEADER UPDATED',/inform

  s = size(output_img,/dim)

  hdr.NAXIS1 = s[0]
  hdr.NAXIS2 = s[1]
  hdr.DSTOP1 = (hdr.DSTOP1)<s[0]
  hdr.DSTOP2 = (hdr.DSTOP2)<s[1]

  WCS = FITSHEAD2WCS(hdr)
  xycen = WCS_GET_COORD(WCS, [(hdr.NAXIS1-1.)/2., (hdr.NAXIS2-1.)/2.])
  hdr.XCEN=xycen[0]
  hdr.YCEN=xycen[1]
  hdr =SCC_UPDATE_HISTORY(hdr,histinfo+',CHANGED IMAGE SIZE')
ENDIF

RETURN,output_img
END
