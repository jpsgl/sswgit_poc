function get_exptime,hdr, _EXTRA=_extra
;+
; $Id: get_exptime.pro,v 1.3 2007/09/25 16:10:29 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_exptime
;               
; Purpose   : This program returns the correct exposure time of a give image
;               
; Explanation: currently, this program just returns the exposure time
;              from the header
;               
; Use       : IDL> exptime = get_exptime(hdr)
;    
; Inputs    : hdr - image header, either fits or SECCHI structure
;               
; Outputs   : exptime - floating point number of time between open and
;                       close of shutter (seconds)
;
; Calls     : SCC_FITSHDR2STRUCT
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU June 2006
;               
; $Log: get_exptime.pro,v $
; Revision 1.3  2007/09/25 16:10:29  nathan
; added _EXTRA keyword
;
; Revision 1.2  2007/01/24 21:16:36  colaninn
; correct header
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.1  2006/07/25 19:15:04  colaninn
; first draft
;
;-            
    
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)   
IF (TAG_NAMES(hdr,/STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

return,hdr.EXPTIME

end

