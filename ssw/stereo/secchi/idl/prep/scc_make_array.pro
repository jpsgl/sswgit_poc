function scc_make_array, filenames,headers,outout,silent=silent,outsize=outsize, trim_off=trim_off,polariz_on=polariz_on,full=full, _extra=ex
;+
;$Id: scc_make_array.pro,v 2.7 2011/12/21 23:30:53 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_make_array.pro
;               
; Purpose   : creates output array from header information
;               
; Explanation: 
;
; Use       : IDL> img_out = scc_make_array(filenames)
;    
; Inputs    : filenames - string array of FITS filenames 
;               
; Outputs   : array - flaoting point cube 
;
; Side Effects : Rebin images if image is large then outsize. Buffers
;                image if outsize is larger then outsize.
;
; Keywords  : trim_off - if set then outsize is set to 2176
;   	    	/FULL 	place the subfield in a FFV image array when scc_putin_array is called
;
; Calls     : SCC_FITSHDR2STRUCT, SCCRORIGIN, SCCREADFITS
;
; Category    : Admistration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU JANUARY 2008
;               
; $Log: scc_make_array.pro,v $
; Revision 2.7  2011/12/21 23:30:53  nathan
; rewrite the whole subfield section
;
; Revision 2.6  2010/08/11 17:26:35  nathan
; fix 2048 value in offset array
;
; Revision 2.5  2008/09/22 19:15:27  nathan
; allow outsize input to be an array
;
; Revision 2.4  2008/07/14 18:29:10  nathan
; fixed comment logging
;
;-
COMMON output_array, out

ON_ERROR,2

num = n_elements(filenames)
void = sccreadfits(filenames,mhdr,/nodata)

IF keyword_set(TRIM_OFF) THEN BEGIN
  outsize =[2176,2176]
  readsize = [2176,2176]
  offset = [1,2176,1,2176]
  summed = 1
ENDIF ELSE BEGIN

    start = SCCRORIGIN(mhdr[0])
    offset = [start[0],start[0]+2047,start[1],start[1]+2047]
    ; --Offset is equivalent to r1(2)col(row)
    
    ;--Exclude over/underscan
    r = [mhdr.R2COL-mhdr.R1COL+mhdr.R2ROW-mhdr.R1ROW]
    w = where(r LT 2047*2, sub_num)
 
    ;--Find maximum extent of CCD readout if there are sub_fields
    IF sub_num GT 0 and not keyword_set(FULL) THEN BEGIN
    ;--Keyword FULL place the subfield in a standard full image

    	;--Check for same spacecraft and detector
    	xxx = where(mhdr.OBSRVTRY NE mhdr[0].OBSRVTRY,nob)
    	IF nob NE 0 THEN message,'Cannot process spacecraft A & B with subfields'
    	xxx = where(mhdr.DETECTOR NE mhdr[0].DETECTOR,nde)
    	IF nde NE 0 THEN message,'Cannot process mixed detectors with subfields'
  
   	offset = [start[0]>min(mhdr.R1COL),(start[0]+2047)<max(mhdr.R2COL),$
    	    	  start[1]>min(mhdr.R1ROW),(start[1]+2047)<max(mhdr.R2ROW)]
    ENDIF
    readsize = [offset[1]-offset[0]+1,offset[3]-offset[2]+1]

    ; /NO_TRIM option for subfield currently not supported
        
  ;--Create output array from input size
    IF keyword_set(OUTSIZE) THEN BEGIN
    	summed = max(readsize)/outsize[0]
    	outsize = readsize/summed
    ENDIF ELSE BEGIN
    	IF num GT 1 THEN BEGIN	
    	    void = max(histogram(mhdr.SUMMED,omin=mn),mxpos)
    	    summed = 2^(mn+mxpos-1)
    	ENDIF ELSE summed = 2^(mhdr.SUMMED-1) 
    	outsize = readsize/summed
    ENDELSE
ENDELSE

;--Find array length of combined images
IF keyword_set(POLARIZ_ON) THEN BEGIN 
  combined =  where(mhdr.POLAR ge 1001,num_comb)
  polar_image = where(mhdr.POLAR lt 1001,num_pol)
  IF num_pol MOD 3 NE 0 THEN  message,'Polarized images must multiples of 3 with /POLARIZ_ON keyword'
  n = num_comb + num_pol/3
ENDIF ELSE n = num

out = create_struct('outsize',outsize,'offset',offset,'readsize',readsize,'summed',summed)
outout = max(outsize)

;--Create output arrays
images = fltarr(outsize[0],outsize[1],n)
headers=replicate(def_secchi_hdr(silent=silent ),n)

RETURN, images
END
