function euvi_cosmicr, image,hdr


nsigmas=3.5 ; threshold for cosmic ray detection
sbin   =7   ; Kernel size for median filter
minthr =0.6 ; Minimum relative detection threshold

; for the brightest parts of the images this last threshold will be the highest
; one.
; at low intensities, the threshold derived from photon statistics takes over.
 
s=size(image)
valid=WHERE(image GT 0,cnt)

IF (cnt GT 0L) THEN BEGIN
  imalim=INTARR(s[1],s[2])
  imamed=FLTARR(s[1],s[2])
  bias = get_biasmean(hdr)
  
  imamed[valid]=MEDIAN(image[valid],sbin)  
  imamed[valid]=temporary(imamed[valid])-bias
  
  imalim[valid]=imamed[valid]+bias+$
         ( fix(nsigmas*SQRT( 2.7 + 0.2*imamed[valid] ))>       $
           fix(imamed[valid]*minthr) )

  hit=WHERE(image GT imalim,ch)
  imacorr=image
  IF (ch GT 0) THEN imacorr[hit]=imamed[hit]+bias

ENDIF ELSE ch=0L

RETURN, imacorr
END
