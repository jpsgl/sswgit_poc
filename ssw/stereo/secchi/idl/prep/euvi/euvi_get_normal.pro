function euvi_get_normal, hdr, SILENT=silent
;+
; $Id: euvi_get_normal.pro,v 1.4 2007/09/25 16:31:15 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : euvi_get_normal
;               
; Purpose   : This function returns the normalization factor for an
;             EUVI image.
;               
; Explanation: 
;               
; Use       : IDL> normal = euvi_get_normal(hdr)
;    
; Inputs    : hdr -image header, either fits or SECCHI structure
;
; Outputs   : normal - floating point number to divide the image by
;                      to normalize the image to a 'CLEAR' filter
;
; Keywords  :
;
; Common    : 
;
; Calls     : SCC_FITSHDR2STRUCT
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU September 2006
;
; $Log: euvi_get_normal.pro,v $
; Revision 1.4  2007/09/25 16:31:15  nathan
; really add /SILENT
;
; Revision 1.3  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.2  2006/12/01 15:41:40  colaninn
; added seperate filter values
;
; Revision 1.1  2006/10/03 15:30:16  nathan
; moved from dev/analysis/euvi
;
; Revision 1.1  2006/09/22 18:37:27  colaninn
; created from inline code
;
;- 

IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)

IF (TAG_NAMES(hdr, /STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE,'ONLY SECCHI HEADER STRUCTURE ALLOWED'

CASE hdr.WAVELNTH OF
  '171' : BEGIN 
     CASE hdr.FILTER OF
       'OPEN' : filter_factor =1    ; no filter
       'S1' : filter_factor = 0.49  ; 1500A filter
       'S2' : filter_factor = 0.49  ; 1500A filter
       'DBL' : filter_factor = 0.41 ; 3000A filter
       ELSE :  message, 'FILTER was not found'
     ENDCASE 
   END
  '195' : BEGIN
    CASE hdr.FILTER OF
       'OPEN' : filter_factor =1    ; no filter
       'S1' : filter_factor = 0.49  ; 1500A filter
       'S2' : filter_factor = 0.49  ; 1500A filter
       'DBL' : filter_factor = 0.41 ; 3000A filter
        ELSE :  message, 'FILTER was not found'
     ENDCASE  
   END
  '284' : BEGIN
     CASE hdr.FILTER OF
       'OPEN' : filter_factor =1    ; no filter
       'S1' : filter_factor = 0.33  ; 1500A filter
       'S2' : filter_factor = 0.33  ; 1500A filter
       'DBL' : filter_factor = 0.24 ; 3000A filter
       ELSE :  message, 'FILTER was not found'
     ENDCASE  
   END
  '304' : BEGIN
    CASE hdr.FILTER OF  
       'OPEN' : filter_factor =1    ; no filter
       'S1' : filter_factor = 0.29  ; 1500A filter
       'S2' : filter_factor = 0.29  ; 1500A filter
       'DBL' : filter_factor = 0.22 ; 3000A filter
       ELSE :  message, 'FILTER was not found'
     ENDCASE  
  END
  ELSE: message, 'WAVELNTH was not found'
ENDCASE

RETURN, filter_factor
END
