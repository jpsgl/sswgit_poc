function euvi_correction, im, hdr,normal_off=normal_off, bias_off=bias_off, exptime_off=exptime_off, calimg_off=calimg_off, dn2p_off=dn2p_off, CALFAC_OFF=calfac_off, sebip_off=sebip_off,silent=silent,_EXTRA=ex

;+
; $Id: euvi_correction.pro,v 1.12 2007/09/25 16:25:13 thompson Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : euvi_correction
;               
; Purpose   : This function returns corrected images.
;               
; Explanation: The default correction procedure involves; subtracting
;        the CCD bias, multiplying by the calibration factor and vignetting
;        function, and dividing by the exposure time. Any of these operations
;        can turned off using the keywords. 
;               
; Use       : IDL> cal_image = euvi_correction(image,hdr)
;    
; Inputs    : image - level 0.5 image in DN (long)
;             hdr -image header, either fits or SECCHI structure
;
; Outputs   : cal_image - image convert from  DN (long) to corrected
;                          DN/s (float)
;
; Keywords  :NORMAL_OFF - set to return image without normalization
;                        to open filter position applied
;            DN2P_OFF - set to return image without convertion from DN
;                      to detected photons applied
;   	     CALFAC_OFF - same as DN2P_OFF
;            BIAS_OFF - set to return image without bias subtraction applied
;            EXPTIME_OFF - set to return image without expsure time normalized
;            CALIMG_OFF - set to return image without vignetting
;                        (flat-field) correction 
;            SEBIP_OFF - set to return image without SEB IP correction
;                        applied
;
; Common    : 
;
; Calls     : SCC_FITSHDR2STRUCT, SCC_SEBIP, GET_EXPTIME, GET_BIASMEAN,
;             GET_CALIMG, EUVI_GET_NORMAL, GET_CALFAC, SCC_UPDATE_HISTORY
; 
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU August 2006
;               
; $Log: euvi_correction.pro,v $
; Revision 1.12  2007/09/25 16:25:13  thompson
; Put _EXTRA back in get_exptime call
;
; Revision 1.11  2007/09/25 16:17:10  thompson
; Removed _EXTRA from get_exptime call
;
; Revision 1.10  2007/09/24 21:29:30  nathan
; Add _EXTRA to subpro calls to propagate /SILENT
;
; Revision 1.9  2007/09/24 20:59:02  nathan
; Added /CALFAC_OFF option to be consistent with other telescopes
;
; Revision 1.8  2007/08/24 19:28:25  nathan
; updated info messages and hdr history
;
; Revision 1.7  2007/08/23 23:57:02  nathan
; added INFO messages
;
; Revision 1.6  2007/05/21 20:16:06  colaninn
; added silent keyword
;
; Revision 1.5  2007/02/26 19:11:13  colaninn
; corrected error with dn2p_off
;
; Revision 1.4  2007/02/06 21:48:45  colaninn
; added scc_update_history.pro
;
; Revision 1.3  2006/12/01 15:42:25  colaninn
; re-ordered get programs
;
; Revision 1.2  2006/11/21 22:13:50  colaninn
; made updates for Beta 2
;
; Revision 1.1  2006/10/03 15:30:16  nathan
; moved from dev/analysis/euvi
;
; Revision 1.4  2006/09/26 20:34:41  colaninn
; changed for correct history
;
; Revision 1.3  2006/09/22 18:36:14  colaninn
;  added scc_sebip.pro euvi_get_normal.pro and get_calfac.pro and 
;  expanded history output
;
; Revision 1.2  2006/09/15 21:26:25  colaninn
; *** empty log message ***
;
; Revision 1.1  2006/08/24 18:03:54  colaninn
; seperated code from euvi_prep
;
;-     
IF datatype(ex) EQ 'UND' THEN ex = create_struct('blank',-1)
IF keyword_set(silent) THEN ex = CREATE_STRUCT('silent',1,ex)
info="$Id: euvi_correction.pro,v 1.12 2007/09/25 16:25:13 thompson Exp $"
len=strlen(info)
version='Applied '+strmid(info,5,len-10)
IF ~keyword_set(silent) THEN message,version,/inform

;--Check Header-------------------------------------------------------
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

IF (hdr[0].DETECTOR NE 'EUVI') THEN $
message,'Calibration for EUVI DETECTOR only'
;----------------------------------------------------------------------

;--Add file version to header history
hdr = scc_update_history(hdr,'EUVI_CORRECTION.PRO,'+version)

;------------------------------------------------------------------------
;--Correct for SEB IP(ON)
IF ~keyword_set(sebip_off) THEN im = SCC_SEBIP(im,hdr,ip_flag, _extra=ex)
; hdr modified in sebip

;--Convert to DN/S (ON)
IF keyword_set(exptime_off) THEN exptime = 1.0 ELSE BEGIN
  exptime = GET_EXPTIME(hdr,_EXTRA=ex)
  IF exptime NE 1.0 THEN BEGIN
   hdr = SCC_UPDATE_HISTORY(hdr,'Exposure Normalized to 1 Second',value=exptime)
   IF ~keyword_set(SILENT) THEN message,'Exposure Normalized to 1 Second',/info
  ENDIF
ENDELSE

;-- Bias Subtraction(ON)
IF keyword_set(bias_off) THEN biasmean = 0.0 ELSE BEGIN
  biasmean = GET_BIASMEAN(hdr,_extra=ex)
  IF biasmean NE 0.0 THEN BEGIN
    hdr = SCC_UPDATE_HISTORY(hdr,'Bias Subtracted',value=biasmean)
    IF ~keyword_set(SILENT) THEN message,'Bias Subtracted',/info
  ENDIF
  hdr.OFFSETCR=biasmean
ENDELSE

;--Normalize to Open filter position (ON)
IF keyword_set(normal_off) THEN normal = 1.0 ELSE BEGIN
    normal = EUVI_GET_NORMAL(hdr,_EXTRA=ex)
 IF normal NE 1.0 THEN BEGIN
    IF ~keyword_set(SILENT) THEN message,'Normalized to Open Filter Position',/info
    hdr = SCC_UPDATE_HISTORY(hdr,'Normalized to Open Filter Position',value=normal)
  ENDIF
ENDELSE

;--Photometric Correction - DN to detected photons - rough calculation 
IF keyword_set(dn2p_off) or keyword_set(CALFAC_OFF) THEN photons_dn = 1.0 ELSE BEGIN 
  photons_dn = GET_CALFAC(hdr,_EXTRA=ex)
  IF photons_dn NE 1.0 THEN BEGIN
    hdr = SCC_UPDATE_HISTORY(hdr,'Photometric Correction - DN to Detected Photons',value=photons_dn)
    IF ~keyword_set(SILENT) THEN message,'Applied Calibration Factor '+trim(photons_dn),/info
  ENDIF
ENDELSE

;--Correction for flat field and vignetting (ON)
IF keyword_set(calimg_off) THEN calimg = 1.0 ELSE BEGIN	
  calimg = GET_CALIMG(hdr,fn,_EXTRA=ex)
  IF (size(calimg))(0) GT 1 THEN $
    hdr = SCC_UPDATE_HISTORY(hdr,'Applied Flat Field/Vignetting '+fn)
ENDELSE

;------------------------------------------------------------------------
;--Apply Correction 
im = ((temporary(im)-biasmean[0])*photons_dn[0]/(exptime[0]*normal[0]))*calimg
;----------------------------------------------------------------------

RETURN,im
END
