;+
; Project     :	STEREO - SECCHI
;
; Name        :	cor1_img_trim()
;
; Purpose     :	Wrapper around scc_img_trim for old upside-down COR1 files
;
; Category    :	SECCHI COR1 Analysis
;
; Explanation :	This routine is used as a wrapper around SCC_IMG_TRIM to handle
;               COR1 data from the early part of the mission that were
;               processed upside-down.  Upside-down images are detected by the
;               date on which they were processed, and whether or not they have
;               been rectified.  Not only are the images trimmed, but they are
;               also turned rightside-up.
;
; Syntax      :	Result = COR1_IMG_TRIM(IM, HDR  [, /VERBOSE ])
;
; Examples    :	IM = SCCREADFITS(FILENAME, HDR)
;               IM = COR1_IMG_TRIM(IM, HDR)
;
; Inputs      :	IM      = Any COR1 image
;               HDR     = Image header (FITS or SECCHI header structure)
;
; Opt. Inputs :	None.
;
; Outputs     :	The result of the function is the trimmed image, turned
;               rightside-up.
;
;               HDR is also modified to reflect the trimmed image.
;
; Opt. Outputs:	None.
;
; Keywords    :	VERBOSE = If set, then some extra output is written to the
;                         screen.
;
; Calls       :	
;
; Common      :	None.
;
; Restrictions:	Only useful for COR1 data.
;
; Side effects:	IF not already a structure, HDR is converted into a SECCHI
;               header structure.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 20-Mar-2007, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
function cor1_img_trim, im, hdr, verbose=verbose
on_error, 2
;
;  Make sure that HDR is a SECCHI structure.
;
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'
;
; Determine whether or not the image needs to be inverted.
;
if (hdr.detector eq 'COR1') and (hdr.date lt '2007-01-11') and $
  (hdr.rectify eq 'T') then invert = 1 else invert = 0
;
;  If inversion is needed, fool SCC_IMG_TRIM by flipping the name of the
;  spacecraft.
;
if invert then begin
    obsrvtry = hdr.obsrvtry
    if obsrvtry eq 'STEREO_A' then hdr.obsrvtry = 'STEREO_B' else $
      hdr.obsrvtry = 'STEREO_A'
endif
;
;  Call SCC_IMG_TRIM to do the actual trimming.
;
image = scc_img_trim(im, hdr)
;
;  If inversion is needed, then reset the observatory name and invert the
;  image.  Modify CRPIX1 and CRPIX2.
;
if invert then begin
    hdr.obsrvtry = obsrvtry
    image = rotate( temporary(image), 2 )
    hdr.crpix1 = hdr.naxis1 + 1 - hdr.crpix1
    hdr.crpix2 = hdr.naxis2 + 1 - hdr.crpix2
endif
;
return, image
end
