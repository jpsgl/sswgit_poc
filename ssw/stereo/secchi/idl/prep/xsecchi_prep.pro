;+
;$Id:
; Project   : STEREO SECCHI
;
; Name      : XSECCHI_PREP
;
; Purpose   : This procedure is a GUI to set all the keyword
;             options for SECCHI_PREP.
;                          
; Use       : IDL> xsecchi_prep,headers,images 
;    
; Inputs    : NONE
;
;Optiotal Output : images - array of processed images, the returned
;                            array is the size of the largest image,
;                            smaller images not scaled
;                  headers - array of level 1 header sturctures
;
; Keywords  : filename - input string of level 0.5 to be processed
;
; Common    : xsecchi_common
;
; Calls     : SECCHI_PREP, SCCLISTER
;
; Category    : Calibration
;
; PROCEDURE:
;	The various widgets are setup and registered. The user can
;	then load the filenames from a file or using SCCLISTER. All
;	the keywords for SECCHI_PREP are shown in the GUI
;	interface. The user can select optinal procedures or un-select
;	defualt procedures. Then the user can call SECCHI_PREP.
;
; Written     : Robin C Colaninno NRL/GMU Feb 2007
;        
;$Log: xsecchi_prep.pro,v $
;Revision 1.7  2008/06/23 19:34:13  colaninn
;switched output
;
;Revision 1.6  2007/04/16 21:54:02  nathan
;fix Log syntax
;
;-

PRO xsecchi_prep_event, ev

WIDGET_CONTROL, ev.TOP, GET_UVALUE=state,/no_copy
common xsecchi_common, newim,newhdr

;------------SETTING-----------------------------
;--Write----------------------------------------- 
IF (ev.ID EQ state.write.write0) THEN BEGIN
  state.keyword.write_fts =0
  state.keyword.write_png = 0
  state.keyword.write_jpg = 0
  WIDGET_CONTROL, state.write.write4,set_value=''
ENDIF
IF (ev.ID EQ state.write.write1) THEN state.keyword.write_fts = ~state.keyword.write_fts
IF (ev.ID EQ state.write.write2) THEN state.keyword.write_png = ~state.keyword.write_png
IF (ev.ID EQ state.write.write3) THEN state.keyword.write_jpg = ~state.keyword.write_jpg
IF (ev.ID EQ state.write.write4) THEN state.keyword.savepath  = ev.value
IF (ev.ID EQ state.write.write5) THEN BEGIN
  dir = DIALOG_PICKFILE(/read,/directory)
  WIDGET_CONTROL, state.write.write4,set_value=dir
  state.keyword.savepath = dir
ENDIF 


;--Formatting------------------------------------

IF (ev.ID EQ state.format.format1) THEN BEGIN
  outsize_array=['','2048','1024',' 512',' 256',' 128','  64']
  IF ev.index EQ 0 THEN state.keyword.outsize = 0 ELSE $
   state.keyword.outsize   = float(outsize_array[ev.index])
ENDIF   
IF (ev.ID EQ state.format.format2) THEN state.keyword.rotate_on = ~state.keyword.rotate_on
IF (ev.ID EQ state.format.format3) THEN state.keyword.color_on  = ~state.keyword.color_on
IF (ev.ID EQ state.format.format4) THEN state.keyword.date_on   = ~state.keyword.date_on
IF (ev.ID EQ state.format.format5) THEN state.keyword.logo_on   = ~state.keyword.logo_on

;--Mask------------------------------------------
IF (ev.ID EQ state.mask.mask1) THEN state.keyword.smask_on = ~state.keyword.smask_on
IF (ev.ID EQ state.mask.mask2) THEN state.keyword.tele_only = ~state.keyword.tele_only
IF (ev.ID EQ state.mask.mask3) THEN print,'Sorry not working yet'
IF (ev.ID EQ state.mask.mask4) THEN state.keyword.fill_mean = ~state.keyword.fill_mean
IF (ev.ID EQ state.mask.mask5) THEN state.keyword.fill_value = ev.value

;--Polariz---------------------------------------
IF (ev.ID EQ state.polariz.polariz1) THEN state.keyword.polariz_on = ~state.keyword.polariz_on
IF (ev.ID EQ state.polariz.polariz2) THEN BEGIN
   state.keyword.polariz_on = ~state.keyword.percent
   state.keyword.percent = ~state.keyword.percent
ENDIF
IF (ev.ID EQ state.polariz.polariz3) THEN BEGIN
   state.keyword.polariz_on = ~state.keyword.pb
   state.keyword.pb = ~state.keyword.pb
ENDIF
IF (ev.ID EQ state.polariz.polariz4) THEN BEGIN
   state.keyword.polariz_on = ~state.keyword.mu
   state.keyword.mu = ~state.keyword.mu
ENDIF
IF (ev.ID EQ state.polariz.polariz0) THEN BEGIN
  state.keyword.polariz_on =0
  state.keyword.percent = 0
  state.keyword.pb = 0
  state.keyword.mu = 0
ENDIF

;--Default---------------------------------------
IF (ev.ID EQ state.default.default1) THEN BEGIN
  state.keyword.trim_off = ~state.keyword.trim_off
  state.keyword.calibrate_off = ~state.keyword.trim_off
  Widget_Control, state.default.default0, Set_Button= state.keyword.calibrate_off
ENDIF

IF (ev.ID EQ state.default.default0) OR (ev.ID EQ state.default.default1) THEN BEGIN
  state.keyword.calibrate_off = ~state.keyword.calibrate_off
  state.keyword.exptime_off   = ~state.keyword.calibrate_off
  state.keyword.bias_off      = ~state.keyword.calibrate_off
  state.keyword.sebip_off     = ~state.keyword.calibrate_off
  state.keyword.calimg_off    = ~state.keyword.calibrate_off
  state.keyword.new_calimg  = 0
  state.keyword.calimg_filename = ''
  state.keyword.nornal_off    = ~state.keyword.calibrate_off
  state.keyword.dn2p_off      = ~state.keyword.calibrate_off
  state.keyword.calfac_off    = ~state.keyword.calibrate_off
  state.keyword.shutterless_off = ~state.keyword.calibrate_off
  
  Widget_Control, state.default.default2, Set_Button= state.keyword.exptime_off
  Widget_Control, state.default.default3, Set_Button= state.keyword.bias_off
  Widget_Control, state.default.default4, Set_Button= state.keyword.sebip_off
  Widget_Control, state.default.default5, Set_Button= state.keyword.calimg_off
  Widget_Control, state.default.default6, Set_Button=0
  Widget_Control, state.default.default7, SET_VALUE=''
  Widget_Control, state.default.default9, Set_Button= state.keyword.nornal_off
  Widget_Control, state.default.default10, Set_Button= state.keyword.dn2p_off
  Widget_Control, state.default. default12, Set_Button= state.keyword.calfac_off
  Widget_Control, state.default.default14, Set_Button= state.keyword.shutterless_off
ENDIF

IF (ev.ID EQ state.default.default2) THEN BEGIN 
  state.keyword.exptime_off = ~state.keyword.exptime_off
  Widget_Control, state.default.default0,Set_Button=0
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default3) THEN BEGIN
  state.keyword.bias_off    = ~state.keyword.bias_off
  Widget_Control, state.default.default0,Set_Button=0 
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default4) THEN BEGIN
  state.keyword.sebip_off   = ~state.keyword.sebip_off
  Widget_Control, state.default.default0,Set_Button=0 
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default5) THEN BEGIN 
  state.keyword.calimg_off  = ~state.keyword.calimg_off
  Widget_Control, state.default.default0,Set_Button=0
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default6) THEN BEGIN 
  state.keyword.new_calimg  = ~state.keyword.new_calimg
  Widget_Control, state.default.default0,Set_Button=0 
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default7) THEN BEGIN 
  state.keyword.calimg_filename = ev.value
  Widget_Control, state.default.default0,Set_Button=0
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default9) THEN BEGIN 
  state.keyword.nornal_off  = ~state.keyword.nornal_off
  Widget_Control, state.default.default0,Set_Button=0
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default10) THEN BEGIN 
  state.keyword.dn2p_off   = ~state.keyword.dn2p_off
  Widget_Control, state.default.default0,Set_Button=0 
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default12) THEN BEGIN
  state.keyword.calfac_off = ~state.keyword.calfac_off
  Widget_Control, state.default.default0,Set_Button=0
  Widget_Control, state.default.default0,Set_Button=0
ENDIF
IF (ev.ID EQ state.default.default14) THEN BEGIN 
  state.keyword.shutterless_off = ~state.keyword.shutterless_off
  Widget_Control, state.default.default0,Set_Button=0
  Widget_Control, state.default.default0,Set_Button=0
ENDIF

;~~~~~~~~~~~~~ACTION~~~~~~~~~~~~~~~~~~~~~~~~~~~

;--Reload XSECCHI_PREP ----------------------
IF (ev.ID EQ state.action.action3) THEN BEGIN
  WIDGET_CONTROL, ev.TOP, /DESTROY
  xsecchi_prep
  RETURN
ENDIF

;--Quit XSECCHI_PREP ------------------------
IF (ev.ID EQ state.action.action2) THEN BEGIN
   WIDGET_CONTROL, ev.TOP, /DESTROY
  RETURN
ENDIF

;--Start SCCLISTER
IF (ev.ID EQ state.action.action4) THEN BEGIN
  l = scclister()
  IF datatype(l) EQ 'STC' THEN BEGIN
    (*state.file) = l.(0) 
     n=n_elements(*state.file)
     display_text = strarr(n)
     FOR i=0,n-1 DO BEGIN
       IF strlen((*state.file)[i]) GT 75 THEN $
         display_text[i]=('...'+strmid((*state.file)[i],strlen((*state.file)[i])-72,$
         strlen((*state.file)[i])-1)) else display_text =(*state.file)[i]
     ENDFOR
     Widget_Control,state.action.action6, set_value =display_text 
  ENDIF
ENDIF

;--Load .SAV file
IF (ev.ID EQ state.action.action5) THEN BEGIN
  savefile = DIALOG_PICKFILE(/read,filter='*.sav',dialog_parent=base,/MUST_EXIST)
  IF strlen(savefile) GT 0 THEN BEGIN
    sobj=obj_new('IDL_Savefile',savefile)
    names = sobj->Names()
    sobj->restore,(names[0])
    obj_destroy,sobj
    exstr = 'IMAGES = '+names[0]
    status=execute(exstr)
  
    CASE datatype(IMAGES) OF
      'STC': (*state.file) = IMAGES.(0)
      'STR': (*state.file) = IMAGES
      ELSE:message,'DATA Type must be String or Structure',/infrom
    ENDCASE
    n=n_elements(*state.file)
    display_text = strarr(n)
    FOR i=0,n-1 DO BEGIN
      IF strlen((*state.file)[i]) GT 75 THEN $
        display_text[i]=('...'+strmid((*state.file)[i],strlen((*state.file)[i])-72,$
        strlen((*state.file)[i])-1)) else display_text =(*state.file)[i]
    ENDFOR
    Widget_Control,state.action.action6, set_value =display_text 
  ENDIF
ENDIF

;--Load TEXT file
IF (ev.ID EQ state.action.action7) THEN BEGIN
  textfile = DIALOG_PICKFILE(/read,dialog_parent=base,/MUST_EXIST)
  IF strlen(textfile) GT 0 THEN BEGIN
    ;--Initiate varibles
    txtline=''
    n = 0
    line=''
    ;--Start extraction loop
    openr,lun,textfile,/get_lun
    while (not eof(lun)) do begin
      readf,lun,line
      txtline=[txtline,line]
      n = n +1
    endwhile
    free_lun,lun

    (*state.file) = txtline[1:*]
    display_text = strarr(n)
    FOR i=0,n-1 DO BEGIN
      IF strlen((*state.file)[i]) GT 75 THEN $
        display_text[i]=('...'+strmid((*state.file)[i],strlen((*state.file)[i])-72,$
        strlen((*state.file)[i])-1)) else display_text =(*state.file)[i]
    ENDFOR
    Widget_Control,state.action.action6, set_value =display_text 
 ENDIF
ENDIF


IF (ev.ID EQ state.action.action1) THEN BEGIN
 IF n_elements(*state.file) GT 0 THEN BEGIN
  secchi_prep,(*state.file),newhdr,newim,$
  savepath        = state.keyword.savepath,$
  outsize         = state.keyword.outsize,$
  rotate_on       = state.keyword.rotate_on,$
  trim_off        = state.keyword.trim_off,$
  calibrate_off   = state.keyword.calibrate_off,$
  sebip_off       = state.keyword.sebip_off,$
  bias_off        = state.keyword.bias_off,$
  exptime_off     = state.keyword.exptime_off,$
  calimg_off      = state.keyword.calimg_off,$
  new_calimg      = state.keyword.new_calimg,$
  calimg_filename = state.keyword.calimg_filename,$
  write_fts       = state.keyword.write_fts,$
  write_png       = state.keyword.write_png ,$
  write_jpg       = state.keyword.write_jpg,$ 
  color_on        = state.keyword.color_on,$
  date_on         = state.keyword.date_on,$
  logo_on         = state.keyword.logo_on,$
  smask_on        = state.keyword.smask_on ,$
  tele_only       = state.keyword.tele_only,$
  mask_only       = state.keyword.mask_only ,$
  fill_mean       = state.keyword.fill_mean,$
  fill_value      = state.keyword.fill_value,$ 
  calfac_off      = state.keyword.calfac_off,$;COR only
  polariz_on      = state.keyword.polariz_on,$
  pb              = state.keyword.pb,$
  mu              = state.keyword.mu,$
  percent         = state.keyword.percent,$
  dn2p_off        = state.keyword.dn2p_off,$  ;EUVI only
  nornal_off      = state.keyword.nornal_off,$ 
  shutterless_off = state.keyword.shutterless_off ;HI only

 WIDGET_CONTROL, ev.TOP, /DESTROY
  
  RETURN
ENDIF ELSE message,'Please enter some files',/inform
ENDIF

widget_control, ev.top, set_uvalue=state,/no_copy 
END

PRO xsecchi_prep,hdr,im,filename=filename
common xsecchi_common, newim,newhdr

newim =0
newhdr = 0
file = ptr_new(/ALLOCATE_HEAP)
IF keyword_set(filename) THEN *file = filename 

font1 = '-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'   
if not allow_font(font1) then get_font, font1

  ;--Create the top-level base and the tab
   base = WIDGET_BASE(TITLE='XSECCHI_PREP',/COLUMN, /BASE_ALIGN_TOP)
   wTab = WIDGET_TAB(base,FONT=font1)
  
   ;--Common Action Base
   wcont_action = WIDGET_BASE(base,/ROW,/ALIGN_CENTER)
   
   ;--Create Base for each tab
   wTab1 = WIDGET_BASE(wTab, TITLE='  Load Filename  ',/ROW,YPAD=10,XPAD=10)
   wTab2 = WIDGET_BASE(wTab, TITLE='  Options  ',/ROW,YPAD=10,XPAD=10)
   wTab3 = WIDGET_BASE(wTab, TITLE='  Defaults  ', /ROW,YPAD=10,XPAD=10)

   ;--Tab 1 - Load Filaname-------------------------------------------------
   wcont_callw = WIDGET_BASE(wTab1,/COlUMN,YPAD=30)
   wcont_filel = WIDGET_BASE(wTab1,/COLUMN,YPAD=10)
   ;------------------------------------------------------------------------  

   ;--Tab 2 - Optional ----------------------------------------------------
   wconttopwrite = WIDGET_BASE(wTab2,/COLUMN,/FRAME,YPAD =10)
   label1 = WIDGET_LABEL(wconttopwrite,VALUE='Write   ',/ALIGN_LEFT,Font=font1)
   wcont_write = WIDGET_BASE(wconttopwrite,/COLUMN,/EXCLUSIVE)
   wcont_write_txt = WIDGET_BASE(wconttopwrite,/COLUMN)
   
   wconttopformat = WIDGET_BASE(wTab2,/COLUMN,/FRAME)
   label2 = WIDGET_LABEL(wconttopformat,VALUE='Format   ',/ALIGN_LEFT,Font=font1)
   wcont_format = WIDGET_BASE(wconttopformat,/COLUMN,/NONEXCLUSIVE)
   wcont_format_txt = WIDGET_BASE(wconttopformat,/ROW)

   wconttopmask = WIDGET_BASE(wTab2,/COLUMN,/FRAME)
   label3 = WIDGET_LABEL(wconttopmask,VALUE='Mask   ',/ALIGN_LEFT,Font=font1)
   wcont_mask = WIDGET_BASE(wconttopmask,/COLUMN,/NONEXCLUSIVE)
   wcont_mask_txt = WIDGET_BASE(wconttopmask,/COLUMN)

   wconttoppolariz = WIDGET_BASE(wTab2,/COLUMN,/FRAME)
   label4 = WIDGET_LABEL(wconttoppolariz,VALUE='Polarization ',/ALIGN_LEFT,Font=font1)
   label5 = WIDGET_LABEL(wconttoppolariz,VALUE=' COR Only ',/ALIGN_LEFT)
   wcont_polariz = WIDGET_BASE(wconttoppolariz,/COLUMN,/EXCLUSIVE)
   ;------------------------------------------------------------------------  
   
   ;--Tab 3 - Default ------------------------------------------------------
   wcont_default0 = WIDGET_BASE(WTab3,/COLUMN,/FRAME)
   wcont_default00 =  WIDGET_BASE(wcont_default0,/COLUMN)
   wcont_default01 =  WIDGET_BASE(wcont_default0,/COLUMN,/NONEXCLUSIVE)
   
   wcont_default1 = widget_base(wTab3,/Column,/frame)
   wcont_default12 =  WIDGET_BASE(wcont_default1,/COLUMN)
   wcont_default10 =  WIDGET_BASE(wcont_default1,/COLUMN,/NONEXCLUSIVE)
   wcont_default11 =  WIDGET_BASE(wcont_default1,/COLUMN)
  
   wcont_default2 = widget_base(wTab3,/COlumn,/frame)
   wcont_default20 =  WIDGET_BASE(wcont_default2,/COLUMN)
   wcont_default21 =  WIDGET_BASE(wcont_default2,/COLUMN,/NONEXCLUSIVE)
   wcont_default22 =  WIDGET_BASE(wcont_default2,/COLUMN)
   wcont_default23 =  WIDGET_BASE(wcont_default2,/COLUMN,/NONEXCLUSIVE)
   wcont_default24 =  WIDGET_BASE(wcont_default2,/COLUMN)
   wcont_default25 =  WIDGET_BASE(wcont_default2,/COLUMN,/NONEXCLUSIVE)


   ;-----------------------------------------------------------------------
   
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;$$$$$$$$$$$$$$$$$$$$$$$$$$$$~ BUTTONS ~$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   ;--Common Action Buttons
   action1 = WIDGET_BUTTON(wcont_action,VALUE=' SECCHI_PREP ',Font=font1)
   action2 = WIDGET_BUTTON(wcont_action,VALUE='     Quit    ',Font=font1)
   action3 = WIDGET_BUTTON(wcont_action,VALUE='     Reset    ',Font=font1)

   ;--GET Filename Buttons
   action4 = WIDGET_BUTTON(wcont_callw,VALUE='    SCC Lister    ')
   action5 = WIDGET_BUTTON(wcont_callw,VALUE='  Load .SAV File  ')
   action7 = WIDGET_BUTTON(wcont_callw,VALUE='  Load TEXT File  ')

   ;--Filenames Display Window
   txt = WIDGET_LABEL(wcont_filel,VALUE=' Filenames',/ALIGN_LEFT,Font=font1)
   list = WIDGET_LIST(wcont_filel,XSIZE=75,YSIZE=20)
   IF keyword_set(filename) THEN BEGIN
      n=n_elements(*file)
      display_text = strarr(n)
      FOR i=0,n-1 DO BEGIN
        IF strlen((*file)[i]) GT 75 THEN $
          display_text[i]=('...'+strmid((*file)[i],strlen((*file)[i])-72,$
          strlen((*file)[i])-1)) else display_text =(*file)[i]
      ENDFOR
      Widget_Control,list, set_value =display_text
   ENDIF ELSE Widget_Control,list, set_value = 'Please select filenames to be processed'
 
   action ={action1:action1,action2:action2,action3:action3,$
            action4:action4,action5:action5,action6:list,action7:action7}
 

   ;--WRITE PANEL
   write0 = WIDGET_BUTTON(wcont_write,VALUE=' None       ')
   write1 = WIDGET_BUTTON(wcont_write,VALUE=' Write FITS ')
   write2 = WIDGET_BUTTON(wcont_write,VALUE=' Write PNG  ')
   write3 = WIDGET_BUTTON(wcont_write,VALUE=' Write JPEG ')
   Label7 = WIDGET_LABEL(wcont_write_txt,VALUE='Save Path ',/ALIGN_LEFT)
   write4 = WIDGET_TEXT(wcont_write_txt,XSIZE=25,/ALL_EVENTS)
   write5 = WIDGET_BUTTON(wcont_write_txt,value='Browse', SCR_XSIZE=15)
   Widget_Control,write0, Set_Button= 1
   write = {write0:write0,write1:write1,write2:write2,write3:write3,write4:write4,write5:write5}

   ;--FORMAT PANEL
   droptxt = WIDGET_LABEL(wcont_format_txt,VALUE='Outsize',/align_left)
   outsize=['Default','  2048 ','  1024 ','   512 ','   256 ','   128 ','    64 ']
   format1 = WIDGET_DROPLIST(wcont_format_txt, VALUE=outsize)
   format2 = WIDGET_BUTTON(wcont_format,VALUE=' Solar North Up   ',UVALUE='1')
   format3 = WIDGET_BUTTON(wcont_format,VALUE=' Load Color Table ',UVALUE='1')
   format4 = WIDGET_BUTTON(wcont_format,VALUE=' Date-Time Stamp  ',UVALUE='1')
   format5 = WIDGET_BUTTON(wcont_format,VALUE=' SECCHI Logo Stamp ',UVALUE='1')
   format = {format1:format1,format2:format2,format3:format3,format4:format4,format5:format5}

   ;--Mask PANEL
   mask1 = WIDGET_BUTTON(wcont_mask,VALUE=' Smooth Mask    ')
   mask2 = WIDGET_BUTTON(wcont_mask,VALUE=' ICER Missing Mask ')
   mask3 = WIDGET_BUTTON(wcont_mask,VALUE=' Missing Block Mask ')
   mask4 = WIDGET_BUTTON(wcont_mask,VALUE=' Fill with Mean ')
   mask5 = CW_FIELD(wcont_mask_txt,VALUE='',/ROW,TITLE='Fill Value',XSIZE=10,/return_events)
   mask = {mask1:mask1,mask2:mask2,mask3:mask3,mask4:mask4,mask5:mask5}
   
   ;--Polariz PANEL
   polariz0 = WIDGET_BUTTON(wcont_polariz,VALUE=' None                 ')
   polariz1 = WIDGET_BUTTON(wcont_polariz,VALUE=' Total Brightness     ')
   polariz2 = WIDGET_BUTTON(wcont_polariz,VALUE=' Perecent Polarized   ')
   polariz3 = WIDGET_BUTTON(wcont_polariz,VALUE=' Polarized Brightness ')
   polariz4 = WIDGET_BUTTON(wcont_polariz,VALUE=' Polarization Angle   ')
   polariz = {polariz0:polariz0,polariz1:polariz1,polariz2:polariz2,polariz3:polariz3,polariz4:polariz4}
   Widget_Control,polariz4, Set_Button=1
  
   ;--Default All Off
   label8 = WIDGET_LABEL(wcont_default00,VALUE='',/ALIGN_LEFT,FONT=font1)
   default0 = WIDGET_BUTTON(wcont_default01,VALUE=' Turn Off All Calibration  ')
   default1 = WIDGET_BUTTON(wcont_default01,VALUE=' Turn Off Image Trim    ');
   
   ;--Default All instruments
   label9 = WIDGET_LABEL(wcont_default12,VALUE='All instruments   ',/ALIGN_LEFT,Font = font1)
   default2 = WIDGET_BUTTON(wcont_default10,VALUE=' Exposure Time      ')
   default3 = WIDGET_BUTTON(wcont_default10,VALUE=' Bias Subtraction   ')
   default4 = WIDGET_BUTTON(wcont_default10,VALUE=' SEB IP Correction  ')
   default5 = WIDGET_BUTTON(wcont_default10,VALUE=' Calibration Image  ')
   default6 = WIDGET_BUTTON(wcont_default10,VALUE=' Load New Image     ')
   default7 = CW_FIELD(wcont_default11,VALUE='',/column,TITLE='Non-Default Calibration Image ',XSIZE=20)
   default15 =  WIDGET_BUTTON(wcont_default11,value='Browse', SCR_XSIZE=15)

   default8 = WIDGET_LABEL(wcont_default20,VALUE='EUVI Only',/ALIGN_LEFT,Font=font1)
   default9 = WIDGET_BUTTON(wcont_default21,VALUE=' Normalization Factor ') 
   default10 = WIDGET_BUTTON(wcont_default21,VALUE=' DN to Photons ') 
   default11 = WIDGET_LABEL(wcont_default22,VALUE='COR Only',/ALIGN_LEFT,Font=font1)
   default12 = WIDGET_BUTTON(wcont_default23,VALUE=' Calibration Factor ') 
   default13 = WIDGET_LABEL(wcont_default24,VALUE='HI Only',/ALIGN_LEFT,Font=font1)
   default14 = WIDGET_BUTTON(wcont_default25,VALUE=' Shutterless Correction ') 

   Widget_Control, default0, Set_Button= 0
   Widget_Control, default1, Set_Button=0
   Widget_Control, default2, Set_Button=1
   Widget_Control, default3, Set_Button=1
   Widget_Control, default4, Set_Button=1
   Widget_Control, default5, Set_Button=1

   Widget_Control, default6, Set_Button=0
   Widget_Control, default9, Set_Button=1
   Widget_Control, default10, Set_Button=1
   Widget_Control, default12, Set_Button=1
   Widget_Control, default14, Set_Button=1

   default = {default0:default0,default1:default1,default2:default2,default3:default3,$
              default4:default4,default5:default5,default6:default6,default7:default7,$
              default9:default9,default10:default10,default12:default12,default13:default13,$
              default14:default14,default15:default15}

   keyword = {savepath:'',outsize:0,rotate_on:0,trim_off:0,calibrate_off:0,sebip_off:0,$ 
             bias_off:0,exptime_off:0,calimg_off:0,new_calimg:0,calimg_filename:0,$
             write_fts:0,write_png:0,write_jpg:0,color_on:0,date_on:0,logo_on:0,smask_on:0,$
             tele_only:0,mask_only:0,fill_mean:0,fill_value:0,calfac_off:0,polariz_on:0,$  
             pb:0,mu:0,percent:0,dn2p_off:0,nornal_off:0,shutterless_off:0}

  state = {file:file,keyword:keyword,default:default,write:write,$
           action:action,format:format,mask:mask,polariz:polariz}

  WIDGET_CONTROL,base, /realize
  WIDGEt_CONTROL,base,SET_UVALUE=state,/no_copy

  XMANAGER, 'xsecchi_prep', base,EVENT_HANDLER = 'xsecchi_prep_event',group_leader=base
im = newim
hdr = newhdr

END
