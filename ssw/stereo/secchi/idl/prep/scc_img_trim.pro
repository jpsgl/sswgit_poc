function scc_img_trim, im, hdr,silent=silent
;+
; $Id: scc_img_trim.pro,v 2.4 2007/12/13 17:01:13 colaninn Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_img_trim.pro
;               
; Purpose   : returns rectified images with under/over scan areas removed
;               
; Explanation: The program returns the imaging area of the CCD. If the
;              image has not been rectified such that ecliptic north
;              is up then the image is rectified
;
; Use       : IDL> img = scc_img_trim(im, hdr)
;    
; Inputs    : im - any SECCHI image
;             hdr -image header (FITS or SECCHI header structure)
;               
; Outputs   : img - imaging area; floating point array  
;
; Side Effects : Modifies header structure to new image values.
;
; Keywords  : cor1_flip - set if using old cor1 data 
;
; Calls     : SCC_FITSHDR2STRUCT, SECCHI_RECTIFY,SCC_UPDATE_HISTORY, 
;              FITSHEAD2WCS, WCS_GET_COORD
;
; Category    : Admistration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU October 2006
;               
; $Log: scc_img_trim.pro,v $
; Revision 2.4  2007/12/13 17:01:13  colaninn
; Removed print
;
; Revision 2.3  2007/11/30 15:17:49  colaninn
; corrected R1(2)ROW(COL)
;
; Revision 2.2  2007/11/01 15:38:51  colaninn
; put update to R1(2)COL(ROW) back
;
; Revision 2.1  2007/10/18 20:49:29  colaninn
; add more checks for DSTOP1(2)
;
; Revision 2.0  2007/10/17 16:27:42  colaninn
; now uses DSTART1(2) DSTOP(1)(2) keywords
;
;
; Revision 1.1  2006/10/20 15:40:46  colaninn
; first entry
;
;-
ON_ERROR,2
info="$Id: scc_img_trim.pro,v 2.4 2007/12/13 17:01:13 colaninn Exp $"
len=strlen(info)
histinfo=strmid(info,1,len-2)

IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

;--Check for un-reprocessed data
IF (hdr.DSTOP1 LT 1) OR (hdr.DSTOP1 GT hdr.NAXIS1) OR (hdr.DSTOP2 GT hdr.NAXIS2) THEN PRECOMMCORRECT,im,hdr,_extra=ex

x1 = hdr.DSTART1-1
x2 = hdr.DSTOP1-1
y1 = hdr.DSTART2-1
y2 = hdr.DSTOP2-1

;--Apply Trim------------------------------------
img = im[x1:x2,y1:y2]

s = size(img,/dim)

IF (hdr.naxis1 NE s[0]) OR (hdr.naxis2 NE s[1]) THEN BEGIN
  IF ~keyword_set(silent) THEN message,'Removing image under/over scan',/inform

  ;--Update header values; image cooridinates
   IF ~keyword_set(silent) THEN message,'Updating header',/inform

  sum = 2^(hdr.summed-1)
  hdr.R1COL = hdr.R1COL+(x1*sum) 
  hdr.R2COL = hdr.R1COL+(s[0]*sum)-1
  hdr.R1ROW = hdr.R1ROW+(y1*sum)
  hdr.R2ROW = hdr.R1ROW+(s[1]*sum)-1

  hdr.DSTART1 = 1
  hdr.DSTOP1 = s[0] 
  hdr.DSTART2 = 1
  hdr.DSTOP2 = s[1]

  hdr.NAXIS1 = s[0]
  hdr.NAXIS2 = s[1]

  hdr.CRPIX1 = hdr.CRPIX1-x1
  hdr.CRPIX1A = hdr.CRPIX1A-x1

  hdr.CRPIX2 = hdr.CRPIX2-y1
  hdr.CRPIX2A = hdr.CRPIX2A-y1

  WCS = FITSHEAD2WCS(hdr)
  xycen = WCS_GET_COORD(WCS, [(hdr.NAXIS1-1.)/2., (hdr.NAXIS2-1.)/2.])
  hdr.XCEN=xycen[0]
  hdr.YCEN=xycen[1]

  hdr = SCC_UPDATE_HISTORY(hdr,'scc_img_trim.pro,v 1.12 2007/01/31 REMOVED OVER/UNDER SCAN')

ENDIF

RETURN,img
END
