function scc_add_logo, img, hdr,LEFT=left,color=color
;+
; $Id: scc_add_logo.pro,v 1.11 2008/03/31 20:07:09 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_add_logo
;               
; Purpose   : This program adds the SECCHI logo to an image
;               
; Use       : IDL> out = scc_add_logo(im,hdr)
;    
; Inputs    : im - image array to add logo stamp 
;             hdr - image header, either fits or SECCHI structure
;               
; Outputs   : out - im with logo stamp included     
;
; Common    : SCC_ADD_LOGO_COMMON   
;               
; Calls     : SCC_FITSHDR2STRUCT
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU JAN 2007
;               
; $Log: scc_add_logo.pro,v $
; Revision 1.11  2008/03/31 20:07:09  nathan
; fixed problem with both large and small being defined at the same time
;
; Revision 1.10  2008/01/31 16:20:25  nathan
; fix congrid call
;
; Revision 1.9  2008/01/24 18:17:56  nathan
; use congrid instead of rebin; changed how sum is recalculated for sum>3
;
; Revision 1.8  2007/10/19 15:10:48  nathan
; implemented COMMON block
;
; Revision 1.7  2007/08/17 19:18:04  nathan
; make hdr optional
;
; Revision 1.6  2007/07/11 21:29:50  colaninn
; added color keyword
;
; Revision 1.5  2007/03/30 19:47:50  colaninn
; made change to pretty-up pretties
;
; Revision 1.4  2007/02/26 19:07:01  colaninn
; removed common block
;
; Revision 1.3  2007/01/31 18:34:28  colaninn
; does not include logo for less then 128
;
; Revision 1.2  2007/01/22 19:45:17  colaninn
; added call for small logo
;
; Revision 1.1  2007/01/17 15:51:06  colaninn
; created from ADD_LASCO_LOGO.pro
;
;-

COMMON SECCHI_LOGO, largescclogo, smallscclogo, large_logo_filename, small_logo_filename

;IF n_params() GT 1 THEN BEGIN
;    IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
;ENDIF

s = size(img)
sum = 2048/s[1]

IF sum GE 4 THEN BEGIN  
  sum = (sum-5)>1<2
  logo_sz= 64/sum
  logo_y_sz=logo_sz/2
  buff = 10/sum

    IF DATATYPE(smallscclogo) EQ 'UND' THEN BEGIN
    	small_logo_filename = 'secchi_logo_small.sav'
	 path = concat_dir(getenv('SSW'),'stereo')
	 RESTORE, FILEPATH(small_logo_filename,ROOT_DIR=path,$
        	SUBDIRECTORY=['secchi','data'])
	 smallscclogo=scclogo
    ENDIF	 
    logo = smallscclogo[*,16:47]
    
ENDIF ELSE BEGIN
  logo_sz= 256/sum
  logo_y_sz=logo_sz
  buff = 40/sum

    IF DATATYPE(largescclogo) EQ 'UND' THEN BEGIN
    	large_logo_filename = 'secchi_logo.sav'
	 path = concat_dir(getenv('SSW'),'stereo')
	 RESTORE, FILEPATH(large_logo_filename,ROOT_DIR=path,$
        	SUBDIRECTORY=['secchi','data'])
	 largescclogo=scclogo
    ENDIF	 
    logo = largescclogo
    
ENDELSE


image = img
whsize = s[1]
wvsize = s[2]

;--Check if LOGO rebin is too small
IF sum GT 8 THEN logo1 = fltarr(logo_sz,logo_y_sz) ELSE $
  logo1 = congrid(logo,logo_sz,logo_y_sz)

wlogo = where(logo1 NE 0,num)

add = image[s[1]-logo_sz-buff:whsize-1-buff,0:logo_y_sz-1]

IF num NE 0 THEN BEGIN
  IF keyword_set(color) THEN add[wlogo]=color ELSE add[wlogo]=max(img)
ENDIF

image[s[1]-logo_sz-buff,0] = add

RETURN, image
END
