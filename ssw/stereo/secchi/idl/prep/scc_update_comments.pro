function scc_update_comments, comments0, hdr
;+
; $Id: scc_update_comments.pro,v 1.3 2007/06/25 14:07:24 colaninn Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_update_comments.pro
;               
; Purpose   : The function returns updated keyword comments for level
;             1 and level 2 processing.
;               
; Use       : IDL> comments1=scc_update_comments(comments_array, headervalues)
;    
; Inputs    : comments0 - string array of comments
;   	    : hdr - SECCHI header structure for value-dependent comments
;
; Outputs   : comments - updated string array of comments
;
; Calls     : 
;
; Common    : secchi_header - defined in sccreadfits.pro
;
; Category    : Administration, Calibration
;
; Written     : Robin C Colaninno NRL/GMU  Feb 2007
;
; $Log: scc_update_comments.pro,v $
; Revision 1.3  2007/06/25 14:07:24  colaninn
; only update polarized comments for CORs
;
; Revision 1.2  2007/03/13 17:36:57  nathan
; make sure input is unchanged; no vars in common block
;
; Revision 1.1  2007/02/06 20:57:48  colaninn
; added to secchi_prep
;
;
;-

COMMON secchi_header	;, sechdrstruct, sechdrtags

comments=comments0  	; so input is unchanged

comments[where(sechdrtags EQ 'BZERO')] = 'Data is Floating Point'


polcmnts=strarr(1005)
polcmnts[1001:1004]=['Total Brightness','Polarized Brightness','Percent Polarized','Polarization Angle']
IF hdr.DETECTOR EQ 'COR1' OR hdr.DETECTOR EQ 'COR2' THEN BEGIN
IF (hdr.N_IMAGES GT 1) THEN BEGIN
  comments[where(sechdrtags EQ 'EXPTIME')]  = 'Original times in comment'
  comments[where(sechdrtags EQ 'EXPCMD')]   = 'Average of input images'
  comments[where(sechdrtags EQ 'BIASMEAN')] = 'Average'
  comments[where(sechdrtags EQ 'BIASSDEV')] = 'Max'
  comments[where(sechdrtags EQ 'CEB_T')]    = 'Average'
  comments[where(sechdrtags EQ 'TEMP_CCD')] = 'Average'
  comments[where(sechdrtags EQ 'READTIME')] = 'Average'
  comments[where(sechdrtags EQ 'CLEARTIM')] = 'Average'
  comments[where(sechdrtags EQ 'IP_TIME')]  = 'Total'
  comments[where(sechdrtags EQ 'COMPFACT')] = 'Average'
  comments[where(sechdrtags EQ 'NMISSING')] = 'Total'
  comments[where(sechdrtags EQ 'TEMPAFT1')] = 'Average'
  comments[where(sechdrtags EQ 'TEMPAFT2')] = 'Average'
  comments[where(sechdrtags EQ 'TEMPMID1')] = 'Average'
  comments[where(sechdrtags EQ 'TEMPMID2')] = 'Average'
  comments[where(sechdrtags EQ 'TEMPFWD1')] = 'Average'
  comments[where(sechdrtags EQ 'TEMPFWD2')] = 'Average'
  comments[where(sechdrtags EQ 'TEMP_CEB')] = 'Average'
  comments[where(sechdrtags EQ 'CROTA')]    = 'Average; see comment'
  comments[where(sechdrtags EQ 'POLAR')]    =  polcmnts[hdr.polar]
ENDIF
ENDIF

return,comments

END
