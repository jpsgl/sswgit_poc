function get_calfac,hdr, SILENT=SILENT, _EXTRA=_extra
;+
; $Id: get_calfac.pro,v 1.17 2018/03/07 22:10:42 thompson Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_calfac
;               
; Purpose   : This function returns calibration factor for a given image.

; Use       : IDL> calfac = get_calfac(hdr)
;    
; Inputs    : hdr -image header, either fits or SECCHI structure
;
; Outputs   : calfac - floating point number to be multiply the image by
;                      to convert DN to physical units
;   	      hdr.ipsum and hdr.calfac are modified
;
; Calls     : SCC_FITSHDR2STRUCT
;
; Procedure : If the images was SEB IP summed then the program corrects 
;             the calibration factor.
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU June 2006
;               
; $Log: get_calfac.pro,v $
; Revision 1.17  2018/03/07 22:10:42  thompson
; Updated with COR1 sensitivity changes from Thompson, 2018, Solar Physics, 293:49
;
; Revision 1.16  2017/01/23 15:25:55  crothers
; updated with revised degredation terms for HI-1 from Tappin et al, Solar Physics 2017
;
; Revision 1.15  2015/07/30 13:28:05  crothers
; Degradation in HI-2 as per Tappin et al Solar Phys 2015
;
; Revision 1.14  2013/01/22 10:37:40  crothers
; added preliminary calibration factors for HI2 camaras
;
; Revision 1.13  2012/06/26 21:44:13  nathan
; set hdr.calfac and do not do ipsum correction if calfac=1
;
; Revision 1.12  2011/07/12 15:04:02  thompson
; Added factor of two for total brightness images formed from three exposures.
;
; Revision 1.11  2010/11/15 21:03:04  colaninn
; changed HI1 calibration factor to Bewsher 2010 SolPhys values (x4 for secchi_prep compatibility)
;
; Revision 1.10  2010-11-05 19:33:53  colaninn
; added HI1 calibration factor
;
; Revision 1.9  2008/08/05 18:56:45  colaninn
; add transmission factor for COR2 polarizer
;
; Revision 1.8  2008/02/13 16:34:42  thompson
; Updated COR1 calibration factors
;
; Revision 1.7  2007/10/09 20:12:42  thompson
; *** empty log message ***
;
; Revision 1.6  2007/09/25 15:46:03  nathan
; added _EXTRA keyword
;
; Revision 1.5  2007/09/24 21:00:50  nathan
; Set hdr.IPSUM=1 if calfac is corrected for IPSUM
;
; Revision 1.4  2007/04/04 19:04:07  colaninn
; added HI1&2 with calfac = 1
;
; Revision 1.3  2007/04/02 14:36:21  antunes
; Added regexp hdr check same as cor_prep etc.
;
; Revision 1.2  2007/01/24 21:15:17  colaninn
; fixed IP SUM correction removed CCD sum correction
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.6  2006/09/22 18:26:37  colaninn
; added EUVI values
;
; Revision 1.5  2006/09/15 21:22:51  colaninn
; changed image path to new SSW path
;
; Revision 1.4  2006/09/05 20:54:29  colaninn
; did not mean to commit
;
; Revision 1.3  2006/08/02 20:44:55  colaninn
; correction for summing
;
; Revision 1.2  2006/07/26 18:44:52  colaninn
; Add COR1 calfac numbers
;
; Revision 1.1  2006/07/25 19:15:37  colaninn
; *** empty log message ***
;
;-            


IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr)
IF ~strmatch(TAG_NAMES(hdr,/STRUCTURE_NAME),'SECCHI_HDR_STRUCT*') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'


CASE hdr.DETECTOR OF
  'COR1': BEGIN
    IF hdr.OBSRVTRY EQ 'STEREO_A'  THEN BEGIN
        calfac = 6.578E-11      ;Bsun/DN
        tai0 = utc2tai('2007-12-01T03:41:48.174') ;Calibration date
        rate = 0.00648                            ;Frac. decline per year
    ENDIF
    IF hdr.OBSRVTRY eq 'STEREO_B'THEN BEGIN 
        calfac = 7.080E-11      ;Bsun/DN
        tai0 = utc2tai('2008-01-17T02:20:15.717') ;Calibration date
        rate = 0.00258                            ;Frac. decline per year
    ENDIF
    years = (anytim2tai(hdr.date_avg)-tai0) / (3600.*24*365.25)
    calfac = calfac / (1-rate*years)
  END

  'COR2': BEGIN
    IF hdr.OBSRVTRY eq 'STEREO_A' THEN BEGIN
      calfac = 2.7E-12*0.5 ;Bsun/DN
                           ;0.5 ~ transmission of polarizer
    ENDIF

    IF hdr.OBSRVTRY eq 'STEREO_B' THEN BEGIN
      calfac = 2.8E-12*0.5 ;Bsun/DN
                           ;0.5 ~ transmission of polarizer
    ENDIF
  END
  
  'EUVI': BEGIN
     gain = 15.0
     calfac = gain*(3.65*hdr.WAVELNTH)/(13.6*911); photon/DN 
   END

   'HI1':BEGIN
    IF hdr.OBSRVTRY EQ 'STEREO_A'  THEN BEGIN
      years=(anytim2tai(hdr.date_avg,/nocorrect)-anytim2tai('2009-01-01T00:00:00.000',/nocorrect))/3600/24/365.25
      calfac=3.63e-13  ; Bsun/DN
      annualchange=0.000910
   ;   calfac = 3.596E-13  ;Bsun/DN
    ENDIF
    IF hdr.OBSRVTRY eq 'STEREO_B'THEN BEGIN 
      years=(anytim2tai(hdr.date_avg,/nocorrect)-anytim2tai('2007-01-01T00:00:00.000',/nocorrect))/3600/24/365.25
      calfac=3.55e-13 ; BSun/DN
      annualchange=0.001503
      ; calfac = 3.616e-13  ;Bsun/DN
    ENDIF     
    if years < 0 then years=0 
    calfac = calfac / (1-annualchange*years)  ; tappin et al Solar Physics 2017 DOI 10.1007/s11207-017-1052-0
    hdr=scc_update_history(hdr,'revised calibration Tappin et al Solar Physics 2017 DOI 10.1007/s11207-017-1052-0')
   END
 
   'HI2':BEGIN
    years=(anytim2tai(hdr.date_avg,/nocorrect)-anytim2tai('2000-12-31T00:00:00.000',/nocorrect))/3600/24/365.25
    IF hdr.OBSRVTRY EQ 'STEREO_A'  THEN BEGIN
;      calfac = 4.28E-14  ;Bsun/DN
       calfac = 4.411E-14 + 7.099E-17 * years
    ENDIF
    IF hdr.OBSRVTRY eq 'STEREO_B'THEN BEGIN 
;      calfac = 4.56e-14  ;Bsun/DN
       calfac = 4.293e-14 + 3.014e-17 * years
    ENDIF     
    END
   

ENDCASE

hdr.calfac=calfac 	

sumcount=0
;--Correct for IP summing scale factor
;--If calfac=1.0, same as setting /NOCALFAC. IPSUM gets corrected in secchi_prep.

IF hdr.ipsum GT 1 and calfac NE 1.0 THEN BEGIN
    divfactor=(2^(hdr.ipsum-1))^2
    sumcount=hdr.ipsum-1
    hdr.ipsum=1
    calfac=calfac/divfactor
    IF ~keyword_set(SILENT) THEN BEGIN
    	message,'Divided calfac by '+trim(divfactor)+' to account for IPSUM',/info
	print,'             IPSUM changed to 1 in header.'
    ENDIF
    hdr=scc_update_history(hdr,'get_calfac Divided calfac by '+trim(divfactor)+' to account for IPSUM')   
ENDIF

;  Apply factor of two for total brightness images that are not double exposures.

if (hdr.polar eq 1001) and (hdr.seb_prog ne 'DOUBLE') then begin
    calfac = 2*calfac
    if ~keyword_set(silent) then message,'Applied factor of 2 for total brightness',/info
    hdr = scc_update_history(hdr,'get_calfac Applied factor of 2 for total brightness')
endif

RETURN,calfac
END
