function get_biasmean, hdr, _extra=ex,silent=silent
;+
; $Id: get_biasmean.pro,v 1.8 2013/10/28 18:25:34 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_biasmean
;               
; Purpose   : This program returns mean bias for a give image.
;               
; Explanation: This program corrects the mean bias for any SEB IP
;               
; Use       : IDL> biasmean = get_biasmean(hdr)
;    
; Inputs    : hdr - image header, either fits or SECCHI structure
;               
; Outputs   : biasmean - floating point number to be substracted from
;                        the image
;
; Calls     : SCC_FITSHDR2STRUCT
;
; Procedure : The program checks that the bias was not subtracted on
;             in the SEB IP. If the bias has already been subtracted
;             the program returns 0.0. If the images was SEB IP summed
;             then the program corrects the bias. 
;               
; Category    : Calibration
;               
; Prev. Hist. : None.
;
; Written     : Robin C Colaninno NRL/GMU June 2006
;               
; $Log: get_biasmean.pro,v $
; Revision 1.8  2013/10/28 18:25:34  nathan
; use IP_00_19 to determine whether bias has been subtracted, plus HI special case
;
; Revision 1.7  2008/01/24 17:36:28  nathan
; if offsetcr GT 0 return 0
;
; Revision 1.6  2007/08/24 19:30:49  nathan
; updated info messages and hdr history
;
; Revision 1.5  2007/05/21 20:16:05  colaninn
; added silent keyword
;
; Revision 1.4  2007/01/24 21:14:26  colaninn
; fixed IP SUM correction
;
; Revision 1.3  2006/11/21 21:22:58  colaninn
; update output message
;
; Revision 1.2  2006/10/24 19:58:01  colaninn
; added bias message
;
; Revision 1.1  2006/10/03 15:28:42  nathan
; moved from dev/analysis
;
; Revision 1.4  2006/09/08 20:00:20  colaninn
; updated header infromation
;
; Revision 1.3  2006/08/15 21:23:39  colaninn
; fixed typo in call stregex
;
; Revision 1.2  2006/08/02 14:30:48  colaninn
; corrected for IP summing and bias subtraction
;
; Revision 1.1  2006/07/25 19:15:45  colaninn
; first entry
;
;-            
IF(DATATYPE(hdr) NE 'STC') THEN hdr=SCC_FITSHDR2STRUCT(hdr) 
IF (TAG_NAMES(hdr,/STRUCTURE_NAME) NE 'SECCHI_HDR_STRUCT') THEN $
MESSAGE, 'ONLY SECCHI HEADER STRUCTURE ALLOWED'

bias = hdr.BIASMEAN
ipsum=hdr.ipsum

;--Check for On-Board BIAS substaction
IF stregex( hdr.IP_00_19,'103',/boolean) $  	; Subtract BIAS
or stregex( hdr.IP_00_19,' 37',/boolean) $  	; Use HI-1 summing buffer
or stregex( hdr.IP_00_19,' 38',/boolean) $  	; Use HI-2 summing buffer
THEN BEGIN
    IF ~keyword_set(silent) THEN $
    message,'BIASMEAN SUBTRACTED ONBOARD IN SEB IP',/inform
    RETURN,0.0
ENDIF

IF strmid(hdr.detector,0,2) EQ 'HI' THEN bias=bias-(bias/hdr.n_images)
; IP always subtracts bias of 1 frame for any HI.

IF hdr.offsetcr GT 0 THEN BEGIN
    IF ~keyword_set(silent) THEN message, 'OFFSETCR='+trim(hdr.offsetcr)+' already subtracted; returning 0',/info
    return,0.0
ENDIF

help,ipsum

;-- Correct BIAS for IP summed images
IF IPSUM GT 1 THEN BEGIN
  bias=bias*((2.0^(IPSUM-1))^2.0)
  IF ~keyword_set(silent) THEN $
   message,'BIASMEAN was corrected for SEB IP SUMMING',/inform
ENDIF


RETURN,bias
END
