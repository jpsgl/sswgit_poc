PRO scc_daily_rdiff,yyyymmdd,cam0,sc0, PNG_ONLY=png_only, nopop=nopop, imlist=imlist, $
    	    	minus30=minus30, USE_DOUBLE=use_double, histars=histars, SOURCE=source, $
		DOALL=DOALL, QUADRANT=quadrant, _EXTRA=_extra
;
;+
; $Id: scc_daily_rdiff.pro,v 1.8 2015/08/03 18:34:29 hutting Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : 
;               
; Purpose   : 
;               
; Explanation: 
;               
; Use       : IDL> scc_daily_pretties,'YYYYMMDD','CAM','SC'
;    
; Inputs    :CAM0 = cor1, cor2, hi1, hi2, euvi
;            SC0 = 'A', 'B'
;            YYYYMMDD = date to be processed
;               
; Outputs   : PNG files in $SECCHI_PNG
;
; Keywords  :
;   	    /USE_DOUBLE     Use type DOUBLE images for cor2 
;   	    /PNG_ONLY Skip jpegs.
;           /NOPOP -- Don't pop up a "preview' window for each image
;	    imlist=imlist string array of images to be done
;   	    SOURCE='lz','pb' or 'rt' ; default is lz
;   	    /doall creates daily prettie for all fits file not just the missing pretties
;   	    /minus30 = changes do date to date-30 days 
;   	    /histars = create hi star daily pretties.
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to disk
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     : Karl Battams, NRL/I2, MAR 2007
;               
; $Log: scc_daily_rdiff.pro,v $
; Revision 1.8  2015/08/03 18:34:29  hutting
; added yearly directories
;
; Revision 1.7  2013/11/25 23:03:36  secchia
; nr - add variable bmin,bmax for srem_movie
;
; Revision 1.6  2012/05/04 14:38:50  nathan
; use SECCHIP_PNG instead of SECCHI_PNG
;
; Revision 1.5  2011/07/21 22:35:44  secchia
; fix previous mod
;
; Revision 1.4  2011/07/21 20:30:33  secchia
; limit cor2 double to 2/hr
;
; Revision 1.3  2011/07/20 21:06:18  secchib
; implement /DOALL
;
; Revision 1.2  2011/05/13 18:43:44  nathan
; print different info
;
; Revision 1.1  2011/05/13 15:44:06  nathan
; writes to SECCHI_PNG, 512 only
;
; Log: scc_daily_pretties.pro,v 
; Revision 1.31  2010/02/08 20:08:38  secchib
;-

; ****************************************************************************************************************************
; ****************************************************************************************************************************
; ******************************** FIRST WE DO ALL THE PREP AND GENERATE A FILE LIST... **************************************
; ****************************************************************************************************************************
; ****************************************************************************************************************************

stime=systime(1)

date=nrlanytim2utc(yyyymmdd) ;date get changed of minus30 keyword set

if keyword_set(minus30) then date.mjd=date.mjd-30

yyyymmdd=utc2yymmdd(date,/yyyy)



; initial setup

if getenv('SECCHIP_PNG') EQ '' THEN BEGIN
    PRINT,''
    PRINT,'ERROR!! $SECCHIP_PNG environment variable is not set!
    PRINT,''
    return
endif

IF keyword_set(imlist) then BEGIN
    files=imlist
    goto, afterlist
ENDIF

;if getenv('SECCHI_JPG') EQ '' THEN BEGIN
;    PRINT,''
;    PRINT,'ERROR!! $SECCHI_JPG environment variable is not set!
;    PRINT,''
;    return
;endif

IF keyword_set(SOURCE) THEN src=strlowcase(source) ELSE src='lz'

cam=strlowcase(cam0)
sc=strcompress(strlowcase(sc0),/remove_all)
usc=strupcase(sc)

use_p0=0
tdir='img'
cor_flag=0 
lgmask=1
pan=512
outer=1
nocal=1
logscl=0
dorot=1
pbser=0
pol  =0

CASE cam OF
    'cor1':  BEGIN
        s=getenv('secchi')+'/'+src+'/L0/'+sc+'/seq/cor1/'
        cor_flag=0
        cam_str='cor1_rdif'
        tdir='seq'
	bmin=-23
	bmax= 35
    	pbser=1     ; Total Brightness
	pol  =120
	desc='rdBc1'
    END
    'cor2':  BEGIN
        IF keyword_set(USE_DOUBLE) THEN BEGIN
	    s=getenv('secchi')+'/'+src+'/L0/'+sc+'/img/cor2/' 
	    bmin=-15
	    bmax=15
	    desc='rdDc2'
	ENDIF ELSE BEGIN 
	    s=GETENV('SECCHI_P0')+'/'+sc+'/cor2/'
	    tdir='pol'
	    use_p0=1
	    bmin=-20
	    bmax= 20
	    desc='rdTc2'
	ENDELSE   
        cor_flag=1 
        cam_str='cor2_rdif'
	pol=1001
    END
    'hi1':  BEGIN
        s=getenv('secchi')+'/'+src+'/L0/'+sc+'/img/hi_1/'
        cam_str='hi_srem'
	desc='SRh1'
	useday=1
	bmin=-250
	bmax=250
    END
    'hi2':  BEGIN
        s=getenv('secchi')+'/'+src+'/L0/'+sc+'/img/hi_2/'
        cam_str='hi_srem'
	desc='SRh2'
	useday=0
	bmin=-1000
	bmax=1000
    END
    'euvi':  BEGIN
        s=getenv('secchi')+'/'+src+'/L0/'+sc+'/img/euvi/'
        cam_str='euvi_rdif' 
	bmin=-70
	bmax= 70
	IF keyword_set(QUADRANT) THEN qs=quadrant ELSE qs=195
	desc=strmid(trim(qs),0,2)+'rdeu'
    END
    ELSE:  BEGIN
        PRINT,'Unrecognized telescope code: '+cam
        RETURN
    END
ENDCASE



; Check files exist
PRINT,''
PRINT,'###### SEARCHING FOR DATA... ######
PRINT,''
;CD,s,curr=orig
f=file_search(s+yyyymmdd+'/*fts')
sz=size(f)
IF (sz(0) EQ 0)  THEN BEGIN
    PRINT,''
    PRINT,'No directory for '+cam+' telescope on '+yyyymmdd
    PRINT,'Did you use the correct date format? (YYYYMMDD)'
    PRINT,''
    RETURN
ENDIF

; Use this date format for scc_read_summary

; generate file list
PRINT,''
PRINT,'###### READING SUMMARY FILE... ######
PRINT,''
summary=scc_read_summary(DATE=date,SPACECRAFT=sc,TELESCOPE=cam,TOTALB=use_p0, TYPE=tdir, SOURCE=source, POLAR=pol, $
    	QUADRANT=qs, /nobeacon, _EXTRA=_extra) 

;IF datatype(summary) NE 'INT' THEN BEGIN  ;remove images form previous day with date obs on current day. (needed for EUVIB ;(2008-01-08 - 2008-01-25)
;   dates=long(strmid(summary.filename,0,8))
;   dodate=long(strmid(dt2,0,4)+strmid(dt2,5,2)+strmid(dt2,8,2))
;   tdo=where(dates eq dodate)
;   summary=summary(tdo)
;ENDIF

IF datatype(summary) EQ 'INT' THEN BEGIN
    RETURN
ENDIF

nf=n_elements(summary)

PRINT,'Found a total of ',nf,' files for the day.'
IF cam EQ 'euvi' and nf LT 25 THEN BEGIN
    inp=''
    read,'Enter c to continue or enter to return and try a different QUADRANT=',inp
    IF inp NE '' THEN return
ENDIF

; summary file query
PRINT,''
PRINT,'###### COMPILING FILE LIST... ######
PRINT,''
IF (cam EQ 'euvi') THEN good = $
    where(  summary.DEST EQ 'SSR1' and $
    	    (summary.compr EQ 'ICER5' or summary.compr EQ 'ICER6' or summary.compr EQ 'ICER4') and $
    	    summary.XSIZE GE 1024) 
	    
IF ((cam EQ 'hi1') OR (cam EQ 'hi2')) THEN good= $
    where(  summary.DEST EQ 'SSR1' and $
    	    summary.XSIZE EQ 1024 and summary.YSIZE EQ 1024) 
	    
IF cam EQ 'cor2' THEN good = $
    where(  summary.DEST EQ 'SSR1' and $
    	    summary.VALUE EQ 1001 and $
    	    summary.XSIZE GE 1024 and summary.YSIZE GE 1024 $
	    and summary.PROG NE 'Dark')
	    
IF (cam EQ 'cor1') THEN good = $
    where(  summary.DEST EQ 'SSR1' and $
	    summary.PROG NE 'Dark')

if n_elements(good) EQ 1 then begin
    print,'ERROR: Could not find any/enough good images for the day...'
    return
endif else files=summary[good].FILENAME

; ****************************************************************************************************************************
; ****************************************************************************************************************************
; ******************************* HERE'S WHERE WE ACTUALLY MAKE THE PRETTY IMAGES... ****************************************
; ****************************************************************************************************************************
; ****************************************************************************************************************************

; for DOUBLE, limit to 2/hour; 
IF cam EQ 'cor2' THEN BEGIN
    gd=where(strmid(files,11,2) NE '39' and strmid(files,11,2) NE '08')
    files=files[gd]
ENDIF
files=s+yyyymmdd+'/'+ files

afterlist: 
nf=n_elements(files)
PRINT,'Found a total of ',strcompress(nf),' good files.'          
first = 1
types=['jpg','png']

if (rstrmid(files[0],0,1) NE 's') then files=files+'s'

;
; Define output filename
;
sdir=getenv('SECCHIP_PNG')+'/'+sc+'/'+cam_str+'/'+strmid(yyyymmdd,0,4)+'/'+yyyymmdd+'/512'
cmd=['mkdir','-p',sdir]
print,cmd
spawn,cmd,/noshell
sfil=sdir+'/yyyymmdd_hhmmss_'+desc+usc+'.png'

;
; Check to see if already completed
;
done=file_search(sdir+'/*_'+desc+usc+'.png')
ndone=n_elements(done)
IF ndone GT 0.9*nf and ~keyword_set(DOALL) THEN BEGIN
    message,'Apparently '+trim(ndone)+' rdif files already completed, so skipping.',/info
    wait,5
    return
ENDIF
;
; Write files
;
IF cam NE 'hi1' and cam NE 'hi2' THEN $
    scc_mkmovie,files,bmin,bmax, /RUNNING_DIFF, PAN=pan, TIMES=1.5, LG_MASK=lgmask, OUTER=outer, /LIMB, $
    /NOCAL, DOROTATE=dorot, PBSER=pbser, SAVE=sfil, _EXTRA=_extra $
ELSE srem_movie,files,/nosnow,use_daily=useday,AUTOSAVE=sfil, BMIN=bmin, BMAX=bmax

ftime=systime(1)
tot_time=strcompress(string((ftime-stime)/60),/remove_all)
if datatype(nf) ne 'UND' then begin
  PRINT,''
  PRINT,'#########################################################
  message,'TOTAL PROCESSING TIME FOR '+trim(nf)+' FILES: '+tot_time+' MINS.',/info
  PRINT,'#########################################################
  PRINT,''
ENDIF ELSE begin
    message,'no files found',/info
    help,nf
    wait,5
ENDELSE

END
