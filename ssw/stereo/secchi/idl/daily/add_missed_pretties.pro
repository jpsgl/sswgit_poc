pro add_missed_pretties,dtst,dtend, sc
; $Id: add_missed_pretties.pro,v 1.4 2008/02/25 14:06:17 secchib Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : add_missed_pretties
;               
; Purpose   : runs scc_daily_pretties on days missed in pipeline
;               
; Explanation: 
;               
; Use       : IDL> add_missed_pretties,dtst,dtend, sc
;    
; Inputs    :dtst = day1 '2007-01-01'
;            dtend = last day '2008-01-01'
;   	     SC = 'a', 'b'
;               
; Outputs   : JPG24 and PNG files in /net/earth/data3/secchi/images
;
; Keywords  
;
;
; Calls from LASCO :  gif2jpeg24
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to disk
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     : Lynn MCNutt, NRL/I2, JAN 2008
;               
; $Log: add_missed_pretties.pro,v $
; Revision 1.4  2008/02/25 14:06:17  secchib
; corrected last change
;
; Revision 1.3  2008/02/25 14:02:46  mcnutt
; added double check for hi1 images incase hi2 are done
;
; Revision 1.2  2008/02/04 12:03:18  mcnutt
; added check for HI2 missed first go around
;
; Revision 1.1  2008/01/22 17:21:48  mcnutt
; new to add missed daily pretties
;



tels=['HI1','HI2', 'COR2','EUVI']
subdir=['hi','hi','cor','euvi']
nt=n_elements(tels)

ut0=anytim2utc(dtst)
ut1=anytim2utc(dtend)

mjd=ut0

IF n_params() GT 2 THEN IF strlen(sc) GT 1 THEN ab=['a','b'] ELSE ab=sc
nsc=n_elements(ab)

FOR j=0,nsc-1 DO BEGIN
    aorb=ab[j]
    FOR k=0,nt-1 DO BEGIN
    	tel=tels[k]
      	FOR i=ut0.mjd,ut1.mjd DO BEGIN
	    mjd.mjd=i
	    yyyymmdd=utc2yymmdd(mjd,/yyyy)
            misday=findfile(getenv('SECCHI_PNG')+'/'+sc+'/'+subdir(k)+'/'+yyyymmdd)            
	    if (tels(k) eq 'HI2')then misday=findfile(getenv('SECCHI_PNG')+'/'+sc+'/'+subdir(k)+'/'+yyyymmdd+'/512/*h2*.png')
	    if (tels(k) eq 'HI1')then misday=findfile(getenv('SECCHI_PNG')+'/'+sc+'/'+subdir(k)+'/'+yyyymmdd+'/512/*h1*.png')
	    if (misday(0) eq '') then scc_daily_pretties,yyyymmdd,tel,sc,/nopop
	ENDFOR	;i
    ENDFOR	;k
ENDFOR	;j

end
