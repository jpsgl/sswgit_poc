pro sccdopolar, d1, d2, AHEAD=ahead, BEHIND=behind, COR1=cor1, COR2=cor2, DOALL=doall, NAXIS=naxis, SOURCE=source
;+
; $Id: sccdopolar.pro,v 1.15 2015/12/03 14:14:13 secchia Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	SCCDOPOLAR
;
; Purpose     :	Generate Total Brightness and % Polarized images
;
; Category    :	SECCHI, pipeline, polarization, image processing
;
; Explanation :	Searches monthly summary files, calls cor_polarize, and saves fits files. If one
;		part of a triplet is missing, than the triplet is skipped.
;
; Syntax      :	sccdopolar, /AHEAD, 'start-date' [,'end-date', /BEHIND]
;
; Examples    :	sccdoploar,/ahead, '2007-01-01','2007-01-20'
;
; Inputs      :	date string(s)
;
; Opt. Inputs :	none
;
; Outputs     :	One percent polarized and one total brightness FITS file per triplet of polarized
;		COR1 and/or COR2 images:
;
;   	    	$secchi/lz/P0/[a,b]/[perc,totb]/[cor1,cor2]/yyyy/yyyymmdd/yyyymmdd_hhmmss_[pp,tb][c1,c2][AB].fts
;
; Opt. Outputs:	None
;
; Keywords    :	/AHEAD	Use STEREO-A only
;		/BEHIND	Use STEREO-B only
;		/COR1	Use COR1 only
;		/COR2	Use COR2 only
;   	    	/DOALL	Do not skip file if it is already there
;   	    	NAXIS=	Only do files of this size.
;
; Env. Vars.  : $secchi.
;
; Calls       :	ANYTIM2UTC, UTC2TAI, BREAK_FILE, VALID_NUM, SCC_DATA_PATH
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :
;
; $Log: sccdopolar.pro,v $
; Revision 1.15  2015/12/03 14:14:13  secchia
; handles yearly directory for COR2 P0 images
;
; Revision 1.14  2013/11/27 19:17:05  secchib
; update for doing pb
;
; Revision 1.13  2012/08/24 17:39:21  secchib
; aorb bug
;
; Revision 1.12  2012/08/24 14:48:01  secchib
; bug
;
; Revision 1.11  2012/08/23 20:33:59  secchib
; bug
;
; Revision 1.10  2012/08/23 15:38:07  secchib
; nr - return if SECCHI_P0 not available
;
; Revision 1.9  2012/04/18 19:27:22  nathan
; do not wait when osnums different
;
; Revision 1.8  2011/08/03 21:12:55  secchib
; syntax error
;
; Revision 1.7  2011/08/03 20:53:46  nathan
; add NAXIS= for re-doing 1024 images
;
; Revision 1.6  2009/06/12 17:00:40  mcnutt
; moved file check to before waits
;
; Revision 1.5  2009/06/12 16:53:00  mcnutt
;  skips file allready created unless keyword doall is set
;
; Revision 1.4  2007/10/26 22:30:27  nathan
; use /nocalfac instead of /calfac_off (corrects for IPsum)
;
; Revision 1.3  2007/07/13 14:43:54  secchib
; nr - fix for case when last triplet is incomplete
;
; Revision 1.2  2007/04/16 21:50:43  nathan
; COMMON secchi_daily; accomodate SPWX in list.dest; do not stop if interval
; is too long; swap secchi_prep args; write summary file
;
; Revision 1.1  2007/01/31 21:52:28  nathan
; moved from ../pipeline
;
; Revision 1.2  2007/01/31 21:20:37  nathan
; use cor_calibrate and /precomm
;
; Revision 1.1  2007/01/29 22:39:07  nathan
; for generating local polarization files - 1st draft
;
;
; Contact     :	nathan.rich@nrl.navy.mil
;-
;
COMMON secchi_daily , ppimage, pphdr

teldir=''
IF keyword_set(COR2) THEN teldir='cor2' ELSE $
IF keyword_set(COR1) THEN teldir='cor1' ELSE $
	read,'Please enter cor1 or cor2:',teldir
teldir=strlowcase(teldir)

ab=''
IF keyword_set(AHEAD) THEN ab='a' ELSE $
IF keyword_set(BEHIND) THEN ab='b' ELSE $
	read,'Please enter a or b:',ab

src='lz'
IF keyword_set(SOURCE) THEN src=source

IF n_params() LT 2 THEN BEGIN
	ut=anytim2utc(d1)
	ut.mjd=ut.mjd+1
	d2=utc2str(ut)
ENDIF

list=scc_read_summary(date=[d1,d2],telescope=teldir,source=src,type='seq',spacecraft=ab, _EXTRA=_extra)

; first take out SPWX images
notspw=where(strpos(list.dest,'SW') LT 0, ns)
IF ns GE 3 THEN list=list[notspw]
IF keyword_set(NAXIS) THEN list=list[where(list.ysize EQ naxis)]
;stop

; then find polar=0 images and match to it
p0=where(list.value EQ 0,np0)
; make sure last triplet is complete
IF p0[np0-1] GT n_elements(list)-3 THEN np0=np0-1
help,p0,np0
wait,2
outdir0=getenv('SECCHI_P0')+'/'  +ab+'/'    +teldir

IF np0 GT 0 THEN FOR i=0,np0-1 DO BEGIN
	trp=list[p0[i]:p0[i]+2]
    	yyyymmdd=strmid(trp[0].filename,0,8)

    	IF ~file_exist(outdir0) THEN BEGIN
	     openw,evlun,'errmsg.txt',/get_lun
	     printf,evlun,'******* SECCHI_P0 Directory Error *******'
	     printf,evlun,''
	     printf,evlun,'pipeline='+src+'/'+ab+'/'    +teldir
	     printf,evlun,outdir0+' does not exist'
	     printf,evlun,'skipping sccdopolar'
	     printf,evlun,''
	     close,evlun
	     free_lun,evlun
	     print,''
	     spawn,'cat errmsg.txt',/sh
	     wait,10
	     maillist='nathan.rich@nrl.navy.mil lynn.simpson@nrl.navy.mil'
	     spawn,'mailx -s SECCHI-'+ab+'_'+teldir+'_DIRERR_'+yyyymmdd+' '+maillist+' < errmsg.txt',/sh
	     return
	ENDIF
	indir=getenv('secchi')+'/'+src+'/L0/'+ab+'/seq/'+teldir+'/'+yyyymmdd+'/'
	outdir=outdir0+'/'+strmid(yyyymmdd,0,4)+'/'+yyyymmdd+'/'

        check=file_search(outdir+'/'+strmid(trp(0).filename,0,15)+'*')
        if check(0) ne '' and ~keyword_set(doall) then goto,next

	for j=0,2 do print,trp[j]
	tais=utc2tai(str2utc(trp.date_obs))
	; make sure they are within 15 minutes of each other and are the same size
	; warn if obs_id is not the same
	notok=0
	IF total(trp.value) NE 360 THEN BEGIN
		message,'Polarizer angles do not add up; skipping',/info
		notok=1
		wait,2
		goto,next
	ENDIF ELSE $
	IF total(trp.xsize)/3 NE trp[0].xsize OR total(trp.ysize)/3 NE trp[0].ysize THEN BEGIN
		message,'Image sizes do not match; skipping.',/info
		notok=1
		wait,2
		goto,next
	ENDIF ELSE $
	IF tais[2]-tais[0] GT 900 THEN BEGIN
		message,'Images are'+string( tais[2]-tais[0])+ ' seconds apart.',/info
		notok=1
		wait,2
		goto,next
	ENDIF
	IF total(trp.osnum)/3 NE trp[0].osnum THEN BEGIN
		print,'WARNING: OSnums are not the same.'
		print
		;wait,2
	ENDIF
wait,2

	spawn,'mkdir '+outdir,/sh
	secchi_prep, indir+trp.filename, hdrout, imout, savepath=outdir, /polariz, /write_fts, $
			/calimg_off,/nocalfac, /precomm, /pipeline
			
;-- write summary file

	sumfile= concat_dir(getenv('SECCHI_P0')+'/'+ab+'/summary','scc'+strupcase(ab)+strmid(hdrout.filename,0,6)+'.pol')
	sccwritesummary,hdrout,sumfile
	sccwritesummary,pphdr,sumfile

	next:
ENDFOR ELSE message,'No '+strupcase(teldir)+' POLAR sequences for '+d1+'-'+d2
		
		
		
end
