pro scc_mk_all_daily,dtst,dtend, sc, tels, TBR_ONLY=tbr_only, HI=hi, COR2=cor2, DBL=DBL, $
	NOSMOOTH=nosmooth, _EXTRA=_extra
	
; $Id: scc_mk_all_daily.pro,v 1.7 2012/05/02 15:53:18 secchia Exp $
;
; Wrapper for scc_mk_daily_med.pro
;
; Keywords:
; 	/TBR_ONLY	Do not do double or indiv polarizers
;	/HI		Do HI1 and HI2
;	/COR2		Same as tels='cor2'
;	/DBL		Do COR2 DOUBLE
;	/NOSMOOTH	Do not run smoothpeaks at all; otherwise do every 4th day
;
; $Log: scc_mk_all_daily.pro,v $
; Revision 1.7  2012/05/02 15:53:18  secchia
; nr - by default only do smoothpeaks every 4th day, except for HI
;
; Revision 1.6  2011-11-17 18:49:05  nathan
; add tels argument
;
; Revision 1.5  2011/08/03 20:52:49  nathan
; use nrlanytim2utc
;
; Revision 1.4  2010/11/18 18:41:13  secchib
; added dbl keyword
;
; Revision 1.3  2010/05/12 15:17:34  nathan
; add /TBR_ONLY
;

IF n_params() LT 4 THEN BEGIN
    tels=['HI1','HI2', 'COR2']
    IF keyword_set(COR2) THEN tels=['COR2']
    IF keyword_set(DBL) THEN tels=['COR2']
    IF keyword_set(HI) THEN tels=['HI1','HI2']
ENDIF 
nt=n_elements(tels)

ut0=nrlanytim2utc(dtst)
ut1=nrlanytim2utc(dtend)

mjd=ut0

IF n_params() GT 2 THEN IF strlen(sc) GT 1 THEN ab=['a','b'] ELSE ab=sc
nsc=n_elements(ab)

FOR j=0,nsc-1 DO BEGIN
    aorb=ab[j]
    FOR k=0,nt-1 DO BEGIN
    	tel=tels[k]
	FOR i=ut0.mjd,ut1.mjd DO BEGIN
	    mjd.mjd=i
	    yyyymmdd=utc2yymmdd(mjd,/yyyy)
	    nosmoo=1
	    IF strmid(tel,0,2) EQ 'HI' THEN nosmoo=0 ELSE $
	    IF ~(i mod 4) THEN nosmoo=0 
	    IF keyword_set(NOSMOOTH) THEN nosmoo=1
	    
	    if not keyword_set(DBL)then begin
	      scc_mk_daily_med,tel,aorb,yyyymmdd,polar='TBR', NOSMOOTHPEAKS=nosmoo, _EXTRA=_extra
	      IF tel EQ 'COR2' and not keyword_set(TBR_ONLY) THEN BEGIN
		scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='0',NOSMOOTHPEAKS=nosmoo,_EXTRA=_extra
		scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='120',NOSMOOTHPEAKS=nosmoo,_EXTRA=_extra
		scc_mk_daily_med,'COR2',aorb,yyyymmdd,polar='240',NOSMOOTHPEAKS=nosmoo,_EXTRA=_extra
	      ENDIF
	    endif else scc_mk_daily_med,tel,aorb,yyyymmdd,polar='dbl', NOSMOOTHPEAKS=nosmoo, _EXTRA=_extra
	ENDFOR	;i
    ENDFOR	;k
ENDFOR	;j

end
