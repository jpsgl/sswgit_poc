;+
; $Id: cor1_mk_daily_med.pro,v 1.6 2014/08/27 20:25:21 thompson Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_MK_DAILY_MED
;
; Purpose     :	Generate COR1 daily "median" background image.
;
; Category    :	STEREO SECCHI COR1 Calibration
;
; Explanation :	This procedure generates a daily background image by taking all
;               the COR1 files of a given polarization angle for one day and
;               finding a background value for each pixel.  The data are first
;               broken down into chunks of one or more hours, and the median is
;               found for each chunk.  Then the chunks are combined to find the
;               minimum.  The first step filters out anomalous images, and
;               takes a statistical approach to the data.  The second step
;               minimizes the coronal contribution.
;
; Syntax      :	COR1_MK_DAILY_MED, SC, DATE, POLAR
;
; Examples    :	COR1_MK_DAILY_MED, 'A', '20070127', '000'
;
; Inputs      :	SC      = Either "a" or "b"
;               DATE    = The observation date, in the form YYYYMMDD.
;               POLAR   = Either "000", "120", "240", or "tbr" (tbr = total
;                         brightness)
;
; Opt. Inputs :	None.
;
; Outputs     :	FITS file in $SECCHI_BKG/[ab]/daily_med/YYYYMM/
;
; Opt. Outputs:	None.
;
; Keywords    :	SAVEDIR = Specify your own directory to save file to.
;               FILES   = Specify your own file list (this may not work
;                         properly yet!!)
;
; Calls       :	DATATYPE, PARSE_STEREO_NAME, SCC_DATA_PATH, SCC_READ_SUMMARY,
;               FILE_EXIST, SCCREADFITS, COR1_IMG_TRIM, COR_PREP, SECCHI_PREP,
;               AVERAGE, TAI2UTC, FXHMAKE, FXADDPAR, MK_DIR, WRITEFITS,
;               SSC_SIDELOBE_PERIOD
;
; Common      :	None.
;
; Restrictions:	Need $SECCHI_BKG and appropriate permissions if not using
;               SAVEDIR option.
;
; Side effects:	None.
;
; Prev. Hist. :	Based on SCC_MK_DAILY_MED by Karl Battams
;
; History     :	Version 1, 3-Apr-2007, William Thompson, GSFC
;               Version 2, 11-Oct-2007, WTT, add COR_PREP, /BKGIMG_OFF
;               Version 3, 24-Oct-2007, WTT, include DATE-AVG in header
;               Version 4, 11-May-2009, WTT, look for 512 images after 20090418
;               Version 5, 27-Aug-2014, WTT, Support side-lobe operations
;
; Contact     :	WTHOMPSON
;-
;
pro cor1_mk_daily_med, spacecraft, Date, polar, SAVEDIR=savedir, FILES=files, $
                       no_minimum=no_minimum
;
;  Check the input parameters.
;
if (n_params() LT 3) then message, 'Syntax: COR1_MK_DAILY_MED, SC, DATE, POLAR'
;
if (datatype(spacecraft) ne 'STR') or (n_elements(spacecraft) ne 1) then $
  message, 'SC must be either "A" or "B".
sc = parse_stereo_name(spacecraft, ['A','B'])
if (sc ne 'A') and (sc ne 'B') then message, $
  'Unrecognized spacecraft name ' + sc
;
if (datatype(date) ne 'STR') or (n_elements(date) ne 1) then $
  message, 'DATE must be a single string, e.g. 20070124".
if strlen(date) ne 8 then message, $
  'DATE must have the form YYYYMMDD, e.g. 20070124'
;
if (datatype(polar) ne 'STR') or (n_elements(polar) ne 1) then $
  message, 'POLAR must be a single string, e.g. "000".
;
;  Define the version.
;        
version='$Id: cor1_mk_daily_med.pro,v 1.6 2014/08/27 20:25:21 thompson Exp $'
len=strlen(version)
version=strmid(version,1,len-2)
;
if keyword_set(no_minimum) then n_median_min = 1 else $
  n_median_min = 50		; number of images needed to generate median
xaxis=1
yaxis=1
;        
pol=strlowcase(polar)
;
imgdir='seq/cor1/'

pref='c1'
if (pol EQ 'tbr') THEN pol_str='pTBr' ELSE $
  pol_str='p'+strcompress(pol,/remove_all)
xaxis=1024
yaxis=1024
if date gt '20090418' then begin
    xaxis = 512
    yaxis = 512
endif
;
;  If during side-lobe operations, then process beacon data.
;
if ssc_sidelobe_period(sc, date) then begin
    xaxis = 128
    yaxis = 128
    n_median_min = 1
endif

img_type=''
dt2 = strmid(date,0,4) + '-' + strmid(date,4,2) + '-' + strmid(date,6,2)
        
; Check if data exists yet...

s = scc_data_path(sc,source='lz',level=0,telescope='cor1',type='seq',/slash)
f=file_search(concat_dir(s,date+'/*fts'))
sz=size(f)
IF (sz(0) EQ 0)  THEN BEGIN
    PRINT,'No directory for COR1 telescope on '+date
    return
ENDIF
	
; Find appropriate files...

IF keyword_set(FILES) THEN BEGIN
    fnames = files 
    good = indgen(n_elements(files)) ; ??????????
ENDIF ELSE BEGIN
    summary = scc_read_summary(DATE=dt2, SPACECRAFT=sc, TELESCOPE='cor1', $
                               TYPE='seq')

    PRINT,'Found a total of ',n_elements(summary),' files for the day.'

    IF summary[0].filename EQ '' THEN begin
        PRINT,'First filename from summary file is blank'
        return
    ENDIF
                
; Either an individual COR polarizer angle, or total brightness is picked. 
                
    good = where((summary.XSIZE GE xaxis) and (summary.YSIZE GE yaxis) and $
                 (summary.PROG NE 'Dark'), count)
    if count eq 0 then return
    if (pol NE 'tbr') then begin
        ggood = where(summary[good].value eq pol, count)
        if count eq 0 then begin
            print, 'No entries found with polarizer value ' + POL
            return
        endif
        good = good[ggood]
    endif
    files=summary[good].FILENAME  
endelse

n=n_elements(files)  
path=s+date+'/'

midpoint=n/2

n_used=0

if (n LT n_median_min) THEN BEGIN
    PRINT,'ERROR: There are not enough images to make a good daily median image.'
    return
ENDIF
       
;
;  Determine the number of substeps to take.
;
stepsize = (ceil(sqrt(n)) > 5) < n
nsteps = ceil(float(n) / stepsize)
if (n-stepsize*(nsteps-1)) lt 5 then nsteps=(nsteps-1) > 1
;
;  Step through the files in subsets.  Save all the headers.
;
datacube=replicate(!values.f_nan, xaxis, yaxis, nsteps)
delvarx, hdr
for istep = 0,nsteps-1 do begin
    i1 = stepsize*istep
    i2 = (i1 + stepsize -1)
    if istep eq (nsteps-1) then i2 = n-1
    nfiles = i2 - i1 + 1
    tempcube = replicate(!values.f_nan, xaxis, yaxis, nfiles)
    for ifile = i1,i2 do begin
        file = concat_dir(path,files[ifile])
        if file_exist(file) then begin
;;
;;  This line changed to allow old upside-down data to be processed.
;;
;;        secchi_prep, file, h, im, /calfac_off, /calimg_off, /update_hdr_off
;;
            im = sccreadfits(file, h)
            im = cor1_img_trim(im,h)
            cor_prep, h, im, /calfac_off, /calimg_off, /update_hdr_off, $
              /bkgimg_off
            sz = size(im)
;;
;;  This line changed to allow older data to be processed.
;;
;;      if (sz[1] eq xaxis) and (sz[2] eq yaxis) and (h.nmissing eq 0) $
;;
            if (sz[1] eq xaxis) and (sz[2] eq yaxis) and (h.nmissing le 0) $
              then begin
                tempcube[*,*,ifile-i1] = im
                if n_used eq 0 then hdr = h else hdr = [hdr,h]
                n_used = n_used + 1
            endif
        endif
    endfor
;
;  Create median images for each segment.
;
    if nfiles eq 1 then datacube = tempcube else $
      datacube[*,*,istep] = median(tempcube, dimension=3)
endfor
if n_used eq 0 then begin
    print, 'No images were deemed usable'
    return
endif
;
;  If NSTEPS=1, then simply use the median image.
;
if nsteps eq 1 then median_array = datacube else begin
;
;  Filter out images which are far from the median of the other images.
;
    test = datacube
    test0 = median(datacube,dimension=3)
    for i=0,nsteps-1 do test[*,*,i] = test[*,*,i] - test0
    test = average(abs(test),[1,2])
    atest = median(test)
    stest = stddev(good_pixels(test))
    w = where(abs(test-atest) lt 2*stest, count)
    if count gt 0 then stest = stddev(good_pixels(test[w]))
    w = where(abs(test-atest) gt 3*stest, count)
    if count gt 0 then datacube[*,*,w] = !values.f_nan
;
;  Take the minimum of all the median images.
;
    median_array = min(datacube, dimension=3, /nan)
endelse
;
;  Calculate the average image date & time.
;
mid_date = tai2utc( average( utc2tai(hdr.date_obs) ), /ccsds)
mid_time = strmid(mid_date,11,12)
;
;  Create the FITS header.
;
fxhmake, outhdr, median_array, /date, /initialize
obsname='STEREO_'+strcompress(strupcase(sc),/remove_all)
fxaddpar,outhdr,'OBSRVTRY',obsname
fxaddpar,outhdr,'TELESCOP','STEREO'
fxaddpar,outhdr,'INSTRUME','SECCHI'
fxaddpar,outhdr,'DETECTOR','COR1'
fxaddpar,outhdr,'DATE-OBS',mid_date,' = MID_DATE'
fxaddpar,outhdr,'TIME-OBS',mid_time,' = MID_TIME'
fxaddpar,outhdr,'DATE-AVG',mid_date,' = MID_DATE'
fxaddpar,outhdr,'EXPTIME',1.0   ;Exposure time has been divided out
fxaddpar,outhdr,'EXPCMD',h.expcmd
fxaddpar,outhdr,'EXP0',average(hdr.exptime) ;Average exposure time
fxaddpar,outhdr,'MID_DATE',mid_date
fxaddpar,outhdr,'MID_TIME',mid_time
fxaddpar,outhdr,'FILTER',h.FILTER
fxaddpar,outhdr,'POLAR',h.POLAR
fxaddpar,outhdr,'P1COL',h.P1COL
fxaddpar,outhdr,'P1ROW',h.P1ROW
fxaddpar,outhdr,'P2COL',h.P2COL
fxaddpar,outhdr,'P2ROW',h.P2ROW
fxaddpar,outhdr,'SUMROW',h.SUMROW
fxaddpar,outhdr,'SUMCOL',h.SUMCOL
fxaddpar,outhdr,'SUMMED',h.SUMMED
fxaddpar,outhdr,'IPSUM',h.IPSUM
fxaddpar,outhdr,'CCDSUM',h.ccdsum
fxaddpar,outhdr,'R1COL',h.R1COL
fxaddpar,outhdr,'R1ROW',h.R1ROW
fxaddpar,outhdr,'R2COL',h.R2COL
fxaddpar,outhdr,'R2ROW',h.R2ROW
fxaddpar,outhdr,'CROTA',average(hdr.CROTA)
fxaddpar,outhdr,'OFFSETCR',average(hdr.OFFSETCR)
fxaddpar,outhdr,'N_IMAGES',n_used

sdir1 = GETENV('SECCHI_BKG')+'/'+strlowcase(sc)+'/daily_med/'
sdir2 = sdir1+strmid(date,0,6)
if not file_exist(sdir2) THEN BEGIN
    PRINT,'Directory ',sdir2, ' does not exist -- creating.'
    mk_dir, sdir2
ENDIF

IF keyword_set(SAVEDIR) THEN sdir = savedir ELSE sdir = sdir2

fname0 = 'd'+pref+sc+'_'+pol_str+'_'+strmid(date,2,6)+'.fts'

fxaddpar,outhdr,'FILENAME',fname0
PRINT,'Writing daily fits file: '+sdir2+'/'+fname0
print,' using '+string(n_used)+' images'
fxaddpar,outhdr,'COMMENT','Created by COR1_MK_DAILY_MED'
fxaddpar,outhdr,'HISTORY',version
	
WRITEFITS,concat_dir(sdir2,fname0),median_array,outhdr
;
RETURN
END
