;+
; $Id: scc_daily_synoptic.pro,v 1.2 2012/12/11 14:06:08 mcnutt Exp $
; NAME:;	scc_daily_synoptic
;
; PURPOSE:
;	call scc_synoptic wil euvia files list for a given day
;
; CATEGORY: movie analysis  
;
; CALLING SEQUENCE:
;	
;
; INPUTS:
;   yyyymmdd = day to process
;   
; KEYWORD inputs:
;
; OUTPUTS:
;   PNG synoptic movie written to $SECCHI_PNG/synoptic
;
; KEYWORD outputs:
;
; COMMON:   
;
; Calls from LASCO :  
;
; SIDE EFFECTS:
;	None
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;
; $Log: scc_daily_synoptic.pro,v $
; Revision 1.2  2012/12/11 14:06:08  mcnutt
; change output directories
;
; Revision 1.1  2012/10/09 17:14:47  mcnutt
; daily wrapers to scc_synoptic to generate the daiy synoptic maps
;
;
pro scc_daily_synoptic,yyyymmdd
    date=strmid(yyyymmdd,0,4)+'-'+strmid(yyyymmdd,4,2)+'-'+strmid(yyyymmdd,6,2)
    ut=tai2utc(anytim2tai(date))
    waves=['195','304','171'];,'284'
    osnum=[1622,1641,1624];,1623
    for n=0,n_Elements(waves)-1 do begin
      s=scc_read_summary(date=[ut],tel='euvi',spacec='a',type='img',quadrant=waves(n),/nobeacon)
      z=where(s.dest eq 'SSR1'and s.xsize eq 2048 and s.ysize eq 2048 and s.osnum eq osnum(n),count)
      if count ge 1 then begin
        files=sccfindfits(s(z).filename)
	pdir=getenv('SECCHI_PNG')
        outdir=pdir+'/synoptic/'+waves(n)+'/'+yyyymmdd
        spawn,'mkdir '+outdir 
        wait,2
        outdir=pdir+'/synoptic/'+waves(n)+'/'+yyyymmdd+'/carrington'
        spawn,'mkdir '+outdir 
        wait,2
        outmovie=outdir+'/yyyymmdd_hhmmss_SYeuA.png'
        scc_synoptic,files,times,datacube,scale=.25,movieout=outmovie,/display,/docolor,/all
        outdir=pdir+'/synoptic/'+waves(n)+'/'+yyyymmdd+'/stonyhurst'
        spawn,'mkdir '+outdir 
        wait,2
        outmovie=outdir+'/yyyymmdd_hhmmss_SHeuA.png'
        scc_synoptic,files,times,datacube,scale=.25,movieout=outmovie,/display,/docolor,/all,/stonyhurst
      endif
    endfor
END
