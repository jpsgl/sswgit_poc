#! /bin/csh -m
# $Id: make_daily_synoptic.csh,v 1.1 2012/10/09 17:14:47 mcnutt Exp $
# $Log: make_daily_synoptic.csh,v $
# Revision 1.1  2012/10/09 17:14:47  mcnutt
# daily wrapers to scc_synoptic to generate the daiy synoptic maps
#
#
# Made for automatic real-time application
# -32 is needed because this is a 64-bit computer. It will make the SPICE software work!
echo $HOME
source /net/cronus/opt/nrl_ssw_setup
setup
setenv AIA_SYNOPTIC /net/ssdfs0/AIA/synoptic/

idl7 $HOME/secchi/idl/daily/make_daily_synoptic.idl

