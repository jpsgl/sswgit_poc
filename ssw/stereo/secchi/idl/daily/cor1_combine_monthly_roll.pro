;+
; $Id: cor1_combine_monthly_roll.pro,v 1.3 2008/08/27 20:44:36 thompson Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_COMBINE_MONTHLY_ROLL
;
; Purpose     :	Combine monthly minimum and roll background images
;
; Category    :	STEREO SECCHI COR1 Calibration
;
; Explanation :	This procedure combines monthly minimum background images with
;               the calibration roll backgrounds.  Called from
;               COR1_MONTHLY_MIN.
;
; Syntax      :	COR1_COMBINE_MONTHLY_ROLL, FILENAME
;
; Examples    :	See COR1_MONTHLY_MIN
;
; Inputs      :	FILENAME = Name of monthly minimum file.
;
; Opt. Inputs :	None.
;
; Outputs     :	FITS file in $SECCHI_BKG/[ab]/monthly_roll/YYYYMM/
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	DATATYPE, PARSE_STEREO_NAME, SCCREADFITS, SCC_GETBKGIMG,
;               ASMOOTH, BREAK_FILE, FXHREAD, FXHMAKE, FXADDPAR, FILE_EXIST,
;               WRITEFITS
;
; Common      :	None.
;
; Restrictions:	Need $SECCHI_BKG and appropriate permissions.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 13-Aug-2008, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro cor1_combine_monthly_roll, filename
;
on_error, 2
;
;  Check the input parameters.
;
if n_params() ne 1 then message, 'Syntax: COR1_COMBINE_MONTHLY_ROLL, FILENAME'
if n_elements(filename) ne 1 then message, 'FILENAME must be scalar'
if datatype(filename) ne 'STR' then message, $
  'FILENAME must be a character string'
if not file_exist(filename) then message, $
  'File ' + FILENAME + ' does not exist'
;
;  Define the version.
;        
version='$Id: cor1_combine_monthly_roll.pro,v 1.3 2008/08/27 20:44:36 thompson Exp $'
len=strlen(version)
version=strmid(version,1,len-2)
;
;  Read in the monthly minimum image.
;
print, 'Reading ' + filename
b0 = sccreadfits(filename, h0)
;
;  Make sure the header has DATE_AVG.
;
no_date_avg = h0.date_avg eq ''
if no_date_avg then h0.date_avg = h0.date_obs  
;
;  Get the associated roll image.  If no files are found, then return.
;
b1 = scc_getbkgimg(h0, outdhr=h1, /raw_calroll, /interpolate, /silent)
if n_elements(b1) eq 1 then begin
    print, 'No roll background found for ' + filename
    return
endif
;
;  Select out the top 75% of data points.
;
x0 = median(b0)  &  x0 = median(b0[where(b0 lt x0)])
x1 = median(b1)  &  x1 = median(b1[where(b1 lt x1)])
w = where((b0 ge x0) and (b1 ge x1), count)
x = b0[w]
y = b1[w]
;
;  Calculate the average ratio between the two values, and remove points more
;  than two sigma away from the fitted line.
;
repeat begin
    n = n_elements(x)
    ratio = total(y) / total(x)
    diff = y - ratio*x
    w = where(abs(diff) lt 2*stddev(diff), count)
    if count gt 0 then begin
        x = x[w]
        y = y[w]
    endif
endrep until (n eq count) or (count le 1)
;
;  Scale the roll image to match the monthly minimum image.
;
b1 = b1/ratio
;
;  Form a mask to determine which background to use at each pixel.  Smooth the
;  mask to remove artifacts, and apply it to generate a new background.
;
b2 = b1 < b0
s = asmooth(float(b0 eq b2), 5)
b2 = s*b0 + (1-s)*b1
;
sc = parse_stereo_name(h0.obsrvtry, ['A','B'])

break_file, filename, disk, dir, name, ext
fname0 = 'mr' + strmid(name,1,strlen(name)-1) + ext

dir = strmid(dir,0,strlen(dir)-1)
break_file, dir, disk1, dir1, mdate

sdir1 = GETENV('SECCHI_BKG')+'/'+strlowcase(sc)+'/monthly_roll/'
sdir2 = sdir1+mdate
if not file_exist(sdir2) THEN BEGIN
    PRINT,'Directory ',sdir2, ' does not exist -- creating.'
    mk_dir, sdir2
ENDIF

openr, unit, filename, /get_lun
fxhread, unit, outhdr
free_lun, unit
if no_date_avg then fxaddpar, h, 'DATE-AVG', h0.date_obs, after='DATE-OBS'

fxhmake, outhdr, b2, /date
fxaddpar,outhdr,'FILENAME',fname0
PRINT,'Writing combined monthly/roll fits file: '+sdir2+'/'+fname0
fxaddpar,outhdr,'COMMENT','Created by COR1_COMBINE_MONTHLY_ROLL'
fxaddpar,outhdr,'HISTORY',version
	
WRITEFITS,concat_dir(sdir2,fname0),b2,outhdr
;
return
end
