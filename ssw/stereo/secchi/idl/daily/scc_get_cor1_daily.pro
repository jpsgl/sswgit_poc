pro scc_get_cor1_daily,dtst,dtend,nominus35=nominus35
;+
; $Id: scc_get_cor1_daily.pro,v 1.5 2012/11/01 17:48:34 secchia Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_get_cor1_daily
;               
; Purpose   : to get COR1 pretties from SSC Browse
;               
; Explanation: 
;               
; Use       : IDL> scc_get_cor1_daily,'YYYY-MM-DD'
;    
; Inputs    :dtst= starting day yyyy-mm-dd ,dtend= ending day yyyy-mm-dd 
;               
; Outputs   : from scc_daily_pretties JPG24 and PNG files in /net/earth/data3/secchi/images
;
; Keywords  : _EXTRA  see sc_daily_Pretties.pro for available keywords
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to disk, enviromenal variable sc must be defined.
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2008
;               
; $Log: scc_get_cor1_daily.pro,v $
; Revision 1.5  2012/11/01 17:48:34  secchia
; added keyword nominus35 used for recovery
;
; Revision 1.4  2011/08/11 12:23:32  secchib
; commented out call to mpegframes
;
; Revision 1.3  2008/05/20 15:07:29  secchia
; moved last date to be set after day-1
;
; Revision 1.2  2008/05/20 14:39:32  secchia
; tested and corrected
;
; Revision 1.1  2008/05/15 13:32:12  mcnutt
; copies COR1 daily proetties from SSC
;

if strlen(dtst) gt 8 then ut0=anytim2utc(dtst) else ut0=anytim2utc(strmid(dtst,0,4)+'-'+strmid(dtst,4,2)+'-'+strmid(dtst,6,2))

ut0.mjd=ut0.mjd-1
if datatype(dtend) ne 'UND' then ut1=anytim2utc(dtend) else ut1=ut0

mjd=ut0

FOR i=ut0.mjd,ut1.mjd DO BEGIN
    mjd.mjd=i
    yyyymmdd=utc2yymmdd(mjd,/yyyy)
    syscmd='~/bin/get_ssc_browse.sh '+yyyymmdd
    spawn,syscmd
    if ~keyword_set(nominus35) then begin
      mjd.mjd=mjd.mjd-35  ;COR1 updated after 35 at ssc
      yyyymmddm35=utc2yymmdd(mjd,/yyyy)
      syscmd='~/bin/get_ssc_browse.sh '+yyyymmddm35
      spawn,syscmd
    endif
    mpegframes,yyyymmdd,'cor1'
ENDFOR

end
