pro mpegframes,yyyymmdd,tel,nominus30=nominus30,nomail=nomail,_EXTRA=_EXTRA

; $Id: mpegframes.pro,v 1.22 2018/12/26 19:53:03 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : mpegframes
;               
; Purpose   : calls daily_select_njpegs and combines weekly mpegs files lists for both SC and all Tels for yyyymmdd - 1
;               
; Explanation: 
;               
; Use       : IDL> mpegframes,yyyymmdd
;    
; Inputs    :yyyymmdd = current day processing
;
;
; Optional Inputs:
;              
; Outputs   : creates weekly bi-weekly and monthly mpeg file list for sccmovies
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Jan 2008
;               
; $Log: mpegframes.pro,v $
; Revision 1.22  2018/12/26 19:53:03  nathan
; nr - update png file directory
;
; Revision 1.21  2017/10/18 16:17:34  secchia
; correct mpeg movie directories
;
; Revision 1.20  2011/06/29 19:30:22  nathan
; changed maillist to secchipipeline@cronus
;
; Revision 1.19  2010/12/14 16:13:46  mcnutt
; correct minus 30 days
;
; Revision 1.18  2010/08/25 15:47:24  mcnutt
; added cadence to cor2 and his
;
; Revision 1.17  2009/12/11 12:40:13  mcnutt
; corrected EUVI171 and 284 for 2 week movies after 090819
;
; Revision 1.16  2009/12/03 13:55:54  mcnutt
; corrected daily pretties dir
;
; Revision 1.15  2009/12/02 18:14:02  secchib
; corrected yyyymmddm1 in daiiy prettie check
;
; Revision 1.14  2009/12/02 18:04:12  mcnutt
; uses scc_pngplay to create browse movies
;
; Revision 1.13  2008/06/16 12:46:24  mcnutt
; correct day -35 for cor1
;
; Revision 1.12  2008/06/02 16:28:25  mcnutt
; change cor1 movies to weekly
;
; Revision 1.11  2008/05/20 15:13:35  secchia
; correct output file anme for cor1 minus35
;
; Revision 1.10  2008/05/20 14:46:51  secchia
; chaeck for _m3 for minus30 and minus35 logs
;
; Revision 1.9  2008/05/20 14:39:52  secchia
; added cor1 settings
;
; Revision 1.8  2008/05/15 13:31:38  mcnutt
; added cor1
;
; Revision 1.7  2008/04/28 13:36:43  mcnutt
; included call to daily_select_njpegs with minus30 keyword
;
; Revision 1.6  2008/03/12 18:54:23  secchib
; added slash to outpur dir
;
; Revision 1.5  2008/03/12 14:36:48  mcnutt
; changed output dir to mpegframes
;
; Revision 1.4  2008/02/27 17:40:19  secchia
; changed ud1 to ud and commented out domissing keyword in call to daily_select_njpegs
;
; Revision 1.3  2008/02/27 14:48:10  mcnutt
; corrected ud1
;
; Revision 1.2  2008/02/27 14:35:58  mcnutt
; changed tel to an input aorb is always a
;
; Revision 1.1  2008/02/25 18:28:47  mcnutt
; creates mpegframe list for all telescopes
;


       tels=tel ;['euvi','cor2','hi1','hi2']
    	IF keyword_set(nomail) THEN sendmsg=0 ELSE sendmsg=1

        ud=anytim2utc(strmid(yyyymmdd,0,4)+'-'+strmid(yyyymmdd,4,2)+'-'+strmid(yyyymmdd,6,2))
        udi=ud
	ud.mjd=ud.mjd-1
	yyyymmddm1=utc2yymmdd(ud,/yyyy)
	yyyy=strmid(yyyymmddm1,0,4)
        doya=doy(yyyy+'-'+strmid(yyyymmddm1,4,2)+'-'+strmid(yyyymmddm1,6,2))
	;add the last 1 or 2 days of the year to previous week since a year is not eq to 52 weeks exaclty 
        if doya ge 364 then if strmid(yyyymmddm1,4,4) ne '1231' then doya=363 else doya=364  
	doeuvi=(doya mod 7)
        docor=(doya mod 14)
        dohi2=(doya mod 28)

	ud.mjd=ud.mjd-30
	yyyymmddm30=utc2yymmdd(ud,/yyyy)
        doyb=doy(strmid(yyyymmddm30,0,4)+'-'+strmid(yyyymmddm30,4,2)+'-'+strmid(yyyymmddm30,6,2))
	;add the last 1 or 2 days of the year to previous week since a year is not eq to 52 weeks exaclty 
        if doyb ge 364 then if strmid(yyyymmddm30,4,4) ne '1231' then doyb=363 else doyb=364  
        docor_m30=(doyb mod 14)
        dohi2_m30=(doyb mod 28)
                   
	ud.mjd=ud.mjd-5  ;COR1 updated after 35 at ssc
	yyyymmddm35=utc2yymmdd(ud,/yyyy)
        doyb=doy(strmid(yyyymmddm35,0,4)+'-'+strmid(yyyymmddm35,4,2)+'-'+strmid(yyyymmddm35,6,2))
	;add the last 1 or 2 days of the year to previous week since a year is not eq to 52 weeks exaclty 
        if doyb ge 364 then if strmid(yyyymmddm30,4,4) ne '1231' then doyb=363 else doyb=364  
        docor_m35=(doyb mod 7)
 
                    ntl=n_Elements(tels)
		    FOR j=0,ntl-1 DO BEGIN
			    teln=strlowcase(tels(j))
                            teld=teln
    	    	    	    IF teln EQ 'hi1' or  teln EQ 'hi2' THEN teld='hi'
    	    	    	    IF teln EQ 'cor2' THEN teld='cor'
			    IF teln eq 'cor1' then imgdir='SECCHI_JPG' else imgdir='SECCHI_PNG'
                            acheck=file_search(getenv(imgdir)+'/a/'+teld+'/'+yyyy+'/'+yyyymmddm1+'/512/*')            
                            bcheck=file_search(getenv(imgdir)+'/b/'+teld+'/'+yyyy+'/'+yyyymmddm1+'/512/*')
                            if bcheck[0] eq '' and long(yyyymmdd) gt 20150101 then no_b=1 else no_b=0
                            if (acheck[0] eq ''  ) then begin 
                                 mailfile=getenv_slash('HOME')+'sca_pretties_'+yyyymmddm1+'.tmp'
				 msg='Daily pretties missing for '+teln+' a  on '+yyyymmddm1
                                 openw,5,mailfile
				 box_message,msg
                                 printf,5,   msg
                                 wait,2
                                 close,5
                                 repro_subj='a_Missing_Daily_'+teln+'_'+yyyymmddm1
                                 repro_mail='secchipipeline@cronus.nrl.navy.mil'
				 IF (sendmsg) THEN BEGIN
                                 spawn,'mailx -s '+repro_subj+' '+repro_mail+' <'+mailfile
                                 wait,30
				 ENDIF
                                 spawn, '/bin/rm -f '+mailfile
                           ENDIF ELSE print,teln,'  ',n_Elements(acheck)
                            if (bcheck[0] eq '' and no_b ne 1)then begin 
                                 mailfile=getenv_slash('HOME')+'scb_pretties_'+yyyymmddm1+'.tmp'
                                 openw,5,mailfile
				 msg='Daily pretties missing for '+teln+' b  on '+yyyymmddm1
				 box_message,msg
                                 printf,5,   msg
                                 wait,2
                                 close,5
                                 repro_subj='b_Missing_Daily_'+teln+'_'+yyyymmddm1
                                 repro_mail='secchipipeline@cronus.nrl.navy.mil'
				 IF (sendmsg) THEN BEGIN
                                 spawn,'mailx -s '+repro_subj+' '+repro_mail+' <'+mailfile
                                 wait,30
				 ENDIF
                                 spawn, '/bin/rm -f '+mailfile
                           ENDIF ELSE print,teln,'  ',n_Elements(bcheck)
                    ENDFOR
		    FOR j=0,ntl-1 DO BEGIN
			    teln=strlowcase(tels(j))
                            teld=teln
    	    	    	    IF teln EQ 'hi1' THEN teln='hi_1'
    	    	    	    IF teln EQ 'hi2' THEN teln='hi_2'

                                     if teln eq 'euvi' and doeuvi eq 0 then begin
                                      ud.mjd=udi.mjd-7
                                      d2=utc2yymmdd(ud,/yyyy)
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/EUVI195/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_eu195.mpg'
                                      if no_b then  mvicam=['euvi_195a'] else mvicam=['euvi_195b','euvi_195a']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,cadence=20,imgsize=512,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/EUVI304/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_eu304.mpg'
                                      if no_b then mvicam=['euvi_304a']  else mvicam=['euvi_304b','euvi_304a']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,cadence=20,imgsize=512,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
                                      if long(yyyymmddm1) le 20090819 then begin
 				        mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/EUVI171/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_eu171.mpg'
                                        if no_b then  mvicam=['euvi_171a'] else mvicam=['euvi_171b','euvi_171a']
                                        scc_pngplay,d2,yyyymmddm1,mvicam,cadence=20,imgsize=512,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				        mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/EUVI284/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_eu284.mpg'
                                        if no_b then   mvicam=['euvi_284a'] else mvicam=['euvi_284b','euvi_284a']
                                        scc_pngplay,d2,yyyymmddm1,mvicam,cadence=20,imgsize=512,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
                                      endif
				   endif
                                   if teln eq 'euvi' and docor eq 0 and long(yyyymmddm1) gt 20090819 then begin
                                      ud.mjd=udi.mjd-14
                                      d2=utc2yymmdd(ud,/yyyy)
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/EUVI171/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_eu171.mpg'
                                      if no_b then mvicam=['euvi_171a'] else mvicam=['euvi_171b','euvi_171a']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,imgsize=512,cadence=120,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/EUVI284/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_eu284.mpg'
                                      if no_b then mvicam=['euvi_284a'] else mvicam=['euvi_284b','euvi_284a']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,imgsize=512,cadence=120,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				   endif
                                   if teln eq 'cor2' or teln eq 'hi_1' and docor eq 0 then begin
                                      ud.mjd=udi.mjd-14
                                      d2=utc2yymmdd(ud,/yyyy)
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/COR2/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_cor2.mpg'
                                      if no_b then mvicam=['cor2a'] else mvicam=['cor2b','cor2a']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,imgsize=512,cadence=60,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/HI1/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_hi1.mpg'
                                      if no_b then mvicam=['hi1a'] else mvicam=['hi1a','hi1b']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,imgsize=512,cadence=45,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				    endif
                                   if teln eq 'cor1' and doeuvi eq 0 then begin
                                      ud.mjd=udi.mjd-7
                                      d2=utc2yymmdd(ud,/yyyy)
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/COR1/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_cor1.mpg'
                                      if no_b then mvicam=['cor1a'] else mvicam=['cor1b','cor1a']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,cadence=20,/jpg,imgsize=512,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				    endif
                                   if teln eq 'hi_2' and dohi2 eq 0 then begin
                                      ud.mjd=udi.mjd-28
                                      d2=utc2yymmdd(ud,/yyyy)
				      mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/HI2/'+d2+'_'+strmid(yyyymmddm1,4,4)+'_hi2.mpg'
                                      if no_b then mvicam=['hi2a'] else mvicam=['hi2a','hi2b']
                                      scc_pngplay,d2,yyyymmddm1,mvicam,imgsize=512,cadence=120,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				    endif
                                   if ~keyword_set(nominus30) then begin
                                     if teln eq 'cor2' or teln eq 'hi_1' and docor_m30 eq 0 then begin
                                        ud.mjd=udi.mjd-(30+14)
                                        d2=utc2yymmdd(ud,/yyyy)
				        mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/COR2/'+d2+'_'+strmid(yyyymmddm30,4,4)+'_cor2.mpg'
                                        if no_b then mvicam=['cor2a'] else mvicam=['cor2b','cor2a']
                                        scc_pngplay,d2,yyyymmddm30,mvicam,imgsize=512,cadence=60,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				        mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/HI1/'+d2+'_'+strmid(yyyymmddm30,4,4)+'_hi1.mpg'
                                        if no_b then mvicam=['hi1a'] else mvicam=['hi1a','hi1b']
                                        scc_pngplay,d2,yyyymmddm30,mvicam,imgsize=512,cadence=45,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				      endif
                                      if teln eq 'hi_2' and dohi2_m30 eq 0 then begin
                                        ud.mjd=udi.mjd-(30+28)
                                        d2=utc2yymmdd(ud,/yyyy)
				        mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/HI2/'+d2+'_'+strmid(yyyymmddm30,4,4)+'_hi2.mpg'
                                        if no_b then mvicam=['hi2a'] else mvicam=['hi2a','hi2b']
                                        scc_pngplay,d2,yyyymmddm30,mvicam,imgsize=512,cadence=120,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				      endif
                                     if teln eq 'cor1' and docor_m35 eq 0 then begin
                                        ud.mjd=udi.mjd-(35+7)
                                        d2=utc2yymmdd(ud,/yyyy)
				        mfile='/net/earth/secchi/movies/mpegs/'+strmid(d2,0,4)+'/COR1/'+d2+'_'+strmid(yyyymmddm35,4,4)+'_cor1.mpg'
                                        if no_b then mvicam=['cor1a'] else mvicam=['cor1b','cor1a']
                                        scc_pngplay,d2,yyyymmddm35,mvicam,cadence=20,/jpg,imgsize=512,mpegname=mfile, SCALE=8, REFERENCE='DECODED'
				      endif
                                  endif

                  ENDFOR

end
