;+
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_BKG_PB2TBR
;
; Purpose     :	Form 'pTBr' background files out of pB sequences
;
; Category    :	STEREO SECCHI COR1 Calibration
;
; Explanation :	This procedure reads in the three polarization background files
;               for a given type and date, and averages them together to form a
;               total brightness "pTBr" background file to support the total
;               brightness images generated starting July 2011.  This is done
;               instead of generating backgrounds directly from the total
;               brightness images so that the pB series and totB images will be
;               consistent with each other.
;
; Syntax      :	COR1_BKG_PB2TBR, SC, DT
;
; Examples    :	COR1_BKG_PB2TBR, 'A', '20110709'
;
; Inputs      :	SC = 'A', 'B'
;               DT = Date to be processed
;
; Opt. Inputs :	None.
;
; Outputs     :	Writes out a 'pTBr' file.
;
; Opt. Outputs:	None.
;
; Keywords    :	TYPE = String containing the type of file to process.  Must be
;                      one of the following: 'daily_med' (default),
;                      'monthly_min', 'monthly_roll', 'roll_min'
;
; Calls       :	PARSE_STEREO_NAME, DATATYPE, CONCAT_DIR, FXREAD, FXHMAKE,
;               FXADDPAR, BREAK_FILE, UTC2TAI, TAI2UTC, AVERAGE, FXWRITE
;
; Common      :	None.
;
; Restrictions:	Need write access to $SECCHI_BKG
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 13-Jul-2011, William Thompson, GSFC
;               Version 2, 27-Nov-2012, WTT, do either 0,120,240 or 60,180,300
;
; Contact     :	WTHOMPSON
;-
;
pro cor1_bkg_pb2tbr, spacecraft, dt, type=type
;
if n_params() ne 2 then message, 'Syntax: COR1_BKG_PB2TBR, SC, DATE'
;
;  Check the spacecraft name.
;
if (datatype(spacecraft) ne 'STR') or (n_elements(spacecraft) ne 1) then $
  message, 'SC must be either "A" or "B".
sc = parse_stereo_name(spacecraft, ['A','B'])
if (sc ne 'A') and (sc ne 'B') then message, $
  'Unrecognized spacecraft name ' + sc
;
;  Check the date.
;
date = anytim2cal(dt, form=8, /date)
if (datatype(date) ne 'STR') or (n_elements(date) ne 1) then $
  message, 'DATE must be a single string, e.g. "20070124".'
;
;  Check the TYPE.
;
if n_elements(type) eq 0 then type = 'daily_med'
if (datatype(type) ne 'STR') or (n_elements(type) ne 1) then $
  message, 'TYPE must be a single string, e.g. "daily_med".'
;
;  Form the path to the FITS files.
;
path = concat_dir(getenv('SECCHI_BKG'), strlowcase(sc) + '/' + type + '/' + $
                  strmid(date,0,6))
;
;  Get the proper prefix for the type of file.
;
case type of
    'daily_med':    prefix = 'dc1'
    'monthly_min':  prefix = 'mc1'
    'monthly_roll': prefix = 'mrc1'
    'roll_min':     prefix = 'rc1'
    else:  message, 'Unrecognized TYPE: ' + type
endcase
;
;  Form the filenames.
;
pol = ['000','120','240','TBr']
files = concat_dir(path, prefix + sc + '_p' + pol + '_' + strmid(date,2,6) + $
                   '.fts')
;
if not file_exist(files[0]) then begin
    pol = ['060','180','300','TBr']
    files = concat_dir(path, prefix + sc + '_p' + pol + '_' + $
                       strmid(date,2,6) + '.fts')
endif
;
;  Read in the polarization sequence.
;
errmsg=''
fxread, files[0], a0, h0, errmsg=errmsg
if errmsg ne '' then begin
    print, 'Unable to read file ' + files[0]
    print, errmsg
    return
endif
fxread, files[1], a1, h1, errmsg=errmsg
if errmsg ne '' then begin
    print, 'Unable to read file ' + files[1]
    print, errmsg
    return
endif
fxread, files[2], a2, h2, errmsg=errmsg
if errmsg ne '' then begin
    print, 'Unable to read file ' + files[2]
    print, errmsg
    return
endif
;
;  Average the three images, and initialize the output header.
;
aa = (a0 + a1 + a2) / 3
hh = h0
fxhmake, hh, aa, /date
fxaddpar, hh, 'POLAR', 1001.
break_file, files[3], disk, dir, name, ext
fxaddpar, hh, 'FILENAME', name+ext
fxaddpar, hh, 'COMMENT', 'Combined by COR1_BKG_PB2TBR'
;
;  Average the observation dates.
;
date_obs = [fxpar(h0,'DATE-OBS'), fxpar(h1,'DATE-OBS'), fxpar(h2,'DATE-OBS')]
date_obs = tai2utc(average(utc2tai(date_obs)), /ccsds)
fxaddpar, hh, 'DATE-OBS', date_obs
fxaddpar, hh, 'TIME_OBS', strmid(date_obs,11,12)
;
date_avg = [fxpar(h0,'DATE-AVG'), fxpar(h1,'DATE-AVG'), fxpar(h2,'DATE-AVG')]
date_avg = tai2utc(average(utc2tai(date_avg)), /ccsds)
fxaddpar, hh, 'DATE-AVG', date_avg
;
mid_date = [fxpar(h0,'MID_DATE'), fxpar(h1,'MID_DATE'), fxpar(h2,'MID_DATE')]
mid_date = tai2utc(average(utc2tai(mid_date)), /ccsds)
fxaddpar, hh, 'MID_DATE', mid_date
fxaddpar, hh, 'MID_TIME', strmid(mid_date,11,12)
;
;  If in the header, take the minimum of N_IMAGES.
;
n_images = fxpar(h0,'N_IMAGES',count=count)
if count gt 0 then begin
    n_images = [n_images, fxpar(h1,'N_IMAGES'), fxpar(h2, 'N_IMAGES')]
    n_images = min(n_images)
    fxaddpar, hh, 'N_IMAGES', n_images
endif
;
;  If in the header, take the minimum of N_DAYS.
;
n_days = fxpar(h0,'N_DAYS',count=count)
if count gt 0 then begin
    n_days = [n_days, fxpar(h1,'N_DAYS'), fxpar(h2, 'N_DAYS')]
    n_days = min(n_days)
    fxaddpar, hh, 'N_DAYS', n_days
endif
;
;  If in the header, take the minimum of FIRSTDAY.
;
firstday = fxpar(h0,'FIRSTDAY',count=count)
if count gt 0 then begin
    firstday = [firstday, fxpar(h1,'FIRSTDAY'), fxpar(h2, 'FIRSTDAY')]
    firstday = min(firstday)
    fxaddpar, hh, 'FIRSTDAY', firstday
endif
;
;  If in the header, take the maximum of LAST_DAY.
;
last_day = fxpar(h0,'LAST_DAY',count=count)
if count gt 0 then begin
    last_day = [last_day, fxpar(h1,'LAST_DAY'), fxpar(h2, 'LAST_DAY')]
    last_day = max(last_day)
    fxaddpar, hh, 'LAST_DAY', last_day
endif
;
;  Average several other parameters in the header.
;
param = ['EXP0', 'CROTA', 'OFFSETCR']
for i=0,n_elements(param)-1 do begin
    val = [fxpar(h0, param[i]), fxpar(h1, param[i]), fxpar(h2, param[i])]
    val = average(val)
    fxaddpar, hh, param[i], val
endfor
;
;  Write out the 'pTBr' file.
;
fxwrite, files[3], hh, aa, errmsg=errmsg
if errmsg ne '' then begin
    print, 'Unable to write file ' + files[3]
    print, errmsg
endif
;
end
