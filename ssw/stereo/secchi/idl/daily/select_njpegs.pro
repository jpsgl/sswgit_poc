function select_njpegs,sc,yyyymmdd,tel,nfiles,binsize,wl
;+
; $Id: select_njpegs.pro,v 1.5 2008/05/15 13:30:54 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : select_njpegs
;               
; Purpose   : selects evenly spaced jpeg files from SECCHI_JPG directories
;               
; Explanation: 
;               
; Use       : IDL> select_njpegs,sc,yyyymmdd,tel,nfiles,binsize,wl
;    
; Inputs    :sc = 'A', 'B'
;            yyyymmdd = select images from this day
;            tel = euvi, cor2, hi1, hi2
;            nfiles = number of evenly space files (returned files my be less but not more than nfiles)
;            binsize= 256, 512 or 1024  defaults to 1024 if not specified
;            wl = '171','195','284', or '304' defaults to '171' used for euvi only
;              
; Outputs   : return names of evelnly spaced image files
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Jan 2008
;               
; $Log: select_njpegs.pro,v $
; Revision 1.5  2008/05/15 13:30:54  mcnutt
; added cor1
;
; Revision 1.4  2008/01/16 20:45:03  nathan
; changed wl to be string
;
; Revision 1.3  2008/01/16 19:25:05  mcnutt
; added nallow for cor2 images
;
; Revision 1.2  2008/01/16 18:11:56  mcnutt
; added nallow for hi images to limit spacing options
;
; Revision 1.1  2008/01/16 15:41:14  mcnutt
; New returns selected jpeg file names
;

if(datatype(binsize) eq 'UND')then binsize=512

if (binsize gt 1000)then binsize=string(binsize,'(i4.4)') else binsize=string(binsize,'(i3.3)')

if (strlowcase(tel) eq 'cor2') then teld='cor'
if (strlowcase(tel) eq 'euvi') then teld='euvi'
if (strlowcase(strmid(tel,0,2)) eq 'hi') then teld='hi'
if (strlowcase(tel)eq 'cor1') then teld='cor1' 

imgdir=getenv('SECCHI_JPG')+'/'+strlowcase(sc)+'/'+teld+'/'+yyyymmdd+'/'+binsize 
imgfiles=file_search(imgdir+'/*')
imgtimes=(strmid(imgfiles,strlen(imgfiles(0))-16,2)*60*60.)+(strmid(imgfiles,strlen(imgfiles(0))-14,2)*60.)+(strmid(imgfiles,strlen(imgfiles(0))-14,2))*1.

if (strlowcase(tel) eq 'euvi') then begin
  if(datatype(wl) eq 'UND')then wl='171' 
  if datatype(wl) NE 'STR' THEN wl=string(wl,'(i3.3)') 
  wls=strmid(imgfiles,strlen(imgfiles(0))-9,2)
  z1=where(wls eq strmid(wl,0,2))
endif
if (strlowcase(tel) eq 'hi1') then begin
  whi=strmid(imgfiles,strlen(imgfiles(0))-7,2)
  z1=where(whi eq 'h1')
  nallow=[1,2,3,4,6,12,18,36] & na=where(nallow le nfiles) & nfiles=nallow(na(n_elements(na)-1))
endif
if (strlowcase(tel)eq 'hi2') then begin
  whi=strmid(imgfiles,strlen(imgfiles(0))-7,2)
  z1=where(whi eq 'h2')
  nallow=[1,2,3,4,6,12] & na=where(nallow le nfiles) & nfiles=nallow(na(n_elements(na)-1))
endif
if (strlowcase(tel) eq 'cor2') then begin
  nallow=[1,2,3,4,6,12,24,48] & na=where(nallow le nfiles) & nfiles=nallow(na(n_elements(na)-1))
  z1=indgen(n_elements(imgfiles))*1
endif
if (strlowcase(tel) eq 'cor1') then begin
  nallow=[1,2,3,4,6,12,24,48] & na=where(nallow le nfiles) & nfiles=nallow(na(n_elements(na)-1))
  z1=indgen(n_elements(imgfiles))*1
endif

imgtime=0 & imgfile=''
if z1(0) gt -1 then begin
  imgtime=imgtimes(z1) & imgfile=imgfiles(z1)
endif

spacing=(24*60.*60)/nfiles
aproxtimes=findgen(nfiles)*spacing +imgtime(0)
aproxtimes=aproxtimes(where(aproxtimes le max(imgtime)))
imgsel=intarr(n_elements(aproxtimes))
for n=0,n_Elements(aproxtimes)-1 do begin
   imgse=where(imgtime ge aproxtimes(n))
   imgsel(n)=imgse(0)
   if (n gt 0) then if (imgsel(n) eq imgsel(n-1)) then imgsel(n-1)=-1 ;only list a image once this should work across data gaps
endfor

tjpg=where(imgsel ne -1)
if(tjpg(0) ne -1)then begin
  imgsel=imgsel(tjpg) 
  mfiles=imgfile(imgsel)
endif else mfiles=''

;print,nfiles
;plot,imgtimes,imgtimes
;oplot,imgtime(imgsel),imgtime(imgsel),psym=5
;oplot,aproxtimes,aproxtimes,psym=2


return,mfiles

end   
