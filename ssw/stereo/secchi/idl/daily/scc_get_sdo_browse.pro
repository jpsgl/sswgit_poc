pro scc_get_sdo_browse,dtst,dtend, IMGSIZE=imgsize, INSTRUME=instrume, WAVELNTH=wavelnth
;+
; $Id: scc_get_sdo_browse.pro,v 1.1 2011/01/20 15:33:15 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : 
;               
; Purpose   : copy SDO browse images from web to $SECCHI_JPG
;               
; Explanation: 
;               
; Use       : IDL> scc_get_sdo_browse,'YYYY-MM-DD'
;    
; Inputs    :
;   	dtst= starting day yyyy-mm-dd
;
; Optional input:
;   	dtend= ending day yyyy-mm-dd 
;               
; Outputs   : none
;
; Keywords  : _EXTRA  see sc_daily_Pretties.pro for available keywords
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to $SECCHI_JPG/sdo
;               
; Side effects: 
;               
; Category    : socket archive browse http 
;               
; Prev. Hist. : None.
;
; Written     :NR/NRL 2011
;               
; $Log: scc_get_sdo_browse.pro,v $
; Revision 1.1  2011/01/20 15:33:15  nathan
; get (currently) 4 wavelengths from sdo.gsfc.nasa.gov and put in $SECCHI_JPG
;
ut0=nrlanytim2utc(dtst) 
IF n_params() GT 1 then ut1=anytim2utc(dtend) else ut1=ut0
IF keyword_set(INSTRUME) THEN inst=instrume ELSE inst='aia'
IF keyword_set(IMGSIZE) THEN siz=trim(imgsize) ELSE siz='512'
IF keyword_set(WAVELNTH) THEN waves=string(wavelnth,'(i4.4)') ELSE waves=['0304','0211','0193','0171']
nwv=n_elements(waves)

website='http://sdo.gsfc.nasa.gov'

mjd=ut0
FOR i=ut0.mjd,ut1.mjd DO BEGIN
    mjd.mjd=i
    yyyymmdd=utc2yymmdd(mjd,/yyyy)
    remodate =utc2str(mjd,/ecs,/date)
    locdir =getenv('SECCHI_JPG')+'/sdo/'+inst+'/'+yyyymmdd+'/'+siz
    remdir ='/assets/img/browse/'+remodate
    spawn,['mkdir','-p',locdir],/noshell
    FOR j=0,nwv-1 DO BEGIN
    	srchstr='*_'+siz+'_'+waves[j]+'.jpg'
	help,website,remdir,srchstr
    	li=sock_find(website,srchstr,path=remdir)
	help,li
	nli=n_elements(li)
	FOR k=0,nli-1 DO BEGIN
	    break_file,li[k],di,pa,ro,sf
	    outfile=strmid(ro,0,16)+rstrmid(ro,0,4)+strmid(inst,1,1)+sf
	    sock_get,li[k],outfile,OUT_DIR=locdir
	ENDFOR
    ENDFOR
ENDFOR


       
end
