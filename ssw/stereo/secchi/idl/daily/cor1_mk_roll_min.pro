pro cor1_mk_roll_min, catalog, SAVEDIR=savedir
;+
; $Id: cor1_mk_roll_min.pro,v 1.3 2010/07/12 15:18:00 thompson Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_MK_ROLL_MIN
;
; Purpose     :	Generate COR1 roll minimum background image.
;
; Category    :	STEREO SECCHI COR1 Calibration
;
; Explanation :	This procedure generates a background image from a calibration
;               roll.  The background is formed by taking the minimum value
;               during the roll at each pixel.
;
; Syntax      :	COR1_MK_ROLL_MIN, CATALOG
;
; Examples    :	list = cor1_pbseries('2008-06-25', 'b')
;               w = where(list[0,*].xsize ne 1024)      ;Selects out roll data
;               cor1_mk_roll_min, list[*,w]
;
; Inputs      :	CATALOG = Structure from SCC_READ_SUMMARY
;
; Opt. Inputs :	None.
;
; Outputs     :	FITS file in $SECCHI_BKG/[ab]/roll_min/YYYYMM/
;
; Opt. Outputs:	None.
;
; Keywords    :	SAVEDIR = Specify your own directory to save file to.
;
; Calls       :	DATATYPE, ALL_VALS, PARSE_STEREO_NAME, SECCHI_PREP, TAI2UTC,
;               UTC2TAI, ANYTIM2CAL, FXHMAKE, FXADDPAR, FILE_EXIST, WRITEFITS
;
; Common      :	None.
;
; Restrictions:	Need $SECCHI_BKG and appropriate permissions if not using
;               SAVEDIR option.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 13-Aug-2008, William Thompson, GSFC
;               Version 2, 09-Jul-2010, WTT, don't expand file size
;
; Contact     :	WTHOMPSON
;-
;
on_error, 2
;
;  Check the input parameters.
;
if (n_params() LT 1) then message, 'Syntax: COR1_MK_ROLL_MIN, CATALOG'
;
if datatype(catalog) ne 'STC' then message, 'CATALOG must be a structure'
;
;  Define the version.
;        
version='$Id: cor1_mk_roll_min.pro,v 1.3 2010/07/12 15:18:00 thompson Exp $'
len=strlen(version)
version=strmid(version,1,len-2)
;
;  Extract the spacecraft.
;
spacecraft = all_vals(catalog.spacecraft)
if n_elements(spacecraft) ne 1 then message, $
  'Files are not all from the same spacecraft'
sc = parse_stereo_name(spacecraft, ['A','B'])
;
;  Determine whether or not the files should be resized.
;
delvarx, outsize
if catalog[0].xsize ge 2048 then outsize=1024
;
;  Step through the polarization angles, and read in the files.
;
pols = all_vals(catalog.value)
for ipol = 0,n_elements(pols)-1 do begin
    pol = round(pols[ipol])
    pol_str='p'+string(pol,format='(I3.3)')
    wpol = where(catalog.value eq pol)
    files = catalog[wpol].filename
    secchi_prep, files, hdr, im, /calimg_off, /calfac_off, /bkgimg_off, $
      /update_hdr_off, outsize=outsize, /silent
    h = hdr[0]
;
;  If the original size was larger than 1024x1024, then rescale the images.
;
    if n_elements(outsize) eq 1 then begin
        ipsumdiff = 2 - hdr[0].ipsum
        if ipsumdiff ne 0 then im = im * 4.0^ipsumdiff
    endif
;
;  Form the minimum image.
;
    im = min(im, dim=3)
;
;  Calculate the average image date & time.
;
    mid_date = tai2utc( average( utc2tai(hdr.date_obs) ), /ccsds)
    mid_time = strmid(mid_date,11,12)
    date = anytim2cal(mid_date, /date, form=8)
;
;  Create the FITS header.
;
    fxhmake, outhdr, im, /date, /initialize
    obsname='STEREO_'+sc
    fxaddpar,outhdr,'OBSRVTRY',obsname
    fxaddpar,outhdr,'TELESCOP','STEREO'
    fxaddpar,outhdr,'INSTRUME','SECCHI'
    fxaddpar,outhdr,'DETECTOR','COR1'
    fxaddpar,outhdr,'DATE-OBS',mid_date,' = MID_DATE'
    fxaddpar,outhdr,'TIME-OBS',mid_time,' = MID_TIME'
    fxaddpar,outhdr,'DATE-AVG',mid_date,' = MID_DATE'
    fxaddpar,outhdr,'EXPTIME',1.0 ;Exposure time has been divided out
    fxaddpar,outhdr,'EXPCMD',h.expcmd
    fxaddpar,outhdr,'EXP0',average(hdr.exptime) ;Average exposure time
    fxaddpar,outhdr,'MID_DATE',mid_date
    fxaddpar,outhdr,'MID_TIME',mid_time
    fxaddpar,outhdr,'FILTER',h.FILTER
    fxaddpar,outhdr,'POLAR',h.POLAR
    fxaddpar,outhdr,'P1COL',h.P1COL
    fxaddpar,outhdr,'P1ROW',h.P1ROW
    fxaddpar,outhdr,'P2COL',h.P2COL
    fxaddpar,outhdr,'P2ROW',h.P2ROW
    fxaddpar,outhdr,'SUMROW',h.SUMROW
    fxaddpar,outhdr,'SUMCOL',h.SUMCOL
    fxaddpar,outhdr,'SUMMED',h.SUMMED
    fxaddpar,outhdr,'IPSUM',h.IPSUM
    fxaddpar,outhdr,'CCDSUM',h.ccdsum
    fxaddpar,outhdr,'R1COL',h.R1COL
    fxaddpar,outhdr,'R1ROW',h.R1ROW
    fxaddpar,outhdr,'R2COL',h.R2COL
    fxaddpar,outhdr,'R2ROW',h.R2ROW
    fxaddpar,outhdr,'CROTA',average(hdr.CROTA)
    fxaddpar,outhdr,'OFFSETCR',average(hdr.OFFSETCR)
    fxaddpar,outhdr,'N_IMAGES',n_elements(hdr)

    sdir1 = GETENV('SECCHI_BKG')+'/'+strlowcase(sc)+'/roll_min/'
    sdir2 = sdir1+strmid(date,0,6)
    if not file_exist(sdir2) THEN BEGIN
        PRINT,'Directory ',sdir2, ' does not exist -- creating.'
        mk_dir, sdir2
    ENDIF

    IF keyword_set(SAVEDIR) THEN sdir = savedir ELSE sdir = sdir2

    fname0 = 'rc1'+sc+'_'+pol_str+'_'+strmid(date,2,6)+'.fts'

    fxaddpar,outhdr,'FILENAME',fname0
    PRINT,'Writing roll minimum fits file: '+sdir2+'/'+fname0
    print,' using '+string(n_elements(hdr))+' images'
    fxaddpar,outhdr,'COMMENT','Created by COR1_MK_ROLL_MIN'
    fxaddpar,outhdr,'HISTORY',version
	
    WRITEFITS,concat_dir(sdir2,fname0),im,outhdr

endfor                          ;ipol
;
RETURN
END
