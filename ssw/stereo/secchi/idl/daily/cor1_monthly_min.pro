pro cor1_monthly_min, spacecraft, dt, pol, NDAYS=ndays, FILES=files, $
                      byhand=byhand, _extra=_extra
;
;+
; $Id: cor1_monthly_min.pro,v 1.24 2018/10/04 20:11:48 thompson Exp $
;
; Project     :	STEREO - SECCHI
;
; Name        :	COR1_MONTHLY_MIN
;
; Purpose     :	Generate monthly minimum from daily median images
;
; Category    :	STEREO SECCHI COR1 Calibration
;
; Explanation :	This procedure generates the minimum of the daily median
;               images for an interval (default is 27 days)
;
; Syntax      :	COR1_MONTHLY_MIN, SC, DT, POL
;
; Examples    :	COR1_MONTHLY_MIN, 'cor1', 'A', '20070521', '120'
;
; Inputs      :	sc = 'A', 'B'
;               dt = date to be processed
;               pol= '0', '120', '240', 'tbr'  (tbr = total brightness)
;
; Opt. Inputs :	None.
;
; Outputs     :	FITS file in $SECCHI_BKG/[ab]/monthly_min/YYYYMM/
;
; Opt. Outputs:	None.
;
; Keywords    :	FILES:   specify your own file list
;
; Calls       :	GET_DELIM, PARSE_STEREO_NAME, ANYTIM2UTC, STR2UTC, CONCAT_DIR,
;               FILE_EXIST, MREADFITS, FLAG_MISSING, AVERAGE, TAI2UTC, UTC2STR,
;               FXHMAKE, FXADDPAR, WRITEFITS, DATATYPE, MK_DIR,
;               COR1_COMBINE_MONTHLY_ROLL
;
; Common      :	None.
;
; Restrictions:	Need $SECCHI_BKG and appropriate permissions if not using
;               savedir option
;
; Side effects:	None.
;
; Prev. Hist. :	Based on SCC_MONTHLY_MIN by Karl Battams
;
; History     :	Version 1, 22-May-2007, William Thompson, GSFC
;               Version 2, 24-Oct-2007, WTT, include DATE-AVG in header
;               Version 3, 26-Aug-2007, WTT, call COR1_COMBINE_MONTHLY_ROLL
;               Version 4, 11-Feb-2009, WTT, use list of repoints
;               Version 5, 11-May-2009, WTT, always create 1024x1024, IPSUM=2
;               Version 6, 12-May-2009, WTT, revert to version 4
;                       Treat change to 512x512 as repoint
;               Version 7, 01-Feb-2010, WTT, include COR1-A event on 2010-01-27
;               Version 8, 01-Apr-2010, WTT, include COR1-B event on 2010-03-24
;               Version 9, 22-Nov-2010, WTT, include COR1-A event on 2010-11-19
;               Version 10, 14-Jan-2011, WTT, inc. COR1-A event on 2011-01-12
;               Version 11, 14-Feb-2011, WTT, inc. COR1-A event on 2011-02-11
;               Version 12, 10-Mar-2011, WTT, inc. COR1-A event on 2011-03-08
;               Version 13, 14-Mar-2011, WTT, inc. COR1-B event on 2011-03-11
;               Version 14, 18-May-2011, WTT, filter out 9-12 May 2011 on B
;               Version 15, 31-May-2011, WTT, fix typo in version 14
;               Version 16, 05-Mar-2012, WTT, inc. COR1-A events, 2011-12-05, 2012-02-19
;               Version 17, 26-Mar-2012, WTT, inc. COR1-A exp. time change 2012-03-16
;               Version 18, 16-Jul-2014, WTT, inc. S/C testing event
;               Version 19, 27-Aug-2014, WTT, inc. sidelobe start and 2014-08-23
;               Version 20, 01-Apr-2016, WTT, COR1-A exp. time change 2016-03-15
;               Version 21, 18-Apr-2016, WTT, COR1-A bkg improvement 2016-04-13
;                       Fix BEGUTC/ENDUTC bug when using FILES keyword
;               Version 22, 01-Oct-2018, WTT, added 2018-09-27 event
;
; Contact     :	WTHOMPSON
;               
;-
;

version = '$Id: cor1_monthly_min.pro,v 1.24 2018/10/04 20:11:48 thompson Exp $'
len = strlen(version)
version = strmid(version,1,len-2)

delim = get_delim()             ;Use in place of non-standard !DELIMITER
xaxis = 1024
yaxis = 1024
tel = 'cor1'

; Do some checks...

;  Parse the spacecraft name.

if (datatype(spacecraft) ne 'STR') or (n_elements(spacecraft) ne 1) then begin
    message = 'SC must be either "A" or "B".'
    goto, handle_error
endif
sc = parse_stereo_name(spacecraft, ['a','b'])
if (sc ne 'a') and (sc ne 'b') then begin
    message = 'Unrecognized spacecraft name ' + sc
    goto, handle_error
endif

; Set up a database of bad dates which should not be processed.

if sc eq 'a' then bad_mjd = 0 else bad_mjd = [55690, 55691, 55692, 55693]

;  If passed, check the NDAYS keyword.  The default is 27 days.

IF KEYWORD_SET(NDAYS)  THEN BEGIN
    IF ndays mod 2 EQ 0 THEN begin
        message = 'NDAYS must be odd.'
        goto, handle_error
    endif
    ndy = ndays 
ENDIF ELSE ndy=27

;  Convert DT into a proper FITS date string, and form the directory name based
;  on the date.

date = ''
IF (STRLEN(dt) EQ 6)  THEN BEGIN
   date = '20'+STRMID(dt,0,2) + '-' + STRMID(dt,2,2) + '-' + STRMID(dt,4,2)
   dir_date = '20' + STRMID(dt,0,4)
   out_date = dt
ENDIF ELSE IF (strlen(dt) eq 8) and valid_num(dt) THEN BEGIN
    date = STRMID(dt,0,4) + '-' + STRMID(dt,4,2) + '-' + STRMID(dt,6,2)
    dir_date = STRMID(dt,0,6)
    out_date = strmid(dt,2,6)
ENDIF
IF date EQ '' THEN BEGIN
    date = anytim2utc(dt, /ccsds, /date_only, _extra=_extra)
    cal_date = anytim2cal(date, form=8)
    dir_date = strmid(cal_date,0,6)
    out_date = strmid(cal_date,2,6)
ENDIF

; end checks

; Get some constants...

dir = GETENV('SECCHI_BKG') + delim + sc + delim + 'daily_med' + delim
outdir= GETENV('SECCHI_BKG') + delim + sc + delim + 'monthly_min' + delim + $
  dir_date + delim

if not file_exist(outdir) THEN mk_dir, outdir

if strlowcase(pol) EQ 'tbr' THEN polstr='pTBr' ELSE $
  polstr='p' + strcompress(string(pol), /remove_all)

tel_str = strmid(tel,0,1) + strmid(tel,strlen(tel)-1,1)
        ; EG hi1 becomes h1, cor2 becomes c2

srchstring_part1 = 'd' + strcompress(tel_str,/remove_all) + strupcase(sc) + $
  '_' + strcompress(polstr,/remove_all) + '_'   ; e.g. dhi1_pTBr_

; end constants

; Setup some initial values

mjd = STR2UTC (date +' 12:00:00')
mjd0 = mjd
mjd0.mjd = mjd.mjd - ndy/2 

; end intial value setup

; do the rest...

if datatype(files) EQ 'STR' THEN goto, jump_ahead

first = 1
tutc = mjd0
endutc = mjd0

;  Set limits based on the major spacecraft repointings, or other major events.

case strupcase(sc) of
    'A': begin
        repoint = ['2006-12-21T13:15', '2007-02-03T13:15']
        if strupcase(tel) eq 'COR1' then repoint = $
          [repoint, '2010-01-27T16:49', '2010-11-19T16:00', $
           '2011-01-12T12:23', '2011-02-11T04:23', '2011-03-08T17', $
	   '2011-12-05T12:03', '2012-02-19T02:33', '2012-03-16', $
           '2014-07-08', '2014-08-20', '2014-08-23T17', '2015-11-16', $
           '2016-03-15', '2016-04-13T01:08', '2016-05-25T05:40:05', $
           '2016-06-01T12', '2016-06-08', '2016-09-02T16:19', $
           '2018-09-27T16:44']
    endcase
    'B': begin
        repoint = ['2007-02-03T18:20', '2007-02-21T20:00']
        if strupcase(tel) eq 'COR1' then repoint = $
          [repoint, '2009-01-30T16:20', '2010-03-24T01:17', '2011-03-11']
    endcase
endcase
if strupcase(tel) eq 'COR1' then begin  ;Treat change to 512x512 as repoint
    repoint = [repoint, '2009-04-18T23:59:59']
    s = sort(repoint)
    repoint = repoint[s]
endif
repoint = anytim2utc(repoint)
tai_repoint = utc2tai(repoint)
taiin = utc2tai(date)
i1 = max(where(tai_repoint lt taiin, tcount))
if tcount gt 0 then mjdmin = repoint[i1].mjd else mjdmin = 0
i2 = min(where(tai_repoint gt taiin, tcount))
if tcount gt 0 then mjdmax = repoint[i2].mjd else mjdmax = 99999

;  Step through the days to find the daily median files over the period.

FOR i=0,ndy-1 DO BEGIN
    wbad = where(tutc.mjd eq bad_mjd, nbad)
    if (tutc.mjd gt mjdmin) and (tutc.mjd lt mjdmax) and (nbad eq 0) then begin
        day = UTC2STR (tutc,/ecs)
        monthdir = concat_dir(dir, strmid(day,0,4) + strmid(day,5,2))
        day = STRMID(day,2,2) + STRMID(day,5,2) + STRMID (day,8,2)
        name = concat_dir(monthdir, srchstring_part1 + day + '.fts')
        if file_exist(name) then begin
            IF first THEN BEGIN
                files = name
                first = 0
                begutc = tutc
            ENDIF ELSE BEGIN
                files = [files, name]
            ENDELSE
            endutc = tutc
        ENDIF
    endif
    tutc.mjd = tutc.mjd + 1
ENDFOR

if n_elements(begutc) eq 0 then begin
    message = 'No files found for date ' + dt
    goto, handle_error
endif
nrange = endutc.mjd - begutc.mjd + 1
if nrange lt (7<((ndy/2)-1)) then begin
    message = 'Not enough days to make model for ' + dt
    goto, handle_error
endif

if keyword_set(byhand) then stop

; +++++++++++++++++++++ END OF MAIN FILE CHECKING SECTION +++++++++++++++++++++

jump_ahead:

first=1
hdrset = 0

;  Read in the files found above.

nfiles = n_elements(files)
mreadfits, files, h, img
if n_elements(begutc) eq 0 then begin
    begutc = str2utc(min(h.date_obs, max=endutc))
    endutc = str2utc(endutc)
endif

;  Take out any evolution in median brightness over the period.

med = fltarr(nfiles)
for i=0,nfiles-1 do med[i] = median(img[*,*,i])
med = med / average(med)
for i=0,nfiles-1 do img[*,*,i] = img[*,*,i] / med[i]

;  Calculate the difference of each image compared with its neighbors.

avg = img
for i=1,nfiles-2 do avg[*,*,i] = (img[*,*,i-1] + img[*,*,i+1]) / 2.
avg[*,*,0] = img[*,*,1]
avg[*,*,nfiles-1] = img[*,*,nfiles-2]
diff = img - avg

;  Break the images up into intensity regimes, and find points where the
;  difference is far outside the normal for that intensity regime.

sz = size(img)
tmp = fltarr(sz[1],sz[2],sz[3])
amin = min(img, max=amax, /nan)
nregime = 100
delta = (amax - amin) / nregime
for iregime = 0,nregime-1 do begin
    rmin = amin + delta * iregime
    rmax = rmin + delta
    wregime = where((img ge rmin) and (img le rmax), count)
    if count gt 1 then begin
        dregime = diff[wregime]
        dsig = stddev(dregime)
        w = where(abs(dregime) gt (3*dsig), count)
        if count gt 0 then tmp[wregime[w]] = 1
    endif
endfor

;  Filter out any images where an unusual number of the pixels have been
;  flagged.  Find the minimum of all the images.

ftest = average(tmp, [1,2])
wtest = where(ftest le 3*median(ftest), ctest)
img = min(img[*,*,wtest], dimension=3, /nan)

;  Calculate the mean rotation angle.

PRINT, 'Minimum CROTA value:', strcompress(string(min(h.crota)))
PRINT, 'Maximum CROTA value:', strcompress(string(max(h.crota)))

meancrota=strcompress(string(mean(h.crota)),/remove_all)
PRINT,'Average CROTA value:',meancrota

;  Calculate the mean observation date.

tai = utc2tai(h.date_d$obs)
tmin = min(tai, max=tmax)
tai = average(tai)
date_obs = tai2utc(tai, /ccsds)
time_obs = strmid(date_obs, 11, strlen(date_obs)-11)
date_mid = tai2utc((tmin+tmax)/2.d0, /ccsds)
time_mid = strmid(date_mid, 11, strlen(date_obs)-11)
print, 'Begin date:   ' + utc2str(begutc, /date_only)
print, 'End date:     ' + utc2str(endutc, /date_only)
print, 'Average date: ' + date_obs

;  Create the FITS header.

sz = SIZE (img)
IF (sz(0) NE 0) THEN BEGIN
    shdr = h[0]
    mname = 'm'+strcompress(STRMID(srchstring_part1,1,9),/remove_all) + $
      out_date + '.fts'

;  Get those parameters which weren't originally in the background headers.

    if tag_exist(shdr,'summed') then begin
        print, 'Using header values'
        summed = shdr.summed
        ipsum  = shdr.ipsum
        ccdsum = shdr.ccdsum
        offsetcr = average(h.offsetcr)
    end else begin
        print, 'Using assumed values'
        summed = 2
        ipsum  = 2
        ccdsum = 1
        offsetcr = 2680.
    endelse

    fxhmake, hdr, img, /INIT, /DATE
    FXADDPAR,hdr,'FILENAME',mname
    FXADDPAR,hdr,'TELESCOP',shdr.TELESCOP
    FXADDPAR,hdr,'OBSRVTRY',shdr.OBSRVTRY
    FXADDPAR,hdr,'INSTRUME',shdr.INSTRUME
    FXADDPAR,hdr,'DETECTOR',shdr.DETECTOR
    FXADDPAR,hdr,'FILTER',shdr.FILTER
    FXADDPAR,hdr,'POLAR',shdr.POLAR
    FXADDPAR,hdr,'P1COL',shdr.P1COL	
    FXADDPAR,hdr,'P1ROW',shdr.P1ROW
    FXADDPAR,hdr,'P2COL',shdr.P2COL
    FXADDPAR,hdr,'P2ROW',shdr.P2ROW
    FXADDPAR,hdr,'SUMROW',shdr.SUMROW
    FXADDPAR,hdr,'SUMCOL',shdr.SUMCOL
    FXADDPAR,hdr,'SUMMED',summed
    FXADDPAR,hdr,'IPSUM',ipsum
    FXADDPAR,hdr,'CCDSUM',ccdsum
    FXADDPAR,hdr,'OFFSETCR',offsetcr
    FXADDPAR,hdr,'LEBXSUM',1
    FXADDPAR,hdr,'LEBYSUM',1
    FXADDPAR,hdr,'DATE-OBS',date_obs
    FXADDPAR,hdr,'TIME-OBS',time_obs
    FXADDPAR,hdr,'DATE-AVG',date_obs
    FXADDPAR,hdr,'EXPTIME',average(h.exptime)
    FXADDPAR,hdr,'EXPCMD',average(h.expcmd)
    FXADDPAR,hdr,'EXP0',average(h.exp0)
    FXADDPAR,hdr,'MID_DATE',date_mid
    FXADDPAR,hdr,'MID_TIME',time_mid
    FXADDPAR,hdr,'R1COL',shdr.R1COL
    FXADDPAR,hdr,'R1ROW',shdr.R1ROW
    FXADDPAR,hdr,'R1COL',shdr.R1COL
    FXADDPAR,hdr,'R1ROW',shdr.R1ROW
    FXADDPAR,hdr,'CROTA',float(meancrota),$
      ' Degrees, average for DATE-OBS, computed from ' + ntrim(nfiles) + ' days.'
    FXADDPAR,hdr,'N_DAYS',nfiles,$
      ' Daily median images used to compute this image'
    FXADDPAR,hdr,'FIRSTDAY',utc2str(begutc, /date_only)
    FXADDPAR,hdr,'LAST_DAY',utc2str(endutc, /date_only)
    FXADDPAR,hdr,'HISTORY',version
    PRINT, 'Writing monthly fits file: ', outdir, mname, ' using', nfiles, $
      ' out of', ndy, ' days.'

;  Write the output file.

    filename = concat_dir(outdir, mname)
    WRITEFITS, filename, img, hdr
    cor1_combine_monthly_roll, filename

ENDIF
;
RETURN
;
;  Error handling point.
;
handle_error:
message, message, /continue
return
END
