pro sccarchivereport, stadate, enddate, USE_DIR=use_dir, _EXTRA=_extra
;
; $Id: sccarchivereport.pro,v 1.7 2012/03/27 16:53:32 nathan Exp $
;
; Purpose:  Generate a report of current and future SECCHI archive volume
; 
; Inputs: 
;  
; Outputs: $NRL_LIB/secchi/data/statistics/nrlarchivestatus.txt
;
; Keywords: 
;   USE_DIR=	Use this directory to find data files
;
;   Following are passed to getsccarchiveinfo:
;			/NOPLOT	  Do not open plot window
;			/OVERPLOT	Plot on top of previous plot instead of redrawing
;			STA_DATE=date to start stats from
;   	    	    	END_DATE=last date for stats
; 
; $Log: sccarchivereport.pro,v $
; Revision 1.7  2012/03/27 16:53:32  nathan
; print start date and float totals
;
; Revision 1.6  2011/04/27 17:42:50  nathan
; call getsccarchiveinfo with sta_date and end_date
;
; Revision 1.5  2010/12/09 21:47:42  nathan
; change units to GB and add num files
;
; Revision 1.4  2007/05/22 18:57:43  nathan
; add _EXTRA
;
; Revision 1.3  2007/05/18 17:50:03  nathan
; add /NOPLOT
;
; Revision 1.2  2007/05/16 16:14:59  nathan
; change outputdir
;
; Revision 1.1  2007/05/14 17:30:06  nathan
; *** empty log message ***
;
;

IF keyword_set(USE_DIR) THEN outdir=use_dir ELSE outdir=getenv('NRL_LIB')+'/secchi/data/statistics'
cd,outdir
spawn,'pwd'

pause=2
wait,pause

outpf='nrlarchivestatus.txt'

;--
; Ahead
;--
dir=' a/img/euvi'
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/img,/volume,tele='euvi', USE_DIR=use_dir

;--Init report output--
curdate=utc2str(curutc,/date)
dp1utc=curutc
dp1utc.mjd=curutc.mjd+30
dp1mon=utc2str(dp1utc,/date)
dp6utc=curutc
dp6utc.mjd=curutc.mjd+183
dp6mon=utc2str(dp6utc,/date)
openw,lun,outpf,/get_lun
;             Directory   yyyy/mm/dd  yyyy/mm/dd  yyyy/mm/dd
;            a/img/euvi       123456      123456      123456 
printf,lun,'SECCHI LZ Archive Volume Report from ',stadate 
printf,lun,'+------------------------------------------------+'
printf,lun,'|                Current                         |'
printf,lun,'| Directory   '+curdate+'  '+dp1mon+'  '+dp6mon+' |'
printf,lun,'+------------------------------------------------+'
printf,lun,'              ------- Volume of files (GB) ------'
;--finish init

dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir+' ' +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=curvol & vol1=dp1vol & vol6=dp6vol

;--get each line
dir=' a/img/cor1 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/img,/volume,tele='cor1' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' a/seq/cor1 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/seq,/volume,tele='cor1' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' a/img/cor2 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/img,/volume,tele='cor2' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' a/seq/cor2 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/seq,/volume,tele='cor2' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' a/img/hi_1 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/img,/volume,tele='hi1' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' a/img/hi_2 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/img,/volume,tele='hi2' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
; Behind
;--
dir=' b/img/euvi '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/img,/volume,tele='euvi' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' b/img/cor1 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/img,/volume,tele='cor1' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' b/seq/cor1 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/seq,/volume,tele='cor1' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' b/img/cor2 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/img,/volume,tele='cor2' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' b/seq/cor2 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/seq,/volume,tele='cor2' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' b/img/hi_1 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/img,/volume,tele='hi1' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
;--
dir=' b/img/hi_2 '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/img,/volume,tele='hi2' 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol/1024.,'(F12.2)') +string(dp1vol/1024.,'(F12.2)') +string(dp6vol/1024.,'(F12.2)')
vol0=vol0+curvol & vol1=vol1+dp1vol & vol6=vol6+dp6vol
dir=' TOTAL      '
printf,lun,dir +string(vol0/1024.,'(I12)') +string(vol1/1024.,'(I12)') +string(vol6/1024.,'(I12)')
;
;--Get sci file info
;
printf,lun,''
printf,lun,'              ------- Volume of files (GB) ------'
scivol=0
scivol1=0
scivol6=0
;--
dir='a/sci EUVI  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/sci,/volume,tele='euvi'
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='a/sci COR1  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/sci,/volume,tele='cor1' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='a/sci COR2  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/sci,/volume,tele='cor2' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='a/sci HI_1  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/sci,/volume,tele='hi1' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='a/sci HI_2  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/sci,/volume,tele='hi2' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')

dir='a/sci ALL   '
printf,lun,dir +string(scivol/1024.,'(I12)') +string(scivol1/1024.,'(I12)') +string(scivol6/1024.,'(I12)')
printf,lun,''

scivol=0
scivol1=0
scivol6=0
;--
dir='b/sci EUVI  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/sci,/volume,tele='euvi' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='b/sci COR1  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/sci,/volume,tele='cor1' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='b/sci COR2  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/sci,/volume,tele='cor2' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='b/sci HI_1  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/sci,/volume,tele='hi1' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')
;--
dir='b/sci HI_2  '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/sci,/volume,tele='hi2' 
scivol=scivol+curvol
dp1vol=bm[0]+bm[1]*dp1utc.mjd
scivol1=scivol1+dp1vol
dp6vol=bm[0]+bm[1]*dp6utc.mjd
scivol6=scivol6+dp6vol
printf,lun,dir +string(curvol/1024.,'(F12.1)') +string(dp1vol/1024.,'(F12.1)') +string(dp6vol/1024.,'(F12.1)')

dir='b/sci ALL   '
printf,lun,dir +string(scivol/1024.,'(I12)') +string(scivol1/1024.,'(I12)') +string(scivol6/1024.,'(I12)')
printf,lun,''

printf,lun,''
printf,lun,'              ------- Number of images (not SPWX) ------'
scivol=0
scivol1=0
scivol6=0
;--
tel='EUVI'
dir=' '+tel+'-A     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='COR1'
dir=' '+tel+'-A     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='COR2'
dir=' '+tel+'-A     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='HI_1'
dir=' '+tel+'-A     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;-- 
tel='HI_2'
dir=' '+tel+'-A     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/ahead,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
;--
tel='EUVI'
dir=' '+tel+'-B     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='COR1'
dir=' '+tel+'-B     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='COR2'
dir=' '+tel+'-B     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='HI_1'
dir=' '+tel+'-B     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol
;--
tel='HI_2'
dir=' '+tel+'-B     '
getsccarchiveinfo,curvol,curutc,bm, STA_DATE=stadate, END_DATE=enddate,_EXTRA=_extra,/behind,/fits,/number,tele=tel,/SSR1, /SSR2, /SSRT 
dp1vol=bm[0]+bm[1]*dp1utc.mjd
dp6vol=bm[0]+bm[1]*dp6utc.mjd
printf,lun,dir +string(curvol,'(I12)') +string(dp1vol,'(I12)') +string(dp6vol,'(I12)')
scivol=scivol+curvol & scivol1=scivol1+dp1vol & scivol6=scivol6+dp6vol

dir=' TOTAL      '
printf,lun,dir +string(scivol,'(I12)') +string(scivol1,'(I12)') +string(scivol6,'(I12)')


close,lun
free_lun, lun
end
