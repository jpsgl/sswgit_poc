pro scc_do_all_daily_pretties,dtst,dtend,tel, _EXTRA=_extra
;+
; $Id: scc_do_all_daily_pretties.pro,v 1.6 2012/08/29 15:30:51 secchib Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_do_all_daily_pretties
;               
; Purpose   : to rerun scc_daily_pretties for given days and telescopes
;               
; Explanation: 
;               
; Use       : IDL> scc_do_all_daily_pretties,'YYYYMMDD','YYYYMMDD','CAM'
;    
; Inputs    :	dtst= starting day yyyy-mm-dd (or yyyymmdd)
;		dtend= ending day yyyy-mm-dd 
;		tel=[euvi, cor2, hi1, hi2] or 'all'
;               
; Outputs   : from scc_daily_pretties JPG24 and PNG files in /net/earth/data3/secchi/images
;
; Keywords  : _EXTRA  see sc_daily_Pretties.pro for available keywords
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to disk, environment variable sc must be defined.
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2008
;               
; $Log: scc_do_all_daily_pretties.pro,v $
; Revision 1.6  2012/08/29 15:30:51  secchib
; nr - input format clarification
;
; Revision 1.5  2012/08/27 19:18:29  secchib
; nr - allow 2 params
;
; Revision 1.4  2011/08/11 18:40:35  secchia
; do /histars automatically
;
; Revision 1.3  2011/08/08 19:04:23  nathan
; always call /planets and use nrlanytim2utc
;
; Revision 1.2  2008/03/31 12:49:04  mcnutt
; commented and change order of do loops to day then tels
;

ut0=nrlanytim2utc(dtst)
IF n_params() LT 3 THEN BEGIN
    ut1=ut0
    tel=dtend
ENDIF ELSE $
ut1=nrlanytim2utc(dtend)

mjd=ut0
ab=getenv('sc');aorb ;['a','b']
if (tel(0) eq 'all') then tel=['cor1','cor2','hi1','hi2','euvi']
FOR i=ut0.mjd,ut1.mjd DO BEGIN
FOR j=0,n_Elements(tel)-1 DO BEGIN
    mjd.mjd=i
    yyyymmdd=utc2yymmdd(mjd,/yyyy)
    scc_daily_pretties,yyyymmdd,tel(j),ab,/planets,_EXTRA=_extra
    if strpos(strlowcase(tel(j)),'hi') ge 0 then scc_daily_pretties,yyyymmdd,tel(j),ab,/planets,/histars,_EXTRA=_extra

ENDFOR
ENDFOR

end
