pro sccimagestats, lastday, FITS_ONLY=fits_only, SCI_ONLY=sci_only, AHEAD=ahead, BEHIND=behind, $
    DIRECTORY=directory, START_DATE=start_date, fullrpt=fullrpt, USESCIDIR=usescidir, LASCO=lasco
;
; $Id: sccimagestats.pro,v 1.20 2013/07/15 19:22:32 nathan Exp $
;
; Project:  STEREO SECCHI
;
; Purpose:  Update volume statistics data files for SECCHI images for each ApID.
;   	    
; Optional Inputs:
;		lastday		Last day to do stats for, string or UTC structure
;
; Keywords:
;   /AHEAD, /BEHIND, /FITS_ONLY, /SCI_ONLY  Limit scope; default is to do all
;   DIRECTORY=path  Use path as working dir instead of $NRL_LIB/secchi/data/statistics
;   START_DATE=date Use date (input to anytim2utc) as start date of report, otherwise uses 
;   	    	last day in DIRECTORY
;   /USESCIDIR	Use $scia or $scib instead of SCC_ATTIC
;
; Restrictions: requires bin/gettotals executable
;
; Author: N.Rich, NRL/I2, April 2007
;
; $Log: sccimagestats.pro,v $
; Revision 1.20  2013/07/15 19:22:32  nathan
; fix c1 tB ata; explicitlyy indicate teldir
;
; Revision 1.19  2011/09/30 21:13:08  nathan
; skip realtime search because hanging on ssdfs0
;
; Revision 1.18  2010/12/09 21:49:07  nathan
; different behavior if START_DATE set
;
; Revision 1.17  2009/01/26 21:16:40  nathan
; added /LASCO for doing LASCO/EIT numbers
;
; Revision 1.16  2008/10/20 19:00:51  mcnutt
; added keyword fullrpt to create new report files and print all data lines to report even if nzv lt 6
;
; Revision 1.15  2008/10/17 16:24:36  mcnutt
; corrected c2seq directory variable to typ
;
; Revision 1.14  2008/09/15 20:23:33  secchib
; nr - use SCC_ATTIC not scix for atticdir
;
; Revision 1.13  2008/03/24 22:19:31  nathan
; added DIRECTORY=, START_DATE=; change s?c2? to ??c2?
;
; Revision 1.12  2007/09/12 22:22:23  nathan
; fix problem for img files
;
; Revision 1.11  2007/08/01 22:37:00  nathan
; redo last row in file instead of just appending
;
; Revision 1.10  2007/07/12 18:09:30  nathan
; print nzv eq 0 case
;
; Revision 1.9  2007/07/11 20:39:13  nathan
; update nzv criteria
;
; Revision 1.8  2007/06/22 17:55:31  mcnutt
; updated sci directory
;
; Revision 1.7  2007/06/08 18:28:16  secchib
; cd to orig dir at end
;
; Revision 1.6  2007/06/06 15:08:19  nathan
; add lastday argument
;
; Revision 1.5  2007/05/17 18:44:59  nathan
; specify path for gettotals
;
; Revision 1.4  2007/05/16 16:14:59  nathan
; change outputdir
;
; Revision 1.3  2007/05/14 17:29:53  nathan
; omit zero lines
;
; Revision 1.2  2007/05/10 22:29:47  nathan
; output to $stereo/dps/volume
;

pause=2

totcmd=getenv('HOME')+'/bin/gettotals'
tel=['cor1','cor2','euvi','hi_1','hi_2']
src=['lz','pb','rt']
sc=['a','b']
sdr=['img','seq','cal']
atticdir=getenv('SCC_ATTIC')
dt0='2006-11-01'


;   	    SSRT   SSR1   SSR2   SPWX

; suffixes for raw image files
IF keyword_set(LASCO) THEN BEGIN
    message,'Processing data for LASCO/EIT',/info
    wait,2
    aorb='EL'
    ab='e'
    usescidir=0
    FITS_ONLY=1
    atticdir=''
    sfxa=['']   

    ; subdirs and prefix for FITS filenames

    atsa=['/c1/1','/c2/2','/c3/3','/c4/4']  ; 
    
    nutifita='N FITSfile,  C1 ,  C2 ,  C3 , EIT '  
             ;yyyy-mm-dd,12345,12345,12345  
    sitifita='MB (FITS) ,     C1 ,     C2 ,     C3 ,    EIT ,'
    nutiscia=''
    sitiscia=''
    dt0='1995-12-08'
ENDIF ELSE $
IF keyword_set(AHEAD) THEN BEGIN
    message,'Processing data for SECCHI-A...',/info
    wait,2
    aorb='A'
    ab='a'
    IF keyword_set(USESCIDIR) THEN atticdir=getenv('scia')
    sfxa=['302', '402', '502', '702', $ ; COR1-A
	  '311', '411', '511', '711', $ ; COR2-A
	  '343', '443', '543', '743', $ ; EUVI-A
	  '325', '425', '525', '725', $ ; HI1-A
	  '334', '434', '534', '734']   ; HI2-A

    ; equivalent ATTS for FITS filenames

    atsa=['s3c1A','s4c1A','s5c1A','s7c1A', $ 
	  'n3c1A','n4c1A','n5c1A','n7c1A', $
	  '?3c2A','?4c2A','?5c2A','?7c2A', $	; subs 8-11 (seq) can be s or n
	  'd3c2A','d4c2A','d5c2A','d7c2A', $
	  '3euA','4euA','5euA','7euA', $
	  '3h1A','4h1A','5h1A','7h1A', $
	  '3h2A','4h2A','5h2A','7h2A']

ENDIF ELSE BEGIN
    message,'Processing data for SECCHI-B...',/info
    wait,2
    aorb='B'
    ab='b'
    IF keyword_set(USESCIDIR) THEN atticdir=getenv('scib')
    sfxa=['307', '407', '507', '707', $ ; COR1-B
	  '316', '416', '516', '716', $ ; COR2-B
	  '348', '448', '548', '748', $ ; EUVI-B
	  '320', '420', '520', '720', $ ; HI1-B
	  '339', '439', '539', '739']   ; HI2-B

    atsa=['s3c1B','s4c1B','s5c1B','s7c1B', $ 
	  'n3c1B','n4c1B','n5c1B','n7c1B', $
	  '?3c2B','?4c2B','?5c2B','?7c2B', $
	  'd3c2B','d4c2B','d5c2B','d7c2B', $
	  '3euB','4euB','5euB','7euB', $
	  '3h1B','4h1B','5h1B','7h1B', $
	  '3h2B','4h2B','5h2B','7h2B'] 
ENDELSE

teldir=['cor1','cor1','cor1','cor1', $
    	'cor1','cor1','cor1','cor1', $
	'cor2','cor2','cor2','cor2', $
	'cor2','cor2','cor2','cor2', $
	'euvi','euvi','euvi','euvi', $
	'hi_1','hi_1','hi_1','hi_1', $
	'hi_2','hi_2','hi_2','hi_2']
; Write 2 files for each of above blocks
; one for number of files and one for total size of files in MB
; subscript of N corresponds to mission day
; Mission day zero is 2006-10-25 = 2006-298
volfalu=5
volsalu=6
nffalu=1
nfsalu=2
ntsa=n_elements(atsa)

IF keyword_set(DIRECTORY) THEN outdir=directory ELSE $
    outdir=getenv('NRL_LIB')+'/secchi/data/statistics'
cd,outdir,cur=curdir
spawn,'pwd'
; --Get last date in list; will redo that date
;   Re-write files up to day before last date
IF ~keyword_set(SCI_ONLY) THEN BEGIN
    ;--Check to see if file is started
    IF file_exist('scc'+aorb+'fitnumbers.txt') and ~keyword_set(fullrpt) THEN app1=1 ELSE app1=0
    IF file_exist('scc'+aorb+'fitsizes.txt') and ~keyword_set(fullrpt) THEN app5=1 ELSE app5=0
    lastfitdate=''
    IF (app5) and ~keyword_set(START_DATE) THEN BEGIN
	rows=readlist('scc'+aorb+'fitsizes.txt')
	nr=n_elements(rows)
	lastrow=rows[nr-1]
	lastfitdate=strmid(lastrow,0,10)
	openw,1,'scc'+aorb+'fitsizes.txt'
	for i=0,nr-2 do printf,1,rows[i]
	close,1
    ENDIF
    IF (app1) and ~keyword_set(START_DATE) THEN BEGIN
	rows=readlist('scc'+aorb+'fitnumbers.txt')
	nr=n_elements(rows)
	openw,5,'scc'+aorb+'fitnumbers.txt'
	for i=0,nr-2 do printf,5,rows[i]
	close,5
    ENDIF
    help,lastfitdate
    wait,2
    ;--Open for writing
    openw,1,'scc'+aorb+'fitnumbers.txt', append=app1
    openw,5,'scc'+aorb+'fitsizes.txt', append=app5
ENDIF
IF ~keyword_set(FITS_ONLY) THEN BEGIN
    IF file_exist('scc'+aorb+'imgnumbers.txt') and ~keyword_set(fullrpt) THEN app2=1 ELSE app2=0
    IF file_exist('scc'+aorb+'imgsizes.txt') and ~keyword_set(fullrpt) THEN app6=1 ELSE app6=0
    lastscidate=''
    IF (app6) and ~keyword_set(START_DATE) THEN BEGIN
    	rows=readlist('scc'+aorb+'imgsizes.txt')
	nr=n_elements(rows)
	lastrow=rows[nr-1]
	lastscidate=strmid(lastrow,0,10)
	openw,6,'scc'+aorb+'imgsizes.txt'
	for i=0,nr-2 do printf,6,rows[i]
	close,6
    ENDIF
    IF (app2) and ~keyword_set(START_DATE) THEN BEGIN
	rows=readlist('scc'+aorb+'imgnumbers.txt')
	nr=n_elements(rows)
	openw,2,'scc'+aorb+'imgnumbers.txt'
	for i=0,nr-2 do printf,2,rows[i]
	close,2
    ENDIF
    help,lastscidate
    wait,2
    openw,2,'scc'+aorb+'imgnumbers.txt',append=app2
    openw,6,'scc'+aorb+'imgsizes.txt',append=app6
ENDIF

nufrmt="(',',I5)"
sifrmt="(',',F8.2)"     ;MB
 fmt=               "(a25,                ' | ',a19,     ' | ',a4,' |',f6.1,'| ',i4,' | ',i4,' | ',a5,' | ',a5,' | ',a4,' |',i5,' | ',a4,' | ',a3)"

IF ~keyword_set(LASCO) THEN BEGIN
    ; generate headings
    nutifita='N FITSfile'
             ;yyyy-mm-dd,12345,12345,12345  
    for i= 0,15 do nutifita=nutifita+',' 	+atsa[i]
    for i=16,27 do nutifita=nutifita+', '	+atsa[i]
    sitifita='MB (FITS) '
    for i= 0,15 do sitifita=sitifita+',   ' +atsa[i]
    for i=16,27 do sitifita=sitifita+',    '+atsa[i]


    nutiscia='N SCI file'
    for i=0,19 do nutiscia=nutiscia+',  '+sfxa[i]
    print,nutiscia
    sitiscia='MB (SCI)  '
    for i=0,19 do sitiscia=sitiscia+',     '+sfxa[i]
    print,sitiscia
ENDIF
print,nutifita
print,sitifita

titles=[nutifita, sitifita, nutiscia, sitiscia]
		
get_utc,utcn
IF n_params() EQ 1 THEN BEGIN
	IF datatype(lastday) NE 'STC' THEN utc1=str2utc(lastday) ELSE utc1=lastday
	mjd1=utc1.mjd
ENDIF ELSE mjd1=utcn.mjd-4

IF ~keyword_set(SCI_ONLY) THEN BEGIN
    IF ~app5 THEN printf,volfalu,sitifita
    IF ~app1 THEN printf,nffalu,nutifita
    IF lastfitdate NE '' THEN dt0=lastfitdate
    IF keyword_set(START_DATE) THEN dt0=utc2str(anytim2utc(start_date))
    ; redo last date
    utc0=str2utc(dt0)
    utci=utc0
    IF keyword_set(LASCO) THEN fitsdir= '$LZ_IMG/level_05' ELSE $
    fitsdir='$secchi/lz/L0/'+ab
    cd,fitsdir
    for mjdi=utc0.mjd,mjd1 do begin
	    utci.mjd=mjdi
	    yyyymmdd=utc2yymmdd(utci,/yyyy)
	    yymmdd=strmid(yyyymmdd,2,6)
	    ;-- one line per date per file for 8 files
	    volfali=utc2str(utci,/date_only,/ecs)
	    nffali=utc2str(utci,/date_only,/ecs)
	    nzv=0

	    for i=0,ntsa-1 do begin

		    vol=0.
    	    	    nf=0
		    ata=atsa[i]
    	    	    typ='*'
		    IF strmid(ata,0,1) NE '3' and strmid(ata,1,1) NE '3' THEN BEGIN
		    	
			sss=where([8,9,10,11] EQ i,c2seq)
			IF c2seq then typ='seq'
			IF keyword_set(LASCO) THEN arg=yymmdd+ata+'*.fts' ELSE $
			arg=typ+'/'+teldir[i]+'/'+yyyymmdd+'/*'+ata+'.fts' 
			print,fitsdir+'/'+arg
			spawn,totcmd+' '+arg,/sh,result
			print,yyyymmdd+' '+ata+' '+result
			parts=strsplit(result,/extract, count=nparts)
			IF parts[0] EQ '/bin/ls:' THEN stop ELSE $
			if nparts EQ 6 then begin
				vol=double(parts[1])
				nf =fix(parts[4])
			endif 
		    ENDIF
		    IF vol GT 0 THEN nzv=nzv+1
		    volfali=volfali+string(vol/(1024.^2),format=sifrmt)
		    nffali =nffali+string(nf, format=nufrmt)
	    endfor
	;--print lines for 8 files
	print,'waiting ',pause
	help,nzv
	wait,pause

	IF nzv GE 6 or nzv EQ 0 or keyword_set(fullrpt) or keyword_set(LASCO) THEN BEGIN
    	    printf,volfalu,volfali
	    printf,nffalu,nffali
    	ENDIF
    endfor
    close,volfalu
    close,nffalu
ENDIF
;---------------------------------------------
IF ~keyword_set(FITS_ONLY) THEN BEGIN
    IF ~app6 THEN printf,volsalu,sitiscia
    IF ~app2 THEN printf,nfsalu,nutiscia
    dt0='2006-11-01'
    IF lastscidate NE '' THEN dt0=lastscidate
    IF keyword_set(START_DATE) THEN dt0=utc2str(anytim2utc(start_date))
    utc0=str2utc(dt0)
    utci=utc0
    cd,atticdir
    for mjdi=utc0.mjd,mjd1 do begin
	utci.mjd=mjdi
	yymmdd=utc2yymmdd(utci)
	IF mjdi LT 54709 and aorb EQ 'B' THEN yymmdd='older/'+yymmdd
	yyyymmdd='20'+yymmdd
	;-- one line per date per file for 8 files
	volsali=utc2str(utci,/date_only,/ecs)
	nfsali=utc2str(utci,/date_only,/ecs)
	nzv=0
    	for i=0,19 do begin
		vol=0.
		nf=0
		sfa=sfxa[i]
		IF strmid(sfa,0,1) NE '3' THEN BEGIN
		    arg=yymmdd+'/*.'+sfa
		    print,atticdir+'/'+arg
		    spawn,totcmd+' '+arg,/sh,result
		    print,yyyymmdd+' '+sfa+' '+result
		    parts=strsplit(result,/extract, count=nparts)
		    IF parts[0] EQ '/bin/ls:' THEN stop ELSE $
		    if nparts EQ 6 then begin
			    vol=double(parts[1])
			    nf =fix(parts[4])
		    endif 
		ENDIF
		IF vol GT 0 THEN nzv=nzv+1
		volsali=volsali+string(vol/(1024.^2),format=sifrmt)
		nfsali =nfsali+string(nf, format=nufrmt)
	endfor
	print,'waiting ',pause
	help,nzv
	wait,pause
	IF nzv GE 6 or nzv EQ 0 or keyword_set(fullrpt) THEN BEGIN
	    printf,volsalu,volsali
	    printf,nfsalu,nfsali
	ENDIF
    endfor

    close,volsalu
    close,nfsalu
ENDIF

cd,curdir
end
