;$Id: sccbatches.pro,v 1.2 2007/05/16 16:18:39 nathan Exp $
;
; Script to be run from crontab (currently secchia@calliope)
;
; $Log: sccbatches.pro,v $
; Revision 1.2  2007/05/16 16:18:39  nathan
; add imagestats and report
;
scc_gtdbase_update, /nrl
print,'PATH:'
print,getenv('PATH')
print,'ITOS_GROUPDIR:'
print,getenv('ITOS_GROUPDIR')
;sccimagestats, /ahead
;sccimagestats, /behind
; done in secchi_reduce after euvi

end
