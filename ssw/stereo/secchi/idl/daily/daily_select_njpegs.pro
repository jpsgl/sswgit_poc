pro daily_select_njpegs,sc0,yyyymmdd,tel0,nfiles,nomail=nomail,domissing=domissing,minus30=minus30
; $Id: daily_select_njpegs.pro,v 1.15 2011/06/29 19:30:22 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : daily_select_njpegs
;               
; Purpose   : calls select_njpegs and writes a text file contain jpg file names
;               
; Explanation: 
;               
; Use       : IDL> daily_select_njpegs,sc0,yyyymmdd,tel0,nfiles
;    
; Inputs    :sc0 = 'A', 'B'
;            yyyymmdd = select images from this day
;            tel0 = euvi, cor2, hi1, hi2
;
; Optional Inputs:
;            nfiles = number of evenly space files 
;              
; Outputs   : creates/appends a text file with output files from select_njpegs
;
; Keywords  : 
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt, NRL/I2, Jan 2008
;               
; $Log: daily_select_njpegs.pro,v $
; Revision 1.15  2011/06/29 19:30:22  nathan
; changed maillist to secchipipeline@cronus
;
; Revision 1.14  2008/05/20 14:40:01  secchia
; added cor1 settings
;
; Revision 1.13  2008/04/28 13:35:48  mcnutt
; added keyword minus30 to repeat movie list for COR2 and HIs d-30
;
; Revision 1.12  2008/03/19 11:25:28  secchib
; replaced findfile with file_search
;
; Revision 1.11  2008/03/13 18:57:31  mcnutt
; no longer change input sc or tel
;
; Revision 1.10  2008/03/12 13:14:57  mcnutt
; coorected report for othersc to use osc not sc
;
; Revision 1.9  2008/02/27 17:40:55  secchia
; removed append when opening list files
;
; Revision 1.8  2008/02/25 18:52:50  mcnutt
; change euvi image check to only check once
;
; Revision 1.7  2008/02/25 18:36:05  mcnutt
; added keyword to create missing daily pretty files and email when they are missing
;
; Revision 1.6  2008/02/14 20:38:29  secchia
; nr - added  /append to openw calls
;
; Revision 1.5  2008/01/23 18:30:24  secchia
; writes outfiles to mpegframe files
;
; Revision 1.4  2008/01/22 21:47:20  secchia
; nr - read in outfile and sort to rid of duplicates
;
; Revision 1.3  2008/01/16 21:07:33  nathan
; do not open output file if no lines to write
;
; Revision 1.2  2008/01/16 20:46:21  nathan
; renamed output directory/filename; wls is type string
;
; Revision 1.1  2008/01/16 18:12:38  mcnutt
; created for pipeline call to select_njpegs.pro
;

;get_lun,njp

date=yyyymmdd

njp=101
outdir=concat_dir(getenv('SCC_LOG'),'mpegframes')
spawn,'mkdir '+outdir,/sh
if(datatype(nfiles) eq 'UND')then nfiles=96
tel=strlowcase(tel0)

m30=''  ;m30 marks image file for cor2 and hi's  day-30 to recreate mpeg movies after 30 days.
; change date to date-30
if keyword_set(minus30) and tel ne 'cor1' then begin
   ut30=anytim2utc(date)
   ut30.mjd=ut30.mjd-30
   date=utc2yymmdd(ut30,/yyyy)
   m30='_m30'
endif

if keyword_set(minus30) and tel eq 'cor1' then begin
   ut35=anytim2utc(date)
   ut35.mjd=ut35.mjd-35
   date=utc2yymmdd(ut35,/yyyy)
   m30='_m35'
endif



sc=strlowcase(sc0)
if strlowcase(sc) eq 'a' then osc ='b' else osc ='a'
teln=tel
IF tel EQ 'hi1' THEN teln='hi_1'
IF tel EQ 'hi2' THEN teln='hi_2'
if (tel eq 'euvi') then begin
  wls=['171','195','284','304']
  lfiles=file_search(getenv('SECCHI_JPG')+'/'+sc+'/euvi/'+date)            
  otherscfiles=file_search(getenv('SECCHI_JPG')+'/'+osc+'/euvi/'+date)
  if (lfiles[0] eq '' and keyword_set(domissing))then BEGIN
           scc_daily_pretties,date,tel,sc,/nopop
  ENDIF
  if (otherscfiles[0] eq '' and keyword_set(domissing) )then BEGIN
          scc_daily_pretties,date,tel,osc,/nopop
  ENDIF
 for n=0,n_elements(wls)-1 do begin
      elfiles=select_njpegs(sc,date,tel,nfiles,512,wls(n))
      if (elfiles[0] ne '')then BEGIN
	  outfile=concat_dir(outdir,'e'+wls(n)+m30+'.lst' )
	  rows=readlist(outfile)
	  outlist=[rows,elfiles]
	  unq=uniq(outlist,sort(outlist))
	  outlist=outlist[unq]
	  openw,njp,outfile;,/append
	  for nf=0,n_Elements(outlist)-1 do printf,njp,outlist(nf)
	  close,njp
      ENDIF
   endfor
endif else begin
  lfiles=select_njpegs(sc,date,tel,nfiles,512)
  otherscfiles=select_njpegs(osc,date,tel,nfiles,512)
      if (lfiles[0] eq '' and keyword_set(domissing))then BEGIN
           scc_daily_pretties,date,tel,sc,/nopop
           lfiles=select_njpegs(sc,date,tel,nfiles,512)
      ENDIF
      if (otherscfiles[0] eq '' and keyword_set(domissing))then BEGIN
          scc_daily_pretties,date,tel,osc,/nopop
          otherscfiles=select_njpegs(osc,date,tel,nfiles,512)
      ENDIF
      if (lfiles[0] ne '')then BEGIN
	  outfile=concat_dir(outdir,teln+m30+'.lst' )
	  rows=readlist(outfile)
	  outlist=[rows,lfiles]
	  unq=uniq(outlist,sort(outlist))
	  outlist=outlist[unq]
	  openw,njp,outfile;,/append
	  for nf=0,n_Elements(outlist)-1 do printf,njp,outlist(nf)
	  close,njp
      ENDIF
endelse

if (lfiles[0] eq ''  and not(keyword_set(nomail)))then begin 
    mailfile=getenv_slash('HOME')+'sc'+sc+'_pretties_'+date+'.tmp'
    openw,5,mailfile
       printf,5,   'Daily pretties missing for '+tel+' '+sc+' on '+date
     wait,2
    close,5
    repro_subj=sc+'_Missing_Daily_'+tel+'_'+date
    repro_mail='secchipipeline@cronus.nrl.navy.mil'
    spawn,'mailx -s '+repro_subj+' '+repro_mail+' <'+mailfile
    wait,10
    spawn, '/bin/rm -f '+mailfile
ENDIF

if(otherscfiles[0] eq '' and not(keyword_set(nomail)))then begin 
    mailfile=getenv_slash('HOME')+'sc'+osc+'_pretties_'+date+'.tmp'
    openw,5,mailfile
       printf,5,   'Daily pretties missing for '+tel+' '+osc+' on '+date
     wait,2
    close,5
    repro_subj=osc+'_Missing_Daily_'+tel+'_'+date
    repro_mail='secchipipeline@cronus.nrl.navy.mil'
    spawn,'mailx -s '+repro_subj+' '+repro_mail+' <'+mailfile
    wait,10
    spawn, '/bin/rm -f '+mailfile
ENDIF
;free_lun,njp      

end
      

      
