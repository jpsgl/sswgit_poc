pro scc_do_all_rdiff,dtst,dtend,tel,sc, _EXTRA=_extra
;+
; $Id: scc_do_all_rdiff.pro,v 1.2 2013/11/25 21:05:48 secchia Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : scc_do_all_daily_rdiff
;               
; Purpose   : to rerun scc_daily_rdiff for given days and telescopes
;               
; Explanation: 
;               
; Use       : IDL> scc_do_all_daily_rdiff,'YYYYMMDD','CAM','SC'
;    
; Inputs    :dtst= starting day yyyy-mm-dd ,dtend= ending day yyyy-mm-dd ,tel=[euvi, cor2, hi1, hi2] or 'all'
;               
; Outputs   : from scc_daily_pretties JPG24 and PNG files in /net/earth/data3/secchi/images
;
; Keywords  : _EXTRA  see sc_daily_Pretties.pro for available keywords
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: Need appropriate permissions to write images to disk, enviromenal variable sc must be defined.
;               
; Side effects: 
;               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2011
;               
; $Log: scc_do_all_rdiff.pro,v $
; Revision 1.2  2013/11/25 21:05:48  secchia
; nr - pass _extra through
;
; Revision 1.1  2011/05/19 17:04:41  mcnutt
; to run multi days of scc_daily_rdiff
;

ut0=anytim2utc(dtst)
ut1=anytim2utc(dtend)

mjd=ut0
if datatype(sc) eq 'UND' then ab=getenv('sc') else ab=strlowcase(sc) ;aorb ;['a','b'] 
if (tel(0) eq 'all') then tel=['cor1','cor2','hi1','hi2','euvi']
FOR i=ut0.mjd,ut1.mjd DO BEGIN
FOR j=0,n_Elements(tel)-1 DO BEGIN
    mjd.mjd=i
    yyyymmdd=utc2yymmdd(mjd,/yyyy)
    scc_daily_rdiff,yyyymmdd,tel(j),ab,/USE_DOUBLE, _EXTRA=_extra

ENDFOR
ENDFOR

end
