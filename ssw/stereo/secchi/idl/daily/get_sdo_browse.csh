#! /bin/csh -m
# $Id: get_sdo_browse.csh,v 1.1 2011/09/22 17:56:14 mcnutt Exp $
# $Log: get_sdo_browse.csh,v $
# Revision 1.1  2011/09/22 17:56:14  mcnutt
# crontab files to copy daily sdo browse images
#
# Revision 1.1  2011/05/02 18:34:02  mcnutt
# sdo prettie automation
#
# Made for automatic real-time application
# -32 is needed because this is a 64-bit computer. It will make the SPICE software work!
echo $HOME
source /net/cronus/opt/nrl_ssw_setup
setup

idl7 $HOME/secchi/idl/daily/get_sdo_browse.idl

