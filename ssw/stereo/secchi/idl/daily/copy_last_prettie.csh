#! /bin/csh -m
# $Id: copy_last_prettie.csh,v 1.1 2013/01/31 16:22:43 mcnutt Exp $
# $Log: copy_last_prettie.csh,v $
# Revision 1.1  2013/01/31 16:22:43  mcnutt
# new programs to copy last daily pretty to last subdir
#
#
# Made for automatic real-time application
# -32 is needed because this is a 64-bit computer. It will make the SPICE software work!
echo $HOME
source /net/cronus/opt/nrl_ssw_setup
setup
idl7 $HOME/secchi/idl/daily/copy_last_prettie.idl

