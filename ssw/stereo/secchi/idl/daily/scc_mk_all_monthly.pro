pro scc_mk_all_monthly,tel,sc,dtst,dtend,pol, INTERVAL=interval, NDAYS=ndays, DOREBIN=Dorebin, $
    	MANUAL=manual, TEST=test, _EXTRA=_extra
;
;+
; $Id: scc_mk_all_monthly.pro,v 1.9 2011/08/15 21:52:45 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : SCC_MONTHLY_MIN
;               
; Purpose   :This procedure is an easy interface to the SCC_MONTHLY_MIN procedure to make
;             background images for SECCHI data
;               
; Explanation: 
;               
; Use       : IDL> scc_mk_all_monthly,tel,sc,startdate, end-date,pol
;    
; Inputs    :tel = cor1, cor2, hi1, hi2
;            sc = 'A', 'B', 'AB' (AB means do both)
;            dtst = start date of interval to be processed (YYYYMMDD)
;            dtend = end date of interval to be processed (YYYYMMDD)
;            pol= '0', '120', '240', 'tbr','all'  (tbr = total brightness,'all' means do 0,120, 240, dbl, and tbr)
;               
; Keywords  :   INTERVAL: No. of days between background (default 7)
;               NDAYS: No. of medians into each background (default 21)
;               MANUAL: Override the default 'dates' for which a background will be generated
;               OUTSIZE=    Desired size of output; should be LE input median images; default is 1024
;
; Calls from LASCO : 
;
; Common    : 
;               
; Restrictions: Need $SECCHI_BKG and appropriate permissions if not using savedir option
;               
; Side effects: 
;
; Procedure:
;	IF MANUAL is not set, this procedure calls mk_monthly_min.pro for dates 
;	corresponding to the included list of dates in between the starting and 
;	ending dates. Gaps of more that 2 days are not spanned. IF MANUAL is set, 
;	all days in the input interval (inclusive) which exist will be used to 
;	compute the minimum image, and it will be named according to the midpoint 
;	of the interval of days used.
               
; Category    : DAILY
;               
; Prev. Hist. : None.
;
; Examples:
;       scc_mk_all_monthly,'hi1','b','20070401','20070601','tbr'
;       scc_mk_all_monthly,'cor2','a','20070401','20070601','120'
;
; Written     : Karl Battams, NRL/I2, Jan 2007
;               
; $Log: scc_mk_all_monthly.pro,v $
; Revision 1.9  2011/08/15 21:52:45  nathan
; eliminate cd,
;
; Revision 1.8  2011/08/03 20:52:49  nathan
; use nrlanytim2utc
;
; Revision 1.7  2011/07/20 15:34:29  nathan
; add type DBL to pol=ALL
;
; Revision 1.6  2010/05/12 15:54:30  nathan
; implement OUTSIZE= keyword via _EXTRA
;
; Revision 1.5  2007/11/13 21:53:35  nathan
; updated doc
;
; Revision 1.4  2007/10/26 22:18:33  nathan
; added tbr to pol=all
;
; Revision 1.3  2007/10/11 16:36:42  reduce
; Trying to fix bugs where processing is abandoned in odd dirs. Karl.
;
; Revision 1.2  2007/09/04 16:22:15  reduce
; Now does both SC and all COR pol angles in one go, if desired. Karl.
;
; Revision 1.1  2007/07/27 18:20:21  reduce
; Initial release. Karl B.
;
;
;-
;

;cd,curr=orig

IF not keyword_set(sc) THEN BEGIN
    PRINT
    PRINT,'Spacecraft identifier missing!'
    PRINT,'Spacecraft can be "a","b" or "ab" (both)'
    PRINT,'Calling sequence is:'
    PRINT,'     scc_mk_all_monthly,CAM,SC,STDTE,ENDDT,POL,[+opts]'
    PRINT
    ;cd,orig
    return
ENDIF

IF not keyword_set(pol) THEN BEGIN
    PRINT
    PRINT,'Polarizer identifier missing!'
    PRINT,'pol= tbr,0,120, 240 or "all"
    PRINT,'Calling sequence is:'
    PRINT,'     scc_mk_all_monthly,CAM,SC,STDTE,ENDDT,POL,[+opts]'
    PRINT
    ;cd,orig
    return
ENDIF

IF keyword_set(MANUAL) THEN nbetween = 7 ELSE nbetween=1

IF keyword_set(INTERVAL) THEN nbetween = interval 

IF keyword_set(MANUAL) THEN print,'Days between mins =',nbetween

print,'Polarizer = ',pol
print

st = nrlanytim2utc(dtst)
en = nrlanytim2utc(dtend)

;
;  Convert the date strings to modified julian dates
;
jd = st
jdstart = st.mjd
jdend = en.mjd

xxlist=['0107', '0114', '0121', '0128', '0204', '0211', $
'0218', '0225', '0304', '0311', '0318', '0325', $
'0401', '0408', '0415', '0422', '0429', '0506', $
'0513', '0520', '0527', '0603', '0610', '0617', $
'0624', '0702', '0709', '0716', '0723', '0730', $
'0806', '0813', '0820', '0827', '0903', '0910', $
'0917', '0924', '1001', '1008', '1015', '1022', $
'1029', '1105', '1112', '1119', '1126', '1203', $
'1210', '1217', '1224', '1231']

if strlowcase(sc) EQ 'ab' THEN sc=['a','b']
nsc=n_elements(sc)

if strlowcase(pol) EQ 'all' THEN pol=['0','120','240','tbr','dbl']
np=n_elements(pol)


FOR j=0,nsc-1 DO BEGIN
    FOR k=0,np-1 DO BEGIN
        FOR i=jdstart,jdend,nbetween DO BEGIN
            jd.mjd = i
            yymmdd = utc2yymmdd(jd)
            mmdd = STRMID(yymmdd,2,4)
            ; stop
            IF NOT (keyword_set(INTERVAL) OR keyword_set(MANUAL)) THEN duh = WHERE(xxlist EQ mmdd,inlist) ELSE inlist=1
            print,i,'  ',yymmdd,inlist

            IF inlist THEN BEGIN
                PRINT,'MAKING MONTHLY IMAGE FOR ',strcompress(yymmdd,/remove_all)
                SCC_MONTHLY_MIN,tel,sc[j],yymmdd, pol[k], NDAYS=ndays, _EXTRA=_extra
            ENDIF
        ENDFOR
    ENDFOR
ENDFOR

;cd,orig

END
