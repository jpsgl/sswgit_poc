
outdir=getenv('SECCHI_PNG')+'/a/last/'
tmp=file_search(getenv('SECCHI_PNG')+'/a/cor1_rdif/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/cor1_rdif/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor1_rdif.png',/overwrite

tmp=file_search(getenv('SECCHI_PNG')+'/a/cor/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/cor/'+yyyymmdd+'/256/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor2.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/a/cor2_rdif/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/cor2_rdif/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor2_rdif.png',/overwrite

tmp=file_search(getenv('SECCHI_PNG')+'/a/hi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi/'+yyyymmdd+'/256/*tbh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi/'+yyyymmdd+'/256/*tbh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi_stars/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi_stars/'+yyyymmdd+'/256/*tbh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2_stars.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi_stars/'+yyyymmdd+'/256/*tbh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1_stars.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi_srem/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi_srem/'+yyyymmdd+'/512/*SRh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2_srem.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/a/hi_srem/'+yyyymmdd+'/512/*SRh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1_srem.png',/overwrite

tmp=file_search(getenv('SECCHI_PNG')+'/a/euvi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
  wls=['171','195','284','304']
  for wn=0,3 do begin  
    outpng=outdir+'euvi'+wls(wn)+'.png'
    tmp=file_search(getenv('SECCHI_PNG')+'/a/euvi/'+yyyymmdd+'/256/'+'*'+strmid(wls(wn),0,2)+'eu*')
    if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outpng,/overwrite
  endfor
tmp=file_search(getenv('SECCHI_PNG')+'/a/euvi_rdif/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/a/euvi_rdif/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'euvi_rdif.png',/overwrite

outdir=getenv('SECCHI_JPG')+'/a/last/'
tmp=file_search(getenv('SECCHI_JPG')+'/a/cor1/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_JPG')+'/a/cor1/'+yyyymmdd+'/256/*.jpg')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor1.jpg',/overwrite

tmp=file_search(getenv('SECCHI_JPG')+'/a/cor/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_JPG')+'/a/cor/'+yyyymmdd+'/256/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor2.jpg',/overwrite

tmp=file_search(getenv('SECCHI_JPG')+'/a/hi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_JPG')+'/a/hi/'+yyyymmdd+'/256/*tbh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2.jpg',/overwrite
tmp=file_search(getenv('SECCHI_JPG')+'/a/hi/'+yyyymmdd+'/256/*tbh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1.jpg',/overwrite

tmp=file_search(getenv('SECCHI_JPG')+'/a/euvi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
  wls=['171','195','284','304']
  for wn=0,3 do begin  
    outjpg=outdir+'euvi'+wls(wn)+'.jpg'
    tmp=file_search(getenv('SECCHI_JPG')+'/a/euvi/'+yyyymmdd+'/256/'+'*'+strmid(wls(wn),0,2)+'eu*')
    if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outjpg,/overwrite
  endfor


outdir=getenv('SECCHI_PNG')+'/b/last/'
tmp=file_search(getenv('SECCHI_PNG')+'/b/cor1_rdif/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/cor1_rdif/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor1_rdif.png',/overwrite

tmp=file_search(getenv('SECCHI_PNG')+'/b/cor/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/cor/'+yyyymmdd+'/256/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor2.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/b/cor2_rdif/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/cor2_rdif/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor2_rdif.png',/overwrite

tmp=file_search(getenv('SECCHI_PNG')+'/b/hi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi/'+yyyymmdd+'/256/*tbh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi/'+yyyymmdd+'/256/*tbh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi_stars/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi_stars/'+yyyymmdd+'/256/*tbh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2_stars.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi_stars/'+yyyymmdd+'/256/*tbh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1_stars.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi_srem/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi_srem/'+yyyymmdd+'/512/*SRh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2_srem.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/b/hi_srem/'+yyyymmdd+'/512/*SRh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1_srem.png',/overwrite

tmp=file_search(getenv('SECCHI_PNG')+'/b/euvi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
  wls=['171','195','284','304']
  for wn=0,3 do begin  
    outpng=outdir+'euvi'+wls(wn)+'.png'
    tmp=file_search(getenv('SECCHI_PNG')+'/b/euvi/'+yyyymmdd+'/256/'+'*'+strmid(wls(wn),0,2)+'eu*')
    if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outpng,/overwrite
  endfor
tmp=file_search(getenv('SECCHI_PNG')+'/b/euvi_rdif/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/b/euvi_rdif/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'euvi_rdif.png',/overwrite

outdir=getenv('SECCHI_JPG')+'/b/last/'
tmp=file_search(getenv('SECCHI_JPG')+'/b/cor1/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_JPG')+'/b/cor1/'+yyyymmdd+'/256/*.jpg')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor1.jpg',/overwrite

tmp=file_search(getenv('SECCHI_JPG')+'/b/cor/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_JPG')+'/b/cor/'+yyyymmdd+'/256/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'cor2.jpg',/overwrite

tmp=file_search(getenv('SECCHI_JPG')+'/b/hi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_JPG')+'/b/hi/'+yyyymmdd+'/256/*tbh2*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi2.jpg',/overwrite
tmp=file_search(getenv('SECCHI_JPG')+'/b/hi/'+yyyymmdd+'/256/*tbh1*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'hi1.jpg',/overwrite

tmp=file_search(getenv('SECCHI_JPG')+'/b/euvi/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
  wls=['171','195','284','304']
  for wn=0,3 do begin  
    outjpg=outdir+'euvi'+wls(wn)+'.jpg'
    tmp=file_search(getenv('SECCHI_JPG')+'/b/euvi/'+yyyymmdd+'/256/'+'*'+strmid(wls(wn),0,2)+'eu*')
    if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outjpg,/overwrite
  endfor

outdir=getenv('SECCHI_PNG')+'/soho/last/'
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c2/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c2/'+yyyymmdd+'/1024/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'c2.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c2_rdiff/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c2_rdiff/'+yyyymmdd+'/1024/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'c2_rdif.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c3/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c3/'+yyyymmdd+'/1024/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'c3.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c3_rdiff/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/soho/c3_rdiff/'+yyyymmdd+'/1024/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'c3_rdif.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/soho/lasco/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/soho/lasco/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'lasco.png',/overwrite
tmp=file_search(getenv('SECCHI_PNG')+'/soho/lasco_rdiff/*')
yyyymmdd=rstrmid(tmp(n_elements(tmp)-1),0,8)
tmp=file_search(getenv('SECCHI_PNG')+'/soho/lasco_rdiff/'+yyyymmdd+'/512/*')
if tmp(0) ne '' then file_copy, tmp(n_elements(tmp)-1),outdir+'lasco_rdif.png',/overwrite



END

