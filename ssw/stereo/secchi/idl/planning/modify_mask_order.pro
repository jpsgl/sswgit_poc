;+
;$Id: modify_mask_order.pro,v 1.4 2009/02/17 16:50:53 esfand Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : MODIFY_MASK_ORDER
;               
; Purpose     : Convert a ROI or OCC mask table from IDL coordinates
;               to FSW CCD-coordinate or vice-versa. See below for details.
;               
; Use         : new_mask= MODIFY_MASK_ORDER(sc, cam, mask, /TABLE2IDL) 
;    
; Inputs      : sc        SpaceCraft String: A or B
;               cam       Telescope String: EUVI, COR1, COR2, HI1, or HI2
;               mask      Mask to be converted.
;
; Opt. Inputs : None
;               
; Outputs     : new_mask Coverted mask. 
;
; Opt. Outputs: None
;
; Keywords    : TABLE2IDL if set change from CCD flight coordinates to PT/IDL coordinates. 
;
; Written by  : Ed Esfandiari, NRL, May 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari 05/20/05 - use ccd depenedent orientaions.
;              Ed Esfandiari 10/30/06 - used latest CCD orientations and inversions.
;              Ed Esfandiari 11/08/06 - Removed debug stuff.
;              Ed Esfandiari 02/12/09 - Change plot coordinates to match Robin's chart.
;
; $Log: modify_mask_order.pro,v $
; Revision 1.4  2009/02/17 16:50:53  esfand
; display coordinate now matches un-rectified ccd depending on telescope - AEE
;
; Revision 1.3  2008/06/20 18:07:15  esfand
; added ICER Mask
;
; Revision 1.3  2006/10/31 18:50:56  esfand
; used new CCD coordinates
;
; Revision 1.2  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.1.1.2  2004/07/01 21:19:04  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:36  esfand
; first checkin
;
;
;-


FUNCTION APPLY_FUNC, mask, func
  omask= mask 
  FOR i= 0, N_ELEMENTS(func)-1 DO BEGIN
    CASE func(i) OF 
     'NO':  omask= omask
     'EW':  omask= REVERSE(omask)
     'SN':  omask= ROTATE(omask,7)
     '+90': omask= ROTATE(omask,1)
     '-90': omask= ROTATE(omask,3)
    ENDCASE
  ENDFOR
  RETURN, omask
END


FUNCTION GSW2FSW_ORIENTAION, sc, cam, mask

   nmask= mask

  ; These are reverse of FSW2GSW_ORIENTATION.

  IF (sc EQ 'A') THEN BEGIN

; **** 10/24 these A cases are done.

    CASE cam OF
      'EUVI': nmask= ROTATE(mask,6)
      'COR1': nmask= ROTATE(mask,1)
      'COR2': nmask= ROTATE(mask,3)
      'HI1': nmask= ROTATE(mask,0)
      'HI2': nmask= ROTATE(mask,0)
    ENDCASE
  ENDIF ELSE BEGIN

; **** 10/24 all of these B cases are done.

    IF (sc EQ 'B') THEN BEGIN
      CASE cam OF
      'EUVI': nmask= ROTATE(mask,1) 
      'COR1': nmask= ROTATE(mask,3)
      'COR2': nmask= ROTATE(mask,1)
      'HI1': nmask= ROTATE(mask,2)
      'HI2': nmask= ROTATE(mask,2)
      ENDCASE
    ENDIF ELSE BEGIN
      PRINT,'**** Invalid S/C: '+sc
    ENDELSE
  ENDELSE

  ;RETURN, APPLY_FUNC(mask,func)
  RETURN, nmask

END
 


FUNCTION FSW2GSW_ORIENTAION, sc, cam, mask

  nmask= mask
  IF (sc EQ 'A') THEN BEGIN
    CASE cam OF
      'EUVI': nmask= ROTATE(mask,6)
      'COR1': nmask= ROTATE(mask,3) 
      'COR2': nmask= ROTATE(mask,1)
      'HI1': nmask= ROTATE(mask,0)
      'HI2': nmask= ROTATE(mask,0)
    ENDCASE
  ENDIF ELSE BEGIN
    IF (sc EQ 'B') THEN BEGIN
      CASE cam OF
        'EUVI': nmask= ROTATE(mask,3)
        'COR1': nmask= ROTATE(mask,1)
        'COR2': nmask= ROTATE(mask,3)
        'HI1': nmask= ROTATE(mask,2)
        'HI2': nmask= ROTATE(mask,2)
      ENDCASE
    ENDIF ELSE BEGIN
      PRINT,'**** Invalid S/C: '+sc
    ENDELSE
  ENDELSE

  ;RETURN, APPLY_FUNC(mask,func)
  RETURN, nmask 
END
 

FUNCTION MODIFY_MASK_ORDER, sc, cam, mask, table2idl=table2idl

;
; The OCC and ROI mask tables contain masking info for 1156 blocks of the CCD (each
; block is 64*64 pixels). The CCD orientaions for various A/B SC are different depending
; on where the first pixel readout is (SE,SW, NE, or NW) and the right or left readout
; port used. We need to chanage from camera CCD coordiantes (tables) used by FSW after
; image is read-out from CCD to GSW proceesed image (N=up, E=left) and to IDL coordinates. 
;
; FSW generates the image by arranging the pixels read from CCD this way:
; places block 0 at the upper-left-corner and increments to the left (row by row):
;
;   0       1    2    .....      31    32     33
;                       .
;                       .
;                       .
;  1122  1123  1124   .....    1153   1154   1155
;
;*** Note: The un-reactified FSW data received at the ground has the 0...33 row at
;          the bottom (not top as listed above), as listed below, so they are the same as ground (rectified):
;
;  1122  1123  1124   .....    1153   1154   1155
;                       .
;                       .
;   0       1    2    .....      31    32     33
;
;
; IDL plotting routines, however, uses the lower-left-corner for block 0 (x=0,y=0)
; and increments to the left (row by row)::
;
;  1122  1123  1124   .....    1153   1154   1155
;                       .
;                       .
;                       .
;   0       1    2    .....      31    32     33
;
; This function takes the blocks to be masked and changes the block numbers to
; the match either IDL order or mask table order, depending on the TABLE2IDL
; keyword.


  IF (KEYWORD_SET(table2idl)) THEN BEGIN  ; change from mask_table coordinates to idl
;    fullmask= mask  ; in this case, mask has always 1156 elements.
;    tmp1 = REFORM(fullmask,34,34)
;    tmp= FSW2GSW_ORIENTAION(sc,cam,tmp1)
;    new= tmp(*,33)
;    FOR j= 32,0,-1 DO new= [new,tmp(*,j)]
;    ;new = ROTATE(tmp,7)    ; flip upside down to match IDL coordinates
;    ;new = REFORM(new,1156) ; change to a 1-dim array
;    nmask=new 
    fullmask= mask  ; in this case, mask has always 1156 elements.
    tmp1 = REFORM(fullmask,34,34)
    tmp= FSW2GSW_ORIENTAION(sc,cam,tmp1)
    nmask= tmp 
  ENDIF ELSE BEGIN                        ; change from idl coordinates to mask_table
    fullmask= INDGEN(34*34)  ; => arry of 0 to 1155
    tmp = REFORM(fullmask,34,34)
;    new= tmp(*,33)
;    FOR j= 32,0,-1 DO new= [new,tmp(*,j)]
    new= tmp(*,0)
    FOR j= 1,33 DO new= [new,tmp(*,j)]
;    ;new = ROTATE(tmp,7)    ; flip upside down to match IDL coordinates
;    ;new = REFORM(new,1156) ; change to a 1-dim array
    nmask= mask  ;  in this case, mask only contain selected blk #s (masked blocks 0-1155)
    FOR i= 0, N_ELEMENTS(mask)-1 DO  $
      nmask(i)= WHERE(nmask(i) EQ new)
;    nmask= nmask(SORT(nmask)) ; now nmask blocks match non-IDL coordinate (0=upper-left)
   
    grnd= tmp 
    flight= GSW2FSW_ORIENTAION(sc,cam,grnd)
    FOR i= 0, N_ELEMENTS(mask)-1 DO nmask(i)= WHERE(nmask(i) EQ flight)
  ENDELSE
  ;print,'nmask= ',nmask

  RETURN,nmask
END
