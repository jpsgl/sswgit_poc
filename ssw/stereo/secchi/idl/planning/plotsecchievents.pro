pro plotsecchievents, evt
; $Id: plotsecchievents.pro,v 1.2 2009/10/27 19:49:34 nathan Exp $
;
; Project:  STEREO SECCHI
;
; Purpose: Plot events for A and B from ops/docs/secchievents.txt
;   	    
; Inputs:
;	evt STRING  string for which to get times from secchievents.txt
;
; Outputs:
;	Generates plots
;
; Keyword output:
;   	ATIMES= string time array
;   	BTIMES= string time array
;
; Restrictions: 
;   	Requires ops/docs to be checked out in $stereo
;
; Author: N.Rich, NRL/I2, 
;
; $Log: plotsecchievents.pro,v $
; Revision 1.2  2009/10/27 19:49:34  nathan
; added plot for time on x-axis
;
; Revision 1.1  2009/07/06 21:24:45  nathan
; plots watchdog resets
;

if datatype(evt) NE 'STR' THEN message,'Input evt is undefined.'

rows0=readlist(getenv('stereo')+'/ops/docs/secchievents.txt')
rows =reverse(grep(evt,rows0))
help,rows
nr=n_elements(rows)
times=strmid(rows,0,16)
utc=anytim2utc(times)
utc0=utc[0]
utc1=utc[nr-1]
utc1.mjd=utc1.mjd+10
tai=utc2tai(utc)

parts=strsplit(rows[0],/extract)
ab=parts[2]

FOR i=1,nr-1 DO BEGIN
    parts=strsplit(rows[i],/extract)
    ab=[ab,parts[2]]
ENDFOR

a=where(ab EQ 'Ahead' or ab EQ 'Ahe/Beh',na)
b=where(ab EQ 'Behind' or ab EQ 'Ahe/Beh',nb)

atimes=times[a[1:na-1]]
btimes=times[b[1:nb-1]]

atdif=tai[a]-shift(tai[a],1)
atdif=atdif[1:na-1]/(3600*24.)

btdif=tai[b]-shift(tai[b],1)
btdif=btdif[1:nb-1]/(3600*24.)

dmin=min([btdif,atdif],max=dmax)

yti='Days since previous reset'
tit='SECCHI Watchdog Timer Resets @ '+times[nr-1]
plotprep

window,2
plot,atdif,psym=2,ytitle=yti,title=tit,yrang=[dmin,dmax]
oplot,atdif,psym=-2,color=2
oplot,btdif,psym=-2,color=4

plotvtime,atimes,atdif,'Ahead',psym=-2,color=2,ytit=yti,titl=tit,yrang=[dmin,dmax],timeran=[utc0,utc1]
oplotvtime,btimes,btdif,'Behind',psym=-2,color=4
stop
end
