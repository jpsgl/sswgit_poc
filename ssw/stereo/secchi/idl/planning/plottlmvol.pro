pro plottlmvol, sc, stadt, enddt
;+
; $Id: plottlmvol.pro,v 1.3 2009/07/06 21:23:43 nathan Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : plottlmvol
;               
; Purpose   : Read in telemetry statistics from $SECCHI_DATA/statistics and plot for 
;   	    given interval; also reads in predicted values from schedules and sizes
;   	    of level-0 files from $SDS_(FIN_)DIR.
;               
; Use       : IDL> 
;    
; Inputs    : sc    'a' or 'b'
;   	      stadt 	First date of interval
;   	      enddt 	Last date of interval
;               
; Outputs   : out - image with datetime stamp included        
;
; Keywords  : 
;               
; Calls     : SCC_FITSHDR2STRUCT
;               
; Category    : plot telemetry volume packets planning data rate
;               
; Prev. Hist. : None.
;
; Written     :N.Rich
;               
; $Log: plottlmvol.pro,v $
; Revision 1.3  2009/07/06 21:23:43  nathan
; added header
;

getsccarchiveinfo, vec=sci1, udates=ssr1dts, sta_date=stadt, end_date=enddt, /volume, /ssr1, /sci, spacecraft=sc

getsccarchiveinfo, vec=sci2, udates=ssr2dts, sta_date=stadt, end_date=enddt, /volume, /ssr2, /sci, spacecraft=sc

getsccarchiveinfo, vec=scix, udates=spwxdts, sta_date=stadt, end_date=enddt, /volume, /spw, /sci, spacecraft=sc

get_lz_size, sc, stadt, enddt, lzdts, lzsz,ver=3
lzsz=lzsz/(1024.*1024)

get_daily_sch_size, sc, stadt, enddt,  schdts, schsz, schstat

help,sci1
maxmin,sci1
help,sci2
maxmin,sci2
help,scix
maxmin,scix
help,lzsz
maxmin,lzsz
help,schsz
maxmin,schsz
help,ssr1dts,ssr2dts, spwxdts, lzdts,schdts

plotprep
!p.multi=0
!y.style=1
window

titl='SECCHI-'+strupcase(sc)+' Telemetry volumes'
wz=where(lzsz NE 0)
ws=where(schstat EQ 0)
maxsz=100*ceil(max(lzsz)/100.)

utplot,lzdts[wz],lzsz[wz],psym=-1,yran=[0,maxsz],ytit='MB',title=titl
toppix=!x.window[1]*!d.y_size
ypos=toppix-30
xpos=0.7*!d.x_size
xyouts,xpos,ypos, 'LZ Tlm Files',/dev,size=1.5

outplot,lzdts,sci1+sci2+scix+19,psym=-1,color=2
ypos=ypos-15
xyouts,xpos,ypos,'Raw Sci image + hk',color=2,size=1.5, /dev

outplot,lzdts[ws],schsz[ws],psym=-1,color=3
ypos=ypos-15
xyouts,xpos,ypos,'Schedules SSR1',color=3,size=1.5, /dev

outplot,lzdts,sci1,psym=-1,color=4
ypos=ypos-15
xyouts,xpos,ypos,'Raw Sci SSR1',color=4,size=1.5, /dev

outplot,lzdts,sci2,psym=-1,color=5
ypos=ypos-15
xyouts,xpos,ypos,'Raw Sci SSR2',color=5,size=1.5, /dev

print,'Use vars lzdts, lzsz[wz], sci1, sci2, scix, schsz[ws] to look at detail.'
stop

end
