pro readsafetytable, sc, out, SAVEFILE=savefile
; $Id: readsafetytable.pro,v 1.1 2009/05/15 20:29:53 nathan Exp $
;
; Project:  STEREO SECCHI
;
; Purpose: Read in and print translation of current autonomy rule table in FSW source
;   	    
; Inputs:
;	sc	'a' or 'b'
;
; Outputs:
;	Prints to window; optional saves output to file
;
; Keywords:
;   SAVEFILE	Set to name of file to save output to, default ~/safettd[sc].img.txt
;
; Restrictions: 
;   Must have $ITOS_GROUPDIR defined and populated sdb/ and source/ in that directory.
;
; Author: N.Rich, NRL/I2, 
;
; $Log: readsafetytable.pro,v $
; Revision 1.1  2009/05/15 20:29:53  nathan
; checkin
;


sc=strlowcase(sc)
rulenum=999
first=1
nrules=0

fn=getenv('ITOS_GROUPDIR')+'/source/insthlth/src/safettd'+sc+'.img'
li=''
fields=['Rule#','ComparTyp','Limit','EvalIntrv','PersThres','PersWin','APID','StaByte','StaBit','NumBits','ConvIndx','EvType','Action#','ActionMd','Enabled','Descr']

nf=n_elements(fields)
nums=intarr(nf-1)
chars=bytarr(20)
allnums=intarr(50,nf-1)
allnames=strarr(50)

head1=''
head2=''
head3=''
openr,1,fn
readf,1,head1
readf,1,head2
readf,1,head3
heads=[head1,head2,head3]
while not eof(1) and rulenum GT 0 do begin
    readf,1,li
    IF strmid(li,0,1) EQ 'X' THEN BEGIN
    	parts=strsplit(li,/extract)
	for i=0,nf-2 do nums[i]=hex2decf(parts[i])
	rulenum=nums[0]
	for i=0,19 do chars[i]=hex2decf(parts[i+nf-1])
	name = string(chars)
	allnums[nrules,*]=nums
	allnames[nrules]=name
	
	nrules=nrules+1
	first=0
    ENDIF
endwhile
close,1

allnums=allnums[0:nrules-1,*]
allnames=allnames[0:nrules-1]
mnmnames=strarr(nrules)
;++Get name of mnemonic by searching sdb/merged_tlm.sdb

first=1
sdbfn=getenv('ITOS_GROUPDIR')+'/sdb/merged_tlm.sdb'
openr,1,sdbfn
print,'Reading ',sdbfn
while not eof(1) do begin
    readf,1,li
    ; Extract all mnemonics of ApID 0x42B
    pos=strpos(li,'0x42B')
    map=strpos(li,'MAP')
    IF pos GT 0 and map NE 0 THEN BEGIN
    	IF first then mnms=li ELSE mnms=[mnms,li]
	first=0
    ENDIF
ENDWHILE
close,1
nmnms=n_elements(mnms)
stabytes=reform(allnums[*,7])
allstabytes=intarr(nmnms)
allmnmnames=strarr(nmnms)
print,'Parsing PKT rows.'
FOR i=0,nmnms-1 DO BEGIN
    parts=strsplit(mnms[i],'|',/extract)
    allstabytes[i]=fix(parts[7])
    allmnmnames[i]=trim(parts[2])
ENDFOR

compstr=['','<','>','=']
outformat='(i3,a17,a2,f7.2,i3,i2,"/",i2,1x,a20)'
IF keyword_set(SAVEFILE) THEN BEGIN
    IF datatype(savefile) NE 'STR' THEN outfn='~/safetdm'+sc+'.img.txt' ELSE outfn=savefile
    print,'Saving ',outfn
    openw,1,outfn
    FOR i=0,2 do printf,1,heads[i]
ENDIF

FOR i=0,nrules-1 DO BEGIN
    w=where(allstabytes EQ stabytes[i])
    mnmnames[i]=allmnmnames[w[0]]
    nums=reform(allnums[i,*])
    descr=allnames[i]
    IF keyword_set(SAVEFILE) THEN $
    printf,1,nums[0],mnmnames[i],compstr[nums[1]],nums[2]/100.,nums[3],nums[4],nums[5],descr,format=outformat
    print,nums[0],mnmnames[i],compstr[nums[1]],nums[2]/100.,nums[3],nums[4],nums[5],descr,format=outformat

ENDFOR
close,1

end
