function getstereortlt, sc, dates, SPICE=spice
; $Id: getstereortlt.pro,v 1.1 2009/01/27 19:04:05 nathan Exp $
;
; Project:  STEREO SECCHI
;
; Purpose: returns round-trip-light-time between earth and one of the stereo spacecraft
;	   based on a simple linear model
;   	    
; Inputs:
;	sc	'a' or 'b'
;	dates	array of times (input to anytim2utc)
;
; Outputs:
;	fltarr = seconds of same dimension as dates
;
; Keywords:
;   /SPICE  Use values from SPICE kernel instead of linear model (more accurate)
;
; Restrictions: 
;
; Author: N.Rich, NRL/I2, 
;
; $Log: getstereortlt.pro,v $
; Revision 1.1  2009/01/27 19:04:05  nathan
; ok
;


sc=strlowcase(sc)

IF keyword_set(SPICE) THEN BEGIN
    aulttime=499.0  ; seconds light to go 1AU
    hee_b  = get_stereo_lonlat(dates, sc,     /au,  system='HEE')
    hee_e  = get_stereo_lonlat(dates, 'Earth', /au,  system='HEE')
    angles = get_stereo_sep_angle(dates,sc, 'Earth')
    ; Law of cosines: a^2 = b^2 + c^2 - 2*b*c*cos A where A is the angle of a scalene traingle opposite side a
    d2sc= sqrt( hee_b[0,*]^2 + hee_e[0,*]^2 - 2 * hee_b[0,*]*hee_e[0,*] * cos(angles))
    lttime=d2sc*499.0   
ENDIF ELSE BEGIN

    IF (sc EQ 'a') THEN BEGIN
       ang0= 4.1655933  
       ; initial angle on 4/1/2007
       degpsec= 6.9591911e-07
       ; degpsec is degrees/second
    ENDIF 
    IF (sc EQ 'b') THEN BEGIN
       ang0= 1.6181840   
       degpsec= 7.1123812e-07
    ENDIF 
    xt=utc2tai(anytim2utc('2007-04-01T00:00:00')) ;normalized to 04-01-2007
    xsec= utc2tai(anytim2utc(dates)) -xt
    ltearth= 498.53178  ;= 1AU

    esar=(ang0+degpsec*xsec)* 0.017453293 ; convert degrees to radians
    lttime=sqrt((ltearth-(ltearth*cos(esar)))^2+(ltearth*sin(esar))^2)
ENDELSE

return, lttime*2

end
