;+
;$Id: read_mask_tables.pro,v 1.7 2011/12/14 23:24:25 nathan Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : READ_MASK_TABLES
;               
; Purpose     : Read FSW OCCulter (OCC) and Region_Of_Interest (ROI) mask tables.
;               
; Explanation : Flight OCCulter (OCC) and Region Of Interest (ROI) mask tables contain
;               maps of 64x64 pixel blocks NOT sent to the ground. Each block is represented
;               by a single bit in an array (34x34=1156 elements) representing the sky region 
;               of the CCD. There is one such array per telescope for OCC and 4 per telescope
;               FOR ROI.
;               This routine reads in the mask_tables.dat containing 25 masked data sections 
;               (one OCC and four ROI tables per telescope). Each section/table contains 73 16-bit
;               words of data on 8 lines.  First seven lines contain 10 words each and 8th line
;               contains the last 3 words. Since only 72.25 (1156/16.0) words are needed, the 
;               last 3 nibbles of last word is not used. The ROI and OCC tables are read in 
;               and are then convert from FSW format (block 0 at upper-right-corner) to IDL
;               coordinates (block 0 at lower-right-corner) for output.
;
;   	    	All lines in file are saved in common block for later reference.
;
; Use         : READ_MASK_TABLES, fn, roi_mask, occ_mask, sc, /verbose 
;    
; Inputs      : fn -  In flight Occulter and ROI mask table filename (i.e. '../TABLES/mask_tables.dat') 
;               
;             : sc -  SpaceCraft 'A' or 'B'.
;
; Opt. Inputs : None
;
; Outputs     : roi_mask - BYTARR(5,1156,4)  representing COR2,COR1,EUVI,HI2,HI1 ROI masked blocks, 
;                          4 tables per telescope.
;               occ_mask - BYTARR(5,1156)  representing COR2,COR1,EUVI,HI2,HI1 OCC masked blocks,
;                          1 table per telescope.
;
; Opt. Outputs: None
;	
; Keywords    : verbose - If set, prints out the mask tables to the screen.
;   	    	/NORECTIFY  Output masks for unrectified images (CCD coords) 
;
; Written By  : Ed Esfandiari, NRL, May 2004 - First Version.
;               
; Modification History: 
;              Ed Esfandiari 07/13/04 - use 4 ROI tables per tele instead of 1.
;              Ed Esfandiari 05/23/05 - use ccd depenedent orientaions.
;              Ed Esfandiari 02/12/09 - Added icer_mask function.
;
; $Log: read_mask_tables.pro,v $
; Revision 1.7  2011/12/14 23:24:25  nathan
; allow converting Icer mask to a Rice mask; add /NORECTIFY option
;
; Revision 1.6  2009/08/07 19:52:22  nathan
; retain comments from input file in output file
;
; Revision 1.5  2009/02/17 16:50:43  esfand
; display coordinate now matches un-rectified ccd depending on telescope - AEE
;
; Revision 1.4  2008/12/12 13:02:05  mcnutt
; removed pipeline keyword
;
; Revision 1.3  2008/12/11 20:32:55  mcnutt
; added pipeline keyword to keep flight config
;
; Revision 1.2  2008/06/20 18:07:25  esfand
; added ICER Mask
;
; Revision 1.1  2006/10/03 19:17:52  nathan
; copied from PT/PRO
;
; Revision 1.3  2005/05/26 20:00:59  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.2  2004/09/01 15:40:46  esfand
; commit new version for Nathan.
;
; Revision 1.1.1.2  2004/07/01 21:19:08  esfand
; first checkin
;
; Revision 1.1.1.1  2004/06/02 19:42:35  esfand
; first checkin
;
;
;-
;

FUNCTION ICER_MASK,coords
  p1r= 1 & p2r= 3 & p1c=0 & p2c=2

  mask= bytarr(2176,2176)+1

  p1c=coords(0) & p2c=coords(2) & p1r=coords(1) & p2r= coords(3) 

  for i= p1c, p2c do begin
   for j= p1r, p2r do begin
     mask(i-1,j-1)= 0
   endfor
  endfor

  ;window,xsize=544,ysize=544,retain=2
  ;tv,rebin(mask,544,544)
  mask1= rebin(mask,34,34)
  mask2= bytarr(1156)
  for i=0,33 do mask2(i*34:i*34+33)= mask1(*,i)
 ;help,mask,mask1,mask2
 ;print,mask2,format='(34z2)'

  RETURN, mask2
END


FUNCTION ICER2BLK,str_coords,sc,tel,table

COMMON ICERMASKS, occ_icer, roi_icer, filelines, slun, flpos

   mask= bytarr(1156)
   coords= FIX(str_coords)
  ; IF (MAX(coords) EQ 0) THEN RETURN, mask
  IF (min(coords) EQ 0 and max(coords) EQ 0) THEN $
    print,'Using Rice occulter mask' ELSE $
  IF (MIN(coords) LT 1 OR MAX(coords) GT 2176) THEN BEGIN
   PRINT,''
   PRINT,'Invalid coordinate - Valid range: 1 - 2176'
   PRINT,''
   RETURN,mask
  ENDIF

  ;coords= intarr(4)  ; p1col,p1row,p2col,p2row
  ;READS, x, coords, format='(8z4)'
  IF (table EQ -1) THEN BEGIN ; occ mask
    occ_icer(tel,*)= coords 
  ENDIF ELSE BEGIN            ; roi mask
    roi_icer(tel,table,*)= coords 
  ENDELSE
  IF (MAX(coords) EQ 0) THEN RETURN, mask

  RETURN, ICER_MASK(coords)

END


FUNCTION X2B,x
  n= STRLEN(x)
  out= 0B
  FOR i=0, n-1 DO BEGIN
    CASE STRUPCASE(STRMID(x,i,1)) OF
      '0': b= BYTE([0,0,0,0])
      '1': b= BYTE([0,0,0,1])
      '2': b= BYTE([0,0,1,0])
      '3': b= BYTE([0,0,1,1])
      '4': b= BYTE([0,1,0,0])
      '5': b= BYTE([0,1,0,1])
      '6': b= BYTE([0,1,1,0])
      '7': b= BYTE([0,1,1,1])
      '8': b= BYTE([1,0,0,0])
      '9': b= BYTE([1,0,0,1])
      'A': b= BYTE([1,0,1,0])
      'B': b= BYTE([1,0,1,1])
      'C': b= BYTE([1,1,0,0])
      'D': b= BYTE([1,1,0,1])
      'E': b= BYTE([1,1,1,0])
      'F': b= BYTE([1,1,1,1])
    ENDCASE
    out= [out,b]
  ENDFOR
  out= out(1:*)
  RETURN,out
END


PRO READ_MASK_TABLES,fn, roi_mask, occ_mask, sc, verbose=verbose, NORECTIFY=norectify
  COMMON ICERMASKS  ;, occ_icer, roi_icer

  ; INPUT:
  ;  fn:  In flight Occulter or ROI mask table filename (i.e. '../TABLES/mask_tables.dat')

  ; OUTPUT:
  ;  mask: A byte array of 5x1024 for 5 cameras (1 mask table per camera)
  ;        Bits for each camera mask are set to 1 accordind to the iput. 

  RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ; => xyblks (34 blocks for a ccd size of 34*34=1156)

  occ_mask= BYTARR(5,xyblks*xyblks)  ; CCD mapped to 34x34 CCD square blocks.
  roi_mask= BYTARR(5,xyblks*xyblks,4)  ; CCD mapped to 34x34 CCD square blocks and 4 tables per tele.

  OPENR,lun, fn, /GET_LUN 
  print,'Reading ',fn

  tnames= ['EUVI','COR1','COR2','HI1','HI2']

  keep= '' 
  line=''
  filelines=''

  IF (DATATYPE(occ_icer) EQ 'UND') THEN BEGIN
    occ_icer= INTARR(5,4)
    roi_icer= INTARR(5,4,4)
  ENDIF

  WHILE NOT EOF(lun) DO BEGIN
    READF, lun, line
    filelines=[filelines,line]
    line= STRTRIM(line,2)
    IF (STRMID(line,0,1) EQ 'X') THEN BEGIN
      ; remove X and all blanks (only keep the data):
      keep= [keep, STRCOMPRESS(STRMID(line,1,500),/REMOVE_ALL)]
    ENDIF 
  ENDWHILE

  CLOSE,lun
  FREE_LUN,lun
help,filelines

    fst=2   ;Remove dummy first element and first Id line
    ;--Check to see if file has new extra first lines or not
    IF strpos(filelines[2],'generated 2009') GT 0 THEN fst=4
    filelines=filelines[fst:*]	
    
  keep= keep(1:*)
  lines= N_ELEMENTS(keep)
  IF (lines NE 200) THEN BEGIN
    PRINT,''
    PRINT,'ERROR - Each of 25 table MUST contain 8 lines of data in '+fn+' for a total of 200 lines.' 
    PRINT,'        Total data lines read = ',lines
    PRINT,''
    STOP
  ENDIF

  tables= lines/8 ; 8 lines of hex data per roi or occ table.

  ;tab_order= ['COR2OCC','COR2ROI1','COR2ROI2','COR2ROI3','COR2ROI4', $
  ;            'COR1OCC','COR1ROI1','COR1ROI2','COR1ROI3','COR1ROI4', $
  ;            'EUVIOCC','EUVIROI1','EUVIROI2','EUVIROI3','EUVIROI4', $
  ;            'HI2OCC' ,'HI2ROI1' ,'HI2ROI2' ,'HI2ROI3' ,'HI2ROI4' , $
  ;            'HI1OCC' ,'HI1ROI1' ,'HI1ROI2' ,'HI1ROI3' ,'HI1ROI4' ]

  mask_map= [2,1,0,4,3] ; 2=cor2, 1=cor1, 0=euvi, 4=hi2, 3=hi1 (in mask tables order is: 0,1,2,3,4).

  k= -1 
  FOR i= 0, tables/5 - 1 DO BEGIN
    tel= mask_map(i) 

    ;IF (KEYWORD_SET(verbose)) THEN PRINT,'READING OCC data for '+tnames(tel)+'...'
    k= k+1
    IF (KEYWORD_SET(verbose)) THEN PRINT,keep(k)
    IF (STRMID(keep(k),16,4) EQ '1CEA') THEN begin
      ;help,keep(k)
      coords= intarr(4)  ; p1col,p1row,p2col,p2row
      READS, keep(k), coords, format='(8z4)'
      data= ICER2BLK(coords,sc,tel,-1)
      k= k+7
    ENDIF ELSE BEGIN
    data= X2B(keep(k))
      FOR j= 1, 7 DO BEGIN 
        k= k+1
        IF (KEYWORD_SET(verbose)) THEN PRINT,keep(k)
        data= [data,X2B(keep(k))]
      ENDFOR
    ENDELSE
    occ_mask(tel,*)= data(0:1155) ; data has 73 words or 73*16 = 1168 bits but only 0-1155 are valid.

    FOR table= 0, 3 DO BEGIN
      IF (KEYWORD_SET(verbose)) THEN PRINT,'READING ROI'+STRTRIM(table+1,2)+' data for '+tnames(tel)+'...'

      k= k+1
      IF (KEYWORD_SET(verbose)) THEN PRINT,keep(k)
      IF (STRMID(keep(k),16,4) EQ '1CEA') THEN begin
        ;help,keep(k)
        coords= intarr(4)  ; p1col,p1row,p2col,p2row
        READS, keep(k), coords, format='(8z4)'
        data= ICER2BLK(coords,sc,tel,table)
        k= k+7
      ENDIF ELSE BEGIN
        data= X2B(keep(k))
        FOR j= 1, 7 DO BEGIN 
          k= k+1
          IF (KEYWORD_SET(verbose)) THEN PRINT,keep(k)
          data= [data,X2B(keep(k))]
        ENDFOR
      ENDELSE
      roi_mask(tel,*,table)= data(0:1155) ; data has 73 words or 73*16 = 1168 bits but only 0-1155 are valid.
    ENDFOR
  ENDFOR

  IF (KEYWORD_SET(verbose)) THEN BEGIN
    FOR i= 0, 4 DO BEGIN
      PRINT,occ_mask(i,*),FORMAT='('+STRTRIM(xyblks,2)+'Z2)'
      tmp= WHERE(occ_mask(i,*) EQ 1, cnt)
      PRINT, tnames(i)+' OCC Table. Number of Masked Blocks Set to 1 (above) = ' + STRTRIM(cnt,2)
      STOP 
      FOR j= 0, 3 DO BEGIN
        PRINT,roi_mask(i,*,j),FORMAT='('+STRTRIM(xyblks,2)+'Z2)'
        tmp= WHERE(roi_mask(i,*,j) EQ 1, cnt)
        PRINT, tnames(i)+' ROI'+STRTRIM(j+1,2)+ $
               ' Table. Number of Masked Blocks Set to 1 (above) = ' + STRTRIM(cnt,2)
        STOP
      ENDFOR
    ENDFOR
  ENDIF

  telstr= ['EUVI','COR1','COR2','HI1','HI2']

  IF not keyword_set(NORECTIFY) THEN FOR i=0,4 DO BEGIN
    ; Change from flight to ground and then to idl coordinates. The occ_mask and roi_mask
    ; are now euvi, cor1, cor2, hi1, and hi2 (not the order in mask file anymore):
    occ_mask(i,*)= MODIFY_MASK_ORDER(sc,telstr(i),occ_mask(i,*),/TABLE2IDL) ; change order to idl coordinates
    FOR j= 0, 3 DO $
      roi_mask(i,*,j)= MODIFY_MASK_ORDER(sc,telstr(i),roi_mask(i,*,J),/TABLE2IDL)  ; change order to idl coordinates
  ENDFOR

  RETURN
END

