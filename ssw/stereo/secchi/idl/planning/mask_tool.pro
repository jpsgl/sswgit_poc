;+
;$Id: mask_tool.pro,v 1.18 2014/04/14 19:26:33 mcnutt Exp $
;
; Project     : STEREO - SECCHI 
;                   
; Name        : MASK_TOOL
;               
; Purpose     : Stand alone widget tool to define a SECCHI Mask or ROI Table.
;               
; Explanation : This tool allows the user to display a SECCHI 
;               image and interactively select the blocks to be 
;		masked (ie. not transmitted). It also lets user
;               generate a mask table using latest changes. 
;               
; Use         : MASK_TOOL
;    
; Inputs      : None.
;               
; Opt. Inputs : None.
;               
; Outputs     : None.
;               
; Opt. Outputs: None.
;               
; Keywords    : None.
;
; Category    : Planning, Scheduling.
;               
; Prev. Hist. : Adapted from SOHO/LASCO. 
;
; Written by  : Ed Esfandiari, NRL, July 2004 - First Version. 
;               
; Modification History:
;              Ed Esfandiari Mar 07, 05 - Display pixel and block information. 
;              Ed Esfandiari Mar 08, 05 - Added decomposed=0 statement and cleaned-up.
;              Ed Esfandiari May 26, 05 - Use ccd depenedency orientaions. Read in SC/A
;                                         or SC/B mask (occulta* or occultb*) and generate
;                                         SC/A or SC/B flight masks.
;              Ed Esfandiari May 30, 06 - Added CCD 0 coordinate for each telescope.
;              Ed Esfandiari Jul 20, 06 - Added ;$Id as first line of a saved mask.
;              Ed Esfandiari Jun 18, 08 - Changed code to indicate ground coordinates display info.
;              Ed Esfandiari Sep 08, 08 - Added more ICER Help info.
;              Ed Esfandiari 02/12/09 -   Change plot coordinates to match Robin's chart.
;              Ed Esfandiari 02/17/09 -   Change 1-34 block labels according to ccd for sc/tel. 
;
; $Log: mask_tool.pro,v $
; Revision 1.18  2014/04/14 19:26:33  mcnutt
; added option to read in a gif file
;
; Revision 1.17  2013/10/10 23:16:07  nathan
; For unrectified input image use PCOL not RCOL; bin after putting in full image;
; create init_colortable subpro
;
; Revision 1.16  2013/10/10 15:54:15  mcnutt
; added binning option for images
;
; Revision 1.15  2013/10/09 22:11:45  nathan
; Display truecolor images if read in jpg (not tested with png); save separate
; fitsdir in common block; change default fitsdir to PT/IN/FTS; save masks
; in maskdir
;
; Revision 1.14  2013/10/08 17:42:42  nathan
; print unmasked percentage
;
; Revision 1.13  2013/10/03 14:40:19  mcnutt
; will read in a png or jpg you will be ask to select a fits file to use for the header
;
; Revision 1.12  2009/08/07 19:52:21  nathan
; retain comments from input file in output file
;
; Revision 1.11  2009/08/07 18:25:23  nathan
; retain mask and image directories in common block
;
; Revision 1.10  2009/02/17 21:34:31  esfand
; Change 1-34 block labels according to ccd for sc/tel - AEE
;
; Revision 1.9  2009/02/17 16:50:30  esfand
; display coordinate now matches un-rectified ccd depending on telescope - AEE
;
; Revision 1.8  2009/02/05 16:01:41  nathan
; moved Dismiss button up 3 rows because it did not appear on my screen
;
; Revision 1.7  2008/11/19 15:15:22  nathan
; make filters for filenames more general
;
; Revision 1.6  2008/07/14 18:32:42  nathan
; fixed history comment in header
;
; Revision 1.5  2008/06/20 18:06:57  esfand
; added ICER Mask
;
; Revision 1.4  2008/06/05 15:09:26  nathan
; use sccreadfits
;
; Revision 1.3  2008/06/04 15:27:05  nathan
; Incorporated mask_tool.pro,v 1.10 2006/10/31 18:50:41 esfand from PT/PRO:
; Added RETAIN=2 to draw widget; Added new block 0 CCD orientations.
;
; Revision 1.2  2007/03/30 22:01:31  thompson
; Add calls to allow_font() and get_font
;
; Revision 1.1  2006/08/28 18:15:49  nathan
; copied from PT/PRO
;
; Revision 1.8  2006/07/20 13:11:56  esfand
; added $Id
;
; Revision 1.6  2006/05/30 18:33:34  esfand
; Added CCD coordinates for each telescope
;
; Revision 1.5  2005/05/26 20:00:58  esfand
; PT version used to create SEC20050525005 TVAC schedule
;
; Revision 1.4  2005/03/08 17:07:10  esfand
; more clean-up
;
; Revision 1.3  2005/03/08 15:57:49  esfand
; added decompsed=0 and cleaned-up
;
; Revision 1.2  2005/03/07 21:38:54  esfand
; added pixel and block info
;
; Revision 1.1  2004/09/01 15:40:44  esfand
; commit new version FOR Nathan.
;
;-


;__________________________________________________________________________________________________________
;

@read_mask_tables.pro

PRO init_colortable

    LOADCT, 0

; TVLCT, 255,0,0, 1      ;** load red into entry 1
; TVLCT, 0,255,0, 2      ;** load green into entry 2
; TVLCT, 142,229,238, 3  ;** load blue into entry 3
; TVLCT, 255,255,0,4     ;** load yellow into entry 4
; TVLCT, 200,0,0, 5      ;** load dull red into entry 5
; TVLCT, 0,200,0, 6      ;** load dull green into entry 6
; TVLCT, 0,206,237, 7    ;** load dull blue into entry 7
; TVLCT, 200,200,0,8     ;** load dull yellow into entry 8
; TVLCT, 142,229,238, 9  ;** load light blue into entry 9 - used by mask.pro

; AEE changed here and in schedule.pro and mask_old.pro

 TVLCT, 255,0,0, 1      ;** load red into entry 1
 TVLCT, 0,255,0, 2      ;** load green into entry 2
 TVLCT, 142,229,238, 3  ;** load blue into entry 3
 TVLCT, 255,255,0,4     ;** load yellow into entry 4
 TVLCT, 204,117,89, 5   ;** load brown  into entry 5
 TVLCT, 200,0,0, 6      ;** load dull red into entry 6
 TVLCT, 0,200,0, 7      ;** load dull green into entry 7
 TVLCT, 0,206,237, 8    ;** load dull blue into entry 8
 TVLCT, 200,200,0, 9    ;** load dull yellow into entry 9
 TVLCT, 180,95,85, 10   ;** load dull dark brown into entry 10
 TVLCT, 142,229,238, 11  ;** load light blue into entry 11 - used by mask.pro

end

PRO UPDATE_ICER,maskt,occ_icer,roi_icer
;help,occ_icer,roi_icer,maskt.type,maskt.tele,maskt.table

  IF (maskt.type EQ 'ROI') THEN $
    coords= roi_icer(maskt.tele,maskt.table-1,*) $
  ELSE $
    coords= occ_icer(maskt.tele,*)

;print,coords

  WIDGET_CONTROL, maskt.p1col, SET_VALUE= STRTRIM(coords(0),2)
  WIDGET_CONTROL, maskt.p1row, SET_VALUE= STRTRIM(coords(1),2)
  WIDGET_CONTROL, maskt.p2col, SET_VALUE= STRTRIM(coords(2),2)
  WIDGET_CONTROL, maskt.p2row, SET_VALUE= STRTRIM(coords(3),2)
  RETURN
END

PRO APPLY_MASK,maskt,roi_blocks,occ_blocks,xyblks
  WIDGET_CONTROL, maskt.p1col, GET_VALUE= p1col
  ;print, p1col
  WIDGET_CONTROL, maskt.p1row, GET_VALUE= p1row
  ;print, p1row
  WIDGET_CONTROL, maskt.p2col, GET_VALUE= p2col
  ;print, p2col
  WIDGET_CONTROL, maskt.p2row, GET_VALUE= p2row
  ;print, p2row
  IF (maskt.type EQ 'ROI') THEN $
    tab= maskt.table-1 $
  ELSE $
    tab= -1 
  blocks= ICER2BLK([p1col,p1row,p2col,p2row],maskt.sc,maskt.tele,tab)
  telstr= ['EUVI','COR1','COR2','HI1','HI2']
  blocks= MODIFY_MASK_ORDER(maskt.sc,telstr(maskt.tele),blocks,/TABLE2IDL) ; change order to idl coordinates
  maskt.blocks= blocks
  IF (maskt.type EQ 'ROI') THEN $
    roi_blocks(maskt.tele,*,tab)= maskt.blocks $
  ELSE $
    occ_blocks(maskt.tele,*)= maskt.blocks 

  ; Erase and Update the display:
  ; erase and add grids:
  ERASE, COLOR=maskt.gcol
  ; clear blocks currently displayed:
  WSET, maskt.new_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
  WSET, maskt.new_gmap & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
  FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
  FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
  ; update:
  selected = WHERE(maskt.blocks EQ 1, ncomplement=nunmasked)
  ; masked blocks are 1?
  print,float(nunmasked)/(32.*32),' of 2048x2048 unmasked'
  IF (selected(0) NE -1) THEN BEGIN
    FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
      yc = selected(i)/xyblks*maskt.bs
      xc = (selected(i) - selected(i)/xyblks*xyblks)*maskt.bs
      WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
      WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
    ENDFOR
  ENDIF
  WSET, maskt.draw_win
  IF (maskt.grid) THEN $
    DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
  ELSE $
    DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
  ADD_BLOCK_LABELS,maskt
  RETURN
END


PRO ADD_BLOCK_LABELS,maskt
    case maskt.sc of
      'A': begin 
         case maskt.tele of
           0: begin ; euvi
                FOR i=0,33 DO XYOUTS,maskt.ms-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
                FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(34-i,2),/DEV
              end
           1: begin ; cor1
               FOR i=0,33 DO XYOUTS,5,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(i+1,2),/DEV
              end
           2: begin ; cor2
               FOR i=0,33 DO XYOUTS,maskt.ms-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(34-i,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.bs-10,STRTRIM(34-i,2),/DEV
              end
           3: begin ;hi-1
               FOR i=0,33 DO XYOUTS,maskt.bs-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(34-i,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.bs-10,STRTRIM(i+1,2),/DEV
              end
           4: begin ;hi-2
               FOR i=0,33 DO XYOUTS,maskt.bs-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(34-i,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.bs-10,STRTRIM(i+1,2),/DEV
              end
         endcase
        end 
      'B': begin 
         case maskt.tele of
           0: begin ; euvi
               FOR i=0,33 DO XYOUTS,5,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(i+1,2),/DEV
              end
          1: begin ; cor1
               FOR i=0,33 DO XYOUTS,maskt.ms-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(34-i,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.bs-10,STRTRIM(34-i,2),/DEV
             end
          2: begin ; cor2
               FOR i=0,33 DO XYOUTS,5,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(i+1,2),/DEV
             end
          3: begin ;hi-1
               FOR i=0,33 DO XYOUTS,maskt.ms-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(34-i,2),/DEV
             end
          4: begin ;hi-2
               FOR i=0,33 DO XYOUTS,maskt.ms-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
               FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(34-i,2),/DEV
             end
        endcase
       end 
    endcase
  RETURN
END


PRO CHANGE_COORD_LABELS,maskt

    blank= '                                   '
    blank1='                     '
    case maskt.sc of
      'A': begin 
         case maskt.tele of
           0: begin ; euvi
                WIDGET_CONTROL,maskt.nt, SET_VALUE= blank+blank+'   N'+blank1+blank1+blank1+'(1,1)'
                WIDGET_CONTROL,maskt.ns, SET_VALUE= '(2176,2176)'+blank+blank1+'S'+blank+blank+blank
              end
           1: begin ; cor1
                WIDGET_CONTROL,maskt.nt, SET_VALUE= '(1,1)'+blank+blank1+'          N'+blank+blank+blank
                WIDGET_CONTROL,maskt.ns, SET_VALUE= blank+blank+'   S'+blank+blank1+'(2176,2176)'
              end
           2: begin ; cor2
                WIDGET_CONTROL,maskt.nt, SET_VALUE= '(2176,2176)'+blank+blank1+'N'+blank+blank+blank
                WIDGET_CONTROL,maskt.ns, SET_VALUE= blank+blank+'   S'+blank+blank1+'       (1,1)'
              end
           3: begin ;hi-1
                WIDGET_CONTROL,maskt.nt, SET_VALUE= blank+blank+'   N'+blank+blank1+'(2176,2176)'
                WIDGET_CONTROL,maskt.ns, SET_VALUE= '(1,1)'+blank+blank1+'          S'+blank+blank+blank
              end
           4: begin ;hi-2
                WIDGET_CONTROL,maskt.nt, SET_VALUE= blank+blank+'   N'+blank+blank1+'(2176,2176)'
                WIDGET_CONTROL,maskt.ns, SET_VALUE= '(1,1)'+blank+blank1+'          S'+blank+blank+blank
              end
         endcase
        end 
      'B': begin 
         case maskt.tele of
           0: begin ; euvi
                WIDGET_CONTROL,maskt.nt, SET_VALUE= '(1,1)'+blank+blank1+'          N'+blank+blank+blank
                WIDGET_CONTROL,maskt.ns, SET_VALUE= blank+blank+'   S'+blank+blank1+'(2176,2176)'
              end
          1: begin ; cor1
               WIDGET_CONTROL,maskt.nt, SET_VALUE= '(2176,2176)'+blank+blank1+'N'+blank+blank+blank
               WIDGET_CONTROL,maskt.ns, SET_VALUE= blank+blank+'   S'+blank+blank1+'       (1,1)'
             end
          2: begin ; cor2
               WIDGET_CONTROL,maskt.nt, SET_VALUE= '(1,1)'+blank+blank1+'          N'+blank+blank+blank
               WIDGET_CONTROL,maskt.ns, SET_VALUE= blank+blank+'   S'+blank+blank1+'(2176,2176)'
             end
          3: begin ;hi-1
               WIDGET_CONTROL,maskt.nt, SET_VALUE= blank+blank+'   N'+blank1+blank1+blank1+'(1,1)'
               WIDGET_CONTROL,maskt.ns, SET_VALUE= '(2176,2176)'+blank+blank1+'S'+blank+blank+blank
             end
          4: begin ;hi-2
               WIDGET_CONTROL,maskt.nt, SET_VALUE= blank+blank+'   N'+blank1+blank1+blank1+'(1,1)'
               WIDGET_CONTROL,maskt.ns, SET_VALUE= '(2176,2176)'+blank+blank1+'S'+blank+blank+blank
             end
        endcase
       end 
    endcase

  RETURN
END

PRO print_mask_comments
; We will always write masks in the same order, so only need to keep track of 
; where we are in the file, using flpos.

    COMMON ICERMASKS, occ_icer, roi_icer, filelines, slun, flpos

    ;help,filelines
    nf=n_elements(filelines)
    IF flpos GT nf-1 THEN stop
    
    WHILE strmid(filelines[flpos],0,1) EQ 'X' DO flpos=flpos+1
    WHILE strmid(filelines[flpos],0,1) EQ ';' DO BEGIN
	printf,slun,filelines[flpos] 
	;print,flpos,'   ',filelines[flpos]
	flpos=flpos+1
    ENDWHILE

END

PRO MASK_TOOL_EVENT, event

COMMON MASK, maskt, occ_blocks, roi_blocks, maskdir, imagedir, fitsdir
COMMON ICERMASKS

    RESTORE, GETENV('PT')+'/IN/OTHER/ccd_size.sav' ;=> xyblks,xyblkpix,xypix,xstart,ystart

    CASE (event.id) OF

	maskt.prnt : BEGIN
           blks= maskt.blocks
           selected = WHERE(blks EQ 1, num)
           chng_arr= INTARR(xyblks*xyblks)
           IF (num GT 0) THEN BEGIN
             ;change= MODIFY_MASK_ORDER(selected) ; change order of selection from idl to mask_table
             telstr= ['EUVI','COR1','COR2','HI1','HI2']
             change= MODIFY_MASK_ORDER(maskt.sc,telstr(maskt.tele),selected) ; change order of selection from idl to mask_table
             chng_arr(change)= 1
           ENDIF
           IF (DATATYPE(mdiag) NE 'UND') THEN $
             WIDGET_CONTROL,mdiag,SET_VALUE='Number of blocks selected = '+STRN(num)
           print,''
	   PRINT, 'Mask Array:'
           ;PRINT, chng_arr, FORMAT='(34Z2)'  ; print the 1156 element mask array as a 34x34 square.
           ; print to the screen the wat un-rectified images on the ground is with 0,0 in the bottom left:
           FOR i=33,0,-1 Do PRINT, chng_arr(i*34:i*34+33), FORMAT='(34Z2)'  ; print the 1156 element mask array as a 34x34 square.
           PRINT, 'Number of blocks selected = ', num
             
           out = FIND_DATA_RUNS(INDGEN(xyblks*xyblks),chng_arr,0,0,1)
           str = 'Blocks = ('
           FOR i=0, (N_ELEMENTS(out)/2)-1 DO BEGIN
              str = str + STRTRIM(STRING(out(i,0)),2) + '-' + STRTRIM(STRING(out(i,1)),2) + ','
           ENDFOR
           str = STRMID(str,0,STRLEN(str)-1) + ')'
           PRINT, str
	END

        maskt.sc_pd : BEGIN
           maskt.sc_id= event.index
           IF (event.index EQ 0) THEN maskt.sc= 'A'
           IF (event.index EQ 1) THEN maskt.sc= 'B' 

           CHANGE_COORD_LABELS,maskt

           WIDGET_CONTROL, maskt.ccd0, SET_VALUE= maskt.tele_coord(maskt.tele+maskt.sc_id*5)+'   (use for entering ICER Mask coordinates 1-2176)'
           apply_mask,maskt,roi_blocks,occ_blocks,xyblks
        END

        maskt.savemask : BEGIN
           telstr= ['EUVI','COR1','COR2','HI1','HI2']
           GET_UTC, sdate, /CCSDS

           ;fname= './' + telstr(maskt.tele)+'_' + $
           ;       STRMID(maskt.type,0,3) + STRTRIM(maskt.table+1,2) + '_occult_' + sdate
           ;OPENW,slun,fname+'.img',/GET_LUN
         
           IF (maskt.sc EQ 'A') THEN $
             fname= concat_dir(maskdir,'occulta_' + sdate+'.img') $
           ELSE $
             fname= concat_dir(maskdir,'occultb_' + sdate+'.img')
  
           OPENW,slun,fname,/GET_LUN
	   flpos=0  ; This is used in PRO print_mask_comments

            fid=';$Id: mask_tool.pro,v 1.18 2014/04/14 19:26:33 mcnutt Exp $'
	    PRINTF,slun, fid
    	    ; NOTE: This will get replaced with occulter file values if committed.
	    printf,slun,'; Mask file generated '+sdate+' by '+strmid(fid,5,21)
	    printf,slun,'; '
           ;blks= maskt.blocks

           tel_order= [2,1,0,4,3] ; cor2, cor1, euvi, hi2, hi1
           FOR tl= 0, 4 DO BEGIN
	    FOR blk_sec= 0, 4 DO BEGIN
             IF (blk_sec EQ 0) THEN BEGIN 
               blks= occ_blocks(tel_order[tl],*) 
               mtype= 'OCC'
               icer_coords= occ_icer(tel_order(tl),*)
             ENDIF ELSE BEGIN
               blks= roi_blocks(tel_order(tl),*,blk_sec-1)
               mtype= 'ROI'+STRTRIM(blk_sec,2)
               icer_coords= roi_icer(tel_order(tl),blk_sec-1,*)
             ENDELSE
             
	     print_mask_comments
	     
           IF (MAX(icer_coords) GT 0) THEN BEGIN
	   ;**** ICER Subfield ****
              ; If any of masks has icer coords, then save it at such:
	      stop
               hc= STRING(icer_coords,'(16Z4.4)')
               hex= 'X'
               FOR ih=0,7 DO hex= hex+STRMID(hc,ih*2,2)+' '
               hex= hex+'1C EA 00 00 00 00 00 00 00 00 00 00'
               PRINTF,slun,hex
               hex='X00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00'
               FOR ih=0,5 DO PRINTF,slun,hex
               hex='X00 00 00 00 00 00'
               PRINTF,slun,hex
           ENDIF ELSE BEGIN             
	   ;**** Rice Mask ****
             new_mask= INTARR(xyblks*xyblks)
             selected = WHERE(blks EQ 1, num)
             IF (num GT 0) THEN BEGIN
               ;change= MODIFY_MASK_ORDER(selected) ; change order of selection from idl to mask_table
               change= MODIFY_MASK_ORDER(maskt.sc,telstr(tel_order(tl)),selected) ; change order of selection from idl to mask_table
               new_mask(change)= 1
             ENDIF

             ;PRINTF,slun, '; '+telstr(tel_order(tl))+' '+mtype+' bit mask values'

             val = [8,4,2,1]
             hex= BYTARR(40)
             FOR i= 0, 6 DO BEGIN
               b= new_mask(i*160:i*160+159)
               FOR j= 0, 39 DO BEGIN
                 b4= b(j*4:j*4+3)
                 w = WHERE(b4 EQ 1, wcnt)
                 IF (wcnt GT 0) THEN $
                   hex(j)= FIX(TOTAL(val(w))) $
                 ELSE $
                   hex(j)= 0
               ENDFOR
               ;PRINT,hex,FORMAT='(40Z1)'
               PRINTF,slun, 'X',hex,FORMAT='(A,20(2Z1,1x))'
             ENDFOR
             b= new_mask(i*160:1155) ; last line has only 6 bytes (6*8=48 characters) but only 
                                     ; first 36 chars are present in the mask (1120:1155).
             hex= BYTARR(12)
             FOR j= 0, 8 DO BEGIN
               b4= b(j*4:j*4+3)
               w = WHERE(b4 EQ 1, wcnt)
               IF (wcnt GT 0) THEN $
                 hex(j)= FIX(TOTAL(val(w))) $
               ELSE $
                 hex(j)= 0
             ENDFOR
             PRINTF,slun, 'X',hex,FORMAT='(A,6(2Z1,1x))'
           ENDELSE ;not icer_coords
            ENDFOR ; mask sections
           ENDFOR ; telescopes

           CLOSE,slun
           FREE_LUN, slun

           ;PRINT,'Saved mask to ' + fname + '.img'
           print,''
	   PRINT,'Saved mask to ' + fname
           WIDGET_CONTROL, maskt.sfn, SET_VALUE= 'Saved as ' + fname
           
        END


        maskt.showgrid : BEGIN
           WSET, maskt.draw_win
           CASE (maskt.grid) OF
              0 : BEGIN		;SHOW GRID
                 WIDGET_CONTROL, maskt.showgrid, SET_VALUE=maskt.grid_types(1)        ;** change labels
                 DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap]
                 maskt.grid = 1

                 ADD_BLOCK_LABELS,maskt

              END
              1 : BEGIN		;HIDE GRID
                 WIDGET_CONTROL, maskt.showgrid, SET_VALUE=maskt.grid_types(0)        ;** change labels
                 DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
                 maskt.grid = 0
              END
              ELSE : BEGIN
              END
           ENDCASE
        END

        maskt.loadmask : BEGIN
    	    IF datatype(maskdir) NE 'STR' THEN maskdir=GETENV_SLASH('TABLES')
    	    
	    newfile = PICKFILE(/READ, FILTER='*occ*.img*', PATH=maskdir , $
                              /FIX_FILTER, /NOCONFIRM, /MUST_EXIST)
           IF (newfile NE '') THEN BEGIN
             break_file,newfile,di,pa,fi,sf
	     maskdir=di+pa
	     WIDGET_CONTROL, /HOUR
             ;READ_MASK_TABLES,newfile,roi_blocks,occ_blocks,maskt.sc
             IF (STRPOS(newfile(0),'occulta') GE 0) THEN $
               READ_MASK_TABLES,newfile,roi_blocks,occ_blocks,'A' $
             ELSE $
               READ_MASK_TABLES,newfile,roi_blocks,occ_blocks,'B'

             IF (maskt.type EQ 'OCCULTER') THEN $
               maskt.blocks = occ_blocks(maskt.tele,*) $
             ELSE $
               maskt.blocks = roi_blocks(maskt.tele,*,maskt.table-1)

             WSET, maskt.draw_win
             ERASE, COLOR=maskt.gcol
             FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
             FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0

             selected = WHERE(maskt.blocks EQ 1)
             IF (selected(0) NE -1) THEN BEGIN
               FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
                 yc = selected(i)/xyblks*maskt.bs
                 xc = (selected(i) - selected(i)/xyblks*xyblks)*maskt.bs
                 WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
                 WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
               ENDFOR
             ENDIF
             ;LOADCT, 0, /SILENT
             ;TVLCT, 142, 229, 238, maskt.gcol
             WSET, maskt.draw_win 
             IF (maskt.grid) THEN $
               DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
             ELSE $
               DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
             toks= STR_SEP(newfile,'/')
             WIDGET_CONTROL, maskt.msk_lab, SET_VALUE= toks(N_ELEMENTS(toks)-1) 
             ADD_BLOCK_LABELS,maskt
           ENDIF
           ; Erase and Update the display:
           ; erase and add grids:
           ERASE, COLOR=maskt.gcol
           ; clear blocks currently displayed:
           WSET, maskt.new_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           WSET, maskt.new_gmap & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
           FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
           ; update:
           selected = WHERE(maskt.blocks EQ 1)
           IF (selected(0) NE -1) THEN BEGIN
             FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
               yc = selected(i)/xyblks*maskt.bs
               xc = (selected(i) - selected(i)/xyblks*xyblks)*maskt.bs
               WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
               WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
             ENDFOR
           ENDIF
           WSET, maskt.draw_win
           IF (maskt.grid) THEN $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
           ELSE $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
           ADD_BLOCK_LABELS,maskt
           UPDATE_ICER,maskt,occ_icer,roi_icer
        END
 
        maskt.loadimage : BEGIN
           telstr=''
           CASE maskt.tele OF
             0: telstr='*eu'
             1: telstr='*c*1'
             2: telstr='*c*2'
             3: telstr='*h*1'
             4: telstr='*h*2'
           ENDCASE
	   help,telstr
             telstr=''
  	    
	    IF datatype(imagedir) NE 'STR' THEN BEGIN
	    ;	imagedir=GETENV('PT')+'/IN/FTS/'
                imagedir=getenv('secchi')+'/lz/L0/'
		fitsdir=imagedir
            ENDIF
	  ; newfile = PICKFILE(/READ, FILTER=telstr+'*.fts', PATH= imagedir, $
          ;                    /FIX_FILTER, /NOCONFIRM, /MUST_EXIST)
           newfile = PICKFILE(/READ,  PATH= imagedir, /NOCONFIRM, /MUST_EXIST)
           IF (newfile NE '') THEN BEGIN
             break_file,newfile,di,pa,fi,sf
	     imagedir=di+pa
             WIDGET_CONTROL, /HOUR
             WSET, maskt.draw_win
             ERASE, COLOR=maskt.gcol
    	    binning=widget_info(maskt.bimg,/droplist_select)

    	    if sf eq '.fts' then begin
    	    	secchi_prep,newfile,hdr,img0,/FULL,/DESMEAR_OFF, /nocalfac
	      ; Always trim images so it is treated the same as PNGs or JPGs
    	    	;std = STDEV(img0, mdn)
		mdn=median(img0)
    	    	bimg = BYTSCL(img0, 0<mdn, mdn*3, TOP=255-10)		;** leave 10 colors unallocated
              	;img = BYTSCL(img, min=0, max=MAX(img)/3, TOP=255-10)	;** leave 10 colors unallocated
              	bimg = bimg + 10
		; Now img is same as jpg/png case.
	      	init_colortable
		
    	    endif else begin
               hdrfile = PICKFILE(/READ,  FILTER=telstr+'*.fts', /FIX_FILTER, PATH= fitsdir, $
                               /NOCONFIRM, /MUST_EXIST, WRITE='Select fits file to use for header',TITLE='Select fits file to use for header')
               fitsimg=sccreadfits(hdrfile,hdr)
               if sf eq '.jpg' then READ_JPEG, newfile, bimg,  clrtable, COLORS=!D.N_COLORS-1,/TWO_PASS_QUANTIZE,/DITHER
               if sf eq '.png' then bimg=read_png(newfile,r,g,b)
                if sf eq '.gif' then read_gif,newfile,bimg,r,g,b
   	    	
	    ENDELSE
	    
 		; We assume all JPGs and PNGs are trimmed and rectified (and corrected for roll, but ignored for now)
    	    	; Unbin image
    	    fimg=rebin(bimg,2048,2048,/sample) 
	        ; Unrectify image, then put into un-trimmed array
    	    if hdr.rectrota ne 0 then begin
    	    	if hdr.rectrota le 3 then $
		    rimg=rotate(fimg,4-hdr.rectrota) else $
    	    	    rimg=rotate(fimg,4-(hdr.rectrota-4)+4)	          
    	    endif else rimg=fimg
    	    tfimg = bytarr(2176,2176)+10
;               img[hdr.r1col-1:hdr.r2col-1,hdr.r1row-1:hdr.r2row-1]=rimg
    	    tfimg[hdr.p1col-1,hdr.p1row-1]=rimg
	       
	       ; Apply binning and re-insert into full array
    	    ffimg = bytarr(2176,2176)+10
    	    ffimg[0,0] = rebin(tfimg,2176/(2^binning),2176/(2^binning),/sample)

    	    	; Re-rectify image
    	    if hdr.rectrota ne 0 then rimg=rotate(ffimg,hdr.rectrota) else rimg=ffimg
    	    img = CONGRID(rimg, maskt.ms, maskt.ms)

    	     help,clrtable,g
	     IF datatype(clrtable) NE 'UND' THEN tvlct,clrtable
	     IF datatype(g) NE 'UND' THEN tvlct,r,g,b

             WSET, maskt.img_map
             TV, img
             WSET, maskt.mask_map
             temp = INTARR(maskt.ms,maskt.ms) + maskt.gcol
             TV, temp
             WSET, maskt.new_map
             TV, img
             WSET, maskt.new_gmap
             TV, img
 
             FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
             FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0

             selected = WHERE(maskt.blocks EQ 1)
             IF (selected(0) NE -1) THEN BEGIN
               FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
                 yc = selected(i)/xyblks*maskt.bs
                 xc = (selected(i) - selected(i)/xyblks*xyblks)*maskt.bs
                 WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
                 WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
               ENDFOR
             ENDIF
            ; LOADCT, 0, /SILENT

             TVLCT, 142, 229, 238, maskt.gcol
             WSET, maskt.draw_win 
             IF (maskt.grid) THEN $
               DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
             ELSE $
               DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]

             ADD_BLOCK_LABELS,maskt
             toks= STR_SEP(newfile,'/')
 	     ;IF datatype(clrtable) NE 'UND' THEN tvlct,clrtable
            WIDGET_CONTROL, maskt.img_lab, SET_VALUE= toks(N_ELEMENTS(toks)-1) 

           ENDIF
        END

	maskt.invert : BEGIN
           WIDGET_CONTROL, /HOUR
           WSET, maskt.new_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           WSET, maskt.new_gmap & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
           FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
           notselected = WHERE(maskt.blocks NE 1)
           newselected = notselected
           maskt.blocks(*) = 0
           IF (newselected(0) NE -1) THEN BEGIN
             maskt.blocks(newselected) = 1
             FOR i=0,N_ELEMENTS(newselected)-1 DO BEGIN
               yc = newselected(i)/xyblks*maskt.bs
	       xc = (newselected(i) - newselected(i)/xyblks*xyblks)*maskt.bs
               WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
               WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
             ENDFOR
           ENDIF
           WSET, maskt.draw_win 
           IF (maskt.grid) THEN BEGIN 
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] 
             FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], $
                 /DEVICE, COLOR=0
             FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], $
                 /DEVICE, COLOR=0
           ENDIF ELSE BEGIN 
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
           ENDELSE
           ADD_BLOCK_LABELS,maskt
           IF (maskt.type EQ 'OCCULTER') THEN occ_blocks(maskt.tele,*) = maskt.blocks ELSE $
           IF (maskt.type EQ 'ROI') THEN roi_blocks(maskt.tele,*,maskt.table-1) = maskt.blocks
	END

	;maskt.clear : BEGIN
        ;   maskt.blocks(*) = 0
        ;   WSET, maskt.new_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
        ;   WSET, maskt.new_gmap & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
        ;   FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
        ;   FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
        ;   WSET, maskt.draw_win 
        ;   IF (maskt.grid) THEN $
        ;     DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
        ;   ELSE $
        ;     DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
        ;   IF (maskt.type EQ 'OCCULTER') THEN occ_blocks(maskt.tele,*) = maskt.blocks ELSE $
        ;   IF (maskt.type EQ 'ROI') THEN roi_blocks(maskt.tele,*,maskt.table-1) = maskt.blocks
	;END

	maskt.draw_w : BEGIN	;** event in draw window
          ;help,event.press,event.x,event.y
          x = 0 > event.x < (maskt.ms-1)
          y = 0 > event.y < (maskt.ms-1)
          bf = FLOAT(xyblks*xyblks)/maskt.ms                        ;** block factor
          xb= FIX(bf*x/xyblks)
          yb= 33-FIX(bf*y/xyblks)
          xyb= xb+yb*xyblks

                   case maskt.sc of
                    'A': begin 
                         case maskt.tele of
                          0: begin ; euvi
                              iyb= 1 > (yb+1)*64 < 2176
                              ixb= 1 > (33-xb+1)*64 < 2176
                            end
                          1:  begin ; cor1
                               iyb= 1 > (yb+1)*64 < 2176
                               ixb= 1 > (xb+1)*64 < 2176
                            end
                          2: begin ; cor2
                               iyb= 1 > (33-yb+1)*64 < 2176
                               ixb= 1 > (33-xb+1)*64 < 2176
                            end
                          3: begin ;hi-1
                               
                               iyb= 1 > (xb+1)*64 < 2176
                               ixb= 1 > (33-yb+1)*64 < 2176
                           end
                          4: begin ;hi-2
                               iyb= 1 > (xb+1)*64 < 2176
                               ixb= 1 > (33-yb+1)*64 < 2176
                           end
                         endcase
                       end 
                    'B': begin 
                         case maskt.tele of
                          0: begin ; euvi
                              iyb= 1 > (yb+1)*64 < 2176
                              ixb= 1 > (xb+1)*64 < 2176
                            end
                          1:  begin ; cor1
                               iyb= 1 > (33-yb+1)*64 < 2176
                               ixb= 1 > (33-xb+1)*64 < 2176
                            end
                          2: begin ; cor2
                               iyb= 1 > (yb+1)*64 < 2176
                               ixb= 1 > (xb+1)*64 < 2176
                            end
                          3: begin ;hi-1
                               iyb= 1 > (33-xb+1)*64 < 2176
                               ixb= 1 > (yb+1)*64 < 2176
                           end
                          4: begin ;hi-2
                               iyb= 1 > (33-xb+1)*64 < 2176
                               ixb= 1 > (yb+1)*64 < 2176
                           end
                         endcase
                       end 
                   endcase
                   xb= ixb/64 - 1
                   yb= iyb/64 - 1
                   xyb= xb*xyblks + yb

          WIDGET_CONTROL,maskt.cpix,SET_VALUE= $
            'Pcol='+STRING(yb*64+1,'(i4.4)')+'-'+STRING(yb*64+63+1,'(i4.4)')+$
            ',Prow='+STRING(xb*64+1,'(i4.4)')+'-'+STRING(xb*64+63+1,'(i4.4)')

           ; 'Xpix='+STRING(xb*64+1,'(i4.4)')+'-'+STRING(xb*64+63+1,'(i4.4)')+$
           ; ',Ypix='+STRING(yb*64+1,'(i4.4)')+'-'+STRING(yb*64+63+1,'(i4.4)')

          WIDGET_CONTROL,maskt.cursr,SET_VALUE= $
            'Block# '+STRING(xyb+1,format='(i4.4)')+' (C='+STRING(yb+1,'(i2.2)')+',R='+STRING(xb+1,'(i2.2)')+')'
            ;'Block# '+STRING(xyb+1,format='(i4.4)')+' (X='+STRING(xb+1,'(i2.2)')+',Y='+STRING(yb+1,'(i2.2)')+')'

           IF (event.press NE 0) THEN maskt.press = event.press
           IF (event.release NE 0) THEN maskt.press = 0B
           IF (maskt.press NE 0) THEN BEGIN	;** button select
              ;x = 0 > event.x < (maskt.ms-1)
              ;y = 0 > event.y < (maskt.ms-1)
              ;;bf = 1024./maskt.ms			;** block factor
              ;bf = FLOAT(xyblks*xyblks)/maskt.ms			;** block factor
              block = FIX(bf*x/xyblks) + FIX(bf*y/xyblks)*xyblks
              yc = block/xyblks*maskt.bs
	      xc = (block - block/xyblks*xyblks)*maskt.bs
              ;xb= FIX(bf*x/xyblks)
              ;yb= 33-FIX(bf*y/xyblks)
              ;xyb= xb+yb*xyblks
              CASE (maskt.press) OF
                 1B: BEGIN	;** Left mouse button; select block
                   WIDGET_CONTROL,maskt.cursr,SET_VALUE= $
                     'Block# '+STRING(xyb+1,format='(i4.4)')+' (C='+STRING(yb+1,'(i2.2)')+',R='+STRING(xb+1,'(i2.2)')+')'
                     ;'Block# '+STRING(xyb+1,format='(i4.4)')+' (X='+STRING(xb+1,'(i2.2)')+',Y='+STRING(yb+1,'(i2.2)')+')'


;                   case maskt.sc of
;                    'A': begin 
;                         case maskt.tele of
;                          0: begin ; euvi
;                              iyb= 1 > (yb+1)*64 < 2176
;                              ixb= 1 > (33-xb+1)*64 < 2176
;                            end
;                          1:  begin ; cor1
;                               iyb= 1 > (yb+1)*64 < 2176
;                               ixb= 1 > (xb+1)*64 < 2176
;                            end
;                          2: begin ; cor2
;                               iyb= 1 > (33-yb+1)*64 < 2176
;                               ixb= 1 > (33-xb+1)*64 < 2176
;                            end
;                          3: begin ;hi-1
;                               
;                               iyb= 1 > (xb+1)*64 < 2176
;                               ixb= 1 > (33-yb+1)*64 < 2176
;                           end
;                          4: begin ;hi-2
;                               iyb= 1 > (xb+1)*64 < 2176
;                               ixb= 1 > (33-yb+1)*64 < 2176
;                           end
;                         endcase
;                       end 
;                    'B': begin 
;                         case maskt.tele of
;                          0: begin ; euvi
;                              iyb= 1 > (yb+1)*64 < 2176
;                              ixb= 1 > (xb+1)*64 < 2176
;                            end
;                          1:  begin ; cor1
;                               iyb= 1 > (33-yb+1)*64 < 2176
;                               ixb= 1 > (33-xb+1)*64 < 2176
;                            end
;                          2: begin ; cor2
;                               iyb= 1 > (yb+1)*64 < 2176
;                               ixb= 1 > (xb+1)*64 < 2176
;                            end
;                          3: begin ;hi-1
;                               iyb= 1 > (33-xb+1)*64 < 2176
;                               ixb= 1 > (yb+1)*64 < 2176
;                           end
;                          4: begin ;hi-2
;                               iyb= 1 > (33-xb+1)*64 < 2176
;                               ixb= 1 > (yb+1)*64 < 2176
;                           end
;                         endcase
;                       end 
;                   endcase
;
;;
;;                   help,xb,yb
;;                   iyb= 1 > (yb+1)*64 < 2176
;;                   ixb= 1 > (33-xb+1)*64 < 2176

                   if (maskt.icer_mouse eq 1) then begin
                     WIDGET_CONTROL, maskt.p1col, SET_VALUE= STRTRIM(iyb,2)
                     WIDGET_CONTROL, maskt.p1row, SET_VALUE= STRTRIM(ixb,2)
                   endif 
                   if (maskt.icer_mouse eq 2) then begin
                     WIDGET_CONTROL, maskt.p2col, SET_VALUE= STRTRIM(iyb,2)
                     WIDGET_CONTROL, maskt.p2row, SET_VALUE= STRTRIM(ixb,2)
                   endif
                   maskt.icer_mouse=0 ; unset it

                    maskt.blocks(block) = 1		
                    WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
                    WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
                    IF (maskt.type EQ 'OCCULTER') THEN occ_blocks(maskt.tele,*) = maskt.blocks ELSE $
                    IF (maskt.type EQ 'ROI') THEN roi_blocks(maskt.tele,*,maskt.table-1) = maskt.blocks
                 END
                 2B: BEGIN 	;** Middle mouse button; de-select block
                   WIDGET_CONTROL,maskt.cursr,SET_VALUE= $
                     'Block# '+STRING(xyb+1,format='(i4.4)')+' (C='+STRING(yb+1,'(i2.2)')+',R='+STRING(xb+1,'(i2.2)')+')'
                     ;'Block# '+STRING(xyb+1,format='(i4.4)')+' (X='+STRING(xb+1,'(i2.2)')+',Y='+STRING(yb+1,'(i2.2)')+')'


                    maskt.blocks(block) = 0 
                    WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.img_map]
                    WSET, maskt.new_gmap & DEVICE, $
                          COPY = [xc+1, yc+1, maskt.bs-1, maskt.bs-1, xc+1, yc+1, maskt.img_map]
                    IF (maskt.type EQ 'OCCULTER') THEN occ_blocks(maskt.tele,*) = maskt.blocks ELSE $
                    IF (maskt.type EQ 'ROI') THEN roi_blocks(maskt.tele,*,maskt.table-1) = maskt.blocks
                 END
                 4B: BEGIN 	;** Right mouse button; adjust color table
                    nc  = ROUND((FLOAT(y)/maskt.ms)*!D.TABLE_SIZE)
                    cen = ROUND((FLOAT(x)/maskt.ms)*!D.TABLE_SIZE)
                    c0 = (cen - nc/2) > 0 < (!D.TABLE_SIZE-1)
                    c1 = (cen + nc/2) > 0 < (!D.TABLE_SIZE-1)
                    p = INTARR(!D.TABLE_SIZE)
                    IF (cen GT 0) THEN p(0:cen-1) = 0
                    IF (cen LT !D.TABLE_SIZE-1) THEN p(cen:!D.TABLE_SIZE-1) = 255
                    IF (c0 NE c1) THEN p(c0:c1-1) = BYTSCL(INDGEN(c1-c0)*!D.TABLE_SIZE, TOP=255-10)
                    TVLCT, p,p,p, maskt.gcol+1
                 END
              ENDCASE
              WSET, maskt.draw_win
              IF (maskt.grid) THEN $
                DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
              ELSE $
                DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
  selected = WHERE(maskt.blocks EQ 1, ncomplement=nunmasked)
  ; masked blocks are 1?
  perc=float(nunmasked)/(32.*32)
  print,string(13b),perc,' of 2048x2048 unmasked',format='($,a,f4.2,a)'
           ENDIF

           ADD_BLOCK_LABELS,maskt

	END

	maskt.quit : BEGIN	;** exit program
	   WIDGET_CONTROL, /DESTROY, maskt.base
	END

	maskt.helpb : BEGIN	;** help text
           help_str = STRARR(33)
           help_str(0) = 'Displayed mask is a 34x34 square representing a 2176x2176 CCD. Each'
           help_str(1) = 'small square is, therefore, a 64x64 pixel block. The orientation of'
           help_str(2) = 'display is the rectified CCD coordinates with N-up and W-right.'
           help_str(3) = 'Please note that block positions and numbers are the un-rectified'
           help_str(4) = 'CCD coordinates and their positions, therefore, changes from one CCD'
           help_str(5) = 'to another.'
           help_str(6) = ''
           help_str(7) = 'There are two ways to generate a mask. First is the traditional way'
           help_str(8) = 'using cursor to select blocks to be masked. Second method is to enter'
           help_str(9) = 'rectangular ICER coordinates p1col, p1row, p2col, and p2row in the '
           help_str(10) = 'provided area.' 
           help_str(11) = '
           help_str(12) = 'Select blocks to be masked with the left mouse button.'
           help_str(13) = 'De-select blocks with the middle mouse button.'
           help_str(14) = 'Use the right mouse button (hold and drag) to adjust the color table.'
           help_str(15) = ''
           help_str(16) = 'ICER Mask usage:'
           help_str(17) = 'The ICER Mask uses a pair of coordinate values between 1-2176 to'
           help_str(18) = 'indicate opposite corners of a rectangular image area. Enter the'
           help_str(19) = 'flight col,row pairs (p1col,p1row) and (p2col,p2row) by following'
           help_str(20) = 'displayed CCD (1,1) coordinates and press "Apply" to save and display'
           help_str(21) = 'the mask on the 34x34 grid. These rectangle coordinates will be saved'
           help_str(22) = 'as ICEA type when "Save FSW Mask" is pressed. '
           help_str(23) = 'A simpler way to to enter ICER coordinates is to press the "press me'
           help_str(24) = 'then Left Mouse Button for P1 Col/Row" followed by moving the cursor'
           help_str(25) = 'to desired P1 block and press the left mouse button to add the block'
           help_str(26) = 'coordinates to the P1col,P1row. Repeat again for selecting the P2 block'
           help_str(27) = '(opposite diagnol corner of P1). Then press "Apply" to save and '
           help_str(28) = 'display the mask on the 34x34 grid. Note that this coordinates are'
           help_str(29) = 'multiples of 64. To use another value, such as 51 instead of 64, simply'
           help_str(30) = 'edit the coordinate that needs coorection before pressing "Apply".
           help_str(31) = ''
           help_str(32) = 'To undo an ICEAR mask, set all values to zero and press "Apply" again.'
 

           POPUP_HELP, help_str, TITLE="MASK TOOL HELP"
	END

	maskt.telepd : BEGIN	;** select telescope table

           IF (event.index EQ maskt.tele) THEN RETURN

           maskt.tele = event.index

           CHANGE_COORD_LABELS,maskt  

           UPDATE_ICER,maskt,occ_icer,roi_icer

           WIDGET_CONTROL, maskt.ccd0, SET_VALUE= maskt.tele_coord(maskt.tele+maskt.sc_id*5)+'   (use for entering ICER Mask coordinates 1-2176)'

           maskt.blocks(*) = 0

           TVLCT, 180,95,85, 10 ; dark brown is backgrogund
           img = BYTARR(maskt.ms,maskt.ms) & img = img + 10 ; set to background color

           IF (maskt.type EQ 'OCCULTER') THEN maskt.blocks = occ_blocks(maskt.tele,*) ELSE $
           IF (maskt.type EQ 'ROI') THEN maskt.blocks = roi_blocks(maskt.tele,*,maskt.table-1)
           WSET, maskt.new_map & TV, img ; new tele selected, don't display old image, if any. Display backgd.
           WSET, maskt.img_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
           WSET, maskt.new_gmap & TV, img

           FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
           FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
           selected = WHERE(maskt.blocks EQ 1)
           IF (selected(0) NE -1) THEN BEGIN
             FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
               yc = selected(i)/xyblks*maskt.bs
               xc = (selected(i) - selected(i)/xyblks*xyblks)*maskt.bs
               WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
               WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
             ENDFOR
           ENDIF
           WSET, maskt.draw_win

           IF (maskt.grid) THEN $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
           ELSE $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]

           ADD_BLOCK_LABELS,maskt
           WIDGET_CONTROL, maskt.img_lab, SET_VALUE=' ' 
           
	END

        maskt.aply: BEGIN
          apply_mask,maskt,roi_blocks,occ_blocks,xyblks
        END

       maskt.p1rowcol: BEGIN
         maskt.icer_mouse=1
       END

      maskt.p2rowcol: BEGIN
         maskt.icer_mouse=2
       END

        maskt.p1col : BEGIN
          WIDGET_CONTROL, maskt.p1col, GET_VALUE= p1col
          print, p1col
        END

        maskt.p1row : BEGIN
          WIDGET_CONTROL, maskt.p1row, GET_VALUE= p1row
          print, p1row
        END

        maskt.p2col : BEGIN
          WIDGET_CONTROL, maskt.p2col, GET_VALUE= p2col
          print, p2col
        END

        maskt.p2row : BEGIN
         WIDGET_CONTROL, maskt.p2row, GET_VALUE= p2row
          print, p2row
        END


        maskt.tabpd : BEGIN 

           IF (event.index EQ maskt.table) THEN RETURN

           IF (event.index EQ 0) THEN $
             maskt.type= 'OCCULTER'   $
           ELSE $
             maskt.type= 'ROI'
 
           ; clear blocks currently displayed:
           maskt.blocks(*) = 0
           WSET, maskt.new_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           WSET, maskt.new_gmap & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
           FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
           WSET, maskt.draw_win 
           IF (maskt.grid) THEN $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
           ELSE $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]

           maskt.table= event.index
           IF (maskt.type EQ 'ROI') THEN $
             maskt.blocks = roi_blocks(maskt.tele,*,maskt.table-1) $
           ELSE $
             maskt.blocks = occ_blocks(maskt.tele,*)

           ;WSET, maskt.new_map  & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           ;WSET, maskt.new_gmap & DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.img_map]
           FOR i=0,xyblks-1 DO PLOTS, [i*maskt.bs,i*maskt.bs], [0,maskt.ms], /DEVICE, COLOR=0
           FOR i=0,xyblks-1 DO PLOTS, [0,maskt.ms], [i*maskt.bs,i*maskt.bs], /DEVICE, COLOR=0
           selected = WHERE(maskt.blocks EQ 1)
           IF (selected(0) NE -1) THEN BEGIN
             FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
               yc = selected(i)/xyblks*maskt.bs
               xc = (selected(i) - selected(i)/xyblks*xyblks)*maskt.bs
               WSET, maskt.new_map  & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
               WSET, maskt.new_gmap & DEVICE, COPY = [xc, yc, maskt.bs, maskt.bs, xc, yc, maskt.mask_map]
             ENDFOR
           ENDIF
           WSET, maskt.draw_win
           IF (maskt.grid) THEN $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_gmap] $
           ELSE $
             DEVICE, COPY = [0, 0, maskt.ms, maskt.ms, 0, 0, maskt.new_map]
           ADD_BLOCK_LABELS,maskt
           UPDATE_ICER,maskt,occ_icer,roi_icer
        END

        ELSE : BEGIN
        END

   ENDCASE

END

;__________________________________________________________________________________________________________
;

;PRO MASK_TOOL, sc
PRO MASK_TOOL

COMMON MASK, maskt, occ_blocks, roi_blocks
COMMON ICERMASKS

  occ_icer= INTARR(5,4)
  roi_icer= INTARR(5,4,4)

;    IF (DATATYPE(sc) EQ 'UND') THEN BEGIN
;      scid=['A','B']
;      res = RESPOND_WIDG(/COLUMN, mess= ["Choose SpaceCraft A or B"], butt= scid) 
;      sc= scid(res)
;    ENDIF
       
    DEVICE, decomposed=0 ; to use 8 bits FOR color (FOR 24 bit systems) - IDL limitation.

    RESTORE,GETENV('PT')+'/IN/OTHER/ccd_size.sav' ;=> xyblks,xyblkpix,xypix,xstart,ystart

; AEE 6/10/08 - commented these and added init occ_blocks and roi_blocks:
;    IF (DATATYPE(occ_blocks) NE 'UND') THEN BEGIN
;      w= WHERE(occ_blocks NE 0, wcnt)
;      IF (wcnt GT 0) THEN occ_blocks(w)= 0
;      w= WHERE(roi_blocks NE 0, wcnt) 
;      IF (wcnt GT 0) THEN roi_blocks(w)= 0 
;    ENDIF ELSE BEGIN
;      occ_blocks= BYTARR(5,xyblks*xyblks)  ; CCD mapped to 34x34 CCD square blocks.
;      roi_blocks= BYTARR(5,xyblks*xyblks,4)  ; CCD mapped to 34x34 CCD square blocks and 4 tables per tele.
;    ENDELSE

    occ_blocks= BYTARR(5,xyblks*xyblks)  ; CCD mapped to 34x34 CCD square blocks.
    roi_blocks= BYTARR(5,xyblks*xyblks,4)  ; CCD mapped to 34x34 CCD square blocks and 4 tables per tele.


    IF xyblks EQ 32 THEN ms = 640
    IF xyblks EQ 34 THEN ms = 680

    ms= 544

    bs = ms/xyblks ;** block size
    blocks = BYTARR(xyblks*xyblks)
    press = 0B
    ;title = 'SECCHI MASK TOOL'
    title = 'SECCHI MASK TOOL (Rectified Ground Coordinates Display)'
    tele_type = [ "  EUVI Table  ", $
                  "  COR1 Table  ", $
                  "  COR2 Table  ", $
                  "  HI1 Table  ", $
                  "  HI2 Table "]

    ; Add CCD 0 coordinate for each telescope:


;   CCD 0 coordinates as appear in the ground display:
    tele_coord= [ "    EUVI  Un-Rectified CCD (1,1) Coordinate: NW -> S", $ ; SC-A
                  "    COR1  Un-Rectified CCD (1,1) Coordinate: NE -> S", $ ; SC-A
                  "    COR2  Un-Rectified CCD (1,1) Coordinate: SW -> N", $ ; SC-A
                  "     HI1  Un-Rectified CCD (1,1) Coordinate: SE -> W", $ ; SC-A
                  "     HI2  Un-Rectified CCD (1,1) Coordinate: SE -> W", $ ; SC-A
                  "    EUVI  Un-Rectified CCD (1,1) Coordinate: NE -> S", $ ; SC-B
                  "    COR1  Un-Rectified CCD (1,1) Coordinate: SW -> N", $ ; SC-B
                  "    COR2  Un-Rectified CCD (1,1) Coordinate: NE -> S", $ ; SC-B
                  "     HI1  Un-Rectified CCD (1,1) Coordinate: NW -> E", $ ; SC-B
                  "     HI2  Un-Rectified CCD (1,1) Coordinate: NW -> E"]   ; SC-B

    tele = 0
    grid = 1
    table= 0
    type = 'OCCULTER' ; FOR table = -


    ;********************************************************************
    ;** SET UP WIDGETS **************************************************

    tables= ["OCC1 Table", $
             "ROI1 Table", $
             "ROI2 Table", $
             "ROI3 Table", $
             "ROI4 Table"]

    msg= "      "

    ;base = WIDGET_BASE(/COLUMN, TITLE=title+"   SpaceCraft - "+sc, /FRAME)
    base = WIDGET_BASE(/COLUMN, TITLE=title, /FRAME)

    row = WIDGET_BASE(base, /ROW)
      telepd = CW_BSELECTOR2(row, tele_type, SET_VALUE=tele)
      tabpd = CW_BSELECTOR2(row, tables, SET_VALUE=table)
      ;tmp = WIDGET_LABEL(row, VALUE =msg+msg+msg+msg+msg+msg+msg+msg+msg)
      ;tmp = WIDGET_LABEL(row, VALUE =msg)
      ;cursr= WIDGET_LABEL(row, VALUE ='X=0000 , y=0000 , Blk= 0000')
      tmp = WIDGET_LABEL(row, VALUE ='    ')
      cpix= WIDGET_LABEL(row, VALUE = $
          'Pcol='+STRING(0*64,'(i4.4)')+'-'+STRING(0*64+63,'(i4.4)')+$
         ',Prow='+STRING(0*64,'(i4.4)')+'-'+STRING(0*64+63,'(i4.4)'), /FRAME)
        ;  'Xpix='+STRING(0*64,'(i4.4)')+'-'+STRING(0*64+63,'(i4.4)')+$
        ; ',Ypix='+STRING(0*64,'(i4.4)')+'-'+STRING(0*64+63,'(i4.4)'), /FRAME)

      tmp = WIDGET_LABEL(row, VALUE =' ')
      cursr= WIDGET_LABEL(row, VALUE ='Block# 0000 (C=00, R=00',/FRAME)
      ;cursr= WIDGET_LABEL(row, VALUE ='Block# 0000 (X=00, y=00',/FRAME)
      tmp = WIDGET_LABEL(row, VALUE ='  ')
      helpb = WIDGET_BUTTON(row, VALUE='  HELP  ')
      tmp = WIDGET_LABEL(row, VALUE ='  ')
      quit = WIDGET_BUTTON(row, VALUE=" Dismiss ")

      font1='-adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1'
      if not allow_font(font1) then get_font, font1
      nt1= WIDGET_BASE(base, /ROW)
      ;blank= '                                                         '
      blank= '                        '
;      tmp= WIDGET_LABEL(nt, VALUE =blank)
;      tmp= WIDGET_LABEL(nt, VALUE = 'N',FONT=font1)
;      tmp= WIDGET_LABEL(nt, VALUE = blank+'                (1,1)',FONT=font1)  ; Default is EUVI-A
      nt= WIDGET_LABEL(nt1,/ALIGN_LEFT,VALUE =blank+blank+blank+'N'+ blank+blank+'               (1,1)        ',FONT=font1) ; Default is EUVI-A
      ew= WIDGET_BASE(base,/ ROW)
      ;draw_w = WIDGET_DRAW(base, XSIZE=ms, YSIZE=ms, /BUTTON_EVENTS, /MOTION_EVENTS)
      dw=  WIDGET_BASE(base, /ROW)
      tmp = WIDGET_LABEL(dw, VALUE ='E',FONT=font1)
      draw_w = WIDGET_DRAW(dw, XSIZE=ms, YSIZE=ms, /BUTTON_EVENTS, /MOTION_EVENTS, RETAIN=2)
      tmp = WIDGET_LABEL(dw, VALUE ='W',FONT=font1)
      ns1= WIDGET_BASE(base,/ROW)
      blank1='                                  '
;      tmp = WIDGET_LABEL(ns, VALUE ='(2176,2176)',FONT=font1) ; Default is EUVI-A
;      tmp = WIDGET_LABEL(ns, VALUE =blank1)
;      tmp= WIDGET_LABEL(ns, VALUE = 'S',FONT=font1)
      ns=  WIDGET_LABEL(ns1,/ALIGN_LEFT,VALUE ='(2176,2176)'+blank+blank+'       S'+blank+blank+blank+blank,FONT=font1)

      sc_id = 0
      c0= WIDGET_BASE(base,/ROW)
      ccd0= WIDGET_LABEL(c0, VALUE = tele_coord(tele+sc_id*5)+'   (use for entering ICER Mask coordinates 1-2176)', FONT=font1)

    row = WIDGET_BASE(base, /ROW)
      loadmask  = WIDGET_BUTTON(row, VALUE='Load Mask ')
      msk_lab = WIDGET_LABEL(row, VALUE =msg+msg+msg+msg+msg+msg+msg+msg+msg,FONT= font1)
      loadimage  = WIDGET_BUTTON(row, VALUE='   Load Image   ')
      img_lab = WIDGET_LABEL(row, VALUE= msg+msg+msg+msg+msg+msg+msg+msg+msg,FONT= font1)
      bimg =   widget_droplist(row, VALUE=string(['1X1','2X2','4X4'],'(A3)'), UVALUE='bimg',title='Binning:')

    row = WIDGET_BASE(base, /ROW)
      grid_types = ['   Show Grid   ','   Hide Grid   ']
      showgrid = WIDGET_BUTTON(row, VALUE=grid_types(1))
      invert = WIDGET_BUTTON(row, VALUE=' Invert Blocks ')
      ;tmp = WIDGET_LABEL(row, VALUE =msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg)
      tmp = WIDGET_LABEL(row, VALUE =msg+msg+msg+msg+msg)
      ;prnt = WIDGET_BUTTON(row, VALUE=' Print Mask ')
      
      r1= WIDGET_BASE(row,/ROW,/FRAME)
      ;sc_id = 0
      sc= 'A'
      ;tmp = WIDGET_LABEL(r1, VALUE ='FSW Coordinates:')
      prnt = WIDGET_BUTTON(r1, VALUE=' Print FSW Mask')
      savemask = WIDGET_BUTTON(r1, VALUE=' Save FSW Mask')
      tmp = WIDGET_LABEL(r1, VALUE =' for ')
      sc_pd = CW_BSELECTOR2(r1, [' S/C-A ',' S/C-B '], SET_VALUE= sc_id)
      
      r2= WIDGET_BASE(base,/ROW,/FRAME)
      ;tmp = WIDGET_LABEL(r2, VALUE ='ICER Mask:  P1col')
      tmp = WIDGET_LABEL(r2, VALUE =' ICER Mask',FONT=font1)

      ;IF (maskt.type EQ 'ROI') THEN $
      ;  coords= roi_icer(maskt.tele,maskt.table-1,*) $
      ;ELSE $
      ;  coords= occ_icer(maskt.tele,*)
      ;coords= occ_icer(0,*)
      coords=[0,0,0,0]

      tmp = WIDGET_LABEL(r2, VALUE ='   P1col')
      p1col= WIDGET_TEXT(r2, YSIZE=1, XSIZE=10, VALUE= STRTRIM(coords(0),2),/EDITABLE)
      tmp = WIDGET_LABEL(r2, VALUE ='   P1row')
      p1row= WIDGET_TEXT(r2, YSIZE=1, XSIZE=10, VALUE= STRTRIM(coords(1),2),/EDITABLE)
      tmp = WIDGET_LABEL(r2, VALUE ='   P2col')
      p2col= WIDGET_TEXT(r2, YSIZE=1, XSIZE=10, VALUE= STRTRIM(coords(2),2),/EDITABLE)
      tmp = WIDGET_LABEL(r2, VALUE ='   P2row')
      p2row= WIDGET_TEXT(r2, YSIZE=1, XSIZE=10, VALUE= STRTRIM(coords(3),2),/EDITABLE)
      tmp = WIDGET_LABEL(r2, VALUE ='  ')
      aply = WIDGET_BUTTON(r2, VALUE='Apply')
      icer_mouse= 0 ; not active
      r3= WIDGET_BASE(base,/ROW,/FRAME)
      tmp = WIDGET_LABEL(r3, VALUE ='         ')
      p1rowcol= WIDGET_BUTTON(r3, VALUE='Press me then Left Mouse Button for P1 Col/Row')
      tmp = WIDGET_LABEL(r3, VALUE ='  ')
      p2rowcol= WIDGET_BUTTON(r3, VALUE='Press me then Left Mouse Button for P2 Col/Row')

      sfn= WIDGET_LABEL(base, VALUE =msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+ $
                  msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg+msg,FONT=font1)

    ;********************************************************************
    ;** REALIZE THE WIDGETS *********************************************

    WIDGET_CONTROL, base, /REALIZE
    WIDGET_CONTROL, draw_w, GET_VALUE=draw_win
    WIDGET_CONTROL, /HOUR

    WIDGET_CONTROL, helpb, SENSITIVE=1
    WIDGET_CONTROL, showgrid, SENSITIVE=1
    WIDGET_CONTROL, loadmask, SENSITIVE=1
    WIDGET_CONTROL, invert, SENSITIVE=1
    ;WIDGET_CONTROL, clear, SENSITIVE=1
    WIDGET_CONTROL, prnt, SENSITIVE=1

;    gcol = 9	;** color index FOR grid
    gcol = 11 ;** color index FOR grid ; AEE
    ;img = READFITS('/home/argus/scott/images/occulter/c2.fits')
    IF ((SIZE(img))(0) NE 2) THEN img = BYTARR(ms,ms) $
    ELSE BEGIN
       img = CONGRID(img, ms, ms)
       std = STDEV(img, mn)
       img = BYTSCL(img, 0, mn+4*std, TOP=255-10)		;** leave 10 colors unallocated
    ENDELSE
    img = img + 10

    TVLCT, 180,95,85, 10   ;** load dull dark brown into entry 10
    img_map = 29
    WINDOW, img_map, XSIZE=ms, YSIZE=ms, /PIXMAP
    TV, img
    mask_map = 30
    WINDOW, mask_map, XSIZE=ms, YSIZE=ms, /PIXMAP
    temp = INTARR(ms,ms) + gcol
    TVLCT, 142,229,238, gcol  ;** load light blue into entry 11 - used by mask.pro
    TV, temp
    new_map = 31
    WINDOW, new_map, XSIZE=ms, YSIZE=ms, /PIXMAP
    TVLCT, 180,95,85, 10   ;** load dull dark brown into entry 10
    TV, img
    new_gmap = 28
    WINDOW, new_gmap, XSIZE=ms, YSIZE=ms, /PIXMAP
    TV, img

    FOR i=0,xyblks-1 DO PLOTS, [i*bs,i*bs], [0,ms], /DEVICE, COLOR=0
    FOR i=0,xyblks-1 DO PLOTS, [0,ms], [i*bs,i*bs], /DEVICE, COLOR=0

    ;** if blocks were set at some previous time reset them
    selected = WHERE(blocks EQ 1)
    IF (selected(0) NE -1) THEN BEGIN
      FOR i=0,N_ELEMENTS(selected)-1 DO BEGIN
        yc = selected(i)/xyblks*bs
        xc = (selected(i) - selected(i)/xyblks*xyblks)*bs
        WSET, new_map  & DEVICE, COPY = [xc, yc, bs, bs, xc, yc, mask_map]
        WSET, new_gmap & DEVICE, COPY = [xc, yc, bs, bs, xc, yc, mask_map]
      ENDFOR
    ENDIF

    init_colortable
    
    WSET, draw_win 
    IF (grid) THEN $
      DEVICE, COPY = [0, 0, ms, ms, 0, 0, new_gmap] $
    ELSE $
      DEVICE, COPY = [0, 0, ms, ms, 0, 0, new_map]


    maskt = CREATE_STRUCT( 'base', base,	 	$
                           'quit', quit, 		$
                           'sfn', sfn,                  $
                           'ccd0', ccd0,                $
                           'tele_coord', tele_coord,    $
                           'telepd', telepd, 		$
                           'tabpd', tabpd,              $
                           'table', table,              $
                           'helpb', helpb, 		$
                           'cpix', cpix,                $
                           'cursr', cursr,              $
                           'draw_win', draw_win, 	$
                           'draw_w', draw_w, 		$
                           'bs', bs, 			$
                           'ms', ms, 			$
                           'blocks', blocks,		$
                           'msk_lab', msk_lab,          $
                           'img_lab', img_lab,          $
                           'img_map', img_map, 		$
                           'mask_map', mask_map, 	$
                           'new_map', new_map, 		$
                           'new_gmap', new_gmap, 	$
                           'grid', grid, 		$
                           'gcol', gcol, 		$
                           'press', press, 		$
                           'showgrid', showgrid, 	$
                           'grid_types', grid_types, 	$
                           ;'clear', clear, 		$
                           'invert', invert, 		$
                           'prnt', prnt, 		$
                           'savemask', savemask,        $
                           'type', type, 		$
                           'tele', tele, 		$
                           'sc_id', sc_id,              $
                           'sc_pd', sc_pd,              $
                           'sc', sc,                    $
                           'loadmask', loadmask,        $
                           'p1col', p1col,              $
                           'p1row', p1row,              $
                           'p2col', p2col,              $
                           'p2row', p2row,              $
                           'p1rowcol', p1rowcol,        $
                           'p2rowcol', p2rowcol,        $
                           'icer_mouse', icer_mouse,    $
                           'nt', nt,                    $
                           'ns', ns,                    $
                           'aply', aply,                $
                           'bimg', bimg,                $
                           'loadimage', loadimage)


   ; EUVI-A (Default):
   ;FOR i=0,33 DO XYOUTS,maskt.ms-10,maskt.ms-5-(i*maskt.bs+5),STRTRIM(i+1,2),/DEV
   ;FOR i=0,33 DO XYOUTS,i*maskt.bs+5,maskt.ms-10,STRTRIM(34-i,2),/DEV
   ADD_BLOCK_LABELS,maskt

   XMANAGER, 'MASK_TOOL', base
END
