; $Id: compare_ssr1_percent.pro,v 1.12 2013/01/31 12:59:27 mcnutt Exp $
pro compare_ssr1_percent,sc,day1,day2,dt, RESET=reset, DEBUG=debug
;
; Project     : STEREO SECCHI
;                   
; Name        : compare_ssr1_percent
;               
; Purpose     : to compare actual SSR percents to schedules
;               
; Explanation : 
;               
; Use         : IDL> compare_ssr1_percent,'a','2009-03-01','2009-03-31'
;    
; Inputs      : sc = A or B
;   	    	day1 = 1st date to plot
;               day2 = last date to plot
;
; Optional INputs:
;   	    	dt  Number of days to compute averages for (defaults to per track)
;               
; Outputs     : plots actual and scheduled SSR1 percent
;
; Keywords :	/RESET	Re-set common block
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : planning 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simpson 03/24/2009 
;               
; Modified    :
;
; $Log: compare_ssr1_percent.pro,v $
; Revision 1.12  2013/01/31 12:59:27  mcnutt
; improved block size plot
;
; Revision 1.11  2013/01/24 17:09:30  mcnutt
; added hour block size plot
;
; Revision 1.10  2012/11/29 15:54:34  mcnutt
; increased report array sizes and added psym to db plot
;
; Revision 1.9  2010/05/21 14:54:44  nathan
; Improved algorithm; added /DEBUG; no 3rd arg. gives per track numbers
;
; Revision 1.8  2010/03/17 14:16:47  mcnutt
; clears common block if sc changes
;
; Revision 1.7  2009/12/15 13:23:20  mcnutt
; corrected plot x axis
;
; Revision 1.6  2009/11/20 23:51:33  nathan
; completed new options
;
; Revision 1.5  2009/11/19 22:17:21  nathan
; moved where ssr1pb is selected from input EOT info
;
; Revision 1.4  2009/11/19 19:17:16  mcnutt
; prints report to screen
;
; Revision 1.3  2009/11/17 20:04:28  mcnutt
; added defference plot
;
; Revision 1.2  2009/08/24 16:30:47  mcnutt
; added new report style (3)
;
; Revision 1.1  2009/03/24 14:54:32  mcnutt
; displays actual and scheduled SSR1 percents
;
common compare_ssr1, eottime, ssr1per,ssr1remain,hrs0,ssr1db0,psc,pbtai

if datatype(psc) EQ 'UND' THEN psc=sc
if sc ne psc then begin
  undefine,eottime
  undefine,ssr1per
  undefine,ssr1remain
  undefine,hrs0
  undefine,ssr1db0
endif
psc=sc

ssr1size=5651./8    ; From SOP

; Check to see if we have read in planning report files:

IF datatype(eottime) NE 'UND' and not keyword_set(RESET) THEN goto, skipreports

rptfile=getenv('sched')+'/'+strupcase(sc)+'/dailyats/reports/ST*.rpt'
tmp=file_search(rptfile)
help,tmp

eottime=strarr(10000) & ssr1per=intarr(10000) &  ssr1remain=intarr(10000) 
pbtai=dblarr(10000)

  if tmp(0) ne '' then begin
  line='' & d=0
  lastepb=-5d*3600
  for n=0,n_elements(tmp)-1 do begin
    rptstyle=2
    openr,lurpt,tmp(n),/get_lun
    readf,lurpt,line
    if strpos(line,'Schedules') eq 0 then readf,lurpt,line
    if strpos(line,'Date') eq 0 then begin
       if strpos(line,'pbtime') lt 0 then rptstyle=0
       if strpos(line,'Adj') gt -1 then rptstyle=1
    endif   
   if strpos(line,'rate') gt -1 then rptstyle=4
   if strpos(line,'SSR2MB/pb') gt -1 then rptstyle=3

    while ~eof(lurpt) do begin
    	readf,lurpt,line
    	IF keyword_set(DEBUG) THEN IF strmid(line,0,1) NE '2' THEN print,'Skipping ',line
    	IF strmid(line,0,1) NE '2' THEN goto, nextline
    	spt=strsplit(line,/extract)
	spb=fix(spt[2])
	epb=fix(spt[3])
    	if spb gt epb then epb=epb+2400
    	eottime0=spt[0]+'T'+strmid(string(epb,'(i4.4)'),0,2)+':'+strmid(string(epb,'(i4.4)'),2,2)
	tai0=anytim2tai(eottime0)
	; skip over mid-track pointer resets; will skip over tracks LT 4hrs apart (end2end)!
	IF keyword_set(DEBUG) THEN IF tai0-lastepb LT 4*3600. THEN print,'Skipping ',eottime0 else print,eottime0
	IF tai0-lastepb LT 4*3600. THEN goto, nextline
	lastepb=tai0
	eottime[d]=eottime0
	pbtai[d]=ceil((epb-spb)/100.)*3600
       if rptstyle eq 0 then begin
;          1         2         3         4  
;01234567890123456789012345678901234567890123456789
;2008-01-01       366  1125  1525   33         99.2715
            ssr1per[d]=round(float(spt[5]))
	    ssr1remain[d]=0
       endif

       if rptstyle eq 1 then begin
;          1         2         3         4         5         6
;0123456789012345678901234567890123456789012345678901234567890123456789
;2009-01-06   6 1730 2134 30:00   04:03  583   056    632  089  006  sch adj 110.25
            ssr1per[d]=     fix(spt[9])
	    ssr1remain[d]=  fix(spt[10])
       endif

       if rptstyle eq 2 then begin
;          1         2         3         4         5         6
;0123456789012345678901234567890123456789012345678901234567890123456789
;2009-01-27  27 0500 0904 16:54   04:03  583  324         0    046  000
            ssr1per[d]=     fix(spt[9])
	    ssr1remain[d]=  fix(spt[10])
       endif

       if rptstyle eq 4 then begin
;          1         2         3         4         5         6         7
;01234567890123456789012345678901234567890123456789012345678901234567890123456789
;Date       DOY SPB  EPB  EPB-EPB pbtime rate MB/pb MB/sch  MB/left SSR1% %aftpb SSR2MB/pb
;2009-08-19 231 1928 0016 15:59   04:47   360  533  380         0    053  000    100
            ssr1per[d]=     fix(spt[10])
	    ssr1remain[d]=  fix(spt[11])
       endif

       if rptstyle eq 3 then begin
;          1         2         3         4         5         6         7
;01234567890123456789012345678901234567890123456789012345678901234567890123456789
;Date       DOY SPB  EPB  EPB-EPB pbtime rate MB/pb MB/sch  MB/left SSR1% %aftpb SSR2MB/pb
;2009-08-19 231 1928 0016 15:59   04:47   360  533  380         0    053  000    100
            ssr1per[d]=     fix(spt[10])
	    ssr1remain[d]=  fix(spt[11])
       endif
       d=d+1
       nextline:
    endwhile  
    close,lurpt
    free_lun,lurpt
;print,eottime[d-2],rptstyle,ssr1per[d-2],ssr1remain[d-2]
  endfor
endif
eottime0=eottime(0:d-1) & ssr1per0=ssr1per(0:d-1) &  ssr1remain0=ssr1remain(0:d-1) & pbtai0=pbtai[0:d-1]
order=sort(eottime0) 
unq=uniq(eottime0(order))
help,order,unq
eottime=    eottime0	(order(unq))
ssr1per=    ssr1per0	(order(unq))
ssr1remain= ssr1remain0 (order(unq))
pbtai=	    pbtai0  	[order[unq]]
hrs0=utc2tai(str2utc(eottime)) + 60*60.  ; add 60 min. to end to overlap EOT in telemetry
IF keyword_set(DEBUG) THEN stop
skipreports:

; Check to see if we already have interval from database

staut=anytim2utc(day1)
staut0=staut
staut0.mjd=staut.mjd-1
staut.mjd=staut.mjd-2
stastr=utc2str(staut)
endut=anytim2utc(day2)
endut.mjd=endut.mjd+1
endstr=utc2str(endut)

IF datatype(ssr1db0) NE 'UND' THEN BEGIN
    ndb=n_elements(ssr1db0)
    dbut=anytim2utc(ssr1db0.PCKT_TIME)
    dbt1=dbut[0]   
    dbt2=dbut[ndb-1]
    IF dbt1.mjd LE staut.mjd and  dbt2.mjd+1 GE endut.mjd THEN BEGIN
    	z=where(dbut.mjd GE staut.mjd and dbut.mjd LT endut.mjd, ngz)
	ssr1db=ssr1db0[z]
	goto, skipdb
    ENDIF
ENDIF  
;--Start retrieve ssr1% from database
dbase= 'secchi_flight_hk_'+strupcase(sc)
resol=' and CAST(hk_ih_'+strlowcase(sc)+'.pckt_time as CHAR) like "%:%0:0%" group by mid(hk_ih_'+strlowcase(sc)+'.pckt_time,1,16)'
cmnd='select unix_time,pckt_time,IHSCSSRAFULL,IHSCSSRBFULL from hk_ih_'+strlowcase(sc)+' where pckt_time between "'+stastr+'" and "'+endstr+'"'+resol; group by mid(hk_ih_'+strlowcase(sc)+'.pckt_time,1,16)'
ssr1db0= sqdb(dbase,cmnd)
ssr1db=ssr1db0
tek_color
device,decompose=0
device,retain=2

skipdb:

;<<<<<<< compare_ssr1_percent.pro
;mtitle='SECCHI-'+strupcase(sc)+'  SSR1 % Full'
;plotvtime,ssr1db.PCKT_TIME,ssr1db.IHSCSSRAFULL,'Actual',background=1,color=0,title=mtitle, $
;    xstyle=1,yrang=[0,100]
;wait,2
;oplotvtime,hrs,ssr1,'Predicted',color=2,linestyle=2
;=======
;mtitle='SECCHI-'+strupcase(sc)+'  SSR1% - actual (Black) - scheduled (Red)'
;!p.multi=[0,0,2,0,0]
;utplot,ssr1db.PCKT_TIME,ssr1db.IHSCSSRAFULL,background=1,color=0,title=mtitle,ytitle='Percent',xstyle=1,ystyle=9,ymargin=[.15,.35],position=[.05,.35,.95,.95]
;outplot,hrs,ssr1,color=2,linestyle=2
;axis,yaxis=1,yrange=ssr1size*!y.crange/100,ytitle='Mega Bytes',ystyle=1,color=0
;outplot,ssr1db.PCKT_TIME,ssr1db.IHSCSSRBFULL,color=3;,linestyle=2
;stop


ssr1t=ssr1db.IHSCSSRAFULL
reset=where(ssr1t-shift(ssr1t,1) lt 0, nres)
pptrck=(ssr1t[reset[1:nres-1]-1]-ssr1t[reset[1:nres-1]])
utctrk=anytim2utc(ssr1db[reset[1:nres-1]].pckt_time)
taitrk =utc2tai(utctrk)
t1a=utc2tai(anytim2utc(ssr1db.PCKT_TIME))
resets=[reset(1:n_elements(reset)-1),reset(1:n_elements(reset)-1)-1]
resets=resets(sort(resets))
ssr1t2=ssr1t(resets)

;--NOW pick out times from schedule report files

t1=utc2tai(anytim2utc(ssr1db(resets).PCKT_TIME))
t2e=utc2tai(anytim2utc(hrs0)) 

stat=utc2tai(staut0)
endt=utc2tai(endut)
z=where(t2e ge stat and t2e lt endt, neot)
; z is where EOTtime=hrs is between times retrieved from database
taieot= [     hrs0[z[0]-1],       hrs0[z]] 
neot=neot+1 ; include previous EOT
t2=t2e(z) 
ssr1p=	[   ssr1per[z[0]-1],   ssr1per[z]]
ssr1r=	[ssr1remain[z[0]-1],ssr1remain[z]]
pbtaiz= [     pbtai[z[0]-1],     pbtai[z]]

hrstai=[taieot-600,taieot]
ssr1=[ssr1p,ssr1r]

ssr1=ssr1(sort(hrstai))
hrstai=hrstai(sort(hrstai))
hrs2=utc2str(tai2utc(hrstai))
ssr1_e=ssr1

hrst=utc2str(tai2utc(t1))
;ssr1_a=interpol(ssr1t2,t1,t2)
;stop

;>>>>>>> 1.3

nt=n_elements(reset)
rst=where(ssr1_e-shift(ssr1_e,1) lt 0)
schedt=0. & pbt=0. 

;--Now we have % played back ([ssr1p-ssr1r],pptrck) and the time of each (taieot,taitrk)
;  We need to begin at time of playback before first in our sample
ut0=tai2utc(t2e[z[0]])    ; EOT before queried start
ut0.time=000000   ; start of day of EOT before queried start
ut1=tai2utc(taieot[neot-1])
ut1.time=0
ut1.mjd=ut1.mjd+1
uti1=ut0
uti2=ut0

IF n_params() LT 4 THEN BEGIN
; do amount for each track
    nsamples=neot-1	
    utar=tai2utc(taieot)
    title2='by track'
    ytit2='MB'
    dt=0
ENDIF ELSE BEGIN
    nsamples=ceil((ut1.mjd-ut0.mjd-1)/dt)
    utar=replicate(ut0,nsamples+1)
    utar.mjd=utar.mjd+indgen(nsamples+1)*dt
    ;values in utar are start of sample interval, except for last one which is end of last sample interval
    title2=trim(dt)+'-day average at end of interval'
    ytit2='Avg MB/Day'
ENDELSE    
dschpb=fltarr(nsamples)	; daily avg over dt, MB
dtlmpb=dschpb 	    	; ""

    print,'End of interval           Dt(days)  Nschpb   Nactpb   schpb-tlmpb
;          2009-09-01T08:27:00.000        2.0       2        2       501.526

for i=0,nsamples-1 do begin
    uti1=utar[i]
    uti2=utar[i+1]
    tai1=utc2tai(uti1)
    tai2=utc2tai(uti2)
    dtdays=(tai2-tai1)/(3600.*24)

    gsp=where(taieot GT tai1 and taieot LE tai2,ngsp)
    IF ngsp GE 1 THEN BEGIN
	IF dt EQ 0 THEN BEGIN
	; bin is each track
	  tai1dt=tai1
	  tai2dt=tai2
	  dtnom=1.
	ENDIF ELSE BEGIN
	  tai1dt= (tai1)<(taieot[gsp[0]]-pbtaiz[i])
	  tai2dt= taieot[gsp[ngsp-1]]
	  dtnom=dtdays
	ENDELSE
    	dschpb[i]=ssr1size*.01*total(ssr1p[gsp]-ssr1r[gsp])/dtnom
	
	  ; for case where track starts before tai1
    	gtm=where(taitrk GT tai1dt and taitrk LE tai2dt,ngtm)
    	; pbtai: making sure entire track is included in gtm total for daily averages only
    	IF ngtm GE 1 THEN dtlmpb[i]=ssr1size*.01*total(pptrck[gtm])/dtnom
	
    ENDIF
    print,utc2str(utar[i+1]),string(dtdays,'(f11.1)'),string(ngsp,'(i8)'), string(ngtm,'(i8)'), string(dschpb[i]-dtlmpb[i],'(f14.3)')
    ;if dtlmpb[i] gt dschpb[i] then stop

endfor

schdays= (tai2-utc2tai(utar[0]))/(3600.*24)
tlmtotalmb=total(dtlmpb)*(dt>1)
; ssr1p/r includes extra track at beginning to define interval of track; do not include it in total
schtotalmb=total(dschpb)*(dt>1)

mtitle='SECCHI-'+strupcase(sc)+'  SSR1%Full: Actual (Black), Scheduled (Red)'
;!p.multi=[0,0,2,0,0]
utplot,ssr1db.pckt_time,ssr1db.ihscssrafull,background=1,color=0,title=mtitle,ytitle='Percent',xstyle=1,ystyle=9, ymargin=[.15,.35], $
position=[.05,.65,.95,.95], xtitle='', xtickname=replicate(' ',30), yrange=[0,100], timerange=[ut0,ut1]
outplot,ssr1db.pckt_time,ssr1db.ihscssrafull,color=0,psym=1
outplot,hrs2,ssr1_e,color=2,linestyle=2
axis,yaxis=1,yrange=ssr1size*!y.crange/100,ytitle='MegaBytes',ystyle=1,color=0


mtitle='SECCHI-'+strupcase(sc)+' playback volume, '+title2
utplot,utar[1:nsamples],dtlmpb,background=1,color=0,title='',xstyle=1, ymargin=[.15,.35], position=[.05,.35,.95,.65], /noerase, psym=-1, timerange=[ut0,ut1], ytitle=ytit2 ,$
 xtitle='', xtickname=replicate(' ',30)
 ;timerange=[hrst[0],hrst[nres-1]]
outplot,utar[1:nsamples],dschpb,color=2,psym=-1
xyouts,0.5,0.63,mtitle,/normal,align=0.5,color=0


tai=utc2tai(ssr1db.pckt_time)
mbs=(ssr1db.ihscssrafull/100.*ssr1size)-(ssr1db(0).ihscssrafull/100.*ssr1size)
for n=1,n_elements(mbs)-1 do if mbs(n) lt mbs(n-1) then mbs(n:n_elements(mbs)-1)=mbs(n:n_elements(mbs)-1)+(mbs(n-1)-mbs(n))

btai=dindgen(fix((tai(n_elements(tai)-1)-tai(0))/(60*60*2.))) *(60*60*2.) +tai(0)
mbs1=spline(tai,mbs,btai)
mbs2=shift(mbs1,-1)-mbs1 >0.

mbs3=(local_slope(tai,mbs,12)*706)/12*100

y2=round((max(mbs2)+5)/10.)*10
mtitle='SECCHI-'+strupcase(sc)+'  SSR1 2 hour block size'
;!p.multi=[0,0,2,0,0]
utplot,utc2str(tai2utc(btai)),mbs2,background=1,color=0,title='',ytitle='MegaBytes',xstyle=1,ystyle=1, ymargin=[.15,.35], $
position=[.05,.05,.95,.35], yrange=[0,y2], timerange=[ut0,ut1],/noerase, psym=2
outplot,ssr1db.pckt_time,mbs3,color=2,psym=3
xyouts,0.5,0.33,mtitle,/normal,align=0.5,color=0
print,''
print,'Overall Total MB:                                   24hr averages:'
print,'      Playback       days        Scheduled        Playback        Scheduled'
print,     tlmtotalmb,   schdays,     schtotalmb,tlmtotalmb/schdays,schtotalmb/schdays

IF keyword_set(DEBUG) THEN stop
END
