pro get_daily_sch_size,sc0,d1,d2,ut, sizes, stat, outfile=outfile,byblocksize=byblocksize, DEBUG=debug
;+
; $Id: get_daily_sch_size.pro,v 1.7 2010/05/10 16:49:19 mcnutt Exp $
;
; Project   : STEREO SECCHI
;                   
; Name      : get_daily_sch_size
;               
; Purpose   : gets total predicted size of daily schedules  
;               
; Explanation: 
;               
; Use       : IDL> get_daily_sch_size,'a','2008-10-01','2009-01-31',dates,sizes,stat
;    
; Inputs    :sc = 'a' or 'b' d1=first day d2 = last day
;               
; Outputs   : dates = UTC array of dates, sizes = daily predicted total (MB), 
;   	    	stat = intarr, 1 if 1 or more BSFs are missing for that day else 0
;
; Keywords  : 
;   	    OUTFILE= name of file to print values in 
;   	    byblocksize= create a report of block size by Date
;
; Calls from LASCO :  
;
; Common    : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : data anal
;               
; Prev. Hist. : None.
;
; Written     :Lynn McNutt 2008
;               
; $Log: get_daily_sch_size.pro,v $
; Revision 1.7  2010/05/10 16:49:19  mcnutt
; works with yearly directories
;
; Revision 1.6  2010/04/26 21:21:54  nathan
; added common block (for daily-size use only); tightened up /byblocksize output
;
; Revision 1.5  2010/04/26 17:29:28  mcnutt
; added byblocksize report option
;
; Revision 1.4  2010/03/29 17:18:37  mcnutt
; updated for new 115ssr2 blocks and added the number of times each block run in the given time frame
;
; Revision 1.3  2009/07/06 18:01:58  nathan
; use scc_avail_blocks for all blocks
;
; Revision 1.2  2009/01/21 23:02:46  nathan
; add comments, return result in array
;

common schedsizes, datesa,datesb,sizesa,sizesb, schstat

ut0=anytim2utc(d1)
ut1=anytim2utc(d2)
sc=strupcase(sc0)
;--use common block if dates already covered
done=0
IF sc EQ 'A' THEN IF datatype(datesa) EQ 'STC' THEN BEGIN
    na=n_elements(datesa)-1
    if datesa[0].mjd GE ut0.mjd AND datesa[na].mjd LE ut1.mjd THEN BEGIN
    	done=1
	ut=datesa
	sizes=sizesa
	stat=schstat
	;return
    endif
ENDIF
IF sc EQ 'B' THEN IF datatype(datesb) EQ 'STC' THEN BEGIN
    nb=n_elements(datesb)-1
    if datesb[0].mjd GE ut0.mjd AND datesb[nb].mjd LE ut1.mjd THEN BEGIN
    	done=1
	ut=datesb
	sizes=sizesb
	stat=schstat
	;return
    endif
ENDIF

IF done and ~keyword_set(BYBLOCKSIZE) and ~keyword_set(OUTFILE) THEN Begin
    print,'output is already defined for input dates; returning.'
    return
ENDIF

IF getenv('sched') EQ '' THEN message,'$sched is not defined.'
atsdir=getenv_slash('sched')+sc+'/dailyats/'

SCC_AVAIL_BLOCKS,n120s2,z120s2,c120s2,n115,z115,c115,n120,z120,c120,min1,n115s2,z115s2,c115s2,/U60HR
; this will include current default schedules
schedule1=[n120s2,n115,n120,n115s2]
sch1size= [z120s2,z115,z120,z115s2]
comments= [c120s2,c115,c120,c115s2]
help,schedule1,sch1size,comments

SCC_AVAIL_BLOCKS,n0,z0,c0,n1,z1,c1,n2,z2,c2,/active
schedule1=[schedule1,n0,n1,n2]
sch1size= [sch1size, z0,z1,z2]
comments= [comments, c0,c1,c2]
help,schedule1,sch1size,comments

SCC_AVAIL_BLOCKS,n0,z0,c0,n1,z1,c1,n2,z2,c2,/campaign
schedule1=[schedule1,n0,n1,n2]
sch1size= [sch1size, z0,z1,z2]
comments= [comments, c0,c1,c2]
help,schedule1,sch1size,comments

SCC_AVAIL_BLOCKS,n0,z0,c0,/MDUMP
schedule1=[schedule1,n0]
sch1size= [sch1size, z0]
comments= [comments, c0]
help,schedule1,sch1size,comments

SCC_AVAIL_BLOCKS,n0,z0,c0,/CALROLL
schedule1=[schedule1,n0]
sch1size= [sch1size, z0]
comments= [comments, c0]
help,schedule1,sch1size,comments

SCC_AVAIL_BLOCKS,n0,z0,c0,n1,z1,c1,n2,z2,c2,min1,n3,z3,c3,/historic
schedule1=[schedule1,n0,n1,n2,n3]
sch1size=[sch1size,z0,z1,z2,z3]
comments=[comments,c0,c1,c2,c3]

scc_avail_blocks,n0,z0,C0,/gtcal
schedule1=[schedule1,n0]
sch1size=[sch1size,z0]
comments=[comments,c0]

scc_avail_blocks,n0,z0,C0,/hioffset
schedule1=[schedule1,n0]
sch1size=[sch1size,z0]
comments=[comments,c0]

SCC_AVAIL_BLOCKS,n0,z0,c0,n1,z1,c1,n2,z2,c2,m,n3,z3,c3,/jop225
schedule1=[schedule1,n0,n1,n2,n3]
sch1size= [sch1size, z0,z1,z2,z3]
comments= [comments, c0,c1,c2,c3]
help,schedule1,sch1size,comments

wait,5

if keyword_set(outfile) then begin
  openw,outlu,outfile,/get_lun
;              ~/loads/PT/flight/operations/A/dailyats/08121900.sch        661.100          0  
  IF ~keyword_set(BYBLOCKSIZE) THEN printf,outlu,'Daily Schedule                                               MB         1=unknown BSF used'
endif


ut=replicate(ut0,ut1.mjd-ut0.mjd+1)
dates=strarr(ut1.mjd-ut0.mjd+1)
sizes=lonarr(ut1.mjd-ut0.mjd+1)
stat= intarr(ut1.mjd-ut0.mjd+1)

bsfused=['']
yymmdd= strarr(ut1.mjd-ut0.mjd+1)
nbsf=intarr(n_elements(schedule1),ut1.mjd-ut0.mjd+2)
FOR mjd=ut0.mjd,ut1.mjd DO BEGIN
    i=mjd-ut0.mjd
    ut[i].mjd=mjd
    yymmdd[i]=utc2yymmdd(ut[i])
    IF keyword_set(DEBUG) THEN help,yymmdd[i]
    schtotal=0 & missbsf=0
    schfile=atsdir+yymmdd(i)+'00.sch'
    tmp=file_search(schfile)
    if tmp eq '' then begin
       schfile=atsdir+'20'+strmid(yymmdd(i),0,2)+'/'+yymmdd(i)+'00.sch'
       tmp=file_search(schfile)
    endif
    missbsf=1
    
    if tmp ne '' then begin
      openr,schlu,schfile,/get_lun
      line=''
      missbsf=0
      while ~eof(schlu) do begin
       readf,schlu,line
       if strpos(line,'bsf') eq strlen(line)-3 then begin
          block=strmid(line,16,12)
	  zsch=where(schedule1 eq block)
          if zsch(0) gt -1 then begin
    	    IF keyword_set(DEBUG) THEN print,sch1size[zsch[0]],'  ',block
	    schtotal=schtotal + sch1size(zsch(0))
            nbsf(zsch(0),i)=nbsf(zsch(0),i)+1
            nbsf(zsch(0),n_elements(nbsf(0,*))-1)=total(nbsf(zsch(0),0:n_elements(nbsf(0,*))-2))
	  endif else begin
      	    missbsf=1
	    help,block
	  endelse
          bsfused=[bsfused,block]
       endif
      endwhile  
      close,schlu
      free_lun,schlu
      sizes[i]=schtotal
    endif
    IF keyword_set(DEBUG) THEN wait,1
    
    stat[i]=missbsf
    if keyword_set(outfile) and ~keyword_set(byblocksize)then printf,outlu,schfile,'  ',schtotal,'   ',missbsf else print,schfile,'  ',schtotal,'   ',missbsf
ENDFOR

schstat=stat
IF sc EQ 'A' THEN BEGIN
    datesa=ut
    sizesa=sizes
ENDIF
IF sc EQ 'B' THEN BEGIN
    datesb=ut
    sizesb=sizes
ENDIF

goto, skipnew
; nr - not sure what this part is supposed to do
bsfused=bsfused(1:n_elements(bsfused)-1)
bsfname=[''] & nbsf=[0]
REPEAT BEGIN  
  z=where(bsfused eq bsfused(0))
  if z(0) ne -1 then begin
     nbsf=[nbsf,n_elements(z)]
     bsfname=[bsfname,bsfused(0)]
     print,bsfused(0) ,'   ',n_elements(z)
     z2=where(bsfused ne bsfused(0))
     ;print,bsfused(0) ,'   ',n_elements(z),'  ',bsfused(z2(0))
     if z2(0) gt -1 then bsfused=bsfused(z2)
  endif
ENDREP UNTIL z2(0) eq -1
bsfname=bsfname(1:n_elements(bsfname)-1)
nbsf=nbsf(1:n_elements(nbsf)-1)

skipnew:

used=where(nbsf(*,n_elements(nbsf(0,*))-1) gt 0,nu)
schused=comments(used)+'/'+schedule1(used)
nbsf=nbsf(used,*)

if keyword_set(byblocksize) then begin
  schusedt=strmid(schused,0,10)
  tmp=nbsf
  usetmp=used
   for n=0,n_elements(used)-1 do begin
      if usetmp(n) ne -1 then begin
        used2=where(schusedt eq schusedt(n))
        usetmp(n)=used2(0)
        if n_Elements(used2) gt 1 then usetmp(used2(1:n_Elements(used2)-1))=-1
        for n1=0,n_Elements(nbsf(0,*)) -1 do tmp(used2(0),n1)=total(nbsf(used2,n1)) 
      endif
   endfor
 tmp=tmp(where(usetmp gt -1),*)
 usetmp=usetmp(where(usetmp gt -1,nut))
 top='Date, '
 FOR i=0,nut-2 DO top=top+schusedt[usetmp[i]]+', '
 top=top+schusedt[usetmp[nut-1]]
 print,top
 ndays=n_elements(yymmdd)
 for n=0,ndays-1 do print, yymmdd(n),string(tmp(*,n),'(i5)')
 print,top
 bott='TOTAL:'
 for n=0,nut-1 DO bott=bott+string(tmp[n,ndays],'(i6)')
 bott=bott+' for '+trim(ndays)+' days.'
 
 print,bott
endif else begin
  fmt='(a14,22x,'+string(n_elements(yymmdd)+1,'(i4)')+'(1x,a6))'
  print,format=fmt,'Schedule block',yymmdd,' total'
  fmt='(a35,1x,'+string(n_elements(yymmdd)+1,'(i4)')+'(1x,a6))'
  for n=0,n_elements(used)-1 do print,format=fmt,schused(n),string(nbsf(n,*),'(i6)')
endelse


if keyword_set(outfile) then begin
  if keyword_set(byblocksize) then begin
     printf,outlu,top
     for n=0,n_elements(yymmdd)-1 do printf,outlu, yymmdd(n),string(tmp(*,n),'(i5)')
     printf,outlu,top
     printf,outlu,bott
  endif else begin
    fmt='(a14,22x,'+string(n_elements(yymmdd)+1,'(i4)')+'(1x,a6))'
    printf,outlu,format=fmt,'Schedule block',yymmdd,' total'
    fmt='(a35,1x,'+string(n_elements(yymmdd)+1,'(i4)')+'(1x,a6))'
    for n=0,n_elements(used)-1 do printf,outlu,format=fmt,schused(n),string(nbsf(n,*),'(i6)')
  endelse
  close,outlu
  free_lun,outlu
endif

end
