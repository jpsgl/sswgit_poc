;  Purpose: Read SECCHI ATS schedule file
; $Revision: 1.1 $
; $Date: 2007/07/24 22:17:47 $
; $Modtime:$
; $Author: wang $
; $Log: scc_readats.pro,v $
; Revision 1.1  2007/07/24 22:17:47  wang
; Initial Commit
;
 
PRO SCC_READATS,filename,icount,year,month,daymon,hour,minute,second,funcode,numbyte,chksum,blkname

openr,lun,filename,/get_lun
icount = 0
year = uintarr(24)
month = uintarr(24)
daymon = uintarr(24)
hour = uintarr(24)
minute = uintarr(24)
second = uintarr(24)
funcode = strarr(24)
numbyte = uintarr(24)
chksum = uintarr(24)
blkname = strarr(24)
oneline=' '
func=' '
blockname=' '
while ( NOT EOF(lun) ) do begin
 readf,lun,format='(A80)',oneline
 print,oneline
 firstchar=strmid(oneline,0,1)
 if ( firstchar eq '#' or firstchar eq '-') then continue
 reads,oneline,format='(I1,I2,I2,I2,I2,I2,A1,I2,I2,A12)',y,mm,dd,hh,mm,ss,func,nbyte,cksum,blockname
 year(icount) = y
 month(icount) = mm
 daymon(icount) = dd
 hour(icount) = hh
 minute(icount) = mm
 second(icount) = ss
 funcode(icount) = func
 numbyte(icount) = nbyte
 chksum(icount) = cksum
 blkname(icount) = blockname
 icount = icount + 1
endwhile
  year = year(0:icount-1) 
 month = month(0:icount-1)
 daymon = daymon(0:icount-1)
 hour = hour(0:icount-1) 
 minute = minute(0:icount-1)
 second = second(0:icount-1)
 funcode = funcode(0:icount-1)
 numbyte = numbyte(0:icount-1)
 chksum = chksum(0:icount-1)
 blkname = blkname(0:icount-1)
free_lun,lun
END
