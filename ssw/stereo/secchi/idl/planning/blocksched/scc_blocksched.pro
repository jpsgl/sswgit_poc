; Purpose: Generate SECCHI uploadable schedule file
; Inputs: pass schedule, schedule block names and sizes
; KEYWORD: atsfile = name of previous day ATS schedule file
;
; $Revision: 1.4 $
; $Date: 2007/07/31 22:20:24 $
; $Modtime:$
; $Author: wang $
; $Log: scc_blocksched.pro,v $
; Revision 1.4  2007/07/31 22:20:24  wang
; Update block sizes and more interface cleanup
;
; Assumptions: Schedule is 12 2 hour blocks
;
PRO pass_sched,date,starttime,endtime,psched
interval = endtime - starttime
psched = CREATE_STRUCT('DATE',date,'STARTTIME',starttime,'ENDTIME',endtime,'INTERVAL',interval)
END

PRO SCC_BLOCKSCHED,atsfile=atsfile,auto=auto
names = uintarr(12)
ssr = dblarr(12)
data_loss = 0;
scc_avail_blocks,blockname,blocksize,blockcomments,blockstart

blkchoices=''
nblk = n_elements(blockname)
for icount=1,nblk do begin
 blkchoices=blkchoices + strtrim(blockname(icount-1),2) +' '
endfor

ssr_at_midnite = 0.0
ssr_remaining = 100.0

if keyword_set(atsfile) then begin
 print,' '
 print,'Reading Previous Daily schedule: ',atsfile
 scc_readats,atsfile,icount,year,month,daymon,hour,minute,second,funcode,numbyte,chksum,blkname
trymidnite:
 read,midniteblk, prompt='How many blocks from previous pass to midnight ? '

 schedlen = n_elements(blkname)
 if(schedlen lt midniteblk) then begin
  print,'Input schedule too short - Choose fewer number of blocks!'
  goto, trymidnite
 endif

 for icount = schedlen-midniteblk-1, schedlen-1 do begin
 w = where(blkname(icount) eq blockname,wcount)
 if(wcount eq 0) then begin
  print,blkname(icount) ,' NOT in block database'
  print,'Cannot estimate SSR usage'
  return
 endif
 if(wcount eq 1) then ssr_at_midnite = ssr_at_midnite + (wcount*blocksize(w(0)))
 if(wcount gt 1) then begin
  print,blkname(icount) ,'in block database multiple times - Check database!'
 endif
 endfor
endif else begin
 read,midniteblk, prompt='How many blocks from previous pass to midnight ? '

 for icount=1, midniteblk do begin
 tryagain:
 print,'Block ',strtrim(string(icount),2),' [',blkchoices,'] Enter 0-',strtrim(string(nblk-1),2)
 read, blkindex
 if(blkindex lt 0 or blkindex gt nblk - 1) then goto, tryagain
 print,blockname(blkindex)
 ssr_at_midnite = ssr_at_midnite + blocksize(blkindex)
 if(ssr_at_midnite gt 100.0) then begin
  print,'SSR1 is full! - Data Loss!'
  data_loss = 1;
 endif
 endfor
endelse

print, 'Index Blocks      Block Size(% of SSR1)       Comment'
for icount=1,nblk do begin
 print,format='(I2,4x,A12,1x,F5.1,1x,A50)', $
  icount-1,blockname(icount-1),blocksize(icount-1),blockcomments(icount-1)
endfor

print,' '
print,'Begin New Schedule with SSR at midnight :',ssr_at_midnite,'% Used'
endofpassblk=0u
read,endofpassblk,prompt='How many blocks from midnight to end of pass ? '

ssr_remaining = 100.0 - ssr_at_midnite
ave_blk_size = ssr_remaining/endofpassblk
auto_choice = -1
auto_index = -1
possible_choices = where(blocksize le ave_blk_size and blockstart eq 0,acount)
if(acount gt 0) then begin
 print,' '
 print,format='(16HChoices LE Ave (,F5.1,17H%) to End of Pass)',ave_blk_size
 print,format='(7HIndex :,12(5x,I2,6x))',possible_choices
 print,'Names :',blockname(possible_choices)
 auto_choice = max(blocksize(possible_choices)) 
 auto_index = where(blocksize eq auto_choice)
 print,format='(25HRecommend to End of Pass:,I2,1x,A12,F5.1,1H%)',auto_index,blockname(auto_index),blocksize(auto_index)
endif else begin
 print,'Recommend to End of Pass: No Block Choices <= Ave Size - Data Loss Expected if NO pass today'
 return
endelse

ave_blk_size = ssr_remaining/12.0
possible_choices = where(blocksize le ave_blk_size(0) and blockstart eq 0,acount)
if(acount gt 0) then begin
 print,' '
 print,format='(16HChoices LE Ave (,F5.1,16H%) to End of Day)',ave_blk_size
 print,format='(7HIndex :,12(5x,I2,6x))',possible_choices
 print,'Names :',blockname(possible_choices)
 day_choice = max(blocksize(possible_choices)) 
 day_index = where(blocksize eq auto_choice)
 print,format='(24HRecommend to End of Day:,I2,1X,A12,F5.1,1H%)',day_index,blockname(day_index),blocksize(day_index)
endif else begin
 print,'Recommend to End of Day: No Block Choices <= Ave Size - Data Loss Expected if NO pass today'
endelse

daycount = 0
if keyword_set(auto) then begin
 possible_choices = where(blockstart eq 1,acount)
 ; assume that each 120 min block is matched by a corresponding 115 min block
 ; in arrays provided by scc_avail_blocks
 names(0) = auto_index + n_elements(possible_choices) 
 names(1:endofpassblk-1) = auto_index
 for icount=1, endofpassblk do begin
  ssr_remaining = ssr_remaining - blocksize(auto_index)
  ssr(daycount) = ssr_remaining
  daycount = daycount + 1
 endfor
endif else begin
 for icount=1, endofpassblk do begin
 tryagain1:
 if (daycount eq 0) then begin
  possible_choices = where(blockstart eq 1,acount)
  print,' '
  print,'Choose 115 min block'
 endif else begin
  possible_choices = where(blockstart eq 0,acount)
 endelse
 blkindex=0
 readprompt='Block '+strtrim(string(icount),2)+' of '+strtrim(string(endofpassblk),2)+' Enter ['
 readprompt = readprompt + strtrim(string(possible_choices,format='(6I3)'),1)+'] : '
 read, blkindex, prompt=readprompt
 okchoice = where(blkindex eq possible_choices,okcount)
 if(okcount lt 0) then goto, tryagain1
 ssr_remaining = ssr_remaining - blocksize(blkindex)
 ssr(daycount) = ssr_remaining
 print,blockname(blkindex),' SSR_remaining :',ssr_remaining,'%'
 if(ssr_remaining  lt 0.0) then begin
  print,'SSR1 is full! - Data Loss Expected'
  data_loss = 1;
 endif
 names(daycount) = blkindex
 daycount = daycount + 1
endfor
endelse

ssr_at_midnite = 0.0
ssr_remaining = 100.0 - ssr_at_midnite
print,' '
print,'End of Pass Assumed - Reset SSR1 to 100% Available'
blk_to_nextmidnite = 12 - endofpassblk
if(blk_to_nextmidnite gt 0) then begin
for icount=1, blk_to_nextmidnite do begin
tryagain2:
 possible_choices = where(blockstart eq 0,acount)
 blkindex=0
 readprompt='Block '+strtrim(string((endofpassblk+icount)),2)+' of '+strtrim(string(endofpassblk+blk_to_nextmidnite),2)
 readprompt = readprompt +' Enter ['+strtrim(string(possible_choices,format='(6I3)'),1)+'] or -1 for new pass : '
read,blkindex,prompt=readprompt
if(blkindex lt 0) then begin
 print,'Ready to start New Pass'
 newpass=''
 read,newpass,prompt='Cancel New Pass (y or n) ? '
 if (newpass ne 'y' and newpass ne 'Y') then begin
  ssr_remaining = 100.0
  print,'New Pass - Setting SSR to 100% available'
 endif
endif
if(blkindex lt 0 or blkindex gt nblk - 1) then goto, tryagain2
ssr_remaining = ssr_remaining - blocksize(blkindex)
print,blockname(blkindex),' SSR_remaining :',ssr_remaining,'%'
if(ssr_remaining  lt 0.0) then begin
 print,'SSR1 is full! - Data Loss Expected'
 data_loss = 1;
endif
names(daycount) = blkindex
ssr(daycount) = ssr_remaining
daycount = daycount + 1
endfor
endif

print,' '
print,'Midnight to Midnight'
print,'Time      Block        SSR%      Comment'
for icount=0,11 do begin
starttime=icount*200
endtime=(icount+1)*200
print, format='(I04,1H-,I04,1x,A12,1x,F5.1,A30)',starttime,endtime,blockname(names(icount)),ssr(icount),blockcomments(names(icount))
endfor

if(data_loss eq 0) then scc_schedule,blockname(names),blockcomments(names),/writefile $
else print,'This schedule has data loss! Aborting schedule generation'

END
