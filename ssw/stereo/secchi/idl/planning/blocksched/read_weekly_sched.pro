;+
; $Id: read_weekly_sched.pro,v 1.20 2018/01/10 14:59:45 secchia Exp $
; Project     : STEREO - SECCHI 
;
; Name        : read_weekly_sched
;
; Purpose     : Read in time from stereo weekly sechedules.
;
; Explanation : 
;
; Use         : passinfo=read_weekly_sched('a',2009,012)
;
;      Example: passinfo=read_weekly_sched('a',2009,012)
;
; Inputs :  sc =spacecraft ( a or b) 
;   	    yr = year (2009) doy = day of year -OR- yr = UTC structure
;
; Outputs     : array of structures of pass information from weekly schedule file which includes input day
;
; Keywords:
;   	/TAI	Return values in seconds rather than string
;   	NDAYS=	Number of days following input date to include (defaults to 7 from file of input day)
;
; Calls       : 
;
; Category    : Planning 
;
; Prev. Hist. : None.
;
; Written     : Lynn Simpson, NRL, Jan 2009.
; $Log: read_weekly_sched.pro,v $
; Revision 1.20  2018/01/10 14:59:45  secchia
; added search for HI offpoint
;
; Revision 1.19  2013/10/21 12:53:36  secchib
; added roll for comet ISON observations
;
; Revision 1.18  2011/06/17 19:06:50  secchib
; fix loop so ndays works; fix missingpbs
;
; Revision 1.17  2011/05/27 17:45:40  secchia
; nr - clarify print messages
;
; Revision 1.16  2010/11/08 15:10:17  secchib
; added check for missing playbacks
;
; Revision 1.15  2010/05/19 13:44:07  secchib
; reads pbrate form all playback starts
;
; Revision 1.14  2010/04/22 13:50:56  mcnutt
; added hi offset calibration
;
; Revision 1.13  2010/04/19 20:53:31  secchib
; add gtcal
;
; Revision 1.12  2010/01/08 18:23:29  mcnutt
; corrected step cal do to cal being scheduled at wrong time on B 2010-01-05
;
; Revision 1.11  2010/01/07 18:34:34  secchib
; change selection of next weeks file
;
; Revision 1.10  2009/11/23 18:27:31  secchia
; removed UT from SECCHI Stepped search
;
; Revision 1.9  2009/08/11 17:32:38  secchia
; reads previous week is week eq yrdoy
;
; Revision 1.8  2009/07/29 17:33:43  mcnutt
; change search for next weeks file
;
; Revision 1.7  2009/07/06 18:04:41  nathan
; add /TAI and allow different input CDS time structure
;
; Revision 1.6  2009/05/28 14:50:19  secchia
; read weekly schedule form calliope not venus
;
; Revision 1.5  2009/05/11 17:28:01  secchib
; removes passes with no playback scheduled
;
; Revision 1.4  2009/03/09 13:52:34  secchib
; removes last start of track if end of track is the next weeks sched and not available yet
;
; Revision 1.3  2009/03/06 13:22:29  secchib
; looks for for BOT before setting track times
;
; Revision 1.2  2009/03/05 19:50:24  secchib
; looks for EOT in next weeks schedule if missing
;
; Revision 1.1  2009/01/22 15:03:48  mcnutt
; returns a structure ocontaining weekly pass info
;



function sec2hms,secs,hhmm=hhmm
  hrs=fix(secs/3600)
  mins=fix((secs - hrs*3600 ) /60)
  sec=fix(secs -hrs*3600 - mins*60)
  hhmmss=string(hrs,'(i2.2)')+':'+string(mins,'(i2.2)')+':'+string(sec,'(i2.2)')
  if keyword_set(hhmm)then  hhmmss=string(hrs,'(i2.2)')+':'+string(mins,'(i2.2)')
  return,hhmmss
end

function read_weekly_sched,sc,yr0,doy, TAI=tai, missingpbs=missingpbs, NDAYS=ndays, sidelobe=sidelobe

IF datatype(yr0) EQ 'STC' THEN BEGIN
    yyyymmdd=utc2str(yr0)
    doy=utc2doy(yr0)
    yr=strmid(yyyymmdd,0,4)
ENDIF ELSE yr=yr0
yr=string(yr,'(i4.4)')
if(strupcase(sc) eq 'B') then sc = 'STB'
if(strupcase(sc) eq 'A') then sc = 'STA'


;  if(sc eq 'STB') then wsdir='/net/venus/secchi3/sds/behind/data_products/weekly_schedule/*';+yr+'*'
;  if(sc eq 'STA') then wsdir='/net/venus/secchi3/sds/ahead/data_products/weekly_schedule/*';+yr+'*'

  if(sc eq 'STB') then wsdir='/net/calliope/data/secchi/sds/behind/data_products/weekly_schedule/*';+yr+'*'
  if(sc eq 'STA') then wsdir='/net/calliope/data/secchi/sds/ahead/data_products/weekly_schedule/*';+yr+'*'
  wsfiles=findfile(wsdir)
  fdays=rstrmid(wsfiles,5,9)
  wsfiles=wsfiles[uniq(fdays)]
  nwsfiles=n_elements(wsfiles)
  weeks=long(strmid(wsfiles,strlen(wsfiles(0))-14,4)*1000l)+long(strmid(wsfiles,strlen(wsfiles(0))-9,3))
  yrdoy=long(string(yr,'(i4.4)')+string(doy,'(i3.3)'))
  z=where(weeks gt yrdoy,nz)
  ;z=(z(n_elements(z)-1))
  IF nz GT 0 THEN wsfile=wsfiles(z[0]-1) ELSE wsfile=wsfiles[nwsfiles-1]
  

  vers=strmid(wsfiles,strlen(wsfiles(0))-5,2)
  print,'Reading ',wsfile

  times=strarr(13,100) & pbrate=strarr(100)
  line='' & line2=''
  d=0L

;0123456789012345678901234567890123456789
; 169 0145 0245 0645 0700  DSS-26  STB    SSR DUMP/UNATT
 
;01234567890123456789012345678901234567890123456789012345678901234567890123456789
;2008:007:12:48:00   UT Start of Playback of S/C   24      0               720/16.1        
;                    Partitions                                                            
;2008:007:13:08:00   UT Start of Playback of       24      0               720/16.1        
;                    Science (and S/C) Partitions                                          
;2008:007:16:27:00   UT Stop of Playback           24      0               720/16.1        

stepcal='' & mdump=''& gtcal='' & hioffset='' & roll=''
bt=0 & et=0
     openr,1,wsfile
     while (not eof(1)) do begin
        readf,1,line
        if(strpos(line,'RT BOT') gt -1) then begin
	  times(0,d)=strmid(line,0,17)
;          bt=bt+1
          if(strpos(line,'Sidelobe Test') gt -1) then times(11,d)='test track'
          if keyword_set(sidelobe) then begin 
             doyn=fix(strmid(strmid(line,0,17),5,3))
             tmp=strsplit(strmid(line,74,9),'/',/extract)
             pbrate(d)= tmp[0]
          endif
        endif
        if(strpos(line,'UT Config C&DH for Track') gt -1) then bt=bt+1
        if bt ge 1 then begin
          if(strpos(line,'UT Start of Playback') gt -1) then begin
             if ~keyword_set(sidelobe) then pbrate(d)=strmid(line,74,3) 
             if(strpos(line,'UT Start of Playback of S/C') gt -1) then begin
                times(1,d)=strmid(line,0,17)
             endif else begin
                readf,1,line2
                if (strpos(line2,'Science') gt -1) or (strpos(line2,'SECCHI') gt -1) then BEGIN
		    time2= strmid(line,0,17)
		    times[2,d]=time2
		    doyn=fix(strmid(time2,5,3))
		ENDIF
    	    	print,line
             endelse
	  endif
          if(strpos(line,'UT Stop of Playback') gt -1) then times(3,d)=strmid(line,0,17)
          if(strpos(line,'RT EOT') gt -1) then begin
             times(4,d)=strmid(line,0,17)
	     d=d+1
             et=et+1
          endif
	  
          if(strpos(line,'SECCHI Stepped')  	    	gt -1 and strpos(line,'SECCHI Stepped') lt 40)	    	    then times(5,d)=strmid(line,0,17)
          if(strpos(line,'RT Pre-Burn Configuration') 	gt -1 and strpos(line,'RT Pre-Burn Configuration') lt 40)   then times(6,d)=strmid(line,0,17)
          if(strpos(line,'SECCHI GT Calibration')   	gt -1 and strpos(line,'SECCHI GT Calibration') lt 40)	    then times(7,d)=strmid(line,0,17)
          if(strpos(line,'SECCHI HI Stray Light')      	gt -1 and strpos(line,'SECCHI HI Stray Light') lt 40)  	    then times(8,d)=strmid(line,0,17)
          if(strpos(line,'SECCHI Stray Light')      	gt -1 and strpos(line,'SECCHI Stray Light') lt 40)  	    then times(8,d)=strmid(line,0,17)
          if(strpos(line,'Degree Roll')  	    	gt -1 and strpos(line,'Degree Roll') lt 40)	    	    then times(9,d)=strmid(line,0,17)
          if(strpos(line,'DSN Uplink Sweep Complete') gt -1 and strpos(line,'DSN Uplink Sweep Complete') lt 40) then times(10,d)=strmid(line,0,17)
          if(strpos(line,'Disable Instrument POC') gt -1 and strpos(line,'Disable Instrument POC') lt 40) then times(11,d)=strmid(line,0,17)
          if(strpos(line,'Enable Instrument POC') gt -1 and strpos(line,'Enable Instrument POC') lt 40) then times(12,d)=strmid(line,0,17)
       endif
     endwhile
     close,1
     
doy2=doyn
IF keyword_set(NDAYS) THEN doy2=doy+ndays     
 ;

IF nz GT 0 THEN next=z[0] ELSE next =nwsfiles
WHILE (et lt bt or doyn LT doy2) and next LT nwsfiles DO begin
    print,'Reading ',wsfiles[next]
    openr,1,wsfiles[next]
    while ((et lt bt) or (doyn LT doy2)) and not eof(1) do begin
        readf,1,line
        if(strpos(line,'RT BOT') gt -1) then begin
	  times(0,d)=strmid(line,0,17)
          bt=bt+1
          if(strpos(line,'Sidelobe Test') gt -1) then times(11,d)='test track'
          if keyword_set(sidelobe) then doyn=fix(strmid(strmid(line,0,17),5,3))
       endif
        if(strpos(line,'UT Start of Playback') gt -1) then begin
             if ~keyword_set(sidelobe) then pbrate(d)=strmid(line,74,3) else begin
               tmp=strsplit(strmid(line,74,9),'/',/extract)
print,tmp[0]
if tmp[0]*1.0 eq 0. then stop
               pbrate(d)= tmp[0]
print,pbrate(d)
             endelse
           if(strpos(line,'UT Start of Playback of S/C') gt -1) then begin
             times(1,d)=strmid(line,0,17)
          endif else begin
            	readf,1,line2
                if (strpos(line2,'Science') gt -1) or (strpos(line2,'SECCHI') gt -1) then BEGIN
		    time2= strmid(line,0,17)
		    times[2,d]=time2
		    doyn=fix(strmid(time2,5,3))
		ENDIF
    	    	print,line
          endelse
	endif
        if(strpos(line,'UT Stop of Playback') gt -1) then times(3,d)=strmid(line,0,17)
        if(strpos(line,'RT EOT') gt -1) then begin
print,line
           times(4,d)=strmid(line,0,17)
	   d=d+1
           et=et+1
        endif
        if(strpos(line,'SECCHI Stepped') gt -1 and strpos(line,'SECCHI Stepped') lt 40)then times(5,d)=strmid(line,0,17)
        if(strpos(line,'RT Pre-Burn Configuration') gt -1)then times(6,d)=strmid(line,0,17)
        if(strpos(line,'SECCHI GT Calibration') gt -1 and strpos(line,'SECCHI GT Calibration') lt 40)then times(7,d)=strmid(line,0,17)
        if(strpos(line,'SECCHI HI Stray Light') gt -1 and strpos(line,'SECCHI HI Stray Light') lt 40)then times(8,d)=strmid(line,0,17)
        if(strpos(line,'SECCHI Stray Light') gt -1 and strpos(line,'SECCHI Stray Light') lt 40)then times(8,d)=strmid(line,0,17)
        if(strpos(line,'Degree Roll') gt -1 and strpos(line,'Degree Roll') lt 40) then times(9,d)=strmid(line,0,17)
        if(strpos(line,'DSN Uplink Sweep Complete') gt -1 and strpos(line,'DSN Uplink Sweep Complete') lt 40) then times(10,d)=strmid(line,0,17)
        if(strpos(line,'Disable Instrument POC') gt -1 and strpos(line,'Disable Instrument POC') lt 40) then times(11,d)=strmid(line,0,17)
        if(strpos(line,'Enable Instrument POC') gt -1 and strpos(line,'Enable Instrument POC') lt 40) then times(12,d)=strmid(line,0,17)
    endwhile
    close,1
    next=next+1
endWHILE

  trackwnopbs=where(times(2,*) eq ''and times(0,*) ne '')
  if trackwnopbs(0) gt -1 then begin
     print,'!!!!!!!!!!!! Some tracks have no playbacks !!!!!!!!!!!!'
     print,times(0,trackwnopbs)
     print,'*************************************************'
     missingpbs=reform(times(0,trackwnopbs))
  endif ELSE missingpbs=''
  if ~keyword_set(sidelobe) then begin
     pbrate=fix(pbrate(where(times(2,*) ne '')))
     times=times(*,where(times(2,*) ne '')) ; remove tracks with no playbacks scheduled
  endif else begin ;remove sidelobe test tracks
     times=times(*,where(times(11,*) ne 'test track'))
     pbrate=pbrate(where(times(0,*) ne ''))
     times=times(*,where(times(0,*) ne ''))
  endelse
  ;pbrate=fix(pbrate(where(pbrate ne ''))) & times=times(*,where(pbrate ne ''))
  if bt gt et then begin  ; remove start of track which crosses over into next week if schedule not available
     pbrate=pbrate(0:et-1)
     times=times(*,0:et-1)
  endif
  for n=0,4 do begin
     time=reform(times(n,*))
     tmp=doy2utc(fix(strmid(time,5,3)),fix(strmid(time,0,4))) & tmp.time=(long(strmid(time,9,2))*3600+long(strmid(time,12,2))*60+long(strmid(time,15,2)))*1e3 
     if n eq 0 then begin
        BOT=utc2tai(tmp)
        BOTSTR=utc2str(tmp)
     endif
     if n eq 1 then SCPB=utc2tai(tmp)
     if n eq 2 then SPB=utc2tai(tmp)
     if n eq 3 then EPB=utc2tai(tmp)
     if n eq 4 then EOT=utc2tai(tmp)
  endfor

;1. BOT to Start SC partition playback
;2. Start SC partition playback to Start Science playback
;3. Start science playback to Stop playback
;4. BOT to EOT
IF keyword_set(TAI) THEN BEGIN
    pinfo={BOT:'',BOT2SCPB:0.,SCPB2SPB:0.,SPB2EPB:0.,BOT2EOT:0.,pbrate:0.,SPB:'',EPB:'',stepcal:'',mdump:'',gtcal:'',hioffset:'',roll:'',sweepend:'',disableque:'',enableque:''}
    passinfo=replicate(pinfo, n_elements(botstr))
    passinfo.BOT=botstr
    passinfo.BOT2SCPB=(SCPB-BOT)
    passinfo.SCPB2SPB=(SPB-SCPB)
    passinfo.SPB2EPB=(EPB-SPB)
    passinfo.BOT2EOT=(EOT-BOT)
    passinfo.pbrate=float(pbrate)
    passinfo.SPB=reform(times(2,*))
    passinfo.EPB=reform(times(3,*))
    passinfo.stepcal=reform(times(5,*))
    passinfo.mdump=reform(times(6,*))
    passinfo.gtcal=reform(times(7,*))
    passinfo.hioffset=reform(times(8,*))
    passinfo.roll=reform(times(9,*))
    passinfo.sweepend=reform(times(10,*))
    passinfo.disableque=reform(times(11,*))
    passinfo.enableque=reform(times(12,*))
ENDIF ELSE BEGIN
   pinfo={BOT:'',BOT2SCPB:'',SCPB2SPB:'',SPB2EPB:'',BOT2EOT:'',pbrate:'',SPB:'',EPB:'',stepcal:'',mdump:'',gtcal:'',hioffset:'',roll:'',sweepend:'',disableque:'',enableque:''}
   passinfo=replicate(pinfo, n_elements(botstr))
   passinfo.BOT=botstr
   passinfo.BOT2SCPB=sec2hms(SCPB-BOT)
   passinfo.SCPB2SPB=sec2hms(SPB-SCPB)
   passinfo.SPB2EPB=sec2hms(EPB-SPB)
   passinfo.BOT2EOT=sec2hms(EOT-BOT)
   passinfo.pbrate=pbrate
   passinfo.SPB=reform(times(2,*))
   passinfo.EPB=reform(times(3,*))
   passinfo.stepcal=reform(times(5,*))
   passinfo.mdump=reform(times(6,*))
   passinfo.gtcal=reform(times(7,*))
   passinfo.hioffset=reform(times(8,*))
   passinfo.roll=reform(times(9,*))
   passinfo.sweepend=reform(times(10,*))
   passinfo.disableque=reform(times(11,*))
   passinfo.enableque=reform(times(12,*))
ENDELSE

return,passinfo

end
