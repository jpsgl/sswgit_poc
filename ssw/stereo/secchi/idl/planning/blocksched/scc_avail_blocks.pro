; Database of schedule blocks
; $Id: scc_avail_blocks.pro,v 1.63 2018/01/10 14:59:14 secchia Exp $
;
; Keywords:
;   active = set to use active block schedules (defaults to PT/flight/operations/BSF/quiet/event_detect/*/sync36 schedules 080429)
;   campaign = set for Jan 08 SECCHI compain block schedules 
;
; $Log: scc_avail_blocks.pro,v $
; Revision 1.63  2018/01/10 14:59:14  secchia
; added hioffpoint schedules
;
; Revision 1.62  2017/05/02 11:51:23  secchia
; corrected cmment length to work with plotting routine
;
; Revision 1.61  2017/04/27 17:37:44  mcnutt
; corrected new euvi304 and hi2spw filenames
;
; Revision 1.60  2017/04/26 14:14:44  secchia
; change euvi304 and hi2_2277 comments for conform to ploting string format
;
; Revision 1.59  2017/04/25 15:16:03  secchia
; added euvi304spw and hi2_2277, set default to 160kbps schedules
;
; Revision 1.58  2016/10/03 12:46:38  secchia
; added ssr2size for 120kpbs/27hr
;
; Revision 1.57  2015/12/21 22:30:40  mcnutt
; added 5 hour step cal
;
; Revision 1.56  2014/03/27 17:59:24  secchib
; corrected cor2 mdump calibration blocks
;
; Revision 1.55  2014/03/27 13:07:56  mcnutt
; added cor2cal blocks to mdump
;
; Revision 1.54  2013/02/08 15:06:19  secchib
; update comments115ssr2 to match comments120ssr2 for psym on plots to work correctly
;
; Revision 1.53  2013/02/01 12:18:16  secchib
; added 130115_1.bsf to 1min offset list
;
; Revision 1.52  2013/01/24 23:58:06  nathan
; update sizes120ssr2 based on actual percentfull telemetry; also update comments120ssr2
;
; Revision 1.51  2013/01/24 23:04:31  nathan
; added whitespace
;
; Revision 1.50  2013/01/14 18:12:49  mcnutt
; corrected 120kbps/30hr block
;
; Revision 1.49  2013/01/08 17:55:44  secchib
; corrected keyword typo
;
; Revision 1.48  2013/01/08 16:17:36  secchib
; fixed error on line 452
;
; Revision 1.47  2013/01/08 16:04:16  mcnutt
; to use 120kbps/30hr block use keyword u120kbps30hr
;
; Revision 1.46  2013/01/08 13:05:09  mcnutt
; added 120kbps/30ssr2/120 bsf
;
; Revision 1.45  2012/10/22 15:12:22  mcnutt
; added 120kbps 27hr block
;
; Revision 1.44  2011/11/22 15:44:12  secchib
; updated step_icer0 block
;
; Revision 1.43  2011/10/21 17:17:10  secchia
; changed step calibration block for icer0
;
; Revision 1.42  2011/10/19 17:51:22  secchia
; corrected if statement step_icer0
;
; Revision 1.41  2011/10/19 17:42:11  mcnutt
; added euvi icer0 step cal block requires keyword step_icer0 to be set
;
; Revision 1.40  2011/07/22 20:37:00  nathan
; added 160kbps/16ssr2/115 block
;
; Revision 1.39  2011/06/24 20:07:03  nathan
; add 240sr2/22ssr2 to u160kbps
;
; Revision 1.38  2011/06/24 17:13:04  nathan
; add /U160KBPS and belonging blocks (ssr2 only)
;
; Revision 1.37  2010/10/04 17:44:37  mcnutt
; added hi cal blocks
;
; Revision 1.36  2010/07/26 13:21:41  secchib
; added new step cal 100524_1.bsf
;
; Revision 1.35  2010/06/07 12:27:18  secchib
; change block schedules directories to 240kbps
;
; Revision 1.34  2010/05/19 18:14:22  secchib
; added 100430_1.bsf to the 30 second offset blocks
;
; Revision 1.33  2010/05/19 13:46:15  secchia
; added reduced step cal block
;
; Revision 1.32  2010/05/17 18:56:40  secchib
; added 51 hour blocks changed 660hr keyword to u51hr
;
; Revision 1.31  2010/05/04 18:18:32  secchib
; added 240kbps/30hr blocks
;
; Revision 1.30  2010/04/22 13:50:48  mcnutt
; added hi offset calibration
;
; Revision 1.29  2010/04/21 20:11:13  secchib
; corrected typo
;
; Revision 1.28  2010/04/21 20:01:35  mcnutt
; added cor only mdump schedule block
;
; Revision 1.27  2010/04/19 20:53:21  secchib
; add gtcal
;
; Revision 1.26  2010/04/07 14:36:06  secchib
; corrected 240kbps ssr2 comment
;
; Revision 1.25  2010/03/29 17:16:24  mcnutt
; moved 240kbps/31 to default 360kbps/36 to historical and added mdump,step cal blocks to historical
;
; Revision 1.24  2010/03/23 15:49:00  nathan
; replaced 090911_1.bsf with 090911_3.bsf
;
; Revision 1.23  2010/03/16 16:21:37  secchib
; added 091215_1.bsf to 30 second offset list
;
; Revision 1.22  2010/01/07 18:04:11  secchia
; added jop225 schedule blocks works with keyword jop225
;
; Revision 1.21  2009/12/22 16:33:11  secchib
; change u240kpbs to only change last schedule block
;
; Revision 1.20  2009/12/18 18:13:44  secchia
; added 360kbps/22ssr2/evtest schedules
;
; Revision 1.19  2009/12/18 17:46:34  secchib
; added 240kbps 31 hr block with  keyword u240kbps
;
; Revision 1.18  2009/09/14 13:21:13  secchib
; added new 60hr blocks
;
; Revision 1.17  2009/08/19 16:08:47  secchia
; corrected 360kbps 115 blocks
;
; Revision 1.16  2009/08/11 17:30:39  secchia
; change default schedules to 360kbps
;
; Revision 1.15  2009/08/10 18:08:54  mcnutt
; corrected u51hr schedules 115 and ssr2 schedules needed to be the same length
;
; Revision 1.14  2009/08/05 11:48:36  mcnutt
; added new names for 360kbps blocks
;
; Revision 1.13  2009/07/30 13:05:23  mcnutt
; added changes for 360kbps blocks and ssr2size
;
; Revision 1.12  2009/07/20 12:40:30  secchib
; corrected error in last commit
;
; Revision 1.11  2009/07/13 18:25:37  secchib
; commented out hi2spw blocks
;
; Revision 1.10  2009/07/06 21:25:50  nathan
; fixed default ssr2 vols
;
; Revision 1.9  2009/07/06 18:50:22  mcnutt
; changed SSR1 size for 22 ssr2 hi2spw block
;
; Revision 1.8  2009/07/06 18:06:04  nathan
; Moved mdump and calroll blocks to scc_avail_blocks and modified make_schedules
; accordingly; also updated comments to include actual (new) dir structure
;
; Revision 1.7  2009/07/06 17:13:22  mcnutt
; added 30/120 and 36/120 hi2spw blocks to 1 minute offset list
;
; Revision 1.6  2009/07/06 13:47:45  mcnutt
; updated for new hi2spw blocks
;
; Revision 1.5  2009/07/02 14:49:09  nathan
; added comments, fixed comments to work with make_schedule plot
;
; Revision 1.4  2009/06/24 18:39:48  nathan
; Complete rewrite to be compatible with make_schedules.pro; includes
; blocks in make_schedules.pro,v 1.64 plus all other blocks ever
; used since 2008/01/01 if /HISTORIC is used
;
; Revision 1.3  2007/07/31 22:20:23  wang
; Update block sizes and more interface cleanup
; Schedule block data from Ed Esfandiari email 24 July 2007
;
;
PRO SCC_AVAIL_BLOCKS,	names120ssr2,sizes120ssr2,comments120ssr2, $
    	    	    	names115,sizes115,comments115, $
    	    	    	names120,sizes120,comments120, $
			names1minoffset,$
			names115ssr2,sizes115ssr2,comments115ssr2, $
			names30secoffset,sizesssr2,sizesssr2_1,$
			JOPOFFSET=jopoffset, HISTORICAL=historical, MDUMP=mdump, CALROLL=calroll, GTCAL=gtcal,HIOFFSET=hioffset, $
			u51hr=u51hr, ACTIVE=active, CAMPAIGN=campaign, PERCENT=percent, _EXTRA=_extra,jop225=jop225, $
			HISKY=HISKY,HILED=HILED, U160KBPS=u160kbps, step_icer0=step_icer0, u120kbps30hr=u120kbps30hr, $
			euvi304spw=euvi304spw,hi2_2277=hi2_2277,U240KBPS=u240kbps,hi22277n304spw=hi22277n304spw
;    	    	    	blockstart

;maxpasslen=[12,30,40]
  maxpasslen=[25,29,35,50];,100]
;defined for blocks after 200907 new make_schedules need them to be defined.
  sizesssr2=[0,0,0,0]
  names115ssr2=['','','','']
  comments115ssr2=['','','','']
  sizes115ssr2=[0,0,0,0]
  sizesssr2_1=[0,0,0,0]

IF keyword_set(HISTORICAL) THEN BEGIN
    message,'Using HISTORICAL blocks.',/info
    names120=['070712_4.bsf']
    names115=['070712_3.bsf']
    sizes120=[44.0]
    sizes115=[44.2]
;maxpasslen=[22,26,30,31,50]
;maxpasslen=[25,29,30,35,50]
    comments115=['']
    comments120=['A/synoptic/720/qsun/1contact_31hr']
    names120=[names120,'070726_2.bsf','070621_1.bsf','070712_1.bsf','070720_3.bsf','070720_4.bsf']
    names115=[names115,'070726_1.bsf','070621_2.bsf','070712_2.bsf','070720_2.bsf','070720_1.bsf']
    sizes120= [sizes120,63.9,52.4,47.3,44.9,39.1]
    sizes115=[sizes115,77.0,66.4,47.1,44.5,38.7]
    comments120=[comments120,'22 hr 120 min','26 hr 120 min','30 hr 120 min','31 hr 120 min','36 hr 120 min']
    comments115=[comments115,'22 hr 115 min','26 hr 115 min','30 hr 115 min','31 hr 115 min','36 hr 115 min']

    names115=[names115,'071126_1.bsf','071126_4.bsf','071126_7.bsf','071127_1.bsf']
    names120ssr2=['071126_2.bsf','071126_5.bsf','071126_8.bsf','071127_2.bsf']
    names120=[names120,'071126_3.bsf','071126_6.bsf','071126_9.bsf','071127_3.bsf']
    sizes115=[sizes115,75.7,66.1,47.2,38.8]
    sizes120ssr2=[63.6,52.1,47.0,38.6]
    sizes120= [sizes120,63.6,52.6,47.4,39.0]
    comments120=[comments120,'quiet/no_event_detect/22/120/no_ssr2','quiet/no_event_detect/26/120/no_ssr2','quiet/no_event_detect/30/120/no_ssr2','quiet/no_event_detect/36/120/no_ssr2']
    comments115=[comments115,'','','','']
    comments120ssr2=['','','','']
    names115=[names115,'071130_1.bsf','071130_4.bsf','071130_7.bsf','071201_1.bsf']
    names120ssr2=[names120ssr2,'071130_2.bsf','071130_5.bsf','071130_8.bsf','071201_2.bsf']
    names120=[names120,'071130_3.bsf','071130_6.bsf','071130_9.bsf','071201_3.bsf']
    sizes120ssr2=[sizes120ssr2,63.6,52.1,47.0,38.6]
    sizes115=[sizes115,75.7,66.1,47.2,38.8]
    sizes120= [sizes120,63.6,52.6,47.4,39.0]
    comments120=[comments120,'quiet/event_detect/22/120/no_ssr2/071130_3.bsf','quiet/event_detect/26/120/no_ssr2/071130_3.bsf','quiet/event_detect/30/120/no_ssr2/071130_3.bsf','quiet/event_detect/36/120/no_ssr2/071130_3.bsf']
    comments115=[comments115,'','','','']
    comments120ssr2=[comments120ssr2,'','','','']
    
; campaign/0801/jp/304A/
    names115=[names115,'080107_5.bsf']
    sizes115=[sizes115,105.3]
    names120=[names120,'080107_8.bsf','080107_6.bsf']
    sizes120=[sizes120,79.3,93]
    comments115=[comments115,'171A campaign 115 min']
    comments120=[comments120,'171A campaign 120 min','171A campaign 120 min 150sCad']

; quiet/event_detect/36/120/ssr2_cal
    names120=[names120,'080304_1.bsf']
    sizes120=[sizes120,39]
    comments120=[comments120,'quiet/event_detect/36/120/ssr2_cal']
    
; quiet/no_event_detect/22/120/ssr2_304*
    names120=[names120,'080709_1.bsf','080905_2.bsf','080909_1.bsf']
    sizes120=[sizes120,63.2,52,53]
    comments120=[comments120,'quiet/no_event_detect/22/120/ssr2_304','quiet/no_event_detect/22/120/ssr2_304_Jones_test','quiet/no_event_detect/22/120/ssr2_304_Jones_test1']
    
; quiet/no_event_detect/26/120/ssr2_304_Jones/
    names120=[names120,'080915_1.bsf']
    sizes120=[sizes120,51.7]
    comments120=[comments120,'quiet/no_event_detect/26/120/ssr2_304_Jones']
    
; quiet/event_detect/26/120/ssr2/sync36_304A_eclipse_support/
    names120=[names120,'080715_1.bsf']
    sizes120=[sizes120,52.1]
    comments120=[comments120,'quiet/event_detect/26/120/ssr2/sync36_304A_eclipse_support']
    
; jop214/
    names120=[names120,'080904_1.bsf']
    sizes120=[sizes120,52]
    comments120=[comments120,'jop214']

; quiet/event_detect/30/115/sync36/hi1_sub/
    names115=[names115,'081106_1.bsf']
    sizes115=[sizes115,58.8]
    comments115=[comments115,'quiet/event_detect/30/115/sync36/hi1_sub']
    
; quiet/event_detect/30/120/ssr2/sync36/cor1_test/
    names120=[names120,'081120_1.bsf']
    sizes120=[sizes120,47]
    comments120=[comments120,'quiet/event_detect/30/120/ssr2/sync36/cor1_test']
    
; COR1/HI_RES/
    names120=[names120,'090107_1.bsf','090105_1.bsf','090105_2.bsf']
    sizes120=[sizes120,79.7,156.5,53.8]
    comments120=[comments120,'COR1/HI_RES/60min','COR1/HI_RES','COR1/HI_RES/40min']
    
; JOP_McIntosh/SSR1/[304,171]hi_cad
    names120=[names120,'090202_1.bsf','090202_2.bsf']
    sizes120=[sizes120,65.1,61.7]
    comments120=[comments120,'JOP_McIntosh/SSR1/304hi_cad','JOP_McIntosh/SSR1/171hi_cad']

;change 080507 to use 36 hrs sync schedule blocks
  names115=[names115,'080429_6.bsf','080429_3.bsf','080502_1.bsf','071201_1.bsf']
  names120ssr2=[names120ssr2,'080429_7.bsf','080429_4.bsf','080502_2.bsf','071201_2.bsf']
  names120=[names120,'080429_8.bsf','080429_5.bsf','080502_3.bsf','071201_3.bsf']
  sizes115=[sizes115,75.7,66.1,47.2,38.8]
  sizes120ssr2=[sizes120ssr2,63.6,52.1,47.0,38.6];19.3]
  sizes120= [sizes120,63.6,52.6,47.4,39.0];19.3]
  comments115=[comments115,'22 hr 115 min quiet','26 hr 115 min quiet','30 hr 115 min quiet','36 hr 115 min quiet']
  comments120ssr2=[comments120ssr2,'22 hr 120 min quiet','26 hr 120 min quiet','30 hr 120 min quiet','36 hr 120 min quiet']
  comments120=[comments120,'quiet/event_detect/22/120/no_ssr2/sync36','quiet/event_detect/26/120/no_ssr2/sync36','quiet/event_detect/30/120/no_ssr2/sync36','quiet/event_detect/36/120/no_ssr2']

; 120kbps/quiet/event_detect/36/120/no_ssr2/old_cor2spw/
    names120=[names120,'090129_3.bsf']
    sizes120=[sizes120,19.3];19.3]
    comments120=[comments120,'120kbps/quiet/event_detect/36/120/no_ssr2/old_cor2spw']
  
; JOP216/
    names120=[names120,'090218_1.bsf']
    sizes120=[sizes120,85.5]
    comments120=[comments120,'JOP216']
  
; jop214/cor1_512/
    names120=[names120,'090416_1.bsf']
    sizes120=[sizes120,46.8]
    comments120=[comments120,'jop214/cor1_512']
    
; quiet/event_detect/22/120/ssr2/sync36/cor1_512/hi2spw/
    names120=[names120,'090603_2.bsf']
    sizes120=[sizes120,44.9]
    comments120=[comments120,'jop214/cor1_512']

; mdumps and step cals
    names120=[names120,'090519_3.bsf','090519_2.bsf','090526_1.bsf','071207_1.bsf','100510_1.bsf','100524_1.bsf','110921_1.bsf','111022_1.bsf',$
            '111102_1.bsf','151222_0.bsf']
    sizes120=[sizes120,213.7,168.9,200.1,206.6,78.191,78.191,80.743]
    comments120=[comments120,'mdumps/nospin/','mdumps/withspin/','mdumps/spin_and_cor2/','stepped_cal_rolls/3hr/','stepped_cal_rolls/3hr/reduced_cor/old/',$
    'stepped_cal_rolls/3hr/reduced_cor/','stepped_cal_rolls/3hr/euvi_icer0/old/','stepped_cal_rolls/3hr/euvi_icer0/old','stepped_cal_rolls/3hr/eui_icer0/',$
    'stepped_cal_rolls/5hr/']

;change 090408 to use cor1_512 schedule blocks
  names115=[names115,'090408_1.bsf','090406_2.bsf','090407_1.bsf']
  names120ssr2=[names120ssr2,'090408_2.bsf','090313_1.bsf','090407_3.bsf']
  names120=[names120,'090408_3.bsf','090406_1.bsf','090407_2.bsf']
  sizes115=[sizes115,60.5,42,35.8]
  sizes120ssr2=[sizes120ssr2,47.1,42.0,35.5];19.3]
  sizes120= [sizes120,47.1,42.2,36.0];19.3]
  comments115=	    [comments115,'480kbps/22/115', '480kbps/30/115', '480kbps/36/115']
  comments120=	    [comments120,'480kbps/22/120', '480kbps/30/120', '480kbps/36/120']
  comments120ssr2=  [comments120ssr2,'480kbps/22/ssr2','480kbps/30/ssr2','480kbps/36/ssr2']

; change 090712 to use cor1_512 new hi2spw (waiting for ip.gz to be corrected before setting as default
  names115=[names115,'090702_1.bsf','090702_4.bsf','090702_7.bsf']
  names120=[names120,'090702_2.bsf','090702_5.bsf','090702_8.bsf']
  names120ssr2=[names120ssr2,'090603_2.bsf','090702_6.bsf','090702_9.bsf']
  sizes115=[sizes115,60.5,42.0,35.8]
  sizes120= [sizes120,47.1,42.2,36.0];19.3]
  sizes120ssr2=[sizes120ssr2,47.1,42.0,35.5];19.3]
  comments115=	    [comments115,'480kbps/22/115/hi2spw', '480kbps/30/115/hi2spw', '480kbps/36/115/hi2spw']
  comments120=	    [comments120,'480kbps/22/120/hi2spw', '480kbps/30/120/hi2spw', '480kbps/36/120/hi2spw']
  comments120ssr2=  [comments120ssr2,'480kbps/22/ssr2/hi2spw','480kbps/30/ssr2/hi2spw','480kbps/36/ssr2/hi2spw']

  names115=[names115,'090408_1.bsf','090406_2.bsf','090407_1.bsf']
  names120ssr2=[names120ssr2,'090408_2.bsf','090313_1.bsf','090407_3.bsf']
  names120=[names120,'090408_3.bsf','090406_1.bsf','090407_2.bsf']
  sizes115=[sizes115,60.5,42,35.8]
  sizes120ssr2=[sizes120ssr2,47.1,42.0,35.5];19.3]
  ;sizesssr2=[42.5,47.6,51.4];19.3]
  sizes120= [sizes120,47.1,42.2,36.0];19.3]
  comments115=	    [comments115,'480kbps/22/115', '480kbps/30/115', '480kbps/36/115']
  comments120=	    [comments120,'480kbps/22/120', '480kbps/30/120', '480kbps/36/120']
  comments120ssr2=  [comments120ssr2,'480kbps/22/ssr2','480kbps/30/ssr2','480kbps/36/ssr2']
  
; original 120ssr2 360kbps/blocks
  names120ssr2=[names120ssr2,'090804_4.bsf']
  sizes120ssr2=[sizes120ssr2,45.759]
  comments120ssr2=  [comments120ssr2,'360kbps/22ssr2/120']

;  360kbps/36 hour blocks replace with 240kbps/30 hour blocks; note first 115ssr2 block 
  names115= 	[names115,'090804_9.bsf']
  names120ssr2= [names120ssr2,'090804_C.bsf']
  names120= 	[names120,'090804_B.bsf']
  sizes115= 	[sizes115,33.4]
  sizes120ssr2= [sizes120ssr2,33.2]
  sizes120= 	[sizes120,33.7]
  comments115=	    [comments115,'360kbps/36/115']
  comments120=	    [comments120,'360kbps/36/120']
  comments120ssr2=  [comments120ssr2,'360kbps/36ssr2/120']
  names115ssr2=     ['090804_A.bsf']
  sizes115ssr2=     [33.4]
  comments115ssr2=  ['360kbps/36ssr2/115']

;  240kbps/31 hour blocks replace with 240kbps/30 hour blocks note first block to 115ssr2 blocks

  names115=[names115,'091216_1.bsf']
  names115ssr2=[names115ssr2,'091216_3.bsf']
  names120=[names120,'091215_1.bsf']
  names120ssr2=[names120ssr2,'091216_2.bsf']
  sizes115=[sizes115,25.927]
  sizes115ssr2=[sizes115ssr2,25.927]
  sizes120= [sizes120,26.258]
  sizes120ssr2=[sizes120ssr2,26.258]
  comments115=	    [comments115, '240kbps/31/115']
  comments115ssr2=  [comments115ssr2, '240kbps/31ssr2/115']
  comments120=	    [comments120, '240kbps/31/120']
  comments120ssr2=  [comments120ssr2,'240kbps/31ssr2/120']

;   360kbps/60hr blocks.',/info
  names115= 	[names115,'090911_2.bsf']    
  names120= 	[names120,'090911_3.bsf'] 
  sizes115= 	[sizes115,20.4]  ;update
  sizes120= 	[sizes120,20.7]
  comments115=	[comments115,'360kbps/60/115']
  comments120=	[comments120,'360kbps/60/120']


endif else $ 

IF keyword_set(CALROLL) THEN BEGIN
    message,'Using CALROLL blocks.',/info
;    names120ssr2='071207_1.bsf'
;    sizes120ssr2=211
;    comments120ssr2='stepped_cal_rolls/3hr/'
;    names120ssr2='100510_1.bsf'
   if keyword_set(step_icer0) then begin
     names120ssr2='111102_1.bsf'
     sizes120ssr2=156.375
     comments120ssr2='stepped_cal_rolls/3hr/eui_icer0/'
   endif else begin
;     names120ssr2='100524_1.bsf'
;     sizes120ssr2=78.191
;     comments120ssr2='stepped_cal_rolls/3hr/reduced_cor/'
     names120ssr2='151222_0.bsf'
     sizes120ssr2=80.743
     comments120ssr2='stepped_cal_rolls/5hr/'
   endelse

ENDIF ELSE $

IF keyword_set(GTCAL) THEN BEGIN
    message,'Using GT CAL blocks.',/info
    names120ssr2='100416_1.bsf'
    sizes120ssr2=63.915
    comments120ssr2='GT_flat_field/171_195/'
    names115='080616_1.bsf'
ENDIF ELSE $

IF keyword_set(HIOFFSET) THEN BEGIN
    message,'Using HIOFFSET CAL blocks.',/info
;    names120ssr2='100419_1.bsf'
;    sizes120ssr2=88.127
    names120ssr2='171222_7.bsf'
    sizes120ssr2=117.948
    comments120ssr2='offpoint/HI_straylight/'


ENDIF ELSE $

IF keyword_set(HILED) THEN BEGIN
    message,'Using HI LED CAL blocks.',/info
    names120ssr2='100630_1.bsf'
    sizes120ssr2=68.170
    comments120ssr2='240kbps/22ssr2/120/hi_led/'
ENDIF ELSE $

IF keyword_set(HISKY) THEN BEGIN
    message,'Using HI Linear Sequence CAL blocks.',/info
    names120ssr2='100504_1.bsf'
    sizes120ssr2=52.442
    comments120ssr2='240kbps/22ssr2/115/hi_linseq/'
ENDIF ELSE $

IF keyword_set(MDUMP) THEN BEGIN
    message,'Using Momentum Dump blocks.',/info
    names120ssr2=['090519_3.bsf','090519_2.bsf','090526_1.bsf','100421_1.bsf','140328_2.bsf','140328_4.bsf','140328_3.bsf']
    sizes120ssr2=[213.7,190.7,203.7,24.6,14.148,24.361,36.831]
    comments120ssr2=['mdumps/nospin   3hr mdump calibration block', $
    	    	     'mdumps/withspin 3hr mdump calibration block With mech spin tests', $
		     'mdumps/spin_and_cor2 4hr mdump calibration block With mech spin tests and COR2 test images', $
		     'mdumps/cor cor only mdump calibration block',$
    	    	     'mdumps/cor2cal cor2cal only mdump calibration block',$
    	    	     'mdumps/cor2cal cor1 & cor2cal mdump calibration block',$
    	    	     'mdumps/cor2cal cor(1&2) & cor2cal mdump calibration block']

ENDIF ELSE $
    
if keyword_set(campaign) then begin
  print,'************defaults to Jan 2008 171A campaign block schedules**************'
  ;stop
    maxpasslen=[17,50]
    names115=['080107_3.bsf']
    names120=['080107_4.bsf','080107_7.bsf']
    names120ssr2=['']
    sizes115=[108]
    sizes120=[95.6,81.8]
    sizes120ssr2= [0]
    comments115=['171A campaign 115 min']
    comments120=['171A campaign 120 min','171A campaign 120 min 150sCad']
    comments120ssr2=['']
endif ELSE $

IF keyword_set(U160KBPS) THEN BEGIN ;current default blocks

    names115ssr2=   ['090819_2.bsf','110723_1.bsf','110624_1.bsf','110624_2.bsf','110624_3.bsf','110624_3.bsf']
    names120ssr2=   ['090804_4.bsf','090804_8.bsf','110622_1.bsf','110622_2.bsf','110622_3.bsf','120501_1.bsf']
    
    sizes115ssr2=   [58.856,43.8, 31.3, 23.2, 19.8, 19.8]
    sizes120ssr2=   [44.507,37.1, 32.6, 23.8, 20.3, 16.72]
    
    comments115ssr2=   ['120kbps/10ssr2/115','120kbps/12ssr2/115','120kbps/14ssr2/115','120kbps/19ssr2/115','120kbps/23ssr2/115','120kbps/23ssr2/115']
    comments120ssr2=   ['120kbps/10ssr2/120','120kbps/12ssr2/120','120kbps/14ssr2/120','120kbps/19ssr2/120','120kbps/23ssr2/120','120kbps/27ssr2/120']
    sizesssr2_1=    [43.8,40.5,40.5,44.0,44.0,44.0]	; 115 block
    sizesssr2=	    [41.1,43.4,46.0,46.0,46.0,45.95]	; 120 block
     if keyword_set(u120kbps30hr) then begin
      names115ssr2=   [names115ssr2,'110624_3.bsf']
      names120ssr2=   [names120ssr2,'130115_1.bsf']
      sizes115ssr2=   [sizes115ssr2,19.80]
      sizes120ssr2=   [sizes120ssr2,10.47]
      comments115ssr2=[comments115ssr2,'120kbps/23ssr2/115']
      comments120ssr2=[comments120ssr2,'120kbps/30ssr2/120']
      sizesssr2_1=    [sizesssr2_1,44.0]	; 115 block
      sizesssr2=      [sizesssr2,46.0]	; 120 block
    endif 
    names115	=['none']
    names120	=names115
    comments115 =names115
    comments120 =names115
    sizes115	=[99]
    sizes120	=sizes115


endif ELSE $

IF keyword_set(U240KBPS) THEN BEGIN
 
;  sizesssr2=[45.5,47.6,51.4]
;  names115ssr2=['','','']
;  comments115ssr2=['','','']
;  sizes115ssr2=[0,0,0]
;  sizesssr2_1=[0,0,0]

; ***NOTE*** 
; Move old block definitions above to /HISTORICAL
; **********
 
; 240kbps/30 now default for the 36 hour schedule
  names115= 	['090819_1.bsf','090804_5.bsf','100430_3.bsf'];,'091216_1.bsf']
  names115ssr2= ['090819_2.bsf','090804_6.bsf','100430_2.bsf'];,'091216_3.bsf']
  names120= 	['090804_3.bsf','090804_7.bsf','100430_4.bsf'];,'091215_1.bsf']
  names120ssr2= ['091218_1.bsf','090804_8.bsf','100430_1.bsf'];,'091216_2.bsf']

  sizes115= 	[58.4,36.9,27.435];,25.927]
  sizes115ssr2= [58.4,36.9,27.435];,25.927]
  sizes120= 	[44.2,37.2,27.648];,26.258]
  sizes120ssr2= [45.759,37.1,27.648];,26.258]

; change path to 240kbps links
;  comments115=	    ['360kbps/22/115', '360kbps/30/115', '240kbps/30/115'];, '240kbps/31/115']
;  comments115ssr2=  ['360kbps/22ssr2/115', '360kbps/30ssr2/115', '240kbps/30ssr2/115'];, '240kbps/31ssr2/115']
;  comments120=	    ['360kbps/22/120', '360kbps/30/120', '240kbps/30/120'];, '240kbps/31/120']
;  comments120ssr2=  ['360kbps/22ssr2/120','360kbps/30ssr2/120','240kbps/30ssr2/120'];,'240kbps/31ssr2/120']
  comments115=	    ['240kbps/18/115', '240kbps/22/115', '240kbps/30/115'];, '240kbps/31/115']
  comments115ssr2=  ['240kbps/18ssr2/115', '240kbps/22ssr2/115', '240kbps/30ssr2/115'];, '240kbps/31ssr2/115']
  comments120=	    ['240kbps/18/120', '240kbps/22/120', '240kbps/30/120'];, '240kbps/31/120']
  comments120ssr2=  ['240kbps/18ssr2/120','240kbps/22ssr2/120','240kbps/30ssr2/120'];,'240kbps/31ssr2/120']

  sizesssr2=	[43.4,43.4,50.722];,47.202]
  sizesssr2_1=	[40.6,41.3,48.372];,44.853]
   
ENDIF ELSE $

if keyword_set(active) then begin
    message,'Using ACTIVE blocks.',/info
;  print,'not updated for active blocks please return and correct pro'
;  return
  maxpasslen=[25,29,31,39,50]
  names115=['071120_1.bsf','071016_1.bsf','071121_2.bsf','071121_4.bsf','071121_6.bsf']
  names120=['071120_2.bsf','071016_2.bsf','071121_1.bsf','071121_3.bsf','071121_5.bsf']
  names120ssr2=['']
  sizes115=[75.7,66.1,47.2,44.6,38.8]
  sizes120=[63.6,52.1,46.9,44.3,38.6]
  sizes120ssr2= [0]
  comments115=['22 hr 115 min active','26 hr 115 min active','30 hr 115 min active','31 hr 115 min active','36 hr 115 min active']
  comments120=['22 hr active/22/120','26 hr cor2sum_new/26/120min','30 hr active/30/120','31 hr active/31/120','36 hr active/36/120']
  comments120ssr2=['']

ENDIF ELSE $

if keyword_set(euvi304spw) then begin
    message,'Using euvi304spw blocks.',/info
    names115ssr2=   ['170428_4.bsf','170428_6.bsf','170428_8.bsf','170428_a.bsf','170428_c.bsf']
    names120ssr2=   ['170428_5.bsf','170428_7.bsf','170428_9.bsf','170428_b.bsf','170428_d.bsf']
    sizes115ssr2=   [58.036,43.734, 31.28, 23.223, 19.754]
    sizes120ssr2=   [43.82,36.92, 31.908, 23.538, 20.068]
    comments115ssr2=   ['euvi304/10hour/115','euvi304/12hour/115','euvi304/14hour/115','euvi304/19hour/115','euvi304/23hour/115']
    comments120ssr2=   ['euvi304/10hour/120','euvi304/12hour/120','euvi304/14hour/120','euvi304/19hour/120','euvi304/23hour/120']
    sizesssr2_1=    [39.781,40.514,40.514,43.965,43.965]	; 115 block
    sizesssr2=	    [42.503,42.503,42.503,45.954,45.954]	; 120 block
    names115	=['none']
    names120	=names115
    comments115 =names115
    comments120 =names115
    sizes115	=[99]
    sizes120	=sizes115
    
ENDIF ELSE $
if keyword_set(hi2_2277) then begin
    message,'Using hi2_2277 blocks.',/info
    names115ssr2=   ['170428_y.bsf','170428_q.bsf','170428_s.bsf','170428_u.bsf','170428_w.bsf']
    names120ssr2=   ['170428_p.bsf','170428_r.bsf','170428_t.bsf','170428_v.bsf','170428_x.bsf']
    sizes115ssr2=   [60.270,45.968,33.515,25.459,21.989]
    sizes120ssr2=   [46.055,39.155,34.144,25.773,22.304]
    comments115ssr2=   ['hi22277/10hour/115','hi22277/12hour/115','hi22277/14hour/115','hi22277/19hour/115','hi22277/23hour/115']
    comments120ssr2=   ['hi22277/10hour/120','hi22277/12hour/120','hi22277/14hour/120','hi22277/19hour/120','hi22277/23hour/120']
    sizesssr2_1=    [39.781,40.514,40.514,43.965,43.965]	; 115 block
    sizesssr2=	    [42.503,42.503,42.503,45.954,45.954]	; 120 block
    names115	=['none']
    names120	=names115
    comments115 =names115
    comments120 =names115
    sizes115	=[99]
    sizes120	=sizes115

ENDIF ELSE $
if keyword_set(hi22277n304spw) then begin
    message,'Using hi2_2277 and 304spw blocks.',/info
    names115ssr2=   ['170428_e.bsf','170428_g.bsf','170428_i.bsf','170428_k.bsf','170428_m.bsf']
    names120ssr2=   ['170428_f.bsf','170428_h.bsf','170428_j.bsf','170428_l.bsf','170428_n.bsf']
    sizes115ssr2=   [60.270,45.968,33.515,25.459,21.989]
    sizes120ssr2=   [46.055,39.155,34.144,25.773,22.304]
    comments115ssr2=   ['hi2e304/10hour/115','hi2e304/12hour/115','hi2e304/14hour/115','hi2e304/19hour/115','hi2e304/23hour/115']
    comments120ssr2=   ['hi2e304/10hour/120','hi2e304/12hour/120','hi2e304/14hour/120','hi2e304/19hour/120','hi2e304/23hour/120']
    sizesssr2_1=    [39.781,40.514,40.514,43.965,43.965]	; 115 block
    sizesssr2=	    [42.503,42.503,42.503,45.954,45.954]	; 120 block
    names115	=['none']
    names120	=names115
    comments115 =names115
    comments120 =names115
    sizes115	=[99]
    sizes120	=sizes115

ENDIF ELSE BEGIN
    message,'Using Default blocks.',/info
    message,'Using Default euvi304spw blocks.',/info
;    names115ssr2=   ['170428_4.bsf','170428_6.bsf','170428_8.bsf','170428_a.bsf','170428_c.bsf']
    names115ssr2=   ['180103_1.bsf','170428_6.bsf','170428_8.bsf','170428_a.bsf','170428_c.bsf']
    names120ssr2=   ['170428_5.bsf','170428_7.bsf','170428_9.bsf','170428_b.bsf','170428_d.bsf']
    sizes115ssr2=   [58.036,43.734, 31.28, 23.223, 19.754]
    sizes120ssr2=   [43.82,36.92, 31.908, 23.538, 20.068]
    comments115ssr2=   ['euvi304/10hour/115','euvi304/12hour/115','euvi304/14hour/115','euvi304/19hour/115','euvi304/23hour/115']
    comments120ssr2=   ['euvi304/10hour/120','euvi304/12hour/120','euvi304/14hour/120','euvi304/19hour/120','euvi304/23hour/120']
    sizesssr2_1=    [39.781,40.514,40.514,43.965,43.965]	; 115 block
    sizesssr2=	    [42.503,42.503,42.503,45.954,45.954]	; 120 block
    names115	=['none']
    names120	=names115
    comments115 =names115
    comments120 =names115
    sizes115	=[99]
    sizes120	=sizes115

;    message,'Using Default blocks.',/info
;    names115ssr2=   ['090819_2.bsf','110723_1.bsf','110624_1.bsf','110624_2.bsf','110624_3.bsf','110624_3.bsf']
;    names120ssr2=   ['090804_4.bsf','090804_8.bsf','110622_1.bsf','110622_2.bsf','110622_3.bsf','120501_1.bsf']
    
;    sizes115ssr2=   [58.856,43.8, 31.3, 23.2, 19.8, 19.8]
;    sizes120ssr2=   [44.507,37.1, 32.6, 23.8, 20.3, 16.72]
    
;    comments115ssr2=   ['120kbps/10ssr2/115','120kbps/12ssr2/115','120kbps/14ssr2/115','120kbps/19ssr2/115','120kbps/23ssr2/115','120kbps/23ssr2/115']
;    comments120ssr2=   ['120kbps/10ssr2/120','120kbps/12ssr2/120','120kbps/14ssr2/120','120kbps/19ssr2/120','120kbps/23ssr2/120','120kbps/27ssr2/120']
;    sizesssr2_1=    [43.8,40.5,40.5,44.0,44.0,44.0]	; 115 block
;    sizesssr2=	    [41.1,43.4,46.0,46.0,46.0,45.95]	; 120 block
     if keyword_set(u120kbps30hr) then begin
      names115ssr2=   [names115ssr2,'110624_3.bsf']
      names120ssr2=   [names120ssr2,'130115_1.bsf']
      sizes115ssr2=   [sizes115ssr2,19.80]
      sizes120ssr2=   [sizes120ssr2,10.47]
      comments115ssr2=[comments115ssr2,'120kbps/23ssr2/115']
      comments120ssr2=[comments120ssr2,'120kbps/30ssr2/120']
      sizesssr2_1=    [sizesssr2_1,44.0]	; 115 block
      sizesssr2=      [sizesssr2,46.0]	; 120 block
    endif 
 ;   names115	=['none']
 ;   names120	=names115
 ;   comments115 =names115
 ;   comments120 =names115
 ;  sizes115	=[99]
 ;  sizes120	=sizes115


endelse


if keyword_set(u51hr) then begin
    message,'Including 240kbps/51hr blocks.',/info
  nblk=n_Elements(names120)-1
  names115= 	[names115,names115ssr2(nblk)+'t']    
  names115ssr2= [names115ssr2,names115ssr2(nblk)+'t'] ;update
  names120ssr2= [names120ssr2,'100517_1.bsf'] ;update
  names120= 	[names120,names120ssr2(nblk)+'t'] 
  sizes115= 	[sizes115,sizes115(nblk)]  ;update
  sizes115ssr2= [sizes115ssr2,sizes115ssr2(nblk)] ;update
  sizes120ssr2= [sizes120ssr2,16.159] ;update
  sizes120= 	[sizes120,sizes120(nblk)]
  comments115=	[comments115,comments115(nblk)]
  comments115ssr2=[comments115ssr2,comments115ssr2(nblk)]
  comments120ssr2=[comments120ssr2,'240kbps/51ssr2/120']
  comments120=	[comments120,comments120(nblk)]
  sizesssr2=[sizesssr2,50.722]
  sizesssr2_1=[sizesssr2_1,sizesssr2_1(nblk)]
endif

if keyword_set(jop225) then begin
  names115= 	['100107_2.bsf']    
  names115ssr2= ['100106_2.bsf']
  names120ssr2= ['100106_1.bsf']
  names120= 	['100107_1.bsf'] 
  sizes115= 	[36.935]  
  sizes115ssr2= [36.935] 
  sizes120ssr2= [37.470] 
  sizes120= 	[37.470]
  comments115=	['360kbps/30/115/JOP225/']
  comments115ssr2=['360kbps/30ssr2/115/JOP225/']
  comments120ssr2=['360kbps/30ssr2/120/JOP225/']
  comments120=	['360kbps/30/120/JOP225/']
  sizesssr2=[43.990]
  sizesssr2_1=[41.961]
endif


; list of block names120 which need to start 1 minute after the hour
names1minoffset=['071130_6.bsf','071130_9.bsf','071201_3.bsf','080429_5.bsf','080502_3.bsf','090129_3.bsf','090202_1.bsf','090202_2.bsf','090306_2.bsf','090407_5.bsf',$
    	         '090406_1.bsf','090407_2.bsf','090702_5.bsf','090702_8.bsf','130115_1.bsf']
; list of block names120 which need to start 30 seconds after the hour
names30secoffset=['090728_5.bsf','090728_6.bsf','090804_B.bsf','090911_1.bsf','090911_3.bsf','091215_1.bsf','100430_4.bsf']


if keyword_set(jopoffset) then begin
  names1minoffset=[names1minoffset,strmid(jopsch,strpos(jopsch,'/',/reverse_search)+1,strlen(jopsch)-1)]
  print,names1minoffset
endif

;blockstart=[0,0,0,0,0,0,1,1,1,1,1,1]  ; 0 = 120 min block 1 = 115 min block

ssr1 = 706.0      ;Megabytes
IF keyword_set(PERCENT) THEN blocksize=100.0*blocksize/ssr1     ; Percent of SSR1
END

