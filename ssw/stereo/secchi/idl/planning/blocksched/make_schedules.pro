; $Id: make_schedules.pro,v 1.184 2018/01/10 15:00:09 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : make_schedules
;               
; Purpose     : to create optimized block schedules from weekly schedules
;               
; Explanation : 
;               
; Use         : IDL> make_schedules,sc,yr,doy,doy2,active=active,campaign=campaign,dsn=dsn,nossr2=nossr2,rmssr2days=rmssr2days,jopsch=jopsch,ssr2hrs=ssr2hrs,$
;    	                              jopdays=jopdays,jophours=jophours,jopoffset=jopoffset, barrow=barrow,NOCMDFILE=nocmdfile,testdl=testdl,testphr=testphr, $
;	                              resets=resets,dailyav=dailyav,u51hr=u51hr, nohtml=nohtml,no36=no36,noskipssr2=noskipssr2
;    
; Inputs      : sc = A or B
;   	    	yr = yyyy
;   	    	doy = monday works best for now weekly schedules start on mondays
;               doy2 = optional last day to schedule (default is doy +6 1 week)
;               
; Outputs     : schedules for tuesday thru the end of the available DSN contacts.
;   	    	command load file
;
; Keywords :	/NOCMDFILE  Do not write command load file in ~/attic/cmdlogs
;   	    	outdir = change output directory from default (default= '~/loads/PT/flight/operations/(AorB)/dailyats')
;   	    	active = set to use active block schedules (defaults to PT/flight/operations/BSF/quiet/event_detect/*/sync36 schedules 080429)
;   	    	campaign = set for Jan 08 SECCHI compain block schedules 
;               dsn = set to use DSN schedules instead of weekly schedules 
;   	    	nossr2 = run quiet schedule with no SSR2 blocks all days
;    	    	JOPSCH = JOP schedule set to string of JOP block schedule with path from $sched/BSF: 
;    	    	    	ie 'quiet/no_event_detect/22/120/ssr2_304_Jones_test1/080909_1.bsf'
;   	    	ssr2hrs = set ssr2 hour blocks from default ([18,20])  can be to [8,10] or [8] or [8,14,20]
;               rmssr2days = array of doy's not to schedule SSR2 blocks (can use nossr2 when omitting the SSR2 blocks for all 7 days)
;               jopdays = array of doy's for the jop schedule to run set to ALL to include all days
;               jophours = even hour block to run the jop schedule ([18,20]) 
;               barrow  = sets the theshold for when to add more to previous schedule default is (-25)
;               NOCMDFILE = will not write itos command file
;               testdl = sets test mode downlink rate ie (360, 240, 160)
;               testphr = test mode hours of play back for downlinkrate "05:41" is estimated for 360 pb
;	        resets = test mode schedules based on a SSR1 pointer reset halfway though playbak
;	        dailyav = use weekly average to create schedules -changed 04/22/10 from- test mode averages the 7 days schedules
;	        u51hr = will  use of the 240kbps/51 hr schedule if needed.
;               nohtml = do not write the schedule html files
;   	    	/TESTSCH    Sets /nohtml and /nocmdfile. Do not repair checksums.
;               no36 = will not include the 36 hour schedules even if the ssr1 to go over 100%
;               rmtrack = will ask which track to delete from weekly schedule before creating schedules
;               max22 = sets max pass length for the largest schedule block (22 hour) used to controll used of the largest block
;               ssr1pbonly = gives ssr2 estimated downlink to ssr1 used to calculate mb per play back using ssr1 only DFD
;               force36 = will include 36 hour schedule in first scheduling run,
;	        MBST = number of MB left on SSR1 after track before start of period, in case report files are incorrect due to a missed pass.
;	        ssr2st=sets ssr2 start same as mbst but for the SSR2
;	        ssr2jop=control ssr2hours during a jop
;	        max22= set the max hours to schedule the 240kpbs/22 hour block set lower to run more 22 hour block and higher to run more (240kbps/18) blocks
;   	    	ssr1pbonly = adds SSR2 playback to SSR1 extra playback (SC SSR1 only DFD)
;   	    	MAXLEFT= maximum MB left on SSR1 after a track (default=0)
;    	    	PLOTRES=    Resolution of plotted SSR1 percent values, in minutes. (default is 15 min.)
;   	    	INTERVAL=   Maximum number of days to go without going to zero; default is 7
;               MDUMP=      Time of momentum dump ignition to use
;               XTRADAYS=   Number of extra days beyond days to be scheduled to compute SSR1 usage for (default is 6)
;   	    	/SKIPSSR2   Use no-ssr2 block for first complete block during a track.
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : planning 
;               
; Prev. Hist. : None.
;
; Written     : Lynn McNutt 07/26/2007 
;               
; Modified    :
;
; $Log: make_schedules.pro,v $
; Revision 1.184  2018/01/10 15:00:09  secchia
; seperated hioffset and himdump
;
; Revision 1.183  2017/04/25 15:16:44  secchia
; removed keywords u160kbps and u240kbps also set default blocks in scc_avail_blocks.pro
;
; Revision 1.182  2016/02/15 11:21:42  secchia
; commented out one day off fix corrected with new is.gz 2016-01-12
;
; Revision 1.181  2015/12/28 14:51:20  secchia
; corrected line 925
;
; Revision 1.180  2015/12/21 22:30:24  mcnutt
; added 5 hour step cal
;
; Revision 1.179  2015/10/19 14:22:16  hutting
; added m1day schedules
;
; Revision 1.178  2014/04/23 13:33:47  secchia
; change mdump spin time offset to 40 to make room for the COR2 door closed cal
;
; Revision 1.177  2014/03/27 17:59:47  secchib
; corrected cor2 mdump calibration blocks
;
; Revision 1.176  2014/03/27 13:07:50  mcnutt
; added cor2cal blocks to mdump
;
; Revision 1.175  2013/11/25 16:00:05  secchib
; correct 1 min offset for ISON rolls and added HI1a schedules after 11/30/2013
;
; Revision 1.174  2013/11/22 18:56:01  mcnutt
; added 1800 block for comet ISON EUVIB ver1
;
; Revision 1.173  2013/11/22 17:27:42  mcnutt
; added 1800 block for comet ISON EUVIB
;
; Revision 1.172  2013/11/13 18:19:02  secchia
; added ison comments
;
; Revision 1.171  2013/11/13 15:14:34  secchib
; corrected last change LH
;
; Revision 1.170  2013/11/13 12:46:06  mcnutt
; finished adding ISON schedules 11/28/2013
;
; Revision 1.169  2013/11/12 18:45:02  mcnutt
; finished adding ISON schedules
;
; Revision 1.168  2013/10/28 11:32:30  secchib
; added check to remove roll blocks lt block0
;
; Revision 1.167  2013/10/22 17:40:46  mcnutt
; added check to uload bsf files only once.
;
; Revision 1.166  2013/10/21 18:06:14  secchib
; correction to roll scheduling
;
; Revision 1.165  2013/10/21 16:38:20  mcnutt
; removed stop
;
; Revision 1.164  2013/10/21 16:36:08  mcnutt
; add roll/dwell schedule option
;
; Revision 1.163  2013/10/21 12:33:07  secchia
; adding any changes to ISON HI2
;
; Revision 1.162  2013/10/07 12:38:02  mcnutt
; add hi2 ISON keyword
;
; Revision 1.161  2013/08/09 20:40:07  secchib
; nr - use maxmbleft instead of 7.1 for pcheck2
;
; Revision 1.160  2013/08/09 17:04:53  secchib
; nr - implement pcheck3 to compute changeblk instead of wnxt0
;
; Revision 1.159  2013/02/08 16:30:12  secchib
; corrected block plot and count
;
; Revision 1.158  2013/01/07 18:15:12  secchib
; correct plot from last change and second hours over 100 calculation
;
; Revision 1.157  2013/01/07 18:03:32  mcnutt
; adjusted Hours over 100 nblocks * plotres not 5 minuntes
;
; Revision 1.156  2012/12/03 16:22:54  mcnutt
; added noneedtochange blocks to use heavier schedules if the SSR1 goes to zero
;
; Revision 1.155  2012/11/29 22:10:21  nathan
; update pbrates for ssr1 120kbps and ssr2 240 and 120 based on actuals
;
; Revision 1.154  2012/10/22 15:26:26  mcnutt
; change mdump spins test for clockwise only and cor1 only for B
;
; Revision 1.153  2012/07/10 13:03:30  secchib
; corrected block printed when MB not found
;
; Revision 1.152  2012/07/10 12:25:23  secchib
; corrected spin question values
;
; Revision 1.151  2012/06/14 13:12:59  secchia
; added cor1 ccw only spin test
;
; Revision 1.150  2012/05/04 13:09:57  secchib
; corrected dospinv in print statement for ccw
;
; Revision 1.149  2012/05/04 13:01:04  secchia
; added COR1 CCW and CW spins test selections
;
; Revision 1.148  2012/03/09 21:34:54  secchia
; nr - set maxpasslen=[50]; print correct track info when prompting for MB
; left at end of previous track; wait after first plot
;
; Revision 1.147  2012/02/03 13:09:56  secchib
; added ,X to end of command lines
;
; Revision 1.146  2011/10/21 17:12:11  secchib
; corrected previous schedule blocks for when end of last track was doy-2
;
; Revision 1.145  2011/10/19 17:42:55  mcnutt
; added _extra to stepcal call to scc_avail_blocks
;
; Revision 1.144  2011/10/05 15:17:54  mcnutt
; changed report names to use the 1st schedule day
;
; Revision 1.143  2011/09/12 11:59:21  secchia
; corrected blocktochange rror if not bloakcs need to change
;
; Revision 1.142  2011/07/22 21:14:09  secchia
; fix doy on command csv file
;
; Revision 1.141  2011/07/20 19:35:38  secchib
; nr - use $sched for input files even when outdir is set
;
; Revision 1.140  2011/07/20 17:00:40  secchia
; prompt for size if block not found; omit extra days from summary on plot
;
; Revision 1.139  2011/07/15 20:13:07  secchib
; nr - add jopssr2size; fix xmargin on plot; fix jopsameasschedule
;
; Revision 1.138  2011/07/15 19:17:47  secchib
; nr - move scc_avail_blocks call; use $sched to find summary file
;
; Revision 1.137  2011/06/30 21:41:59  nathan
; make /noskipssr2 the default, add /SKIPSSR2 option
;
; Revision 1.136  2011/06/28 23:19:16  secchia
; nr - implemented MDUMP=
;
; Revision 1.135  2011/06/28 22:35:43  secchia
; nr - added XTRADAYS=
;
; Revision 1.134  2011/06/28 17:15:11  secchib
; fix jopcomment, utplot margins, spawn cmd
;
; Revision 1.133  2011/06/27 20:11:03  nathan
; some changes to JOPSCH= and /TESTSCH
;
; Revision 1.132  2011/06/24 22:17:39  nathan
; Pretty major change to how looping is done; basically, the interval in each
; iteration is from the last ssr1=0 to the next ssr1=0 or the end of the input
; times. And the changes are done left to right instead of right to left. Also
; I changed how which block to use is determined.
; Added PLOTRES= and INTERVAL=.
;
; Revision 1.131  2011/06/20 15:27:13  nathan
; put pbrates in arrays
;
; Revision 1.130  2011/06/17 19:11:22  secchib
; Added MAXLEFT=; always get 14 days from weekly_sched; more diagnostic
; messages and plot each iteration; stop before writing outputs
;
; Revision 1.129  2011/05/27 17:47:00  secchia
; nr - move where mbr_start is set, and search for correct .rpt file to get it from
;
; Revision 1.128  2011/05/26 19:14:00  secchib
; nr - clarified some instructions
;
; Revision 1.127  2011/03/21 14:32:31  secchib
; prints SSR2 playback amount even if ssr1only keyword is set
;
; Revision 1.126  2011/02/17 12:54:46  secchib
; increase mb to 750 when over 100% to force use36 to work
;
; Revision 1.125  2010/11/20 13:16:06  secchia
; corrected hi sky calibration scheduling
;
; Revision 1.124  2010/11/08 15:10:29  secchib
; added check for missing playbacks
;
; Revision 1.123  2010/10/18 12:07:18  secchib
; correct hioffset selection with mdump uses mb(0)
;
; Revision 1.122  2010/10/15 12:23:04  mcnutt
; correct hioffset start time adding vairiable hioffsetstart, added keyword definitions
;
; Revision 1.121  2010/10/13 16:41:18  secchia
; nr - adjust selection of 2nd weekly sched to read in
;
; Revision 1.120  2010/10/05 14:31:59  secchia
; small changes to hicaltimes
;
; Revision 1.119  2010/10/05 13:36:20  mcnutt
; added time selection to hi calibration blocks
;
; Revision 1.118  2010/10/04 17:44:20  mcnutt
; added hi cal blocks to mdump
;
; Revision 1.117  2010/09/27 16:35:40  mcnutt
; writes upload commands for calibration blocks when used
;
; Revision 1.116  2010/07/12 14:56:58  secchib
; allows jophours =[0]
;
; Revision 1.115  2010/06/11 18:51:27  secchib
; added keyword ssr1pbonly
;
; Revision 1.114  2010/06/11 18:39:57  secchia
; corrected jop comments
;
; Revision 1.113  2010/06/07 12:43:34  secchib
; plots correctly with bsf file used for JOP
;
; Revision 1.112  2010/06/01 17:42:12  secchia
; changed jop comments on weekly plot
;
; Revision 1.111  2010/06/01 13:03:34  secchia
; remove track which don't have SPB and EPB
;
; Revision 1.110  2010/05/24 12:15:01  secchib
; corrected ask statements for spins test and deletes tmp files in year dirs
;
; Revision 1.109  2010/05/18 12:01:06  secchia
; fixed when n2 lt -1 line 1015
;
; Revision 1.108  2010/05/17 18:56:26  secchib
; added 51 hour blocks changed 660hr keyword to u51hr
;
; Revision 1.107  2010/05/17 13:26:17  secchia
; corrected GT calibration notification
;
; Revision 1.106  2010/05/10 15:59:27  secchia
; corrected dir for last schedule
;
; Revision 1.105  2010/05/10 15:27:47  secchib
; added yearly directories for schedule files
;
; Revision 1.104  2010/05/05 18:07:44  secchib
; moved resets and added pcheck2 to use up to 100% onplaybacks before end of pass =0
;
; Revision 1.103  2010/05/04 18:11:38  secchib
; reads in previsious days schedule
;
; Revision 1.102  2010/04/22 14:53:00  secchia
; change dailyav keyword usage
;
; Revision 1.101  2010/04/22 14:31:06  secchib
; correction for hioffset
;
; Revision 1.100  2010/04/22 13:50:31  mcnutt
; added hi offset calibration
;
; Revision 1.99  2010/04/21 20:01:13  mcnutt
; added cor mdump and spin test
;
; Revision 1.98  2010/04/19 20:53:13  secchib
; add gtcal
;
; Revision 1.97  2010/01/07 18:03:40  secchia
; added extra keyword
;
; Revision 1.96  2009/12/28 14:15:02  secchia
; handles years as per manual 2010=0 2015=A 2040=Z
;
; Revision 1.95  2009/12/22 13:12:01  secchia
; corrected end of year adjustments
;
; Revision 1.94  2009/12/15 13:10:48  secchib
; added skip mdump calibration option
;
; Revision 1.93  2009/11/10 16:04:42  secchib
; added keyword ssr2jop to control ssr2hours during a jop
;
; Revision 1.92  2009/10/07 19:18:26  secchia
; change 360 kbps to 237
;
; Revision 1.91  2009/09/14 16:57:33  mcnutt
; ask for ssr2start if not set and added keywords mbst and ssr2st
;
; Revision 1.90  2009/09/14 13:21:41  secchib
; added nossr2 features
;
; Revision 1.89  2009/09/11 18:18:05  secchib
; added /force36 and other changes by Lynn yesterday
;
; Revision 1.88  2009/09/08 17:31:20  mcnutt
; corrected avvergae MB per day
;
; Revision 1.87  2009/09/08 17:08:13  mcnutt
; corrected scheduled mbytes in report file
;
; Revision 1.86  2009/09/03 12:51:24  secchia
; corrected schedule comment line
;
; Revision 1.85  2009/09/02 18:56:47  mcnutt
; added keyword comments
;
; Revision 1.84  2009/09/02 18:44:09  mcnutt
; defaults to event detect mode removed keyword eventdetc and added keyword noskipssr2
;
; Revision 1.83  2009/08/31 19:01:20  secchia
; added no36 keyword
;
; Revision 1.82  2009/08/31 17:36:52  mcnutt
; sets mb to 707 if ssr1 reaches 100% and plots ssr1 in t5min times
;
; Revision 1.81  2009/08/28 14:58:28  secchia
; corrected ssr2 count to work with reset keyword
;
; Revision 1.80  2009/08/25 16:55:01  secchia
; reads new report format correctly
;
; Revision 1.79  2009/08/24 17:13:42  secchib
; change logic for barrow
;
; Revision 1.78  2009/08/17 18:58:25  mcnutt
; only uses 36 hour block if ssr1 goes above 100
;
; Revision 1.77  2009/08/10 18:21:18  mcnutt
; added print statements to make sure 60 block schedule is uploaded
;
; Revision 1.76  2009/08/10 18:10:14  mcnutt
; corrected u60hr and added keyword rmtrack to remove selected track from weekly schedules
;
; Revision 1.75  2009/08/10 17:15:31  mcnutt
; correct report file read for mbstart
;
; Revision 1.74  2009/08/07 21:23:55  nathan
; added /TESTSCH; print pcheck info; combine sw struct
;
; Revision 1.73  2009/07/30 13:05:16  mcnutt
; added changes for 360kbps blocks and ssr2size
;
; Revision 1.72  2009/07/28 11:46:39  secchib
; defined mdcorxdoorblock if not mdump
;
; Revision 1.71  2009/07/20 14:53:04  secchia
; round cor door open times instead of fix to get 3 minutes
;
; Revision 1.70  2009/07/20 12:40:30  secchib
; corrected error in last commit
;
; Revision 1.69  2009/07/13 18:23:10  secchib
; will write cor door opening in the next days schedule if mdump is close to midnight gmt
;
; Revision 1.68  2009/07/06 18:06:05  nathan
; Moved mdump and calroll blocks to scc_avail_blocks and modified make_schedules
; accordingly; also updated comments to include actual (new) dir structure
;
; Revision 1.67  2009/07/01 18:54:11  mcnutt
; added estimated ssr2 MB per playback to report
;
; Revision 1.66  2009/07/01 17:18:42  mcnutt
;  added new keywords to comments and up the kbps for 480 to 334 from 319
;
; Revision 1.65  2009/06/24 18:40:30  nathan
; moved all block definitinos to scc_avail_blocks.pro
;
; Revision 1.64  2009/06/09 13:48:43  mcnutt
; corrected mdump MB per block
;
; Revision 1.63  2009/06/02 12:52:20  secchia
; correted stepcal times in schedule
;
; Revision 1.62  2009/06/02 11:57:06  secchib
; only asks for mdump block if mdump day in the days to be scheduled
;
; Revision 1.61  2009/05/28 12:00:58  mcnutt
; added pb rate to report file
;
; Revision 1.60  2009/05/28 11:59:44  secchia
; only reads next schedules if doye lt doy2+1
;
; Revision 1.59  2009/05/27 16:53:06  secchib
; added COR1 and 2 Door opens to schedule if mdump
;
; Revision 1.58  2009/05/26 13:39:20  mcnutt
; added mdump schedule blokc options
;
; Revision 1.57  2009/05/04 13:20:18  secchia
; looks for report in th ereport dir
;
; Revision 1.56  2009/05/04 12:54:58  mcnutt
; calls blcksched2html after schedules are created and changed report file names to yyyy_mm_dd
;
; Revision 1.55  2009/04/20 12:32:21  secchib
; changed maxpasslen for new schedule blocks
;
; Revision 1.54  2009/04/14 11:55:32  secchia
; added new schedules cor2 512 starting on 090419
;
; Revision 1.53  2009/04/01 11:31:53  secchib
; added keyword u60hr to use 120KB schedule
;
; Revision 1.52  2009/03/04 15:08:33  secchia
; changes Lynn made yesterday
;
; Revision 1.51  2009/02/23 15:25:06  secchia
; added keyword barrow to set negative ssr1 remain value on shorter passes
;
; Revision 1.50  2009/02/23 15:04:53  secchib
; added keyword jopoffset to set start time to 1 min after the hour
;
; Revision 1.49  2009/02/17 12:47:36  secchib
; reset stepcal if not in scheduling week
;
; Revision 1.48  2009/02/09 16:52:13  secchia
; corrected mbpb in calculating passlen
;
; Revision 1.47  2009/02/03 14:51:57  mcnutt
; corrected dates for schedule blocks psyms on plot
;
; Revision 1.46  2009/02/03 13:44:12  secchib
; added hi cadance 304 and 171 schedules to 1min block array
;
; Revision 1.45  2009/02/03 12:57:12  secchib
; correct special schedule dir
;
; Revision 1.44  2009/02/03 12:23:55  mcnutt
; changed psym for special schedules
;
; Revision 1.43  2009/02/02 17:52:39  mcnutt
; steps all blocks in previous pass up if remain is lt then -30
;
; Revision 1.42  2009/01/26 12:19:58  mcnutt
; added reset option and change plot to block schedules
;
; Revision 1.41  2009/01/16 19:43:54  mcnutt
; prints MB per daily block schedule
;
; Revision 1.40  2009/01/07 22:13:04  nathan
; added /NOCMDFILE
;
; Revision 1.39  2008/12/29 15:56:30  secchib
; corrected year cross over change
;
; Revision 1.38  2008/12/19 14:54:25  secchia
; corrected doys and doye for year boundary
;
; Revision 1.37  2008/10/16 14:03:49  secchib
; uses last weekly schedule if doy gt then last schedule
;
; Revision 1.36  2008/10/06 17:41:49  secchib
; changed nossr2days rmssr2days
;
; Revision 1.35  2008/10/02 16:36:13  mcnutt
; added comments about new keywords
;
; Revision 1.34  2008/10/01 17:47:02  mcnutt
; separtaed ssr2 blocks from jop keywords, added rmssr2days keyword
;
; Revision 1.33  2008/10/01 11:37:23  secchib
; defined stcshedules when step calbration not found
;
; Revision 1.32  2008/09/25 16:31:15  mcnutt
; make stepped calibration schedules if SECCHI stepped is found in weekly_schedules
;
; Revision 1.31  2008/09/17 18:39:11  mcnutt
; documented better and added JOP keywords
;
; Revision 1.30  2008/09/16 17:31:33  secchib
; change low tlm rate ssr2 blocks to 18,20
;
; Revision 1.29  2008/08/27 17:00:31  mcnutt
; optimized schedules for lower telemetry rate
;
; Revision 1.28  2008/08/25 15:39:39  secchia
; fixed date for 23:59:00
;080304_1.bsf
; Revision 1.27  2008/05/13 17:01:38  secchib
; added sync36 block files to the 1 minute offset list
;
; Revision 1.26  2008/05/08 13:00:26  secchia
; update quit block schedules for sync36
;
; Revision 1.25  2008/02/26 13:04:22  secchib
; changed loadfile time from 10sec to 300 sec
;
; Revision 1.24  2008/02/26 13:00:34  secchib
; dsn schedules now creates the same days as the weekly_schedules
;
; Revision 1.23  2008/02/26 12:46:11  mcnutt
; add dsn keyword to create schedules from dsn files if weekly_schedules are not yet available
;
; Revision 1.22  2008/01/07 17:28:01  mcnutt
; uses weekly_scheudle for start and end of playback
;
; Revision 1.21  2008/01/03 19:17:07  mcnutt
; added campaign keyword and blocks
;
; Revision 1.20  2007/12/18 14:49:40  secchia
; corrected to cross yr boundary
;
; Revision 1.19  2007/12/12 16:25:55  secchia
; updated for correct 36 hr quiet sun blocks schedules
;
; Revision 1.18  2007/12/11 14:58:44  mcnutt
; added active schedule blocks
;
; Revision 1.17  2007/12/03 17:55:35  mcnutt
; quiet schedules use event detect blocks
;
; Revision 1.16  2007/11/29 18:52:56  mcnutt
; corrected SSR1 percent for 36 hour blocks
;
; Revision 1.15  2007/11/28 17:03:28  secchia
; corrected schedule2 spelling
;
; Revision 1.14  2007/11/27 19:31:30  mcnutt
; added active sun and quiet sun ssr2 blocks
;
; Revision 1.13  2007/09/25 18:30:06  secchia
; creates command .csv file for weekly schedules and defaults to PT/fligh/operations/X/dailyats for outdir
;
; Revision 1.12  2007/09/11 14:01:56  secchia
; handles when only 2 weeks of dsn schedules are available
;
; Revision 1.11  2007/08/02 15:48:56  secchia
; corrected last change
;
; Revision 1.10  2007/08/02 15:41:24  mcnutt
; corrected sc dsn files dir
;
; Revision 1.9  2007/08/01 19:53:41  mcnutt
; make one weeks of schedules tue-mon
;
; Revision 1.8  2007/07/30 15:57:24  secchia
; added commants and deletes .tmp files
;
; Revision 1.7  2007/07/27 17:08:26  mcnutt
; added new block schedules and change output of graph to png
;
; Revision 1.6  2007/07/27 15:34:34  mcnutt
; makes to week of schedules start from the first tuesday after DOY and creates a SSR1 percentage PDF file
;
; Revision 1.5  2007/07/26 19:13:59  mcnutt
; removed stop
;
; Revision 1.4  2007/07/26 19:09:15  mcnutt
; added plot of SSR1 percent full
;
; Revision 1.3  2007/07/26 18:12:01  mcnutt
; corrected schedsize
;
; Revision 1.2  2007/07/26 17:58:31  mcnutt
; coorected print statement
;
; Revision 1.1  2007/07/26 17:55:19  mcnutt
; new scheduling program
;

pro make_schedules,sc0,yr,doy,doy2,outdir=outdir,active=active,campaign=campaign,nossr2=nossr2, $
    	 rmssr2days=rmssr2days,jopsch=jopsch,ssr2hrs=ssr2hrs,jopdays=jopdays,jophours=jophours, $
	 jopoffset=jopoffset, barrow=barrow,NOCMDFILE=nocmdfile,testdl=testdl,testphr=testphr, $
	 resets=resets,dailyav=dailyav,u51hr=u51hr, nohtml=nohtml, noskipssr2=noskipssr2, $
	 TESTSCH=testsch,rmtrack=rmtrack,no36=no36,force36=force36,mbst=mbst,ssr2st=ssr2st, ssr2jop=ssr2jop,$
	 max22=max22,ssr1pbonly=ssr1pbonly, MAXLEFT=maxleft, PLOTRES=plotres, INTERVAL=interval, $
         XTRADAYS=xtradays, MDUMP=mdump, SKIPSSR2=skipssr2,rmblk=rmblk, ISON=ISON,_EXTRA=_EXTRA

;device,retain=2
;device,decompose=0
;tek_color

IF keyword_set(OUTDIR) THEN cmdfiledir=outdir ELSE BEGIN
    cmdfiledir='~/attic/cmdlogs'
    outdir='~/loads/PT/flight/operations/'+strupcase(sc0)+'/dailyats'
ENDELSE

if datatype(doy2) eq 'UND' then doy2=doy+6

cmdfile=cmdfiledir+'/'+strupcase(sc0)+'_weekly_schedules_'+string(yr,'(i4.4)')+'_'+string(doy,'(i3.3)')+'.csv'
loadz=strpos(outdir,'~/loads/')
if(loadz eq 0)then updir=strmid(outdir,8,strlen(outdir)-8) else updir=outdir

if keyword_set(ISON) and sc0 eq 'a' then begin
  hi2isonh=['2013:283:00:00:00','2013:327:00:00:00']
  hi2isonh=(((indgen((7848/24-6792/24)*2)*.5+6792/24))*24)+3+((51*60)+21)/(60.*60.)
  hi1isonh=['2013:325:00:00:00','2013:332:00:00:00']
  hi1isonh=[(((indgen((332-325)*12)*.08333+325))*24)+0+((39*60)+01)/(60.*60.),(((indgen((341-334)*12)*.08333+334))*24)+0+((39*60)+01)/(60.*60.)]
  allisonh=['2013:332:00:00:00','2013:333:00:00:00']
  allisonh=round(((indgen((333-332)*12)*.08333+332))*24)
  cor2isonh=['2013:333:00:00:00','2013:334:00:00:00']
  cor2isonh=round(((indgen(7)*.08333+333))*24)
endif
if keyword_set(ISON) and sc0 eq 'b' then begin
  cor2isonh=['2013:330:06:00:00','2013:332:00:00:00','2013:333:00:00:00','2013:334:00:00:00']
  cor2isonh=[round(((indgen((332-330)*12)*.08333+330))*24),round(((indgen((334-333)*12)*.08333+333))*24)]
  cor2isonh=cor2isonh[3:n_elements(cor2isonh)-1]; remove the first 6 hours
  allisonh=['2013:332:00:00:00','2013:333:00:00:00']
  allisonh=round(((indgen((333-332)*12)*.08333+332))*24)
endif


;maxpasslen=[12,30,40]
maxpasslen=[25,29,35,50];,100]
;changed 090413 to use cor1_512 schedule blocks
maxpasslen=[32,50];,100]
maxpasslen=[50]
;  maxpasslen=[25,29,35,50];,100]
IF keyword_set(max22) then maxpasslen(0)=max22
IF keyword_set(CAMPAIGN) THEN maxpasslen=[17,50]
IF keyword_set(MAXLEFT) THEN maxmbleft=maxleft ELSE maxmbleft=0
IF keyword_set(INTERVAL) THEN chngintrvl=interval ELSE chngintrvl=7
IF keyword_set(TESTSCH) THEN BEGIN
    nocmdfile=1
    nohtml=1
ENDIF
IF keyword_set(XTRADAYS) THEN ndaysread=doy2-doy+1+xtradays ELSE ndaysread=doy2-doy+7
help,ndaysread

yr=string(yr,'(i4.4)')
if(strupcase(sc0) eq 'B') then sc = 'STB'
if(strupcase(sc0) eq 'A') then sc = 'STA'

  misspb1=''
  sw=read_weekly_sched(sc,yr,doy-1,missingpbs=misspbs, NDAYS=ndaysread)
  
  doye=[strmid(sw.EPB,5,3)]

  
sw=sw(where(sw.SPB ne '' and sw.EPB ne ''))
  ;--Store pass structure in one var so I can refer back to it. -NR
if keyword_set(rmtrack) then begin
  nx=indgen(n_elements(sw))  
  for i=0,n_elements(sw)-1 do print,nx(i),'   ',sw(i).bot
  read,'Enter track number to be remove:  ',rmtnum
  sw=sw(where(nx ne rmtnum))  
endif

  doys0=[strmid(sw.SPB,5,3)]
  doye=[strmid(sw.EPB,5,3)]
  SPB=[strmid(sw.SPB,9,2)+strmid(sw.SPB,12,2)]
  EPB=[strmid(sw.EPB,9,2)+strmid(sw.EPB,12,2)]
  pbrate=[sw.pbrate]
  stepcal=[sw.stepcal] 
  mdumps=[sw.mdump]      ; time of pre-burn configuration which is 5 min. before ignition
  
  gtcal=[sw.gtcal] 
  hioffset=[sw.hioffset] 
  degroll=[sw.roll] 
  yr1=[strmid(sw.SPB,0,4)]
  yr2=[strmid(sw.SPB,0,4)]

  if (where(stepcal ne ''))[0] gt -1 then stepcal=stepcal(where(stepcal ne '')) else stepcal=''
  IF keyword_set(MDUMP) THEN BEGIN
  ; convert time of ignition in anytim format to weekly schedule format -  5 min.
    mtai=anytim2tai(mdump)
    ntai=mtai-5*60.
    mutc=tai2utc(ntai)
    mdump=strmid(utc2str(mutc,/date),0,5)+trim(utc2doy(mutc))+':'+utc2str(mutc,/trunc,/time)
  ENDIF ELSE  if (where(mdumps ne ''))[0] gt -1 then mdump=mdumps(where(mdumps ne '')) else mdump=''
  if (where(gtcal ne ''))[0] gt -1 then gtcal=gtcal(where(gtcal ne '')) else gtcal=''
  if (where(hioffset ne ''))[0] gt -1 then hioffset=hioffset(where(hioffset ne '')) else hioffset=''
;  if (where(himdump ne ''))[0] gt -1 then himdump=himdump(where(himdump ne '')) else himdump=''
  if (where(degroll ne ''))[0] gt -1 then degroll=degroll(where(degroll ne '')) else degroll=''
  
 ; adjust hours for new year
  doysmax=max(doys0)
  ny=where(yr1 gt yr1(0)) & if ny(0) gt -1 then doys0(ny)=fix(doys0(ny))+doysmax
  ny=where(yr2 gt yr2(0)) & if ny(0) gt -1 then doye(ny)=fix(doye(ny))+doysmax
  if yr1(0) lt yr then doys0=fix(doys0)-doysmax
  if yr2(0) lt yr then doye=fix(doye)-doysmax
  z=where(doys0 ge doy-1) 
  if(doye(z(0)) ge doy) then begin
     z=[z(0)-1,z] 
     readprevsched=1 
  endif else readprevsched=0
  doys=fix(doys0(z)) & pbrate=pbrate(z) & doye=fix(doye(z)) & spb=spb(z) & epb=epb(z) & yr1=yr1(z)

  bhours=(fix(doys)*24)+fix(strmid(SPB,0,2))+fix(strmid(SPB,2,2))/60.
  ehours=(fix(doye)*24)+fix(strmid(EPB,0,2))+fix(strmid(EPB,2,2))/60.
 
if keyword_set(testphr) then begin
 nepb=strmid(SPB,0,2)*60+strmid(SPB,2,2)+(fix(strmid(testphr,0,2))*60+fix(strmid(testphr,3,2)))
 EPB=string(nepb/60,'(i2.2)')+string(nepb-(nepb/60)*60,'(i2.2)')
 zt=where(fix(EPB) gt 2400) 
 if zt(0) gt -1 then begin
    EPB(zt)=string(fix(epb(zt))-2400,'(i4.4)')
    for nzt=0,n_elements(zt)-1 do if doye(zt(nzt)) ne doys(zt(nzt))+1 then doye(zt(nzt))=doye(zt(nzt))+1
 endif
 ehours=(fix(doye)*24)+fix(strmid(EPB,0,2))+fix(strmid(EPB,2,2))/60.
endif

ssr1total=706.0
; pb rates
pbrates=    [720,   480,    360,    240,    160,   120]    ; kbps
ssr1rates=  [440,   334,    237,    163,    103,    62]
ssr2rates=  [ 40,    25.6,  18.58,  12.10,   11,    12]
nrates=n_elements(pbrates)

kbps=	    intarr(n_Elements(pbrate)) +ssr1rates[0]
ssr2kbps=   fltarr(n_Elements(pbrate)) +ssr2rates[0]
if keyword_set(testdl)then pbrate=intarr(n_Elements(pbrate))+testdl

FOR i=1,nrates-1 DO BEGIN
    pbcheck=where(pbrate eq pbrates[i],nc)
    IF nc GT 0 THEN BEGIN
    	kbps[pbcheck]=ssr1rates[i]
	ssr2kbps[pbcheck]=ssr2rates[i]
    ENDIF
ENDFOR

;if keyword_set(noskipssr2) then noskipssr2=1 else noskipssr2=0
IF keyword_set(SKIPSSR2) THEN noskipssr2=0 ELSE noskipssr2=1

ssr2kbpsest=ssr2kbps
if keyword_set(ssr1pbonly) then begin
  noskipssr2=1
  kbps=kbps+(ssr2kbps*0.75)
  ssr2kbps(*)=0.
  ssr2dl=0
endif else ssr2dl=1.

print
print,'ehours-bhours:',ehours-bhours
wait,3

;if (kbps le 319) then ssr2hours=[18,20]
mbpb=((kbps*(ehours(0:n_Elements(ehours)-1)-bhours(0:n_Elements(ehours)-1))*3600.)/8000.);/(ssr1p4(1:n_Elements(ehours)-1)))*100.
tmp=where(mbpb gt ssr1total) & if tmp(0) gt -1 then mbpb(tmp)=ssr1total

mbph=total(mbpb/(ehours-bhours))/n_elements(mbpb)
pbperday=total(ehours(where(doys ge doy and doys le doy2))-bhours(where(doys ge doy and doys le doy+6)))*mbph/(doy2-doy+1)
embpb=total(mbpb)/n_elements(mbpb)



doyblck=indgen(max(fix(doys))-min(fix(doys))+1) +min(fix(doys))
;doyblck=indgen(8) +min(fix(doys))
blckhours=[0,2,4,6,8,10,12,14,16,18,20,22]
blocks=intarr(n_Elements(doyblck)*n_elements(blckhours))

print,'writing schedules for DOYs: ',doyblck[1:*]

for n=0,n_elements(doyblck)-1 do blocks(n*n_elements(blckhours):((n+1)*n_elements(blckhours))-1)=(doyblck(n)*24)+blckhours

blocks=blocks(where(blocks gt ehours(0) and blocks lt max(ehours)))

d1=utc2tai(str2utc(yr+'-01-01T00:00:00'))-24*3600.
blkut =tai2utc(d1+(blocks*3600.))
blkhrs=utc2str(blkut)

SCC_AVAIL_BLOCKS,   schedule2,sch2size,comment2, $
    	    	    schedule1,sch1size,comment1, $
		    schedules,schsize,comments, $
		    m1sch, $
		    schedule1ssr2,sch1sizessr2,comment1ssr2, $
		    s30sch, sizessr2, sizessr2_1, $
		    U51HR=u51hr, ACTIVE=active, CAMPAIGN=campaign, _EXTRA=_extra
; schedule1ssr2,sch1sizesssr2,comment1ssr2, sizessr2_1 are for ssr2/115 blocks
; sizessr2 and sizessr2_1 are size of SSR2 for 120 and 115 blocks respectively

nschs=n_elements(schedules)
nsch1=n_elements(schedule1)
nsch2=n_elements(schedule2)
nsch12=n_elements(schedule1ssr2)
if keyword_set(rmblk) then begin
  if nschs ge rmblk then begin
    keepblocks=indgen(nschs)
    keepblocks=where(keepblocks ne rmblk)
    schedules=schedules(keepblocks)
    schsize=schsize(keepblocks)
    comments=comments(keepblocks)
  endif
  if nsch1 ge rmblk then begin
    keepblocks=indgen(nsch1)
    keepblocks=where(keepblocks ne rmblk)
    schedule1=schedule1(keepblocks)
    sch1size=sch1size(keepblocks)
    comment1=comment1(keepblocks)
  endif
  if nsch2 ge rmblk then begin
    keepblocks=indgen(nsch2)
    keepblocks=where(keepblocks ne rmblk)
    schedule2=schedule2(keepblocks)
    sch2size=sch2size(keepblocks)
    comment2=comment2(keepblocks)
  endif
  if nsch12 ge rmblk then begin
    keepblocks=indgen(nsch12)
    keepblocks=where(keepblocks ne rmblk)
    schedule1ssr2=schedule1ssr2(keepblocks)
    sch1sizessr2=sch1sizessr2(keepblocks)
    comment1ssr2=comment1ssr2(keepblocks)
  endif
  nschs=n_elements(schedules)
  nsch1=n_elements(schedule1)
  nsch2=n_elements(schedule2)
  nsch12=n_elements(schedule1ssr2)
endif

 ;SSR2 blocks
;ssr2hours=[18,20]
ssr2hours=indgen(12)*2 +0
IF schedule2[0] EQ '' THEN ssr2hours=[-1]
IF keyword_set(NOSSR2) THEN ssr2hours=[-1]
if Keyword_set(ssr2hrs) then ssr2hours=ssr2hrs
IF keyword_set(SSR2JOP) THEN ssr2days=jopdays ELSE ssr2days=doyblck
if keyword_set(rmssr2days)then for n=0,n_Elements(rmssr2days)-1 do ssr2days=ssr2days(where(ssr2days ne rmssr2days(n)))
if ssr2hours[0] LT 0 THEN ssr2blocks=[-1] $
ELSE BEGIN  
    ssr2blocks=intarr(n_Elements(ssr2days)*n_elements(ssr2hours))
    for n=0,n_elements(ssr2days)-1 do ssr2blocks(n*n_elements(ssr2hours):((n+1)*n_elements(ssr2hours))-1)=(ssr2days(n)*24)+ssr2hours
    IF noskipssr2 eq 0 and ~keyword_set(SSR2JOP) then begin
    ; remove ssr2 blocks from first block after start of each track
;    rmssr2blk=fix((bhours+1)/2.)*2
    	rmssr2blk=fix((bhours+1.5)/2.)*2
    ;    rmssr2blk=[rmssr2blk,rmssr2blk+2]
    	for nrm=0,n_elements(rmssr2blk)-1 do ssr2blocks=ssr2blocks(where(ssr2blocks ne rmssr2blk(nrm)))
    ENDIF
ENDELSE

jopschedule=''
if keyword_set(jopsch) then begin
    jopschedule=[strmid(jopsch,strpos(jopsch,'/',/reverse_search)+1,strlen(jopsch)-1)]
    summary=file_search(getenv('sched')+'/BSF/'+strmid(jopsch,0,strpos(jopsch,'/',/reverse_search)+1)+'*.summary')
;      summary=file_search(strmid(jopsch,0,strpos(jopsch,'/',/reverse_search)+1)+'*.summary')
     openr,8,summary[0]
     line='' & jopssr2size=0.
     jopssr2check=0
     while jopssr2check eq 0 do begin
        readf,8,line
;01234567890123456  from summary file
;SSR1        52.0          19.8          11.6        10.6          4.9         2.2         2.9         0.0
	if strpos(line,'SSR1') EQ 0 then jopsize=float(strmid(line,10,6)) 
    	IF strpos(line,'SSR2') EQ 0 THEN BEGIN
            jopssr2size=float(strmid(line,10,6))
            jopssr2check=1
        ENDIF
      endwhile
       close,8
       jopcomment=strmid(jopsch,0,strpos(jopsch,'/',/reverse_search))
       
       if ~keyword_set(jopdays) then jopdays=doyblkc
       if datatype(jophours) ne 'INT' then jophours=ssr2hours
       jopblocks=intarr(n_Elements(jopdays)*n_elements(jophours))
       for n=0,n_elements(jopdays)-1 do jopblocks(n*n_elements(jophours):((n+1)*n_elements(jophours))-1)=(jopdays(n)*24)+jophours
endif ELSE $
    jopcomment=comment2[0]

stcschedule=''
if stepcal ne '' then begin
  sthour=(fix(strmid(stepcal,5,3))*24)+fix(strmid(stepcal,9,2))+(fix(strmid(stepcal,12,2))+7)/60. & sthour=sthour(0)
  if sthour gt blocks(0) then begin
    scc_avail_blocks,stcschedule,stsize,stcomment,/CALROLL, _EXTRA=_extra
    if strpos(stcomment,'5hr') gt -1 then begin
      stepcalst=17
      sthourend=(fix(strmid(stepcal,5,3))*24)+fix(strmid(stepcal,9,2))+4+(fix(strmid(stepcal,12,2))+57)/60. 
    endif else begin
      stepcalst=7
      sthourend=(fix(strmid(stepcal,5,3))*24)+fix(strmid(stepcal,9,2))+2+(fix(strmid(stepcal,12,2))+57)/60. 
    endelse
    sthourend=sthourend(0)
    stblocks=blocks(where(blocks+2 gt sthour and blocks lt sthourend))
    stsize=stsize/n_elements(stblocks)
    print,'************************************************************'
    print,'************************************************************'
    print,'!!!!  Scheduling Stepped Calibration @ ',stepcal,' !!!!!!!'
    print,'************************************************************'
    print,'************************************************************'
  endif else stepcal=''
endif

gtcschedule=''
if gtcal ne '' then begin
  gthour=(fix(strmid(gtcal,5,3))*24)+fix(strmid(gtcal,9,2))+(fix(strmid(gtcal,12,2)))/60. & gthour=gthour(0)
  gthourend=(fix(strmid(gtcal,5,3))*24)+fix(strmid(gtcal,9,2))+1+(fix(strmid(gtcal,12,2))+31)/60. & gthourend=gthourend(0)
  if gthour gt blocks(0) then begin
    gtblocks=blocks(where(blocks+2 gt gthour and blocks lt gthourend))
    scc_avail_blocks,gtcschedule,gtsize,gtcomment,gtoffsets,/gtcal
    gtsize=gtsize/n_elements(gtblocks)
    print,'************************************************************'
    print,'************************************************************'
    print,'!!!!  Scheduling GT Calibration @ ',gtcal,' !!!!!!!'
    print,'************************************************************'
    print,'************************************************************'
  endif else gtcal=''
endif

hioffsetcschedule=''
if hioffset ne '' then begin
  offsethour=(fix(strmid(hioffset,5,3))*24)+fix(strmid(hioffset,9,2))+(fix(strmid(hioffset,12,2))-15)/60. & offsethour=offsethour(0)
  offsethourend=(fix(strmid(hioffset,5,3))*24)+fix(strmid(hioffset,9,2))+1+(fix(strmid(hioffset,12,2))+45)/60. & offsethourend=offsethourend(0)
  if offsethour gt blocks(0) then begin
    hioffsetblocks=blocks(where(blocks+2 gt offsethour and blocks lt offsethourend))
    scc_avail_blocks,hioffsetcschedule,hioffsetsize,hioffsetcomment,/hioffset
    hioffsetsize=hioffsetsize/n_elements(hioffsetblocks)
    print,'************************************************************'
    print,'************************************************************'
    print,'!!!!  Scheduling hioffset Calibration @ ',hioffset,' !!!!!!!'
    print,'************************************************************'
    print,'************************************************************'
  endif else hioffset=''
endif

;himdumpcschedule=''
;if himdump ne '' then begin
;  mdumphour=(fix(strmid(himdump,5,3))*24)+fix(strmid(himdump,9,2))+(fix(strmid(himdump,12,2))-15)/60. & mdumphour=mdumphour(0)
;  mdumphourend=(fix(strmid(himdump,5,3))*24)+fix(strmid(himdump,9,2))+1+(fix(strmid(himdump,12,2))+45)/60. & mdumphourend=mdumphourend(0)
;  if mdumphour gt blocks(0) then begin
;    himdumpblocks=blocks(where(blocks+2 gt mdumphour and blocks lt mdumphourend))
;    scc_avail_blocks,himdumpcschedule,himdumpsize,himdumpcomment,/himdump
;    himdumpsize=himdumpsize/n_elements(himdumpblocks)
;    print,'************************************************************'
;    print,'************************************************************'
;    print,'!!!!  Scheduling himdump Calibration @ ',himdump,' !!!!!!!'
 ;   print,'************************************************************'
 ;   print,'************************************************************'
 ; endif else himdump=''
;endif


rollschedule=''
if degroll[0] ne '' then begin
  offsethour=(fix(strmid(degroll,5,3))*24)+fix(strmid(degroll,9,2));+(fix(strmid(degroll,12,2)))/60.
  offsethourend=(fix(strmid(degroll,5,3))*24)+fix(strmid(degroll,9,2))+4;+(fix(strmid(degroll,12,2)))/60.
  z=where(offsethour ge blocks[0],cnt)
  if cnt gt 0 then begin
    offsethour=offsethour(z) & offsethourend=offsethourend(z) 
  endif
  if offsethour[n_elements(offsethour)-1] gt blocks(0) then begin
    degrollblocks=[0]
    for n=0,n_Elements(offsethour)-1 do degrollblocks=[degrollblocks,blocks(where(blocks+2 gt offsethour[n] and blocks lt offsethourend[n]))]
    degrollblocks=degrollblocks[1:n_elements(degrollblocks)-1]
    rollsched=''
    read,'Enter Roll bsf name including Directory path sched/BSF/????/YYMMDD_x.bsf:  ',rollsched
    rollschedule=rstrmid(rollsched,0,12)
    rollcomment=strmid(rollsched,0,strlen(rollsched)-12)
    sumlines=readlist(getenv('sched')+'/BSF/'+rollcomment+'/SEC20'+strmid(rollschedule,0,6)+'00'+strmid(rollschedule,7,1)+'.summary')
    rollsize=strmid(sumlines[where(strmid(sumlines,0,4) eq 'SSR1')],9,7)/2.
    rolloffset=''
    read,'Enter roll offset mm:ss from start of 2 hour block:  ',rolloffset
    print,'************************************************************'
    print,'************************************************************'
    print,'!!!!  Scheduling dwell @ ',degroll,' !!!!!!!'
    print,'************************************************************'
    print,'************************************************************'
  endif else degroll=''
endif



mdpschedule='' & mdpcor2doorblock=-1 & mdpcor1doorblock=-1
dohicalv=0
mdump=mdump[0]
if mdump ne '' then begin
  mdphour=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))-1+(fix(strmid(mdump,12,2)-20))/60. 
  ; subtract 1 hour and 20 minutes from RT Pre-Burn Configuration time, which is 5 min. before ignition
  mdphourend=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))+1+(fix(strmid(mdump,12,2))+30)/60. 
  ; add 1 hour and 30 minutes

  if mdphour gt blocks(0) then begin
    scc_avail_blocks,n0,z0,c0,/MDUMP
    n0=[n0,'Skipped']  ;added skipped marker for mdump
    z0=[z0,0]
    c0=[c0,'Skipping Mdump calibration']
    print,'************************************************************'
    print,'************************************************************'
    print,'!!!!  Scheduling mdump Calibration @ ',mdump,' !!!!!!!'
    print,'************************************************************'
    print,'************************************************************'
    for nz=0,n_elements(c0)-1 do print,' '+string((nz)+1,'(i1.1)')+' = (',c0[nz],')'
    mdumpv=4
    inp=''
    read,'Enter Mdump selection ['+trim(mdumpv)+']:  ',inp
    IF inp NE '' THEN mdumpv=fix(inp)
    help,mdumpv
    print,''
    print,' 0 = no'
    print,' 1 = All CW SECCHI-(A and B)'
    print,' 2 = COR1 CW SECCHI-B only'
;    print,' 2 = COR1 CCW SECCHI-B'
;    print,' 3 = COR1 CCW only SECCHI-B'
    read,'Enter spin test selection:  ',dospinv
    help,dospinv
    print,''
    print,' 0 = none'
    print,' 1 = LED'
    print,' 2 = Sky LinSeq'
    read,'Enter HI calibration selection:  ',dohicalv
    help,dohicalv
    wait,4
    if dospinv gt 0 then z0(mdumpv-1)=z0(mdumpv-1)+0.45
    coropen=[87,90]
    mdumpstart=-75
    if mdumpv ge 4 then mdumpstart=20
    mdumpv=mdumpv-1
    mdpschedule=n0[mdumpv]
    mdpcomment=c0[mdumpv]
    if mdumpv eq 3 then mdphour=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))+(fix(strmid(mdump,12,2))+20)/60. 
    if mdumpv ge 4 then mdphour=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))+(fix(strmid(mdump,12,2))-5)/60. 
    IF strpos(mdpcomment,'4hr') GE 0 THEN BEGIN
    	mdphourend=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))+1+(fix(strmid(mdump,12,2))+57)/60. 
    	coropen=[95,98]
    ENDIF 
    mb=where(blocks+2 gt mdphour and blocks lt mdphourend)
    mdpblocks=blocks(mb)
    mdpsize=z0[mdumpv]/n_elements(mdpblocks)
    cor2door=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))+(fix(strmid(mdump,12,2))+coropen(0))/60.
    cor1door=(fix(strmid(mdump,5,3))*24)+fix(strmid(mdump,9,2))+(fix(strmid(mdump,12,2))+coropen(1))/60.
    mdpcor2doorblock=blocks(where(blocks gt cor2door(0))-1) & mdpcor2doorblock=mdpcor2doorblock(0)
    mdpcor1doorblock=blocks(where(blocks gt cor1door(0))-1) & mdpcor1doorblock=mdpcor1doorblock(0)
    
    print,'************************************************************'
    print,'************************************************************'
    print,'!!!!  Scheduling mdump Calibration block @ ',mdump,' !!!!!!!'
    print,'************************************************************'
    print,'************************************************************'
    
    hicaltime2=''
    if dohicalv eq 1 then begin
        hicaltime=string(blocks(mb(0)-1)/24,'(i3.3)')+':'+string(round([(blocks(mb(0)-2)/24.)-blocks(mb(0)-2)/24]*24),'(i2.2)')
        read,'Enter HI calibraion time "doy:HH" or return to use default '+hicaltime+': ',hicaltime2
        if hicaltime2 ne '' then hicaltime=hicaltime2
        himdump=yr+':'+hicaltime+':00:00'      
    endif
    if dohicalv eq 2 then begin
        hicaltime=string(blocks(mb(0)-1)/24,'(i3.3)')
        read,'Enter HI calibration day "doy" or return to use same day ('+hicaltime+':00:00:00): ',hicaltime2
        if hicaltime2 ne '' then hicaltime=hicaltime2
        himdump=yr+':'+strmid(hicaltime,0,3)+':00:00:00'
    endif

 endif else begin
     mdump=''
     mdpcor2doorblock=-1
     mdpcor1doorblock=-1
  endelse
endif

hioffsetcschedule=''
if hioffset ne '' then begin
  hioffsetstart=15
  offsethour=(fix(strmid(hioffset,5,3))*24)+fix(strmid(hioffset,9,2))+(fix(strmid(hioffset,12,2))-15)/60. & offsethour=offsethour(0)
  offsethourend=(fix(strmid(hioffset,5,3))*24)+fix(strmid(hioffset,9,2))+1+(fix(strmid(hioffset,12,2))+45)/60. & offsethourend=offsethourend(0)
  hioffsetblocks=blocks(where(blocks+2 gt offsethour and blocks lt offsethourend))
  scc_avail_blocks,hioffsetcschedule,hioffsetsize,hioffsetcomment,/hioffset
  hioffsettype='HI offset'
  if offsethour gt blocks(0) then begin
        hioffsetblocks=blocks(where(blocks+2 gt offsethour and blocks lt offsethourend))
        hioffsetsize=hioffsetsize/n_elements(hioffsetblocks)
        print,'************************************************************'
        print,'************************************************************'
        print,'!!!!  Scheduling ',hioffsettype,' Calibration @ ',hioffset,' !!!!!!!'
        print,'************************************************************'
        print,'************************************************************'
  endif else hioffset=''
ENDIF


himdumpcschedule=''
if himdump ne '' then begin
  if dohicalv eq 1 then begin
     himdumpstart=0
     mdumphour=(fix(strmid(himdump,5,3))*24)+fix(strmid(himdump,9,2))+(fix(strmid(himdump,12,2)))/60. & mdumphour=mdumphour(0)
     mdumphourend=mdumphour+2 & mdumphourend=mdumphourend(0)
     scc_avail_blocks,himdumpcschedule,himdumpsize,himdumpcomment,/HILED
     hicaltype='HI LED'
  endif
  if dohicalv eq 2 then begin
     himdumpstart=0
     mdumphour=(fix(strmid(himdump,5,3))*24) & mdumphour=mdumphour(0)
     mdumphourend=(fix(strmid(himdump,5,3))*24)+2 & mdumphourend=mdumphourend(0)
     scc_avail_blocks,himdumpcschedule,himdumpsize,himdumpcomment,/HISKY
     hicaltype='HI Sky'
  endif
  if mdumphour gt blocks(0) then begin
        himdumpblocks=blocks(where(blocks+2 gt mdumphour and blocks lt mdumphourend))
        himdumpsize=himdumpsize/n_elements(himdumpblocks)
        print,'************************************************************'
        print,'************************************************************'
        print,'!!!!  Scheduling ',hicaltype,' Calibration @ ',himdump,' !!!!!!!'
        print,'************************************************************'
        print,'************************************************************'
  endif else himdump=''
endif

;ehours=ehours(where(ehours le max(blocks)+24)) 

redo=-1
mredo=-1

utc0=doy2utc(doy,yr)
utc2=doy2utc(doy2,yr)
;
; Find and read in report prior to input date
; 
inpdir=getenv('sched')+'/'+strupcase(sc0)+'/dailyats'
  rptfile=inpdir+'/reports/'+sc+'*'
  if keyword_set(testdl) then rptfile=rptfile+'_'+string(testdl,'(i3.3)')
  if keyword_set(resets) then rptfile=rptfile+'_resets'
  ;if keyword_set(dailyav) then rptfile=rptfile+'_dailyav'
  rptfile=rptfile+'.rpt'
  mbr_start=-1
  allrpts=file_search(rptfile)
  if allrpts[0] ne '' then begin
    alldays=rstrmid(allrpts,4,10)
    strput,alldays,'-',4
    strput,alldays,'-',7

    allutc=str2utc(alldays)

    nlast=find_closest(utc0.mjd,allutc.mjd,/less)
    
    line=''
    openr,lurpt,allrpts[nlast],/get_lun
    while ~eof(lurpt) do begin
    	readf,lurpt,line
        if strmid(line,0,2) eq '20' then $
        if fix(strmid(line,11,3)) eq doys(1) and fix(strmid(line,15,4)) eq SPB(1) and mbr_start eq -1  then begin
	    mbr_start=fix(strmid(line,61,3))
            ssr2start=fix(strmid(line,strlen(line)-3,3))
         endif
    endwhile  
    close,lurpt
    free_lun,lurpt
  endif
  if keyword_set(mbst) then mbr_start=mbst
  if keyword_set(ssr2st) then ssr2start=ssr2st
  if mbr_start eq -1 and ~keyword_set(mbst) then begin
    mbr_start=0
    inp=''
    print,'Start 1st pass = DOY '+string(doys[0],'(i3.3)')+'-'+string(SPB[0],'(i4.4)')
    print,'End 1st pass   = DOY '+string(doye[0],'(i3.3)')+'-'+string(EPB[0],'(i4.4)')
    read, 'Enter MB/left at end of above pass [0]:',inp
    IF inp NE '' THEN mbr_start=float(inp)
    
  endif

print,'Start prev pass = DOY '+string(doys(0),'(i3.3)')+'-'+string(SPB(0),'(i4.4)')
print,'End prev pass   = DOY '+string(doye(0),'(i3.3)')+'-'+string(EPB(0),'(i4.4)')
help,mbr_start
wait,5


nblocks=n_elements(blocks)
sched=strarr(nblocks)
schedcntchng=intarr(nblocks) +0

; Find and read in schedule from previous day
blckdate=utc2str(doy2utc((doy-1),yr),/DATE_ONLY)
prevsch=file_search(inpdir+'/20'+strmid(blckdate,2,2)+'/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch')
if prevsch(0) eq '' then read,'Enter full path for '+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch  ',prevsch
prevlist=readlist(prevsch(0))
prevlist=prevlist(where(strmid(prevlist,0,1) ne '#'))
pblk=where(blocks/24 eq doy-1,cnt)
if cnt gt 0  then prevblocks=blocks(pblk) else prevblocks=[0]

if blocks(0)/24 lt doy-1 then begin ; go 2 days back
  blckdate=utc2str(doy2utc((doy-2),yr),/DATE_ONLY)
  prevsch=file_search(inpdir+'/20'+strmid(blckdate,2,2)+'/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch')
  if prevsch(0) eq '' then read,'Enter full path for '+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch  ',prevsch
  prevlist2=readlist(prevsch(0))
  prevlist=[prevlist2(where(strmid(prevlist2,0,1) ne '#')),prevlist]
  pblk=where(blocks/24 eq doy-2,cnt)
  if cnt gt 0  then prevblocks=[blocks(pblk),prevblocks]
endif 
;
; First cut at schedule and special blocks
;
for n=0,n_Elements(blocks)-1 do begin
   z1=where(ehours le blocks(n)) & z2=n_elements(z1)-1
   
    if (blocks(n)-(blocks(n)/24)*24 eq 0) then usesch=schedule1 else usesch=schedules
    if ssr2blocks(0) ne -1 then begin
       zssr2=where(ssr2blocks eq blocks(n))
       if zssr2(0) gt -1 then  usesch=schedule2
       if zssr2(0) gt -1 and (blocks(n)-(blocks(n)/24)*24 eq 0) then usesch=schedule1ssr2 
    endif

    passlen=ehours(z1(z2)+1)-ehours(z1(z2))
    mbpu=mbpb(z1(z2)+1)   
;    if keyword_set(resets) then begin
;       if passlen lt 6. then passlen=ehours(z1(z2)+1)-ehours(z1(z2)-1) else passlen=ehours(z1(z2)+2)-ehours(z1(z2))
;       mbpu=mbpu*2.
;    endif
    if keyword_set(dailyav) then begin
      passlen=24 & mbpu=pbperday
    endif

;    maxpl=maxpasslen*(mbpu/706.)
    maxpl=maxpasslen
    zx=where(maxpl gt passlen)

    if zx(0) gt -1 then sched(n)=usesch(zx(0)) else sched(n)=usesch(n_elements(usesch)-1) 

    zst=where(prevblocks eq blocks(n))
    if zst(0) gt -1 then begin
          zst2=where(strpos(prevlist,strmid(blckdate,5,2)+strmid(blckdate,8,2)+string(blocks(n)-(blocks(n)/24)*24,'(i2.2)')) gt 0)
          if zst2(0) gt -1 then begin
            scht=prevlist(zst2(0))
	    sched(n)=strmid(scht,strpos(scht,'.bsf')-8,13)
            schedcntchng(n)=1
         endif
    endif

    if jopschedule ne '' then begin
       zst=where(jopblocks eq blocks(n))
       if zst(0) gt -1 then begin
           sched(n)=jopschedule
          schedcntchng(n)=1
       endif
    endif

    if stepcal ne '' then begin
       zst=where(stblocks eq blocks(n))
       if zst(0) gt -1 then begin
          sched(n)=stcschedule
          schedcntchng(n)=1
       endif
    endif

    if gtcal ne '' then begin
       zst=where(gtblocks eq blocks(n))
       if zst(0) gt -1 then begin
           sched(n)=gtcschedule
          schedcntchng(n)=1
       endif
    endif
    if hioffset ne '' then begin
       zst=where(hioffsetblocks eq blocks(n))
       if zst(0) gt -1 then begin
          sched(n)=hioffsetcschedule
          schedcntchng(n)=1
       endif
    endif
    if himdump ne '' then begin
       zst=where(himdumpblocks eq blocks(n))
       if zst(0) gt -1 then begin
          sched(n)=himdumpcschedule
          schedcntchng(n)=1
       endif
    endif

    if mdump ne '' then begin
       zst=where(mdpblocks eq blocks(n))
       if zst(0) gt -1 then begin
          sched(n)=mdpschedule
          schedcntchng(n)=1
       endif
    endif

    if degroll[0] ne '' then begin
       zst=where(degrollblocks eq blocks(n))
       if zst(0) gt -1 then begin
          sched(n)=rollschedule
          schedcntchng(n)=1
       endif
    endif

  print,sched(n),passlen
endfor

    if keyword_set(ISON) then begin
      isonschedule=['130919_4.bsf','130919_0.bsf','131123_1.bsf']
      isonsize=[23.197,23.512,22.455]
      isoncomment='/campaign/ison/'
      allisonmb=fltarr(n_Elements(blocks))
      allisonmb[n_Elements(blocks)-1]=-1
      for nj=1,n_Elements(blocks)-1 do allisonmb[nj-1]=where(allisonh ge blocks(nj-1) and allisonh lt blocks(nj)) 
      isonblocks=where(allisonmb ge 0,cnt)
      if cnt gt 0 then begin
        sched[isonblocks(0)]=isonschedule[0]
        sched[isonblocks[1]:isonblocks[n_elements(isonblocks)-1]]=isonschedule[1]
        schedcntchng[isonblocks]=1
        if sc0 eq 'b' then sched[isonblocks(8)]=isonschedule[2]
      endif 
    endif else isonschedule=''

;if ssr1 pointer is reset 1/2 way though playback
if keyword_set(resets) then begin
   resets=ehours-((ehours-bhours)/2)
   mbperrst=((ehours-bhours)/2)*mbpb/(ehours-bhours)
   resethrs=[ehours,resets]
   mbpb=[mbpb-mbperrst,mbperrst]
   mbpb=mbpb(sort(resethrs))
   rhhmm=(ehours-bhours)/2
   rhhmm=string(fix(rhhmm),'(i2.2)')+string(fix((rhhmm-fix(rhhmm))*60),'(i2.2)')
   mspb=fix(strmid(spb,2,2))+fix(strmid(rhhmm,2,2))
   hspb=fix(strmid(spb,0,2))+fix(strmid(rhhmm,0,2))
   zr=where(mspb ge 60)
   if zr(0) gt -1 then begin
      mspb(zr)=mspb(zr)-60
      hspb(zr)=hspb(zr)+1
   endif
   ehours=resethrs(sort(resethrs))
   bresethrs=[bhours,resets]
   bhours=bresethrs(sort(bresethrs))
   doys=[doys,doys]
   doys=doys(sort(resethrs))
   doye=[doye,doye]
   doye=doye(sort(resethrs))
   yr1=[yr1,yr1]
   yr1=yr1(sort(resethrs))

   SPB=[SPB,string(hspb,'(i2.2)')+string(mspb,'(i2.2)')]
   SPB=SPB(sort(bresethrs))
   EPB=[EPB,string(hspb,'(i2.2)')+string(mspb,'(i2.2)')]
   EPB=EPB(sort(resethrs)) 
   zr=n_Elements(mbpb)-1
   ehours=ehours(1:zr) & bhours=bhours(1:zr) & doys=doys(1:zr) & doye=doye(1:zr) & mbpb=mbpb(1:zr)
   SPB=SPB(1:zr) & EPB=EPB(1:zr)
   pbrate2=intarr(n_elements(pbrate)*2)
   ssr2kbps2=intarr(n_elements(pbrate)*2)
   for rn=0,n_elements(pbrate)-1 do begin
       pbrate2(rn*2)=pbrate(rn)
       pbrate2(rn*2+1)=pbrate(rn)
       ssr2kbps2(rn*2)=ssr2kbps(rn)
       ssr2kbps2(rn*2+1)=ssr2kbps(rn)
   endfor
   pbrate=pbrate2
   ssr2kbps=ssr2kbps2
endif

ssr2pb=((ssr2kbps*(ehours(0:n_Elements(ehours)-1)-bhours(0:n_Elements(ehours)-1))*3600.)/8000.)
ssr2pbest=((ssr2kbpsest*(ehours(0:n_Elements(ehours)-1)-bhours(0:n_Elements(ehours)-1))*3600.)/8000.)

adjsch=strarr(n_elements(ehours))
cntchange=intarr(n_elements(ehours))
maxedout=intarr(n_elements(ehours))


schedhrs=round(ehours)
schedhrs=schedhrs+(schedhrs/2.-schedhrs/2)*2
ssr1full=(mbpb/ssr1total)*100.

if keyword_set(resets) then ssr1full=ssr1full*2
mredo=0
;use36=2 ; if equal to 1 it will use the 36 hour blocks n_elemnets(schedules)-use36 the 36 hour blocks will be used the second time through after the barrow pcheck loop
;if keyword_set(force36) then use36=1
;
; NEED TO MOVE FORCE36 to scc_avail_blocks.pro. Always use all blocks returned from that pro.

first=1
changeblkctr=0
lastnpcheck2=0

ssr1mb=fltarr(n_elements(sched))
ssr2mb=fltarr(n_elements(sched))

getpercent:
print,'At getpercent...'

;
; build array with sizes of each block
;
for n=0,n_Elements(schedules)-1 do begin
  z=where(sched eq schedules(n))
  if(z(0) gt -1)then ssr1mb(z)=schsize(n)
endfor
for n=0,n_Elements(schedule1)-1 do begin
  z=where(sched eq schedule1(n))
  if(z(0) gt -1)then ssr1mb(z)=sch1size(n)
endfor
if ssr2blocks(0) ne -1 then begin
  for n=0,n_Elements(schedule2)-1 do begin
    z=where(sched eq schedule2(n))
    if(z(0) gt -1)then ssr1mb(z)=sch2size(n)
    if(z(0) gt -1)then ssr2mb(z)=sizessr2(n)
  endfor
  for n=0,n_Elements(schedule1ssr2)-1 do begin
    z=where(sched eq schedule1ssr2(n))
    if(z(0) gt -1)then ssr1mb(z)=sch1sizessr2(n)
    if(z(0) gt -1)then ssr2mb(z)=sizessr2_1(n)
  endfor
endif
if stepcal ne '' then begin
  for n=0,n_Elements(stcschedule)-1 do begin
    z=where(sched eq stcschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=stsize(n)
  endfor
endif
if gtcal ne '' then begin
  for n=0,n_Elements(gtcschedule)-1 do begin
    z=where(sched eq gtcschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=gtsize(n)
  endfor
endif
if hioffset ne '' then begin
  for n=0,n_Elements(hioffsetcschedule)-1 do begin
    z=where(sched eq hioffsetcschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=hioffsetsize(n)
  endfor
endif
if himdump ne '' then begin
  for n=0,n_Elements(himdumpcschedule)-1 do begin
    z=where(sched eq himdumpcschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=himdumpsize(n)
  endfor
endif
if mdump ne '' then begin
  for n=0,n_Elements(mdpschedule)-1 do begin
    z=where(sched eq mdpschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=mdpsize(n)
  endfor
endif
if degroll[0] ne '' then begin
  for n=0,n_Elements(rollschedule)-1 do begin
    z=where(sched eq rollschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=rollsize(n)
  endfor
endif

if jopschedule ne '' then begin
  for n=0,n_Elements(jopschedule)-1 do begin
    z=where(sched eq jopschedule(n))
    if(z(0) gt -1)then ssr1mb(z)=jopsize(n)
    if(z(0) gt -1)then ssr2mb(z)=jopssr2size(n)
  endfor
endif

if isonschedule[0] ne '' then begin
  for n=0,n_Elements(isonschedule)-1 do begin
    z=where(sched eq isonschedule[n])
    if(z[0] gt -1)then ssr1mb[z]=isonsize[n]
  endfor
endif

wz=where(ssr1mb EQ 0,nwz)
IF nwz GT 0 THEN BEGIN
    print,'The following blocks were not found:'
    FOR i=0,nwz-1 DO BEGIN 
        print,blkhrs[wz(i)],'   ',sched[wz(i)]
        inp=0.
        read,'Please enter an average block size in MB:',inp
        ssr1mb[wz(i)]=inp
        schedcntchng[wz(i)]=1
    ENDFOR
    stop
ENDIF

if keyword_set(ISON) and sc0 eq 'a' then begin
    hi2isonmb=fltarr(n_Elements(blocks))
    hi2isonmb[n_Elements(blocks)-1]=-1
    for n=1,n_Elements(blocks)-1 do hi2isonmb[n-1]=where(hi2isonh ge blocks(n-1) and hi2isonh le blocks(n)) 
    h1=where(hi2isonmb ge 0,cnt)
if cnt gt 0 then begin
    hi2isonmb(where(hi2isonmb gt -1))=1.04750
    hi2isonmb(where(hi2isonmb eq -1))=0
print,'***********ADDING MB FOR HI2ISON*************'
    ssr1mb=ssr1mb+hi2isonmb
endif
    hi1isonmb=fltarr(n_Elements(blocks))
    hi1isonmb[n_Elements(blocks)-1]=-1
    for n=1,n_Elements(blocks)-1 do hi1isonmb[n-1]=where(hi1isonh ge blocks(n-1) and hi1isonh le blocks(n)) 
    h1=where(hi1isonmb ge 0,cnt)
if cnt gt 0 then begin
    hi1isonmb(where(hi1isonmb gt -1))=2.71125
    hi1isonmb(where(hi1isonmb eq -1))=0
print,'***********ADDING MB FOR HI1ISON*************'
    ssr1mb=ssr1mb+hi1isonmb
endif
    allisonmb=fltarr(n_Elements(blocks))
    allisonmb[n_Elements(blocks)-1]=-1
    for n=1,n_Elements(blocks)-1 do allisonmb[n-1]=where(allisonh ge blocks(n-1) and allisonh le blocks(n)) 
    h1=where(allisonmb ge 0,cnt)
if cnt gt 0 then begin
    allisonmb(where(allisonmb gt -1))=6.33617
    allisonmb(where(allisonmb eq -1))=0
print,'***********ADDING MB FOR ALL ISON*************'
    ssr1mb=ssr1mb+allisonmb
endif
    cor2isonmb=fltarr(n_Elements(blocks))
    cor2isonmb[n_Elements(blocks)-1]=-1
    for n=1,n_Elements(blocks)-1 do cor2isonmb[n-1]=where(cor2isonh ge blocks(n-1) and cor2isonh le blocks(n)) 
    h1=where(cor2isonmb ge 0,cnt)
if cnt gt 0 then begin
    cor2isonmb(where(cor2isonmb gt -1))= 6.28871
    cor2isonmb(where(cor2isonmb eq -1))=0
print,'***********ADDING MB FOR COR2 ISON*************'
    ssr1mb=ssr1mb+cor2isonmb
endif

endif
if keyword_set(ISON) and sc0 eq 'b' then begin
    allisonmb=fltarr(n_Elements(blocks))
    allisonmb[n_Elements(blocks)-1]=-1
    for n=1,n_Elements(blocks)-1 do allisonmb[n-1]=where(allisonh ge blocks(n-1) and allisonh le blocks(n)) 
    h1=where(allisonmb ge 0,cnt)
if cnt gt 0 then begin
    allisonmb(where(allisonmb gt -1))=5.58475
    allisonmb(where(allisonmb eq -1))=0
print,'***********ADDING MB FOR ALL ISON*************'
    ssr1mb=ssr1mb+allisonmb
endif
    cor2isonmb=fltarr(n_Elements(blocks))
    cor2isonmb[n_Elements(blocks)-1]=-1
    for n=1,n_Elements(blocks)-1 do cor2isonmb[n-1]=where(cor2isonh ge blocks(n-1) and cor2isonh le blocks(n)) 
    h1=where(cor2isonmb ge 0,cnt)
if cnt gt 0 then begin
    cor2isonmb(where(cor2isonmb gt -1))= 5.73092
    cor2isonmb(where(cor2isonmb eq -1))=0
print,'***********ADDING MB FOR COR2 ISON*************'
    ssr1mb=ssr1mb+cor2isonmb
endif

endif


ssr1p1=fltarr(n_elements(ssr1mb))
for n=0,n_elements(ssr1mb)-1 do ssr1p1(n)=total(ssr1mb(0:n))
ssr1p1=[0,ssr1p1]
blocks2=[blocks,blocks(n_elements(blocks)-1)+2]

IF ~keyword_set(PLOTRES) THEN plotres= 15 ; min.

t5min=findgen((max(ehours)-min(ehours)+1)*60./plotres) *plotres/60. +min(ehours)

;ssr1p2=spline(blocks2,ssr1p1,t5min)
ssr1p2=interpol(ssr1p1,blocks2,t5min)

if(min(ssr1p2) lt 0)then ssr1p3=ssr1p2+abs(min(ssr1p2)) else ssr1p3=ssr1p2
ssr1p4=fltarr(n_elements(ehours))

;for n=1,n_elements(ehours)-1 do begin
;   z1=where(t5min ge ehours(n))
;   ssr1p3(z1)=ssr1p3(z1)-ssr1p3(z1(0))
;   ssr1p4(n)=ssr1p3(z1(0)-1)
;endfor


;mbremain=[0,mbr_start]
;for i=1,n_Elements(ssr1p4)-2 do begin
;  mbr1=ssr1p4(i)-mbpb(i)+mbremain(i)
;  if mbr1 ge 0 then mbremain=[mbremain,mbr1] Else mbremain=[mbremain,0]
;endfor

;remove mb over 706 

mbremain=[0,mbr_start]

ssr1p3=ssr1p3+mbr_start
; ssr1p3 is the MB on SSR1 at 5 minute intervals

fulltotal=0
mbover=0
for n=1,n_elements(ehours)-1 do begin
   z1=where(t5min ge ehours(n))
   if n ne n_elements(ehours)-1 then 	z2=where(t5min ge ehours(n) and t5min le ehours(n+1)) else $
    	    	    	    	    	z2=where(t5min ge ehours(n))
   ; ssr1p3[z2] is MB on SSR1 during end-to-end interval;
   ; end-to-end duration is 5min * nz2
   minpb=min([ssr1p3(z2),mbpb(n)])
   ; minpb is MB, the lesser of [the amount on SSR1 before playback] or [the amount played back]
   ssr1p3(z1)=ssr1p3(z1)-minpb;+mbremain(n)
   ; ssr1p3 is amount on SSR1 after playback 
   ssr1p4(n)=ssr1p3(z1(0)-1)
   ; ssr1p4 is amount on SSR1 at EOT
   fullcheck=where(ssr1p3(z2) gt 706)
   if fullcheck(0) gt -1 then begin
      fulltotal=fulltotal+n_elements(fullcheck)
      fullcorr=max(ssr1p3(z2(fullcheck)))-706
      mbover=mbover+fullcorr
      ssr1p3(z2(fullcheck))=707
      f1=z2(fullcheck(n_elements(fullcheck)-1))+1
      f2=n_Elements(ssr1p3)-1
      ssr1p3(f1:f2)=ssr1p3(f1:f2)-fullcorr
;      Oplot,t5min,ssr1p3
   endif
   mbr1=ssr1p3(z1(0))
   if mbr1 ge 0 then mbremain=[mbremain,mbr1] Else mbremain=[mbremain,0]
endfor
;ssr1p4=ssr1p4+mbremain

ssr1percent=((ssr1p4)/ssr1total)*100.

ssr1=((ssr1p3)/ssr1total)*100
;ssr1r=remain & ssr1r(0)=(mbr_start/ssr1total)*100

d1=utc2tai(str2utc(yr+'-01-01T00:00:00'))-24*3600.  ;moved to get calander date for report files

;thrp=tai2utc(d1+(thr*3600.))
;thrs=utc2str(thrp)
thrp=tai2utc(d1+(t5min*3600.))
thrs=utc2str(thrp)
utb=tai2utc(d1+(bhours*3600.))
ute=tai2utc(d1+(ehours*3600.))

IF (first) THEN BEGIN
    TEK_COLOR
    device,decompose=0
    device,retain=2
    window,xsize=1600,ysize=512
    ; set_plot,'ps'
    ; device ,/color

    utplot,thrs,ssr1,ystyle=9,yrange=[-5,100],ytitle='Percent',title=mtitle,$
    ;    ymargin=[.15,.35],position=[.05,.05,.95,.95],background=1,color=0,timerange=[thrp(0),thrp(n_Elements(thrp)-1)],xstyle=1
    ymargin=[.15,.35],position=[.05,.05,.95,.95],background=1,color=0,timerange=[doy2utc(doy-1,yr),doy2utc(doy2+2,yr)],xstyle=1
    axis,yaxis=1,yrange=706.*!y.crange/100,ytitle='Mega Bytes',ystyle=1,color=0

    evt_grid,utb,linestyle=2,color=2
    evt_grid,ute,linestyle=2,color=4
    outplot,[utc2str(doy2utc(doy-1,yr)),utc2str(doy2utc(doy2+2,yr))],[0,0],color=0
    outplot,[utc2str(doy2utc(doy,yr)),utc2str(doy2utc(doy,yr))],[0,100],linestyle=2,color=0,thick=3
    outplot,[utc2str(doy2utc(doy2+1,yr)),utc2str(doy2utc(doy2+1,yr))],[0,100],linestyle=2,color=0,thick=3
    
ENDIF ELSE BEGIN
    avar=pcheck[0]>0
    IF  doye[avar] LT doy2+1 THEN outplot,thrs,ssr1,color=0
    
ENDELSE

;
; define schcmnt from which the symbol for each block is determined
;
schcmnt=strarr(n_elements(sched))
for n=0,n_elements(sched)-1 do begin
        z=where(schedule1 eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comment1(z(0)) 
        z=where(schedules eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comments(z(0)) 
        z=where(schedule2 eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comment2(z(0)) 
        z=where(schedule1ssr2 eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comment1ssr2(z(0)) 
        if datatype(jopblocks) ne 'UND' then begin
           z=where(jopblocks eq blocks(n)) & if (z(0) gt -1) then schcmnt(n)=jopcomment;(z(0)) 
        endif
        if(sched(n) eq stcschedule) then schcmnt(n)=stcomment
        if(sched(n) eq gtcschedule) then schcmnt(n)=gtcomment
        if(sched(n) eq hioffsetcschedule) then schcmnt(n)=hioffsetcomment
        if(sched(n) eq himdumpcschedule) then schcmnt(n)=himdumpcomment
        if(sched(n) eq mdpschedule) then schcmnt(n)=mdpcomment
        if(sched(n) eq rollschedule) then schcmnt(n)=rollcomment
endfor
;
; plot the symbols indicating which block
;
tmp=intarr(153)-1.5
clr=[0,2,3,4,5,6,7]
xout1=((utc2tai(thrs(n_Elements(thrs)-1))-utc2tai(thrs(0)))/7)/2
; xout1 is about 1 day

for ns=0,n_Elements(schedule2)-1 do begin
  z=where(strpos(schcmnt,'/'+strmid(comment2(ns),8,5)) ge 0 and strpos(schcmnt,strmid(comment2(ns),0,3)) ge 0)
  znblk=0
  if(z(0) gt -1) then begin
     outplot,blkhrs(z),tmp(z),psym=1,color=clr(ns)
     znblk=n_Elements(where(blocks(z)/24 ge doy and blocks(z)/24 le doy2)) 
  endif
  xyouts,xout1*4,94-ns*4,string(znblk,'(i3)')+' '+strmid(comment2(ns),0,10)+'hr schedules',color=clr(ns)
endfor

IF first then wait,2


;if mredo eq 0 or mredo ge 3 then goto, pcheck_r1
if mredo eq 1 then goto, pcheck_r2

pcheck_r1:
; 
; This is where we see where smaller blocks need to be added
;
print,ssr1percent-ssr1full
pcheck=where(ssr1percent ge 100 and cntchange ne 1) 
if pcheck[0] eq n_Elements(ssr1percent)-1 then pcheck[0]=(-1) ; not enough weekly_schedules to fix last group
if pcheck(0) ne -1 then begin
  pcheck=where(ssr1percent ge ssr1full+(100*maxmbleft/706.) and cntchange ne 1)
  ssr1over100=where(ssr1percent gt 100)
  ssr1empty=where(ssr1percent-ssr1full le 0,cnt)
  noneedtochange=intarr(n_Elements(blocks))
  if cnt gt 1 then begin
    ssr1empty=ssr1empty(1:cnt-1)
    for i1=0,cnt-2 do begin
      i2=where(ssr1over100 lt ssr1empty(i1),cnt1)
      if cnt1 gt 0 then noneedtochange(where(blocks gt ehours(ssr1over100(i2(cnt1-1))) and blocks le ehours(ssr1empty(i1))))=1
    endfor
  endif
endif


; ssr1full is % played back in a track
; pcheck is where amount in SSR1 exceeds amount played back
print,'At pcheck_r1 pcheck is'
print,pcheck

if keyword_set(resets)then  pcheck2=where(ssr1p4-mbpb lt maxmbleft*2) else $
    	    	    	    pcheck2=where(ssr1p4-mbpb lt maxmbleft)
			    ; ~2% for resets else ~ 1% for 

; pcheck2 is where amount left on SSR1 after PB is OK
npcheck2=n_elements(pcheck2)
;IF npcheck2 GT lastnpcheck2 THEN changeblkctr=0
; after pcheck2 increases, we have a new interval; otherwise still on previous interval
lastnpcheck2=npcheck2

if npcheck2 gt 1 then begin
  for n=1,n_elements(pcheck2)-1 do begin
    n2=where(ssr1percent(pcheck2(n-1)+1:pcheck2(n)) ge 100)
    ; n2 is where amt on SSR1 between T of SSR1=0 and previous T when SSR1=0, is GE 100%
    help,n2
    if n2(0) eq -1 then begin
    
      n3=(where(pcheck lt pcheck2(n-1) or pcheck gt pcheck2(n)))
      help,n3
      print,n3
      
      ; n3 is where 100% is exceeded, but not between T and previous T
      if n3(0) gt -1 then pcheck=pcheck(n3) else pcheck=(-1)
    endif
  endfor
endif
; Now pcheck is where 100% is not played back after last time ssr1 was at maxmbleft

IF (first) THEN pcheck0=pcheck[0]

print,'pcheck:'
print,pcheck
print,'pcheck2:'
print,pcheck2

;wait,1
redo=0
if (pcheck(0) gt -1)then begin 
    redo=1
 ;    for n1=0,n_elements(pcheck)-1 do begin
 ; as much as possible we want to keep changed blocks together
     n1=0
     n=pcheck(n1)
     if keyword_set(resets) then n2=n-2 else n2=n-1
     if mredo eq 4 then n2=n-4 
     if n2 le -1 then n2=0
     
    ; changeblk is blocks between last time SSR1 was 0 and [next time SSR1 is zero, or chngintrvl days, or end of available interval]
    ; This may change each redo.

	 n2 = (pcheck[n1]-1)>0
	 trkb4nxt0 = pcheck[n_elements(pcheck)-1]
	 ; default is all remaining

    pcheck3=[where(ssr1p4-mbpb le 0),trkb4nxt0]
    np3=1
    while pcheck3[np3] LE n do np3=np3+1
    trkb4nxt0=pcheck3[np3]-1	
    help,trkb4nxt0
    
    	; ; this is track before the next ssr1 at 0
	; ; now check for chngintrvl days
     	;IF doye[n]-doye[n2] GT chngintrvl THEN BEGIN
     	;    wlt5=where(doye LE doye[n2]+chngintrvl,nwlt5)
	;    n=wlt5[nwlt5-1]
    	;ENDIF
	
;     if n2 ne 0 then repeat n2=n2-1 until ((n2 eq 0) or (mbremain(n2) eq 0))
    	changeblk=where(blocks ge schedhrs(n2) and blocks lt schedhrs(trkb4nxt0),nchb0)

     
;     if keyword_set(jopsch) then changeblk=changeblk(where(sched(changeblk) ne jopschedule(0)))
;     if stepcal ne '' then changeblk=changeblk(where(sched(changeblk) ne stcschedule(0)))
;     if gtcal ne '' then changeblk=changeblk(where(sched(changeblk) ne gtcschedule(0)))
;     if hioffset ne '' then changeblk=changeblk(where(sched(changeblk) ne hioffsetcschedule(0)))
;     if mdump ne '' then changeblk=changeblk(where(sched(changeblk) ne mdpschedule(0)))

    	notspecial=where(schedcntchng(changeblk) + noneedtochange(changeblk) lt 1, ncb)

	; if we are at the last block in interval, go back to first block
	
	IF changeblkctr GE ncb THEN BEGIN
	    message,'Going back to start of interval:',/inf
	    print,blkhrs[changeblk[0]]
	    wait,2
	    changeblkctr=0
	ENDIF
        

    IF pcheck[0] NE pcheck0 THEN BEGIN
        print,'Starting new interval!'
        wait,4
        changeblkctr=0
	pcheck0=pcheck[0]
    ENDIF




     
     ; schedule2 is ssr2/120
     ; schedule1 is nossr2/115
     ; schedules is nossr2/120
     ; schedule1ssr2 is ssr2/115
    	if notspecial(0) gt -1 then begin
    	    changeblk=changeblk(notspecial)
            
            ; for each block, use the next lighter one, if available
	    
    	    blocktochange=changeblk[changeblkctr]
    	    if (blocktochange ne -1) then begin
	    ; replace block with next-lower block of same type
		prevblock=sched(blocktochange)
		; which kind of block is this?
    		z2c=where(schedule2 eq prevblock)
		z2c=z2c[0]
		newblock=schedule2[(z2c+1)<(nsch2-1)]
		; choose the next lower block, or the same if lowest block already there.


		IF z2c lt 0 THEN BEGIN
    		    z2c=where(schedule1ssr2 eq prevblock)
		    z2c=z2c[0]
		    newblock=schedule1ssr2[(z2c+1)<(nsch12-1)]
		    ; choose the next lower block, or the same if lowest block already there.
		ENDIF
		; now do the no-ssr2 blocks
		IF z2c lt 0 THEN BEGIN
    		    z2c=where(schedules eq prevblock)
		    z2c=z2c[0]
		    newblock=schedules[(z2c+1)<(nschs-1)]
		    ; choose the next lower block, or the same if lowest block already there.
		ENDIF
		IF z2c lt 0 THEN BEGIN
    		    z2c=where(schedule1 eq prevblock)
		    z2c=z2c[0]
		    newblock=schedule1[(z2c+1)<(nsch1-1)]
		    ; choose the next lower block, or the same if lowest block already there.
		ENDIF
		sched[blocktochange]=newblock
		;IF newblock EQ '110622_3.bsf' THEN stop
	    ; if there is not a next-lower block, cntchange[n]=1
		IF sched(blocktochange) EQ prevblock THEN schedcntchng[blocktochange]=1
		changeblkctr=changeblkctr+1
    	    endif else cntchange(n)=1
    	endif else cntchange(n)=1

;  endfor
endif
print,'cntchange:'
print,cntchange
;print,'changeblk:'
help,changeblk,changeblkctr
help,blocktochange,redo,mredo
avar=pcheck[0]>0

;IF  blkut[blocktochange].mjd LT utc2.mjd+1 THEN wait,2

first=0
;stop
if datatype(blocktochange) eq 'UND' then redo=0 else IF blocktochange GE nblocks-1 and schedcntchng[blocktochange] GT 0 THEN redo=0

if (redo eq 1) then goto, getpercent
if mredo eq 3 then begin
   mredo=4
   cntchange(*)=0
  goto , getpercent
endif

;if keyword_set(dailyav) then begin
if mredo ne 0 then goto, done
pcheck_r2:
print,'At pcheck_r2'
  zval=(max(mbpb-ssr1total))
  ; zval is the most playback happens
  if zval gt -10 then zval=-10
  if keyword_set(barrow) then zval=barrow*(-1)
  pcheck=where((ssr1p4)-mbpb le zval and maxedout ne 1)
   mredo=0
   print,total(ssr1percent),'   at pcheck_r2 pcheck:'
   print,pcheck
   ;wait,1
   if (total(pcheck) gt 0) then begin 
        mredo=1
        n=pcheck(n_elements(pcheck)-1)
	; last pb where ssr1% is GT 0 after
	if keyword_set(resets) then n2=n-4 else n2=n-2
    	; n2 is 2 pb before this
;        if ssr1percent(n2) lt ssr1full then if keyword_set(resets) then n2=n2-2 else n2=n2-1
	if n2 eq -1 then n2=0
        changeblk=where(blocks ge schedhrs(n2) and blocks lt schedhrs(n))
	; changeblk is time between 3rd to last pb and last pb 
        ;if keyword_set(jopsch) then changeblk=changeblk(where(sched(changeblk) ne jopschedule(0)))
        ;if stepcal ne '' then changeblk=changeblk(where(sched(changeblk) ne stcschedule(0)))
        ;if gtcal ne '' then changeblk=changeblk(where(sched(changeblk) ne gtcschedule(0)))
        ;if hioffset ne '' then changeblk=changeblk(where(sched(changeblk) ne hioffsetcschedule(0)))
       ;if mdump ne '' then changeblk=changeblk(where(sched(changeblk) ne mdpschedule(0)))
      tmp2=where(schedcntchng(changeblk) ne 1)
     if tmp(0) gt -1 AND tmp2(0) GT -1 then begin
       changeblk=changeblk(where(schedcntchng(changeblk) ne 1))
        z2c=where(schedules eq sched(changeblk(n_elements(changeblk)-1)))
        if(z2c(0) eq -1)then z2c=where(schedule1 eq sched(changeblk(n_elements(changeblk)-1)))
        if(z2c(0) eq -1)then z2c=where(schedule2 eq sched(changeblk(n_elements(changeblk)-1)))
        if(z2c(0) eq -1)then z2c=where(schedule1ssr2 eq sched(changeblk(n_elements(changeblk)-1)))
        sch1=schedules(z2c) & sch2=schedule1(z2c) & sch3=schedule2(z2c) & sch4=schedule1ssr2(z2c) & sch1=sch1(0) & sch2=sch2(0) & sch3=sch3(0) & sch4=sch4(0)

        z2x=where(schedules eq sched(changeblk(0)))
        if(z2x(0) eq -1)then z2x=where(schedule1 eq sched(changeblk(0)))
        if(z2x(0) eq -1)then z2x=where(schedule2 eq sched(changeblk(0)))
        if(z2x(0) eq -1)then z2x=where(schedule1ssr2 eq sched(changeblk(0)))
        sch1x=schedules(z2x) & sch2x=schedule1(z2x) & sch3x=schedule2(z2x) & sch4x=schedule1ssr2(z2x) & sch1x=sch1x(0) & sch2x=sch2x(0) & sch3x=sch3x(0) & sch4x=sch4x(0)

        z2=where(sched(changeblk) ne sch1x and sched(changeblk) ne sch2x and sched(changeblk) ne sch3x  and sched(changeblk) ne sch4x and sched(changeblk) ne sch1 and sched(changeblk) ne sch2 and sched(changeblk) ne sch3 and sched(changeblk) ne sch4)
        if(z2(0) eq -1)then  z2=where(sched(changeblk) ne sch1 and sched(changeblk) ne sch2 and sched(changeblk) ne sch3 and sched(changeblk) ne sch4)

 ;       if(z2(0) ne -1 and z2x gt 0) then begin
       if(z2(0) ne -1 and z2x le n_Elements(schedules)-1) then begin
            for nz2=0,n_elements(z2)-1 do begin

            cs=where(schedules eq sched(changeblk(z2(nz2)))) & if cs(0) gt 0 then sched(changeblk(z2(nz2)))=schedules(cs(0)-1)
            cs=where(schedule1 eq sched(changeblk(z2(nz2)))) & if cs(0) gt 0 then sched(changeblk(z2(nz2)))=schedule1(cs(0)-1)
            cs=where(schedule2 eq sched(changeblk(z2(nz2)))) & if cs(0) gt 0 then sched(changeblk(z2(nz2)))=schedule2(cs(0)-1)
            cs=where(schedule1ssr2 eq sched(changeblk(z2(nz2)))) & if cs(0) gt 0 then sched(changeblk(z2(nz2)))=schedule1ssr2(cs(0)-1)

          endfor
       endif else maxedout(n)=1
     endif else maxedout(n)=1
  endif
;endif
if (mredo eq 1) then goto, getpercent
if (mredo eq 0) then begin
   mredo=3
   cntchange(*)=0
   ssr1full=100
   if keyword_set(nossr2) then begin
     ssr2t=fltarr(n_elements(ssr2pb))
     ssr2pb1=ssr2pb
     if datatype(ssr2start) eq 'UND' then read,'Enter ssr2 start value  ',ssr2start
     ssr2pb1(0)=100-ssr2start
     ssr2pb1(1)=((mbpb(1)-ssr1p4(1)>0)+ssr2pb(1)<100)
     for nsr2=0,n_elements(ssr2pb)-1 do ssr2t(nsr2)=total(ssr2pb1(0:nsr2))
     mbpb=mbpb+(ssr2t-100<ssr2pb1>0)
     ssr2pb=ssr2pb-(ssr2t-100<ssr2pb1>0)
   endif
    if ~keyword_set(no36) then use36=1
    goto, getpercent 
endif

done:
; SSR2 MB

ssr2p1=fltarr(n_elements(ssr2mb))
for n=0,n_elements(ssr2mb)-1 do ssr2p1(n)=total(ssr2mb(0:n))
ssr2p1=[0,ssr2p1]
ssr2mbpb=(mbpb-ssr1p4>0)+ssr2pb<100

ssr2pbread=fltarr(n_Elements(blocks2))
for n=0,n_Elements(ehours)-1 do begin
  n2=where(blocks ge bhours(n) and blocks le ehours(n))
  if n2(0) ne -1 then ssr2pbread(n2)=ssr2mbpb(n)/n_Elements(n2)
endfor
ssr2rd=fltarr(n_elements(ssr2pbread))
for n=0,n_elements(ssr2pbread)-1 do ssr2rd(n)=total(ssr2pbread(0:n))

ssr2rd5=interpol(ssr2rd,blocks2,t5min)
ssr2wt5=interpol(ssr2p1,blocks2,t5min)

;
; define schcmnt from which the symbol for each block is determined
;
schcmnt=strarr(n_elements(sched))
for n=0,n_elements(sched)-1 do begin
        z=where(schedule1 eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comment1(z(0)) 
        z=where(schedules eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comments(z(0)) 
        z=where(schedule2 eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comment2(z(0)) 
        z=where(schedule1ssr2 eq sched(n)) & if (z(0) gt -1) then schcmnt(n)=comment1ssr2(z(0)) 
        if datatype(jopblocks) ne 'UND' then begin
           z=where(jopblocks eq blocks(n)) & if (z(0) gt -1) then schcmnt(n)=jopcomment;(z(0)) 
        endif
        if(sched(n) eq stcschedule) then schcmnt(n)=stcomment
        if(sched(n) eq gtcschedule) then schcmnt(n)=gtcomment
        if(sched(n) eq hioffsetcschedule) then schcmnt(n)=hioffsetcomment
        if(sched(n) eq himdumpcschedule) then schcmnt(n)=himdumpcomment
        if(sched(n) eq mdpschedule) then schcmnt(n)=mdpcomment
        if(sched(n) eq rollschedule) then schcmnt(n)=rollcomment
        if isonschedule[0] ne '' then begin
         if(sched(n) eq isonschedule[0]) then schcmnt(n)=isoncomment
         if(sched(n) eq isonschedule[1]) then schcmnt(n)=isoncomment
       endif
endfor

yrs=yr

pbtime=string(fix(ehours-bhours),'(i2.2)')+':'+string(fix(((ehours-bhours)-fix(ehours-bhours))*60.),'(i2.2)')


remain=[ssr1percent-((mbpb/ssr1total)*100)]
z=where(remain le 0)
remain(z)=0

mbperday=total(ssr1p4(where(doys ge doy and doys le doy2))-mbremain(where(doys ge doy and doys le doy2)))/n_elements(where(doys ge doy and doys le doy2))
if keyword_set(resets) then mbperday=mbperday*2
;hours=ehours
;hours2=hours+.1
;thr=[hours,hours2]
;z=sort(thr)
;ssr1p=[ssr1,ssr1r]
;ssr1p=ssr1p(z)
;thr=thr(z);-ehours(0)

  rday=utc2str(doy2utc(doy,yr))
  rptfile=outdir+'/reports/'+sc+'_'+strmid(rday(0),0,4)+'_'+strmid(rday(0),5,2)+'_'+strmid(rday(0),8,2)
  if keyword_set(testdl) then rptfile=rptfile+'_'+string(testdl,'(i3.3)')
  if keyword_set(resets) then rptfile=rptfile+'_resets'
  ;if keyword_set(dailyav) then rptfile=rptfile+'_dailyav'
  rptfile=rptfile+'.rpt'

;spawn,'mkdir '+outdir+'/reports',/sh

ny=where(yr1 gt yr1(0)) & if ny(0) gt -1 then doys(ny)=fix(doys(ny)-doysmax)

;openw,2,rptfile,width=200
if keyword_set(testdl) then print,'Downlink rate = ',testdl
if keyword_set(testphr) then print,'Playback (Hrs) = ',testphr
print,'Schedules for ',utc2str(doy2utc(doy,yr),/date),' thru ',utc2str(doy2utc(doy2,yr),/date),'  Daily Av MB scheduled = ',string(mbperday,'(i3)'),' Played Back = '+string(pbperday,'(i3)')
print,'Date       DOY SPB  EPB  EPB-EPB pbtime rate MB/pb MB/sch  MB/left SSR1% %aftpb SSR2MB/pb'
pdate=strarr(n_elements(ehours))
for n=1,n_elements(ehours)-1 do begin
  etoe_time=string(fix(ehours(n)-ehours(n-1)),'(i2.2)')+':'+string(fix(((ehours(n)-ehours(n-1))-fix(ehours(n)-ehours(n-1)))*60.),'(i2.2)')
  
  zt=where(blocks ge ehours(n-1) and blocks le ehours(n))
  if zt(0) gt -1 then begin 
    schcmnt1=strmid(schcmnt(zt),0,3)
   ; print,ehours(n-1),ehours(n),blocks(zt(0)),blocks(zt(n_elements(zt)-1))
    print,utc2str(doy2utc(doys(n),yr1(n)),/DATE_ONLY),' ',string(doys(n),'(i3)'),' ',SPB(n),' ',EPB(n),' ',etoe_time,$
    	    	    	    	'   ',pbtime(n),'   ',string(pbrate(n),'(i3.3)'),'  ',string(mbpb(n),'(i3.3)'),'  ',string(ssr1p4(n)-mbremain(n),'(i3.3)'),'     ',$
				string(mbremain(n),'(i5)'),'    ',string(ssr1percent(n),'(i3.3)'),'  ',$
				string(remain(n),'(i3.3)'),'  ',string(ssr2dl*(mbpb(n)-ssr1p4(n)>0)+ssr2pb(n)<100,'(i5)'),'  ',string(1.*(mbpb(n)-ssr1p4(n)>0)+ssr2pbest(n)<100,'(i5)');,'   ',schcmnt1;,'  ',adjsch(n)
     pdate(n)=utc2str(doy2utc(doys(n),yr1(n)),/DATE_ONLY)+' '+strmid(EPB(n),0,2)+':'+strmid(EPB(n),2,2)
  endif
endfor
if fulltotal gt 0 then print,'Hours above 100 ='+string((fulltotal*plotres)/60.)
if mbover gt 0 then print,'MB over 706 ='+string(mbover)
;close,2


  rday=utc2str(doy2utc(doy,yr))
  pltfile=outdir+'/reports/'+sc+'_'+strmid(rday(0),0,4)+'_'+strmid(rday(0),5,2)+'_'+strmid(rday(0),8,2)
  if keyword_set(testdl) then pltfile=pltfile+'_'+string(testdl,'(i3.3)')
  if keyword_set(resets) then pltfile=pltfile+'_resets'
  ;if keyword_set(dailyav) then pltfile=pltfile+'_dailyav'

mtitle='SSR1 Usage'
if keyword_set(testdl) then mtitle=mtitle+'  Downlink rate = '+string(testdl,'(i3)')
if keyword_set(testphr) then mtitle=mtitle+'  Playback (Hrs) = '+testphr
if keyword_set(resets) then mtitle=mtitle+' resets'
if keyword_set(dailyav) then mtitle=mtitle+' daily average'

utplot,thrs,ssr1,ystyle=9,yrange=[-5,100],ytitle='Percent',title=mtitle,$
;    ymargin=[.15,.35],position=[.05,.05,.95,.95],background=1,color=0,timerange=[thrp(0),thrp(n_Elements(thrp)-1)],xstyle=1
;    ymargin=[.15,.35],position=[.05,.05,.95,.95],background=1,color=0,timerange=[doy2utc(doy-1,yr),doy2utc(doy2+2,yr)],xstyle=1
    background=1,color=0,timerange=[doy2utc(doy-1,yr),doy2utc(doy2+2,yr)],xstyle=1,xmargin=[10,7]
axis,yaxis=1,yrange=706.*!y.crange/100,ytitle='Mega Bytes',ystyle=1,color=0

for np=0,n_elements(bhours)-1 do outplot,[utc2str(tai2utc(d1+(bhours(np)*3600.))),utc2str(tai2utc(d1+(bhours(np)*3600.)))],[0,100],linestyle=2,color=2
for np=0,n_elements(ehours)-1 do outplot,[utc2str(tai2utc(d1+(ehours(np)*3600.))),utc2str(tai2utc(d1+(ehours(np)*3600.)))],[0,100],linestyle=2,color=4


ssr2hrp=tai2utc(d1+(ssr2blocks*3600.))
thrs2=utc2str(ssr2hrp)
ssr2y=intarr(n_Elements(thrs2))+0
outplot,thrs2,ssr2y,psym=4,color=4

xyouts,xout1*1.5,94,'Average MB scheduled per 24 hours = '+string(mbperday,'(i3)'),color=0
xyouts,xout1*1.5,90,'Average MB played back per 24 hours = '+string(pbperday,'(i3)'),color=0
xyouts,xout1*1.5,86,'Estimated MB played back per Pass = '+string(embpb,'(i3)'),color=0
xyouts,xout1*4,  94,'Totals only include blocks between black dashed lines:',color=0

outplot,[utc2str(doy2utc(doy-1,yr)),utc2str(doy2utc(doy2+2,yr))],[0,0],color=0
outplot,[utc2str(doy2utc(doy,yr)),utc2str(doy2utc(doy,yr))],[0,100],linestyle=2,color=0,thick=3
outplot,[utc2str(doy2utc(doy2+1,yr)),utc2str(doy2utc(doy2+1,yr))],[0,100],linestyle=2,color=0,thick=3

;
; plot the symbols indicating which block
;
tmp=intarr(153)-1.5
clr=[0,2,3,4,5,6,7]
d1=utc2tai(str2utc(yr+'-01-01T00:00:00'))-24*3600.
blkhrs=utc2str(tai2utc(d1+(blocks*3600.)))

for ns=0,n_Elements(schedule2)-1 do begin
  z=where(strpos(schcmnt,'/'+strmid(comment2(ns),8,5)) ge 0 and strpos(schcmnt,strmid(comment2(ns),0,3)) ge 0)
  znblk=0
  if(z(0) gt -1) then begin
     outplot,blkhrs(z),tmp(z),psym=1,color=clr(ns)
     in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
     
  endif
  xyouts,xout1*4,94-(ns+1)*4,string(znblk,'(i3)')+' '+strmid(comment2(ns),0,10)+'hr schedules',color=clr(ns)

endfor
ns=n_Elements(schedule2)+1
w=where(comment2 EQ jopcomment,jopsameasschedule)
if ~(jopsameasschedule) then begin
    z=[-1]
    for nz=0,n_elements(jopblocks)-1 do begin
       zn=where(blocks eq jopblocks(nz)) & zn=zn(0)
       if zn(0) gt -1 then z=[z,zn(0)]
    endfor
    z=z(where(z gt -1))
    if z(0) gt -1 then begin
    	outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules))
        in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    	xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+jopcomment(0),color=clr(n_Elements(schedules))
    endif
    ns=ns+1
endif
if stepcal ne '' then begin
  z=where(schcmnt eq stcomment) 
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+1)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+stcomment,color=clr(n_Elements(schedules)+1)
  endif
  IF znblk EQ 0 THEN stepcal=''
    ns=ns+1
endif 

if gtcal ne '' then begin
  z=where(schcmnt eq gtcomment) 
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+2)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+gtcomment,color=clr(n_Elements(schedules)+2)
  endif
  IF znblk EQ 0 THEN gtcal=''
    ns=ns+1
endif 

if hioffset ne '' then begin
  z=where(schcmnt eq hioffsetcomment) 
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+3)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+hioffsetcomment,color=clr(n_Elements(schedules)+3)
  endif
  IF znblk EQ 0 THEN hioffset=''
    ns=ns+1
endif 

if himdump ne '' then begin
  z=where(schcmnt eq himdumpcomment) 
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+2)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+himdumpcomment,color=clr(n_Elements(schedules)+2)
  endif
  IF znblk EQ 0 THEN himdump=''
    ns=ns+1
endif 


if mdump ne '' then begin
  z=where(schcmnt eq mdpcomment) 
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+1)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+mdpcomment,color=clr(n_Elements(schedules)+1)
  endif
  IF znblk EQ 0 THEN mdump=''
    ns=ns+1
endif 

if degroll[0] ne '' then begin
  z=where(schcmnt eq rollcomment) 
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+2)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' '+rollcomment,color=clr(n_Elements(schedules)+2)
  endif
endif 

if isonschedule[0] ne '' then begin
  z=[where(sched eq isonschedule[0]), where(sched eq isonschedule[1])]
  if z(0) gt -1 then begin
    outplot,blkhrs(z),tmp(z),psym=4,color=clr(n_Elements(schedules)+2)
    in_interval=where(blocks(z)/24 ge doy and blocks(z)/24 le doy2, znblk) 
    xyouts,xout1*4,94-(ns)*4,string(znblk,'(i3)')+' 19 Hour no event detect',color=clr(n_Elements(schedules)+2)
  endif
endif 

if stepcal ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled Stepped Calibration @ ',stepcal,' !!!!!!!'
  print,'!!!!  make sure block schedule ',stcschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if mdump ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled mdump Calibration @ ',mdump,' !!!!!!!'
  print,'!!!!  make sure block schedule ',mdpschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if gtcal ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled GT Calibration @ ',gtcal,' !!!!!!!'
  print,'!!!!  make sure block schedule ',gtcschedule,' is uploaded !!!!!!'
  print,'!!!!  and offpoint block ',gtoffsets,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if hioffset ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled ',hioffsettype,' Calibration @ ',hioffset,' !!!!!!!'
  print,'!!!!  make sure block schedule ',hioffsetcschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if himdump ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled ',hicaltype,' Calibration @ ',himdump,' !!!!!!!'
  print,'!!!!  make sure block schedule ',himdumpcschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if degroll[0] ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled roll @ ',degroll,' !!!!!!!'
  print,'!!!!  make sure block schedule ',rollschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif

; write schedules
if keyword_set(u51hr) then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Using 51 hour schedule  !!!!!!!'
  print,'!!!!  make sure block schedule ',schedule2(n_Elements(schedule2)-1) ,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
  bsft=where(strpos(sched,'.bsft') gt -1)
  if bsft(0) gt -1 then sched(bsft)=strmid(sched(bsft),0,12)
endif

;IF keyword_set(TESTSCH) THEN BEGIN
    print,'To continue and use these settings, .cont'
    stop
;ENDIF




t=ftvread(/png,filename=pltfile,/noprompt)
;device,/close
;set_plot,'x'
;cmd='ps2pdf idl.ps '+outfile
;spawn,cmd


openw,2,rptfile,width=200
if keyword_set(testdl) then printf,2,'Downlink rate = ',testdl
if keyword_set(testphr) then printf,2,'Playback (Hrs) = ',testphr
printf,2,'Schedules for ',utc2str(doy2utc(doy,yr),/date),' thru ',utc2str(doy2utc(doy2,yr),/date),'  Daily Av MB scheduled = ',string(mbperday,'(i3)'),' Played Back = '+string(pbperday,'(i3)')
printf,2,'Date       DOY SPB  EPB  EPB-EPB pbtime rate MB/pb MB/sch  MB/left SSR1% %aftpb SSR2MB/pb'
pdate=strarr(n_elements(ehours))
for n=1,n_elements(ehours)-1 do begin
  etoe_time=string(fix(ehours(n)-ehours(n-1)),'(i2.2)')+':'+string(fix(((ehours(n)-ehours(n-1))-fix(ehours(n)-ehours(n-1)))*60.),'(i2.2)')
  
  zt=where(blocks ge ehours(n-1) and blocks le ehours(n))
  if zt(0) gt -1 then begin 
    schcmnt1=strmid(schcmnt(zt),0,3)
    print,ehours(n-1),ehours(n),blocks(zt(0)),blocks(zt(n_elements(zt)-1))
    printf,2,utc2str(doy2utc(doys(n),yr1(n)),/DATE_ONLY),' ',string(doys(n),'(i3)'),' ',SPB(n),' ',EPB(n),' ',etoe_time,$
    	    	    	    	'   ',pbtime(n),'   ',string(pbrate(n),'(i3.3)'),'  ',string(mbpb(n),'(i3.3)'),'  ',string(ssr1p4(n)-mbremain(n),'(i3.3)'),'     ',$
				string(mbremain(n),'(i5)'),'    ',string(ssr1percent(n),'(i3.3)'),'  ',$
				string(remain(n),'(i3.3)'),'  ',string(ssr2dl*(mbpb(n)-ssr1p4(n)>0)+ssr2pb(n)<100,'(i5)'),'  ',string(1.*(mbpb(n)-ssr1p4(n)>0)+ssr2pbest(n)<100,'(i5)');,'   ',schcmnt1;,'  ',adjsch(n)
     pdate(n)=utc2str(doy2utc(doys(n),yr1(n)),/DATE_ONLY)+' '+strmid(EPB(n),0,2)+':'+strmid(EPB(n),2,2)
  endif
endfor
if fulltotal gt 0 then printf,2,'Hours above 100 ='+string((fulltotal*plotres)/60.)
if mbover gt 0 then printf,2,'MB over 706 ='+string(mbover)
close,2


loadblock=''

if ~keyword_set(testdl) then begin
   close,4
   close,5
   docmdfile=1
   IF keyword_set(NOCMDFILE) THEN docmdfile=0
   IF (docmdfile) THEN openw,5,cmdfile

   stschedule=0
   stepcaladded=0
   gtcaladded=0
   hioffsetadded=0
   himdumpadded=0
   mdumpadded=0
   mdpcor2d=0
   mdpcor1d=0
   loadblock2=''
   rolladded=0
  schdir=['']
  for n=0,n_Elements(blocks)-1 do begin
    if(blocks(n)/24 ge doy and blocks(n)/24 le doy2) then begin
     blckdate=utc2str(doy2utc((blocks(n)/24),yr),/DATE_ONLY)
     blckyr=strmid(blckdate,2,2)
     blckyr2=strmid(blckdate,2,2)
     
     if fix(blckyr) ge 15 then blckyr=inttobn(fix(blckyr)-5,36,1) else blckyr=strmid(blckdate,3,1)
     if (blocks(n)-(blocks(n)/24)*24 eq 0) then begin
        loadblock=''
        stschedule=1
 
        schfile=outdir+'/20'+strmid(blckdate,2,2)+'/'+sc+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.tmp'
        schfile2=outdir+'/20'+strmid(blckdate,2,2)+'/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch'
        schfileup=updir+'/20'+strmid(blckdate,2,2)+'/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch'
        schfileld='temp/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch'
 
        if schdir(n_elements(schdir)-1) ne outdir+'/20'+strmid(blckdate,2,2)+'/' then schdir=[schdir,outdir+'/20'+strmid(blckdate,2,2)+'/'] 
        openw,4,schfile
        printf,4,'#;PT Schedule = ',schfile2
        printf,4,'---00000000B16B0
     	if (sched(n) ne stcschedule and sched(n) ne mdpschedule and sched(n) ne rollschedule) then begin
          printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(blocks(n)-(blocks(n)/24)*24,'(i2.2)'),'0500F2800',sched(n) 
          printf,4,'#   ',schcmnt(n)
        endif

        if keyword_set(ISON) and sc0 eq 'a' then begin
          isonst='2013-10-10' & isonend='2013-11-22' ;HI2
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'035521F2800131001_3.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-21' & isonend='2013-11-27' ;HI1
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'003901F2800131031_2.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-28' & isonend='2013-11-28' ;ALL
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'000700F2800131107_0.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-29' & isonend='2013-11-29' ;COR2
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'001200F2800131031_4.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-30' & isonend='2013-12-03' ;HI1
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'003901F2800131126_0.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-12-04' & isonend='2013-12-06' ;HI1
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'043901F2800131126_1.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-12-07' & isonend='2013-12-07' ;HI1
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'003901F2800131126_0.bsf'
             printf,4,'#  campaign/ison/'
          endif  
	endif

        if keyword_set(ISON) and sc0 eq 'b' then begin
          isonst='2013-11-26' & isonend='2013-11-26' ;COR2
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'061200F2800131106_2.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-27' & isonend='2013-11-27' ;COR2
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'001200F2800131106_1.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-28' & isonend='2013-11-28' ;ALL
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'000700F2800131107_2.bsf'
             printf,4,'#  campaign/ison/'
          endif  
          isonst='2013-11-29' & isonend='2013-11-29' ;COR2
	  if blckdate ge isonst and blckdate le isonend then begin
             printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),'001200F2800131106_1.bsf'
             printf,4,'#  campaign/ison/'
          endif  
	endif


     endif 
     if (stschedule gt 0 and blocks(n)-(blocks(n)/24)*24 ne 0 and sched(n) ne stcschedule and sched(n) ne gtcschedule and sched(n) ne hioffsetcschedule and sched(n) ne himdumpcschedule and sched(n) ne mdpschedule and sched(n) ne rollschedule) then begin
        m1check=where(m1sch eq sched(n)) 
        s30check=where(s30sch eq sched(n)) 
        if m1check(0) gt -1 then ststring= '0100F2800' else if s30check(0) gt -1 then ststring= '0030F2800' else ststring= '0000F2800'
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(blocks(n)-(blocks(n)/24)*24,'(i2.2)'),ststring,sched(n)
        printf,4,'#   ',schcmnt(n) 
     endif
     if (stschedule gt 0 and sched(n) eq stcschedule and stepcaladded eq 0) then begin  ;stepcal
        ststring= '00F2800'
        stmin=(fix(strmid(stepcal,9,2))*60)+fix(strmid(stepcal,12,2))+stepcalst
	sthr=stmin/60
	stmin=stmin-sthr*60
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,sched(n)
        printf,4,'#   ',stcomment
        stepcaladded=1     
        loadblock=[loadblock,sched(n)]
     endif
     if (stschedule gt 0 and sched(n) eq gtcschedule and gtcaladded eq 0) then begin  ;gtcal
        ststring= '00F2800'
        stmin=(fix(strmid(gtcal,9,2))*60)+fix(strmid(gtcal,12,2))
	sthr=stmin/60
	stmin=stmin-sthr*60
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,sched(n)
        printf,4,'#   ',gtcomment
        if sc eq 'STB' then ststring= '23F2800' else ststring= '00F2800'
        stmin=(fix(strmid(gtcal,9,2))*60)+fix(strmid(gtcal,12,2)+1)
	sthr=stmin/60
	stmin=stmin-sthr*60
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,gtoffsets
        printf,4,'#   gt offpoints'
        gtcaladded=1     
        loadblock=[loadblock,sched(n)]
     endif
     if (stschedule gt 0 and sched(n) eq hioffsetcschedule and hioffsetadded eq 0) then begin  ;hioffset
        ststring= '00F2800'
        stmin=(fix(strmid(hioffset,9,2))*60)+fix(strmid(hioffset,12,2)-hioffsetstart)
	sthr=stmin/60
	stmin=stmin-sthr*60
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,sched(n)
        printf,4,'#   ',hioffsetcomment
        hioffsetadded=1     
        loadblock=[loadblock,sched(n)]
     endif
     if (stschedule gt 0 and sched(n) eq himdumpcschedule and himdumpadded eq 0) then begin  ;himdump
        ststring= '00F2800'
        stmin=(fix(strmid(himdump,9,2))*60)+fix(strmid(himdump,12,2)-himdumpstart)
	sthr=stmin/60
	stmin=stmin-sthr*60
       ; set hi mdump but do not write hi sky schedule it is written in the first block of day
        if sched(n) ne '100504_1.bsf' then printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,sched(n)
        if sched(n) ne '100504_1.bsf' then printf,4,'#   ',himdumpcomment
        himdumpadded=1     
        loadblock=[loadblock,sched(n)]
     endif
     if (stschedule gt 0 and sched(n) eq mdpschedule and mdumpadded eq 0) then begin  ;mdump
        ststring= '00F2800'
        stmin=(fix(strmid(mdump,9,2))*60)+fix(strmid(mdump,12,2))+mdumpstart
	sthr=stmin/60
	stmin=stmin-sthr*60
        if mdpschedule ne 'Skipped' then begin
           printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,sched(n)
           printf,4,'#   ',mdpcomment
           loadblock=[loadblock,sched(n)]
           if dospinv ge 1 then begin
             stmin=(fix(strmid(mdump,9,2))*60)+fix(strmid(mdump,12,2))+mdumpstart+40
	     sthr=stmin/60
	     stmin=stmin-sthr*60
             if dospinv eq 1 then begin
               printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,'100421_2.bsf'
               printf,4,'#  mdumps/spins/'
               loadblock=[loadblock,'100421_2.bsf']
	     endif
             if dospinv eq 2 then begin
               printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,'121022_1.bsf'
               printf,4,'#  mdumps/cor1spins/'
               loadblock=[loadblock,'121022_1.bsf']
	     endif
 ;            if dospinv eq 2 then begin
 ;              printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,'120614_2.bsf'
 ;              printf,4,'#  mdumps/ccwspins/'
 ;              loadblock=[loadblock,'120614_2.bsf']
;	     endif
;             if dospinv eq 3 then begin
;               printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr,'(i2.2)'),string(stmin,'(i2.2)'),ststring,'120614_1.bsf'
;               printf,4,'#  mdumps/ccwspins/'
;               loadblock=[loadblock,'120614_1.bsf']
;	     endif

	   endif
        endif
        mdumpadded=1     
     endif
     if (stschedule gt 0 and sched(n) eq rollschedule) then begin  ;dwellroll
       if (rolladded eq 0) then begin
        z=where(strmid(degroll,5,3) eq blocks(n)/24)
	extmin=strmid(degroll(z),12,2)
	ststring= 'F2800'
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(blocks(n)-(blocks(n)/24)*24,'(i2.2)'),string(fix(strmid(rolloffset,0,2))+fix(extmin),'(i2.2)'),strmid(rolloffset,3,2),ststring,sched(n)
        printf,4,'#   ',rollcomment
        rolladded=1     
        t=where(loadblock eq sched[n],cnt)
	if cnt eq 0 then loadblock=[loadblock,sched(n)]
       endif else rolladded=0 
     endif


; 0pen COR2 DOOR
     if (stschedule gt 0 and blocks(n) eq mdpcor2doorblock and mdpcor2d eq 0) then begin  ;mdump
        stmin=(cor2door-blocks(n))*60;(fix(strmid(mdump,9,2))*60)+fix(strmid(mdump,12,2))+coropen(0)
	sthr=round(stmin)/60
	stmin=round(stmin)-sthr*60
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr+blocks(n)-(blocks(n)/24)*24,'(i2.2)'),string(stmin,'(i2.2)'),'00516CA'
        printf,4,'#   OPEN COR2 DOOR'
        mdpcor2d=1     
     endif

; Open COR1 DOOR
     if (stschedule gt 0 and blocks(n) eq mdpcor1doorblock and mdpcor1d eq 0) then begin  ;mdump
        stmin=(cor1door-blocks(n))*60;(fix(strmid(mdump,9,2))*60)+fix(strmid(mdump,12,2))+coropen(1)
	sthr=round(stmin)/60
	stmin=round(stmin)-sthr*60
        printf,4,blckyr,strmid(blckdate,5,2),strmid(blckdate,8,2),string(sthr+blocks(n)-(blocks(n)/24)*24,'(i2.2)'),string(stmin,'(i2.2)'),'00316CB'
        printf,4,'#   OPEN COR1 DOOR'
        mdpcor1d=1
     endif


     if (blocks(n)-(blocks(n)/24)*24 eq 22 and stschedule gt 0) then begin
    	printf,4,'---00000000T16C2'
	print,'Closing ',schfile
    	close,4
    	wait,2
    	IF (docmdfile) THEN BEGIN
	    cmd='fix_schedule '+schfile+' '+schfile2
	    print,cmd
	    wait,3
        spawn,cmd

; NEW IS.gz installed and corrected date issue on 02-11-2016
;        if fix(blckyr2) eq 15 then begin
;          m1sched=readlist(schfile)
;          for n1m=0,n_Elements(m1sched)-1 do begin
;            if strmid(m1sched[n1m],0,1) ne '#' and strmid(m1sched[n1m],0,1) ne '-' then begin
;              yr=2000+bntoint(strmid(m1sched[n1m],0,1),36)+5
;              sdate=anytim2utc(string(yr,'(i4.4)')+'/'+strmid(m1sched[n1m],1,2)+'/'+strmid(m1sched[n1m],3,2))
;              sdate.mjd=sdate.mjd-1
;              udate=utc2str(sdate,/date_only)
;              blckyr=strmid(udate,2,2)
;              blckyr=inttobn(fix(blckyr)-5,36,1)
;              tmp=m1sched[n1m]
;              strput,tmp,blckyr+strmid(udate,5,2)+strmid(udate,8,2),0 
;              m1sched[n1m]=tmp
;            endif
;          endfor

;          break_file,schfile,v1,path,sday,sfx
;          outfile=path+'m1day/'+sday+'.tmp'
;          break_file,schfile2,v1,path,sday,sfx
;          outfile2=path+'m1day/'+sday+'.sch'
;          openw,slun,outfile,/get_lun
;          for n1=0,n_Elements(m1sched) -1 do printf,slun,m1sched[n1]
;          close,slun
;          free_lun,slun

;          cmd='fix_schedule '+outfile+' '+outfile2
;          print,cmd
;          wait,3
;          spawn,cmd

;          schfileup=updir+'/20'+strmid(blckdate,2,2)+'/m1day/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch'
;          schfileld='temp/'+strmid(udate,2,2)+strmid(udate,5,2)+strmid(udate,8,2)+'00.sch'

 ;       endif        
 ;       if fix(blckyr2) ge 16 then begin
 ;         m1sched=readlist(schfile)
 ;         for n1m=0,n_Elements(m1sched)-1 do begin
 ;           if strmid(m1sched[n1m],0,1) ne '#' and strmid(m1sched[n1m],0,1) ne '-' then begin
 ;             tmp=m1sched[n1m]
 ;             strput,tmp,'---00',0 
 ;             m1sched[n1m]=tmp
 ;           endif
 ;         endfor

;          break_file,schfile,v1,path,sday,sfx
;          outfile=path+'rts/'+sday+'.tmp'
;          break_file,schfile2,v1,path,sday,sfx
;          outfile2=path+'rts/'+sday+'.sch'
;          openw,slun,outfile,/get_lun
;          for n1=0,n_Elements(m1sched) -1 do printf,slun,m1sched[n1]
;          close,slun
;          free_lun,slun

;          cmd='fix_schedule '+outfile+' '+outfile2
;          print,cmd
;          wait,3
;          spawn,cmd

;          schfileup=updir+'/20'+strmid(blckdate,2,2)+'/rts/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch'
;          schfileld='temp/'+strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)+'00.sch'

;        endif
; END date fix
	    
          if n_Elements(loadblock) gt 1 then begin
               bsffiles=readlist('~/loads/PT/flight/operations/BSF/bsffiles.txt')
               for zb=1,n_Elements(loadblock)-1 do begin
	         z=where(strpos(bsffiles,loadblock(zb)) gt -1)
                 z2=where(strpos(loadblock2,loadblock(zb)) gt -1,cnt)
                 if z(0) gt -1 and cnt eq 0 then begin
                     z=z(n_elements(z)-1)
	             printf,5,'loadfile,600,PT/flight/operations/'+strmid(bsffiles(z),0,strlen(bsffiles(z))-12)+loadblock(zb)+',temp/'+loadblock(zb)+',X'
                     loadblock2=[loadblock2,loadblock(zb)+' has been added to the weekly schedule load']
                 endif else loadblock2=[loadblock2,'********!!!!!! Block file '+loadblock(zb)+' has not been added to weekly_schedule MUST BE added manually !!!!!*********']
               endfor
	       endif
    	    printf,5,'loadfile,300,'+schfileup+','+schfileld+',X'
    	    dm14=utc2str(doy2utc(((blocks(n)/24)-14),yr),/DATE_ONLY)
    	    printf,5,'Command,180,FMDELETE,file,temp/'+strmid(dm14,2,2)+strmid(dm14,5,2)+strmid(dm14,8,2)+'00.sch,X'
    	    wait,2
            if ~keyword_set(nohtml) then blcksched2html, strmid(sc,2,1), strmid(blckdate,2,2)+strmid(blckdate,5,2)+strmid(blckdate,8,2)
    	ENDIF
     endif
    endif
  endfor
  close,4
  IF (docmdfile) THEN close,5
endif

IF not keyword_set(TESTSCH) THEN for n=1,n_Elements(schdir) -1 do begin
  delcmd='/bin/rm -f '+schdir(n)+'*.tmp'
  spawn,delcmd
  delcmd='/bin/rm -f '+schdir(n)+'/rts/*.tmp'
  spawn,delcmd
endfor

if stepcal ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled Stepped Calibration @ ',stepcal,' !!!!!!!'
  print,'!!!!  make sure block schedule ',stcschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if mdump ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled mdump Calibration @ ',mdump,' !!!!!!!'
  print,'!!!!  make sure block schedule ',mdpschedule,' is uploaded !!!!!!'
  if dospinv eq 1 then print,'!!!!  and  block schedule 100421_2.bsf is uploaded !!!!!!'
  if dospinv eq 2 then print,'!!!!  and  block schedule 120614_2.bsf is uploaded !!!!!!'
  if dospinv eq 3 then print,'!!!!  and  block schedule 120614_1.bsf is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if gtcal ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled GT Calibration @ ',gtcal,' !!!!!!!'
  print,'!!!!  make sure block schedule ',gtcschedule,' is uploaded !!!!!!'
  print,'!!!!  and offpoint block ',gtoffsets,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if hioffset ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled ',hioffsettype,' Calibration @ ',hioffset,' !!!!!!!'
  print,'!!!!  make sure block schedule ',hioffsetcschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
if himdump ne '' then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Scheduled ',hicaltype,' Calibration @ ',himdump,' !!!!!!!'
  print,'!!!!  make sure block schedule ',himdumpcschedule,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif

; write schedules
; write schedules
if keyword_set(u51hr) then begin
  print,'************************************************************'
  print,'************************************************************'
  print,'!!!!  Using 51 hour schedule  !!!!!!!'
  print,'!!!!  make sure block schedule ',schedule2(n_Elements(schedule2)-1) ,' is uploaded !!!!!!'
  print,'************************************************************'
  print,'************************************************************'
endif
for zb = 0,n_elements(loadblock2)-1 do print,loadblock2(zb)

trackwnopbs=where(misspbs ne '')
if trackwnopbs(0) gt -1 then begin
     print,'!!!!!!!!!!!! Some tracks have no playbacks!!!!!!!!!!!!'
     print,misspbs(trackwnopbs)
     print,'*************************************************'
endif


end
