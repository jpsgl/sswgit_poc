; $Id: blcksched2html.pro,v 1.8 2014/08/07 15:31:06 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : blcksched2html
;               
; Purpose     : to create schedules with html link to the block schedules files.
;               
; Explanation : 
;               
; Use         : IDL> blcksched2html,sc,yymmdd
;    
; Inputs      : sc = A or B
;   	    	yymmdd = yymmdd
;               
; Outputs     : a html version of the daily schedules for SECCHI-wiki
;
; Keywords :	
;
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : planning 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Simspon 05/04/2009 
;               
; Modified    :
;
; $Log: blcksched2html.pro,v $
; Revision 1.8  2014/08/07 15:31:06  secchia
; added sidelobe keyword
;
; Revision 1.7  2014/08/07 13:16:25  hutting
; added sidelobe directory
;
; Revision 1.6  2010/06/07 12:28:02  secchib
; works with 240kbps links
;
; Revision 1.5  2010/05/10 15:27:34  secchib
; added yearly directories for schedule files
;
; Revision 1.4  2009/12/28 14:15:23  secchia
; corrected year in html files
;
; Revision 1.3  2009/05/28 14:56:00  secchib
; added line returns if not bsf file
;
; Revision 1.2  2009/05/04 13:19:40  secchia
; uses file_search instead of findfile
;
; Revision 1.1  2009/05/04 12:58:45  mcnutt
; new pro to create html schedule files
;

pro blcksched2html, sc, yymmdd, sidelobe=sidelobe
     sfile=file_search('~/loads/PT/flight/operations/'+strupcase(sc)+'/dailyats/20'+strmid(yymmdd,0,2)+'/'+yymmdd+'*.sch')
     if keyword_set(sidelobe) then sfile=file_search('~/loads/PT/flight/operations/sidelobe/'+strupcase(sc)+'/'+yymmdd+'*.sch')
     htmlyr=string(strmid(yymmdd,0,2)+2000,'(i4)')
     sfile=sfile(0)
     bsffiles=readlist('~/loads/PT/flight/operations/BSF/bsffiles.txt')
     openr,slun,sfile,/get_lun
     htmlfile='~/loads/PT/flight/operations/'+strupcase(sc)+'/dailyats/html/20'+strmid(yymmdd,0,2)+'/'+yymmdd+'00.html'
     openw,wslun,htmlfile,/get_lun
     printf,wslun,'<p>'
     line='' & line2=''
     while not(eof(slun)) do begin
       readf,slun,line
       bsf=strpos(line,'.bsf')
       pound=strpos(line,'#')
       dash=strpos(line,'--')
       if pound eq -1 and bsf gt -1 and dash eq -1 then begin
          bsfile=strmid(line,bsf-8,12)
	  print,bsfile
          z=where(strpos(bsffiles,bsfile) gt -1)
          if z(0) gt -1 then begin
            z=z(n_elements(z)-1)
	    path='http://stereo.nrl.navy.mil/PT/flight/operations/'+strmid(bsffiles(z),0,strlen(bsffiles(z))-12)
            if ~keyword_set(sidelobe) then readf,slun,line2
            printf,wslun,htmlyr+'/'+strmid(line,1,2)+'/'+strmid(line,3,2)+' '+strmid(line,5,2)+':'+strmid(line,7,2)+':'+strmid(line,9,2)+'  <a href="'+path+'">'+bsfile+'<a>'+' &nbsp' +line2+'<br />'
          endif else printf,wslun,htmlyr+'/'+strmid(line,1,2)+'/'+strmid(line,3,2)+' '+strmid(line,5,2)+':'+strmid(line,7,2)+':'+strmid(line,9,2)+'  '+strmid(line,11,strlen(line)-11)+'<br />'
       endif
       if pound eq -1 and bsf eq -1 and dash eq -1 then begin
            readf,slun,line2
            printf,wslun,htmlyr+'/'+strmid(line,1,2)+'/'+strmid(line,3,2)+' '+strmid(line,5,2)+':'+strmid(line,7,2)+':'+strmid(line,9,2)+'  '+strmid(line,11,strlen(line)-11)+' '+line2+'<br />'
       endif
       if pound ne -1 or dash ne -1 then printf,wslun,line+'<br />'
     endwhile
     printf,wslun,'</p>'
    close,slun
    close,wslun
    free_lun,slun
    free_lun,wslun
END   
