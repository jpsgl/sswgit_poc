; $Id: sidelobe_schedule_rpt.pro,v 1.5 2014/12/22 15:32:51 secchia Exp $
;
; Project     : STEREO SECCHI
;                   
; Name        : sidelobe_schedule_rpt
;               
; Purpose     : report image save time and download time during sidelobe operations.
;               
; Explanation : 
;               
; Use         : IDL> sidelobe_schedule_rpt
;    
; Inputs      : 
;               
; Outputs     : 
;
; Keywords :	
;               
; Common      : 
;               
; Restrictions: 
;               
; Side effects: 
;               
; Category    : Pipeline 
;               
; Prev. Hist. : None.
;
; Written     : Lynn Hutting 08/15/2014 
;               
; Modified    :
;
; $Log: sidelobe_schedule_rpt.pro,v $
; Revision 1.5  2014/12/22 15:32:51  secchia
; correcct year for 2015 and 2016 schedules
;
; Revision 1.4  2014/08/21 12:55:29  hutting
; corrected times
;
; Revision 1.3  2014/08/21 12:01:49  secchia
; completed schedules door close removal
;
; Revision 1.2  2014/08/20 18:24:15  secchia
; fixed call to get_sidelobe_sched_times
;
; Revision 1.1  2014/08/20 14:05:43  hutting
; first rev
;
;

pro get_sidelobe_sched_times,sc,d1,d2

common sidelobe_sched_times,saveimg,iptimes,matchip,downip,scu,d1u,downobsnum,downtimes

if datatype(d1u) ne 'UND' then if d1 eq d1u and scu eq sc then goto,noupdate

schdir=getenv('sched')+'/sidelobe/'+strupcase(sc)+'/'
schdir2=getenv('sched')+'/'+strupcase(sc)+'/dailyats/'
ut0=nrlanytim2utc(d1)
ut1=nrlanytim2utc(d2)


mjd=ut0
FOR i=ut0.mjd-1,ut1.mjd DO BEGIN
  mjd.mjd=i

ud=anytim2utc(mjd)
yyyymmdd=utc2yymmdd(mjd,/yyyy)
sfile=file_search(schdir+'/'+strmid(yyyymmdd,2,6)+'00.sch')
if sfile eq '' then sfile=file_search(schdir2+'/'+strmid(yyyymmdd,0,4)+'/'+strmid(yyyymmdd,2,6)+'00.sch')
lst=readlist(sfile)      
if lst[0] ne '' then blocklst1=lst(where(strpos(lst,'#') ne 0 and strpos(lst,'-') ne 0))
if lst[0] ne '' then cmntlst1=lst(where(strpos(lst,'#') ne 0 and strpos(lst,'-') ne 0)+1)
if datatype(blocklst) eq 'UND' then blocklst=blocklst1 else blocklst=[blocklst,blocklst1] 
if datatype(cmntlst) eq 'UND' then cmntlst=cmntlst1 else cmntlst=[cmntlst,cmntlst1]
ENDFOR

z=where(strpos(blocklst,'.bsf') gt 0)
cmntlst=cmntlst[z]
blocklst=blocklst[z]
bsfile=strmid(blocklst,16,12)
yr=strmid(blocklst,0,1)
tmp=where(yr eq 'A',cnt)
if cnt gt 0 then yr[tmp] ='5'
tmp=where(yr eq 'B',cnt)
if cnt gt 0 then yr[tmp] ='6'
btime='201'+yr+'-'+strmid(blocklst,1,2)+'-'+strmid(blocklst,3,2)+' '+strmid(blocklst,5,2)+':'+strmid(blocklst,7,2)+':'+strmid(blocklst,9,2)
bsffiles=readlist(getenv('sched')+'/sidelobe/bsffiles.txt')

downip=['IP28','IP63','IP67','IP68','IP78','IP77']
saveimg=['IP29','IP55','IP56','IP66','IP79','IP82']
iptimes=strarr(n_Elements(saveimg),n_elements(blocklst)*2)
downtimes=strarr(n_Elements(downip),n_elements(blocklst)*2)


for n=0,n_Elements(bsfile)-1 do begin
  z=where(strpos(bsffiles,bsfile[n]) gt -1,cnt)
  if cnt gt 0 then begin
    lst=readlist(getenv('sched')+'/'+bsffiles[z[0]])
    lst=lst(where(strpos(lst,'---') gt -1))
    offset=strmid(lst[0],5,2)*60*60+strmid(lst[0],7,2)*60+strmid(lst[0],9,2)
    break_file,bsffiles[z],path,dir,name,ext
    for n1=0,n_Elements(downip)-1 do begin
      cmd='grep '+downip[n1]+' '+getenv('sched')+'/'+dir+'SEC20'+strmid(name,0,6)+'00'+strmid(name,7,1)+'.summary'
      spawn,cmd,ipgrep
      if ipgrep[0] ne '' then begin
        tmp=where(fix(strmid(ipgrep,18,2)*60*60+strmid(ipgrep,21,2)*60+strmid(ipgrep,24,2)-offset) le fix(rstrmid(cmntlst[n],0,3)*60),cnt)
        if cnt gt 0 then begin
          ipgrep=ipgrep[tmp]
          t1=anytim2tai(btime[n])+strmid(ipgrep,18,2)*60*60+strmid(ipgrep,21,2)*60+strmid(ipgrep,24,2)-offset
          z=where(downtimes[n1,*] eq '',cnt)    
          downtimes[n1,z[0]:z[0]+n_Elements(ipgrep)-1]=utc2str(tai2utc(t1))
      endif
    endif
  endfor
  for n1=0,n_Elements(saveimg)-1 do begin
    cmd='grep '+saveimg[n1]+' '+getenv('sched')+'/'+dir+'SEC20'+strmid(name,0,6)+'00'+strmid(name,7,1)+'.summary'
    spawn,cmd,ipgrep
    if ipgrep[0] ne '' then begin
          t1=anytim2tai(btime[n])+strmid(ipgrep,18,2)*60*60+strmid(ipgrep,21,2)*60+strmid(ipgrep,24,2)-offset
          z=where(iptimes[n1,*] eq '',cnt)    
          iptimes[n1,z[0]:z[0]+n_Elements(ipgrep)-1]=utc2str(tai2utc(t1))
    endif
   endfor
  endif
endfor  

scu=sc
d1u=d1
noupdate:
end  

pro sidelobe_schedule_rpt,sc,d1,d2,outdir=outdir

common sidelobe_sched_times

matchip=['IP29','IP55','IP56','IP66','IP79','IP82']
downip=['IP28','IP63','IP67','IP68','IP78','IP77']
downobsnum=[2176,2180,2182,2178,2157,2166];sidelobe 1 obids need to change for sidelobe 2.
tels=['EUVI_171','EUVI_284','EUVI_304','EUVI_195','HI1','HI2']

get_sidelobe_sched_times,sc,d1,d2

if strlowcase(sc) eq 'a' then lighttravel=0 else lighttravel=0 

ut0=nrlanytim2utc(d1)
ut1=nrlanytim2utc(d2)
yyyymmdd1=utc2yymmdd(ut0,/yyyy)
yyyymmdd2=utc2yymmdd(ut1,/yyyy)

if keyword_set(outdir) then outdir=outdir else outdir=getenv('sched')+'/'+strupcase(sc)+'/dailyats/reports/'

openw,1,outdir+'ST'+strupcase(sc)+'_'+yyyymmdd1+'_'+yyyymmdd2+'.rpt',width=100

for n=0,n_Elements(tels)-1 do begin
if strlowcase(sc) eq 'a' then lighttravel=0 else lighttravel=0 
printf,1,tels[n]
printf,1,'CNT   Start of exposure         Start of downlink'
  z=where(downtimes[n,*] gt d1,cnt)
  itimes=reform(iptimes[n,*])
  itimes=anytim2tai(itimes[where(itimes ne '')])
  if cnt gt 0 then begin
     for n2=0,cnt-1 do begin
        date_end=anytim2tai(downtimes[n,z[n2]])
        z1=where(itimes lt anytim2tai(date_end))
        if z1[0] ne -1 then begin
          itime=itimes[z1[n_elements(z1)-1]]
          date_obs=utc2str(tai2utc(itime+lighttravel))
        endif else date_obs='?????'
	pstr=string(n2,'(i3)')+'  '+date_obs+'  '+downtimes[n,z[n2]]
        printf,1,pstr
     endfor
  endif  
endfor
close,1

end
