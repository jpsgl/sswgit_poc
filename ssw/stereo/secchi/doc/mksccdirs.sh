#!/bin/csh
#cd $secchi/lz
mkdir -p L0/a/cal
cd L0/a/cal
ln -s /net/venus/secchi2/preflight/rt/cor1/L0/a/cal cor1
ln -s /net/venus/secchi2/preflight/rt/cor2/L0/a/cal cor2
ln -s /net/venus/secchi1/preflight/rt/euvi/L0/a/cal euvi
ln -s /net/venus/secchi2/preflight/rt/hi_1/L0/a/cal hi_1
ln -s /net/venus/secchi2/preflight/rt/hi_2/L0/a/cal hi_2
mkdir ../img
cd ../img
ln -s /net/venus/secchi2/preflight/rt/cor1/L0/a/img cor1
ln -s /net/venus/secchi2/preflight/rt/cor2/L0/a/img cor2
ln -s /net/venus/secchi1/preflight/rt/euvi/L0/a/img euvi
ln -s /net/venus/secchi2/preflight/rt/hi_1/L0/a/img hi_1
ln -s /net/venus/secchi2/preflight/rt/hi_2/L0/a/img hi_2
mkdir ../seq
cd ../seq
ln -s /net/venus/secchi2/preflight/rt/cor1/L0/a/seq cor1
ln -s /net/venus/secchi2/preflight/rt/cor2/L0/a/seq cor2
ln -s /net/venus/secchi1/preflight/rt/euvi/L0/a/seq euvi
ln -s /net/venus/secchi2/preflight/rt/hi_1/L0/a/seq hi_1
ln -s /net/venus/secchi2/preflight/rt/hi_2/L0/a/seq hi_2
mkdir ../summary

cd ../..
mkdir -p b/cal
cd b/cal
ln -s /net/venus/secchi2/preflight/rt/cor1/L0/b/cal cor1
ln -s /net/venus/secchi2/preflight/rt/cor2/L0/b/cal cor2
ln -s /net/venus/secchi1/preflight/rt/euvi/L0/b/cal euvi
ln -s /net/venus/secchi2/preflight/rt/hi_1/L0/b/cal hi_1
ln -s /net/venus/secchi2/preflight/rt/hi_2/L0/b/cal hi_2
mkdir ../img
cd ../img
ln -s /net/venus/secchi2/preflight/rt/cor1/L0/b/img cor1
ln -s /net/venus/secchi2/preflight/rt/cor2/L0/b/img cor2
ln -s /net/venus/secchi1/preflight/rt/euvi/L0/b/img euvi
ln -s /net/venus/secchi2/preflight/rt/hi_1/L0/b/img hi_1
ln -s /net/venus/secchi2/preflight/rt/hi_2/L0/b/img hi_2
mkdir ../seq
cd ../seq
ln -s /net/venus/secchi2/preflight/rt/cor1/L0/b/seq cor1
ln -s /net/venus/secchi2/preflight/rt/cor2/L0/b/seq cor2
ln -s /net/venus/secchi1/preflight/rt/euvi/L0/b/seq euvi
ln -s /net/venus/secchi2/preflight/rt/hi_1/L0/b/seq hi_1
ln -s /net/venus/secchi2/preflight/rt/hi_2/L0/b/seq hi_2
mkdir ../summary
