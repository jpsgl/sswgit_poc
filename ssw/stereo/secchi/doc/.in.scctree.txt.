Accessing the SECCHI Data Archive 

$Id: scctree.txt,v 1.55 2012/12/21 19:13:54 nathan Exp $

			  Section I.  Initial setup.

For NRL users:  Make sure your shell sources /net/cronus/opt/nrl_ssw_setup.
(You do if you see the banner which starts:
+-------------------------------------------------------+
+ Instructions for using the SSW IDL environment at NRL +
+-------------------------------------------------------+
when you log in.)

For users outside of NRL, please see Section III below.

A list of image files can be generated in IDL using scclister.pro.
(Make sure $NRL_LIB/dev or your local dev directory is in $IDL_PATH.)


			Section II.  Archive structure.

The SECCHI data archive is organized by four levels:

1. Method the data is received ("stream")
1.1.	lz - "Level-0" files retrieved via FTP from SSC
1.2.	pb - Files from playback of SSR or VC7 realtime via socket to APL
1.3.	rt - Files from Realtime telemetry (including spaceweather)

2. Level of processing
2.1.	L0 - Level 0.5 FITS (uncalibrated)
2.2.	L1 - Calibrated = "scientific units"

3. Type of image
3.1.	cal - "Calibration" image: DARK, LED, CONTIN, and door CLOSED
3.2.	img - "Brightness" image: EUVI, HI, COR combined onboard
3.3.	seq - polarized images (not combined) from COR1 or COR2

4. Telescope
4.1.	EUVI
4.2.	COR1
4.3.	COR2
4.4.	HI_1
4.5.	HI_2

Directory Structure

- If more than one path is listed for a directory, then the data
  is split between those directories, with newer data in the last
  listed location.
- Path in parenthesis means location is currently unavailable.

$secchi
|-- lz
|   `-- L0
|       |-- a
|       |   |-- cal
|       |   |   |-- cor1 -> /net/neptune/secchi1/lz/cor1/L0/a/cal 
|       |   |   |-- cor2:
|       |   |   |20061027-> /net/ssdfs0/SECCHI/p6/secchi/lz/cor2/L0/a/cal
|       |   |   |20101202-> ./
|       |   |   |-- euvi -> /net/ssdfs0/SECCHI/p8/secchi/lz/euvi/L0/a/cal
|       |   |   |-- hi_1 -> /net/pluto/p2/secchi/lz/hi_1/L0/a/cal
|       |   |   |        -> /net/earth/secchi/lz/hi_1/L0/a/cal
|       |   |   `-- hi_2 -> /net/earth/data4/secchi/hi_2/L0/a/cal
|       |   |-- img
|       |   |   |-- cor1 -> /net/pluto/p5/secchi/lz/cor1/L0/a/img
|       |   |   |20121212-> /net/saturn/s3/secchi/lz/cor1/L0/a/img
|       |   |   |-- cor2: 
|       |   |   |20061204-> /net/ssdfs0/SECCHI/p6/secchi/lz/cor2/L0/a/img
|       |   |   |20100616-> /net/saturn/s3/secchi/lz/cor2/L0/a/img
|       |   |   |20120901-> /net/pluto/p5/secchi/lz/cor2/L0/a/img
|       |   |   |20121212-> /net/saturn/s3/secchi/lz/cor2/L0/a/img
|       |   |   |-- euvi:
|       |   |   |20061107-> /net/ssdfs0/SECCHI/p8/secchi/lz/euvi/L0/a/img 
|       |   |   |20070301-> /net/neptune/secchi3/lz/euvi/L0/a/img
|       |   |   |20080108-> /net/neptune/secchi4/lz/euvi/L0/a/img 
|       |   |   |20080115-> /net/ssdfs0/SECCHI/p8/secchi/lz/euvi/L0/a/img 
|       |   |   |20080201-> /net/neptune/secchi4/lz/euvi/L0/a/img 
|       |   |   |20081202-> /net/ssdfs0/SECCHI/p8/secchi/lz/euvi/L0/a/img 
|       |   |   |20090406-> /net/saturn/s4/secchi/lz/euvi/L0/a/img
|       |   |   |20091205-> /net/ssdfs0/SECCHI/L0/euvi/a/img
|       |   |   |20110211-> /net/pluto/p1/secchi/lz/euvi/L0/a/img
|       |   |   |20110602-> /net/ssdfs0/SECCHI/L0/euvi/a/img
|       |   |   |20110629-> /net/saturn/s4/secchi/lz/euvi/L0/a/img
|       |   |   |20120901-> /net/secchig/data2/secchi/lz/euvi/L0/a/img
|       |   |   |-- hi_1 -> /net/pluto/p2/secchi/lz/hi_1/L0/a/img
|       |   |   |20121212-> /net/earth/secchi/lz/hi_1/L0/a/img
|       |   |   `-- hi_2 -> /net/earth/data4/secchi/hi_2/L0/a/img
|       |   |-- seq
|       |   |   |-- cor1 -> /net/neptune/secchi3/lz/cor1/L0/a/seq
|       |   |   |20090101-> /net/ssdfs0/SECCHI/p8/secchi/lz/cor1/L0/a/seq
|       |   |   |20090426-> /net/neptune/secchi2/lz/cor1/L0/a/seq
|	|   |   |20100204-> /net/saturn/s3/secchi/lz/cor1/L0/a/seq
|	|   |   |20120901-> /net/pluto/p2/secchi/lz/cor1/L0/a/seq
|	|   |   |20121212-> /net/saturn/s3/secchi/lz/cor1/L0/a/seq
|       |   |   |-- cor2 -> /net/earth/earth2/secchi/lz/cor2/L0/a/seq 
|       |   |   |20090107-> /net/ssdfs0/SECCHI/p3/secchi/lz/cor2/L0/a/seq
|       |   |   |20101113-> /net/pluto/p5/secchi/lz/cor2/L0/a/seq
|       |   |   |20121212-> /net/saturn/s3/secchi/lz/cor2/L0/a/seq
|       |   |   |-- hi_1 -> /net/pluto/p2/secchi/lz/hi_1/L0/a/seq
|       |   |   |        -> /net/earth/secchi/lz/hi_1/L0/a/seq
|       |   |   `-- hi_2 -> /net/earth/data4/secchi/hi_2/L0/a/seq
|       |   `-- summary
|       `-- b
|           |-- cal
|           |   |-- cor1:
|           |   |20061027-> /net/ssdfs0/SECCHI/p7/secchi/lz/cor2/L0/b/cal
|           |   |20101215-> ./
|           |   |-- cor2:
|           |   |20061107-> /net/ssdfs0/SECCHI/p7/secchi/lz/cor2/L0/b/cal
|           |   |20101215-> ./
|           |   |-- euvi -> /net/neptune/secchi2/lz/euvi/L0/b/cal
|           |   |-- hi_1 -> /net/pluto/p2/secchi/lz/hi_1/L0/b/cal
|           |   |        -> /net/earth/secchi/lz/hi_1/L0/b/cal
|           |   `-- hi_2 -> /net/earth/data4/secchi/hi_2/L0/b/cal
|           |-- img
|           |   |-- cor1 -> /net/pluto/p5/secchi/lz/cor1/L0/b/img
|           |   |20121213-> /net/saturn/s3/secchi/lz/cor1/L0/b/img
|           |   |-- cor2: 
|           |   |20061229-> /net/ssdfs0/SECCHI/p7/secchi/lz/cor2/L0/b/img
|           |   |20100615-> /net/saturn/s3/secchi/lz/cor2/L0/b/img
|           |   |20120901-> /net/pluto/p5/secchi/lz/cor2/L0/b/img
|           |   |20121213-> /net/saturn/s3/secchi/lz/cor2/L0/b/img
|           |   |-- euvi -> /net/neptune/secchi2/lz/euvi/L0/b/img
|           |   |20090121-> /net/pluto/p9/secchi/lz/euvi/L0/b/img
|           |   |20090616-> /net/neptune/secchi2/lz/euvi/L0/b/img
|           |   |20090618-> /net/pluto/p9/secchi/lz/euvi/L0/b/img
|           |   |20090726-> /net/ssdfs0/SECCHI-1/L0/euvi/b/img
|           |   |20111001-> /net/secchig/data3/secchi/lz/euvi/L0/b/img
|           |   |20120918 ->/net/secchig/data2/secchi/lz/euvi/L0/b/img
|           |   |-- hi_1 -> /net/pluto/p2/secchi/lz/hi_1/L0/b/img
|           |   |20121213-> /net/earth/secchi/lz/hi_1/L0/b/img
|           |   `-- hi_2 -> /net/earth/data4/secchi/hi_2/L0/b/img
|           |-- seq
|           |   |-- cor1: 
|           |   |20061213-> /net/ssdfs0/SECCHI/p6/secchi/lz/cor1/L0/b/seq
|           |   |20100819-> /net/saturn/s3/secchi/lz/cor1/L0/b/seq
|           |   |20120313-> /net/pluto/p2/secchi/lz/cor1/L0/b/seq
|           |   |20121213-> /net/saturn/s3/secchi/lz/cor1/L0/b/seq
|           |   |-- cor2: 
|           |   |20061213-> /net/ssdfs0/SECCHI/p7/secchi/lz/cor2/L0/b/seq
|           |   |20120101-> /net/saturn/s3/secchi/lz/cor2/L0/b/seq
|           |   |20120901-> /net/pluto/p5/secchi/lz/cor2/L0/b/seq
|           |   |20121213-> /net/saturn/s3/secchi/lz/cor2/L0/b/seq
|           |   |-- hi_1 -> /net/pluto/p2/secchi/lz/hi_1/L0/b/seq
|           |   |        -> /net/earth/secchi/lz/hi_1/L0/b/seq
|           |   `-- hi_2 -> /net/earth/data4/secchi/hi_2/L0/b/seq
|           `-- summary
|-- pb
|   `-- L0
|       |-- a
|       |   |-- cal
|       |   |   |-- cor1 -> /net/venus/secchi2/pb/cor1/L0/a/cal
|       |   |   |-- cor2 -> /net/venus/secchi2/pb/cor2/L0/a/cal
|       |   |   |-- euvi -> /net/venus/secchi2/pb/euvi/L0/a/cal
|       |   |   |-- hi_1 -> /net/venus/secchi2/pb/hi_1/L0/a/cal
|       |   |   `-- hi_2 -> /net/venus/secchi2/pb/hi_2/L0/a/cal
|       |   |-- img
|       |   |   |-- cor1 -> /net/venus/secchi2/pb/cor1/L0/a/img
|       |   |   |-- cor2 -> /net/venus/secchi2/pb/cor2/L0/a/img
|       |   |   |-- euvi -> /net/venus/secchi2/pb/euvi/L0/a/img
|       |   |   |-- hi_1 -> /net/venus/secchi2/pb/hi_1/L0/a/img
|       |   |   `-- hi_2 -> /net/venus/secchi2/pb/hi_2/L0/a/img
|       |   |-- seq
|       |   |   |-- cor1 -> /net/venus/secchi2/pb/cor1/L0/a/seq
|       |   |   |-- cor2 -> /net/venus/secchi2/pb/cor2/L0/a/seq
|       |   |   `-- euvi -> /net/venus/secchi2/pb/euvi/L0/a/seq
|       |   `-- summary
|       `-- b
|           |-- cal
|           |   |-- cor1 -> /net/venus/secchi2/pb/cor1/L0/b/cal
|           |   |-- cor2 -> /net/venus/secchi2/pb/cor2/L0/b/cal
|           |   |-- euvi -> /net/venus/secchi2/pb/euvi/L0/b/cal
|           |   |-- hi_1 -> /net/venus/secchi2/pb/hi_1/L0/b/cal
|           |   `-- hi_2 -> /net/venus/secchi2/pb/hi_2/L0/b/cal
|           |-- img
|           |   |-- cor1 -> /net/venus/secchi2/pb/cor1/L0/b/img
|           |   |-- cor2 -> /net/venus/secchi2/pb/cor2/L0/b/img
|           |   |-- euvi -> /net/venus/secchi2/pb/euvi/L0/b/img
|           |   |-- hi_1 -> /net/venus/secchi2/pb/hi_1/L0/b/img
|           |   `-- hi_2 -> /net/venus/secchi2/pb/hi_2/L0/b/img
|           |-- seq
|           |   |-- cor1 -> /net/venus/secchi2/pb/cor1/L0/b/seq
|           |   |-- cor2 -> /net/venus/secchi2/pb/cor2/L0/b/seq
|           |   `-- euvi -> /net/venus/secchi2/pb/euvi/L0/b/seq
|           `-- summary
`-- rt
    `-- L0
        |-- a
        |   |-- cal
        |   |   |-- cor1 -> /net/venus/secchi2/rt/cor1/L0/a/cal
        |   |   |-- cor2 -> /net/venus/secchi2/rt/cor2/L0/a/cal
        |   |   |-- euvi -> /net/venus/secchi2/rt/euvi/L0/a/cal
        |   |   |-- hi_1 -> /net/venus/secchi2/rt/hi_1/L0/a/cal
        |   |   `-- hi_2 -> /net/venus/secchi2/rt/hi_2/L0/a/cal
        |   |-- img
        |   |   |-- cor1 -> /net/venus/secchi2/rt/cor1/L0/a/img
        |   |   |-- cor2 -> /net/venus/secchi2/rt/cor2/L0/a/img
        |   |   |-- euvi -> /net/venus/secchi2/rt/euvi/L0/a/img
        |   |   |-- hi_1 -> /net/venus/secchi2/rt/hi_1/L0/a/img
        |   |   `-- hi_2 -> /net/venus/secchi2/rt/hi_2/L0/a/img
        |   |-- seq
        |   |   |-- cor1 -> /net/venus/secchi2/rt/cor1/L0/a/seq
        |   |   `-- cor2 -> /net/venus/secchi2/rt/cor2/L0/a/seq
        |   `-- summary
        `-- b
            |-- cal
            |   |-- cor1 -> /net/venus/secchi2/rt/cor1/L0/b/cal
            |   |-- cor2 -> /net/venus/secchi2/rt/cor2/L0/b/cal
            |   |-- euvi -> /net/venus/secchi2/rt/euvi/L0/b/cal
            |   |-- hi_1 -> /net/venus/secchi2/rt/hi_1/L0/b/cal
            |   `-- hi_2 -> /net/venus/secchi2/rt/hi_2/L0/b/cal
            |-- img
            |   |-- cor1 -> /net/venus/secchi2/rt/cor1/L0/b/img
            |   |-- cor2 -> /net/venus/secchi2/rt/cor2/L0/b/img
            |   |-- euvi -> /net/venus/secchi2/rt/euvi/L0/b/img
            |   |-- hi_1 -> /net/venus/secchi2/rt/hi_1/L0/b/img
            |   `-- hi_2 -> /net/venus/secchi2/rt/hi_2/L0/b/img
            |-- seq
            |   |-- cor1 -> /net/venus/secchi2/rt/cor1/L0/b/seq
            |   `-- cor2 -> /net/venus/secchi2/rt/cor2/L0/b/seq
            `-- summary


			  Section III.  Outside users

Depending on where you are, you may have a copy of the entire tree, or only
part of the tree.  For example, the STEREO Science Center archives only the lz
part of the tree, in a directory called "secchi".  There are several levels of
environment variables which can be defined:

1.  A complete copy of the SECCHI tree exists at your site.

In this case, the only environment variable which needs to be defined is
$secchi (lowercase), which points to the directory containing the lz, rt, and
pb directories, e.g.

	setenv secchi /net/corona/secchi/flight

In this scenario, the only environment variable which needs to be defined is
$secchi.

2.  Only the lz tree exists at your site.

The environment variable $SECCHI_LZ (uppercase) can be defined to point to the
equivalent of the $secchi/lz directory.  It points to the directory containing
the L0 and L1 directories, e.g.

	setenv SECCHI_LZ /service/stereo2/ins_data/secchi

3.  The rt and pb trees are separate from the lz tree.

The environment variables $SECCHI_RT and $SECCHI_PB point to the equivalents of
the $secchi/rt and $secchi/pb directories respectively.  They are defined in a
similar manner to $SECCHI_LZ.

4. Browse image locations:

/net/uranus/u2/secchi/images/asteroids

/net/uranus/u2/secchi/images/jpg -> /net/saturn/s4/secchi/images/jpg
	/net/saturn/s4/secchi/images/jpg/a/cor/
	/net/saturn/s4/secchi/images/jpg/a/cor1/
	/net/saturn/s4/secchi/images/jpg/a/euvi/
	/net/saturn/s4/secchi/images/jpg/a/hi/
	/net/saturn/s4/secchi/images/jpg/b/cor/
	/net/saturn/s4/secchi/images/jpg/b/cor1/ 
	/net/saturn/s4/secchi/images/jpg/b/euvi/
	/net/saturn/s4/secchi/images/jpg/b/hi/
	/net/saturn/s4/secchi/images/jpg/mpegframes/attic/
	/net/saturn/s4/secchi/images/jpg/sdo/aia/

/net/uranus/u2/secchi/images/png/a -> /net/uranus/u1/secchi/images/png/a
	/net/uranus/u1/secchi/images/png/a/cor/
	/net/uranus/u1/secchi/images/png/a/cor1_rdif/
	/net/uranus/u1/secchi/images/png/a/cor2_rdif/
	/net/uranus/u1/secchi/images/png/a/euvi/
	/net/uranus/u1/secchi/images/png/a/euvi_rdif/
	/net/uranus/u1/secchi/images/png/a/hi/
	/net/uranus/u1/secchi/images/png/a/hi_srem/
	/net/uranus/u1/secchi/images/png/a/hi_stars/

/net/uranus/u2/secchi/images/png/anaglyph/euvi/

/net/uranus/u2/secchi/images/png/b/cor/
/net/uranus/u2/secchi/images/png/b/cor1_rdif/
/net/uranus/u2/secchi/images/png/b/cor2_rdif/
/net/uranus/u2/secchi/images/png/b/euvi/
/net/uranus/u2/secchi/images/png/b/euvi_rdif/
/net/uranus/u2/secchi/images/png/b/hi/
/net/uranus/u2/secchi/images/png/b/hi_srem/
/net/uranus/u2/secchi/images/png/b/hi_stars/

/net/uranus/u2/secchi/images/png/soho/c2/
/net/uranus/u2/secchi/images/png/soho/c2_rdiff/
/net/uranus/u2/secchi/images/png/soho/c3/
/net/uranus/u2/secchi/images/png/soho/c3_rdiff/
/net/uranus/u2/secchi/images/png/soho/lasco/
/net/uranus/u2/secchi/images/png/soho/lasco_rdiff/

/net/uranus/u2/secchi/images/png/synoptic/171/
/net/uranus/u2/secchi/images/png/synoptic/195/
/net/uranus/u2/secchi/images/png/synoptic/304/
