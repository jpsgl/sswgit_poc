//
// $Id: models71to80.cpp,v 1.1 2010/09/01 19:49:05 thernis Exp $
//

#include "models71to80.h"
#include <cmath>


float CModel71::Density(const Cvec &v) {
  float r=v.norm();
  if (r <= 1.05) return 0.;

  // -- see article SolPhy 183: 165-180, 1998
/*  float a=3.3e5;
  float b=4.1e6;
  float c=8.0e7;*/
  
  float nel=3.3e5*pow(r,-2)+4.1e6*pow(r,-4)+8.0e7*pow(r,-6);

  return nel;
}
void CModel71::dumpDefaultParamForIDL(std::vector<moddefparam>& vp,int& flagcase)
{   
    flagcase=0x4;
    vp.push_back(moddefparam("","Leblanc, Dulk, Bougeret model","",""));
    return;
}
