// $Id: mainpagehtml.cpp,v 1.3 2009/02/09 20:51:11 thernis Exp $

/*! \mainpage Solar Corona Raytracing Software Code Documentation
 *
 *
 * \section intro_sec About this documentation
 *
 * This documentation is useful for the user who needs to:
 * - Understand the model implementation by looking directly into the code.
 * - Knowing what are the parameters of a model.
 * - Implement new models.
 */

/*
 * \section link_to_main Raytrace Home Page
 * <A href="../index.html" >Raytrace Home Page</A>
 */
 
/* ! \page page1 A documentation page
  Leading text.
  \section sec An example section
  This page contains the subsections \ref subsection1 and \ref subsection2.
  For more info see page \ref page2.
  \subsection subsection1 The first subsection
  Text.
  \subsection subsection2 The second subsection
  More text.
*/

/* ! \page page2 Another page
  Even more info.
*/


/*
* $Log: mainpagehtml.cpp,v $
* Revision 1.3  2009/02/09 20:51:11  thernis
* - Clean up the code
* - Change CModel::Density prototype
* - Update documentation
* - Implement multi-threading using boost thread
* - Add new models
*
* Revision 1.2  2007/05/14 17:19:41  thernis
* Add CVS id and log in all files
*
*/
