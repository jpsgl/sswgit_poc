//
// $Id: models71to80.h,v 1.1 2010/09/01 19:49:05 thernis Exp $
//
#ifndef MODELS71TO80_H
#define MODELS71TO80_H

#include "CModelBase.h"


//! Leblanc, Dulk, Bougeret electron density model: SolPhy 183: 165-180, 1998
class CModel71 : public CModelBase
{
    public:
      float Density(const Cvec &v); 
      void dumpDefaultParamForIDL(std::vector<moddefparam>& vp,int& flagcase);
};


#endif
