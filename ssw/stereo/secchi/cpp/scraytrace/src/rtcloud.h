/***************************************************************************
 *  $Id: rtcloud.h,v 1.2 2009/02/09 20:51:20 thernis Exp $
 ****************************************************************************/

#ifndef RTCLOUD_H
#define RTCLOUD_H


/** \file rtcloud.h
 * \brief Generate a view of a cloud of points
 */


//! rtcloud wrapper called by IDL
extern "C" int rtcloud(int argc, void **argv);

#endif
