/***************************************************************************
 *            constant.h
 *
 *  $Id: constant.h,v 1.3 2009/02/09 20:51:10 thernis Exp $
 ****************************************************************************/

#ifndef _CONSTANT_H
#define _CONSTANT_H

#define PI 3.14159265359
#define TWOPI (2*PI)
#define DTOR (PI/180.)
#define RADEG (1./DTOR)
//! Rsun, in cm
#define RSUN_CM 696000e5

//! Useful to print variables
#define printvar(X) std::cout << #X << " : " << X << std::endl;
#define printvarnoend(X) std::cout << #X << " : " << X << ", " ;



//! Minimum limit for the radius of integration around the sun
#define LIMBLIMIT 1.001
//! Maximum recursion
#define MAXRECURSION 10
//! Rsun, in Rsun (very useful !)
#define RSUN 1.
//! limb darkening
#define U 0.58
//! Thomson scattering integral constant factor 
#define constfactor(u) (1.24878E-25/(1-u/3)*RSUN_CM)






#endif /* _CONSTANT_H */


/*
* $Log: constant.h,v $
* Revision 1.3  2009/02/09 20:51:10  thernis
* - Clean up the code
* - Change CModel::Density prototype
* - Update documentation
* - Implement multi-threading using boost thread
* - Add new models
*
* Revision 1.2  2007/05/14 17:19:40  thernis
* Add CVS id and log in all files
*
*/
