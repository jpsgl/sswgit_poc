/***************************************************************************
 *   2006 by Arnaud Thernisien, USRA - Naval Reseach Laboratory
 *   $Id: scort.cpp,v 1.1 2009/02/09 20:57:22 thernis Exp $
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[])
{
  cout << "Hello, world!" << endl;

  return EXIT_SUCCESS;
}


/*
* $Log: scort.cpp,v $
* Revision 1.1  2009/02/09 20:57:22  thernis
* Put some old code here in case I need that one day...
*
* Revision 1.2  2007/05/14 17:19:41  thernis
* Add CVS id and log in all files
*
*/
