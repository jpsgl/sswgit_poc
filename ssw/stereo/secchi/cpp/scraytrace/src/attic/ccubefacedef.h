//
// $Id: ccubefacedef.h,v 1.1 2009/02/09 20:57:17 thernis Exp $
//
#ifndef RTCCUBEFACEDEF_H
#define RTCCUBEFACEDEF_H

namespace rt {

/**
Definition of the cube face positions with respect to closest vertice.

@author Arnaud Thernisien
*/
class Ccubefacedef{
public:
    Ccubefacedef();

    ~Ccubefacedef();

    int posfacenormal[8][3]; //! pos of the 3 adjacent face normal vector from the closest vert
    int faceid[8][3]; //! the 3 adjacent face id number
    int backfaceid[8][3]; //! the 3 backside faces
    int coordid[8][3][2]; //! 2 axis id of the 3 adjacent faces
    
};

}

#endif


/*
* $Log: ccubefacedef.h,v $
* Revision 1.1  2009/02/09 20:57:17  thernis
* Put some old code here in case I need that one day...
*
* Revision 1.2  2007/05/14 17:19:40  thernis
* Add CVS id and log in all files
*
*/
