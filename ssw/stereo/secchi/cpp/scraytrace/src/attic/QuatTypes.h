/**** QuatTypes.h - Basic type declarations            ****/
/**** by Ken Shoemake, shoemake@graphics.cis.upenn.edu ****/
/**** in "Graphics Gems IV", Academic Press, 1994      ****/
// $Id: QuatTypes.h,v 1.1 2009/02/09 20:57:16 thernis Exp $
#ifndef _H_QuatTypes
#define _H_QuatTypes

/*** Definitions ***/
//! Quaternion
typedef struct {float x, y, z, w;} Quat; /* Quaternion */
enum QuatPart {X, Y, Z, W};
//! Right-handed, for column vectors
typedef float HMatrix[4][4]; /* Right-handed, for column vectors */
//! (x,y,z)=ang 1,2,3, w=order code
typedef Quat EulerAngles;    /* (x,y,z)=ang 1,2,3, w=order code  */

#endif


/*
* $Log: QuatTypes.h,v $
* Revision 1.1  2009/02/09 20:57:16  thernis
* Put some old code here in case I need that one day...
*
* Revision 1.2  2007/05/14 17:19:40  thernis
* Add CVS id and log in all files
*
*/
