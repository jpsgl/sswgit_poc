/***************************************************************************
 *            losintegadaptstep.h
 * $Id: losintegadaptstep.h,v 1.1 2009/02/09 20:57:19 thernis Exp $ ****************************************************************************/

//float rvar=100.;
// -- compute pos of ortho proj Sun cntr on the LOS
calcqlos(pofinteg,frontinteg,obs,vlosabs,abs,poi,qlos);

// if (pofinteg == 0 ) qlos=orthoproj(obs.o,vlosabs,abs.o); else {
	// get normal vector to POI: defined as the z axis of POI coord sys
// 	Cvec poinormal=poi.ui.column(3);
	// compute intersection LOS - POI (see 25Aug2005 notes)
// 	qlos=obs.o - vlosabs * (pscal(obs.o,poinormal)/pscal(vlosabs,poinormal));
// }
// -- initialize
*posbtot=0;
*posbpol=0;
*posne=0;

// -- current position on the LOS
s=los.sstart;
vlosstep=vlosabs * los.ds;
vs=qlos + vlosabs * s;

// -- loop
bool flagfirstloop=true;
float f0ne=0,f1ne,f2ne;
for(k=0;k<los.nbp;k++,s+=los.ds,vs+=vlosstep) {
	float r0=vs.norm();
	Cvec vs2=vs+vlosstep;
	float r2=vs2.norm();
//	cout << "s : " << s << endl;
//	cout << "r0 : " << r0 << endl;
//	cout << "r2 : " << r2 << endl;
	// -- check if chunk not within or behind the sun
	if (checkchunkwbsun(rho,r0,r2,vs.v[2],vs2.v[2])) continue;
	
	// -- if first loop then compute f0 (set a flag once done)
	if (flagfirstloop) {
		f0ne=pmod->Density(nps.u * (vs + nps.o), pmodparam,temperature);
		flagfirstloop=false;
	}
	
	// -- compute Ne : f1, f2
	f1ne=pmod->Density(nps.u * ((vs+(vlosstep*0.5)) + nps.o), pmodparam,temperature);
	f2ne=pmod->Density(nps.u * (vs2 + nps.o), pmodparam,temperature);

//	cout << endl;
//	cout << "--------- On Top ---------" << endl;
//	cout << "ds : " << los.ds << endl;
//	cout << "s : " << s << endl;
//	cout << "vs : " << vs << endl;
//	cout << "vlosstep : " << vlosstep << endl;
	//cout << "f0ne : " << f0ne << ", f1ne : " << f1ne << ", f2ne : " << f2ne << endl;
//	cout << "ds : " << los.ds << endl;
	// -- call backtrack integrator
	float sumne02=0,stot=0,spol=0;
	int depth=0;
	adaptabacksim(adapthres,vs,f0ne,f1ne,f2ne,los.ds*0.5,vlosstep*0.5,1,sumne02,stot,spol,pmod,pmodparam,temperature,nps,depth,maxsubdiv,rho);

	// -- add density
    *posne +=sumne02;
	*posbtot +=stot;
	*posbpol +=spol;
	
	//*posbpol +=(f1ne * los.ds);
	
	// -- switch last to first point
	f0ne=f2ne;
	
}
// multiply by the integral constant factor
*posbtot *=constfactor;
*posbpol *=constfactor;
*posne *=RSUN_CM;


/*
* $Log: losintegadaptstep.h,v $
* Revision 1.1  2009/02/09 20:57:19  thernis
* Put some old code here in case I need that one day...
*
* Revision 1.3  2008/09/30 18:33:28  thernis
* Replace include of losinteg.h and losintegadaptstep.h by inline functions.
*
* Revision 1.2  2007/05/14 17:19:40  thernis
* Add CVS id and log in all files
*
*/

