// $Id: physicsbase.cpp,v 1.1 2010/09/01 19:56:55 thernis Exp $

#include "physicsbase.h"
#include "physicsthomson.h"
#include "physicsuv.h"
#include "physicsfcor.h"


PhysicsBase* physicsSelect(PhysicsType phytype)
{
  PhysicsBase *pphy;
  PhysicsThomson *ppthom;
  switch (phytype)
  {
    case 0 :
      ppthom=new PhysicsThomson;
      pphy=(PhysicsBase*) ppthom;
      break;
    case 1 :
      PhysicsUV *ppuv;
      ppuv=new PhysicsUV;
      pphy=(PhysicsBase*) ppuv;
      break;
    case 2 :
      PhysicsFCor *ppfc;
      ppfc=new PhysicsFCor;
      pphy=(PhysicsBase*) ppfc;
      break;
    default :
      ppthom=new PhysicsThomson;
      pphy=(PhysicsBase*) ppthom;
  }
  return pphy;
}
