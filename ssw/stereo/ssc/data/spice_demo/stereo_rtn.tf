Dynamic Heliospheric Coordinate Frames developed for the NASA STEREO mission

The coordinate frames in this file all have ID values based on the pattern
18ccple, where

	18 = Prefix to put in the allowed 1400000 to 2000000 range
	cc = 03 for geocentric, 10 for heliocentric
	p  = Pole basis: 1=geographic, 2=geomagnetic, 3=ecliptic, 4=solar
	l  = Longitude basis: 1=Earth-Sun, 2=ecliptic, 4=STEREO-A, 5=STEREO-B
	e  = Ecliptic basis: 0=J2000, 1=mean, 2=true

     Author:  William Thompson
	      NASA Goddard Space Flight Center
	      Code 612.1
	      Greenbelt, MD 20771

	      William.T.Thompson.1@gsfc.nasa.gov


STEREO Ahead - Heliocentric Radial Tangential Normal (STAHGRTN) Frame

     Definition of the STEREO-A HGRTN Frame
 
              All vectors are geometric: no aberration corrections are used.
 
              The position of the spacecraft relative to the sun is the primary
              vector: the X axis points from the sun center to the spacecraft.
 
              The solar rotation axis is the secondary vector: the Z axis is
	      the component of the solar north direction perpendicular to X.
 
              The Y axis is Z cross X, completing the right-handed reference
              frame.

\begindata

        FRAME_STAHGRTN               =  1810440
        FRAME_1810440_NAME           = 'STAHGRTN'
        FRAME_1810440_CLASS          =  5
        FRAME_1810440_CLASS_ID       =  1810440
        FRAME_1810440_CENTER         =  10
        FRAME_1810440_RELATIVE       = 'J2000'
        FRAME_1810440_DEF_STYLE      = 'PARAMETERIZED'
        FRAME_1810440_FAMILY         = 'TWO-VECTOR'
        FRAME_1810440_PRI_AXIS       = 'X'
        FRAME_1810440_PRI_VECTOR_DEF = 'OBSERVER_TARGET_POSITION'
        FRAME_1810440_PRI_OBSERVER   = 'SUN'
        FRAME_1810440_PRI_TARGET     = 'STEREO AHEAD'
        FRAME_1810440_PRI_ABCORR     = 'NONE'
        FRAME_1810440_PRI_FRAME      = 'IAU_SUN'
        FRAME_1810440_SEC_AXIS       = 'Z'
        FRAME_1810440_SEC_VECTOR_DEF = 'CONSTANT'
        FRAME_1810440_SEC_FRAME      = 'IAU_SUN'
        FRAME_1810440_SEC_SPEC       = 'RECTANGULAR'
        FRAME_1810440_SEC_VECTOR      = ( 0, 0, 1 )

\begintext

STEREO Behind - Heliocentric Radial Tangential Normal (STBHGRTN) Frame

     Definition of the STEREO-B HGRTN Frame
 
              All vectors are geometric: no aberration corrections are used.
 
              The position of the spacecraft relative to the sun is the primary
              vector: the X axis points from the sun center to the spacecraft.
 
              The solar rotation axis is the secondary vector: the Z axis is
	      the component of the solar north direction perpendicular to X.
 
              The Y axis is Z cross X, completing the right-handed reference
              frame.

\begindata

        FRAME_STBHGRTN               =  1810450
        FRAME_1810450_NAME           = 'STBHGRTN'
        FRAME_1810450_CLASS          =  5
        FRAME_1810450_CLASS_ID       =  1810450
        FRAME_1810450_CENTER         =  10
        FRAME_1810450_RELATIVE       = 'J2000'
        FRAME_1810450_DEF_STYLE      = 'PARAMETERIZED'
        FRAME_1810450_FAMILY         = 'TWO-VECTOR'
        FRAME_1810450_PRI_AXIS       = 'X'
        FRAME_1810450_PRI_VECTOR_DEF = 'OBSERVER_TARGET_POSITION'
        FRAME_1810450_PRI_OBSERVER   = 'SUN'
        FRAME_1810450_PRI_TARGET     = 'STEREO BEHIND'
        FRAME_1810450_PRI_ABCORR     = 'NONE'
        FRAME_1810450_PRI_FRAME      = 'IAU_SUN'
        FRAME_1810450_SEC_AXIS       = 'Z'
        FRAME_1810450_SEC_VECTOR_DEF = 'CONSTANT'
        FRAME_1810450_SEC_FRAME      = 'IAU_SUN'
        FRAME_1810450_SEC_SPEC       = 'RECTANGULAR'
        FRAME_1810450_SEC_VECTOR      = ( 0, 0, 1 )

\begintext

