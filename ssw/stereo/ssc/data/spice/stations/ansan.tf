\begindata

   FRAME_ANSAN_TOPO			 = 1437126
   FRAME_1437126_NAME                    = 'ANSAN_TOPO'
   FRAME_1437126_CLASS                   = 4
   FRAME_1437126_CLASS_ID                = 1437126
   FRAME_1437126_CENTER                  = 399

   TKFRAME_1437126_RELATIVE          = 'ITRF93'
   TKFRAME_1437126_SPEC              = 'ANGLES'
   TKFRAME_1437126_UNITS             = 'DEGREES'
   TKFRAME_1437126_AXES              = ( 3, 2, 3)
   TKFRAME_1437126_ANGLES            = ( -126.4812, -52.8158, 180.00 )

\begintext

