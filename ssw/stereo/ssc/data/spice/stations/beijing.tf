\begindata

   FRAME_BEIJING_TOPO                     = 1440116
   FRAME_1440116_NAME                    = 'BEIJING_TOPO'
   FRAME_1440116_CLASS                   = 4
   FRAME_1440116_CLASS_ID                = 1440116
   FRAME_1440116_CENTER                  = 399

   TKFRAME_1440116_RELATIVE          = 'ITRF93'
   TKFRAME_1440116_SPEC              = 'ANGLES'
   TKFRAME_1440116_UNITS             = 'DEGREES'
   TKFRAME_1440116_AXES              = ( 3, 2, 3)
   TKFRAME_1440116_ANGLES            = ( -116.05165, -49.7231, 180.00 )

\begintext

