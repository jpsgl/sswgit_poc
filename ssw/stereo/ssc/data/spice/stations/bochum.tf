\begindata

   FRAME_BOCHUM_TOPO                     = 1451007
   FRAME_1451007_NAME                    = 'BOCHUM_TOPO'
   FRAME_1451007_CLASS                   = 4
   FRAME_1451007_CLASS_ID                = 1451007
   FRAME_1451007_CENTER                  = 399

   TKFRAME_1451007_RELATIVE          = 'IAU_EARTH'
   TKFRAME_1451007_SPEC              = 'ANGLES'
   TKFRAME_1451007_UNITS             = 'DEGREES'
   TKFRAME_1451007_AXES              = ( 3, 2, 3)
   TKFRAME_1451007_ANGLES            = ( -7.192, -38.573, 180.00 )

\begintext

