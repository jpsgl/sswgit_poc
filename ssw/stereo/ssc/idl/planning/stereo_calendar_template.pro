pro stereo_calendar_template, year, outfile=outfile
;+
; Project     :	STEREO - SSC
;
; Name        :	STEREO_CALENDAR_TEMPLATE
;
; Purpose     :	Makes a template planning calendar for the year
;
; Category    :	STEREO, Operations, Planning
;
; Explanation :	This procedure writes out a template calendar file, ready to be
;               filled in with the appropriate planning information.  The file
;               has minimal HTML formatting, with most of it in a <PRE></PRE>
;               environment.
;
; Syntax      :	STEREO_CALENDAR_TEMPLATE  [, YEAR  [, OUTFILE=OUTFILE ] ]
;
; Examples    :	STEREO_CALENDAR_TEMPLATE, 2006
;
; Inputs      :	None required.
;
; Opt. Inputs :	YEAR    = The year to generate a calendar file for.  If not
;                         passed, the current year is assumed.
;
; Outputs     :	The template calendar is written to a file.
;
; Opt. Outputs:	None.
;
; Keywords    :	OUTFILE = Contains the name of the file to write the template
;                         calendar to.  If not passed, the calendar is written
;                         to the file calendarYYYY.txt, where YYYY is the
;                         year.  If the extension is not passed, it defaults to
;                         ".txt" for safety.  This needs to be renamed to
;                         ".shtml" to be used.
;
; Calls       :	
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 23-Jan-2006, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
on_error, 2
days = ['M', 'T', 'W', 'T', 'F', 'S', 'S']
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', $
          'Oct', 'Nov', 'Dec']
;
;  Check the input parameter.
;
if n_elements(year) eq 0 then begin
    get_utc, utc, /external
    year = utc.year
endif
;
;  Determine the name of the file to be written, and open it up for write.
;
if datatype(outfile,1) eq 'String' then $
  filename = form_filename(outfile,'.txt') else $
  filename = 'calendar' + ntrim(year) + '.txt'
openw, out, filename, /get_lun
;
;  Open up the prefix file, and copy it into the output file.  Replace any
;  instances of ???? with the year.
;
prefix = find_with_def('calendar_prefix.txt','$SSW_SSC/data/calendar')
openr, in, prefix, /get_lun
while not eof(in) do begin
    line = 'String'
    readf, in, line
    w = strpos(line, '????')
    if w ge 0 then begin
        save = line
        nsave = strlen(save)
        line = strmid(line, 0, w) + ntrim(year)
        if (w + 4) lt nsave then line = line + strmid(save, w+4, nsave-w-4)
    endif
    printf, out, line
endwhile
free_lun, in
;
;  Step through the weeks of the year.  Define week #1 as the week containing
;  January 4th.  Define the week as starting on Monday.  (Both are based on
;  ISO-8601.)
;
week = 1
utc = week2utc(year, week, /external)
repeat begin
    sweek = string(week,format='(I2.2)')
    printf, out, format='(79("-"))'
    for i=0,6 do begin
        line = days[i] + ' ' + months(utc.month-1) + ' ' + $
          string(utc.day, format='(I2.2)')
        if i eq 0 then $
          line = line + ' (W' + sweek + ')  ' else line = line + '        '
        printf, out, line
        utc.day = utc.day + 1
        check_ext_time, utc
    endfor
    printf, out
    printf, out, 'Other activities for Week ' + sweek + ':'
    printf, out, '* TBD'
    printf, out
    printf, out, 'Planners for Week ' + sweek + ':'
    printf, out, 'IMPACT  -- TBD                         PLASTIC  -- TBD'
    printf, out, 'SECCHI  -- TBD                         SWAVES   -- TBD'
    printf, out, 'LASCO   -- TBD                         EIT      -- TBD'
    printf, out, 'TRACE   -- TBD'
    week = week + 1
endrep until utc.year gt year
;
;  Open up the suffix file, and copy it into the output file.  Replace any
;  instances of ???? with the filename.
;
break_file, filename, disk, dir, name, ext
suffix = find_with_def('calendar_suffix.txt','$SSW_SSC/data/calendar')
openr, in, suffix, /get_lun
while not eof(in) do begin
    line = 'String'
    readf, in, line
    w = strpos(line, '????')
    if w ge 0 then begin
        save = line
        nsave = strlen(save)
        line = strmid(line, 0, w) + name
        if (w + 4) lt nsave then line = line + strmid(save, w+4, nsave-w-4)
    endif
    printf, out, line
endwhile
free_lun, in
;
free_lun, out
end
