;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_SECCHI_HTML
;
; Purpose     :	Create SECCHI beacon web page
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	Called from SSC_BEACON_SECCHI to generate a web page showing
;               the latest SECCHI beacon images.
;
; Syntax      :	SSC_BEACON_SECCHI_HTML
;
; Examples    :	See SSC_BEACON_SECCHI
;
; Inputs      :	None
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	BROWSE_TOP         = The name of the top browse directory for
;                                    the URL.  The default is "/browse'.  This
;                                    keyword is mainly used for testing
;                                    purposes.
;
; Env. Vars.  :	STEREO_BROWSE      = Top directory of browse pages
;
;               STEREO_BEACON_HTML = Location of beacon web pages
;
; Calls       :	GET_UTC, CONCAT_DIR, BREAK_FILE, CHECK_EXT_TIME
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1,  8-Feb-2007, William Thompson, GSFC
;               Version 2, 21-Mar-2007, William Thompson, GSFC
;                       Only process 304 and 195 (later only 195)
;               Version 3, 09-Jul-2007, WTT, include COR1 images
;               Version 4, 01-Aug-2007, WTT, include COR2 images
;               Version 5, 06-Sep-2007, WTT, include HI1 images
;               Version 6, 26-Oct-2007, WTT, make static links in "latest"
;               Version 7, 23-Mar-2009, WTT, link latest for 128,256,512 res
;               Version 8, 08-Apr-2009, WTT, Fix bug in version 7
;               Version 9, 10-Apr-2009, WTT, include SOHO/Mk4
;               Version 10, 24-Apr-2009, WTT, Fix bug reversing HI links
;               Version 11, 10-Jul-2009, WTT, include MPEG files
;               Version 12, 21-Aug-2009, WTT, include HI2 images
;               Version 13, 08-Mar-2010, WTT, link to latest directory images
;               Version 14, 24-Jun-2010, WTT, Include SDO images
;               Version 15, 06-Jan-2011, WTT, point to sdo.gsfc
;               Version 16, 14-Dec-2011, WTT, add links to running differences
;               Version 17, 29-Apr-2014, WTT, use KCOR instead of Mk4
;               Version 18, 20-Aug-2014, WTT, process all wavelengths
;               Version 19, 21-Aug-2014, WTT, bug fixes
;               Version 20, 14-Jul-2015, WTT, reverse order post-conjunction
;               Version 21, 21-Dec-2015, WTT, updated number of days to 5
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_secchi_html, browse_top=browse_top
;
on_error, 2
;
stereo_browse = getenv('STEREO_BROWSE')
beacon_html = getenv('STEREO_BEACON_HTML')
nodatajpg = '/browse/nodata_256.jpg'
if n_elements(browse_top) eq 0 then browse_top='/browse'
;
get_utc, now
if now.mjd ge 57161 then scn = ['Ahead','Behind'] else $
   scn = ['Behind','Ahead']
sc = strlowcase(scn)
wavelnth = [195, 171, 284, 304]
nwave = n_elements(wavelnth)
res = [2048,1024,512,256,128]
latest_res = ['512','256','128']
mres = ['512','256']            ;MPEG file resolutions
;
sdo_wave = ['193','171','211','304']
sdo = 't0' + sdo_wave + '.jpg'
;
;  Look for the most recent SECCHI images.  Start with today, and then work
;  backwards.
;
euvi_latest_file = strarr(2,nwave)
euvi_latest_path = strarr(2,nwave)
cor1_latest_file = strarr(2)
cor1_latest_path = strarr(2)
cor2_latest_file = strarr(2)
cor2_latest_path = strarr(2)
hi1_latest_file  = strarr(2)
hi1_latest_path  = strarr(2)
hi2_latest_file  = strarr(2)
hi2_latest_path  = strarr(2)
get_utc, today, /external
utc = today
nrep = 0
repeat begin
    for isc = 0,1 do begin
;
;  Start with EUVI
;
        for iwave = 0,nwave-1 do begin
            if euvi_latest_file[isc,iwave] eq '' then begin
                path = string(utc.year, format='(I4.4)')  + '/' + $
                       string(utc.month, format='(I2.2)') + '/' + $
                       string(utc.day, format='(I2.2)')   + '/' + $
                       sc[isc] + '/euvi/' + $
                       string(wavelnth[iwave], format='(I3.3)')
                fullpath = concat_dir(stereo_browse, path)
                files = file_search(concat_dir(fullpath, '256/*.jpg'), $
                                    count=count)
                if count gt 0 then begin
                    euvi_latest_path[isc,iwave] = concat_dir(browse_top, path)
                    s = reverse(sort(files))
                    temp = files[s[0]]
                    break_file, temp, disk, dir, name, ext
                    temp1 = concat_dir(euvi_latest_path[isc,iwave], $
                                      '256/' + name + ext)
                    euvi_latest_file[isc,iwave] = temp1
;
                    for jres=0,n_elements(latest_res)-1 do begin
                        lres = latest_res[jres]
                        temp = concat_dir(fullpath, lres + '/' + name + ext)
                        latest_name = 'latest_' + lres + '/' + sc[isc] + $
                          '_euvi_' + ntrim(wavelnth[iwave]) + '_latest.jpg'
                        latest_name = concat_dir(beacon_html, latest_name)
                        spawn, 'ln -fs ' + temp + ' ' + latest_name
                    endfor
                endif
            endif
        endfor
;
;  Next, do COR1
;
        if cor1_latest_file[isc] eq '' then begin
            path = string(utc.year, format='(I4.4)')  + '/' + $
              string(utc.month, format='(I2.2)') + '/' + $
              string(utc.day, format='(I2.2)')   + '/' + $
              sc[isc] + '/cor1'
            fullpath = concat_dir(stereo_browse, path)
            files = file_search(concat_dir(fullpath, '256/*.jpg'), count=count)
            if count gt 0 then begin
                cor1_latest_path[isc] = concat_dir(browse_top, path)
                s = reverse(sort(files))
                temp = files[s[0]]
                break_file, temp, disk, dir, name, ext
                temp1 = concat_dir(cor1_latest_path[isc], '256/' + name + ext)
                cor1_latest_file[isc] = temp1
;
                for jres=0,n_elements(latest_res)-1 do begin
                    lres = latest_res[jres]
                    temp = concat_dir(fullpath, lres + '/' + name + ext)
                    latest_name = 'latest_' + lres + '/' + sc[isc] + $
                      '_cor1_latest.jpg'
                    latest_name = concat_dir(beacon_html, latest_name)
                    spawn, 'ln -fs ' + temp + ' ' + latest_name
                endfor
            endif
        endif
;
;  Next, do COR2
;
        if cor2_latest_file[isc] eq '' then begin
            path = string(utc.year, format='(I4.4)')  + '/' + $
              string(utc.month, format='(I2.2)') + '/' + $
              string(utc.day, format='(I2.2)')   + '/' + $
              sc[isc] + '/cor2'
            fullpath = concat_dir(stereo_browse, path)
            files = file_search(concat_dir(fullpath, '256/*.jpg'), count=count)
            if count gt 0 then begin
                cor2_latest_path[isc] = concat_dir(browse_top, path)
                s = reverse(sort(files))
                temp = files[s[0]]
                break_file, temp, disk, dir, name, ext
                temp1 = concat_dir(cor2_latest_path[isc], '256/' + name + ext)
                cor2_latest_file[isc] = temp1
;
                for jres=0,n_elements(latest_res)-1 do begin
                    lres = latest_res[jres]
                    temp = concat_dir(fullpath, lres + '/' + name + ext)
                    latest_name = 'latest_' + lres + '/' + sc[isc] + $
                      '_cor2_latest.jpg'
                    latest_name = concat_dir(beacon_html, latest_name)
                    spawn, 'ln -fs ' + temp + ' ' + latest_name
                endfor
            endif
        endif        
;
;  Next, do HI1
;
        if hi1_latest_file[isc] eq '' then begin
            path = string(utc.year, format='(I4.4)')  + '/' + $
              string(utc.month, format='(I2.2)') + '/' + $
              string(utc.day, format='(I2.2)')   + '/' + $
              sc[isc] + '/hi1'
            fullpath = concat_dir(stereo_browse, path)
            files = file_search(concat_dir(fullpath, '256/*.jpg'), count=count)
            if count gt 0 then begin
                hi1_latest_path[isc] = concat_dir(browse_top, path)
                s = reverse(sort(files))
                temp = files[s[0]]
                break_file, temp, disk, dir, name, ext
                temp1 = concat_dir(hi1_latest_path[isc], '256/' + name + ext)
                hi1_latest_file[isc] = temp1
;
                for jres=0,n_elements(latest_res)-1 do begin
                    lres = latest_res[jres]
                    temp = concat_dir(fullpath, lres + '/' + name + ext)
                    latest_name = 'latest_' + lres + '/' + sc[isc] + $
                      '_hi1_latest.jpg'
                    latest_name = concat_dir(beacon_html, latest_name)
                    spawn, 'ln -fs ' + temp + ' ' + latest_name
                endfor
            endif
        endif
;
;  Next, do HI2
;
        if hi2_latest_file[isc] eq '' then begin
            path = string(utc.year, format='(I4.4)')  + '/' + $
              string(utc.month, format='(I2.2)') + '/' + $
              string(utc.day, format='(I2.2)')   + '/' + $
              sc[isc] + '/hi2'
            fullpath = concat_dir(stereo_browse, path)
            files = file_search(concat_dir(fullpath, '256/*.jpg'), count=count)
            if count gt 0 then begin
                hi2_latest_path[isc] = concat_dir(browse_top, path)
                s = reverse(sort(files))
                temp = files[s[0]]
                break_file, temp, disk, dir, name, ext
                temp1 = concat_dir(hi2_latest_path[isc], '256/' + name + ext)
                hi2_latest_file[isc] = temp1
;
                for jres=0,n_elements(latest_res)-1 do begin
                    lres = latest_res[jres]
                    temp = concat_dir(fullpath, lres + '/' + name + ext)
                    latest_name = 'latest_' + lres + '/' + sc[isc] + $
                      '_hi2_latest.jpg'
                    latest_name = concat_dir(beacon_html, latest_name)
                    spawn, 'ln -fs ' + temp + ' ' + latest_name
                endfor
            endif
        endif
    endfor
;
    utc.day = utc.day - 1
    check_ext_time, utc
    w = where(euvi_latest_file eq '', count1)
    w = where(cor1_latest_file eq '', count2)
    w = where(cor2_latest_file eq '', count3)
    w = where(hi1_latest_file  eq '', count4)
    w = where(hi2_latest_file  eq '', count5)
    count = count1 + count2 + count3 + count4 + count5
    nrep = nrep + 1
endrep until (count eq 0) or (nrep gt 5)
;
;  Link any missing images to "nodata"
;
for isc = 0,1 do begin
    for iwave = 0,nwave-1 do begin
        if euvi_latest_file[isc,iwave] eq '' then begin
            for jres = 0,n_elements(latest_res)-1 do begin
                lres = latest_res[jres]
                temp = concat_dir(stereo_browse, 'nodata_'+lres+'.jpg')
                latest_name = 'latest_' + lres + '/' + sc[isc] + $
                              '_euvi_' + ntrim(wavelnth[iwave]) + '_latest.jpg'
                latest_name = concat_dir(beacon_html, latest_name)
                spawn, 'ln -fs ' + temp + ' ' + latest_name
            endfor
        endif
    endfor
;
    if cor1_latest_file[isc] eq '' then begin
        for jres=0,n_elements(latest_res)-1 do begin
            lres = latest_res[jres]
            temp = concat_dir(stereo_browse, 'nodata_'+lres+'.jpg')
            latest_name = 'latest_' + lres + '/' + sc[isc] + $
                          '_cor1_latest.jpg'
            latest_name = concat_dir(beacon_html, latest_name)
            spawn, 'ln -fs ' + temp + ' ' + latest_name
        endfor
    endif
;
    if cor2_latest_file[isc] eq '' then begin
        for jres=0,n_elements(latest_res)-1 do begin
            lres = latest_res[jres]
            temp = concat_dir(stereo_browse, 'nodata_'+lres+'.jpg')
            latest_name = 'latest_' + lres + '/' + sc[isc] + $
                          '_cor2_latest.jpg'
            latest_name = concat_dir(beacon_html, latest_name)
            spawn, 'ln -fs ' + temp + ' ' + latest_name
        endfor
    endif
;
    if hi1_latest_file[isc] eq '' then begin
        for jres=0,n_elements(latest_res)-1 do begin
            lres = latest_res[jres]
            temp = concat_dir(stereo_browse, 'nodata_'+lres+'.jpg')
            latest_name = 'latest_' + lres + '/' + sc[isc] + $
                          '_hi1_latest.jpg'
            latest_name = concat_dir(beacon_html, latest_name)
            spawn, 'ln -fs ' + temp + ' ' + latest_name
        endfor
    endif
;
    if hi2_latest_file[isc] eq '' then begin
        for jres=0,n_elements(latest_res)-1 do begin
            lres = latest_res[jres]
            temp = concat_dir(stereo_browse, 'nodata_'+lres+'.jpg')
            latest_name = 'latest_' + lres + '/' + sc[isc] + $
                          '_hi2_latest.jpg'
            latest_name = concat_dir(beacon_html, latest_name)
            spawn, 'ln -fs ' + temp + ' ' + latest_name
        endfor
    endif
endfor
;
;  Create the web page initially as a temporary file.
;
openw, output, concat_dir(beacon_html, 'beacon_secchi.temp'), /get_lun
;
;  Start by inserting the header.
;
openr, header, concat_dir(beacon_html, 'secchi_header.txt'), /get_lun
line = 'String'
while not eof(header) do begin
    readf, header, line
    printf, output, line
endwhile
free_lun, header
;
;  Write out the table with the SECCHI images
;
printf, output, '<TABLE CLASS="coords" ' + $
  'ALIGN=CENTER STYLE="text-align:center">'
printf, output, '<TR>'
printf, output, '<TH>STEREO ' + scn[0] + '</TH>'
printf, output, '<TD WIDTH=20></TD>'
;;href = "http://sdowww.lmsal.com/suntoday"
href = "http://sdo.gsfc.nasa.gov/data/"
printf, output, '<TH><A HREF="' + href + '" TARGET="_blank">' + 'SDO</A>'
href = "http://sohowww.nascom.nasa.gov/data/realtime-images.html"
printf, output, ' / <A HREF="' + href + '" TARGET="_blank">' + 'SOHO</A>'
href = "http://www2.hao.ucar.edu/mlso/mlso-home-page"
printf, output, ' / <A HREF="' + href + '" TARGET="_blank">' + 'MLSO</A></TH>'
printf, output, '<TD WIDTH=20></TD>'
printf, output, '<TH>STEREO ' + scn[1] + '</TH>'
printf, output, '</TR>'
;
;  Start with the EUVI images.
;
for iwave = 0,nwave-1 do begin
    printf, output, '<TR>'
    for isc = 0,1 do begin
        printf, output, '<TD STYLE="text-align:center">'
        filename = '/beacon/latest_256/' + sc[isc] + '_euvi_' + $
          ntrim(wavelnth[iwave]) + '_latest.jpg'
        printf, output, '<IMG SRC="' + filename + '" ALT="' + filename + '">'
        printf, output, '</TD>'
;
;  If the SOHO equivalent exists, then include a link.
;
        if isc eq 0 then begin
            printf, output, '<TD></TD>'
            sohofile = 'soho_eit_'+ntrim(wavelnth[iwave])+'.jpg'
            sohotext = 'SOHO/EIT '+ntrim(wavelnth[iwave])+' A'
            if not file_exist(concat_dir(beacon_html, sohofile)) then begin
                sohofile = 'soho_eit_'+ntrim(wavelnth[iwave])+'.gif'
                if not file_exist(concat_dir(beacon_html, sohofile)) then begin
                    sohofile = nodatajpg
                    sohotext = ''
                endif
            endif
;
;  If the SDO equivalent exists, then use it instead.
;
            sdofile = concat_dir(beacon_html, sdo[iwave])
            if file_exist(sdofile) then begin
                sohofile = sdo[iwave]
                sohotext = 'SDO/AIA ' + sdo_wave[iwave] + ' A'
            endif
;
            printf, output, '<TD STYLE="text-align:center">'
            printf, output, '<IMG SRC="' + sohofile + $
              '" WIDTH=256 HEIGHT=256 ALT="' + sohofile + '">'
            printf, output, '</TD>'
            printf, output, '<TD></TD>'
        endif
    endfor
    printf, output, '</TR>'
;
;  Write out the links to the JPEG images, if found.
;
    printf, output, '<TR>'
    for isc = 0,1 do begin
        printf, output, '<TD STYLE="text-align:center">'
        entry = ''
        if euvi_latest_path[isc,iwave] ne '' then $
          for ires = 0,n_elements(res)-1 do begin
            sres = ntrim(res[ires])
            path = concat_dir(euvi_latest_path[isc,iwave], sres)
            entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
            if ires lt (n_elements(res)-1) then entry = entry + ','
        endfor
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 0 then printf, output, $
          '<TD></TD><TD STYLE="text-align:center">' + sohotext + '</TD><TD></TD>'
    endfor
    printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
    printf, output, '<TR>'
    caldate = anytim2cal(today, form=8, /date)
    for isc = 0,1 do begin
        break_file, euvi_latest_file[isc,iwave], disk, dir, name
        date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + $
          strmid(name,6,2)
        mpegpath = concat_dir(browse_top, date)
        filepath = concat_dir(stereo_browse, date)
;
        printf, output, '<TD STYLE="text-align:center">'
        mpeg_file = sc[isc] + '_' + caldate + '_euvi_' + $
                    ntrim(wavelnth[iwave]) + '_' + mres + '.mpg'
        entry = ''
        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
            entry = 'MPEG: '
            for ires = 0,n_elements(mres)-1 do begin
                entry = entry + '<A HREF=' + $
                        concat_dir(mpegpath,mpeg_file[ires]) + '>' + $
                        mres[ires] + '</A>'
                if ires lt (n_elements(mres)-1) then entry = entry + ','
            endfor
        endif
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 0 then printf, output, '<TD></TD><TD></TD><TD></TD>'
    endfor
;
    printf, output, '</TR><TR><TD></TD></TR>'
endfor
;
;  Next, do COR1
;
printf, output, '<TR>'
for isc = 0,1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    filename = '/beacon/latest_256/' + sc[isc] + '_cor1_latest.jpg'
    printf, output, '<IMG SRC="' + filename + '" ALT="' + filename + '">'
    printf, output, '</TD>'
;
;  If the Kcor equivalent exists, then include a link.
;
    if isc eq 0 then begin
        printf, output, '<TD></TD>'
        kcorfile = 'kcor.jpg'
        if not file_exist(concat_dir(beacon_html, kcorfile)) $
          then begin
            kcorfile = 'latest.kcor.gif'
            if not file_exist(concat_dir(beacon_html, kcorfile)) $
              then kcorfile = nodatajpg
        endif
        printf, output, '<TD STYLE="text-align:center">'
        printf, output, '<IMG SRC="' + kcorfile + $
          '" WIDTH=256 HEIGHT=256 ALT="' + kcorfile + '">'
        printf, output, '</TD>'
        printf, output, '<TD></TD>'
    endif
endfor
printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
printf, output, '<TR>'
for isc = 0,1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    entry = ''
    if cor1_latest_path[isc] ne '' then $
      for ires = 1,n_elements(res)-1 do begin
        sres = ntrim(res[ires])
        path = concat_dir(cor1_latest_path[isc], sres)
        entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
        if ires lt (n_elements(res)-1) then entry = entry + ','
    endfor
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 0 then printf, output, $
      '<TD></TD><TD STYLE="text-align:center">MLSO</TD><TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
printf, output, '<TR>'
caldate = anytim2cal(today, form=8, /date)
for isc = 0,1 do begin
    break_file, cor1_latest_file[isc], disk, dir, name
    date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2)
    mpegpath = concat_dir(browse_top, date)
    filepath = concat_dir(stereo_browse, date)
;
    printf, output, '<TD STYLE="text-align:center">'
    mpeg_file = sc[isc] + '_' + caldate + '_cor1_' + mres + '.mpg'
    entry = ''
    if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
        entry = 'MPEG: '
        for ires = 0,n_elements(mres)-1 do begin
            entry = entry + '<A HREF=' + $
                    concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                    '</A>'
            if ires lt (n_elements(mres)-1) then entry = entry + ','
        endfor
    endif
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 0 then printf, output, '<TD></TD><TD></TD><TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the running difference images, if found.
;
cor1_rdiff_latest_path = cor1_latest_path + '_rdiff'
break_file, cor1_latest_file, disk, dir, name
path = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2) + $
  '/' + sc + '/cor1_rdiff'
full_rdiff_path = concat_dir(stereo_browse, path)
if total(file_exist(full_rdiff_path)) gt 0 then begin
    printf, output, '<TR>'
    for isc = 0,1 do begin
        printf, output, '<TD STYLE="text-align:center">'
        entry = 'Diff: '
        if file_exist(full_rdiff_path[isc]) then $
          for ires = 1,n_elements(res)-1 do begin
            sres = ntrim(res[ires])
            path = concat_dir(cor1_rdiff_latest_path[isc], sres)
            entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
            if ires lt (n_elements(res)-1) then entry = entry + ','
        endfor
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 0 then printf, output, $
          '<TD></TD><TD STYLE="text-align:center">MLSO</TD><TD></TD>'
    endfor
    printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
    printf, output, '<TR>'
    caldate = anytim2cal(today, form=8, /date)
    for isc = 0,1 do begin
        break_file, cor1_latest_file[isc], disk, dir, name
        date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + $
          strmid(name,6,2)
        mpegpath = concat_dir(browse_top, date)
        filepath = concat_dir(stereo_browse, date)
;
        printf, output, '<TD STYLE="text-align:center">'
        mpeg_file = sc[isc] + '_' + caldate + '_cor1_rdiff_' + mres + '.mpg'
        entry = ''
        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
            entry = 'Diff MPEG: '
            for ires = 0,n_elements(mres)-1 do begin
                entry = entry + '<A HREF=' + $
                  concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                  '</A>'
                if ires lt (n_elements(mres)-1) then entry = entry + ','
            endfor
        endif
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 0 then printf, output, '<TD></TD><TD></TD><TD></TD>'
    endfor
    printf, output, '</TR>'
endif                           ;Running difference images found
;
printf, output, '<TR><TD></TD></TR>'
;
;  Next, do COR2
;
printf, output, '<TR>'
for isc = 0,1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    filename = '/beacon/latest_256/' + sc[isc] + '_cor2_latest.jpg'
    printf, output, '<IMG SRC="' + filename + '" ALT="' + filename + '">'
    printf, output, '</TD>'
;
;  If the SOHO equivalent exists, then include a link.
;
    if isc eq 0 then begin
        printf, output, '<TD></TD>'
        sohofile = 'soho_c2.jpg'
        if not file_exist(concat_dir(beacon_html, sohofile)) then begin
            sohofile = 'soho_c2.gif'
            if not file_exist(concat_dir(beacon_html, sohofile)) $
              then sohofile = nodatajpg
        endif
        printf, output, '<TD STYLE="text-align:center">'
        printf, output, '<IMG SRC="' + sohofile + $
          '" WIDTH=256 HEIGHT=256 ALT="' + sohofile + '">'
        printf, output, '</TD>'
        printf, output, '<TD></TD>'
    endif
endfor
printf, output, '</TR>'
;
;  Write out the links to the JPEG images.
;
printf, output, '<TR>'
for isc = 0,1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    entry = ''
    if cor2_latest_path[isc] ne '' then $
      for ires = 0,n_elements(res)-1 do begin
        sres = ntrim(res[ires])
        path = concat_dir(cor2_latest_path[isc], sres)
        entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
        if ires lt (n_elements(res)-1) then entry = entry + ','
    endfor
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 0 then printf, output, $
      '<TD></TD><TD STYLE="text-align:center">SOHO/LASCO/C2</TD><TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
printf, output, '<TR>'
caldate = anytim2cal(today, form=8, /date)
for isc = 0,1 do begin
    break_file, cor2_latest_file[isc], disk, dir, name
    date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2)
    mpegpath = concat_dir(browse_top, date)
    filepath = concat_dir(stereo_browse, date)
;
    printf, output, '<TD STYLE="text-align:center">'
    mpeg_file = sc[isc] + '_' + caldate + '_cor2_' + mres + '.mpg'
    entry = ''
    if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
        entry = 'MPEG: '
        for ires = 0,n_elements(mres)-1 do begin
            entry = entry + '<A HREF=' + $
                    concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                    '</A>'
            if ires lt (n_elements(mres)-1) then entry = entry + ','
        endfor
    endif
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 0 then printf, output, '<TD></TD><TD></TD><TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the running difference images, if found.
;
cor2_rdiff_latest_path = cor2_latest_path + '_rdiff'
break_file, cor2_latest_file, disk, dir, name
path = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2) + $
  '/' + sc + '/cor2_rdiff'
full_rdiff_path = concat_dir(stereo_browse, path)
if total(file_exist(full_rdiff_path)) gt 0 then begin
    printf, output, '<TR>'
    for isc = 0,1 do begin
        printf, output, '<TD STYLE="text-align:center">'
        entry = 'Diff: '
        if file_exist(full_rdiff_path[isc]) then $
          for ires = 0,n_elements(res)-1 do begin
            sres = ntrim(res[ires])
            path = concat_dir(cor2_rdiff_latest_path[isc], sres)
            entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
            if ires lt (n_elements(res)-1) then entry = entry + ','
        endfor
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 0 then printf, output, '<TD></TD><TD></TD><TD></TD>'
    endfor
    printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
    printf, output, '<TR>'
    caldate = anytim2cal(today, form=8, /date)
    for isc = 0,1 do begin
        break_file, cor2_latest_file[isc], disk, dir, name
        date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + $
          strmid(name,6,2)
        mpegpath = concat_dir(browse_top, date)
        filepath = concat_dir(stereo_browse, date)
;
        printf, output, '<TD STYLE="text-align:center">'
        mpeg_file = sc[isc] + '_' + caldate + '_cor2_rdiff_' + mres + '.mpg'
        entry = ''
        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
            entry = 'Diff MPEG: '
            for ires = 0,n_elements(mres)-1 do begin
                entry = entry + '<A HREF=' + $
                  concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                  '</A>'
                if ires lt (n_elements(mres)-1) then entry = entry + ','
            endfor
        endif
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 0 then printf, output, '<TD></TD><TD></TD><TD></TD>'
    endfor
    printf, output, '</TR>'
endif
;
printf, output, '<TR><TD></TD></TR>'
printf, output, '</TABLE><P>'
;
;  Write out the table with the HI images
;
if now.mjd ge 57161 then hi_exp_file = 'hi_exp_post.txt' else $
  hi_exp_file = 'hi_exp.txt'
openr, hi_exp, concat_dir(stereo_browse,hi_exp_file), /get_lun
line = 'String'
while not eof(hi_exp) do begin
    readf, hi_exp, line
    printf, output, line
endwhile
free_lun, hi_exp
;
printf, output, '<TABLE CLASS="coords" ALIGN=CENTER STYLE="text-align:center">'
printf, output, '<TR>'
printf, output, '<TH>STEREO ' + scn[1] + '</TH>'
printf, output, '<TD WIDTH=20></TD>'
printf, output, '<TH>STEREO ' + scn[0] + '</TH>'
printf, output, '</TR>'
;
;  Next, do HI1
;
printf, output, '<TR>'
for isc = 1,0,-1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    filename = '/beacon/latest_256/' + sc[isc] + '_hi1_latest.jpg'
    printf, output, '<IMG SRC="' + filename + '" ALT="' + filename + '">'
    printf, output, '</TD>'
    if isc eq 1 then printf, output, '<TD></TD>'
endfor
printf, output, '</TR>'
printf, output, '<TR>'
for isc = 1,0,-1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    entry = ''
    if hi1_latest_path[isc] ne '' then $
      for ires = 1,n_elements(res)-1 do begin
        sres = ntrim(res[ires])
        path = concat_dir(hi1_latest_path[isc], sres)
        entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
        if ires lt (n_elements(res)-1) then entry = entry + ','
    endfor
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 1 then printf, output, '<TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
printf, output, '<TR>'
caldate = anytim2cal(today, form=8, /date)
for isc = 1,0,-1 do begin
    break_file, hi1_latest_file[isc], disk, dir, name
    date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2)
    mpegpath = concat_dir(browse_top, date)
    filepath = concat_dir(stereo_browse, date)
;
    printf, output, '<TD STYLE="text-align:center">'
    mpeg_file = sc[isc] + '_' + caldate + '_hi1_' + mres + '.mpg'
    entry = ''
    if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
        entry = 'MPEG: '
        for ires = 0,n_elements(mres)-1 do begin
            entry = entry + '<A HREF=' + $
                    concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                    '</A>'
            if ires lt (n_elements(mres)-1) then $
              entry = entry + ','
        endfor
    endif
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 1 then printf, output, '<TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the running difference images if found.
;
hi1_rdiff_latest_path = hi1_latest_path + '_rdiff'
break_file, cor2_latest_file, disk, dir, name
path = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2) + $
  '/' + sc + '/hi1_rdiff'
full_rdiff_path = concat_dir(stereo_browse, path)
if total(file_exist(full_rdiff_path)) gt 0 then begin
    printf, output, '<TR>'
    for isc = 1,0,-1 do begin
        printf, output, '<TD STYLE="text-align:center">'
        entry = 'Diff: '
        if file_exist(full_rdiff_path[isc]) then $
          for ires = 1,n_elements(res)-1 do begin
            sres = ntrim(res[ires])
            path = concat_dir(hi1_rdiff_latest_path[isc], sres)
            entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
            if ires lt (n_elements(res)-1) then entry = entry + ','
        endfor
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 1 then printf, output, '<TD></TD>'
    endfor
    printf, output, '</TR>'
;
;  Write out the links to the running difference MPEG movies, if found.
;
    printf, output, '<TR>'
    caldate = anytim2cal(today, form=8, /date)
    for isc = 1,0,-1 do begin
        break_file, hi1_latest_file[isc], disk, dir, name
        date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + $
          strmid(name,6,2)
        mpegpath = concat_dir(browse_top, date)
        filepath = concat_dir(stereo_browse, date)
;
        printf, output, '<TD STYLE="text-align:center">'
        mpeg_file = sc[isc] + '_' + caldate + '_hi1_rdiff_' + mres + '.mpg'
        entry = ''
        if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
            entry = 'Diff MPEG: '
            for ires = 0,n_elements(mres)-1 do begin
                entry = entry + '<A HREF=' + $
                  concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                  '</A>'
                if ires lt (n_elements(mres)-1) then $
                  entry = entry + ','
            endfor
        endif
        printf, output, entry
        printf, output, '</TD>'
        if isc eq 1 then printf, output, '<TD></TD>'
    endfor
    printf, output, '</TR>'
endif
;
printf, output, '<TR><TD></TD></TR>'
;
;  Next, do HI2
;
printf, output, '<TR>'
for isc = 1,0,-1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    filename = '/beacon/latest_256/' + sc[isc] + '_hi2_latest.jpg'
    printf, output, '<IMG SRC="' + filename + '" ALT="' + filename + '">'
    printf, output, '</TD>'
    if isc eq 1 then printf, output, '<TD></TD>'
endfor
printf, output, '</TR>'
printf, output, '<TR>'
for isc = 1,0,-1 do begin
    printf, output, '<TD STYLE="text-align:center">'
    entry = ''
    if hi2_latest_path[isc] ne '' then $
      for ires = 1,n_elements(res)-1 do begin
        sres = ntrim(res[ires])
        path = concat_dir(hi2_latest_path[isc], sres)
        entry = entry + '<A HREF=' + path + '/>' + sres + '</A>'
        if ires lt (n_elements(res)-1) then entry = entry + ','
    endfor
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 1 then printf, output, '<TD></TD>'
endfor
printf, output, '</TR>'
;
;  Write out the links to the MPEG movies, if found.
;
printf, output, '<TR>'
caldate = anytim2cal(today, form=8, /date)
for isc = 1,0,-1 do begin
    break_file, hi2_latest_file[isc], disk, dir, name
    date = strmid(name,0,4)  + '/' + strmid(name,4,2) + '/' + strmid(name,6,2)
    mpegpath = concat_dir(browse_top, date)
    filepath = concat_dir(stereo_browse, date)
;
    printf, output, '<TD STYLE="text-align:center">'
    mpeg_file = sc[isc] + '_' + caldate + '_hi2_' + mres + '.mpg'
    entry = ''
    if file_exist(concat_dir(filepath,mpeg_file[0])) then begin
        entry = 'MPEG: '
        for ires = 0,n_elements(mres)-1 do begin
            entry = entry + '<A HREF=' + $
                    concat_dir(mpegpath,mpeg_file[ires]) + '>' + mres[ires] + $
                    '</A>'
            if ires lt (n_elements(mres)-1) then $
              entry = entry + ','
        endfor
    endif
    printf, output, entry
    printf, output, '</TD>'
    if isc eq 1 then printf, output, '<TD></TD>'
endfor
;
printf, output, '</TR><TR><TD></TD></TR>'
;
printf, output, '</TABLE><P>'
;
;  Write out the footer.
;
openr, footer, concat_dir(beacon_html, 'secchi_footer.txt'), /get_lun
line = 'String'
while not eof(footer) do begin
    readf, footer, line
    printf, output, line
endwhile
free_lun, footer, output
;
;  Rename the temporary file to .shtml
;
file_move, concat_dir(beacon_html, 'beacon_secchi.temp'), $
  concat_dir(beacon_html, 'beacon_secchi.shtml'), /overwrite
;
end
