;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_ROTATE_EUVI_SYNOPTIC
;
; Purpose     :	Generate movie of EUVI synoptic data
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation :	This routine takes the output of SSC_EUVI_SYNOPTIC and
;               generates a movie going 360 degrees around the Sun.
;
; Syntax      :	SSC_ROTATE_EUVI_SYNOPTIC, FILENAME, IMAGE, RED, GREEN, BLUE
;
; Examples    :	See SSC_MAKE_EUVI_SYNOPTIC
;
; Inputs      :	FILENAME = Name of output GIF file.
;               IMAGE    = Image from SSC_EUVI_SYNOPTIC
;               RED, GREEN, BLUE = Color table
;
; Opt. Inputs :	None.
;
; Outputs     :	Creates animated GIF movie.
;
; Opt. Outputs:	None.
;
; Keywords    :	NOGRID  = If set, then no grid is overplotted.
;
;               NOREDUCE= If set, then the image is not reduced in size by 2
;
;               NSTEPS  = Number of steps in movie, default is 36.  The angular
;                         step size between each step is 360/NSTEPS.
;
;               FRAMES  = If set, then write out each individual frame to a
;                         separate file, instead of of a GIF movie file
;                         containing all the frames.  When this option is used,
;                         then FILENAME is appended by "_000.gif", etc.
;
;               DELAY_TIME = Amount of delay between frames.  Default is 20.
;
; Calls       :	WCS_CONV_HCC_HG
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 10-Sep-2009, William Thompson, GSFC
;               Version 2, 12-Sep-2009, WTT, overplot lon/lat lines, labels
;               Version 3, 18-Sep-2009, WTT, use INTERPOLATE,MISSING=0
;               Version 4, 05-Jan-2011, Fix scaling bug.  Add keywords NOGRID,
;                       NOREDUCE, NSTEPS, FRAMES, DELAY_TIME
;               Version 5, 06-Jan-2011, WTT, changed fix--really interp. bug
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_rotate_euvi_synoptic, filename, image0, red, green, blue, $
                              nogrid=nogrid, noreduce=noreduce, nsteps=nsteps, $
                              frames=frames, delay_time=delay_time
;
;  Set default values.
;
if n_elements(nsteps) eq 0 then nsteps = 36
if n_elements(delay_time) eq 0 then delay_time = 20
;
;  Reduce the image size by two
;
if ~keyword_set(noreduce) then image = reduce(image0, 2, /average) else $
  image = image0
;
;  Determine the plate scale from the size of the image.
;
sz = size(image)
nx = sz[1]  &  dx = 360.d0 / nx
ny = sz[2]  &  dy = 180.d0 / ny
;
;  Direct graphics output to the Z buffer.
;
dname = !d.name
set_plot, 'z'
device, set_resolution=[nx,ny]
tvlct, red, green, blue
tv,image
;
;  Overplot the longitude and latitude lines.
;
if ~keyword_set(nogrid) then begin
    for i= 0,330,30 do plots, [i,i] / dx, [0,ny], /device, color=255
    for i=30,150,30 do plots, [0,nx], [i,i] / dy, /device, color=255
;
;  Write out the longitude labels.
;
    y = 90 / dy + 1
    xyouts, 1, y, '180', /device, font=0, charsize=0.75
    xyouts, 1 + 90/dx, y, '-90', /device, font=0, charsize=0.75
    xyouts, 1 + 180/dx, y, '0', /device, font=0, charsize=0.75
    xyouts, 1 + 270/dx, y, '90', /device, font=0, charsize=0.75
endif
;
;  Read back in the image.
;
image = tvread(r,g,b)
;
;  Repeat the first column at the end to avoid interpolation problems.
;
temp = temporary(image)
image = bytarr(nx+1,ny)
image[0,0] = temp
image[nx,0] = temp[0,*]
;
;  Calculate the heliographic longitude and latitude arrays.
;
nn = nx < ny
x = (findgen(nn) - (nn-1)/2.) * 2.1 / nn
x = rebin( reform(x,nn,1), nn, nn)
y = transpose(x)
wcs_conv_hcc_hg, x, y, hgln, hglt, length_units='solRad'
;
;  From NSTEPS, determine the amount to rotate the Sun between each step.  The
;  default is 10 degrees.
;
delta = 360. / nsteps
;
;  Step in DELTA degree increments in longitude, and interpolate from the
;  heliographic map to the projected image.
;
for i=0,nsteps do begin
    ii = (hgln+180) / dx
    jj = (hglt+90)  / dy
    im = interpolate(image, ii, jj, /cubic, missing=0)
;
;  Write out the GIF frame.
;
    if keyword_set(frames) then begin
        name = filename + '_' + string(i,format='(I3.3)') + '.gif'
        write_gif, name, im, r, g, b
    end else write_gif, filename, im, r, g, b, /multiple, repeat_count=0, $
      delay_time=delay_time
;
;  Increment the longitude by 10 degrees, and keep it within +/- 180 degrees.
;
    hgln = hgln - delta
    w = where(hgln lt (-180), count)
    if count gt 0 then hgln[w] = hgln[w] + 360
endfor
;
;  Close the GIF file, and return to the previous plotting device.
;
if ~keyword_set(frames) then write_gif, /close
set_plot, dname
;
end
