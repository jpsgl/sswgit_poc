;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_AIA_SOS
;
; Purpose     :	Create heliographic maps for Science-on-a-Sphere
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation : This routine calls SSC_GATHERE_EUVI_SYNOPTIC and
;               SSC_FORM_EUVI_SYNOPTIC to create a series of heliographic maps
;               in JPEG format which can be used to make movies by
;               Science-on-a-Sphere.  The maps are made in both 195 and 304 at
;               a 10 minute cadence.
;
;               This routine differs from the more usual SSC_MAKE_EUVI_SOS in
;               that it creates maps at a constant cadence, whether the STEREO
;               images exist or not.  It is intended for use during the period
;               in and around 2015, when both STEREO spacecraft are in HGA side
;               lobe mode.
;
; Syntax      :	SSC_MAKE_AIA_SOS, DATE
;
; Examples    :	SSC_MAKE_AIA_SOS, '2011-01-20'
;
; Inputs      :	DATE = The date to process
;
; Opt. Inputs :	None.
;
; Outputs     :	Creates files under $STEREO_BROWSE_SPHERE for the specified
;               date and wavelengths.
;
; Opt. Outputs:	None.
;
; Keywords    :	REPLACE = Replace existing JPEG files.
;
;               GRID    = If set, then a grid is overplotted on the image at 60
;                         degree intervals in longitude, and 30 degree
;                         intervals in latitude.
;
;               Keywords are also passed through to SSC_GATHER_EUVI_SYNOPTIC
;               and SSC_FORM_EUVI_SYNOPTIC.
;
;               Also allows VSO keywords to be passed, such as SITE='SAO'
;
; Env. Vars.  : STEREO_BROWSE_SPHERE = Base directory for JPEG files.
;
; Calls       :	
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 1-Aug-2014, William Thompson, GSFC
;               Version 2, 7-Aug-2014, WTT, removed /NOBEACON keywords
;                       All images are marked as beacon during side-lobe ops.
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_aia_sos, date, replace=replace, grid=grid, _extra=_extra
;
;  Form a date range covering the days before and after the requested date.
;
utc = anytim2utc(date)
utc.time = 0
prv = utc  &  prv.mjd = prv.mjd - 1
nxt = utc  &  nxt.mjd = nxt.mjd + 2
;
;  Collect a list of all images from each spacecraft.
;
errmsg = ''
cata = scc_read_summary(date=[prv,nxt], spacecraft='a', telescope='euvi', $
                        errmsg=errmsg)
if errmsg ne '' then ncata = 0 else begin
    w = where((cata.xsize ge 2048) and (cata.nmiss eq 0) and $
              (cata.prog ne 'Dark'), ncata)
    if ncata gt 0 then cata = cata[w]
endelse
;
errmsg = ''
catb = scc_read_summary(date=[prv,nxt], spacecraft='b', telescope='euvi', $
                        errmsg=errmsg)
if errmsg ne '' then ncatb = 0 else begin
    w = where((catb.xsize ge 2048) and (catb.nmiss eq 0) and $
              (catb.prog ne 'Dark'), ncatb)
    if ncatb gt 0 then catb = catb[w]
endelse
;
;  If no images were found, then return
;
if (ncata eq 0) and (ncatb eq 0) then begin
    print, 'No SECCHI images found'
    return
endif
;
;  Use the Z buffer.
;
dname = !d.name
setplot, 'z'
!p.color = !d.table_size - 1
!p.background = 0
device, set_resolution=[3000,1500]
;
;  Form the name of the output directory for the given date.  If it doesn't yet
;  exist, then create it.
;
browse_path = getenv('STEREO_BROWSE_SPHERE')
browse_path = concat_dir(browse_path, utc2str(utc, /date_only, /ecs))
if ~file_exist(browse_path) then mk_dir, browse_path
;
;  Step through the wavelengths.
;
waves = [195, 304]
for iwave=0,n_elements(waves)-1 do begin
    wave = waves[iwave]
    jpeg_path = concat_dir(browse_path, ntrim(wave))
    if ~file_exist(jpeg_path) then mk_dir, jpeg_path
    if ncata eq 0 then counta=0 else wa = where(cata.value eq wave, counta)
    if ncatb eq 0 then countb=0 else wb = where(catb.value eq wave, countb)
    if (counta gt 0) or (countb gt 0) then begin
        if counta gt 0 then lista = cata[wa]
        if countb gt 0 then listb = catb[wb]
;
;  Step through the times in 10 minute intervals, starting from 00:05 UT.
;
        if counta gt 0 then taia = utc2tai(lista.date_obs)
        if countb gt 0 then taib = utc2tai(listb.date_obs)
        utc.time = 300D3
        tai0 = utc2tai(utc)
        for itime = 0,143 do begin
;
;  Form the output filename.
;
            jpeg_name = anytim2cal(utc,form=8,/date) + '_' + $
                        anytim2cal(utc,form=8,/time) + '_' + $
                        ntrim(wave) + '.jpg'
            jpeg_name = concat_dir(jpeg_path, jpeg_name)
;
;  If the JPEG file already exists, and the /REPLACE keyword hasn't been
;  passed, then skip to the next file.
;
            if ~file_exist(jpeg_name) or keyword_set(replace) then begin
;
;  Look for the AIA image.
;
                sdo_wave = wave
                if sdo_wave eq 195 then sdo_wave = 193
                if sdo_wave eq 284 then sdo_wave = 211
                ssc_get_nearest_aia, utc, haia, aia, wavelength=sdo_wave, $
                                     found=found, /nosearch, /only4096, $
                                     _extra=_extra
                if found and (n_elements(aia) gt 0) then begin
;
;  Find the nearest STEREO images.
;
                    if counta eq 0 then filea = '' else begin
                        delta = abs(taia - tai0)
                        w = where(delta eq min(delta))
                        filea = lista[w[0]].filename
                    endelse
                    if countb eq 0 then fileb = '' else begin
                        delta = abs(taib - tai0)
                        w = where(delta eq min(delta))
                        fileb = listb[w[0]].filename
                    endelse
                    help, filea
                    help, fileb
                    ssc_gather_euvi_synoptic, ha, a, hb, b, wavelength=wave, $
                                              filename=[filea,fileb], /color, $
                                              _extra=_extra
;
;  Form and display the synoptic map.
;
                    errmsg = ''
                    ssc_form_euvi_synoptic, map, origin, ha, a, hb, b, $
                                            haia, aia, npixels=[3000,1500], $
                                            /carrington, errmsg=errmsg, $
                                            _extra=_extra
                    tv, map
;
;  Overplot the grid, if requested.
;
                    if keyword_set(grid) then begin
                        xtickv = npixels[0] * (indgen(5) + 1.) / 6
                        for i=0,4 do plots, replicate(xtickv[i],2), $
                                            [0,npixels[1]-1], /device
                        if keyword_set(cea) then begin
                            theta = (-60+30*indgen(5)) / radeg
                            ytickv = (sin(theta) + 1) * (npixels[1]-1) / 2
                        end else ytickv = npixels[1] * (indgen(5) + 1.) / 6
                        for i=0,4 do plots, [0,npixels[0]-1], $
                                            replicate(ytickv[i],2), /device
                    endif
;
;  Write out the JPEG file.
;
                    if errmsg eq '' then begin
                        image = tvread(red, green, blue)
                        true_image = bytarr(3,3000,1500)
                        true_image[0,*,*] = red[image]
                        true_image[1,*,*] = green[image]
                        true_image[2,*,*] = blue[image]
                        write_jpeg, jpeg_name, true_image, true=1, quality=90
                    endif else print, errmsg
                endif
            endif
;
;  Step by 10 minutes for the next file.
;
            if itime ne 143 then begin
                tai0 = tai0 + 600D0
                utc = tai2utc(tai0)
            endif
        endfor
    endif
endfor
;
setplot, dname
;
end
