;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_FORM_EUVI_SYNOPTIC
;
; Purpose     :	Form synoptic images from beacon data.
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation :	This routine forms synoptic maps from SECCHI EUVI images.  The
;               images, which are passed to this routine already read in, are
;               converted to heliographic coordinates, and combined.
;               Matching SDO/AIA data are also used when available.
;
; Syntax      :	SSC_FORM_EUVI_SYNOPTIC, MAP, ORIGIN, HA, A, HB, B [, HAIA, AIA ]
;
; Examples    :	See SC_EUVI_SYNOPTIC
;
; Inputs      :	HA      = Header index for Ahead
;               A       = Ahead image
;               HB      = Header index for Behind
;               B       = Behind image
;
;               Either HA,A or HB,B can be undefined, but not both.
;
; Opt. Inputs :	HAIA    = Header index for AIA
;               AIA     = AIA image
;
; Outputs     :	MAP     = Synoptic map, byte-scaled for display
;
;               ORIGIN  = Coordinates of lower-left corner, for use with
;                         PLOT_IMAGE.
;
; Opt. Outputs:	None.
;
; Keywords    :	SCALE   = Map scale, in degrees.  The default is 1.
;
;               CARRINGTON = If set, then produce a Carrington map from 0-360
;                         degrees.  The default is return a map centered on the
;                         sub-Earth point, i.e. Stonyhurst from -180 to +180.
;
;               CEA     = If set, then the data are displayed in the
;                         Cylindrical Equal Area projection, with the latitude
;                         axis scaled as the sine of the latitude.
;
;               NPIXELS = Two-element array containing the number of pixels in
;                         the longitude and latitude directions.  When this
;                         option is passed, the SCALE keyword is ignored.  Used
;                         for generating images to be used in spherical
;                         displays.
;
;               DATE_OBS= If defined, and not blank, then rotate all images to
;                         this time.  Otherwise, returns the observation date
;                         to the calling routine.
;
;                         Normally, when the Ahead and Behind dates disagree,
;                         this routine will choose whichever date is later, and
;                         rotate the other to match.  This can be overridden by
;                         passing in a non-blank DATE_OBS value.
;
;               FILL_MISSING= If set, then fill in missing pixels.
;
;               BACKGRID= If set, then put a grid behind the image at 30 degree
;                         intervals in both longitude and latitude.
;
;               ERRMSG  = If defined, then return any error messages in this
;                         variable.
;
; Calls       :	FITSHEAD2WCS, UTC2TAI, WCS_2D_SIMULATE, WCS_CONVERT_DIFF_ROT,
;               WCS_CONVERT_TO_COORD, WCS_GET_PIXEL, WHERE_MISSING, SCC_BYTSCL
;
; Common      :	None.
;
; Restrictions:	At least one of an Ahead or Behind image and associated header
;               structure must be passed in.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 24-Jul-2014, WTT, split off from SSC_EUVI_SYNOPTIC
;               Version 2, 11-Aug-2014, WTT, Fix bug with WCS_ROT
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_form_euvi_synoptic, map, origin, ha, a, hb, b, haia, aia, scale=scale, $
                       carrington=carrington, npixels=npixels, errmsg=errmsg, $
                       date_obs=date_obs, cea=cea, fill_missing=k_fill_missing,$
                       backgrid=backgrid, _extra=_extra
;
on_error, 2
;
;  Define some defaults.
;
if n_elements(scale) ne 1 then scale = 1
if n_elements(date_obs) eq 0 then date_obs = ''
;
;  Determine the observation date and wavelength, and whether either the Ahead
;  or Behind image needs to be rotated in time.
;
date_obs_a = ''
date_obs_b = ''
if n_elements(ha) eq 1 then begin
    wcs_a = fitshead2wcs(ha)
    date_obs_a = ha.date_obs
    wavelength = ha.wavelnth
endif
if n_elements(hb) eq 1 then begin
    wcs_b = fitshead2wcs(hb)
    date_obs_b = hb.date_obs
    wavelength = hb.wavelnth
endif
if n_elements(wavelength) eq 0 then begin
    message = 'Neither an Ahead nor Behind image was passed.'
    goto, handle_error
endif
;
diff_rot_a = 0
diff_rot_b = 0
if date_obs eq '' then begin
    if (date_obs_a ne '') and (date_obs_b ne '') then begin
        wcs_rot = wcs_a
;
;  If the Behind date is earlier than the Ahead date, then rotate Behind to
;  time of Ahead.
;
        if date_obs_b lt date_obs_a then begin
            date_obs = date_obs_a
            diff_rot_b = 1
        end else begin
;
;  If the Behind date is more than 5 minutes after the Ahead date, then rotate
;  Ahead to time of Behind
;
            tai_a = utc2tai(date_obs_a)
            tai_b = utc2tai(date_obs_b)
            if (tai_b-tai_a) gt 300 then begin
                date_obs = date_obs_b
                diff_rot_a = 1
                wcs_rot = wcs_b
;
;  If the images are at basically the same time, then use the Ahead time.
;
            end else date_obs = date_obs_a
        endelse
;
;  Otherwise, just use the time from whichever image exists.
;
    end else if date_obs_a ne '' then begin
        date_obs = date_obs_a
        wcs_rot = wcs_a
    end else begin
        date_obs = date_obs_b
        wcs_rot = wcs_b
    endelse
;
;  If an observation date was passed, determine whether the Ahead or Behind
;  image needs to be rotated to that time.
;
end else begin
    tai = utc2tai(date_obs)
    tai_a = utc2tai(date_obs_a)
    tai_b = utc2tai(date_obs_b)
    if abs(tai_a-tai) gt 300 then begin
        diff_rot_a = 1
        wcs_rot = wcs_b
    endif
    if abs(tai_b-tai) gt 300 then begin
        diff_rot_b = 1
        wcs_rot = wcs_a
    endif
    if diff_rot_a eq diff_rot_b then $
      wcs_rot = wcs_2d_simulate(512, date_obs=date_obs)
endelse
;
;  Form the longitude and latitude arrays.  If the NPIXELS keyword was passed,
;  then ignore the SCALE keyword, and don't wrap around.
;
if n_elements(npixels) eq 2 then begin
    nx = npixels[0]
    lon = findgen(nx) * 360 / nx
    ny = npixels[1]
    lat = findgen(ny) * 180 / ny - 90
end else begin
    nx = 1 + 360 / scale
    lon = scale*findgen(nx)
    ny = 1 + 180 / scale
    lat = scale*findgen(ny) - 90
endelse
if not keyword_set(carrington) then lon = lon - 180
radeg = 180.d0 / !dpi
if keyword_set(cea) then lat = asin(lat/90) * radeg
origin = [lon[0], lat[0]]
lon = rebin(reform(lon,nx,1), nx, ny)
lat = rebin(reform(lat,1,ny), nx, ny)
;
;  Convert the images into synoptic maps.
;
if date_obs_a ne '' then begin
    lon_a = lon
    if diff_rot_a then wcs_convert_diff_rot, wcs_rot, wcs_a, lon_a, lat, $
                                             carrington=carrington
    wcs_convert_to_coord, wcs_a, coord_a, 'hg', lon_a, lat, $
                          carrington=carrington
    pixel_a = wcs_get_pixel(wcs_a, coord_a)
    map_a = interpolate(a, reform(pixel_a[0,*,*]), reform(pixel_a[1,*,*]), $
                        missing=!values.f_nan, _extra=_extra)
end else map_a = replicate(!values.f_nan, nx, ny)
if date_obs_b ne '' then begin
    lon_b = lon
    if diff_rot_b then wcs_convert_diff_rot, wcs_rot, wcs_b, lon_b, lat, $
                                             carrington=carrington
    wcs_convert_to_coord, wcs_b, coord_b, 'hg', lon_b, lat, $
                          carrington=carrington
    pixel_b = wcs_get_pixel(wcs_b, coord_b)
    map_b = interpolate(b, reform(pixel_b[0,*,*]), reform(pixel_b[1,*,*]), $
                        missing=!values.f_nan, _extra=_extra)
end else map_b = replicate(!values.f_nan, nx, ny)
;
;  Combine the two maps, filling in from Behind where Ahead is missing.
;
map = map_a
w = where_missing(map_a)
map[w] = map_b[w]
;
;  Calculate the distance (squared) from sun center for Ahead and Behind, and
;  use whichever is closest.
;
if (date_obs_a ne '') and (date_obs_b ne '') then begin
    dist_a = total((coord_a/ha.rsun)^2, 1)
    dist_b = total((coord_b/hb.rsun)^2, 1)
    w = where(dist_b lt dist_a, count)
    if count gt 0 then map[w] = map_b[w]
end else if date_obs_a eq '' then begin
    dist_b = total((coord_b/hb.rsun)^2, 1)
    dist_a = dist_b
end else begin
    dist_a = total((coord_a/ha.rsun)^2, 1)
    dist_b = dist_a
endelse
;
;  If an SDO/AIA image was passed, then add it to the map.
;
if n_elements(haia) ne 0 then begin
    hc = haia
    c  = aia / fxpar(hc, 'exptime')
    case wavelength of
        171: c = c * 1.1
        195: c = c / 1.9
        284: c = c / 3.4
        304: c = c * ssc_get_aia_304_factor(date_obs)
        else:
    endcase
    wcs_c = fitshead2wcs(hc)
    lon_c = lon
    wcs_convert_diff_rot, wcs_rot, wcs_c, lon_c, lat, carrington=carrington
    wcs_convert_to_coord, wcs_c, coord_c, 'hg', lon_c, lat, $
                          carrington=carrington
    pixel_c = wcs_get_pixel(wcs_c, coord_c)
    map_c = interpolate(c, reform(pixel_c[0,*,*]), reform(pixel_c[1,*,*]), $
                        missing=!values.f_nan, _extra=_extra)
    rsun_c = 648000.d0 * wcs_rsun() / (!dpi * wcs_c.position.dsun_obs)
    dist_c = total((coord_c/rsun_c)^2, 1)
    w = where(((dist_c lt dist_a) or (not finite(dist_a))) and $
              ((dist_c lt dist_b) or (not finite(dist_b))) and $
              finite(dist_c))
    map[w] = map_c[w]
endif
;
;  If requested, fill in the missing pixels.
;
if keyword_set(k_fill_missing) then fill_missing, map, 0, 1, /extrapolate
;
;  Byte-scale the map.
;
if date_obs_a ne '' then $
  map = scc_bytscl(map, ha) else $
  map = scc_bytscl(map, hb)
;
;  Underplot the grid, if requested.
;
if keyword_set(backgrid) then begin
    xtickv = nx * indgen(12) / 12
    for i=0,11 do begin
        ii = xtickv[i]
        w = where(map[ii,*] eq 0, count)
        if count gt 0 then map[ii,w] = 128
    endfor
    ytickv = ny * (indgen(5)+1) / 6
    for j=0,4 do begin
        jj = ytickv[j]
        w = where(map[*,jj] eq 0, count)
        if count gt 0 then map[w,jj] = 128
    endfor
endif
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else errmsg=message
;
end
