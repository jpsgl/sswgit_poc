;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GET_NEAREST_AIA
;
; Purpose     :	Retrieve nearest AIA 193 image to specified time.
;
; Category    :	
;
; Explanation :	This routine uses VSO_SEARCH to find the nearest AIA image to a
;               specified time.  By default the routine searches for a 193 A
;               image, but this can be changed with the WAVELENGTH keyword.
;               The file is retrieved via VSO_GET to a temporary directory,
;               read in, and then deleted.
;
; Syntax      :	SSC_GET_NEAREST_AIA, DATE, HEADER, IMAGE, FOUND=FOUND
;
; Examples    :	See ssc_euvi_synoptic.pro
;
; Inputs      :	DATE    = Date/time to look for.
;
; Opt. Inputs :	None.
;
; Outputs     :	HEADER  = FITS header of AIA image.
;               IMAGE   = Image array
;
; Opt. Outputs:	None.
;
; Keywords    :	WAVELENGTH = Wavelength to search for.  Default is 193.  If
;                            passed as 195, then automatically converted to 193.
;
;               FOUND   = Returns 1 if an image was found, 0 otherwise.
;
;               CACHE_AIA = Save the file for possible future use.  Only to be
;                           used when processing realtime data.
;
;               NOSEARCH = Don't search around in time if image is rejected.
;
;               NOSYNOPTIC = Don't read in realtime synoptic files.
;
;               ONLY4096 = Don't read in 1024x1024 images.  Implies NOSYNOPTIC
;
;               Also allows VSO keywords to be passed, such as SITE='SAO', /RICE
;
; Calls       :	VSO_SEARCH, VSO_GET, FXREAD, GET_TEMP_DIR, MK_TEMP_DIR,
;               CONCAT_DIR, DELVARX, WHICH, READ_SDO, DATATYPE
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 21-Jun-2010, William Thompson, GSFC
;               Version 2, 15-Jul-2010, WTT, search for ACS_MODE=SCIENCE
;               Version 3, 11-Aug-2010, WTT, reject if no positive values
;               Version 4, 27-Aug-2010, WTT, reject dark images
;                       Change 3600 seconds to 3500 to avoid hourly patterns
;               Version 5, 30-Aug-2010, WTT, return if no files found
;                       Keep going until file found.  (Took out site='sao')
;               Version 6, 29-Sep-2010, WTT, fixed bug with FOUND parameter
;               Version 7, 30-Sep-2010, WTT, added CACHE_AIA keyword
;               Version 8, 06-Jan-2011, WTT, pass through VSO keywords
;               Version 9, 25-Jan-2011, WTT, check MISSVALS
;               Version 10, 03-Feb-2011, WTT, added keyword NOSEARCH
;               Version 11, 17-Feb-2011, WTT, failover to SSC_GET_NEAREST_AIA_SYNOPTIC
;               Version 12, 18-Mar-2011, WTT, call SSC_GET_NEAREST_AIA, FILES=FILES
;               Version 13, 02-Jun-2011, WTT, sort by reverse of file size
;               Version 14, 09-Apr-2012, WTT, added keywords NOSYNOPTIC, ONLY4096
;                       If VSO_GET fails, then wait 10 seconds, for a max of 60 sec.
;               Version 15, 22-Jun-2012, WTT, add VSO_GET, READ_TIMEOUT=60
;               Version 16, 16-Jul-2015, WTT, allow blank ACS_MODE
;               Version 17, 03-Aug-2016, WTT, add support for /RICE via READ_SDO
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_get_nearest_aia, date, header, image, wavelength=wavelength, $
                         found=found, cache_aia=cache_aia, nosearch=nosearch, $
                         nosynoptic=nosynoptic, only4096=only4096, $
                         _extra=_extra
;
if keyword_set(nosearch) and (n_elements(image) gt 0) then begin
    header_save = temporary(header)
    image_save  = temporary(image)
endif
;
if n_elements(wavelength) eq 0 then wavelength = 193
swave = ntrim(wavelength)
if swave eq '195' then swave = '193'
cache_file = concat_dir(getenv('STEREO_BEACON_HTML'), 'recent_aia.fts')
cache_temp = ''
;
;  Set up a 2-day search window centered on the requested date/time.
;
utc = anytim2utc(date)
utc0 = utc  &  utc0.mjd = utc0.mjd - 1  &  utc0=utc2str(utc0)
utc1 = utc  &  utc1.mjd = utc1.mjd + 1  &  utc1=utc2str(utc1)
utc = utc2str(utc)
;
;  Perform the search.  If the result is not a structure, then return.
;
perform_search:
found = 0
print, 'Calling VSO_SEARCH'
list = vso_search(utc0, utc1, near=utc, inst='aia', wave=swave, _extra=_extra)
if datatype(list) ne 'STC' then goto, try_other_methods
;
;  Sort the list in reverse order of file size.  If the ONLY4096 keyword is
;  set, then make sure the file size is large enough.  (A value of 6000 should
;  be sufficient to discriminate between 1024^2 and 4096^2, even if the latter
;  is compressed.)
;
list = list[reverse(sort(list.size))]
if keyword_set(only4096) and (list[0].size lt 6000) then $
  goto, try_other_methods
;
;  Create a temporary directory, and retrieve the file.
;
mk_temp_dir, get_temp_dir(), temp_dir
cd, temp_dir, current=current
ilist = 0
while (found eq 0) and (ilist lt n_elements(list)) do begin
    maxwait = 60
    nwait = 0
call_vso_get:
    print, 'Calling VSO_GET'
    dummy = vso_get(list[ilist], read_timeout=60, _extra=_extra)
;
;  Look for FITS files in the temporary directory.  If found, then read the
;  file.  If the read is successful, then set FOUND.
;
    files = file_search('*.fits', count=count)
    if count gt 0 then begin
        ifile = 0
        while (found eq 0) and (ifile lt count) do begin
            message = ''
            delvarx, image, header
            fxread, files[ifile], image, header, errmsg=message
;
;  If no image was read in, then try READ_SDO.
;
            if n_elements(image) eq 0 then begin
                which, 'read_sdo', outfile=outfile, /quiet
                if outfile[0] ne '' then begin
                    delvarx, index, image
                    read_sdo, files[ifile], index, image
                    if datatype(index) eq 'STC' then $
                      header = struct2fitshead(index) else $
                        message = 'READ_SDO unable to read file'
                end else message = 'READ_SDO not available'
            endif
;
;  Reading the image was successful.
;
            if message eq '' then begin
                found = 1
;
;  If requested, save the file for possible caching.
;
                if keyword_set(cache_aia) then begin
                    cache_temp = cache_file + '.TEMPORARY'
                    file_copy, files[ifile], cache_temp, /overwrite
                endif
;
            endif else message, message, /continue
            ifile = ifile + 1
        endwhile
;
;  If no files found, then wait 10 seconds and try again.
;
    end else if nwait lt maxwait then begin
        wait, 10
        nwait = nwait + 10
        goto, call_vso_get
    endif
    ilist = ilist + 1
endwhile
;
;  Return to the original directory, and delete the temporary directory and its
;  contents.
;
cd, current
file_delete, temp_dir, /recursive
;
;  If no FITS files were found, then try the AIA synoptic website.
;
try_other_methods:
if not found then begin
    if keyword_set(nosynoptic) or keyword_set(only4096) then found=0 else $
      ssc_get_nearest_aia_synoptic, date, header, image, $
      wavelength=wavelength, found=found, files=files
;
;  If no FITS files were read, then try the cached file, or simply return.
;
    if not found then begin
        if keyword_set(cache_aia) and file_exist(cache_file) then begin
            fxread, cache_file, image, header
            found = 1
        end else return
    endif
endif
;
;  If the file was not in science mode, or it's a dark image, or the image
;  contains no positive values, then start searching around the original target
;  date in units of one hour.
;
acs_mode = strtrim(fxpar(header, 'acs_mode'))
img_type = strtrim(fxpar(header, 'img_type'))
missvals = fxpar(header, 'missvals')
w = where(image gt 0, npos)
if ((acs_mode ne 'SCIENCE') and (acs_mode ne '')) or (img_type ne 'LIGHT') or $
  (npos eq 0) or (missvals gt 0) then begin
    if (acs_mode ne 'SCIENCE') and (acs_mode ne '') then print, $
      'Rejecting file ' + files[0] + ' -- mode is ' + strtrim(acs_mode,2)
    if img_type ne 'LIGHT' then print, $
      'Rejecting file ' + files[0] + ' -- type is ' + strtrim(img_type,2)
    if npos eq 0 then print, $
      'Rejecting file ' + files[0] + ' -- no positive values'
    if missvals gt 0 then print, $
      'Rejecting file ' + files[0] + ' -- missing pixels'
;
;  If the file was saved for possible caching, then delete it.
;
    if cache_temp ne '' then file_delete, cache_temp
;
    if not keyword_set(nosearch) then begin
        update_utc:
        if n_elements(delta) eq 0 then begin
            delta = -3500.d0
            get_utc, now, /ccsds
        end else delta = -(delta + sign(3500.d0, delta))
        utc = anytim2utc(utc2tai(utc) + delta, /ccsds)
        if utc gt now then goto, update_utc
;
        if (utc lt utc0) or (utc gt utc1) then begin
            found = 0
            return
        end else goto, perform_search
    end else begin
        if n_elements(image_save) gt 1 then begin
            header = temporary(header_save)
            image  = temporary(image_save)
        end else delvarx, header, image
        found = 0
    endelse
endif
;
;  If the file was saved for possible caching, then cache it.
;
if (cache_temp ne '') and file_exist(cache_temp) then $
  file_move, cache_temp, cache_file, /overwrite
;
end
