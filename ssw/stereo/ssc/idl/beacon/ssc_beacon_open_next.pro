;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_OPEN_NEXT
;
; Purpose     :	Open the next IMPACT, PLASTIC or SWAVES beacon telemetry file
;
; Category    :	STEREO, Telemetry
;
; Explanation :	This procedure is called by SSC_BEACON_READER to open the
;               next STEREO beacon telemetry file for the IMPACT, PLASTIC, or
;               SWAVES instruments, for either the Ahead or Behind observatory.
;
; Syntax      :	SSC_BEACON_OPEN_NEXT, INSTRUMENTLUN, ISC
;
; Examples    :	See SSC_BEACON_READER.
;
; Inputs      :	INSTRUMENTLUN   = The two-element array containing the logical
;                                 unit numbers of the currently open Ahead and
;                                 Behind files for the instrument.
;
;               ISC             = The index for the file which needs to be
;                                 refreshed.
;
; Opt. Inputs :	None.
;
; Outputs     :	INSTRUMENTLUN is potentially modified by this routine.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	BREAK_FILE, VALID_NUM, CONCAT_DIR
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 08-Feb-2006, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_open_next, instrumentlun, isc
on_error, 2
;
;  Get the filename, and first 6 characters of the name (e.g. "impact").
;
lun = instrumentlun[isc]
filename = (fstat(lun)).name
break_file, filename, disk, dir, name, ext
prefix = strmid(name,0,6)
;
;  Find the relative position of the next file within that directory.
;
files = file_search(concat_dir(disk+dir, prefix+'*.sw.ptp'))
files = files[sort(files)]
break_file, files, disks, dirs, names, exts
index = (where(name eq names))[0] + 1
;
;  If the next file exists, open it.
;
if index lt n_elements(files) then begin
    file = files[index]
    close, lun
    openr, lun, file
    print, 'Opened file ' + file
    instrumentlun[isc] = lun
;
;  Otherwise, try incrementing the month by 1.
;
end else begin
    month = strmid(dir,strlen(dir)-3,2)
    year  = strmid(dir,strlen(dir)-8,4)
    path = strmid(dir,0,strlen(dir)-9)          ;Strip off year and month
    if valid_num(month) and valid_num(year) then begin
        year = fix(year)
        month = fix(month) + 1
        if month eq 13 then begin
            year = year + 1
            month = 1
        endif
;
;  Form a pathname out of the year and month.  If the path exists, see if there
;  are any files to open in there.  If there are, open the first file.
;
        path = concat_dir(path, string(year, format='(I4.4)'))
        path = concat_dir(path, string(month,format='(I2.2)'))
        if is_dir(path) then begin
            files = file_search(concat_dir(disk+path, prefix+'*.sw.ptp'), $
                                count=count)
            if count gt 0 then begin
                files = files[sort(files)]
                file = files[0]
                close, lun
                openr, lun, file
                print, 'Opened file ' + file
                instrumentlun[isc] = lun
            endif
        endif
    endif
endelse
;
return
end
