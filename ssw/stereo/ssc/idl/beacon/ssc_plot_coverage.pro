;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_PLOT_COVERAGE
;
; Purpose     :	Plot station coverage for STEREO beacon
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Called from SSC_BEACON_READER to plot which antenna station is
;               being used for the spaceweather beacon data for the last 24
;               hours.
;
; Syntax      :	SSC_PLOT_COVERAGE, PKT_DATES, DSN_IDS, SC_IDS
;
; Examples    :	See SSC_BEACON_READER.
;
; Inputs      :	PKT_DATES = The packet date/times
;               DSN_IDS   = The antenna IDs
;               SC_IDS    = The spacecraft IDs
;
; Opt. Inputs :	None.
;
; Outputs     :	Produces a plot.  If the Z-buffer is used, then the plot is
;               captured to a GIF file in the directory given by the
;               environment variable STEREO_BEACON_GIF.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	UTC2TAI, ANYTIM2UTC, CHECK_INT_TIME, UTPLOT, OUTPLOT,
;               CONCAT_DIR
;
; Common      :	None.
;
; Env. Vars.  : STEREO_BEACON_GIF controls where the GIF file is written.
;
; Restrictions:	This routine will need to be updated as new antenna stations
;               are added.
;
; Side effects:	Manipulates various plotting settings.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 04-Apr-2008, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_plot_coverage, pkt_dates, dsn_ids, sc_ids
;
stations = [1,14,15,16,17,18]
names = ['DSN', 'CHI', 'KOG', 'TOU','KIE','BOC']
nstations = n_elements(stations)
;
pkt_date = max(utc2tai(pkt_dates))
today = anytim2utc(pkt_date)
yesterday = today
yesterday.mjd = yesterday.mjd - 1
;
;  Calculate a start time on the appropriate 6 hour boundary on the previous
;  day, i.e. between 24-30 hours before PKT_DATE.
;
sixhour = 21600D3
t0 = yesterday
t0.time = sixhour * floor(t0.time/sixhour)
;
;  Calculate an end time which is 30 hours after the start time.
;
t1 = t0
t1.mjd = t1.mjd + 1
t1.time = t1.time + sixhour
check_int_time, t1
;
;  Define the time range, and set the tick unit to 6 hours.
;
timerange = [t0,t1]
tick_unit = 21600
;
;  Translate the antenna IDs into something easier to plot.
;
ids = 0*dsn_ids
for i = 1,nstations-1 do begin
    w = where(dsn_ids eq stations[i], count)
    if count gt 0 then ids[w] = i
endfor
;
;  Initialize the plot.
;
ytickname = replicate(' ',30)
linecolor, 1, 'red'
linecolor, 2, 'blue'
if !d.name eq 'Z' then device, set_resolution=[600,400]
utplot, timerange, [0, nstations], /nodata, xstyle=1, $
  ytickname=ytickname, ytitle='', yticklen=1e-6, tick_unit=tick_unit
for i=0,nstations-1 do begin
    xyouts, !x.crange[0], i+0.5, names[i]+' ', alignment=1
    if i gt 0 then oplot, !x.crange, [i,i]
endfor
;
;  Put on labels for Ahead and Behind.
;
charsize=!p.charsize
if charsize eq 0 then charsize=1
ysize = charsize*!d.y_ch_size
xwindow = !x.window * !d.x_size
xyouts, xwindow[1], ysize, /device, 'RED: Ahead', charsize=charsize, $
  color=1, alignment=1
xyouts, xwindow[0], ysize, /device, 'BLUE: Behind', charsize=charsize, $
  color=2, alignment=0
;
;  Plot the Ahead and Behind points in red and blue respectively.
;
w = where(sc_ids eq 234, count)
if count gt 0 then outplot, pkt_dates[w], ids[w]+0.33, psym=2, color=1
w = where(sc_ids eq 235, count)
if count gt 0 then outplot, pkt_dates[w], ids[w]+0.67, psym=2, color=2
;
if !d.name eq 'Z' then begin
    image = tvread(red, green, blue, /quiet)
    filename = concat_dir(getenv('STEREO_BEACON_GIF'), 'coverage.gif')
    write_gif, filename, image, red, green, blue
endif
;
end
