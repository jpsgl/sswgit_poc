;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_FROM_LEVEL0
;
; Purpose     :	Reprocess beacon files from level-0 telemetry
;
; Category    :	STEREO, Telemetry
;
; Explanation :	This procedure reprocesses IMPACT and PLASTIC beacon data from
;               the level-0 telemetry, so that gaps in the realtime beacon data
;               can be filled in.  SSC_MAKE_BROWSE is called with the /NOSECCHI
;               keyword to regenerate the in-situ plots.
;
;               The CDF files are first created in /tmp and then moved to the
;               proper directory.  The original CDF files are kept with the
;               extension ".cdf_orig".
;
; Syntax      :	SSC_BEACON_FROM_LEVEL0  [, DATE ]
;
; Examples    :	SSC_BEACON_FROM_LEVEL0
;
; Inputs      :	None required.
;
; Opt. Inputs :	DATE    = The date to process.  The default is to process the
;                         dates 5-7 days in the past.
;
; Outputs     :	Creates .cdf files and web output.
;
; Opt. Outputs:	None.
;
; Keywords    :	QUIET   = Sets the !QUIET system variable.  The default is 1.
;
;               FORCE   = Force file replacement.
;
;               VERSION = Telemetry file version to use.  The default is "02".
;
; Calls       :	SEP_BEACON_COMMON, SWEASTE_B_COMMON, GET_UTC, ANYTIM2UTC,
;               ANYTIM2CAL, CONCAT_DIR, GET_TEMP_DIR, SSC_OPEN_LEVEL0_TELEM,
;               READ_STEREO_PKT, PARSE_STEREO_PKT, IMPACT_BEACON,
;               SSC_BEACON_PATH, PLASTIC_PARSE_PACKET, BREAK_FILE,
;               SSC_MAKE_BROWSE, MK_TEMP_DIR
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	!QUIET is set by this routine
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 28-Jan-2008, William Thompson, GSFC
;               Version 2, 01-Feb-2008, WTT, Fixed bug with .cdf_orig versions
;               Version 3, 18-Dec-2008, WTT, Try for 3 days.  Don't process if
;                       already done.
;               Version 4, 23-Feb-2009, WTT, Added keyword /FORCE
;               Version 5, 29-Jun-2011, WTT, fix for multiple PLASTIC files in
;                       temp directory
;               Version 6, 07-Jul-2011, WTT, catch telemetry file errors
;               Version 7, 30-Jan-2012, WTT, added keyword VERSION
;               Version 8, 12-Apr-2017, WTT, use MK_TEMP_DIR to avoid leaving
;                                            files behind
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_from_level0, date, quiet=quiet, force=force, version=version
;
;  Set up the common blocks needed for the IMPACT beacon software.
;
sep_beacon_common
sweaste_b_common
;
;  Set the !QUIET system variable.
;
if n_elements(quiet) eq 1 then !quiet = quiet else !quiet = 1
;
;  If no date was passed, then go back 5-7 days.
;
if n_params() eq 0 then begin
    get_utc, utc
    utc.mjd = utc.mjd - 4
    for i=1,3 do begin
        utc.mjd = utc.mjd - 1
        ssc_beacon_from_level0, utc
    endfor
    return
end else utc = anytim2utc(date)
;
;  Create a temporary directory for the output files.
;
mk_temp_dir, get_temp_dir(), temppath
;
;  Step through the spacecraft.
;
n_changed = 0
sc = ['STA','STB']
for isc = 0,1 do begin
;
;  Form the name of the IMPACT CDF file.  Remove any old copies in the temp
;  directory.
;
    cdf_name = sc[isc] + '_LB_IMPACT_' + anytim2cal(utc,form=8,/date) + $
      '_V02.cdf'
    tempfile = concat_dir(temppath, cdf_name)
    if file_exist(tempfile) then file_delete, tempfile
;
;  Open the appropriate IMPACT telemetry file.
;
    unit = ssc_open_level0_telem(isc,utc,'impact',version=version)
    if unit gt 0 then begin
;
;  Look for the current file, and process if the file is older than the
;  telemetry file.
;
        filepath = ssc_beacon_path(sc[isc],'impact',utc)
        cdf_name = concat_dir(filepath, cdf_name)
        mtime1 = (fstat(unit)).mtime
        mtime0 = (file_info(cdf_name)).mtime
        if (mtime0 lt mtime1) or keyword_set(force) then $
          while not eof(unit) do begin
;
;  Read an IMPACT packet.  Packets with a datatype of zero mark the end of the
;  packet file.
;
            read_stereo_pkt, unit, packet
            if packet.grh.datatype ne 0 then begin
;
;  Make sure that the packet is really a beacon packet, and that the date is
;  valid (i.e. not back in 1958), and that the ground receipt and packet header
;  sizes agree.
;
                diff = packet.grh.size - packet.pkt.size
                if packet.grh.datatype eq 2 then diff_expect = 64 else $
                  diff_expect = 33
                pkt_date = parse_stereo_pkt(packet, /pkt_date)
                if (parse_stereo_pkt(packet, /data_fmt_id) ge 112) and $
                  (pkt_date.mjd gt 50000) and (diff eq diff_expect) then begin
;
;  Process the packet.
;
                    impact_beacon, packet, tempfile
                endif
;
            endif               ;datatype = 0
        endwhile
        free_lun, unit
;
;  If a CDF file was created, then move it to the proper directory.  Keep a
;  copy of the original version, but not of subsequent versions.
;
        if file_exist(tempfile) then begin
            n_changed = n_changed + 1
            temp = cdf_name
            strput, temp, '??', strlen(temp)-6
            old_files = file_search(temp+'_orig', count=n_old_files)
            if n_old_files eq 0 then begin
                old_files = file_search(temp, count=count)
                if count gt 0 then for i=0,count-1 do $
                  file_move, old_files[i], old_files[i]+'_orig'
            endif
            file_move, tempfile, cdf_name, /overwrite
            print, 'Processed file ' + cdf_name
        endif
    endif                       ;telemetry file found
;
;  Open the appropriate PLASTIC telemetry file.
;
    unit = ssc_open_level0_telem(isc,utc,'plastc')
    if unit gt 0 then begin
;
;  Look for the current file, and process if the file is older than the
;  telemetry file.
;
        filepath = ssc_beacon_path(sc[isc],'plastic',utc)
        cdf_name = sc[isc] + '_LB_PLASTIC_' + anytim2cal(utc,form=8,/date) + $
          '_V*.cdf'
        cdf_name = concat_dir(filepath, cdf_name)
        cdf_file = file_search(cdf_name, count=n_old_files)
        if n_old_files eq 0 then mtime0 = 0 else mtime0 = $
          max((file_info(cdf_file)).mtime)
        mtime1 = (fstat(unit)).mtime
        if (mtime0 lt mtime1) or keyword_set(force) then $
           while not eof(unit) do begin
;
;  Read a PLASTIC packet.  Packets with a datatype of zero mark the end of the
;  packet file.
;
            read_stereo_pkt, unit, packet
            if datatype(packet) eq 'STC' then $
              if packet.grh.datatype ne 0 then begin
;
;  Make sure that the packet is really a beacon packet, and that the date is
;  valid (i.e. not back in 1958).
;
                pkt_date = parse_stereo_pkt(packet, /pkt_date)
                if (parse_stereo_pkt(packet, /data_fmt_id) ge 112) and $
                  (pkt_date.mjd gt 50000) then begin
;
;  Process the packet.
;
                    plastic_parse_packet, packet, filepath=temppath
                endif
;
;  If an end-of-file packet was found, then try to open the next file.
;
            endif
        endwhile
        free_lun, unit
;
;  If a CDF file was created, then move it to the proper directory.  Keep a
;  copy of the original version, but not of subsequent versions.
;
        tempname = concat_dir(temppath, sc[isc] + '_LB_PLASTIC_' + $
                              anytim2cal(utc, form=8, /date) + '_V*.cdf')
        tempfile = file_search(tempname, count=count)
        if count eq 1 then begin
            n_changed = n_changed + 1
            tempfile = tempfile[0]
            break_file, tempfile, disk, dir, name, ext
            cdf_name = name + ext
            filepath = ssc_beacon_path(sc[isc],'plastic',utc)
            cdf_name = concat_dir(filepath, cdf_name)
            temp = cdf_name
            strput, temp, '??', strlen(temp)-6
            old_files = file_search(temp+'_orig', count=n_old_files)
            if n_old_files eq 0 then begin
                old_files = file_search(temp, count=count)
                if count gt 0 then for i=0,count-1 do $
                  file_move, old_files[i], old_files[i]+'_orig'
            endif
            file_move, tempfile, cdf_name, /overwrite
            print, 'Processed file ' + cdf_name
        endif
    endif                       ;telemetry file found
endfor                          ;isc
;
;  Delete the temporary directory and its contents.
;
file_delete, temppath, /recursive
;
;  Regenerate the in-situ web pages.
;
if n_changed gt 0 then ssc_make_browse, utc, /nosecchi
;
end
