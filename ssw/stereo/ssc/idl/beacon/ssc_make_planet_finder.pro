;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_PLANET_FINDER
;
; Purpose     :	Find planets in latest SECCHI beacon images
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	This routine adds labels to the most recent SECCHI coronagraph
;               beacon to show where the planets are.
;
; Syntax      :	SSC_MAKE_PLANET_FINDER
;
; Examples    :	SSC_MAKE_PLANET_FINDER
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	JPEG images are written to the beacon/planets directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  : STEREO_BROWSE      = Location of browse pages
;               STEREO_BEACON_HTML = Location of beacon web pages
;
; Calls       :	CONCAT_DIR, GET_UTC, BREAK_FILE, SCCFINDFITS, SCCREADFITS,
;               GET_STEREO_HPC_POINT, FITSHEAD2WCS, CIRCLE_SYM,
;               GET_STEREO_LONLAT, WCS_GET_PIXEL
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 15-Oct-2009, William Thompson, GSFC
;               Version 2, 18-Jun-2015, WTT, adjust for post-conjunction
;               Version 3, 21-Jun-2017, WTT, rewrote to use SSC_OVERPLOT_PLANET
;                                            overplot HI2 in red
;               Version 4, 22-Jun-2017, WTT, use QUALITY=100, take out red for HI2
;               Version 5, 31-May-2018, WTT, use outline for HI2
;               Version 6, 04-Jun-2018, WTT, also HI1
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_planet_finder
;
;  Get the environment variables
;
stereo_browse = getenv('STEREO_BROWSE')
stereo_beacon = getenv('STEREO_BEACON_HTML')
stereo_planet = concat_dir(stereo_beacon, 'planets')
;
;  Get the date.
;
get_utc, date, /ecs, /date_only
stereo_browse = concat_dir(stereo_browse, date)
;
;  Step through the spacecraft, and the telescopes.
;
sc = ['behind', 'ahead']
tel = ['cor1', 'cor2', 'hi1', 'hi2']
for isc = 0,n_elements(sc)-1 do begin
    sc_path = concat_dir(stereo_browse, sc[isc])
    for itel = 0,n_elements(tel)-1 do begin
        path = concat_dir(sc_path, tel[itel])
        path = concat_dir(path, '256')
;
;  Look for a JPEG file, and read it.
;
        jpeg_file = file_search(concat_dir(path, '*.jpg'), count=njpeg)
        if njpeg gt 0 then begin
            jpeg_file = jpeg_file[njpeg-1]
            ssc_overplot_planet, jpeg_file, image, /beacon, $
                                 outline=(strpos(tel[itel],0,2) eq 'hi')
;
;  Write out the image to the planets directory.
;
            out_name = sc[isc] + '_' + tel[itel] + '_planets.jpg'
            out_name = concat_dir(stereo_planet, out_name)
            sz = size(image)
            true_color = (where(sz[1:sz[0]] eq 3))[0] + 1
            write_jpeg, out_name, image, true=true_color, quality=100
;
        endif                   ;JPEG image found
    endfor                      ;itel
endfor                          ;isc
;
end
