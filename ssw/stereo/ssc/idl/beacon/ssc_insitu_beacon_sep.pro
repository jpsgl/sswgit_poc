;+
; Project     :	STEREO - IMPACT, PLASTIC
;
; Name        : SSC_INSITU_BEACON_SEP
;
; Purpose     : Plot STEREO/IMPACT/SEP space weather beacon data
;
; Category    :	STEREO, IMPACT
;
; Explanation :	Plots STEREO solar energetic particle data from the in-situ
;               space weather beacon, for display on the web.
;
; Syntax      :	SSC_INSITU_BEACON_SEP
;
; Examples    :	See SSC_BEACON_PLOTS
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	TIMERANGE = Time range to plot.  If not passed, then determined
;                           from the data.
;
;               SMOOTH    = Time in seconds to smooth the data.
;
; Calls       :	PARSE_STEREO_NAME, CDF2UTC, UTPLOT, OUTPLOT, SETVIEW
;
; Common      : Uses common block SSC_INSITU_BEACON from ssc_insitu_beacon.cmn
;
; Restrictions:	The data to plot must have been previously read by
;               SSC_INSITU_BEACON_READ.  This is normally done within
;               SSC_INSITU_BEACON_PANELS.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 26-Jun-2007, William Thompson, GSFC
;               Version 2, 02-Jul-2007, WTT, include spacecraft label
;               Version 3, 13-Jul-2007, WTT, plot both A&B
;               Version 4, 01-Jul-2010, WTT, added keyword SMOOTH.
;                       Force EPOCH > launch
;               Version 5, 09-Jul-2014, WTT, fix bug when no Ahead data
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_insitu_beacon_sep, timerange=timerange, smooth=smooth, _extra=_extra
@ssc_insitu_beacon.cmn
;
;  Define the CDF Epoch corresponding to launch.
;
epoch_launch = anytim2cdf('26-Oct-2006 00:52')
;
;  If no files were read in, then simply return.
;
if (n_elements(epoch_sep_5a) eq 0) and $
   (n_elements(epoch_sep_5b) eq 0) then return
;
;  Derive a time range to apply to all the plots.
;
if n_elements(timerange) ne 2 then begin
    epoch_sep = 0.d0
    if n_elements(epoch_sep_5a) gt 0 then $
      epoch_sep = [epoch_sep, epoch_sep_5a * 300d3]
    if n_elements(epoch_sep_5b) gt 0 then $
      epoch_sep = [epoch_sep, epoch_sep_5b * 300d3]
    epoch_sep = epoch_sep[1:*]
    timerange = cdf2utc([min(epoch_sep,max=emax), emax] > epoch_launch)
endif
;
;  Set up for plotting.
;
erase
linecolor,1,'blue'
linecolor,2,'brown'
linecolor,3,'red'
xtickname = replicate(' ',30)
;
;  Step through the spacecraft.
;
spacecraft = ['b','a']
for isc = 0,1 do begin
    delvarx, epoch_sep, sfseptelectrons, sfhetelectrons, sfseptions1
    delvarx, sfseptions2, sfletprotons, sfhetprotons, sfsithe, sfsitcno
    delvarx, sfsitfe, sflethe, sfletcno, sfletfe
    sc = spacecraft[isc]
;
;  Extract out the names of the parameters.
;
    test = execute('epoch_sep = epoch_sep_5' + sc + ' * 300d3')
    test = execute('sfseptelectrons = sfseptelectrons_5' + sc + $
                   '/(sfseptelectrons_n' + sc + '>1)')
    test = execute('sfhetelectrons = sfhetelectrons_5' + sc + $
                   '/(sfhetelectrons_n' + sc + '>1)')
    test = execute('sfseptions1 = sfseptions1_5' + sc + $
                   '/(sfseptions1_n' + sc + '>1)')
    test = execute('sfseptions2 = sfseptions2_5' + sc + $
                   '/(sfseptions2_n' + sc + '>1)')
    test = execute('sfletprotons = sfletprotons_5' + sc + $
                   '/(sfletprotons_n' + sc + '>1)')
    test = execute('sfhetprotons = sfhetprotons_5' + sc + $
                   '/(sfhetprotons_n' + sc + '>1)')
    test = execute('sfsithe = sfsithe_5' + sc + $
                   '/(sfsithe_n' + sc + '>1)')
    test = execute('sfsitcno = sfsitcno_5' + sc + $
                   '/(sfsitcno_n' + sc + '>1)')
    test = execute('sfsitfe = sfsitfe_5' + sc + $
                   '/(sfsitfe_n' + sc + '>1)')
    test = execute('sflethe = sflethe_5' + sc + $
                   '/(sflethe_n' + sc + '>1)')
    test = execute('sfletcno = sfletcno_5' + sc + $
                   '/(sfletcno_n' + sc + '>1)')
    test = execute('sfletfe = sfletfe_5' + sc + $
                   '/(sfletfe_n' + sc + '>1)')
;
;  Convert SEP epoch data into UTC.
;  return.
;
    test_read = n_elements(epoch_sep) gt 0
    if test_read then begin
        epoch_sep_utc = cdf2utc(epoch_sep > epoch_launch)
;
;  If requested, smooth the data.
;
        if keyword_set(smooth) then begin
            sfseptelectrons = utc_bin_smooth(epoch_sep_utc, sfseptelectrons, $
                                             smooth)
            sfhetelectrons = utc_bin_smooth(epoch_sep_utc, sfhetelectrons, $
                                             smooth)
            sfseptions1 = utc_bin_smooth(epoch_sep_utc, sfseptions1, smooth)
            sfseptions2 = utc_bin_smooth(epoch_sep_utc, sfseptions2, smooth)
            sfletprotons = utc_bin_smooth(epoch_sep_utc, sfletprotons, smooth)
            sfhetprotons = utc_bin_smooth(epoch_sep_utc, sfhetprotons, smooth)
            sfsithe = utc_bin_smooth(epoch_sep_utc, sfsithe, smooth)
            sfsitcno = utc_bin_smooth(epoch_sep_utc, sfsitcno, smooth)
            sfsitfe = utc_bin_smooth(epoch_sep_utc, sfsitfe, smooth)
            sflethe = utc_bin_smooth(epoch_sep_utc, sflethe, smooth)
            sfletcno = utc_bin_smooth(epoch_sep_utc, sfletcno, smooth)
            sfletfe = utc_bin_smooth(epoch_sep_utc, sfletfe, smooth)
        endif
    endif
;
;  Plot the electron data.
;
    setview, isc+1, 2, 1, 4, 1, 0.5
    ytitle = '1/(cm!U2!N s sr MeV)'
    utplot, timerange, [1e-3,1e7], /nodata, xstyle=1, /ylog, _extra=_extra, $
      xtitle='', xtickname=xtickname, ytitle=ytitle, ystyle=1
    if test_read then begin
        outplot, epoch_sep_utc, sfseptelectrons, psym=2, symsize=.1
        outplot, epoch_sep_utc, sfhetelectrons,  psym=2, symsize=.1, color=1
    endif
    if isc eq 0 then begin
        xx = !x.window[0] * !d.x_size
        yy = !y.window[1] * !d.y_size + !d.y_ch_size/2
        label = '0.035 to 0.065 MeV Electrons'
        xyouts, xx, yy, label, /device
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '0.7 to 4.0 Mev Electrons'
        xyouts, xx, yy, label, /device, color=1
    endif
;
;  Plot the proton data.
;
    setview, isc+1, 2, 2, 4, 1, 0.5
    utplot, timerange, [1e-5,1e6], /nodata, xstyle=1, /ylog, _extra=_extra, $
      xtitle='', xtickname=xtickname, ytitle=ytitle, ystyle=1
    if test_read then begin
        outplot, epoch_sep_utc, sfseptions1,  psym=2, symsize=.1
        outplot, epoch_sep_utc, sfseptions2,  psym=2, symsize=.1, color=1
        outplot, epoch_sep_utc, sfletprotons, psym=2, symsize=.1, color=2
        outplot, epoch_sep_utc, sfhetprotons, psym=2, symsize=.1, color=3
    endif
    if isc eq 0 then begin
        xx = !x.window[0] * !d.x_size
        yy = !y.window[1] * !d.y_size + !d.y_ch_size/2
        label = '0.14-0.62 MeV H'
        xyouts, xx, yy, label, /device
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '0.62-2.22 MeV H'
        xyouts, xx, yy, label, /device, color=1
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '2.2-12 MeV H'
        xyouts, xx, yy, label, /device, color=2
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '13-100 MeV H'
        xyouts, xx, yy, label, /device, color=3
    endif
;
;  Plot the lower energy metal data.
;
    ytitle='1/(cm!U2!N s sr MeV/nuc.)'
    setview, isc+1, 2, 3, 4, 1, 0.5
    utplot, timerange, [1e-3,1e4], /nodata, xstyle=1, /ylog, _extra=_extra, $
      xtitle='', xtickname=xtickname, ytitle=ytitle, ystyle=1
    if test_read then begin
        outplot, epoch_sep_utc, sfsithe,  psym=2, symsize=.1
        outplot, epoch_sep_utc, sfsitcno, psym=2, symsize=.1, color=1
        outplot, epoch_sep_utc, sfsitfe,  psym=2, symsize=.1, color=3
    endif
    if isc eq 0 then begin
        xx = !x.window[0] * !d.x_size
        yy = !y.window[1] * !d.y_size + !d.y_ch_size/2
        label = '0.12-1.08 MeV/n He'
        xyouts, xx, yy, label, /device
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '0.12-1.08 MeV/n CNO'
        xyouts, xx, yy, label, /device, color=1
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '0.12-1.08 MeV Fe'
        xyouts, xx, yy, label, /device, color=3
    endif
;
;  Plot the higher energy metal data.
;
    setview, isc+1, 2, 4, 4, 1, 0.5
    xtitle = 'Start: ' + anytim2utc(timerange[0],/vms,/date) + ' '
    xtitle = xtitle + strmid(anytim2utc(timerange[0],/ccsds,/time),0,5) + ' UTC'
    utplot, timerange, [1e-5,1e3], /nodata, xstyle=1, /ylog, _extra=_extra, $
      xtitle='', ytitle=ytitle, ystyle=1
    if test_read then begin
        outplot, epoch_sep_utc, sflethe,  psym=2, symsize=.1
        outplot, epoch_sep_utc, sfletcno, psym=2, symsize=.1, color=1
        outplot, epoch_sep_utc, sfletfe,  psym=2, symsize=.1, color=3
    endif
    if isc eq 0 then begin
        xx = !x.window[0] * !d.x_size
        yy = !y.window[1] * !d.y_size + !d.y_ch_size/2
        label = '4 to 12 MeV/n He'
        xyouts, xx, yy, label, /device
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '4 to 12 MeV/n CNO'
        xyouts, xx, yy, label, /device, color=1
        xx = xx + (strlen(label)+3)*!d.x_ch_size
        label = '4 to 12 MeV Fe'
        xyouts, xx, yy, label, /device, color=3
;
        xx = !x.window[0] * !d.x_size
        yy = !y.window[0] * !d.y_size - !d.y_ch_size*2.8
        xyouts, xx, yy, 'STEREO Behind', /device, alignment=0
        xyouts, 0.5*!d.x_size, yy, xtitle, /device, alignment=0.5
    end else begin
        xx = !x.window[1] * !d.x_size
        yy = !y.window[0] * !d.y_size - !d.y_ch_size*2.8
        xyouts, xx, yy, 'STEREO Ahead', /device, alignment=1
    endelse
endfor                          ;isc
;
;  Empty the plot buffer, and reset the plot viewport.
;
empty
setview
;
return
end
