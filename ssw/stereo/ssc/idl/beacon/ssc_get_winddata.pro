;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GET_WINDDATA
;
; Purpose     :	SSC-specific front-end to SolarSoft ACE/DSCOVR routines
;
; Category    :	STEREO, Telemetry
;
; Explanation : This routine serves as a front-end to the ACE and DSCOVR
;               routines in the SolarSoft library, and is specifically for use
;               by the STEREO Science Center.  Returns the SWEPAM and MAG data
;               from ACE, combined with the equivalent data from DSCOVR.  The
;               MAG data is converted from GSM to Geocentric RTN coordinates.
;               All data are converted into the format returned by
;               SSC_GET_ACEDATA for backward compatibility.
;
; Syntax      :	SSC_GET_WINDDATA, TIMERANGE, PLASMA, MAG
;
; Examples    :	See SSC_INSITU_BEACON_PANELS
;
; Inputs      :	TIMERANGE = The time range over which to read the data.
;
; Opt. Inputs :	None
;
; Outputs     :	PLASMA = Structure array containing the PLASMA data.
;
;               MAG    = Structure array containing the MAG data.  The Bx, By,
;                        Bz, Lat, and Long tags are modified to convert from
;                        GSM to Geocentric RTN (GRTN) coordinates.
;
; Opt. Outputs:	None
;
; Keywords    :	/REALTIME = If set, then only read in new data if it's
;                           been more than 5 minutes since the last time data
;                           were read in, or if the timerange has changed.
;
;               Will also accept any other keywords used by SSW_GET_WINDDATA or
;               SSC_GET_ACEDATA (READ_ACE/GET_ACEDATA).
;
; Calls       :	GET_UTC, UTC2TAI, ANYTIM2TAI, TAI2UTC, SSW_GET_WINDDATA,
;               ANYTIM2UTC, CONVERT_SUNSPICE_COORD, DELVARX, SSC_GET_ACEDATA
;
; Common      :	Uses the internal common block SSC_GET_WINDDATA.
;
; Restrictions:	None
;
; Side effects:	None
;
; Prev. Hist. :	Based on SSC_GET_ACEDATA
;
; History     :	Version 1, 2016-08-11, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_get_winddata, timerange, plasma, mag, realtime=realtime, _extra=_extra
;
;  Cache already read-in data in the common block.
;
common ssc_get_winddata, last_tai, last_tairange, last_plasma, last_mag
if n_elements(last_tai) eq 0 then last_tai = 0.0d0
if n_elements(last_tairange) eq 0 then last_tairange = dblarr(2)
;
;  Check the input parameters.
;
if n_params() ne 3 then message, $
  'Syntax: SSC_GET_WINDDATA, TIMERANGE, PLASMA, MAG'
if n_elements(timerange) ne 2 then message, $
  'TIMERANGE must have two elements'
;
;  Check to see if it's time to read in new data.
;
get_utc, utc  &  now = utc2tai(utc)
tairange = anytim2tai(timerange)
if keyword_set(realtime) and ((now-last_tai) lt 300) and $
  (tairange[0] eq last_tairange[0]) and (tairange[1] eq last_tairange[1]) $
  then begin
    plasma = last_plasma
    mag = last_mag
    return
endif
;
;  If the end time is less than 7 days ago, call SSW_GET_WINDATA.
;
weekago = now - 604800.d0
if tairange[1] gt weekago then begin
    starttime = tai2utc(tairange[0] > weekago)
    plasma0 = ssw_get_winddata(starttime, timerange[1], /plasma, _extra=_extra)
    plasma1 = {mjd: 0L, time: 0L, p_density: 0.0, b_speed: 0.0, ion_temp: 0.0d0}
    plasma1 = replicate(plasma1, n_elements(plasma0))
;
    utc = anytim2utc(plasma0.time_tag)
    plasma1.mjd = utc.mjd
    plasma1.time = utc.time
    taimin = utc2tai(utc[0])
;
    plasma1.p_density = plasma0.density
    plasma1.b_speed = plasma0.speed
    plasma1.ion_temp = plasma0.temperature
;
;  Convert the magnetometer data from GSM to Geocentric RTN coordinates.
;
    mag0 = ssw_get_winddata(starttime, timerange[1], /mag, _extra=_extra)
    mag1 = {mjd: 0L, time: 0L, bx: 0.0, by: 0.0, bz: 0.0, bt: 0.0, $
            lat: 0.0, long: 0.0}
    mag1 = replicate(mag1, n_elements(mag0))
;
    utc = anytim2utc(mag0.time_tag)
    mag1.mjd = utc.mjd
    mag1.time = utc.time
    taimin = utc2tai(utc[0]) < taimin
;
    coord = mag0.b_gsm
    convert_sunspice_coord, utc, coord, 'gsm', 'grtn'
    mag1.bx = reform(coord[0,*])
    mag1.by = reform(coord[1,*])
    mag1.bz = reform(coord[2,*])
    mag1.bt = mag0.bt
;
    mag1.lat = reform(asin(coord[2,*]/mag1.bt))  * !radeg
    lon = reform(atan(coord[1,*], coord[0,*])) * !radeg
    w = where(lon lt 0, count)
    if count gt 0 then lon[w] = lon[w] + 360
    mag1.long = lon
end else begin
    delvarx, plasma1, mag1
    taimin = tairange[1]
endelse
;
;  If additional data needs to be read (more than 5 minutes), call
;  ssc_get_acedata.
;
delvarx, plasma0, mag0
if (taimin-tairange[0]) gt 300 then $
  ssc_get_acedata, tai2utc([tairange[0], taimin-1]), plasma0, mag0, $
                   _extra=_extra
;
;  Merge together the two data streams.
;
if datatype(plasma0) eq 'STC' then begin
    plasma = plasma0
    if datatype(plasma1) eq 'STC' then plasma = [plasma, plasma1]
end else if datatype(plasma1) eq 'STC' then plasma = plasma1 else plasma = -1
;
if datatype(mag0) eq 'STC' then begin
    mag = mag0
    if datatype(mag1) eq 'STC' then mag = [mag, mag1]
end else if datatype(mag1) eq 'STC' then mag = mag1 else mag = -1
;
;  Store the data in the common block.
;
last_tai = now
last_tairange = tairange
last_plasma = plasma
last_mag = mag
;
return
end
