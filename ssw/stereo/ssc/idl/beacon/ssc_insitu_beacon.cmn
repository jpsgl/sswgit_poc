;+
; Project     :	STEREO - IMPACT, PLASTIC
;
; Name        : ssc_insitu_beacon.cmn
;
; Purpose     : Common block for SSC insitu beacon processing.
;
; Category    :	STEREO, IMPACT, PLASTIC
;
; Syntax      :	@ssc_insitu_beacon.cmn
;
; Examples    :	See SSC_INSITU_BEACON_READ
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 25-Jun-2007, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
common ssc_insitu_beacon, $
	epoch_mag_a,		epoch_mag_b, $
	magbfield_a,		magbfield_b, $
;
	epoch_sep_a,		epoch_sep_b, $
	epoch_sep_5a,		epoch_sep_5b, $
	sfseptelectrons_a,	sfseptelectrons_b, $
	sfseptelectrons_5a,	sfseptelectrons_5b, $
	sfseptelectrons_na,	sfseptelectrons_nb, $
	sfhetelectrons_a,	sfhetelectrons_b, $
	sfhetelectrons_5a,	sfhetelectrons_5b, $
	sfhetelectrons_na,	sfhetelectrons_nb, $
	sfseptions1_a,		sfseptions1_b, $
	sfseptions1_5a,		sfseptions1_5b, $
	sfseptions1_na,		sfseptions1_nb, $
	sfseptions2_a,		sfseptions2_b, $
	sfseptions2_5a,		sfseptions2_5b, $
	sfseptions2_na,		sfseptions2_nb, $
	sfletprotons_a,		sfletprotons_b, $
	sfletprotons_5a,	sfletprotons_5b, $
	sfletprotons_na,	sfletprotons_nb, $
	sfhetprotons_a,		sfhetprotons_b, $
	sfhetprotons_5a,	sfhetprotons_5b, $
	sfhetprotons_na,	sfhetprotons_nb, $
	sfsithe_a,		sfsithe_b, $
	sfsithe_5a,		sfsithe_5b, $
	sfsithe_na,		sfsithe_nb, $
	sfsitcno_a,		sfsitcno_b, $
	sfsitcno_5a,		sfsitcno_5b, $
	sfsitcno_na,		sfsitcno_nb, $
	sfsitfe_a,		sfsitfe_b, $
	sfsitfe_5a,		sfsitfe_5b, $
	sfsitfe_na,		sfsitfe_nb, $
	sflethe_a,		sflethe_b, $
	sflethe_5a,		sflethe_5b, $
	sflethe_na,		sflethe_nb, $
	sfletcno_a,		sfletcno_b, $
	sfletcno_5a,		sfletcno_5b, $
	sfletcno_na,		sfletcno_nb, $
	sfletfe_a,		sfletfe_b, $
	sfletfe_5a,		sfletfe_5b, $
	sfletfe_na,		sfletfe_nb, $
;
	epoch_plastic_a,	epoch_plastic_b, $
	density_a,		density_b, $
	velocity_a,		velocity_b, $
	temperature_a,		temperature_b
