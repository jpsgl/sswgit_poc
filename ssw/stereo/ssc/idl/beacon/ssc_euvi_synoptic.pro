;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_EUVI_SYNOPTIC
;
; Purpose     :	Form synoptic images from beacon data.
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation :	This routine forms synoptic maps from SECCHI EUVI beacon
;               images.  The most recent matched pair of EUVI images are read
;               in, converted to heliographic coordinates, and combined.
;               Matching SDO/AIA data are also used when available.
;
; Syntax      :	SSC_EUVI_SYNOPTIC  [, MAP, ORIGIN ]
;
; Examples    :	SSC_EUVI_SYNOPTIC
;               IMAGE = TVREAD(RED,GREEN,BLUE)
;               WRITE_GIF, 'euvi_synoptic.gif', IMAGE, RED, GREEN, BLUE
;
; Inputs      :	None required
;
; Opt. Inputs :	None.
;
; Outputs     :	The output of the program is a plot of the synoptic map.
;
; Opt. Outputs:	MAP     = Synoptic map, byte-scaled for display
;
;               ORIGIN  = Coordinates of lower-left corner, for use with
;                         plot_image.
;
; Keywords    :	DATES   = Optional date(s) to pass to SCC_READ_SUMMARY.  If not
;                         passed, then yesterday and today are searched.
;
;               FILENAME= Two-element string array containing the filenames of
;                         the Ahead and Behind files to process.  Use of this
;                         keyword bypasses searching by date.
;
;               SCALE   = Map scale, in degrees.  The default is 1.
;
;               WAVELENGTH = Wavelength to search for.  Default is 195.
;
;               CARRINGTON = If set, then produce a Carrington map from 0-360
;                         degrees.  The default is return a map centered on the
;                         sub-Earth point, i.e. Stonyhurst from -180 to +180.
;
;               CEA     = If set, then the data are displayed in the
;                         Cylindrical Equal Area projection, with the latitude
;                         axis scaled as the sine of the latitude.
;
;               GRID    = If set, then a grid is overplotted on the image at 60
;                         degree intervals in longitude, and 30 degree
;                         intervals in latitude.
;
;               BACKGRID= If set, then put a grid behind the image at 30 degree
;                         intervals in both longitude and latitude.
;
;               COLOR   = If set, then use SECCHI_PREP to load the color table.
;
;               BEACON  = Look for beacon data.  This is the default unless the
;                         DATES keyword is used.
;
;               NPIXELS = Two-element array containing the number of pixels in
;                         the longitude and latitude directions.  When this
;                         option is passed, the SCALE keyword is ignored.
;                         Also, the resulting image is displayed without any
;                         labeling.  Used for generating images to be used in
;                         spherical displays.
;
;               DATE_OBS= Returns the observation date to the calling routine.
;
;               FILL_MISSING= If set, then fill in missing pixels.
;
;               NOAIA   = If set, then don't process the AIA image.  This
;                         option is intended mainly for imaging the far side of
;                         the Sun.
;
;               NOSEARCH= If set, then tell SSC_GET_NEAREST_AIA to not search
;                         around in time, and use the image in the common
;                         block instead.  This is to support the routine
;                         SSC_MAKE_EUVI_SOS.
;
;               REQUIRE_AIA= If set, then require that a corresponding AIA
;                         image be found.  Ignored for dates prior to
;                         2010-05-13.
;
;               SINGLE_SC= If set, then allow map to be built up from a single
;                         STEREO spacecraft.  This option is intended for the
;                         period when one or both spacecraft is behind the Sun.
;
;               ERRMSG  = If defined, then return any error messages in this
;                         variable.
;
; Calls       :	GET_UTC, SCC_READ_SUMMARY, UTC2TAI, SECCHI_PREP, FITSHEAD2WCS,
;               WCS_CONVERT_TO_COORD, WCS_GET_PIXEL, INTERPOLATE,
;               WHERE_MISSING, SCC_BYTSCL, PLOT_IMAGE, SSC_GET_NEAREST_AIA,
;               WCS_CONVERT_DIFF_ROT, WCS_RSUN, SSC_GET_AIA_304_FACTOR
;
; Common      :	Common block SSC_EUVI_SYNOPTIC supports /NOSEARCH keyword
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 8-Apr-2009, William Thompson, GSFC
;               Version 2, 9-Apr-2009, WTT, filter out images with NMISS>0
;               Version 3, 7-May-2009, WTT, include observation date on plot
;               Version 4, 29-Jul-2009, WTT, added keyword NPIXELS
;               Version 5, 12-Nov-2009, WTT, added keyword CEA
;               Version 6, 13-Nov-2009, WTT, added keyword GRID
;               Version 7, 01-Feb-2010, WTT, added keyword BACKGRID
;               Version 8, 22-Jun-2010, WTT, include SDO/AIA data
;               Version 9, 25-Jun-2010, WTT, more robust logic for SDO/AIA
;               Version 10, 27-Sep-2010, WTT, corrected bug when at 180 degrees
;               Version 11, 30-Sep-2010, WTT, use CACHE_AIA for current data
;               Version 12, 05-Jan-2011, WTT, added keyword FILL_MISSING
;                       Improved AIA 304 scaling.  Combine 284 with AIA 211
;               Version 13, 06-Jan-2011, WTT, pass through VSO keywords
;               Version 14, 10-Jan-2011, WTT, added keyword FILENAME
;               Version 15, 13-Jan-2011, WTT, added keyword NOAIA
;               Version 16, 03-Feb-2011, WTT, added keyword NOSEARCH
;               Version 17, 07-Feb-2011, WTT, fixed NOSEARCH bug when no AIA data
;               Version 18, 09-Feb-2011, WTT, call SSC_GET_AIA_304_FACTOR
;               Version 19, 09-Apr-2012, WTT, added keyword REQUIRE_AIA
;               Version 20, 28-Jun-2012, WTT, don't look for AIA before 2010-05-13
;               Version 21, 21-Apr-2014, WTT, added keyword SINGLE_SC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_euvi_synoptic, map, origin, dates=dates, scale=scale, beacon=k_beacon, $
                       wavelength=wavelength, carrington=carrington, $
                       color=color, npixels=npixels, errmsg=errmsg, $
                       date_obs=date_obs, cea=cea, grid=grid, $
                       backgrid=backgrid, fill_missing=k_fill_missing, $
                       filename=filename, noaia=noaia, nosearch=nosearch, $
                       require_aia=require_aia, single_sc=single_sc, $
                       _extra=_extra
;
on_error, 2
common ssc_euvi_synoptic, haia, aia
;
;  Define some defaults.
;
if n_elements(scale) ne 1 then scale = 1
if n_elements(wavelength) eq 0 then wavelength = 195
if n_elements(k_beacon) eq 1 then beacon=k_beacon else beacon=0
;
;  If the filenames were passed directly in, then skip ahead to forming the
;  synoptic map.
;
if n_elements(filename) eq 2 then begin
    filea = filename[0]
    fileb = filename[1]
    goto, proceed
endif
;
;  If DATES was not set, then use the beacon data from yesterday and today, and
;  set the CACHE_AIA parameter.
;
cache_aia = 0
if n_elements(dates) eq 0 then begin
    get_utc, today
    yesterday = today
    yesterday.mjd = yesterday.mjd - 1
    yesterday.time = 0
    dates = [yesterday, today]
    beacon = 1
    cache_aia = 1
endif 
;
;  Get a list of the Ahead files at the right wavelength.
;
list_a = scc_read_summary(date=dates, spacecraft='a', telescope='euvi', $
                         beacon=beacon)
if datatype(list_a) ne 'STC' then begin
    if keyword_set(single_sc) then begin
        nlist_a = 0
        goto, try_b
    endif
    message = 'No Ahead images found'
    goto, handle_error
endif
w = where(list_a.nmiss eq 0, count)
if count eq 0 then begin
    if keyword_set(single_sc) then begin
        nlist_a = 0
        goto, try_b
    endif
    message = 'No valid Ahead images found'
    goto, handle_error
end else list_a = list_a[w]
w = where(list_a.value eq wavelength, count)
if count eq 0 then begin
    if keyword_set(single_sc) then begin
        nlist_a = 0
        goto, try_b
    endif
    message = 'No Ahead images found at wavelength ' + ntrim(wavelength)
    goto, handle_error
end else list_a = list_a[w]
nlist_a = n_elements(list_a)
;
;  Get a list of the Behind files at the right wavelength.
;
try_b:
list_b = scc_read_summary(date=dates, spacecraft='b', telescope='euvi', $
                         beacon=beacon)
if datatype(list_b) ne 'STC' then begin
    if keyword_set(single_sc) and (nlist_a gt 0) then begin
        nlist_b = 0
        goto, select_file
    endif
    message = 'No Behind images found'
    goto, handle_error
endif
w = where(list_b.nmiss eq 0, count)
if count eq 0 then begin
    if keyword_set(single_sc) and (nlist_a gt 0) then begin
        nlist_b = 0
        goto, select_file
    endif
    message = 'No valid Behind images found'
    goto, handle_error
end else list_b = list_b[w]
w = where(list_b.value eq wavelength, count)
if count eq 0 then begin
    if keyword_set(single_sc) and (nlist_a gt 0) then begin
        nlist_b = 0
        goto, select_file
    endif
    message = 'No Behind images found at wavelength ' + ntrim(wavelength)
    goto, handle_error
end else list_b = list_b[w]
nlist_b = n_elements(list_b)
;
;  If only one spacecraft has been selected, then take the last filename.
;
select_file:
if (nlist_a eq 0) or (nlist_b eq 0) then begin
    if nlist_a gt 0 then filea = list_a[nlist_a-1].filename else filea = ''
    if nlist_b gt 0 then fileb = list_b[nlist_b-1].filename else fileb = ''
    goto, proceed
endif
;
;  Convert the time stamps to TAI, and find the smallest difference between
;  images (or 100 seconds, whichever is bigger).
;
tai_a = utc2tai(list_a.date_obs)
tai_b = utc2tai(list_b.date_obs)
tai = [tai_a, tai_b]
tai = tai[reverse(sort(tai))]
delta = min(tai[1:*]-tai) > 100
;
;  Step backwards through the times, and find the first instance where the
;  Ahead and Behind images are within DELTA of each other.
;
for i=0,n_elements(tai)-1 do begin
    da = abs(tai_a - tai[i])
    wa = (where(da eq min(da)))[0]
    db = abs(tai_b - tai[i])
    wb = (where(db eq min(db)))[0]
    if (da[wa] le delta) and (db[wb] le delta) then goto, form_synop
endfor
;
;  Read in the images.
;
form_synop:
filea = list_a[wa].filename
fileb = list_b[wb].filename
;
proceed:
errmsg = ''
date_obs = ''
if filea ne '' then begin
    filea = sccfindfits(filea, beacon=beacon, errmsg=errmsg)
    if errmsg ne '' then return
    secchi_prep, filea, ha, a, /silent, color=color
    date_obs = ha.date_obs
endif
if fileb ne '' then begin
    fileb = sccfindfits(fileb, beacon=beacon, errmsg=errmsg)
    if errmsg ne '' then return
    secchi_prep, fileb, hb, b, /silent, color=color
    if date_obs eq '' then date_obs = hb.date_obs
endif
;
;  Form the longitude and latitude arrays.  If the NPIXELS keyword was passed,
;  then ignore the SCALE keyword, and don't wrap around.
;
if n_elements(npixels) eq 2 then begin
    nx = npixels[0]
    lon = findgen(nx) * 360 / nx
    ny = npixels[1]
    lat = findgen(ny) * 180 / ny - 90
end else begin
    nx = 1 + 360 / scale
    lon = scale*findgen(nx)
    ny = 1 + 180 / scale
    lat = scale*findgen(ny) - 90
endelse
if not keyword_set(carrington) then lon = lon - 180
radeg = 180.d0 / !dpi
if keyword_set(cea) then lat = asin(lat/90) * radeg
origin = [lon[0], lat[0]]
lon = rebin(reform(lon,nx,1), nx, ny)
lat = rebin(reform(lat,1,ny), nx, ny)
;
;  Form the WCS structures, and convert the images into synoptic maps.
;
if filea ne '' then begin
    wcs_a = fitshead2wcs(ha)
    wcs_convert_to_coord, wcs_a, coord_a, 'hg', lon, lat, carrington=carrington
    pixel_a = wcs_get_pixel(wcs_a, coord_a)
    map_a = interpolate(a, reform(pixel_a[0,*,*]), reform(pixel_a[1,*,*]), $
                        missing=!values.f_nan, _extra=_extra)
end else map_a = replicate(!values.f_nan, nx, ny)
if fileb ne '' then begin
    wcs_b = fitshead2wcs(hb)
    wcs_convert_to_coord, wcs_b, coord_b, 'hg', lon, lat, carrington=carrington
    pixel_b = wcs_get_pixel(wcs_b, coord_b)
    map_b = interpolate(b, reform(pixel_b[0,*,*]), reform(pixel_b[1,*,*]), $
                        missing=!values.f_nan, _extra=_extra)
end else map_b = replicate(!values.f_nan, nx, ny)
;
;  Combine the two maps, filling in from Behind where Ahead is missing.
;
map = map_a
w = where_missing(map_a)
map[w] = map_b[w]
;
;  Calculate the distance (squared) from sun center for Ahead and Behind, and
;  use whichever is closest.
;
if (filea ne '') and (fileb ne '') then begin
    dist_a = total((coord_a/ha.rsun)^2, 1)
    dist_b = total((coord_b/hb.rsun)^2, 1)
    w = where(dist_b lt dist_a, count)
    if count gt 0 then map[w] = map_b[w]
end else if filea eq '' then begin
    wcs_a = wcs_b
    dist_b = total((coord_b/hb.rsun)^2, 1)
    dist_a = dist_b
end else begin
    wcs_b = wcs_a
    dist_a = total((coord_a/ha.rsun)^2, 1)
    dist_b = dist_a
endelse
;
;  Look for the nearest SDO/AIA image.  If found, add it to the map.
;
if ~keyword_set(noaia) then begin
    sdo_wave = wavelength
    if sdo_wave eq 195 then sdo_wave = 193
    if sdo_wave eq 284 then sdo_wave = 211
    if filea ne '' then observ_date = wcs_a.time.observ_date else $
      observ_date = wcs_b.time.observ_date
    if observ_date lt '2010-05-13' then found=0 else $
      ssc_get_nearest_aia, observ_date, haia, aia, $
      wavelength=sdo_wave, found=found, cache_aia=cache_aia, $
      nosearch=nosearch, _extra=_extra
    if (found or keyword_set(nosearch)) and (n_elements(aia) gt 0) then begin
        hc = haia
        c  = aia
        found = 1
    end else found = 0
    if found then begin
        c = c / fxpar(hc, 'exptime')
        case wavelength of
            171: c = c * 1.1
            195: c = c / 1.9
            284: c = c / 3.4
            304: c = c * ssc_get_aia_304_factor(wcs_a.time.observ_date)
            else:
        endcase
        wcs_c = fitshead2wcs(hc)
        lon_c = lon
        wcs_convert_diff_rot, wcs_a, wcs_c, lon_c, lat, carrington=carrington
        wcs_convert_to_coord, wcs_c, coord_c, 'hg', lon_c, lat, $
          carrington=carrington
        pixel_c = wcs_get_pixel(wcs_c, coord_c)
        map_c = interpolate(c, reform(pixel_c[0,*,*]), reform(pixel_c[1,*,*]), $
                            missing=!values.f_nan, _extra=_extra)
        rsun_c = 648000.d0 * wcs_rsun() / (!dpi * wcs_c.position.dsun_obs)
        dist_c = total((coord_c/rsun_c)^2, 1)
        w = where(((dist_c lt dist_a) or (not finite(dist_a))) and $
                  ((dist_c lt dist_b) or (not finite(dist_b))) and $
                  finite(dist_c))
        map[w] = map_c[w]
    end else if keyword_set(require_aia) and (date_obs gt '2010-05-13') then begin
        message = 'AIA image not found'
        goto, handle_error
    endif
endif
;
;  If requested, fill in the missing pixels.
;
if keyword_set(k_fill_missing) then fill_missing, map, 0, 1, /extrapolate
;
;  Byte-scale the map.
;
if filea ne '' then map = scc_bytscl(map, ha) else map = scc_bytscl(map, hb)
;
;  Underplot the grid, if requested.
;
if keyword_set(backgrid) then begin
    xtickv = nx * indgen(12) / 12
    for i=0,11 do begin
        ii = xtickv[i]
        w = where(map[ii,*] eq 0, count)
        if count gt 0 then map[ii,w] = 128
    endfor
    ytickv = ny * (indgen(5)+1) / 6
    for j=0,4 do begin
        jj = ytickv[j]
        w = where(map[*,jj] eq 0, count)
        if count gt 0 then map[w,jj] = 128
    endfor
endif
;
;  Scale the map.
;
;
;  If NPIXELS is passed, then simply display the image
;
if n_elements(npixels) eq 2 then begin
    tv, map
;
;  Overplot the grid, if requested.
;
    if keyword_set(grid) then begin
        xtickv = npixels[0] * (indgen(5) + 1.) / 6
        for i=0,4 do plots, replicate(xtickv[i],2), [0,npixels[1]-1], /device
        if keyword_set(cea) then begin
            theta = (-60+30*indgen(5)) / radeg
            ytickv = (sin(theta) + 1) * (npixels[1]-1) / 2
        end else ytickv = npixels[1] * (indgen(5) + 1.) / 6
        for i=0,4 do plots, [0,npixels[0]-1], replicate(ytickv[i],2), /device
    endif
;
;  Otherwise, plot the map.
;
end else begin
    xtickv = origin[0] + 30*indgen(13)
    ytickv = origin[1] + 30*indgen(7)
    ytickname = ntrim(round(ytickv))
    if keyword_set(cea) then ytickv = 90 * sin(ytickv/radeg)
    plot_image, map, origin=origin, scale=scale, /noscale, ticklen=-0.01, $
      xtitle = 'Longitude', ytitle='Latitude', $
      xticks=12, yticks=6, xtickv=xtickv, ytickv=ytickv, ytickname=ytickname, $
      _extra=_extra
;
;  Overplot the grid, if requested.
;
    if keyword_set(grid) then begin
        for i=2,10,2 do oplot, replicate(xtickv[i],2), !y.crange
        for i=1,n_elements(ytickv)-2 do oplot, !x.crange, $
          replicate(ytickv[i],2)
    endif
;
    date = anytim2utc(date_obs, /ecs, /truncate)
    xyouts, !d.x_size/2, !d.y_ch_size/2, alignment=0.5, /device, $
      'Observation date: ' + date, _extra=_extra
endelse
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else errmsg=message
;
end
