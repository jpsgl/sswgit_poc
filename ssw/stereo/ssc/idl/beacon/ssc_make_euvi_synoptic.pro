;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_EUVI_SYNOPTIC
;
; Purpose     :	Wrapper around SSC_EUVI_SYNOPTIC for beacon data
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation :	This routine calls SSC_EUVI_SYNOPTIC to create the file
;               euvi_195_synoptic.gif in the beacon directory.
;
; Syntax      :	SSC_MAKE_EUVI_SYNOPTIC
;
; Examples    :	SSC_MAKE_EUVI_SYNOPTIC
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	Creates $STEREO_BEACON_HTML/euvi_195_synoptic.gif
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Env. Vars.  : UPDATE_BLDG21_BEACON = Script to update bldg21 copy
;
; Calls       :	SETPLOT, SSC_EUVI_SYNOPTIC, TVREAD, CONCAT_DIR, WRITE_GIF,
;               SSC_ROTATE_EUVI_SYNOPTIC
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 8-Apr-2009, William Thompson, GSFC
;               Version 2, 29-Jul-2009, WTT, also write out unlabeled images.
;               Version 3, 10-Sep-2009, WTT, call SSC_ROTATE_EUVI_SYNOPTIC
;               Version 4, 18-Nov-2009, WTT, create GONG comparison map
;               Version 5, 03-Mar-2010, WTT, copy files to building 21
;               Version 6, 04-Mar-2010, WTT, use script (rsync) instead of scp
;               Version 7, 22-Jun-2010, WTT, change EUVI to EUVI/AIA
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_euvi_synoptic
;
;  Generate the labeled image.
;
dname = !d.name
setplot, 'z'
device, set_resolution=[640, 400]
!p.color = !d.table_size - 1
!p.background = 0
errmsg = ''
ssc_euvi_synoptic, /color, errmsg=errmsg, $
  title='EUVI/AIA 195 Stonyhurst Heliographic (Earth-view)'
if errmsg eq '' then begin
    image = tvread(red, green, blue)
    filename = concat_dir('$STEREO_BEACON_HTML', 'euvi_195_heliographic')
    write_gif, filename+'.gif', image, red, green, blue
;
;  Generate the unlabeled images.
;
    device, set_resolution=[1024,512]
    ssc_euvi_synoptic, /color, npixels=[1024,512], date_obs=date_obs
    image = tvread(red, green, blue)
    write_gif, filename+'_1024x512.gif', image, red, green, blue
    write_gif, filename+'_512x256.gif', rebin(image,512,256), red, green, blue
    openw, unit, filename+'.txt', /get_lun
    printf, unit, anytim2utc(date_obs, /vms)
    printf, unit, 195
    free_lun, unit
;
;  Generate the movie.
;
    filename = concat_dir('$STEREO_BEACON_HTML', 'euvi_195_rotated')
    ssc_rotate_euvi_synoptic, filename+'.tmp', image, red, green, blue
    file_move, filename+'.tmp', filename+'.gif', /overwrite
;
;  Generate the image to compare with GONG far-side maps.
;
    device, set_resolution=[500,200]
    ssc_euvi_synoptic, /color, npixels=[500,200], date_obs=date_obs, $
      /carrington, /cea, /grid
    image = tvread(red, green, blue)
    gongfile = 'euvi_195_carrington_500x200.gif'
    gongfile = concat_dir('$STEREO_BEACON_HTML', gongfile)
    write_gif, gongfile, image, red, green, blue
endif
;
setplot, dname
;
;  Copy the images to the public website.
;
update_bldg21_beacon = getenv('UPDATE_BLDG21_BEACON')
if update_bldg21_beacon ne '' then spawn, update_bldg21_beacon
;
end
