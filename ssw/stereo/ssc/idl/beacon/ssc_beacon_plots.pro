;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_PLOTS
;
; Purpose     :	Create STEREO beacon plots for the web
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Creates the STEREO space weather beacon plots for the web.
;               Currently, this procedure only creates the in-situ beacon plot.
;
; Syntax      :	SSC_BEACON_PLOTS  [, PKT_DATE ]
;
; Examples    :	See SSC_BEACON_READER.
;
; Inputs      :	None required.
;
; Opt. Inputs :	PKT_DATE = The date of the latest ingested packet.  When
;                          passed, the time-range of the plot is set to 30
;                          hours long, on 6 hour segments, with PKT_DATE in
;                          the last segment.  Ignored, if TIMERANGE is passed.
;
; Outputs     :	Produces a plot.  If the Z-buffer is used, then the plot is
;               captured to a GIF file in the directory given by the
;               environment variable STEREO_BEACON_GIF, and
;               SSC_INSITU_BEACON_SEP is called to create additional plots for
;               Ahead and Behind.
;
; Opt. Outputs:	None.
;
; Keywords    :	TIMERANGE = Range of times to plot over.
;
;               TICK_UNIT = Number of seconds between tick marks,
;                           e.g. TICK_UNIT=21600 for six hours.
;
;               FILEPATH  = Path to write GIF file.
;
;               TITLE     = Plot title.  If not passed, then generated
;                           automatically from the end time.
;
;               SUFFIX    = Suffix to add to filenames, e.g. SUFFIX='_3day' to
;                           produce beacon_insitu_3day.gif.
;
;               SWAVES    = If set, then call SSC_SWAVES_SWX_PLOT
;
; Calls       :	DELVARX, ANYTIM2UTC, ANYTIM2CAL, SSC_INSITU_BEACON_PANELS,
;               SSC_BEACON_PATH, SSC_INSITU_BEACON_SEP
;
; Common      :	None.
;
; Env. Vars.  : STEREO_BEACON_GIF controls where the GIF file is written.
;
; Restrictions:	None.
;
; Side effects:	Manipulates various plotting settings.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 10-Feb-2006, W. Thompson, GSFC
;               Version 2, 13-Jun-2006, W. Thompson, GSFC
;                       Added TIMERANGE, TICK_UNIT, FILEPATH, TITLE
;               Version 3, 26-Jun-2007, William Thompson, GSFC
;                       Added calls to SSC_INSITU_BEACON_SEP
;                       Make window a little bigger for total B plot
;               Version 4, 17-Jul-2007, WTT, one SEP plot for both A&B
;               Version 5, 31-Jan-2008, WTT, Make solar wind plot bigger
;               Version 6, 11-Aug-2008, WTT, Pass REALTIME keyword to
;                       SSC_INSITU_BEACON_PANELS
;               Version 7, 03-Sep-2008, WTT, Make solar wind plot bigger
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_plots, pkt_date, timerange=k_timerange, tick_unit=tick_unit, $
  filepath=filepath, title=title, suffix=suffix, swaves=swaves
;
;  Define the time range.
;
realtime = 0
if n_elements(k_timerange) eq 2 then begin
    timerange = k_timerange
;
;  Otherwise, if PKT_DATE was passed, then use that to define the time range.
;
end else if n_elements(pkt_date) eq 1 then begin
    realtime = 1
    today = anytim2utc(pkt_date)
    yesterday = today
    yesterday.mjd = yesterday.mjd - 1
;
;  Calculate a start time on the appropriate 6 hour boundary on the previous
;  day, i.e. between 24-30 hours before PKT_DATE.
;
    sixhour = 21600D3
    t0 = yesterday
    t0.time = sixhour * floor(t0.time/sixhour)
;
;  Calculate an end time which is 30 hours after the start time.
;
    t1 = t0
    t1.mjd = t1.mjd + 1
    t1.time = t1.time + sixhour
    check_int_time, t1
;
;  Define the time range, and set the tick unit to 6 hours.
;
    timerange = [t0,t1]
    tick_unit = 21600
endif
;
;  If a timerange has been specified, then find all the files.
;
if n_elements(timerange) eq 2 then begin
    mjd0 = (anytim2utc(timerange[0])).mjd
    mjd1 = (anytim2utc(timerange[1])).mjd
    for mjd = mjd0,mjd1 do begin
        date = {mjd: mjd, time: 0L}
        s_date = anytim2cal(date, form=8, /date)
;
        path = ssc_beacon_path('STA', 'impact', date)
        test = concat_dir(path, 'STA_LB_IMPACT_'  + s_date + '*.cdf')
        files = file_search(test)
        if mjd eq mjd0 then impact_a=files else impact_a=[impact_a, files]
;
        path = ssc_beacon_path('STB', 'impact', date)
        test = concat_dir(path, 'STB_LB_IMPACT_'  + s_date + '*.cdf')
        files = file_search(test)
        if mjd eq mjd0 then impact_b=files else impact_b=[impact_b, files]
;
        path = ssc_beacon_path('STA', 'plastic', date)
        test = concat_dir(path, 'STA_LB_PLASTIC_'  + s_date + '*.cdf')
        files = file_search(test)
        if mjd eq mjd0 then plastic_a=files else plastic_a=[plastic_a,files]
;
        path = ssc_beacon_path('STB', 'plastic', date)
        test = concat_dir(path, 'STB_LB_PLASTIC_'  + s_date + '*.cdf')
        files = file_search(test)
        if mjd eq mjd0 then plastic_b=files else plastic_b=[plastic_b,files]
;
        if keyword_set(swaves) then begin
            path = ssc_beacon_path('STA', 'swaves', date)
            test = concat_dir(path, 'STA_LB_SWAVES_'  + s_date + '.idlsave')
            files = file_search(test)
            if mjd eq mjd0 then swaves_a=files else swaves_a=[swaves_a, files]
;
            path = ssc_beacon_path('STB', 'swaves', date)
            test = concat_dir(path, 'STB_LB_SWAVES_'  + s_date + '.idlsave')
            files = file_search(test)
            if mjd eq mjd0 then swaves_b=files else swaves_b=[swaves_b, files]
        endif
    endfor
;
;  Otherwise, simply find all the CDF files in the current directory, and let
;  the time range be selected automatically.
;
end else begin
    impact_a  = (file_search('STA_LB_IMPACT_*.cdf'))
    impact_b  = (file_search('STB_LB_IMPACT_*.cdf'))
    plastic_a = (file_search('STA_LB_PLASTIC_*.cdf'))
    plastic_b = (file_search('STB_LB_PLASTIC_*.cdf'))
    if keyword_set(swaves) then begin
        swaves_a  = (file_search('STA_LB_SWAVES_*.idlsave'))
        swaves_b  = (file_search('STB_LB_SWAVES_*.idlsave'))
    endif
endelse
;
;  So long as at least one file was found, make the plot.
;
if (total(impact_a ne '') + total(plastic_a ne '') + $
    total(impact_b ne '') + total(plastic_b ne '')) gt 0 then begin
    !p.color = 0
    !p.background = !d.table_size-1
    linecolor,0,'black'
    if !d.name eq 'Z' then device,set_resolution=[550,800]
    if have_windows() then window,0,xsize=550,ysize=800
;
;  If the timerange ends before 2007-06-26, then call version 1 of the plotting
;  program.
;
    ver1 = anytim2utc(timerange[1], /ccsds) le '2007-06-26T00:00:00.000'
    if ver1 then $
      ssc_insitu_beacon_panels_v1, impact_a, impact_b, plastic_a, plastic_b, $
          timerange=timerange, tick_unit=tick_unit, title=title else $
      ssc_insitu_beacon_panels, impact_a, impact_b, plastic_a, plastic_b, $
          timerange=timerange, tick_unit=tick_unit, title=title, $
          realtime=realtime
;
;  If the Z-buffer is used, then capture the plot into a GIF file.
;
    if !d.name eq 'Z' then begin
        image = tvread(red, green, blue, /quiet)
        if n_elements(filepath) eq 0 then filepath=getenv('STEREO_BEACON_GIF')
        filename = concat_dir(filepath, 'beacon_insitu')
        if n_elements(suffix) eq 1 then filename = filename + suffix
        filename = filename + '.gif'
        write_gif, filename, image, red, green, blue
;
;  Also, go on to make the SEP plots.
;
        if not ver1 then begin
            device, set_resolution=[800,900]
            if (total(impact_a ne '') gt 0) or (total(impact_b ne '') gt 0) $
              then begin
                ssc_insitu_beacon_sep, timerange=timerange, tick_unit=tick_unit
                image = tvread(red, green, blue, /quiet)
                filename = concat_dir(filepath, 'beacon_sep')
                if n_elements(suffix) eq 1 then filename = filename + suffix
                filename = filename + '.gif'
                write_gif, filename, image, red, green, blue
            endif
        endif                   ;Not IMPACT version 1
    endif                       ;Z buffer
endif
;
;  If the /SWAVES keyword was set, then read in the SWAVES data.
;
if keyword_set(swaves) and (n_elements(timerange) eq 2) then begin
    delvarx, date1a, spec1a, date1b, spec1b
;
    for ifile=0,n_elements(swaves_a)-1 do if file_exist(swaves_a[ifile]) then $
      begin
        restore, swaves_a[ifile]
        if n_elements(date1a) eq 0 then begin
            date1a = specdate
            spec1a = spectra
        end else begin
            date1a = [date1a, specdate]
            spec1a = [[spec1a], [spectra]]
        endelse
    endif
    s = sort(date1a)
    date1a = date1a[s]
    spec1a = spec1a[*,s]
    u = uniq(date1a)
    date1a = date1a[u]
    spec1a = spec1a[*,u]
;
    for ifile=0,n_elements(swaves_b)-1 do if file_exist(swaves_b[ifile]) then $
      begin
        restore, swaves_b[ifile]
        if n_elements(date1b) eq 0 then begin
            date1b = specdate
            spec1b = spectra
        end else begin
            date1b = [date1b, specdate]
            spec1b = [[spec1b], [spectra]]
        endelse
    endif
    s = sort(date1b)
    date1b = date1b[s]
    spec1b = spec1b[*,s]
    u = uniq(date1b)
    date1b = date1b[u]
    spec1b = spec1b[*,u]
;
    target = utc2tai(timerange[0],/nocorrect)/60.d0 + dindgen(1800)
    inputa = long(utc2tai(date1a,/nocorrect)/60.d0)
    inputb = long(utc2tai(date1b,/nocorrect)/60.d0)
    speca = replicate(-1,1800,318)
    specb = replicate(-1,1800,318)
    for i=0,n_elements(inputa)-1 do begin
        w = where(inputa[i] eq target, count)
        if count gt 0 then speca[w[0],*] = spec1a[*,i]
    endfor
    for i=0,n_elements(inputb)-1 do begin
        w = where(inputb[i] eq target, count)
        if count gt 0 then specb[w[0],*] = spec1b[*,i]
    endfor
;
    device, set_resolution=[800,600]
    ssc_swaves_swx_plot, speca, specb, tick_unit=tick_unit, $
                         anytim2utc(timerange[0],/ecs,/truncate)
    if !d.name eq 'Z' then begin
        image = tvread(red, green, blue, /quiet)
        if n_elements(filepath) eq 0 then filepath=getenv('STEREO_BEACON_GIF')
        filename = concat_dir(filepath, 'beacon_swaves')
        if n_elements(suffix) eq 1 then filename = filename + suffix
        filename = filename + '.gif'
        write_gif, filename, image, red, green, blue
    endif
endif                           ;Process SWAVES
return
end
