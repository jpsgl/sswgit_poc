;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GATHER_EUVI_SYNOPTIC
;
; Purpose     :	Gather STEREO images for synoptic maps.
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation : This routine gathers together the STEREO and SDO images for
;               synoptic maps. The matched pair of EUVI images best fitting the
;               search criteria are read in.  Matching SDO/AIA data are also
;               used when available.
;
; Syntax      :	SSC_GATHER_EUVI_SYNOPTIC, HA, A, HB, B  [, HAIA, AIA ]
;
; Examples    :	See SSC_EUVI_SYNOPTIC
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	HA      = Header index for Ahead
;               A       = Ahead image
;               HB      = Header index for Behind
;               B       = Behind image
;
; Opt. Outputs:	HAIA    = Header index for AIA
;               AIA     = AIA image
;
; Keywords    :	DATES   = Optional date(s) to pass to SCC_READ_SUMMARY.  If not
;                         passed, then yesterday and today are searched.
;
;               FILENAME= Two-element string array containing the filenames of
;                         the Ahead and Behind files to process.  Use of this
;                         keyword bypasses searching by date.
;
;               WAVELENGTH = Wavelength to search for.  Default is 195.
;
;               COLOR   = If set, then use SECCHI_PREP to load the color table.
;
;               BEACON  = Look for beacon data.  This is the default unless the
;                         DATES keyword is used.
;
;               NOSEARCH= If set, then tell SSC_GET_NEAREST_AIA to not search
;                         around in time, and use the image in the common
;                         block instead.  This is to support the routine
;                         SSC_MAKE_EUVI_SOS.
;
;               SINGLE_SC= If set, then allow map to be built up from a single
;                         STEREO spacecraft.  This option is intended for the
;                         period when one or both spacecraft is behind the Sun.
;
;               NEWEST  = If set, then select whichever image is newest, and
;                         find the closest match on the other spacecraft.
;
;               AHEAD_ONLY= If set, then don't look for Behind images.
;
;               BEHIND_ONLY= If set, then don't look for Ahead images.
;
;               ERRMSG  = If defined, then return any error messages in this
;                         variable.
;
; Calls       :	DELVARX, GET_UTC, SCC_READ_SUMMARY, DATATYPE, SCCFINDFITS,
;               SECCHI_PREP, SCC_GET_NEAREST_AIA
;
; Common      :	Common block SSC_EUVI_SYNOPTIC supports /NOSEARCH keyword
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 24-Jul-2014, WTT, split off from SSC_EUVI_SYNOPTIC
;               Version 2, 25-Jul-2014, WTT, added keyword NEWEST
;                       Improved logic for finding matches.
;               Version 3, 28-Jul-2014, WTT, added /AHEAD_ONLY, /BEHIND_ONLY
;               Version 4, 04-Aug-2014, WTT, added check for prog='Dark'
;               Version 5, 07-Aug-2014, WTT, filter out beacon images if other
;                      images are available
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_gather_euvi_synoptic, ha, a, hb, b, hc, c, dates=dates, $
                              filename=filename, beacon=k_beacon, $
                              wavelength=wavelength, color=color, $
                              nosearch=nosearch, single_sc=single_sc, $
                              newest=newest, ahead_only=ahead_only, $
                              behind_only=behind_only, errmsg=errmsg, $
                              _extra=_extra
;
on_error, 2
common ssc_euvi_synoptic, haia, aia
;
;  Define some defaults.
;
if n_elements(scale) ne 1 then scale = 1
if n_elements(wavelength) eq 0 then wavelength = 195
if n_elements(k_beacon) eq 1 then beacon=k_beacon else beacon=0
delvarx, ha, a, hb, b, hc, c, /old
;
;  If the filenames were passed directly in, then skip ahead to reading in the
;  data.
;
if n_elements(filename) eq 2 then begin
    filea = filename[0]
    fileb = filename[1]
    goto, proceed
endif
;
;  If DATES was not set, then use the beacon data from yesterday and today, and
;  set the CACHE_AIA parameter.
;
cache_aia = 0
if n_elements(dates) eq 0 then begin
    get_utc, today
    yesterday = today
    yesterday.mjd = yesterday.mjd - 1
    yesterday.time = 0
    dates = [yesterday, today]
    beacon = 1
    cache_aia = 1
endif 
;
;  Get a list of the Ahead files at the right wavelength.
;
if keyword_set(behind_only) then nlist_a = 0 else begin
    list_a = scc_read_summary(date=dates, spacecraft='a', telescope='euvi', $
                              beacon=beacon)
    if datatype(list_a) ne 'STC' then begin
        if keyword_set(single_sc) then begin
            nlist_a = 0
            goto, try_b
        endif
        message = 'No Ahead images found'
        goto, handle_error
    endif
    w = where((list_a.nmiss eq 0) and (list_a.prog ne 'Dark'), count)
    if count eq 0 then begin
        if keyword_set(single_sc) then begin
            nlist_a = 0
            goto, try_b
        endif
        message = 'No valid Ahead images found'
        goto, handle_error
    end else list_a = list_a[w]
    w = where(list_a.value eq wavelength, count)
    if count eq 0 then begin
        if keyword_set(single_sc) then begin
            nlist_a = 0
            goto, try_b
        endif
        message = 'No Ahead images found at wavelength ' + ntrim(wavelength)
        goto, handle_error
    end else list_a = list_a[w]
;
;  If the list contains both beacon and non-beacon images, then remove the
;  beacon images.
;
    w = where(list_a.dest ne 'SW', count)
    if count gt 0 then list_a = list_a[w]
;
    nlist_a = n_elements(list_a)
endelse
;
;  Get a list of the Behind files at the right wavelength.
;
try_b:
if keyword_set(ahead_only) then nlist_b = 0 else begin
    list_b = scc_read_summary(date=dates, spacecraft='b', telescope='euvi', $
                              beacon=beacon)
    if datatype(list_b) ne 'STC' then begin
        if keyword_set(single_sc) and (nlist_a gt 0) then begin
            nlist_b = 0
            goto, select_file
        endif
        message = 'No Behind images found'
        goto, handle_error
    endif
    w = where((list_b.nmiss eq 0) and (list_b.prog ne 'Dark'), count)
    if count eq 0 then begin
        if keyword_set(single_sc) and (nlist_a gt 0) then begin
            nlist_b = 0
            goto, select_file
        endif
        message = 'No valid Behind images found'
        goto, handle_error
    end else list_b = list_b[w]
    w = where(list_b.value eq wavelength, count)
    if count eq 0 then begin
        if keyword_set(single_sc) and (nlist_a gt 0) then begin
            nlist_b = 0
            goto, select_file
        endif
        message = 'No Behind images found at wavelength ' + ntrim(wavelength)
        goto, handle_error
    end else list_b = list_b[w]
;
;  If the list contains both beacon and non-beacon images, then remove the
;  beacon images.
;
    w = where(list_b.dest ne 'SW', count)
    if count gt 0 then list_b = list_b[w]
;
    nlist_b = n_elements(list_b)
endelse
;
;  If only one spacecraft has been selected, then take the last filename.
;
select_file:
if (nlist_a eq 0) or (nlist_b eq 0) then begin
    if nlist_a gt 0 then filea = list_a[nlist_a-1].filename else filea = ''
    if nlist_b gt 0 then fileb = list_b[nlist_b-1].filename else fileb = ''
    goto, proceed
endif
;
;  Convert the time stamps to TAI.
;
tai_a = utc2tai(list_a.date_obs)
tai_b = utc2tai(list_b.date_obs)
;
;  If the /NEWEST option was selected, then determine which spacecraft has the
;  most recent image, and remove all other entries for that spacecraft.
;
if keyword_set(newest) then begin
    max_ta = max(tai_a)
    max_tb = max(tai_b)
    if max_ta gt max_tb then begin
        w = (where(tai_a eq max_ta))[0]
        list_a = list_a[w]
        tai_a = tai_a[w]
        nlist_a = 1
    end else begin
        w = (where(tai_b eq max_ta))[0]
        list_b = list_b[w]
        tai_b = tai_b[w]
        nlist_b = 1
    endelse
endif
;
;  Find the smallest difference between images (or 100 seconds, whichever is
;  bigger).
;
delta = rebin(reform(tai_b, 1, nlist_b), nlist_a, nlist_b) - $
        rebin(reform(tai_a, nlist_a, 1), nlist_a, nlist_b)
delta = min(abs(delta)) > 100
;
;  Step backwards through the times, and find the first instance where the
;  Ahead and Behind images are within DELTA of each other.
;
for wa=nlist_a-1,0,-1 do begin
    db = abs(tai_b - tai_a[wa])
    wb = where(db eq min(db), nb)
    wb = wb[nb-1]
    if db[wb] le delta then goto, form_synop
endfor
;
;  Read in the images.
;
form_synop:
filea = list_a[wa].filename
fileb = list_b[wb].filename
;
proceed:
errmsg = ''
date_obs = ''
if filea ne '' then begin
    filea = sccfindfits(filea, beacon=beacon, errmsg=errmsg)
    if errmsg ne '' then return
    secchi_prep, filea, ha, a, /silent, color=color
    date_obs = ha.date_obs
endif
if fileb ne '' then begin
    fileb = sccfindfits(fileb, beacon=beacon, errmsg=errmsg)
    if errmsg ne '' then return
    secchi_prep, fileb, hb, b, /silent, color=color
    if date_obs eq '' then date_obs = hb.date_obs
endif
;
;  Look for the nearest SDO/AIA image.  If found, add it to the map.
;
if n_params() eq 6 then begin
    sdo_wave = wavelength
    if sdo_wave eq 195 then sdo_wave = 193
    if sdo_wave eq 284 then sdo_wave = 211
    if date_obs lt '2010-05-13' then found=0 else $
      ssc_get_nearest_aia, date_obs, haia, aia, $
      wavelength=sdo_wave, found=found, cache_aia=cache_aia, $
      nosearch=nosearch, _extra=_extra
    if (found or keyword_set(nosearch)) and (n_elements(aia) gt 0) then begin
        hc = haia
        c  = aia
    endif
endif
;
return
;
;  Error handling point.
;
handle_error:
if n_elements(errmsg) eq 0 then message, message else errmsg=message
;
end
