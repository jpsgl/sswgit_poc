;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_SWAVES_SWX_PLOT
;
; Purpose     :	Plot the SWAVES beacon data
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Takes two SWAVES beacon telemetry arrays and 
;
; Syntax      :	SSC_SWAVES_SWX_PLOT, SPECA, SPECB, DATE
;
; Examples    :	See SSC_BEACON_PLOTS
;
; Inputs      :	SPECA   = Ahead spectrum.  Must have dimensions (1800,318).
;                         Contains the intensity values for 318 frequencies
;                         every minute for 30 hours (i.e. 60*30=1800)
;               SPECB   = Behind spectrum.
;               DATE    = Start date as character string
;
; Opt. Inputs :	None
;
; Outputs     :	Generates a plot of the SWAVES beacon data
;
; Opt. Outputs:	None
;
; Keywords    :	TICK_UNIT = Number of seconds between tick marks,
;                           e.g. TICK_UNIT=21600 for six hours.
;
; Calls       :	BLANKTICKS, UTPLOT_IMAGE
;
; Common      :	None.
;
; Restrictions:	Only 30-hour plots are supported.
;
; Side effects:	None.
;
; Prev. Hist. :	
; Original  Jan. 1997 for Wind/WAVES
; Minor correction to DOY labelling Feb. 1997
; Minor correction to correct filename construction Mar 31, 1997
; Minor correction to fix long plots April 29, 1997
; Addition of log scales for rads May 16, 1997
; Fix of tnr intensities (x2 error --> 20 log not 10 log) June 4, 1997
; Fix fractional hour plots bug  June 24, 1997  [RJM calls this version 1.2]
; Added capability to plot single time series a la RAR.  1/25/98 RJM
; Version 2.0:  Modified to use CASE statement, added RJM stuff.   1/26/98 RJM
; Version 2.01: Corrected up/low freq limits when < full range.    1/30/98 RJM
; Version 2.02:  Removed some of MacDowall's modifications
; Version 3.0:  New rad formats and new file names                 8/17/98 MLK
; Version 3.01  Fix color problem for non Sun 8-bit machines       8/25/98 MLK
; Version 3.02  Fix tnr fixed frequency plot                       9/01/98 MLK
; Version 4.0  Use of shade_surf to do the spectra
;              add log capabilty for frequency axis
;              clean up B&W printing a bit                         1/21/99 MLK
; Version 4.01 Fixed B&W printing bug (Oops!)                      1/25/99 MLK
; Version 4.1  Added comnbined dynamic spectra                     3/10/99 MLK
; Version 4.11 Fixed minor freq labelling bug in fixed freq plot   3/23/99 MLK
; Version 5.0  Crude widget replacement of WMENU                   8/21/00 MLK
; Version 5.1  Addition of RAD1+RAD2 capability                   11/14/01 MLK
; Version 5.2  Addition of 1/f capability                         11/27/01 MLK
; Version 5.3  Addition of gif and png capability                 05/08/02 MLK
; Version 6.0  Use environment variables for data directories
;              and use GET_SCREEN_SIZE                            03/21/05 WTT
;Version 7.0  Converted for S/WAVES                              03/23/2007 MLK
;Version 7.1  Trimmed way down for SWAVES beacon data  11/06/2007  MLK
;
; History     :	Version 1, 28-Nov-2007, William Thompson, GSFC
;                       Rewrote to use UTPLOT_IMAGE
;
; Contact     :	WTHOMPSON
;-
;

pro SSC_SWAVES_SWx_plot, speca, specb, date, tick_unit=tick_unit
newy=indgen(318)
freqlo=0.125+findgen(318)*.05
hour=findgen(1800)/60.
xdim=1800
ydim=318
array_a=fltarr(1800,318)
array_b=fltarr(1800,318)
p_title='STEREO WAVES beginning on '+date
; Load w-b color table
;
wb=255-indgen(256)
tvlct,wb,wb,wb
;
; Set plotting background to white, labelling to black (most color tables)
;
!p.background=0
!p.color=255
flow=0.125
fhigh=16.025
fspace=.050
funit='MHz'
linlog=1
yll='log'
;  Need to interpolate over missing data

for i = 0,1799 do begin
    lzer = where(speca(i,*) gt 0.)
    if(n_elements(lzer) lt 5) then goto,next_b
    v = speca(i,lzer)
    array_a(i,0:317)= interpol(v,lzer,newy)>0.
next_b:
    lzer = where(specb(i,*) gt 0.)
    if(n_elements(lzer) lt 5) then goto,next_time
    v = specb(i,lzer)
    array_b(i,0:317)= interpol(v,lzer,newy)>0.
next_time:
endfor
;
; background subtraction
;
for fr=0,317 do begin
    tot=total(array_a(*,fr))
    bkgnd=0.
    if  (tot le 0.) then goto,bback
    gtz=where(array_a(*,fr) gt 0.)
    bkgnd=min(array_a(gtz,fr))
    array_a(*,fr)=array_a(*,fr)-bkgnd
bback:
    tot=total(array_b(*,fr))
    bkgnd=0.
    if  (tot le 0.) then goto,nextb
    gtz=where(array_b(*,fr) gt 0.)
    bkgnd=min(array_b(gtz,fr))
    array_b(*,fr)=array_b(*,fr)-bkgnd
nextb:
endfor
plot_data:
scale1=0.0
scale2=10.0
nticks=10

erase

ncolors=(!d.table_size<256)-3
colors=indgen(ncolors)
bar=intarr(ncolors,5)
for j=0,4 do bar(*,j)=colors
contour,bar,position=[.2,.075,.8,.1],xrange=[scale1,scale2],xticks=nticks,$
xstyle=1,ystyle=4,font=0,$
xtitle='intensity (dB) relative to background',/nodata,/noerase
px=!x.window*!d.x_vsize
py=!y.window*!d.y_vsize
sx=px(1)-px(0)+1
sy=py(1)-py(0)+1
tv,poly_2d(byte(bar),[[0,0],[ncolors/sx,0]],[[0,5/sy],$
[0,0]],0,sx,sy),px(0),py(0)
xyouts,.5,.81,/normal,font=0,p_title,charsize=2.0,alignment=0.5
arrayab=bytscl(array_a,min=scale1,max=scale2,top=ncolors-1)
arraybb=bytscl(array_b,min=scale1,max=scale2,top=ncolors-1)
;
utc = anytim2utc(date)
utc = replicate(utc, 1800)
utc.time = utc.time + hour*3.6d6
check_int_time, utc
;
utplot_image, reverse(arrayab,2), utc, -reverse(alog10(freqlo)), $
  position=[.1,.5,.9,.78],tick_unit=tick_unit,$
  xstyle=5,ystyle=5,/noscale,ytitle=funit,ticklen=-.01,/noerase,yaxis=1, $
  xtitle=''
!y.crange=-!y.crange
axis, xaxis=1, xstyle=1, ticklen=-0.01, xtickformat='blankticks'
axis, yaxis=0, ystyle=1, /ylog, ticklen=-0.01, ytitle=funit
axis, yaxis=1, ystyle=1, /ylog, ticklen=-0.01, ytickformat='blankticks'
red = ncolors
linecolor, red, 'red'
xyouts,.915,.62,'Ahead',orientation=270,/normal,alignment=0.5,color=red
;
utplot_image, arraybb, utc, alog10(freqlo), position=[.1,.2,.9,.48],$
  xstyle=9,ystyle=5,/noscale,ytitle=funit,ticklen=-.01,/noerase, $
  xtitle='Time of day',tick_unit=tick_unit
axis, yaxis=0, ystyle=1, /ylog, ticklen=-0.01, ytitle=funit
axis, yaxis=1, ystyle=1, /ylog, ticklen=-0.01, ytickformat='blankticks'
blue = ncolors+1
linecolor, blue, 'blue'
xyouts,.915,.34,'Behind',orientation=270,/normal,alignment=0.5,color=blue
;
end
