;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_MAKE_EUVI_FULLRES
;
; Purpose     :	Wrapper around SSC_EUVI_SYNOPTIC for full resolution data
;
; Category    :	STEREO, SECCHI, Coordinates
;
; Explanation :	This routine calls SSC_EUVI_SYNOPTIC to create the files
;               euvi_171_fullres.gif and other wavelengths in the beacon
;               directory.
;
; Syntax      :	SSC_MAKE_EUVI_FULLRES
;
; Examples    :	SSC_MAKE_EUVI_FULLRES
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	Creates $STEREO_BEACON_HTML/euvi_171_fullres.gif, and the same
;               for 195, 284, and 304.  Also creates .txt files.
;
; Opt. Outputs:	None.
;
; Keywords    :	SINGLE_SC= If set, then allow map to be built up from a single
;                         STEREO spacecraft.  This option is intended for the
;                         period when one or both spacecraft is behind the Sun.
;
;               Allows VSO keywords to be passed, such as SITE='SAO'
;
; Env. Vars.  : UPDATE_BLDG21_BEACON = Script to update bldg21 copy
;
; Calls       :	SETPLOT, SSC_EUVI_SYNOPTIC, TVREAD, CONCAT_DIR, WRITE_GIF
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 07-Jan-2010, William Thompson, GSFC
;               Version 2, 01-Feb-2010, WTT, make 1024x512
;               Version 3, 02-Feb-2010, WTT, add /BACKGRID
;               Version 4, 03-Mar-2010, WTT, copy files to building 21
;               Version 5, 04-Mar-2010, WTT, use script (rsync) instead of scp
;               Version 6, 06-Jan-2011, WTT, pass through VSO keywords
;               Version 7, 21-Apr-2014, WTT, added keyword SINGLE_SC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_make_euvi_fullres, single_sc=single_sc, _extra=_extra
;
;  Look for simultaneous image pairs from both spacecraft over the last five
;  days.
;
get_utc, utc
utc = replicate(utc,2)
utc[0].mjd = utc[0].mjd - 5
;
errmsg = ''
cata = scc_read_summary(date=utc, spacecraft='a', telescope='euvi', $
                        errmsg=errmsg)
if errmsg ne '' then begin
    ncata = 0
    if not keyword_set(single_sc) then return
end else ncata = n_elements(cata)
;
errmsg = ''
catb = scc_read_summary(date=utc, spacecraft='b', telescope='euvi', $
                        errmsg=errmsg)
if errmsg ne '' then begin
    ncatb = 0
    if (not keyword_set(single_sc)) or (ncata eq 0) then return
end else ncatb = n_elements(catb)
;
;  Look for matching filenames.
;
if (ncata gt 0) and (ncatb gt 0) then begin
    break_file, cata.filename, diska, dira, namea
    break_file, catb.filename, diskb, dirb, nameb
    namea = strmid(namea,0,15)
    nameb = strmid(nameb,0,15)
    match, namea, nameb, wa, wb, count=count
    if count eq 0 then return
    cata = cata[wa]
    catb = catb[wb]
end else if ncata eq 0 then cata = catb else catb = cata
;
;  Make sure the wavelengths are identical.
;
w = where(cata.value eq catb.value, count)
if count eq 0 then return
cata = cata[w]
catb = catb[w]
;
;  Collect the times for each wavelength.
;
w = where(cata.value eq 171)
t171a = reverse(utc2tai(cata[w].date_obs))
t171b = reverse(utc2tai(catb[w].date_obs))
w = where(cata.value eq 195)
t195a = reverse(utc2tai(cata[w].date_obs))
t195b = reverse(utc2tai(catb[w].date_obs))
w = where(cata.value eq 284)
t284a = reverse(utc2tai(cata[w].date_obs))
t284b = reverse(utc2tai(catb[w].date_obs))
w = where(cata.value eq 304)
t304a = reverse(utc2tai(cata[w].date_obs))
t304b = reverse(utc2tai(catb[w].date_obs))
;
;  Step through the 284 times until a match is found with other instruments
;  within 10 minutes.
;
for i=0,n_elements(t284a)-1 do begin
    tai = [t284a[i], t284b[i]]
;
    d171 = abs(t171a-t284a[i])
    w171 = where(d171 eq min(d171))
    d171 = d171[w171]
    tai = [tai, t171a[w171], t171b[w171]]
;
    d195 = abs(t195a-t284a[i])
    w195 = where(d195 eq min(d195))
    d195 = d195[w195]
    tai = [tai, t195a[w195], t195b[w195]]
;
    d304 = abs(t304a-t284a[i])
    w304 = where(d304 eq min(d304))
    d304 = d304[w304]
    tai = [tai, t304a[w304], t304b[w304]]
;
    if (d171 le 600) and (d195 le 600) and (d304 le 600) then begin
        dates = tai2utc([min(tai)-1, max(tai)+1])
        goto, generate_images
    endif
endfor
return                          ;No matches found
;
;  Use the Z buffer.
;
generate_images:
dname = !d.name
setplot, 'z'
!p.color = !d.table_size - 1
!p.background = 0
;
;  Generate images for each wavelength, based on the full resolution data.
;
device, set_resolution=[1024,512]
wave = [171,195,284,304]
for iwave=0,3 do begin
    errmsg = ''
    help, wave[iwave]
    hprint, utc2str(dates)
    ssc_euvi_synoptic, /color, npixels=[1024,512], date_obs=date_obs, $
      wavelength=wave[iwave], dates=dates, /backgrid, errmsg=errmsg, $
      single_sc=single_sc, _extra=_extra
    if errmsg eq '' then begin
        image = tvread(red, green, blue)
        filename = 'euvi_' + ntrim(wave[iwave]) + '_fullres'
        filename = concat_dir('$STEREO_BEACON_HTML', filename)
        write_gif, filename+'.gif', image, red, green, blue
        openw, unit, filename+'.txt', /get_lun
        printf, unit, anytim2utc(date_obs, /vms)
        printf, unit, wave[iwave]
        free_lun, unit
    endif else print, errmsg
endfor
;
;  Copy the images to the public website.
;
update_bldg21_beacon = getenv('UPDATE_BLDG21_BEACON')
if update_bldg21_beacon ne '' then spawn, update_bldg21_beacon
;
setplot, dname
;
end
