;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_OPEN_FILES
;
; Purpose     :	Open the IMPACT, PLASTIC and SWAVES beacon telemetry files
;
; Category    :	STEREO, Telemetry
;
; Explanation :	This procedure is called by SSC_BEACON_READER to open the
;               most recent STEREO beacon telemetry files for the IMPACT,
;               PLASTIC, and SWAVES instruments.  Files are opened for both the
;               Ahead and Behind spacecraft.
;
; Syntax      :	SSC_BEACON_OPEN_FILES, IMPACTLUN, PLASTICLUN, SWAVESLUN
;
; Examples    :	See SSC_BEACON_READER.
;
; Inputs      :	None.
;
; Opt. Inputs :	None.
;
; Outputs     :	IMPACTLUN, PLASTICLUN, SWAVESLUN
;                       Each is a two-element array containing the logical unit
;                       numbers of the Ahead and Behind files for each
;                       instrument.
;
; Opt. Outputs:	None.
;
; Keywords    :	TEST    = If set, then files from the beginning of March 2006
;                         are opened, instead of the most recent.
;
; Calls       :	GET_UTC, CONCAT_DIR
;
; Common      :	None.
;
; Env. Vars.  : STEREO_SWX_TELEM points to the top of the spaceweather
;      	      	telemetry tree. Under this, the routine looks for "ahead" and
;               "behind", followed by the year and month.
;
; Restrictions:	Requires the environment variable STEREO_SWX_TELEM to be
;               defined.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 08-Feb-2006, William Thompson, GSFC
;		Version 2, 07-Dec-2006, William Thompson, GSFC
;			Use STEREO_SWX_TELEM instead of STEREO_MOC_TELEM
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_open_files, impactlun, plasticlun, swaveslun, test=test
on_error, 2
;
;  Check the calling sequence.
;
if n_params() ne 3 then message, $
  'Syntax: SSC_BEACON_OPEN_FILES, IMPACTLUN, PLASTICLUN, SWAVESLUN'
;
;  First close any files that are already open, and initialize the arrays.
;
if n_elements(impactlun) eq 2 then begin
    if impactlun[0] gt 0 then free_lun, impactlun[0]
    if impactlun[1] gt 0 then free_lun, impactlun[1]
endif
impactlun = lonarr(2)
;
if n_elements(plasticlun) eq 2 then begin
    if plasticlun[0] gt 0 then free_lun, plasticlun[0]
    if plasticlun[1] gt 0 then free_lun, plasticlun[1]
endif
plasticlun = lonarr(2)
;
if n_elements(swaveslun) eq 2 then begin
    if swaveslun[0] gt 0 then free_lun, swaveslun[0]
    if swaveslun[1] gt 0 then free_lun, swaveslun[1]
endif
swaveslun = lonarr(2)
;
;  Get today's date, and extract the year and month.
;
if keyword_set(test) then begin
    year = 2006
    month = 3
end else begin
    get_utc, utc, /external
    year  = utc.year
    month = utc.month
endelse
spacecraft = ['ahead','behind']
;
;  Find the most recent instrument files for each spacecraft.
;
while min(impactlun*plasticlun*swaveslun) eq 0 do begin
    for isc=0,1 do begin
        path = concat_dir('$STEREO_SWX_TELEM', spacecraft[isc])
        path = concat_dir(path, string(year, format='(I4.4)'))
        path = concat_dir(path, string(month,format='(I2.2)'))
;
        if impactlun[isc] eq 0 then begin
            files = file_search(concat_dir(path,'impact*.sw.ptp'), count=count)
            if count gt 0 then begin
                s = sort(files)
                if not keyword_set(test) then s = reverse(s)
                file = files[s[0]]
                openr, unit, file, /get_lun
                print, 'Opened file ' + file
                impactlun[isc] = unit
            endif
        endif
;
        if plasticlun[isc] eq 0 then begin
            files = file_search(concat_dir(path,'plastc*.sw.ptp'), count=count)
            if count gt 0 then begin
                s = sort(files)
                if not keyword_set(test) then s = reverse(s)
                file = files[s[0]]
                openr, unit, file, /get_lun
                print, 'Opened file ' + file
                plasticlun[isc] = unit
            endif
        endif
;
        if swaveslun[isc] eq 0 then begin
            files = file_search(concat_dir(path,'swaves*.sw.ptp'), count=count)
            if count gt 0 then begin
                s = sort(files)
                if not keyword_set(test) then s = reverse(s)
                file = files[s[0]]
                openr, unit, file, /get_lun
                print, 'Opened file ' + file
                swaveslun[isc] = unit
            endif
        endif
    endfor
;
;  Decrement the month, and keep going.
;
    month = month-1
    if month eq 0 then begin
        year = year - 1
        month = 12
        if year lt 2006 then message, 'Not all files found'
    endif
endwhile
;
return
end
