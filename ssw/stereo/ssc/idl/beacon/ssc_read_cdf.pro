;+
; Project     :	STEREO - IMPACT, PLASTIC
;
; Name        : SSC_READ_CDF
;
; Purpose     : Read IMPACT and PLASTIC CDF files
;
; Category    :	STEREO, IMPACT, PLASTIC
;
; Explanation :	Reads parameters from a series of IMPACT or PLASTIC CDF files.
;
; Syntax      :	SSC_READ_CDF, CDFNAME, NAME, VARIABLE
;                       [ , EPOCH5  [,EPOCH1, VAR5, NUM5 ]]
;
; Examples    :	REREAD = 0
;               SSC_READ_CDF, cdfname, 'Epoch', epoch, /EPOCH, REREAD=REREAD, $
;                       epoch5
;               SSC_READ_CDF, cdfname, 'SFLETFe', sfletfe, REREAD=REREAD, $
;                       epoch5, epoch, sfletfe5, sfletfe_n5
;
; Inputs      :	CDFNAME = List of CDF filenames.  Null filenames are ignored.
;               NAME    = The name of the variable to read in.
;
; Opt. Inputs :	EPOCH5  = The epoch divided by 5 minutes (300000 milliseconds),
;                         sorted and with duplicates removed.  If /EPOCH is
;                         set, then this is actually an output parameter, and
;                         the EPOCH1, VAR5, and NUM5 parameters are ignored.
;
;               EPOCH1  = The epoch previously read in for this variable.  This
;                         is compared against EPOCH5 to calculate the five
;                         minute average data.
;
; Outputs     :	VARIABLE= The variable to be read in.  If already existing,
;                         and REREAD is not set, then new data are appended to
;                         this array.
;
; Opt. Outputs:	VAR5    = The variable summed over 5 minute intervals.  If the
;                         EPOCH keyword is set, then this is simply the epoch
;                         divided by 5 minutes (300000 milliseconds).
;
;               NUM5    = The number of variables making up VAR5.  The ratio
;                         VAR5/NUM5 is the 5-minute average.
;
;               If already existing, and REREAD is not set, then new data are
;               added to the existing VAR5 and NUM5 arrays.
;
; Keywords    :	REREAD  = If set, then reread the files.  Otherwise, only new
;                         data is appended to the existing array.
;
;               EPOCH   = If set, then check the first and last times in each
;                         file against the parameters in the existing array.
;                         If these differ, then set REREAD=1 and reread the
;                         files.  One should read the epoch parameter first,
;                         and use that to set the REREAD keyword for the
;                         associated parameters.
;
;               DIM     = Array giving the dimensions of a single record, e.g.
;                         DIM=3 for MAGBField, DIM=[2,6,2] for SWEAPAD.  The
;                         default is DIM=1.
;
;               MAGNITUDE = If set, then the data are reprocessed into a
;                           3D vector magnitude.  Requires DIM=3.
;
;               VERSION = The minimum version to use when reading in the data.
;
;               RPARAM  = A mechanism to preserve read parameters between calls
;                         when reading in the files one at a time.
;
; Calls       :	DELVARX, BREAK_FILE, VALID_NUM, ALL_VALS
;
; Common      : None
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 26-June-2007, William Thompson, GSFC
;               Version 2, 28-June-2007, William Thompson, GSFC
;                       Fixed bug appending data to 5-minute averages.
;               Version 3, 05-Feb-2008, WTT, fix bug going past end of EPOCH1
;               Version 4, 03-Sep-2008, WTT, added /MAGNITUDE
;               Version 5, 05-Oct-2008, WTT, added RPARAM to fix bug
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_read_cdf, cdfname, name, variable, epoch5, epoch1, var5, num5, $
                  reread=reread, epoch=epoch, dim=dim, version=version, $
                  magnitude=k_magnitude, rparam=rparam
;
;  Starting point.  If rereading from scratch, initialize the appropriate
;  variables.
;
RESTART:
if n_elements(variable) eq 0 then reread = 1
if keyword_set(reread) then begin
    delvarx, variable, var5, num5
    if keyword_set(epoch) then delvarx, epoch5
endif
;
;  Characterize an individual record, based on the DIM keyword.
;
ndim = n_elements(dim)          ;Number of dimensions of each record
if ndim eq 0 then rsize=1 else rsize=round(product(dim)) ;Size of each record
magnitude = keyword_set(k_magnitude) and (rsize eq 3)
if magnitude then begin
    ndim = 0
    rsize = 1
endif
;
;  Set up the initial parameters controlling the read.
;
if n_elements(rparam) eq 3 then begin
    nread  = rparam[0]
    nfirst = rparam[1]
    nleft  = rparam[2]
end else begin
    nread = 0                   ;Number of new records read in
    nfirst = 0                  ;Index of first record for current file
    nleft = n_elements(variable) / rsize
                                ;Number of old records left to compare
endelse
;
;  If EPOCH5 is bigger than NUM5, then expand VAR5 and NUM5 to accommodate the
;  new data.
;
nn = n_elements(epoch5)
nnum5 = n_elements(num5)
if (nn gt nnum5) and (nnum5 gt 0) then begin
    temp = num5
    num5 = lonarr(nn)
    num5[0] = temp
    if rsize eq 1 then begin
        temp = var5
        var5 = dblarr(nn)
        var5[0] = temp
    end else begin
        temp = var5
        var5 = dblarr([dim,nn])
        case ndim of
            1: var5[0,0] = temp
            2: var5[0,0,0] = temp
            3: var5[0,0,0,0] = temp
            4: var5[0,0,0,0,0] = temp
            5: var5[0,0,0,0,0,0] = temp
        endcase
    endelse
endif
;
;  Step through the files and read in any data which hasn't been read in yet.
;
for ifile = 0,n_elements(cdfname)-1 do begin
    cdfname0 = cdfname[ifile]
;
;  If the VERSION keyword was passed, then make sure that the version number
;  agrees.
;
    if n_elements(version) eq 1 then begin
        break_file, cdfname0, disk0, dir0, name0
        ver0 = strmid(name0, strlen(name0)-2, 2)
        if valid_num(ver0) then $
          if fix(ver0) lt version then cdfname0 = ''
    endif
;
    if cdfname0 ne '' then begin
        cdfid = cdf_open(cdfname[ifile])
;
;  Get the number of records in the file.
;
        quiet = !quiet  &  !quiet = 1
        cdf_control, cdfid, variable=name, get_var_info=info
        !quiet = quiet
        numrec = info.maxrec + 1
;
;  If the EPOCH keyword is set, then check the first and last times against
;  those which were previously read in.  If they don't match, then set the
;  REREAD keyword and read all the files.
;
        if keyword_set(epoch) and (not keyword_set(reread)) then begin
            nlast = (nleft < numrec) - 1
            if (nlast ge 0) then begin
                cdf_varget, cdfid, name, var0, rec_start=0, rec_count=1
                cdf_varget, cdfid, name, var1, rec_start=nlast, rec_count=1
                if (variable[nfirst] ne var0) or $
                  (variable[nlast+nfirst] ne var1) then begin
                    reread = 1
                    cdf_close, cdfid
                    goto, RESTART
                endif
            endif
        endif
;
;  Read in any additional parameters added since the last time this file was
;  read.
;
        if numrec gt nleft then begin
            cdf_varget, cdfid, name, var0, rec_start=nleft, $
              rec_count=numrec-nleft
            var0 = reform(var0, /overwrite)
            if keyword_set(magnitude) then var0 = sqrt(total(var0^2,1))
            if n_elements(variable) eq 0 then variable = var0 else begin
                if rsize eq 1 then variable = [variable, var0] else $
                  variable = [[variable], [var0]]
            endelse
            nread = nread + numrec - nleft
;
;  If requested, calculate the parameters for the 5 minute averages from VAR0
;
            if (n_elements(epoch5) gt 0) and (n_params() eq 7) and $
              (not keyword_set(epoch)) then begin
                if n_elements(var5) eq 0 then begin
                    nn = n_elements(epoch5)
                    if rsize eq 1 then var5 = dblarr(nn) else $
                      var5 = dblarr([dim,nn])
                    num5 = lonarr(nn)
                endif
                i0 = nfirst + nleft
                i1 = ((nfirst + numrec) < n_elements(epoch1)) - 1
                epoch0 = round(epoch1[i0:i1]/300d3)
                e0 = all_vals(epoch0)
                for i=0,n_elements(e0)-1 do begin
                    w = where(epoch5 eq e0[i], count)
                    if count eq 1 then begin
                        ww = where(epoch0 eq e0[i], ccount)
                        if ndim eq 0 then vtot = total(var0[ww]) else begin
                            sz = size(var0)
                            if sz[0] eq ndim then vtot=var0 else begin
                                case ndim of
                                    1: vtot = var0[*,ww]
                                    2: vtot = var0[*,*,ww]
                                    3: vtot = var0[*,*,*,ww]
                                    4: vtot = var0[*,*,*,*,ww]
                                    5: vtot = var0[*,*,*,*,*,ww]
                                endcase
                                sz = size(vtot)
                                if sz[0] gt ndim then vtot = total(vtot,ndim+1)
                            endelse
                        endelse
                        case ndim of
                            0: var5[w]           = var5[w]           + vtot
                            1: var5[*,w]         = var5[*,w]         + vtot
                            2: var5[*,*,w]       = var5[*,*,w]       + vtot
                            3: var5[*,*,*,w]     = var5[*,*,*,w]     + vtot
                            4: var5[*,*,*,*,w]   = var5[*,*,*,*,w]   + vtot
                            5: var5[*,*,*,*,*,w] = var5[*,*,*,*,*,w] + vtot
                        endcase
                        num5[w] = num5[w] + ccount
                    endif
                endfor
            endif
;
;  Reset NLEFT to account for the additional data read in.
;
            nleft = numrec
        endif
;
;  Update the parameters NFIRST and NLEFT, and move to the next file.
;
        cdf_close, cdfid
        nfirst = nfirst + numrec
        nleft = nleft - numrec
    endif
endfor
;
;  If the /EPOCH keyword was set, and the EPOCH5 parameter was passed, then
;  calculate EPOCH5.
;
if keyword_set(epoch) and (n_params() gt 3) and (nread gt 0) then $
  epoch5 = all_vals(round(variable/300d3))
;
;  Preserve the parameters controlling the read.
;
rparam = [nread, nfirst, nleft]
;
return
end
