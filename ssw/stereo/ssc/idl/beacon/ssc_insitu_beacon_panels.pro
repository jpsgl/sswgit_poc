;+
; Project     :	STEREO - IMPACT, PLASTIC
;
; Name        : SSC_INSITU_BEACON_PANELS
;
; Purpose     : Plot STEREO in-situ space weather beacon data
;
; Category    :	STEREO, IMPACT, PLASTIC
;
; Explanation :	Plots STEREO in-situ space weather beacon data, for display on
;               the web.
;
; Syntax      :	SSC_INSITU_BEACON_PANELS, IMPACT_A_CDFNAME, IMPACT_B_CDFNAME, $
;                       PLASTIC_A_CDFNAME, PLASTIC_B_CDFNAME
;
; Examples    :	See SSC_BEACON_PLOTS
;
; Inputs      :	IMPACT_A_CDFNAME  = List of IMPACT Beacon CDF filenames for the
;                                   Ahead spacecraft.  Null filenames are
;                                   ignored.
;               IMPACT_B_CDFNAME  = Same for the Behind spacecraft
;               PLASTIC_A_CDFNAME = Same for PLASTIC on Ahead spacecraft
;               PLASTIC_B_CDFNAME = Same for PLASTIC on Behind spacecraft
;
; Opt. Inputs :	None.
;
; Outputs     :	None.
;
; Opt. Outputs:	None.
;
; Keywords    :	TIMERANGE = Time range to plot.  If not passed, then determined
;                           from the data.
;
;               TICK_UNIT = Number of seconds between tick marks,
;                           e.g. TICK_UNIT=21600 for six hours.
;
;               TITLE     = Plot title.  If not passed, then generated
;                           automatically from the end time.
;
;               REALTIME  = Signal passed through to SSC_GET_ACEDATA to use
;                           realtime processing.
;
;               SMOOTH    = Time in seconds to smooth the data.
;
; Calls       :	SSC_INSITU_BEACON_READ, DELVARX, CDF2UTC, LINECOLOR, CDF2TAI,
;               ANYTIM2TAI, TAI2UTC, GOOD_PIXELS, SSC_BEACON_PANEL, OUTPLOT,
;               SETVIEW, SSC_GET_ACEDATA
;
; Common      : Uses common block SSC_INSITU_BEACON from ssc_insitu_beacon.cmn
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	Based on STEREO_INSITU_BEACON_PANELS, 06-Feb-2006,
;                       Peter Schroeder, UC-Berkeley.
;
; History     :	Version 1, 10-Feb-2006, William Thompson, GSFC
;                       Rewrote to allow multiple files, both A & B spacecraft.
;               Version 2, 13-Jun-2006, William Thompson, GSFC
;                       Added keyword TITLE.
;               Version 3, 10-Jan-2007, William Thompson, GSFC
;                       Changed "Velocity" to "Velocity_HGRTN".
;                       Put in realistic limits for density and velocity
;               Version 4, 30-Jan-2007, William Thompson, GSFC
;                       Fixed bug with density scaling
;               Version 5, 2-Feb-2007, WTT, plot velocity magnitude only
;               Version 6, 26-Jun-2007, William Thompson, GSFC
;                       Use SSC_INSITU_BEACON_READ, plot total B,
;                       label B as being in RTN, use ZEROCOLOR for B
;               Version 7, 18-Jul-2007, WTT, use SIGRANGE for better scaling
;               Version 8, 20-Sep-2007, WTT, corrected velocity ranging bug
;               Version 9, 31-Jan-2008, WTT, added magnetic lon/lat
;               Version 10, 5-Feb-2008, WTT, fixed ASIN vs. ATAN bug
;               Version 11, 11-Aug-2008, WTT, added ACE data.
;               Version 12, 3-Sep-2008, WTT, plot ion temperature
;               Version 13, 13-May-2009, WTT, fixed bug when no PLASTIC data
;               Version 14, 07-Oct-2009, WTT, removed fudge factors for
;                       temperature--now applied in SSC_INSITU_BEACON_READ
;               Version 15, 06-Apr-2010, WTT, better autoranging of velocity
;                       Use range of [1E4,1E7] for temperature
;               Version 16, 30-Jun-2010, WTT, added keyword SMOOTH.
;               Version 17, 01-Jul-2010, WTT, force EPOCH > launch
;               Version 18, 12-Jan-2011, WTT, fix bug when few points
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_insitu_beacon_panels, impact_a_cdfname, impact_b_cdfname, $
                              plastic_a_cdfname, plastic_b_cdfname, $
                              timerange=timerange, tick_unit=tick_unit, $
                              title=k_title, realtime=realtime, smooth=smooth
@ssc_insitu_beacon.cmn
;
;  Define the CDF Epoch corresponding to launch.
;
epoch_launch = anytim2cdf('26-Oct-2006 00:52')
;
;  Read in the data.
;
ssc_insitu_beacon_read, impact_a_cdfname, impact_b_cdfname, $
  plastic_a_cdfname, plastic_b_cdfname
;
;  Get the corresponding ACE data.
;
if n_elements(timerange) ne 2 then begin
    swepam = -1
    mag    = -1
end else ssc_get_acedata, timerange, swepam, mag, realtime=realtime
;
;  Set up some plotting constants.
;
charsize=!p.charsize
if charsize eq 0 then charsize=1
ysize = charsize*!d.y_ch_size
;
;  Convert the various epoch data into UTC.  Initialize EPOCH, and start
;  accumulating all the epoch data arrays.
;
delvarx, epoch
if n_elements(epoch_mag_a) gt 0 then begin
    epoch = epoch_mag_a
    epoch_mag_a_utc = cdf2utc(epoch_mag_a > epoch_launch)
endif
;
if n_elements(epoch_mag_b) gt 0 then begin
    if n_elements(epoch) eq 0 then epoch = epoch_mag_b else $
      epoch = [epoch, epoch_mag_b]
    epoch_mag_b_utc = cdf2utc(epoch_mag_b > epoch_launch)
endif
;
if n_elements(epoch_plastic_a) gt 0 then begin
    if n_elements(epoch) eq 0 then epoch = epoch_plastic_a else $
      epoch = [epoch, epoch_plastic_a]
    epoch_plastic_a_utc = cdf2utc(epoch_plastic_a > epoch_launch)
endif
;
if n_elements(epoch_plastic_b) gt 0 then begin
    if n_elements(epoch) eq 0 then epoch = epoch_plastic_b else $
      epoch = [epoch, epoch_plastic_b]
    epoch_plastic_b_utc = cdf2utc(epoch_plastic_b > epoch_launch)
endif
;
;  If no files were read, then simply return.
;
if n_elements(epoch) eq 0 then return
;
;  Derive a time range to apply to all the plots.
;
if n_elements(timerange) ne 2 then $
  timerange = cdf2utc([min(epoch,max=emax), emax] > epoch_launch)
;
;  If requested, smooth the data.
;
if n_elements(magbfield_a) gt 0 then begin
    mag_a = magbfield_a
    if keyword_set(smooth) then for i=0,2 do $
      mag_a[i,*] = utc_bin_smooth(epoch_mag_a_utc, magbfield_a[i,*], smooth)
endif
if n_elements(magbfield_b) gt 0 then begin
    mag_b = magbfield_b
    if keyword_set(smooth) then for i=0,2 do $
      mag_b[i,*] = utc_bin_smooth(epoch_mag_b_utc, magbfield_b[i,*], smooth)
endif
if n_elements(velocity_a) gt 0 then begin
    vel_a = velocity_a
    if keyword_set(smooth) then for i=0,3 do $
      vel_a[i,*] = utc_bin_smooth(epoch_plastic_a_utc, vel_a[i,*], smooth)
endif
if n_elements(velocity_b) gt 0 then begin
    vel_b = velocity_b
    if keyword_set(smooth) then for i=0,3 do $
      vel_b[i,*] = utc_bin_smooth(epoch_plastic_b_utc, vel_b[i,*], smooth)
endif
;
;  Set up for plotting.
;
erase
loadct, 0
linecolor,1,'red'
linecolor,2,'blue'
linecolor,3,'brown'
grey = !d.table_size / 2
xtickname = replicate(' ',30)
;
; Set the plot title.
;
if n_elements(k_title) eq 1 then title=k_title else begin
    lasttime = cdf2tai(max(epoch)) < anytim2tai(timerange[1])
    lasttime = tai2utc(lasttime,/vms,/truncate)
    lasttime = strmid(lasttime,0,strlen(lasttime)-3) ;Remove seconds
    title = 'IMPACT/PLASTIC  ' + lasttime
endelse
;
;  Define a plot range for magnetic field data.  Use the SIGRANGE() function,
;  and adjust by 10% upwards and downwards.
;
ymin = 0  &  ymin0 = 0
ymax = 1  &  ymax0 = 1
if n_elements(mag_a) gt 0 then begin
    amax = max(sigrange(good_pixels(mag_a)), min=amin)
    ymin = ymin < amin
    ymax = ymax > amax
    amax0 = max(good_pixels(mag_a), min=amin0)
    ymin0 = ymin0 < amin0
    ymax0 = ymax0 > amax0
endif
if n_elements(mag_b) gt 0 then begin
    bmax = max(sigrange(good_pixels(mag_b)), min=bmin)
    ymin = ymin < bmin
    ymax = ymax > bmax
    bmax0 = max(good_pixels(mag_b), min=bmin0)
    ymin0 = ymin0 < bmin0
    ymax0 = ymax0 > bmax0
endif
delta = ymax - ymin
ymax = (ymax + 0.1*delta) < ymax0
ymin = (ymin - 0.1*delta) > ymin0
;
;  Plot the magnetic field vector components.
;
n_panels = 9
setview,1.075,1.075,1,n_panels,0,0
ssc_beacon_panel, timerange, [ymin, ymax], ytitle = 'B!LX!N(R)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, title=title, $
  zerocolor=grey
if n_elements(mag) gt 1 then begin
    mag_bx = mag.bx
    if keyword_set(smooth) then mag_bx = utc_bin_smooth(mag, mag_bx, smooth)
    outplot, mag, mag_bx, color=3, psym=3
endif
if n_elements(mag_a) gt 0 then outplot, epoch_mag_a_utc, $
  mag_a[0,*], color=1, psym=3
if n_elements(mag_b) gt 0 then outplot, epoch_mag_b_utc, $
  mag_b[0,*], color=2, psym=3
;
setview,1.075,1.075,2,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'B!LY!N(T)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, zerocolor=grey
ywindow = !y.window * !d.y_size
if n_elements(mag) gt 1 then begin
    mag_by = mag.by
    if keyword_set(smooth) then mag_by = utc_bin_smooth(mag, mag_by, smooth)
    outplot, mag, mag_by, color=3, psym=3
endif
if n_elements(mag_a) gt 0 then outplot, epoch_mag_a_utc, $
  mag_a[1,*], color=1, psym=3
if n_elements(mag_b) gt 0 then outplot, epoch_mag_b_utc, $
  mag_b[1,*], color=2, psym=3
;
xyouts, 2*ysize, ywindow[0], /device, '(nanoTesla, RTN)', alignment=0.5, $
  orientation=90, charsize=charsize
;
setview,1.075,1.075,3,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'B!LZ!N(N)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, zerocolor=grey
if n_elements(mag) gt 1 then begin
    mag_bz = mag.bz
    if keyword_set(smooth) then mag_bz = utc_bin_smooth(mag, mag_bz, smooth)
    outplot, mag, mag_bz, color=3, psym=3
endif
if n_elements(mag_a) gt 0 then outplot, epoch_mag_a_utc, $
  mag_a[2,*], color=1, psym=3
if n_elements(mag_b) gt 0 then outplot, epoch_mag_b_utc, $
  mag_b[2,*], color=2, psym=3
;
;  Define a plot range for total magnetic field data.  Use the SIGRANGE()
;  function, and adjust by 10%
;
ymin = 0
ymax = 1  &  ymax0 = 1
if n_elements(mag_a) gt 0 then begin
    magbtot_a = sqrt(total(mag_a^2,1))
    amax = max(sigrange(good_pixels(magbtot_a)))
    ymax = ymax > amax
    amax0 = max(good_pixels(magbtot_a))
    ymax0 = ymax0 > amax0
endif
if n_elements(mag_b) gt 0 then begin
    magbtot_b = sqrt(total(mag_b^2,1))
    bmax = max(sigrange(good_pixels(magbtot_b)))
    ymax = ymax > bmax
    bmax0 = max(good_pixels(magbtot_b))
    ymax0 = ymax0 > bmax0
endif
ymax = (ymax * 1.1) < ymax0
;
setview,1.075,1.075,4,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'total B', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit
if n_elements(mag) gt 1 then begin
    mag_bt = mag.bt
    if keyword_set(smooth) then mag_bt = utc_bin_smooth(mag, mag_bt, smooth)
    outplot, mag, mag_bt, color=3, psym=3
endif
if n_elements(mag_a) gt 0 then outplot, epoch_mag_a_utc, magbtot_a, $
  color=1, psym=3
if n_elements(mag_b) gt 0 then outplot, epoch_mag_b_utc, magbtot_b, $
  color=2, psym=3
;
;  Plot the magnetic latitude and longitude.
;
radeg = 180.d0 / !dpi
setview,1.075,1.075,5,n_panels,0,0
ssc_beacon_panel, timerange, [-90,90], ytitle='!4h!3!LB!N', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, ystyle=1, $
  ytickv=[-90,-45,0,45]
if n_elements(mag) gt 1 then begin
    mag_lat = mag.lat
    if keyword_set(smooth) then mag_lat = utc_bin_smooth(mag, mag_lat, smooth)
    outplot, mag, mag_lat, color=3, psym=3
endif
if n_elements(mag_a) gt 0 then begin
    lat = asin(mag_a[2,*]/magbtot_a) * radeg
    outplot, epoch_mag_a_utc, lat, color=1, psym=3
endif
if n_elements(mag_b) gt 0 then begin
    lat = asin(mag_b[2,*]/magbtot_b) * radeg
    outplot, epoch_mag_b_utc, lat, color=2, psym=3
endif
;
setview,1.075,1.075,6,n_panels,0,0
ssc_beacon_panel, timerange, [0,360], ytitle='!4u!3!LB!N', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, ystyle=1, $
  ytickv=[0,90,180,270]
if n_elements(mag) gt 1 then begin
    mag_long = mag.long
    if keyword_set(smooth) then mag_long = utc_bin_smooth(mag, mag_long, smooth)
    outplot, mag, mag_long, color=3, psym=3
endif
ywindow = !y.window * !d.y_size
if n_elements(mag_a) gt 0 then begin
    lon = atan(mag_a[1,*], mag_a[0,*]) * radeg
    w = where(lon lt 0, count)
    if count gt 0 then lon[w] = lon[w] + 360
    outplot, epoch_mag_a_utc, lon, color=1, psym=3
endif
if n_elements(mag_b) gt 0 then begin
    lon = atan(mag_b[1,*], mag_b[0,*]) * radeg
    w = where(lon lt 0, count)
    if count gt 0 then lon[w] = lon[w] + 360
    outplot, epoch_mag_b_utc, lon, color=2, psym=3
endif
;
xyouts, 2*ysize, ywindow[0], /device, 'Brown: ACE equivalents', alignment=0.5,$
  orientation=90, charsize=charsize, color=3
;
;  Process the PLASTIC 5-minute averaged data.
;
if n_elements(epoch_plastic_5a) gt 0 then begin
    utc_plastic_5a = cdf2utc((epoch_plastic_5a * 300d3) > epoch_launch)
    temp_5a = temperature_5a / (temperature_na > 1)
    if keyword_set(smooth) then temp_5a = $
      utc_bin_smooth(utc_plastic_5a, temp_5a, smooth)
endif
if n_elements(epoch_plastic_5b) gt 0 then begin
    utc_plastic_5b = cdf2utc((epoch_plastic_5b * 300d3) > epoch_launch)
    temp_5b = temperature_5b / (temperature_nb > 1)
    if keyword_set(smooth) then temp_5b = $
      utc_bin_smooth(utc_plastic_5b, temp_5b, smooth)
endif
;
;  Define a plot range for density data.  Use the SIGRANGE() function, and
;  adjust by 10% upwards and downwards.
;
ymin = 0.1  &  ymin0 = 0.1
ymax = 10   &  ymax0 = 10
if n_elements(density_a) gt 0 then begin
    w = where((density_a ge 0.1) and (density_a le 100), count)
    if count gt 0 then begin
        amax = max(sigrange(density_a[w]), min=amin)
        ymin = ymin < amin
        ymax = ymax > amax
        amax0 = max(density_a[w], min=amin0)
        ymin0 = ymin0 < amin0
        ymax0 = ymax0 > amax0
    endif
endif
if n_elements(density_b) gt 0 then begin
    w = where((density_b ge 0.1) and (density_b le 100), count)
    if count gt 0 then begin
        bmax = max(sigrange(density_b[w]), min=bmin)
        ymin = ymin < bmin
        ymax = ymax > bmax
        bmax0 = max(density_b[w], min=bmin0)
        ymin0 = ymin0 < bmin0
        ymax0 = ymax0 > bmax0
    endif
endif
delta = ymax - ymin
ymax = (ymax + 0.1*delta) < ymax0
ymin = (ymin - 0.1*delta) > ymin0
;
ymin = ymin > 0.1
ymax = ymax < 100
if ymin ge ymax then begin
    ymin = 0.1
    ymax = 100
endif
;
;  Plot the density data.
;
setview,1.075,1.075,7,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'N!LP!N (cm!U-3!N)', $
  xtickname=xtickname, xtitle=' ', tick_unit=tick_unit, /ylog, yminor=0
if n_elements(swepam) gt 1 then begin
    p_density = swepam.p_density
    if keyword_set(smooth) then p_density = $
      utc_bin_smooth(swepam, p_density, smooth)
    outplot, swepam, p_density, color=3, psym=3
endif
if n_elements(density_a) gt 0 then begin
    dens_a = density_a
    if keyword_set(smooth) then dens_a = $
      utc_bin_smooth(epoch_plastic_a_utc, dens_a, smooth)
    outplot, epoch_plastic_a_utc, dens_a, color=1, psym=3
endif
if n_elements(density_b) gt 0 then begin
    dens_b = density_b
    if keyword_set(smooth) then dens_b = $
      utc_bin_smooth(epoch_plastic_b_utc, dens_b, smooth)
    outplot, epoch_plastic_b_utc, dens_b, color=2, psym=3
endif
;
;  Define a plot range for velocity data.  Use the SIGRANGE() function, and
;  adjust by 20% upwards and downwards.
;
if n_elements(swepam) gt 1 then begin
    b_speed = swepam.b_speed
    if keyword_set(smooth) then b_speed = utc_bin_smooth(swepam, b_speed, smooth)
    temp = reform(b_speed)
    if n_elements(temp) ge 9 then temp = median(temp, 9)
    ymin = min(sigrange(temp), max=ymax)
    ymin0 = ymin
    ymax0 = ymax
end else begin
    ymin = 2000  &  ymin0 = 2000
    ymax = 1     &  ymax0 = 1
endelse
if n_elements(vel_a) gt 0 then begin
    temp = reform(vel_a[3,*])
    if n_elements(temp) ge 9 then temp = median(temp, 9)
    w = where((temp ge 0) and (temp le 2000), count)
    if count gt 0 then begin
        amax = max(sigrange(temp[w]), min=amin)
        ymin = ymin < amin
        ymax = ymax > amax
        amax0 = max(temp[w], min=amin0)
        ymin0 = ymin0 < amin0
        ymax0 = ymax0 > amax0
    endif
endif
if n_elements(vel_b) gt 0 then begin
    temp = reform(vel_b[3,*])
    if n_elements(temp) ge 9 then temp = median(temp, 9)
    w = where((temp ge 0) and (temp le 2000), count)
    if count gt 0 then begin
        bmax = max(sigrange(temp[w]), min=bmin)
        ymin = ymin < bmin
        ymax = ymax > bmax
        bmax0 = max(temp[w], min=bmin0)
        ymin0 = ymin0 < bmin0
        ymax0 = ymax0 > bmax0
    endif
endif
delta = ymax - ymin
ymax = (ymax + 0.2*delta) < ymax0
ymin = (ymin - 0.2*delta) > ymin0
;
ymin = ymin > 0
ymax = ymax < 2000
if ymin ge ymax then begin
    ymin = 0
    ymax = 2000
endif
;
;  Plot the velocity.
;
setview,1.075,1.075,8,n_panels,0,0
ssc_beacon_panel, timerange, [ymin,ymax], ytitle = 'V (km/s)', $
  tick_unit=tick_unit, xtitle=' ', yminor=0, xtickname=xtickname, /ynozero
if n_elements(swepam) gt 1 then outplot, swepam, b_speed, color=3, psym=3
if n_elements(vel_a) gt 0 then outplot, epoch_plastic_a_utc, $
  vel_a[3,*], color=1, psym=3
if n_elements(vel_b) gt 0 then outplot, epoch_plastic_b_utc, $
  vel_b[3,*], color=2, psym=3
;
;  Plot the temperature.
;
setview,1.075,1.075,9,n_panels,0,0
xtitle = 'Start: ' + anytim2utc(timerange[0],/vms,/date) + ' '
xtitle = xtitle + strmid(anytim2utc(timerange[0],/ccsds,/time),0,5) + ' UTC'
ssc_beacon_panel, timerange, [1e4,1e7], ytitle = 'Ion Temp', $
  tick_unit=tick_unit, xtitle=xtitle, /ylog
if n_elements(swepam) gt 1 then begin
    ion_temp = swepam.ion_temp
    if keyword_set(smooth) then ion_temp = $
      utc_bin_smooth(swepam, ion_temp, smooth)
    outplot, swepam, ion_temp, color=3, psym=3
endif
if n_elements(temp_5a) gt 0 then outplot, utc_plastic_5a, $
  temp_5a, color=1, psym=2, symsize=0.1
if n_elements(temp_5b) gt 0 then outplot, utc_plastic_5b, $
  temp_5b, color=2, psym=2, symsize=0.1
;
;  Put on labels for Ahead and Behind.
;
xwindow = !x.window * !d.x_size
xyouts, xwindow[1], ysize, /device, 'RED: Ahead', charsize=charsize, $
  color=1, alignment=1
xyouts, xwindow[0], ysize, /device, 'BLUE: Behind', charsize=charsize, $
  color=2, alignment=0
;
;  Empty the plot buffer, and reset the plot viewport.
;
empty
setview
;
return
end
