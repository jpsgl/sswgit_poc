;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_GET_DSCOVRDATA
;
; Purpose     :	Read in archived DSCOVR solar wind data in NetCDF format
;
; Category    :	STEREO, Telemetry
;
; Explanation : This routine complements the routines SSC_GET_ACEDATA and
;               SSC_GET_WINDDATA to read in archived DSCOVR daily solar wind
;               data files in NetCDF format that have been ingested via
;               SSC_MIRROR_DSCOVR_FILES.  The MAG data are converted from GSM
;               to Geocentric RTN coordinates.  All data are converted into the
;               format returned by SSC_GET_ACEDATA for backward compatibility.
;
; Syntax      :	SSC_GET_DSCOVRDATA, TIMERANGE, PLASMA, MAG
;
; Examples    :	See SSC_GET_WINDDATA
;
; Inputs      :	TIMERANGE = The time range over which to read the data.
;
; Opt. Inputs :	None
;
; Outputs     :	PLASMA = Structure array containing the PLASMA data.
;
;               MAG    = Structure array containing the MAG data.  The Bx, By,
;                        Bz, Lat, and Long tags are modified to convert from
;                        GSM to Geocentric RTN (GRTN) coordinates.
;
; Opt. Outputs:	None
;
; Keywords    :	None
;
; Env. vars.  : SSW_DSCOVR_DATA = Location of archived DSCOVR NetCDF files.
;
; Calls       :	ANYTIM2TAI, ANYTIM2UTC, DELVARX, ANYTIM2CAL, CONCAT_DIR,
;               UTC2STR, NCDF2UTC, CONVERT_SUNSPICE_COORD
;
; Common      :	None
;
; Restrictions:	None
;
; Side effects:	None
;
; Prev. Hist. :	Based on SSC_GET_ACEDATA
;
; History     :	Version 1, 2016-08-15, William Thompson, GSFC
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_get_dscovrdata, timerange, plasma, mag
;
;  Check the input parameters.
;
if n_params() ne 3 then message, $
  'Syntax: SSC_GET_DSCOVRDATA, TIMERANGE, PLASMA, MAG'
if n_elements(timerange) ne 2 then message, $
  'TIMERANGE must have two elements'
;
plasma0 = {mjd: 0L, time: 0L, p_density: 0.0, b_speed: 0.0, ion_temp: 0.0d0}
mag0 = {mjd: 0L, time: 0L, bx: 0.0, by: 0.0, bz: 0.0, bt: 0.0, $
        lat: 0.0, long: 0.0}
dscovr_data = getenv('SSW_DSCOVR_DATA')
;
utc = anytim2utc(timerange)
mjd0 = utc[0].mjd  &  time0 = utc[0].time
mjd1 = utc[1].mjd  &  time1 = utc[1].time
utc = utc[0]
;
;  Step through the days.
;
delvarx, plasma, mag
for mjd = mjd0,mjd1 do begin
    utc.mjd = mjd
    sdate = anytim2cal(utc, form=8, /date)
    dir = concat_dir(dscovr_data, strmid(utc2str(utc,/ecs), 0, 7))
;
;  Look for the plasma file for that day.  If the file exists, then open it for
;  read.
;
    file = file_search(dir, 'oe_f1m_dscovr_s' + sdate + '*.nc')
    if file[0] ne '' then begin
        id = ncdf_open(file[0])
;
;  Read in the data.
;
        ncdf_varget, id, 'time', time
        time = ncdf2utc(time)
;
        ncdf_varget, id, 'proton_density', density
        flag_missing, density, where(density lt 0), missing=!values.f_nan
;
        ncdf_varget, id, 'proton_speed', speed
        flag_missing, speed, where(speed lt 0), missing=!values.f_nan
;
        ncdf_varget, id, 'proton_temperature', temperature
        flag_missing, temperature, where(temperature lt 0), $
                      missing=!values.f_nan
;
;  Filter out data outside the requested range.
;
        if mjd eq mjd0 then begin
            w = where(time.time ge time0, count)
            if count gt 0 then begin
                time = time[w]
                density = density[w]
                speed = speed[w]
                temperature = temperature[w]
            end else delvarx, time
        endif
;
        if mjd eq mjd1 then begin
            w = where(time.time le time1, count)
            if count gt 0 then begin
                time = time[w]
                density = density[w]
                speed = speed[w]
                temperature = temperature[w]
            end else delvarx, time
        endif
;
;  If any data were read in, then add it to the output arrays.
;
        if n_elements(time) gt 0 then begin
            plasma1 = replicate(plasma0, n_elements(time))
            plasma1.mjd = time.mjd
            plasma1.time = time.time
            plasma1.p_density = density
            plasma1.b_speed = speed
            plasma1.ion_temp = temperature
            if n_elements(plasma) eq 0 then plasma = plasma1 else $
              plasma = [plasma, plasma1]
        endif
    endif                       ;File exists.
;
;  Look for the magnetometer file for that day.  If the file exists, then open
;  it for read.
;
    file = file_search(dir, 'oe_m1m_dscovr_s' + sdate + '*.nc')
    if file[0] ne '' then begin
        id = ncdf_open(file[0])
;
;  Read in the data.
;
        ncdf_varget, id, 'time', time
        time = ncdf2utc(time)
;
        ncdf_varget, id, 'bx_gsm', bx_gsm
        flag_missing, bx_gsm, where(bx_gsm eq -99999), missing=!values.f_nan
;
        ncdf_varget, id, 'by_gsm', by_gsm
        flag_missing, by_gsm, where(by_gsm eq -99999), missing=!values.f_nan
;
        ncdf_varget, id, 'bz_gsm', bz_gsm
        flag_missing, bz_gsm, where(bz_gsm eq -99999), missing=!values.f_nan
;
        ncdf_varget, id, 'phi_gsm', phi_gsm
        flag_missing, phi_gsm, where(phi_gsm eq -99999), missing=!values.f_nan
;
        ncdf_varget, id, 'theta_gsm', theta_gsm
        flag_missing, theta_gsm, where(theta_gsm eq -99999), $
                      missing=!values.f_nan
;
        ncdf_varget, id, 'bt', bt
        flag_missing, bt, where(bt lt 0), missing=!values.f_nan
;
;  Filter out data outside the requested range.
;
        if mjd eq mjd0 then begin
            w = where(time.time ge time0, count)
            if count gt 0 then begin
                time = time[w]
                bx_gsm = bx_gsm[w]
                by_gsm = by_gsm[w]
                bz_gsm = bz_gsm[w]
                phi_gsm = phi_gsm[w]
                theta_gsm = theta_gsm[w]
                bt = bt[w]
            end else delvarx, time
        endif
;
        if mjd eq mjd1 then begin
            w = where(time.time le time1, count)
            if count gt 0 then begin
                time = time[w]
                bx_gsm = bx_gsm[w]
                by_gsm = by_gsm[w]
                bz_gsm = bz_gsm[w]
                phi_gsm = phi_gsm[w]
                theta_gsm = theta_gsm[w]
                bt = bt[w]
            end else delvarx, time
        endif
;
;  If any data were read in, then add it to the output arrays.  Convert the
;  magnetometer data from GSM to Geocentric RTN coordinates.
;
        if n_elements(time) gt 0 then begin
            coord = fltarr(3,n_elements(time))
            coord[0,*] = bx_gsm
            coord[1,*] = by_gsm
            coord[2,*] = bz_gsm
            convert_sunspice_coord, time, coord, 'gsm', 'grtn'
;
            mag1 = replicate(mag0, n_elements(time))
            mag1.mjd = time.mjd
            mag1.time = time.time
            mag1.bx = reform(coord[0,*])
            mag1.by = reform(coord[1,*])
            mag1.bz = reform(coord[2,*])
            mag1.bt = bt
;
            mag1.lat = reform(asin(coord[2,*]/mag1.bt))  * !radeg
            lon = reform(atan(coord[1,*], coord[0,*])) * !radeg
            w = where(lon lt 0, count)
            if count gt 0 then lon[w] = lon[w] + 360
            mag1.long = lon
;
            if n_elements(mag) eq 0 then mag = mag1 else mag = [mag, mag1]
        endif
    endif                       ;File exists
endfor                          ;mjd
;
return
end
