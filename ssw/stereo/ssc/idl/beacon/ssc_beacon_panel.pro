;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_PANEL
;
; Purpose     :	Initialize a UTPLOT panel for beacon data
;
; Category    :	STEREO, Telemetry
;
; Explanation :	Sets up one of a series of UTPLOT panels.  The axes are
;               created, with a minimum of Y tick marks.  Follow this routine
;               with the OUTPLOT commands to plot the actual data.
;
; Syntax      :	SSC_BEACON_PANEL, TIMERANGE, YRANGE, 
;
; Examples    :	See SSC_INSITU_BEACON_PANELS
;
; Inputs      :	TIMERANGE = The (exact) range of time to be plotted.
;               YRANGE    = The range of Y values to be plotted.
;
; Opt. Inputs :	None.
;
; Outputs     :	Produces a plot panel.
;
; Opt. Outputs:	None.
;
; Keywords    :	ZEROCOLOR = If defined, then a dashed line representing zero is
;                           plotted with this color.
;
;               Also takes the standard UTPLOT keywords.
;
; Calls       :	UTPLOT
;
; Common      :	None.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 13-Feb-2006, William Thompson, GSFC
;               Version 2, 10-Jan-2007, William Thompson, GSFC
;                       Added support for logarithmic plots
;               Version 3, 16-Feb-2007, William Thompson, GSFC
;                       Fixed minor bug with tick value selection
;               Version 4, 26-Jun-2007, WTT, Added ZEROCOLOR keyword
;               Version 5, 24-Sep-2007, WTT, Added keyword YMINOR
;               Version 6, 31-Jan-2008, WTT, Handle explicit YTICKV
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_panel, timerange, yrange, ytitle=ytitle, yticklen=yticklen, $
                      ylog=ylog, zerocolor=zerocolor, yminor=yminor, $
                      ytickv=ytickv, _extra=_extra
;
if n_elements(yminor)   eq 0 then yminor   = 1
if n_elements(yticklen) eq 0 then yticklen = 0.02
;
ytickname = replicate(' ',30)
yticklen2 = yticklen/2
if n_elements(ytickv) gt 0 then yticklen2=1e-30
utplot, timerange, yrange, /nodata, xstyle=1, ytickname=ytickname, ytitle='', $
  yminor=yminor, yticklen=yticklen2, ylog=ylog, _extra=_extra
if n_elements(zerocolor) eq 1 then $
  oplot, !x.crange, [0,0], line=2, color=zerocolor
;
;  Unless explicitly passed, get the current axis tick marks.
;
if keyword_set(ylog) then ycrange=10^!y.crange else ycrange=!y.crange
yticks = n_elements(ytickv) - 1
if yticks lt 0 then begin
    axis, yaxis=0, yrange=ycrange, ystyle=4, yminor=yminor, ytick_get=ytickv
;
;  For logarithmic plots, work in log base 10, and filter off the top and
;  bottom ticks.
;
    if keyword_set(ylog) then begin
        yticks = (n_elements(ytickv)-1) > 1
        ytickv = ytickv[0:yticks-1]
        if yticks gt 2 then ytickv = ytickv[1:*]
        ycrange = alog10(ycrange)
        ytickv  = alog10(ytickv)
    endif
;
;  Choose no more than 3 tick marks.  Keep increasing the tick spacing by 2
;  until this requirement is met.
;
    delta0 = ytickv[1]-ytickv[0]
    i_delta = 1
    yticks = n_elements(ytickv)
    while yticks gt 3 do begin
        i_delta = i_delta + 1
        delta  = i_delta * delta0
;
;  Find all the multiples of DELTA within the data range.
;
        i_min = ceil (ycrange[0] / delta)
        i_max = floor(ycrange[1] / delta)
        if (1.01*i_max*delta) ge ycrange[1] then i_max = i_max - 1
        yticks = i_max - i_min + 1
        ytickv = (i_min + indgen(yticks)) * delta
    endwhile
;
    if keyword_set(ylog) then begin
        ycrange = 10^ycrange
        ytickv  = 10^ytickv
    endif
endif
;
axis, yaxis=0, yrange=ycrange, ystyle=1, yticks=yticks, ytickv=ytickv, $
      ytitle=ytitle, yticklen=yticklen, yminor=yminor, ylog=ylog, _extra=_extra
axis, yaxis=1, yrange=ycrange, ystyle=1, yticks=yticks, ytickv=ytickv, $
      ytitle='', yticklen=yticklen, yminor=yminor, ytickname=ytickname, $
      ylog=ylog, _extra=_extra
;
end
