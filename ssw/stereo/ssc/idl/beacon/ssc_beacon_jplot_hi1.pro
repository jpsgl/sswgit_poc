;+
; Project     :	STEREO - SSC
;
; Name        :	SSC_BEACON_JPLOT_HI1
;
; Purpose     :	Produce J-plots from HI1 beacon data
;
; Category    :	SECCHI, Quicklook
;
; Explanation :	This routine processes SECCHI/HI1 beacon images to produce
;               time-elongation plots (aka "J-plots").  Each file is processed
;               individually, and merged with previously processed data stored
;               in a common block.  An IDL save file is used to maintain data
;               between sessions.
;
; Syntax      :	SSC_BEACON_JPLOT, HI1, FILENAME
;
; Examples    :	See SSC_BROWSE_SECCHI_JPEG
;
; Inputs      :	FILENAME = The name of the HI1 file to process.
;
; Opt. Inputs :	None.
;
; Outputs     :	This routine writes GIF files to the $STEREO_BEACON_HTML
;               directory.
;
; Opt. Outputs:	None.
;
; Keywords    :	TEST_MODE = Used for testing via level-0 beacon files.
;
; Env. Vars.  :	STEREO_BEACON_HTML = Location of beacon web pages
;
; Calls       :	SECCHI_PREP, PARSE_STEREO_NAME, AVERAGE, UTC2TAI,
;               COR1_PBSERIES, DATATYPE, SCC_READ_SUMMARY, SCCFINDFITS,
;               ANYTIM2UTC, CHECK_INT_TIME, FITSHEAD2WCS, WCS_GET_COORD,
;               SETPLOT, NTRIM, UTPLOT_IMAGE, TVREAD, CONCAT_DIR, WRITE_GIF,
;               DELVARX
;
; Common      :	SSC_BEACON_JPLOT_HI1 is used internally to maintain
;               information between calls.
;
; Restrictions:	None.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 02-Jul-2009, William Thompson, GSFC
;               Version 2, 10-Jul-2009, WTT, better rejection of bad images
;               Version 3, 04-Nov-2009, WTT, Catch restore errors
;               Version 4, 09-Nov-2009, WTT, Wait 24 hours before deleting
;                                            save file
;               Version 5, 13-Nov-2009, WTT, Remove 24 hour rule.
;                       Save into temporary file first.
;               Version 6, 20-Dec-2010, WTT, Adjust for changed SECCHI_PREP
;               Version 7, 22-Dec-2015, WTT, No inf. loop for catch
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_beacon_jplot_hi1, filename, test_mode=test_mode
;
common ssc_beacon_jplot_hi1, utc_hi1, tai_hi1, jplot_hi1
;
;  If the common blocks variables haven't been defined yet, then look for an
;  IDL save file.
;
savefile = 'ssc_beacon_jplot_hi1.idl'
catch, error_status
if error_status ne 0 then begin
    catch,/cancel
    print, 'Deleting ' + savefile
    file_delete, savefile
endif
;
if (n_elements(utc_hi1) eq 0) and file_exist(savefile) then restore, savefile
;
;  Set the BEACON parameter based on the /TEST_MODE keyword
;
beacon = 1 - keyword_set(test_mode)
;
;  Set up the parameters.
;
tdelta = 7200.d0                        ;2 hours
thalf = tdelta / 2
sixhour = 21600D3                       ;milliseconds
nsix = round(sixhour / tdelta / 1000)   ;six hours
ndays  = 5
ntimes = (4*ndays+1)*nsix               ;N days + 6 hours
nstart = 0
rdelta = 0.25                           ;degrees
nrad = round(20 / rdelta)               ;From 4 to 24 degrees
rhalf = rdelta / 2
rad = 4 + rdelta*findgen(nrad) + rhalf  ;Central values
pa = [90]                               ;Position angles
npa = n_elements(pa)
pahalf = 15.                            ;Pos. angle half-width
;
;  If the size of the common block variables doesn't match the above
;  definitions, then reset them.
;
if n_elements(utc_hi1) ne ntimes then delvarx, utc_hi1, tai_hi1, jplot_hi1
;
;  Read in the file.
;
secchi_prep, sccfindfits(filename, beacon=beacon), h1, a1, /silent
if h1.nmissing gt 0 then return         ;Incomplete image
sc = parse_stereo_name(h1.obsrvtry, ['Ahead','Behind'])
if sc eq 'Ahead' then isc = 0 else isc = 1
tai1 = utc2tai(h1.date_obs)
;
;  Find the file from two hours earlier, +/- 15 minutes
;
t0 = tai1 - 7200.d0 - 450.d0
t0 = [t0, t0+900.d0]
utc0 = tai2utc(t0)
list = scc_read_summary(date=utc0, spacecraft=sc, telescope='hi1', $
                        type='img', beacon=beacon)
if datatype(list) ne 'STC' then return
w = where(list.dest eq h1.downlink, count)
if count gt 0 then list = list[w]
if list[0].nmiss gt 0 then return               ;Incomplete image
file0 = sccfindfits(list[0].filename, beacon=beacon)
;
;  Read in the earlier file, and form the difference image.
;
sz = size(a1)
secchi_prep, file0, h0, a0, /silent, outsize=sz[1]
if h0.nmissing gt 0 then return         ;Incomplete image
diff = median(a1 - a0, 3)
;
;  Make sure the common block variables are defined.
;
if n_elements(utc_hi1) eq 0 then begin
;
;  Calculate a start time on the appropriate 6 hour boundary on the previous
;  day, i.e. between 24-30 hours before the current time.
;
    tstart = anytim2utc(h1.date_obs)
    tstart.mjd = tstart.mjd - ndays
    tstart.time = sixhour * floor(tstart.time/sixhour) + thalf * 1000
    check_int_time, tstart
;
;  Generate an array of times.
;
    utc_hi1 = replicate(tstart, ntimes)
    utc_hi1.time = utc_hi1.time + tdelta * lindgen(ntimes) * 1000
    check_int_time, utc_hi1
    tai_hi1 = utc2tai(utc_hi1)
    tmin = min(tai_hi1, max=tmax)
;
;  Create the JPLOT array.
;
    jplot_hi1 = replicate(!values.f_nan, ntimes, nrad, npa, 2)
endif
;
;  If the time in the FITS file is significantly after the largest time in the
;  array, then move by six hours.
;
t1 = tai1 - thalf
while t1 gt tai_hi1[ntimes-1] do begin
    jplot_new = replicate(!values.f_nan, ntimes, nrad, npa, 2)
    jplot_new[0,0,0,0] = jplot_hi1[nsix:*,*,*,*]
    jplot_hi1 = temporary(jplot_new)
    utc_hi1 = replicate(utc_hi1[nsix], ntimes)
    utc_hi1.time = utc_hi1.time + tdelta * lindgen(ntimes) * 1000
    check_int_time, utc_hi1
    tai_hi1 = utc2tai(utc_hi1)
    tmin = min(tai_hi1, max=tmax)
endwhile
;
;  If the time is significantly earlier than the smallest time in the array,
;  then return.
;
t1 = tai1 + thalf
if t1 lt tai_hi1[0] then return
;
;  Determine the time index for these data.
;
del = abs(tai_hi1 - tai1)
w = where(del eq min(del), count)
itime = w[0]
if count gt 1 then for i=0,count-1 do $
  if not finite(jplot_hi1[w[i],0,0,isc]) then itime = w[i]
;
;  Calculate the position angle and radius arrays.
;
wcs = fitshead2wcs(h1)
coord = wcs_get_coord(wcs)
wcs_convert_from_coord, wcs, coord, 'hpr', pa1, hrlt
pa1 = abs(pa1)
rad1 = hrlt + 90
;
;  Calculate the J-plot values for each position angle.
;
for irad = 0,nrad-1 do begin
    for ipa = 0,npa-1 do begin
        pa0 = pa[ipa]
        w = where((rad1 ge (rad[irad]-rhalf)) and $
                  (rad1 le (rad[irad]+rhalf)) and $
                  (pa1 ge (pa0-pahalf)) and $
                  (pa1 le (pa0+pahalf)), count)
        if count gt 0 then jplot_hi1[itime,irad,ipa,isc] = $
          average(diff[w])
    endfor
endfor
;
;  Save the common block variables in the save file.
;
save, file=savefile+'.temp', utc_hi1, tai_hi1, jplot_hi1
file_move, savefile+'.temp', savefile, /overwrite
;
;  Make the GIF files.
;
;;jmax = 5
jmax = 1.8e-12
dname = !d.name
setplot, 'z'
device, set_resolution=[800,250]
loadct, 1, /silent
for ipa=0,npa-1 do begin
    latitude = 90 - pa[ipa]
    if latitude gt 0 then latitude = '+' + ntrim(latitude) else $
      latitude = ntrim(latitude)
    title = 'STEREO HI1 ' + sc + ', latitude ' + latitude
    utplot_image, jplot_hi1[nstart:*,*,ipa,isc], $
      utc_hi1[nstart:*], rad, min=-jmax, max=jmax, xticklen=-0.03, $
      yticklen=-0.01, ytitle='Degrees', title=title, yticks=5, yminor=3
    image = tvread(red, green, blue, /quiet)
    filename = 'jplot_hi1_' + strlowcase(sc) + '_' + $
      string(pa[ipa],format='(I3.3)') + '.gif'
    path = getenv('STEREO_BEACON_HTML')
    if path ne '' then filename = concat_dir(path, filename)
    write_gif, filename, image, red, green, blue
endfor
setplot, dname
;
end
