;+
; Project     :	STEREO - IMPACT, PLASTIC
;
; Name        : SSC_INSITU_BEACON_READ
;
; Purpose     : Read STEREO in-situ space weather beacon data
;
; Category    :	STEREO, IMPACT, PLASTIC
;
; Explanation :	Uses SSC_READ_CDF to read STEREO in-situ space weather beacon
;               data, for display on the web.
;
; Syntax      :	SSC_INSITU_BEACON_READ, IMPACT_A_CDFNAME, IMPACT_B_CDFNAME, $
;                       PLASTIC_A_CDFNAME, PLASTIC_B_CDFNAME
;
; Examples    :	See SSC_BEACON_PLOTS
;
; Inputs      :	IMPACT_A_CDFNAME  = List of IMPACT Beacon CDF filenames for the
;                                   Ahead spacecraft.  Null filenames are
;                                   ignored.
;               IMPACT_B_CDFNAME  = Same for the Behind spacecraft
;               PLASTIC_A_CDFNAME = Same for PLASTIC on Ahead spacecraft
;               PLASTIC_B_CDFNAME = Same for PLASTIC on Behind spacecraft
;
; Opt. Inputs :	None.
;
; Outputs     :	The parameters are stored in the common block SSC_INSITU_BEACON
;
; Opt. Outputs:	None.
;
; Keywords    :	None.
;
; Calls       :	SSC_READ_CDF
;
; Common      : Uses common block SSC_INSITU_BEACON from ssc_insitu_beacon.cmn
;
; Restrictions:	IMPACT beacon files must be version 2 or higher.
;
; Side effects:	None.
;
; Prev. Hist. :	None.
;
; History     :	Version 1, 26-Jul-2007, William Thompson, GSFC
;               Version 2, 03-Sep-2008, WTT, read ion temperatures
;               Version 3, 17-Aug-2009, WTT, read PLASTIC version 9
;
; Contact     :	WTHOMPSON
;-
;
pro ssc_insitu_beacon_read, impact_a_cdfname, impact_b_cdfname, $
                            plastic_a_cdfname, plastic_b_cdfname
;
@ssc_insitu_beacon.cmn
;
;  Read the IMPACT-Ahead parameters.  Make sure the version number is at least
;  02.
;
reread = 0
ssc_read_cdf, impact_a_cdfname, 'Epoch_MAG', epoch_mag_a, /epoch, $
  reread=reread, version=2
ssc_read_cdf, impact_a_cdfname, 'MAGBField', magbfield_a, reread=reread, $
  dim=3, version=2
;
ssc_read_cdf, impact_a_cdfname, 'Epoch_SEP', epoch_sep_a, /epoch, $
  reread=reread, epoch_sep_5a, version=2
ssc_read_cdf, impact_a_cdfname, 'SFSEPTElectrons', sfseptelectrons_a, $
  reread=reread, epoch_sep_5a, epoch_sep_a, sfseptelectrons_5a, $
  sfseptelectrons_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFHETElectrons', sfhetelectrons_a, $
  reread=reread, epoch_sep_5a, epoch_sep_a, sfhetelectrons_5a, $
  sfhetelectrons_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFSEPTIons1', sfseptions1_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfseptions1_5a, sfseptions1_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFSEPTIons2', sfseptions2_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfseptions2_5a, sfseptions2_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFLETProtons', sfletprotons_a, reread=reread,$
  epoch_sep_5a, epoch_sep_a, sfletprotons_5a, sfletprotons_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFHETProtons', sfhetprotons_a, reread=reread,$
  epoch_sep_5a, epoch_sep_a, sfhetprotons_5a, sfhetprotons_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFSITHe', sfsithe_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfsithe_5a, sfsithe_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFSITCNO', sfsitcno_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfsitcno_5a, sfsitcno_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFSITFe', sfsitfe_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfsitfe_5a, sfsitfe_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFLETHe', sflethe_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sflethe_5a, sflethe_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFLETCNO', sfletcno_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfletcno_5a, sfletcno_na, version=2
ssc_read_cdf, impact_a_cdfname, 'SFLETFe', sfletfe_a, reread=reread, $
  epoch_sep_5a, epoch_sep_a, sfletfe_5a, sfletfe_na, version=2
;
;  Read the IMPACT Behind parameters.
;
reread = 0
ssc_read_cdf, impact_b_cdfname, 'Epoch_MAG', epoch_mag_b, /epoch, $
  reread=reread, version=2
ssc_read_cdf, impact_b_cdfname, 'MAGBField', magbfield_b, reread=reread, $
  dim=3, version=2
;
ssc_read_cdf, impact_b_cdfname, 'Epoch_SEP', epoch_sep_b, /epoch, $
  reread=reread, epoch_sep_5b, version=2
ssc_read_cdf, impact_b_cdfname, 'SFSEPTElectrons', sfseptelectrons_b, $
  reread=reread, epoch_sep_5b, epoch_sep_b, sfseptelectrons_5b, $
  sfseptelectrons_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFHETElectrons', sfhetelectrons_b, $
  reread=reread, epoch_sep_5b, epoch_sep_b, sfhetelectrons_5b, $
  sfhetelectrons_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFSEPTIons1', sfseptions1_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfseptions1_5b, sfseptions1_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFSEPTIons2', sfseptions2_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfseptions2_5b, sfseptions2_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFLETProtons', sfletprotons_b, reread=reread,$
  epoch_sep_5b, epoch_sep_b, sfletprotons_5b, sfletprotons_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFHETProtons', sfhetprotons_b, reread=reread,$
  epoch_sep_5b, epoch_sep_b, sfhetprotons_5b, sfhetprotons_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFSITHe', sfsithe_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfsithe_5b, sfsithe_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFSITCNO', sfsitcno_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfsitcno_5b, sfsitcno_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFSITFe', sfsitfe_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfsitfe_5b, sfsitfe_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFLETHe', sflethe_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sflethe_5b, sflethe_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFLETCNO', sfletcno_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfletcno_5b, sfletcno_nb, version=2
ssc_read_cdf, impact_b_cdfname, 'SFLETFe', sfletfe_b, reread=reread, $
  epoch_sep_5b, epoch_sep_b, sfletfe_5b, sfletfe_nb, version=2
;
;  Read the PLASTIC Ahead parameters.
;
reread=0
ssc_read_cdf, plastic_a_cdfname, 'Epoch1', epoch_plastic_a, /epoch, $
  reread=reread, epoch_plastic_5a
ssc_read_cdf, plastic_a_cdfname, 'Density', density_a, reread=reread
;
;  Get the version number from the filename, and act accordingly.
;
delvarx, vparam, tparam
for i=0,n_elements(plastic_a_cdfname)-1 do begin
    break_file, plastic_a_cdfname[i], disk0, dir0, name0
    ver0 = fix(strmid(name0, strlen(name0)-2, 2))
    if ver0 ge 9 then begin
        ssc_read_cdf, plastic_a_cdfname[i], 'Velocity_RTN', velocity_a, $
          reread=reread, dim=4, rparam=vparam
        ssc_read_cdf, plastic_a_cdfname[i], 'Temperature_Inst', temperature_a, $
          epoch_plastic_5a, epoch_plastic_a, temperature_5a, temperature_na, $
          reread=reread, rparam=tparam
    end else begin
        ssc_read_cdf, plastic_a_cdfname[i], 'Velocity_HGRTN', velocity_a, $
          reread=reread, dim=4, rparam=vparam
        ssc_read_cdf, plastic_a_cdfname[i], 'Temperature_Inst', temperature_a, $
          epoch_plastic_5a, epoch_plastic_a, temperature_5a, temperature_na, $
          reread=reread, dim=3, /magnitude, rparam=tparam
    endelse
    reread = 0
endfor
;
;  Read in the PLASTIC Behind parameters.
;
reread=0
ssc_read_cdf, plastic_b_cdfname, 'Epoch1', epoch_plastic_b, /epoch, $
  reread=reread, epoch_plastic_5b
ssc_read_cdf, plastic_b_cdfname, 'Density', density_b, reread=reread
;
;  Get the version number from the filename, and act accordingly.
;
delvarx, vparam, tparam
for i=0,n_elements(plastic_b_cdfname)-1 do begin
    break_file, plastic_b_cdfname[i], disk0, dir0, name0
    ver0 = fix(strmid(name0, strlen(name0)-2, 2))
    if ver0 ge 9 then begin
        ssc_read_cdf, plastic_b_cdfname[i], 'Velocity_RTN', velocity_b, $
          reread=reread, dim=4, rparam=vparam
        ssc_read_cdf, plastic_b_cdfname[i], 'Temperature_Inst', temperature_b, $
          epoch_plastic_5b, epoch_plastic_b, temperature_5b, temperature_nb, $
          reread=reread, rparam=tparam
    end else begin
        ssc_read_cdf, plastic_b_cdfname[i], 'Velocity_HGRTN', velocity_b, $
          reread=reread, dim=4, rparam=vparam
        ssc_read_cdf, plastic_b_cdfname[i], 'Temperature_Inst', temperature_b, $
          epoch_plastic_5b, epoch_plastic_b, temperature_5b, temperature_nb, $
          reread=reread, dim=3, /magnitude, rparam=tparam
    endelse
    reread = 0
endfor
;
end
